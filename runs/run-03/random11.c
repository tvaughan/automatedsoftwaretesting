/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      1841162353
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   volatile uint64_t  f0;
   unsigned f1 : 12;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S1 {
   unsigned f0 : 8;
   volatile unsigned f1 : 26;
   signed f2 : 21;
   volatile unsigned f3 : 5;
   signed f4 : 10;
   signed f5 : 11;
   volatile signed f6 : 1;
};
#pragma pack(pop)

struct S2 {
   signed f0 : 27;
   unsigned f1 : 24;
   volatile signed f2 : 2;
   const volatile signed f3 : 5;
   const signed f4 : 22;
   const signed f5 : 11;
   unsigned f6 : 17;
   volatile unsigned f7 : 10;
   volatile unsigned f8 : 4;
   unsigned f9 : 26;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2 = 0x8F01BA02L;/* VOLATILE GLOBAL g_2 */
static uint16_t g_9 = 0x11D0L;
static uint16_t g_13 = 0xE01BL;
static uint16_t g_15 = 0xF998L;
static int64_t g_18 = 2L;
static uint16_t g_25[6] = {0x6A6CL,0x6A6CL,0x6A6CL,0x6A6CL,0x6A6CL,0x6A6CL};
static int32_t g_40 = (-1L);
static int32_t *g_39 = &g_40;
static int64_t g_63 = 5L;
static int8_t g_65[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
static uint64_t g_83 = 0xA6D6C577B3F785D6LL;
static uint32_t g_86 = 1UL;
static int64_t g_88 = 0L;
static uint32_t g_89 = 3UL;
static volatile int32_t g_93 = 0x15B29D1CL;/* VOLATILE GLOBAL g_93 */
static volatile uint32_t g_95 = 0x90685852L;/* VOLATILE GLOBAL g_95 */
static volatile uint32_t g_108 = 4UL;/* VOLATILE GLOBAL g_108 */
static int32_t ** volatile g_111 = &g_39;/* VOLATILE GLOBAL g_111 */
static volatile uint64_t g_112 = 5UL;/* VOLATILE GLOBAL g_112 */
static volatile struct S0 g_115 = {1UL,10};/* VOLATILE GLOBAL g_115 */
static int32_t g_117[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
static int16_t g_119 = 0xFA24L;
static volatile int32_t g_120 = 0x25565FB1L;/* VOLATILE GLOBAL g_120 */
static int16_t g_121 = 5L;
static uint16_t g_122[6][9][2] = {{{0x5237L,1UL},{1UL,0x54AEL},{0xBBA2L,0x8991L},{0xBBA2L,0x54AEL},{1UL,1UL},{0x5237L,0x8991L},{1UL,0xAFA2L},{1UL,0xAFA2L},{1UL,0x8991L}},{{0x5237L,1UL},{1UL,0x8991L},{0xEEBDL,65534UL},{0xEEBDL,0x8991L},{65531UL,1UL},{0x6814L,65534UL},{1UL,1UL},{65531UL,1UL},{1UL,65534UL}},{{0x6814L,1UL},{65531UL,0x8991L},{0xEEBDL,65534UL},{0xEEBDL,0x8991L},{65531UL,1UL},{0x6814L,65534UL},{1UL,1UL},{65531UL,1UL},{1UL,65534UL}},{{0x6814L,1UL},{65531UL,0x8991L},{0xEEBDL,65534UL},{0xEEBDL,0x8991L},{65531UL,1UL},{0x6814L,65534UL},{1UL,1UL},{65531UL,1UL},{1UL,65534UL}},{{0x6814L,1UL},{65531UL,0x8991L},{0xEEBDL,65534UL},{0xEEBDL,0x8991L},{65531UL,1UL},{0x6814L,65534UL},{1UL,1UL},{65531UL,1UL},{1UL,65534UL}},{{0x6814L,1UL},{65531UL,0x8991L},{0xEEBDL,65534UL},{0xEEBDL,0x8991L},{65531UL,1UL},{0x6814L,65534UL},{1UL,1UL},{65531UL,1UL},{1UL,65534UL}}};
static struct S2 g_125 = {-8101,530,0,0,-909,20,162,19,3,8188};/* VOLATILE GLOBAL g_125 */
static uint64_t g_130 = 18446744073709551615UL;
static struct S1 g_149 = {3,12,-103,1,-22,-8,0};/* VOLATILE GLOBAL g_149 */
static struct S0 g_159 = {7UL,6};/* VOLATILE GLOBAL g_159 */
static struct S0 *g_161 = &g_159;
static struct S0 ** volatile g_160 = &g_161;/* VOLATILE GLOBAL g_160 */
static uint32_t g_179 = 1UL;
static struct S2 g_185 = {-1945,2924,0,-1,-1423,-23,76,27,2,2777};/* VOLATILE GLOBAL g_185 */
static int32_t ** volatile g_203[7][9] = {{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39},{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39},{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39},{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39},{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39},{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39},{&g_39,(void*)0,&g_39,(void*)0,&g_39,&g_39,(void*)0,(void*)0,&g_39}};
static struct S1 g_242[10] = {{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0},{14,7037,310,3,18,-44,0}};
static uint64_t *g_268 = &g_83;
static uint64_t **g_267 = &g_268;
static uint64_t *** volatile g_266 = &g_267;/* VOLATILE GLOBAL g_266 */
static struct S2 ** volatile g_270 = (void*)0;/* VOLATILE GLOBAL g_270 */
static int32_t ** volatile g_272 = &g_39;/* VOLATILE GLOBAL g_272 */
static const volatile struct S1 g_296 = {14,1483,926,0,-11,18,-0};/* VOLATILE GLOBAL g_296 */
static volatile struct S1 g_298 = {6,681,-348,2,19,-7,0};/* VOLATILE GLOBAL g_298 */
static volatile struct S1 * volatile g_297 = &g_298;/* VOLATILE GLOBAL g_297 */
static uint16_t g_319 = 0xAC97L;
static int32_t g_344 = 0xEEBFF7D4L;
static int32_t **g_349 = (void*)0;
static int32_t *** volatile g_348[8][4][3] = {{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}},{{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0},{&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0}}};
static struct S1 g_385 = {9,835,54,2,-11,-8,-0};/* VOLATILE GLOBAL g_385 */
static struct S1 *g_384 = &g_385;
static const int32_t * const g_398[7][4] = {{&g_40,&g_40,&g_40,&g_40},{&g_40,&g_40,&g_40,&g_40},{&g_40,&g_40,&g_40,&g_40},{&g_40,&g_40,&g_40,&g_40},{&g_40,&g_40,&g_40,&g_40},{&g_40,&g_40,&g_40,&g_40},{&g_40,&g_40,&g_40,&g_40}};
static const int32_t * const *g_397 = &g_398[6][0];
static const int32_t * const **g_396 = &g_397;
static const int32_t * const ***g_395[8][3] = {{&g_396,(void*)0,(void*)0},{&g_396,(void*)0,&g_396},{&g_396,&g_396,(void*)0},{&g_396,&g_396,&g_396},{&g_396,&g_396,&g_396},{&g_396,(void*)0,(void*)0},{&g_396,(void*)0,&g_396},{&g_396,&g_396,(void*)0}};
static int32_t g_418 = 0L;
static int64_t g_436 = 1L;
static volatile struct S1 g_459 = {4,5917,-163,4,23,-40,-0};/* VOLATILE GLOBAL g_459 */
static struct S1 g_533 = {0,4863,595,2,15,20,-0};/* VOLATILE GLOBAL g_533 */
static struct S1 g_534 = {0,4938,1410,3,29,-8,0};/* VOLATILE GLOBAL g_534 */
static volatile struct S0 g_551 = {0x29A7B6F1853B3420LL,24};/* VOLATILE GLOBAL g_551 */
static volatile struct S0 * volatile g_553[7][10][3] = {{{(void*)0,&g_551,&g_551},{(void*)0,(void*)0,&g_115},{&g_551,&g_115,&g_551},{&g_115,&g_115,&g_115},{&g_115,&g_551,&g_115},{&g_115,&g_115,(void*)0},{&g_115,&g_115,&g_115},{&g_115,(void*)0,&g_551},{&g_551,&g_551,&g_551},{&g_551,&g_115,(void*)0}},{{&g_551,&g_551,&g_551},{&g_551,&g_115,&g_115},{&g_551,&g_115,&g_115},{&g_551,(void*)0,&g_551},{&g_551,&g_115,&g_115},{&g_551,&g_551,&g_115},{&g_551,&g_551,&g_551},{&g_115,(void*)0,&g_115},{&g_115,&g_551,(void*)0},{&g_115,&g_115,&g_551}},{{&g_115,&g_115,(void*)0},{&g_115,&g_551,&g_115},{&g_551,&g_115,&g_551},{(void*)0,&g_115,&g_115},{(void*)0,&g_551,&g_551},{(void*)0,&g_115,&g_115},{&g_551,(void*)0,&g_551},{&g_551,&g_551,(void*)0},{(void*)0,(void*)0,&g_115},{&g_551,&g_115,&g_551}},{{&g_551,&g_551,&g_551},{(void*)0,&g_551,(void*)0},{&g_551,&g_551,&g_115},{(void*)0,&g_115,&g_115},{&g_551,&g_551,&g_551},{&g_115,(void*)0,&g_551},{&g_551,&g_115,&g_115},{(void*)0,&g_551,&g_115},{&g_551,&g_551,(void*)0},{(void*)0,(void*)0,&g_551}},{{&g_551,&g_115,&g_115},{&g_551,&g_115,&g_115},{(void*)0,&g_551,&g_115},{&g_551,&g_551,&g_115},{&g_551,&g_115,&g_115},{(void*)0,&g_115,&g_551},{&g_115,(void*)0,(void*)0},{(void*)0,&g_551,&g_115},{&g_551,&g_115,&g_115},{&g_115,&g_115,&g_551}},{{&g_115,&g_115,&g_551},{&g_551,&g_115,&g_115},{&g_551,&g_115,&g_115},{&g_551,&g_551,(void*)0},{&g_551,(void*)0,&g_551},{&g_551,&g_115,&g_551},{&g_115,&g_115,&g_115},{(void*)0,&g_551,(void*)0},{&g_115,&g_551,&g_551},{(void*)0,&g_115,&g_115}},{{&g_115,&g_115,&g_551},{&g_551,(void*)0,&g_551},{&g_551,&g_551,&g_551},{&g_551,&g_551,&g_115},{&g_551,&g_115,(void*)0},{&g_551,(void*)0,&g_551},{&g_115,&g_551,(void*)0},{&g_115,&g_115,&g_115},{&g_551,&g_551,&g_551},{(void*)0,&g_551,&g_551}}};
static volatile struct S0 * volatile g_554 = &g_551;/* VOLATILE GLOBAL g_554 */
static struct S2 g_623 = {2835,932,-1,-1,996,-27,159,14,2,7681};/* VOLATILE GLOBAL g_623 */
static struct S2 g_624 = {-2686,1912,0,0,-857,-14,221,18,3,7452};/* VOLATILE GLOBAL g_624 */
static struct S2 g_625 = {-954,2236,-1,2,-945,26,196,8,1,3808};/* VOLATILE GLOBAL g_625 */
static struct S2 g_626 = {10844,2227,1,0,1074,-18,295,19,0,5916};/* VOLATILE GLOBAL g_626 */
static struct S1 g_642 = {8,6837,-1204,0,-7,33,0};/* VOLATILE GLOBAL g_642 */
static struct S1 * volatile g_643 = &g_149;/* VOLATILE GLOBAL g_643 */
static struct S1 g_676 = {13,6208,1236,0,9,-30,0};/* VOLATILE GLOBAL g_676 */
static struct S0 ** volatile g_682 = &g_161;/* VOLATILE GLOBAL g_682 */
static const int32_t *g_687 = &g_40;
static const int32_t ** volatile g_686 = &g_687;/* VOLATILE GLOBAL g_686 */
static uint64_t ***g_703[8][1][5] = {{{&g_267,&g_267,&g_267,&g_267,&g_267}},{{&g_267,(void*)0,&g_267,(void*)0,(void*)0}},{{(void*)0,&g_267,&g_267,&g_267,(void*)0}},{{&g_267,(void*)0,(void*)0,&g_267,&g_267}},{{&g_267,&g_267,(void*)0,&g_267,(void*)0}},{{&g_267,&g_267,&g_267,(void*)0,(void*)0}},{{&g_267,&g_267,&g_267,&g_267,&g_267}},{{(void*)0,&g_267,&g_267,&g_267,&g_267}}};
static int64_t *g_717 = (void*)0;
static int64_t **g_716 = &g_717;
static int64_t ***g_715[10] = {&g_716,&g_716,&g_716,&g_716,&g_716,&g_716,&g_716,&g_716,&g_716,&g_716};
static volatile int16_t g_741 = 0x6D0CL;/* VOLATILE GLOBAL g_741 */
static volatile int16_t * volatile g_740[4] = {&g_741,&g_741,&g_741,&g_741};
static volatile int16_t * const  volatile * volatile g_739 = &g_740[1];/* VOLATILE GLOBAL g_739 */
static volatile int16_t * const  volatile * volatile * volatile g_742 = &g_739;/* VOLATILE GLOBAL g_742 */
static int16_t *g_770 = &g_121;
static int16_t * volatile *g_769 = &g_770;
static struct S2 g_780 = {-3341,1084,0,3,-377,18,130,28,3,3177};/* VOLATILE GLOBAL g_780 */
static struct S2 g_781[1] = {{8126,2593,0,-0,1391,24,311,23,0,6311}};
static volatile struct S1 g_783[8] = {{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0},{11,7114,-1412,3,-29,28,0}};
static volatile int32_t g_858[3][8] = {{0x1F2EA3A1L,8L,8L,0x1F2EA3A1L,0x5DC2AECDL,0x1F2EA3A1L,8L,8L},{8L,0x5DC2AECDL,3L,3L,0x5DC2AECDL,8L,0x5DC2AECDL,3L},{0x1F2EA3A1L,0x5DC2AECDL,0x1F2EA3A1L,8L,8L,0x1F2EA3A1L,0x5DC2AECDL,0x1F2EA3A1L}};
static const int32_t ** volatile g_865 = &g_687;/* VOLATILE GLOBAL g_865 */
static struct S0 g_872 = {0x0DED5BCB8687870FLL,9};/* VOLATILE GLOBAL g_872 */
static struct S0 * volatile g_873 = &g_872;/* VOLATILE GLOBAL g_873 */
static volatile struct S0 g_896 = {18446744073709551615UL,22};/* VOLATILE GLOBAL g_896 */
static volatile uint32_t g_906[8] = {18446744073709551609UL,0UL,18446744073709551609UL,18446744073709551609UL,0UL,18446744073709551609UL,18446744073709551609UL,0UL};
static volatile uint32_t g_907 = 0xD43A15ACL;/* VOLATILE GLOBAL g_907 */
static volatile uint32_t g_908 = 1UL;/* VOLATILE GLOBAL g_908 */
static volatile uint32_t g_909 = 0xC6B6C95DL;/* VOLATILE GLOBAL g_909 */
static volatile uint32_t g_910 = 1UL;/* VOLATILE GLOBAL g_910 */
static volatile uint32_t g_911[2] = {7UL,7UL};
static volatile uint32_t *g_905[8][7] = {{(void*)0,&g_911[1],(void*)0,&g_911[1],(void*)0,(void*)0,&g_911[1]},{(void*)0,&g_906[0],(void*)0,&g_907,&g_907,(void*)0,&g_906[0]},{&g_911[1],(void*)0,(void*)0,(void*)0,(void*)0,&g_911[1],(void*)0},{(void*)0,&g_907,&g_907,(void*)0,&g_906[0],(void*)0,&g_907},{(void*)0,(void*)0,&g_911[1],(void*)0,&g_911[1],(void*)0,(void*)0},{&g_910,&g_907,&g_909,&g_907,&g_910,&g_910,&g_907},{&g_908,(void*)0,&g_908,&g_911[1],&g_911[1],&g_908,(void*)0},{&g_907,&g_906[0],&g_909,&g_909,&g_906[0],&g_907,&g_906[0]}};
static volatile uint32_t ** const g_904[9][3][9] = {{{&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4]},{&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0]},{&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4]}},{{&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0]},{&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4]},{&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0]}},{{&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4]},{&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0]},{&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4],&g_905[1][4],&g_905[1][3],&g_905[1][4]}},{{&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0],&g_905[5][0],(void*)0,&g_905[5][0]},{&g_905[1][4],&g_905[1][3],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]},{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]}},{{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]},{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]},{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]}},{{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]},{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]},{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]}},{{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]},{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]},{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]}},{{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]},{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]},{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]}},{{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]},{&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4],&g_905[1][4],&g_905[5][0],&g_905[1][4]},{&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1],&g_905[4][1],&g_905[1][4],&g_905[4][1]}}};
static volatile struct S0 g_919 = {0xBF32B83967713A04LL,16};/* VOLATILE GLOBAL g_919 */
static struct S2 g_926 = {-3840,2870,0,1,-1080,-4,89,21,0,5208};/* VOLATILE GLOBAL g_926 */
static volatile uint8_t g_933 = 1UL;/* VOLATILE GLOBAL g_933 */
static volatile uint8_t * volatile g_932 = &g_933;/* VOLATILE GLOBAL g_932 */
static volatile uint8_t * volatile *g_931 = &g_932;
static volatile struct S1 g_946[7][9] = {{{1,8075,640,0,-25,11,-0},{6,1984,-69,3,23,-23,0},{2,7021,-1277,1,3,-32,0},{2,7021,-1277,1,3,-32,0},{6,1984,-69,3,23,-23,0},{1,8075,640,0,-25,11,-0},{3,7577,-1154,0,-30,1,0},{8,2997,-767,0,24,-42,-0},{7,4592,699,3,23,19,0}},{{1,8075,640,0,-25,11,-0},{5,8036,983,4,16,-20,0},{0,58,244,3,-12,31,0},{2,7021,-1277,1,3,-32,0},{5,8036,983,4,16,-20,0},{8,6885,-722,3,-17,-29,0},{3,7577,-1154,0,-30,1,0},{6,1984,-69,3,23,-23,0},{5,6703,1276,1,-12,42,0}},{{1,8075,640,0,-25,11,-0},{8,2997,-767,0,24,-42,-0},{7,1653,-1047,4,25,0,-0},{2,7021,-1277,1,3,-32,0},{8,2997,-767,0,24,-42,-0},{10,2198,-921,4,8,13,-0},{3,7577,-1154,0,-30,1,0},{5,8036,983,4,16,-20,0},{3,7577,-1154,0,-30,1,0}},{{1,8075,640,0,-25,11,-0},{6,1984,-69,3,23,-23,0},{2,7021,-1277,1,3,-32,0},{2,7021,-1277,1,3,-32,0},{6,1984,-69,3,23,-23,0},{1,8075,640,0,-25,11,-0},{3,7577,-1154,0,-30,1,0},{8,2997,-767,0,24,-42,-0},{7,4592,699,3,23,19,0}},{{1,8075,640,0,-25,11,-0},{5,8036,983,4,16,-20,0},{0,58,244,3,-12,31,0},{2,7021,-1277,1,3,-32,0},{2,7021,-1277,1,3,-32,0},{4,7112,480,4,28,-37,0},{2,978,410,1,-2,-43,-0},{0,58,244,3,-12,31,0},{14,5490,-347,3,8,-23,0}},{{2,4111,-819,0,-25,-10,-0},{7,1653,-1047,4,25,0,-0},{4,5090,-1097,3,0,1,-0},{10,6088,5,0,1,-28,0},{7,1653,-1047,4,25,0,-0},{3,2922,786,2,-13,36,-0},{2,978,410,1,-2,-43,-0},{2,7021,-1277,1,3,-32,0},{2,978,410,1,-2,-43,-0}},{{2,4111,-819,0,-25,-10,-0},{0,58,244,3,-12,31,0},{10,6088,5,0,1,-28,0},{10,6088,5,0,1,-28,0},{0,58,244,3,-12,31,0},{2,4111,-819,0,-25,-10,-0},{2,978,410,1,-2,-43,-0},{7,1653,-1047,4,25,0,-0},{12,812,819,0,27,15,0}}};
static volatile struct S1 * volatile g_947 = &g_946[5][4];/* VOLATILE GLOBAL g_947 */
static struct S0 g_957[9] = {{0x25EDD9D212023132LL,45},{0x72AD09CF88EB080CLL,18},{0x25EDD9D212023132LL,45},{0x72AD09CF88EB080CLL,18},{0x25EDD9D212023132LL,45},{0x72AD09CF88EB080CLL,18},{0x25EDD9D212023132LL,45},{0x72AD09CF88EB080CLL,18},{0x25EDD9D212023132LL,45}};
static struct S2 g_958 = {7750,163,-0,1,-1080,-31,179,12,1,876};/* VOLATILE GLOBAL g_958 */
static uint8_t g_978 = 2UL;
static struct S0 *g_986 = &g_957[3];
static struct S0 ** volatile g_985[7] = {&g_986,&g_986,&g_986,&g_986,&g_986,&g_986,&g_986};
static int32_t * volatile g_1007 = &g_40;/* VOLATILE GLOBAL g_1007 */
static int32_t g_1024 = (-1L);
static int16_t **g_1059 = &g_770;
static int32_t *** volatile g_1070 = (void*)0;/* VOLATILE GLOBAL g_1070 */
static int32_t *** volatile g_1071 = &g_349;/* VOLATILE GLOBAL g_1071 */
static struct S0 ** volatile g_1074[6] = {&g_986,&g_986,&g_986,&g_986,&g_986,&g_986};
static struct S0 ** volatile g_1075 = &g_986;/* VOLATILE GLOBAL g_1075 */
static volatile struct S1 g_1086 = {1,228,370,1,28,24,0};/* VOLATILE GLOBAL g_1086 */
static struct S1 g_1199 = {13,2908,-7,3,-4,-20,-0};/* VOLATILE GLOBAL g_1199 */
static int32_t ***g_1230[2][9] = {{&g_349,&g_349,&g_349,&g_349,(void*)0,&g_349,&g_349,&g_349,&g_349},{&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349}};
static const int64_t **g_1268 = (void*)0;
static const int64_t ***g_1267 = &g_1268;
static const int64_t ****g_1266 = &g_1267;
static volatile struct S1 g_1276[1][3][5] = {{{{3,5188,-1217,4,20,22,-0},{7,5605,1269,4,16,34,0},{3,5188,-1217,4,20,22,-0},{15,6123,-739,0,21,-37,0},{15,6123,-739,0,21,-37,0}},{{3,5188,-1217,4,20,22,-0},{7,5605,1269,4,16,34,0},{3,5188,-1217,4,20,22,-0},{15,6123,-739,0,21,-37,0},{15,6123,-739,0,21,-37,0}},{{3,5188,-1217,4,20,22,-0},{7,5605,1269,4,16,34,0},{3,5188,-1217,4,20,22,-0},{15,6123,-739,0,21,-37,0},{15,6123,-739,0,21,-37,0}}}};
static uint32_t g_1289[6] = {9UL,9UL,0xB3DFF427L,9UL,9UL,0xB3DFF427L};
static struct S1 g_1307 = {1,5223,1037,1,-19,-34,0};/* VOLATILE GLOBAL g_1307 */
static volatile int32_t g_1326 = 5L;/* VOLATILE GLOBAL g_1326 */
static volatile int32_t *g_1325 = &g_1326;
static volatile int32_t * volatile *g_1324 = &g_1325;
static volatile int32_t * volatile ** volatile g_1323 = &g_1324;/* VOLATILE GLOBAL g_1323 */
static volatile int32_t * volatile ** volatile *g_1322 = &g_1323;
static volatile int32_t * volatile ** volatile * volatile *g_1321[10][5] = {{&g_1322,&g_1322,&g_1322,&g_1322,&g_1322},{&g_1322,(void*)0,&g_1322,(void*)0,&g_1322},{&g_1322,&g_1322,&g_1322,&g_1322,&g_1322},{&g_1322,(void*)0,&g_1322,(void*)0,&g_1322},{&g_1322,&g_1322,&g_1322,&g_1322,&g_1322},{&g_1322,(void*)0,&g_1322,(void*)0,&g_1322},{&g_1322,&g_1322,&g_1322,&g_1322,&g_1322},{&g_1322,(void*)0,&g_1322,(void*)0,&g_1322},{&g_1322,&g_1322,&g_1322,&g_1322,&g_1322},{&g_1322,(void*)0,&g_1322,(void*)0,&g_1322}};
static int32_t g_1331 = (-1L);
static volatile struct S1 *g_1363 = &g_1276[0][1][1];
static volatile struct S1 **g_1362 = &g_1363;
static volatile struct S1 *** const  volatile g_1361 = &g_1362;/* VOLATILE GLOBAL g_1361 */
static volatile uint16_t g_1392 = 1UL;/* VOLATILE GLOBAL g_1392 */
static volatile uint32_t g_1397 = 0x965ED418L;/* VOLATILE GLOBAL g_1397 */
static int8_t g_1420 = 0x0EL;
static struct S1 g_1429 = {2,1233,931,3,25,-26,0};/* VOLATILE GLOBAL g_1429 */
static const struct S0 g_1437 = {1UL,0};/* VOLATILE GLOBAL g_1437 */
static struct S0 * volatile g_1438 = &g_159;/* VOLATILE GLOBAL g_1438 */
static volatile uint32_t g_1470[1] = {4294967295UL};
static volatile uint32_t *g_1469 = &g_1470[0];
static volatile uint32_t * const *g_1468 = &g_1469;
static const int32_t ** volatile g_1476 = &g_687;/* VOLATILE GLOBAL g_1476 */
static struct S2 g_1506 = {3083,3943,0,4,223,-41,10,29,0,2467};/* VOLATILE GLOBAL g_1506 */
static struct S2 g_1508 = {-6918,610,-0,-3,1573,37,309,18,0,7680};/* VOLATILE GLOBAL g_1508 */
static struct S2 g_1510 = {5922,979,-1,-0,1356,-28,46,23,0,7279};/* VOLATILE GLOBAL g_1510 */
static struct S0 g_1517 = {0UL,53};/* VOLATILE GLOBAL g_1517 */
static struct S0 * volatile g_1518 = (void*)0;/* VOLATILE GLOBAL g_1518 */
static struct S0 g_1519 = {0UL,58};/* VOLATILE GLOBAL g_1519 */
static struct S1 g_1538 = {7,2037,291,1,-18,3,0};/* VOLATILE GLOBAL g_1538 */
static volatile struct S0 g_1550[5][8] = {{{0x7824AD43B62921F4LL,34},{0x793051B453B24F57LL,19},{0x96896CE04A28C6FALL,18},{0x793051B453B24F57LL,19},{0x7824AD43B62921F4LL,34},{0UL,27},{0x01BD71589FC1932CLL,45},{0x8DBCA84A4A9AE7E7LL,25}},{{0x793051B453B24F57LL,19},{1UL,10},{0UL,57},{0x96896CE04A28C6FALL,18},{0x96896CE04A28C6FALL,18},{0UL,57},{1UL,10},{0x793051B453B24F57LL,19}},{{0xAACD94002BB751D4LL,8},{0UL,27},{0UL,57},{1UL,37},{0x01BD71589FC1932CLL,45},{0x7824AD43B62921F4LL,34},{0x01BD71589FC1932CLL,45},{1UL,37}},{{0x96896CE04A28C6FALL,18},{0xBB1697F650BF3919LL,59},{0x96896CE04A28C6FALL,18},{0x8DBCA84A4A9AE7E7LL,25},{1UL,37},{0x7824AD43B62921F4LL,34},{0UL,57},{0UL,57}},{{0UL,57},{0UL,27},{0xAACD94002BB751D4LL,8},{0xAACD94002BB751D4LL,8},{0UL,27},{0UL,57},{1UL,37},{0x01BD71589FC1932CLL,45}}};
static volatile struct S0 * volatile g_1551 = &g_1550[3][1];/* VOLATILE GLOBAL g_1551 */
static uint32_t g_1553 = 4294967295UL;
static struct S0 g_1616 = {1UL,53};/* VOLATILE GLOBAL g_1616 */
static struct S1 g_1629 = {3,3811,1117,0,-0,-44,-0};/* VOLATILE GLOBAL g_1629 */
static int32_t ** volatile g_1678 = &g_39;/* VOLATILE GLOBAL g_1678 */
static int64_t ****g_1688[6] = {&g_715[3],&g_715[3],&g_715[3],&g_715[3],&g_715[3],&g_715[3]};
static int64_t *****g_1687 = &g_1688[1];
static volatile struct S0 * volatile g_1706 = &g_551;/* VOLATILE GLOBAL g_1706 */
static volatile int32_t g_1711 = 0x3259E7F5L;/* VOLATILE GLOBAL g_1711 */
static int32_t ** volatile g_1729 = &g_39;/* VOLATILE GLOBAL g_1729 */
static int32_t ** volatile g_1758 = &g_39;/* VOLATILE GLOBAL g_1758 */
static struct S2 *g_1783 = (void*)0;
static struct S2 ** volatile g_1782[1] = {&g_1783};
static struct S2 g_1793 = {9082,504,0,-4,1007,-27,87,19,1,4901};/* VOLATILE GLOBAL g_1793 */
static volatile struct S0 g_1794 = {0x020B2DE47E5F8F0BLL,21};/* VOLATILE GLOBAL g_1794 */
static struct S2 g_1895 = {-10337,348,0,-2,-735,-34,211,21,1,4566};/* VOLATILE GLOBAL g_1895 */
static uint8_t ****g_1948 = (void*)0;
static volatile uint8_t * volatile **g_1950 = &g_931;
static volatile uint8_t * volatile ***g_1949 = &g_1950;
static struct S1 g_1968 = {7,1213,425,2,-0,-28,-0};/* VOLATILE GLOBAL g_1968 */
static uint32_t *g_1993 = (void*)0;
static uint32_t **g_1992[4] = {&g_1993,&g_1993,&g_1993,&g_1993};
static uint32_t ***g_1991 = &g_1992[2];
static volatile struct S0 g_2002 = {0x78FF907EE7F2809ELL,59};/* VOLATILE GLOBAL g_2002 */
static const int8_t g_2027 = 1L;
static const int8_t *g_2026 = &g_2027;
static const int8_t **g_2025 = &g_2026;
static volatile struct S0 g_2056 = {1UL,52};/* VOLATILE GLOBAL g_2056 */
static struct S2 g_2086 = {7814,880,1,-0,-158,-7,80,9,3,8085};/* VOLATILE GLOBAL g_2086 */
static struct S0 g_2098 = {0x49ED9AD2610EC722LL,24};/* VOLATILE GLOBAL g_2098 */
static uint32_t **g_2132 = (void*)0;
static volatile struct S0 g_2173 = {1UL,6};/* VOLATILE GLOBAL g_2173 */
static volatile struct S0 * volatile g_2174 = &g_2002;/* VOLATILE GLOBAL g_2174 */
static struct S2 g_2209 = {3875,2388,-1,-0,-1065,14,235,23,1,4935};/* VOLATILE GLOBAL g_2209 */
static int8_t g_2220 = 0x24L;
static uint64_t g_2257 = 4UL;
static int32_t **** const  volatile g_2266[10] = {&g_1230[1][1],&g_1230[1][1],&g_1230[1][1],&g_1230[1][1],&g_1230[1][3],&g_1230[1][1],&g_1230[1][3],&g_1230[1][3],&g_1230[1][1],&g_1230[1][3]};
static uint16_t g_2290 = 65531UL;
static uint64_t g_2338 = 0x74CCD8308F94D16ALL;
static volatile struct S1 g_2410 = {9,7313,732,0,17,-29,-0};/* VOLATILE GLOBAL g_2410 */


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static int8_t  func_3(int64_t  p_4, uint32_t  p_5);
static int32_t  func_19(int64_t * p_20);
static int64_t * func_21(int32_t  p_22);
static uint64_t  func_26(int16_t  p_27, uint32_t  p_28, uint32_t  p_29, int32_t  p_30);
static const uint32_t  func_31(uint16_t  p_32, int64_t * const  p_33, int32_t * p_34, uint16_t * p_35, uint16_t  p_36);
static int64_t * const  func_37(int32_t * p_38);
static int32_t  func_43(int64_t * p_44, int8_t  p_45);
static int64_t * func_46(int32_t  p_47);
static struct S2  func_48(uint16_t  p_49);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_9 g_15 g_39 g_13 g_40 g_63 g_65 g_88 g_95 g_108 g_111 g_112 g_115 g_122 g_125 g_130 g_117 g_18 g_149 g_296 g_297 g_319 g_344 g_119 g_270 g_384 g_385.f1 g_159.f0 g_185.f6 g_268 g_83 g_159 g_395 g_396 g_397 g_398 g_242.f0 g_121 g_459 g_242.f2 g_385.f3 g_185.f0 g_185.f5 g_86 g_185.f4 g_266 g_267 g_551 g_554 g_533.f5 g_272 g_160 g_161 g_533.f2 g_385.f0 g_534.f5 g_623.f8 g_642 g_643 g_242.f3 g_676 g_682 g_242.f6 g_686 g_687 g_185.f1 g_715 g_623.f0 g_625.f6 g_298 g_739 g_742 g_740 g_741 g_769 g_770 g_783 g_623.f6 g_625.f0 g_624.f6 g_865 g_872 g_873 g_781.f4 g_896 g_904 g_623.f4 g_919 g_931 g_946 g_947 g_626.f1 g_957 g_958 g_625.f5 g_906 g_1007 g_932 g_933 g_1024 g_624.f0 g_781.f0 g_1071 g_1075 g_1086 g_1059 g_626.f6 g_911 g_926.f9 g_385.f2 g_1199 g_626.f4 g_926.f4 g_185 g_781 g_1276 g_89 g_1289 g_1307 g_978 g_1321 g_436 g_1324 g_1325 g_1326 g_242.f4 g_1361 g_780.f9 g_1322 g_1323 g_1392 g_1397 g_625.f1 g_1429 g_1362 g_1363 g_1517 g_1538 g_533.f4 g_1550 g_1551 g_1553 g_1469 g_1470 g_625.f9 g_1331 g_1616 g_1629 g_717 g_179 g_1506.f0 g_1678 g_1468 g_1706 g_1729 g_1758 g_858 g_1793 g_1794 g_1506.f9 g_926.f1 g_626.f0 g_780.f1 g_1895 g_1420 g_1949 g_909 g_1968 g_624.f4 g_1991 g_1476 g_2002 g_2026 g_2027 g_2056 g_2025 g_1510.f7 g_1508.f1 g_2086 g_1950 g_2098 g_2132 g_1506.f3 g_25 g_2173 g_2174 g_1992 g_2209 g_2220 g_2257 g_625.f2 g_2338 g_1519.f0 g_926.f2 g_2410
 * writes: g_9 g_13 g_15 g_18 g_25 g_40 g_63 g_65 g_83 g_86 g_88 g_89 g_95 g_108 g_39 g_112 g_122 g_2 g_130 g_121 g_125.f0 g_298 g_319 g_179 g_395 g_551 g_436 g_161 g_149 g_687 g_703 g_626.f6 g_739 g_716 g_872 g_715 g_946 g_978 g_986 g_1059 g_349 g_344 g_1024 g_1230 g_1266 g_1289 g_1331 g_1326 g_1397 g_1420 g_1276 g_1519 g_1325 g_1550 g_1553 g_185.f9 g_717 g_119 g_1687 g_125.f1 g_858 g_1948 g_1991 g_1688 g_2025 g_1517.f1 g_159 g_2002 g_2257 g_2290 g_2338
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    uint16_t *l_8 = &g_9;
    uint16_t *l_12 = &g_13;
    uint16_t *l_14 = &g_15;
    const int32_t l_16 = 4L;
    int64_t *l_17 = &g_18;
    int32_t *l_2409 = &g_40;
    (*l_2409) = (g_2 && func_3(((*l_17) = (safe_sub_func_uint16_t_u_u(((*l_14) = ((*l_12) = ((*l_8)--))), l_16))), l_16));
    for (g_130 = 3; (g_130 <= 9); g_130 += 1)
    { /* block id: 1066 */
        (**g_1362) = g_2410;
    }
    return (*g_932);
}


/* ------------------------------------------ */
/* 
 * reads : g_15 g_9 g_2 g_39 g_13 g_40 g_63 g_65 g_88 g_95 g_108 g_111 g_112 g_115 g_122 g_125 g_130 g_117 g_18 g_149 g_296 g_297 g_319 g_344 g_119 g_270 g_384 g_385.f1 g_159.f0 g_185.f6 g_268 g_83 g_159 g_395 g_396 g_397 g_398 g_242.f0 g_121 g_459 g_242.f2 g_385.f3 g_185.f0 g_185.f5 g_86 g_185.f4 g_266 g_267 g_551 g_554 g_533.f5 g_272 g_160 g_161 g_533.f2 g_385.f0 g_534.f5 g_623.f8 g_642 g_643 g_242.f3 g_676 g_682 g_242.f6 g_686 g_687 g_185.f1 g_715 g_623.f0 g_625.f6 g_298 g_739 g_742 g_740 g_741 g_769 g_770 g_783 g_623.f6 g_625.f0 g_624.f6 g_865 g_872 g_873 g_781.f4 g_896 g_904 g_623.f4 g_919 g_931 g_946 g_947 g_626.f1 g_957 g_958 g_625.f5 g_906 g_1007 g_932 g_933 g_1024 g_624.f0 g_781.f0 g_1071 g_1075 g_1086 g_1059 g_626.f6 g_911 g_926.f9 g_385.f2 g_1199 g_626.f4 g_926.f4 g_185 g_781 g_1276 g_89 g_1289 g_1307 g_978 g_1321 g_436 g_1324 g_1325 g_1326 g_242.f4 g_1361 g_780.f9 g_1322 g_1323 g_1392 g_1397 g_625.f1 g_1429 g_1362 g_1363 g_1517 g_1538 g_533.f4 g_1550 g_1551 g_1553 g_1469 g_1470 g_625.f9 g_1331 g_1616 g_1629 g_717 g_179 g_1506.f0 g_1678 g_1468 g_1706 g_1729 g_1758 g_858 g_1793 g_1794 g_1506.f9 g_926.f1 g_626.f0 g_780.f1 g_1895 g_1420 g_1949 g_909 g_1968 g_624.f4 g_1991 g_1476 g_2002 g_2026 g_2027 g_2056 g_2025 g_1510.f7 g_1508.f1 g_2086 g_1950 g_2098 g_2132 g_1506.f3 g_25 g_2173 g_2174 g_1992 g_2209 g_2220 g_2257 g_625.f2 g_2338 g_1519.f0 g_926.f2
 * writes: g_25 g_40 g_63 g_18 g_65 g_83 g_86 g_88 g_13 g_89 g_95 g_108 g_39 g_112 g_122 g_2 g_130 g_121 g_125.f0 g_298 g_319 g_179 g_395 g_551 g_436 g_161 g_149 g_687 g_703 g_626.f6 g_739 g_716 g_872 g_715 g_946 g_978 g_986 g_9 g_1059 g_349 g_15 g_344 g_1024 g_1230 g_1266 g_1289 g_1331 g_1326 g_1397 g_1420 g_1276 g_1519 g_1325 g_1550 g_1553 g_185.f9 g_717 g_119 g_1687 g_125.f1 g_858 g_1948 g_1991 g_1688 g_2025 g_1517.f1 g_159 g_2002 g_2257 g_2290 g_2338
 */
static int8_t  func_3(int64_t  p_4, uint32_t  p_5)
{ /* block id: 5 */
    int32_t l_23 = 0L;
    int32_t *l_24[2];
    int64_t **l_1628 = &g_717;
    uint64_t l_1930 = 18446744073709551615UL;
    const int64_t ****l_1941 = &g_1267;
    int8_t *l_1942 = &g_1420;
    uint8_t *l_1947 = &g_978;
    uint8_t **l_1946 = &l_1947;
    uint8_t ***l_1945 = &l_1946;
    uint8_t ****l_1944[2][7][9] = {{{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945,&l_1945},{&l_1945,&l_1945,(void*)0,&l_1945,(void*)0,&l_1945,&l_1945,(void*)0,&l_1945},{&l_1945,&l_1945,(void*)0,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945,&l_1945,(void*)0},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945,&l_1945,&l_1945},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945}},{{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945,&l_1945,&l_1945},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,(void*)0,&l_1945,&l_1945},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0},{&l_1945,&l_1945,&l_1945,(void*)0,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945},{(void*)0,(void*)0,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945},{&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945},{(void*)0,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,(void*)0,&l_1945,&l_1945}}};
    uint8_t *****l_1943[8];
    int32_t l_1970 = 0L;
    uint8_t l_1975[8][2] = {{5UL,0x18L},{0xC3L,0xC3L},{0xC3L,0x18L},{5UL,1UL},{0x18L,1UL},{5UL,0x18L},{0xC3L,0xC3L},{0xC3L,0x18L}};
    const int8_t *l_2024[6];
    const int8_t **l_2023 = &l_2024[2];
    int16_t l_2054 = 0L;
    uint64_t ****l_2097 = &g_703[3][0][3];
    uint8_t l_2101 = 7UL;
    uint32_t ***l_2146 = &g_1992[2];
    uint16_t l_2169 = 0x1746L;
    uint16_t l_2190[1][5][6] = {{{0x864FL,65532UL,0x54F3L,0xB6F0L,0x40B7L,0x0189L},{0x0189L,65527UL,65535UL,65527UL,0x0189L,65532UL},{0x0189L,0x864FL,65527UL,0xB6F0L,1UL,1UL},{0x864FL,0x40B7L,0x40B7L,0x864FL,65535UL,1UL},{0x54F3L,1UL,65527UL,65532UL,0xB6F0L,65532UL}}};
    uint16_t l_2217[8] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
    int64_t l_2218 = 0xA9DDD121242E491FLL;
    int32_t l_2236[2][6] = {{0xAEB6FD9CL,(-1L),6L,(-1L),0xAEB6FD9CL,(-1L)},{6L,0L,0L,6L,0xAEB6FD9CL,6L}};
    uint32_t l_2245 = 0xF8F12ADEL;
    int32_t ***l_2267 = &g_349;
    int32_t l_2336[6][8] = {{0xBC618A3FL,0x1D974592L,0xBC618A3FL,0L,0L,0xBC618A3FL,0x1D974592L,0xBC618A3FL},{(-3L),0L,0xB8B3FD80L,0L,(-3L),(-3L),0L,0xB8B3FD80L},{(-3L),(-3L),0L,0xB8B3FD80L,0L,(-3L),(-3L),0L},{0xBC618A3FL,0L,0L,0xBC618A3FL,0x1D974592L,0xBC618A3FL,0L,0L},{0L,0x1D974592L,0xB8B3FD80L,0xB8B3FD80L,0x1D974592L,0L,0x1D974592L,0xB8B3FD80L},{0xBC618A3FL,0x1D974592L,0xBC618A3FL,0L,0L,0xBC618A3FL,0x1D974592L,0xBC618A3FL}};
    int32_t * const l_2377 = (void*)0;
    int32_t * const *l_2376[4][10] = {{&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377},{&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377},{&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377},{&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377,&l_2377}};
    int32_t * const *l_2378 = &l_2377;
    int32_t * const *l_2379 = (void*)0;
    int32_t * const *l_2380 = (void*)0;
    int32_t * const *l_2381 = &l_2377;
    int32_t * const *l_2382 = (void*)0;
    int32_t * const *l_2383[4][2][2] = {{{(void*)0,&l_2377},{&l_2377,(void*)0}},{{(void*)0,&l_2377},{&l_2377,(void*)0}},{{&l_2377,&l_2377},{(void*)0,(void*)0}},{{&l_2377,&l_2377},{(void*)0,&l_2377}}};
    int32_t * const ** const l_2375[9][7] = {{(void*)0,&l_2382,&l_2382,(void*)0,&l_2381,(void*)0,&l_2382},{(void*)0,(void*)0,&l_2380,(void*)0,&l_2380,(void*)0,(void*)0},{&l_2379,&l_2382,&l_2376[1][6],&l_2382,&l_2379,&l_2379,&l_2382},{&l_2383[1][1][1],&l_2378,&l_2383[1][1][1],&l_2380,&l_2380,&l_2383[1][1][1],&l_2378},{&l_2382,&l_2381,&l_2376[1][6],&l_2376[1][6],&l_2381,&l_2382,&l_2381},{&l_2383[1][1][1],&l_2380,&l_2380,&l_2383[1][1][1],&l_2378,&l_2383[1][1][1],&l_2380},{&l_2379,&l_2379,&l_2382,&l_2376[1][6],&l_2382,&l_2379,&l_2379},{(void*)0,&l_2380,(void*)0,&l_2380,(void*)0,(void*)0,&l_2380},{(void*)0,&l_2381,(void*)0,&l_2382,&l_2382,(void*)0,&l_2381}};
    int32_t * const ** const * const l_2374 = &l_2375[6][1];
    int32_t * const ** const * const *l_2373 = &l_2374;
    int32_t * const ** const l_2386 = (void*)0;
    int32_t * const ** const * const l_2385 = &l_2386;
    int32_t * const ** const * const *l_2384 = &l_2385;
    uint32_t *l_2393[2];
    uint32_t **l_2392 = &l_2393[0];
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_24[i] = (void*)0;
    for (i = 0; i < 8; i++)
        l_1943[i] = &l_1944[1][1][5];
    for (i = 0; i < 6; i++)
        l_2024[i] = &g_65[4];
    for (i = 0; i < 2; i++)
        l_2393[i] = &g_1553;
    (*g_1007) = func_19(((*l_1628) = func_21(((l_23 == (((g_25[2] = 5L) , 5UL) & func_26(g_15, g_9, (g_2 || func_31(g_15, func_37(l_24[1]), g_39, &g_15, g_9)), g_625.f5))) < 0L))));
    if ((((((((void*)0 == &g_1783) || (safe_sub_func_int64_t_s_s((p_4 , 2L), l_1930))) , (((safe_div_func_uint16_t_u_u(((safe_mul_func_uint32_t_u_u((safe_sub_func_int8_t_s_s(0L, ((safe_div_func_int32_t_s_s((safe_rshift_func_int8_t_s_s(p_4, ((g_1948 = (((*l_1942) = (((void*)0 == l_1941) | 65535UL)) , (void*)0)) != g_1949))), p_5)) > p_5))), p_5)) || 0x67D7L), p_5)) || p_5) | 0xB9L)) ^ g_909) <= 0x1E91L) && (*g_268)))
    { /* block id: 898 */
        int32_t ***l_1956[9][7] = {{&g_349,(void*)0,&g_349,&g_349,&g_349,&g_349,&g_349},{(void*)0,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349},{&g_349,(void*)0,&g_349,&g_349,&g_349,&g_349,&g_349},{&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349},{&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349},{&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349},{(void*)0,&g_349,(void*)0,&g_349,&g_349,(void*)0,&g_349},{&g_349,&g_349,&g_349,&g_349,&g_349,&g_349,&g_349},{&g_349,&g_349,&g_349,&g_349,&g_349,(void*)0,(void*)0}};
        uint8_t *****l_1969 = &l_1944[1][1][5];
        int16_t l_1976[6] = {0x524DL,0x524DL,0x524DL,0x524DL,0x524DL,0x524DL};
        int32_t l_1997[7][4] = {{0xFACFC060L,0xFACFC060L,0L,0x15BEFCB4L},{0x3E2F3346L,0x326F7BA1L,0x3E2F3346L,0L},{0x3E2F3346L,0L,0L,0x3E2F3346L},{0xFACFC060L,0L,0x15BEFCB4L,0L},{0L,0x326F7BA1L,0x15BEFCB4L,0x15BEFCB4L},{0xFACFC060L,0xFACFC060L,0L,0x15BEFCB4L},{0x3E2F3346L,0x326F7BA1L,0x3E2F3346L,0L}};
        int64_t l_2011 = 0x5953B077A0422CCCLL;
        int8_t l_2080 = 0xD7L;
        uint32_t *l_2131 = &g_89;
        uint32_t **l_2130 = &l_2131;
        uint32_t ***l_2147 = &g_1992[3];
        int64_t **l_2223 = &g_717;
        int8_t l_2224 = 8L;
        uint16_t *l_2334 = &g_15;
        uint32_t l_2365 = 18446744073709551615UL;
        int i, j;
        for (g_119 = 13; (g_119 <= (-30)); g_119 = safe_sub_func_int16_t_s_s(g_119, 9))
        { /* block id: 901 */
            uint64_t l_1955 = 0UL;
            int32_t ****l_1957 = &g_1230[1][3];
            int64_t *l_1971 = (void*)0;
            int64_t *l_1972 = (void*)0;
            int64_t *l_1973[6][4][2] = {{{(void*)0,&g_436},{&g_18,&g_18},{&g_18,&g_436},{(void*)0,(void*)0}},{{&g_436,(void*)0},{(void*)0,&g_436},{&g_18,&g_18},{&g_18,&g_436}},{{(void*)0,(void*)0},{&g_436,(void*)0},{(void*)0,&g_436},{&g_18,&g_18}},{{&g_18,&g_436},{(void*)0,(void*)0},{&g_436,(void*)0},{(void*)0,&g_436}},{{&g_18,&g_18},{&g_18,&g_436},{(void*)0,(void*)0},{&g_436,(void*)0}},{{(void*)0,&g_436},{&g_18,&g_18},{&g_18,&g_436},{(void*)0,(void*)0}}};
            int32_t l_1974 = 0x6CEED222L;
            int32_t l_1977 = (-1L);
            int32_t l_1978 = 0x730AFC9AL;
            int32_t l_1979[1][8] = {{3L,3L,3L,3L,3L,3L,3L,3L}};
            uint8_t ****l_2005[6] = {&l_1945,&l_1945,&l_1945,&l_1945,&l_1945,&l_1945};
            uint32_t l_2008 = 0x5736F658L;
            int64_t l_2055 = 0x0633B5AC01637A76LL;
            int64_t l_2072 = (-3L);
            int16_t l_2088 = 0xCBE3L;
            int32_t *l_2104 = &l_1977;
            uint32_t l_2170 = 0xDCB692BDL;
            uint32_t l_2179 = 0xC6C4D5F3L;
            uint32_t **l_2201 = &g_1993;
            uint64_t l_2221 = 0UL;
            uint64_t l_2225 = 0x1EFA3E66F1F6D6D7LL;
            uint8_t *****l_2241 = &l_1944[1][3][0];
            uint16_t l_2289 = 65535UL;
            uint32_t ****l_2293 = &g_1991;
            int32_t l_2298 = 0L;
            int i, j, k;
            if (((safe_add_func_uint16_t_u_u(((l_1955 = ((**l_1628) ^= p_4)) && (p_4 = (((*l_1957) = l_1956[7][0]) != (((safe_lshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s(((l_1974 ^= ((*g_268) != ((*g_717) = ((safe_mul_func_int16_t_s_s((safe_lshift_func_int64_t_s_s((0x7EL != (((safe_rshift_func_uint8_t_u_u((((g_1968 , l_1969) == (l_1943[6] = (l_1955 , &g_1948))) && (((p_5 <= g_624.f4) <= 4L) || l_1970)), p_5)) || 0xBB2B1666AC21E7AELL) | p_4)), 58)), p_5)) || p_5)))) & p_4), 0x10A5EB146457C4E0LL)), p_4)) == 0xA2A86C69L) , l_1956[7][0])))), 0L)) <= (*g_268)))
            { /* block id: 909 */
                return l_1975[3][0];
            }
            else
            { /* block id: 911 */
                uint8_t l_1980 = 0x4AL;
                uint32_t ****l_1994 = &g_1991;
                int64_t ****l_1995[3];
                int64_t *****l_1996 = &g_1688[1];
                int i;
                for (i = 0; i < 3; i++)
                    l_1995[i] = (void*)0;
                l_1980++;
                (****g_1322) ^= p_5;
                if (l_1980)
                    continue;
                l_1974 &= (safe_mul_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s(p_5, (safe_lshift_func_int8_t_s_u((g_1276[0][2][1].f6 <= g_185.f1), 6)))) | ((((safe_add_func_uint64_t_u_u((&g_904[8][2][4] != ((*l_1994) = g_1991)), ((l_1995[0] != ((*l_1996) = l_1995[1])) , ((0L <= (((l_1977 |= (p_5 | (**g_267))) == (***g_396)) ^ 1UL)) , (*g_268))))) , p_4) != g_1629.f2) , (-5L))), l_1997[1][1]));
            }
            (*g_1476) = &l_1978;
            (****g_1322) ^= 0xBE7CA82CL;
            if (((((safe_mul_func_int32_t_s_s((safe_lshift_func_int32_t_s_s((((g_2002 , (safe_mul_func_uint32_t_u_u(((&g_1950 == ((*l_1969) = l_2005[0])) < (&l_24[0] == (void*)0)), ((*g_1469) && (!(g_783[2].f5 , (!0x1615A0E3L))))))) <= ((l_1978 = p_5) > g_130)) , 0x4A862867L), p_5)), l_2008)) == 0x2AC4L) != 0xD5L) , 0xAC4E3724L))
            { /* block id: 924 */
                uint32_t *l_2015 = (void*)0;
                const uint32_t l_2018 = 0x80F5ECD5L;
                uint32_t *l_2020 = &g_1553;
                uint32_t **l_2019 = &l_2020;
                uint16_t *l_2028[2];
                int32_t l_2058 = 0x4B316CF7L;
                uint32_t ***l_2062 = &g_1992[2];
                int32_t l_2068[5];
                int16_t l_2121 = (-1L);
                int64_t *l_2164 = &l_2055;
                int i;
                for (i = 0; i < 2; i++)
                    l_2028[i] = &g_25[2];
                for (i = 0; i < 5; i++)
                    l_2068[i] = 0L;
                if (((p_4 >= (safe_add_func_uint16_t_u_u(l_2011, ((p_4 & (safe_unary_minus_func_int32_t_s((safe_add_func_uint64_t_u_u((l_2015 == ((*l_2019) = ((safe_mod_func_int8_t_s_s(p_5, l_2018)) , &g_1553))), (p_5 & ((g_319 = (((safe_rshift_func_int32_t_s_s((((g_2025 = l_2023) != &l_1942) && 0x97L), 30)) || 1L) <= g_125.f9)) <= (*g_770)))))))) >= 0x4690L)))) , 1L))
                { /* block id: 928 */
                    int32_t *l_2029 = &l_1997[1][1];
                    (**g_1324) ^= (l_1977 = p_5);
                    (*g_1758) = l_2029;
                }
                else
                { /* block id: 932 */
                    int32_t **l_2057 = &l_24[0];
                    int32_t l_2100 = 0xB09E31F1L;
                    if (((((safe_unary_minus_func_uint16_t_u(g_242[7].f0)) <= ((safe_mul_func_int32_t_s_s((***g_1323), (((safe_mod_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((((safe_rshift_func_uint16_t_u_u((p_4 , (p_4 | (!(safe_lshift_func_int16_t_s_s((**g_769), (((void*)0 != l_2005[0]) > (~(((safe_unary_minus_func_uint32_t_u((safe_sub_func_int16_t_s_s(((((safe_add_func_int64_t_s_s((safe_div_func_int16_t_s_s((safe_mul_func_uint64_t_u_u(((*g_268) ^= (safe_rshift_func_uint32_t_u_u(((**g_397) != (g_1793.f4 , p_4)), 15))), 18446744073709551615UL)), l_2054)), p_4)) | l_2055) <= p_4) < 0L), p_4)))) , (*g_268)) & p_4)))))))), p_4)) , p_4) || p_4), p_5)), p_4)) , 0x0BL) == 0x9DL))) != 0xA31FB828L)) , (void*)0) != (void*)0))
                    { /* block id: 934 */
                        uint32_t ****l_2063 = &g_1991;
                        l_2068[4] = (((*g_2026) , ((g_2056 , (((*g_2026) <= ((((**g_769) = (((((l_2058 = ((**g_267) = (l_2057 != (void*)0))) | (safe_lshift_func_uint8_t_u_u((+(((**l_1946) |= ((l_2062 == ((*l_2063) = &g_1992[2])) & (safe_mod_func_int16_t_s_s((safe_mod_func_uint32_t_u_u(((((0xCBL & (((*g_2026) && p_4) <= (**g_2025))) & p_4) < 5L) & p_4), p_4)), p_4)))) >= (-1L))), 4))) & 1UL) == 0L) , (**g_739))) >= g_872.f1) > (***g_396))) || 249UL)) , &g_1362)) == (void*)0);
                    }
                    else
                    { /* block id: 941 */
                        uint32_t l_2073 = 9UL;
                        int32_t *l_2074 = &l_23;
                        uint64_t *l_2077 = &l_1930;
                        struct S0 *l_2099 = &g_159;
                        (*g_1325) = ((((safe_lshift_func_uint32_t_u_s((g_1517.f1 = (~(l_2072 & (((*l_2074) = l_2073) , ((*l_2077) &= ((*g_268) = (safe_lshift_func_int16_t_s_u((*g_770), g_1510.f7)))))))), ((p_5 || ((safe_add_func_uint32_t_u_u((0x4EB6L ^ (((((p_4 >= ((((**g_2025) , (l_2068[4] , 0UL)) >= g_1508.f1) , 0x8635562E8DBA7CDBLL)) , p_4) | p_4) & p_5) | l_2073)), (***g_396))) != 0x68F2L)) || l_2080))) , 65535UL) , p_5) < (*g_770));
                        (*l_2099) = ((safe_add_func_uint16_t_u_u(((+(safe_mod_func_int32_t_s_s(((*g_1007) = (((p_5 != (-6L)) , (((g_2086 , ((((((((!(l_2088 == (~p_4))) < (****g_1949)) , &g_266) == ((safe_lshift_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s((+((p_4 > ((safe_lshift_func_int32_t_s_s(p_4, 8)) != l_2073)) ^ l_2068[4])), 2)) >= 4294967295UL), p_5)) , l_2097)) , p_5) ^ p_5) , 8UL) , 0x50L)) == l_2073) >= 8UL)) , p_4)), p_4))) != (**g_769)), g_242[7].f0)) , g_2098);
                    }
                    --l_2101;
                }
                l_2104 = &l_2068[1];
                for (g_130 = 26; (g_130 <= 43); g_130++)
                { /* block id: 955 */
                    int8_t **l_2126 = &l_1942;
                    int32_t l_2127 = 0L;
                    uint32_t ***l_2133 = &l_2019;
                    for (g_18 = 0; (g_18 == 18); ++g_18)
                    { /* block id: 958 */
                        int64_t *l_2111[5][2][10];
                        int32_t *l_2120 = &g_1024;
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                        {
                            for (j = 0; j < 2; j++)
                            {
                                for (k = 0; k < 10; k++)
                                    l_2111[i][j][k] = (void*)0;
                            }
                        }
                        l_24[1] = ((p_5 != (l_2111[0][0][7] != ((*g_717) , func_37((l_24[1] = ((safe_add_func_uint16_t_u_u((safe_add_func_int64_t_s_s((safe_add_func_int64_t_s_s((safe_mul_func_int8_t_s_s(p_4, (l_2120 == (void*)0))), (l_2121 && ((***g_742) ^ (((((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(0xDEL, p_5)), 65535UL)) , l_2126) != (void*)0) <= g_780.f1) & 255UL))))), l_2127)), p_5)) , (void*)0)))))) , &l_2058);
                    }
                    (*l_2104) ^= (((((--(**g_267)) , p_4) | 0x3CL) >= ((l_2130 == ((*l_2133) = g_2132)) && ((**l_1946) &= ((p_5 , (4294967288UL <= (safe_unary_minus_func_int16_t_s(((safe_mod_func_uint16_t_u_u((((safe_mod_func_int16_t_s_s(((safe_lshift_func_uint64_t_u_u((safe_div_func_uint16_t_u_u(((4294967295UL <= (safe_sub_func_int64_t_s_s(1L, (!(((l_2147 = l_2146) == &g_904[0][0][1]) ^ g_1506.f3))))) | 0x68L), p_4)), 4)) ^ 1L), l_2127)) , 0x54L) > 0x05L), p_5)) && g_2086.f9))))) , p_4)))) & l_2121);
                    if ((safe_div_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(((**g_267) = (safe_div_func_uint64_t_u_u(((safe_mod_func_int64_t_s_s((0x9F5DL != (safe_lshift_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((**g_1324), (safe_mul_func_uint32_t_u_u(((*l_2131) &= ((safe_div_func_uint16_t_u_u(((g_25[2] != (g_122[4][6][1] |= ((void*)0 == l_2164))) && (p_4 != (safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s(0x6836L, 9)), 0x45AD9A41L)))), (p_4 & 0xF12EBC4DABD73BBALL))) , l_2169)), g_624.f4)))), l_2058))), 0xA9759546844F8621LL)) | p_4), (*l_2104)))), p_4)), l_2127)))
                    { /* block id: 970 */
                        (*l_2104) = 0x2188C2A2L;
                        --l_2170;
                        (*g_2174) = g_2173;
                        if (l_2058)
                            break;
                    }
                    else
                    { /* block id: 975 */
                        uint32_t l_2188[6][10][4] = {{{0UL,0UL,1UL,0UL},{0x3AB30C8EL,1UL,0x750DB596L,0UL},{0xF2A60F1CL,0UL,1UL,0xF2A60F1CL},{0UL,0UL,0x2FA7069FL,0UL},{0UL,0x2FB95DA0L,1UL,0x6EDBB662L},{0xF2A60F1CL,0UL,0x750DB596L,0x750DB596L},{0x3AB30C8EL,0x3AB30C8EL,1UL,0UL},{0UL,0xF2A60F1CL,0UL,0UL},{0x2FB95DA0L,0UL,0x750DB596L,0UL},{0UL,0UL,5UL,0UL}},{{0UL,0xF2A60F1CL,0x2FB95DA0L,0UL},{1UL,0x3AB30C8EL,1UL,0x750DB596L},{1UL,0x750DB596L,0UL,0UL},{0x81CB12F6L,4294967295UL,0x2FA7069FL,0x750DB596L},{1UL,1UL,0x2FA7069FL,5UL},{0x81CB12F6L,1UL,0UL,0x2FB95DA0L},{1UL,0x411F6EFBL,0x411F6EFBL,1UL},{0x411F6EFBL,1UL,4294967295UL,1UL},{1UL,0x81CB12F6L,0UL,0UL},{1UL,1UL,1UL,0UL}},{{4294967295UL,0x81CB12F6L,0x2FA7069FL,1UL},{0x750DB596L,1UL,0x3AB30C8EL,1UL},{0x81CB12F6L,0x411F6EFBL,1UL,0x2FB95DA0L},{5UL,1UL,0x411F6EFBL,5UL},{1UL,1UL,0xE53794E8L,0x750DB596L},{1UL,4294967295UL,0x411F6EFBL,0UL},{5UL,0x750DB596L,1UL,1UL},{0x81CB12F6L,0x81CB12F6L,0x3AB30C8EL,0x750DB596L},{0x750DB596L,5UL,0x2FA7069FL,1UL},{4294967295UL,1UL,1UL,0x2FA7069FL}},{{1UL,1UL,0UL,1UL},{1UL,5UL,4294967295UL,0x750DB596L},{0x411F6EFBL,0x81CB12F6L,0x411F6EFBL,1UL},{1UL,0x750DB596L,0UL,0UL},{0x81CB12F6L,4294967295UL,0x2FA7069FL,0x750DB596L},{1UL,1UL,0x2FA7069FL,5UL},{0x81CB12F6L,1UL,0UL,0x2FB95DA0L},{1UL,0x411F6EFBL,0x411F6EFBL,1UL},{0x411F6EFBL,1UL,4294967295UL,1UL},{1UL,0x81CB12F6L,0UL,0UL}},{{1UL,1UL,1UL,0UL},{4294967295UL,0x81CB12F6L,0x2FA7069FL,1UL},{0x750DB596L,1UL,0x3AB30C8EL,1UL},{0x81CB12F6L,0x411F6EFBL,1UL,0x2FB95DA0L},{5UL,1UL,0x411F6EFBL,5UL},{1UL,1UL,0xE53794E8L,0x750DB596L},{1UL,4294967295UL,0x411F6EFBL,0UL},{5UL,0x750DB596L,1UL,1UL},{0x81CB12F6L,0x81CB12F6L,0x3AB30C8EL,0x750DB596L},{0x750DB596L,5UL,0x2FA7069FL,1UL}},{{4294967295UL,1UL,1UL,0x2FA7069FL},{1UL,1UL,0UL,1UL},{1UL,5UL,4294967295UL,0x750DB596L},{0x411F6EFBL,0x81CB12F6L,0x411F6EFBL,1UL},{1UL,0x750DB596L,0UL,0UL},{0x81CB12F6L,4294967295UL,0x2FA7069FL,0x750DB596L},{1UL,1UL,0x2FA7069FL,5UL},{0x81CB12F6L,1UL,0UL,0x2FB95DA0L},{1UL,0x411F6EFBL,0x411F6EFBL,1UL},{0x411F6EFBL,1UL,4294967295UL,0UL}}};
                        int32_t l_2189 = 0L;
                        int i, j, k;
                        l_2189 &= ((((((safe_lshift_func_int32_t_s_u((l_2127 = ((***g_742) == 0xF6B4L)), p_4)) < (((safe_mod_func_uint32_t_u_u((l_2179 >= p_4), (safe_rshift_func_int16_t_s_s((safe_div_func_int32_t_s_s((**g_397), 0x1445092BL)), (safe_lshift_func_int64_t_s_s((safe_lshift_func_uint64_t_u_s(((254UL && g_185.f9) < (*g_2026)), 50)), 8)))))) != l_2188[5][0][0]) < p_5)) , (*l_2104)) == (-3L)) , p_5) == p_5);
                    }
                    return p_4;
                }
                l_2190[0][2][0]--;
            }
            else
            { /* block id: 982 */
                int32_t l_2216 = 0xB1FD68FDL;
                int16_t *l_2219 = &l_1976[0];
                int32_t l_2222 = 0xE7600F69L;
                int64_t l_2244 = 0x5EEB36802E420340LL;
                int32_t l_2249 = 7L;
                int32_t l_2250 = 0xD535E3D7L;
                int32_t l_2251 = 3L;
                int32_t l_2252 = 5L;
                int8_t l_2256[2][2];
                int32_t *** const l_2262 = (void*)0;
                int i, j;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2256[i][j] = 0xEBL;
                }
                if ((p_4 < (safe_mul_func_uint64_t_u_u((((safe_div_func_uint8_t_u_u((safe_rshift_func_int32_t_s_s(((safe_add_func_uint8_t_u_u(((***l_1945) &= (l_2201 == (*l_2146))), (safe_mul_func_uint64_t_u_u(((((((((1L < (safe_add_func_int64_t_s_s((((((~(l_2222 = (p_5 < ((((safe_lshift_func_int16_t_s_s(((*l_2219) ^= (65535UL != ((g_2209 , (safe_lshift_func_int64_t_s_s(((*l_2104) = (safe_mul_func_int8_t_s_s(((((g_115.f0 > (((((((safe_div_func_int64_t_s_s(((*g_717) = 0x7C4BEDB2D02CD699LL), l_2216)) & l_2217[5]) == (*l_2104)) >= p_5) || p_4) >= (*g_770)) , 0xD2D0L)) , &p_5) != &p_5) || 0x1E884A586B22E814LL), l_2218))), 36))) && 18446744073709551615UL))), l_2216)) , p_4) != g_2220) == l_2221)))) ^ (*g_770)) , (*g_739)) != (*g_1059)) && 9L), 0xC09EE8C583EF6DBFLL))) , (void*)0) != l_2223) <= (**g_267)) <= l_2224) <= p_5) || 0xB4L) || 255UL), (*g_268))))) ^ p_5), 0)), (*g_2026))) ^ l_2225) && p_5), 0x0FFD1B05F9BC5C2ELL))))
                { /* block id: 988 */
                    struct S0 *l_2226 = &g_2098;
                    int64_t *****l_2229[5][4] = {{&g_1688[1],&g_1688[1],&g_1688[1],&g_1688[1]},{&g_1688[1],&g_1688[1],&g_1688[1],&g_1688[1]},{&g_1688[1],&g_1688[1],&g_1688[1],&g_1688[1]},{&g_1688[1],&g_1688[1],&g_1688[1],&g_1688[1]},{&g_1688[1],&g_1688[1],&g_1688[1],&g_1688[1]}};
                    uint8_t *****l_2242 = (void*)0;
                    int8_t *l_2243[8] = {&g_65[8],&g_65[8],&g_65[8],&g_65[8],&g_65[8],&g_65[8],&g_65[8],&g_65[8]};
                    int i, j;
                    (*g_1075) = l_2226;
                    (**g_1324) = (((l_2245 ^= ((((*l_1942) = (safe_div_func_uint32_t_u_u(((void*)0 == l_2229[2][1]), (***g_396)))) < ((safe_rshift_func_int32_t_s_s((safe_sub_func_int32_t_s_s((g_533.f5 , (((safe_mul_func_int32_t_s_s(7L, 0UL)) , l_2236[0][3]) || ((p_4 & ((l_2244 ^= ((**l_2223) = ((((g_65[8] |= (l_2222 ^= (safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s((((l_2242 = l_2241) == &l_2005[4]) < p_5), p_5)), 1UL)))) == l_2216) && 0x1EFDEB9E8036F443LL) & (*l_2104)))) > 2UL)) || 1UL))), l_2216)), 21)) && p_4)) <= 0x79L)) & p_4) || 1L);
                }
                else
                { /* block id: 998 */
                    uint32_t l_2253 = 4294967295UL;
                    for (g_86 = 0; (g_86 == 52); g_86 = safe_add_func_int64_t_s_s(g_86, 3))
                    { /* block id: 1001 */
                        (****g_1322) &= (~0x22575DABE2094F7FLL);
                        return p_5;
                    }
                    ++l_2253;
                    g_2257++;
                }
                for (l_2250 = (-30); (l_2250 > 21); l_2250++)
                { /* block id: 1010 */
                    int32_t ****l_2263[10][10] = {{(void*)0,&l_1956[7][0],&g_1230[1][0],&l_1956[7][0],(void*)0,&l_1956[5][5],&g_1230[1][1],&g_1230[1][1],&g_1230[1][1],&g_1230[1][1]},{(void*)0,&l_1956[5][5],&l_1956[7][0],&l_1956[7][0],&l_1956[5][5],(void*)0,(void*)0,&g_1230[1][1],(void*)0,&g_1230[1][1]},{&l_1956[7][0],&l_1956[7][0],(void*)0,&g_1230[1][1],(void*)0,&l_1956[7][0],&l_1956[7][0],(void*)0,&g_1230[1][1],&g_1230[1][1]},{&l_1956[7][0],&g_1230[1][1],(void*)0,&g_1230[1][0],&g_1230[1][0],(void*)0,&g_1230[1][1],&l_1956[7][0],&l_1956[5][5],(void*)0},{(void*)0,&g_1230[1][1],&l_1956[7][0],&l_1956[5][5],(void*)0,&l_1956[5][5],&l_1956[7][0],&g_1230[1][1],(void*)0,&g_1230[1][0]},{(void*)0,&l_1956[7][0],&l_1956[7][0],(void*)0,&g_1230[1][1],&g_1230[1][1],(void*)0,&l_1956[7][0],&l_1956[7][0],(void*)0},{&l_1956[7][0],&l_1956[5][5],(void*)0,(void*)0,&g_1230[1][1],(void*)0,&g_1230[1][1],(void*)0,(void*)0,&l_1956[5][5]},{&g_1230[1][0],&l_1956[7][0],(void*)0,&l_1956[5][5],&g_1230[1][1],&g_1230[1][1],&g_1230[1][1],&g_1230[1][1],&l_1956[5][5],(void*)0},{&g_1230[1][1],&g_1230[1][1],&l_1956[7][0],&g_1230[1][0],&g_1230[1][1],&g_1230[1][1],(void*)0,&g_1230[1][1],&g_1230[1][1],&g_1230[1][0]},{&g_1230[1][0],&l_1956[2][3],&g_1230[1][0],&g_1230[1][1],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                    int32_t ****l_2264[7] = {&g_1230[1][0],&g_1230[1][4],&g_1230[1][0],&g_1230[1][0],&g_1230[1][4],&g_1230[1][0],&g_1230[1][0]};
                    int32_t ****l_2265[4] = {&l_1956[0][1],&l_1956[0][1],&l_1956[0][1],&l_1956[0][1]};
                    uint16_t *l_2284[8] = {&g_15,&g_15,&g_15,&g_15,&g_15,&g_15,&g_15,&g_15};
                    const int32_t **l_2294 = &g_687;
                    int i, j;
                    l_2267 = l_2262;
                    l_2252 &= ((safe_div_func_uint16_t_u_u((g_319 = (safe_add_func_int64_t_s_s(((0x54AE5F48L && (*g_1007)) & ((safe_rshift_func_int64_t_s_s((safe_lshift_func_int32_t_s_u(((void*)0 == &g_986), 15)), (~p_4))) <= p_5)), ((g_2290 = (((((*g_268) = (safe_add_func_uint8_t_u_u(((***l_1945) ^= ((safe_add_func_uint64_t_u_u((((+g_185.f4) , (((safe_mul_func_uint16_t_u_u((g_9++), (p_4 < (safe_mul_func_int16_t_s_s(((**g_769) ^= 0xD7CDL), p_4))))) , p_5) > p_4)) , p_5), p_4)) | l_2289)), (*l_2104)))) | p_5) > p_5) && p_4)) < 0x331A4846L)))), 2L)) ^ p_4);
                    (*l_2104) &= (safe_rshift_func_int32_t_s_s(0xDA9AF18FL, ((g_185 , &g_1991) == l_2293)));
                    (*l_2294) = (*g_1476);
                }
                if (p_5)
                    continue;
                l_2298 &= (p_5 , (safe_unary_minus_func_uint32_t_u((0x89E8L >= ((5UL <= (((safe_mul_func_int8_t_s_s(p_5, p_5)) | (g_159.f1 <= (0x4364L || (*l_2104)))) == (-1L))) == (**g_931))))));
            }
        }
        for (g_89 = 10; (g_89 >= 36); g_89++)
        { /* block id: 1028 */
            uint64_t l_2308 = 4UL;
            uint32_t ****l_2309 = &g_1991;
            int32_t l_2332[1];
            int64_t *l_2333 = &g_18;
            int i;
            for (i = 0; i < 1; i++)
                l_2332[i] = 1L;
            if ((((safe_mul_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u(g_1629.f3, (-1L))) , (safe_sub_func_uint32_t_u_u(((safe_unary_minus_func_int16_t_s(((l_2308 | (((((*l_2309) = &g_1992[3]) == l_2146) , (safe_mul_func_uint32_t_u_u((safe_mod_func_int16_t_s_s((safe_div_func_int64_t_s_s((safe_div_func_int64_t_s_s(((((**g_1059) | (safe_add_func_int16_t_s_s(((safe_lshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_u((l_2308 || ((safe_mul_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u((**g_2025), 7)), ((safe_div_func_uint32_t_u_u((((*l_2333) = (safe_add_func_int64_t_s_s((p_4 = ((**l_2223) = ((l_2332[0] ^= p_4) >= p_4))), p_5))) > 0xD9664106C4D9F563LL), p_5)) ^ 0UL))) < l_2308)), 4)), (****g_1949))) <= g_149.f2), (*g_770)))) , l_2334) == l_2334), p_5)), 0xF0999442CDBCFA42LL)), 0xC098L)), 2UL))) , g_625.f2)) >= 7UL))) == 0x9AL), 0xFBFAB440L))), 0x5D0DL)) , l_2308) && p_4))
            { /* block id: 1034 */
                int32_t *l_2335 = &l_1997[1][1];
                int32_t l_2337[9][1][6] = {{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-2L),0x6B150798L,(-2L),(-2L),0x6B150798L}},{{(-2L),(-7L),(-2L),(-7L),(-7L),(-2L)}},{{(-7L),(-7L),(-2L),(-7L),(-7L),(-2L)}}};
                int i, j, k;
                l_2335 = l_2335;
                if ((*l_2335))
                    break;
                g_2338--;
            }
            else
            { /* block id: 1038 */
                (**g_1324) = ((((((safe_mul_func_int64_t_s_s(p_4, (safe_mod_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((safe_mod_func_int8_t_s_s(((((safe_lshift_func_uint8_t_u_u(p_4, (safe_div_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((safe_sub_func_uint16_t_u_u(g_1519.f0, 0xEC3EL)), (p_4 & (65535UL >= ((safe_rshift_func_int16_t_s_u(((**g_267) & (safe_div_func_int8_t_s_s((safe_mul_func_int8_t_s_s(1L, 255UL)), p_4))), p_4)) != 1UL))))), p_5)))) | 0x0A04DD40L) , l_2308) < (*g_268)), l_2308)), (*g_268))), p_5)))) , (-6L)) || 0x89L) <= p_5) == 0x52D6AA68L) < (**g_2025));
            }
            --l_2365;
        }
    }
    else
    { /* block id: 1043 */
        int64_t l_2368 = 0xCE346E0961FB28E4LL;
        int64_t l_2387[5][6] = {{0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L,0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L},{0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L,0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L},{0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L,0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L},{0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L,0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L},{0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L,0xEEBEE58F8B51C965LL,0xEEBEE58F8B51C965LL,9L}};
        uint32_t *l_2388 = &g_86;
        int32_t l_2389 = 0L;
        int64_t *l_2408 = (void*)0;
        int64_t **l_2407 = &l_2408;
        int i, j;
        l_2389 &= (((*l_2388) |= ((((g_926.f2 <= ((((p_4 > (p_4 < ((**l_1628) = ((((l_2368 , &g_1949) == (void*)0) >= ((safe_add_func_uint8_t_u_u(((**g_2025) && (safe_mul_func_int32_t_s_s(((((l_2384 = (l_2373 = (((void*)0 == l_2146) , (void*)0))) != (void*)0) != l_2387[2][3]) , p_4), l_2387[2][3]))), 0xBFL)) && (-1L))) | 1L)))) && 0UL) , l_2368) , (-3L))) == 0x95L) > l_2387[2][4]) == p_4)) && l_2387[2][3]);
        if (g_1307.f2)
            goto lbl_2395;
lbl_2395:
        for (l_2101 = (-9); (l_2101 >= 20); l_2101++)
        { /* block id: 1051 */
            uint32_t ***l_2394 = &l_2392;
            (*l_2394) = l_2392;
        }
        l_2389 = l_2368;
        (**g_1324) = (((l_2389 = (~(p_5 <= ((***l_1945) = ((**g_2025) ^ (safe_rshift_func_int8_t_s_u(((*g_1469) == (safe_rshift_func_uint8_t_u_u(p_5, 2))), 1))))))) >= ((safe_mul_func_int16_t_s_s((safe_mul_func_uint32_t_u_u((255UL < l_2387[2][3]), (safe_mod_func_uint16_t_u_u((((*l_1628) = &p_4) == ((*l_2407) = &p_4)), l_2387[2][3])))), l_2387[2][3])) , p_5)) & p_4);
    }
    return (*g_2026);
}


/* ------------------------------------------ */
/* 
 * reads : g_1629 g_185.f6 g_676.f0 g_958.f1 g_717 g_63 g_179 g_906 g_769 g_770 g_121 g_267 g_268 g_83 g_396 g_397 g_398 g_40 g_1322 g_1323 g_1324 g_1325 g_9 g_270 g_1506.f0 g_978 g_911 g_1007 g_1326 g_344 g_122 g_1678 g_39 g_1468 g_1469 g_1470 g_1553 g_266 g_130 g_149.f0 g_125.f4 g_554 g_551 g_1706 g_1729 g_297 g_298 g_1758 g_865 g_858 g_18 g_1616.f1 g_149.f2 g_1793 g_1794 g_1506.f9 g_1059 g_926.f1 g_89 g_626.f0 g_932 g_933 g_534.f5 g_1361 g_1362 g_1363 g_1276 g_65 g_780.f1 g_676.f6 g_1895 g_931 g_1420 g_1331
 * writes: g_986 g_179 g_83 g_1326 g_978 g_9 g_63 g_40 g_344 g_119 g_39 g_1687 g_1553 g_18 g_551 g_125.f1 g_687 g_858 g_65 g_1331 g_121 g_1420
 */
static int32_t  func_19(int64_t * p_20)
{ /* block id: 725 */
    struct S0 *l_1630[1][9][6] = {{{&g_957[7],(void*)0,&g_957[3],&g_957[0],&g_1519,&g_957[4]},{&g_1519,&g_957[7],&g_957[3],&g_1517,&g_957[3],&g_957[7]},{&g_1517,&g_1517,&g_957[4],&g_872,&g_957[3],&g_1616},{&g_872,&g_957[3],&g_1616,&g_957[3],(void*)0,&g_1517},{&g_159,&g_957[3],&g_1517,&g_1517,&g_957[3],&g_159},{&g_957[3],&g_1517,&g_957[7],(void*)0,&g_957[3],&g_957[0]},{&g_1517,&g_957[7],(void*)0,&g_1519,&g_1519,&g_1616},{&g_1517,(void*)0,&g_1519,(void*)0,&g_1517,&g_957[3]},{&g_957[3],&g_957[3],&g_1519,&g_1517,&g_1616,&g_1517}}};
    struct S0 **l_1631 = &g_986;
    int32_t l_1634 = (-1L);
    struct S2 * const *l_1655 = (void*)0;
    struct S2 * const **l_1654 = &l_1655;
    uint32_t *l_1675 = &g_1553;
    int32_t l_1701 = 7L;
    int32_t l_1713 = 0x768561DBL;
    int8_t *l_1751 = &g_65[8];
    const uint64_t l_1756 = 4UL;
    int64_t ***l_1768 = &g_716;
    int32_t l_1777 = 0x3CCC0998L;
    struct S2 *l_1781 = &g_1506;
    uint8_t *l_1911 = &g_978;
    uint8_t **l_1910 = &l_1911;
    uint64_t ** const l_1919 = &g_268;
    int32_t *l_1922 = &g_40;
    int i, j, k;
    if ((((g_1629 , l_1630[0][5][3]) != ((*l_1631) = l_1630[0][5][1])) < (0xE1963712ACB17D9DLL || (safe_rshift_func_int8_t_s_s((l_1634 != (safe_lshift_func_int64_t_s_s((((safe_mod_func_int8_t_s_s(((safe_mod_func_int32_t_s_s((safe_lshift_func_uint16_t_u_s(l_1634, (((safe_sub_func_uint32_t_u_u((g_185.f6 || 0x346F9776L), 0xA6FBF31AL)) < g_676.f0) >= g_958.f1))), l_1634)) , 0xCAL), l_1634)) , l_1634) , (*g_717)), 34))), 3)))))
    { /* block id: 727 */
        uint16_t l_1649 = 0xD724L;
        int32_t l_1657 = (-1L);
        const uint16_t l_1658 = 1UL;
        int16_t l_1673[8][4] = {{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L},{0xC883L,0L,0xC883L,0L}};
        uint32_t *l_1676[1][1][2];
        int8_t l_1691 = 3L;
        int32_t l_1694 = 0xF3FFD7F8L;
        int32_t l_1695[4][4][7] = {{{(-4L),0xF6F21841L,0L,0xF9B701B9L,1L,6L,1L},{0x58BEA87CL,(-3L),(-3L),0x58BEA87CL,0x17E46B78L,0xFAF72494L,(-1L)},{(-4L),0xF9B701B9L,0x137EA54FL,(-4L),0xD2CA1190L,0xD2CA1190L,(-4L)},{(-1L),(-1L),(-1L),0xFAF72494L,0x06E29823L,0x59B3A747L,(-1L)}},{{6L,7L,1L,0x137EA54FL,(-1L),0x137EA54FL,1L},{0x06E29823L,0x06E29823L,0x17E46B78L,0L,(-2L),0x59B3A747L,(-1L)},{0x1B3BDE39L,0L,0xD2CA1190L,0xF6F21841L,0xF6F21841L,0xD2CA1190L,0L},{(-3L),(-3L),0x06E29823L,(-1L),(-2L),0xFAF72494L,0L}},{{0xD2CA1190L,6L,(-1L),0L,(-1L),6L,0xD2CA1190L},{0xFAF72494L,(-3L),7L,0x59B3A747L,0x17E46B78L,(-2L),0x58BEA87CL},{0x137EA54FL,(-4L),0xD2CA1190L,0xD2CA1190L,(-4L),0x137EA54FL,0xF9B701B9L},{0L,(-3L),7L,0xFAF72494L,(-1L),0x17E46B78L,0x17E46B78L}},{{0xF6F21841L,6L,(-4L),6L,0xF6F21841L,2L,0x1B3BDE39L},{(-1L),(-3L),0x17E46B78L,(-3L),0x59B3A747L,0L,0x59B3A747L},{0L,(-4L),(-4L),0L,6L,7L,1L},{(-1L),(-3L),(-1L),0x06E29823L,0x58BEA87CL,0x58BEA87CL,0x06E29823L}}};
        int32_t l_1710 = 0x4A63D2F7L;
        uint8_t *l_1757 = &g_978;
        int32_t ****l_1770 = (void*)0;
        int32_t *****l_1769 = &l_1770;
        uint8_t l_1778 = 8UL;
        struct S1 * const *l_1819 = &g_384;
        uint8_t ****l_1820 = (void*)0;
        uint8_t **l_1824 = &l_1757;
        uint8_t ***l_1823 = &l_1824;
        uint8_t ****l_1822 = &l_1823;
        uint32_t l_1923[8][4] = {{0x5318BE7EL,0x822E769DL,0xC6F1CC89L,0x822E769DL},{0x5318BE7EL,4UL,4294967295UL,0x822E769DL},{4294967295UL,0x822E769DL,4294967295UL,4UL},{0x5318BE7EL,0x822E769DL,0xC6F1CC89L,0x822E769DL},{0x5318BE7EL,4UL,4294967295UL,0x822E769DL},{4294967295UL,0x822E769DL,4294967295UL,4UL},{0x5318BE7EL,0x822E769DL,0xC6F1CC89L,0x822E769DL},{0x5318BE7EL,4UL,4294967295UL,0x822E769DL}};
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 2; k++)
                    l_1676[i][j][k] = &g_89;
            }
        }
lbl_1826:
        for (g_179 = 0; (g_179 <= 7); g_179 += 1)
        { /* block id: 730 */
            int32_t l_1653 = 1L;
            uint64_t *l_1656[2][3] = {{&g_130,&g_130,&g_130},{(void*)0,(void*)0,(void*)0}};
            int64_t ****l_1686 = &g_715[7];
            int64_t *****l_1685[7][8] = {{&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,&l_1686,&l_1686,(void*)0},{&l_1686,&l_1686,&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,&l_1686},{&l_1686,(void*)0,&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,(void*)0},{&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,&l_1686,&l_1686,(void*)0},{&l_1686,&l_1686,&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,&l_1686},{&l_1686,(void*)0,&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,(void*)0},{&l_1686,(void*)0,&l_1686,&l_1686,&l_1686,&l_1686,&l_1686,(void*)0}};
            uint32_t l_1697 = 1UL;
            int32_t l_1712 = (-9L);
            const int32_t *l_1718 = &g_40;
            int i, j;
            (****g_1322) = (l_1634 = ((g_906[g_179] , (safe_mod_func_uint64_t_u_u((l_1657 = (((safe_mul_func_int32_t_s_s((g_906[g_179] | ((**g_769) & (l_1649 == ((((l_1634 , (safe_unary_minus_func_uint8_t_u(0xCFL))) > (((0UL > ((**g_267)--)) >= (***g_396)) < (l_1653 , 0x0B300DDDL))) , l_1654) == (void*)0)))), 1L)) , g_906[g_179]) , 0x5B4EF141C6064355LL)), l_1658))) , l_1634));
            if (l_1634)
                break;
            for (g_978 = 0; (g_978 <= 1); g_978 += 1)
            { /* block id: 738 */
                uint16_t *l_1659 = &g_9;
                int32_t *l_1670[3][1];
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1670[i][j] = &l_1634;
                }
                (*g_1007) = ((++(*l_1659)) < (((safe_add_func_int32_t_s_s(((((void*)0 == g_270) >= g_1506.f0) , (safe_lshift_func_uint32_t_u_u((((safe_rshift_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((g_911[g_978] <= ((*g_717) ^= ((l_1657 = 0x7FAD1805L) >= l_1634))), (safe_mod_func_int16_t_s_s(0xD3ADL, 0xBEC2L)))), (((&g_1362 == &g_1362) , (void*)0) == (void*)0))) != l_1634) , l_1653), 20))), l_1649)) == l_1673[3][0]) & l_1649));
                if (l_1634)
                    continue;
                for (g_344 = 0; (g_344 <= 0); g_344 += 1)
                { /* block id: 746 */
                    int32_t *l_1677 = &l_1634;
                    int64_t *****l_1690 = &l_1686;
                    int32_t l_1693[6] = {6L,6L,6L,6L,6L,6L};
                    int i, j, k;
                    (*g_1325) |= (safe_unary_minus_func_uint16_t_u((l_1675 != l_1676[0][0][1])));
                    if (g_122[(g_978 + 2)][(g_344 + 6)][g_344])
                        continue;
                    for (g_119 = 0; (g_119 <= 6); g_119 += 1)
                    { /* block id: 751 */
                        int32_t **l_1679 = &l_1670[0][0];
                        int64_t *****l_1689 = &l_1686;
                        (*g_1678) = l_1677;
                        (*l_1679) = ((*l_1677) , (*g_1678));
                        l_1657 ^= (((safe_mul_func_int64_t_s_s((safe_lshift_func_uint32_t_u_s((~(l_1634 , (((*g_1007) &= 0xA0ABA33DL) ^ (((0xD9A00573E00FBEEELL | (-1L)) || ((((g_1687 = l_1685[6][5]) != (l_1690 = l_1689)) , ((*g_717) ^ ((**g_267) = 0xE06121335384AF8CLL))) ^ (0x99L < 0x62L))) != l_1658)))), 8)), (*p_20))) > (**l_1679)) || (**l_1679));
                    }
                    if (((**g_1468) > ((*l_1675) |= (*l_1677))))
                    { /* block id: 761 */
                        if ((*l_1677))
                            break;
                    }
                    else
                    { /* block id: 763 */
                        int32_t l_1692 = 0xB4840E81L;
                        int32_t l_1696 = 0xEBC134F8L;
                        int i, j;
                        ++l_1697;
                    }
                }
            }
            for (g_9 = 0; (g_9 <= 0); g_9 += 1)
            { /* block id: 770 */
                int32_t l_1700 = (-1L);
                int32_t *l_1708 = &l_1657;
                int32_t *l_1709[4] = {&l_1657,&l_1657,&l_1657,&l_1657};
                int64_t l_1714 = 1L;
                uint8_t l_1715[10] = {0xD2L,9UL,0xD2L,9UL,0xD2L,9UL,0xD2L,9UL,0xD2L,9UL};
                int i;
                for (g_18 = 0; (g_18 <= 5); g_18 += 1)
                { /* block id: 773 */
                    int32_t l_1702 = 0L;
                    int32_t l_1705 = 0xA4014AEEL;
                    int32_t *l_1707 = &l_1653;
                    int i, j, k;
                    (**g_1324) = ((((l_1700 >= 0x3CL) & ((l_1634 &= ((***g_266) = l_1658)) != (2L > ((l_1702 &= (g_130 < (l_1701 , (g_149.f0 , 4294967295UL)))) | (safe_sub_func_int16_t_s_s(l_1705, l_1700)))))) <= g_125.f4) | 0UL);
                    (*g_1706) = (*g_554);
                    if (l_1691)
                        break;
                    (****g_1322) = ((*l_1707) |= l_1691);
                }
                l_1715[5]--;
                (**g_1324) = ((*g_268) != 18446744073709551607UL);
                for (g_63 = 0; (g_63 >= 0); g_63 -= 1)
                { /* block id: 787 */
                    uint8_t l_1719 = 9UL;
                    l_1718 = (void*)0;
                    for (l_1653 = 0; (l_1653 >= 0); l_1653 -= 1)
                    { /* block id: 791 */
                        ++l_1719;
                    }
                    if (l_1701)
                        break;
                    for (l_1653 = 0; (l_1653 <= 5); l_1653 += 1)
                    { /* block id: 797 */
                        uint32_t l_1722 = 1UL;
                        int32_t l_1725 = 9L;
                        uint32_t l_1726 = 5UL;
                        l_1722++;
                        l_1726--;
                        (*g_1729) = l_1708;
                    }
                }
            }
        }
lbl_1851:
        (*g_1758) = ((((*g_297) , (l_1695[0][1][2] = ((safe_mul_func_uint64_t_u_u(((!((safe_rshift_func_int32_t_s_u(l_1694, (g_125.f1 = (safe_add_func_uint64_t_u_u(((*g_268) &= ((safe_sub_func_uint32_t_u_u(1UL, ((safe_lshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u(((((*l_1757) &= (safe_sub_func_uint32_t_u_u((safe_add_func_uint32_t_u_u((--(*l_1675)), ((safe_lshift_func_int8_t_s_u(((void*)0 != l_1751), (safe_lshift_func_uint8_t_u_u(((((*p_20) = (safe_rshift_func_uint32_t_u_u(l_1756, l_1673[0][2]))) , (*p_20)) != (l_1695[0][3][2] | l_1634)), 2)))) > 0x93951375L))), 0xF0A5B249L))) ^ l_1694) , l_1713), 0L)), l_1657)) , l_1673[3][0]))) && (-1L))), l_1694))))) , 0x02C4FCED356F27E0LL)) & l_1691), l_1634)) , (*p_20)))) >= l_1710) , &l_1634);
        for (g_18 = 5; (g_18 >= 1); g_18 -= 1)
        { /* block id: 814 */
            const int16_t l_1766[7][10][3] = {{{0x766DL,0x69F0L,(-1L)},{(-1L),0x332FL,0x3C76L},{1L,0x69F0L,1L},{(-1L),(-1L),0x3C76L},{0x766DL,0x69F0L,(-1L)},{(-1L),0x332FL,0x3C76L},{1L,0x69F0L,1L},{(-1L),(-1L),0x3C76L},{0x766DL,0x69F0L,(-1L)},{(-1L),0x13B2L,0x332FL}},{{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL}},{{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL}},{{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL}},{{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL}},{{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL}},{{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL},{0L,0x7C9CL,1L},{0L,0L,0x332FL},{0x5816L,0x7C9CL,0x766DL},{0L,0x13B2L,0x332FL}}};
            int32_t l_1774 = (-1L);
            int32_t l_1776 = 0L;
            int64_t ****l_1797 = (void*)0;
            struct S2 ***l_1807 = (void*)0;
            uint32_t *l_1858 = &g_89;
            int8_t **l_1873 = &l_1751;
            int32_t l_1881[6][8][3] = {{{0x829FC7FAL,0L,0x3209CDE0L},{1L,0xCA6ABCE8L,0x164B9299L},{0L,0xFB248A18L,9L},{(-6L),0x07D60D11L,0x37E41B8BL},{0xBB164CB8L,(-1L),0x81AEEC97L},{0x57DFA0F4L,(-1L),1L},{(-1L),(-1L),0L},{0x8AA34682L,(-1L),0xFB248A18L}},{{0x2203A28BL,0x07D60D11L,1L},{0xABE74A42L,0x07EF0612L,1L},{(-1L),0x2203A28BL,1L},{0xC710B0ACL,(-1L),0xFB248A18L},{(-1L),0xC710B0ACL,0L},{9L,0xABE74A42L,1L},{9L,0x3DA0ACF1L,0x81AEEC97L},{(-1L),(-1L),0x37E41B8BL}},{{0xC710B0ACL,0x1E8EA3F6L,0x829FC7FAL},{(-1L),0xEE216E9BL,0xEBAA32B3L},{0xABE74A42L,0x1E8EA3F6L,0xCA6ABCE8L},{0x2203A28BL,(-1L),1L},{0x8AA34682L,0x3DA0ACF1L,1L},{(-1L),0xABE74A42L,1L},{0x57DFA0F4L,0xC710B0ACL,1L},{0xBB164CB8L,(-1L),0xCA6ABCE8L}},{{(-6L),0x2203A28BL,0xEBAA32B3L},{(-1L),0x07EF0612L,0x829FC7FAL},{(-6L),0x07D60D11L,0x37E41B8BL},{0xBB164CB8L,(-1L),0x81AEEC97L},{0x57DFA0F4L,(-1L),1L},{(-1L),(-1L),0L},{0x8AA34682L,(-1L),0xFB248A18L},{0x2203A28BL,0x07D60D11L,1L}},{{0xABE74A42L,0x07EF0612L,1L},{(-1L),0x2203A28BL,1L},{0xC710B0ACL,(-1L),0xFB248A18L},{(-1L),0xC710B0ACL,0L},{9L,0xABE74A42L,1L},{9L,0x3DA0ACF1L,0x81AEEC97L},{(-1L),(-1L),0x37E41B8BL},{0xC710B0ACL,0x1E8EA3F6L,0x829FC7FAL}},{{(-1L),0xEE216E9BL,0xEBAA32B3L},{0xABE74A42L,0x1E8EA3F6L,0xCA6ABCE8L},{0x2203A28BL,(-1L),1L},{0x8AA34682L,0x3DA0ACF1L,1L},{(-1L),0xABE74A42L,1L},{0x57DFA0F4L,0xC710B0ACL,1L},{0xBB164CB8L,(-1L),0xCA6ABCE8L},{(-6L),0x2203A28BL,0xEBAA32B3L}}};
            uint32_t l_1889 = 0x0BFB30C4L;
            int i, j, k;
            if (l_1694)
            { /* block id: 815 */
                (*g_865) = &l_1634;
                if (l_1634)
                    break;
            }
            else
            { /* block id: 818 */
                uint32_t l_1761 = 0xA772BD0AL;
                int32_t *l_1773 = &l_1695[2][0][0];
                int32_t *l_1775[4][2] = {{&g_40,&l_1657},{&g_40,&l_1657},{&g_40,&l_1657},{&g_40,&l_1657}};
                struct S2 *l_1784 = (void*)0;
                int i, j;
                for (g_344 = 0; (g_344 <= 2); g_344 += 1)
                { /* block id: 821 */
                    uint32_t **l_1767 = (void*)0;
                    int32_t l_1772[9] = {0xB546158DL,0xEE76F502L,0xB546158DL,0xEE76F502L,0xB546158DL,0xEE76F502L,0xB546158DL,0xEE76F502L,0xB546158DL};
                    int i, j;
                    g_858[g_344][(g_344 + 3)] ^= (-1L);
                    for (g_1553 = 0; (g_1553 <= 2); g_1553 += 1)
                    { /* block id: 825 */
                        int64_t ***l_1771 = &g_716;
                        int i, j;
                        if (g_858[g_1553][g_18])
                            break;
                        (*g_39) &= (((safe_mod_func_int16_t_s_s(l_1761, 1UL)) > (-4L)) ^ (3L == (safe_rshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(l_1766[2][1][0], ((((l_1767 == l_1767) , (l_1768 == (((((((void*)0 != l_1769) , 0x049249333E2A0D01LL) , 0x4EFA35B839B6D325LL) > 1L) > (***g_266)) , l_1771))) >= g_1616.f1) <= l_1772[2]))), g_149.f2))));
                        return l_1756;
                    }
                }
                --l_1778;
                l_1784 = l_1781;
            }
            if ((((l_1701 ^ (l_1713 ^= l_1766[2][0][2])) <= (((safe_sub_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(l_1766[4][6][0], 1)), (safe_sub_func_uint16_t_u_u((0xF9E8BFA0L >= (((*g_717) == (((l_1766[2][1][0] || ((g_1793 , (g_1794 , (safe_lshift_func_int16_t_s_s(l_1766[5][6][2], 6)))) > g_1506.f9)) , l_1797) != l_1797)) <= l_1756)), (**g_1059))))) , 0xA349A2DDL) > g_926.f1)) == g_89))
            { /* block id: 835 */
                uint64_t l_1810 = 0UL;
                const int32_t **l_1850 = &g_687;
                const int32_t ***l_1849 = &l_1850;
                int16_t *l_1864 = &l_1673[3][0];
                int32_t l_1885 = 0xB1C4912AL;
                if ((((l_1756 > ((~l_1766[4][7][2]) <= ((safe_lshift_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(((0x90L | ((safe_mod_func_int8_t_s_s(((-8L) | ((void*)0 != l_1807)), (safe_lshift_func_int8_t_s_s(l_1810, (l_1713 , ((*l_1751) = g_626.f0)))))) != l_1756)) && 0x60L), (*g_932))), 10)) , l_1774))) && (**g_769)) || (*p_20)))
                { /* block id: 837 */
                    int32_t l_1827 = 0x03C2894BL;
                    int32_t l_1830[4][4][4] = {{{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L}},{{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L}},{{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L}},{{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L}}};
                    int i, j, k;
                    for (g_1331 = 0; (g_1331 <= 0); g_1331 += 1)
                    { /* block id: 840 */
                        uint8_t *****l_1821[8][3] = {{&l_1820,&l_1820,&l_1820},{&l_1820,&l_1820,&l_1820},{&l_1820,&l_1820,&l_1820},{&l_1820,&l_1820,&l_1820},{&l_1820,&l_1820,&l_1820},{&l_1820,&l_1820,&l_1820},{&l_1820,&l_1820,&l_1820},{(void*)0,(void*)0,&l_1820}};
                        uint32_t l_1825 = 0x4519F073L;
                        int i, j;
                        (*g_39) ^= ((safe_mul_func_int16_t_s_s((safe_rshift_func_uint64_t_u_u(((safe_div_func_uint64_t_u_u(l_1701, (0xD0DD081FB7192EEDLL | (safe_sub_func_int16_t_s_s(((***g_266) > (((((void*)0 == (*l_1769)) ^ (&g_384 != l_1819)) , (((l_1822 = l_1820) == (void*)0) <= l_1825)) > g_534.f5)), l_1766[2][1][0]))))) <= l_1810), 21)), (**g_1059))) ^ (-8L));
                        if (l_1658)
                            goto lbl_1826;
                        l_1777 &= (8UL >= l_1756);
                    }
                    for (l_1634 = 0; (l_1634 >= 0); l_1634 -= 1)
                    { /* block id: 848 */
                        int32_t *l_1829[6] = {&l_1710,&l_1710,&l_1710,&l_1710,&l_1710,&l_1710};
                        int i;
                        l_1830[0][2][1] |= (((*g_1706) , l_1766[2][1][0]) , ((*g_770) == (l_1827 >= (safe_unary_minus_func_int8_t_s(l_1827)))));
                    }
                    if (((safe_div_func_int16_t_s_s(0x28F8L, (safe_rshift_func_int8_t_s_u((safe_mod_func_uint8_t_u_u((++(**l_1824)), (0xFFCBC420B167AC4CLL && ((safe_lshift_func_int32_t_s_u((((l_1766[2][1][0] > (safe_mod_func_int64_t_s_s((249UL | ((*l_1751) |= (safe_div_func_int64_t_s_s(((safe_mul_func_int8_t_s_s(l_1810, (&l_1691 == ((***g_1361) , ((0L & ((((safe_mul_func_uint8_t_u_u(((void*)0 != l_1849), l_1830[0][2][1])) != l_1701) < (*g_932)) > 0x5B2801F5L)) , &g_65[8]))))) <= l_1827), 0x1A3A0DB06F5C0CD6LL)))), l_1830[3][0][2]))) <= g_63) , l_1830[0][2][1]), 29)) , l_1774)))), g_780.f1)))) , l_1713))
                    { /* block id: 853 */
                        if (l_1634)
                            break;
                        if (g_89)
                            goto lbl_1851;
                    }
                    else
                    { /* block id: 856 */
                        uint32_t **l_1859 = &l_1675;
                        int32_t l_1871 = (-1L);
                        uint32_t l_1872 = 0UL;
                        (*g_39) = (l_1776 = (safe_lshift_func_uint64_t_u_u((--(**g_267)), 59)));
                        (****g_1322) = ((safe_div_func_uint32_t_u_u((l_1858 == ((*l_1859) = &g_89)), (safe_lshift_func_int64_t_s_s(((((*g_770) = (safe_rshift_func_int16_t_s_u(((void*)0 != l_1864), ((safe_lshift_func_int32_t_s_s(((*g_39) = (safe_sub_func_int32_t_s_s((((safe_lshift_func_int32_t_s_s(l_1871, 29)) < (g_676.f6 < g_926.f1)) & 0xCC0FEBE7L), l_1830[0][2][1]))), 22)) , 0UL)))) ^ g_676.f0) || 255UL), 36)))) > l_1872);
                    }
                }
                else
                { /* block id: 865 */
                    int32_t l_1874 = 0x00C07DD8L;
                    int32_t *l_1875 = &l_1713;
                    int32_t *l_1876 = &l_1695[2][0][0];
                    int32_t *l_1877 = &l_1776;
                    int32_t *l_1878 = &l_1710;
                    int32_t *l_1879 = &l_1694;
                    int32_t *l_1880 = &l_1874;
                    int32_t *l_1882 = &l_1777;
                    int32_t *l_1883 = &l_1777;
                    int32_t *l_1884[10][9] = {{(void*)0,&l_1777,&l_1777,(void*)0,(void*)0,(void*)0,&l_1777,&l_1777,(void*)0},{&l_1776,(void*)0,&l_1634,(void*)0,&l_1776,&l_1657,&l_1634,&l_1710,&l_1634},{&l_1777,(void*)0,(void*)0,(void*)0,(void*)0,&l_1777,(void*)0,(void*)0,(void*)0},{&l_1776,&l_1657,&l_1634,&l_1710,&l_1634,&l_1657,&l_1776,(void*)0,&l_1634},{(void*)0,(void*)0,(void*)0,&l_1777,&l_1777,(void*)0,(void*)0,(void*)0,&l_1777},{&l_1777,(void*)0,&l_1634,&g_1024,&g_40,&g_1024,&l_1634,(void*)0,&l_1777},{(void*)0,&l_1777,(void*)0,&l_1777,(void*)0,(void*)0,&l_1777,(void*)0,&l_1777},{&l_1634,&g_1024,&l_1634,&l_1710,&g_40,(void*)0,&g_40,&l_1710,&l_1634},{(void*)0,(void*)0,&l_1777,(void*)0,&l_1777,(void*)0,(void*)0,&l_1777,(void*)0},{&l_1777,&g_1024,&l_1777,(void*)0,&l_1634,&g_1024,&g_40,&g_1024,&l_1634}};
                    uint8_t l_1886[2];
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_1886[i] = 6UL;
                    l_1873 = (void*)0;
                    if (l_1776)
                        continue;
                    l_1886[1]--;
                }
            }
            else
            { /* block id: 870 */
                uint8_t l_1894 = 254UL;
                int16_t l_1906 = (-1L);
                uint8_t **l_1909 = &l_1757;
                uint32_t **l_1912 = &l_1858;
                int64_t * const *l_1917 = &g_717;
                int64_t * const * const *l_1916 = &l_1917;
                int64_t * const * const **l_1915 = &l_1916;
                int64_t * const * const ***l_1918 = &l_1915;
                int32_t *l_1920[1];
                int32_t **l_1921[2];
                int i;
                for (i = 0; i < 1; i++)
                    l_1920[i] = (void*)0;
                for (i = 0; i < 2; i++)
                    l_1921[i] = &g_39;
                (**g_1324) |= l_1889;
                (*g_1325) = (safe_lshift_func_uint32_t_u_u((((*g_717) = (safe_lshift_func_int16_t_s_u(l_1894, 7))) != (g_1895 , ((((safe_sub_func_int32_t_s_s((safe_mod_func_int32_t_s_s((((((((((safe_sub_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((safe_mul_func_int64_t_s_s(l_1906, (((((*g_39) &= (safe_lshift_func_int8_t_s_u((-10L), 4))) , (((*g_39) ^= (l_1909 == l_1910)) || ((l_1912 == &g_1469) <= (safe_lshift_func_uint32_t_u_s((((*l_1918) = l_1915) != l_1797), l_1766[2][1][0]))))) , 0x69L) ^ l_1776))), (**g_931))), 0x632BL)) & 0xCE5D3BFB3928AAD0LL) > l_1701) , l_1713) <= l_1713) < l_1776) < 0x57L) || l_1756) ^ (-10L)), l_1776)), l_1906)) , (void*)0) != l_1919) < l_1777))), 25));
                l_1922 = l_1920[0];
            }
            return l_1923[5][2];
        }
    }
    else
    { /* block id: 881 */
        for (g_1420 = 6; (g_1420 >= 0); g_1420 -= 1)
        { /* block id: 884 */
            uint8_t l_1927 = 0x00L;
            int i;
            for (g_63 = 0; (g_63 <= 6); g_63 += 1)
            { /* block id: 887 */
                (**g_1324) |= (~(safe_lshift_func_uint8_t_u_s(l_1927, 6)));
                (*g_1325) = 0xB078A46EL;
            }
            (*l_1631) = (void*)0;
        }
    }
    return (*l_1922);
}


/* ------------------------------------------ */
/* 
 * reads : g_1616 g_267 g_268 g_83 g_40
 * writes: g_18 g_88 g_40
 */
static int64_t * func_21(int32_t  p_22)
{ /* block id: 719 */
    uint32_t l_1613[7][7] = {{2UL,18446744073709551611UL,18446744073709551615UL,2UL,0x43BF06C9L,0UL,18446744073709551611UL},{0xCD832D40L,0x592A46D1L,0xE45CE847L,0x58CB9793L,0xE45CE847L,0x592A46D1L,0xCD832D40L},{0x592A46D1L,18446744073709551611UL,0UL,0xE45CE847L,0xCD832D40L,0x592A46D1L,0xE45CE847L},{2UL,0x43BF06C9L,0UL,18446744073709551611UL,18446744073709551611UL,0UL,0x43BF06C9L},{18446744073709551611UL,0x0705CFAEL,0UL,0x58CB9793L,0x0705CFAEL,0xE45CE847L,0x43BF06C9L},{18446744073709551612UL,18446744073709551611UL,0xE45CE847L,18446744073709551612UL,0x43BF06C9L,18446744073709551612UL,0xE45CE847L},{0xCD832D40L,0xCD832D40L,18446744073709551615UL,0x0705CFAEL,18446744073709551612UL,18446744073709551611UL,0xE45CE847L}};
    int8_t *l_1617 = &g_65[8];
    uint8_t l_1618 = 0x98L;
    uint64_t l_1619 = 0x97F108F5A5F65447LL;
    int64_t *l_1620 = &g_18;
    int64_t *l_1621 = &g_88;
    int64_t l_1622 = (-9L);
    int8_t l_1623 = 0x37L;
    int32_t *l_1624 = (void*)0;
    int32_t *l_1625 = (void*)0;
    int32_t *l_1626 = &g_40;
    int64_t *l_1627 = &g_63;
    int i, j;
    (*l_1626) ^= (safe_mul_func_int64_t_s_s((((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint32_t_u_u(((safe_mod_func_uint32_t_u_u((((safe_sub_func_uint32_t_u_u((safe_mul_func_int32_t_s_s((l_1613[6][3] > (safe_add_func_uint32_t_u_u(7UL, (g_1616 , l_1613[6][3])))), 0x333860B4L)), (0x282446119A2F106CLL < ((*l_1621) = ((*l_1620) = (((((void*)0 == &g_931) | ((((((((0L && p_22) || (**g_267)) || 0UL) && 0xF52D8A49A919655FLL) , l_1617) != (void*)0) || l_1618) <= l_1619)) ^ (-1L)) > l_1613[6][3])))))) > p_22) ^ l_1613[0][3]), p_22)) == l_1618), 19)), 6)) & l_1622) , p_22), l_1623));
    return l_1627;
}


/* ------------------------------------------ */
/* 
 * reads : g_63 g_906 g_396 g_397 g_398 g_40 g_642.f4 g_1007 g_9 g_65 g_268 g_83 g_932 g_933 g_676.f2 g_1024 g_266 g_267 g_958.f7 g_623.f6 g_459.f4 g_624.f0 g_781.f0 g_1071 g_1075 g_1086 g_1059 g_770 g_121 g_626.f6 g_159.f0 g_125 g_739 g_740 g_741 g_911 g_926.f9 g_385.f2 g_742 g_1199 g_122 g_931 g_130 g_626.f4 g_344 g_769 g_926.f4 g_185 g_554 g_551 g_533.f5 g_459.f6 g_781 g_1276 g_15 g_89 g_1289 g_1307 g_978 g_1321 g_436 g_1324 g_1325 g_1326 g_88 g_242.f4 g_1361 g_780.f9 g_947 g_946 g_1322 g_1323 g_1392 g_1397 g_625.f1 g_623.f4 g_1429 g_1362 g_1363 g_242.f2 g_86 g_119 g_1517 g_1538 g_533.f4 g_1550 g_1551 g_1553 g_1469 g_1470 g_625.f9 g_1331
 * writes: g_63 g_978 g_86 g_161 g_986 g_65 g_436 g_40 g_88 g_9 g_122 g_1059 g_349 g_15 g_344 g_1024 g_121 g_83 g_687 g_1230 g_1266 g_89 g_1289 g_1331 g_18 g_1326 g_1397 g_1420 g_1276 g_1519 g_1325 g_1550 g_1553 g_185.f9
 */
static uint64_t  func_26(int16_t  p_27, uint32_t  p_28, uint32_t  p_29, int32_t  p_30)
{ /* block id: 426 */
    int16_t **l_975 = &g_770;
    int16_t ***l_974 = &l_975;
    int32_t l_976 = 0xBB574F1FL;
    int32_t l_1023 = 0x5E8901E5L;
    int32_t l_1025 = 0xBE64E440L;
    int64_t ****l_1080 = &g_715[3];
    struct S2 *l_1096 = (void*)0;
    struct S2 **l_1095 = &l_1096;
    const int32_t **l_1106 = &g_687;
    const int32_t ***l_1105 = &l_1106;
    const int32_t ****l_1104 = &l_1105;
    const int32_t *****l_1103 = &l_1104;
    const uint32_t l_1107 = 0xC46FF4EEL;
    uint64_t **l_1109[7] = {&g_268,&g_268,&g_268,&g_268,&g_268,&g_268,&g_268};
    int8_t l_1122 = 0xEEL;
    int32_t l_1163 = (-4L);
    int32_t l_1164 = 1L;
    int32_t l_1167 = (-7L);
    int16_t l_1168 = 0xB7E3L;
    int32_t l_1170 = 0x4CF22095L;
    int32_t l_1171 = 0x974B60E3L;
    int32_t l_1173[3][2] = {{(-1L),0x53AC4E1CL},{0x53AC4E1CL,(-1L)},{0x53AC4E1CL,0x53AC4E1CL}};
    uint16_t l_1174 = 0x1DFAL;
    int32_t *l_1212[5] = {&l_1164,&l_1164,&l_1164,&l_1164,&l_1164};
    uint8_t **l_1238 = (void*)0;
    uint8_t ***l_1237 = &l_1238;
    uint64_t l_1310 = 0x4BD0B899AF6AA860LL;
    uint8_t l_1348 = 253UL;
    uint64_t l_1366 = 0xCECD55B79FE5D5D1LL;
    int8_t l_1396 = 0x73L;
    int32_t l_1430 = 6L;
    const struct S1 *l_1431[5][8][6] = {{{&g_1307,(void*)0,&g_534,&g_642,(void*)0,&g_1199},{(void*)0,&g_676,&g_642,&g_1307,(void*)0,&g_1307},{(void*)0,&g_533,&g_1307,&g_642,(void*)0,&g_385},{&g_1307,(void*)0,&g_1199,&g_1429,&g_1307,&g_534},{&g_1199,&g_1307,(void*)0,(void*)0,&g_534,&g_385},{&g_534,&g_1307,&g_676,(void*)0,&g_385,&g_1429},{&g_1199,&g_534,(void*)0,&g_534,(void*)0,(void*)0},{&g_533,(void*)0,&g_534,&g_385,&g_534,(void*)0}},{{&g_242[7],&g_242[2],&g_242[7],&g_1307,&g_676,(void*)0},{&g_534,&g_533,(void*)0,&g_534,(void*)0,(void*)0},{&g_242[2],&g_533,&g_533,&g_642,&g_676,&g_534},{&g_1307,&g_242[2],&g_149,&g_1199,&g_534,&g_534},{&g_534,(void*)0,&g_242[6],&g_1307,(void*)0,&g_642},{(void*)0,&g_534,&g_534,&g_242[7],&g_385,&g_1307},{(void*)0,&g_1307,&g_534,(void*)0,&g_534,(void*)0},{&g_534,&g_1307,&g_642,&g_1307,&g_1307,&g_642}},{{(void*)0,(void*)0,&g_534,&g_385,(void*)0,(void*)0},{&g_676,&g_533,&g_242[2],&g_642,(void*)0,&g_534},{(void*)0,&g_676,&g_242[2],(void*)0,(void*)0,(void*)0},{&g_534,(void*)0,&g_534,(void*)0,&g_534,&g_642},{(void*)0,&g_534,&g_642,&g_1307,&g_385,(void*)0},{&g_1199,&g_1199,&g_534,(void*)0,&g_533,&g_1307},{&g_533,&g_533,&g_534,&g_1307,&g_242[6],&g_642},{&g_533,&g_534,&g_242[6],&g_534,(void*)0,&g_534}},{{&g_149,&g_534,&g_149,(void*)0,&g_534,&g_534},{(void*)0,&g_149,&g_533,&g_1199,&g_242[7],(void*)0},{&g_534,(void*)0,(void*)0,&g_1199,(void*)0,(void*)0},{(void*)0,&g_534,&g_242[7],(void*)0,&g_534,(void*)0},{&g_149,&g_242[2],&g_1307,&g_242[7],&g_642,&g_1307},{&g_534,&g_534,&g_534,&g_533,&g_242[7],&g_534},{(void*)0,&g_642,&g_534,&g_642,(void*)0,&g_642},{(void*)0,&g_149,&g_534,&g_149,&g_533,&g_533}},{{&g_1307,(void*)0,(void*)0,&g_1307,&g_534,(void*)0},{&g_242[7],&g_1199,&g_533,&g_149,&g_1429,&g_149},{&g_533,&g_1307,&g_242[6],&g_676,&g_1429,&g_242[2]},{&g_385,&g_1199,&g_1307,&g_642,&g_534,(void*)0},{&g_1307,(void*)0,&g_534,&g_533,&g_533,(void*)0},{&g_1307,&g_149,(void*)0,&g_534,(void*)0,&g_1307},{&g_534,&g_642,&g_385,&g_1307,&g_242[7],&g_533},{&g_1199,&g_534,&g_385,&g_676,&g_642,&g_1429}}};
    int32_t l_1458 = 0x87B22F88L;
    int32_t *l_1471 = &g_1024;
    int32_t l_1552 = 0xCD780687L;
    uint64_t l_1562 = 1UL;
    uint16_t l_1571 = 0x32E5L;
    int32_t l_1577 = (-8L);
    uint32_t *l_1597 = (void*)0;
    int16_t l_1598 = 0x9859L;
    int32_t l_1599[2][7] = {{(-1L),3L,0x60B873E6L,3L,(-1L),0x70DE9619L,0x70DE9619L},{(-1L),3L,0x60B873E6L,3L,(-1L),0x70DE9619L,0x70DE9619L}};
    int i, j, k;
    for (g_63 = 7; (g_63 >= 0); g_63 -= 1)
    { /* block id: 429 */
        uint32_t *l_961 = &g_179;
        uint32_t **l_960 = &l_961;
        uint32_t ***l_959[1];
        const int32_t l_1008 = 1L;
        uint8_t l_1048[4][2] = {{0x8BL,0x8BL},{0x8BL,0x8BL},{0x8BL,0x8BL},{0x8BL,0x8BL}};
        int32_t **l_1069 = (void*)0;
        int16_t l_1094 = (-6L);
        const struct S2 *l_1098 = &g_623;
        const struct S2 **l_1097 = &l_1098;
        uint64_t * const *l_1100 = &g_268;
        uint64_t * const **l_1099 = &l_1100;
        int64_t l_1101 = 9L;
        int32_t *****l_1102 = (void*)0;
        int64_t l_1137 = 0x3DB7CA9A0789A12CLL;
        int32_t l_1161 = 0x69A3C84BL;
        int32_t l_1166[10][6][2] = {{{0x48AFEB26L,0x6E7FCB54L},{0L,0x395A2325L},{7L,0x626A87DCL},{1L,0L},{0x562D5A41L,0x562D5A41L},{(-10L),(-4L)}},{{0L,0x5FB8D227L},{7L,9L},{(-1L),7L},{3L,0x562D5A41L},{3L,7L},{(-1L),9L}},{{7L,0x5FB8D227L},{0L,(-4L)},{(-10L),0x562D5A41L},{0x562D5A41L,0L},{1L,0x626A87DCL},{7L,0x395A2325L}},{{0L,0x6E7FCB54L},{0x48AFEB26L,0x562D5A41L},{1L,0x706A4AE3L},{0x21F8F435L,0L},{7L,0L},{0x21F8F435L,0x706A4AE3L}},{{1L,0x562D5A41L},{0x48AFEB26L,0x6E7FCB54L},{0L,0x395A2325L},{7L,0x626A87DCL},{1L,0L},{0x562D5A41L,0x562D5A41L}},{{(-10L),(-4L)},{0L,0x5FB8D227L},{7L,9L},{(-1L),7L},{3L,0x562D5A41L},{3L,7L}},{{(-1L),9L},{7L,0x5FB8D227L},{0L,(-4L)},{(-10L),0x562D5A41L},{0x562D5A41L,0L},{1L,0x626A87DCL}},{{7L,0x395A2325L},{0L,0x6E7FCB54L},{0x48AFEB26L,0x562D5A41L},{1L,0x706A4AE3L},{0x21F8F435L,0L},{7L,0L}},{{0x21F8F435L,0x706A4AE3L},{0x6E7FCB54L,7L},{0L,0L},{8L,(-10L)},{0x21F8F435L,0x562D5A41L},{0x34149814L,1L}},{{7L,7L},{(-4L),0L},{0x1BEE0716L,0x48AFEB26L},{0x21F8F435L,1L},{1L,0x21F8F435L},{0x706A4AE3L,7L}}};
        uint64_t l_1209 = 18446744073709551612UL;
        const uint8_t **l_1236 = (void*)0;
        const uint8_t ***l_1235[7] = {&l_1236,&l_1236,&l_1236,&l_1236,&l_1236,&l_1236,&l_1236};
        int32_t ***l_1247 = &g_349;
        uint32_t l_1249 = 7UL;
        const int64_t ***l_1259 = (void*)0;
        const int64_t ****l_1258 = &l_1259;
        uint16_t l_1446 = 65535UL;
        int32_t l_1497[4][6][8] = {{{(-2L),0x1F9F91F8L,0L,8L,7L,0x7A894DCEL,0xBA72883AL,(-1L)},{7L,9L,0L,7L,7L,0L,9L,7L},{(-2L),7L,2L,(-1L),0x1F9F91F8L,9L,0L,0x9FDA5691L},{1L,0x1C7F9111L,0x69B0C230L,9L,(-1L),9L,0x69B0C230L,0x1C7F9111L},{0x7A894DCEL,7L,0x9FDA5691L,(-6L),(-2L),0L,0x1F9F91F8L,0x69B0C230L},{0x1C7F9111L,9L,7L,0x1F9F91F8L,0x7A894DCEL,0x7A894DCEL,0x1F9F91F8L,7L}},{{0x1F9F91F8L,0x1F9F91F8L,0x9FDA5691L,0L,8L,2L,0x69B0C230L,1L},{8L,2L,0x69B0C230L,1L,0L,7L,0L,1L},{2L,0x9FDA5691L,2L,0L,0xBA72883AL,0x1C7F9111L,9L,7L},{(-6L),(-2L),0L,0x1F9F91F8L,0x69B0C230L,0xBA72883AL,0xBA72883AL,0x69B0C230L},{(-6L),0L,0L,(-6L),0xBA72883AL,0x1F9F91F8L,1L,0x1C7F9111L},{2L,(-1L),0x1F9F91F8L,9L,0L,0x9FDA5691L,0x1C7F9111L,0x9FDA5691L}},{{8L,(-1L),1L,(-1L),8L,0x1F9F91F8L,(-6L),7L},{0x1F9F91F8L,0L,8L,7L,0x7A894DCEL,0xBA72883AL,(-1L),(-1L)},{0x1C7F9111L,(-2L),8L,8L,(-2L),0x1C7F9111L,(-6L),0x7A894DCEL},{0x7A894DCEL,0x9FDA5691L,1L,7L,(-1L),7L,0x1C7F9111L,0x1F9F91F8L},{1L,2L,0x1F9F91F8L,7L,0x1F9F91F8L,2L,1L,0x7A894DCEL},{(-2L),0x1F9F91F8L,0L,8L,7L,0x7A894DCEL,0xBA72883AL,(-1L)}},{{7L,9L,0L,7L,7L,0L,9L,7L},{(-2L),7L,2L,(-1L),0x1F9F91F8L,9L,0L,0x9FDA5691L},{1L,(-1L),7L,0x1C7F9111L,0x1F9F91F8L,0x1C7F9111L,7L,(-1L)},{0L,(-6L),9L,0x7A894DCEL,0L,(-2L),1L,7L},{(-1L),0x1C7F9111L,8L,1L,0L,0L,1L,8L},{1L,1L,9L,(-2L),2L,0xBA72883AL,7L,7L}}};
        struct S2 *l_1507 = &g_1508;
        uint16_t l_1527 = 0xECF3L;
        uint16_t l_1549 = 65535UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_959[i] = &l_960;
        if ((&g_904[8][2][4] != l_959[0]))
        { /* block id: 430 */
            int32_t l_966 = (-4L);
            int32_t l_981 = 1L;
            uint8_t l_1029 = 0x05L;
            int16_t **l_1060 = &g_770;
            struct S0 *l_1072 = (void*)0;
            struct S0 **l_1073 = &l_1072;
            int i;
            if (g_906[g_63])
            { /* block id: 431 */
                uint32_t **l_971[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                uint8_t *l_977[10][6] = {{&g_978,&g_978,&g_978,&g_978,&g_978,&g_978},{&g_978,&g_978,&g_978,&g_978,(void*)0,&g_978},{(void*)0,&g_978,&g_978,&g_978,(void*)0,&g_978},{&g_978,&g_978,&g_978,&g_978,&g_978,&g_978},{&g_978,&g_978,&g_978,&g_978,&g_978,&g_978},{&g_978,&g_978,&g_978,(void*)0,&g_978,&g_978},{&g_978,&g_978,&g_978,&g_978,&g_978,&g_978},{&g_978,&g_978,&g_978,(void*)0,&g_978,&g_978},{&g_978,&g_978,&g_978,&g_978,&g_978,&g_978},{&g_978,&g_978,&g_978,&g_978,&g_978,&g_978}};
                int32_t *l_979 = (void*)0;
                int32_t *l_980[7] = {&g_40,(void*)0,&g_40,&g_40,(void*)0,&g_40,&g_40};
                int i, j;
                l_981 |= ((safe_sub_func_int16_t_s_s(((void*)0 != &g_676), g_906[g_63])) >= (safe_div_func_int32_t_s_s((l_966 >= (g_978 = ((safe_lshift_func_uint64_t_u_s(((safe_mul_func_int8_t_s_s((l_971[6] == l_971[1]), (safe_mod_func_uint64_t_u_u(0xA22850328C63BF3ALL, (-6L))))) < (&g_769 == l_974)), 59)) , l_976))), 0xF02214A9L)));
                if ((***g_396))
                    break;
            }
            else
            { /* block id: 435 */
                int32_t l_982 = 0L;
                struct S0 *l_983 = &g_872;
                struct S0 **l_984 = &g_161;
                struct S0 **l_987 = &g_986;
                struct S2 *l_994 = &g_926;
                struct S2 **l_1018 = &l_994;
                struct S2 ***l_1017 = &l_1018;
                int32_t l_1067 = 0xA7E0A81EL;
                for (g_86 = 0; (g_86 <= 2); g_86 += 1)
                { /* block id: 438 */
                    return l_982;
                }
                (*l_987) = ((*l_984) = l_983);
                if (l_982)
                { /* block id: 443 */
                    struct S2 **l_995 = &l_994;
                    int64_t l_1001[4][8][8] = {{{(-1L),0L,6L,0xCE1B3E055C2145F6LL,4L,0xC4962D159F93DCB6LL,4L,0xCE1B3E055C2145F6LL},{7L,0L,7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL},{0L,0xC4962D159F93DCB6LL,6L,0xC4962D159F93DCB6LL,0L,0xFC032BD0A88A176CLL,(-1L),0xCE1B3E055C2145F6LL},{0L,0xFC032BD0A88A176CLL,(-1L),0xCE1B3E055C2145F6LL,(-1L),0xFC032BD0A88A176CLL,0L,0xC4962D159F93DCB6LL},{7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL,(-1L),0xC4962D159F93DCB6LL},{(-1L),0L,6L,0xCE1B3E055C2145F6LL,4L,0xC4962D159F93DCB6LL,4L,0xCE1B3E055C2145F6LL},{7L,0L,7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL},{0L,0xC4962D159F93DCB6LL,6L,0xC4962D159F93DCB6LL,0L,0xFC032BD0A88A176CLL,(-1L),0xCE1B3E055C2145F6LL}},{{0L,0xFC032BD0A88A176CLL,(-1L),0xCE1B3E055C2145F6LL,(-1L),0xFC032BD0A88A176CLL,0L,0xC4962D159F93DCB6LL},{7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL,(-1L),0xC4962D159F93DCB6LL},{(-1L),0L,6L,0xCE1B3E055C2145F6LL,4L,0x3C205C40B1DA0F8CLL,0L,0L},{6L,0xFC032BD0A88A176CLL,6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL},{(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL,(-1L),0xC4962D159F93DCB6LL,7L,0L},{(-1L),0xC4962D159F93DCB6LL,7L,0L,7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL},{6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL,7L,0x3C205C40B1DA0F8CLL},{7L,0xFC032BD0A88A176CLL,4L,0L,0L,0x3C205C40B1DA0F8CLL,0L,0L}},{{6L,0xFC032BD0A88A176CLL,6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL},{(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL,(-1L),0xC4962D159F93DCB6LL,7L,0L},{(-1L),0xC4962D159F93DCB6LL,7L,0L,7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL},{6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL,7L,0x3C205C40B1DA0F8CLL},{7L,0xFC032BD0A88A176CLL,4L,0L,0L,0x3C205C40B1DA0F8CLL,0L,0L},{6L,0xFC032BD0A88A176CLL,6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL},{(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL,(-1L),0xC4962D159F93DCB6LL,7L,0L},{(-1L),0xC4962D159F93DCB6LL,7L,0L,7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL}},{{6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL,7L,0x3C205C40B1DA0F8CLL},{7L,0xFC032BD0A88A176CLL,4L,0L,0L,0x3C205C40B1DA0F8CLL,0L,0L},{6L,0xFC032BD0A88A176CLL,6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL},{(-1L),0x3C205C40B1DA0F8CLL,4L,0x3C205C40B1DA0F8CLL,(-1L),0xC4962D159F93DCB6LL,7L,0L},{(-1L),0xC4962D159F93DCB6LL,7L,0L,7L,0xC4962D159F93DCB6LL,(-1L),0x3C205C40B1DA0F8CLL},{6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL,7L,0x3C205C40B1DA0F8CLL},{7L,0xFC032BD0A88A176CLL,4L,0L,0L,0x3C205C40B1DA0F8CLL,0L,0L},{6L,0xFC032BD0A88A176CLL,6L,0x3C205C40B1DA0F8CLL,7L,0xCE1B3E055C2145F6LL,0L,0xCE1B3E055C2145F6LL}}};
                    int32_t l_1004 = 0x9E19AC71L;
                    int8_t *l_1005[2][4];
                    int64_t *l_1006 = &g_436;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 4; j++)
                            l_1005[i][j] = &g_65[4];
                    }
                    (*g_1007) &= (safe_mul_func_int64_t_s_s((((safe_mod_func_int16_t_s_s(2L, 0xD3F9L)) , ((*l_1006) = ((((*l_995) = l_994) == (void*)0) == (((safe_mul_func_uint16_t_u_u((((safe_unary_minus_func_int16_t_s(p_30)) != ((((-1L) | (l_982 &= (g_65[8] = (safe_mul_func_int32_t_s_s(((((l_1001[3][3][0] == (1L != (safe_mul_func_uint8_t_u_u(((p_29 != g_642.f4) < l_976), 0x70L)))) <= l_976) < l_1004) > l_1004), 1UL))))) , l_981) ^ p_28)) != p_29), (-9L))) > 0UL) < 0xDDBE91C57AA4AF4CLL)))) , (-8L)), l_976));
                }
                else
                { /* block id: 449 */
                    const uint64_t **l_1028 = (void*)0;
                    int16_t **l_1058 = &g_770;
                    int64_t *l_1061 = &g_88;
                    const int32_t l_1062 = 1L;
                    int8_t *l_1063 = &g_65[4];
                    int64_t l_1064 = (-1L);
                    uint32_t *l_1065 = (void*)0;
                    uint32_t *l_1066[2];
                    int32_t *l_1068 = &l_981;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1066[i] = &g_86;
                    for (l_982 = 1; (l_982 >= 0); l_982 -= 1)
                    { /* block id: 452 */
                        if (l_1008)
                            break;
                        if (p_30)
                            continue;
                        if (p_28)
                            continue;
                    }
                    for (g_88 = 0; (g_88 <= 1); g_88 += 1)
                    { /* block id: 459 */
                        uint16_t *l_1012 = &g_9;
                        int32_t l_1015 = 0xDF2926BDL;
                        int32_t *l_1016 = &l_976;
                        uint16_t *l_1019 = &g_122[4][7][1];
                        struct S1 *l_1020 = &g_385;
                        int8_t *l_1021 = &g_65[8];
                        l_1025 |= (((safe_lshift_func_uint8_t_u_s((((safe_unary_minus_func_int64_t_s((((*l_1012)++) ^ ((((-1L) ^ ((*l_1016) = l_1015)) , 0x563EL) >= ((*l_1019) = (&g_270 != l_1017)))))) >= ((((-10L) != (((*l_1021) &= ((void*)0 != l_1020)) != (+(((*g_268) <= p_30) || 3L)))) < p_27) & (*g_932))) & g_676.f2), l_1023)) <= l_982) && g_1024);
                        l_1029 ^= (0L > (safe_rshift_func_uint16_t_u_s((((*g_266) == l_1028) | 2L), 10)));
                    }
                    (*l_1068) ^= (0x1C3DL || ((safe_rshift_func_uint8_t_u_u(l_1008, 1)) && (safe_mul_func_uint32_t_u_u((l_1067 |= (((safe_div_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s((+((((safe_div_func_int8_t_s_s(((*l_1063) = ((safe_mod_func_uint64_t_u_u((safe_rshift_func_int8_t_s_s(g_958.f7, (safe_div_func_uint8_t_u_u((~(l_1048[2][1] >= (g_978 = g_623.f6))), (+(safe_mod_func_uint8_t_u_u((safe_mod_func_int64_t_s_s(((*l_1061) = (p_27 , (((safe_div_func_int16_t_s_s(1L, (safe_mul_func_int16_t_s_s(((l_1025 >= ((g_1059 = l_1058) == l_1060)) | p_29), 1UL)))) <= 0xF09EL) < 7UL))), l_1062)), l_982))))))), 0x863B7E5CBAD94890LL)) != 0xD5A6F83287131721LL)), 251UL)) || 0x4DL) || g_459.f4) || (*g_268))), 0xF128D8D4L)) == l_1064), l_982)) < g_624.f0) & p_28)), g_781[0].f0))));
                    (*g_1071) = l_1069;
                }
            }
            (*g_1075) = ((*l_1073) = l_1072);
        }
        else
        { /* block id: 478 */
            return (*g_268);
        }
        if (((p_30 <= (((safe_div_func_int8_t_s_s((((p_28 & (((((void*)0 == l_1080) , (l_1102 = (((l_1023 , (safe_mul_func_int32_t_s_s((((((((~((safe_lshift_func_int64_t_s_u((g_1086 , (~((l_1023 = (safe_mod_func_int64_t_s_s((0x09L | (((((((safe_rshift_func_uint64_t_u_u((((safe_mul_func_uint64_t_u_u((l_1094 != (**g_1059)), 0x1A5AECBC7EFD906FLL)) , l_1095) == l_1097), p_30)) & p_29) <= 5L) , l_1099) == &g_267) , g_626.f6) != l_1101)), p_29))) && g_159.f0))), 53)) , p_28)) , g_125) , 0UL) == l_976) , p_28) == p_28) , p_27), 0xFBABAEDCL))) > p_29) , (void*)0))) != l_1103) > p_30)) <= p_29) , l_1107), p_28)) ^ p_30) , 0x0CL)) , (-1L)))
        { /* block id: 483 */
            uint64_t **l_1108 = &g_268;
            int32_t l_1110 = 0xD10AAFA8L;
            uint16_t *l_1115 = &g_15;
            int8_t *l_1123 = &g_65[8];
            int32_t l_1159 = 9L;
            int32_t l_1169[2];
            uint8_t *l_1234 = (void*)0;
            uint8_t **l_1233 = &l_1234;
            uint32_t l_1254 = 0x6525D683L;
            struct S1 **l_1329[7][4][8] = {{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}},{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}},{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}},{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}},{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}},{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}},{{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384},{&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384}}};
            const int32_t * const * const *l_1416 = &g_397;
            const int32_t * const * const **l_1415 = &l_1416;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1169[i] = 0L;
            l_1110 &= (l_1108 == l_1109[5]);
            if (((p_30 >= (safe_div_func_int8_t_s_s(((*l_1123) &= (safe_div_func_int8_t_s_s((((((**g_739) != p_28) >= l_1110) , (((*l_1115) = 65535UL) & ((safe_rshift_func_uint64_t_u_s((safe_sub_func_uint32_t_u_u((0x29678791L <= ((((l_1110 , p_28) , g_911[1]) == l_1122) > g_125.f5)), l_1110)), p_30)) ^ p_29))) > g_83), g_926.f9))), 0x9BL))) > g_385.f2))
            { /* block id: 487 */
                int32_t *l_1136 = &l_1025;
                struct S2 *l_1138 = &g_926;
                int32_t l_1156 = 0x3775ABECL;
                int32_t l_1157 = 0L;
                int32_t l_1158 = 0x2AFCFD04L;
                int32_t l_1162 = (-1L);
                int32_t l_1165[6][6] = {{0xA58E5FA0L,0x97AF455AL,0x345DEA8DL,0x97AF455AL,0xA58E5FA0L,0x97AF455AL},{0x0AA58E8FL,0x07A76DB9L,0x0AA58E8FL,0x97AF455AL,0x0AA58E8FL,0x07A76DB9L},{0xA58E5FA0L,0x07A76DB9L,0x345DEA8DL,0x07A76DB9L,0xA58E5FA0L,0x07A76DB9L},{0x0AA58E8FL,0x97AF455AL,0x0AA58E8FL,0x07A76DB9L,0x0AA58E8FL,0x97AF455AL},{0xA58E5FA0L,0x97AF455AL,0x345DEA8DL,0x97AF455AL,0xA58E5FA0L,0x97AF455AL},{0x0AA58E8FL,0x07A76DB9L,0x0AA58E8FL,0x97AF455AL,0x0AA58E8FL,0x07A76DB9L}};
                int32_t *** const *l_1204 = (void*)0;
                uint8_t *l_1208[5][8][4] = {{{&g_978,&l_1048[3][1],(void*)0,(void*)0},{&g_978,&l_1048[2][1],&l_1048[2][1],&g_978},{(void*)0,&l_1048[2][1],(void*)0,&g_978},{(void*)0,&l_1048[2][1],&l_1048[3][0],(void*)0},{&l_1048[1][0],&l_1048[3][1],&l_1048[1][1],(void*)0},{&l_1048[0][0],&g_978,&l_1048[0][0],&g_978},{&g_978,(void*)0,&g_978,&g_978},{&l_1048[3][1],&l_1048[2][1],&g_978,(void*)0}},{{&l_1048[3][0],(void*)0,&g_978,(void*)0},{&l_1048[3][1],&l_1048[3][0],&g_978,&l_1048[1][0]},{&g_978,&l_1048[1][1],&l_1048[0][0],&l_1048[0][0]},{&l_1048[0][0],&l_1048[0][0],&l_1048[1][1],&g_978},{&l_1048[1][0],&g_978,&l_1048[3][0],&l_1048[3][1]},{(void*)0,&g_978,(void*)0,&l_1048[3][0]},{(void*)0,&g_978,&l_1048[2][1],&l_1048[3][1]},{&g_978,&g_978,(void*)0,&g_978}},{{&g_978,&l_1048[0][0],&g_978,&l_1048[0][0]},{(void*)0,&l_1048[1][1],&l_1048[3][1],&l_1048[1][0]},{(void*)0,&l_1048[3][0],&l_1048[2][1],(void*)0},{&g_978,(void*)0,&l_1048[2][1],(void*)0},{&g_978,&l_1048[2][1],&l_1048[2][1],&g_978},{(void*)0,(void*)0,&l_1048[3][1],&g_978},{(void*)0,&g_978,&g_978,(void*)0},{&g_978,&l_1048[3][1],(void*)0,(void*)0}},{{&g_978,&l_1048[2][1],&l_1048[2][1],&g_978},{(void*)0,&l_1048[2][1],(void*)0,&g_978},{(void*)0,&l_1048[2][1],&l_1048[3][0],(void*)0},{&l_1048[1][0],&l_1048[3][1],&l_1048[1][1],(void*)0},{&l_1048[0][0],&g_978,&l_1048[0][0],&g_978},{&g_978,(void*)0,&g_978,&g_978},{&l_1048[3][1],&l_1048[2][1],&g_978,(void*)0},{&l_1048[3][0],(void*)0,&g_978,(void*)0}},{{&l_1048[3][1],&l_1048[3][0],&g_978,&l_1048[1][0]},{&g_978,&l_1048[1][1],&l_1048[0][0],&l_1048[0][0]},{&l_1048[0][0],&l_1048[0][0],&l_1048[1][1],&g_978},{&l_1048[1][0],(void*)0,&g_978,&g_978},{&g_978,(void*)0,&l_1048[1][1],&g_978},{&g_978,(void*)0,&g_978,&g_978},{(void*)0,(void*)0,&g_978,&l_1048[0][0]},{&l_1048[3][0],(void*)0,&g_978,(void*)0}}};
                int16_t **l_1231 = &g_770;
                int32_t l_1232 = 7L;
                uint32_t **l_1244 = &l_961;
                uint32_t *l_1248 = (void*)0;
                int64_t ** const **l_1257 = (void*)0;
                int32_t l_1284 = 4L;
                int i, j, k;
                for (g_344 = 0; (g_344 <= 9); g_344 += 1)
                { /* block id: 490 */
                    int32_t *l_1139 = &l_1023;
                    int32_t l_1160 = (-8L);
                    int32_t l_1172[8][5][6] = {{{0x5FDA54FAL,3L,0x989C68FCL,0x0F89B295L,0x6B020A0CL,5L},{0x4AEC4D38L,0x55B43929L,1L,0L,5L,3L},{0x4AEC4D38L,0x856C1A5BL,0x55B43929L,0x0F89B295L,0x37AC638DL,0x37AC638DL},{0x5FDA54FAL,0x6B020A0CL,0x6B020A0CL,0x5FDA54FAL,1L,0x37AC638DL},{0x09A2742BL,0x37AC638DL,0x55B43929L,9L,1L,3L}},{{0xFB3AD7EEL,0L,1L,(-6L),1L,5L},{0L,0x37AC638DL,0x989C68FCL,0xFB3AD7EEL,1L,0x989C68FCL},{(-1L),0x6B020A0CL,0x856C1A5BL,0xFB3AD7EEL,0x37AC638DL,1L},{0L,0x856C1A5BL,5L,(-6L),5L,0x856C1A5BL},{0xFB3AD7EEL,0x55B43929L,5L,9L,0x6B020A0CL,1L}},{{0x09A2742BL,3L,0x856C1A5BL,0x5FDA54FAL,3L,0x989C68FCL},{0x5FDA54FAL,3L,0x989C68FCL,0x0F89B295L,0x6B020A0CL,5L},{0x4AEC4D38L,0x55B43929L,1L,0L,5L,3L},{0x4AEC4D38L,0x856C1A5BL,0x55B43929L,0x0F89B295L,0x37AC638DL,0x37AC638DL},{0x5FDA54FAL,0x6B020A0CL,0x6B020A0CL,0x5FDA54FAL,1L,0x37AC638DL}},{{0x09A2742BL,0x37AC638DL,0x55B43929L,9L,1L,3L},{0xFB3AD7EEL,0L,1L,(-6L),1L,5L},{0L,0x37AC638DL,0x989C68FCL,0xFB3AD7EEL,1L,0x989C68FCL},{(-1L),0x6B020A0CL,0x856C1A5BL,0xFB3AD7EEL,0x37AC638DL,1L},{0L,0x856C1A5BL,5L,(-6L),5L,0x856C1A5BL}},{{0xFB3AD7EEL,0x55B43929L,5L,9L,0x6B020A0CL,1L},{0x09A2742BL,3L,0x856C1A5BL,0x5FDA54FAL,3L,0x989C68FCL},{0x5FDA54FAL,3L,0x989C68FCL,0x0F89B295L,0x6B020A0CL,5L},{0x4AEC4D38L,0x55B43929L,1L,0L,5L,3L},{0x4AEC4D38L,0x856C1A5BL,0x55B43929L,0x0F89B295L,0x37AC638DL,0x37AC638DL}},{{0x5FDA54FAL,0x6B020A0CL,0x6B020A0CL,0x5FDA54FAL,1L,0x37AC638DL},{0x09A2742BL,0x37AC638DL,0x55B43929L,9L,1L,3L},{0xFB3AD7EEL,0L,1L,(-6L),1L,5L},{0L,0x37AC638DL,0x989C68FCL,0xFB3AD7EEL,1L,0L},{1L,6L,0xC9406E90L,3L,3L,0x1D7615C0L}},{{0L,0xC9406E90L,(-6L),0x55B43929L,(-6L),0xC9406E90L},{3L,0L,(-6L),0x856C1A5BL,6L,0x1D7615C0L},{1L,0xAFEE8347L,0xC9406E90L,0x6B020A0CL,0xAFEE8347L,0L},{0x6B020A0CL,0xAFEE8347L,0L,0x37AC638DL,6L,(-6L)},{0x989C68FCL,0L,1L,0L,(-6L),0xAFEE8347L}},{{0x989C68FCL,0xC9406E90L,0L,0x37AC638DL,3L,3L},{0x6B020A0CL,6L,6L,0x6B020A0CL,1L,3L},{1L,3L,0L,0x856C1A5BL,1L,0xAFEE8347L},{3L,(-1L),1L,0x55B43929L,1L,(-6L)},{0L,3L,0L,3L,1L,0L}}};
                    int16_t l_1207 = 0x8A26L;
                    int i, j, k;
                    if ((safe_sub_func_int16_t_s_s(g_65[g_63], (safe_add_func_uint8_t_u_u((g_65[g_63] && (safe_sub_func_int32_t_s_s((safe_sub_func_int16_t_s_s(0xB034L, ((safe_mod_func_uint16_t_u_u((((0UL >= (safe_div_func_int16_t_s_s((&l_1110 != l_1136), p_30))) , (-1L)) != l_1137), (*l_1136))) , (*l_1136)))), g_65[g_63]))), 1UL)))))
                    { /* block id: 491 */
                        uint32_t l_1140 = 0xABC3F4D3L;
                        (*l_1095) = l_1138;
                        l_1136 = l_1139;
                        (*l_1136) ^= 9L;
                        l_1140++;
                    }
                    else
                    { /* block id: 496 */
                        int32_t *l_1143 = &g_40;
                        int32_t *l_1144 = &l_1023;
                        int32_t *l_1145 = &l_1025;
                        int32_t *l_1146 = &l_1110;
                        int32_t *l_1147 = &g_1024;
                        int32_t *l_1148 = (void*)0;
                        int32_t *l_1149 = &l_1023;
                        int32_t *l_1150 = &g_40;
                        int32_t *l_1151 = &l_1025;
                        int32_t *l_1152 = &g_1024;
                        int32_t *l_1153 = (void*)0;
                        int32_t *l_1154 = &l_1025;
                        int32_t *l_1155[6] = {&l_1110,(void*)0,(void*)0,&l_1110,(void*)0,(void*)0};
                        int i;
                        ++l_1174;
                        (*l_1152) |= (((&g_905[1][4] != (void*)0) == (*l_1136)) != (safe_add_func_int64_t_s_s(l_1169[1], ((***g_742) && p_29))));
                    }
                    for (l_1167 = 1; (l_1167 >= 0); l_1167 -= 1)
                    { /* block id: 502 */
                        const uint32_t *l_1181 = (void*)0;
                        uint32_t *l_1183 = (void*)0;
                        uint32_t **l_1182 = &l_1183;
                        int32_t *l_1210 = (void*)0;
                        int32_t *l_1211 = &l_1025;
                        int i, j, k;
                        (*l_1211) ^= (((safe_rshift_func_uint64_t_u_s((l_1181 != ((*l_1182) = l_961)), 7)) , ((**l_1108) = (safe_add_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_uint16_t_u((safe_div_func_int64_t_s_s((safe_rshift_func_int8_t_s_u((l_1163 = (l_1209 = (safe_rshift_func_uint16_t_u_s(((safe_add_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s(((g_1199 , (safe_mul_func_int16_t_s_s(((**g_769) &= ((g_65[g_344] = (safe_div_func_uint32_t_u_u(g_122[(l_1167 + 4)][(l_1167 + 4)][l_1167], ((((void*)0 != l_1204) , (((((safe_add_func_int8_t_s_s((g_122[(l_1167 + 3)][(l_1167 + 2)][l_1167] < (l_1207 , (l_1208[1][7][2] == (*g_931)))), g_130)) && 0UL) > g_626.f4) != 1UL) , (*l_1139))) & l_1110)))) == l_1159)), 0x7D1AL))) ^ 0UL), 3)), 3UL)) , p_27), p_30)))), 0)), g_122[(l_1167 + 3)][(l_1167 + 2)][l_1167])))), 0x34A5L)), g_926.f4)))) || 0x1FE0991AE317A22ELL);
                        (*l_1106) = (l_1212[1] = &l_1110);
                    }
                }
                if ((safe_mod_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((safe_div_func_int8_t_s_s((safe_rshift_func_int64_t_s_u((+0x53L), (((g_906[5] && (((*l_1123) |= ((safe_add_func_uint32_t_u_u(p_30, (safe_div_func_int8_t_s_s((g_185 , (safe_rshift_func_int64_t_s_u((safe_lshift_func_int16_t_s_u(((((*g_554) , (**g_397)) , (**l_1103)) == (g_1230[1][1] = (void*)0)), ((void*)0 != l_1231))), l_1159))), l_1169[1])))) || l_1232)) || (**g_931))) && 0x8987A9CFL) > 65535UL))), p_29)), p_29)), 249UL)))
                { /* block id: 516 */
                    (*l_1136) = p_29;
                    if ((*l_1136))
                        continue;
                }
                else
                { /* block id: 519 */
                    uint8_t ****l_1239 = &l_1237;
                    for (g_1024 = 0; (g_1024 <= 1); g_1024 += 1)
                    { /* block id: 522 */
                        (*l_1136) &= p_27;
                        return (***g_266);
                    }
                    if (p_27)
                        break;
                    (*l_1136) = ((((l_1233 == (void*)0) | (p_30 < g_533.f5)) , l_1235[4]) != ((*l_1239) = l_1237));
                    for (l_1157 = 9; (l_1157 >= 0); l_1157 -= 1)
                    { /* block id: 531 */
                        int i;
                        return g_906[g_63];
                    }
                }
                if ((safe_add_func_uint16_t_u_u((safe_lshift_func_int32_t_s_s((g_459.f6 != 0L), (l_1169[1] = (p_27 ^ ((void*)0 == l_1244))))), (safe_sub_func_int32_t_s_s((((p_28 = (((*l_1104) != l_1247) , p_30)) & (***g_396)) , l_1249), 0x5B44E2F3L)))))
                { /* block id: 537 */
                    int32_t l_1251[3];
                    const int64_t *****l_1260 = &l_1258;
                    const int64_t *****l_1261 = (void*)0;
                    const int64_t ****l_1263 = &l_1259;
                    const int64_t *****l_1262 = &l_1263;
                    const int64_t ****l_1265 = (void*)0;
                    const int64_t *****l_1264[6];
                    uint64_t l_1279 = 0x1EA13A8E4C05CCDCLL;
                    int64_t *l_1294 = &l_1101;
                    int64_t *l_1308 = &g_88;
                    int8_t l_1309[1][2];
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1251[i] = 0L;
                    for (i = 0; i < 6; i++)
                        l_1264[i] = &l_1265;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_1309[i][j] = (-1L);
                    }
                    for (l_1170 = 1; (l_1170 >= 0); l_1170 -= 1)
                    { /* block id: 540 */
                        int16_t l_1250 = 0x30C3L;
                        int32_t l_1252 = 0x9FA76796L;
                        int32_t l_1253 = 1L;
                        l_1254++;
                    }
                    if ((l_1257 != (g_1266 = ((*l_1262) = (g_781[0] , ((*l_1260) = l_1258))))))
                    { /* block id: 546 */
                        int32_t l_1275 = 0x083D5DE4L;
                        uint32_t *l_1280 = &g_89;
                        int32_t l_1281 = 3L;
                        int32_t l_1282 = 1L;
                        int32_t l_1283 = (-1L);
                        int32_t l_1285 = 0x786D95E4L;
                        int32_t l_1286 = 0x22BB0DD2L;
                        int32_t l_1287 = 0xF95EC541L;
                        int32_t l_1288[10][9] = {{(-1L),(-1L),1L,3L,0x1E8A2A1DL,3L,1L,(-1L),(-1L)},{1L,0x0FC69F04L,0x751B6326L,0x0FC69F04L,1L,1L,0x0FC69F04L,0x751B6326L,0x0FC69F04L},{1L,0x2DB2ECF3L,0xE993BB80L,1L,0x1E8A2A1DL,(-1L),0x1E8A2A1DL,1L,0xE993BB80L},{1L,1L,0x0FC69F04L,0x751B6326L,0x0FC69F04L,1L,1L,0x0FC69F04L,0x751B6326L},{(-1L),0x2DB2ECF3L,(-1L),(-1L),1L,3L,0x1E8A2A1DL,3L,1L},{0x1B8C11F2L,0x0FC69F04L,0x0FC69F04L,0x1B8C11F2L,0L,0x1B8C11F2L,0x0FC69F04L,0x0FC69F04L,0x1B8C11F2L},{0xD6B59484L,(-1L),0xE993BB80L,(-1L),0xD6B59484L,0L,1L,1L,1L},{0x0FC69F04L,0L,0x751B6326L,0x751B6326L,0L,0x0FC69F04L,0L,0x751B6326L,0x751B6326L},{0xD6B59484L,0L,1L,1L,1L,0L,0xD6B59484L,(-1L),0xE993BB80L},{0x1B8C11F2L,0L,0x1B8C11F2L,0x0FC69F04L,0x0FC69F04L,0x1B8C11F2L,0L,0x1B8C11F2L,0x0FC69F04L}};
                        int i, j;
                        (*l_1136) ^= 0xA9813D91L;
                        l_1166[2][4][1] &= (((safe_lshift_func_int8_t_s_u((*l_1136), (safe_mod_func_int32_t_s_s((safe_unary_minus_func_uint32_t_u(((safe_unary_minus_func_int16_t_s((-3L))) | ((l_1275 , ((*l_1280) &= ((p_29 , (((*g_554) , (g_1276[0][2][1] , p_30)) , ((g_978 = (l_1279 = (l_1254 >= ((*l_1115) &= ((safe_rshift_func_uint16_t_u_s(0xA93DL, 14)) >= p_29))))) , &l_1208[1][1][1]))) == (void*)0))) , p_28)))), p_28)))) || l_1275) > g_130);
                        ++g_1289[2];
                    }
                    else
                    { /* block id: 554 */
                        return (**g_267);
                    }
                    l_1110 |= ((*l_1136) = (((safe_rshift_func_uint8_t_u_s((((*l_1294) = (p_30 , (*l_1136))) < (*l_1136)), 2)) == (safe_rshift_func_int32_t_s_u(((l_1169[1] , ((safe_div_func_int8_t_s_s(((*l_1123) = 0x06L), (safe_sub_func_uint64_t_u_u((safe_mul_func_uint64_t_u_u(((p_30 && (safe_sub_func_uint16_t_u_u((((*l_1308) = (((safe_mul_func_uint64_t_u_u((0xE0F393C4L < (((g_1307 , (p_30 & 0x4F39C58EL)) >= p_29) & p_30)), 1L)) > 0UL) != 0UL)) < 0x4A7069AC46359E36LL), l_1309[0][0]))) >= (*g_268)), 0x80F6C5131A2CA5CBLL)), 18446744073709551606UL)))) == (**g_267))) < l_1310), 24))) , p_28));
                }
                else
                { /* block id: 562 */
                    uint64_t l_1327 = 0x635A30AABB9D076FLL;
                    struct S1 **l_1328 = &g_384;
                    int64_t *l_1330 = &g_436;
                    int64_t *l_1332 = &g_18;
                    l_1161 &= (((safe_div_func_int64_t_s_s(0x7BE0145F364D9E9DLL, ((*l_1332) = (l_1169[1] = (g_1331 = ((*l_1330) &= ((((*l_1136) = (g_63 >= ((*l_1115) = ((((l_1169[0] || ((0xC4385C7EL || p_28) , (p_29 = 0x1357C7A0L))) >= ((g_978++) && ((safe_unary_minus_func_int16_t_s((safe_mod_func_int16_t_s_s(((safe_unary_minus_func_uint8_t_u((**g_931))) <= (((((void*)0 == g_1321[7][0]) , (-8L)) & (**g_1059)) || 0x4FCB20E6L)), l_1327)))) > p_28))) <= 0x4D5647C7DE89136ALL) != 0x4827F0CCEC796FC3LL)))) , l_1328) != l_1329[6][0][0]))))))) != 0x8C50L) ^ p_30);
                }
            }
            else
            { /* block id: 573 */
                int16_t *l_1343 = &l_1094;
                int16_t *l_1344 = &l_1168;
                int32_t l_1358[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
                int16_t l_1395 = (-1L);
                int i;
                if (p_28)
                    break;
                (**g_1324) &= (((*l_1344) = ((*l_1343) ^= ((**g_769) = (safe_mul_func_int64_t_s_s((safe_add_func_uint32_t_u_u(1UL, (safe_lshift_func_uint32_t_u_s(2UL, 31)))), ((safe_div_func_int32_t_s_s(l_1254, (safe_mod_func_int32_t_s_s(p_29, p_28)))) <= (&g_9 != (void*)0))))))) == (((1L || (0xEFEF9AA1L == 0x6E1E4EEEL)) > p_28) > (-1L)));
                for (l_1209 = 0; (l_1209 <= 1); l_1209 += 1)
                { /* block id: 581 */
                    int32_t l_1347[2][5];
                    int32_t l_1359[4][2] = {{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)}};
                    uint8_t *l_1360 = &l_1048[2][1];
                    int8_t l_1365 = 0x6FL;
                    int32_t ****l_1417 = &g_1230[1][6];
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_1347[i][j] = (-4L);
                    }
                    if ((safe_mul_func_int8_t_s_s((l_1110 > ((((((***g_266) , (l_1347[1][1] ^= p_30)) > (l_1348 ^ ((safe_add_func_int32_t_s_s((((safe_mul_func_uint8_t_u_u(((*l_1360) ^= (g_88 > (safe_rshift_func_int8_t_s_s(p_29, (((safe_sub_func_uint64_t_u_u((((~(p_30 <= (1L ^ p_30))) > g_781[0].f1) & g_242[7].f4), l_1358[4])) < 0xA939L) != l_1359[1][0]))))), p_28)) >= 0x27AD068CL) <= 65530UL), (-5L))) ^ p_27))) < 4294967295UL) , g_1361) == (void*)0)), g_780.f9)))
                    { /* block id: 584 */
                        if (p_27)
                            break;
                        if (p_29)
                            continue;
                    }
                    else
                    { /* block id: 587 */
                        int32_t l_1364 = 9L;
                        uint32_t *l_1378 = &l_1249;
                        int32_t l_1391 = 0x1F488693L;
                        ++l_1366;
                        (****g_1322) = (l_1166[3][1][0] &= (((l_1359[3][1] |= l_1347[1][4]) > ((((*g_947) , (&g_932 == ((*l_1237) = &l_1360))) | (safe_add_func_uint64_t_u_u((safe_sub_func_int16_t_s_s(((safe_add_func_uint32_t_u_u(((((*l_1378) = (0x5B57FFB08DC68C99LL | (!p_27))) != (((*l_1123) = ((safe_unary_minus_func_int64_t_s((safe_mul_func_int64_t_s_s(0L, (((safe_lshift_func_int16_t_s_u(((*l_1344) |= p_28), ((safe_mod_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((l_1391 = (((safe_mul_func_int64_t_s_s(((((((!(-5L)) , p_27) <= p_27) && p_27) < p_28) < 0x6EL), p_27)) >= p_30) <= 0x1E75L)) >= l_1347[0][1]), l_1364)), p_27)) , p_28))) && 248UL) ^ (*g_268)))))) != g_1199.f0)) != l_1347[1][1])) < l_1358[3]), g_533.f5)) < p_30), (**g_769))), 18446744073709551615UL))) < p_30)) & p_30));
                        return p_29;
                    }
                    l_1166[3][1][0] &= g_1392;
                    for (l_1025 = 2; (l_1025 >= 0); l_1025 -= 1)
                    { /* block id: 602 */
                        int8_t l_1393 = 0xA1L;
                        int32_t l_1394[10][1] = {{(-1L)},{(-1L)},{0xA9BF00D0L},{0x4226E716L},{0xA9BF00D0L},{(-1L)},{(-1L)},{0xA9BF00D0L},{0x4226E716L},{0xA9BF00D0L}};
                        int8_t *l_1418 = (void*)0;
                        int8_t *l_1419[1][2];
                        uint16_t l_1421 = 1UL;
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_1419[i][j] = &l_1393;
                        }
                        g_1397--;
                        (****g_1322) = 0x4A5BA09EL;
                        l_1421 &= ((safe_lshift_func_int64_t_s_s((safe_div_func_uint16_t_u_u((((**g_267) = g_122[(l_1025 + 2)][(l_1025 + 4)][l_1209]) || (((g_122[(l_1025 + 1)][(l_1209 + 4)][l_1209] < ((void*)0 != &l_1233)) < (18446744073709551615UL > ((g_1420 = ((((*g_932) || ((*l_1360) &= (safe_mod_func_int8_t_s_s(((+(safe_rshift_func_int8_t_s_s((((**g_1059) >= ((l_1169[1] = (safe_lshift_func_int8_t_s_s((g_625.f1 <= ((safe_add_func_int8_t_s_s(((*l_1123) &= 3L), 0xF5L)) && l_1365)), g_459.f6))) >= p_27)) != (*g_268)), 5))) <= p_29), g_125.f1)))) , l_1415) != l_1417)) == g_623.f4))) | p_28)), (**g_769))), 18)) & p_29);
                    }
                    for (l_1159 = 0; (l_1159 <= 1); l_1159 += 1)
                    { /* block id: 614 */
                        uint8_t *l_1426 = (void*)0;
                        uint8_t *l_1427 = &g_978;
                        int32_t l_1428[1][10][5] = {{{(-6L),(-6L),(-6L),0x09AECA0EL,4L},{0x00B48A6DL,0L,0xE9716E49L,0x09AECA0EL,0xA8086E9CL},{6L,0x687D2C20L,0L,0x09AECA0EL,0x6ED7E73AL},{0x582502D5L,0xE9716E49L,1L,0x09AECA0EL,0x1FCCD315L},{(-3L),1L,0x687D2C20L,0x09AECA0EL,(-1L)},{0x09AECA0EL,(-6L),(-6L),0x09AECA0EL,4L},{0x00B48A6DL,0L,0xE9716E49L,0x09AECA0EL,0xA8086E9CL},{6L,0x687D2C20L,0L,0x09AECA0EL,0x6ED7E73AL},{0x582502D5L,0xE9716E49L,1L,0x09AECA0EL,0x1FCCD315L},{(-3L),1L,0x687D2C20L,0x09AECA0EL,(-1L)}}};
                        int i, j, k;
                        l_1428[0][5][1] |= (safe_mod_func_uint8_t_u_u(((*l_1427) = (++(*l_1360))), g_122[(l_1209 + 4)][(l_1209 + 3)][l_1209]));
                        (**g_1362) = g_1429;
                    }
                }
            }
            l_1430 = (0xCA3F2B19L | 0xB8CC8C6EL);
        }
        else
        { /* block id: 623 */
            int16_t l_1436[6][2][4] = {{{0xCC39L,0xCF5DL,0xCF5DL,0xCC39L},{0x5FBAL,(-7L),0x51AEL,0x98ABL}},{{(-7L),0xBBCEL,(-1L),0xCF5DL},{0x51AEL,0x1C9AL,0x1B1CL,0xCF5DL}},{{0x6592L,0xBBCEL,6L,0x98ABL},{0L,(-7L),0xB280L,0xCC39L}},{{0xFEC6L,0xCF5DL,0xFEC6L,0x5FBAL},{0x7E71L,0x51AEL,0L,(-7L)}},{{(-7L),(-1L),(-7L),0x51AEL},{6L,0x1B1CL,(-7L),0x6592L}},{{(-7L),6L,0L,0L},{0x7E71L,0xB280L,0xFEC6L,0xFEC6L}}};
            int32_t l_1448 = 4L;
            int32_t l_1475 = (-1L);
            struct S1 *l_1487 = &g_676;
            int32_t **l_1511 = (void*)0;
            int8_t l_1530 = (-5L);
            int64_t *l_1539 = &g_88;
            int i, j, k;
            for (l_1101 = 0; (l_1101 <= 1); l_1101 += 1)
            { /* block id: 626 */
                const struct S1 **l_1432 = &l_1431[3][7][4];
                int32_t **l_1447 = &g_39;
                uint32_t *l_1490 = (void*)0;
                uint32_t *l_1491 = &g_86;
                int8_t *l_1494 = &g_65[8];
                uint8_t *l_1495 = (void*)0;
                uint8_t *l_1496[6][8][5] = {{{&l_1048[0][1],&l_1048[0][1],(void*)0,&l_1048[2][1],&g_978},{&l_1048[1][1],&l_1048[3][1],&l_1048[0][1],&l_1048[2][1],(void*)0},{&l_1048[2][1],&l_1048[2][1],&l_1348,&l_1048[2][1],&l_1048[2][1]},{(void*)0,&l_1048[3][1],&l_1048[2][1],&l_1348,&g_978},{&l_1048[2][1],&l_1048[0][1],&l_1048[2][1],&l_1048[0][1],&l_1048[2][1]},{&l_1348,&l_1048[2][1],&g_978,&l_1348,(void*)0},{&l_1048[2][1],&g_978,&l_1348,&l_1048[2][1],&l_1048[2][1]},{&g_978,&l_1048[2][1],&g_978,&l_1048[2][1],(void*)0}},{{&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&l_1048[2][1]},{(void*)0,(void*)0,(void*)0,(void*)0,&g_978},{&g_978,(void*)0,&l_1048[2][1],&l_1048[2][1],&l_1048[2][1]},{(void*)0,&l_1348,&l_1048[2][0],&g_978,(void*)0},{&l_1048[2][1],&l_1348,&l_1048[2][1],&l_1048[2][1],&g_978},{&l_1048[2][1],&l_1348,(void*)0,&g_978,&l_1048[2][1]},{&l_1048[2][0],(void*)0,&l_1048[2][1],&g_978,&l_1348},{&l_1048[1][1],&g_978,&g_978,(void*)0,&l_1348}},{{&l_1048[2][1],&l_1348,&l_1348,&l_1048[2][1],&g_978},{&l_1048[1][1],&g_978,&g_978,&g_978,(void*)0},{&l_1048[2][0],&l_1048[2][1],&l_1048[2][1],&l_1048[2][0],&l_1048[2][1]},{(void*)0,(void*)0,&g_978,&l_1348,(void*)0},{&g_978,&l_1048[2][1],(void*)0,(void*)0,&l_1048[2][1]},{&l_1348,&g_978,&l_1048[1][1],&l_1348,&l_1348},{&l_1348,&l_1048[2][1],&l_1048[2][1],&l_1348,&l_1048[2][1]},{&g_978,&l_1048[2][1],&l_1048[2][0],&g_978,&l_1048[2][1]}},{{&l_1048[2][1],(void*)0,&l_1348,&g_978,&l_1348},{&l_1348,&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&g_978},{&l_1048[2][1],(void*)0,&l_1348,&l_1348,(void*)0},{(void*)0,&l_1048[2][1],&l_1348,&l_1048[2][1],(void*)0},{&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&l_1348},{(void*)0,&g_978,&l_1048[1][1],(void*)0,&l_1348},{&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&l_1048[2][1],&l_1348},{&g_978,(void*)0,(void*)0,&l_1048[3][1],(void*)0}},{{&l_1348,&g_978,&l_1348,&l_1048[2][1],(void*)0},{&l_1048[0][1],&l_1348,&l_1048[2][1],(void*)0,&g_978},{&g_978,(void*)0,&l_1048[2][1],&l_1048[2][1],&l_1348},{(void*)0,&l_1048[2][1],&l_1048[2][1],&g_978,&l_1048[2][1]},{&l_1048[0][1],&l_1048[2][1],&l_1348,&l_1348,&l_1048[2][1]},{&l_1048[2][1],&g_978,(void*)0,&g_978,&l_1348},{&l_1048[2][1],&l_1348,&l_1048[2][1],&g_978,&l_1048[2][1]},{&l_1048[2][1],&l_1348,&l_1048[1][1],&g_978,(void*)0}},{{&l_1048[2][1],&l_1348,&l_1048[2][1],&l_1048[0][1],&l_1048[2][1]},{&l_1048[2][1],&l_1048[2][1],&l_1348,(void*)0,(void*)0},{&l_1048[0][1],&l_1048[2][1],&l_1348,&l_1048[2][1],&l_1048[2][0]},{(void*)0,&l_1348,&l_1048[2][1],&g_978,(void*)0},{&g_978,&l_1048[2][1],&l_1348,&l_1048[2][1],&l_1348},{&l_1048[0][1],(void*)0,&l_1048[2][0],(void*)0,&l_1048[2][0]},{&l_1348,&l_1348,&l_1048[2][1],&l_1048[0][1],&l_1048[2][1]},{&g_978,&g_978,&l_1048[1][1],&g_978,&l_1048[2][1]}}};
                struct S2 *l_1509 = &g_1510;
                int32_t l_1523 = 0L;
                int32_t l_1526 = 0x69439DE7L;
                int64_t *l_1540[7] = {&l_1137,(void*)0,(void*)0,&l_1137,(void*)0,(void*)0,&l_1137};
                int i, j, k;
                (*l_1432) = l_1431[4][5][0];
                for (l_1025 = 1; (l_1025 >= 0); l_1025 -= 1)
                { /* block id: 630 */
                    int32_t *l_1435 = (void*)0;
                    int32_t l_1474 = 0x7F1B5925L;
                    int i, j, k;
                    (*g_1325) = g_122[(l_1025 + 4)][(l_1025 + 2)][l_1025];
                }
                if (p_29)
                    continue;
                if ((l_1448 = (((safe_sub_func_uint16_t_u_u(g_242[7].f2, (safe_mod_func_int8_t_s_s((((void*)0 == l_1487) || (((safe_mod_func_uint64_t_u_u(((**g_267) = (*g_268)), p_28)) >= (l_1497[0][0][4] |= (((*l_1494) = (((*l_1491) |= p_29) > ((*g_947) , ((*l_1471) &= (safe_rshift_func_uint64_t_u_s(p_29, 56)))))) != ((((-2L) >= 0xE7A6L) & p_30) < 0xDCC99FCB2939CBF4LL)))) > p_29)), 1UL)))) & p_28) <= g_1429.f0)))
                { /* block id: 669 */
                    struct S2 *l_1505 = &g_1506;
                    uint32_t l_1513[1];
                    int32_t l_1516 = 8L;
                    int32_t l_1521 = 0x40503A5BL;
                    int32_t l_1524[3];
                    int32_t l_1525 = 0x45CDA3B0L;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1513[i] = 0UL;
                    for (i = 0; i < 3; i++)
                        l_1524[i] = 0L;
                    for (l_1094 = 6; (l_1094 >= 2); l_1094 -= 1)
                    { /* block id: 672 */
                        int i, j;
                        (**l_1105) = &l_1475;
                        return (***g_266);
                    }
                    if ((safe_rshift_func_uint16_t_u_u(p_29, (l_1516 = (((safe_mul_func_uint8_t_u_u((((*l_1491) = ((((*l_1494) |= (p_28 | ((*g_268)--))) < ((g_978 ^= (*g_932)) >= (safe_unary_minus_func_int8_t_s((l_1505 != (l_1509 = l_1507)))))) <= ((((void*)0 == l_1511) || (!(((l_1513[0] , (((safe_add_func_int16_t_s_s((g_119 || p_27), g_642.f4)) == 0x08CBD755E0BA2A70LL) , &p_29)) == (void*)0) , p_29))) == p_27))) && p_27), p_29)) <= p_28) != 2L)))))
                    { /* block id: 682 */
                        int32_t l_1520 = 5L;
                        int32_t l_1522[9][4][7] = {{{0L,(-1L),1L,0x8F8A017EL,0x8F8A017EL,1L,(-1L)},{0xE7390E9CL,(-7L),1L,0L,7L,0x8F8A017EL,(-2L)},{1L,1L,0x016FA79EL,7L,9L,(-1L),1L},{1L,7L,(-2L),0L,0x016FA79EL,0x016FA79EL,0L}},{{1L,1L,1L,0x8F8A017EL,0x016FA79EL,0L,0xE7390E9CL},{1L,(-1L),0x67E51288L,(-6L),1L,0L,(-1L)},{(-6L),0x67E51288L,1L,0L,9L,(-1L),(-1L)},{7L,7L,(-6L),7L,7L,(-7L),(-1L)}},{{(-1L),0x016FA79EL,(-2L),1L,(-1L),1L,(-1L)},{(-2L),(-7L),0L,0x323D217DL,0xE7390E9CL,7L,0xF1936EE8L},{(-1L),1L,0L,0xF1936EE8L,0L,1L,(-1L)},{7L,1L,0x8F8A017EL,(-1L),7L,(-1L),(-6L)}},{{(-6L),(-7L),1L,0x8F8A017EL,0xF1936EE8L,(-1L),0x016FA79EL},{0L,0x016FA79EL,0x8F8A017EL,1L,1L,1L,0x8F8A017EL},{7L,7L,0L,1L,0x67E51288L,(-2L),0xE7390E9CL},{1L,0x67E51288L,0L,0x8F8A017EL,(-6L),0x016FA79EL,(-7L)}},{{0x323D217DL,1L,(-2L),(-1L),0x67E51288L,1L,0x67E51288L},{0xF1936EE8L,(-6L),(-6L),0xF1936EE8L,1L,1L,1L},{(-1L),(-2L),1L,0x323D217DL,0xF1936EE8L,0x016FA79EL,1L},{0x8F8A017EL,0L,0x67E51288L,1L,7L,(-2L),1L}},{{1L,0L,7L,7L,0L,1L,0x67E51288L},{1L,0x8F8A017EL,0x016FA79EL,0L,0xE7390E9CL,(-1L),(-7L)},{0x8F8A017EL,1L,(-7L),(-6L),(-1L),(-1L),0xE7390E9CL},{(-1L),0x8F8A017EL,1L,7L,7L,1L,0x8F8A017EL}},{{0xF1936EE8L,0L,1L,(-1L),9L,7L,0x016FA79EL},{0x323D217DL,0L,(-7L),(-2L),1L,1L,(-6L)},{1L,(-2L),0x016FA79EL,(-1L),(-7L),(-7L),(-1L)},{7L,(-6L),7L,7L,(-7L),(-1L),0xF1936EE8L}},{{0L,1L,0x67E51288L,(-6L),1L,0L,(-1L)},{(-6L),0x67E51288L,1L,0L,9L,(-1L),(-1L)},{7L,7L,(-6L),7L,7L,(-7L),(-1L)},{(-1L),0x016FA79EL,(-2L),1L,(-1L),1L,(-1L)}},{{(-2L),(-7L),0L,0x323D217DL,0xF1936EE8L,(-2L),(-1L)},{1L,(-6L),1L,(-1L),1L,(-6L),1L},{(-2L),(-6L),7L,0x8F8A017EL,9L,1L,0L},{0L,0L,0x323D217DL,7L,(-1L),0x67E51288L,(-7L)}}};
                        int i, j, k;
                        g_1519 = g_1517;
                        --l_1527;
                        if (l_1530)
                            break;
                    }
                    else
                    { /* block id: 686 */
                        volatile int32_t * volatile l_1531[2][5][4] = {{{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2}},{{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2},{(void*)0,&g_2,(void*)0,&g_2}}};
                        const int64_t *l_1545 = &g_88;
                        const int64_t *l_1548 = (void*)0;
                        int i, j, k;
                        (*g_1324) = (*g_1324);
                        l_1531[0][0][2] = (**g_1323);
                        (*g_1551) = ((safe_sub_func_uint32_t_u_u((safe_add_func_int32_t_s_s((safe_lshift_func_uint64_t_u_u((l_1516 = l_1521), (g_1538 , ((l_1540[6] = l_1539) == l_1539)))), 0L)), ((safe_mod_func_uint8_t_u_u(p_27, 246UL)) | (safe_mod_func_uint32_t_u_u(((l_1545 != (l_1548 = (((safe_lshift_func_int16_t_s_s((((*l_1491) = g_533.f4) && 0xDC3DDF3AL), 4)) & 0xAD3FL) , (void*)0))) & p_30), l_1549))))) , g_1550[3][1]);
                    }
                }
                else
                { /* block id: 695 */
                    return p_27;
                }
            }
        }
        if (p_27)
            break;
        g_1553--;
    }
    for (g_15 = 0; (g_15 != 35); g_15 = safe_add_func_int64_t_s_s(g_15, 5))
    { /* block id: 705 */
        const int8_t *l_1563 = &g_65[3];
        int32_t l_1570 = 0x6256A46BL;
        l_1570 |= ((safe_lshift_func_int64_t_s_u(((((((((safe_lshift_func_int16_t_s_s((l_1562 &= ((*g_770) ^= (**g_739))), 8)) > (l_1563 == (p_29 , l_1563))) & (((0L == ((0xAEL & (safe_mul_func_uint64_t_u_u((*g_268), (safe_mod_func_int8_t_s_s((-1L), (safe_div_func_uint8_t_u_u((((***g_396) != 0x9CA9A45EL) , 247UL), p_30))))))) >= p_27)) , 1UL) == 4294967295UL)) , (void*)0) == (void*)0) , 0x428ED6FAC5964465LL) < 1L) , 0L), p_28)) > p_28);
    }
    l_1599[0][6] |= ((((((((**g_267) |= l_1571) && (safe_mul_func_int8_t_s_s((safe_rshift_func_int32_t_s_s(((~l_1577) < ((*g_1469) != (safe_add_func_uint16_t_u_u((safe_rshift_func_uint64_t_u_u((((((safe_mul_func_int8_t_s_s(3L, (1UL <= (g_185.f9 = (((p_29 || ((safe_lshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s(g_125.f0, ((*l_1471) > (+((safe_sub_func_int32_t_s_s((safe_sub_func_int16_t_s_s((safe_rshift_func_int64_t_s_u((safe_rshift_func_int32_t_s_u(p_29, p_28)), p_28)), p_27)), 0xAA6C4300L)) != g_533.f4))))), 6)) , p_30)) , (*g_268)) , p_27))))) ^ g_625.f9) , p_29) || 0x2D6163FBL) == 0xBD26E53FL), 59)), 0UL)))), 0)), 0xDEL))) & p_27) <= l_1598) ^ g_1331) && p_28) <= 1L);
    for (l_1430 = 0; (l_1430 <= 1); l_1430 += 1)
    { /* block id: 715 */
        int32_t l_1600 = (-7L);
        return l_1600;
    }
    return (*g_268);
}


/* ------------------------------------------ */
/* 
 * reads : g_13 g_39 g_40 g_15 g_2 g_63 g_65 g_88 g_9 g_95 g_108 g_111 g_112 g_115 g_122 g_125 g_130 g_117 g_18 g_149 g_296 g_297 g_319 g_344 g_119 g_270 g_384 g_385.f1 g_159.f0 g_185.f6 g_268 g_83 g_159 g_395 g_396 g_397 g_398 g_242.f0 g_121 g_459 g_242.f2 g_385.f3 g_185.f0 g_185.f5 g_86 g_185.f4 g_266 g_267 g_551 g_554 g_533.f5 g_272 g_160 g_161 g_533.f2 g_385.f0 g_534.f5 g_623.f8 g_642 g_643 g_242.f3 g_676 g_682 g_242.f6 g_686 g_687 g_185.f1 g_715 g_623.f0 g_625.f6 g_298 g_739 g_742 g_740 g_741 g_769 g_770 g_783 g_623.f6 g_625.f0 g_624.f6 g_865 g_872 g_873 g_781.f4 g_896 g_904 g_623.f4 g_919 g_931 g_946 g_947 g_626.f1 g_957 g_958
 * writes: g_40 g_63 g_18 g_65 g_83 g_86 g_88 g_13 g_89 g_95 g_108 g_39 g_112 g_122 g_2 g_130 g_121 g_125.f0 g_298 g_319 g_179 g_395 g_551 g_436 g_161 g_149 g_687 g_703 g_626.f6 g_739 g_716 g_872 g_715 g_946
 */
static const uint32_t  func_31(uint16_t  p_32, int64_t * const  p_33, int32_t * p_34, uint16_t * p_35, uint16_t  p_36)
{ /* block id: 9 */
    uint64_t l_126 = 0x0B1BDB0722B24768LL;
    uint64_t *l_129 = &g_130;
    int32_t l_441[4];
    int32_t l_956[1];
    int i;
    for (i = 0; i < 4; i++)
        l_441[i] = (-1L);
    for (i = 0; i < 1; i++)
        l_956[i] = 9L;
    for (p_32 = 0; (p_32 != 32); ++p_32)
    { /* block id: 12 */
        return g_13;
    }
    (*g_39) &= 0L;
    (*p_34) = (((((l_956[0] ^= (func_43(func_46(((l_126 = ((func_48(g_15) , (-1L)) & g_125.f0)) > (safe_mod_func_uint8_t_u_u(((((((((*l_129) &= 0UL) | 0xC8BCB830702C5E9DLL) || 0xFF8E19E864CEE9C7LL) > 0x59B4EE9BL) , (g_15 || p_32)) == (*p_35)) <= g_125.f9), g_117[6])))), l_441[3]) , l_126)) , g_957[3]) , 1L) , g_958) , 0L);
    return p_36;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int64_t * const  func_37(int32_t * p_38)
{ /* block id: 7 */
    return &g_18;
}


/* ------------------------------------------ */
/* 
 * reads : g_242.f0 g_121 g_459 g_268 g_83 g_130 g_242.f2 g_385.f3 g_185.f0 g_185.f5 g_86 g_185.f4 g_18 g_88 g_63 g_149.f2 g_122 g_125.f8 g_117 g_149.f0 g_149.f3 g_13 g_266 g_267 g_344 g_65 g_125.f0 g_9 g_551 g_554 g_149.f6 g_533.f5 g_272 g_111 g_39 g_160 g_161 g_112 g_533.f2 g_385.f0 g_534.f5 g_270 g_623.f8 g_642 g_643 g_242.f3 g_676 g_682 g_242.f6 g_396 g_397 g_398 g_686 g_687 g_185.f1 g_125.f5 g_715 g_623.f0 g_185.f6 g_625.f6 g_297 g_298 g_739 g_742 g_740 g_741 g_769 g_770 g_40 g_125.f6 g_783 g_623.f6 g_625.f0 g_149.f4 g_296.f0 g_624.f6 g_865 g_872 g_873 g_95 g_781.f4 g_119 g_896 g_904 g_623.f4 g_319 g_919 g_931 g_946 g_947 g_626.f1
 * writes: g_83 g_86 g_125.f0 g_65 g_551 g_436 g_39 g_161 g_319 g_149 g_121 g_687 g_703 g_122 g_626.f6 g_739 g_88 g_179 g_89 g_18 g_63 g_716 g_872 g_715 g_946 g_13
 */
static int32_t  func_43(int64_t * p_44, int8_t  p_45)
{ /* block id: 186 */
    uint8_t l_454 = 0xEBL;
    int16_t *l_468 = &g_121;
    const uint16_t l_469 = 0x6D47L;
    int16_t l_470 = 0xB4C8L;
    int32_t l_471 = (-1L);
    uint32_t *l_472 = &g_86;
    const uint32_t l_493 = 18446744073709551615UL;
    int16_t l_503[9][2][3] = {{{1L,0x1E16L,0x146DL},{0L,0L,0x344BL}},{{0xC178L,(-2L),(-10L)},{0xA10FL,0L,0x8C9CL}},{{(-9L),0x7F10L,(-2L)},{0x8C9CL,0xA10FL,0x8C9CL}},{{2L,0x99D5L,(-10L)},{(-1L),0L,0x344BL}},{{0x7F10L,0xC178L,0x146DL},{0x43B9L,0x66BEL,0x66BEL}},{{0x7F10L,(-2L),(-9L)},{(-1L),(-1L),0L}},{{2L,(-9L),0x7F10L},{0x8C9CL,0x344BL,0x6D35L}},{{(-9L),(-9L),0x1E16L},{0xA10FL,(-1L),0x3954L}},{{0xC178L,(-2L),1L},{0L,0x66BEL,(-1L)}}};
    int64_t l_510 = (-9L);
    int32_t *l_555 = &l_471;
    int32_t l_561 = 0xA19E48F5L;
    int64_t * const l_581[5][1][10] = {{{&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0}},{{&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0}},{{&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0}},{{&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0}},{{&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0}}};
    uint32_t l_582[7][4][1] = {{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}},{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}},{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}},{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}},{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}},{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}},{{0UL},{0xD558BEE2L},{0UL},{0xD558BEE2L}}};
    struct S2 *l_622[10] = {&g_626,(void*)0,&g_624,(void*)0,&g_626,&g_626,(void*)0,&g_624,(void*)0,&g_626};
    struct S2 **l_621 = &l_622[6];
    int32_t l_663 = 1L;
    uint8_t l_664 = 0x52L;
    int32_t ****l_674 = (void*)0;
    uint32_t l_729 = 0xB85782A0L;
    int32_t l_747 = (-5L);
    int32_t l_748 = 0xE213A353L;
    int32_t l_749[1];
    int8_t l_751 = 0xD0L;
    int32_t l_846[9] = {0L,0x1C48D715L,0x1C48D715L,0L,0x1C48D715L,0x1C48D715L,0L,0x1C48D715L,0x1C48D715L};
    int64_t l_859 = 0L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_749[i] = (-1L);
    if ((safe_rshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_u(0L, (((*l_472) = (((safe_mul_func_int8_t_s_s(g_242[7].f0, ((((safe_sub_func_uint16_t_u_u((((safe_rshift_func_uint8_t_u_s(g_121, (0x6AL == l_454))) && (l_471 = (((((safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s((g_459 , (safe_div_func_uint64_t_u_u((0x6DL < (safe_sub_func_int32_t_s_s(((((((~((+l_454) | ((safe_rshift_func_uint64_t_u_s(((*g_268) ^= (l_468 != l_468)), g_130)) | g_242[7].f2))) || l_469) <= 0UL) > g_385.f3) > g_185.f0) < l_469), g_242[7].f2))), l_454))), 10)), l_470)) , p_45) | 0L) != l_469) < l_469))) , p_45), g_185.f5)) >= l_454) || g_86) , p_45))) , g_185.f4) >= (*p_44))) < 0xD26710AEL))), g_149.f2)))
    { /* block id: 190 */
        uint8_t l_473 = 0x0FL;
        int32_t *l_483[9][1][8] = {{{&g_40,(void*)0,&g_40,&l_471,&l_471,&g_40,(void*)0,&g_40}},{{(void*)0,(void*)0,&l_471,&g_40,&g_40,&l_471,&g_40,&g_40}},{{&l_471,&l_471,(void*)0,&l_471,&g_40,&l_471,(void*)0,&l_471}},{{&g_40,(void*)0,&l_471,&l_471,(void*)0,&g_40,&l_471,&g_40}},{{&l_471,(void*)0,(void*)0,(void*)0,&l_471,&g_40,(void*)0,&l_471}},{{(void*)0,(void*)0,(void*)0,&g_40,&g_40,&l_471,&l_471,(void*)0}},{{&l_471,&l_471,(void*)0,(void*)0,(void*)0,&l_471,(void*)0,&l_471}},{{&g_40,&l_471,(void*)0,&l_471,(void*)0,(void*)0,&l_471,(void*)0}},{{&g_40,&g_40,&l_471,(void*)0,&l_471,(void*)0,(void*)0,(void*)0}}};
        uint32_t l_490 = 0x4F5BB740L;
        int32_t ***l_494 = &g_349;
        int64_t l_543[4] = {0xA881CBF21CBE5820LL,0xA881CBF21CBE5820LL,0xA881CBF21CBE5820LL,0xA881CBF21CBE5820LL};
        const uint64_t l_544 = 18446744073709551608UL;
        uint8_t l_571 = 0xA4L;
        int64_t *l_576[1][9] = {{&g_88,&g_88,&g_88,&g_88,&g_88,&g_88,&g_88,&g_88,&g_88}};
        const uint64_t ***l_577 = (void*)0;
        int8_t l_586 = (-1L);
        int16_t l_651 = 0x9202L;
        uint32_t l_692[7] = {6UL,6UL,6UL,6UL,6UL,6UL,6UL};
        uint32_t l_829 = 0x5DEB9771L;
        struct S1 **l_851 = &g_384;
        uint8_t *l_929[6][9] = {{&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664},{&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571},{&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664},{&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571},{&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664,&l_664},{&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571,&l_571}};
        uint8_t **l_928[2];
        uint64_t ****l_935 = &g_703[5][0][1];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_928[i] = &l_929[2][0];
        for (l_470 = 0; (l_470 <= 1); l_470 += 1)
        { /* block id: 193 */
            int32_t *l_480 = (void*)0;
            int32_t *l_481 = &l_471;
            int32_t **l_482 = &l_480;
            int32_t l_549 = 0x3026D93EL;
            int32_t l_559 = 0xA123DC11L;
            int32_t l_560 = 0x21C82B79L;
            uint16_t l_580[2];
            int16_t l_585 = 1L;
            int32_t l_631 = (-10L);
            int32_t l_656 = 0xCFDCEEF3L;
            int32_t l_658 = 0x57473C8DL;
            int32_t ****l_677[6][3][4] = {{{&l_494,(void*)0,&l_494,(void*)0},{(void*)0,(void*)0,&l_494,&l_494},{&l_494,&l_494,(void*)0,&l_494}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_494,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_494,&l_494,&l_494,(void*)0}},{{&l_494,(void*)0,(void*)0,&l_494},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_494,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{&l_494,&l_494,&l_494,(void*)0},{&l_494,(void*)0,(void*)0,&l_494}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_494,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}}};
            uint64_t ***l_688 = (void*)0;
            int64_t ***l_714 = (void*)0;
            int64_t l_750 = (-6L);
            int32_t l_752 = (-1L);
            uint32_t l_753 = 18446744073709551612UL;
            int16_t **l_768 = (void*)0;
            int16_t l_773 = 0xD0B5L;
            uint64_t l_799 = 8UL;
            uint64_t l_830[8];
            uint16_t l_862 = 0xAD8EL;
            int16_t ****l_866 = (void*)0;
            int16_t ***l_869 = &l_768;
            int16_t ****l_868 = &l_869;
            int32_t l_887 = 0xF06473D8L;
            struct S2 *l_925 = &g_926;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_580[i] = 0x253BL;
            for (i = 0; i < 8; i++)
                l_830[i] = 0x6B72D1B8F337D519LL;
            if ((18446744073709551615UL == ((l_469 , l_473) == (((p_45 > l_454) <= 0L) , (((*l_482) = (((*l_481) = (safe_lshift_func_uint8_t_u_s((safe_add_func_uint8_t_u_u((((g_122[4][6][1] <= (safe_mod_func_uint8_t_u_u(((l_471 , (4294967287UL != g_125.f8)) != g_242[7].f0), g_117[6]))) == l_473) < 4294967295UL), g_149.f0)), l_473))) , (void*)0)) == (void*)0)))))
            { /* block id: 196 */
                uint64_t l_504 = 0x0C3E78EBC6D4E48CLL;
                int32_t l_505 = 4L;
                int32_t l_506[3];
                uint64_t ***l_578 = &g_267;
                int i;
                for (i = 0; i < 3; i++)
                    l_506[i] = 0xE898E9E7L;
                (*l_482) = l_483[2][0][4];
                if ((safe_mul_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((l_469 , (safe_lshift_func_uint16_t_u_s((p_45 || l_490), 8))), ((((safe_div_func_uint8_t_u_u(l_493, (-1L))) > ((void*)0 != l_494)) <= (safe_sub_func_uint64_t_u_u((+(((safe_sub_func_uint32_t_u_u((((safe_rshift_func_int8_t_s_u((+(g_149.f3 != l_503[7][0][0])), 5)) <= 0x86L) || 9L), p_45)) == g_13) > (-10L))), l_504))) || 0x636F0699L))), (-1L))))
                { /* block id: 198 */
                    uint8_t l_507 = 0xCFL;
                    struct S1 *l_532[9] = {&g_533,&g_533,&g_533,&g_533,&g_533,&g_533,&g_533,&g_533,&g_533};
                    uint8_t *l_545 = &l_507;
                    int64_t l_546 = (-5L);
                    int i;
                    l_507--;
                    l_505 = ((l_510 ^ ((**g_266) == p_44)) | (l_493 || ((+((*l_472)--)) >= (safe_mod_func_uint32_t_u_u(((safe_rshift_func_uint32_t_u_s(((g_344 >= ((*l_545) = (safe_mod_func_uint16_t_u_u(1UL, (safe_div_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((g_125.f0 &= (l_543[2] = (safe_add_func_int8_t_s_s(((safe_div_func_int8_t_s_s((safe_div_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_u(((void*)0 == l_532[8]), (safe_add_func_int32_t_s_s(((*l_481) = (((safe_add_func_uint64_t_u_u(((**g_267) = (safe_rshift_func_uint64_t_u_s((safe_lshift_func_uint64_t_u_s(((9UL <= g_65[4]) && g_185.f4), g_242[7].f2)), 16))), 0x3552ED8A79831960LL)) <= p_45) == l_507)), l_454)))) < g_65[8]), p_45)), 0x23L)) ^ g_65[1]), 2L)))) , (-7L)), l_544)), 254UL)))))) != g_9), 18)) != g_130), l_546)))));
                }
                else
                { /* block id: 207 */
                    int8_t *l_547 = (void*)0;
                    int8_t *l_548 = &g_65[8];
                    int32_t l_558 = 0xF05F94E3L;
                    int64_t *l_565 = &g_436;
                    int64_t **l_566 = &l_565;
                    uint64_t ****l_579 = &l_578;
                    if (((((*l_548) = l_493) ^ l_549) || g_149.f2))
                    { /* block id: 209 */
                        volatile struct S0 *l_552 = (void*)0;
                        (*l_481) = (~p_45);
                        (*g_554) = g_551;
                    }
                    else
                    { /* block id: 212 */
                        (*l_482) = l_555;
                        if ((**l_482))
                            continue;
                    }
                    for (g_436 = 0; (g_436 <= 1); g_436 += 1)
                    { /* block id: 218 */
                        uint16_t l_562[8][9][3] = {{{1UL,1UL,1UL},{1UL,0x9011L,0xB481L},{0xD6CBL,2UL,0xB481L},{0UL,0x9045L,1UL},{0xE345L,1UL,0xC52EL},{0x9045L,0x9045L,0x0AB9L},{0UL,2UL,0xFA2DL},{0UL,0x9011L,0xFC4CL},{0x9045L,1UL,0x7449L}},{{0xE345L,0UL,0xFC4CL},{0UL,65535UL,0xFA2DL},{0xD6CBL,65535UL,0x0AB9L},{1UL,0UL,0xC52EL},{1UL,1UL,1UL},{1UL,0x9011L,0xB481L},{0xD6CBL,2UL,0xB481L},{0UL,0x9045L,1UL},{0xE345L,1UL,0xC52EL}},{{0x9045L,0x9045L,0x0AB9L},{0UL,2UL,0xFA2DL},{0UL,0x9011L,0xFC4CL},{0x9045L,1UL,0x7449L},{0xE345L,0UL,0xFC4CL},{0UL,65535UL,0xFA2DL},{0xD6CBL,65535UL,0x0AB9L},{1UL,0UL,0xC52EL},{1UL,1UL,1UL}},{{1UL,0x9011L,0xB481L},{0xD6CBL,2UL,0xB481L},{0UL,0x9045L,1UL},{0xE345L,1UL,0xC52EL},{0x9045L,0x9045L,0x0AB9L},{0UL,2UL,0xFA2DL},{0UL,0x9011L,0xFC4CL},{0x9045L,1UL,0x7449L},{0xE345L,0UL,0xFC4CL}},{{0UL,65535UL,0xFA2DL},{0xD6CBL,65535UL,0x0AB9L},{1UL,0UL,0xC52EL},{1UL,1UL,1UL},{1UL,0x9011L,0xB481L},{0xD6CBL,2UL,0xB481L},{0UL,0x9045L,1UL},{0xE345L,0x8A73L,0xD6CBL},{8UL,8UL,0UL}},{{0xB21BL,0xA9E9L,0UL},{0xB21BL,0xE125L,2UL},{8UL,0x5046L,1UL},{0x4A85L,0xB21BL,2UL},{65531UL,1UL,0UL},{0UL,1UL,0UL},{0x5046L,0xB21BL,0xD6CBL},{0UL,0x5046L,1UL},{0x5046L,0xE125L,1UL}},{{0UL,0xA9E9L,1UL},{65531UL,8UL,1UL},{0x4A85L,0x8A73L,0xD6CBL},{8UL,8UL,0UL},{0xB21BL,0xA9E9L,0UL},{0xB21BL,0xE125L,2UL},{8UL,0x5046L,1UL},{0x4A85L,0xB21BL,2UL},{65531UL,1UL,0UL}},{{0UL,1UL,0UL},{0x5046L,0xB21BL,0xD6CBL},{0UL,0x5046L,1UL},{0x5046L,0xE125L,1UL},{0UL,0xA9E9L,1UL},{65531UL,8UL,1UL},{0x4A85L,0x8A73L,0xD6CBL},{8UL,8UL,0UL},{0xB21BL,0xA9E9L,0UL}}};
                        int i, j, k;
                        (*l_481) = (l_558 ^= (safe_sub_func_uint8_t_u_u(0x97L, (((0xF0D5L < 65534UL) , (**g_266)) == (*g_267)))));
                        (*l_482) = &l_505;
                        ++l_562[2][6][2];
                    }
                    if ((((*l_566) = l_565) == ((safe_div_func_uint64_t_u_u((safe_mod_func_int64_t_s_s(l_571, ((safe_mul_func_uint8_t_u_u(p_45, ((safe_mul_func_int16_t_s_s(((((l_506[0] = ((void*)0 != l_576[0][7])) != ((l_555 = ((*l_482) = &l_471)) != &l_561)) || (l_577 == ((*l_579) = l_578))) <= g_149.f6), g_533.f5)) == l_580[1]))) ^ p_45))), p_45)) , l_581[3][0][6])))
                    { /* block id: 229 */
                        return p_45;
                    }
                    else
                    { /* block id: 231 */
                        (*g_272) = (*l_482);
                        if ((**g_111))
                            break;
                        (*g_160) = (*g_160);
                        if (l_505)
                            continue;
                    }
                    --l_582[0][1][0];
                }
                return l_585;
            }
            else
            { /* block id: 240 */
                uint8_t l_587 = 253UL;
                l_587--;
            }
            for (l_510 = 7; (l_510 >= 0); l_510 -= 1)
            { /* block id: 245 */
                uint16_t l_591 = 65535UL;
                uint64_t ** const *l_618 = &g_267;
                uint64_t ** const **l_617 = &l_618;
                for (g_319 = 0; (g_319 <= 2); g_319 += 1)
                { /* block id: 248 */
                    int32_t l_590 = 0xAA742B33L;
                    uint8_t *l_602 = (void*)0;
                    uint8_t *l_603 = &l_473;
                    int i, j, k;
                    l_591--;
                    l_590 ^= ((safe_lshift_func_uint32_t_u_u(0x5C5C6906L, (safe_div_func_int64_t_s_s(((safe_sub_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((--(*l_603)), (g_242[7].f2 >= (safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_s(((safe_unary_minus_func_uint8_t_u(0x28L)) || g_112), 13)), ((*l_481) = ((((safe_sub_func_int16_t_s_s(((safe_sub_func_uint8_t_u_u((0x54L != ((*l_555) , (0xDCED1D32E5B735EBLL != (((safe_lshift_func_uint64_t_u_u((((void*)0 != l_617) <= g_533.f2), 36)) , g_161) != (void*)0)))), g_385.f0)) == p_45), p_45)) == 0x0FL) , 4UL) <= g_534.f5))))))), 1UL)) < (*g_268)), g_125.f0)))) && 1L);
                    if (((((*l_555) ^ ((0x562CL <= (g_270 != (p_45 , l_621))) && g_623.f8)) , ((safe_sub_func_int32_t_s_s(p_45, (safe_add_func_uint64_t_u_u(((((0x3ADAD5D69AFFEA12LL >= 2UL) ^ (*p_44)) ^ l_631) == 0xD4L), 1L)))) && p_45)) & (-1L)))
                    { /* block id: 253 */
                        uint16_t l_632 = 0x0DE7L;
                        const int32_t *l_635[2][3] = {{(void*)0,(void*)0,(void*)0},{&l_561,&l_561,&l_561}};
                        const int32_t **l_636 = &l_635[0][2];
                        int i, j;
                        l_632--;
                        (*l_636) = l_635[0][2];
                    }
                    else
                    { /* block id: 256 */
                        int32_t l_637 = (-10L);
                        int32_t l_638 = 0xE27FAAD7L;
                        uint8_t l_639 = 7UL;
                        l_639++;
                        if (p_45)
                            break;
                        (*g_643) = g_642;
                    }
                    (*l_481) ^= 0x1BED9AFAL;
                }
                return g_242[7].f3;
            }
            for (l_586 = 0; (l_586 <= 1); l_586 += 1)
            { /* block id: 267 */
                int8_t l_648 = 9L;
                int32_t l_649 = 4L;
                int32_t l_650[6][9][4] = {{{1L,(-6L),0xC40E42EBL,(-1L)},{0x8962FBE8L,(-1L),0L,1L},{0x8962FBE8L,(-1L),0xC40E42EBL,7L},{1L,1L,(-1L),(-3L)},{5L,0xBF80183BL,5L,(-3L)},{(-1L),1L,1L,7L},{0xC40E42EBL,(-1L),0x8962FBE8L,1L},{0L,(-1L),0x8962FBE8L,(-1L)},{0xC40E42EBL,(-6L),1L,(-1L)}},{{(-1L),0xA091E2DAL,5L,0xA091E2DAL},{5L,0xA091E2DAL,(-1L),(-1L)},{1L,(-6L),0xC40E42EBL,(-1L)},{0x8962FBE8L,(-1L),0L,1L},{0x8962FBE8L,(-1L),0xC40E42EBL,7L},{1L,1L,(-1L),(-3L)},{5L,0xBF80183BL,5L,(-3L)},{(-1L),1L,1L,7L},{0xC40E42EBL,(-1L),0x8962FBE8L,1L}},{{0L,(-1L),0x8962FBE8L,(-1L)},{0xC40E42EBL,(-6L),1L,(-1L)},{(-1L),0xA091E2DAL,5L,0xA091E2DAL},{5L,0xA091E2DAL,(-1L),(-1L)},{1L,(-6L),0xC40E42EBL,(-1L)},{0x8962FBE8L,(-1L),0L,1L},{0x8962FBE8L,(-1L),0xC40E42EBL,(-6L)},{0L,(-1L),0xC40E42EBL,0xA091E2DAL},{(-9L),7L,(-9L),0xA091E2DAL}},{{0xC40E42EBL,(-1L),0L,(-6L)},{5L,1L,0L,(-1L)},{0xA8C5668AL,(-3L),0L,(-1L)},{5L,0x46896D9DL,0L,1L},{0xC40E42EBL,0xBF80183BL,(-9L),0xBF80183BL},{(-9L),0xBF80183BL,0xC40E42EBL,1L},{0L,0x46896D9DL,5L,(-1L)},{0L,(-3L),0xA8C5668AL,(-1L)},{0L,1L,5L,(-6L)}},{{0L,(-1L),0xC40E42EBL,0xA091E2DAL},{(-9L),7L,(-9L),0xA091E2DAL},{0xC40E42EBL,(-1L),0L,(-6L)},{5L,1L,0L,(-1L)},{0xA8C5668AL,(-3L),0L,(-1L)},{5L,0x46896D9DL,0L,1L},{0xC40E42EBL,0xBF80183BL,(-9L),0xBF80183BL},{(-9L),0xBF80183BL,0xC40E42EBL,1L},{0L,0x46896D9DL,5L,(-1L)}},{{0L,(-3L),0xA8C5668AL,(-1L)},{0L,1L,5L,(-6L)},{0L,(-1L),0xC40E42EBL,0xA091E2DAL},{(-9L),7L,(-9L),0xA091E2DAL},{0xC40E42EBL,(-1L),0L,(-6L)},{5L,1L,0L,(-1L)},{0xA8C5668AL,(-3L),0L,(-1L)},{5L,0x46896D9DL,0L,1L},{0xC40E42EBL,0xBF80183BL,(-9L),0xBF80183BL}}};
                struct S0 **l_708[4][8] = {{&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161},{(void*)0,&g_161,&g_161,(void*)0,&g_161,(void*)0,&g_161,(void*)0},{&g_161,(void*)0,&g_161,&g_161,&g_161,(void*)0,&g_161,(void*)0},{&g_161,(void*)0,&g_161,(void*)0,(void*)0,(void*)0,(void*)0,&g_161}};
                const int64_t *l_736 = &l_543[3];
                int32_t l_744 = 0x4E244E56L;
                int32_t *****l_792 = &l_677[3][1][1];
                uint16_t *l_824 = &g_122[4][6][1];
                uint16_t *l_827[6][6][5] = {{{&g_9,&l_580[1],(void*)0,&l_580[1],&g_319},{(void*)0,&g_15,(void*)0,&l_580[1],&g_319},{&g_13,&g_15,&g_13,&l_580[1],&g_9},{&l_580[1],&l_580[1],&g_13,&l_580[1],&g_15},{&g_319,(void*)0,&g_15,&l_580[1],&g_319},{&l_580[1],&l_580[1],&g_9,&l_580[1],&g_319}},{{&g_319,&g_13,(void*)0,&l_580[1],&l_580[0]},{&g_13,(void*)0,(void*)0,&l_580[1],&g_13},{&l_580[1],&l_580[1],&l_580[1],&g_15,&g_319},{&g_15,&g_319,&l_580[1],&g_13,&l_580[1]},{(void*)0,&g_319,&g_15,&g_15,&g_319},{(void*)0,&g_9,&g_319,&g_15,&l_580[1]}},{{&g_13,&g_319,&g_319,&g_13,&g_9},{&g_15,&l_580[1],&l_580[1],&g_15,&g_13},{(void*)0,&g_319,&l_580[1],&g_15,&l_580[1]},{&g_9,&g_319,&g_15,&g_13,&g_15},{(void*)0,&l_580[1],(void*)0,&g_15,(void*)0},{(void*)0,&g_319,(void*)0,&g_15,&g_9}},{{&g_13,&l_580[1],&l_580[1],&g_13,&g_319},{(void*)0,&g_15,&l_580[1],&g_15,(void*)0},{(void*)0,&g_319,&g_13,&g_15,&g_15},{&g_319,&g_9,(void*)0,&g_13,&l_580[1]},{(void*)0,(void*)0,&g_319,&g_15,&g_9},{&g_15,&l_580[1],&l_580[1],&g_15,&g_319}},{{&g_15,&g_319,&l_580[1],&g_13,&l_580[1]},{(void*)0,&g_319,&g_15,&g_15,&g_319},{(void*)0,&g_9,&g_319,&g_15,&l_580[1]},{&g_13,&g_319,&g_319,&g_13,&g_9},{&g_15,&l_580[1],&l_580[1],&g_15,&g_13},{(void*)0,&g_319,&l_580[1],&g_15,&l_580[1]}},{{&g_9,&g_319,&g_15,&g_13,&g_15},{(void*)0,&l_580[1],(void*)0,&g_15,(void*)0},{(void*)0,&g_319,(void*)0,&g_15,&g_9},{&g_13,&l_580[1],&l_580[1],&g_13,&g_319},{(void*)0,&g_15,&l_580[1],&g_15,(void*)0},{(void*)0,&g_319,&g_13,&g_15,&g_15}}};
                int8_t *l_828 = &g_65[8];
                uint8_t *l_831 = &l_454;
                int i, j, k;
                for (l_549 = 0; (l_549 <= 8); l_549 += 1)
                { /* block id: 270 */
                    int32_t l_645 = (-1L);
                    int32_t l_653 = 0x8480AFDFL;
                    int32_t l_654 = 0x2105FA08L;
                    int32_t l_655 = 0xBF79A86AL;
                    int32_t l_657 = (-10L);
                    int32_t l_659[4][7][2] = {{{2L,0x21A163D9L},{4L,1L},{4L,0x21A163D9L},{2L,0x6EB75884L},{0xEF73A879L,0x6EB75884L},{2L,0x21A163D9L},{4L,1L}},{{4L,0x21A163D9L},{2L,0x6EB75884L},{0xEF73A879L,0x6EB75884L},{2L,0x21A163D9L},{4L,1L},{4L,0x21A163D9L},{2L,0x6EB75884L}},{{0xEF73A879L,0x6EB75884L},{2L,0x21A163D9L},{4L,1L},{4L,0x21A163D9L},{2L,0x6EB75884L},{0xEF73A879L,0x6EB75884L},{2L,0x21A163D9L}},{{4L,1L},{4L,0x21A163D9L},{2L,0x6EB75884L},{0xEF73A879L,0x6EB75884L},{2L,0x21A163D9L},{0xEF73A879L,0x21A163D9L},{0xEF73A879L,0L}}};
                    int64_t l_678[1];
                    int32_t *l_683 = &l_663;
                    int16_t l_685 = 9L;
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_678[i] = 1L;
                    if (l_503[(l_470 + 2)][l_586][l_586])
                    { /* block id: 271 */
                        int16_t l_644 = 4L;
                        int32_t l_646 = (-1L);
                        int32_t l_647 = (-1L);
                        int32_t l_652[7][8][4] = {{{0xC1B82F2AL,0x8A91F534L,(-1L),(-1L)},{(-1L),(-1L),1L,(-1L)},{0x221A3C5CL,(-1L),0xF1EAD1A0L,(-1L)},{(-1L),0x8A91F534L,(-1L),1L},{3L,0x221A3C5CL,1L,(-1L)},{0xC1B82F2AL,(-1L),0xD3B9B612L,(-1L)},{0xC1B82F2AL,1L,1L,0xC1B82F2AL},{3L,(-1L),(-1L),1L}},{{(-1L),(-1L),0xF1EAD1A0L,1L},{0x221A3C5CL,3L,1L,1L},{(-1L),(-1L),(-1L),1L},{0xC1B82F2AL,(-1L),5L,0xC1B82F2AL},{0x221A3C5CL,1L,(-1L),(-1L)},{1L,(-1L),(-1L),(-1L)},{0x221A3C5CL,0x221A3C5CL,5L,1L},{0xC1B82F2AL,0x8A91F534L,(-1L),(-1L)}},{{(-1L),(-1L),1L,(-1L)},{0x221A3C5CL,(-1L),0xF1EAD1A0L,(-1L)},{(-1L),0x8A91F534L,(-1L),1L},{3L,0x221A3C5CL,1L,(-1L)},{0xC1B82F2AL,(-1L),0xD3B9B612L,(-1L)},{0xC1B82F2AL,1L,1L,0xC1B82F2AL},{3L,(-1L),(-1L),1L},{(-1L),(-1L),0xF1EAD1A0L,1L}},{{0x221A3C5CL,3L,1L,1L},{(-1L),(-1L),(-1L),1L},{0xC1B82F2AL,(-1L),5L,0xC1B82F2AL},{0x221A3C5CL,1L,(-1L),(-1L)},{1L,(-1L),(-1L),(-1L)},{0x221A3C5CL,0x221A3C5CL,5L,1L},{0xC1B82F2AL,0x8A91F534L,(-1L),(-1L)},{(-1L),(-1L),1L,(-1L)}},{{0x221A3C5CL,(-1L),0xF1EAD1A0L,(-1L)},{(-1L),0x8A91F534L,(-1L),1L},{3L,0x221A3C5CL,1L,(-1L)},{0xC1B82F2AL,(-1L),0xD3B9B612L,(-1L)},{0xC1B82F2AL,1L,1L,0xC1B82F2AL},{3L,(-1L),(-1L),1L},{(-1L),(-1L),0xF1EAD1A0L,1L},{0x221A3C5CL,3L,1L,1L}},{{(-1L),(-1L),(-1L),1L},{0xC1B82F2AL,(-1L),5L,0xC1B82F2AL},{0x221A3C5CL,1L,(-1L),(-1L)},{1L,(-1L),(-1L),(-1L)},{0x221A3C5CL,0x221A3C5CL,5L,1L},{0xC1B82F2AL,0x8A91F534L,(-1L),(-1L)},{(-1L),(-1L),0xD3B9B612L,0xF1EAD1A0L},{1L,(-1L),(-1L),(-1L)}},{{(-1L),0x7CC77AD7L,0x15F07962L,0x8A91F534L},{5L,1L,0xD3B9B612L,0x15F07962L},{(-1L),3L,1L,(-1L)},{(-1L),0xD3B9B612L,0xD3B9B612L,(-1L)},{5L,(-1L),0x15F07962L,0xD3B9B612L},{(-1L),3L,(-1L),0x8A91F534L},{1L,5L,0xD3B9B612L,0x8A91F534L},{0xF1EAD1A0L,3L,0xF1EAD1A0L,0xD3B9B612L}}};
                        uint32_t l_660 = 0x7AAA15CBL;
                        int8_t *l_671 = &g_65[9];
                        int32_t *****l_675 = &l_674;
                        struct S1 * volatile *l_680 = &g_643;
                        struct S1 * volatile **l_679 = &l_680;
                        int i, j, k;
                        l_660--;
                        l_664--;
                        (*l_555) &= ((safe_rshift_func_uint16_t_u_s((l_543[(l_470 + 2)] , l_655), l_648)) < (p_45 != (((*l_671) = (0x38D5L | ((*l_468) = l_653))) , (255UL ^ ((safe_sub_func_uint64_t_u_u((((*l_675) = l_674) != ((g_676 , 18446744073709551615UL) , l_677[3][1][1])), l_678[0])) >= g_130)))));
                        (*l_679) = &g_643;
                    }
                    else
                    { /* block id: 279 */
                        struct S0 *l_681[1][1][8] = {{{&g_159,&g_159,&g_159,&g_159,&g_159,&g_159,&g_159,&g_159}}};
                        int i, j, k;
                        (*g_682) = l_681[0][0][6];
                        (*l_482) = (void*)0;
                        (*l_555) &= 3L;
                    }
                    l_683 = &l_663;
                    if ((((~g_242[7].f6) != l_685) , p_45))
                    { /* block id: 285 */
                        uint64_t ***l_689 = &g_267;
                        int32_t l_690 = 1L;
                        int32_t l_691 = 0x58A27D53L;
                        (*g_686) = (**g_396);
                        l_689 = l_688;
                        l_692[2]--;
                    }
                    else
                    { /* block id: 289 */
                        int32_t *l_695[2];
                        uint64_t ***l_702 = &g_267;
                        uint64_t **l_712 = &g_268;
                        uint64_t ***l_711 = &l_712;
                        int64_t ***l_713 = (void*)0;
                        int64_t l_735 = 1L;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_695[i] = &l_659[3][6][1];
                        (*g_686) = l_695[0];
                        (*l_555) = (*g_687);
                        l_561 &= ((*l_481) = (((safe_sub_func_uint64_t_u_u((***g_266), ((((((safe_mod_func_int64_t_s_s((safe_lshift_func_int8_t_s_s((0xDBCEL < ((g_703[4][0][2] = l_702) != &g_267)), 3)), ((safe_lshift_func_int16_t_s_s((((l_714 = (((safe_rshift_func_int32_t_s_u((((void*)0 == l_708[1][6]) & (safe_div_func_uint8_t_u_u((p_45 == (((*l_711) = &g_268) == (((*l_683) <= p_45) , &g_268))), g_185.f1))), p_45)) && g_125.f5) , l_713)) != g_715[3]) || g_623.f0), 2)) ^ p_45))) <= g_185.f6) < 0x85E89A456B7E243BLL) != 0xCA0A443608F1C08ELL) , &g_65[8]) == &g_65[0]))) | (*g_268)) , p_45));
                        (*l_683) = (((((g_626.f6 = (g_625.f6 ^ (safe_add_func_int16_t_s_s((((safe_sub_func_uint64_t_u_u((safe_lshift_func_uint32_t_u_s(((safe_div_func_uint16_t_u_u((((~p_45) | (((safe_add_func_int32_t_s_s(((*g_297) , (*l_683)), (l_729 ^= p_45))) , (((~(*l_683)) | ((safe_mod_func_int64_t_s_s((&l_510 == &l_543[3]), ((((g_122[l_470][(l_470 + 3)][l_586] = ((safe_add_func_int64_t_s_s((l_543[l_586] = (*p_44)), (*p_44))) | 9L)) , l_481) == &g_89) && 0x3F69EB70490F4CDBLL))) >= (***g_266))) & 0xA6A4ACC3A994C2BALL)) & l_649)) >= (*p_44)), 4UL)) == 18446744073709551610UL), 7)), p_45)) == p_45) , (*l_555)), l_735)))) == 0x9970E7B5L) , (*l_555)) , l_736) == l_576[0][6]);
                    }
                }
                (*g_742) = ((safe_rshift_func_uint64_t_u_s((65526UL == p_45), (*p_44))) , g_739);
                for (l_729 = 0; (l_729 <= 8); l_729 += 1)
                { /* block id: 307 */
                    int32_t l_743 = 0xB46F27AEL;
                    int32_t l_745 = 1L;
                    int32_t l_746[4][10] = {{0L,0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,3L,0L,0L,3L,0L,0L,3L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L,0L}};
                    int i, j;
                    l_753--;
                    for (g_88 = 2; (g_88 <= 8); g_88 += 1)
                    { /* block id: 311 */
                        (*l_482) = &l_746[3][5];
                    }
                    for (l_561 = 7; (l_561 >= 0); l_561 -= 1)
                    { /* block id: 316 */
                        int32_t **l_772 = (void*)0;
                        uint32_t *l_774 = &g_179;
                        struct S2 * const l_779[8] = {&g_781[0],&g_781[0],&g_781[0],&g_781[0],&g_781[0],&g_781[0],&g_781[0],&g_781[0]};
                        struct S2 * const *l_778 = &l_779[2];
                        struct S2 * const **l_777 = &l_778;
                        int i;
                        if (l_744)
                            break;
                        (*l_555) |= (((((*l_774) = (safe_mod_func_int16_t_s_s((safe_div_func_int16_t_s_s((safe_div_func_int64_t_s_s(((((((***g_742) > (safe_mul_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((1UL > (((safe_add_func_int8_t_s_s((l_768 == (l_650[5][5][2] , g_769)), p_45)) == ((+(*p_44)) ^ (((void*)0 != l_772) , p_45))) <= 0x04L)) == l_650[0][8][3]), p_45)), 2L))) && g_122[4][6][1]) && p_45) < l_773) || (**g_769)), (*p_44))), 0xEB6CL)), p_45))) , l_648) >= (**g_397)) != g_125.f6);
                        (*l_555) = (safe_lshift_func_int64_t_s_u(((l_650[4][8][1] , g_270) != ((*l_777) = (void*)0)), 50));
                    }
                    if (p_45)
                    { /* block id: 323 */
                        uint32_t l_782[3][10][4] = {{{0UL,0xF6C1455EL,0UL,5UL},{0UL,5UL,0xE5418A3EL,0xF13F22E2L},{0xFBB26DA7L,5UL,0xCD278B7DL,5UL},{0xE5418A3EL,0xF6C1455EL,0xCD278B7DL,18446744073709551615UL},{0xFBB26DA7L,18446744073709551606UL,0xE5418A3EL,18446744073709551615UL},{0UL,0xF6C1455EL,0UL,5UL},{0UL,5UL,0xE5418A3EL,0xF13F22E2L},{0xFBB26DA7L,5UL,0xCD278B7DL,5UL},{0xE5418A3EL,0xF6C1455EL,0xCD278B7DL,18446744073709551615UL},{0xFBB26DA7L,18446744073709551606UL,0xE5418A3EL,18446744073709551615UL}},{{0UL,0xF6C1455EL,0UL,5UL},{0UL,5UL,0xE5418A3EL,0xF13F22E2L},{0xFBB26DA7L,5UL,0xCD278B7DL,5UL},{0xE5418A3EL,0xF6C1455EL,0xCD278B7DL,18446744073709551615UL},{0xFBB26DA7L,18446744073709551606UL,0xE5418A3EL,18446744073709551615UL},{0UL,0xF6C1455EL,0UL,5UL},{0UL,5UL,0xE5418A3EL,0xF13F22E2L},{0xFBB26DA7L,5UL,0xCD278B7DL,5UL},{0xE5418A3EL,0xF6C1455EL,0xCD278B7DL,18446744073709551615UL},{0xFBB26DA7L,18446744073709551606UL,0xE5418A3EL,18446744073709551615UL}},{{0UL,0xF6C1455EL,0UL,5UL},{0UL,5UL,0xE5418A3EL,0xF13F22E2L},{0xFBB26DA7L,5UL,0xCD278B7DL,5UL},{0xE5418A3EL,0xF6C1455EL,0xCD278B7DL,18446744073709551615UL},{0xFBB26DA7L,5UL,0UL,0xF6C1455EL},{0xCD278B7DL,18446744073709551606UL,0xCD278B7DL,0xF13F22E2L},{0xCD278B7DL,0xF13F22E2L,0UL,18446744073709551615UL},{0xE5418A3EL,0xF13F22E2L,1UL,0xF13F22E2L},{0UL,18446744073709551606UL,1UL,0xF6C1455EL},{0xE5418A3EL,5UL,0UL,0xF6C1455EL}}};
                        uint8_t *l_790 = &l_473;
                        int32_t *****l_791[8] = {&l_677[3][1][1],&l_677[3][1][1],&l_677[3][1][1],&l_677[3][1][1],&l_677[3][1][1],&l_677[3][1][1],&l_677[3][1][1],&l_677[3][1][1]};
                        uint8_t *l_794 = &l_664;
                        uint8_t **l_793 = &l_794;
                        int i, j, k;
                        (*l_481) = (l_782[0][2][3] = 0x71EC5D34L);
                        (*l_481) ^= ((g_783[2] , (safe_add_func_int32_t_s_s((l_746[3][5] != 0x18565031CD926B5FLL), (safe_add_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(((*l_790) ^= 0UL), g_623.f6)), (((l_792 = (l_791[0] = (void*)0)) != &g_395[2][1]) >= (&l_664 != ((*l_793) = &l_454)))))))) ^ g_117[6]);
                    }
                    else
                    { /* block id: 331 */
                        (*l_481) = 0x156F0FB3L;
                    }
                }
                (*l_555) = (((g_117[6] == g_625.f0) , (safe_lshift_func_uint16_t_u_s(l_799, l_648))) <= ((((safe_div_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s(((*l_831) &= (safe_add_func_int16_t_s_s(p_45, ((((safe_mod_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((safe_unary_minus_func_uint32_t_u((((safe_add_func_uint8_t_u_u(((*l_555) == (safe_rshift_func_int64_t_s_s((safe_mod_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(((safe_sub_func_int8_t_s_s(((*l_828) = (safe_add_func_uint8_t_u_u(((p_45 > 0xADL) >= (!(g_319 = (--(*l_824))))), 0xACL))), (*l_555))) , 255UL), p_45)), 0x5D82L)), (*p_44)))), p_45)) , p_45) >= l_829))), (**g_769))) || g_149.f4), p_45)) ^ l_830[1]) , g_296.f0) , p_45)))), p_45)), 0x684B6D4FE925D447LL)) & (*l_555)) == 0xCBC1B665L) <= p_45));
            }
            l_561 ^= ((((safe_lshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_s(0x2EL, 5)), g_624.f6)) & (safe_sub_func_uint16_t_u_u(p_45, (((*l_555) = 0x518CE344L) , 4L)))) != (((safe_mod_func_int64_t_s_s((safe_add_func_int8_t_s_s(p_45, (safe_sub_func_uint16_t_u_u((((***g_742) , p_45) < (safe_div_func_int8_t_s_s((l_846[5] > (**g_397)), p_45))), (*g_770))))), 0xAC552D7B235E8927LL)) || 1L) <= p_45)) && p_45);
            for (g_89 = 0; (g_89 <= 8); g_89 += 1)
            { /* block id: 345 */
                uint64_t l_854 = 0xAF8812824FC4E4C2LL;
                int32_t l_857[1];
                int16_t l_860 = 0xBBFBL;
                int16_t *****l_867 = &l_866;
                int16_t ****l_871 = &l_869;
                int16_t *****l_870 = &l_871;
                int8_t l_945[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_857[i] = 0xAE2E157BL;
                for (i = 0; i < 1; i++)
                    l_945[i] = (-1L);
                for (l_829 = 0; (l_829 <= 1); l_829 += 1)
                { /* block id: 348 */
                    struct S1 **l_852 = &g_384;
                    struct S1 ***l_853 = &l_852;
                    int64_t ***l_855 = &g_716;
                    int32_t l_856[4] = {0x59836A42L,0x59836A42L,0x59836A42L,0x59836A42L};
                    int16_t l_861[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_861[i] = 0L;
                    l_856[3] ^= ((*l_555) = (9UL ^ ((safe_mod_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((((*p_44) &= ((p_45 != (l_851 != ((*l_853) = l_852))) < l_854)) >= (p_45 && 3L)), (((*l_855) = &p_44) != &p_44))), (*l_555))) , 1L)));
                    l_862++;
                    for (g_83 = 0; (g_83 <= 2); g_83 += 1)
                    { /* block id: 357 */
                        int i, j, k;
                        (*g_865) = (*g_397);
                        if (p_45)
                            break;
                    }
                }
                (*l_870) = (l_868 = ((*l_867) = l_866));
                if (l_854)
                { /* block id: 365 */
                    int32_t **l_883 = &l_483[1][0][5];
                    (*g_873) = g_872;
                    (*l_482) = &l_857[0];
                    for (l_752 = 2; (l_752 <= 8); l_752 += 1)
                    { /* block id: 370 */
                        const uint32_t *l_875 = &l_493;
                        const uint32_t **l_874 = &l_875;
                        int32_t l_878[5];
                        int64_t l_886 = (-7L);
                        int32_t l_894 = 0L;
                        uint8_t *l_895 = &l_454;
                        int i;
                        for (i = 0; i < 5; i++)
                            l_878[i] = 0x070C166EL;
                        (*l_480) |= (((*l_874) = (void*)0) == ((0xDDC6A44FL ^ (safe_lshift_func_uint64_t_u_s(l_860, 36))) , &g_108));
                        (*l_481) = (((*p_44) & (l_878[1] = 1L)) || 0L);
                        (*l_481) = ((**g_739) || (((safe_rshift_func_uint32_t_u_u(((safe_mod_func_int32_t_s_s((l_878[2] = ((void*)0 == l_883)), (((*l_472) = (safe_mod_func_int32_t_s_s(((((g_95 != ((*l_895) = (((*l_480) = ((l_886 & (l_887 && (&g_716 == ((safe_div_func_int8_t_s_s(3L, (safe_div_func_int32_t_s_s((safe_div_func_int8_t_s_s((g_781[0].f4 , g_86), (*l_480))), 1UL)))) , (void*)0)))) != l_894)) != p_45))) & g_122[0][7][0]) , g_783[2].f3) ^ 65535UL), l_886))) | 0x5700BE27L))) & g_119), p_45)) | (*l_481)) || 3UL));
                    }
                    (**l_482) = p_45;
                }
                else
                { /* block id: 382 */
                    const uint32_t **l_902 = (void*)0;
                    const struct S2 *l_927 = (void*)0;
                    int32_t l_934 = 9L;
                    if (((*g_742) != &l_468))
                    { /* block id: 383 */
                        const uint32_t ***l_903 = &l_902;
                        uint32_t *l_912 = (void*)0;
                        uint32_t *l_913 = &l_582[0][1][0];
                        int32_t l_914[5][7][5] = {{{0L,(-3L),1L,1L,(-3L)},{0xFA96521FL,0x01024323L,1L,0x080AC66CL,0x99FCD422L},{0x8429912BL,0L,0x9A52B266L,0x1D369B66L,0x97DA4AE2L},{4L,0x9F8285DAL,0xB90E42B9L,0L,0L},{0x8429912BL,4L,0L,0x7175589EL,0xC620B15DL},{0xFA96521FL,0xA3420A7FL,0x0B1B26FAL,0x4698AA9AL,0x219764BCL},{0L,(-9L),0xB90E42B9L,1L,(-5L)}},{{(-5L),0xC620B15DL,0x2D244A8AL,0xA3420A7FL,0x01024323L},{0L,0xB5318B5FL,0x7C922488L,0L,0x8429912BL},{0x01024323L,0xB5318B5FL,1L,0x99FCD422L,0L},{0xA3420A7FL,0xC620B15DL,0x01024323L,9L,0L},{0x080AC66CL,(-9L),0L,0L,0L},{1L,0xA3420A7FL,1L,0L,(-3L)},{(-9L),4L,(-1L),0xA3420A7FL,0xB90E42B9L}},{{0x080AC66CL,0x9F8285DAL,0x3DDE7A29L,0xB5318B5FL,0xFA96521FL},{(-6L),0L,(-1L),0xB90E42B9L,9L},{0x219764BCL,0x01024323L,1L,0x7175589EL,(-1L)},{0L,(-3L),0L,0x97DA4AE2L,0xFA96521FL},{0x0D0E04E4L,0x97DA4AE2L,0x01024323L,0L,0x4698AA9AL},{0x1D369B66L,0x21E2B3D5L,1L,0x080AC66CL,0x21E2B3D5L},{0xFA96521FL,0x219764BCL,0x7C922488L,0x080AC66CL,0L}},{{0xC620B15DL,0L,0x2D244A8AL,0L,0x97DA4AE2L},{0x7175589EL,0x55EF5714L,0xB90E42B9L,0x97DA4AE2L,0L},{0x8429912BL,0x7175589EL,0x0B1B26FAL,0x7175589EL,0x8429912BL},{0xEC16BE25L,0xA3420A7FL,0L,0xB90E42B9L,0x219764BCL},{0x1D369B66L,1L,0xB90E42B9L,0xB5318B5FL,0x0D0E04E4L},{(-5L),0x8429912BL,0x9A52B266L,0xA3420A7FL,0x219764BCL},{0L,0xB5318B5FL,1L,0L,0x8429912BL}},{{0x219764BCL,1L,1L,0L,0L},{0xA3420A7FL,0x8429912BL,0xD76C0B00L,9L,0x97DA4AE2L},{1L,(-9L),0x55EF5714L,0x99FCD422L,0L},{(-9L),0L,0x563A4A5CL,1L,0x7A7E4DA8L},{0xEC16BE25L,1L,0x8429912BL,0x55EF5714L,1L},{0x0B1B26FAL,0L,9L,0xD76C0B00L,0x99FCD422L},{0x55EF5714L,0L,0x3DDE7A29L,1L,0x3DDE7A29L}}};
                        int i, j, k;
                        (*l_555) |= ((l_851 == (g_896 , &g_643)) <= (((((*l_913) = (safe_mul_func_uint16_t_u_u(((p_45 > p_45) , ((safe_add_func_uint32_t_u_u(((*l_472) ^= ((safe_unary_minus_func_int16_t_s(p_45)) , ((((p_45 >= (((((*l_903) = l_902) != g_904[8][2][4]) == 0x271A16E6L) , (-1L))) , (void*)0) == (void*)0) , 0x8994FD5BL))), 0xA56DCA73L)) , g_623.f4)), g_319))) ^ g_149.f2) | l_914[1][3][2]) ^ 2UL));
                        (*l_482) = &l_857[0];
                        if (p_45)
                            continue;
                    }
                    else
                    { /* block id: 390 */
                        uint8_t l_917 = 1UL;
                        uint16_t *l_918 = &g_122[4][6][1];
                        uint8_t ***l_930 = &l_928[1];
                        (*l_481) &= (safe_add_func_int32_t_s_s((((*l_918) = l_917) && (g_919 , (~((safe_lshift_func_uint32_t_u_u(((((safe_rshift_func_int8_t_s_s(((g_65[8] , p_45) , 5L), 2)) , (l_925 = (void*)0)) == l_927) && (((*l_930) = l_928[0]) != g_931)), g_459.f6)) , l_934)))), p_45));
                        if (l_857[0])
                            continue;
                    }
                }
                l_935 = &g_703[4][0][2];
                for (l_571 = 1; (l_571 <= 8); l_571 += 1)
                { /* block id: 401 */
                    int32_t l_936[10][6][4] = {{{(-1L),(-2L),(-1L),(-1L)},{1L,1L,(-1L),(-1L)},{(-2L),(-2L),1L,1L},{1L,1L,1L,1L},{(-2L),(-1L),(-1L),1L},{1L,(-1L),(-1L),1L}},{{(-1L),1L,(-2L),1L},{(-1L),(-2L),(-1L),(-1L)},{1L,1L,(-1L),(-1L)},{(-2L),(-2L),1L,1L},{1L,1L,1L,1L},{(-2L),(-1L),(-1L),1L}},{{1L,(-1L),(-1L),1L},{(-1L),1L,(-2L),1L},{(-1L),(-2L),(-1L),(-1L)},{1L,1L,(-1L),(-1L)},{(-2L),(-2L),1L,1L},{1L,1L,1L,1L}},{{(-2L),(-1L),(-1L),1L},{1L,(-1L),(-1L),1L},{(-1L),1L,(-2L),1L},{(-1L),(-2L),(-1L),(-1L)},{1L,1L,(-1L),(-1L)},{(-2L),(-2L),1L,1L}},{{1L,1L,1L,1L},{(-2L),(-1L),(-1L),1L},{1L,(-1L),(-1L),1L},{(-1L),1L,(-2L),1L},{(-1L),(-2L),(-1L),(-1L)},{1L,1L,(-1L),(-1L)}},{{(-2L),(-2L),1L,1L},{1L,1L,1L,1L},{(-2L),(-1L),(-1L),1L},{1L,(-1L),(-1L),1L},{(-1L),1L,(-2L),1L},{(-1L),(-2L),(-1L),(-1L)}},{{1L,1L,(-1L),(-1L)},{(-2L),(-2L),1L,1L},{1L,1L,1L,1L},{(-2L),(-1L),(-1L),1L},{1L,(-1L),(-1L),1L},{(-1L),1L,(-2L),1L}},{{(-1L),(-2L),(-1L),1L},{(-1L),(-1L),1L,1L},{(-1L),(-1L),(-2L),(-1L)},{(-1L),(-1L),(-2L),(-1L)},{(-1L),1L,1L,(-2L)},{(-1L),1L,1L,(-1L)}},{{1L,(-1L),(-1L),(-1L)},{1L,(-1L),1L,1L},{(-1L),(-1L),1L,1L},{(-1L),(-1L),(-2L),(-1L)},{(-1L),(-1L),(-2L),(-1L)},{(-1L),1L,1L,(-2L)}},{{(-1L),1L,1L,(-1L)},{1L,(-1L),(-1L),(-1L)},{1L,(-1L),1L,1L},{(-1L),(-1L),1L,1L},{(-1L),(-1L),(-2L),(-1L)},{(-1L),(-1L),(-2L),(-1L)}}};
                    int32_t l_937 = (-1L);
                    int32_t l_938 = 0x4EEA0F7DL;
                    uint8_t l_939[2][9] = {{0UL,0UL,3UL,0UL,0UL,3UL,0UL,0UL,3UL},{0UL,0UL,3UL,0UL,0UL,3UL,0UL,0UL,3UL}};
                    int i, j, k;
                    l_936[6][1][2] |= 5L;
                    l_939[1][6]--;
                    for (l_748 = 1; (l_748 >= 0); l_748 -= 1)
                    { /* block id: 406 */
                        int64_t ****l_942[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_942[i] = &l_714;
                        g_715[3] = &g_716;
                        (*l_555) |= (safe_sub_func_int16_t_s_s(0x0E50L, l_945[0]));
                    }
                }
            }
        }
    }
    else
    { /* block id: 413 */
        int32_t l_948 = 1L;
        int32_t *l_949 = &g_40;
        int32_t *l_950 = &l_471;
        int32_t *l_951 = &g_40;
        int32_t *l_952[3][9] = {{&l_471,(void*)0,&l_846[5],&l_471,&l_748,&l_748,&l_471,&l_846[5],(void*)0},{&l_748,&l_846[5],&l_471,&l_747,&l_749[0],&g_40,&g_40,&l_749[0],&l_747},{&l_748,&l_749[0],&l_748,&g_40,&l_471,&l_471,(void*)0,(void*)0,&l_471}};
        uint32_t l_953 = 0x9CD7733EL;
        int i, j;
        (*g_947) = g_946[5][4];
        for (g_13 = 0; (g_13 <= 1); g_13 += 1)
        { /* block id: 417 */
            return g_626.f1;
        }
        l_953++;
    }
    return p_45;
}


/* ------------------------------------------ */
/* 
 * reads : g_18 g_149 g_125.f6 g_296 g_297 g_319 g_115.f1 g_65 g_344 g_111 g_39 g_119 g_270 g_384 g_385.f1 g_159.f0 g_185.f6 g_125.f4 g_268 g_83 g_159 g_395 g_40 g_396 g_397 g_398 g_13
 * writes: g_121 g_125.f0 g_13 g_298 g_18 g_319 g_63 g_88 g_179 g_39 g_395
 */
static int64_t * func_46(int32_t  p_47)
{ /* block id: 48 */
    uint32_t l_131 = 4294967288UL;
    int16_t *l_150 = &g_121;
    int32_t l_151 = 0x481FF659L;
    int32_t *l_152[2];
    int64_t *l_157 = &g_18;
    int32_t l_164 = 0x08EC85A5L;
    int32_t l_176 = 1L;
    int16_t l_182 = 0xF12BL;
    int16_t l_236 = (-1L);
    uint64_t *l_265[7];
    uint64_t **l_264 = &l_265[0];
    struct S2 *l_269 = &g_125;
    const uint32_t l_345 = 0x9508DF86L;
    uint32_t l_387 = 0UL;
    int i;
    for (i = 0; i < 2; i++)
        l_152[i] = &g_40;
    for (i = 0; i < 7; i++)
        l_265[i] = &g_130;
lbl_331:
    g_125.f0 = (l_151 = ((((((l_131 != ((safe_unary_minus_func_uint32_t_u(4294967290UL)) != ((0x570D041EFA3C54B2LL || (safe_sub_func_int16_t_s_s(((safe_add_func_uint32_t_u_u((safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(l_131, ((((*l_150) = (safe_sub_func_int16_t_s_s((g_18 && 0xB509E705L), (safe_mod_func_uint8_t_u_u(l_131, (safe_add_func_int64_t_s_s((safe_mod_func_int64_t_s_s((g_149 , l_131), p_47)), g_149.f2))))))) == p_47) && g_125.f6))), 0x50L)), 2L)) == (-1L)), 0xECCDL))) , 6UL))) | g_125.f6) | p_47) == 0x9019L) != p_47) && p_47));
    for (g_13 = 0; (g_13 <= 8); g_13 += 1)
    { /* block id: 54 */
        int8_t l_162 = (-1L);
        int16_t l_163 = 0xE97EL;
        int32_t l_168 = 0xA05D0EB5L;
        int32_t l_169 = 0xD1E233BCL;
        int32_t l_171 = (-1L);
        int32_t l_174 = 4L;
        int32_t l_175 = 0xA8405193L;
        int32_t l_177 = (-4L);
        int32_t l_178[6][7][6] = {{{0x8EF08E0EL,0x0012FF69L,0xC616BB7FL,0x6A13217AL,0x0012FF69L,5L},{(-1L),0x8EF08E0EL,0xC616BB7FL,0x8EF08E0EL,(-1L),0x7FF6CDE4L},{0L,0x8EF08E0EL,5L,0L,0x0012FF69L,0x11D92424L},{0L,0x0012FF69L,0x11D92424L,0x8EF08E0EL,0x8EF08E0EL,0x11D92424L},{(-1L),(-1L),5L,0x6A13217AL,0x8EF08E0EL,0x7FF6CDE4L},{0x8EF08E0EL,0x0012FF69L,0xC616BB7FL,0x6A13217AL,0x0012FF69L,5L},{(-1L),0x8EF08E0EL,0xC616BB7FL,0x8EF08E0EL,(-1L),0x7FF6CDE4L}},{{0L,0x8EF08E0EL,5L,0L,0x0012FF69L,0x11D92424L},{0L,0x0012FF69L,0x11D92424L,0x8EF08E0EL,0x8EF08E0EL,0x11D92424L},{(-1L),(-1L),5L,0x6A13217AL,0x8EF08E0EL,0x7FF6CDE4L},{0x8EF08E0EL,0x0012FF69L,0xC616BB7FL,0x6A13217AL,0x0012FF69L,5L},{(-1L),0x8EF08E0EL,0xC616BB7FL,0x8EF08E0EL,(-1L),0x7FF6CDE4L},{0L,0x8EF08E0EL,5L,0L,0x0012FF69L,0x6A13217AL},{0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL,0x276C94B1L,0x276C94B1L,0x6A13217AL}},{{0x2E843886L,0x2E843886L,0L,6L,0x276C94B1L,0x8EF08E0EL},{0x276C94B1L,0xDF4CC50AL,0x0012FF69L,6L,0xDF4CC50AL,0L},{0x2E843886L,0x276C94B1L,0x0012FF69L,0x276C94B1L,0x2E843886L,0x8EF08E0EL},{0x40C9C9E0L,0x276C94B1L,0L,0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL},{0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL,0x276C94B1L,0x276C94B1L,0x6A13217AL},{0x2E843886L,0x2E843886L,0L,6L,0x276C94B1L,0x8EF08E0EL},{0x276C94B1L,0xDF4CC50AL,0x0012FF69L,6L,0xDF4CC50AL,0L}},{{0x2E843886L,0x276C94B1L,0x0012FF69L,0x276C94B1L,0x2E843886L,0x8EF08E0EL},{0x40C9C9E0L,0x276C94B1L,0L,0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL},{0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL,0x276C94B1L,0x276C94B1L,0x6A13217AL},{0x2E843886L,0x2E843886L,0L,6L,0x276C94B1L,0x8EF08E0EL},{0x276C94B1L,0xDF4CC50AL,0x0012FF69L,6L,0xDF4CC50AL,0L},{0x2E843886L,0x276C94B1L,0x0012FF69L,0x276C94B1L,0x2E843886L,0x8EF08E0EL},{0x40C9C9E0L,0x276C94B1L,0L,0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL}},{{0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL,0x276C94B1L,0x276C94B1L,0x6A13217AL},{0x2E843886L,0x2E843886L,0L,6L,0x276C94B1L,0x8EF08E0EL},{0x276C94B1L,0xDF4CC50AL,0x0012FF69L,6L,0xDF4CC50AL,0L},{0x2E843886L,0x276C94B1L,0x0012FF69L,0x276C94B1L,0x2E843886L,0x8EF08E0EL},{0x40C9C9E0L,0x276C94B1L,0L,0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL},{0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL,0x276C94B1L,0x276C94B1L,0x6A13217AL},{0x2E843886L,0x2E843886L,0L,6L,0x276C94B1L,0x8EF08E0EL}},{{0x276C94B1L,0xDF4CC50AL,0x0012FF69L,6L,0xDF4CC50AL,0L},{0x2E843886L,0x276C94B1L,0x0012FF69L,0x276C94B1L,0x2E843886L,0x8EF08E0EL},{0x40C9C9E0L,0x276C94B1L,0L,0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL},{0x40C9C9E0L,0xDF4CC50AL,0x6A13217AL,0x276C94B1L,0x276C94B1L,0x6A13217AL},{0x2E843886L,0x2E843886L,0L,6L,0x276C94B1L,0x8EF08E0EL},{0x276C94B1L,0xDF4CC50AL,0x0012FF69L,6L,0xDF4CC50AL,0L},{0x2E843886L,0x276C94B1L,0x0012FF69L,0x276C94B1L,0x2E843886L,0x8EF08E0EL}}};
        struct S0 *l_217[5];
        const int8_t *l_233[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint64_t l_239 = 0UL;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_217[i] = &g_159;
    }
    if ((l_265[0] != l_265[3]))
    { /* block id: 118 */
        (*g_297) = g_296;
    }
    else
    { /* block id: 120 */
        int32_t l_303 = 1L;
        int32_t l_304 = (-7L);
        int32_t l_305 = 0L;
        int32_t l_306 = 0L;
        int32_t l_307 = (-1L);
        int32_t l_308 = 0x4EB133E6L;
        int32_t l_309 = 0x2067BBAEL;
        int32_t l_310 = 0xF52C8BBBL;
        int32_t l_311 = 0x939939CFL;
        int32_t l_312 = 0xDB190079L;
        int32_t l_313 = 0x69803BF5L;
        int32_t l_314 = (-6L);
        int32_t l_315 = (-1L);
        int32_t l_316 = 0x287C2302L;
        int32_t l_318 = 1L;
        uint32_t *l_338 = &l_131;
        int32_t ***l_361 = (void*)0;
        int32_t ****l_360 = &l_361;
        struct S0 *l_390 = &g_159;
        int64_t **l_410[2][10] = {{(void*)0,&l_157,(void*)0,(void*)0,&l_157,(void*)0,&l_157,(void*)0,&l_157,(void*)0},{&l_157,&l_157,&l_157,&l_157,&l_157,&l_157,&l_157,&l_157,&l_157,&l_157}};
        int64_t ***l_409 = &l_410[1][5];
        int16_t l_415[1][9] = {{1L,1L,1L,1L,1L,1L,1L,1L,1L}};
        uint64_t l_433 = 0xEAE9487F12CDA75CLL;
        int32_t l_437 = 0xD8576F8EL;
        uint32_t l_438 = 1UL;
        int i, j;
        for (g_18 = (-23); (g_18 != (-25)); g_18 = safe_sub_func_int16_t_s_s(g_18, 1))
        { /* block id: 123 */
            int32_t l_301 = 0x9D83285AL;
            int32_t l_302[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
            int32_t l_317 = (-8L);
            uint16_t *l_322 = &g_319;
            int64_t *l_323 = &g_63;
            int64_t *l_324 = (void*)0;
            int64_t *l_325 = &g_88;
            int32_t ***l_358[4];
            int32_t ****l_357 = &l_358[1];
            struct S1 *l_383 = (void*)0;
            struct S0 *l_391 = (void*)0;
            int8_t l_413[2][9] = {{0x71L,0x71L,0x71L,0x71L,0x71L,0x71L,0x71L,0x71L,0x71L},{0x4EL,2L,0x4EL,2L,0x4EL,2L,0x4EL,2L,0x4EL}};
            int64_t l_428 = (-1L);
            int i, j;
            for (i = 0; i < 4; i++)
                l_358[i] = &g_349;
            --g_319;
            if (p_47)
                break;
            if (((((((*l_323) = (((*l_322) = l_316) && 0x3868L)) <= ((*l_325) = (-1L))) , 0xA3F1A3C8B688D3BBLL) == 0x3B69A0E57AB407BELL) >= (safe_mod_func_uint8_t_u_u(0x78L, ((l_312 = 0xF0591421L) && ((((((safe_unary_minus_func_uint8_t_u((p_47 ^ ((safe_mul_func_uint32_t_u_u(0x56CEE686L, 6L)) <= p_47)))) , (void*)0) != (void*)0) | 0x4CD1L) < l_312) | g_296.f0))))))
            { /* block id: 130 */
                for (g_179 = 1; (g_179 <= 6); g_179 += 1)
                { /* block id: 133 */
                    if (g_18)
                        goto lbl_331;
                }
            }
            else
            { /* block id: 136 */
                uint32_t *l_337 = &g_86;
                int64_t *l_339[1];
                int64_t **l_340 = &l_323;
                int32_t **l_343 = &g_39;
                uint16_t *l_379 = &g_319;
                int32_t * const *l_389 = (void*)0;
                int32_t * const **l_388 = &l_389;
                int32_t l_417 = 0x29C2B25CL;
                uint8_t l_419 = 0x40L;
                int32_t l_427 = 0xFCEBD946L;
                int32_t l_430 = 0x24D83999L;
                int32_t l_431 = 2L;
                int32_t l_432 = 0x433BF013L;
                int i;
                for (i = 0; i < 1; i++)
                    l_339[i] = (void*)0;
                if (((safe_add_func_uint64_t_u_u((((((+(((safe_mod_func_uint8_t_u_u(((l_337 != l_338) && 0x2E38L), ((g_115.f1 < p_47) || 0x9AL))) > (((*l_340) = l_339[0]) == (void*)0)) & ((safe_lshift_func_int16_t_s_s((((*l_343) = &l_309) != (void*)0), p_47)) == g_65[8]))) != (-1L)) != g_344) != g_18) || 0xCBL), 4UL)) > g_65[3]))
                { /* block id: 139 */
                    uint32_t l_351 = 1UL;
                    (**g_111) ^= l_345;
                    (*l_343) = &l_302[8];
                    for (g_13 = (-13); (g_13 != 23); g_13 = safe_add_func_int64_t_s_s(g_13, 6))
                    { /* block id: 144 */
                        int32_t **l_350 = &l_152[0];
                        l_350 = &l_152[0];
                        if (p_47)
                            break;
                        (**l_343) = 2L;
                        if (p_47)
                            continue;
                    }
                    l_351 = 0x92ABD192L;
                }
                else
                { /* block id: 151 */
                    uint32_t l_382[1][10] = {{0x10267A71L,0x10267A71L,0UL,0x10267A71L,0x10267A71L,0UL,0x10267A71L,0x10267A71L,0UL,0x10267A71L}};
                    int32_t l_386 = (-8L);
                    int32_t l_412[8][3][1] = {{{(-1L)},{0L},{(-1L)}},{{0x7CAFCDCCL},{(-1L)},{0L}},{{(-1L)},{0x7CAFCDCCL},{(-1L)}},{{0L},{(-1L)},{0x7CAFCDCCL}},{{(-1L)},{0L},{(-1L)}},{{0x7CAFCDCCL},{(-1L)},{0L}},{{(-1L)},{0x7CAFCDCCL},{(-1L)}},{{0L},{(-1L)},{0x7CAFCDCCL}}};
                    int8_t l_425 = 1L;
                    int i, j, k;
                    for (l_307 = 0; (l_307 <= (-16)); l_307--)
                    { /* block id: 154 */
                        int32_t *****l_359 = (void*)0;
                        int32_t *****l_362 = &l_357;
                        (**l_343) ^= p_47;
                        (**l_343) &= (safe_mul_func_uint32_t_u_u(4294967289UL, (!p_47)));
                        (*l_362) = (l_360 = l_357);
                        if (l_316)
                            goto lbl_331;
                    }
                    if (((((safe_div_func_uint8_t_u_u((((safe_sub_func_int16_t_s_s(((((*l_150) = (((safe_add_func_uint16_t_u_u(g_119, (((safe_mod_func_uint32_t_u_u((safe_rshift_func_int32_t_s_u(((((safe_rshift_func_uint16_t_u_u((safe_mul_func_int64_t_s_s((g_63 = ((void*)0 == g_270)), (l_386 ^= (safe_add_func_int64_t_s_s(((void*)0 != l_379), (safe_lshift_func_int8_t_s_s(l_382[0][5], (l_383 != g_384)))))))), 1)) || g_385.f1) < p_47) & l_382[0][2]), p_47)), p_47)) >= p_47) && g_159.f0))) & g_185.f6) <= g_125.f4)) > 0x2926L) && l_387), 65535UL)) , &g_349) != l_388), 7L)) , l_390) == l_391) , (**g_111)))
                    { /* block id: 164 */
                        const int32_t * const ****l_399[3];
                        int32_t l_408 = 0x4B5438F7L;
                        int64_t ***l_411 = &l_340;
                        int32_t l_414 = 0xAE2BD94CL;
                        int32_t l_416[6];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_399[i] = &g_395[5][2];
                        for (i = 0; i < 6; i++)
                            l_416[i] = 0xB9387726L;
                        l_408 = (safe_mod_func_uint64_t_u_u(((~(*g_268)) | (((g_159 , &l_388) == (g_395[6][2] = g_395[6][1])) <= ((*l_379) = 1UL))), (safe_mul_func_int64_t_s_s((safe_mod_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_s((safe_add_func_int16_t_s_s(((*l_150) = 0xB3A1L), ((((((void*)0 != l_325) ^ (((*g_39) ^= p_47) || p_47)) ^ p_47) >= g_149.f2) >= 0UL))), 5)) , g_185.f6), 2UL)), g_40))));
                        l_411 = l_409;
                        l_386 |= ((void*)0 != (**g_396));
                        l_419++;
                    }
                    else
                    { /* block id: 173 */
                        int16_t l_422 = 1L;
                        int32_t l_423 = 0x03F2F67CL;
                        int32_t l_424 = 0x296573E7L;
                        int32_t l_426 = (-5L);
                        int32_t l_429 = 0x3B7A1951L;
                        l_433++;
                        if (p_47)
                            break;
                    }
                    return &g_88;
                }
            }
            l_152[0] = (void*)0;
        }
        ++l_438;
        return l_157;
    }
    return &g_63;
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_13 g_39 g_40 g_63 g_15 g_65 g_88 g_9 g_95 g_108 g_111 g_112 g_115 g_122 g_125
 * writes: g_63 g_18 g_65 g_83 g_86 g_88 g_13 g_89 g_40 g_95 g_108 g_39 g_112 g_122 g_2
 */
static struct S2  func_48(uint16_t  p_49)
{ /* block id: 16 */
    uint8_t l_60 = 0x7BL;
    const int64_t *l_61 = &g_18;
    int32_t *l_62[8][4];
    const int16_t l_64 = 0xA9A6L;
    uint32_t l_66 = 0UL;
    uint16_t *l_79[1][8][7] = {{{&g_13,&g_13,&g_13,&g_13,&g_13,&g_13,&g_13},{&g_9,&g_13,&g_9,&g_13,&g_13,&g_13,&g_9},{&g_13,&g_13,&g_13,&g_9,&g_15,&g_13,&g_15},{&g_13,&g_9,&g_9,&g_13,&g_13,&g_9,&g_13},{&g_9,&g_15,&g_13,&g_13,&g_15,&g_9,&g_13},{&g_15,&g_13,(void*)0,&g_13,&g_13,(void*)0,&g_13},{&g_15,&g_13,&g_9,&g_15,&g_13,&g_13,&g_15},{&g_9,&g_13,&g_9,&g_13,&g_13,&g_9,&g_9}}};
    int32_t l_116[7];
    int16_t l_118 = 6L;
    int i, j, k;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 4; j++)
            l_62[i][j] = (void*)0;
    }
    for (i = 0; i < 7; i++)
        l_116[i] = 0xFD921813L;
    if (((((safe_mod_func_int32_t_s_s(((((((g_2 == (safe_rshift_func_uint8_t_u_s(p_49, ((((0xD2L > (((((((safe_mul_func_uint64_t_u_u(((((safe_lshift_func_int16_t_s_u(g_13, 2)) > (1UL != (((g_63 = (3L <= (((((((((((l_60 ^ ((void*)0 != l_61)) >= 0xEFL) , l_61) != (void*)0) || l_60) < p_49) | p_49) , (*g_39)) , l_62[0][1]) == (void*)0) || 1UL))) , 0x6DB78960L) , 0x9B76FC49L))) & p_49) , g_63), p_49)) == (-2L)) && 9UL) , (void*)0) != &g_18) , p_49) && l_64)) , g_15) >= 0xDE48L) > g_65[8])))) != 0x03D53AF4L) >= 6UL) , p_49) | p_49) , 0x9074591FL), 0x66298A20L)) >= g_15) ^ g_65[8]) == l_66))
    { /* block id: 18 */
        int64_t *l_69 = &g_18;
        int64_t *l_70 = (void*)0;
        int64_t *l_71 = &g_63;
        const int32_t l_72 = 0x5026E73BL;
        int8_t *l_80 = (void*)0;
        int8_t *l_81 = &g_65[5];
        uint64_t *l_82 = &g_83;
        uint32_t *l_84 = &l_66;
        uint32_t *l_85 = &g_86;
        int64_t *l_87[1][3][3] = {{{&g_88,&g_88,&g_88},{&g_88,&g_88,&g_88},{&g_88,&g_88,&g_88}}};
        uint32_t l_90 = 0UL;
        int32_t l_94[3];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_94[i] = 0xF3BD0260L;
        l_90 = ((*g_39) = ((g_89 = (((((safe_sub_func_uint16_t_u_u((g_13 = ((((*l_71) |= ((*l_69) = 0x824F9D4896031D56LL)) & ((((g_88 = (l_72 <= ((*l_85) = ((*l_84) = ((*g_39) > (safe_add_func_uint16_t_u_u((((safe_sub_func_int64_t_s_s(0x3EBA449F347F2DBDLL, ((safe_mod_func_int64_t_s_s((((*l_81) = (&g_13 != l_79[0][7][4])) , 2L), ((*l_82) = p_49))) == g_15))) != 0xDAL) ^ 65535UL), g_40))))))) != p_49) != p_49) , g_88)) >= (-3L))), p_49)) && p_49) | p_49) == g_2) >= 4294967291UL)) == g_9));
        for (l_66 = 29; (l_66 != 24); l_66--)
        { /* block id: 32 */
            (*g_39) ^= p_49;
        }
        ++g_95;
        (*g_39) = (safe_rshift_func_int64_t_s_s(p_49, 58));
    }
    else
    { /* block id: 37 */
        int16_t l_100 = 0x79B8L;
        int32_t l_101 = (-8L);
        int32_t l_102 = 0x720D768DL;
        int32_t l_103 = (-1L);
        int32_t l_104 = 0L;
        int32_t l_105[1][2][5] = {{{(-2L),(-2L),0x3A393D43L,0x5AD653C0L,0x3A393D43L},{(-2L),(-2L),0x3A393D43L,0x5AD653C0L,0x3A393D43L}}};
        int32_t l_106[10] = {(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L)};
        int32_t l_107 = 0xE8EEE9D0L;
        int i, j, k;
        ++g_108;
        (*g_111) = l_62[0][3];
        --g_112;
    }
    g_40 |= (g_115 , p_49);
    --g_122[4][6][1];
    g_2 ^= (l_61 == &g_88);
    return g_125;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_25[i], "g_25[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_40, "g_40", print_hash_value);
    transparent_crc(g_63, "g_63", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_65[i], "g_65[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_83, "g_83", print_hash_value);
    transparent_crc(g_86, "g_86", print_hash_value);
    transparent_crc(g_88, "g_88", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_108, "g_108", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_115.f0, "g_115.f0", print_hash_value);
    transparent_crc(g_115.f1, "g_115.f1", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_117[i], "g_117[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_120, "g_120", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_122[i][j][k], "g_122[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_125.f0, "g_125.f0", print_hash_value);
    transparent_crc(g_125.f1, "g_125.f1", print_hash_value);
    transparent_crc(g_125.f2, "g_125.f2", print_hash_value);
    transparent_crc(g_125.f3, "g_125.f3", print_hash_value);
    transparent_crc(g_125.f4, "g_125.f4", print_hash_value);
    transparent_crc(g_125.f5, "g_125.f5", print_hash_value);
    transparent_crc(g_125.f6, "g_125.f6", print_hash_value);
    transparent_crc(g_125.f7, "g_125.f7", print_hash_value);
    transparent_crc(g_125.f8, "g_125.f8", print_hash_value);
    transparent_crc(g_125.f9, "g_125.f9", print_hash_value);
    transparent_crc(g_130, "g_130", print_hash_value);
    transparent_crc(g_149.f0, "g_149.f0", print_hash_value);
    transparent_crc(g_149.f1, "g_149.f1", print_hash_value);
    transparent_crc(g_149.f2, "g_149.f2", print_hash_value);
    transparent_crc(g_149.f3, "g_149.f3", print_hash_value);
    transparent_crc(g_149.f4, "g_149.f4", print_hash_value);
    transparent_crc(g_149.f5, "g_149.f5", print_hash_value);
    transparent_crc(g_149.f6, "g_149.f6", print_hash_value);
    transparent_crc(g_159.f0, "g_159.f0", print_hash_value);
    transparent_crc(g_159.f1, "g_159.f1", print_hash_value);
    transparent_crc(g_179, "g_179", print_hash_value);
    transparent_crc(g_185.f0, "g_185.f0", print_hash_value);
    transparent_crc(g_185.f1, "g_185.f1", print_hash_value);
    transparent_crc(g_185.f2, "g_185.f2", print_hash_value);
    transparent_crc(g_185.f3, "g_185.f3", print_hash_value);
    transparent_crc(g_185.f4, "g_185.f4", print_hash_value);
    transparent_crc(g_185.f5, "g_185.f5", print_hash_value);
    transparent_crc(g_185.f6, "g_185.f6", print_hash_value);
    transparent_crc(g_185.f7, "g_185.f7", print_hash_value);
    transparent_crc(g_185.f8, "g_185.f8", print_hash_value);
    transparent_crc(g_185.f9, "g_185.f9", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_242[i].f0, "g_242[i].f0", print_hash_value);
        transparent_crc(g_242[i].f1, "g_242[i].f1", print_hash_value);
        transparent_crc(g_242[i].f2, "g_242[i].f2", print_hash_value);
        transparent_crc(g_242[i].f3, "g_242[i].f3", print_hash_value);
        transparent_crc(g_242[i].f4, "g_242[i].f4", print_hash_value);
        transparent_crc(g_242[i].f5, "g_242[i].f5", print_hash_value);
        transparent_crc(g_242[i].f6, "g_242[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_296.f0, "g_296.f0", print_hash_value);
    transparent_crc(g_296.f1, "g_296.f1", print_hash_value);
    transparent_crc(g_296.f2, "g_296.f2", print_hash_value);
    transparent_crc(g_296.f3, "g_296.f3", print_hash_value);
    transparent_crc(g_296.f4, "g_296.f4", print_hash_value);
    transparent_crc(g_296.f5, "g_296.f5", print_hash_value);
    transparent_crc(g_296.f6, "g_296.f6", print_hash_value);
    transparent_crc(g_298.f0, "g_298.f0", print_hash_value);
    transparent_crc(g_298.f1, "g_298.f1", print_hash_value);
    transparent_crc(g_298.f2, "g_298.f2", print_hash_value);
    transparent_crc(g_298.f3, "g_298.f3", print_hash_value);
    transparent_crc(g_298.f4, "g_298.f4", print_hash_value);
    transparent_crc(g_298.f5, "g_298.f5", print_hash_value);
    transparent_crc(g_298.f6, "g_298.f6", print_hash_value);
    transparent_crc(g_319, "g_319", print_hash_value);
    transparent_crc(g_344, "g_344", print_hash_value);
    transparent_crc(g_385.f0, "g_385.f0", print_hash_value);
    transparent_crc(g_385.f1, "g_385.f1", print_hash_value);
    transparent_crc(g_385.f2, "g_385.f2", print_hash_value);
    transparent_crc(g_385.f3, "g_385.f3", print_hash_value);
    transparent_crc(g_385.f4, "g_385.f4", print_hash_value);
    transparent_crc(g_385.f5, "g_385.f5", print_hash_value);
    transparent_crc(g_385.f6, "g_385.f6", print_hash_value);
    transparent_crc(g_418, "g_418", print_hash_value);
    transparent_crc(g_436, "g_436", print_hash_value);
    transparent_crc(g_459.f0, "g_459.f0", print_hash_value);
    transparent_crc(g_459.f1, "g_459.f1", print_hash_value);
    transparent_crc(g_459.f2, "g_459.f2", print_hash_value);
    transparent_crc(g_459.f3, "g_459.f3", print_hash_value);
    transparent_crc(g_459.f4, "g_459.f4", print_hash_value);
    transparent_crc(g_459.f5, "g_459.f5", print_hash_value);
    transparent_crc(g_459.f6, "g_459.f6", print_hash_value);
    transparent_crc(g_533.f0, "g_533.f0", print_hash_value);
    transparent_crc(g_533.f1, "g_533.f1", print_hash_value);
    transparent_crc(g_533.f2, "g_533.f2", print_hash_value);
    transparent_crc(g_533.f3, "g_533.f3", print_hash_value);
    transparent_crc(g_533.f4, "g_533.f4", print_hash_value);
    transparent_crc(g_533.f5, "g_533.f5", print_hash_value);
    transparent_crc(g_533.f6, "g_533.f6", print_hash_value);
    transparent_crc(g_534.f0, "g_534.f0", print_hash_value);
    transparent_crc(g_534.f1, "g_534.f1", print_hash_value);
    transparent_crc(g_534.f2, "g_534.f2", print_hash_value);
    transparent_crc(g_534.f3, "g_534.f3", print_hash_value);
    transparent_crc(g_534.f4, "g_534.f4", print_hash_value);
    transparent_crc(g_534.f5, "g_534.f5", print_hash_value);
    transparent_crc(g_534.f6, "g_534.f6", print_hash_value);
    transparent_crc(g_551.f0, "g_551.f0", print_hash_value);
    transparent_crc(g_551.f1, "g_551.f1", print_hash_value);
    transparent_crc(g_623.f0, "g_623.f0", print_hash_value);
    transparent_crc(g_623.f1, "g_623.f1", print_hash_value);
    transparent_crc(g_623.f2, "g_623.f2", print_hash_value);
    transparent_crc(g_623.f3, "g_623.f3", print_hash_value);
    transparent_crc(g_623.f4, "g_623.f4", print_hash_value);
    transparent_crc(g_623.f5, "g_623.f5", print_hash_value);
    transparent_crc(g_623.f6, "g_623.f6", print_hash_value);
    transparent_crc(g_623.f7, "g_623.f7", print_hash_value);
    transparent_crc(g_623.f8, "g_623.f8", print_hash_value);
    transparent_crc(g_623.f9, "g_623.f9", print_hash_value);
    transparent_crc(g_624.f0, "g_624.f0", print_hash_value);
    transparent_crc(g_624.f1, "g_624.f1", print_hash_value);
    transparent_crc(g_624.f2, "g_624.f2", print_hash_value);
    transparent_crc(g_624.f3, "g_624.f3", print_hash_value);
    transparent_crc(g_624.f4, "g_624.f4", print_hash_value);
    transparent_crc(g_624.f5, "g_624.f5", print_hash_value);
    transparent_crc(g_624.f6, "g_624.f6", print_hash_value);
    transparent_crc(g_624.f7, "g_624.f7", print_hash_value);
    transparent_crc(g_624.f8, "g_624.f8", print_hash_value);
    transparent_crc(g_624.f9, "g_624.f9", print_hash_value);
    transparent_crc(g_625.f0, "g_625.f0", print_hash_value);
    transparent_crc(g_625.f1, "g_625.f1", print_hash_value);
    transparent_crc(g_625.f2, "g_625.f2", print_hash_value);
    transparent_crc(g_625.f3, "g_625.f3", print_hash_value);
    transparent_crc(g_625.f4, "g_625.f4", print_hash_value);
    transparent_crc(g_625.f5, "g_625.f5", print_hash_value);
    transparent_crc(g_625.f6, "g_625.f6", print_hash_value);
    transparent_crc(g_625.f7, "g_625.f7", print_hash_value);
    transparent_crc(g_625.f8, "g_625.f8", print_hash_value);
    transparent_crc(g_625.f9, "g_625.f9", print_hash_value);
    transparent_crc(g_626.f0, "g_626.f0", print_hash_value);
    transparent_crc(g_626.f1, "g_626.f1", print_hash_value);
    transparent_crc(g_626.f2, "g_626.f2", print_hash_value);
    transparent_crc(g_626.f3, "g_626.f3", print_hash_value);
    transparent_crc(g_626.f4, "g_626.f4", print_hash_value);
    transparent_crc(g_626.f5, "g_626.f5", print_hash_value);
    transparent_crc(g_626.f6, "g_626.f6", print_hash_value);
    transparent_crc(g_626.f7, "g_626.f7", print_hash_value);
    transparent_crc(g_626.f8, "g_626.f8", print_hash_value);
    transparent_crc(g_626.f9, "g_626.f9", print_hash_value);
    transparent_crc(g_642.f0, "g_642.f0", print_hash_value);
    transparent_crc(g_642.f1, "g_642.f1", print_hash_value);
    transparent_crc(g_642.f2, "g_642.f2", print_hash_value);
    transparent_crc(g_642.f3, "g_642.f3", print_hash_value);
    transparent_crc(g_642.f4, "g_642.f4", print_hash_value);
    transparent_crc(g_642.f5, "g_642.f5", print_hash_value);
    transparent_crc(g_642.f6, "g_642.f6", print_hash_value);
    transparent_crc(g_676.f0, "g_676.f0", print_hash_value);
    transparent_crc(g_676.f1, "g_676.f1", print_hash_value);
    transparent_crc(g_676.f2, "g_676.f2", print_hash_value);
    transparent_crc(g_676.f3, "g_676.f3", print_hash_value);
    transparent_crc(g_676.f4, "g_676.f4", print_hash_value);
    transparent_crc(g_676.f5, "g_676.f5", print_hash_value);
    transparent_crc(g_676.f6, "g_676.f6", print_hash_value);
    transparent_crc(g_741, "g_741", print_hash_value);
    transparent_crc(g_780.f0, "g_780.f0", print_hash_value);
    transparent_crc(g_780.f1, "g_780.f1", print_hash_value);
    transparent_crc(g_780.f2, "g_780.f2", print_hash_value);
    transparent_crc(g_780.f3, "g_780.f3", print_hash_value);
    transparent_crc(g_780.f4, "g_780.f4", print_hash_value);
    transparent_crc(g_780.f5, "g_780.f5", print_hash_value);
    transparent_crc(g_780.f6, "g_780.f6", print_hash_value);
    transparent_crc(g_780.f7, "g_780.f7", print_hash_value);
    transparent_crc(g_780.f8, "g_780.f8", print_hash_value);
    transparent_crc(g_780.f9, "g_780.f9", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_781[i].f0, "g_781[i].f0", print_hash_value);
        transparent_crc(g_781[i].f1, "g_781[i].f1", print_hash_value);
        transparent_crc(g_781[i].f2, "g_781[i].f2", print_hash_value);
        transparent_crc(g_781[i].f3, "g_781[i].f3", print_hash_value);
        transparent_crc(g_781[i].f4, "g_781[i].f4", print_hash_value);
        transparent_crc(g_781[i].f5, "g_781[i].f5", print_hash_value);
        transparent_crc(g_781[i].f6, "g_781[i].f6", print_hash_value);
        transparent_crc(g_781[i].f7, "g_781[i].f7", print_hash_value);
        transparent_crc(g_781[i].f8, "g_781[i].f8", print_hash_value);
        transparent_crc(g_781[i].f9, "g_781[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_783[i].f0, "g_783[i].f0", print_hash_value);
        transparent_crc(g_783[i].f1, "g_783[i].f1", print_hash_value);
        transparent_crc(g_783[i].f2, "g_783[i].f2", print_hash_value);
        transparent_crc(g_783[i].f3, "g_783[i].f3", print_hash_value);
        transparent_crc(g_783[i].f4, "g_783[i].f4", print_hash_value);
        transparent_crc(g_783[i].f5, "g_783[i].f5", print_hash_value);
        transparent_crc(g_783[i].f6, "g_783[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_858[i][j], "g_858[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_872.f0, "g_872.f0", print_hash_value);
    transparent_crc(g_872.f1, "g_872.f1", print_hash_value);
    transparent_crc(g_896.f0, "g_896.f0", print_hash_value);
    transparent_crc(g_896.f1, "g_896.f1", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_906[i], "g_906[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_907, "g_907", print_hash_value);
    transparent_crc(g_908, "g_908", print_hash_value);
    transparent_crc(g_909, "g_909", print_hash_value);
    transparent_crc(g_910, "g_910", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_911[i], "g_911[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_919.f0, "g_919.f0", print_hash_value);
    transparent_crc(g_919.f1, "g_919.f1", print_hash_value);
    transparent_crc(g_926.f0, "g_926.f0", print_hash_value);
    transparent_crc(g_926.f1, "g_926.f1", print_hash_value);
    transparent_crc(g_926.f2, "g_926.f2", print_hash_value);
    transparent_crc(g_926.f3, "g_926.f3", print_hash_value);
    transparent_crc(g_926.f4, "g_926.f4", print_hash_value);
    transparent_crc(g_926.f5, "g_926.f5", print_hash_value);
    transparent_crc(g_926.f6, "g_926.f6", print_hash_value);
    transparent_crc(g_926.f7, "g_926.f7", print_hash_value);
    transparent_crc(g_926.f8, "g_926.f8", print_hash_value);
    transparent_crc(g_926.f9, "g_926.f9", print_hash_value);
    transparent_crc(g_933, "g_933", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_946[i][j].f0, "g_946[i][j].f0", print_hash_value);
            transparent_crc(g_946[i][j].f1, "g_946[i][j].f1", print_hash_value);
            transparent_crc(g_946[i][j].f2, "g_946[i][j].f2", print_hash_value);
            transparent_crc(g_946[i][j].f3, "g_946[i][j].f3", print_hash_value);
            transparent_crc(g_946[i][j].f4, "g_946[i][j].f4", print_hash_value);
            transparent_crc(g_946[i][j].f5, "g_946[i][j].f5", print_hash_value);
            transparent_crc(g_946[i][j].f6, "g_946[i][j].f6", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_957[i].f0, "g_957[i].f0", print_hash_value);
        transparent_crc(g_957[i].f1, "g_957[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_958.f0, "g_958.f0", print_hash_value);
    transparent_crc(g_958.f1, "g_958.f1", print_hash_value);
    transparent_crc(g_958.f2, "g_958.f2", print_hash_value);
    transparent_crc(g_958.f3, "g_958.f3", print_hash_value);
    transparent_crc(g_958.f4, "g_958.f4", print_hash_value);
    transparent_crc(g_958.f5, "g_958.f5", print_hash_value);
    transparent_crc(g_958.f6, "g_958.f6", print_hash_value);
    transparent_crc(g_958.f7, "g_958.f7", print_hash_value);
    transparent_crc(g_958.f8, "g_958.f8", print_hash_value);
    transparent_crc(g_958.f9, "g_958.f9", print_hash_value);
    transparent_crc(g_978, "g_978", print_hash_value);
    transparent_crc(g_1024, "g_1024", print_hash_value);
    transparent_crc(g_1086.f0, "g_1086.f0", print_hash_value);
    transparent_crc(g_1086.f1, "g_1086.f1", print_hash_value);
    transparent_crc(g_1086.f2, "g_1086.f2", print_hash_value);
    transparent_crc(g_1086.f3, "g_1086.f3", print_hash_value);
    transparent_crc(g_1086.f4, "g_1086.f4", print_hash_value);
    transparent_crc(g_1086.f5, "g_1086.f5", print_hash_value);
    transparent_crc(g_1086.f6, "g_1086.f6", print_hash_value);
    transparent_crc(g_1199.f0, "g_1199.f0", print_hash_value);
    transparent_crc(g_1199.f1, "g_1199.f1", print_hash_value);
    transparent_crc(g_1199.f2, "g_1199.f2", print_hash_value);
    transparent_crc(g_1199.f3, "g_1199.f3", print_hash_value);
    transparent_crc(g_1199.f4, "g_1199.f4", print_hash_value);
    transparent_crc(g_1199.f5, "g_1199.f5", print_hash_value);
    transparent_crc(g_1199.f6, "g_1199.f6", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1276[i][j][k].f0, "g_1276[i][j][k].f0", print_hash_value);
                transparent_crc(g_1276[i][j][k].f1, "g_1276[i][j][k].f1", print_hash_value);
                transparent_crc(g_1276[i][j][k].f2, "g_1276[i][j][k].f2", print_hash_value);
                transparent_crc(g_1276[i][j][k].f3, "g_1276[i][j][k].f3", print_hash_value);
                transparent_crc(g_1276[i][j][k].f4, "g_1276[i][j][k].f4", print_hash_value);
                transparent_crc(g_1276[i][j][k].f5, "g_1276[i][j][k].f5", print_hash_value);
                transparent_crc(g_1276[i][j][k].f6, "g_1276[i][j][k].f6", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1289[i], "g_1289[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1307.f0, "g_1307.f0", print_hash_value);
    transparent_crc(g_1307.f1, "g_1307.f1", print_hash_value);
    transparent_crc(g_1307.f2, "g_1307.f2", print_hash_value);
    transparent_crc(g_1307.f3, "g_1307.f3", print_hash_value);
    transparent_crc(g_1307.f4, "g_1307.f4", print_hash_value);
    transparent_crc(g_1307.f5, "g_1307.f5", print_hash_value);
    transparent_crc(g_1307.f6, "g_1307.f6", print_hash_value);
    transparent_crc(g_1326, "g_1326", print_hash_value);
    transparent_crc(g_1331, "g_1331", print_hash_value);
    transparent_crc(g_1392, "g_1392", print_hash_value);
    transparent_crc(g_1397, "g_1397", print_hash_value);
    transparent_crc(g_1420, "g_1420", print_hash_value);
    transparent_crc(g_1429.f0, "g_1429.f0", print_hash_value);
    transparent_crc(g_1429.f1, "g_1429.f1", print_hash_value);
    transparent_crc(g_1429.f2, "g_1429.f2", print_hash_value);
    transparent_crc(g_1429.f3, "g_1429.f3", print_hash_value);
    transparent_crc(g_1429.f4, "g_1429.f4", print_hash_value);
    transparent_crc(g_1429.f5, "g_1429.f5", print_hash_value);
    transparent_crc(g_1429.f6, "g_1429.f6", print_hash_value);
    transparent_crc(g_1437.f0, "g_1437.f0", print_hash_value);
    transparent_crc(g_1437.f1, "g_1437.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1470[i], "g_1470[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1506.f0, "g_1506.f0", print_hash_value);
    transparent_crc(g_1506.f1, "g_1506.f1", print_hash_value);
    transparent_crc(g_1506.f2, "g_1506.f2", print_hash_value);
    transparent_crc(g_1506.f3, "g_1506.f3", print_hash_value);
    transparent_crc(g_1506.f4, "g_1506.f4", print_hash_value);
    transparent_crc(g_1506.f5, "g_1506.f5", print_hash_value);
    transparent_crc(g_1506.f6, "g_1506.f6", print_hash_value);
    transparent_crc(g_1506.f7, "g_1506.f7", print_hash_value);
    transparent_crc(g_1506.f8, "g_1506.f8", print_hash_value);
    transparent_crc(g_1506.f9, "g_1506.f9", print_hash_value);
    transparent_crc(g_1508.f0, "g_1508.f0", print_hash_value);
    transparent_crc(g_1508.f1, "g_1508.f1", print_hash_value);
    transparent_crc(g_1508.f2, "g_1508.f2", print_hash_value);
    transparent_crc(g_1508.f3, "g_1508.f3", print_hash_value);
    transparent_crc(g_1508.f4, "g_1508.f4", print_hash_value);
    transparent_crc(g_1508.f5, "g_1508.f5", print_hash_value);
    transparent_crc(g_1508.f6, "g_1508.f6", print_hash_value);
    transparent_crc(g_1508.f7, "g_1508.f7", print_hash_value);
    transparent_crc(g_1508.f8, "g_1508.f8", print_hash_value);
    transparent_crc(g_1508.f9, "g_1508.f9", print_hash_value);
    transparent_crc(g_1510.f0, "g_1510.f0", print_hash_value);
    transparent_crc(g_1510.f1, "g_1510.f1", print_hash_value);
    transparent_crc(g_1510.f2, "g_1510.f2", print_hash_value);
    transparent_crc(g_1510.f3, "g_1510.f3", print_hash_value);
    transparent_crc(g_1510.f4, "g_1510.f4", print_hash_value);
    transparent_crc(g_1510.f5, "g_1510.f5", print_hash_value);
    transparent_crc(g_1510.f6, "g_1510.f6", print_hash_value);
    transparent_crc(g_1510.f7, "g_1510.f7", print_hash_value);
    transparent_crc(g_1510.f8, "g_1510.f8", print_hash_value);
    transparent_crc(g_1510.f9, "g_1510.f9", print_hash_value);
    transparent_crc(g_1517.f0, "g_1517.f0", print_hash_value);
    transparent_crc(g_1517.f1, "g_1517.f1", print_hash_value);
    transparent_crc(g_1519.f0, "g_1519.f0", print_hash_value);
    transparent_crc(g_1519.f1, "g_1519.f1", print_hash_value);
    transparent_crc(g_1538.f0, "g_1538.f0", print_hash_value);
    transparent_crc(g_1538.f1, "g_1538.f1", print_hash_value);
    transparent_crc(g_1538.f2, "g_1538.f2", print_hash_value);
    transparent_crc(g_1538.f3, "g_1538.f3", print_hash_value);
    transparent_crc(g_1538.f4, "g_1538.f4", print_hash_value);
    transparent_crc(g_1538.f5, "g_1538.f5", print_hash_value);
    transparent_crc(g_1538.f6, "g_1538.f6", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1550[i][j].f0, "g_1550[i][j].f0", print_hash_value);
            transparent_crc(g_1550[i][j].f1, "g_1550[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1553, "g_1553", print_hash_value);
    transparent_crc(g_1616.f0, "g_1616.f0", print_hash_value);
    transparent_crc(g_1616.f1, "g_1616.f1", print_hash_value);
    transparent_crc(g_1629.f0, "g_1629.f0", print_hash_value);
    transparent_crc(g_1629.f1, "g_1629.f1", print_hash_value);
    transparent_crc(g_1629.f2, "g_1629.f2", print_hash_value);
    transparent_crc(g_1629.f3, "g_1629.f3", print_hash_value);
    transparent_crc(g_1629.f4, "g_1629.f4", print_hash_value);
    transparent_crc(g_1629.f5, "g_1629.f5", print_hash_value);
    transparent_crc(g_1629.f6, "g_1629.f6", print_hash_value);
    transparent_crc(g_1711, "g_1711", print_hash_value);
    transparent_crc(g_1793.f0, "g_1793.f0", print_hash_value);
    transparent_crc(g_1793.f1, "g_1793.f1", print_hash_value);
    transparent_crc(g_1793.f2, "g_1793.f2", print_hash_value);
    transparent_crc(g_1793.f3, "g_1793.f3", print_hash_value);
    transparent_crc(g_1793.f4, "g_1793.f4", print_hash_value);
    transparent_crc(g_1793.f5, "g_1793.f5", print_hash_value);
    transparent_crc(g_1793.f6, "g_1793.f6", print_hash_value);
    transparent_crc(g_1793.f7, "g_1793.f7", print_hash_value);
    transparent_crc(g_1793.f8, "g_1793.f8", print_hash_value);
    transparent_crc(g_1793.f9, "g_1793.f9", print_hash_value);
    transparent_crc(g_1794.f0, "g_1794.f0", print_hash_value);
    transparent_crc(g_1794.f1, "g_1794.f1", print_hash_value);
    transparent_crc(g_1895.f0, "g_1895.f0", print_hash_value);
    transparent_crc(g_1895.f1, "g_1895.f1", print_hash_value);
    transparent_crc(g_1895.f2, "g_1895.f2", print_hash_value);
    transparent_crc(g_1895.f3, "g_1895.f3", print_hash_value);
    transparent_crc(g_1895.f4, "g_1895.f4", print_hash_value);
    transparent_crc(g_1895.f5, "g_1895.f5", print_hash_value);
    transparent_crc(g_1895.f6, "g_1895.f6", print_hash_value);
    transparent_crc(g_1895.f7, "g_1895.f7", print_hash_value);
    transparent_crc(g_1895.f8, "g_1895.f8", print_hash_value);
    transparent_crc(g_1895.f9, "g_1895.f9", print_hash_value);
    transparent_crc(g_1968.f0, "g_1968.f0", print_hash_value);
    transparent_crc(g_1968.f1, "g_1968.f1", print_hash_value);
    transparent_crc(g_1968.f2, "g_1968.f2", print_hash_value);
    transparent_crc(g_1968.f3, "g_1968.f3", print_hash_value);
    transparent_crc(g_1968.f4, "g_1968.f4", print_hash_value);
    transparent_crc(g_1968.f5, "g_1968.f5", print_hash_value);
    transparent_crc(g_1968.f6, "g_1968.f6", print_hash_value);
    transparent_crc(g_2002.f0, "g_2002.f0", print_hash_value);
    transparent_crc(g_2002.f1, "g_2002.f1", print_hash_value);
    transparent_crc(g_2027, "g_2027", print_hash_value);
    transparent_crc(g_2056.f0, "g_2056.f0", print_hash_value);
    transparent_crc(g_2056.f1, "g_2056.f1", print_hash_value);
    transparent_crc(g_2086.f0, "g_2086.f0", print_hash_value);
    transparent_crc(g_2086.f1, "g_2086.f1", print_hash_value);
    transparent_crc(g_2086.f2, "g_2086.f2", print_hash_value);
    transparent_crc(g_2086.f3, "g_2086.f3", print_hash_value);
    transparent_crc(g_2086.f4, "g_2086.f4", print_hash_value);
    transparent_crc(g_2086.f5, "g_2086.f5", print_hash_value);
    transparent_crc(g_2086.f6, "g_2086.f6", print_hash_value);
    transparent_crc(g_2086.f7, "g_2086.f7", print_hash_value);
    transparent_crc(g_2086.f8, "g_2086.f8", print_hash_value);
    transparent_crc(g_2086.f9, "g_2086.f9", print_hash_value);
    transparent_crc(g_2098.f0, "g_2098.f0", print_hash_value);
    transparent_crc(g_2098.f1, "g_2098.f1", print_hash_value);
    transparent_crc(g_2173.f0, "g_2173.f0", print_hash_value);
    transparent_crc(g_2173.f1, "g_2173.f1", print_hash_value);
    transparent_crc(g_2209.f0, "g_2209.f0", print_hash_value);
    transparent_crc(g_2209.f1, "g_2209.f1", print_hash_value);
    transparent_crc(g_2209.f2, "g_2209.f2", print_hash_value);
    transparent_crc(g_2209.f3, "g_2209.f3", print_hash_value);
    transparent_crc(g_2209.f4, "g_2209.f4", print_hash_value);
    transparent_crc(g_2209.f5, "g_2209.f5", print_hash_value);
    transparent_crc(g_2209.f6, "g_2209.f6", print_hash_value);
    transparent_crc(g_2209.f7, "g_2209.f7", print_hash_value);
    transparent_crc(g_2209.f8, "g_2209.f8", print_hash_value);
    transparent_crc(g_2209.f9, "g_2209.f9", print_hash_value);
    transparent_crc(g_2220, "g_2220", print_hash_value);
    transparent_crc(g_2257, "g_2257", print_hash_value);
    transparent_crc(g_2290, "g_2290", print_hash_value);
    transparent_crc(g_2338, "g_2338", print_hash_value);
    transparent_crc(g_2410.f0, "g_2410.f0", print_hash_value);
    transparent_crc(g_2410.f1, "g_2410.f1", print_hash_value);
    transparent_crc(g_2410.f2, "g_2410.f2", print_hash_value);
    transparent_crc(g_2410.f3, "g_2410.f3", print_hash_value);
    transparent_crc(g_2410.f4, "g_2410.f4", print_hash_value);
    transparent_crc(g_2410.f5, "g_2410.f5", print_hash_value);
    transparent_crc(g_2410.f6, "g_2410.f6", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 656
   depth: 1, occurrence: 40
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 18
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 3
XXX volatile bitfields defined in structs: 7
XXX structs with bitfields in the program: 82
breakdown:
   indirect level: 0, occurrence: 40
   indirect level: 1, occurrence: 29
   indirect level: 2, occurrence: 9
   indirect level: 3, occurrence: 4
XXX full-bitfields structs in the program: 24
breakdown:
   indirect level: 0, occurrence: 24
XXX times a bitfields struct's address is taken: 165
XXX times a bitfields struct on LHS: 1
XXX times a bitfields struct on RHS: 47
XXX times a single bitfield on LHS: 7
XXX times a single bitfield on RHS: 140

XXX max expression depth: 50
breakdown:
   depth: 1, occurrence: 429
   depth: 2, occurrence: 87
   depth: 3, occurrence: 3
   depth: 4, occurrence: 6
   depth: 5, occurrence: 3
   depth: 6, occurrence: 4
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 2
   depth: 15, occurrence: 3
   depth: 16, occurrence: 3
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 5
   depth: 20, occurrence: 9
   depth: 21, occurrence: 6
   depth: 22, occurrence: 3
   depth: 23, occurrence: 2
   depth: 24, occurrence: 3
   depth: 25, occurrence: 4
   depth: 26, occurrence: 1
   depth: 27, occurrence: 4
   depth: 28, occurrence: 4
   depth: 29, occurrence: 3
   depth: 30, occurrence: 3
   depth: 31, occurrence: 4
   depth: 32, occurrence: 2
   depth: 33, occurrence: 4
   depth: 34, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 42, occurrence: 1
   depth: 44, occurrence: 1
   depth: 50, occurrence: 1

XXX total number of pointers: 548

XXX times a variable address is taken: 1098
XXX times a pointer is dereferenced on RHS: 208
breakdown:
   depth: 1, occurrence: 128
   depth: 2, occurrence: 56
   depth: 3, occurrence: 22
   depth: 4, occurrence: 2
XXX times a pointer is dereferenced on LHS: 323
breakdown:
   depth: 1, occurrence: 270
   depth: 2, occurrence: 39
   depth: 3, occurrence: 5
   depth: 4, occurrence: 9
XXX times a pointer is compared with null: 47
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 9832

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1156
   level: 2, occurrence: 341
   level: 3, occurrence: 138
   level: 4, occurrence: 79
   level: 5, occurrence: 12
XXX number of pointers point to pointers: 266
XXX number of pointers point to scalars: 232
XXX number of pointers point to structs: 50
XXX percent of pointers has null in alias set: 30.1
XXX average alias set size: 1.38

XXX times a non-volatile is read: 1718
XXX times a non-volatile is write: 917
XXX times a volatile is read: 191
XXX    times read thru a pointer: 71
XXX times a volatile is write: 109
XXX    times written thru a pointer: 65
XXX times a volatile is available for access: 7.12e+03
XXX percentage of non-volatile access: 89.8

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 399
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 34
   depth: 2, occurrence: 34
   depth: 3, occurrence: 67
   depth: 4, occurrence: 99
   depth: 5, occurrence: 134

XXX percentage a fresh-made variable is used: 17.4
XXX percentage an existing variable is used: 82.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
XXX total OOB instances added: 0
********************* end of statistics **********************/


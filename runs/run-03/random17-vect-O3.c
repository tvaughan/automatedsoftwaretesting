/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      2911022467
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int32_t  f0;
   const volatile int16_t  f1;
   const volatile signed f2 : 7;
   const uint64_t  f3;
   uint32_t  f4;
};

/* --- GLOBAL VARIABLES --- */
static int8_t g_8 = 0L;
static int32_t g_11 = 0xBB9A67B9L;
static int32_t g_50 = 1L;
static uint16_t g_58[5][8][6] = {{{0x2563L,0x2563L,65526UL,0x4949L,0x0E64L,0x38AAL},{65535UL,0x38AAL,65535UL,1UL,1UL,65526UL},{65526UL,65535UL,65535UL,0xAAC4L,0x2563L,0x38AAL},{0x040FL,0xAAC4L,65526UL,0xE4BBL,0x04C3L,0x0E64L},{0xE4BBL,0x04C3L,0x0E64L,0xE0A1L,0x0730L,0xE0A1L},{0x19B5L,0x040FL,0x19B5L,65535UL,0xB362L,0xCE2AL},{0xB362L,0xE0A1L,1UL,65535UL,0x8F9BL,0x5AEEL},{0x04C3L,65535UL,0x355DL,65535UL,0xE0A1L,65535UL}},{{65526UL,1UL,1UL,65535UL,0x38AAL,65535UL},{65535UL,0x04C3L,0xCE2AL,1UL,0x8F9BL,0x8F9BL},{65527UL,0x38AAL,0x38AAL,65527UL,0x040FL,0x2603L},{0xE4BBL,65535UL,0UL,0xCE2AL,1UL,65526UL},{0x2563L,0x355DL,65527UL,0x8F9BL,1UL,0x0730L},{0x0E64L,65535UL,0x66BEL,0xB362L,0x040FL,1UL},{5UL,0x38AAL,0xAAC4L,0x0730L,0x8F9BL,0xB362L},{0x38AAL,0x04C3L,1UL,0x04C3L,0x38AAL,65535UL}},{{1UL,1UL,0xE4BBL,0x2563L,0xE0A1L,0x38AAL},{0x04C3L,0x040FL,0x4949L,1UL,0UL,0x38AAL},{0xEC61L,1UL,0xE4BBL,0x0E64L,65526UL,65535UL},{0UL,0xE4BBL,1UL,0x355DL,0x4949L,0xB362L},{1UL,0x66BEL,0xAAC4L,0xAAC4L,0x66BEL,1UL},{0xE0A1L,0xCE2AL,0x66BEL,1UL,5UL,0x0730L},{0xB362L,0x0E64L,65527UL,0UL,0x04C3L,65526UL},{0xB362L,0x5AEEL,0UL,1UL,0x355DL,0x2603L}},{{0xE0A1L,5UL,0x38AAL,0xAAC4L,0x0730L,0x8F9BL},{1UL,65535UL,0xCE2AL,0x355DL,0xCE2AL,65535UL},{0UL,0xB362L,1UL,0x0E64L,1UL,65535UL},{0xEC61L,65535UL,0x0730L,1UL,0x2603L,1UL},{0x04C3L,65535UL,65535UL,0x2563L,1UL,0xE0A1L},{1UL,0xB362L,65535UL,0x04C3L,0xCE2AL,1UL},{0x38AAL,65535UL,0x355DL,0x0730L,0x0730L,0x355DL},{5UL,5UL,0x2563L,0xB362L,0x355DL,0x5AEEL}},{{0x0E64L,0x5AEEL,0x040FL,0x8F9BL,0x04C3L,0x2563L},{0x2563L,0x0E64L,0x040FL,0xCE2AL,5UL,0x5AEEL},{0xE4BBL,0xCE2AL,0x2563L,65527UL,0x66BEL,0x355DL},{65527UL,0x66BEL,0x355DL,1UL,0x4949L,1UL},{65535UL,0xE4BBL,65535UL,65535UL,65526UL,0xE0A1L},{65526UL,1UL,65535UL,0xEC61L,0UL,1UL},{0x66BEL,0x040FL,0x0730L,0xEC61L,0xE0A1L,65535UL},{65526UL,1UL,1UL,0x040FL,0x5AEEL,0x0E64L}}};
static uint32_t g_64[5] = {0UL,0UL,0UL,0UL,0UL};
static volatile union U0 g_72 = {0x41605789L};/* VOLATILE GLOBAL g_72 */
static volatile int32_t g_88[6][2][3] = {{{0xCD4103A8L,0x23B65C4EL,0xFBC0B842L},{0x6F30DCE9L,0x23B65C4EL,0L}},{{0x065BCAC1L,0x23B65C4EL,0x23B65C4EL},{0xCD4103A8L,0x23B65C4EL,0xFBC0B842L}},{{0x6F30DCE9L,0x23B65C4EL,0L},{0x065BCAC1L,0x23B65C4EL,0x23B65C4EL}},{{0xCD4103A8L,0x23B65C4EL,0xFBC0B842L},{0x6F30DCE9L,0x23B65C4EL,0L}},{{0x065BCAC1L,0x23B65C4EL,0x23B65C4EL},{0xCD4103A8L,0x23B65C4EL,0xFBC0B842L}},{{0x6F30DCE9L,0x23B65C4EL,0L},{0x065BCAC1L,0x23B65C4EL,0x23B65C4EL}}};
static int32_t g_120 = 0xCA476E1AL;
static int64_t g_143 = 1L;
static int64_t g_145 = (-1L);
static uint64_t g_150 = 0x535500DB5DDC8211LL;
static volatile int16_t g_155 = 0x871FL;/* VOLATILE GLOBAL g_155 */
static uint8_t g_172 = 0x46L;
static int8_t g_204 = 0xE2L;
static uint32_t g_210 = 0xAAC1A399L;
static int32_t *g_214 = &g_50;
static int32_t ** volatile g_213 = &g_214;/* VOLATILE GLOBAL g_213 */
static int32_t g_238 = (-3L);
static volatile int32_t * volatile *g_248 = (void*)0;
static uint8_t g_250 = 0xCBL;
static int32_t g_252[8][2] = {{1L,0xB63AA831L},{1L,1L},{0xB63AA831L,1L},{1L,0xB63AA831L},{1L,1L},{0xB63AA831L,1L},{1L,0xB63AA831L},{1L,1L}};
static int32_t ** volatile g_256 = &g_214;/* VOLATILE GLOBAL g_256 */
static int64_t *g_261 = &g_145;
static uint8_t * const ** volatile g_268 = (void*)0;/* VOLATILE GLOBAL g_268 */
static uint8_t * const g_271 = &g_250;
static uint8_t * const *g_270 = &g_271;
static uint8_t * const ** volatile g_269 = &g_270;/* VOLATILE GLOBAL g_269 */
static uint32_t g_275 = 0UL;
static int16_t g_280 = (-3L);
static int16_t *g_279 = &g_280;
static int16_t * volatile *g_278[7][1][6] = {{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}},{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}},{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}},{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}},{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}},{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}},{{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279}}};
static int16_t * volatile ** volatile g_281[7] = {&g_278[5][0][2],&g_278[5][0][2],&g_278[5][0][2],&g_278[5][0][2],&g_278[5][0][2],&g_278[5][0][2],&g_278[5][0][2]};
static int16_t * volatile ** volatile g_282 = &g_278[6][0][4];/* VOLATILE GLOBAL g_282 */
static int16_t **g_319 = &g_279;
static int16_t ***g_318 = &g_319;
static int16_t ** const *g_340 = &g_319;
static int32_t *g_398 = &g_120;
static int32_t * const *g_397[10] = {&g_398,&g_398,&g_398,&g_398,&g_398,&g_398,&g_398,&g_398,&g_398,&g_398};
static int32_t * const ** volatile g_396 = &g_397[5];/* VOLATILE GLOBAL g_396 */
static volatile uint8_t g_402 = 255UL;/* VOLATILE GLOBAL g_402 */
static const int32_t *g_435 = (void*)0;
static const int32_t ** volatile g_434 = &g_435;/* VOLATILE GLOBAL g_434 */
static const uint64_t g_441[8] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
static uint8_t g_465 = 255UL;
static uint16_t g_470 = 0xE1FDL;
static uint16_t *g_476 = &g_58[2][1][2];
static uint16_t **g_475 = &g_476;
static uint16_t *** volatile g_474 = &g_475;/* VOLATILE GLOBAL g_474 */
static int32_t g_519[3][6][7] = {{{0x18BAEBD4L,0x4E005F0DL,(-1L),4L,0x5A63E456L,5L,0xA8DC9678L},{4L,0x77F4D5EFL,0L,5L,5L,0L,0x77F4D5EFL},{0x18BAEBD4L,4L,0x4E005F0DL,0xC4B1949DL,1L,0L,0xC863A66AL},{(-6L),0xBB65886CL,(-8L),(-1L),1L,5L,1L},{0xC4B1949DL,1L,1L,0xC4B1949DL,0x04F6EDACL,0xC863A66AL,(-1L)},{0xC863A66AL,1L,(-6L),5L,0x18BAEBD4L,0x04F6EDACL,0x5A63E456L}},{{(-1L),0xBB65886CL,0xC863A66AL,4L,0xC863A66AL,0xBB65886CL,(-1L)},{1L,4L,0xBB65886CL,0x5A63E456L,0xC863A66AL,0x77F4D5EFL,1L},{0xA8DC9678L,0x77F4D5EFL,5L,0xC863A66AL,0x18BAEBD4L,0x18BAEBD4L,0xC863A66AL},{0x04F6EDACL,0x77F4D5EFL,0x04F6EDACL,1L,0x4E005F0DL,0x5A63E456L,0xA8DC9678L},{0x04F6EDACL,0xC863A66AL,(-1L),0xA8DC9678L,(-8L),0x77F4D5EFL,0x5A63E456L},{0x5A63E456L,1L,0xBB65886CL,0xBB65886CL,1L,0x5A63E456L,0x18BAEBD4L}},{{1L,(-1L),(-8L),0xBB65886CL,(-6L),0L,0x4E005F0DL},{4L,0x5A63E456L,5L,0xA8DC9678L,0xC863A66AL,0xA8DC9678L,5L},{(-1L),(-1L),0x77F4D5EFL,1L,0xBB65886CL,0x04F6EDACL,5L},{0x18BAEBD4L,1L,1L,(-1L),5L,0x4E005F0DL,0x4E005F0DL},{0xBB65886CL,0xC863A66AL,4L,0xC863A66AL,0xBB65886CL,(-1L),0x18BAEBD4L},{0L,0x77F4D5EFL,4L,0xC4B1949DL,0xC863A66AL,(-6L),0x5A63E456L}}};
static volatile union U0 *g_538[5] = {&g_72,&g_72,&g_72,&g_72,&g_72};
static volatile union U0 ** volatile g_537[3] = {&g_538[3],&g_538[3],&g_538[3]};
static volatile union U0 ** volatile g_539 = (void*)0;/* VOLATILE GLOBAL g_539 */
static volatile union U0 ** volatile g_540 = &g_538[2];/* VOLATILE GLOBAL g_540 */
static const uint8_t g_588 = 0x24L;
static volatile int8_t g_651 = 0L;/* VOLATILE GLOBAL g_651 */
static volatile int8_t * volatile g_650 = &g_651;/* VOLATILE GLOBAL g_650 */
static volatile int8_t * volatile * volatile g_649 = &g_650;/* VOLATILE GLOBAL g_649 */
static volatile uint64_t g_656 = 0xE3861B9F40AA37B4LL;/* VOLATILE GLOBAL g_656 */
static volatile uint64_t *g_655 = &g_656;
static volatile uint64_t * volatile *g_654 = &g_655;
static int32_t ** volatile g_671 = &g_214;/* VOLATILE GLOBAL g_671 */
static union U0 g_690[6] = {{0x961912F3L},{0x961912F3L},{0x961912F3L},{0x961912F3L},{0x961912F3L},{0x961912F3L}};
static union U0 g_704 = {-3L};/* VOLATILE GLOBAL g_704 */
static int8_t g_714 = 0xF6L;
static const volatile union U0 g_725 = {0x7E1D8439L};/* VOLATILE GLOBAL g_725 */
static uint16_t ***g_735 = &g_475;
static uint16_t ****g_734[9] = {&g_735,&g_735,&g_735,&g_735,&g_735,&g_735,&g_735,&g_735,&g_735};
static uint16_t ***** volatile g_733 = &g_734[7];/* VOLATILE GLOBAL g_733 */
static int32_t **g_741 = &g_398;
static int32_t ***g_740 = &g_741;
static int32_t ****g_739 = &g_740;
static int32_t ***** volatile g_738 = &g_739;/* VOLATILE GLOBAL g_738 */
static volatile uint8_t *g_759[4] = {&g_402,&g_402,&g_402,&g_402};
static volatile uint8_t * volatile *g_758 = &g_759[1];
static volatile union U0 g_813[5] = {{0xB0B81B2DL},{0xB0B81B2DL},{0xB0B81B2DL},{0xB0B81B2DL},{0xB0B81B2DL}};
static union U0 **g_815 = (void*)0;
static union U0 *g_840 = (void*)0;
static union U0 ** volatile g_839 = &g_840;/* VOLATILE GLOBAL g_839 */
static uint8_t **g_863 = (void*)0;
static int32_t **g_874[5][6][8] = {{{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}},{{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}},{{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}},{{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}},{{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}}};
static int8_t g_875[2] = {0x48L,0x48L};
static union U0 g_896[5][4][5] = {{{{3L},{-1L},{1L},{-1L},{0x2040CD65L}},{{8L},{1L},{0x40122C75L},{1L},{8L}},{{0x75315738L},{-1L},{-1L},{8L},{1L}},{{0x75315738L},{1L},{0x2040CD65L},{0x4FA34058L},{0x4FA34058L}}},{{{8L},{0xF01FC199L},{8L},{-1L},{1L}},{{3L},{0x4FA34058L},{1L},{-1L},{8L}},{{1L},{0x2040CD65L},{0x4FA34058L},{0x4FA34058L},{0x2040CD65L}},{{0x2040CD65L},{7L},{1L},{8L},{-1L}}},{{{0xF01FC199L},{7L},{8L},{1L},{0x40122C75L}},{{1L},{-1L},{-1L},{1L},{0xF01FC199L}},{{0x75315738L},{0x2040CD65L},{0x40122C75L},{-1L},{0xF01FC199L}},{{-1L},{0x75315738L},{3L},{1L},{3L}}},{{{1L},{1L},{0xF01FC199L},{-1L},{0x40122C75L}},{{8L},{7L},{0xF01FC199L},{1L},{-1L}},{{0x4FA34058L},{0xF01FC199L},{3L},{0xF01FC199L},{0x4FA34058L}},{{-1L},{7L},{0x40122C75L},{0x4FA34058L},{1L}}},{{{-1L},{1L},{-1L},{0x2040CD65L},{0x2040CD65L}},{{0x4FA34058L},{0x75315738L},{0x4FA34058L},{7L},{1L}},{{8L},{0x2040CD65L},{1L},{7L},{0x4FA34058L}},{{1L},{-1L},{0x2040CD65L},{0x2040CD65L},{-1L}}}};
static union U0 g_906 = {-10L};/* VOLATILE GLOBAL g_906 */
static volatile union U0 g_985 = {2L};/* VOLATILE GLOBAL g_985 */
static union U0 g_994 = {4L};/* VOLATILE GLOBAL g_994 */
static volatile union U0 g_997 = {0x1DE50212L};/* VOLATILE GLOBAL g_997 */
static volatile uint32_t g_1107 = 18446744073709551615UL;/* VOLATILE GLOBAL g_1107 */
static const int8_t g_1115 = (-9L);
static uint32_t g_1136 = 0x6809FC6FL;
static uint32_t *g_1135 = &g_1136;
static uint32_t **g_1134[8][7][4] = {{{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{(void*)0,&g_1135,(void*)0,&g_1135},{&g_1135,&g_1135,(void*)0,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135}},{{(void*)0,(void*)0,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135}},{{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{(void*)0,&g_1135,(void*)0,&g_1135},{&g_1135,&g_1135,(void*)0,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135}},{{(void*)0,(void*)0,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135}},{{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{(void*)0,&g_1135,(void*)0,&g_1135},{&g_1135,&g_1135,(void*)0,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{(void*)0,(void*)0,&g_1135,(void*)0}},{{&g_1135,&g_1135,&g_1135,(void*)0},{&g_1135,&g_1135,&g_1135,(void*)0},{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,(void*)0}},{{&g_1135,&g_1135,&g_1135,(void*)0},{(void*)0,&g_1135,&g_1135,(void*)0},{&g_1135,(void*)0,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{(void*)0,(void*)0,&g_1135,(void*)0}},{{&g_1135,&g_1135,&g_1135,(void*)0},{&g_1135,&g_1135,&g_1135,(void*)0},{&g_1135,(void*)0,(void*)0,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,(void*)0}}};
static uint32_t *** volatile g_1133 = &g_1134[7][1][1];/* VOLATILE GLOBAL g_1133 */
static union U0 g_1168 = {-10L};/* VOLATILE GLOBAL g_1168 */
static int32_t ** volatile g_1171[7][1] = {{&g_214},{(void*)0},{&g_214},{&g_214},{(void*)0},{&g_214},{&g_214}};
static volatile uint32_t g_1187 = 0x1A565070L;/* VOLATILE GLOBAL g_1187 */
static int32_t ** volatile g_1192[4][6][6] = {{{&g_214,&g_214,(void*)0,(void*)0,(void*)0,&g_214},{&g_214,(void*)0,&g_214,&g_214,(void*)0,&g_214},{(void*)0,(void*)0,&g_214,(void*)0,(void*)0,&g_214},{&g_214,(void*)0,&g_214,&g_214,(void*)0,&g_214},{&g_214,&g_214,&g_214,(void*)0,(void*)0,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}},{{(void*)0,&g_214,&g_214,&g_214,&g_214,&g_214},{(void*)0,&g_214,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,(void*)0,(void*)0,&g_214,&g_214},{&g_214,&g_214,(void*)0,&g_214,&g_214,(void*)0},{&g_214,&g_214,(void*)0,(void*)0,&g_214,(void*)0},{(void*)0,&g_214,(void*)0,&g_214,&g_214,(void*)0}},{{&g_214,&g_214,(void*)0,(void*)0,(void*)0,&g_214},{&g_214,&g_214,(void*)0,(void*)0,(void*)0,&g_214},{&g_214,(void*)0,&g_214,&g_214,(void*)0,&g_214},{(void*)0,(void*)0,&g_214,(void*)0,(void*)0,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,(void*)0},{(void*)0,(void*)0,(void*)0,&g_214,&g_214,(void*)0}},{{(void*)0,&g_214,&g_214,(void*)0,(void*)0,&g_214},{&g_214,&g_214,&g_214,(void*)0,&g_214,&g_214},{&g_214,&g_214,(void*)0,(void*)0,&g_214,&g_214},{(void*)0,(void*)0,&g_214,&g_214,(void*)0,(void*)0},{(void*)0,(void*)0,&g_214,&g_214,&g_214,&g_214},{&g_214,&g_214,&g_214,&g_214,&g_214,&g_214}}};
static uint16_t g_1210 = 65531UL;
static int32_t *****g_1211 = &g_739;
static int32_t * volatile *g_1221 = (void*)0;
static uint64_t *g_1226 = &g_150;
static uint64_t **g_1225 = &g_1226;
static uint64_t *** volatile g_1224[5][7] = {{&g_1225,(void*)0,(void*)0,(void*)0,&g_1225,(void*)0,(void*)0},{&g_1225,&g_1225,&g_1225,&g_1225,&g_1225,&g_1225,&g_1225},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1225,&g_1225,&g_1225,&g_1225,&g_1225,&g_1225,&g_1225},{&g_1225,(void*)0,(void*)0,(void*)0,&g_1225,(void*)0,(void*)0}};
static uint64_t *** volatile g_1227 = &g_1225;/* VOLATILE GLOBAL g_1227 */
static const union U0 g_1303 = {1L};/* VOLATILE GLOBAL g_1303 */
static volatile int32_t * volatile * volatile * volatile *g_1312 = (void*)0;
static volatile uint16_t g_1349[1][8][3] = {{{0UL,1UL,1UL},{1UL,0xDF29L,0xDF29L},{0UL,1UL,1UL},{1UL,0xDF29L,0xDF29L},{0UL,1UL,1UL},{1UL,0xDF29L,0xDF29L},{0UL,1UL,1UL},{1UL,0xDF29L,0xDF29L}}};
static int32_t ** const  volatile g_1357 = &g_214;/* VOLATILE GLOBAL g_1357 */
static volatile union U0 g_1370 = {1L};/* VOLATILE GLOBAL g_1370 */
static int32_t * const  volatile g_1374 = &g_906.f0;/* VOLATILE GLOBAL g_1374 */
static int32_t ** volatile g_1431 = &g_214;/* VOLATILE GLOBAL g_1431 */
static volatile union U0 g_1447[9] = {{0x80A58D34L},{0x80A58D34L},{0x80A58D34L},{0x80A58D34L},{0x80A58D34L},{0x80A58D34L},{0x80A58D34L},{0x80A58D34L},{0x80A58D34L}};
static uint8_t g_1467[4][8][6] = {{{0x74L,0UL,0x0AL,1UL,0x6DL,0x5AL},{0x74L,0x5AL,1UL,0xA2L,247UL,0x6DL},{0x27L,254UL,0xE0L,254UL,0x27L,0x0AL},{0UL,0x49L,255UL,0UL,1UL,250UL},{0xE0L,0x0AL,247UL,0x49L,3UL,250UL},{0xF3L,1UL,255UL,0xF3L,0x0AL,0x0AL},{3UL,0xE0L,0xE0L,3UL,0xF3L,0x6DL},{0xF3L,255UL,1UL,0xF3L,0xA2L,0x5AL}},{{0x49L,247UL,0x0AL,0xE0L,0xA2L,0xF3L},{0UL,255UL,0x49L,0UL,0xF3L,0UL},{254UL,0xE0L,254UL,0x27L,0x0AL,0xA2L},{0xA2L,1UL,0x5AL,0x74L,3UL,255UL},{1UL,0x0AL,0xA2L,1UL,3UL,1UL},{0x49L,0x27L,0xE0L,1UL,1UL,0xE0L},{0UL,0UL,247UL,254UL,0UL,255UL},{0xA2L,0UL,0UL,255UL,0xF3L,247UL}},{{0x27L,0xA2L,0UL,0xE0L,0UL,255UL},{0x5AL,0xE0L,247UL,250UL,247UL,0xE0L},{250UL,247UL,0xE0L,0x5AL,0x6DL,1UL},{0xE0L,0UL,0xA2L,0x27L,255UL,0x0AL},{255UL,0UL,0UL,0xA2L,0x6DL,0x49L},{254UL,247UL,0UL,0UL,247UL,254UL},{1UL,0xE0L,0x27L,0x49L,0UL,0x5AL},{1UL,0xA2L,6UL,255UL,0xF3L,0UL}},{{1UL,0UL,255UL,0x49L,0UL,0xF3L},{1UL,0UL,255UL,0UL,1UL,6UL},{254UL,0x27L,0x0AL,0xA2L,3UL,247UL},{255UL,6UL,0x74L,0x27L,250UL,247UL},{0xE0L,255UL,0x0AL,0x5AL,6UL,6UL},{250UL,255UL,255UL,250UL,0xE0L,0xF3L},{0x5AL,0x0AL,255UL,0xE0L,0x49L,0UL},{0x27L,0x74L,6UL,255UL,0x49L,0x5AL}}};
static volatile uint64_t g_1497 = 0x0976ECD56BC531FCLL;/* VOLATILE GLOBAL g_1497 */
static int32_t ***g_1568 = &g_874[0][4][3];
static int32_t ****g_1567 = &g_1568;
static const int32_t g_1574 = (-1L);
static union U0 g_1653 = {-4L};/* VOLATILE GLOBAL g_1653 */
static int32_t *****g_1662 = &g_739;
static int32_t *****g_1663 = &g_739;
static union U0 g_1679 = {0xFFD1E856L};/* VOLATILE GLOBAL g_1679 */
static int32_t ** volatile g_1686 = &g_214;/* VOLATILE GLOBAL g_1686 */
static int32_t * const  volatile *g_1727 = &g_1374;
static int32_t * const  volatile * volatile *g_1726 = &g_1727;
static int32_t * const  volatile * volatile ** volatile g_1725 = &g_1726;/* VOLATILE GLOBAL g_1725 */
static uint32_t *g_1756 = &g_210;
static uint32_t **g_1755 = &g_1756;
static uint64_t ***g_1803 = &g_1225;
static uint64_t ****g_1802 = &g_1803;
static uint64_t *****g_1801 = &g_1802;
static union U0 g_1808 = {9L};/* VOLATILE GLOBAL g_1808 */
static int8_t *g_1824[4][5] = {{&g_714,&g_714,&g_714,&g_714,&g_714},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_714,&g_714,&g_714,&g_714,&g_714},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int8_t **g_1823 = &g_1824[1][1];
static union U0 g_1892 = {0x280F06C7L};/* VOLATILE GLOBAL g_1892 */
static union U0 **g_1913 = &g_840;
static int64_t g_1926 = 0x18F3E62FB8BE5C38LL;
static const int32_t g_2044 = 1L;
static uint64_t g_2064 = 0xFA3DFAD5BEFA1E1CLL;
static volatile union U0 g_2096 = {0x74B76FEFL};/* VOLATILE GLOBAL g_2096 */
static const union U0 g_2104 = {0xFA5B821BL};/* VOLATILE GLOBAL g_2104 */
static uint64_t g_2157 = 18446744073709551615UL;
static int32_t *g_2175[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t ** const g_2174 = &g_2175[4];
static int32_t ** const *g_2173 = &g_2174;
static int32_t ** const **g_2172 = &g_2173;
static int32_t ** const ***g_2171 = &g_2172;
static volatile int64_t g_2209 = 0x871747DE5B839079LL;/* VOLATILE GLOBAL g_2209 */
static const uint32_t g_2235[1][6] = {{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}};
static const uint32_t *g_2234 = &g_2235[0][0];
static const uint32_t **g_2233 = &g_2234;
static int16_t g_2291 = (-5L);
static int32_t g_2348[8] = {0xFFA558B1L,0xFFA558B1L,0xFFA558B1L,0xFFA558B1L,0xFFA558B1L,0xFFA558B1L,0xFFA558B1L,0xFFA558B1L};
static volatile union U0 g_2375[9] = {{0x9127B27DL},{0x9127B27DL},{0x9127B27DL},{0x9127B27DL},{0x9127B27DL},{0x9127B27DL},{0x9127B27DL},{0x9127B27DL},{0x9127B27DL}};
static uint16_t *****g_2409[5][3][2] = {{{&g_734[8],&g_734[3]},{(void*)0,&g_734[7]},{(void*)0,&g_734[3]}},{{&g_734[8],(void*)0},{&g_734[3],&g_734[7]},{(void*)0,(void*)0}},{{&g_734[8],(void*)0},{(void*)0,&g_734[7]},{&g_734[3],(void*)0}},{{&g_734[8],&g_734[3]},{(void*)0,&g_734[7]},{(void*)0,&g_734[3]}},{{&g_734[7],(void*)0},{&g_734[7],&g_734[5]},{&g_734[8],&g_734[8]}}};
static volatile union U0 g_2420 = {0xA1DE5EF3L};/* VOLATILE GLOBAL g_2420 */
static int32_t * volatile * volatile g_2467 = (void*)0;/* VOLATILE GLOBAL g_2467 */
static uint64_t ** const * const ** volatile g_2471[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint32_t *g_2485 = &g_1136;
static uint32_t ** const g_2484 = &g_2485;
static uint32_t ** const *g_2483 = &g_2484;
static uint32_t ***g_2493[4][1][2] = {{{&g_1755,&g_1755}},{{&g_1755,&g_1755}},{{&g_1755,&g_1755}},{{&g_1755,&g_1755}}};
static uint64_t g_2499[2][9] = {{6UL,0xA01A74263DBFB251LL,0x75C8675AC424181CLL,0x845E03EAA9540174LL,0x845E03EAA9540174LL,0x75C8675AC424181CLL,0xA01A74263DBFB251LL,6UL,0xA01A74263DBFB251LL},{18446744073709551615UL,0x56C795F501CA5F23LL,0x75C8675AC424181CLL,0x75C8675AC424181CLL,0x56C795F501CA5F23LL,18446744073709551615UL,0x845E03EAA9540174LL,18446744073709551615UL,0x56C795F501CA5F23LL}};
static union U0 g_2503 = {0x06F8F1E1L};/* VOLATILE GLOBAL g_2503 */
static union U0 g_2512 = {0x9D06D223L};/* VOLATILE GLOBAL g_2512 */
static union U0 g_2573 = {0xC558C8EDL};/* VOLATILE GLOBAL g_2573 */
static uint8_t g_2605 = 255UL;
static volatile int64_t g_2611[1] = {1L};
static const union U0 *g_2625[10][8] = {{&g_896[4][3][1],&g_1808,(void*)0,&g_1808,&g_896[4][3][1],&g_896[4][3][1],&g_1808,(void*)0},{&g_896[4][3][1],&g_896[4][3][1],&g_1808,(void*)0,&g_1808,&g_896[4][3][1],&g_896[4][3][1],&g_1808},{&g_2503,&g_1808,&g_1808,&g_2503,&g_2104,&g_2503,&g_1808,&g_1808},{&g_1808,&g_2104,(void*)0,(void*)0,&g_2104,&g_1808,&g_2104,(void*)0},{&g_2503,&g_2104,&g_2503,&g_1808,&g_1808,&g_2503,&g_2104,&g_2503},{&g_896[4][3][1],&g_1808,(void*)0,&g_1808,&g_896[4][3][1],&g_896[4][3][1],&g_1808,(void*)0},{&g_896[4][3][1],&g_896[4][3][1],&g_1808,(void*)0,&g_1808,&g_896[4][3][1],&g_896[4][3][1],&g_1808},{&g_2503,&g_1808,&g_1808,&g_2503,&g_2104,&g_2503,&g_1808,&g_1808},{&g_1808,&g_2104,(void*)0,(void*)0,&g_2104,&g_1808,&g_2104,(void*)0},{&g_2503,&g_2104,&g_2503,&g_1808,&g_1808,&g_2503,&g_2104,&g_2503}};
static const union U0 ** volatile g_2624 = &g_2625[6][4];/* VOLATILE GLOBAL g_2624 */
static union U0 ** volatile g_2662 = &g_840;/* VOLATILE GLOBAL g_2662 */
static uint32_t *** const ** volatile g_2665 = (void*)0;/* VOLATILE GLOBAL g_2665 */
static uint32_t ***g_2669[10] = {&g_1134[7][1][1],&g_1134[4][3][2],&g_1134[7][1][1],&g_1134[4][3][2],&g_1134[7][1][1],&g_1134[4][3][2],&g_1134[7][1][1],&g_1134[4][3][2],&g_1134[7][1][1],&g_1134[4][3][2]};
static uint32_t *** const *g_2668[4][10][5] = {{{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0}},{{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0}},{{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0}},{{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0},{(void*)0,&g_2669[6],(void*)0,&g_2669[6],(void*)0},{(void*)0,(void*)0,&g_2669[7],&g_2669[7],(void*)0},{&g_2669[7],&g_2669[6],&g_2669[7],&g_2669[6],&g_2669[7]},{(void*)0,&g_2669[7],&g_2669[7],(void*)0,(void*)0}}};
static uint32_t *** const ** volatile g_2667 = &g_2668[2][0][3];/* VOLATILE GLOBAL g_2667 */
static volatile uint32_t g_2819 = 4294967292UL;/* VOLATILE GLOBAL g_2819 */
static volatile union U0 g_2898 = {1L};/* VOLATILE GLOBAL g_2898 */
static volatile int64_t g_2915 = 4L;/* VOLATILE GLOBAL g_2915 */
static uint32_t g_2918[9] = {0x41EC6F59L,0x41EC6F59L,0x41EC6F59L,0x41EC6F59L,0x41EC6F59L,0x41EC6F59L,0x41EC6F59L,0x41EC6F59L,0x41EC6F59L};
static union U0 ** volatile g_2922 = &g_840;/* VOLATILE GLOBAL g_2922 */
static union U0 g_2959 = {0x4A317628L};/* VOLATILE GLOBAL g_2959 */
static int16_t g_2992 = 0xBFD0L;
static volatile int32_t g_3021 = 0xEE998358L;/* VOLATILE GLOBAL g_3021 */
static volatile int32_t *g_3020 = &g_3021;
static int32_t g_3022 = 0x5E11DFD5L;
static union U0 g_3069 = {0xE86FDBDCL};/* VOLATILE GLOBAL g_3069 */
static union U0 g_3080 = {0xF648547CL};/* VOLATILE GLOBAL g_3080 */


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static uint64_t  func_2(uint32_t  p_3, uint8_t  p_4, int64_t  p_5, uint32_t  p_6, int32_t  p_7);
static uint32_t  func_15(int8_t  p_16, int8_t  p_17, int32_t * p_18);
static int32_t * func_19(int64_t  p_20, uint32_t  p_21);
static int32_t  func_22(uint64_t  p_23, uint64_t  p_24, int8_t  p_25);
static uint32_t  func_28(int32_t * p_29, int32_t * p_30);
static int32_t * func_31(const uint8_t  p_32, int32_t * p_33);
static int32_t * func_34(int32_t * const  p_35);
static int32_t * const  func_36(int32_t * p_37, int32_t * p_38, uint8_t  p_39, uint32_t  p_40);
static const int32_t  func_54(uint16_t  p_55, int32_t * p_56);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_1808.f0
 * writes:
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_9[8][9] = {{1UL,0x193208AF066B7A7BLL,0xA31E468A07C694A6LL,0x6A56C37FC0549EC7LL,1UL,2UL,0x951BB35B68906353LL,0xBFF1EB7E5950169FLL,0x951BB35B68906353LL},{2UL,0xBFF1EB7E5950169FLL,1UL,1UL,0xBFF1EB7E5950169FLL,2UL,0xF7A9D2E819D3388ALL,0x77E06985BA5616D2LL,0x2B441A62A96950A4LL},{0xC96C7979B3895196LL,0x6A56C37FC0549EC7LL,0x8FBC781CED8F6EFBLL,18446744073709551606UL,18446744073709551615UL,1UL,0xCB27F08E2294BB48LL,0x951BB35B68906353LL,1UL},{18446744073709551615UL,0UL,0xCB27F08E2294BB48LL,0x08373B2C57E71F09LL,0xBF0796840EB44952LL,0xB1857497F3C88FEDLL,0xF7A9D2E819D3388ALL,0x193208AF066B7A7BLL,0x4B082B0002500428LL},{0x193208AF066B7A7BLL,0x951BB35B68906353LL,0UL,18446744073709551615UL,18446744073709551615UL,0UL,0x951BB35B68906353LL,0x2B441A62A96950A4LL,0x193208AF066B7A7BLL},{0x88E00F68029989D2LL,1UL,0xA31E468A07C694A6LL,18446744073709551606UL,18446744073709551615UL,0x8FBC781CED8F6EFBLL,0x193208AF066B7A7BLL,0xBF0796840EB44952LL,0xF7A9D2E819D3388ALL},{0xBFF1EB7E5950169FLL,18446744073709551615UL,0UL,0xCB27F08E2294BB48LL,0x08373B2C57E71F09LL,0xBF0796840EB44952LL,0xB1857497F3C88FEDLL,0xF7A9D2E819D3388ALL,0x193208AF066B7A7BLL},{0x4B082B0002500428LL,1UL,0x7AF9CA3EAA3E8F08LL,0x88E00F68029989D2LL,0xB1857497F3C88FEDLL,0xB1857497F3C88FEDLL,0x88E00F68029989D2LL,0x7AF9CA3EAA3E8F08LL,1UL}};
    int32_t *l_10[2][7];
    int32_t l_12 = 0x04FC4B66L;
    const int32_t *l_1573 = &g_1574;
    int16_t l_2179 = 0x7F72L;
    int32_t l_2505 = 0xB6A4BD2FL;
    const uint8_t *l_2527 = &g_1467[2][5][3];
    const uint8_t **l_2526[10];
    const uint8_t ***l_2525[9][2][3] = {{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}},{{(void*)0,(void*)0,(void*)0},{&l_2526[6],&l_2526[6],&l_2526[6]}}};
    uint8_t l_2541 = 3UL;
    uint16_t ****l_2547 = &g_735;
    uint32_t **l_2561 = &g_2485;
    int32_t l_2589 = 0x5A6B9DA7L;
    int32_t l_2590 = 0xB079C1DDL;
    int32_t l_2593 = 4L;
    int32_t *****l_2604 = &g_739;
    uint64_t l_2622 = 0x4762D0538200F602LL;
    uint16_t l_2660 = 0UL;
    union U0 *l_2661[9][2][4] = {{{&g_896[3][3][0],&g_1679,&g_906,&g_2573},{&g_2573,&g_994,&g_1679,&g_1679}},{{&g_1168,&g_1679,&g_1679,&g_1168},{&g_2573,&g_896[0][3][2],&g_906,&g_1679}},{{&g_896[3][3][0],&g_2503,&g_1679,(void*)0},{&g_1679,(void*)0,&g_906,(void*)0}},{{&g_1679,&g_2503,&g_994,&g_1679},{&g_1653,&g_896[0][3][2],(void*)0,&g_1168}},{{&g_994,&g_1679,&g_1679,&g_1679},{&g_994,&g_994,(void*)0,&g_2573}},{{&g_1653,&g_1679,&g_994,&g_896[0][3][2]},{&g_1679,&g_896[3][3][0],&g_906,&g_994}},{{&g_1679,&g_896[3][3][0],&g_1679,&g_896[0][3][2]},{&g_896[3][3][0],&g_1679,&g_906,&g_2573}},{{&g_2573,&g_994,&g_1679,&g_1679},{&g_1168,&g_1679,&g_1679,&g_1168}},{{&g_2573,&g_896[0][3][2],&g_906,&g_1679},{&g_896[3][3][0],&g_2503,&g_1679,(void*)0}}};
    int32_t l_2742[4] = {3L,3L,3L,3L};
    uint8_t l_2754[8] = {1UL,0xF3L,1UL,1UL,0xF3L,1UL,1UL,0xF3L};
    int32_t *l_2777[6][6] = {{&l_2593,&g_519[1][4][3],&l_2505,&l_2593,&l_2505,&g_519[1][4][3]},{(void*)0,&g_519[1][4][3],&g_1808.f0,(void*)0,&l_2505,&l_2505},{&g_252[6][1],&g_519[1][4][3],&g_519[1][4][3],&g_252[6][1],&l_2505,&g_1808.f0},{&l_2593,&g_519[1][4][3],&l_2505,&l_2593,&l_2505,&g_519[1][4][3]},{&l_2505,(void*)0,&g_252[6][1],&l_2505,&g_906.f0,&g_906.f0},{&g_519[1][4][3],(void*)0,(void*)0,&g_519[1][4][3],&g_906.f0,&g_252[6][1]}};
    const uint64_t l_2797 = 0UL;
    uint32_t ****l_2801 = &g_2493[0][0][1];
    uint32_t l_2821 = 0xE88A8AADL;
    uint16_t l_2864 = 0x5E9DL;
    int32_t *l_2884 = &g_1808.f0;
    int8_t l_2936[2];
    uint32_t l_2954 = 0x4C688882L;
    uint32_t l_2997 = 18446744073709551615UL;
    union U0 ***l_3092 = &g_815;
    int32_t **l_3105 = (void*)0;
    uint32_t l_3128 = 0xF0DB6933L;
    int64_t l_3133 = (-2L);
    uint32_t l_3134 = 18446744073709551612UL;
    uint32_t *l_3135 = &g_3080.f4;
    const int64_t ** const *l_3144 = (void*)0;
    uint16_t *l_3145 = &g_1210;
    int64_t l_3146 = 0x7AF54D9A59019B31LL;
    uint32_t l_3147 = 18446744073709551615UL;
    const int16_t l_3148 = 7L;
    int8_t l_3149[9][8] = {{1L,(-2L),0L,0xE2L,0xE2L,0L,(-2L),1L},{0x80L,1L,0L,3L,2L,(-2L),6L,0x4EL},{(-2L),0xF4L,1L,(-2L),3L,(-2L),1L,0xF4L},{5L,1L,0x4EL,1L,0x80L,0L,2L,1L},{0xF4L,(-2L),0xE2L,2L,5L,5L,2L,0xE2L},{2L,2L,0x4EL,0L,3L,0L,1L,(-2L)},{3L,0L,1L,(-2L),6L,0xE2L,6L,(-2L)},{0L,0x4EL,0L,0L,(-1L),0xF4L,(-2L),0xE2L},{1L,0x80L,0L,2L,1L,(-1L),(-1L),1L}};
    uint16_t l_3150 = 1UL;
    uint16_t *l_3151 = &g_470;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
            l_10[i][j] = &g_11;
    }
    for (i = 0; i < 10; i++)
        l_2526[i] = &l_2527;
    for (i = 0; i < 2; i++)
        l_2936[i] = 1L;
    return (*l_2884);
}


/* ------------------------------------------ */
/* 
 * reads : g_1653.f4 g_1567 g_1568 g_874 g_214 g_120 g_1679.f0 g_474 g_475 g_476 g_58 g_250 g_1803 g_1225 g_1226 g_1136 g_2348 g_261 g_145 g_655 g_656 g_318 g_319 g_279 g_280 g_269 g_270 g_271 g_1756 g_210 g_896.f0 g_1726 g_1727 g_1374 g_2375 g_340 g_11 g_733 g_734 g_2233 g_2234 g_2235 g_150 g_671 g_2173 g_2174 g_1653.f0 g_238 g_2064 g_252 g_282 g_278 g_2157 g_1725 g_906.f0 g_2484 g_2485 g_2499 g_839 g_840 g_1892.f0 g_88
 * writes: g_1653.f4 g_1679.f0 g_1168.f4 g_250 g_150 g_2233 g_1136 g_214 g_145 g_896.f0 g_280 g_210 g_906.f0 g_11 g_1653.f0 g_2175 g_2409 g_252 g_238 g_2064 g_906.f4 g_2157 g_120 g_2291 g_2483 g_2493 g_1892.f4 g_874 g_1892.f0 g_1926
 */
static uint64_t  func_2(uint32_t  p_3, uint8_t  p_4, int64_t  p_5, uint32_t  p_6, int32_t  p_7)
{ /* block id: 1037 */
    int32_t l_2191 = 0L;
    int32_t l_2203 = 4L;
    int32_t l_2204 = (-1L);
    int32_t l_2205[3][10][1] = {{{0x72DA4C94L},{0xFCBFF38EL},{0L},{0x7561C6BEL},{0xAB8839ADL},{0x72DA4C94L},{0x7561C6BEL},{0x59904FF0L},{0x7561C6BEL},{0x72DA4C94L}},{{0xAB8839ADL},{0x7561C6BEL},{0L},{0xFCBFF38EL},{0x72DA4C94L},{0x72DA4C94L},{0xFCBFF38EL},{0L},{0x7561C6BEL},{0xAB8839ADL}},{{0x72DA4C94L},{0x7561C6BEL},{0x59904FF0L},{0x7561C6BEL},{0x72DA4C94L},{0xAB8839ADL},{0x7561C6BEL},{0L},{0xFCBFF38EL},{0x72DA4C94L}}};
    const uint32_t *l_2231 = &g_64[4];
    const uint32_t **l_2230 = &l_2231;
    int32_t *l_2326 = &g_896[3][3][0].f0;
    int32_t l_2328 = (-4L);
    uint32_t l_2345 = 0xF156B236L;
    uint32_t ***l_2366 = &g_1134[0][5][0];
    uint32_t ****l_2365 = &l_2366;
    int64_t *l_2378[6][9][4] = {{{&g_143,&g_143,(void*)0,&g_1926},{&g_143,(void*)0,&g_143,&g_143},{&g_143,&g_1926,&g_143,&g_143},{(void*)0,(void*)0,&g_145,&g_1926},{&g_1926,&g_143,&g_145,&g_143},{(void*)0,&g_143,&g_143,&g_145},{&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,(void*)0,&g_1926},{&g_143,(void*)0,&g_143,&g_143}},{{&g_143,&g_1926,&g_143,&g_143},{(void*)0,(void*)0,&g_145,&g_1926},{&g_1926,&g_143,&g_145,&g_143},{(void*)0,&g_143,&g_143,&g_145},{&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,(void*)0,&g_1926},{&g_143,(void*)0,&g_143,&g_143},{&g_143,&g_1926,&g_143,&g_143},{(void*)0,(void*)0,&g_145,&g_1926}},{{&g_1926,&g_143,&g_145,&g_143},{(void*)0,&g_1926,&g_145,(void*)0},{&g_143,&g_1926,&g_1926,&g_143},{&g_1926,&g_143,(void*)0,&g_143},{&g_1926,(void*)0,&g_1926,&g_145},{&g_143,&g_143,&g_145,&g_145},{(void*)0,(void*)0,(void*)0,&g_143},{&g_143,&g_143,(void*)0,&g_143},{(void*)0,&g_1926,&g_145,(void*)0}},{{&g_143,&g_1926,&g_1926,&g_143},{&g_1926,&g_143,(void*)0,&g_143},{&g_1926,(void*)0,&g_1926,&g_145},{&g_143,&g_143,&g_145,&g_145},{(void*)0,(void*)0,(void*)0,&g_143},{&g_143,&g_143,(void*)0,&g_143},{(void*)0,&g_1926,&g_145,(void*)0},{&g_143,&g_1926,&g_1926,&g_143},{&g_1926,&g_143,(void*)0,&g_143}},{{&g_1926,(void*)0,&g_1926,&g_145},{&g_143,&g_143,&g_145,&g_145},{(void*)0,(void*)0,(void*)0,&g_143},{&g_143,&g_143,(void*)0,&g_143},{(void*)0,&g_1926,&g_145,(void*)0},{&g_143,&g_1926,&g_1926,&g_143},{&g_1926,&g_143,(void*)0,&g_143},{&g_1926,(void*)0,&g_1926,&g_145},{&g_143,&g_143,&g_145,&g_145}},{{(void*)0,(void*)0,(void*)0,&g_143},{&g_143,&g_143,(void*)0,&g_143},{(void*)0,&g_1926,&g_145,(void*)0},{&g_143,&g_1926,&g_1926,&g_143},{&g_1926,&g_143,(void*)0,&g_143},{&g_1926,(void*)0,&g_1926,&g_145},{&g_143,&g_143,&g_145,&g_145},{(void*)0,(void*)0,(void*)0,&g_143},{&g_143,&g_143,(void*)0,&g_143}}};
    const int8_t *l_2396 = (void*)0;
    const int8_t **l_2395 = &l_2396;
    uint64_t l_2464 = 0x0222A775543B0F5CLL;
    uint64_t ** const * const l_2470 = &g_1225;
    uint64_t ** const * const *l_2469 = &l_2470;
    int i, j, k;
    for (g_1653.f4 = 0; (g_1653.f4 > 40); g_1653.f4 = safe_add_func_uint64_t_u_u(g_1653.f4, 1))
    { /* block id: 1040 */
        int8_t l_2190 = 0L;
        int32_t l_2202 = 0x99A9D7EBL;
        int32_t l_2206 = 0x4B7CAA22L;
        int32_t l_2207 = 5L;
        int32_t l_2208 = (-1L);
        int32_t l_2210 = 0xC18F8403L;
        int32_t l_2211 = 0xF7D0607FL;
        int32_t ***l_2217 = (void*)0;
        uint32_t * const *l_2241 = &g_1135;
        uint32_t * const **l_2240 = &l_2241;
        uint32_t * const ***l_2239 = &l_2240;
        uint32_t * const ****l_2238 = &l_2239;
        uint16_t *****l_2243[8][7][4] = {{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}},{{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]},{&g_734[7],&g_734[7],&g_734[7],&g_734[7]}}};
        int16_t l_2254[7] = {0xC7D3L,0xC7D3L,0xC7D3L,0xC7D3L,0xC7D3L,0xC7D3L,0xC7D3L};
        int16_t l_2282 = 0x8B39L;
        int32_t l_2283 = 0x78449884L;
        int32_t l_2289 = 5L;
        int32_t l_2290 = 0xC095B8E1L;
        int32_t l_2292 = 0x0DB132F7L;
        int32_t l_2293 = (-5L);
        int32_t l_2294 = 0x08064E77L;
        int32_t l_2295 = (-1L);
        int32_t l_2296 = (-5L);
        int32_t l_2297 = 0xCB97E76AL;
        uint16_t l_2305 = 0x8D80L;
        uint64_t *l_2316 = (void*)0;
        int8_t l_2341[9] = {0xF2L,0xE0L,0xF2L,0xE0L,0xF2L,0xE0L,0xF2L,0xE0L,0xF2L};
        int32_t l_2342 = 0xB5E08B1FL;
        int32_t l_2343 = (-2L);
        int32_t l_2344[8] = {0xDF1DFA21L,0xDF1DFA21L,0xDF1DFA21L,0xDF1DFA21L,0xDF1DFA21L,0xDF1DFA21L,0xDF1DFA21L,0xDF1DFA21L};
        int64_t *l_2351[1][3][3] = {{{(void*)0,(void*)0,(void*)0},{&g_143,&g_1926,&g_143},{(void*)0,(void*)0,(void*)0}}};
        const uint16_t l_2359 = 0xD3E0L;
        int i, j, k;
        if ((****g_1567))
        { /* block id: 1041 */
            int32_t l_2186 = (-7L);
            int32_t l_2189 = 0x605F8564L;
            int32_t l_2193 = 0L;
            int32_t l_2194 = 0L;
            int32_t l_2196 = 0x9176D058L;
            int32_t l_2198[8][5] = {{1L,1L,3L,1L,1L},{0L,1L,0L,0L,1L},{1L,0L,0L,1L,0L},{1L,1L,3L,1L,1L},{0L,1L,0L,0L,1L},{1L,0L,0L,1L,0L},{1L,1L,3L,1L,1L},{0L,1L,0L,0L,1L}};
            uint32_t l_2298 = 0x9F52238DL;
            int16_t l_2329 = 0L;
            uint32_t l_2330 = 0x55F90A5DL;
            int i, j;
            for (g_1679.f0 = 0; (g_1679.f0 != (-6)); g_1679.f0 = safe_sub_func_uint16_t_u_u(g_1679.f0, 2))
            { /* block id: 1044 */
                int16_t l_2187 = 0x26B5L;
                int32_t l_2192 = 0x102788A4L;
                int32_t l_2195 = 0x33BDD32EL;
                int32_t l_2197 = 0xCA1CDE34L;
                int32_t l_2199 = (-2L);
                int32_t l_2200 = 0x35C7AEBAL;
                int32_t l_2201[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_2201[i] = (-1L);
                for (g_1168.f4 = (-18); (g_1168.f4 >= 34); g_1168.f4++)
                { /* block id: 1047 */
                    int32_t *l_2188[5][7][1] = {{{&g_50},{(void*)0},{&g_690[0].f0},{&g_690[0].f0},{(void*)0},{&g_238},{&g_238}},{{(void*)0},{&g_690[0].f0},{&g_690[0].f0},{(void*)0},{&g_50},{&g_50},{&g_238}},{{&g_704.f0},{&g_238},{&g_50},{&g_50},{(void*)0},{&g_690[0].f0},{&g_690[0].f0}},{{(void*)0},{&g_238},{&g_238},{(void*)0},{&g_690[0].f0},{&g_690[0].f0},{(void*)0}},{{&g_50},{&g_50},{&g_238},{&g_704.f0},{&g_238},{&g_50},{&g_50}}};
                    uint8_t l_2212 = 2UL;
                    int i, j, k;
                    l_2212--;
                }
            }
            if ((safe_mul_func_uint16_t_u_u((***g_474), (l_2190 <= ((void*)0 == l_2217)))))
            { /* block id: 1051 */
                int16_t l_2242 = 0x7F39L;
                int32_t l_2270 = 4L;
                int32_t l_2272 = 0x1C5965C8L;
                int32_t l_2275 = 0xAB5A91B5L;
                int32_t l_2276 = 0x6D654594L;
                int32_t l_2277 = 0x03BB1635L;
                int32_t l_2281[3][8][8] = {{{0x19E8A7EAL,0L,0x19E8A7EAL,0xA2B6602EL,9L,0x94392299L,0xF7F73C63L,(-1L)},{0xA69871A9L,0x19E8A7EAL,4L,0x9904F228L,0x025D2DD3L,0x528FEA07L,9L,9L},{0xA69871A9L,9L,(-6L),(-6L),9L,0xA69871A9L,0L,0xFDDAB443L},{0x19E8A7EAL,0x2BB54E96L,0x528FEA07L,0L,4L,(-1L),(-1L),0xB10EE143L},{0x025D2DD3L,0xFDDAB443L,(-1L),0L,0x65B617F0L,0L,(-1L),0xFDDAB443L},{(-1L),0x65B617F0L,0x9904F228L,(-6L),0xB10EE143L,0xA2B6602EL,0x025D2DD3L,9L},{(-6L),0x94392299L,0xFDDAB443L,0x9904F228L,(-1L),0x025D2DD3L,0x025D2DD3L,(-1L)},{0xA2B6602EL,0x9904F228L,0x9904F228L,0xA2B6602EL,0xFDDAB443L,0xB10EE143L,(-1L),0xA69871A9L}},{{0xFDDAB443L,0xB10EE143L,(-1L),0xA69871A9L,0xF7F73C63L,0x19E8A7EAL,(-1L),0x94392299L},{0L,0xB10EE143L,0x528FEA07L,4L,0x528FEA07L,0xB10EE143L,0L,(-6L)},{0x65B617F0L,0x9904F228L,(-6L),0xB10EE143L,0xA2B6602EL,0x025D2DD3L,9L,0x2BB54E96L},{0x2BB54E96L,0x94392299L,4L,0xF7F73C63L,0xA2B6602EL,0xA2B6602EL,0xF7F73C63L,4L},{0x65B617F0L,0x65B617F0L,0x19E8A7EAL,0x2BB54E96L,0x528FEA07L,0x94392299L,(-6L),0L},{0x94392299L,0x2BB54E96L,0x025D2DD3L,0xFDDAB443L,(-1L),0L,0x65B617F0L,0L},{0x2BB54E96L,0x9904F228L,0x19E8A7EAL,0x9904F228L,0x2BB54E96L,4L,(-1L),(-6L)},{0x19E8A7EAL,0xF7F73C63L,0xA69871A9L,(-1L),0xB10EE143L,0xFDDAB443L,0xA2B6602EL,0x9904F228L}},{{0xA2B6602EL,(-1L),0xA69871A9L,0x528FEA07L,0x528FEA07L,0xA69871A9L,(-1L),0xA2B6602EL},{0xB10EE143L,0x94392299L,0x19E8A7EAL,(-6L),0x025D2DD3L,0xF7F73C63L,0x65B617F0L,0xA69871A9L},{0x9757A992L,(-1L),0x025D2DD3L,4L,(-6L),0xF7F73C63L,(-6L),4L},{(-1L),0x94392299L,(-1L),0x19E8A7EAL,0xF7F73C63L,0xA69871A9L,(-1L),0xB10EE143L},{4L,(-1L),(-6L),0x65B617F0L,0x9757A992L,0xFDDAB443L,0xF7F73C63L,0xF7F73C63L},{4L,0xF7F73C63L,0xA2B6602EL,0xA2B6602EL,0xF7F73C63L,4L,0x94392299L,0x2BB54E96L},{(-1L),0x9904F228L,0xFDDAB443L,0x94392299L,(-6L),0L,0xB10EE143L,0x528FEA07L},{0x9757A992L,0x2BB54E96L,0L,0x94392299L,0x025D2DD3L,0x94392299L,0L,0x2BB54E96L}}};
                uint8_t l_2322 = 0x0BL;
                int i, j, k;
                for (g_250 = 0; (g_250 <= 1); g_250 += 1)
                { /* block id: 1054 */
                    const uint32_t ***l_2232 = &l_2230;
                    int32_t l_2249 = 0x68D38584L;
                    int32_t l_2278 = (-10L);
                    int32_t l_2279 = 0xA0A2A0B7L;
                    int32_t l_2280 = 0xBAB020CAL;
                    int32_t l_2284 = 0x45528EACL;
                    int16_t l_2285 = (-9L);
                    int32_t l_2286 = 0x44B101EAL;
                    int32_t l_2287 = 3L;
                    int32_t l_2288[3][4][3] = {{{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)}}};
                    int32_t l_2321 = (-1L);
                    int i, j, k;
                    l_2210 = ((safe_lshift_func_uint8_t_u_s(0x3AL, ((0x2C0EL | (((safe_mod_func_uint16_t_u_u(0x5523L, ((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint64_t_u_u(((***g_1803) = 0x4C4BB440DB710F9ELL), 60)), ((safe_lshift_func_uint8_t_u_s((p_5 , (safe_lshift_func_uint32_t_u_u(((g_2233 = ((*l_2232) = l_2230)) != &g_1756), (safe_div_func_uint32_t_u_u(((void*)0 != l_2238), l_2242))))), 1)) >= p_3))) , l_2193))) | l_2242) > l_2196)) | 0xBA135BB748AC1276LL))) && 0x10L);
                }
                for (g_1136 = 0; (g_1136 <= 2); g_1136 = safe_add_func_int8_t_s_s(g_1136, 1))
                { /* block id: 1087 */
                    (***g_1567) = l_2326;
                }
            }
            else
            { /* block id: 1090 */
                int32_t *l_2327[5][7][7] = {{{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210}},{{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283}},{{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210}},{{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283}},{{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210},{&l_2283,&l_2293,&l_2283,&l_2293,&l_2283,&l_2293,&l_2283},{&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210,&l_2210}}};
                int i, j, k;
                l_2330++;
            }
            return p_7;
        }
        else
        { /* block id: 1094 */
            int32_t *l_2333 = &l_2297;
            int32_t *l_2334 = &l_2203;
            int32_t *l_2335 = &g_994.f0;
            int32_t *l_2336 = &l_2204;
            int32_t *l_2337 = (void*)0;
            int32_t *l_2338 = &l_2205[0][8][0];
            int32_t *l_2339 = &l_2205[0][7][0];
            int32_t *l_2340[7] = {&l_2211,&l_2211,&l_2211,&l_2211,&l_2211,&l_2211,&l_2211};
            int i;
            l_2345--;
            if (p_5)
                continue;
        }
        (***g_1726) = (g_2348[1] , ((safe_add_func_uint16_t_u_u(((((p_5 >= ((*l_2326) = ((*g_261) = (*g_261)))) == ((*g_655) , (safe_mod_func_uint64_t_u_u(((safe_unary_minus_func_uint32_t_u(((safe_rshift_func_uint32_t_u_s(((*g_1756) ^= (((safe_rshift_func_uint8_t_u_u(((((***g_318) ^= p_3) || (*g_476)) < p_3), 2)) == (p_7 >= ((*g_214) < l_2359))) < (***g_269))), 11)) & p_3))) , 18446744073709551613UL), p_6)))) & 0xE4A85ADF769661E0LL) || (*l_2326)), p_5)) , 1L));
    }
    (*l_2326) = ((((+((void*)0 != &l_2230)) >= (safe_mul_func_int32_t_s_s((safe_sub_func_int8_t_s_s((l_2365 != ((((*g_1756) = (*l_2326)) , ((safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint64_t_u_u(18446744073709551615UL, 11)), 3)) != (safe_add_func_uint8_t_u_u((((safe_lshift_func_int32_t_s_u((((g_2375[0] , (safe_div_func_int64_t_s_s((((-4L) < (*l_2326)) , ((((1UL == 7UL) > (-4L)) == (***g_340)) , 0x5515DEA639C6697DLL)), 6L))) , (void*)0) != l_2378[0][0][0]), 30)) , p_4) && (*l_2326)), p_4)))) , &l_2366)), (*g_271))), p_5))) && (*l_2326)) > 1UL);
    for (g_11 = 6; (g_11 >= 11); g_11 = safe_add_func_uint64_t_u_u(g_11, 7))
    { /* block id: 1108 */
        uint8_t l_2381[7][9][4] = {{{249UL,0xCBL,0xC4L,0xB5L},{4UL,0xBAL,0x98L,0x21L},{0x43L,4UL,255UL,253UL},{0x98L,0x51L,0xBAL,6UL},{6UL,1UL,0x34L,0UL},{0x4AL,0x40L,0xC9L,1UL},{1UL,6UL,255UL,6UL},{1UL,0x4DL,6UL,0xB5L},{0x27L,8UL,0xE3L,4UL}},{{0x4DL,1UL,255UL,1UL},{0x4DL,0x4AL,0xE3L,6UL},{0x27L,1UL,6UL,0x34L},{1UL,255UL,255UL,0x7BL},{1UL,0x21L,0xC9L,9UL},{0x4AL,0x98L,0x34L,0xB5L},{6UL,0xE3L,0xBAL,0xCBL},{0x98L,9UL,255UL,0xE4L},{0x43L,1UL,0x98L,0x84L}},{{4UL,1UL,0xC4L,0xC4L},{0UL,0UL,0x98L,0x38L},{1UL,0xCBL,0xB5L,0x27L},{0x51L,0xBAL,6UL,0xB5L},{0x21L,0xBAL,0x4DL,0x27L},{0xBAL,0xCBL,255UL,0x38L},{0xBAL,0UL,0x43L,0xC4L},{1UL,1UL,0UL,0x84L},{255UL,1UL,249UL,0xE4L}},{{1UL,9UL,3UL,0xCBL},{0x41L,0xE3L,0UL,0xB5L},{0xCBL,0x98L,0xBAL,9UL},{0xE3L,0x21L,255UL,0x7BL},{8UL,255UL,8UL,0x34L},{9UL,1UL,0x84L,6UL},{0x40L,0x4AL,254UL,1UL},{1UL,1UL,254UL,4UL},{0x40L,8UL,0x84L,0xB5L}},{{9UL,0x4DL,8UL,6UL},{8UL,6UL,255UL,1UL},{0xE3L,0x40L,0xBAL,0UL},{0xCBL,1UL,0UL,6UL},{0x41L,0x51L,3UL,253UL},{1UL,4UL,249UL,0x21L},{255UL,0xBAL,0UL,0xB5L},{1UL,0x43L,0x43L,1UL},{0xBAL,0x27L,255UL,3UL}},{{0xBAL,0x41L,0x4DL,0UL},{0x21L,1UL,6UL,0UL},{0x51L,0x41L,0xB5L,3UL},{1UL,8UL,0x41L,0xBAL},{0x7BL,0xE4L,0x98L,0x4AL},{0x98L,0x7BL,1UL,0xE3L},{0xE4L,0x98L,0xB9L,0UL},{1UL,0xBCL,0x7BL,0xB5L},{0x4DL,0x84L,249UL,254UL}},{{0UL,255UL,0x40L,6UL},{6UL,0x4DL,1UL,0x4DL},{1UL,253UL,0xB5L,0x4AL},{8UL,3UL,1UL,0x98L},{253UL,0xBAL,0xB9L,0x84L},{253UL,0UL,1UL,0xC9L},{8UL,0x84L,0xB5L,249UL},{1UL,0xB9L,1UL,0x34L},{6UL,0xE3L,0x40L,0xBAL}}};
        int i, j, k;
        ++l_2381[2][5][1];
        if (p_6)
            break;
    }
    if ((!(((safe_add_func_int8_t_s_s(p_6, (safe_add_func_uint16_t_u_u((((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(((((&l_2230 != &g_1755) > (safe_add_func_uint8_t_u_u(((*g_733) != &g_735), (((l_2395 != &g_1824[3][4]) > 0xFD5DA289L) < (((safe_add_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((((safe_rshift_func_uint32_t_u_s(((*l_2326) == 1L), p_3)) || (*g_271)) >= 0xC8381D5CL) , (*l_2326)), (**g_2233))), (*l_2326))) , 0xDC36L) && (-9L)))))) | (*l_2326)) , (*l_2326)), (*g_476))), p_6)) | p_3) , 0xAEB9L), (*l_2326))))) || p_5) , 0x338570F9L)))
    { /* block id: 1112 */
        int32_t *l_2406[6] = {&g_906.f0,&g_906.f0,(void*)0,&g_906.f0,&g_906.f0,(void*)0};
        int32_t * volatile l_2468 = (void*)0;/* VOLATILE GLOBAL l_2468 */
        int i;
        for (g_150 = (-20); (g_150 == 23); g_150 = safe_add_func_uint8_t_u_u(g_150, 4))
        { /* block id: 1115 */
            int32_t *l_2405 = (void*)0;
            const uint16_t *l_2414 = (void*)0;
            const uint16_t **l_2413 = &l_2414;
            const uint16_t ***l_2412 = &l_2413;
            const uint16_t ****l_2411 = &l_2412;
            const uint16_t *****l_2410[8] = {&l_2411,&l_2411,&l_2411,&l_2411,&l_2411,&l_2411,&l_2411,&l_2411};
            int i;
            for (g_1653.f0 = 1; (g_1653.f0 >= 0); g_1653.f0 -= 1)
            { /* block id: 1118 */
                int32_t l_2415 = 6L;
                int i, j;
                l_2406[2] = (l_2405 = ((**g_2173) = (*g_671)));
                (**g_1568) = (void*)0;
                l_2415 ^= (g_252[(g_1653.f0 + 4)][g_1653.f0] = (p_5 < ((safe_rshift_func_int32_t_s_s(0x4201EA4BL, 15)) && ((g_2409[2][1][0] = &g_734[7]) != l_2410[4]))));
                if (p_4)
                    break;
            }
            (**g_2173) = &l_2205[0][8][0];
            for (g_238 = 0; (g_238 <= 4); g_238 += 1)
            { /* block id: 1150 */
                uint32_t l_2466 = 6UL;
                if (p_3)
                    break;
                for (g_2064 = 0; (g_2064 <= 1); g_2064 += 1)
                { /* block id: 1154 */
                    for (g_906.f4 = 0; (g_906.f4 <= 1); g_906.f4 += 1)
                    { /* block id: 1157 */
                        int i, j;
                        return g_252[(g_238 + 1)][g_2064];
                    }
                    for (g_1679.f0 = 0; (g_1679.f0 <= 1); g_1679.f0 += 1)
                    { /* block id: 1162 */
                        uint64_t *l_2465 = &g_2157;
                        int i, j, k;
                        (**g_1727) = ((safe_add_func_uint32_t_u_u((g_252[(g_1679.f0 + 5)][g_2064] , (g_252[(g_2064 + 4)][g_2064] >= 0xD3D4L)), (p_3 >= (*g_261)))) , (((((***g_282) == (safe_mul_func_int64_t_s_s(p_4, p_3))) , (+((*l_2465) ^= (safe_lshift_func_int64_t_s_u((safe_div_func_uint16_t_u_u(((safe_mul_func_int64_t_s_s(p_4, 0xD633E236188BAC01LL)) != 0xEC76C5F78A96D0B8LL), l_2464)), 32))))) | l_2466) | p_6));
                        if ((*l_2326))
                            break;
                    }
                }
            }
        }
        l_2468 = (*g_1727);
    }
    else
    { /* block id: 1172 */
        const uint32_t *l_2488 = &g_1136;
        const uint32_t **l_2487 = &l_2488;
        const uint32_t ***l_2486 = &l_2487;
        int32_t l_2498 = (-1L);
        union U0 *l_2502 = &g_2503;
        l_2469 = l_2469;
        (***g_1568) ^= (&l_2205[0][4][0] != &l_2205[0][8][0]);
        for (g_1679.f0 = 0; (g_1679.f0 >= 7); g_1679.f0 = safe_add_func_uint16_t_u_u(g_1679.f0, 1))
        { /* block id: 1177 */
            uint32_t ***l_2491[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            if (p_7)
            { /* block id: 1178 */
                uint32_t ** const *l_2482 = &g_1134[5][3][1];
                for (g_2291 = 0; (g_2291 != (-23)); g_2291 = safe_sub_func_uint16_t_u_u(g_2291, 1))
                { /* block id: 1181 */
                    uint32_t ****l_2492[10][2] = {{&l_2491[2],&l_2491[2]},{&l_2491[2],&l_2491[2]},{&l_2491[2],&l_2491[2]},{&l_2491[3],&l_2491[2]},{&l_2491[2],&l_2491[2]},{&l_2491[2],&l_2491[2]},{&l_2491[2],&l_2491[3]},{&l_2491[2],&l_2491[2]},{&l_2491[2],&l_2491[2]},{&l_2491[2],&l_2491[2]}};
                    uint64_t *****l_2497 = &g_1802;
                    int32_t l_2500[8] = {0x860A00E6L,0x860A00E6L,0x860A00E6L,0x860A00E6L,0x860A00E6L,0x860A00E6L,0x860A00E6L,0x860A00E6L};
                    int i, j;
                    (****g_1567) = (((safe_mul_func_int32_t_s_s((****g_1725), ((safe_div_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u(0x3375F236L, ((((g_2483 = l_2482) == l_2486) , (safe_add_func_int16_t_s_s(((&l_2230 != (g_2493[1][0][0] = l_2491[2])) || (!((*g_1226) , ((-10L) != ((((++(**g_2484)) , (l_2497 == (void*)0)) && p_6) , l_2498))))), 0x8472L))) > g_2499[1][6]))) , 0xD7L), l_2500[6])) > (*l_2326)))) | (*g_261)) == 7L);
                }
            }
            else
            { /* block id: 1187 */
                union U0 **l_2501[8] = {&g_840,&g_840,&g_840,&g_840,&g_840,&g_840,&g_840,&g_840};
                int i;
                l_2502 = (*g_839);
                for (p_6 = 0; p_6 < 5; p_6 += 1)
                {
                    for (g_1892.f4 = 0; g_1892.f4 < 6; g_1892.f4 += 1)
                    {
                        for (g_2064 = 0; g_2064 < 8; g_2064 += 1)
                        {
                            g_874[p_6][g_1892.f4][g_2064] = &l_2326;
                        }
                    }
                }
                for (g_1892.f0 = 1; (g_1892.f0 >= 0); g_1892.f0 -= 1)
                { /* block id: 1192 */
                    int64_t l_2504[6][4][8] = {{{0L,0L,1L,9L,0xB9A53B729DBD1B41LL,(-8L),0x0C0591D28F1C1515LL,(-1L)},{(-3L),1L,7L,0xFEE60ADF701A50E4LL,0L,0L,0xD57CF03FB6DB4E23LL,0x109D57A820F3FBC0LL},{(-1L),(-1L),0L,7L,3L,0xC5B9237BF76D025ALL,(-10L),0xC252FAF2E1369F4ALL},{1L,(-10L),0x70444DC0829D7028LL,5L,0L,0xB9A53B729DBD1B41LL,9L,8L}},{{2L,6L,(-10L),0x60DC08007495D0D2LL,0L,0x435133D2EA744423LL,0L,7L},{0x0C0591D28F1C1515LL,0xC02C388A77AAD4BFLL,0x109D57A820F3FBC0LL,(-1L),0x117AF15A8779C1CFLL,0L,(-8L),0x636A92AA2B3F103CLL},{0L,0x0C0591D28F1C1515LL,(-1L),2L,8L,3L,8L,2L},{0x78B601E9E3677458LL,5L,0x78B601E9E3677458LL,0L,0L,6L,7L,0xC02C388A77AAD4BFLL}},{{0L,0xEDA0411F8DB44618LL,0L,0xC252FAF2E1369F4ALL,(-6L),9L,0L,0x432AE54056596CF2LL},{0L,0x2E51B1E854B1BCD1LL,0xC02C388A77AAD4BFLL,0L,0L,0x080417E1F4C8475FLL,0xFEB2BDE5A32EAD55LL,(-1L)},{0xCEC0CB0BD1382A2CLL,2L,9L,1L,0L,0x2E51B1E854B1BCD1LL,1L,0L},{(-1L),0x636A92AA2B3F103CLL,0L,(-6L),(-9L),0x0C0591D28F1C1515LL,(-1L),0x435133D2EA744423LL}},{{0x435133D2EA744423LL,7L,6L,9L,3L,(-1L),9L,0xF9DD7FCD8D597E99LL},{0xD57CF03FB6DB4E23LL,8L,0L,0L,3L,0L,0L,(-1L)},{0x636A92AA2B3F103CLL,0xC252FAF2E1369F4ALL,0x109D57A820F3FBC0LL,0x8FDF12E4645F7928LL,0xFEB2BDE5A32EAD55LL,0x435133D2EA744423LL,0x78B601E9E3677458LL,0x78B601E9E3677458LL},{7L,0x109D57A820F3FBC0LL,0x2E51B1E854B1BCD1LL,0x2E51B1E854B1BCD1LL,0x109D57A820F3FBC0LL,7L,0xC252FAF2E1369F4ALL,0x60DC08007495D0D2LL}},{{0L,(-1L),(-9L),(-8L),0x69A21F12AEC7EE54LL,(-6L),0xD57CF03FB6DB4E23LL,0xFEB2BDE5A32EAD55LL},{0xFEE60ADF701A50E4LL,9L,(-1L),(-8L),0L,(-10L),0xB9A53B729DBD1B41LL,0x60DC08007495D0D2LL},{0L,0L,0L,0x2E51B1E854B1BCD1LL,0L,(-1L),0xC02C388A77AAD4BFLL,0x78B601E9E3677458LL},{3L,0xCEC0CB0BD1382A2CLL,(-8L),0x8FDF12E4645F7928LL,0xFEE60ADF701A50E4LL,6L,(-3L),(-1L)}},{{0xC5B9237BF76D025ALL,(-10L),0xC252FAF2E1369F4ALL,0L,(-1L),(-4L),0L,0xF9DD7FCD8D597E99LL},{0L,0x69A21F12AEC7EE54LL,0xEDA0411F8DB44618LL,9L,7L,(-1L),(-9L),0x435133D2EA744423LL},{6L,0x78B601E9E3677458LL,0L,(-6L),0L,0x432AE54056596CF2LL,(-4L),0L},{2L,(-1L),0x70444DC0829D7028LL,1L,7L,1L,0x70444DC0829D7028LL,(-1L)}}};
                    int i, j, k;
                    for (g_1926 = 1; (g_1926 >= 0); g_1926 -= 1)
                    { /* block id: 1195 */
                        int i, j, k;
                        l_2504[1][0][1] |= 3L;
                        return g_88[(g_1892.f0 + 1)][g_1892.f0][(g_1892.f0 + 1)];
                    }
                }
                if (p_5)
                    break;
            }
        }
    }
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_1892 g_261 g_145 g_270 g_271 g_250 g_214 g_120 g_1568 g_874 g_1725 g_1726 g_1727 g_1374 g_906.f0 g_340 g_319 g_279 g_280 g_475 g_476 g_58 g_1225 g_1226 g_150 g_1567 g_650 g_651 g_1755 g_1756 g_210 g_1801 g_1802 g_1803 g_733 g_734 g_735 g_1663 g_739 g_740 g_741 g_398 g_671 g_1431 g_204 g_649 g_256 g_2157 g_318 g_1892.f4
 * writes: g_1892.f4 g_120 g_906.f0 g_280 g_815 g_1913 g_58 g_1926 g_875 g_994.f4 g_214 g_11 g_210 g_204 g_1679.f4 g_150 g_2171
 */
static uint32_t  func_15(int8_t  p_16, int8_t  p_17, int32_t * p_18)
{ /* block id: 910 */
    uint32_t **l_1880 = &g_1135;
    uint32_t ***l_1881 = &g_1134[7][1][1];
    uint32_t **l_1882 = &g_1135;
    const int32_t l_1883 = 5L;
    uint8_t l_1899 = 0x30L;
    uint16_t l_1900 = 0x972BL;
    int32_t l_1901 = 0L;
    int64_t * const l_1928 = (void*)0;
    int64_t * const *l_1927 = &l_1928;
    int64_t **l_1933 = &g_261;
    const int64_t * const l_1953 = (void*)0;
    const int64_t * const *l_1952 = &l_1953;
    int32_t l_1977 = 0L;
    uint16_t ** const *l_2008 = (void*)0;
    uint16_t ** const * const *l_2007 = &l_2008;
    int32_t l_2060 = 9L;
    int32_t l_2063 = 1L;
    uint64_t ****l_2087 = &g_1803;
    int32_t *l_2119 = (void*)0;
    const uint8_t l_2167 = 0xECL;
    int32_t ** const *l_2170 = &g_874[0][1][5];
    int32_t ** const **l_2169[1][2];
    int32_t ** const ***l_2168 = &l_2169[0][1];
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_2169[i][j] = &l_2170;
    }
lbl_2178:
    l_1901 |= (safe_mul_func_int64_t_s_s((((p_16 , ((l_1880 == (l_1882 = &g_1135)) , l_1883)) > (safe_div_func_uint32_t_u_u(((l_1883 & (safe_mod_func_uint64_t_u_u(l_1883, (((safe_mul_func_int32_t_s_s(((safe_mul_func_int64_t_s_s((g_1892 , (safe_lshift_func_uint8_t_u_u((safe_div_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u((((l_1883 ^ (((1UL > (*g_261)) <= 0xB8AD74A273B085ACLL) , l_1883)) && (**g_270)) < l_1899), l_1883)), l_1899)), p_16))), 0x1CE02AA1CB1FBDEFLL)) == l_1883), p_17)) ^ 0UL) & l_1900)))) , 4294967295UL), (*g_214)))) , (*g_261)), l_1899));
    for (g_1892.f4 = 0; (g_1892.f4 != 20); g_1892.f4++)
    { /* block id: 915 */
        const union U0 **l_1911 = (void*)0;
        union U0 ***l_1912 = &g_815;
        int32_t l_1914[10][6] = {{0xBC5BEE65L,0x5AC17421L,0x5CC5A9E0L,(-1L),0x7E3D7335L,(-6L)},{(-8L),0x5AC17421L,(-7L),(-7L),0x5AC17421L,(-8L)},{0x5AC17421L,1L,0L,0xA6319153L,(-1L),0xC8B76998L},{(-7L),0L,0x7E3D7335L,0x7E343F90L,(-3L),0xEC0C3860L},{(-7L),0xA6319153L,0x7E343F90L,0xA6319153L,(-7L),(-1L)},{0x5AC17421L,(-1L),(-3L),(-7L),0xEC0C3860L,1L},{(-8L),0xC8B76998L,0xEC0C3860L,(-1L),1L,1L},{0xBC5BEE65L,(-3L),(-3L),0xBC5BEE65L,(-6L),(-1L)},{1L,5L,0x7E343F90L,1L,0xA6319153L,0xEC0C3860L},{0x7E343F90L,(-8L),0x7E3D7335L,0xC8B76998L,0xA6319153L,0xC8B76998L}};
        int64_t * const l_1925 = &g_1926;
        int64_t * const *l_1924 = &l_1925;
        int64_t * const **l_1923[8][4][4] = {{{&l_1924,&l_1924,&l_1924,&l_1924},{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,(void*)0,&l_1924},{&l_1924,&l_1924,&l_1924,&l_1924}},{{&l_1924,(void*)0,(void*)0,&l_1924},{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,&l_1924,(void*)0}},{{&l_1924,&l_1924,&l_1924,(void*)0},{(void*)0,&l_1924,&l_1924,&l_1924},{&l_1924,(void*)0,&l_1924,&l_1924},{&l_1924,(void*)0,(void*)0,&l_1924}},{{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,&l_1924,&l_1924},{(void*)0,&l_1924,&l_1924,&l_1924}},{{&l_1924,&l_1924,&l_1924,&l_1924},{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,(void*)0,(void*)0},{&l_1924,&l_1924,(void*)0,&l_1924}},{{&l_1924,(void*)0,(void*)0,(void*)0},{&l_1924,&l_1924,(void*)0,&l_1924},{&l_1924,&l_1924,&l_1924,&l_1924},{&l_1924,&l_1924,&l_1924,&l_1924}},{{&l_1924,&l_1924,&l_1924,(void*)0},{(void*)0,(void*)0,&l_1924,&l_1924},{&l_1924,&l_1924,&l_1924,(void*)0},{&l_1924,&l_1924,&l_1924,&l_1924}},{{&l_1924,&l_1924,(void*)0,&l_1924},{&l_1924,&l_1924,&l_1924,&l_1924},{&l_1924,&l_1924,&l_1924,(void*)0},{(void*)0,&l_1924,&l_1924,&l_1924}}};
        union U0 *l_1929[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int64_t **l_1930 = (void*)0;
        int64_t ***l_1931 = (void*)0;
        int64_t ***l_1932[1];
        uint32_t l_1965 = 18446744073709551615UL;
        int32_t l_2054 = 0xFA580608L;
        const int32_t **l_2071 = (void*)0;
        const int32_t ***l_2070[2];
        const int32_t ****l_2069[5][2] = {{&l_2070[1],&l_2070[1]},{&l_2070[1],&l_2070[1]},{&l_2070[1],&l_2070[1]},{&l_2070[1],&l_2070[1]},{&l_2070[1],&l_2070[1]}};
        uint8_t ***l_2151 = &g_863;
        int32_t **l_2154 = &g_398;
        int16_t ***l_2165[4][1][10] = {{{&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319}},{{&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319}},{{&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319}},{{&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319,&g_319}}};
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1932[i] = &l_1930;
        for (i = 0; i < 2; i++)
            l_2070[i] = &l_2071;
        (****g_1725) ^= (p_17 <= ((***g_1568) = p_16));
        if ((((+(((&g_261 == (l_1933 = ((safe_lshift_func_int8_t_s_s((~((((l_1927 = ((((255UL || ((((***g_1726) = (safe_lshift_func_uint16_t_u_s((!(-6L)), ((***g_340) |= 0xE6DEL)))) , l_1911) == (g_1913 = ((*l_1912) = (void*)0)))) ^ ((**g_475)++)) > (safe_div_func_int16_t_s_s((safe_add_func_int32_t_s_s(l_1900, (((safe_add_func_int64_t_s_s(p_17, (**g_1225))) & 7L) & 0x093B4893B3D37023LL))), 65535UL))) , &g_261)) == &g_261) , l_1929[1]) == (void*)0)), p_17)) , l_1930))) == 8L) , p_16)) || 0xB2L) <= 2UL))
        { /* block id: 925 */
            int64_t l_1940 = 0L;
            int32_t l_1942[5][7] = {{0x6298842AL,0x5888D753L,0x5888D753L,0x6298842AL,0L,0xE1508690L,0x9564C31DL},{0xE1508690L,0x5888D753L,0L,1L,0xEECD7248L,0L,1L},{0x9564C31DL,2L,0xE1508690L,(-9L),0xE1508690L,2L,0x9564C31DL},{0x5888D753L,0x6298842AL,0L,0xE1508690L,0x9564C31DL,0L,0x3E1A9083L},{1L,0L,0L,0x9564C31DL,0x99716219L,0x99716219L,0x9564C31DL}};
            int8_t l_1966 = 0L;
            uint16_t l_1978 = 7UL;
            uint16_t **l_1994 = &g_476;
            int32_t l_2018 = 0x4F650C8FL;
            uint8_t *l_2031 = &g_1467[3][4][1];
            int32_t *l_2055[3];
            uint8_t ***l_2099 = &g_863;
            int64_t l_2103 = 1L;
            uint32_t l_2137 = 0x2479DA46L;
            int i, j;
            for (i = 0; i < 3; i++)
                l_2055[i] = &l_1901;
            for (l_1899 = 0; (l_1899 >= 54); l_1899 = safe_add_func_uint8_t_u_u(l_1899, 2))
            { /* block id: 928 */
                int32_t l_1936 = 0x88DA731AL;
                int32_t l_1941[1][4][7] = {{{0x083FEE0FL,0x703973F5L,0xB730A09CL,0xB730A09CL,0x703973F5L,0x083FEE0FL,(-1L)},{0x4FF4E89BL,0x0E75D753L,0xBCEDCE79L,0xBCEDCE79L,0x0E75D753L,0x4FF4E89BL,(-1L)},{0x083FEE0FL,0x703973F5L,0xB730A09CL,0xB730A09CL,0x703973F5L,0x083FEE0FL,(-1L)},{0x4FF4E89BL,0x0E75D753L,0xBCEDCE79L,0xBCEDCE79L,0x0E75D753L,0x4FF4E89BL,(-1L)}}};
                const int64_t * const l_1950 = &l_1940;
                const int64_t * const *l_1949 = &l_1950;
                int32_t l_2019 = 0x0F632A2EL;
                int i, j, k;
                (***g_1726) |= p_16;
                for (g_1926 = 4; (g_1926 >= 1); g_1926 -= 1)
                { /* block id: 932 */
                    int32_t l_1939[6] = {0x40AF7A22L,0x40AF7A22L,0x40AF7A22L,0x40AF7A22L,0x40AF7A22L,0x40AF7A22L};
                    uint16_t ****l_2017 = &g_735;
                    int i, j, k;
                    if (p_16)
                    { /* block id: 933 */
                        int32_t *l_1937 = (void*)0;
                        int32_t *l_1938[7][4][2];
                        uint32_t l_1943 = 1UL;
                        const int64_t * const **l_1951[10][4][3] = {{{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,(void*)0},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949}},{{(void*)0,&l_1949,&l_1949},{(void*)0,(void*)0,&l_1949},{&l_1949,(void*)0,&l_1949},{&l_1949,&l_1949,&l_1949}},{{&l_1949,&l_1949,&l_1949},{&l_1949,(void*)0,(void*)0},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949}},{{&l_1949,&l_1949,(void*)0},{&l_1949,&l_1949,(void*)0},{(void*)0,&l_1949,&l_1949},{(void*)0,&l_1949,&l_1949}},{{&l_1949,&l_1949,&l_1949},{&l_1949,(void*)0,&l_1949},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949}},{{&l_1949,(void*)0,(void*)0},{&l_1949,(void*)0,(void*)0},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949}},{{&l_1949,&l_1949,(void*)0},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949},{(void*)0,&l_1949,&l_1949}},{{(void*)0,(void*)0,&l_1949},{&l_1949,(void*)0,&l_1949},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949}},{{&l_1949,(void*)0,(void*)0},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949},{&l_1949,&l_1949,(void*)0}},{{&l_1949,&l_1949,(void*)0},{(void*)0,&l_1949,&l_1949},{(void*)0,&l_1949,&l_1949},{&l_1949,&l_1949,&l_1949}}};
                        int8_t *l_1964 = &g_875[0];
                        int i, j, k;
                        for (i = 0; i < 7; i++)
                        {
                            for (j = 0; j < 4; j++)
                            {
                                for (k = 0; k < 2; k++)
                                    l_1938[i][j][k] = &g_252[7][1];
                            }
                        }
                        l_1943++;
                        l_1901 |= (((****g_1567) == p_16) < (!(((safe_rshift_func_uint64_t_u_s((4294967295UL & (((l_1952 = l_1949) == (void*)0) & ((safe_div_func_int64_t_s_s((safe_mul_func_int64_t_s_s(((p_16 != ((*l_1964) = (safe_rshift_func_int32_t_s_s((0x80A5L >= ((safe_lshift_func_uint16_t_u_u(((l_1914[7][1] = (safe_div_func_uint8_t_u_u((p_16 == p_16), (*g_650)))) && p_16), p_16)) != (**g_1755))), l_1899)))) | l_1965), 0x32D6C208DA0A4CD7LL)), (*****g_1801))) < 0x4DL))), l_1941[0][1][2])) >= (**g_1755)) & l_1966)));
                    }
                    else
                    { /* block id: 939 */
                        int32_t *l_1967 = &g_252[3][1];
                        int32_t *l_1968 = &l_1942[4][2];
                        int32_t *l_1969 = &g_519[1][4][1];
                        int32_t *l_1970 = &g_690[0].f0;
                        int32_t *l_1971 = &l_1942[2][2];
                        int32_t *l_1972 = &l_1939[5];
                        int32_t *l_1973 = &g_896[3][3][0].f0;
                        int32_t *l_1974 = &g_896[3][3][0].f0;
                        int32_t *l_1975 = &l_1901;
                        int32_t *l_1976[7] = {&l_1914[5][0],&l_1914[5][0],&l_1914[5][0],&l_1914[5][0],&l_1914[5][0],&l_1914[5][0],&l_1914[5][0]};
                        uint16_t **l_1993 = &g_476;
                        uint16_t ***l_1995 = &l_1994;
                        int8_t *l_2006 = &l_1966;
                        int i;
                        ++l_1978;
                        (*l_1968) = (((((safe_mul_func_int32_t_s_s((safe_rshift_func_uint64_t_u_u(p_16, ((safe_mul_func_uint32_t_u_u((((*****g_733)++) <= p_16), (((((safe_add_func_uint64_t_u_u(l_1941[0][3][1], (safe_mod_func_int8_t_s_s(((((l_1901 = 0x36DEL) && p_17) , l_1993) == ((*l_1995) = l_1994)), (safe_add_func_int64_t_s_s(((*g_271) && ((safe_add_func_uint8_t_u_u(((l_1941[0][1][2] < 0UL) > 0x7C02L), 0xA1L)) || p_17)), (****g_1802))))))) | 0xA7L) <= (*g_271)) ^ 0x46EA90CDL) , p_16))) | l_1899))), 0x05D7CF0FL)) != (*g_261)) || p_16) < l_1940) < p_17);
                        l_1976[2] = &l_1941[0][2][0];
                        (**g_1727) = 0x35A32679L;
                    }
                    for (g_994.f4 = 0; (g_994.f4 <= 0); g_994.f4 += 1)
                    { /* block id: 956 */
                        (**g_1568) = func_31(l_1965, (****g_1663));
                        if ((**g_671))
                            continue;
                    }
                    return p_17;
                }
                l_1914[7][1] &= (l_2019 ^= (l_1941[0][1][2] , (l_1901 ^= (**g_1431))));
            }
            for (g_11 = 0; (g_11 < (-10)); g_11--)
            { /* block id: 968 */
                uint32_t *l_2027[1];
                uint8_t **l_2028 = (void*)0;
                uint8_t *l_2030 = &g_250;
                uint8_t **l_2029 = &l_2030;
                int8_t *l_2032 = (void*)0;
                int8_t *l_2033 = &g_204;
                int8_t *l_2034 = (void*)0;
                int8_t *l_2035 = &l_1966;
                const int32_t *l_2045 = &g_896[3][3][0].f0;
                int32_t l_2072[5];
                int16_t l_2102[1][9] = {{0x851AL,0x851AL,0x851AL,0x851AL,0x851AL,0x851AL,0x851AL,0x851AL,0x851AL}};
                int i, j;
                for (i = 0; i < 1; i++)
                    l_2027[i] = &g_1808.f4;
                for (i = 0; i < 5; i++)
                    l_2072[i] = 0xAED1D25BL;
                (***g_1568) ^= ((((*l_2035) ^= (safe_mod_func_uint64_t_u_u(((safe_add_func_int16_t_s_s((p_16 || ((*****g_733) >= (((+7UL) || 4294967295UL) < (l_1914[7][1] = ((**g_1755) = p_16))))), (((*l_2033) &= (l_1978 && (l_1977 , (((*l_2029) = &g_1467[1][0][0]) == l_2031)))) ^ l_1965))) ^ 18446744073709551607UL), (*g_1226)))) || (**g_649)) && 0xC8D6F8B1EDE8DF52LL);
                for (l_1899 = 0; (l_1899 >= 23); l_1899 = safe_add_func_uint32_t_u_u(l_1899, 3))
                { /* block id: 977 */
                    int8_t l_2040 = 0x8BL;
                    uint32_t ***l_2041 = &g_1755;
                    const int32_t *l_2043 = &g_2044;
                    int16_t *l_2046 = (void*)0;
                    uint16_t **l_2051 = &g_476;
                    int32_t l_2056[1][4] = {{(-2L),(-2L),(-2L),(-2L)}};
                    int8_t l_2058 = 5L;
                    uint64_t l_2115 = 18446744073709551611UL;
                    uint16_t l_2116 = 0x5B4CL;
                    int i, j;
                    for (g_1679.f4 = 0; (g_1679.f4 <= 3); g_1679.f4 += 1)
                    { /* block id: 980 */
                        return p_16;
                    }
                }
            }
            (****g_1567) ^= (safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_s(((l_2137 , ((safe_rshift_func_int64_t_s_u(((safe_add_func_uint16_t_u_u(p_17, (safe_lshift_func_uint16_t_u_u(0x5FAEL, (~(*g_1374)))))) < 1UL), ((p_16 > (!(((p_16 == (safe_mod_func_uint8_t_u_u((+(((1UL ^ (safe_lshift_func_int64_t_s_s(((void*)0 == l_2151), p_16))) >= (*g_261)) , (*g_476))), 0x71L))) == p_16) , p_16))) , (****g_1802)))) <= p_16)) <= p_17), p_16)), p_17));
        }
        else
        { /* block id: 1022 */
            uint32_t *****l_2158 = (void*)0;
            uint32_t *****l_2159 = (void*)0;
            uint32_t ****l_2161 = &l_1881;
            uint32_t *****l_2160 = &l_2161;
            int16_t * const **l_2164 = (void*)0;
            int32_t **l_2166 = &g_398;
            int32_t ***** const l_2176[1] = {&g_1567};
            int8_t *l_2177[1];
            int i;
            for (i = 0; i < 1; i++)
                l_2177[i] = &g_714;
            (**g_1727) = (((((*g_1756) >= p_16) , (safe_sub_func_uint16_t_u_u(((**g_256) != ((l_2154 = &p_18) != (((**g_475) = (safe_lshift_func_int32_t_s_s(g_2157, (((*****g_1801) = p_17) , ((((*l_2160) = &l_1881) != &l_1881) < (safe_rshift_func_int16_t_s_s(((l_2164 != l_2165[1][0][6]) ^ p_16), (***g_318)))))))) , l_2166))), l_2167))) > 0x44L) || (*g_261));
            (***g_1568) &= ((p_16 ^ ((*g_1756) ^= ((g_2171 = l_2168) != l_2176[0]))) > (p_17 = 0x57L));
            if (g_150)
                goto lbl_2178;
            return (*g_1756);
        }
    }
    return p_17;
}


/* ------------------------------------------ */
/* 
 * reads : g_1225 g_1226 g_150 g_261 g_145 g_650 g_651 g_1755 g_1756 g_210 g_271 g_250 g_282 g_278 g_279 g_280 g_214 g_120 g_1662 g_739 g_740 g_741 g_398
 * writes: g_145 g_120 g_985.f4 g_875
 */
static int32_t * func_19(int64_t  p_20, uint32_t  p_21)
{ /* block id: 761 */
    int32_t l_1612 = (-7L);
    int32_t l_1637[4] = {0x96E84323L,0x96E84323L,0x96E84323L,0x96E84323L};
    int64_t l_1645 = 0x72DB8DA277E6C4F0LL;
    int32_t *****l_1661[6];
    int32_t l_1710 = 0L;
    int64_t **l_1738[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t **l_1753 = (void*)0;
    int16_t ***l_1796 = (void*)0;
    uint64_t *****l_1822 = &g_1802;
    uint8_t l_1860 = 0x97L;
    int i;
    for (i = 0; i < 6; i++)
        l_1661[i] = &g_739;
    if (((**g_1225) == (-1L)))
    { /* block id: 762 */
        int32_t *l_1598 = &g_238;
        int32_t *l_1599 = (void*)0;
        int32_t *l_1600[4][7] = {{&g_906.f0,&g_519[2][0][2],&g_519[2][0][2],&g_906.f0,&g_906.f0,&g_519[2][0][2],&g_519[2][0][2]},{&g_906.f0,&g_690[0].f0,&g_906.f0,&g_690[0].f0,&g_906.f0,&g_690[0].f0,&g_906.f0},{&g_906.f0,&g_906.f0,&g_519[2][0][2],&g_519[2][0][2],&g_906.f0,&g_906.f0,&g_519[2][0][2]},{&g_238,&g_690[0].f0,&g_238,&g_690[0].f0,&g_238,&g_690[0].f0,&g_238}};
        uint32_t l_1601 = 9UL;
        uint32_t *l_1606 = &g_1168.f4;
        uint64_t *l_1617[1];
        uint64_t *l_1619 = &g_150;
        uint64_t **l_1618 = &l_1619;
        union U0 *l_1670 = &g_896[3][3][0];
        int32_t **l_1688[3][2] = {{&l_1599,&l_1599},{&g_214,&l_1599},{&l_1599,&g_214}};
        int16_t * const *l_1693 = &g_279;
        int16_t l_1707 = 0xC5EAL;
        int16_t l_1708 = (-5L);
        int32_t ****l_1734 = &g_740;
        const int32_t l_1759 = 0xB3DC3294L;
        int i, j;
        for (i = 0; i < 1; i++)
            l_1617[i] = &g_150;
        --l_1601;
    }
    else
    { /* block id: 902 */
        int16_t l_1858 = 0x53A6L;
        int32_t *l_1859[2];
        int64_t **l_1877[4];
        int i;
        for (i = 0; i < 2; i++)
            l_1859[i] = &g_704.f0;
        for (i = 0; i < 4; i++)
            l_1877[i] = (void*)0;
        l_1860++;
        l_1710 ^= ((*g_214) &= (safe_rshift_func_int64_t_s_u(((*g_261) = (*g_261)), ((*g_650) || (+((**g_1755) == (((safe_rshift_func_int32_t_s_s(((*g_271) , (safe_add_func_uint32_t_u_u(4294967295UL, (safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u((~(p_21 && p_20)), ((l_1877[2] != (void*)0) | p_20))) != p_21), 0xF1L)), (***g_282)))))), 2)) > (**g_1755)) & l_1637[3])))))));
    }
    for (g_985.f4 = 0; g_985.f4 < 2; g_985.f4 += 1)
    {
        g_875[g_985.f4] = 0xBAL;
    }
    return (****g_1662);
}


/* ------------------------------------------ */
/* 
 * reads : g_1568 g_874 g_214 g_120 g_256
 * writes: g_120
 */
static int32_t  func_22(uint64_t  p_23, uint64_t  p_24, int8_t  p_25)
{ /* block id: 757 */
    int8_t l_1597 = 0x9AL;
    (***g_1568) &= (l_1597 ^= p_25);
    return (**g_256);
}


/* ------------------------------------------ */
/* 
 * reads : g_1568 g_874 g_997.f0
 * writes: g_214
 */
static uint32_t  func_28(int32_t * p_29, int32_t * p_30)
{ /* block id: 754 */
    (**g_1568) = p_30;
    return g_997.f0;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_31(const uint8_t  p_32, int32_t * p_33)
{ /* block id: 751 */
    int32_t *l_1575 = &g_252[6][1];
    int32_t *l_1576 = (void*)0;
    int32_t *l_1577 = &g_50;
    int32_t *l_1578 = &g_906.f0;
    int32_t *l_1579 = &g_252[6][1];
    int32_t *l_1580 = (void*)0;
    int32_t *l_1581 = &g_50;
    int32_t *l_1582 = (void*)0;
    int32_t *l_1583 = &g_252[6][1];
    int32_t l_1584[6];
    int32_t *l_1585 = &l_1584[3];
    int32_t *l_1586 = &g_252[6][1];
    int32_t *l_1587 = (void*)0;
    int32_t *l_1588 = &g_896[3][3][0].f0;
    int32_t *l_1589 = &g_896[3][3][0].f0;
    int32_t *l_1590 = &g_896[3][3][0].f0;
    int32_t *l_1591 = &g_519[2][2][6];
    int32_t *l_1592 = (void*)0;
    int32_t *l_1593[3];
    uint64_t l_1594 = 0x23AE9C8A77373C5CLL;
    int i;
    for (i = 0; i < 6; i++)
        l_1584[i] = 0xFEED37C1L;
    for (i = 0; i < 3; i++)
        l_1593[i] = &g_704.f0;
    --l_1594;
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads : g_250 g_261 g_475 g_476 g_58 g_270 g_271 g_650 g_651 g_143 g_735 g_398 g_214 g_690.f0 g_519 g_120 g_11 g_50 g_654 g_655 g_656 g_875 g_210 g_213 g_739 g_740 g_741 g_340 g_319 g_279 g_145 g_1107 g_649 g_280 g_714 g_318 g_738 g_1133 g_1357 g_874 g_1370 g_1225 g_1226 g_150 g_204 g_1374 g_269 g_64 g_282 g_278 g_1168.f0 g_1431 g_1210 g_1447 g_704.f0 g_1467 g_671 g_1497 g_1567 g_1568
 * writes: g_250 g_704.f0 g_172 g_145 g_435 g_143 g_690.f0 g_58 g_120 g_875 g_470 g_214 g_280 g_714 g_519 g_1107 g_994.f4 g_50 g_1134 g_238 g_150 g_204 g_906.f0 g_1210 g_1168.f0 g_275 g_1497
 */
static int32_t * func_34(int32_t * const  p_35)
{ /* block id: 502 */
    int8_t l_1062 = (-6L);
    uint32_t *l_1071 = (void*)0;
    int32_t l_1072 = (-1L);
    int32_t l_1103 = 2L;
    int32_t l_1105[3][9][9] = {{{0x23A077C1L,1L,(-1L),0x8EA38B45L,0L,0x4D5C2E4AL,1L,1L,0x8854AB3FL},{(-1L),0x973D712EL,3L,0x643A4971L,3L,(-1L),3L,1L,3L},{3L,3L,0x8A396189L,0x8A396189L,3L,3L,0x5D24120BL,(-1L),3L},{0xBA12B6F0L,1L,0x43AC7ED5L,0x50A33079L,1L,(-1L),3L,(-2L),5L},{(-1L),0x8965670AL,0x23A077C1L,(-2L),0x43AC7ED5L,0L,0x5D24120BL,0x8EA38B45L,0x5F29DA4CL},{0x4D5C2E4AL,(-2L),(-1L),0xD3E69AFCL,(-1L),3L,3L,0x973D712EL,0xE3C8657AL},{0xD562F84EL,(-1L),4L,0x54C958EBL,(-1L),0xBA12B6F0L,1L,0L,0x6280E4A6L},{0x54C958EBL,3L,(-1L),1L,(-1L),0x9FCD10A9L,0xAB2D1D60L,0xEDD8A960L,1L},{0xE7B0EBC3L,0x457C0C8FL,1L,(-1L),0x9FCD10A9L,0xB9E31CB1L,(-4L),1L,1L}},{{(-1L),0xF5C2E57FL,0x1C162152L,1L,(-1L),0x2AE05624L,(-1L),0x23A077C1L,0x6280E4A6L},{0L,(-1L),0x973D712EL,0xE52FDF18L,0L,0x4D5C2E4AL,0x6280E4A6L,1L,0xE3C8657AL},{0xB9E31CB1L,0x5F29DA4CL,0x50A33079L,(-1L),(-1L),(-1L),(-1L),0x50A33079L,0x5F29DA4CL},{1L,(-1L),0L,0L,3L,1L,0L,0xEDD8A960L,5L},{0L,0xB9E31CB1L,0x6280E4A6L,0L,(-2L),(-4L),0x4E9BE862L,3L,3L},{0x58EE4CF1L,(-1L),0x4D5C2E4AL,(-2L),(-1L),0xD3E69AFCL,(-1L),3L,3L},{0x8EA38B45L,0x5F29DA4CL,1L,0x2AE05624L,0x4AE7FA9AL,0xE52FDF18L,0x5F29DA4CL,0x643A4971L,0x8854AB3FL},{0x3C85A14DL,(-1L),4L,0x43AC7ED5L,(-1L),(-1L),3L,0L,0L},{0x8EA38B45L,(-1L),0xE27DE0B5L,0x5D24120BL,0L,1L,0x2100034AL,0xE3C872A4L,0x5628E65DL}},{{1L,0x5F29DA4CL,0x8EA38B45L,0x5D24120BL,0L,0x43AC7ED5L,(-2L),0x23A077C1L,0x8965670AL},{3L,1L,(-7L),0x4D5C2E4AL,0xE3C8657AL,3L,(-1L),(-1L),0x8EA38B45L},{3L,0x1C162152L,0x4D5C2E4AL,0x8A396189L,9L,0L,0L,0L,0L},{1L,0L,(-1L),0L,1L,1L,(-1L),0x5D24120BL,0x83CA3327L},{1L,0xBA12B6F0L,0xE27DE0B5L,0L,0x4E9BE862L,0xD3E69AFCL,3L,0xF48196D8L,(-1L)},{1L,0x6280E4A6L,0x1C162152L,0x5628E65DL,3L,1L,0x643A4971L,0x2518B86CL,0x4AE7FA9AL},{(-1L),1L,0x01E9FA1AL,0x23A077C1L,0x2AE05624L,0L,0x457C0C8FL,0x50A33079L,0x23A077C1L},{5L,0xD3E69AFCL,1L,0x01E9FA1AL,0x457C0C8FL,3L,0x83CA3327L,(-1L),3L},{1L,1L,0x4AE7FA9AL,0x8EA38B45L,0x6280E4A6L,0x43AC7ED5L,0x1C162152L,(-1L),0x2AE05624L}}};
    const int8_t *l_1114 = &g_1115;
    uint64_t *l_1120[5][3][9] = {{{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,(void*)0,&g_150}},{{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,(void*)0,(void*)0,&g_150,(void*)0,(void*)0,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150}},{{&g_150,&g_150,(void*)0,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150}},{{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,(void*)0,&g_150,&g_150,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150}},{{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150},{(void*)0,&g_150,&g_150,&g_150,&g_150,&g_150,(void*)0,&g_150,&g_150},{&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150,&g_150}}};
    const uint32_t *l_1157 = &g_1136;
    const uint32_t **l_1156 = &l_1157;
    const uint32_t ***l_1155[1];
    uint8_t l_1217 = 0xF9L;
    int32_t ***l_1304 = &g_874[3][0][7];
    int8_t l_1314 = 8L;
    int8_t l_1430 = 0x42L;
    uint8_t *l_1486 = (void*)0;
    int32_t l_1532 = 0x7E5D0305L;
    int8_t l_1538 = (-1L);
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1155[i] = &l_1156;
    for (g_250 = 0; (g_250 > 55); g_250 = safe_add_func_uint8_t_u_u(g_250, 5))
    { /* block id: 505 */
        int32_t l_1051 = 0L;
        int32_t l_1073 = 7L;
        int32_t l_1096 = 0x337B765AL;
        int32_t l_1104[10] = {0xD29D6232L,0xCF78A984L,0xAB3834D0L,0xAB3834D0L,0xCF78A984L,0xD29D6232L,0xCF78A984L,0xAB3834D0L,0xAB3834D0L,0xCF78A984L};
        const int8_t *l_1113 = &g_204;
        int16_t **l_1121 = &g_279;
        int32_t **l_1150 = &g_214;
        uint32_t ***l_1154 = (void*)0;
        int32_t l_1185 = (-8L);
        int64_t l_1209 = 0x2764135F407E96FBLL;
        uint64_t **l_1223 = (void*)0;
        int8_t l_1253 = 0x1FL;
        uint16_t **l_1256[5];
        uint8_t l_1265 = 1UL;
        int32_t l_1287 = 0xF613FFE7L;
        int32_t ***l_1305 = &g_874[3][5][5];
        uint64_t l_1322 = 0x5355C59CE6B6B303LL;
        uint64_t ***l_1372 = &l_1223;
        uint64_t ****l_1371 = &l_1372;
        int64_t l_1398 = 0L;
        uint8_t *l_1488 = (void*)0;
        int8_t l_1500 = 0L;
        union U0 ***l_1562 = &g_815;
        int i;
        for (i = 0; i < 5; i++)
            l_1256[i] = &g_476;
        for (g_704.f0 = 0; (g_704.f0 >= 21); g_704.f0 = safe_add_func_uint32_t_u_u(g_704.f0, 7))
        { /* block id: 508 */
            int32_t *l_1065 = &g_11;
            int32_t l_1066 = (-5L);
            uint16_t **l_1088 = &g_476;
            int32_t *l_1094 = &g_252[6][1];
            int32_t *l_1095 = &g_994.f0;
            int32_t *l_1097 = &l_1051;
            int32_t *l_1098 = (void*)0;
            int32_t *l_1099 = &g_50;
            int32_t *l_1100 = &l_1066;
            int32_t *l_1101 = &g_690[0].f0;
            int32_t *l_1102[7][4] = {{(void*)0,(void*)0,&g_238,&g_238},{(void*)0,(void*)0,&g_238,&g_238},{(void*)0,(void*)0,&g_238,&g_238},{(void*)0,(void*)0,&g_238,&g_238},{(void*)0,(void*)0,&g_238,&g_238},{(void*)0,(void*)0,&g_238,&g_238},{(void*)0,(void*)0,&g_238,&g_238}};
            int64_t l_1106 = 1L;
            uint32_t l_1127 = 8UL;
            int i, j;
            for (g_172 = 0; (g_172 > 31); g_172 = safe_add_func_uint32_t_u_u(g_172, 1))
            { /* block id: 511 */
                const int32_t **l_1060 = (void*)0;
                const int32_t **l_1061 = &g_435;
                int64_t *l_1063 = &g_143;
                int32_t *l_1064 = &g_690[0].f0;
                int32_t **l_1074 = &g_214;
                int8_t *l_1093[4] = {&g_875[1],&g_875[1],&g_875[1],&g_875[1]};
                int i;
                (*l_1064) = ((+(((*l_1063) &= (((((((*g_261) = l_1051) == l_1051) || ((1UL > 0x8261F4B6761A4BD3LL) | (safe_lshift_func_uint32_t_u_u((safe_div_func_int32_t_s_s((((((((safe_lshift_func_uint16_t_u_s((**g_475), (safe_mul_func_int8_t_s_s(((&l_1051 == ((*l_1061) = ((0xF82973E2L || (l_1051 || (l_1051 >= 255UL))) , &l_1051))) != l_1062), l_1051)))) , (**g_270)) || 0x72L) < l_1062) , (-4L)) , 0UL) != l_1051), l_1062)), 10)))) && (**g_475)) || (*g_650)) == 8UL)) , (-1L))) , 0x14426B89L);
                (*l_1074) = func_36(l_1065, ((l_1072 |= ((*g_398) = (0xBAE9L || (l_1066 , ((***g_735) = (!((((l_1066 & ((safe_unary_minus_func_int32_t_s((4294967292UL || (l_1051 >= (*g_476))))) > (l_1051 && (safe_div_func_uint16_t_u_u(0x74A1L, l_1066))))) , l_1071) == (void*)0) , 0xCBL))))))) , (void*)0), (*g_271), l_1073);
                (**l_1074) = ((safe_div_func_int8_t_s_s(((safe_unary_minus_func_uint64_t_u((!((safe_div_func_int64_t_s_s(((g_714 = (+(safe_mod_func_uint16_t_u_u(((((((safe_div_func_int16_t_s_s((safe_lshift_func_int8_t_s_u((((***g_340) = ((**l_1074) >= ((*l_1064) = ((void*)0 == l_1088)))) <= (((**g_270) , l_1062) > ((void*)0 == &g_650))), ((((safe_add_func_int16_t_s_s((l_1072 = ((safe_lshift_func_uint16_t_u_s(((*g_271) >= ((((*g_261) = (l_1066 , l_1062)) == 5L) || l_1062)), 11)) < l_1062)), (*g_476))) == l_1051) | 4294967295UL) == 65529UL))), l_1066)) & (**g_654)) && (-1L)) ^ 0x58EBF681L) , 1L) & 1L), 65535UL)))) && l_1066), l_1062)) , (*g_261))))) != l_1066), 0x56L)) >= (**g_270));
            }
            ++g_1107;
            if (l_1104[6])
            { /* block id: 528 */
                uint32_t l_1110 = 0xE55B264AL;
                (*l_1100) &= l_1110;
                for (g_994.f4 = 0; (g_994.f4 > 7); g_994.f4 = safe_add_func_uint32_t_u_u(g_994.f4, 5))
                { /* block id: 532 */
                    uint8_t *l_1124[4][4] = {{&g_250,(void*)0,&g_465,&g_465},{&g_172,&g_172,&g_250,(void*)0},{(void*)0,&g_250,&g_250,&g_250},{&g_172,&g_465,&g_465,&g_250}};
                    int32_t l_1125 = (-3L);
                    uint16_t l_1126 = 0UL;
                    int i, j;
                    (*l_1099) = (((l_1114 = l_1113) != (*g_649)) , ((*l_1097) &= ((l_1072 = ((safe_rshift_func_uint16_t_u_s((((((safe_lshift_func_int8_t_s_s((*g_650), 1)) , l_1120[4][1][7]) == (void*)0) , l_1121) == ((((((l_1096 = (-5L)) <= (-5L)) || ((safe_add_func_int8_t_s_s((((*p_35) = ((l_1125 ^= l_1096) > l_1110)) , 1L), (*l_1101))) == (***g_340))) & (*g_271)) , g_714) , (*g_318))), l_1126)) , (*g_271))) , l_1062)));
                    if (l_1103)
                        continue;
                    return (****g_738);
                }
            }
            else
            { /* block id: 543 */
                uint16_t l_1130 = 8UL;
                uint32_t *l_1132 = &l_1127;
                uint32_t **l_1131[8][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1132,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1132,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
                int i, j;
                l_1127++;
                if (l_1130)
                    continue;
                (*g_1133) = l_1131[1][0];
            }
        }
        for (g_238 = 0; (g_238 <= 4); g_238 += 1)
        { /* block id: 551 */
            int32_t l_1148 = 0x401F9940L;
            int32_t **l_1149 = (void*)0;
            int32_t ***l_1151 = &g_874[2][1][0];
            int32_t ***l_1152 = &l_1150;
            int32_t *l_1153[3][1][3] = {{{&g_906.f0,&g_906.f0,&g_50}},{{&g_906.f0,&g_906.f0,&g_50}},{{&g_906.f0,&g_906.f0,&g_50}}};
            uint32_t **l_1158 = &g_1135;
            union U0 *l_1167[1];
            uint8_t l_1262 = 0xDFL;
            int64_t l_1317 = 0x3762AE6CF2EBCC9BLL;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1167[i] = &g_1168;
        }
        (*g_1357) = p_35;
        if ((safe_div_func_int16_t_s_s(0x8B87L, 0x943EL)))
        { /* block id: 663 */
            uint32_t l_1364[6][5] = {{0UL,0xDC30B3A8L,0UL,0xDC30B3A8L,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0UL,0xDC30B3A8L,0UL,0xDC30B3A8L,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0UL,0xDC30B3A8L,0UL,0xDC30B3A8L,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}};
            uint32_t *l_1376 = (void*)0;
            uint32_t l_1390 = 0xB43F48A2L;
            int32_t l_1395 = 1L;
            int32_t l_1396 = (-3L);
            int32_t l_1397 = 0x6F6244E2L;
            int32_t l_1399 = 0L;
            int16_t l_1400 = 0xCD62L;
            int32_t l_1401 = 0xFA658E70L;
            uint16_t l_1402[1][3];
            union U0 *l_1448[3];
            int32_t *l_1469 = &l_1401;
            int32_t *l_1470 = (void*)0;
            int32_t *l_1471 = &l_1104[1];
            int32_t *l_1472 = &g_1168.f0;
            int32_t *l_1473 = &l_1105[0][4][2];
            int32_t *l_1474 = &g_906.f0;
            int32_t *l_1475 = &l_1399;
            uint16_t l_1476 = 0x5F8CL;
            int8_t l_1485 = (-3L);
            uint8_t **l_1487[8][8] = {{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486}};
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1402[i][j] = 0x5B9EL;
            }
            for (i = 0; i < 3; i++)
                l_1448[i] = &g_1168;
            for (l_1217 = 27; (l_1217 >= 21); --l_1217)
            { /* block id: 666 */
                uint8_t l_1365[6][1][1] = {{{251UL}},{{0x37L}},{{251UL}},{{0x37L}},{{251UL}},{{0x37L}}};
                int8_t *l_1373[10][4] = {{&g_204,&g_714,&l_1253,&g_8},{&g_875[1],&g_8,&l_1253,&l_1253},{&g_875[1],&g_875[1],&l_1253,&g_204},{&g_204,&l_1253,&g_875[1],&g_714},{&g_8,&g_204,&l_1314,&g_875[1]},{&l_1253,&g_204,&l_1253,&g_714},{&g_204,&l_1253,&g_875[1],&g_204},{&g_204,&g_875[1],&g_8,&l_1253},{&g_8,&g_8,&g_8,&g_8},{&g_204,&g_714,&g_875[1],&l_1253}};
                int i, j, k;
                (*g_1374) = (safe_add_func_int8_t_s_s((g_204 &= (l_1364[5][4] | (l_1365[4][0][0] >= (safe_sub_func_uint8_t_u_u(((((**g_1225) |= (((***g_318) = (*g_279)) || ((-4L) <= (((*g_214) = (((***l_1304) > ((g_1370 , ((((((**g_475) , ((**g_649) < ((void*)0 != l_1371))) , l_1364[5][0]) & l_1364[4][4]) != (***l_1305)) , 1L)) , (***l_1304))) , 0x6350EEC9L)) , 0UL)))) == l_1365[3][0][0]) < 0UL), 0xFFL))))), l_1364[5][4]));
            }
            if ((***l_1305))
            { /* block id: 673 */
                uint8_t l_1387 = 0UL;
                int32_t *l_1391 = &l_1096;
                int32_t *l_1392 = &g_1168.f0;
                int32_t *l_1393 = &g_704.f0;
                int32_t *l_1394[4][3] = {{&l_1103,&l_1103,&l_1103},{&g_252[2][0],&g_252[2][0],&g_252[2][0]},{&l_1103,&l_1103,&l_1103},{&g_252[2][0],&g_252[2][0],&g_252[2][0]}};
                union U0 *l_1449[3][1][9] = {{{&g_906,&g_994,&g_906,&g_896[4][0][0],&g_704,&g_896[4][0][0],&g_906,&g_994,&g_906}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_906,&g_994,&g_906,&g_896[4][0][0],&g_704,&g_896[4][0][0],&g_906,&g_994,&g_906}}};
                int i, j, k;
                (**l_1150) = 0L;
                (*l_1391) &= (((!l_1364[5][4]) , (((0x59L <= ((void*)0 != l_1376)) == ((safe_sub_func_int8_t_s_s(((*p_35) , (((safe_sub_func_int64_t_s_s((safe_sub_func_uint8_t_u_u((((safe_div_func_int64_t_s_s((((*g_261) &= (~((*g_279) | ((~(l_1387 & (safe_div_func_uint8_t_u_u((***g_269), 0xBCL)))) ^ 8L)))) , (*g_261)), l_1387)) & (*g_214)) && 0xE67F6FC2L), l_1364[4][0])), 7L)) , g_714) > l_1390)), l_1364[5][4])) , 0L)) >= (**g_270))) <= (***l_1304));
                --l_1402[0][0];
                for (g_1210 = 0; (g_1210 <= 3); g_1210 += 1)
                { /* block id: 680 */
                    uint8_t l_1408 = 255UL;
                    int i;
                    for (g_143 = 0; (g_143 <= 3); g_143 += 1)
                    { /* block id: 683 */
                        int i;
                        (*l_1392) ^= (g_64[g_143] && (!((safe_div_func_int64_t_s_s(l_1408, (safe_add_func_uint64_t_u_u((safe_rshift_func_int16_t_s_s((((*g_476) = (safe_mod_func_int16_t_s_s(((+((***l_1304) & (safe_mul_func_int16_t_s_s(9L, (***g_282))))) || (*g_1226)), (safe_lshift_func_int8_t_s_u((safe_add_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(((0UL <= (safe_div_func_uint16_t_u_u((((safe_sub_func_uint8_t_u_u((**g_270), ((safe_rshift_func_uint16_t_u_u(0x7C1EL, l_1430)) != (***l_1304)))) || 0x93D7E55CL) & l_1408), (***l_1304)))) == 5UL), 1L)), l_1396)), 5))))) & l_1408), 8)), g_64[g_143])))) & 0x2363C56CL)));
                        (*g_1431) = p_35;
                        (**g_213) &= 1L;
                    }
                    (*l_1391) |= (((g_64[g_1210] <= (((safe_lshift_func_int32_t_s_u(g_64[g_1210], ((***l_1304) , ((l_1397 = (safe_mod_func_int32_t_s_s((((safe_div_func_uint16_t_u_u((((+((**g_475) = ((&g_758 == (void*)0) >= (-7L)))) <= (((l_1399 = (safe_rshift_func_uint16_t_u_s(((safe_sub_func_uint8_t_u_u(((((((l_1408 , ((safe_mul_func_uint64_t_u_u((((*p_35) , ((g_1447[6] , l_1364[4][1]) == g_64[g_1210])) > l_1401), 0x446C15BBE763C16ELL)) == 1L)) > 0xF1L) && 0x008C76E05D0312BALL) >= 0x5584A71354E6E2FFLL) <= (*g_214)) , g_64[g_1210]), (**l_1150))) > 1L), (*l_1392)))) && 0L) , g_64[g_1210])) <= 0x3D3547B8L), l_1390)) < 0xF0L) <= l_1408), 0x57057631L))) != (***l_1305))))) > (*l_1393)) <= g_64[g_1210])) , 0xCF16L) <= 0x6BF5L);
                    for (g_994.f4 = 0; (g_994.f4 <= 3); g_994.f4 += 1)
                    { /* block id: 695 */
                        l_1449[1][0][2] = l_1448[2];
                        (**l_1150) = (safe_sub_func_uint64_t_u_u((***l_1304), (**g_1225)));
                        (***l_1305) &= (-3L);
                    }
                }
            }
            else
            { /* block id: 701 */
                int16_t l_1466 = 6L;
                uint64_t l_1468 = 0x9788F3C1FEF2479FLL;
                if ((***l_1305))
                    break;
                l_1468 ^= ((+(***g_340)) <= (safe_sub_func_uint16_t_u_u(((***l_1304) = ((((safe_mod_func_uint16_t_u_u(65535UL, (safe_rshift_func_int32_t_s_u((!((safe_mod_func_uint64_t_u_u(((safe_mul_func_uint16_t_u_u(((**g_475) = (**l_1150)), (**g_319))) , ((***l_1304) <= (safe_div_func_uint8_t_u_u((***l_1304), (0xBDL && (((*g_261) |= l_1466) || ((***l_1305) && (**l_1150)))))))), 0x12DFD87F5C666EA8LL)) ^ (***l_1304))), 31)))) <= g_1467[3][4][1]) && 7L) , (**g_475))), l_1390)));
            }
            l_1476--;
            (*l_1473) = (safe_mul_func_int32_t_s_s((safe_mul_func_int16_t_s_s((safe_div_func_uint32_t_u_u((**l_1150), l_1485)), (((l_1488 = l_1486) == (void*)0) , 0xBDE4L))), (**g_671)));
        }
        else
        { /* block id: 711 */
            uint64_t l_1489 = 0xD860C007426B51C2LL;
            int32_t l_1496[4] = {0xA0BFE79FL,0xA0BFE79FL,0xA0BFE79FL,0xA0BFE79FL};
            union U0 ***l_1563 = &g_815;
            int32_t ****l_1570 = &l_1304;
            int i;
            if ((***l_1304))
            { /* block id: 712 */
                (*l_1150) = func_36(&l_1073, l_1071, l_1489, (***l_1304));
            }
            else
            { /* block id: 714 */
                int32_t l_1501 = 0xB3FDE348L;
                int8_t l_1527 = 0x95L;
                int32_t l_1529 = 0x3BDD0481L;
                int32_t l_1531 = (-7L);
                int32_t l_1533 = 0L;
                int32_t l_1534 = 5L;
                int32_t l_1535 = (-1L);
                int32_t l_1537 = 0xA0F1652AL;
                int32_t l_1539 = (-9L);
                int32_t l_1540 = 0x66B75491L;
                int32_t l_1541 = (-9L);
                int32_t l_1542 = (-6L);
                int32_t ****l_1571 = &g_1568;
                for (g_275 = (-2); (g_275 == 53); ++g_275)
                { /* block id: 717 */
                    int32_t l_1495 = 0xA1E5388CL;
                    int32_t l_1522 = (-2L);
                    int32_t l_1523 = 0xA5ED92D6L;
                    int32_t l_1524 = 0xCF88D28FL;
                    int32_t l_1525 = 0L;
                    int32_t l_1526 = 1L;
                    int32_t l_1528 = 0x7644AFABL;
                    int32_t l_1530 = 0x67AC2040L;
                    int32_t l_1536[6][8] = {{0xBFB5777BL,1L,0x8CCCFBB2L,0xBFB5777BL,0L,0xDCA281C5L,0L,0xBFB5777BL},{0xC127661AL,0L,0xC127661AL,0x333490E5L,0x6A3ABD92L,0xA81BC7A7L,0x333490E5L,1L},{0L,0xF6098DC6L,0xA81BC7A7L,0x38B8520CL,9L,(-2L),0x6A3ABD92L,0xF6098DC6L},{0L,1L,0L,0x6A3ABD92L,0x6A3ABD92L,0L,1L,0L},{0xC127661AL,0xBFB5777BL,0xF6098DC6L,(-2L),0L,7L,0xC127661AL,1L},{0xBFB5777BL,(-2L),0x333490E5L,1L,(-2L),7L,0x6A3ABD92L,7L}};
                    int i, j;
                    if (l_1489)
                    { /* block id: 718 */
                        int32_t *l_1492 = &g_704.f0;
                        int32_t l_1493 = 0x6B5FB037L;
                        int32_t *l_1494[6] = {&l_1105[1][2][1],&l_1105[1][2][1],&l_1096,&l_1105[1][2][1],&l_1105[1][2][1],&l_1096};
                        int i;
                        if ((***l_1304))
                            break;
                        g_1497++;
                        return (***g_739);
                    }
                    else
                    { /* block id: 722 */
                        int32_t *l_1502 = &g_50;
                        int32_t *l_1503 = &g_252[3][1];
                        int32_t *l_1504 = &g_906.f0;
                        int32_t *l_1505 = (void*)0;
                        int32_t *l_1506 = &l_1496[3];
                        int32_t *l_1507 = &g_252[6][1];
                        int32_t *l_1508 = &l_1096;
                        int32_t *l_1509 = &g_252[6][1];
                        int32_t *l_1510 = &g_896[3][3][0].f0;
                        int32_t *l_1511 = &l_1496[3];
                        int32_t *l_1512 = &g_519[0][2][1];
                        int32_t *l_1513 = &l_1104[6];
                        int32_t *l_1514 = &l_1103;
                        int32_t *l_1515 = &l_1105[0][4][2];
                        int32_t *l_1516 = &l_1287;
                        int32_t *l_1517 = &l_1496[1];
                        int32_t l_1518 = 0x222F2782L;
                        int32_t *l_1519 = &g_519[1][4][1];
                        int32_t *l_1520 = &l_1287;
                        int32_t *l_1521[9][7] = {{&l_1185,&g_690[0].f0,&l_1104[4],&g_690[0].f0,&l_1185,&g_238,&g_690[0].f0},{(void*)0,&g_238,&l_1104[4],&l_1104[3],&l_1496[2],&l_1104[4],&l_1518},{&l_1496[2],&l_1518,(void*)0,(void*)0,&l_1518,&l_1496[2],&g_238},{&l_1496[1],&l_1104[3],(void*)0,&l_1496[1],&l_1518,&l_1287,&l_1104[3]},{&g_704.f0,&l_1105[0][4][2],&l_1496[2],&l_1072,&l_1496[2],&l_1105[0][4][2],&g_704.f0},{&l_1105[0][4][2],&l_1104[3],(void*)0,&l_1496[2],&g_704.f0,&l_1105[0][4][2],&l_1496[2]},{&l_1496[1],&l_1518,&l_1287,&l_1104[3],&l_1104[3],&l_1287,&l_1518},{&l_1104[3],&g_238,(void*)0,&l_1072,&g_238,&l_1496[2],&l_1518},{&l_1104[4],&l_1104[3],&l_1496[2],&l_1104[4],&l_1518,&l_1104[4],&l_1496[2]}};
                        uint16_t l_1543 = 0xC7EDL;
                        int i, j;
                        ++l_1543;
                    }
                    l_1533 &= (((**g_654) && l_1501) >= ((((safe_mul_func_uint8_t_u_u((l_1535 ^= 0x3BL), (safe_div_func_uint16_t_u_u(((safe_lshift_func_int64_t_s_u(((safe_lshift_func_uint32_t_u_u(((safe_lshift_func_int16_t_s_s((***l_1304), 9)) <= 0UL), (safe_mul_func_uint8_t_u_u(l_1489, ((safe_sub_func_int32_t_s_s(((((***l_1305) >= (*g_1226)) != (***l_1305)) == l_1496[3]), l_1489)) >= 3L))))) , (*g_261)), 14)) > l_1489), 1UL)))) == (***l_1304)) && l_1496[2]) ^ (***l_1305)));
                    (***l_1304) |= l_1496[0];
                    if ((**g_1357))
                        break;
                }
                for (g_204 = 0; (g_204 > 23); g_204 = safe_add_func_uint16_t_u_u(g_204, 5))
                { /* block id: 732 */
                    int32_t *l_1564 = &g_690[0].f0;
                    (*g_214) &= (l_1562 != l_1563);
                    (*l_1150) = l_1564;
                    for (l_1073 = 0; (l_1073 >= (-30)); --l_1073)
                    { /* block id: 738 */
                        int32_t *****l_1569[10][2] = {{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567},{&g_1567,&g_1567}};
                        uint64_t **l_1572 = &l_1120[4][1][7];
                        int i, j;
                        (****l_1571) = ((l_1570 = g_1567) == l_1571);
                        l_1096 &= (****l_1570);
                        (**l_1371) = l_1572;
                    }
                }
            }
            l_1496[3] |= ((****l_1570) , (*g_214));
        }
    }
    return l_1071;
}


/* ------------------------------------------ */
/* 
 * reads : g_214 g_50 g_654 g_655 g_656 g_271 g_250 g_875 g_210 g_213 g_11 g_143 g_739 g_740 g_741 g_398 g_120 g_519 g_690.f0
 * writes: g_875 g_470 g_435 g_214 g_143
 */
static int32_t * const  func_36(int32_t * p_37, int32_t * p_38, uint8_t  p_39, uint32_t  p_40)
{ /* block id: 2 */
    uint64_t l_51 = 7UL;
    union U0 *l_895[6][3][2] = {{{(void*)0,&g_690[2]},{&g_690[2],&g_690[0]},{&g_690[0],&g_704}},{{(void*)0,&g_690[0]},{&g_704,&g_690[0]},{&g_704,&g_690[0]}},{{(void*)0,&g_704},{&g_690[0],&g_690[0]},{&g_690[2],&g_690[2]}},{{(void*)0,&g_690[2]},{&g_690[2],&g_690[0]},{&g_690[0],&g_704}},{{(void*)0,&g_690[0]},{&g_704,&g_690[0]},{&g_704,&g_690[0]}},{{(void*)0,&g_704},{&g_690[0],&g_690[0]},{&g_690[2],&g_690[2]}}};
    int32_t **l_918 = &g_214;
    int32_t l_924[10] = {0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL,0xF4E52CACL};
    int32_t l_936 = 8L;
    int64_t l_957[1][7] = {{(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)}};
    uint8_t l_958 = 0x0EL;
    int32_t * const l_996 = (void*)0;
    int32_t l_1011 = 0x19AB7BCDL;
    uint16_t l_1020 = 1UL;
    int8_t *l_1027 = &g_875[1];
    int8_t l_1028 = (-1L);
    int i, j, k;
    for (p_39 = 18; (p_39 <= 58); p_39 = safe_add_func_uint16_t_u_u(p_39, 3))
    { /* block id: 5 */
        union U0 *l_894 = &g_690[0];
        int32_t l_921 = 0x01E23F29L;
        int32_t l_926 = 0xDD3CF17EL;
        int32_t l_928 = 0x6B1E4166L;
        int32_t l_929[6][10][4] = {{{(-1L),(-1L),0L,0xF82CA302L},{0x1C932004L,0xD1DCAE94L,0xF2FEF936L,(-5L)},{0xA5E1F4BCL,0xA7DA28BEL,0xBE996E22L,0xB8D88147L},{0xBBE8AD58L,1L,0L,0x3DC8E66DL},{0x00AA58BDL,0x0F1AF56DL,0xDA6B4A7BL,(-6L)},{0xC251C789L,1L,0xC6AAFE27L,0x7476E42BL},{0xDA6B4A7BL,(-7L),2L,0xC20DEBE8L},{(-1L),0L,6L,0x51302A6DL},{0x51302A6DL,(-1L),0L,0x1C932004L},{0xCD63024BL,6L,(-1L),0xF2FEF936L}},{{0x460D119EL,0x51302A6DL,0x00AA58BDL,0x98020321L},{0x7476E42BL,0x6F958864L,0x51302A6DL,0x6F958864L},{(-6L),0xC251C789L,(-5L),0xCD63024BL},{(-1L),(-1L),0xA5E1F4BCL,(-1L)},{(-6L),(-6L),0xF82CA302L,(-1L)},{(-6L),0xBE996E22L,0xF2FEF936L,(-1L)},{(-2L),0x7476E42BL,0xB5BA0434L,(-4L)},{0xA7DA28BEL,0xD1DCAE94L,(-1L),0xC6AAFE27L},{(-1L),0x6434592AL,0xA5E1F4BCL,0xCD63024BL},{0x085C697EL,0xAF029DBDL,0xB8D88147L,7L}},{{0xE5D1F55EL,0x51302A6DL,0x660B17CEL,0x3DC8E66DL},{(-1L),(-6L),(-6L),0xA5E1F4BCL},{0x98020321L,0L,6L,0xF2FEF936L},{(-8L),0xBE996E22L,0xD27DB716L,(-6L)},{8L,0xB8D88147L,(-8L),(-1L)},{0xA5E1F4BCL,0xF82CA302L,(-4L),0xD1DCAE94L},{(-1L),0xF1638E71L,(-8L),(-8L)},{0xF2FEF936L,0xF2FEF936L,0x6F958864L,0x098F1183L},{(-1L),0xE5D1F55EL,(-7L),0L},{0x7476E42BL,0L,0x1C932004L,(-7L)}},{{0x51302A6DL,0L,0xCD63024BL,0L},{0L,0xE5D1F55EL,(-1L),0x098F1183L},{0xD1DCAE94L,0xF2FEF936L,(-5L),(-8L)},{0xB5BA0434L,0xF1638E71L,0L,0xD1DCAE94L},{0x1C932004L,0xF82CA302L,0xA7DA28BEL,(-1L)},{0xEF31B142L,0xB8D88147L,0L,(-6L)},{0x460D119EL,0xBE996E22L,8L,0xF2FEF936L},{1L,0L,1L,0xA5E1F4BCL},{0x00AA58BDL,(-6L),(-1L),0x3DC8E66DL},{2L,0x51302A6DL,0xF82CA302L,7L}},{{0xF1638E71L,0xAF029DBDL,0x6434592AL,0xCD63024BL},{9L,0x6434592AL,0xE5D1F55EL,0xC6AAFE27L},{7L,0xD1DCAE94L,7L,(-4L)},{0x6F958864L,0x7476E42BL,0x085C697EL,(-1L)},{0xAF029DBDL,(-8L),1L,0x7476E42BL},{(-6L),7L,1L,0xB8D88147L},{0xAF029DBDL,0x98020321L,0x085C697EL,0xE5D1F55EL},{0x6F958864L,8L,7L,0xF4BFB243L},{7L,0xF4BFB243L,0xE5D1F55EL,0xC20DEBE8L},{9L,(-1L),0x6434592AL,0x6F958864L}},{{0xF1638E71L,(-6L),0xF82CA302L,(-1L)},{2L,(-2L),(-1L),(-1L)},{0x00AA58BDL,(-7L),1L,0xDA6B4A7BL},{1L,9L,8L,(-1L)},{0x460D119EL,1L,0L,0L},{0xEF31B142L,0xC6AAFE27L,0xA7DA28BEL,2L},{0x1C932004L,(-1L),0L,0x5A078C7FL},{0xB5BA0434L,(-5L),(-5L),0xB5BA0434L},{0xD1DCAE94L,0L,(-1L),1L},{0L,0xC251C789L,0xCD63024BL,9L}}};
        int16_t l_939 = 1L;
        int8_t l_943[1];
        int32_t l_971[1];
        uint32_t *l_1004 = &g_210;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_943[i] = (-1L);
        for (i = 0; i < 1; i++)
            l_971[i] = 0x49A2C47CL;
    }
    l_924[5] &= ((**l_918) & ((((-1L) <= ((((*l_1027) ^= (safe_unary_minus_func_int8_t_s((safe_div_func_int16_t_s_s(((**g_654) & (**l_918)), ((p_39 != ((*g_271) != (((safe_mul_func_uint64_t_u_u((**l_918), (~((-9L) < p_40)))) > p_39) , (**l_918)))) && (*g_271))))))) > g_210) , 0UL)) & l_1028) > (**l_918)));
    for (l_51 = 1; (l_51 <= 9); l_51 += 1)
    { /* block id: 485 */
        int8_t l_1038 = 0L;
        int32_t l_1039 = 0x8BB11455L;
        int32_t l_1040 = 0x41DAB63AL;
        if ((**g_213))
            break;
        for (g_470 = 1; (g_470 <= 4); g_470 += 1)
        { /* block id: 489 */
            const int32_t * const l_1029 = &g_704.f0;
            const int32_t **l_1030 = &g_435;
            int32_t l_1031[7][10][3] = {{{1L,1L,0xF6063D24L},{1L,3L,0xC6FB75CFL},{1L,(-6L),0xFBDB92B6L},{0x2CE9BFB4L,1L,(-7L)},{7L,(-4L),0x66AC3F8CL},{0x77E5DA0AL,1L,(-2L)},{1L,0x22CA08C6L,0x4D3A10C6L},{0x8D4CB17AL,0x22CA08C6L,0xC6FB75CFL},{0xF6063D24L,1L,(-6L)},{(-4L),(-4L),1L}},{{0x6FFE5024L,0L,0xFBDB92B6L},{0x66AC3F8CL,0L,0x4D3A10C6L},{0x77E5DA0AL,7L,0x8D4CB17AL},{1L,0x66AC3F8CL,0x4D3A10C6L},{(-4L),1L,0xFBDB92B6L},{(-7L),1L,1L},{(-2L),0L,(-6L)},{0x6FFE5024L,4L,0xC6FB75CFL},{1L,(-2L),0x4D3A10C6L},{1L,1L,(-2L)}},{{0x6FFE5024L,0x66AC3F8CL,0x66AC3F8CL},{(-2L),0x22CA08C6L,0x28E00AEFL},{(-7L),7L,0x7B568976L},{(-4L),(-2L),(-6L)},{1L,0xF6063D24L,0xFBDB92B6L},{0x77E5DA0AL,(-2L),0x66AC3F8CL},{0x66AC3F8CL,7L,(-6L)},{0x6FFE5024L,0x22CA08C6L,0x77E5DA0AL},{(-4L),0x66AC3F8CL,0x28E00AEFL},{0xF6063D24L,1L,1L}},{{0x8D4CB17AL,(-2L),1L},{1L,4L,0x28E00AEFL},{0x77E5DA0AL,0L,0x77E5DA0AL},{1L,1L,(-6L)},{1L,1L,0x66AC3F8CL},{0x8D4CB17AL,0x66AC3F8CL,0xFBDB92B6L},{0L,7L,(-6L)},{0x8D4CB17AL,0L,0x7B568976L},{1L,0L,0x28E00AEFL},{1L,(-4L),0x66AC3F8CL}},{{0x77E5DA0AL,1L,(-2L)},{1L,0x22CA08C6L,0x4D3A10C6L},{0x8D4CB17AL,0x22CA08C6L,0xC6FB75CFL},{0xF6063D24L,1L,(-6L)},{(-4L),(-4L),1L},{0x6FFE5024L,0L,0xFBDB92B6L},{0x66AC3F8CL,0L,0x4D3A10C6L},{0x77E5DA0AL,7L,0x8D4CB17AL},{1L,0x66AC3F8CL,0x4D3A10C6L},{(-4L),1L,0xFBDB92B6L}},{{(-7L),1L,1L},{(-2L),0L,(-6L)},{0x6FFE5024L,4L,0xC6FB75CFL},{1L,(-2L),0x4D3A10C6L},{1L,1L,(-2L)},{0x6FFE5024L,0x66AC3F8CL,0x66AC3F8CL},{(-2L),0x22CA08C6L,0x28E00AEFL},{(-7L),7L,0x7B568976L},{(-4L),(-2L),(-6L)},{1L,0xF6063D24L,0xFBDB92B6L}},{{0x77E5DA0AL,(-2L),0x66AC3F8CL},{0x66AC3F8CL,7L,(-6L)},{0x6FFE5024L,0x22CA08C6L,0x77E5DA0AL},{(-4L),0x66AC3F8CL,0x28E00AEFL},{0xF6063D24L,1L,1L},{0x8D4CB17AL,(-2L),1L},{1L,4L,0x28E00AEFL},{0x77E5DA0AL,0L,0x77E5DA0AL},{0L,0x77E5DA0AL,0x2CE9BFB4L},{0x66AC3F8CL,0L,(-7L)}}};
            int32_t *l_1032 = &l_1011;
            int32_t *l_1033 = &l_1011;
            int32_t *l_1034 = &g_704.f0;
            int32_t *l_1035 = &g_704.f0;
            int32_t *l_1036 = &g_519[1][5][2];
            int32_t *l_1037[2];
            uint8_t l_1041 = 0x1AL;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1037[i] = &l_924[4];
            (*l_1030) = l_1029;
            --l_1041;
            l_1040 = p_40;
            (*l_918) = p_37;
            for (g_143 = 0; (g_143 <= 4); g_143 += 1)
            { /* block id: 496 */
                return l_1036;
            }
        }
    }
    return (***g_739);
}


/* ------------------------------------------ */
/* 
 * reads : g_50
 * writes:
 */
static const int32_t  func_54(uint16_t  p_55, int32_t * p_56)
{ /* block id: 12 */
    int32_t l_63 = 0xE195D318L;
    int32_t l_83[3][8] = {{0L,0xFB14099FL,0xFB14099FL,0L,0L,0L,0xFB14099FL,0xFB14099FL},{0xFB14099FL,0L,0L,0L,0L,0xFB14099FL,0L,0L},{0L,0L,0L,0xFB14099FL,0xFB14099FL,0L,0L,0L}};
    int32_t *l_130[3][6][6];
    int16_t l_197 = (-1L);
    uint32_t l_205 = 0UL;
    int32_t l_251 = 9L;
    int32_t l_292 = 4L;
    int64_t l_294[8][10][2] = {{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}},{{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)},{(-1L),0x64C185FE07A8C25ALL},{1L,0x74D873EA0DD71AC3LL},{0x64C185FE07A8C25ALL,0x74D873EA0DD71AC3LL},{1L,0x64C185FE07A8C25ALL},{(-1L),(-1L)}}};
    int16_t **l_317 = &g_279;
    int16_t ***l_316 = &l_317;
    uint8_t *l_418[4][1] = {{&g_250},{&g_250},{&g_250},{&g_250}};
    uint8_t **l_417 = &l_418[1][0];
    int32_t l_424 = 6L;
    uint32_t l_427 = 18446744073709551615UL;
    uint64_t *l_455 = (void*)0;
    int32_t **l_507 = &l_130[0][2][4];
    int32_t ***l_506 = &l_507;
    int32_t l_571 = 0x13D668B3L;
    uint32_t l_572 = 0UL;
    union U0 * const l_596[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_647[4][1] = {{(-1L)},{1L},{(-1L)},{1L}};
    int8_t *l_653 = &g_204;
    int8_t **l_652 = &l_653;
    union U0 * const l_838 = (void*)0;
    int32_t l_845 = (-1L);
    int64_t l_848 = 7L;
    uint32_t l_850 = 0xA3A4E35AL;
    int32_t l_864 = 1L;
    int32_t **l_871 = &g_214;
    int32_t ***l_870 = &l_871;
    int32_t **l_873 = &g_214;
    int32_t ***l_872[10] = {&l_873,&l_873,&l_873,&l_873,&l_873,&l_873,&l_873,&l_873,&l_873,&l_873};
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
                l_130[i][j][k] = &g_120;
        }
    }
    return (*p_56);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_50, "g_50", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_58[i][j][k], "g_58[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_64[i], "g_64[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_72.f0, "g_72.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_88[i][j][k], "g_88[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_120, "g_120", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    transparent_crc(g_150, "g_150", print_hash_value);
    transparent_crc(g_155, "g_155", print_hash_value);
    transparent_crc(g_172, "g_172", print_hash_value);
    transparent_crc(g_204, "g_204", print_hash_value);
    transparent_crc(g_210, "g_210", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    transparent_crc(g_250, "g_250", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_252[i][j], "g_252[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_275, "g_275", print_hash_value);
    transparent_crc(g_280, "g_280", print_hash_value);
    transparent_crc(g_402, "g_402", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_441[i], "g_441[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_465, "g_465", print_hash_value);
    transparent_crc(g_470, "g_470", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_519[i][j][k], "g_519[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_588, "g_588", print_hash_value);
    transparent_crc(g_651, "g_651", print_hash_value);
    transparent_crc(g_656, "g_656", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_690[i].f0, "g_690[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_704.f0, "g_704.f0", print_hash_value);
    transparent_crc(g_714, "g_714", print_hash_value);
    transparent_crc(g_725.f0, "g_725.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_813[i].f0, "g_813[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_875[i], "g_875[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_896[i][j][k].f0, "g_896[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_906.f0, "g_906.f0", print_hash_value);
    transparent_crc(g_985.f0, "g_985.f0", print_hash_value);
    transparent_crc(g_994.f0, "g_994.f0", print_hash_value);
    transparent_crc(g_997.f0, "g_997.f0", print_hash_value);
    transparent_crc(g_1107, "g_1107", print_hash_value);
    transparent_crc(g_1115, "g_1115", print_hash_value);
    transparent_crc(g_1136, "g_1136", print_hash_value);
    transparent_crc(g_1168.f0, "g_1168.f0", print_hash_value);
    transparent_crc(g_1187, "g_1187", print_hash_value);
    transparent_crc(g_1210, "g_1210", print_hash_value);
    transparent_crc(g_1303.f0, "g_1303.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1349[i][j][k], "g_1349[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1370.f0, "g_1370.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1447[i].f0, "g_1447[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1467[i][j][k], "g_1467[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1497, "g_1497", print_hash_value);
    transparent_crc(g_1574, "g_1574", print_hash_value);
    transparent_crc(g_1653.f0, "g_1653.f0", print_hash_value);
    transparent_crc(g_1679.f0, "g_1679.f0", print_hash_value);
    transparent_crc(g_1808.f0, "g_1808.f0", print_hash_value);
    transparent_crc(g_1892.f0, "g_1892.f0", print_hash_value);
    transparent_crc(g_1926, "g_1926", print_hash_value);
    transparent_crc(g_2044, "g_2044", print_hash_value);
    transparent_crc(g_2064, "g_2064", print_hash_value);
    transparent_crc(g_2096.f0, "g_2096.f0", print_hash_value);
    transparent_crc(g_2104.f0, "g_2104.f0", print_hash_value);
    transparent_crc(g_2157, "g_2157", print_hash_value);
    transparent_crc(g_2209, "g_2209", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_2235[i][j], "g_2235[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2291, "g_2291", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2348[i], "g_2348[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2375[i].f0, "g_2375[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2420.f0, "g_2420.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_2499[i][j], "g_2499[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2503.f0, "g_2503.f0", print_hash_value);
    transparent_crc(g_2512.f0, "g_2512.f0", print_hash_value);
    transparent_crc(g_2573.f0, "g_2573.f0", print_hash_value);
    transparent_crc(g_2605, "g_2605", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2611[i], "g_2611[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2819, "g_2819", print_hash_value);
    transparent_crc(g_2898.f0, "g_2898.f0", print_hash_value);
    transparent_crc(g_2915, "g_2915", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2918[i], "g_2918[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2959.f0, "g_2959.f0", print_hash_value);
    transparent_crc(g_2992, "g_2992", print_hash_value);
    transparent_crc(g_3021, "g_3021", print_hash_value);
    transparent_crc(g_3022, "g_3022", print_hash_value);
    transparent_crc(g_3069.f0, "g_3069.f0", print_hash_value);
    transparent_crc(g_3080.f0, "g_3080.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 852
XXX total union variables: 24

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 49
breakdown:
   indirect level: 0, occurrence: 24
   indirect level: 1, occurrence: 16
   indirect level: 2, occurrence: 5
   indirect level: 3, occurrence: 4
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 63
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 26
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 37
breakdown:
   depth: 1, occurrence: 163
   depth: 2, occurrence: 46
   depth: 3, occurrence: 4
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
   depth: 7, occurrence: 3
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 3
   depth: 21, occurrence: 3
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1

XXX total number of pointers: 729

XXX times a variable address is taken: 1500
XXX times a pointer is dereferenced on RHS: 600
breakdown:
   depth: 1, occurrence: 325
   depth: 2, occurrence: 149
   depth: 3, occurrence: 94
   depth: 4, occurrence: 23
   depth: 5, occurrence: 9
XXX times a pointer is dereferenced on LHS: 439
breakdown:
   depth: 1, occurrence: 285
   depth: 2, occurrence: 82
   depth: 3, occurrence: 57
   depth: 4, occurrence: 9
   depth: 5, occurrence: 6
XXX times a pointer is compared with null: 57
XXX times a pointer is compared with address of another variable: 17
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 13164

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1737
   level: 2, occurrence: 836
   level: 3, occurrence: 545
   level: 4, occurrence: 173
   level: 5, occurrence: 63
XXX number of pointers point to pointers: 348
XXX number of pointers point to scalars: 361
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28
XXX average alias set size: 1.45

XXX times a non-volatile is read: 2943
XXX times a non-volatile is write: 1470
XXX times a volatile is read: 223
XXX    times read thru a pointer: 86
XXX times a volatile is write: 100
XXX    times written thru a pointer: 57
XXX times a volatile is available for access: 2.54e+03
XXX percentage of non-volatile access: 93.2

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 165
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 20
   depth: 2, occurrence: 30
   depth: 3, occurrence: 39
   depth: 4, occurrence: 24
   depth: 5, occurrence: 27

XXX percentage a fresh-made variable is used: 16.8
XXX percentage an existing variable is used: 83.2
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
XXX total OOB instances added: 0
********************* end of statistics **********************/


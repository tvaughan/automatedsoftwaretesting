#!/usr/bin/env bash
set -e
DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PATH_TO_DIR=$DIR"/reduced_test_cases"
PATH_TO_PYTHON=$DIR/..
FOLDER=test_case_1
TESTCASE=vect
FLAGS=O3
FILE_NAME=random17


## First compile using gcc

gcc -$FLAGS -no-pie -mavx -ftree-vectorize -I/usr/include/csmith/ -o main_gcc $FILE_NAME-$TESTCASE-$FLAGS.c&&\
clang -$FLAGS -no-pie -mavx -ftree-vectorize -I/usr/include/csmith/ -o main_clang $FILE_NAME-$TESTCASE-$FLAGS.c &&\
python3.10 $PATH_TO_PYTHON/main.py main_gcc > checker1.txt &&\
python3.10 $PATH_TO_PYTHON/main.py main_clang  > checker2.txt &&\
python3.10 $PATH_TO_PYTHON/creduce/reducer.py $TESTCASE > interesting.txt &&\
grep "$TESTCASE interesting" interesting.txt 


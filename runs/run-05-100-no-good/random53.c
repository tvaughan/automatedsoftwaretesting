/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      56466936
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   signed f0 : 10;
   volatile int64_t  f1;
   int64_t  f2;
   const volatile uint8_t  f3;
   const float  f4;
   int32_t  f5;
   unsigned f6 : 7;
   unsigned f7 : 9;
   uint16_t  f8;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static uint8_t g_2 = 0x97L;
static volatile int32_t g_31 = 0xF7B1466AL;/* VOLATILE GLOBAL g_31 */
static volatile int32_t * const g_30 = &g_31;
static int32_t g_34 = (-1L);
static uint8_t g_36 = 3UL;
static uint32_t g_85 = 18446744073709551615UL;
static uint64_t g_88[4][9][1] = {{{0xEEAF792179B1000BLL},{0x579AB67338D581CALL},{0x3351F4A1AB70486DLL},{0UL},{0x3351F4A1AB70486DLL},{0x579AB67338D581CALL},{0xEEAF792179B1000BLL},{0x579AB67338D581CALL},{0x3351F4A1AB70486DLL}},{{0UL},{0x3351F4A1AB70486DLL},{0x579AB67338D581CALL},{0xEEAF792179B1000BLL},{0x579AB67338D581CALL},{0x3351F4A1AB70486DLL},{0UL},{0x3351F4A1AB70486DLL},{0x579AB67338D581CALL}},{{0xEEAF792179B1000BLL},{0x579AB67338D581CALL},{0x3351F4A1AB70486DLL},{0UL},{0x3351F4A1AB70486DLL},{0x579AB67338D581CALL},{0xEEAF792179B1000BLL},{0x579AB67338D581CALL},{0x3351F4A1AB70486DLL}},{{0UL},{0x3351F4A1AB70486DLL},{0x579AB67338D581CALL},{0xEEAF792179B1000BLL},{0x579AB67338D581CALL},{0x3351F4A1AB70486DLL},{0UL},{0x3351F4A1AB70486DLL},{0x579AB67338D581CALL}}};
static uint16_t g_102[8][4] = {{1UL,0xA800L,0x28F7L,0xA800L},{0xA800L,65528UL,0x28F7L,0x28F7L},{1UL,1UL,0x0D3BL,65528UL},{0x28F7L,1UL,0x28F7L,0x0D3BL},{0x28F7L,0x0D3BL,0x0D3BL,0x28F7L},{0xA800L,0x0D3BL,65528UL,0x0D3BL},{0x0D3BL,1UL,65528UL,65528UL},{0xA800L,0xA800L,0x0D3BL,65528UL}};
static uint32_t g_104 = 0xD2826019L;
static uint64_t g_105 = 0xC330632420D6CD47LL;
static int8_t g_107[9] = {0xABL,0xABL,0xABL,0xABL,0xABL,0xABL,0xABL,0xABL,0xABL};
static volatile struct S0 g_140 = {0,-8L,0L,0UL,-0x1.5p-1,1L,2,12,0UL};/* VOLATILE GLOBAL g_140 */
static volatile struct S0 *g_139 = &g_140;
static int32_t g_156 = 0x7106077EL;
static uint32_t g_157 = 0xF2D310D2L;
static float g_170 = (-0x1.Cp+1);
static struct S0 g_174 = {-1,0x0954A6F35BB462C2LL,0x114C857B7439DAF5LL,255UL,0x6.E0B1FDp-58,4L,4,3,0x0D59L};/* VOLATILE GLOBAL g_174 */
static int32_t *g_183 = &g_156;
static int32_t * volatile *g_182 = &g_183;
static int32_t * volatile ** volatile g_181 = &g_182;/* VOLATILE GLOBAL g_181 */
static int32_t * volatile ** volatile *g_180 = &g_181;
static int16_t g_209 = 1L;
static int8_t g_210[8] = {0x07L,(-4L),0x07L,(-4L),0x07L,(-4L),0x07L,(-4L)};
static uint32_t g_211 = 0x2D32AF4CL;
static int16_t g_235[2] = {7L,7L};
static int32_t g_236 = 0x4A69D0D3L;
static uint32_t *g_240 = &g_104;
static uint32_t **g_239 = &g_240;
static float g_291 = 0xF.A6EBB1p+99;
static int64_t g_292 = 0x17DDE3D06CCA6240LL;
static struct S0 g_297[8][4][1] = {{{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{30,0xF465C1140D0F7886LL,0x37CD8EF88622F486LL,9UL,0x1.7p+1,0x5EB80928L,0,16,9UL}}},{{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}}},{{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{30,0xF465C1140D0F7886LL,0x37CD8EF88622F486LL,9UL,0x1.7p+1,0x5EB80928L,0,16,9UL}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}}},{{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}}},{{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{30,0xF465C1140D0F7886LL,0x37CD8EF88622F486LL,9UL,0x1.7p+1,0x5EB80928L,0,16,9UL}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}}},{{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}}},{{{30,0xF465C1140D0F7886LL,0x37CD8EF88622F486LL,9UL,0x1.7p+1,0x5EB80928L,0,16,9UL}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}}},{{{3,0xDD5C52C778925770LL,-4L,0x72L,0xB.66DF69p-48,0xDDC878D3L,7,8,6UL}},{{-23,0L,-5L,255UL,0x1.D59750p-30,1L,6,8,0x88E9L}},{{-7,1L,-1L,0xC2L,0x0.8p-1,0xF5F39972L,3,14,65528UL}},{{30,0xF465C1140D0F7886LL,0x37CD8EF88622F486LL,9UL,0x1.7p+1,0x5EB80928L,0,16,9UL}}}};
static struct S0 *g_299 = (void*)0;
static uint8_t *g_316 = &g_36;
static uint8_t **g_315[9] = {&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316};
static const int32_t *g_461 = &g_156;
static const int32_t **g_460 = &g_461;
static struct S0 g_467 = {-25,0L,1L,255UL,0x0.Bp-1,0x74DAA309L,2,14,0xCAE3L};/* VOLATILE GLOBAL g_467 */
static uint16_t g_481 = 0x3A01L;
static float * volatile g_528 = &g_291;/* VOLATILE GLOBAL g_528 */
static float * volatile *g_527 = &g_528;
static const volatile int8_t g_575 = (-1L);/* VOLATILE GLOBAL g_575 */
static const volatile int8_t * volatile g_574 = &g_575;/* VOLATILE GLOBAL g_574 */
static const volatile int8_t * volatile *g_573[5] = {&g_574,&g_574,&g_574,&g_574,&g_574};
static struct S0 * volatile *g_607 = &g_299;
static struct S0 * volatile **g_606 = &g_607;
static int16_t g_616 = (-7L);
static uint32_t g_619 = 0x71C1B63BL;
static uint16_t g_654 = 65528UL;
static uint32_t * volatile * volatile * volatile *g_685 = (void*)0;
static uint32_t * volatile * volatile g_688 = &g_240;/* VOLATILE GLOBAL g_688 */
static int32_t *g_714 = &g_297[2][2][0].f5;
static float g_725 = 0x0.Dp-1;
static int32_t g_727 = 0x18016193L;
static uint16_t g_728 = 0x0251L;
static uint8_t g_797 = 3UL;
static int32_t g_823 = 2L;
static uint64_t g_824 = 18446744073709551607UL;
static float g_834 = 0xA.682D99p+17;
static float *g_862 = (void*)0;
static float **g_861 = &g_862;
static float ***g_860[9][3] = {{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861},{&g_861,&g_861,&g_861}};
static int8_t g_881 = 0x82L;
static int32_t g_883 = 0xBF34334CL;
static uint8_t g_884 = 1UL;
static int32_t **g_897 = (void*)0;
static int32_t ***g_896 = &g_897;
static int64_t g_920 = 0x4176977E19FBF3DCLL;
static const int32_t ** const * const g_953 = &g_460;
static const int32_t ** const * const *g_952 = &g_953;
static const int32_t ** const * const **g_951[7] = {&g_952,&g_952,&g_952,&g_952,&g_952,&g_952,&g_952};
static uint32_t g_963[2] = {0x89DCB8E9L,0x89DCB8E9L};
static uint16_t g_999 = 0x8F86L;
static struct S0 g_1009[6] = {{27,0x3B4FAAA6AC5AE78CLL,0x7919CAF1DF02F19DLL,0UL,0x0.Ap+1,0x6D37B155L,9,12,3UL},{27,0x3B4FAAA6AC5AE78CLL,0x7919CAF1DF02F19DLL,0UL,0x0.Ap+1,0x6D37B155L,9,12,3UL},{27,0x3B4FAAA6AC5AE78CLL,0x7919CAF1DF02F19DLL,0UL,0x0.Ap+1,0x6D37B155L,9,12,3UL},{27,0x3B4FAAA6AC5AE78CLL,0x7919CAF1DF02F19DLL,0UL,0x0.Ap+1,0x6D37B155L,9,12,3UL},{27,0x3B4FAAA6AC5AE78CLL,0x7919CAF1DF02F19DLL,0UL,0x0.Ap+1,0x6D37B155L,9,12,3UL},{27,0x3B4FAAA6AC5AE78CLL,0x7919CAF1DF02F19DLL,0UL,0x0.Ap+1,0x6D37B155L,9,12,3UL}};
static int32_t g_1180 = 0x33D904C9L;
static int64_t g_1245 = 1L;
static int32_t g_1246 = 0x5F11542BL;
static float g_1247 = 0xF.91E9C9p+79;
static int8_t g_1248 = 0x7FL;
static uint16_t g_1269 = 65535UL;
static const uint16_t g_1324 = 65535UL;
static float * const *g_1362 = &g_862;
static float * const **g_1361 = &g_1362;
static float * const ** const *g_1360 = &g_1361;
static float g_1370 = 0x8.12FD89p-24;
static int16_t g_1476[6] = {0L,0L,0L,0L,0L,0L};
static float g_1478 = 0x6.BC8776p+43;
static uint8_t g_1479 = 249UL;
static uint64_t g_1483[2][6][2] = {{{2UL,0x31CB85A3395538FALL},{18446744073709551615UL,0x31CB85A3395538FALL},{2UL,5UL},{2UL,0x31CB85A3395538FALL},{18446744073709551615UL,0x31CB85A3395538FALL},{2UL,5UL}},{{2UL,0x31CB85A3395538FALL},{18446744073709551615UL,0x31CB85A3395538FALL},{2UL,5UL},{2UL,0x31CB85A3395538FALL},{18446744073709551615UL,0x31CB85A3395538FALL},{2UL,5UL}}};
static struct S0 **g_1496 = &g_299;
static struct S0 **g_1497 = &g_299;
static struct S0 g_1558 = {31,0xA38F20EA4D5A5A6DLL,-1L,255UL,0x8.153586p-28,0L,5,7,0x1363L};/* VOLATILE GLOBAL g_1558 */
static const float ***g_1599 = (void*)0;
static uint32_t ***g_1631 = &g_239;
static uint32_t **** volatile g_1630 = &g_1631;/* VOLATILE GLOBAL g_1630 */
static const int32_t g_1660[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static const int32_t g_1662 = 0x9EFBD1A8L;
static const int32_t *g_1661 = &g_1662;
static const uint8_t * const *g_1664 = (void*)0;
static const uint8_t * const **g_1663 = &g_1664;
static uint8_t ***g_1666 = &g_315[4];
static volatile struct S0 g_1670 = {7,0x55AC1FFDDF027FB7LL,-9L,254UL,-0x3.2p-1,6L,7,8,0x145DL};/* VOLATILE GLOBAL g_1670 */
static const int8_t g_1674 = 0x96L;
static const uint8_t g_1765 = 0xBFL;
static struct S0 g_1785[8] = {{12,0xA5753EF49F44C1DELL,0x96112DA759F48A8BLL,0UL,0x4.182CABp+77,0L,1,13,65526UL},{12,0xA5753EF49F44C1DELL,0x96112DA759F48A8BLL,0UL,0x4.182CABp+77,0L,1,13,65526UL},{-30,0x9C40F386231DFE84LL,0x6016776914EBD365LL,0xB8L,0x9.376A7Ep-54,0xCF442D94L,3,18,65535UL},{12,0xA5753EF49F44C1DELL,0x96112DA759F48A8BLL,0UL,0x4.182CABp+77,0L,1,13,65526UL},{12,0xA5753EF49F44C1DELL,0x96112DA759F48A8BLL,0UL,0x4.182CABp+77,0L,1,13,65526UL},{-30,0x9C40F386231DFE84LL,0x6016776914EBD365LL,0xB8L,0x9.376A7Ep-54,0xCF442D94L,3,18,65535UL},{12,0xA5753EF49F44C1DELL,0x96112DA759F48A8BLL,0UL,0x4.182CABp+77,0L,1,13,65526UL},{12,0xA5753EF49F44C1DELL,0x96112DA759F48A8BLL,0UL,0x4.182CABp+77,0L,1,13,65526UL}};
static volatile int32_t g_1823 = 0x01119E6DL;/* VOLATILE GLOBAL g_1823 */
static uint32_t g_1825 = 0UL;
static volatile float g_1829 = 0x1.D63E4Ap+78;/* VOLATILE GLOBAL g_1829 */
static uint8_t g_1830 = 255UL;
static const struct S0 g_1854 = {14,0x5F6C05454257BA09LL,0x7AC565363327C817LL,1UL,0x1.D04AC6p+30,0x7C5C4CACL,7,12,1UL};/* VOLATILE GLOBAL g_1854 */
static struct S0 g_1904 = {-6,0xDA1447C1D364C7C0LL,0xFDCED506E4904BCALL,9UL,-0x5.3p-1,0x96C45432L,7,16,0x913EL};/* VOLATILE GLOBAL g_1904 */
static int8_t g_1922 = 0x57L;
static volatile uint32_t g_1952 = 0x1F3D69ACL;/* VOLATILE GLOBAL g_1952 */
static int8_t *g_2079[10] = {&g_210[6],&g_881,&g_210[6],&g_210[6],&g_881,&g_210[6],&g_210[6],&g_881,&g_210[6],&g_210[6]};
static struct S0 g_2134 = {29,-5L,1L,255UL,0x0.4EF895p+42,-1L,9,6,0xA0F0L};/* VOLATILE GLOBAL g_2134 */
static int16_t * volatile g_2137 = (void*)0;/* VOLATILE GLOBAL g_2137 */
static int16_t * volatile *g_2136 = &g_2137;
static int16_t * volatile **g_2135 = &g_2136;
static int8_t ** const g_2186[2][10][1] = {{{&g_2079[9]},{&g_2079[3]},{(void*)0},{&g_2079[3]},{&g_2079[9]},{&g_2079[3]},{(void*)0},{&g_2079[3]},{&g_2079[9]},{&g_2079[3]}},{{(void*)0},{&g_2079[3]},{&g_2079[9]},{&g_2079[3]},{(void*)0},{&g_2079[3]},{&g_2079[9]},{&g_2079[3]},{(void*)0},{&g_2079[3]}}};
static int8_t ** const *g_2185 = &g_2186[1][7][0];
static int8_t * const * volatile g_2278 = &g_2079[9];/* VOLATILE GLOBAL g_2278 */
static int8_t * const * volatile *g_2277[8][3][10] = {{{(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,&g_2278,&g_2278,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,(void*)0,&g_2278,&g_2278}},{{(void*)0,&g_2278,(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,&g_2278}},{{(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,(void*)0,&g_2278}},{{(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,&g_2278,&g_2278,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,(void*)0,&g_2278,&g_2278}},{{(void*)0,&g_2278,(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,&g_2278}},{{(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,&g_2278,(void*)0}},{{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0,(void*)0,&g_2278,&g_2278},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278}},{{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278},{(void*)0,(void*)0,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,(void*)0},{&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278,&g_2278}}};
static int8_t * const * volatile **g_2276 = &g_2277[2][2][9];
static int8_t * const * volatile *** volatile g_2275 = &g_2276;/* VOLATILE GLOBAL g_2275 */
static int32_t ****g_2327 = &g_896;
static int32_t *****g_2326 = &g_2327;
static volatile struct S0 g_2344 = {28,1L,0xD2A2C162474E4EE1LL,0UL,0x1.2ABDB5p-16,0x2519A30FL,9,3,2UL};/* VOLATILE GLOBAL g_2344 */
static volatile struct S0 g_2369 = {13,0xB74247C733E58A29LL,0x349F55A302F57282LL,0x8EL,0x0.9p+1,0x7D9FCFD1L,7,7,1UL};/* VOLATILE GLOBAL g_2369 */
static const int32_t ****g_2389 = (void*)0;
static uint32_t *** const g_2426 = &g_239;
static volatile struct S0 g_2467[6] = {{-8,0xE486B6D7F758CD9DLL,-5L,0UL,0x0.6p+1,3L,1,3,0x7204L},{-8,0xE486B6D7F758CD9DLL,-5L,0UL,0x0.6p+1,3L,1,3,0x7204L},{-8,0xE486B6D7F758CD9DLL,-5L,0UL,0x0.6p+1,3L,1,3,0x7204L},{-8,0xE486B6D7F758CD9DLL,-5L,0UL,0x0.6p+1,3L,1,3,0x7204L},{-8,0xE486B6D7F758CD9DLL,-5L,0UL,0x0.6p+1,3L,1,3,0x7204L},{-8,0xE486B6D7F758CD9DLL,-5L,0UL,0x0.6p+1,3L,1,3,0x7204L}};
static const uint32_t *g_2537 = (void*)0;
static const uint32_t **g_2536 = &g_2537;
static const uint32_t ***g_2535 = &g_2536;
static int16_t g_2583 = 9L;
static struct S0 g_2623 = {-28,0xCB9137EFE7B18E49LL,-5L,0UL,0xE.2C80CFp-12,0x406A83B7L,3,10,3UL};/* VOLATILE GLOBAL g_2623 */
static int32_t g_2651 = 1L;
static int16_t g_2653 = 0L;
static volatile int16_t g_2654 = 0x6FC9L;/* VOLATILE GLOBAL g_2654 */
static int64_t g_2655 = (-1L);
static uint8_t g_2656 = 255UL;
static float g_2668 = 0xF.1BC87Bp-25;
static int8_t g_2709 = 9L;
static int16_t g_2710[8][7] = {{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L},{0x1510L,1L,0xD390L,0xD390L,1L,0x1510L,0xABF4L}};
static int8_t g_2711 = (-10L);
static volatile int8_t g_2712 = (-1L);/* VOLATILE GLOBAL g_2712 */
static int32_t g_2713 = (-9L);
static volatile int32_t g_2714[1][6] = {{0xEA6B4D2BL,0xEA6B4D2BL,0xEA6B4D2BL,0xEA6B4D2BL,0xEA6B4D2BL,0xEA6B4D2BL}};
static volatile uint16_t g_2715 = 0xE302L;/* VOLATILE GLOBAL g_2715 */
static volatile struct S0 g_2746 = {-18,-1L,0x4EF0DABAFA6F5BC0LL,0UL,0x7.0DA48Ep-61,0x56E728DEL,9,20,0x0320L};/* VOLATILE GLOBAL g_2746 */
static int8_t g_2796 = 0xF1L;
static volatile struct S0 g_2859 = {-13,0x57F909E8A0C153BCLL,0x031B403E69350123LL,0UL,0x7.8E438Ep+27,5L,9,15,0xEDD7L};/* VOLATILE GLOBAL g_2859 */
static int16_t ****g_2927 = (void*)0;
static struct S0 g_2951 = {-20,6L,0L,0x5BL,-0x1.Ap-1,0x83BB0D35L,3,13,65527UL};/* VOLATILE GLOBAL g_2951 */
static struct S0 g_2954 = {-4,0xFFD0442BE3EC5E41LL,0x8117F9502C47089CLL,0x83L,0x0.9p-1,-2L,4,19,0xA01CL};/* VOLATILE GLOBAL g_2954 */
static volatile int16_t g_2957 = 0xF147L;/* VOLATILE GLOBAL g_2957 */
static const uint16_t g_3011 = 1UL;
static volatile int64_t g_3056 = 0xC14F6E15CC3A1C9DLL;/* VOLATILE GLOBAL g_3056 */
static volatile int8_t g_3057 = 0x32L;/* VOLATILE GLOBAL g_3057 */
static uint16_t g_3058 = 1UL;
static uint16_t * const g_3097 = &g_1904.f8;
static uint16_t * const *g_3096 = &g_3097;
static volatile uint32_t g_3109 = 18446744073709551615UL;/* VOLATILE GLOBAL g_3109 */
static int32_t g_3118[10] = {1L,1L,1L,1L,1L,1L,1L,1L,1L,1L};
static volatile uint64_t * volatile g_3184[3][1] = {{(void*)0},{(void*)0},{(void*)0}};
static volatile uint64_t * volatile * volatile g_3183 = &g_3184[2][0];/* VOLATILE GLOBAL g_3183 */
static uint8_t g_3209 = 255UL;
static float ** const *g_3306 = &g_861;
static float ** const ** const  volatile g_3305 = &g_3306;/* VOLATILE GLOBAL g_3305 */
static float g_3343[10] = {(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1),(-0x9.4p-1)};
static volatile uint8_t g_3372 = 254UL;/* VOLATILE GLOBAL g_3372 */


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static const uint16_t  func_11(int32_t * p_12, int32_t  p_13, int8_t  p_14, uint64_t  p_15, int32_t * p_16);
static int32_t * func_17(int32_t * p_18, const int32_t  p_19);
static int32_t * func_20(uint8_t  p_21, float  p_22, int32_t * p_23);
static uint8_t  func_46(int16_t  p_47, int32_t  p_48, uint8_t * p_49, int32_t  p_50);
static uint8_t  func_55(int32_t * p_56, int64_t  p_57, int32_t ** p_58, uint8_t  p_59, int8_t  p_60);
static int32_t * func_61(int32_t  p_62, uint32_t  p_63, int32_t  p_64, float  p_65);
static uint16_t  func_72(int32_t ** p_73, int32_t * p_74, float  p_75);
static int32_t * func_78(uint8_t * p_79, const int8_t  p_80, const int32_t * p_81);
static uint8_t * func_82(uint32_t  p_83);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_30 g_34 g_31 g_36 g_1269 g_2134 g_2135 g_240 g_104 g_1630 g_1631 g_953 g_460 g_461 g_156 g_1666 g_315 g_316 g_999 g_2185 g_1785.f5 g_688 g_1558.f7 g_2344 g_1483 g_297.f7 g_1765 g_1904.f8 g_180 g_181 g_182 g_183 g_1558.f6 g_823 g_2369 g_527 g_528 g_105 g_2389 g_140.f6 g_297.f2 g_2426 g_1904.f5 g_1558.f8 g_952 g_574 g_575 g_211 g_210 g_174.f5 g_140.f2 g_881 g_107 g_654 g_1854.f7 g_239 g_174.f8 g_88 g_2079 g_1245 g_2467 g_140.f1 g_1009.f2 g_2275 g_2276 g_2277 g_2535 g_140.f5 g_2186 g_1479 g_1823 g_2623 g_619 g_2656 g_2327 g_896 g_1361 g_1362 g_1360 g_139 g_140 g_897 g_797 g_2715 g_2655 g_824 g_2746 g_2136 g_85 g_291 g_2796 g_606 g_607 g_102 g_2278 g_1476 g_1854 g_1663 g_1664 g_2859 g_1180 g_481 g_297.f0 g_2927 g_2951 g_299 g_2954 g_3011 g_157 g_297.f1 g_1904.f0 g_3058 g_883 g_3096 g_3097 g_3109 g_1904.f7 g_174.f4 g_174.f6 g_174.f0 g_174.f2 g_209 g_174.f7 g_236 g_685 g_467.f2 g_728 g_2710 g_1830 g_1904.f2 g_3183 g_2713 g_3209 g_3118 g_714 g_467.f5 g_297.f5 g_1009.f3 g_727 g_3305 g_3372
 * writes: g_36 g_31 g_34 g_104 g_210 g_1904.f8 g_1558.f2 g_999 g_1558.f5 g_2185 g_2326 g_467.f2 g_1483 g_156 g_291 g_105 g_2389 g_85 g_884 g_1496 g_1497 g_797 g_1009.f8 g_481 g_1476 g_174.f8 g_1558.f8 g_1245 g_240 g_316 g_2535 g_1479 g_315 g_102 g_881 g_461 g_1269 g_2656 g_897 g_88 g_2623.f2 g_1666 g_823 g_2715 g_2655 g_183 g_1785.f2 g_239 g_1180 g_2927 g_157 g_3058 g_727 g_2954.f5 g_883 g_3096 g_107 g_139 g_170 g_180 g_174.f2 g_211 g_174.f0 g_209 g_235 g_236 g_292 g_299 g_654 g_714 g_728 g_467.f5 g_963 g_1904.f2 g_2713 g_725 g_2583 g_2623.f5 g_1904.f5 g_824 g_2954.f8 g_3306 g_3372
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_5 = 0xBA20B95AL;
    int32_t l_3346 = (-1L);
    int32_t *l_3347[9][7] = {{&g_236,&g_3118[9],&g_34,(void*)0,&g_236,&g_236,(void*)0},{(void*)0,(void*)0,(void*)0,&l_3346,(void*)0,&g_236,(void*)0},{&g_34,&g_3118[9],&g_236,&l_3346,&g_236,&g_3118[9],&g_34},{&g_3118[9],(void*)0,(void*)0,(void*)0,&g_2713,&g_236,&g_2713},{&g_3118[9],&g_2713,&g_2713,&g_3118[9],(void*)0,&g_236,&l_3346},{&g_34,&g_236,(void*)0,(void*)0,(void*)0,(void*)0,&g_236},{(void*)0,(void*)0,(void*)0,&g_3118[9],&l_3346,&g_34,&g_34},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_3118[9],&l_3346},{(void*)0,&g_236,(void*)0,&g_3118[9],&g_236,&g_3118[9],(void*)0}};
    uint32_t l_3348 = 1UL;
    uint8_t *l_3351 = &g_3209;
    int8_t **l_3367[5][1] = {{&g_2079[6]},{&g_2079[0]},{&g_2079[6]},{&g_2079[0]},{&g_2079[6]}};
    int8_t ***l_3366[10][1][1] = {{{(void*)0}},{{&l_3367[2][0]}},{{&l_3367[1][0]}},{{&l_3367[2][0]}},{{(void*)0}},{{(void*)0}},{{&l_3367[2][0]}},{{&l_3367[1][0]}},{{&l_3367[2][0]}},{{(void*)0}}};
    int8_t ****l_3365 = &l_3366[2][0][0];
    int8_t **** const *l_3364[6][4][8] = {{{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365},{&l_3365,&l_3365,&l_3365,(void*)0,(void*)0,&l_3365,&l_3365,&l_3365}},{{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,(void*)0,(void*)0},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365},{(void*)0,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365,&l_3365}},{{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,(void*)0,(void*)0,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365},{&l_3365,(void*)0,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365}},{{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365},{&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0},{&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365}},{{&l_3365,&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365}},{{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365},{&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365},{(void*)0,&l_3365,(void*)0,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365},{&l_3365,(void*)0,&l_3365,&l_3365,&l_3365,&l_3365,&l_3365,(void*)0}}};
    uint8_t l_3375[5][2][9] = {{{0x7BL,247UL,0xB5L,1UL,1UL,0xB5L,247UL,0x7BL,0xB5L},{2UL,0x1DL,255UL,0x44L,0x44L,255UL,0x1DL,2UL,255UL}},{{0x7BL,247UL,0xB5L,1UL,1UL,0xB5L,247UL,0x7BL,0xB5L},{2UL,0x1DL,255UL,0x44L,0x44L,255UL,0x1DL,2UL,255UL}},{{0x7BL,247UL,0xB5L,1UL,1UL,0xB5L,247UL,0x7BL,0xB5L},{2UL,0x1DL,255UL,0x44L,0x44L,255UL,0x1DL,2UL,255UL}},{{0x7BL,247UL,0xB5L,1UL,1UL,0xB5L,247UL,0x7BL,0xB5L},{2UL,0x1DL,255UL,0x44L,0x44L,255UL,0x1DL,2UL,255UL}},{{0x7BL,247UL,0xB5L,1UL,1UL,0xB5L,247UL,0x7BL,0xB5L},{2UL,0x1DL,255UL,0x44L,0x44L,255UL,0x1DL,2UL,255UL}}};
    int i, j, k;
    if (g_2)
    { /* block id: 1 */
        int32_t *l_33 = &g_34;
        int32_t **l_32 = &l_33;
        uint8_t *l_35 = &g_36;
        int32_t l_3140 = 0x6CF4E910L;
        (**g_953) = ((((safe_mod_func_uint8_t_u_u(((l_5 ^ g_2) > (safe_add_func_int8_t_s_s((safe_div_func_uint8_t_u_u((((safe_unary_minus_func_uint8_t_u(g_2)) != (func_11(func_17(func_20(((*l_35) &= (safe_div_func_uint32_t_u_u(0xDF11BBB4L, (safe_lshift_func_uint8_t_u_s((((((safe_add_func_int64_t_s_s((g_30 != ((*l_32) = (void*)0)), g_34)) >= (g_34 > (g_31 == g_34))) , g_2) , g_34) && 0x748BBE26B5CFDA6CLL), l_5))))), l_5, &g_34), l_5), l_5, l_3140, g_2710[7][5], &l_3140) , l_5)) , (*g_316)), g_3011)), 0x6DL))), l_5)) , l_5) < 0x0.Ap+1) , (*l_32));
    }
    else
    { /* block id: 1482 */
        int16_t l_3345 = 0x3C93L;
        return l_3345;
    }
    l_3348--;
    for (g_2655 = 0; (g_2655 <= 6); g_2655 += 1)
    { /* block id: 1488 */
        const uint64_t l_3352 = 0x85D3E9BDE2F4E097LL;
        int32_t l_3355 = (-8L);
        float l_3370[6] = {0x1.5p+1,0x1.5p+1,0x1.114171p-41,0x1.5p+1,0x1.5p+1,0x1.114171p-41};
        int32_t l_3371 = (-3L);
        int i;
        (*g_182) = (**g_181);
    }
    --g_3372;
    return l_3375[0][0][6];
}


/* ------------------------------------------ */
/* 
 * reads : g_1830 g_1904.f2 g_823 g_3183 g_236 g_2713 g_3097 g_1904.f8 g_2954.f5 g_3209 g_3118 g_714 g_467.f5 g_297.f5 g_574 g_575 g_105 g_211 g_210 g_180 g_181 g_182 g_183 g_30 g_31 g_174.f5 g_140.f2 g_316 g_881 g_36 g_107 g_240 g_104 g_654 g_2623.f5 g_1904.f5 g_1476 g_1631 g_239 g_2954.f1 g_3096 g_1009.f3 g_688 g_999 g_1854.f2 g_460 g_2134.f0 g_824 g_2426 g_727 g_2275 g_2276 g_1785.f5 g_3305 g_157 g_481 g_85 g_34
 * writes: g_1904.f2 g_34 g_2713 g_725 g_2583 g_85 g_884 g_1496 g_1497 g_31 g_797 g_1483 g_1009.f8 g_104 g_481 g_963 g_183 g_2623.f5 g_1904.f5 g_823 g_461 g_824 g_2954.f8 g_1904.f8 g_236 g_727 g_3306 g_157
 */
static const uint16_t  func_11(int32_t * p_12, int32_t  p_13, int8_t  p_14, uint64_t  p_15, int32_t * p_16)
{ /* block id: 1374 */
    int64_t l_3141 = (-1L);
    int32_t *l_3142 = &g_2713;
    int32_t *l_3143 = (void*)0;
    int32_t *l_3144 = &g_2713;
    int32_t *l_3145 = &g_1180;
    int32_t *l_3146 = &g_823;
    int32_t *l_3147 = &g_236;
    int32_t *l_3148 = &g_823;
    int32_t *l_3149 = (void*)0;
    int32_t *l_3150 = &g_3118[6];
    int32_t *l_3151 = (void*)0;
    int32_t *l_3152 = &g_3118[4];
    int32_t *l_3153 = &g_236;
    int32_t *l_3154 = (void*)0;
    int32_t *l_3155 = &g_236;
    int32_t *l_3156 = &g_727;
    int32_t *l_3157 = (void*)0;
    int32_t *l_3158[1][2][9] = {{{(void*)0,&g_3118[9],(void*)0,(void*)0,&g_3118[9],&g_3118[9],(void*)0,(void*)0,&g_3118[9]},{&g_3118[9],(void*)0,(void*)0,&g_3118[9],&g_3118[9],(void*)0,(void*)0,&g_3118[9],(void*)0}}};
    uint8_t l_3159 = 0xAAL;
    const int8_t *l_3170 = &g_2711;
    const int8_t **l_3169 = &l_3170;
    const int8_t ***l_3168[6] = {&l_3169,&l_3169,&l_3169,&l_3169,&l_3169,&l_3169};
    int16_t l_3210 = 0L;
    const uint32_t l_3234 = 0x465DB1F0L;
    const uint32_t l_3245[2][1] = {{18446744073709551614UL},{18446744073709551614UL}};
    float ** const *l_3264 = (void*)0;
    float ** const * const *l_3263 = &l_3264;
    int16_t **** const *l_3288 = &g_2927;
    uint32_t l_3300 = 0xB2BA91AFL;
    int i, j, k;
    l_3159++;
    if ((g_1830 || p_14))
    { /* block id: 1376 */
        uint32_t l_3165 = 4294967295UL;
        for (g_1904.f2 = 15; (g_1904.f2 == 13); g_1904.f2 = safe_sub_func_uint8_t_u_u(g_1904.f2, 2))
        { /* block id: 1379 */
            (*p_12) = (+0x919256BBL);
            l_3165--;
        }
    }
    else
    { /* block id: 1383 */
        int64_t l_3171 = 0x2DEF34DEBAC6C5CCLL;
        uint64_t *l_3182 = &g_88[1][5][0];
        uint64_t **l_3181 = &l_3182;
        uint64_t ***l_3180 = &l_3181;
        int32_t l_3206 = 0xF747E856L;
        float *l_3207 = &g_725;
        int16_t *l_3208 = &g_2583;
        int32_t l_3211 = 0L;
        uint32_t *l_3212[10][3][3] = {{{&g_1825,&g_1825,&g_963[1]},{(void*)0,&g_963[1],(void*)0},{&g_1825,(void*)0,&g_963[1]}},{{&g_619,&g_963[1],&g_619},{&g_1825,&g_1825,&g_963[1]},{(void*)0,&g_963[1],(void*)0}},{{&g_1825,(void*)0,&g_963[1]},{&g_619,&g_963[1],&g_619},{&g_1825,&g_1825,&g_963[1]}},{{(void*)0,&g_211,&g_85},{(void*)0,&g_1825,&g_85},{&g_85,&g_211,&g_85}},{{(void*)0,&g_963[1],&g_85},{&g_85,&g_211,&g_85},{(void*)0,&g_1825,&g_85}},{{&g_85,&g_211,&g_85},{(void*)0,&g_963[1],&g_85},{&g_85,&g_211,&g_85}},{{(void*)0,&g_1825,&g_85},{&g_85,&g_211,&g_85},{(void*)0,&g_963[1],&g_85}},{{&g_85,&g_211,&g_85},{(void*)0,&g_1825,&g_85},{&g_85,&g_211,&g_85}},{{(void*)0,&g_963[1],&g_85},{&g_85,&g_211,&g_85},{(void*)0,&g_1825,&g_85}},{{&g_85,&g_211,&g_85},{(void*)0,&g_963[1],&g_85},{&g_85,&g_211,&g_85}}};
        int32_t l_3213 = 0x62FFB11AL;
        int32_t *l_3214[2];
        int8_t l_3219 = 0x5AL;
        const int32_t *****l_3270 = &g_2389;
        const int16_t *l_3315 = &g_2710[0][6];
        const int16_t * const *l_3314[10] = {&l_3315,&l_3315,&l_3315,&l_3315,&l_3315,&l_3315,&l_3315,&l_3315,&l_3315,&l_3315};
        const int16_t * const **l_3313 = &l_3314[8];
        const int16_t * const ***l_3312 = &l_3313;
        const int16_t * const ****l_3311 = &l_3312;
        int32_t l_3325 = 0xA6AB7E3EL;
        uint64_t l_3328 = 0UL;
        uint64_t l_3344 = 0xDB56300813B1D81FLL;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_3214[i] = (void*)0;
        l_3214[0] = func_17((((void*)0 != l_3168[0]) , func_61((*l_3148), (l_3171 , (((safe_div_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s(((g_85 = (((safe_sub_func_uint64_t_u_u(0UL, (l_3211 = (safe_rshift_func_int8_t_s_s((((*l_3180) = (void*)0) != g_3183), (((safe_div_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((safe_add_func_int16_t_s_s(((*l_3208) = (safe_sub_func_int8_t_s_s((safe_add_func_uint64_t_u_u((safe_mod_func_int8_t_s_s((*l_3155), (safe_sub_func_uint16_t_u_u(((+(((*l_3207) = (safe_mul_func_float_f_f((safe_div_func_float_f_f((((*l_3144) = (l_3206 = (safe_sub_func_float_f_f((*l_3144), 0x0.A86871p+75)))) != p_13), p_15)), 0x1.99DF32p+42))) , 0x37L)) || p_14), (*g_3097))))), g_2954.f5)), l_3171))), l_3171)), g_3209)), 1UL)) < l_3210) >= (*p_16))))))) < l_3171) || (*l_3150))) , 0xF4L), l_3171)) & l_3213), l_3171)) & 0x9981428CE797C2F9LL) | (*g_3097))), (*g_714), p_14)), p_13);
lbl_3318:
        for (g_2623.f5 = 0; (g_2623.f5 < 20); g_2623.f5 = safe_add_func_int32_t_s_s(g_2623.f5, 6))
        { /* block id: 1394 */
            int8_t l_3227 = (-1L);
            float l_3246 = 0xE.9BCD4Dp+70;
            int32_t *l_3260 = &g_823;
            for (g_1904.f5 = 5; (g_1904.f5 >= 1); g_1904.f5 -= 1)
            { /* block id: 1397 */
                int32_t l_3228[2];
                const uint32_t *l_3239 = (void*)0;
                int64_t *l_3243 = &g_1009[2].f2;
                float ****l_3244[7];
                int32_t l_3249 = (-1L);
                int i;
                for (i = 0; i < 2; i++)
                    l_3228[i] = 0x8A3698E8L;
                for (i = 0; i < 7; i++)
                    l_3244[i] = (void*)0;
                if ((safe_mod_func_uint16_t_u_u(g_1476[g_1904.f5], (l_3219 , (safe_lshift_func_uint8_t_u_u((*g_316), (safe_sub_func_uint16_t_u_u((*g_3097), ((safe_unary_minus_func_uint8_t_u((safe_mod_func_uint16_t_u_u(((l_3228[1] = ((***g_1631) &= l_3227)) | (((safe_add_func_uint32_t_u_u(4294967295UL, ((!p_13) & (1UL != p_15)))) >= g_2954.f1) > (*l_3150))), p_14)))) | 0x9612E03246D1A088LL)))))))))
                { /* block id: 1400 */
                    int16_t l_3240 = (-6L);
                    uint16_t *l_3247 = (void*)0;
                    uint16_t *l_3248[3][8][7] = {{{&g_2623.f8,&g_467.f8,&g_2954.f8,(void*)0,(void*)0,&g_999,(void*)0},{&g_1558.f8,(void*)0,&g_728,&g_2623.f8,&g_1558.f8,&g_654,&g_654},{&g_1009[2].f8,(void*)0,&g_102[6][1],(void*)0,&g_1009[2].f8,&g_1904.f8,&g_2954.f8},{&g_2623.f8,&g_2623.f8,&g_1785[5].f8,&g_1558.f8,&g_728,&g_2134.f8,&g_2623.f8},{&g_467.f8,(void*)0,(void*)0,&g_1785[5].f8,&g_999,&g_467.f8,&g_2951.f8},{&g_2623.f8,&g_1558.f8,&g_1904.f8,&g_1785[5].f8,(void*)0,&g_1785[5].f8,&g_1904.f8},{&g_1009[2].f8,&g_1009[2].f8,&g_467.f8,&g_467.f8,&g_2954.f8,&g_2951.f8,&g_1904.f8},{&g_1558.f8,&g_728,&g_1785[5].f8,(void*)0,(void*)0,&g_1558.f8,&g_1558.f8}},{{&g_2623.f8,&g_999,(void*)0,&g_467.f8,&g_2954.f8,(void*)0,&g_999},{&g_1904.f8,(void*)0,&g_1009[2].f8,&g_1009[2].f8,(void*)0,&g_1904.f8,&g_1269},{(void*)0,&g_2954.f8,&g_467.f8,(void*)0,&g_999,&g_2623.f8,&g_1904.f8},{&g_1558.f8,(void*)0,(void*)0,&g_1785[5].f8,&g_728,&g_1558.f8,&g_654},{&g_2951.f8,&g_2954.f8,&g_467.f8,&g_467.f8,&g_1009[2].f8,&g_1009[2].f8,&g_467.f8},{&g_1785[5].f8,(void*)0,&g_1785[5].f8,&g_1904.f8,&g_1558.f8,&g_2623.f8,(void*)0},{&g_467.f8,&g_999,&g_1785[5].f8,(void*)0,(void*)0,&g_467.f8,&g_1009[2].f8},{&g_2134.f8,&g_728,&g_1558.f8,&g_1785[5].f8,&g_2623.f8,&g_2623.f8,(void*)0}},{{&g_1904.f8,&g_1009[2].f8,(void*)0,&g_102[6][1],(void*)0,&g_1009[2].f8,&g_1904.f8},{&g_1558.f8,&g_174.f8,&g_1904.f8,&g_999,&g_1904.f8,(void*)0,&g_1785[5].f8},{&g_467.f8,&g_174.f8,(void*)0,&g_1904.f8,&g_999,&g_102[6][1],&g_174.f8},{&g_2134.f8,&g_2623.f8,&g_1904.f8,&g_1269,&g_654,&g_2134.f8,&g_654},{(void*)0,(void*)0,(void*)0,(void*)0,&g_1904.f8,&g_297[2][2][0].f8,&g_2623.f8},{&g_102[6][1],&g_1904.f8,&g_174.f8,&g_2623.f8,&g_2134.f8,&g_728,&g_1558.f8},{&g_467.f8,&g_999,&g_1009[2].f8,(void*)0,&g_467.f8,(void*)0,&g_2623.f8},{&g_2623.f8,&g_654,&g_1785[5].f8,&g_1785[5].f8,&g_1785[5].f8,&g_1785[5].f8,&g_654}}};
                    int i, j, k;
                    if ((safe_sub_func_uint16_t_u_u(p_14, (l_3249 ^= (((l_3234 , (((safe_lshift_func_uint16_t_u_s((**g_3096), (l_3228[1] = ((((0UL ^ (safe_rshift_func_int16_t_s_s((l_3239 == (void*)0), l_3240))) > ((l_3227 < (safe_mod_func_uint8_t_u_u((((((((l_3243 != l_3243) == g_1476[g_1904.f5]) , 0L) >= (*g_316)) , &g_1599) != l_3244[3]) <= p_15), l_3245[1][0]))) ^ l_3228[1])) && g_1009[2].f3) ^ (*l_3148))))) <= 7L) , l_3227)) & (**g_239)) | p_14)))))
                    { /* block id: 1403 */
                        (*l_3146) |= 0x2ABF844DL;
                    }
                    else
                    { /* block id: 1405 */
                        uint8_t l_3258 = 0x90L;
                        (*l_3207) = (p_15 != (0xE.97B356p-23 <= ((safe_mul_func_float_f_f((safe_mul_func_float_f_f(0x8.4p-1, ((safe_mod_func_uint16_t_u_u((g_1476[g_1904.f5] < (**g_688)), ((0x40FFL <= ((safe_mod_func_uint64_t_u_u(g_999, 18446744073709551608UL)) <= g_1854.f2)) && 18446744073709551615UL))) , l_3258))), 0x4.480113p-65)) == 0x6.0085E9p+91)));
                    }
                }
                else
                { /* block id: 1408 */
                    int32_t **l_3259 = &l_3155;
                    (*g_460) = ((*l_3259) = func_17(p_12, p_15));
                }
                p_16 = p_12;
                return (**g_3096);
            }
            if (l_3227)
                continue;
            (*g_182) = l_3260;
        }
        if ((safe_lshift_func_int8_t_s_u(((&g_2276 == &g_2276) != 1UL), ((0UL != ((g_2134.f0 , l_3263) == &g_1361)) , (((safe_unary_minus_func_int64_t_s((1L != 0x17CE26A64633ABB0LL))) & (*l_3148)) & (-1L))))))
        { /* block id: 1418 */
            int16_t *l_3277 = &g_235[0];
            int64_t *l_3279[9][6][4] = {{{&l_3171,&g_2134.f2,&g_2134.f2,&g_920},{(void*)0,&g_1785[5].f2,&g_292,&g_920},{&g_467.f2,&g_2134.f2,&l_3141,&g_1245},{&g_1245,&g_2655,&l_3141,(void*)0},{&g_467.f2,&g_2134.f2,&g_292,(void*)0},{(void*)0,&g_2134.f2,&g_2134.f2,(void*)0}},{{&l_3171,&g_2655,&g_2134.f2,&g_1245},{&l_3171,&g_2134.f2,&g_2134.f2,&g_920},{(void*)0,&g_1785[5].f2,&g_292,&g_920},{&g_467.f2,&g_2134.f2,&l_3141,&g_1245},{&g_1245,&g_2655,&l_3141,(void*)0},{&g_467.f2,&g_2134.f2,&g_292,(void*)0}},{{(void*)0,&g_2134.f2,&g_2134.f2,(void*)0},{&l_3171,&g_2655,&g_2134.f2,&g_1245},{&l_3171,&g_2134.f2,&g_2134.f2,&g_920},{(void*)0,&g_1785[5].f2,&g_292,&g_920},{&g_467.f2,&g_2134.f2,&l_3141,&g_1245},{&g_1245,&g_2655,&l_3141,(void*)0}},{{&g_467.f2,&g_2134.f2,&g_292,(void*)0},{(void*)0,&g_2134.f2,&g_2134.f2,(void*)0},{&l_3171,&g_2655,&g_2134.f2,&g_1245},{&l_3171,&g_2134.f2,&g_2134.f2,&g_920},{(void*)0,&g_1785[5].f2,&g_292,&g_920},{&g_467.f2,&g_2134.f2,&l_3141,&g_1245}},{{&g_1245,&g_2655,&l_3141,(void*)0},{&g_467.f2,&g_2134.f2,&g_292,(void*)0},{(void*)0,&g_2134.f2,&g_2134.f2,(void*)0},{&l_3171,&g_2655,&g_2134.f2,&g_1245},{&l_3171,&g_2134.f2,&g_2134.f2,&g_920},{(void*)0,&g_1785[5].f2,&g_292,&g_920}},{{&g_467.f2,&g_2134.f2,&l_3141,&g_1245},{&g_1245,&g_2655,&l_3141,(void*)0},{&g_467.f2,&g_2134.f2,&g_292,(void*)0},{(void*)0,&g_2134.f2,&g_2134.f2,(void*)0},{&l_3171,&g_2655,&g_2134.f2,&g_1245},{&l_3171,&g_2954.f2,&g_1785[5].f2,(void*)0}},{{&l_3171,&g_292,&l_3141,(void*)0},{(void*)0,&g_2954.f2,&g_2134.f2,&g_920},{&g_920,&g_2134.f2,&g_2134.f2,&l_3171},{(void*)0,&g_1785[5].f2,&l_3141,&g_467.f2},{&l_3171,&g_1785[5].f2,&g_1785[5].f2,&l_3171},{&g_1245,&g_2134.f2,&g_2954.f2,&g_920}},{{&g_1245,&g_2954.f2,&g_1785[5].f2,(void*)0},{&l_3171,&g_292,&l_3141,(void*)0},{(void*)0,&g_2954.f2,&g_2134.f2,&g_920},{&g_920,&g_2134.f2,&g_2134.f2,&l_3171},{(void*)0,&g_1785[5].f2,&l_3141,&g_467.f2},{&l_3171,&g_1785[5].f2,&g_1785[5].f2,&l_3171}},{{&g_1245,&g_2134.f2,&g_2954.f2,&g_920},{&g_1245,&g_2954.f2,&g_1785[5].f2,(void*)0},{&l_3171,&g_292,&l_3141,(void*)0},{(void*)0,&g_2954.f2,&g_2134.f2,&g_920},{&g_920,&g_2134.f2,&g_2134.f2,&l_3171},{(void*)0,&g_1785[5].f2,&l_3141,&g_467.f2}}};
            float ****l_3281 = &g_860[7][0];
            int32_t l_3295 = (-4L);
            int32_t l_3297[6][7] = {{1L,4L,(-1L),4L,1L,(-1L),0x143BD9DAL},{0x143BD9DAL,0x948C9129L,(-2L),0x143BD9DAL,(-2L),0x948C9129L,0x143BD9DAL},{1L,0x143BD9DAL,0x948C9129L,(-2L),0x143BD9DAL,(-2L),0x948C9129L},{0x143BD9DAL,0x143BD9DAL,(-1L),1L,4L,(-1L),4L},{1L,0x948C9129L,0x948C9129L,1L,(-2L),1L,1L},{1L,4L,(-2L),(-2L),4L,1L,0x948C9129L}};
            const int16_t * const *l_3310 = (void*)0;
            const int16_t * const **l_3309[10];
            const int16_t * const ***l_3308 = &l_3309[5];
            const int16_t * const ****l_3307 = &l_3308;
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_3309[i] = &l_3310;
            for (g_824 = 8; (g_824 < 33); g_824 = safe_add_func_int64_t_s_s(g_824, 1))
            { /* block id: 1421 */
                uint64_t l_3276 = 18446744073709551612UL;
                int16_t *l_3278 = &g_2653;
                int64_t *l_3280[1];
                int32_t l_3289 = 0x66C99F2DL;
                int32_t l_3298 = 4L;
                int i;
                for (i = 0; i < 1; i++)
                    l_3280[i] = (void*)0;
                (*l_3207) = p_13;
                for (g_2954.f8 = 17; (g_2954.f8 < 41); g_2954.f8 = safe_add_func_int32_t_s_s(g_2954.f8, 6))
                { /* block id: 1425 */
                    l_3270 = l_3270;
                }
                if (((*l_3144) = (safe_mul_func_uint8_t_u_u((((***g_2426) | ((safe_lshift_func_uint8_t_u_s((((((~l_3276) , l_3277) != l_3278) & ((l_3279[5][3][0] == l_3280[0]) <= ((l_3281 = &g_860[7][0]) == (void*)0))) | (safe_lshift_func_uint16_t_u_u((l_3289 = ((*g_3097) = (safe_unary_minus_func_int16_t_s(((*l_3208) = ((~(((safe_lshift_func_uint16_t_u_u(((((void*)0 != l_3288) , p_14) , 0x3ACFL), 3)) == p_13) , 0UL)) , 1L)))))), 9))), 1)) == (*l_3156))) != p_13), 0xF3L))))
                { /* block id: 1433 */
                    int8_t **l_3294 = (void*)0;
                    int8_t ***l_3293 = &l_3294;
                    int8_t ****l_3292 = &l_3293;
                    (*g_182) = (void*)0;
                    (*l_3147) &= (safe_div_func_int64_t_s_s((l_3292 == (*g_2275)), g_1785[5].f5));
                }
                else
                { /* block id: 1436 */
                    float l_3296 = 0xA.89FBECp+25;
                    int32_t l_3299 = 0x791B5283L;
                    ++l_3300;
                    for (g_2583 = (-1); (g_2583 > (-14)); --g_2583)
                    { /* block id: 1440 */
                        (*l_3156) &= 0x85932B28L;
                    }
                }
                (*g_3305) = (*l_3263);
            }
            if (l_3159)
                goto lbl_3316;
lbl_3316:
            l_3311 = l_3307;
            for (g_157 = 0; (g_157 <= 8); g_157 += 1)
            { /* block id: 1450 */
                int32_t l_3317 = 0x8B6A1639L;
                for (g_1904.f2 = 5; (g_1904.f2 >= 0); g_1904.f2 -= 1)
                { /* block id: 1453 */
                    return p_15;
                }
                if (l_3317)
                    break;
                for (g_481 = 0; (g_481 <= 8); g_481 += 1)
                { /* block id: 1459 */
                    return p_15;
                }
            }
        }
        else
        { /* block id: 1463 */
            int64_t l_3319[9][5] = {{0L,0L,1L,0L,0L},{(-1L),(-1L),(-1L),1L,1L},{0L,1L,1L,0L,1L},{1L,(-1L),(-2L),(-1L),1L},{1L,0L,1L,1L,0L},{1L,1L,(-1L),(-1L),(-1L)},{0L,0L,1L,0L,0L},{(-1L),(-1L),(-1L),1L,1L},{0L,1L,1L,0L,1L}};
            int32_t l_3322 = 0L;
            int32_t l_3323 = (-1L);
            int32_t l_3324 = 0xE11AFCBEL;
            int32_t l_3326 = 6L;
            int32_t l_3327 = (-1L);
            int i, j;
            for (g_85 = 0; (g_85 <= 1); g_85 += 1)
            { /* block id: 1466 */
                int32_t l_3320 = 0x8A9D1A96L;
                int32_t l_3321[10][5] = {{(-9L),0xE6C19BC9L,0x0C54E594L,0xE6C19BC9L,(-9L)},{0xFE860BC0L,0xBBC88F27L,0xFE860BC0L,0xFE860BC0L,0xBBC88F27L},{(-9L),4L,(-3L),0xE6C19BC9L,(-3L)},{0xBBC88F27L,0xBBC88F27L,0L,0xBBC88F27L,0xBBC88F27L},{(-3L),0xE6C19BC9L,(-3L),4L,(-9L)},{0xBBC88F27L,0xFE860BC0L,0xFE860BC0L,0xBBC88F27L,0L},{(-3L),4L,(-9L),4L,(-3L)},{0L,0xFE860BC0L,0L,0L,0xFE860BC0L},{(-3L),(-8L),0x0C54E594L,4L,0x0C54E594L},{0xFE860BC0L,0xFE860BC0L,0xBBC88F27L,0xFE860BC0L,0xFE860BC0L}};
                int i, j;
                for (g_104 = 1; (g_104 <= 5); g_104 += 1)
                { /* block id: 1469 */
                    if (g_2134.f0)
                        goto lbl_3318;
                }
                --l_3328;
                (*l_3144) = ((safe_add_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((p_13 != 0x4A5B6866C0B1FE78LL), ((void*)0 == &g_3096))), 6L)) && (safe_mul_func_uint8_t_u_u(((safe_div_func_uint16_t_u_u((((safe_sub_func_int32_t_s_s((*p_12), (((safe_mul_func_uint16_t_u_u((*g_3097), (*g_3097))) < ((*l_3148) = ((*p_16) &= (*p_12)))) >= (*l_3150)))) && l_3322) & l_3323), 1UL)) == l_3344), p_15)));
            }
            (***g_180) = l_3154;
        }
    }
    return (*l_3156);
}


/* ------------------------------------------ */
/* 
 * reads : g_240 g_30 g_31 g_182
 * writes: g_963 g_104 g_31 g_183
 */
static int32_t * func_17(int32_t * p_18, const int32_t  p_19)
{ /* block id: 1364 */
    float ***l_3125 = &g_861;
    int32_t l_3126[9] = {(-9L),0x6359A03AL,(-9L),0x6359A03AL,(-9L),0x6359A03AL,(-9L),0x6359A03AL,(-9L)};
    int64_t *l_3127 = (void*)0;
    int32_t l_3128 = 1L;
    uint32_t *l_3129 = &g_963[1];
    const int32_t **l_3132 = &g_461;
    int32_t ***l_3133 = &g_897;
    int32_t ***l_3134 = &g_897;
    int32_t ***l_3135 = &g_897;
    int32_t ***l_3136[4][7];
    int32_t **l_3137 = &g_183;
    int8_t *l_3138[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t l_3139 = 0x5CA0L;
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
            l_3136[i][j] = &g_897;
    }
    (*g_30) &= ((l_3139 = (safe_sub_func_uint32_t_u_u(((l_3125 != (void*)0) | (l_3126[8] >= (l_3128 = p_19))), (((*l_3129) = p_19) , ((((safe_add_func_uint8_t_u_u((((*g_240) = ((l_3132 = l_3132) != (l_3137 = &p_18))) <= p_19), 0x21L)) , &l_3125) != &l_3125) <= p_19))))) >= p_19);
    (*g_182) = p_18;
    return p_18;
}


/* ------------------------------------------ */
/* 
 * reads : g_36 g_30 g_31 g_34 g_1269 g_2134 g_2135 g_240 g_104 g_1630 g_1631 g_953 g_460 g_461 g_156 g_1666 g_315 g_316 g_999 g_2185 g_1785.f5 g_688 g_1558.f7 g_2344 g_1483 g_297.f7 g_1765 g_1904.f8 g_180 g_181 g_182 g_183 g_1558.f6 g_823 g_2369 g_527 g_528 g_105 g_2389 g_140.f6 g_297.f2 g_2426 g_1904.f5 g_1558.f8 g_952 g_574 g_575 g_211 g_210 g_174.f5 g_140.f2 g_881 g_107 g_654 g_1854.f7 g_239 g_174.f8 g_88 g_2079 g_1245 g_2467 g_140.f1 g_1009.f2 g_2275 g_2276 g_2277 g_2535 g_140.f5 g_2186 g_1479 g_1823 g_2623 g_619 g_2656 g_2327 g_896 g_1361 g_1362 g_1360 g_139 g_140 g_897 g_797 g_2715 g_2655 g_824 g_2746 g_2136 g_85 g_291 g_2796 g_606 g_607 g_102 g_2278 g_1476 g_1854 g_1663 g_1664 g_2859 g_1180 g_481 g_297.f0 g_2927 g_2951 g_299 g_2954 g_3011 g_157 g_297.f1 g_1904.f0 g_3058 g_883 g_3096 g_3097 g_3109 g_1904.f7 g_174.f4 g_174.f6 g_174.f0 g_174.f2 g_209 g_174.f7 g_236 g_685 g_467.f2 g_2 g_728
 * writes: g_36 g_31 g_34 g_104 g_210 g_1904.f8 g_1558.f2 g_999 g_1558.f5 g_2185 g_2326 g_467.f2 g_1483 g_156 g_291 g_105 g_2389 g_85 g_884 g_1496 g_1497 g_797 g_1009.f8 g_481 g_1476 g_174.f8 g_1558.f8 g_1245 g_240 g_316 g_2535 g_1479 g_315 g_102 g_881 g_461 g_1269 g_2656 g_897 g_88 g_2623.f2 g_1666 g_823 g_2715 g_2655 g_183 g_1785.f2 g_239 g_1180 g_2927 g_157 g_3058 g_727 g_2954.f5 g_883 g_3096 g_107 g_139 g_170 g_180 g_174.f2 g_211 g_174.f0 g_209 g_235 g_236 g_292 g_299 g_654 g_714 g_728 g_467.f5
 */
static int32_t * func_20(uint8_t  p_21, float  p_22, int32_t * p_23)
{ /* block id: 4 */
    float l_37[7][4];
    int32_t l_38 = 0xE7EB6652L;
    int32_t *l_2034 = &g_1558.f5;
    float l_2042 = 0x0.Ep-1;
    int64_t l_2059[1];
    int8_t *l_2080 = &g_210[6];
    int32_t l_2114 = (-1L);
    uint32_t l_2115 = 8UL;
    int32_t l_2130 = 9L;
    uint32_t l_2142[3][8] = {{0x3F8E6975L,0xEEA605E1L,1UL,0xB83292B6L,1UL,0xEEA605E1L,0x3F8E6975L,1UL},{1UL,0xEEA605E1L,0x3F8E6975L,1UL,0x6499A196L,0x6499A196L,1UL,0x3F8E6975L},{0UL,0UL,0xEEA605E1L,0UL,0x6499A196L,0xB83292B6L,4294967295UL,0xB83292B6L}};
    int32_t l_2150 = 0L;
    int8_t l_2169[8] = {4L,4L,(-1L),4L,4L,(-1L),4L,4L};
    int8_t **l_2184 = &g_2079[2];
    int8_t *** const l_2183[8] = {&l_2184,&l_2184,&l_2184,&l_2184,&l_2184,&l_2184,&l_2184,&l_2184};
    uint32_t * const ***l_2191 = (void*)0;
    uint32_t * const ****l_2190 = &l_2191;
    int8_t l_2222[10][7] = {{1L,(-10L),0x91L,0x14L,1L,1L,0x14L},{0L,(-8L),0L,1L,1L,0L,0xE8L},{0x14L,(-5L),0xA9L,(-1L),(-5L),0L,(-5L)},{(-7L),1L,1L,(-7L),0xE8L,0L,1L},{1L,1L,(-10L),0x91L,0x14L,1L,1L},{0x3CL,1L,6L,1L,0x3CL,0xA3L,1L},{1L,0x94L,0L,0x14L,(-10L),0L,(-5L)},{1L,(-8L),0x08L,0x08L,(-8L),1L,0xE8L},{1L,0x14L,0L,1L,(-5L),4L,0x14L},{0x3CL,0xA6L,1L,0L,1L,0xA6L,0x3CL}};
    const uint64_t **l_2321 = (void*)0;
    uint8_t l_2393 = 1UL;
    int32_t l_2396 = 3L;
    int32_t l_2397 = (-10L);
    int32_t l_2398 = 1L;
    int32_t l_2400 = (-6L);
    uint32_t ***l_2425 = (void*)0;
    uint64_t l_2427 = 6UL;
    const int32_t *l_2428 = &l_38;
    uint8_t l_2435 = 1UL;
    int16_t *l_2453 = &g_1476[4];
    int32_t l_2454 = (-7L);
    uint8_t l_2476 = 248UL;
    int8_t l_2590 = 0xB7L;
    int16_t l_2596 = 0xB1B5L;
    int32_t l_2645 = (-2L);
    int32_t l_2647[3];
    const float l_2662 = 0x0.5p-1;
    int16_t l_2708 = 0x2773L;
    int32_t l_2735 = 0x325233B7L;
    int16_t l_2745 = 1L;
    int16_t **l_2789 = &l_2453;
    int16_t ***l_2788 = &l_2789;
    uint32_t l_2948[7][10][3] = {{{0xEFB3D1CAL,18446744073709551608UL,0x352D91BAL},{7UL,1UL,7UL},{0xE96E14C3L,18446744073709551615UL,0xB691603DL},{0xF126BD78L,0xB691603DL,0x20D7E9BDL},{5UL,0x352D91BAL,2UL},{0x85DBE9D8L,0x25E9E4BCL,1UL},{5UL,0xEFB3D1CAL,0x25E9E4BCL},{0xF126BD78L,18446744073709551615UL,0x5204E420L},{0xE96E14C3L,18446744073709551615UL,0xBC2FC7ECL},{7UL,0x0483A6C7L,0x83386B89L}},{{0xEFB3D1CAL,0x0C030AABL,0xD41BEAE0L},{0x0C030AABL,4UL,2UL},{0x84F2D46EL,0x581D6A7EL,5UL},{1UL,0x581D6A7EL,18446744073709551611UL},{0xBC2FC7ECL,4UL,18446744073709551609UL},{18446744073709551615UL,0x85DBE9D8L,18446744073709551609UL},{0x20D7E9BDL,18446744073709551615UL,0x83386B89L},{0x8D27C7E6L,0x8D27C7E6L,0xF126BD78L},{0x25E9E4BCL,18446744073709551615UL,18446744073709551615UL},{1UL,0x0C030AABL,0xC661E745L}},{{5UL,18446744073709551615UL,0x0483A6C7L},{4UL,1UL,0xC661E745L},{2UL,0xD41BEAE0L,18446744073709551615UL},{18446744073709551615UL,0x4D9299A7L,0xF126BD78L},{0xC661E745L,4UL,0x83386B89L},{1UL,0xE96E14C3L,18446744073709551609UL},{0x52D36825L,1UL,0xF63E6086L},{0xEFB3D1CAL,7UL,5UL},{0xC188BE91L,18446744073709551615UL,0x5204E420L},{0xC188BE91L,0xF63E6086L,0x52D36825L}},{{0xEFB3D1CAL,2UL,5UL},{0x52D36825L,1UL,1UL},{1UL,18446744073709551608UL,18446744073709551608UL},{0xC661E745L,0x20D7E9BDL,0xC188BE91L},{18446744073709551615UL,18446744073709551611UL,18446744073709551615UL},{2UL,0x581D6A7EL,4UL},{4UL,0x52D36825L,7UL},{5UL,0x581D6A7EL,0x84F2D46EL},{1UL,18446744073709551611UL,0xD41BEAE0L},{0x25E9E4BCL,0x20D7E9BDL,0xAE79508EL}},{{0x8D27C7E6L,18446744073709551608UL,1UL},{0x20D7E9BDL,1UL,18446744073709551615UL},{1UL,2UL,0xE96E14C3L},{18446744073709551608UL,0xF63E6086L,1UL},{0x20426F2AL,18446744073709551615UL,1UL},{18446744073709551615UL,7UL,0xE96E14C3L},{0x85DBE9D8L,1UL,18446744073709551615UL},{0x0C030AABL,0xE96E14C3L,1UL},{0xAE79508EL,4UL,0xAE79508EL},{18446744073709551609UL,0x4D9299A7L,0xD41BEAE0L}},{{18446744073709551615UL,0xD41BEAE0L,0x84F2D46EL},{0x5204E420L,1UL,7UL},{0x83386B89L,18446744073709551615UL,4UL},{0x5204E420L,0x0C030AABL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0xC188BE91L},{18446744073709551609UL,0x8D27C7E6L,18446744073709551608UL},{0xAE79508EL,18446744073709551615UL,1UL},{0x0C030AABL,0x85DBE9D8L,5UL},{0x85DBE9D8L,0xC661E745L,0x52D36825L},{18446744073709551615UL,18446744073709551608UL,0x5204E420L}},{{0x20426F2AL,18446744073709551608UL,5UL},{18446744073709551608UL,0xC661E745L,0xF63E6086L},{1UL,0x85DBE9D8L,18446744073709551609UL},{0x20D7E9BDL,18446744073709551615UL,0x83386B89L},{0x8D27C7E6L,0x8D27C7E6L,0xF126BD78L},{0x25E9E4BCL,18446744073709551615UL,18446744073709551615UL},{1UL,0x0C030AABL,0xC661E745L},{5UL,18446744073709551615UL,0x0483A6C7L},{4UL,1UL,0xC661E745L},{2UL,0xD41BEAE0L,18446744073709551615UL}}};
    float **l_3010[3][7] = {{&g_862,(void*)0,&g_862,&g_862,(void*)0,&g_862,&g_862},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_862,&g_862,(void*)0,&g_862,&g_862,(void*)0}};
    int32_t *****l_3025 = (void*)0;
    const uint16_t *l_3088 = (void*)0;
    const uint16_t * const *l_3087 = &l_3088;
    int i, j, k;
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
            l_37[i][j] = 0x5.Ap+1;
    }
    for (i = 0; i < 1; i++)
        l_2059[i] = 0x93BFD0B52EF8F22FLL;
    for (i = 0; i < 3; i++)
        l_2647[i] = 1L;
    for (g_36 = 0; (g_36 <= 3); g_36 += 1)
    { /* block id: 7 */
        int32_t *l_39 = &l_38;
        (*g_30) |= 5L;
        if (l_38)
            continue;
        for (l_38 = 3; (l_38 >= 0); l_38 -= 1)
        { /* block id: 12 */
            return p_23;
        }
    }
    for (g_34 = 5; (g_34 >= (-29)); g_34 = safe_sub_func_int8_t_s_s(g_34, 6))
    { /* block id: 18 */
        int32_t *l_77 = &g_34;
        int32_t **l_76 = &l_77;
        uint8_t *l_1657 = (void*)0;
        int32_t *l_2033 = &g_1785[5].f5;
        uint32_t l_2105[8][8][3] = {{{0xC33DE144L,1UL,0xC4921319L},{0UL,0x28143689L,1UL},{0x2E7FBA71L,8UL,0UL},{4294967294UL,0xA6387004L,0x8C1F9071L},{0x88BE0E6FL,0UL,0xA6387004L},{4294967287UL,4294967287UL,0xA4CF869BL},{0xD3861601L,4294967295UL,1UL},{0xF095586BL,0UL,4294967295UL}},{{9UL,4294967294UL,0x2E7FBA71L},{0xF5A20341L,0xF095586BL,4294967295UL},{0x28143689L,4294967295UL,1UL},{0x37A5A934L,1UL,0xA4CF869BL},{0xC67E0A41L,4294967295UL,0xA6387004L},{1UL,9UL,0x8C1F9071L},{4294967292UL,0UL,0UL},{0xC402F9F5L,0x699D8072L,0UL}},{{9UL,0x28143689L,0xE1558FE9L},{0x8C1F9071L,0UL,9UL},{4UL,9UL,6UL},{8UL,0x01A55309L,0x699D8072L},{9UL,4294967295UL,0xC402F9F5L},{0xABCB1F2EL,0xC4274E4BL,0x4C8178C9L},{0x602E9095L,0xC67E0A41L,5UL},{1UL,9UL,4294967294UL}},{{1UL,0xC402F9F5L,0xABCB1F2EL},{0x602E9095L,0x3DB298D3L,0xD3861601L},{0xABCB1F2EL,8UL,5UL},{9UL,0xC4921319L,4294967295UL},{8UL,0x43A33A30L,0xE2EA3C6EL},{4UL,0UL,0x21550A47L},{0x8C1F9071L,4294967291UL,9UL},{9UL,0x2B225B2AL,9UL}},{{0xC402F9F5L,4294967292UL,0x01A55309L},{4294967292UL,1UL,0UL},{1UL,4294967289UL,4294967289UL},{0xC67E0A41L,4294967289UL,0x9B735262L},{0x37A5A934L,0x0D199161L,1UL},{0x28143689L,0x23516F02L,4294967295UL},{0xF5A20341L,4294967289UL,0x7EAFB810L},{9UL,0x23516F02L,0x3150CF7AL}},{{0xF095586BL,0x0D199161L,9UL},{0xD3861601L,4294967289UL,0x0AE5D50EL},{4294967287UL,4294967289UL,1UL},{0x88BE0E6FL,1UL,4294967288UL},{4294967294UL,4294967292UL,0x37A5A934L},{0x2E7FBA71L,0x2B225B2AL,4294967295UL},{0xE2EA3C6EL,4294967291UL,4294967295UL},{4294967295UL,0UL,1UL}},{{0UL,0x43A33A30L,8UL},{0x7EAFB810L,0xC4921319L,0UL},{1UL,8UL,4294967292UL},{0x474EAF3BL,0x3DB298D3L,0xC4921319L},{1UL,0xC402F9F5L,0xD8057F8AL},{4294967295UL,9UL,0xD8057F8AL},{0UL,0xC67E0A41L,0xC4921319L},{0x21550A47L,0xC4274E4BL,4294967292UL}},{{0x6ECBA56FL,4294967295UL,0UL},{4294967289UL,0x01A55309L,8UL},{0xA6387004L,9UL,1UL},{0x2B225B2AL,0UL,4294967295UL},{0xC4921319L,0x28143689L,4294967295UL},{1UL,0x699D8072L,0x37A5A934L},{0x0D199161L,0UL,4294967288UL},{0x734B9151L,9UL,1UL}}};
        int32_t l_2110 = (-3L);
        int32_t l_2113[1];
        uint32_t l_2131 = 18446744073709551607UL;
        uint32_t *l_2149 = &l_2115;
        uint64_t l_2157[3][10][2] = {{{0xC997861EE04E8381LL,18446744073709551615UL},{0xEE478B98D3BA957BLL,18446744073709551607UL},{0x4339720A9B97801CLL,18446744073709551607UL},{8UL,18446744073709551607UL},{0xC997861EE04E8381LL,3UL},{0xFE60F2DF3951CB87LL,18446744073709551607UL},{1UL,18446744073709551607UL},{0xFE60F2DF3951CB87LL,3UL},{0xC997861EE04E8381LL,18446744073709551607UL},{8UL,18446744073709551607UL}},{{0xC997861EE04E8381LL,3UL},{0xFE60F2DF3951CB87LL,18446744073709551607UL},{1UL,18446744073709551607UL},{0xFE60F2DF3951CB87LL,3UL},{0xC997861EE04E8381LL,18446744073709551607UL},{8UL,18446744073709551607UL},{0xC997861EE04E8381LL,3UL},{0xFE60F2DF3951CB87LL,18446744073709551607UL},{1UL,18446744073709551607UL},{0xFE60F2DF3951CB87LL,3UL}},{{0xC997861EE04E8381LL,18446744073709551607UL},{8UL,18446744073709551607UL},{0xC997861EE04E8381LL,3UL},{0xFE60F2DF3951CB87LL,18446744073709551607UL},{1UL,18446744073709551607UL},{0xFE60F2DF3951CB87LL,3UL},{0xC997861EE04E8381LL,18446744073709551607UL},{8UL,18446744073709551607UL},{0xC997861EE04E8381LL,3UL},{0xFE60F2DF3951CB87LL,18446744073709551607UL}}};
        uint16_t *l_2168[10] = {&g_1785[5].f8,&g_102[6][1],&g_1785[5].f8,&g_102[6][1],&g_1785[5].f8,&g_102[6][1],&g_1785[5].f8,&g_102[6][1],&g_1785[5].f8,&g_102[6][1]};
        uint8_t l_2206 = 0xB0L;
        int8_t l_2223 = 0xF2L;
        int8_t * const * volatile *** volatile l_2279[3];
        int32_t *l_2294 = &l_2113[0];
        int32_t *****l_2328 = &g_2327;
        uint32_t ** const **l_2368 = (void*)0;
        uint32_t ** const ***l_2367 = &l_2368;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2113[i] = (-5L);
        for (i = 0; i < 3; i++)
            l_2279[i] = &g_2276;
        for (g_36 = 0; (g_36 < 56); ++g_36)
        { /* block id: 21 */
            int8_t l_1653 = 1L;
            int8_t *l_2038 = &g_1922;
            int8_t **l_2037 = &l_2038;
            int8_t ***l_2036[9][10][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,&l_2037},{(void*)0,&l_2037},{&l_2037,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2037,(void*)0},{&l_2037,(void*)0},{&l_2037,&l_2037},{(void*)0,(void*)0},{(void*)0,&l_2037},{&l_2037,(void*)0},{&l_2037,(void*)0},{&l_2037,&l_2037}},{{(void*)0,(void*)0},{(void*)0,&l_2037},{&l_2037,(void*)0},{&l_2037,(void*)0},{&l_2037,&l_2037},{(void*)0,(void*)0},{(void*)0,&l_2037},{&l_2037,(void*)0},{&l_2037,(void*)0},{&l_2037,&l_2037}},{{(void*)0,(void*)0},{(void*)0,&l_2037},{&l_2037,(void*)0},{&l_2037,(void*)0},{&l_2037,&l_2037},{(void*)0,(void*)0},{(void*)0,&l_2037},{&l_2037,(void*)0},{&l_2037,(void*)0},{&l_2037,&l_2037}}};
            uint32_t l_2047 = 0x8E9A2B37L;
            float l_2081 = 0xF.90EF2Ap-78;
            int32_t l_2104[4];
            int16_t l_2106 = 0x0A22L;
            int32_t l_2107[8];
            int32_t *l_2118 = (void*)0;
            int32_t *l_2119 = &g_156;
            int32_t *l_2120 = &g_236;
            int32_t *l_2121 = &l_2113[0];
            int32_t *l_2122 = &g_156;
            int32_t *l_2123 = &l_2104[1];
            int32_t *l_2124 = &l_2104[2];
            int32_t *l_2125 = (void*)0;
            int32_t *l_2126 = &l_2104[0];
            int32_t *l_2127 = &l_38;
            int32_t *l_2128 = &g_823;
            int32_t *l_2129[4] = {&g_727,&g_727,&g_727,&g_727};
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_2104[i] = 0x3048569BL;
            for (i = 0; i < 8; i++)
                l_2107[i] = 9L;
            for (l_38 = 3; (l_38 >= 0); l_38 -= 1)
            { /* block id: 24 */
                uint32_t *l_84[9] = {&g_85,&g_85,&g_85,&g_85,&g_85,&g_85,&g_85,&g_85,&g_85};
                int32_t l_650 = (-1L);
                int16_t *l_1654 = (void*)0;
                int16_t *l_1655 = (void*)0;
                int16_t *l_1656[4];
                int32_t l_2041 = 1L;
                int32_t l_2046[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1656[i] = &g_1476[4];
            }
            l_2131--;
        }
        p_22 = ((g_1269 , (void*)0) != (((g_2134 , ((void*)0 == g_2135)) && ((safe_mul_func_uint16_t_u_u(0x7AA1L, (safe_mul_func_int8_t_s_s((((l_2142[0][6] && ((*l_2149) |= ((*g_240) = ((((-2L) ^ (((((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_s(l_38, 0)), 9)), 10)) , (*l_77)) > 0xF2L) , (**l_76)) > p_21)) <= (*g_240)) , 4294967291UL)))) , 0L) < p_21), l_2150)))) ^ p_21)) , (*g_1630)));
        if ((((((safe_lshift_func_uint8_t_u_s((safe_lshift_func_uint16_t_u_u((((((p_21 , (((*l_2080) = l_2059[0]) == ((g_1904.f8 = (((l_2130 || p_21) && p_21) | ((safe_mod_func_uint16_t_u_u((l_2114 ^= ((((((l_2157[0][3][1] , ((*g_240) |= (((*l_77) != (safe_mod_func_uint32_t_u_u(p_21, (safe_sub_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((safe_mod_func_int16_t_s_s(0xA46CL, (-5L))), (*p_23))), p_21)) && 0x7623520EL), p_21))))) <= (-10L)))) == (***g_953)) <= (***g_1666)) != 0xBF0FE86FL) || (***g_1666)) | (**l_76))), (-2L))) < p_21))) ^ p_21))) , l_2150) <= 18446744073709551611UL) >= (**l_76)) == l_2169[0]), p_21)), p_21)) | g_999) | p_21) < (**l_76)) && (-2L)))
        { /* block id: 980 */
            int32_t *l_2179[3];
            int8_t ***l_2233 = &l_2184;
            int8_t ****l_2232[2][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0}};
            int i, j;
            for (i = 0; i < 3; i++)
                l_2179[i] = &g_34;
            for (g_1558.f2 = (-29); (g_1558.f2 != (-20)); g_1558.f2 = safe_add_func_int8_t_s_s(g_1558.f2, 9))
            { /* block id: 983 */
                uint32_t * const ****l_2192[8][4][6] = {{{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,(void*)0},{&l_2191,&l_2191,(void*)0,&l_2191,&l_2191,&l_2191},{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191},{(void*)0,&l_2191,&l_2191,(void*)0,&l_2191,&l_2191}},{{&l_2191,(void*)0,(void*)0,&l_2191,(void*)0,(void*)0},{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,(void*)0},{&l_2191,(void*)0,&l_2191,&l_2191,&l_2191,&l_2191},{&l_2191,(void*)0,(void*)0,&l_2191,&l_2191,&l_2191}},{{(void*)0,&l_2191,&l_2191,&l_2191,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2191,&l_2191,(void*)0},{(void*)0,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191},{(void*)0,&l_2191,(void*)0,&l_2191,&l_2191,(void*)0}},{{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191},{(void*)0,&l_2191,&l_2191,(void*)0,(void*)0,&l_2191},{&l_2191,(void*)0,&l_2191,(void*)0,&l_2191,&l_2191},{&l_2191,(void*)0,&l_2191,&l_2191,&l_2191,(void*)0}},{{&l_2191,&l_2191,(void*)0,(void*)0,&l_2191,&l_2191},{(void*)0,&l_2191,&l_2191,&l_2191,&l_2191,(void*)0},{&l_2191,&l_2191,(void*)0,&l_2191,&l_2191,&l_2191},{(void*)0,&l_2191,(void*)0,&l_2191,&l_2191,&l_2191}},{{&l_2191,(void*)0,&l_2191,&l_2191,&l_2191,(void*)0},{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191},{&l_2191,(void*)0,&l_2191,&l_2191,&l_2191,&l_2191},{(void*)0,&l_2191,&l_2191,&l_2191,&l_2191,(void*)0}},{{&l_2191,&l_2191,&l_2191,(void*)0,&l_2191,(void*)0},{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191},{(void*)0,&l_2191,&l_2191,(void*)0,&l_2191,&l_2191},{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191}},{{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191},{&l_2191,&l_2191,&l_2191,(void*)0,&l_2191,&l_2191},{&l_2191,&l_2191,(void*)0,&l_2191,&l_2191,(void*)0},{&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191}}};
                int32_t l_2221 = (-4L);
                uint8_t ***l_2231[5][6][5] = {{{&g_315[6],&g_315[3],&g_315[3],(void*)0,&g_315[0]},{(void*)0,&g_315[3],&g_315[4],&g_315[3],(void*)0},{&g_315[3],&g_315[3],(void*)0,&g_315[3],(void*)0},{&g_315[4],&g_315[3],&g_315[3],&g_315[4],&g_315[0]},{&g_315[6],&g_315[4],(void*)0,(void*)0,&g_315[3]},{&g_315[5],&g_315[6],&g_315[0],(void*)0,&g_315[6]}},{{&g_315[4],&g_315[3],&g_315[3],(void*)0,&g_315[3]},{&g_315[3],(void*)0,&g_315[3],&g_315[4],(void*)0},{&g_315[0],&g_315[6],&g_315[5],&g_315[3],(void*)0},{&g_315[6],&g_315[3],&g_315[5],&g_315[3],&g_315[5]},{(void*)0,(void*)0,&g_315[3],(void*)0,&g_315[3]},{(void*)0,&g_315[3],&g_315[3],&g_315[0],(void*)0}},{{(void*)0,&g_315[3],&g_315[0],&g_315[3],&g_315[3]},{&g_315[3],&g_315[3],(void*)0,(void*)0,(void*)0},{&g_315[3],(void*)0,&g_315[3],&g_315[3],(void*)0},{&g_315[6],&g_315[3],(void*)0,(void*)0,&g_315[3]},{&g_315[6],&g_315[6],&g_315[4],&g_315[6],&g_315[6]},{&g_315[3],(void*)0,&g_315[3],&g_315[3],(void*)0}},{{&g_315[3],&g_315[3],&g_315[3],&g_315[4],&g_315[3]},{(void*)0,&g_315[6],(void*)0,(void*)0,(void*)0},{(void*)0,&g_315[4],&g_315[5],&g_315[0],&g_315[6]},{(void*)0,&g_315[3],&g_315[3],(void*)0,&g_315[3]},{&g_315[3],&g_315[5],(void*)0,&g_315[3],&g_315[3]},{&g_315[3],&g_315[3],&g_315[3],(void*)0,&g_315[3]}},{{&g_315[0],(void*)0,&g_315[5],&g_315[3],&g_315[3]},{&g_315[3],&g_315[0],&g_315[3],&g_315[3],&g_315[3]},{&g_315[0],&g_315[3],&g_315[5],&g_315[3],&g_315[5]},{&g_315[3],&g_315[5],&g_315[3],&g_315[3],&g_315[0]},{&g_315[3],&g_315[3],(void*)0,&g_315[3],&g_315[0]},{&g_315[4],&g_315[3],&g_315[4],&g_315[4],&g_315[3]}}};
                uint8_t l_2274 = 0x96L;
                int i, j, k;
                for (l_2130 = 0; (l_2130 < (-14)); --l_2130)
                { /* block id: 986 */
                    uint8_t l_2176 = 0UL;
                    for (g_999 = 0; (g_999 >= 9); ++g_999)
                    { /* block id: 989 */
                        --l_2176;
                    }
                    if ((*g_30))
                        break;
                    return l_2179[0];
                }
                for (g_1558.f5 = (-20); (g_1558.f5 >= (-22)); g_1558.f5--)
                { /* block id: 997 */
                    int16_t l_2182[2];
                    int8_t ** const **l_2187 = &g_2185;
                    int32_t l_2272 = (-6L);
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2182[i] = 0xDF33L;
                    (*g_30) &= ((l_2182[0] , l_2183[6]) != ((*l_2187) = g_2185));
                }
            }
        }
        else
        { /* block id: 1036 */
            uint32_t l_2293 = 18446744073709551613UL;
            uint16_t *l_2342[3][3] = {{&g_481,&g_102[6][1],&g_102[6][1]},{&g_481,&g_102[6][1],&g_102[6][1]},{&g_481,&g_102[6][1],&g_102[6][1]}};
            const int32_t *l_2371 = &l_2130;
            int32_t l_2374 = (-8L);
            int32_t l_2376[1][1];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                    l_2376[i][j] = 0x57DCB255L;
            }
            if ((((safe_mul_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(((void*)0 != &g_2276), (safe_div_func_int16_t_s_s(1L, (safe_mul_func_uint8_t_u_u((**l_76), ((*l_2080) = p_21))))))), 0x60L)) < (((safe_unary_minus_func_int16_t_s((((0xC97BL & (l_2222[6][5] , (safe_mod_func_uint8_t_u_u(0x85L, (***g_1666))))) || (*p_23)) >= 0x411C46E9L))) || l_2293) || p_21)) | l_2293))
            { /* block id: 1038 */
                return p_23;
            }
            else
            { /* block id: 1040 */
                int8_t *** const *l_2319 = &l_2183[4];
                int8_t *** const **l_2318 = &l_2319;
                int32_t l_2320[10] = {0x6CA65F7BL,(-1L),(-2L),(-2L),(-1L),0x6CA65F7BL,(-1L),(-2L),(-2L),(-1L)};
                int16_t l_2322 = (-2L);
                int32_t l_2323[3];
                int32_t ****l_2325 = (void*)0;
                int32_t *****l_2324 = &l_2325;
                int64_t *l_2329 = &g_467.f2;
                const int32_t *****l_2390 = &g_2389;
                int i;
                for (i = 0; i < 3; i++)
                    l_2323[i] = 0L;
                if (((safe_mod_func_int64_t_s_s(((*l_2329) = (((g_2326 = (l_2324 = (((l_2323[0] ^= ((*l_77) && (safe_sub_func_int32_t_s_s((safe_mod_func_uint8_t_u_u(l_2142[0][6], 0x8CL)), ((safe_div_func_uint8_t_u_u((((+(0xC2D909217BF6768BLL == 0x9FDDC8CFAB99EA41LL)) >= p_21) || (safe_rshift_func_int8_t_s_u((safe_sub_func_int64_t_s_s((safe_sub_func_int8_t_s_s(8L, (safe_div_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s((safe_mod_func_int16_t_s_s((safe_add_func_uint64_t_u_u(((l_2318 != (((((l_2320[3] | p_21) , (void*)0) == l_2321) >= 0x944D75A98C41FAE5LL) , &g_2276)) , 18446744073709551615UL), l_2322)), p_21)), 3UL)) == 18446744073709551615UL), (-1L))))), g_1785[5].f5)), 3))), l_2222[7][6])) >= 0x5EL))))) , (*g_240)) , l_2324))) == l_2328) | 0x4AL)), p_21)) & p_21))
                { /* block id: 1045 */
                    float l_2343 = 0x3.DA1790p-27;
                    uint64_t *l_2345 = (void*)0;
                    uint64_t *l_2346 = (void*)0;
                    uint64_t *l_2347[6] = {&g_1483[1][3][0],&l_2157[0][3][1],&g_1483[1][3][0],&g_1483[1][3][0],&l_2157[0][3][1],&g_1483[1][3][0]};
                    uint32_t **** const l_2366 = &g_1631;
                    uint32_t **** const *l_2365[3];
                    uint8_t *l_2370 = &g_1830;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_2365[i] = &l_2366;
                    if ((safe_mod_func_int64_t_s_s(((safe_lshift_func_int16_t_s_u(l_2114, 6)) != (safe_lshift_func_uint16_t_u_u((p_22 , (safe_mod_func_uint64_t_u_u(((void*)0 == &g_1599), (safe_lshift_func_int16_t_s_s((4294967289UL && ((g_1483[1][5][0] &= (((safe_sub_func_uint16_t_u_u((0x2FL & ((l_2342[0][2] == l_2342[0][1]) , (((**g_688) ^ (*p_23)) != p_21))), g_1558.f7)) , g_2344) , g_2134.f5)) , 0xEEF0BA7FL)), 3))))), l_2293))), 0x0FE9C7A3676E010ELL)))
                    { /* block id: 1047 */
                        uint32_t l_2364 = 18446744073709551606UL;
                        l_2114 = ((((safe_mod_func_uint32_t_u_u((((0xF4A0F1AA3ADC1995LL > p_21) >= ((*l_2294) = ((g_156 , (((!(l_2293 >= (safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u(8UL, (((safe_unary_minus_func_int8_t_s((((((safe_div_func_uint32_t_u_u(p_21, 0x1DA439C5L)) && l_2293) , (((safe_lshift_func_uint16_t_u_s((((****g_180) &= (safe_mod_func_uint64_t_u_u(((safe_div_func_int64_t_s_s((((g_297[2][2][0].f7 ^ l_2293) == 0x7E693B58L) && g_1765), g_1904.f8)) >= (**l_76)), l_2364))) , 0x7DDEL), 3)) , l_2365[1]) == l_2367)) <= p_21) & l_2169[0]))) != p_21) == g_1558.f6))), 9)))) , l_2222[6][1]) || g_823)) || 1UL))) , p_21), l_2130)) , g_2369) , p_21) < 18446744073709551612UL);
                        return p_23;
                    }
                    else
                    { /* block id: 1052 */
                        (**g_527) = (((*l_76) = &l_2114) == ((((*l_2080) = 1L) , ((*l_2294) && (-6L))) , p_23));
                        (**l_76) = (****g_180);
                    }
                }
                else
                { /* block id: 1058 */
                    uint64_t l_2384[6][2][8] = {{{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL},{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL}},{{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL},{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL}},{{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL},{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL}},{{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL},{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL}},{{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL},{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL}},{{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL},{0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL,0xE3C5CC93481BEBF3LL}}};
                    int i, j, k;
                    for (g_105 = 0; (g_105 <= 40); g_105++)
                    { /* block id: 1061 */
                        int32_t *l_2375 = (void*)0;
                        int32_t *l_2377 = &l_2320[6];
                        int32_t *l_2378 = &l_2114;
                        int32_t *l_2379 = &l_2114;
                        int32_t *l_2380 = &g_156;
                        int32_t *l_2381 = (void*)0;
                        int32_t *l_2382 = &l_2376[0][0];
                        int32_t *l_2383[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_2383[i] = &l_2323[0];
                        l_2384[5][0][7]--;
                    }
                    return p_23;
                }
                (*g_183) = (&g_1246 != l_2371);
                (*l_2294) = (safe_mul_func_uint8_t_u_u((((((*l_2390) = g_2389) == (void*)0) && (safe_div_func_int16_t_s_s(l_2222[3][0], p_21))) , (*l_2294)), p_21));
            }
        }
    }
    if (((**g_182) |= ((*p_23) && l_2393)))
    { /* block id: 1073 */
        int32_t *l_2394 = &l_2130;
        int32_t *l_2395[5] = {&l_2130,&l_2130,&l_2130,&l_2130,&l_2130};
        int32_t l_2399 = 0x321FB49EL;
        uint32_t l_2401 = 0x6558D536L;
        int i;
        l_2401++;
        (*p_23) = (safe_mod_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u(p_21, 7)), ((safe_lshift_func_int16_t_s_s((((safe_unary_minus_func_uint64_t_u(g_140.f6)) , ((safe_rshift_func_uint8_t_u_u((p_21 == (p_21 <= ((safe_mod_func_uint8_t_u_u((((((*g_240) = ((((safe_mod_func_int16_t_s_s(((safe_sub_func_int64_t_s_s(p_21, ((g_297[2][2][0].f2 , (l_2130 = (safe_sub_func_uint32_t_u_u((safe_sub_func_uint16_t_u_u((((g_2369.f2 , (****g_180)) , l_2425) != g_2426), 0xA314L)), l_38)))) , l_2114))) ^ p_21), 2L)) , l_2427) <= g_1904.f5) , 4294967290UL)) >= p_21) , l_2400) ^ g_1558.f8), l_2169[0])) > 0L))), 7)) > l_2115)) > g_1483[1][4][1]), 14)) , (***g_1666))));
        l_2428 = (***g_952);
        (****g_180) &= (safe_lshift_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((-1L), 0xEA7CL)), 15));
    }
    else
    { /* block id: 1080 */
        int32_t *l_2433 = &l_2400;
        int32_t *l_2434[1][10][1] = {{{&g_34},{&g_34},{(void*)0},{&l_2396},{(void*)0},{&g_34},{&g_34},{(void*)0},{&l_2396},{(void*)0}}};
        int8_t ***l_2438 = (void*)0;
        int8_t ***l_2440 = &l_2184;
        int8_t ****l_2439 = &l_2440;
        int i, j, k;
        ++l_2435;
        l_2434[0][5][0] = func_61((((((l_2438 == ((*l_2439) = &l_2184)) < (safe_lshift_func_int16_t_s_u(p_21, 9))) == (*l_2428)) != ((safe_mul_func_int8_t_s_s((*l_2428), 1L)) == 0x72L)) || ((safe_lshift_func_uint8_t_u_s((*l_2428), 7)) | ((0UL ^ (*l_2428)) <= (*l_2428)))), (*l_2428), p_21, p_21);
    }
    if (((((safe_add_func_uint32_t_u_u((p_21 , (safe_add_func_uint16_t_u_u((0UL ^ (*l_2428)), ((*l_2453) = (safe_mul_func_uint16_t_u_u(p_21, (*l_2428))))))), (g_881 > (g_1854.f7 , 0xBB08A425773C5E6ELL)))) == ((***g_2426) & l_2454)) == 248UL) <= 0x3ACCL))
    { /* block id: 1086 */
        return p_23;
    }
    else
    { /* block id: 1088 */
        uint8_t l_2458 = 0x40L;
        const uint32_t ***l_2576 = (void*)0;
        int32_t l_2582 = 0x0DDD9CD4L;
        int32_t l_2586 = (-1L);
        int32_t l_2591 = 0xBF12959AL;
        int32_t l_2592 = 3L;
        int32_t l_2595 = 4L;
        int32_t l_2599 = 0L;
        int32_t l_2602[8] = {1L,0xBEC372B4L,1L,1L,0xBEC372B4L,1L,1L,0xBEC372B4L};
        int32_t l_2644[7][7][1] = {{{1L},{(-1L)},{1L},{1L},{(-1L)},{1L},{1L}},{{0L},{(-1L)},{(-1L)},{0L},{(-1L)},{(-1L)},{0L}},{{(-1L)},{(-1L)},{0L},{(-1L)},{(-1L)},{0L},{(-1L)}},{{(-1L)},{0L},{(-1L)},{(-1L)},{0L},{(-1L)},{(-1L)}},{{0L},{(-1L)},{(-1L)},{0L},{(-1L)},{(-1L)},{0L}},{{(-1L)},{(-1L)},{0L},{(-1L)},{(-1L)},{0L},{(-1L)}},{{(-1L)},{0L},{(-1L)},{(-1L)},{0L},{(-1L)},{(-1L)}}};
        int16_t l_2652[8] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
        struct S0 ***l_2659 = &g_1496;
        int64_t l_2801 = (-1L);
        int16_t * const *l_2872 = &l_2453;
        int16_t * const **l_2871 = &l_2872;
        int16_t * const ***l_2870 = &l_2871;
        int32_t l_2882 = (-1L);
        uint32_t l_2929 = 0xCD8F38D1L;
        float l_2939 = 0x1.1p+1;
        const uint8_t l_3007 = 0x13L;
        float **l_3009 = &g_862;
        int16_t l_3119 = 0xBB66L;
        uint8_t l_3120[7];
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_3120[i] = 0x53L;
        if ((*p_23))
        { /* block id: 1089 */
            int64_t l_2457 = 1L;
            float **l_2473 = &g_862;
            int32_t l_2524 = 0xD70EE71DL;
            int32_t l_2598 = 0x36227FA2L;
            int32_t l_2600 = 0L;
            int32_t l_2605 = 0L;
            uint32_t **l_2631[8] = {&g_240,&g_240,&g_240,&g_240,&g_240,&g_240,&g_240,&g_240};
            uint16_t *l_2638 = &g_1269;
            int32_t l_2646 = 0xF547D1AEL;
            int32_t l_2648 = 0L;
            int32_t l_2649 = 0L;
            int32_t l_2650[9][3][6] = {{{1L,0x8E6F13F4L,0x084BBD9AL,1L,0xB7F529EDL,0xA61EF3BDL},{0x6B6E5B2EL,1L,0x824C827FL,0x824C827FL,1L,0x6B6E5B2EL},{0x824C827FL,1L,0x6B6E5B2EL,(-5L),0xB7F529EDL,0x084BBD9AL}},{{0x084BBD9AL,0x8E6F13F4L,1L,0x8E6F13F4L,0x084BBD9AL,1L},{0x084BBD9AL,0x824C827FL,0x8E6F13F4L,(-5L),0x91106ACAL,0x91106ACAL},{0x824C827FL,0xB7F529EDL,0xB7F529EDL,0x824C827FL,1L,0x91106ACAL}},{{0x6B6E5B2EL,0x91106ACAL,0x8E6F13F4L,1L,(-5L),1L},{1L,(-1L),1L,0xA61EF3BDL,(-5L),0x084BBD9AL},{0x8E6F13F4L,0x91106ACAL,0x6B6E5B2EL,1L,1L,0x6B6E5B2EL}},{{0xB7F529EDL,0xB7F529EDL,0x824C827FL,1L,0x91106ACAL,0xA61EF3BDL},{0x8E6F13F4L,0x824C827FL,0x084BBD9AL,0xA61EF3BDL,0x084BBD9AL,0x824C827FL},{1L,0x8E6F13F4L,0x084BBD9AL,1L,0xB7F529EDL,0xA61EF3BDL}},{{0x6B6E5B2EL,1L,0x824C827FL,0x824C827FL,1L,0x6B6E5B2EL},{0x824C827FL,1L,0x6B6E5B2EL,(-5L),0xB7F529EDL,0x084BBD9AL},{0x084BBD9AL,0x8E6F13F4L,1L,0x8E6F13F4L,0x084BBD9AL,1L}},{{0x084BBD9AL,0x824C827FL,0x8E6F13F4L,(-5L),0x91106ACAL,0x91106ACAL},{0x824C827FL,0xB7F529EDL,0xB7F529EDL,0x824C827FL,1L,0x91106ACAL},{0x6B6E5B2EL,0x91106ACAL,0x8E6F13F4L,1L,(-5L),1L}},{{1L,(-1L),1L,0xA61EF3BDL,(-5L),0x084BBD9AL},{0x8E6F13F4L,0x91106ACAL,0x6B6E5B2EL,1L,1L,0x6B6E5B2EL},{0xB7F529EDL,0xB7F529EDL,0x824C827FL,1L,0x91106ACAL,0xA61EF3BDL}},{{0x8E6F13F4L,0x824C827FL,0x084BBD9AL,0xA61EF3BDL,0x084BBD9AL,0x824C827FL},{1L,(-1L),0x6B6E5B2EL,0x824C827FL,0xA61EF3BDL,0x8E6F13F4L},{1L,0x824C827FL,0xB7F529EDL,0xB7F529EDL,0x824C827FL,1L}},{{0xB7F529EDL,0x824C827FL,1L,0x91106ACAL,0xA61EF3BDL,0x6B6E5B2EL},{0x6B6E5B2EL,(-1L),1L,(-1L),0x6B6E5B2EL,0x824C827FL},{0x6B6E5B2EL,0xB7F529EDL,(-1L),0x91106ACAL,0x084BBD9AL,0x084BBD9AL}}};
            uint32_t l_2669[8][3] = {{0xF001BC32L,0xF001BC32L,0x7A9B2710L},{18446744073709551609UL,18446744073709551609UL,0UL},{0xF001BC32L,0xF001BC32L,0x7A9B2710L},{18446744073709551609UL,18446744073709551609UL,0UL},{0xF001BC32L,0xF001BC32L,0x7A9B2710L},{18446744073709551609UL,18446744073709551609UL,0UL},{0xF001BC32L,0xF001BC32L,0x7A9B2710L},{18446744073709551609UL,18446744073709551609UL,0UL}};
            uint8_t ***l_2670 = &g_315[3];
            uint32_t l_2693 = 0x7628BC17L;
            int16_t l_2694 = 0x683EL;
            int32_t *l_2700 = &l_2648;
            int32_t *l_2701 = (void*)0;
            int32_t *l_2702 = &g_1180;
            int32_t *l_2703 = &l_2130;
            int32_t *l_2704 = &l_2396;
            int32_t *l_2705 = (void*)0;
            int32_t *l_2706 = (void*)0;
            int32_t *l_2707[9] = {(void*)0,&l_2645,(void*)0,&l_2645,(void*)0,&l_2645,(void*)0,&l_2645,(void*)0};
            int8_t *** const * const l_2743 = &l_2183[6];
            const float *l_2793 = (void*)0;
            const float **l_2792[1];
            int32_t *** const *l_2825 = (void*)0;
            int32_t *** const **l_2824[2][4] = {{&l_2825,&l_2825,(void*)0,&l_2825},{&l_2825,&l_2825,&l_2825,&l_2825}};
            struct S0 **l_2841 = &g_299;
            const uint32_t l_2851[1][2] = {{0x6E2CB0DAL,0x6E2CB0DAL}};
            int16_t ****l_2869 = &l_2788;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_2792[i] = &l_2793;
            for (g_174.f8 = 0; (g_174.f8 <= 9); g_174.f8 += 1)
            { /* block id: 1092 */
                int32_t *l_2455 = &l_2400;
                int32_t *l_2456[4][2][3] = {{{&g_1180,&g_1180,&l_2400},{&l_2400,&l_2400,&l_2400}},{{&g_1180,&l_2400,&l_2400},{&l_2400,&l_2400,&l_2400}},{{&g_1180,&g_1180,&l_2400},{&l_2400,&l_2400,&l_2400}},{{&g_1180,&l_2400,&l_2400},{&l_2400,&l_2400,&l_2400}}};
                int32_t l_2468 = 0xAC95D2E3L;
                uint64_t l_2483[2];
                int32_t *l_2497 = &g_1009[2].f5;
                float l_2498 = 0x8.C1C120p-78;
                int8_t **l_2556 = (void*)0;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_2483[i] = 0UL;
                --l_2458;
                for (g_1558.f8 = 0; (g_1558.f8 <= 0); g_1558.f8 += 1)
                { /* block id: 1096 */
                    int16_t *l_2463 = &g_1476[3];
                    int32_t l_2469 = (-1L);
                    for (g_1245 = 8; (g_1245 >= 2); g_1245 -= 1)
                    { /* block id: 1099 */
                        uint64_t *l_2466 = &g_1483[1][4][0];
                        int i, j, k;
                        (**g_182) |= (safe_rshift_func_uint16_t_u_s((((((((void*)0 == l_2463) != (((l_2468 = (safe_lshift_func_uint8_t_u_s(((((g_88[g_1558.f8][(g_1558.f8 + 7)][g_1558.f8] & ((((((**g_2426) = (**g_2426)) != ((((*l_2466) = (((**g_1666) = g_2079[(g_1558.f8 + 1)]) == g_2079[g_1558.f8])) > 0L) , l_2455)) >= (g_88[(g_1558.f8 + 2)][g_1245][g_1558.f8] > p_21)) , g_2467[5]) , g_140.f1)) == 4294967291UL) != p_21) & 0x82L), p_21))) , &g_1630) != &g_685)) | p_21) ^ l_2457) >= 0x3464L) > l_2469), p_21));
                    }
                    if (((safe_sub_func_int64_t_s_s(p_21, l_2457)) , ((**g_182) ^= 0xA5ACB5C8L)))
                    { /* block id: 1107 */
                        int64_t *l_2472 = (void*)0;
                        (*p_23) = ((0UL > ((*l_2455) |= p_21)) || (***g_953));
                        if ((*p_23))
                            break;
                        return p_23;
                    }
                    else
                    { /* block id: 1112 */
                        int32_t **l_2496 = (void*)0;
                        float *l_2506 = (void*)0;
                        float *l_2507 = &l_2498;
                        int32_t l_2521[10] = {0xF7BF18AFL,0xF7BF18AFL,7L,0x04029F0DL,7L,0xF7BF18AFL,0xF7BF18AFL,7L,0x04029F0DL,7L};
                        int i, j;
                        (*p_23) ^= ((void*)0 == l_2473);
                        (*p_23) = (p_21 & (safe_sub_func_int8_t_s_s((l_2476 <= (((safe_add_func_int16_t_s_s((safe_mod_func_int8_t_s_s(((safe_sub_func_int32_t_s_s(l_2483[1], ((**g_182) = ((*l_2455) &= (safe_add_func_int64_t_s_s((safe_sub_func_int64_t_s_s(g_2369.f8, ((p_21 == p_21) & (+((*g_461) == ((safe_lshift_func_int16_t_s_u((~(safe_mod_func_uint8_t_u_u(p_21, (safe_div_func_uint64_t_u_u(((l_2497 = l_2456[3][1][1]) != l_2456[0][1][0]), 0x0C1F65EAB8FAD2F3LL))))), p_21)) || 0L)))))), 0x9A7922CF8E40FC4ELL)))))) ^ g_1009[2].f2), p_21)), l_2469)) && 248UL) , 0x5C731E48L)), 0UL)));
                        (*l_2455) |= (((((((void*)0 != (**g_2275)) <= ((((***g_2426) = p_21) >= ((*g_183) = ((((!l_37[(g_1558.f8 + 1)][(g_1558.f8 + 3)]) == (safe_sub_func_float_f_f(((*l_2507) = (((0x4.9p-1 != (0x7.5240E3p-10 == (safe_add_func_float_f_f((((safe_div_func_float_f_f((-0x9.4p-1), ((0xD.F1853Cp-48 <= p_21) < (*l_2428)))) == l_2458) < p_21), 0x9.0p-1)))) != (*l_2428)) == (-0x7.Fp+1))), p_21))) , 6UL) || p_21))) | 0x76CEEC754A357ADCLL)) , p_21) , 9UL) > 0x964318E1L) | p_21);
                        p_22 = (safe_div_func_float_f_f((p_21 <= ((safe_mul_func_float_f_f((0x5.48304Fp+8 == (-(((safe_sub_func_float_f_f((safe_add_func_float_f_f(0x4.E700BAp-66, (safe_sub_func_float_f_f(l_2457, (!(-((**g_527) = (l_2521[5] , p_22)))))))), 0x2.D4ABC4p-30)) > p_21) >= (((*l_2507) = (safe_sub_func_float_f_f(p_22, 0x7.2p-1))) <= l_2469)))), 0xE.E9A042p+32)) > p_22)), p_21));
                    }
                    if (l_2458)
                        break;
                }
                for (l_2457 = 0; (l_2457 <= 2); l_2457 += 1)
                { /* block id: 1130 */
                    const uint32_t ****l_2538 = (void*)0;
                    const uint32_t ****l_2539 = &g_2535;
                    int32_t l_2548 = 0xE0B7BABDL;
                    int32_t l_2549 = 0L;
                    uint64_t l_2606 = 5UL;
                    int i;
                    for (l_2398 = 0; (l_2398 <= 7); l_2398 += 1)
                    { /* block id: 1133 */
                        uint8_t l_2525 = 4UL;
                        l_2525--;
                    }
                    if (l_2458)
                        continue;
                    l_2549 ^= ((safe_unary_minus_func_uint8_t_u((safe_sub_func_int16_t_s_s((safe_sub_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((*g_574) , (***g_1666)), l_2524)), 255UL)), (((((((((((*l_2539) = g_2535) == &g_2536) <= (safe_sub_func_int8_t_s_s((safe_mul_func_int16_t_s_s((safe_div_func_int8_t_s_s((l_2169[l_2457] &= (((p_21 && (safe_lshift_func_int8_t_s_s((&g_654 == &g_1324), p_21))) , (*p_23)) ^ (*p_23))), (-1L))), 0xD0E3L)), l_2548))) > p_21) < (-9L)) | (*l_2428)) >= (*l_2428)) != (***g_953)) < p_21) >= (*p_23)))))) != l_2524);
                    for (g_1479 = 0; (g_1479 <= 3); g_1479 += 1)
                    { /* block id: 1142 */
                        uint64_t *l_2550[5][10] = {{&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0]},{&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0]},{&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0]},{&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0]},{&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0],&l_2483[0]}};
                        uint32_t l_2561 = 4294967287UL;
                        uint8_t **l_2573 = &g_316;
                        uint16_t *l_2581 = &g_102[6][1];
                        int i, j;
                        (*l_2455) &= ((g_1483[1][5][0] ^= g_140.f5) < (((*l_2080) = (safe_div_func_int32_t_s_s((((+(p_21 == ((safe_rshift_func_uint16_t_u_s(0xAA81L, ((0x1.7p-1 >= (((*g_2185) != (l_2556 = &g_2079[(l_2457 + 1)])) , (l_2524 = l_37[(g_1479 + 2)][(l_2457 + 1)]))) , ((*l_2453) = (safe_rshift_func_uint16_t_u_s(0x78DFL, (((safe_rshift_func_int16_t_s_u((((-7L) < l_2549) < l_2561), 8)) == (*p_23)) , p_21))))))) < p_21))) , p_21) > 0x205EL), 0xA74ADA1DL))) == p_21));
                        l_2524 ^= (safe_mod_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s(((**l_2184) = (safe_rshift_func_uint16_t_u_s(((*l_2581) = (~((safe_rshift_func_uint16_t_u_u((*l_2455), (p_21 , (safe_rshift_func_int8_t_s_u((((((*g_1666) = l_2573) != ((safe_mul_func_uint8_t_u_u((((*g_316) <= (((((*l_2539) = l_2576) != (void*)0) , ((safe_sub_func_int32_t_s_s((*p_23), ((((**g_460) , (safe_rshift_func_uint16_t_u_s((p_21 < 0xEC0EL), 13))) == (*p_23)) > (***g_2426)))) != l_2458)) || l_2561)) || 0x3951L), p_21)) , (void*)0)) | 0x33L) < 0L), l_2458))))) ^ p_21))), (*l_2428)))), p_21)), (*g_461)));
                    }
                    for (l_2400 = 3; (l_2400 >= 0); l_2400 -= 1)
                    { /* block id: 1157 */
                        float l_2584 = 0x6.29C4DDp+44;
                        int32_t l_2585 = 1L;
                        int32_t l_2587 = 0x5FAFE44FL;
                        int32_t l_2588 = 0x6A81D68CL;
                        int32_t l_2589 = 7L;
                        int32_t l_2593 = 0x34832001L;
                        int32_t l_2594 = 0x643E267AL;
                        int32_t l_2597 = 0xD9805F63L;
                        int32_t l_2601 = 0x366113C5L;
                        int32_t l_2603 = 0x4C1CE2A3L;
                        int32_t l_2604[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
                        int i;
                        l_2606--;
                    }
                }
                for (l_2393 = 2; (l_2393 <= 7); l_2393 += 1)
                { /* block id: 1163 */
                    uint32_t l_2611 = 0UL;
                    int64_t *l_2616[10] = {(void*)0,&g_1558.f2,&g_1558.f2,(void*)0,&g_1558.f2,&g_1558.f2,(void*)0,&g_1558.f2,&g_1558.f2,(void*)0};
                    int32_t l_2617 = 0xBC4EB09AL;
                    int i;
                    if (((safe_lshift_func_int8_t_s_s((l_2611 ^ 6UL), (0x48L != ((*l_2428) , ((+(p_21 > ((safe_rshift_func_int8_t_s_s(0xA9L, 2)) , (+p_21)))) , p_21))))) , ((l_2582 = (l_2617 = g_105)) >= g_1823)))
                    { /* block id: 1166 */
                        uint32_t l_2618 = 4294967292UL;
                        l_2618--;
                    }
                    else
                    { /* block id: 1168 */
                        if ((***g_181))
                            break;
                        if (l_2598)
                            break;
                        (*g_460) = &l_2605;
                    }
                    (*g_30) ^= (&g_1631 == (void*)0);
                    if ((*p_23))
                        break;
                }
            }
            if ((safe_mod_func_uint16_t_u_u((g_2623 , (safe_mul_func_int8_t_s_s((l_2600 = (safe_sub_func_int64_t_s_s(p_21, (safe_mul_func_int8_t_s_s(((!(((**g_1630) == (l_2631[7] = (void*)0)) >= (safe_mod_func_int64_t_s_s(0L, 0x38541C8ED9F2ED6ALL)))) && (safe_add_func_int16_t_s_s(((((*p_23) |= ((****g_180) = ((p_23 != (void*)0) < ((((*l_2638) = ((l_2398 |= (((safe_mul_func_int8_t_s_s(((**l_2184) = (-1L)), (*l_2428))) , (*l_2428)) != p_21)) , l_2605)) != 0xB0C6L) || 0x25B7L)))) & l_2600) & 0L), g_619))), 255UL))))), l_2605))), l_2524)))
            { /* block id: 1184 */
                int32_t *l_2639 = &g_1180;
                int32_t *l_2640 = &g_156;
                int32_t *l_2641 = (void*)0;
                int32_t *l_2642[4][7] = {{&l_2396,&l_38,&l_2396,&l_38,&l_2396,&l_38,&l_2396},{&l_2150,&g_823,&g_823,&l_2150,&l_2150,&g_823,&g_823},{&l_2602[2],&l_38,&l_2602[2],&l_38,&l_2602[2],&l_38,&l_2602[2]},{&l_2150,&l_2150,&g_823,&g_823,&l_2150,&l_2150,&g_823}};
                int32_t l_2643 = (-1L);
                float * const l_2667 = &g_2668;
                float * const *l_2666[7][9] = {{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667},{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667},{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667},{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667},{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667},{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667},{(void*)0,&l_2667,&l_2667,&l_2667,(void*)0,&l_2667,&l_2667,&l_2667,&l_2667}};
                float * const **l_2665 = &l_2666[3][8];
                uint64_t *l_2678 = &g_88[2][1][0];
                int64_t *l_2685 = (void*)0;
                int64_t *l_2686 = &g_2623.f2;
                uint8_t ****l_2691 = (void*)0;
                uint8_t ****l_2692 = &g_1666;
                int i, j;
                --g_2656;
                (**g_2327) = &l_2642[2][3];
                (**g_182) ^= ((l_2150 |= (((l_2650[4][1][5] , l_2659) != (void*)0) >= (safe_lshift_func_uint8_t_u_u((0x10D7L == l_2595), p_21)))) & (safe_rshift_func_uint16_t_u_s(7UL, ((*g_1361) == ((*l_2665) = (**g_1360))))));
                (****g_2327) = ((((l_2669[6][1] , l_2670) == ((*l_2692) = (((safe_lshift_func_int8_t_s_s(((!((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u(0x1CD0L, 7)), ((++(*l_2678)) || (safe_rshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u(l_2644[4][0][0], (((*l_2686) = ((*g_139) , 0xB215A1ACACCE184CLL)) ^ (safe_lshift_func_int16_t_s_s((l_2586 = (safe_lshift_func_uint16_t_u_s(0x869AL, 14))), 15))))), ((***g_1666) &= (2UL >= p_21))))))) , 0xD3425632B76836BCLL)) || p_21), 5)) & p_21) , (void*)0))) != 0x0AE3L) < l_2693);
            }
            else
            { /* block id: 1196 */
                uint64_t l_2697 = 18446744073709551614UL;
                (**g_182) ^= (l_2694 , (1L <= 0xB0718E8BL));
                for (g_797 = 3; (g_797 != 29); g_797++)
                { /* block id: 1200 */
                    (**g_182) &= (*p_23);
                    return p_23;
                }
                ++l_2697;
            }
            ++g_2715;
            for (g_2655 = 0; (g_2655 > (-29)); g_2655--)
            { /* block id: 1209 */
                int8_t l_2732 = 0xD9L;
                uint64_t *l_2742 = &g_1483[0][1][0];
                int32_t l_2744 = 0x65241338L;
                const struct S0 * const l_2844 = &g_1854;
                const struct S0 * const *l_2843 = &l_2844;
                if ((safe_mul_func_int8_t_s_s((l_2745 = (l_2744 = (safe_lshift_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s(((p_21 >= (((((((-3L) <= (-5L)) ^ (((****g_180) = ((safe_rshift_func_int8_t_s_u((safe_sub_func_int64_t_s_s((safe_lshift_func_int8_t_s_u(((**l_2184) |= l_2732), (l_2602[3] &= ((((safe_mod_func_int32_t_s_s(l_2735, ((*p_23) ^= ((p_21 < (safe_lshift_func_int16_t_s_u((p_21 | (safe_add_func_int16_t_s_s(0x24C6L, (safe_add_func_uint32_t_u_u((((((*l_2742) |= 18446744073709551614UL) == p_21) & p_21) , (**g_688)), (*g_461)))))), g_824))) <= 1UL)))) && (*l_2428)) || 0x01CBL) < 0x8A4EL)))), 0x24C56DE614161B19LL)), 2)) & p_21)) > p_21)) | p_21) , l_2743) != &g_2185) || p_21)) || g_1269), 2)) > l_2732), l_2599)))), 0x3FL)))
                { /* block id: 1217 */
                    int8_t ***l_2756 = (void*)0;
                    int8_t ****l_2755 = &l_2756;
                    uint8_t *** const *l_2763 = &g_1666;
                    int32_t l_2774 = 0x1C151A9AL;
                    uint8_t *l_2775 = &g_1479;
                    (***g_180) = (g_2746 , &l_2644[4][0][0]);
                    (*l_2703) = ((*l_2700) = (safe_mul_func_int16_t_s_s(((safe_add_func_uint32_t_u_u(((**g_239) &= 0x000606C9L), (safe_rshift_func_uint16_t_u_s((((*l_2775) ^= (((safe_rshift_func_uint16_t_u_s(((void*)0 == l_2755), 5)) | (((safe_add_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_s(((void*)0 != &g_2655), ((((**g_182) = (safe_add_func_uint8_t_u_u((l_2763 != (((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((l_2744 |= ((((safe_rshift_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((**l_2184) &= (0x4FC341C217ABD4A5LL | 0xA45A5E1E7725FC32LL)), (safe_div_func_int64_t_s_s((0xAADCFEEEL && (*l_2428)), p_21)))), (*g_574))) && l_2774) , (*g_2135)) != (void*)0)), l_2732)), p_21)) ^ p_21) , &l_2670)), (*l_2428)))) , 65526UL) != p_21))), 0x48B8FF58L)) > 0x58D9L) , 0xDCE98FE0L)) && l_2774)) , g_85), 12)))) > g_2623.f7), p_21)));
                }
                else
                { /* block id: 1226 */
                    const int32_t l_2776[4] = {0x87EB05F3L,0x87EB05F3L,0x87EB05F3L,0x87EB05F3L};
                    struct S0 **l_2800 = &g_299;
                    int32_t l_2819 = 8L;
                    uint32_t l_2821 = 2UL;
                    int64_t *l_2868[2];
                    uint32_t **l_2886 = &g_240;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2868[i] = &g_1558.f2;
                    for (g_823 = 0; (g_823 <= 7); g_823 += 1)
                    { /* block id: 1229 */
                        int16_t ***l_2791 = &l_2789;
                        int16_t ****l_2790 = &l_2791;
                        const float ***l_2794 = (void*)0;
                        const float ***l_2795 = &l_2792[0];
                        float *l_2797 = &l_37[6][0];
                        int32_t l_2798 = 0xF0EFE122L;
                        int32_t l_2799 = 0x7896ADE0L;
                        int i;
                        (*l_2703) |= l_2602[g_823];
                        if ((****g_180))
                            break;
                        l_2799 = (l_2776[0] <= (l_2744 = ((safe_mul_func_float_f_f((0xD.2E66A8p-12 > 0x0.7p+1), (safe_sub_func_float_f_f((l_2798 = ((*l_2797) = ((**g_527) = ((l_2602[g_823] = (p_22 = (!(safe_div_func_float_f_f(((safe_add_func_float_f_f((**g_527), ((safe_add_func_uint32_t_u_u(((p_21 = 0xD4L) , (((l_2788 != ((*l_2790) = (void*)0)) , ((**g_1360) != ((*l_2795) = l_2792[0]))) == g_2796)), (***g_1631))) , 0x1.Dp-1))) != l_2602[g_823]), l_2776[0]))))) <= 0x1.3p-1)))), l_2595)))) >= l_2799)));
                        (*g_30) &= (*p_23);
                    }
                    if ((l_2800 == (*g_606)))
                    { /* block id: 1244 */
                        float l_2812 = 0xA.7E1A15p-78;
                        int32_t l_2820 = 0x83ACA40FL;
                        (**g_527) = (l_2801 > (((0x9BE8B9BED272BF7BLL ^ (safe_div_func_int8_t_s_s(p_21, (((safe_mul_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((((l_2819 &= ((safe_add_func_int16_t_s_s((g_102[6][1] <= (safe_rshift_func_int8_t_s_u(p_21, 6))), ((((((safe_add_func_int16_t_s_s((safe_add_func_uint32_t_u_u((246UL > (safe_rshift_func_uint8_t_u_u(255UL, 1))), 0UL)), (((((-1L) < 18446744073709551615UL) == 0x7CA15AD2L) , 0UL) <= (-1L)))) == 65533UL) && p_21) ^ (**g_2278)) ^ p_21) || (*g_574)))) <= p_21)) , (*l_2428)) , (**g_688)), (*g_240))), 0xC3L)) || l_2820) & (*p_23))))) , l_2821) <= (-0x1.1p+1)));
                        (*g_183) = (*p_23);
                        (*l_2704) ^= ((((safe_add_func_uint16_t_u_u((&g_180 != l_2824[1][3]), ((***l_2788) &= (safe_mod_func_int32_t_s_s(0x1C15214DL, (*p_23)))))) , (p_21 ^ (+((*l_2742)++)))) > (*l_2428)) && (safe_rshift_func_int8_t_s_s(((g_104 < (safe_add_func_int64_t_s_s(((((***g_2426) <= ((safe_sub_func_uint8_t_u_u(l_2820, (-1L))) , p_21)) , (*l_2703)) || l_2744), 0L))) & 0x14A35671L), (**g_2278))));
                    }
                    else
                    { /* block id: 1251 */
                        float ****l_2842[9][10][2] = {{{&g_860[7][0],(void*)0},{(void*)0,(void*)0},{&g_860[7][0],&g_860[7][0]},{&g_860[7][0],&g_860[7][0]},{&g_860[7][0],&g_860[8][2]},{&g_860[3][1],(void*)0},{&g_860[7][0],&g_860[5][1]},{&g_860[8][2],&g_860[5][1]},{&g_860[7][0],&g_860[7][1]},{&g_860[7][0],&g_860[7][0]}},{{&g_860[5][1],(void*)0},{&g_860[7][0],&g_860[7][0]},{&g_860[4][0],&g_860[4][1]},{&g_860[7][0],&g_860[5][1]},{&g_860[4][0],(void*)0},{&g_860[7][0],&g_860[7][0]},{&g_860[0][0],&g_860[7][0]},{&g_860[8][2],&g_860[7][0]},{&g_860[5][1],(void*)0},{&g_860[4][1],&g_860[7][0]}},{{&g_860[7][0],&g_860[7][0]},{&g_860[1][0],&g_860[7][0]},{&g_860[5][1],&g_860[4][0]},{&g_860[7][0],&g_860[7][1]},{(void*)0,&g_860[1][0]},{(void*)0,&g_860[5][1]},{(void*)0,&g_860[7][0]},{&g_860[6][0],&g_860[7][0]},{(void*)0,(void*)0},{&g_860[7][1],(void*)0}},{{(void*)0,&g_860[7][0]},{&g_860[6][0],&g_860[7][0]},{(void*)0,&g_860[5][1]},{(void*)0,&g_860[1][0]},{(void*)0,&g_860[7][1]},{&g_860[7][0],&g_860[4][0]},{&g_860[5][1],&g_860[7][0]},{&g_860[1][0],&g_860[7][0]},{&g_860[7][0],&g_860[7][0]},{&g_860[4][1],(void*)0}},{{&g_860[5][1],&g_860[7][0]},{&g_860[8][2],&g_860[7][0]},{&g_860[0][0],&g_860[7][0]},{&g_860[7][0],(void*)0},{&g_860[4][0],&g_860[5][1]},{&g_860[7][0],&g_860[4][1]},{&g_860[4][0],&g_860[7][0]},{&g_860[7][0],(void*)0},{&g_860[5][1],&g_860[7][0]},{&g_860[7][0],&g_860[7][1]}},{{&g_860[7][0],&g_860[5][1]},{&g_860[8][2],&g_860[5][1]},{&g_860[7][0],(void*)0},{&g_860[3][1],&g_860[8][2]},{&g_860[7][0],&g_860[7][0]},{&g_860[7][0],&g_860[3][2]},{&g_860[7][1],&g_860[7][0]},{&g_860[7][0],&g_860[7][0]},{&g_860[7][0],&g_860[8][2]},{(void*)0,(void*)0}},{{(void*)0,&g_860[7][0]},{&g_860[7][0],&g_860[4][0]},{&g_860[6][0],&g_860[6][2]},{&g_860[7][1],&g_860[6][0]},{&g_860[8][1],(void*)0},{&g_860[8][1],&g_860[6][0]},{&g_860[7][1],&g_860[6][2]},{&g_860[6][0],&g_860[4][0]},{&g_860[7][0],&g_860[7][0]},{(void*)0,(void*)0}},{{(void*)0,&g_860[8][2]},{&g_860[7][0],&g_860[7][0]},{&g_860[7][0],&g_860[7][0]},{&g_860[7][1],&g_860[3][2]},{&g_860[7][0],&g_860[7][0]},{&g_860[7][0],&g_860[5][1]},{&g_860[7][0],&g_860[4][0]},{&g_860[4][0],&g_860[6][1]},{(void*)0,&g_860[7][0]},{&g_860[7][0],&g_860[7][0]}},{{(void*)0,&g_860[4][1]},{&g_860[7][0],&g_860[3][1]},{&g_860[7][0],&g_860[7][0]},{&g_860[4][2],(void*)0},{&g_860[7][0],&g_860[1][0]},{&g_860[7][0],(void*)0},{&g_860[7][0],&g_860[1][0]},{&g_860[6][2],&g_860[7][1]},{&g_860[5][1],&g_860[7][0]},{&g_860[6][1],&g_860[7][0]}}};
                        int32_t l_2850 = 0x320F7385L;
                        int32_t l_2852 = 0L;
                        int i, j, k;
                        if (l_2744)
                            break;
                        l_2852 &= (((safe_mod_func_int8_t_s_s((l_2850 = ((**l_2184) = ((safe_mod_func_int32_t_s_s((((((l_2841 == ((&g_860[7][0] != l_2842[6][3][1]) , l_2843)) | (~((safe_mod_func_uint64_t_u_u(((safe_add_func_int32_t_s_s((*p_23), (((*l_2844) , (*g_1663)) != (void*)0))) | l_2850), l_2732)) < p_21))) < l_2821) >= 4294967295UL) < (**g_182)), 0x39589938L)) || l_2851[0][1]))), 250UL)) < 2L) != (-7L));
                        (*l_2700) ^= ((0xD5L > p_21) != l_2599);
                    }
                    (*l_2703) |= (((255UL < 6UL) >= ((l_2647[0] |= (((((((*l_2638)++) >= (safe_mod_func_uint32_t_u_u(((((*g_528) , (l_2819 = (g_1785[5].f2 = (safe_add_func_int16_t_s_s((((*l_2034) = (g_2859 , (p_21 & ((safe_rshift_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((-9L), ((safe_add_func_uint64_t_u_u((((*l_2428) , 0xA685L) & ((((((safe_sub_func_int16_t_s_s((l_2652[3] , p_21), 1UL)) , g_654) <= p_21) & l_2819) | l_2602[4]) , l_2776[3])), (*l_2428))) | p_21))), 5)) < (*l_2428))))) , 9L), 65528UL))))) != 1UL) <= (*g_316)), (*l_2428)))) , 65535UL) , l_2869) != l_2870) < 4L)) | 65528UL)) <= p_21);
                    for (l_2476 = (-29); (l_2476 == 38); ++l_2476)
                    { /* block id: 1266 */
                        uint64_t **l_2875 = &l_2742;
                        int32_t ****l_2885 = &g_896;
                        (***g_180) = (((((*l_2875) = l_2868[1]) == &g_824) >= (safe_div_func_uint16_t_u_u(((safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(((void*)0 != &l_2841), p_21)), (l_2882 >= (l_2776[3] >= (safe_add_func_int32_t_s_s(((((*g_1631) = (*g_2426)) != (l_2886 = ((l_2885 != l_2885) , (void*)0))) == p_21), 0x977B9727L)))))) < (*l_2428)), p_21))) , (*g_182));
                    }
                }
                (*l_2702) &= (*g_461);
            }
        }
        else
        { /* block id: 1275 */
            int16_t l_2911 = 1L;
            int32_t l_2937 = 8L;
            int32_t l_2938 = (-1L);
            int32_t l_2940 = 0xE4634240L;
            int32_t l_2946 = (-1L);
            int32_t l_2977[6] = {0xDD39C15EL,8L,8L,0xDD39C15EL,8L,8L};
            uint16_t l_3012[8] = {65526UL,65526UL,65526UL,65526UL,65526UL,65526UL,65526UL,65526UL};
            int8_t l_3040[9];
            float l_3043 = 0x1.9p+1;
            int32_t l_3061 = 0x72BD41F7L;
            uint32_t l_3067[6][4][3] = {{{18446744073709551614UL,18446744073709551609UL,18446744073709551609UL},{18446744073709551609UL,1UL,18446744073709551614UL},{18446744073709551614UL,1UL,18446744073709551614UL},{0x2D70BE8DL,18446744073709551609UL,18446744073709551614UL}},{{0x2D70BE8DL,0x2D70BE8DL,18446744073709551609UL},{18446744073709551614UL,18446744073709551609UL,18446744073709551609UL},{18446744073709551609UL,1UL,18446744073709551614UL},{18446744073709551614UL,1UL,18446744073709551614UL}},{{0x2D70BE8DL,18446744073709551609UL,18446744073709551614UL},{0x2D70BE8DL,0x2D70BE8DL,18446744073709551609UL},{18446744073709551614UL,18446744073709551609UL,18446744073709551609UL},{18446744073709551609UL,1UL,18446744073709551614UL}},{{18446744073709551614UL,1UL,18446744073709551614UL},{0x2D70BE8DL,18446744073709551609UL,18446744073709551614UL},{0x2D70BE8DL,0x2D70BE8DL,18446744073709551609UL},{18446744073709551614UL,18446744073709551609UL,18446744073709551609UL}},{{18446744073709551609UL,1UL,18446744073709551614UL},{18446744073709551614UL,1UL,18446744073709551614UL},{0x2D70BE8DL,18446744073709551609UL,18446744073709551614UL},{0x2D70BE8DL,0x2D70BE8DL,18446744073709551609UL}},{{18446744073709551614UL,18446744073709551609UL,18446744073709551609UL},{18446744073709551609UL,1UL,18446744073709551614UL},{18446744073709551614UL,1UL,18446744073709551614UL},{0x2D70BE8DL,18446744073709551609UL,18446744073709551614UL}}};
            int i, j, k;
            for (i = 0; i < 9; i++)
                l_3040[i] = 0xB6L;
            if ((*g_30))
            { /* block id: 1276 */
                int8_t l_2916 = 0x7FL;
                uint64_t *l_2917 = &g_1483[1][5][0];
                int16_t *****l_2928 = &g_2927;
                int32_t l_2935 = 0xB6373B77L;
                int32_t l_2941 = 7L;
                int32_t l_2943 = 1L;
                int32_t l_2944 = 0xE95B9818L;
                int32_t l_2945[7] = {5L,5L,5L,5L,5L,5L,5L};
                uint32_t l_2975 = 18446744073709551610UL;
                int32_t *****l_3026 = &g_2327;
                int i;
                (*g_30) ^= (safe_rshift_func_uint16_t_u_s(65535UL, p_21));
                (*g_30) = (l_2652[3] < (safe_lshift_func_int16_t_s_s((p_21 , (safe_rshift_func_int8_t_s_u((safe_mul_func_int8_t_s_s((*l_2428), (((safe_sub_func_uint16_t_u_u(0x27D6L, (((safe_add_func_uint32_t_u_u((*g_240), ((safe_mod_func_uint16_t_u_u((safe_add_func_int16_t_s_s((p_21 == (safe_mul_func_int8_t_s_s((safe_div_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_u(0UL, ((void*)0 == (*g_2275)))) , l_2911), l_2911)), 0UL))), p_21)), g_481)) >= 1L))) && 0xD3F4L) && g_297[2][2][0].f0))) < (***g_1631)) && 0UL))), 4))), p_21)));
                if ((safe_sub_func_int64_t_s_s((safe_add_func_int64_t_s_s(l_2916, ((*l_2917) = p_21))), ((!(safe_div_func_int32_t_s_s(((((*g_139) , ((((safe_mod_func_int8_t_s_s(((safe_mod_func_uint32_t_u_u(0x91C18062L, (*g_30))) < 0x5AL), l_2911)) < ((safe_mul_func_int8_t_s_s(((((*l_2928) = g_2927) != &g_2135) , l_2929), l_2911)) == 0x0AED3171A2AE1EEBLL)) & 8L) ^ (*p_23))) && p_21) == (*p_23)), (*g_240)))) && (-1L)))))
                { /* block id: 1281 */
                    int32_t l_2934 = 0x842243E4L;
                    int32_t l_2936 = (-6L);
                    int32_t l_2942 = 0xE1526C65L;
                    int32_t l_2947 = 0x3598A36EL;
                    uint16_t l_2958[8] = {6UL,1UL,6UL,1UL,6UL,1UL,6UL,1UL};
                    uint64_t l_2986 = 9UL;
                    int i;
                    for (g_2656 = 0; (g_2656 != 35); g_2656 = safe_add_func_uint8_t_u_u(g_2656, 5))
                    { /* block id: 1284 */
                        int8_t l_2932 = 0x18L;
                        int32_t *l_2933[10] = {(void*)0,(void*)0,&l_2647[1],&l_2150,&l_2647[1],(void*)0,(void*)0,&l_2647[1],&l_2150,&l_2647[1]};
                        int i;
                        --l_2948[5][5][2];
                    }
                    if (((((p_21 , g_2951) , (p_21 , (safe_add_func_float_f_f(((((g_1904.f5 >= ((void*)0 == (**g_606))) >= p_21) || ((void*)0 != &l_2911)) , p_22), 0xD.072CA4p-84)))) , g_2954) , (*g_30)))
                    { /* block id: 1287 */
                        int64_t l_2955 = 1L;
                        int32_t *l_2956[9][7] = {{&l_2945[0],&l_2592,&l_2602[4],&l_2591,&g_823,(void*)0,&l_2591},{&l_2114,(void*)0,&l_38,&l_2936,&l_2644[4][0][0],&l_2936,&l_2735},{(void*)0,&l_2592,&l_2644[5][4][0],&l_2645,&l_2941,&g_823,&l_2397},{&l_2936,&l_2735,&l_2114,&l_2114,&l_2735,&l_2936,&l_2937},{&l_2591,&l_2945[0],&l_2644[5][4][0],(void*)0,&l_2946,&l_2941,&l_2941},{&l_2942,(void*)0,&l_2644[4][0][0],(void*)0,&l_2942,&l_2396,&l_2936},{&l_2591,&l_2945[0],&l_2941,&l_2592,&l_2645,&l_2591,&l_2645},{&l_2591,&l_2735,&l_2735,&l_2591,(void*)0,&l_2130,&l_2592},{&l_2591,&l_2592,&l_2946,&l_2602[4],&l_2397,&l_2397,&l_2602[4]}};
                        int i, j;
                        l_2955 = p_22;
                        l_2958[3]++;
                    }
                    else
                    { /* block id: 1290 */
                        const uint32_t l_2976 = 0x3FC76D1DL;
                        int32_t l_2978 = (-6L);
                        int32_t *l_2979 = &l_2937;
                        int32_t *l_2980 = &l_2591;
                        int32_t *l_2981 = (void*)0;
                        int32_t *l_2982 = &l_2978;
                        int32_t *l_2983 = &l_2602[4];
                        int32_t *l_2984 = &l_2938;
                        int32_t *l_2985[9][8][3] = {{{&l_2644[4][0][0],&g_823,&g_823},{&l_2945[6],(void*)0,&l_2400},{&l_2592,(void*)0,&l_2599},{(void*)0,&l_2978,&l_2978},{&g_823,&l_2592,&l_2586},{&l_2586,&l_2978,&l_38},{&l_2592,(void*)0,&l_2937},{&l_2398,(void*)0,&l_2592}},{{&l_2586,&g_823,&l_2592},{&l_2947,&l_2400,&l_2936},{&l_2602[5],(void*)0,&l_2945[6]},{&l_2592,&l_2937,&l_2945[6]},{&l_2978,&l_2398,&l_2936},{(void*)0,&l_2938,&l_2592},{&l_2592,&l_38,&l_2592},{(void*)0,(void*)0,&l_2937}},{{&l_2945[6],&l_2644[4][0][0],&l_38},{&l_2398,&l_2592,&l_2586},{&l_2400,&l_2114,&l_2978},{&l_2398,&l_2978,&l_2599},{&l_2945[6],&l_2595,&l_2400},{(void*)0,&l_2586,&g_823},{&l_2592,&l_2400,&l_2937},{(void*)0,&l_2592,&l_2644[4][0][0]}},{{&l_2978,&l_2936,&l_2586},{&l_2592,&l_2936,(void*)0},{&l_2602[5],&l_2592,&l_2398},{&l_2947,&l_2400,&l_2935},{&l_2586,&l_2586,&l_2602[5]},{&l_2398,&l_2595,&l_2586},{&l_2592,&l_2978,&l_2938},{&l_2586,&l_2114,(void*)0}},{{&g_823,&l_2592,&l_2938},{(void*)0,&l_2644[4][0][0],&l_2586},{&l_2592,(void*)0,&l_2602[5]},{&l_2945[6],&l_38,&l_2935},{&l_2644[4][0][0],&l_2938,&l_2398},{&g_156,&l_2398,(void*)0},{&l_2595,&l_2937,&l_2586},{&l_2595,(void*)0,&l_2644[4][0][0]}},{{&g_156,&l_2400,&l_2937},{&l_2644[4][0][0],&g_823,&g_823},{&l_2945[6],(void*)0,&l_2400},{&l_2592,(void*)0,&l_2599},{(void*)0,&l_2978,&l_2978},{&g_823,&l_2592,&l_2586},{&l_2592,&l_2586,&l_2398},{&l_2595,(void*)0,&l_2943}},{{&l_2400,&l_2935,&l_2592},{&l_2602[5],&l_2644[4][0][0],&l_2938},{&l_2945[6],&l_2586,(void*)0},{&l_2398,&l_2592,(void*)0},{&l_2937,&l_2943,(void*)0},{&l_2400,&l_2400,(void*)0},{&l_2935,(void*)0,&l_2938},{&l_2592,&l_2398,&l_2592}},{{(void*)0,&l_2978,&l_2943},{(void*)0,&l_2398,&l_2398},{&l_2936,&l_2595,&l_2592},{&l_2586,&l_2400,&l_2937},{&l_2936,&l_2400,&l_2947},{(void*)0,&l_2595,&l_2978},{(void*)0,&l_2602[5],&l_2644[4][0][0]},{&l_2592,&l_2978,&l_2586}},{{&l_2935,&l_2937,&l_2398},{&l_2400,(void*)0,&l_2644[1][4][0]},{&l_2937,(void*)0,(void*)0},{&l_2398,&l_2937,&l_2400},{&l_2945[6],&l_2978,&l_2599},{&l_2602[5],&l_2602[5],&l_2398},{&l_2400,&l_2595,&l_2602[5]},{&l_2595,&l_2400,(void*)0}}};
                        int i, j, k;
                        l_2947 = (((safe_div_func_float_f_f((safe_div_func_float_f_f((((**g_527) = ((p_21 , (p_21 > ((-0x1.Ap-1) <= ((safe_mul_func_float_f_f((((l_2978 = (((safe_sub_func_uint8_t_u_u((((safe_div_func_uint8_t_u_u((((((**g_2278) = (g_824 == ((safe_add_func_float_f_f((l_2936 = (safe_sub_func_float_f_f(0x3.0ABB05p-47, 0x4.7878D7p+89))), (((((-1L) && 0x75C69EFBL) , (**g_527)) < l_2975) <= 0xC.939F18p+38))) , l_2801))) | l_2976) | p_21) & l_2946), l_2977[5])) < 0xB3FC340FL) ^ (*p_23)), 0x8FL)) , 0x8.1C6324p-67) >= p_22)) < 0x0.Ap-1) >= l_2977[5]), l_2599)) < 0x5.7p+1)))) < p_21)) == p_22), (-0x1.Cp+1))), 0x0.Fp-1)) < 0x2.1F37BAp+14) == p_22);
                        --l_2986;
                        p_22 = ((-0x1.0p+1) < (+(safe_mul_func_float_f_f(p_22, 0x2.3DF25Cp-94))));
                    }
                    for (l_2596 = (-24); (l_2596 <= 3); l_2596++)
                    { /* block id: 1301 */
                        uint16_t l_3000 = 0x29D1L;
                        float ***l_3008[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        if (l_2644[4][0][0])
                            break;
                        (*g_183) ^= ((*p_23) = ((safe_lshift_func_uint16_t_u_s(g_1180, 0)) > l_2595));
                        if ((**g_182))
                            break;
                        (*g_30) |= (safe_mod_func_int16_t_s_s((((safe_lshift_func_int8_t_s_s((l_3000 , (safe_mod_func_uint64_t_u_u((((1UL > (1L >= (safe_lshift_func_int16_t_s_s(0x5055L, 5)))) == (safe_mul_func_uint8_t_u_u((l_3007 > ((l_3009 = (void*)0) == l_3010[0][3])), (0xADL <= (l_2591 > p_21))))) && (*l_2428)), p_21))), g_3011)) & 0x81F5L) >= (*p_23)), 0xED47L));
                    }
                }
                else
                { /* block id: 1309 */
                    ++l_3012[7];
                }
                for (g_157 = 0; (g_157 <= 2); g_157 += 1)
                { /* block id: 1314 */
                    uint32_t ***l_3027 = &g_239;
                    uint16_t *l_3032 = &l_3012[1];
                    int32_t l_3041 = 0x28B59007L;
                    uint16_t l_3042 = 0x754DL;
                    int32_t *l_3044 = &l_2940;
                    int32_t *l_3045 = &l_2602[7];
                    int32_t *l_3046 = (void*)0;
                    int32_t *l_3047 = &l_2599;
                    int32_t *l_3048 = (void*)0;
                    int32_t *l_3049 = (void*)0;
                    int32_t *l_3050 = &l_2941;
                    int32_t *l_3051 = &l_38;
                    int32_t *l_3052 = &l_2647[1];
                    int32_t *l_3053 = (void*)0;
                    int32_t *l_3054 = &l_2130;
                    int32_t *l_3055[1][7][9] = {{{&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0,&l_2946,(void*)0,(void*)0}}};
                    int i, j, k;
                    for (l_2935 = 2; (l_2935 >= 0); l_2935 -= 1)
                    { /* block id: 1317 */
                        (**g_527) = (safe_mul_func_float_f_f(0x0.6p-1, p_21));
                    }
                    l_2647[g_157] = ((safe_rshift_func_uint16_t_u_s(((safe_sub_func_int32_t_s_s((((0x5168L < ((((safe_mul_func_uint16_t_u_u(g_297[2][2][0].f1, (safe_sub_func_uint8_t_u_u((((((l_3025 = &g_2327) != l_3026) == (((l_3027 == l_2576) <= (((safe_mul_func_uint16_t_u_u(((*l_3032) &= (safe_rshift_func_uint8_t_u_s((*g_316), 2))), (~0xAEL))) <= ((safe_sub_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u(((void*)0 == l_2917), p_21)), l_3040[1])), l_3041)) | p_21)) > 0UL)) , g_881)) , p_21) && l_3042), 255UL)))) , l_3041) , (*l_2789)) != (void*)0)) > 0x5809344FL) && l_3042), (*p_23))) <= (*l_2428)), 7)) == g_1904.f0);
                    ++g_3058;
                    for (g_727 = 2; (g_727 >= 0); g_727 -= 1)
                    { /* block id: 1326 */
                        (**g_953) = p_23;
                        l_3061 = 0x8.741FD6p-48;
                    }
                }
            }
            else
            { /* block id: 1331 */
                int32_t l_3062 = 1L;
                int32_t l_3063[6] = {0x24622A7AL,0x24622A7AL,0x24622A7AL,0x24622A7AL,0x24622A7AL,0x24622A7AL};
                int32_t *l_3064 = &l_2937;
                int32_t *l_3065 = &l_3062;
                int32_t *l_3066[8];
                int i;
                for (i = 0; i < 8; i++)
                    l_3066[i] = &l_2130;
                l_3067[1][3][1]--;
                for (g_2954.f5 = 0; (g_2954.f5 >= 5); g_2954.f5 = safe_add_func_uint8_t_u_u(g_2954.f5, 5))
                { /* block id: 1335 */
                    if ((*p_23))
                        break;
                    (*l_3064) = ((((****g_180) = (*p_23)) , p_21) , l_2946);
                    (*g_183) &= 0x8ED53F47L;
                }
                for (g_883 = 0; (g_883 >= 16); g_883 = safe_add_func_int8_t_s_s(g_883, 8))
                { /* block id: 1343 */
                    uint16_t l_3094 = 0xD4C4L;
                    for (l_2735 = 0; (l_2735 <= 2); l_2735 += 1)
                    { /* block id: 1346 */
                        int32_t l_3093 = 0L;
                        int16_t l_3095[7];
                        uint16_t * const **l_3098[4];
                        uint64_t *l_3114 = &g_105;
                        int32_t * const l_3117 = &g_3118[9];
                        int32_t * const *l_3116 = &l_3117;
                        int32_t * const **l_3115 = &l_3116;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_3095[i] = (-3L);
                        for (i = 0; i < 4; i++)
                            l_3098[i] = &g_3096;
                        (**g_527) = ((safe_sub_func_float_f_f((safe_mul_func_float_f_f(p_21, (safe_add_func_float_f_f(((((((0xB.5D10B8p-15 < p_21) > (safe_sub_func_float_f_f(((p_21 != (p_22 < (safe_sub_func_float_f_f((((safe_mul_func_float_f_f((!(l_2882 < (((void*)0 != l_3087) , ((((safe_div_func_float_f_f(((safe_sub_func_float_f_f(0x8.59C7E1p+34, p_21)) != p_21), (*g_528))) < l_3093) != l_3094) > p_22)))), 0x0.1p-1)) == p_21) >= 0xD.765CDDp-50), p_21)))) <= p_22), l_3095[6]))) <= (-0x8.7p-1)) != (-0x1.7p-1)) <= p_21) > 0x0.A4ADF7p+29), p_21)))), l_2938)) < l_2938);
                        (*p_23) = ((((g_3096 = g_3096) != (void*)0) || (safe_rshift_func_int16_t_s_u(((**l_2789) = (((*l_3115) = ((!(safe_add_func_int16_t_s_s(((p_21 && ((**g_3096)++)) > (safe_lshift_func_int8_t_s_u(((*p_23) != (p_21 < (!g_3109))), (((safe_div_func_int8_t_s_s(p_21, (safe_sub_func_uint64_t_u_u(((*l_3114) = (*l_2428)), 0x561BA5810A249C1CLL)))) > p_21) , 0x1BL)))), 0xDBF4L))) , (void*)0)) == &l_3117)), 0))) == 0xCEL);
                        if ((*p_23))
                            continue;
                        (**g_953) = func_78(func_82(g_1904.f7), (((void*)0 == &g_3056) >= 0L), &l_3063[1]);
                    }
                    if ((*l_3064))
                        continue;
                }
                l_3120[5]++;
            }
        }
        return p_23;
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_157
 * writes: g_157
 */
static uint8_t  func_46(int16_t  p_47, int32_t  p_48, uint8_t * p_49, int32_t  p_50)
{ /* block id: 758 */
    int16_t l_1693 = (-1L);
    int32_t l_1699 = 0x61D89FF1L;
    int32_t l_1702 = 0xBA3AC81FL;
    int32_t l_1703 = 1L;
    int64_t l_1704 = 0xD2E7FC64FC98A360LL;
    int32_t l_1705 = (-3L);
    int32_t l_1706 = 0x1148ECF6L;
    int32_t l_1707[6];
    uint8_t *** const l_1905[1][10][4] = {{{&g_315[3],&g_315[4],&g_315[3],&g_315[4]},{&g_315[4],&g_315[3],&g_315[3],&g_315[3]},{&g_315[3],&g_315[3],&g_315[4],&g_315[3]},{(void*)0,&g_315[3],(void*)0,&g_315[4]},{(void*)0,&g_315[4],&g_315[4],(void*)0},{&g_315[3],&g_315[4],&g_315[3],&g_315[4]},{&g_315[4],&g_315[3],&g_315[3],&g_315[3]},{&g_315[3],&g_315[3],&g_315[4],&g_315[3]},{(void*)0,&g_315[3],(void*)0,(void*)0},{&g_315[3],(void*)0,(void*)0,&g_315[3]}}};
    uint8_t l_1930 = 0xE8L;
    int8_t l_1965[5][3] = {{0x5DL,0x5DL,0x5DL},{(-2L),0x5FL,(-2L)},{0x5DL,0x5DL,0x5DL},{(-2L),0x5FL,(-2L)},{0x5DL,0x5DL,0x5DL}};
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1707[i] = 5L;
    for (g_157 = 0; (g_157 <= 0); g_157 += 1)
    { /* block id: 761 */
        const int32_t *l_1659 = &g_1660[4];
        const int32_t **l_1658[1][9] = {{(void*)0,&l_1659,(void*)0,&l_1659,(void*)0,&l_1659,(void*)0,&l_1659,(void*)0}};
        int32_t l_1667[10] = {0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L,0x141A0494L};
        int32_t l_1691[5];
        uint8_t ****l_1715 = &g_1666;
        uint8_t l_1808 = 255UL;
        int32_t *****l_1816[4];
        uint32_t l_1817 = 0x9E952C5CL;
        float l_1822 = 0x0.1p+1;
        int64_t *l_1835 = &g_920;
        int32_t l_1874[8] = {0xFFEA7847L,0xFFEA7847L,0xFFEA7847L,0xFFEA7847L,0xFFEA7847L,0xFFEA7847L,0xFFEA7847L,0xFFEA7847L};
        uint32_t ****l_1921[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int8_t l_1993 = 0x0AL;
        uint32_t l_2001[4][8][8] = {{{0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L},{0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L},{0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL},{0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L},{0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L},{0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL},{0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L},{0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L}},{{0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL},{0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L},{0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L},{0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL},{0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L},{0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L},{0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL},{0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L,0UL,0x0463FF92L,0x0463FF92L}},{{0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L},{0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL},{0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL},{0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL},{0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL},{0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL},{0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL},{0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL}},{{0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL},{0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL},{0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL},{0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL},{0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL},{0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL},{0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL,0x0463FF92L,0x97FA8D5AL,0x97FA8D5AL},{0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL,0UL,0UL,0x97FA8D5AL}}};
        int16_t *l_2014 = &g_235[1];
        int16_t ** const l_2013 = &l_2014;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_1691[i] = 0x5E3F6877L;
        for (i = 0; i < 4; i++)
            l_1816[i] = (void*)0;
    }
    return l_1693;
}


/* ------------------------------------------ */
/* 
 * reads : g_174.f2 g_1558 g_182 g_183 g_156 g_34 g_181 g_239 g_240 g_104 g_1246 g_606 g_607 g_299 g_1497 g_528 g_953 g_460 g_824 g_1630 g_1476 g_527
 * writes: g_654 g_156 g_1009.f2 g_174.f2 g_104 g_1599 g_291 g_1478 g_461 g_824 g_1631 g_107 g_1476
 */
static uint8_t  func_55(int32_t * p_56, int64_t  p_57, int32_t ** p_58, uint8_t  p_59, int8_t  p_60)
{ /* block id: 707 */
    uint64_t l_1560 = 18446744073709551615UL;
    uint16_t l_1621 = 1UL;
    struct S0 *l_1632 = &g_467;
    int16_t l_1633 = 0xBFF9L;
    int32_t *l_1652 = (void*)0;
    for (p_59 = 6; (p_59 != 46); p_59 = safe_add_func_int8_t_s_s(p_59, 8))
    { /* block id: 710 */
        int64_t l_1559 = 0x41D495626A07BEA7LL;
        uint64_t l_1581[8] = {4UL,18446744073709551606UL,4UL,18446744073709551606UL,4UL,18446744073709551606UL,4UL,18446744073709551606UL};
        int8_t *l_1615 = &g_107[5];
        int8_t ** const l_1614 = &l_1615;
        uint32_t ***l_1629 = &g_239;
        int i;
        for (g_654 = 4; (g_654 < 60); g_654 = safe_add_func_uint64_t_u_u(g_654, 7))
        { /* block id: 713 */
            int8_t l_1557[8][8] = {{0x19L,0x84L,0x0FL,0xB7L,0L,7L,0xFBL,0xECL},{(-1L),0xBFL,0x0FL,0xA6L,(-5L),6L,(-1L),(-1L)},{0L,(-1L),0x89L,0x89L,(-1L),0L,(-1L),0xB7L},{6L,(-5L),0xA6L,0x0FL,0xBFL,(-1L),(-5L),0xECL},{7L,0L,0xB7L,0x0FL,0x84L,0x19L,0xBFL,0xB7L},{0xFBL,0x84L,0xECL,0x89L,0xECL,0x84L,0xFBL,(-1L)},{0xECL,(-1L),0xBFL,0xA6L,(-1L),9L,0x0FL,0xECL},{0L,0xAFL,0xEEL,0xB7L,(-1L),0L,(-1L),(-1L)}};
            int16_t l_1590 = 0xEBB2L;
            float ***l_1593 = &g_861;
            int8_t **l_1613[3];
            struct S0 * volatile l_1625 = (void*)0;/* VOLATILE GLOBAL l_1625 */
            int32_t *l_1626[5][9][5] = {{{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823}},{{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823}},{{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823}},{{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823}},{{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_727,&g_34,&g_727,&g_823,&g_823},{&g_236,(void*)0,&g_236,&g_727,&g_727},{&g_236,(void*)0,&g_236,&g_727,&g_727},{&g_236,(void*)0,&g_236,&g_727,&g_727},{&g_236,(void*)0,&g_236,&g_727,&g_727},{&g_236,(void*)0,&g_236,&g_727,&g_727},{&g_236,(void*)0,&g_236,&g_727,&g_727},{&g_236,(void*)0,&g_236,&g_727,&g_727}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1613[i] = (void*)0;
            (**g_182) |= (safe_add_func_int8_t_s_s(((p_57 | (((~(safe_mod_func_int64_t_s_s(0x8E5387D26CE5CC34LL, (-9L)))) , p_60) & (g_174.f2 == (l_1559 = (safe_mod_func_uint16_t_u_u((p_60 > l_1557[3][6]), (0xA5L | (g_1558 , 1UL)))))))) > l_1560), 0x57L));
            if ((**p_58))
            { /* block id: 716 */
                int8_t *l_1567 = &g_107[8];
                int8_t ** const l_1566[4] = {&l_1567,&l_1567,&l_1567,&l_1567};
                int8_t ** const * const l_1565 = &l_1566[3];
                int64_t *l_1574 = (void*)0;
                int64_t *l_1575 = &g_1009[2].f2;
                int64_t *l_1576 = &g_174.f2;
                uint16_t l_1591 = 4UL;
                const float **l_1595 = (void*)0;
                const float ***l_1594 = &l_1595;
                const float ***l_1598 = (void*)0;
                uint32_t l_1620[5];
                int16_t *l_1650 = &g_1476[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_1620[i] = 0x41855ECFL;
                if ((safe_div_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((l_1565 == ((safe_div_func_int64_t_s_s((safe_lshift_func_int16_t_s_u(l_1559, (((safe_sub_func_int64_t_s_s(((*l_1576) &= ((7UL >= p_57) != (((*l_1575) = 0x96D8000795CEAC85LL) != l_1560))), (safe_add_func_int16_t_s_s(((safe_div_func_uint8_t_u_u(((l_1581[4] |= l_1559) < ((safe_mod_func_int64_t_s_s((safe_mul_func_int8_t_s_s((((safe_mod_func_int32_t_s_s((***g_181), (--(**g_239)))) > 0L) ^ l_1590), l_1591)), 18446744073709551606UL)) >= p_60)), l_1557[3][6])) != 8UL), (-2L))))) , g_1246) == 65531UL))), p_57)) , &g_573[4])), 2L)), l_1557[3][6])))
                { /* block id: 721 */
                    (*g_183) = (-1L);
                }
                else
                { /* block id: 723 */
                    const float ****l_1596 = (void*)0;
                    const float ****l_1597[2][9][6] = {{{&l_1594,(void*)0,(void*)0,&l_1594,(void*)0,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,(void*)0},{&l_1594,&l_1594,&l_1594,&l_1594,(void*)0,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594}},{{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,(void*)0},{(void*)0,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,&l_1594,&l_1594,(void*)0},{&l_1594,&l_1594,(void*)0,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,(void*)0,(void*)0,&l_1594},{&l_1594,&l_1594,(void*)0,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,&l_1594,(void*)0,&l_1594,(void*)0},{(void*)0,&l_1594,&l_1594,&l_1594,&l_1594,&l_1594},{&l_1594,&l_1594,(void*)0,&l_1594,&l_1594,&l_1594}}};
                    int32_t l_1610 = 0x6C91FE38L;
                    float *l_1616 = (void*)0;
                    float *l_1617 = &g_1478;
                    int i, j, k;
                    (**g_182) |= ((!(((((l_1593 == (g_1599 = (l_1598 = l_1594))) || (((safe_add_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s(((((**g_606) != (*g_1497)) , ((safe_add_func_float_f_f((-0x3.6p-1), (safe_sub_func_float_f_f((safe_sub_func_float_f_f(((*g_528) = l_1610), (safe_div_func_float_f_f(((*l_1617) = (l_1613[1] != l_1614)), (safe_add_func_float_f_f(p_59, 0x8.8D9769p+2)))))), (-0x2.Bp+1))))) , 0x9647L)) ^ l_1620[1]), l_1621)) != p_60), l_1610)) & l_1560) != (**g_239))) , 0UL) , (void*)0) != &l_1596)) && p_59);
                    for (p_57 = 1; (p_57 != (-19)); p_57 = safe_sub_func_int64_t_s_s(p_57, 5))
                    { /* block id: 731 */
                        uint16_t l_1624 = 0xE8CEL;
                        l_1624 = l_1557[0][3];
                        l_1625 = (*g_607);
                    }
                    (**g_953) = l_1626[0][4][1];
                    for (g_824 = 0; (g_824 < 10); g_824 = safe_add_func_int32_t_s_s(g_824, 2))
                    { /* block id: 738 */
                        (*g_1630) = l_1629;
                        l_1632 = l_1632;
                        return l_1633;
                    }
                }
                (**g_527) = ((-0x1.8p+1) >= (l_1621 == (p_60 != (0x9.5293CBp-27 <= (safe_mul_func_float_f_f((((l_1581[0] >= (++(*g_240))) , (safe_sub_func_float_f_f(((safe_sub_func_float_f_f(0x0.371D7Ap+94, ((((*l_1650) = ((safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_s(((safe_rshift_func_int8_t_s_s((g_1476[3] >= (((**l_1614) = p_57) , ((safe_div_func_int16_t_s_s(0xA3ADL, 0xE50CL)) > p_59))), l_1559)) | l_1581[4]), p_57)), 0xFCL)) , 0xEDFEL)) , (void*)0) != (void*)0))) >= p_60), p_57))) == 0xC.390157p+94), 0xD.AAD0B4p-40))))));
            }
            else
            { /* block id: 748 */
                int32_t *l_1651[4][8] = {{&g_236,&g_727,&g_34,&g_236,&g_236,&g_34,&g_727,&g_236},{&g_727,&g_1180,&g_236,&g_823,&g_236,&g_1180,&g_727,&g_727},{&g_1180,&g_823,&g_34,&g_34,&g_823,&g_1180,&g_236,&g_1180},{&g_823,&g_1180,&g_236,&g_1180,&g_823,&g_34,&g_34,&g_823}};
                int i, j;
                if ((*g_183))
                    break;
                (*p_58) = l_1651[3][1];
                return p_57;
            }
        }
    }
    l_1652 = l_1652;
    return p_59;
}


/* ------------------------------------------ */
/* 
 * reads : g_85 g_574 g_575 g_105 g_211 g_210 g_180 g_181 g_182 g_183 g_30 g_31 g_174.f5 g_140.f2 g_316 g_36 g_881 g_107 g_240 g_104 g_654
 * writes: g_85 g_884 g_1496 g_1497 g_156 g_31 g_797 g_1483 g_1009.f8 g_104 g_481 g_34
 */
static int32_t * func_61(int32_t  p_62, uint32_t  p_63, int32_t  p_64, float  p_65)
{ /* block id: 684 */
    int32_t *l_1486[2][10] = {{&g_34,&g_156,&g_236,&g_34,&g_236,&g_156,&g_34,&g_823,&g_823,&g_34},{&g_823,&g_236,&g_34,&g_34,&g_236,(void*)0,&g_34,&g_236,&g_34,(void*)0}};
    uint8_t *l_1491 = &g_884;
    struct S0 **l_1494 = (void*)0;
    struct S0 ***l_1495[2][3];
    struct S0 **l_1498[5];
    float ****l_1500 = (void*)0;
    float *****l_1499 = &l_1500;
    uint64_t *l_1501[10] = {&g_1483[1][4][0],&g_88[0][6][0],&g_88[0][6][0],&g_1483[1][4][0],&g_1483[1][3][1],&g_1483[1][4][0],&g_88[0][6][0],&g_88[0][6][0],&g_1483[1][4][0],&g_1483[1][3][1]};
    int32_t * const *l_1506[5][7][7] = {{{(void*)0,&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714},{(void*)0,&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{(void*)0,(void*)0,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,(void*)0,&g_714,&g_714,&g_714,(void*)0,&g_714},{&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714,&g_714}},{{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,(void*)0,(void*)0,&g_714,&g_714,&g_714,&g_714},{(void*)0,&g_714,(void*)0,(void*)0,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714}},{{&g_714,&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{(void*)0,&g_714,(void*)0,&g_714,(void*)0,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714}},{{(void*)0,&g_714,(void*)0,&g_714,&g_714,(void*)0,&g_714},{&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714,(void*)0},{&g_714,&g_714,(void*)0,(void*)0,&g_714,(void*)0,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,(void*)0,(void*)0,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714},{&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714,&g_714}},{{&g_714,&g_714,&g_714,&g_714,&g_714,(void*)0,&g_714},{(void*)0,&g_714,&g_714,(void*)0,&g_714,(void*)0,&g_714},{&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714,&g_714},{(void*)0,&g_714,(void*)0,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,(void*)0,&g_714,&g_714,&g_714}}};
    uint16_t l_1507 = 0x1C38L;
    int32_t l_1510 = 0x2885AC3DL;
    uint16_t *l_1511 = &l_1507;
    int32_t l_1516[2];
    uint8_t l_1517 = 0xADL;
    uint8_t *l_1528 = &g_797;
    float l_1541 = 0x1.Bp+1;
    uint16_t *l_1542 = &g_481;
    uint8_t l_1543 = 0xC7L;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
            l_1495[i][j] = &l_1494;
    }
    for (i = 0; i < 5; i++)
        l_1498[i] = &g_299;
    for (i = 0; i < 2; i++)
        l_1516[i] = 0L;
    for (g_85 = 24; (g_85 == 55); g_85 = safe_add_func_int16_t_s_s(g_85, 9))
    { /* block id: 687 */
        return l_1486[1][5];
    }
    (****g_180) = (((((safe_rshift_func_int16_t_s_s((((((((1UL ^ ((*l_1491) = (safe_lshift_func_int16_t_s_s(p_62, 2)))) <= (((safe_div_func_uint8_t_u_u(((g_1496 = l_1494) == (l_1498[0] = (g_1497 = &g_299))), (*g_574))) , l_1499) == &l_1500)) & (p_64 , 0x33502371E6B4C937LL)) , l_1501[5]) != (void*)0) <= 0xB7L) >= g_105), g_211)) == p_63) & p_62) | 65535UL) | g_210[4]);
    (*g_30) |= (safe_sub_func_uint16_t_u_u(((0UL ^ ((safe_rshift_func_int16_t_s_u(((void*)0 == l_1506[1][2][2]), l_1507)) >= (safe_mod_func_uint8_t_u_u(((l_1510 <= 0x5BL) > p_63), (-1L))))) , (++(*l_1511))), (safe_add_func_uint16_t_u_u((l_1516[0] = (p_62 == 252UL)), p_62))));
    p_62 = (((((*l_1542) = (l_1517 > (!((safe_div_func_uint32_t_u_u((safe_mod_func_int8_t_s_s(((((*g_240) ^= (!(0x3906L > (safe_mod_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u(((*l_1528) = ((*l_1491) = p_64)), (((((p_64 > (safe_sub_func_uint32_t_u_u((safe_add_func_int8_t_s_s((safe_div_func_uint16_t_u_u(g_174.f5, (((g_1483[1][5][0] = 18446744073709551615UL) >= (safe_sub_func_int16_t_s_s(g_140.f2, (safe_mul_func_uint16_t_u_u((g_1009[2].f8 = ((*l_1511) = (safe_div_func_uint8_t_u_u((*g_316), 255UL)))), g_881))))) , 0xB69AL))), 1L)), p_63))) != 0xADA82CC290A4493ELL) ^ g_210[2]) & 0L) , p_63))) > 0UL), g_107[6]))))) , p_64) , p_64), p_62)), p_63)) | p_63)))) == g_654) >= (*g_316)) , l_1543);
    return &g_727;
}


/* ------------------------------------------ */
/* 
 * reads : g_1483
 * writes:
 */
static uint16_t  func_72(int32_t ** p_73, int32_t * p_74, float  p_75)
{ /* block id: 350 */
    uint32_t l_738 = 0x29BF98FBL;
    uint32_t **l_763[4][4] = {{&g_240,&g_240,&g_240,&g_240},{&g_240,&g_240,&g_240,&g_240},{&g_240,&g_240,&g_240,&g_240},{&g_240,&g_240,&g_240,&g_240}};
    int32_t l_770 = 0x5ECFBEB2L;
    int32_t l_819 = 0xC1C61F4AL;
    int32_t l_820 = (-4L);
    uint64_t l_835[4][9] = {{0xB5CF9A4630753B39LL,0xDD453C3BA185487DLL,0xDD453C3BA185487DLL,0xB5CF9A4630753B39LL,18446744073709551615UL,0xA19035F3022545CFLL,0xB5CF9A4630753B39LL,0xA19035F3022545CFLL,18446744073709551615UL},{0xB5CF9A4630753B39LL,0xDD453C3BA185487DLL,0xDD453C3BA185487DLL,0xB5CF9A4630753B39LL,18446744073709551615UL,0xA19035F3022545CFLL,0xB5CF9A4630753B39LL,0xA19035F3022545CFLL,18446744073709551615UL},{0xB5CF9A4630753B39LL,0xDD453C3BA185487DLL,0xDD453C3BA185487DLL,0xB5CF9A4630753B39LL,18446744073709551615UL,0xA19035F3022545CFLL,0xB5CF9A4630753B39LL,0xA19035F3022545CFLL,18446744073709551615UL},{0xB5CF9A4630753B39LL,0xDD453C3BA185487DLL,0xDD453C3BA185487DLL,0xB5CF9A4630753B39LL,18446744073709551615UL,0xA19035F3022545CFLL,0xB5CF9A4630753B39LL,0xA19035F3022545CFLL,18446744073709551615UL}};
    uint8_t *l_841 = &g_36;
    int32_t *l_867 = &l_820;
    int32_t l_879 = 0xA5EBE5ABL;
    const int8_t l_918 = 0x2BL;
    int64_t l_919 = (-1L);
    int8_t l_964 = 2L;
    struct S0 * const l_1008 = &g_1009[2];
    int32_t **l_1095 = &g_714;
    uint16_t l_1100 = 65535UL;
    float **l_1112[9][1][1] = {{{&g_862}},{{&g_862}},{{&g_862}},{{&g_862}},{{&g_862}},{{&g_862}},{{&g_862}},{{&g_862}},{{&g_862}}};
    int32_t ** const *l_1183[7][7][5] = {{{&g_897,&g_897,&g_897,&g_897,&g_897},{(void*)0,(void*)0,&g_897,(void*)0,(void*)0},{&g_897,&g_897,&g_897,&g_897,&g_897},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_897,&g_897,(void*)0,&g_897,&g_897},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_897,&g_897,&g_897,&g_897,&g_897}},{{(void*)0,(void*)0,&g_897,(void*)0,(void*)0},{&g_897,&g_897,&g_897,&g_897,&g_897},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_897,&g_897,(void*)0,&g_897,&g_897},{(void*)0,(void*)0,&g_897,&g_897,(void*)0},{&g_897,(void*)0,(void*)0,&g_897,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_897,(void*)0,(void*)0,&g_897},{(void*)0,&g_897,&g_897,(void*)0,&g_897},{&g_897,&g_897,&g_897,&g_897,&g_897},{&g_897,(void*)0,&g_897,&g_897,(void*)0},{&g_897,(void*)0,(void*)0,&g_897,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_897,(void*)0,(void*)0,&g_897}},{{(void*)0,&g_897,&g_897,(void*)0,&g_897},{&g_897,&g_897,&g_897,&g_897,&g_897},{&g_897,(void*)0,&g_897,&g_897,(void*)0},{&g_897,(void*)0,(void*)0,&g_897,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_897,(void*)0,(void*)0,&g_897},{(void*)0,&g_897,&g_897,(void*)0,&g_897}},{{&g_897,&g_897,&g_897,&g_897,&g_897},{&g_897,(void*)0,&g_897,&g_897,(void*)0},{&g_897,(void*)0,(void*)0,&g_897,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_897,(void*)0,(void*)0,&g_897},{(void*)0,&g_897,&g_897,(void*)0,&g_897},{&g_897,&g_897,&g_897,&g_897,&g_897}},{{&g_897,(void*)0,&g_897,&g_897,(void*)0},{&g_897,(void*)0,(void*)0,&g_897,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_897,(void*)0,(void*)0,&g_897},{(void*)0,&g_897,&g_897,(void*)0,&g_897},{&g_897,&g_897,&g_897,&g_897,&g_897},{&g_897,(void*)0,&g_897,&g_897,(void*)0}},{{&g_897,(void*)0,(void*)0,&g_897,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_897,(void*)0,(void*)0,&g_897},{(void*)0,&g_897,&g_897,(void*)0,&g_897},{&g_897,&g_897,&g_897,&g_897,&g_897},{&g_897,(void*)0,&g_897,&g_897,(void*)0},{&g_897,(void*)0,(void*)0,&g_897,(void*)0}}};
    int32_t ** const **l_1182 = &l_1183[3][3][3];
    int32_t ** const ***l_1181[6];
    int32_t l_1185 = 1L;
    int32_t l_1213 = (-2L);
    uint16_t l_1233[4];
    int64_t l_1239 = (-1L);
    uint32_t l_1249[6][9] = {{0xDF6E686BL,0xE48DB441L,0xE48DB441L,0xE48DB441L,0x995B195BL,0xBC137369L,4294967295UL,1UL,0xDF6E686BL},{0x778721CDL,0x9802ECC7L,0x39A79628L,0xDF6E686BL,0x995B195BL,0x995B195BL,0xDF6E686BL,0x39A79628L,0x9802ECC7L},{0x995B195BL,1UL,0xBC137369L,0x69356178L,0UL,0x9802ECC7L,4294967295UL,4294967295UL,0x9802ECC7L},{0x69356178L,0x39A79628L,1UL,0x39A79628L,0x69356178L,1UL,6UL,0xBC137369L,0xDF6E686BL},{6UL,1UL,0x69356178L,0x39A79628L,1UL,0x39A79628L,0x69356178L,1UL,6UL},{4294967295UL,0x9802ECC7L,0UL,0x69356178L,0xBC137369L,1UL,0x995B195BL,1UL,0xBC137369L}};
    float **l_1274[1][6] = {{&g_862,&g_862,&g_862,&g_862,&g_862,&g_862}};
    int8_t l_1289 = 3L;
    int8_t l_1302 = 0x73L;
    const uint64_t *l_1312 = &g_88[3][0][0];
    const uint64_t **l_1311[7];
    int32_t l_1348 = 0x277EFA1CL;
    float * const l_1369 = &g_1370;
    float * const *l_1368[9] = {&l_1369,&l_1369,(void*)0,&l_1369,&l_1369,(void*)0,&l_1369,&l_1369,(void*)0};
    float * const ** const l_1367 = &l_1368[3];
    float * const ** const *l_1366[6][3] = {{&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367}};
    int32_t l_1414 = 1L;
    int64_t l_1416[4];
    uint8_t l_1424 = 255UL;
    int16_t l_1470 = (-1L);
    uint16_t l_1471[4] = {0xF5ACL,0xF5ACL,0xF5ACL,0xF5ACL};
    int32_t l_1477 = 0xA5D75176L;
    float l_1482 = 0xE.8DE2FAp-87;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1181[i] = &l_1182;
    for (i = 0; i < 4; i++)
        l_1233[i] = 0x8E48L;
    for (i = 0; i < 7; i++)
        l_1311[i] = &l_1312;
    for (i = 0; i < 4; i++)
        l_1416[i] = 0xD649AA7AB30293CDLL;
    return g_1483[1][3][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_654 g_174.f8 g_174.f5 g_183 g_156 g_36 g_236 g_102 g_107 g_685 g_467.f2 g_2 g_174.f2 g_182 g_728 g_460 g_884
 * writes: g_654 g_174.f8 g_235 g_714 g_156 g_728 g_467.f5 g_461
 */
static int32_t * func_78(uint8_t * p_79, const int8_t  p_80, const int32_t * p_81)
{ /* block id: 324 */
    int32_t *l_651 = &g_236;
    int32_t *l_652 = &g_156;
    int32_t *l_653[8][3] = {{&g_236,&g_34,&g_34},{&g_156,&g_34,&g_156},{&g_34,&g_236,(void*)0},{&g_156,&g_156,(void*)0},{&g_236,&g_34,&g_156},{&g_34,&g_156,&g_34},{&g_34,&g_236,&g_156},{&g_236,&g_34,&g_34}};
    struct S0 **l_679 = &g_299;
    uint32_t l_683[8][5] = {{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551611UL,18446744073709551613UL,18446744073709551611UL,18446744073709551615UL,18446744073709551615UL}};
    float l_718 = (-0x8.7p-1);
    int i, j;
    g_654--;
    for (g_174.f8 = (-4); (g_174.f8 == 39); g_174.f8++)
    { /* block id: 328 */
        uint64_t l_662 = 18446744073709551615UL;
        struct S0 **l_680 = &g_299;
        int64_t l_681 = (-9L);
        uint8_t **l_682 = &g_316;
        int8_t l_684 = 0xF3L;
        uint32_t * volatile * volatile *l_687 = &g_688;
        uint32_t * volatile * volatile * volatile *l_686 = &l_687;
        int8_t * const l_694[1] = {&g_107[5]};
        int8_t * const *l_693 = &l_694[0];
        uint16_t l_698 = 0x4230L;
        int16_t *l_710 = &g_209;
        int16_t **l_709 = &l_710;
        int32_t *l_712 = &g_467.f5;
        int32_t l_717 = (-8L);
        int32_t l_719 = 0L;
        int32_t l_722 = 1L;
        int32_t l_724[10][5][5] = {{{1L,0L,0xE2C58FA0L,6L,6L},{0x7A9B4B67L,0xCEDA2C19L,0x7A9B4B67L,0x1AC08895L,0xC4D5F2B8L},{(-1L),(-3L),0x5D4795EFL,(-1L),0x979310B8L},{0x93B8C77DL,0x7A9B4B67L,0xB38D386AL,0xDFBF31C4L,0L},{1L,0xFB524446L,0x5D4795EFL,0x979310B8L,8L}},{{0xA42E1878L,0L,0x7A9B4B67L,1L,0xFE837BC3L},{(-1L),(-10L),0xE2C58FA0L,0L,0x728D326FL},{0xF98345A5L,(-6L),0xA46269B6L,0xC4DC21A2L,0x9E8EAE52L},{1L,0x7FD3A707L,0x89F07253L,9L,1L},{7L,0xE2F8FB0EL,6L,3L,0xC4DC21A2L}},{{0L,1L,0x98B35AB7L,0x48BBEEBAL,0x6C288B1AL},{0x95CAE070L,0xA46269B6L,(-2L),0x61427306L,(-7L)},{8L,1L,(-5L),0L,0x7FD3A707L},{(-3L),(-1L),(-1L),(-3L),0xAB5C1C1EL},{(-1L),0xAE9F1726L,8L,0xFB524446L,0x229DAC8EL}},{{0x6CC8B6D9L,1L,1L,0xE2F8FB0EL,1L},{0x5D4795EFL,(-10L),0xE31D9B47L,0xFB524446L,0x4A83CC39L},{1L,0x1AC08895L,1L,(-3L),0xC4D5F2B8L},{(-1L),0L,0xECF2D6E7L,0L,0L},{1L,0xB38D386AL,0xCCE87406L,0x61427306L,1L}},{{0xECF2D6E7L,0x6C288B1AL,0x4A83CC39L,0x48BBEEBAL,0x979310B8L},{(-2L),(-6L),0x9E8EAE52L,3L,1L},{0x7FD3A707L,0xA243C440L,1L,9L,0L},{(-1L),6L,0xB38D386AL,0xC4DC21A2L,0xE2F8FB0EL},{(-5L),0x979310B8L,0x5566737CL,(-1L),0L}},{{(-10L),0L,0xDFBF31C4L,0L,(-10L)},{(-1L),0x64F39930L,1L,5L,0xE31D9B47L},{0xCEDA2C19L,(-7L),(-3L),0x6CC8B6D9L,0L},{0x6C288B1AL,(-3L),(-3L),0x64F39930L,0xE31D9B47L},{1L,0x6CC8B6D9L,(-3L),(-9L),(-10L)}},{{0xE31D9B47L,(-1L),(-10L),0xE2C58FA0L,0L},{(-6L),0xDFBF31C4L,0x0CED7490L,1L,0xE2F8FB0EL},{0xE1087EE6L,6L,0x64F39930L,1L,0L},{0xDFBF31C4L,0x7A9B4B67L,0L,(-4L),1L},{(-1L),0x34EF41ACL,1L,1L,0x979310B8L}},{{0xF98345A5L,(-3L),0x1AC08895L,0xEFD34D95L,1L},{0xA58A38B8L,0L,0L,(-3L),0L},{0x61427306L,0xC4D5F2B8L,(-7L),(-7L),0xC4D5F2B8L},{0x5EB44682L,1L,0L,0xECF2D6E7L,0x4A83CC39L},{1L,0L,0xFE837BC3L,(-6L),1L}},{{3L,(-1L),0x7FD3A707L,6L,0x229DAC8EL},{1L,8L,0x193EBE9EL,0xFBBFCA93L,0xAB5C1C1EL},{0x5EB44682L,(-1L),1L,0x05FC240CL,0x7FD3A707L},{0x61427306L,3L,(-9L),(-1L),(-7L)},{0xA58A38B8L,1L,0x979310B8L,0x98B35AB7L,0x6C288B1AL}},{{0xF98345A5L,0L,1L,1L,0L},{0L,0x4A83CC39L,1L,(-1L),7L},{0x6CC8B6D9L,0xB38D386AL,0xE2C8192CL,0xFE837BC3L,0L},{1L,0x89F07253L,(-1L),0L,0x48BBEEBAL},{0xCEDA2C19L,0xEA9371F0L,0x93B8C77DL,(-4L),0x93B8C77DL}}};
        int i, j, k;
        if ((safe_add_func_int16_t_s_s((((safe_unary_minus_func_uint64_t_u(l_662)) && ((safe_lshift_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((p_80 != ((l_684 = ((((((((p_80 < (((((((safe_add_func_uint16_t_u_u(g_174.f5, ((((0L != (safe_lshift_func_uint16_t_u_s((safe_sub_func_int32_t_s_s((safe_lshift_func_uint16_t_u_s((3L <= (safe_add_func_int32_t_s_s(((safe_lshift_func_int8_t_s_s(l_662, (l_652 == p_81))) , (*g_183)), (*g_183)))), g_36)), (*l_651))), p_80))) , l_679) == l_680) , g_174.f5))) , l_681) <= l_681) , (void*)0) == l_682) , (-0x1.6p-1)) > p_80)) <= 0x7.B11EB4p+50) == p_80) >= 0x4.AF2D04p-13) < 0xB.0816E5p-16) < l_683[2][1]) <= p_80) > p_80)) , (-1L))), 0x92L)), g_102[6][1])) != l_681)) ^ g_107[5]), 4L)))
        { /* block id: 330 */
            l_686 = g_685;
        }
        else
        { /* block id: 332 */
            int8_t *l_696 = (void*)0;
            int8_t **l_695 = &l_696;
            const int32_t l_701[9][7][4] = {{{0x2C39CD98L,0L,0x3599A88AL,0x2C39CD98L},{0x3599A88AL,0x2C39CD98L,1L,0x5DBAB2EEL},{0xD4BC153DL,0xBAE2B226L,2L,0xFE42344FL},{0xDEE12901L,0x836EA15FL,(-1L),(-1L)},{0xA824CC9CL,0xDF4E50ECL,2L,0x5DBAB2EEL},{5L,0x4B603DD0L,0x5D952B61L,0xDEE12901L},{0xDF4E50ECL,0L,0xB1EF1FF3L,0x8036C46FL}},{{0x38725AF0L,0xD4BC153DL,0L,2L},{0xE91EAF27L,0x2C39CD98L,0x8BB2A4C4L,5L},{0x4B82F9F6L,(-6L),0x3599A88AL,(-7L)},{0x38725AF0L,5L,(-1L),(-1L)},{0xBAE2B226L,0xBAE2B226L,0x5D952B61L,(-5L)},{0xFC3680D7L,(-4L),0x485E7848L,0xDEE12901L},{0xA824CC9CL,0xE91EAF27L,8L,0x485E7848L}},{{0x2C39CD98L,0xE91EAF27L,2L,0xDEE12901L},{0xE91EAF27L,(-4L),(-6L),(-5L)},{0x3599A88AL,0xBAE2B226L,0L,(-1L)},{0x4B603DD0L,5L,(-3L),(-7L)},{0L,(-6L),2L,5L},{8L,0x2C39CD98L,0x485E7848L,2L},{0xBAE2B226L,0xD4BC153DL,(-3L),0x8036C46FL}},{{0xDEE12901L,0L,0x83E6DF3CL,0xDEE12901L},{0x3599A88AL,0x4B603DD0L,0xA824CC9CL,0x5DBAB2EEL},{0x4B82F9F6L,0xDF4E50ECL,2L,(-1L)},{0x4B603DD0L,0x836EA15FL,0xB1EF1FF3L,0xFE42344FL},{0xA824CC9CL,0xBAE2B226L,0L,0x5DBAB2EEL},{(-5L),0x2C39CD98L,0x5D952B61L,0x2C39CD98L},{0L,0L,8L,0xE50E91ABL}},{{0x38725AF0L,0x4B82F9F6L,0x83E6DF3CL,2L},{0xD4BC153DL,0xDEE12901L,0x8BB2A4C4L,(-5L)},{0xD4BC153DL,(-6L),0x83E6DF3CL,(-1L)},{0x38725AF0L,(-5L),8L,(-1L)},{0L,0xDF4E50ECL,0x5D952B61L,0xFC3680D7L},{(-5L),(-4L),0L,0x4B603DD0L},{0xA824CC9CL,0xD4BC153DL,0xB1EF1FF3L,0x485E7848L}},{{0x4B603DD0L,0x4B82F9F6L,2L,0x2C39CD98L},{0x4B82F9F6L,(-4L),0xA824CC9CL,5L},{0x3599A88AL,0L,0x83E6DF3CL,(-1L)},{0xDEE12901L,0xFC3680D7L,(-3L),0xFE42344FL},{0xBAE2B226L,(-6L),0x485E7848L,0xFC3680D7L},{8L,0x4B603DD0L,2L,2L},{0L,0xE91EAF27L,(-3L),6L}},{{0L,0x7943931DL,0x7943931DL,0L},{1L,0x3599A88AL,0x0BACC384L,(-1L)},{0x836EA15FL,0L,0xFC3680D7L,0x485E7848L},{0x83E6DF3CL,(-3L),0x4B239331L,0x485E7848L},{0xD5020EB6L,0L,0x619D96BFL,(-1L)},{8L,0x3599A88AL,0xBAE2B226L,0L},{0L,0x7943931DL,0x05D0C503L,0xA824CC9CL}},{{1L,0x836EA15FL,1L,(-10L)},{0x5DBAB2EEL,0L,0xC2554385L,8L},{0x836EA15FL,0x0BACC384L,0x7943931DL,0L},{1L,8L,0x5D952B61L,0x05D0C503L},{0xED5FE6FCL,0L,0xBAE2B226L,(-1L)},{(-1L),2L,0xF81060DBL,0x83E6DF3CL},{0xD5020EB6L,0x5DBAB2EEL,0x05D0C503L,0x619D96BFL}},{{0x3599A88AL,8L,0xFC3680D7L,0L},{8L,2L,0xA54655FDL,8L},{1L,0xED5FE6FCL,1L,0x05D0C503L},{0x83E6DF3CL,0xB1EF1FF3L,6L,2L},{0xED5FE6FCL,0x0BACC384L,0x8BB2A4C4L,0xB1EF1FF3L},{0L,0x3599A88AL,0x8BB2A4C4L,(-10L)},{0xED5FE6FCL,0x5DBAB2EEL,6L,1L}}};
            int32_t l_703 = 0x06A1AFFBL;
            int16_t *l_704 = &g_235[0];
            int32_t l_720 = 0xDBD6FB7FL;
            int32_t l_721 = 0L;
            int32_t l_723 = 9L;
            int32_t l_726[10][6] = {{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L},{(-8L),0x40B65088L,(-5L),0x50C116BFL,(-5L),0x40B65088L}};
            int i, j, k;
            if (((safe_div_func_int32_t_s_s(((safe_mul_func_int64_t_s_s((l_693 == l_695), ((0xA6L || (+(((void*)0 != &p_79) == ((l_698 , (safe_mul_func_uint8_t_u_u((l_701[2][2][1] || g_467.f2), (!((*l_704) = (((((((0x1B0FL | 1UL) | l_681) , 1UL) ^ (-1L)) , p_80) , l_703) > 0x8AL)))))) == l_684)))) > l_698))) | l_701[5][5][0]), 0x53FE2603L)) > 0UL))
            { /* block id: 334 */
                int16_t **l_711 = &l_704;
                int32_t **l_713[1];
                int8_t ***l_715 = (void*)0;
                int32_t l_716[6][3];
                int i, j;
                for (i = 0; i < 1; i++)
                    l_713[i] = &l_712;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_716[i][j] = 0x7BA05042L;
                }
                (**g_182) = (((safe_lshift_func_uint16_t_u_u(((0xDAL == (((safe_mul_func_uint16_t_u_u((l_709 != l_711), ((g_714 = l_712) != (((((l_716[0][2] = ((&l_695 == (((*g_183) ^ (-1L)) , l_715)) >= (*p_79))) <= p_80) < l_701[2][2][1]) , p_80) , (void*)0)))) <= (*p_79)) <= 253UL)) != g_174.f2), l_684)) == 6UL) | 0xF0B12B1D51664BE3LL);
                g_728++;
            }
            else
            { /* block id: 339 */
                return &g_34;
            }
            for (g_467.f5 = 7; (g_467.f5 >= 2); g_467.f5 -= 1)
            { /* block id: 344 */
                (*g_460) = &l_722;
            }
        }
    }
    return l_653[3][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_85 g_36 g_104 g_105 g_88 g_102 g_107 g_34 g_157 g_139 g_156 g_180 g_183 g_174.f8 g_174.f4 g_211 g_210 g_174.f6 g_182 g_174.f0 g_174.f2 g_239 g_240 g_209 g_174.f7 g_236 g_315 g_316
 * writes: g_85 g_102 g_104 g_107 g_139 g_157 g_170 g_156 g_180 g_174.f2 g_211 g_174.f0 g_209 g_235 g_236 g_174.f8 g_88 g_292 g_299 g_291
 */
static uint8_t * func_82(uint32_t  p_83)
{ /* block id: 26 */
    uint8_t *l_100[9] = {&g_2,(void*)0,&g_2,(void*)0,&g_2,(void*)0,&g_2,(void*)0,&g_2};
    int32_t l_118 = 3L;
    int32_t l_121 = 0xFA9739A1L;
    const int32_t ***l_202 = (void*)0;
    const int32_t **** const l_201 = &l_202;
    const int32_t **** const *l_200 = &l_201;
    uint16_t l_237 = 0x39A4L;
    int64_t *l_293[7] = {&g_174.f2,&g_292,&g_292,&g_174.f2,&g_292,&g_292,&g_174.f2};
    uint32_t l_347 = 0x7116458DL;
    float l_348 = (-0x6.3p-1);
    int32_t **l_349[10][2][9] = {{{&g_183,(void*)0,&g_183,&g_183,&g_183,&g_183,(void*)0,&g_183,&g_183},{&g_183,&g_183,(void*)0,&g_183,&g_183,&g_183,(void*)0,&g_183,&g_183}},{{&g_183,&g_183,&g_183,&g_183,(void*)0,&g_183,&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183}},{{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183,(void*)0,(void*)0,&g_183,&g_183,&g_183,&g_183}},{{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,(void*)0,&g_183},{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,(void*)0,&g_183,&g_183}},{{(void*)0,&g_183,&g_183,(void*)0,(void*)0,&g_183,&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183}},{{(void*)0,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183}},{{&g_183,(void*)0,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183}},{{&g_183,&g_183,(void*)0,&g_183,&g_183,&g_183,(void*)0,&g_183,(void*)0},{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183}},{{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183},{&g_183,(void*)0,&g_183,&g_183,(void*)0,&g_183,(void*)0,(void*)0,(void*)0}},{{&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,(void*)0},{&g_183,&g_183,&g_183,&g_183,&g_183,(void*)0,(void*)0,&g_183,&g_183}}};
    struct S0 **l_356 = &g_299;
    float *l_384[10][8][3] = {{{&l_348,&l_348,&g_170},{&l_348,(void*)0,(void*)0},{(void*)0,(void*)0,&l_348},{(void*)0,&g_291,&l_348},{&l_348,&g_170,&g_291},{&l_348,&g_170,&l_348},{(void*)0,&l_348,&g_291},{&l_348,&l_348,&l_348}},{{&l_348,&g_170,&l_348},{&l_348,&g_170,(void*)0},{(void*)0,&l_348,&g_170},{&g_170,&l_348,(void*)0},{(void*)0,&g_170,&g_170},{&g_170,&g_170,&l_348},{&l_348,(void*)0,(void*)0},{&g_170,(void*)0,(void*)0}},{{&l_348,&g_291,&l_348},{&g_170,&l_348,&l_348},{&l_348,&g_291,(void*)0},{(void*)0,&l_348,&l_348},{(void*)0,&g_291,&l_348},{(void*)0,(void*)0,&l_348},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_291,&g_170}},{{(void*)0,&l_348,(void*)0},{&l_348,(void*)0,&g_170},{&g_170,&l_348,(void*)0},{&l_348,&g_170,&l_348},{&g_170,&g_170,&l_348},{&l_348,&l_348,&l_348},{&g_170,(void*)0,(void*)0},{&g_291,&l_348,&l_348}},{{&g_170,&g_291,&l_348},{&l_348,(void*)0,(void*)0},{&g_170,(void*)0,(void*)0},{&l_348,&g_291,&l_348},{&g_170,&l_348,&l_348},{&l_348,&g_291,(void*)0},{(void*)0,&l_348,&l_348},{(void*)0,&g_291,&l_348}},{{(void*)0,(void*)0,&l_348},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_291,&g_170},{(void*)0,&l_348,(void*)0},{&l_348,(void*)0,&g_170},{&g_170,&l_348,(void*)0},{&l_348,&g_170,&l_348},{&g_170,&g_170,&l_348}},{{&l_348,&l_348,&l_348},{&g_170,(void*)0,(void*)0},{&g_291,&l_348,&l_348},{&g_170,&g_291,&l_348},{&l_348,(void*)0,(void*)0},{&g_170,(void*)0,(void*)0},{&l_348,&g_291,&l_348},{&g_170,&l_348,&l_348}},{{&l_348,&g_291,(void*)0},{(void*)0,&l_348,&l_348},{(void*)0,&g_291,&l_348},{(void*)0,(void*)0,&l_348},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_291,&g_170},{(void*)0,&l_348,(void*)0},{&l_348,(void*)0,&g_170}},{{&g_170,&l_348,(void*)0},{&l_348,&g_170,&l_348},{&g_170,&g_170,&l_348},{&l_348,&l_348,&l_348},{&g_170,(void*)0,(void*)0},{&g_291,&l_348,&l_348},{&g_170,&g_291,&l_348},{&l_348,(void*)0,(void*)0}},{{&g_170,(void*)0,(void*)0},{&l_348,&g_291,&l_348},{&g_170,&l_348,&l_348},{&l_348,&g_291,(void*)0},{(void*)0,&l_348,&l_348},{(void*)0,(void*)0,&l_348},{&l_348,&l_348,&g_170},{&l_348,(void*)0,&l_348}}};
    uint8_t ***l_427 = &g_315[3];
    const int32_t l_570 = 2L;
    int32_t l_596 = 0x315A43EBL;
    int i, j, k;
    for (p_83 = 0; (p_83 <= 0); p_83 += 1)
    { /* block id: 29 */
        int64_t l_91 = 0x0F1475AC3517D3E8LL;
        int32_t l_155 = 1L;
        int32_t *l_165 = (void*)0;
        int32_t **l_164 = &l_165;
        uint32_t l_194[10] = {18446744073709551615UL,0x0BA58DE8L,18446744073709551615UL,0x0BA58DE8L,18446744073709551615UL,0x0BA58DE8L,18446744073709551615UL,0x0BA58DE8L,18446744073709551615UL,0x0BA58DE8L};
        int32_t l_196 = 1L;
        uint8_t *l_285 = &g_2;
        struct S0 *l_296 = &g_297[2][2][0];
        int i;
        for (g_85 = 0; (g_85 <= 0); g_85 += 1)
        { /* block id: 32 */
            uint8_t *l_99 = &g_36;
            uint16_t *l_101 = &g_102[6][1];
            uint32_t *l_103 = &g_104;
            int8_t *l_106 = &g_107[5];
            uint32_t l_133 = 1UL;
            int32_t l_154 = (-1L);
            const int32_t *l_168 = &l_154;
            const int32_t **l_167 = &l_168;
            uint64_t l_195[3];
            int i;
            for (i = 0; i < 3; i++)
                l_195[i] = 1UL;
            if (((safe_rshift_func_int16_t_s_s((((l_91 == ((safe_rshift_func_uint8_t_u_u((2L == (((safe_div_func_int8_t_s_s(((*l_106) = (safe_rshift_func_int16_t_s_u((((((((safe_unary_minus_func_int32_t_s(((((((&p_83 == (void*)0) , ((*l_101) = (g_85 == (l_99 != l_100[2])))) , &g_85) == (((*l_103) ^= g_36) , l_103)) , 0x58EF108C6EC2A3D9LL) || 0L))) != g_105) < l_91) == 3UL) ^ 18446744073709551609UL) == g_88[1][4][0]) >= p_83), g_36))), p_83)) , l_99) != (void*)0)), 7)) > p_83)) && p_83) != p_83), 15)) , p_83))
            { /* block id: 36 */
                uint32_t l_134 = 1UL;
                int32_t *l_135 = &l_118;
                (*l_135) = (safe_rshift_func_int8_t_s_s(p_83, ((*l_106) = (safe_unary_minus_func_int32_t_s((((safe_div_func_uint64_t_u_u(((safe_div_func_int32_t_s_s(((((((safe_sub_func_int64_t_s_s((safe_unary_minus_func_uint32_t_u((l_118 , (safe_lshift_func_uint8_t_u_s((l_121 ^= l_118), (g_88[1][3][0] >= ((*l_101)--))))))), (((((safe_mul_func_int8_t_s_s((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((~((((+(((!(g_105 >= g_85)) <= ((g_88[1][2][0] >= l_133) == 0x1.Bp+1)) , 0x5.741E93p-57)) <= 0x8.5642B8p-59) != 0x1.402E79p+23) , g_107[5])), 11)), l_133)), 0UL)) && 7L) < g_34) <= 0x70A8L) , p_83))) , (void*)0) != &g_104) > 18446744073709551615UL) < g_105) || 0x31B5L), p_83)) & l_134), 0xCEAEEF4A51DBEEE9LL)) >= p_83) <= 0x85E37C3EL))))));
                if (p_83)
                    continue;
            }
            else
            { /* block id: 42 */
                int64_t l_153 = (-7L);
                int32_t l_160 = 0L;
                int32_t ***l_166 = &l_164;
                float *l_169 = &g_170;
                if ((safe_unary_minus_func_int8_t_s((safe_rshift_func_uint16_t_u_s(0xE26AL, 1)))))
                { /* block id: 43 */
                    g_139 = (void*)0;
                }
                else
                { /* block id: 45 */
                    int32_t *l_150[6];
                    int32_t **l_149 = &l_150[0];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_150[i] = &l_118;
                    if ((safe_sub_func_int32_t_s_s(((safe_div_func_int8_t_s_s(((0x0792702ECBAF5A79LL > (safe_mod_func_uint32_t_u_u((safe_add_func_uint16_t_u_u((g_102[6][1] <= (l_149 == &g_30)), (safe_rshift_func_uint8_t_u_u((0x2C0FF22B7714DF77LL & (l_133 == p_83)), 3)))), (((l_91 ^ (l_153 && 18446744073709551612UL)) || l_91) && 4294967295UL)))) == g_34), g_107[6])) == g_102[3][2]), g_85)))
                    { /* block id: 46 */
                        g_157--;
                    }
                    else
                    { /* block id: 48 */
                        return &g_36;
                    }
                    for (l_153 = 0; (l_153 <= 0); l_153 += 1)
                    { /* block id: 53 */
                        uint16_t l_161 = 0x7AAFL;
                        ++l_161;
                        return l_106;
                    }
                    if (g_102[2][0])
                        break;
                }
                (*l_169) = ((((*l_166) = l_164) == l_167) < (-0x3.0p-1));
                if (g_157)
                    continue;
            }
            for (l_155 = 0; (l_155 <= 0); l_155 += 1)
            { /* block id: 65 */
                const struct S0 *l_173 = &g_174;
                int32_t *l_175 = (void*)0;
                int32_t *l_176 = (void*)0;
                int32_t *l_177 = &l_154;
                int i, j, k;
                (*l_177) ^= ((safe_add_func_uint64_t_u_u(18446744073709551615UL, (g_139 == l_173))) || (g_88[(l_155 + 3)][(l_155 + 7)][g_85] ^ (g_105 < 0x906447CF268A7AFDLL)));
                for (g_104 = 0; (g_104 <= 8); g_104 += 1)
                { /* block id: 69 */
                    uint16_t l_178 = 65532UL;
                    (*l_167) = &g_34;
                    l_178 &= (p_83 != g_102[6][1]);
                }
                for (l_133 = 0; (l_133 <= 0); l_133 += 1)
                { /* block id: 75 */
                    int8_t l_179[10];
                    int32_t * volatile ** volatile **l_184 = &g_180;
                    uint32_t *l_187 = (void*)0;
                    int32_t l_193 = (-6L);
                    int i;
                    for (i = 0; i < 10; i++)
                        l_179[i] = 0x4FL;
                    g_156 &= l_179[5];
                    (*l_184) = g_180;
                    for (g_174.f2 = 0; g_174.f2 < 9; g_174.f2 += 1)
                    {
                        g_107[g_174.f2] = (-9L);
                    }
                    for (l_121 = 0; (l_121 >= 0); l_121 -= 1)
                    { /* block id: 81 */
                        uint32_t *l_188 = &g_104;
                        float *l_191 = &g_170;
                        float *l_192 = (void*)0;
                        (*g_183) = 0xB8141019L;
                        l_194[6] = (l_121 == (safe_add_func_float_f_f(((l_193 = ((g_107[7] >= (l_187 != l_188)) > (safe_div_func_float_f_f(((g_102[6][1] != ((*l_191) = (-0x1.8p+1))) <= (g_174.f8 , 0x1.80367Ap+93)), (-0x4.Ep-1))))) < p_83), 0x5.1819CCp-12)));
                        if (l_195[0])
                            continue;
                        l_196 = 0L;
                    }
                }
            }
            for (l_91 = 0; (l_91 <= 0); l_91 += 1)
            { /* block id: 93 */
                float *l_197[3];
                int32_t l_198 = 6L;
                int i;
                for (i = 0; i < 3; i++)
                    l_197[i] = (void*)0;
                l_198 = g_174.f4;
                for (g_174.f2 = 0; (g_174.f2 <= 0); g_174.f2 += 1)
                { /* block id: 97 */
                    uint32_t l_199 = 4294967287UL;
                    if (l_199)
                        break;
                    for (l_198 = 0; (l_198 >= 0); l_198 -= 1)
                    { /* block id: 101 */
                        int32_t l_219 = 0x13514C0AL;
                        int32_t *l_220[4] = {&l_118,&l_118,&l_118,&l_118};
                        int i;
                        g_174.f0 ^= (((l_198 || ((void*)0 != l_200)) , 2UL) , (safe_lshift_func_int16_t_s_u(((safe_div_func_int8_t_s_s(p_83, ((safe_rshift_func_uint8_t_u_s((--g_211), ((*l_106) = ((((+(g_210[4] && g_174.f6)) & g_102[2][3]) != (safe_add_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(p_83, l_198)), 0x3FL))) ^ (**g_182))))) , l_219))) , p_83), 12)));
                        return &g_36;
                    }
                }
            }
        }
        for (g_209 = 0; (g_209 <= 0); g_209 += 1)
        { /* block id: 112 */
            uint8_t l_238[8][1][7] = {{{0x43L,0x09L,0xD2L,3UL,0x44L,0x44L,3UL}},{{6UL,9UL,6UL,0x09L,3UL,6UL,1UL}},{{3UL,9UL,0x57L,0UL,9UL,0xD3L,9UL}},{{255UL,0x09L,0x09L,255UL,1UL,6UL,3UL}},{{0x44L,0x43L,0x09L,0xD2L,3UL,0x44L,0x44L}},{{0x43L,3UL,0x57L,3UL,0x43L,4UL,3UL}},{{255UL,1UL,6UL,3UL,0x09L,6UL,9UL}},{{0x09L,9UL,0xD2L,0xD2L,9UL,0x09L,1UL}}};
            const int8_t l_276[6][8][1] = {{{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L}},{{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L}},{{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L}},{{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L}},{{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L}},{{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L},{1L},{0xB7L}}};
            int64_t *l_295 = (void*)0;
            int i, j, k;
            for (g_157 = 0; (g_157 <= 0); g_157 += 1)
            { /* block id: 115 */
                int64_t l_233 = 0L;
                uint8_t *l_257 = &l_238[5][0][2];
                uint16_t l_268 = 0UL;
                int32_t *l_300[8][2][3] = {{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}},{{(void*)0,(void*)0,(void*)0},{&g_156,&g_156,&g_156}}};
                int i, j, k;
                for (g_104 = 0; (g_104 <= 0); g_104 += 1)
                { /* block id: 118 */
                    int64_t *l_230 = &g_174.f2;
                    int16_t *l_234 = &g_235[0];
                    uint32_t **l_242 = &g_240;
                    int i, j, k;
                    if (((((g_88[(g_104 + 2)][g_104][g_157] , (((safe_sub_func_int64_t_s_s(p_83, (-1L))) & ((safe_rshift_func_uint8_t_u_u((!(safe_div_func_int64_t_s_s((-1L), ((*l_230) |= (safe_div_func_uint8_t_u_u(g_85, p_83)))))), (l_237 = (safe_add_func_int32_t_s_s(((0x9534L > (g_236 = ((*l_234) = (0x1F19B35F8E2DF95CLL & (l_233 & 6UL))))) , g_88[(g_104 + 2)][g_104][g_157]), 0x9B1E83F6L))))) > 1L)) <= l_233)) , g_174.f0) != l_238[7][0][4]) , (*g_183)))
                    { /* block id: 123 */
                        return &g_36;
                    }
                    else
                    { /* block id: 125 */
                        uint32_t ***l_241 = (void*)0;
                        l_242 = g_239;
                        (**g_182) &= g_88[(g_104 + 2)][g_104][g_157];
                    }
                    for (l_237 = 0; (l_237 <= 0); l_237 += 1)
                    { /* block id: 131 */
                        uint16_t *l_247[8][1];
                        uint8_t **l_256 = &l_100[2];
                        int32_t *l_261 = &l_121;
                        int i, j;
                        for (i = 0; i < 8; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_247[i][j] = &g_174.f8;
                        }
                        (*g_183) ^= (-3L);
                        (*l_261) &= (((*g_183) = (safe_rshift_func_uint16_t_u_u((++g_102[(g_104 + 3)][g_104]), (g_174.f8 = p_83)))) , ((**g_182) = (safe_lshift_func_int8_t_s_s((safe_mod_func_uint8_t_u_u(p_83, (safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(((((0x2D6A1B3C1EEC739DLL & (l_165 == (((l_257 = ((*l_256) = &g_2)) == &l_238[7][0][4]) , (*g_239)))) == (g_102[6][1] = (g_156 ^ (safe_rshift_func_int16_t_s_u(((safe_unary_minus_func_uint64_t_u((p_83 >= (-3L)))) || 18446744073709551614UL), 4))))) < g_105) == g_88[(g_104 + 2)][g_104][g_157]), 6)), p_83)))), g_104))));
                    }
                }
                (*g_183) ^= (safe_unary_minus_func_uint64_t_u((safe_add_func_uint8_t_u_u((l_268 = (!(++g_88[(g_209 + 2)][g_157][g_157]))), (3L >= ((l_118 = (!(((l_238[5][0][5] , g_209) & ((g_292 = (((**g_239)++) != (safe_rshift_func_uint8_t_u_s(((safe_sub_func_uint64_t_u_u(l_276[1][7][0], ((safe_mod_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(((safe_sub_func_uint8_t_u_u(0x0DL, (((safe_mod_func_uint32_t_u_u(((void*)0 != l_285), ((safe_sub_func_int16_t_s_s((0xC38EL >= (safe_div_func_uint32_t_u_u((l_121 |= (safe_unary_minus_func_uint32_t_u(0x2619962EL))), p_83))), 0x7361L)) | g_107[4]))) < p_83) | p_83))) || p_83), p_83)), g_174.f7)) , 0UL))) ^ 7L), g_174.f0)))) == l_233)) != 0x36L))) || g_174.f6))))));
                if (((((l_293[4] == &l_91) <= p_83) == (((!(((&l_91 == (l_295 = &g_292)) | (l_233 , ((g_174.f2 = (p_83 != ((((g_104 && 0x4DL) && 255UL) > g_36) == 1UL))) || g_88[(g_209 + 2)][g_157][g_157]))) < p_83)) < p_83) >= g_209)) & p_83))
                { /* block id: 152 */
                    struct S0 **l_298[3][3] = {{&l_296,&l_296,&l_296},{&l_296,&l_296,&l_296},{&l_296,&l_296,&l_296}};
                    int i, j;
                    g_299 = l_296;
                    (*g_183) ^= (&p_83 != (void*)0);
                    for (l_118 = 0; (l_118 <= 3); l_118 += 1)
                    { /* block id: 157 */
                        int32_t *l_301 = &l_121;
                        int i;
                        l_301 = l_300[6][0][0];
                        (**g_182) = 0xE18812C0L;
                        if (g_107[(g_209 + 8)])
                            continue;
                    }
                    if ((*g_183))
                        break;
                }
                else
                { /* block id: 163 */
                    for (l_91 = 8; (l_91 >= 26); l_91++)
                    { /* block id: 166 */
                        return &g_2;
                    }
                }
            }
        }
    }
    for (g_236 = 15; (g_236 == 26); g_236 = safe_add_func_uint8_t_u_u(g_236, 6))
    { /* block id: 175 */
        int32_t *l_306 = &g_236;
        int32_t **l_307 = &l_306;
        (*l_307) = l_306;
    }
    for (l_121 = 0; (l_121 > 23); l_121 = safe_add_func_uint8_t_u_u(l_121, 5))
    { /* block id: 180 */
        uint8_t **l_319 = &g_316;
        int32_t l_333 = 0xDFF827C7L;
        struct S0 **l_338 = &g_299;
        struct S0 **l_355 = &g_299;
        float *l_364[10][5][5] = {{{&g_291,(void*)0,(void*)0,&g_291,&l_348},{&g_291,&l_348,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291}},{{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348}},{{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291}},{{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348}},{{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291}},{{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348}},{{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291}},{{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,(void*)0,&l_348},{&l_348,&g_291,&g_291,&l_348,&g_291},{(void*)0,&g_291,&g_291,&g_291,&l_348}},{{&l_348,&l_348,&l_348,&l_348,(void*)0},{&g_291,(void*)0,(void*)0,&g_291,&l_348},{&l_348,&l_348,&l_348,&l_348,(void*)0},{&g_291,(void*)0,(void*)0,&g_291,&l_348},{&l_348,&l_348,&l_348,&l_348,(void*)0}},{{&g_291,(void*)0,(void*)0,&g_291,&l_348},{&l_348,&l_348,&l_348,&l_348,(void*)0},{&g_291,(void*)0,(void*)0,&g_291,&l_348},{&l_348,&l_348,&l_348,&l_348,(void*)0},{&g_291,(void*)0,(void*)0,&g_291,&l_348}}};
        int32_t l_365 = (-1L);
        const int8_t **l_642 = (void*)0;
        const int8_t **l_643 = (void*)0;
        const int8_t **l_644 = (void*)0;
        const int8_t *l_646 = (void*)0;
        const int8_t **l_645 = &l_646;
        int32_t *l_647 = &g_236;
        int i, j, k;
    }
    g_291 = p_83;
    return (**l_427);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_31, "g_31", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_88[i][j][k], "g_88[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_102[i][j], "g_102[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_107[i], "g_107[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_140.f0, "g_140.f0", print_hash_value);
    transparent_crc(g_140.f1, "g_140.f1", print_hash_value);
    transparent_crc(g_140.f2, "g_140.f2", print_hash_value);
    transparent_crc(g_140.f3, "g_140.f3", print_hash_value);
    transparent_crc_bytes (&g_140.f4, sizeof(g_140.f4), "g_140.f4", print_hash_value);
    transparent_crc(g_140.f5, "g_140.f5", print_hash_value);
    transparent_crc(g_140.f6, "g_140.f6", print_hash_value);
    transparent_crc(g_140.f7, "g_140.f7", print_hash_value);
    transparent_crc(g_140.f8, "g_140.f8", print_hash_value);
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc(g_157, "g_157", print_hash_value);
    transparent_crc_bytes (&g_170, sizeof(g_170), "g_170", print_hash_value);
    transparent_crc(g_174.f0, "g_174.f0", print_hash_value);
    transparent_crc(g_174.f1, "g_174.f1", print_hash_value);
    transparent_crc(g_174.f2, "g_174.f2", print_hash_value);
    transparent_crc(g_174.f3, "g_174.f3", print_hash_value);
    transparent_crc_bytes (&g_174.f4, sizeof(g_174.f4), "g_174.f4", print_hash_value);
    transparent_crc(g_174.f5, "g_174.f5", print_hash_value);
    transparent_crc(g_174.f6, "g_174.f6", print_hash_value);
    transparent_crc(g_174.f7, "g_174.f7", print_hash_value);
    transparent_crc(g_174.f8, "g_174.f8", print_hash_value);
    transparent_crc(g_209, "g_209", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_210[i], "g_210[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_211, "g_211", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_235[i], "g_235[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_236, "g_236", print_hash_value);
    transparent_crc_bytes (&g_291, sizeof(g_291), "g_291", print_hash_value);
    transparent_crc(g_292, "g_292", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_297[i][j][k].f0, "g_297[i][j][k].f0", print_hash_value);
                transparent_crc(g_297[i][j][k].f1, "g_297[i][j][k].f1", print_hash_value);
                transparent_crc(g_297[i][j][k].f2, "g_297[i][j][k].f2", print_hash_value);
                transparent_crc(g_297[i][j][k].f3, "g_297[i][j][k].f3", print_hash_value);
                transparent_crc_bytes(&g_297[i][j][k].f4, sizeof(g_297[i][j][k].f4), "g_297[i][j][k].f4", print_hash_value);
                transparent_crc(g_297[i][j][k].f5, "g_297[i][j][k].f5", print_hash_value);
                transparent_crc(g_297[i][j][k].f6, "g_297[i][j][k].f6", print_hash_value);
                transparent_crc(g_297[i][j][k].f7, "g_297[i][j][k].f7", print_hash_value);
                transparent_crc(g_297[i][j][k].f8, "g_297[i][j][k].f8", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_467.f0, "g_467.f0", print_hash_value);
    transparent_crc(g_467.f1, "g_467.f1", print_hash_value);
    transparent_crc(g_467.f2, "g_467.f2", print_hash_value);
    transparent_crc(g_467.f3, "g_467.f3", print_hash_value);
    transparent_crc_bytes (&g_467.f4, sizeof(g_467.f4), "g_467.f4", print_hash_value);
    transparent_crc(g_467.f5, "g_467.f5", print_hash_value);
    transparent_crc(g_467.f6, "g_467.f6", print_hash_value);
    transparent_crc(g_467.f7, "g_467.f7", print_hash_value);
    transparent_crc(g_467.f8, "g_467.f8", print_hash_value);
    transparent_crc(g_481, "g_481", print_hash_value);
    transparent_crc(g_575, "g_575", print_hash_value);
    transparent_crc(g_616, "g_616", print_hash_value);
    transparent_crc(g_619, "g_619", print_hash_value);
    transparent_crc(g_654, "g_654", print_hash_value);
    transparent_crc_bytes (&g_725, sizeof(g_725), "g_725", print_hash_value);
    transparent_crc(g_727, "g_727", print_hash_value);
    transparent_crc(g_728, "g_728", print_hash_value);
    transparent_crc(g_797, "g_797", print_hash_value);
    transparent_crc(g_823, "g_823", print_hash_value);
    transparent_crc(g_824, "g_824", print_hash_value);
    transparent_crc_bytes (&g_834, sizeof(g_834), "g_834", print_hash_value);
    transparent_crc(g_881, "g_881", print_hash_value);
    transparent_crc(g_883, "g_883", print_hash_value);
    transparent_crc(g_884, "g_884", print_hash_value);
    transparent_crc(g_920, "g_920", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_963[i], "g_963[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_999, "g_999", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1009[i].f0, "g_1009[i].f0", print_hash_value);
        transparent_crc(g_1009[i].f1, "g_1009[i].f1", print_hash_value);
        transparent_crc(g_1009[i].f2, "g_1009[i].f2", print_hash_value);
        transparent_crc(g_1009[i].f3, "g_1009[i].f3", print_hash_value);
        transparent_crc_bytes(&g_1009[i].f4, sizeof(g_1009[i].f4), "g_1009[i].f4", print_hash_value);
        transparent_crc(g_1009[i].f5, "g_1009[i].f5", print_hash_value);
        transparent_crc(g_1009[i].f6, "g_1009[i].f6", print_hash_value);
        transparent_crc(g_1009[i].f7, "g_1009[i].f7", print_hash_value);
        transparent_crc(g_1009[i].f8, "g_1009[i].f8", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1180, "g_1180", print_hash_value);
    transparent_crc(g_1245, "g_1245", print_hash_value);
    transparent_crc(g_1246, "g_1246", print_hash_value);
    transparent_crc_bytes (&g_1247, sizeof(g_1247), "g_1247", print_hash_value);
    transparent_crc(g_1248, "g_1248", print_hash_value);
    transparent_crc(g_1269, "g_1269", print_hash_value);
    transparent_crc(g_1324, "g_1324", print_hash_value);
    transparent_crc_bytes (&g_1370, sizeof(g_1370), "g_1370", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1476[i], "g_1476[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1478, sizeof(g_1478), "g_1478", print_hash_value);
    transparent_crc(g_1479, "g_1479", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1483[i][j][k], "g_1483[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1558.f0, "g_1558.f0", print_hash_value);
    transparent_crc(g_1558.f1, "g_1558.f1", print_hash_value);
    transparent_crc(g_1558.f2, "g_1558.f2", print_hash_value);
    transparent_crc(g_1558.f3, "g_1558.f3", print_hash_value);
    transparent_crc_bytes (&g_1558.f4, sizeof(g_1558.f4), "g_1558.f4", print_hash_value);
    transparent_crc(g_1558.f5, "g_1558.f5", print_hash_value);
    transparent_crc(g_1558.f6, "g_1558.f6", print_hash_value);
    transparent_crc(g_1558.f7, "g_1558.f7", print_hash_value);
    transparent_crc(g_1558.f8, "g_1558.f8", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1660[i], "g_1660[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1662, "g_1662", print_hash_value);
    transparent_crc(g_1670.f0, "g_1670.f0", print_hash_value);
    transparent_crc(g_1670.f1, "g_1670.f1", print_hash_value);
    transparent_crc(g_1670.f2, "g_1670.f2", print_hash_value);
    transparent_crc(g_1670.f3, "g_1670.f3", print_hash_value);
    transparent_crc_bytes (&g_1670.f4, sizeof(g_1670.f4), "g_1670.f4", print_hash_value);
    transparent_crc(g_1670.f5, "g_1670.f5", print_hash_value);
    transparent_crc(g_1670.f6, "g_1670.f6", print_hash_value);
    transparent_crc(g_1670.f7, "g_1670.f7", print_hash_value);
    transparent_crc(g_1670.f8, "g_1670.f8", print_hash_value);
    transparent_crc(g_1674, "g_1674", print_hash_value);
    transparent_crc(g_1765, "g_1765", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1785[i].f0, "g_1785[i].f0", print_hash_value);
        transparent_crc(g_1785[i].f1, "g_1785[i].f1", print_hash_value);
        transparent_crc(g_1785[i].f2, "g_1785[i].f2", print_hash_value);
        transparent_crc(g_1785[i].f3, "g_1785[i].f3", print_hash_value);
        transparent_crc_bytes(&g_1785[i].f4, sizeof(g_1785[i].f4), "g_1785[i].f4", print_hash_value);
        transparent_crc(g_1785[i].f5, "g_1785[i].f5", print_hash_value);
        transparent_crc(g_1785[i].f6, "g_1785[i].f6", print_hash_value);
        transparent_crc(g_1785[i].f7, "g_1785[i].f7", print_hash_value);
        transparent_crc(g_1785[i].f8, "g_1785[i].f8", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1823, "g_1823", print_hash_value);
    transparent_crc(g_1825, "g_1825", print_hash_value);
    transparent_crc_bytes (&g_1829, sizeof(g_1829), "g_1829", print_hash_value);
    transparent_crc(g_1830, "g_1830", print_hash_value);
    transparent_crc(g_1854.f0, "g_1854.f0", print_hash_value);
    transparent_crc(g_1854.f1, "g_1854.f1", print_hash_value);
    transparent_crc(g_1854.f2, "g_1854.f2", print_hash_value);
    transparent_crc(g_1854.f3, "g_1854.f3", print_hash_value);
    transparent_crc_bytes (&g_1854.f4, sizeof(g_1854.f4), "g_1854.f4", print_hash_value);
    transparent_crc(g_1854.f5, "g_1854.f5", print_hash_value);
    transparent_crc(g_1854.f6, "g_1854.f6", print_hash_value);
    transparent_crc(g_1854.f7, "g_1854.f7", print_hash_value);
    transparent_crc(g_1854.f8, "g_1854.f8", print_hash_value);
    transparent_crc(g_1904.f0, "g_1904.f0", print_hash_value);
    transparent_crc(g_1904.f1, "g_1904.f1", print_hash_value);
    transparent_crc(g_1904.f2, "g_1904.f2", print_hash_value);
    transparent_crc(g_1904.f3, "g_1904.f3", print_hash_value);
    transparent_crc_bytes (&g_1904.f4, sizeof(g_1904.f4), "g_1904.f4", print_hash_value);
    transparent_crc(g_1904.f5, "g_1904.f5", print_hash_value);
    transparent_crc(g_1904.f6, "g_1904.f6", print_hash_value);
    transparent_crc(g_1904.f7, "g_1904.f7", print_hash_value);
    transparent_crc(g_1904.f8, "g_1904.f8", print_hash_value);
    transparent_crc(g_1922, "g_1922", print_hash_value);
    transparent_crc(g_1952, "g_1952", print_hash_value);
    transparent_crc(g_2134.f0, "g_2134.f0", print_hash_value);
    transparent_crc(g_2134.f1, "g_2134.f1", print_hash_value);
    transparent_crc(g_2134.f2, "g_2134.f2", print_hash_value);
    transparent_crc(g_2134.f3, "g_2134.f3", print_hash_value);
    transparent_crc_bytes (&g_2134.f4, sizeof(g_2134.f4), "g_2134.f4", print_hash_value);
    transparent_crc(g_2134.f5, "g_2134.f5", print_hash_value);
    transparent_crc(g_2134.f6, "g_2134.f6", print_hash_value);
    transparent_crc(g_2134.f7, "g_2134.f7", print_hash_value);
    transparent_crc(g_2134.f8, "g_2134.f8", print_hash_value);
    transparent_crc(g_2344.f0, "g_2344.f0", print_hash_value);
    transparent_crc(g_2344.f1, "g_2344.f1", print_hash_value);
    transparent_crc(g_2344.f2, "g_2344.f2", print_hash_value);
    transparent_crc(g_2344.f3, "g_2344.f3", print_hash_value);
    transparent_crc_bytes (&g_2344.f4, sizeof(g_2344.f4), "g_2344.f4", print_hash_value);
    transparent_crc(g_2344.f5, "g_2344.f5", print_hash_value);
    transparent_crc(g_2344.f6, "g_2344.f6", print_hash_value);
    transparent_crc(g_2344.f7, "g_2344.f7", print_hash_value);
    transparent_crc(g_2344.f8, "g_2344.f8", print_hash_value);
    transparent_crc(g_2369.f0, "g_2369.f0", print_hash_value);
    transparent_crc(g_2369.f1, "g_2369.f1", print_hash_value);
    transparent_crc(g_2369.f2, "g_2369.f2", print_hash_value);
    transparent_crc(g_2369.f3, "g_2369.f3", print_hash_value);
    transparent_crc_bytes (&g_2369.f4, sizeof(g_2369.f4), "g_2369.f4", print_hash_value);
    transparent_crc(g_2369.f5, "g_2369.f5", print_hash_value);
    transparent_crc(g_2369.f6, "g_2369.f6", print_hash_value);
    transparent_crc(g_2369.f7, "g_2369.f7", print_hash_value);
    transparent_crc(g_2369.f8, "g_2369.f8", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2467[i].f0, "g_2467[i].f0", print_hash_value);
        transparent_crc(g_2467[i].f1, "g_2467[i].f1", print_hash_value);
        transparent_crc(g_2467[i].f2, "g_2467[i].f2", print_hash_value);
        transparent_crc(g_2467[i].f3, "g_2467[i].f3", print_hash_value);
        transparent_crc_bytes(&g_2467[i].f4, sizeof(g_2467[i].f4), "g_2467[i].f4", print_hash_value);
        transparent_crc(g_2467[i].f5, "g_2467[i].f5", print_hash_value);
        transparent_crc(g_2467[i].f6, "g_2467[i].f6", print_hash_value);
        transparent_crc(g_2467[i].f7, "g_2467[i].f7", print_hash_value);
        transparent_crc(g_2467[i].f8, "g_2467[i].f8", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2583, "g_2583", print_hash_value);
    transparent_crc(g_2623.f0, "g_2623.f0", print_hash_value);
    transparent_crc(g_2623.f1, "g_2623.f1", print_hash_value);
    transparent_crc(g_2623.f2, "g_2623.f2", print_hash_value);
    transparent_crc(g_2623.f3, "g_2623.f3", print_hash_value);
    transparent_crc_bytes (&g_2623.f4, sizeof(g_2623.f4), "g_2623.f4", print_hash_value);
    transparent_crc(g_2623.f5, "g_2623.f5", print_hash_value);
    transparent_crc(g_2623.f6, "g_2623.f6", print_hash_value);
    transparent_crc(g_2623.f7, "g_2623.f7", print_hash_value);
    transparent_crc(g_2623.f8, "g_2623.f8", print_hash_value);
    transparent_crc(g_2651, "g_2651", print_hash_value);
    transparent_crc(g_2653, "g_2653", print_hash_value);
    transparent_crc(g_2654, "g_2654", print_hash_value);
    transparent_crc(g_2655, "g_2655", print_hash_value);
    transparent_crc(g_2656, "g_2656", print_hash_value);
    transparent_crc_bytes (&g_2668, sizeof(g_2668), "g_2668", print_hash_value);
    transparent_crc(g_2709, "g_2709", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_2710[i][j], "g_2710[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2711, "g_2711", print_hash_value);
    transparent_crc(g_2712, "g_2712", print_hash_value);
    transparent_crc(g_2713, "g_2713", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_2714[i][j], "g_2714[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2715, "g_2715", print_hash_value);
    transparent_crc(g_2746.f0, "g_2746.f0", print_hash_value);
    transparent_crc(g_2746.f1, "g_2746.f1", print_hash_value);
    transparent_crc(g_2746.f2, "g_2746.f2", print_hash_value);
    transparent_crc(g_2746.f3, "g_2746.f3", print_hash_value);
    transparent_crc_bytes (&g_2746.f4, sizeof(g_2746.f4), "g_2746.f4", print_hash_value);
    transparent_crc(g_2746.f5, "g_2746.f5", print_hash_value);
    transparent_crc(g_2746.f6, "g_2746.f6", print_hash_value);
    transparent_crc(g_2746.f7, "g_2746.f7", print_hash_value);
    transparent_crc(g_2746.f8, "g_2746.f8", print_hash_value);
    transparent_crc(g_2796, "g_2796", print_hash_value);
    transparent_crc(g_2859.f0, "g_2859.f0", print_hash_value);
    transparent_crc(g_2859.f1, "g_2859.f1", print_hash_value);
    transparent_crc(g_2859.f2, "g_2859.f2", print_hash_value);
    transparent_crc(g_2859.f3, "g_2859.f3", print_hash_value);
    transparent_crc_bytes (&g_2859.f4, sizeof(g_2859.f4), "g_2859.f4", print_hash_value);
    transparent_crc(g_2859.f5, "g_2859.f5", print_hash_value);
    transparent_crc(g_2859.f6, "g_2859.f6", print_hash_value);
    transparent_crc(g_2859.f7, "g_2859.f7", print_hash_value);
    transparent_crc(g_2859.f8, "g_2859.f8", print_hash_value);
    transparent_crc(g_2951.f0, "g_2951.f0", print_hash_value);
    transparent_crc(g_2951.f1, "g_2951.f1", print_hash_value);
    transparent_crc(g_2951.f2, "g_2951.f2", print_hash_value);
    transparent_crc(g_2951.f3, "g_2951.f3", print_hash_value);
    transparent_crc_bytes (&g_2951.f4, sizeof(g_2951.f4), "g_2951.f4", print_hash_value);
    transparent_crc(g_2951.f5, "g_2951.f5", print_hash_value);
    transparent_crc(g_2951.f6, "g_2951.f6", print_hash_value);
    transparent_crc(g_2951.f7, "g_2951.f7", print_hash_value);
    transparent_crc(g_2951.f8, "g_2951.f8", print_hash_value);
    transparent_crc(g_2954.f0, "g_2954.f0", print_hash_value);
    transparent_crc(g_2954.f1, "g_2954.f1", print_hash_value);
    transparent_crc(g_2954.f2, "g_2954.f2", print_hash_value);
    transparent_crc(g_2954.f3, "g_2954.f3", print_hash_value);
    transparent_crc_bytes (&g_2954.f4, sizeof(g_2954.f4), "g_2954.f4", print_hash_value);
    transparent_crc(g_2954.f5, "g_2954.f5", print_hash_value);
    transparent_crc(g_2954.f6, "g_2954.f6", print_hash_value);
    transparent_crc(g_2954.f7, "g_2954.f7", print_hash_value);
    transparent_crc(g_2954.f8, "g_2954.f8", print_hash_value);
    transparent_crc(g_2957, "g_2957", print_hash_value);
    transparent_crc(g_3011, "g_3011", print_hash_value);
    transparent_crc(g_3056, "g_3056", print_hash_value);
    transparent_crc(g_3057, "g_3057", print_hash_value);
    transparent_crc(g_3058, "g_3058", print_hash_value);
    transparent_crc(g_3109, "g_3109", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_3118[i], "g_3118[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3209, "g_3209", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_3343[i], sizeof(g_3343[i]), "g_3343[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3372, "g_3372", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 852
   depth: 1, occurrence: 14
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 3
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 41
breakdown:
   indirect level: 0, occurrence: 14
   indirect level: 1, occurrence: 8
   indirect level: 2, occurrence: 16
   indirect level: 3, occurrence: 3
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 7
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 14
XXX times a single bitfield on LHS: 4
XXX times a single bitfield on RHS: 58

XXX max expression depth: 43
breakdown:
   depth: 1, occurrence: 291
   depth: 2, occurrence: 79
   depth: 3, occurrence: 6
   depth: 4, occurrence: 7
   depth: 6, occurrence: 1
   depth: 7, occurrence: 3
   depth: 11, occurrence: 2
   depth: 13, occurrence: 6
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 3
   depth: 19, occurrence: 4
   depth: 20, occurrence: 2
   depth: 21, occurrence: 4
   depth: 22, occurrence: 2
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 4
   depth: 26, occurrence: 6
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 4
   depth: 34, occurrence: 5
   depth: 35, occurrence: 1
   depth: 37, occurrence: 4
   depth: 38, occurrence: 1
   depth: 43, occurrence: 1

XXX total number of pointers: 717

XXX times a variable address is taken: 1664
XXX times a pointer is dereferenced on RHS: 386
breakdown:
   depth: 1, occurrence: 274
   depth: 2, occurrence: 65
   depth: 3, occurrence: 32
   depth: 4, occurrence: 7
   depth: 5, occurrence: 8
XXX times a pointer is dereferenced on LHS: 398
breakdown:
   depth: 1, occurrence: 290
   depth: 2, occurrence: 78
   depth: 3, occurrence: 19
   depth: 4, occurrence: 9
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 54
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 10262

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1759
   level: 2, occurrence: 576
   level: 3, occurrence: 263
   level: 4, occurrence: 165
   level: 5, occurrence: 44
XXX number of pointers point to pointers: 332
XXX number of pointers point to scalars: 376
XXX number of pointers point to structs: 9
XXX percent of pointers has null in alias set: 32.4
XXX average alias set size: 1.6

XXX times a non-volatile is read: 2648
XXX times a non-volatile is write: 1278
XXX times a volatile is read: 114
XXX    times read thru a pointer: 55
XXX times a volatile is write: 102
XXX    times written thru a pointer: 92
XXX times a volatile is available for access: 4.9e+03
XXX percentage of non-volatile access: 94.8

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 304
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 28
   depth: 2, occurrence: 30
   depth: 3, occurrence: 62
   depth: 4, occurrence: 73
   depth: 5, occurrence: 77

XXX percentage a fresh-made variable is used: 18.6
XXX percentage an existing variable is used: 81.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


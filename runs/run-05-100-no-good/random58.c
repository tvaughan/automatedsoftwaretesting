/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1056029824
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   float  f0;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = (-2L);
static int32_t g_28 = 0x55FFF839L;
static float g_33 = (-0x6.6p+1);
static uint16_t g_34 = 0xFBE6L;
static int8_t g_69 = 0x33L;
static int8_t g_73 = 1L;
static int32_t * volatile g_80[1][9][8] = {{{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28},{&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28,&g_28}}};
static uint32_t g_103 = 2UL;
static int8_t g_113 = 1L;
static uint8_t g_115[9] = {0x3EL,0xB7L,0x3EL,0x3EL,0xB7L,0x3EL,0x3EL,0xB7L,0x3EL};
static uint16_t g_124 = 0x0555L;
static int64_t g_126 = 0xD78A3DA4F77C9A86LL;
static uint16_t g_146 = 0x39EBL;
static union U0 g_153 = {0x1.7p+1};
static union U0 *g_152 = &g_153;
static union U0 **g_151 = &g_152;
static union U0 *** volatile g_150 = &g_151;/* VOLATILE GLOBAL g_150 */
static int16_t g_159 = (-1L);
static volatile int8_t *g_178 = (void*)0;
static volatile int8_t **g_177 = &g_178;
static volatile uint32_t g_205 = 1UL;/* VOLATILE GLOBAL g_205 */
static volatile int64_t g_211[3][7][9] = {{{9L,1L,9L,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0xC35A1D51EC3C3146LL},{0x35DF793C2613D90FLL,0xAC61B89C927E87AFLL,0x90661D1CEF0AC721LL,0L,0xAC61B89C927E87AFLL,(-1L),0x94198814D170816ELL,0xC71CFC68CC6768EALL,1L},{0x2165D2405D245C51LL,1L,0x2165D2405D245C51LL,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0x9EDA30AC000487FCLL},{0x35DF793C2613D90FLL,0xC71CFC68CC6768EALL,0x90661D1CEF0AC721LL,1L,0xAC61B89C927E87AFLL,0x94198814D170816ELL,0x94198814D170816ELL,0xAC61B89C927E87AFLL,1L},{9L,1L,9L,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0xC35A1D51EC3C3146LL},{0x35DF793C2613D90FLL,0xAC61B89C927E87AFLL,0x90661D1CEF0AC721LL,0L,0xAC61B89C927E87AFLL,(-1L),0x94198814D170816ELL,0xC71CFC68CC6768EALL,1L},{0x2165D2405D245C51LL,1L,0x2165D2405D245C51LL,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0x9EDA30AC000487FCLL}},{{0x35DF793C2613D90FLL,0xC71CFC68CC6768EALL,0x90661D1CEF0AC721LL,1L,0xAC61B89C927E87AFLL,0x94198814D170816ELL,0x94198814D170816ELL,0xAC61B89C927E87AFLL,1L},{9L,1L,9L,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0xC35A1D51EC3C3146LL},{0x35DF793C2613D90FLL,0xAC61B89C927E87AFLL,0x90661D1CEF0AC721LL,0L,0xAC61B89C927E87AFLL,(-1L),0x94198814D170816ELL,0xC71CFC68CC6768EALL,1L},{0x2165D2405D245C51LL,1L,0x2165D2405D245C51LL,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0x9EDA30AC000487FCLL},{0x35DF793C2613D90FLL,0xC71CFC68CC6768EALL,0x90661D1CEF0AC721LL,1L,0xAC61B89C927E87AFLL,0x94198814D170816ELL,0x94198814D170816ELL,0xAC61B89C927E87AFLL,1L},{9L,1L,9L,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0xC35A1D51EC3C3146LL},{0x35DF793C2613D90FLL,0xAC61B89C927E87AFLL,0x90661D1CEF0AC721LL,0L,0xAC61B89C927E87AFLL,(-1L),0x94198814D170816ELL,0xC71CFC68CC6768EALL,1L}},{{0x2165D2405D245C51LL,1L,0x2165D2405D245C51LL,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0x9EDA30AC000487FCLL},{0x35DF793C2613D90FLL,0xC71CFC68CC6768EALL,0x90661D1CEF0AC721LL,1L,0xAC61B89C927E87AFLL,0x94198814D170816ELL,0x94198814D170816ELL,0xAC61B89C927E87AFLL,1L},{9L,1L,9L,0x4CE61BCC1DD9ACFFLL,1L,0x848104350C7B6A19LL,1L,1L,0xC35A1D51EC3C3146LL},{0x35DF793C2613D90FLL,0xAC61B89C927E87AFLL,0x90661D1CEF0AC721LL,0x7D913FAE4BFFE456LL,(-1L),(-10L),0x0C5D1EC6DD82FEA0LL,0x94198814D170816ELL,0xAEA880BC6D34CFEFLL},{0xC9E869F92A7757BELL,0x848104350C7B6A19LL,0xC9E869F92A7757BELL,0x1417789D2C2FFDF5LL,1L,0x010FE33B61AE5F70LL,0x9BDC76872175B47ALL,0x848104350C7B6A19LL,(-1L)},{0x1EA7A1799D2B88D7LL,0x94198814D170816ELL,0xB023839C7493250CLL,0xAEA880BC6D34CFEFLL,(-1L),0x0C5D1EC6DD82FEA0LL,0x0C5D1EC6DD82FEA0LL,(-1L),0xAEA880BC6D34CFEFLL},{0x788A37CDCD6FD788LL,0x848104350C7B6A19LL,0x788A37CDCD6FD788LL,0x1417789D2C2FFDF5LL,1L,0x010FE33B61AE5F70LL,1L,0x848104350C7B6A19LL,0x62DB80F26FC9603BLL}}};
static volatile int64_t *g_210[7][4][8] = {{{&g_211[2][0][5],(void*)0,(void*)0,&g_211[1][4][7],(void*)0,(void*)0,&g_211[2][0][5],&g_211[2][0][5]},{&g_211[1][3][2],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[0][0][0]},{&g_211[2][0][5],(void*)0,&g_211[2][2][6],&g_211[1][5][0],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[1][4][7]},{&g_211[1][3][2],&g_211[0][3][7],&g_211[2][0][5],&g_211[0][0][0],(void*)0,&g_211[2][0][5],(void*)0,(void*)0}},{{&g_211[2][0][5],&g_211[2][3][3],&g_211[2][0][5],&g_211[2][0][8],(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5]},{&g_211[2][4][7],(void*)0,&g_211[1][6][7],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5]},{(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],(void*)0},{&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[0][0][0],&g_211[1][3][1],&g_211[2][0][5]}},{{(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[0][0][0],&g_211[1][6][7],&g_211[1][5][0]},{&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[1][4][7]},{&g_211[1][5][0],&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[2][3][3],&g_211[2][0][5],(void*)0,&g_211[2][0][5]},{&g_211[2][0][5],(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[0][2][0],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5]}},{{&g_211[2][4][4],&g_211[2][3][3],(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[2][3][3]},{&g_211[2][0][5],&g_211[0][3][7],&g_211[2][0][5],&g_211[2][0][5],&g_211[1][6][7],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5]},{&g_211[0][3][7],(void*)0,(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[1][3][1]},{&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],(void*)0,(void*)0,&g_211[1][3][2]}},{{&g_211[2][0][5],(void*)0,(void*)0,&g_211[1][3][2],(void*)0,&g_211[2][0][5],&g_211[2][0][5],&g_211[1][4][7]},{&g_211[0][0][0],&g_211[2][0][5],&g_211[2][0][5],&g_211[1][1][1],&g_211[2][4][7],&g_211[2][0][5],(void*)0,(void*)0},{&g_211[1][3][1],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5]},{&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[0][3][4]}},{{&g_211[0][3][4],&g_211[2][0][5],(void*)0,&g_211[2][4][4],(void*)0,&g_211[0][2][0],(void*)0,&g_211[0][3][4]},{&g_211[2][0][5],&g_211[1][3][1],&g_211[0][0][3],&g_211[2][0][5],&g_211[0][2][5],&g_211[2][0][5],&g_211[1][4][7],&g_211[2][0][5]},{&g_211[2][0][5],&g_211[2][0][5],&g_211[1][6][7],&g_211[0][2][0],&g_211[2][0][5],&g_211[1][3][1],&g_211[2][4][7],&g_211[2][0][5]},{&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[1][3][1],&g_211[2][0][5],&g_211[2][0][5],(void*)0,&g_211[2][0][5]}},{{&g_211[1][3][1],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[0][3][4],&g_211[2][0][5],&g_211[0][3][4],&g_211[2][0][5]},{&g_211[2][0][5],&g_211[0][0][3],&g_211[2][0][5],(void*)0,&g_211[2][0][5],&g_211[2][4][7],(void*)0,(void*)0},{&g_211[2][1][4],&g_211[0][2][5],(void*)0,&g_211[2][0][5],&g_211[1][1][1],&g_211[2][0][8],&g_211[2][0][5],&g_211[2][0][5]},{&g_211[2][1][4],&g_211[2][0][5],&g_211[0][2][0],&g_211[2][4][4],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][0][5],&g_211[2][1][4]}}};
static const union U0 g_228 = {-0x1.0p-1};
static int32_t *g_244[10][6] = {{&g_28,&g_28,&g_28,&g_28,&g_28,(void*)0},{&g_28,&g_2,(void*)0,(void*)0,(void*)0,&g_2},{(void*)0,(void*)0,&g_28,&g_28,(void*)0,&g_28},{&g_28,&g_2,&g_28,&g_28,&g_28,(void*)0},{(void*)0,&g_28,&g_2,&g_2,&g_28,&g_28},{(void*)0,&g_28,&g_28,&g_2,&g_2,&g_28},{(void*)0,(void*)0,&g_2,&g_28,&g_2,&g_28},{&g_28,(void*)0,(void*)0,&g_28,&g_28,&g_2},{(void*)0,&g_28,(void*)0,(void*)0,(void*)0,&g_28},{&g_28,(void*)0,&g_2,&g_28,(void*)0,&g_28}};
static int32_t ** volatile g_243 = &g_244[7][5];/* VOLATILE GLOBAL g_243 */
static int32_t ** volatile g_245 = &g_244[7][5];/* VOLATILE GLOBAL g_245 */
static uint64_t g_289 = 18446744073709551607UL;
static int32_t * volatile g_297 = (void*)0;/* VOLATILE GLOBAL g_297 */
static int32_t * const  volatile g_298 = (void*)0;/* VOLATILE GLOBAL g_298 */
static int32_t * volatile g_299 = &g_28;/* VOLATILE GLOBAL g_299 */
static int32_t * volatile g_386 = &g_28;/* VOLATILE GLOBAL g_386 */
static float g_454 = 0x0.77C87Fp+5;
static int8_t g_460[7] = {0x39L,0x39L,0x39L,0x39L,0x39L,0x39L,0x39L};
static int32_t ** volatile g_502 = &g_244[7][5];/* VOLATILE GLOBAL g_502 */
static volatile uint64_t g_535 = 18446744073709551609UL;/* VOLATILE GLOBAL g_535 */
static volatile uint64_t *g_534[4] = {&g_535,&g_535,&g_535,&g_535};
static volatile uint64_t ** volatile g_533 = &g_534[3];/* VOLATILE GLOBAL g_533 */
static int64_t g_556[1] = {0x0EF3DCCD3F2AFD6ALL};
static const int8_t *g_608 = &g_460[0];
static const int8_t **g_607 = &g_608;
static uint32_t g_618 = 0xF7781105L;
static float *g_619 = &g_454;
static float *g_624 = &g_33;
static int32_t g_631 = 0xDA063160L;
static uint32_t g_634 = 8UL;
static int16_t g_645 = (-1L);
static int16_t g_656 = 2L;
static int16_t g_658 = 0L;
static int32_t ** volatile g_691 = &g_244[8][0];/* VOLATILE GLOBAL g_691 */
static uint16_t g_741 = 0x7A08L;
static int32_t g_787 = (-6L);
static int32_t * const  volatile g_786[1][10][10] = {{{&g_2,(void*)0,&g_28,&g_2,&g_28,&g_2,&g_2,&g_28,&g_787,&g_28},{(void*)0,&g_787,&g_28,(void*)0,&g_787,&g_2,&g_2,&g_2,(void*)0,&g_2},{&g_2,(void*)0,&g_28,&g_2,&g_2,&g_787,&g_28,&g_28,&g_787,&g_787},{&g_787,&g_2,(void*)0,&g_787,&g_787,(void*)0,&g_2,&g_787,&g_2,(void*)0},{&g_787,&g_2,&g_787,&g_28,(void*)0,&g_2,&g_2,(void*)0,&g_28,&g_787},{&g_2,(void*)0,&g_787,&g_2,(void*)0,&g_28,&g_787,&g_787,&g_28,(void*)0},{&g_787,&g_787,&g_28,&g_787,&g_787,&g_28,(void*)0,(void*)0,(void*)0,&g_28},{(void*)0,&g_28,&g_2,&g_28,(void*)0,&g_787,(void*)0,&g_2,(void*)0,&g_2},{&g_2,&g_787,(void*)0,&g_2,&g_2,&g_787,&g_28,&g_787,&g_28,&g_2},{(void*)0,&g_2,(void*)0,&g_787,(void*)0,&g_787,&g_28,&g_2,&g_787,&g_28}}};
static uint8_t g_789 = 0xC5L;
static int32_t * volatile g_792 = &g_28;/* VOLATILE GLOBAL g_792 */
static uint8_t g_822 = 0xE4L;
static float * volatile g_954 = &g_153.f0;/* VOLATILE GLOBAL g_954 */
static volatile union U0 g_1027 = {0x0.6p-1};/* VOLATILE GLOBAL g_1027 */
static volatile uint64_t * volatile * volatile g_1121 = &g_534[3];/* VOLATILE GLOBAL g_1121 */
static volatile uint64_t * volatile * volatile *g_1120 = &g_1121;
static volatile uint64_t * volatile * volatile **g_1119 = &g_1120;
static float * const  volatile g_1125[7][5] = {{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0},{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0},{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0},{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0},{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0},{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0},{&g_153.f0,&g_454,&g_153.f0,&g_454,&g_153.f0}};
static const int64_t g_1134 = (-1L);
static const int64_t *g_1133 = &g_1134;
static int8_t * volatile * volatile ** volatile g_1221 = (void*)0;/* VOLATILE GLOBAL g_1221 */
static volatile uint32_t g_1302 = 4294967295UL;/* VOLATILE GLOBAL g_1302 */
static volatile uint32_t *g_1301[8][1] = {{&g_1302},{&g_1302},{&g_1302},{&g_1302},{&g_1302},{&g_1302},{&g_1302},{&g_1302}};
static volatile uint32_t * volatile * volatile g_1300 = &g_1301[2][0];/* VOLATILE GLOBAL g_1300 */
static volatile uint32_t * volatile * volatile * volatile g_1303 = &g_1300;/* VOLATILE GLOBAL g_1303 */


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static int32_t  func_5(int64_t  p_6);
static int16_t  func_12(uint64_t  p_13, float  p_14, const int8_t  p_15, uint32_t  p_16, int32_t  p_17);
static uint64_t  func_24(const int32_t  p_25);
static int32_t  func_37(int32_t * p_38, int8_t  p_39, float  p_40);
static uint32_t  func_43(int32_t * p_44, uint8_t  p_45);
static int8_t  func_46(int32_t * p_47, int32_t * p_48);
static int32_t * func_49(int32_t * p_50, uint32_t  p_51);
static uint32_t  func_56(int32_t * const  p_57, const int64_t  p_58, int32_t * p_59, int32_t * p_60, int32_t  p_61);
static int32_t  func_62(float  p_63);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_502 g_244 g_789 g_1300 g_1303 g_1133 g_1134
 * writes: g_2 g_244 g_789 g_1300
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    int32_t l_1139 = 0xF1B417A9L;
    int32_t l_1243 = 0xDD94BB64L;
    float l_1244[9];
    int32_t l_1245 = 0xB130F6F0L;
    int32_t l_1246 = 0x28F6155AL;
    int32_t l_1247 = 1L;
    int32_t l_1248 = 0x9DD662EFL;
    uint16_t *l_1294 = (void*)0;
    int32_t **l_1295 = &g_244[7][5];
    int i;
    for (i = 0; i < 9; i++)
        l_1244[i] = 0x4.Cp-1;
    for (g_2 = 0; (g_2 != 10); g_2 = safe_add_func_int64_t_s_s(g_2, 5))
    { /* block id: 3 */
        const uint64_t l_26 = 18446744073709551609UL;
        uint32_t *l_1138 = &g_634;
        int32_t *l_1237 = &g_787;
        int32_t *l_1238 = (void*)0;
        int32_t *l_1239 = &g_787;
        int32_t *l_1240 = &g_28;
        int32_t *l_1241 = &g_28;
        int32_t *l_1242[8][10][3] = {{{(void*)0,&g_28,(void*)0},{&g_2,&g_787,&l_1139},{&g_28,&l_1139,(void*)0},{&g_787,&g_28,&g_2},{&g_787,&g_28,&g_787},{&g_28,&l_1139,&g_28},{&g_2,(void*)0,&g_28},{(void*)0,&l_1139,&g_28},{&l_1139,&g_787,&l_1139},{&l_1139,&g_787,&g_787}},{{(void*)0,(void*)0,&g_787},{&g_787,&g_787,&g_28},{&g_787,&g_787,&g_28},{&g_787,&g_787,&g_2},{(void*)0,&l_1139,&g_787},{&l_1139,(void*)0,(void*)0},{&l_1139,&l_1139,&l_1139},{(void*)0,&g_787,&l_1139},{&g_2,(void*)0,(void*)0},{&g_28,(void*)0,(void*)0}},{{&g_787,&l_1139,&g_28},{&g_787,&g_28,&g_787},{&g_28,&l_1139,&g_28},{&g_2,&g_787,&g_28},{(void*)0,&g_28,&g_28},{&l_1139,&g_787,&g_787},{(void*)0,&g_787,&g_787},{&l_1139,&l_1139,&g_28},{&g_787,(void*)0,&g_28},{&l_1139,&g_28,&g_28}},{{&g_787,&g_28,&g_787},{(void*)0,&l_1139,&g_28},{&g_28,&g_2,(void*)0},{&l_1139,(void*)0,&g_787},{&l_1139,(void*)0,&g_28},{&g_787,&g_787,&g_2},{&g_787,&g_28,&g_28},{&l_1139,&g_28,&g_787},{&g_28,&g_28,&g_2},{&l_1139,&g_787,&l_1139}},{{&g_787,&l_1139,&g_2},{&g_2,&g_787,&l_1139},{&g_28,&g_28,&g_28},{(void*)0,&g_28,(void*)0},{&g_787,&g_28,(void*)0},{&g_787,&g_787,&g_2},{&l_1139,(void*)0,(void*)0},{&l_1139,(void*)0,&g_28},{&g_787,&g_28,&g_28},{(void*)0,&g_2,(void*)0}},{{&g_787,&l_1139,&g_2},{&g_2,(void*)0,&g_787},{&g_2,&l_1139,&g_787},{&g_787,&g_2,&g_787},{&g_28,(void*)0,&g_787},{&g_28,&g_28,&g_2},{&g_787,&g_787,(void*)0},{&g_2,&g_28,&g_28},{&g_2,(void*)0,&g_787},{&g_787,&g_787,&g_28}},{{(void*)0,&g_28,&g_28},{&g_787,&g_28,&l_1139},{&l_1139,&l_1139,&g_28},{&l_1139,(void*)0,&g_2},{&g_787,(void*)0,&g_28},{&g_787,&g_787,&g_787},{(void*)0,&g_2,&g_28},{&g_28,(void*)0,&g_787},{&g_2,(void*)0,&g_28},{&g_787,&g_28,&g_787}},{{&l_1139,&g_28,&g_28},{&g_28,(void*)0,&g_787},{&l_1139,&g_28,&g_28},{&g_787,(void*)0,&g_2},{&g_787,&g_2,&g_28},{&l_1139,(void*)0,&l_1139},{(void*)0,&g_787,&g_28},{&g_787,&g_28,&g_28},{&g_787,&g_2,&g_787},{&g_787,&g_28,&g_28}}};
        uint8_t l_1249[6][6][7] = {{{1UL,0x3AL,246UL,0x59L,0x7DL,246UL,0x7DL},{8UL,0xE7L,0xE7L,8UL,1UL,0x84L,255UL},{0x7DL,1UL,0x96L,0x4DL,0xC3L,255UL,0x17L},{1UL,0x8CL,0x5DL,0xCAL,0x59L,0x34L,255UL},{0UL,0x19L,0x94L,0UL,0x34L,3UL,0x7DL},{255UL,1UL,0xD6L,0x17L,0xE5L,0x7EL,0x98L}},{{0x02L,0x0CL,255UL,0xE5L,0UL,0UL,0x78L},{0x7EL,246UL,0xE7L,7UL,255UL,1UL,255UL},{0UL,0x0CL,7UL,0xF5L,0x3AL,5UL,0x5DL},{0UL,8UL,0x98L,0x7BL,0x01L,0x4BL,0x6EL},{1UL,0x98L,0x31L,4UL,0xF5L,1UL,0x34L},{0xF5L,0xD6L,0x4DL,0UL,0x22L,0xB5L,1UL}},{{0xD6L,0x4BL,1UL,1UL,0x4BL,0xD6L,255UL},{250UL,0x44L,255UL,0x98L,0xE5L,1UL,0x84L},{255UL,0x6CL,0xA8L,0x19L,255UL,0x31L,246UL},{0x94L,0x44L,0x6EL,0xC3L,255UL,0UL,0x32L},{1UL,0x4BL,0UL,0UL,1UL,0x42L,0x1BL},{246UL,0xD6L,0x42L,0xA8L,2UL,0x81L,0x78L}},{{255UL,0x98L,0x2DL,0x6CL,0x99L,0x1BL,0x02L},{9UL,8UL,0x4BL,0x2DL,0x81L,7UL,255UL},{0x7EL,0x0CL,0x4BL,0x99L,0xD6L,0x6EL,0xFBL},{0x59L,0x7BL,0x2DL,0x32L,0UL,1UL,0x44L},{246UL,0x4BL,0x42L,0x94L,0x7EL,0x26L,0x43L},{0x7BL,2UL,0UL,3UL,3UL,0UL,2UL}},{{0x6CL,246UL,0x6EL,0x81L,0x34L,0xC3L,0x01L},{0UL,0x22L,0xA8L,1UL,255UL,9UL,0x7DL},{0x2DL,255UL,255UL,0x81L,0x84L,0x17L,0xB5L},{0x78L,0x7DL,1UL,3UL,246UL,0UL,9UL},{255UL,7UL,0x4DL,0x94L,255UL,0x7EL,0x6CL},{255UL,0UL,0x31L,0x32L,5UL,1UL,1UL}},{{0x4BL,0xB5L,0x98L,0x99L,0x4DL,255UL,0x77L},{0x77L,8UL,7UL,0x2DL,0x4DL,0x7DL,0x17L},{0x31L,255UL,0xE7L,0x6CL,5UL,2UL,0x2DL},{8UL,0x01L,0xD6L,0xA8L,255UL,0x59L,0UL},{0xA8L,0x6EL,1UL,255UL,4UL,4UL,255UL},{0x98L,0x59L,0x98L,0x6CL,0x96L,0xA8L,0x32L}}};
        int32_t **l_1259 = (void*)0;
        int32_t **l_1260 = &l_1240;
        float l_1269 = (-0x1.1p+1);
        int i, j, k;
    }
    (*l_1295) = (*g_502);
    for (g_789 = 0; (g_789 > 28); g_789 = safe_add_func_uint32_t_u_u(g_789, 7))
    { /* block id: 563 */
        float l_1298 = 0x0.1p+1;
        int32_t l_1299 = 6L;
        return l_1299;
    }
    (*g_1303) = g_1300;
    return (*g_1133);
}


/* ------------------------------------------ */
/* 
 * reads : g_741 g_244 g_73 g_556 g_608 g_460 g_386 g_28 g_607 g_2 g_1221
 * writes: g_741 g_244 g_73 g_33 g_454
 */
static int32_t  func_5(int64_t  p_6)
{ /* block id: 493 */
    int64_t l_1179 = 8L;
    int32_t l_1187[9][7][2] = {{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}},{{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L},{0x4901D041L,0x1703FB48L}}};
    int8_t *l_1203 = &g_460[4];
    int8_t **l_1202 = &l_1203;
    float *l_1213[1];
    int32_t l_1214 = 0x7FA30DB2L;
    union U0 l_1217 = {0x1.Bp-1};
    int8_t ***l_1219 = &l_1202;
    int8_t ****l_1218[1][10][7] = {{{&l_1219,&l_1219,&l_1219,&l_1219,&l_1219,&l_1219,(void*)0},{(void*)0,&l_1219,&l_1219,(void*)0,&l_1219,&l_1219,&l_1219},{&l_1219,&l_1219,&l_1219,&l_1219,&l_1219,&l_1219,(void*)0},{&l_1219,&l_1219,&l_1219,(void*)0,&l_1219,(void*)0,&l_1219},{&l_1219,&l_1219,&l_1219,&l_1219,&l_1219,(void*)0,&l_1219},{&l_1219,(void*)0,&l_1219,&l_1219,&l_1219,&l_1219,&l_1219},{&l_1219,(void*)0,&l_1219,(void*)0,&l_1219,&l_1219,&l_1219},{(void*)0,&l_1219,&l_1219,&l_1219,&l_1219,(void*)0,&l_1219},{&l_1219,&l_1219,(void*)0,(void*)0,&l_1219,&l_1219,(void*)0},{(void*)0,&l_1219,&l_1219,&l_1219,&l_1219,&l_1219,&l_1219}}};
    int8_t *****l_1220 = (void*)0;
    int32_t l_1222 = 0x3803FAF9L;
    uint32_t l_1233 = 0x462F05FBL;
    int64_t l_1234 = 0x3288EDB3D23F15ECLL;
    int32_t l_1235 = 6L;
    int32_t l_1236 = (-1L);
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1213[i] = &g_153.f0;
lbl_1204:
    for (g_741 = 0; (g_741 <= 5); g_741 += 1)
    { /* block id: 496 */
        uint64_t l_1173 = 18446744073709551610UL;
        int32_t *l_1174 = (void*)0;
        int32_t l_1180 = 0x922F8593L;
        int32_t l_1181 = 0L;
        int32_t l_1182 = 0x63A75718L;
        int32_t l_1183 = 0xA6AE4BF2L;
        int32_t l_1184 = 0x951C6F9CL;
        int32_t l_1185 = 0L;
        int32_t l_1186 = 7L;
        int32_t l_1188[4];
        int i;
        for (i = 0; i < 4; i++)
            l_1188[i] = 0xF4F3B2EEL;
        if (l_1173)
        { /* block id: 497 */
            int32_t **l_1175 = &g_244[7][5];
            int32_t **l_1176 = &l_1174;
            (*l_1176) = ((*l_1175) = l_1174);
            if (l_1173)
                goto lbl_1204;
            return p_6;
        }
        else
        { /* block id: 501 */
            int32_t *l_1177 = &g_28;
            int32_t *l_1178[6][8];
            uint16_t l_1189 = 0x9203L;
            int i, j;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 8; j++)
                    l_1178[i][j] = &g_787;
            }
            l_1189++;
        }
        for (l_1184 = 5; (l_1184 >= 0); l_1184 -= 1)
        { /* block id: 506 */
            int32_t l_1194 = (-1L);
            int i, j;
            g_244[(l_1184 + 1)][l_1184] = g_244[l_1184][g_741];
            for (g_73 = 0; (g_73 >= 0); g_73 -= 1)
            { /* block id: 510 */
                int i;
                l_1194 = ((safe_rshift_func_uint8_t_u_u(g_556[g_73], 4)) == (*g_608));
            }
        }
        for (l_1173 = 0; (l_1173 <= 5); l_1173 += 1)
        { /* block id: 516 */
            uint16_t *l_1199 = &g_34;
            int i, j;
            if ((*g_386))
                break;
            if (p_6)
                continue;
            g_244[(l_1173 + 2)][l_1173] = &l_1187[4][1][1];
        }
    }
    l_1187[4][1][1] = (((l_1179 < (safe_rshift_func_uint16_t_u_s((safe_add_func_int64_t_s_s(l_1187[5][3][0], ((((*g_607) == (*l_1202)) != (safe_add_func_uint32_t_u_u(p_6, 0xC5823D7CL))) , (g_2 > 0x7A6BB42CL)))), 7))) , 0xE4L) , l_1187[0][4][0]);
    l_1236 = (safe_sub_func_float_f_f(0x8.81C24Dp+8, ((l_1214 = (0x1.Ep+1 > (l_1187[2][5][0] = (l_1179 < 0x1.Ep-1)))) == (safe_div_func_float_f_f(((l_1222 = (l_1217 , ((l_1218[0][1][1] = l_1218[0][1][1]) == g_1221))) > (l_1235 = (+((safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f(((((((-(safe_div_func_float_f_f(((g_454 = (g_33 = p_6)) < (l_1203 == l_1203)), l_1233))) <= l_1179) , p_6) == p_6) >= 0x5.A65B7Dp+96) == l_1234), p_6)), l_1234)), 0x5.Ep-1)) != p_6)))), 0x0.1p-1)))));
    return l_1235;
}


/* ------------------------------------------ */
/* 
 * reads : g_822 g_115 g_556 g_789 g_656 g_502 g_244 g_28
 * writes: g_115 g_556 g_28 g_789 g_656 g_244
 */
static int16_t  func_12(uint64_t  p_13, float  p_14, const int8_t  p_15, uint32_t  p_16, int32_t  p_17)
{ /* block id: 473 */
    uint8_t *l_1140 = &g_115[5];
    union U0 **l_1151 = &g_152;
    int64_t *l_1152 = &g_556[0];
    int32_t *l_1154 = &g_28;
    int32_t *l_1155[8] = {&g_787,&g_2,&g_787,&g_2,&g_787,&g_2,&g_787,&g_2};
    uint8_t *l_1158 = &g_789;
    int8_t *l_1162 = (void*)0;
    int8_t **l_1161[10][5][5] = {{{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162}},{{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162}},{{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162}},{{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162}},{{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162}},{{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162}},{{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,(void*)0,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{(void*)0,&l_1162,&l_1162,(void*)0,&l_1162}},{{(void*)0,(void*)0,&l_1162,&l_1162,(void*)0},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{(void*)0,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,(void*)0,(void*)0},{(void*)0,&l_1162,&l_1162,&l_1162,&l_1162}},{{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{(void*)0,&l_1162,&l_1162,(void*)0,&l_1162},{(void*)0,(void*)0,&l_1162,&l_1162,(void*)0},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{(void*)0,&l_1162,&l_1162,&l_1162,&l_1162}},{{&l_1162,&l_1162,&l_1162,(void*)0,(void*)0},{(void*)0,&l_1162,&l_1162,&l_1162,&l_1162},{&l_1162,&l_1162,&l_1162,&l_1162,&l_1162},{(void*)0,&l_1162,&l_1162,(void*)0,&l_1162},{(void*)0,(void*)0,&l_1162,&l_1162,(void*)0}}};
    const uint32_t l_1163 = 0UL;
    uint64_t l_1164 = 1UL;
    int i, j, k;
    l_1164 ^= (((*l_1140) |= g_822) < ((safe_sub_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(5L, (p_16 == p_13))), (((((((*l_1152) ^= (l_1151 != l_1151)) && 4L) , (p_17 = ((*l_1154) = (+0L)))) , ((safe_lshift_func_uint8_t_u_u((++(*l_1158)), (l_1161[4][2][1] != &l_1162))) != 0x36C486A8L)) ^ 7L) <= p_15))), 65535UL)) > 4294967295UL), p_13)), p_15)) <= l_1163));
    for (g_656 = 0; (g_656 < (-30)); g_656--)
    { /* block id: 482 */
        uint32_t l_1169[3][8][4] = {{{4294967295UL,0xD5E48A1FL,0x4AC06A47L,0xB6E11A17L},{0xD32DF51EL,4294967295UL,0xCD314BD0L,0xD5E48A1FL},{0UL,3UL,0xCD314BD0L,1UL},{0xD32DF51EL,0x4AC06A47L,0x4AC06A47L,0xD32DF51EL},{4294967295UL,0UL,1UL,0xDF5CE63EL},{1UL,0xDF5CE63EL,0xD5E48A1FL,3UL},{0UL,0x83A3923CL,0x29703089L,3UL},{0xB6E11A17L,0xDF5CE63EL,0UL,0xDF5CE63EL}},{{3UL,0UL,4294967291UL,0xD32DF51EL},{0xD5E48A1FL,0x4AC06A47L,0xB6E11A17L,1UL},{0x83A3923CL,3UL,1UL,0xD5E48A1FL},{0x83A3923CL,4294967295UL,0xB6E11A17L,0xB6E11A17L},{0xD5E48A1FL,0xD5E48A1FL,4294967291UL,0xCD314BD0L},{3UL,4294967291UL,0UL,0UL},{0xB6E11A17L,1UL,0x29703089L,0UL},{0UL,1UL,0xD5E48A1FL,0UL}},{{1UL,4294967291UL,1UL,0xCD314BD0L},{4294967295UL,0xD5E48A1FL,0x4AC06A47L,0xB6E11A17L},{0xD32DF51EL,4294967295UL,0xCD314BD0L,0xD5E48A1FL},{0UL,3UL,0xCD314BD0L,1UL},{0xD32DF51EL,0x4AC06A47L,0x4AC06A47L,0xD32DF51EL},{4294967295UL,0UL,1UL,0xDF5CE63EL},{1UL,0xDF5CE63EL,0xD5E48A1FL,3UL},{0UL,0x83A3923CL,0x29703089L,3UL}}};
        int32_t l_1171 = 0xBEB50160L;
        int64_t l_1172 = 0xBAC8D80B0F206DA4LL;
        int i, j, k;
        for (g_789 = 22; (g_789 <= 45); ++g_789)
        { /* block id: 485 */
            int32_t **l_1170 = &g_244[7][5];
            l_1169[2][0][3] |= p_17;
            (*l_1170) = (*g_502);
        }
        l_1171 = p_13;
        return l_1172;
    }
    return (*l_1154);
}


/* ------------------------------------------ */
/* 
 * reads : g_34 g_33 g_69 g_2 g_73 g_28 g_115 g_113 g_124 g_126 g_150 g_103 g_386 g_289 g_205 g_460 g_80 g_159 g_535 g_556 g_607 g_608 g_211 g_634 g_502 g_244 g_151 g_691 g_299 g_658 g_741 g_152 g_789 g_153 g_792 g_645 g_146 g_822 g_534 g_243 g_1119 g_1121 g_954 g_153.f0
 * writes: g_34 g_69 g_28 g_80 g_103 g_115 g_124 g_126 g_146 g_33 g_151 g_159 g_153.f0 g_460 g_113 g_533 g_289 g_556 g_73 g_619 g_624 g_631 g_634 g_645 g_656 g_244 g_658 g_741 g_789 g_822 g_1133
 */
static uint64_t  func_24(const int32_t  p_25)
{ /* block id: 4 */
    int32_t *l_27 = &g_28;
    int32_t *l_29 = &g_28;
    int32_t l_30 = 0xAF2C9A13L;
    int32_t *l_31 = &g_28;
    int32_t *l_32[7] = {&l_30,&l_30,&l_30,&l_30,&l_30,&l_30,&l_30};
    int8_t *l_67 = (void*)0;
    int8_t *l_68 = &g_69;
    int8_t *l_72[4][7] = {{&g_73,&g_73,&g_73,&g_73,&g_73,(void*)0,(void*)0},{(void*)0,&g_73,&g_73,&g_73,(void*)0,(void*)0,(void*)0},{&g_73,(void*)0,(void*)0,&g_73,(void*)0,(void*)0,&g_73},{&g_73,&g_73,&g_73,&g_73,(void*)0,&g_73,&g_73}};
    int8_t **l_76 = &l_72[2][0];
    int16_t l_77 = 0xBDF5L;
    int8_t *l_458 = (void*)0;
    int8_t *l_459[4][8][1] = {{{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]}},{{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]}},{{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]}},{{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]},{&g_460[4]}}};
    uint64_t *l_821 = (void*)0;
    uint8_t l_1094 = 0xB2L;
    int32_t *l_1126[1][2][8] = {{{&g_2,(void*)0,&g_2,&g_2,(void*)0,&g_2,&g_2,(void*)0},{(void*)0,&g_2,&g_2,(void*)0,&g_2,&g_2,(void*)0,&g_2}}};
    int i, j, k;
    g_34++;
    if ((l_1094 = ((*l_31) = func_37(&g_28, ((g_822 |= (safe_lshift_func_int16_t_s_s(((((*l_27) = func_43((func_46((l_32[1] = func_49(&l_30, ((g_460[4] |= (safe_add_func_uint32_t_u_u((safe_add_func_int16_t_s_s((func_56(&l_30, (func_62((&g_28 != ((g_33 == (((safe_div_func_int16_t_s_s(((((*l_68) &= (!g_34)) < ((((safe_add_func_int8_t_s_s(((*l_27) = 0x6DL), g_34)) < (safe_sub_func_uint8_t_u_u(((((*l_76) = l_67) == (void*)0) != g_2), p_25))) , g_34) & g_2)) || (-7L)), (-3L))) , l_77) <= 0x3.Bp-1)) , &l_30))) | 0xF0EBE340L), &l_30, &l_30, p_25) <= (-10L)), g_73)), 0x3F585C68L))) >= 0x3EL))), &g_2) , (void*)0), p_25)) >= 0L) || g_535), 0))) , p_25), p_25))))
    { /* block id: 457 */
        return p_25;
    }
    else
    { /* block id: 459 */
        uint64_t **l_1100 = (void*)0;
        uint64_t ***l_1099 = &l_1100;
        uint64_t ****l_1098[6] = {(void*)0,(void*)0,&l_1099,(void*)0,(void*)0,&l_1099};
        union U0 l_1118 = {0xE.4F521Ep-59};
        const int32_t l_1123[5][10][5] = {{{0x0D37EF7BL,1L,0xABA68385L,5L,(-1L)},{0x51331505L,1L,0x55C18DB6L,(-4L),1L},{(-3L),(-1L),(-4L),5L,0xE446DDDCL},{(-3L),(-1L),0xABA68385L,0x2C863E4CL,0x265E757CL},{0x51331505L,0x265E757CL,(-4L),(-4L),0x265E757CL},{0x0D37EF7BL,(-1L),0x55C18DB6L,3L,0xE446DDDCL},{(-10L),0x265E757CL,0xABA68385L,3L,1L},{0x51331505L,(-1L),0xF2B12AA8L,(-4L),(-1L)},{(-10L),(-1L),0xF2B12AA8L,0x2C863E4CL,0xE446DDDCL},{0x0D37EF7BL,1L,0xABA68385L,5L,(-1L)}},{{0x51331505L,1L,0x55C18DB6L,(-4L),1L},{(-3L),(-1L),(-4L),5L,0xE446DDDCL},{(-3L),(-1L),0xABA68385L,0x2C863E4CL,0x265E757CL},{0x51331505L,0x265E757CL,(-4L),(-4L),0x265E757CL},{0x0D37EF7BL,(-1L),0x55C18DB6L,3L,0xE446DDDCL},{(-10L),0x265E757CL,0xABA68385L,3L,1L},{0x51331505L,(-1L),0xF2B12AA8L,(-4L),(-1L)},{(-10L),(-1L),0xF2B12AA8L,0x2C863E4CL,0xE446DDDCL},{0x0D37EF7BL,1L,0xABA68385L,5L,(-1L)},{0x51331505L,1L,0x55C18DB6L,(-4L),1L}},{{(-3L),(-1L),(-4L),5L,0xE446DDDCL},{(-3L),(-1L),0xABA68385L,0x2C863E4CL,0x265E757CL},{0x51331505L,0x265E757CL,(-4L),0xBC93E918L,0x388EC0BBL},{0x6E9B1E23L,(-1L),1L,0xE446DDDCL,(-10L)},{1L,0x388EC0BBL,(-1L),0xE446DDDCL,0x2F44DF5CL},{0xEC651443L,0x03D16A25L,0x0E5886F9L,0xBC93E918L,0x03D16A25L},{1L,(-1L),0x0E5886F9L,(-1L),(-10L)},{0x6E9B1E23L,0x2F44DF5CL,(-1L),(-2L),0x03D16A25L},{0xEC651443L,0x2F44DF5CL,1L,0xBC93E918L,0x2F44DF5CL},{0x859E6D66L,(-1L),0xBC93E918L,(-2L),(-10L)}},{{0x859E6D66L,0x03D16A25L,(-1L),(-1L),0x388EC0BBL},{0xEC651443L,0x388EC0BBL,0xBC93E918L,0xBC93E918L,0x388EC0BBL},{0x6E9B1E23L,(-1L),1L,0xE446DDDCL,(-10L)},{1L,0x388EC0BBL,(-1L),0xE446DDDCL,0x2F44DF5CL},{0xEC651443L,0x03D16A25L,0x0E5886F9L,0xBC93E918L,0x03D16A25L},{1L,(-1L),0x0E5886F9L,(-1L),(-10L)},{0x6E9B1E23L,0x2F44DF5CL,(-1L),(-2L),0x03D16A25L},{0xEC651443L,0x2F44DF5CL,1L,0xBC93E918L,0x2F44DF5CL},{0x859E6D66L,(-1L),0xBC93E918L,(-2L),(-10L)},{0x859E6D66L,0x03D16A25L,(-1L),(-1L),0x388EC0BBL}},{{0xEC651443L,0x388EC0BBL,0xBC93E918L,0xBC93E918L,0x388EC0BBL},{0x6E9B1E23L,(-1L),1L,0xE446DDDCL,(-10L)},{1L,0x388EC0BBL,(-1L),0xE446DDDCL,0x2F44DF5CL},{0xEC651443L,0x03D16A25L,0x0E5886F9L,0xBC93E918L,0x03D16A25L},{1L,(-1L),0x0E5886F9L,(-1L),(-10L)},{0x6E9B1E23L,0x2F44DF5CL,(-1L),(-2L),0x03D16A25L},{0xEC651443L,0x2F44DF5CL,1L,0xBC93E918L,0x2F44DF5CL},{0x859E6D66L,(-1L),0xBC93E918L,(-2L),(-10L)},{0x859E6D66L,0x03D16A25L,(-1L),(-1L),0x388EC0BBL},{0xEC651443L,0x388EC0BBL,0xBC93E918L,0xBC93E918L,0x388EC0BBL}}};
        int16_t l_1130 = (-10L);
        const int64_t *l_1132 = (void*)0;
        const int64_t **l_1131 = &l_1132;
        int i, j, k;
lbl_1135:
        for (g_146 = 0; (g_146 >= 18); g_146++)
        { /* block id: 462 */
            uint16_t l_1122 = 65533UL;
            float *l_1124 = (void*)0;
            int32_t *l_1127 = &g_28;
            (*l_27) = ((-(l_1098[5] == (void*)0)) < (((safe_mul_func_float_f_f((safe_add_func_float_f_f(0x6.0E3FBAp+55, (safe_mul_func_float_f_f(((safe_lshift_func_int16_t_s_u((-4L), ((~(safe_add_func_int64_t_s_s((safe_div_func_uint64_t_u_u((((safe_mul_func_int16_t_s_s((((0xDBECB32C1263E12CLL > (((safe_sub_func_int8_t_s_s(((7L > (l_1118 , (((void*)0 != g_1119) || p_25))) ^ 0xF0L), l_1122)) & p_25) <= p_25)) == g_28) || p_25), p_25)) , 0xD675L) && l_1122), (**g_1121))), l_1122))) , l_1123[4][3][4]))) , (*g_954)), p_25)))), 0x0.Dp-1)) >= p_25) >= p_25));
            l_1127 = l_1126[0][0][7];
        }
        (*l_31) = ((safe_add_func_uint64_t_u_u(l_1130, 0xEA875FED8779467BLL)) > (l_1130 | ((g_1133 = ((*l_1131) = &g_556[0])) == &g_556[0])));
        if (g_73)
            goto lbl_1135;
    }
    return (*l_27);
}


/* ------------------------------------------ */
/* 
 * reads : g_28 g_159 g_115 g_645 g_534 g_556 g_608 g_460 g_289 g_73 g_152 g_153 g_103 g_691 g_244 g_243
 * writes: g_28 g_159 g_645 g_115 g_289 g_113 g_656 g_34 g_244
 */
static int32_t  func_37(int32_t * p_38, int8_t  p_39, float  p_40)
{ /* block id: 320 */
    uint64_t l_823 = 0xB8F029AD72A6E7FELL;
    int8_t *l_830 = &g_113;
    int64_t l_857 = (-9L);
    union U0 *l_859[10][10] = {{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,(void*)0,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153}};
    uint64_t l_879 = 0x8D5E0679B276A1BBLL;
    int32_t *l_884 = &g_28;
    int8_t **l_898 = &l_830;
    int8_t ***l_897[6][9] = {{&l_898,(void*)0,&l_898,&l_898,(void*)0,&l_898,&l_898,&l_898,&l_898},{&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898},{&l_898,&l_898,&l_898,&l_898,(void*)0,&l_898,&l_898,(void*)0,&l_898},{&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898},{&l_898,(void*)0,&l_898,&l_898,(void*)0,&l_898,&l_898,&l_898,&l_898},{&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898,&l_898}};
    int32_t l_920[10][2] = {{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L},{0x2444EA04L,0x2444EA04L}};
    uint32_t l_928 = 0x307A7AC1L;
    const float *l_953[10][1] = {{&g_33},{&g_33},{&g_33},{&g_33},{&g_33},{&g_33},{&g_33},{&g_33},{&g_33},{&g_33}};
    const float **l_952 = &l_953[4][0];
    uint8_t l_990 = 0UL;
    int64_t l_996 = (-2L);
    union U0 ***l_1010 = &g_151;
    uint16_t *l_1020 = &g_124;
    int32_t *l_1024 = &l_920[8][0];
    int8_t l_1046[10];
    int i, j;
    for (i = 0; i < 10; i++)
        l_1046[i] = 9L;
    (*p_38) ^= l_823;
    for (g_159 = 0; (g_159 <= 3); g_159 += 1)
    { /* block id: 324 */
        uint32_t l_837 = 6UL;
        union U0 *l_860[5] = {&g_153,&g_153,&g_153,&g_153,&g_153};
        uint64_t *l_861 = &g_289;
        int32_t *l_882 = &g_631;
        int32_t l_910 = 1L;
        int32_t l_912 = 0x8185CF06L;
        int32_t l_914 = 0xE3A2E663L;
        uint32_t l_965 = 4294967294UL;
        float **l_978 = &g_619;
        int32_t l_984 = 0x846831D0L;
        int32_t l_988[10] = {1L,1L,(-10L),0x58C2EC8CL,(-10L),1L,1L,(-10L),0x58C2EC8CL,(-10L)};
        float l_997 = 0xE.C163F1p+26;
        int32_t *l_1005 = &l_988[6];
        uint64_t l_1016 = 0x0EFD17900A3B7C2CLL;
        uint16_t *l_1021 = &g_124;
        int64_t *l_1074 = (void*)0;
        int64_t **l_1073 = &l_1074;
        int i;
        for (g_645 = 3; (g_645 >= 0); g_645 -= 1)
        { /* block id: 327 */
            uint8_t *l_826 = (void*)0;
            uint8_t *l_827 = &g_115[5];
            int64_t *l_834 = &g_556[0];
            int64_t **l_833 = &l_834;
            uint64_t *l_835[5][4][9] = {{{&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289},{&g_289,&l_823,&g_289,&g_289,&l_823,&g_289,(void*)0,(void*)0,&g_289},{&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823},{&l_823,&g_289,&g_289,&l_823,&g_289,(void*)0,(void*)0,&g_289,&l_823}},{{&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289},{&l_823,&l_823,(void*)0,&g_289,&g_289,&g_289,&g_289,(void*)0,&l_823},{&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823},{&g_289,&g_289,(void*)0,&l_823,&l_823,(void*)0,&g_289,&g_289,&g_289}},{{&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289},{&g_289,&l_823,&g_289,&g_289,&l_823,&g_289,(void*)0,(void*)0,&g_289},{&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823},{&l_823,&g_289,&g_289,&l_823,&g_289,(void*)0,(void*)0,&g_289,&l_823}},{{&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289},{&l_823,&l_823,(void*)0,&g_289,&g_289,&g_289,&g_289,(void*)0,&l_823},{&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823},{&g_289,&g_289,(void*)0,&l_823,&l_823,(void*)0,&g_289,&g_289,&g_289}},{{&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289,&l_823,&g_289},{&g_289,&l_823,&g_289,&g_289,&l_823,&g_289,(void*)0,(void*)0,&g_289},{&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823,&l_823},{&l_823,&g_289,&g_289,&l_823,&g_289,(void*)0,(void*)0,&g_289,&l_823}}};
            int8_t *l_836 = (void*)0;
            int32_t l_838 = (-5L);
            int i, j, k;
            if ((0x2427L < (safe_mul_func_uint8_t_u_u(((++(*l_827)) == ((p_39 = (1L != ((l_830 != ((0xE88A1F79L > ((safe_rshift_func_uint8_t_u_u((65531UL <= ((void*)0 == g_534[g_645])), 7)) >= ((g_289 = (((*l_833) = &g_126) != ((g_556[0] > 0xBFB5L) , &g_211[1][3][3]))) != 0x66CDBD4EB3932B10LL))) , l_836)) == l_837))) ^ l_838)), l_837))))
            { /* block id: 332 */
                int64_t l_844 = (-7L);
                int16_t *l_856 = &g_656;
                l_857 = (((*l_856) = ((safe_div_func_int32_t_s_s(((l_823 & (*g_608)) , (safe_div_func_int8_t_s_s(0x2CL, l_838))), (((safe_unary_minus_func_uint64_t_u((--g_289))) | (p_39 > (safe_add_func_int64_t_s_s(p_39, (((*l_830) = ((((safe_add_func_uint32_t_u_u(g_73, (safe_rshift_func_int8_t_s_s(((((safe_mul_func_int16_t_s_s(l_844, (!((*g_152) , l_837)))) == l_838) , l_838) , p_39), 7)))) != 1UL) ^ 0x50808D87L) && l_823)) == (-1L)))))) ^ l_823))) || 0L)) & g_103);
            }
            else
            { /* block id: 337 */
                for (g_34 = 0; (g_34 <= 3); g_34 += 1)
                { /* block id: 340 */
                    int32_t **l_858[2][6] = {{&g_244[9][2],(void*)0,&g_244[9][2],&g_244[9][2],(void*)0,&g_244[9][2]},{&g_244[9][2],(void*)0,&g_244[9][2],&g_244[9][2],(void*)0,&g_244[9][2]}};
                    int i, j;
                    (*g_243) = (*g_691);
                }
            }
            l_860[1] = l_859[0][1];
        }
    }
    return (*p_38);
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_150 g_151 g_28 g_691 g_299 g_2 g_103 g_113 g_658 g_34 g_386 g_634 g_73 g_460 g_205 g_741 g_152 g_211 g_159 g_789 g_153 g_792 g_645 g_115 g_607 g_608 g_556 g_146
 * writes: g_34 g_634 g_244 g_28 g_631 g_658 g_645 g_460 g_113 g_741 g_115 g_556 g_789 g_146 g_69
 */
static uint32_t  func_43(int32_t * p_44, uint8_t  p_45)
{ /* block id: 230 */
    const int8_t * const *l_664[1];
    union U0 **l_684 = &g_152;
    int8_t *l_685 = &g_460[4];
    int32_t l_686 = 0x4414D05CL;
    uint16_t *l_687 = (void*)0;
    uint16_t *l_688 = &g_34;
    uint32_t *l_689 = &g_634;
    int32_t l_763 = 0x57EFF83CL;
    int32_t l_765 = 0x104346B0L;
    int i;
    for (i = 0; i < 1; i++)
        l_664[i] = &g_608;
    if (((safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u(((((l_686 = ((l_664[0] == l_664[0]) ^ (((((safe_sub_func_uint32_t_u_u(4294967289UL, ((!((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_s(((((safe_div_func_uint32_t_u_u(((*l_689) = ((safe_sub_func_int16_t_s_s(((safe_sub_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s(g_69, 3)), (l_684 == (*g_150)))) & ((*l_688) = (g_28 == ((l_685 != (void*)0) != l_686)))), 0xF788L)) < l_686)), g_69)) == l_686) >= p_45) , l_686), p_45)), l_686)), p_45)) & 0UL)) != 0x6C6FAC174C09435ALL))) , 3L) < l_686) && 0xA72E60E4292E7D2ALL) && p_45))) == 0xE99179DA892AEDE2LL) , 0xC10AL) , p_45), 6)), 0x37AB5CD10F039CBALL)) < 0xCFL))
    { /* block id: 234 */
        int32_t *l_690 = &g_2;
        int32_t l_692 = 0xF303F353L;
        int32_t *l_693 = &l_686;
        int8_t **l_748 = &l_685;
        int8_t ***l_747 = &l_748;
        const union U0 *l_753[9] = {&g_153,&g_228,&g_153,&g_153,&g_228,&g_153,&g_153,&g_228,&g_153};
        int32_t l_760 = (-2L);
        int32_t l_761 = (-6L);
        int32_t l_762 = (-3L);
        int32_t l_764[2][6][2] = {{{0xC16169E5L,(-7L)},{(-7L),0xC16169E5L},{(-7L),(-7L)},{0xC16169E5L,(-7L)},{(-7L),0xC16169E5L},{(-7L),(-7L)}},{{0xC16169E5L,(-7L)},{(-7L),0xC16169E5L},{(-7L),(-7L)},{0xC16169E5L,(-7L)},{(-7L),0xC16169E5L},{(-7L),(-7L)}}};
        int i, j, k;
lbl_754:
        (*g_691) = l_690;
        l_692 &= 0x167B04E5L;
        if (((*l_693) = (((*g_299) = p_45) < 0xD6CFB48DL)))
        { /* block id: 239 */
            const int32_t *l_695 = (void*)0;
            const int32_t **l_694 = &l_695;
            uint8_t *l_704[4][3][1];
            int32_t *l_712 = &g_631;
            int32_t l_716 = 0L;
            uint32_t l_723 = 2UL;
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_704[i][j][k] = &g_115[8];
                }
            }
            (*l_694) = (void*)0;
            if ((safe_sub_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mul_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((p_45 = (*l_690)), ((safe_sub_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(((*l_690) | (~((*l_690) && (safe_add_func_uint64_t_u_u((((*l_712) = 1L) , ((safe_mul_func_int8_t_s_s((*l_693), 0xBAL)) < (g_103 != (safe_unary_minus_func_int16_t_s(0x93CCL))))), (l_716 == l_686)))))), 7)), g_113)) , l_686))), 0xEEL)) >= l_686), 0)), 0x13L)))
            { /* block id: 243 */
                return p_45;
            }
            else
            { /* block id: 245 */
                float l_745[10] = {(-0x1.2p+1),(-0x1.2p+1),0xE.BC651Ap+25,0x6.250EBCp+48,0xE.BC651Ap+25,(-0x1.2p+1),(-0x1.2p+1),0xE.BC651Ap+25,0x6.250EBCp+48,0xE.BC651Ap+25};
                const int32_t l_746 = (-1L);
                const union U0 *l_752 = &g_228;
                const union U0 **l_751 = &l_752;
                int i;
                for (g_658 = 0; (g_658 >= 0); g_658 -= 1)
                { /* block id: 248 */
                    const uint8_t l_742 = 0x91L;
                    int8_t ***l_750 = (void*)0;
                    for (g_34 = 0; (g_34 <= 0); g_34 += 1)
                    { /* block id: 251 */
                        int32_t *l_717 = &l_716;
                        int32_t *l_718 = &l_686;
                        int32_t *l_719 = &l_716;
                        int32_t *l_720 = &l_692;
                        int32_t *l_721 = &g_28;
                        int32_t *l_722[10];
                        int8_t **l_734[9];
                        int16_t *l_737 = (void*)0;
                        int16_t *l_738 = (void*)0;
                        int16_t *l_739 = &g_645;
                        uint32_t l_740[5][10] = {{0x391E6DAEL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL,0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL},{0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL,0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL},{0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL,0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL},{0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL,0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL},{0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL,0x3B5CE08EL,0x3B5CE08EL,0x8FA1AB3AL,4294967286UL,0x8FA1AB3AL}};
                        int i, j;
                        for (i = 0; i < 10; i++)
                            l_722[i] = &l_692;
                        for (i = 0; i < 9; i++)
                            l_734[i] = &l_685;
                        (*l_693) = (*g_386);
                        ++l_723;
                        (*l_717) |= (((((g_741 = (1L <= ((*l_721) = (((*l_693) < (safe_add_func_uint32_t_u_u(((65535UL <= (safe_lshift_func_int8_t_s_u((safe_sub_func_int8_t_s_s((safe_add_func_int16_t_s_s((((l_734[8] == ((++(*l_689)) , &g_608)) || (g_73 != ((l_740[1][5] |= ((*l_739) = 0xE1BDL)) , 0x8F84L))) , ((((g_113 ^= ((*l_685) &= (p_44 != (void*)0))) , p_45) & p_45) , 0L)), g_205)), g_69)), p_45))) < 6UL), p_45))) , (*g_386))))) != 0x0FL) > 0x1EL) | (*l_690)) < g_103);
                        if (l_742)
                            break;
                    }
                    for (l_692 = 0; (l_692 <= 0); l_692 += 1)
                    { /* block id: 266 */
                        int32_t l_744[10] = {0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L,0x2C5C9436L};
                        int i;
                        (*l_693) &= ((safe_unary_minus_func_int64_t_s((247UL == l_744[8]))) , l_744[2]);
                        if (l_746)
                            continue;
                    }
                    for (g_741 = 0; (g_741 <= 0); g_741 += 1)
                    { /* block id: 272 */
                        int8_t ****l_749 = (void*)0;
                        l_750 = l_747;
                        (*l_694) = (void*)0;
                        (*l_694) = p_44;
                    }
                }
                l_753[0] = ((*l_751) = (*l_684));
            }
            if (g_741)
                goto lbl_754;
        }
        else
        { /* block id: 282 */
            int32_t *l_755 = (void*)0;
            int32_t *l_756 = &g_28;
            int32_t *l_757 = &g_28;
            int32_t *l_758 = (void*)0;
            int32_t *l_759[3][4][8] = {{{&l_692,&g_2,&g_2,(void*)0,&g_2,(void*)0,(void*)0,(void*)0},{&g_28,&l_692,&g_2,(void*)0,&l_692,&g_28,&g_2,&l_692},{&g_2,&g_2,&g_2,&l_692,&g_2,&g_2,&g_2,&l_686},{(void*)0,&g_28,&l_686,&g_2,(void*)0,(void*)0,&l_692,(void*)0}},{{&g_2,&g_2,&g_28,&g_2,(void*)0,&l_692,&l_686,&l_692},{(void*)0,&g_28,&l_686,(void*)0,&g_2,&g_2,&g_28,&g_28},{&g_2,&g_2,(void*)0,&g_28,&l_692,&g_28,&g_2,&g_2},{&g_28,&g_2,&g_28,&g_28,&g_2,&g_28,(void*)0,(void*)0}},{{&l_692,&g_28,&g_28,&g_2,&l_686,&g_2,&g_28,&l_692},{&g_28,(void*)0,&g_28,&g_2,&l_692,&l_692,&l_692,(void*)0},{(void*)0,&l_692,&g_28,&g_28,&g_28,&g_28,&g_28,&g_2},{&g_2,&l_692,(void*)0,&g_28,&g_28,&g_28,&g_2,&g_28}}};
            uint32_t l_766 = 0xAE839E1BL;
            int i, j, k;
            l_766--;
            for (g_645 = 0; g_645 < 9; g_645 += 1)
            {
                g_115[g_645] = 1UL;
            }
        }
    }
    else
    { /* block id: 286 */
        uint8_t l_771 = 0UL;
        int32_t l_788 = 0x4808F680L;
        for (g_28 = 5; (g_28 <= (-15)); g_28--)
        { /* block id: 289 */
            uint8_t l_772 = 1UL;
            int64_t l_785[4];
            int i;
            for (i = 0; i < 4; i++)
                l_785[i] = 8L;
            l_788 = (l_771 || ((g_556[0] = ((((((l_772 , (g_211[2][0][5] <= (l_772 != (safe_mul_func_int16_t_s_s(g_103, (safe_div_func_int64_t_s_s(((safe_rshift_func_int16_t_s_u(0L, 15)) != (safe_add_func_int8_t_s_s((safe_add_func_int8_t_s_s((((safe_mul_func_uint8_t_u_u((l_772 || l_771), l_785[0])) >= g_460[4]) <= p_45), p_45)), (-1L)))), l_771))))))) , (void*)0) != (void*)0) != l_763) <= g_69) >= 0x91A0L)) , g_159));
            ++g_789;
        }
        (*g_792) = ((**l_684) , l_763);
        for (g_645 = 0; (g_645 <= 3); g_645 += 1)
        { /* block id: 297 */
            int32_t *l_812 = &l_763;
            l_686 |= (safe_add_func_int16_t_s_s((safe_mod_func_uint64_t_u_u(0x1B02404C0F0027C1LL, (+(safe_sub_func_uint32_t_u_u((p_45 , ((g_115[2]--) <= (((safe_sub_func_int8_t_s_s(((((safe_lshift_func_uint16_t_u_s(((((safe_unary_minus_func_uint8_t_u(0x70L)) || 0L) , (safe_sub_func_int32_t_s_s((+((void*)0 == (*g_607))), ((*l_812) = 1L)))) & (safe_add_func_int16_t_s_s(((!(!(9L > (safe_rshift_func_uint16_t_u_s((l_788 ^ 0L), g_205))))) <= p_45), p_45))), g_556[0])) || (*l_812)) | (-3L)) < 18446744073709551615UL), l_765)) , l_771) >= 0xA8L))), 0x1E6F6333L))))), 0xFA66L));
            for (g_146 = 0; (g_146 <= 3); g_146 += 1)
            { /* block id: 303 */
                for (g_69 = 3; (g_69 >= 0); g_69 -= 1)
                { /* block id: 306 */
                    (*l_812) = ((void*)0 == &g_619);
                }
                for (g_113 = 3; (g_113 >= 0); g_113 -= 1)
                { /* block id: 311 */
                    return l_763;
                }
            }
        }
    }
    return p_45;
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_2 g_34 g_124 g_126 g_80 g_159 g_535 g_556 g_113 g_607 g_115 g_103 g_608 g_460 g_211 g_634 g_289 g_205 g_502 g_244
 * writes: g_69 g_126 g_124 g_34 g_113 g_533 g_80 g_289 g_159 g_556 g_73 g_115 g_619 g_624 g_103 g_631 g_33 g_634 g_645 g_656
 */
static int8_t  func_46(int32_t * p_47, int32_t * p_48)
{ /* block id: 169 */
    int8_t l_551 = 0xFBL;
    const union U0 **l_554[10];
    int32_t l_563[10][6][4] = {{{0xF395BC63L,0xFB147702L,0x1C25D0F7L,0xFB147702L},{0xFB147702L,0xACD9180AL,0xD712F377L,0xFB147702L},{0xD712F377L,0xFB147702L,0x0A1AE2D2L,0x0A1AE2D2L},{0x410AC822L,0x410AC822L,0x1C25D0F7L,0xF395BC63L},{0x410AC822L,0xACD9180AL,0x0A1AE2D2L,0x410AC822L},{0xD712F377L,0xF395BC63L,0xD712F377L,0x0A1AE2D2L}},{{0xFB147702L,0xF395BC63L,0x1C25D0F7L,0x410AC822L},{0xF395BC63L,0xACD9180AL,0xACD9180AL,0xF395BC63L},{0xD712F377L,0x410AC822L,0xACD9180AL,0x0A1AE2D2L},{0xACD9180AL,0x0A1AE2D2L,0x410AC822L,0x0A1AE2D2L},{0x0A1AE2D2L,0x1C25D0F7L,4L,0x0A1AE2D2L},{4L,0x0A1AE2D2L,0x7182AC67L,0x7182AC67L}},{{0xD712F377L,0xD712F377L,0x410AC822L,0xACD9180AL},{0xD712F377L,0x1C25D0F7L,0x7182AC67L,0xD712F377L},{4L,0xACD9180AL,4L,0x7182AC67L},{0x0A1AE2D2L,0xACD9180AL,0x410AC822L,0xD712F377L},{0xACD9180AL,0x1C25D0F7L,0x1C25D0F7L,0xACD9180AL},{4L,0xD712F377L,0x1C25D0F7L,0x7182AC67L}},{{0xACD9180AL,0x0A1AE2D2L,0x410AC822L,0x0A1AE2D2L},{0x0A1AE2D2L,0x1C25D0F7L,4L,0x0A1AE2D2L},{4L,0x0A1AE2D2L,0x7182AC67L,0x7182AC67L},{0xD712F377L,0xD712F377L,0x410AC822L,0xACD9180AL},{0xD712F377L,0x1C25D0F7L,0x7182AC67L,0xD712F377L},{4L,0xACD9180AL,4L,0x7182AC67L}},{{0x0A1AE2D2L,0xACD9180AL,0x410AC822L,0xD712F377L},{0xACD9180AL,0x1C25D0F7L,0x1C25D0F7L,0xACD9180AL},{4L,0xD712F377L,0x1C25D0F7L,0x7182AC67L},{0xACD9180AL,0x0A1AE2D2L,0x410AC822L,0x0A1AE2D2L},{0x0A1AE2D2L,0x1C25D0F7L,4L,0x0A1AE2D2L},{4L,0x0A1AE2D2L,0x7182AC67L,0x7182AC67L}},{{0xD712F377L,0xD712F377L,0x410AC822L,0xACD9180AL},{0xD712F377L,0x1C25D0F7L,0x7182AC67L,0xD712F377L},{4L,0xACD9180AL,4L,0x7182AC67L},{0x0A1AE2D2L,0xACD9180AL,0x410AC822L,0xD712F377L},{0xACD9180AL,0x1C25D0F7L,0x1C25D0F7L,0xACD9180AL},{4L,0xD712F377L,0x1C25D0F7L,0x7182AC67L}},{{0xACD9180AL,0x0A1AE2D2L,0x410AC822L,0x0A1AE2D2L},{0x0A1AE2D2L,0x1C25D0F7L,4L,0x0A1AE2D2L},{4L,0x0A1AE2D2L,0x7182AC67L,0x7182AC67L},{0xD712F377L,0xD712F377L,0x410AC822L,0xACD9180AL},{0xD712F377L,0x1C25D0F7L,0x7182AC67L,0xD712F377L},{4L,0xACD9180AL,4L,0x7182AC67L}},{{0x0A1AE2D2L,0xACD9180AL,0x410AC822L,0xD712F377L},{0xACD9180AL,0x1C25D0F7L,0x1C25D0F7L,0xACD9180AL},{4L,0xD712F377L,0x1C25D0F7L,0x7182AC67L},{0xACD9180AL,0x0A1AE2D2L,0x410AC822L,0x0A1AE2D2L},{0x0A1AE2D2L,0x1C25D0F7L,4L,0x0A1AE2D2L},{4L,0x0A1AE2D2L,0x7182AC67L,0x7182AC67L}},{{0xD712F377L,0xD712F377L,0x410AC822L,0xACD9180AL},{0xD712F377L,0x1C25D0F7L,0x7182AC67L,0xD712F377L},{4L,0xACD9180AL,4L,0x7182AC67L},{0x0A1AE2D2L,0xACD9180AL,0x410AC822L,0xD712F377L},{0xACD9180AL,0x1C25D0F7L,0x1C25D0F7L,0xACD9180AL},{4L,0xD712F377L,0x1C25D0F7L,0x7182AC67L}},{{0xACD9180AL,0x0A1AE2D2L,0x410AC822L,0x0A1AE2D2L},{0x0A1AE2D2L,0x1C25D0F7L,4L,0x0A1AE2D2L},{4L,0x0A1AE2D2L,0x7182AC67L,0xF395BC63L},{4L,4L,0xD712F377L,0x1C25D0F7L},{4L,0x410AC822L,0xF395BC63L,4L},{0xFB147702L,0x1C25D0F7L,0xFB147702L,0xF395BC63L}}};
    uint64_t *l_654[1][6] = {{&g_289,&g_289,&g_289,&g_289,&g_289,&g_289}};
    uint64_t **l_653 = &l_654[0][0];
    uint64_t ***l_652[8][3] = {{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653}};
    int32_t l_659[8][9][3] = {{{0x8D5163F6L,1L,0xC4502664L},{0L,0x698E1C8DL,1L},{0xA47CE592L,0x2A020401L,0x3BE8044FL},{0L,0L,(-8L)},{0x8D5163F6L,0x8DF5382BL,0x472817B4L},{0x2DC30F3CL,0x7658E30DL,1L},{0xF402F563L,0x472817B4L,0xFF381DCFL},{7L,1L,2L},{(-1L),(-4L),1L}},{{1L,0x99D654C9L,0x2DC30F3CL},{(-9L),0x90033C97L,0x0A833F4FL},{0x926B9AACL,(-1L),1L},{1L,1L,(-9L)},{0xE93DE514L,(-5L),0x7658E30DL},{0x8302BE02L,(-1L),1L},{1L,(-1L),1L},{0x1507F501L,0x8302BE02L,1L},{(-7L),2L,0x7658E30DL}},{{1L,1L,(-9L)},{0x809354ABL,(-5L),1L},{1L,1L,0x0A833F4FL},{1L,0xE93DE514L,0x2DC30F3CL},{0L,0L,1L},{0x6AA4601BL,0x558CFC5EL,2L},{0x83FAD280L,0xA47CE592L,0xFF381DCFL},{2L,1L,1L},{9L,0x2AAC4329L,0x472817B4L}},{{9L,(-7L),(-8L)},{9L,0x4786907FL,0x3BE8044FL},{1L,0x1CA56935L,1L},{(-1L),0x4786907FL,0xC4502664L},{0x698E1C8DL,(-7L),0xEAD6FA8AL},{0x0A833F4FL,0x2AAC4329L,0xF402F563L},{0x7658E30DL,1L,9L},{0xB41F1F41L,0xA47CE592L,(-4L)},{0xA93F9F4DL,0x558CFC5EL,0x00B39D6FL}},{{0x90033C97L,0L,0x8D5163F6L},{0L,0xE93DE514L,0x99D654C9L},{0x2AAC4329L,1L,1L},{0x1CA56935L,(-5L),0x1CA56935L},{0L,1L,0L},{0x00B39D6FL,2L,(-5L)},{0x37D3BEAAL,0x8302BE02L,(-1L)},{1L,(-1L),0x44E784FEL},{0x37D3BEAAL,(-1L),0xC7027813L}},{{0x00B39D6FL,(-5L),(-1L)},{0L,1L,0x1507F501L},{0x1CA56935L,(-1L),0x45DBE835L},{0x2AAC4329L,0x90033C97L,1L},{1L,0x558CFC5EL,(-1L)},{0xFF381DCFL,(-1L),1L},{7L,0x45DBE835L,(-2L)},{0x8D5163F6L,0x4786907FL,0xC7027813L},{0xA93F9F4DL,0xA93F9F4DL,7L}},{{0x9B32B689L,0L,(-9L)},{(-5L),1L,(-7L)},{1L,0x472817B4L,0x3BE8044FL},{2L,(-5L),(-7L)},{0x2AAC4329L,0x37D3BEAAL,(-9L)},{0x1C34EDDFL,0x44E784FEL,7L},{0x2A020401L,0x0A833F4FL,0xC7027813L},{0L,(-7L),(-2L)},{0xBB59CB78L,0x9B32B689L,1L}},{{0L,8L,(-1L)},{(-1L),0xF402F563L,0x37D3BEAAL},{7L,1L,0x7658E30DL},{0x37D3BEAAL,1L,1L},{(-1L),1L,0L},{1L,0x2AAC4329L,0xB41F1F41L},{7L,0xD45DA4A9L,0xEAD6FA8AL},{1L,0x8302BE02L,1L},{9L,0xD45DA4A9L,0xE93DE514L}}};
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_554[i] = (void*)0;
    for (g_69 = 17; (g_69 != 29); g_69 = safe_add_func_int16_t_s_s(g_69, 1))
    { /* block id: 172 */
        float l_558 = 0x2.EF5C32p+45;
        int32_t l_560 = 0xF256409DL;
        float l_567 = (-0x7.2p+1);
        int32_t l_568 = (-1L);
        int32_t l_569 = 0x5D63546EL;
        int32_t l_573[10][5][1] = {{{0x10B4AC55L},{(-5L)},{0L},{(-5L)},{0x10B4AC55L}},{{0x5EA16113L},{0x10B4AC55L},{(-5L)},{0L},{(-5L)}},{{0x10B4AC55L},{0x5EA16113L},{0x10B4AC55L},{(-5L)},{0L}},{{(-5L)},{0x10B4AC55L},{0x5EA16113L},{0x10B4AC55L},{(-5L)}},{{0L},{(-5L)},{0x10B4AC55L},{0x5EA16113L},{0x10B4AC55L}},{{(-5L)},{0L},{(-5L)},{0x10B4AC55L},{0x5EA16113L}},{{0x10B4AC55L},{(-5L)},{0L},{(-5L)},{0x10B4AC55L}},{{0x5EA16113L},{0x10B4AC55L},{(-5L)},{0L},{(-5L)}},{{0x10B4AC55L},{0x5EA16113L},{0x10B4AC55L},{(-5L)},{0L}},{{(-5L)},{0x10B4AC55L},{0x5EA16113L},{0x10B4AC55L},{(-5L)}}};
        int8_t l_580 = (-1L);
        uint8_t l_611 = 0x40L;
        float *l_621 = &g_153.f0;
        float **l_620 = &l_621;
        float *l_623[9][5] = {{&g_454,&l_558,&g_33,&g_153.f0,&g_33},{&g_153.f0,&g_153.f0,&g_33,&g_33,&g_153.f0},{&l_558,&g_33,(void*)0,&g_33,&l_558},{&l_567,&l_558,&g_33,&g_153.f0,&l_558},{&g_153.f0,(void*)0,&g_33,&l_558,&l_567},{&l_567,&g_153.f0,&l_567,&g_153.f0,&l_567},{&l_558,&l_567,&l_567,&g_33,&g_33},{&g_153.f0,&g_33,&g_33,(void*)0,&g_454},{&g_454,&l_567,&g_33,&l_567,&g_33}};
        float **l_622[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        uint32_t *l_625 = (void*)0;
        uint32_t *l_626 = &g_103;
        uint16_t *l_629 = &g_34;
        int32_t *l_630 = &g_631;
        union U0 l_632[9] = {{0x0.Fp-1},{0x0.Fp-1},{0x1.Ap-1},{0x0.Fp-1},{0x0.Fp-1},{0x1.Ap-1},{0x0.Fp-1},{0x0.Fp-1},{0x1.Ap-1}};
        uint32_t *l_633 = &g_634;
        float l_639 = 0x6.5p-1;
        uint64_t *l_642 = &g_289;
        int16_t *l_643 = &g_159;
        int16_t *l_644 = &g_645;
        const uint64_t *l_650 = (void*)0;
        const uint64_t **l_649 = &l_650;
        const uint64_t ***l_648 = &l_649;
        const uint64_t ****l_651 = &l_648;
        int16_t *l_655 = &g_656;
        int16_t *l_657[6];
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_657[i] = &g_658;
        for (g_126 = 3; (g_126 >= 0); g_126 -= 1)
        { /* block id: 175 */
            int32_t l_564 = 0x0604CCA5L;
            int32_t l_566 = 0xD069C041L;
            int32_t l_572 = (-1L);
            int32_t l_575 = (-5L);
            int32_t l_577 = 0x88789C9BL;
            int32_t l_578 = 0L;
            int32_t l_579 = 0xC5392D06L;
            int32_t l_581[8][10][3] = {{{(-1L),0x3597BB8CL,1L},{0x803AB772L,8L,0xD5449E65L},{1L,0xC44AB901L,0L},{1L,1L,0x7AE461EEL},{1L,(-1L),1L},{1L,0x225BD84CL,0x6B01930BL},{(-3L),0xF04D5946L,5L},{0x6463C597L,1L,0x6B01930BL},{0L,1L,1L},{(-3L),0x3BF3F987L,1L}},{{3L,0xAA5D3305L,0x3597BB8CL},{0x225BD84CL,0x3BF3F987L,5L},{0xF4494DC4L,1L,(-8L)},{(-1L),1L,0x385D29E2L},{0xF04D5946L,0xF04D5946L,0xE5BE57ACL},{(-1L),0x225BD84CL,0x823D3CF9L},{0xF4494DC4L,(-3L),1L},{0x225BD84CL,0x0FF96024L,(-1L)},{3L,0xF4494DC4L,1L},{(-3L),(-1L),0x823D3CF9L}},{{0L,0x2AAA77DDL,0xE5BE57ACL},{0x6463C597L,0xBF149A44L,0x385D29E2L},{(-3L),0x2AAA77DDL,(-8L)},{1L,(-1L),5L},{(-3L),0xF4494DC4L,0x3597BB8CL},{0x0FF96024L,0x0FF96024L,1L},{(-3L),(-3L),1L},{1L,0x225BD84CL,0x6B01930BL},{(-3L),0xF04D5946L,5L},{0x6463C597L,1L,0x6B01930BL}},{{0L,1L,1L},{(-3L),0x3BF3F987L,1L},{3L,0xAA5D3305L,0x3597BB8CL},{0x225BD84CL,0x3BF3F987L,5L},{0xF4494DC4L,1L,(-8L)},{(-1L),1L,0x385D29E2L},{0xF04D5946L,0xF04D5946L,0xE5BE57ACL},{(-1L),0x225BD84CL,0x823D3CF9L},{0xF4494DC4L,(-3L),1L},{0x225BD84CL,0x0FF96024L,(-1L)}},{{3L,0xF4494DC4L,1L},{(-3L),(-1L),0x823D3CF9L},{0L,0x2AAA77DDL,0xE5BE57ACL},{0x6463C597L,0xBF149A44L,0x385D29E2L},{(-3L),0x2AAA77DDL,(-8L)},{1L,(-1L),5L},{(-3L),0xF4494DC4L,0x3597BB8CL},{0x0FF96024L,0x0FF96024L,1L},{(-3L),(-3L),1L},{1L,0x225BD84CL,0x6B01930BL}},{{(-3L),0xF04D5946L,5L},{0x6463C597L,1L,0x6B01930BL},{0L,1L,1L},{(-3L),0x3BF3F987L,1L},{3L,0xAA5D3305L,0x3597BB8CL},{0x225BD84CL,0x3BF3F987L,5L},{0xF4494DC4L,1L,(-8L)},{(-1L),1L,0x385D29E2L},{0xF04D5946L,0xF04D5946L,0xE5BE57ACL},{(-1L),0x225BD84CL,0x823D3CF9L}},{{0xF4494DC4L,(-3L),1L},{0x225BD84CL,0x0FF96024L,(-1L)},{3L,0xF4494DC4L,1L},{(-3L),(-1L),0x823D3CF9L},{0L,0x2AAA77DDL,0xE5BE57ACL},{0x6463C597L,0xBF149A44L,0x385D29E2L},{(-3L),0x2AAA77DDL,(-8L)},{1L,(-1L),5L},{(-3L),(-1L),0x2AAA77DDL},{0xB015BA3EL,0xB015BA3EL,0x0FF96024L}},{{0L,(-3L),0xAA5D3305L},{7L,0x6F1ED72FL,(-3L)},{(-3L),0xB05880BFL,0x0A538E90L},{0x7AE461EEL,7L,(-3L)},{4L,1L,0xAA5D3305L},{(-3L),0x33245FDCL,0x0FF96024L},{0x3F59E911L,1L,0x2AAA77DDL},{0x6F1ED72FL,0x33245FDCL,0x6463C597L},{(-1L),1L,3L},{0xBE532B8FL,7L,0x3BF3F987L}}};
            int8_t *l_606 = &g_73;
            int8_t **l_605 = &l_606;
            uint8_t l_616 = 0UL;
            int i, j, k;
            (*p_47) |= (*p_48);
            for (g_124 = 0; (g_124 <= 8); g_124 += 1)
            { /* block id: 179 */
                int8_t l_546[10][7] = {{0xD1L,0L,0L,0x7CL,0x4EL,0x5FL,0x9EL},{0x54L,(-1L),(-1L),0x7AL,0xD1L,(-1L),0xA5L},{3L,1L,(-1L),0L,0L,(-1L),1L},{0xC0L,(-4L),0xA5L,(-7L),0xB8L,0x5FL,0L},{(-4L),(-7L),0x7AL,0xC0L,0x97L,8L,3L},{0x7CL,0xA5L,0x54L,(-7L),8L,(-1L),0xB8L},{8L,3L,0x3DL,0L,0L,(-4L),0L},{0x7AL,3L,3L,0x7AL,0L,1L,0x4EL},{8L,0xA5L,1L,0x7CL,0xC0L,1L,(-1L)},{(-1L),(-7L),0L,(-1L),8L,0x9EL,0x4EL}};
                uint64_t l_555 = 0x3BFA72C67BC11B7ELL;
                int32_t l_561 = (-6L);
                float l_562 = (-0x1.4p-1);
                int32_t l_565 = 0L;
                int32_t l_570 = 0xB2FA3450L;
                int32_t l_571 = 0x30141B51L;
                int32_t l_574 = 9L;
                int32_t l_576[3];
                int i, j;
                for (i = 0; i < 3; i++)
                    l_576[i] = 0x057EC2F8L;
                for (g_34 = 0; (g_34 <= 0); g_34 += 1)
                { /* block id: 182 */
                    int i, j, k;
                    for (g_113 = 0; (g_113 <= 3); g_113 += 1)
                    { /* block id: 185 */
                        uint64_t *l_532 = (void*)0;
                        uint64_t **l_531[8] = {&l_532,&l_532,&l_532,&l_532,&l_532,&l_532,&l_532,&l_532};
                        uint64_t ***l_530[10][3][6] = {{{(void*)0,&l_531[3],&l_531[3],&l_531[3],(void*)0,&l_531[3]},{&l_531[3],&l_531[3],&l_531[3],&l_531[0],(void*)0,&l_531[3]},{(void*)0,&l_531[2],(void*)0,&l_531[3],(void*)0,&l_531[3]}},{{(void*)0,&l_531[6],(void*)0,&l_531[3],&l_531[1],&l_531[3]},{&l_531[5],(void*)0,&l_531[3],(void*)0,&l_531[5],&l_531[3]},{&l_531[4],&l_531[3],&l_531[5],&l_531[6],&l_531[6],&l_531[3]}},{{&l_531[3],(void*)0,&l_531[3],&l_531[3],(void*)0,&l_531[3]},{&l_531[3],&l_531[3],&l_531[5],&l_531[3],&l_531[3],&l_531[3]},{(void*)0,&l_531[3],&l_531[3],(void*)0,&l_531[3],&l_531[3]}},{{&l_531[2],&l_531[3],(void*)0,&l_531[3],&l_531[4],&l_531[3]},{&l_531[6],&l_531[3],(void*)0,&l_531[3],&l_531[6],&l_531[3]},{&l_531[3],(void*)0,&l_531[3],&l_531[6],&l_531[5],&l_531[3]}},{{&l_531[4],&l_531[3],&l_531[3],&l_531[0],&l_531[3],&l_531[3]},{&l_531[3],&l_531[3],&l_531[6],&l_531[0],&l_531[2],&l_531[3]},{&l_531[3],&l_531[6],&l_531[3],&l_531[3],&l_531[0],&l_531[0]}},{{(void*)0,&l_531[3],&l_531[3],(void*)0,&l_531[4],&l_531[3]},{&l_531[3],&l_531[2],&l_531[0],&l_531[6],&l_531[3],&l_531[3]},{&l_531[3],&l_531[3],&l_531[3],&l_531[3],&l_531[3],&l_531[5]}},{{&l_531[6],&l_531[2],&l_531[3],&l_531[3],&l_531[4],&l_531[3]},{(void*)0,&l_531[3],(void*)0,&l_531[3],&l_531[0],(void*)0},{&l_531[3],&l_531[6],&l_531[3],&l_531[3],&l_531[2],&l_531[3]}},{{&l_531[3],&l_531[3],&l_531[0],&l_531[3],&l_531[3],&l_531[3]},{&l_531[0],&l_531[5],&l_531[3],&l_531[6],&l_531[3],&l_531[3]},{&l_531[3],&l_531[2],&l_531[1],(void*)0,&l_531[2],&l_531[3]}},{{(void*)0,&l_531[3],&l_531[3],&l_531[3],&l_531[3],(void*)0},{&l_531[3],(void*)0,&l_531[3],&l_531[3],(void*)0,&l_531[3]},{&l_531[3],&l_531[3],&l_531[3],&l_531[6],&l_531[3],(void*)0}},{{&l_531[3],&l_531[3],&l_531[4],(void*)0,&l_531[3],&l_531[0]},{&l_531[3],(void*)0,&l_531[1],(void*)0,&l_531[4],&l_531[6]},{&l_531[3],&l_531[3],&l_531[6],&l_531[6],&l_531[3],&l_531[6]}}};
                        int i, j, k;
                        g_533 = (void*)0;
                    }
                    g_80[g_34][(g_126 + 5)][(g_34 + 5)] = g_80[g_34][g_124][(g_126 + 2)];
                    if ((*p_47))
                        continue;
                }
                for (g_289 = 0; (g_289 <= 0); g_289 += 1)
                { /* block id: 193 */
                    int32_t l_557 = (-1L);
                    int32_t *l_559[7];
                    uint32_t l_582 = 0x8AC2A3BFL;
                    int16_t *l_599 = &g_159;
                    uint16_t *l_602 = (void*)0;
                    const int8_t **l_609 = &g_608;
                    int64_t *l_610 = &g_556[0];
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_559[i] = &g_28;
                    l_560 &= (((((safe_mod_func_int64_t_s_s(((safe_lshift_func_uint16_t_u_s((safe_add_func_uint64_t_u_u((safe_mod_func_int32_t_s_s((safe_add_func_uint8_t_u_u(l_546[4][0], (((safe_rshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((-1L), ((void*)0 != &g_115[5]))), (((((((l_551 && ((safe_lshift_func_uint8_t_u_u((g_159 > 0x29L), 2)) >= l_551)) , (l_554[6] != (void*)0)) <= 0x7.4p+1) != l_555) > l_551) , l_551) , l_551))) && (*p_47)) < g_535))), 4294967286UL)), 0xF2B2CC251EA64895LL)), 14)) > g_556[0]), g_556[0])) | 0UL) | l_557) >= l_551) & g_113);
                    --l_582;
                    (*p_47) = (((((((safe_lshift_func_int16_t_s_u((safe_sub_func_int8_t_s_s(((l_573[3][3][0] || (safe_rshift_func_uint16_t_u_s((safe_add_func_uint16_t_u_u(g_124, ((safe_mul_func_int8_t_s_s(((*l_606) = (safe_sub_func_int16_t_s_s(((*l_599) = 0x44CDL), (l_563[2][4][2] && (((*l_610) = (((safe_rshift_func_uint16_t_u_s((--g_34), 3)) && (g_126 , ((l_605 == (l_609 = g_607)) , l_571))) , 1L)) == l_551))))), l_563[2][4][2])) || (-8L)))), 8))) == 0x721EA3F7L), g_115[5])), l_546[4][0])) ^ l_574) , 65528UL) == l_555) == 0L) != l_576[2]) | l_611);
                }
            }
            for (l_577 = 0; (l_577 <= 3); l_577 += 1)
            { /* block id: 206 */
                uint32_t *l_617 = &g_618;
                int i, j, k;
                (*p_47) = (safe_rshift_func_uint8_t_u_s((g_115[(l_577 + 1)] = ((((safe_sub_func_int64_t_s_s(l_616, l_569)) , &g_205) != (void*)0) != ((g_103 && (((void*)0 == p_48) , (((-1L) > ((((void*)0 != l_617) , (*p_47)) & 0x7606BC38L)) <= 4UL))) != g_115[3]))), (*g_608)));
            }
        }
        (*p_47) |= (((*l_626) = (((*l_620) = (g_619 = (void*)0)) == (g_624 = (void*)0))) < ((*l_633) &= (((((*l_630) = (safe_mul_func_uint16_t_u_u(((*l_629) = 0x886BL), 0x470CL))) , (g_556[0] , (l_551 , (l_632[0] , ((l_551 == ((g_33 = 0x3.CAE96Ap+39) <= l_551)) != g_211[2][0][5]))))) >= l_580) , l_611)));
        (*p_47) &= (safe_mul_func_int16_t_s_s((l_659[4][3][0] ^= ((*l_655) = (safe_add_func_int16_t_s_s(((((l_632[0] , ((l_551 , ((safe_lshift_func_int16_t_s_s(((*l_644) = ((*l_643) ^= (((void*)0 != l_629) , (((*l_642) |= l_563[2][4][2]) , l_573[3][4][0])))), 14)) || ((((safe_div_func_uint64_t_u_u((((-6L) > (((*l_651) = l_648) != l_652[2][0])) , l_563[3][0][3]), 18446744073709551615UL)) >= l_563[7][3][0]) & 0x8863L) <= 0x79E5L))) < g_205)) <= l_611) , 0UL) && l_551), 65529UL)))), l_573[0][3][0]));
    }
    p_48 = (*g_502);
    return (*g_608);
}


/* ------------------------------------------ */
/* 
 * reads : g_113 g_34 g_28
 * writes: g_113 g_34 g_103 g_28
 */
static int32_t * func_49(int32_t * p_50, uint32_t  p_51)
{ /* block id: 147 */
    int8_t l_463 = 0x2EL;
    int32_t l_485[10] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
    uint16_t *l_516 = &g_34;
    uint64_t *l_524[9] = {&g_289,&g_289,&g_289,&g_289,&g_289,&g_289,&g_289,&g_289,&g_289};
    int32_t l_525 = (-1L);
    uint32_t *l_526 = &g_103;
    int32_t l_527 = 9L;
    int i;
    for (g_113 = 0; (g_113 >= (-7)); g_113 = safe_sub_func_int8_t_s_s(g_113, 1))
    { /* block id: 150 */
        uint16_t *l_486 = (void*)0;
        uint16_t *l_487[4];
        uint64_t *l_492[9][3];
        int32_t l_499 = 0x82C1216FL;
        int32_t l_500 = 0x8B115873L;
        int i, j;
        for (i = 0; i < 4; i++)
            l_487[i] = (void*)0;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 3; j++)
                l_492[i][j] = &g_289;
        }
        l_463 = 0x9868C86DL;
    }
    (*p_50) ^= (safe_add_func_int8_t_s_s((safe_add_func_int64_t_s_s((!(safe_add_func_uint16_t_u_u((((safe_add_func_uint32_t_u_u(((l_485[8] < l_463) == 0xD02C7FD85FB8E5D7LL), (l_527 = ((*l_526) = (safe_mod_func_int8_t_s_s(l_463, ((safe_add_func_uint16_t_u_u(((*l_516) ^= l_485[7]), (l_516 == l_516))) ^ ((safe_rshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((((safe_div_func_uint16_t_u_u(((!(p_51 & (l_525 &= 0x789E923C09D82609LL))) & p_51), l_463)) > p_51) || 2L), (-2L))), 4)) & 1UL)))))))) , (void*)0) != (void*)0), 1UL))), 0x52020361A2B5CAFCLL)), p_51));
    return p_50;
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_2 g_115 g_113 g_69 g_124 g_126 g_28 g_150 g_103 g_386 g_289 g_205 g_34
 * writes: g_34 g_69 g_28 g_80 g_103 g_115 g_124 g_126 g_146 g_33 g_151 g_159 g_153.f0
 */
static uint32_t  func_56(int32_t * const  p_57, const int64_t  p_58, int32_t * p_59, int32_t * p_60, int32_t  p_61)
{ /* block id: 13 */
    uint32_t l_93 = 0x5D263D1FL;
    int32_t l_112 = 1L;
    int32_t l_114 = 4L;
    union U0 l_120 = {0x0.Fp+1};
    union U0 **l_166 = &g_152;
    int8_t l_194[7];
    int32_t l_240[1];
    int32_t l_257[9] = {0L,6L,0L,6L,0L,6L,0L,6L,0L};
    uint64_t *l_288 = &g_289;
    int32_t l_296 = (-1L);
    union U0 ***l_378 = (void*)0;
    uint16_t l_428 = 0x486CL;
    int32_t *l_449 = (void*)0;
    int32_t *l_450 = &g_28;
    int32_t *l_451 = &l_114;
    int32_t *l_452[10] = {(void*)0,&l_114,&l_114,(void*)0,&l_114,&l_114,(void*)0,&l_114,&l_114,(void*)0};
    int8_t l_453[3];
    uint32_t l_455 = 0xD3A7DFD2L;
    int i;
    for (i = 0; i < 7; i++)
        l_194[i] = 0x86L;
    for (i = 0; i < 1; i++)
        l_240[i] = 0xC17A1031L;
    for (i = 0; i < 3; i++)
        l_453[i] = 0xDCL;
    for (p_61 = (-25); (p_61 <= (-8)); ++p_61)
    { /* block id: 16 */
        uint16_t *l_94 = &g_34;
        uint32_t *l_102 = &g_103;
        uint64_t *l_110[1];
        int32_t l_111 = (-1L);
        union U0 *l_121 = &l_120;
        int32_t *l_122 = &l_112;
        uint16_t *l_123 = &g_124;
        int64_t *l_125 = &g_126;
        const int8_t *l_176[3];
        const int8_t **l_175 = &l_176[0];
        const union U0 *l_227[10][9] = {{&g_228,&g_228,(void*)0,&g_228,&g_228,&g_228,(void*)0,&g_228,&g_228},{&g_228,&g_228,(void*)0,&g_228,&g_228,&g_228,&g_228,(void*)0,&g_228},{&g_228,&g_228,(void*)0,&g_228,&g_228,(void*)0,(void*)0,&g_228,&g_228},{&g_228,&g_228,&g_228,&g_228,&g_228,&g_228,&g_228,(void*)0,(void*)0},{&g_228,(void*)0,(void*)0,(void*)0,&g_228,(void*)0,&g_228,&g_228,&g_228},{&g_228,&g_228,&g_228,&g_228,&g_228,&g_228,(void*)0,&g_228,&g_228},{&g_228,&g_228,&g_228,&g_228,&g_228,(void*)0,&g_228,&g_228,&g_228},{&g_228,&g_228,(void*)0,&g_228,&g_228,&g_228,(void*)0,&g_228,&g_228},{&g_228,&g_228,&g_228,(void*)0,&g_228,&g_228,&g_228,&g_228,&g_228},{&g_228,&g_228,(void*)0,&g_228,&g_228,(void*)0,(void*)0,&g_228,&g_228}};
        int32_t l_249 = 0xFD860E4EL;
        int32_t l_251 = (-9L);
        int32_t l_254 = 0x1B915A60L;
        int32_t l_263 = (-1L);
        int32_t l_265 = (-4L);
        int32_t l_267 = 7L;
        uint32_t l_332 = 0x48E914F0L;
        int32_t l_343 = (-1L);
        int32_t l_369 = 2L;
        const uint32_t l_387 = 0x6DB067AFL;
        uint16_t l_431 = 0x941CL;
        uint8_t *l_434 = &g_115[5];
        int32_t *l_443 = &l_254;
        int32_t *l_444 = (void*)0;
        int32_t *l_445[7][1][2];
        uint32_t l_446 = 3UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_110[i] = (void*)0;
        for (i = 0; i < 3; i++)
            l_176[i] = &g_69;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 2; k++)
                    l_445[i][j][k] = &l_265;
            }
        }
        for (g_34 = 0; g_34 < 1; g_34 += 1)
        {
            for (g_69 = 0; g_69 < 9; g_69 += 1)
            {
                for (g_28 = 0; g_28 < 8; g_28 += 1)
                {
                    g_80[g_34][g_69][g_28] = &g_2;
                }
            }
        }
        if (((g_73 , 7L) < ((*l_125) = (safe_add_func_uint16_t_u_u(((*l_94) = l_93), (((safe_mul_func_uint16_t_u_u((((*l_123) |= (safe_rshift_func_int16_t_s_s((safe_add_func_uint64_t_u_u((((((*l_102) = (safe_unary_minus_func_int32_t_s((*p_57)))) < ((*l_122) = ((p_61 != (safe_mod_func_uint16_t_u_u((g_2 ^ ((safe_rshift_func_uint16_t_u_u((safe_mod_func_int8_t_s_s(((g_115[5]--) > (((*l_121) = ((safe_lshift_func_int16_t_s_s(p_58, 14)) , l_120)) , 1L)), g_113)), p_58)) < 0xBA6CL)), 0x1744L))) > (*p_60)))) >= g_69) , p_61), p_61)), 0))) | 0x02C8L), p_61)) | l_93) < (-9L)))))))
        { /* block id: 25 */
            uint32_t l_143 = 0xFC4CEA79L;
            float *l_148 = &g_33;
            union U0 **l_149[2][8];
            uint8_t l_187[9];
            int32_t l_188 = 7L;
            int32_t l_198 = (-6L);
            int32_t l_199 = 0x1FC7E891L;
            int32_t l_200 = (-1L);
            int32_t l_202 = 9L;
            int32_t l_203 = 0x08070889L;
            int64_t l_255 = 0L;
            int32_t l_261[7][10] = {{0x08B36070L,0x47EAB302L,(-6L),(-6L),0x47EAB302L,0x08B36070L,0xDF11BD20L,1L,(-6L),0xDF11BD20L},{0xDB443416L,0x47EAB302L,(-1L),1L,0x47EAB302L,0xA9B597B5L,0x47EAB302L,1L,(-1L),0x47EAB302L},{0xDB443416L,0xDF11BD20L,(-6L),1L,0xDF11BD20L,0x08B36070L,0x47EAB302L,(-6L),(-6L),0x47EAB302L},{0x08B36070L,0x47EAB302L,(-6L),(-6L),0x47EAB302L,0x08B36070L,0xDF11BD20L,1L,(-6L),0xDF11BD20L},{0xDB443416L,0x47EAB302L,(-1L),1L,0x47EAB302L,0xA9B597B5L,0x47EAB302L,1L,(-1L),0x47EAB302L},{0xDB443416L,0xDF11BD20L,(-6L),1L,0xDF11BD20L,0x08B36070L,0x47EAB302L,(-6L),(-6L),0x47EAB302L},{0x08B36070L,0x47EAB302L,(-6L),(-6L),0x47EAB302L,0x08B36070L,0xDF11BD20L,1L,(-6L),0xDF11BD20L}};
            uint16_t * const l_272 = &g_124;
            int32_t l_328[3];
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 8; j++)
                    l_149[i][j] = (void*)0;
            }
            for (i = 0; i < 9; i++)
                l_187[i] = 249UL;
            for (i = 0; i < 3; i++)
                l_328[i] = 7L;
            for (g_69 = 0; (g_69 > 28); ++g_69)
            { /* block id: 28 */
                int32_t l_142 = 3L;
                int32_t *l_147 = &l_111;
                (*l_147) |= ((((safe_mod_func_uint64_t_u_u(l_112, (~(safe_lshift_func_uint8_t_u_s(0x47L, 3))))) != ((g_115[5] = (safe_mod_func_int64_t_s_s(((safe_sub_func_int64_t_s_s(((g_146 = (safe_mul_func_int8_t_s_s((((((safe_add_func_uint64_t_u_u((l_143 = l_142), (g_124 != l_142))) , (((*p_59) = 1L) != (0xFB16L || p_61))) , (safe_add_func_int8_t_s_s(((p_61 | g_126) >= l_143), 0xB3L))) ^ (*l_122)) >= 1UL), (-1L)))) >= p_61), p_61)) && l_143), p_58))) > g_28)) || l_142) ^ 1UL);
            }
            (*l_148) = g_2;
            (*g_150) = l_149[1][3];
            for (l_111 = 0; (l_111 >= (-26)); --l_111)
            { /* block id: 39 */
                int16_t l_160 = (-1L);
                int32_t l_195 = 1L;
                int32_t l_196 = 0x5D7662A5L;
                int32_t l_197 = 4L;
                int32_t l_201 = 0L;
                int32_t l_258 = 0x4D792688L;
                int32_t l_268 = (-8L);
                uint64_t l_275 = 4UL;
                union U0 l_321[8] = {{0x0.4F5E98p-73},{0x0.4F5E98p-73},{0x0.4F5E98p-73},{0x0.4F5E98p-73},{0x0.4F5E98p-73},{0x0.4F5E98p-73},{0x0.4F5E98p-73},{0x0.4F5E98p-73}};
                union U0 **l_324 = &l_121;
                int32_t l_341 = 3L;
                uint16_t l_344 = 0UL;
                int32_t l_358 = 8L;
                int32_t l_368 = (-5L);
                int i;
            }
        }
        else
        { /* block id: 111 */
            uint16_t l_385 = 0x80C5L;
            float *l_409 = &l_120.f0;
            int32_t l_410 = 0x3892F7E3L;
            int32_t *l_440 = &l_240[0];
            int8_t *l_442 = &g_113;
            int8_t **l_441[10];
            int i;
            for (i = 0; i < 10; i++)
                l_441[i] = &l_442;
            for (g_34 = 3; (g_34 < 55); g_34 = safe_add_func_uint32_t_u_u(g_34, 1))
            { /* block id: 114 */
                int16_t *l_379 = &g_159;
                float *l_388 = &g_153.f0;
                int32_t l_389[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_389[i] = (-2L);
                (*g_386) ^= (g_103 >= ((((*l_122) != (((*l_379) = (g_69 ^ (0xDDL || (~((l_378 = &g_151) == &l_166))))) <= ((safe_mod_func_uint32_t_u_u((((*p_60) = (safe_sub_func_int16_t_s_s(((g_69 != ((+g_69) , 0UL)) < (*l_122)), p_58))) , l_385), g_103)) ^ p_58))) , p_61) != 2UL));
                (*l_388) = (l_387 >= g_113);
                if (l_389[1])
                    break;
            }
            for (l_265 = 0; (l_265 <= (-4)); l_265 = safe_sub_func_int8_t_s_s(l_265, 4))
            { /* block id: 124 */
                uint32_t l_411 = 4294967294UL;
                uint32_t *l_426 = &l_332;
                int32_t l_427 = (-1L);
                for (l_267 = 0; (l_267 > (-21)); l_267 = safe_sub_func_int16_t_s_s(l_267, 4))
                { /* block id: 127 */
                    int8_t l_408 = 0L;
                    (*l_122) |= (p_61 , (((((((safe_sub_func_int64_t_s_s(l_385, (65528UL <= (safe_rshift_func_uint16_t_u_s(((safe_lshift_func_uint8_t_u_s((safe_mod_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s((((((((safe_lshift_func_uint16_t_u_u(((p_57 != (void*)0) <= (g_115[7] < l_408)), 12)) & ((l_409 == l_409) > l_408)) | (-1L)) != l_410) & (*p_59)) && 0xC9L) < p_61), g_289)) || l_408), g_205)), 1UL)), 6)) <= 0x5C96L), g_126))))) || (*p_57)) & 0x5DL) , &p_58) != &g_126) < p_58) | l_408));
                }
                (*l_122) = ((*p_59) = (l_411 && (((safe_rshift_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((safe_sub_func_uint64_t_u_u(l_240[0], (((*l_426) &= (((l_410 = ((safe_mul_func_int8_t_s_s(l_411, (safe_mod_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(g_113, (safe_mul_func_uint8_t_u_u(((p_58 ^ (((*p_60) , (l_240[0] , l_385)) < ((*l_102) = ((((p_58 || p_58) , l_411) ^ p_58) <= 1UL)))) , g_113), 0xE7L)))), 1UL)))) , p_58)) | (*l_122)) <= 4L)) , l_427))), 0xEAE0860E1631D18ELL)), 4)) || g_205) || p_61)));
                l_428 = ((*l_409) = p_61);
            }
            (*p_57) ^= ((safe_sub_func_uint16_t_u_u((g_34 |= l_431), ((((p_58 , (safe_add_func_int8_t_s_s((l_434 != (void*)0), ((safe_unary_minus_func_int16_t_s((l_428 > ((((*l_440) &= (!(!p_58))) , (void*)0) == (void*)0)))) == (p_61 <= (*l_122)))))) , l_441[6]) == (void*)0) < 0x227B8D68D9D34FF5LL))) | 0x75C4978B0F99AD8ELL);
        }
        ++l_446;
    }
    l_455++;
    return g_126;
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_28 g_34
 * writes: g_28
 */
static int32_t  func_62(float  p_63)
{ /* block id: 9 */
    int8_t *l_79 = &g_69;
    int8_t **l_78[6] = {&l_79,(void*)0,&l_79,&l_79,(void*)0,&l_79};
    int32_t *l_81 = (void*)0;
    int32_t *l_82 = &g_28;
    uint8_t l_87 = 8UL;
    int16_t l_88 = 1L;
    int i;
    (*l_82) = (l_78[0] == (void*)0);
    (*l_82) = ((((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(0xFFL, 4)), g_73)) && (g_73 , ((l_87 , (((((g_28 , l_81) == (void*)0) >= ((*l_82) && g_34)) < (*l_82)) , (*l_82))) , 0x95AE89E6L))) & (*l_82)) >= l_88);
    return (*l_82);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_28, "g_28", print_hash_value);
    transparent_crc_bytes (&g_33, sizeof(g_33), "g_33", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    transparent_crc(g_69, "g_69", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_113, "g_113", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_115[i], "g_115[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_124, "g_124", print_hash_value);
    transparent_crc(g_126, "g_126", print_hash_value);
    transparent_crc(g_146, "g_146", print_hash_value);
    transparent_crc_bytes (&g_153.f0, sizeof(g_153.f0), "g_153.f0", print_hash_value);
    transparent_crc(g_159, "g_159", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_211[i][j][k], "g_211[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_228.f0, sizeof(g_228.f0), "g_228.f0", print_hash_value);
    transparent_crc(g_289, "g_289", print_hash_value);
    transparent_crc_bytes (&g_454, sizeof(g_454), "g_454", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_460[i], "g_460[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_535, "g_535", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_556[i], "g_556[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_618, "g_618", print_hash_value);
    transparent_crc(g_631, "g_631", print_hash_value);
    transparent_crc(g_634, "g_634", print_hash_value);
    transparent_crc(g_645, "g_645", print_hash_value);
    transparent_crc(g_656, "g_656", print_hash_value);
    transparent_crc(g_658, "g_658", print_hash_value);
    transparent_crc(g_741, "g_741", print_hash_value);
    transparent_crc(g_787, "g_787", print_hash_value);
    transparent_crc(g_789, "g_789", print_hash_value);
    transparent_crc(g_822, "g_822", print_hash_value);
    transparent_crc_bytes (&g_1027.f0, sizeof(g_1027.f0), "g_1027.f0", print_hash_value);
    transparent_crc(g_1134, "g_1134", print_hash_value);
    transparent_crc(g_1302, "g_1302", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 345
XXX total union variables: 9

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 50
breakdown:
   depth: 1, occurrence: 124
   depth: 2, occurrence: 42
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 7, occurrence: 1
   depth: 11, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 19, occurrence: 2
   depth: 21, occurrence: 1
   depth: 22, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 3
   depth: 27, occurrence: 4
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 31, occurrence: 2
   depth: 35, occurrence: 1
   depth: 50, occurrence: 1

XXX total number of pointers: 275

XXX times a variable address is taken: 743
XXX times a pointer is dereferenced on RHS: 112
breakdown:
   depth: 1, occurrence: 108
   depth: 2, occurrence: 4
XXX times a pointer is dereferenced on LHS: 144
breakdown:
   depth: 1, occurrence: 142
   depth: 2, occurrence: 2
XXX times a pointer is compared with null: 23
XXX times a pointer is compared with address of another variable: 3
XXX times a pointer is compared with another pointer: 11
XXX times a pointer is qualified to be dereferenced: 5979

XXX max dereference level: 3
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 651
   level: 2, occurrence: 44
   level: 3, occurrence: 8
XXX number of pointers point to pointers: 78
XXX number of pointers point to scalars: 188
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.6
XXX average alias set size: 1.36

XXX times a non-volatile is read: 877
XXX times a non-volatile is write: 432
XXX times a volatile is read: 43
XXX    times read thru a pointer: 4
XXX times a volatile is write: 20
XXX    times written thru a pointer: 1
XXX times a volatile is available for access: 1.61e+03
XXX percentage of non-volatile access: 95.4

XXX forward jumps: 1
XXX backward jumps: 3

XXX stmts: 132
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 25
   depth: 2, occurrence: 34
   depth: 3, occurrence: 18
   depth: 4, occurrence: 13
   depth: 5, occurrence: 10

XXX percentage a fresh-made variable is used: 16.5
XXX percentage an existing variable is used: 83.5
********************* end of statistics **********************/


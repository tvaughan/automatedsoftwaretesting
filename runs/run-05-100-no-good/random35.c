/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3337104726
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U1 {
   volatile uint8_t  f0;
   int64_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 6L;
static float g_12[3][5] = {{0x1.Ep-1,0x1.Ep-1,0x1.Ep-1,0x1.Ep-1,0x1.Ep-1},{0x0.9p+1,0x6.6p-1,0x0.9p+1,0x6.6p-1,0x0.9p+1},{0x1.Ep-1,0x1.Ep-1,0x1.Ep-1,0x1.Ep-1,0x1.Ep-1}};
static volatile int32_t g_13 = 0x19FA553FL;/* VOLATILE GLOBAL g_13 */
static volatile float g_14 = 0x3.F6A083p-13;/* VOLATILE GLOBAL g_14 */
static uint32_t g_15[7] = {4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL};


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_15
 * writes: g_2 g_15
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    union U1 *l_8 = (void*)0;
    int32_t *l_10[8][6][1] = {{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}},{{&g_2},{&g_2},{&g_2},{&g_2},{&g_2},{&g_2}}};
    float l_11[1];
    int8_t l_18 = 0x28L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_11[i] = 0x5.B6478Cp+76;
    for (g_2 = 19; (g_2 >= (-25)); g_2 = safe_sub_func_uint16_t_u_u(g_2, 5))
    { /* block id: 3 */
        float l_5 = 0x3.Cp+1;
        float *l_6[1][1];
        int32_t l_7 = 0x82D83D78L;
        union U1 *l_9 = (void*)0;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_6[i][j] = &l_5;
        }
        l_7 = l_5;
        l_9 = l_8;
    }
    g_15[1]++;
    return l_18;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc_bytes(&g_12[i][j], sizeof(g_12[i][j]), "g_12[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc_bytes (&g_14, sizeof(g_14), "g_14", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_15[i], "g_15[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 10
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 2
breakdown:
   depth: 1, occurrence: 7
   depth: 2, occurrence: 1

XXX total number of pointers: 4

XXX times a variable address is taken: 5
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 0
breakdown:
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 38
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 2
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 50
XXX average alias set size: 1

XXX times a non-volatile is read: 4
XXX times a non-volatile is write: 4
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 2
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 5
XXX max block depth: 1
breakdown:
   depth: 0, occurrence: 3
   depth: 1, occurrence: 2

XXX percentage a fresh-made variable is used: 66.7
XXX percentage an existing variable is used: 33.3
********************* end of statistics **********************/


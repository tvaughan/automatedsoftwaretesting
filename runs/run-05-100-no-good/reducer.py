import sys
import json
f1 = open("checker2.txt", "r")
f2 = open("checker1.txt", "r")
# print(f.read())
values1 = json.load(f1)
values2 = json.load(f2)
test = sys.argv[1]
if test not in values1 or test not in values2:
    print(f"Unknown test {test}")
    sys.exit(1)

value1 = int(values1[test])
value2 = int(values2[test])
if value1 > 0 and value2 > 0:
    low = min(value1, value2)
    high = max(value1, value2)
    percentage = 100 * ((high-low)/high)
    if test == "instr":
        if percentage > 50:
            print(test + " interesting")
    elif test == "loads":
        if percentage > 35:
            print(test + " interesting")
    elif test == "calls":
        if percentage > 30:
            print(test + " interesting")
    elif test == "div":
        if percentage > 30:
            print(test + " interesting")
    elif test == "vect":
        if percentage > 10 and high-low > 3:
            print(test + " interesting")

if value1 > 0 and value2 == 0 or value1 == 0 and value2 > 0:
    print(test + " interesting")

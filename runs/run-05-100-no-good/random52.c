/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3093217582
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int64_t g_10[4] = {0x37C04CCB63E8E1B8LL,0x37C04CCB63E8E1B8LL,0x37C04CCB63E8E1B8LL,0x37C04CCB63E8E1B8LL};
static volatile int64_t g_22 = 0x494ABC24891023F1LL;/* VOLATILE GLOBAL g_22 */
static int32_t g_24 = 0x5ECCE35AL;
static int16_t g_25[7][3] = {{0x6FD8L,0L,0L},{5L,7L,5L},{0x6FD8L,0x6FD8L,0L},{0x2DB7L,7L,0x2DB7L},{0x6FD8L,0L,0L},{5L,7L,5L},{0x6FD8L,0x6FD8L,0L}};
static volatile int8_t g_26[9][8] = {{0x74L,0xEDL,0x22L,0x69L,0x06L,0xDAL,0x5FL,0x5FL},{(-7L),0x70L,0x22L,0x22L,0x70L,(-7L),0xB8L,0x06L},{0x06L,(-1L),0xE2L,0L,0x5FL,0x22L,0x5FL,0xE2L},{0L,0xDAL,0xE2L,0xB8L,0xE2L,0xDAL,0L,0x76L},{0xEDL,0xE2L,0x86L,0x46L,0x22L,0x76L,(-1L),0x74L},{0xB8L,(-7L),0x70L,0x22L,0x22L,0x70L,(-7L),0xB8L},{0xEDL,0xB8L,0xDAL,0x74L,0xE2L,(-7L),0x86L,6L},{0L,0x5FL,0x69L,(-7L),0x74L,(-7L),0x69L,0x5FL},{0x76L,0xB8L,6L,0x06L,0xEDL,0x70L,0xE2L,0x69L}};
static int32_t g_27[6] = {1L,1L,1L,1L,1L,1L};
static float g_53 = 0xC.07A264p+33;
static float g_80[10][4][6] = {{{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19}},{{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19}},{{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19}},{{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19}},{{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19}},{{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19,0xA.07EE33p-35,0xA.07EE33p-35,0x3.23EC08p+19},{0xA.07EE33p-35,0xA.07EE33p-35,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35}},{{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35}},{{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35}},{{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35}},{{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35},{0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35,0x7.0BAEA4p-1,0x7.0BAEA4p-1,0xA.07EE33p-35}}};
static uint8_t g_97[3] = {0x42L,0x42L,0x42L};
static const int32_t *g_99[4][4][1] = {{{(void*)0},{&g_24},{(void*)0},{&g_24}},{{(void*)0},{&g_24},{(void*)0},{(void*)0}},{{&g_24},{(void*)0},{&g_24},{(void*)0}},{{&g_24},{(void*)0},{(void*)0},{&g_24}}};
static const int32_t **g_98 = &g_99[0][1][0];
static int8_t g_104 = 0xA0L;
static uint32_t g_110 = 0x54E034D0L;
static int32_t g_111 = 1L;
static int32_t g_114 = 0x92F5982EL;
static int32_t g_117 = 1L;
static int32_t * volatile g_116 = &g_117;/* VOLATILE GLOBAL g_116 */
static float * volatile g_139[4][5][5] = {{{&g_80[0][0][4],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[4][0][0]},{&g_80[9][2][5],&g_80[0][0][4],&g_80[9][2][5],&g_80[4][0][0],&g_80[9][2][5]},{(void*)0,&g_80[9][2][5],&g_80[4][0][0],&g_80[9][2][5],&g_80[4][0][0]},{&g_80[4][0][0],&g_80[4][0][0],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5]},{&g_80[4][0][0],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5]}},{{(void*)0,&g_80[3][1][3],&g_80[9][2][5],&g_80[8][0][0],&g_80[9][2][5]},{&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[1][0][5]},{&g_80[0][0][4],&g_80[4][0][0],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5]},{&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],(void*)0,&g_80[9][2][5]},{&g_80[3][1][3],&g_80[0][0][4],&g_80[9][2][5],&g_80[9][2][5],&g_80[0][0][4]}},{{&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[0][0][4]},{&g_80[9][2][5],&g_80[9][2][5],&g_80[4][0][0],&g_80[8][0][0],&g_80[9][2][5]},{&g_80[9][2][5],&g_80[8][0][0],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5]},{&g_80[1][0][5],&g_80[8][0][0],&g_80[1][0][5],&g_80[0][0][4],&g_80[9][2][5]},{&g_80[9][2][5],&g_80[8][0][0],&g_80[9][2][5],&g_80[9][2][5],&g_80[4][0][0]}},{{&g_80[9][2][5],&g_80[3][1][3],&g_80[9][2][5],(void*)0,&g_80[9][2][5]},{&g_80[9][2][5],&g_80[4][0][0],&g_80[9][2][5],&g_80[4][0][0],&g_80[9][2][5]},{&g_80[9][2][5],&g_80[9][2][5],&g_80[1][0][5],&g_80[4][0][0],(void*)0},{&g_80[9][2][5],&g_80[9][2][5],&g_80[0][0][4],(void*)0,&g_80[9][2][5]},{&g_80[9][2][5],&g_80[1][0][5],(void*)0,&g_80[9][2][5],(void*)0}}};
static uint64_t g_165 = 1UL;
static uint16_t g_177 = 1UL;
static const float g_215 = 0x2.4p+1;
static const float *g_214[7] = {&g_215,&g_215,&g_215,&g_215,&g_215,&g_215,&g_215};
static int32_t * volatile g_234 = &g_114;/* VOLATILE GLOBAL g_234 */
static int32_t *g_237 = (void*)0;
static volatile float g_256 = 0x4.E523F6p-54;/* VOLATILE GLOBAL g_256 */
static volatile uint32_t g_258[1][6][8] = {{{0x0AB87254L,0x0AB87254L,5UL,0xF8DD8E09L,4294967288UL,5UL,4294967288UL,0xF8DD8E09L},{0x81A3279AL,0xF8DD8E09L,0x81A3279AL,0x288EAD58L,0xF8DD8E09L,0x25FB43AFL,0x25FB43AFL,0xF8DD8E09L},{0xF8DD8E09L,0x25FB43AFL,0x25FB43AFL,0xF8DD8E09L,0x288EAD58L,0x81A3279AL,0xF8DD8E09L,0x81A3279AL},{0xF8DD8E09L,4294967288UL,5UL,4294967288UL,0xF8DD8E09L,5UL,0x0AB87254L,0x0AB87254L},{0x81A3279AL,4294967288UL,0x288EAD58L,0x288EAD58L,4294967288UL,0x81A3279AL,0x25FB43AFL,4294967288UL},{0x0AB87254L,0x25FB43AFL,0x288EAD58L,0x0AB87254L,0x288EAD58L,0x25FB43AFL,0x0AB87254L,0x81A3279AL}}};
static int8_t g_294[1][9][3] = {{{8L,8L,0L},{1L,1L,0xCBL},{8L,8L,0L},{1L,1L,0xCBL},{8L,8L,0L},{1L,1L,0xCBL},{8L,8L,0L},{1L,1L,0xCBL},{8L,8L,0L}}};
static uint32_t g_295 = 0x7AB18FCEL;
static volatile uint32_t g_326 = 0x3E966DBCL;/* VOLATILE GLOBAL g_326 */
static const int32_t **g_332 = (void*)0;
static const int32_t *** volatile g_331[9] = {&g_332,&g_332,&g_332,&g_332,&g_332,&g_332,&g_332,&g_332,&g_332};
static const int32_t *** volatile g_333 = (void*)0;/* VOLATILE GLOBAL g_333 */
static volatile uint16_t * volatile ** const g_347 = (void*)0;
static int16_t g_371 = 2L;
static volatile uint32_t g_389 = 4294967293UL;/* VOLATILE GLOBAL g_389 */
static int32_t g_406 = 0x5581C0DEL;
static volatile int8_t g_411 = 0x9FL;/* VOLATILE GLOBAL g_411 */
static uint32_t g_412 = 6UL;
static const uint32_t g_446[2] = {0x0842FC37L,0x0842FC37L};
static int32_t * volatile g_460 = (void*)0;/* VOLATILE GLOBAL g_460 */
static const uint16_t **g_463 = (void*)0;
static const uint16_t ***g_462 = &g_463;
static volatile uint32_t *g_498 = &g_389;
static volatile uint32_t ** volatile g_497 = &g_498;/* VOLATILE GLOBAL g_497 */
static volatile float g_507 = 0x4.F5FCD8p-22;/* VOLATILE GLOBAL g_507 */
static uint32_t g_508 = 1UL;
static uint16_t ***g_527[3][1] = {{(void*)0},{(void*)0},{(void*)0}};
static uint32_t g_552 = 4294967295UL;
static float **g_584 = (void*)0;
static int32_t **g_606 = (void*)0;
static volatile uint32_t g_608 = 0UL;/* VOLATILE GLOBAL g_608 */
static float g_648[6][7] = {{0xF.96F7D0p+49,0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49,0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49},{0x7.DEC948p-67,0x7.DEC948p-67,0xC.F8D817p-1,0x7.DEC948p-67,0x7.DEC948p-67,0xC.F8D817p-1,0x7.DEC948p-67},{0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49,0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49,0x7.DEC948p-67},{0xF.96F7D0p+49,0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49,0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49},{0x7.DEC948p-67,0x7.DEC948p-67,0xC.F8D817p-1,0x7.DEC948p-67,0x7.DEC948p-67,0xC.F8D817p-1,0x7.DEC948p-67},{0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49,0x7.DEC948p-67,0xF.96F7D0p+49,0xF.96F7D0p+49,0x7.DEC948p-67}};
static volatile float g_649 = 0x7.3p+1;/* VOLATILE GLOBAL g_649 */
static int8_t g_651 = 0x5BL;
static int64_t g_720 = 0x76C6D1C196B98F73LL;
static const volatile int64_t *g_768 = &g_22;
static const volatile int64_t ** volatile g_767[10][9][2] = {{{(void*)0,&g_768},{&g_768,&g_768},{(void*)0,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768}},{{(void*)0,&g_768},{&g_768,&g_768},{(void*)0,&g_768},{&g_768,&g_768},{&g_768,(void*)0},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768}},{{&g_768,(void*)0},{&g_768,(void*)0},{(void*)0,&g_768},{(void*)0,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,(void*)0},{(void*)0,&g_768}},{{(void*)0,&g_768},{(void*)0,(void*)0},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{(void*)0,&g_768},{(void*)0,(void*)0},{&g_768,(void*)0}},{{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,(void*)0},{&g_768,&g_768},{&g_768,&g_768},{(void*)0,&g_768},{&g_768,&g_768}},{{(void*)0,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,(void*)0},{&g_768,(void*)0},{(void*)0,&g_768},{&g_768,&g_768},{&g_768,&g_768}},{{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{(void*)0,&g_768},{&g_768,(void*)0},{(void*)0,(void*)0},{&g_768,(void*)0},{(void*)0,&g_768}},{{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768}},{{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{(void*)0,(void*)0},{&g_768,(void*)0},{(void*)0,(void*)0},{&g_768,&g_768},{(void*)0,&g_768},{&g_768,&g_768}},{{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{&g_768,&g_768},{(void*)0,(void*)0},{&g_768,(void*)0},{&g_768,&g_768},{&g_768,&g_768}}};
static const uint64_t g_818 = 0x3D008FD666DD0E0FLL;
static float g_880 = 0x1.CAF0DEp-68;
static float * const g_879 = &g_880;
static float * const *g_878[6][3][6] = {{{(void*)0,(void*)0,&g_879,(void*)0,&g_879,&g_879},{&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879},{&g_879,(void*)0,(void*)0,&g_879,&g_879,&g_879}},{{&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879},{&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879}},{{(void*)0,(void*)0,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879}},{{(void*)0,&g_879,&g_879,&g_879,&g_879,(void*)0},{&g_879,(void*)0,&g_879,&g_879,(void*)0,&g_879},{&g_879,(void*)0,&g_879,&g_879,&g_879,&g_879}},{{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,(void*)0,&g_879,&g_879,&g_879}},{{(void*)0,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,(void*)0},{(void*)0,&g_879,&g_879,&g_879,&g_879,&g_879}}};
static int64_t ** const g_913 = (void*)0;
static int64_t ** const *g_912 = &g_913;
static uint8_t g_980 = 1UL;
static int16_t *g_1034 = &g_25[1][2];
static int16_t * volatile *g_1033 = &g_1034;
static int16_t *g_1044 = &g_25[1][0];
static int16_t **g_1043 = &g_1044;
static int32_t * const  volatile g_1151 = &g_117;/* VOLATILE GLOBAL g_1151 */
static int32_t * volatile g_1176 = (void*)0;/* VOLATILE GLOBAL g_1176 */
static int16_t g_1193 = 0x11EFL;
static volatile uint8_t g_1216 = 0xB9L;/* VOLATILE GLOBAL g_1216 */
static int64_t **g_1253 = (void*)0;
static int64_t ***g_1252 = &g_1253;
static int64_t ****g_1251 = &g_1252;
static float ***g_1267[7][6][6] = {{{(void*)0,&g_584,&g_584,&g_584,(void*)0,&g_584},{&g_584,(void*)0,(void*)0,(void*)0,(void*)0,&g_584},{(void*)0,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,(void*)0,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{(void*)0,&g_584,(void*)0,(void*)0,&g_584,&g_584}},{{&g_584,&g_584,&g_584,&g_584,&g_584,(void*)0},{(void*)0,(void*)0,&g_584,&g_584,(void*)0,(void*)0},{&g_584,(void*)0,&g_584,(void*)0,(void*)0,&g_584},{(void*)0,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,(void*)0,&g_584,&g_584,(void*)0},{&g_584,&g_584,&g_584,&g_584,(void*)0,&g_584}},{{(void*)0,&g_584,&g_584,(void*)0,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{(void*)0,&g_584,&g_584,&g_584,(void*)0,(void*)0},{&g_584,(void*)0,(void*)0,(void*)0,(void*)0,&g_584},{(void*)0,(void*)0,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,(void*)0}},{{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,(void*)0,&g_584,&g_584,&g_584},{(void*)0,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,(void*)0,&g_584},{(void*)0,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584}},{{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{(void*)0,&g_584,&g_584,&g_584,(void*)0,&g_584},{&g_584,(void*)0,&g_584,&g_584,&g_584,&g_584},{(void*)0,(void*)0,&g_584,&g_584,(void*)0,&g_584}},{{&g_584,&g_584,(void*)0,&g_584,(void*)0,(void*)0},{&g_584,(void*)0,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,(void*)0,&g_584,&g_584,&g_584},{&g_584,(void*)0,&g_584,&g_584,&g_584,(void*)0},{(void*)0,(void*)0,(void*)0,&g_584,&g_584,&g_584},{&g_584,&g_584,(void*)0,&g_584,(void*)0,&g_584}},{{(void*)0,(void*)0,&g_584,&g_584,(void*)0,(void*)0},{&g_584,(void*)0,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,&g_584,&g_584,&g_584,&g_584},{&g_584,(void*)0,&g_584,&g_584,&g_584,&g_584},{&g_584,&g_584,(void*)0,&g_584,&g_584,&g_584},{(void*)0,&g_584,&g_584,&g_584,&g_584,&g_584}}};
static int32_t ***g_1276 = &g_606;
static int32_t **** volatile g_1275[1][8] = {{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276}};
static uint64_t g_1328 = 0x9BA30EF4C9CB89B3LL;
static int32_t * volatile g_1364 = &g_114;/* VOLATILE GLOBAL g_1364 */
static uint16_t g_1382 = 0xAEE9L;
static uint8_t *g_1395[2] = {&g_97[2],&g_97[2]};
static uint8_t **g_1394 = &g_1395[0];
static uint8_t *** const  volatile g_1393 = &g_1394;/* VOLATILE GLOBAL g_1393 */
static uint16_t g_1444 = 6UL;
static int32_t * volatile g_1479 = &g_117;/* VOLATILE GLOBAL g_1479 */
static volatile float g_1501 = 0x8.AF4AB3p+20;/* VOLATILE GLOBAL g_1501 */
static volatile int32_t g_1502 = 1L;/* VOLATILE GLOBAL g_1502 */
static int32_t g_1503 = 0L;
static uint32_t g_1504 = 0xB9043665L;
static volatile uint32_t g_1511 = 0x1014CB3DL;/* VOLATILE GLOBAL g_1511 */
static float * volatile *g_1596[4][2] = {{&g_139[3][3][0],&g_139[3][3][0]},{&g_139[3][3][0],&g_139[3][3][0]},{&g_139[3][3][0],&g_139[3][3][0]},{&g_139[3][3][0],&g_139[3][3][0]}};
static float * volatile * const  volatile *g_1595 = &g_1596[2][0];
static float * volatile * const  volatile **g_1594 = &g_1595;
static float * volatile * const  volatile ** volatile * volatile g_1593 = &g_1594;/* VOLATILE GLOBAL g_1593 */
static uint16_t g_1630 = 0x3D85L;
static uint8_t g_1657 = 5UL;
static int64_t g_1690 = 0L;
static volatile int32_t g_1768 = 0x5A814A65L;/* VOLATILE GLOBAL g_1768 */
static uint32_t *g_1784[7][1][6] = {{{(void*)0,&g_412,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_552,&g_552,(void*)0,&g_110,(void*)0,&g_552}},{{(void*)0,&g_412,&g_110,&g_110,&g_412,(void*)0}},{{&g_552,(void*)0,&g_110,(void*)0,&g_552,&g_552}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_412,(void*)0}},{{(void*)0,&g_412,&g_110,(void*)0,(void*)0,&g_110}},{{(void*)0,(void*)0,(void*)0,&g_412,(void*)0,(void*)0}}};
static uint32_t **g_1783 = &g_1784[0][0][3];
static uint32_t *** volatile g_1782[7] = {&g_1783,&g_1783,&g_1783,&g_1783,&g_1783,&g_1783,&g_1783};
static int8_t g_1790[6][3][4] = {{{(-1L),(-1L),0xFBL,0x07L},{(-1L),0x90L,4L,(-1L)},{0xEEL,0x07L,0xEEL,4L}},{{0L,0x07L,0xFBL,(-1L)},{0x07L,0x90L,0x90L,0x07L},{0xEEL,(-1L),0x90L,4L}},{{0x07L,0L,0xFBL,0L},{0L,0x90L,0xEEL,0L},{0xEEL,0L,4L,4L}},{{(-1L),(-1L),0xFBL,0x07L},{(-1L),0x90L,4L,(-1L)},{0xEEL,0x07L,0xEEL,4L}},{{0L,0x07L,0xFBL,(-1L)},{0x07L,0x90L,0x90L,0x07L},{0xEEL,(-1L),0x90L,4L}},{{0x07L,0L,0xFBL,0L},{0L,0x90L,0xEEL,0L},{0xEEL,0L,4L,4L}}};
static int64_t g_1791 = (-7L);
static volatile uint32_t g_1792 = 0x79F6F354L;/* VOLATILE GLOBAL g_1792 */
static uint32_t g_1796 = 5UL;
static int16_t g_1966 = 0xB1E4L;
static int32_t **g_1989 = &g_237;
static int8_t g_2040 = 0x94L;
static volatile uint32_t g_2041 = 1UL;/* VOLATILE GLOBAL g_2041 */
static const int32_t g_2069 = 0x806393FDL;
static uint32_t g_2072 = 0x4718C972L;
static uint32_t *g_2083 = &g_2072;
static uint32_t *g_2087 = &g_1504;
static volatile int16_t g_2164 = 0x595FL;/* VOLATILE GLOBAL g_2164 */
static volatile int8_t g_2165 = 0L;/* VOLATILE GLOBAL g_2165 */
static const int32_t g_2175 = (-4L);
static const int32_t * const g_2174 = &g_2175;
static const int32_t * const *g_2173[2] = {&g_2174,&g_2174};
static uint32_t g_2182 = 0xDBF44FF0L;
static volatile int64_t g_2268[5] = {7L,7L,7L,7L,7L};
static volatile int32_t g_2295 = 0x3C1D0A33L;/* VOLATILE GLOBAL g_2295 */
static uint64_t g_2308[3] = {0xB24A3FA4F76311B3LL,0xB24A3FA4F76311B3LL,0xB24A3FA4F76311B3LL};
static uint8_t g_2350[10][7][3] = {{{0UL,255UL,1UL},{0x68L,0x62L,0x22L},{0x2BL,2UL,0xBFL},{0x2FL,2UL,0x1FL},{1UL,0x62L,0x2FL},{0xB3L,255UL,0xB3L},{255UL,0x8BL,0UL}},{{0x9BL,254UL,0x48L},{254UL,0xC9L,0xD0L},{249UL,0x2CL,0x8BL},{254UL,249UL,0x62L},{0x9BL,0xB3L,0x2BL},{255UL,0x22L,0xCFL},{0xB3L,0UL,1UL}},{{1UL,1UL,249UL},{0x2FL,0x85L,249UL},{0x2BL,0xE9L,1UL},{0x68L,0xCFL,0xCFL},{0UL,0x2FL,0x2BL},{0x85L,252UL,0x62L},{0x8BL,0x42L,0x8BL}},{{0x33L,0x68L,0xD0L},{0x1FL,0x42L,0x48L},{0xC9L,252UL,0UL},{0xACL,0x2FL,0xB3L},{0x22L,0xCFL,0x2FL},{0x8BL,0xE9L,0x1FL},{0x42L,0x85L,0xBFL}},{{0x42L,1UL,0x22L},{0x8BL,0UL,1UL},{0x22L,0x22L,255UL},{252UL,0UL,0x9BL},{1UL,0x42L,254UL},{0x2BL,8UL,249UL},{255UL,1UL,254UL}},{{254UL,6UL,0x9BL},{0UL,0x1FL,255UL},{0UL,0xE9L,0xB3L},{2UL,0xC9L,1UL},{0xACL,255UL,0x2FL},{0x68L,255UL,0x2BL},{0xB3L,0xC9L,0x68L}},{{0UL,0xE9L,0UL},{249UL,0x1FL,0x85L},{1UL,6UL,0x8BL},{6UL,1UL,0x33L},{0x42L,8UL,0x1FL},{6UL,0x42L,0xC9L},{1UL,0UL,0xACL}},{{249UL,1UL,0x22L},{0UL,0xBFL,0x8BL},{0xB3L,0x8BL,0x42L},{0x68L,0UL,0x42L},{0xACL,0xCFL,0x8BL},{2UL,0x22L,0x22L},{0UL,0x68L,0xACL}},{{0UL,1UL,0xC9L},{254UL,0x48L,0x1FL},{255UL,2UL,0x33L},{0x2BL,0x48L,0x8BL},{1UL,1UL,0x85L},{252UL,0x68L,0UL},{1UL,0x22L,0x68L}},{{0x1FL,0xCFL,0x2BL},{0x48L,0UL,0x2FL},{0x48L,0x8BL,1UL},{0x1FL,0xBFL,0xB3L},{1UL,1UL,255UL},{252UL,0UL,0x9BL},{1UL,0x42L,254UL}}};
static int8_t *g_2424 = &g_104;
static int32_t g_2448 = 1L;
static uint16_t * const **g_2462 = (void*)0;
static uint16_t g_2466 = 0UL;
static const volatile int16_t * volatile ** volatile g_2524 = (void*)0;/* VOLATILE GLOBAL g_2524 */
static const volatile int16_t * volatile ** volatile *g_2523 = &g_2524;
static uint64_t *g_2580 = (void*)0;
static int8_t g_2624 = 0L;
static int32_t * volatile g_2625 = (void*)0;/* VOLATILE GLOBAL g_2625 */
static const int32_t g_2631 = 1L;
static uint32_t g_2726 = 0x871494E0L;
static int32_t * volatile g_2828[4][9][6] = {{{(void*)0,&g_2448,&g_406,(void*)0,&g_111,&g_406},{(void*)0,&g_2448,&g_111,&g_406,&g_2448,&g_2448},{&g_406,&g_2448,&g_2448,&g_406,&g_111,&g_2448},{(void*)0,&g_406,&g_111,(void*)0,&g_406,&g_2448},{(void*)0,&g_111,(void*)0,&g_406,&g_406,&g_2448},{&g_2448,&g_406,(void*)0,&g_406,&g_111,&g_111},{(void*)0,&g_2448,&g_406,(void*)0,&g_2448,&g_111},{&g_406,&g_2448,(void*)0,(void*)0,&g_111,&g_2448},{(void*)0,&g_2448,(void*)0,(void*)0,(void*)0,&g_2448}},{{(void*)0,&g_2448,&g_111,(void*)0,(void*)0,&g_2448},{&g_406,&g_111,&g_2448,(void*)0,&g_406,&g_2448},{(void*)0,&g_111,&g_111,&g_406,(void*)0,&g_406},{&g_2448,&g_406,&g_406,&g_2448,&g_2448,&g_111},{&g_406,(void*)0,&g_406,&g_2448,&g_2448,&g_406},{&g_406,&g_2448,&g_111,&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111,&g_111,&g_111,&g_2448},{&g_406,&g_406,&g_2448,&g_2448,&g_406,(void*)0},{&g_406,&g_111,&g_2448,&g_2448,&g_406,&g_406}},{{&g_2448,&g_406,&g_2448,&g_2448,&g_111,&g_2448},{&g_111,&g_111,&g_406,&g_2448,&g_111,&g_2448},{&g_2448,&g_2448,&g_2448,&g_406,&g_2448,&g_406},{(void*)0,(void*)0,&g_2448,&g_111,&g_2448,(void*)0},{(void*)0,&g_406,&g_2448,&g_406,&g_2448,&g_2448},{&g_2448,&g_2448,&g_111,&g_2448,&g_406,&g_111},{&g_111,&g_2448,&g_111,&g_2448,&g_2448,&g_406},{&g_2448,&g_406,&g_406,&g_2448,&g_2448,&g_111},{&g_406,(void*)0,&g_406,&g_2448,&g_2448,&g_406}},{{&g_406,&g_2448,&g_111,&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111,&g_111,&g_111,&g_2448},{&g_406,&g_406,&g_2448,&g_2448,&g_406,(void*)0},{&g_406,&g_111,&g_2448,&g_2448,&g_406,&g_406},{&g_2448,&g_406,&g_2448,&g_2448,&g_111,&g_2448},{&g_111,&g_111,&g_406,&g_2448,&g_111,&g_2448},{&g_2448,&g_2448,&g_2448,&g_406,&g_2448,&g_406},{(void*)0,(void*)0,&g_2448,&g_111,&g_2448,(void*)0},{(void*)0,&g_406,&g_2448,&g_406,&g_2448,&g_2448}}};
static uint16_t g_2838[3][10][3] = {{{65535UL,65535UL,0xA1EBL},{0xB257L,0xDBFEL,0xB396L},{0UL,0x3149L,8UL},{0xB257L,8UL,0xDBFEL},{65535UL,8UL,7UL},{0x58C2L,0x3149L,0x3149L},{2UL,0xDBFEL,7UL},{0xD46BL,65535UL,0xDBFEL},{0xD46BL,0UL,8UL},{2UL,0x72E4L,0xB396L}},{{0x58C2L,0UL,0xA1EBL},{65535UL,65535UL,0xA1EBL},{0xB257L,0xDBFEL,0xB396L},{0UL,0x3149L,8UL},{0xB257L,8UL,0xDBFEL},{65535UL,8UL,0xA345L},{0x3149L,0UL,0UL},{0UL,3UL,0xA345L},{0xB396L,0x9EA8L,3UL},{0xB396L,0x8F48L,0UL}},{{0UL,0x0D7CL,65534UL},{0x3149L,0x8F48L,65535UL},{0x72E4L,0x9EA8L,65535UL},{0xA1EBL,3UL,65534UL},{0UL,0UL,0UL},{0xA1EBL,0UL,3UL},{0x72E4L,0UL,0xA345L},{0x3149L,0UL,0UL},{0UL,3UL,0xA345L},{0xB396L,0x9EA8L,3UL}}};
static int32_t g_2847 = (-9L);
static int32_t *g_2904 = &g_1503;
static int64_t ** const ***g_2930 = (void*)0;
static int32_t * volatile *g_2994 = &g_2828[0][0][2];
static int32_t * volatile ** volatile g_2993 = &g_2994;/* VOLATILE GLOBAL g_2993 */
static int32_t * volatile ** const  volatile *g_2992[4] = {&g_2993,&g_2993,&g_2993,&g_2993};
static int32_t * volatile ** const  volatile * volatile * volatile g_2991 = &g_2992[3];/* VOLATILE GLOBAL g_2991 */
static volatile uint16_t g_3019 = 65535UL;/* VOLATILE GLOBAL g_3019 */
static float ** const *g_3146 = &g_584;
static float ** const **g_3145 = &g_3146;
static const uint8_t ***g_3188 = (void*)0;
static const uint8_t ****g_3187 = &g_3188;
static volatile float ** volatile **g_3233 = (void*)0;
static volatile float ** volatile ** const  volatile *g_3232 = &g_3233;
static int64_t g_3286 = 3L;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static uint16_t  func_41(int64_t  p_42, int8_t  p_43, const uint32_t  p_44, const int32_t * p_45);
static int32_t * func_48(uint8_t  p_49);
static float * func_55(uint64_t  p_56, int32_t * p_57, uint16_t  p_58, uint64_t  p_59, float * p_60);
static float  func_61(uint16_t  p_62);
static int8_t  func_72(int32_t ** p_73);
static int32_t ** func_74(float * p_75, int16_t  p_76, const float * p_77, uint32_t  p_78);
static uint32_t  func_81(const int64_t  p_82, int32_t * p_83, const int8_t  p_84);
static int64_t  func_85(int32_t  p_86, int32_t * p_87);
static int32_t  func_88(int32_t  p_89, const int32_t ** p_90, int32_t * p_91, float  p_92, float  p_93);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2174 g_2175
 * writes:
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int32_t l_2 = 0xE0BCD8C4L;
    int32_t l_21 = 0x9FA85F88L;
    int32_t l_23 = (-7L);
    int32_t l_28 = 0x40DE197BL;
    int32_t l_30 = 0x497C3167L;
    int32_t l_31 = 0x3A3C3A87L;
    int32_t l_32 = 1L;
    int32_t l_33 = (-10L);
    int32_t l_34[3][10] = {{0L,0x9496125BL,0x743E03EBL,6L,6L,0x743E03EBL,0x9496125BL,0L,0x9496125BL,0x743E03EBL},{0xFD1B63E3L,0xD15778C4L,6L,0xD15778C4L,0xFD1B63E3L,0x743E03EBL,0x743E03EBL,0xFD1B63E3L,0xD15778C4L,6L},{0L,0L,6L,0xFD1B63E3L,0x39A3A415L,0xFD1B63E3L,6L,0L,0L,6L}};
    uint8_t l_36 = 0xEEL;
    uint16_t l_50 = 65535UL;
    int32_t *l_54 = &l_33;
    float *l_2183 = &g_80[9][2][5];
    int16_t *** const l_2305 = (void*)0;
    uint32_t l_2454[7][3][8] = {{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}},{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}},{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}},{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}},{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}},{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}},{{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L},{1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L,1UL,0x15FF6416L}}};
    float ****l_2490[9] = {&g_1267[4][3][4],&g_1267[5][2][5],&g_1267[4][3][4],&g_1267[4][3][4],&g_1267[5][2][5],&g_1267[4][3][4],&g_1267[4][3][4],&g_1267[5][2][5],&g_1267[4][3][4]};
    float *****l_2489 = &l_2490[8];
    uint8_t l_2517[4];
    uint32_t l_2559 = 4294967289UL;
    int32_t l_2681 = (-1L);
    int32_t l_2787 = 1L;
    uint32_t l_2842 = 4294967290UL;
    int32_t l_2854 = 1L;
    float l_2878 = 0xE.0E1A0Fp+58;
    float l_2891[10] = {0xF.40D5FFp-58,0xD.4DAF73p+91,0xF.40D5FFp-58,0xF.40D5FFp-58,0xD.4DAF73p+91,0xF.40D5FFp-58,0xF.40D5FFp-58,0xD.4DAF73p+91,0xF.40D5FFp-58,0xF.40D5FFp-58};
    int16_t ***l_3029 = &g_1043;
    int16_t ****l_3028[4][4][7] = {{{&l_3029,(void*)0,(void*)0,&l_3029,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,(void*)0,&l_3029,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,(void*)0,(void*)0,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029}},{{&l_3029,&l_3029,&l_3029,&l_3029,(void*)0,&l_3029,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,(void*)0,&l_3029},{(void*)0,&l_3029,&l_3029,&l_3029,(void*)0,&l_3029,(void*)0}},{{&l_3029,(void*)0,&l_3029,(void*)0,&l_3029,&l_3029,&l_3029},{(void*)0,&l_3029,(void*)0,&l_3029,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,&l_3029,(void*)0,&l_3029,&l_3029,&l_3029}},{{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,(void*)0,(void*)0,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,(void*)0,&l_3029},{&l_3029,&l_3029,&l_3029,&l_3029,&l_3029,(void*)0,(void*)0}}};
    float l_3035 = (-0x6.1p-1);
    int32_t ***l_3090 = (void*)0;
    uint32_t **l_3102 = (void*)0;
    int32_t *l_3171 = &g_111;
    int64_t l_3176 = 7L;
    const int64_t * const *l_3260 = (void*)0;
    const int64_t * const **l_3259 = &l_3260;
    uint64_t l_3275 = 6UL;
    uint32_t l_3287 = 4294967295UL;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_2517[i] = 255UL;
    return (*g_2174);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t  func_41(int64_t  p_42, int8_t  p_43, const uint32_t  p_44, const int32_t * p_45)
{ /* block id: 1008 */
    uint64_t l_2230[1][7][9] = {{{0x608C8F5AE3319B8DLL,1UL,0x1E90580285273AFALL,0UL,1UL,0x97784316A9ABB461LL,0xC2DD3773D15C02F3LL,0xC2DD3773D15C02F3LL,0x608C8F5AE3319B8DLL},{0x608C8F5AE3319B8DLL,0UL,4UL,0UL,0x608C8F5AE3319B8DLL,0x4476048C18E35143LL,0x0D70BCBA6706D7DBLL,0xC2DD3773D15C02F3LL,0UL},{0UL,0x1E90580285273AFALL,0UL,4UL,1UL,18446744073709551612UL,0xBB00F5A57BA8E8B5LL,1UL,0x72B2A42E171D5DB1LL},{0x859C61D9C186A6D4LL,0x731551B4BF990A2ELL,1UL,0x72B2A42E171D5DB1LL,0x4476048C18E35143LL,0x4476048C18E35143LL,0x72B2A42E171D5DB1LL,1UL,0x731551B4BF990A2ELL},{0xC2DD3773D15C02F3LL,18446744073709551612UL,0x859C61D9C186A6D4LL,0x72B2A42E171D5DB1LL,0xEF9A912F49EB61E1LL,0x608C8F5AE3319B8DLL,1UL,0x1E90580285273AFALL,0UL},{0x97784316A9ABB461LL,0UL,0x4476048C18E35143LL,4UL,0x0D70BCBA6706D7DBLL,0x72B2A42E171D5DB1LL,18446744073709551607UL,5UL,18446744073709551607UL},{5UL,18446744073709551612UL,0UL,0UL,18446744073709551612UL,5UL,0UL,0x731551B4BF990A2ELL,18446744073709551607UL}}};
    int i, j, k;
    l_2230[0][5][4] = (*p_45);
    return p_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_24 g_27
 * writes: g_27 g_53
 */
static int32_t * func_48(uint8_t  p_49)
{ /* block id: 3 */
    float *l_51 = (void*)0;
    float *l_52 = &g_53;
    g_27[1] |= g_24;
    (*l_52) = 0x4.8p-1;
    return &g_27[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_1791 g_406 g_1593 g_1594 g_1595 g_1596 g_139 g_498 g_389 g_1043 g_1034 g_25 g_80 g_1503 g_880
 * writes: g_1043 g_177 g_1791 g_80 g_1503 g_880
 */
static float * func_55(uint64_t  p_56, int32_t * p_57, uint16_t  p_58, uint64_t  p_59, float * p_60)
{ /* block id: 995 */
    int32_t l_2184 = 0xC7D014BCL;
    int16_t ***l_2199 = &g_1043;
    int16_t ** const l_2206[8][7] = {{&g_1034,&g_1044,&g_1034,&g_1034,&g_1044,&g_1044,(void*)0},{&g_1034,&g_1034,&g_1034,&g_1034,(void*)0,&g_1044,(void*)0},{(void*)0,&g_1034,(void*)0,(void*)0,&g_1034,(void*)0,&g_1034},{(void*)0,&g_1034,&g_1044,&g_1034,(void*)0,&g_1044,&g_1034},{&g_1034,(void*)0,&g_1034,&g_1044,&g_1034,&g_1044,(void*)0},{&g_1034,&g_1034,&g_1034,&g_1044,&g_1044,&g_1034,&g_1034},{&g_1044,&g_1034,&g_1034,&g_1044,&g_1044,&g_1044,&g_1044},{&g_1034,&g_1034,&g_1034,&g_1044,&g_1044,&g_1034,&g_1044}};
    const int16_t **l_2208 = (void*)0;
    const int16_t ***l_2207[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const int16_t ****l_2209 = &l_2207[4];
    uint16_t *l_2210 = &g_177;
    uint64_t *l_2211[2];
    int32_t l_2212 = 0xDB7C9C67L;
    int64_t *l_2213[6][1] = {{&g_10[2]},{&g_10[2]},{&g_1690},{&g_10[2]},{&g_10[2]},{&g_1690}};
    float *l_2214[5][10] = {{&g_80[5][1][3],(void*)0,&g_880,(void*)0,&g_80[5][1][3],(void*)0,&g_80[9][2][5],&g_648[2][6],&g_80[9][2][5],&g_80[0][0][3]},{(void*)0,&g_80[9][2][5],&g_648[2][6],&g_80[9][2][5],&g_80[0][0][3],&g_648[4][0],&g_880,&g_880,&g_648[4][0],&g_80[0][0][3]},{&g_880,&g_80[9][2][5],&g_80[9][2][5],&g_880,&g_80[5][1][3],&g_880,(void*)0,(void*)0,&g_648[4][0],&g_80[9][2][5]},{&g_880,&g_80[5][1][3],&g_80[9][2][5],&g_648[4][0],&g_648[4][0],(void*)0,&g_80[9][2][5],(void*)0,&g_648[4][0],&g_648[4][0]},{&g_80[9][2][5],&g_880,&g_80[9][2][5],&g_880,&g_880,&g_80[9][2][5],&g_648[4][0],&g_80[3][0][1],&g_648[4][0],&g_880}};
    const uint16_t l_2227 = 0x8195L;
    int i, j;
    for (i = 0; i < 2; i++)
        l_2211[i] = &g_165;
    (*p_57) = (((l_2184 <= (safe_add_func_uint32_t_u_u((((+(g_1791 ^= ((safe_mod_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s(l_2184, 15)) , (((safe_mod_func_int64_t_s_s(((safe_mul_func_int8_t_s_s(p_59, ((((safe_sub_func_int32_t_s_s((+((void*)0 != l_2199)), (((safe_add_func_int32_t_s_s((*p_57), (safe_div_func_uint64_t_u_u((l_2212 = (safe_lshift_func_uint16_t_u_u(((((*l_2199) = &g_1034) == l_2206[7][4]) >= ((*l_2210) = (((((*l_2209) = l_2207[4]) == (void*)0) > p_56) < l_2184))), l_2184))), 0x40629FB627D42E82LL)))) <= (-1L)) >= p_59))) && l_2184) != l_2184) , 252UL))) <= p_58), l_2184)) && p_56) != l_2184)), 9UL)) > 1L))) , p_58) == (-5L)), 0xDA4BC37FL))) >= (-1L)) , 0x64B1EB02L);
    l_2212 = (((p_58 == (l_2184 = l_2184)) == (g_406 <= ((****g_1593) != l_2214[3][6]))) , (safe_div_func_float_f_f(0xC.F7256Ep+41, ((*p_60) = (((safe_add_func_float_f_f((safe_div_func_float_f_f(((l_2212 == ((+(((*p_57) > (safe_lshift_func_int8_t_s_s((!(safe_add_func_int64_t_s_s((l_2184 = (((*g_498) == l_2184) > (**g_1043))), l_2212))), p_56))) != 1L)) , 0x7.FEA400p-71)) != 0x1.1p+1), l_2212)), l_2212)) <= (*p_60)) != l_2227)))));
    return p_60;
}


/* ------------------------------------------ */
/* 
 * reads : g_720 g_116 g_879 g_2072 g_2069 g_1276 g_1503
 * writes: g_720 g_2083 g_2087 g_1503 g_117 g_880 g_2072 g_606 g_2173
 */
static float  func_61(uint16_t  p_62)
{ /* block id: 8 */
    int32_t *l_71[8][6][5] = {{{&g_24,(void*)0,&g_24,&g_24,(void*)0},{&g_24,&g_24,&g_24,&g_24,&g_24},{(void*)0,(void*)0,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,(void*)0,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,(void*)0},{&g_24,&g_24,&g_24,&g_24,&g_24}},{{(void*)0,&g_24,(void*)0,(void*)0,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24,&g_24},{&g_24,(void*)0,(void*)0,&g_24,(void*)0},{&g_24,&g_24,&g_24,&g_24,&g_24}}};
    int32_t l_1514 = 0x5D37FBECL;
    uint16_t ***l_1527 = (void*)0;
    int8_t *l_1591 = &g_104;
    uint16_t ***l_1625 = (void*)0;
    float l_1631[9][3] = {{0x3.0p+1,(-0x1.Fp-1),(-0x1.Fp-1)},{0x1.202DA6p-16,(-0x1.Fp-1),0x2.603276p+51},{0x0.Dp+1,0x3.0p+1,0x3.A5C5DCp+73},{0x1.202DA6p-16,0x1.202DA6p-16,0x3.A5C5DCp+73},{0x3.0p+1,0x0.Dp+1,0x2.603276p+51},{(-0x1.Fp-1),0x1.202DA6p-16,(-0x1.Fp-1)},{(-0x1.Fp-1),0x3.0p+1,0x1.202DA6p-16},{0x3.0p+1,(-0x1.Fp-1),(-0x1.Fp-1)},{0x1.202DA6p-16,(-0x1.Fp-1),0x2.603276p+51}};
    float l_1654[2][2];
    float l_1656[6][8] = {{0x3.CCE182p+3,0x7.A13E7Dp+69,0x3.CCE182p+3,0xA.0B7CF1p-74,0x4.ADAA56p-97,0xA.0B7CF1p-74,0x3.CCE182p+3,0x7.A13E7Dp+69},{0x4.ADAA56p-97,0xA.0B7CF1p-74,0x3.CCE182p+3,0x7.A13E7Dp+69,0x3.CCE182p+3,0xA.0B7CF1p-74,0x4.ADAA56p-97,0xA.0B7CF1p-74},{0x4.ADAA56p-97,0x7.A13E7Dp+69,0x0.Fp+1,0x7.A13E7Dp+69,0x4.ADAA56p-97,0x2.BF34FBp+93,0x4.ADAA56p-97,0x7.A13E7Dp+69},{0x3.CCE182p+3,0x7.A13E7Dp+69,0x3.CCE182p+3,0xA.0B7CF1p-74,0x4.ADAA56p-97,0xA.0B7CF1p-74,0x3.CCE182p+3,0x7.A13E7Dp+69},{0x4.ADAA56p-97,0xA.0B7CF1p-74,0x3.CCE182p+3,0x7.A13E7Dp+69,0x3.CCE182p+3,0xA.0B7CF1p-74,0x4.ADAA56p-97,0xA.0B7CF1p-74},{0x4.ADAA56p-97,0x7.A13E7Dp+69,0x0.Fp+1,0x7.A13E7Dp+69,0x4.ADAA56p-97,0x2.BF34FBp+93,0x4.ADAA56p-97,0x7.A13E7Dp+69}};
    uint32_t l_1660 = 0xA6CF74D1L;
    int64_t l_1689[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
    uint16_t l_1707 = 1UL;
    uint16_t **** const l_1714 = (void*)0;
    float * const **l_1733 = (void*)0;
    float * const ***l_1732 = &l_1733;
    int64_t * const l_1744 = &g_10[2];
    uint32_t **l_1883 = &g_1784[0][0][3];
    int32_t *l_1937[9][10] = {{&g_111,&g_406,&g_111,&g_406,&g_406,(void*)0,(void*)0,&g_406,&g_406,&g_111},{(void*)0,(void*)0,(void*)0,&g_406,(void*)0,(void*)0,(void*)0,&g_406,(void*)0,(void*)0},{(void*)0,(void*)0,&g_111,(void*)0,&g_406,&g_406,(void*)0,&g_111,(void*)0,(void*)0},{&g_111,(void*)0,(void*)0,&g_406,(void*)0,&g_406,(void*)0,(void*)0,&g_111,&g_111},{(void*)0,&g_406,(void*)0,(void*)0,(void*)0,(void*)0,&g_406,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_406,(void*)0,&g_406,(void*)0,(void*)0,&g_111,&g_111,(void*)0},{&g_111,(void*)0,&g_406,&g_406,(void*)0,&g_111,(void*)0,(void*)0,(void*)0,&g_111},{(void*)0,&g_406,(void*)0,&g_406,(void*)0,(void*)0,&g_111,&g_111,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_1514,(void*)0,&l_1514,(void*)0,(void*)0}};
    int32_t **l_1936 = &l_1937[1][7];
    int32_t *l_1953 = &g_1503;
    int8_t l_1970 = (-1L);
    const int64_t *l_2028 = &g_10[3];
    const int64_t **l_2027 = &l_2028;
    const int64_t ***l_2026 = &l_2027;
    const int64_t ****l_2025 = &l_2026;
    int32_t l_2044 = 0xF3913D54L;
    const int32_t *l_2068 = &g_2069;
    uint32_t **l_2084 = (void*)0;
    uint32_t *l_2086[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t **l_2085[4];
    int64_t l_2101 = 0x80DE2CC979397F02LL;
    int16_t *l_2124 = &g_1193;
    int32_t l_2142 = 0x1C6B61ECL;
    const int32_t * const *l_2171 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
            l_1654[i][j] = (-0x1.Ep-1);
    }
    for (i = 0; i < 4; i++)
        l_2085[i] = &l_2086[0];
    for (p_62 = 0; (p_62 <= 3); p_62 += 1)
    { /* block id: 11 */
        int32_t *l_70[5];
        int32_t **l_69 = &l_70[1];
        float *l_79 = &g_80[9][2][5];
        uint8_t *l_96 = &g_97[2];
        int32_t *l_112 = &g_111;
        uint16_t *l_1530[1];
        uint16_t **l_1529 = &l_1530[0];
        uint16_t ***l_1528 = &l_1529;
        int32_t ***l_1531 = &l_69;
        int16_t l_1539 = (-1L);
        uint64_t *l_1567 = &g_165;
        uint32_t l_1624[8][7][4] = {{{0x4C51C9D2L,1UL,1UL,4294967292UL},{1UL,0xAC57588FL,4294967295UL,0xB8510601L},{1UL,4294967294UL,4294967295UL,0x312469A4L},{0x5C41762AL,0x80F84385L,1UL,2UL},{6UL,4294967295UL,0x6F84EFD3L,8UL},{2UL,4294967288UL,0x3CB9FDCCL,0x80F84385L},{4294967295UL,0UL,1UL,4294967287UL}},{{0UL,1UL,0UL,4294967295UL},{0x80F84385L,0xAC57588FL,0xB8510601L,0UL},{0x3D00C541L,0x3887295EL,1UL,0xAC57588FL},{0x5C41762AL,1UL,1UL,0xDA578FF8L},{0x3D00C541L,0UL,0xB8510601L,8UL},{0x80F84385L,1UL,0UL,1UL},{0UL,1UL,1UL,0x5C41762AL}},{{4294967295UL,0x3D00C541L,0x3CB9FDCCL,0x6F84EFD3L},{2UL,0xAC57588FL,0x6F84EFD3L,0xB7866070L},{6UL,0x64D61B10L,1UL,0xFEE52C1DL},{0x5C41762AL,0xDA578FF8L,4294967295UL,0UL},{1UL,0x4C51C9D2L,4294967295UL,8UL},{1UL,1UL,1UL,1UL},{0x4C51C9D2L,0x12E6B76AL,1UL,1UL}},{{0x3CB9FDCCL,6UL,1UL,4294967295UL},{0UL,0xAC57588FL,0UL,4294967295UL},{0x64D61B10L,6UL,8UL,1UL},{0x5C41762AL,0x12E6B76AL,4UL,1UL},{1UL,1UL,4294967292UL,8UL},{0xDA578FF8L,0x4C51C9D2L,4294967288UL,0UL},{1UL,0xDA578FF8L,1UL,0xFEE52C1DL}},{{4294967288UL,0x64D61B10L,0x4C51C9D2L,0xB7866070L},{1UL,0xAC57588FL,4294967295UL,0x6F84EFD3L},{0x3887295EL,0x3D00C541L,0xCAEAF282L,0x5C41762AL},{0x5C41762AL,1UL,0x92691933L,1UL},{4294967294UL,1UL,0xB7866070L,8UL},{0x12E6B76AL,0UL,4294967295UL,0xDA578FF8L},{1UL,1UL,1UL,0xAC57588FL}},{{1UL,0x3887295EL,4294967295UL,0UL},{0x12E6B76AL,0xAC57588FL,0xB7866070L,4294967295UL},{4294967294UL,1UL,0x92691933L,4294967287UL},{0x5C41762AL,0UL,0xCAEAF282L,0x80F84385L},{0x3887295EL,4294967288UL,4294967295UL,8UL},{1UL,4294967295UL,4294967287UL,1UL},{0x5C41762AL,0x3CB9FDCCL,0xBCA984FDL,0x6F84EFD3L}},{{0x312469A4L,0UL,0x5C41762AL,0x92691933L},{4294967288UL,0xB8510601L,4UL,4UL},{4294967295UL,4294967295UL,6UL,0UL},{0xB7866070L,1UL,0x3887295EL,0UL},{1UL,2UL,0xCAEAF282L,0x3887295EL},{4294967295UL,2UL,0xFEE52C1DL,0UL},{2UL,1UL,0xBCA984FDL,0UL}},{{4294967287UL,4294967295UL,0x312469A4L,4UL},{0x4C51C9D2L,0xB8510601L,1UL,0x92691933L},{0xBCA984FDL,0UL,1UL,0x6F84EFD3L},{0xB7866070L,0x3CB9FDCCL,1UL,1UL},{4294967290UL,0xAC57588FL,1UL,0x3887295EL},{1UL,0x5C41762AL,2UL,0x3CB9FDCCL},{0xAC57588FL,4294967295UL,0xBCA984FDL,4294967295UL}}};
        uint8_t l_1649 = 0x58L;
        uint16_t l_1725[4];
        int16_t l_1765 = (-1L);
        float *l_1810 = &l_1654[0][1];
        int64_t * const *l_1839[9][8] = {{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744},{&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744,&l_1744}};
        int64_t * const **l_1838 = &l_1839[0][6];
        uint64_t l_1892[9] = {0x128C62EBAED66B1ELL,0xFD8FDDE9F39FC137LL,0xFD8FDDE9F39FC137LL,0x128C62EBAED66B1ELL,0xFD8FDDE9F39FC137LL,0xFD8FDDE9F39FC137LL,0x128C62EBAED66B1ELL,0xFD8FDDE9F39FC137LL,0xFD8FDDE9F39FC137LL};
        const int16_t l_1897 = 0x8AEBL;
        int8_t l_1908 = 0xAFL;
        uint32_t l_1967[8] = {0x83484736L,0x83484736L,0x83484736L,0x83484736L,0x83484736L,0x83484736L,0x83484736L,0x83484736L};
        uint32_t l_1987 = 0x68BD3900L;
        int32_t ****l_1998 = &g_1276;
        int16_t * const l_2002 = &g_25[5][2];
        int32_t ** const ***l_2052[2][8][5] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
        uint32_t l_2062 = 4294967295UL;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_70[i] = &g_24;
        for (i = 0; i < 1; i++)
            l_1530[i] = &g_177;
        for (i = 0; i < 4; i++)
            l_1725[i] = 1UL;
    }
    for (g_720 = (-15); (g_720 < 15); g_720++)
    { /* block id: 937 */
        return p_62;
    }
    if (((*g_116) = ((*l_1953) = ((g_2083 = &g_1504) != (g_2087 = l_1937[1][7])))))
    { /* block id: 944 */
        int32_t l_2096[4][6] = {{4L,0xD357697FL,4L,(-9L),(-9L),4L},{0x0A3099D8L,0x0A3099D8L,(-9L),(-10L),(-9L),0x0A3099D8L},{(-9L),0xD357697FL,(-10L),(-10L),0xD357697FL,(-9L)},{0x0A3099D8L,(-9L),(-10L),(-9L),0x0A3099D8L,0x0A3099D8L}};
        float *l_2102 = &l_1654[0][1];
        int32_t *l_2105 = &g_114;
        uint32_t **l_2111 = &l_2086[0];
        int64_t *****l_2114 = &g_1251;
        int i, j;
        (*l_2102) = ((safe_rshift_func_int8_t_s_s(p_62, 1)) , ((safe_mul_func_float_f_f(p_62, p_62)) >= (safe_add_func_float_f_f(p_62, (safe_sub_func_float_f_f(p_62, ((l_2096[3][1] < (((safe_div_func_float_f_f((safe_sub_func_float_f_f(l_2101, 0x4.969587p+52)), ((*g_879) = l_2096[3][2]))) != p_62) == p_62)) < p_62)))))));
        for (g_2072 = 0; (g_2072 == 39); g_2072++)
        { /* block id: 949 */
            uint32_t ** const l_2110 = &l_2086[6];
            int32_t l_2112 = 0xBF6D351EL;
            int16_t *l_2152 = (void*)0;
            int32_t l_2154[10][9] = {{0L,0xCC241F18L,1L,0L,(-5L),0L,0xE10E4636L,0xE1276682L,0x5722737FL},{0x5A68EC14L,(-1L),1L,0xE1276682L,0x49FDB6A4L,(-1L),9L,(-6L),(-1L)},{9L,0x3647E07DL,0xF47BDD2DL,0xA7D3A391L,0x0D45F4D3L,0xCC241F18L,1L,1L,1L},{(-1L),0x5A68EC14L,9L,0xA7D3A391L,9L,0x5A68EC14L,(-1L),(-1L),(-6L)},{0x5722737FL,(-8L),0x0925079BL,0xE1276682L,0xCC241F18L,0L,0L,0x5722737FL,0xE1276682L},{(-6L),9L,(-1L),0L,1L,0x0925079BL,1L,(-1L),(-6L)},{0x49FDB6A4L,(-1L),0x1BA8D494L,0xE10E4636L,0xAE221560L,(-1L),(-1L),1L,(-1L)},{0xE1276682L,(-5L),0x1BA8D494L,0x1BA8D494L,(-5L),0xE1276682L,1L,(-6L),0xA7D3A391L},{0x5A68EC14L,(-1L),(-1L),(-6L),9L,(-1L),0x49FDB6A4L,0xE1276682L,1L},{(-6L),0x3647E07DL,0x0925079BL,0xCC241F18L,0x24475AD7L,0x5722737FL,1L,(-6L),9L}};
            int64_t *l_2158 = &g_720;
            int64_t **l_2157 = &l_2158;
            int i, j;
            l_2105 = (void*)0;
        }
    }
    else
    { /* block id: 987 */
        const int32_t * const **l_2172[10][6] = {{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171},{&l_2171,&l_2171,&l_2171,&l_2171,&l_2171,&l_2171}};
        int32_t l_2181 = 1L;
        int i, j;
        l_2181 = (safe_sub_func_uint16_t_u_u(p_62, (((((*g_1276) = ((*l_2068) , &l_1937[2][9])) != (l_2171 = (g_2173[1] = l_2171))) && (((-1L) ^ ((*l_1953) ^= ((safe_div_func_uint16_t_u_u(((0UL | ((((!0xFE5B2F0DL) & p_62) < 246UL) <= p_62)) <= 0xB269L), p_62)) ^ 0UL))) && 1UL)) , l_2181)));
    }
    return p_62;
}


/* ------------------------------------------ */
/* 
 * reads : g_295 g_371 g_1504 g_1511 g_1216
 * writes: g_295 g_371 g_1504 g_1511
 */
static int8_t  func_72(int32_t ** p_73)
{ /* block id: 461 */
    int32_t l_1110 = 0xEEEACB47L;
    int64_t * const l_1126 = &g_720;
    int32_t l_1208 = 0xC0B9B40BL;
    int32_t l_1215[3][1][9] = {{{(-1L),0x4FE3C85BL,0x4FE3C85BL,(-1L),(-1L),0x4FE3C85BL,0x4FE3C85BL,(-1L),(-1L)}},{{0xB140016DL,0xD50E2FA4L,0xB140016DL,0xD50E2FA4L,0xB140016DL,0xD50E2FA4L,0xB140016DL,0xD50E2FA4L,0xB140016DL}},{{(-1L),(-1L),0x4FE3C85BL,0x4FE3C85BL,(-1L),(-1L),0x4FE3C85BL,0x4FE3C85BL,(-1L)}}};
    const int8_t l_1250 = 1L;
    int64_t ****l_1259 = &g_1252;
    float ***l_1264 = (void*)0;
    uint32_t *l_1291 = &g_552;
    uint32_t **l_1290[6] = {&l_1291,&l_1291,&l_1291,&l_1291,&l_1291,&l_1291};
    int16_t ***l_1320 = &g_1043;
    int64_t * const l_1387[6][5] = {{&g_10[2],(void*)0,&g_10[0],&g_10[0],(void*)0},{(void*)0,&g_720,&g_10[2],(void*)0,&g_10[0]},{&g_720,(void*)0,&g_720,(void*)0,&g_720},{&g_10[2],(void*)0,&g_720,&g_10[0],(void*)0},{&g_720,&g_720,&g_720,&g_720,&g_10[0]},{(void*)0,&g_720,&g_720,(void*)0,(void*)0}};
    int16_t l_1456 = 9L;
    int32_t *l_1508 = &l_1215[2][0][7];
    int32_t *l_1509[3][5];
    int8_t l_1510 = 0x42L;
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
            l_1509[i][j] = &l_1215[2][0][7];
    }
lbl_1507:
    for (g_295 = 10; (g_295 != 59); ++g_295)
    { /* block id: 464 */
        int16_t l_1100 = 4L;
        int32_t l_1111 = 0x6A55E405L;
        int32_t l_1130 = 0xA48CAE53L;
        int32_t l_1172 = 8L;
        uint8_t *l_1227 = &g_97[2];
        int16_t l_1245 = 0xB4A9L;
        float *l_1295 = &g_880;
        float **l_1294[6];
        int32_t l_1325 = 0xA8C81FFBL;
        const uint16_t ****l_1330 = (void*)0;
        const uint16_t *****l_1329 = &l_1330;
        int32_t ****l_1358[7][7] = {{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,(void*)0,(void*)0,&g_1276,&g_1276,&g_1276},{(void*)0,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,(void*)0,&g_1276,(void*)0,&g_1276,&g_1276},{(void*)0,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276}};
        uint8_t l_1477 = 1UL;
        int i, j;
        for (i = 0; i < 6; i++)
            l_1294[i] = &l_1295;
    }
    for (g_371 = (-4); (g_371 < 6); g_371 = safe_add_func_int32_t_s_s(g_371, 9))
    { /* block id: 638 */
        int32_t *l_1500[2];
        int i;
        for (i = 0; i < 2; i++)
            l_1500[i] = (void*)0;
        g_1504++;
    }
    if (g_295)
        goto lbl_1507;
    ++g_1511;
    return g_1216;
}


/* ------------------------------------------ */
/* 
 * reads : g_98 g_498 g_389 g_165 g_24 g_295 g_114 g_10 g_412 g_912 g_606 g_720 g_446 g_110 g_26 g_294 g_371 g_508 g_258 g_97 g_980 g_497 g_80 g_879 g_1033 g_1034 g_25 g_913 g_111 g_116
 * writes: g_99 g_165 g_878 g_97 g_412 g_294 g_110 g_114 g_371 g_980 g_10 g_880 g_1043 g_177 g_25 g_552 g_117 g_508 g_258
 */
static int32_t ** func_74(float * p_75, int16_t  p_76, const float * p_77, uint32_t  p_78)
{ /* block id: 342 */
    int32_t *l_867[7][7][2] = {{{&g_24,&g_24},{&g_24,(void*)0},{(void*)0,&g_114},{(void*)0,&g_114},{(void*)0,(void*)0},{&g_24,&g_24},{&g_24,(void*)0}},{{(void*)0,&g_114},{(void*)0,&g_114},{(void*)0,(void*)0},{&g_24,&g_24},{&g_24,(void*)0},{(void*)0,&g_114},{(void*)0,&g_114}},{{(void*)0,(void*)0},{&g_24,&g_24},{&g_24,(void*)0},{(void*)0,&g_114},{(void*)0,&g_114},{(void*)0,(void*)0},{&g_24,&g_24}},{{&g_24,(void*)0},{(void*)0,&g_114},{(void*)0,&g_114},{(void*)0,(void*)0},{&g_24,&g_24},{&g_24,(void*)0},{(void*)0,&g_24}},{{(void*)0,&g_24},{&g_117,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_117,&g_24},{(void*)0,&g_24},{&g_117,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_117,&g_24},{(void*)0,&g_24},{&g_117,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{&g_117,&g_24},{(void*)0,&g_24},{&g_117,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_117,&g_24},{(void*)0,&g_24}}};
    float * const *l_875[3];
    float * const **l_876 = (void*)0;
    float * const **l_877[5][8][6] = {{{&l_875[1],&l_875[2],(void*)0,(void*)0,&l_875[2],&l_875[1]},{&l_875[2],(void*)0,&l_875[2],(void*)0,&l_875[0],&l_875[2]},{&l_875[2],&l_875[2],&l_875[2],&l_875[0],&l_875[1],&l_875[2]},{&l_875[2],(void*)0,&l_875[0],(void*)0,&l_875[2],&l_875[2]},{&l_875[2],&l_875[0],(void*)0,(void*)0,&l_875[2],&l_875[2]},{&l_875[1],(void*)0,&l_875[2],&l_875[2],&l_875[2],&l_875[2]},{&l_875[0],&l_875[0],&l_875[2],&l_875[0],&l_875[2],&l_875[1]},{&l_875[2],(void*)0,&l_875[2],&l_875[2],&l_875[1],&l_875[2]}},{{&l_875[2],&l_875[2],&l_875[2],(void*)0,&l_875[0],&l_875[1]},{(void*)0,(void*)0,&l_875[2],(void*)0,&l_875[2],&l_875[2]},{(void*)0,&l_875[2],&l_875[2],&l_875[0],&l_875[2],&l_875[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_875[2]},{(void*)0,&l_875[0],&l_875[0],(void*)0,(void*)0,&l_875[2]},{&l_875[2],(void*)0,&l_875[2],&l_875[2],(void*)0,&l_875[2]},{&l_875[2],&l_875[0],&l_875[2],&l_875[0],(void*)0,&l_875[1]},{&l_875[0],(void*)0,(void*)0,&l_875[2],&l_875[2],&l_875[2]}},{{&l_875[1],&l_875[2],(void*)0,(void*)0,&l_875[2],&l_875[1]},{&l_875[2],(void*)0,&l_875[2],(void*)0,&l_875[0],&l_875[2]},{&l_875[2],&l_875[2],&l_875[2],&l_875[0],&l_875[1],&l_875[2]},{&l_875[2],(void*)0,&l_875[0],(void*)0,&l_875[2],&l_875[2]},{&l_875[2],&l_875[0],(void*)0,(void*)0,&l_875[2],&l_875[2]},{&l_875[1],(void*)0,&l_875[2],&l_875[2],&l_875[2],&l_875[2]},{&l_875[0],&l_875[0],&l_875[2],&l_875[0],&l_875[2],&l_875[1]},{&l_875[2],(void*)0,&l_875[2],&l_875[2],&l_875[1],&l_875[2]}},{{&l_875[2],&l_875[2],&l_875[2],(void*)0,&l_875[0],&l_875[1]},{(void*)0,(void*)0,&l_875[2],(void*)0,&l_875[2],&l_875[2]},{(void*)0,&l_875[2],&l_875[2],&l_875[0],&l_875[2],&l_875[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_875[2]},{(void*)0,&l_875[0],&l_875[0],(void*)0,(void*)0,&l_875[2]},{&l_875[2],(void*)0,&l_875[2],&l_875[2],(void*)0,&l_875[2]},{&l_875[2],&l_875[0],&l_875[2],&l_875[0],(void*)0,&l_875[1]},{&l_875[0],(void*)0,(void*)0,&l_875[2],&l_875[2],&l_875[2]}},{{&l_875[1],&l_875[2],(void*)0,(void*)0,&l_875[2],&l_875[1]},{&l_875[2],(void*)0,&l_875[2],(void*)0,&l_875[0],&l_875[2]},{&l_875[2],&l_875[2],&l_875[2],&l_875[0],&l_875[1],&l_875[2]},{&l_875[2],(void*)0,&l_875[0],(void*)0,&l_875[2],&l_875[2]},{&l_875[2],&l_875[0],(void*)0,(void*)0,&l_875[2],&l_875[2]},{&l_875[1],(void*)0,&l_875[2],&l_875[2],&l_875[2],&l_875[2]},{&l_875[0],&l_875[0],&l_875[2],&l_875[0],&l_875[2],&l_875[1]},{&l_875[2],(void*)0,&l_875[2],&l_875[2],&l_875[1],&l_875[2]}}};
    uint64_t l_885 = 0xA62475DE2C97F813LL;
    uint16_t *l_888[8] = {&g_177,&g_177,&g_177,&g_177,&g_177,&g_177,&g_177,&g_177};
    int32_t l_889 = 0x864FD20AL;
    uint8_t *l_890 = (void*)0;
    uint8_t *l_891 = &g_97[2];
    const uint64_t l_892 = 0x08AF9FBDA014212ALL;
    int64_t *l_916 = &g_720;
    int64_t *l_917[7] = {&g_10[2],(void*)0,(void*)0,&g_10[2],(void*)0,(void*)0,&g_10[2]};
    int64_t *l_918 = &g_10[2];
    int64_t *l_919[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int64_t *l_920 = &g_10[1];
    int64_t *l_921 = &g_720;
    int64_t *l_922 = &g_720;
    int64_t *l_923[5][5] = {{&g_720,(void*)0,(void*)0,&g_720,(void*)0},{&g_720,&g_720,&g_720,&g_720,&g_10[0]},{&g_720,(void*)0,(void*)0,&g_720,(void*)0},{&g_720,&g_720,&g_720,&g_720,&g_10[0]},{&g_720,(void*)0,(void*)0,&g_720,(void*)0}};
    int64_t ** const l_915[4][10] = {{&l_916,&l_919[2],&l_916,&l_921,&l_919[2],&l_918,&l_918,&l_919[2],&l_921,&l_916},{&l_917[2],&l_917[2],(void*)0,&l_919[2],&l_922,(void*)0,&l_922,&l_919[2],(void*)0,&l_917[2]},{&l_922,&l_918,&l_916,&l_922,&l_921,&l_921,&l_922,&l_916,&l_918,&l_922},{&l_916,&l_917[2],&l_918,&l_921,&l_917[2],&l_921,&l_918,&l_917[2],&l_916,&l_916}};
    int64_t ** const *l_914 = &l_915[0][5];
    int8_t l_943 = 0L;
    int8_t l_963 = (-1L);
    int64_t **l_1029 = &l_923[3][3];
    int64_t ***l_1028 = &l_1029;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_875[i] = (void*)0;
lbl_981:
    (*g_98) = l_867[1][0][0];
    if ((safe_mod_func_uint32_t_u_u((*g_498), (((((((safe_unary_minus_func_uint64_t_u((--g_165))) < 0x893FL) , ((((((((((safe_rshift_func_int8_t_s_u(g_24, ((*l_891) = (((((g_878[0][1][1] = l_875[2]) != (void*)0) , 0UL) | ((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u(l_885, (safe_rshift_func_uint8_t_u_s((((l_889 = (&p_75 != (void*)0)) || g_295) >= p_78), 3)))), g_114)) & p_78)) | 0x2AL)))) && 0x81D0E2BFL) != (-4L)) | g_114) > p_76) ^ g_10[2]) && 0x3ECAF740L) < p_76) | (-1L)) | 0x0B3F9D9D12604D79LL)) , l_892) && p_78) > g_114) , p_78))))
    { /* block id: 348 */
        int32_t l_936 = 9L;
        const float l_942 = 0xA.8F5246p+92;
        int32_t **l_983 = &g_237;
        int32_t l_984 = 0xEBDC0122L;
        int32_t l_985 = 0x7025579BL;
        int32_t l_986 = 4L;
        int32_t l_988 = 0L;
        for (g_412 = 0; (g_412 <= 3); g_412 += 1)
        { /* block id: 351 */
            int32_t l_893 = 3L;
            uint32_t *l_944 = &g_295;
            int16_t *l_945[4][7][9] = {{{&g_25[5][2],&g_371,(void*)0,&g_25[2][1],(void*)0,&g_371,&g_25[5][2],&g_25[4][1],&g_25[4][1]},{&g_25[5][0],(void*)0,&g_25[1][0],&g_371,(void*)0,&g_371,&g_25[5][2],&g_25[1][0],&g_371},{&g_25[5][2],(void*)0,&g_371,&g_371,(void*)0,&g_25[5][2],&g_371,&g_371,(void*)0},{&g_371,&g_25[5][2],&g_371,&g_25[1][0],&g_25[5][0],&g_25[5][2],&g_371,(void*)0,&g_371},{(void*)0,&g_371,&g_25[5][2],&g_25[5][0],(void*)0,&g_25[5][0],&g_25[5][2],&g_371,(void*)0},{&g_25[5][2],&g_25[4][0],&g_25[5][0],(void*)0,&g_371,&g_371,&g_25[5][2],&g_25[5][2],&g_371},{&g_371,&g_25[5][2],&g_371,&g_25[5][2],&g_25[5][2],&g_25[4][0],&g_25[6][1],&g_25[4][1],&g_371}},{{&g_25[5][2],&g_371,&g_25[5][2],&g_371,&g_25[1][0],&g_25[5][0],&g_25[5][2],&g_371,(void*)0},{(void*)0,&g_25[5][2],(void*)0,&g_25[2][1],&g_371,(void*)0,&g_371,&g_25[2][1],(void*)0},{&g_371,&g_371,&g_25[5][2],(void*)0,(void*)0,&g_25[4][0],(void*)0,&g_371,&g_25[5][2]},{&g_25[5][2],&g_25[5][2],(void*)0,&g_371,&g_371,&g_25[5][2],(void*)0,&g_371,(void*)0},{&g_25[5][2],&g_25[4][0],&g_25[5][2],&g_25[5][0],&g_371,&g_25[5][2],&g_25[4][2],&g_25[5][2],(void*)0},{&g_25[4][1],&g_371,(void*)0,&g_25[1][0],&g_25[6][1],&g_371,(void*)0,&g_371,&g_25[6][1]},{&g_25[5][0],&g_25[5][2],&g_25[5][2],&g_25[5][0],(void*)0,&g_25[1][0],&g_371,&g_371,&g_25[4][2]}},{{&g_371,(void*)0,&g_371,&g_371,(void*)0,&g_25[1][0],&g_25[5][2],(void*)0,&g_371},{(void*)0,(void*)0,&g_25[5][0],(void*)0,(void*)0,&g_371,&g_25[5][2],(void*)0,&g_25[5][2]},{(void*)0,&g_371,&g_25[5][2],&g_25[2][1],&g_25[6][1],(void*)0,&g_371,&g_25[5][2],&g_371},{&g_371,&g_371,&g_371,&g_371,&g_371,&g_371,&g_25[4][0],(void*)0,&g_371},{&g_25[5][2],(void*)0,&g_371,&g_25[5][2],&g_371,&g_25[1][0],&g_25[5][2],(void*)0,&g_371},{&g_25[4][0],&g_25[1][0],(void*)0,(void*)0,(void*)0,&g_25[1][0],&g_25[4][0],&g_371,&g_25[5][2]},{&g_371,(void*)0,&g_371,&g_25[5][0],&g_371,&g_371,&g_371,&g_371,&g_371}},{{&g_371,&g_25[5][2],&g_25[5][2],&g_25[1][0],&g_25[1][0],&g_25[5][2],&g_25[5][2],&g_371,&g_25[5][2]},{(void*)0,&g_371,&g_25[5][2],&g_371,&g_25[5][2],&g_25[5][2],&g_25[6][1],&g_371,&g_25[4][1]},{(void*)0,(void*)0,&g_25[4][0],&g_25[5][2],&g_25[5][2],&g_25[5][2],&g_25[4][0],(void*)0,(void*)0},{(void*)0,&g_25[5][2],&g_371,&g_371,(void*)0,&g_25[5][2],(void*)0,&g_25[2][1],&g_371},{&g_25[5][2],&g_371,&g_371,&g_371,&g_25[5][2],&g_25[5][2],&g_371,&g_371,&g_371},{(void*)0,&g_25[5][2],(void*)0,&g_25[4][0],&g_371,(void*)0,&g_371,&g_25[5][2],(void*)0},{(void*)0,&g_371,&g_25[5][2],(void*)0,&g_371,&g_25[4][2],&g_371,(void*)0,&g_25[5][2]}}};
            int32_t l_946 = 0xA02A8456L;
            int8_t l_953 = 1L;
            int32_t **l_982 = &l_867[1][0][0];
            uint32_t l_989 = 0xF24F9D6EL;
            int i, j, k;
            l_893 ^= g_10[g_412];
            l_946 = ((safe_unary_minus_func_int8_t_s((safe_mul_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(((!((safe_sub_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((((safe_rshift_func_int16_t_s_s((l_893 = (((((((safe_sub_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_s((g_294[0][1][0] = 1L), ((l_914 = g_912) == ((safe_sub_func_int64_t_s_s((((safe_lshift_func_int8_t_s_u((safe_div_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((safe_add_func_uint64_t_u_u(18446744073709551615UL, (safe_mul_func_uint16_t_u_u(l_936, ((!((safe_sub_func_uint8_t_u_u(g_389, ((g_606 == (((safe_div_func_uint64_t_u_u((&g_412 == &g_412), p_76)) , p_76) , (void*)0)) , g_720))) || p_76)) , 0x965BL))))), l_893)) && p_78), g_446[0])), 0)) == 1UL) , (-1L)), l_943)) , &g_767[0][0][1])))) == g_114) ^ p_78), 0xDFL)), p_76)) , l_944) == (void*)0) , l_936) != 0x7C8D26A196467FADLL) , g_10[g_412]) , 0x86EFL)), 9)) & 0xDC8054CDB20CCD5DLL) , l_893), p_78)), 0x81L)) & 255UL)) >= p_76), g_24)), 1UL)))) >= 0x309A9D60L);
            for (g_110 = 0; (g_110 <= 3); g_110 += 1)
            { /* block id: 359 */
                int32_t l_961 = 0L;
                uint32_t l_962 = 0x81F27F0CL;
                float *l_975 = &g_648[5][1];
                float **l_974 = &l_975;
                float *l_977[6];
                float **l_976 = &l_977[1];
                uint8_t *l_978 = (void*)0;
                uint8_t *l_979 = &g_980;
                int i, j;
                for (i = 0; i < 6; i++)
                    l_977[i] = &g_80[9][2][5];
                l_946 |= (safe_lshift_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((((((safe_div_func_uint16_t_u_u(0x6A94L, g_26[(g_110 + 3)][g_110])) || l_953) , (1UL != (((((0xCE9D9A97CF3B8012LL > ((safe_mul_func_int16_t_s_s(((+(((((safe_sub_func_uint16_t_u_u(g_26[(g_110 + 3)][g_110], g_294[0][5][1])) < ((g_371 |= ((safe_rshift_func_int8_t_s_s(((g_114 ^= l_961) >= ((void*)0 != &g_237)), p_76)) && l_961)) > (-10L))) < g_10[2]) > l_962) || l_963)) , l_936), 0x3583L)) != 0x61A8L)) <= p_76) == p_78) & p_78) < g_10[0]))) == l_961) || g_26[(g_110 + 3)][g_110]) , g_371), l_961)), p_76));
                l_961 |= (((safe_div_func_int64_t_s_s((1L > (((safe_rshift_func_uint8_t_u_s(g_508, 3)) || g_258[0][3][4]) > (p_78 || (p_76 <= (safe_mul_func_uint16_t_u_u((((*l_979) = (((((((safe_rshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((((*l_974) = l_867[1][0][0]) != ((*l_976) = l_944)), (p_76 , ((*l_891) &= (g_720 != g_720))))), 8)) & g_295) > l_946) , p_78) , 0x9.0A10DEp-53) , l_893) , g_412)) <= 0x47L), l_962)))))), 0x7138BE31AC2966F3LL)) , l_962) >= 0xB5EBL);
                if (p_78)
                    continue;
                for (g_980 = 0; (g_980 <= 3); g_980 += 1)
                { /* block id: 371 */
                    for (l_961 = 3; (l_961 >= 0); l_961 -= 1)
                    { /* block id: 374 */
                        if (p_76)
                            goto lbl_981;
                        return l_983;
                    }
                }
                for (l_889 = 3; (l_889 >= 0); l_889 -= 1)
                { /* block id: 381 */
                    float l_987 = (-0x4.Cp-1);
                    ++l_989;
                }
            }
        }
        return &g_237;
    }
    else
    { /* block id: 387 */
        uint16_t ****l_1023[8][9] = {{&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0]},{(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0]},{&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0]},{(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0]},{&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0]},{(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0]},{&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0],&g_527[0][0]},{(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0],(void*)0,(void*)0,&g_527[0][0]}};
        int64_t **l_1026 = (void*)0;
        int64_t ***l_1025[5][3] = {{&l_1026,&l_1026,&l_1026},{&l_1026,&l_1026,&l_1026},{&l_1026,&l_1026,&l_1026},{&l_1026,&l_1026,&l_1026},{&l_1026,&l_1026,&l_1026}};
        uint32_t l_1030 = 0xFBDBCABEL;
        int32_t l_1047 = 0L;
        int32_t l_1048 = (-8L);
        uint16_t * const *l_1056 = &l_888[3];
        uint16_t * const **l_1055 = &l_1056;
        const int32_t *l_1071 = &l_889;
        const int32_t **l_1070 = &l_1071;
        uint32_t l_1073 = 0x1B6EBAD1L;
        int i, j;
        for (g_980 = 0; (g_980 <= 4); g_980 += 1)
        { /* block id: 390 */
            const uint32_t l_992 = 4294967295UL;
            uint16_t * const *l_1054 = &l_888[4];
            uint16_t * const **l_1053 = &l_1054;
            float *l_1060 = (void*)0;
            uint16_t l_1065 = 3UL;
            int32_t l_1093 = 3L;
            for (p_76 = 0; (p_76 <= 4); p_76 += 1)
            { /* block id: 393 */
                uint16_t **l_995 = &l_888[5];
                uint16_t ** const *l_994 = &l_995;
                uint16_t ** const **l_993 = &l_994;
                uint16_t ** const ***l_996 = &l_993;
                int64_t *l_1001 = &g_10[2];
                int32_t l_1020 = (-1L);
                int32_t l_1032 = 0xBC2DFF35L;
                int16_t * volatile *l_1035 = &g_1034;
                int16_t **l_1041[4] = {&g_1034,&g_1034,&g_1034,&g_1034};
                int i;
                if (l_992)
                    break;
                if (p_76)
                    continue;
                (*l_996) = l_993;
                for (l_943 = 1; (l_943 <= 4); l_943 += 1)
                { /* block id: 399 */
                    int16_t l_1012 = 4L;
                    const uint16_t l_1021 = 0xE9E7L;
                    uint16_t **l_1031 = &l_888[2];
                    int32_t l_1046 = 0L;
                    uint16_t l_1049 = 65533UL;
                    for (g_110 = 1; (g_110 <= 4); g_110 += 1)
                    { /* block id: 402 */
                        const int32_t l_1009 = (-1L);
                        uint16_t ****l_1022 = &g_527[0][0];
                        int32_t l_1024 = 0xCDB58BD0L;
                        int64_t ****l_1027 = &l_1025[4][2];
                        l_1032 ^= (safe_mul_func_int8_t_s_s(((((safe_mod_func_uint32_t_u_u(((((void*)0 != l_1001) ^ ((((*l_1001) = (((safe_sub_func_int8_t_s_s((&g_913 != (l_1028 = ((*l_1027) = ((((safe_mul_func_int8_t_s_s((safe_sub_func_int8_t_s_s((safe_unary_minus_func_uint32_t_u((*g_498))), ((((l_1024 = (l_1009 || ((safe_mul_func_int16_t_s_s(l_992, l_1012)) & (safe_rshift_func_int16_t_s_u((((safe_div_func_int64_t_s_s(((safe_mod_func_uint64_t_u_u((+(0x9234L & (l_1020 && p_76))), p_78)) || l_1021), p_76)) , l_1022) != l_1023[6][6]), 12))))) && g_10[3]) ^ 0x33250837L) == 1L))), 0x0CL)) , p_76) || p_78) , l_1025[4][2])))), p_78)) == g_294[0][5][1]) , l_1030)) && 0x927EF522B59A5A08LL) & 0x8B04E3C3L)) < l_1030), l_1021)) , l_1031) == (**l_993)) && (**g_497)), p_76));
                        (*g_98) = &l_1032;
                        (*g_879) = (*p_75);
                    }
                    for (p_78 = 0; (p_78 <= 4); p_78 += 1)
                    { /* block id: 413 */
                        int8_t *l_1038[3][6] = {{&g_294[0][5][1],(void*)0,(void*)0,&g_294[0][5][1],(void*)0,(void*)0},{&g_294[0][5][1],(void*)0,(void*)0,&g_294[0][5][1],(void*)0,(void*)0},{&g_294[0][5][1],(void*)0,(void*)0,&g_294[0][5][1],(void*)0,(void*)0}};
                        int16_t ***l_1042[7] = {&l_1041[1],&l_1041[3],&l_1041[3],&l_1041[1],&l_1041[3],&l_1041[3],&l_1041[1]};
                        int32_t l_1045 = 0L;
                        int i, j;
                        l_1035 = g_1033;
                        l_1046 &= (safe_mul_func_int8_t_s_s((g_294[0][8][2] = 0x15L), (((p_76 , (((*g_1034) < (*g_1034)) | (safe_mul_func_int8_t_s_s(p_78, (((g_26[0][7] || g_980) , (g_1043 = l_1041[1])) == ((((((**g_1033) , g_508) >= l_1045) != 0L) <= (**g_1033)) , (void*)0)))))) || l_1020) == (**g_1033))));
                        ++l_1049;
                    }
                }
            }
            for (g_177 = 0; (g_177 <= 4); g_177 += 1)
            { /* block id: 424 */
                uint32_t l_1052[1];
                uint16_t * const **l_1057 = &l_1056;
                float *l_1062 = &g_648[0][2];
                float **l_1061 = &l_1062;
                int32_t **l_1076 = &l_867[1][0][0];
                int32_t l_1092 = 0x561395C2L;
                int32_t l_1094 = 1L;
                int i;
                for (i = 0; i < 1; i++)
                    l_1052[i] = 0x1B36EE8FL;
                (*g_98) = p_77;
                l_1052[0] |= l_1048;
                if ((l_992 <= ((4294967292UL == (l_1053 == (l_1057 = l_1055))) && (safe_mul_func_int8_t_s_s(((l_1060 != (p_76 , ((*l_1061) = p_75))) < l_1047), (safe_div_func_int16_t_s_s(((p_75 == (*g_497)) != l_1065), p_76)))))))
                { /* block id: 429 */
                    uint32_t l_1072 = 18446744073709551607UL;
                    uint8_t l_1077 = 1UL;
                    if ((((((((18446744073709551615UL && 0L) & ((safe_rshift_func_int8_t_s_u((-10L), 3)) != ((safe_sub_func_int64_t_s_s(0xD21913ADDDE9C0A3LL, p_76)) == (((4294967287UL <= 0x2BA4A3F2L) <= (((((void*)0 != l_1070) <= 1L) & l_1048) ^ l_1048)) || l_1047)))) , (void*)0) == &g_767[6][8][1]) | l_992) , p_76) , p_78))
                    { /* block id: 430 */
                        l_1072 = (*p_75);
                        l_1073++;
                        return &g_237;
                    }
                    else
                    { /* block id: 434 */
                        --l_1077;
                    }
                }
                else
                { /* block id: 437 */
                    uint8_t l_1080 = 0xB5L;
                    l_1048 ^= l_1080;
                    for (l_885 = 0; (l_885 <= 4); l_885 += 1)
                    { /* block id: 441 */
                        uint32_t l_1090 = 0xC5FAEA39L;
                        float **l_1091[4][7][8] = {{{&l_1062,&l_1060,(void*)0,&l_1060,&l_1062,&l_1060,(void*)0,&l_1060},{&l_1060,&l_1060,(void*)0,&l_1062,(void*)0,&l_1060,&l_1060,&l_1060},{&l_1060,&l_1060,(void*)0,&l_1062,(void*)0,&l_1062,(void*)0,&l_1062},{(void*)0,&l_1062,&l_1062,&l_1060,&l_1060,&l_1060,(void*)0,&l_1062},{(void*)0,&l_1060,(void*)0,&l_1062,&l_1060,&l_1062,&l_1060,&l_1060},{(void*)0,&l_1062,&l_1060,&l_1060,&l_1060,&l_1060,&l_1060,&l_1062},{&l_1062,&l_1062,(void*)0,&l_1060,&l_1062,&l_1060,&l_1060,&l_1060}},{{(void*)0,&l_1060,&l_1062,&l_1062,&l_1062,&l_1062,&l_1060,&l_1062},{&l_1060,&l_1062,(void*)0,&l_1060,(void*)0,&l_1062,&l_1060,&l_1060},{(void*)0,&l_1062,&l_1060,&l_1060,&l_1062,&l_1062,&l_1060,&l_1062},{&l_1062,&l_1060,(void*)0,&l_1060,&l_1062,&l_1060,(void*)0,&l_1060},{(void*)0,&l_1062,&l_1062,&l_1062,(void*)0,&l_1060,&l_1062,&l_1060},{&l_1060,&l_1062,(void*)0,&l_1060,&l_1062,&l_1062,&l_1062,&l_1062},{(void*)0,&l_1060,(void*)0,&l_1060,&l_1062,&l_1060,&l_1062,&l_1060}},{{&l_1062,&l_1062,&l_1062,&l_1060,&l_1060,&l_1060,(void*)0,&l_1062},{(void*)0,&l_1060,(void*)0,&l_1062,&l_1060,&l_1062,&l_1060,&l_1060},{(void*)0,&l_1062,&l_1060,&l_1060,&l_1060,&l_1060,&l_1060,&l_1062},{&l_1062,&l_1062,(void*)0,&l_1060,&l_1062,&l_1060,&l_1060,&l_1060},{(void*)0,&l_1060,&l_1062,&l_1062,&l_1062,&l_1062,&l_1060,&l_1062},{&l_1060,&l_1062,(void*)0,&l_1060,(void*)0,&l_1062,&l_1060,&l_1060},{(void*)0,&l_1062,&l_1060,&l_1060,&l_1062,&l_1062,&l_1060,&l_1062}},{{&l_1062,&l_1060,(void*)0,&l_1060,&l_1062,&l_1060,(void*)0,&l_1060},{(void*)0,&l_1062,&l_1062,&l_1062,(void*)0,&l_1060,&l_1062,&l_1060},{&l_1060,&l_1062,(void*)0,&l_1060,&l_1062,&l_1062,&l_1062,&l_1062},{(void*)0,&l_1060,(void*)0,&l_1060,&l_1062,&l_1060,&l_1062,&l_1060},{&l_1062,&l_1062,&l_1062,&l_1060,&l_1060,&l_1060,(void*)0,&l_1062},{(void*)0,&l_1060,(void*)0,&l_1062,&l_1060,&l_1062,&l_1060,&l_1060},{(void*)0,&l_1060,&l_1060,&l_1062,&l_1062,&l_1062,&l_1060,&l_1060}}};
                        int i, j, k;
                        (*g_116) = (((((g_258[0][0][5] && ((*g_912) == (void*)0)) ^ ((((safe_rshift_func_int16_t_s_u(((*g_1034) = (-3L)), 13)) < (safe_mod_func_uint16_t_u_u(((((*l_918) = ((((g_552 = p_78) == (l_1090 = (~(l_1047 = (l_1065 > (((safe_sub_func_uint32_t_u_u(4294967292UL, (((p_78 != p_78) > 0x06DB8343L) , g_111))) | p_78) & g_980)))))) , (void*)0) != l_1091[0][1][2])) & l_1030) , 0xB90EL), p_78))) <= l_992) & l_1065)) | p_76) || 0xC82CB5CE5838A2CCLL) || p_76);
                    }
                }
                for (l_1065 = 0; (l_1065 <= 4); l_1065 += 1)
                { /* block id: 452 */
                    uint32_t l_1095[4][9] = {{0UL,0x1D947CE1L,0xFFC6498AL,0UL,0xFFC6498AL,0x1D947CE1L,0UL,0x1D947CE1L,0xFFC6498AL},{18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL},{0UL,0x1D947CE1L,0xFFC6498AL,0UL,0xFFC6498AL,0x1D947CE1L,0UL,0x1D947CE1L,0xFFC6498AL},{18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL}};
                    int i, j;
                    for (l_1047 = 0; l_1047 < 1; l_1047 += 1)
                    {
                        for (g_508 = 0; g_508 < 6; g_508 += 1)
                        {
                            for (p_76 = 0; p_76 < 8; p_76 += 1)
                            {
                                g_258[l_1047][g_508][p_76] = 1UL;
                            }
                        }
                    }
                    l_1095[2][0]--;
                }
            }
        }
        (*g_98) = &l_1048;
    }
    return &g_237;
}


/* ------------------------------------------ */
/* 
 * reads : g_10
 * writes:
 */
static uint32_t  func_81(const int64_t  p_82, int32_t * p_83, const int8_t  p_84)
{ /* block id: 29 */
    float *l_140[6][1][10] = {{{&g_80[9][2][5],&g_80[7][1][5],&g_80[2][1][3],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[2][1][3],&g_80[7][1][5],&g_80[9][2][5],&g_80[4][2][3]}},{{&g_80[2][0][1],&g_80[9][2][5],(void*)0,(void*)0,&g_80[8][3][5],&g_80[4][3][4],&g_80[2][1][3],&g_80[4][3][4],&g_80[8][3][5],(void*)0}},{{(void*)0,&g_80[7][1][5],(void*)0,(void*)0,&g_80[2][1][3],&g_80[4][2][3],(void*)0,&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5]}},{{(void*)0,&g_80[4][3][4],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[4][3][4],(void*)0,&g_80[9][2][5],&g_80[2][0][1],&g_80[7][1][5]}},{{&g_80[2][0][1],&g_80[9][2][5],(void*)0,&g_80[4][3][4],&g_80[9][2][5],&g_80[9][2][5],&g_80[9][2][5],&g_80[4][3][4],(void*)0,&g_80[9][2][5]}},{{&g_80[9][2][5],&g_80[9][2][5],(void*)0,&g_80[4][2][3],&g_80[2][1][3],(void*)0,(void*)0,&g_80[7][1][5],(void*)0,(void*)0}}};
    int32_t l_141 = (-9L);
    uint8_t l_162 = 0x53L;
    uint64_t *l_164 = &g_165;
    int32_t l_172 = 0x9BD7B0D8L;
    int16_t *l_173 = &g_25[5][2];
    uint16_t *l_174 = (void*)0;
    uint16_t *l_175 = (void*)0;
    uint16_t *l_176[7][10][3] = {{{&g_177,&g_177,(void*)0},{(void*)0,&g_177,&g_177},{&g_177,&g_177,&g_177},{(void*)0,&g_177,&g_177},{(void*)0,(void*)0,&g_177},{(void*)0,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177}},{{&g_177,(void*)0,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{(void*)0,&g_177,&g_177},{(void*)0,&g_177,(void*)0},{(void*)0,&g_177,&g_177},{&g_177,(void*)0,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,(void*)0},{(void*)0,&g_177,&g_177}},{{&g_177,&g_177,&g_177},{(void*)0,&g_177,&g_177},{(void*)0,(void*)0,&g_177},{(void*)0,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,(void*)0,&g_177},{&g_177,&g_177,&g_177}},{{&g_177,&g_177,&g_177},{&g_177,(void*)0,(void*)0},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{(void*)0,&g_177,(void*)0},{&g_177,&g_177,&g_177},{(void*)0,&g_177,&g_177},{&g_177,(void*)0,&g_177},{&g_177,&g_177,(void*)0},{&g_177,&g_177,&g_177}},{{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,(void*)0,&g_177},{&g_177,&g_177,(void*)0},{&g_177,&g_177,(void*)0},{&g_177,&g_177,(void*)0},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,(void*)0,(void*)0}},{{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{(void*)0,&g_177,(void*)0},{&g_177,&g_177,&g_177},{(void*)0,&g_177,&g_177},{&g_177,(void*)0,&g_177},{&g_177,&g_177,(void*)0},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177}},{{&g_177,&g_177,&g_177},{&g_177,(void*)0,&g_177},{&g_177,&g_177,(void*)0},{&g_177,&g_177,(void*)0},{&g_177,&g_177,(void*)0},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177},{&g_177,(void*)0,(void*)0},{&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177}}};
    int64_t *l_178[4];
    int32_t l_179 = (-7L);
    const float *l_212 = &g_80[4][0][3];
    uint16_t **l_230 = &l_176[1][6][1];
    uint16_t ***l_229 = &l_230;
    uint64_t l_232 = 0x5C6A8F6B6309575DLL;
    int64_t l_482 = (-10L);
    int32_t l_506 = 1L;
    int32_t l_587 = 0L;
    float l_592 = (-0x1.Bp+1);
    int32_t *l_605 = &g_111;
    int32_t **l_604 = &l_605;
    float l_685 = (-0x6.0p-1);
    uint16_t **l_742[4];
    int8_t l_863 = 0x8BL;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_178[i] = (void*)0;
    for (i = 0; i < 4; i++)
        l_742[i] = &l_175;
    l_141 = p_84;
    return g_10[2];
}


/* ------------------------------------------ */
/* 
 * reads : g_114 g_116 g_117 g_22 g_104 g_10 g_110 g_97
 * writes: g_114 g_117 g_25
 */
static int64_t  func_85(int32_t  p_86, int32_t * p_87)
{ /* block id: 22 */
    int32_t *l_113 = &g_114;
    int32_t l_115 = 1L;
    int16_t *l_123 = (void*)0;
    int16_t *l_124 = &g_25[3][1];
    int16_t l_138 = 6L;
    (*l_113) ^= 0L;
    l_115 &= (*l_113);
    (*g_116) &= (g_114 > p_86);
    (*l_113) = (((void*)0 == &l_113) > (safe_sub_func_int8_t_s_s((g_22 || (0x09L <= (safe_add_func_int16_t_s_s(((*l_124) = (+g_104)), ((safe_mul_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(((g_10[2] >= (safe_unary_minus_func_int16_t_s(0xF68EL))) & ((safe_add_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((safe_add_func_int64_t_s_s((g_110 & g_104), g_97[1])), 13)), 0xE8ACL)) != (*l_113))), (*l_113))), 0x04L)) == (*l_113)))))), l_138)));
    return (*l_113);
}


/* ------------------------------------------ */
/* 
 * reads : g_98 g_99 g_10 g_111
 * writes: g_99 g_104 g_110 g_111
 */
static int32_t  func_88(int32_t  p_89, const int32_t ** p_90, int32_t * p_91, float  p_92, float  p_93)
{ /* block id: 14 */
    int32_t l_100 = (-6L);
    int8_t *l_103[5][6] = {{&g_104,&g_104,&g_104,&g_104,&g_104,&g_104},{(void*)0,&g_104,&g_104,&g_104,(void*)0,(void*)0},{&g_104,&g_104,&g_104,&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104,&g_104,&g_104,&g_104},{(void*)0,(void*)0,&g_104,&g_104,&g_104,(void*)0}};
    int32_t l_107 = 4L;
    int32_t l_108 = (-1L);
    uint32_t *l_109 = &g_110;
    int i, j;
    (*p_90) = (*g_98);
    g_111 |= ((l_100 , ((((*l_109) = ((safe_rshift_func_int8_t_s_s((((p_89 , 0L) || (((l_100 = (g_104 = 0xCDL)) & (safe_add_func_int32_t_s_s((p_89 > (l_103[4][1] == (void*)0)), p_89))) < (((l_107 && g_10[1]) & l_107) == l_107))) , l_108), p_89)) >= p_89)) ^ l_108) & p_89)) | l_108);
    return p_89;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_10[i], "g_10[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_22, "g_22", print_hash_value);
    transparent_crc(g_24, "g_24", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_25[i][j], "g_25[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_26[i][j], "g_26[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_27[i], "g_27[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_53, sizeof(g_53), "g_53", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_80[i][j][k], sizeof(g_80[i][j][k]), "g_80[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_97[i], "g_97[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_165, "g_165", print_hash_value);
    transparent_crc(g_177, "g_177", print_hash_value);
    transparent_crc_bytes (&g_215, sizeof(g_215), "g_215", print_hash_value);
    transparent_crc_bytes (&g_256, sizeof(g_256), "g_256", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_258[i][j][k], "g_258[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_294[i][j][k], "g_294[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_326, "g_326", print_hash_value);
    transparent_crc(g_371, "g_371", print_hash_value);
    transparent_crc(g_389, "g_389", print_hash_value);
    transparent_crc(g_406, "g_406", print_hash_value);
    transparent_crc(g_411, "g_411", print_hash_value);
    transparent_crc(g_412, "g_412", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_446[i], "g_446[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_507, sizeof(g_507), "g_507", print_hash_value);
    transparent_crc(g_508, "g_508", print_hash_value);
    transparent_crc(g_552, "g_552", print_hash_value);
    transparent_crc(g_608, "g_608", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc_bytes(&g_648[i][j], sizeof(g_648[i][j]), "g_648[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_649, sizeof(g_649), "g_649", print_hash_value);
    transparent_crc(g_651, "g_651", print_hash_value);
    transparent_crc(g_720, "g_720", print_hash_value);
    transparent_crc(g_818, "g_818", print_hash_value);
    transparent_crc_bytes (&g_880, sizeof(g_880), "g_880", print_hash_value);
    transparent_crc(g_980, "g_980", print_hash_value);
    transparent_crc(g_1193, "g_1193", print_hash_value);
    transparent_crc(g_1216, "g_1216", print_hash_value);
    transparent_crc(g_1328, "g_1328", print_hash_value);
    transparent_crc(g_1382, "g_1382", print_hash_value);
    transparent_crc(g_1444, "g_1444", print_hash_value);
    transparent_crc_bytes (&g_1501, sizeof(g_1501), "g_1501", print_hash_value);
    transparent_crc(g_1502, "g_1502", print_hash_value);
    transparent_crc(g_1503, "g_1503", print_hash_value);
    transparent_crc(g_1504, "g_1504", print_hash_value);
    transparent_crc(g_1511, "g_1511", print_hash_value);
    transparent_crc(g_1630, "g_1630", print_hash_value);
    transparent_crc(g_1657, "g_1657", print_hash_value);
    transparent_crc(g_1690, "g_1690", print_hash_value);
    transparent_crc(g_1768, "g_1768", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1790[i][j][k], "g_1790[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1791, "g_1791", print_hash_value);
    transparent_crc(g_1792, "g_1792", print_hash_value);
    transparent_crc(g_1796, "g_1796", print_hash_value);
    transparent_crc(g_1966, "g_1966", print_hash_value);
    transparent_crc(g_2040, "g_2040", print_hash_value);
    transparent_crc(g_2041, "g_2041", print_hash_value);
    transparent_crc(g_2069, "g_2069", print_hash_value);
    transparent_crc(g_2072, "g_2072", print_hash_value);
    transparent_crc(g_2164, "g_2164", print_hash_value);
    transparent_crc(g_2165, "g_2165", print_hash_value);
    transparent_crc(g_2175, "g_2175", print_hash_value);
    transparent_crc(g_2182, "g_2182", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2268[i], "g_2268[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2295, "g_2295", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2308[i], "g_2308[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2350[i][j][k], "g_2350[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2448, "g_2448", print_hash_value);
    transparent_crc(g_2466, "g_2466", print_hash_value);
    transparent_crc(g_2624, "g_2624", print_hash_value);
    transparent_crc(g_2631, "g_2631", print_hash_value);
    transparent_crc(g_2726, "g_2726", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2838[i][j][k], "g_2838[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2847, "g_2847", print_hash_value);
    transparent_crc(g_3019, "g_3019", print_hash_value);
    transparent_crc(g_3286, "g_3286", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 791
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 85
   depth: 2, occurrence: 19
   depth: 6, occurrence: 1
   depth: 14, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 21, occurrence: 1
   depth: 23, occurrence: 1
   depth: 27, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 33, occurrence: 1
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 687

XXX times a variable address is taken: 1657
XXX times a pointer is dereferenced on RHS: 484
breakdown:
   depth: 1, occurrence: 402
   depth: 2, occurrence: 71
   depth: 3, occurrence: 10
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 452
breakdown:
   depth: 1, occurrence: 413
   depth: 2, occurrence: 27
   depth: 3, occurrence: 12
XXX times a pointer is compared with null: 70
XXX times a pointer is compared with address of another variable: 23
XXX times a pointer is compared with another pointer: 25
XXX times a pointer is qualified to be dereferenced: 7909

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1734
   level: 2, occurrence: 402
   level: 3, occurrence: 197
   level: 4, occurrence: 71
   level: 5, occurrence: 13
XXX number of pointers point to pointers: 325
XXX number of pointers point to scalars: 362
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.1
XXX average alias set size: 1.4

XXX times a non-volatile is read: 2743
XXX times a non-volatile is write: 1330
XXX times a volatile is read: 190
XXX    times read thru a pointer: 58
XXX times a volatile is write: 40
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 1.57e+03
XXX percentage of non-volatile access: 94.7

XXX forward jumps: 1
XXX backward jumps: 8

XXX stmts: 81
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 9
   depth: 2, occurrence: 6
   depth: 3, occurrence: 13
   depth: 4, occurrence: 9
   depth: 5, occurrence: 13

XXX percentage a fresh-made variable is used: 16.8
XXX percentage an existing variable is used: 83.2
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3058824753
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_4 = 0x59E57AD9L;
static uint32_t g_23 = 0xA5202749L;
static float g_27 = 0x1.3p-1;
static float g_28 = 0x1.5p+1;
static uint8_t g_31 = 255UL;
static int32_t *g_33 = (void*)0;
static int32_t ** volatile g_32[7][6][4] = {{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33}}};
static int64_t g_43[6][4][3] = {{{0L,(-1L),0x7B3FA8726ABE926FLL},{0xCFCCFF5F9F7E48FFLL,(-1L),0x51EF224B34D7AEB8LL},{0x12BBF3AF71E1D77FLL,9L,0x8ECEA67F41B6ADD7LL},{3L,0x3FA0B5BCDCEC458ALL,0x8ECEA67F41B6ADD7LL}},{{(-1L),0L,0x51EF224B34D7AEB8LL},{0x7B3FA8726ABE926FLL,(-1L),0x7B3FA8726ABE926FLL},{0x3A8A1D584D75F053LL,(-1L),(-8L)},{9L,0x3A8A1D584D75F053LL,(-1L)}},{{3L,(-1L),0x11AFD208DD2DC097LL},{0x9878FF5F6F594A39LL,0xB20E9505C00D4B3BLL,0x1B70F82EDDAAD19DLL},{3L,(-1L),1L},{9L,0x9878FF5F6F594A39LL,0x51EF224B34D7AEB8LL}},{{0x3A8A1D584D75F053LL,0xCFCCFF5F9F7E48FFLL,(-1L)},{0x7B3FA8726ABE926FLL,(-1L),0x3FA0B5BCDCEC458ALL},{(-1L),(-1L),(-1L)},{3L,(-1L),3L}},{{0x12BBF3AF71E1D77FLL,(-1L),0x1B70F82EDDAAD19DLL},{0xCFCCFF5F9F7E48FFLL,0xCFCCFF5F9F7E48FFLL,0x8ECEA67F41B6ADD7LL},{0L,0x9878FF5F6F594A39LL,0x11AFD208DD2DC097LL},{(-1L),(-1L),(-1L)}},{{0x7B3FA8726ABE926FLL,0xB20E9505C00D4B3BLL,3L},{9L,(-1L),(-1L)},{0xCFCCFF5F9F7E48FFLL,0x3A8A1D584D75F053LL,0x11AFD208DD2DC097LL},{0x51EF224B34D7AEB8LL,0x7B3FA8726ABE926FLL,3L}}};
static float g_75[4][7] = {{(-0x1.Cp-1),(-0x1.1p+1),0x3.F785D6p-75,0x3.F785D6p-75,(-0x1.1p+1),(-0x1.Cp-1),(-0x2.Ep-1)},{(-0x1.1p+1),0xB.02009Ap+78,0x1.7F0BA7p+36,0xB.C53C27p-1,0xB.C53C27p-1,0x1.7F0BA7p+36,0xB.02009Ap+78},{(-0x1.1p+1),(-0x2.Ep-1),(-0x1.Cp-1),(-0x1.1p+1),0x3.F785D6p-75,0x3.F785D6p-75,(-0x1.1p+1)},{(-0x1.Cp-1),0xB.02009Ap+78,(-0x1.Cp-1),0x3.F785D6p-75,0xB.02009Ap+78,(-0x2.Ep-1),(-0x2.Ep-1)}};
static int64_t g_76 = 0xF55BD23A2E2E988ALL;
static int32_t g_95 = 8L;
static int32_t * volatile g_94 = &g_95;/* VOLATILE GLOBAL g_94 */
static uint8_t g_132 = 249UL;
static uint8_t *g_131 = &g_132;
static int32_t g_137 = 0xE6C69FB0L;
static int16_t g_141 = 0x892BL;
static float * volatile g_147[5][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
static int64_t g_160 = 7L;
static uint8_t g_226 = 8UL;
static volatile int32_t g_248[2][8] = {{0L,0x447648BDL,0L,3L,3L,0L,0x447648BDL,0L},{0x18BE8490L,3L,(-3L),3L,0x18BE8490L,0x18BE8490L,3L,(-3L)}};
static float g_251 = 0x5.A6EA2Fp+76;
static uint64_t g_252 = 0x3E8FABF89D0C8DE3LL;
static int32_t g_255 = 0x54527A59L;
static uint8_t **g_265 = &g_131;
static uint8_t *** volatile g_264 = &g_265;/* VOLATILE GLOBAL g_264 */
static float * volatile g_268 = (void*)0;/* VOLATILE GLOBAL g_268 */
static float * volatile g_272 = &g_27;/* VOLATILE GLOBAL g_272 */
static int32_t * volatile g_276 = (void*)0;/* VOLATILE GLOBAL g_276 */
static int32_t * volatile g_277 = &g_137;/* VOLATILE GLOBAL g_277 */
static int32_t * volatile g_319[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t * volatile g_320[4] = {&g_137,&g_137,&g_137,&g_137};
static int32_t * volatile g_321[8][8][4] = {{{&g_137,&g_137,&g_4,&g_95},{(void*)0,&g_137,(void*)0,&g_137},{&g_255,&g_255,&g_255,&g_255},{&g_255,(void*)0,&g_95,&g_95},{&g_4,&g_4,(void*)0,&g_137},{&g_255,&g_95,&g_137,&g_137},{(void*)0,&g_4,&g_137,&g_95},{&g_137,(void*)0,(void*)0,&g_255}},{{&g_95,&g_255,&g_137,&g_137},{(void*)0,&g_137,&g_137,&g_95},{(void*)0,&g_137,&g_4,(void*)0},{&g_137,(void*)0,(void*)0,&g_255},{&g_95,&g_137,(void*)0,(void*)0},{&g_255,(void*)0,&g_255,&g_95},{&g_95,&g_137,(void*)0,&g_4},{&g_4,&g_137,&g_95,&g_255}},{{(void*)0,&g_4,&g_4,&g_137},{(void*)0,&g_95,&g_137,&g_95},{&g_137,&g_95,(void*)0,(void*)0},{&g_137,(void*)0,&g_137,(void*)0},{&g_137,&g_255,&g_137,&g_137},{(void*)0,&g_4,&g_95,&g_137},{&g_4,(void*)0,&g_4,&g_137},{&g_4,&g_137,&g_255,&g_95}},{{&g_137,&g_255,&g_95,&g_137},{(void*)0,&g_4,&g_95,&g_4},{&g_137,(void*)0,&g_137,&g_4},{&g_255,&g_137,&g_255,&g_95},{&g_4,(void*)0,&g_4,(void*)0},{&g_4,&g_137,&g_255,&g_255},{&g_255,(void*)0,&g_137,&g_137},{&g_137,&g_137,&g_95,(void*)0}},{{&g_255,&g_255,&g_255,&g_95},{&g_255,&g_255,&g_4,&g_255},{&g_137,&g_255,&g_95,(void*)0},{&g_95,&g_95,&g_95,&g_137},{(void*)0,(void*)0,&g_255,&g_137},{&g_255,&g_4,&g_255,(void*)0},{&g_137,&g_137,(void*)0,&g_255},{&g_137,&g_255,&g_4,&g_4}},{{&g_255,&g_255,&g_4,(void*)0},{&g_95,(void*)0,&g_255,&g_95},{&g_255,&g_255,(void*)0,(void*)0},{&g_95,&g_137,&g_95,&g_255},{&g_137,&g_95,&g_4,&g_95},{&g_255,&g_255,(void*)0,(void*)0},{&g_255,&g_255,&g_95,&g_95},{&g_95,&g_4,&g_137,&g_255}},{{&g_4,&g_137,&g_4,&g_4},{&g_95,&g_95,&g_95,&g_255},{&g_137,(void*)0,&g_137,&g_255},{&g_4,&g_95,&g_255,&g_137},{&g_137,&g_95,&g_137,&g_255},{&g_95,(void*)0,(void*)0,&g_255},{&g_95,&g_95,&g_4,&g_4},{&g_255,&g_137,&g_137,&g_255}},{{&g_255,&g_4,&g_255,&g_95},{&g_137,&g_255,(void*)0,(void*)0},{(void*)0,&g_255,&g_4,&g_95},{&g_95,&g_95,&g_137,&g_255},{&g_4,&g_137,&g_255,(void*)0},{(void*)0,&g_255,&g_137,&g_95},{&g_137,(void*)0,&g_255,(void*)0},{&g_4,&g_255,&g_255,&g_4}}};
static int32_t * volatile g_322 = &g_255;/* VOLATILE GLOBAL g_322 */
static volatile int64_t g_327 = 0xDF9C6E8BCC63CCAELL;/* VOLATILE GLOBAL g_327 */
static uint32_t *g_348 = &g_23;
static uint32_t **g_347 = &g_348;
static volatile uint32_t g_363 = 4294967295UL;/* VOLATILE GLOBAL g_363 */
static volatile uint32_t * volatile g_362[4] = {&g_363,&g_363,&g_363,&g_363};
static volatile uint32_t * volatile *g_361[7][3][9] = {{{&g_362[2],&g_362[2],&g_362[0],&g_362[0],&g_362[2],&g_362[2],&g_362[2],&g_362[0],&g_362[0]},{&g_362[3],&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[3],&g_362[3],&g_362[0]},{(void*)0,&g_362[2],(void*)0,&g_362[2],&g_362[2],(void*)0,&g_362[2],(void*)0,&g_362[2]}},{{&g_362[3],&g_362[3],&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[3],&g_362[3]},{&g_362[0],&g_362[2],&g_362[0],&g_362[2],&g_362[0],&g_362[0],&g_362[2],&g_362[0],&g_362[2]},{&g_362[3],&g_362[0],&g_362[0],&g_362[0],&g_362[0],&g_362[3],&g_362[0],&g_362[0],&g_362[0]}},{{&g_362[0],&g_362[0],&g_362[2],&g_362[0],(void*)0,&g_362[2],&g_362[2],(void*)0,&g_362[2]},{&g_362[0],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[0],&g_362[3]},{&g_362[0],(void*)0,(void*)0,&g_362[0],&g_362[0],&g_362[0],(void*)0,(void*)0,&g_362[0]}},{{&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[3],&g_362[3],&g_362[0],&g_362[3]},{(void*)0,&g_362[0],&g_362[2],&g_362[2],&g_362[0],(void*)0,&g_362[0],&g_362[2],&g_362[2]},{&g_362[3],&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[3],&g_362[3],&g_362[0]}},{{&g_362[0],&g_362[0],&g_362[0],(void*)0,(void*)0,&g_362[0],&g_362[0],&g_362[0],(void*)0},{&g_362[0],&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[0]},{&g_362[2],(void*)0,&g_362[2],(void*)0,&g_362[2],&g_362[2],(void*)0,&g_362[2],(void*)0}},{{&g_362[3],&g_362[3],&g_362[0],&g_362[0],&g_362[3],&g_362[3],&g_362[3],&g_362[0],&g_362[0]},{&g_362[2],&g_362[2],(void*)0,&g_362[2],(void*)0,&g_362[2],&g_362[2],(void*)0,&g_362[2]},{&g_362[0],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[0],&g_362[3]}},{{&g_362[0],(void*)0,(void*)0,&g_362[0],&g_362[0],&g_362[0],(void*)0,(void*)0,&g_362[0]},{&g_362[3],&g_362[3],&g_362[0],&g_362[3],&g_362[3],&g_362[3],&g_362[3],&g_362[0],&g_362[3]},{(void*)0,&g_362[0],&g_362[2],&g_362[2],&g_362[0],(void*)0,&g_362[0],&g_362[2],&g_362[2]}}};
static volatile uint32_t * volatile **g_360[2][8][5] = {{{&g_361[3][2][8],&g_361[1][1][1],&g_361[1][1][1],&g_361[3][2][8],&g_361[1][1][1]},{&g_361[2][2][7],&g_361[1][1][1],&g_361[1][1][1],(void*)0,&g_361[1][1][1]},{(void*)0,&g_361[1][1][1],&g_361[6][2][1],&g_361[4][1][0],&g_361[1][1][1]},{(void*)0,&g_361[2][2][7],(void*)0,(void*)0,&g_361[2][2][7]},{(void*)0,&g_361[1][1][1],&g_361[4][2][4],&g_361[3][2][8],&g_361[2][2][7]},{&g_361[1][1][1],&g_361[4][0][2],&g_361[1][1][1],&g_361[5][0][6],&g_361[1][1][1]},{&g_361[1][1][1],&g_361[5][0][6],&g_361[3][1][0],&g_361[1][1][1],&g_361[1][1][1]},{&g_361[1][1][1],&g_361[4][2][4],&g_361[1][1][1],&g_361[3][1][0],&g_361[1][1][1]}},{{(void*)0,&g_361[4][2][4],&g_361[4][1][0],&g_361[1][1][1],&g_361[4][0][2]},{(void*)0,&g_361[5][0][6],&g_361[1][1][1],&g_361[1][1][1],(void*)0},{(void*)0,&g_361[4][0][2],&g_361[4][1][0],&g_361[4][0][2],(void*)0},{&g_361[2][2][7],&g_361[1][1][1],&g_361[1][1][1],&g_361[4][0][2],&g_361[1][1][1]},{(void*)0,&g_361[1][1][1],&g_361[2][2][7],&g_361[4][1][0],&g_361[1][1][1]},{&g_361[1][1][1],&g_361[1][1][1],&g_361[4][1][0],&g_361[3][2][8],&g_361[4][1][0]},{&g_361[4][1][0],&g_361[4][1][0],&g_361[5][0][6],&g_361[2][2][7],&g_361[1][1][1]},{&g_361[4][1][0],&g_361[6][2][1],&g_361[1][1][1],(void*)0,&g_361[3][1][0]}}};
static int32_t * volatile g_404 = &g_255;/* VOLATILE GLOBAL g_404 */
static float * volatile g_438 = &g_75[1][6];/* VOLATILE GLOBAL g_438 */
static float * volatile g_519[6] = {&g_28,&g_28,&g_251,&g_28,&g_28,&g_251};
static int32_t g_552 = 0xA27B80A1L;
static int64_t g_556 = 0x603639A3677B4BBDLL;
static uint8_t g_557 = 0x5AL;
static volatile float g_586 = 0x1.7p-1;/* VOLATILE GLOBAL g_586 */
static float * volatile g_593 = &g_75[2][2];/* VOLATILE GLOBAL g_593 */
static uint32_t g_655 = 0xA17F7195L;
static uint32_t ***g_711 = (void*)0;
static int32_t * volatile g_712 = &g_95;/* VOLATILE GLOBAL g_712 */
static float * volatile g_719 = &g_27;/* VOLATILE GLOBAL g_719 */
static int32_t g_738 = (-1L);
static float * volatile g_782[5][9] = {{&g_75[3][3],(void*)0,&g_75[0][2],&g_75[2][6],&g_75[1][6],&g_251,&g_75[3][5],&g_27,&g_28},{&g_28,&g_75[3][3],&g_75[1][6],&g_27,&g_27,&g_75[1][6],&g_75[3][3],&g_28,(void*)0},{&g_75[0][3],&g_28,&g_28,&g_27,(void*)0,(void*)0,&g_27,(void*)0,&g_251},{(void*)0,&g_75[1][6],&g_251,&g_75[2][6],&g_75[0][3],&g_75[2][6],&g_251,&g_75[1][6],(void*)0},{&g_75[3][5],&g_75[2][6],(void*)0,&g_75[1][6],&g_75[0][3],&g_27,&g_28,&g_75[1][6],&g_28}};
static float * const  volatile g_809 = &g_251;/* VOLATILE GLOBAL g_809 */
static int32_t g_835 = 0x0BCBA515L;
static int8_t g_849[10][5] = {{1L,0xB9L,6L,(-1L),0x62L},{5L,0xA7L,0xA7L,5L,0L},{0xA1L,4L,0xA7L,0x04L,0xD7L},{0x04L,0x9AL,6L,1L,0xA7L},{0xB9L,0xA1L,0x04L,0x04L,0xA1L},{1L,(-1L),0x6DL,5L,0xA1L},{0x9AL,0x62L,4L,(-1L),0xA7L},{(-1L),(-1L),(-8L),0xD7L,0xD7L},{0x9AL,0x6DL,0x9AL,(-8L),0L},{1L,0x6DL,1L,(-1L),0x62L}};
static int16_t g_857 = 0x96F8L;
static uint8_t * const *g_865 = &g_131;
static uint8_t * const **g_864 = &g_865;
static uint8_t * const ***g_863 = &g_864;
static uint32_t *g_888 = &g_655;
static int32_t ** volatile g_898 = &g_33;/* VOLATILE GLOBAL g_898 */
static int32_t ** volatile g_904 = &g_33;/* VOLATILE GLOBAL g_904 */
static uint8_t ***g_920 = &g_265;
static uint8_t ****g_919 = &g_920;
static uint8_t *****g_918 = &g_919;
static int8_t g_940[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
static uint16_t g_974 = 0x0A71L;
static int8_t *g_1000 = &g_940[3];
static int8_t **g_999[9] = {(void*)0,&g_1000,(void*)0,(void*)0,&g_1000,(void*)0,(void*)0,&g_1000,(void*)0};
static int16_t g_1027 = 0x807DL;
static int32_t * volatile g_1042 = (void*)0;/* VOLATILE GLOBAL g_1042 */
static int32_t * volatile g_1043 = (void*)0;/* VOLATILE GLOBAL g_1043 */
static volatile int64_t *g_1046 = &g_327;
static volatile int64_t * const * const  volatile g_1045 = &g_1046;/* VOLATILE GLOBAL g_1045 */
static uint16_t g_1085 = 0x19B1L;
static int32_t * volatile g_1086 = &g_137;/* VOLATILE GLOBAL g_1086 */
static int16_t * const g_1107 = &g_1027;
static int16_t * const *g_1106 = &g_1107;
static int16_t * const **g_1105 = &g_1106;
static int32_t ** volatile g_1114 = &g_33;/* VOLATILE GLOBAL g_1114 */
static int16_t *g_1200 = &g_141;
static int16_t **g_1199 = &g_1200;
static int32_t ** volatile g_1207 = &g_33;/* VOLATILE GLOBAL g_1207 */
static uint64_t * volatile *g_1228 = (void*)0;
static int32_t ** volatile g_1280[4][9][6] = {{{&g_33,&g_33,&g_33,(void*)0,&g_33,&g_33},{(void*)0,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,(void*)0,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,(void*)0,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,(void*)0},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,(void*)0,(void*)0,&g_33,&g_33}},{{&g_33,&g_33,&g_33,&g_33,&g_33,(void*)0},{(void*)0,&g_33,&g_33,&g_33,&g_33,&g_33},{(void*)0,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,(void*)0,&g_33,&g_33,&g_33},{&g_33,(void*)0,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,(void*)0,&g_33,&g_33},{(void*)0,&g_33,(void*)0,&g_33,&g_33,(void*)0}},{{&g_33,&g_33,&g_33,&g_33,(void*)0,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,(void*)0,&g_33,&g_33,(void*)0,&g_33},{&g_33,&g_33,&g_33,(void*)0,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,(void*)0,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,(void*)0,(void*)0,&g_33,(void*)0,&g_33}},{{&g_33,(void*)0,&g_33,&g_33,&g_33,&g_33},{(void*)0,&g_33,(void*)0,(void*)0,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33,&g_33,&g_33},{(void*)0,&g_33,&g_33,&g_33,&g_33,&g_33},{(void*)0,&g_33,&g_33,&g_33,&g_33,&g_33}}};
static int32_t ** volatile g_1281 = &g_33;/* VOLATILE GLOBAL g_1281 */
static int32_t g_1363 = 1L;
static int32_t ** volatile g_1381 = &g_33;/* VOLATILE GLOBAL g_1381 */
static int32_t * volatile g_1405[8][4] = {{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95}};
static int32_t g_1417 = (-3L);
static int32_t g_1447 = 1L;
static int32_t ** volatile g_1513 = (void*)0;/* VOLATILE GLOBAL g_1513 */
static volatile int16_t g_1569 = (-5L);/* VOLATILE GLOBAL g_1569 */
static uint64_t * const g_1572 = &g_252;
static uint64_t * const *g_1571 = &g_1572;
static float * volatile g_1577 = &g_28;/* VOLATILE GLOBAL g_1577 */
static volatile uint64_t g_1597 = 0x216EA86145526955LL;/* VOLATILE GLOBAL g_1597 */
static const int32_t g_1627 = (-6L);
static const int32_t *g_1626 = &g_1627;
static int8_t g_1696 = (-2L);
static uint32_t ****g_1725 = &g_711;
static const volatile uint32_t *g_1759 = (void*)0;
static const volatile uint32_t **g_1758 = &g_1759;
static const volatile uint32_t ** volatile * volatile g_1757 = &g_1758;/* VOLATILE GLOBAL g_1757 */
static const volatile uint32_t ** volatile * volatile * volatile g_1760 = &g_1757;/* VOLATILE GLOBAL g_1760 */
static uint32_t * const *g_1768 = (void*)0;
static uint32_t * const **g_1767 = &g_1768;
static volatile uint16_t g_1770 = 1UL;/* VOLATILE GLOBAL g_1770 */
static uint32_t **g_1772 = &g_888;
static uint32_t ***g_1771 = &g_1772;
static uint64_t g_1789 = 0xCCB5D82824D7F7D4LL;
static uint64_t g_1793 = 0UL;
static volatile float *g_1815[2] = {&g_586,&g_586};
static volatile float ** volatile g_1814 = &g_1815[1];/* VOLATILE GLOBAL g_1814 */
static volatile float ** volatile *g_1813 = &g_1814;
static float g_1860[3][7][4] = {{{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1}},{{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1}},{{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1},{0x5.7p+1,0x5.7p+1,0x5.7p+1,0x5.7p+1}}};
static int32_t ** const  volatile g_1920 = &g_33;/* VOLATILE GLOBAL g_1920 */
static int32_t * volatile g_1936[2][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_738,&g_738,&g_738,&g_738,&g_738,&g_738,&g_738}};
static int32_t * volatile g_1937 = &g_1417;/* VOLATILE GLOBAL g_1937 */
static int32_t ** volatile g_1941 = &g_33;/* VOLATILE GLOBAL g_1941 */
static int32_t * volatile g_1960 = &g_255;/* VOLATILE GLOBAL g_1960 */
static uint32_t **g_1987[6] = {&g_888,&g_888,&g_888,&g_888,&g_888,&g_888};
static uint32_t **g_1988 = &g_888;
static uint32_t **g_1989[8] = {&g_888,&g_888,&g_888,&g_888,&g_888,&g_888,&g_888,&g_888};
static int32_t **g_2023 = (void*)0;
static int32_t *** volatile g_2022[8][8][4] = {{{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,(void*)0},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,(void*)0,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023}},{{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,(void*)0},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,(void*)0,(void*)0,&g_2023}},{{&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,(void*)0,&g_2023},{&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,(void*)0,(void*)0,(void*)0},{&g_2023,&g_2023,&g_2023,&g_2023}},{{(void*)0,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0},{(void*)0,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,(void*)0,&g_2023},{&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023}},{{&g_2023,&g_2023,(void*)0,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,(void*)0,(void*)0,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0},{(void*)0,&g_2023,&g_2023,&g_2023}},{{&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0},{&g_2023,(void*)0,(void*)0,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,(void*)0,&g_2023}},{{&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,(void*)0,(void*)0,(void*)0},{&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0}},{{(void*)0,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,(void*)0,&g_2023},{&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,(void*)0,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023}}};
static int32_t ** volatile g_2080 = &g_33;/* VOLATILE GLOBAL g_2080 */
static const uint32_t g_2121 = 0xB43C673DL;
static int32_t * volatile g_2122 = (void*)0;/* VOLATILE GLOBAL g_2122 */
static int32_t ** const  volatile g_2182 = &g_33;/* VOLATILE GLOBAL g_2182 */
static int32_t ** volatile g_2183 = &g_33;/* VOLATILE GLOBAL g_2183 */
static uint16_t g_2187 = 0xBA2CL;
static uint16_t g_2237 = 0x56E4L;
static const int32_t ** const  volatile g_2352 = &g_1626;/* VOLATILE GLOBAL g_2352 */
static int32_t ** volatile g_2472 = (void*)0;/* VOLATILE GLOBAL g_2472 */
static int32_t *g_2474[9] = {&g_255,&g_95,&g_255,&g_255,&g_95,&g_255,&g_1447,&g_255,&g_1447};
static int32_t ** volatile g_2473 = &g_2474[2];/* VOLATILE GLOBAL g_2473 */
static const int32_t *g_2496 = &g_1363;
static int64_t g_2626 = 0xCF879DB0CF9A7C70LL;
static int32_t * volatile g_2627[5] = {&g_255,&g_255,&g_255,&g_255,&g_255};
static int32_t g_2659 = 0xA256F408L;
static int32_t g_2703 = 0x9D784B3FL;
static volatile uint8_t g_2744 = 0xC8L;/* VOLATILE GLOBAL g_2744 */
static const uint8_t *g_2751[4] = {&g_557,&g_557,&g_557,&g_557};
static const uint8_t **g_2750 = &g_2751[1];
static const uint8_t *** const  volatile g_2749 = &g_2750;/* VOLATILE GLOBAL g_2749 */
static volatile int32_t g_2767[4] = {1L,1L,1L,1L};
static volatile int32_t g_2768 = 0xFDFCA8BCL;/* VOLATILE GLOBAL g_2768 */
static volatile int32_t *g_2766[6] = {&g_2767[3],&g_2767[3],&g_2767[3],&g_2767[3],&g_2767[3],&g_2767[3]};
static volatile int32_t * volatile *g_2765 = &g_2766[3];
static volatile int32_t * volatile * volatile *g_2764 = &g_2765;
static volatile int32_t * volatile * volatile **g_2763[1][3] = {{&g_2764,&g_2764,&g_2764}};
static int32_t ** volatile g_2793 = &g_2474[2];/* VOLATILE GLOBAL g_2793 */
static volatile uint64_t g_2844[2][7] = {{0xDB698DEAF84879CALL,2UL,0xDB698DEAF84879CALL,0UL,18446744073709551606UL,18446744073709551606UL,0UL},{0xDB698DEAF84879CALL,2UL,0xDB698DEAF84879CALL,0UL,18446744073709551606UL,18446744073709551606UL,0UL}};
static const int32_t ***g_2900 = (void*)0;
static const int32_t **g_2903 = (void*)0;
static const int32_t ***g_2902 = &g_2903;
static const int32_t **** volatile g_2901 = &g_2902;/* VOLATILE GLOBAL g_2901 */
static const int64_t g_2920 = 8L;
static const int64_t *g_2919 = &g_2920;
static const int64_t **g_2918 = &g_2919;
static int32_t *g_3058 = &g_2659;
static int32_t **g_3057 = &g_3058;
static volatile int8_t g_3066 = 0x89L;/* VOLATILE GLOBAL g_3066 */
static uint16_t g_3104 = 0x8CE5L;
static int32_t ***g_3138 = &g_3057;
static int32_t ***g_3139[2][3][3] = {{{&g_3057,&g_3057,&g_3057},{&g_3057,(void*)0,&g_3057},{(void*)0,&g_3057,&g_3057}},{{&g_3057,&g_3057,&g_3057},{&g_3057,(void*)0,&g_3057},{&g_3057,&g_3057,&g_3057}}};
static volatile int16_t g_3293 = 0L;/* VOLATILE GLOBAL g_3293 */
static const uint64_t * volatile g_3320 = &g_1793;/* VOLATILE GLOBAL g_3320 */
static const uint64_t * volatile * const g_3319 = &g_3320;
static const uint64_t * volatile * const *g_3318 = &g_3319;
static const uint64_t * volatile * const ** volatile g_3317 = &g_3318;/* VOLATILE GLOBAL g_3317 */
static int32_t ** volatile g_3340 = (void*)0;/* VOLATILE GLOBAL g_3340 */
static int32_t ** volatile g_3341 = &g_2474[2];/* VOLATILE GLOBAL g_3341 */
static volatile uint32_t g_3365 = 1UL;/* VOLATILE GLOBAL g_3365 */
static float *g_3482[1] = {&g_1860[1][6][3]};
static float **g_3481 = &g_3482[0];
static float ***g_3480[9][5][2] = {{{&g_3481,&g_3481},{(void*)0,(void*)0},{&g_3481,&g_3481},{(void*)0,&g_3481},{&g_3481,&g_3481}},{{(void*)0,&g_3481},{(void*)0,(void*)0},{&g_3481,(void*)0},{&g_3481,(void*)0},{&g_3481,&g_3481}},{{(void*)0,&g_3481},{&g_3481,(void*)0},{&g_3481,&g_3481},{&g_3481,(void*)0},{&g_3481,&g_3481}},{{(void*)0,&g_3481},{&g_3481,(void*)0},{(void*)0,&g_3481},{(void*)0,(void*)0},{&g_3481,&g_3481}},{{(void*)0,&g_3481},{&g_3481,(void*)0},{&g_3481,&g_3481},{&g_3481,(void*)0},{&g_3481,&g_3481}},{{(void*)0,&g_3481},{&g_3481,(void*)0},{&g_3481,(void*)0},{&g_3481,(void*)0},{(void*)0,&g_3481}},{{(void*)0,&g_3481},{&g_3481,&g_3481},{(void*)0,&g_3481},{&g_3481,(void*)0},{(void*)0,&g_3481}},{{&g_3481,&g_3481},{(void*)0,(void*)0},{&g_3481,&g_3481},{(void*)0,&g_3481},{&g_3481,&g_3481}},{{(void*)0,&g_3481},{(void*)0,(void*)0},{&g_3481,(void*)0},{&g_3481,(void*)0},{&g_3481,&g_3481}}};
static uint16_t g_3556 = 0x8322L;
static volatile float g_3560 = 0x2.669FE4p+18;/* VOLATILE GLOBAL g_3560 */
static volatile int16_t * volatile g_3687[8] = {&g_3293,&g_3293,&g_3293,&g_3293,&g_3293,&g_3293,&g_3293,&g_3293};
static volatile int16_t * volatile * volatile g_3686 = &g_3687[7];/* VOLATILE GLOBAL g_3686 */
static volatile int16_t g_3690[9] = {1L,0x1091L,1L,1L,0x1091L,1L,1L,0x1091L,1L};
static volatile int16_t *g_3689 = &g_3690[3];
static volatile int16_t **g_3688[8][9][3] = {{{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{(void*)0,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,(void*)0},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,(void*)0}},{{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,(void*)0},{&g_3689,&g_3689,(void*)0},{(void*)0,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689}},{{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,(void*)0},{&g_3689,&g_3689,(void*)0},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,(void*)0},{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,(void*)0},{&g_3689,&g_3689,&g_3689}},{{&g_3689,&g_3689,&g_3689},{(void*)0,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,(void*)0},{(void*)0,&g_3689,&g_3689},{(void*)0,&g_3689,&g_3689},{(void*)0,&g_3689,(void*)0},{&g_3689,&g_3689,&g_3689}},{{&g_3689,(void*)0,(void*)0},{&g_3689,&g_3689,(void*)0},{(void*)0,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,&g_3689}},{{&g_3689,&g_3689,(void*)0},{&g_3689,&g_3689,(void*)0},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,(void*)0},{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,(void*)0},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689}},{{(void*)0,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,(void*)0},{(void*)0,&g_3689,&g_3689},{(void*)0,&g_3689,&g_3689},{(void*)0,&g_3689,(void*)0},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,(void*)0}},{{&g_3689,&g_3689,(void*)0},{(void*)0,(void*)0,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,&g_3689,&g_3689},{&g_3689,(void*)0,&g_3689},{&g_3689,&g_3689,(void*)0}}};
static volatile int64_t g_3695[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static int8_t *g_3740 = &g_940[8];
static int8_t ** const g_3739 = &g_3740;
static int8_t ** const *g_3738 = &g_3739;
static float *g_3776 = &g_75[0][5];
static float ** const g_3775 = &g_3776;
static float ** const *g_3774 = &g_3775;
static int16_t g_3785 = (-10L);
static uint8_t g_3786 = 0UL;
static uint16_t *g_3809 = &g_974;
static uint16_t * volatile * const  volatile g_3808 = &g_3809;/* VOLATILE GLOBAL g_3808 */
static volatile int32_t g_3897 = (-1L);/* VOLATILE GLOBAL g_3897 */
static const volatile uint32_t g_3935 = 8UL;/* VOLATILE GLOBAL g_3935 */
static uint32_t g_3944 = 0xC4E8AC93L;
static const int32_t **** const g_3978 = &g_2900;
static const int32_t **** const *g_3977[3] = {&g_3978,&g_3978,&g_3978};
static volatile uint32_t g_3993[1][5] = {{0x18D859A8L,0x18D859A8L,0x18D859A8L,0x18D859A8L,0x18D859A8L}};


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static float  func_10(uint16_t  p_11, uint8_t  p_12);
static uint64_t  func_15(const uint16_t  p_16, int16_t  p_17, int32_t * p_18, uint32_t  p_19, const int16_t  p_20);
static uint64_t  func_47(uint32_t * p_48, uint16_t  p_49, uint64_t  p_50);
static uint16_t  func_59(int32_t * p_60, float * p_61, float * const  p_62);
static float  func_68(int64_t  p_69, int32_t * p_70, float * const  p_71, uint32_t * p_72);
static float  func_81(uint64_t  p_82, float * p_83);
static uint8_t  func_102(int64_t * p_103, uint8_t * p_104);
static int64_t * func_105(uint32_t  p_106, uint32_t * const  p_107, float * p_108);
static uint64_t  func_115(uint32_t * p_116, int16_t  p_117, uint8_t * p_118);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_2919 g_2920
 * writes:
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    uint16_t l_2 = 0UL;
    int32_t *l_3[5][4][2];
    uint64_t l_5 = 18446744073709551615UL;
    int32_t l_3909 = 0xE70B76EFL;
    int16_t *l_3916 = (void*)0;
    const int32_t l_3926 = 0L;
    uint8_t l_3938[2][7][1] = {{{249UL},{0xCEL},{249UL},{0xE7L},{0UL},{0xE7L},{249UL}},{{0xCEL},{249UL},{0xE7L},{0UL},{0xE7L},{249UL},{0xCEL}}};
    float * const l_3951[8] = {&g_251,&g_251,(void*)0,&g_251,&g_251,(void*)0,&g_251,&g_251};
    uint8_t **** const *l_3956 = &g_919;
    const uint8_t l_3981[8][3] = {{0x43L,0x7AL,0x43L},{1UL,0x62L,1UL},{0x43L,0x7AL,0x43L},{1UL,0x62L,1UL},{0x43L,0x7AL,0x43L},{1UL,0x62L,1UL},{0x43L,0x7AL,0x43L},{1UL,0x62L,1UL}};
    int32_t *** const l_3987 = (void*)0;
    int32_t *** const *l_3986 = &l_3987;
    uint32_t *****l_3988[1];
    uint32_t l_3996 = 0x089F0365L;
    int32_t *l_3999 = (void*)0;
    int32_t *l_4000 = &g_95;
    int i, j, k;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 2; k++)
                l_3[i][j][k] = &g_4;
        }
    }
    for (i = 0; i < 1; i++)
        l_3988[i] = (void*)0;
    l_5 = l_2;
    for (l_5 = 0; (l_5 == 3); l_5 = safe_add_func_int64_t_s_s(l_5, 2))
    { /* block id: 4 */
        return g_4;
    }
    for (l_5 = 4; (l_5 != 37); l_5++)
    { /* block id: 9 */
        int32_t *l_21 = &g_4;
        uint32_t *l_22 = &g_23;
        float *l_26 = &g_27;
        uint8_t *l_29 = (void*)0;
        uint8_t *l_30[6] = {&g_31,&g_31,&g_31,&g_31,&g_31,&g_31};
        int32_t l_3855[1];
        int16_t l_3908 = (-9L);
        uint64_t l_3952 = 0x3C234B93B56894B0LL;
        uint8_t l_3953 = 246UL;
        uint8_t ***l_3959 = &g_265;
        uint8_t ***l_3960 = &g_265;
        uint8_t ***l_3961 = &g_265;
        uint8_t ***l_3962 = &g_265;
        uint8_t ***l_3963 = &g_265;
        uint8_t ***l_3964[9][1] = {{&g_265},{&g_265},{&g_265},{&g_265},{&g_265},{&g_265},{&g_265},{&g_265},{&g_265}};
        uint8_t ***l_3965[6] = {&g_265,(void*)0,&g_265,&g_265,(void*)0,&g_265};
        uint8_t ***l_3966 = &g_265;
        uint8_t ***l_3967[7][7] = {{&g_265,&g_265,&g_265,&g_265,&g_265,&g_265,&g_265},{(void*)0,&g_265,&g_265,(void*)0,&g_265,(void*)0,(void*)0},{&g_265,&g_265,&g_265,&g_265,&g_265,&g_265,&g_265},{&g_265,(void*)0,&g_265,&g_265,(void*)0,&g_265,(void*)0},{&g_265,&g_265,&g_265,&g_265,&g_265,&g_265,&g_265},{&g_265,&g_265,(void*)0,(void*)0,(void*)0,(void*)0,&g_265},{&g_265,&g_265,&g_265,&g_265,&g_265,&g_265,&g_265}};
        uint8_t ***l_3968 = &g_265;
        uint8_t ***l_3969 = &g_265;
        uint8_t ***l_3970 = &g_265;
        uint8_t ***l_3971 = &g_265;
        uint8_t ***l_3972 = &g_265;
        uint8_t ***l_3973[1];
        uint8_t **** const l_3958[6][6] = {{&l_3962,&l_3972,&l_3961,&l_3972,&l_3962,&l_3961},{&l_3972,&l_3962,&l_3961,&l_3968,&l_3968,&l_3961},{&l_3968,&l_3968,&l_3961,&l_3962,&l_3972,&l_3961},{&l_3962,&l_3972,&l_3961,&l_3972,&l_3962,&l_3961},{&l_3972,&l_3962,&l_3961,&l_3968,&l_3968,&l_3961},{&l_3968,&l_3968,&l_3961,&l_3962,&l_3972,&l_3961}};
        uint8_t **** const *l_3957[3];
        const int32_t **** const *l_3979 = (void*)0;
        uint32_t l_3980[6][2] = {{0x8F996630L,0x8F996630L},{0x8F996630L,0x8F996630L},{0x8F996630L,0x8F996630L},{0x8F996630L,0x8F996630L},{0x8F996630L,0x8F996630L},{0x8F996630L,0x8F996630L}};
        uint32_t ****l_3990 = (void*)0;
        uint32_t *****l_3989 = &l_3990;
        int i, j;
        for (i = 0; i < 1; i++)
            l_3855[i] = 0x38093101L;
        for (i = 0; i < 1; i++)
            l_3973[i] = &g_265;
        for (i = 0; i < 3; i++)
            l_3957[i] = &l_3958[4][0];
    }
    l_4000 = l_3999;
    return (*g_2919);
}


/* ------------------------------------------ */
/* 
 * reads : g_1577 g_28 g_3776 g_3808 g_3809 g_974 g_1572 g_252 g_1199 g_1200 g_1772 g_1771 g_888 g_1105 g_1106 g_1107 g_1027 g_3740 g_940 g_141 g_2919 g_2920 g_265 g_131 g_132 g_31 g_3897 g_918 g_919 g_920 g_3738 g_3739 g_2765 g_2766
 * writes: g_75 g_141 g_888 g_1027 g_31 g_2768 g_2767
 */
static float  func_10(uint16_t  p_11, uint8_t  p_12)
{ /* block id: 1737 */
    uint16_t ** const l_3856[3][8] = {{(void*)0,(void*)0,&g_3809,(void*)0,(void*)0,&g_3809,(void*)0,(void*)0},{(void*)0,(void*)0,&g_3809,&g_3809,(void*)0,&g_3809,&g_3809,(void*)0},{(void*)0,&g_3809,&g_3809,(void*)0,&g_3809,&g_3809,(void*)0,&g_3809}};
    int64_t l_3862 = 1L;
    int32_t l_3863 = 0x2E1773B3L;
    uint32_t * const l_3866 = &g_655;
    uint32_t *l_3867 = &g_655;
    int32_t l_3868 = 0L;
    int32_t l_3869 = (-4L);
    uint32_t *** const **l_3875 = (void*)0;
    const uint32_t *l_3878 = &g_2121;
    const uint32_t **l_3877 = &l_3878;
    uint64_t *l_3891 = &g_1793;
    uint64_t **l_3890 = &l_3891;
    uint64_t ***l_3889 = &l_3890;
    int16_t l_3892 = 0xA46FL;
    int i, j;
    (*g_3776) = (((((l_3856[0][7] == l_3856[0][7]) < ((!p_11) != ((0x2.2p-1 > (-((((((safe_rshift_func_uint8_t_u_s(p_12, p_12)) , ((~p_11) , 0xA.768A5Dp+26)) >= p_11) == p_11) >= 0x1.9p-1) > l_3862))) >= (*g_1577)))) <= 0x0.9p-1) < p_12) < 0x6.745B24p-30);
    l_3869 &= (l_3868 ^= ((((l_3863 ^= 0x8AL) < (safe_mod_func_uint16_t_u_u((**g_3808), ((**g_1199) = ((l_3862 != (*g_1572)) & 0xDCDEL))))) , l_3866) != (l_3867 = ((*g_1772) = (void*)0))));
    l_3868 = (safe_unary_minus_func_int64_t_s((((p_12 == (l_3863 |= ((((safe_lshift_func_int8_t_s_s((safe_sub_func_int64_t_s_s((l_3875 != (void*)0), ((!0xF05AD928L) | l_3862))), 0)) != (l_3867 != ((*l_3877) = (**g_1771)))) & (safe_div_func_int16_t_s_s((((safe_sub_func_int16_t_s_s(((((((((***g_1105) |= ((safe_rshift_func_int16_t_s_s((l_3862 <= (safe_rshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u(((l_3889 != &l_3890) != 0x36DCL), p_12)), l_3862))), 5)) , l_3892)) | (*g_3809)) , l_3869) & (*g_3740)) < l_3868) > 4294967286UL) , l_3862), (*g_1200))) != 0x6D831B410B76761CLL) || l_3869), l_3862))) ^ l_3892))) < (*g_2919)) == (**g_265))));
    for (g_31 = 0; (g_31 > 26); g_31 = safe_add_func_uint8_t_u_u(g_31, 6))
    { /* block id: 1751 */
        float l_3906 = (-0x1.4p+1);
        int32_t l_3907 = 0x8620445DL;
        (**g_2765) = (safe_add_func_int32_t_s_s(g_3897, (safe_div_func_uint64_t_u_u((p_11 | ((0x97577F879489ADAALL < ((void*)0 != &l_3889)) | ((((safe_add_func_int64_t_s_s(((**g_918) != (void*)0), (safe_mod_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u(((((void*)0 != &g_2763[0][2]) < (***g_3738)) | p_12), l_3863)), (*g_3809))))) & 4294967289UL) <= p_11) < p_11))), l_3907))));
    }
    return l_3892;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_1571 g_1572 g_252 g_1200 g_141 g_919 g_920 g_265 g_131 g_132 g_95 g_347 g_348 g_23 g_1000 g_940 g_1199 g_809 g_918 g_1813 g_1814 g_1815 g_1045 g_1046 g_327 g_974 g_1105 g_1106 g_1107 g_1027 g_43 g_2182 g_2183 g_1417 g_2187 g_1207 g_865 g_849 g_2237 g_1086 g_137 g_863 g_864 g_2121 g_248 g_226 g_1447 g_322 g_255 g_160 g_655 g_264 g_1114 g_33 g_1725 g_2352 g_1988 g_75 g_835 g_557 g_251 g_1789 g_1941 g_2473 g_711 g_438 g_76 g_1627 g_857 g_31 g_1937 g_556 g_94 g_2626 g_2659 g_2703 g_1920 g_1381 g_898 g_1577 g_28 g_2763 g_1085 g_2765 g_2766 g_586 g_2793 g_1757 g_1758 g_1771 g_1772 g_1767 g_1768 g_2901 g_1960 g_2749 g_2750 g_2751 g_2919 g_2920 g_2768 g_2767 g_2918 g_1696 g_3066 g_1793 g_2902 g_2903 g_2844 g_3293 g_3317 g_1626 g_27 g_3341 g_3689 g_3690 g_3809 g_738
 * writes: g_4 g_857 g_132 g_141 g_226 g_251 g_586 g_940 g_974 g_33 g_1417 g_2187 g_95 g_23 g_1027 g_1696 g_43 g_1447 g_255 g_655 g_160 g_711 g_1626 g_888 g_738 g_2237 g_2474 g_347 g_2496 g_782 g_1860 g_75 g_137 g_556 g_1085 g_252 g_1793 g_76 g_1363 g_2766 g_1789 g_2900 g_2902 g_2918 g_2768 g_2767 g_1772 g_3057 g_348 g_920 g_3104 g_2903
 */
static uint64_t  func_15(const uint16_t  p_16, int16_t  p_17, int32_t * p_18, uint32_t  p_19, const int16_t  p_20)
{ /* block id: 13 */
    uint32_t l_38 = 18446744073709551615UL;
    int32_t l_2032 = 0xE0B65CB3L;
    int32_t l_2039 = 0x8F32FDE1L;
    int32_t l_2041 = (-8L);
    int32_t l_2048 = 0L;
    int32_t l_2049 = 0L;
    int32_t l_2051 = 0x43631E19L;
    int32_t l_2053 = 1L;
    int32_t l_2054 = (-2L);
    int32_t l_2056 = 0x617092FFL;
    int32_t ***l_2178 = (void*)0;
    uint16_t l_2258 = 0UL;
    int32_t l_2321 = 1L;
    int32_t l_2322[8];
    const int32_t *l_2351 = (void*)0;
    uint32_t *****l_2363[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint64_t *l_2386 = &g_1793;
    uint64_t **l_2385 = &l_2386;
    int64_t l_2462 = 0x8836697AC779E09FLL;
    uint8_t ** const *l_2513 = (void*)0;
    uint8_t ** const **l_2512 = &l_2513;
    uint8_t ** const ***l_2511 = &l_2512;
    int64_t l_2551 = 0x9ADF28D8758FBD49LL;
    uint64_t l_2572 = 1UL;
    int16_t l_2577 = 0x1242L;
    int64_t *l_2614 = &g_43[4][0][1];
    int64_t **l_2613 = &l_2614;
    float *l_2694 = &g_75[3][6];
    float **l_2693 = &l_2694;
    float ***l_2692 = &l_2693;
    int16_t * const *l_2831 = &g_1200;
    uint32_t l_2835[2][3] = {{9UL,0x6E66CEE5L,9UL},{9UL,0x6E66CEE5L,9UL}};
    uint8_t l_2886[7] = {0xACL,0xACL,0xACL,0xACL,0xACL,0xACL,0xACL};
    uint16_t l_2889[1][7] = {{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL}};
    uint32_t **l_2898[8] = {&g_348,&g_348,&g_348,&g_348,&g_348,&g_348,&g_348,&g_348};
    uint32_t l_2911 = 0UL;
    uint64_t l_2959 = 0x03E02DD1EA363274LL;
    uint8_t l_2974 = 252UL;
    float l_2986 = 0x2.B5C3BCp-26;
    uint32_t **l_2989 = &g_888;
    uint64_t l_3002 = 0x7955C6F4A7C675C6LL;
    uint32_t l_3224 = 1UL;
    uint8_t l_3255 = 0x91L;
    uint64_t ***l_3381 = &l_2385;
    uint64_t ****l_3380[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint64_t *****l_3379 = &l_3380[1];
    uint64_t l_3433[8][6][2] = {{{0x98E373513ADB4865LL,18446744073709551610UL},{0x2052CAF1EEA7D88CLL,0x98E373513ADB4865LL},{0x88C68E1A81F529D8LL,0UL},{0x88C68E1A81F529D8LL,0x98E373513ADB4865LL},{0x2052CAF1EEA7D88CLL,18446744073709551610UL},{0x98E373513ADB4865LL,0UL}},{{1UL,18446744073709551614UL},{1UL,0xA6E7A28F9B5428A5LL},{0xA6E7A28F9B5428A5LL,18446744073709551608UL},{1UL,18446744073709551615UL},{0x85A1C21C27E5A151LL,0x88C68E1A81F529D8LL},{0xF7ED9FA4C7E19484LL,0xAFB92E59CDE7314ELL}},{{18446744073709551614UL,1UL},{18446744073709551608UL,0xCDDD5FA7CB6FF250LL},{0xAFB92E59CDE7314ELL,0xCDDD5FA7CB6FF250LL},{18446744073709551608UL,1UL},{18446744073709551614UL,0xAFB92E59CDE7314ELL},{0xF7ED9FA4C7E19484LL,0x88C68E1A81F529D8LL}},{{0x85A1C21C27E5A151LL,18446744073709551615UL},{1UL,18446744073709551608UL},{0xA6E7A28F9B5428A5LL,0xA6E7A28F9B5428A5LL},{1UL,18446744073709551614UL},{1UL,0UL},{0x98E373513ADB4865LL,18446744073709551610UL}},{{0x2052CAF1EEA7D88CLL,0x98E373513ADB4865LL},{0x88C68E1A81F529D8LL,0UL},{0x88C68E1A81F529D8LL,0x98E373513ADB4865LL},{0x2052CAF1EEA7D88CLL,18446744073709551610UL},{0x98E373513ADB4865LL,0UL},{1UL,18446744073709551614UL}},{{1UL,0xA6E7A28F9B5428A5LL},{0xA6E7A28F9B5428A5LL,18446744073709551608UL},{1UL,18446744073709551615UL},{0x85A1C21C27E5A151LL,0x88C68E1A81F529D8LL},{0xF7ED9FA4C7E19484LL,0xAFB92E59CDE7314ELL},{18446744073709551614UL,1UL}},{{18446744073709551608UL,0xCDDD5FA7CB6FF250LL},{0xAFB92E59CDE7314ELL,0xCDDD5FA7CB6FF250LL},{18446744073709551608UL,1UL},{18446744073709551614UL,0xAFB92E59CDE7314ELL},{0xF7ED9FA4C7E19484LL,0x88C68E1A81F529D8LL},{0x85A1C21C27E5A151LL,18446744073709551615UL}},{{1UL,18446744073709551608UL},{0xA6E7A28F9B5428A5LL,0xA6E7A28F9B5428A5LL},{1UL,18446744073709551614UL},{1UL,0UL},{0x98E373513ADB4865LL,18446744073709551610UL},{0x2052CAF1EEA7D88CLL,0x98E373513ADB4865LL}}};
    const int16_t **l_3455 = (void*)0;
    uint8_t l_3490[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    uint16_t l_3502 = 0xD0D9L;
    float l_3626[5];
    float ***l_3633[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t *****l_3648 = &g_1725;
    uint32_t l_3651 = 18446744073709551608UL;
    uint32_t l_3744 = 0x9CD954FCL;
    int32_t l_3784[7] = {(-1L),(-1L),0L,(-1L),(-1L),0L,(-1L)};
    int8_t *l_3817 = &g_940[3];
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_2322[i] = 1L;
    for (i = 0; i < 5; i++)
        l_3626[i] = 0x1.5p-1;
    if ((*p_18))
    { /* block id: 14 */
        uint32_t l_41[7][2] = {{0x5E3970C0L,0UL},{0x5E3970C0L,0x5E3970C0L},{0UL,0x5E3970C0L},{0x5E3970C0L,0UL},{0x5E3970C0L,0x5E3970C0L},{0UL,0x5E3970C0L},{0x5E3970C0L,0x6FBC79A4L}};
        int32_t *l_51 = (void*)0;
        int32_t l_2019 = 0x823F749FL;
        int32_t l_2038[5][1][4];
        int8_t l_2143 = (-2L);
        uint32_t l_2180[8][8][4] = {{{0UL,0x5ED8C95FL,4294967295UL,1UL},{0x10159FC8L,0UL,0x32BFF614L,4294967288UL},{0x8F2DC3F2L,1UL,4294967288UL,0x76405BA7L},{8UL,9UL,0x699311DFL,0x405A429EL},{0xC6D6D273L,0x7AD20B73L,3UL,4294967295UL},{1UL,0x8F2DC3F2L,1UL,4294967295UL},{0x2C6D3FC9L,0x70C58C61L,0x1497D112L,0x21089FC9L},{4294967295UL,8UL,4294967295UL,0x06BAACADL}},{{0x11684F62L,0x3A8A002EL,2UL,0UL},{0x9A4F9705L,4294967295UL,5UL,0x3A8A002EL},{0x279E9490L,4294967295UL,5UL,0x699311DFL},{0x9A4F9705L,4294967290UL,2UL,0x25E86CD1L},{0x11684F62L,0UL,4294967295UL,1UL},{4294967295UL,1UL,0x1497D112L,2UL},{0x2C6D3FC9L,0UL,1UL,0xB785A8A2L},{1UL,0x2A8C7724L,3UL,0x64357ABCL}},{{0xC6D6D273L,0x5AFF450AL,0x699311DFL,0xDC513A41L},{8UL,0x5EC79DE5L,4294967288UL,1UL},{0x8F2DC3F2L,3UL,0x32BFF614L,4294967295UL},{0x10159FC8L,1UL,4294967295UL,1UL},{0UL,4294967295UL,0UL,0x29C7E725L},{4294967295UL,0x1012F7A8L,0x3A494EFEL,0x0DAB3317L},{0x17FABF1FL,0x322CA619L,1UL,0UL},{0x346647A8L,5UL,4294967295UL,0UL}},{{0x0037EAE1L,0xCAF22537L,2UL,1UL},{0UL,0x8F7BE227L,0x7C3CB9D3L,1UL},{4294967295UL,0x6DCAA265L,0x1012F7A8L,0x11684F62L},{0x32BFF614L,0UL,4294967295UL,0x875687A5L},{5UL,0x279E9490L,0x64357ABCL,0x32BFF614L},{0x44AD6765L,3UL,0x346647A8L,0x8F7BE227L},{1UL,4294967294UL,1UL,1UL},{0x2044E5F5L,0xF972EF30L,0x17FABF1FL,0xC6D6D273L}},{{3UL,0x21089FC9L,0x10159FC8L,0x5AFF450AL},{0x25E86CD1L,1UL,1UL,1UL},{4294967295UL,4294967295UL,0x6E7FB2DBL,0x1497D112L},{0xDC513A41L,0x17FABF1FL,0UL,4294967292UL},{0x549730AFL,1UL,0xF69E9B9EL,0UL},{9UL,1UL,4294967294UL,4294967292UL},{1UL,0x17FABF1FL,0UL,0x1497D112L},{0x875687A5L,4294967295UL,4294967287UL,1UL}},{{0xC93C72FFL,1UL,4294967294UL,0x5AFF450AL},{4294967288UL,0x21089FC9L,4294967295UL,0xC6D6D273L},{0x1012F7A8L,0xF972EF30L,4294967289UL,1UL},{4294967286UL,4294967294UL,0x21089FC9L,0x8F7BE227L},{1UL,3UL,0x2C6D3FC9L,0x32BFF614L},{1UL,0x279E9490L,1UL,0x875687A5L},{4294967292UL,0UL,1UL,0x11684F62L},{0x3EB170D8L,0x6DCAA265L,0xF69E9B9EL,0x1094D660L}},{{0x3A8A002EL,4294967288UL,4294967295UL,4294967295UL},{0x6E7FB2DBL,0x0DAB3317L,5UL,0UL},{0x44AD6765L,1UL,0x5ED8C95FL,0x6DCAA265L},{4294967295UL,4294967292UL,1UL,0xB785A8A2L},{0x76405BA7L,2UL,1UL,0x3A8A002EL},{0x32BFF614L,0x322CA619L,4294967295UL,0x64357ABCL},{0x21089FC9L,0xF972EF30L,1UL,4294967288UL},{0x17FABF1FL,0x25E86CD1L,0x3EB170D8L,4294967289UL}},{{0x7AD20B73L,4294967295UL,0x1094D660L,0x3A494EFEL},{5UL,4294967295UL,4294967295UL,0x2A8C7724L},{1UL,5UL,0UL,2UL},{0x875687A5L,1UL,0x17FABF1FL,4294967286UL},{0xCAF22537L,0x76405BA7L,0x405A429EL,0x76405BA7L},{4294967295UL,0x6DCAA265L,1UL,0UL},{4294967295UL,0xB84EC6A1L,0x0DAB3317L,0xDC513A41L},{4294967295UL,3UL,0x7AD20B73L,0xC6D6D273L}}};
        int8_t l_2186[9] = {9L,9L,9L,9L,9L,9L,9L,9L,9L};
        int32_t l_2256 = 0L;
        uint64_t l_2324 = 0xE39566D86F12B3CDLL;
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 4; k++)
                    l_2038[i][j][k] = (-1L);
            }
        }
lbl_2113:
        p_18 = p_18;
        if (g_4)
            goto lbl_2083;
lbl_2309:
        for (p_17 = 0; (p_17 <= 3); p_17 += 1)
        { /* block id: 18 */
            int32_t *l_34 = (void*)0;
            int32_t *l_35 = (void*)0;
            int32_t *l_36 = &g_4;
            int32_t *l_37[9][10] = {{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4}};
            int64_t l_2018 = 0x01C2AE07900661D2LL;
            int i, j;
            l_38++;
        }
lbl_2083:
        (*p_18) = (safe_add_func_uint8_t_u_u((((**g_1571) < 0xE352544EE796224ALL) && (*p_18)), (l_41[4][0] , l_2054)));
        for (g_4 = 0; (g_4 >= 29); g_4 = safe_add_func_int32_t_s_s(g_4, 1))
        { /* block id: 960 */
            const uint32_t l_2106 = 0x5CECF825L;
            int32_t l_2112[1];
            uint8_t *** const l_2114 = (void*)0;
            float l_2185 = (-0x2.Ap+1);
            int16_t **l_2234 = &g_1200;
            int i;
            for (i = 0; i < 1; i++)
                l_2112[i] = 0xE8BC9617L;
            for (g_857 = 0; (g_857 <= 2); g_857 += 1)
            { /* block id: 963 */
                uint64_t l_2086 = 0x6EA79F159E9DC621LL;
                int32_t l_2127[6][6] = {{0x2653F318L,9L,0xF0C6F357L,0xF0C6F357L,9L,0x2653F318L},{0x2653F318L,0xF0C6F357L,0x497F570EL,9L,9L,0x497F570EL},{9L,9L,0x497F570EL,0xF0C6F357L,0x2653F318L,0x2653F318L},{9L,0xF0C6F357L,0xF0C6F357L,9L,0x2653F318L,0x497F570EL},{0x2653F318L,9L,0xF0C6F357L,0xF0C6F357L,9L,0x2653F318L},{0x2653F318L,0xF0C6F357L,0x497F570EL,9L,9L,0x497F570EL}};
                int64_t **l_2146 = (void*)0;
                const int32_t l_2179 = 0x3DAEE2EEL;
                int i, j;
                if ((l_2086 > (*g_1200)))
                { /* block id: 964 */
                    return l_2049;
                }
                else
                { /* block id: 966 */
                    uint32_t l_2111 = 0x6A81A3A0L;
                    int32_t l_2126 = (-1L);
                    int32_t l_2129 = 0x7B4A3B70L;
                    int32_t l_2130 = 0xF6632BE3L;
                    int32_t l_2131 = 0xBFB3928AL;
                    int32_t l_2132 = 0x28DFF64AL;
                    int32_t l_2133 = (-6L);
                    int32_t l_2134 = 9L;
                    int32_t l_2135 = (-2L);
                    uint16_t *l_2161 = (void*)0;
                    uint16_t *l_2162 = (void*)0;
                    uint16_t *l_2163 = (void*)0;
                    uint16_t *l_2164 = (void*)0;
                    uint16_t *l_2165 = &g_974;
                    uint64_t l_2181 = 0xFC0601BAD9EB1176LL;
                    int i, j;
                    l_2041 = ((((**g_1199) = (safe_mul_func_int16_t_s_s(((((*g_131) = (l_2112[0] &= ((safe_mul_func_uint16_t_u_u(((****g_919) || (safe_lshift_func_uint8_t_u_s(((safe_mod_func_int64_t_s_s((p_17 && (safe_lshift_func_int8_t_s_u(((safe_mod_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((~(((((safe_lshift_func_uint16_t_u_u(7UL, g_95)) > (l_2056 ^ 0x90L)) & (l_2106 , (safe_lshift_func_uint16_t_u_s(l_2106, (((safe_div_func_int8_t_s_s(0x5DL, l_2111)) , (**g_347)) , p_17))))) & (*g_131)) || l_2106)), 4294967295UL)), l_2111)), (*p_18))) , (*g_1000)), 4))), 0x4046E41CA980D529LL)) <= 0L), p_16))), p_17)) ^ p_16))) <= p_20) < 0UL), 0xFB7EL))) , (-1L)) & 246UL);
                    for (g_226 = 0; (g_226 <= 2); g_226 += 1)
                    { /* block id: 973 */
                        const uint32_t *l_2120[1];
                        const uint32_t ** const l_2119 = &l_2120[0];
                        const uint32_t ** const *l_2118 = &l_2119;
                        const uint32_t ** const **l_2117 = &l_2118;
                        int32_t *l_2123 = &l_2038[0][0][2];
                        int32_t *l_2124 = &l_2041;
                        int32_t *l_2125[10][9][2] = {{{(void*)0,(void*)0},{&l_2056,&l_2032},{(void*)0,&g_137},{(void*)0,&g_95},{&l_2053,&g_137},{&g_95,&l_2056},{&g_835,&l_2051},{(void*)0,&g_1417},{&g_1447,&l_2112[0]}},{{(void*)0,&l_2041},{(void*)0,&g_137},{(void*)0,&g_95},{&g_1417,&l_2019},{&g_137,&g_835},{&l_2038[4][0][3],&l_2038[4][0][3]},{(void*)0,(void*)0},{&g_137,(void*)0},{&l_2112[0],&g_1417}},{{&g_255,&l_2112[0]},{&l_2032,&l_2053},{&l_2032,&l_2112[0]},{&g_255,&g_1417},{&l_2112[0],(void*)0},{&g_137,(void*)0},{(void*)0,&l_2038[4][0][3]},{&l_2038[4][0][3],&g_835},{&g_137,&l_2019}},{{&g_1417,&g_95},{(void*)0,&g_137},{(void*)0,&l_2041},{(void*)0,&l_2112[0]},{&g_1447,&g_1417},{(void*)0,&l_2051},{&g_835,&l_2056},{&g_95,&g_137},{&l_2053,&g_95}},{{(void*)0,&g_137},{(void*)0,&l_2032},{&l_2056,(void*)0},{(void*)0,&g_1363},{&l_2019,&l_2053},{&g_137,(void*)0},{&l_2053,&g_1417},{&g_835,&g_1417},{&l_2053,(void*)0}},{{&g_137,&l_2053},{&l_2019,&g_1363},{(void*)0,(void*)0},{&l_2056,&l_2032},{(void*)0,&g_137},{(void*)0,&g_95},{&l_2053,&g_137},{&g_95,&l_2056},{&g_835,&l_2051}},{{(void*)0,&g_1417},{&g_1447,&l_2112[0]},{(void*)0,&l_2041},{(void*)0,&g_137},{(void*)0,&g_95},{&g_1417,&l_2019},{&g_137,&g_835},{&l_2038[4][0][3],&l_2038[4][0][3]},{(void*)0,(void*)0}},{{&g_137,(void*)0},{&l_2112[0],&g_1417},{&g_255,&l_2112[0]},{&l_2032,&l_2053},{&l_2032,&l_2112[0]},{&g_255,&g_1417},{&l_2112[0],(void*)0},{&g_137,(void*)0},{(void*)0,&l_2038[4][0][3]}},{{&l_2038[4][0][3],&g_835},{&g_137,&l_2019},{&g_1417,&g_95},{(void*)0,&g_137},{(void*)0,&l_2041},{(void*)0,&l_2112[0]},{&g_1447,&g_1417},{(void*)0,&l_2051},{&g_835,&l_2056}},{{&g_95,&g_137},{&l_2053,&g_95},{(void*)0,&g_137},{(void*)0,&l_2032},{&l_2056,(void*)0},{(void*)0,&g_1363},{&l_2019,&l_2053},{&g_137,(void*)0},{&l_2053,&g_1417}}};
                        int32_t l_2128[9][10][2] = {{{1L,0x7BA1556EL},{0x56F2A087L,1L},{0xAA7E9AD5L,0x1782160FL},{0xA5B65589L,(-3L)},{0xC423CDDDL,0L},{1L,0x838165E9L},{0x796EE1C1L,0x1C4912ACL},{4L,0x1C4912ACL},{0x796EE1C1L,0x838165E9L},{1L,0L}},{{0xC423CDDDL,(-3L)},{0xA5B65589L,0x1782160FL},{0xAA7E9AD5L,1L},{0x56F2A087L,0x7BA1556EL},{0x470CACEDL,0x853BCE19L},{0xC423CDDDL,0xD2ECADEDL},{1L,0xEE826CDDL},{(-3L),0x1C4912ACL},{0xE6AABEE1L,4L},{0x853BCE19L,0xB982E971L}},{{1L,0xD2ECADEDL},{0L,1L},{0xED147D6BL,0x1782160FL},{0x56F2A087L,(-1L)},{5L,0xC423CDDDL},{0x470CACEDL,(-3L)},{0L,1L},{(-1L),0xEE826CDDL},{0x796EE1C1L,4L},{(-1L),(-1L)}},{{0x853BCE19L,0x838165E9L},{1L,1L},{0x7BA1556EL,1L},{0xA5B65589L,0x7BA1556EL},{5L,1L},{5L,0x7BA1556EL},{0xA5B65589L,1L},{0x7BA1556EL,1L},{1L,0x838165E9L},{0x853BCE19L,(-1L)}},{{(-1L),4L},{0x796EE1C1L,0xEE826CDDL},{(-1L),1L},{0L,(-3L)},{0x470CACEDL,0xC423CDDDL},{5L,(-1L)},{0x56F2A087L,0x1782160FL},{0xED147D6BL,1L},{0L,0xD2ECADEDL},{1L,0xB982E971L}},{{0x853BCE19L,4L},{0xE6AABEE1L,0x1C4912ACL},{(-3L),0xEE826CDDL},{1L,0xD2ECADEDL},{0xC423CDDDL,0x853BCE19L},{0x470CACEDL,0x7BA1556EL},{0x56F2A087L,1L},{0xAA7E9AD5L,0x1782160FL},{0xA5B65589L,(-3L)},{0xC423CDDDL,0L}},{{1L,0x838165E9L},{0x796EE1C1L,0x1C4912ACL},{4L,0x1C4912ACL},{0x796EE1C1L,0x838165E9L},{1L,0L},{0xC423CDDDL,(-3L)},{0xA5B65589L,0x1782160FL},{0xAA7E9AD5L,1L},{0x56F2A087L,0x7BA1556EL},{0x470CACEDL,0x853BCE19L}},{{0xC423CDDDL,0xD2ECADEDL},{1L,0xEE826CDDL},{(-3L),0x1C4912ACL},{0xE6AABEE1L,4L},{0x853BCE19L,0xB982E971L},{1L,0xD2ECADEDL},{0L,1L},{0xED147D6BL,0x1782160FL},{0x56F2A087L,(-1L)},{5L,0xC423CDDDL}},{{0x470CACEDL,(-3L)},{0L,1L},{(-1L),0xEE826CDDL},{0x796EE1C1L,4L},{(-1L),(-1L)},{0x853BCE19L,0x838165E9L},{1L,1L},{0x7BA1556EL,1L},{0xA5B65589L,0x7BA1556EL},{5L,1L}}};
                        uint32_t l_2136 = 18446744073709551615UL;
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                            l_2120[i] = &g_2121;
                        if (l_2111)
                            goto lbl_2113;
                        (*l_2123) = ((l_2114 != l_2114) && (safe_lshift_func_int8_t_s_u((((*l_2117) = (void*)0) == (void*)0), (***g_920))));
                        if (l_2112[0])
                            continue;
                        l_2136++;
                    }
                    (***g_1813) = (safe_sub_func_float_f_f((((l_2127[5][4] , l_2112[0]) < l_2032) <= ((safe_sub_func_float_f_f(l_2143, ((*g_809) = (safe_mul_func_float_f_f(l_2112[0], l_2127[3][5]))))) <= ((void*)0 != l_2146))), ((*g_918) != (void*)0)));
                    l_2181 = (safe_mul_func_uint8_t_u_u((((((((((((safe_mod_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(((((*g_1000) &= p_16) != p_20) | (0L <= (**g_1045))), ((**g_1199) = (safe_div_func_int32_t_s_s(0xF2B3A501L, p_20))))), ((**g_265)--))) >= (safe_rshift_func_uint16_t_u_u((--(*l_2165)), 13))) == (***g_1105)) & (safe_mod_func_int64_t_s_s((p_16 ^ ((safe_sub_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_u((((safe_sub_func_uint8_t_u_u((safe_add_func_int16_t_s_s((3L || 0x84316CF6L), (**g_1106))), 0x10L)) <= p_20) ^ p_20), g_43[0][3][2])) <= p_19), 0x44L)) == (-1L))), 7UL))) , (void*)0) == l_2178) == l_2179) <= 0x014FL) , p_16) , 0L) != l_2180[1][5][3]), l_2127[5][4]));
                }
                (*g_2182) = p_18;
                (*g_2183) = &l_2019;
                for (g_1417 = 2; (g_1417 >= 0); g_1417 -= 1)
                { /* block id: 992 */
                    return p_20;
                }
            }
            if (((**g_918) == l_2114))
            { /* block id: 996 */
                int32_t *l_2184[8];
                int32_t *l_2194 = &l_2112[0];
                int64_t *l_2222 = &g_43[0][3][2];
                int64_t **l_2221 = &l_2222;
                uint32_t ***l_2229 = &g_347;
                int16_t **l_2233[7][2] = {{&g_1200,&g_1200},{&g_1200,&g_1200},{&g_1200,&g_1200},{&g_1200,&g_1200},{&g_1200,&g_1200},{&g_1200,&g_1200},{&g_1200,&g_1200}};
                uint32_t l_2236[4][9][5] = {{{0x583D3E0EL,0xF5F32CE3L,0xB69EB052L,0x893E11B6L,4294967295UL},{0x329D72D5L,0xDA247D1CL,0xA870D0B1L,0x000DD47AL,0x8E2AD8C1L},{4UL,0x583D3E0EL,0x893E11B6L,0x893E11B6L,0x583D3E0EL},{0xD6124FCFL,1UL,0x6346030FL,1UL,0x8BC55929L},{0xA7959F43L,0x58E96002L,0xF5F32CE3L,4294967295UL,0xB69EB052L},{1UL,1UL,0x4754564AL,0x0D1C003EL,0x0D1C003EL},{0xA7959F43L,0UL,0xA7959F43L,0UL,0x80145DC0L},{0xD6124FCFL,0x6346030FL,0x000DD47AL,1UL,0UL},{4UL,4294967295UL,4294967295UL,0xF5F32CE3L,4294967288UL}},{{0x329D72D5L,0UL,0x000DD47AL,0UL,0x329D72D5L},{0x583D3E0EL,0x0633B5ACL,0xA7959F43L,0x58E96002L,0xF5F32CE3L},{1UL,0x8BC55929L,0x4754564AL,4294967295UL,0xA870D0B1L},{0x6A331119L,0xA7959F43L,0xF5F32CE3L,0x0633B5ACL,0xF5F32CE3L},{4294967295UL,4294967295UL,0x6346030FL,0x4754564AL,0x329D72D5L},{0xF5F32CE3L,0xB69EB052L,0x893E11B6L,4294967295UL,4294967288UL},{0x000DD47AL,0x11752853L,0xA870D0B1L,1UL,0UL},{0x864092F5L,0xB69EB052L,0xB69EB052L,0x864092F5L,0x80145DC0L},{0x8BC55929L,4294967295UL,0x8E2AD8C1L,0x329D72D5L,0x0D1C003EL}},{{0x893E11B6L,0xA7959F43L,0x0CA31FB8L,0x6A331119L,0xB69EB052L},{0x11752853L,0x8BC55929L,0x329D72D5L,0x329D72D5L,0x8BC55929L},{4294967288UL,0x0633B5ACL,0UL,0x864092F5L,0x583D3E0EL},{0xDA247D1CL,0UL,4294967295UL,1UL,0x8E2AD8C1L},{0x0633B5ACL,4294967295UL,0UL,4294967295UL,4294967295UL},{0xDA247D1CL,0x6346030FL,0xDA247D1CL,0x4754564AL,7UL},{4294967288UL,0UL,0x6A331119L,0x0633B5ACL,0x58E96002L},{0x11752853L,1UL,0x0D1C003EL,4294967295UL,0xD6124FCFL},{0x893E11B6L,0x58E96002L,0x6A331119L,0x58E96002L,0x893E11B6L}},{{0x8BC55929L,1UL,0xDA247D1CL,0UL,4294967295UL},{0x864092F5L,0x583D3E0EL,0UL,0xF5F32CE3L,0x0CA31FB8L},{0x000DD47AL,0xDA247D1CL,4294967295UL,1UL,4294967295UL},{0xF5F32CE3L,0xF5F32CE3L,0UL,0UL,0x893E11B6L},{4294967295UL,0x8E2AD8C1L,0x329D72D5L,0x0D1C003EL,0xD6124FCFL},{0x6A331119L,4UL,0x0CA31FB8L,4294967295UL,0x58E96002L},{1UL,0x8E2AD8C1L,0x8E2AD8C1L,1UL,7UL},{0x583D3E0EL,0xF5F32CE3L,0xB69EB052L,0x893E11B6L,4294967295UL},{0x329D72D5L,0xDA247D1CL,0xA870D0B1L,0x000DD47AL,0x8E2AD8C1L}}};
                int32_t l_2300 = 0xB1F5828BL;
                int8_t l_2301 = 3L;
                int i, j, k;
                for (i = 0; i < 8; i++)
                    l_2184[i] = &l_2038[0][0][2];
                g_2187--;
                l_2019 = (*p_18);
                for (g_95 = 0; (g_95 >= 0); g_95 -= 1)
                { /* block id: 1001 */
                    float l_2192[10] = {0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84,0x4.F3AD51p+84};
                    int32_t l_2193 = 0xC775E319L;
                    int64_t **l_2235 = &l_2222;
                    int32_t l_2254 = 0x9718AAE4L;
                    int64_t l_2257[2][10] = {{0L,(-7L),0L,0L,(-7L),0L,(-7L),0L,0L,(-7L)},{0L,(-7L),0L,0L,(-7L),0L,(-7L),0L,0L,(-7L)}};
                    uint32_t ***l_2305[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int32_t l_2313 = 0xCF89285CL;
                    int32_t l_2318 = 0x6A6466EBL;
                    int32_t l_2320[10][8][3] = {{{0x3AB30C8EL,0xE68F76C6L,(-1L)},{2L,0xCD1235C6L,(-3L)},{0x35AAB0ADL,8L,0x30CC2137L},{0x3FFF13A0L,0xCD1235C6L,(-5L)},{0xF85561B8L,0xE68F76C6L,0L},{(-1L),0x223A13F9L,0x3EB0F610L},{0x6119B086L,(-4L),(-1L)},{(-1L),(-3L),0x223A13F9L}},{{0xF85561B8L,(-1L),6L},{0x3FFF13A0L,5L,5L},{0x35AAB0ADL,(-1L),6L},{2L,3L,0x223A13F9L},{0x3AB30C8EL,0xCE1F16F2L,(-1L)},{0xD7376442L,9L,0x3EB0F610L},{1L,0xCE1F16F2L,0L},{(-4L),3L,(-5L)}},{{0x430A2925L,(-1L),0x30CC2137L},{0xA52EC5A5L,5L,(-3L)},{0x430A2925L,(-1L),(-1L)},{(-4L),(-3L),0xD73707B3L},{1L,(-4L),(-4L)},{0xD7376442L,0x223A13F9L,0xD73707B3L},{0x3AB30C8EL,0xE68F76C6L,(-1L)},{2L,0xCD1235C6L,(-3L)}},{{0x35AAB0ADL,8L,0x30CC2137L},{0x3FFF13A0L,0xCD1235C6L,(-5L)},{0xF85561B8L,0xE68F76C6L,0L},{(-1L),0x223A13F9L,0x3EB0F610L},{0x6119B086L,(-4L),(-1L)},{(-1L),(-3L),0x223A13F9L},{0xF85561B8L,(-1L),6L},{0x3FFF13A0L,5L,5L}},{{0x35AAB0ADL,(-1L),6L},{2L,3L,0x223A13F9L},{0x3AB30C8EL,0xCE1F16F2L,(-1L)},{0xD7376442L,9L,0x3EB0F610L},{1L,0xCE1F16F2L,0L},{(-4L),3L,(-5L)},{0x430A2925L,(-1L),0x30CC2137L},{0xA52EC5A5L,5L,(-3L)}},{{0x430A2925L,(-1L),(-1L)},{(-4L),(-3L),0xD73707B3L},{1L,(-4L),(-4L)},{0xD7376442L,0x223A13F9L,0xD73707B3L},{0x3AB30C8EL,0xE68F76C6L,(-1L)},{2L,0xCD1235C6L,(-3L)},{0x35AAB0ADL,8L,0x30CC2137L},{0x3FFF13A0L,0xCD1235C6L,(-5L)}},{{0xF85561B8L,0xE68F76C6L,0L},{(-1L),0x223A13F9L,0x3EB0F610L},{0x6119B086L,(-4L),(-1L)},{(-1L),(-3L),0x223A13F9L},{0xF85561B8L,(-1L),6L},{5L,0x794E8506L,0x794E8506L},{0xCE1F16F2L,1L,6L},{0x3EB0F610L,0x826489CFL,0L}},{{0x30CC2137L,0x092B7236L,0xFB8BF155L},{0xCD1235C6L,0L,(-1L)},{(-4L),0x092B7236L,0L},{9L,0x826489CFL,0x38E81CB1L},{0L,1L,(-1L)},{0x07A1A099L,0x794E8506L,0L},{0L,0xFB8BF155L,1L},{9L,0L,(-1L)}},{{(-4L),0xC4D5F313L,0xC4D5F313L},{0xCD1235C6L,0L,(-1L)},{0x30CC2137L,0x52E19161L,1L},{0x3EB0F610L,0x66D2FB95L,0L},{0xCE1F16F2L,0x08E6182CL,(-1L)},{5L,0x66D2FB95L,0x38E81CB1L},{8L,0x52E19161L,0L},{(-5L),0L,(-1L)}},{{0xF6C079ABL,0xC4D5F313L,0xFB8BF155L},{(-5L),0L,0L},{8L,0xFB8BF155L,6L},{5L,0x794E8506L,0x794E8506L},{0xCE1F16F2L,1L,6L},{0x3EB0F610L,0x826489CFL,0L},{0x30CC2137L,0x092B7236L,0xFB8BF155L},{0xCD1235C6L,0L,(-1L)}}};
                    int i, j, k;
                    if ((safe_lshift_func_int8_t_s_s(p_19, 6)))
                    { /* block id: 1002 */
                        int i;
                        l_2193 &= l_2112[0];
                        return p_16;
                    }
                    else
                    { /* block id: 1005 */
                        int32_t **l_2195 = &l_2184[2];
                        l_2194 = p_18;
                        (*l_2195) = ((*g_1207) = p_18);
                    }
                    if ((*p_18))
                        continue;
                    if ((safe_rshift_func_uint8_t_u_s((safe_lshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_s((++(**g_865)), ((safe_sub_func_uint16_t_u_u((((((safe_sub_func_int8_t_s_s(((*g_1000) = ((((safe_mod_func_int16_t_s_s((**g_1106), g_849[6][3])) , (~((*g_348) = p_16))) && (safe_rshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u(((safe_mul_func_int16_t_s_s(0L, (((l_2221 != (((safe_lshift_func_uint8_t_u_u(((safe_add_func_int8_t_s_s((((((**g_1199) &= (safe_div_func_int16_t_s_s(((((void*)0 == l_2229) ^ (safe_sub_func_int64_t_s_s((+(0UL && (l_2233[3][0] == l_2234))), l_2112[0]))) > (-1L)), 65528UL))) ^ (**g_1106)) <= (**g_1106)) , l_2193), 0x7EL)) | p_17), 0)) ^ 255UL) , l_2235)) == (*g_1046)) , 1L))) >= 2L), 3)), 253UL)), p_19))) > 1L)), p_20)) , (*g_1000)) != l_2236[3][8][4]) ^ 0xDC627800E5EF0ED2LL) ^ g_2237), 0x62A0L)) != 4294967293UL))) ^ (*p_18)), l_2112[0])), p_17)), (*l_2194))))
                    { /* block id: 1015 */
                        uint64_t **l_2253 = (void*)0;
                        uint64_t ***l_2252 = &l_2253;
                        uint64_t ****l_2251 = &l_2252;
                        int32_t l_2255 = 0xF13C25BEL;
                        l_2254 &= (((((safe_mod_func_uint32_t_u_u((safe_div_func_uint32_t_u_u((0x2CC8F6F3A44097DELL ^ 0x74B316CF73F7FB9CLL), (safe_sub_func_int16_t_s_s((safe_div_func_int8_t_s_s((~(l_2193 = 0x00L)), ((safe_sub_func_uint32_t_u_u((p_19 , (((((void*)0 == &g_1572) <= (safe_add_func_int16_t_s_s(p_19, ((((*l_2251) = (void*)0) != (void*)0) || p_16)))) == 0UL) == 0x9411L)), (*g_1086))) & 1L))), p_16)))), 0x9E4EC8DBL)) <= p_20) | (-1L)) != 0UL) | 0L);
                        if ((*p_18))
                            continue;
                        l_2258--;
                    }
                    else
                    { /* block id: 1021 */
                        uint32_t l_2264 = 0x4CE853F9L;
                        int8_t *l_2281 = &g_1696;
                        int32_t l_2282 = 0x76F051CDL;
                        float *l_2294 = &g_75[1][6];
                        float **l_2293 = &l_2294;
                        uint16_t *l_2302 = (void*)0;
                        uint16_t *l_2303 = &g_974;
                        uint8_t *l_2304 = &g_226;
                        l_2282 |= (!(((safe_mul_func_int16_t_s_s(l_2264, (p_19 , (safe_sub_func_int16_t_s_s(0L, (safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint8_t_u_s(((safe_mod_func_int64_t_s_s(((**l_2235) = ((!((**l_2234) = (*g_1200))) <= ((((*l_2281) = (safe_mul_func_int16_t_s_s(((((safe_mod_func_int16_t_s_s((((*g_1000) = p_19) >= (((**g_1106) = p_20) == (p_17 , ((safe_lshift_func_uint8_t_u_s(((7L ^ (safe_rshift_func_uint8_t_u_s(0UL, p_17))) & (****g_863)), (*l_2194))) ^ l_2106)))), g_2121)) == 1UL) != 2UL) <= p_20), (-1L)))) <= l_2257[0][1]) >= 0xD4C81C72L))), 3UL)) ^ 0x3C1C3E2F1878CFACLL), 6))))))))) && p_19) != 0x4E7BD56AAE49ED9ALL));
                        g_1447 |= (safe_div_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(((safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s(((***g_1105) = ((((*l_2304) &= (safe_add_func_uint16_t_u_u(((*l_2303) = ((*l_2194) < (((void*)0 != l_2293) <= (safe_unary_minus_func_int8_t_s((((safe_sub_func_uint8_t_u_u((65530UL && (p_17 & (l_2301 = (l_2300 = ((((-3L) || (safe_div_func_int8_t_s_s((*g_1000), 1L))) , (((((*g_1200) = 9L) ^ p_16) <= (**g_1571)) || (***g_1105))) & l_2257[1][9]))))), (****g_919))) , l_2264) && 0UL)))))), g_248[0][7]))) , l_2305[8]) != (void*)0)), p_20)), l_2106)) < 255UL), p_17)), 3L));
                        if ((*l_2194))
                            continue;
                    }
                    for (l_2193 = 0; (l_2193 <= 5); l_2193 += 1)
                    { /* block id: 1039 */
                        int32_t **l_2306 = &l_2184[4];
                        int32_t l_2310 = 0xD7B59271L;
                        int32_t l_2311 = 1L;
                        int32_t l_2312 = 0L;
                        int32_t l_2314 = 0xB8E56F12L;
                        int32_t l_2315 = 1L;
                        int32_t l_2316 = 0xD1537A97L;
                        int32_t l_2317 = 0x2E34D353L;
                        int32_t l_2319 = 0xF5379B79L;
                        int32_t l_2323 = 0xA6A2FACDL;
                        int i, j, k;
                        (*l_2306) = p_18;
                        (*g_322) |= (safe_rshift_func_int8_t_s_s(0xC1L, 5));
                        if (p_16)
                            goto lbl_2309;
                        l_2324--;
                    }
                }
            }
            else
            { /* block id: 1046 */
                uint8_t l_2345 = 0x9BL;
                for (g_655 = 0; (g_655 <= 1); g_655 += 1)
                { /* block id: 1049 */
                    int32_t *** const l_2344[8][10] = {{&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,(void*)0,&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,(void*)0,&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,(void*)0,&g_2023,&g_2023,&g_2023,&g_2023},{(void*)0,&g_2023,&g_2023,&g_2023,&g_2023,(void*)0,&g_2023,(void*)0,&g_2023,&g_2023}};
                    int i, j;
                    for (g_160 = 0; (g_160 <= 1); g_160 += 1)
                    { /* block id: 1052 */
                        uint32_t l_2328 = 1UL;
                        uint16_t *l_2337 = &g_2187;
                        int i, j;
                        l_2038[1][0][2] = (l_2256 = (safe_unary_minus_func_int16_t_s((l_41[(g_160 + 2)][g_655] == ((l_2328 != (safe_div_func_uint16_t_u_u(((p_19 &= (safe_mod_func_uint32_t_u_u(((**g_347) = ((safe_mod_func_int8_t_s_s(((*g_1000) = ((safe_div_func_uint16_t_u_u(((*l_2337) ^= g_940[3]), (safe_lshift_func_int8_t_s_s((((7L != (safe_mul_func_uint8_t_u_u((0x35F50D609D7A43A0LL < ((safe_add_func_uint32_t_u_u(0x3CDE079AL, p_16)) != (((void*)0 != l_2344[3][4]) & 5L))), 1UL))) > (***g_264)) ^ g_940[3]), 0)))) != (**g_1106))), 0x3AL)) <= (*****g_918))), l_2345))) ^ 0UL), (**g_1199)))) > g_252)))));
                        if ((*p_18))
                            continue;
                    }
                    return l_41[(g_655 + 4)][g_655];
                }
                for (l_2258 = 0; (l_2258 <= 58); l_2258++)
                { /* block id: 1065 */
                    int32_t **l_2348 = &g_33;
                    for (g_255 = 0; (g_255 <= 0); g_255 += 1)
                    { /* block id: 1068 */
                        int i, j, k;
                        if ((*p_18))
                            break;
                    }
                    (*l_2348) = (*g_1114);
                }
            }
            return (*g_1572);
        }
    }
    else
    { /* block id: 1076 */
        uint32_t ** const *l_2349 = (void*)0;
        uint32_t ***l_2350 = &g_347;
        int32_t l_2353[4];
        int8_t l_2355 = 0x94L;
        int32_t l_2356 = 0xC50344CFL;
        uint8_t l_2357[6][6] = {{0UL,0UL,0x70L,0UL,0UL,0x70L},{0UL,0UL,0x70L,0UL,0UL,0x70L},{0UL,0UL,0x70L,0UL,0UL,0x70L},{0UL,0UL,0x70L,0UL,0UL,0x70L},{0UL,0UL,0x70L,0UL,0UL,0UL},{255UL,255UL,0UL,255UL,255UL,0UL}};
        uint32_t *** const *l_2361 = &g_711;
        uint32_t *** const ** const l_2360 = &l_2361;
        uint32_t *****l_2362 = &g_1725;
        uint32_t *l_2364 = (void*)0;
        uint64_t l_2428 = 18446744073709551615UL;
        uint64_t l_2467 = 0x3C0BACBB77943FBELL;
        uint32_t l_2493[10] = {0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L,0x845A26E1L};
        int32_t l_2494 = 0xE873C77DL;
        uint32_t l_2530 = 0UL;
        uint16_t l_2540 = 1UL;
        int32_t *l_2543 = &l_2494;
        float *l_2544 = &g_75[1][6];
        int32_t l_2550 = 0xFAD96504L;
        int32_t l_2552 = 0L;
        float l_2553[2][4] = {{0x0.Dp+1,0x0.Dp+1,0x0.Dp+1,0x0.Dp+1},{0x0.Dp+1,0x0.Dp+1,0x0.Dp+1,0x0.Dp+1}};
        int16_t l_2555 = 0x91A4L;
        uint16_t l_2556[1];
        int16_t l_2586 = 0x31D2L;
        int32_t ***l_2662 = (void*)0;
        const uint16_t l_2724[9] = {0xC6DEL,0xC6DEL,0xC6DEL,0xC6DEL,0xC6DEL,0xC6DEL,0xC6DEL,0xC6DEL,0xC6DEL};
        uint64_t **l_2777 = (void*)0;
        int8_t **l_2845 = &g_1000;
        int i, j;
        for (i = 0; i < 4; i++)
            l_2353[i] = 0x7D170F1EL;
        for (i = 0; i < 1; i++)
            l_2556[i] = 0x473DL;
lbl_2559:
        if ((l_2349 != ((*g_1725) = l_2350)))
        { /* block id: 1078 */
            int32_t *l_2354[1][1][10];
            int32_t l_2387 = 0x40C8B587L;
            uint32_t **l_2407 = &g_888;
            const int32_t ***l_2436 = (void*)0;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 10; k++)
                        l_2354[i][j][k] = &l_2048;
                }
            }
            (*p_18) = 1L;
            (*g_2352) = l_2351;
            --l_2357[4][3];
            if (((l_2360 == (l_2363[1] = l_2362)) | ((l_2364 = p_18) == ((*g_1988) = ((((0x5E8F94AEL >= ((safe_mul_func_int16_t_s_s((safe_mod_func_int32_t_s_s((((safe_mul_func_int8_t_s_s((l_2353[3] , (((safe_mul_func_int64_t_s_s(p_20, ((safe_mul_func_int8_t_s_s(l_2356, l_2355)) , (((safe_sub_func_uint8_t_u_u(0x6BL, 0UL)) >= p_16) == 18446744073709551614UL)))) & 9L) , p_17)), 255UL)) != p_17) >= p_20), l_2357[4][3])), p_17)) == 4UL)) >= (**g_1571)) & p_16) , (void*)0)))))
            { /* block id: 1085 */
                int32_t l_2388 = 9L;
                float l_2445 = (-0x1.6p+1);
                int32_t l_2447 = (-1L);
                int32_t l_2450 = (-6L);
                int32_t l_2454 = 0L;
                int32_t l_2455 = 0x6FE90140L;
                int32_t l_2456 = 0x5444BFD5L;
                int32_t l_2461 = 0L;
                int32_t l_2463 = 0L;
                int32_t l_2464 = 0x2DD9EDCCL;
                int32_t l_2465 = 0xA5459E9BL;
                int32_t l_2466[7];
                uint32_t * const *l_2480[4];
                int i;
                for (i = 0; i < 7; i++)
                    l_2466[i] = 0xD96228B2L;
                for (i = 0; i < 4; i++)
                    l_2480[i] = &g_348;
                for (g_738 = 0; (g_738 == (-8)); g_738 = safe_sub_func_int64_t_s_s(g_738, 4))
                { /* block id: 1088 */
                    (*p_18) ^= ((safe_div_func_int8_t_s_s((0x5DADAD878331DD33LL <= ((p_16 > ((safe_mod_func_int16_t_s_s((**g_1199), 0x3BCFL)) >= (&g_919 != &g_919))) != (safe_div_func_uint8_t_u_u(((void*)0 == l_2385), ((*g_1000) = l_2387))))), l_2353[1])) || l_2388);
                }
                if ((((safe_mul_func_float_f_f(((((&g_1000 != (void*)0) , (****g_918)) == &l_2357[4][1]) , ((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(p_17, (safe_div_func_float_f_f((((((((safe_sub_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s((***g_1105), 11)) < ((*p_18) ^ ((0x059EL <= (safe_mod_func_uint32_t_u_u((((safe_sub_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u(0x67L, p_19)), p_16)) , &l_2364) != l_2407), l_2388))) , 4294967292UL))), (***g_1105))) ^ 0x9389DE86L) | p_16) && (***g_264)) != p_17) | l_2388) , p_19), p_19)))) != g_75[1][3]), p_20)) != l_2353[1])), g_835)) , p_20) , (*p_18)))
                { /* block id: 1092 */
                    return p_20;
                }
                else
                { /* block id: 1094 */
                    uint16_t *l_2419 = &l_2258;
                    uint16_t *l_2420 = &g_2237;
                    uint8_t * const l_2427 = &g_31;
                    int32_t l_2446 = 1L;
                    int32_t l_2452 = 0x02C3873BL;
                    int32_t l_2457 = 0x5E385DD3L;
                    int32_t l_2458 = (-7L);
                    int32_t l_2459 = 0L;
                    int32_t l_2460[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_2460[i] = 0xA777E9B6L;
                    l_2056 &= (safe_sub_func_uint8_t_u_u((****g_863), ((safe_div_func_int32_t_s_s(((p_16 == (0x25L && (safe_sub_func_int32_t_s_s((!(**g_865)), (safe_div_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(g_141, ((*l_2420) = ((*l_2419) = g_557)))), (safe_rshift_func_uint8_t_u_s(0x7BL, ((safe_div_func_uint32_t_u_u((safe_mod_func_uint32_t_u_u(((***g_919) == l_2427), l_2428)), (*p_18))) <= 7UL))))))))) >= p_20), 0x3F06F65CL)) < p_20)));
                    if ((*p_18))
                    { /* block id: 1098 */
                        float *l_2435 = (void*)0;
                        float * const *l_2434 = &l_2435;
                        float * const ** const l_2433 = &l_2434;
                        const int32_t ****l_2437 = &l_2436;
                        int32_t l_2449 = 0x784B8DE7L;
                        int32_t l_2451 = 0xF5EFEF09L;
                        int32_t l_2453[9] = {3L,3L,1L,3L,3L,1L,3L,3L,1L};
                        int i;
                        l_2447 = ((((((safe_div_func_float_f_f((((*p_18) = (((safe_lshift_func_uint16_t_u_s((((*l_2437) = ((&g_1814 != l_2433) , l_2436)) == &g_2023), 6)) , (safe_mod_func_int32_t_s_s(((((*g_1107) , (safe_div_func_uint64_t_u_u((p_20 >= (((65528UL == (((p_16 != ((((safe_div_func_int64_t_s_s((+l_2388), (*g_1046))) || (*p_18)) < (*p_18)) > 0x86B176C4F27E922CLL)) == 18446744073709551615UL) <= p_20)) == 0xBDE3DF99L) != (***g_1105))), 0x55DEDC63624E54D5LL))) ^ 0x5B00A23398D60BE8LL) > (*p_18)), l_2353[3]))) < l_2446)) , (*g_809)), g_226)) != 0x3.F9513Ep+19) == (-0x4.Ep-1)) >= g_160) == l_2357[0][0]) < g_1789);
                        if (g_1447)
                            goto lbl_2448;
lbl_2448:
                        (*g_1941) = (*g_1941);
                        l_2467--;
                    }
                    else
                    { /* block id: 1105 */
                        int32_t **l_2470 = &l_2354[0][0][4];
                        int32_t **l_2471 = &g_33;
                        int64_t *l_2483 = &g_160;
                        const uint32_t *l_2489 = &l_38;
                        const uint32_t **l_2488 = &l_2489;
                        (*g_2473) = ((*l_2471) = ((*l_2470) = p_18));
                        l_2353[2] &= ((+((safe_add_func_uint32_t_u_u((safe_mod_func_uint32_t_u_u(((l_2480[2] == ((*g_711) = (*g_711))) , ((safe_add_func_int16_t_s_s(l_2460[0], (((*l_2483) |= 0L) != (safe_rshift_func_int16_t_s_u((safe_mul_func_int8_t_s_s((l_2407 == l_2488), (((*p_18) > (!(safe_sub_func_uint32_t_u_u((((void*)0 != &l_2385) >= l_2493[3]), 1L)))) < 4UL))), 1))))) <= p_17)), 0x231D321BL)), l_2494)) == p_20)) & 0UL);
                    }
                }
            }
            else
            { /* block id: 1114 */
                const int32_t **l_2495[2][5] = {{&g_1626,&g_1626,(void*)0,&g_1626,&g_1626},{(void*)0,&g_1626,(void*)0,(void*)0,&g_1626}};
                int i, j;
                g_2496 = ((*g_2352) = (*g_1941));
                for (l_2356 = 0; l_2356 < 5; l_2356 += 1)
                {
                    for (g_4 = 0; g_4 < 9; g_4 += 1)
                    {
                        g_782[l_2356][g_4] = (void*)0;
                    }
                }
            }
        }
        else
        { /* block id: 1119 */
            uint16_t l_2525 = 0x8B4BL;
            int32_t l_2526[6][6][7] = {{{1L,0xB47FD7B1L,(-7L),0xEA438903L,0xE8BBE87AL,1L,(-7L)},{0L,9L,2L,0xF31D62F5L,2L,9L,0L},{(-9L),(-7L),0x1A8732FAL,0xB47FD7B1L,(-9L),2L,(-7L)},{0x1EAFBC1CL,6L,0x1EAFBC1CL,0xDF4F4DEAL,(-3L),1L,(-1L)},{0xB47FD7B1L,0xE8BBE87AL,0x1A8732FAL,0x1A8732FAL,0xE8BBE87AL,0xB47FD7B1L,0xD12FF9C1L},{8L,0xDF4F4DEAL,2L,1L,0x0C6F0F1DL,1L,2L}},{{(-9L),2L,(-7L),0xD539A67DL,0xB47FD7B1L,2L,2L},{1L,0xDF4F4DEAL,(-5L),0xDF4F4DEAL,1L,9L,(-3L)},{1L,0xE8BBE87AL,0xEA438903L,(-7L),0xB47FD7B1L,1L,0xD12FF9C1L},{2L,6L,0L,0xF31D62F5L,0x0C6F0F1DL,0xDF4F4DEAL,0x0C6F0F1DL},{1L,(-7L),(-7L),1L,0xE8BBE87AL,0xEA438903L,(-7L)},{1L,9L,(-3L),0xF31D62F5L,(-3L),9L,1L}},{{(-9L),0xB47FD7B1L,0x1A8732FAL,(-7L),(-9L),(-9L),(-7L)},{8L,6L,8L,0xDF4F4DEAL,2L,1L,0x0C6F0F1DL},{0xB47FD7B1L,0xD12FF9C1L,0x1A8732FAL,0xD539A67DL,0xE8BBE87AL,(-7L),0xD12FF9C1L},{0x1EAFBC1CL,0xDF4F4DEAL,(-3L),1L,(-1L),1L,(-3L)},{(-9L),(-9L),(-7L),0x1A8732FAL,0xB47FD7B1L,(-9L),2L},{0L,0xDF4F4DEAL,0L,0xDF4F4DEAL,0L,9L,2L}},{{1L,0xD12FF9C1L,0xEA438903L,0xB47FD7B1L,0xB47FD7B1L,0xEA438903L,0xD12FF9C1L},{(-3L),6L,(-5L),0xF31D62F5L,(-1L),1L,1L},{0xD539A67DL,1L,0xEA438903L,0x1A8732FAL,(-9L),0xD539A67DL,0xEA438903L},{2L,0xDF4F4DEAL,8L,6L,8L,0xDF4F4DEAL,2L},{0xB47FD7B1L,0xEA438903L,0xD12FF9C1L,1L,0xB47FD7B1L,(-7L),0xEA438903L},{(-5L),9L,(-5L),1L,0x1EAFBC1CL,0xF31D62F5L,1L}},{{1L,(-9L),0xD12FF9C1L,0xD12FF9C1L,(-9L),1L,2L},{0L,1L,8L,0xF31D62F5L,0L,0xF31D62F5L,8L},{0xB47FD7B1L,(-7L),0xEA438903L,0xE8BBE87AL,1L,(-7L),(-7L)},{(-3L),1L,(-1L),1L,(-3L),0xDF4F4DEAL,0x1EAFBC1CL},{0xD539A67DL,(-9L),0x1A8732FAL,0xEA438903L,1L,0xD539A67DL,2L},{8L,9L,0x0C6F0F1DL,6L,0L,1L,0L}},{{0xD539A67DL,0xEA438903L,0xEA438903L,0xD539A67DL,(-9L),0x1A8732FAL,0xEA438903L},{(-3L),0xDF4F4DEAL,0x1EAFBC1CL,6L,0x1EAFBC1CL,0xDF4F4DEAL,(-3L)},{0xB47FD7B1L,1L,0xD12FF9C1L,0xEA438903L,0xB47FD7B1L,0xB47FD7B1L,0xEA438903L},{0L,9L,0L,1L,8L,0xF31D62F5L,0L},{1L,2L,0xD12FF9C1L,0xE8BBE87AL,(-9L),0xEA438903L,2L},{(-5L),1L,0x1EAFBC1CL,0xF31D62F5L,1L,0xF31D62F5L,0x1EAFBC1CL}}};
            int i, j, k;
            for (g_1027 = 0; (g_1027 <= 7); ++g_1027)
            { /* block id: 1122 */
                float *l_2514 = &g_1860[1][2][3];
                int32_t l_2527 = 0L;
                float *l_2528[1][7][10] = {{{&g_75[0][1],(void*)0,(void*)0,&g_75[0][1],(void*)0,(void*)0,&g_75[0][1],(void*)0,(void*)0,&g_75[0][1]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_75[1][6],(void*)0,(void*)0,(void*)0,&g_75[1][6],(void*)0,&g_75[0][1],&g_75[0][1],(void*)0,&g_75[1][6]},{&g_75[1][6],(void*)0,(void*)0,&g_75[1][6],(void*)0,(void*)0,&g_75[1][6],(void*)0,(void*)0,&g_75[1][6]},{(void*)0,&g_75[1][6],(void*)0,(void*)0,&g_75[1][6],(void*)0,(void*)0,&g_75[1][6],(void*)0,(void*)0},{&g_75[0][1],&g_75[0][1],(void*)0,&g_75[1][6],(void*)0,(void*)0,(void*)0,&g_75[1][6],(void*)0,&g_75[0][1]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
                int32_t l_2529 = 0x4D6958B0L;
                int32_t *l_2531 = &l_2494;
                int i, j, k;
                (*p_18) = (((*l_2531) ^= ((((*g_1000) = (0x47A5188AL | (((safe_add_func_int16_t_s_s(l_2353[1], 9UL)) == (safe_div_func_int64_t_s_s((((((l_2527 = (safe_mul_func_float_f_f((safe_add_func_float_f_f((((safe_add_func_float_f_f((safe_sub_func_float_f_f(p_19, (*g_438))), (((*l_2514) = (l_2511 == (void*)0)) >= (safe_div_func_float_f_f((safe_sub_func_float_f_f(l_2353[2], ((((((safe_mod_func_int64_t_s_s((safe_rshift_func_int8_t_s_s((l_2526[0][2][4] = (safe_rshift_func_uint16_t_u_s(g_835, l_2525))), l_2357[1][5])), l_2357[0][0])) > l_2527) >= (****g_919)) > (*p_18)) && 0x62F8D9F0L) , g_76))), p_16))))) == g_1627) < g_76), g_857)), p_20))) , p_17) == p_17) == 0x4BL) ^ 2L), (**g_1571)))) >= l_2529))) > l_2530) >= (*p_18))) , l_2526[2][2][6]);
            }
        }
        if ((safe_add_func_int8_t_s_s(((safe_div_func_int8_t_s_s(((***g_264) < ((*g_1000) &= (safe_sub_func_int32_t_s_s(((((safe_mul_func_float_f_f(0x6.491730p-97, ((*l_2544) = ((l_2540 != g_23) < (g_31 >= (safe_mul_func_float_f_f((0x1.Ep-1 == (((((l_2357[4][3] , (p_16 && ((((*l_2543) &= p_17) , 1L) < 0x9604E554L))) >= g_2187) <= (-9L)) , g_1789) , 0x4.E40599p+26)), 0xF.BEFD25p-5))))))) != g_2187) , p_16) | p_20), (*p_18))))), p_16)) || 0xE36CL), l_2493[3])))
        { /* block id: 1134 */
            int32_t *l_2545 = &l_2051;
            int32_t *l_2546 = &l_2353[1];
            int32_t *l_2547 = &g_1363;
            int32_t *l_2548 = &l_2039;
            int32_t *l_2549[9][8][3] = {{{(void*)0,&l_2353[0],&l_2051},{(void*)0,&l_2353[3],&l_2356},{&l_2056,&l_2056,&l_2056},{&g_1447,&g_1363,&l_2353[0]},{&g_738,&l_2353[0],&l_2056},{&l_2041,&l_2049,&g_1417},{&l_2039,&l_2353[0],(void*)0},{&l_2356,&g_95,&l_2356}},{{&g_1417,&l_2056,&g_1363},{&g_95,&g_1447,&g_738},{&l_2049,&g_1447,&g_1447},{&l_2051,&g_1417,&l_2051},{&l_2048,(void*)0,&l_2353[1]},{&l_2321,(void*)0,&l_2353[1]},{&g_95,(void*)0,&g_738},{(void*)0,(void*)0,&l_2353[3]}},{{(void*)0,(void*)0,&l_2056},{(void*)0,&g_1417,&g_4},{(void*)0,&g_1447,&l_2039},{&l_2056,&g_1447,&g_1417},{&g_1447,&l_2056,(void*)0},{&g_738,&g_95,(void*)0},{&l_2353[1],&l_2353[0],&l_2032},{(void*)0,&l_2049,(void*)0}},{{&l_2356,&l_2353[0],&l_2039},{&l_2353[0],&g_1363,&l_2056},{(void*)0,&l_2056,&l_2053},{(void*)0,&l_2051,(void*)0},{&l_2056,&g_738,&l_2032},{&l_2353[0],(void*)0,&l_2051},{&l_2049,&g_1417,&l_2356},{&l_2054,&g_1447,&l_2049}},{{&l_2054,(void*)0,&l_2353[0]},{&l_2049,&l_2053,(void*)0},{&l_2353[0],&l_2321,(void*)0},{&l_2056,&l_2356,&l_2051},{(void*)0,&l_2032,&g_95},{(void*)0,&l_2041,(void*)0},{&l_2353[0],&l_2353[1],&l_2356},{&l_2356,&g_255,&g_1417}},{{(void*)0,&l_2054,(void*)0},{&l_2353[1],&l_2056,&g_1363},{&g_738,&g_1363,&g_1417},{&g_1447,(void*)0,&l_2041},{&l_2056,&g_1447,&g_137},{(void*)0,(void*)0,(void*)0},{(void*)0,&l_2049,&g_1363},{(void*)0,&l_2051,(void*)0}},{{(void*)0,(void*)0,&l_2041},{&g_95,(void*)0,(void*)0},{&l_2321,&l_2049,&g_1363},{&l_2048,&g_137,(void*)0},{&l_2051,&l_2032,&g_137},{&l_2049,&g_95,&l_2041},{&g_95,&g_95,&g_1417},{&g_1417,(void*)0,&g_1363}},{{&l_2356,&l_2032,(void*)0},{&l_2039,&g_1447,&g_1417},{&l_2041,&l_2353[0],&l_2356},{&g_738,(void*)0,(void*)0},{&g_1447,(void*)0,&g_95},{(void*)0,&g_1447,&l_2051},{&l_2353[1],&g_4,(void*)0},{&l_2032,(void*)0,(void*)0}},{{&l_2322[6],&l_2056,&l_2353[0]},{&l_2356,&l_2048,&l_2039},{&l_2049,(void*)0,(void*)0},{&l_2039,(void*)0,&l_2051},{&l_2051,&g_1447,&g_1447},{(void*)0,&g_1417,&l_2056},{&l_2321,&l_2041,&g_95},{&l_2051,&g_95,(void*)0}}};
            int64_t l_2554 = (-7L);
            int i, j, k;
            ++l_2556[0];
            if ((*p_18))
            { /* block id: 1136 */
                if (g_76)
                    goto lbl_2559;
                for (l_2555 = (-26); (l_2555 < 28); l_2555++)
                { /* block id: 1140 */
                    (*g_1937) |= l_2556[0];
                    for (g_1447 = 0; (g_1447 <= 8); g_1447 += 1)
                    { /* block id: 1144 */
                        return (*g_1572);
                    }
                }
            }
            else
            { /* block id: 1148 */
                int32_t l_2578 = 1L;
                uint16_t *l_2581 = &g_1085;
                int32_t l_2607[7];
                const float l_2608 = 0xD.06FDEAp+36;
                int i;
                for (i = 0; i < 7; i++)
                    l_2607[i] = 0xCE3A34EDL;
                for (l_2056 = 0; (l_2056 >= 22); l_2056 = safe_add_func_int16_t_s_s(l_2056, 7))
                { /* block id: 1151 */
                    int32_t l_2575 = 0x41C6AEDBL;
                    for (g_137 = 0; (g_137 <= 1); g_137 += 1)
                    { /* block id: 1154 */
                        int64_t *l_2576 = &g_556;
                        int i;
                        l_2353[g_137] ^= ((safe_lshift_func_uint8_t_u_s((((*g_1937) , (*g_1725)) == ((safe_rshift_func_int16_t_s_u((((*l_2576) |= (safe_sub_func_uint16_t_u_u((((**g_1199) &= (safe_mod_func_uint32_t_u_u((g_557 , (l_2572 != ((**g_1106) = 0xC54CL))), (p_19 , (*p_18))))) != (safe_lshift_func_int16_t_s_u(((((*l_2545) ^ p_17) || (-1L)) & (*g_131)), l_2575))), p_17))) == 0x29CE2CD280648D4BLL), l_2577)) , (void*)0)), p_16)) < 0L);
                        if (l_2575)
                            break;
                    }
                }
                (*l_2546) = l_2578;
                (*g_94) = ((safe_div_func_uint16_t_u_u(((*l_2581) = (*l_2546)), (safe_mod_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((l_2586 && l_2578), 14)), (safe_div_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u((((*l_2386) = ((**g_1571) = (((((void*)0 == p_18) || (safe_mod_func_int64_t_s_s((safe_sub_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(((safe_sub_func_uint64_t_u_u((((safe_add_func_int64_t_s_s((((*p_18) = (0xDC1FL >= ((safe_lshift_func_int16_t_s_s(((*g_1107) = l_2555), (l_2607[5] = (safe_sub_func_uint64_t_u_u((*l_2548), (safe_sub_func_int8_t_s_s((p_20 | 0UL), (*g_1000)))))))) , (*g_1107)))) != l_2357[4][3]), (*g_1572))) && g_557) , p_20), (**g_1571))) > 0UL), p_16)), p_19)), 5L))) && 65530UL) , (*l_2546)))) <= p_17), (****g_863))), (-1L))))))) , 0xC1ED6FC1L);
            }
            return l_2357[4][3];
        }
        else
        { /* block id: 1172 */
            int32_t l_2621[6][3] = {{0L,0x5AC925F6L,0L},{1L,(-8L),1L},{0L,0x5AC925F6L,0L},{1L,(-8L),1L},{0L,0x5AC925F6L,0L},{1L,(-8L),1L}};
            int32_t l_2634 = 0x43A284DEL;
            int32_t *l_2647 = &l_2056;
            uint32_t *****l_2689 = (void*)0;
            int i, j;
            if ((safe_rshift_func_int16_t_s_s(((((0x42L != (safe_mul_func_uint16_t_u_u((&g_1046 == l_2613), (safe_mul_func_uint16_t_u_u((247UL && ((((**g_1045) & (safe_mul_func_uint8_t_u_u((safe_div_func_int32_t_s_s(l_2552, l_2621[2][1])), l_2621[2][1]))) ^ ((safe_lshift_func_int16_t_s_u(((*g_1200) = (((safe_div_func_int16_t_s_s(((((((l_2428 >= (**g_1571)) ^ 0x882208EF774DC31FLL) < l_2540) != g_2626) > 255UL) ^ l_2621[2][1]), l_2621[4][2])) != g_849[9][3]) <= l_2550)), 0)) ^ p_19)) && 0x09L)), 65529UL))))) ^ 4294967287UL) > l_2356) ^ l_2555), (***g_1105))))
            { /* block id: 1174 */
                int32_t *l_2628 = &g_95;
                int32_t **l_2648 = &g_2474[2];
                (*l_2628) |= (*p_18);
                for (g_857 = 0; (g_857 <= 5); g_857 += 1)
                { /* block id: 1178 */
                    float l_2629 = (-0x1.7p+1);
                    int32_t l_2646 = 0x26271037L;
                    (*l_2628) &= (*p_18);
                    for (p_19 = 0; (p_19 <= 3); p_19 += 1)
                    { /* block id: 1182 */
                        int32_t l_2645 = 3L;
                        (*p_18) = (((**g_1199) &= ((((l_2621[2][1] , (((((*l_2543) |= (safe_lshift_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(((*****g_918) , (l_2634 ^= ((*****l_2360) ^= (0xE3L == 0xB6L)))), (((*l_2628) = (safe_mul_func_uint8_t_u_u(0xF2L, ((safe_mul_func_int8_t_s_s(((safe_sub_func_uint64_t_u_u((*g_1572), (((p_17 <= (safe_sub_func_uint64_t_u_u((safe_mod_func_int8_t_s_s(((0x3A09B3E919AE39FCLL < (**g_1571)) || l_2645), p_20)), l_2646))) && (*l_2628)) , l_2353[0]))) <= (**g_865)), 0x08L)) , (****g_919))))) , l_2555))), (*g_131)))) , g_327) , 0xC19499FEL) || (*p_18))) <= 0x567EFC9637432DD8LL) ^ p_19) < 0x00L)) <= g_76);
                        if ((*p_18))
                            break;
                    }
                }
                p_18 = p_18;
                (*l_2648) = (l_2647 = p_18);
            }
            else
            { /* block id: 1195 */
                int16_t *l_2660 = &l_2586;
                int32_t l_2661 = (-7L);
                uint8_t ***l_2681 = (void*)0;
                int32_t *l_2683[10][9][2] = {{{(void*)0,&l_2321},{&l_2322[6],&l_2353[1]},{&l_2661,&l_2321},{&g_1417,(void*)0},{(void*)0,&l_2048},{(void*)0,(void*)0},{(void*)0,&g_95},{&g_835,&g_95},{&l_2321,(void*)0}},{{&l_2661,&g_1447},{(void*)0,(void*)0},{&l_2322[6],&g_95},{&l_2353[1],&g_1447},{&l_2353[1],&g_1363},{&l_2321,&l_2353[0]},{&l_2634,&g_95},{&l_2322[6],&l_2054},{(void*)0,&l_2552}},{{&g_1363,(void*)0},{&l_2356,&l_2053},{&l_2661,&l_2661},{(void*)0,&l_2321},{&g_4,(void*)0},{&g_255,(void*)0},{&l_2041,&l_2322[6]},{&g_4,&l_2048},{&l_2322[6],&l_2661}},{{&g_95,&l_2321},{&l_2356,&g_1363},{(void*)0,&l_2552},{&l_2053,(void*)0},{&l_2322[6],&l_2056},{&g_835,&l_2353[0]},{&l_2056,(void*)0},{&l_2353[1],&l_2552},{(void*)0,&g_95}},{{(void*)0,&g_95},{(void*)0,&l_2552},{&l_2353[1],(void*)0},{&l_2056,&l_2353[0]},{&g_835,&l_2056},{&l_2322[6],(void*)0},{&l_2053,&l_2552},{(void*)0,&g_1363},{&l_2356,&l_2321}},{{&g_95,&l_2661},{&l_2322[6],&l_2048},{&g_4,&l_2322[6]},{&l_2041,(void*)0},{&g_255,(void*)0},{&g_4,&l_2321},{(void*)0,&l_2661},{&l_2661,&l_2053},{&l_2356,(void*)0}},{{&g_1363,&l_2552},{(void*)0,&l_2054},{&l_2322[6],&g_95},{&l_2634,&l_2353[0]},{&l_2321,&g_1363},{&l_2353[1],&g_1447},{&l_2353[1],&g_95},{&l_2322[6],(void*)0},{(void*)0,&g_1447}},{{&l_2661,(void*)0},{&l_2321,&g_95},{&g_835,&g_95},{(void*)0,(void*)0},{(void*)0,&l_2048},{(void*)0,(void*)0},{&g_1417,&l_2321},{&l_2661,&l_2353[1]},{&l_2322[6],&l_2321}},{{(void*)0,&l_2322[6]},{&g_255,&g_1363},{&g_255,&l_2322[6]},{(void*)0,&l_2321},{&l_2322[6],&l_2353[1]},{&l_2048,&l_2634},{&l_2322[4],(void*)0},{(void*)0,&g_255},{&l_2321,&g_1363}},{{&l_2356,&g_1417},{&l_2049,(void*)0},{&l_2048,(void*)0},{&l_2039,(void*)0},{&l_2356,&l_2353[2]},{&l_2353[1],&l_2041},{(void*)0,(void*)0},{&g_95,&g_738},{&l_2048,&g_95}}};
                int i, j, k;
                (*p_18) = (safe_rshift_func_uint8_t_u_s((p_20 & (**g_347)), (g_95 | ((safe_sub_func_int32_t_s_s((safe_lshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(0UL, 6UL)), 6)), (*l_2647))) , (((safe_lshift_func_int16_t_s_u(g_2659, ((void*)0 == l_2660))) || l_2661) , g_1627)))));
                if (((((*****l_2360) = (0x697BF793AF75EEA8LL >= ((p_17 >= (((l_2662 == (void*)0) , (safe_sub_func_uint16_t_u_u((((safe_lshift_func_int8_t_s_u(p_20, 6)) || (safe_add_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((*g_1046), (((0xA3L >= (safe_sub_func_uint32_t_u_u(((safe_mul_func_int8_t_s_s((*g_1000), ((safe_add_func_int32_t_s_s(((*p_18) = (safe_rshift_func_uint8_t_u_u(p_16, 2))), 0xB3770866L)) && p_19))) | (*l_2647)), (*l_2647)))) & 0xB219L) , l_2661))), (-6L)))) > (*l_2647)), g_2121))) , (**g_1106))) < (-10L)))) < (*l_2647)) | (-6L)))
                { /* block id: 1199 */
                    uint16_t l_2682[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_2682[i] = 1UL;
                    l_2682[0] &= (safe_div_func_int64_t_s_s((l_2681 == (void*)0), p_19));
                }
                else
                { /* block id: 1201 */
                    (*l_2647) &= (*p_18);
                }
                l_2683[2][2][1] = p_18;
                (*p_18) = (p_16 || (safe_mul_func_uint16_t_u_u(p_16, ((***g_920) | (((***g_711) = (~((safe_sub_func_uint64_t_u_u((l_2689 == (l_2362 = l_2689)), (safe_add_func_int8_t_s_s(((l_2692 != &g_1814) < (safe_mod_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((((1UL && (safe_sub_func_uint32_t_u_u((safe_add_func_int8_t_s_s(((*g_1572) != 0x06CDBAE5E0EB74BFLL), 0x1CL)), g_2703))) < 0x2FB5965AL) ^ 0UL), 4294967295UL)), 0xA5L))), p_16)))) >= 0x2CL))) , p_19)))));
            }
            (*g_1920) = &l_2634;
        }
        for (g_76 = 0; (g_76 >= 24); g_76 = safe_add_func_uint16_t_u_u(g_76, 1))
        { /* block id: 1213 */
            uint32_t * const ** const *l_2706 = &g_1767;
            int32_t * const l_2707[2][10] = {{&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447},{&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447,&g_1447}};
            const uint8_t *l_2748[8];
            const uint8_t **l_2747 = &l_2748[2];
            const uint64_t *l_2792 = &g_252;
            uint32_t l_2797 = 0x3331A082L;
            float l_2815 = 0x1.FD95B3p+36;
            int16_t ***l_2861 = &g_1199;
            uint32_t l_2865 = 4294967294UL;
            int32_t *l_2868 = (void*)0;
            int i, j;
            for (i = 0; i < 8; i++)
                l_2748[i] = &g_132;
            (*p_18) = (*p_18);
            if (((void*)0 != l_2706))
            { /* block id: 1215 */
                int64_t l_2729[9];
                int32_t *l_2752 = &l_2356;
                int i;
                for (i = 0; i < 9; i++)
                    l_2729[i] = 1L;
                (*g_1381) = l_2707[0][9];
                for (g_1363 = 0; (g_1363 == (-12)); --g_1363)
                { /* block id: 1219 */
                    int32_t **** const l_2714 = &l_2662;
                    (*g_898) = p_18;
                }
                l_2752 = l_2752;
            }
            else
            { /* block id: 1245 */
                int32_t l_2759 = 0x9A94A739L;
                int32_t * const **l_2762 = (void*)0;
                int32_t * const ***l_2761 = &l_2762;
                float *l_2782 = &g_1860[1][2][1];
                (***g_1813) = (p_17 == ((safe_mul_func_float_f_f((p_17 != (safe_div_func_float_f_f((safe_div_func_float_f_f((((l_2759 |= (&g_1770 == &l_2724[1])) , (((*g_1577) < (!(l_2761 == g_2763[0][1]))) == ((**l_2693) = ((0L >= p_17) , g_1085)))) > p_20), g_2626)), g_2626))), 0x0.8p+1)) < p_20));
                for (g_4 = 0; (g_4 <= (-4)); g_4 = safe_sub_func_int32_t_s_s(g_4, 1))
                { /* block id: 1251 */
                    (*g_2765) = (*g_2765);
                    return p_16;
                }
                (***g_1813) = (safe_sub_func_float_f_f(0xB.BF0F24p-29, (safe_mul_func_float_f_f(((((*l_2694) = (safe_add_func_float_f_f(((void*)0 == l_2777), (***g_1813)))) != (safe_mul_func_float_f_f((safe_add_func_float_f_f(((*l_2782) = p_16), (safe_sub_func_float_f_f((((--(*g_131)) , (safe_add_func_float_f_f(p_20, (((safe_rshift_func_int8_t_s_u((((safe_unary_minus_func_int64_t_s((((-1L) == (((*l_2385) = &g_1789) != (p_17 , l_2792))) == g_974))) < 1L) || 0x22FC0B2E66A2DB0ELL), (**g_865))) , g_655) != p_19)))) != p_16), p_20)))), 0x2.08BB2Bp-18))) >= 0x1.0p-1), g_4))));
                (*g_2793) = p_18;
            }
            for (l_2586 = 12; (l_2586 < (-17)); l_2586 = safe_sub_func_uint32_t_u_u(l_2586, 2))
            { /* block id: 1264 */
                int32_t *l_2796 = &l_2053;
                int32_t l_2869 = (-2L);
                p_18 = l_2796;
            }
        }
    }
    l_2053 = (((*l_2694) = (safe_mul_func_float_f_f(p_16, (((safe_div_func_int32_t_s_s((((void*)0 == &g_863) >= 0xE7L), ((((safe_lshift_func_uint8_t_u_u((((((*g_1572) &= ((**l_2385) = p_19)) ^ (safe_div_func_uint32_t_u_u((((((safe_mul_func_int16_t_s_s((p_17 = (**g_1106)), (***g_1105))) >= (l_2886[2] < (p_16 != l_2886[2]))) || 0x902FL) == p_16) , 4294967287UL), (*p_18)))) != (*g_1200)) & (-1L)), 7)) > (-1L)) >= (*p_18)) && (**g_1199)))) || 251UL) , 0xF.36E3A5p-0)))) > p_19);
    if ((safe_lshift_func_int16_t_s_s(((l_2889[0][4] == (-1L)) ^ (safe_mod_func_uint64_t_u_u(((*p_18) > ((0x5C40BFC8L & (*p_18)) , (safe_rshift_func_uint8_t_u_u(((***g_264) = ((**g_1571) > (safe_add_func_int8_t_s_s(((*g_1000) |= (((((safe_mul_func_uint16_t_u_u(g_1627, (((g_1085 = (((p_19 == (p_16 || 0x57B33D429DA002B7LL)) , (*g_1757)) != (*g_1771))) <= p_20) != 0x47FC9EF285E67DD7LL))) , l_2898[5]) != (*g_1767)) , 0xAD70L) != 0x2933L)), 0x34L)))), p_17)))), p_20))), 7)))
    { /* block id: 1321 */
        const int32_t ***l_2899 = (void*)0;
        (*g_2901) = (g_2900 = l_2899);
    }
    else
    { /* block id: 1324 */
        int32_t l_2908 = 0x27F7644AL;
        const int64_t *l_2916 = &g_2626;
        const int64_t **l_2915 = &l_2916;
        const int64_t ***l_2917[9][2] = {{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915},{&l_2915,&l_2915}};
        int32_t l_2937 = 0x1D3B1AC6L;
        int32_t l_2938 = 1L;
        int32_t l_2939[5] = {(-8L),(-8L),(-8L),(-8L),(-8L)};
        int32_t l_2958 = 0xDC72D017L;
        int64_t l_3034 = 3L;
        int64_t l_3073 = 0xB84074E6D8F4D858LL;
        float l_3080 = 0x8.8EAFADp+23;
        int64_t l_3083[1];
        int16_t l_3127 = 0x63E9L;
        uint64_t l_3128 = 0xA197D474FEB9BC79LL;
        const int32_t *l_3153 = &l_2056;
        int16_t **l_3185 = &g_1200;
        uint64_t ***l_3201 = &l_2385;
        uint64_t ****l_3200 = &l_3201;
        uint8_t l_3280[5][4][4] = {{{0UL,0x3AL,253UL,0x3AL},{0UL,0x53L,0xB0L,0x3AL},{0xB0L,0x3AL,0xB0L,0x53L},{0UL,0x3AL,253UL,0x3AL}},{{0UL,0x53L,0xB0L,0x3AL},{0xB0L,0x3AL,0xB0L,0x53L},{0UL,0x3AL,253UL,0x3AL},{0UL,0x53L,0xB0L,0x3AL}},{{0xB0L,0x3AL,0xB0L,0x53L},{0UL,0x3AL,253UL,0x3AL},{0UL,0x53L,0xB0L,0x3AL},{0xB0L,0x3AL,0xB0L,0x53L}},{{0UL,0x3AL,253UL,0x3AL},{0UL,0x53L,0xB0L,0x3AL},{0xB0L,0x3AL,0xB0L,0x53L},{0UL,0x3AL,253UL,0x3AL}},{{0UL,0x53L,0xB0L,0x3AL},{0xB0L,0x3AL,0xB0L,0x53L},{0UL,0x3AL,253UL,0x3AL},{0UL,0x53L,0xB0L,0x3AL}}};
        uint64_t ****l_3324 = &l_3201;
        int32_t l_3356 = 6L;
        uint8_t l_3358[7][7][2] = {{{0xB3L,0xB3L},{0x80L,0UL},{0xCDL,0x5BL},{0xD1L,255UL},{255UL,0xD1L},{0xF5L,0x69L},{0xF5L,0xD1L}},{{255UL,255UL},{0xD1L,0x5BL},{0xCDL,0UL},{0x80L,0xB3L},{0xB3L,255UL},{0xA1L,255UL},{0xB3L,0xB3L}},{{0x80L,0UL},{0xCDL,0x5BL},{0xD1L,255UL},{255UL,0xD1L},{0xF5L,0x69L},{0xF5L,0xD1L},{255UL,255UL}},{{0x80L,255UL},{0xF5L,255UL},{0UL,0x69L},{0x69L,0xA1L},{0xB3L,0xA1L},{0x69L,0x69L},{0UL,255UL}},{{0xF5L,255UL},{0x80L,0x9EL},{0xA1L,0x80L},{0x5BL,0xD1L},{0x5BL,0x80L},{0xA1L,0x9EL},{0x80L,255UL}},{{0xF5L,255UL},{0UL,0x69L},{0x69L,0xA1L},{0xB3L,0xA1L},{0x69L,0x69L},{0UL,255UL},{0xF5L,255UL}},{{0x80L,0x9EL},{0xA1L,0x80L},{0x5BL,0xD1L},{0x5BL,0x80L},{0xA1L,0x9EL},{0x80L,255UL},{0xF5L,255UL}}};
        int32_t ****l_3370[10] = {&l_2178,&l_2178,&l_2178,&l_2178,&l_2178,&l_2178,&l_2178,&l_2178,&l_2178,&l_2178};
        uint64_t *****l_3382[4];
        uint8_t **l_3461 = &g_131;
        int8_t l_3563 = 0xA4L;
        uint8_t l_3564 = 255UL;
        float ** const *l_3630 = &g_3481;
        uint64_t l_3632 = 0x6F8E2FA1A76960BDLL;
        uint32_t *****l_3708[8] = {(void*)0,&g_1725,(void*)0,(void*)0,&g_1725,(void*)0,(void*)0,&g_1725};
        int32_t ** const *l_3722 = &g_3057;
        int32_t ** const **l_3721[3][1];
        int8_t ***l_3741 = (void*)0;
        int32_t l_3751 = (-1L);
        int16_t l_3769 = 0xBDDFL;
        int32_t l_3789 = (-9L);
        uint16_t **l_3810 = &g_3809;
        uint64_t l_3830 = 0x179FBAD1F152F30ELL;
        uint32_t l_3853[6][2] = {{4UL,4UL},{4UL,4UL},{4UL,4UL},{4UL,4UL},{4UL,4UL},{4UL,4UL}};
        float l_3854 = 0x5.F1353Bp-75;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_3083[i] = 0xEA90D7189DE282E9LL;
        for (i = 0; i < 4; i++)
            l_3382[i] = &l_3380[2];
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_3721[i][j] = &l_3722;
        }
        if ((safe_lshift_func_int8_t_s_s(((safe_add_func_uint8_t_u_u((9UL | l_2908), (((((p_19 >= (safe_rshift_func_uint16_t_u_s((g_2187 = l_2911), 6))) >= (*g_1960)) || (l_2908 , (safe_lshift_func_int8_t_s_u(((*g_1000) = (p_17 > ((!((g_2918 = l_2915) == &g_1046)) ^ (***g_2749)))), 4)))) , l_2908) , 0x49L))) != (**g_347)), p_17)))
        { /* block id: 1328 */
            int32_t l_2929[9] = {0xBC0B2851L,(-1L),(-1L),0xBC0B2851L,(-1L),(-1L),0xBC0B2851L,(-1L),(-1L)};
            int32_t l_2940 = 6L;
            int32_t l_2941 = 0L;
            int32_t l_2942 = 0x53940099L;
            int32_t l_2943 = 0x67F7752CL;
            int16_t l_2944 = 0x81D9L;
            int32_t l_2945 = 0x62097F22L;
            uint32_t l_2946 = 0x95AEB1EEL;
            int32_t *l_2949 = &l_2945;
            int32_t *l_2950 = &g_95;
            int32_t *l_2951 = &g_255;
            int32_t *l_2952 = &l_2048;
            int32_t *l_2953 = &l_2942;
            int32_t *l_2954 = &g_4;
            int32_t *l_2955 = &g_137;
            int32_t *l_2956 = &l_2041;
            int32_t *l_2957[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            const float l_3021 = 0x8.E6BE34p-67;
            int32_t *l_3055 = &l_2929[3];
            int32_t **l_3054 = &l_3055;
            uint8_t l_3084 = 0xA4L;
            int16_t **l_3186 = (void*)0;
            int8_t l_3236 = 0x59L;
            uint32_t ***l_3241 = &l_2898[5];
            int i;
            for (g_76 = 6; (g_76 == 17); ++g_76)
            { /* block id: 1331 */
                int32_t l_2932 = 0x42AB5169L;
                int64_t *l_2933 = &g_160;
                int32_t *l_2934 = &g_1447;
                int32_t *l_2935 = &l_2056;
                int32_t *l_2936[1][8][9] = {{{&g_95,&l_2054,&l_2054,&g_95,&l_2048,&l_2054,&g_1417,&l_2048,&l_2048},{(void*)0,&g_1363,(void*)0,&l_2322[5],(void*)0,&g_1363,(void*)0,&g_1363,(void*)0},{&g_95,&l_2048,&l_2054,&g_1417,&l_2048,&l_2048,&g_1417,&l_2054,&l_2048},{&g_95,&g_1363,&l_2032,&l_2322[5],&l_2032,&g_1363,&g_95,&g_1363,&l_2032},{&g_95,&l_2054,&l_2054,&g_95,&l_2048,&l_2054,&g_1417,&l_2048,&l_2048},{(void*)0,&g_1363,(void*)0,&l_2322[5],(void*)0,&g_1363,(void*)0,&g_1363,(void*)0},{&g_95,&l_2048,&l_2054,&g_1417,&l_2048,&l_2048,&g_1417,&l_2054,&l_2048},{&g_95,&g_1363,&l_2032,&l_2322[5],&l_2032,&g_1363,&g_95,&g_1363,&l_2032}}};
                int i, j, k;
                (**g_2765) ^= (((safe_div_func_int64_t_s_s(((*l_2933) = (safe_mod_func_int8_t_s_s((((((((*l_2614) = ((safe_sub_func_int8_t_s_s(l_2929[1], 4UL)) >= (safe_rshift_func_uint16_t_u_u((((p_20 & ((**g_265) < (((l_2908 && (l_2932 || 0x95852B95L)) && (-1L)) == (p_20 , l_2932)))) > l_2932) && p_19), 8)))) , l_2929[6]) | (*p_18)) <= l_2908) && g_2121) | p_19), p_17))), (*g_2919))) , 4294967295UL) | (*p_18));
                ++l_2946;
            }
            ++l_2959;
            if (l_2939[0])
            { /* block id: 1338 */
lbl_2973:
                for (l_2054 = 0; (l_2054 <= (-2)); l_2054 = safe_sub_func_uint16_t_u_u(l_2054, 2))
                { /* block id: 1341 */
                    uint8_t l_2964 = 0x53L;
                    int32_t **l_2967 = &l_2954;
                    l_2964--;
                    (*l_2967) = &l_2939[3];
                }
            }
            else
            { /* block id: 1345 */
                int32_t *l_2968[5];
                int32_t l_2969 = 0x7EACFA3EL;
                uint32_t l_2970 = 4294967294UL;
                int8_t **l_2981 = &g_1000;
                uint8_t l_3016[6][3] = {{0UL,0x6FL,247UL},{0UL,0UL,0x6FL},{255UL,0x6FL,0x6FL},{0x6FL,1UL,247UL},{255UL,1UL,255UL},{0UL,0x6FL,1UL}};
                int i, j;
                for (i = 0; i < 5; i++)
                    l_2968[i] = (void*)0;
                if (((((*l_2831) == (*g_1106)) , 0x65BD34A5L) & ((void*)0 != &p_19)))
                { /* block id: 1346 */
                    l_2968[0] = p_18;
                    --l_2970;
                }
                else
                { /* block id: 1349 */
                    p_18 = &l_2939[0];
                    if (g_655)
                        goto lbl_2973;
                }
                l_2974++;
                if ((safe_mod_func_int64_t_s_s((((((*g_1572) ^= (safe_mul_func_int8_t_s_s(((*g_348) || (&l_2915 == (void*)0)), (&g_1000 != l_2981)))) & 7L) == 7UL) , (p_20 , (safe_sub_func_int32_t_s_s((safe_lshift_func_int8_t_s_u((*l_2954), 0)), l_2939[0])))), p_17)))
                { /* block id: 1355 */
                    uint32_t **l_2990 = &g_888;
                    int32_t l_2998 = 0xD4A2F5FCL;
                    int32_t l_2999 = 0xFF21CF9EL;
                    int32_t l_3000[10] = {1L,1L,0xB8C536C1L,0x4CB45BF2L,0xB8C536C1L,1L,1L,0xB8C536C1L,0x4CB45BF2L,0xB8C536C1L};
                    int i;
                    (*l_2955) = (*p_18);
                    p_18 = p_18;
                    (*l_2954) = (*p_18);
                    if ((safe_sub_func_uint32_t_u_u(4294967295UL, (((**l_2613) = (((*g_2918) == ((((((((*g_1771) = l_2989) == (p_19 , l_2990)) | ((safe_lshift_func_uint8_t_u_u((((((**g_347) , (*l_2954)) ^ (safe_sub_func_uint32_t_u_u((((safe_div_func_int32_t_s_s((((!(((*g_1572) || p_20) || 0x2D06D5CDL)) < g_2659) | p_16), p_17)) < (*g_1046)) > 0x3153AEDDL), 0x673AF345L))) & p_17) || l_2998), 4)) || 1L)) && 255UL) , (***g_1813)) , (*g_1572)) , (*g_1045))) != (**g_1571))) , (*l_2952)))))
                    { /* block id: 1361 */
                        float l_3001 = 0xB.0B7640p-99;
                        l_3002--;
                    }
                    else
                    { /* block id: 1363 */
                        l_2968[0] = &l_2939[4];
                    }
                }
                else
                { /* block id: 1366 */
                    uint8_t *l_3005 = &l_2886[4];
                    int32_t l_3009 = 0x2ECB25AEL;
                    int32_t l_3075 = 0L;
                    int32_t l_3076 = 2L;
                    int32_t l_3082[10][5] = {{0x78AFAE1FL,0x7EBADC7DL,0x7EBADC7DL,0x78AFAE1FL,0x78AFAE1FL},{0x59F997EAL,0xA3112748L,0x59F997EAL,0xA3112748L,0x59F997EAL},{0x78AFAE1FL,0x78AFAE1FL,0x7EBADC7DL,0x7EBADC7DL,0x78AFAE1FL},{(-1L),0xA3112748L,(-1L),0xA3112748L,(-1L)},{0x78AFAE1FL,0x7EBADC7DL,0x7EBADC7DL,0x78AFAE1FL,0x78AFAE1FL},{0x59F997EAL,0xA3112748L,0x59F997EAL,0xA3112748L,0x59F997EAL},{0x78AFAE1FL,0x78AFAE1FL,0x7EBADC7DL,0x7EBADC7DL,0x78AFAE1FL},{(-1L),0xA3112748L,(-1L),0xA3112748L,(-1L)},{0x78AFAE1FL,0x7EBADC7DL,0x7EBADC7DL,0x78AFAE1FL,0x78AFAE1FL},{0x59F997EAL,0xA3112748L,0x59F997EAL,0xA3112748L,0x59F997EAL}};
                    const uint16_t l_3105 = 0xE494L;
                    uint16_t *l_3106 = (void*)0;
                    uint16_t *l_3107 = &l_2258;
                    int i, j;
                    (*l_2953) ^= (((*l_3005) &= ((****g_863) = (***g_864))) ^ (((-1L) != ((*l_2952) = (&g_248[0][2] == (void*)0))) | (((!((safe_mul_func_uint8_t_u_u((l_3009 = (l_2937 = (*l_2955))), p_19)) && ((p_19 <= (~l_3009)) == (((**g_1571) = (safe_add_func_int8_t_s_s(((((+l_2572) , (*g_2918)) == (*g_1045)) != (*l_2950)), p_17))) == l_3016[2][2])))) >= p_16) || l_2908)));
                    if ((safe_rshift_func_int16_t_s_u((safe_add_func_int16_t_s_s((**g_1199), (***g_1105))), 14)))
                    { /* block id: 1374 */
                        uint16_t *l_3047 = (void*)0;
                        uint16_t *l_3048 = &l_2889[0][6];
                        float *l_3049 = &g_1860[2][2][2];
                        int8_t *l_3050 = (void*)0;
                        int8_t *l_3051 = (void*)0;
                        int8_t *l_3052 = (void*)0;
                        int8_t *l_3053 = &g_1696;
                        int32_t ***l_3056[3];
                        int32_t ****l_3059 = &l_3056[2];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_3056[i] = &l_3054;
                        g_3057 = ((p_16 || (safe_lshift_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u(((safe_div_func_int16_t_s_s(((((safe_div_func_int32_t_s_s((*p_18), (l_2939[1] & p_19))) ^ ((((*l_3053) &= (safe_div_func_int8_t_s_s(l_3034, ((!(!((((-7L) | (((**l_2981) = (p_17 & (safe_rshift_func_int16_t_s_s((safe_sub_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(251UL, (safe_mod_func_uint16_t_u_u(((*l_3048) = g_835), p_20)))), 5)), (-7L))), p_20)))) != 6L)) , (void*)0) != l_3049))) | 0xA59BL)))) , (*p_18)) || (**g_347))) ^ (*p_18)) <= p_17), 1L)) <= p_16), p_20)) > p_16), 12)), p_20))) , l_3054);
                        (*l_3059) = &l_3054;
                    }
                    else
                    { /* block id: 1380 */
                        uint64_t l_3074[10] = {0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL,0xD3126F19EDAB9ED3LL};
                        int32_t l_3077 = 0x1985E4B2L;
                        int32_t l_3078 = 3L;
                        int32_t l_3079 = 0x426BF7E4L;
                        int32_t l_3081[1][4] = {{3L,3L,3L,3L}};
                        int i, j;
                        (*l_2953) &= (safe_mul_func_uint8_t_u_u(p_16, (safe_div_func_int32_t_s_s(((!(+(l_3034 < (((g_3066 >= (safe_div_func_uint64_t_u_u(p_20, (((**l_2385) |= (&p_19 == ((*g_347) = (*g_347)))) && (((&p_18 == (((g_160 = ((safe_lshift_func_int16_t_s_s(((*g_1107) = (safe_rshift_func_int8_t_s_s((((l_3073 < (((**g_2918) || 18446744073709551611UL) < 0xF4L)) != l_3074[8]) || 1L), 7))), l_3009)) , p_16)) < l_3073) , (*g_2902))) >= (**g_865)) | p_20))))) >= p_16) , l_2958)))) == p_17), 0x047E1A2EL))));
                        l_3084--;
                        l_2952 = p_18;
                    }
                    (*l_2951) = (safe_unary_minus_func_int16_t_s((((*l_3107) = (0xC2D6L | (safe_mod_func_int8_t_s_s((((safe_mul_func_float_f_f(l_2958, p_16)) , (safe_rshift_func_int16_t_s_s((safe_sub_func_uint64_t_u_u((~(+((safe_add_func_uint32_t_u_u(((void*)0 == (**g_1813)), (((**g_918) = (void*)0) == ((l_3075 < ((*p_18) != (((safe_sub_func_uint32_t_u_u(((((*l_2956) = (safe_sub_func_float_f_f((g_3104 = ((**g_2918) , 0x2.Cp+1)), (-0x7.Fp-1)))) , (-9L)) && l_3105), (*p_18))) && (*l_2950)) == (****g_863)))) , (void*)0)))) , p_16))), p_16)), l_2938))) != (**g_865)), p_19)))) , p_17)));
                }
                p_18 = (void*)0;
            }
            if ((((safe_div_func_int16_t_s_s(((1UL < (*g_1200)) != (g_2844[1][4] ^ (safe_sub_func_uint32_t_u_u(p_17, 1L)))), (((((*g_1107) &= (safe_mul_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((safe_sub_func_int32_t_s_s((((safe_lshift_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s(((safe_div_func_uint64_t_u_u((p_20 ^ (**g_2918)), (safe_add_func_uint32_t_u_u((**g_347), (safe_unary_minus_func_uint16_t_u(((1L || p_17) != (*g_2919)))))))) ^ l_2908), 0L)) , 0x86L), 1)) , l_3073) >= p_17), p_20)), 0x062EA73CL)), p_16))) != 0xA7AFL) && 0x3BFF26BEEC2E0B5DLL) , p_19))) ^ 0xBBL) , l_3127))
            { /* block id: 1398 */
                for (l_2321 = 0; (l_2321 <= 3); l_2321 += 1)
                { /* block id: 1401 */
                    ++l_3128;
                    return (*l_2951);
                }
            }
            else
            { /* block id: 1405 */
                int64_t l_3149[5] = {3L,3L,3L,3L,3L};
                int32_t *l_3152 = &l_2321;
                uint8_t *l_3184 = &l_2886[2];
                uint32_t ** const l_3207 = &g_888;
                uint64_t l_3208 = 18446744073709551615UL;
                int32_t **l_3247 = &g_3058;
                int32_t l_3303 = 0L;
                uint64_t ****l_3321 = &l_3201;
                int64_t l_3325[3][7][4] = {{{0L,(-6L),(-5L),0L},{0x2B8B5C538DDAC156LL,0x19AC9BB751FB3309LL,0x4E8E0BA9E2C621BBLL,0xD9B4FFF12458C1D8LL},{0x52CD016B695C532FLL,0x4E8E0BA9E2C621BBLL,0xB543227953C0EF12LL,0L},{0L,(-7L),0xF16B052C97003043LL,0xE52CA267534369B0LL},{0x0C84B110BAEE51F8LL,0x9176B48AFCB5D6ACLL,8L,8L},{(-9L),(-9L),0x646EC8DCCEA53709LL,1L},{8L,(-1L),0xDBC97CF997A1CB41LL,(-1L)}},{{0L,0x33FE23D4E089D9CALL,(-5L),0xDBC97CF997A1CB41LL},{0xBFD6C9BD28C81796LL,0x33FE23D4E089D9CALL,0x9176B48AFCB5D6ACLL,(-1L)},{0x33FE23D4E089D9CALL,(-1L),0x786CC1590F38264ELL,1L},{1L,(-9L),0xBFD6C9BD28C81796LL,8L},{0xF16B052C97003043LL,0x9176B48AFCB5D6ACLL,0x0F2D7D416B6169A7LL,0xE52CA267534369B0LL},{0x646EC8DCCEA53709LL,(-7L),0x7D228CE4B1E7C85DLL,0L},{0L,0x4E8E0BA9E2C621BBLL,1L,0xD9B4FFF12458C1D8LL}},{{1L,0x19AC9BB751FB3309LL,0x5E3C49716521E896LL,0L},{1L,(-6L),1L,0L},{0x0F2D7D416B6169A7LL,9L,(-9L),0x9176B48AFCB5D6ACLL},{(-1L),0xD9B4FFF12458C1D8LL,0xCCF8823407422CA7LL,9L},{0x19AC9BB751FB3309LL,(-5L),0xCCF8823407422CA7LL,0x52CD016B695C532FLL},{(-1L),0xF16B052C97003043LL,(-9L),0x7D228CE4B1E7C85DLL},{0x0F2D7D416B6169A7LL,0x0C84B110BAEE51F8LL,1L,0L}}};
                int16_t ***l_3329 = (void*)0;
                int i, j, k;
                for (g_556 = 0; (g_556 >= (-19)); g_556--)
                { /* block id: 1408 */
                    float l_3142 = 0x1.Cp+1;
                    int32_t *l_3167 = &l_2051;
                    uint32_t l_3187 = 0x057EAF0CL;
                    const uint16_t l_3188[3][7] = {{0x9E5BL,0x1C22L,0x1C22L,0x9E5BL,0xD03AL,6UL,0xD03AL},{0x9E5BL,0x1C22L,0x1C22L,0x9E5BL,0xD03AL,6UL,0xD03AL},{0x9E5BL,0x1C22L,0x1C22L,0x9E5BL,0xD03AL,6UL,0xD03AL}};
                    uint16_t *l_3189[5] = {&l_2258,&l_2258,&l_2258,&l_2258,&l_2258};
                    float *l_3190 = &g_1860[0][3][3];
                    int32_t l_3242 = (-2L);
                    const uint8_t ***l_3251 = &g_2750;
                    const uint8_t ****l_3250 = &l_3251;
                    int64_t l_3279 = 2L;
                    int i, j;
                }
                for (g_137 = 0; (g_137 > (-19)); g_137--)
                { /* block id: 1476 */
                    int64_t l_3288 = 0x4DC07409A9395E35LL;
                    int32_t l_3289 = 0xE7D2C775L;
                    uint16_t l_3290 = 0x5291L;
                    int32_t l_3304[3];
                    int32_t l_3314 = 1L;
                    uint32_t l_3315 = 0UL;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_3304[i] = 0L;
                    for (g_738 = 0; (g_738 <= 28); g_738 = safe_add_func_uint8_t_u_u(g_738, 3))
                    { /* block id: 1479 */
                        int64_t l_3287 = 0xAC677F35FF8EAEB5LL;
                        float *l_3296 = &l_2986;
                        --l_3290;
                        (*l_2949) &= g_3293;
                        (***g_1813) = (p_20 , (((*l_3296) = ((*l_2694) = (safe_mul_func_float_f_f(p_16, (***g_1813))))) < ((p_19 <= ((safe_sub_func_float_f_f(p_16, (p_18 == p_18))) < 0x4.538449p+65)) >= l_3287)));
                    }
                    for (p_17 = (-19); (p_17 != 7); ++p_17)
                    { /* block id: 1488 */
                        int64_t l_3301 = 0x03C7B0EB6B66E7ADLL;
                        int32_t l_3302[3][3] = {{0xA9FD5565L,0xF0DE70FFL,0xA9FD5565L},{0x9637B163L,0x9637B163L,0x9637B163L},{0xA9FD5565L,0xF0DE70FFL,0xA9FD5565L}};
                        uint8_t l_3305 = 3UL;
                        int i, j;
                        l_3305--;
                    }
                    if (((*l_3152) = (*l_3152)))
                    { /* block id: 1492 */
                        uint32_t *l_3316 = &l_3315;
                        uint64_t *****l_3322 = &l_3321;
                        uint64_t *****l_3323 = &l_3200;
                        int16_t ***l_3328[9][4] = {{&l_3186,&l_3185,(void*)0,(void*)0},{&l_3185,&l_3185,(void*)0,&l_3186},{(void*)0,&l_3186,&l_3186,&l_3186},{(void*)0,&l_3185,&l_3186,(void*)0},{&g_1199,&l_3185,&l_3186,&l_3185},{&l_3186,(void*)0,(void*)0,(void*)0},{&l_3186,&l_3186,&l_3186,&l_3186},{&g_1199,(void*)0,&l_3186,&l_3185},{(void*)0,&l_3185,&l_3186,&l_3186}};
                        int i, j;
                        l_3153 = (((*l_3152) < ((p_19 > 0x26A612476BC960A9LL) , ((safe_mul_func_int16_t_s_s((safe_div_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(l_3314, (((((*l_3316) = (l_3315 <= p_16)) , 0x6234L) ^ (((g_3317 != (l_3324 = ((*l_3323) = ((*l_3322) = l_3321)))) > (*g_2919)) & (***g_1105))) > (*l_2950)))) , (***g_2749)), l_3325[2][2][2])), (-4L))) & (*g_1000)))) , (*g_2352));
                        if ((*l_3152))
                            break;
                        (***l_2692) = (safe_sub_func_float_f_f(p_19, ((&l_3185 == (l_3329 = (((**l_2613) = 0x458585BA3CA3656FLL) , l_3328[3][2]))) == (safe_div_func_float_f_f((safe_sub_func_float_f_f((*l_3152), (safe_mul_func_float_f_f(((*g_809) > ((*l_3152) == (!(safe_sub_func_float_f_f((+(p_20 , (g_1085 == p_17))), p_19))))), g_27)))), p_16)))));
                    }
                    else
                    { /* block id: 1502 */
                        p_18 = p_18;
                    }
                    for (l_2039 = 0; (l_2039 <= 4); l_2039 += 1)
                    { /* block id: 1507 */
                        int i, j;
                        if (g_849[(l_2039 + 5)][l_2039])
                            break;
                        (*l_2949) ^= ((*l_3152) >= ((*l_2953) = 2UL));
                        if ((*l_2955))
                            break;
                    }
                }
                (*g_2902) = (*g_2902);
                (*g_3341) = p_18;
            }
        }
        else
        { /* block id: 1517 */
            int16_t l_3342 = (-6L);
            int32_t l_3351 = 0xBCA4522BL;
            int32_t l_3352 = 0x78C0234DL;
            int32_t l_3353 = 0x19E62F0FL;
            int32_t l_3354 = 0x6F5D1A09L;
            int32_t l_3355 = 0L;
            int32_t l_3357 = 0x00BD2887L;
            int32_t l_3362 = (-2L);
            int32_t l_3363 = 0xEA2C127BL;
            int32_t l_3364[2][1];
            uint64_t *****l_3383[7] = {&l_3380[6],&l_3380[6],&l_3380[6],&l_3380[6],&l_3380[6],&l_3380[6],&l_3380[6]};
            int32_t l_3436 = 0x29810673L;
            uint32_t **l_3627[3][5] = {{&g_888,&g_888,&g_888,&g_888,&g_888},{&g_888,&g_888,&g_888,&g_888,&g_888},{&g_888,&g_888,&g_888,&g_888,&g_888}};
            int32_t ***l_3646 = &g_3057;
            int8_t l_3781[6] = {1L,0x61L,1L,1L,0x61L,1L};
            const uint32_t *l_3799 = &l_2835[1][1];
            const uint32_t **l_3798[6][4] = {{&l_3799,&l_3799,&l_3799,&l_3799},{&l_3799,&l_3799,&l_3799,&l_3799},{&l_3799,&l_3799,&l_3799,&l_3799},{&l_3799,&l_3799,&l_3799,&l_3799},{&l_3799,&l_3799,&l_3799,&l_3799},{&l_3799,&l_3799,&l_3799,&l_3799}};
            uint32_t * const *l_3800 = &g_348;
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                    l_3364[i][j] = 1L;
            }
        }
        l_2056 &= (l_2041 = (((void*)0 == &g_1199) & ((safe_sub_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(0x39A4L, (safe_rshift_func_uint16_t_u_u((safe_div_func_uint32_t_u_u((p_19 = (safe_add_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(((safe_add_func_int64_t_s_s(((**l_2613) = (safe_sub_func_uint64_t_u_u(((*g_1572) |= (p_20 ^ (p_20 && 1L))), 0x089E32DC6459CE11LL))), ((safe_add_func_uint8_t_u_u(((**g_265) = (safe_div_func_int8_t_s_s(((((*g_1046) , (l_2054 = ((((*g_3689) >= (*g_3809)) & p_19) >= p_17))) & p_16) , l_3853[2][0]), 0x59L))), p_20)) && 0xE0376C06L))) ^ 0UL), (-3L))), (***g_1105)))), p_17)), 14)))), p_16)) && (**g_1571))));
    }
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_1114 g_1571 g_1572 g_252 g_1000 g_940 g_593 g_75 g_1627 g_920 g_265 g_131 g_347 g_348 g_918 g_919 g_1199 g_1200 g_132 g_1107 g_865 g_1046 g_327 g_1086 g_137 g_835 g_248 g_1626 g_1106 g_1027 g_655 g_43 g_76 g_1757 g_1760 g_1767 g_1770 g_1771 g_1789 g_1793 g_1725 g_711 g_1447 g_1813 g_1758 g_1814 g_1815 g_863 g_864 g_404 g_255 g_1105 g_557 g_23 g_1920 g_1937 g_1417 g_1941 g_141 g_1960 g_31 g_974 g_1045 g_556 g_272 g_27 g_4
 * writes: g_4 g_31 g_132 g_33 g_27 g_23 g_1696 g_141 g_1027 g_43 g_1725 g_835 g_251 g_75 g_1757 g_1767 g_1771 g_252 g_1789 g_1793 g_137 g_738 g_940 g_1417 g_255 g_1772 g_1987 g_1988 g_1989 g_586 g_131 g_160
 */
static uint64_t  func_47(uint32_t * p_48, uint16_t  p_49, uint64_t  p_50)
{ /* block id: 24 */
    int32_t l_91 = 0L;
    int32_t l_1599 = 0x66EC8901L;
    int32_t l_1604[2][8][7] = {{{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL}},{{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),(-9L),(-1L),0L,0x3635BADCL},{0x3635BADCL,0L,(-1L),0x3315A2D2L,(-6L),0x3635BADCL,(-9L)},{(-9L),0x3635BADCL,(-6L),0x3315A2D2L,(-6L),0x3635BADCL,(-9L)},{(-9L),0x3635BADCL,(-6L),0x3315A2D2L,(-6L),0x3635BADCL,(-9L)}}};
    const int32_t *l_1628[8] = {&l_1604[1][1][4],&l_1604[1][1][4],&l_1604[1][1][4],&l_1604[1][1][4],&l_1604[1][1][4],&l_1604[1][1][4],&l_1604[1][1][4],&l_1604[1][1][4]};
    uint8_t * const *l_1629 = (void*)0;
    int8_t l_1702 = 0xD9L;
    uint32_t ****l_1726 = (void*)0;
    float l_1868 = (-0x4.8p-1);
    int8_t l_1877 = (-1L);
    int64_t *l_1967 = &g_160;
    int32_t *l_1977 = &l_1604[1][1][4];
    uint32_t **l_1985 = &g_888;
    uint32_t ***l_1986[4] = {&l_1985,&l_1985,&l_1985,&l_1985};
    uint8_t *****l_1990 = &g_919;
    uint64_t *l_2002 = &g_252;
    uint64_t ** const l_2001[1][10][4] = {{{&l_2002,&l_2002,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_2002,&l_2002,&l_2002,(void*)0},{(void*)0,&l_2002,&l_2002,(void*)0},{&l_2002,(void*)0,&l_2002,(void*)0},{&l_2002,&l_2002,&l_2002,&l_2002},{(void*)0,(void*)0,&l_2002,&l_2002},{&l_2002,&l_2002,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_2002,&l_2002,&l_2002,(void*)0}}};
    uint8_t *l_2003 = (void*)0;
    uint8_t *l_2009 = &g_132;
    int i, j, k;
    for (p_49 = (-23); (p_49 != 45); ++p_49)
    { /* block id: 27 */
        const int32_t l_92[9] = {0x0913F082L,0x0913F082L,0x0913F082L,0x0913F082L,0x0913F082L,0x0913F082L,0x0913F082L,0x0913F082L,0x0913F082L};
        int32_t l_1584[6] = {0x63909544L,0xB7281836L,0xB7281836L,0x63909544L,0xB7281836L,0xB7281836L};
        int32_t l_1600 = 0xFF95EC54L;
        int32_t l_1601 = 0x38B555C8L;
        uint8_t **l_1630[3];
        int32_t l_1648 = 0xA4681830L;
        int32_t l_1651 = 1L;
        int32_t l_1654 = 0L;
        int32_t l_1655 = 0L;
        int32_t l_1658 = 0x46CBAB98L;
        int32_t l_1660 = 6L;
        int8_t l_1661 = (-2L);
        int32_t l_1662 = 0xCF03621CL;
        int32_t l_1663 = 0xB9DA3295L;
        int32_t l_1664 = 5L;
        int32_t l_1665 = 0x64C59BF2L;
        int32_t l_1666 = (-1L);
        int32_t l_1667[8] = {0L,0L,0L,0L,0L,0L,0L,0L};
        float *l_1691 = &g_27;
        float **l_1690 = &l_1691;
        uint8_t ****l_1715 = (void*)0;
        uint8_t *** const *l_1716[5];
        int64_t *l_1722 = &g_43[5][2][1];
        uint32_t **l_1750[3];
        int64_t l_1754 = 0x5CF9F7DB78C17185LL;
        float l_1787 = 0x0.AB0B39p-50;
        int16_t ** const *l_1794 = &g_1199;
        int32_t l_1866 = (-4L);
        uint64_t l_1921[8][1][5] = {{{1UL,0xE0C9EBC1A994DB33LL,0xA0C916E0FFAC43ACLL,0xDD70E9447F3E1967LL,4UL}},{{1UL,0xCD5A7BD399D01D4FLL,0xCD5A7BD399D01D4FLL,1UL,2UL}},{{1UL,0xE0C9EBC1A994DB33LL,0xA0C916E0FFAC43ACLL,0xDD70E9447F3E1967LL,4UL}},{{1UL,0xCD5A7BD399D01D4FLL,0xCD5A7BD399D01D4FLL,1UL,2UL}},{{1UL,0xE0C9EBC1A994DB33LL,0xA0C916E0FFAC43ACLL,0xDD70E9447F3E1967LL,4UL}},{{1UL,0xCD5A7BD399D01D4FLL,0xCD5A7BD399D01D4FLL,1UL,2UL}},{{1UL,0xE0C9EBC1A994DB33LL,0xA0C916E0FFAC43ACLL,0xDD70E9447F3E1967LL,4UL}},{{1UL,0xCD5A7BD399D01D4FLL,0xCD5A7BD399D01D4FLL,1UL,2UL}}};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1630[i] = &g_131;
        for (i = 0; i < 5; i++)
            l_1716[i] = &g_920;
        for (i = 0; i < 3; i++)
            l_1750[i] = (void*)0;
        for (p_50 = 0; (p_50 <= 3); p_50 += 1)
        { /* block id: 30 */
            int32_t **l_55[5][4] = {{&g_33,&g_33,&g_33,&g_33},{&g_33,&g_33,(void*)0,&g_33},{&g_33,(void*)0,&g_33,&g_33},{&g_33,&g_33,&g_33,&g_33},{(void*)0,(void*)0,&g_33,&g_33}};
            int32_t *l_56 = &g_4;
            uint32_t l_1668 = 8UL;
            uint8_t *l_1674 = &g_31;
            float * const *l_1693 = &l_1691;
            int i, j;
            l_56 = (void*)0;
            for (g_4 = 3; (g_4 >= 0); g_4 -= 1)
            { /* block id: 34 */
                uint8_t *l_90[7][8][3];
                int32_t l_1446 = 0xA39BBCFCL;
                int32_t l_1605 = 0x0A905CC3L;
                uint32_t l_1606 = 0xB187BDD8L;
                int32_t *l_1633 = &l_1605;
                int32_t l_1646 = 0xBDAA025AL;
                int32_t l_1656[3][9] = {{0x2A10C82FL,0xAF01BD41L,0xAF01BD41L,0x2A10C82FL,1L,0x2A10C82FL,0xAF01BD41L,0xAF01BD41L,0x2A10C82FL},{0x0EC16832L,0xAF01BD41L,0x4DE29E64L,0xAF01BD41L,0x0EC16832L,0x0EC16832L,0xAF01BD41L,0x4DE29E64L,0xAF01BD41L},{0xAF01BD41L,1L,0x4DE29E64L,0x4DE29E64L,1L,0xAF01BD41L,1L,0x4DE29E64L,0x4DE29E64L}};
                int i, j, k;
                for (i = 0; i < 7; i++)
                {
                    for (j = 0; j < 8; j++)
                    {
                        for (k = 0; k < 3; k++)
                            l_90[i][j][k] = &g_31;
                    }
                }
                for (g_31 = 0; (g_31 <= 3); g_31 += 1)
                { /* block id: 37 */
                    uint32_t l_80 = 2UL;
                    int32_t *l_1418[9][4] = {{&g_255,&g_738,&g_255,(void*)0},{&g_835,&g_95,(void*)0,&g_95},{&g_95,&g_835,(void*)0,&g_255},{&g_95,(void*)0,&g_835,&g_95},{&g_835,&g_835,&g_835,&g_835},{(void*)0,&g_95,&g_255,&g_95},{&g_255,&g_95,&g_255,(void*)0},{&g_255,&g_255,&g_835,(void*)0},{&g_255,&g_95,&g_255,&g_95}};
                    int64_t l_1598 = 0xF1F78EA5B13C5B10LL;
                    uint32_t l_1636 = 0xD75A02E4L;
                    int i, j, k;
                }
                for (g_132 = (-3); (g_132 >= 58); g_132 = safe_add_func_int32_t_s_s(g_132, 1))
                { /* block id: 775 */
                    (*g_1114) = &l_1600;
                    return (**g_1571);
                }
                for (l_1446 = 0; (l_1446 < (-1)); l_1446 = safe_sub_func_uint64_t_u_u(l_1446, 1))
                { /* block id: 781 */
                    int64_t l_1653[5];
                    int32_t l_1657 = 0L;
                    int32_t l_1659[8] = {0xD92B10BCL,0xD92B10BCL,0xD92B10BCL,0xD92B10BCL,0xD92B10BCL,0xD92B10BCL,0xD92B10BCL,0xD92B10BCL};
                    int32_t l_1694 = 0x5967CE3CL;
                    int32_t l_1695 = (-3L);
                    int i;
                    for (i = 0; i < 5; i++)
                        l_1653[i] = 0x7EBF81B6D82D98B6LL;
                    for (l_1600 = 0; (l_1600 <= 3); l_1600 += 1)
                    { /* block id: 784 */
                        float l_1643 = 0xA.A1D76Ep-93;
                        int32_t l_1644 = 0xEE26A2F2L;
                        int32_t l_1645 = 1L;
                        int32_t l_1647 = 0x5A95C0FEL;
                        int32_t l_1649 = 0xAAC1E503L;
                        int32_t l_1650 = 0L;
                        int32_t l_1652[1];
                        float ***l_1692[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        uint32_t *l_1697 = &l_1668;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1652[i] = (-1L);
                        ++l_1668;
                        l_1659[6] = (safe_sub_func_uint32_t_u_u(l_1659[2], ((*l_1697) = (g_1696 = ((**g_347) = (safe_unary_minus_func_uint8_t_u((((void*)0 == l_1674) && (safe_rshift_func_uint16_t_u_s((p_50 >= (((safe_rshift_func_uint16_t_u_s((safe_add_func_uint8_t_u_u(((***g_920) = ((((l_1695 = (l_1694 = (l_1657 = ((-((safe_add_func_float_f_f(0xC.6D91FCp+38, ((*g_1000) , (safe_add_func_float_f_f((*l_1633), (safe_mul_func_float_f_f((l_1604[0][2][6] = (*g_593)), ((*l_1691) = ((l_1690 = l_1690) == l_1693))))))))) > (-0x7.Dp-1))) >= p_49)))) >= g_1627) , 1L) , 0x9CL)), 8UL)), p_49)) > 4L) <= p_50)), 4))))))))));
                    }
                }
            }
        }
        if (((((safe_mul_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(p_49, l_1702)), (safe_sub_func_int64_t_s_s(((*l_1722) = ((safe_div_func_int16_t_s_s(((((l_1654 > ((*g_1107) = (((safe_mod_func_int64_t_s_s((safe_div_func_uint32_t_u_u(((((safe_mul_func_int8_t_s_s((l_1667[5] <= ((0xA2ADL != ((**g_1199) = (safe_mul_func_int16_t_s_s(((((l_1715 = l_1715) != l_1716[3]) <= (((safe_mod_func_uint8_t_u_u(((*****g_918) = ((safe_add_func_uint16_t_u_u((+(0x3AL >= 0xA5L)), 0x84E9L)) == p_50)), 1L)) ^ 3UL) && 7UL)) == p_50), p_50)))) , (**g_265))), (*g_1000))) > p_50) & 0UL) & p_50), p_50)), 0xD336073C609D0034LL)) , 0x12D94DEFEEDAC6D5LL) <= 0x2F88DEE5F0D4CCC5LL))) , (*g_865)) != (**g_920)) <= p_49), 0xE9ECL)) ^ (*g_1046))), l_1601)))) & p_49) || 7UL) || p_50))
        { /* block id: 806 */
            uint32_t ****l_1723 = &g_711;
            uint32_t *****l_1724[6][4][10] = {{{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723}},{{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723}},{{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723}},{{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723}},{{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723}},{{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{(void*)0,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723},{&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723,&l_1723},{&l_1723,&l_1723,(void*)0,&l_1723,(void*)0,&l_1723,&l_1723,&l_1723,(void*)0,&l_1723}}};
            int32_t *l_1727 = &g_835;
            uint32_t **l_1749 = &g_348;
            uint8_t ** const l_1806 = &g_131;
            int32_t l_1852 = 1L;
            int32_t l_1854 = 1L;
            int32_t l_1855 = (-1L);
            int32_t l_1856 = 0x190A1666L;
            int32_t l_1858 = 0xD24E259EL;
            int32_t l_1861[10] = {(-3L),0x787EC0F2L,0x787EC0F2L,(-3L),0x53E5F97FL,(-3L),0x787EC0F2L,0x787EC0F2L,(-3L),0x53E5F97FL};
            const uint8_t l_1898 = 0xCAL;
            int32_t *l_1919 = &l_1667[0];
            int i, j, k;
            l_1726 = (g_1725 = l_1723);
            (*l_1727) ^= (*g_1086);
            if (((*g_1000) ^ ((!((!((*g_1572) != (safe_mul_func_uint16_t_u_u(((!(((*g_1000) != 0x92L) != 65535UL)) , g_248[0][2]), ((((((safe_lshift_func_int16_t_s_u((safe_add_func_int8_t_s_s((((safe_add_func_uint64_t_u_u(18446744073709551615UL, l_1667[6])) | (p_49 , 0x353EL)) >= 0x8FCB501F31350E1ELL), p_50)), 11)) || (*g_1626)) != (*g_1572)) , p_50) < (-1L)) ^ 18446744073709551610UL))))) > 0xFFFBA33BL)) >= (**g_1106))))
            { /* block id: 810 */
                float *l_1751 = &g_251;
                float *l_1752 = &g_75[1][6];
                uint64_t *l_1753 = &g_252;
                l_1754 ^= (safe_mul_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(p_50, ((((g_655 , ((*l_1752) = (safe_add_func_float_f_f(((-0x4.Fp-1) > ((*g_593) >= p_50)), (-(-((safe_mul_func_float_f_f(((*l_1691) = (l_1749 != (l_1750[1] = &p_48))), (g_43[0][3][2] <= ((*l_1751) = g_940[3])))) >= 0xA.16ADAEp-77))))))) , p_50) , &p_50) == l_1753))) < g_76), 0xF5L));
            }
            else
            { /* block id: 816 */
                uint32_t * const ***l_1769 = &g_1767;
                uint32_t ****l_1773 = &g_1771;
                uint32_t ***l_1775 = &g_1772;
                uint32_t ****l_1774 = &l_1775;
                uint32_t ****l_1776 = (void*)0;
                uint32_t ***l_1778 = (void*)0;
                uint32_t ****l_1777 = &l_1778;
                uint64_t *l_1788 = &g_1789;
                uint64_t *l_1792 = &g_1793;
                int32_t l_1802 = 6L;
                int32_t l_1846 = (-10L);
                int32_t l_1848 = (-1L);
                uint16_t l_1849 = 65530UL;
                int32_t l_1853[2];
                int32_t l_1857 = 0xAFE56A71L;
                int32_t l_1859 = 0x70D7F799L;
                int32_t l_1862 = 1L;
                int32_t l_1863 = 0xC66A14D9L;
                int32_t l_1864[9][10] = {{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L}};
                int32_t l_1867 = 1L;
                uint64_t l_1869 = 0x14B687950CE375CFLL;
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1853[i] = (-4L);
                for (p_50 = 0; (p_50 >= 22); p_50 = safe_add_func_uint16_t_u_u(p_50, 1))
                { /* block id: 819 */
                    if (p_50)
                        break;
                }
                (*g_1760) = g_1757;
                if ((safe_lshift_func_int16_t_s_u((safe_sub_func_int8_t_s_s((safe_mod_func_int16_t_s_s((((((*l_1769) = g_1767) != (((g_1770 | (((((((*l_1777) = ((*l_1774) = ((*l_1773) = g_1771))) == &g_1758) >= (((**l_1690) = ((p_49 || (safe_div_func_int8_t_s_s(((((*l_1792) |= (((safe_div_func_uint64_t_u_u((++(*g_1572)), (safe_rshift_func_int8_t_s_u((((g_137 && (-1L)) | (((l_1654 , (((*l_1788)--) <= 0x84B3F33F66BE1773LL)) , 0xB4L) > p_49)) ^ p_50), 3)))) & (*l_1727)) , (**g_1571))) , l_1794) != &g_1199), (*g_1000)))) , 0xD.9759AEp-81)) <= 0x0.5p+1)) == (*l_1727)) >= p_49) , (*g_1107))) < l_1754) , (*g_1725))) >= (*g_1000)) <= (-1L)), (-2L))), 0x0CL)), g_1447)))
                { /* block id: 831 */
                    uint8_t l_1816[4][2] = {{253UL,1UL},{253UL,1UL},{253UL,1UL},{253UL,1UL}};
                    int32_t *l_1817 = (void*)0;
                    int32_t *l_1818 = &g_137;
                    int32_t *l_1850 = (void*)0;
                    int32_t *l_1851[7] = {&l_1667[2],&l_1667[2],&l_1667[2],&l_1667[2],&l_1667[2],&l_1667[2],&l_1667[2]};
                    int8_t l_1865 = 0x4FL;
                    int i, j;
                    (*l_1818) &= (safe_unary_minus_func_int8_t_s((safe_rshift_func_int16_t_s_s((((((safe_add_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u(l_1802, 11)) , ((+((*l_1727) |= (safe_sub_func_int64_t_s_s((l_1806 != (*g_920)), (safe_lshift_func_int8_t_s_u(((p_50 , ((safe_rshift_func_uint8_t_u_s(p_49, (safe_lshift_func_uint16_t_u_u(0xD815L, 13)))) > ((6UL >= (g_1813 == (void*)0)) == l_1654))) || 0xBCL), l_1816[2][0])))))) & 6L)), 0xC3L)) ^ p_50) , (*g_1757)) == (void*)0) == (*g_1000)), l_1802))));
                    for (g_23 = 0; (g_23 <= 8); g_23 += 1)
                    { /* block id: 836 */
                        const uint8_t l_1826 = 0xC7L;
                        uint16_t *l_1847[1][2][9] = {{{(void*)0,&g_1085,&g_974,(void*)0,(void*)0,&g_974,&g_1085,(void*)0,&g_974},{(void*)0,&g_1085,&g_974,(void*)0,(void*)0,&g_974,&g_1085,(void*)0,&g_974}}};
                        int i, j, k;
                        l_1849 = (!((safe_lshift_func_int8_t_s_u((((l_1599 |= ((safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((l_1826 == l_1802) , p_49), (!((safe_sub_func_int32_t_s_s(((l_1848 = (safe_mod_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((((*g_1572) = 18446744073709551615UL) >= (((((*l_1722) = (l_1846 = (safe_lshift_func_uint8_t_u_u((((**g_1106) = 0x4DCEL) , (0xA8L == (safe_lshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s(((l_1648 = ((safe_add_func_int64_t_s_s(((((safe_div_func_int8_t_s_s(((p_50 | (18446744073709551612UL <= (~(+(l_1628[1] == (*g_1814)))))) && 0x9111L), (****g_863))) , p_50) <= 1UL) < 0xC9L), l_1665)) && (*l_1727))) , (-1L)), (**g_1106))), l_1802)))), (****g_863))))) < 0xA1F9F91F830F5085LL) >= p_49) == 0x5021880BDEF1F619LL)), 3)), 0x6AD8L))) ^ 0x5D06L), p_50)) | g_1793)))), 0x07FAL)) > (*g_131))) , 1UL) >= l_1802), 4)) , 0x1.9p-1));
                        (*l_1818) &= 0x95F1CE61L;
                        if (p_50)
                            break;
                    }
                    if (p_49)
                        continue;
                    l_1869--;
                }
                else
                { /* block id: 850 */
                    uint64_t l_1876[10] = {0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL,0xA79DD844C763CB0DLL};
                    int32_t l_1899[1];
                    int64_t l_1900 = 3L;
                    int32_t l_1902 = 0x0298F8C7L;
                    int16_t l_1903 = 0x516AL;
                    uint8_t l_1904[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1899[i] = (-10L);
                    for (i = 0; i < 1; i++)
                        l_1904[i] = 0UL;
                    for (g_738 = (-30); (g_738 < 26); ++g_738)
                    { /* block id: 853 */
                        int32_t **l_1890 = &l_1727;
                        int32_t *l_1901[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        l_1856 = (l_1902 |= (((**g_347) = (safe_rshift_func_int16_t_s_s(l_1876[1], (((((***g_864) = l_1877) <= (0xA1AAFE79L >= (((*g_1000) = (safe_add_func_uint32_t_u_u(((l_1663 = (safe_mod_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_s(l_1665, 4)) , (l_1604[1][1][4] = (((safe_rshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((l_1899[0] = (((((safe_add_func_int32_t_s_s(((((((*l_1890) = &l_1861[2]) != p_48) | (safe_rshift_func_int16_t_s_s(((***g_1105) = (l_1876[3] < ((0x2CE7E70DL < (safe_mul_func_int8_t_s_s((((((safe_mul_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s(l_1898)), p_50)) < 0x2CL) <= 1UL) == 9UL) & (*g_404)), p_49))) , (*g_1572)))), p_50))) <= 0xB744A9677141B694LL) , (-1L)), p_49)) , (void*)0) != (void*)0) , g_1447) ^ 0x82DAL)), p_50)), l_1869)) || l_1754) != l_1900))), p_50))) == l_1876[1]), l_1661))) || l_1900))) && (*g_1046)) & p_50)))) | 0xC2B0FD75L));
                        (*l_1890) = p_48;
                        ++l_1904[0];
                        (*l_1890) = (void*)0;
                    }
                }
            }
            (*g_1920) = (((((safe_sub_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(255UL, p_50)), (safe_rshift_func_int16_t_s_u(((safe_add_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((&p_48 == &p_48), ((*l_1919) = (((((((((((safe_mod_func_int32_t_s_s((0x0D665625DEF34BD8LL == (*g_1046)), l_1861[2])) || (p_50 == ((((((*g_1814) != (*l_1690)) != p_49) , 0x2194L) == p_50) != 0x5FC032A6L))) & p_49) || p_50) >= g_557) >= p_50) , (**g_347)) != (-1L)) , 0x68L) <= (**g_265)) & 0xEA895B7939E4AE0ELL)))), 8L)) > 0x3D6D0475L), p_49)))) == 5UL) >= 4UL) <= l_1658) , &l_1604[1][1][4]);
        }
        else
        { /* block id: 872 */
            uint8_t l_1925 = 0x63L;
            int32_t l_1933 = (-7L);
            uint32_t l_1969 = 0UL;
            if (((l_1921[4][0][2] ^ 0x45533A851B7AF932LL) | (-1L)))
            { /* block id: 873 */
                uint32_t *** const l_1932 = &g_347;
                (*g_1937) &= (safe_mul_func_uint16_t_u_u((+l_1925), (safe_sub_func_uint8_t_u_u(p_49, (safe_mod_func_uint8_t_u_u((((safe_rshift_func_uint16_t_u_s(0xB24DL, ((void*)0 != l_1932))) == l_1925) , ((*****g_918)++)), p_49))))));
                return p_49;
            }
            else
            { /* block id: 877 */
                uint8_t *l_1957 = &g_31;
                int32_t l_1958 = 0xB7ED7240L;
                uint32_t ****l_1959 = (void*)0;
                int32_t *l_1968 = &l_1667[1];
                for (g_1417 = 0; (g_1417 == 9); g_1417 = safe_add_func_int64_t_s_s(g_1417, 7))
                { /* block id: 880 */
                    int32_t *l_1940 = &l_1664;
                    (*g_1941) = l_1940;
                }
                (*g_1960) &= (safe_rshift_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s(p_49, (((&g_360[0][7][3] == l_1726) <= (((**g_1106) |= (safe_sub_func_int8_t_s_s(((18446744073709551615UL ^ p_50) > p_50), (+(safe_sub_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((****g_863), (safe_lshift_func_int16_t_s_s(((g_835 , ((safe_rshift_func_uint8_t_u_s((&l_1925 != l_1957), l_1958)) , l_1959)) != (void*)0), (*g_1200))))) >= l_92[3]), 0x63E2L)))))) , l_1660)) & 0xD6E1329DA93F5705LL))) & p_50), 4));
                (*l_1968) = (l_1958 = (safe_rshift_func_int16_t_s_u(((***l_1794) = ((safe_rshift_func_int8_t_s_u(p_50, ((*l_1957)--))) && 18446744073709551607UL)), (&l_1754 == l_1967))));
            }
            if (l_1969)
                continue;
            return (**g_1571);
        }
    }
    (*l_1977) = ((*g_1000) , (safe_sub_func_int16_t_s_s((safe_lshift_func_int16_t_s_u((g_974 , (~(((0L <= (safe_rshift_func_uint8_t_u_u(1UL, (p_50 != 0x02519CA6L)))) || ((void*)0 == (**g_919))) == (p_49 , (**g_1571))))), g_327)), p_49)));
    (***g_1813) = ((*l_1977) <= ((((((safe_sub_func_float_f_f((+((safe_add_func_float_f_f(((((**g_347)--) , ((g_1987[5] = ((*g_1771) = l_1985)) == (g_1989[6] = (g_1988 = &g_888)))) , ((((g_655 != (((**g_1045) , (((*****g_918) , l_1990) == ((safe_lshift_func_uint8_t_u_s(p_49, 6)) , l_1990))) < g_556)) ^ (***g_1105)) & (***g_864)) , (*g_272))), 0x9.4p-1)) <= g_1793)), g_4)) == g_1627) < g_4) > 0xE.15BE33p+54) < g_255) > p_50));
    (*l_1977) = (safe_lshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s(0x4B9275C4L, (safe_rshift_func_uint16_t_u_u((((safe_add_func_uint32_t_u_u((&g_1572 != l_2001[0][9][1]), (((****l_1990) = (****l_1990)) != l_2003))) == ((((((((*l_1967) = (~((*l_1977) < ((safe_rshift_func_uint8_t_u_u(0x7EL, ((*l_2009) = ((safe_add_func_int16_t_s_s((((*g_1200) != p_49) == 0xB128FCEFL), 0xF507L)) > 0L)))) ^ 0UL)))) == (-2L)) > p_50) == 0xE996L) < (*g_1000)) >= 7UL) >= p_49)) & p_49), 9)))), 7));
    return (**g_1571);
}


/* ------------------------------------------ */
/* 
 * reads : g_160 g_226 g_864 g_865 g_131 g_31 g_132 g_1107 g_1027 g_1199 g_1200 g_1105 g_1106 g_719 g_27 g_95 g_593 g_272 g_1085 g_141 g_1000 g_940 g_43 g_974 g_75 g_857 g_712 g_1417 g_4 g_1447 g_1045 g_1046 g_327 g_835 g_863 g_738 g_904 g_33 g_1569 g_1571 g_1572 g_252 g_438 g_1577 g_1363
 * writes: g_33 g_160 g_226 g_1027 g_141 g_1199 g_75 g_1085 g_43 g_95 g_1417 g_557 g_835 g_28
 */
static uint16_t  func_59(int32_t * p_60, float * p_61, float * const  p_62)
{ /* block id: 688 */
    int32_t *l_1448 = (void*)0;
    int32_t **l_1449 = (void*)0;
    int32_t **l_1450 = (void*)0;
    int32_t **l_1451 = &g_33;
    int32_t l_1454 = 0xC5442EC4L;
    int32_t l_1455[4];
    int16_t l_1456[9][1] = {{0x1988L},{(-1L)},{0x1988L},{(-1L)},{0x1988L},{(-1L)},{0x1988L},{(-1L)},{0x1988L}};
    uint16_t * const l_1489 = &g_1085;
    int32_t l_1491[5][3] = {{1L,1L,1L},{1L,1L,0x8D890AF0L},{1L,1L,1L},{1L,1L,0x8D890AF0L},{1L,1L,1L}};
    int64_t *l_1545 = &g_556;
    int64_t **l_1544[10] = {&l_1545,(void*)0,&l_1545,(void*)0,&l_1545,(void*)0,&l_1545,(void*)0,&l_1545,(void*)0};
    uint32_t l_1547 = 4294967295UL;
    const uint64_t *l_1566 = &g_252;
    const uint64_t **l_1565 = &l_1566;
    float l_1570 = 0x0.Ap+1;
    int i, j;
    for (i = 0; i < 4; i++)
        l_1455[i] = 0x5E8048FDL;
    (*l_1451) = l_1448;
    for (g_160 = 2; (g_160 >= 0); g_160 -= 1)
    { /* block id: 692 */
        int32_t *l_1452 = (void*)0;
        int32_t *l_1453[8] = {&g_738,&g_4,&g_738,&g_4,&g_738,&g_4,&g_738,&g_4};
        uint16_t l_1457 = 0x8494L;
        int8_t **l_1486 = &g_1000;
        uint64_t *l_1568 = &g_252;
        uint64_t **l_1567 = &l_1568;
        int i;
        ++l_1457;
        for (g_226 = 0; (g_226 <= 2); g_226 += 1)
        { /* block id: 696 */
            float l_1466 = 0x1.Ap-1;
            const int32_t l_1483 = 0xB643BA5FL;
            uint8_t *****l_1508[3][1][1];
            int32_t *l_1515 = &g_1417;
            int32_t l_1531[3];
            uint64_t * const l_1575 = (void*)0;
            uint64_t * const *l_1574 = &l_1575;
            int i, j, k;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_1508[i][j][k] = &g_919;
                }
            }
            for (i = 0; i < 3; i++)
                l_1531[i] = (-1L);
            if (((safe_lshift_func_uint8_t_u_s((***g_864), 7)) || 0x7D1FDE11L))
            { /* block id: 697 */
                int16_t ***l_1473 = &g_1199;
                int32_t l_1484 = 0x11370250L;
                int64_t l_1485 = 2L;
                if ((safe_sub_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u((((**g_1199) = ((*g_1107) = (*g_1107))) , ((safe_add_func_uint32_t_u_u((0xA3D8L < (safe_div_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u((((void*)0 == (*g_1105)) , 65535UL), (((((*l_1473) = &g_1200) == (((safe_mul_func_float_f_f(((0x1.4p+1 == 0x0.Fp+1) < ((*g_593) = ((+(safe_sub_func_float_f_f((safe_div_func_float_f_f(((((((safe_add_func_float_f_f((*g_719), 0x7.07FE36p-90)) != 0x0.Cp+1) > l_1483) < l_1484) < 0x1.474B45p+6) > g_95), 0x7.Fp-1)), 0xA.916AF6p-72))) <= l_1485))), 0x1.4p-1)) > l_1483) , (*g_1105))) , (void*)0) != l_1486))), l_1483))), l_1484)) >= 8L)), l_1484)) >= l_1483) ^ l_1485), 0x1EL)))
                { /* block id: 702 */
                    int32_t l_1487[4];
                    int32_t l_1488 = 0x135130D1L;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1487[i] = 0L;
                    l_1488 |= l_1487[0];
                }
                else
                { /* block id: 704 */
                    uint32_t l_1490 = 4294967287UL;
                    uint32_t l_1512 = 1UL;
                    l_1484 = (l_1490 = (l_1489 != &l_1457));
                    if (l_1491[2][0])
                    { /* block id: 707 */
                        uint32_t l_1507 = 0UL;
                        int32_t **l_1514 = &l_1453[7];
                        (*l_1451) = ((0x0.6p-1 <= ((safe_add_func_float_f_f(((safe_div_func_float_f_f(((safe_sub_func_float_f_f((((*g_272) == (safe_mul_func_float_f_f((safe_div_func_float_f_f((((!0L) ^ (safe_sub_func_int16_t_s_s(((((*l_1489) &= g_226) <= (safe_rshift_func_uint16_t_u_u((l_1507 , (((((l_1508[0][0][0] != &g_863) > ((***l_1473) |= (safe_sub_func_int16_t_s_s((!1UL), (*g_1107))))) < 0UL) >= l_1507) >= (*g_1000))), 11))) | l_1485), 0L))) , l_1512), 0x1.56A90Ep+15)), g_43[0][3][2]))) < g_974), g_940[3])) <= g_43[0][3][2]), g_75[1][6])) == 0xF.836C35p-28), (-0x8.3p+1))) >= 0x0.8p+1)) , p_60);
                        (*l_1514) = ((*l_1451) = p_60);
                        l_1515 = p_62;
                    }
                    else
                    { /* block id: 714 */
                        int64_t *l_1518 = (void*)0;
                        int64_t *l_1519 = &g_43[2][3][0];
                        (*g_712) &= (((*l_1519) = l_1512) || (!g_857));
                        (*l_1515) = ((safe_mul_func_int8_t_s_s(0xD5L, (((safe_mul_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(l_1512, 1)), (safe_sub_func_uint64_t_u_u((*l_1515), 0x76557AB2B24841ADLL)))) , l_1512) <= (((*l_1515) , ((l_1531[0] = ((*l_1515) > (safe_sub_func_uint8_t_u_u(((255UL >= ((*l_1515) != g_4)) != g_43[1][3][2]), 0x73L)))) < (*l_1515))) , 0UL)))) >= g_1447);
                        if (l_1484)
                            continue;
                        (*l_1451) = (void*)0;
                    }
                }
                return l_1484;
            }
            else
            { /* block id: 724 */
                int64_t l_1541 = 0x6D46610ABEDEB387LL;
                uint8_t l_1542 = 0x03L;
                int64_t **l_1543 = (void*)0;
                int32_t l_1546 = 3L;
                l_1546 ^= ((((**g_1045) & (safe_mul_func_uint16_t_u_u(((((((-10L) != g_226) , (safe_unary_minus_func_int32_t_s((safe_sub_func_int16_t_s_s(((safe_mod_func_uint8_t_u_u((l_1531[2] &= (((((7UL | g_835) ^ ((*l_1515) , (((*l_1515) = (****g_863)) | ((safe_mod_func_int16_t_s_s((*g_1200), g_43[0][3][2])) , l_1541)))) > l_1542) , &g_1046) == l_1543)), l_1541)) , (***g_1105)), 0xAEC8L))))) , l_1543) != l_1544[0]) , g_738), 1UL))) | g_738) >= 0x807D921AL);
            }
            (*l_1451) = (*g_904);
            l_1547--;
            for (g_1417 = 0; (g_1417 <= 1); g_1417 += 1)
            { /* block id: 733 */
                uint16_t *l_1556 = &l_1457;
                uint8_t l_1564[3][8][10] = {{{247UL,0xFDL,0x25L,1UL,1UL,1UL,0xF9L,0x24L,1UL,251UL},{0x84L,0x4BL,0x45L,1UL,9UL,0xDFL,0xCFL,0x13L,0xA8L,0x13L},{0x03L,0x24L,253UL,0xFDL,253UL,0x24L,0x03L,3UL,247UL,0x4BL},{255UL,3UL,0x45L,1UL,0xA8L,0xFDL,0x32L,1UL,0x84L,3UL},{253UL,3UL,0x25L,0xDFL,0xF9L,251UL,0x03L,0x4BL,0x03L,251UL},{0x12L,0x24L,4UL,0x24L,0x12L,1UL,0xCFL,0x39L,255UL,0xDFL},{253UL,0x4BL,0xF9L,0xFDL,255UL,0x39L,0xF9L,255UL,253UL,0xDFL},{255UL,0xFDL,0xCFL,251UL,0x12L,1UL,4UL,1UL,0x12L,251UL}},{{0x03L,255UL,0x03L,1UL,0xF9L,249UL,0x25L,0x13L,253UL,3UL},{0x84L,0x24L,0x32L,0x39L,0xA8L,0x24L,0x45L,0x13L,255UL,0x4BL},{0x03L,0x39L,1UL,3UL,255UL,1UL,255UL,3UL,1UL,0x39L},{1UL,0xFDL,4UL,255UL,0xA8L,249UL,0UL,0UL,0x12L,249UL},{0x25L,3UL,253UL,0x13L,0x25L,249UL,0xF9L,1UL,0x03L,255UL},{1UL,247UL,0x32L,1UL,255UL,1UL,0x32L,247UL,1UL,0x4BL},{0x03L,1UL,0xF9L,249UL,0x25L,0x13L,253UL,3UL,0x25L,0xDFL},{0x12L,0UL,0UL,249UL,0xA8L,255UL,4UL,0xFDL,1UL,0xFDL}},{{1UL,3UL,255UL,1UL,255UL,3UL,1UL,0x39L,0x03L,0UL},{0x84L,0x39L,0UL,0x13L,1UL,1UL,0xC7L,0x13L,0x12L,0x39L},{255UL,0x39L,0xF9L,255UL,253UL,0xDFL,1UL,0UL,1UL,0xDFL},{0x3EL,3UL,0x32L,3UL,0x3EL,249UL,4UL,251UL,0x84L,255UL},{255UL,0UL,253UL,1UL,247UL,251UL,253UL,247UL,255UL,255UL},{0x84L,1UL,4UL,0xDFL,0x3EL,0x13L,0x32L,0x13L,0x3EL,0xDFL},{1UL,247UL,1UL,249UL,253UL,0x4BL,0xF9L,0xFDL,255UL,0x39L},{0x12L,3UL,0xC7L,251UL,1UL,3UL,0UL,0xFDL,0x84L,0UL}}};
                uint64_t * const **l_1573[3][1][7] = {{{&g_1571,&g_1571,(void*)0,&g_1571,&g_1571,(void*)0,&g_1571}},{{&g_1571,&g_1571,&g_1571,&g_1571,&g_1571,&g_1571,&g_1571}},{{&g_1571,&g_1571,&g_1571,&g_1571,&g_1571,&g_1571,&g_1571}}};
                uint16_t l_1576[6] = {0xE4C0L,0xE4C0L,0xCCDCL,0xE4C0L,0xE4C0L,0xCCDCL};
                uint16_t l_1578 = 0x9596L;
                int i, j, k;
                g_835 ^= ((~((((***g_1105) = (0x7A07L && 0xF500L)) , (((l_1574 = (((safe_mul_func_int8_t_s_s((safe_sub_func_uint8_t_u_u((*l_1515), ((+((((**g_1106) || ((*l_1556) = ((((*l_1489) = 65535UL) , g_1447) != 65535UL))) ^ (~(((safe_div_func_int16_t_s_s(((g_557 = ((safe_div_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s((l_1564[1][6][7] < (l_1565 != l_1567)), 14)), (*l_1515))) , 0xCAL)) & g_1569), (*g_1200))) , (*l_1515)) <= l_1564[1][6][7]))) & (-10L))) == l_1564[1][6][7]))), (*l_1515))) < 9UL) , g_1571)) == &g_1572) , &l_1574)) != &g_1571)) ^ (**g_1571));
                (*g_1577) = ((*g_438) = l_1576[0]);
                --l_1578;
            }
        }
    }
    return g_1363;
}


/* ------------------------------------------ */
/* 
 * reads : g_252 g_23 g_363 g_1027
 * writes: g_252 g_23
 */
static float  func_68(int64_t  p_69, int32_t * p_70, float * const  p_71, uint32_t * p_72)
{ /* block id: 680 */
    float *l_1419[5][7] = {{&g_75[1][6],&g_251,&g_75[1][6],&g_251,&g_75[1][6],&g_75[3][1],&g_75[3][1]},{&g_75[1][6],&g_75[1][6],&g_27,&g_75[1][6],&g_75[1][6],&g_27,&g_27},{&g_75[1][6],&g_251,&g_75[1][6],&g_251,&g_75[1][6],&g_75[3][1],&g_75[3][1]},{&g_75[1][6],&g_75[1][6],&g_27,&g_75[1][6],&g_27,&g_27,&g_27},{&g_75[3][1],&g_75[1][6],&g_75[1][6],&g_75[1][6],&g_75[3][1],&g_75[1][6],&g_75[1][6]}};
    int32_t l_1420 = (-1L);
    uint64_t *l_1429 = &g_252;
    int16_t l_1432 = 1L;
    int32_t l_1437 = (-5L);
    int32_t l_1442[10] = {0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L,0x05F79966L};
    const uint32_t l_1443 = 0xC68EAB57L;
    int32_t l_1444 = 0x8032B908L;
    uint16_t l_1445 = 6UL;
    int i, j;
    l_1420 = (-0x1.Bp-1);
    l_1445 = ((0L < (safe_mod_func_uint16_t_u_u((safe_add_func_int64_t_s_s((((((safe_mod_func_uint64_t_u_u(((*l_1429)++), l_1420)) & ((*p_72) |= l_1432)) ^ (((l_1420 && (safe_div_func_int64_t_s_s(((l_1437 = (safe_rshift_func_int8_t_s_u(p_69, 0))) | ((l_1442[6] = ((safe_div_func_uint8_t_u_u(((((safe_mul_func_uint8_t_u_u((l_1432 && l_1432), (-6L))) , g_363) > l_1420) , 253UL), 1L)) , l_1432)) && l_1443)), 7L))) | l_1420) != l_1444)) <= (-1L)) == p_69), p_69)), g_1027))) , l_1420);
    return l_1445;
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_95 g_76 g_277 g_23 g_226 g_31 g_132 g_264 g_265 g_131 g_33 g_252 g_322 g_255 g_43 g_327 g_141 g_347 g_160 g_360 g_404 g_363 g_4 g_438 g_272 g_27 g_348 g_75 g_557 g_593 g_552 g_248 g_137 g_712 g_719 g_738 g_655 g_809 g_857 g_863 g_849 g_864 g_898 g_904 g_918 g_865 g_556 g_251 g_999 g_919 g_920 g_1027 g_1000 g_940 g_1045 g_974 g_1085 g_1086 g_888 g_1105 g_1106 g_1107 g_1114 g_1199 g_1200 g_1207 g_1228 g_1281 g_1046 g_1363 g_1381 g_1417
 * writes: g_95 g_76 g_23 g_137 g_33 g_252 g_255 g_347 g_75 g_265 g_132 g_360 g_552 g_556 g_557 g_27 g_141 g_655 g_226 g_711 g_131 g_251 g_835 g_849 g_857 g_863 g_888 g_160 g_918 g_974 g_1085 g_1105 g_1027 g_319 g_738 g_1417
 */
static float  func_81(uint64_t  p_82, float * p_83)
{ /* block id: 39 */
    uint32_t l_93 = 1UL;
    uint8_t *l_119 = &g_31;
    uint8_t **l_120 = &l_119;
    int8_t *l_848 = &g_849[9][3];
    const uint8_t *l_853 = &g_31;
    const uint8_t **l_852 = &l_853;
    const uint8_t ***l_851[9] = {&l_852,&l_852,&l_852,&l_852,&l_852,&l_852,&l_852,&l_852,&l_852};
    const uint8_t ****l_850 = &l_851[5];
    uint8_t ** const *l_855[7];
    uint8_t ** const **l_854 = &l_855[3];
    int16_t *l_856 = &g_857;
    float *l_858[3][8] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    int64_t *l_1384 = &g_43[0][3][2];
    int64_t **l_1383 = &l_1384;
    uint8_t *l_1385[9] = {&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31};
    int32_t *l_1416 = &g_1417;
    int i, j;
    for (i = 0; i < 7; i++)
        l_855[i] = &l_120;
    (*g_94) |= l_93;
    (*l_1416) &= (safe_mul_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((safe_add_func_int32_t_s_s((func_102(((*l_1383) = func_105((((safe_lshift_func_int8_t_s_s(((safe_mod_func_uint16_t_u_u(l_93, (safe_add_func_uint64_t_u_u(0xF97C6D2ED0B757F9LL, func_115(&l_93, p_82, ((*l_120) = l_119)))))) > (((((*l_856) |= (safe_sub_func_uint64_t_u_u(g_43[2][3][0], (safe_sub_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(((*l_848) = 0x49L), ((l_850 != l_854) , g_160))), 65535UL))))) , p_82) & l_93) == l_93)), 0)) && 0x408C5A9BL) , p_82), &l_93, l_858[2][0])), l_1385[8]) , 0x65CA4A78L), 0L)) , 0UL), (*g_1000))), g_940[3]));
    return (*l_1416);
}


/* ------------------------------------------ */
/* 
 * reads : g_552 g_1000 g_940 g_248 g_1107 g_557 g_31
 * writes: g_552 g_1027 g_557 g_95
 */
static uint8_t  func_102(int64_t * p_103, uint8_t * p_104)
{ /* block id: 667 */
    uint64_t l_1390 = 18446744073709551615UL;
    int8_t **l_1397 = (void*)0;
    uint32_t *l_1403 = &g_655;
    int32_t *l_1407 = (void*)0;
    int32_t l_1408 = 0L;
    int32_t *l_1409 = &g_738;
    int32_t *l_1410 = &l_1408;
    int32_t *l_1411[7][7][5] = {{{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408}},{{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408}},{{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408}},{{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_835,&g_137,&l_1408},{&l_1408,&g_137,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137}},{{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137}},{{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137}},{{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137},{&g_137,&g_738,&g_738,&g_738,&g_137}}};
    int64_t l_1412[5][5] = {{0x3CCDB0BE86104754LL,0x9935E1C26D2F0753LL,0x3CCDB0BE86104754LL,0x3CCDB0BE86104754LL,0x9935E1C26D2F0753LL},{0x05872F554C7966AALL,0xA1D9737FD0A65E13LL,0xA1D9737FD0A65E13LL,0x05872F554C7966AALL,0xA1D9737FD0A65E13LL},{0x9935E1C26D2F0753LL,0x9935E1C26D2F0753LL,(-1L),0x9935E1C26D2F0753LL,0x9935E1C26D2F0753LL},{0xA1D9737FD0A65E13LL,0x05872F554C7966AALL,0xA1D9737FD0A65E13LL,0xA1D9737FD0A65E13LL,0x05872F554C7966AALL},{0x9935E1C26D2F0753LL,0x3CCDB0BE86104754LL,0x3CCDB0BE86104754LL,0x9935E1C26D2F0753LL,0x3CCDB0BE86104754LL}};
    uint32_t l_1413[10][4][6] = {{{4294967292UL,0x716CFEE1L,0xC51DCA8FL,4294967290UL,0x64FA1F5DL,0UL},{0x90361942L,4294967287UL,0xD1340D0EL,6UL,0x44645444L,4294967286UL},{0x64FA1F5DL,0xFC07A8DCL,0xD1340D0EL,0x716CFEE1L,0x0E511DEAL,0UL},{0xD3497A6EL,0xA2D83357L,0xC51DCA8FL,4294967295UL,4294967294UL,0UL}},{{0x44645444L,6UL,0xD1340D0EL,4294967287UL,0x90361942L,4294967286UL},{0x5A1350BCL,4294967295UL,0xD1340D0EL,1UL,0xD3497A6EL,0UL},{0x0E511DEAL,1UL,0xC51DCA8FL,0xFC07A8DCL,0x5A1350BCL,0UL},{0x55982513L,8UL,0xD1340D0EL,8UL,0x55982513L,4294967286UL}},{{1UL,0x8073F2E7L,4294967292UL,0x53DC6A91L,0UL,1UL},{0UL,1UL,0x0E511DEAL,0x8073F2E7L,0xF0E63F69L,1UL},{8UL,0xD971D7B5L,4294967292UL,0x203EAE97L,0xDD902FCFL,0x716CFEE1L},{0xF0E63F69L,0x4D349D0DL,4294967292UL,1UL,0x63311675L,1UL}},{{0x1537D578L,0x53DC6A91L,0x0E511DEAL,0xCD3EB034L,1UL,1UL},{0xDD902FCFL,0x203EAE97L,4294967292UL,0xD971D7B5L,8UL,0x716CFEE1L},{4294967287UL,0xCD3EB034L,4294967292UL,0x286FFA54L,0x1537D578L,1UL},{0x63311675L,0x286FFA54L,0x0E511DEAL,0x4D349D0DL,4294967287UL,1UL}},{{0x5732241DL,0x1002E80FL,4294967292UL,0x1002E80FL,0x5732241DL,0x716CFEE1L},{1UL,0x8073F2E7L,4294967292UL,0x53DC6A91L,0UL,1UL},{0UL,1UL,0x0E511DEAL,0x8073F2E7L,0xF0E63F69L,1UL},{8UL,0xD971D7B5L,4294967292UL,0x203EAE97L,0xDD902FCFL,0x716CFEE1L}},{{0xF0E63F69L,0x4D349D0DL,4294967292UL,1UL,0x63311675L,1UL},{0x1537D578L,0x53DC6A91L,0x0E511DEAL,0xCD3EB034L,1UL,1UL},{0xDD902FCFL,0x203EAE97L,4294967292UL,0xD971D7B5L,8UL,0x716CFEE1L},{4294967287UL,0xCD3EB034L,4294967292UL,0x286FFA54L,0x1537D578L,1UL}},{{0x63311675L,0x286FFA54L,0x0E511DEAL,0x4D349D0DL,4294967287UL,1UL},{0x5732241DL,0x1002E80FL,4294967292UL,0x1002E80FL,0x5732241DL,0x716CFEE1L},{1UL,0x8073F2E7L,4294967292UL,0x53DC6A91L,0UL,1UL},{0UL,1UL,0x0E511DEAL,0x8073F2E7L,0xF0E63F69L,1UL}},{{8UL,0xD971D7B5L,4294967292UL,0x203EAE97L,0xDD902FCFL,0x716CFEE1L},{0xF0E63F69L,0x4D349D0DL,4294967292UL,1UL,0x63311675L,1UL},{0x1537D578L,0x53DC6A91L,0x0E511DEAL,0xCD3EB034L,1UL,1UL},{0xDD902FCFL,0x203EAE97L,4294967292UL,0xD971D7B5L,8UL,0x716CFEE1L}},{{4294967287UL,0xCD3EB034L,4294967292UL,0x286FFA54L,0x1537D578L,1UL},{0x63311675L,0x286FFA54L,0x0E511DEAL,0x4D349D0DL,4294967287UL,1UL},{0x5732241DL,0x1002E80FL,4294967292UL,0x1002E80FL,0x5732241DL,0x716CFEE1L},{1UL,0x8073F2E7L,4294967292UL,0x53DC6A91L,0UL,1UL}},{{0UL,1UL,0x0E511DEAL,0x8073F2E7L,0xF0E63F69L,1UL},{8UL,0xD971D7B5L,4294967292UL,0x203EAE97L,0xDD902FCFL,0x716CFEE1L},{0xF0E63F69L,0x4D349D0DL,4294967292UL,1UL,0x63311675L,1UL},{0x1537D578L,0x53DC6A91L,0x0E511DEAL,0xCD3EB034L,1UL,0x286FFA54L}}};
    int i, j, k;
    for (g_552 = (-28); (g_552 > 19); g_552 = safe_add_func_uint64_t_u_u(g_552, 1))
    { /* block id: 670 */
        uint32_t **l_1388 = (void*)0;
        int32_t l_1404 = (-5L);
        int32_t *l_1406 = &g_95;
        (*l_1406) = ((l_1388 == l_1388) , (((safe_unary_minus_func_int16_t_s(l_1390)) , (safe_rshift_func_int8_t_s_u((*g_1000), (g_557 &= (safe_add_func_uint32_t_u_u(((((safe_add_func_int64_t_s_s(9L, (l_1397 == (void*)0))) , (!(+((*g_1107) = (+(safe_add_func_uint16_t_u_u((((void*)0 == l_1403) , 65529UL), g_248[1][5]))))))) > (*g_1000)) > l_1404), l_1404)))))) , l_1390));
        return (*p_104);
    }
    ++l_1413[7][2][4];
    return (*p_104);
}


/* ------------------------------------------ */
/* 
 * reads : g_863 g_272 g_27 g_849 g_95 g_76 g_31 g_255 g_864 g_898 g_322 g_904 g_918 g_719 g_347 g_348 g_252 g_557 g_712 g_865 g_131 g_132 g_23 g_556 g_655 g_4 g_809 g_251 g_999 g_226 g_552 g_43 g_137 g_919 g_920 g_141 g_1027 g_1000 g_940 g_1045 g_974 g_1085 g_1086 g_888 g_1105 g_1106 g_1107 g_277 g_33 g_1114 g_248 g_438 g_75 g_1199 g_1200 g_1207 g_404 g_1228 g_327 g_738 g_1281 g_94 g_1046 g_1363 g_1381
 * writes: g_863 g_849 g_255 g_141 g_888 g_132 g_33 g_557 g_160 g_918 g_27 g_75 g_655 g_76 g_23 g_252 g_556 g_974 g_1085 g_137 g_1105 g_226 g_1027 g_319 g_835 g_738 g_552
 */
static int64_t * func_105(uint32_t  p_106, uint32_t * const  p_107, float * p_108)
{ /* block id: 421 */
    uint8_t * const ****l_866 = &g_863;
    int32_t l_878 = 1L;
    int16_t l_879 = 1L;
    float *l_880[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_881 = 6L;
    int32_t l_882[3][9] = {{0x174C0731L,5L,5L,0x174C0731L,1L,0xF846DB75L,1L,0x174C0731L,5L},{1L,1L,0x5F7CB74FL,0xF846DB75L,1L,0xF846DB75L,0x5F7CB74FL,1L,1L},{5L,0x174C0731L,1L,0xF846DB75L,1L,0x174C0731L,5L,5L,0x174C0731L}};
    int8_t *l_883 = &g_849[9][3];
    int32_t l_884[6] = {(-1L),(-1L),0xC8AC75BCL,(-1L),(-1L),0xC8AC75BCL};
    int32_t *l_885 = &g_255;
    float l_889[7] = {0x2.931DABp-26,0x2.931DABp-26,0xC.F4B53Bp+63,0x2.931DABp-26,0x2.931DABp-26,0xC.F4B53Bp+63,0x2.931DABp-26};
    int8_t *l_1041 = &g_849[9][3];
    int32_t l_1111 = 0x1211B8EBL;
    uint64_t *l_1158 = &g_252;
    uint64_t * const *l_1157 = &l_1158;
    float l_1170 = 0x1.9p-1;
    float l_1178 = 0x6.235379p-63;
    int8_t l_1182[10][10] = {{0xB1L,0xABL,(-1L),0L,(-1L),0xABL,0xB1L,0xABL,(-1L),0L},{0xC1L,0L,0xC1L,0xABL,0x7EL,0xABL,0xC1L,0L,0xC1L,0xABL},{0xB1L,0L,0L,0L,0xB1L,1L,0xB1L,0L,0L,0L},{0x7EL,0xABL,0xC1L,0L,0xC1L,0xABL,0x7EL,0xABL,0xC1L,0L},{(-1L),0L,(-1L),0xABL,0xB1L,0xABL,(-1L),0L,(-1L),0xABL},{0x7EL,0L,4L,0L,0x7EL,1L,0x7EL,0L,4L,0L},{0xB1L,0xABL,(-1L),0L,(-1L),0xABL,0xB1L,0xABL,(-1L),0L},{0xC1L,0L,0xC1L,0xABL,0x7EL,0xABL,0xC1L,0L,0xC1L,0xABL},{0xB1L,0L,0L,0L,0xB1L,1L,0xB1L,0L,0L,0L},{0x7EL,0xABL,0xC1L,0L,0xC1L,0xABL,0x7EL,0xABL,0xC1L,0L}};
    int32_t l_1206 = 1L;
    int32_t l_1327 = (-4L);
    int32_t l_1379 = (-2L);
    int i, j;
lbl_1066:
    (*l_885) = (((safe_lshift_func_int16_t_s_u(0x4E5BL, (safe_sub_func_uint8_t_u_u((((*l_866) = g_863) == (((safe_add_func_int64_t_s_s(((safe_sub_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((l_878 = (safe_mul_func_float_f_f((+(l_878 >= l_879)), (-0x1.Ap-1)))), (l_882[2][5] = (l_881 = 0x0.4p+1)))), (*g_272))), (0x1.Ap-1 >= (l_884[2] = ((((((*l_883) |= l_879) , g_95) >= g_76) <= p_106) > l_879))))) , l_881), g_31)) < p_106) , &g_264)), p_106)))) , l_881) == 0x159BE40049C3349FLL);
lbl_928:
    (*l_885) |= 0x23C2C77CL;
lbl_1209:
    for (g_141 = (-26); (g_141 >= (-5)); g_141 = safe_add_func_uint64_t_u_u(g_141, 2))
    { /* block id: 432 */
        l_889[0] = ((g_888 = &g_655) != (void*)0);
    }
    for (g_132 = 0; (g_132 != 50); ++g_132)
    { /* block id: 438 */
        uint8_t ** const **l_892 = (void*)0;
        uint8_t *l_896 = &g_557;
        uint8_t ** const l_895 = &l_896;
        uint8_t ** const *l_894 = &l_895;
        uint8_t ** const **l_893 = &l_894;
        int32_t l_927 = (-1L);
        int32_t l_935[4];
        uint32_t l_942[7] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
        int16_t l_945[5][4][1] = {{{0x363BL},{0L},{0x363BL},{0x6476L}},{{0L},{(-10L)},{3L},{(-10L)}},{{0L},{0x6476L},{0x363BL},{0L}},{{0x363BL},{0x6476L},{0L},{(-10L)}},{{3L},{(-10L)},{0L},{0x6476L}}};
        int16_t l_1040 = 1L;
        const int64_t *l_1048[5][2][8] = {{{&g_160,&g_43[4][1][0],&g_160,&g_76,(void*)0,(void*)0,(void*)0,&g_556},{&g_43[0][3][2],(void*)0,&g_160,&g_43[5][3][1],&g_556,&g_160,&g_556,&g_556}},{{&g_43[0][3][2],&g_43[4][1][0],(void*)0,(void*)0,&g_43[4][1][0],&g_43[0][3][2],(void*)0,&g_556},{&g_76,&g_160,&g_160,&g_160,&g_43[0][3][2],&g_556,&g_160,&g_160}},{{(void*)0,&g_43[0][3][2],&g_556,&g_160,(void*)0,&g_556,&g_43[0][3][2],&g_556},{&g_160,(void*)0,&g_43[5][3][1],(void*)0,&g_43[5][3][1],(void*)0,&g_160,&g_160}},{{&g_160,&g_160,&g_43[0][3][2],&g_160,(void*)0,&g_43[3][1][2],&g_160,&g_43[5][3][1]},{&g_43[0][3][2],(void*)0,(void*)0,&g_556,(void*)0,&g_43[0][3][2],&g_43[4][1][0],(void*)0}},{{&g_160,(void*)0,&g_160,&g_43[5][3][1],&g_43[5][3][1],&g_160,(void*)0,&g_160},{&g_160,&g_160,&g_76,&g_43[0][3][2],(void*)0,&g_556,&g_43[5][3][1],&g_76}}};
        const int64_t **l_1047 = &l_1048[4][0][6];
        int16_t ** const *l_1131[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int16_t ** const **l_1130 = &l_1131[2];
        int8_t l_1161 = 0x07L;
        int32_t l_1172 = 1L;
        int16_t **l_1201 = &g_1200;
        int64_t *l_1210 = &g_556;
        uint64_t l_1223[1];
        int32_t **l_1382 = &g_33;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_935[i] = 6L;
        for (i = 0; i < 1; i++)
            l_1223[i] = 0xBFA276C8EFF4D48ALL;
        if (((*g_863) != ((*l_893) = &g_265)))
        { /* block id: 440 */
            int32_t l_908 = 1L;
            uint64_t l_911 = 4UL;
            int32_t l_929 = (-8L);
            int32_t l_930 = 0x15CDDB3EL;
            int32_t l_933 = 0x296702E4L;
            int32_t l_936 = 0x57235693L;
            float l_937 = (-0x7.7p-1);
            int32_t l_938[9] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
            int32_t l_939 = 2L;
            uint16_t *l_973 = &g_974;
            int32_t l_992[5] = {0x0919740EL,0x0919740EL,0x0919740EL,0x0919740EL,0x0919740EL};
            int16_t *l_997 = &l_945[0][0][0];
            int16_t **l_996 = &l_997;
            const int8_t *l_1019 = &g_849[1][1];
            const uint8_t l_1031 = 0x26L;
            int64_t l_1083 = 0x269E0DBBFBEF1324LL;
            uint32_t l_1173 = 1UL;
            int i;
            if (p_106)
            { /* block id: 441 */
                int32_t *l_897 = (void*)0;
                if (p_106)
                    break;
                (*g_898) = l_897;
            }
            else
            { /* block id: 444 */
                int8_t l_905 = 0xA6L;
                int32_t l_906 = 0L;
                int32_t l_910 = 0xA3C08320L;
                int32_t l_934[10][10][2] = {{{0x03621DE9L,0x77C15324L},{0x10745EBDL,(-2L)},{0x77C15324L,0x10745EBDL},{0x03621DE9L,(-2L)},{(-1L),0x77C15324L},{0x77C15324L,0xEC2BA96BL},{0x77C15324L,0x77C15324L},{(-1L),(-2L)},{0x03621DE9L,0x10745EBDL},{0x77C15324L,(-2L)}},{{0x10745EBDL,0x77C15324L},{0x03621DE9L,0xEC2BA96BL},{0x03621DE9L,0x77C15324L},{0x10745EBDL,(-2L)},{0x77C15324L,0x10745EBDL},{0x03621DE9L,(-2L)},{(-1L),0x77C15324L},{0x77C15324L,0xEC2BA96BL},{0x77C15324L,0x77C15324L},{(-1L),(-2L)}},{{0x03621DE9L,0x10745EBDL},{0x77C15324L,(-2L)},{0x10745EBDL,0x77C15324L},{0x03621DE9L,0xEC2BA96BL},{0x03621DE9L,0x77C15324L},{0x10745EBDL,(-2L)},{0x77C15324L,0x10745EBDL},{0x03621DE9L,(-2L)},{(-1L),0x77C15324L},{0x77C15324L,0xEC2BA96BL}},{{0x77C15324L,0x77C15324L},{(-1L),(-2L)},{0x03621DE9L,0x10745EBDL},{0x77C15324L,(-2L)},{0x10745EBDL,0x77C15324L},{0x03621DE9L,0xEC2BA96BL},{0x03621DE9L,0x77C15324L},{0x10745EBDL,(-2L)},{0x77C15324L,0x10745EBDL},{0x03621DE9L,(-2L)}},{{(-1L),0x77C15324L},{0x77C15324L,0xEC2BA96BL},{0x77C15324L,0x77C15324L},{(-1L),(-2L)},{0x03621DE9L,0x10745EBDL},{0x77C15324L,(-2L)},{0x10745EBDL,0x77C15324L},{0x03621DE9L,0xEC2BA96BL},{0x03621DE9L,0x77C15324L},{0x10745EBDL,(-2L)}},{{0x77C15324L,0x10745EBDL},{0x03621DE9L,(-2L)},{(-1L),0x77C15324L},{0x77C15324L,0x2920F983L},{0x10745EBDL,0x10745EBDL},{0x1992AF46L,0xEC2BA96BL},{(-1L),6L},{0x10745EBDL,0xEC2BA96BL},{6L,0x10745EBDL},{(-1L),0x2920F983L}},{{(-1L),0x10745EBDL},{6L,0xEC2BA96BL},{0x10745EBDL,6L},{(-1L),0xEC2BA96BL},{0x1992AF46L,0x10745EBDL},{0x10745EBDL,0x2920F983L},{0x10745EBDL,0x10745EBDL},{0x1992AF46L,0xEC2BA96BL},{(-1L),6L},{0x10745EBDL,0xEC2BA96BL}},{{6L,0x10745EBDL},{(-1L),0x2920F983L},{(-1L),0x10745EBDL},{6L,0xEC2BA96BL},{0x10745EBDL,6L},{(-1L),0xEC2BA96BL},{0x1992AF46L,0x10745EBDL},{0x10745EBDL,0x2920F983L},{0x10745EBDL,0x10745EBDL},{0x1992AF46L,0xEC2BA96BL}},{{(-1L),6L},{0x10745EBDL,0xEC2BA96BL},{6L,0x10745EBDL},{(-1L),0x2920F983L},{(-1L),0x10745EBDL},{6L,0xEC2BA96BL},{0x10745EBDL,6L},{(-1L),0xEC2BA96BL},{0x1992AF46L,0x10745EBDL},{0x10745EBDL,0x2920F983L}},{{0x10745EBDL,0x10745EBDL},{0x1992AF46L,0xEC2BA96BL},{(-1L),6L},{0x10745EBDL,0xEC2BA96BL},{6L,0x10745EBDL},{(-1L),0x2920F983L},{(-1L),0x10745EBDL},{6L,0xEC2BA96BL},{0x10745EBDL,6L},{(-1L),0xEC2BA96BL}}};
                int64_t l_941[7][7][5] = {{{0x94087F874E24C6F9LL,(-6L),0xDB7DB2B7A3B75BB1LL,(-10L),(-1L)},{0xC0EFD77E52D57B63LL,0xEFC8DC9E2555B0A9LL,0x3E1C86584785B89CLL,1L,0L},{1L,(-6L),1L,0L,1L},{(-5L),(-6L),0xB18565031CD926B5LL,0x8E2AFDC0F8F2BD54LL,0x1AF24E94D4ED8D4DLL},{(-6L),0xEFC8DC9E2555B0A9LL,1L,0xEC9DBA4E7D7C916ALL,0x3E1C86584785B89CLL},{0xD51D8F04AA1DA862LL,(-6L),9L,0x39F631C6329EB919LL,(-1L)},{0x242D23DF564E2FD0LL,(-6L),0x3E1C86584785B89CLL,0xF6D6C9C5357231AALL,0xA3339AEFC159C08ELL}},{{1L,0xEFC8DC9E2555B0A9LL,0L,0L,0xDB7DB2B7A3B75BB1LL},{0L,(-6L),0xBC49854AA4B10876LL,0x7B171787DD97B897LL,0x1AF24E94D4ED8D4DLL},{0xEFC8DC9E2555B0A9LL,(-6L),1L,0x82D310CC5089357FLL,0x3B744DE550F7C7A4LL},{0xD51D8F04AA1DA862LL,0xEFC8DC9E2555B0A9LL,0xDB7DB2B7A3B75BB1LL,0x39F631C6329EB919LL,1L},{0xC0EFD77E52D57B63LL,(-6L),2L,1L,0xA3339AEFC159C08ELL},{(-1L),(-6L),0L,0xC64E1B36B0221BE4LL,1L},{0L,0xEFC8DC9E2555B0A9LL,0xB18565031CD926B5LL,0x7B171787DD97B897LL,0xB18565031CD926B5LL}},{{(-6L),(-6L),2L,0xEC9DBA4E7D7C916ALL,0x3B744DE550F7C7A4LL},{0x94087F874E24C6F9LL,(-6L),0xDB7DB2B7A3B75BB1LL,(-10L),(-1L)},{0xC0EFD77E52D57B63LL,0xEFC8DC9E2555B0A9LL,0x3E1C86584785B89CLL,1L,0L},{1L,(-6L),1L,0L,1L},{(-5L),(-6L),0xB18565031CD926B5LL,0x8E2AFDC0F8F2BD54LL,0x1AF24E94D4ED8D4DLL},{(-6L),0xEFC8DC9E2555B0A9LL,0x20399AA096509F96LL,8L,(-10L)},{0xB18565031CD926B5LL,1L,9L,0L,(-4L)}},{{1L,1L,(-10L),0xA27BCC94892969ACLL,0x889F360F2B9E968DLL},{0x3E1C86584785B89CLL,2L,0x199EB8269DDD903CLL,(-1L),1L},{0xDB7DB2B7A3B75BB1LL,1L,(-9L),0xA4664D36F4136B08LL,0x0219C06045B720AFLL},{2L,1L,0x20399AA096509F96LL,0x32894BADDC59C72BLL,0x16DF510FE3E9A163LL},{0xB18565031CD926B5LL,2L,1L,0L,0x20399AA096509F96LL},{0L,1L,0L,0x5E34DE789E89B326LL,0x889F360F2B9E968DLL},{2L,1L,0x199EB8269DDD903CLL,0L,1L}},{{0xDB7DB2B7A3B75BB1LL,2L,0x87730A8F54596B0CLL,0xA4664D36F4136B08LL,0x87730A8F54596B0CLL},{1L,1L,0xFD20156F0FB33A6ELL,8L,0x16DF510FE3E9A163LL},{0xBC49854AA4B10876LL,1L,1L,0L,(-4L)},{0L,2L,(-10L),0x5E34DE789E89B326LL,0x199EB8269DDD903CLL},{0x3E1C86584785B89CLL,1L,(-1L),(-1L),1L},{9L,1L,0x87730A8F54596B0CLL,0x5ADBFEA17978D41FLL,0x0219C06045B720AFLL},{1L,2L,0x20399AA096509F96LL,8L,(-10L)}},{{0xB18565031CD926B5LL,1L,9L,0L,(-4L)},{1L,1L,(-10L),0xA27BCC94892969ACLL,0x889F360F2B9E968DLL},{0x3E1C86584785B89CLL,2L,0x199EB8269DDD903CLL,(-1L),1L},{0xDB7DB2B7A3B75BB1LL,1L,(-9L),0xA4664D36F4136B08LL,0x0219C06045B720AFLL},{2L,1L,0x20399AA096509F96LL,0x32894BADDC59C72BLL,0x16DF510FE3E9A163LL},{0xB18565031CD926B5LL,2L,1L,0L,0x20399AA096509F96LL},{0L,1L,0L,0x5E34DE789E89B326LL,0x889F360F2B9E968DLL}},{{2L,1L,0x199EB8269DDD903CLL,0L,1L},{0xDB7DB2B7A3B75BB1LL,2L,0x87730A8F54596B0CLL,0xA4664D36F4136B08LL,0x87730A8F54596B0CLL},{1L,1L,0xFD20156F0FB33A6ELL,8L,0x16DF510FE3E9A163LL},{0xBC49854AA4B10876LL,1L,1L,0L,(-4L)},{0L,2L,(-10L),0x5E34DE789E89B326LL,0x199EB8269DDD903CLL},{0x3E1C86584785B89CLL,1L,(-1L),(-1L),1L},{9L,1L,0x87730A8F54596B0CLL,0x5ADBFEA17978D41FLL,0x0219C06045B720AFLL}}};
                int16_t l_1018[1][10] = {{0x5AF9L,0x03D0L,0x5AF9L,0x03D0L,0x5AF9L,0x03D0L,0x5AF9L,0x03D0L,0x5AF9L,0x03D0L}};
                uint8_t *l_1132 = &g_31;
                uint64_t **l_1160[1];
                int32_t l_1171 = 0L;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_1160[i] = &l_1158;
                for (g_557 = 18; (g_557 != 4); g_557--)
                { /* block id: 447 */
                    float l_907[10][1] = {{0x1.61F20Ep+1},{0x9.8D9530p+14},{0x1.61F20Ep+1},{0x9.Fp-1},{0x9.Fp-1},{0x1.61F20Ep+1},{0x9.8D9530p+14},{0x1.61F20Ep+1},{0x9.Fp-1},{0x9.Fp-1}};
                    int32_t l_909[7][7][1] = {{{2L},{(-8L)},{0x938D5CEDL},{(-8L)},{2L},{0x5668767EL},{0xA782F1EAL}},{{(-8L)},{0L},{(-8L)},{0xA782F1EAL},{0x5668767EL},{2L},{(-8L)}},{{0x938D5CEDL},{(-8L)},{2L},{0x5668767EL},{0xA782F1EAL},{(-8L)},{0L}},{{(-8L)},{0xA782F1EAL},{0xADC517C6L},{0L},{0x5668767EL},{0x3C4BE032L},{0x5668767EL}},{{0L},{0xADC517C6L},{0x938D5CEDL},{0x5668767EL},{0x07F29927L},{0x5668767EL},{0x938D5CEDL}},{{0xADC517C6L},{0L},{0x5668767EL},{0x3C4BE032L},{0x5668767EL},{0L},{0xADC517C6L}},{{0x938D5CEDL},{0x5668767EL},{0x07F29927L},{0x5668767EL},{0x938D5CEDL},{0xADC517C6L},{0L}}};
                    int32_t *l_931 = (void*)0;
                    int32_t *l_932[2][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0}};
                    int i, j, k;
                    if ((*g_322))
                        break;
                    for (g_160 = 0; (g_160 <= 1); g_160 = safe_add_func_int16_t_s_s(g_160, 9))
                    { /* block id: 451 */
                        int32_t *l_903[9];
                        int i;
                        for (i = 0; i < 9; i++)
                            l_903[i] = &l_884[2];
                        (*g_904) = l_903[8];
                        --l_911;
                    }
                    for (l_910 = 4; (l_910 >= 0); l_910 -= 1)
                    { /* block id: 457 */
                        int i;
                        l_927 = (safe_mul_func_float_f_f((safe_mul_func_float_f_f((0x4.Dp-1 >= ((g_918 = g_918) == (void*)0)), (safe_div_func_float_f_f((((*g_719) = 0x1.4p+1) > ((*g_347) != (void*)0)), l_884[l_910])))), ((safe_div_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_u((l_906 = (((((g_75[1][6] = 0x7.1p-1) >= ((void*)0 == p_108)) >= (*l_885)) , 0x5CL) && 5L)), 1)), g_252)) , l_884[l_910])));
                        if (g_557)
                            goto lbl_928;
                    }
                    l_942[0]++;
                }
                if (l_945[4][1][0])
                { /* block id: 467 */
                    int16_t *l_979 = (void*)0;
                    int16_t **l_998 = &l_997;
                    int32_t l_1001 = 0xE04AB5C5L;
                    const uint8_t ****l_1016 = (void*)0;
                    for (g_655 = 0; (g_655 <= 1); g_655 += 1)
                    { /* block id: 470 */
                        uint16_t l_975 = 1UL;
                        int16_t *l_976 = &l_879;
                        int16_t **l_980 = (void*)0;
                        int16_t **l_981 = &l_976;
                        int16_t ***l_993 = &l_980;
                        int32_t *l_1002 = &l_930;
                        uint8_t ** const **l_1015[9] = {&l_894,&l_894,&l_894,&l_894,&l_894,&l_894,&l_894,&l_894,&l_894};
                        int64_t *l_1017[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_1017[i] = &l_941[4][3][1];
                        (*l_885) |= (safe_mul_func_int8_t_s_s(p_106, ((safe_lshift_func_int16_t_s_s(((safe_sub_func_int64_t_s_s((safe_div_func_int32_t_s_s((((safe_mul_func_int8_t_s_s(0x13L, ((safe_rshift_func_int16_t_s_u(((*l_976) = (!(safe_lshift_func_int16_t_s_s((((g_76 = p_106) , (safe_mod_func_int8_t_s_s(p_106, (l_975 = (safe_rshift_func_int8_t_s_s(((*l_883) = (safe_add_func_int32_t_s_s((p_106 || (safe_mul_func_uint8_t_u_u((safe_add_func_int8_t_s_s(((((safe_mul_func_int16_t_s_s((g_141 = p_106), (l_973 == (void*)0))) || (&g_809 != &g_272)) && (*g_712)) || (-4L)), (***g_864))), 0xE8L))), (*g_348)))), 4)))))) == g_556), p_106)))), 9)) , 7UL))) && l_945[3][2][0]) , p_106), 1UL)), p_106)) , l_975), l_935[3])) , (-1L))));
                        (*l_1002) ^= ((p_106 != (safe_rshift_func_int16_t_s_u(((((((*l_981) = (l_979 = &g_141)) == ((((g_655 ^ (g_252 = (((safe_rshift_func_uint8_t_u_u(((*l_896) |= (((*g_348) = ((safe_mul_func_int8_t_s_s((((safe_mul_func_float_f_f((safe_add_func_float_f_f((l_934[2][4][0] = ((g_4 , ((safe_sub_func_uint16_t_u_u((l_992[2] >= (((*l_993) = l_981) != ((safe_div_func_int16_t_s_s((l_996 != (void*)0), 0xBD11L)) , l_998))), p_106)) >= p_106)) , p_106)), (*l_885))), (*g_809))) , g_999[1]) == (void*)0), 0x9BL)) , 0x199523C1L)) , l_1001)), 7)) , p_106) , p_106))) | l_935[0]) ^ 0x0612L) , (void*)0)) | 65535UL) != g_226) , p_106), 13))) <= g_552);
                        (*l_1002) |= ((g_226 , ((safe_mod_func_int16_t_s_s((safe_rshift_func_int16_t_s_s(((*l_976) = (p_106 | ((safe_mod_func_int32_t_s_s((l_935[0] |= (safe_mul_func_int16_t_s_s((((**g_347) ^= ((safe_rshift_func_int16_t_s_u(((safe_add_func_int64_t_s_s((g_556 = (p_106 || (g_43[1][0][1] , ((l_906 , (g_137 , (**l_866))) == (((l_1015[4] == l_1016) || (-1L)) , (*g_919)))))), p_106)) != l_992[2]), 0)) , (*p_107))) == (*l_885)), 0x31BFL))), p_106)) != p_106))), l_1001)), 0x4B84L)) , 4294967295UL)) , 0L);
                    }
                    (*l_885) &= l_1018[0][0];
                    if (l_938[5])
                    { /* block id: 492 */
                        const int8_t **l_1020 = &l_1019;
                        uint64_t *l_1028 = &g_252;
                        int32_t *l_1044 = &l_930;
                        (*l_1044) ^= ((((*l_1020) = l_1019) != (((safe_mul_func_uint16_t_u_u((((g_141 , (safe_lshift_func_int16_t_s_u((((l_905 != (safe_mul_func_int8_t_s_s((((*l_885) |= g_1027) <= (((*l_1028)--) || l_1031)), (p_106 <= ((safe_add_func_int32_t_s_s(0x546C2D82L, 0x1B62AE85L)) == (safe_sub_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_u(l_1040, 5)) && g_557), l_935[1])), 0xD8EDB0BA3DA87163LL))))))) , p_106) & p_106), 2))) <= l_1001) < (*g_1000)), 0x835CL)) , 0xCED8423DL) , l_1041)) || 7L);
                    }
                    else
                    { /* block id: 497 */
                        int8_t l_1051 = 0xFBL;
                        l_1001 = ((g_1045 == l_1047) , ((*l_885) = (((((safe_div_func_int64_t_s_s((l_1051 && ((*l_973) |= ((safe_mul_func_int16_t_s_s((safe_mod_func_int64_t_s_s((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s((safe_mod_func_uint32_t_u_u(l_927, ((*p_107) ^= (((-5L) != (7UL != ((l_1051 != 0xBB651C54L) == ((((1UL <= 1L) <= l_942[1]) || (****g_863)) & (*l_885))))) || l_942[0])))), l_992[2])) ^ l_939), p_106)) , p_106), p_106)), 0x07AC13589B4B095DLL)), l_1001)) && 1UL))), p_106)) <= (*g_322)) , (void*)0) != (void*)0) <= p_106)));
                    }
                }
                else
                { /* block id: 503 */
                    int32_t l_1115 = 0xBA407A08L;
                    uint8_t *l_1133 = &g_31;
                    int32_t **l_1134 = &g_33;
                    for (l_933 = 0; (l_933 >= (-26)); --l_933)
                    { /* block id: 506 */
                        uint16_t *l_1084 = &g_1085;
                        int16_t *** const l_1104 = (void*)0;
                        int16_t * const ***l_1108 = (void*)0;
                        int16_t * const ***l_1109 = (void*)0;
                        int16_t * const ***l_1110 = &g_1105;
                        if (g_23)
                            goto lbl_1066;
                        if (l_942[0])
                            continue;
                        (*g_1086) &= (safe_mod_func_uint16_t_u_u(((safe_div_func_uint64_t_u_u(p_106, 1L)) , (p_106 && ((*l_1084) |= (safe_rshift_func_uint16_t_u_u(((*l_973) = (((~((*g_348) <= (((safe_lshift_func_uint8_t_u_s(((~(safe_sub_func_int16_t_s_s((!((safe_add_func_uint32_t_u_u(((*p_107) = (*p_107)), (((*l_896) ^= ((((**l_996) ^= 1L) <= (((void*)0 == p_107) <= (+4294967295UL))) < 0xB06B18AF1500516ALL)) < 1L))) <= 0x7BL)), l_1083))) >= g_849[0][2]), 5)) > (*l_885)) <= p_106))) != 0x226647F308418F00LL) < l_908)), g_23))))), p_106));
                        (*l_885) = ((safe_add_func_uint32_t_u_u(((l_934[2][4][0] = (((safe_add_func_int16_t_s_s((+(safe_add_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((((safe_sub_func_int8_t_s_s(((*l_883) |= (-5L)), (((safe_rshift_func_uint8_t_u_s((safe_sub_func_uint16_t_u_u((((0L >= (l_906 = (((*g_888) = (*l_885)) , p_106))) >= ((((*l_996) != (((safe_lshift_func_uint16_t_u_u(((l_1104 == ((*l_1110) = g_1105)) & l_1040), 14)) ^ 0x41L) , &l_1018[0][3])) > 0x21L) == 0x3709DD17L)) || 0x23L), l_1111)), p_106)) < p_106) , (*l_885)))) , 0x9F2AL) <= 0L), 0x1326L)) && l_935[0]), (*g_1000)))), (**g_1106))) != (*l_885)) == 0x3FDEL)) == (*g_1000)), (*g_277))) && (*l_885));
                    }
                    for (l_911 = 15; (l_911 == 37); l_911 = safe_add_func_uint32_t_u_u(l_911, 7))
                    { /* block id: 524 */
                        (*g_1114) = (*g_898);
                        if (l_1115)
                            break;
                    }
                    (*l_1134) = ((l_935[1] = (safe_div_func_int32_t_s_s((0xABF33BD556B35F80LL != (safe_rshift_func_uint8_t_u_u((((l_927 = (~((**g_347) = (safe_unary_minus_func_uint16_t_u((((&g_519[0] == (void*)0) & (safe_lshift_func_uint16_t_u_s(g_248[0][2], 8))) != (safe_sub_func_int64_t_s_s(((l_1130 == (void*)0) < (l_1115 <= (g_4 , ((l_1132 == l_1133) != p_106)))), 1UL)))))))) >= 0L) | p_106), p_106))), (*p_107)))) , &l_929);
                    (*g_272) = (((l_941[0][4][3] , (safe_unary_minus_func_uint8_t_u(((*l_896) |= (safe_mod_func_uint16_t_u_u(((*l_973)--), 0x84B1L)))))) , (*g_438)) == (safe_sub_func_float_f_f(((&l_883 == (((safe_mod_func_int8_t_s_s((*g_1000), (l_906 = (safe_mul_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(6UL, (*l_885))), (safe_add_func_int64_t_s_s((safe_lshift_func_int16_t_s_u(p_106, (((*g_1107) , &g_1000) == (void*)0))), g_4))))))) || l_929) , &g_1000)) == 0x1.0p+1), g_4)));
                }
                for (g_226 = 0; (g_226 >= 17); g_226++)
                { /* block id: 539 */
                    uint64_t * const **l_1159 = &l_1157;
                    int32_t l_1162 = 0xAAE52DEDL;
                    int32_t *l_1163 = &l_933;
                    int32_t *l_1164 = (void*)0;
                    int32_t *l_1165 = &l_884[0];
                    int32_t *l_1166 = &l_878;
                    int32_t *l_1167 = &l_884[4];
                    int32_t *l_1168[10];
                    int16_t l_1169 = 1L;
                    int i;
                    for (i = 0; i < 10; i++)
                        l_1168[i] = &g_255;
                    l_929 = ((249UL & (l_935[3] != ((safe_unary_minus_func_int64_t_s((((*l_1159) = l_1157) != l_1160[0]))) > l_1161))) && l_1162);
                    ++l_1173;
                    for (g_255 = (-1); (g_255 == 21); g_255 = safe_add_func_uint8_t_u_u(g_255, 3))
                    { /* block id: 545 */
                        uint32_t l_1179 = 18446744073709551614UL;
                        if ((*g_712))
                            break;
                        ++l_1179;
                        l_1182[7][4] = 0x3.Ap+1;
                    }
                }
            }
            (*l_885) = p_106;
            (*l_885) = (safe_div_func_int16_t_s_s((safe_div_func_int32_t_s_s(((safe_mod_func_int8_t_s_s(0xAEL, (safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint16_t_u_s(((****g_863) <= (safe_sub_func_uint32_t_u_u((((safe_mul_func_int16_t_s_s((safe_sub_func_int32_t_s_s(p_106, (*p_107))), ((l_1201 = g_1199) != (void*)0))) >= (((***g_1105) &= (safe_sub_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((((0x589EL != (4294967295UL ^ 9UL)) != l_930) , p_106), p_106)), l_1206))) && 0xCC62L)) != p_106), (*p_107)))), 8)), l_930)))) == l_1161), (*p_107))), (**g_1199)));
            (*g_1207) = p_107;
        }
        else
        { /* block id: 557 */
            const int16_t l_1208[2][9] = {{1L,0xDB28L,0xCB12L,0xCB12L,0xDB28L,1L,0x6807L,0xDB28L,0x6807L},{1L,0xDB28L,0xCB12L,0xCB12L,0xDB28L,1L,0x6807L,0xDB28L,0x6807L}};
            uint16_t l_1227 = 0x3CFAL;
            uint32_t l_1229[1][6] = {{2UL,0x6149539FL,2UL,2UL,0x6149539FL,2UL}};
            int i, j;
            for (g_141 = 0; (g_141 <= 5); g_141 += 1)
            { /* block id: 560 */
                int64_t **l_1224 = &l_1210;
                int i;
                l_884[g_141] = l_884[g_141];
                (*l_866) = (*l_866);
                g_319[(g_141 + 1)] = (void*)0;
                for (g_835 = 4; (g_835 >= 2); g_835 -= 1)
                { /* block id: 566 */
                    float l_1225 = 0xC.583009p+60;
                    const int32_t l_1226 = 0x74C254DFL;
                    int32_t l_1230 = 0x43E62A8DL;
                    if (l_1040)
                    { /* block id: 567 */
                        (*g_404) = ((l_1208[0][6] != g_4) != 0x588A3A6E1003DA64LL);
                        if (g_556)
                            goto lbl_1209;
                    }
                    else
                    { /* block id: 570 */
                        return l_1210;
                    }
                    g_738 &= (safe_unary_minus_func_int32_t_s(((***g_1105) > ((l_1230 ^= ((safe_div_func_uint32_t_u_u((*l_885), ((((!((*g_348) = l_935[0])) == (safe_rshift_func_uint16_t_u_u(((((safe_mod_func_uint16_t_u_u(((((((((g_76 |= ((safe_mod_func_uint16_t_u_u(l_1223[0], g_95)) > (((&l_1048[4][0][6] == l_1224) < (((*g_1200) < l_1223[0]) > p_106)) && 18446744073709551612UL))) , 0xCAEFL) ^ l_1226) | 0L) && l_1227) , p_106) >= p_106) >= 0x9FL), 0x1012L)) , (void*)0) == g_1228) , g_132), g_940[3]))) & g_327) && 0x4066CD088D2ABAFALL))) , l_1229[0][5])) <= 0x932CL))));
                }
            }
            if (p_106)
                continue;
            return l_1210;
        }
        for (g_552 = 0; (g_552 <= (-17)); g_552--)
        { /* block id: 584 */
            uint32_t l_1245 = 0x1459B5A7L;
            int32_t l_1277 = 0x5D605BA8L;
            uint64_t l_1325[8][6][5] = {{{0UL,0UL,18446744073709551615UL,1UL,0x222EB2D167D45392LL},{2UL,0x9BF01522D23B7BB3LL,5UL,0UL,0xB084E63AB87C667CLL},{0x27FBCBFF353C018CLL,18446744073709551615UL,0xFEFA5133A8DFE166LL,18446744073709551615UL,0x27FBCBFF353C018CLL},{0x4A6F2F09B513B298LL,0x9BF01522D23B7BB3LL,0xA18532CB9A438C2BLL,0x132D5F2333E4F547LL,1UL},{1UL,0UL,0xB014ABE2FBBB98DCLL,0xEA5C847F97BD9A8ELL,0xEA5C847F97BD9A8ELL},{0x132D5F2333E4F547LL,18446744073709551615UL,0x132D5F2333E4F547LL,0x9BF01522D23B7BB3LL,1UL}},{{0x1AE40A07D771B36CLL,0xEA5C847F97BD9A8ELL,0UL,18446744073709551614UL,0x27FBCBFF353C018CLL},{1UL,0xB084E63AB87C667CLL,18446744073709551615UL,18446744073709551615UL,0xB084E63AB87C667CLL},{0xB014ABE2FBBB98DCLL,18446744073709551615UL,0UL,0x27FBCBFF353C018CLL,0x222EB2D167D45392LL},{18446744073709551615UL,0x7C8DD90D5FCD5CCCLL,0x132D5F2333E4F547LL,5UL,0x69610D268E2DEBB7LL},{0x3D38361B79A4DC58LL,0xB014ABE2FBBB98DCLL,0xB014ABE2FBBB98DCLL,0x3D38361B79A4DC58LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0xA18532CB9A438C2BLL,0x4A6F2F09B513B298LL,5UL}},{{0xB014ABE2FBBB98DCLL,1UL,0xFEFA5133A8DFE166LL,0UL,0xFEFA5133A8DFE166LL},{1UL,1UL,5UL,0x4A6F2F09B513B298LL,0xA18532CB9A438C2BLL},{0x1AE40A07D771B36CLL,18446744073709551614UL,18446744073709551615UL,0x3D38361B79A4DC58LL,0xB014ABE2FBBB98DCLL},{0x132D5F2333E4F547LL,5UL,0x69610D268E2DEBB7LL,5UL,0x132D5F2333E4F547LL},{1UL,18446744073709551614UL,0x222EB2D167D45392LL,0x27FBCBFF353C018CLL,0UL},{0x4A6F2F09B513B298LL,1UL,0xB084E63AB87C667CLL,18446744073709551615UL,18446744073709551615UL}},{{0x27FBCBFF353C018CLL,1UL,0x27FBCBFF353C018CLL,18446744073709551614UL,0UL},{2UL,18446744073709551615UL,1UL,0x9BF01522D23B7BB3LL,0x132D5F2333E4F547LL},{0UL,0xB014ABE2FBBB98DCLL,0xEA5C847F97BD9A8ELL,0xEA5C847F97BD9A8ELL,0xB014ABE2FBBB98DCLL},{0xB084E63AB87C667CLL,0x7C8DD90D5FCD5CCCLL,1UL,0x132D5F2333E4F547LL,0xA18532CB9A438C2BLL},{1UL,18446744073709551615UL,0x27FBCBFF353C018CLL,18446744073709551615UL,0xFEFA5133A8DFE166LL},{0UL,0xB084E63AB87C667CLL,0xB084E63AB87C667CLL,0UL,5UL}},{{1UL,0xEA5C847F97BD9A8ELL,0x222EB2D167D45392LL,1UL,18446744073709551615UL},{0xB084E63AB87C667CLL,18446744073709551615UL,0x69610D268E2DEBB7LL,5UL,2UL},{18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551614UL,0xFEFA5133A8DFE166LL},{0x132D5F2333E4F547LL,0x7C8DD90D5FCD5CCCLL,18446744073709551615UL,1UL,0UL},{0xEA5C847F97BD9A8ELL,1UL,0x1AE40A07D771B36CLL,1UL,0xEA5C847F97BD9A8ELL},{0x9BF01522D23B7BB3LL,0x7C8DD90D5FCD5CCCLL,0x69610D268E2DEBB7LL,18446744073709551615UL,5UL}},{{18446744073709551614UL,18446744073709551615UL,0x3D38361B79A4DC58LL,0xB014ABE2FBBB98DCLL,0xB014ABE2FBBB98DCLL},{18446744073709551615UL,0x4A6F2F09B513B298LL,18446744073709551615UL,0x7C8DD90D5FCD5CCCLL,5UL},{0x27FBCBFF353C018CLL,0xB014ABE2FBBB98DCLL,18446744073709551615UL,18446744073709551615UL,0xEA5C847F97BD9A8ELL},{5UL,0UL,0xB084E63AB87C667CLL,0xB084E63AB87C667CLL,0UL},{0x3D38361B79A4DC58LL,0x222EB2D167D45392LL,18446744073709551615UL,0xEA5C847F97BD9A8ELL,0xFEFA5133A8DFE166LL},{0x4A6F2F09B513B298LL,0xA18532CB9A438C2BLL,18446744073709551615UL,18446744073709551615UL,2UL}},{{0UL,0x3D38361B79A4DC58LL,0x3D38361B79A4DC58LL,0UL,1UL},{0x4A6F2F09B513B298LL,0xB084E63AB87C667CLL,0x69610D268E2DEBB7LL,0x9BF01522D23B7BB3LL,18446744073709551615UL},{0x3D38361B79A4DC58LL,1UL,0x1AE40A07D771B36CLL,18446744073709551615UL,0x1AE40A07D771B36CLL},{5UL,5UL,18446744073709551615UL,0x9BF01522D23B7BB3LL,0x69610D268E2DEBB7LL},{0x27FBCBFF353C018CLL,18446744073709551615UL,1UL,0UL,0x3D38361B79A4DC58LL},{18446744073709551615UL,18446744073709551615UL,2UL,18446744073709551615UL,18446744073709551615UL}},{{18446744073709551614UL,18446744073709551615UL,0xFEFA5133A8DFE166LL,0xEA5C847F97BD9A8ELL,18446744073709551615UL},{0x9BF01522D23B7BB3LL,5UL,0UL,0xB084E63AB87C667CLL,0xB084E63AB87C667CLL},{0xEA5C847F97BD9A8ELL,1UL,0xEA5C847F97BD9A8ELL,18446744073709551615UL,18446744073709551615UL},{0x132D5F2333E4F547LL,0xB084E63AB87C667CLL,5UL,0x7C8DD90D5FCD5CCCLL,18446744073709551615UL},{18446744073709551615UL,0x3D38361B79A4DC58LL,0xB014ABE2FBBB98DCLL,0xB014ABE2FBBB98DCLL,0x3D38361B79A4DC58LL},{0UL,0xA18532CB9A438C2BLL,5UL,18446744073709551615UL,0x69610D268E2DEBB7LL}}};
            int32_t l_1373[6][7] = {{(-4L),(-1L),(-4L),0x49FC66F1L,(-1L),0x49FC66F1L,(-4L)},{(-1L),(-1L),(-6L),(-1L),(-1L),(-6L),(-1L)},{(-1L),0x49FC66F1L,(-4L),(-1L),(-4L),0x49FC66F1L,(-1L)},{(-7L),(-1L),(-7L),(-7L),(-1L),(-7L),(-7L)},{(-1L),(-1L),6L,(-1L),(-1L),0x055C2145L,(-1L)},{(-1L),(-7L),(-7L),(-1L),(-7L),(-7L),(-1L)}};
            uint8_t l_1375 = 0x0DL;
            int i, j, k;
            for (g_557 = 10; (g_557 <= 44); g_557++)
            { /* block id: 587 */
                int16_t l_1250 = 0x9C52L;
                int32_t l_1252 = 0x68F89D76L;
                int32_t l_1253 = 1L;
                for (g_556 = (-12); (g_556 >= 3); g_556 = safe_add_func_uint32_t_u_u(g_556, 5))
                { /* block id: 590 */
                    int64_t l_1251 = (-1L);
                    int32_t l_1254 = 0x774E538FL;
                    int64_t l_1289[9];
                    int8_t * const l_1295 = &g_849[9][3];
                    int i;
                    for (i = 0; i < 9; i++)
                        l_1289[i] = 4L;
                    (*g_33) ^= (safe_rshift_func_int8_t_s_u((g_141 || p_106), 7));
                    if (((*g_33) = (safe_div_func_uint64_t_u_u(((safe_lshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u(0xD4L, l_1245)), 6)) & (g_23 ^ ((safe_mul_func_int8_t_s_s(((l_1245 , 0xBFL) || (safe_rshift_func_int16_t_s_u(((l_1250 != (l_1251 = p_106)) >= ((((*l_885) = (l_1172 & (&l_1182[7][4] != &l_1161))) & p_106) | p_106)), 10))), 6UL)) ^ l_1252))), (-3L)))))
                    { /* block id: 595 */
                        uint8_t *l_1274[7] = {&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132};
                        int32_t l_1278 = (-1L);
                        int32_t *l_1279 = &g_738;
                        int i;
                        l_1253 = ((&g_655 != &g_655) <= 0xC.F04BCAp+38);
                        l_1254 &= p_106;
                        (*l_1279) = ((((((((safe_lshift_func_uint8_t_u_u((0x2B76L ^ (-1L)), 1)) != (((safe_add_func_uint16_t_u_u(l_1223[0], (safe_mul_func_uint8_t_u_u((((*g_33) = ((*l_885) = (safe_sub_func_int32_t_s_s((*l_885), (l_1252 & (!((safe_rshift_func_int16_t_s_s(((p_106 || ((safe_rshift_func_int16_t_s_s(((l_1277 = (safe_mul_func_uint8_t_u_u((g_226 = (safe_lshift_func_int8_t_s_u((safe_mod_func_uint32_t_u_u((*l_885), p_106)), 2))), ((safe_lshift_func_int16_t_s_s(0x5EE2L, (*g_1200))) == 0xC50124A0L)))) == p_106), 5)) || l_935[0])) , (**g_1199)), (*g_1200))) , l_1277))))))) <= (-1L)), p_106)))) , l_1277) | 3L)) , 3L) | (**g_347)) >= l_942[6]) != 0UL) != 0xB1CA55FBL) && l_1278);
                        (*g_1281) = (*g_1207);
                    }
                    else
                    { /* block id: 604 */
                        float l_1298 = 0x0.2p+1;
                        int32_t l_1305 = 0x6043F437L;
                        uint16_t *l_1313 = &g_1085;
                        uint8_t l_1324 = 3UL;
                        uint16_t *l_1326 = &g_974;
                        int32_t *l_1328[6][4][8] = {{{&l_927,&g_137,(void*)0,&l_1111,&l_881,&l_884[0],&l_1111,(void*)0},{(void*)0,(void*)0,&g_738,&l_935[2],&l_882[2][8],(void*)0,(void*)0,&l_1111},{(void*)0,(void*)0,(void*)0,&l_882[2][8],&l_1277,&l_935[0],(void*)0,&l_935[0]},{&l_884[1],&l_927,&l_884[0],&l_927,&l_884[1],(void*)0,&l_1111,&l_884[2]}},{{(void*)0,&l_1277,&l_882[2][5],&l_884[4],&l_935[0],&l_882[2][5],&l_884[0],(void*)0},{&l_1253,(void*)0,&l_882[2][5],&l_1253,&g_137,&l_882[2][5],&l_1111,&l_884[0]},{&l_935[0],&l_882[2][5],&l_882[2][5],&l_884[2],(void*)0,&l_1111,(void*)0,(void*)0},{(void*)0,&l_935[0],&g_4,(void*)0,&l_884[2],&g_255,&l_884[3],(void*)0}},{{&l_1111,&l_927,(void*)0,&g_738,&g_255,&l_882[2][5],&l_935[0],&g_255},{(void*)0,&l_1111,(void*)0,(void*)0,&l_1277,&l_884[1],&l_1277,&l_1277},{(void*)0,&l_1277,&l_884[0],&l_884[0],&l_1277,(void*)0,&l_927,&l_935[2]},{&l_1111,(void*)0,&l_882[2][5],&l_882[2][5],&l_884[3],&l_1277,&l_935[0],&g_137}},{{&l_1277,&l_935[2],(void*)0,&l_882[2][5],(void*)0,&g_137,(void*)0,&l_935[2]},{&g_255,(void*)0,&l_1172,&l_884[0],&l_1253,(void*)0,(void*)0,&l_1277},{&l_1253,&l_882[2][5],&g_137,(void*)0,&g_255,(void*)0,&l_1277,&g_255},{&g_738,(void*)0,(void*)0,&g_738,&l_935[2],&l_882[2][8],(void*)0,(void*)0}},{{&g_137,&l_1253,&l_1277,(void*)0,&l_935[0],(void*)0,&g_255,(void*)0},{&l_882[2][5],&g_137,&l_882[2][5],&l_884[2],&l_882[2][5],&g_137,&l_882[2][5],&l_884[0]},{(void*)0,(void*)0,&l_881,&l_1253,&g_738,&l_1254,&l_1277,(void*)0},{(void*)0,(void*)0,&l_884[3],&l_884[4],&g_738,(void*)0,&l_884[4],&l_884[2]}},{{(void*)0,&g_4,(void*)0,(void*)0,&l_882[2][5],&l_927,&l_884[2],&l_1277},{&l_882[2][5],&l_935[2],&l_1254,(void*)0,&l_935[0],(void*)0,(void*)0,(void*)0},{&g_137,(void*)0,(void*)0,(void*)0,&l_935[2],(void*)0,(void*)0,&l_884[2]},{&g_738,&g_255,&l_882[2][5],&l_935[0],&g_255,&l_882[2][5],&l_1253,(void*)0}}};
                        int i, j, k;
                        l_935[1] ^= ((safe_rshift_func_int8_t_s_u((!(safe_unary_minus_func_int32_t_s(0xCD589D69L))), (l_1254 , (((0L || (*l_885)) == (0xB63474E96CC5D3A0LL & (g_226 , (safe_lshift_func_int8_t_s_u((l_1250 < (**g_1106)), 0))))) > (!p_106))))) , l_1251);
                        (*g_33) &= ((l_1289[5] && (safe_sub_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((((*g_94) <= ((*l_885) = l_935[3])) , (((*l_1158) = p_106) < ((((~((0x99D9L > ((***g_864) ^ p_106)) , ((void*)0 == l_1295))) | 0UL) && (**g_347)) <= p_106))), p_106)), l_1252))) != l_1277);
                        l_884[2] |= ((safe_div_func_int8_t_s_s((0xDBL | (*l_885)), (safe_add_func_uint16_t_u_u(((*l_1326) &= (safe_div_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(l_1305, (((safe_mod_func_uint32_t_u_u((safe_add_func_uint8_t_u_u((+(safe_add_func_uint16_t_u_u((8L && ((((((*l_1313) = ((void*)0 != (*l_1157))) == (((((safe_mul_func_int16_t_s_s(((((0xD1L > ((safe_rshift_func_uint8_t_u_u(((~p_106) > (+(((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_int8_t_s_u((((((l_1040 <= (*g_33)) != p_106) & 5UL) > l_1305) | 1UL), l_1324)) <= 0x71480AD5L), (*l_885))) , (*l_885)) , (*p_107)))), 6)) , l_1325[1][3][3])) ^ 0xFE04L) | 0xF8L) , (-1L)), p_106)) ^ p_106) , (*g_863)) == (void*)0) , l_1324)) | l_1252) , (*g_1107)) > (*g_1107))), p_106))), (-1L))), 0x38BE669BL)) == p_106) >= l_1289[5]))), p_106))), l_1327)))) <= g_226);
                        (*g_33) |= 0xE713BDFCL;
                    }
                    (*l_885) |= (safe_rshift_func_int16_t_s_s(p_106, 4));
                }
            }
            for (l_881 = (-30); (l_881 >= (-4)); l_881 = safe_add_func_int32_t_s_s(l_881, 6))
            { /* block id: 619 */
                uint32_t l_1342[9];
                int32_t l_1374 = 0xB704E920L;
                int64_t *l_1378 = (void*)0;
                int i;
                for (i = 0; i < 9; i++)
                    l_1342[i] = 0x63BF3AB4L;
                if ((4294967286UL || ((*g_1000) <= ((**g_1199) & p_106))))
                { /* block id: 620 */
                    uint16_t l_1337 = 65535UL;
                    int32_t l_1341 = 1L;
                    for (p_106 = 18; (p_106 <= 22); p_106 = safe_add_func_uint32_t_u_u(p_106, 4))
                    { /* block id: 623 */
                        int32_t *l_1335 = &g_255;
                        int32_t *l_1336 = (void*)0;
                        int32_t *l_1340[7][1][1];
                        int i, j, k;
                        for (i = 0; i < 7; i++)
                        {
                            for (j = 0; j < 1; j++)
                            {
                                for (k = 0; k < 1; k++)
                                    l_1340[i][j][k] = (void*)0;
                            }
                        }
                        ++l_1337;
                        --l_1342[0];
                    }
                    for (g_255 = 0; (g_255 <= 3); g_255 += 1)
                    { /* block id: 629 */
                        return l_1210;
                    }
                    for (g_226 = 0; (g_226 <= 5); g_226 += 1)
                    { /* block id: 634 */
                        uint64_t l_1345[6] = {0x54F4805F94CC74FFLL,0x54F4805F94CC74FFLL,0xE9A8E7D506395895LL,0x54F4805F94CC74FFLL,0x54F4805F94CC74FFLL,0xE9A8E7D506395895LL};
                        int i;
                        l_935[3] = ((*g_33) |= l_1345[2]);
                        if (p_106)
                            break;
                        return l_1210;
                    }
                }
                else
                { /* block id: 640 */
                    uint16_t l_1358 = 6UL;
                    int32_t *l_1364 = &l_1111;
                    (*l_1364) |= (safe_mod_func_int64_t_s_s((**g_1045), ((*l_1210) = (safe_sub_func_uint8_t_u_u((p_106 <= (((safe_rshift_func_int8_t_s_u(p_106, (((safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u(((*p_107) , (safe_sub_func_uint64_t_u_u(l_1358, (safe_sub_func_int16_t_s_s(((p_106 < ((safe_rshift_func_int8_t_s_u((*g_1000), 7)) >= (0x8DB9L != (((((*l_885) || l_1325[1][3][3]) && p_106) > 0xD24FL) < g_1363)))) != (-3L)), 0x1718L))))), l_1358)), 5)) | 0xF0C1D540L) , l_1161))) < p_106) ^ 0x39FC90AAL)), l_1325[1][3][3])))));
                }
                for (l_1327 = 0; (l_1327 == 14); l_1327 = safe_add_func_uint64_t_u_u(l_1327, 1))
                { /* block id: 646 */
                    int32_t *l_1369 = &l_1111;
                    int32_t *l_1370 = &l_935[0];
                    int32_t *l_1371 = &g_1363;
                    int32_t *l_1372[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1372[i] = &l_882[1][0];
                    for (l_879 = (-2); (l_879 <= 9); ++l_879)
                    { /* block id: 649 */
                        if (p_106)
                            break;
                    }
                    l_1375--;
                    return l_1378;
                }
            }
            if (l_1245)
                break;
            if (l_1379)
            { /* block id: 657 */
                int64_t *l_1380 = &g_43[0][3][2];
                return l_1380;
            }
            else
            { /* block id: 659 */
                (*g_1381) = (*g_1207);
            }
        }
        (*l_1382) = p_108;
    }
    return l_1158;
}


/* ------------------------------------------ */
/* 
 * reads : g_95 g_277 g_23 g_226 g_31 g_132 g_264 g_265 g_131 g_33 g_252 g_322 g_255 g_43 g_327 g_141 g_76 g_347 g_160 g_360 g_404 g_94 g_363 g_4 g_438 g_272 g_27 g_348 g_75 g_557 g_593 g_552 g_248 g_137 g_712 g_719 g_738 g_655 g_809
 * writes: g_95 g_76 g_23 g_137 g_33 g_252 g_255 g_347 g_75 g_265 g_132 g_360 g_552 g_556 g_557 g_27 g_141 g_655 g_226 g_711 g_131 g_251 g_835
 */
static uint64_t  func_115(uint32_t * p_116, int16_t  p_117, uint8_t * p_118)
{ /* block id: 42 */
    uint32_t l_142 = 18446744073709551615UL;
    int16_t l_144 = 0x6133L;
    int32_t l_148 = 1L;
    int32_t l_204[2][3][9] = {{{0L,0x6B5423CFL,1L,0xE7F02424L,0xE7F02424L,1L,0x6B5423CFL,0L,3L},{0L,(-1L),1L,(-10L),(-1L),0x26FB7A4CL,0xE7F02424L,0L,1L},{0L,0x6B5423CFL,3L,0L,0xA174EF01L,(-10L),0xA174EF01L,0x59B509E7L,(-1L)}},{{0xA174EF01L,0xA174EF01L,0L,(-1L),(-6L),(-10L),(-1L),0xA174EF01L,(-1L)},{1L,0x59B509E7L,(-1L),(-1L),0xA174EF01L,0x6B5423CFL,1L,1L,0x6B5423CFL},{(-1L),0x59B509E7L,0L,0x59B509E7L,(-1L),0xE7F02424L,8L,(-1L),0x6B5423CFL}}};
    int32_t l_221 = 0x2D112435L;
    uint16_t l_270 = 65529UL;
    uint32_t *l_281[3];
    uint8_t l_286 = 0x2FL;
    uint16_t *l_294 = &l_270;
    int8_t l_315[4];
    uint32_t ***l_368 = (void*)0;
    float l_437 = 0x3.Ap-1;
    int32_t l_443 = 0xA52B89B7L;
    int32_t l_447[7][1][4] = {{{0x344011D6L,0x5C3D7BF4L,0xF89CFCFFL,0x5C3D7BF4L}},{{0x5C3D7BF4L,3L,0xF89CFCFFL,0xF89CFCFFL}},{{0x344011D6L,0x344011D6L,0x5C3D7BF4L,0xF89CFCFFL}},{{0xFA7152C9L,3L,0xFA7152C9L,0x5C3D7BF4L}},{{0xFA7152C9L,0x5C3D7BF4L,0x5C3D7BF4L,0xFA7152C9L}},{{0x344011D6L,0x5C3D7BF4L,0xF89CFCFFL,0x5C3D7BF4L}},{{0x5C3D7BF4L,3L,0xF89CFCFFL,0xF89CFCFFL}}};
    uint32_t l_458 = 0xC393DA23L;
    float l_482 = 0xE.3E7C0Ap+75;
    int32_t **l_518[8][1][2] = {{{&g_33,&g_33}},{{&g_33,&g_33}},{{&g_33,&g_33}},{{&g_33,&g_33}},{{&g_33,&g_33}},{{&g_33,&g_33}},{{&g_33,&g_33}},{{&g_33,&g_33}}};
    uint32_t l_536[7][3] = {{0UL,2UL,0UL},{0UL,2UL,0UL},{0UL,2UL,0UL},{0UL,2UL,0UL},{0UL,2UL,0UL},{0UL,2UL,0UL},{0UL,2UL,0UL}};
    int32_t l_542 = (-1L);
    uint8_t ***l_549 = &g_265;
    int8_t *l_550 = (void*)0;
    int8_t *l_551[7][10][3] = {{{&l_315[1],&l_315[3],&l_315[3]},{&l_315[1],(void*)0,&l_315[0]},{&l_315[0],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[0]},{&l_315[3],&l_315[2],&l_315[1]},{&l_315[0],&l_315[3],&l_315[3]},{&l_315[1],&l_315[1],&l_315[3]},{&l_315[1],&l_315[3],&l_315[3]},{(void*)0,&l_315[3],&l_315[3]},{&l_315[0],&l_315[0],&l_315[1]}},{{(void*)0,(void*)0,&l_315[1]},{&l_315[1],&l_315[2],&l_315[3]},{&l_315[1],&l_315[0],&l_315[1]},{&l_315[0],&l_315[3],(void*)0},{&l_315[3],&l_315[2],&l_315[3]},{&l_315[3],&l_315[1],&l_315[1]},{&l_315[0],&l_315[3],&l_315[0]},{&l_315[1],&l_315[3],(void*)0},{&l_315[1],&l_315[3],&l_315[3]},{(void*)0,&l_315[3],&l_315[0]}},{{&l_315[0],&l_315[3],&l_315[1]},{(void*)0,&l_315[3],&l_315[0]},{&l_315[1],(void*)0,(void*)0},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[0],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[1]},{&l_315[3],&l_315[3],&l_315[1]},{&l_315[0],(void*)0,&l_315[1]},{&l_315[1],&l_315[2],&l_315[3]},{&l_315[1],&l_315[2],(void*)0}},{{(void*)0,&l_315[3],&l_315[1]},{&l_315[3],&l_315[3],(void*)0},{&l_315[3],&l_315[0],&l_315[3]},{&l_315[1],&l_315[1],&l_315[3]},{(void*)0,&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],(void*)0},{&l_315[3],&l_315[1],(void*)0},{&l_315[3],&l_315[3],(void*)0},{(void*)0,&l_315[1],&l_315[3]}},{{&l_315[1],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],(void*)0},{&l_315[3],&l_315[3],&l_315[2]},{&l_315[3],&l_315[3],&l_315[1]},{&l_315[1],&l_315[0],&l_315[3]},{(void*)0,&l_315[3],&l_315[0]},{&l_315[3],&l_315[1],&l_315[3]},{&l_315[3],(void*)0,&l_315[3]},{&l_315[3],&l_315[1],&l_315[2]},{&l_315[3],&l_315[3],&l_315[3]}},{{(void*)0,&l_315[1],(void*)0},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[3],&l_315[1],&l_315[3]},{&l_315[3],(void*)0,&l_315[0]},{&l_315[3],&l_315[1],(void*)0},{&l_315[1],&l_315[3],&l_315[3]},{(void*)0,(void*)0,&l_315[1]},{&l_315[3],(void*)0,&l_315[3]},{&l_315[3],&l_315[3],&l_315[1]},{&l_315[3],&l_315[1],&l_315[0]}},{{&l_315[3],&l_315[0],&l_315[2]},{(void*)0,&l_315[1],&l_315[3]},{&l_315[1],(void*)0,(void*)0},{&l_315[3],(void*)0,&l_315[2]},{&l_315[3],&l_315[3],(void*)0},{&l_315[3],&l_315[0],&l_315[3]},{&l_315[1],&l_315[1],&l_315[3]},{(void*)0,&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],(void*)0}}};
    uint64_t *l_553 = &g_252;
    uint16_t l_554[5][7] = {{0x96D2L,0x040DL,0x96D2L,1UL,0x96D2L,0x040DL,0x96D2L},{8UL,65533UL,0x6850L,0xC50FL,0xC50FL,0x6850L,65533UL},{6UL,0x040DL,0x7F09L,0x040DL,6UL,0x040DL,0x7F09L},{0xC50FL,0xC50FL,0x6850L,65533UL,8UL,8UL,65533UL},{0x96D2L,1UL,0x96D2L,0x040DL,0x96D2L,1UL,0x96D2L}};
    uint64_t l_555 = 0xDA1053C46341C5B8LL;
    int32_t l_679 = (-4L);
    uint64_t l_685 = 18446744073709551612UL;
    const int32_t *l_691 = &l_679;
    float l_707[7][8][4] = {{{0xA.5A6FDCp+63,0x6.58A27Dp+36,0x9.0p+1,0xA.F87E1Bp-33},{0xB.B6E5E8p-64,0x1.Ap+1,0xE.48E2A5p+14,0xA.F87E1Bp-33},{0x0.Ap-1,0x6.58A27Dp+36,0x1.Cp-1,0x6.5p+1},{0x6.9p+1,0x3.7A48B6p+93,0x0.Ap-1,0x9.0p+1},{0x6.4D807Ap+70,0x6.9p+1,0xD.AF01BDp-77,0x8.D3E98Fp-30},{0x0.7p-1,0xE.48E2A5p+14,(-0x1.Cp+1),0xE.48E2A5p+14},{0x0.0p-1,0xA.F87E1Bp-33,0x1.Ap+1,(-0x4.4p+1)},{0x6.4CBEC8p+31,(-0x2.9p+1),(-0x4.4p+1),0x6.9p+1}},{{0x8.D3E98Fp-30,0x0.7p-1,0xA.F87E1Bp-33,0x0.4p+1},{0x8.D3E98Fp-30,0xB.B6E5E8p-64,(-0x4.4p+1),0x6.9p+1},{0x6.9p+1,0xA.5A6FDCp+63,0x6.4D807Ap+70,0x6.5p+1},{0x8.967C9Ap-35,0xD.AF01BDp-77,0x3.7A48B6p+93,0x9.14CC46p+58},{0x3.CAE3ECp+27,0x3.7A48B6p+93,(-0x1.Bp+1),(-0x1.Bp+1)},{0x6.5p+1,0x6.5p+1,0x0.7p-1,0xB.B6E5E8p-64},{0x8.D3E98Fp-30,0x0.4p+1,0x9.0p+1,0x1.Cp-1},{0x0.7p-1,0x0.Ap-1,(-0x1.Ap+1),0x9.0p+1}},{{(-0x1.Cp+1),0x0.Ap-1,0x6.58A27Dp+36,0x1.Cp-1},{0x0.Ap-1,0x0.4p+1,0x9.14CC46p+58,0xB.B6E5E8p-64},{0x6.4CBEC8p+31,0x6.5p+1,0x8.D3E98Fp-30,(-0x1.Bp+1)},{0x6.58A27Dp+36,0x3.7A48B6p+93,0x1.Ap+1,0x9.14CC46p+58},{0x0.4p+1,0xD.AF01BDp-77,0x0.4p+1,0x6.5p+1},{0x9.14CC46p+58,0xA.5A6FDCp+63,0x3.CAE3ECp+27,0x6.9p+1},{(-0x1.Ap+1),(-0x1.Cp+1),(-0x2.5p-1),0xA.5A6FDCp+63},{0x0.0p-1,0x3.CAE3ECp+27,(-0x2.5p-1),0x8.D3E98Fp-30}},{{(-0x1.Ap+1),0xB.B6E5E8p-64,0x3.CAE3ECp+27,0xA.F87E1Bp-33},{0x9.14CC46p+58,0x9.E56FDEp-19,0x0.4p+1,(-0x1.Ap+1)},{0x0.4p+1,(-0x1.Ap+1),0x1.Ap+1,(-0x2.9p+1)},{0x6.58A27Dp+36,0x8.D3E98Fp-30,0x8.D3E98Fp-30,0x6.58A27Dp+36},{0x6.4CBEC8p+31,0x1.Cp-1,0x9.14CC46p+58,(-0x4.4p+1)},{0x0.Ap-1,0x1.Ap+1,0x6.58A27Dp+36,0x9.E56FDEp-19},{(-0x1.Cp+1),0x6.4D807Ap+70,(-0x1.Ap+1),0x9.E56FDEp-19},{0x0.7p-1,0x1.Ap+1,0x9.0p+1,(-0x4.4p+1)}},{{0x8.D3E98Fp-30,0x1.Cp-1,0x0.7p-1,0x6.58A27Dp+36},{0x6.5p+1,0x8.D3E98Fp-30,(-0x1.Bp+1),(-0x2.9p+1)},{0x3.CAE3ECp+27,(-0x1.Ap+1),0x3.7A48B6p+93,(-0x1.Ap+1)},{0x8.967C9Ap-35,0x9.E56FDEp-19,0x6.4D807Ap+70,0xA.F87E1Bp-33},{0x6.9p+1,0xB.B6E5E8p-64,0xA.F87E1Bp-33,0x8.D3E98Fp-30},{(-0x2.9p+1),0x3.CAE3ECp+27,0x9.E56FDEp-19,0xA.5A6FDCp+63},{(-0x2.9p+1),(-0x1.Cp+1),0xA.F87E1Bp-33,0x6.9p+1},{0x6.9p+1,0xA.5A6FDCp+63,0x6.4D807Ap+70,0x6.5p+1}},{{0x8.967C9Ap-35,0xD.AF01BDp-77,0x3.7A48B6p+93,0x9.14CC46p+58},{0x3.CAE3ECp+27,0x3.7A48B6p+93,(-0x1.Bp+1),(-0x1.Bp+1)},{0x6.5p+1,0x6.5p+1,0x0.7p-1,0xB.B6E5E8p-64},{0x8.D3E98Fp-30,0x0.4p+1,0x9.0p+1,0x1.Cp-1},{0x0.7p-1,0x0.Ap-1,(-0x1.Ap+1),0x9.0p+1},{(-0x1.Cp+1),0x0.Ap-1,0x6.58A27Dp+36,0x1.Cp-1},{0x0.Ap-1,0x0.4p+1,0x9.14CC46p+58,0xB.B6E5E8p-64},{0x6.4CBEC8p+31,0x6.5p+1,0x8.D3E98Fp-30,(-0x1.Bp+1)}},{{0x6.58A27Dp+36,0x3.7A48B6p+93,0x1.Ap+1,0x9.14CC46p+58},{0x0.4p+1,0xD.AF01BDp-77,0x0.4p+1,0x6.5p+1},{0x9.14CC46p+58,0xA.5A6FDCp+63,0x3.CAE3ECp+27,0x6.9p+1},{(-0x1.Ap+1),(-0x1.Cp+1),(-0x2.5p-1),0xA.5A6FDCp+63},{0x0.0p-1,0x3.CAE3ECp+27,(-0x2.5p-1),0x8.D3E98Fp-30},{(-0x1.Ap+1),0xB.B6E5E8p-64,0x3.CAE3ECp+27,0xA.F87E1Bp-33},{0x9.14CC46p+58,0x9.E56FDEp-19,0x0.4p+1,(-0x1.Ap+1)},{0x0.4p+1,(-0x1.Ap+1),0x1.Ap+1,(-0x2.9p+1)}}};
    int32_t l_827 = (-1L);
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_281[i] = &g_23;
    for (i = 0; i < 4; i++)
        l_315[i] = 1L;
lbl_406:
    for (g_95 = 0; (g_95 <= 3); g_95 += 1)
    { /* block id: 45 */
        uint8_t l_139 = 0xBDL;
        int32_t l_143 = (-2L);
        const int32_t *l_158 = &g_95;
        const int32_t **l_157 = &l_158;
        uint8_t **l_199[3];
        int32_t l_209 = 0x17869A76L;
        int32_t l_213 = 0x6F889582L;
        int32_t l_214 = 2L;
        int32_t l_215 = (-9L);
        int32_t l_220 = 6L;
        int32_t l_222 = (-1L);
        uint64_t l_223[7][5][6] = {{{0xE0DB0FBA3A9F7FF6LL,8UL,0x204D8F182E843886LL,8UL,0xDF4CC50A3A8EB31CLL,0xCD4D2746254181E0LL},{0UL,18446744073709551615UL,8UL,0xE16E4E52E893FEBCLL,0xE0DB0FBA3A9F7FF6LL,0UL},{0UL,0xE16E4E52E893FEBCLL,0xCD4D2746254181E0LL,8UL,0x832A488D4B9C1E0ALL,4UL},{8UL,0x832A488D4B9C1E0ALL,4UL,0xE16E4E52E893FEBCLL,5UL,0UL},{0xE0DB0FBA3A9F7FF6LL,18446744073709551615UL,0xE16E4E52E893FEBCLL,18446744073709551607UL,5UL,0x822B14FDEF38617DLL}},{{5UL,0x832A488D4B9C1E0ALL,8UL,8UL,0x832A488D4B9C1E0ALL,5UL},{0UL,0xE16E4E52E893FEBCLL,0x23FC29F5B1E060BFLL,18446744073709551615UL,0xE0DB0FBA3A9F7FF6LL,0x822B14FDEF38617DLL},{8UL,18446744073709551615UL,0xCD4D2746254181E0LL,0x204D8F182E843886LL,18446744073709551612UL,0UL},{8UL,1UL,0x204D8F182E843886LL,18446744073709551615UL,5UL,4UL},{0UL,0xE0DB0FBA3A9F7FF6LL,0xE16E4E52E893FEBCLL,8UL,18446744073709551615UL,0UL}},{{5UL,1UL,0x23FC29F5B1E060BFLL,18446744073709551607UL,0x832A488D4B9C1E0ALL,0xCD4D2746254181E0LL},{0xE0DB0FBA3A9F7FF6LL,18446744073709551615UL,0x23FC29F5B1E060BFLL,0xE16E4E52E893FEBCLL,0UL,0UL},{8UL,0xE16E4E52E893FEBCLL,0xE16E4E52E893FEBCLL,8UL,18446744073709551612UL,4UL},{0UL,0x832A488D4B9C1E0ALL,0x204D8F182E843886LL,0xE16E4E52E893FEBCLL,18446744073709551615UL,0UL},{0UL,18446744073709551615UL,0xCD4D2746254181E0LL,18446744073709551607UL,18446744073709551615UL,0x822B14FDEF38617DLL}},{{18446744073709551615UL,0x832A488D4B9C1E0ALL,0x23FC29F5B1E060BFLL,8UL,18446744073709551612UL,5UL},{0xE0DB0FBA3A9F7FF6LL,0xE16E4E52E893FEBCLL,8UL,18446744073709551615UL,0UL,0x822B14FDEF38617DLL},{0UL,18446744073709551615UL,0xE16E4E52E893FEBCLL,0x204D8F182E843886LL,0x832A488D4B9C1E0ALL,0UL},{0UL,1UL,4UL,18446744073709551615UL,18446744073709551615UL,4UL},{0xE0DB0FBA3A9F7FF6LL,0xE0DB0FBA3A9F7FF6LL,0xCD4D2746254181E0LL,8UL,5UL,0UL}},{{18446744073709551615UL,1UL,8UL,18446744073709551607UL,18446744073709551612UL,0xCD4D2746254181E0LL},{0UL,18446744073709551615UL,8UL,0xE16E4E52E893FEBCLL,0xE0DB0FBA3A9F7FF6LL,0UL},{0UL,0xE16E4E52E893FEBCLL,0xCD4D2746254181E0LL,8UL,0x832A488D4B9C1E0ALL,4UL},{8UL,0x832A488D4B9C1E0ALL,4UL,0xE16E4E52E893FEBCLL,5UL,0UL},{0xE0DB0FBA3A9F7FF6LL,18446744073709551615UL,0xE16E4E52E893FEBCLL,18446744073709551607UL,5UL,0x822B14FDEF38617DLL}},{{5UL,0x832A488D4B9C1E0ALL,8UL,8UL,0x832A488D4B9C1E0ALL,5UL},{0UL,0xE16E4E52E893FEBCLL,0x23FC29F5B1E060BFLL,18446744073709551615UL,0xE0DB0FBA3A9F7FF6LL,0x822B14FDEF38617DLL},{8UL,18446744073709551615UL,0xCD4D2746254181E0LL,0x204D8F182E843886LL,18446744073709551612UL,0UL},{8UL,1UL,0x204D8F182E843886LL,18446744073709551615UL,5UL,4UL},{0UL,0xE0DB0FBA3A9F7FF6LL,0xE16E4E52E893FEBCLL,8UL,18446744073709551615UL,0UL}},{{5UL,1UL,0x23FC29F5B1E060BFLL,18446744073709551607UL,0x832A488D4B9C1E0ALL,0xCD4D2746254181E0LL},{0xE0DB0FBA3A9F7FF6LL,18446744073709551615UL,0x23FC29F5B1E060BFLL,0xE16E4E52E893FEBCLL,0UL,0UL},{8UL,0xE16E4E52E893FEBCLL,0xE16E4E52E893FEBCLL,8UL,0UL,0x23FC29F5B1E060BFLL},{0xDF4CC50A3A8EB31CLL,0xE0DB0FBA3A9F7FF6LL,8UL,0x204D8F182E843886LL,8UL,0xDF4CC50A3A8EB31CLL},{5UL,0xE16E4E52E893FEBCLL,4UL,0x832A488D4B9C1E0ALL,8UL,0xCD4D2746254181E0LL}}};
        uint16_t *l_273 = &l_270;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_199[i] = &g_131;
        for (g_76 = 3; (g_76 >= 0); g_76 -= 1)
        { /* block id: 48 */
            int8_t l_164 = 3L;
            uint32_t l_186 = 4294967295UL;
            int32_t l_188 = 0x32BF6445L;
            int16_t l_196 = 0L;
            int32_t l_212 = 0L;
            int32_t l_216 = (-10L);
            int32_t l_218 = 0L;
            for (p_117 = 0; (p_117 <= 3); p_117 += 1)
            { /* block id: 51 */
                float l_145 = (-0x5.3p+1);
                int32_t l_146 = 1L;
                uint8_t **l_156[7] = {&g_131,&g_131,&g_131,&g_131,&g_131,&g_131,&g_131};
                int32_t l_165 = 0x720CA041L;
                float l_166[3][7][5] = {{{0x1.7p+1,(-0x7.9p+1),0x6.DA9005p-76,(-0x10.8p-1),0x6.DA9005p-76},{0x5.Fp+1,0x5.Fp+1,0x7.11A968p+25,0x4.9D27B8p-84,0x5.Fp+1},{0x9.2p-1,(-0x10.8p-1),0x9.2p-1,(-0x7.9p+1),0x7.620F8Fp-27},{0x5.Fp+1,0x3.F184A6p+51,0x3.F184A6p+51,0x5.Fp+1,0xF.9EE90Ap+60},{0x1.7p+1,(-0x10.8p-1),(-0x8.6p-1),(-0x10.8p-1),0x1.7p+1},{0xF.9EE90Ap+60,0x5.Fp+1,0x3.F184A6p+51,0x3.F184A6p+51,0x5.Fp+1},{0x7.620F8Fp-27,(-0x7.9p+1),0x9.2p-1,(-0x10.8p-1),0x9.2p-1}},{{0x5.Fp+1,0x4.9D27B8p-84,0x7.11A968p+25,0x5.Fp+1,0x5.Fp+1},{0x6.DA9005p-76,(-0x10.8p-1),0x6.DA9005p-76,(-0x7.9p+1),0x1.7p+1},{0x5.Fp+1,0xF.9EE90Ap+60,0x3.F184A6p+51,0x4.9D27B8p-84,0xF.9EE90Ap+60},{0x7.620F8Fp-27,(-0x10.8p-1),0x0.5p-1,(-0x10.8p-1),0x7.620F8Fp-27},{0xF.9EE90Ap+60,0x4.9D27B8p-84,0x3.F184A6p+51,0xF.9EE90Ap+60,0x5.Fp+1},{0x1.7p+1,(-0x7.9p+1),0x6.DA9005p-76,(-0x10.8p-1),0x6.DA9005p-76},{0x5.Fp+1,0x5.Fp+1,0x7.11A968p+25,0x4.9D27B8p-84,0x5.Fp+1}},{{0x9.2p-1,(-0x10.8p-1),0x9.2p-1,(-0x7.9p+1),0x7.620F8Fp-27},{0x5.Fp+1,0x3.F184A6p+51,0x3.F184A6p+51,0x5.Fp+1,0xF.9EE90Ap+60},{0x1.7p+1,(-0x10.8p-1),(-0x8.6p-1),(-0x10.8p-1),0x1.7p+1},{0xF.9EE90Ap+60,0x5.Fp+1,0x3.F184A6p+51,0x3.F184A6p+51,0x5.Fp+1},{0x7.620F8Fp-27,(-0x7.9p+1),0x9.2p-1,(-0x10.8p-1),0x9.2p-1},{0x5.Fp+1,0x4.9D27B8p-84,0x7.11A968p+25,0x5.Fp+1,0x5.Fp+1},{0x6.DA9005p-76,(-0x10.8p-1),0x6.DA9005p-76,(-0x7.9p+1),0x1.7p+1}}};
                int64_t *l_187 = (void*)0;
                int32_t l_201[7] = {1L,(-2L),(-2L),1L,(-2L),(-2L),1L};
                int i, j, k;
                for (g_23 = 0; (g_23 <= 3); g_23 += 1)
                { /* block id: 54 */
                    return p_117;
                }
            }
        }
        (*g_277) = ((++(*l_273)) , (*l_158));
        return p_117;
    }
    if ((safe_lshift_func_uint8_t_u_s(((~(g_23++)) == 0UL), ((((safe_mul_func_uint8_t_u_u((65535UL || (l_286 == (safe_rshift_func_int8_t_s_s(0xBDL, 5)))), (safe_div_func_uint16_t_u_u(((g_226 | (&g_141 != ((safe_unary_minus_func_uint16_t_u(((*l_294) = 65527UL))) , (void*)0))) == ((safe_sub_func_int32_t_s_s(p_117, 3UL)) >= l_148)), l_286)))) >= p_117) < p_117) == l_144))))
    { /* block id: 112 */
        uint32_t *l_300 = &g_23;
        int32_t l_309 = (-4L);
        float l_314 = 0x1.DE035Cp+72;
        int32_t **l_334 = &g_33;
        int32_t l_411 = 0x5249A5ACL;
        int32_t l_449 = (-1L);
        int32_t l_450 = (-3L);
        int32_t l_451 = 1L;
        int32_t l_453 = 0xF2908580L;
        int32_t l_454 = 0x825FBDCAL;
        int32_t l_456 = 0x3D5B23B7L;
        int32_t l_457 = 0L;
        uint32_t l_499[3];
        uint64_t l_515 = 0x6C29CCD04E46CC1ELL;
        int i;
        for (i = 0; i < 3; i++)
            l_499[i] = 0xA8135809L;
        if (p_117)
        { /* block id: 113 */
            int32_t **l_297 = &g_33;
            uint32_t **l_301 = &l_281[2];
            (*l_297) = p_116;
            (*l_297) = &l_221;
            (*g_33) = ((*p_118) || (((((p_116 == ((*l_301) = l_300)) || (safe_div_func_uint16_t_u_u((!0x84L), p_117))) , ((g_132 | (((1L & ((p_117 || (safe_mod_func_int32_t_s_s((safe_div_func_uint64_t_u_u(0x9C6337962B1F8A0ELL, l_221)), (*p_116)))) , 0xE3L)) >= p_117) | l_309)) & l_204[0][1][7])) & p_117) , (***g_264)));
        }
        else
        { /* block id: 118 */
            float l_337[5] = {(-0x5.Bp+1),(-0x5.Bp+1),(-0x5.Bp+1),(-0x5.Bp+1),(-0x5.Bp+1)};
            int32_t l_381 = 0xD11A3A0CL;
            int32_t l_383 = 9L;
            float l_422 = (-0x2.2p+1);
            int32_t l_436 = 0x268EFD22L;
            int32_t l_448[2][9] = {{0xD86919F3L,0xB2F26B3DL,0xD86919F3L,7L,0x4DBAB3D3L,0x4DBAB3D3L,7L,0xD86919F3L,0xB2F26B3DL},{0xB2F26B3DL,0xD86919F3L,7L,0x4DBAB3D3L,0x4DBAB3D3L,7L,0xD86919F3L,0xB2F26B3DL,0xD86919F3L}};
            uint8_t ***l_467 = &g_265;
            int i, j;
            for (l_142 = 0; (l_142 >= 55); l_142 = safe_add_func_uint16_t_u_u(l_142, 2))
            { /* block id: 121 */
                int32_t **l_333 = &g_33;
                uint16_t l_384[3][4] = {{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL}};
                int32_t *l_409 = (void*)0;
                int32_t *l_410[3][5] = {{&g_95,&l_309,(void*)0,&g_4,&g_4},{&l_309,&g_95,&l_309,(void*)0,&g_4},{&l_383,&l_309,&g_4,&l_309,&l_383}};
                int i, j;
                if (l_309)
                { /* block id: 122 */
                    uint64_t l_332 = 0x289F323D6819490DLL;
                    int32_t *l_335 = (void*)0;
                    int32_t *l_336[9] = {&g_4,&l_309,&l_309,&g_4,&l_309,&l_309,&g_4,&l_309,&g_137};
                    int16_t *l_365 = (void*)0;
                    int i;
                    for (p_117 = 21; (p_117 >= 24); p_117++)
                    { /* block id: 125 */
                        uint64_t *l_316 = &g_252;
                        uint16_t *l_330[2][3][8] = {{{&l_270,&l_270,&l_270,(void*)0,(void*)0,&l_270,&l_270,&l_270},{&l_270,(void*)0,&l_270,(void*)0,&l_270,&l_270,(void*)0,&l_270},{&l_270,&l_270,(void*)0,&l_270,(void*)0,&l_270,&l_270,(void*)0}},{{&l_270,(void*)0,(void*)0,&l_270,&l_270,&l_270,(void*)0,(void*)0},{(void*)0,&l_270,&l_270,&l_270,&l_270,(void*)0,&l_270,&l_270},{&l_270,&l_270,&l_270,(void*)0,(void*)0,&l_270,&l_270,&l_270}}};
                        float *l_331[10];
                        int i, j, k;
                        for (i = 0; i < 10; i++)
                            l_331[i] = (void*)0;
                        (*g_322) ^= (l_315[3] ^ (l_144 , ((*l_316)++)));
                        l_309 = ((safe_add_func_float_f_f((((p_116 = &g_23) == (g_43[0][3][2] , &g_23)) , ((((safe_mod_func_int64_t_s_s(((0x1.9p-1 >= (l_332 = (g_327 < (g_132 < (safe_add_func_float_f_f(((*p_116) , (l_330[0][1][6] != (void*)0)), g_43[3][3][1])))))) , g_141), p_117)) , 0x8.2C021Dp-8) != 0x8.556ABFp+4) != g_141)), p_117)) != 0x6.Cp-1);
                        l_148 = (l_333 != l_334);
                    }
                    l_221 = ((*g_322) = p_117);
                    for (g_76 = 1; (g_76 <= 4); g_76 += 1)
                    { /* block id: 137 */
                        uint32_t **l_350 = &g_348;
                        uint32_t ***l_349 = &l_350;
                        int16_t *l_355 = &l_144;
                        int32_t l_364 = 0xA0BDD3A9L;
                        int8_t *l_382 = &l_315[0];
                        int i;
                        l_364 = (safe_add_func_uint32_t_u_u(g_76, ((safe_lshift_func_int16_t_s_s((safe_add_func_uint64_t_u_u((((!g_252) & g_255) != ((g_347 = g_347) == ((*l_349) = &g_348))), (safe_add_func_uint32_t_u_u((g_23 > (safe_lshift_func_int16_t_s_s(((*l_355) |= g_160), 9))), (((safe_sub_func_float_f_f(((safe_div_func_uint64_t_u_u((g_360[1][3][0] == (((*g_264) != &p_118) , &g_361[1][1][1])), 0x4567FAADBBC287C2LL)) , 0x3.0F2211p+42), l_337[g_76])) >= l_337[g_76]) , 0x63D11B82L))))), g_43[0][1][1])) > (-1L))));
                        l_148 ^= (l_365 == ((((p_117 | (l_383 = (safe_rshift_func_int8_t_s_s((l_221 = ((l_368 != (p_117 , (void*)0)) <= (safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((***g_264), (((!((safe_rshift_func_uint8_t_u_u((*p_118), (safe_rshift_func_int8_t_s_u((g_141 , ((*l_382) = (!(safe_mul_func_int16_t_s_s((l_381 = ((*l_355) = (p_117 , l_381))), 0x4487L))))), 1)))) > g_255)) , p_117) , 1L))), 5UL)))), 2)))) < p_117) | l_204[0][1][5]) , &g_141));
                        if (l_384[0][0])
                            break;
                    }
                    if (p_117)
                        break;
                }
                else
                { /* block id: 151 */
                    uint8_t * const *l_395 = &g_131;
                    int32_t l_402 = 0x7D43EE36L;
                    for (g_95 = (-9); (g_95 <= (-30)); --g_95)
                    { /* block id: 154 */
                        uint32_t l_403[7] = {0x2EB8970CL,0x2EB8970CL,0x2EB8970CL,0x2EB8970CL,0x2EB8970CL,0x2EB8970CL,0x2EB8970CL};
                        int32_t l_405 = 0xEC1892ABL;
                        int i;
                        (*g_404) = ((((-2L) > (g_43[4][1][0] & ((((!((~0x88L) , l_383)) , ((safe_mod_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(l_315[1], (l_395 == (*g_264)))), (safe_add_func_int32_t_s_s((((safe_div_func_uint32_t_u_u((3UL > (safe_lshift_func_uint16_t_u_u(l_402, g_141))), l_403[5])) , p_117) == l_403[5]), p_117)))) , (-10L))) != 8UL) < g_43[5][3][2]))) , 0xD9L) < l_383);
                        l_405 &= l_402;
                    }
                    if (p_117)
                        break;
                    if (l_142)
                        goto lbl_406;
                }
                l_411 = (l_309 = (safe_lshift_func_int16_t_s_s(p_117, 2)));
                for (l_383 = 2; (l_383 <= 6); l_383 += 1)
                { /* block id: 165 */
                    int16_t l_434 = 7L;
                    int32_t l_440 = 0x4BEFF9A2L;
                    int32_t l_441 = (-10L);
                    int32_t l_442 = 0xBA58C7C7L;
                    int32_t l_452[8][6] = {{(-6L),0x783C2FE7L,0x20026B50L,0x20026B50L,0x783C2FE7L,(-6L)},{(-10L),(-6L),1L,0x783C2FE7L,1L,(-6L)},{1L,(-10L),0x20026B50L,(-1L),(-1L),0x20026B50L},{1L,1L,(-1L),0x783C2FE7L,0x21DBE675L,0x783C2FE7L},{(-10L),1L,(-10L),0x20026B50L,(-1L),(-1L)},{(-6L),(-10L),(-10L),(-6L),1L,0x783C2FE7L},{0x783C2FE7L,(-6L),(-1L),(-6L),0x783C2FE7L,0x20026B50L},{(-6L),0x783C2FE7L,0x20026B50L,0x20026B50L,0x783C2FE7L,(-6L)}};
                    float l_455 = 0x0.6AE8D6p+98;
                    int i, j;
                    if (p_117)
                        break;
                    for (g_23 = 0; (g_23 <= 1); g_23 += 1)
                    { /* block id: 169 */
                        int8_t *l_418 = &l_315[3];
                        uint8_t *l_419[4] = {&g_31,&g_31,&g_31,&g_31};
                        uint8_t l_423 = 255UL;
                        int32_t l_435 = 0xA1927647L;
                        int8_t l_439 = 0xD9L;
                        int32_t l_444 = 0x23F7150EL;
                        int32_t l_445 = 0x864AA925L;
                        int32_t l_446[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_446[i] = 0x45FCD5E2L;
                        if ((*g_94))
                            break;
                        (*g_438) = ((p_117 == (((((((((((safe_mod_func_int64_t_s_s(((safe_rshift_func_int16_t_s_s((-9L), (((safe_mod_func_int8_t_s_s(((*l_418) ^= (-1L)), (l_381 &= (*g_131)))) > (safe_div_func_int32_t_s_s((l_423 = p_117), (safe_sub_func_uint8_t_u_u(((((0x71L != g_43[0][3][2]) && (safe_rshift_func_int8_t_s_u((l_436 = (((g_95 != ((safe_add_func_uint64_t_u_u((safe_div_func_int32_t_s_s(((safe_mod_func_uint64_t_u_u(g_363, g_23)) ^ l_434), 4294967295UL)), p_117)) <= l_435)) & l_434) < l_383)), (**g_265)))) && l_436) && 0xA808844ED671C620LL), (**g_265)))))) < p_117))) == p_117), p_117)) || (***g_264)) <= g_4) || p_117) == g_226) == 0x794AB9A4L) >= p_117) == 9L) < g_132) && 0x8FEEL) , 1L)) , p_117);
                        if (p_117)
                            break;
                        ++l_458;
                    }
                }
                for (l_453 = 0; (l_453 == 3); ++l_453)
                { /* block id: 182 */
                    uint8_t ***l_466 = &g_265;
                    uint8_t ****l_465 = &l_466;
                    const uint8_t ***l_468 = (void*)0;
                    float *l_475 = &l_337[4];
                    int32_t l_476 = (-1L);
                    int32_t l_477 = 5L;
                    float *l_478 = &g_75[1][6];
                    float *l_479 = &l_314;
                    uint32_t *l_500 = &l_458;
                    uint8_t l_501 = 0xE4L;
                    (*l_479) = (0x4.71C384p-22 > (((safe_add_func_float_f_f((((*l_465) = (void*)0) == (((void*)0 == l_467) , l_468)), (safe_div_func_float_f_f(((*l_478) = ((l_476 = (((safe_mul_func_float_f_f((((*l_475) = (l_270 , ((safe_add_func_float_f_f((l_383 > 0x3.1p-1), p_117)) <= p_117))) >= (*g_272)), p_117)) < 0x6.A15BBFp+3) < g_160)) > l_477)), (-0x1.9p+1))))) <= l_477) > p_117));
                    (*g_277) = (safe_mul_func_uint8_t_u_u(((p_117 & p_117) || 0x61L), ((safe_rshift_func_int8_t_s_u((l_436 && (safe_rshift_func_uint8_t_u_s(0x39L, 2))), 3)) && (0xF2L || (safe_mul_func_uint16_t_u_u((((((safe_mod_func_uint32_t_u_u(((safe_mod_func_int16_t_s_s((safe_mod_func_int32_t_s_s(((l_148 &= (((safe_div_func_float_f_f(p_117, ((0x8.4A8A5Cp+36 != (((((*l_467) = (((*l_500) = (safe_add_func_uint16_t_u_u(((*l_294) = ((((((**g_347) ^= 0x0BC30D72L) != 0x915107CCL) , l_204[0][1][7]) <= g_141) | l_499[1])), l_448[1][7]))) , &p_118)) != &p_118) == (-0x3.Fp-1)) == (-0x7.6p+1))) <= (*g_438)))) , l_501) != p_117)) > 0x27575E4DL), p_117)), 0xECB7L)) && l_381), 8L)) & 0x91L) | l_204[0][1][7]) <= l_447[1][0][0]) , l_448[1][6]), (-5L)))))));
                    (*l_475) = l_204[1][2][8];
                }
            }
        }
        (*l_334) = (*l_334);
        for (g_132 = (-16); (g_132 == 45); ++g_132)
        { /* block id: 201 */
            int64_t l_508 = (-1L);
            int32_t l_509 = 0x91283DDDL;
            uint32_t l_510 = 4UL;
            for (l_451 = 0; (l_451 >= 12); l_451 = safe_add_func_uint16_t_u_u(l_451, 9))
            { /* block id: 204 */
                int32_t *l_506 = (void*)0;
                int32_t *l_507[4] = {&l_411,&l_411,&l_411,&l_411};
                int i;
                l_510--;
            }
            for (l_457 = 0; l_457 < 2; l_457 += 1)
            {
                for (l_508 = 0; l_508 < 8; l_508 += 1)
                {
                    for (l_449 = 0; l_449 < 5; l_449 += 1)
                    {
                        g_360[l_457][l_508][l_449] = &g_361[2][1][4];
                    }
                }
            }
        }
        if (p_117)
        { /* block id: 209 */
            int32_t *l_513 = &l_450;
            int32_t *l_514[8] = {&l_449,&l_457,&l_457,&l_449,&l_457,&l_457,&l_449,&l_457};
            int i;
            l_515++;
            (*l_513) ^= p_117;
            (*l_334) = p_116;
        }
        else
        { /* block id: 213 */
            float *l_520 = &g_75[0][3];
            int32_t l_523 = 0x62988685L;
            (*l_334) = p_116;
            (*l_520) = ((void*)0 != l_518[1][0][0]);
            for (l_144 = (-3); (l_144 < (-27)); l_144 = safe_sub_func_uint16_t_u_u(l_144, 4))
            { /* block id: 218 */
                return l_523;
            }
        }
    }
    else
    { /* block id: 222 */
        uint32_t l_524 = 0xF0C3E78EL;
        l_524--;
        for (p_117 = 26; (p_117 > 10); --p_117)
        { /* block id: 226 */
            return p_117;
        }
    }
    if ((safe_unary_minus_func_uint64_t_u((safe_mul_func_int8_t_s_s((-4L), (g_557 |= (((&g_265 == &g_265) , ((safe_mul_func_uint8_t_u_u((((((void*)0 == &g_43[0][3][2]) , (g_556 = ((safe_add_func_int8_t_s_s((((l_536[1][2] , (safe_rshift_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u(((((((((~l_542) == ((l_554[2][0] ^= (safe_rshift_func_uint16_t_u_u((((*l_553) = ((safe_lshift_func_int16_t_s_u(p_117, 10)) < (safe_add_func_uint8_t_u_u((((((g_552 = ((((*p_116) , (void*)0) == l_549) | 0x171BL)) < 0x34L) || (-1L)) , 1UL) , 0xC6L), (***g_264))))) || p_117), p_117))) < g_4)) , g_23) & p_117) & g_141) ^ l_555) < (**g_347)) & 0x8CL), (-5L))) || p_117) , (*p_118)), (*p_118)))) <= g_43[1][3][0]) == 0xA90B229A7B6F1853LL), p_117)) && p_117))) | (**g_265)) & p_117), (*p_118))) || 0x5ABDL)) , 2L)))))))
    { /* block id: 235 */
        int16_t l_560 = 0x3D43L;
        int32_t l_566[6][4] = {{(-7L),(-7L),(-7L),(-7L)},{(-7L),(-7L),(-7L),(-7L)},{(-7L),(-7L),(-7L),(-7L)},{(-7L),(-7L),(-7L),(-7L)},{(-7L),(-7L),(-7L),(-7L)},{(-7L),(-7L),(-7L),(-7L)}};
        int32_t *l_607 = (void*)0;
        const int8_t *l_645[8][5][5] = {{{&l_315[2],(void*)0,(void*)0,&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[3],&l_315[3],&l_315[3]},{&l_315[0],(void*)0,(void*)0,&l_315[1],&l_315[3]},{&l_315[3],&l_315[3],&l_315[0],&l_315[3],&l_315[3]},{&l_315[3],(void*)0,&l_315[3],&l_315[1],&l_315[3]}},{{&l_315[3],&l_315[3],&l_315[3],&l_315[3],(void*)0},{(void*)0,(void*)0,&l_315[3],&l_315[3],&l_315[2]},{&l_315[3],&l_315[3],&l_315[3],(void*)0,&l_315[3]},{&l_315[0],&l_315[1],&l_315[3],(void*)0,&l_315[3]},{&l_315[3],&l_315[2],&l_315[3],&l_315[3],&l_315[2]}},{{&l_315[1],(void*)0,(void*)0,&l_315[3],&l_315[3]},{(void*)0,&l_315[3],&l_315[0],&l_315[3],&l_315[1]},{(void*)0,&l_315[1],&l_315[2],&l_315[1],(void*)0},{&l_315[0],&l_315[3],&l_315[3],&l_315[0],&l_315[3]},{&l_315[2],(void*)0,&l_315[3],&l_315[1],&l_315[3]}},{{&l_315[3],(void*)0,&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[1],(void*)0,(void*)0,(void*)0},{&l_315[3],&l_315[0],&l_315[3],&l_315[3],&l_315[1]},{&l_315[3],&l_315[0],&l_315[3],&l_315[3],&l_315[3]},{&l_315[0],&l_315[3],&l_315[1],(void*)0,&l_315[2]}},{{&l_315[3],&l_315[3],&l_315[1],&l_315[1],&l_315[1]},{&l_315[3],&l_315[3],&l_315[3],&l_315[2],&l_315[3]},{&l_315[3],(void*)0,&l_315[0],&l_315[3],&l_315[2]},{&l_315[3],&l_315[0],&l_315[3],&l_315[3],&l_315[0]},{&l_315[2],(void*)0,&l_315[3],&l_315[3],&l_315[3]}},{{&l_315[0],&l_315[3],&l_315[0],&l_315[3],&l_315[3]},{(void*)0,&l_315[3],&l_315[3],&l_315[1],&l_315[1]},{(void*)0,&l_315[3],&l_315[3],&l_315[2],&l_315[3]},{&l_315[1],&l_315[0],&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[0],&l_315[0],&l_315[3],&l_315[3]}},{{&l_315[3],&l_315[1],&l_315[3],&l_315[3],(void*)0},{&l_315[3],(void*)0,&l_315[3],&l_315[1],&l_315[3]},{&l_315[3],(void*)0,&l_315[0],&l_315[3],&l_315[3]},{&l_315[2],&l_315[3],&l_315[3],&l_315[3],&l_315[2]},{&l_315[0],&l_315[1],&l_315[1],&l_315[3],&l_315[2]}},{{&l_315[3],&l_315[3],&l_315[1],&l_315[2],&l_315[3]},{&l_315[3],(void*)0,&l_315[3],&l_315[1],&l_315[2]},{(void*)0,&l_315[2],&l_315[3],&l_315[3],&l_315[2]},{&l_315[2],(void*)0,(void*)0,&l_315[3],&l_315[3]},{(void*)0,&l_315[3],&l_315[3],&l_315[3],&l_315[3]}}};
        int8_t *l_648[10][7][3] = {{{&l_315[1],&l_315[3],&l_315[0]},{&l_315[3],&l_315[0],&l_315[3]},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[1],&l_315[0],&l_315[1]},{&l_315[3],&l_315[3],&l_315[3]}},{{&l_315[1],(void*)0,&l_315[3]},{&l_315[1],&l_315[1],&l_315[0]},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[1],&l_315[3]},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[0],(void*)0,&l_315[1]}},{{&l_315[3],&l_315[0],&l_315[3]},{&l_315[0],&l_315[0],&l_315[3]},{&l_315[0],&l_315[1],(void*)0},{(void*)0,&l_315[1],&l_315[2]},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[1],&l_315[0]},{&l_315[3],&l_315[1],&l_315[3]}},{{&l_315[3],&l_315[3],(void*)0},{&l_315[3],&l_315[1],&l_315[3]},{(void*)0,&l_315[1],(void*)0},{&l_315[3],&l_315[0],&l_315[0]},{(void*)0,&l_315[0],&l_315[1]},{(void*)0,(void*)0,&l_315[0]},{&l_315[3],&l_315[3],&l_315[0]}},{{&l_315[3],(void*)0,&l_315[3]},{&l_315[1],&l_315[2],&l_315[1]},{(void*)0,(void*)0,&l_315[3]},{&l_315[1],&l_315[0],&l_315[0]},{&l_315[3],&l_315[1],&l_315[3]},{&l_315[3],&l_315[1],&l_315[1]},{(void*)0,&l_315[3],&l_315[3]}},{{&l_315[3],&l_315[3],&l_315[0]},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],(void*)0,&l_315[1]},{&l_315[1],&l_315[3],&l_315[3]},{(void*)0,&l_315[1],&l_315[0]},{&l_315[3],&l_315[1],&l_315[0]},{&l_315[1],&l_315[1],&l_315[1]}},{{&l_315[1],&l_315[3],&l_315[0]},{&l_315[1],&l_315[3],(void*)0},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],(void*)0,(void*)0},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[3],&l_315[3],&l_315[0]},{&l_315[3],&l_315[3],&l_315[3]}},{{&l_315[3],(void*)0,&l_315[2]},{&l_315[3],(void*)0,(void*)0},{&l_315[3],&l_315[3],&l_315[3]},{&l_315[1],&l_315[1],&l_315[3]},{&l_315[1],&l_315[3],&l_315[1]},{&l_315[1],&l_315[3],&l_315[3]},{&l_315[3],&l_315[2],(void*)0}},{{(void*)0,&l_315[0],&l_315[2]},{&l_315[1],&l_315[1],&l_315[1]},{&l_315[3],&l_315[0],&l_315[3]},{&l_315[3],(void*)0,&l_315[1]},{&l_315[3],&l_315[1],&l_315[3]},{(void*)0,&l_315[3],&l_315[0]},{&l_315[3],&l_315[1],&l_315[0]}},{{&l_315[3],(void*)0,(void*)0},{&l_315[1],&l_315[0],&l_315[1]},{(void*)0,&l_315[1],&l_315[3]},{&l_315[1],&l_315[0],&l_315[3]},{&l_315[3],&l_315[2],&l_315[3]},{&l_315[3],&l_315[3],&l_315[1]},{(void*)0,&l_315[3],&l_315[3]}}};
        float l_666 = 0x0.Fp-1;
        uint16_t * const l_739[8][2][3] = {{{&l_270,&l_554[2][0],&l_554[1][6]},{&l_270,&l_554[2][0],(void*)0}},{{&l_270,&l_554[2][0],&l_270},{&l_270,&l_554[2][0],&l_554[1][6]}},{{&l_270,&l_554[2][0],(void*)0},{&l_270,&l_554[2][0],&l_554[2][0]}},{{&l_554[2][0],&l_270,(void*)0},{&l_554[2][0],(void*)0,&l_270}},{{&l_554[2][0],&l_554[1][6],&l_554[2][0]},{&l_554[2][0],&l_270,(void*)0}},{{&l_554[2][0],(void*)0,&l_270},{&l_554[2][0],&l_554[1][6],&l_554[2][0]}},{{&l_554[2][0],&l_270,(void*)0},{&l_554[2][0],(void*)0,&l_270}},{{&l_554[2][0],&l_554[1][6],&l_554[2][0]},{&l_554[2][0],&l_270,(void*)0}}};
        float *l_770 = &l_707[0][7][3];
        float *l_775 = (void*)0;
        float *l_776 = &l_666;
        uint32_t l_777[4] = {0UL,0UL,0UL,0UL};
        float *l_778 = (void*)0;
        float *l_779 = (void*)0;
        float *l_780 = &l_437;
        float *l_781 = (void*)0;
        float *l_783[7][6][4] = {{{(void*)0,&g_75[1][6],&g_251,&g_27},{&l_482,&g_27,&g_27,(void*)0},{&l_482,&g_75[1][6],&g_251,&g_251},{(void*)0,(void*)0,&l_482,&g_27},{(void*)0,&l_482,(void*)0,&g_251},{&g_251,&g_251,&g_27,(void*)0}},{{(void*)0,&g_251,(void*)0,&g_251},{&g_251,&l_482,&g_251,&g_27},{&g_75[1][6],(void*)0,&g_75[1][6],&g_251},{&g_28,&g_75[1][6],&g_27,(void*)0},{&g_251,&g_27,&g_27,&g_27},{&g_28,&g_75[1][6],&g_75[1][6],&g_28}},{{&g_75[1][6],&g_251,&g_251,&l_482},{&g_251,&l_482,(void*)0,&g_27},{(void*)0,&l_482,&g_27,&g_27},{&g_251,&l_482,(void*)0,&l_482},{(void*)0,&g_251,&l_482,&g_28},{(void*)0,&g_75[1][6],&g_251,&g_27}},{{&l_482,&g_27,&g_27,(void*)0},{&l_482,&g_75[1][6],&g_251,&g_251},{(void*)0,(void*)0,&l_482,&g_27},{(void*)0,&l_482,(void*)0,&g_251},{&g_251,&g_251,&g_27,(void*)0},{(void*)0,&g_251,(void*)0,&g_251}},{{&g_251,&l_482,&g_251,&g_27},{&g_75[1][6],(void*)0,&g_75[1][6],&g_251},{&g_28,&g_75[1][6],&g_27,(void*)0},{&g_251,&g_27,&g_27,&g_27},{&g_28,&g_75[1][6],&g_75[1][6],&g_28},{&g_75[1][6],&g_251,&g_251,&l_482}},{{&g_251,&l_482,(void*)0,&g_27},{(void*)0,&l_482,&g_27,&g_27},{&g_251,&l_482,(void*)0,&l_482},{(void*)0,&g_251,&l_482,&g_28},{(void*)0,&g_75[1][6],&g_251,&g_27},{&l_482,&g_27,&g_27,(void*)0}},{{&l_482,&g_75[1][6],&g_251,&g_251},{(void*)0,(void*)0,&l_482,&g_27},{(void*)0,&l_482,(void*)0,&g_251},{&g_251,&g_75[1][6],&g_27,&g_251},{&g_251,&g_75[1][6],(void*)0,&g_28},{&g_75[1][6],&g_27,&g_75[1][6],&g_75[1][6]}}};
        int i, j, k;
        if ((p_117 & (0xA9154283D9E84E47LL & p_117)))
        { /* block id: 236 */
            int16_t * const l_563 = &l_560;
            const int32_t l_587 = 0xB87FFF08L;
            const int64_t l_592[9] = {0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL,0xA7ED9374FF80EC02LL};
            int32_t l_597[8][2] = {{0x4FF17FD9L,0x6C5E1BB3L},{(-1L),0x6C5E1BB3L},{0x4FF17FD9L,0xB45B4236L},{0xB45B4236L,0x4FF17FD9L},{0x6C5E1BB3L,(-1L)},{0x6C5E1BB3L,0x4FF17FD9L},{0xB45B4236L,0xB45B4236L},{0x4FF17FD9L,0x6C5E1BB3L}};
            uint64_t l_599 = 0UL;
            int32_t *l_609 = &l_443;
            uint32_t l_636 = 4294967295UL;
            uint32_t *l_677 = &l_142;
            uint8_t l_678 = 255UL;
            int8_t *l_693 = &l_315[3];
            int32_t * volatile l_713 = &l_597[1][1];/* VOLATILE GLOBAL l_713 */
            uint64_t l_731 = 18446744073709551608UL;
            int i, j;
            l_566[2][3] |= (safe_mod_func_uint64_t_u_u((l_560 , ((safe_mod_func_int16_t_s_s((((*l_553) = p_117) > (l_563 == &l_144)), 0xBF0DL)) ^ (--(**g_347)))), 0x421DBD94C7CA80F0LL));
            if ((safe_div_func_uint16_t_u_u(l_560, (g_557 || p_117))))
            { /* block id: 240 */
                int8_t l_588 = 0xC4L;
                int32_t *l_596 = &l_566[2][3];
                int32_t l_598[8] = {0xB44950D6L,0xB44950D6L,0xB44950D6L,0xB44950D6L,0xB44950D6L,0xB44950D6L,0xB44950D6L,0xB44950D6L};
                int i;
                for (p_117 = (-9); (p_117 >= (-30)); p_117 = safe_sub_func_uint8_t_u_u(p_117, 4))
                { /* block id: 243 */
                    int32_t l_583 = (-9L);
                    uint8_t *l_591[8] = {&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132};
                    int i;
                    for (g_23 = 0; (g_23 >= 49); g_23 = safe_add_func_uint16_t_u_u(g_23, 6))
                    { /* block id: 246 */
                        int32_t l_574[6][8] = {{(-10L),0L,0x56D6B89FL,(-1L),0x7B3A8BC6L,0xCBA19E48L,0x9ED0AB90L,0x9E808343L},{0xD8795FE5L,(-1L),0xCBA19E48L,(-1L),0xF905A4D7L,(-1L),0xCBA19E48L,(-1L)},{0x9ED0AB90L,0xF905A4D7L,0xF6000761L,(-2L),0x9E808343L,4L,0xD8795FE5L,0xB1218080L},{(-2L),0x023D9A0AL,(-1L),0xF6000761L,0x9ED0AB90L,0xD8795FE5L,0xD8795FE5L,0x9ED0AB90L},{4L,0xF6000761L,0xF6000761L,4L,(-1L),0x9E808343L,0xCBA19E48L,0x564E056DL},{(-1L),0x9E808343L,0xCBA19E48L,0x564E056DL,1L,(-10L),0x9ED0AB90L,0x023D9A0AL}};
                        int i, j;
                        l_574[3][6] &= (+l_566[4][2]);
                        if (l_560)
                            continue;
                    }
                    l_566[5][3] = 0L;
                    (*g_593) = (((((safe_mod_func_uint16_t_u_u((safe_add_func_uint16_t_u_u((0x44905046337D9467LL >= (safe_sub_func_int8_t_s_s((l_588 = (safe_lshift_func_uint8_t_u_u((l_583 != ((safe_mod_func_int16_t_s_s(g_23, (l_587 && p_117))) | 1UL)), 0))), (safe_lshift_func_uint16_t_u_s(((*l_294) = p_117), p_117))))), 0x4744L)), g_141)) , l_591[5]) != (void*)0) < l_583) < l_592[1]);
                    for (g_557 = (-30); (g_557 > 26); g_557++)
                    { /* block id: 256 */
                        l_596 = p_116;
                    }
                }
                ++l_599;
                for (l_560 = 0; (l_560 <= (-4)); --l_560)
                { /* block id: 263 */
                    uint32_t l_604[4];
                    int i;
                    for (i = 0; i < 4; i++)
                        l_604[i] = 0UL;
                    l_604[0]++;
                    return g_363;
                }
            }
            else
            { /* block id: 267 */
                uint64_t l_614[8][10];
                int32_t l_615 = 0xD17B30D2L;
                float *l_634 = &g_27;
                float **l_635 = &l_634;
                float *l_637[2];
                int i, j;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 10; j++)
                        l_614[i][j] = 0x1FB9C6074A83995DLL;
                }
                for (i = 0; i < 2; i++)
                    l_637[i] = (void*)0;
                for (g_23 = 1; (g_23 <= 4); g_23 += 1)
                { /* block id: 270 */
                    uint64_t l_616 = 0x734D41EAA17CDEE6LL;
                    for (l_286 = 0; (l_286 <= 1); l_286 += 1)
                    { /* block id: 273 */
                        int32_t *l_608[9][1] = {{&l_148},{&l_597[5][0]},{&l_148},{&l_597[5][0]},{&l_148},{&l_597[5][0]},{&l_148},{&l_597[5][0]},{&l_148}};
                        int i, j;
                        l_608[7][0] = (l_607 = p_116);
                    }
                    l_609 = p_116;
                    l_616 |= (((safe_sub_func_int64_t_s_s((*l_609), (p_117 , (safe_mul_func_int8_t_s_s(p_117, ((l_615 = ((p_116 == p_116) , ((l_518[2][0][1] == &g_404) , (l_614[2][4] >= (*p_118))))) ^ p_117)))))) , g_132) && p_117);
                    return p_117;
                }
                l_615 = (safe_div_func_float_f_f(((safe_add_func_float_f_f((safe_div_func_float_f_f((((safe_add_func_float_f_f(((*g_272) = (safe_sub_func_float_f_f(((-0x5.4p-1) != p_117), ((((safe_div_func_uint16_t_u_u(g_141, 0xAE7AL)) & (safe_lshift_func_uint8_t_u_s(((*l_609) > (l_636 &= ((p_117 > (~(l_614[0][9] >= (safe_sub_func_int8_t_s_s(((((*l_635) = l_634) != (void*)0) & (-1L)), l_614[2][7]))))) != (*g_131)))), g_31))) , 0x0.7p-1) <= p_117)))), p_117)) >= p_117) < p_117), g_160)), 0xC.E7ADC2p-15)) <= g_252), p_117));
            }
            for (g_23 = (-20); (g_23 >= 59); g_23++)
            { /* block id: 289 */
                uint64_t l_644[10][5] = {{0UL,18446744073709551615UL,0UL,0xDFB4784EE852844DLL,0x8A5423E521EDE268LL},{1UL,0xCC77AD716EAF8518LL,0xCC77AD716EAF8518LL,1UL,0x471877B1CB72B3EDLL},{1UL,18446744073709551615UL,18446744073709551606UL,18446744073709551615UL,1UL},{0x471877B1CB72B3EDLL,1UL,0xCC77AD716EAF8518LL,0xCC77AD716EAF8518LL,1UL},{0x8A5423E521EDE268LL,0xDFB4784EE852844DLL,0UL,18446744073709551615UL,0UL},{1UL,6UL,0UL,1UL,1UL},{18446744073709551612UL,18446744073709551615UL,18446744073709551612UL,0xDFB4784EE852844DLL,1UL},{1UL,0x471877B1CB72B3EDLL,0xCC77AD716EAF8518LL,6UL,0x471877B1CB72B3EDLL},{0x8A5423E521EDE268LL,18446744073709551615UL,0xDD3B9B61214F122ALL,18446744073709551615UL,0x8A5423E521EDE268LL},{0x471877B1CB72B3EDLL,6UL,0xCC77AD716EAF8518LL,0x471877B1CB72B3EDLL,1UL}};
                const int8_t **l_646 = &l_645[7][3][2];
                int8_t **l_647[9] = {&l_550,&l_551[2][8][0],&l_550,&l_551[2][8][0],&l_550,&l_551[2][8][0],&l_550,&l_551[2][8][0],&l_550};
                int32_t l_654[7][3][3] = {{{1L,(-1L),0xBEF645EDL},{0x3A87905BL,1L,0x4E3C683AL},{1L,(-1L),(-1L)}},{{0x527B2BDFL,0L,0x527B2BDFL},{(-1L),(-1L),0x527B2BDFL},{0xABDAD0C7L,(-1L),(-1L)}},{{(-1L),0x527B2BDFL,0x4E3C683AL},{1L,1L,(-1L)},{0x527B2BDFL,0xABDAD0C7L,(-1L)}},{{0x3A87905BL,0x3A87905BL,1L},{(-1L),0x3A87905BL,0x4E3C683AL},{0L,0xABDAD0C7L,0x3A87905BL}},{{8L,1L,(-1L)},{(-1L),0L,0x3A87905BL},{0x9E1F2C23L,0x4E3C683AL,0x4E3C683AL}},{{0xBEF645EDL,1L,1L},{0xBEF645EDL,1L,(-1L)},{0x9E1F2C23L,(-1L),(-1L)}},{{(-1L),8L,6L},{8L,(-1L),0x527B2BDFL},{0L,1L,0L}}};
                int i, j, k;
                g_655 = (l_654[0][1][0] = ((*l_609) = (g_552 && (safe_div_func_uint16_t_u_u((((safe_mul_func_uint8_t_u_u((l_644[0][0] <= ((0x46480058L | (((((((*l_646) = l_645[5][0][3]) == (l_648[1][2][0] = g_131)) || g_76) > (((safe_lshift_func_int8_t_s_u((0UL && (((g_141 = g_248[0][2]) | ((((safe_mul_func_int8_t_s_s((safe_unary_minus_func_uint16_t_u(p_117)), p_117)) | p_117) , 0x6699L) , g_43[0][3][2])) != 0UL)), (*p_118))) >= l_644[0][0]) == 7L)) , (***g_264)) , 0UL)) & p_117)), 0UL)) && 1UL) != (*p_118)), 0x812BL)))));
            }
            if (((p_117 = (safe_mod_func_int8_t_s_s(((safe_div_func_int32_t_s_s(p_117, (safe_lshift_func_int8_t_s_u((p_117 == (safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((((((p_117 | (((((safe_sub_func_float_f_f((((0x0.Dp-1 > 0x4.Ap+1) > (+((((((safe_lshift_func_int16_t_s_u(p_117, (((*l_609) = ((safe_mul_func_int16_t_s_s(((*l_563) = (~(((*l_677) = (safe_mul_func_int16_t_s_s(g_76, 65535UL))) , (p_117 , p_117)))), 0UL)) | p_117)) > 0x1AL))) , 0x5EED898C85BED784LL) || 0xBE2B76E0FAA7E179LL) , p_117) <= 0x5.54500Dp-28) < p_117))) <= 0x4.CD7B97p+50), p_117)) <= p_117) , l_678) ^ (*g_277)) < (*g_348))) ^ 0xC8A53785L) , 1L) > g_43[0][3][2]) || l_679), p_117)), p_117))), 6)))) != l_636), 0xEBL))) != l_678))
            { /* block id: 301 */
                (*l_609) &= ((void*)0 != &g_265);
                for (g_76 = 18; (g_76 != 19); ++g_76)
                { /* block id: 305 */
                    for (l_443 = 0; (l_443 > 22); ++l_443)
                    { /* block id: 308 */
                        return g_4;
                    }
                    if ((*l_609))
                        continue;
                    (*l_609) = (*g_94);
                }
                return g_137;
            }
            else
            { /* block id: 315 */
                int32_t *l_688 = (void*)0;
                if (((*l_609) = (~l_685)))
                { /* block id: 317 */
                    int8_t *l_692[2][4][1];
                    int64_t *l_702 = &g_43[4][1][1];
                    int64_t **l_701 = &l_702;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 4; j++)
                        {
                            for (k = 0; k < 1; k++)
                                l_692[i][j][k] = &l_315[1];
                        }
                    }
                    for (l_560 = 16; (l_560 != 7); l_560 = safe_sub_func_uint16_t_u_u(l_560, 9))
                    { /* block id: 320 */
                        const int32_t *l_690 = (void*)0;
                        const int32_t **l_689[8][4][5] = {{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_690,(void*)0,&l_690,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
                        int i, j, k;
                        l_688 = p_116;
                        l_691 = &l_587;
                    }
                    (*g_404) = (((*g_348) > (g_131 == (l_693 = l_692[0][3][0]))) || (safe_rshift_func_uint16_t_u_u((safe_mod_func_int32_t_s_s(((((safe_sub_func_uint8_t_u_u((~(((*l_553) = ((((*l_693) |= p_117) < (((*l_701) = &g_43[3][1][0]) == &l_592[5])) && g_363)) ^ (safe_add_func_int8_t_s_s(p_117, (((safe_lshift_func_int8_t_s_u(((*p_118) >= (*g_131)), (*p_118))) != (*g_131)) | p_117))))), p_117)) & p_117) < (*g_348)) > p_117), (*p_116))), 6)));
                }
                else
                { /* block id: 329 */
                    (*l_609) &= ((-1L) != 0x6FDB6640L);
                    for (g_226 = 0; (g_226 > 44); g_226 = safe_add_func_uint16_t_u_u(g_226, 3))
                    { /* block id: 333 */
                        uint32_t ***l_710 = &g_347;
                        g_711 = l_710;
                        l_713 = g_712;
                    }
                    if ((*g_404))
                    { /* block id: 337 */
                        (*l_609) |= (safe_mod_func_uint16_t_u_u(65530UL, 1UL));
                    }
                    else
                    { /* block id: 339 */
                        uint16_t l_725 = 0x0C4BL;
                        int32_t *l_740 = &l_597[3][1];
                        (*g_719) = (p_117 >= (safe_div_func_float_f_f((+p_117), 0x0.C28E18p+34)));
                        (*l_713) = (safe_lshift_func_uint16_t_u_u(((*l_294) = (~((*l_553) = (((((*p_118) && ((*l_693) = (safe_rshift_func_int16_t_s_u(l_725, (0x2BL | (safe_rshift_func_uint8_t_u_u((+(safe_mod_func_int8_t_s_s(((-1L) | (g_137 > (l_731 || (safe_div_func_uint8_t_u_u((safe_add_func_int8_t_s_s(((((((((*p_118) || (((safe_lshift_func_uint8_t_u_u((*p_118), ((*g_131) = ((g_131 == g_131) , g_738)))) ^ 0UL) > (*l_713))) > 0xA1F933B224213C23LL) , 0x6AF79AC2500AAFEDLL) || 0xF907B71581BD90BELL) , l_739[6][0][2]) == (void*)0) < (-5L)), 1L)), (*p_118)))))), 0xB6L))), 3))))))) , 0x0AL) != 0xB0L) && 0xDFL)))), g_76));
                        l_740 = (void*)0;
                    }
                }
                (*l_609) = 0L;
            }
        }
        else
        { /* block id: 351 */
            int32_t l_745 = 1L;
            int16_t *l_757[3];
            int32_t l_758[2][6] = {{0xA3FD29B3L,0x28121B85L,0xA3FD29B3L,0xA3FD29B3L,0x28121B85L,0xA3FD29B3L},{0xA3FD29B3L,0x28121B85L,0xA3FD29B3L,0xA3FD29B3L,0x28121B85L,0xA3FD29B3L}};
            int32_t l_759 = 1L;
            int i, j;
            for (i = 0; i < 3; i++)
                l_757[i] = &g_141;
            (*g_94) |= (0xC8A8B5ECL || ((safe_div_func_uint8_t_u_u(1UL, (safe_mod_func_int16_t_s_s(l_745, (g_141 = (l_759 = (safe_add_func_int16_t_s_s((safe_mul_func_int8_t_s_s((safe_unary_minus_func_uint16_t_u((*l_691))), (safe_div_func_int16_t_s_s((-5L), (++(*l_294)))))), (l_758[0][5] ^= (safe_sub_func_uint32_t_u_u(0xE30F2E67L, p_117))))))))))) <= 253UL));
        }
        l_566[2][3] = ((safe_div_func_float_f_f(p_117, (((*l_780) = (safe_sub_func_float_f_f((safe_div_func_float_f_f((p_117 < ((safe_add_func_float_f_f((safe_div_func_float_f_f(((*l_770) = (*g_438)), (((p_117 <= g_655) > ((*l_776) = ((((**l_549) = p_118) == l_645[5][4][1]) == (((safe_mul_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(p_117, p_117)), g_141)) < p_117) , g_255)))) <= 0xC.005621p-19))), l_777[3])) != p_117)), g_137)), (-0x1.9p+1)))) >= 0xD.0CABD9p-82))) != p_117);
    }
    else
    { /* block id: 363 */
        int32_t *l_784 = (void*)0;
        uint32_t * const *l_825 = (void*)0;
        uint32_t * const **l_826 = &l_825;
        int32_t l_828 = (-2L);
        int32_t l_829[10];
        uint16_t l_830 = 9UL;
        int64_t *l_840 = &g_556;
        int i;
        for (i = 0; i < 10; i++)
            l_829[i] = 0x3E9E6155L;
        l_784 = l_784;
        for (g_23 = 0; (g_23 > 35); g_23 = safe_add_func_uint32_t_u_u(g_23, 1))
        { /* block id: 367 */
            int32_t *l_787 = (void*)0;
            int32_t l_788[5];
            uint32_t ***l_791 = &g_347;
            int64_t *l_798 = &g_43[0][3][2];
            int64_t **l_797 = &l_798;
            uint32_t *l_804[10] = {&l_458,&g_655,&l_458,&g_655,&l_458,&g_655,&l_458,&g_655,&l_458,&g_655};
            uint32_t **l_803 = &l_804[0];
            uint32_t *l_805[4][1] = {{&l_536[1][2]},{&l_458},{&l_536[1][2]},{&l_458}};
            uint64_t **l_806 = (void*)0;
            uint64_t **l_807 = &l_553;
            float *l_808 = &g_75[1][6];
            int i, j;
            for (i = 0; i < 5; i++)
                l_788[i] = 0x4EA821C8L;
            l_784 = l_787;
            if (l_788[0])
                continue;
            (*g_809) = (safe_sub_func_float_f_f((l_791 == l_791), ((*l_808) = (+((p_117 != ((safe_mod_func_int32_t_s_s((((safe_lshift_func_uint8_t_u_s(((((*l_797) = &g_76) == l_553) && (safe_add_func_int32_t_s_s((((((((*l_803) = &g_655) != (l_805[3][0] = p_116)) | (((*l_807) = &g_252) != (void*)0)) < 0xDCF887DDE452376DLL) ^ 18446744073709551615UL) < p_117), 0UL))), g_248[1][4])) > 0UL) == p_117), p_117)) <= 1L)) , (*g_719))))));
            for (l_458 = 0; (l_458 >= 32); l_458 = safe_add_func_uint32_t_u_u(l_458, 9))
            { /* block id: 378 */
                int64_t l_812[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_812[i] = 0xAAEB7D7E977F98DFLL;
                if (l_812[1])
                { /* block id: 379 */
                    return l_788[0];
                }
                else
                { /* block id: 381 */
                    int32_t *l_813 = &l_679;
                    float *l_814[7] = {&g_27,&g_27,&g_27,&g_27,&g_27,&g_27,&g_27};
                    int i;
                    for (l_443 = 0; (l_443 <= 3); l_443 += 1)
                    { /* block id: 384 */
                        int i;
                        if (l_315[l_443])
                            break;
                    }
                    for (l_148 = 0; (l_148 <= 4); l_148 += 1)
                    { /* block id: 389 */
                        float **l_815 = (void*)0;
                        float **l_816 = &l_814[5];
                        int i;
                        l_813 = &l_788[l_148];
                        l_788[l_148] &= (((*l_816) = l_814[5]) == (((*g_712) >= 4294967290UL) , (void*)0));
                    }
                    for (g_255 = 0; (g_255 >= 0); g_255 -= 1)
                    { /* block id: 396 */
                        int i, j, k;
                        if (p_117)
                            break;
                        (*l_813) = ((*l_808) = (*g_272));
                        l_447[(g_255 + 4)][g_255][(g_255 + 1)] &= p_117;
                    }
                }
            }
        }
        if ((l_828 = ((safe_mul_func_uint16_t_u_u((((0x3BL ^ (((safe_add_func_int32_t_s_s((0x43F26E2F37CADBFALL || g_252), ((((**g_347) = (*p_116)) == ((safe_sub_func_int64_t_s_s((-4L), (safe_rshift_func_int16_t_s_u(p_117, ((((*l_826) = l_825) == (void*)0) , l_827))))) , p_117)) ^ p_117))) , g_43[2][2][2]) , (*p_118))) ^ 1L) , p_117), l_828)) <= g_248[0][0])))
        { /* block id: 408 */
            l_830--;
        }
        else
        { /* block id: 410 */
            uint16_t *l_833 = (void*)0;
            uint16_t *l_834 = &l_554[2][0];
            int64_t *l_839 = &g_76;
            int32_t l_841[4][2];
            int i, j;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 2; j++)
                    l_841[i][j] = (-5L);
            }
            l_841[0][1] |= (((((*l_834) ^= ((*l_294) = 65526UL)) || p_117) ^ ((*g_131) = ((*p_116) <= (g_835 = (*p_116))))) , (!((safe_mod_func_uint8_t_u_u((*g_131), (***g_264))) , (l_839 == l_840))));
        }
    }
    return p_117;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    transparent_crc_bytes (&g_27, sizeof(g_27), "g_27", print_hash_value);
    transparent_crc_bytes (&g_28, sizeof(g_28), "g_28", print_hash_value);
    transparent_crc(g_31, "g_31", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_43[i][j][k], "g_43[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc_bytes(&g_75[i][j], sizeof(g_75[i][j]), "g_75[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_132, "g_132", print_hash_value);
    transparent_crc(g_137, "g_137", print_hash_value);
    transparent_crc(g_141, "g_141", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    transparent_crc(g_226, "g_226", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_248[i][j], "g_248[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_251, sizeof(g_251), "g_251", print_hash_value);
    transparent_crc(g_252, "g_252", print_hash_value);
    transparent_crc(g_255, "g_255", print_hash_value);
    transparent_crc(g_327, "g_327", print_hash_value);
    transparent_crc(g_363, "g_363", print_hash_value);
    transparent_crc(g_552, "g_552", print_hash_value);
    transparent_crc(g_556, "g_556", print_hash_value);
    transparent_crc(g_557, "g_557", print_hash_value);
    transparent_crc_bytes (&g_586, sizeof(g_586), "g_586", print_hash_value);
    transparent_crc(g_655, "g_655", print_hash_value);
    transparent_crc(g_738, "g_738", print_hash_value);
    transparent_crc(g_835, "g_835", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_849[i][j], "g_849[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_857, "g_857", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_940[i], "g_940[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_974, "g_974", print_hash_value);
    transparent_crc(g_1027, "g_1027", print_hash_value);
    transparent_crc(g_1085, "g_1085", print_hash_value);
    transparent_crc(g_1363, "g_1363", print_hash_value);
    transparent_crc(g_1417, "g_1417", print_hash_value);
    transparent_crc(g_1447, "g_1447", print_hash_value);
    transparent_crc(g_1569, "g_1569", print_hash_value);
    transparent_crc(g_1597, "g_1597", print_hash_value);
    transparent_crc(g_1627, "g_1627", print_hash_value);
    transparent_crc(g_1696, "g_1696", print_hash_value);
    transparent_crc(g_1770, "g_1770", print_hash_value);
    transparent_crc(g_1789, "g_1789", print_hash_value);
    transparent_crc(g_1793, "g_1793", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc_bytes(&g_1860[i][j][k], sizeof(g_1860[i][j][k]), "g_1860[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2121, "g_2121", print_hash_value);
    transparent_crc(g_2187, "g_2187", print_hash_value);
    transparent_crc(g_2237, "g_2237", print_hash_value);
    transparent_crc(g_2626, "g_2626", print_hash_value);
    transparent_crc(g_2659, "g_2659", print_hash_value);
    transparent_crc(g_2703, "g_2703", print_hash_value);
    transparent_crc(g_2744, "g_2744", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2767[i], "g_2767[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2768, "g_2768", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_2844[i][j], "g_2844[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2920, "g_2920", print_hash_value);
    transparent_crc(g_3066, "g_3066", print_hash_value);
    transparent_crc(g_3104, "g_3104", print_hash_value);
    transparent_crc(g_3293, "g_3293", print_hash_value);
    transparent_crc(g_3365, "g_3365", print_hash_value);
    transparent_crc(g_3556, "g_3556", print_hash_value);
    transparent_crc_bytes (&g_3560, sizeof(g_3560), "g_3560", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_3690[i], "g_3690[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_3695[i], "g_3695[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3785, "g_3785", print_hash_value);
    transparent_crc(g_3786, "g_3786", print_hash_value);
    transparent_crc(g_3897, "g_3897", print_hash_value);
    transparent_crc(g_3935, "g_3935", print_hash_value);
    transparent_crc(g_3944, "g_3944", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_3993[i][j], "g_3993[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1032
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 498
   depth: 2, occurrence: 125
   depth: 3, occurrence: 19
   depth: 4, occurrence: 2
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 2
   depth: 11, occurrence: 3
   depth: 12, occurrence: 5
   depth: 13, occurrence: 3
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 7
   depth: 18, occurrence: 1
   depth: 19, occurrence: 5
   depth: 20, occurrence: 6
   depth: 21, occurrence: 5
   depth: 22, occurrence: 5
   depth: 23, occurrence: 3
   depth: 24, occurrence: 4
   depth: 25, occurrence: 4
   depth: 26, occurrence: 9
   depth: 27, occurrence: 4
   depth: 28, occurrence: 5
   depth: 29, occurrence: 1
   depth: 30, occurrence: 7
   depth: 31, occurrence: 5
   depth: 32, occurrence: 5
   depth: 33, occurrence: 1
   depth: 34, occurrence: 4
   depth: 36, occurrence: 1
   depth: 38, occurrence: 4
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 42, occurrence: 2
   depth: 44, occurrence: 1
   depth: 45, occurrence: 2

XXX total number of pointers: 773

XXX times a variable address is taken: 1852
XXX times a pointer is dereferenced on RHS: 650
breakdown:
   depth: 1, occurrence: 459
   depth: 2, occurrence: 112
   depth: 3, occurrence: 54
   depth: 4, occurrence: 20
   depth: 5, occurrence: 5
XXX times a pointer is dereferenced on LHS: 557
breakdown:
   depth: 1, occurrence: 443
   depth: 2, occurrence: 70
   depth: 3, occurrence: 35
   depth: 4, occurrence: 4
   depth: 5, occurrence: 5
XXX times a pointer is compared with null: 66
XXX times a pointer is compared with address of another variable: 28
XXX times a pointer is compared with another pointer: 35
XXX times a pointer is qualified to be dereferenced: 17198

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2081
   level: 2, occurrence: 561
   level: 3, occurrence: 295
   level: 4, occurrence: 126
   level: 5, occurrence: 68
XXX number of pointers point to pointers: 372
XXX number of pointers point to scalars: 401
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.5
XXX average alias set size: 1.52

XXX times a non-volatile is read: 3429
XXX times a non-volatile is write: 1652
XXX times a volatile is read: 175
XXX    times read thru a pointer: 48
XXX times a volatile is write: 124
XXX    times written thru a pointer: 49
XXX times a volatile is available for access: 4.89e+03
XXX percentage of non-volatile access: 94.4

XXX forward jumps: 4
XXX backward jumps: 12

XXX stmts: 499
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 40
   depth: 1, occurrence: 37
   depth: 2, occurrence: 70
   depth: 3, occurrence: 98
   depth: 4, occurrence: 121
   depth: 5, occurrence: 133

XXX percentage a fresh-made variable is used: 15.3
XXX percentage an existing variable is used: 84.7
********************* end of statistics **********************/


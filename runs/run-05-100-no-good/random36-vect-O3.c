/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3398150151
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile float g_9 = 0x6.2751C7p+52;/* VOLATILE GLOBAL g_9 */
static volatile int32_t g_17[1] = {0x880E033CL};
static int16_t g_19 = 3L;
static int16_t g_31 = (-2L);
static int16_t *g_30[6] = {&g_31,&g_31,&g_31,&g_31,&g_31,&g_31};
static int32_t *g_60[3] = {(void*)0,(void*)0,(void*)0};
static int32_t g_64[2][8] = {{0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL},{0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL,0xFE1DB4DBL}};
static uint32_t g_73 = 7UL;
static int32_t g_85 = 0x42EC213DL;
static int32_t g_89 = (-10L);
static int32_t g_92 = 0x227CDBA0L;
static int32_t g_98 = 0x45F054F5L;
static float g_133 = 0xE.866A73p-6;
static uint64_t g_136 = 18446744073709551615UL;
static int16_t g_137 = (-10L);
static uint8_t g_145[5] = {0UL,0UL,0UL,0UL,0UL};
static uint8_t g_147 = 0xCCL;
static uint16_t g_159 = 65535UL;
static int32_t *g_165 = &g_98;
static int16_t **g_185 = &g_30[0];
static int16_t ***g_184[10] = {&g_185,&g_185,(void*)0,&g_185,&g_185,(void*)0,&g_185,&g_185,(void*)0,&g_185};
static uint32_t g_204[7] = {0xF7597347L,0xF7597347L,0xF7597347L,0xF7597347L,0xF7597347L,0xF7597347L,0xF7597347L};
static uint16_t *g_296 = &g_159;
static uint8_t g_319[10] = {0x49L,0UL,0x49L,0x49L,0UL,0x49L,0x49L,0UL,0x49L,0x49L};
static int16_t g_338 = 1L;
static int8_t g_350[9][5][5] = {{{6L,(-1L),1L,9L,0xFBL},{0x22L,6L,0xAFL,0x4AL,0x55L},{9L,0L,5L,1L,0x98L},{0xFBL,(-1L),0xE9L,1L,9L},{(-6L),(-1L),0xBCL,0xD8L,0xBCL}},{{0xEEL,0xEEL,0xD2L,1L,6L},{1L,0xBCL,0x14L,0x16L,0x72L},{0xF2L,0x37L,1L,0x18L,0xBFL},{1L,0xBCL,0xDDL,0x37L,0L},{0xAEL,0xEEL,6L,0x0DL,8L}},{{0L,(-1L),0xF2L,(-4L),0xA0L},{0xF5L,(-1L),3L,6L,0xE2L},{9L,0L,0xFFL,0x8FL,0xFAL},{0L,6L,6L,0x2AL,0x37L},{(-6L),(-1L),(-2L),0xDDL,(-4L)}},{{0x24L,(-1L),9L,0L,1L},{0L,0xF2L,0x2AL,0xEFL,0x8CL},{0x18L,(-2L),0x2AL,4L,0x01L},{(-2L),0x16L,9L,0xE3L,0L},{0L,0xA0L,(-2L),(-1L),0x22L}},{{0x8FL,0x64L,6L,(-2L),0L},{0xBFL,8L,0xFFL,0xC2L,(-10L)},{0xD8L,0xE3L,3L,0xAEL,(-1L)},{(-1L),0x0DL,0xF2L,0x72L,0xF2L},{0x8CL,6L,6L,0x8CL,0xDDL}},{{0L,0xF5L,0xDDL,9L,0xC2L},{0xF2L,1L,1L,(-1L),3L},{(-1L),1L,0x14L,9L,6L},{0x55L,(-4L),0xD2L,0x8CL,0L},{0xEFL,0L,0xBCL,0x72L,(-1L)}},{{0x98L,(-6L),0xE9L,0xAEL,(-1L)},{(-2L),0L,5L,0xC2L,0L},{0x51L,0x2BL,0xAFL,(-2L),0L},{(-6L),0xE2L,1L,(-1L),(-10L)},{(-1L),0x44L,(-7L),0xE3L,9L}},{{3L,0x87L,0x81L,4L,0xE2L},{0x81L,(-7L),(-1L),0xAFL,0xE2L},{0x51L,(-2L),0xC2L,(-1L),0L},{(-7L),(-1L),0x98L,1L,(-2L)},{0x2AL,0x64L,9L,0x22L,(-1L)}},{{(-6L),3L,0xE9L,0L,(-1L)},{0xBFL,0x4AL,0xFBL,(-1L),0xE9L},{7L,0xC4L,(-1L),0xA0L,0x2AL},{0L,(-4L),0xAEL,0xAEL,(-4L)},{0L,(-4L),0x18L,0x2BL,3L}}};
static int8_t *g_414 = (void*)0;
static int8_t **g_413 = &g_414;
static int16_t *****g_520 = (void*)0;
static int16_t ****g_522 = &g_184[3];
static int16_t *****g_521 = &g_522;
static volatile uint8_t **g_523[3] = {(void*)0,(void*)0,(void*)0};
static int32_t g_550 = 1L;
static int64_t g_595 = (-1L);
static int16_t *** const *g_598 = &g_184[3];
static float g_657[10] = {0x7.4CFCB4p-8,0xC.39EC42p+35,(-0x4.Cp-1),0xC.39EC42p+35,0x7.4CFCB4p-8,0x7.4CFCB4p-8,0xC.39EC42p+35,(-0x4.Cp-1),0xC.39EC42p+35,0x7.4CFCB4p-8};
static uint16_t **g_763 = &g_296;
static uint16_t ***g_762 = &g_763;
static int8_t g_769 = 0xE8L;
static int64_t g_795 = (-10L);
static int32_t **g_814 = &g_165;
static volatile uint8_t g_884[7] = {0xE1L,0x28L,0x28L,0xE1L,0x28L,0x28L,0xE1L};
static volatile uint8_t *g_883 = &g_884[2];
static volatile uint8_t * volatile *g_882 = &g_883;
static volatile uint8_t * volatile * volatile *g_881 = &g_882;
static int32_t ** volatile *g_905 = &g_814;
static int32_t ** volatile * const *g_904 = &g_905;
static int32_t g_926 = 0L;
static uint32_t g_940 = 0UL;
static const uint8_t g_1041 = 0UL;
static uint64_t g_1058 = 18446744073709551615UL;
static uint8_t **g_1235 = (void*)0;
static uint8_t ***g_1234 = &g_1235;
static int16_t g_1324 = 0L;
static int32_t g_1326 = 0x5C61A22CL;
static uint8_t *g_1335 = (void*)0;
static int64_t g_1340 = 0x638DD356693DD13FLL;
static float g_1369[6] = {0x7.B25C42p-77,0x7.B25C42p-77,0x7.B25C42p-77,0x7.B25C42p-77,0x7.B25C42p-77,0x7.B25C42p-77};
static int16_t *g_1409[9] = {&g_338,(void*)0,&g_338,(void*)0,&g_338,(void*)0,&g_338,(void*)0,&g_338};
static int32_t g_1481 = 0xC96FA41BL;
static int64_t *g_1487 = (void*)0;
static int64_t **g_1486 = &g_1487;
static int64_t ***g_1485 = &g_1486;
static int8_t * volatile * const g_1547 = &g_414;
static int8_t * volatile * const  volatile * volatile g_1546 = &g_1547;/* VOLATILE GLOBAL g_1546 */
static int8_t * volatile * const  volatile * volatile *g_1545 = &g_1546;
static const int32_t *g_1555 = &g_64[1][3];
static int8_t g_1558 = 0x4EL;
static uint16_t g_1570 = 0x96FAL;
static uint16_t g_1584[1][8][3] = {{{2UL,65534UL,2UL},{2UL,65534UL,2UL},{2UL,65534UL,2UL},{2UL,65534UL,2UL},{2UL,65534UL,2UL},{2UL,65534UL,2UL},{2UL,65534UL,2UL},{2UL,65534UL,2UL}}};
static int8_t **g_1606 = &g_414;
static const int8_t g_1665 = 0x18L;
static volatile uint16_t g_1702 = 65529UL;/* VOLATILE GLOBAL g_1702 */
static int32_t * volatile g_1707 = &g_89;/* VOLATILE GLOBAL g_1707 */
static int16_t g_1872 = (-5L);
static int32_t g_1924 = 1L;
static volatile uint16_t * volatile g_1948 = &g_1702;/* VOLATILE GLOBAL g_1948 */
static volatile uint16_t * volatile *g_1947 = &g_1948;
static volatile uint16_t * volatile * const * volatile g_1946[1] = {&g_1947};
static volatile uint16_t * volatile * const * volatile *g_1945 = &g_1946[0];
static volatile uint16_t * volatile * const * volatile **g_1944 = &g_1945;
static const volatile uint16_t g_1971 = 0UL;/* VOLATILE GLOBAL g_1971 */
static const uint32_t g_1975 = 18446744073709551615UL;
static const uint32_t g_1979[10] = {18446744073709551612UL,0x5C3C22C8L,18446744073709551612UL,18446744073709551615UL,18446744073709551615UL,18446744073709551612UL,0x5C3C22C8L,18446744073709551612UL,18446744073709551615UL,18446744073709551615UL};
static const uint32_t *g_1978 = &g_1979[9];
static uint8_t *g_2012 = (void*)0;
static uint8_t *g_2014 = &g_319[6];
static uint8_t ** const *g_2067[6] = {&g_1235,&g_1235,&g_1235,&g_1235,&g_1235,&g_1235};
static uint8_t ** const **g_2066[10] = {&g_2067[2],&g_2067[1],&g_2067[1],&g_2067[2],&g_2067[1],&g_2067[1],&g_2067[2],&g_2067[1],&g_2067[1],&g_2067[2]};
static uint8_t **g_2074 = &g_2012;
static int16_t g_2096 = (-10L);
static uint16_t ****g_2138 = &g_762;
static uint16_t ***** volatile g_2137 = &g_2138;/* VOLATILE GLOBAL g_2137 */
static int16_t *g_2175 = &g_1872;
static uint8_t ***g_2324 = (void*)0;
static int16_t *g_2414 = &g_338;
static float * volatile g_2435 = &g_657[0];/* VOLATILE GLOBAL g_2435 */
static volatile int32_t g_2506 = 0xD0BBCC8FL;/* VOLATILE GLOBAL g_2506 */
static uint8_t ****g_2523[2] = {&g_2324,&g_2324};
static uint8_t **** const *g_2522 = &g_2523[1];
static int16_t g_2549 = (-1L);
static const int64_t g_2641 = 0xAF792179B1000BAFLL;
static float * volatile g_2670 = &g_133;/* VOLATILE GLOBAL g_2670 */
static uint32_t g_2706 = 18446744073709551615UL;
static volatile uint32_t g_2774 = 5UL;/* VOLATILE GLOBAL g_2774 */
static volatile uint32_t *g_2773[7][5][6] = {{{(void*)0,&g_2774,(void*)0,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774}},{{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,(void*)0},{&g_2774,(void*)0,&g_2774,&g_2774,&g_2774,(void*)0},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0,&g_2774,&g_2774,&g_2774}},{{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,(void*)0}},{{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,(void*)0},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774}},{{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,(void*)0,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774,&g_2774,&g_2774,(void*)0},{&g_2774,&g_2774,&g_2774,(void*)0,&g_2774,&g_2774}},{{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,(void*)0,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,(void*)0,&g_2774}},{{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,(void*)0,&g_2774},{&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774}}};
static volatile uint32_t **g_2772 = &g_2773[1][1][2];
static volatile uint32_t ***g_2771 = &g_2772;
static uint32_t g_2802 = 0xD3D38F9BL;


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static const uint64_t  func_4(float  p_5, int8_t  p_6);
static uint16_t  func_24(int16_t * p_25, int16_t * p_26, int32_t  p_27, int16_t  p_28, float  p_29);
static float  func_34(int16_t * p_35, int16_t * const  p_36);
static int16_t * func_37(int64_t  p_38, int16_t * p_39, uint64_t  p_40, int32_t  p_41, int16_t * p_42);
static int32_t * func_51(int16_t * p_52, uint16_t  p_53, uint32_t  p_54, int16_t * p_55, const uint64_t  p_56);
static int16_t * func_57(int32_t * p_58, const uint16_t  p_59);
static uint16_t  func_68(int64_t  p_69, int16_t * p_70, int16_t * p_71, uint8_t  p_72);
static int16_t * func_74(int16_t * p_75);
static int16_t * func_76(float  p_77, uint32_t  p_78, uint8_t  p_79, uint64_t  p_80, int16_t  p_81);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_10 = 0x5059B8BCL;
    int16_t *l_18 = &g_19;
    uint8_t l_32[6][5] = {{255UL,0x5DL,0xC2L,255UL,0xC2L},{255UL,255UL,0xBBL,0x2CL,0xC9L},{0x94L,0xC9L,0xC2L,0xC2L,0xC9L},{0xC9L,0x5DL,0x94L,0xC9L,0xC2L},{0x2CL,0xC9L,0xBBL,0xC9L,0x2CL},{0x94L,255UL,0x5DL,0xC2L,255UL}};
    int8_t *l_1557 = &g_1558;
    uint32_t l_1559 = 0UL;
    const int64_t ***l_1727 = (void*)0;
    const int64_t ****l_1726 = &l_1727;
    uint16_t l_1763 = 0xC251L;
    int32_t *l_1796 = &g_89;
    uint16_t l_1914[7];
    int16_t ***l_1920 = &g_185;
    int32_t l_1935 = 2L;
    int32_t l_1939 = 0xE98102CAL;
    uint16_t **l_1955 = &g_296;
    int8_t ***l_1958 = &g_1606;
    int8_t ****l_1957 = &l_1958;
    uint8_t *l_2013 = &g_145[0];
    uint8_t l_2048[8][10] = {{0UL,0UL,0x1DL,0x63L,0xBBL,0x63L,0x1DL,0xBBL,0xBBL,0UL},{255UL,0xCAL,0x1DL,0x1DL,0xCAL,255UL,0UL,255UL,0xCAL,0x1DL},{0x63L,0xBBL,0x63L,0x1DL,0UL,0UL,0x1DL,0x63L,0xBBL,0x63L},{0x63L,255UL,0xBBL,0xCAL,0xBBL,255UL,0x63L,0x63L,255UL,0xBBL},{255UL,0x63L,0x63L,255UL,0xBBL,0xCAL,0xBBL,255UL,0x63L,0x63L},{0xBBL,0x63L,0x1DL,0UL,0UL,0x1DL,0x63L,0xBBL,0x63L,0x1DL},{0xCAL,255UL,0UL,255UL,0xCAL,0x1DL,0x1DL,0xCAL,255UL,0UL},{0xBBL,0xBBL,0UL,0xCAL,0x2EL,0xCAL,0UL,0xBBL,0xBBL,0UL}};
    int64_t l_2069 = (-1L);
    int32_t *l_2098 = (void*)0;
    int16_t *****l_2114 = &g_522;
    int32_t l_2156 = 0xCCDB60AFL;
    int16_t l_2173[3][1];
    int16_t l_2235 = 0xBA05L;
    int32_t l_2284 = 0xE4276BECL;
    int32_t l_2285 = 0x23E64F6CL;
    int32_t l_2286 = 1L;
    int32_t l_2287 = 0x22130CF7L;
    int32_t l_2288 = 0x2AB605F8L;
    int32_t l_2289 = 0xF7483A1CL;
    int32_t l_2290 = 0x8DCFCF3DL;
    uint8_t * const *l_2326 = &g_1335;
    uint8_t * const **l_2325 = &l_2326;
    float l_2330 = 0x2.1B321Ap+43;
    float l_2375 = 0xD.4D7EC5p+49;
    int8_t l_2467 = 0x8FL;
    uint8_t l_2559 = 0x40L;
    uint16_t l_2640[3];
    int64_t **l_2664 = &g_1487;
    const int32_t l_2668[9][9] = {{0xB278982BL,0x10ABA4BFL,0x84503A70L,(-6L),0xFF7D9343L,(-6L),0x84503A70L,0x10ABA4BFL,0xB278982BL},{0x9DFF4590L,(-6L),0xF4A51DBEL,2L,0xFF7D9343L,0x227D8CF6L,(-1L),0x84370A8DL,1L},{(-1L),0x84370A8DL,2L,(-5L),0xF4A51DBEL,0xF4A51DBEL,(-5L),2L,0x84370A8DL},{0x9DFF4590L,0xFF7D9343L,(-1L),1L,1L,0xF4A51DBEL,0xB278982BL,0x227D8CF6L,(-1L)},{0xB278982BL,(-5L),0xFF7D9343L,1L,2L,0x227D8CF6L,2L,1L,0xFF7D9343L},{0xFF7D9343L,0xFF7D9343L,(-1L),(-1L),(-3L),(-6L),2L,0x84503A70L,0x9DFF4590L},{1L,0x84370A8DL,0x10ABA4BFL,(-1L),0x9DFF4590L,0xB278982BL,0xB278982BL,0x9DFF4590L,(-1L)},{(-1L),(-6L),(-1L),0xF4A51DBEL,0x227D8CF6L,0x84370A8DL,(-5L),0x9DFF4590L,(-3L)},{(-6L),0x10ABA4BFL,0xFF7D9343L,(-3L),(-5L),0x84503A70L,(-1L),0x84503A70L,(-5L)}};
    int32_t l_2677 = 0xEC77404BL;
    uint32_t l_2762 = 6UL;
    int32_t l_2788 = 0x9CF0709EL;
    int8_t l_2801 = 0x3DL;
    int32_t l_2809 = (-6L);
    int i, j;
    for (i = 0; i < 7; i++)
        l_1914[i] = 1UL;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_2173[i][j] = (-1L);
    }
    for (i = 0; i < 3; i++)
        l_2640[i] = 65530UL;
    return l_2809;
}


/* ------------------------------------------ */
/* 
 * reads : g_762 g_763 g_296 g_159 g_31 g_1570 g_137 g_926 g_1558 g_89 g_1584 g_133 g_595 g_940 g_1326 g_884 g_92 g_883 g_319 g_145 g_204 g_769 g_413 g_414 g_1606 g_64 g_1058 g_1702 g_147 g_1485 g_1707 g_350 g_882
 * writes: g_159 g_31 g_1570 g_1584 g_926 g_1558 g_89 g_1606 g_657 g_137 g_1369 g_296 g_940 g_1058 g_1702 g_147 g_1486
 */
static const uint64_t  func_4(float  p_5, int8_t  p_6)
{ /* block id: 694 */
    const uint32_t l_1560 = 0x8822C72EL;
    uint16_t *l_1569 = &g_1570;
    const uint8_t *****l_1579 = (void*)0;
    int32_t l_1580 = 8L;
    int64_t *l_1581[10][3][1] = {{{&g_595},{(void*)0},{(void*)0}},{{&g_595},{(void*)0},{(void*)0}},{{&g_595},{&g_595},{&g_795}},{{&g_795},{&g_595},{&g_595}},{{(void*)0},{(void*)0},{&g_595}},{{(void*)0},{(void*)0},{&g_595}},{{&g_595},{&g_795},{&g_795}},{{&g_595},{&g_595},{(void*)0}},{{(void*)0},{&g_595},{(void*)0}},{{(void*)0},{&g_595},{&g_595}}};
    int32_t l_1582 = (-1L);
    uint16_t *l_1583 = &g_1584[0][5][0];
    const int64_t *l_1617 = &g_795;
    const int64_t **l_1616 = &l_1617;
    const int64_t ***l_1615[6][1][8] = {{{&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616}},{{&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616}},{{&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616}},{{&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616}},{{&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616}},{{&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616,&l_1616}}};
    int32_t l_1697[1];
    int8_t l_1698[7][9] = {{7L,7L,1L,7L,7L,1L,7L,7L,1L},{7L,7L,1L,7L,7L,1L,7L,7L,1L},{7L,7L,1L,7L,7L,1L,7L,7L,1L},{7L,7L,1L,7L,7L,1L,7L,7L,1L},{7L,7L,1L,7L,7L,1L,7L,7L,1L},{7L,7L,1L,7L,7L,1L,7L,7L,1L},{7L,7L,1L,7L,7L,1L,7L,7L,1L}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1697[i] = 0xB99C906BL;
    if (((p_5 , l_1560) ^ (safe_add_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(l_1560, (((*l_1569) &= ((***g_762)++)) & l_1560))) & (&g_136 != ((+(safe_div_func_int64_t_s_s((((safe_rshift_func_uint16_t_u_u(l_1560, 0)) == ((safe_div_func_int16_t_s_s((((*l_1583) = (0x086CF731EBE2D08FLL == (l_1582 ^= (l_1580 = ((l_1560 || ((((safe_unary_minus_func_uint16_t_u(((l_1579 = l_1579) == (void*)0))) <= (-1L)) < 4294967295UL) & g_137)) , l_1560))))) , 0xA262L), 1L)) , (-10L))) , 0L), l_1560))) , &g_136))), 0)), l_1560))))
    { /* block id: 701 */
        float *l_1586 = &g_1369[2];
        float **l_1585[9][8] = {{&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586},{&l_1586,&l_1586,&l_1586,(void*)0,&l_1586,&l_1586,&l_1586,&l_1586},{&l_1586,&l_1586,&l_1586,&l_1586,(void*)0,&l_1586,(void*)0,&l_1586},{(void*)0,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,(void*)0},{&l_1586,&l_1586,&l_1586,(void*)0,&l_1586,(void*)0,&l_1586,(void*)0},{&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586,&l_1586},{&l_1586,&l_1586,(void*)0,(void*)0,(void*)0,&l_1586,&l_1586,&l_1586},{&l_1586,&l_1586,&l_1586,(void*)0,&l_1586,&l_1586,&l_1586,&l_1586},{&l_1586,&l_1586,&l_1586,(void*)0,&l_1586,&l_1586,&l_1586,&l_1586}};
        int32_t l_1591 = 0x7173DBDAL;
        int8_t **l_1604[1];
        const int16_t *l_1656[4][8] = {{&g_1324,&g_1324,&g_1324,&g_1324,&g_1324,&g_1324,&g_1324,&g_1324},{&g_19,&g_1324,&g_19,&g_19,&g_1324,&g_19,&g_19,&g_19},{&g_19,&g_1324,&g_1324,&g_19,&g_1324,&g_1324,&g_19,&g_1324},{&g_19,&g_19,&g_1324,&g_19,&g_19,&g_1324,&g_19,&g_19}};
        const int16_t **l_1655[7] = {&l_1656[3][1],&l_1656[3][1],&l_1656[3][1],&l_1656[3][1],&l_1656[3][1],&l_1656[3][1],&l_1656[3][1]};
        const int16_t ***l_1654[2][10] = {{&l_1655[5],&l_1655[5],&l_1655[5],(void*)0,&l_1655[5],&l_1655[5],&l_1655[5],&l_1655[5],(void*)0,&l_1655[5]},{&l_1655[5],&l_1655[5],&l_1655[5],(void*)0,&l_1655[5],&l_1655[5],&l_1655[5],&l_1655[5],(void*)0,&l_1655[5]}};
        const int16_t ****l_1653 = &l_1654[1][4];
        const int16_t *****l_1652 = &l_1653;
        int16_t l_1688[7][9] = {{2L,1L,2L,0L,0x6E66L,0x07EDL,0x553FL,0xD805L,0xD805L},{0x746AL,1L,0x8BFBL,(-1L),0x8BFBL,1L,0x746AL,0x7DE2L,0x8DAEL},{0x553FL,0x07EDL,0x6E66L,0L,2L,1L,2L,0L,0x6E66L},{(-1L),(-1L),0x01ABL,(-9L),0x8DAEL,(-1L),1L,0x7DE2L,1L},{0x6E66L,1L,0x07EDL,0x07EDL,1L,0x6E66L,2L,0xD805L,0x5C30L},{0x7DE2L,0x78DEL,0x01ABL,0x023AL,0x746AL,0x746AL,0x023AL,0x01ABL,0x78DEL},{1L,(-9L),0x6E66L,2L,0xB55CL,0L,2L,2L,0L}};
        int32_t l_1692 = (-7L);
        int32_t l_1694 = (-1L);
        int32_t l_1695[3][1][4] = {{{0x32CF6BB0L,(-1L),0x32CF6BB0L,(-1L)}},{{0x32CF6BB0L,(-1L),0x32CF6BB0L,(-1L)}},{{0x32CF6BB0L,(-1L),0x32CF6BB0L,(-1L)}}};
        float l_1701 = (-0x9.3p+1);
        int32_t *l_1708 = &l_1692;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1604[i] = &g_414;
        l_1580 ^= (l_1582 > ((void*)0 != l_1585[5][7]));
        for (g_926 = (-13); (g_926 > 21); g_926 = safe_add_func_uint16_t_u_u(g_926, 7))
        { /* block id: 705 */
            uint32_t l_1611[3][1];
            int16_t l_1612[6];
            int16_t ***l_1632 = &g_185;
            int32_t l_1690 = (-9L);
            int32_t l_1691[4][3][10] = {{{3L,0x44AE5D80L,(-1L),0L,0L,0x22363485L,(-1L),0xC8EB2560L,1L,4L},{0L,0xB2D44F5AL,0x419EC9C0L,0x27937292L,(-1L),0x22363485L,3L,4L,1L,(-10L)},{3L,0L,0x402DDBCBL,4L,0xC9157C5DL,1L,0x5FE25B4BL,0xE2E39E95L,0xC392DF80L,0x22363485L}},{{0xF6889D1AL,(-1L),0xC9157C5DL,6L,1L,(-1L),0xE2E39E95L,(-2L),0x7414B88BL,0x2594C14FL},{3L,0x97028452L,0xE2E39E95L,(-6L),(-7L),1L,(-4L),0L,(-1L),0x7205F748L},{0x93F959E3L,(-1L),0x97028452L,0L,4L,4L,0L,0x97028452L,(-1L),0x93F959E3L}},{{0xB2D44F5AL,4L,0x70A4EB65L,0xF6889D1AL,0xE1DAD087L,1L,(-10L),(-1L),0x419EC9C0L,0xF2C1A4B6L},{(-1L),(-2L),4L,0x70A4EB65L,0x402DDBCBL,1L,(-4L),0xF2C1A4B6L,8L,3L},{0x402DDBCBL,1L,0x2D603E6FL,(-6L),0x70A4EB65L,1L,0x2594C14FL,(-1L),0xE1DAD087L,0x22746729L}},{{(-4L),3L,(-1L),0x9F8DB88FL,0xC8EB2560L,(-1L),6L,(-6L),0x402DDBCBL,0x419EC9C0L},{0xF2C1A4B6L,(-1L),0x22746729L,(-1L),(-5L),(-7L),0x5FE25B4BL,0x419EC9C0L,(-1L),0x2594C14FL},{(-6L),1L,(-7L),0x93F959E3L,0x7414B88BL,0x93F959E3L,(-7L),1L,(-6L),1L}}};
            int8_t l_1699 = 0x7BL;
            int i, j, k;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1611[i][j] = 0x47FB8791L;
            }
            for (i = 0; i < 6; i++)
                l_1612[i] = 0x6E4CL;
            for (g_1558 = 1; (g_1558 != 23); g_1558++)
            { /* block id: 708 */
                int16_t ***l_1631 = &g_185;
                uint64_t l_1645 = 0x6774CDB3DF593BBBLL;
                int8_t ***l_1666 = &g_413;
                uint16_t * const l_1679[3] = {&g_1584[0][0][2],&g_1584[0][0][2],&g_1584[0][0][2]};
                int32_t l_1686 = 0xDFD44395L;
                int8_t l_1693[4][9] = {{0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L},{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L},{0L,0L,0L,0L,0L,0L,0L,0L,0L}};
                int32_t l_1696[6] = {0x1FFC0B82L,2L,0x1FFC0B82L,0x1FFC0B82L,2L,0x1FFC0B82L};
                int64_t l_1700 = 0x425D0413A3B262F1LL;
                int i, j;
                for (g_89 = 0; (g_89 <= 2); g_89 += 1)
                { /* block id: 711 */
                    uint32_t l_1614 = 0UL;
                    if (((p_6 && l_1591) | p_6))
                    { /* block id: 712 */
                        return l_1560;
                    }
                    else
                    { /* block id: 714 */
                        int8_t ***l_1605[5][1];
                        int32_t l_1613 = 8L;
                        const int64_t ****l_1618 = (void*)0;
                        const int64_t ****l_1619 = &l_1615[0][0][2];
                        int i, j;
                        for (i = 0; i < 5; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1605[i][j] = &g_413;
                        }
                        g_657[7] = ((safe_add_func_float_f_f(g_1558, (g_1584[0][3][0] < (((safe_div_func_float_f_f((safe_mul_func_float_f_f(g_89, ((safe_add_func_float_f_f((safe_mul_func_float_f_f((((((safe_mul_func_float_f_f((((g_1606 = l_1604[0]) == (void*)0) , (l_1580 = g_133)), (((safe_div_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((p_6 != (1L && l_1611[2][0])), l_1612[2])), g_1584[0][6][2])) || p_6) , l_1612[1]))) <= l_1613) < g_595) >= l_1613) == g_940), (-0x1.8p-1))), g_1326)) != l_1612[2]))), g_884[2])) > l_1614) == g_92)))) >= p_5);
                        (*l_1619) = l_1615[0][0][2];
                        if (p_6)
                            continue;
                    }
                    for (g_137 = 0; (g_137 <= 2); g_137 += 1)
                    { /* block id: 723 */
                        const uint64_t l_1620[10][10] = {{0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL,0x880806BBADA671FCLL,0x880806BBADA671FCLL,0xCC4D057D24ADC7EBLL,0x928A5FB31DE3A08ELL,0UL},{0UL,0UL,0x5CDCB76549C584F9LL,18446744073709551615UL,0xCC4D057D24ADC7EBLL,1UL,0UL,0xCC4D057D24ADC7EBLL,0xF06A9F842C01F830LL,0xCC4D057D24ADC7EBLL},{0UL,0x880806BBADA671FCLL,0UL,18446744073709551615UL,0UL,0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL},{0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL,0x880806BBADA671FCLL,0x880806BBADA671FCLL,0xCC4D057D24ADC7EBLL,0x928A5FB31DE3A08ELL,0UL},{0UL,0UL,0x5CDCB76549C584F9LL,18446744073709551615UL,0xCC4D057D24ADC7EBLL,1UL,0UL,0xCC4D057D24ADC7EBLL,0xF06A9F842C01F830LL,0xCC4D057D24ADC7EBLL},{0UL,0x880806BBADA671FCLL,0UL,18446744073709551615UL,0UL,0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL},{0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL,0x880806BBADA671FCLL,0x880806BBADA671FCLL,0xCC4D057D24ADC7EBLL,0x928A5FB31DE3A08ELL,0UL},{0UL,0UL,0x5CDCB76549C584F9LL,18446744073709551615UL,0xCC4D057D24ADC7EBLL,1UL,0UL,0xCC4D057D24ADC7EBLL,0xF06A9F842C01F830LL,0xCC4D057D24ADC7EBLL},{0UL,0x880806BBADA671FCLL,0UL,18446744073709551615UL,0UL,0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL},{0x880806BBADA671FCLL,0UL,0UL,0x928A5FB31DE3A08ELL,0xCC4D057D24ADC7EBLL,0x880806BBADA671FCLL,0x880806BBADA671FCLL,1UL,0UL,0x168FB15667CCA8F2LL}};
                        int i, j;
                        return l_1620[1][8];
                    }
                    return l_1614;
                }
                if ((safe_mul_func_uint16_t_u_u(p_6, (safe_rshift_func_uint8_t_u_s((safe_add_func_int64_t_s_s(p_6, (l_1591 = ((*g_883) ^ ((((safe_mod_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(p_6, ((l_1632 = l_1631) == (g_319[3] , (void*)0)))), ((safe_rshift_func_uint8_t_u_u(((+((safe_mul_func_int8_t_s_s(p_6, ((safe_sub_func_int8_t_s_s((~(safe_rshift_func_int16_t_s_s((((((safe_sub_func_int32_t_s_s(l_1612[2], l_1591)) > 0xE4L) >= 0xB158F9C0FFEC3C24LL) || 0x41L) | p_6), l_1591))), 1UL)) == 4L))) , l_1611[2][0])) > g_145[4]), 5)) && l_1645))) < (**g_763)) , (-7L)) , l_1645))))), 1)))))
                { /* block id: 730 */
                    uint8_t l_1650 = 0xA2L;
                    const int16_t *****l_1657 = (void*)0;
                    (*l_1586) = (safe_mul_func_float_f_f(0xF.8AC565p+42, (l_1650 = (safe_mul_func_float_f_f((g_657[0] = g_204[3]), (-0x7.Ep-1))))));
                    if (l_1591)
                        break;
                    g_657[6] = (p_6 , (((*l_1586) = l_1591) < l_1645));
                    if ((safe_unary_minus_func_uint32_t_u((g_769 , ((l_1657 = l_1652) == (void*)0)))))
                    { /* block id: 738 */
                        const int8_t *l_1664 = &g_1665;
                        const int8_t **l_1663 = &l_1664;
                        int32_t l_1682[3];
                        int16_t *l_1683 = &g_1324;
                        uint32_t *l_1684 = &g_940;
                        uint64_t *l_1685 = &g_1058;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1682[i] = 0L;
                        l_1582 = (safe_unary_minus_func_int32_t_s((((l_1580 = (((safe_sub_func_uint64_t_u_u((l_1591 & ((*l_1685) ^= (safe_add_func_int16_t_s_s((((*l_1663) = (*g_413)) == (*g_1606)), ((((((void*)0 != l_1666) < (safe_mod_func_int32_t_s_s((safe_rshift_func_int16_t_s_s(((safe_sub_func_uint8_t_u_u((*g_883), (safe_div_func_uint32_t_u_u((((safe_add_func_int64_t_s_s(((safe_div_func_uint32_t_u_u(((*l_1684) = (l_1679[0] == ((**g_762) = (l_1569 = (**g_762))))), l_1591)) , 0xC4638D76C79833C5LL), p_6)) >= g_64[0][0]) <= l_1650), 0x4F846141L)))) , l_1611[2][0]), p_6)), 4294967295UL))) , l_1645) > l_1560) >= l_1591))))), 0UL)) == l_1682[1]) & l_1560)) | l_1582) < p_6)));
                        return l_1682[1];
                    }
                    else
                    { /* block id: 747 */
                        (*l_1586) = (((-0x1.8p-1) < 0x8.Cp-1) > ((void*)0 == &l_1631));
                    }
                }
                else
                { /* block id: 750 */
                    int32_t *l_1687 = &g_92;
                    int32_t *l_1689[2][8][3] = {{{&g_1481,&g_1481,&l_1686},{&g_64[0][4],(void*)0,(void*)0},{&l_1686,&g_85,&l_1686},{&g_64[0][4],&g_85,&g_64[0][4]},{&g_1481,&l_1686,&l_1686},{&g_64[0][0],&g_64[0][0],(void*)0},{&l_1582,&l_1686,&l_1686},{(void*)0,&g_85,&g_64[0][0]}},{{&l_1582,&g_85,&l_1582},{&g_64[0][0],(void*)0,&g_64[0][0]},{&g_1481,&g_1481,&l_1686},{&g_64[0][4],(void*)0,(void*)0},{&l_1686,&g_85,&l_1686},{&g_64[0][4],&g_85,&g_64[0][4]},{&g_1481,&l_1686,&l_1686},{&g_64[0][0],&g_64[0][0],(void*)0}}};
                    int i, j, k;
                    g_1702++;
                }
                for (g_147 = (-9); (g_147 < 50); g_147 = safe_add_func_int32_t_s_s(g_147, 7))
                { /* block id: 755 */
                    (*g_1707) |= ((((*g_1485) = (p_6 , (void*)0)) == (void*)0) <= p_6);
                    return g_350[6][4][3];
                }
            }
        }
        (*l_1708) = 0x5FE61A72L;
        return p_6;
    }
    else
    { /* block id: 764 */
        uint8_t **l_1711 = (void*)0;
        int32_t l_1721[8] = {0x86ED784EL,0x86ED784EL,0x86ED784EL,0x86ED784EL,0x86ED784EL,0x86ED784EL,0x86ED784EL,0x86ED784EL};
        int i;
        for (p_6 = 0; (p_6 == 27); ++p_6)
        { /* block id: 767 */
            int32_t **l_1718[2];
            uint64_t *l_1722[1][9][6] = {{{&g_1058,&g_136,&g_1058,&g_136,&g_136,&g_136},{&g_1058,&g_1058,&g_136,&g_136,&g_1058,&g_1058},{&g_1058,&g_1058,&g_136,&g_136,&g_1058,&g_136},{&g_1058,&g_136,&g_1058,&g_136,&g_136,&g_136},{&g_1058,&g_1058,&g_136,&g_136,&g_1058,&g_1058},{&g_1058,&g_1058,&g_136,&g_136,&g_1058,&g_136},{&g_1058,&g_136,&g_1058,&g_136,&g_136,&g_136},{&g_1058,&g_1058,&g_136,&g_136,&g_1058,&g_1058},{&g_1058,&g_1058,&g_136,&g_136,&g_1058,&g_136}}};
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1718[i] = &g_60[0];
            l_1697[0] = (((l_1721[0] = (l_1560 <= ((((void*)0 != l_1711) , 0x1A1BBA8AL) || (safe_rshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_mul_func_int16_t_s_s((l_1580 = ((((l_1718[1] != (void*)0) == ((((p_6 <= (safe_rshift_func_int16_t_s_u((((((*g_882) == (void*)0) , 0UL) , p_6) && 0x04A0L), 6))) , 18446744073709551615UL) , (void*)0) != &g_881)) || p_6) > p_6)), l_1721[3])), l_1721[3])), l_1721[1]))))) < p_6) > p_6);
        }
        return g_319[1];
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_60 g_64 g_73 g_30 g_85 g_89 g_136 g_137 g_159 g_98 g_92 g_165 g_145 g_204 g_147 g_319 g_595 g_762 g_413 g_769 g_350 g_795 g_521 g_522 g_940 g_414 g_1058 g_1041 g_926 g_1324 g_904 g_905 g_814 g_185 g_1340 g_1335 g_296 g_1409 g_1481 g_1485 g_338 g_550 g_763 g_1545
 * writes: g_31 g_64 g_85 g_89 g_92 g_133 g_137 g_145 g_147 g_159 g_165 g_184 g_204 g_98 g_296 g_657 g_136 g_762 g_414 g_350 g_769 g_595 g_73 g_814 g_522 g_940 g_338 g_1058 g_319 g_60 g_1335 g_795 g_1369 g_30 g_1340 g_1485 g_1555
 */
static uint16_t  func_24(int16_t * p_25, int16_t * p_26, int32_t  p_27, int16_t  p_28, float  p_29)
{ /* block id: 2 */
    float l_33 = 0x2.Dp-1;
    int32_t l_49[10] = {0x739425A3L,0x739425A3L,0xED863780L,(-5L),0xED863780L,0x739425A3L,0x739425A3L,0xED863780L,(-5L),0xED863780L};
    int16_t *l_50 = &g_31;
    float *l_1556 = &l_33;
    int i;
    (*l_1556) = ((l_33 != func_34(func_37(g_31, (p_27 , (void*)0), (safe_mul_func_uint8_t_u_u(0UL, (0xA30B91300D3B51ADLL && 0x481D3EFE35FE57B6LL))), (g_31 , (safe_mul_func_int16_t_s_s(((l_49[0] = (safe_sub_func_uint32_t_u_u(g_31, 0x5E9D070FL))) >= 1UL), 0x4944L))), l_50), (*g_185))) < g_1481);
    return l_49[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_1340 g_137 g_31 g_64 g_350 g_1335 g_136 g_769 g_185 g_30 g_147 g_940 g_145 g_296 g_159 g_1409 g_904 g_905 g_814 g_521 g_522 g_204 g_1481 g_1058 g_1485 g_338 g_1324 g_550 g_763 g_73 g_1545 g_98 g_795
 * writes: g_657 g_133 g_795 g_137 g_769 g_1369 g_350 g_145 g_136 g_64 g_30 g_204 g_940 g_31 g_60 g_165 g_159 g_73 g_1340 g_1058 g_1485 g_338 g_85 g_1555
 */
static float  func_34(int16_t * p_35, int16_t * const  p_36)
{ /* block id: 592 */
    uint8_t l_1339 = 0xECL;
    uint16_t l_1408 = 0xB98DL;
    float *l_1421 = &g_133;
    float **l_1420 = &l_1421;
    int32_t l_1427 = 0x4AF853F6L;
    int32_t l_1428 = (-8L);
    int32_t l_1433 = 0x30622070L;
    int32_t l_1434[6] = {0x36B5AEA5L,0x36B5AEA5L,0L,0x36B5AEA5L,0x36B5AEA5L,0L};
    float l_1480 = 0x3.A2090Dp-23;
    const uint8_t *l_1534 = &g_1041;
    const uint8_t **l_1533 = &l_1534;
    const uint8_t ***l_1532[10] = {&l_1533,&l_1533,&l_1533,&l_1533,&l_1533,&l_1533,&l_1533,&l_1533,&l_1533,&l_1533};
    int16_t **l_1550 = &g_30[3];
    int i;
    if ((l_1339 & (g_1340 <= l_1339)))
    { /* block id: 593 */
        float *l_1341 = &g_657[0];
        float *l_1342 = (void*)0;
        float *l_1343 = &g_133;
        int32_t l_1388 = (-1L);
        int64_t *l_1398 = &g_1340;
        int32_t ***l_1416[8][7][4] = {{{&g_814,(void*)0,&g_814,(void*)0},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,(void*)0,&g_814},{&g_814,&g_814,&g_814,&g_814}},{{(void*)0,&g_814,(void*)0,(void*)0},{(void*)0,(void*)0,&g_814,&g_814},{&g_814,(void*)0,(void*)0,&g_814},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,&g_814},{&g_814,(void*)0,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814}},{{(void*)0,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,(void*)0,&g_814},{(void*)0,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,&g_814}},{{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{(void*)0,&g_814,(void*)0,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{(void*)0,&g_814,&g_814,(void*)0}},{{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,(void*)0,&g_814},{(void*)0,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814}},{{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{(void*)0,&g_814,(void*)0,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{(void*)0,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,(void*)0}},{{&g_814,&g_814,(void*)0,&g_814},{(void*)0,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814}},{{&g_814,&g_814,&g_814,&g_814},{(void*)0,&g_814,(void*)0,&g_814},{&g_814,&g_814,&g_814,&g_814},{&g_814,&g_814,&g_814,&g_814},{(void*)0,&g_814,&g_814,(void*)0},{&g_814,&g_814,&g_814,(void*)0},{&g_814,&g_814,(void*)0,&g_814}}};
        int16_t ** const *l_1424 = (void*)0;
        int16_t ** const **l_1423[7] = {&l_1424,&l_1424,&l_1424,&l_1424,&l_1424,&l_1424,&l_1424};
        int16_t l_1429 = 0x26E9L;
        int i, j, k;
lbl_1468:
        (*l_1343) = ((*l_1341) = g_137);
        for (g_795 = 0; (g_795 >= (-18)); g_795 = safe_sub_func_uint8_t_u_u(g_795, 1))
        { /* block id: 598 */
            uint64_t l_1351 = 1UL;
            const uint16_t *l_1365 = &g_159;
            const uint16_t **l_1364 = &l_1365;
            const uint16_t ***l_1363[5];
            const uint16_t ****l_1362 = &l_1363[0];
            float *l_1368 = &g_1369[1];
            int8_t *l_1370[2][2];
            uint8_t *l_1371 = &l_1339;
            int32_t l_1372 = (-10L);
            float **l_1419 = &l_1341;
            uint8_t l_1422 = 0x6FL;
            int32_t l_1426[7] = {0xC5D09C13L,0xDA08D219L,0xDA08D219L,0xC5D09C13L,0xDA08D219L,0xDA08D219L,0xC5D09C13L};
            int i, j;
            for (i = 0; i < 5; i++)
                l_1363[i] = &l_1364;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 2; j++)
                    l_1370[i][j] = &g_350[1][2][4];
            }
            for (g_137 = 6; (g_137 < (-20)); g_137--)
            { /* block id: 601 */
                for (g_769 = 4; (g_769 >= 0); g_769 -= 1)
                { /* block id: 604 */
                    uint64_t l_1348 = 1UL;
                    l_1348++;
                }
            }
            if (l_1351)
                continue;
            l_1372 |= ((safe_rshift_func_uint8_t_u_u((((safe_rshift_func_uint8_t_u_u((((0xB6FBL > 0UL) >= 0x3751L) || (*p_35)), ((*l_1371) = ((*g_1335) = (safe_add_func_int8_t_s_s(l_1351, (g_350[1][1][2] ^= ((((safe_sub_func_float_f_f(((*l_1368) = ((*l_1341) = ((safe_add_func_float_f_f((l_1351 != ((l_1362 != &g_762) > ((*l_1343) = (safe_add_func_float_f_f(((-0x1.Ap+1) >= (-0x1.3p+1)), g_64[0][0]))))), l_1351)) > 0xC.1898CAp-16))), 0x1.Ep+1)) , (void*)0) != (void*)0) , 0L)))))))) <= 0xD0BD2A07CFBC7C1ELL) < 0xBBF0E30AL), 5)) || l_1351);
            for (g_769 = 1; (g_769 >= 0); g_769 -= 1)
            { /* block id: 618 */
                int16_t l_1379 = (-6L);
                int32_t l_1425 = 0xB6E195C4L;
                int32_t l_1431 = 0xC89CDF34L;
                int32_t l_1439 = 0x396A32BEL;
                uint64_t l_1442 = 0x2AEA397FCD42C12DLL;
                for (g_136 = 0; (g_136 <= 1); g_136 += 1)
                { /* block id: 621 */
                    int32_t l_1430 = 0x89BA232EL;
                    int32_t l_1432 = 0x93638374L;
                    int32_t l_1437 = 0x0715F70CL;
                    int32_t l_1438 = 0x8BE7201CL;
                    int32_t l_1440 = 0xD17612BBL;
                    int32_t **l_1462[4] = {&g_60[0],&g_60[0],&g_60[0],&g_60[0]};
                    int i, j;
                    g_64[g_136][(g_136 + 1)] &= (1L <= 246UL);
                    if ((safe_add_func_int8_t_s_s((safe_lshift_func_int8_t_s_s(g_64[g_136][(g_769 + 5)], (((**g_185) , (safe_rshift_func_uint16_t_u_u(l_1379, 4))) & (safe_sub_func_uint64_t_u_u(1UL, ((+((((safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint16_t_u_u(65527UL, 14)), 0)) ^ l_1339) <= (0xA9BBL & (!l_1388))) ^ (0x4EFEFB57L >= g_147))) <= l_1351)))))), 0x3DL)))
                    { /* block id: 623 */
                        int16_t *l_1389 = &g_137;
                        uint32_t *l_1399 = &g_940;
                        int32_t l_1405 = 0x9B2E6733L;
                        (**g_905) = func_51(((*g_185) = l_1389), (((safe_mod_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((safe_sub_func_uint16_t_u_u(((safe_add_func_uint32_t_u_u((g_204[3] = l_1339), ((*l_1399) ^= (g_64[g_136][(g_769 + 5)] != ((void*)0 == l_1398))))) >= (safe_div_func_int8_t_s_s((safe_div_func_int16_t_s_s(((*p_36) |= (((((*g_1335) ^= 0UL) && l_1372) || (!l_1405)) , ((safe_mod_func_uint8_t_u_u(255UL, l_1388)) > g_769))), 4L)), l_1379))), l_1351)) == l_1388), 0x9440L)), l_1339)) | l_1408) , (*g_296)), l_1372, g_1409[1], g_64[0][0]);
                        l_1425 |= (safe_add_func_uint16_t_u_u((0xD5E065200E5974ECLL != (((*g_521) != ((((((safe_sub_func_uint16_t_u_u(((*g_296) |= 65535UL), (0x9A27A7DE02492BA9LL > (g_136 , (safe_mod_func_uint32_t_u_u(((*l_1399) = ((l_1416[4][4][0] != (*g_904)) | (((*l_1389) = (safe_lshift_func_uint16_t_u_s((l_1419 == l_1420), 12))) , l_1422))), l_1339)))))) , 0L) || (-1L)) & l_1372) || 4294967295UL) , l_1423[5])) > (-7L))), l_1408));
                    }
                    else
                    { /* block id: 634 */
                        int64_t l_1435 = 0L;
                        int32_t l_1436 = 0x545E54CCL;
                        float l_1441[6][10] = {{0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1),0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1)},{0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1),0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1)},{0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1),0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1)},{0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1),0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1)},{0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1),0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1)},{0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1),0xC.3E3688p-52,(-0x1.Fp+1),(-0x1.Fp+1),0xC.3E3688p-52,(-0x1.0p-1)}};
                        uint32_t *l_1465 = (void*)0;
                        uint32_t *l_1466 = &g_73;
                        uint32_t *l_1467 = &g_204[3];
                        int i, j;
                        l_1442++;
                        l_1436 = (safe_add_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((g_145[4] < l_1426[2]) | ((safe_mod_func_uint8_t_u_u(((0xD315F0291D506FC4LL < (safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(((l_1436 > ((safe_div_func_int64_t_s_s((((((*l_1467) ^= ((~((*l_1398) = ((&l_1408 == ((*l_1364) = &l_1408)) <= ((safe_lshift_func_int16_t_s_u(l_1425, ((l_1462[1] == (((*l_1466) = (safe_rshift_func_int16_t_s_u(0xD735L, l_1426[2]))) , (void*)0)) >= (-4L)))) , 0xB1L)))) , 0x03965818L)) != 1L) , g_64[0][6]) < l_1379), l_1408)) > l_1422)) && 0UL), 1)), 0x9EL))) || (-5L)), l_1433)) | (*p_35))), 0x9AL)), 2L)), 0xC8010BDD37726623LL));
                    }
                }
                if (g_136)
                    goto lbl_1468;
                if (g_31)
                    goto lbl_1469;
                return l_1431;
            }
        }
lbl_1469:
        (**g_905) = &l_1434[4];
        for (g_137 = 9; (g_137 >= 3); g_137 -= 1)
        { /* block id: 651 */
            uint32_t l_1474 = 0UL;
            int32_t *l_1477 = &l_1428;
            int8_t *l_1482 = &g_350[8][4][2];
            int32_t l_1483 = 0x2F9DCA14L;
            int64_t ***l_1489 = &g_1486;
            float l_1551 = 0x1.Cp+1;
            int32_t *l_1552 = &g_98;
            int i;
            l_1483 ^= ((((*l_1482) &= (((safe_mod_func_uint8_t_u_u(((l_1433 <= (((&g_1058 == (void*)0) | (safe_rshift_func_uint8_t_u_u(l_1474, l_1428))) ^ ((safe_mul_func_uint16_t_u_u((l_1477 != l_1477), (safe_mod_func_int64_t_s_s((((*l_1398) = ((((l_1408 || 1UL) >= g_1481) < (*p_35)) > (*g_1335))) && (*l_1477)), 0xB0C3650673D474ADLL)))) < (-10L)))) >= 0L), (-8L))) <= l_1434[4]) > (*l_1477))) , l_1339) || 1UL);
            for (g_1058 = 0; (g_1058 <= 5); g_1058 += 1)
            { /* block id: 657 */
                int16_t *l_1484 = &g_338;
                int32_t l_1490 = 2L;
                uint64_t l_1508 = 0x63C539940E456B88LL;
                int16_t ****l_1542[2][10] = {{(void*)0,(void*)0,&g_184[6],&g_184[3],(void*)0,&g_184[3],&g_184[6],(void*)0,(void*)0,&g_184[6]},{(void*)0,&g_184[3],(void*)0,(void*)0,&g_184[3],(void*)0,&g_184[6],(void*)0,&g_184[3],(void*)0}};
                int i, j;
                for (l_1483 = 5; (l_1483 >= 0); l_1483 -= 1)
                { /* block id: 660 */
                    int i;
                    return l_1434[g_1058];
                }
                for (l_1433 = 9; (l_1433 >= 0); l_1433 -= 1)
                { /* block id: 665 */
                    int64_t ****l_1488 = &g_1485;
                    int32_t l_1510 = 0xDDAD6722L;
                    int8_t ***l_1549 = &g_413;
                    int8_t ****l_1548 = &l_1549;
                    int i;
                    (***g_904) = func_51(l_1484, (l_1490 = (((*l_1488) = ((*p_36) , g_1485)) != l_1489)), l_1408, func_76((safe_div_func_float_f_f((g_1369[g_1058] = ((((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int8_t_s_u(((!(safe_add_func_int16_t_s_s((((l_1484 != (*g_185)) != (safe_add_func_int8_t_s_s(((*l_1482) = ((((-2L) < (((safe_rshift_func_int16_t_s_s(((*l_1484) ^= ((safe_rshift_func_int8_t_s_u(((safe_lshift_func_uint8_t_u_s((*l_1477), 4)) == 0x97CDD06701139895LL), 3)) , 0xA278L)), 9)) ^ 1UL) >= (*g_1335))) && l_1433) | 0x7959C2B6E645683DLL)), 1UL))) & 3UL), 0x597FL))) | g_137), 0)) , 3L), l_1339)) || (-8L)) , l_1428) != g_1324)), 0x9.178089p-7)), g_550, l_1339, l_1508, l_1508), l_1433);
                    l_1434[4] = ((safe_unary_minus_func_uint8_t_u(((*g_1335) = l_1510))) && (safe_unary_minus_func_uint32_t_u((((safe_mul_func_int16_t_s_s(((l_1510 >= l_1508) || (safe_div_func_int8_t_s_s(((((g_136 |= (l_1428 != (safe_sub_func_int16_t_s_s((safe_div_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((**g_763), l_1434[4])), (safe_sub_func_uint64_t_u_u((0L > ((*g_1335) = (safe_lshift_func_uint16_t_u_s((((safe_rshift_func_uint8_t_u_s((((l_1339 == ((*p_36) <= l_1408)) , 0xD3E54E10L) >= l_1510), 0)) , (*g_1335)) && l_1433), (*p_36))))), l_1510)))), l_1434[4])))) >= g_73) | l_1434[2]) , 0xADL), 0x94L))), l_1510)) || l_1510) && 0xCB67E290A8CDA56FLL))));
                    if ((((*l_1477) < l_1508) < ((*l_1482) = (safe_add_func_int8_t_s_s((safe_add_func_int32_t_s_s(((void*)0 == l_1532[0]), (safe_mod_func_int8_t_s_s((safe_add_func_uint8_t_u_u(((&p_35 == ((+(safe_add_func_int16_t_s_s(((void*)0 != l_1542[1][7]), ((safe_lshift_func_uint16_t_u_u(l_1434[4], 5)) && ((((((*l_1420) == l_1343) , g_1545) != l_1548) , 0x31L) | (*l_1477)))))) , l_1550)) , (*g_1335)), l_1508)), l_1508)))), (*g_1335))))))
                    { /* block id: 677 */
                        return l_1510;
                    }
                    else
                    { /* block id: 679 */
                        return g_98;
                    }
                }
            }
            if ((*l_1477))
                continue;
            (***g_904) = l_1552;
        }
    }
    else
    { /* block id: 687 */
        const int32_t *l_1553 = &g_550;
        const int32_t **l_1554[7][4][7] = {{{&l_1553,&l_1553,&l_1553,(void*)0,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,&l_1553,(void*)0,(void*)0,&l_1553},{&l_1553,(void*)0,(void*)0,&l_1553,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,(void*)0,&l_1553,&l_1553,&l_1553}},{{&l_1553,&l_1553,&l_1553,(void*)0,(void*)0,(void*)0,&l_1553},{&l_1553,&l_1553,(void*)0,&l_1553,(void*)0,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,&l_1553,(void*)0,(void*)0,&l_1553}},{{&l_1553,&l_1553,(void*)0,&l_1553,(void*)0,&l_1553,&l_1553},{&l_1553,(void*)0,(void*)0,&l_1553,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,(void*)0,&l_1553,(void*)0,&l_1553,&l_1553}},{{&l_1553,(void*)0,(void*)0,(void*)0,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,(void*)0,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,&l_1553,(void*)0,(void*)0,&l_1553},{&l_1553,(void*)0,(void*)0,&l_1553,&l_1553,&l_1553,&l_1553}},{{&l_1553,&l_1553,&l_1553,&l_1553,(void*)0,&l_1553,&l_1553},{&l_1553,(void*)0,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553},{(void*)0,(void*)0,&l_1553,(void*)0,(void*)0,&l_1553,&l_1553},{(void*)0,(void*)0,&l_1553,&l_1553,&l_1553,&l_1553,(void*)0}},{{&l_1553,(void*)0,(void*)0,&l_1553,(void*)0,&l_1553,(void*)0},{&l_1553,&l_1553,&l_1553,(void*)0,&l_1553,&l_1553,&l_1553},{(void*)0,&l_1553,(void*)0,&l_1553,(void*)0,(void*)0,&l_1553},{(void*)0,&l_1553,&l_1553,&l_1553,&l_1553,(void*)0,(void*)0}},{{&l_1553,&l_1553,(void*)0,(void*)0,&l_1553,(void*)0,(void*)0},{&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,(void*)0,&l_1553},{&l_1553,&l_1553,(void*)0,&l_1553,&l_1553,&l_1553,&l_1553},{&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553,&l_1553}}};
        int i, j, k;
        g_1555 = l_1553;
    }
    return g_145[4];
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_31 g_64 g_73 g_30 g_85 g_89 g_136 g_137 g_159 g_98 g_92 g_165 g_145 g_204 g_147 g_319 g_595 g_762 g_413 g_769 g_350 g_795 g_521 g_522 g_940 g_414 g_1058 g_1041 g_926 g_1324 g_904 g_905 g_814 g_185
 * writes: g_31 g_64 g_85 g_89 g_92 g_133 g_137 g_145 g_147 g_159 g_165 g_184 g_204 g_98 g_296 g_657 g_136 g_762 g_414 g_350 g_769 g_595 g_73 g_814 g_522 g_940 g_338 g_1058 g_319 g_60 g_1335
 */
static int16_t * func_37(int64_t  p_38, int16_t * p_39, uint64_t  p_40, int32_t  p_41, int16_t * p_42)
{ /* block id: 4 */
    const int16_t l_61 = 7L;
    int32_t **l_1332 = &g_60[0];
    uint8_t *l_1333 = &g_145[4];
    uint8_t **l_1334[6][8][5] = {{{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{&l_1333,&l_1333,&l_1333,&l_1333,(void*)0},{&l_1333,(void*)0,(void*)0,&l_1333,&l_1333},{(void*)0,&l_1333,(void*)0,&l_1333,(void*)0},{&l_1333,&l_1333,(void*)0,(void*)0,&l_1333},{(void*)0,&l_1333,&l_1333,&l_1333,&l_1333},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{&l_1333,&l_1333,&l_1333,&l_1333,(void*)0}},{{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,&l_1333,&l_1333,&l_1333,(void*)0},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,&l_1333,&l_1333,&l_1333,&l_1333},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{&l_1333,&l_1333,&l_1333,&l_1333,(void*)0},{&l_1333,(void*)0,(void*)0,&l_1333,&l_1333},{(void*)0,&l_1333,(void*)0,&l_1333,(void*)0}},{{&l_1333,&l_1333,(void*)0,(void*)0,&l_1333},{(void*)0,&l_1333,&l_1333,&l_1333,&l_1333},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{&l_1333,&l_1333,&l_1333,&l_1333,(void*)0},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1333},{&l_1333,(void*)0,(void*)0,&l_1333,(void*)0}},{{&l_1333,&l_1333,&l_1333,(void*)0,&l_1333},{&l_1333,&l_1333,&l_1333,(void*)0,&l_1333},{&l_1333,(void*)0,(void*)0,&l_1333,(void*)0},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,&l_1333,(void*)0,(void*)0,&l_1333},{&l_1333,(void*)0,&l_1333,&l_1333,&l_1333},{&l_1333,(void*)0,&l_1333,&l_1333,&l_1333},{(void*)0,&l_1333,(void*)0,(void*)0,&l_1333}},{{&l_1333,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1333},{&l_1333,(void*)0,(void*)0,&l_1333,(void*)0},{&l_1333,&l_1333,&l_1333,(void*)0,&l_1333},{&l_1333,&l_1333,&l_1333,(void*)0,&l_1333},{&l_1333,(void*)0,(void*)0,&l_1333,(void*)0},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333}},{{(void*)0,&l_1333,(void*)0,(void*)0,&l_1333},{&l_1333,(void*)0,&l_1333,&l_1333,&l_1333},{&l_1333,(void*)0,&l_1333,&l_1333,&l_1333},{(void*)0,&l_1333,(void*)0,(void*)0,&l_1333},{&l_1333,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1333,&l_1333,(void*)0,&l_1333,&l_1333},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1333},{&l_1333,(void*)0,(void*)0,&l_1333,(void*)0}}};
    uint8_t *l_1336 = &g_147;
    int i, j, k;
    (*l_1332) = func_51(func_57(g_60[2], l_61), func_68(g_73, func_74(func_76(l_61, g_73, (((safe_add_func_int16_t_s_s(l_61, ((((((0xDF500C33L > p_41) <= l_61) , 0x090FL) > g_73) == p_41) & 0x55L))) , (-1L)) & 0x67L), g_73, l_61)), p_42, l_61), g_1324, p_42, l_61);
    (**g_905) = (((g_1335 = l_1333) != l_1336) , &p_41);
    return (*g_185);
}


/* ------------------------------------------ */
/* 
 * reads : g_904 g_905 g_814
 * writes: g_60 g_165
 */
static int32_t * func_51(int16_t * p_52, uint16_t  p_53, uint32_t  p_54, int16_t * p_55, const uint64_t  p_56)
{ /* block id: 583 */
    int32_t *l_1325[3][3];
    int32_t l_1327[8] = {(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)};
    uint32_t l_1328 = 0UL;
    int32_t *l_1331 = &g_64[0][7];
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            l_1325[i][j] = (void*)0;
    }
    --l_1328;
    (***g_904) = l_1325[1][2];
    (**g_905) = l_1325[1][2];
    return l_1331;
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_64
 * writes: g_31 g_64
 */
static int16_t * func_57(int32_t * p_58, const uint16_t  p_59)
{ /* block id: 5 */
    int16_t *l_67[2];
    int i;
    for (i = 0; i < 2; i++)
        l_67[i] = &g_31;
    for (g_31 = (-3); (g_31 >= (-6)); g_31--)
    { /* block id: 8 */
        for (g_64[0][0] = (-17); (g_64[0][0] <= 16); g_64[0][0] = safe_add_func_uint64_t_u_u(g_64[0][0], 4))
        { /* block id: 11 */
            return l_67[0];
        }
    }
    return &g_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_89 g_137 g_204 g_98 g_145 g_319 g_147 g_595 g_159 g_85 g_136 g_762 g_413 g_769 g_92 g_350 g_795 g_521 g_522 g_940 g_414 g_1058 g_1041 g_926 g_1324
 * writes: g_137 g_145 g_165 g_133 g_159 g_657 g_85 g_136 g_147 g_762 g_414 g_350 g_769 g_595 g_73 g_92 g_814 g_522 g_89 g_204 g_940 g_338 g_1058 g_98 g_319
 */
static uint16_t  func_68(int64_t  p_69, int16_t * p_70, int16_t * p_71, uint8_t  p_72)
{ /* block id: 125 */
    int16_t *****l_335 = (void*)0;
    int32_t l_336 = 1L;
    int16_t *l_337[5] = {&g_338,&g_338,&g_338,&g_338,&g_338};
    int32_t l_339[3][10][4] = {{{2L,0xDF0D93D8L,0xDF0D93D8L,2L},{0xB4687BF4L,0x94AD0A1EL,0L,5L},{0x55470488L,0xAE905A57L,5L,0xA7C000D3L},{5L,0xA7C000D3L,0xEE1E9C62L,0xA7C000D3L},{0xDF0D93D8L,0xAE905A57L,0xF4D0AC22L,5L},{0x13AE606FL,0x94AD0A1EL,0xA7C000D3L,2L},{0xF4D0AC22L,0xDF0D93D8L,0xFCB9613DL,0xFCB9613DL},{0xF4D0AC22L,0xF4D0AC22L,0xA7C000D3L,0xB4687BF4L},{0x13AE606FL,0xFCB9613DL,0xF4D0AC22L,0x94AD0A1EL},{0xDF0D93D8L,0x55470488L,0xEE1E9C62L,0xF4D0AC22L}},{{5L,0x55470488L,5L,0xAE905A57L},{0xB4687BF4L,0xDF0D93D8L,0xEE1E9C62L,0x94AD0A1EL},{0x94AD0A1EL,0xA7C000D3L,2L,0xDF0D93D8L},{0xF4D0AC22L,2L,2L,0xF4D0AC22L},{0x94AD0A1EL,0xAE905A57L,0xEE1E9C62L,0xFCB9613DL},{0xB4687BF4L,0L,0xFCB9613DL,0x55470488L},{0xFCB9613DL,0x55470488L,0x13AE606FL,0x55470488L},{2L,0L,0xA7C000D3L,0xFCB9613DL},{5L,0xAE905A57L,0x55470488L,0xF4D0AC22L},{0xA7C000D3L,2L,0xDF0D93D8L,0xDF0D93D8L}},{{0xA7C000D3L,0xA7C000D3L,0x55470488L,0x94AD0A1EL},{5L,0xDF0D93D8L,0xA7C000D3L,0xAE905A57L},{2L,0xB4687BF4L,0x13AE606FL,0xA7C000D3L},{0xFCB9613DL,0xB4687BF4L,0xFCB9613DL,0xAE905A57L},{0xB4687BF4L,0xDF0D93D8L,0xEE1E9C62L,0x94AD0A1EL},{0x94AD0A1EL,0xA7C000D3L,2L,0xDF0D93D8L},{0xF4D0AC22L,2L,2L,0xF4D0AC22L},{0x94AD0A1EL,0xAE905A57L,0xEE1E9C62L,0xFCB9613DL},{0xB4687BF4L,0L,0xFCB9613DL,0x55470488L},{0xFCB9613DL,0x55470488L,0x13AE606FL,0x55470488L}}};
    float *l_348 = &g_133;
    int32_t l_371 = (-2L);
    uint16_t l_504 = 0x08B3L;
    uint64_t l_549 = 0x303C48A45170DFD6LL;
    const uint8_t *l_600 = &g_145[4];
    const uint8_t **l_599[4];
    const uint8_t l_681 = 252UL;
    int32_t *l_719 = (void*)0;
    int32_t *l_720 = &g_85;
    uint16_t *l_727 = &l_504;
    uint8_t *l_739 = &g_147;
    uint16_t l_870 = 0x620BL;
    int16_t l_1001 = 0L;
    uint64_t l_1004 = 8UL;
    uint8_t **l_1014 = (void*)0;
    uint8_t ***l_1013 = &l_1014;
    uint16_t l_1177 = 1UL;
    int16_t * const **l_1181 = (void*)0;
    int16_t * const ***l_1180 = &l_1181;
    uint16_t l_1192 = 65526UL;
    int32_t l_1212 = 0x5CFCC6FDL;
    uint16_t ***l_1246 = &g_763;
    uint16_t ****l_1245 = &l_1246;
    uint16_t *****l_1244 = &l_1245;
    int8_t l_1286 = 0x80L;
    uint16_t l_1321 = 0xDF19L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_599[i] = &l_600;
lbl_974:
    if ((g_73 , (safe_div_func_uint64_t_u_u(((g_89 , (p_69 & (((safe_sub_func_uint8_t_u_u((safe_div_func_int64_t_s_s((safe_mod_func_int16_t_s_s((safe_mul_func_int16_t_s_s((l_339[2][6][3] ^= ((*p_70) &= (safe_add_func_uint16_t_u_u((0L == ((void*)0 == l_335)), l_336)))), (18446744073709551615UL > p_72))), g_204[3])), l_336)), l_336)) <= p_69) | 18446744073709551615UL))) == l_336), l_336))))
    { /* block id: 128 */
        int8_t *l_349[2];
        int32_t l_351 = 0x69016A66L;
        const int32_t l_354 = (-1L);
        uint8_t *l_355 = &g_145[4];
        const uint16_t *l_360 = &g_159;
        int32_t l_369 = 0xD43575AFL;
        float *l_374 = &g_133;
        int16_t *** const l_527 = &g_185;
        const int16_t l_552 = 0x698FL;
        int32_t l_573 = 0x94B52D4FL;
        uint8_t **l_603 = &l_355;
        int32_t l_663 = 0x0506F04DL;
        int32_t l_664 = 0xE4B44343L;
        int32_t l_665 = 0x8BA2D645L;
        int32_t l_666 = 6L;
        int32_t l_667 = 0x1C0AD85CL;
        int32_t l_668 = 0x7FF9B10BL;
        int32_t l_669 = (-7L);
        int32_t l_670[8] = {(-5L),0xBFC19294L,0xBFC19294L,(-5L),0xBFC19294L,0xBFC19294L,(-5L),0xBFC19294L};
        int64_t l_705[3];
        int32_t ***l_708 = (void*)0;
        int32_t *l_710 = &l_670[7];
        int i;
        for (i = 0; i < 2; i++)
            l_349[i] = &g_350[1][1][2];
        for (i = 0; i < 3; i++)
            l_705[i] = (-1L);
        if ((safe_sub_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u((((((safe_mul_func_uint16_t_u_u(1UL, 0x4A9DL)) ^ ((-1L) | ((((*l_355) &= ((safe_mul_func_int8_t_s_s(p_69, (l_351 &= (l_348 == (void*)0)))) & (safe_sub_func_uint64_t_u_u((6UL != (((((g_204[4] <= l_354) & 0x20B5L) != p_72) <= g_204[3]) & l_354)), g_98)))) , &g_165) != &g_60[2]))) || l_354) != p_69) < (*p_70)), 6)), p_69)))
        { /* block id: 131 */
            int32_t **l_356 = &g_165;
            uint16_t *l_359[10] = {(void*)0,(void*)0,&g_159,(void*)0,(void*)0,&g_159,(void*)0,(void*)0,&g_159,(void*)0};
            uint8_t *l_370 = &g_147;
            float *l_372[5] = {&g_133,&g_133,&g_133,&g_133,&g_133};
            float **l_373[4] = {&l_348,&l_348,&l_348,&l_348};
            int32_t l_406 = 0x3629C55FL;
            int8_t **l_412[6] = {(void*)0,&l_349[1],&l_349[1],(void*)0,&l_349[1],&l_349[1]};
            const uint16_t ***l_479 = (void*)0;
            int32_t l_492[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            int64_t l_524[6];
            uint64_t *l_556 = &g_136;
            int i;
            for (i = 0; i < 6; i++)
                l_524[i] = 0x3880585DC4AE50C9LL;
            (*l_356) = &g_85;
        }
        else
        { /* block id: 253 */
            uint8_t **l_601 = &l_355;
            uint8_t ***l_602[10][7][1] = {{{&l_601},{(void*)0},{&l_601},{&l_601},{(void*)0},{&l_601},{(void*)0}},{{&l_601},{&l_601},{(void*)0},{&l_601},{&l_601},{&l_601},{&l_601}},{{&l_601},{(void*)0},{&l_601},{&l_601},{(void*)0},{&l_601},{(void*)0}},{{&l_601},{&l_601},{(void*)0},{&l_601},{&l_601},{&l_601},{&l_601}},{{&l_601},{(void*)0},{&l_601},{&l_601},{(void*)0},{&l_601},{(void*)0}},{{&l_601},{&l_601},{(void*)0},{&l_601},{&l_601},{&l_601},{&l_601}},{{&l_601},{(void*)0},{&l_601},{&l_601},{(void*)0},{&l_601},{(void*)0}},{{&l_601},{&l_601},{(void*)0},{&l_601},{&l_601},{&l_601},{&l_601}},{{&l_601},{(void*)0},{&l_601},{&l_601},{(void*)0},{&l_601},{(void*)0}},{{&l_601},{&l_601},{(void*)0},{&l_601},{&l_601},{&l_601},{&l_601}}};
            int32_t l_604[8] = {0x6A9A56D5L,0x6A9A56D5L,0x6A9A56D5L,0x6A9A56D5L,0x6A9A56D5L,0x6A9A56D5L,0x6A9A56D5L,0x6A9A56D5L};
            int32_t l_651 = 0x281B2A45L;
            uint16_t l_653 = 65531UL;
            float **l_658 = &l_374;
            uint16_t l_671 = 65535UL;
            int8_t l_680 = 0L;
            int32_t **l_712 = &l_710;
            int i, j, k;
            l_604[6] |= (l_599[2] == (l_603 = l_601));
lbl_713:
            if ((0x2EF8CA27L != l_604[3]))
            { /* block id: 256 */
                uint32_t l_650 = 0xFAA675AEL;
                uint8_t ***l_652 = &l_601;
                int32_t l_654 = 0x7DBF01EEL;
                uint16_t *l_655 = &g_159;
                float *l_656 = &g_657[0];
                uint32_t *l_659[3];
                int32_t **l_660 = &g_60[2];
                int32_t *l_661 = &g_85;
                int i;
                for (i = 0; i < 3; i++)
                    l_659[i] = &g_204[0];
                l_604[6] |= 1L;
                (*l_661) |= (safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint8_t_u_s((safe_sub_func_int8_t_s_s((((safe_add_func_int32_t_s_s(((l_654 = ((((safe_sub_func_uint8_t_u_u((((safe_add_func_float_f_f(((*l_656) = ((safe_sub_func_float_f_f((safe_add_func_float_f_f(((((safe_rshift_func_int8_t_s_s((((safe_mul_func_uint16_t_u_u(((*l_655) ^= (((l_339[1][8][1] = (safe_mod_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(l_604[1], (safe_add_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(((((((safe_unary_minus_func_int8_t_s(l_354)) , (((*p_70) = (-1L)) > ((((((((safe_mul_func_uint16_t_u_u(p_69, l_504)) == p_69) & (g_145[1] > (((safe_div_func_int8_t_s_s((safe_div_func_uint64_t_u_u((((safe_sub_func_float_f_f((safe_add_func_float_f_f((safe_div_func_float_f_f(((((g_133 = g_89) >= (!(((safe_add_func_float_f_f((l_651 = (((l_650 = (g_98 >= 0x1.0p+1)) < 0xA.F270E8p+51) >= g_145[2])), l_354)) == (-0x1.9p-1)) == 0x9.09EE27p-87))) >= 0x1.6p+1) <= p_69), (-0x7.1p+1))), l_573)), l_371)) , (void*)0) != (void*)0), p_69)), (-10L))) >= p_72) ^ p_72))) && g_319[8]) < p_72) , 3UL) , l_652) == &l_601))) || l_369) & l_604[0]) || g_147) <= l_604[6]), 0x23E3L)), l_604[6])))), g_595)), l_653))) >= l_654) == l_654)), p_69)) , &p_71) != &p_70), 7)) || 0xE73AL) ^ 0xB8D3L) , 0x8.F0D124p-56), l_354)), l_653)) < p_72)), l_654)) , l_658) != (void*)0), 0xA7L)) != l_654) < l_573) < 0UL)) >= p_69), 0UL)) , (void*)0) == l_660), p_72)), l_549))));
            }
            else
            { /* block id: 267 */
                int32_t *l_662[9] = {&l_339[0][7][1],&l_371,&l_339[0][7][1],&l_339[0][7][1],&l_371,&l_339[0][7][1],&l_339[0][7][1],&l_371,&l_339[0][7][1]};
                int32_t **l_711 = &l_662[1];
                int i;
                --l_671;
                l_371 |= (((safe_lshift_func_uint16_t_u_s(g_145[0], (safe_unary_minus_func_int8_t_s((((!((0UL <= (l_604[6] = ((((safe_mul_func_int8_t_s_s(p_69, 0xCDL)) , p_69) & (0xE41D9E40A73EFE97LL == ((((void*)0 != &l_371) , l_349[1]) == l_600))) < l_680))) < g_136)) | l_669) , 0x05L))))) , g_595) | l_681);
                for (l_336 = 0; (l_336 <= 2); l_336 += 1)
                { /* block id: 273 */
                    int32_t l_682 = 1L;
                    uint16_t l_683 = 0xA698L;
                    int16_t **** const l_686 = (void*)0;
                    const uint32_t l_701 = 18446744073709551615UL;
                    int i;
                    --l_683;
                    l_682 |= (p_72 ^ (l_686 == ((safe_mul_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(0x73L, 0x32L)), (l_339[2][6][3] = ((((((safe_sub_func_int8_t_s_s((((safe_mod_func_int8_t_s_s(p_69, (safe_div_func_int8_t_s_s((safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((((0x1142L ^ 0x9F83L) == (-3L)) < (((p_69 < p_69) , p_72) & 1L)) | l_504), l_701)), p_72)), l_549)))) & 0xBDC81E3D2028AD5CLL) > p_72), l_669)) > l_681) & 255UL) <= p_72) ^ g_137) && l_604[6])))) , &l_527)));
                    for (l_682 = 0; (l_682 <= 2); l_682 += 1)
                    { /* block id: 279 */
                        int i;
                        l_651 = p_69;
                    }
                    for (l_664 = 2; (l_664 >= 0); l_664 -= 1)
                    { /* block id: 284 */
                        int64_t l_702 = 0L;
                        int32_t l_709 = (-5L);
                        l_371 |= (((l_702 = (g_159 ^ 7UL)) | (g_159 , l_681)) & ((((safe_mod_func_uint16_t_u_u((l_705[0] , (((((safe_lshift_func_uint8_t_u_s(p_72, ((((l_708 != (void*)0) , (g_136 = (((p_72 > l_339[1][1][3]) < p_72) || 0xDA36EB4BEAC0CD4DLL))) && g_319[6]) <= p_72))) != l_680) < 0xBD66L) | p_69) <= p_72)), l_709)) , (-6L)) < p_69) == l_709));
                        g_165 = l_710;
                        (*l_710) = 1L;
                    }
                }
                (*l_711) = &g_85;
            }
            (*l_712) = &l_651;
            if (l_504)
                goto lbl_713;
        }
    }
    else
    { /* block id: 297 */
        uint16_t l_714[10][7] = {{0x0C83L,1UL,0x0C83L,1UL,1UL,8UL,1UL},{1UL,0x7CFDL,0x9AF4L,0x3B63L,65535UL,1UL,65534UL},{3UL,0xD4E3L,1UL,0x0C83L,65534UL,8UL,1UL},{0x3312L,65535UL,0x7672L,65535UL,0x7672L,65535UL,0x3312L},{0x3312L,1UL,4UL,0xD4E3L,65535UL,5UL,0x7672L},{3UL,65535UL,0x0C83L,1UL,1UL,0x9EC7L,65535UL},{1UL,65534UL,4UL,65529UL,0x44F0L,1UL,0x44F0L},{0x0C83L,0x7672L,0x7672L,0x0C83L,0x44F0L,1UL,0xD4E3L},{0x57CEL,65535UL,1UL,0x9AF4L,1UL,65534UL,0x3312L},{65534UL,1UL,0x9AF4L,1UL,65535UL,0x57CEL,0xD4E3L}};
        int i, j;
        return l_714[4][0];
    }
lbl_933:
    (*l_720) ^= (safe_div_func_int16_t_s_s(((*p_70) = (safe_mod_func_int64_t_s_s(g_137, (-1L)))), 65535UL));
    if (((safe_sub_func_uint64_t_u_u(((safe_mul_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_u(((*l_727) = p_69), 7)) , (((safe_mul_func_int16_t_s_s(0xE23FL, ((*p_70) = (*p_70)))) , (g_145[4] <= (safe_rshift_func_int16_t_s_u((safe_unary_minus_func_uint8_t_u((((((safe_mul_func_int16_t_s_s((safe_add_func_int16_t_s_s(((safe_sub_func_int64_t_s_s((((g_136 = 18446744073709551615UL) <= (((*l_739)++) | p_72)) || (*l_720)), (safe_sub_func_int16_t_s_s((safe_mod_func_int32_t_s_s((safe_sub_func_uint32_t_u_u(4294967288UL, (~(*l_720)))), 4294967295UL)), g_145[4])))) , 0xD016L), 0L)), (*l_720))) == g_145[4]) >= 248UL) | g_159) && (-1L)))), p_72)))) , p_72)), 1L)) & 0xCB985AA0L), g_145[4])) <= p_72))
    { /* block id: 306 */
        uint64_t l_749 = 18446744073709551615UL;
        int16_t ****l_824 = &g_184[3];
        const int32_t *l_831 = &g_92;
        int16_t **l_840 = &l_337[1];
        float * const * const l_841 = &l_348;
        uint8_t **l_852 = &l_739;
        uint16_t **l_869 = &g_296;
        int32_t *** const *l_871 = (void*)0;
        uint8_t l_906 = 1UL;
        int32_t l_928 = 0x051A5C64L;
        int32_t l_929[9] = {0xCB790C08L,0xCB790C08L,0x69B795ACL,0xCB790C08L,0xCB790C08L,0x69B795ACL,0xCB790C08L,0xCB790C08L,0x69B795ACL};
        int32_t *l_936 = &l_371;
        int16_t *****l_1015 = (void*)0;
        uint8_t ****l_1029[7];
        uint16_t l_1031 = 65528UL;
        uint64_t l_1162 = 0x11CC11284A981DC4LL;
        float l_1206[4];
        uint64_t l_1224 = 0x246D887B2949B4DALL;
        int i;
        for (i = 0; i < 7; i++)
            l_1029[i] = &l_1013;
        for (i = 0; i < 4; i++)
            l_1206[i] = 0xF.B089C4p-91;
        if ((l_749 , 0L))
        { /* block id: 307 */
            int32_t **l_750 = &l_719;
            uint16_t ***l_766 = &g_763;
            int8_t *l_767 = &g_350[2][3][2];
            uint32_t *l_811 = &g_204[2];
            int32_t ***l_873 = &l_750;
            int32_t *** const *l_872 = &l_873;
            const uint8_t *** const l_885[10][7] = {{&l_599[2],&l_599[2],&l_599[3],&l_599[2],&l_599[3],&l_599[2],&l_599[1]},{&l_599[1],&l_599[2],&l_599[1],&l_599[2],&l_599[1],&l_599[2],&l_599[1]},{&l_599[2],&l_599[2],&l_599[2],&l_599[1],&l_599[1],&l_599[1],&l_599[2]},{&l_599[2],&l_599[2],&l_599[2],&l_599[2],&l_599[2],&l_599[3],&l_599[2]},{&l_599[3],&l_599[2],&l_599[0],&l_599[3],&l_599[2],(void*)0,&l_599[2]},{(void*)0,&l_599[2],&l_599[2],&l_599[1],&l_599[2],&l_599[2],&l_599[0]},{&l_599[2],(void*)0,&l_599[3],&l_599[1],&l_599[1],&l_599[3],(void*)0},{&l_599[2],&l_599[0],&l_599[2],&l_599[2],&l_599[1],&l_599[2],&l_599[2]},{(void*)0,&l_599[2],(void*)0,&l_599[2],&l_599[3],&l_599[0],&l_599[2]},{&l_599[3],&l_599[2],&l_599[3],&l_599[2],&l_599[2],&l_599[2],&l_599[2]}};
            int32_t l_924 = 0xC4A24A0CL;
            int32_t l_925 = (-9L);
            int32_t l_927 = (-7L);
            int32_t * const *l_948 = &l_936;
            int32_t * const **l_947 = &l_948;
            uint32_t l_972 = 3UL;
            int32_t l_996 = 0x5BAFE5CEL;
            int32_t l_997 = (-1L);
            int32_t l_998[1][5][6] = {{{0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L},{0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L},{0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L},{0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L},{0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L,0xB2A7D011L}}};
            int i, j, k;
            (*l_750) = (void*)0;
            for (p_72 = 0; (p_72 != 15); p_72 = safe_add_func_uint16_t_u_u(p_72, 5))
            { /* block id: 311 */
                uint64_t *l_755 = &g_136;
                const int32_t l_758 = 0x2AFD43E0L;
                uint16_t ****l_764 = (void*)0;
                uint16_t ****l_765 = &g_762;
                int8_t *l_768 = &g_769;
                int32_t l_812 = (-1L);
                int16_t ***l_900 = &g_185;
                uint8_t l_907 = 0xFCL;
                if (((safe_div_func_int8_t_s_s((-7L), ((((*l_755) = 0x587A1C313AE0275BLL) != (safe_sub_func_uint16_t_u_u(l_758, (*p_70)))) ^ (((~((safe_mod_func_int8_t_s_s(((*l_768) |= ((*l_767) = (1UL == (((((*l_765) = g_762) == l_766) | (((*g_413) = l_767) != l_739)) != 0UL)))), 9L)) || l_758)) | l_758) , p_72)))) >= (*p_70)))
                { /* block id: 317 */
                    int64_t *l_789 = &g_595;
                    const int32_t l_792 = 0L;
                    int64_t *l_793 = (void*)0;
                    int64_t *l_794[1][10][4] = {{{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795},{&g_795,&g_795,&g_795,&g_795}}};
                    float **l_842[9] = {&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348};
                    int i, j, k;
                    (*l_750) = ((((safe_mod_func_uint64_t_u_u((safe_sub_func_int16_t_s_s(((~0xA7L) & 0x1A77ABCE0D16FD0BLL), (0x4F02EAF1L < ((((safe_div_func_float_f_f(((p_69 = (safe_add_func_int64_t_s_s((safe_div_func_uint32_t_u_u(0x2C66DC4CL, g_204[3])), ((((((safe_add_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(p_72, (safe_sub_func_uint32_t_u_u((((((safe_sub_func_int64_t_s_s(((*l_789) |= g_769), (&p_70 == ((safe_add_func_int16_t_s_s(((*p_70) = (*p_70)), 0x8AA3L)) , &p_71)))) >= l_792) , g_147) & l_758) == 0xE79B7AF8L), (*l_720))))), g_204[3])) & 0x9CL) && p_72) , g_98) | g_145[3]) <= l_758)))) , p_72), 0x5.5p-1)) , &g_204[5]) == (void*)0) > (*l_720))))), 0x807618E62DF6B9FBLL)) , 0x1164L) && l_749) , &g_92);
                    for (g_73 = 0; (g_73 <= 3); g_73 += 1)
                    { /* block id: 324 */
                        int16_t ***l_802[3];
                        int32_t l_813 = (-1L);
                        int32_t **l_815[6][10][4] = {{{&g_165,&g_165,&l_719,&g_60[2]},{&g_60[0],&g_60[2],&g_60[1],&g_60[1]},{&g_60[0],&g_60[0],&l_719,&l_720},{&g_165,&g_60[1],&g_60[0],&g_165},{&g_60[2],(void*)0,&g_60[2],&g_60[0]},{&l_719,(void*)0,&l_719,&g_165},{(void*)0,&g_60[1],&g_165,&l_720},{&l_720,&g_60[0],&g_60[2],&g_60[1]},{&g_60[2],&g_60[2],&g_60[2],&g_60[2]},{&l_720,&g_165,&g_165,&l_719}},{{(void*)0,&g_60[0],&l_719,&l_719},{&l_719,&l_719,&g_60[2],&l_719},{&g_60[2],&g_60[0],&g_60[0],&l_719},{&g_165,&g_165,&l_719,&g_60[2]},{&g_60[0],&g_60[2],&g_60[1],&g_60[1]},{&g_60[0],&g_60[0],&l_719,&l_720},{&g_165,&g_60[1],&g_60[0],&g_165},{&g_60[2],(void*)0,&g_60[2],&g_60[0]},{&l_719,(void*)0,&l_719,&g_165},{(void*)0,&g_60[1],&g_165,&l_720}},{{&l_720,&g_60[0],&g_60[2],&g_60[1]},{&g_60[2],&g_60[2],&g_60[2],&g_60[2]},{&l_720,&g_165,&g_165,&l_719},{(void*)0,&g_60[0],&l_719,&l_719},{&l_719,&l_719,&g_60[2],&l_719},{&g_60[2],&g_60[0],&g_60[0],&l_719},{&g_165,&g_165,&l_719,&g_60[2]},{&g_60[0],&g_60[2],&g_60[1],&g_60[1]},{&g_60[0],&g_60[0],&l_719,&l_720},{&g_165,&g_60[1],&g_60[0],&g_165}},{{&g_60[2],(void*)0,&g_60[2],&g_60[0]},{&l_719,(void*)0,&l_719,&g_165},{(void*)0,&g_60[1],&g_165,&l_720},{&l_720,&g_60[0],&g_60[2],&g_60[1]},{&g_60[2],&g_60[2],&g_60[2],&g_60[2]},{&l_720,&g_60[0],&g_60[2],&g_60[1]},{&l_720,&g_165,&g_60[1],(void*)0},{&g_60[1],(void*)0,&g_165,(void*)0},{&g_60[2],&g_165,&l_719,&g_60[1]},{&l_719,&g_60[0],(void*)0,&g_60[0]}},{{&l_719,&g_60[2],&g_60[2],&g_60[2]},{&l_719,&l_719,(void*)0,&g_165},{&l_719,&g_60[2],&l_719,&g_60[0]},{&g_60[2],&l_720,&g_165,&l_719},{&g_60[1],&l_720,&g_60[1],&g_60[0]},{&l_720,&g_60[2],&g_60[2],&g_165},{&g_165,&l_719,&g_60[2],&g_60[2]},{&g_60[0],&g_60[2],&g_60[2],&g_60[0]},{&g_165,&g_60[0],&g_60[2],&g_60[1]},{&l_720,&g_165,&g_60[1],(void*)0}},{{&g_60[1],(void*)0,&g_165,(void*)0},{&g_60[2],&g_165,&l_719,&g_60[1]},{&l_719,&g_60[0],(void*)0,&g_60[0]},{&l_719,&g_60[2],&g_60[2],&g_60[2]},{&l_719,&l_719,(void*)0,&g_165},{&l_719,&g_60[2],&l_719,&g_60[0]},{&g_60[2],&l_720,&g_165,&l_719},{&g_60[1],&l_720,&g_60[1],&g_60[0]},{&l_720,&g_60[2],&g_60[2],&g_165},{&g_165,&l_719,&g_60[2],&g_60[2]}}};
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_802[i] = &g_185;
                        l_339[2][6][3] ^= (((*p_70) > (((((**l_750) ^= 4294967294UL) & p_69) == (safe_rshift_func_uint8_t_u_u((((((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((((*l_755) = g_350[6][0][2]) < ((l_802[1] == (void*)0) | ((*l_739) = 255UL))), (safe_add_func_int32_t_s_s(((*l_720) = 0xB514DCF4L), (safe_div_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u((l_812 = ((safe_mul_func_int16_t_s_s(((&g_204[3] == l_811) <= l_792), l_749)) , 0xAFB3L)), (*p_70))) <= 255UL), l_749)))))), l_813)) | l_749) >= 0x597EF3B9F8A1C4C7LL) && l_813) ^ l_758), p_72))) < g_795)) > l_749);
                        if (l_813)
                            continue;
                        (*l_720) |= ((g_814 = &g_60[0]) != l_815[5][8][2]);
                        (*l_719) |= p_69;
                    }
                    (**l_750) = (safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u(((p_69 , (safe_lshift_func_uint8_t_u_u(((*l_719) & (safe_sub_func_int32_t_s_s((l_824 == (((((!(safe_add_func_int8_t_s_s((safe_unary_minus_func_int8_t_s(p_69)), (safe_rshift_func_uint16_t_u_u(((l_831 == ((safe_add_func_float_f_f(((((safe_sub_func_float_f_f((((safe_div_func_float_f_f(((safe_div_func_float_f_f(p_69, (g_350[0][1][2] <= (l_812 = ((*l_348) = (*l_831)))))) != g_137), p_69)) != g_795) <= g_595), (*l_831))) , (void*)0) == l_840) < g_350[1][1][2]), l_758)) , l_831)) & g_136), p_69))))) , l_841) != l_842[5]) , g_89) , (void*)0)), l_758))), p_72))) != (*l_720)), (**l_750))), p_69));
                    if (p_72)
                        continue;
                }
                else
                { /* block id: 340 */
                    return (*l_720);
                }
                for (l_549 = 15; (l_549 > 51); l_549 = safe_add_func_uint8_t_u_u(l_549, 2))
                { /* block id: 345 */
                    int32_t *** const *l_903[6][10] = {{&l_873,&l_873,(void*)0,&l_873,(void*)0,(void*)0,&l_873,(void*)0,&l_873,&l_873},{(void*)0,&l_873,(void*)0,&l_873,&l_873,(void*)0,&l_873,(void*)0,(void*)0,&l_873},{(void*)0,&l_873,&l_873,&l_873,&l_873,(void*)0,(void*)0,(void*)0,(void*)0,&l_873},{&l_873,&l_873,&l_873,&l_873,(void*)0,(void*)0,(void*)0,(void*)0,&l_873,&l_873},{&l_873,&l_873,(void*)0,&l_873,(void*)0,(void*)0,&l_873,(void*)0,&l_873,&l_873},{(void*)0,&l_873,(void*)0,&l_873,&l_873,(void*)0,&l_873,(void*)0,(void*)0,&l_873}};
                    int i, j;
                }
                (*g_521) = (*g_521);
            }
            for (g_89 = 0; (g_89 >= (-20)); g_89 = safe_sub_func_int64_t_s_s(g_89, 7))
            { /* block id: 379 */
                uint32_t l_930 = 18446744073709551615UL;
                int32_t l_977 = 0L;
                int32_t l_991 = 1L;
                int32_t l_992 = (-8L);
                int32_t l_993 = 1L;
                int32_t l_994 = 0xC48B2BECL;
                int32_t l_995 = (-6L);
                int32_t l_999 = 6L;
                int32_t l_1000 = 0L;
                int32_t l_1002 = 0xF11735C5L;
                int32_t l_1003 = 0x0CACDEABL;
                for (l_749 = 0; (l_749 != 39); ++l_749)
                { /* block id: 382 */
                    int8_t l_917 = (-3L);
                    int32_t *l_918 = &l_371;
                    int32_t *l_919 = (void*)0;
                    int32_t *l_920 = &l_339[2][6][3];
                    int32_t *l_921 = &g_550;
                    int32_t *l_922 = &l_371;
                    int32_t *l_923[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    --l_930;
                    (**l_873) = &g_926;
                    (*l_750) = (*l_750);
                    if (l_504)
                        goto lbl_933;
                }
                if ((safe_rshift_func_uint16_t_u_u(p_69, 9)))
                { /* block id: 388 */
                    uint32_t *l_939 = &g_940;
                    uint8_t **l_946 = &l_739;
                    int32_t * const **l_949 = (void*)0;
                    int32_t l_950 = 0xD0819EE2L;
                    int64_t l_951 = 3L;
                    (**l_948) ^= (4294967286UL || (l_951 = ((((**l_840) = ((((((*l_750) = (***l_872)) == l_936) | (g_319[1] , (((((((g_319[1] , (safe_div_func_int32_t_s_s((((*l_939) = ((*l_811) |= (6L == 9L))) <= ((safe_lshift_func_int8_t_s_u((safe_div_func_uint16_t_u_u((((l_949 = ((!((void*)0 != l_946)) , l_947)) == (void*)0) , 0x3B97L), p_72)), 5)) & p_69)), l_930))) ^ l_930) ^ l_930) ^ l_950) < 0x5CF9L) | 0x7AL) >= (*l_720)))) , 5L) >= (*l_720))) || (*p_70)) == (*l_720))));
                    (***l_872) = &l_339[1][7][3];
                    if (p_69)
                    { /* block id: 397 */
                        return p_69;
                    }
                    else
                    { /* block id: 399 */
                        int64_t *l_952[3];
                        int32_t l_971 = 1L;
                        int32_t *l_973 = &l_336;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_952[i] = &g_595;
                        (*l_348) = p_72;
                        (*l_973) &= (g_940 == ((((***l_947) = 0x3600FE3A836A2CBELL) >= ((((((l_971 ^= (safe_rshift_func_uint8_t_u_u((((**l_840) = ((safe_sub_func_int8_t_s_s((~((*g_413) == (*g_413))), ((((**l_750) <= ((safe_lshift_func_int8_t_s_u(p_69, (p_72 >= (safe_sub_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((*l_739)--), (safe_mod_func_int32_t_s_s(((&g_762 == ((!0x153EL) , &l_766)) != p_72), g_595)))), (*l_720))), (-2L)))))) == p_72)) | (****l_872)) || 0x3CL))) & p_72)) <= 0x255CL), (*l_719)))) <= p_72) <= (***l_873)) != l_930) , 0x1B30EC50000620D4LL) & (-9L))) && l_972));
                        (***l_872) = &l_371;
                    }
                }
                else
                { /* block id: 408 */
                    uint16_t * const *l_975 = (void*)0;
                    uint16_t * const **l_976 = &l_975;
                    int32_t *l_978 = &l_371;
                    int32_t *l_979 = &l_928;
                    int32_t *l_980 = &g_926;
                    int32_t *l_981 = &l_925;
                    int32_t *l_982 = &g_926;
                    int32_t *l_983 = &l_924;
                    int32_t *l_984 = &l_929[3];
                    int32_t *l_985 = &l_336;
                    int32_t *l_986 = &l_924;
                    int32_t *l_987 = (void*)0;
                    int32_t *l_988 = &l_336;
                    int32_t *l_989 = (void*)0;
                    int32_t *l_990[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_990[i] = &l_371;
                    if (l_749)
                        goto lbl_974;
                    (*l_976) = l_975;
                    l_977 ^= (g_204[5] <= (0x8DL >= (1L <= p_69)));
                    l_1004++;
                }
                if (p_69)
                    continue;
                (***l_947) = 1L;
            }
        }
        else
        { /* block id: 417 */
            uint8_t ***l_1012[6] = {&l_852,&l_852,(void*)0,&l_852,&l_852,(void*)0};
            uint8_t ****l_1011 = &l_1012[2];
            int32_t l_1019 = 0xC663A7DFL;
            int32_t l_1022 = (-4L);
            int i;
            (*l_720) = (safe_div_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s(((l_1013 = ((*l_1011) = (void*)0)) == (p_72 , &l_852)), ((l_1015 = l_335) == ((((*l_727)--) ^ ((+(l_1019 |= (0x4BEA0089L && p_72))) != (safe_lshift_func_uint16_t_u_s(((-1L) < 0x27B7C6FF36525174LL), l_1022)))) , (void*)0)))), (*l_831)));
        }
        for (l_504 = 0; (l_504 <= 4); l_504 += 1)
        { /* block id: 427 */
            uint8_t ****l_1030 = (void*)0;
            int32_t l_1037 = (-1L);
            uint32_t *l_1038 = (void*)0;
            int16_t * const *l_1084[3];
            int16_t * const **l_1083 = &l_1084[1];
            int16_t ***l_1085 = &l_840;
            uint8_t l_1088 = 0x75L;
            int8_t l_1089 = 0xB5L;
            uint16_t **l_1117 = &g_296;
            int32_t l_1144 = 0L;
            uint64_t l_1172 = 0x52AD3801EAC33C10LL;
            int32_t ****l_1193 = (void*)0;
            float l_1220 = (-0x3.1p-1);
            int32_t l_1221 = 0xA0CAF03EL;
            int32_t l_1222[1];
            int i;
            for (i = 0; i < 3; i++)
                l_1084[i] = (void*)0;
            for (i = 0; i < 1; i++)
                l_1222[i] = 0x1D197453L;
        }
    }
    else
    { /* block id: 559 */
        const int64_t *l_1250[5][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_595,&g_595,&g_595,&g_595,&g_595},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_595,&g_595,&g_595,&g_595,&g_595},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        const int64_t **l_1249 = &l_1250[4][0];
        const int64_t ***l_1251 = &l_1249;
        uint16_t *l_1260 = &l_1192;
        int32_t l_1266 = 0xFD954EB9L;
        int32_t l_1273 = (-1L);
        int i, j;
        (*l_1251) = l_1249;
        (*l_720) ^= (safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mod_func_int8_t_s_s((0x65L >= (safe_lshift_func_int16_t_s_u(0xF4C7L, 14))), ((((g_769 | 0xC46C9BC3E7AA8AF2LL) , l_1260) == l_1260) & (safe_mod_func_uint32_t_u_u(((((safe_unary_minus_func_int8_t_s(0xF5L)) ^ (p_69 , (safe_div_func_uint16_t_u_u(((p_69 >= 7L) | l_1266), p_72)))) ^ 0x95BDB6C141A43F5FLL) & 255UL), l_1266))))) == 1UL), 4)), g_159));
        l_1273 ^= (safe_mod_func_uint32_t_u_u(g_159, (safe_mod_func_int64_t_s_s((*l_720), (safe_add_func_uint64_t_u_u(((void*)0 != l_348), l_1266))))));
    }
    for (g_1058 = 0; (g_1058 <= 29); ++g_1058)
    { /* block id: 566 */
        float l_1280 = (-0x1.2p+1);
        int32_t l_1281[2][8] = {{1L,(-1L),(-1L),1L,(-1L),(-1L),1L,(-1L)},{1L,1L,1L,1L,1L,1L,1L,1L}};
        int32_t *l_1283 = &g_98;
        uint8_t *l_1301 = &g_319[7];
        int32_t *l_1318[1];
        int16_t l_1319[4] = {0x7C04L,0x7C04L,0x7C04L,0x7C04L};
        int16_t l_1320[10] = {0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL,0xF5FAL};
        int i, j;
        for (i = 0; i < 1; i++)
            l_1318[i] = &l_339[2][6][3];
        l_339[0][3][1] &= ((safe_rshift_func_uint16_t_u_s((0x89L ^ (safe_mul_func_int16_t_s_s(l_1281[0][4], ((((((*l_720) >= ((~p_72) , ((*l_1283) |= (*l_720)))) > (safe_rshift_func_uint8_t_u_s(l_1286, p_72))) != 0x741F59B8L) == (*l_720)) == p_72)))), (*p_70))) != g_136);
        for (l_504 = 12; (l_504 <= 51); l_504 = safe_add_func_uint64_t_u_u(l_504, 1))
        { /* block id: 571 */
            uint64_t *l_1291 = (void*)0;
            uint64_t *l_1292 = &g_136;
            int32_t l_1299 = 0x1D68B363L;
            uint8_t *l_1300[7][4][6] = {{{(void*)0,&g_319[1],&g_147,&g_147,&g_145[3],&g_145[4]},{(void*)0,&g_319[6],(void*)0,&g_147,&g_145[3],&g_147},{&g_145[2],&g_145[3],(void*)0,&g_145[2],&g_145[4],&g_147},{&g_319[1],&g_145[3],&g_319[4],&g_319[6],&g_319[6],&g_319[4]}},{{&g_319[4],&g_319[4],&g_145[4],&g_145[4],&g_147,&g_145[4]},{&g_145[2],(void*)0,(void*)0,&g_319[1],&g_147,&g_145[4]},{&g_319[4],&g_145[2],(void*)0,&g_145[2],&g_319[4],&g_145[4]},{&g_145[2],&g_145[2],&g_145[4],&g_145[3],&g_145[2],&g_319[4]}},{{&g_145[3],&g_145[2],&g_319[4],&g_145[1],(void*)0,&g_147},{&g_319[6],&g_145[4],(void*)0,&g_147,(void*)0,&g_147},{(void*)0,&g_319[7],(void*)0,&g_145[3],&g_145[2],&g_145[4]},{&g_319[1],&g_145[4],&g_147,(void*)0,&g_145[4],(void*)0}},{{(void*)0,&g_145[4],&g_145[2],(void*)0,&g_319[4],&g_145[3]},{&g_319[1],(void*)0,&g_147,&g_145[3],&g_145[2],&g_145[3]},{(void*)0,&g_147,&g_145[3],&g_147,&g_145[0],&g_145[4]},{&g_319[6],&g_145[2],(void*)0,&g_145[1],&g_145[2],&g_145[2]}},{{&g_145[3],(void*)0,(void*)0,&g_145[3],&g_145[1],(void*)0},{&g_145[2],&g_147,&g_145[2],&g_145[2],&g_319[7],&g_145[3]},{&g_319[4],&g_145[3],&g_147,&g_319[1],&g_319[7],&g_319[1]},{&g_145[2],&g_147,&g_145[0],&g_145[4],&g_145[1],&g_319[1]}},{{&g_319[4],(void*)0,&g_319[6],&g_319[6],&g_145[2],&g_145[2]},{&g_319[1],&g_145[2],&g_147,&g_145[2],&g_145[0],(void*)0},{&g_145[2],&g_147,&g_319[1],&g_147,&g_145[2],&g_145[3]},{(void*)0,(void*)0,&g_319[6],&g_147,&g_319[4],&g_147}},{{(void*)0,&g_145[4],&g_145[4],(void*)0,&g_145[4],&g_147},{(void*)0,&g_145[4],&g_319[6],&g_145[4],&g_145[2],&g_145[3]},{&g_145[4],&g_145[3],(void*)0,&g_145[4],&g_145[4],&g_319[4]},{(void*)0,&g_145[4],&g_319[6],(void*)0,&g_147,(void*)0}}};
            int32_t l_1314 = (-1L);
            int32_t l_1317 = (-10L);
            int i, j, k;
            if (g_147)
                goto lbl_933;
            (*l_1283) = ((*l_720) , (safe_sub_func_uint64_t_u_u((++(*l_1292)), g_1041)));
            (*l_720) = ((safe_div_func_int16_t_s_s((((((safe_lshift_func_int8_t_s_u((-7L), 5)) && (l_1299 = 0xC7L)) , l_1300[0][0][5]) != l_1301) ^ ((0xC6A4C864L || (safe_sub_func_int32_t_s_s(((p_72 , ((safe_rshift_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(((g_204[3] != (l_1314 = (*l_720))) ^ ((*l_1301) = ((((safe_add_func_uint8_t_u_u(p_69, (*l_1283))) , (*l_720)) > 246UL) , (*l_1283)))), 2)), g_926)), (*l_1283))) & 254UL), l_1317)) < 0xCFBE1339C8F71F7CLL), p_72)) < 0xFC7D92AFL)) <= 0x89L), p_72))) , p_72)), (*l_1283))) || 4294967293UL);
        }
        --l_1321;
    }
    return g_1324;
}


/* ------------------------------------------ */
/* 
 * reads : g_85 g_89 g_73 g_136 g_137 g_159 g_98 g_92 g_60 g_165 g_145 g_204 g_147 g_319
 * writes: g_85 g_89 g_92 g_133 g_137 g_145 g_147 g_159 g_165 g_184 g_204 g_98 g_296
 */
static int16_t * func_74(int16_t * p_75)
{ /* block id: 24 */
    int8_t l_99[5][3][3] = {{{0L,0xBAL,(-6L)},{0x84L,0L,(-1L)},{1L,1L,0xA3L}},{{0x84L,0xA3L,0L},{0L,0xA3L,0x84L},{0xA3L,1L,1L}},{{(-1L),0L,0x84L},{(-6L),0xBAL,0L},{(-6L),0xFFL,0xA3L}},{{(-1L),0xECL,(-1L)},{0xA3L,0xFFL,(-6L)},{0L,0xBAL,(-6L)}},{{0x84L,0L,(-1L)},{1L,1L,0xA3L},{0x84L,0xA3L,0L}}};
    int32_t l_108[6][7] = {{0x32CF88EAL,0x32CF88EAL,0xDD310FEDL,(-5L),0xDD310FEDL,0x32CF88EAL,0x32CF88EAL},{0x32CF88EAL,0xDD310FEDL,(-5L),0xDD310FEDL,0x32CF88EAL,0x32CF88EAL,0xDD310FEDL},{(-10L),0L,(-10L),0xDD310FEDL,0xDD310FEDL,(-10L),0L},{0xDD310FEDL,0L,(-5L),(-5L),0L,0xDD310FEDL,0L},{(-10L),0xDD310FEDL,0xDD310FEDL,(-10L),0L,(-10L),0xDD310FEDL},{0x32CF88EAL,0x32CF88EAL,0xDD310FEDL,(-5L),0xDD310FEDL,0x32CF88EAL,0x32CF88EAL}};
    int32_t l_163 = 0x0C890823L;
    int16_t ****l_218 = &g_184[7];
    uint16_t *l_238 = &g_159;
    uint8_t *l_260[1];
    float *l_287 = &g_133;
    int16_t *l_322 = &g_137;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_260[i] = &g_145[2];
    for (g_85 = 0; (g_85 >= (-1)); g_85 = safe_sub_func_uint8_t_u_u(g_85, 1))
    { /* block id: 27 */
        uint64_t l_109 = 2UL;
        const int8_t *l_205[5][6] = {{(void*)0,&l_99[0][1][1],&l_99[4][0][1],(void*)0,&l_99[4][0][1],(void*)0},{(void*)0,&l_99[3][2][1],(void*)0,&l_99[4][1][1],&l_99[4][0][1],&l_99[0][1][1]},{&l_99[4][0][1],&l_99[0][1][1],(void*)0,(void*)0,(void*)0,(void*)0},{&l_99[0][1][1],&l_99[0][1][1],&l_99[1][2][1],(void*)0,&l_99[0][1][1],&l_99[4][1][1]},{&l_99[4][0][1],&l_99[1][2][1],&l_99[0][1][1],&l_99[4][1][1],&l_99[0][1][1],&l_99[1][2][1]}};
        int32_t l_222 = (-2L);
        int32_t **l_279 = (void*)0;
        int32_t ***l_278 = &l_279;
        int32_t *l_320 = &l_163;
        int32_t **l_321 = &l_320;
        int i, j;
        for (g_89 = 0; (g_89 > 3); ++g_89)
        { /* block id: 30 */
            float l_107 = 0x3.897E00p+79;
            int32_t *l_112 = &l_108[3][0];
            int32_t **l_122[2];
            int32_t *** const l_121 = &l_122[1];
            int16_t **l_175[6][6] = {{&g_30[0],(void*)0,(void*)0,(void*)0,&g_30[0],&g_30[0]},{&g_30[0],(void*)0,(void*)0,&g_30[0],&g_30[0],&g_30[0]},{&g_30[0],&g_30[0],&g_30[0],(void*)0,(void*)0,&g_30[0]},{&g_30[0],&g_30[0],(void*)0,(void*)0,(void*)0,&g_30[0]},{(void*)0,&g_30[0],(void*)0,(void*)0,&g_30[0],(void*)0},{&g_30[0],(void*)0,(void*)0,(void*)0,&g_30[0],&g_30[0]}};
            int16_t ***l_174 = &l_175[5][1];
            int16_t *l_176 = (void*)0;
            int16_t *l_177 = (void*)0;
            int16_t *l_178 = &g_137;
            int16_t ***l_188 = &l_175[5][1];
            int8_t *l_189[2];
            int32_t l_210 = 0xCA076940L;
            uint64_t l_214 = 0xB2CF5B4EFAD5DA70LL;
            const int32_t l_245[8][2][2] = {{{1L,0x4A37D6F5L},{1L,1L}},{{0x4A37D6F5L,1L},{1L,0x4A37D6F5L}},{{1L,1L},{0x4A37D6F5L,1L}},{{1L,0x4A37D6F5L},{1L,1L}},{{0x4A37D6F5L,1L},{1L,0x4A37D6F5L}},{{1L,1L},{0x4A37D6F5L,1L}},{{1L,0x4A37D6F5L},{1L,0x4A37D6F5L}},{{0x5CE17B37L,0x4A37D6F5L},{0x4A37D6F5L,0x5CE17B37L}}};
            const int16_t l_248[10][3] = {{0x54AAL,0x8D65L,0x8D65L},{0x9AA8L,1L,0x9AA8L},{0x54AAL,0x54AAL,0x8D65L},{(-1L),1L,(-1L)},{0x54AAL,0x8D65L,0x8D65L},{0x9AA8L,1L,0x9AA8L},{0x54AAL,0x54AAL,0x8D65L},{(-1L),1L,(-1L)},{0x54AAL,0x8D65L,0x8D65L},{0x9AA8L,1L,0x9AA8L}};
            uint16_t **l_275 = &l_238;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_122[i] = &g_60[1];
            for (i = 0; i < 2; i++)
                l_189[i] = &l_99[1][1][2];
            for (g_92 = (-4); (g_92 <= (-4)); g_92 = safe_add_func_int8_t_s_s(g_92, 7))
            { /* block id: 33 */
                int32_t ***l_123 = &l_122[1];
                uint32_t l_139 = 18446744073709551613UL;
                if (((safe_rshift_func_uint8_t_u_s(((void*)0 == &g_30[2]), 5)) && g_89))
                { /* block id: 34 */
                    int32_t *l_97 = &g_98;
                    int32_t *l_100 = &g_98;
                    int32_t *l_101 = (void*)0;
                    int32_t *l_102 = (void*)0;
                    int32_t *l_103 = &g_98;
                    int32_t *l_104 = &g_98;
                    int32_t *l_105 = &g_98;
                    int32_t *l_106[7][2][9] = {{{&g_85,&g_92,&g_92,&g_89,&g_92,&g_89,&g_85,&g_98,&g_89},{&g_92,&g_89,&g_89,(void*)0,&g_85,(void*)0,&g_89,&g_89,&g_92}},{{&g_98,&g_85,&g_89,&g_85,&g_89,&g_89,&g_92,&g_85,(void*)0},{&g_92,&g_85,&g_92,&g_85,&g_92,&g_98,&g_85,&g_85,&g_98}},{{&g_98,&g_89,&g_92,&g_89,&g_98,&g_85,&g_85,&g_85,&g_92},{&g_92,&g_98,&g_92,&g_92,&g_92,&g_89,&g_98,&g_85,&g_89}},{{&g_85,&g_92,&g_89,&g_89,&g_85,&g_85,&g_89,&g_89,&g_92},{&g_85,&g_89,&g_85,&g_89,&g_92,&g_98,&g_89,&g_98,(void*)0}},{{&g_89,(void*)0,&g_85,&g_92,&g_85,&g_89,&g_92,(void*)0,&g_92},{(void*)0,&g_89,&g_89,&g_89,&g_89,(void*)0,&g_92,&g_92,&g_92}},{{&g_85,&g_92,&g_98,&g_85,&g_85,&g_89,(void*)0,&g_89,(void*)0},{&g_85,&g_98,&g_92,&g_85,&g_92,&g_92,&g_92,&g_85,&g_92}},{{&g_89,&g_89,&g_89,(void*)0,&g_92,&g_92,&g_92,(void*)0,&g_89},{&g_92,&g_85,(void*)0,&g_89,&g_85,&g_98,&g_89,&g_92,&g_92}}};
                    int32_t **l_113 = &l_106[6][0][7];
                    int32_t ***l_114 = &l_113;
                    int i, j, k;
                    ++l_109;
                    (*l_113) = l_112;
                    (*l_114) = (void*)0;
                }
                else
                { /* block id: 38 */
                    uint32_t l_128 = 0xAB27A36CL;
                    float *l_129 = (void*)0;
                    float *l_130 = (void*)0;
                    float *l_131 = &l_107;
                    float *l_132 = &g_133;
                    int8_t *l_138 = &l_99[0][1][1];
                    int32_t l_164 = 0L;
                    if (((safe_rshift_func_uint8_t_u_u((l_109 & (safe_rshift_func_int8_t_s_s(((l_108[1][6] &= 4294967286UL) , ((*l_138) = (((l_121 == l_123) ^ ((((((*l_112) = ((*l_132) = (safe_sub_func_float_f_f((safe_div_func_float_f_f((0x0.2p-1 <= l_128), l_108[5][1])), ((*l_131) = l_109))))) >= (g_137 = (safe_mul_func_float_f_f((((0x0.571713p+28 > l_99[2][0][2]) <= g_73) , g_136), 0xE.3E4B84p+13)))) >= 0xB.40075Ap-1) , (void*)0) != p_75)) && 0xBFA59640L))), l_139))), 6)) || l_109))
                    { /* block id: 45 */
                        uint8_t *l_144 = &g_145[4];
                        uint8_t *l_146 = &g_147;
                        uint16_t *l_158 = &g_159;
                        int32_t l_162[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_162[i] = (-8L);
                        l_164 = ((safe_add_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_u(((*l_146) = ((*l_144) = l_128)), (safe_div_func_uint32_t_u_u(((((safe_mod_func_int16_t_s_s(l_108[3][6], (safe_mul_func_int16_t_s_s((4294967294UL || 0L), l_109)))) | ((l_163 = (safe_lshift_func_uint8_t_u_s((((*l_158) |= (l_128 , (safe_sub_func_int64_t_s_s(g_137, l_128)))) | ((((safe_mul_func_uint16_t_u_u(0x33A1L, l_162[2])) != 246UL) && g_98) & 0xC7178F01L)), g_92))) & l_162[2])) <= 0x79C98821L) || l_139), l_162[2])))) > l_109), (*l_112))) != g_85);
                    }
                    else
                    { /* block id: 51 */
                        g_165 = (**l_123);
                    }
                    return &g_137;
                }
            }
            if (l_99[0][1][1])
                break;
            if ((((((((*l_112) , (safe_lshift_func_uint16_t_u_u(((void*)0 != &g_30[0]), (safe_mul_func_uint16_t_u_u(g_92, (*l_112)))))) ^ ((*l_178) = (((*l_121) == (void*)0) > (l_109 <= (safe_mod_func_uint8_t_u_u(((((*l_174) = &p_75) != &p_75) , 0x2FL), l_109)))))) < 0x67FBL) ^ (*g_165)) != g_159) != 0xFCL))
            { /* block id: 60 */
                int32_t *l_179 = &g_85;
                int16_t ***l_183 = &l_175[5][1];
                int16_t ****l_186 = &l_183;
                int16_t ****l_187 = &l_174;
                uint32_t *l_203 = &g_204[3];
                float *l_206 = &l_107;
                l_179 = l_179;
                for (g_137 = 15; (g_137 > (-8)); g_137 = safe_sub_func_uint64_t_u_u(g_137, 2))
                { /* block id: 64 */
                    uint32_t l_182 = 4294967292UL;
                    g_133 = (l_182 , 0xB.935C2Cp-86);
                    return &g_137;
                }
                l_188 = ((*l_187) = ((*l_186) = (g_184[3] = l_183)));
                (*l_206) = (l_189[0] == (((-1L) && 0UL) , ((safe_add_func_uint8_t_u_u(((!0UL) != (safe_div_func_uint64_t_u_u((safe_add_func_uint8_t_u_u(((((*l_203) = ((safe_rshift_func_uint16_t_u_s(g_145[2], ((*l_179) > g_145[3]))) == (safe_add_func_uint32_t_u_u(((safe_mod_func_uint32_t_u_u(g_145[4], ((l_109 || g_145[4]) ^ g_89))) == (*l_112)), 0x7099D399L)))) > (*l_179)) | g_137), 6UL)), (*l_179)))), g_136)) , l_205[2][3])));
            }
            else
            { /* block id: 74 */
                const uint16_t l_209 = 1UL;
                int32_t l_211 = (-2L);
                int8_t *l_215 = &l_99[0][2][2];
                int16_t **l_216[9] = {&g_30[0],&g_30[0],&g_30[0],&g_30[0],&g_30[0],&g_30[0],&g_30[0],&g_30[0],&g_30[0]};
                int16_t ****l_217 = &l_188;
                uint64_t l_288[2][9][10] = {{{1UL,18446744073709551615UL,0x033F974B7E782F84LL,18446744073709551615UL,1UL,1UL,1UL,18446744073709551615UL,0x033F974B7E782F84LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL},{0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL,18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL},{0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL,18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL},{0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL,0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL},{0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL,0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL},{0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL,18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL}},{{0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL,18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL},{0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL,0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL},{0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL,0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL},{0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL,18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL},{0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,0xC69A7FB0264D0DC1LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL,0x033F974B7E782F84LL,1UL,18446744073709551615UL,1UL,0x033F974B7E782F84LL,18446744073709551615UL},{0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL,0xC69A7FB0264D0DC1LL,1UL,0x87E54C48CFC1C320LL,18446744073709551615UL,0x87E54C48CFC1C320LL,1UL}}};
                int32_t l_317 = 0xA4CD5AEAL;
                int32_t l_318 = (-1L);
                int i, j, k;
                if (((safe_mod_func_int64_t_s_s((((l_163 , (l_209 == (l_211 = (l_210 = 0x5DL)))) || g_159) < (&l_174 != (l_218 = ((((safe_lshift_func_int8_t_s_u((l_214 , (l_215 == (void*)0)), 1)) ^ (((l_216[6] == (void*)0) || 0x73355EA1L) | 4UL)) || l_108[2][1]) , l_217)))), g_89)) , l_99[0][1][1]))
                { /* block id: 78 */
                    uint16_t *l_221 = &g_159;
                    int32_t *l_224[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_224[i] = &l_222;
                    if (((safe_mod_func_uint16_t_u_u(0x52DCL, ((*l_221) = l_109))) ^ ((l_222 = l_109) & g_204[3])))
                    { /* block id: 81 */
                        uint32_t l_223[3];
                        uint16_t **l_237 = (void*)0;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_223[i] = 0xE12BB269L;
                        if (l_223[2])
                            break;
                        if (l_99[0][1][1])
                            break;
                        l_224[0] = &g_89;
                        (*g_165) |= ((safe_mul_func_uint8_t_u_u(l_99[0][1][1], (safe_mod_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s(((safe_add_func_int16_t_s_s((safe_sub_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(((l_211 , &g_159) != (l_238 = &g_159)), (~(safe_sub_func_int64_t_s_s((g_204[1] | (safe_mul_func_uint16_t_u_u(l_209, (l_222 = 0xA050L)))), ((+(l_245[4][0][1] , (safe_sub_func_int16_t_s_s((*l_112), l_109)))) > 1L)))))), l_211)), l_248[7][2])) > l_99[1][2][1]), l_109)) | l_223[2]), 0x1B2CL)))) & g_145[3]);
                    }
                    else
                    { /* block id: 88 */
                        int16_t *****l_249 = &l_217;
                        int32_t l_250 = 1L;
                        (*l_249) = (void*)0;
                        (*l_112) = (-3L);
                        if (l_250)
                            break;
                    }
                    l_224[0] = (void*)0;
                }
                else
                { /* block id: 94 */
                    int32_t l_257 = 8L;
                    uint8_t **l_261 = &l_260[0];
                    float *l_262 = &g_133;
                    float *l_267 = &l_107;
                    l_211 = ((safe_add_func_float_f_f(g_137, (safe_sub_func_float_f_f(l_163, ((safe_div_func_float_f_f(l_257, ((((safe_mul_func_float_f_f((((*l_261) = l_260[0]) != &g_145[3]), ((*l_262) = l_108[3][6]))) >= (0x5.2AD6DEp+14 != (safe_add_func_float_f_f((safe_div_func_float_f_f(g_159, ((*l_267) = l_209))), l_211)))) >= l_209) <= 0x6.890561p-57))) == g_98))))) == 0x9.6F6E79p-99);
                }
                l_163 = (safe_rshift_func_int16_t_s_u(((void*)0 == &g_60[2]), 7));
                if ((~(safe_lshift_func_uint8_t_u_u(l_209, 0))))
                { /* block id: 101 */
                    uint16_t **l_274[5][4][3] = {{{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238}},{{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238}},{{&l_238,&l_238,&l_238},{&l_238,&l_238,&l_238},{&l_238,(void*)0,&l_238},{(void*)0,(void*)0,&l_238}},{{(void*)0,(void*)0,&l_238},{(void*)0,(void*)0,&l_238},{(void*)0,(void*)0,&l_238},{(void*)0,(void*)0,&l_238}},{{(void*)0,(void*)0,&l_238},{(void*)0,(void*)0,&l_238},{(void*)0,(void*)0,&l_238},{(void*)0,(void*)0,&l_238}}};
                    uint16_t ***l_273 = &l_274[1][0][2];
                    int32_t l_280 = (-5L);
                    int i, j, k;
                    l_211 = ((((((*l_273) = &l_238) != l_275) == (safe_div_func_float_f_f(0x4.319678p+86, ((g_145[0] , ((void*)0 == l_278)) < (((g_145[4] || l_163) , l_280) <= 0x1.2p-1))))) != g_98) <= (*l_112));
                    (*g_165) ^= ((0x0.0p-1 == (safe_add_func_float_f_f(((safe_rshift_func_uint8_t_u_s((g_147++), (l_288[0][3][4] ^= ((void*)0 == l_287)))) , (safe_add_func_float_f_f((+(safe_add_func_float_f_f(l_163, (l_99[2][0][2] >= (((safe_sub_func_float_f_f(((*l_287) = l_163), (g_136 >= (((*l_275) = (g_296 = p_75)) == &g_159)))) > 0xD.329275p-46) >= l_99[0][1][1]))))), l_163))), (*l_112)))) , l_288[0][3][4]);
                    g_165 = &g_98;
                    l_318 = (l_163 , (safe_sub_func_int32_t_s_s((l_99[0][1][1] , (safe_div_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((safe_sub_func_uint8_t_u_u((l_211 = (l_317 &= (safe_add_func_int8_t_s_s((safe_sub_func_int64_t_s_s((safe_div_func_uint8_t_u_u(((&g_296 != (void*)0) < (((0xACFBA1E86DC7E6EELL <= (*l_112)) <= ((*l_178) |= ((safe_div_func_int8_t_s_s(((safe_sub_func_int8_t_s_s((l_287 == (void*)0), ((((safe_lshift_func_uint8_t_u_s(l_99[2][0][0], 4)) && l_99[0][1][1]) == g_98) >= l_209))) | l_280), l_280)) < g_204[4]))) && g_85)), l_288[1][7][4])), g_145[4])), l_280)))), l_108[5][4])), l_280)) || l_209), l_288[0][3][1]))), 4294967295UL)));
                }
                else
                { /* block id: 115 */
                    if (g_319[1])
                        break;
                }
            }
        }
        (*l_321) = l_320;
        if ((*g_165))
            continue;
    }
    (*g_165) = l_108[3][6];
    return l_322;
}


/* ------------------------------------------ */
/* 
 * reads : g_30
 * writes: g_85
 */
static int16_t * func_76(float  p_77, uint32_t  p_78, uint8_t  p_79, uint64_t  p_80, int16_t  p_81)
{ /* block id: 16 */
    int16_t **l_86 = &g_30[0];
    for (p_80 = 0; (p_80 <= 5); p_80 += 1)
    { /* block id: 19 */
        int32_t *l_84 = &g_85;
        int i;
        (*l_84) = (-9L);
        l_86 = &g_30[p_80];
    }
    return (*l_86);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc_bytes (&g_9, sizeof(g_9), "g_9", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_17[i], "g_17[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_19, "g_19", print_hash_value);
    transparent_crc(g_31, "g_31", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_64[i][j], "g_64[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc(g_98, "g_98", print_hash_value);
    transparent_crc_bytes (&g_133, sizeof(g_133), "g_133", print_hash_value);
    transparent_crc(g_136, "g_136", print_hash_value);
    transparent_crc(g_137, "g_137", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_145[i], "g_145[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_147, "g_147", print_hash_value);
    transparent_crc(g_159, "g_159", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_204[i], "g_204[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_319[i], "g_319[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_338, "g_338", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_350[i][j][k], "g_350[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_550, "g_550", print_hash_value);
    transparent_crc(g_595, "g_595", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_657[i], sizeof(g_657[i]), "g_657[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_769, "g_769", print_hash_value);
    transparent_crc(g_795, "g_795", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_884[i], "g_884[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_926, "g_926", print_hash_value);
    transparent_crc(g_940, "g_940", print_hash_value);
    transparent_crc(g_1041, "g_1041", print_hash_value);
    transparent_crc(g_1058, "g_1058", print_hash_value);
    transparent_crc(g_1324, "g_1324", print_hash_value);
    transparent_crc(g_1326, "g_1326", print_hash_value);
    transparent_crc(g_1340, "g_1340", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_1369[i], sizeof(g_1369[i]), "g_1369[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1481, "g_1481", print_hash_value);
    transparent_crc(g_1558, "g_1558", print_hash_value);
    transparent_crc(g_1570, "g_1570", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1584[i][j][k], "g_1584[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1665, "g_1665", print_hash_value);
    transparent_crc(g_1702, "g_1702", print_hash_value);
    transparent_crc(g_1872, "g_1872", print_hash_value);
    transparent_crc(g_1924, "g_1924", print_hash_value);
    transparent_crc(g_1971, "g_1971", print_hash_value);
    transparent_crc(g_1975, "g_1975", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1979[i], "g_1979[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2096, "g_2096", print_hash_value);
    transparent_crc(g_2506, "g_2506", print_hash_value);
    transparent_crc(g_2549, "g_2549", print_hash_value);
    transparent_crc(g_2641, "g_2641", print_hash_value);
    transparent_crc(g_2706, "g_2706", print_hash_value);
    transparent_crc(g_2774, "g_2774", print_hash_value);
    transparent_crc(g_2802, "g_2802", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 660
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 72
breakdown:
   depth: 1, occurrence: 191
   depth: 2, occurrence: 40
   depth: 3, occurrence: 6
   depth: 4, occurrence: 9
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 13, occurrence: 2
   depth: 15, occurrence: 1
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 1
   depth: 20, occurrence: 5
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 3
   depth: 27, occurrence: 3
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 2
   depth: 34, occurrence: 1
   depth: 41, occurrence: 1
   depth: 72, occurrence: 1

XXX total number of pointers: 649

XXX times a variable address is taken: 1604
XXX times a pointer is dereferenced on RHS: 348
breakdown:
   depth: 1, occurrence: 299
   depth: 2, occurrence: 29
   depth: 3, occurrence: 8
   depth: 4, occurrence: 7
   depth: 5, occurrence: 5
XXX times a pointer is dereferenced on LHS: 369
breakdown:
   depth: 1, occurrence: 315
   depth: 2, occurrence: 33
   depth: 3, occurrence: 20
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 58
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 22
XXX times a pointer is qualified to be dereferenced: 7713

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1602
   level: 2, occurrence: 416
   level: 3, occurrence: 291
   level: 4, occurrence: 164
   level: 5, occurrence: 38
XXX number of pointers point to pointers: 275
XXX number of pointers point to scalars: 374
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.5
XXX average alias set size: 1.62

XXX times a non-volatile is read: 2296
XXX times a non-volatile is write: 1131
XXX times a volatile is read: 82
XXX    times read thru a pointer: 51
XXX times a volatile is write: 32
XXX    times written thru a pointer: 21
XXX times a volatile is available for access: 762
XXX percentage of non-volatile access: 96.8

XXX forward jumps: 2
XXX backward jumps: 11

XXX stmts: 196
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 27
   depth: 2, occurrence: 26
   depth: 3, occurrence: 31
   depth: 4, occurrence: 52
   depth: 5, occurrence: 35

XXX percentage a fresh-made variable is used: 16.5
XXX percentage an existing variable is used: 83.5
********************* end of statistics **********************/


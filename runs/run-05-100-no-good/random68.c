/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      544939792
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile uint32_t  f0;
   int64_t  f1;
   int16_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static uint32_t g_7 = 0UL;
static int32_t g_11 = 0x23B1A956L;
static int32_t * volatile g_10 = &g_11;/* VOLATILE GLOBAL g_10 */
static union U0 g_66 = {0x0997736EL};/* VOLATILE GLOBAL g_66 */
static uint16_t g_73 = 0x4E0BL;
static uint8_t g_86 = 0x2AL;
static int8_t g_104 = 9L;
static uint64_t g_106 = 0UL;
static float g_108 = 0x3.0262A8p+15;
static union U0 g_112 = {4294967291UL};/* VOLATILE GLOBAL g_112 */
static uint32_t g_121 = 0xA7FFF357L;
static uint32_t g_122[8] = {0x3F56D1B7L,0x3F56D1B7L,0x3F56D1B7L,0x3F56D1B7L,0x3F56D1B7L,0x3F56D1B7L,0x3F56D1B7L,0x3F56D1B7L};
static int32_t * volatile g_128 = &g_11;/* VOLATILE GLOBAL g_128 */
static int32_t * volatile g_152 = (void*)0;/* VOLATILE GLOBAL g_152 */
static int32_t * volatile g_153 = &g_11;/* VOLATILE GLOBAL g_153 */
static union U0 g_200 = {4294967295UL};/* VOLATILE GLOBAL g_200 */
static volatile uint32_t g_213[6][6] = {{7UL,3UL,0xA60D7D18L,2UL,0UL,0xA60D7D18L},{0xC5006F50L,0xB927C989L,0UL,18446744073709551615UL,0UL,0xB927C989L},{2UL,3UL,0xF8001326L,18446744073709551615UL,3UL,0UL},{0xC5006F50L,0UL,0xF8001326L,2UL,0xB927C989L,0xB927C989L},{7UL,0UL,0UL,7UL,3UL,0xA60D7D18L},{7UL,3UL,0xA60D7D18L,2UL,0UL,0xA60D7D18L}};
static uint16_t g_221 = 0UL;
static int32_t g_235 = 0x44EFE062L;
static int32_t * volatile g_234[4][1][2] = {{{&g_235,&g_235}},{{&g_235,&g_235}},{{&g_235,&g_235}},{{&g_235,&g_235}}};
static union U0 g_282[2][5][9] = {{{{0x9C763639L},{0xC5F1AE00L},{0xC5F1AE00L},{0x9C763639L},{0x638CF908L},{0x0527778EL},{0x3F9A5F5DL},{4294967289UL},{0UL}},{{0xFD8CBC5EL},{0UL},{0UL},{1UL},{1UL},{0x25318A65L},{0x25318A65L},{1UL},{1UL}},{{0xEAA5562FL},{1UL},{0xEAA5562FL},{0x9CEF7823L},{0x638CF908L},{4294967289UL},{4294967295UL},{0UL},{0x0527778EL}},{{0xA464EB96L},{9UL},{1UL},{0x1F7334F8L},{0x83DFED90L},{0xF951E7C5L},{0x83DFED90L},{0x1F7334F8L},{1UL}},{{0x0527778EL},{0x0527778EL},{1UL},{0x9CEF7823L},{0x242CBDC2L},{0xEAA5562FL},{0UL},{0x3F9A5F5DL},{0xC5F1AE00L}}},{{{1UL},{0xF8B01E69L},{0xF951E7C5L},{1UL},{4294967293UL},{4294967293UL},{1UL},{0xF951E7C5L},{0xF8B01E69L}},{{0xDA6D7309L},{0xE643068DL},{1UL},{0x9C763639L},{4294967291UL},{0x9CEF7823L},{0x0527778EL},{0UL},{0x638CF908L}},{{0x1F7334F8L},{1UL},{1UL},{0xF8B01E69L},{0x1C98CFF1L},{0xF8B01E69L},{1UL},{1UL},{0x1F7334F8L}},{{4294967291UL},{0xE643068DL},{0xEAA5562FL},{0x8573A59FL},{0x0527778EL},{4294967295UL},{0xC5F1AE00L},{1UL},{4294967289UL}},{{0x6BEF0A75L},{0xF8B01E69L},{0UL},{0xF951E7C5L},{0xF951E7C5L},{0UL},{0xF8B01E69L},{0x6BEF0A75L},{0xFD8CBC5EL}}}};
static float *g_324 = (void*)0;
static float **g_323 = &g_324;
static float **g_407 = &g_324;
static float *** const  volatile g_406 = &g_407;/* VOLATILE GLOBAL g_406 */
static uint32_t g_412 = 1UL;
static volatile uint32_t *g_555 = &g_213[2][1];
static volatile uint32_t * volatile * volatile g_554 = &g_555;/* VOLATILE GLOBAL g_554 */
static volatile union U0 g_587 = {0xFC97C912L};/* VOLATILE GLOBAL g_587 */
static const uint8_t *g_591 = (void*)0;
static const uint8_t **g_590 = &g_591;
static union U0 g_594 = {7UL};/* VOLATILE GLOBAL g_594 */
static float * const  volatile g_615 = &g_108;/* VOLATILE GLOBAL g_615 */
static float g_623 = 0x0.2B0BE0p-80;
static float g_626 = 0x6.AE7EE2p-95;
static float g_628 = (-0x1.3p+1);
static int32_t * volatile g_639 = &g_235;/* VOLATILE GLOBAL g_639 */
static int32_t * volatile g_658 = &g_11;/* VOLATILE GLOBAL g_658 */
static int32_t * volatile g_660 = &g_11;/* VOLATILE GLOBAL g_660 */
static int16_t * volatile g_678 = &g_112.f2;/* VOLATILE GLOBAL g_678 */
static int64_t g_688 = 0x72D0E3FC21E75D40LL;
static int32_t *g_692 = &g_235;
static int32_t ** volatile g_691[5][4] = {{&g_692,&g_692,&g_692,&g_692},{&g_692,(void*)0,&g_692,&g_692},{&g_692,&g_692,&g_692,&g_692},{&g_692,&g_692,&g_692,&g_692},{&g_692,(void*)0,&g_692,&g_692}};
static int32_t ** volatile g_693 = &g_692;/* VOLATILE GLOBAL g_693 */
static int8_t g_715 = (-2L);
static int8_t *g_733 = &g_104;
static int8_t **g_732[2][3] = {{(void*)0,&g_733,(void*)0},{(void*)0,&g_733,(void*)0}};
static int32_t ** volatile g_849 = &g_692;/* VOLATILE GLOBAL g_849 */
static uint32_t g_851[3][3][6] = {{{0x2B4C9C16L,3UL,0xABBA070CL,4294967286UL,4294967286UL,0xABBA070CL},{4294967286UL,4294967286UL,0xABBA070CL,3UL,0x2B4C9C16L,0x2B4C9C16L},{4294967286UL,3UL,3UL,4294967286UL,0x2B4C9C16L,0xABBA070CL}},{{0x2B4C9C16L,4294967286UL,3UL,3UL,4294967286UL,0x2B4C9C16L},{0x2B4C9C16L,3UL,0xABBA070CL,4294967286UL,4294967286UL,0xABBA070CL},{4294967286UL,4294967286UL,0xABBA070CL,3UL,0x2B4C9C16L,0x2B4C9C16L}},{{4294967286UL,3UL,3UL,4294967286UL,0x2B4C9C16L,0xABBA070CL},{0x2B4C9C16L,4294967286UL,3UL,3UL,4294967286UL,0x2B4C9C16L},{0x2B4C9C16L,3UL,0xABBA070CL,4294967286UL,4294967286UL,0xABBA070CL}}};
static volatile uint32_t g_861 = 1UL;/* VOLATILE GLOBAL g_861 */
static volatile uint32_t *g_860 = &g_861;
static volatile uint32_t **g_859 = &g_860;
static uint64_t g_867 = 0x7EDDB55E725C5BC9LL;
static int32_t ** volatile g_871 = (void*)0;/* VOLATILE GLOBAL g_871 */
static const float g_881 = 0x7.D47C2Ap-68;
static volatile union U0 g_902 = {6UL};/* VOLATILE GLOBAL g_902 */
static volatile uint64_t ** volatile g_907 = (void*)0;/* VOLATILE GLOBAL g_907 */
static volatile uint64_t ** volatile * volatile g_908 = (void*)0;/* VOLATILE GLOBAL g_908 */
static volatile uint64_t ** volatile * volatile g_909 = (void*)0;/* VOLATILE GLOBAL g_909 */
static union U0 *g_914 = &g_282[0][1][0];
static union U0 ** volatile g_913 = &g_914;/* VOLATILE GLOBAL g_913 */
static int32_t g_1016 = 9L;
static int16_t g_1032 = 1L;
static volatile int32_t g_1038 = 1L;/* VOLATILE GLOBAL g_1038 */
static const volatile uint64_t * volatile * volatile * const g_1093 = (void*)0;
static uint8_t g_1101[7] = {0x9CL,0x9CL,0x9CL,0x9CL,0x9CL,0x9CL,0x9CL};
static float * volatile g_1104 = &g_628;/* VOLATILE GLOBAL g_1104 */
static volatile union U0 g_1132 = {4294967294UL};/* VOLATILE GLOBAL g_1132 */
static uint16_t *g_1169 = (void*)0;
static uint32_t *g_1290 = &g_121;
static uint32_t **g_1289[9][10][2] = {{{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,&g_1290},{(void*)0,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,(void*)0}},{{(void*)0,&g_1290},{&g_1290,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,&g_1290},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,(void*)0}},{{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,&g_1290}},{{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,(void*)0}},{{(void*)0,&g_1290},{&g_1290,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,&g_1290},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,(void*)0}},{{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,&g_1290}},{{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,(void*)0}},{{(void*)0,&g_1290},{&g_1290,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,&g_1290},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,&g_1290},{(void*)0,(void*)0},{&g_1290,(void*)0}},{{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,(void*)0},{(void*)0,&g_1290},{&g_1290,&g_1290},{&g_1290,(void*)0},{&g_1290,(void*)0},{&g_1290,&g_1290}}};
static uint32_t ***g_1288[5][5][6] = {{{&g_1289[2][4][1],&g_1289[4][9][0],&g_1289[8][5][0],&g_1289[5][1][0],&g_1289[8][5][0],&g_1289[4][9][0]},{&g_1289[8][5][0],&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[5][1][0],&g_1289[2][9][0],&g_1289[4][8][1]},{&g_1289[2][4][1],&g_1289[4][8][1],&g_1289[4][9][0],&g_1289[4][8][1],&g_1289[2][4][1],&g_1289[4][9][0]},{&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0],&g_1289[2][4][1],&g_1289[5][1][0]},{&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0]}},{{&g_1289[8][5][0],&g_1289[4][8][1],(void*)0,&g_1289[4][9][0],(void*)0,&g_1289[4][8][1]},{(void*)0,&g_1289[4][9][0],&g_1289[2][9][0],&g_1289[4][9][0],&g_1289[2][4][1],&g_1289[4][8][1]},{&g_1289[8][5][0],&g_1289[4][8][1],&g_1289[2][9][0],&g_1289[4][8][1],&g_1289[8][5][0],&g_1289[4][8][1]},{&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0],&g_1289[2][4][1],&g_1289[5][1][0]},{&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0]}},{{&g_1289[8][5][0],&g_1289[4][8][1],(void*)0,&g_1289[4][9][0],(void*)0,&g_1289[4][8][1]},{(void*)0,&g_1289[4][9][0],&g_1289[2][9][0],&g_1289[4][9][0],&g_1289[2][4][1],&g_1289[4][8][1]},{&g_1289[8][5][0],&g_1289[4][8][1],&g_1289[2][9][0],&g_1289[4][8][1],&g_1289[8][5][0],&g_1289[4][8][1]},{&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0],&g_1289[2][4][1],&g_1289[5][1][0]},{&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0]}},{{&g_1289[8][5][0],&g_1289[4][8][1],(void*)0,&g_1289[4][9][0],(void*)0,&g_1289[4][8][1]},{(void*)0,&g_1289[4][9][0],&g_1289[2][9][0],&g_1289[4][9][0],&g_1289[2][4][1],&g_1289[4][8][1]},{&g_1289[8][5][0],&g_1289[4][8][1],&g_1289[2][9][0],&g_1289[4][8][1],&g_1289[8][5][0],&g_1289[4][8][1]},{&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0],&g_1289[2][4][1],&g_1289[5][1][0]},{&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0]}},{{&g_1289[8][5][0],&g_1289[4][8][1],(void*)0,&g_1289[4][9][0],(void*)0,&g_1289[4][8][1]},{(void*)0,&g_1289[4][9][0],&g_1289[2][9][0],&g_1289[4][9][0],&g_1289[2][4][1],&g_1289[4][8][1]},{&g_1289[8][5][0],&g_1289[4][8][1],&g_1289[2][9][0],&g_1289[4][8][1],&g_1289[8][5][0],&g_1289[4][8][1]},{&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0],&g_1289[2][4][1],&g_1289[5][1][0]},{&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][9][0],&g_1289[4][8][1],(void*)0,&g_1289[5][1][0]}}};
static float g_1368 = 0x1.F6F7E5p-7;
static float g_1383 = 0x9.EB5E93p+60;
static uint64_t *g_1388 = &g_106;
static uint64_t * const *g_1387 = &g_1388;
static uint64_t * const * const *g_1386[9] = {&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387};
static uint64_t * const * const **g_1385 = &g_1386[6];
static int64_t ** const g_1417 = (void*)0;
static union U0 g_1448 = {0xF104D221L};/* VOLATILE GLOBAL g_1448 */
static int64_t g_1507 = 8L;
static int32_t ** const  volatile g_1512 = (void*)0;/* VOLATILE GLOBAL g_1512 */
static int32_t ** volatile g_1513 = (void*)0;/* VOLATILE GLOBAL g_1513 */
static int32_t ** volatile g_1514 = &g_692;/* VOLATILE GLOBAL g_1514 */
static int32_t ** volatile g_1562 = &g_692;/* VOLATILE GLOBAL g_1562 */
static uint8_t *g_1564 = &g_86;
static uint8_t **g_1563 = &g_1564;
static int8_t *** volatile g_1676 = &g_732[1][2];/* VOLATILE GLOBAL g_1676 */
static int16_t *g_1690[4][8][8] = {{{&g_66.f2,&g_112.f2,&g_66.f2,&g_1448.f2,&g_1032,&g_66.f2,&g_66.f2,&g_66.f2},{(void*)0,(void*)0,&g_1032,(void*)0,&g_1032,&g_282[1][3][2].f2,&g_594.f2,(void*)0},{&g_66.f2,(void*)0,&g_66.f2,&g_112.f2,&g_66.f2,(void*)0,&g_66.f2,&g_66.f2},{&g_200.f2,&g_66.f2,&g_594.f2,&g_66.f2,&g_282[1][3][2].f2,&g_1032,&g_112.f2,(void*)0},{&g_282[1][3][2].f2,&g_1032,&g_112.f2,&g_594.f2,&g_282[1][3][2].f2,&g_66.f2,&g_200.f2,&g_112.f2},{&g_200.f2,&g_66.f2,&g_200.f2,(void*)0,&g_66.f2,&g_594.f2,&g_1448.f2,&g_594.f2},{&g_66.f2,&g_1032,(void*)0,&g_66.f2,&g_1032,(void*)0,&g_594.f2,&g_1032},{&g_66.f2,&g_66.f2,&g_112.f2,&g_200.f2,&g_282[1][3][2].f2,&g_112.f2,&g_1032,(void*)0}},{{(void*)0,&g_1448.f2,&g_594.f2,&g_66.f2,(void*)0,&g_112.f2,&g_594.f2,&g_112.f2},{&g_594.f2,&g_200.f2,&g_282[1][3][2].f2,&g_66.f2,&g_282[1][3][2].f2,(void*)0,&g_594.f2,&g_200.f2},{(void*)0,&g_282[1][3][2].f2,&g_1032,&g_282[1][3][2].f2,&g_200.f2,&g_1448.f2,(void*)0,&g_112.f2},{&g_1032,(void*)0,&g_200.f2,(void*)0,&g_112.f2,&g_1032,(void*)0,&g_282[1][3][2].f2},{&g_282[1][3][2].f2,&g_66.f2,&g_1448.f2,(void*)0,&g_1448.f2,&g_1032,(void*)0,(void*)0},{&g_112.f2,(void*)0,&g_1032,&g_66.f2,&g_66.f2,(void*)0,(void*)0,&g_66.f2},{&g_1448.f2,(void*)0,(void*)0,&g_1448.f2,&g_594.f2,(void*)0,&g_594.f2,&g_112.f2},{&g_282[1][3][2].f2,(void*)0,(void*)0,&g_200.f2,&g_282[1][3][2].f2,(void*)0,(void*)0,&g_200.f2}},{{(void*)0,(void*)0,&g_112.f2,&g_1448.f2,&g_112.f2,(void*)0,&g_1448.f2,&g_200.f2},{&g_200.f2,(void*)0,&g_1448.f2,&g_282[1][3][2].f2,&g_112.f2,(void*)0,&g_200.f2,&g_1448.f2},{&g_594.f2,(void*)0,(void*)0,&g_66.f2,&g_1032,&g_1032,(void*)0,&g_200.f2},{&g_1032,&g_66.f2,&g_1448.f2,&g_112.f2,(void*)0,&g_1032,&g_112.f2,&g_282[1][3][2].f2},{(void*)0,(void*)0,&g_66.f2,&g_282[1][3][2].f2,&g_112.f2,&g_1448.f2,&g_1448.f2,&g_1448.f2},{&g_594.f2,&g_282[1][3][2].f2,&g_282[1][3][2].f2,&g_594.f2,&g_200.f2,(void*)0,&g_594.f2,&g_66.f2},{(void*)0,&g_200.f2,&g_66.f2,(void*)0,&g_112.f2,&g_112.f2,&g_594.f2,&g_594.f2},{&g_594.f2,&g_1448.f2,&g_1032,&g_200.f2,&g_282[1][3][2].f2,&g_112.f2,&g_200.f2,&g_1448.f2}},{{&g_282[1][3][2].f2,&g_66.f2,(void*)0,&g_594.f2,(void*)0,(void*)0,&g_112.f2,&g_66.f2},{&g_1032,&g_1032,&g_66.f2,(void*)0,&g_66.f2,(void*)0,&g_66.f2,&g_1032},{&g_200.f2,(void*)0,(void*)0,&g_282[1][3][2].f2,&g_112.f2,&g_594.f2,&g_282[1][3][2].f2,&g_1448.f2},{&g_282[1][3][2].f2,(void*)0,&g_282[1][3][2].f2,&g_1448.f2,&g_200.f2,&g_282[1][3][2].f2,&g_282[1][3][2].f2,&g_200.f2},{&g_112.f2,&g_1448.f2,(void*)0,&g_594.f2,&g_200.f2,&g_200.f2,&g_66.f2,&g_282[1][3][2].f2},{&g_200.f2,&g_200.f2,&g_66.f2,&g_282[1][3][2].f2,&g_1448.f2,&g_66.f2,&g_112.f2,(void*)0},{&g_594.f2,&g_1448.f2,&g_66.f2,&g_112.f2,(void*)0,&g_66.f2,&g_112.f2,(void*)0},{&g_282[1][3][2].f2,&g_1448.f2,&g_282[1][3][2].f2,&g_282[1][3][2].f2,&g_112.f2,&g_282[1][3][2].f2,(void*)0,&g_282[1][3][2].f2}}};
static int16_t **g_1689 = &g_1690[0][4][6];
static int32_t ** volatile g_1727 = &g_692;/* VOLATILE GLOBAL g_1727 */
static int64_t g_1796 = (-5L);
static int32_t ** volatile g_1825 = &g_692;/* VOLATILE GLOBAL g_1825 */
static int32_t **g_1869 = &g_692;
static int32_t ** volatile * const g_1868 = &g_1869;
static int32_t g_1907[6] = {(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)};
static volatile int32_t g_2006 = 0x3CC042CEL;/* VOLATILE GLOBAL g_2006 */
static int64_t *g_2068 = (void*)0;
static int64_t **g_2067 = &g_2068;
static int64_t ***g_2066 = &g_2067;
static int16_t g_2078 = 0xE8A8L;
static volatile union U0 g_2133 = {0UL};/* VOLATILE GLOBAL g_2133 */
static volatile uint64_t g_2177 = 0xA37FA31C47EB1617LL;/* VOLATILE GLOBAL g_2177 */
static volatile int32_t * const * volatile g_2188 = (void*)0;/* VOLATILE GLOBAL g_2188 */
static volatile int32_t g_2191[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
static uint16_t g_2214[9] = {0xBD84L,0xBD84L,0xBD84L,0xBD84L,0xBD84L,0xBD84L,0xBD84L,0xBD84L,0xBD84L};
static uint32_t g_2217 = 1UL;
static int32_t *g_2269 = (void*)0;
static int32_t **g_2268 = &g_2269;
static int32_t ***g_2267[3][6] = {{(void*)0,&g_2268,&g_2268,(void*)0,&g_2268,&g_2268},{(void*)0,&g_2268,&g_2268,(void*)0,&g_2268,&g_2268},{(void*)0,&g_2268,&g_2268,(void*)0,&g_2268,&g_2268}};
static int32_t * volatile g_2292[3][6][10] = {{{&g_235,&g_235,&g_235,&g_235,(void*)0,(void*)0,&g_235,&g_235,&g_235,&g_235},{(void*)0,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,(void*)0,&g_235,&g_235,&g_235,(void*)0,&g_235,&g_235,&g_235,(void*)0},{&g_235,&g_235,(void*)0,&g_235,&g_235,(void*)0,&g_235,&g_235,(void*)0,&g_235},{&g_235,(void*)0,&g_235,(void*)0,&g_235,(void*)0,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235,(void*)0,(void*)0,(void*)0,&g_235,&g_235}},{{&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235},{(void*)0,(void*)0,(void*)0,&g_235,(void*)0,(void*)0,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,(void*)0,(void*)0,&g_235,&g_235,&g_235,(void*)0,(void*)0},{&g_235,(void*)0,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,(void*)0,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,(void*)0,(void*)0,&g_235,&g_235,&g_235,&g_235,&g_235}},{{&g_235,(void*)0,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,(void*)0},{(void*)0,&g_235,&g_235,(void*)0,(void*)0,&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,(void*)0,(void*)0,&g_235,(void*)0,(void*)0,(void*)0,(void*)0,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235},{(void*)0,&g_235,&g_235,(void*)0,&g_235,&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235,(void*)0,&g_235,&g_235,(void*)0,&g_235}}};
static volatile union U0 g_2425 = {0UL};/* VOLATILE GLOBAL g_2425 */
static int64_t g_2474 = 0xD2506546D79C1F62LL;


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static float  func_4(const uint32_t  p_5);
static uint8_t  func_15(int32_t  p_16, const uint64_t  p_17, uint32_t * p_18, uint32_t  p_19, const int32_t  p_20);
static uint64_t  func_23(uint32_t * p_24, int8_t  p_25, const int32_t * const  p_26, int32_t * p_27);
static uint32_t * func_28(const uint32_t * p_29, int32_t  p_30, uint64_t  p_31);
static int32_t  func_32(int32_t * const  p_33, uint16_t  p_34, int32_t * p_35);
static int32_t * func_39(uint16_t  p_40);
static uint32_t * func_46(uint32_t * p_47, int32_t * const  p_48, uint64_t  p_49);
static uint32_t * func_50(int64_t  p_51, uint32_t  p_52);
static int32_t  func_62(const int32_t  p_63, int32_t  p_64, int32_t  p_65);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_10 g_11 g_122 g_639 g_235 g_112.f0 g_86 g_406 g_407 g_324 g_66.f2 g_106 g_658 g_660 g_590 g_591 g_692 g_112 g_112.f2 g_121 g_678 g_733 g_104 g_859 g_860 g_861 g_412 g_913 g_73 g_715 g_200.f2 g_1417 g_1016 g_594.f2 g_1101 g_1727 g_66.f1 g_1868 g_1869 g_688 g_851 g_1388 g_1563 g_1564 g_1387 g_1289 g_1796 g_1825 g_1385 g_1386 g_153 g_2066 g_112.f1 g_615 g_108 g_2133 g_221 g_1032 g_213 g_1676 g_732 g_555 g_2177 g_1907 g_2188 g_1514 g_587.f0 g_594.f1 g_914 g_282 g_2268 g_2474
 * writes: g_7 g_11 g_235 g_86 g_112.f2 g_66.f2 g_122 g_200.f2 g_221 g_104 g_594.f1 g_200.f1 g_121 g_914 g_867 g_73 g_692 g_590 g_1288 g_594.f2 g_66.f1 g_688 g_234 g_106 g_1016 g_2066 g_628 g_112.f1 g_1448.f1 g_851 g_715 g_1032 g_623 g_412 g_2267 g_282.f2 g_1368 g_626 g_2268 g_2214 g_1796 g_2078
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    uint32_t *l_6 = &g_7;
    int32_t l_2461 = 0xF851F08AL;
    int32_t l_2473 = 0x885C3B36L;
    uint16_t *l_2475[2][8][8] = {{{(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73,&g_221},{(void*)0,(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73},{&g_2214[8],&g_73,&g_73,&g_2214[8],&g_2214[1],&g_2214[8],&g_73,&g_73},{&g_73,&g_2214[1],&g_221,&g_221,&g_2214[1],&g_73,&g_2214[1],&g_221},{&g_2214[8],&g_2214[1],&g_2214[8],&g_73,&g_73,&g_2214[8],&g_2214[1],&g_2214[8]},{(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73,&g_221},{(void*)0,(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73},{&g_2214[8],&g_73,&g_73,&g_2214[8],&g_2214[1],&g_2214[8],&g_73,&g_73}},{{&g_73,&g_2214[1],&g_221,&g_221,&g_2214[1],&g_73,&g_2214[1],&g_221},{&g_2214[8],&g_2214[1],&g_2214[8],&g_73,&g_73,&g_2214[8],&g_2214[1],&g_2214[8]},{(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73,&g_221},{(void*)0,(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73},{&g_2214[8],&g_73,&g_73,&g_2214[8],&g_2214[1],&g_2214[8],&g_73,&g_73},{&g_73,&g_2214[1],&g_221,&g_221,&g_2214[1],&g_73,&g_2214[1],&g_221},{&g_2214[8],&g_2214[1],&g_2214[8],&g_73,&g_73,&g_2214[8],&g_2214[1],&g_2214[8]},{(void*)0,&g_73,&g_221,&g_73,(void*)0,(void*)0,&g_73,&g_221}}};
    int32_t l_2476 = (-1L);
    int16_t *l_2477 = &g_2078;
    int32_t l_2478 = 0xD8A5C089L;
    int i, j, k;
    l_2478 = (safe_mul_func_uint8_t_u_u((func_4(((*l_6)--)) , ((((safe_sub_func_int8_t_s_s(((((((((*l_2477) = (((((((safe_div_func_int8_t_s_s((9UL != ((l_2476 &= ((safe_sub_func_uint16_t_u_u((safe_add_func_int8_t_s_s((safe_div_func_uint64_t_u_u((l_2461 != (--(*g_1564))), (safe_rshift_func_int16_t_s_u((safe_mul_func_uint8_t_u_u((((safe_sub_func_int32_t_s_s((((((l_2473 = ((*g_860) && (safe_add_func_int16_t_s_s(l_2461, (l_6 == ((((*g_914) , ((((l_2461 & (+(-4L))) , (*g_733)) , 0x75E6D99275A8055ALL) <= 18446744073709551615UL)) && l_2461) , (void*)0)))))) | l_2461) <= 0x345464CDL) | 0xBAEDL) > l_2461), 4294967295UL)) > g_2474) < 0xAC6914F0L), (*g_733))), g_715)))), (-1L))), 0x10F0L)) , l_2461)) == l_2461)), 0xC1L)) , 4294967295UL) , 0x70L) >= l_2461) != l_2461) , (**g_859)) != (-5L))) ^ l_2461) && 0x56C0L) != 1UL) || l_2461) || l_2473) <= l_2478), 0UL)) > (*g_733)) , l_2473) && l_2473)), l_2478));
    return (**g_1563);
}


/* ------------------------------------------ */
/* 
 * reads : g_10 g_11 g_7 g_122 g_639 g_235 g_112.f0 g_86 g_406 g_407 g_324 g_66.f2 g_106 g_658 g_660 g_590 g_591 g_692 g_112 g_112.f2 g_121 g_678 g_733 g_104 g_859 g_860 g_861 g_412 g_913 g_73 g_715 g_200.f2 g_1417 g_1016 g_594.f2 g_1101 g_1727 g_66.f1 g_1868 g_1869 g_688 g_851 g_1388 g_1563 g_1564 g_1387 g_1289 g_1796 g_1825 g_1385 g_1386 g_153 g_2066 g_112.f1 g_615 g_108 g_2133 g_221 g_1032 g_213 g_1676 g_732 g_555 g_2177 g_1907 g_2188 g_1514 g_587.f0 g_594.f1 g_914 g_282 g_2268
 * writes: g_11 g_7 g_235 g_86 g_112.f2 g_66.f2 g_122 g_200.f2 g_221 g_104 g_594.f1 g_200.f1 g_121 g_914 g_867 g_73 g_692 g_590 g_1288 g_594.f2 g_66.f1 g_688 g_234 g_106 g_1016 g_2066 g_628 g_112.f1 g_1448.f1 g_851 g_715 g_1032 g_623 g_412 g_2267 g_282.f2 g_1368 g_626 g_2268 g_2214 g_1796
 */
static float  func_4(const uint32_t  p_5)
{ /* block id: 2 */
    int16_t l_14 = 0x5165L;
    int32_t *l_36 = &g_11;
    uint64_t *l_1067[10] = {&g_867,&g_867,&g_867,&g_867,&g_867,&g_867,&g_867,&g_867,&g_867,&g_867};
    const float l_1068[1] = {0x1.Cp+1};
    int64_t * const l_1416 = (void*)0;
    int64_t * const *l_1415 = &l_1416;
    uint32_t ***l_1418 = &g_1289[3][7][1];
    int32_t l_1440 = (-1L);
    int32_t l_1453 = 1L;
    int32_t l_1455 = 0L;
    uint8_t l_1476[9][5][5] = {{{1UL,0xD2L,0x95L,0xD2L,1UL},{0x4BL,0x5BL,8UL,0xEAL,250UL},{1UL,0xD2L,0x3EL,0x31L,0xADL},{0xEAL,0UL,0xEAL,0x5BL,250UL},{1UL,0x31L,249UL,0UL,0x7AL}},{{250UL,255UL,251UL,251UL,255UL},{0x3EL,1UL,249UL,0x71L,0x95L},{0UL,3UL,0xEAL,0xA5L,250UL},{255UL,247UL,0x3EL,255UL,0xEEL},{0UL,251UL,8UL,0x4BL,0xA5L}},{{0x3EL,0xACL,0x27L,0xD2L,0x27L},{250UL,250UL,0xA5L,0x4BL,8UL},{1UL,0UL,0xEEL,255UL,0x3EL},{0xEAL,0xA5L,250UL,0xA5L,0xEAL},{1UL,0UL,0x95L,0x71L,249UL}},{{0x4BL,250UL,255UL,251UL,251UL},{0x7AL,0xACL,0x7AL,0UL,249UL},{0UL,251UL,250UL,0x5BL,0xEAL},{249UL,247UL,0xADL,0x31L,0x3EL},{255UL,3UL,250UL,0xEAL,8UL}},{{255UL,1UL,0x7AL,8UL,0x27L},{0UL,255UL,255UL,0UL,0xA5L},{255UL,0x31L,0x95L,0x70L,0xEEL},{255UL,0UL,250UL,250UL,250UL},{249UL,0xD2L,0xEEL,0x70L,0x95L}},{{0UL,0x5BL,0xA5L,0UL,255UL},{0x7AL,8UL,0x27L,8UL,0x7AL},{0x4BL,0x5BL,8UL,0xEAL,250UL},{1UL,0xD2L,0x3EL,0x31L,0xADL},{0xEAL,0UL,0xEAL,0x5BL,250UL}},{{1UL,0x31L,249UL,0UL,0x7AL},{250UL,255UL,251UL,251UL,255UL},{0x3EL,1UL,249UL,0x71L,0x95L},{0UL,3UL,0xEAL,0xA5L,250UL},{255UL,247UL,0x3EL,255UL,0xEEL}},{{0UL,251UL,8UL,0x4BL,0xA5L},{0x3EL,0xACL,0x27L,0xD2L,0x27L},{250UL,250UL,0xA5L,0x4BL,8UL},{1UL,0UL,0xEEL,255UL,0x3EL},{0xEAL,0UL,0UL,0UL,251UL}},{{0UL,1UL,0x27L,0x31L,0xEEL},{0x5BL,0xA5L,0UL,255UL,255UL},{0xADL,0x70L,0xADL,1UL,0xEEL},{0xEAL,255UL,0xA5L,3UL,251UL},{0xEEL,255UL,0x3EL,247UL,255UL}}};
    uint8_t l_1505 = 0UL;
    uint32_t l_1620 = 18446744073709551615UL;
    int64_t l_1624 = 0xF5D4820C15B67B52LL;
    float ***l_1655 = (void*)0;
    uint32_t *l_1663 = &g_7;
    int32_t l_1782 = 1L;
    int32_t l_1784 = 0xC6E9D0ABL;
    int8_t l_1791 = 1L;
    int32_t l_1794 = 9L;
    int32_t l_1795[1];
    uint32_t l_1850 = 1UL;
    uint32_t l_1909 = 5UL;
    int8_t *l_1917 = &l_1791;
    uint32_t ** const l_2072 = (void*)0;
    int64_t l_2076 = 0x6858D935F300BA2CLL;
    int8_t l_2081 = 1L;
    int8_t ***l_2205 = (void*)0;
    int64_t l_2221 = 0xF9A22C35AA14A747LL;
    uint8_t l_2245[2][7];
    int32_t l_2285 = 0x075AE293L;
    uint64_t **l_2448[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint64_t ***l_2447 = &l_2448[2];
    uint16_t l_2450 = 0x3938L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1795[i] = 1L;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
            l_2245[i][j] = 0xF5L;
    }
    (*g_10) ^= p_5;
    if (((safe_div_func_uint16_t_u_u(l_14, (func_15((safe_rshift_func_uint8_t_u_u(((g_867 = func_23(func_28(&g_7, (func_32(((((void*)0 == l_36) , (safe_lshift_func_int8_t_s_s(5L, 5))) , func_39(g_11)), ((!((*l_36) , p_5)) , g_122[2]), l_36) , 0xE78D2849L), p_5), p_5, l_36, l_36)) <= l_14), 7)), g_73, &g_851[2][0][0], p_5, g_715) , 0x280DL))) , 0xFB75AA32L))
    { /* block id: 633 */
        float l_1421 = (-0x1.Fp-1);
        int32_t l_1441 = 0L;
        int32_t l_1454 = (-1L);
        int32_t l_1456 = 0xE92D44FEL;
        int32_t l_1457[3];
        uint32_t l_1525 = 1UL;
        int16_t *l_1581 = &g_594.f2;
        int16_t **l_1580 = &l_1581;
        uint16_t l_1589 = 1UL;
        int32_t *l_1623[8][6][3] = {{{&g_235,&g_235,&l_1454},{(void*)0,&g_235,&l_1457[2]},{&l_1440,&l_1440,&g_235},{(void*)0,&g_235,&l_1457[2]},{&l_1453,&l_1440,&g_235},{&g_11,&l_1457[2],&l_1457[2]}},{{&l_1453,&l_1457[1],&l_1454},{&l_1453,&l_1454,&l_1453},{&g_11,(void*)0,&l_1453},{&l_1453,(void*)0,&l_1457[1]},{(void*)0,(void*)0,&l_1440},{&l_1440,&l_1454,&l_1440}},{{(void*)0,&l_1457[1],&l_1440},{&g_235,&l_1457[2],&l_1440},{&l_1440,&l_1440,&l_1457[1]},{&l_1457[1],&g_235,&l_1453},{&l_1440,&l_1440,&l_1453},{&g_235,&g_235,&l_1454}},{{(void*)0,&g_235,&l_1457[2]},{&l_1440,&l_1440,&g_235},{(void*)0,&g_235,&l_1457[2]},{&l_1453,&l_1440,&g_235},{&g_11,&l_1457[2],&l_1457[2]},{&l_1453,&l_1457[1],&l_1454}},{{&l_1453,&l_1454,&l_1453},{&g_11,(void*)0,&l_1453},{&l_1453,(void*)0,&l_1457[1]},{(void*)0,(void*)0,&l_1440},{&l_1440,&l_1454,&l_1440},{(void*)0,&l_1457[1],&l_1440}},{{&g_235,&l_1457[2],&l_1440},{&l_1440,&l_1440,&l_1457[1]},{&l_1457[1],&g_235,&l_1453},{&l_1440,&l_1440,&l_1453},{&g_235,&g_235,&l_1454},{(void*)0,&g_235,&l_1457[2]}},{{&l_1440,&l_1440,&g_235},{(void*)0,&g_235,&l_1457[2]},{&l_1453,&l_1440,&g_235},{&g_11,&l_1457[2],&l_1457[2]},{&l_1453,&l_1457[1],&l_1454},{&l_1453,&l_1454,&l_1453}},{{&g_11,(void*)0,&l_1453},{&l_1453,(void*)0,&l_1457[1]},{(void*)0,(void*)0,&l_1440},{&l_1440,&l_1454,&l_1440},{(void*)0,&l_1457[1],&l_1440},{&g_235,&l_1457[2],&l_1440}}};
        int16_t **l_1688 = &l_1581;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1457[i] = 0x53A9BAF2L;
        for (g_200.f2 = 0; (g_200.f2 <= (-10)); --g_200.f2)
        { /* block id: 636 */
            uint8_t *l_1422 = &g_86;
            int32_t l_1432 = 0xD1659965L;
            int16_t *l_1439 = &g_594.f2;
            float l_1442 = (-0x1.5p-1);
            uint8_t l_1443 = 249UL;
            int32_t *l_1444 = &g_235;
            (*l_1444) |= (((*g_678) = (l_1415 == g_1417)) != (((g_1288[0][0][2] = l_1418) == (((safe_mod_func_uint16_t_u_u(p_5, (((*l_1422) = (p_5 != g_1016)) | (*l_36)))) > (safe_add_func_uint32_t_u_u((((safe_mul_func_uint16_t_u_u(((((safe_add_func_int32_t_s_s((+(safe_sub_func_int8_t_s_s(l_1432, (safe_add_func_uint64_t_u_u((((safe_lshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_u(((*l_1439) |= (-1L)), 10)) < 0x2A67524CL), 10)) & l_1440) , 0xB3FC1B4A276FED2ALL), 0x364B024A0548A449LL))))), g_1101[1])) != p_5) && l_1441) ^ 0x47L), p_5)) < l_1432) < p_5), l_1443))) , &g_1289[4][9][0])) > 0xF84CEEA8L));
        }
        l_36 = func_39((*l_36));
        l_1441 |= p_5;
        for (l_14 = 0; (l_14 <= 1); l_14 += 1)
        { /* block id: 647 */
            int32_t l_1447[1][6][3] = {{{0x467E6C1CL,0x467E6C1CL,0xEE4A012DL},{(-1L),(-1L),0x652D287FL},{0x467E6C1CL,0x467E6C1CL,0xEE4A012DL},{(-1L),(-1L),0x652D287FL},{0x467E6C1CL,0x467E6C1CL,0x467E6C1CL},{(-1L),(-1L),(-1L)}}};
            int32_t l_1506 = 0x64255C8FL;
            uint16_t *l_1553[4][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
            const uint8_t **l_1567 = (void*)0;
            float l_1593 = 0x1.7A73C8p+48;
            const float *l_1602 = (void*)0;
            const float **l_1601 = &l_1602;
            const float ***l_1600 = &l_1601;
            uint64_t l_1609 = 18446744073709551615UL;
            int32_t l_1622 = 0xFE543D5BL;
            uint32_t **l_1701 = &g_1290;
            uint8_t l_1708 = 0x07L;
            int32_t l_1723 = 0xE17A819AL;
            int i, j, k;
        }
    }
    else
    { /* block id: 784 */
        int32_t *l_1726 = &l_1453;
        int8_t l_1742 = 0x83L;
        int32_t l_1775 = 0xC0E43E23L;
        int32_t l_1778 = 4L;
        int32_t l_1780 = 0x4515FFDBL;
        int32_t l_1785 = 0x1F2402C6L;
        int32_t l_1788 = 0x9035DE03L;
        int32_t l_1790 = (-9L);
        int32_t l_1792 = 0x11CBC48EL;
        int32_t l_1793 = 0x97854483L;
        int32_t l_1797 = 8L;
        int8_t l_1842[8][4] = {{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L}};
        uint64_t **l_1928 = &l_1067[0];
        uint64_t ***l_1927 = &l_1928;
        uint64_t ****l_1926[7];
        uint64_t *****l_1925 = &l_1926[6];
        float * const *l_1938 = (void*)0;
        int8_t l_2059 = 0L;
        int8_t l_2063 = 0xEFL;
        int64_t ***l_2069 = &g_2067;
        uint32_t **l_2150 = &g_1290;
        int i, j;
        for (i = 0; i < 7; i++)
            l_1926[i] = &l_1927;
        for (g_104 = 4; (g_104 >= 0); g_104 -= 1)
        { /* block id: 787 */
            int i;
            (*g_1727) = l_1726;
            return g_1101[g_104];
        }
        if ((l_36 != ((safe_rshift_func_int16_t_s_s(0xA823L, p_5)) , (void*)0)))
        { /* block id: 791 */
            uint64_t l_1743 = 18446744073709551611UL;
            int32_t l_1783 = 0x6C4A6E37L;
            int32_t l_1786 = 0xF9EC248EL;
            int32_t l_1787 = 0xAA87C60EL;
            int32_t l_1789[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
            int16_t l_1824[3][7] = {{0L,0L,0xA35BL,0L,0L,0xA35BL,0L},{0L,0x1956L,0x1956L,0L,0x1956L,0x1956L,0L},{0x1956L,0L,0x1956L,0x1956L,0L,0x1956L,0x1956L}};
            const int64_t l_1843 = (-2L);
            uint32_t *l_1908 = &l_1620;
            uint32_t **l_1912 = &l_1663;
            uint32_t ***l_1911[4][9] = {{&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912},{(void*)0,&l_1912,&l_1912,&l_1912,(void*)0,&l_1912,&l_1912,&l_1912,&l_1912},{&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912,&l_1912},{(void*)0,(void*)0,&l_1912,&l_1912,&l_1912,&l_1912,(void*)0,&l_1912,&l_1912}};
            uint64_t ****l_1924 = (void*)0;
            uint64_t *****l_1923 = &l_1924;
            int i, j;
            for (g_86 = 0; (g_86 <= 7); g_86++)
            { /* block id: 794 */
                uint64_t **l_1735 = &g_1388;
                int32_t l_1736 = 0xB262AB91L;
                int32_t l_1740 = 0x6F8F7167L;
                int32_t l_1741[10];
                uint32_t **l_1769 = (void*)0;
                uint32_t ***l_1768[9] = {&l_1769,&l_1769,&l_1769,&l_1769,&l_1769,&l_1769,&l_1769,&l_1769,&l_1769};
                uint16_t *l_1802 = (void*)0;
                int8_t *l_1918 = &l_1742;
                uint16_t *l_1929 = &g_221;
                int16_t l_1930 = 6L;
                int i;
                for (i = 0; i < 10; i++)
                    l_1741[i] = 0x2DA3946AL;
            }
        }
        else
        { /* block id: 837 */
            const int32_t l_1931 = 4L;
            (*l_36) &= l_1931;
        }
        if ((safe_rshift_func_int8_t_s_u(((*l_1917) = (*l_36)), (safe_lshift_func_int16_t_s_s((-3L), ((safe_rshift_func_uint8_t_u_s((*l_1726), 0)) | (((l_1938 = (void*)0) == (void*)0) >= ((((*l_36) >= (0x3FEB67DC469A818ELL != (*l_36))) ^ (safe_div_func_int32_t_s_s(p_5, (*l_1726)))) | p_5))))))))
        { /* block id: 842 */
            const int16_t l_1949[5] = {3L,3L,3L,3L,3L};
            int32_t l_1986 = 0L;
            int32_t l_2001[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            uint32_t l_2007[1][8] = {{0x64347335L,0x64347335L,0x64347335L,0x64347335L,0x64347335L,0x64347335L,0x64347335L,0x64347335L}};
            uint16_t l_2056 = 1UL;
            uint32_t l_2060 = 4UL;
            uint32_t **l_2071 = &g_1290;
            float *l_2073 = &g_628;
            int32_t l_2079 = 0xE91A64ABL;
            uint16_t l_2082 = 0xC850L;
            int i, j;
            for (g_66.f1 = 0; (g_66.f1 <= (-24)); g_66.f1 = safe_sub_func_int64_t_s_s(g_66.f1, 6))
            { /* block id: 845 */
                int16_t l_1952 = 9L;
                uint32_t *l_1988[7][7] = {{&l_1909,&l_1620,&l_1620,&l_1620,&l_1620,&l_1909,&l_1620},{(void*)0,&l_1620,&l_1620,&l_1620,(void*)0,&l_1909,(void*)0},{&l_1620,&l_1620,&l_1620,&l_1620,&l_1909,&l_1620,&l_1909},{&l_1909,&l_1620,&l_1909,(void*)0,(void*)0,(void*)0,&l_1909},{&l_1620,&l_1620,&l_1620,&l_1909,&l_1620,&l_1620,&l_1909},{(void*)0,(void*)0,&l_1620,&l_1620,&l_1620,(void*)0,(void*)0},{&l_1909,&l_1909,&l_1620,&l_1909,&l_1620,&l_1620,&l_1620}};
                int32_t l_1997 = 0L;
                int32_t l_1998 = 0x68DE3482L;
                int32_t l_1999 = 7L;
                int32_t l_2000[6][3][8] = {{{0x5D17D431L,1L,6L,6L,0x07E816D2L,(-1L),0xF4A56E7FL,0L},{5L,0x094CCB61L,(-6L),0xF4A56E7FL,0x07E816D2L,0L,2L,(-1L)},{0L,0L,0L,0L,6L,(-6L),1L,0x3F6ACDA0L}},{{1L,0L,(-7L),0L,0x3F6ACDA0L,(-1L),1L,0L},{0x11DB2278L,0L,0L,0x5D17D431L,0x5D17D431L,0L,0L,0x11DB2278L},{(-7L),0xF9753CC9L,(-1L),0L,(-5L),0xE17927C6L,0L,0xA7FA6E39L}},{{0L,0L,0x3F6ACDA0L,0xB6D91997L,0L,0xE17927C6L,8L,0L},{0xD73D94AFL,0xF9753CC9L,0x11DB2278L,(-1L),6L,0L,(-1L),0x31AFA24AL},{0L,0L,(-1L),1L,0xCF5DC239L,(-1L),0L,8L}},{{0xF4A56E7FL,0L,0x0C4D26D9L,(-6L),(-1L),(-6L),0x0C4D26D9L,0L},{0xE17927C6L,0L,0L,0x9A999264L,0x6A3EDF16L,0L,0xD73D94AFL,(-8L)},{0x3F6ACDA0L,0x094CCB61L,0xF9753CC9L,0xA7FA6E39L,0xE17927C6L,(-1L),0xD73D94AFL,0x6A3EDF16L}},{{0x9A999264L,0xA7FA6E39L,0L,0L,0xB6D91997L,5L,0x0C4D26D9L,0x07E816D2L},{0xB6D91997L,5L,0x0C4D26D9L,0x07E816D2L,1L,6L,0L,3L},{(-1L),8L,(-1L),(-1L),(-8L),(-8L),(-1L),(-1L)}},{{(-5L),(-5L),0x11DB2278L,1L,0x094CCB61L,(-1L),8L,5L},{0x6A3EDF16L,1L,0x3F6ACDA0L,2L,3L,0xCF5DC239L,0L,5L},{1L,0x31AFA24AL,(-1L),1L,0x0C4D26D9L,0L,0L,(-1L)}}};
                int16_t l_2022 = 0x7249L;
                int i, j, k;
                (**g_1868) = (void*)0;
                for (g_11 = 1; (g_11 >= 0); g_11 -= 1)
                { /* block id: 849 */
                    uint32_t l_1943 = 0x18088454L;
                    int32_t l_1993 = 0x32027011L;
                    uint64_t * const **l_1994[1];
                    int32_t l_2002 = 0x28CB9236L;
                    int32_t l_2003 = 5L;
                    int32_t l_2004 = 0L;
                    int32_t l_2005[9][4][6] = {{{0x842030A6L,0L,9L,(-7L),0x805EDFD5L,0x573F1E2EL},{1L,5L,0L,0x3A6B74DCL,0xBCF2C6D8L,0xB8956384L},{0x34AA8FC4L,0xA1BAE3AFL,0xCED52BD3L,0x85F02417L,8L,0x805EDFD5L},{0x25F5B0F3L,0xACA2BD5FL,0x6F4D3A95L,0x274B33B1L,0x6F4D3A95L,0xACA2BD5FL}},{{0xE768ED1AL,0x191CA6EAL,1L,0x5D0C47BAL,1L,0xF0A419BCL},{5L,0x0DF5B09EL,0x999A3DF8L,0L,0x34AA8FC4L,0x2AF2609CL},{(-9L),0x0DF5B09EL,(-7L),0xA5A6D87EL,1L,0L},{0xC70E4D51L,0x191CA6EAL,(-1L),0x573F1E2EL,0x6F4D3A95L,0xE768ED1AL}},{{1L,0xACA2BD5FL,5L,8L,0xA5A6D87EL,0x53C495C1L},{0xA1BAE3AFL,1L,1L,(-9L),0xB859037BL,(-3L)},{0xB24BFF8FL,(-3L),0L,1L,0xECF0D14EL,5L},{(-1L),1L,9L,1L,0x34AA8FC4L,(-9L)}},{{2L,0xACA2BD5FL,0xBC67A8D4L,0x3B85222CL,0x11FC5240L,0x34AA8FC4L},{(-8L),5L,0xECF0D14EL,0L,0xF5D5986EL,0x3B85222CL},{0L,(-1L),0xA5A6D87EL,0x0DF5B09EL,0xB5C3D899L,1L},{0xA5A6D87EL,(-1L),(-5L),0L,0xB8956384L,0xD9CF8D7EL}},{{0x805EDFD5L,(-5L),1L,0xECF0D14EL,0xACA2BD5FL,0xB7B3E26AL},{0x0DF5B09EL,0x2CA02C0DL,0xB24BFF8FL,1L,0x9F42D2CDL,0xAEA6F82AL},{(-8L),(-6L),0x25F5B0F3L,0x6F4D3A95L,(-1L),0x805EDFD5L},{(-7L),0x999A3DF8L,0L,(-3L),(-3L),0L}},{{2L,2L,0xBCF2C6D8L,9L,1L,0x2AF2609CL},{(-2L),1L,6L,0xC49DE76EL,0x25F5B0F3L,0xBCF2C6D8L},{0x53C495C1L,(-2L),6L,0x008AD114L,2L,0x2AF2609CL},{(-6L),0x008AD114L,0xBCF2C6D8L,0xF5D5986EL,0x7612BD47L,0L}},{{0xF5D5986EL,0x7612BD47L,0L,0x34AA8FC4L,0xB24BFF8FL,0x805EDFD5L},{1L,1L,0x25F5B0F3L,0x0F24557BL,1L,0xAEA6F82AL},{1L,(-9L),0xB24BFF8FL,0x85F02417L,0x2AF2609CL,0xB7B3E26AL},{0xF0A419BCL,0L,1L,0x805EDFD5L,0x1D25C2BBL,0xD9CF8D7EL}},{{0L,0xB8956384L,(-5L),(-6L),0x573F1E2EL,1L},{0xC9D55D2DL,0x11FC5240L,0xA5A6D87EL,5L,1L,0x3B85222CL},{0xC49DE76EL,(-1L),0xECF0D14EL,(-9L),0xBC67A8D4L,0x34AA8FC4L},{(-9L),6L,0xBC67A8D4L,0xE768ED1AL,0x008AD114L,(-9L)}},{{(-1L),(-1L),9L,0xCED52BD3L,6L,5L},{0x842030A6L,2L,0L,0xAEA6F82AL,(-8L),(-3L)},{0xCED52BD3L,0x0DF5B09EL,1L,(-2L),0L,0x53C495C1L},{1L,5L,5L,(-1L),0x53C495C1L,(-1L)}}};
                    int16_t *l_2014 = &l_14;
                    int16_t *l_2021[2];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1994[i] = &g_1387;
                    for (i = 0; i < 2; i++)
                        l_2021[i] = &g_1448.f2;
                    for (g_688 = 0; (g_688 >= 0); g_688 -= 1)
                    { /* block id: 852 */
                        int i, j, k;
                        g_234[(g_688 + 1)][g_688][g_688] = func_39(g_851[(g_688 + 1)][(g_11 + 1)][g_11]);
                    }
                    if (l_1943)
                        break;
                    if ((!(safe_add_func_int32_t_s_s(p_5, (p_5 <= (((void*)0 != &g_693) > (safe_sub_func_uint8_t_u_u(l_1949[0], ((((((0xDD9707F9L == (g_122[3] ^= ((*l_1663)++))) < (((l_1952 && ((safe_sub_func_uint32_t_u_u((p_5 & p_5), (*l_1726))) | (*l_1726))) | 0xB206L) <= l_1949[4])) , &l_1663) != (void*)0) | (*g_1388)) >= 0L)))))))))
                    { /* block id: 858 */
                        uint16_t *l_1987 = &g_73;
                        int64_t *** const l_1989[2] = {(void*)0,(void*)0};
                        int i;
                        (*l_1726) = ((~((((safe_lshift_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_add_func_uint8_t_u_u(((safe_mod_func_int64_t_s_s((((((((&g_688 != (void*)0) , (((safe_rshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((safe_mod_func_uint64_t_u_u((l_1952 || (++(**g_1563))), (safe_rshift_func_uint8_t_u_u(0xCFL, p_5)))), (255UL | (safe_mod_func_uint32_t_u_u((((l_1988[6][0] = func_39(((*l_1987) = ((l_1986 = (safe_rshift_func_int16_t_s_s(((safe_add_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(((**g_1387) = (safe_add_func_int32_t_s_s((g_861 <= (safe_mul_func_uint8_t_u_u((*l_36), (l_1943 || 0x9D1FDFD4L)))), p_5))), p_5)), 0L)) ^ 0x51AFB047ACF0A431LL), l_1943))) , l_1986)))) != (void*)0) , 4294967295UL), 0xDBBD43D8L))))) == 65531UL), 9)) < l_1943) < 9UL)) , 1UL) , (void*)0) != (*l_1418)) & l_1943) , 0x9E60308E9DA21708LL), p_5)) >= 0x742BL), 0xDBL)), p_5)), (*l_1726))) >= g_1796) , &g_1417) != l_1989[1])) , 1L);
                        (**g_1868) = (*g_1825);
                    }
                    else
                    { /* block id: 866 */
                        int32_t *l_1992[5] = {&g_1016,&g_1016,&g_1016,&g_1016,&g_1016};
                        int32_t l_1995 = 7L;
                        int32_t *l_1996[3][1];
                        int i, j;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1996[i][j] = &l_1788;
                        }
                        if (p_5)
                            break;
                        l_1995 |= (safe_rshift_func_int16_t_s_u(((g_1016 = (l_1993 ^= ((void*)0 != g_1417))) , (l_1994[0] != (*g_1385))), 15));
                        --l_2007[0][7];
                    }
                    if ((((0xB692BB7CL && ((l_1998 , p_5) > ((*l_2014) |= (safe_mul_func_uint8_t_u_u(0xCAL, p_5))))) ^ (l_2001[2] &= ((&l_1663 != &l_1663) || ((safe_mod_func_uint16_t_u_u(((5UL ^ (safe_sub_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((((*g_1387) != l_1416) < p_5), p_5)), 65535UL))) , 0x0762L), l_1952)) < (-1L))))) & l_2022))
                    { /* block id: 875 */
                        uint64_t l_2055 = 0xEA6CBCCD753DD7CELL;
                        int64_t *l_2057 = (void*)0;
                        int64_t *l_2058[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int32_t *l_2061 = &l_1998;
                        int i;
                        (*l_2061) ^= (l_2060 &= ((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(((*l_2014) = ((safe_mul_func_uint16_t_u_u(((*g_153) == g_86), (safe_add_func_int16_t_s_s(((safe_mod_func_int16_t_s_s((((void*)0 == (**g_1868)) || (l_1986 |= (safe_mod_func_uint64_t_u_u(18446744073709551613UL, (((((safe_sub_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u((*l_1726), (safe_mul_func_uint16_t_u_u(p_5, (safe_add_func_uint32_t_u_u(((((**g_1387) < (safe_mul_func_int8_t_s_s((+(((safe_add_func_uint32_t_u_u((safe_mod_func_int64_t_s_s(((l_2001[4] |= ((~((((safe_add_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u(((-1L) && p_5), p_5)) , l_2002), 0UL)) <= 1UL) || (*l_36)) == p_5)) && p_5)) == l_2055), (*l_36))), p_5)) , l_2056) < l_2055)), p_5))) <= (-1L)) ^ 0xB5A73B84L), (*l_1726))))))) ^ p_5), l_2055)) , (*g_1564)) & p_5) < (*l_36)) | (*l_1726)))))), l_2055)) > 0UL), p_5)))) <= l_2059)), 6)), 5)) | 4L));
                    }
                    else
                    { /* block id: 881 */
                        (*g_1869) = (*g_1869);
                    }
                    for (l_1952 = 0; (l_1952 <= 0); l_1952 += 1)
                    { /* block id: 886 */
                        int16_t l_2062 = 6L;
                        return l_2062;
                    }
                }
            }
            (*l_2073) = ((l_2063 , (((((safe_div_func_int32_t_s_s((((p_5 , (g_2066 = g_2066)) != (p_5 , l_2069)) ^ 0x77E25BAF09A29D8ELL), ((*l_1663) = (safe_unary_minus_func_uint8_t_u(((p_5 && ((*l_36) & (l_2071 == l_2072))) || (*l_36))))))) == (*g_860)) , l_2073) == l_1726) == (*l_1726))) <= (-0x6.Cp+1));
            for (l_2056 = 0; (l_2056 != 10); l_2056 = safe_add_func_int64_t_s_s(l_2056, 5))
            { /* block id: 896 */
                int32_t *l_2077[2][6][10] = {{{(void*)0,&l_1793,&l_2001[6],&g_235,&l_1797,(void*)0,&l_1792,&l_1793,&l_1795[0],&l_1795[0]},{&g_235,(void*)0,&l_1797,(void*)0,(void*)0,&l_1797,(void*)0,&g_235,&l_1778,&l_1797},{(void*)0,&l_1788,(void*)0,&l_1986,(void*)0,(void*)0,(void*)0,(void*)0,&l_1797,(void*)0},{&l_1797,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_235,(void*)0,(void*)0},{(void*)0,&l_1797,&l_1797,&l_1797,(void*)0,&l_1793,&l_1986,(void*)0,&l_1793,&l_2001[6]},{&l_2001[6],&l_1788,(void*)0,&l_2001[6],(void*)0,&l_1795[0],&l_1792,&l_1797,(void*)0,&l_2001[6]}},{{&l_1797,&l_2001[6],(void*)0,(void*)0,(void*)0,&l_1788,&l_1788,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1793,&l_1792,(void*)0,&l_1797,&g_235,&l_2001[6],&l_1793,(void*)0},{&l_1797,(void*)0,&l_1795[0],&g_235,(void*)0,&l_1785,&g_235,&l_1797,&l_1788,&l_1797},{(void*)0,(void*)0,&l_1788,(void*)0,(void*)0,(void*)0,&l_1788,(void*)0,(void*)0,&l_1795[0]},{(void*)0,&l_2001[6],&l_1797,(void*)0,&l_1986,&l_1797,&l_1792,&l_1797,&l_1778,&g_235},{&l_1986,&l_1788,&l_1785,(void*)0,(void*)0,&l_1785,&l_1986,(void*)0,(void*)0,&l_1986}}};
                int64_t l_2080 = 0x74A25A5D18F0F837LL;
                int i, j, k;
                if (p_5)
                    break;
                ++l_2082;
            }
        }
        else
        { /* block id: 900 */
            uint8_t l_2100 = 0xFAL;
            int32_t l_2110[8];
            float l_2111 = (-0x1.5p-1);
            uint16_t l_2112 = 0xAAE6L;
            int64_t l_2120 = (-1L);
            uint32_t **l_2151 = &g_1290;
            uint32_t *l_2152 = &g_851[2][0][1];
            int i;
            for (i = 0; i < 8; i++)
                l_2110[i] = 0x1BAD35D6L;
            l_1793 &= (safe_unary_minus_func_int8_t_s((((safe_mul_func_uint8_t_u_u(((**g_1563) = (safe_mul_func_int16_t_s_s(p_5, (safe_rshift_func_int16_t_s_s((safe_add_func_int16_t_s_s((((safe_add_func_int8_t_s_s((*l_36), (safe_div_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((**g_1563), (((*l_1917) = ((*g_733) = ((*l_36) || l_2100))) && (safe_mul_func_uint16_t_u_u(6UL, (0xC4L | (+((l_2110[1] = ((**g_1387) = ((safe_add_func_int32_t_s_s((*l_1726), ((safe_mul_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(9UL, (*l_1726))), (*l_1726))) <= p_5))) < 0x97L))) >= (-6L))))))))) || l_2100), 1UL)))) , (*g_406)) != l_1938), p_5)), 14))))), l_2100)) == (*l_1726)) ^ l_2112)));
            for (g_112.f1 = 0; (g_112.f1 <= 21); ++g_112.f1)
            { /* block id: 909 */
                int32_t ** const l_2117 = (void*)0;
                for (g_1448.f1 = 0; (g_1448.f1 <= (-19)); g_1448.f1 = safe_sub_func_int64_t_s_s(g_1448.f1, 3))
                { /* block id: 912 */
                    float *l_2124[6];
                    const float *l_2126 = (void*)0;
                    const float **l_2125 = &l_2126;
                    const float *l_2128 = &g_881;
                    const float **l_2127 = &l_2128;
                    int32_t l_2131 = 1L;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_2124[i] = (void*)0;
                    (**g_1868) = func_39(((*g_615) , (((((void*)0 != l_2117) | (safe_sub_func_uint16_t_u_u(l_2120, ((safe_lshift_func_int16_t_s_s((+p_5), (l_2124[1] == ((*l_2127) = ((*l_2125) = (void*)0))))) ^ (((p_5 >= (6L || p_5)) ^ (*g_1564)) <= (*g_860)))))) | 1L) > 0x46L)));
                    for (g_66.f2 = 0; (g_66.f2 == 24); g_66.f2++)
                    { /* block id: 918 */
                        if (l_2131)
                            break;
                        (*l_1726) &= p_5;
                        return p_5;
                    }
                }
                (*g_1869) = &l_2110[1];
            }
            (*l_1726) = ((*l_36) = (((safe_unary_minus_func_uint32_t_u((g_2133 , (safe_sub_func_uint32_t_u_u(((safe_add_func_int8_t_s_s((p_5 | ((safe_lshift_func_uint8_t_u_u(((**g_1563) ^= ((safe_lshift_func_uint16_t_u_s(65528UL, (safe_div_func_int8_t_s_s(((p_5 , ((safe_mul_func_uint16_t_u_u(((l_2110[7] | ((*l_2152) ^= ((*l_1663) = ((safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(0xC91EL, (l_2150 != l_2151))), 0x310AL)) != 0x4CL)))) >= (*g_660)), 0L)) , 251UL)) <= p_5), (*l_36))))) ^ g_221)), 4)) <= p_5)), (*l_1726))) || (*l_36)), g_104))))) , (*l_1726)) , p_5));
        }
    }
    for (g_715 = 0; (g_715 <= 2); g_715 = safe_add_func_int16_t_s_s(g_715, 3))
    { /* block id: 935 */
        uint32_t l_2162 = 0xC2FAEB45L;
        int32_t l_2165 = 0xA05C94F7L;
        uint8_t **l_2185 = &g_1564;
        int32_t l_2232 = (-1L);
        int32_t ***l_2265[2];
        uint64_t **l_2276 = &l_1067[0];
        uint64_t ***l_2275 = &l_2276;
        int32_t *l_2286 = &l_1794;
        int i;
        for (i = 0; i < 2; i++)
            l_2265[i] = (void*)0;
        for (l_1455 = 1; (l_1455 >= 0); l_1455 -= 1)
        { /* block id: 938 */
            int8_t **l_2175[6][9] = {{&l_1917,&g_733,&g_733,&g_733,&g_733,&l_1917,&g_733,(void*)0,&g_733},{&l_1917,&l_1917,&l_1917,&g_733,&l_1917,&l_1917,&g_733,&g_733,&g_733},{&l_1917,(void*)0,(void*)0,&g_733,(void*)0,&g_733,&g_733,&l_1917,&g_733},{&l_1917,&g_733,&g_733,&g_733,&g_733,&l_1917,&g_733,(void*)0,&g_733},{&l_1917,&l_1917,&l_1917,&g_733,&l_1917,&l_1917,&g_733,&g_733,&g_733},{&l_1917,(void*)0,(void*)0,&g_733,(void*)0,&g_733,&g_733,&l_1917,&g_733}};
            const uint8_t **l_2187 = &g_591;
            int32_t l_2222[9][5][5] = {{{1L,0xC2B654B8L,(-1L),(-1L),0x725D7DB6L},{(-1L),0x3F2F8799L,1L,(-6L),1L},{0x220481F4L,0x7D840F79L,0xE3CD9A95L,(-6L),0x44E1A4F3L},{0L,0xE0C2B442L,(-1L),(-1L),0L},{3L,0xE2475EB0L,0x4BDF7578L,0x27FDDB34L,(-10L)}},{{0x78F43B4EL,(-7L),3L,1L,0L},{(-1L),0x3F2F8799L,1L,(-10L),(-6L)},{0L,0x969DB6F5L,3L,(-9L),0x9B37302FL},{1L,1L,0x3BEE6DFDL,0xE2475EB0L,(-1L)},{(-1L),0xB4B51D77L,(-1L),(-1L),0xB4B51D77L}},{{9L,0x969DB6F5L,(-1L),0xB4B51D77L,(-1L)},{0xD53A5143L,0x78F43B4EL,1L,(-1L),0x671C2B73L},{0x0582FA8CL,(-1L),(-1L),3L,0x0D160BECL},{0xD53A5143L,0L,0x5480DB6AL,(-1L),3L},{9L,0xCF2701B7L,0xCF1C24E5L,1L,6L}},{{(-1L),(-6L),1L,0x5B34F9A5L,0xE2475EB0L},{1L,0x2180F6B8L,(-1L),1L,3L},{0L,1L,0xE0C2B442L,0xE2475EB0L,(-1L)},{(-1L),0x0445351EL,3L,0x290F808FL,(-1L)},{0x78F43B4EL,9L,(-1L),1L,(-1L)}},{{(-1L),0xB4B51D77L,(-9L),0x78F43B4EL,0xD808E930L},{7L,(-9L),0xE3CD9A95L,0x5B34F9A5L,(-1L)},{0xD53A5143L,(-1L),0xE3CD9A95L,0x4BDF7578L,0x9B37302FL},{(-1L),(-7L),(-9L),0L,0x0D160BECL},{1L,(-6L),(-1L),3L,(-1L)}},{{0L,0L,3L,1L,(-10L)},{0L,0x0D160BECL,0xE0C2B442L,0xD808E930L,1L},{0x2A9F406DL,(-1L),(-1L),(-1L),9L},{(-1L),(-1L),1L,1L,(-2L)},{0x671C2B73L,0x0445351EL,0xCF1C24E5L,0x0445351EL,0x671C2B73L}},{{0x2A9F406DL,1L,0x5480DB6AL,(-10L),(-1L)},{(-1L),9L,(-1L),0x3F2F8799L,0x5B34F9A5L},{9L,0xD53A5143L,1L,1L,(-1L)},{1L,0x3F2F8799L,(-1L),0x5DF42862L,0x671C2B73L},{(-1L),0x2180F6B8L,(-1L),0L,(-2L)}},{{(-9L),0x0D160BECL,0x3BEE6DFDL,(-1L),9L},{7L,0xF3A2F4B6L,3L,0x5DF42862L,1L},{0x9E207D71L,0L,1L,0xB4B51D77L,(-10L)},{0x3F2F8799L,(-1L),3L,0x78F43B4EL,(-1L)},{(-1L),1L,0x969DB6F5L,(-10L),0x0D160BECL}},{{0x671C2B73L,0x969DB6F5L,4L,0xD53A5143L,0x9B37302FL},{0x78F43B4EL,0xD808E930L,0x3BEE6DFDL,6L,(-1L)},{(-1L),0xD808E930L,0x2A9F406DL,(-1L),0xD808E930L},{0x48B6755AL,0L,0x44E1A4F3L,0x2A9F406DL,0x220481F4L},{1L,0x0582FA8CL,0L,0x969DB6F5L,0x8E7FD0D8L}}};
            uint16_t ** const l_2231 = &g_1169;
            int i, j, k;
            for (g_1032 = 0; (g_1032 <= 1); g_1032 += 1)
            { /* block id: 941 */
                int32_t *l_2155 = &g_235;
                int32_t *l_2156 = &l_1782;
                int32_t *l_2157 = &l_1784;
                int32_t *l_2158 = &l_1453;
                int32_t *l_2159 = &l_1784;
                int32_t *l_2160 = &l_1782;
                int32_t *l_2161[10] = {&g_11,&l_1784,&g_11,&l_1784,&g_11,&l_1784,&g_11,&l_1784,&g_11,&l_1784};
                uint64_t l_2166 = 0xB2EF5991467784E2LL;
                volatile int32_t * const l_2190 = &g_2191[3];
                volatile int32_t * const * volatile l_2189 = &l_2190;/* VOLATILE GLOBAL l_2189 */
                int i;
                ++l_2162;
                for (g_11 = 0; (g_11 <= 1); g_11 += 1)
                { /* block id: 945 */
                    uint8_t l_2169 = 0UL;
                    float *l_2172 = &g_623;
                    l_2166--;
                    l_2169--;
                    (*l_2172) = 0x0.Ep-1;
                    for (l_1505 = 0; (l_1505 <= 2); l_1505 += 1)
                    { /* block id: 951 */
                        uint8_t ***l_2186[3];
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_2186[i] = &g_1563;
                        if (g_213[(g_1032 + 1)][(l_1505 + 3)])
                            break;
                        (**g_1868) = func_39(((((((safe_div_func_uint16_t_u_u((g_851[l_1455][l_1455][l_1455] , ((l_2175[1][8] != (*g_1676)) && ((~((*g_555) , g_2177)) < (safe_lshift_func_uint8_t_u_s((l_2165 = (safe_sub_func_int32_t_s_s(p_5, ((safe_sub_func_uint64_t_u_u((!l_2162), ((l_2185 = l_2185) == l_2187))) , l_2165)))), 7))))), 1UL)) == 255UL) <= g_1907[1]) | (**g_1563)) , l_2165) && (-1L)));
                        l_2189 = g_2188;
                        if ((**g_1514))
                            break;
                    }
                }
            }
            for (g_221 = 0; (g_221 <= 1); g_221 += 1)
            { /* block id: 963 */
                int32_t **l_2197 = (void*)0;
                int32_t l_2220 = 0x7DDEEAEAL;
                const uint8_t ***l_2241 = &g_590;
                for (g_594.f1 = 0; (g_594.f1 <= 3); g_594.f1 += 1)
                { /* block id: 966 */
                    int32_t *l_2195 = &g_1016;
                    int32_t * const *l_2194 = &l_2195;
                    int32_t * const **l_2196 = &l_2194;
                    uint16_t *l_2198 = &g_73;
                    int32_t l_2210 = 0xCCE46845L;
                    uint16_t *l_2213 = &g_2214[1];
                    uint8_t *** const l_2243 = (void*)0;
                    int i, j;
                }
                if ((*g_639))
                    continue;
                return (*g_615);
            }
            if (l_2222[7][2][2])
                break;
            if (l_2245[1][6])
                continue;
            for (l_1850 = 0; (l_1850 <= 1); l_1850 += 1)
            { /* block id: 994 */
                uint64_t l_2272[3][4][1] = {{{0x068D818482A38971LL},{0x068D818482A38971LL},{18446744073709551614UL},{0x068D818482A38971LL}},{{0x068D818482A38971LL},{18446744073709551614UL},{0x068D818482A38971LL},{0x068D818482A38971LL}},{{18446744073709551614UL},{0x068D818482A38971LL},{0x068D818482A38971LL},{18446744073709551614UL}}};
                int i, j, k;
                for (g_7 = 0; (g_7 <= 1); g_7 += 1)
                { /* block id: 997 */
                    uint32_t *l_2250 = &l_1909;
                    uint32_t *l_2259 = &g_412;
                    int32_t *l_2264[8][3][9] = {{{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0,&g_1907[0],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
                    int32_t **l_2263 = &l_2264[6][2][7];
                    int32_t ***l_2262[9] = {&l_2263,&l_2263,&l_2263,&l_2263,&l_2263,&l_2263,&l_2263,&l_2263,&l_2263};
                    int32_t ****l_2266[3][5];
                    int16_t *l_2270 = &g_282[1][3][2].f2;
                    int16_t *l_2271[6][9][4] = {{{&g_1448.f2,&g_66.f2,&g_112.f2,&g_66.f2},{&g_112.f2,&g_1448.f2,&g_200.f2,(void*)0},{&g_594.f2,&g_2078,&g_2078,&g_2078},{&g_2078,&g_2078,&g_2078,&g_594.f2},{(void*)0,&g_200.f2,&g_1448.f2,&g_112.f2},{&g_66.f2,&g_112.f2,&g_66.f2,&g_1448.f2},{&g_1032,&g_112.f2,&g_200.f2,&g_112.f2},{&g_112.f2,&g_200.f2,&g_1032,&g_594.f2},{&l_14,&g_2078,&g_1448.f2,&g_2078}},{{(void*)0,&g_2078,&g_112.f2,(void*)0},{&g_2078,&g_1448.f2,(void*)0,&g_66.f2},{&g_1448.f2,&g_66.f2,&g_594.f2,&g_1032},{&g_1448.f2,&g_200.f2,(void*)0,&g_112.f2},{&g_2078,&g_1032,&g_112.f2,&l_14},{(void*)0,&g_1448.f2,&g_1448.f2,(void*)0},{&l_14,&g_112.f2,&g_1032,&g_2078},{&g_112.f2,(void*)0,&g_200.f2,&g_1448.f2},{&g_1032,&g_594.f2,&g_66.f2,&g_1448.f2}},{{&g_66.f2,(void*)0,&g_1448.f2,&g_2078},{(void*)0,&g_112.f2,&g_2078,(void*)0},{&g_2078,&g_1448.f2,&g_2078,&l_14},{&g_594.f2,&g_1032,&g_200.f2,&g_112.f2},{&g_112.f2,&g_200.f2,&g_112.f2,&g_1032},{&g_1448.f2,&g_66.f2,&g_112.f2,&g_66.f2},{&g_112.f2,&g_1448.f2,&g_200.f2,(void*)0},{&g_594.f2,&g_2078,&g_2078,&g_2078},{&g_2078,&g_2078,&g_2078,&g_594.f2}},{{(void*)0,&g_200.f2,&g_1448.f2,&g_112.f2},{&g_66.f2,&g_112.f2,&g_66.f2,&g_1448.f2},{&g_1032,&g_112.f2,&g_200.f2,&g_112.f2},{&g_112.f2,&g_200.f2,&g_1032,&g_594.f2},{&l_14,&g_1448.f2,&l_14,&g_1448.f2},{&g_112.f2,&g_594.f2,&g_1448.f2,(void*)0},{&g_1032,&g_200.f2,&g_112.f2,(void*)0},{&l_14,&g_2078,&g_66.f2,&l_14},{&l_14,&g_594.f2,&g_112.f2,&g_2078}},{{&g_1032,&l_14,&g_1448.f2,&g_1448.f2},{&g_112.f2,&l_14,&l_14,&g_112.f2},{&g_1448.f2,&g_1448.f2,&l_14,&g_1032},{&g_2078,&g_112.f2,&g_594.f2,&l_14},{&l_14,&g_66.f2,&g_2078,&l_14},{(void*)0,&g_112.f2,&g_200.f2,&g_1032},{(void*)0,&g_1448.f2,&g_594.f2,&g_112.f2},{&g_1448.f2,&l_14,&g_1448.f2,&g_1448.f2},{&g_2078,&l_14,&g_66.f2,&g_2078}},{{&g_1448.f2,&g_594.f2,&g_2078,&l_14},{&g_200.f2,&g_2078,&g_2078,(void*)0},{&g_1448.f2,&g_200.f2,&g_66.f2,(void*)0},{&g_2078,&g_594.f2,&g_1448.f2,&g_1448.f2},{&g_1448.f2,&g_1448.f2,&g_594.f2,&g_2078},{(void*)0,&g_66.f2,&g_200.f2,&g_1448.f2},{(void*)0,&g_2078,&g_2078,&g_200.f2},{&l_14,&g_2078,&g_594.f2,&g_1448.f2},{&g_2078,&g_66.f2,&l_14,&g_2078}}};
                    uint64_t *l_2280 = (void*)0;
                    uint64_t ** const l_2279 = &l_2280;
                    uint64_t ** const *l_2278 = &l_2279;
                    uint64_t ** const **l_2277 = &l_2278;
                    int32_t l_2281 = 0xDF061A98L;
                    int32_t l_2282 = 0x3BB6E2B1L;
                    float *l_2283 = &g_1368;
                    float *l_2284 = &g_626;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_2266[i][j] = &l_2265[1];
                    }
                    (*l_2284) = (((*l_2283) = (((((l_2222[7][2][2] = ((safe_sub_func_uint8_t_u_u(((((l_2165 = (((safe_mod_func_int16_t_s_s((((l_2250 = (void*)0) == (void*)0) && ((**g_1387)--)), ((+(safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((!(((((*l_2259)--) & 1L) , (l_2262[7] == (g_2267[0][5] = l_2265[1]))) <= (l_2272[0][2][0] = ((*l_2270) = 1L)))), (safe_lshift_func_uint16_t_u_u(((l_2275 == ((*l_2277) = &l_2276)) == l_2222[4][2][3]), g_587.f0)))), 3L))) , l_2281))) || l_2281) <= l_2282)) || 0L) , &l_1795[0]) == (void*)0), p_5)) & l_2222[7][2][2])) ^ (*l_36)) , 65535UL) , p_5) != p_5)) < (-0x1.0p-1));
                    (*l_36) |= l_2285;
                    l_2286 = (void*)0;
                }
                (**g_1868) = &l_2232;
            }
        }
        if (l_2165)
            break;
    }
    for (g_594.f1 = (-15); (g_594.f1 < (-9)); g_594.f1 = safe_add_func_uint8_t_u_u(g_594.f1, 1))
    { /* block id: 1019 */
        uint16_t l_2293 = 65535UL;
        int32_t l_2305 = 0xD2B09BC0L;
        int32_t ***l_2308 = &g_2268;
        int32_t l_2312 = (-1L);
        int32_t *** const l_2316 = &g_2268;
        int32_t l_2338[3][1];
        uint8_t * const *l_2371 = &g_1564;
        uint8_t * const **l_2370[4][6][10] = {{{(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371},{(void*)0,(void*)0,(void*)0,&l_2371,(void*)0,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371},{(void*)0,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371,(void*)0,(void*)0,&l_2371,&l_2371},{(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371},{(void*)0,(void*)0,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371},{(void*)0,&l_2371,&l_2371,(void*)0,&l_2371,(void*)0,(void*)0,(void*)0,&l_2371,&l_2371}},{{&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371},{&l_2371,(void*)0,&l_2371,&l_2371,(void*)0,(void*)0,&l_2371,(void*)0,(void*)0,&l_2371},{(void*)0,&l_2371,(void*)0,(void*)0,&l_2371,&l_2371,(void*)0,&l_2371,(void*)0,&l_2371},{(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0},{&l_2371,(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371},{&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371}},{{&l_2371,&l_2371,&l_2371,(void*)0,(void*)0,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371},{&l_2371,(void*)0,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0},{&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371},{&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371},{&l_2371,(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371},{&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371}},{{&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,(void*)0},{&l_2371,(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371},{&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371},{&l_2371,&l_2371,&l_2371,(void*)0,(void*)0,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371},{&l_2371,(void*)0,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0},{&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,&l_2371,(void*)0,&l_2371,&l_2371,&l_2371}}};
        uint8_t * const ***l_2369 = &l_2370[0][1][0];
        uint16_t l_2374[5][8] = {{65531UL,65531UL,0x4B59L,65531UL,65531UL,0x4B59L,65531UL,65531UL},{0UL,65531UL,0UL,0UL,65531UL,0UL,0UL,65531UL},{65531UL,0UL,0UL,65531UL,0UL,0UL,65531UL,0UL},{65531UL,65531UL,0x4B59L,65531UL,65531UL,0x4B59L,65531UL,65531UL},{0UL,65531UL,0UL,0UL,65531UL,0UL,0UL,65531UL}};
        uint64_t *l_2391[3];
        uint64_t l_2433 = 18446744073709551610UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_2338[i][j] = 0xE127366CL;
        }
        for (i = 0; i < 3; i++)
            l_2391[i] = &g_867;
        for (g_106 = 0; (g_106 == 10); g_106 = safe_add_func_uint16_t_u_u(g_106, 6))
        { /* block id: 1022 */
            int32_t *l_2291 = (void*)0;
            int32_t *l_2309 = &l_1453;
            int32_t **l_2310[1][6];
            int16_t *l_2311[4][6][3] = {{{&g_1448.f2,&g_1448.f2,&g_1032},{&g_112.f2,(void*)0,&g_1032},{(void*)0,&g_1448.f2,(void*)0},{(void*)0,&g_200.f2,&g_1032},{(void*)0,&l_14,&l_14},{(void*)0,&l_14,&g_112.f2}},{{&g_1032,&g_200.f2,&g_1032},{(void*)0,&g_1032,&g_1032},{(void*)0,&g_2078,&g_112.f2},{(void*)0,&g_1032,&l_14},{(void*)0,&g_200.f2,&g_1032},{(void*)0,&l_14,&l_14}},{{(void*)0,&l_14,&g_112.f2},{&g_1032,&g_200.f2,&g_1032},{(void*)0,&g_1032,&g_1032},{(void*)0,&g_2078,&g_112.f2},{(void*)0,&g_1032,&l_14},{(void*)0,&g_200.f2,&g_1032}},{{(void*)0,&l_14,&l_14},{(void*)0,&l_14,&g_112.f2},{&g_1032,&g_200.f2,&g_1032},{(void*)0,&g_1032,&g_1032},{(void*)0,&g_2078,&g_112.f2},{(void*)0,&g_1032,&l_14}}};
            uint16_t l_2314 = 0xAE8AL;
            int32_t l_2335 = 1L;
            int32_t l_2336 = 6L;
            int32_t l_2337 = (-1L);
            int32_t l_2339 = (-3L);
            int32_t l_2340 = 4L;
            int64_t ***l_2366 = &g_2067;
            int32_t l_2407 = 0x26935EBDL;
            int8_t l_2422 = 0x88L;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 6; j++)
                    l_2310[i][j] = &g_2269;
            }
            l_2293 ^= ((*l_36) = 9L);
            if ((safe_lshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(((((*g_914) , (*g_639)) && (safe_unary_minus_func_uint8_t_u((*l_36)))) != ((l_2312 &= ((((*g_1564) = (((*g_1564) && 0x81L) && (safe_sub_func_int64_t_s_s((((*l_2308) = (((safe_mul_func_int8_t_s_s((l_2293 > l_2305), (((*l_1663)++) != (((*l_2309) = ((l_2308 == (void*)0) , 0xAC20B690L)) && g_86)))) & (*g_1388)) , (*l_2308))) != l_2310[0][5]), (*l_36))))) , 0x74L) >= p_5)) < 0x92BDL)), p_5)), p_5)), 11)))
            { /* block id: 1030 */
                int32_t ****l_2315 = &g_2267[2][3];
                int32_t l_2333 = 1L;
                int32_t l_2334[8][2];
                uint32_t l_2345[8];
                int i, j;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2334[i][j] = 5L;
                }
                for (i = 0; i < 8; i++)
                    l_2345[i] = 4294967295UL;
                if ((((p_5 , (((!(((*l_2315) = (l_2314 , &g_2268)) != l_2316)) , (l_2312 ^= (safe_sub_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(p_5, (safe_mul_func_int8_t_s_s(((*l_1917) = ((safe_lshift_func_int16_t_s_s((((p_5 || l_2293) ^ ((((*l_36) , (*g_914)) , p_5) || l_2293)) == p_5), 5)) || 0x5867C1BAL)), p_5)))), l_2305)) & p_5), p_5)))) < 0xF51E70DDAEA9D88DLL)) , 0UL) > g_1101[2]))
                { /* block id: 1034 */
                    int32_t *l_2327 = &l_1782;
                    int32_t *l_2328 = &l_1794;
                    int32_t *l_2329 = &l_1795[0];
                    int32_t *l_2330 = &l_1782;
                    int32_t *l_2331 = (void*)0;
                    int32_t *l_2332[8][9] = {{&l_2312,&l_1794,&l_1795[0],(void*)0,&l_2312,&l_1440,&l_1440,&l_2312,(void*)0},{&l_2312,&l_1794,&l_2312,&l_1440,&l_2285,&l_1795[0],&l_1795[0],&l_1795[0],&l_1795[0]},{&l_1795[0],&l_1794,&l_2312,&l_1794,&l_1795[0],(void*)0,&l_2312,&l_1440,&l_1440},{&l_1794,&l_2312,&l_1795[0],&l_1440,&l_1795[0],&l_2312,&l_1794,&l_2285,(void*)0},{&l_1795[0],(void*)0,&l_1794,(void*)0,&l_2285,(void*)0,&l_1794,(void*)0,&l_1795[0]},{&l_2312,(void*)0,&l_1795[0],&l_2285,&l_2312,&l_1795[0],&l_2312,&l_2285,&l_1795[0]},{&l_2312,&l_2312,&l_2312,&l_2312,(void*)0,&l_1440,&l_1795[0],&l_1440,(void*)0},{&l_2312,&l_2312,&l_2312,&l_2312,&l_2312,(void*)0,&l_1440,&l_1795[0],&l_1440}};
                    uint8_t l_2341 = 0x7FL;
                    int i, j;
                    l_2341++;
                    if ((*g_660))
                        break;
                }
                else
                { /* block id: 1037 */
                    int32_t *l_2344 = (void*)0;
                    l_2345[1]++;
                }
                for (l_2314 = 0; (l_2314 != 59); l_2314++)
                { /* block id: 1042 */
                    uint16_t *l_2358[8] = {&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73};
                    uint8_t ***l_2364 = &g_1563;
                    uint8_t ****l_2363 = &l_2364;
                    int64_t *l_2365 = &g_1796;
                    int i;
                    (*l_36) &= (safe_sub_func_int64_t_s_s(p_5, ((safe_mod_func_uint16_t_u_u(((*l_2309) &= ((safe_mul_func_int8_t_s_s((safe_rshift_func_int16_t_s_u((*g_678), 0)), l_2345[5])) || 0UL)), 0x40AFL)) , ((g_2214[1] = (l_2338[0][0] >= p_5)) , ((*l_2365) = (((safe_div_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u(((((*l_2363) = &g_1563) != &g_1563) <= (*g_860)), p_5)), 5UL)) == 0L) & p_5))))));
                }
                (*l_36) = (l_2366 == &g_1417);
                (**g_1868) = func_39(p_5);
            }
            else
            { /* block id: 1051 */
                uint32_t l_2375 = 0x2F0E3E5DL;
                int64_t **l_2376[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_2376[i] = &g_2068;
                (**g_1868) = (((((((l_2369 != ((safe_add_func_int32_t_s_s((*g_10), (l_2374[1][2] >= l_2375))) , (((void*)0 == l_2291) , (void*)0))) , l_2376[0]) != (void*)0) ^ l_2374[4][1]) && 0x01DAA50F3AF14530LL) && 0L) , &l_2338[1][0]);
                if ((*g_692))
                    break;
            }
            for (l_1791 = 0; (l_1791 <= 4); l_1791 += 1)
            { /* block id: 1057 */
                int32_t l_2414 = 0xD2D03CE6L;
                float l_2415[7][4][3] = {{{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1},{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1}},{{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1},{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1}},{{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1},{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1}},{{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1},{0x4.8p+1,0x4.8p+1,0x1.6p-1},{(-0x10.1p+1),(-0x10.1p+1),0x1.Dp+1}},{{0x4.8p+1,0xA.441DC6p-49,0x4.8p+1},{0x8.E0267Bp-94,0x8.E0267Bp-94,(-0x10.1p+1)},{0xA.441DC6p-49,0xA.441DC6p-49,0x4.8p+1},{0x8.E0267Bp-94,0x8.E0267Bp-94,(-0x10.1p+1)}},{{0xA.441DC6p-49,0xA.441DC6p-49,0x4.8p+1},{0x8.E0267Bp-94,0x8.E0267Bp-94,(-0x10.1p+1)},{0xA.441DC6p-49,0xA.441DC6p-49,0x4.8p+1},{0x8.E0267Bp-94,0x8.E0267Bp-94,(-0x10.1p+1)}},{{0xA.441DC6p-49,0xA.441DC6p-49,0x4.8p+1},{0x8.E0267Bp-94,0x8.E0267Bp-94,(-0x10.1p+1)},{0xA.441DC6p-49,0xA.441DC6p-49,0x4.8p+1},{0x8.E0267Bp-94,0x8.E0267Bp-94,(-0x10.1p+1)}}};
                uint64_t l_2432[5][10][3] = {{{0x20C02D52FDE45678LL,0x7773E01CE79CC74BLL,9UL},{7UL,0x365BAC7376792686LL,0xB6F90E6189789D39LL},{7UL,0xBBE4D72C246F7885LL,0x7F1AAEA77F77ABA3LL},{0x20C02D52FDE45678LL,18446744073709551615UL,0xBBE4D72C246F7885LL},{0x7F1AAEA77F77ABA3LL,0UL,18446744073709551613UL},{18446744073709551615UL,1UL,0UL},{0UL,6UL,1UL},{0xC7302E2E79B1B1AALL,9UL,9UL},{0x9ABD3FED3744BFA6LL,0x1B3E24991E664DC8LL,0UL},{0UL,0x1CE104FF39054066LL,6UL}},{{18446744073709551606UL,0x1E6A02F0E540FFDALL,0x7773E01CE79CC74BLL},{0x4ECD91283E48BC65LL,18446744073709551613UL,18446744073709551606UL},{1UL,0x1E6A02F0E540FFDALL,0x4ECD91283E48BC65LL},{9UL,0x1CE104FF39054066LL,0xE6BB217F6122C4B7LL},{1UL,0x1B3E24991E664DC8LL,0x7D7A735D2AE78425LL},{1UL,9UL,0x365BAC7376792686LL},{0xFF4630696D94C67DLL,6UL,1UL},{1UL,1UL,0xC7302E2E79B1B1AALL},{0xF60BD3985F41ADBDLL,0UL,0xC0F5CEA307EA2617LL},{0x1CE104FF39054066LL,18446744073709551615UL,0UL}},{{0x2B4542F755E60B62LL,0xBBE4D72C246F7885LL,0x08308BA8331FB7A7LL},{5UL,0x365BAC7376792686LL,0x08308BA8331FB7A7LL},{18446744073709551613UL,0x7773E01CE79CC74BLL,0UL},{18446744073709551606UL,0xFF4630696D94C67DLL,0xC0F5CEA307EA2617LL},{0x2F03C5DB0BBBE831LL,0x3B77F14F6A8209BDLL,0xC7302E2E79B1B1AALL},{0x2B8D43D43B926BEBLL,5UL,1UL},{0x7773E01CE79CC74BLL,0UL,0x365BAC7376792686LL},{0x7D7A735D2AE78425LL,1UL,0x7D7A735D2AE78425LL},{0UL,0x7D7A735D2AE78425LL,0xE6BB217F6122C4B7LL},{0x365BAC7376792686LL,0xF60BD3985F41ADBDLL,0x4ECD91283E48BC65LL}},{{0UL,9UL,18446744073709551606UL},{0xB6F90E6189789D39LL,0UL,0x7773E01CE79CC74BLL},{0UL,0xA82645E782EC1ACDLL,6UL},{0x365BAC7376792686LL,18446744073709551615UL,0UL},{0UL,1UL,9UL},{0x7D7A735D2AE78425LL,0x6F3B0CD7C50982C1LL,1UL},{0x7773E01CE79CC74BLL,0x9ABD3FED3744BFA6LL,0UL},{0x2B8D43D43B926BEBLL,0x2F03C5DB0BBBE831LL,18446744073709551613UL},{0x2F03C5DB0BBBE831LL,0x44D6A85C3116D5CCLL,0xBBE4D72C246F7885LL},{18446744073709551606UL,0xC0F5CEA307EA2617LL,0x7F1AAEA77F77ABA3LL}},{{18446744073709551613UL,0UL,0xB6F90E6189789D39LL},{5UL,0UL,9UL},{0x2B4542F755E60B62LL,0xC0F5CEA307EA2617LL,0x1E6A02F0E540FFDALL},{0x1CE104FF39054066LL,0x44D6A85C3116D5CCLL,0x6F3B0CD7C50982C1LL},{0xF60BD3985F41ADBDLL,0x2F03C5DB0BBBE831LL,0x3B925D46823EA306LL},{1UL,0x9ABD3FED3744BFA6LL,0x44D6A85C3116D5CCLL},{0xFF4630696D94C67DLL,0x6F3B0CD7C50982C1LL,18446744073709551615UL},{1UL,1UL,18446744073709551615UL},{1UL,18446744073709551615UL,18446744073709551613UL},{9UL,0xA82645E782EC1ACDLL,1UL}}};
                int i, j, k;
                for (l_1782 = 4; (l_1782 >= 0); l_1782 -= 1)
                { /* block id: 1060 */
                    uint8_t l_2399 = 255UL;
                    int32_t *l_2436 = (void*)0;
                }
                (*l_2309) = 0xE.26BA52p-88;
                for (g_86 = 0; (g_86 <= 4); g_86 += 1)
                { /* block id: 1084 */
                    uint64_t l_2449 = 0xE92E3F9150FAEB38LL;
                    (*l_2309) = ((safe_lshift_func_uint16_t_u_s((((0UL != 0x59FFCF1824A8C6A5LL) | (+(((**g_1727) |= (((+p_5) != 0x6CCB03629AA810D2LL) <= (**g_1563))) >= p_5))) != 0x66C9L), (((l_2432[0][6][1] && ((safe_mul_func_uint8_t_u_u((safe_add_func_int32_t_s_s((((safe_rshift_func_int16_t_s_u((((((*g_1385) != l_2447) , p_5) , 0L) >= 0x00168AA2L), p_5)) | p_5) < l_2432[0][5][2]), l_2449)), l_2450)) != p_5)) != l_2433) < 0x000E2E4ED383DE46LL))) && p_5);
                }
            }
        }
    }
    return (*l_36);
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_11
 * writes: g_73 g_221 g_7 g_692 g_590 g_11
 */
static uint8_t  func_15(int32_t  p_16, const uint64_t  p_17, uint32_t * p_18, uint32_t  p_19, const int32_t  p_20)
{ /* block id: 482 */
    uint16_t *l_1069[7][4][2] = {{{&g_73,&g_73},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}},{{&g_73,(void*)0},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}},{{&g_73,(void*)0},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}},{{&g_73,(void*)0},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}},{{&g_73,(void*)0},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}},{{&g_73,(void*)0},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}},{{&g_73,(void*)0},{&g_73,&g_73},{&g_73,(void*)0},{&g_73,&g_73}}};
    int32_t l_1070 = 0x1EA6F840L;
    int32_t **l_1073 = &g_692;
    int32_t **l_1074 = (void*)0;
    int32_t **l_1075 = &g_692;
    uint8_t l_1120 = 0UL;
    int32_t l_1341 = 0xF7C4FD22L;
    int32_t l_1345 = 0x140ACBA8L;
    int32_t l_1360 = 7L;
    int32_t l_1361 = 0x085AEE15L;
    int32_t l_1362 = 0x5964A95DL;
    uint64_t * const * const **l_1389[8] = {&g_1386[6],&g_1386[6],&g_1386[6],&g_1386[6],&g_1386[6],&g_1386[6],&g_1386[6],&g_1386[6]};
    uint32_t l_1412 = 0x762EDDEFL;
    int i, j, k;
    (*l_1073) = func_39((l_1070 &= (g_221 = (--g_73))));
    (*l_1073) = &p_16;
    g_590 = &g_591;
    for (g_11 = 0; (g_11 != 5); g_11++)
    { /* block id: 492 */
        float *l_1085 = (void*)0;
        const int32_t l_1086 = 0x5DE2D19CL;
        uint16_t l_1099 = 0x46EEL;
        uint8_t *l_1100 = &g_1101[2];
        int32_t l_1102 = 0x40122B1AL;
        int16_t l_1103 = 0x20F2L;
        uint16_t l_1113 = 1UL;
        uint8_t l_1121 = 0x0DL;
        int32_t l_1177[10] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
        uint64_t l_1178 = 0xFCBB1AF3C2DEC7E3LL;
        int32_t *l_1195 = (void*)0;
        uint32_t *l_1286[8][8][4] = {{{(void*)0,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,&g_121},{&g_121,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121}},{{&g_121,(void*)0,(void*)0,&g_121},{(void*)0,&g_121,&g_121,&g_121},{(void*)0,&g_121,&g_121,&g_121},{&g_121,(void*)0,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121}},{{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,(void*)0,&g_121},{(void*)0,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,(void*)0},{&g_121,(void*)0,&g_121,&g_121}},{{&g_121,(void*)0,(void*)0,&g_121},{&g_121,(void*)0,&g_121,&g_121},{(void*)0,(void*)0,&g_121,(void*)0},{(void*)0,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,(void*)0,&g_121},{&g_121,&g_121,&g_121,(void*)0},{&g_121,(void*)0,&g_121,(void*)0}},{{&g_121,&g_121,&g_121,(void*)0},{&g_121,(void*)0,(void*)0,&g_121},{&g_121,&g_121,&g_121,(void*)0},{&g_121,&g_121,&g_121,&g_121},{(void*)0,&g_121,&g_121,(void*)0},{(void*)0,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,(void*)0}},{{(void*)0,&g_121,&g_121,&g_121},{(void*)0,&g_121,(void*)0,(void*)0},{&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,(void*)0},{(void*)0,&g_121,&g_121,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0},{(void*)0,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121}},{{&g_121,&g_121,(void*)0,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0},{(void*)0,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,&g_121},{&g_121,(void*)0,&g_121,&g_121},{(void*)0,&g_121,&g_121,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0},{&g_121,&g_121,&g_121,&g_121}},{{&g_121,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,(void*)0},{&g_121,(void*)0,&g_121,(void*)0},{&g_121,&g_121,&g_121,(void*)0},{&g_121,(void*)0,(void*)0,&g_121},{&g_121,&g_121,&g_121,(void*)0},{&g_121,&g_121,&g_121,&g_121},{(void*)0,&g_121,&g_121,(void*)0}}};
        uint32_t **l_1285 = &l_1286[3][1][2];
        uint32_t ***l_1284 = &l_1285;
        uint8_t l_1370 = 255UL;
        int i, j, k;
    }
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t  func_23(uint32_t * p_24, int8_t  p_25, const int32_t * const  p_26, int32_t * p_27)
{ /* block id: 479 */
    return p_25;
}


/* ------------------------------------------ */
/* 
 * reads : g_86 g_406 g_407 g_324 g_66.f2 g_106 g_658 g_660 g_11 g_235 g_7 g_590 g_591 g_692 g_112 g_112.f2 g_121 g_122 g_678 g_733 g_104 g_859 g_860 g_861 g_412 g_913
 * writes: g_86 g_112.f2 g_66.f2 g_122 g_7 g_11 g_200.f2 g_235 g_221 g_104 g_594.f1 g_200.f1 g_121 g_914
 */
static uint32_t * func_28(const uint32_t * p_29, int32_t  p_30, uint64_t  p_31)
{ /* block id: 244 */
    float * const l_644 = &g_108;
    uint64_t * const l_648 = &g_106;
    int32_t l_655 = (-1L);
    const uint8_t ** const l_671 = &g_591;
    int32_t l_743 = 1L;
    int32_t l_755 = 0L;
    int32_t l_757 = (-1L);
    int32_t l_760 = 0xB4717B7DL;
    int32_t l_762 = 0x966114D0L;
    int32_t l_764 = 0L;
    int32_t l_766 = 0x380B5C6DL;
    int32_t l_767 = 5L;
    int32_t l_768 = 0L;
    int32_t l_769 = 8L;
    int32_t l_770[2][3][10] = {{{0x0A46DF9EL,0x0A46DF9EL,0x965F5852L,0x0A46DF9EL,0x0A46DF9EL,0x965F5852L,0x0A46DF9EL,0x0A46DF9EL,0x965F5852L,0x0A46DF9EL},{0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL},{0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL}},{{0x0A46DF9EL,0x0A46DF9EL,0x965F5852L,0x0A46DF9EL,0x0A46DF9EL,0x965F5852L,0x0A46DF9EL,0x0A46DF9EL,0x965F5852L,0x0A46DF9EL},{0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL},{0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0x0A46DF9EL,0xEE812BBEL,0xEE812BBEL,0xEE812BBEL,0x965F5852L,0x965F5852L}}};
    int8_t * const **l_846 = (void*)0;
    int32_t *l_848 = &l_768;
    uint32_t *l_863 = (void*)0;
    uint32_t **l_862 = &l_863;
    volatile uint64_t *l_911 = (void*)0;
    volatile uint64_t ** volatile l_910 = &l_911;/* VOLATILE GLOBAL l_910 */
    int32_t l_958[2][7];
    uint64_t l_962 = 18446744073709551615UL;
    union U0 *l_1064 = (void*)0;
    uint32_t l_1065 = 0UL;
    uint32_t *l_1066[9] = {&g_122[7],&g_122[7],&g_122[7],&g_122[7],&g_122[7],&g_122[7],&g_122[7],&g_122[7],&g_122[7]};
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
            l_958[i][j] = 0L;
    }
    for (g_86 = 23; (g_86 < 26); g_86 = safe_add_func_int32_t_s_s(g_86, 9))
    { /* block id: 247 */
        uint8_t l_645 = 1UL;
        int16_t *l_653 = &g_112.f2;
        int16_t *l_654 = &g_66.f2;
        uint32_t *l_656 = &g_122[2];
        uint32_t *l_657 = &g_7;
        int32_t l_665 = (-6L);
        int32_t l_689 = 1L;
        int32_t l_690 = 0xAD896129L;
        uint64_t l_714 = 0xB55DFAFFE4F2B79DLL;
        int8_t **l_730[10];
        int32_t l_753 = 0L;
        int32_t l_754[1];
        uint64_t l_771 = 0x70BD17F6E7D1F7B0LL;
        int32_t l_820 = 0L;
        int i;
        for (i = 0; i < 10; i++)
            l_730[i] = (void*)0;
        for (i = 0; i < 1; i++)
            l_754[i] = (-2L);
        if ((((safe_div_func_int16_t_s_s(((((**g_406) != l_644) , l_645) | p_30), (safe_sub_func_uint32_t_u_u(((*l_657) = ((9UL ^ (&g_106 != (p_30 , l_648))) != ((*l_656) = (safe_div_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(((*l_654) |= ((*l_653) = 2L)), l_655)), l_645))))), g_106)))) , l_655) & 4L))
        { /* block id: 252 */
            uint64_t l_666 = 0x49F4265B5F8CA4A3LL;
            int8_t **l_734[2];
            uint32_t *l_736 = &g_412;
            int32_t l_737 = 1L;
            int32_t l_744 = 0x4B4B763EL;
            int32_t l_750[1];
            uint32_t *l_786 = &g_122[6];
            int i;
            for (i = 0; i < 2; i++)
                l_734[i] = &g_733;
            for (i = 0; i < 1; i++)
                l_750[i] = 0x66DA8208L;
            if (p_30)
                break;
            (*g_658) = l_655;
            for (g_200.f2 = 5; (g_200.f2 >= 0); g_200.f2 -= 1)
            { /* block id: 257 */
                int32_t *l_659 = (void*)0;
                int32_t l_661 = 6L;
                int32_t *l_662 = &l_661;
                int32_t *l_663 = (void*)0;
                int32_t *l_664[10] = {(void*)0,(void*)0,(void*)0,&g_235,&g_235,(void*)0,(void*)0,(void*)0,&g_235,&g_235};
                const int8_t *l_706 = &g_104;
                uint16_t l_735 = 0xD5F9L;
                uint32_t l_745 = 0xC7E02ACCL;
                int32_t l_765[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_765[i] = 0L;
                (*g_660) ^= p_30;
                l_666++;
                for (g_235 = 0; (g_235 >= 0); g_235 -= 1)
                { /* block id: 262 */
                    uint8_t *l_670 = &g_86;
                    uint8_t **l_669 = &l_670;
                    int32_t l_716[2];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_716[i] = 0x6DD4881FL;
                }
                for (p_31 = 0; (p_31 <= 5); p_31 += 1)
                { /* block id: 296 */
                    uint8_t l_739 = 0x3BL;
                    int32_t l_742 = 0x28B89C93L;
                    int32_t l_751 = (-6L);
                    int32_t l_752 = (-1L);
                    int32_t l_761[10];
                    int i;
                    for (i = 0; i < 10; i++)
                        l_761[i] = 0x79C12CCDL;
                    if (((*p_29) <= (l_689 = 0x782DF9F4L)))
                    { /* block id: 298 */
                        int16_t l_738 = 2L;
                        int32_t l_748 = 0xACA2C57DL;
                        int32_t l_749 = (-8L);
                        int32_t l_756 = 6L;
                        int32_t l_758 = (-1L);
                        int32_t l_759 = 0x1389D32BL;
                        int32_t l_763[10] = {0L,0x97E1E7C1L,0L,1L,1L,0L,0x97E1E7C1L,0L,1L,1L};
                        int i;
                        l_739--;
                        --l_745;
                        --l_771;
                    }
                    else
                    { /* block id: 302 */
                        uint16_t *l_776[7][9][3] = {{{(void*)0,(void*)0,&g_221},{(void*)0,&g_73,&l_735},{(void*)0,&g_221,(void*)0},{(void*)0,(void*)0,&g_221},{(void*)0,&g_221,&l_735},{(void*)0,&g_73,&g_221},{&g_73,(void*)0,&g_221},{&g_221,(void*)0,&g_221},{&l_735,&g_73,&l_735}},{{&l_735,&g_221,&g_221},{(void*)0,(void*)0,(void*)0},{&g_73,&g_221,&l_735},{&g_221,&g_221,&g_221},{&l_735,(void*)0,&l_735},{&l_735,&g_221,(void*)0},{&g_73,&g_73,&g_221},{&g_73,(void*)0,&g_73},{(void*)0,(void*)0,&g_73}},{{&g_73,&g_73,(void*)0},{&g_73,&g_221,&l_735},{&l_735,(void*)0,(void*)0},{&l_735,&g_221,&l_735},{&g_221,&g_73,&l_735},{&g_73,(void*)0,(void*)0},{(void*)0,&l_735,&l_735},{&l_735,&l_735,(void*)0},{&l_735,&g_221,&g_73}},{{&g_221,&g_73,&g_73},{&g_73,&g_221,&g_221},{(void*)0,&l_735,(void*)0},{(void*)0,&l_735,&l_735},{(void*)0,(void*)0,&g_221},{(void*)0,&g_73,&l_735},{(void*)0,&g_221,(void*)0},{(void*)0,(void*)0,&g_221},{(void*)0,&g_221,&l_735}},{{(void*)0,&g_73,&g_221},{&g_73,(void*)0,&g_221},{&g_221,(void*)0,&g_221},{&l_735,&g_73,&l_735},{&l_735,&g_221,&g_221},{(void*)0,(void*)0,(void*)0},{&g_73,&g_221,&l_735},{&g_221,&g_221,&g_221},{&l_735,(void*)0,&l_735}},{{&l_735,&g_221,(void*)0},{&g_73,&g_73,&g_221},{&g_73,(void*)0,&g_73},{(void*)0,(void*)0,&g_73},{&g_73,&g_73,(void*)0},{&g_73,&g_221,&l_735},{&l_735,(void*)0,(void*)0},{&l_735,&g_221,&g_221},{(void*)0,(void*)0,&g_221}},{{&g_73,&l_735,(void*)0},{&l_735,&g_73,&g_221},{&g_221,&g_221,&l_735},{&l_735,&l_735,&g_73},{&l_735,&g_73,&g_73},{&g_73,&l_735,&g_221},{&g_73,&g_221,&g_73},{(void*)0,&g_73,&g_73},{&g_73,&l_735,(void*)0}}};
                        int8_t l_779 = 0x9CL;
                        int i, j, k;
                        p_30 ^= p_31;
                        (*g_692) |= ((safe_mul_func_int16_t_s_s(((g_221 = (l_760 &= ((*p_29) == (*p_29)))) <= (safe_mul_func_uint8_t_u_u(0x8AL, l_779))), ((safe_mod_func_uint8_t_u_u(((safe_mul_func_int64_t_s_s(0xA8B038DAAFE18497LL, (&l_739 == (*g_590)))) & ((safe_rshift_func_int8_t_s_u((p_29 == p_29), l_761[6])) >= l_739)), 6L)) == p_31))) < 0x67L);
                        if (p_31)
                            break;
                    }
                    return l_786;
                }
            }
            (*g_692) = l_737;
        }
        else
        { /* block id: 313 */
            uint16_t *l_819 = &g_221;
            int32_t *l_821 = &l_689;
            (*l_821) |= ((((l_743 || 0x0F38962CL) && p_30) | (~((*l_654) = (safe_mod_func_uint32_t_u_u(((+(safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(((*g_733) |= (g_112 , ((safe_unary_minus_func_int8_t_s(((~(((safe_mod_func_int32_t_s_s(((safe_add_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(((p_30 || (safe_mod_func_int32_t_s_s((safe_add_func_uint16_t_u_u(0x8E70L, ((*l_819) = (safe_mod_func_uint64_t_u_u((safe_div_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((safe_mod_func_int8_t_s_s((((safe_unary_minus_func_uint32_t_u((g_112.f2 || ((!((5L <= (safe_div_func_uint64_t_u_u(l_714, g_121))) == g_122[0])) , l_764)))) < g_66.f2) , (-1L)), p_31)), p_31)), p_30)), p_31))))), l_820))) & g_235), (*g_678))), g_86)) == (*p_29)), (*p_29))) & p_31) | 0x1A4AL)) < 0L))) != l_743))), p_31)), p_30))) == 7L), 4294967294UL))))) < 0xC7ABL);
            (*l_821) = ((*g_692) = 1L);
            return &g_412;
        }
    }
    for (g_7 = 1; (g_7 > 38); g_7++)
    { /* block id: 325 */
        int32_t l_837 = 0xB280DF48L;
        int32_t l_838 = (-1L);
        int32_t l_841 = 1L;
        float l_850 = 0x0.Fp+1;
        uint64_t *l_866 = &g_867;
        float *l_878 = &g_628;
        int64_t l_950 = 0xEF4D02C5A81F7B00LL;
        int32_t l_951 = 0x2EE28E55L;
        uint64_t l_954 = 0x44508AAA6943C3D6LL;
        int32_t l_959 = 0x5876F491L;
        int32_t l_960 = 1L;
        int32_t l_961 = 1L;
        uint64_t ***l_1059 = (void*)0;
        union U0 *l_1062 = &g_112;
        union U0 **l_1063[9] = {&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914};
        int i;
        for (g_594.f1 = 0; (g_594.f1 <= 1); g_594.f1 += 1)
        { /* block id: 328 */
            int32_t l_839 = 0xE833521AL;
            int32_t l_840 = 0x3A7CBC14L;
            int32_t l_842[3];
            int32_t *l_847 = &l_655;
            uint8_t *l_897[3][9];
            uint16_t *l_903 = &g_73;
            union U0 *l_912[4][3][9] = {{{&g_66,&g_112,&g_282[1][3][2],&g_594,(void*)0,(void*)0,&g_594,&g_282[1][3][2],&g_112},{&g_200,&g_594,&g_112,&g_66,&g_282[1][3][2],&g_200,&g_200,&g_112,&g_282[0][0][6]},{(void*)0,(void*)0,&g_112,&g_594,&g_594,&g_112,&g_112,&g_594,&g_594}},{{&g_282[1][3][2],&g_594,&g_112,&g_66,&g_66,&g_112,&g_594,&g_282[1][3][2],&g_594},{&g_66,&g_112,&g_594,&g_282[1][3][2],&g_282[1][3][2],&g_66,(void*)0,&g_66,&g_282[1][3][2]},{(void*)0,&g_200,&g_282[0][0][6],&g_282[0][4][7],(void*)0,&g_112,&g_594,&g_282[0][0][6],&g_594}},{{&g_594,&g_282[1][4][1],&g_282[0][3][6],&g_282[0][3][6],&g_282[1][4][1],&g_594,&g_282[1][3][2],&g_112,&g_594},{&g_112,(void*)0,&g_282[0][4][7],&g_282[0][0][6],&g_200,(void*)0,(void*)0,&g_66,&g_282[0][0][6]},{&g_66,&g_282[1][3][2],&g_282[1][3][2],&g_594,&g_112,&g_66,&g_282[1][3][2],&g_66,&g_112}},{{&g_112,&g_66,&g_66,&g_112,&g_594,&g_282[1][3][2],&g_594,&g_200,&g_594},{&g_112,&g_594,&g_594,&g_112,(void*)0,(void*)0,(void*)0,&g_594,&g_66},{&g_200,&g_282[1][3][2],&g_66,&g_112,&g_594,&g_200,&g_594,&g_112,&g_66}}};
            const uint8_t l_930 = 0UL;
            int32_t l_957 = 3L;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_842[i] = (-1L);
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 9; j++)
                    l_897[i][j] = &g_86;
            }
        }
        for (p_31 = 0; (p_31 == 21); ++p_31)
        { /* block id: 458 */
            uint64_t l_1043 = 0UL;
            for (g_200.f1 = 0; (g_200.f1 == 27); ++g_200.f1)
            { /* block id: 461 */
                int32_t *l_1037 = &l_959;
                int32_t *l_1039 = (void*)0;
                int32_t *l_1040 = (void*)0;
                int32_t *l_1041 = &g_235;
                int32_t *l_1042[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                const union U0 *l_1053[6] = {&g_594,&g_594,&g_112,&g_594,&g_594,&g_112};
                const union U0 **l_1052 = &l_1053[3];
                uint8_t *l_1056 = &g_86;
                int i;
                l_1043--;
                for (l_764 = 0; (l_764 != (-6)); l_764 = safe_sub_func_uint64_t_u_u(l_764, 3))
                { /* block id: 465 */
                    (*g_692) &= (p_30 >= 1UL);
                }
                if (p_31)
                    continue;
                (*g_692) &= (safe_mul_func_uint16_t_u_u(p_31, (safe_lshift_func_uint8_t_u_s(((((l_1052 == (void*)0) <= ((safe_add_func_uint8_t_u_u(((*l_1056) &= l_951), ((((**g_859) >= (safe_div_func_uint64_t_u_u((((0xB9929CECL && ((((l_1059 == ((g_121 = (safe_rshift_func_uint16_t_u_s((((void*)0 == &l_1056) >= p_31), g_11))) , (void*)0)) != (*p_29)) ^ 0L) | (-1L))) ^ 0x347D7D0D864C044ELL) | (-1L)), g_412))) || 0xBC9FEDFDL) , 8L))) == l_1043)) , l_961) != p_31), 1))));
            }
        }
        l_1064 = ((*g_913) = l_1062);
        if (l_1065)
            break;
    }
    return l_1066[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_11 g_639 g_235 g_112.f0
 * writes: g_235
 */
static int32_t  func_32(int32_t * const  p_33, uint16_t  p_34, int32_t * p_35)
{ /* block id: 241 */
    (*g_639) |= (*p_35);
    return g_112.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_7
 * writes: g_7
 */
static int32_t * func_39(uint16_t  p_40)
{ /* block id: 4 */
    int16_t l_621 = 0xB72FL;
    int32_t *l_631 = (void*)0;
    int32_t *l_632 = &g_235;
    int32_t *l_633[5][5][4] = {{{&g_11,&g_11,&g_11,&g_11},{&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_11},{&g_11,(void*)0,&g_235,&g_235},{&g_11,(void*)0,&g_11,&g_11}},{{(void*)0,&g_235,&g_11,&g_235},{&g_235,&g_235,&g_235,&g_11},{&g_11,&g_11,(void*)0,(void*)0},{&g_11,&g_11,&g_235,&g_235},{&g_11,&g_235,&g_11,&g_235}},{{&g_235,&g_235,&g_235,&g_235},{&g_11,(void*)0,&g_235,&g_11},{&g_235,&g_235,&g_11,&g_11},{&g_235,&g_11,&g_11,&g_11},{(void*)0,&g_235,(void*)0,&g_11}},{{&g_235,(void*)0,&g_235,&g_235},{&g_11,&g_235,&g_11,&g_235},{&g_11,&g_235,&g_11,&g_235},{&g_235,&g_11,&g_11,&g_11},{&g_11,(void*)0,&g_11,&g_11}},{{&g_11,&g_11,&g_235,&g_235},{&g_235,&g_235,(void*)0,(void*)0},{(void*)0,&g_235,&g_11,(void*)0},{&g_235,&g_235,&g_11,(void*)0},{&g_235,&g_235,&g_235,&g_235}}};
    int32_t l_634 = 0L;
    uint16_t l_635 = 65530UL;
    int i, j, k;
    for (g_7 = 0; (g_7 == 3); ++g_7)
    { /* block id: 7 */
        int32_t *l_45 = &g_11;
        uint32_t l_123 = 18446744073709551614UL;
        int64_t *l_124 = &g_112.f1;
        uint32_t *l_332 = (void*)0;
        uint32_t **l_331 = &l_332;
        float *l_620 = &g_108;
        float *l_622 = &g_623;
        float *l_624 = (void*)0;
        float *l_625 = &g_626;
        float *l_627[7][10][3] = {{{&g_628,&g_628,&g_628},{(void*)0,&g_628,(void*)0},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{&g_628,&g_628,&g_628},{(void*)0,&g_628,(void*)0},{&g_628,&g_628,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628}},{{&g_628,&g_628,(void*)0},{&g_628,&g_628,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{&g_628,&g_628,&g_628}},{{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628}},{{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{(void*)0,&g_628,(void*)0},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{&g_628,&g_628,&g_628},{(void*)0,&g_628,(void*)0}},{{&g_628,&g_628,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,(void*)0},{&g_628,(void*)0,(void*)0},{(void*)0,(void*)0,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{&g_628,(void*)0,&g_628}},{{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628},{(void*)0,(void*)0,&g_628},{&g_628,(void*)0,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,&g_628,(void*)0},{&g_628,&g_628,&g_628}},{{&g_628,(void*)0,&g_628},{&g_628,&g_628,&g_628},{(void*)0,&g_628,&g_628},{(void*)0,&g_628,&g_628},{&g_628,&g_628,&g_628},{&g_628,(void*)0,&g_628},{&g_628,(void*)0,&g_628},{&g_628,&g_628,(void*)0},{&g_628,&g_628,&g_628},{&g_628,&g_628,&g_628}}};
        int64_t **l_630 = &l_124;
        int64_t ***l_629 = &l_630;
        int i, j, k;
    }
    l_635++;
    return &g_11;
}


/* ------------------------------------------ */
/* 
 * reads : g_66.f2 g_112.f1 g_200 g_122 g_235 g_200.f2 g_104 g_86 g_406 g_221 g_213 g_412 g_112.f2 g_106 g_282.f0 g_153 g_11 g_200.f1 g_121 g_554 g_66.f1 g_282.f2 g_587 g_590 g_594 g_587.f0 g_594.f2 g_407
 * writes: g_66.f2 g_73 g_235 g_104 g_106 g_323 g_407 g_412 g_200.f2 g_122 g_86 g_200.f1 g_11 g_66.f1 g_152 g_108
 */
static uint32_t * func_46(uint32_t * p_47, int32_t * const  p_48, uint64_t  p_49)
{ /* block id: 132 */
    int8_t l_342[5] = {1L,1L,1L,1L,1L};
    uint8_t **l_366 = (void*)0;
    int32_t l_389 = 8L;
    int32_t l_400 = 0x2519BD88L;
    uint32_t *l_492 = &g_412;
    const int8_t l_495 = 1L;
    int32_t l_501 = 0x46A7C776L;
    int32_t l_502 = 1L;
    int32_t l_503 = 0x723A0C79L;
    int32_t l_504 = (-6L);
    int32_t l_505 = 0xCEF0461DL;
    int32_t l_506 = 0L;
    int32_t l_507 = 0L;
    int32_t l_508 = 7L;
    int32_t l_509 = 0x24F12A3BL;
    int32_t l_510[1];
    uint32_t *l_553 = (void*)0;
    uint32_t **l_552 = &l_553;
    uint16_t l_582 = 65535UL;
    float **l_607 = (void*)0;
    int i;
    for (i = 0; i < 1; i++)
        l_510[i] = 0x288B0B05L;
    for (g_66.f2 = 0; (g_66.f2 < 26); g_66.f2 = safe_add_func_int64_t_s_s(g_66.f2, 6))
    { /* block id: 135 */
        uint16_t *l_351 = &g_73;
        const uint8_t *l_359 = (void*)0;
        const uint8_t **l_358 = &l_359;
        int32_t l_360 = 0x6C08D800L;
        uint8_t l_401 = 0x7CL;
        int32_t l_433 = 0x47DCB6B8L;
        int32_t l_434 = 0x631843F4L;
        (*p_48) = (safe_unary_minus_func_uint32_t_u((safe_mod_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u(((safe_div_func_uint16_t_u_u(l_342[2], (safe_sub_func_int16_t_s_s(g_112.f1, ((safe_sub_func_int16_t_s_s((g_200 , (-1L)), ((*l_351) = (safe_add_func_uint8_t_u_u(p_49, l_342[2]))))) > (((safe_mul_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u(((1L > (safe_rshift_func_uint8_t_u_u(255UL, 3))) >= ((((l_358 != &l_359) && l_360) , 0x42A22D07L) , p_49)), (-1L))) == 0xD59CC582CEDBFFA5LL), p_49)) <= (*p_47)) == (*p_48))))))) > p_49), l_342[4])), g_200.f2))));
        if ((*p_48))
        { /* block id: 138 */
            uint8_t * const l_368 = &g_86;
            uint8_t * const *l_367 = &l_368;
            int8_t *l_373 = &g_104;
            uint64_t *l_384 = &g_106;
            int32_t l_387 = 0x97C8A839L;
            uint32_t *l_388[3];
            uint16_t l_428 = 0x9782L;
            int i;
            for (i = 0; i < 3; i++)
                l_388[i] = &g_121;
            if ((safe_rshift_func_int8_t_s_u((~((safe_sub_func_uint16_t_u_u(((*l_351) = ((l_366 = (void*)0) != l_367)), (safe_div_func_int8_t_s_s(((*l_373) |= (safe_lshift_func_uint8_t_u_u(l_360, l_342[2]))), (safe_sub_func_int64_t_s_s(l_360, 0L)))))) , ((((safe_sub_func_uint32_t_u_u(g_86, (safe_div_func_uint8_t_u_u(((safe_div_func_uint16_t_u_u(((l_389 &= (safe_add_func_int32_t_s_s(((((*l_384) = p_49) || (safe_div_func_uint32_t_u_u((l_360 & l_387), l_342[2]))) ^ (-1L)), (*p_48)))) , 65532UL), 0xAEB5L)) , 0x0EL), 6L)))) & 0UL) < p_49) & p_49))), 1)))
            { /* block id: 144 */
                uint16_t l_390 = 0x4265L;
                int32_t l_398 = (-4L);
                int32_t l_399[2][7] = {{(-9L),0x0256F460L,0x69E6BECDL,8L,8L,0x69E6BECDL,0x0256F460L},{(-9L),0x0256F460L,0x69E6BECDL,8L,8L,0x69E6BECDL,0x0256F460L}};
                int32_t *l_468 = &l_360;
                int32_t **l_469 = &l_468;
                int i, j;
                if (l_390)
                { /* block id: 145 */
                    int32_t *l_392 = &l_360;
                    int32_t **l_391 = &l_392;
                    (*l_391) = &g_235;
                }
                else
                { /* block id: 147 */
                    int32_t *l_393 = (void*)0;
                    int32_t *l_394 = &l_389;
                    int32_t *l_395 = &l_389;
                    int32_t *l_396 = &g_235;
                    int32_t *l_397[2];
                    float **l_404[1][2][8] = {{{&g_324,&g_324,&g_324,&g_324,&g_324,&g_324,&g_324,&g_324},{&g_324,&g_324,&g_324,&g_324,&g_324,&g_324,&g_324,&g_324}}};
                    float ***l_405 = &g_323;
                    uint32_t *l_411 = &g_412;
                    uint16_t l_413 = 0x71E6L;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_397[i] = &l_389;
                    l_401--;
                    (*g_406) = ((*l_405) = l_404[0][0][0]);
                    if ((((l_342[2] & (safe_mod_func_uint8_t_u_u(l_398, (l_360 = ((*p_47) || (&g_86 != (void*)0)))))) <= (((!(((*l_373) &= l_401) > (((*p_47) , ((*l_411) = l_398)) | ((0xE35E3FC0619C7830LL >= l_387) && 0L)))) | p_49) >= g_221)) <= l_413))
                    { /* block id: 154 */
                        int64_t *l_431 = (void*)0;
                        int16_t *l_432 = &g_200.f2;
                        uint64_t l_435[6][8] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x05AC7BDA4903D656LL,0xE1EA96E3C937039ELL,0xF68952115EF3ABFBLL,0xE9846EECE68CFEE6LL,18446744073709551615UL},{6UL,18446744073709551615UL,0UL,18446744073709551615UL,18446744073709551615UL,9UL,0xE1EA96E3C937039ELL,0xE1EA96E3C937039ELL},{6UL,0xE1EA96E3C937039ELL,0x1F59CEDBF0104374LL,0x1F59CEDBF0104374LL,0xE1EA96E3C937039ELL,6UL,18446744073709551615UL,0x515674DFA43F426DLL},{18446744073709551615UL,1UL,9UL,18446744073709551615UL,0UL,18446744073709551615UL,18446744073709551615UL,0UL},{18446744073709551615UL,0x515674DFA43F426DLL,18446744073709551615UL,18446744073709551615UL,0x80818782F0279FFELL,18446744073709551615UL,18446744073709551615UL,0x515674DFA43F426DLL},{18446744073709551615UL,0x80818782F0279FFELL,18446744073709551615UL,0x1F59CEDBF0104374LL,0UL,0x05AC7BDA4903D656LL,18446744073709551615UL,0xE1EA96E3C937039ELL}};
                        int i, j;
                        (*l_396) ^= (p_49 & ((*l_432) = (0xE766L < ((l_401 & (safe_sub_func_int16_t_s_s((safe_add_func_uint16_t_u_u(g_122[2], (((*l_384) = ((safe_add_func_int8_t_s_s(((0x76L || p_49) & (((safe_div_func_int32_t_s_s((~(safe_rshift_func_uint8_t_u_u((g_213[1][4] & (safe_unary_minus_func_int32_t_s((l_428 & (safe_add_func_int64_t_s_s((l_399[0][1] &= (p_49 && 0L)), 5L)))))), 7))), (-7L))) > l_400) > p_49)), (-5L))) | g_200.f2)) ^ g_112.f1))), g_122[2]))) & 0x0966FC5EL))));
                        ++l_435[5][6];
                    }
                    else
                    { /* block id: 160 */
                        int64_t *l_466[10] = {&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1,&g_282[1][3][2].f1};
                        int32_t l_467 = 0x6F50C885L;
                        int i;
                        (*l_396) |= (safe_add_func_uint64_t_u_u(((safe_add_func_uint8_t_u_u(g_213[0][4], ((safe_sub_func_int64_t_s_s(g_200.f2, (safe_mod_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((safe_mul_func_int16_t_s_s((g_412 && (safe_mod_func_uint32_t_u_u(g_412, ((safe_rshift_func_uint8_t_u_s(l_428, (safe_lshift_func_int16_t_s_u(g_66.f2, g_112.f2)))) & (((*p_47) = (safe_add_func_int16_t_s_s(0x68FAL, (safe_unary_minus_func_int16_t_s((safe_div_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((safe_add_func_int64_t_s_s((l_433 = (safe_unary_minus_func_int16_t_s((&g_121 == p_48)))), 0L)), l_399[0][1])), 4294967295UL))))))) == p_49))))), g_112.f2)), l_434)), l_428)))) || l_467))) <= p_49), (-1L)));
                    }
                }
                (*l_469) = l_468;
                return &g_122[0];
            }
            else
            { /* block id: 168 */
                return p_47;
            }
        }
        else
        { /* block id: 171 */
            int32_t *l_471[7] = {&l_433,(void*)0,(void*)0,&l_433,(void*)0,(void*)0,&l_433};
            int32_t **l_470 = &l_471[2];
            int i;
            (*l_470) = &l_360;
            (*l_470) = &g_11;
        }
    }
    if (((((safe_lshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_u((g_73 = (safe_rshift_func_int8_t_s_u((p_49 && (safe_add_func_uint64_t_u_u(p_49, ((0x6D6CFD9D4A11782BLL <= (safe_mul_func_int16_t_s_s((safe_div_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_s(l_342[2], ((safe_sub_func_uint64_t_u_u(((((safe_mul_func_int16_t_s_s(g_112.f2, 0x5BE1L)) & ((((*l_492) = ((*p_47) = 0x0E6010BBL)) <= ((safe_mod_func_uint8_t_u_u(g_200.f2, (g_86 = 0x90L))) <= 0xEFL)) | g_106)) >= g_200.f2) == 1UL), g_221)) & g_282[1][3][2].f0))), p_49)), (*p_48))), p_49))) != p_49)))), 5))), 10)), l_389)) && p_49) > l_495) , (*g_153)))
    { /* block id: 180 */
        int32_t *l_496 = &g_11;
        int32_t *l_497 = (void*)0;
        int32_t l_498 = 6L;
        int32_t *l_499 = &g_235;
        int32_t *l_500[5][3][1] = {{{(void*)0},{&l_389},{(void*)0}},{{&l_389},{(void*)0},{&l_389}},{{(void*)0},{&l_389},{(void*)0}},{{&l_389},{(void*)0},{&l_389}},{{(void*)0},{&l_389},{(void*)0}}};
        uint16_t l_511 = 3UL;
        int i, j, k;
        l_511--;
    }
    else
    { /* block id: 182 */
        uint32_t l_545 = 6UL;
        int32_t l_551[7][9][4] = {{{(-9L),0x0070319FL,0x54722D62L,0x3B71CA11L},{(-2L),8L,0xD6DD971EL,1L},{0x7B7E052AL,0x6B98D452L,0x96C10FC3L,0x770A7E69L},{0x634E4AA5L,1L,0x54F81EF6L,0x9C20DDC4L},{0x96C10FC3L,0L,0x0070319FL,3L},{0x0F8C35CBL,(-1L),0xEFC51182L,0xD6DD971EL},{0x770A7E69L,0x54F81EF6L,0L,0x54F81EF6L},{0x0070319FL,0L,0L,0L},{0xD6DD971EL,1L,0L,8L}},{{0x3B71CA11L,0L,1L,0xD433844AL},{0x3B71CA11L,2L,0L,0x12191CCBL},{0xD6DD971EL,0xD433844AL,0L,0xBAD35858L},{0x0070319FL,(-1L),0L,0x0F8C35CBL},{0x770A7E69L,0x634E4AA5L,0xEFC51182L,0x0070319FL},{0x0F8C35CBL,(-2L),0x0070319FL,0x7FAE190EL},{0x96C10FC3L,(-8L),0x54F81EF6L,4L},{0x634E4AA5L,0x4435A646L,0x96C10FC3L,0x54722D62L},{0x7B7E052AL,0xD6DD971EL,0xD6DD971EL,0x7B7E052AL}},{{(-2L),0xEFC51182L,0x54722D62L,0x4435A646L},{(-9L),0x7FAE190EL,1L,2L},{1L,0xBAD35858L,0x6B98D452L,2L},{(-1L),0x7FAE190EL,0x098B0D4BL,0x4435A646L},{4L,0xEFC51182L,0x9C20DDC4L,0x7B7E052AL},{8L,0xD6DD971EL,1L,0x54722D62L},{1L,0x4435A646L,0L,4L},{0L,(-8L),0xCDAF3530L,0x7FAE190EL},{(-1L),(-2L),0x770A7E69L,0x0070319FL}},{{1L,0x634E4AA5L,(-9L),0x0F8C35CBL},{0L,(-1L),0L,0xBAD35858L},{0xBAD35858L,0xD433844AL,0x7FAE190EL,0x12191CCBL},{0L,2L,5L,0xD433844AL},{3L,0L,5L,8L},{0L,1L,0x7FAE190EL,0L},{0xBAD35858L,0L,0L,0x54F81EF6L},{0L,0x54F81EF6L,(-9L),0xD6DD971EL},{1L,(-1L),0x770A7E69L,3L}},{{(-1L),0L,0xCDAF3530L,0x9C20DDC4L},{0L,1L,0L,0x770A7E69L},{1L,0x6B98D452L,1L,1L},{8L,8L,0x9C20DDC4L,0x3B71CA11L},{4L,0x0070319FL,0x098B0D4BL,0xEFC51182L},{(-1L),(-9L),0x6B98D452L,0x098B0D4BL},{1L,(-9L),1L,0xEFC51182L},{(-9L),0x0070319FL,0x54722D62L,0x3B71CA11L},{(-2L),8L,0xD6DD971EL,1L}},{{0x770A7E69L,0x62AB24DEL,(-2L),0x12191CCBL},{0L,1L,0x6B98D452L,0x0F8C35CBL},{(-2L),0L,1L,0xD433844AL},{0x54722D62L,0xD6DD971EL,0x098B0D4BL,1L},{0x12191CCBL,0x6B98D452L,3L,0x6B98D452L},{1L,3L,4L,0L},{1L,2L,1L,0x4435A646L},{0x634E4AA5L,4L,1L,0x96C10FC3L},{0x634E4AA5L,0x0070319FL,1L,8L}},{{1L,0x96C10FC3L,4L,5L},{1L,0x7B7E052AL,3L,0x54722D62L},{0x12191CCBL,0L,0x098B0D4BL,1L},{0x54722D62L,0L,1L,0xBAD35858L},{(-2L),0x54F81EF6L,0x6B98D452L,0xF02715E0L},{0L,1L,(-2L),(-8L)},{0x770A7E69L,1L,1L,0x770A7E69L},{0L,0x098B0D4BL,(-8L),1L},{(-1L),0xBAD35858L,0x7FAE190EL,0x0070319FL}}};
        int32_t *l_556 = (void*)0;
        int32_t *l_579 = &l_504;
        int32_t *l_580 = &g_235;
        int32_t *l_581[2][9] = {{&l_501,&l_400,&l_501,&g_11,(void*)0,(void*)0,&g_11,&l_501,&l_400},{&l_400,&l_501,&g_11,(void*)0,(void*)0,&g_11,&l_501,&l_400,&l_501}};
        float *l_603 = &g_108;
        int i, j, k;
        for (g_200.f1 = 0; (g_200.f1 <= 13); g_200.f1 = safe_add_func_uint16_t_u_u(g_200.f1, 5))
        { /* block id: 185 */
            int16_t l_548 = 0x36FEL;
            int32_t l_566 = 0x6220BE0AL;
            int32_t l_570 = 0xC51BBEC8L;
            if ((1UL > 0x8EA864731757C944LL))
            { /* block id: 186 */
                const int8_t *l_546 = (void*)0;
                const int32_t l_549 = (-4L);
                int32_t l_562 = 0x86DA8D54L;
                int32_t l_567[10][8][3] = {{{0xBCEC03A2L,(-1L),8L},{0xE79E4504L,1L,0xE79E4504L},{0xBCEC03A2L,0L,0xF65B01A7L},{0xF5EA2E80L,1L,0x035FF378L},{4L,(-1L),0xF65B01A7L},{0xE79E4504L,6L,0xE79E4504L},{4L,0L,8L},{0xF5EA2E80L,6L,0x035FF378L}},{{0xBCEC03A2L,(-1L),8L},{0xE79E4504L,1L,0xE79E4504L},{0xBCEC03A2L,0L,0xF65B01A7L},{0xF5EA2E80L,1L,0x035FF378L},{4L,(-1L),0xF65B01A7L},{0xE79E4504L,6L,0xE79E4504L},{4L,0L,8L},{0xF5EA2E80L,6L,0x035FF378L}},{{0xBCEC03A2L,(-1L),8L},{0xE79E4504L,1L,0xE79E4504L},{0xBCEC03A2L,0L,0xF65B01A7L},{0xF5EA2E80L,1L,0x035FF378L},{4L,(-1L),0xF65B01A7L},{0xE79E4504L,6L,0xE79E4504L},{4L,0L,8L},{0xF5EA2E80L,6L,0x035FF378L}},{{0xBCEC03A2L,(-1L),8L},{0xE79E4504L,1L,0xE79E4504L},{0xBCEC03A2L,0L,0xF65B01A7L},{0xF5EA2E80L,1L,0x035FF378L},{4L,(-1L),0xF65B01A7L},{0xE79E4504L,6L,0xE79E4504L},{4L,0L,8L},{0xF5EA2E80L,6L,0x035FF378L}},{{0xBCEC03A2L,(-1L),8L},{0xE79E4504L,0L,0x0687225EL},{1L,0xF65B01A7L,0x04EF3593L},{0L,0L,0x8CE44BD0L},{0xA89DE9E2L,0xBCEC03A2L,0x04EF3593L},{0x0687225EL,1L,0x0687225EL},{0xA89DE9E2L,0xF65B01A7L,(-3L)},{0L,1L,0x8CE44BD0L}},{{1L,0xBCEC03A2L,(-3L)},{0x0687225EL,0L,0x0687225EL},{1L,0xF65B01A7L,0x04EF3593L},{0L,0L,0x8CE44BD0L},{0xA89DE9E2L,0xBCEC03A2L,0x04EF3593L},{0x0687225EL,1L,0x0687225EL},{0xA89DE9E2L,0xF65B01A7L,(-3L)},{0L,1L,0x8CE44BD0L}},{{1L,0xBCEC03A2L,(-3L)},{0x0687225EL,0L,0x0687225EL},{1L,0xF65B01A7L,0x04EF3593L},{0L,0L,0x8CE44BD0L},{0xA89DE9E2L,0xBCEC03A2L,0x04EF3593L},{0x0687225EL,1L,0x0687225EL},{0xA89DE9E2L,0xF65B01A7L,(-3L)},{0L,1L,0x8CE44BD0L}},{{1L,0xBCEC03A2L,(-3L)},{0x0687225EL,0L,0x0687225EL},{1L,0xF65B01A7L,0x04EF3593L},{0L,0L,0x8CE44BD0L},{0xA89DE9E2L,0xBCEC03A2L,0x04EF3593L},{0x0687225EL,1L,0x0687225EL},{0xA89DE9E2L,0xF65B01A7L,(-3L)},{0L,1L,0x8CE44BD0L}},{{1L,0xBCEC03A2L,(-3L)},{0x0687225EL,0L,0x0687225EL},{1L,0xF65B01A7L,0x04EF3593L},{0L,0L,0x8CE44BD0L},{0xA89DE9E2L,0xBCEC03A2L,0x04EF3593L},{0x0687225EL,1L,0x0687225EL},{0xA89DE9E2L,0xF65B01A7L,(-3L)},{0L,1L,0x8CE44BD0L}},{{1L,0xBCEC03A2L,(-3L)},{0x0687225EL,0L,0x0687225EL},{1L,0xF65B01A7L,0x04EF3593L},{0L,0L,0x8CE44BD0L},{0xA89DE9E2L,0xBCEC03A2L,0x04EF3593L},{0x0687225EL,1L,0x0687225EL},{0xA89DE9E2L,0xF65B01A7L,(-3L)},{0L,1L,0x8CE44BD0L}}};
                int i, j, k;
                for (g_73 = (-25); (g_73 > 32); g_73 = safe_add_func_uint64_t_u_u(g_73, 4))
                { /* block id: 189 */
                    int32_t l_547 = (-3L);
                    int32_t l_550 = 0x74B78649L;
                    for (g_86 = 0; (g_86 < 8); g_86++)
                    { /* block id: 192 */
                        int32_t **l_520 = (void*)0;
                        int32_t *l_522 = (void*)0;
                        int32_t **l_521 = &l_522;
                        uint32_t *l_524 = &g_121;
                        uint32_t **l_523 = &l_524;
                        uint64_t *l_525[10];
                        int i;
                        for (i = 0; i < 10; i++)
                            l_525[i] = (void*)0;
                        (*l_521) = (g_86 , &g_11);
                        l_550 |= ((*l_522) = (((p_49 = (((*l_523) = p_47) == &g_213[4][5])) < ((((*l_492) = ((safe_div_func_uint8_t_u_u((~(safe_add_func_int64_t_s_s(g_121, (safe_rshift_func_int8_t_s_s((((safe_sub_func_int8_t_s_s((((safe_div_func_int64_t_s_s((((((safe_add_func_uint8_t_u_u(l_501, (safe_mod_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(((g_66.f2 , (safe_sub_func_int32_t_s_s((l_545 >= 0UL), (0xA0L != (((((((((*p_48) , l_546) != &g_104) , l_547) , g_213[0][4]) , l_548) > 1L) , g_213[0][4]) > l_548))))) != l_545), 0L)) < 5L), l_549)))) , g_213[2][0]) | l_548) < l_547) ^ g_200.f1), 0x1F8B2898107CF972LL)) & (**l_521)) , 0x54L), 0x61L)) || l_549) & g_122[3]), l_547))))), 0x1DL)) == 1L)) || (*p_48)) , l_545)) , (*g_153)));
                    }
                    if (l_551[3][0][0])
                        continue;
                }
                if ((*g_153))
                    continue;
                l_556 = (((p_49 >= ((g_106 , l_552) == g_554)) && g_122[3]) , &l_551[3][0][0]);
                for (g_66.f1 = 1; (g_66.f1 != (-23)); g_66.f1 = safe_sub_func_int64_t_s_s(g_66.f1, 6))
                { /* block id: 206 */
                    uint8_t l_561 = 0x94L;
                    int32_t l_565 = 0x38EF27D7L;
                    int32_t l_568 = 0x6FD0C9EEL;
                    int32_t l_569 = 0xE24FA462L;
                    int32_t l_571 = 0x2E6F3730L;
                    int32_t l_572 = 4L;
                    int32_t l_573 = 0xC7AA1577L;
                    int32_t l_574 = (-7L);
                    uint64_t l_575 = 18446744073709551607UL;
                    if (((p_49 < 0L) , (safe_div_func_int8_t_s_s(0L, l_561))))
                    { /* block id: 207 */
                        int32_t *l_563 = &l_506;
                        int32_t *l_564[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_564[i] = (void*)0;
                        --l_575;
                    }
                    else
                    { /* block id: 209 */
                        return l_492;
                    }
                }
            }
            else
            { /* block id: 213 */
                float l_578 = (-0x1.Cp-1);
                g_152 = &g_11;
                (*p_48) &= (g_282[1][3][2].f2 , l_506);
                return &g_122[5];
            }
        }
        --l_582;
        (*l_579) = (*p_48);
        (*l_603) = ((safe_sub_func_int8_t_s_s((g_587 , (safe_div_func_int8_t_s_s((((void*)0 == g_590) & (0xA549L || ((((0x10A7BF99D2271E9CLL > (safe_sub_func_int32_t_s_s(((g_594 , ((safe_div_func_int32_t_s_s((*p_48), (*p_48))) & ((safe_mul_func_uint8_t_u_u((safe_div_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_s(((l_508 && 0xA4L) <= p_49), 2)) != g_587.f0), p_49)), g_594.f2)) ^ g_106))) & 0x4FD9A4FE0EFC14EFLL), l_502))) , g_112.f2) >= 0x3853552CL) , 1UL))), 0xBDL))), p_49)) , 0x8.2E470Ap+13);
    }
    for (g_200.f1 = 0; (g_200.f1 >= 0); g_200.f1 -= 1)
    { /* block id: 225 */
        float **l_606 = &g_324;
        int32_t *l_608 = &l_505;
        int32_t *l_609 = (void*)0;
        int32_t l_610 = 0xA21C4B69L;
        int32_t *l_611[2][10][1] = {{{&l_506},{(void*)0},{&l_506},{(void*)0},{&l_506},{(void*)0},{&l_506},{(void*)0},{&l_506},{(void*)0}},{{&l_506},{(void*)0},{&l_506},{(void*)0},{&l_506},{(void*)0},{&l_506},{(void*)0},{&l_506},{(void*)0}}};
        uint32_t l_612 = 1UL;
        int i, j, k;
        l_552 = ((safe_lshift_func_int16_t_s_u(((*g_406) == (l_607 = l_606)), 13)) , &l_553);
        l_612++;
    }
    return &g_122[2];
}


/* ------------------------------------------ */
/* 
 * reads : g_104 g_128 g_11 g_213 g_7 g_106 g_221 g_121 g_122 g_200.f2
 * writes: g_104 g_11 g_66.f2 g_106 g_221 g_323 g_200.f2 g_122
 */
static uint32_t * func_50(int64_t  p_51, uint32_t  p_52)
{ /* block id: 22 */
    int64_t l_127 = 0x3A75C33281310918LL;
    int32_t l_150 = (-1L);
    const uint8_t *l_180 = (void*)0;
    int32_t l_210 = 0x22FFCB45L;
    int8_t l_241[4][9][7] = {{{0xDAL,0x4DL,0x29L,0x9EL,0x92L,0x92L,0x9EL},{0x12L,0L,0x12L,0x09L,0L,(-8L),9L},{0x29L,0x4DL,0xDAL,(-8L),0xDAL,0x4DL,0x29L},{0x93L,0L,9L,9L,(-8L),(-8L),(-8L)},{0x4DL,0xDDL,0xDDL,0xDDL,0x15L,0xDAL,0x29L},{0xACL,(-8L),0x12L,0L,(-1L),0x50L,0xF3L},{0x15L,1L,(-2L),0x4DL,(-8L),0x29L,0x29L},{9L,0L,(-1L),0L,9L,0x6AL,0L},{0x87L,0xDAL,1L,0x4DL,0x92L,0x4DL,1L}},{{0L,0x09L,(-8L),0L,0xF3L,0x6FL,0x12L},{0x87L,0x4DL,0xDDL,0xDDL,0x4DL,0x87L,0x92L},{9L,1L,0xACL,1L,0xF3L,(-8L),(-1L)},{0x15L,0x9EL,0x15L,0x29L,0x92L,(-2L),0x87L},{0xACL,1L,9L,0x6FL,9L,1L,0xACL},{0xDDL,0x4DL,0x87L,0x92L,(-8L),(-2L),(-8L)},{(-8L),0x09L,0L,1L,(-1L),(-8L),(-1L)},{1L,0xDAL,0x87L,0x15L,0x15L,0x87L,0xDAL},{(-1L),0L,9L,0x6AL,0L,0x6FL,(-1L)}},{{(-2L),1L,0x15L,1L,(-2L),0x4DL,(-8L)},{0x12L,(-8L),0xACL,0x6AL,(-1L),0x6AL,0xACL},{(-8L),(-8L),0xDDL,0x15L,0xDAL,0x29L,0x87L},{0x12L,0x6AL,(-8L),1L,0x93L,0x50L,(-1L)},{(-2L),0xDDL,1L,0x92L,0xDAL,0xDAL,0x92L},{(-1L),9L,(-1L),0x6FL,(-1L),(-3L),0x12L},{1L,0xDDL,(-2L),0x29L,(-2L),0xDDL,1L},{(-8L),0x6AL,0x12L,1L,0L,(-3L),0L},{0xDDL,(-8L),(-8L),0xDDL,0x15L,0xDAL,0x29L}},{{0xACL,(-8L),0x12L,0L,(-1L),0x50L,0xF3L},{0x15L,1L,(-2L),0x4DL,(-8L),0x29L,0x29L},{9L,0L,(-1L),0L,9L,0x6AL,0L},{0x87L,0xDAL,1L,0x4DL,0x92L,0x4DL,1L},{0L,0x09L,(-8L),0L,0xF3L,0L,(-1L)},{0x15L,0xDDL,(-8L),(-8L),0xDDL,0x15L,0xDAL},{0x12L,0x09L,0L,(-8L),9L,(-3L),0xF3L},{0x4DL,0x92L,0x4DL,1L,0xDAL,0x87L,0x15L},{0L,0x09L,0x12L,0L,0x12L,0x09L,0L}}};
    int32_t l_242 = 0xAC3F2620L;
    int32_t l_243 = 1L;
    uint32_t l_269 = 4294967295UL;
    const uint32_t *l_297 = &g_122[2];
    uint64_t *l_300 = &g_106;
    float **l_325 = &g_324;
    int64_t l_326 = 8L;
    int8_t l_327[2][4][9] = {{{0xCEL,(-1L),(-1L),0xCEL,0x66L,1L,0xCEL,1L,0x66L},{4L,0L,0L,4L,5L,0L,4L,0L,5L},{0xCEL,(-1L),(-1L),0xCEL,0x66L,1L,0xCEL,1L,0x66L},{4L,0L,0L,4L,5L,0L,4L,0L,5L}},{{0xCEL,(-1L),(-1L),0xCEL,0x66L,1L,0xCEL,1L,0x66L},{4L,0L,0L,4L,5L,0L,4L,0L,5L},{0xCEL,(-1L),(-1L),0xCEL,0x66L,1L,0xCEL,1L,0x66L},{4L,0L,0L,4L,5L,0L,4L,0L,5L}}};
    uint32_t *l_330 = &l_269;
    int i, j, k;
    for (g_104 = 1; (g_104 == (-16)); g_104 = safe_sub_func_uint16_t_u_u(g_104, 2))
    { /* block id: 25 */
        int16_t l_142 = (-5L);
        int32_t l_151 = 0L;
        int8_t *l_163 = &g_104;
        float *l_205 = &g_108;
        int32_t l_218 = 0xB84BFDF1L;
        int32_t l_220 = 0x5C0E4766L;
        int32_t *l_262[1];
        uint32_t l_264 = 0UL;
        uint8_t *l_291 = &g_86;
        uint8_t **l_290 = &l_291;
        int32_t **l_296 = &l_262[0];
        int i;
        for (i = 0; i < 1; i++)
            l_262[i] = &l_151;
        (*g_128) &= l_127;
        for (g_66.f2 = 0; (g_66.f2 >= 21); g_66.f2 = safe_add_func_uint32_t_u_u(g_66.f2, 2))
        { /* block id: 29 */
            uint32_t *l_133 = (void*)0;
            int32_t l_134 = 0x607BBC66L;
            int64_t *l_138 = &l_127;
            const int8_t *l_164 = &g_104;
            int32_t **l_207 = (void*)0;
            int32_t l_211 = 0xB96844ECL;
            int32_t l_212[9][8] = {{(-8L),1L,(-1L),0x944C1B7AL,4L,0xA8138454L,1L,(-2L)},{0x88CA29FBL,0x944C1B7AL,1L,0L,0L,1L,0x944C1B7AL,0x88CA29FBL},{0L,1L,0x944C1B7AL,0x88CA29FBL,0L,1L,(-8L),0xDDD3BA11L},{0x944C1B7AL,(-1L),1L,(-8L),0x88CA29FBL,1L,8L,(-8L)},{1L,1L,0xB6FFE17FL,4L,0xB6FFE17FL,1L,1L,(-9L)},{(-8L),0x944C1B7AL,1L,0x11B318C5L,8L,0xA8138454L,1L,0L},{1L,1L,0x11B318C5L,0xDDD3BA11L,8L,0L,4L,4L},{(-8L),0xB6FFE17FL,0L,0L,0xB6FFE17FL,(-8L),(-2L),1L},{1L,1L,1L,1L,0x88CA29FBL,(-2L),0xA8138454L,0x93DA9AB6L}};
            int32_t *l_261 = &l_150;
            int64_t l_268[10][3][3] = {{{0x81322219A26B157ELL,0xD2AB83D353EA13BFLL,0x81322219A26B157ELL},{0xF1D9F503595D045BLL,0x70A9BE4DEC8AD6DFLL,0x09794D5BFBD02E40LL},{9L,1L,3L}},{{0xA8C21EBF77952CBCLL,0x83345E133C69A03ELL,1L},{3L,0x74C6D23637AD52E6LL,0x47229B2597998434LL},{0xA8C21EBF77952CBCLL,0x52B4E3854A16A100LL,0xD993A821AC233131LL}},{{0x0D2587FE98B080ABLL,0x83345E133C69A03ELL,0x9B3750E86C6B0ADALL},{9L,0x4CD03F4EF0FF0162LL,0x923B6AD0AE36DBBDLL},{0x82172DE9098E09D7LL,3L,0x52B4E3854A16A100LL}},{{0x09794D5BFBD02E40LL,3L,0x70A9BE4DEC8AD6DFLL},{0x4CD03F4EF0FF0162LL,0x4CD03F4EF0FF0162LL,0x74C6D23637AD52E6LL},{0x47229B2597998434LL,0x83345E133C69A03ELL,0x09794D5BFBD02E40LL}},{{0xA8C21EBF77952CBCLL,0x52B4E3854A16A100LL,0x48F8A4C053BF6E99LL},{1L,0x09794D5BFBD02E40LL,0x81322219A26B157ELL},{1L,0xA8C21EBF77952CBCLL,0x48F8A4C053BF6E99LL}},{{3L,8L,0x09794D5BFBD02E40LL},{0x923B6AD0AE36DBBDLL,3L,0x74C6D23637AD52E6LL},{0xD2AB83D353EA13BFLL,0x9F67F97CAD44DE8CLL,0x70A9BE4DEC8AD6DFLL}},{{0x48F8A4C053BF6E99LL,0x70A9BE4DEC8AD6DFLL,0x52B4E3854A16A100LL},{0x48F8A4C053BF6E99LL,0x923B6AD0AE36DBBDLL,0x923B6AD0AE36DBBDLL},{0xD2AB83D353EA13BFLL,0x81322219A26B157ELL,0x9B3750E86C6B0ADALL}},{{0x923B6AD0AE36DBBDLL,0x82172DE9098E09D7LL,0xD993A821AC233131LL},{3L,1L,3L},{1L,0x9B3750E86C6B0ADALL,3L}},{{1L,1L,(-2L)},{0xA8C21EBF77952CBCLL,0x82172DE9098E09D7LL,0x47229B2597998434LL},{0x47229B2597998434LL,0x81322219A26B157ELL,0x82172DE9098E09D7LL}},{{0x4CD03F4EF0FF0162LL,0x923B6AD0AE36DBBDLL,0xF1D9F503595D045BLL},{0x09794D5BFBD02E40LL,0x70A9BE4DEC8AD6DFLL,0xF1D9F503595D045BLL},{0x82172DE9098E09D7LL,0x9F67F97CAD44DE8CLL,0x82172DE9098E09D7LL}}};
            int i, j, k;
        }
        (*l_296) = &l_220;
    }
    if ((((void*)0 == l_297) && (0x85E0CE5C9CA6B344LL ^ ((*l_300) &= (((((((g_104 = (safe_mul_func_int8_t_s_s(0x09L, (1UL == ((g_213[4][3] < (g_7 >= 0x5EDDL)) != p_52))))) || 0x42L) || 4UL) ^ p_52) == 0x9F472009605B7846LL) < 0x40A1C30BL) < p_51)))))
    { /* block id: 115 */
        int16_t l_304 = 1L;
        (*g_128) &= (safe_lshift_func_int16_t_s_u(0x99E6L, (+l_304)));
        for (g_221 = 0; (g_221 != 33); ++g_221)
        { /* block id: 119 */
            return &g_122[2];
        }
    }
    else
    { /* block id: 122 */
        int32_t *l_308[6] = {&l_210,&l_242,&l_242,&l_210,&l_242,&l_242};
        int32_t **l_307 = &l_308[4];
        uint32_t l_318 = 18446744073709551610UL;
        float *l_322 = &g_108;
        float **l_321 = &l_322;
        uint32_t *l_328 = &g_122[2];
        uint16_t l_329 = 0x2ADBL;
        int i;
        (*l_307) = &l_243;
        l_150 &= ((((safe_add_func_int32_t_s_s((*g_128), ((*l_328) = ((!(((g_121 > (safe_div_func_int32_t_s_s(((g_200.f2 ^= ((l_241[0][1][5] > (safe_mul_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(l_318, (g_122[2] <= 0x021EL))), ((safe_add_func_int16_t_s_s((p_52 , ((l_241[0][1][5] >= ((((g_323 = l_321) == l_325) , p_51) , g_106)) ^ p_51)), 0x97FFL)) == 0x4DL)))) && 3UL)) != p_52), g_104))) , 0x8A52L) && l_326)) >= l_327[0][2][3])))) , 0L) ^ l_329) , p_51);
        return &g_122[5];
    }
    return &g_122[4];
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_11 g_7 g_86 g_104 g_106 g_66.f0 g_112 g_66 g_122 g_10
 * writes: g_73 g_86 g_104 g_106 g_108 g_121 g_122
 */
static int32_t  func_62(const int32_t  p_63, int32_t  p_64, int32_t  p_65)
{ /* block id: 8 */
    int32_t *l_67 = &g_11;
    int32_t l_68[8][8] = {{0x1DC630DEL,0x96F77D90L,0x95C4B79AL,0xDD68647DL,(-5L),(-5L),0xDD68647DL,0x95C4B79AL},{(-2L),(-2L),1L,1L,0x055833AEL,(-4L),0xD2964071L,(-2L)},{0x96F77D90L,0x1DC630DEL,0x54C3DC4DL,(-5L),0xD2964071L,0x54C3DC4DL,0x95C4B79AL,(-2L)},{0x1DC630DEL,1L,0x96F77D90L,1L,0x96F77D90L,1L,0x1DC630DEL,0x95C4B79AL},{(-1L),1L,0x19CD890FL,0xDD68647DL,1L,0xFD17BA7DL,0xD2964071L,(-1L)},{0L,(-1L),(-5L),1L,1L,0x54C3DC4DL,0x54C3DC4DL,1L},{(-1L),0x95C4B79AL,0x95C4B79AL,(-1L),0x96F77D90L,0L,(-2L),1L},{0x1DC630DEL,(-2L),9L,0xDD68647DL,0xD2964071L,1L,0xDD68647DL,0x1DC630DEL}};
    int32_t *l_69 = &l_68[0][2];
    int32_t *l_70 = &l_68[1][4];
    int32_t *l_71 = (void*)0;
    int32_t *l_72[6] = {&g_11,&l_68[0][2],&g_11,&g_11,&l_68[0][2],&g_11};
    uint8_t *l_85 = &g_86;
    const int32_t *l_88 = &g_11;
    const int32_t **l_87 = &l_88;
    int8_t *l_103 = &g_104;
    uint64_t *l_105 = &g_106;
    float *l_107 = &g_108;
    int i, j;
    --g_73;
    (*l_87) = (((safe_rshift_func_int8_t_s_s(g_11, ((safe_mod_func_int64_t_s_s((safe_unary_minus_func_int8_t_s(((p_63 > g_7) && ((p_65 & p_64) == ((*l_85) = ((p_63 && g_7) ^ ((safe_div_func_int64_t_s_s((safe_div_func_int8_t_s_s(g_11, p_64)), 0x536403AC46406CCCLL)) , (*l_67)))))))), 0xAA6B35ED357DCCC7LL)) != p_65))) , g_86) , (void*)0);
    (*l_107) = ((!(((void*)0 != &g_7) < 0x8.A7CF5Ep-32)) >= (safe_div_func_float_f_f(((((safe_add_func_uint64_t_u_u(((*l_105) |= (+(((*l_69) = g_7) > (p_63 | (((safe_lshift_func_int16_t_s_s(g_86, (safe_rshift_func_int16_t_s_s((&l_72[0] == &l_72[3]), (safe_add_func_int8_t_s_s(((*l_103) ^= ((safe_add_func_uint64_t_u_u(18446744073709551615UL, 0L)) == p_65)), p_64)))))) != 7L) <= 65535UL))))), 4L)) , g_66.f0) <= g_73) == 0xC.9F79E4p-66), g_11)));
    g_122[2] |= ((safe_unary_minus_func_int32_t_s(((((safe_div_func_uint16_t_u_u((((g_112 , (safe_mul_func_float_f_f(0x9.6CDD17p+53, (safe_sub_func_float_f_f((g_121 = ((*l_107) = ((*l_69) > (((safe_mul_func_float_f_f((g_66 , ((((safe_sub_func_float_f_f(((*l_69) == 0x5.E685ACp-88), p_63)) != p_65) > p_64) != (*l_67))), (*l_69))) < g_7) != p_64)))), p_63))))) , l_103) == (void*)0), g_106)) , &g_106) != (void*)0) , p_64))) ^ 0x48AAL);
    return (*g_10);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_66.f2, "g_66.f2", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_86, "g_86", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc(g_106, "g_106", print_hash_value);
    transparent_crc_bytes (&g_108, sizeof(g_108), "g_108", print_hash_value);
    transparent_crc(g_112.f2, "g_112.f2", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_122[i], "g_122[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_200.f2, "g_200.f2", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_213[i][j], "g_213[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_221, "g_221", print_hash_value);
    transparent_crc(g_235, "g_235", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_282[i][j][k].f2, "g_282[i][j][k].f2", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_412, "g_412", print_hash_value);
    transparent_crc(g_587.f0, "g_587.f0", print_hash_value);
    transparent_crc(g_587.f2, "g_587.f2", print_hash_value);
    transparent_crc(g_594.f0, "g_594.f0", print_hash_value);
    transparent_crc(g_594.f2, "g_594.f2", print_hash_value);
    transparent_crc_bytes (&g_623, sizeof(g_623), "g_623", print_hash_value);
    transparent_crc_bytes (&g_626, sizeof(g_626), "g_626", print_hash_value);
    transparent_crc_bytes (&g_628, sizeof(g_628), "g_628", print_hash_value);
    transparent_crc(g_688, "g_688", print_hash_value);
    transparent_crc(g_715, "g_715", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_851[i][j][k], "g_851[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_861, "g_861", print_hash_value);
    transparent_crc(g_867, "g_867", print_hash_value);
    transparent_crc_bytes (&g_881, sizeof(g_881), "g_881", print_hash_value);
    transparent_crc(g_902.f0, "g_902.f0", print_hash_value);
    transparent_crc(g_902.f2, "g_902.f2", print_hash_value);
    transparent_crc(g_1016, "g_1016", print_hash_value);
    transparent_crc(g_1032, "g_1032", print_hash_value);
    transparent_crc(g_1038, "g_1038", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1101[i], "g_1101[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1132.f0, "g_1132.f0", print_hash_value);
    transparent_crc(g_1132.f2, "g_1132.f2", print_hash_value);
    transparent_crc_bytes (&g_1368, sizeof(g_1368), "g_1368", print_hash_value);
    transparent_crc_bytes (&g_1383, sizeof(g_1383), "g_1383", print_hash_value);
    transparent_crc(g_1448.f0, "g_1448.f0", print_hash_value);
    transparent_crc(g_1448.f2, "g_1448.f2", print_hash_value);
    transparent_crc(g_1507, "g_1507", print_hash_value);
    transparent_crc(g_1796, "g_1796", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1907[i], "g_1907[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2006, "g_2006", print_hash_value);
    transparent_crc(g_2078, "g_2078", print_hash_value);
    transparent_crc(g_2133.f0, "g_2133.f0", print_hash_value);
    transparent_crc(g_2133.f2, "g_2133.f2", print_hash_value);
    transparent_crc(g_2177, "g_2177", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2191[i], "g_2191[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2214[i], "g_2214[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2217, "g_2217", print_hash_value);
    transparent_crc(g_2425.f0, "g_2425.f0", print_hash_value);
    transparent_crc(g_2425.f2, "g_2425.f2", print_hash_value);
    transparent_crc(g_2474, "g_2474", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 586
XXX total union variables: 11

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 185
   depth: 2, occurrence: 60
   depth: 3, occurrence: 1
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 3
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 4
   depth: 24, occurrence: 2
   depth: 26, occurrence: 2
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 36, occurrence: 2
   depth: 41, occurrence: 1
   depth: 43, occurrence: 1
   depth: 44, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 556

XXX times a variable address is taken: 1403
XXX times a pointer is dereferenced on RHS: 303
breakdown:
   depth: 1, occurrence: 256
   depth: 2, occurrence: 46
   depth: 3, occurrence: 1
XXX times a pointer is dereferenced on LHS: 318
breakdown:
   depth: 1, occurrence: 287
   depth: 2, occurrence: 30
   depth: 3, occurrence: 0
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 47
XXX times a pointer is compared with address of another variable: 16
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 7353

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1069
   level: 2, occurrence: 295
   level: 3, occurrence: 67
   level: 4, occurrence: 13
   level: 5, occurrence: 1
XXX number of pointers point to pointers: 206
XXX number of pointers point to scalars: 345
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.1
XXX average alias set size: 1.43

XXX times a non-volatile is read: 1799
XXX times a non-volatile is write: 921
XXX times a volatile is read: 113
XXX    times read thru a pointer: 21
XXX times a volatile is write: 46
XXX    times written thru a pointer: 11
XXX times a volatile is available for access: 5.84e+03
XXX percentage of non-volatile access: 94.5

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 196
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 32
   depth: 2, occurrence: 32
   depth: 3, occurrence: 41
   depth: 4, occurrence: 29
   depth: 5, occurrence: 29

XXX percentage a fresh-made variable is used: 18.1
XXX percentage an existing variable is used: 81.9
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      87478941
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_4 = 0xE00FE1CAL;
static int32_t g_37 = (-3L);
static int16_t g_42 = 7L;
static uint32_t g_55[9][8] = {{4294967294UL,4294967295UL,0x3F327E0EL,4294967295UL,0UL,0x14A8A5A2L,0UL,4294967295UL},{4294967289UL,0UL,4294967289UL,3UL,0xEC2C04DDL,0x535453F2L,4294967288UL,4294967289UL},{3UL,0x4162A971L,0x4693715CL,0x14A8A5A2L,0x90704A96L,3UL,0xEC2C04DDL,0UL},{3UL,0xD06D0C81L,3UL,0x4693715CL,0xEC2C04DDL,1UL,0xD06D0C81L,0x4162A971L},{4294967289UL,4294967294UL,0x4162A971L,0x557C7BD5L,0UL,1UL,4294967293UL,4294967293UL},{4294967294UL,4294967295UL,1UL,1UL,4294967295UL,4294967294UL,3UL,4294967295UL},{1UL,0xD06D0C81L,0UL,4294967289UL,0xF140B356L,0x4693715CL,0x3B032F89L,1UL},{4294967293UL,0x557C7BD5L,4294967295UL,4294967289UL,4294967291UL,3UL,0xDBBF849EL,4294967295UL},{0x14A8A5A2L,4294967291UL,0x557C7BD5L,4294967295UL,0xF73439D6L,0x4162A971L,0xEC2C04DDL,0x557C7BD5L}};
static uint32_t *g_54 = &g_55[3][5];
static int16_t g_80 = 0xE515L;
static const int32_t g_89 = 8L;
static const int32_t *g_90[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int8_t g_91 = 0L;
static int32_t g_93 = 0xC2A0AA59L;
static int64_t g_124[8][1][5] = {{{0x732090DF5CE33710LL,0L,0x732090DF5CE33710LL,0xBB3B6D225344571ALL,0x1BA0C28541D7298CLL}},{{0xE78B5E36A62F8029LL,1L,(-1L),1L,0xE78B5E36A62F8029LL}},{{0x732090DF5CE33710LL,2L,0L,(-1L),0L}},{{0x0A5162D90DD55268LL,0x0A5162D90DD55268LL,(-1L),0xE78B5E36A62F8029LL,(-6L)}},{{2L,0x732090DF5CE33710LL,0x732090DF5CE33710LL,2L,0L}},{{1L,0xE78B5E36A62F8029LL,1L,1L,0xE78B5E36A62F8029LL}},{{0L,0x732090DF5CE33710LL,0xBB3B6D225344571ALL,0x1BA0C28541D7298CLL,0x1BA0C28541D7298CLL}},{{1L,0x0A5162D90DD55268LL,1L,1L,(-1L)}}};
static int32_t * volatile g_128 = (void*)0;/* VOLATILE GLOBAL g_128 */
static const uint64_t g_139 = 0x0C2188DBB9F33108LL;
static const uint64_t * volatile g_138 = &g_139;/* VOLATILE GLOBAL g_138 */
static int32_t *g_146 = &g_4;
static int32_t **g_145 = &g_146;
static uint64_t g_169 = 18446744073709551615UL;
static int32_t **** volatile g_185 = (void*)0;/* VOLATILE GLOBAL g_185 */
static uint8_t g_198 = 0xECL;
static int32_t ***** volatile g_208 = (void*)0;/* VOLATILE GLOBAL g_208 */
static int32_t g_215 = 0xE929E165L;
static uint64_t g_236 = 0xCD8D3F1B2D74E970LL;
static int32_t * volatile g_266 = &g_215;/* VOLATILE GLOBAL g_266 */
static uint8_t g_279[1][2][9] = {{{0x6DL,0xFDL,0UL,0xFDL,0x6DL,0xE3L,0xE3L,0x6DL,0xFDL},{1UL,1UL,1UL,0xE3L,0UL,0UL,0xE3L,1UL,1UL}}};
static float g_308 = 0x9.5B2BABp+98;
static float * volatile g_311 = &g_308;/* VOLATILE GLOBAL g_311 */
static int32_t g_316[5][2][2] = {{{0x856F0ABEL,4L},{0xB6165234L,(-1L)}},{{4L,(-1L)},{0xB6165234L,4L}},{{0x856F0ABEL,0x856F0ABEL},{0x856F0ABEL,4L}},{{0xB6165234L,(-1L)},{4L,(-1L)}},{{0xB6165234L,4L},{0x856F0ABEL,0x856F0ABEL}}};
static volatile uint32_t g_319 = 6UL;/* VOLATILE GLOBAL g_319 */
static float * volatile g_339 = &g_308;/* VOLATILE GLOBAL g_339 */
static uint16_t g_360 = 0UL;
static int32_t * volatile g_377 = &g_215;/* VOLATILE GLOBAL g_377 */
static uint32_t **g_391 = &g_54;
static uint16_t *g_425[8] = {(void*)0,&g_360,(void*)0,(void*)0,&g_360,(void*)0,(void*)0,&g_360};
static uint16_t **g_424 = &g_425[4];
static int32_t * volatile g_440 = &g_215;/* VOLATILE GLOBAL g_440 */
static uint16_t ***g_445 = &g_424;
static uint64_t g_457 = 18446744073709551609UL;
static int32_t ***g_468 = &g_145;
static int32_t ****g_467[7] = {&g_468,(void*)0,(void*)0,&g_468,(void*)0,(void*)0,&g_468};
static int32_t *****g_466 = &g_467[2];
static int32_t g_495 = 0x77E10AD3L;
static volatile uint64_t g_502 = 18446744073709551612UL;/* VOLATILE GLOBAL g_502 */
static int64_t *g_570 = &g_124[5][0][0];
static int64_t * volatile *g_569 = &g_570;
static int64_t * volatile ** volatile g_571 = &g_569;/* VOLATILE GLOBAL g_571 */
static volatile int8_t g_663 = 0xF2L;/* VOLATILE GLOBAL g_663 */
static uint32_t g_682 = 0UL;
static int64_t g_762 = 0xCC9FE7ED45B5F330LL;
static int64_t g_769 = 0L;
static int32_t g_787[10] = {1L,1L,1L,1L,1L,1L,1L,1L,1L,1L};
static int32_t *****g_833[9][8][3] = {{{&g_467[2],&g_467[2],&g_467[2]},{(void*)0,&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[4]},{(void*)0,&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[2]},{(void*)0,&g_467[2],&g_467[3]}},{{&g_467[2],&g_467[2],&g_467[4]},{&g_467[3],&g_467[2],&g_467[4]},{&g_467[2],&g_467[2],(void*)0},{&g_467[2],&g_467[2],(void*)0},{&g_467[3],&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[2]},{(void*)0,&g_467[4],&g_467[4]},{&g_467[2],&g_467[2],&g_467[2]}},{{(void*)0,&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],(void*)0},{&g_467[2],&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[1]},{&g_467[6],&g_467[0],&g_467[4]},{(void*)0,&g_467[2],&g_467[3]},{&g_467[0],&g_467[6],&g_467[2]},{&g_467[6],&g_467[4],&g_467[2]}},{{&g_467[0],&g_467[2],&g_467[1]},{(void*)0,&g_467[0],&g_467[2]},{&g_467[6],&g_467[2],&g_467[2]},{&g_467[2],(void*)0,&g_467[2]},{&g_467[6],&g_467[1],&g_467[2]},{(void*)0,(void*)0,&g_467[1]},{&g_467[3],&g_467[0],&g_467[2]},{&g_467[3],&g_467[2],&g_467[2]}},{{&g_467[5],&g_467[0],&g_467[3]},{&g_467[6],(void*)0,&g_467[4]},{&g_467[4],&g_467[1],&g_467[1]},{&g_467[2],(void*)0,&g_467[2]},{&g_467[2],&g_467[2],(void*)0},{&g_467[4],&g_467[0],(void*)0},{&g_467[6],&g_467[2],&g_467[2]},{&g_467[5],&g_467[4],&g_467[1]}},{{&g_467[3],&g_467[6],&g_467[2]},{&g_467[3],&g_467[2],(void*)0},{(void*)0,&g_467[0],(void*)0},{&g_467[6],&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[1]},{&g_467[6],&g_467[0],&g_467[4]},{(void*)0,&g_467[2],&g_467[3]},{&g_467[0],&g_467[6],&g_467[2]}},{{&g_467[6],&g_467[4],&g_467[2]},{&g_467[0],&g_467[2],&g_467[1]},{(void*)0,&g_467[0],&g_467[2]},{&g_467[6],&g_467[2],&g_467[2]},{&g_467[2],(void*)0,&g_467[2]},{&g_467[6],&g_467[1],&g_467[2]},{(void*)0,(void*)0,&g_467[1]},{&g_467[3],&g_467[0],&g_467[2]}},{{&g_467[3],&g_467[2],&g_467[2]},{&g_467[5],&g_467[0],&g_467[3]},{&g_467[6],(void*)0,&g_467[4]},{&g_467[4],&g_467[1],&g_467[1]},{&g_467[2],(void*)0,&g_467[2]},{&g_467[2],&g_467[2],(void*)0},{&g_467[4],&g_467[0],(void*)0},{&g_467[6],&g_467[2],&g_467[2]}},{{&g_467[5],&g_467[4],&g_467[1]},{&g_467[3],&g_467[6],&g_467[2]},{&g_467[3],&g_467[2],(void*)0},{(void*)0,&g_467[0],(void*)0},{&g_467[6],&g_467[2],&g_467[2]},{&g_467[2],&g_467[2],&g_467[1]},{&g_467[6],&g_467[0],&g_467[4]},{(void*)0,&g_467[2],&g_467[3]}}};
static int32_t ** const  volatile g_843[9] = {&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146};
static int32_t ** volatile g_844 = &g_146;/* VOLATILE GLOBAL g_844 */
static float g_846 = (-0x1.6p-1);
static uint32_t g_927 = 18446744073709551615UL;
static int8_t g_943[9][8][3] = {{{0x78L,0L,0L},{0L,0L,5L},{(-8L),0x78L,0xF1L},{0L,0L,0xF1L},{0x78L,(-8L),5L},{0L,0L,0L},{0L,0x78L,0L},{0L,(-8L),(-8L)}},{{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),(-8L)},{5L,(-8L),0xF1L}},{{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),(-8L)},{5L,(-8L),0xF1L},{5L,0L,0L}},{{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),(-8L)},{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L}},{{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),(-8L)},{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L}},{{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),(-8L)},{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)}},{{(-8L),0L,5L},{0L,(-8L),(-8L)},{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L}},{{0L,(-8L),(-8L)},{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),(-8L)}},{{5L,(-8L),0xF1L},{5L,0L,0L},{5L,5L,0L},{0L,5L,0xF1L},{(-8L),5L,(-8L)},{(-8L),0L,5L},{0L,(-8L),5L},{0x78L,5L,0L}}};
static float * volatile g_982 = &g_846;/* VOLATILE GLOBAL g_982 */
static volatile float g_1009[7][5] = {{(-0x6.Dp-1),(-0x1.0p-1),0x0.Fp+1,0x6.F0A4A3p+53,0x0.Ep+1},{(-0x10.Bp-1),(-0x4.2p-1),0x6.F0A4A3p+53,0x1.Ep-1,0x6.F0A4A3p+53},{0x6.F0A4A3p+53,0x6.F0A4A3p+53,0x0.Ap+1,0x0.Fp+1,0x1.Bp-1},{0x6.F0A4A3p+53,0x0.D43720p-48,0x1.Bp-1,0x7.Ep+1,0x2.1p-1},{(-0x10.Bp-1),0x3.BE0979p+88,0x0.Ep+1,0x6.92F03Ap-99,(-0x1.Ap-1)},{(-0x6.Dp-1),0x0.D43720p-48,0x0.D43720p-48,(-0x6.Dp-1),0x5.9p-1},{(-0x1.0p-1),0x6.F0A4A3p+53,0x0.D43720p-48,0x1.Bp-1,0x7.Ep+1}};
static const int32_t ** volatile g_1013 = &g_90[5];/* VOLATILE GLOBAL g_1013 */
static float g_1029 = 0x1.EEE0ACp-8;
static int8_t g_1034 = 0x8BL;
static volatile float g_1056[5] = {0x9.708B10p+70,0x9.708B10p+70,0x9.708B10p+70,0x9.708B10p+70,0x9.708B10p+70};
static volatile float *g_1055 = &g_1056[0];
static volatile float **g_1054 = &g_1055;
static uint8_t g_1095 = 255UL;
static int32_t * volatile g_1097 = &g_37;/* VOLATILE GLOBAL g_1097 */
static int32_t ** volatile g_1127 = (void*)0;/* VOLATILE GLOBAL g_1127 */
static const uint32_t g_1149 = 0x231EE244L;
static const uint32_t *g_1148 = &g_1149;
static uint8_t g_1186 = 4UL;
static float g_1234 = 0x1.3p+1;
static uint8_t g_1313 = 255UL;
static int64_t **g_1339 = &g_570;
static int64_t ***g_1338 = &g_1339;
static int64_t ****g_1337 = &g_1338;
static int32_t g_1368 = 0xAA592C1DL;
static float * volatile *g_1396[1] = {(void*)0};
static float * volatile ** volatile g_1395[8] = {&g_1396[0],(void*)0,&g_1396[0],(void*)0,&g_1396[0],(void*)0,&g_1396[0],(void*)0};
static int16_t g_1398 = 0x0ECAL;
static uint64_t g_1473 = 0xC42E54D3639447D7LL;
static int32_t ** volatile g_1478 = (void*)0;/* VOLATILE GLOBAL g_1478 */
static uint16_t g_1497 = 0x5C0EL;
static int32_t ** volatile g_1646 = (void*)0;/* VOLATILE GLOBAL g_1646 */


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static uint8_t  func_8(uint16_t  p_9, int32_t * p_10, int32_t * p_11, uint16_t  p_12);
static int32_t  func_17(int8_t  p_18, uint64_t  p_19, int8_t  p_20);
static int32_t  func_23(uint8_t  p_24, uint8_t  p_25, const int16_t  p_26);
static int32_t  func_28(int32_t * const  p_29);
static int32_t * const  func_30(const uint8_t  p_31, int32_t  p_32);
static int32_t  func_46(int32_t  p_47, int32_t * p_48, int32_t * p_49);
static int32_t * func_50(int32_t  p_51, uint32_t * p_52, uint32_t  p_53);
static int32_t * func_58(const uint32_t * p_59, const int32_t * p_60, uint16_t  p_61, int32_t * p_62);
static uint64_t  func_63(int64_t  p_64, int64_t  p_65, float  p_66);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_37 g_42 g_54 g_55 g_91 g_89 g_80 g_93 g_138 g_139 g_124 g_145 g_90 g_146 g_236 g_198 g_266 g_215 g_279 g_311 g_319 g_316 g_339 g_169 g_424 g_308 g_440 g_445 g_466 g_360 g_377 g_495 g_502 g_468 g_457 g_569 g_571 g_844 g_787 g_927 g_943 g_982 g_570 g_1013 g_1034 g_769 g_846 g_1054 g_682 g_1029 g_1055 g_1095 g_1097 g_1186 g_1337 g_1339 g_1338 g_1368 g_1398 g_1056 g_1313
 * writes: g_4 g_37 g_80 g_42 g_90 g_91 g_124 g_93 g_55 g_145 g_146 g_198 g_185 g_236 g_215 g_169 g_279 g_308 g_319 g_54 g_391 g_424 g_445 g_457 g_360 g_316 g_569 g_846 g_495 g_927 g_943 g_1029 g_1056 g_1148 g_1186 g_1313 g_1337 g_833 g_682 g_570 g_1398
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int32_t l_2[10] = {(-6L),0x5286E871L,0L,0x5286E871L,(-6L),(-6L),0x5286E871L,0L,0x5286E871L,(-6L)};
    int32_t *l_3[6][9][4] = {{{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,(void*)0,(void*)0},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4},{(void*)0,&g_4,&g_4,&g_4},{(void*)0,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4}},{{&g_4,&g_4,&g_4,&g_4},{(void*)0,&g_4,&g_4,(void*)0},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4}},{{&g_4,&g_4,&g_4,(void*)0},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4},{(void*)0,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,&g_4,&g_4},{(void*)0,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,(void*)0},{&g_4,&g_4,&g_4,&g_4}},{{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{(void*)0,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,&g_4,&g_4}},{{&g_4,&g_4,&g_4,&g_4},{(void*)0,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,(void*)0,(void*)0,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{(void*)0,(void*)0,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4}},{{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,(void*)0,&g_4},{&g_4,&g_4,(void*)0,&g_4},{(void*)0,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4},{&g_4,(void*)0,&g_4,(void*)0},{&g_4,(void*)0,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4}}};
    float l_5[9][8] = {{0x1.Bp-1,0x1.0p+1,(-0x2.1p+1),0x9.6A0B59p+99,0x7.7EBD00p+74,(-0x4.9p+1),0x1.E6E404p-33,(-0x4.9p+1)},{0xC.005D9Cp+10,0x3.Cp-1,0x7.7EBD00p+74,0x3.Cp-1,0xC.005D9Cp+10,(-0x6.8p+1),0x4.B3F4B1p-71,0x1.E6E404p-33},{0x9.5p+1,0x4.B3F4B1p-71,0xC.29478Cp+69,0x1.Bp-1,(-0x6.Bp-1),0x6.9CD17Cp-38,0x3.Cp-1,0x3.Cp-1},{0x3.2955F1p-93,0x2.Cp+1,0xC.29478Cp+69,0x9.5p+1,0x3.Cp-1,0x9.69B42Cp-75,0x4.B3F4B1p-71,(-0x2.1p+1)},{(-0x6.Bp-1),0x8.8p-1,0x7.7EBD00p+74,0x1.E6E404p-33,0xC.29478Cp+69,0xC.29478Cp+69,0x1.E6E404p-33,0x7.7EBD00p+74},{0xB.A5E0B2p-1,0xB.A5E0B2p-1,(-0x2.1p+1),0x4.B3F4B1p-71,0x9.69B42Cp-75,0x3.Cp-1,0x9.5p+1,0xC.29478Cp+69},{(-0x2.1p+1),0x1.Bp-1,0x3.Cp-1,0x3.Cp-1,0x6.9CD17Cp-38,(-0x6.Bp-1),0x1.Bp-1,0xC.29478Cp+69},{0x1.Bp-1,0xA.B6CE1Ap+59,0x1.E6E404p-33,0x4.B3F4B1p-71,(-0x6.8p+1),0xC.005D9Cp+10,0x3.Cp-1,0x7.7EBD00p+74},{0x9.6A0B59p+99,0x7.7EBD00p+74,(-0x4.9p+1),0x1.E6E404p-33,(-0x4.9p+1),0x7.7EBD00p+74,0x9.6A0B59p+99,(-0x2.1p+1)}};
    const int32_t l_27 = 0x198FE831L;
    int8_t l_1151 = 0x1DL;
    int8_t l_1152 = 4L;
    int64_t *l_1417 = &g_124[5][0][0];
    uint16_t l_1420 = 0xF12EL;
    int32_t l_1429[5] = {0xCB32F2A7L,0xCB32F2A7L,0xCB32F2A7L,0xCB32F2A7L,0xCB32F2A7L};
    int16_t *l_1448 = &g_1398;
    int16_t **l_1447 = &l_1448;
    int32_t * const *l_1492 = &g_146;
    int32_t * const **l_1491 = &l_1492;
    uint32_t l_1507 = 0xD317E7F2L;
    int8_t l_1510 = 0x94L;
    int16_t l_1518 = 0L;
    uint8_t l_1590 = 0xFDL;
    uint16_t ** const *l_1592[2][3][4] = {{{&g_424,&g_424,&g_424,&g_424},{&g_424,&g_424,(void*)0,&g_424},{(void*)0,&g_424,&g_424,(void*)0}},{{(void*)0,&g_424,(void*)0,(void*)0},{&g_424,(void*)0,&g_424,&g_424},{&g_424,(void*)0,&g_424,&g_424}}};
    uint64_t l_1599 = 0x0F11C56E3F305EEELL;
    int32_t *l_1606 = &g_787[6];
    float l_1676[7];
    int16_t l_1678[3];
    int32_t l_1680[4] = {9L,9L,9L,9L};
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1676[i] = (-0x2.9p-1);
    for (i = 0; i < 3; i++)
        l_1678[i] = 0xA0CEL;
    g_4 |= l_2[5];
    if (((g_4 , (safe_sub_func_int32_t_s_s((g_4 == (g_4 >= func_8(((((((safe_div_func_int32_t_s_s(1L, (safe_sub_func_int32_t_s_s(func_17((safe_sub_func_uint8_t_u_u((func_23(g_4, g_4, l_27) , g_89), g_4)), g_1034, g_769), l_1151)))) , (*g_138)) , (*g_54)) , 4UL) ^ 0x4CL) , 1UL), l_3[4][7][1], l_3[4][2][0], l_1152))), g_1398))) == 0xEB640FF53C5A3E42LL))
    { /* block id: 657 */
        int64_t l_1403 = 0xB93E2F29CD813EE6LL;
        uint32_t l_1421 = 18446744073709551615UL;
        uint16_t *l_1428[7][3][2] = {{{&l_1420,&l_1420},{&l_1420,(void*)0},{&g_360,&g_360}},{{&l_1420,&g_360},{&g_360,(void*)0},{&g_360,&g_360}},{{&l_1420,&g_360},{&g_360,(void*)0},{&l_1420,&l_1420}},{{&l_1420,&l_1420},{&l_1420,(void*)0},{&g_360,&g_360}},{{&l_1420,&g_360},{&g_360,(void*)0},{&g_360,&g_360}},{{&l_1420,&g_360},{&g_360,(void*)0},{&l_1420,&l_1420}},{{&l_1420,&l_1420},{&l_1420,(void*)0},{&g_360,&g_360}}};
        uint32_t *l_1430 = (void*)0;
        const uint32_t *l_1433[3][3][7] = {{{(void*)0,&g_55[3][5],&g_682,(void*)0,&g_55[3][5],&g_682,&g_682},{(void*)0,(void*)0,&g_55[2][2],(void*)0,(void*)0,&g_682,(void*)0},{&g_55[3][5],(void*)0,&g_682,&g_55[3][5],(void*)0,(void*)0,&g_55[3][5]}},{{(void*)0,(void*)0,(void*)0,&g_55[3][5],&g_682,&g_55[3][5],(void*)0},{&g_55[3][5],&g_55[3][5],&g_682,&g_55[3][5],&g_55[3][5],&g_682,&g_55[3][5]},{(void*)0,&g_55[3][5],&g_55[4][7],(void*)0,&g_55[4][7],&g_55[3][5],(void*)0}},{{(void*)0,&g_55[3][5],&g_682,&g_682,&g_55[3][5],(void*)0,&g_682},{&g_682,(void*)0,&g_55[5][2],(void*)0,&g_682,&g_682,&g_682},{&g_55[3][5],&g_682,&g_682,&g_55[3][5],(void*)0,&g_682,&g_55[3][5]}}};
        float *l_1434 = &g_1234;
        int32_t l_1435 = 0x62823D17L;
        int16_t * const l_1444 = (void*)0;
        int16_t * const *l_1443 = &l_1444;
        int16_t * const **l_1445 = (void*)0;
        int16_t * const **l_1446 = &l_1443;
        int i, j, k;
        for (g_682 = 0; (g_682 == 49); ++g_682)
        { /* block id: 660 */
            float *l_1404 = &g_1029;
            int32_t l_1409[8];
            int16_t *l_1412[5];
            int32_t l_1413 = 0xB86CE23BL;
            int64_t *l_1416 = &g_769;
            uint32_t l_1422 = 18446744073709551615UL;
            int i;
            for (i = 0; i < 8; i++)
                l_1409[i] = 0x35D306E0L;
            for (i = 0; i < 5; i++)
                l_1412[i] = &g_42;
            (*g_1055) = (safe_div_func_float_f_f(l_1403, (((((*l_1404) = (*g_1055)) < l_1403) >= l_1403) < ((safe_div_func_int32_t_s_s((((safe_add_func_int32_t_s_s(l_1409[0], (safe_lshift_func_int16_t_s_s((l_1413 ^= g_1313), 3)))) , ((safe_add_func_uint16_t_u_u((((g_80 = ((((**g_1338) = l_1416) != l_1417) , (safe_div_func_int32_t_s_s(((l_1420 & 1L) , l_1403), l_1403)))) , l_1409[4]) < l_1403), (-8L))) , l_1409[5])) | l_1409[7]), l_1409[6])) , l_1421))));
            return l_1422;
        }
        l_1435 |= (~((l_1429[0] = ((safe_div_func_int8_t_s_s((g_943[1][7][1] |= (l_1403 > l_1403)), ((safe_sub_func_int64_t_s_s(0x665A9C2CC4716266LL, 9L)) && g_1034))) || l_1403)) , ((((**g_1339) == (l_1430 == ((((safe_div_func_int32_t_s_s((*g_440), (*g_54))) < 0x9FD4AE9DL) , 1L) , l_1433[2][2][5]))) , l_1434) != (*g_1054))));
        (**g_1054) = l_1435;
        (*g_266) |= (safe_mul_func_uint16_t_u_u((g_55[8][6] & (l_1435 ^ l_1435)), ((((*l_1417) = (**g_1339)) ^ (safe_sub_func_int8_t_s_s((l_1421 <= 0xD56A1D63A498E048LL), ((&g_316[3][0][1] == &g_316[1][1][0]) ^ ((safe_unary_minus_func_int64_t_s((safe_lshift_func_int8_t_s_u((((*l_1446) = l_1443) != l_1447), 7)))) <= g_279[0][1][2]))))) & l_1403)));
    }
    else
    { /* block id: 675 */
        float l_1449 = 0x8.Bp-1;
        int32_t l_1450 = 0L;
        int64_t l_1506[10][5][1] = {{{2L},{(-2L)},{2L},{0x73255B9A3DB3E886LL},{0xDC4DC8108297A83BLL}},{{0x20B0607EF9BFF1FALL},{0xBDFB7897CE65954CLL},{0xB6DEAF519C5CBAB6LL},{(-1L)},{0xE3A6F65740143E59LL}},{{(-1L)},{(-1L)},{0xE3A6F65740143E59LL},{(-1L)},{0xB6DEAF519C5CBAB6LL}},{{0xBDFB7897CE65954CLL},{0x20B0607EF9BFF1FALL},{0xDC4DC8108297A83BLL},{0x73255B9A3DB3E886LL},{2L}},{{(-2L)},{2L},{0x73255B9A3DB3E886LL},{0xDC4DC8108297A83BLL},{0x20B0607EF9BFF1FALL}},{{0xBDFB7897CE65954CLL},{0xB6DEAF519C5CBAB6LL},{(-1L)},{0xE3A6F65740143E59LL},{(-1L)}},{{(-1L)},{0xE3A6F65740143E59LL},{(-1L)},{0xB6DEAF519C5CBAB6LL},{0xBDFB7897CE65954CLL}},{{0x20B0607EF9BFF1FALL},{0xDC4DC8108297A83BLL},{0x73255B9A3DB3E886LL},{2L},{(-2L)}},{{2L},{0x73255B9A3DB3E886LL},{0xDC4DC8108297A83BLL},{0x20B0607EF9BFF1FALL},{0xBDFB7897CE65954CLL}},{{0xB6DEAF519C5CBAB6LL},{(-1L)},{0xE3A6F65740143E59LL},{(-1L)},{(-1L)}}};
        uint16_t l_1528 = 0x5BF7L;
        int64_t l_1558 = 0x788E792B277931FELL;
        int32_t l_1560[6] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
        uint16_t ***l_1591 = &g_424;
        const uint32_t l_1615 = 4294967286UL;
        int32_t *l_1637 = (void*)0;
        int32_t *l_1648 = (void*)0;
        const int64_t l_1660[4][7] = {{8L,0L,8L,(-1L),(-1L),8L,0L},{0x5B95E23302319A47LL,(-1L),0xE907650903E2551ELL,0xE907650903E2551ELL,(-1L),0x5B95E23302319A47LL,(-1L)},{8L,(-1L),(-1L),8L,0L,8L,(-1L)},{0L,0L,0x5B95E23302319A47LL,0xE907650903E2551ELL,0x5B95E23302319A47LL,0L,0x5B95E23302319A47LL}};
        uint16_t l_1665 = 0xC973L;
        int32_t l_1674 = 0x1E18CC86L;
        int16_t *l_1675 = &g_80;
        int64_t l_1677[1][4][8] = {{{4L,(-1L),3L,0L,(-1L),0L,3L,(-1L)},{0x742494380EB494E9LL,3L,4L,0x742494380EB494E9LL,0L,0L,0x742494380EB494E9LL,4L},{(-1L),(-1L),0xDF82C9A8914A100ALL,6L,0x742494380EB494E9LL,0xDF82C9A8914A100ALL,0x742494380EB494E9LL,6L},{4L,6L,4L,0L,6L,3L,3L,6L}}};
        int32_t l_1679 = 0x17C76DC8L;
        int i, j, k;
        l_1450 ^= (*g_440);
        l_1450 &= (*g_266);
        for (g_682 = (-27); (g_682 > 10); ++g_682)
        { /* block id: 680 */
            int32_t l_1453 = 0xF39529E1L;
            uint64_t l_1456 = 0x75DDCC020904F4AALL;
            float l_1531 = 0x9.941F69p-4;
            int32_t *l_1536 = &l_1450;
            int32_t l_1561 = 0x1A737177L;
            uint32_t l_1562 = 9UL;
            int64_t *****l_1567 = &g_1337;
            int64_t l_1571 = 0x4854ECC50DDB37E1LL;
            const uint32_t **l_1611 = (void*)0;
            const uint32_t ***l_1610 = &l_1611;
            uint64_t l_1649[8][7][3] = {{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,3UL},{0x7512B8D3805A5C52LL,3UL,0x1E1FD489CF71F1E6LL},{0x75880E319AC56765LL,0UL,0x0BFA0F3F3CD605ACLL},{0x7512B8D3805A5C52LL,0x7512B8D3805A5C52LL,0x0BFA0F3F3CD605ACLL}},{{0UL,0x75880E319AC56765LL,0x1E1FD489CF71F1E6LL},{3UL,0x7512B8D3805A5C52LL,3UL},{3UL,0UL,0x7512B8D3805A5C52LL},{0UL,3UL,0x75880E319AC56765LL},{7UL,0x75880E319AC56765LL,0x0BFA0F3F3CD605ACLL},{0x1E1FD489CF71F1E6LL,3UL,0x7512B8D3805A5C52LL},{7UL,7UL,0x7512B8D3805A5C52LL}}};
            int i, j, k;
        }
        l_1679 ^= ((safe_mul_func_uint8_t_u_u((((((*l_1675) = ((safe_mul_func_int16_t_s_s((safe_mod_func_int32_t_s_s(0x9E53DD6DL, (safe_rshift_func_int8_t_s_s(g_787[6], (g_80 , (safe_rshift_func_int16_t_s_s(((*l_1448) ^= (l_1660[1][0] < ((*g_138) && 0x964355309429F338LL))), g_316[1][1][0]))))))), (l_1674 = ((safe_mod_func_int16_t_s_s(0xADA3L, (safe_mul_func_uint16_t_u_u((l_1560[0] = (l_1450 &= (l_1665++))), (safe_div_func_int16_t_s_s(((safe_mul_func_int16_t_s_s((safe_div_func_int8_t_s_s(g_457, 1UL)), g_236)) != (*l_1606)), 0x14E2L)))))) || g_1095)))) || g_1034)) && l_1677[0][3][1]) <= l_1678[2]) > g_139), g_495)) < 0x74EF62170664EC97LL);
    }
    return l_1680[2];
}


/* ------------------------------------------ */
/* 
 * reads : g_91 g_457 g_236 g_169 g_138 g_139 g_570 g_80 g_198 g_316 g_943 g_927 g_89 g_54 g_124 g_319 g_787 g_1055 g_377 g_215 g_468 g_55 g_1186 g_982 g_846 g_844 g_1054 g_4 g_360 g_1337 g_1339 g_1338 g_1368 g_93
 * writes: g_91 g_457 g_236 g_943 g_169 g_124 g_360 g_80 g_1186 g_55 g_37 g_1056 g_215 g_145 g_1313 g_146 g_1337 g_198 g_833
 */
static uint8_t  func_8(uint16_t  p_9, int32_t * p_10, int32_t * p_11, uint16_t  p_12)
{ /* block id: 543 */
    uint16_t l_1185 = 0xFF57L;
    int32_t l_1210 = 0xE901B68EL;
    uint64_t l_1219 = 0UL;
    const int32_t *l_1222 = &g_787[6];
    int8_t l_1305 = (-9L);
    uint64_t *l_1351 = &g_457;
    uint64_t **l_1350 = &l_1351;
    for (g_91 = 11; (g_91 < (-24)); g_91 = safe_sub_func_uint8_t_u_u(g_91, 7))
    { /* block id: 546 */
        int16_t *l_1159 = &g_80;
        int32_t l_1187 = 2L;
        int32_t l_1218 = 0x23FEBB10L;
        const uint32_t *l_1221 = &g_55[8][5];
        int64_t l_1241 = 0x07B2F2431F7AB097LL;
        int32_t l_1388 = 4L;
        float * volatile *l_1397 = &g_982;
        for (g_457 = 26; (g_457 <= 12); g_457 = safe_sub_func_int64_t_s_s(g_457, 5))
        { /* block id: 549 */
            int16_t **l_1160 = &l_1159;
            uint64_t *l_1165 = &g_236;
            int32_t l_1168 = 0xF9269011L;
            int8_t *l_1169 = &g_943[1][7][1];
            int32_t l_1174 = 5L;
            float l_1179 = 0xA.E4C173p-87;
            uint64_t *l_1180 = &g_169;
            uint16_t *l_1183 = &g_360;
            int16_t *l_1184 = &g_80;
            int32_t *l_1188[1][3][6] = {{{&g_4,&g_787[9],&g_93,&g_93,&g_787[9],&g_4},{&l_1187,&g_4,&l_1168,&g_787[9],&l_1168,&g_4},{&l_1168,&l_1187,&g_93,(void*)0,(void*)0,&g_93}}};
            int8_t l_1189 = 0x46L;
            int32_t l_1211 = 0L;
            int i, j, k;
            l_1189 ^= (safe_add_func_int16_t_s_s(((&g_42 == ((*l_1160) = l_1159)) | (safe_mul_func_uint16_t_u_u(p_12, ((((((*l_1165)--) >= ((((*l_1169) = l_1168) ^ ((g_1186 = ((((*l_1184) ^= (safe_sub_func_uint16_t_u_u(l_1168, ((*l_1183) = (safe_add_func_uint64_t_u_u((l_1174 = 18446744073709551610UL), ((*g_570) = (safe_mul_func_int16_t_s_s((safe_mul_func_uint64_t_u_u(p_9, (l_1168 >= (--(*l_1180))))), ((*g_138) & p_12)))))))))) , l_1185) <= 0x0442L)) , l_1185)) > l_1187)) & l_1168) & l_1185) , p_9)))), 0x0243L));
            l_1211 &= (safe_lshift_func_int8_t_s_u(((l_1210 = (safe_sub_func_uint32_t_u_u(((safe_rshift_func_int16_t_s_u(((safe_mod_func_uint32_t_u_u(((*g_54) = (0xBFL || ((safe_add_func_int8_t_s_s((l_1185 , ((safe_div_func_int8_t_s_s(p_9, g_198)) || ((safe_lshift_func_uint8_t_u_s(((safe_div_func_uint8_t_u_u((((*g_138) & ((((**l_1160) |= ((safe_sub_func_int8_t_s_s(6L, l_1187)) && 0x33AED746L)) , ((*l_1169) ^= g_316[0][1][1])) > l_1185)) , p_12), 0x8CL)) && 0UL), p_12)) | p_9))), g_927)) != g_89))), p_12)) , 0x9F45L), l_1185)) , l_1187), p_12))) ^ 4294967288UL), g_124[5][0][0]));
        }
        for (g_236 = 0; (g_236 <= 2); g_236 += 1)
        { /* block id: 568 */
            const int32_t l_1220 = 0xF4A262E2L;
            int32_t ****l_1225[10] = {&g_468,&g_468,&g_468,&g_468,&g_468,&g_468,&g_468,&g_468,&g_468,&g_468};
            int8_t *l_1232 = &g_943[4][4][2];
            int64_t *l_1233[9] = {(void*)0,&g_769,(void*)0,(void*)0,&g_769,(void*)0,(void*)0,&g_769,(void*)0};
            uint64_t *l_1349 = &g_236;
            uint64_t **l_1348 = &l_1349;
            uint8_t *l_1392 = &g_1186;
            int i;
            p_10 = p_10;
            if (((safe_div_func_uint64_t_u_u((l_1218 = (0UL | ((p_12 , l_1225[5]) == (void*)0))), (l_1210 &= ((safe_lshift_func_uint16_t_u_s((safe_div_func_int8_t_s_s(((*l_1232) ^= ((safe_mul_func_uint16_t_u_u((l_1221 == &g_927), ((p_12 < l_1187) ^ (p_12 > 0x06L)))) > g_124[3][0][1])), g_319)), g_457)) >= 0x34L)))) == (*l_1222)))
            { /* block id: 575 */
                uint16_t l_1235 = 0xECC4L;
                int32_t l_1239 = 0x41F63C9DL;
                l_1218 |= (*l_1222);
                for (g_37 = 0; (g_37 <= 2); g_37 += 1)
                { /* block id: 579 */
                    uint32_t l_1243[9][8][2] = {{{0UL,1UL},{18446744073709551608UL,18446744073709551612UL},{0x851B4625L,6UL},{18446744073709551615UL,0x851B4625L},{0xA5AF4887L,0x67EF3D1FL},{0xA5AF4887L,0x851B4625L},{18446744073709551615UL,6UL},{0x851B4625L,18446744073709551612UL}},{{18446744073709551608UL,1UL},{0UL,0xED49FDCEL},{0xED49FDCEL,18446744073709551615UL},{0x71A609DAL,1UL},{18446744073709551606UL,18446744073709551613UL},{18446744073709551615UL,0xD285E08EL},{0x0C3ECC0BL,18446744073709551608UL},{18446744073709551615UL,18446744073709551615UL}},{{0x89A8184AL,18446744073709551606UL},{18446744073709551615UL,18446744073709551615UL},{0xB8337D6FL,0xDE449307L},{0xD285E08EL,18446744073709551615UL},{1UL,18446744073709551615UL},{0xD285E08EL,0xDE449307L},{0xB8337D6FL,18446744073709551615UL},{18446744073709551615UL,18446744073709551606UL}},{{0x89A8184AL,18446744073709551615UL},{18446744073709551615UL,18446744073709551608UL},{0x0C3ECC0BL,0xD285E08EL},{18446744073709551615UL,18446744073709551613UL},{18446744073709551606UL,1UL},{0x71A609DAL,18446744073709551615UL},{0xED49FDCEL,0xED49FDCEL},{0UL,1UL}},{{18446744073709551608UL,18446744073709551612UL},{0x851B4625L,6UL},{18446744073709551615UL,0x851B4625L},{0xA5AF4887L,0x67EF3D1FL},{0xA5AF4887L,0x851B4625L},{18446744073709551615UL,6UL},{0x851B4625L,18446744073709551612UL},{18446744073709551608UL,1UL}},{{0UL,0xED49FDCEL},{0xED49FDCEL,18446744073709551615UL},{0x71A609DAL,1UL},{18446744073709551606UL,18446744073709551613UL},{18446744073709551615UL,0xD285E08EL},{0x0C3ECC0BL,18446744073709551608UL},{18446744073709551615UL,18446744073709551615UL},{0x89A8184AL,18446744073709551606UL}},{{18446744073709551615UL,18446744073709551615UL},{0xB8337D6FL,0xDE449307L},{0xD285E08EL,18446744073709551615UL},{1UL,18446744073709551615UL},{0xD285E08EL,0xDE449307L},{0xB8337D6FL,18446744073709551615UL},{18446744073709551615UL,18446744073709551606UL},{0x89A8184AL,18446744073709551615UL}},{{18446744073709551615UL,18446744073709551608UL},{0x0C3ECC0BL,0xD285E08EL},{18446744073709551615UL,18446744073709551613UL},{18446744073709551606UL,1UL},{0x71A609DAL,18446744073709551615UL},{0xED49FDCEL,0xED49FDCEL},{0UL,1UL},{18446744073709551608UL,18446744073709551612UL}},{{0x851B4625L,6UL},{18446744073709551615UL,0x851B4625L},{0xA5AF4887L,0x67EF3D1FL},{0xA5AF4887L,0x851B4625L},{18446744073709551615UL,6UL},{0x851B4625L,18446744073709551612UL},{18446744073709551608UL,1UL},{0UL,0xED49FDCEL}}};
                    uint16_t l_1263 = 0x67FEL;
                    uint8_t *l_1264[4][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                    int i, j, k;
                    if (((void*)0 == p_10))
                    { /* block id: 580 */
                        int i, j, k;
                        l_1235 = p_12;
                        (*g_1055) = (0x3.CA67FDp+69 < 0x4.Bp+1);
                        return g_198;
                    }
                    else
                    { /* block id: 584 */
                        int16_t **l_1236 = &l_1159;
                        l_1239 &= (((void*)0 != l_1236) != ((*g_570) != (safe_lshift_func_int8_t_s_s((0x73861E8BDD6CA21CLL ^ (0UL != (-1L))), 5))));
                    }
                    for (l_1235 = 0; (l_1235 <= 7); l_1235 += 1)
                    { /* block id: 589 */
                        int32_t l_1240 = 0x6549D052L;
                        int32_t l_1242 = 0xC95F8636L;
                        int i, j, k;
                        (*g_377) &= (l_1241 = l_1240);
                        l_1243[1][3][1]--;
                    }
                    if ((safe_sub_func_uint8_t_u_u(0x2DL, ((safe_div_func_int32_t_s_s((safe_mul_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(0xA5AEL, (l_1210 = (*l_1222)))), ((l_1187 = (safe_lshift_func_uint16_t_u_s((!(safe_mod_func_int64_t_s_s(0x747F485BAFA7B295LL, (safe_sub_func_uint64_t_u_u(p_12, 0x7DEA573643A6E606LL))))), (safe_rshift_func_uint8_t_u_s(0x4AL, ((*l_1232) = ((l_1263 , ((*g_468) = &p_11)) != &p_10))))))) , (-5L)))), l_1263)) , l_1243[3][7][0]))))
                    { /* block id: 598 */
                        l_1239 ^= (safe_mod_func_int32_t_s_s(((safe_add_func_uint32_t_u_u((*g_54), (+(0xBDDAFFFBL == (l_1218 != 0x0DB1L))))) | (*l_1222)), (safe_mod_func_int16_t_s_s(0x4C2AL, l_1263))));
                    }
                    else
                    { /* block id: 600 */
                        l_1210 = p_12;
                        return g_1186;
                    }
                }
            }
            else
            { /* block id: 605 */
                int32_t *l_1324 = (void*)0;
                int32_t l_1386 = 8L;
                for (g_80 = 2; (g_80 >= 0); g_80 -= 1)
                { /* block id: 608 */
                    uint8_t l_1272 = 255UL;
                    uint16_t ** const *l_1298 = &g_424;
                    uint16_t ** const ** const l_1297 = &l_1298;
                    uint32_t ***l_1330 = &g_391;
                    uint32_t ***l_1334 = &g_391;
                    int32_t l_1352 = 0xCED246BDL;
                    const int16_t l_1383 = (-7L);
                    int64_t l_1387 = 0L;
                    if ((l_1272 = 9L))
                    { /* block id: 610 */
                        uint64_t *l_1310 = &g_169;
                        int32_t l_1311 = 0L;
                        float *l_1312[5][4] = {{&g_846,&g_846,&g_846,&g_846},{&g_846,&g_846,&g_308,&g_846},{&g_846,&g_846,&g_846,&g_846},{&g_846,&g_846,&g_846,&g_846},{&g_846,&g_846,&g_308,&g_846}};
                        uint16_t l_1314 = 0xC440L;
                        int i, j, k;
                        (*g_1055) = (safe_sub_func_float_f_f((0x4.Cp-1 < (safe_add_func_float_f_f(((safe_mul_func_float_f_f((safe_mul_func_float_f_f(p_9, (((safe_sub_func_float_f_f((l_1210 = ((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f((-(l_1272 == (g_1313 = (safe_div_func_float_f_f((!(&g_445 == l_1297)), (safe_add_func_float_f_f((safe_sub_func_float_f_f(((safe_mod_func_int16_t_s_s(l_1305, (safe_lshift_func_uint8_t_u_s((safe_add_func_uint64_t_u_u((((p_12 > (((-1L) >= ((*l_1310) |= p_12)) && 0x92L)) , (void*)0) == &g_138), p_9)), l_1311)))) , l_1272), (*g_982))), p_9))))))), p_12)), p_9)), 0x4.728ADFp-99)), p_12)), p_12)) < l_1314)), 0x2.8739F6p+5)) < (-0x1.Ep-1)) >= l_1241))), 0xC.8F39E0p-59)) != 0x0.205EAFp-82), g_846))), p_12));
                        (*g_844) = p_11;
                        l_1324 = ((((safe_mul_func_uint16_t_u_u(p_12, ((((safe_lshift_func_int16_t_s_u(((safe_add_func_int16_t_s_s(0xC9E2L, ((safe_unary_minus_func_int16_t_s(((*g_570) , ((void*)0 != (*g_1054))))) ^ (*g_54)))) == (safe_add_func_int16_t_s_s(p_9, p_12))), 13)) ^ p_9) , p_12) ^ g_787[6]))) | g_4) & g_360) , &l_1218);
                    }
                    else
                    { /* block id: 617 */
                        uint8_t *l_1327[1][3][6] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1272,(void*)0,(void*)0,&l_1272,&g_1313,&l_1272},{&l_1272,&g_1313,&l_1272,(void*)0,(void*)0,&l_1272}}};
                        uint32_t ****l_1331 = &l_1330;
                        uint32_t ***l_1333 = &g_391;
                        uint32_t ****l_1332 = &l_1333;
                        int64_t *****l_1340 = &g_1337;
                        int32_t l_1353 = 0x192D2A14L;
                        int i, j, k;
                        l_1352 &= ((0x81CE1D27L <= (safe_div_func_uint8_t_u_u((l_1187 = p_12), (safe_sub_func_uint8_t_u_u((((*l_1331) = l_1330) != (l_1334 = ((*l_1332) = &g_391))), (safe_lshift_func_uint8_t_u_u((((*l_1340) = g_1337) != &g_571), (g_198--)))))))) | (safe_sub_func_int8_t_s_s((((**g_1339) &= (+(((0L || (l_1210 = (safe_div_func_int16_t_s_s((l_1348 == l_1350), p_12)))) , p_9) & 0L))) > (*g_138)), 0xD8L)));
                        if (l_1353)
                            break;
                        p_11 = p_10;
                        (*g_1055) = ((*g_982) <= (((safe_div_func_float_f_f(((void*)0 != l_1334), (-0x5.5p+1))) == ((((+((((safe_add_func_uint8_t_u_u((l_1210 = (safe_div_func_uint64_t_u_u((~(p_9 ^ (safe_rshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s((safe_div_func_uint32_t_u_u(p_9, 4294967294UL)), l_1241)), 0)))), (****g_1337)))), 0xCEL)) , g_787[0]) < g_787[6]) , p_12)) , g_1186) > g_1368) < 0x1.Ep+1)) == p_12));
                    }
                    for (l_1352 = 0; (l_1352 <= 0); l_1352 += 1)
                    { /* block id: 634 */
                        float l_1389 = 0xE.8E0E29p+45;
                        int i, j, k;
                        l_1210 |= ((g_124[(g_80 + 2)][l_1352][(l_1352 + 2)] > ((safe_sub_func_int8_t_s_s(((((safe_div_func_int64_t_s_s((l_1388 = ((safe_rshift_func_uint16_t_u_s(((((*g_54) = (p_9 , (safe_div_func_int16_t_s_s(0xD37CL, ((*g_54) , p_12))))) <= (safe_lshift_func_uint16_t_u_s(p_9, 7))) , (safe_div_func_int16_t_s_s((((((safe_sub_func_uint32_t_u_u(l_1383, ((*g_54) = (safe_rshift_func_uint8_t_u_u((((l_1386 &= 1L) >= ((((((g_89 && g_198) & g_93) != p_9) , 0xC0B6L) != (*l_1222)) || (-1L))) & 0x86L), (*l_1222)))))) >= (*g_570)) != 0x3E8BL) || p_9) , p_9), (-1L)))), g_316[3][1][1])) && l_1387)), p_9)) < g_124[(g_80 + 2)][l_1352][(l_1352 + 2)]) >= 0x950AC1EBL) || g_457), p_9)) , 5L)) && (-9L));
                        g_833[(g_80 + 4)][(g_80 + 2)][(l_1352 + 1)] = (void*)0;
                        l_1218 ^= 0x83A7114BL;
                        if (l_1352)
                            break;
                    }
                    p_10 = p_10;
                }
                (**g_1054) = (0x4.8p+1 >= (safe_sub_func_float_f_f(p_12, (*l_1222))));
            }
            if ((*l_1222))
                continue;
            p_11 = p_10;
        }
        l_1397 = &g_311;
        return p_12;
    }
    return (*l_1222);
}


/* ------------------------------------------ */
/* 
 * reads : g_982 g_846 g_1054 g_682 g_215 g_1029 g_80 g_1055 g_266 g_124 g_198 g_54 g_138 g_139 g_42 g_1095 g_236 g_1097 g_169 g_927 g_339 g_308 g_89 g_787 g_311 g_55 g_360
 * writes: g_169 g_360 g_846 g_943 g_1056 g_198 g_55 g_80 g_37 g_1148 g_1029 g_308
 */
static int32_t  func_17(int8_t  p_18, uint64_t  p_19, int8_t  p_20)
{ /* block id: 495 */
    uint64_t l_1064 = 1UL;
    float *l_1066 = (void*)0;
    float **l_1065[1][5] = {{&l_1066,&l_1066,&l_1066,&l_1066,&l_1066}};
    uint32_t l_1092[9][2][5] = {{{1UL,4294967291UL,0x16C63623L,8UL,0x16C63623L},{0xEA160922L,0xEA160922L,1UL,0UL,0xEBCC09A7L}},{{1UL,0UL,4294967291UL,1UL,1UL},{0UL,0xA8035074L,0UL,0x0F4705BCL,4294967295UL}},{{4294967291UL,0UL,1UL,4294967286UL,0xBC9FB651L},{1UL,0xEA160922L,0xEA160922L,1UL,0UL}},{{0x16C63623L,4294967291UL,1UL,0xBC9FB651L,0UL},{0xD5D264CAL,1UL,0UL,1UL,0xD5D264CAL}},{{4294967291UL,4294967286UL,4294967291UL,0xBC9FB651L,4294967288UL},{0xCFA7B1FDL,4294967294UL,1UL,1UL,4294967294UL}},{{1UL,1UL,0x16C63623L,4294967286UL,4294967288UL},{1UL,1UL,0xD5D264CAL,0x0F4705BCL,0xD5D264CAL}},{{4294967288UL,4294967288UL,4294967291UL,1UL,0UL},{1UL,0xEBCC09A7L,0xCFA7B1FDL,0UL,0UL}},{{1UL,4294967293UL,1UL,8UL,0xBC9FB651L},{0xCFA7B1FDL,0xEBCC09A7L,1UL,4294967294UL,4294967295UL}},{{4294967291UL,4294967288UL,4294967288UL,4294967291UL,1UL},{0xD5D264CAL,1UL,1UL,4294967295UL,0xEBCC09A7L}}};
    int32_t l_1106 = (-5L);
    int32_t l_1108 = (-1L);
    int32_t l_1110[1];
    int32_t *l_1126[9] = {&g_4,(void*)0,&g_4,(void*)0,&g_4,(void*)0,&g_4,(void*)0,&g_4};
    int32_t *l_1129 = (void*)0;
    int32_t **l_1150 = &l_1126[7];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1110[i] = 0x7563B933L;
lbl_1098:
    for (g_169 = 29; (g_169 < 6); g_169--)
    { /* block id: 498 */
        return p_19;
    }
    for (g_360 = 0; (g_360 >= 14); ++g_360)
    { /* block id: 503 */
        float *l_1051 = &g_846;
        int8_t *l_1061 = &g_943[4][3][0];
        int32_t l_1063 = (-3L);
        uint8_t l_1067 = 3UL;
        uint8_t *l_1072 = &g_198;
        uint8_t *l_1087 = &l_1067;
        int16_t *l_1094 = &g_80;
        uint32_t l_1096 = 0xEC939840L;
        int32_t l_1107[8] = {0xFEBC5EC0L,0xFEBC5EC0L,0xD3A87D7FL,0xFEBC5EC0L,0xFEBC5EC0L,0xD3A87D7FL,0xFEBC5EC0L,0xFEBC5EC0L};
        int32_t l_1109 = (-1L);
        int64_t l_1112 = 0x8A85FAD3866562AFLL;
        uint64_t l_1113[4];
        uint32_t * const l_1145 = (void*)0;
        const uint32_t *l_1147 = (void*)0;
        const uint32_t **l_1146[1][3][9] = {{{(void*)0,&l_1147,(void*)0,(void*)0,&l_1147,(void*)0,(void*)0,&l_1147,(void*)0},{&l_1147,&l_1147,&l_1147,&l_1147,&l_1147,&l_1147,&l_1147,&l_1147,(void*)0},{&l_1147,(void*)0,&l_1147,&l_1147,(void*)0,&l_1147,&l_1147,(void*)0,&l_1147}}};
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1113[i] = 0x930719EFD3348EE6LL;
        (*g_1055) = ((safe_rshift_func_int16_t_s_s(((safe_add_func_float_f_f(((safe_sub_func_float_f_f((0x7.F77359p-25 <= ((safe_mul_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((*l_1051) = (0x0.Dp-1 <= (*g_982))), (safe_sub_func_float_f_f(((g_1054 == (((safe_lshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s((0xB6A6L & 0x706DL), g_682)), ((*l_1061) = (-10L)))) , ((((~p_20) | l_1063) & p_18) && l_1064)) , l_1065[0][3])) , l_1063), p_18)))), 0x6.F77984p+28)), g_215)) != 0x5.2p+1)), p_20)) != g_1029), g_80)) , l_1067), 14)) , p_19);
        for (p_20 = 4; (p_20 >= 0); p_20 -= 1)
        { /* block id: 509 */
            return (*g_266);
        }
        if ((safe_mod_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_s(((*l_1072) |= g_124[5][0][0]), 7)) <= (safe_mul_func_int8_t_s_s((((((l_1064 > l_1064) | ((safe_rshift_func_int16_t_s_s((safe_mod_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(0xAAL, (safe_lshift_func_int16_t_s_u((0xF7L ^ (--(*l_1087))), 7)))), (((*g_54) = (safe_sub_func_uint16_t_u_u((l_1063 = p_18), l_1092[3][0][2]))) == p_20))), ((*l_1094) &= (!0xFB695BD1444E1706LL)))) | (*g_138)), g_42)), 0)) == l_1064)) , g_1095) && 0x6A8D03B39FF370D2LL) | 1L), g_236))), p_20)))
        { /* block id: 517 */
            float l_1101 = 0xB.0D8F7Cp+8;
            int32_t l_1103 = 0L;
            int32_t l_1104 = 0L;
            int32_t l_1105[10][1] = {{0x0220C6B7L},{0x2D4315E1L},{0x0220C6B7L},{0x2D4315E1L},{0x0220C6B7L},{0x2D4315E1L},{0x0220C6B7L},{0x2D4315E1L},{0x0220C6B7L},{0x2D4315E1L}};
            int8_t l_1111[1][3][1];
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_1111[i][j][k] = 0x58L;
                }
            }
            (*g_1097) = l_1096;
            if (g_215)
                goto lbl_1098;
            for (p_18 = (-4); (p_18 <= 0); p_18 = safe_add_func_uint16_t_u_u(p_18, 4))
            { /* block id: 522 */
                int32_t *l_1102[10][10] = {{(void*)0,&g_215,&g_215,&g_215,(void*)0,(void*)0,(void*)0,&g_215,&g_215,&g_215},{(void*)0,(void*)0,&l_1063,&g_215,&l_1063,(void*)0,(void*)0,(void*)0,&l_1063,&g_215},{&l_1063,&g_215,&l_1063,(void*)0,(void*)0,(void*)0,&l_1063,&g_215,&l_1063,(void*)0},{(void*)0,&g_215,&g_215,&g_215,(void*)0,(void*)0,(void*)0,&g_215,&g_215,&g_215},{(void*)0,(void*)0,&l_1063,&g_215,&l_1063,(void*)0,(void*)0,(void*)0,&l_1063,&g_215},{&l_1063,&g_215,&l_1063,(void*)0,(void*)0,(void*)0,&l_1063,&g_215,&l_1063,(void*)0},{(void*)0,&g_215,&g_215,&g_215,(void*)0,(void*)0,(void*)0,&g_215,&g_215,&g_215},{(void*)0,(void*)0,&l_1063,&g_215,&l_1063,(void*)0,(void*)0,(void*)0,&l_1063,&g_215},{&l_1063,&g_215,&l_1063,(void*)0,(void*)0,(void*)0,&l_1063,&g_215,&l_1063,(void*)0},{(void*)0,&g_215,&g_215,&g_215,(void*)0,(void*)0,(void*)0,&g_215,&g_215,&g_215}};
                float * const *l_1123[2][7] = {{&l_1066,&l_1066,&l_1066,&l_1066,&l_1066,&l_1066,&l_1066},{&l_1066,&l_1066,&l_1066,&l_1066,&l_1066,&l_1066,&l_1066}};
                int i, j;
                l_1113[3]--;
                l_1110[0] = (!(*g_138));
                if (g_1095)
                    goto lbl_1098;
                l_1103 = (g_169 || (safe_sub_func_uint16_t_u_u(0x9897L, (safe_mul_func_uint8_t_u_u((safe_add_func_uint8_t_u_u((l_1123[0][0] != (void*)0), g_215)), 1L)))));
            }
            l_1105[7][0] &= (~g_927);
        }
        else
        { /* block id: 529 */
            int32_t *l_1125 = &l_1110[0];
            int32_t **l_1128 = (void*)0;
            l_1129 = (l_1126[5] = l_1125);
            return p_20;
        }
        l_1109 |= ((safe_div_func_uint32_t_u_u(((0x8.3p-1 == (l_1107[4] = ((*g_311) = (!((safe_add_func_float_f_f((*g_339), ((*l_1051) = (-((void*)0 != &g_1055))))) > (g_1029 = (safe_mul_func_float_f_f((g_89 >= (((safe_sub_func_float_f_f((-(safe_sub_func_float_f_f(g_787[7], (safe_sub_func_float_f_f(0xF.F95E73p-20, ((l_1145 == (g_1148 = l_1129)) >= l_1112)))))), g_215)) > p_18) != 0xF.E39029p+48)), p_19)))))))) , (*g_54)), 1L)) < p_20);
    }
    (*l_1150) = &l_1110[0];
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_4 g_42 g_54 g_55 g_91 g_89 g_80 g_93 g_138 g_139 g_124 g_145 g_90 g_146 g_236 g_198 g_266 g_215 g_279 g_311 g_319 g_316 g_339 g_169 g_424 g_308 g_440 g_445 g_466 g_360 g_377 g_495 g_502 g_468 g_457 g_569 g_571 g_844 g_787 g_927 g_943 g_982 g_570 g_1013
 * writes: g_37 g_80 g_42 g_90 g_91 g_124 g_93 g_55 g_145 g_146 g_198 g_185 g_236 g_215 g_169 g_279 g_308 g_319 g_54 g_391 g_424 g_445 g_457 g_360 g_316 g_569 g_846 g_495 g_927 g_943 g_1029
 */
static int32_t  func_23(uint8_t  p_24, uint8_t  p_25, const int16_t  p_26)
{ /* block id: 2 */
    uint32_t l_35[1];
    int32_t *l_36 = &g_37;
    uint32_t *l_43 = &l_35[0];
    int32_t **l_191 = &g_146;
    uint32_t **l_853 = &g_54;
    uint32_t **l_855 = &g_54;
    uint8_t l_867 = 0x16L;
    int32_t **l_905 = &g_146;
    int32_t l_951 = 0xD750A3DAL;
    uint8_t l_955 = 0x50L;
    const int16_t l_960 = 0xC767L;
    int32_t l_981 = 9L;
    uint64_t *l_987 = &g_236;
    uint8_t l_1011[7] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    int64_t ***l_1033[1];
    int64_t ****l_1032 = &l_1033[0];
    int i;
    for (i = 0; i < 1; i++)
        l_35[i] = 0x9D6240C4L;
    for (i = 0; i < 1; i++)
        l_1033[i] = (void*)0;
    if (func_28(func_30((safe_add_func_uint32_t_u_u((((*l_36) = l_35[0]) && (((void*)0 == l_36) >= ((((((*l_36) >= (safe_mul_func_int8_t_s_s(((--(*l_43)) && (((p_24 <= (*l_36)) , 0x33E235BDL) , func_46(g_4, ((*l_191) = func_50(g_42, g_54, p_25)), l_36))), 4L))) >= 65529UL) , (void*)0) != (void*)0) > (*l_36)))), p_25)), g_89)))
    { /* block id: 421 */
        uint8_t l_850 = 0xFAL;
        int32_t l_866 = 0x99444335L;
        int64_t **l_868 = &g_570;
        uint32_t ***l_869[6] = {&g_391,&g_391,&g_391,&g_391,&g_391,&g_391};
        uint16_t l_870 = 0x9524L;
        int i;
        if (p_24)
        { /* block id: 422 */
            uint64_t *l_849[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t **l_854 = &l_43;
            uint8_t *l_858[9] = {&g_279[0][0][3],&g_279[0][0][3],(void*)0,&g_279[0][0][3],&g_279[0][0][3],(void*)0,&g_279[0][0][3],&g_279[0][0][3],(void*)0};
            int32_t l_859 = 0x56AC5E45L;
            int32_t l_871 = 0x2212A5EAL;
            int32_t *l_872 = &g_495;
            int i;
            (*l_872) |= (l_871 ^= (safe_sub_func_int8_t_s_s(((g_236 = (l_850 = (*g_138))) >= (&g_316[1][1][1] != &g_316[1][1][0])), (safe_lshift_func_uint8_t_u_s((((l_853 = &g_54) != (l_855 = l_854)) <= (((safe_mul_func_uint8_t_u_u((l_859 = g_93), (safe_rshift_func_uint8_t_u_u((((((((*l_36) ^= ((((safe_div_func_int64_t_s_s((&g_570 == ((safe_sub_func_uint16_t_u_u(l_866, (l_867 , g_787[6]))) , l_868)), 0xE823B1F9AB812316LL)) ^ 0x4B85L) == p_26) , p_26)) , l_869[1]) == &l_854) , 0xAC3EABCCL) < p_26) | p_26), l_870)))) <= 1UL) > 0L)), p_25)))));
        }
        else
        { /* block id: 431 */
            for (g_91 = 10; (g_91 >= 29); g_91 = safe_add_func_int16_t_s_s(g_91, 4))
            { /* block id: 434 */
                for (g_236 = 0; (g_236 == 28); ++g_236)
                { /* block id: 437 */
                    return (**l_191);
                }
            }
        }
    }
    else
    { /* block id: 442 */
        uint64_t l_877[7][3] = {{0xBF1C3E57C7D1AC97LL,9UL,5UL},{0xD5E16CA73B1DD9B2LL,0xD5E16CA73B1DD9B2LL,5UL},{9UL,0xBF1C3E57C7D1AC97LL,5UL},{0xBF1C3E57C7D1AC97LL,9UL,5UL},{0xD5E16CA73B1DD9B2LL,0xD5E16CA73B1DD9B2LL,5UL},{9UL,0xBF1C3E57C7D1AC97LL,5UL},{0xBF1C3E57C7D1AC97LL,9UL,5UL}};
        int32_t l_893[6][5] = {{(-6L),0x3B7BA43DL,(-6L),0x3B7BA43DL,(-6L)},{0x448734A6L,0x448734A6L,0x448734A6L,0x448734A6L,0x448734A6L},{(-6L),0x3B7BA43DL,(-6L),0x3B7BA43DL,(-6L)},{0x448734A6L,0x448734A6L,0x448734A6L,0x448734A6L,0x448734A6L},{(-6L),0x3B7BA43DL,(-6L),0x3B7BA43DL,(-6L)},{0x448734A6L,0x448734A6L,0x448734A6L,0x448734A6L,0x448734A6L}};
        uint32_t *l_964[8];
        uint64_t *l_988 = &l_877[5][2];
        int32_t l_989 = 1L;
        int i, j;
        for (i = 0; i < 8; i++)
            l_964[i] = (void*)0;
lbl_990:
        if (l_877[5][2])
        { /* block id: 443 */
            (*l_191) = (*l_191);
        }
        else
        { /* block id: 445 */
            int8_t *l_888 = &g_91;
            uint64_t *l_894 = &g_236;
            int32_t l_920[4];
            int16_t *l_979 = &g_42;
            int32_t l_980 = 0xB929E4E5L;
            int i;
            for (i = 0; i < 4; i++)
                l_920[i] = 2L;
            if ((safe_mul_func_uint8_t_u_u(g_55[3][5], ((*g_440) == (safe_mul_func_uint8_t_u_u((((**l_191) == 1L) <= (safe_add_func_uint64_t_u_u(((*l_894) |= ((g_316[1][1][0] >= (safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_s(((-1L) != ((*l_888) = 0xAEL)), l_877[5][2])), 4))) > (safe_div_func_uint32_t_u_u(((safe_add_func_int32_t_s_s((l_893[0][3] = ((*g_54) ^ (*l_36))), 5L)) == 0xF13AAEC4109855CBLL), p_24)))), (*l_36)))), 0x7CL))))))
            { /* block id: 449 */
                int32_t ***l_918 = &l_905;
                uint8_t *l_919 = &l_867;
                int16_t *l_921 = &g_80;
                uint32_t *l_922 = &l_35[0];
                float l_925[7] = {0x0.2p+1,0x0.2p+1,0x1.Bp-1,0x0.2p+1,0x0.2p+1,0x1.Bp-1,0x0.2p+1};
                uint32_t *l_926 = &g_927;
                int8_t *l_942[7][2][1] = {{{&g_943[1][7][1]},{&g_943[1][0][2]}},{{&g_943[1][0][2]},{&g_943[1][7][1]}},{{(void*)0},{&g_943[1][7][1]}},{{(void*)0},{&g_943[1][7][1]}},{{&g_943[1][0][2]},{&g_943[1][0][2]}},{{&g_943[1][7][1]},{(void*)0}},{{&g_943[1][7][1]},{(void*)0}}};
                int i, j, k;
                (*l_905) = (**l_918);
                (*l_36) &= (p_25 && (g_943[1][7][1] = ((*l_888) = (safe_div_func_int8_t_s_s((p_25 & (0x9894338A38AB27E3LL <= (((*l_926) |= p_26) , (safe_lshift_func_uint8_t_u_s(0x40L, 4))))), (safe_div_func_int32_t_s_s((p_26 ^ (safe_mul_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(p_25, 65535UL)), p_24)), p_26)), p_25)), g_4))), p_25)))))));
            }
            else
            { /* block id: 457 */
                int32_t *l_944 = &g_495;
                int32_t *l_945 = &l_920[0];
                int32_t *l_946 = &l_893[0][3];
                int32_t *l_947 = &l_893[3][0];
                int32_t *l_948 = &g_787[6];
                int32_t *l_949 = &l_920[0];
                int32_t *l_950[5][1];
                uint32_t l_952[9][6] = {{4294967295UL,4294967290UL,0x56581D91L,0UL,4294967295UL,1UL},{4294967292UL,0xF8F01CBBL,0xAED866B1L,4294967295UL,0xAED866B1L,0xF8F01CBBL},{4294967292UL,1UL,4294967295UL,0UL,0x56581D91L,4294967290UL},{4294967295UL,4294967295UL,4294967292UL,4294967294UL,4294967294UL,4294967292UL},{4294967295UL,4294967295UL,2UL,4294967295UL,0x56581D91L,4294967294UL},{0xF8F01CBBL,1UL,0UL,2UL,0xAED866B1L,2UL},{0UL,0xF8F01CBBL,0UL,4294967290UL,4294967295UL,4294967294UL},{4294967288UL,4294967290UL,2UL,7UL,4294967292UL,4294967292UL},{7UL,4294967292UL,4294967292UL,7UL,2UL,4294967290UL}};
                int i, j;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_950[i][j] = &l_920[0];
                }
                l_952[0][5]--;
                l_955++;
            }
            (*g_982) = ((safe_sub_func_float_f_f((l_960 , (((+(((safe_mod_func_int64_t_s_s(((void*)0 != l_964[6]), (((((safe_sub_func_float_f_f((((safe_sub_func_int32_t_s_s((p_25 & ((*l_36) = (((((((safe_sub_func_int16_t_s_s((-1L), 0x8F2FL)) , (safe_add_func_uint64_t_u_u((g_495 <= (safe_lshift_func_int16_t_s_s(((*l_979) = (l_920[0] ^= (safe_div_func_uint16_t_u_u(((((-1L) ^ (safe_add_func_int8_t_s_s(p_25, l_893[1][1]))) >= p_26) | l_893[5][1]), p_26)))), l_980))), p_26))) , &g_927) != l_36) && 0xB6CEC9C408085E66LL) == 3L) <= g_124[5][0][0]))), l_981)) , (*g_339)) < p_26), (-0x6.8p+1))) >= 0x0.Ep+1) == g_943[1][7][1]) , (-7L)) && p_24))) > l_893[0][3]) > 0x671EL)) , &g_42) != (void*)0)), (-0x3.Cp+1))) < 0x1.C73DBEp-87);
        }
        if ((((((*l_36) ^ ((*g_570) ^= (l_893[4][4] = (((**l_191) ^ (((((*g_266) = (safe_add_func_uint64_t_u_u(((*l_988) = ((*l_987) = (safe_rshift_func_uint8_t_u_s(p_24, ((void*)0 != l_987))))), p_24))) <= p_26) ^ (-2L)) & p_25)) & p_24)))) ^ l_989) , g_124[0][0][0]) & p_24))
        { /* block id: 471 */
            uint32_t l_1000 = 0x38503F3CL;
            uint16_t *l_1010[9] = {&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360};
            const int32_t *l_1012 = &g_215;
            int32_t ****l_1020 = &g_468;
            uint8_t *l_1021 = &g_279[0][0][3];
            float *l_1026 = &g_846;
            float *l_1027 = &g_308;
            float *l_1028 = &g_1029;
            int i;
            if (g_89)
                goto lbl_990;
            (*g_1013) = l_1012;
            (*l_1028) = ((0x1.3p+1 <= (safe_sub_func_float_f_f(((*l_1027) = ((((*l_1026) = ((0xB.CB5DDDp-88 == ((safe_mul_func_uint8_t_u_u(((*l_1021) ^= ((safe_sub_func_int16_t_s_s(0x5787L, ((*l_36) = ((void*)0 != l_1020)))) > ((*l_1012) ^ (*g_54)))), ((safe_mod_func_uint8_t_u_u(g_198, (safe_div_func_int64_t_s_s(((l_989 || l_877[1][0]) >= p_26), (**g_569))))) <= (**l_191)))) , 0x0.3p-1)) >= p_25)) <= p_24) <= l_989)), p_25))) > l_989);
        }
        else
        { /* block id: 482 */
            float *l_1030 = &g_308;
            float *l_1031 = &g_1029;
            (*l_905) = ((0x9.4F9C46p-11 == ((p_25 < ((*l_1031) = (((((*l_1030) = (l_893[5][0] = ((g_319 >= g_943[1][7][1]) , 0x5.00F517p+4))) <= p_24) > ((((*l_36) |= ((*l_987) = g_55[3][5])) , (*g_468)) == (void*)0)) >= p_26))) > 0x9.9BBA09p+16)) , &l_989);
            return p_26;
        }
    }
    (*l_36) |= (**l_905);
    (*l_1032) = (void*)0;
    return p_26;
}


/* ------------------------------------------ */
/* 
 * reads : g_468 g_146 g_4 g_377 g_844 g_339 g_308
 * writes: g_145 g_215 g_146 g_846 g_308
 */
static int32_t  func_28(int32_t * const  p_29)
{ /* block id: 407 */
    int32_t * const *l_825 = &g_146;
    int32_t **l_826[6];
    int32_t ***l_827[4][7][9] = {{{&l_826[0],&l_826[0],&l_826[4],(void*)0,&l_826[1],&l_826[0],&l_826[1],(void*)0,&l_826[3]},{&l_826[1],&l_826[5],(void*)0,&l_826[5],&l_826[1],&l_826[0],&l_826[2],&l_826[2],&l_826[0]},{&l_826[1],(void*)0,&l_826[4],&l_826[0],&l_826[0],&l_826[0],(void*)0,(void*)0,&l_826[3]},{&l_826[0],(void*)0,(void*)0,&l_826[4],&l_826[5],&l_826[0],&l_826[0],&l_826[0],&l_826[0]},{&l_826[0],&l_826[0],&l_826[4],&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[3]},{&l_826[5],&l_826[4],(void*)0,(void*)0,&l_826[0],&l_826[0],&l_826[2],&l_826[2],&l_826[0]},{&l_826[0],&l_826[0],&l_826[4],(void*)0,&l_826[1],&l_826[0],&l_826[1],(void*)0,&l_826[3]}},{{&l_826[1],&l_826[5],(void*)0,&l_826[5],&l_826[1],&l_826[0],&l_826[2],&l_826[2],&l_826[0]},{&l_826[1],(void*)0,&l_826[4],&l_826[0],&l_826[0],&l_826[0],(void*)0,(void*)0,&l_826[3]},{&l_826[0],(void*)0,(void*)0,&l_826[4],&l_826[0],&l_826[2],&l_826[5],&l_826[5],&l_826[2]},{&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],(void*)0,&l_826[0],(void*)0},{&l_826[0],&l_826[0],&l_826[2],(void*)0,(void*)0,&l_826[2],&l_826[1],(void*)0,&l_826[2]},{&l_826[0],(void*)0,&l_826[0],&l_826[0],(void*)0,&l_826[0],&l_826[0],&l_826[3],(void*)0},{&l_826[0],(void*)0,&l_826[2],(void*)0,&l_826[0],&l_826[2],(void*)0,&l_826[1],&l_826[2]}},{{(void*)0,&l_826[0],&l_826[0],(void*)0,&l_826[0],&l_826[0],&l_826[0],&l_826[5],(void*)0},{(void*)0,(void*)0,&l_826[2],&l_826[0],&l_826[0],&l_826[2],&l_826[5],&l_826[5],&l_826[2]},{&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],(void*)0,&l_826[0],(void*)0},{&l_826[0],&l_826[0],&l_826[2],(void*)0,(void*)0,&l_826[2],&l_826[1],(void*)0,&l_826[2]},{&l_826[0],(void*)0,&l_826[0],&l_826[0],(void*)0,&l_826[0],&l_826[0],&l_826[3],(void*)0},{&l_826[0],(void*)0,&l_826[2],(void*)0,&l_826[0],&l_826[2],(void*)0,&l_826[1],&l_826[2]},{(void*)0,&l_826[0],&l_826[0],(void*)0,&l_826[0],&l_826[0],&l_826[0],&l_826[5],(void*)0}},{{(void*)0,(void*)0,&l_826[2],&l_826[0],&l_826[0],&l_826[2],&l_826[5],&l_826[5],&l_826[2]},{&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],&l_826[0],(void*)0,&l_826[0],(void*)0},{&l_826[0],&l_826[0],&l_826[2],(void*)0,(void*)0,&l_826[2],&l_826[1],(void*)0,&l_826[2]},{&l_826[0],(void*)0,&l_826[0],&l_826[0],(void*)0,&l_826[0],&l_826[0],&l_826[3],(void*)0},{&l_826[0],(void*)0,&l_826[2],(void*)0,&l_826[0],&l_826[2],(void*)0,&l_826[1],&l_826[2]},{(void*)0,&l_826[0],&l_826[0],(void*)0,&l_826[0],&l_826[0],&l_826[0],&l_826[5],(void*)0},{(void*)0,(void*)0,&l_826[2],&l_826[0],&l_826[0],&l_826[2],&l_826[5],&l_826[5],&l_826[2]}}};
    int32_t **l_828 = &g_146;
    int32_t **l_829 = &g_146;
    uint8_t l_830 = 0xACL;
    int32_t *****l_834 = &g_467[1];
    float *l_839 = &g_308;
    uint16_t *l_840 = &g_360;
    int16_t *l_841[1][4][6];
    uint8_t l_842 = 0xEAL;
    float *l_845 = &g_846;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_826[i] = (void*)0;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 6; k++)
                l_841[i][j][k] = &g_80;
        }
    }
    l_830 |= ((*g_377) = (safe_add_func_uint8_t_u_u((l_825 != (l_829 = (l_828 = ((*g_468) = l_826[0])))), (**l_825))));
    (*g_844) = p_29;
    (*l_839) = ((*g_339) > ((*l_845) = 0x4.B9B904p+78));
    return (*p_29);
}


/* ------------------------------------------ */
/* 
 * reads : g_198 g_145 g_146
 * writes: g_198
 */
static int32_t * const  func_30(const uint8_t  p_31, int32_t  p_32)
{ /* block id: 276 */
    uint8_t l_600 = 4UL;
    int32_t *l_603 = (void*)0;
    int32_t l_604 = (-5L);
    const uint32_t *l_606 = &g_55[3][5];
    int32_t l_654 = 0L;
    int32_t l_657 = (-1L);
    int32_t l_660 = 7L;
    int32_t l_661 = 1L;
    int32_t l_662 = (-1L);
    int32_t l_666 = 0L;
    int32_t l_668 = (-1L);
    int32_t l_669 = 0x2B6240B7L;
    int32_t l_670[9] = {2L,9L,2L,9L,2L,9L,2L,9L,2L};
    int32_t ** const *l_716 = &g_145;
    int64_t l_765 = (-8L);
    float l_792 = (-0x2.Ep+1);
    int32_t *l_810[3][4][7] = {{{&l_660,&l_660,&l_660,&l_660,&l_660,&l_660,&l_660},{&l_669,&l_669,&l_669,&l_669,&l_669,&l_669,&l_669},{&l_660,&l_660,&l_660,&l_660,&l_660,&l_660,&l_660},{&l_669,&l_669,&l_669,&l_669,&l_669,&l_669,&l_669}},{{&l_660,&l_660,&l_660,&l_660,&l_660,&l_660,&l_660},{&l_669,&l_669,&l_669,&l_669,&l_669,&l_669,&l_669},{&l_660,&l_660,&l_660,&l_660,&l_660,&l_660,&l_660},{&l_669,&l_669,&l_669,&l_669,&l_669,&l_669,&l_669}},{{&l_660,&l_660,&l_660,&l_660,&l_660,&l_660,&l_660},{&l_669,&l_669,&l_669,&l_669,&l_669,&l_669,&l_669},{&l_660,&l_660,&l_660,&l_660,&l_660,&l_660,&l_660},{&l_669,&l_669,&l_669,&l_669,&l_669,&l_669,&l_669}}};
    int16_t l_811[7][2][4] = {{{0x60B3L,0x4562L,0L,1L},{0x60B3L,0x99A6L,0x97E5L,7L}},{{(-1L),1L,0x804AL,(-1L)},{0x32B0L,0L,0x15ADL,0x4562L}},{{0L,0x99A6L,0x99A6L,0L},{0x804AL,0x60B3L,0x33B6L,0xE82AL}},{{0x8276L,0xD39AL,0x15ADL,0x32B0L},{0xB202L,0xB190L,(-1L),0x32B0L}},{{(-1L),0xD39AL,(-1L),0xE82AL},{0x4562L,0x60B3L,0L,0L}},{{7L,0x99A6L,0xE82AL,0x4562L},{(-1L),0L,(-1L),(-1L)}},{{0xD39AL,1L,0x15ADL,7L},{1L,0x99A6L,0xB190L,1L}}};
    float l_812 = 0x8.DBA17Dp-18;
    float l_813 = 0xA.58251Fp-91;
    float l_814 = (-0x1.1p-1);
    uint8_t l_815 = 255UL;
    int64_t l_818 = 7L;
    int32_t l_819[10][10][2] = {{{1L,0x20C5F331L},{0xE1A5159CL,0x9AEF7998L},{(-1L),0x9AEF7998L},{0xE1A5159CL,0x20C5F331L},{1L,0xF633DBECL},{4L,1L},{(-7L),(-4L)},{0xDAE0EF65L,0xC56A6018L},{0x6026DFDCL,2L},{5L,(-1L)}},{{0x25696C7EL,(-7L)},{0L,0L},{0L,1L},{0x7839E82CL,1L},{0x2CB581B4L,0L},{(-4L),0x85C6F22BL},{0xF633DBECL,0xE567CE94L},{0xE476DB24L,0xE476DB24L},{0xDA7390A9L,1L},{0xF58CB7CDL,1L}},{{1L,0L},{0x83E27DA3L,1L},{0xE567CE94L,0x6026DFDCL},{0xE567CE94L,1L},{0x83E27DA3L,0L},{1L,1L},{0xF58CB7CDL,1L},{0xDA7390A9L,0xE476DB24L},{0xE476DB24L,0xE567CE94L},{0xF633DBECL,0x85C6F22BL}},{{(-4L),0L},{0x2CB581B4L,1L},{0x7839E82CL,0x81E92043L},{(-3L),1L},{(-1L),0x4A37264AL},{(-1L),0xE1A5159CL},{4L,0x20C5F331L},{1L,(-7L)},{0x9AEF7998L,1L},{0x4A37264AL,0x7839E82CL}},{{0xDA7390A9L,0x83E27DA3L},{(-4L),8L},{0xF58CB7CDL,2L},{0xE1A5159CL,2L},{0xF58CB7CDL,8L},{(-4L),0x83E27DA3L},{0xDA7390A9L,0x7839E82CL},{0x4A37264AL,1L},{0x9AEF7998L,(-7L)},{1L,0x20C5F331L}},{{4L,0xE1A5159CL},{(-1L),0x4A37264AL},{(-1L),1L},{(-3L),0x81E92043L},{0L,0x6026DFDCL},{7L,(-3L)},{1L,1L},{0x83E27DA3L,0xF633DBECL},{0x2CB581B4L,0x2CB581B4L},{1L,(-1L)}},{{0x85C6F22BL,(-4L)},{1L,(-1L)},{0L,1L},{0xF633DBECL,1L},{0xF633DBECL,1L},{0L,(-1L)},{1L,(-4L)},{0x85C6F22BL,(-1L)},{1L,0x2CB581B4L},{0x2CB581B4L,0xF633DBECL}},{{0x83E27DA3L,1L},{1L,(-3L)},{7L,0x6026DFDCL},{0L,0x81E92043L},{(-3L),1L},{(-1L),0x4A37264AL},{(-1L),0xE1A5159CL},{4L,0x20C5F331L},{1L,(-7L)},{0x9AEF7998L,1L}},{{0x4A37264AL,0x7839E82CL},{0xDA7390A9L,0x83E27DA3L},{(-4L),8L},{0xF58CB7CDL,2L},{0xE1A5159CL,2L},{0xF58CB7CDL,8L},{(-4L),0x83E27DA3L},{0xDA7390A9L,0x7839E82CL},{0x4A37264AL,1L},{0x9AEF7998L,(-7L)}},{{1L,0x20C5F331L},{4L,0xE1A5159CL},{(-1L),0x4A37264AL},{(-1L),1L},{(-3L),0x81E92043L},{0L,0x6026DFDCL},{7L,(-3L)},{1L,1L},{0x83E27DA3L,0xF633DBECL},{0x2CB581B4L,0x2CB581B4L}}};
    uint8_t l_820 = 0xF6L;
    int i, j, k;
lbl_806:
    for (g_198 = 0; (g_198 >= 13); ++g_198)
    { /* block id: 279 */
        float *l_574[5][4][2] = {{{&g_308,&g_308},{&g_308,&g_308},{&g_308,&g_308},{&g_308,(void*)0}},{{&g_308,(void*)0},{&g_308,&g_308},{&g_308,&g_308},{&g_308,&g_308}},{{&g_308,&g_308},{(void*)0,&g_308},{&g_308,&g_308},{&g_308,&g_308}},{{(void*)0,&g_308},{&g_308,&g_308},{&g_308,&g_308},{&g_308,&g_308}},{{&g_308,(void*)0},{&g_308,(void*)0},{&g_308,&g_308},{&g_308,&g_308}}};
        int32_t l_575 = 0xB7784B61L;
        int16_t l_576 = 0xDA8DL;
        int8_t l_648[3];
        int32_t l_650 = (-6L);
        int32_t l_655 = 0x801F6295L;
        int32_t l_658 = 4L;
        int32_t l_659 = (-1L);
        int16_t l_665 = 0L;
        int32_t l_667[4] = {0x1A3E9B58L,0x1A3E9B58L,0x1A3E9B58L,0x1A3E9B58L};
        uint64_t *l_696 = &g_457;
        uint64_t **l_695 = &l_696;
        float l_786 = 0x8.621FBBp+76;
        uint32_t ***l_804 = &g_391;
        uint32_t ****l_803[1];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_648[i] = 1L;
        for (i = 0; i < 1; i++)
            l_803[i] = &l_804;
        l_576 = (l_575 = 0x4.A70251p-67);
        if (g_198)
            goto lbl_806;
    }
    for (l_604 = 0; (l_604 <= 12); l_604 = safe_add_func_int64_t_s_s(l_604, 3))
    { /* block id: 401 */
        int32_t * const l_809 = &l_666;
        return (**l_716);
    }
    l_815--;
    --l_820;
    return (**l_716);
}


/* ------------------------------------------ */
/* 
 * reads : g_42 g_37 g_4 g_145 g_146 g_236 g_139 g_55 g_91 g_198 g_266 g_215 g_138 g_279 g_80 g_93 g_124 g_311 g_319 g_89 g_316 g_339 g_169 g_424 g_308 g_440 g_445 g_466 g_360 g_377 g_495 g_502 g_468 g_457 g_569 g_571
 * writes: g_42 g_198 g_185 g_93 g_236 g_91 g_215 g_169 g_80 g_279 g_308 g_319 g_54 g_391 g_146 g_424 g_445 g_457 g_360 g_316 g_569
 */
static int32_t  func_46(int32_t  p_47, int32_t * p_48, int32_t * p_49)
{ /* block id: 84 */
    int32_t l_204 = 7L;
    int32_t l_224 = 0x085C7284L;
    int32_t l_232 = 0xBAEE0F38L;
    int32_t l_233 = 0x96EB4E54L;
    int32_t l_234[3][9][2] = {{{3L,3L},{3L,1L},{0x4590AD85L,0xD00658ABL},{1L,0xD00658ABL},{0x4590AD85L,1L},{3L,3L},{1L,0x4590AD85L},{5L,3L},{0x4590AD85L,3L}},{{5L,0x4590AD85L},{1L,1L},{1L,0x4590AD85L},{5L,3L},{0x4590AD85L,3L},{5L,0x4590AD85L},{1L,1L},{1L,0x4590AD85L},{5L,3L}},{{0x4590AD85L,3L},{5L,0x4590AD85L},{1L,1L},{1L,0x4590AD85L},{5L,3L},{0x4590AD85L,3L},{5L,0x4590AD85L},{1L,1L},{1L,0x4590AD85L}}};
    uint8_t l_256 = 0x6BL;
    float l_280 = 0xF.C31715p+22;
    uint64_t l_301 = 0x2B20FDAC42A58B84LL;
    float l_317[9] = {0x9.0D848Cp+94,0x9.0D848Cp+94,0xA.F4D414p+13,0x9.0D848Cp+94,0x9.0D848Cp+94,0xA.F4D414p+13,0x9.0D848Cp+94,0x9.0D848Cp+94,0xA.F4D414p+13};
    uint32_t **l_394 = &g_54;
    uint32_t **l_395 = &g_54;
    uint32_t **l_396 = &g_54;
    int16_t l_455 = 0x66C7L;
    uint32_t l_518 = 0x28BFD3ABL;
    int16_t l_528 = 0x02AEL;
    uint16_t **l_539[8] = {&g_425[2],&g_425[2],&g_425[6],&g_425[2],&g_425[2],&g_425[6],&g_425[2],&g_425[2]};
    int i, j, k;
    for (g_42 = 0; (g_42 < 5); g_42++)
    { /* block id: 87 */
        const int16_t *l_196 = &g_42;
        uint8_t *l_197 = &g_198;
        int32_t ***l_206 = &g_145;
        int32_t ****l_205 = &l_206;
        uint32_t l_209[3];
        int32_t l_225 = 5L;
        int32_t l_227 = 0xD81846CFL;
        int32_t l_229 = 0xBA6F03AAL;
        int32_t l_235 = 1L;
        uint32_t *l_326 = &g_55[3][5];
        int32_t l_338[5] = {0x143D4C7EL,0x143D4C7EL,0x143D4C7EL,0x143D4C7EL,0x143D4C7EL};
        uint16_t *l_416 = &g_360;
        uint16_t **l_415 = &l_416;
        uint16_t *l_418 = &g_360;
        uint16_t **l_417 = &l_418;
        int16_t *l_419 = &g_80;
        uint16_t ***l_448 = &g_424;
        uint64_t l_553 = 0UL;
        int i;
        for (i = 0; i < 3; i++)
            l_209[i] = 0x79396580L;
        if (((safe_mul_func_uint8_t_u_u(((*l_197) = ((void*)0 == l_196)), (p_47 > (safe_mul_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(((!(((((void*)0 != l_197) >= (0x901D41C1D69C55FCLL ^ l_204)) | (*p_49)) == (((void*)0 != l_205) | p_47))) & l_204), (*p_48))), l_204))))) , (*p_48)))
        { /* block id: 89 */
            int32_t ****l_207 = &l_206;
            int32_t l_222 = 1L;
            int32_t l_223 = 0x4F32749CL;
            int32_t l_226 = 0x6723AC2EL;
            int32_t l_228 = (-1L);
            int32_t l_230 = 0xF70EB5F4L;
            int32_t l_231 = (-1L);
            uint32_t l_243 = 0xF38200EBL;
            int32_t **l_328 = &g_146;
            int16_t l_347 = 0x2D21L;
            if ((*p_48))
                break;
            for (p_47 = 0; (p_47 >= 0); p_47 -= 1)
            { /* block id: 93 */
                g_185 = l_207;
                return l_209[0];
            }
            for (g_93 = 23; (g_93 > 25); g_93 = safe_add_func_int16_t_s_s(g_93, 4))
            { /* block id: 99 */
                int32_t *l_214 = &g_215;
                int32_t *l_216 = &g_215;
                int32_t *l_217 = &g_215;
                int32_t *l_218 = &g_215;
                int32_t *l_219 = (void*)0;
                int32_t *l_220 = (void*)0;
                int32_t *l_221[6];
                int8_t *l_257 = &g_91;
                int16_t *l_258[7][4] = {{&g_80,&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80,&g_80},{&g_80,&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80,&g_80},{&g_80,&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80,&g_80}};
                int32_t l_259[10] = {0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L,0x08633E49L};
                int i, j;
                for (i = 0; i < 6; i++)
                    l_221[i] = (void*)0;
                for (l_204 = 19; (l_204 < 12); --l_204)
                { /* block id: 102 */
                    return (****l_207);
                }
                ++g_236;
                (*l_214) = (safe_lshift_func_uint16_t_u_s((p_47 < 18446744073709551609UL), (l_259[9] ^= ((((safe_lshift_func_int8_t_s_u(l_243, (safe_div_func_uint8_t_u_u(g_139, (((*l_257) ^= (safe_mul_func_uint16_t_u_u(g_55[3][5], (safe_add_func_uint64_t_u_u(((safe_add_func_uint32_t_u_u((l_234[1][8][0] , (safe_mod_func_int16_t_s_s(p_47, (safe_mod_func_int8_t_s_s(((void*)0 != &l_234[1][0][0]), l_256))))), 2L)) ^ (****l_207)), p_47))))) & 0xC5L))))) < g_198) , l_207) != (void*)0))));
            }
            for (l_231 = 0; (l_231 <= 1); l_231 += 1)
            { /* block id: 112 */
                uint64_t *l_273 = &g_169;
                int16_t *l_276 = (void*)0;
                int16_t *l_277 = &g_80;
                int32_t l_278 = 0xFCCACB04L;
                int64_t l_318 = 4L;
                float *l_348 = (void*)0;
                int32_t l_351 = (-1L);
                int32_t l_352 = 1L;
                int32_t l_353 = (-1L);
                int32_t l_354[6][4] = {{(-1L),0x10E3C8D5L,(-1L),0xB3F34CA0L},{(-1L),0xB3F34CA0L,0xB3F34CA0L,(-1L)},{(-1L),0xB3F34CA0L,(-5L),0xB3F34CA0L},{0xB3F34CA0L,0x10E3C8D5L,(-5L),(-5L)},{(-1L),(-1L),0xB3F34CA0L,(-5L)},{(-1L),0x10E3C8D5L,(-1L),0xB3F34CA0L}};
                int8_t l_358 = 6L;
                uint32_t ** const l_369 = &l_326;
                int i, j;
                for (l_204 = 0; (l_204 <= 7); l_204 += 1)
                { /* block id: 115 */
                    for (l_256 = 1; (l_256 <= 7); l_256 += 1)
                    { /* block id: 118 */
                        uint64_t *l_263 = &g_236;
                        int i, j, k;
                        (*g_266) ^= ((safe_add_func_uint64_t_u_u((+(--(*l_263))), l_234[(l_231 + 1)][l_256][l_231])) || l_234[l_231][l_256][l_231]);
                    }
                }
                if ((&g_169 != ((((p_47 < (g_279[0][0][3] &= ((l_232 > (safe_lshift_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((g_55[3][0] & (safe_sub_func_uint8_t_u_u(((p_47 , ((*l_273) = p_47)) <= (safe_add_func_int16_t_s_s((((*l_277) = ((****l_207) , l_224)) ^ (0L | 0x40L)), g_4))), p_47))), l_278)), l_256))) != (*g_138)))) & l_204) > (-1L)) , &g_236)))
                { /* block id: 126 */
                    uint8_t l_281 = 0x8EL;
                    int32_t l_315 = 0x61FFB33DL;
                    int8_t *l_334 = &g_91;
                    int32_t *l_335[3][6][6] = {{{(void*)0,(void*)0,&l_229,&g_215,&l_227,&l_229},{(void*)0,&g_93,&l_278,(void*)0,(void*)0,&l_278},{&g_4,&g_4,&l_278,(void*)0,&g_93,&l_233},{(void*)0,&l_230,&l_229,&g_215,&l_226,&l_278},{(void*)0,(void*)0,&l_229,&l_232,&g_4,&l_233},{&l_228,&l_232,&l_278,&l_230,&l_232,&l_278}},{{&l_230,&l_232,&l_278,&l_227,&g_4,&l_229},{&l_226,(void*)0,&l_229,(void*)0,&l_226,&l_229},{&l_226,&l_230,&l_315,&l_227,&g_93,&l_235},{&l_230,&g_4,&l_226,&l_230,(void*)0,&l_235},{&l_228,&g_93,&l_315,&l_232,&l_227,&l_230},{&l_229,&l_224,&l_232,&l_225,(void*)0,&l_228}},{{&l_233,(void*)0,(void*)0,&l_229,&l_229,(void*)0},{&l_227,&l_227,&g_4,&l_229,(void*)0,(void*)0},{&l_233,&l_230,&l_228,&l_225,&l_235,&g_4},{&l_229,&l_233,&l_228,&l_227,&l_227,(void*)0},{(void*)0,&l_227,&g_4,&l_230,&l_227,(void*)0},{&l_230,&l_227,(void*)0,(void*)0,&l_227,&l_228}}};
                    int i, j, k;
                    if ((l_281 ^= (*g_146)))
                    { /* block id: 128 */
                        const float l_296 = 0xA.877698p+37;
                        float *l_306 = &l_280;
                        float *l_307 = &g_308;
                        float *l_309 = (void*)0;
                        l_278 = (((safe_sub_func_float_f_f(((g_80 == ((safe_mul_func_float_f_f(((((safe_mul_func_float_f_f(((*l_307) = ((*l_306) = (g_93 != (safe_add_func_float_f_f(g_215, (safe_div_func_float_f_f(((safe_mul_func_int8_t_s_s((((safe_sub_func_int32_t_s_s(((g_37 >= (safe_lshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(l_301, (safe_div_func_int16_t_s_s((safe_sub_func_int32_t_s_s((*p_48), ((((g_80 , 18446744073709551606UL) > p_47) >= p_47) ^ l_281))), l_281)))), (****l_207)))) != (*p_48)), (*p_48))) == p_47) > g_37), g_236)) , g_124[5][0][0]), p_47))))))), 0x5.Ap+1)) == 0xE.C5B960p-8) < l_281) != p_47), 0x0.5p-1)) != p_47)) < l_234[2][1][1]), (****l_205))) <= (-0x10.2p-1)) , p_47);
                    }
                    else
                    { /* block id: 132 */
                        float *l_310 = (void*)0;
                        int32_t *l_314[7] = {(void*)0,(void*)0,&l_234[1][1][1],(void*)0,(void*)0,&l_234[1][1][1],(void*)0};
                        uint32_t **l_327 = &g_54;
                        int i;
                        (*g_311) = p_47;
                        l_230 &= ((safe_sub_func_int8_t_s_s(g_37, l_278)) != p_47);
                        g_319++;
                        l_230 |= ((safe_div_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u(((g_89 , l_314[3]) != ((*l_327) = l_326)), p_47)), (((l_328 != ((0x090F102AL || (!1UL)) , ((p_47 == (*g_138)) , (void*)0))) , (**l_328)) && 1UL))) & g_279[0][0][8]);
                    }
                    l_278 = (safe_mod_func_uint64_t_u_u((l_234[1][0][0] ^= (safe_lshift_func_int8_t_s_s(((*l_334) = l_318), 1))), g_316[4][1][1]));
                }
                else
                { /* block id: 142 */
                    uint8_t l_336 = 0UL;
                    (*g_339) = (l_336 , (-l_338[4]));
                }
                if ((***l_206))
                    continue;
            }
        }
        else
        { /* block id: 176 */
            uint64_t *l_378 = &g_236;
            uint32_t **l_393 = &g_54;
            uint32_t ***l_392 = &l_393;
            uint32_t *l_399 = &g_55[1][7];
            int32_t l_400 = 0xAE3B4095L;
            const uint16_t * const l_401 = &g_360;
            int32_t *l_402 = (void*)0;
            int32_t *l_403 = &l_234[1][0][0];
            (***l_205) = ((((*l_378) = (p_47 <= 0UL)) == (safe_mod_func_int16_t_s_s((safe_div_func_int32_t_s_s(((((&g_198 == (g_169 , &g_279[0][0][6])) < (((*l_205) == (void*)0) >= (safe_mod_func_int16_t_s_s((+(((p_47 && ((+(safe_lshift_func_uint8_t_u_u(((l_396 = (l_395 = (l_394 = ((*l_392) = (g_391 = ((p_47 != (*g_138)) , (void*)0)))))) == &g_54), 2))) | (****l_205))) <= g_279[0][0][0]) < 0xA053FD55D3DFACCELL)), p_47)))) , (*g_138)) > 5UL), 6UL)), p_47))) , p_48);
            (*l_403) = (p_47 , (g_42 <= ((((l_399 != l_326) > ((0x706F268AL > l_400) >= 65535UL)) & 1UL) ^ (l_196 == l_401))));
        }
        if ((safe_rshift_func_uint8_t_u_s(((((safe_sub_func_int16_t_s_s((p_47 , ((p_47 , (((safe_add_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_u(0x4BCCL, 3)), (safe_unary_minus_func_int64_t_s((l_232 ^= g_4))))) != 0x4B4BL), ((*l_419) = (((*l_417) = ((*l_415) = (l_204 , &g_360))) != &g_360)))) <= g_279[0][0][3]) > l_233)) <= g_124[5][0][0])), g_279[0][0][3])) || (*p_48)) && (*g_138)) , g_80), p_47)))
        { /* block id: 190 */
            uint16_t **l_429 = &l_418;
            uint32_t l_437[9] = {0x2E903839L,4294967290UL,0x2E903839L,4294967290UL,0x2E903839L,4294967290UL,0x2E903839L,4294967290UL,0x2E903839L};
            uint16_t l_463 = 0x2727L;
            int64_t *l_491[7] = {&g_124[0][0][1],&g_124[5][0][0],&g_124[5][0][0],&g_124[0][0][1],&g_124[5][0][0],&g_124[5][0][0],&g_124[0][0][1]};
            int32_t * const l_494 = &g_495;
            int32_t * const *l_493 = &l_494;
            int32_t * const **l_492 = &l_493;
            int32_t *l_496 = &l_225;
            int i;
            if ((*p_48))
                break;
            for (g_91 = 1; (g_91 <= 4); g_91 += 1)
            { /* block id: 194 */
                uint16_t ***l_426 = &l_417;
                uint16_t ***l_427 = (void*)0;
                uint16_t ***l_428 = &g_424;
                uint16_t **l_430 = &g_425[7];
                uint16_t ***l_433 = (void*)0;
                uint16_t ***l_434 = &l_415;
                uint16_t **l_436 = (void*)0;
                uint16_t ***l_435 = &l_436;
                float *l_438 = &l_317[1];
                int32_t l_473 = 0x41F01BC5L;
                int32_t l_474 = 0x422183DCL;
                int i;
                (*l_438) = (safe_div_func_float_f_f((safe_add_func_float_f_f((((*l_428) = ((*l_426) = g_424)) != (l_430 = l_429)), (l_234[1][0][0] = ((l_338[g_91] > ((((l_338[g_91] < (safe_mod_func_int16_t_s_s((g_124[3][0][1] > l_234[2][4][0]), ((l_204 < ((((*l_434) = l_429) != ((*l_435) = &g_425[4])) , 3UL)) ^ 0xF2B4DD08391A86DALL)))) || p_47) , 0xD.0187A2p-79) == (*g_339))) <= l_437[0])))), 0x3.A9094Dp+75));
                for (g_93 = 3; (g_93 >= 1); g_93 -= 1)
                { /* block id: 204 */
                    uint8_t l_439 = 0xB9L;
                    (*g_440) = (l_439 = 0x22128A1CL);
                }
                for (l_301 = 0; (l_301 <= 4); l_301 += 1)
                { /* block id: 210 */
                    uint16_t ****l_446 = &l_435;
                    uint16_t ****l_447[6][5] = {{(void*)0,&l_428,(void*)0,(void*)0,&l_428},{&l_428,(void*)0,(void*)0,&l_428,(void*)0},{&l_428,&l_428,&l_428,&l_428,&l_428},{(void*)0,&l_428,(void*)0,(void*)0,&l_428},{&l_428,(void*)0,(void*)0,&l_428,(void*)0},{&l_428,&l_428,&l_428,&l_428,&l_428}};
                    int8_t *l_456[1][4] = {{&g_91,&g_91,&g_91,&g_91}};
                    int32_t l_458 = 1L;
                    int i, j;
                    if ((((((0x4AL ^ g_124[0][0][3]) , 65530UL) >= ((safe_mul_func_int16_t_s_s((((p_47 , (safe_add_func_int8_t_s_s(((g_445 = &l_436) == ((*l_446) = &g_424)), l_338[g_91]))) > ((l_448 = &g_424) == (((safe_mul_func_int8_t_s_s((g_457 = (((safe_rshift_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((***l_206), 0x7BFCL)), l_455)) <= (***l_206)) == (-1L))), p_47)) >= l_437[0]) , &g_424))) || l_458), p_47)) == p_47)) , 0xBE45L) || l_458))
                    { /* block id: 215 */
                        int32_t *****l_469 = &g_467[6];
                        int32_t *****l_470 = &g_467[2];
                        uint64_t *l_471 = (void*)0;
                        uint64_t *l_472[9];
                        int i;
                        for (i = 0; i < 9; i++)
                            l_472[i] = &g_169;
                        l_232 &= (safe_lshift_func_uint8_t_u_s((safe_add_func_int32_t_s_s((((*l_428) = (*g_445)) != (void*)0), p_47)), ((((((((l_463 | ((safe_mod_func_int16_t_s_s(((l_469 = g_466) != l_470), ((l_474 = ((l_473 = 1UL) > g_55[6][5])) || 0x1AL))) == (****l_205))) ^ l_463) , p_47) != p_47) && g_360) , p_47) < l_463) >= (*g_138))));
                    }
                    else
                    { /* block id: 221 */
                        int16_t l_475 = (-8L);
                        l_475 &= 0x625011BEL;
                        (*l_438) = (*g_339);
                    }
                    for (l_233 = 4; (l_233 >= 0); l_233 -= 1)
                    { /* block id: 227 */
                        (***l_205) = (**l_206);
                        return (*g_377);
                    }
                }
            }
            (*l_496) &= (safe_sub_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(((p_47 | (safe_lshift_func_uint16_t_u_u(((*p_48) <= (safe_add_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((g_236 = (&p_48 != ((*l_492) = ((((g_279[0][0][3] < (safe_div_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((+(l_224 = p_47)) , (g_360 , (&g_391 == (void*)0))), 0x55L)), l_301))) | p_47) , g_215) , (void*)0)))), (*l_494))), g_55[3][5]))), l_455))) ^ (***l_206)), 0x26L)), g_37));
        }
        else
        { /* block id: 237 */
            int8_t *l_497 = (void*)0;
            uint64_t **l_513 = (void*)0;
            uint64_t *l_515[2];
            uint64_t **l_514 = &l_515[1];
            const int32_t l_516 = (-1L);
            int32_t l_517 = (-1L);
            int32_t l_519[6][5] = {{0x50B8D68BL,0x86F22048L,0x50B8D68BL,0x86F22048L,0x50B8D68BL},{0xB7038C9CL,0xB7038C9CL,0xB7038C9CL,0xB7038C9CL,0xB7038C9CL},{0x50B8D68BL,0x86F22048L,0x50B8D68BL,0x86F22048L,0x50B8D68BL},{0xB7038C9CL,0xB7038C9CL,0xB7038C9CL,0xB7038C9CL,0xB7038C9CL},{0x50B8D68BL,0x86F22048L,0x50B8D68BL,0x86F22048L,0x50B8D68BL},{0xB7038C9CL,0xB7038C9CL,0xB7038C9CL,0xB7038C9CL,0xB7038C9CL}};
            int32_t *l_540 = &l_224;
            uint32_t l_557 = 0x30C5CDF6L;
            const uint32_t *l_564 = &g_55[3][5];
            const uint32_t **l_563 = &l_564;
            const uint32_t ***l_562 = &l_563;
            int i, j;
            for (i = 0; i < 2; i++)
                l_515[i] = &g_169;
            (*g_377) = (((l_234[0][0][1] ^= g_80) < (g_279[0][0][3] = ((safe_mul_func_uint8_t_u_u(((*l_197) &= (safe_mod_func_uint16_t_u_u(g_502, (safe_rshift_func_int8_t_s_s((g_91 = (~(((l_232 = ((((((void*)0 != &g_424) && (***g_468)) , ((safe_rshift_func_uint8_t_u_u(0xD8L, 5)) > (251UL <= (safe_add_func_int8_t_s_s(((safe_unary_minus_func_uint32_t_u(p_47)) > ((safe_lshift_func_uint8_t_u_s((((((*l_514) = ((p_47 | 0xDA64L) , (void*)0)) != &l_301) , g_124[6][0][4]) | l_256), l_516)) ^ 0x7BL)), 0x59L))))) , p_47) , (*p_48))) == 7UL) > l_516))), 5))))), g_4)) & g_124[5][0][0]))) , l_234[0][8][1]);
            l_519[3][1] = (l_518 = l_517);
            if ((((safe_sub_func_int32_t_s_s(((*l_540) &= ((((safe_rshift_func_uint8_t_u_u((safe_add_func_int64_t_s_s(4L, (((safe_lshift_func_uint16_t_u_u(((l_233 = (l_528 < ((safe_lshift_func_uint8_t_u_s((+((p_47 , (void*)0) == (void*)0)), 0)) < l_234[1][0][0]))) < ((*l_416) = (((safe_rshift_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u((g_236 &= l_234[1][1][1]))), 0)) == (safe_div_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u(((void*)0 != l_539[6]), 14)) > (-6L)), 0x55F4L))) > p_47))), l_234[1][0][0])) & g_55[3][5]) ^ 0L))), (***l_206))) || p_47) , 0x067BCB3CL) , (*p_49))), (*g_146))) != 0x6D584A5DL) ^ p_47))
            { /* block id: 251 */
                int8_t l_541[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_541[i] = 0xC1L;
                if (l_541[1])
                    break;
                if ((****l_205))
                    continue;
            }
            else
            { /* block id: 254 */
                int8_t *l_546 = (void*)0;
                int8_t *l_554 = &g_91;
                int32_t l_568 = 0x1EB0AC21L;
                (*l_540) = ((safe_mul_func_uint8_t_u_u(((0UL & (safe_lshift_func_int8_t_s_u(((*l_554) = ((l_546 == &g_91) && ((safe_sub_func_uint64_t_u_u((*g_138), (&g_198 != ((safe_div_func_uint32_t_u_u((9UL && (safe_mul_func_uint16_t_u_u(p_47, 4L))), ((l_553 , p_47) || 0xA466L))) , &g_279[0][0][3])))) & p_47))), g_457))) ^ 253UL), p_47)) <= 0x97E6BE9ACBB18AA3LL);
                for (l_225 = 0; (l_225 == 25); l_225 = safe_add_func_uint32_t_u_u(l_225, 4))
                { /* block id: 259 */
                    if (l_557)
                        break;
                    for (g_93 = 23; (g_93 <= (-19)); g_93--)
                    { /* block id: 263 */
                        uint32_t * const *l_561[6][4][10] = {{{&l_326,&g_54,&g_54,&g_54,&l_326,&g_54,&l_326,&g_54,&g_54,&g_54},{&l_326,(void*)0,&g_54,&g_54,&g_54,(void*)0,&l_326,(void*)0,&g_54,&g_54},{&l_326,&g_54,&l_326,(void*)0,&l_326,(void*)0,&l_326,&g_54,&l_326,(void*)0},{&l_326,&g_54,(void*)0,&g_54,&l_326,&g_54,&l_326,&g_54,(void*)0,&g_54}},{{&l_326,(void*)0,&l_326,&g_54,&l_326,(void*)0,&l_326,(void*)0,&l_326,&g_54},{&g_54,&g_54,&g_54,(void*)0,&l_326,(void*)0,&g_54,&g_54,&g_54,(void*)0},{&l_326,(void*)0,&l_326,(void*)0,&l_326,&g_54,&l_326,(void*)0,&l_326,(void*)0},{&g_54,&g_54,(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0}},{{&g_54,(void*)0,&g_54,&g_54,&l_326,&g_54,&g_54,(void*)0,&g_54,&g_54},{&g_54,(void*)0,&l_326,(void*)0,&g_54,&g_54,&g_54,(void*)0,&l_326,(void*)0},{&l_326,&g_54,&g_54,(void*)0,&g_54,&g_54,&l_326,&g_54,&g_54,(void*)0},{(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0,(void*)0,&g_54}},{{&l_326,(void*)0,&l_326,(void*)0,&l_326,&g_54,&l_326,(void*)0,&l_326,(void*)0},{&g_54,&g_54,(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0},{&g_54,(void*)0,&g_54,&g_54,&l_326,&g_54,&g_54,(void*)0,&g_54,&g_54},{&g_54,(void*)0,&l_326,(void*)0,&g_54,&g_54,&g_54,(void*)0,&l_326,(void*)0}},{{&l_326,&g_54,&g_54,(void*)0,&g_54,&g_54,&l_326,&g_54,&g_54,(void*)0},{(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0,(void*)0,&g_54},{&l_326,(void*)0,&l_326,(void*)0,&l_326,&g_54,&l_326,(void*)0,&l_326,(void*)0},{&g_54,&g_54,(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0}},{{&g_54,(void*)0,&g_54,&g_54,&l_326,&g_54,&g_54,(void*)0,&g_54,&g_54},{&g_54,(void*)0,&l_326,(void*)0,&g_54,&g_54,&g_54,(void*)0,&l_326,(void*)0},{&l_326,&g_54,&g_54,(void*)0,&g_54,&g_54,&l_326,&g_54,&g_54,(void*)0},{(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0,(void*)0,&g_54}}};
                        uint32_t * const **l_560[10][2] = {{&l_561[2][0][9],&l_561[2][0][9]},{&l_561[2][0][9],&l_561[2][0][9]},{&l_561[4][3][0],&l_561[2][0][9]},{&l_561[4][3][0],&l_561[2][0][9]},{&l_561[2][0][9],&l_561[2][0][9]},{&l_561[2][0][9],&l_561[2][0][9]},{&l_561[2][0][9],&l_561[2][0][9]},{&l_561[2][0][9],&l_561[3][2][5]},{&l_561[3][2][5],&l_561[2][0][9]},{&l_561[2][0][9],&l_561[2][0][9]}};
                        int32_t *l_567[5][8] = {{&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0},{&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0},{&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0},{&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0},{&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0,&l_204,(void*)0}};
                        int i, j, k;
                        l_568 &= ((g_316[4][0][0] = ((*l_540) = ((l_560[5][1] == l_562) & (safe_mul_func_int8_t_s_s(p_47, ((*g_138) && (((((((p_47 , (*l_540)) || (p_47 ^ (0x14L && ((p_47 | (*l_540)) != 0x19F6L)))) && p_47) & 0UL) & p_47) && (*p_49)) , (*g_138)))))))) , 0x0FFA9D9DL);
                    }
                    return (*g_440);
                }
                if ((*g_146))
                    continue;
            }
        }
        (*g_571) = g_569;
    }
    return l_455;
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_55 g_4 g_91 g_89 g_80 g_93 g_138 g_42 g_139 g_124 g_54 g_145 g_90 g_146
 * writes: g_80 g_42 g_90 g_91 g_124 g_93 g_55 g_145
 */
static int32_t * func_50(int32_t  p_51, uint32_t * p_52, uint32_t  p_53)
{ /* block id: 5 */
    int32_t l_70 = (-7L);
    uint8_t l_83 = 0xCAL;
    int32_t *l_190 = (void*)0;
    for (p_53 = 0; (p_53 < 48); ++p_53)
    { /* block id: 8 */
        int32_t l_69[6] = {0xC70A1111L,0xC70A1111L,0xC70A1111L,0xC70A1111L,0xC70A1111L,0xC70A1111L};
        int16_t *l_81 = (void*)0;
        int16_t *l_82 = &g_42;
        int i;
        l_190 = func_58(&g_55[3][5], (func_63((safe_div_func_uint16_t_u_u(l_69[2], ((((((((((((l_70 , (safe_sub_func_int16_t_s_s(((void*)0 != &g_4), (safe_div_func_int64_t_s_s((safe_sub_func_uint8_t_u_u(0xBEL, g_37)), ((((safe_unary_minus_func_int64_t_s((g_55[3][5] , (safe_lshift_func_int16_t_s_s(((*l_82) = (((((((g_80 = (p_53 & p_51)) < g_55[2][1]) >= g_55[5][4]) & 4294967288UL) > 1L) , g_55[3][6]) != 255UL)), g_37))))) , (-1L)) | l_83) , g_37)))))) , p_52) == (void*)0) != l_69[5]) ^ p_51) ^ l_70) | p_53) && l_83) | g_4) , l_70) || 0L) && (-1L)))), l_69[5], p_53) , &p_51), g_139, &g_4);
    }
    return (*g_145);
}


/* ------------------------------------------ */
/* 
 * reads : g_42 g_145 g_146
 * writes: g_42
 */
static int32_t * func_58(const uint32_t * p_59, const int32_t * p_60, uint16_t  p_61, int32_t * p_62)
{ /* block id: 72 */
    int32_t ***l_187 = &g_145;
    int32_t ****l_186 = &l_187;
    (*l_186) = &g_145;
    for (g_42 = (-26); (g_42 <= (-27)); g_42 = safe_sub_func_uint8_t_u_u(g_42, 9))
    { /* block id: 76 */
        return p_62;
    }
    return (**l_187);
}


/* ------------------------------------------ */
/* 
 * reads : g_55 g_4 g_91 g_37 g_89 g_80 g_93 g_42 g_138 g_139 g_124 g_54 g_145 g_90 g_146
 * writes: g_90 g_91 g_42 g_124 g_93 g_55 g_145 g_80
 */
static uint64_t  func_63(int64_t  p_64, int64_t  p_65, float  p_66)
{ /* block id: 11 */
    const uint8_t l_84 = 0x63L;
    const int32_t *l_88[2][6] = {{&g_89,&g_89,&g_89,&g_89,&g_89,&g_89},{&g_89,&g_89,&g_89,&g_89,&g_89,&g_89}};
    const int32_t **l_87[8] = {&l_88[1][1],&l_88[0][2],&l_88[1][1],&l_88[0][2],&l_88[1][1],&l_88[0][2],&l_88[1][1],&l_88[0][2]};
    int32_t l_95 = 0x01A4A5B4L;
    int16_t * const l_121 = &g_80;
    float l_140 = 0x5.7B996Fp+95;
    int32_t l_180[4][4] = {{0xDD84C012L,(-8L),0xDD84C012L,0xDD84C012L},{(-8L),(-8L),0x8824DB70L,(-8L)},{(-8L),0xDD84C012L,0xDD84C012L,(-8L)},{0xDD84C012L,(-8L),0xDD84C012L,0xDD84C012L}};
    int i, j;
    g_91 |= (l_84 < (safe_sub_func_int16_t_s_s((l_84 >= g_55[3][5]), (((g_90[1] = &g_37) != (void*)0) <= g_4))));
    if ((0x3E099908L && 1UL))
    { /* block id: 14 */
        int32_t *l_92 = &g_93;
        int16_t *l_94 = &g_42;
        uint64_t l_122 = 0UL;
        uint64_t *l_123[7];
        int i;
        for (i = 0; i < 7; i++)
            l_123[i] = &l_122;
        l_92 = &g_37;
        g_93 |= ((l_95 |= ((*l_94) = 0xDD93L)) & (safe_add_func_int8_t_s_s(((*l_92) || (g_4 == (0x8F1F7CC5E97E1C57LL || ((g_124[5][0][0] = ((safe_lshift_func_int16_t_s_u(((safe_mul_func_float_f_f(((safe_sub_func_int8_t_s_s(((safe_add_func_int8_t_s_s((((safe_div_func_int8_t_s_s(g_89, ((safe_sub_func_uint8_t_u_u((p_64 , (safe_mod_func_uint32_t_u_u(3UL, (safe_rshift_func_uint8_t_u_u((g_55[3][1] > ((safe_unary_minus_func_uint8_t_u((safe_mod_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s((l_121 == (void*)0), (*l_92))) , (*l_92)), 9UL)))) != 0UL)), 6))))), l_122)) , 0x4AL))) , l_92) != (void*)0), 5UL)) ^ p_64), 0x25L)) , g_4), g_37)) , g_89), 6)) && p_64)) | (*l_92))))), g_80)));
    }
    else
    { /* block id: 20 */
        uint32_t l_132[5] = {7UL,7UL,7UL,7UL,7UL};
        int32_t l_158 = 0x89A78842L;
        int32_t * const *l_159 = &g_146;
        uint8_t l_182 = 0x62L;
        int i;
        for (p_64 = 1; (p_64 >= 0); p_64 -= 1)
        { /* block id: 23 */
            int32_t l_125 = (-1L);
            int32_t *l_129 = &g_93;
            int16_t *l_165 = &g_42;
            int32_t l_181[5][3];
            int i, j;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 3; j++)
                    l_181[i][j] = (-3L);
            }
            if (l_125)
                break;
            (*l_129) = (safe_add_func_uint64_t_u_u(p_65, ((void*)0 != &g_55[3][5])));
            for (g_91 = 1; (g_91 >= 0); g_91 -= 1)
            { /* block id: 28 */
                int32_t ***l_147 = &g_145;
                int32_t l_162 = 0x56080F64L;
                uint64_t *l_168 = &g_169;
                int32_t l_172 = 0x3D49D56FL;
                int i, j;
                for (g_42 = 7; (g_42 >= 0); g_42 -= 1)
                { /* block id: 31 */
                    int i, j;
                    return g_55[(p_64 + 4)][(p_64 + 6)];
                }
                for (g_42 = 1; (g_42 >= 0); g_42 -= 1)
                { /* block id: 36 */
                    int32_t *l_137 = &l_95;
                    int i, j;
                    (*l_129) = (safe_add_func_int8_t_s_s(((l_132[2] , l_121) != (void*)0), (((safe_mul_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s(((l_137 = l_137) != ((((p_64 || p_64) || p_65) | (g_138 == &g_139)) , l_88[g_42][(g_42 + 1)])), g_139)) < g_139), g_124[7][0][4])) & 1UL) , g_80)));
                }
                l_158 = (safe_lshift_func_uint8_t_u_s((!(((((*g_54) = p_64) , ((&g_80 == ((+p_65) , l_121)) , &l_88[g_91][p_64])) != ((*l_147) = g_145)) || ((((-2L) ^ ((safe_add_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((g_90[(p_64 + 2)] == ((safe_add_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((safe_rshift_func_int16_t_s_s(g_89, (***l_147))) >= 0x0A8A3E14BFD0F756LL), (*g_138))), p_64)) , &g_55[1][1])), g_42)), g_55[2][4])) & p_65)) != p_65) || (-1L)))), 7));
                if ((*l_129))
                    continue;
                for (g_80 = 0; (g_80 <= 5); g_80 += 1)
                { /* block id: 46 */
                    int8_t l_170 = 0x5FL;
                    int32_t *l_171[4];
                    int i;
                    for (i = 0; i < 4; i++)
                        l_171[i] = &l_125;
                    l_172 &= ((*g_138) != ((((l_159 != (void*)0) || (safe_mod_func_int32_t_s_s((l_162 = (g_93 = 0x272FD4B8L)), (safe_mul_func_int8_t_s_s((&g_42 == l_165), ((safe_lshift_func_uint16_t_u_u(((((void*)0 == l_168) , &g_55[5][1]) == (void*)0), 14)) <= p_64)))))) != l_170) , 1L));
                }
            }
            for (g_42 = 0; (g_42 <= 0); g_42 += 1)
            { /* block id: 54 */
                int32_t *l_173 = (void*)0;
                int32_t *l_174 = &l_95;
                int32_t *l_175 = &l_125;
                int32_t *l_176 = &l_158;
                int32_t *l_177 = &l_158;
                int32_t *l_178 = &l_158;
                int32_t *l_179[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_179[i] = &l_125;
                for (l_158 = 1; (l_158 >= 0); l_158 -= 1)
                { /* block id: 57 */
                    int i, j, k;
                    return g_124[(g_42 + 2)][g_42][(g_42 + 4)];
                }
                l_182++;
                for (l_182 = 0; (l_182 <= 0); l_182 += 1)
                { /* block id: 63 */
                    int i, j, k;
                    if (g_124[(l_182 + 4)][l_182][(p_64 + 3)])
                        break;
                    if (p_64)
                        break;
                }
            }
        }
        return p_64;
    }
    return (*g_138);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_37, "g_37", print_hash_value);
    transparent_crc(g_42, "g_42", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_55[i][j], "g_55[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_80, "g_80", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_91, "g_91", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_124[i][j][k], "g_124[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_139, "g_139", print_hash_value);
    transparent_crc(g_169, "g_169", print_hash_value);
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_215, "g_215", print_hash_value);
    transparent_crc(g_236, "g_236", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_279[i][j][k], "g_279[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_308, sizeof(g_308), "g_308", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_316[i][j][k], "g_316[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_319, "g_319", print_hash_value);
    transparent_crc(g_360, "g_360", print_hash_value);
    transparent_crc(g_457, "g_457", print_hash_value);
    transparent_crc(g_495, "g_495", print_hash_value);
    transparent_crc(g_502, "g_502", print_hash_value);
    transparent_crc(g_663, "g_663", print_hash_value);
    transparent_crc(g_682, "g_682", print_hash_value);
    transparent_crc(g_762, "g_762", print_hash_value);
    transparent_crc(g_769, "g_769", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_787[i], "g_787[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_846, sizeof(g_846), "g_846", print_hash_value);
    transparent_crc(g_927, "g_927", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_943[i][j][k], "g_943[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc_bytes(&g_1009[i][j], sizeof(g_1009[i][j]), "g_1009[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1029, sizeof(g_1029), "g_1029", print_hash_value);
    transparent_crc(g_1034, "g_1034", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc_bytes(&g_1056[i], sizeof(g_1056[i]), "g_1056[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1095, "g_1095", print_hash_value);
    transparent_crc(g_1149, "g_1149", print_hash_value);
    transparent_crc(g_1186, "g_1186", print_hash_value);
    transparent_crc_bytes (&g_1234, sizeof(g_1234), "g_1234", print_hash_value);
    transparent_crc(g_1313, "g_1313", print_hash_value);
    transparent_crc(g_1368, "g_1368", print_hash_value);
    transparent_crc(g_1398, "g_1398", print_hash_value);
    transparent_crc(g_1473, "g_1473", print_hash_value);
    transparent_crc(g_1497, "g_1497", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 406
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 184
   depth: 2, occurrence: 51
   depth: 3, occurrence: 4
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 4
   depth: 10, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 4
   depth: 18, occurrence: 3
   depth: 19, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 4
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 2
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 3
   depth: 33, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 40, occurrence: 1

XXX total number of pointers: 353

XXX times a variable address is taken: 884
XXX times a pointer is dereferenced on RHS: 199
breakdown:
   depth: 1, occurrence: 143
   depth: 2, occurrence: 30
   depth: 3, occurrence: 13
   depth: 4, occurrence: 13
XXX times a pointer is dereferenced on LHS: 192
breakdown:
   depth: 1, occurrence: 182
   depth: 2, occurrence: 8
   depth: 3, occurrence: 2
XXX times a pointer is compared with null: 31
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 9
XXX times a pointer is qualified to be dereferenced: 5738

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 845
   level: 2, occurrence: 211
   level: 3, occurrence: 151
   level: 4, occurrence: 91
   level: 5, occurrence: 52
XXX number of pointers point to pointers: 130
XXX number of pointers point to scalars: 223
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.6
XXX average alias set size: 1.54

XXX times a non-volatile is read: 1397
XXX times a non-volatile is write: 627
XXX times a volatile is read: 73
XXX    times read thru a pointer: 10
XXX times a volatile is write: 31
XXX    times written thru a pointer: 9
XXX times a volatile is available for access: 937
XXX percentage of non-volatile access: 95.1

XXX forward jumps: 1
XXX backward jumps: 3

XXX stmts: 198
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 32
   depth: 2, occurrence: 41
   depth: 3, occurrence: 37
   depth: 4, occurrence: 24
   depth: 5, occurrence: 32

XXX percentage a fresh-made variable is used: 14.8
XXX percentage an existing variable is used: 85.2
********************* end of statistics **********************/


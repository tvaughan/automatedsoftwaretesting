/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      910873953
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int32_t  f0;
   volatile float  f1;
   volatile uint32_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_8[7] = {0xFE4B2830L,0xFE4B2830L,0xFE4B2830L,0xFE4B2830L,0xFE4B2830L,0xFE4B2830L,0xFE4B2830L};
static int32_t *g_7 = &g_8[1];
static int8_t g_26[6][7][6] = {{{8L,0L,9L,0L,0xD3L,0L},{0L,0xB6L,9L,0x8FL,0x8FL,9L},{2L,2L,0L,0xD3L,0L,9L},{0xD3L,0L,9L,0L,8L,0L},{(-1L),0xD3L,9L,1L,2L,9L},{0x8FL,1L,0L,0xB6L,(-1L),9L},{0xB6L,(-1L),9L,(-7L),4L,0x8FL}},{{(-4L),0x93L,1L,0x31L,0x97L,1L},{0x97L,3L,0x8FL,0x93L,(-1L),1L},{0x93L,(-1L),1L,(-4L),0xE4L,0x8FL},{(-1L),4L,1L,3L,3L,1L},{0x31L,0x31L,0x8FL,0xE4L,(-4L),1L},{0xE4L,(-4L),1L,(-1L),0x93L,0x8FL},{(-7L),0xE4L,1L,0x97L,0x31L,1L}},{{3L,0x97L,0x8FL,4L,(-7L),1L},{4L,(-7L),1L,(-7L),4L,0x8FL},{(-4L),0x93L,1L,0x31L,0x97L,1L},{0x97L,3L,0x8FL,0x93L,(-1L),1L},{0x93L,(-1L),1L,(-4L),0xE4L,0x8FL},{(-1L),4L,1L,3L,3L,1L},{0x31L,0x31L,0x8FL,0xE4L,(-4L),1L}},{{0xE4L,(-4L),1L,(-1L),0x93L,0x8FL},{(-7L),0xE4L,1L,0x97L,0x31L,1L},{3L,0x97L,0x8FL,4L,(-7L),1L},{4L,(-7L),1L,(-7L),4L,0x8FL},{(-4L),0x93L,1L,0x31L,0x97L,1L},{0x97L,3L,0x8FL,0x93L,(-1L),1L},{0x93L,(-1L),1L,(-4L),0xE4L,0x8FL}},{{(-1L),4L,1L,3L,3L,1L},{0x31L,0x31L,0x8FL,0xE4L,(-4L),1L},{0xE4L,(-4L),1L,(-1L),0x93L,0x8FL},{(-7L),0xE4L,1L,0x97L,0x31L,1L},{3L,0x97L,0x8FL,4L,(-7L),1L},{4L,(-7L),1L,(-7L),4L,0x8FL},{(-4L),0x93L,1L,0x31L,0x97L,1L}},{{0x97L,3L,0x8FL,0x93L,(-1L),1L},{0x93L,(-1L),1L,(-4L),0xE4L,0x8FL},{(-1L),4L,1L,3L,3L,1L},{0x31L,0x31L,3L,0x0EL,0L,0x97L},{0x0EL,0L,0x97L,1L,9L,3L},{0x73L,0x0EL,0x97L,(-6L),0x56L,0x97L},{(-2L),(-6L),3L,0L,0x73L,0x97L}}};
static uint8_t g_32[9][8] = {{5UL,249UL,252UL,0x92L,0xA9L,0x1CL,255UL,0x1CL},{0xFAL,9UL,0xA9L,9UL,0xFAL,0x47L,0x42L,255UL},{0UL,0x42L,5UL,5UL,0x43L,255UL,0x36L,9UL},{0UL,246UL,5UL,0UL,9UL,255UL,0x42L,252UL},{0x43L,0UL,0xA9L,255UL,5UL,5UL,255UL,0xA9L},{0x4BL,0x4BL,252UL,0x42L,255UL,9UL,0UL,5UL},{252UL,0x17L,9UL,0x36L,255UL,0x43L,5UL,5UL},{0x17L,0x32L,255UL,0x42L,0x47L,0xFAL,9UL,0xA9L},{0x92L,0xA9L,0x1CL,255UL,0x1CL,0xA9L,0x92L,252UL}};
static uint32_t g_50 = 18446744073709551615UL;
static volatile int32_t g_58 = 0L;/* VOLATILE GLOBAL g_58 */
static int32_t g_59 = 0L;
static volatile int32_t g_62 = (-1L);/* VOLATILE GLOBAL g_62 */
static volatile int32_t g_63[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static int32_t g_64 = 1L;
static uint8_t g_92 = 0UL;
static uint64_t g_95[1][9][2] = {{{0xAC934C6454C9BF6ALL,0xAC934C6454C9BF6ALL},{0xAC934C6454C9BF6ALL,0x709F62D42CB0EA5CLL},{0xAC934C6454C9BF6ALL,0xAC934C6454C9BF6ALL},{0xAC934C6454C9BF6ALL,0x709F62D42CB0EA5CLL},{0xAC934C6454C9BF6ALL,0xAC934C6454C9BF6ALL},{0xAC934C6454C9BF6ALL,0x709F62D42CB0EA5CLL},{0xAC934C6454C9BF6ALL,0xAC934C6454C9BF6ALL},{0xAC934C6454C9BF6ALL,0x709F62D42CB0EA5CLL},{0xAC934C6454C9BF6ALL,0xAC934C6454C9BF6ALL}}};
static union U0 g_150 = {0L};/* VOLATILE GLOBAL g_150 */
static uint32_t *g_160 = &g_50;
static uint32_t * volatile * volatile g_159[2] = {&g_160,&g_160};
static uint32_t g_174 = 0x1705F15DL;
static int32_t *g_176 = (void*)0;
static int32_t ** volatile g_200[1][7] = {{&g_176,&g_176,&g_176,&g_176,&g_176,&g_176,&g_176}};
static union U0 g_206[1][1] = {{{0x9424BF6EL}}};
static union U0 *g_205 = &g_206[0][0];
static uint32_t **g_211 = (void*)0;
static uint32_t ***g_210 = &g_211;
static uint32_t **** volatile g_209 = &g_210;/* VOLATILE GLOBAL g_209 */
static int16_t g_219 = 1L;
static union U0 ** volatile g_229 = &g_205;/* VOLATILE GLOBAL g_229 */
static int64_t g_330 = 0x855FFF41DDBBDD34LL;
static int16_t g_343 = (-6L);
static uint64_t g_344 = 18446744073709551615UL;
static union U0 **g_376 = &g_205;
static union U0 ***g_375 = &g_376;
static union U0 ****g_374 = &g_375;
static union U0 ***** volatile g_373 = &g_374;/* VOLATILE GLOBAL g_373 */
static int32_t g_379 = 0xB7F2A978L;
static volatile int32_t *g_407 = &g_63[6];
static volatile int32_t ** const  volatile g_406 = &g_407;/* VOLATILE GLOBAL g_406 */
static float g_443[10] = {0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18,0x0.B4E7AAp+18};
static float * volatile g_442 = &g_443[3];/* VOLATILE GLOBAL g_442 */
static int8_t g_498[10][8][3] = {{{0x4CL,(-5L),1L},{(-1L),(-1L),(-1L)},{(-1L),0xF1L,0x96L},{0L,0L,1L},{0L,0x28L,8L},{(-5L),7L,0xF1L},{0L,(-5L),(-5L)},{0L,(-1L),(-6L)}},{{(-1L),(-9L),0x50L},{(-1L),0L,0x3CL},{0x4CL,0x3CL,(-5L)},{1L,0xE5L,(-5L)},{(-6L),0xD6L,0x3CL},{0xE2L,0x50L,0x50L},{1L,1L,(-6L)},{0xE5L,0xE2L,(-5L)}},{{0x92L,0xDFL,0xF1L},{(-1L),(-2L),5L},{(-6L),1L,0x92L},{0x3CL,0x61L,0xE5L},{0xE2L,0xE2L,1L},{0x4CL,(-9L),0xE2L},{0x57L,0x50L,(-6L)},{1L,0L,1L}},{{1L,0xF1L,0x4CL},{0x57L,(-4L),(-1L)},{0x4CL,0x4CL,(-1L)},{0xE2L,1L,0L},{0x3CL,0xDFL,0L},{(-6L),0xEAL,(-5L)},{(-1L),0x3CL,0L},{0L,0x3DL,0L}},{{0L,0x57L,(-1L)},{0x96L,0xD6L,(-1L)},{(-2L),0x28L,0x4CL},{(-1L),(-1L),1L},{0xE2L,(-1L),(-6L)},{(-1L),0x28L,0xE2L},{1L,0xD6L,1L},{(-5L),0x57L,0xE5L}},{{1L,0x3DL,0x92L},{0x3DL,0x3CL,5L},{0xDFL,0xEAL,0x57L},{0x3DL,0xDFL,0x28L},{1L,1L,(-1L)},{(-5L),0x4CL,(-9L)},{1L,(-4L),0xF1L},{(-1L),0xF1L,0xDFL}},{{0xE2L,0L,0xDFL},{(-1L),0x50L,0xF1L},{(-2L),(-9L),(-9L)},{0x96L,0xE2L,(-1L)},{0L,0x61L,0x28L},{0L,1L,0x57L},{(-1L),(-2L),5L},{(-6L),1L,0x92L}},{{0x3CL,0x61L,0xE5L},{0xE2L,0xE2L,1L},{0x4CL,(-9L),0xE2L},{0x57L,0x50L,(-6L)},{1L,0L,1L},{1L,0xF1L,0x4CL},{0x57L,(-4L),(-1L)},{0x4CL,0x4CL,(-1L)}},{{0xE2L,1L,0L},{0x3CL,0xDFL,0L},{(-6L),0xEAL,(-5L)},{(-1L),0x3CL,0L},{0L,0x3DL,0L},{0L,0x57L,(-1L)},{0x96L,0xD6L,(-1L)},{(-2L),0x28L,0x4CL}},{{(-1L),(-1L),1L},{0xE2L,(-1L),(-6L)},{(-1L),0x28L,0xE2L},{1L,0xD6L,1L},{(-5L),0x57L,0xE5L},{1L,0x3DL,0x92L},{0x3DL,0x3CL,5L},{0xDFL,0xEAL,0x57L}}};
static int32_t g_511 = (-2L);
static float g_530 = (-0x1.8p+1);
static volatile uint16_t g_563 = 65526UL;/* VOLATILE GLOBAL g_563 */
static volatile float g_575 = 0x8.Fp-1;/* VOLATILE GLOBAL g_575 */
static volatile union U0 g_588 = {0xC7A944FEL};/* VOLATILE GLOBAL g_588 */
static volatile int8_t g_592 = 4L;/* VOLATILE GLOBAL g_592 */
static uint8_t *g_639 = &g_92;
static const float *g_680 = &g_530;
static const float **g_679 = &g_680;
static const float *** volatile g_678 = &g_679;/* VOLATILE GLOBAL g_678 */
static int8_t g_714[4][9][7] = {{{0x2AL,0x7AL,0x7AL,0x65L,0x65L,0x7AL,0x6AL},{(-7L),0x10L,0x96L,0L,0x96L,0x10L,(-7L)},{0x02L,0x65L,(-1L),(-1L),0x65L,0x02L,(-1L)},{(-10L),0L,0x30L,0L,(-10L),8L,(-10L)},{0x65L,(-1L),(-1L),0x65L,0x02L,(-1L),0x6AL},{0x96L,0L,0x96L,0x10L,(-7L),0x10L,0x96L},{0x65L,0x65L,0x7AL,0x6AL,0x65L,0x2AL,0x6AL},{(-10L),0x10L,0xAAL,0L,0xAAL,0x10L,(-10L)},{0x02L,0x6AL,(-1L),0x02L,0x65L,(-1L),(-1L)}},{{(-7L),0L,1L,0L,(-7L),8L,(-7L)},{0x65L,0x02L,(-1L),0x6AL,0x02L,0x02L,0x6AL},{0xAAL,0L,0xAAL,0x10L,(-10L),0x10L,0xAAL},{0x65L,0x6AL,0x7AL,0x65L,0x65L,0x7AL,0x6AL},{(-7L),0x10L,0x96L,0L,0x96L,0x10L,(-7L)},{0x02L,0x65L,(-1L),(-1L),0x65L,0x02L,(-1L)},{(-10L),0L,0x30L,0L,(-10L),8L,(-10L)},{0x65L,(-1L),(-1L),0x65L,0x02L,(-1L),0x6AL},{0x96L,0L,0x96L,0x10L,(-7L),0x10L,0x96L}},{{0x65L,0x65L,0x7AL,0x6AL,0x65L,0x2AL,0x6AL},{(-10L),0x10L,0xAAL,0L,0xAAL,0x10L,(-10L)},{0x02L,0x6AL,(-1L),0x02L,0x65L,(-1L),(-1L)},{(-7L),0L,1L,0L,(-7L),8L,(-7L)},{0x65L,0x02L,(-1L),0x6AL,0x02L,0x02L,0x6AL},{0xAAL,0L,0xAAL,0x10L,(-10L),0x10L,0xAAL},{0x65L,0x6AL,0x7AL,0x65L,0x65L,0x7AL,0x6AL},{(-7L),0x10L,0x96L,0L,0x96L,0x10L,(-7L)},{0x02L,0x65L,(-1L),(-1L),0x65L,0x02L,(-1L)}},{{(-10L),0L,0x30L,0L,0xAAL,0L,0xAAL},{0x02L,0x7AL,0x7AL,0x02L,0x2AL,0x7AL,(-1L)},{1L,0x10L,1L,8L,0x96L,8L,1L},{0x02L,0x02L,0x6AL,(-1L),0x02L,0x65L,(-1L)},{0xAAL,8L,0x30L,0x10L,0x30L,8L,0xAAL},{0x2AL,(-1L),0x7AL,0x2AL,0x02L,0x7AL,0x7AL},{0x96L,0x10L,(-7L),0x10L,0x96L,0L,0x96L},{0x02L,0x2AL,0x7AL,(-1L),0x2AL,0x2AL,(-1L)},{0x30L,0x10L,0x30L,8L,0xAAL,8L,0x30L}}};
static const uint32_t g_740[9][7] = {{0UL,18446744073709551615UL,18446744073709551615UL,0UL,18446744073709551615UL,0x1C8EDD1EL,0x1C8EDD1EL},{0x83D36227L,1UL,18446744073709551615UL,1UL,0x83D36227L,1UL,18446744073709551615UL},{18446744073709551615UL,0UL,18446744073709551615UL,18446744073709551615UL,0UL,18446744073709551615UL,0x1C8EDD1EL},{18446744073709551606UL,18446744073709551615UL,18446744073709551606UL,1UL,18446744073709551606UL,18446744073709551615UL,18446744073709551606UL},{18446744073709551615UL,18446744073709551615UL,0x1C8EDD1EL,0UL,0UL,0x1C8EDD1EL,18446744073709551615UL},{0x83D36227L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x83D36227L,18446744073709551615UL,18446744073709551615UL},{0UL,0UL,0x1C8EDD1EL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551606UL,1UL,18446744073709551606UL,18446744073709551615UL,18446744073709551606UL,1UL,18446744073709551606UL},{0UL,18446744073709551615UL,18446744073709551615UL,0UL,18446744073709551615UL,0x1C8EDD1EL,0x1C8EDD1EL}};
static int32_t **g_741 = &g_176;
static uint8_t g_749[1] = {0x1BL};
static uint16_t g_751[1][4][9] = {{{0x0A9CL,4UL,4UL,0x0A9CL,65532UL,0x6501L,0x36A3L,4UL,0x6501L},{65526UL,4UL,0UL,0x36A3L,65535UL,65535UL,0x36A3L,0UL,4UL},{0x36A3L,65532UL,0UL,65534UL,65532UL,4UL,0xF69FL,0UL,0UL},{65526UL,65535UL,4UL,65534UL,4UL,65535UL,65526UL,4UL,0UL}}};
static uint16_t g_755[5] = {0xB2FEL,0xB2FEL,0xB2FEL,0xB2FEL,0xB2FEL};
static uint32_t ****g_757 = (void*)0;
static uint32_t *****g_756 = &g_757;
static const int32_t g_766 = 0x323B8338L;
static int32_t * volatile * const * volatile * volatile * volatile g_905 = (void*)0;/* VOLATILE GLOBAL g_905 */
static int32_t ***g_939 = &g_741;
static int8_t ** volatile g_948 = (void*)0;/* VOLATILE GLOBAL g_948 */
static uint64_t g_1052[10] = {6UL,0xAC74C6C4A150BEE9LL,6UL,1UL,1UL,6UL,0xAC74C6C4A150BEE9LL,6UL,1UL,1UL};
static uint32_t g_1055 = 1UL;
static uint8_t g_1082 = 1UL;
static int32_t * volatile **** const g_1086 = (void*)0;
static int32_t g_1164 = 4L;
static int32_t ** volatile g_1166 = (void*)0;/* VOLATILE GLOBAL g_1166 */
static int32_t ** volatile g_1167[4][4][8] = {{{&g_176,&g_7,&g_176,(void*)0,&g_176,&g_7,&g_7,&g_176},{&g_7,(void*)0,(void*)0,&g_7,&g_176,&g_176,(void*)0,&g_7},{&g_176,&g_176,&g_7,&g_176,(void*)0,&g_176,&g_7,&g_176},{&g_7,&g_176,&g_176,&g_7,&g_7,&g_176,&g_176,&g_176}},{{&g_176,&g_7,(void*)0,(void*)0,&g_7,&g_176,&g_176,(void*)0},{(void*)0,(void*)0,&g_176,&g_176,&g_7,(void*)0,&g_7,&g_176},{&g_7,(void*)0,&g_7,&g_176,&g_176,(void*)0,(void*)0,(void*)0},{&g_176,&g_176,&g_7,(void*)0,(void*)0,&g_7,&g_176,&g_176}},{{&g_176,&g_176,&g_7,&g_7,&g_176,&g_176,&g_7,&g_176},{&g_7,&g_176,(void*)0,&g_176,&g_7,&g_176,(void*)0,&g_176},{(void*)0,&g_176,&g_176,&g_7,&g_7,&g_7,&g_7,&g_176},{&g_176,&g_176,&g_176,&g_176,&g_7,(void*)0,(void*)0,(void*)0}},{{&g_7,(void*)0,(void*)0,&g_176,(void*)0,(void*)0,&g_7,(void*)0},{(void*)0,(void*)0,&g_7,&g_176,&g_176,&g_176,&g_176,&g_176},{&g_7,&g_7,&g_7,&g_7,&g_176,&g_176,(void*)0,&g_176},{(void*)0,&g_176,&g_7,&g_176,(void*)0,&g_176,&g_7,&g_176}}};
static int32_t ** volatile g_1168 = &g_176;/* VOLATILE GLOBAL g_1168 */
static uint64_t *g_1182 = &g_344;
static uint64_t ** volatile g_1181 = &g_1182;/* VOLATILE GLOBAL g_1181 */
static union U0 g_1226 = {1L};/* VOLATILE GLOBAL g_1226 */
static volatile int32_t g_1266[7][10][3] = {{{0x5D67C34CL,0x5D67C34CL,0L},{0xE69D9BD5L,0x8B57B8ACL,0xF836C566L},{(-1L),0x037933B0L,(-6L)},{0x9061CB7DL,0xF836C566L,(-1L)},{4L,(-1L),(-6L)},{0x09A05551L,0x6DD85EFFL,0xF836C566L},{9L,(-5L),0L},{0xC0EB9A87L,0L,0L},{(-6L),0xD8F19699L,0x037933B0L},{4L,0xB07D6C07L,0xB07D6C07L}},{{1L,(-1L),(-1L)},{0xB07D6C07L,0x9212306FL,(-3L)},{1L,4L,7L},{0L,0x25DC2011L,0x09A05551L},{0x47B5AD66L,4L,0x42676019L},{0xE6988B52L,0x9212306FL,0xE69D9BD5L},{0x372AFE40L,(-1L),1L},{0L,0xB07D6C07L,(-6L)},{(-1L),0xD8F19699L,(-4L)},{0x9212306FL,0L,0x9212306FL}},{{(-1L),(-5L),0x372AFE40L},{0x4D41954BL,0x6DD85EFFL,0x613FB273L},{0L,(-1L),1L},{0x77C5C021L,0xF836C566L,0xF4EFC7B8L},{0L,0x037933B0L,6L},{0x4D41954BL,0x8B57B8ACL,0x25DC2011L},{(-1L),1L,0x47B5AD66L},{0x8B57B8ACL,0x77C5C021L,0x25DC2011L},{0x372AFE40L,(-1L),(-5L)},{0xF9D72DD1L,0xF9D72DD1L,0xF836C566L}},{{7L,(-1L),0x372AFE40L},{(-1L),0x25DC2011L,0xE2A9CB83L},{0xAE14745FL,0x372AFE40L,1L},{0L,(-1L),0xE2A9CB83L},{(-1L),4L,0x372AFE40L},{0x9061CB7DL,(-3L),0xF836C566L},{0x4D0B8819L,(-4L),(-5L)},{0xE2A9CB83L,0x491554C8L,0x25DC2011L},{0x037933B0L,0x47B5AD66L,0x47B5AD66L},{0L,0x9212306FL,0L}},{{0x47B5AD66L,0xB7D765DBL,(-6L)},{0x77C5C021L,0L,(-1L)},{0xD8F19699L,0x5D67C34CL,(-1L)},{0x6DD85EFFL,0L,(-1L)},{0L,0xB7D765DBL,7L},{0xF4EFC7B8L,0x9212306FL,0x8B57B8ACL},{1L,0x47B5AD66L,(-1L)},{0xE69D9BD5L,0x491554C8L,1L},{0xB7D765DBL,(-4L),0xB7D765DBL},{0x4D41954BL,(-3L),0xF4EFC7B8L}},{{9L,4L,(-1L)},{0xF836C566L,(-1L),0x77C5C021L},{1L,0x372AFE40L,(-1L)},{0xF836C566L,0x25DC2011L,4L},{9L,(-1L),0x5D67C34CL},{0x4D41954BL,0xF9D72DD1L,0x9061CB7DL},{0xB7D765DBL,(-1L),(-1L)},{0xE69D9BD5L,0x77C5C021L,0x491554C8L},{1L,1L,(-1L)},{0xF4EFC7B8L,0xE6988B52L,0xE69D9BD5L}},{{0L,(-1L),0x037933B0L},{0x6DD85EFFL,0xE69D9BD5L,0x9212306FL},{0xD8F19699L,0L,0x037933B0L},{0x77C5C021L,0L,0xE69D9BD5L},{0x47B5AD66L,6L,(-1L)},{0L,(-6L),0x491554C8L},{0x037933B0L,(-5L),(-1L)},{0xE2A9CB83L,0x9061CB7DL,0x9061CB7DL},{0x4D0B8819L,1L,0x5D67C34CL},{0x9061CB7DL,0x8B57B8ACL,4L}}};
static union U0 *g_1289[2][9] = {{&g_206[0][0],&g_1226,&g_206[0][0],&g_1226,&g_206[0][0],&g_1226,&g_206[0][0],&g_1226,&g_206[0][0]},{&g_1226,&g_1226,(void*)0,(void*)0,&g_1226,&g_1226,(void*)0,(void*)0,&g_1226}};
static union U0 ** volatile g_1288[8] = {&g_1289[0][2],&g_1289[0][2],&g_1289[0][8],&g_1289[0][2],&g_1289[0][2],&g_1289[0][8],&g_1289[0][2],&g_1289[0][2]};
static int64_t *g_1382 = &g_330;
static int64_t ** volatile g_1381 = &g_1382;/* VOLATILE GLOBAL g_1381 */
static int64_t ** volatile * volatile g_1383 = &g_1381;/* VOLATILE GLOBAL g_1383 */
static int64_t g_1399[9] = {0L,1L,0L,0L,1L,0L,0L,1L,0L};
static volatile uint32_t g_1420 = 18446744073709551614UL;/* VOLATILE GLOBAL g_1420 */
static int16_t *g_1459 = (void*)0;
static int16_t **g_1458 = &g_1459;
static int16_t *** volatile g_1457 = &g_1458;/* VOLATILE GLOBAL g_1457 */
static union U0 g_1472[4] = {{1L},{1L},{1L},{1L}};
static int32_t ** volatile g_1526 = &g_176;/* VOLATILE GLOBAL g_1526 */
static volatile uint64_t g_1696 = 1UL;/* VOLATILE GLOBAL g_1696 */
static uint16_t *g_1739 = &g_755[1];
static uint8_t * volatile ** volatile g_1758 = (void*)0;/* VOLATILE GLOBAL g_1758 */
static int32_t **g_1814 = &g_176;
static const int32_t *g_1845 = &g_1226.f0;
static const int32_t ** volatile g_1844 = &g_1845;/* VOLATILE GLOBAL g_1844 */
static volatile int64_t g_1855[10][7] = {{0x714EC7415B985CF5LL,0x714EC7415B985CF5LL,0xA0067690FB511520LL,0x3CD9261D7327D853LL,(-7L),0x3CD9261D7327D853LL,0xA0067690FB511520LL},{0xC444D6F39BB83CA4LL,2L,0xDE3A215EAEE3A106LL,(-1L),0L,(-1L),0xDE3A215EAEE3A106LL},{0x714EC7415B985CF5LL,0x714EC7415B985CF5LL,0xA0067690FB511520LL,0x3CD9261D7327D853LL,(-7L),0x3CD9261D7327D853LL,0xA0067690FB511520LL},{0L,1L,0xC444D6F39BB83CA4LL,8L,0x0B764E15D5E054DBLL,8L,0xC444D6F39BB83CA4LL},{(-7L),(-7L),0x714EC7415B985CF5LL,5L,1L,5L,0x714EC7415B985CF5LL},{0L,1L,0xC444D6F39BB83CA4LL,8L,0x0B764E15D5E054DBLL,8L,0xC444D6F39BB83CA4LL},{(-7L),(-7L),0x714EC7415B985CF5LL,5L,1L,5L,0x714EC7415B985CF5LL},{0L,1L,0xC444D6F39BB83CA4LL,8L,0x0B764E15D5E054DBLL,8L,0xC444D6F39BB83CA4LL},{(-7L),(-7L),0x714EC7415B985CF5LL,5L,1L,5L,0x714EC7415B985CF5LL},{0L,1L,0xC444D6F39BB83CA4LL,8L,0x0B764E15D5E054DBLL,8L,0xC444D6F39BB83CA4LL}};
static int32_t g_1897 = 0x1E2A0722L;
static float g_1984 = 0x0.AFFAE3p-52;
static uint32_t *g_2119[1][1] = {{&g_174}};
static volatile union U0 g_2124 = {8L};/* VOLATILE GLOBAL g_2124 */
static union U0 g_2132 = {1L};/* VOLATILE GLOBAL g_2132 */
static const volatile union U0 g_2133[5][10] = {{{8L},{-1L},{8L},{0x01585D8FL},{-1L},{1L},{1L},{-1L},{0x01585D8FL},{8L}},{{0xCDB6089FL},{0xCDB6089FL},{0L},{-1L},{-9L},{0L},{-9L},{-1L},{0L},{0xCDB6089FL}},{{-9L},{1L},{8L},{-9L},{0x01585D8FL},{0x01585D8FL},{-9L},{8L},{1L},{-9L}},{{8L},{0xCDB6089FL},{1L},{0x01585D8FL},{0xCDB6089FL},{0x01585D8FL},{1L},{0xCDB6089FL},{8L},{8L}},{{-9L},{-1L},{0L},{0xCDB6089FL},{0xCDB6089FL},{0L},{-1L},{-9L},{0L},{-9L}}};
static volatile union U0 g_2156[3] = {{0x75DC10C0L},{0x75DC10C0L},{0x75DC10C0L}};
static uint8_t **g_2175 = &g_639;
static uint8_t ***g_2174 = &g_2175;
static union U0 g_2226 = {1L};/* VOLATILE GLOBAL g_2226 */
static uint64_t g_2260[8] = {0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL,0x64C767A1E7967B2DLL};
static union U0 g_2271 = {0x4418BAE6L};/* VOLATILE GLOBAL g_2271 */
static uint32_t * const *** const *g_2287 = (void*)0;
static uint8_t * const *g_2311 = &g_639;
static uint8_t * const **g_2310 = &g_2311;
static volatile int16_t g_2313 = (-1L);/* VOLATILE GLOBAL g_2313 */
static const volatile union U0 g_2348 = {-9L};/* VOLATILE GLOBAL g_2348 */
static const int32_t ** volatile g_2439 = &g_1845;/* VOLATILE GLOBAL g_2439 */
static uint64_t **g_2459 = (void*)0;
static uint64_t *** volatile g_2458 = &g_2459;/* VOLATILE GLOBAL g_2458 */
static union U0 g_2494 = {0x3D710824L};/* VOLATILE GLOBAL g_2494 */
static const int64_t g_2519 = (-9L);
static const volatile union U0 g_2546 = {0xFE7E1470L};/* VOLATILE GLOBAL g_2546 */
static const int32_t **g_2588 = &g_1845;
static const int32_t ***g_2587 = &g_2588;
static const int32_t ****g_2586 = &g_2587;
static const int32_t ***** volatile g_2585 = &g_2586;/* VOLATILE GLOBAL g_2585 */
static volatile union U0 g_2589 = {2L};/* VOLATILE GLOBAL g_2589 */
static int8_t *g_2604 = &g_26[2][6][2];
static int8_t * volatile * const g_2603 = &g_2604;
static int8_t * volatile * const  volatile *g_2602 = &g_2603;
static uint8_t g_2644[5] = {1UL,1UL,1UL,1UL,1UL};
static volatile union U0 g_2651 = {1L};/* VOLATILE GLOBAL g_2651 */
static volatile union U0 g_2652 = {4L};/* VOLATILE GLOBAL g_2652 */
static union U0 g_2685 = {3L};/* VOLATILE GLOBAL g_2685 */
static float * volatile g_2686 = &g_530;/* VOLATILE GLOBAL g_2686 */
static const union U0 g_2719 = {0x1EEF9518L};/* VOLATILE GLOBAL g_2719 */
static float * volatile g_2789 = (void*)0;/* VOLATILE GLOBAL g_2789 */
static volatile union U0 g_2794 = {0x83CCD3D7L};/* VOLATILE GLOBAL g_2794 */
static uint64_t g_2801[9][8][3] = {{{0x0C36443D8F797876LL,1UL,1UL},{0xE6FEF77287CA7439LL,0x10F8B717B7E7231ALL,0x9DE779A6DBF73DF8LL},{2UL,18446744073709551615UL,4UL},{0xA0EA3BD537FA9469LL,0x9D0F0BFD801FAE97LL,0x8FFBE02146CDEF67LL},{0x11764BBBBA5DEF67LL,0UL,0UL},{18446744073709551615UL,0x9D0F0BFD801FAE97LL,0x30AEC916919EB15CLL},{0xE87252CCE60E7CBCLL,18446744073709551615UL,0x2D1A0BFCB2A0C56ELL},{0xFEC6C0E0DDF580AALL,0x10F8B717B7E7231ALL,1UL}},{{18446744073709551611UL,1UL,18446744073709551612UL},{9UL,0UL,0UL},{0x05BA0C593697F98ELL,0xC3F12FEFC6529018LL,0x8837E4CE3C55C0D6LL},{0x30AEC916919EB15CLL,1UL,0x5D4D2EB79085DD59LL},{0xFED8E2B71FAEAF98LL,18446744073709551615UL,0x01706368EF4A9197LL},{0UL,18446744073709551615UL,0x84F4C266ADE1D2B0LL},{0UL,0xF00AA8067FFC54B9LL,18446744073709551615UL},{18446744073709551615UL,0xC3F12FEFC6529018LL,0x9168EB2C80FEA0A1LL}},{{0x9BF56DDC655EDA16LL,1UL,5UL},{0x0C36443D8F797876LL,0x7633A62DEC051ABBLL,18446744073709551615UL},{6UL,0xDD748EAE79B95C6FLL,0x9DE779A6DBF73DF8LL},{6UL,0x16E2BA2A765E246DLL,0x9BF56DDC655EDA16LL},{0x0C36443D8F797876LL,0x9D0F0BFD801FAE97LL,0x8837E4CE3C55C0D6LL},{0x9BF56DDC655EDA16LL,0x8DA23C5F631C381BLL,0x1B2813B619126B17LL},{18446744073709551615UL,8UL,1UL},{0UL,18446744073709551615UL,0x2D1A0BFCB2A0C56ELL}},{{0UL,0xE8ABD83A481962ADLL,0xE87252CCE60E7CBCLL},{0x5D4D2EB79085DD59LL,8UL,1UL},{1UL,18446744073709551613UL,0x206901091CB97A2ELL},{0x85B6E3927A4CADA7LL,0UL,0xEEEEBCEF79F7ABC6LL},{0xE87252CCE60E7CBCLL,0xFED8E2B71FAEAF98LL,0x10F8B717B7E7231ALL},{0x9BF56DDC655EDA16LL,0x9BF56DDC655EDA16LL,8UL},{0x84F4C266ADE1D2B0LL,4UL,8UL},{0x9470F15384712F4FLL,18446744073709551611UL,18446744073709551615UL}},{{0x8C8A24BF8560F551LL,0UL,18446744073709551612UL},{0xDD748EAE79B95C6FLL,0x9470F15384712F4FLL,18446744073709551615UL},{0xE6FEF77287CA7439LL,0UL,8UL},{0x8FFBE02146CDEF67LL,0x0C36443D8F797876LL,8UL},{18446744073709551615UL,18446744073709551614UL,0x10F8B717B7E7231ALL},{0x04AC55E533BB5D43LL,18446744073709551607UL,0xEEEEBCEF79F7ABC6LL},{0xDD748EAE79B95C6FLL,1UL,0x206901091CB97A2ELL},{0x1EBFCBA550DBCD00LL,1UL,1UL}},{{0xA88442E2430590EALL,8UL,0x9470F15384712F4FLL},{18446744073709551615UL,0xA0EA3BD537FA9469LL,0x66ECBCFD18466C6ALL},{0x5D4D2EB79085DD59LL,1UL,0x2D1A0BFCB2A0C56ELL},{0xE87252CCE60E7CBCLL,0x9470F15384712F4FLL,0x206901091CB97A2ELL},{0UL,0xC5149A9C2E6BA83ELL,0xFEC6C0E0DDF580AALL},{0x2D1A0BFCB2A0C56ELL,0xFED8E2B71FAEAF98LL,0x05BA0C593697F98ELL},{4UL,0x11764BBBBA5DEF67LL,0x9B48DECC0FF6B5A0LL},{0x84F4C266ADE1D2B0LL,0x11764BBBBA5DEF67LL,0x1B2813B619126B17LL}},{{0x5C14CEE90828A4E0LL,0xFED8E2B71FAEAF98LL,18446744073709551615UL},{0x1EBFCBA550DBCD00LL,0xC5149A9C2E6BA83ELL,1UL},{0UL,0x9470F15384712F4FLL,4UL},{2UL,1UL,0UL},{0x8FFBE02146CDEF67LL,0xA0EA3BD537FA9469LL,0xECDB52DE6F556366LL},{0x8837E4CE3C55C0D6LL,8UL,0x10F8B717B7E7231ALL},{0xE6FEF77287CA7439LL,1UL,0xFEC6C0E0DDF580AALL},{0UL,1UL,0xAC132D8C81D6EA14LL}},{{1UL,18446744073709551607UL,1UL},{0xA88442E2430590EALL,18446744073709551614UL,0xA88442E2430590EALL},{1UL,0x0C36443D8F797876LL,0x66ECBCFD18466C6ALL},{0x9BF56DDC655EDA16LL,0UL,1UL},{0x2D1A0BFCB2A0C56ELL,0x9470F15384712F4FLL,0xAC132D8C81D6EA14LL},{0x0C36443D8F797876LL,0UL,8UL},{0x2D1A0BFCB2A0C56ELL,18446744073709551611UL,0UL},{0x9BF56DDC655EDA16LL,4UL,0x9B48DECC0FF6B5A0LL}},{{1UL,0x9BF56DDC655EDA16LL,8UL},{0xA88442E2430590EALL,0xFED8E2B71FAEAF98LL,4UL},{1UL,0UL,1UL},{0UL,18446744073709551613UL,0x5D4D2EB79085DD59LL},{0xE6FEF77287CA7439LL,8UL,0UL},{0x8837E4CE3C55C0D6LL,0UL,8UL},{0x8FFBE02146CDEF67LL,8UL,0x05BA0C593697F98ELL},{2UL,18446744073709551607UL,8UL}}};
static float * const  volatile g_2821 = &g_443[3];/* VOLATILE GLOBAL g_2821 */
static volatile int8_t g_2882[5] = {0xA2L,0xA2L,0xA2L,0xA2L,0xA2L};
static float * volatile g_2916 = &g_443[3];/* VOLATILE GLOBAL g_2916 */
static uint32_t ***g_2950 = (void*)0;
static uint32_t ****g_2949 = &g_2950;
static float * volatile g_2953 = &g_1984;/* VOLATILE GLOBAL g_2953 */
static volatile union U0 g_3072 = {0x7A838375L};/* VOLATILE GLOBAL g_3072 */
static uint32_t g_3096 = 0xC1DE62E9L;
static float * volatile g_3173 = (void*)0;/* VOLATILE GLOBAL g_3173 */


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t * func_2(int32_t  p_3, int8_t  p_4, int32_t * p_5);
static int32_t * func_10(int32_t * p_11, int8_t  p_12);
static int32_t * func_13(uint16_t  p_14, const int16_t  p_15, int32_t * p_16);
static int64_t  func_18(const int32_t * p_19, int8_t  p_20, int8_t  p_21);
static const int32_t * func_22(int32_t  p_23, int32_t * p_24);
static int32_t  func_27(uint32_t  p_28, int32_t  p_29, const int32_t * p_30);
static const int32_t * func_35(int64_t  p_36, int32_t  p_37, int8_t  p_38, uint16_t  p_39);
static float  func_41(int64_t  p_42, uint32_t * p_43, int16_t  p_44, int16_t  p_45);
static int64_t  func_46(int32_t * p_47, uint32_t * p_48);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_8 g_26 g_32 g_50 g_64 g_58 g_95 g_150 g_159 g_150.f0 g_150.f2 g_63 g_92 g_206.f0 g_174 g_59 g_62 g_219 g_229 g_205 g_206 g_209 g_210 g_373 g_160 g_406 g_344 g_442 g_206.f2 g_407 g_379 g_330 g_511 g_343 g_563 g_498 g_588 g_678 g_443 g_679 g_530 g_680 g_375 g_639 g_749 g_376 g_755 g_766 g_714 g_1052 g_1055 g_751 g_905 g_1086 g_740 g_1164 g_1168 g_1181 g_1182 g_1082 g_1226 g_588.f0 g_1266 g_1381 g_1383 g_1399 g_1472.f0 g_1526 g_176 g_1382 g_939 g_1696 g_1226.f0 g_2348 g_1814 g_2175 g_2439 g_1472.f2 g_1739 g_2119 g_2458 g_2459 g_2174 g_2604 g_1845 g_2719.f0 g_2587 g_2588 g_2310 g_2311 g_2602 g_2603 g_2686 g_2586
 * writes: g_26 g_32 g_50 g_59 g_64 g_92 g_95 g_174 g_176 g_219 g_205 g_330 g_343 g_344 g_374 g_379 g_206.f0 g_150.f0 g_407 g_443 g_63 g_498 g_210 g_511 g_530 g_639 g_679 g_714 g_741 g_749 g_751 g_755 g_756 g_1052 g_1055 g_1082 g_1164 g_1381 g_8 g_376 g_1226.f0 g_1845 g_2271.f0
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_6 = 18446744073709551611UL;
    int32_t *l_9 = (void*)0;
    const uint16_t l_17 = 65535UL;
    uint16_t l_25 = 0xD72AL;
    int32_t *l_1731 = &g_150.f0;
    (***g_2586) = func_2(l_6, (g_7 != l_9), ((*g_1814) = func_10((l_1731 = func_13(g_8[1], l_17, ((0xC1L <= ((func_18(func_22((g_26[1][3][0] |= (l_25 & (l_9 != (void*)0))), &g_8[1]), g_740[1][4], g_740[6][6]) & g_740[4][3]) & l_17)) , (*g_1526)))), g_740[0][6])));
    return l_25;
}


/* ------------------------------------------ */
/* 
 * reads : g_1164 g_2311 g_639 g_92 g_1381 g_1382 g_330 g_2602 g_2603 g_2604 g_7 g_2587 g_2588 g_1845 g_1472.f0 g_206.f0 g_1226.f0 g_680 g_530 g_8 g_1526 g_176
 * writes: g_1164 g_92 g_330 g_26 g_8 g_443
 */
static int32_t * func_2(int32_t  p_3, int8_t  p_4, int32_t * p_5)
{ /* block id: 1410 */
    uint64_t l_3164[4] = {0x0349F8B403D7FE89LL,0x0349F8B403D7FE89LL,0x0349F8B403D7FE89LL,0x0349F8B403D7FE89LL};
    int8_t l_3167[6][5][8] = {{{0x4EL,(-6L),0x3EL,0x81L,1L,0x2DL,0xBCL,0x6DL},{(-7L),0x81L,0x2DL,5L,5L,0x2DL,0x81L,(-7L)},{5L,0x2DL,0x81L,(-7L),0x8CL,0x14L,0x4EL,0xDEL},{0x81L,0x3EL,(-6L),0x4EL,(-7L),0x14L,0L,0x2AL},{0x14L,0x2DL,7L,1L,7L,0x2DL,0x14L,(-6L)}},{{0x8CL,5L,(-6L),(-7L),1L,0x6DL,7L,(-6L)},{0xBCL,7L,(-7L),0L,1L,(-1L),0x2DL,0x2DL},{0x8CL,0x3EL,(-6L),(-6L),0x3EL,0x8CL,0x2AL,0xBCL},{(-2L),(-6L),0xBCL,0x4EL,0x81L,0L,0x6DL,0x2DL},{5L,(-7L),0x2AL,0x4EL,(-6L),0xBCL,0xDEL,0xBCL}},{{(-1L),(-6L),6L,(-6L),(-1L),7L,0x81L,0x2DL},{0x81L,0xBCL,0L,0L,0x2DL,(-6L),0x8CL,(-6L)},{6L,0x2AL,0L,(-7L),6L,0x81L,0x81L,6L},{0x2DL,6L,6L,0x2DL,0x4EL,0x2DL,0xDEL,0x8CL},{(-7L),0L,0x2AL,6L,0x2DL,0x3EL,0x6DL,0L}},{{0L,0L,0xBCL,0x81L,0x2AL,0x2DL,0x2AL,0x81L},{(-6L),6L,(-6L),(-1L),7L,0x81L,0x2DL,0x2AL},{0x4EL,0x2AL,(-7L),5L,6L,(-6L),7L,0L},{0x4EL,0xBCL,(-6L),(-2L),7L,7L,(-2L),(-6L)},{(-6L),(-6L),0x3EL,0x8CL,0x2AL,0xBCL,1L,0xDEL}},{{0L,(-7L),7L,0xBCL,0x2DL,0L,6L,0xDEL},{(-7L),(-6L),5L,0x8CL,0x4EL,0x8CL,5L,(-6L)},{0x2DL,0x3EL,0x4EL,(-2L),6L,(-1L),(-6L),0L},{6L,7L,0xDEL,5L,0x2DL,0x6DL,(-6L),0x2AL},{0x81L,5L,0x4EL,(-1L),(-1L),0x4EL,5L,0x81L}},{{(-1L),0x4EL,5L,0x81L,(-6L),(-2L),6L,0L},{5L,0xDEL,7L,6L,0x81L,(-2L),1L,0x8CL},{(-2L),0x4EL,0x3EL,0x2DL,0x3EL,0x4EL,(-2L),6L},{0x8CL,5L,(-6L),(-7L),1L,0x6DL,7L,6L},{(-6L),0x3EL,0x81L,1L,0x2DL,0xBCL,0x6DL,0x6DL}}};
    int32_t l_3168[3];
    int32_t l_3169[3];
    int32_t l_3171[7][8] = {{0xEB3EC7C4L,0xD947DDBBL,0xEB3EC7C4L,0x3549E379L,1L,0xF5E503E7L,0xA3DCE85FL,0xA3DCE85FL},{0xA3DCE85FL,0xA6B54CDBL,(-9L),(-9L),0xA6B54CDBL,0xA3DCE85FL,1L,0xEB8AF7ABL},{0xA3DCE85FL,(-1L),0L,0xA6B54CDBL,1L,0xA6B54CDBL,0L,(-1L)},{0xEB3EC7C4L,0L,0xF5E503E7L,0xA6B54CDBL,0xEB8AF7ABL,0x3549E379L,0x3549E379L,0xEB8AF7ABL},{(-9L),0xEB8AF7ABL,0xEB8AF7ABL,(-9L),0xEB3EC7C4L,(-1L),0x3549E379L,0xA3DCE85FL},{0L,(-9L),0xF5E503E7L,0x3549E379L,0xF5E503E7L,(-9L),0L,1L},{0xF5E503E7L,(-9L),0L,1L,(-1L),(-1L),1L,0L}};
    int16_t * const *l_3172[1];
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_3168[i] = 1L;
    for (i = 0; i < 3; i++)
        l_3169[i] = 0L;
    for (i = 0; i < 1; i++)
        l_3172[i] = &g_1459;
    for (g_1164 = 0; (g_1164 != 15); g_1164 = safe_add_func_int8_t_s_s(g_1164, 5))
    { /* block id: 1413 */
        float l_3155 = 0x6.Dp-1;
        int32_t l_3166 = 1L;
        uint32_t *l_3170[3][9][1] = {{{&g_174},{&g_174},{&g_174},{&g_174},{&g_174},{(void*)0},{&g_174},{&g_174},{(void*)0}},{{&g_174},{&g_174},{&g_174},{&g_174},{&g_174},{&g_174},{&g_174},{&g_174},{&g_174}},{{(void*)0},{&g_174},{&g_174},{(void*)0},{&g_174},{&g_174},{&g_174},{&g_174},{&g_174}}};
        float *l_3174 = &g_443[7];
        int i, j, k;
        (*l_3174) = (safe_mul_func_float_f_f((((safe_add_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((((l_3171[3][3] |= ((((((p_3 <= p_3) || (!1L)) | (l_3168[0] ^= (safe_rshift_func_uint16_t_u_u((((((safe_add_func_int16_t_s_s(((safe_mod_func_uint8_t_u_u(((**g_2311) &= (~l_3164[2])), p_4)) & ((l_3166 = ((*g_7) = (+((((((***g_2602) = (p_4 || (l_3166 ^ (((**g_1381) &= l_3164[0]) <= (0x7E13B4EB0583D635LL != l_3166))))) >= 0x49L) < l_3167[1][1][4]) <= p_4) & l_3166)))) && p_4)), p_4)) > l_3167[3][4][5]) > p_3) != p_4) == 0xCBD6L), l_3167[1][1][4])))) > 0xCD96B470L) == l_3169[0]) | (-1L))) != (***g_2587)) != p_4) >= 4UL), l_3167[1][1][4])), p_3)) , (void*)0) != l_3172[0]), (*g_680)));
    }
    (*g_7) &= (&l_3164[2] == (void*)0);
    return (*g_1526);
}


/* ------------------------------------------ */
/* 
 * reads : g_1226.f0 g_2348 g_8 g_1182 g_344 g_1814 g_406 g_407 g_63 g_2175 g_639 g_92 g_1399 g_755 g_7 g_32 g_26 g_50 g_64 g_58 g_95 g_150 g_159 g_150.f0 g_150.f2 g_206.f0 g_174 g_59 g_62 g_219 g_229 g_205 g_206 g_209 g_210 g_373 g_160 g_442 g_206.f2 g_379 g_330 g_511 g_343 g_563 g_498 g_588 g_678 g_443 g_679 g_530 g_680 g_375 g_749 g_376 g_766 g_714 g_1052 g_1055 g_751 g_905 g_1086 g_740 g_1164 g_1472.f0 g_1168 g_1181 g_1226 g_588.f0 g_1266 g_2439 g_1472.f2 g_1739 g_1382 g_2119 g_2458 g_2459 g_2174 g_2604 g_1845 g_2719.f0 g_2587 g_2588 g_2310 g_2311 g_2602 g_2603 g_2686
 * writes: g_1226.f0 g_344 g_176 g_8 g_32 g_50 g_59 g_64 g_92 g_95 g_174 g_26 g_219 g_205 g_330 g_343 g_374 g_379 g_206.f0 g_150.f0 g_407 g_443 g_63 g_498 g_210 g_511 g_530 g_639 g_679 g_714 g_741 g_749 g_751 g_755 g_756 g_1052 g_1055 g_1082 g_1164 g_1845 g_2271.f0
 */
static int32_t * func_10(int32_t * p_11, int8_t  p_12)
{ /* block id: 762 */
    uint8_t l_1748[7] = {0xD7L,0xD7L,0xD7L,0xD7L,0xD7L,0xD7L,0xD7L};
    int32_t * const ****l_1750 = (void*)0;
    uint32_t l_1755 = 1UL;
    int32_t l_1774 = 0xAC6637FDL;
    int32_t l_1786 = (-6L);
    int32_t **l_1817[4][9][5] = {{{&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,(void*)0,&g_7,&g_7,(void*)0},{(void*)0,&g_7,&g_7,&g_7,&g_7},{(void*)0,(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7},{&g_176,&g_176,&g_7,&g_7,&g_176},{&g_7,&g_176,&g_7,(void*)0,&g_176},{&g_176,&g_7,&g_7,(void*)0,&g_176},{&g_7,(void*)0,&g_176,&g_7,&g_7}},{{&g_176,&g_7,&g_7,&g_176,&g_7},{&g_7,(void*)0,(void*)0,&g_7,&g_7},{&g_176,&g_7,&g_7,&g_176,&g_7},{&g_7,&g_176,(void*)0,&g_7,&g_176},{(void*)0,&g_7,&g_7,&g_176,&g_176},{(void*)0,&g_7,&g_176,&g_7,&g_176},{&g_7,&g_7,&g_176,&g_176,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,(void*)0,(void*)0,&g_7}},{{&g_7,&g_7,(void*)0,&g_7,&g_176},{&g_7,&g_176,&g_7,&g_176,&g_7},{&g_7,(void*)0,&g_7,&g_7,&g_176},{(void*)0,&g_176,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_176,(void*)0,&g_176},{&g_7,&g_7,(void*)0,&g_7,&g_7},{&g_176,&g_7,&g_176,&g_7,&g_176},{&g_7,(void*)0,&g_7,(void*)0,&g_7},{&g_7,&g_176,&g_7,&g_7,(void*)0}},{{&g_7,&g_176,&g_7,&g_7,&g_176},{&g_7,&g_7,&g_176,&g_176,&g_7},{(void*)0,&g_7,(void*)0,&g_7,&g_176},{&g_7,&g_176,&g_176,&g_7,&g_7},{&g_7,&g_176,&g_7,&g_7,&g_7},{&g_7,(void*)0,&g_7,&g_7,(void*)0},{(void*)0,&g_7,&g_7,(void*)0,(void*)0},{&g_7,&g_7,(void*)0,&g_7,&g_7},{&g_7,&g_7,&g_176,&g_7,&g_7}}};
    uint32_t * const *l_1836 = &g_160;
    uint32_t * const **l_1835 = &l_1836;
    uint32_t * const ***l_1834 = &l_1835;
    uint64_t l_1837[6] = {0UL,0UL,0UL,0UL,0UL,0UL};
    int8_t l_1923 = (-1L);
    uint32_t ****l_1975 = &g_210;
    uint32_t l_2020 = 0x85A90F39L;
    uint64_t l_2090[2];
    float l_2135 = 0x1.Cp+1;
    uint8_t l_2181 = 0x9BL;
    uint64_t l_2220 = 0x0783DB47DC46221ELL;
    union U0 ** const l_2261 = &g_1289[1][5];
    union U0 **l_2269 = &g_1289[0][2];
    int16_t l_2327[9][7] = {{1L,0xBF01L,0x3D7EL,0x3D7EL,0xBF01L,1L,(-10L)},{0xAA74L,0xBF01L,0xC0EFL,0x8A90L,0xBF01L,0x76D8L,0xDF7CL},{0xAA74L,0L,0x3D7EL,0x8A90L,0L,1L,0xDF7CL},{1L,0xBF01L,0x3D7EL,0x3D7EL,0xBF01L,1L,(-10L)},{0xAA74L,0xBF01L,0xC0EFL,0x8A90L,0xBF01L,0x76D8L,0xDF7CL},{0xAA74L,0L,0x3D7EL,0x8A90L,0L,1L,0xDF7CL},{1L,0xBF01L,0x3D7EL,0x3D7EL,0xBF01L,1L,(-10L)},{0xAA74L,0xBF01L,0xC0EFL,0x8A90L,0xBF01L,0x76D8L,0xDF7CL},{0xAA74L,0L,0x3D7EL,0x8A90L,0L,1L,0xDF7CL}};
    uint16_t l_2341 = 0x2089L;
    uint64_t l_2374[3];
    float l_2393 = 0x6.4F156Dp-81;
    uint32_t **l_2414 = &g_2119[0][0];
    uint32_t ***l_2413 = &l_2414;
    int8_t l_2527 = 0x88L;
    uint32_t l_2559 = 0xE887E1C5L;
    const int64_t *l_2653 = (void*)0;
    uint32_t l_2693 = 0x46FDF361L;
    int32_t l_2709 = (-1L);
    uint16_t l_2712 = 8UL;
    float *l_2748 = (void*)0;
    uint8_t **l_2770 = &g_639;
    int64_t **l_2888 = (void*)0;
    int64_t ***l_2887 = &l_2888;
    int8_t ****l_2965 = (void*)0;
    float l_2987 = 0x2.C278DEp+24;
    uint32_t *****l_2996 = &g_2949;
    int32_t *l_3055[5][5][5] = {{{&g_150.f0,&g_59,&g_8[1],&g_1472[1].f0,&g_59},{&g_150.f0,(void*)0,(void*)0,&g_379,&l_1786},{&g_379,(void*)0,&g_59,(void*)0,&g_379},{&g_2494.f0,&g_379,(void*)0,&l_1786,&g_1164},{&g_2685.f0,&g_8[5],(void*)0,&g_2685.f0,&g_8[1]}},{{&g_379,&g_1164,&g_2226.f0,&g_379,&g_1164},{&g_150.f0,&g_2685.f0,&l_1786,&g_2132.f0,&g_1164},{(void*)0,&g_2132.f0,&l_1786,&g_2494.f0,&l_1786},{&g_150.f0,&g_150.f0,(void*)0,&g_8[5],&g_8[1]},{&g_206[0][0].f0,&l_1786,&l_1774,&l_1786,&l_1786}},{{&g_1226.f0,&g_59,(void*)0,&g_1472[1].f0,&g_1164},{(void*)0,&l_1786,&g_379,&g_2132.f0,&g_2226.f0},{&g_8[1],&g_150.f0,&g_64,&g_1164,&l_1786},{(void*)0,&g_2132.f0,&g_2132.f0,(void*)0,&g_64},{&g_8[1],(void*)0,&g_206[0][0].f0,&g_150.f0,&g_150.f0}},{{&g_206[0][0].f0,&g_2226.f0,(void*)0,&g_64,(void*)0},{&g_150.f0,&g_64,&g_8[1],&g_150.f0,&g_1164},{&g_2494.f0,(void*)0,(void*)0,(void*)0,&g_2271.f0},{&g_64,&g_1226.f0,&g_1164,&g_1164,&g_1226.f0},{&l_1786,&g_2226.f0,&l_1786,&g_2132.f0,&l_1786}},{{&g_8[5],&g_8[1],&g_206[0][0].f0,&g_1472[1].f0,(void*)0},{&l_1786,&g_2494.f0,&g_2226.f0,&l_1786,(void*)0},{&g_8[5],&g_64,(void*)0,&g_8[5],&g_2132.f0},{&l_1786,(void*)0,&g_379,&g_2494.f0,&g_2494.f0},{&g_64,&g_1164,&g_64,&g_2132.f0,&g_8[5]}}};
    float l_3059 = 0x8.50549Ap+66;
    uint64_t l_3073 = 0x819B46D2304D8B5DLL;
    uint32_t l_3129 = 18446744073709551614UL;
    uint32_t l_3140 = 0x39C7C496L;
    int32_t *l_3146 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2090[i] = 0x08C7F0823F65FAC8LL;
    for (i = 0; i < 3; i++)
        l_2374[i] = 0x0675B281ED6BCF50LL;
    if (p_12)
    { /* block id: 763 */
        uint16_t *l_1740[9][8] = {{(void*)0,&g_751[0][0][8],(void*)0,(void*)0,&g_755[3],&g_751[0][1][2],&g_751[0][1][2],&g_755[3]},{&g_751[0][0][8],&g_755[3],&g_755[3],&g_751[0][0][8],(void*)0,(void*)0,&g_751[0][1][2],&g_755[4]},{&g_751[0][0][8],&g_751[0][0][8],(void*)0,&g_751[0][1][2],(void*)0,&g_751[0][0][8],&g_751[0][0][8],&g_755[3]},{(void*)0,&g_751[0][0][8],&g_751[0][0][8],&g_755[3],(void*)0,(void*)0,&g_755[3],&g_751[0][0][8]},{&g_755[3],&g_755[3],&g_751[0][0][8],(void*)0,(void*)0,&g_751[0][1][2],&g_755[4],&g_751[0][1][2]},{(void*)0,&g_751[0][0][8],(void*)0,&g_751[0][0][8],(void*)0,(void*)0,&g_755[3],&g_751[0][1][2]},{&g_751[0][0][8],(void*)0,&g_755[4],(void*)0,(void*)0,&g_755[4],(void*)0,&g_751[0][0][8]},{&g_751[0][0][8],(void*)0,&g_755[4],&g_755[3],&g_755[3],(void*)0,&g_755[3],&g_755[3]},{(void*)0,&g_755[0],(void*)0,&g_751[0][1][2],&g_755[3],(void*)0,&g_755[4],&g_755[4]}};
        uint32_t *l_1745 = &g_174;
        uint32_t **l_1744[5];
        int32_t l_1749 = 0xD90784C2L;
        int32_t ****l_1752 = &g_939;
        int32_t *****l_1751 = &l_1752;
        int32_t *l_1753 = (void*)0;
        int32_t *l_1754 = (void*)0;
        int32_t l_1789 = 0x58FA5B43L;
        float **l_1811 = (void*)0;
        float l_1854 = (-0x4.3p+1);
        uint32_t l_1873 = 6UL;
        uint32_t ***l_1881 = &g_211;
        int32_t l_1901 = 0x969D9EFDL;
        int64_t ** const l_1929[7] = {&g_1382,&g_1382,&g_1382,&g_1382,&g_1382,&g_1382,&g_1382};
        int64_t ** const * const l_1928 = &l_1929[6];
        int32_t l_1999 = 1L;
        int32_t l_2019[4][1][6] = {{{(-1L),0xFA08765EL,(-1L),0xF6DD4DFEL,9L,0xF6DD4DFEL}},{{(-1L),0xFA08765EL,(-1L),0xF6DD4DFEL,9L,0xF6DD4DFEL}},{{(-1L),0xFA08765EL,(-1L),0xF6DD4DFEL,9L,0xF6DD4DFEL}},{{(-1L),0xFA08765EL,(-7L),0x60576B74L,(-1L),0x60576B74L}}};
        int32_t l_2038 = 0x7BFDEF14L;
        int32_t l_2039[7] = {1L,0x6CD460CFL,1L,1L,0x6CD460CFL,1L,1L};
        uint16_t l_2061 = 0x2FC5L;
        int64_t l_2099 = (-2L);
        uint32_t l_2102 = 0xA020DB79L;
        const int16_t * const l_2139 = &g_343;
        int8_t l_2154[5] = {0x09L,0x09L,0x09L,0x09L,0x09L};
        uint8_t ***l_2177 = (void*)0;
        int32_t l_2219[1][8] = {{(-1L),0x268D9A65L,(-1L),0x268D9A65L,(-1L),0x268D9A65L,(-1L),0x268D9A65L}};
        int16_t ***l_2274[9][3][9] = {{{&g_1458,(void*)0,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0}},{{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458},{(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458},{&g_1458,(void*)0,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458}},{{(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0},{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0},{&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458}},{{&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458},{(void*)0,&g_1458,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458}},{{&g_1458,&g_1458,(void*)0,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0},{&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0}},{{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458},{(void*)0,(void*)0,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,(void*)0,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,(void*)0,&g_1458}},{{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0},{(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,(void*)0,(void*)0,(void*)0,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458}},{{&g_1458,(void*)0,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458},{(void*)0,&g_1458,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,(void*)0},{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458}},{{&g_1458,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458,&g_1458},{&g_1458,&g_1458,(void*)0,&g_1458,(void*)0,&g_1458,&g_1458,&g_1458,&g_1458}}};
        int64_t *l_2285 = &g_1399[3];
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_1744[i] = &l_1745;
    }
    else
    { /* block id: 1084 */
        const uint8_t l_2362 = 8UL;
        const uint32_t *l_2368[1];
        const uint32_t **l_2367 = &l_2368[0];
        const uint32_t ***l_2366 = &l_2367;
        const uint32_t ****l_2365[5] = {&l_2366,&l_2366,&l_2366,&l_2366,&l_2366};
        int32_t l_2373 = (-8L);
        int32_t l_2382 = 0xF9C3A904L;
        int32_t l_2383 = 0L;
        int32_t l_2384 = 1L;
        int32_t l_2386 = 0x912DD5A0L;
        int32_t l_2389 = 0L;
        int32_t l_2392 = 0x1B9957DAL;
        int32_t l_2394 = (-2L);
        int32_t l_2395[3];
        uint32_t **l_2454[7] = {&g_2119[0][0],&g_2119[0][0],&g_2119[0][0],&g_2119[0][0],&g_2119[0][0],&g_2119[0][0],&g_2119[0][0]};
        int32_t l_2507[7][5];
        const int64_t *l_2516 = &g_330;
        const int64_t **l_2517 = &l_2516;
        const int64_t *l_2518[7] = {&g_2519,&g_2519,&g_2519,&g_2519,&g_2519,&g_2519,&g_2519};
        int32_t ***l_2526 = (void*)0;
        uint64_t *l_2675 = (void*)0;
        float l_2687 = 0xC.2F53C5p-93;
        uint16_t l_2730[7][9][4] = {{{0xB237L,0x7612L,65534UL,0x1049L},{65534UL,65534UL,1UL,0x1D32L},{0x7612L,0x1049L,0x61F1L,0x1D32L},{65534UL,65534UL,0x9256L,0x1049L},{0xC158L,0x7612L,65535UL,0x7612L},{65534UL,0xB237L,0x61F1L,0x923BL},{0xD069L,65534UL,0xB237L,0xD069L},{65534UL,0x7612L,0x16BAL,0xC158L},{65534UL,65534UL,0xB237L,0x1D32L}},{{0xD069L,0xC158L,0x61F1L,0x61F1L},{65534UL,65534UL,65535UL,0xC158L},{0xC158L,0xD069L,0x9256L,0x7612L},{65534UL,65534UL,0x61F1L,0x9256L},{0x7612L,65534UL,1UL,0x7612L},{65534UL,0xD069L,65534UL,0xC158L},{0xB237L,65534UL,0xB237L,0x61F1L},{0x7612L,0xC158L,0x2651L,0x1D32L},{65534UL,65534UL,0x9256L,0xC158L}},{{0x1049L,0x7612L,0x9256L,0xD069L},{65534UL,65534UL,0x2651L,0x923BL},{0x7612L,0xB237L,0xB237L,0x7612L},{0xB237L,0x7612L,65534UL,0x1049L},{65534UL,65534UL,1UL,0x1D32L},{0x7612L,0x1049L,0x61F1L,0x1D32L},{0x4302L,1UL,0x16BAL,0x2651L},{0x61F1L,0xB237L,65534UL,0xB237L},{1UL,0UL,65535UL,65534UL}},{{1UL,0x1049L,0UL,1UL},{0x1049L,0xB237L,0x5C28L,0x61F1L},{0x1049L,0x4302L,0UL,0x9256L},{1UL,0x61F1L,65535UL,65535UL},{1UL,1UL,65534UL,0x61F1L},{0x61F1L,1UL,0x16BAL,0xB237L},{0x4302L,0x1049L,65535UL,0x16BAL},{0xB237L,0x1049L,0xC158L,0xB237L},{0x1049L,1UL,0x4302L,0x61F1L}},{{0UL,1UL,0UL,65535UL},{0xB237L,0x61F1L,0x923BL,0x9256L},{1UL,0x4302L,0x16BAL,0x61F1L},{0x2651L,0xB237L,0x16BAL,1UL},{1UL,0x1049L,0x923BL,65534UL},{0xB237L,0UL,0UL,0xB237L},{0UL,0xB237L,0x4302L,0x2651L},{0x1049L,1UL,0xC158L,0x9256L},{0xB237L,0x2651L,65535UL,0x9256L}},{{0x4302L,1UL,0x16BAL,0x2651L},{0x61F1L,0xB237L,65534UL,0xB237L},{1UL,0UL,65535UL,65534UL},{1UL,0x1049L,0UL,1UL},{0x1049L,0xB237L,0x5C28L,0x61F1L},{0x1049L,0x4302L,0UL,0x9256L},{1UL,0x61F1L,65535UL,65535UL},{1UL,1UL,65534UL,0x61F1L},{0x61F1L,1UL,0x16BAL,0xB237L}},{{0x4302L,0x1049L,65535UL,0x16BAL},{0xB237L,0x1049L,0xC158L,0xB237L},{0x1049L,1UL,0x4302L,0x61F1L},{0UL,1UL,0UL,65535UL},{0xB237L,0x61F1L,0x923BL,0x9256L},{1UL,0x4302L,0x16BAL,0x61F1L},{0x2651L,0xB237L,0x16BAL,1UL},{1UL,0x1049L,0x923BL,65534UL},{0xB237L,0UL,0UL,0xB237L}}};
        const float *l_2747[1];
        uint32_t l_2755 = 0x978E1599L;
        int32_t l_2822 = 8L;
        uint8_t l_2836 = 255UL;
        int8_t l_2837 = 0xE8L;
        uint32_t l_2858 = 0x3ADF0835L;
        float **l_2867 = &l_2748;
        union U0 ***l_2909 = &l_2269;
        uint32_t l_2951[5][1][8] = {{{0UL,0xEF07BE6BL,0UL,4294967293UL,4UL,0x718067F0L,0xC3055773L,0x718067F0L}},{{0xFBC1DD27L,4294967293UL,0xD263EF85L,4294967293UL,0xFBC1DD27L,0x8A879FAFL,4UL,0xB83CA26FL}},{{0xFBC1DD27L,0x8A879FAFL,4UL,0xB83CA26FL,4UL,0x8A879FAFL,0xFBC1DD27L,4294967293UL}},{{0UL,4294967293UL,4UL,0x718067F0L,0xC3055773L,0x718067F0L,4UL,4294967293UL}},{{4UL,0xEF07BE6BL,0xC3055773L,0xEF07BE6BL,0xFBC1DD27L,0x718067F0L,0xFBC1DD27L,0xEF07BE6BL}}};
        uint32_t l_2955 = 0xD5727240L;
        int8_t **l_2963 = &g_2604;
        int8_t ***l_2962 = &l_2963;
        int8_t ****l_2961 = &l_2962;
        uint32_t l_2988 = 1UL;
        uint32_t **l_3006 = &g_2119[0][0];
        uint64_t ***l_3044 = &g_2459;
        int32_t l_3094 = 0xBBCD8489L;
        int64_t l_3098[5];
        uint32_t l_3106 = 0xB5A81DB4L;
        int8_t l_3145 = 0xB5L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2368[i] = &g_50;
        for (i = 0; i < 3; i++)
            l_2395[i] = 1L;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 5; j++)
                l_2507[i][j] = 1L;
        }
        for (i = 0; i < 1; i++)
            l_2747[i] = (void*)0;
        for (i = 0; i < 5; i++)
            l_3098[i] = (-3L);
        for (g_1226.f0 = 7; (g_1226.f0 <= (-9)); g_1226.f0 = safe_sub_func_uint16_t_u_u(g_1226.f0, 1))
        { /* block id: 1087 */
            uint8_t l_2372 = 0x31L;
            int32_t l_2378 = (-1L);
            int32_t l_2379[9][1];
            uint32_t l_2410 = 18446744073709551615UL;
            uint32_t ***l_2415 = &l_2414;
            int32_t l_2481 = 6L;
            int8_t *l_2505 = &l_1923;
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 1; j++)
                    l_2379[i][j] = 0x801E87ECL;
            }
            for (l_1786 = 6; (l_1786 >= 0); l_1786 -= 1)
            { /* block id: 1090 */
                int32_t l_2357 = 0x75DE231EL;
                uint32_t ****l_2369[1];
                int32_t l_2385 = 9L;
                int32_t l_2391 = 8L;
                int32_t l_2400 = 1L;
                int32_t l_2405 = 0x4B4AF36FL;
                int32_t l_2407 = (-1L);
                int32_t l_2409[7][6][6] = {{{(-9L),0x272699EAL,0x6676456BL,0x64BF7144L,(-9L),(-5L)},{(-9L),0xE30AD3ECL,1L,(-5L),4L,0x056BA9F2L},{(-9L),0xB234ABE8L,1L,0x1BFF6495L,0x6676456BL,1L},{0L,0x64BF7144L,0L,(-5L),0x6676456BL,1L},{4L,0xB234ABE8L,(-9L),0xB234ABE8L,4L,(-5L)},{0L,0xE30AD3ECL,(-1L),0x056BA9F2L,(-9L),0xF3ED7555L}},{{(-1L),0x272699EAL,4L,0xE30AD3ECL,0xA94D3F08L,0xF3ED7555L},{2L,1L,(-1L),0x37E0689CL,(-1L),(-5L)},{0xA94D3F08L,0x1BFF6495L,(-9L),1L,0L,1L},{0xFBE7ED0BL,0x29A52E5FL,0L,0x9D286BB9L,0x00477544L,1L},{0xFBE7ED0BL,0x056BA9F2L,1L,1L,1L,0x056BA9F2L},{0xA94D3F08L,0xCF7481F4L,1L,0x37E0689CL,1L,(-5L)}},{{2L,1L,0x6676456BL,0xE30AD3ECL,0L,0x9D286BB9L},{(-1L),1L,0x14FFD6B9L,0x056BA9F2L,1L,0x272699EAL},{0L,0xCF7481F4L,(-9L),0xB234ABE8L,1L,0x1BFF6495L},{4L,0x056BA9F2L,0xDC0F58D2L,(-5L),0x00477544L,1L},{0L,0x29A52E5FL,0xDC0F58D2L,0x1BFF6495L,0L,0x1BFF6495L},{(-9L),0x1BFF6495L,(-9L),(-5L),(-1L),0x272699EAL}},{{(-9L),1L,0x14FFD6B9L,0x64BF7144L,0xA94D3F08L,0x9D286BB9L},{(-9L),0x272699EAL,0x6676456BL,0x64BF7144L,(-9L),(-5L)},{(-9L),0xE30AD3ECL,1L,(-5L),4L,0x056BA9F2L},{(-9L),0xB234ABE8L,1L,0x1BFF6495L,0x6676456BL,1L},{0L,0x64BF7144L,0L,(-5L),0x6676456BL,1L},{4L,0xB234ABE8L,(-9L),0xB234ABE8L,4L,(-5L)}},{{0L,0xE30AD3ECL,(-1L),0x056BA9F2L,(-9L),0xF3ED7555L},{(-1L),0x272699EAL,4L,0xE30AD3ECL,0xA94D3F08L,0xF3ED7555L},{2L,1L,(-1L),0x37E0689CL,(-1L),(-5L)},{0xA94D3F08L,0x1BFF6495L,(-9L),1L,0L,1L},{0xFBE7ED0BL,0x29A52E5FL,0L,0x9D286BB9L,0x00477544L,1L},{0xFBE7ED0BL,0x056BA9F2L,1L,1L,1L,0x056BA9F2L}},{{0xA94D3F08L,0xCF7481F4L,1L,0x37E0689CL,1L,1L},{0x6676456BL,0xBCE2F1D7L,(-1L),0xCF7481F4L,2L,0x29A52E5FL},{(-9L),0xBCE2F1D7L,2L,0x9D286BB9L,1L,(-5L)},{0L,0xF3ED7555L,0xA94D3F08L,0xE30AD3ECL,4L,0x272699EAL},{(-9L),0x9D286BB9L,0xFBE7ED0BL,1L,0xDC0F58D2L,0xBCE2F1D7L},{2L,0xB234ABE8L,0xFBE7ED0BL,0x272699EAL,0L,0x272699EAL}},{{0xA94D3F08L,0x272699EAL,0xA94D3F08L,1L,0x00477544L,(-5L)},{(-9L),0x64BF7144L,2L,1L,1L,0x29A52E5FL},{1L,(-5L),(-1L),1L,(-9L),1L},{(-9L),0xCF7481F4L,0L,1L,(-9L),0x9D286BB9L},{0xA94D3F08L,0xE30AD3ECL,4L,0x272699EAL,(-1L),0x1BFF6495L},{2L,1L,0L,1L,(-1L),0x64BF7144L}}};
                uint64_t **l_2457 = &g_1182;
                float l_2474 = 0xC.D12A0Ep+71;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_2369[i] = &g_210;
                if ((safe_lshift_func_uint16_t_u_s(((g_2348 , (safe_lshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(g_8[l_1786], 4)), (-1L))) != g_8[l_1786]) >= (p_12 & (((*g_1182)++) ^ (safe_add_func_uint64_t_u_u(l_2362, (safe_mul_func_uint8_t_u_u((l_2365[4] != l_2369[0]), (safe_mul_func_uint8_t_u_u(0UL, l_2372))))))))), l_2373)), p_12))) < l_2372), l_2374[1])))
                { /* block id: 1092 */
                    uint32_t *l_2377[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int32_t l_2380 = 3L;
                    int32_t l_2381 = 0L;
                    int32_t l_2387 = 1L;
                    int32_t l_2388 = 1L;
                    int32_t l_2390 = (-1L);
                    int32_t l_2396 = 0xE5B74115L;
                    int32_t l_2397 = 0xC9C791ECL;
                    int32_t l_2398 = 2L;
                    int32_t l_2399 = (-2L);
                    int32_t l_2401 = 0x613677A5L;
                    int32_t l_2402 = 0x5BB41102L;
                    int32_t l_2403 = 0xB30FE5EAL;
                    int32_t l_2404 = 0xE889AAD8L;
                    int32_t l_2406 = (-9L);
                    int32_t l_2408[6] = {0x50D2704CL,0x4260BFFEL,0x4260BFFEL,0x50D2704CL,0x4260BFFEL,0x4260BFFEL};
                    uint32_t ****l_2416 = &l_2415;
                    int i;
                    (*g_1814) = p_11;
                    l_2405 &= ((*g_7) |= (safe_mul_func_uint16_t_u_u(((l_2357 != (l_2406 = (l_2410++))) != (p_12 && ((**g_406) , (l_2413 != ((*l_2416) = l_2415))))), ((l_2408[2] = (safe_sub_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u((**g_2175), (safe_lshift_func_uint8_t_u_s((l_2404 ^ (((safe_rshift_func_int8_t_s_u(((safe_div_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(((safe_add_func_uint16_t_u_u(((((((safe_mod_func_int64_t_s_s((safe_div_func_uint8_t_u_u(((l_2409[2][2][2] , (safe_sub_func_int16_t_s_s(p_12, 0UL))) >= p_12), l_2390)), p_12)) >= 1UL) , &g_680) == (void*)0) ^ p_12) || 0x39FD0B5B68615E9ALL), l_2409[5][4][0])) & g_1399[3]), g_755[4])), l_2401)) == p_12), p_12)) || 18446744073709551615UL) != p_12)), 4)))), p_12))) > 246UL))));
                }
                else
                { /* block id: 1100 */
                    int8_t l_2437 = (-1L);
                    int32_t *l_2438 = &g_1472[1].f0;
                    (*g_2439) = func_22(l_2437, (p_12 , l_2438));
                    if ((*g_407))
                        continue;
                }
                for (l_2181 = 0; (l_2181 <= 0); l_2181 += 1)
                { /* block id: 1106 */
                    uint32_t **l_2455[10][9][2] = {{{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]}},{{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{(void*)0,&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]}},{{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{(void*)0,&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]}},{{(void*)0,&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0}},{{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{(void*)0,&g_2119[0][0]}},{{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{(void*)0,&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]}},{{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0}},{{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]}},{{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{(void*)0,&g_2119[0][0]},{(void*)0,&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{(void*)0,&g_2119[0][0]}},{{(void*)0,&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],(void*)0},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]},{&g_2119[0][0],&g_2119[0][0]}}};
                    int8_t l_2456 = 0xF2L;
                    int32_t *l_2476 = &g_1472[1].f0;
                    int32_t l_2477[3];
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_2477[i] = 0x2178FB9EL;
                    (*g_1814) = p_11;
                    (*g_407) = (g_59 |= ((*g_7) = ((((g_751[l_2181][(l_2181 + 1)][(l_1786 + 2)] & ((((safe_sub_func_int16_t_s_s((safe_add_func_uint16_t_u_u(((*g_1739) = (safe_lshift_func_uint16_t_u_u((0x2085F373A9CE76E7LL & (safe_rshift_func_uint8_t_u_u((l_2407 < (safe_mul_func_int8_t_s_s(0x1FL, (l_2395[0] > (safe_div_func_int64_t_s_s(((g_1472[1].f2 > g_26[1][3][0]) && (((*l_2413) = l_2454[0]) == l_2455[1][4][0])), (*g_1182))))))), 2))), (*g_1739)))), 0UL)), 0x3738L)) & p_12) & l_2456) > 65535UL)) , (**g_1181)) <= (*g_1382)) >= p_12)));
                }
            }
            (*g_1814) = p_11;
        }
        if (((p_12 , ((0UL ^ 0xF3L) != (((*g_1382) = (safe_mod_func_uint32_t_u_u(((((***l_2413)++) & p_12) && (safe_sub_func_int32_t_s_s(((safe_rshift_func_int8_t_s_u(p_12, (**g_2175))) > (((l_2518[5] = ((*l_2517) = l_2516)) != (((((+0x0EE6D7A2L) < (p_12 != p_12)) , (void*)0) != (*g_2458)) , (void*)0)) , (*g_1739))), p_12))), 4L))) , p_12))) & g_714[3][5][0]))
        { /* block id: 1138 */
            uint64_t l_2536 = 0x0D4CFEDF92E9D8B5LL;
            uint32_t *l_2537 = (void*)0;
            uint64_t ***l_2566 = &g_2459;
            int16_t *l_2568 = &l_2327[0][1];
            int32_t l_2571[7] = {0x322263E7L,0x86C88854L,0x86C88854L,0x322263E7L,0x86C88854L,0x86C88854L,0x322263E7L};
            const int32_t **l_2583 = &g_1845;
            const int32_t ***l_2582 = &l_2583;
            const int32_t ****l_2581 = &l_2582;
            int8_t *l_2592[8] = {&g_714[3][1][6],&g_714[3][1][6],&g_714[3][1][6],&g_714[3][1][6],&g_714[3][1][6],&g_714[3][1][6],&g_714[3][1][6],&g_714[3][1][6]};
            int8_t **l_2591 = &l_2592[5];
            int8_t ***l_2590[5][10][4] = {{{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0}},{{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591}},{{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,(void*)0},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591}},{{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591}},{{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,(void*)0,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591},{&l_2591,&l_2591,&l_2591,&l_2591}}};
            int32_t l_2688 = 1L;
            float l_2692 = 0x2.F8A92Cp-72;
            int32_t *l_2787[1][4];
            union U0 *l_2915 = (void*)0;
            uint32_t ****l_2938 = &l_2413;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 4; j++)
                    l_2787[i][j] = &l_2709;
            }
            if (((l_2395[2] , (safe_unary_minus_func_uint16_t_u(((((safe_div_func_int64_t_s_s((safe_sub_func_uint64_t_u_u(p_12, ((void*)0 == l_2526))), (*g_1382))) && ((((*g_1382) = l_2527) < ((((safe_lshift_func_int16_t_s_u(((safe_rshift_func_int16_t_s_u(((safe_div_func_uint64_t_u_u((safe_add_func_int32_t_s_s((0x288ED9C1L > l_2536), (&p_11 != (void*)0))), l_2536)) <= 0xA6L), 1)) >= 0x54L), 12)) , (void*)0) != l_2537) || 5UL)) | l_2536)) && (***g_2174)) & l_2536)))) | (*g_1739)))
            { /* block id: 1140 */
                uint32_t ****l_2540[9][1] = {{&g_210},{&g_210},{(void*)0},{&g_210},{&g_210},{(void*)0},{&g_210},{&g_210},{(void*)0}};
                int32_t l_2541 = (-1L);
                int16_t *l_2569 = &g_343;
                int32_t l_2572 = 0x50B8E3C5L;
                int32_t l_2608 = 0x332367E9L;
                int32_t l_2611 = 0xC3BF3500L;
                int32_t l_2612 = 0x01BB9193L;
                int32_t l_2613 = 0xF349309AL;
                int32_t l_2614 = 0xBE5F99B5L;
                int32_t l_2615 = 0x597CD431L;
                float l_2616 = 0x0.Dp-1;
                int32_t l_2617 = 5L;
                int32_t l_2618[6][9][4] = {{{0xD99D0C57L,1L,0x508A925BL,0x8CBEC45EL},{(-6L),0x09F8E03CL,0x4FED4E28L,0x52127907L},{0x4FED4E28L,0x52127907L,0xE1428946L,1L},{1L,(-8L),(-9L),1L},{(-1L),0x52127907L,1L,0x52127907L},{1L,0x09F8E03CL,(-8L),0x8CBEC45EL},{0xE1428946L,1L,(-1L),0xF19CDF98L},{0x4F751E2AL,1L,0x0293E845L,0xCB027895L},{0x4F751E2AL,1L,(-1L),1L}},{{0xE1428946L,0xCB027895L,(-8L),0x19BBE340L},{1L,0xAB2E5AB3L,1L,0x09F8E03CL},{(-1L),0xE877C398L,(-9L),0xD6B0D55BL},{1L,0xE877C398L,0xE1428946L,0x09F8E03CL},{0x4FED4E28L,0xAB2E5AB3L,0x4FED4E28L,0x19BBE340L},{(-6L),0xCB027895L,0x508A925BL,1L},{0xD99D0C57L,1L,(-1L),0xCB027895L},{0L,1L,(-1L),0xF19CDF98L},{0xD99D0C57L,1L,0x508A925BL,0x8CBEC45EL}},{{(-6L),0x09F8E03CL,0x4FED4E28L,0x52127907L},{0x4FED4E28L,0x52127907L,0xE1428946L,1L},{1L,(-8L),(-9L),1L},{(-1L),0xCB027895L,0L,0xCB027895L},{(-1L),0x8CBEC45EL,0xCA09BEB7L,0x49A8FA3EL},{1L,(-7L),0x4FED4E28L,1L},{0xAD42CE47L,(-8L),(-1L),0xD6B0D55BL},{0xAD42CE47L,1L,0x4FED4E28L,0xE877C398L},{1L,0xD6B0D55BL,0xCA09BEB7L,1L}},{{(-1L),1L,0L,0x8CBEC45EL},{0x4FED4E28L,(-10L),0x0293E845L,0x09F8E03CL},{0L,(-10L),1L,0x8CBEC45EL},{1L,1L,1L,1L},{0x508A925BL,0xD6B0D55BL,(-9L),0xE877C398L},{1L,1L,(-6L),0xD6B0D55BL},{0xD99D0C57L,(-8L),(-6L),1L},{1L,(-7L),(-9L),0x49A8FA3EL},{0x508A925BL,0x8CBEC45EL,1L,0xCB027895L}},{{1L,0xCB027895L,1L,(-8L)},{0L,0x52127907L,0x0293E845L,(-8L)},{0x4FED4E28L,0xCB027895L,0L,0xCB027895L},{(-1L),0x8CBEC45EL,0xCA09BEB7L,0x49A8FA3EL},{1L,(-7L),0x4FED4E28L,1L},{0xAD42CE47L,(-8L),(-1L),0xD6B0D55BL},{0xAD42CE47L,1L,0x4FED4E28L,0xE877C398L},{1L,0xD6B0D55BL,0xCA09BEB7L,1L},{(-1L),1L,0L,0x8CBEC45EL}},{{0x4FED4E28L,(-10L),0x0293E845L,0x09F8E03CL},{0L,(-10L),1L,0x8CBEC45EL},{1L,1L,1L,1L},{0x508A925BL,0xD6B0D55BL,(-9L),0xE877C398L},{1L,1L,(-6L),0xD6B0D55BL},{0xD99D0C57L,(-8L),(-6L),1L},{1L,(-7L),(-9L),0x49A8FA3EL},{0x508A925BL,0x8CBEC45EL,1L,0xCB027895L},{1L,0xCB027895L,1L,(-8L)}}};
                int64_t l_2619 = (-4L);
                union U0 **l_2636 = &g_1289[0][8];
                int32_t l_2637 = 0x65F0813EL;
                uint32_t l_2647 = 8UL;
                uint64_t *l_2678 = (void*)0;
                uint64_t l_2698 = 0UL;
                int i, j, k;
                for (l_1923 = 0; (l_1923 >= (-17)); l_1923 = safe_sub_func_uint16_t_u_u(l_1923, 1))
                { /* block id: 1143 */
                    (*g_1814) = p_11;
                }
            }
            else
            { /* block id: 1208 */
                int8_t l_2705 = 0L;
                union U0 *****l_2706[4];
                uint32_t *** const * const l_2708[10][10][2] = {{{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210}},{{(void*)0,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,(void*)0},{&g_210,&g_210},{(void*)0,&g_210},{&g_210,(void*)0},{&g_210,&g_210},{(void*)0,&g_210}},{{&g_210,(void*)0},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210}},{{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210},{&g_210,&g_210}},{{&g_210,&g_210},{&g_210,&g_210},{&g_210,(void*)0},{&g_210,(void*)0},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210},{&g_210,&g_210}},{{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,(void*)0}},{{&g_210,(void*)0},{(void*)0,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,(void*)0},{&g_210,&g_210}},{{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,(void*)0},{&g_210,(void*)0},{&g_210,&g_210},{&g_210,&g_210}},{{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210}},{{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,(void*)0},{&g_210,(void*)0},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{&g_210,&g_210},{(void*)0,&g_210}}};
                uint32_t *** const * const *l_2707 = &l_2708[3][1][0];
                int32_t l_2732 = 1L;
                int32_t l_2733 = 0x21137411L;
                uint32_t l_2735 = 6UL;
                float **l_2751 = &l_2748;
                int i, j, k;
                for (i = 0; i < 4; i++)
                    l_2706[i] = (void*)0;
                for (g_2271.f0 = 0; (g_2271.f0 < (-22)); --g_2271.f0)
                { /* block id: 1211 */
                    int32_t l_2710 = 0L;
                    int32_t l_2711 = 0x63A9674EL;
                    uint8_t l_2729[10];
                    int32_t l_2734 = 0L;
                    int i;
                    for (i = 0; i < 10; i++)
                        l_2729[i] = 0xCDL;
                }
                for (l_2386 = 1; (l_2386 >= (-9)); l_2386--)
                { /* block id: 1228 */
                    (*l_1975) = (*g_209);
                    return p_11;
                }
                l_2392 ^= ((((((0xE7DFA5FAL ^ (safe_div_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((((((((l_2747[0] = p_11) == l_2748) > ((safe_mul_func_int16_t_s_s(((void*)0 == l_2751), ((((**l_2414) |= ((*g_1739) <= ((((*g_1382) = ((p_12 = ((*g_2604) |= p_12)) != (l_2733 ^= (****l_2581)))) , (*g_1845)) , 0x0853L))) ^ l_2735) >= l_2732))) | g_2719.f0)) >= 0x6B7CB82FL) < (***l_2582)) & (***g_2587)) , p_12), 7)), 65533UL))) != (***l_2582)) , (****l_2581)) && (-1L)) < l_2735) != (*g_407));
            }
        }
        else
        { /* block id: 1346 */
            uint64_t l_2983 = 18446744073709551615UL;
            uint32_t *****l_2997 = &g_2949;
            int32_t l_3045 = 0x5FE39476L;
            const int8_t l_3049 = 0L;
            int32_t l_3074 = 0xE7B34649L;
            int32_t l_3076[8] = {0x17992301L,0x17992301L,0x17992301L,0x17992301L,0x17992301L,0x17992301L,0x17992301L,0x17992301L};
            int32_t l_3119 = 0x24F646E7L;
            int16_t *l_3141 = (void*)0;
            int16_t *l_3142 = &g_343;
            int64_t *l_3143 = &l_3098[4];
            float *l_3144 = &g_530;
            int i;
            for (g_50 = 0; (g_50 < 58); g_50 = safe_add_func_int32_t_s_s(g_50, 5))
            { /* block id: 1349 */
                int32_t ***l_2980 = &g_1814;
                float *l_2986 = &g_443[3];
                float l_3016 = 0xD.83DE65p+2;
                uint8_t l_3017 = 255UL;
                uint8_t l_3020 = 0xE5L;
                int32_t l_3075 = (-1L);
                int32_t l_3079[10][9] = {{1L,0x97D71A25L,0xB705F65AL,0x7F57146BL,0xB705F65AL,0x97D71A25L,1L,0xB9FFFC9CL,0xA16B0F73L},{0xC64EDA79L,(-9L),0xDD4E7C2FL,0x476B462FL,1L,0x97D71A25L,0L,0x9D7A397FL,0xDD4E7C2FL},{0x7F57146BL,0xDD4E7C2FL,0x3D00FA40L,1L,0xA16B0F73L,(-9L),0xEFD4E25CL,0xB9FFFC9CL,0x97D71A25L},{0x7F57146BL,0xE58FE04BL,(-9L),(-1L),0xE58FE04BL,0xDD4E7C2FL,1L,0xB705F65AL,0L},{0xC64EDA79L,0x9D7A397FL,0x3D00FA40L,(-1L),0xE58FE04BL,0xE58FE04BL,0xDD4E7C2FL,0x6F53151EL,0xE6BFAF9EL},{0L,(-5L),0x418F4A30L,(-9L),0xFF728BBEL,0xE6BFAF9EL,0x9D7A397FL,(-5L),0xE6BFAF9EL},{(-9L),0x6F53151EL,0xC64A5B1EL,0x97D71A25L,0x5FD26009L,(-5L),6L,0x418F4A30L,0x50758B5EL},{6L,(-5L),0x5FD26009L,0x97D71A25L,0xC64A5B1EL,0x6F53151EL,(-9L),(-5L),(-1L)},{0x9D7A397FL,0xE6BFAF9EL,0xFF728BBEL,(-9L),0x418F4A30L,(-5L),0L,(-5L),0x418F4A30L},{0xDD4E7C2FL,0xFFB35B82L,0xFFB35B82L,0xDD4E7C2FL,0x6F53151EL,0xE6BFAF9EL,1L,0x418F4A30L,0xFF728BBEL}};
                int i, j;
            }
            (*l_3144) = (safe_div_func_float_f_f(p_12, ((safe_mul_func_float_f_f((safe_add_func_float_f_f((((l_2983 | (!(18446744073709551615UL && ((*l_3143) = (p_12 > (safe_unary_minus_func_int64_t_s(((*g_1382) = ((***g_2310) ^ (l_3119 == (p_12 == ((safe_sub_func_uint16_t_u_u((~((((*l_3142) = (safe_unary_minus_func_uint32_t_u(((l_3140 = (((safe_lshift_func_int16_t_s_s((+l_3129), 6)) || ((safe_lshift_func_int8_t_s_u(((safe_div_func_float_f_f((((((safe_sub_func_int16_t_s_s((safe_add_func_uint64_t_u_u(((safe_mul_func_uint8_t_u_u((*g_639), 0x4DL)) ^ p_12), l_3076[6])), p_12)) <= p_12) != 0xD76698877290C26CLL) && 0xA4FFDF2CL) , 0x1.Bp-1), l_3119)) , l_3045), 0)) || (*g_1739))) , 8L)) < (***g_2602))))) != p_12) < (*g_1182))), (-6L))) >= p_12)))))))))))) , 0x9.C7EA85p+17) >= l_3045), (*g_2686))), 0x3.6CE351p-79)) < p_12)));
            (*g_407) |= (l_3074 = ((*g_7) ^= l_3145));
        }
        p_11 = p_11;
        (*g_407) &= 1L;
    }
    return l_3146;
}


/* ------------------------------------------ */
/* 
 * reads : g_7 g_8 g_1181 g_1182 g_344 g_1383 g_1381 g_1382 g_330 g_751 g_639 g_92 g_407 g_442 g_443 g_939 g_95 g_1526 g_379 g_1696 g_26 g_1399 g_174 g_150.f0 g_343 g_63 g_375 g_376 g_58 g_1082 g_59 g_511
 * writes: g_344 g_8 g_1082 g_59 g_751 g_92 g_26 g_63 g_176 g_741 g_530 g_443 g_511 g_379 g_174 g_150.f0 g_343 g_376
 */
static int32_t * func_13(uint16_t  p_14, const int16_t  p_15, int32_t * p_16)
{ /* block id: 670 */
    uint8_t **l_1571 = &g_639;
    uint8_t ***l_1570 = &l_1571;
    int32_t l_1573 = 0x77FE31E0L;
    const int32_t l_1596 = 9L;
    int32_t l_1664 = 1L;
    union U0 **l_1712 = &g_205;
    int32_t *l_1727[9][9][3] = {{{&g_1472[1].f0,&l_1664,&g_1226.f0},{&g_8[5],&g_1226.f0,&g_8[1]},{&g_8[1],&g_64,&l_1573},{&g_379,&g_64,&g_1472[1].f0},{&g_1472[1].f0,&g_1226.f0,&g_59},{&g_1226.f0,&l_1664,(void*)0},{&g_1164,&l_1573,&g_1164},{(void*)0,(void*)0,&g_379},{&g_59,&g_59,&g_1164}},{{(void*)0,&g_8[1],(void*)0},{&g_150.f0,&g_64,&g_59},{&g_8[1],&g_8[3],&g_1472[1].f0},{&g_206[0][0].f0,&g_1472[1].f0,&l_1573},{&g_206[0][0].f0,&g_8[1],&g_8[1]},{&g_8[1],&g_379,&g_1226.f0},{&g_150.f0,&g_150.f0,&g_64},{(void*)0,&g_206[0][0].f0,(void*)0},{&g_59,&g_1226.f0,&g_64}},{{(void*)0,&g_206[0][0].f0,&g_1472[1].f0},{&g_1164,&g_150.f0,&g_8[4]},{&g_1226.f0,&g_379,&g_150.f0},{&g_1472[1].f0,&g_8[1],&g_59},{&g_379,&g_1472[1].f0,&g_59},{&g_8[1],&g_8[3],&g_150.f0},{&g_8[5],&g_64,&g_8[4]},{&g_1472[1].f0,&g_8[1],&g_1472[1].f0},{&g_59,&g_59,&g_64}},{{&l_1664,(void*)0,(void*)0},{&g_59,&l_1573,&g_64},{&g_1472[1].f0,&l_1664,&g_1226.f0},{&g_8[5],&g_1226.f0,&g_8[1]},{&g_8[1],&g_64,&l_1573},{&g_379,&g_64,&g_1472[1].f0},{&g_1472[1].f0,&g_1226.f0,&g_59},{&g_1226.f0,&l_1664,(void*)0},{&g_1164,&l_1573,&g_1164}},{{(void*)0,(void*)0,&g_379},{&g_59,&g_59,&g_1164},{(void*)0,&g_8[1],(void*)0},{&g_150.f0,&g_64,&g_59},{&g_8[1],&g_8[3],&g_1472[1].f0},{&g_206[0][0].f0,&g_1472[1].f0,&l_1573},{&g_206[0][0].f0,&g_8[1],&g_8[1]},{&g_8[1],&g_379,&g_1226.f0},{&g_150.f0,&g_150.f0,&g_64}},{{(void*)0,&g_206[0][0].f0,(void*)0},{&g_59,&g_1226.f0,&g_64},{(void*)0,&g_206[0][0].f0,&g_8[1]},{&g_59,&g_59,&g_1164},{(void*)0,&l_1573,&g_59},{(void*)0,&g_1226.f0,&g_206[0][0].f0},{&g_8[4],&g_59,&g_206[0][0].f0},{&l_1664,&g_64,&g_59},{&g_1226.f0,&g_379,&g_1164}},{{&g_8[1],&g_8[1],&g_8[1]},{(void*)0,&g_1164,&g_8[0]},{(void*)0,&g_1472[1].f0,&l_1664},{(void*)0,&g_206[0][0].f0,&g_379},{&g_8[1],&g_64,&g_64},{&g_1226.f0,(void*)0,&g_1226.f0},{&l_1664,&g_8[0],&g_206[0][0].f0},{&g_8[4],&g_8[0],&g_59},{(void*)0,(void*)0,&g_150.f0}},{{(void*)0,&g_64,&g_1472[1].f0},{&g_59,&g_206[0][0].f0,&g_8[3]},{&g_8[1],&g_1472[1].f0,&l_1573},{&g_1472[1].f0,&g_1164,&g_8[3]},{&g_1472[1].f0,&g_8[1],&g_1472[1].f0},{&l_1573,&g_379,&g_150.f0},{&g_8[3],&g_64,&g_59},{&l_1573,&g_59,&g_206[0][0].f0},{&l_1573,&g_1226.f0,&g_1226.f0}},{{&g_8[3],&l_1573,&g_64},{&l_1573,&g_59,&g_379},{&g_1472[1].f0,(void*)0,&l_1664},{&g_1472[1].f0,&g_64,&g_8[0]},{&g_8[1],(void*)0,&g_8[1]},{&g_59,&g_59,&g_1164},{(void*)0,&l_1573,&g_59},{(void*)0,&g_1226.f0,&g_206[0][0].f0},{&g_8[4],&g_59,&g_206[0][0].f0}}};
    uint32_t l_1728 = 1UL;
    int i, j, k;
lbl_1705:
    for (g_344 = 0; (g_344 <= 1); g_344 += 1)
    { /* block id: 673 */
        uint8_t ***l_1572[8][8] = {{&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571},{(void*)0,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,(void*)0},{&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571},{(void*)0,&l_1571,&l_1571,(void*)0,&l_1571,&l_1571,(void*)0,&l_1571},{&l_1571,&l_1571,&l_1571,&l_1571,(void*)0,&l_1571,(void*)0,&l_1571},{&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571},{&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571,&l_1571},{&l_1571,(void*)0,&l_1571,(void*)0,&l_1571,&l_1571,&l_1571,&l_1571}};
        int i, j;
        l_1572[4][1] = l_1570;
        (*g_7) &= l_1573;
    }
    for (g_1082 = 29; (g_1082 == 40); g_1082 = safe_add_func_uint8_t_u_u(g_1082, 5))
    { /* block id: 679 */
        int32_t l_1587[10][7] = {{1L,0xEBDFA1C7L,0x405B5E6CL,0xE5873C27L,(-2L),(-1L),(-8L)},{2L,0xE5873C27L,0x78AF2AD9L,0x99ABB39DL,0x405B5E6CL,0x405B5E6CL,0x99ABB39DL},{1L,(-8L),1L,0xC010716AL,0x405B5E6CL,0xBDDECDB7L,(-6L)},{0xEBDFA1C7L,(-1L),(-1L),(-8L),(-2L),0xA09D4F75L,(-1L)},{(-6L),(-1L),0xEBDFA1C7L,0xBDDECDB7L,(-2L),0L,0L},{0xE5873C27L,0x130F667EL,(-6L),0x130F667EL,0xE5873C27L,0xA09D4F75L,0L},{(-1L),0x405B5E6CL,0x78AF2AD9L,(-8L),0L,0xEBDFA1C7L,(-1L)},{0x78AF2AD9L,0xA09D4F75L,0xBDDECDB7L,(-3L),(-6L),0xE5873C27L,0x5202109BL},{(-1L),(-8L),0x457DD02AL,0x5202109BL,0x457DD02AL,(-8L),(-1L)},{0xE5873C27L,(-8L),0xC010716AL,0L,0x130F667EL,(-1L),(-6L)}};
        int32_t l_1599 = 0xBF7DF18BL;
        union U0 **l_1609 = &g_205;
        float *l_1612 = &g_530;
        int8_t l_1671 = 0xB5L;
        int i, j;
        for (g_59 = 8; (g_59 != (-9)); g_59--)
        { /* block id: 682 */
            uint8_t l_1590 = 250UL;
            int8_t *l_1591 = &g_26[5][5][1];
            int64_t *l_1597 = (void*)0;
            int64_t *l_1598[9];
            int32_t l_1610[3][5] = {{0x1BFB89AFL,0x82CA3289L,0x9629B206L,0x82CA3289L,0x1BFB89AFL},{0x1BFB89AFL,0x82CA3289L,0x9629B206L,0x82CA3289L,0x1BFB89AFL},{0x1BFB89AFL,0x82CA3289L,0x9629B206L,0x82CA3289L,0x1BFB89AFL}};
            int16_t l_1616[2];
            int8_t **l_1618 = (void*)0;
            int8_t ***l_1617 = &l_1618;
            int64_t l_1639 = 2L;
            int16_t l_1648 = 0xB330L;
            int32_t l_1703 = (-1L);
            int i, j;
            for (i = 0; i < 9; i++)
                l_1598[i] = &g_1399[5];
            for (i = 0; i < 2; i++)
                l_1616[i] = 0x4BBBL;
            (*g_407) = (((safe_mod_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((l_1599 |= ((+(p_14 == (**g_1181))) , (safe_mul_func_uint8_t_u_u(((((***l_1570) ^= (safe_mul_func_int8_t_s_s((l_1587[8][2] & (***g_1383)), (safe_mod_func_uint16_t_u_u((g_751[0][3][2] ^= p_14), 65528UL))))) != ((*l_1591) = l_1590)) | (safe_sub_func_int16_t_s_s((l_1573 = (safe_add_func_int64_t_s_s((*g_1382), ((l_1573 || 8L) & 0xAC51CE9D66F46A11LL)))), l_1596))), p_14)))), 1L)), 0x69E924682BB62A4FLL)) , p_14) || l_1599);
            if (l_1590)
            { /* block id: 689 */
                int32_t **l_1600 = (void*)0;
                const int32_t **l_1601 = (void*)0;
                int32_t **l_1602 = &g_176;
                float *l_1615[8][4][1] = {{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}},{{&g_443[3]},{&g_443[3]},{&g_443[3]},{&g_443[3]}}};
                int8_t ****l_1619 = &l_1617;
                int i, j, k;
                (*l_1602) = (void*)0;
                (*g_442) = (l_1616[0] = (((p_14 < 0x5.708896p-57) == (*g_442)) > (safe_sub_func_float_f_f(((*l_1612) = (safe_sub_func_float_f_f((safe_add_func_float_f_f(p_15, (l_1610[0][2] = ((void*)0 != l_1609)))), ((!((l_1612 == ((safe_mul_func_uint8_t_u_u(((***l_1570) = l_1590), (((*g_939) = &p_16) != (void*)0))) , p_16)) != p_14)) != 0x6.85C0BEp+64)))), p_15))));
                (*l_1619) = l_1617;
                if (l_1616[0])
                    continue;
            }
            else
            { /* block id: 699 */
                int16_t l_1644 = 0x913BL;
                uint8_t l_1668 = 0x13L;
                for (l_1573 = 2; (l_1573 == (-15)); l_1573 = safe_sub_func_int32_t_s_s(l_1573, 4))
                { /* block id: 702 */
                    float l_1642 = 0x1.Ap-1;
                    int32_t l_1643 = (-8L);
                    for (g_344 = 0; (g_344 != 56); ++g_344)
                    { /* block id: 705 */
                        uint8_t l_1636 = 6UL;
                        float *l_1645 = &l_1642;
                        (*l_1645) = (((*l_1612) = 0xB.AAA249p+66) >= ((*g_442) < (safe_div_func_float_f_f((((safe_sub_func_float_f_f((safe_sub_func_float_f_f((((safe_mod_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((((safe_sub_func_int32_t_s_s(l_1596, ((((((((0x9CDDAE1DL && l_1636) , ((((*g_639) == (((safe_mul_func_int16_t_s_s(l_1639, ((-4L) > ((((safe_add_func_float_f_f((l_1642 >= l_1643), l_1643)) != l_1644) , g_95[0][4][1]) > p_15)))) , l_1636) & 0UL)) > (*g_1182)) || 18446744073709551608UL)) | (*g_1182)) >= p_15) , 0x9805B568L) , (*g_7)) == l_1587[8][2]) ^ l_1590))) | 0xEE64018950E7B509LL) , 0xBC4768DB79DEAA33LL), 1UL)), (*g_639))) , l_1599) == 0x7.1D64DCp+43), l_1636)), 0x1.Cp-1)) > 0x9.C69FD7p+63) <= 0x1.0p+1), p_15))));
                    }
                    for (g_511 = 0; (g_511 <= 24); g_511++)
                    { /* block id: 711 */
                        int32_t *l_1649 = &g_1164;
                        int32_t *l_1650 = &g_1226.f0;
                        int32_t *l_1651 = &g_8[4];
                        int32_t *l_1652 = &l_1643;
                        int32_t *l_1653 = &g_206[0][0].f0;
                        int32_t *l_1654 = (void*)0;
                        int32_t *l_1655 = (void*)0;
                        int32_t *l_1656 = &g_1472[1].f0;
                        int32_t *l_1657 = &g_8[1];
                        int32_t *l_1658 = &g_1164;
                        int32_t *l_1659 = (void*)0;
                        int32_t *l_1660 = &g_8[1];
                        int32_t *l_1661 = &g_8[0];
                        int32_t *l_1662 = (void*)0;
                        int32_t *l_1663 = &g_1164;
                        int32_t *l_1665 = &l_1610[2][0];
                        int32_t *l_1666 = &l_1664;
                        int32_t *l_1667[7] = {&l_1664,&g_1472[1].f0,&l_1664,&l_1664,&g_1472[1].f0,&l_1664,&l_1664};
                        int i;
                        (*g_1526) = p_16;
                        l_1668++;
                    }
                }
            }
            if (l_1671)
            { /* block id: 717 */
                uint8_t l_1704[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_1704[i] = 255UL;
                for (g_379 = 0; (g_379 <= 9); g_379 += 1)
                { /* block id: 720 */
                    float l_1675[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1675[i] = 0x6.B43C3Dp+83;
                    for (l_1648 = 0; (l_1648 <= 9); l_1648 += 1)
                    { /* block id: 723 */
                        int8_t l_1672 = 0x35L;
                        int64_t l_1684 = (-1L);
                        union U0 * const *l_1695 = (void*)0;
                        union U0 * const ** const l_1694 = &l_1695;
                        union U0 * const ** const *l_1693[10][2] = {{&l_1694,&l_1694},{&l_1694,&l_1694},{(void*)0,(void*)0},{(void*)0,&l_1694},{&l_1694,&l_1694},{&l_1694,&l_1694},{&l_1694,&l_1694},{(void*)0,(void*)0},{(void*)0,&l_1694},{&l_1694,&l_1694}};
                        uint16_t *l_1697 = &g_751[0][0][8];
                        int i, j;
                        if (l_1672)
                            break;
                        (*g_7) = (safe_sub_func_uint8_t_u_u((*g_639), l_1587[3][4]));
                        (*g_407) = (((safe_sub_func_int64_t_s_s(0xEDBBC620385F612FLL, ((g_174 &= (safe_mul_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_add_func_int64_t_s_s((((((l_1684 , (safe_add_func_int16_t_s_s(p_14, ((*l_1697) = (((p_14 < (((safe_add_func_uint16_t_u_u(p_15, (((safe_lshift_func_uint16_t_u_u((((safe_rshift_func_int8_t_s_s(l_1672, ((void*)0 == l_1693[5][0]))) && 0xCA8806E743BB44CALL) | g_1696), g_26[1][3][0])) && 0xF07DL) & 0x4D2EB212E8FB2A4ALL))) <= p_15) | 0UL)) | 1UL) | (*g_1382)))))) , p_14) ^ p_14) && 255UL) >= g_1399[2]), p_14)), 0x754B4E37BC133766LL)), p_14))) < 1L))) != p_15) & p_15);
                    }
                }
                for (g_150.f0 = 0; (g_150.f0 <= 6); g_150.f0++)
                { /* block id: 733 */
                    int32_t *l_1702[8][4][4] = {{{&g_150.f0,&g_1472[1].f0,(void*)0,(void*)0},{&l_1587[4][0],&l_1587[4][0],&l_1599,&g_64},{&g_150.f0,(void*)0,&g_8[2],&l_1573},{&g_150.f0,&g_64,&l_1664,&g_8[2]}},{{&g_1472[1].f0,&g_64,&g_1164,&l_1573},{&g_64,(void*)0,&l_1599,&g_64},{&g_1164,&l_1587[4][0],(void*)0,(void*)0},{&g_1226.f0,&g_1472[1].f0,&g_1164,&g_1472[1].f0}},{{&g_8[0],&g_1164,&g_1164,&l_1573},{&g_150.f0,&g_8[0],&g_379,(void*)0},{&l_1573,&g_150.f0,&l_1599,&l_1610[1][0]},{&l_1573,(void*)0,&g_379,&l_1587[4][0]}},{{&g_150.f0,&l_1610[1][0],&g_1164,&g_8[2]},{&g_8[0],&g_1226.f0,&g_1164,&g_150.f0},{&g_1226.f0,(void*)0,(void*)0,&g_1226.f0},{&g_1164,&l_1573,&l_1599,(void*)0}},{{&g_64,&l_1573,&g_1164,&g_8[0]},{&g_1472[1].f0,&g_1164,&l_1664,&g_8[0]},{&g_150.f0,&g_8[2],&l_1587[8][2],(void*)0},{&g_1164,&g_150.f0,&g_150.f0,&l_1599}},{{&l_1664,&g_1164,(void*)0,&g_1164},{(void*)0,&l_1599,(void*)0,&l_1587[8][2]},{&g_8[2],&g_1164,&l_1573,&l_1664},{&g_1164,&g_1164,&g_1164,&g_1164}},{{&g_59,&g_1164,&g_1164,(void*)0},{&g_1164,(void*)0,&l_1573,&g_8[2]},{&g_8[2],&g_1164,(void*)0,&g_379},{(void*)0,&g_379,(void*)0,(void*)0}},{{&l_1664,&l_1664,&g_150.f0,(void*)0},{&g_1164,&g_1164,&l_1587[8][2],&g_150.f0},{(void*)0,(void*)0,(void*)0,&l_1587[8][2]},{&g_379,(void*)0,&l_1573,&g_150.f0}}};
                    int32_t ***l_1706 = (void*)0;
                    int i, j, k;
                    for (g_343 = (-20); (g_343 < (-25)); g_343 = safe_sub_func_uint8_t_u_u(g_343, 4))
                    { /* block id: 736 */
                        return p_16;
                    }
                    l_1704[1] = (p_15 | ((*l_1591) = l_1703));
                    if (l_1648)
                        goto lbl_1705;
                    if (((void*)0 != l_1706))
                    { /* block id: 742 */
                        (*g_7) = (*g_407);
                    }
                    else
                    { /* block id: 744 */
                        if (l_1704[1])
                            break;
                    }
                }
            }
            else
            { /* block id: 748 */
                uint32_t *l_1710 = (void*)0;
                uint32_t *l_1711 = &g_174;
                int32_t l_1726 = (-2L);
                (*g_407) = ((((safe_add_func_uint8_t_u_u(((+((*l_1711) = p_15)) , (((p_14 ^ (((((*g_375) = (l_1712 = (*g_375))) != (void*)0) , ((safe_lshift_func_int16_t_s_s((safe_div_func_int32_t_s_s(((((((g_58 >= (safe_rshift_func_uint8_t_u_u((~(l_1664 != (safe_mul_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((251UL || (safe_lshift_func_int16_t_s_s(5L, ((((*g_1182) = (0x4CB0D4F6L || l_1587[7][0])) > (*g_1382)) >= l_1610[0][2])))), (-1L))), l_1726)))), 0))) , (**g_1181)) || p_14) | p_14) <= p_15) & (-5L)), 1UL)), 14)) , l_1587[7][3])) != l_1616[0])) , 0x09L) | 1L)), 0x77L)) , p_15) == 0x37A6FF10385E9E83LL) != (*g_639));
            }
        }
        l_1664 |= 0xA17E4E2BL;
        if ((*g_407))
            break;
    }
    --l_1728;
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads : g_150.f0 g_749 g_26 g_407 g_63 g_32 g_1055 g_1381 g_1383 g_1399 g_330 g_1472.f0 g_563 g_639 g_92 g_64 g_1164
 * writes: g_150.f0 g_26 g_63 g_1055 g_330 g_1381 g_443 g_755 g_219 g_511 g_92 g_1164
 */
static int64_t  func_18(const int32_t * p_19, int8_t  p_20, int8_t  p_21)
{ /* block id: 571 */
    int16_t l_1364 = (-2L);
    int32_t l_1367[5] = {0xCD5A2B7EL,0xCD5A2B7EL,0xCD5A2B7EL,0xCD5A2B7EL,0xCD5A2B7EL};
    uint32_t l_1368 = 1UL;
    union U0 *l_1392 = &g_206[0][0];
    uint64_t ** const l_1397 = &g_1182;
    int32_t l_1455[1];
    uint64_t l_1461 = 8UL;
    const union U0 *l_1471 = &g_1472[1];
    const union U0 **l_1470[9][3] = {{&l_1471,(void*)0,&l_1471},{&l_1471,&l_1471,(void*)0},{&l_1471,&l_1471,&l_1471},{(void*)0,&l_1471,&l_1471},{(void*)0,&l_1471,&l_1471},{(void*)0,(void*)0,&l_1471},{&l_1471,(void*)0,&l_1471},{&l_1471,(void*)0,&l_1471},{&l_1471,&l_1471,(void*)0}};
    const union U0 ***l_1469 = &l_1470[7][0];
    int16_t l_1476 = 1L;
    int32_t l_1477 = (-1L);
    uint16_t l_1499[10][1] = {{0x5AE7L},{0x7194L},{0x5AE7L},{0x7194L},{0x5AE7L},{0x7194L},{0x5AE7L},{0x7194L},{0x5AE7L},{0x7194L}};
    int8_t *l_1553 = (void*)0;
    int8_t **l_1552 = &l_1553;
    int i, j;
    for (i = 0; i < 1; i++)
        l_1455[i] = 0xBBC4B16BL;
    for (g_150.f0 = 0; (g_150.f0 <= 1); g_150.f0 += 1)
    { /* block id: 574 */
        int32_t *l_1365 = (void*)0;
        int32_t *l_1366[10][5] = {{&g_8[4],&g_379,&g_379,&g_8[4],&g_379},{&g_59,&g_59,&g_379,&g_59,&g_59},{&g_379,&g_8[4],&g_379,&g_379,&g_8[4]},{&g_59,&g_1226.f0,&g_1226.f0,&g_59,&g_1226.f0},{&g_8[4],&g_8[4],&g_1226.f0,&g_8[4],&g_8[4]},{&g_1226.f0,&g_59,&g_1226.f0,&g_1226.f0,&g_59},{&g_8[4],&g_379,&g_379,&g_8[4],&g_379},{&g_59,&g_59,&g_379,&g_59,&g_59},{&g_379,&g_8[4],&g_379,&g_379,&g_8[4]},{&g_59,&g_1226.f0,&g_1226.f0,&g_59,&g_1226.f0}};
        const int32_t **l_1371 = (void*)0;
        const int32_t *l_1373 = (void*)0;
        const int32_t **l_1372[10][3] = {{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373},{&l_1373,&l_1373,&l_1373}};
        int8_t *l_1376 = &g_26[4][6][0];
        int i, j;
        --l_1368;
        p_19 = p_19;
        (*g_407) ^= ((l_1367[1] = g_749[0]) != (safe_add_func_uint8_t_u_u(l_1368, ((*l_1376) |= l_1364))));
        return g_32[6][3];
    }
    for (g_1055 = 0; (g_1055 >= 23); g_1055 = safe_add_func_int64_t_s_s(g_1055, 5))
    { /* block id: 584 */
        int16_t l_1387 = (-10L);
        union U0 *l_1393 = &g_1226;
        int32_t l_1405 = 0x5518A747L;
        int32_t l_1412 = 0x58668812L;
        int32_t l_1414 = (-1L);
        int32_t l_1416 = 0x8137C461L;
        int32_t l_1417 = (-1L);
        int32_t l_1418 = 0L;
        int32_t l_1419[10][10][2] = {{{(-1L),0x43DCD021L},{0xD3F8E2C2L,0x17390595L},{0x8D608C07L,(-1L)},{0xF3E657B0L,0x99A9707CL},{(-9L),0x31E6ED2CL},{0x1EEED631L,0xD3F8E2C2L},{7L,7L},{(-1L),0xD3F8E2C2L},{0x687177ABL,0x43DCD021L},{0xF3E657B0L,0x40DEA5D2L}},{{0xFCC02763L,0xF3E657B0L},{0x86C54CFBL,8L},{0x86C54CFBL,0xF3E657B0L},{0xFCC02763L,0x40DEA5D2L},{0xF3E657B0L,0x43DCD021L},{0x687177ABL,0xD3F8E2C2L},{(-1L),0x778D944AL},{0x778D944AL,0L},{0L,(-1L)},{7L,0x86C54CFBL}},{{0x99A9707CL,0xCF74877BL},{0xD3F8E2C2L,(-9L)},{0L,0x31E6ED2CL},{0xCF74877BL,0x31E6ED2CL},{0L,(-9L)},{0xD3F8E2C2L,0xCF74877BL},{0x99A9707CL,0x86C54CFBL},{7L,(-1L)},{0L,0L},{0x778D944AL,0x778D944AL}},{{(-1L),0xD3F8E2C2L},{0x687177ABL,0x43DCD021L},{0xF3E657B0L,0x40DEA5D2L},{0xFCC02763L,0xF3E657B0L},{0x86C54CFBL,8L},{0x86C54CFBL,0xF3E657B0L},{0xFCC02763L,0x40DEA5D2L},{0xF3E657B0L,0x43DCD021L},{0x687177ABL,0xD3F8E2C2L},{(-1L),0x778D944AL}},{{0x778D944AL,0L},{0L,(-1L)},{7L,0x86C54CFBL},{0x99A9707CL,0xCF74877BL},{0xD3F8E2C2L,(-9L)},{0L,0x31E6ED2CL},{0xCF74877BL,0x31E6ED2CL},{0L,(-9L)},{0xD3F8E2C2L,0xCF74877BL},{0x99A9707CL,0x86C54CFBL}},{{7L,(-1L)},{0L,0L},{0x778D944AL,0x778D944AL},{(-1L),0xD3F8E2C2L},{0x687177ABL,0x43DCD021L},{0xF3E657B0L,0x40DEA5D2L},{0xFCC02763L,0xF3E657B0L},{0x86C54CFBL,8L},{0x86C54CFBL,0xF3E657B0L},{0xFCC02763L,0x40DEA5D2L}},{{0xF3E657B0L,0x43DCD021L},{0x687177ABL,0xD3F8E2C2L},{(-1L),0x778D944AL},{0x778D944AL,0L},{0L,(-1L)},{7L,0x86C54CFBL},{0x99A9707CL,0xCF74877BL},{0xD3F8E2C2L,(-9L)},{0L,0x31E6ED2CL},{0xCF74877BL,0x31E6ED2CL}},{{0L,(-9L)},{0xD3F8E2C2L,0xCF74877BL},{0x99A9707CL,0x86C54CFBL},{7L,(-1L)},{0L,0L},{0x778D944AL,0x778D944AL},{(-1L),0xD3F8E2C2L},{0x687177ABL,0x43DCD021L},{0xF3E657B0L,0x40DEA5D2L},{0xFCC02763L,0xF3E657B0L}},{{0x86C54CFBL,8L},{0x86C54CFBL,0xF3E657B0L},{0xFCC02763L,0x40DEA5D2L},{0xF3E657B0L,0x43DCD021L},{0x687177ABL,0xD3F8E2C2L},{(-1L),0x778D944AL},{0x778D944AL,0L},{0L,(-1L)},{7L,0x86C54CFBL},{0x99A9707CL,0xCF74877BL}},{{0xD3F8E2C2L,(-9L)},{0L,0x31E6ED2CL},{0xCF74877BL,0x31E6ED2CL},{0L,(-9L)},{0xD3F8E2C2L,0xCF74877BL},{0x99A9707CL,0x86C54CFBL},{7L,(-1L)},{0L,0L},{0x778D944AL,0x778D944AL},{(-1L),0xD3F8E2C2L}}};
        int16_t **l_1520 = (void*)0;
        uint32_t *l_1534 = (void*)0;
        int8_t *l_1551 = &g_498[8][4][2];
        int8_t **l_1550[9] = {&l_1551,&l_1551,&l_1551,&l_1551,&l_1551,(void*)0,(void*)0,&l_1551,(void*)0};
        int8_t ***l_1549[3][6][1] = {{{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]}},{{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]}},{{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]},{&l_1550[0]}}};
        uint16_t *l_1556[9][7] = {{&l_1499[4][0],(void*)0,(void*)0,&l_1499[4][0],&l_1499[2][0],&g_751[0][0][8],&g_755[0]},{(void*)0,&g_751[0][3][8],&l_1499[0][0],&l_1499[9][0],&l_1499[9][0],&l_1499[0][0],&g_751[0][3][8]},{&l_1499[2][0],(void*)0,(void*)0,&g_755[0],(void*)0,&g_755[0],&g_755[0]},{&g_755[4],(void*)0,&l_1499[9][0],(void*)0,&g_755[4],&g_751[0][2][6],&l_1499[0][0]},{&l_1499[2][0],&g_751[0][0][8],(void*)0,&g_755[0],&g_755[0],&g_755[0],(void*)0},{&l_1499[0][0],&l_1499[0][0],(void*)0,&l_1499[9][0],&g_751[0][3][8],&l_1499[0][0],&l_1499[0][0]},{&l_1499[2][0],&g_755[0],&l_1499[4][0],&l_1499[4][0],&g_755[0],&l_1499[2][0],&g_755[0]},{&g_755[4],(void*)0,(void*)0,&g_751[0][2][7],&g_751[0][3][8],&g_751[0][3][8],&g_751[0][2][7]},{&l_1499[2][0],&g_755[4],&l_1499[2][0],&g_755[0],&g_755[0],(void*)0,&l_1499[2][0]}};
        uint8_t **l_1564 = (void*)0;
        int16_t *l_1565 = (void*)0;
        int16_t *l_1566 = &g_219;
        int32_t *l_1567 = &g_511;
        int32_t *l_1568 = (void*)0;
        int32_t *l_1569 = &g_1164;
        int i, j, k;
        for (g_330 = 16; (g_330 == 11); g_330 = safe_sub_func_uint8_t_u_u(g_330, 7))
        { /* block id: 587 */
            float *l_1384 = &g_443[2];
            int64_t *l_1398 = &g_1399[3];
            int32_t l_1400 = 0x6755FF38L;
            int32_t *l_1401 = (void*)0;
            int32_t *l_1402 = (void*)0;
            int32_t *l_1403 = &l_1367[1];
            int16_t *l_1404 = (void*)0;
            float l_1413[1][7];
            int32_t l_1415[2][10][9] = {{{0x88F82DCFL,1L,1L,(-6L),(-7L),(-6L),1L,1L,0x88F82DCFL},{0x90332DB5L,(-1L),0x412EF4CCL,0x101CFB27L,0x88F82DCFL,0x3B8CA3D7L,1L,(-1L),0xD2364632L},{0xC2F0412DL,0xD2364632L,(-1L),1L,0x3B8CA3D7L,0x88F82DCFL,0x101CFB27L,0x412EF4CCL,(-1L)},{0x90332DB5L,0x88F82DCFL,1L,1L,(-6L),(-7L),(-6L),1L,1L},{0x88F82DCFL,0x88F82DCFL,3L,0L,0xC2F0412DL,(-1L),0xBDB503CCL,(-7L),0x268A4B34L},{(-6L),0xD2364632L,0x03A697C5L,0xEE9B094FL,0x5E513084L,0x101CFB27L,(-1L),0x73FEF656L,0x90D9D2BFL},{1L,(-1L),3L,0xA311D025L,(-1L),0xEB4B0433L,0x412EF4CCL,0xEB4B0433L,(-1L)},{0xA311D025L,1L,1L,0xA311D025L,(-1L),0x03A697C5L,(-7L),0xD2364632L,0L},{(-1L),0x268A4B34L,(-1L),0xEE9B094FL,3L,1L,0x03A697C5L,0xA311D025L,0xEB4B0433L},{0x3B8CA3D7L,0x90D9D2BFL,0x412EF4CCL,0L,(-1L),0xD2364632L,(-1L),(-1L),(-1L)}},{{0xD2364632L,(-1L),1L,1L,(-1L),0xD2364632L,0x88F82DCFL,0x84B7A889L,0xEE9B094FL},{0xBDB503CCL,0L,(-7L),1L,0x5E513084L,1L,0xA311D025L,0xC2F0412DL,3L},{0xD2364632L,0x84B7A889L,0xA311D025L,(-7L),0x5E513084L,0x90D9D2BFL,0x101CFB27L,0xEE9B094FL,1L},{0xEE9B094FL,0xC2F0412DL,0x84B7A889L,0L,0L,0x84B7A889L,0xC2F0412DL,0xEE9B094FL,0x90D9D2BFL},{(-1L),0x3B8CA3D7L,0x412EF4CCL,1L,3L,(-7L),0x90D9D2BFL,0xC2F0412DL,0x88F82DCFL},{(-1L),3L,0xEB4B0433L,0x73FEF656L,0x101CFB27L,0xC2F0412DL,0x03A697C5L,0x88F82DCFL,0x90D9D2BFL},{0x7369C74DL,1L,(-1L),(-1L),0x03A697C5L,0x03A697C5L,(-1L),(-1L),1L},{0x7369C74DL,0x90D9D2BFL,0x88F82DCFL,0x03A697C5L,0xC2F0412DL,0x101CFB27L,0x73FEF656L,0xEB4B0433L,3L},{(-1L),0x88F82DCFL,0xC2F0412DL,0x90D9D2BFL,(-7L),3L,1L,0x412EF4CCL,0x3B8CA3D7L},{(-1L),0x90D9D2BFL,0xEE9B094FL,0xC2F0412DL,0x84B7A889L,0L,0L,0x84B7A889L,0xC2F0412DL}}};
            uint64_t l_1454[1];
            int32_t l_1524[9][2][3] = {{{0xE9DF820CL,0x53A6080DL,0xCA5F7D71L},{0L,0xBEE0AF79L,0xBEE0AF79L}},{{0xE9DF820CL,(-1L),(-3L)},{0xF5544455L,9L,(-5L)}},{{0x7CF62068L,(-3L),0xE9DF820CL},{0x28B23645L,0x9E43BF98L,(-9L)}},{{(-3L),(-3L),(-1L)},{(-8L),9L,0x76B91DBFL}},{{0x53A6080DL,(-1L),0L},{(-5L),0xBEE0AF79L,9L}},{{0L,0x53A6080DL,0L},{0x5D9A8732L,0xFFE2BEF8L,0x76B91DBFL}},{{0x30B4F654L,(-1L),(-1L)},{0xBEE0AF79L,(-8L),(-9L)}},{{0x2DE80895L,0xE9DF820CL,0xE9DF820CL},{0xBEE0AF79L,0xC8DB303BL,(-5L)}},{{0x30B4F654L,1L,(-3L)},{0x5D9A8732L,(-5L),0xBEE0AF79L}}};
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 7; j++)
                    l_1413[i][j] = 0x1.32D5DCp-3;
            }
            for (i = 0; i < 1; i++)
                l_1454[i] = 18446744073709551615UL;
            (*g_1383) = g_1381;
            (*l_1384) = (-0x1.2p-1);
        }
        (*l_1569) &= (safe_lshift_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(g_1399[3], g_330)), (p_20 = (safe_sub_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(g_1472[1].f0, (((*g_639) = ((((safe_mul_func_int16_t_s_s(((safe_div_func_int64_t_s_s((l_1416 , (((l_1552 = (void*)0) != (((safe_lshift_func_uint16_t_u_u((g_755[0] = g_749[0]), 14)) , (((*l_1567) = (safe_add_func_int8_t_s_s((safe_add_func_uint64_t_u_u(l_1368, (!((*l_1566) = (safe_lshift_func_int16_t_s_s((((l_1564 != (void*)0) & g_563) | (*g_639)), 5)))))), p_20))) , p_20)) , (void*)0)) & l_1419[8][1][1])), 0x8EC9F95957D45699LL)) <= p_21), 0L)) != g_64) & l_1419[6][0][0]) || l_1476)) || 1L))), 0x72L)))));
    }
    l_1455[0] &= 3L;
    (*g_407) &= 0x29939A4BL;
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_26 g_8 g_50 g_64 g_7 g_58 g_95 g_150 g_159 g_150.f0 g_150.f2 g_63 g_92 g_206.f0 g_174 g_59 g_62 g_219 g_229 g_205 g_206 g_209 g_210 g_373 g_160 g_406 g_344 g_442 g_206.f2 g_407 g_379 g_330 g_511 g_343 g_563 g_498 g_588 g_678 g_443 g_679 g_530 g_680 g_375 g_639 g_749 g_376 g_755 g_766 g_714 g_1052 g_1055 g_751 g_905 g_1086 g_740 g_1164 g_1168 g_1181 g_1182 g_1082 g_1226 g_588.f0 g_1266 g_1472.f0
 * writes: g_32 g_50 g_59 g_64 g_92 g_95 g_174 g_176 g_26 g_219 g_205 g_330 g_343 g_344 g_374 g_379 g_206.f0 g_150.f0 g_407 g_443 g_63 g_498 g_210 g_511 g_530 g_639 g_679 g_714 g_741 g_749 g_751 g_755 g_756 g_1052 g_1055 g_1082 g_1164
 */
static const int32_t * func_22(int32_t  p_23, int32_t * p_24)
{ /* block id: 2 */
    int32_t * const l_1165 = (void*)0;
    uint8_t **l_1173 = (void*)0;
    uint16_t l_1191[4][9] = {{0x0491L,5UL,0x2C96L,0x2C96L,5UL,0x0491L,65533UL,0x0491L,5UL},{0x0491L,0xB31EL,0xB31EL,0x0491L,65535UL,5UL,65535UL,0x0491L,0xB31EL},{65535UL,65535UL,65533UL,5UL,0UL,5UL,65533UL,65535UL,65535UL},{0xB31EL,0x0491L,65535UL,5UL,65535UL,0x0491L,0xB31EL,0xB31EL,0x0491L}};
    const int32_t *l_1194 = &g_206[0][0].f0;
    int32_t ***l_1212 = &g_741;
    int8_t l_1238 = 0xD7L;
    uint32_t ***l_1243 = &g_211;
    int32_t l_1275 = 0xB906BDB2L;
    int32_t l_1281 = 0xEB14DA6EL;
    int32_t l_1282 = 0L;
    int32_t l_1300[9][3][5] = {{{(-1L),1L,0x5AE8F73AL,0x29E3185AL,0x480EBBBDL},{0xDD8B23A0L,0x2CE91B43L,(-5L),0xD40AE4D0L,8L},{0L,0x29E3185AL,0x29E3185AL,0L,1L}},{{8L,1L,0xAFABC01EL,1L,(-4L)},{0xCFC61E44L,1L,0xDC5D0CFFL,0xF14FDA08L,(-5L)},{0xAFABC01EL,4L,0x8CC9C999L,1L,1L}},{{(-4L),0x0E777A1CL,(-4L),1L,(-5L)},{4L,1L,0x2CE91B43L,0xDF3EB20EL,0xAFABC01EL},{0L,(-1L),0x093431A0L,0xDC5D0CFFL,4L}},{{0xDF3EB20EL,1L,0x2CE91B43L,0xAFABC01EL,0x2CE91B43L},{0x480EBBBDL,0x480EBBBDL,(-4L),0x29E3185AL,1L},{1L,0x9835DF30L,0x8CC9C999L,(-5L),1L}},{{0L,0xD8CD6B9BL,0x480EBBBDL,0x5AE8F73AL,0L},{0x408ED603L,0x9835DF30L,(-4L),(-4L),0x9835DF30L},{4L,0x480EBBBDL,0xDC5D0CFFL,0L,0xF14FDA08L}},{{0x11518F3FL,1L,0xD40AE4D0L,0x408ED603L,(-5L)},{0x29E3185AL,(-1L),0L,0xD8CD6B9BL,1L},{0x11518F3FL,1L,1L,1L,0x11518F3FL}},{{4L,0x0E777A1CL,1L,0L,0x480EBBBDL},{0x408ED603L,4L,0xAFABC01EL,8L,0x98413BDAL},{0L,0x093431A0L,0xCFC61E44L,0x0E777A1CL,0x480EBBBDL}},{{1L,8L,8L,1L,0x11518F3FL},{0x480EBBBDL,0xDC5D0CFFL,0L,0xF14FDA08L,1L},{0xDF3EB20EL,0x11518F3FL,0xDD8B23A0L,0x2CE91B43L,(-5L)}},{{0L,(-4L),(-1L),0xF14FDA08L,0xF14FDA08L},{4L,0xDF3EB20EL,4L,1L,0x9835DF30L},{(-4L),0x29E3185AL,1L,0x0E777A1CL,0L}}};
    uint32_t l_1305 = 4294967288UL;
    uint32_t * const l_1335 = (void*)0;
    uint32_t * const *l_1334[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_1359 = 0x6D1064E7L;
    int i, j, k;
    for (p_23 = 5; (p_23 >= 0); p_23 -= 1)
    { /* block id: 5 */
        uint32_t *l_31 = (void*)0;
        int32_t l_40 = 0x6BE82365L;
        const int32_t *l_1031 = &g_766;
        const int32_t **l_1030 = &l_1031;
        int32_t *l_1163 = &g_1164;
        float l_1237 = 0x6.04DAD7p+8;
        union U0 ***l_1241 = &g_376;
        uint8_t l_1255 = 0x21L;
        int32_t l_1283 = 0x3956462CL;
        int32_t l_1301 = (-7L);
        int32_t l_1303 = 0x7BBADC2EL;
        int32_t l_1352 = 9L;
        int32_t l_1353 = 0x05D404ABL;
        float l_1360 = 0x7.CAABE5p-15;
        (*l_1163) ^= func_27((++g_32[5][1]), p_23, ((*l_1030) = func_35(g_26[2][0][4], ((void*)0 != &g_8[1]), l_40, p_23)));
        (*g_407) = (*p_24);
        (*g_1168) = l_1165;
        if ((0xD9029580L || 0xDA5C8231L))
        { /* block id: 499 */
            uint8_t ** const l_1176[10] = {&g_639,&g_639,&g_639,&g_639,&g_639,&g_639,&g_639,&g_639,&g_639,&g_639};
            uint64_t *l_1180[8];
            uint64_t **l_1179 = &l_1180[5];
            int16_t *l_1183 = &g_343;
            int64_t *l_1184 = &g_330;
            uint32_t *l_1185 = &g_174;
            int32_t l_1186 = (-7L);
            int32_t **l_1187 = (void*)0;
            int32_t **l_1188 = &g_176;
            int32_t *l_1189 = &g_379;
            int32_t *l_1190[10] = {&g_206[0][0].f0,&g_1164,&g_1164,&g_206[0][0].f0,&g_1164,&g_1164,&g_206[0][0].f0,&g_1164,&g_1164,&g_206[0][0].f0};
            int i;
            for (i = 0; i < 8; i++)
                l_1180[i] = (void*)0;
            l_1186 = ((safe_mul_func_int8_t_s_s(p_23, (safe_mod_func_int16_t_s_s((4294967295UL & ((*l_1185) = ((l_1173 != (((l_1176[3] != l_1173) >= ((*l_1184) = (0xDAA934651D8E82DFLL > ((safe_sub_func_int16_t_s_s(((*l_1183) = (l_1179 == g_1181)), g_751[0][0][8])) > g_206[0][0].f0)))) , (void*)0)) == p_23))), p_23)))) > p_23);
            (*l_1030) = ((**g_1181) , ((*l_1188) = p_24));
            l_1191[2][3]--;
        }
        else
        { /* block id: 507 */
            (*g_407) = 0x87113298L;
        }
        for (g_511 = 5; (g_511 >= 0); g_511 -= 1)
        { /* block id: 512 */
            union U0 *****l_1208 = (void*)0;
            int32_t l_1219[7][3][7] = {{{0xBF83F2C6L,1L,(-1L),0x46FF4F43L,0xA484728BL,6L,0L},{1L,0xA484728BL,6L,0x5F918604L,2L,2L,0x138FE7B9L},{0L,0x22F17105L,0x79DC433DL,0x5F918604L,0L,0xC72A8D38L,5L}},{{0x50CD7588L,1L,0x46FF4F43L,0x46FF4F43L,1L,0x50CD7588L,0x22F17105L},{(-6L),0x5F918604L,(-3L),(-1L),8L,0x6D36E1ECL,0x5F918604L},{2L,0L,0x50CD7588L,0L,0L,6L,2L}},{{0xBF83F2C6L,0x5F918604L,(-1L),0L,0L,0L,(-1L)},{0L,1L,0x6D36E1ECL,0x138FE7B9L,0x5F918604L,0x6D36E1ECL,8L},{0xA484728BL,2L,0x6AFE514AL,0x5F2550ABL,0x72321B38L,0x6D36E1ECL,0xBF83F2C6L}},{{0x246459F2L,0xC72A8D38L,0x6D36E1ECL,0xBB1B432DL,0xBF83F2C6L,0xBB1B432DL,0x6D36E1ECL},{0x50CD7588L,0x50CD7588L,(-1L),0x22F17105L,0x4F63842DL,0xA484728BL,0L},{(-1L),0x6D36E1ECL,(-7L),0x79DC433DL,0L,8L,0x79DC433DL}},{{0x246459F2L,6L,0xBB1B432DL,0xF2EE120BL,0x4F63842DL,0x246459F2L,2L},{0x4F63842DL,0L,8L,0x5F2550ABL,0xBF83F2C6L,(-1L),0L},{0x46FF4F43L,0x6D36E1ECL,7L,0x46FF4F43L,0x72321B38L,0xBB1B432DL,0x4F63842DL}},{{8L,(-1L),7L,0x6AFE514AL,0x79DC433DL,(-3L),(-3L)},{(-1L),0x4F63842DL,8L,0x4F63842DL,(-1L),0xA484728BL,0xC72A8D38L},{(-1L),2L,0xBB1B432DL,0xC72A8D38L,0xF2EE120BL,6L,0xBF83F2C6L}},{{7L,0xBF83F2C6L,(-7L),0x6AFE514AL,0xBF83F2C6L,0xF2EE120BL,2L},{(-1L),0xC72A8D38L,(-1L),1L,6L,0xDCB071E5L,0x4F63842DL},{(-1L),(-3L),0x6D36E1ECL,0x22F17105L,0xF2EE120BL,8L,(-1L)}}};
            int32_t ***l_1233 = &g_741;
            int16_t l_1273 = 1L;
            uint32_t l_1284[7] = {0x4D80DD25L,0x4D80DD25L,0x4D80DD25L,0x4D80DD25L,0x4D80DD25L,0x4D80DD25L,0x4D80DD25L};
            union U0 *l_1291[7][6][4] = {{{&g_1226,&g_150,(void*)0,&g_150},{(void*)0,(void*)0,&g_150,&g_1226},{&g_206[0][0],&g_150,&g_206[0][0],&g_206[0][0]},{&g_1226,(void*)0,(void*)0,&g_206[0][0]},{&g_1226,(void*)0,&g_206[0][0],&g_206[0][0]},{&g_206[0][0],&g_206[0][0],&g_150,&g_206[0][0]}},{{(void*)0,&g_1226,(void*)0,&g_206[0][0]},{&g_1226,&g_206[0][0],&g_206[0][0],&g_206[0][0]},{&g_206[0][0],&g_150,(void*)0,(void*)0},{&g_1226,&g_206[0][0],&g_206[0][0],&g_206[0][0]},{&g_206[0][0],&g_206[0][0],(void*)0,&g_150},{(void*)0,&g_206[0][0],(void*)0,&g_206[0][0]}},{{&g_206[0][0],&g_206[0][0],&g_150,&g_206[0][0]},{&g_150,&g_206[0][0],(void*)0,&g_150},{(void*)0,&g_150,(void*)0,(void*)0},{&g_206[0][0],&g_206[0][0],&g_1226,&g_150},{&g_206[0][0],&g_206[0][0],(void*)0,&g_1226},{&g_206[0][0],&g_1226,(void*)0,(void*)0}},{{(void*)0,&g_1226,&g_150,&g_1226},{&g_1226,&g_206[0][0],&g_206[0][0],&g_150},{&g_150,&g_206[0][0],&g_150,(void*)0},{&g_150,&g_150,&g_206[0][0],&g_150},{&g_1226,&g_206[0][0],(void*)0,&g_206[0][0]},{(void*)0,&g_206[0][0],&g_1226,&g_206[0][0]}},{{(void*)0,(void*)0,&g_206[0][0],(void*)0},{&g_206[0][0],&g_206[0][0],&g_1226,(void*)0},{(void*)0,(void*)0,&g_206[0][0],&g_150},{&g_150,&g_206[0][0],(void*)0,(void*)0},{&g_206[0][0],&g_206[0][0],(void*)0,(void*)0},{&g_206[0][0],&g_206[0][0],&g_206[0][0],&g_1226}},{{&g_206[0][0],(void*)0,&g_150,(void*)0},{(void*)0,&g_206[0][0],&g_206[0][0],(void*)0},{&g_206[0][0],(void*)0,&g_206[0][0],&g_150},{(void*)0,&g_206[0][0],&g_150,&g_206[0][0]},{&g_206[0][0],&g_206[0][0],&g_206[0][0],&g_150},{&g_206[0][0],&g_150,(void*)0,&g_206[0][0]}},{{&g_206[0][0],&g_206[0][0],(void*)0,(void*)0},{&g_150,(void*)0,&g_206[0][0],&g_206[0][0]},{(void*)0,&g_150,&g_1226,&g_206[0][0]},{&g_206[0][0],&g_206[0][0],&g_206[0][0],&g_206[0][0]},{(void*)0,&g_1226,&g_1226,&g_206[0][0]},{(void*)0,(void*)0,(void*)0,&g_206[0][0]}}};
            float l_1302 = (-0x1.3p-1);
            int64_t l_1304 = 1L;
            uint32_t *l_1332 = &g_174;
            uint32_t **l_1331 = &l_1332;
            int8_t l_1337 = 0x5AL;
            uint32_t l_1349 = 0x9D22B2B5L;
            int i, j, k;
            for (g_1082 = 0; (g_1082 <= 0); g_1082 += 1)
            { /* block id: 515 */
                return l_1194;
            }
            for (g_344 = 0; (g_344 <= 5); g_344 += 1)
            { /* block id: 520 */
                int64_t *l_1205 = &g_330;
                union U0 *****l_1209 = (void*)0;
                uint16_t *l_1213 = (void*)0;
                uint16_t *l_1214 = &g_751[0][3][5];
                int32_t l_1215 = 0xEF58F5A2L;
                int32_t *l_1216 = (void*)0;
                int32_t *l_1217 = &g_379;
                uint64_t *l_1218[1][8][1] = {{{&g_1052[9]},{&g_95[0][4][0]},{&g_1052[9]},{&g_95[0][4][0]},{&g_1052[9]},{&g_95[0][4][0]},{&g_1052[9]},{&g_95[0][4][0]}}};
                union U0 ***l_1242 = &g_376;
                uint32_t ***l_1244 = &g_211;
                int32_t l_1252 = 2L;
                int32_t l_1298[4][10] = {{0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L},{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L},{1L,0xF4A3BBB6L,0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L,1L},{0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L,1L,0xF4A3BBB6L,0xF4A3BBB6L}};
                int32_t l_1299[10] = {1L,1L,(-6L),1L,1L,(-6L),1L,1L,(-6L),1L};
                int32_t l_1320 = (-1L);
                int i, j, k;
                if ((safe_lshift_func_int8_t_s_u(0xCCL, ((l_1219[6][2][0] = ((((*l_1217) = ((((*g_639) |= 0xCCL) , ((safe_mod_func_int16_t_s_s((safe_div_func_uint16_t_u_u((l_1215 ^= ((*l_1214) = (safe_div_func_int64_t_s_s(((*l_1205) ^= 0x8BB1BADFB4FF8659LL), (((*l_1163) = ((l_1208 == (l_1209 = l_1208)) == ((safe_mul_func_int8_t_s_s(((l_1212 == &l_1030) >= g_26[g_511][(p_23 + 1)][g_511]), 1UL)) > 0x2CL))) , g_26[p_23][g_511][g_511]))))), (-1L))), 0xDF73L)) ^ 0x2E17L)) > 0L)) & 0xBD746FFAL) & g_26[g_344][p_23][g_344])) > g_150.f2))))
                { /* block id: 529 */
                    uint32_t l_1225 = 0UL;
                    float *l_1234[10];
                    int32_t l_1235 = 0xB7DE879CL;
                    int32_t l_1236[9] = {6L,0x5002EB11L,6L,0x5002EB11L,6L,0x5002EB11L,6L,0x5002EB11L,6L};
                    uint32_t ****l_1245 = &l_1244;
                    int32_t l_1292 = 0x16CBA796L;
                    int32_t *l_1293 = &g_64;
                    int32_t *l_1294 = &g_59;
                    int32_t *l_1295 = &g_206[0][0].f0;
                    int32_t *l_1296 = &g_150.f0;
                    int32_t *l_1297[9][5][5] = {{{&l_1275,&l_1236[4],&l_1236[4],&l_1275,&l_1236[4]},{&l_1275,&l_1275,&l_1219[3][2][4],&l_1275,&l_1275},{&l_1236[4],&l_1275,&l_1236[4],&l_1236[4],&l_1275},{&l_1275,&l_1236[4],&l_1236[4],&l_1275,&l_1236[4]},{&l_1275,&l_1275,&l_1219[3][2][4],&l_1275,&l_1275}},{{&l_1236[4],&l_1275,&l_1236[4],&l_1236[4],&l_1275},{&l_1275,&l_1236[4],&l_1236[4],&l_1275,&l_1236[4]},{&l_1275,&l_1275,&l_1219[3][2][4],&l_1275,&l_1275},{&l_1236[4],&l_1275,&l_1236[4],&l_1236[4],&l_1275},{&l_1275,&l_1236[4],&l_1236[4],&l_1275,&l_1236[4]}},{{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]}},{{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]}},{{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]}},{{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]}},{{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]}},{{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]}},{{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]},{&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4]},{&l_1236[4],&l_1236[4],&l_1275,&l_1236[4],&l_1236[4]},{&l_1219[3][2][4],&l_1236[4],&l_1219[3][2][4],&l_1219[3][2][4],&l_1236[4]}}};
                    int i, j, k;
                    for (i = 0; i < 10; i++)
                        l_1234[i] = (void*)0;
                    (*l_1163) = (safe_lshift_func_uint16_t_u_s((((g_95[0][0][0] ^= ((((safe_sub_func_uint64_t_u_u((safe_unary_minus_func_uint8_t_u((&p_23 == (l_1225 , (g_1226 , &p_23))))), (*g_1182))) && (**g_406)) , ((safe_div_func_float_f_f(((l_1236[4] = ((*g_442) = ((safe_add_func_float_f_f((safe_sub_func_float_f_f(((l_1235 = ((((p_23 , &l_1030) == l_1233) != (*p_24)) , (*g_442))) >= p_23), p_23)), (*g_680))) < (*l_1217)))) != 0x0.4p+1), 0x1.308D4Dp-84)) , g_588.f0)) , l_1238)) != p_23) < 0UL), p_23));
                    if ((((safe_rshift_func_uint8_t_u_u(((l_1242 = l_1241) == l_1241), 1)) & ((0xDF9CL | ((l_1243 == ((*l_1245) = l_1244)) , (safe_sub_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u(((l_1252 || (*p_24)) & (*p_24)), 0)), (safe_div_func_int64_t_s_s((((((*g_160) , g_766) < 0xDCC8L) || 0x7145L) == p_23), g_344)))) <= (*p_24)), 0x194837BA30B1EFF9LL)))) <= g_150.f2)) >= 0xD3L))
                    { /* block id: 537 */
                        int16_t l_1260 = (-3L);
                        uint32_t *l_1265 = &g_174;
                        int32_t *l_1274 = &l_1215;
                        (*l_1274) &= (l_1255 == (safe_rshift_func_uint16_t_u_u((safe_mod_func_int8_t_s_s(l_1260, (((safe_add_func_int32_t_s_s(((*l_1163) = l_1260), ((*l_1265) = (safe_sub_func_uint8_t_u_u((*g_639), (*g_639)))))) || g_1266[5][5][1]) | ((((--(*g_160)) , (((((g_443[3] = (((g_1052[5] < (safe_rshift_func_uint8_t_u_s(0UL, 0))) <= (((safe_rshift_func_uint8_t_u_s((0x729612FDL < g_219), 6)) ^ (*g_639)) != p_23)) , 0x8.D8C52Cp-66)) , g_8[1]) < (*g_639)) && g_379) || l_1273)) == 0UL) , g_219)))), p_23)));
                    }
                    else
                    { /* block id: 543 */
                        int32_t *l_1276 = &g_59;
                        int32_t *l_1277 = (void*)0;
                        int32_t *l_1278 = &l_1215;
                        int32_t *l_1279 = &g_206[0][0].f0;
                        int32_t *l_1280[2];
                        union U0 *l_1287 = &g_206[0][0];
                        union U0 **l_1290[5] = {&g_1289[0][8],&g_1289[0][8],&g_1289[0][8],&g_1289[0][8],&g_1289[0][8]};
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1280[i] = &l_1236[4];
                        l_1284[6]++;
                        l_1291[5][2][3] = ((*g_376) = l_1287);
                    }
                    --l_1305;
                }
                else
                { /* block id: 549 */
                    int32_t l_1316 = (-5L);
                    float *l_1319 = &g_443[3];
                    int8_t *l_1327[1];
                    uint32_t ***l_1333 = &l_1331;
                    uint32_t * const **l_1336 = &l_1334[2];
                    int32_t l_1344 = (-1L);
                    int32_t l_1345 = 0L;
                    int32_t l_1346 = 0xA8E40F06L;
                    int32_t l_1347[6][6] = {{0x144385ECL,0L,0L,0x144385ECL,0x16ACDBD3L,(-3L)},{0L,(-3L),0x4205F0E7L,0x513663EBL,(-3L),0x144385ECL},{0x513663EBL,0x8AA9C211L,0x513663EBL,0x4205F0E7L,(-3L),0L},{0L,0x5D8EB1D0L,0x16ACDBD3L,0x513663EBL,0x513663EBL,0x16ACDBD3L},{0x51B4D6F3L,0x51B4D6F3L,0L,0x513663EBL,0x5D8EB1D0L,0x4205F0E7L},{0L,0L,0L,0x4205F0E7L,0L,0L}};
                    uint32_t l_1354[8][3][7] = {{{0xA655CF0DL,1UL,0UL,18446744073709551610UL,18446744073709551615UL,0UL,18446744073709551610UL},{18446744073709551614UL,0UL,0xB617C118L,0x90E90C16L,0xDEB60FBBL,0x1DBE9B4EL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551615UL,6UL,18446744073709551615UL,18446744073709551606UL,6UL}},{{18446744073709551614UL,0x4190D338L,0x5F23A57AL,0x90E90C16L,0x3A9200F7L,0xE9202FF4L,18446744073709551615UL},{0xA655CF0DL,0x4DF52748L,18446744073709551615UL,18446744073709551610UL,18446744073709551606UL,18446744073709551606UL,18446744073709551610UL},{0xB617C118L,0x4190D338L,0xB617C118L,9UL,0x3A9200F7L,0x1DBE9B4EL,0UL}},{{0xA655CF0DL,1UL,0UL,18446744073709551610UL,18446744073709551615UL,0UL,18446744073709551610UL},{18446744073709551614UL,0UL,0xB617C118L,0x90E90C16L,0xDEB60FBBL,0x1DBE9B4EL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551615UL,6UL,18446744073709551615UL,18446744073709551606UL,6UL}},{{18446744073709551614UL,0x4190D338L,0x5F23A57AL,0x90E90C16L,0x3A9200F7L,0xE9202FF4L,18446744073709551615UL},{0xA655CF0DL,0x4DF52748L,18446744073709551615UL,18446744073709551610UL,18446744073709551606UL,18446744073709551606UL,18446744073709551610UL},{0xB617C118L,0x4190D338L,0xB617C118L,9UL,0x3A9200F7L,0x1DBE9B4EL,0UL}},{{0xA655CF0DL,1UL,0UL,18446744073709551610UL,18446744073709551615UL,0UL,18446744073709551610UL},{18446744073709551614UL,0UL,0xB617C118L,0x90E90C16L,0xDEB60FBBL,0x1DBE9B4EL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551615UL,6UL,18446744073709551615UL,18446744073709551606UL,6UL}},{{18446744073709551614UL,0x4190D338L,0x5F23A57AL,0x90E90C16L,0x3A9200F7L,0xE9202FF4L,18446744073709551615UL},{0xA655CF0DL,0x4DF52748L,18446744073709551615UL,18446744073709551610UL,18446744073709551606UL,18446744073709551606UL,18446744073709551610UL},{0xB617C118L,0x4190D338L,0xB617C118L,9UL,0x3A9200F7L,0x1DBE9B4EL,0UL}},{{0xA655CF0DL,1UL,0UL,18446744073709551610UL,18446744073709551615UL,0UL,18446744073709551610UL},{18446744073709551614UL,0UL,0xB617C118L,0x90E90C16L,0xDEB60FBBL,0x1DBE9B4EL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551615UL,6UL,18446744073709551615UL,18446744073709551606UL,6UL}},{{18446744073709551614UL,0x4190D338L,0x5F23A57AL,0x90E90C16L,0x3A9200F7L,0xE9202FF4L,18446744073709551615UL},{0xA655CF0DL,0x4DF52748L,18446744073709551615UL,18446744073709551610UL,18446744073709551606UL,18446744073709551606UL,18446744073709551610UL},{0xB617C118L,0x4190D338L,0xB617C118L,9UL,0x296B72B7L,0x39900CF5L,0x5D387E22L}}};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1327[i] = &g_714[3][5][0];
                    (*l_1217) |= (1L > ((safe_lshift_func_uint8_t_u_u((0x8E08E86FL | ((safe_sub_func_int8_t_s_s(1L, (*g_639))) <= (safe_lshift_func_uint16_t_u_s(((*l_1214) = (0xDDA310265FA3818BLL ^ 0x6EE132F193FDE79BLL)), ((safe_mul_func_uint16_t_u_u(((l_1316 <= (safe_rshift_func_uint8_t_u_s((l_1319 != p_24), 6))) || 0xA8C431A5D04CEEC6LL), g_64)) != 8UL))))), 6)) | l_1320));
                    if (((safe_div_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((g_498[6][7][0] = (g_26[g_344][(g_344 + 1)][g_511] = (safe_sub_func_int64_t_s_s((*l_1217), 0xF027E36F9B357446LL)))), (safe_sub_func_int8_t_s_s((!(((*l_1333) = l_1331) != ((*l_1336) = l_1334[2]))), (g_714[3][5][0] = l_1337))))), g_63[1])) , 0x4143BE35L))
                    { /* block id: 557 */
                        int16_t l_1338 = 0xC749L;
                        int32_t *l_1339 = (void*)0;
                        int32_t *l_1340 = &g_1226.f0;
                        int32_t *l_1341 = &l_1283;
                        int32_t *l_1342 = &g_206[0][0].f0;
                        int32_t *l_1343[9] = {&l_1303,&l_1303,&l_1303,&l_1303,&l_1303,&l_1303,&l_1303,&l_1303,&l_1303};
                        int8_t l_1348 = 0x50L;
                        int i;
                        --l_1349;
                        ++l_1354[1][0][2];
                        return p_24;
                    }
                    else
                    { /* block id: 561 */
                        int32_t *l_1357 = &g_59;
                        int32_t *l_1358[4];
                        uint64_t l_1361 = 0x21D5D2E70BA0F9B9LL;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_1358[i] = &l_1301;
                        --l_1361;
                        return p_24;
                    }
                }
                return p_24;
            }
        }
    }
    return p_24;
}


/* ------------------------------------------ */
/* 
 * reads : g_59 g_1052 g_407 g_63 g_1055 g_442 g_639 g_92 g_206.f0 g_32 g_751 g_905 g_1086 g_379 g_330 g_58 g_755 g_714 g_740 g_95 g_511 g_26 g_7 g_8 g_343 g_498 g_563 g_174 g_406 g_50
 * writes: g_59 g_1052 g_1055 g_530 g_443 g_343 g_1082 g_379 g_498 g_330 g_206.f0 g_63 g_174 g_50
 */
static int32_t  func_27(uint32_t  p_28, int32_t  p_29, const int32_t * p_30)
{ /* block id: 436 */
    uint32_t l_1034 = 0UL;
    int32_t l_1042 = 1L;
    int32_t l_1043 = 0x2170BD0FL;
    int32_t l_1044 = 0x9242FA13L;
    int32_t l_1045 = (-8L);
    int32_t l_1046[7] = {0x02F048ADL,0x02F048ADL,0x02F048ADL,0x02F048ADL,0x02F048ADL,0x02F048ADL,0x02F048ADL};
    int8_t l_1049 = 9L;
    int16_t l_1050 = 0x0ED0L;
    int32_t l_1051 = 0L;
    uint32_t **l_1066 = &g_160;
    uint8_t l_1087 = 0x0DL;
    union U0 ***l_1098 = &g_376;
    int32_t ****l_1102 = &g_939;
    int32_t *****l_1101 = &l_1102;
    float *l_1145 = &g_530;
    float **l_1144 = &l_1145;
    float ***l_1143 = &l_1144;
    int i;
lbl_1135:
    l_1034 |= (safe_lshift_func_uint8_t_u_s(0x69L, 5));
    for (g_59 = 0; (g_59 == (-29)); g_59 = safe_sub_func_uint32_t_u_u(g_59, 1))
    { /* block id: 440 */
        int8_t l_1037[1];
        int32_t *l_1038 = (void*)0;
        int32_t *l_1039 = &g_206[0][0].f0;
        int32_t l_1040 = (-4L);
        int32_t *l_1041[7];
        int64_t l_1047 = 0x6A803F6A104519DALL;
        float l_1048 = 0x4.D000F0p+37;
        uint32_t ** const l_1068 = &g_160;
        int32_t ****l_1100[5] = {&g_939,&g_939,&g_939,&g_939,&g_939};
        int32_t *****l_1099 = &l_1100[1];
        int i;
        for (i = 0; i < 1; i++)
            l_1037[i] = 1L;
        for (i = 0; i < 7; i++)
            l_1041[i] = &g_64;
        g_1052[9]--;
        if ((*g_407))
        { /* block id: 442 */
            uint32_t l_1060 = 0xC865519CL;
            float *l_1061 = (void*)0;
            float *l_1062 = &g_530;
            float *l_1065[7] = {&l_1048,&l_1048,&l_1048,&l_1048,&l_1048,&l_1048,&l_1048};
            uint32_t **l_1067 = &g_160;
            int16_t *l_1079[8] = {&g_219,(void*)0,(void*)0,&g_219,(void*)0,(void*)0,&g_219,(void*)0};
            int32_t l_1083 = 0x579B6E40L;
            int i;
            ++g_1055;
            l_1083 = ((safe_sub_func_float_f_f(((*l_1062) = l_1060), (-0x1.Ep+1))) > (((safe_div_func_float_f_f((l_1042 = ((*g_442) = p_29)), (0x8.83850Ep+90 >= ((l_1067 = l_1066) == l_1068)))) < ((safe_div_func_float_f_f(((((g_1082 = ((safe_sub_func_uint32_t_u_u((safe_div_func_int16_t_s_s(((safe_mod_func_uint8_t_u_u((safe_add_func_int8_t_s_s(((((g_343 = l_1060) > ((safe_lshift_func_uint8_t_u_s((*g_639), 7)) == (0x24F15A40DDA64551LL | p_28))) , 0UL) | 0x73L), 0x21L)), (*l_1039))) >= g_32[5][1]), g_751[0][0][8])), l_1049)) , p_29)) >= p_29) >= p_29) != 0xF.C60350p+75), (-0x1.7p-1))) <= p_29)) == l_1044));
            l_1087 ^= (safe_add_func_uint8_t_u_u(((g_905 != g_1086) , p_29), 0xD2L));
            for (g_379 = 0; (g_379 <= (-24)); g_379--)
            { /* block id: 454 */
                int8_t *l_1103 = (void*)0;
                int8_t *l_1104 = &g_498[5][6][1];
                int32_t l_1132 = 0xC5BF682EL;
                int32_t l_1134 = 0x7415EB05L;
                const int32_t *l_1137 = &l_1045;
                const int32_t **l_1136 = &l_1137;
                l_1046[3] = (safe_mod_func_int16_t_s_s(((g_330 < ((safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((((((void*)0 != l_1098) && p_29) || g_58) , ((*l_1104) = (l_1099 != l_1101))), (safe_mul_func_uint16_t_u_u((((*g_639) , (g_1052[9] && g_755[0])) <= l_1083), g_714[3][5][0])))), g_740[0][6])), g_92)) >= 0x42L)) , p_29), g_755[0]));
                for (l_1051 = (-21); (l_1051 >= (-22)); l_1051 = safe_sub_func_int32_t_s_s(l_1051, 8))
                { /* block id: 459 */
                    uint8_t l_1109[10][10][2] = {{{0xBCL,0xAAL},{251UL,1UL},{255UL,251UL},{0x1CL,1UL},{255UL,9UL},{255UL,247UL},{9UL,0xAAL},{8UL,0xEAL},{4UL,0xB0L},{1UL,9UL}},{{1UL,247UL},{9UL,0xBDL},{5UL,1UL},{0xA5L,1UL},{0x16L,0xDFL},{0x10L,0x04L},{251UL,0x04L},{0x10L,0xDFL},{0x16L,1UL},{0xA5L,1UL}},{{5UL,0xBDL},{9UL,247UL},{1UL,9UL},{1UL,0xB0L},{4UL,0xEAL},{8UL,0xAAL},{9UL,247UL},{255UL,9UL},{255UL,1UL},{0x1CL,251UL}},{{255UL,1UL},{251UL,0xAAL},{0xBCL,0x3EL},{4UL,1UL},{0xB0L,9UL},{0xA2L,1UL},{9UL,1UL},{0xCAL,1UL},{0x89L,0xB0L},{0x16L,0UL}},{{1UL,0x04L},{9UL,0x80L},{0x10L,0UL},{2UL,1UL},{0x89L,1UL},{5UL,1UL},{251UL,247UL},{0xA2L,0x52L},{1UL,1UL},{255UL,0xEAL}},{{0xBCL,0x8CL},{9UL,1UL},{0x46L,9UL},{0x1CL,0xB0L},{0x1CL,9UL},{0x46L,1UL},{9UL,0x8CL},{0xBCL,0xEAL},{255UL,1UL},{1UL,0x52L}},{{0xA2L,247UL},{251UL,1UL},{5UL,1UL},{0x89L,1UL},{2UL,0UL},{0x10L,0x80L},{9UL,0x04L},{1UL,0UL},{0x16L,0xB0L},{0x89L,1UL}},{{0xCAL,1UL},{9UL,1UL},{0xA2L,9UL},{0xB0L,1UL},{4UL,0x3EL},{0xBCL,0xA5L},{0xD8L,2UL},{0x9DL,0xD8L},{0xDFL,251UL},{0xADL,1UL}},{{0x9DL,252UL},{1UL,0xA5L},{255UL,0xCAL},{0x3EL,0xF9L},{251UL,1UL},{0x77L,0xB0L},{1UL,8UL},{0xABL,8UL},{0x49L,251UL},{1UL,0UL}},{{0UL,255UL},{0xD8L,255UL},{0UL,0UL},{1UL,251UL},{0x49L,8UL},{0xABL,8UL},{1UL,0xB0L},{0x77L,1UL},{251UL,0xF9L},{0x3EL,0xCAL}}};
                    int64_t *l_1133 = &g_330;
                    int i, j, k;
                    l_1109[8][7][0]++;
                    l_1083 = (safe_mul_func_uint16_t_u_u(((((l_1134 = (safe_mul_func_int8_t_s_s((l_1109[4][4][0] , (safe_mul_func_int16_t_s_s((((*l_1133) |= ((0xD5737C888FE3E3B7LL > (safe_rshift_func_int16_t_s_s(l_1083, 8))) ^ ((safe_sub_func_uint32_t_u_u(g_95[0][2][1], (safe_sub_func_uint32_t_u_u(1UL, l_1109[2][0][1])))) , (safe_mul_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u((safe_mod_func_int32_t_s_s(((g_511 || (0xE717A73EE0020860LL >= p_29)) < p_28), l_1083)), 0x34136065CABF1454LL)), l_1132)), p_28))))) < l_1060), g_26[1][3][0]))), (*g_639)))) || (*g_407)) >= (*g_639)) < (*g_7)), l_1109[8][7][0]));
                }
                if (p_29)
                    goto lbl_1135;
                (*l_1136) = p_30;
            }
        }
        else
        { /* block id: 468 */
            uint32_t *l_1138 = &l_1034;
            uint32_t ** const *l_1147[1];
            uint32_t ** const **l_1148 = &l_1147[0];
            int32_t l_1149 = 0x10DABF3FL;
            int i;
            for (i = 0; i < 1; i++)
                l_1147[i] = &l_1068;
            (*g_407) = ((*l_1039) = (((((*l_1138) = g_343) <= p_29) < (&g_174 == ((safe_lshift_func_uint16_t_u_s((1L | 6UL), ((0x2CA18F13BA174FC4LL && (safe_sub_func_uint8_t_u_u((((l_1143 == (((~(p_28 > ((((*l_1148) = l_1147[0]) == &l_1068) > l_1149))) , g_498[8][5][1]) , &l_1144)) <= p_29) >= g_563), p_29))) , p_29))) , &g_174))) | 0xAEECL));
            for (g_174 = 0; (g_174 <= 55); g_174 = safe_add_func_int64_t_s_s(g_174, 1))
            { /* block id: 475 */
                uint32_t l_1154 = 1UL;
                uint64_t l_1157 = 18446744073709551612UL;
                int32_t l_1158 = (-1L);
                (**g_406) = ((safe_add_func_uint32_t_u_u((l_1154++), 4294967295UL)) && 4L);
                if (l_1154)
                { /* block id: 478 */
                    (**g_406) ^= ((255UL & 0UL) < l_1157);
                }
                else
                { /* block id: 480 */
                    uint32_t l_1160 = 4294967288UL;
                    l_1158 ^= (*g_7);
                    for (g_50 = 0; (g_50 <= 0); g_50 += 1)
                    { /* block id: 484 */
                        int32_t l_1159 = 3L;
                        int i;
                        if (l_1037[g_50])
                            break;
                        ++l_1160;
                    }
                    return p_29;
                }
            }
        }
        if (p_29)
            continue;
        return (*g_407);
    }
    return (**g_406);
}


/* ------------------------------------------ */
/* 
 * reads : g_8 g_26 g_50 g_64 g_7 g_58 g_95 g_150 g_159 g_150.f0 g_150.f2 g_63 g_92 g_206.f0 g_174 g_59 g_62 g_219 g_229 g_205 g_206 g_209 g_210 g_373 g_160 g_406 g_344 g_442 g_206.f2 g_407 g_379 g_330 g_511 g_343 g_563 g_498 g_588 g_678 g_443 g_679 g_530 g_680 g_375 g_639 g_749 g_376 g_755 g_766 g_714
 * writes: g_50 g_59 g_64 g_92 g_95 g_174 g_176 g_26 g_219 g_205 g_330 g_343 g_344 g_374 g_379 g_206.f0 g_150.f0 g_407 g_443 g_63 g_498 g_210 g_511 g_530 g_639 g_679 g_714 g_741 g_749 g_751 g_755 g_756
 */
static const int32_t * func_35(int64_t  p_36, int32_t  p_37, int8_t  p_38, uint16_t  p_39)
{ /* block id: 7 */
    uint32_t *l_49 = &g_50;
    int32_t *l_377 = (void*)0;
    int32_t *l_378[4][3] = {{&g_379,&g_379,&g_379},{(void*)0,&g_8[1],(void*)0},{&g_379,&g_379,&g_379},{(void*)0,&g_8[1],(void*)0}};
    float *l_531 = &g_443[2];
    uint32_t l_534 = 1UL;
    union U0 **l_540 = (void*)0;
    uint32_t *l_620 = &g_174;
    int16_t l_628 = 0x634EL;
    union U0 ** const *l_690 = &l_540;
    union U0 ** const **l_689[7][4] = {{&l_690,(void*)0,&l_690,&l_690},{&l_690,&l_690,&l_690,(void*)0},{(void*)0,&l_690,(void*)0,(void*)0},{&l_690,&l_690,(void*)0,&l_690},{&l_690,(void*)0,(void*)0,(void*)0},{&l_690,&l_690,(void*)0,(void*)0},{(void*)0,&l_690,&l_690,(void*)0}};
    const uint32_t *l_739 = &g_740[0][6];
    const uint32_t **l_738 = &l_739;
    const uint32_t ***l_737 = &l_738;
    const uint32_t ****l_736[4][10][6] = {{{&l_737,&l_737,&l_737,&l_737,&l_737,(void*)0},{&l_737,&l_737,&l_737,&l_737,(void*)0,&l_737},{&l_737,&l_737,&l_737,(void*)0,&l_737,&l_737},{(void*)0,(void*)0,&l_737,&l_737,&l_737,(void*)0},{&l_737,(void*)0,&l_737,&l_737,(void*)0,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737}},{{&l_737,&l_737,&l_737,&l_737,(void*)0,(void*)0},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,(void*)0,&l_737,&l_737,&l_737,&l_737},{(void*)0,&l_737,&l_737,&l_737,&l_737,(void*)0},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{(void*)0,&l_737,&l_737,&l_737,&l_737,&l_737},{(void*)0,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,(void*)0,(void*)0,&l_737,&l_737},{(void*)0,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,(void*)0}},{{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,(void*)0,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,(void*)0,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,(void*)0},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737}},{{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,(void*)0,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{(void*)0,(void*)0,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,(void*)0},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,&l_737,&l_737,&l_737},{&l_737,&l_737,&l_737,(void*)0,&l_737,&l_737}}};
    const int32_t *l_765[2];
    const int32_t **l_764 = &l_765[0];
    const int32_t ***l_763 = &l_764;
    int16_t l_782 = 0L;
    float * const *l_797 = &l_531;
    float * const * const *l_796 = &l_797;
    uint16_t l_1028 = 0x45A7L;
    const int32_t *l_1029[9];
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_765[i] = &g_766;
    for (i = 0; i < 9; i++)
        l_1029[i] = &g_766;
lbl_653:
    (*l_531) = (func_41(func_46(&g_8[1], l_49), l_49, ((g_379 = (*g_7)) ^ (safe_rshift_func_uint8_t_u_u(0UL, ((safe_sub_func_uint32_t_u_u(g_8[5], ((safe_unary_minus_func_uint64_t_u(0x848DC905E4684376LL)) >= p_39))) ^ 8L)))), p_39) == 0x0.9A0926p+19);
    for (g_511 = (-23); (g_511 != 17); g_511 = safe_add_func_uint32_t_u_u(g_511, 8))
    { /* block id: 195 */
        uint16_t l_537 = 0x209FL;
        union U0 **l_541 = (void*)0;
        int32_t l_546 = (-1L);
        union U0 *** const *l_560 = (void*)0;
        union U0 **** const l_562 = &g_375;
        int32_t l_573 = (-1L);
        int32_t l_574[10] = {3L,3L,3L,3L,3L,3L,3L,3L,3L,3L};
        int8_t l_603 = 0L;
        uint16_t l_608 = 0xFB36L;
        int32_t l_647 = 0xB0753075L;
        int i;
        if (l_534)
            break;
        g_64 ^= (safe_div_func_uint16_t_u_u((g_95[0][4][1] != l_537), (safe_rshift_func_uint8_t_u_s((0UL ^ g_150.f2), p_39))));
        l_541 = l_540;
        for (g_64 = 16; (g_64 <= 2); g_64 = safe_sub_func_uint16_t_u_u(g_64, 1))
        { /* block id: 201 */
            int8_t l_553 = 0L;
            int8_t *l_555[10];
            int8_t **l_554 = &l_555[9];
            union U0 *** const **l_561 = &l_560;
            int32_t l_577 = 3L;
            int32_t l_578 = 1L;
            int32_t l_596 = 0x48DE643FL;
            int32_t l_601 = (-8L);
            int32_t l_604 = 1L;
            float l_605 = 0xB.F28619p-33;
            int32_t l_606 = 0xA6A75746L;
            int32_t l_607[1][7][1] = {{{(-6L)},{1L},{(-6L)},{(-6L)},{1L},{(-6L)},{(-6L)}}};
            uint64_t l_656 = 0xA4D6C4803CC9B171LL;
            int64_t l_661[2][6] = {{(-8L),0x00A146998C722FCALL,(-8L),0x00A146998C722FCALL,(-8L),0x00A146998C722FCALL},{(-8L),0x00A146998C722FCALL,(-8L),0x00A146998C722FCALL,(-8L),0x00A146998C722FCALL}};
            uint32_t l_664 = 0x52A11FE4L;
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_555[i] = &g_26[3][6][3];
            if ((((safe_mul_func_int16_t_s_s((((l_546 = p_37) >= (safe_rshift_func_uint16_t_u_u((safe_add_func_int8_t_s_s(((safe_lshift_func_uint8_t_u_s(((g_219 , ((l_553 >= 5L) & (((((*l_554) = &p_38) == (void*)0) | (((safe_mul_func_int8_t_s_s(((((safe_mod_func_int8_t_s_s((0x7118666125E4D65CLL < (0x4D643D6F182F2CC5LL > (((*l_561) = l_560) != l_562))), 1UL)) , l_537) == 0L) <= 0x1AD6L), l_537)) > g_563) || g_498[0][0][2])) < g_498[5][6][1]))) != g_92), 3)) , l_553), p_39)), 0))) || g_206[0][0].f2), g_92)) ^ l_537) & l_553))
            { /* block id: 205 */
                uint64_t l_564 = 0xE2D4773189BFA013LL;
                int32_t *l_567 = &g_64;
                int32_t l_571[7][6][6] = {{{2L,8L,0x4222B34CL,0x23B5B305L,0xC4AF2616L,1L},{0xCF334D77L,0x6135A41AL,0L,0x6135A41AL,0xCF334D77L,1L},{0x23B5B305L,0x7DA76AF3L,1L,1L,(-1L),5L},{0x2483151DL,0x6E75292EL,0xA2331033L,0x7DA76AF3L,8L,5L},{1L,1L,0L,(-1L),(-6L),(-2L)},{1L,0xC1FD13DDL,8L,(-7L),2L,0x0D6B95CCL}},{{0x7DA76AF3L,1L,9L,0x2BFDE44EL,0L,1L},{0x2483151DL,0x7DA76AF3L,0x9E37ED42L,0xCBCAA734L,0x910B214CL,1L},{1L,8L,(-6L),0xAE3B5AC5L,(-7L),0x2BFDE44EL},{8L,1L,0x1917BBBFL,0x1917BBBFL,1L,8L},{0x9E37ED42L,0xDF1C3F2AL,0xC1FD13DDL,0x0D6B95CCL,0xC12C7BB8L,0x2483151DL},{0xF343B5F4L,9L,1L,(-1L),0xC4AF2616L,0L}},{{0xF343B5F4L,0x910B214CL,(-1L),0x0D6B95CCL,2L,(-7L)},{0x9E37ED42L,0xC12C7BB8L,0x6F6F6073L,0x1917BBBFL,(-1L),1L},{8L,0x9E37ED42L,0xCF334D77L,0xAE3B5AC5L,1L,1L},{1L,0x4222B34CL,0xA2331033L,0xCBCAA734L,1L,0x1917BBBFL},{0x2483151DL,1L,0xFFE094D9L,0x2BFDE44EL,0x5B350D0EL,0x40FA2D91L},{0x7DA76AF3L,0xAE3B5AC5L,0xC1FD13DDL,(-7L),0xC1FD13DDL,0xAE3B5AC5L}},{{1L,(-1L),(-2L),(-1L),0xA2331033L,0xFFE094D9L},{1L,0x9339EB70L,0xC4AF2616L,1L,2L,0x5B350D0EL},{(-1L),0x9339EB70L,0x7DA76AF3L,1L,0xA2331033L,1L},{0x4222B34CL,(-1L),1L,0x2483151DL,0xC1FD13DDL,1L},{1L,0xAE3B5AC5L,0L,0x4222B34CL,0x5B350D0EL,1L},{0xCBCAA734L,1L,0x2BFDE44EL,(-2L),1L,0x4222B34CL}},{{0xCF334D77L,0x4222B34CL,0xC1FD13DDL,0x5B350D0EL,1L,0xCBCAA734L},{0xC1FD13DDL,0x9E37ED42L,0L,(-1L),(-1L),0L},{0xC12C7BB8L,0xC12C7BB8L,1L,1L,2L,0x39FA1EDFL},{9L,0x910B214CL,(-1L),0L,0xC4AF2616L,1L},{0xAE3B5AC5L,9L,(-1L),0xDF1C3F2AL,0xC12C7BB8L,0x39FA1EDFL},{1L,0xDF1C3F2AL,1L,0x40FA2D91L,1L,0L}},{{0x40FA2D91L,1L,0L,0L,(-7L),0xCBCAA734L},{0x6F6F6073L,8L,0xC1FD13DDL,0x39FA1EDFL,0x910B214CL,0x4222B34CL},{0x910B214CL,0x7DA76AF3L,0x2BFDE44EL,(-1L),0L,1L},{0x9339EB70L,1L,0L,1L,2L,1L},{1L,0xC1FD13DDL,1L,0xFFE094D9L,(-6L),1L},{0xDF1C3F2AL,1L,0x7DA76AF3L,8L,0x6E75292EL,0x2BFDE44EL}},{{(-2L),0x9E37ED42L,0xF343B5F4L,0xCF334D77L,0L,(-6L)},{1L,0L,(-1L),(-6L),(-2L),0x7DA76AF3L},{0x0D6B95CCL,(-1L),0x23B5B305L,(-2L),0x46351CE9L,0x9E37ED42L},{0x2CF89BD3L,1L,(-6L),0x9339EB70L,1L,1L},{2L,0x46351CE9L,0xC1FD13DDL,0L,0xCBCAA734L,1L},{0x5B350D0EL,0x6E75292EL,1L,8L,0x910B214CL,0x910B214CL}}};
                int i, j, k;
                if ((**g_406))
                    break;
                l_564++;
                if (p_36)
                { /* block id: 208 */
                    int32_t **l_568 = (void*)0;
                    int32_t **l_569 = &l_378[3][2];
                    int32_t **l_570 = &g_176;
                    int32_t l_572 = 0xF0B637EFL;
                    int32_t l_576 = 0x715F2F05L;
                    uint32_t l_579 = 0x71C4078FL;
                    uint32_t * const *l_589[7][2] = {{&l_49,&l_49},{&l_49,(void*)0},{&g_160,&g_160},{(void*)0,&g_160},{&g_160,(void*)0},{&l_49,&l_49},{&l_49,(void*)0}};
                    int i, j;
                    (*l_570) = ((*l_569) = l_567);
                    --l_579;
                    if (p_37)
                        continue;
                    l_574[5] |= (safe_sub_func_int8_t_s_s(((-6L) == (safe_rshift_func_int16_t_s_u((safe_mod_func_int8_t_s_s((g_588 , ((void*)0 == &l_49)), p_39)), 15))), (((l_573 = p_37) > ((l_589[5][1] == (void*)0) >= (-1L))) ^ 0xB931A71AL)));
                }
                else
                { /* block id: 215 */
                    return l_377;
                }
                for (p_37 = 29; (p_37 != (-26)); p_37--)
                { /* block id: 220 */
                    return l_49;
                }
            }
            else
            { /* block id: 223 */
                int32_t l_593 = 7L;
                int32_t l_594 = 0x6D202D93L;
                int32_t l_595 = (-1L);
                int32_t l_597 = 3L;
                int16_t l_598 = 0xA83DL;
                int32_t l_599 = 1L;
                int32_t l_600[8];
                int64_t l_602 = 0x9F481947DF420BCDLL;
                uint32_t **l_617 = (void*)0;
                uint32_t *l_619 = &l_534;
                uint32_t **l_618[2][7][3] = {{{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619}},{{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619},{&l_619,&l_619,&l_619}}};
                int16_t *l_626 = (void*)0;
                int16_t *l_627 = &l_598;
                int32_t l_654 = 1L;
                uint16_t l_672 = 0xA955L;
                int i, j, k;
                for (i = 0; i < 8; i++)
                    l_600[i] = 0x356115D8L;
                --l_608;
                l_599 ^= (((safe_lshift_func_int8_t_s_u((g_64 || (safe_mul_func_uint16_t_u_u(g_563, ((p_37 >= ((l_620 = &l_534) != &l_534)) ^ (((**l_554) = (((safe_mul_func_int16_t_s_s(((*l_627) = (safe_mul_func_uint64_t_u_u(0x09A29872115D6B1DLL, (!((l_574[0] != (-10L)) < (0L < (-2L))))))), g_330)) , 0UL) & p_38)) < g_150.f0))))), g_95[0][8][1])) & l_628) == 0xC0L);
                if ((p_37 ^ p_37))
                { /* block id: 229 */
                    union U0 ****l_640 = &g_375;
                    int32_t l_642 = 0x34EB9836L;
                    int32_t l_643 = 0L;
                    int32_t l_648 = (-9L);
                    int32_t l_649 = 9L;
                    uint32_t l_650[3][2][8] = {{{0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL,0xB510D235L},{0x648B647FL,0x648B647FL,4294967294UL,0x648B647FL,0x648B647FL,4294967294UL,0x648B647FL,0x648B647FL}},{{0xB510D235L,0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL},{0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL,0xB510D235L}},{{0x648B647FL,0x648B647FL,4294967294UL,0x648B647FL,0x648B647FL,4294967294UL,0x648B647FL,0x648B647FL},{0xB510D235L,0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL,0xB510D235L,0xB510D235L,0x648B647FL}}};
                    int i, j, k;
                    if ((((-4L) <= p_39) , (safe_rshift_func_uint16_t_u_s(((safe_add_func_int16_t_s_s((l_604 , (safe_mul_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u((l_600[7] = 2UL), ((safe_sub_func_uint32_t_u_u(((g_639 = &g_92) != l_555[9]), g_206[0][0].f2)) != ((*l_627) = ((void*)0 != l_640))))), 255UL))), 0x16FCL)) <= p_37), 0))))
                    { /* block id: 233 */
                        float l_641 = 0xA.608225p-28;
                        int32_t l_644 = 0L;
                        int32_t l_645 = (-7L);
                        int32_t l_646 = 0x8CB3FF9EL;
                        int32_t l_655 = 0x6440AD55L;
                        l_650[0][0][1]++;
                        if (p_39)
                            goto lbl_653;
                        ++l_656;
                        return l_378[3][2];
                    }
                    else
                    { /* block id: 238 */
                        float l_659[4];
                        int32_t l_660 = 0x2B59B06EL;
                        int32_t l_662 = 1L;
                        int32_t l_663 = (-6L);
                        int i;
                        for (i = 0; i < 4; i++)
                            l_659[i] = 0xA.A95683p-93;
                        ++l_664;
                    }
                    l_600[3] = (safe_add_func_int8_t_s_s(0xA0L, 0x84L));
                    for (p_37 = (-8); (p_37 == (-8)); p_37 = safe_add_func_int64_t_s_s(p_37, 8))
                    { /* block id: 244 */
                        (*g_407) ^= p_38;
                        (*l_531) = 0x1.2p-1;
                    }
                }
                else
                { /* block id: 248 */
                    l_599 ^= (!0x43CDL);
                }
                l_672++;
            }
        }
    }
    for (p_39 = 0; (p_39 <= 5); p_39 += 1)
    { /* block id: 257 */
        const float *l_677 = &g_530;
        const float **l_676 = &l_677;
        union U0 ****l_687 = &g_375;
        int32_t l_710 = 6L;
        const int32_t *l_761 = &g_379;
        const int32_t **l_760 = &l_761;
        const int32_t ***l_759 = &l_760;
        if (p_36)
            break;
        if (p_38)
            break;
        for (g_50 = 0; (g_50 <= 5); g_50 += 1)
        { /* block id: 262 */
            const int32_t *l_675 = &g_59;
            return l_675;
        }
        for (g_219 = 1; (g_219 <= 5); g_219 += 1)
        { /* block id: 267 */
            union U0 *****l_688 = &l_687;
            int32_t l_711[10][9][2] = {{{0xDCB74D84L,0xF4E59F3BL},{5L,(-1L)},{(-1L),0xFF8CE0E7L},{(-1L),0x96229F45L},{0xD414EFDAL,0x96229F45L},{(-1L),0xFF8CE0E7L},{(-1L),(-1L)},{5L,0xF4E59F3BL},{0xDCB74D84L,0x0B6CB0A9L}},{{(-5L),0x54D0E48AL},{(-1L),0x0B6CB0A9L},{0L,0xF4E59F3BL},{0x3458302EL,(-1L)},{0xA4540C12L,0xFF8CE0E7L},{0L,0x96229F45L},{5L,0x96229F45L},{0L,0xFF8CE0E7L},{0xA4540C12L,(-1L)}},{{0x3458302EL,0xF4E59F3BL},{0L,0x0B6CB0A9L},{(-1L),0x54D0E48AL},{(-5L),0x0B6CB0A9L},{0xDCB74D84L,0xF4E59F3BL},{5L,(-1L)},{(-1L),0xF4E59F3BL},{5L,0L},{5L,0L}},{{5L,0xF4E59F3BL},{(-1L),1L},{(-1L),0x96229F45L},{(-1L),0x54D0E48AL},{0L,0L},{0x2B2329A9L,0x54D0E48AL},{0xA4540C12L,0x96229F45L},{(-9L),1L},{(-5L),0xF4E59F3BL}},{{0xD414EFDAL,0L},{0x8736853AL,0L},{0xD414EFDAL,0xF4E59F3BL},{(-5L),1L},{(-9L),0x96229F45L},{0xA4540C12L,0x54D0E48AL},{0x2B2329A9L,0L},{0L,0x54D0E48AL},{(-1L),0x96229F45L}},{{(-1L),1L},{(-1L),0xF4E59F3BL},{5L,0L},{5L,0L},{5L,0xF4E59F3BL},{(-1L),1L},{(-1L),0x96229F45L},{(-1L),0x54D0E48AL},{0L,0L}},{{0x2B2329A9L,0x54D0E48AL},{0xA4540C12L,0x96229F45L},{(-9L),1L},{(-5L),0xF4E59F3BL},{0xD414EFDAL,0L},{0x8736853AL,0L},{0xD414EFDAL,0xF4E59F3BL},{(-5L),1L},{(-9L),0x96229F45L}},{{0xA4540C12L,0x54D0E48AL},{0x2B2329A9L,0L},{0L,0x54D0E48AL},{(-1L),0x96229F45L},{(-1L),1L},{(-1L),0xF4E59F3BL},{5L,0L},{5L,0L},{5L,0xF4E59F3BL}},{{(-1L),1L},{(-1L),0x96229F45L},{(-1L),0x54D0E48AL},{0L,0L},{0x2B2329A9L,0x54D0E48AL},{0xA4540C12L,0x96229F45L},{(-9L),1L},{(-5L),0xF4E59F3BL},{0xD414EFDAL,0L}},{{0x8736853AL,0L},{0xD414EFDAL,0xF4E59F3BL},{(-5L),1L},{(-9L),0x96229F45L},{0xA4540C12L,0x54D0E48AL},{0x2B2329A9L,0L},{0L,0x54D0E48AL},{(-1L),0x96229F45L},{(-1L),1L}}};
            const int32_t ***l_769 = (void*)0;
            uint32_t **l_772 = (void*)0;
            int32_t l_815 = (-3L);
            int32_t **** const *l_822 = (void*)0;
            int i, j, k;
            (*g_678) = l_676;
            if (((((void*)0 == l_677) <= 0xF959L) | ((safe_div_func_uint64_t_u_u((p_39 & ((safe_div_func_uint16_t_u_u(p_36, (p_38 , ((safe_rshift_func_uint8_t_u_u(7UL, ((((*l_688) = l_687) != ((**g_229) , l_689[5][3])) | 7L))) , 8L)))) | 0L)), 4UL)) <= (-9L))))
            { /* block id: 270 */
                float l_691 = 0x1.5p-1;
                union U0 ***l_722 = &g_376;
                uint32_t **l_731 = &g_160;
                const uint32_t ***l_733[5];
                const uint32_t ****l_732 = &l_733[3];
                int32_t l_743 = 0xEB7AAB78L;
                int32_t *l_748 = &g_511;
                uint16_t *l_750 = &g_751[0][0][8];
                uint16_t *l_754 = &g_755[0];
                const int32_t ****l_762 = &l_759;
                const int32_t ***l_768 = (void*)0;
                const int32_t ****l_767[3][6][2] = {{{&l_768,&l_768},{(void*)0,&l_768},{&l_768,&l_768},{(void*)0,&l_768},{&l_768,&l_768},{(void*)0,&l_768}},{{&l_768,&l_768},{(void*)0,&l_768},{&l_768,&l_768},{(void*)0,&l_768},{&l_768,&l_768},{(void*)0,&l_768}},{{&l_768,&l_768},{(void*)0,&l_768},{&l_768,&l_768},{(void*)0,&l_768},{&l_768,&l_768},{(void*)0,&l_768}}};
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_733[i] = (void*)0;
                for (g_379 = 5; (g_379 >= 1); g_379 -= 1)
                { /* block id: 273 */
                    uint32_t l_692 = 0x835B1242L;
                    int32_t *l_699[9][7] = {{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{(void*)0,&g_511,&g_511,&g_511,(void*)0,&g_511,(void*)0},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511},{&g_511,&g_511,&g_511,&g_511,&g_511,&g_511,&g_511}};
                    float *l_712[5][1][10] = {{{&g_530,&l_691,&l_691,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&l_691,&l_691,&g_530,&g_530,&g_530}},{{&l_691,&l_691,&l_691,&g_530,(void*)0,&l_691,(void*)0,&g_530,&l_691,&l_691}},{{(void*)0,&l_691,&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530,&l_691,(void*)0}},{{&g_530,&l_691,&l_691,&g_530,&l_691,&g_530,&l_691,&l_691,&g_530,&g_530}}};
                    int32_t l_713 = 0L;
                    uint64_t *l_715 = &g_95[0][1][0];
                    int32_t l_723[4];
                    int32_t ***l_724 = (void*)0;
                    int32_t **l_726 = &l_377;
                    int32_t ***l_725 = &l_726;
                    const uint32_t *****l_734 = (void*)0;
                    const uint32_t *****l_735 = (void*)0;
                    int32_t **l_742 = &l_378[3][2];
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_723[i] = 0x5AA3FEC9L;
                    l_692--;
                    g_714[3][5][0] = ((g_530 = (((((g_26[p_39][g_219][g_219] >= (((((safe_div_func_float_f_f((p_38 == (l_713 = (((0x1.0p+1 < (safe_sub_func_float_f_f(p_36, ((*l_531) = ((((g_511 = g_206[0][0].f0) , ((((((safe_sub_func_float_f_f(((safe_div_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f((*g_442), (safe_sub_func_float_f_f(p_39, 0xD.27A457p-77)))), ((**g_679) < l_710))), 0xD.356E54p+32)) >= 0xA.31470Dp-94), l_711[1][3][1])) == (**g_679)) < 0xC.087F01p-93) > (**g_679)) == l_711[6][4][0]) > p_37)) <= 0xA.521F16p-70) != l_711[6][3][1]))))) > p_38) != (*g_680)))), (*g_680))) == p_38) < 0xC.444395p-62) < (**g_679)) != (**g_679))) <= 0x0.8p+1) != 0x9.7DBC3Ap-13) < (*g_680)) >= p_39)) < p_38);
                    l_723[1] |= ((((g_219 && p_36) | (++(*l_715))) != p_39) > (p_38 ^ ((((safe_rshift_func_uint16_t_u_u((((((safe_mod_func_uint64_t_u_u(((l_713 <= ((((*l_687) != l_722) , p_37) | ((g_50 , (**g_406)) && l_692))) < 18446744073709551611UL), 0x7B4C5C54F07967E5LL)) == 4294967295UL) ^ l_711[7][5][1]) , p_36) , l_711[1][3][1]), 3)) == p_36) & p_37) || (-1L))));
                    l_711[1][3][1] = (((*l_742) = (((((*l_725) = &g_176) != (g_741 = ((l_710 = ((safe_mul_func_float_f_f(p_38, ((*l_531) = ((0x0.34CC20p+81 > ((&g_679 != ((((*g_407) >= ((void*)0 != l_731)) , ((l_736[0][5][2] = l_732) != &l_737)) , (void*)0)) == p_37)) >= (**g_679))))) >= p_39)) , (void*)0))) && l_710) , l_378[1][2])) == (void*)0);
                }
                (*l_531) = l_743;
                if ((safe_mul_func_int8_t_s_s((g_8[4] || ((safe_mod_func_uint16_t_u_u((((((*l_748) = 0x8B532D64L) , (*g_639)) == (((*l_750) = (g_749[0] ^= g_498[5][6][1])) ^ (((*l_754) = (safe_rshift_func_int8_t_s_u(l_743, 3))) >= ((&l_736[2][0][4] == (g_756 = (void*)0)) & (~((l_769 = (l_763 = ((*l_762) = l_759))) != (g_206[0][0].f2 , (void*)0))))))) ^ p_38), 0xF322L)) & 0x8610L)), 0x1EL)))
                { /* block id: 299 */
                    uint64_t *l_773 = &g_344;
                    uint32_t **l_774 = &g_160;
                    int32_t l_775 = (-7L);
                    int32_t l_776[7][6][6] = {{{6L,0x4566B8F7L,(-1L),(-5L),6L,0xD0D301EFL},{0x4566B8F7L,(-5L),1L,0xD0D301EFL,6L,(-5L)},{0x5F02D1EAL,0x4566B8F7L,0x15FE7E41L,(-5L),(-3L),0x4566B8F7L},{(-3L),0xC06A2D68L,1L,(-1L),(-1L),1L},{(-3L),(-3L),(-1L),(-5L),(-9L),6L},{0x5F02D1EAL,0x15FE7E41L,0xC06A2D68L,0xD0D301EFL,(-1L),(-1L)}},{{0x4566B8F7L,0x5F02D1EAL,0xC06A2D68L,(-5L),(-3L),6L},{6L,(-5L),(-1L),0x4566B8F7L,6L,1L},{0x4566B8F7L,6L,1L,1L,6L,0x4566B8F7L},{0x5F02D1EAL,(-5L),0x15FE7E41L,6L,(-3L),(-5L)},{(-3L),0x5F02D1EAL,1L,(-1L),(-1L),0xD0D301EFL},{(-3L),0x15FE7E41L,(-1L),6L,(-9L),(-5L)}},{{0x5F02D1EAL,(-3L),0xC06A2D68L,1L,(-1L),(-1L)},{0x4566B8F7L,0xC06A2D68L,0xC06A2D68L,0x4566B8F7L,(-3L),(-5L)},{6L,0x4566B8F7L,(-1L),(-5L),6L,0xD0D301EFL},{0x4566B8F7L,(-5L),1L,0xD0D301EFL,6L,(-5L)},{0x5F02D1EAL,0x4566B8F7L,0x15FE7E41L,(-5L),(-3L),0x5F02D1EAL},{(-9L),(-5L),(-1L),(-3L),(-3L),(-1L)}},{{(-9L),(-9L),0x15FE7E41L,1L,0x4566B8F7L,0xD0D301EFL},{6L,1L,(-5L),(-1L),(-3L),0x15FE7E41L},{0x5F02D1EAL,6L,(-5L),0xC06A2D68L,(-9L),0xD0D301EFL},{0xD0D301EFL,0xC06A2D68L,0x15FE7E41L,0x5F02D1EAL,0xD0D301EFL,(-1L)},{0x5F02D1EAL,0xD0D301EFL,(-1L),(-1L),0xD0D301EFL,0x5F02D1EAL},{6L,0xC06A2D68L,1L,0xD0D301EFL,(-9L),0xC06A2D68L}},{{(-9L),6L,(-1L),0x15FE7E41L,(-3L),(-1L)},{(-9L),1L,0x15FE7E41L,0xD0D301EFL,0x4566B8F7L,1L},{6L,(-9L),(-5L),(-1L),(-3L),(-3L)},{0x5F02D1EAL,(-5L),(-5L),0x5F02D1EAL,(-9L),1L},{0xD0D301EFL,0x5F02D1EAL,0x15FE7E41L,0xC06A2D68L,0xD0D301EFL,(-1L)},{0x5F02D1EAL,1L,(-1L),(-1L),0xD0D301EFL,0xC06A2D68L}},{{6L,0x5F02D1EAL,1L,1L,(-9L),0x5F02D1EAL},{(-9L),(-5L),(-1L),(-3L),(-3L),(-1L)},{(-9L),(-9L),0x15FE7E41L,1L,0x4566B8F7L,0xD0D301EFL},{6L,1L,(-5L),(-1L),(-3L),0x15FE7E41L},{0x5F02D1EAL,6L,(-5L),0xC06A2D68L,(-9L),0xD0D301EFL},{0xD0D301EFL,0xC06A2D68L,0x15FE7E41L,0x5F02D1EAL,0xD0D301EFL,(-1L)}},{{0x5F02D1EAL,0xD0D301EFL,(-1L),(-1L),0xD0D301EFL,0x5F02D1EAL},{6L,0xC06A2D68L,1L,0xD0D301EFL,(-9L),0xC06A2D68L},{(-9L),6L,(-1L),0x15FE7E41L,(-3L),(-1L)},{(-9L),1L,0x15FE7E41L,0xD0D301EFL,0x4566B8F7L,1L},{6L,(-9L),(-5L),(-1L),(-3L),(-3L)},{0x5F02D1EAL,(-5L),(-5L),0x5F02D1EAL,(-9L),1L}}};
                    const union U0 *l_781 = &g_150;
                    const union U0 **l_780 = &l_781;
                    const union U0 ***l_779 = &l_780;
                    const union U0 ****l_778[9][1] = {{&l_779},{&l_779},{&l_779},{&l_779},{&l_779},{&l_779},{&l_779},{&l_779},{&l_779}};
                    const union U0 *****l_777 = &l_778[0][0];
                    int i, j, k;
                    if ((l_776[6][4][5] = ((p_36 != ((((((safe_div_func_uint16_t_u_u(((***g_375) , (((((((*l_773) |= (((void*)0 != l_772) > p_38)) , (g_63[5] ^ (((void*)0 == l_774) && l_775))) > p_36) , p_38) | 1UL) , p_36)), g_755[1])) != 3L) , 255UL) >= (**l_764)) >= g_92) , g_344)) & 0x3B58L)))
                    { /* block id: 302 */
                        l_777 = l_777;
                    }
                    else
                    { /* block id: 304 */
                        if (l_782)
                            break;
                    }
                    if ((*g_407))
                        break;
                    if (l_775)
                        break;
                }
                else
                { /* block id: 309 */
                    const int32_t *l_783 = &g_8[1];
                    (*l_531) = 0xB.C6BFEFp+85;
                    return l_783;
                }
            }
            else
            { /* block id: 313 */
                uint16_t l_803 = 7UL;
                uint32_t *l_818 = &g_50;
                int32_t ***l_821 = &g_741;
                int32_t **** const l_820 = &l_821;
                int32_t **** const * const l_819[5] = {&l_820,&l_820,&l_820,&l_820,&l_820};
                int i;
                for (g_330 = 3; (g_330 >= 0); g_330 -= 1)
                { /* block id: 316 */
                    uint64_t l_800 = 8UL;
                    uint16_t *l_811 = &g_755[2];
                    int32_t l_814[7] = {0x5DAB9F54L,0x5DAB9F54L,0x5DAB9F54L,0x5DAB9F54L,0x5DAB9F54L,0x5DAB9F54L,0x5DAB9F54L};
                    int i, j;
                    for (g_92 = 0; (g_92 <= 3); g_92 += 1)
                    { /* block id: 319 */
                        uint16_t *l_789 = &g_755[0];
                        uint16_t *l_798[8][8][4] = {{{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][5],&g_751[0][0][0],&g_751[0][0][7],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][3][6]},{&g_751[0][3][2],(void*)0,&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][5],&g_751[0][3][6],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][7],&g_751[0][3][6],&g_751[0][3][2],&g_751[0][1][1]},{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][3][5]}},{{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][2][8]},{&g_751[0][0][7],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][8],&g_751[0][3][6],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],(void*)0},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][1][1]}},{{&g_751[0][0][7],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][3][2],(void*)0},{&g_751[0][0][7],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][7],&g_751[0][3][6],&g_751[0][3][2],&g_751[0][1][1]},{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][2][8]}},{{&g_751[0][0][7],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][8],&g_751[0][3][6],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],(void*)0},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][7],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][1][1]}},{{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][3][2],(void*)0},{&g_751[0][0][7],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][7],&g_751[0][3][6],&g_751[0][3][2],&g_751[0][1][1]},{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][2][8]},{&g_751[0][0][7],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][3][5]}},{{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][8],&g_751[0][3][6],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],(void*)0},{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][7],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][1][1]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]}},{{&g_751[0][0][8],&g_751[0][3][5],&g_751[0][3][2],(void*)0},{&g_751[0][0][7],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][3][5]},{&g_751[0][0][8],&g_751[0][0][0],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][7],&g_751[0][3][6],&g_751[0][3][2],&g_751[0][1][1]},{&g_751[0][0][8],(void*)0,&g_751[0][0][8],&g_751[0][2][8]},{&g_751[0][0][8],&g_751[0][2][8],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][2][8],&g_751[0][3][2],&g_751[0][2][8]},{&g_751[0][0][8],(void*)0,&g_751[0][0][5],(void*)0}},{{&g_751[0][3][2],&g_751[0][0][8],&g_751[0][0][8],(void*)0},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][2][8]},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][3][2],&g_751[0][2][8],&g_751[0][0][5],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][3][2],(void*)0},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],(void*)0},{&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8],&g_751[0][0][8]},{&g_751[0][0][8],&g_751[0][2][8],&g_751[0][0][8],&g_751[0][0][8]}}};
                        int32_t l_799 = 5L;
                        int i, j, k;
                        l_711[3][4][1] |= (l_799 = (g_714[g_330][g_330][(g_92 + 1)] & (((**l_760) , ((safe_div_func_uint32_t_u_u(4294967295UL, ((safe_lshift_func_uint8_t_u_s((p_36 <= (((safe_unary_minus_func_uint32_t_u((((**g_678) == (void*)0) || (--(*l_789))))) < (safe_sub_func_uint16_t_u_u(p_37, (g_751[0][0][8] = (safe_mul_func_int8_t_s_s((l_796 == &g_679), g_92)))))) > g_174)), 7)) | (-1L)))) , (void*)0)) != &g_679)));
                        if (p_38)
                            continue;
                        ++l_800;
                        l_803++;
                    }
                    l_710 = (safe_add_func_float_f_f((((safe_rshift_func_uint16_t_u_s(((*l_811) = (+p_39)), 5)) , ((*****l_688) , (((safe_sub_func_float_f_f((l_814[3] = 0x8.1404A3p-33), l_815)) <= ((***l_796) = (safe_sub_func_float_f_f(l_800, (*g_680))))) <= 0xD.126D1Fp-19))) < ((-0x1.Cp+1) >= (((((void*)0 == l_818) != p_38) , p_38) == 0x9.Ap-1))), 0x7.5A13B0p-3));
                    if ((**l_760))
                        continue;
                    l_822 = l_819[1];
                    for (p_37 = 0; (p_37 <= 3); p_37 += 1)
                    { /* block id: 336 */
                        volatile int32_t **l_823 = &g_407;
                        (*l_823) = (*g_406);
                    }
                }
            }
        }
    }
    for (g_343 = 8; (g_343 > 29); g_343++)
    { /* block id: 345 */
        int8_t *l_828 = &g_498[5][5][2];
        int16_t *l_829 = &l_782;
        int32_t l_832[10] = {0x8AA54C97L,0L,0x848EE5CDL,0L,0x8AA54C97L,0x8AA54C97L,0L,0x848EE5CDL,0L,0x8AA54C97L};
        int64_t *l_841 = &g_330;
        uint16_t *l_842 = (void*)0;
        float **l_846 = (void*)0;
        int8_t l_849 = 0xE7L;
        int32_t l_855 = 0x6D67DF85L;
        int8_t **l_936 = (void*)0;
        uint8_t l_959 = 0xAAL;
        uint64_t *l_1022[3];
        uint32_t ****l_1025[7][1] = {{(void*)0},{&g_210},{&g_210},{(void*)0},{&g_210},{&g_210},{(void*)0}};
        int i, j;
        for (i = 0; i < 3; i++)
            l_1022[i] = &g_95[0][4][1];
    }
    return l_1029[2];
}


/* ------------------------------------------ */
/* 
 * reads : g_160 g_50 g_63 g_150.f2 g_95 g_59 g_206.f0 g_150.f0 g_406 g_344 g_8 g_26 g_205 g_206 g_64 g_442 g_62 g_206.f2 g_407 g_379 g_330 g_92 g_209 g_511 g_343
 * writes: g_219 g_95 g_206.f0 g_150.f0 g_407 g_344 g_64 g_443 g_379 g_63 g_498 g_343 g_26 g_92 g_210 g_511 g_530
 */
static float  func_41(int64_t  p_42, uint32_t * p_43, int16_t  p_44, int16_t  p_45)
{ /* block id: 133 */
    uint32_t *l_400[5];
    int16_t *l_401 = &g_219;
    int32_t l_402 = 0L;
    uint64_t *l_403 = &g_95[0][4][1];
    int8_t l_404 = (-10L);
    int32_t *l_405 = &g_206[0][0].f0;
    uint16_t l_415 = 65535UL;
    uint8_t l_444 = 0xABL;
    uint32_t * const *l_513 = &g_160;
    uint32_t * const **l_512 = &l_513;
    float *l_515 = &g_443[8];
    int16_t l_528 = (-2L);
    float *l_529 = &g_530;
    int i;
    for (i = 0; i < 5; i++)
        l_400[i] = &g_174;
    if ((!((((*l_405) &= (safe_rshift_func_int8_t_s_u(((safe_mod_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u(((((*g_160) , g_63[5]) || (safe_mod_func_uint16_t_u_u((safe_add_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((*l_401) = ((g_150.f2 , &g_174) == l_400[4])), g_50)), (((*l_403) = (l_402 ^ ((&g_344 == (void*)0) , g_95[0][4][1]))) < 1L))), p_44))) <= g_59), p_44)), p_42)), l_404)) <= l_402), l_402))) < p_44) > 0x71F6F16E7EFF4FA7LL)))
    { /* block id: 137 */
        int32_t l_413 = 0xFE3CDBE7L;
        int32_t l_414 = 1L;
        uint64_t **l_467 = (void*)0;
        int32_t *l_468 = &g_379;
        for (g_150.f0 = 0; (g_150.f0 <= 5); g_150.f0 += 1)
        { /* block id: 140 */
            int32_t *l_408 = &g_206[0][0].f0;
            int32_t *l_409 = &l_402;
            int32_t *l_410 = &g_379;
            int32_t *l_411 = &g_59;
            int32_t *l_412[8][2][1] = {{{(void*)0},{&g_379}},{{(void*)0},{&g_379}},{{(void*)0},{&g_379}},{{(void*)0},{&g_379}},{{(void*)0},{&g_379}},{{(void*)0},{&g_379}},{{(void*)0},{&g_379}},{{(void*)0},{&g_379}}};
            int i, j, k;
            (*g_406) = &g_63[g_150.f0];
            l_415--;
            for (g_344 = 0; (g_344 <= 5); g_344 += 1)
            { /* block id: 145 */
                uint64_t l_418[3][6][3] = {{{18446744073709551614UL,18446744073709551608UL,18446744073709551614UL},{0x9520D26692E6D8D1LL,1UL,0x82EA3C633B4A1280LL},{1UL,0x46AE8F079D2087A2LL,0x7F213C8FBD4BF0A5LL},{0xBB5DCD1AAE636F43LL,1UL,0x6BB92B22A9D3D1F5LL},{0xEBAE75223C14E26DLL,18446744073709551615UL,18446744073709551615UL},{0xBB5DCD1AAE636F43LL,0xD79D5BB680DFCA66LL,0x940FC8A15306002ALL}},{{1UL,0xE7564B511363FC42LL,0xB2605AA8DD26E86ELL},{0x9520D26692E6D8D1LL,0x940FC8A15306002ALL,0xBB5DCD1AAE636F43LL},{18446744073709551614UL,0x7F213C8FBD4BF0A5LL,0x8ECD150BA0D21EE8LL},{0x940FC8A15306002ALL,0x940FC8A15306002ALL,0x5439D7C7F515F2E3LL},{18446744073709551608UL,0xE7564B511363FC42LL,9UL},{1UL,0xD79D5BB680DFCA66LL,1UL}},{{0xB2605AA8DD26E86ELL,18446744073709551615UL,0xE7564B511363FC42LL},{1UL,1UL,1UL},{0xF6B114E9ACD06F12LL,0x46AE8F079D2087A2LL,9UL},{5UL,1UL,0x5439D7C7F515F2E3LL},{18446744073709551615UL,18446744073709551608UL,0x8ECD150BA0D21EE8LL},{0x59FF775714E60344LL,0xBB5DCD1AAE636F43LL,0xBB5DCD1AAE636F43LL}}};
                int32_t *l_423 = &g_206[0][0].f0;
                int8_t *l_440 = &g_26[1][3][0];
                int i, j, k;
                l_418[1][5][2]++;
                if (p_42)
                    break;
                for (l_415 = 0; (l_415 <= 5); l_415 += 1)
                { /* block id: 150 */
                    int32_t *l_421 = &g_64;
                    int32_t **l_422 = (void*)0;
                    l_423 = l_421;
                    if (p_45)
                    { /* block id: 152 */
                        int32_t **l_426 = (void*)0;
                        int32_t **l_427 = &l_410;
                        uint64_t l_438 = 0x61F9DEB38CE7A4FBLL;
                        uint32_t l_439 = 0x4F018074L;
                        (*l_409) &= (((safe_div_func_int64_t_s_s((g_8[4] <= (((((*l_427) = p_43) == (((((p_45 != (safe_mod_func_int8_t_s_s((*l_408), (safe_lshift_func_uint8_t_u_s(g_344, 7))))) == 0xD73C05AAE4A37D48LL) , ((0L >= ((*l_423) = ((safe_mul_func_int8_t_s_s((g_26[4][6][3] >= ((*l_405) = (((safe_mul_func_uint8_t_u_u(((*g_205) , 0xA5L), p_45)) == 0L) , p_44))), (-1L))) != l_438))) != g_59)) & g_150.f0) , (void*)0)) | p_42) > (-7L))), 0xAE87C96607F19A99LL)) == p_45) <= g_59);
                        (*l_405) |= l_439;
                        return (*l_423);
                    }
                    else
                    { /* block id: 159 */
                        float *l_441 = (void*)0;
                        (*g_442) = (((void*)0 == &g_26[3][0][3]) == (l_440 != l_440));
                        --l_444;
                    }
                }
            }
            for (l_402 = 0; (l_402 <= 5); l_402 += 1)
            { /* block id: 167 */
                return (*l_411);
            }
        }
        (*g_407) = ((*l_468) = ((0xEDL || (safe_mul_func_uint16_t_u_u((g_150.f2 && ((((safe_unary_minus_func_uint32_t_u((safe_lshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_u((safe_add_func_int64_t_s_s(0x41F528355B21C627LL, (!(g_26[1][3][0] > 0x9C6657B6L)))), 2)) ^ ((safe_lshift_func_uint16_t_u_u((safe_add_func_int8_t_s_s(((safe_div_func_int64_t_s_s((safe_div_func_int32_t_s_s(((*l_405) |= ((p_45 > (-1L)) && (l_414 &= (g_62 >= ((safe_rshift_func_uint8_t_u_u(((void*)0 != l_467), 1)) , 1UL))))), (-1L))), g_26[1][3][2])) || g_206[0][0].f2), g_344)), 6)) | p_44)), 5)))) | l_415) <= 0UL) ^ 2UL)), 0xE221L))) >= 0x82F471A3L));
    }
    else
    { /* block id: 175 */
        union U0 **l_482[7] = {&g_205,&g_205,&g_205,&g_205,&g_205,&g_205,&g_205};
        int8_t *l_496 = &l_404;
        int8_t *l_497[4][3][6] = {{{&g_498[5][6][1],&g_498[5][6][1],(void*)0,&g_26[0][1][2],&g_498[7][7][1],&g_498[7][7][1]},{&g_498[7][7][1],&g_498[5][1][0],&g_498[5][1][0],&g_498[7][7][1],&g_498[5][6][1],&g_498[5][6][1]},{(void*)0,&g_498[5][6][1],&g_498[5][6][1],(void*)0,(void*)0,(void*)0}},{{&g_498[5][6][1],&g_26[0][1][2],&g_26[1][3][0],&g_26[1][3][0],(void*)0,&g_498[5][6][1]},{&g_498[5][6][1],&g_498[5][6][1],&g_498[7][7][1],&g_498[5][6][1],&g_498[5][6][1],&g_26[1][3][0]},{&g_26[1][3][0],&g_498[5][1][0],&g_498[5][6][1],&g_498[5][6][1],&g_498[7][7][1],&g_498[5][6][1]}},{{&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][1][0],&g_26[0][0][0],&g_498[5][6][1]},{&g_26[1][3][0],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_26[1][3][0]},{&g_26[0][0][0],(void*)0,(void*)0,&g_498[5][6][1],&g_498[5][6][1],&g_26[0][1][2]}},{{&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],(void*)0,(void*)0},{&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1],&g_498[5][6][1]},{&g_498[5][6][1],&g_498[5][6][1],&g_26[1][3][0],&g_498[7][7][1],&g_498[5][6][1],(void*)0}}};
        int32_t l_499 = 2L;
        uint8_t *l_500[3][1];
        uint32_t ****l_501[4][1][5];
        uint16_t *l_504 = &l_415;
        int32_t *l_510 = &g_511;
        int32_t l_514 = (-1L);
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_500[i][j] = &g_92;
        }
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 5; k++)
                    l_501[i][j][k] = &g_210;
            }
        }
        (*g_407) ^= (safe_sub_func_int32_t_s_s(((safe_lshift_func_uint8_t_u_u((g_92 &= (safe_rshift_func_int8_t_s_s((g_26[3][3][3] = ((((g_343 = (safe_mul_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((safe_unary_minus_func_int16_t_s(((*l_401) = p_44))), (safe_mod_func_int64_t_s_s((l_482[2] != (void*)0), (((safe_mul_func_uint8_t_u_u(((+g_26[1][3][0]) & (safe_rshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(((((safe_div_func_int16_t_s_s(((&g_95[0][8][1] != &g_344) && p_44), (((safe_lshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((*l_405), ((g_498[0][0][0] = ((*l_496) = g_379)) ^ 251UL))), 10)) != 255UL) | p_45))) & (*l_405)) && (*l_405)) , 6L), 7)), p_45))), 7L)) , (*l_405)) | g_330))))) ^ 4294967288UL), l_499))) > 1UL) , (*l_405)) != 0xAA37DFD5CE5605D7LL)), 7))), 6)) <= (*l_405)), 1L));
        l_514 &= (((((((((((*g_209) = (void*)0) == ((safe_mul_func_int8_t_s_s((((g_379 , ((((l_504 == (void*)0) ^ (((safe_div_func_int8_t_s_s(4L, p_42)) | (((*l_510) = (!(safe_mul_func_int8_t_s_s(((l_482[0] == (void*)0) | (((l_499 = (g_26[3][0][2] , (*l_405))) || l_499) == 3UL)), 0xF7L)))) , 249UL)) >= 1UL)) > p_42) , g_511)) , g_95[0][8][0]) , 0x86L), 0x6FL)) , l_512)) == g_59) < p_44) ^ 0xFECF855AL) == p_42) == (*l_405)) < g_50) , l_405) != p_43);
    }
    (*l_529) = (((((*l_405) < (((*l_515) = (0x2.9B6947p-15 > (*l_405))) != (((safe_add_func_uint16_t_u_u(((safe_add_func_int32_t_s_s(((!(p_42 , ((((*l_405) || ((void*)0 == &l_403)) == (((l_402 = ((*l_405) == ((safe_lshift_func_int16_t_s_s((!(safe_rshift_func_uint16_t_u_s(g_379, 2))), 13)) < 0x1DL))) | (*l_405)) > 0x310098D11F52AC1ALL)) | g_206[0][0].f0))) & g_343), 0x367712A5L)) | 0x2DA6B6E503A2894CLL), 1UL)) != p_45) , l_528))) >= p_42) <= p_44) != (*l_405));
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_50 g_8 g_26 g_64 g_7 g_58 g_95 g_150 g_159 g_150.f0 g_150.f2 g_63 g_92 g_206.f0 g_174 g_59 g_62 g_219 g_229 g_205 g_206 g_209 g_210 g_373
 * writes: g_50 g_59 g_64 g_92 g_95 g_174 g_176 g_26 g_219 g_205 g_330 g_343 g_344 g_374
 */
static int64_t  func_46(int32_t * p_47, uint32_t * p_48)
{ /* block id: 8 */
    uint16_t l_51[7][4][9] = {{{0x5A42L,0x3A86L,0x75E6L,0xC6A3L,1UL,0xD8D4L,0x1F04L,0x75E6L,0x4D1CL},{0xC72FL,0x75E6L,65535UL,0UL,0x5A54L,0xD027L,0x75E6L,1UL,0x75E6L},{0x2B72L,65529UL,65535UL,65535UL,65529UL,0x2B72L,1UL,0x6971L,65532UL},{4UL,5UL,0x75E6L,0x6971L,0x3A86L,0x6007L,0xC72FL,0x2B72L,0x493AL}},{{0x4D1CL,0UL,0xD8D4L,0x2547L,4UL,0xECCDL,1UL,0x4D1CL,0x3A86L},{0x5A42L,0x2B72L,0x3A86L,0xECCDL,0UL,1UL,0x75E6L,0x75E6L,1UL},{0x6007L,0x2B72L,0x0509L,0x2B72L,0x6007L,0xD027L,0x1F04L,65532UL,0x2B72L},{0x75E6L,0UL,0x493AL,65535UL,1UL,0UL,0x191BL,0x493AL,1UL}},{{0xC72FL,5UL,0x1F04L,0x0509L,0UL,0xD027L,5UL,0x3A86L,0x493AL},{1UL,65529UL,0xC6A3L,0x1F04L,0x2547L,1UL,1UL,1UL,0x2547L},{1UL,0x75E6L,0x75E6L,1UL,0UL,0xECCDL,0x3A86L,0x2B72L,0x5A42L},{0xC72FL,0x3A86L,0x0509L,0x2547L,65535UL,0x6007L,0UL,1UL,0x3A86L}},{{0x75E6L,0x76A1L,1UL,65532UL,0UL,0x2B72L,0x25B7L,0x493AL,0x6971L},{0x6007L,4UL,0UL,0x6971L,0x2547L,0xD027L,0xD027L,0x2547L,0x6971L},{0x5A42L,0UL,0x5A42L,0x1F04L,0UL,0xD8D4L,0x191BL,0x5A42L,0x3A86L},{0x4D1CL,0x75E6L,0x1F04L,0xD8D4L,1UL,0xC6A3L,0x75E6L,0x3A86L,0x5A42L}},{{4UL,0x2547L,65535UL,0x1F04L,0x6007L,4UL,0UL,0x6971L,0x2547L},{0x2B72L,0x25B7L,0x493AL,0x6971L,0UL,0x2547L,0UL,0x6971L,0x493AL},{0xC72FL,0xC72FL,0UL,65532UL,4UL,0x6007L,0x3A86L,7UL,65529UL},{65532UL,5UL,65531UL,0xC6A3L,7UL,0x493AL,0x6007L,65532UL,0x5A42L}},{{65535UL,0x5A42L,0x4D1CL,0x493AL,0xD027L,0x0509L,0xD8D4L,0xC6A3L,0x493AL},{0x2547L,7UL,65529UL,0xD8D4L,0x3A86L,0x3A86L,0xD8D4L,65529UL,7UL},{7UL,0x6007L,1UL,0UL,0xC72FL,0xD8D4L,0x6007L,65529UL,65529UL},{0x75E6L,0x1F04L,0xD8D4L,1UL,0xC6A3L,0x75E6L,0x3A86L,0x5A42L,0x191BL}},{{0x493AL,0x6007L,65532UL,0x5A42L,7UL,65535UL,65535UL,0x493AL,0xECCDL},{65531UL,7UL,0x4D1CL,65535UL,4UL,65535UL,0x4D1CL,7UL,65531UL},{0x2547L,0x5A42L,65529UL,0xC6A3L,4UL,0x75E6L,0xECCDL,65529UL,0x5A42L},{0xC6A3L,5UL,0UL,0x25B7L,0xD027L,0xD8D4L,0x5A54L,0x191BL,0x25B7L}}};
    uint32_t *l_96 = &g_50;
    uint64_t *l_142 = (void*)0;
    uint8_t *l_192 = &g_92;
    int32_t *l_201 = &g_64;
    uint32_t **l_208 = &l_96;
    uint32_t ***l_207 = &l_208;
    int64_t l_234 = 0x613806CF389D0FCBLL;
    const int16_t *l_272 = &g_219;
    int64_t l_273 = 0x8B98E25F7C6AF0C3LL;
    int32_t l_293 = 0x69C72776L;
    union U0 *l_325 = &g_206[0][0];
    int i, j, k;
    ++l_51[5][2][1];
    if (((void*)0 == &g_50))
    { /* block id: 10 */
        int64_t l_83 = 0x3D8A5C089A473F19LL;
        uint32_t l_185 = 0x52435B83L;
        uint8_t *l_193 = &g_92;
        union U0 *l_228 = (void*)0;
        float l_237 = 0x1.3p+1;
        int32_t l_238 = (-1L);
        int64_t *l_256 = &l_234;
        int8_t *l_261 = (void*)0;
        int8_t *l_262 = &g_26[1][5][1];
        int16_t *l_271 = (void*)0;
        int32_t l_274 = 1L;
        for (g_50 = 21; (g_50 <= 45); g_50++)
        { /* block id: 13 */
            if ((*p_47))
                break;
        }
        for (g_50 = (-29); (g_50 == 47); g_50++)
        { /* block id: 18 */
            int64_t l_75[10];
            const int8_t l_93 = 0x8BL;
            int32_t l_120[9] = {(-2L),(-4L),(-2L),(-2L),(-4L),(-2L),(-2L),(-4L),(-2L)};
            uint64_t *l_138[9] = {&g_95[0][2][1],&g_95[0][0][1],&g_95[0][2][1],&g_95[0][2][1],&g_95[0][0][1],&g_95[0][2][1],&g_95[0][2][1],&g_95[0][0][1],&g_95[0][2][1]};
            int32_t *l_179 = &l_120[0];
            uint64_t l_225 = 18446744073709551615UL;
            int i;
            for (i = 0; i < 10; i++)
                l_75[i] = 0xE83513FE74C19FD8LL;
            for (g_59 = 0; (g_59 != (-15)); g_59 = safe_sub_func_int32_t_s_s(g_59, 5))
            { /* block id: 21 */
                uint16_t l_76[10];
                uint64_t * const l_105 = &g_95[0][1][1];
                int32_t l_144 = 0x49FB5A3AL;
                int32_t l_163 = 0xF810095CL;
                uint32_t ***l_186 = (void*)0;
                uint32_t ***l_187 = (void*)0;
                uint32_t ***l_188 = (void*)0;
                uint32_t ***l_189 = (void*)0;
                uint32_t **l_191[1];
                uint32_t ***l_190 = &l_191[0];
                uint8_t *l_194 = &g_92;
                int i;
                for (i = 0; i < 10; i++)
                    l_76[i] = 65528UL;
                for (i = 0; i < 1; i++)
                    l_191[i] = (void*)0;
                for (g_64 = 0; (g_64 != 16); ++g_64)
                { /* block id: 24 */
                    uint64_t l_77 = 0xFF7D1D410A02D528LL;
                    int32_t l_84 = (-3L);
                    uint8_t *l_91 = &g_92;
                    uint64_t *l_94 = &g_95[0][4][1];
                    uint16_t l_97 = 65535UL;
                    uint32_t *l_131 = (void*)0;
                    int32_t l_175 = 1L;
                    int32_t *l_178 = &l_84;
                    int32_t **l_177[7];
                    int i;
                    for (i = 0; i < 7; i++)
                        l_177[i] = &l_178;
                    l_97 |= (safe_add_func_uint16_t_u_u(((((((safe_rshift_func_int8_t_s_s((((safe_mod_func_uint16_t_u_u((safe_div_func_int16_t_s_s(l_75[3], l_76[3])), ((l_77 != (safe_mul_func_int16_t_s_s(((l_84 ^= (l_75[2] | (safe_unary_minus_func_uint64_t_u(((safe_rshift_func_uint16_t_u_u(l_83, l_51[5][2][1])) < 3L))))) , ((safe_div_func_uint64_t_u_u(((*l_94) = ((safe_mod_func_int16_t_s_s(((~0xBBD03C5CEC10C1A6LL) && (safe_unary_minus_func_int8_t_s((((*l_91) = 0UL) < 0xC9L)))), l_93)) & l_75[3])), 0x87B7884FD301F6F3LL)) == g_26[2][6][3])), l_93))) , 0x3C53L))) && 0UL) & l_51[5][2][1]), l_76[9])) && l_83) & g_50) , l_96) == &g_50) <= (*p_47)), l_76[3]));
                    if ((~(safe_sub_func_uint32_t_u_u((((0xA61002F4L && (g_64 >= ((safe_mod_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u(0x18FD6B89D5A4BDFCLL, (((((void*)0 != l_105) && (((safe_add_func_uint16_t_u_u(l_97, l_76[3])) > (((safe_mul_func_int8_t_s_s(1L, (safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_s((safe_mod_func_int8_t_s_s((1UL > 0x80A7C56116C19582LL), g_50)), 5)), l_76[9])))) , l_51[5][2][1]) > 248UL)) < g_64)) || (*g_7)) || 0xC284EEC12BCECFDELL))), g_58)) , g_95[0][3][1]))) > l_76[3]) , g_64), 8UL))))
                    { /* block id: 29 */
                        int32_t *l_117[8][7][4] = {{{&g_59,&g_8[1],&g_8[3],&g_64},{(void*)0,&g_8[1],(void*)0,&g_64},{&l_84,&g_8[1],&g_64,&g_59},{&l_84,&g_8[6],&l_84,&g_8[1]},{&g_64,&g_59,&g_64,&g_8[0]},{&g_8[3],&g_59,&g_8[1],&l_84},{(void*)0,&g_59,&g_8[1],&g_8[1]}},{{(void*)0,&g_8[1],&g_8[1],(void*)0},{&g_64,&g_8[1],&g_59,&l_84},{(void*)0,(void*)0,(void*)0,&g_8[1]},{&g_8[1],&g_64,&g_8[1],&g_8[0]},{(void*)0,&g_64,&g_8[4],&g_64},{&g_8[0],&g_59,&g_8[4],&g_64},{(void*)0,&g_64,&g_8[1],&g_8[1]}},{{&g_8[1],(void*)0,(void*)0,&g_59},{(void*)0,&g_59,&g_59,&g_59},{&g_64,&l_84,&g_8[1],&g_8[0]},{(void*)0,&g_8[1],&g_8[1],&l_84},{(void*)0,(void*)0,&g_8[1],&g_8[1]},{&g_8[3],(void*)0,&g_64,&g_64},{&g_64,&g_64,&l_84,&g_59}},{{&l_84,(void*)0,&g_64,&g_8[6]},{&l_84,&l_84,&g_64,&g_8[3]},{(void*)0,&l_84,&g_8[1],&g_64},{&l_84,&g_64,&g_59,&g_8[1]},{&g_59,&g_59,&g_64,&g_59},{&g_8[1],&g_64,&g_8[1],&g_8[0]},{(void*)0,(void*)0,(void*)0,&l_84}},{{&g_8[1],(void*)0,&g_64,&g_64},{&g_8[0],&g_59,&g_64,&g_64},{&g_8[0],(void*)0,&g_8[0],(void*)0},{&g_64,&g_8[0],&l_84,(void*)0},{&g_64,&g_64,(void*)0,&g_59},{&g_8[1],&l_84,(void*)0,&l_84},{&g_8[1],(void*)0,(void*)0,&g_64}},{{&g_64,&l_84,&l_84,&g_8[4]},{&g_64,&g_8[1],&g_8[0],&g_8[0]},{&g_8[0],(void*)0,&g_64,&g_64},{&g_8[0],&g_59,&g_64,&g_8[1]},{&g_8[1],&g_59,(void*)0,&g_64},{(void*)0,&g_64,&g_8[1],&g_59},{&g_8[1],&g_64,&g_64,&g_8[1]}},{{&g_59,&g_64,&g_59,&g_64},{&l_84,&g_8[1],&g_8[1],(void*)0},{(void*)0,&g_8[4],&g_64,(void*)0},{&g_59,&g_8[1],&g_8[3],&g_64},{&g_64,&g_64,&l_84,&g_8[1]},{&g_59,&g_64,&g_59,&g_59},{&g_8[1],&g_64,(void*)0,&g_64}},{{(void*)0,&g_59,&l_84,&g_8[1]},{&g_8[1],&g_59,&g_8[0],&g_64},{&g_59,(void*)0,&l_84,&g_8[0]},{&g_8[6],&g_8[1],&g_8[6],&g_8[4]},{&g_8[4],&l_84,&g_8[1],&g_64},{&g_8[0],(void*)0,&g_8[1],&l_84},{&g_64,&l_84,&g_8[1],&g_59}}};
                        int32_t **l_116 = &l_117[7][6][1];
                        uint32_t **l_129 = (void*)0;
                        uint32_t **l_130 = &l_96;
                        uint64_t *l_141 = &g_95[0][4][1];
                        uint16_t *l_143 = &l_76[5];
                        int i, j, k;
                        (*l_116) = &g_59;
                        l_144 = (safe_lshift_func_int16_t_s_u(((l_120[5] = l_76[6]) , (safe_mod_func_uint8_t_u_u((l_120[5] , (safe_div_func_int32_t_s_s(((safe_add_func_int16_t_s_s(((((*l_130) = p_48) != l_131) & ((safe_add_func_uint64_t_u_u(g_50, (((g_26[2][2][2] ^ (((((*l_143) = (safe_mul_func_uint16_t_u_u((l_138[2] == (((safe_sub_func_int32_t_s_s(((void*)0 != l_141), (*p_47))) <= g_26[1][3][0]) , l_142)), l_83))) & l_77) , (void*)0) == &p_47)) , l_97) == g_58))) <= l_144)), 2L)) != l_51[5][2][1]), 0xEC11135BL))), g_26[1][3][0]))), l_83));
                    }
                    else
                    { /* block id: 35 */
                        uint16_t l_147 = 0UL;
                        int64_t *l_161[1][5];
                        int32_t *l_162[3][1][3];
                        uint32_t *l_173 = (void*)0;
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 5; j++)
                                l_161[i][j] = &l_75[7];
                        }
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                            {
                                for (k = 0; k < 3; k++)
                                    l_162[i][j][k] = &l_120[6];
                            }
                        }
                        l_163 |= (safe_add_func_int16_t_s_s(((l_147 , ((safe_mul_func_uint16_t_u_u(((((((((l_144 = (g_150 , ((safe_rshift_func_uint16_t_u_u((safe_add_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((((safe_mod_func_uint8_t_u_u(l_75[1], (l_76[0] , 6UL))) != g_26[5][5][2]) <= (g_159[0] == &g_160)), l_75[6])), g_8[1])), l_75[0])) , l_83))) > 0x5B22E30894340C13LL) , l_83) , 1L) >= l_97) != 0xE748EE0A1F665F5ALL) , (void*)0) == (void*)0), l_83)) , g_150)) , l_76[6]), l_84));
                        l_175 &= (+(safe_add_func_uint64_t_u_u(((*l_105) = (l_94 != ((9L >= (safe_lshift_func_int16_t_s_u((((safe_unary_minus_func_int64_t_s((((safe_mod_func_int32_t_s_s((((l_120[5] & l_144) || (!((l_83 && 18446744073709551612UL) , 0L))) || ((g_174 = l_84) || ((0UL >= (-5L)) || g_150.f0))), (*g_7))) | l_163) < g_95[0][6][1]))) , l_77) && g_95[0][8][0]), 11))) , (void*)0))), 4L)));
                    }
                    l_179 = (g_176 = &g_8[2]);
                }
            }
            l_238 &= ((*l_201) = ((-1L) > (((*l_201) | ((((safe_mul_func_int8_t_s_s((*l_179), (&g_174 != l_179))) >= (safe_add_func_uint16_t_u_u(l_234, (safe_rshift_func_int8_t_s_u(g_150.f2, 7))))) , ((l_185 >= ((0xD22769CAL < (*l_201)) , g_63[5])) , g_92)) && g_8[1])) && g_206[0][0].f0)));
        }
        l_274 |= ((safe_rshift_func_int8_t_s_u(((((safe_rshift_func_int16_t_s_s((safe_add_func_uint32_t_u_u((((safe_div_func_uint32_t_u_u((*l_201), ((safe_rshift_func_uint8_t_u_s((safe_mod_func_uint64_t_u_u(((+l_185) < ((l_185 , ((*l_256) = (((safe_mul_func_uint16_t_u_u((*l_201), l_185)) || l_83) > (*p_47)))) < (((safe_mod_func_uint32_t_u_u((l_238 ^= (safe_rshift_func_int8_t_s_s(((*l_262) ^= (*l_201)), 2))), ((safe_sub_func_int8_t_s_s((((safe_rshift_func_int16_t_s_s((((safe_mod_func_int64_t_s_s((((safe_add_func_uint32_t_u_u((0xE9BCL | l_185), (*p_47))) , l_271) == l_272), l_185)) == 0x12FE7324F2782A31LL) || l_273), g_50)) ^ (*p_47)) || l_185), 4UL)) | (*p_47)))) <= 18446744073709551615UL) > 0x72L))), (*l_201))), 7)) & g_174))) ^ 0xC6L) & l_185), 0x015D564FL)), 6)) | (*l_201)) ^ 0x9731L) || 65527UL), 0)) && l_83);
    }
    else
    { /* block id: 80 */
        int8_t *l_287 = (void*)0;
        int8_t *l_288[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_289 = 9L;
        int64_t *l_290[10][7][3] = {{{&l_273,&l_234,&l_234},{&l_273,&l_273,&l_234},{&l_234,(void*)0,&l_234},{&l_234,&l_273,&l_234},{&l_234,(void*)0,&l_234},{(void*)0,&l_234,&l_234},{(void*)0,&l_273,&l_234}},{{&l_273,&l_234,&l_234},{&l_234,&l_234,&l_234},{&l_273,&l_234,&l_234},{&l_273,&l_273,&l_234},{&l_234,(void*)0,&l_234},{&l_234,&l_273,&l_234},{&l_234,(void*)0,&l_234}},{{(void*)0,&l_234,&l_234},{(void*)0,&l_273,&l_234},{&l_234,(void*)0,&l_234},{&l_273,&l_273,(void*)0},{&l_234,&l_273,&l_234},{&l_234,&l_234,&l_234},{&l_234,(void*)0,(void*)0}},{{(void*)0,&l_234,&l_234},{&l_273,&l_273,&l_234},{(void*)0,&l_234,(void*)0},{&l_273,&l_234,&l_234},{&l_234,(void*)0,&l_234},{&l_273,&l_273,(void*)0},{&l_234,&l_273,&l_234}},{{&l_234,&l_234,&l_234},{&l_234,(void*)0,(void*)0},{(void*)0,&l_234,&l_234},{&l_273,&l_273,&l_234},{(void*)0,&l_234,(void*)0},{&l_273,&l_234,&l_234},{&l_234,(void*)0,&l_234}},{{&l_273,&l_273,(void*)0},{&l_234,&l_273,&l_234},{&l_234,&l_234,&l_234},{&l_234,(void*)0,(void*)0},{(void*)0,&l_234,&l_234},{&l_273,&l_273,&l_234},{(void*)0,&l_234,(void*)0}},{{&l_273,&l_234,&l_234},{&l_234,(void*)0,&l_234},{&l_273,&l_273,(void*)0},{&l_234,&l_273,&l_234},{&l_234,&l_234,&l_234},{&l_234,(void*)0,(void*)0},{(void*)0,&l_234,&l_234}},{{&l_273,&l_273,&l_234},{(void*)0,&l_234,(void*)0},{&l_273,&l_234,&l_234},{&l_234,(void*)0,&l_234},{&l_273,&l_273,(void*)0},{&l_234,&l_273,&l_234},{&l_234,&l_234,&l_234}},{{&l_234,(void*)0,(void*)0},{(void*)0,&l_234,&l_234},{&l_273,&l_273,&l_234},{(void*)0,&l_234,(void*)0},{&l_273,&l_234,&l_234},{&l_234,(void*)0,&l_234},{&l_273,&l_273,(void*)0}},{{&l_234,&l_273,&l_234},{&l_234,&l_234,&l_234},{&l_234,(void*)0,(void*)0},{(void*)0,&l_234,&l_234},{&l_273,&l_273,&l_234},{(void*)0,&l_234,(void*)0},{&l_273,&l_234,&l_234}}};
        int32_t l_291 = 1L;
        int32_t *l_292[6] = {&g_206[0][0].f0,&g_206[0][0].f0,(void*)0,&g_206[0][0].f0,&g_206[0][0].f0,(void*)0};
        int i, j, k;
lbl_306:
        l_293 ^= (((((*l_201) < g_26[1][3][0]) , (safe_sub_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((((((void*)0 == p_47) <= (((*l_201) < ((l_291 &= (2L < (((safe_add_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u(((safe_mul_func_int8_t_s_s((safe_mod_func_int8_t_s_s((((g_26[1][6][4] ^= (-3L)) > g_59) , (g_92 ^ (-9L))), l_289)), g_63[1])) , l_289), (*l_201))), 0xC0F596ABL)) && (*l_201)) > (*l_201)))) & (*l_201))) <= 0x4F03L)) , g_26[1][3][0]) & 8L), (*l_201))) <= 0x21811C08D2476922LL), g_8[1]))) , g_95[0][4][0]) == (*l_201));
        for (g_64 = 6; (g_64 >= 0); g_64 -= 1)
        { /* block id: 86 */
            uint64_t l_300 = 0UL;
            int16_t l_304 = 0xE447L;
            uint32_t l_305 = 0x1F6D53FDL;
            uint32_t l_370 = 18446744073709551608UL;
            for (g_174 = 0; (g_174 <= 5); g_174 += 1)
            { /* block id: 89 */
                uint32_t l_301 = 18446744073709551615UL;
                int32_t l_326 = 0L;
                const uint8_t *l_367 = (void*)0;
                uint32_t ***l_369 = &g_211;
                for (g_59 = 1; (g_59 >= 0); g_59 -= 1)
                { /* block id: 92 */
                    int32_t l_294 = 1L;
                    int8_t l_299 = 0xBAL;
                    int i, j, k;
                    l_305 |= (((l_304 = ((((*l_201) | (l_294 || (((safe_div_func_int64_t_s_s(((((safe_mod_func_uint16_t_u_u((l_299 && l_300), 65527UL)) , l_301) < (((safe_rshift_func_uint16_t_u_u(((l_294 , g_8[1]) > (l_299 && g_26[1][3][0])), l_299)) , g_26[1][3][0]) || 0x8E9BL)) <= 1UL), g_95[0][8][1])) || g_62) , (-1L)))) != (*p_47)) == 0x345722FBL)) < (*g_7)) > 0L);
                    if (g_92)
                        goto lbl_306;
                    for (g_219 = 0; (g_219 <= 3); g_219 += 1)
                    { /* block id: 98 */
                        int32_t **l_307 = &g_176;
                        int i, j, k;
                        (*l_307) = p_47;
                        return l_51[(g_59 + 1)][g_219][(g_219 + 2)];
                    }
                }
                for (g_50 = 0; (g_50 <= 1); g_50 += 1)
                { /* block id: 105 */
                    int16_t *l_317 = &g_219;
                    union U0 *l_323 = &g_206[0][0];
                    union U0 **l_324 = &l_323;
                    uint32_t *l_329[7][1] = {{&g_174},{&l_305},{&g_174},{&l_305},{&g_174},{&l_305},{&g_174}};
                    uint64_t **l_339 = &l_142;
                    uint64_t *l_341 = &g_95[0][4][1];
                    uint64_t **l_340 = &l_341;
                    int64_t l_342[5];
                    int32_t l_345 = 9L;
                    int32_t l_368[9][9][1] = {{{(-1L)},{(-1L)},{1L},{1L},{4L},{0xD127F73BL},{0L},{0L},{3L}},{{0L},{0L},{0xD127F73BL},{4L},{1L},{1L},{0L},{4L},{(-10L)}},{{0x5AEE3F43L},{(-1L)},{0x5AEE3F43L},{(-10L)},{4L},{0L},{0x160161ADL},{(-1L)},{0xE0B8F0D8L}},{{(-1L)},{3L},{0xBE708EF6L},{0xE9181DD6L},{0xBE708EF6L},{3L},{(-1L)},{0xE0B8F0D8L},{(-1L)}},{{0x160161ADL},{0L},{4L},{(-10L)},{0x5AEE3F43L},{(-1L)},{0x5AEE3F43L},{(-10L)},{4L}},{{0L},{0x160161ADL},{(-1L)},{0xE0B8F0D8L},{(-1L)},{3L},{0xBE708EF6L},{0xE9181DD6L},{0xBE708EF6L}},{{3L},{(-1L)},{0xE0B8F0D8L},{(-1L)},{0x160161ADL},{0L},{4L},{(-10L)},{0x5AEE3F43L}},{{(-1L)},{0x5AEE3F43L},{(-10L)},{4L},{0L},{0x160161ADL},{(-1L)},{0xE0B8F0D8L},{(-1L)}},{{3L},{0xBE708EF6L},{0xE9181DD6L},{0xBE708EF6L},{3L},{(-1L)},{0xE0B8F0D8L},{(-1L)},{0x160161ADL}}};
                    union U0 ***l_372 = &l_324;
                    union U0 ****l_371 = &l_372;
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_342[i] = 0x57D721A587B83316LL;
                    l_345 = (safe_lshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((--(*l_192)), 6)), ((safe_unary_minus_func_uint32_t_u((safe_add_func_int64_t_s_s((l_317 != (void*)0), (g_344 = (g_343 = (safe_lshift_func_int16_t_s_u(((safe_unary_minus_func_int32_t_s((l_326 = (safe_lshift_func_uint16_t_u_s(((((*l_324) = l_323) != ((*g_229) = (l_325 = (*g_229)))) > 0x25L), (*l_201)))))) | ((g_330 = (safe_rshift_func_uint16_t_u_s(0xF732L, 7))) < (safe_sub_func_uint8_t_u_u(l_301, ((((safe_rshift_func_int16_t_s_u((safe_sub_func_int32_t_s_s((safe_add_func_uint8_t_u_u((((*l_340) = ((*l_339) = &g_95[0][4][1])) == (void*)0), 0xCDL)), (*l_201))), l_301)) != l_342[4]) < (*l_201)) , l_305))))), g_95[0][4][1])))))))) | l_301)));
                    for (l_345 = 0; (l_345 <= 5); l_345 += 1)
                    { /* block id: 119 */
                        int64_t l_352 = 0xF5EC8C0D651198E9LL;
                        union U0 ***l_353 = (void*)0;
                        union U0 ***l_354 = &l_324;
                        uint8_t *l_366[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_366[i] = (void*)0;
                        l_292[g_174] = ((safe_lshift_func_uint8_t_u_s((safe_sub_func_int32_t_s_s(0L, 0xECCFEC03L)), ((safe_rshift_func_uint16_t_u_u((((l_326 < l_352) , &l_323) == ((*l_354) = &l_325)), 1)) != (((~(((((safe_div_func_uint64_t_u_u((safe_sub_func_int16_t_s_s((safe_add_func_uint32_t_u_u((safe_add_func_int64_t_s_s((g_330 = (safe_sub_func_int16_t_s_s(g_50, (((l_366[4] == (l_367 = ((**g_229) , (void*)0))) == l_305) & l_345)))), 0xC1DF6277E42EC95ELL)), (*g_7))), l_304)), (*l_201))) == (*l_201)) , l_368[3][2][0]) , (*g_209)) == l_369)) | l_370) || l_304)))) , p_47);
                        (*g_373) = l_371;
                        if ((*p_47))
                            break;
                    }
                }
            }
        }
    }
    return g_63[5];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_8[i], "g_8[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_26[i][j][k], "g_26[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_32[i][j], "g_32[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_50, "g_50", print_hash_value);
    transparent_crc(g_58, "g_58", print_hash_value);
    transparent_crc(g_59, "g_59", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_63[i], "g_63[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_64, "g_64", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_95[i][j][k], "g_95[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_150.f0, "g_150.f0", print_hash_value);
    transparent_crc_bytes (&g_150.f1, sizeof(g_150.f1), "g_150.f1", print_hash_value);
    transparent_crc(g_150.f2, "g_150.f2", print_hash_value);
    transparent_crc(g_174, "g_174", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_206[i][j].f0, "g_206[i][j].f0", print_hash_value);
            transparent_crc_bytes(&g_206[i][j].f1, sizeof(g_206[i][j].f1), "g_206[i][j].f1", print_hash_value);
            transparent_crc(g_206[i][j].f2, "g_206[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_219, "g_219", print_hash_value);
    transparent_crc(g_330, "g_330", print_hash_value);
    transparent_crc(g_343, "g_343", print_hash_value);
    transparent_crc(g_344, "g_344", print_hash_value);
    transparent_crc(g_379, "g_379", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_443[i], sizeof(g_443[i]), "g_443[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_498[i][j][k], "g_498[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_511, "g_511", print_hash_value);
    transparent_crc_bytes (&g_530, sizeof(g_530), "g_530", print_hash_value);
    transparent_crc(g_563, "g_563", print_hash_value);
    transparent_crc_bytes (&g_575, sizeof(g_575), "g_575", print_hash_value);
    transparent_crc(g_588.f0, "g_588.f0", print_hash_value);
    transparent_crc_bytes (&g_588.f1, sizeof(g_588.f1), "g_588.f1", print_hash_value);
    transparent_crc(g_588.f2, "g_588.f2", print_hash_value);
    transparent_crc(g_592, "g_592", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_714[i][j][k], "g_714[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_740[i][j], "g_740[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_749[i], "g_749[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_751[i][j][k], "g_751[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_755[i], "g_755[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_766, "g_766", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1052[i], "g_1052[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1055, "g_1055", print_hash_value);
    transparent_crc(g_1082, "g_1082", print_hash_value);
    transparent_crc(g_1164, "g_1164", print_hash_value);
    transparent_crc(g_1226.f0, "g_1226.f0", print_hash_value);
    transparent_crc_bytes (&g_1226.f1, sizeof(g_1226.f1), "g_1226.f1", print_hash_value);
    transparent_crc(g_1226.f2, "g_1226.f2", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1266[i][j][k], "g_1266[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1399[i], "g_1399[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1420, "g_1420", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1472[i].f0, "g_1472[i].f0", print_hash_value);
        transparent_crc_bytes(&g_1472[i].f1, sizeof(g_1472[i].f1), "g_1472[i].f1", print_hash_value);
        transparent_crc(g_1472[i].f2, "g_1472[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1696, "g_1696", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1855[i][j], "g_1855[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1897, "g_1897", print_hash_value);
    transparent_crc_bytes (&g_1984, sizeof(g_1984), "g_1984", print_hash_value);
    transparent_crc(g_2124.f0, "g_2124.f0", print_hash_value);
    transparent_crc_bytes (&g_2124.f1, sizeof(g_2124.f1), "g_2124.f1", print_hash_value);
    transparent_crc(g_2124.f2, "g_2124.f2", print_hash_value);
    transparent_crc(g_2132.f0, "g_2132.f0", print_hash_value);
    transparent_crc_bytes (&g_2132.f1, sizeof(g_2132.f1), "g_2132.f1", print_hash_value);
    transparent_crc(g_2132.f2, "g_2132.f2", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2133[i][j].f0, "g_2133[i][j].f0", print_hash_value);
            transparent_crc_bytes(&g_2133[i][j].f1, sizeof(g_2133[i][j].f1), "g_2133[i][j].f1", print_hash_value);
            transparent_crc(g_2133[i][j].f2, "g_2133[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2156[i].f0, "g_2156[i].f0", print_hash_value);
        transparent_crc_bytes(&g_2156[i].f1, sizeof(g_2156[i].f1), "g_2156[i].f1", print_hash_value);
        transparent_crc(g_2156[i].f2, "g_2156[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2226.f0, "g_2226.f0", print_hash_value);
    transparent_crc_bytes (&g_2226.f1, sizeof(g_2226.f1), "g_2226.f1", print_hash_value);
    transparent_crc(g_2226.f2, "g_2226.f2", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2260[i], "g_2260[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2271.f0, "g_2271.f0", print_hash_value);
    transparent_crc_bytes (&g_2271.f1, sizeof(g_2271.f1), "g_2271.f1", print_hash_value);
    transparent_crc(g_2271.f2, "g_2271.f2", print_hash_value);
    transparent_crc(g_2313, "g_2313", print_hash_value);
    transparent_crc(g_2348.f0, "g_2348.f0", print_hash_value);
    transparent_crc_bytes (&g_2348.f1, sizeof(g_2348.f1), "g_2348.f1", print_hash_value);
    transparent_crc(g_2348.f2, "g_2348.f2", print_hash_value);
    transparent_crc(g_2494.f0, "g_2494.f0", print_hash_value);
    transparent_crc_bytes (&g_2494.f1, sizeof(g_2494.f1), "g_2494.f1", print_hash_value);
    transparent_crc(g_2494.f2, "g_2494.f2", print_hash_value);
    transparent_crc(g_2519, "g_2519", print_hash_value);
    transparent_crc(g_2546.f0, "g_2546.f0", print_hash_value);
    transparent_crc_bytes (&g_2546.f1, sizeof(g_2546.f1), "g_2546.f1", print_hash_value);
    transparent_crc(g_2546.f2, "g_2546.f2", print_hash_value);
    transparent_crc(g_2589.f0, "g_2589.f0", print_hash_value);
    transparent_crc_bytes (&g_2589.f1, sizeof(g_2589.f1), "g_2589.f1", print_hash_value);
    transparent_crc(g_2589.f2, "g_2589.f2", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2644[i], "g_2644[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2651.f0, "g_2651.f0", print_hash_value);
    transparent_crc_bytes (&g_2651.f1, sizeof(g_2651.f1), "g_2651.f1", print_hash_value);
    transparent_crc(g_2651.f2, "g_2651.f2", print_hash_value);
    transparent_crc(g_2652.f0, "g_2652.f0", print_hash_value);
    transparent_crc_bytes (&g_2652.f1, sizeof(g_2652.f1), "g_2652.f1", print_hash_value);
    transparent_crc(g_2652.f2, "g_2652.f2", print_hash_value);
    transparent_crc(g_2685.f0, "g_2685.f0", print_hash_value);
    transparent_crc_bytes (&g_2685.f1, sizeof(g_2685.f1), "g_2685.f1", print_hash_value);
    transparent_crc(g_2685.f2, "g_2685.f2", print_hash_value);
    transparent_crc(g_2719.f0, "g_2719.f0", print_hash_value);
    transparent_crc_bytes (&g_2719.f1, sizeof(g_2719.f1), "g_2719.f1", print_hash_value);
    transparent_crc(g_2719.f2, "g_2719.f2", print_hash_value);
    transparent_crc(g_2794.f0, "g_2794.f0", print_hash_value);
    transparent_crc_bytes (&g_2794.f1, sizeof(g_2794.f1), "g_2794.f1", print_hash_value);
    transparent_crc(g_2794.f2, "g_2794.f2", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2801[i][j][k], "g_2801[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2882[i], "g_2882[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3072.f0, "g_3072.f0", print_hash_value);
    transparent_crc_bytes (&g_3072.f1, sizeof(g_3072.f1), "g_3072.f1", print_hash_value);
    transparent_crc(g_3072.f2, "g_3072.f2", print_hash_value);
    transparent_crc(g_3096, "g_3096", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 870
XXX total union variables: 18

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 39
breakdown:
   depth: 1, occurrence: 253
   depth: 2, occurrence: 67
   depth: 3, occurrence: 4
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 11, occurrence: 1
   depth: 12, occurrence: 2
   depth: 14, occurrence: 1
   depth: 16, occurrence: 4
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 5
   depth: 23, occurrence: 7
   depth: 24, occurrence: 4
   depth: 26, occurrence: 3
   depth: 27, occurrence: 4
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 31, occurrence: 3
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 36, occurrence: 3
   depth: 37, occurrence: 1
   depth: 39, occurrence: 2

XXX total number of pointers: 684

XXX times a variable address is taken: 1684
XXX times a pointer is dereferenced on RHS: 374
breakdown:
   depth: 1, occurrence: 278
   depth: 2, occurrence: 59
   depth: 3, occurrence: 30
   depth: 4, occurrence: 5
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 403
breakdown:
   depth: 1, occurrence: 355
   depth: 2, occurrence: 27
   depth: 3, occurrence: 20
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 72
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 23
XXX times a pointer is qualified to be dereferenced: 11614

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 6264
   level: 2, occurrence: 889
   level: 3, occurrence: 352
   level: 4, occurrence: 70
   level: 5, occurrence: 77
XXX number of pointers point to pointers: 318
XXX number of pointers point to scalars: 349
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 33.6
XXX average alias set size: 1.46

XXX times a non-volatile is read: 2441
XXX times a non-volatile is write: 1206
XXX times a volatile is read: 170
XXX    times read thru a pointer: 43
XXX times a volatile is write: 87
XXX    times written thru a pointer: 50
XXX times a volatile is available for access: 8.73e+03
XXX percentage of non-volatile access: 93.4

XXX forward jumps: 1
XXX backward jumps: 12

XXX stmts: 259
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 42
   depth: 2, occurrence: 35
   depth: 3, occurrence: 42
   depth: 4, occurrence: 61
   depth: 5, occurrence: 47

XXX percentage a fresh-made variable is used: 17.5
XXX percentage an existing variable is used: 82.5
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1597756387
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   unsigned f0 : 16;
   uint32_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static volatile int16_t g_29 = 0x7507L;/* VOLATILE GLOBAL g_29 */
static uint64_t g_33 = 0x0025004286A4ABFFLL;
static int32_t g_37 = (-9L);
static int32_t *g_39[1] = {&g_37};
static int32_t ** volatile g_38 = &g_39[0];/* VOLATILE GLOBAL g_38 */
static uint8_t g_50 = 0x0AL;
static uint8_t g_54 = 2UL;
static uint16_t g_78 = 0xF814L;
static volatile int16_t g_100 = (-1L);/* VOLATILE GLOBAL g_100 */
static int32_t g_118 = 4L;
static int32_t * volatile g_117 = &g_118;/* VOLATILE GLOBAL g_117 */
static int64_t g_122[7] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
static union U0 g_133 = {1UL};
static volatile uint64_t g_145 = 18446744073709551615UL;/* VOLATILE GLOBAL g_145 */
static volatile uint64_t *g_144 = &g_145;
static volatile uint64_t **g_143[5] = {&g_144,&g_144,&g_144,&g_144,&g_144};
static int32_t ** volatile g_147 = &g_39[0];/* VOLATILE GLOBAL g_147 */
static int8_t g_167 = 0xD4L;
static uint32_t g_175 = 7UL;
static float * volatile g_177 = (void*)0;/* VOLATILE GLOBAL g_177 */
static float g_179 = 0x9.C70198p+22;
static int32_t * volatile g_182 = &g_118;/* VOLATILE GLOBAL g_182 */
static int8_t g_195 = 0xB9L;
static uint64_t g_230[5] = {6UL,6UL,6UL,6UL,6UL};
static int32_t ** volatile g_234[5] = {&g_39[0],&g_39[0],&g_39[0],&g_39[0],&g_39[0]};
static int32_t ** volatile g_235 = &g_39[0];/* VOLATILE GLOBAL g_235 */
static int32_t g_301 = 0x6257B961L;
static int32_t g_304 = 0xD0C41935L;
static int32_t g_314 = 0x84EC217BL;
static float * const  volatile g_320 = &g_179;/* VOLATILE GLOBAL g_320 */
static int32_t ** volatile g_358 = &g_39[0];/* VOLATILE GLOBAL g_358 */
static uint8_t *g_370 = (void*)0;
static uint8_t * volatile * const  volatile g_369 = &g_370;/* VOLATILE GLOBAL g_369 */
static int32_t ** volatile g_396 = (void*)0;/* VOLATILE GLOBAL g_396 */
static int32_t ** volatile g_397 = &g_39[0];/* VOLATILE GLOBAL g_397 */
static uint16_t *g_407 = &g_78;
static uint16_t **g_406 = &g_407;
static uint16_t *** volatile g_405[1][5] = {{&g_406,&g_406,&g_406,&g_406,&g_406}};
static uint16_t *** volatile g_408[1] = {&g_406};
static uint16_t *** volatile g_409 = &g_406;/* VOLATILE GLOBAL g_409 */
static int16_t g_425 = 2L;
static const uint32_t g_458[6][4] = {{1UL,0x98B4BF59L,1UL,0x98B4BF59L},{1UL,0x98B4BF59L,1UL,0x98B4BF59L},{1UL,0x98B4BF59L,1UL,0x98B4BF59L},{1UL,0x98B4BF59L,1UL,0x98B4BF59L},{1UL,0x98B4BF59L,1UL,0x98B4BF59L},{1UL,0x98B4BF59L,1UL,0x98B4BF59L}};
static uint32_t g_461 = 0x7DC21431L;
static float g_464 = 0x6.C6C08Cp+31;
static uint8_t g_547 = 3UL;
static uint64_t *g_587 = (void*)0;
static uint64_t **g_586 = &g_587;
static uint64_t *** volatile g_585 = &g_586;/* VOLATILE GLOBAL g_585 */
static int32_t g_589[2][5][8] = {{{0L,0x94660D6DL,1L,(-5L),0xA6973961L,(-1L),0xA6973961L,(-5L)},{0L,(-1L),0L,0xCD381465L,(-1L),1L,1L,0L},{1L,0x0DA98EC3L,(-5L),0xAB095452L,0x3984E6F7L,0x94D6A80BL,(-1L),0x8840CBE3L},{1L,(-7L),(-1L),0x566F6BD2L,(-1L),0L,0xAB095452L,0xD0818F5EL},{0L,0x8134376AL,0L,(-7L),0xA6973961L,0xA6973961L,(-7L),0L}},{{0L,0L,1L,(-1L),0xB99C1D2AL,1L,0xCD381465L,1L},{(-1L),0x566F6BD2L,(-1L),0L,0x8134376AL,(-1L),0x94660D6DL,1L},{0x566F6BD2L,0xCD381465L,0xD0818F5EL,(-1L),0xE09C0F09L,0xAB095452L,0x6E5F3C94L,0L},{(-1L),0xD0818F5EL,0xB99C1D2AL,(-7L),(-7L),(-7L),0xB99C1D2AL,0xD0818F5EL},{0L,(-1L),(-7L),0x566F6BD2L,0xAB095452L,0xCD381465L,(-5L),0x8840CBE3L}}};
static uint32_t * volatile g_620 = (void*)0;/* VOLATILE GLOBAL g_620 */
static uint32_t * volatile * volatile g_619 = &g_620;/* VOLATILE GLOBAL g_619 */
static float * const  volatile g_674 = (void*)0;/* VOLATILE GLOBAL g_674 */
static uint64_t g_701 = 1UL;
static int32_t g_703[1] = {0L};
static uint32_t *g_739 = &g_461;
static uint32_t g_741 = 4294967295UL;
static int8_t g_777 = 0xD9L;
static uint32_t g_791 = 18446744073709551615UL;
static int8_t *g_798 = &g_167;
static const uint16_t g_812[3][5][8] = {{{1UL,0x985AL,0x7A1CL,1UL,7UL,65528UL,0x2DA5L,0x3CAEL},{1UL,0xE207L,0x7571L,1UL,0x2DA5L,0xAFAAL,65535UL,65535UL},{0x3CAEL,1UL,0UL,0UL,1UL,0x3CAEL,0x7A1CL,1UL},{0xBDA9L,0x985AL,7UL,4UL,0UL,0x7A1CL,0xBDA9L,65529UL},{1UL,0x2DA5L,0x7571L,4UL,65535UL,0x7571L,1UL,1UL}},{{65529UL,65535UL,0x985AL,0UL,0xBDA9L,0UL,0x985AL,65535UL},{0xE207L,0x7A1CL,65529UL,1UL,0UL,7UL,1UL,0x3CAEL},{4UL,0xBDA9L,0xAFAAL,1UL,0xE207L,0x7571L,1UL,0x2DA5L},{0x3CAEL,1UL,65529UL,7UL,0UL,0x985AL,0x985AL,0UL},{0UL,0x985AL,0x985AL,0UL,7UL,65529UL,1UL,0x3CAEL}},{{0x2DA5L,1UL,0x7571L,0xE207L,1UL,0xAFAAL,0xBDA9L,4UL},{0x3CAEL,1UL,7UL,0UL,1UL,65529UL,0x7A1CL,0xE207L},{65535UL,0x985AL,0UL,0xBDA9L,0UL,0x985AL,65535UL,65535UL},{7UL,0x7A1CL,4UL,65528UL,0x3CAEL,4UL,0x985AL,65535UL},{65535UL,65529UL,0x7571L,1UL,0x3CAEL,0x3F6CL,0UL,65529UL}}};
static float * volatile g_863 = (void*)0;/* VOLATILE GLOBAL g_863 */
static float * volatile g_864[2] = {&g_464,&g_464};
static uint32_t g_892 = 0x55EC2979L;
static int32_t ** volatile g_950 = &g_39[0];/* VOLATILE GLOBAL g_950 */
static volatile uint32_t ***g_985 = (void*)0;
static float * volatile g_1007 = &g_179;/* VOLATILE GLOBAL g_1007 */
static int32_t *g_1057[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t ** volatile g_1056 = &g_1057[7];/* VOLATILE GLOBAL g_1056 */
static float * volatile g_1060 = &g_179;/* VOLATILE GLOBAL g_1060 */
static int64_t *g_1074 = &g_122[2];
static union U0 g_1160 = {0x2BF6C799L};
static uint16_t *** const g_1192[2] = {&g_406,&g_406};
static uint16_t *** const *g_1191 = &g_1192[1];
static int32_t ** volatile g_1197 = &g_39[0];/* VOLATILE GLOBAL g_1197 */
static uint32_t **g_1215 = &g_739;
static uint32_t ***g_1214 = &g_1215;
static uint32_t ****g_1213 = &g_1214;
static uint8_t **g_1282 = &g_370;
static uint8_t ***g_1281 = &g_1282;
static volatile int32_t g_1326[5][4] = {{(-8L),0xFD139385L,(-1L),0xFD139385L},{0xFD139385L,0L,(-1L),(-1L)},{(-8L),(-8L),0xFD139385L,(-1L)},{0xC66FCB32L,0L,0xC66FCB32L,0xFD139385L},{0xC66FCB32L,0xFD139385L,0xFD139385L,0xC66FCB32L}};
static volatile int32_t *g_1325 = &g_1326[2][0];
static volatile int32_t * const *g_1324 = &g_1325;
static volatile int32_t * const * volatile * volatile g_1323 = &g_1324;/* VOLATILE GLOBAL g_1323 */
static float g_1343 = (-0x1.9p-1);
static float * const g_1342 = &g_1343;
static float * const *g_1341 = &g_1342;
static const float *g_1345 = &g_1343;
static const float **g_1344 = &g_1345;
static uint16_t g_1380[7] = {0xADFAL,0xADFAL,0xADFAL,0xADFAL,0xADFAL,0xADFAL,0xADFAL};
static const uint16_t *g_1470[9][10] = {{&g_1380[2],&g_1380[3],&g_812[0][3][4],&g_1380[3],&g_1380[2],&g_1380[2],&g_812[0][1][6],&g_812[0][1][6],(void*)0,(void*)0},{(void*)0,&g_1380[2],&g_1380[2],&g_812[1][3][3],&g_812[0][1][3],&g_812[1][3][3],&g_1380[2],&g_1380[2],(void*)0,&g_1380[2]},{(void*)0,&g_1380[5],&g_78,&g_1380[2],&g_1380[2],&g_812[0][1][6],&g_1380[2],(void*)0,&g_812[0][3][4],(void*)0},{(void*)0,&g_78,&g_812[0][1][6],&g_1380[2],&g_1380[5],&g_1380[2],&g_1380[2],(void*)0,(void*)0,&g_812[0][1][6]},{&g_812[0][1][6],(void*)0,&g_1380[2],&g_812[1][3][3],(void*)0,(void*)0,&g_812[1][3][3],&g_1380[2],(void*)0,&g_812[0][1][6]},{&g_1380[2],&g_1380[2],&g_1380[2],&g_1380[3],&g_78,&g_1380[2],&g_812[0][1][6],&g_812[0][3][4],&g_1380[5],&g_812[0][1][3]},{&g_1380[2],&g_812[0][1][6],(void*)0,&g_1380[2],&g_78,&g_1380[3],&g_1380[2],&g_1380[2],&g_78,&g_812[0][1][6]},{&g_78,&g_812[0][1][6],&g_812[0][1][6],&g_1380[2],(void*)0,&g_1380[2],(void*)0,&g_1380[2],&g_812[0][1][6],&g_812[0][1][6]},{&g_812[0][3][4],&g_1380[2],&g_812[1][3][3],&g_812[0][1][6],&g_1380[5],&g_78,&g_1380[2],&g_1380[3],(void*)0,(void*)0}};
static const uint16_t **g_1469 = &g_1470[1][6];
static int32_t *g_1497 = &g_301;
static int32_t **g_1496 = &g_1497;
static const volatile int64_t g_1513 = 0L;/* VOLATILE GLOBAL g_1513 */
static uint16_t g_1553 = 0UL;
static int32_t ** volatile g_1585 = (void*)0;/* VOLATILE GLOBAL g_1585 */
static float g_1615 = 0x7.8p-1;
static int32_t ***g_1620 = (void*)0;
static int32_t * volatile g_1679[2][2] = {{&g_703[0],&g_703[0]},{&g_703[0],&g_703[0]}};
static uint32_t *****g_1704 = &g_1213;
static int64_t g_1717 = 1L;
static int32_t ** volatile g_1742 = &g_1057[7];/* VOLATILE GLOBAL g_1742 */
static int32_t g_1770 = (-8L);
static int32_t ** volatile g_1794[7][10][3] = {{{(void*)0,(void*)0,(void*)0},{&g_1057[7],&g_1057[7],&g_1057[7]},{(void*)0,(void*)0,&g_39[0]},{&g_39[0],&g_39[0],&g_1057[5]},{(void*)0,&g_1057[7],&g_39[0]},{&g_1057[6],&g_39[0],&g_1057[5]},{&g_39[0],&g_1057[6],&g_39[0]},{&g_39[0],&g_39[0],&g_1057[7]},{&g_39[0],&g_1057[7],(void*)0},{(void*)0,&g_39[0],&g_1057[6]}},{{(void*)0,&g_1057[1],&g_39[0]},{(void*)0,&g_39[0],&g_39[0]},{&g_39[0],&g_1057[5],&g_39[0]},{&g_39[0],&g_1057[7],&g_39[0]},{&g_39[0],&g_39[0],&g_39[0]},{&g_1057[6],&g_39[0],&g_1057[7]},{(void*)0,&g_39[0],(void*)0},{&g_39[0],&g_1057[7],&g_1057[7]},{(void*)0,&g_1057[5],&g_39[0]},{&g_1057[7],&g_39[0],&g_39[0]}},{{(void*)0,&g_1057[1],&g_39[0]},{&g_39[0],&g_39[0],&g_39[0]},{&g_39[0],&g_1057[7],&g_39[0]},{&g_39[0],&g_39[0],&g_1057[7]},{&g_39[0],&g_1057[6],(void*)0},{&g_1057[7],&g_39[0],&g_1057[7]},{(void*)0,&g_1057[7],&g_39[0]},{&g_1057[7],&g_39[0],&g_39[0]},{&g_39[0],(void*)0,&g_39[0]},{&g_39[0],&g_1057[7],&g_39[0]}},{{&g_39[0],(void*)0,&g_39[0]},{&g_39[0],&g_39[0],&g_1057[6]},{(void*)0,(void*)0,(void*)0},{&g_1057[7],&g_1057[7],&g_1057[7]},{(void*)0,(void*)0,&g_39[0]},{&g_39[0],&g_39[0],&g_1057[5]},{(void*)0,&g_1057[7],&g_39[0]},{&g_1057[6],&g_39[0],&g_1057[5]},{&g_39[0],&g_1057[6],&g_39[0]},{&g_39[0],&g_39[0],&g_1057[7]}},{{&g_39[0],&g_1057[7],(void*)0},{(void*)0,&g_39[0],&g_1057[6]},{(void*)0,&g_1057[1],&g_39[0]},{(void*)0,&g_39[0],&g_39[0]},{&g_39[0],&g_1057[5],&g_39[0]},{&g_39[0],&g_1057[7],&g_39[0]},{&g_39[0],&g_39[0],&g_39[0]},{&g_1057[6],&g_39[0],&g_1057[7]},{(void*)0,&g_39[0],(void*)0},{&g_39[0],&g_1057[7],&g_1057[7]}},{{(void*)0,&g_1057[5],&g_39[0]},{&g_1057[7],&g_39[0],&g_39[0]},{(void*)0,&g_1057[1],&g_39[0]},{&g_39[0],&g_39[0],&g_39[0]},{&g_39[0],&g_1057[7],&g_39[0]},{&g_39[0],&g_39[0],&g_1057[7]},{&g_39[0],&g_1057[6],(void*)0},{&g_1057[7],&g_39[0],&g_1057[7]},{(void*)0,&g_1057[7],&g_39[0]},{&g_1057[7],&g_39[0],&g_39[0]}},{{&g_39[0],(void*)0,&g_39[0]},{&g_39[0],&g_1057[7],&g_39[0]},{&g_39[0],(void*)0,&g_39[0]},{&g_39[0],&g_39[0],&g_1057[6]},{(void*)0,(void*)0,(void*)0},{&g_1057[7],&g_1057[7],&g_1057[7]},{(void*)0,(void*)0,&g_39[0]},{&g_39[0],&g_39[0],&g_1057[5]},{(void*)0,&g_1057[7],&g_39[0]},{&g_1057[6],&g_39[0],&g_1057[5]}}};
static int32_t ** volatile g_1802 = &g_39[0];/* VOLATILE GLOBAL g_1802 */
static int32_t * volatile g_1887 = &g_589[0][2][2];/* VOLATILE GLOBAL g_1887 */
static uint16_t g_1905 = 65527UL;
static int16_t g_2035 = 0xB075L;
static int32_t * volatile g_2043 = &g_37;/* VOLATILE GLOBAL g_2043 */
static volatile int32_t **g_2070 = (void*)0;
static volatile int32_t ** volatile *g_2069 = &g_2070;
static volatile int32_t ** volatile ** volatile g_2068 = &g_2069;/* VOLATILE GLOBAL g_2068 */
static volatile int32_t ** volatile ** volatile *g_2067[10] = {(void*)0,&g_2068,(void*)0,&g_2068,(void*)0,&g_2068,(void*)0,&g_2068,(void*)0,&g_2068};
static int32_t * volatile g_2076 = &g_589[1][1][2];/* VOLATILE GLOBAL g_2076 */
static const volatile uint16_t g_2129[2][2] = {{1UL,1UL},{1UL,1UL}};
static volatile uint8_t g_2185 = 0xF0L;/* VOLATILE GLOBAL g_2185 */
static int64_t *** volatile g_2277 = (void*)0;/* VOLATILE GLOBAL g_2277 */
static int64_t **g_2279[4][8][3] = {{{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074}},{{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074}},{{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074}},{{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074}}};
static int64_t *** volatile g_2278 = &g_2279[3][1][0];/* VOLATILE GLOBAL g_2278 */
static volatile uint32_t *g_2353 = (void*)0;
static uint32_t g_2410[2][6][2] = {{{0x723FB8AFL,1UL},{4294967294UL,4294967294UL},{4294967294UL,1UL},{0x723FB8AFL,0UL},{1UL,0UL},{0x723FB8AFL,1UL}},{{4294967294UL,4294967294UL},{4294967294UL,1UL},{0x723FB8AFL,0UL},{1UL,0UL},{0x723FB8AFL,1UL},{4294967294UL,4294967294UL}}};
static int16_t g_2414 = (-1L);
static uint16_t g_2418 = 1UL;
static int32_t ** volatile g_2421[1] = {(void*)0};
static int32_t ** volatile g_2432 = &g_39[0];/* VOLATILE GLOBAL g_2432 */
static int32_t ** volatile g_2505 = &g_1057[0];/* VOLATILE GLOBAL g_2505 */
static int32_t g_2512 = (-10L);
static volatile uint8_t g_2558 = 252UL;/* VOLATILE GLOBAL g_2558 */
static const int32_t *g_2718 = (void*)0;
static const int32_t *g_2720 = &g_2512;
static const int32_t ** volatile g_2719[2] = {&g_2720,&g_2720};
static const int32_t ** volatile g_2722 = (void*)0;/* VOLATILE GLOBAL g_2722 */
static const uint8_t g_2773 = 0xEDL;
static uint16_t *g_2830 = &g_78;
static uint16_t ** const g_2829 = &g_2830;
static uint16_t ** const *g_2828 = &g_2829;
static int32_t ** volatile g_2859 = (void*)0;/* VOLATILE GLOBAL g_2859 */
static int32_t ** volatile g_2880 = &g_1057[7];/* VOLATILE GLOBAL g_2880 */
static int8_t **g_2917[9] = {&g_798,&g_798,&g_798,&g_798,&g_798,&g_798,&g_798,&g_798,&g_798};
static int8_t ** volatile * volatile g_2916 = &g_2917[4];/* VOLATILE GLOBAL g_2916 */
static float ****g_2946 = (void*)0;
static int8_t g_2955 = (-3L);
static uint16_t ***g_2989 = &g_406;
static uint16_t ****g_2988 = &g_2989;
static uint16_t *****g_2987[5] = {&g_2988,&g_2988,&g_2988,&g_2988,&g_2988};
static uint32_t g_3005 = 0x3040868EL;
static const uint16_t ***g_3072 = (void*)0;
static const uint16_t ****g_3071 = &g_3072;
static const uint16_t ****g_3074 = &g_3072;
static int32_t * volatile * volatile g_3096 = &g_182;/* VOLATILE GLOBAL g_3096 */
static int32_t * volatile * volatile g_3121 = &g_2076;/* VOLATILE GLOBAL g_3121 */
static int64_t g_3138[7] = {0xC6AFF327A650592ALL,0xC6AFF327A650592ALL,0xF2DFCBDE2ABB46FBLL,0xC6AFF327A650592ALL,0xC6AFF327A650592ALL,0xF2DFCBDE2ABB46FBLL,0xC6AFF327A650592ALL};


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static uint8_t  func_6(int16_t  p_7, int32_t  p_8);
static int32_t  func_9(const uint32_t  p_10, int32_t  p_11, int64_t  p_12);
static uint16_t  func_15(uint16_t  p_16, uint64_t  p_17, const uint32_t  p_18, const uint32_t  p_19);
static uint16_t  func_20(float  p_21, uint8_t  p_22);
static float  func_24(int8_t  p_25, int16_t  p_26);
static int64_t  func_42(uint32_t  p_43, uint8_t  p_44, uint32_t  p_45);
static float  func_59(int32_t  p_60, uint32_t  p_61, uint8_t  p_62, int8_t  p_63);
static uint32_t  func_65(int32_t ** p_66, int32_t * p_67, uint64_t * p_68);
static int32_t ** func_69(const int32_t  p_70, uint32_t  p_71, uint8_t  p_72, int32_t * p_73, uint32_t  p_74);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2558
 * writes:
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    int32_t l_3[1][6][8] = {{{0x4D63A88EL,0xBECCCE1EL,0xBECCCE1EL,0x4D63A88EL,(-2L),1L,0x4D63A88EL,1L},{0x4D63A88EL,0x923E64F1L,0L,0x923E64F1L,0x4D63A88EL,0L,6L,6L},{1L,0x923E64F1L,(-2L),(-2L),0x923E64F1L,1L,0xBECCCE1EL,0x923E64F1L},{6L,0xBECCCE1EL,(-2L),6L,(-2L),0xBECCCE1EL,6L,1L},{0x923E64F1L,0x4D63A88EL,0L,6L,6L,0L,0x4D63A88EL,0x923E64F1L},{1L,6L,0xBECCCE1EL,(-2L),6L,(-2L),0xBECCCE1EL,6L}}};
    float l_23 = 0x4.89F7A9p-15;
    uint64_t *l_32 = &g_33;
    uint8_t l_2231 = 254UL;
    int32_t l_2513 = 0xAD020B3AL;
    float l_2538 = 0x7.BFC2C2p+98;
    float l_2559[6] = {0x1.9p-1,0x2.3A8F00p-6,0x2.3A8F00p-6,0x1.9p-1,0x2.3A8F00p-6,0x2.3A8F00p-6};
    int64_t *l_2578 = (void*)0;
    uint32_t ** const *l_2587 = &g_1215;
    uint32_t ** const **l_2586[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t ** const ***l_2585 = &l_2586[0];
    int32_t l_2628 = 0xA60C50A9L;
    int16_t l_2658 = 0x335AL;
    const float l_2668 = (-0x1.Cp-1);
    float l_2726[6];
    int32_t l_2729 = 0x520A6631L;
    uint8_t l_2736 = 1UL;
    const float l_2756 = (-0x9.6p-1);
    float l_2801[3][7] = {{0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1},{0xE.DE4F1Fp-47,0x6.Ap+1,0xE.DE4F1Fp-47,0x6.Ap+1,0xE.DE4F1Fp-47,0x6.Ap+1,0xE.DE4F1Fp-47},{0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1,0x6.6p+1}};
    uint32_t l_2802[5][9] = {{18446744073709551615UL,18446744073709551615UL,0x2FFDAE55L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x2FFDAE55L,18446744073709551615UL,18446744073709551615UL},{1UL,18446744073709551615UL,9UL,0x2FFDAE55L,0UL,1UL,0x2FFDAE55L,2UL,0x2FFDAE55L},{0x2FFDAE55L,18446744073709551615UL,9UL,9UL,18446744073709551615UL,0x2FFDAE55L,1UL,2UL,9UL},{1UL,0UL,0x2FFDAE55L,9UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,9UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x2FFDAE55L,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551608UL,0x2FFDAE55L}};
    uint32_t l_2835 = 0xC63B7A9DL;
    uint32_t l_2845 = 0x18899B2EL;
    int16_t l_2854 = (-2L);
    union U0 l_2888 = {0UL};
    uint16_t *l_2899 = &g_78;
    int32_t *** const **l_2922[3];
    int32_t *l_2974[1];
    uint64_t *** const l_2980 = &g_586;
    uint64_t *** const *l_2979 = &l_2980;
    float l_3018 = 0x6.62E682p-44;
    int32_t **l_3065[9][6] = {{(void*)0,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,(void*)0,&g_1497,&g_1497,&g_1497,(void*)0},{&g_1497,&g_1497,&g_1497,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_1497,&g_1497},{&g_1497,(void*)0,&g_1497,&g_1497,&g_1497,(void*)0},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{(void*)0,(void*)0,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,(void*)0,(void*)0,(void*)0,(void*)0,&g_1497},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497}};
    uint16_t ****l_3078 = &g_2989;
    uint64_t l_3088 = 0x84AD7AA351F4C86ELL;
    int8_t l_3112 = 0xDCL;
    int8_t l_3156 = 0xD5L;
    int32_t l_3174[7] = {(-4L),0xECD91E67L,(-4L),(-4L),0xECD91E67L,(-4L),(-4L)};
    int16_t l_3188 = 0x8AB8L;
    uint32_t l_3194 = 0xD8A53432L;
    const uint32_t l_3203 = 0UL;
    uint16_t l_3205 = 0UL;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_2726[i] = 0x1.Ap-1;
    for (i = 0; i < 3; i++)
        l_2922[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_2974[i] = &g_703[0];
    return g_2558;
}


/* ------------------------------------------ */
/* 
 * reads : g_304 g_2432 g_892 g_703 g_798 g_167 g_777 g_133 g_407 g_78 g_1282 g_370 g_1074 g_122 g_1326 g_2035
 * writes: g_304 g_1057 g_39 g_892 g_195 g_78 g_2035 g_54
 */
static uint8_t  func_6(int16_t  p_7, int32_t  p_8)
{ /* block id: 1054 */
    int32_t *l_2423 = &g_304;
    int32_t *l_2424[1];
    int32_t l_2425 = 0L;
    int8_t l_2426 = 0x00L;
    float l_2427 = (-0x2.2p-1);
    uint16_t l_2428 = 0xDA53L;
    int32_t **l_2431 = &g_1057[7];
    int16_t l_2443 = 1L;
    uint16_t l_2455 = 0x92E8L;
    int i;
    for (i = 0; i < 1; i++)
        l_2424[i] = &g_118;
    --l_2428;
    (*l_2423) ^= p_8;
    (*g_2432) = ((*l_2431) = &p_8);
    for (g_892 = 0; (g_892 <= 7); g_892 += 1)
    { /* block id: 1061 */
        int32_t l_2444 = (-7L);
        int8_t *l_2445[5][1];
        uint32_t l_2456 = 18446744073709551613UL;
        int32_t *l_2457 = &g_304;
        int32_t l_2462[9][10] = {{1L,0x621D3B27L,(-1L),0x085524D1L,0x978D4368L,0x063194C8L,1L,0x428787CFL,0x063194C8L,0x16E1489CL},{1L,1L,0x7FF3C31EL,1L,1L,0L,0x16E1489CL,0L,1L,1L},{0L,0x16E1489CL,0L,1L,1L,0x7FF3C31EL,1L,1L,(-1L),0xE525C4B5L},{0x428787CFL,1L,0x063194C8L,0x978D4368L,0x085524D1L,(-1L),0x621D3B27L,1L,0x063194C8L,0x67079259L},{0x621D3B27L,0L,0L,0L,(-1L),(-1L),0L,0L,0L,0x621D3B27L},{0xE525C4B5L,0x428787CFL,0x7FF3C31EL,(-1L),0x67079259L,0x7C41DA49L,1L,0L,8L,0xB15D3C87L},{0x7C41DA49L,1L,0x428787CFL,0L,0x4C3B89E9L,0x428787CFL,0L,1L,0x67079259L,1L},{0x4C3B89E9L,0L,0x1DF26746L,0xE525C4B5L,0x1DF26746L,0L,0x4C3B89E9L,0L,0xB15D3C87L,0x4C3B89E9L},{8L,(-1L),0x7E97366CL,0x1DF26746L,(-1L),0xB15D3C87L,(-1L),0x7C41DA49L,0x063194C8L,0L}};
        uint64_t l_2465 = 0UL;
        uint32_t *****l_2496 = &g_1213;
        int i, j;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
                l_2445[i][j] = &l_2426;
        }
        if (((safe_sub_func_int16_t_s_s((*l_2423), ((safe_add_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((g_703[0] <= (safe_div_func_int8_t_s_s(((((*g_798) > (g_133 , (g_195 = (safe_div_func_int32_t_s_s(l_2443, l_2444))))) && (safe_lshift_func_uint16_t_u_u((255UL > (~(safe_mul_func_uint8_t_u_u(((safe_add_func_int16_t_s_s(p_8, ((*g_407)--))) , 255UL), l_2444)))), 1))) != l_2455), (*l_2423)))), l_2444)), p_8)) <= l_2456))) == p_7))
        { /* block id: 1064 */
            int32_t l_2461 = 0xC08E4460L;
            int32_t l_2464[7];
            int i;
            for (i = 0; i < 7; i++)
                l_2464[i] = 1L;
            g_1057[g_892] = l_2457;
            if (p_8)
            { /* block id: 1066 */
                (*l_2457) &= (p_7 , p_8);
            }
            else
            { /* block id: 1068 */
                int32_t l_2459 = 0xB2379CF2L;
                int32_t l_2460[2][5][10] = {{{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL}},{{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL},{(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL,(-1L),0L,(-1L),0xC66BF4AEL,0xC66BF4AEL}}};
                int i, j, k;
                if ((~l_2459))
                { /* block id: 1069 */
                    float l_2463[9] = {0x6.FD9D8Ap-22,0x0.26D569p-54,0x6.FD9D8Ap-22,0x0.26D569p-54,0x6.FD9D8Ap-22,0x0.26D569p-54,0x6.FD9D8Ap-22,0x0.26D569p-54,0x6.FD9D8Ap-22};
                    int i;
                    --l_2465;
                }
                else
                { /* block id: 1071 */
                    (*l_2431) = &p_8;
                }
            }
            l_2464[3] = (((void*)0 == &g_791) == (255UL == p_8));
            if ((*l_2423))
                break;
        }
        else
        { /* block id: 1077 */
            int32_t ***l_2471 = (void*)0;
            int16_t *l_2472 = &g_2035;
            int32_t l_2478 = 0L;
            int32_t l_2479 = (-1L);
            int32_t l_2480 = 0x302A0E08L;
            int32_t l_2483 = (-1L);
            int32_t l_2484 = 1L;
            int32_t l_2485 = 0L;
            int32_t l_2486 = 0x156B2362L;
            int32_t l_2487 = 0x0C22F5F7L;
            int32_t l_2488 = 0x073C2761L;
            int32_t l_2489 = 0xA8C9FFB0L;
            int32_t l_2490 = 0xB151787EL;
            int32_t l_2491[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            uint16_t l_2492 = 0UL;
            int i;
            if (((safe_sub_func_int32_t_s_s(((p_7 && (((*l_2472) = (+(&g_950 != l_2471))) || (safe_sub_func_int16_t_s_s(((+(((*l_2457) & (((*l_2457) > p_7) ^ (safe_lshift_func_uint16_t_u_u((((p_8 == (*l_2457)) , l_2445[0][0]) == (*g_1282)), 7)))) | (*g_1074))) != (*l_2457)), (-9L))))) < 0x20B9L), (*l_2423))) != (*l_2457)))
            { /* block id: 1079 */
                int32_t l_2481 = 0xED16A30CL;
                int32_t l_2482[4][9] = {{0xEA38CC61L,0xF96539C2L,0L,0xF96539C2L,0xEA38CC61L,0xEA38CC61L,0xF96539C2L,0L,0xF96539C2L},{0xF96539C2L,0x9F5B0F61L,0L,0L,0x9F5B0F61L,0xF96539C2L,0x9F5B0F61L,0L,0L},{0xEA38CC61L,0xEA38CC61L,0xF96539C2L,0L,0xF96539C2L,0xEA38CC61L,0xEA38CC61L,0xF96539C2L,0L},{(-1L),0x9F5B0F61L,(-1L),0xF96539C2L,0xF96539C2L,(-1L),0x9F5B0F61L,(-1L),0xF96539C2L}};
                int i, j;
                l_2492++;
            }
            else
            { /* block id: 1081 */
                uint32_t *****l_2495[4][8][5] = {{{&g_1213,(void*)0,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,(void*)0},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213}},{{&g_1213,&g_1213,&g_1213,&g_1213,(void*)0},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213}},{{&g_1213,&g_1213,(void*)0,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,(void*)0,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,(void*)0},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213}},{{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,(void*)0},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213},{&g_1213,&g_1213,&g_1213,&g_1213,&g_1213}}};
                int i, j, k;
                for (l_2444 = 3; (l_2444 >= 0); l_2444 -= 1)
                { /* block id: 1084 */
                    int i, j;
                    if (g_1326[l_2444][l_2444])
                    { /* block id: 1085 */
                        (*l_2431) = &p_8;
                    }
                    else
                    { /* block id: 1087 */
                        return p_7;
                    }
                    for (g_54 = 0; (g_54 <= 3); g_54 += 1)
                    { /* block id: 1092 */
                        l_2496 = (((*l_2472) |= 0x3C8EL) , l_2495[0][3][3]);
                    }
                }
            }
        }
        return (*l_2457);
    }
    return p_8;
}


/* ------------------------------------------ */
/* 
 * reads : g_2418 g_1056 g_182 g_118
 * writes: g_2418 g_1057
 */
static int32_t  func_9(const uint32_t  p_10, int32_t  p_11, int64_t  p_12)
{ /* block id: 1049 */
    int32_t *l_2412 = (void*)0;
    int32_t *l_2413[10][2] = {{(void*)0,&g_304},{&g_118,&g_304},{(void*)0,&g_304},{&g_118,&g_304},{(void*)0,&g_304},{&g_118,&g_304},{(void*)0,&g_304},{&g_118,&g_304},{(void*)0,&g_304},{&g_118,&g_304}};
    int32_t l_2415 = 6L;
    int32_t l_2416 = 0L;
    float l_2417[10] = {0x8.195563p-8,0xB.8D4A8Bp+62,0xB.8D4A8Bp+62,0x8.195563p-8,0xB.8D4A8Bp+62,0xB.8D4A8Bp+62,0x8.195563p-8,0xB.8D4A8Bp+62,0xB.8D4A8Bp+62,0x8.195563p-8};
    uint16_t l_2422 = 65535UL;
    int i, j;
    g_2418--;
    (*g_1056) = l_2412;
    l_2422 |= (*g_182);
    return p_12;
}


/* ------------------------------------------ */
/* 
 * reads : g_777 g_950 g_1887 g_589 g_739 g_461 g_1215 g_1905 g_1074 g_37 g_798 g_167 g_144 g_145 g_117 g_2278 g_320 g_179 g_1341 g_1342 g_1323 g_1324 g_1325 g_1326 g_33 g_122 g_407 g_78 g_1214 g_100 g_1191 g_1192 g_406 g_195 g_703 g_2353 g_409 g_1343 g_1344 g_1345 g_1007 g_812 g_301
 * writes: g_777 g_701 g_39 g_1057 g_461 g_589 g_122 g_118 g_2279 g_1343 g_33 g_2035 g_78 g_195 g_167 g_2410
 */
static uint16_t  func_15(uint16_t  p_16, uint64_t  p_17, const uint32_t  p_18, const uint32_t  p_19)
{ /* block id: 981 */
    int32_t *l_2236[2][2][4] = {{{&g_589[0][4][0],&g_589[0][2][2],&g_589[0][4][0],&g_589[0][4][0]},{&g_589[0][4][0],&g_589[0][4][0],&g_589[0][2][2],&g_589[0][4][0]}},{{&g_589[0][4][0],(void*)0,(void*)0,&g_589[0][4][0]},{(void*)0,&g_589[0][4][0],(void*)0,(void*)0}}};
    const int8_t l_2245[1][6] = {{3L,3L,3L,3L,3L,3L}};
    uint32_t l_2273 = 0xBB62769FL;
    int32_t l_2301 = 0L;
    float l_2328 = 0x6.740609p-89;
    uint32_t l_2334 = 0x14A35B98L;
    union U0 l_2343 = {4294967293UL};
    const int16_t *l_2407 = &g_2035;
    int16_t *l_2408 = &g_2035;
    uint32_t l_2409 = 1UL;
    uint8_t l_2411 = 0UL;
    int i, j, k;
    for (g_777 = 0; (g_777 != 18); g_777 = safe_add_func_uint64_t_u_u(g_777, 9))
    { /* block id: 984 */
        uint8_t l_2238 = 251UL;
        int32_t l_2270 = 0xA5C2F8D4L;
        const union U0 l_2280 = {4294967290UL};
        union U0 l_2283 = {0xD5BEFA1EL};
        int8_t **l_2297 = &g_798;
        int32_t l_2329 = 0x78B1D698L;
        int32_t l_2330 = 1L;
        int32_t l_2331 = 0xCD0C3B02L;
        int32_t l_2332 = 6L;
        int32_t l_2333[6][10][2] = {{{0xD168D339L,0xD168D339L},{(-7L),0x9C6BEA79L},{0x07BB088AL,0x0CDD5450L},{0x9C6BEA79L,6L},{(-4L),0x9C6BEA79L},{0xD168D339L,(-7L)},{0xD168D339L,0x9C6BEA79L},{(-4L),6L},{0x9C6BEA79L,0x0CDD5450L},{0x07BB088AL,0x9C6BEA79L}},{{(-7L),0xD168D339L},{0xD168D339L,7L},{0x07BB088AL,6L},{7L,6L},{0x07BB088AL,7L},{0xD168D339L,0xD168D339L},{(-7L),0x9C6BEA79L},{0x07BB088AL,0x0CDD5450L},{0x9C6BEA79L,6L},{(-4L),0x9C6BEA79L}},{{0xD168D339L,(-7L)},{0xD168D339L,0x9C6BEA79L},{(-4L),6L},{0x9C6BEA79L,0x0CDD5450L},{0x07BB088AL,0x9C6BEA79L},{(-7L),0xD168D339L},{0xD168D339L,7L},{0x07BB088AL,6L},{7L,6L},{0x07BB088AL,7L}},{{0xD168D339L,0xD168D339L},{(-7L),0x9C6BEA79L},{0x07BB088AL,0x0CDD5450L},{0x9C6BEA79L,6L},{(-4L),0x9C6BEA79L},{0xD168D339L,(-7L)},{0xD168D339L,0x9C6BEA79L},{(-4L),6L},{0x9C6BEA79L,0x0CDD5450L},{0x07BB088AL,0x9C6BEA79L}},{{(-7L),0xD168D339L},{0xD168D339L,7L},{0x07BB088AL,6L},{7L,6L},{0x07BB088AL,7L},{0xD168D339L,0xD168D339L},{(-7L),0x9C6BEA79L},{0x07BB088AL,0x0CDD5450L},{0x9C6BEA79L,6L},{(-4L),0x9C6BEA79L}},{{0xD168D339L,(-7L)},{0xD168D339L,0x9C6BEA79L},{(-4L),6L},{0x9C6BEA79L,0x0CDD5450L},{0x07BB088AL,0x9C6BEA79L},{(-7L),0xD168D339L},{0xD168D339L,7L},{0x07BB088AL,6L},{7L,6L},{0x07BB088AL,7L}}};
        int i, j, k;
        for (g_701 = 0; (g_701 <= 26); g_701++)
        { /* block id: 987 */
            int32_t **l_2237 = &g_1057[4];
            int8_t * const l_2284 = &g_195;
            int32_t l_2324[5] = {1L,1L,1L,1L,1L};
            int32_t *l_2366 = &l_2301;
            int i;
            (*l_2237) = ((*g_950) = l_2236[1][1][3]);
            --l_2238;
            for (g_461 = (-16); (g_461 == 6); g_461 = safe_add_func_uint64_t_u_u(g_461, 1))
            { /* block id: 993 */
                uint32_t l_2269[1][5];
                int32_t l_2274 = 4L;
                float l_2300 = (-0x1.9p+1);
                float l_2308 = 0xC.5ADF6Fp-46;
                int32_t l_2325 = 0xCAB5B233L;
                int32_t l_2326 = 9L;
                int8_t *l_2344 = &g_167;
                uint8_t *l_2345[7] = {&l_2238,&g_54,&l_2238,&l_2238,&g_54,&l_2238,&l_2238};
                const union U0 l_2352 = {0x030246D1L};
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 5; j++)
                        l_2269[i][j] = 0x2EA0FFA5L;
                }
                if ((safe_add_func_uint64_t_u_u(p_17, l_2238)))
                { /* block id: 994 */
                    uint16_t l_2246 = 1UL;
                    int8_t *l_2271[6] = {&g_167,&g_167,&g_195,&g_167,&g_167,&g_195};
                    int32_t l_2272 = 0x7CA9EFDAL;
                    int i;
                    (*g_1887) |= (p_16 & (l_2245[0][2] | p_16));
                    if (l_2246)
                        continue;
                    l_2274 = ((safe_sub_func_int32_t_s_s(((safe_add_func_int32_t_s_s(((p_17 , ((safe_rshift_func_int8_t_s_s((((0UL != ((~(safe_div_func_uint16_t_u_u(((((safe_rshift_func_int16_t_s_s(((((*g_1074) = (safe_div_func_uint16_t_u_u((((safe_sub_func_int8_t_s_s((p_18 <= (safe_mod_func_uint32_t_u_u(p_17, (((!(249UL > (l_2272 &= (((safe_sub_func_int32_t_s_s((safe_rshift_func_int16_t_s_u((l_2238 > ((*g_739) <= (l_2269[0][0] <= (l_2270 = (0x2519D3C8F421748BLL | l_2246))))), 10)), (**g_1215))) , (*g_739)) && 0xBD40B49DL)))) ^ 0UL) , (**g_1215))))), p_16)) , g_1905) ^ p_18), p_17))) | l_2246) > p_19), 6)) , (void*)0) != &l_2269[0][0]) , l_2273), g_37))) == (-2L))) != p_17) , (*g_798)), 1)) || p_16)) > p_19), 0UL)) > p_19), l_2269[0][0])) <= (*g_144));
                    l_2274 = ((*g_117) = (-7L));
                }
                else
                { /* block id: 1003 */
                    uint8_t l_2298[1];
                    uint64_t *l_2299 = &g_33;
                    float l_2322 = 0xA.86E60Ep+26;
                    int32_t l_2323 = 4L;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_2298[i] = 0x35L;
                    (*l_2237) = &l_2270;
                    for (l_2273 = (-6); (l_2273 == 1); l_2273 = safe_add_func_uint16_t_u_u(l_2273, 7))
                    { /* block id: 1007 */
                        (*g_2278) = &g_1074;
                    }
                    if (((l_2280 , ((l_2280.f0 || (((*g_320) != (safe_add_func_float_f_f(((**g_1341) = (l_2283 , l_2274)), ((void*)0 == l_2284)))) , (0xD2717F4039EE491BLL ^ ((safe_add_func_uint16_t_u_u(((safe_div_func_uint32_t_u_u((safe_div_func_int8_t_s_s((((safe_div_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((((*l_2299) |= (((safe_rshift_func_uint8_t_u_u(((((((***g_1323) , &g_798) != l_2297) > l_2270) >= 0xFA8FL) < (-1L)), 7)) | 0x4A3A6D7DL) , l_2298[0])) , (-9L)), 5)), (*g_1074))) , 8UL) > p_16), l_2298[0])), 0x12F8BA79L)) , p_17), p_17)) != (**g_1215))))) < 7L)) == l_2301))
                    { /* block id: 1012 */
                        return (*g_407);
                    }
                    else
                    { /* block id: 1014 */
                        uint16_t l_2302 = 0xBF68L;
                        int32_t l_2305 = 0x8FF05C8AL;
                        int16_t *l_2319 = &g_2035;
                        int32_t l_2327[5] = {0L,0L,0L,0L,0L};
                        int i;
                        ++l_2302;
                        l_2305 = p_16;
                        l_2274 = (((p_16 | ((*g_117) = ((safe_rshift_func_uint16_t_u_s(l_2302, 3)) , (safe_mod_func_int32_t_s_s((safe_mod_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(p_19, p_19)), (safe_div_func_int8_t_s_s(6L, (safe_unary_minus_func_int64_t_s(l_2270)))))), (+p_18)))))) <= (((*l_2319) = g_78) || (((((safe_rshift_func_int16_t_s_s(g_37, 3)) , g_122[3]) , (***g_1214)) , p_17) <= g_37))) <= (-1L));
                        ++l_2334;
                    }
                    (*l_2237) = &l_2274;
                }
                if (((g_100 , (((*l_2297) == &l_2245[0][2]) <= ((****g_1191) = p_19))) < (l_2333[4][3][0] = (safe_mul_func_uint16_t_u_u(((p_19 > (((*l_2344) = ((safe_mul_func_int8_t_s_s(l_2333[1][4][0], (safe_sub_func_int64_t_s_s((((*l_2284) |= (l_2343 , (-1L))) ^ 251UL), 0xA4CC697D3C6209D1LL)))) != p_17)) >= p_18)) == 0x76CAL), 0xE457L)))))
                { /* block id: 1028 */
                    int16_t *l_2360 = (void*)0;
                    float l_2363 = 0x9.D0DECCp+57;
                    int32_t l_2364[6][4][9] = {{{0x529F704EL,1L,(-6L),1L,0x40B0BE37L,(-1L),1L,1L,1L},{0xF2A897AEL,7L,1L,0x83723A68L,0xBEC01999L,0xA2A6CCB9L,0x78119176L,0x3E1635C7L,(-1L)},{1L,1L,(-9L),(-1L),0x3E1635C7L,0x78119176L,0xA2A6CCB9L,0xBEC01999L,0x83723A68L},{0x36DD0C68L,7L,0x529F704EL,(-5L),0xA58BC828L,(-6L),(-1L),0xF2A897AEL,(-6L)}},{{1L,0x9B9E4C78L,0x33BDD32EL,(-5L),0x36573A9DL,(-1L),1L,0x78119176L,0x772AFA6FL},{(-9L),0x36DD0C68L,(-8L),(-1L),0x9B9E4C78L,0xE03F5253L,0x33BDD32EL,1L,0xB04C19E5L},{0xB04C19E5L,(-1L),0xC5DB116DL,(-9L),1L,(-1L),0xFEE94D8EL,5L,0xB8A50C05L},{0xC6F71F75L,5L,0L,(-6L),0xF2F1483AL,(-6L),0L,5L,0xC6F71F75L}},{{0L,0xA58BC828L,0x3188522CL,0x772AFA6FL,0x914191B5L,0xCB638BEBL,1L,1L,0xFEE94D8EL},{0x3188522CL,0x102BB184L,0xCB638BEBL,0xB04C19E5L,1L,(-1L),0xD84AA603L,0x78119176L,0x03195652L},{0L,0x914191B5L,1L,0xB8A50C05L,0xA2BCBDF4L,(-1L),0x03195652L,0xF2A897AEL,0xCB638BEBL},{0xC6F71F75L,0x36573A9DL,1L,0xC6F71F75L,0x102BB184L,1L,0L,1L,0xE03F5253L}},{{0xB04C19E5L,0xA2A6CCB9L,0xCB638BEBL,0xFEE94D8EL,0xCB78A07DL,0x33BDD32EL,0x02797321L,0x97F755CBL,0xB5C891F8L},{(-9L),0L,0x3188522CL,0x03195652L,0x102BB184L,0xFEE94D8EL,0x30BC1044L,0xA58BC828L,0L},{1L,0x83723A68L,0L,0xCB638BEBL,0xA2BCBDF4L,0L,0x30BC1044L,0x36573A9DL,(-1L)},{0x33BDD32EL,1L,0xC5DB116DL,0xE03F5253L,1L,1L,0x02797321L,0x9B9E4C78L,9L}},{{0xE03F5253L,0L,(-8L),0xB5C891F8L,0x914191B5L,0xD84AA603L,0L,1L,(-1L)},{9L,(-9L),0x33BDD32EL,0L,0xF2F1483AL,0x03195652L,0x03195652L,0xF2F1483AL,0L},{(-1L),(-9L),(-1L),(-1L),1L,0L,0xD84AA603L,0x914191B5L,0xB5C891F8L},{0x03195652L,0L,0xC6F71F75L,9L,0x9B9E4C78L,0x02797321L,1L,1L,0xE03F5253L}},{{0x02797321L,1L,(-6L),(-1L),0x36573A9DL,0x30BC1044L,0L,0xA2BCBDF4L,0xCB638BEBL},{0xFEE94D8EL,0x83723A68L,0xB5C891F8L,0L,0xA58BC828L,0x30BC1044L,0xFEE94D8EL,0x102BB184L,0x03195652L},{0xC5DB116DL,0L,9L,0xB5C891F8L,0x97F755CBL,0x02797321L,0x33BDD32EL,0xCB78A07DL,0xFEE94D8EL},{(-5L),0xA2A6CCB9L,0x30BC1044L,0xE03F5253L,1L,0L,1L,0x102BB184L,0xC6F71F75L}}};
                    int32_t l_2365 = 0x39FB3832L;
                    int i, j, k;
                    l_2324[0] |= (l_2325 = (safe_sub_func_int32_t_s_s(((((((((((p_18 > (safe_div_func_uint64_t_u_u((safe_mod_func_uint32_t_u_u(1UL, ((l_2352 , (g_703[0] , (g_2353 == (**g_1214)))) , (((l_2365 |= (safe_mod_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(p_16, (l_2270 = (safe_mul_func_int16_t_s_s((g_2035 = l_2274), ((((safe_sub_func_uint16_t_u_u(0x4819L, p_18)) , 0x189EFAF6FF180447LL) >= 0xDDAFE078D04C5E4BLL) , (***g_409))))))), l_2364[1][2][6]))) & p_19) & 0x0ADCB03EBDBDEA83LL)))), 0xEA0E9085B8020A6BLL))) & p_16) , (void*)0) == l_2366) | l_2364[1][1][6]) || p_18) , p_17) > p_19) && l_2333[1][4][0]) <= l_2364[2][3][4]), p_16)));
                }
                else
                { /* block id: 1034 */
                    float *l_2369 = &l_2308;
                    int32_t ****l_2375 = &g_1620;
                    int32_t *****l_2374 = &l_2375;
                    int16_t *l_2376 = &g_2035;
                    float l_2377 = 0xF.38EDEDp-43;
                    int32_t l_2378[10] = {2L,2L,2L,2L,2L,2L,2L,2L,2L,2L};
                    int i;
                    (**g_1341) = (safe_add_func_float_f_f((-0x1.2p+1), ((((*g_1342) <= ((p_16 < ((((*l_2369) = 0x8.5p-1) >= p_19) == (safe_div_func_float_f_f(l_2238, ((((*l_2376) = (safe_lshift_func_uint16_t_u_s((0x7EL > (((*l_2374) = &g_1620) != (void*)0)), g_33))) , l_2378[3]) , 0x7.9p+1))))) > (**g_1344))) < (*g_1007)) >= 0x7.610E74p+15)));
                }
            }
            (*l_2237) = l_2236[1][0][2];
        }
        (*g_1342) = (((((p_17 , (p_17 | ((1UL ^ (*g_798)) , (((safe_sub_func_uint64_t_u_u(((safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u(0xEFL, 0)), l_2283.f0)) , 1UL), (((p_17 < l_2329) | p_19) , l_2332))) , p_17) , g_812[0][0][4])))) , 0xDE72FA70L) & p_18) < g_301) , 0x4.0841C1p-80);
    }
    l_2411 ^= (0x73E6FF05L && (safe_add_func_int64_t_s_s(0x71797351A84ED831LL, (g_2410[1][0][1] = (((*g_798) & (safe_add_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u((safe_div_func_int64_t_s_s((p_17 | (safe_mod_func_uint8_t_u_u((safe_add_func_int32_t_s_s(0xD5ADA9CFL, (safe_mod_func_int64_t_s_s(p_17, ((*g_1074) ^= (((((l_2343 , (-((-((((safe_mul_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(p_18, 0x008311E2F1326CC6LL)), p_16)) , p_19) , (*g_1345)) < (*g_1007))) <= (-0x1.1p+1)))) != 0xC.3EBD93p-2) > p_16) , l_2407) == l_2408)))))), l_2409))), p_16)), p_19)), p_17)), p_18))) && p_19)))));
    return p_19;
}


/* ------------------------------------------ */
/* 
 * reads : g_798 g_167 g_777 g_358 g_1620 g_701 g_122 g_54 g_33 g_1007 g_179 g_1342 g_304 g_1074 g_118 g_1191 g_1192 g_406 g_407 g_78 g_739 g_461 g_1380 g_1215 g_50 g_369 g_370 g_1717 g_314 g_1214 g_195 g_1742 g_1802 g_301 g_1213 g_589 g_1887 g_1905 g_1160 g_230 g_1326 g_425 g_1282 g_117 g_703 g_409 g_1281 g_144 g_145 g_2043 g_37 g_1770 g_2067 g_2076 g_182 g_585 g_586 g_2129 g_29 g_2035 g_397 g_2185 g_1344 g_1345 g_1343 g_791 g_1060 g_133.f1
 * writes: g_304 g_39 g_37 g_1620 g_701 g_703 g_589 g_54 g_33 g_1343 g_118 g_301 g_195 g_461 g_122 g_1704 g_1717 g_78 g_314 g_1057 g_167 g_777 g_1380 g_1770 g_230 g_1905 g_425 g_1615 g_370 g_2035 g_133.f1 g_1160.f1 g_791
 */
static uint16_t  func_20(float  p_21, uint8_t  p_22)
{ /* block id: 647 */
    int32_t l_1612 = (-1L);
    int32_t l_1614 = (-1L);
    int32_t l_1616 = 7L;
    float *l_1653 = &g_1615;
    float ** const l_1652 = &l_1653;
    float ** const *l_1651 = &l_1652;
    int32_t *** const *l_1670[10] = {&g_1620,&g_1620,&g_1620,(void*)0,(void*)0,&g_1620,&g_1620,&g_1620,(void*)0,(void*)0};
    uint8_t * const l_1711 = &g_54;
    uint32_t **l_1740 = (void*)0;
    int32_t l_1760[2][5][6] = {{{0x720D2997L,0x720D2997L,0L,0x2FFD238AL,(-5L),0xA9EDF9D7L},{0x3FDB9446L,0x57B878CEL,1L,(-1L),0xD7C98413L,0L},{0xF11424E2L,0x3FDB9446L,1L,(-1L),0x720D2997L,0xA9EDF9D7L},{0x153CAABCL,(-1L),0L,1L,(-6L),0x57B878CEL},{1L,(-6L),0x57B878CEL,0xC15584A7L,0x57B878CEL,(-6L)}},{{0L,(-1L),0x247EF602L,0x0B63B79CL,0xA9EDF9D7L,0x720D2997L},{0xA9EDF9D7L,0xF11424E2L,0x15BBE763L,0x12BA217FL,0L,0xD7C98413L},{0x3547B876L,0xF11424E2L,0x0B63B79CL,0xAED93FA3L,0xA9EDF9D7L,(-5L)},{0xC15584A7L,(-1L),0x35B000C6L,0xA9EDF9D7L,0x57B878CEL,0x3547B876L},{(-1L),(-6L),0xD7C98413L,0xD7C98413L,(-6L),(-1L)}}};
    int8_t **l_1784 = &g_798;
    uint64_t l_1822 = 0xE2225C86807AD70ELL;
    uint32_t ***l_1836[3];
    uint64_t l_1869 = 1UL;
    uint16_t **l_1878[1];
    union U0 l_1900 = {1UL};
    uint32_t l_2132[3][5][6] = {{{0x496849E2L,1UL,7UL,7UL,1UL,0x496849E2L},{7UL,1UL,0x496849E2L,0UL,0x0AFE9055L,4294967295UL},{4294967295UL,1UL,1UL,1UL,4294967295UL,1UL},{4294967295UL,7UL,1UL,0UL,0xBBDBC3FBL,0xBBDBC3FBL},{7UL,0x0AFE9055L,0x0AFE9055L,7UL,1UL,0xBBDBC3FBL}},{{0x496849E2L,0xBBDBC3FBL,1UL,1UL,0UL,1UL},{1UL,0xC4C55937L,1UL,0xC97C2C83L,0UL,4294967295UL},{1UL,0xBBDBC3FBL,0x496849E2L,1UL,1UL,0x496849E2L},{0x0AFE9055L,0x0AFE9055L,7UL,1UL,0xBBDBC3FBL,0xC97C2C83L},{1UL,7UL,4294967295UL,0xC97C2C83L,4294967295UL,7UL}},{{1UL,1UL,4294967295UL,1UL,0x0AFE9055L,0xC97C2C83L},{0x496849E2L,1UL,7UL,7UL,1UL,0x496849E2L},{7UL,1UL,0x496849E2L,0UL,0x0AFE9055L,4294967295UL},{4294967295UL,1UL,1UL,1UL,4294967295UL,1UL},{4294967295UL,7UL,1UL,0UL,0xBBDBC3FBL,0xBBDBC3FBL}}};
    uint8_t ****l_2137[3][3][9] = {{{(void*)0,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,(void*)0,(void*)0,(void*)0},{&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,(void*)0},{(void*)0,&g_1281,(void*)0,&g_1281,&g_1281,(void*)0,&g_1281,&g_1281,&g_1281}},{{&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,(void*)0,&g_1281,&g_1281},{&g_1281,&g_1281,(void*)0,(void*)0,&g_1281,(void*)0,(void*)0,&g_1281,&g_1281},{&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,(void*)0,&g_1281,(void*)0,&g_1281}},{{&g_1281,(void*)0,(void*)0,&g_1281,&g_1281,&g_1281,&g_1281,(void*)0,(void*)0},{&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,&g_1281,(void*)0,&g_1281},{&g_1281,&g_1281,&g_1281,&g_1281,(void*)0,&g_1281,(void*)0,&g_1281,&g_1281}}};
    float l_2163 = 0x1.Ap+1;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1836[i] = &g_1215;
    for (i = 0; i < 1; i++)
        l_1878[i] = &g_407;
lbl_2093:
    if ((safe_mul_func_int8_t_s_s((*g_798), (*g_798))))
    { /* block id: 648 */
        int8_t l_1601 = 0xACL;
        int32_t l_1610 = 0xAEEAE21EL;
        int32_t l_1613 = 2L;
        uint64_t l_1617 = 8UL;
lbl_1622:
        for (g_304 = (-19); (g_304 >= 23); g_304 = safe_add_func_int32_t_s_s(g_304, 7))
        { /* block id: 651 */
            int32_t *l_1583[5] = {&g_703[0],&g_703[0],&g_703[0],&g_703[0],&g_703[0]};
            int32_t **l_1584 = &l_1583[1];
            int i;
            (*g_358) = ((*l_1584) = l_1583[1]);
        }
        for (g_37 = 0; (g_37 < 2); ++g_37)
        { /* block id: 657 */
            int32_t *l_1588 = (void*)0;
            int32_t *l_1589 = &g_703[0];
            int32_t *l_1590 = &g_703[0];
            int32_t *l_1591 = &g_589[1][4][2];
            int32_t *l_1592 = &g_118;
            int32_t *l_1593 = &g_118;
            int32_t *l_1594 = &g_304;
            int32_t *l_1595 = &g_304;
            int32_t *l_1596 = &g_118;
            int32_t *l_1597 = &g_703[0];
            int32_t *l_1598 = &g_304;
            int32_t *l_1599 = &g_703[0];
            int32_t *l_1600 = &g_314;
            int32_t l_1602 = 0xD60BEAE2L;
            int32_t *l_1603 = (void*)0;
            int32_t *l_1604 = (void*)0;
            int32_t *l_1605 = &g_589[0][2][2];
            int32_t *l_1606 = &l_1602;
            int32_t *l_1607 = &g_314;
            int32_t *l_1608 = &g_118;
            int32_t *l_1609 = &g_703[0];
            int32_t *l_1611[10][2][6] = {{{&g_703[0],&g_703[0],&g_37,&g_37,&g_703[0],&g_703[0]},{&l_1602,&g_703[0],&g_118,&g_703[0],&g_118,&g_703[0]}},{{&g_118,&l_1602,&g_37,&g_304,&g_304,&g_37},{&g_118,&g_118,&g_304,&g_703[0],&g_589[0][2][2],&g_703[0]}},{{&l_1602,&g_118,&l_1602,&g_37,&g_304,&g_304},{&g_703[0],&l_1602,&l_1602,&g_703[0],&g_118,&g_703[0]}},{{&g_703[0],&g_703[0],&g_304,&g_703[0],&g_703[0],&g_37},{&g_703[0],&g_703[0],&g_37,&g_37,&g_703[0],&g_703[0]}},{{&l_1602,&g_703[0],&g_118,&g_703[0],&g_118,&g_703[0]},{&g_118,&l_1602,&g_37,&g_304,&g_304,&g_37}},{{&g_118,&g_118,&g_304,&g_703[0],&g_589[0][2][2],&g_703[0]},{&l_1602,&g_118,&l_1602,&g_37,&g_304,&g_304}},{{&g_703[0],&l_1602,&l_1602,&g_703[0],&g_118,&g_703[0]},{&g_703[0],&g_703[0],&g_304,&g_703[0],&g_703[0],&g_37}},{{&g_703[0],&g_703[0],&g_37,&g_37,&g_703[0],&g_703[0]},{&l_1602,&g_703[0],&g_118,&g_703[0],&g_118,&g_703[0]}},{{&g_118,&l_1602,&g_37,&g_304,&g_304,&g_37},{&g_118,&g_118,&g_304,&g_703[0],&g_589[0][2][2],&g_703[0]}},{{&l_1602,&g_118,&l_1602,&g_37,&g_304,&g_304},{&g_703[0],&l_1602,&l_1602,&g_703[0],&g_118,&g_703[0]}}};
            int i, j, k;
            l_1617--;
            if (p_22)
            { /* block id: 659 */
                int32_t ****l_1621 = &g_1620;
                int32_t l_1654 = 0x3AAFB01FL;
                (*l_1606) ^= (p_22 <= (((*l_1621) = g_1620) != (void*)0));
                for (l_1602 = 0; (l_1602 <= 1); l_1602 += 1)
                { /* block id: 664 */
                    int64_t l_1626 = 0x9F96E36CA8229406LL;
                    int32_t l_1627 = 0x0B86FECFL;
                    if (g_777)
                        goto lbl_1622;
                    for (g_701 = 2; (g_701 <= 6); g_701 += 1)
                    { /* block id: 668 */
                        uint8_t *l_1625[7][10][3] = {{{&g_54,&g_50,&g_54},{&g_54,(void*)0,&g_54},{&g_547,&g_547,&g_547},{(void*)0,(void*)0,&g_50},{&g_547,&g_547,(void*)0},{&g_50,(void*)0,&g_547},{&g_50,&g_547,&g_547},{&g_54,(void*)0,&g_54},{&g_54,&g_547,&g_54},{(void*)0,(void*)0,(void*)0}},{{&g_547,&g_54,&g_547},{&g_54,&g_54,&g_54},{&g_547,&g_50,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_50,&g_547},{(void*)0,&g_54,(void*)0},{&g_54,&g_50,&g_54},{&g_54,&g_50,&g_54},{&g_50,&g_54,&g_547},{&g_54,&g_54,&g_547}},{{&g_50,&g_54,(void*)0},{&g_54,&g_50,&g_50},{&g_54,&g_547,&g_54},{(void*)0,&g_54,&g_54},{&g_54,&g_547,&g_54},{&g_54,&g_54,(void*)0},{&g_547,&g_547,&g_50},{&g_54,&g_54,&g_54},{&g_547,&g_547,&g_547},{(void*)0,&g_50,&g_54}},{{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_50,&g_54,&g_54},{&g_50,&g_50,&g_54},{&g_547,&g_50,&g_547},{(void*)0,&g_54,&g_54},{&g_54,&g_50,&g_50},{&g_54,&g_54,(void*)0},{&g_547,&g_50,&g_54},{&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54},{(void*)0,(void*)0,&g_50},{&g_547,&g_547,(void*)0},{&g_50,(void*)0,&g_547},{&g_50,&g_547,&g_547},{&g_54,(void*)0,&g_54},{&g_54,&g_547,&g_54},{(void*)0,(void*)0,(void*)0},{&g_547,&g_54,&g_547},{&g_54,&g_54,&g_54}},{{&g_547,&g_50,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_50,&g_547},{(void*)0,&g_54,(void*)0},{&g_54,&g_50,&g_54},{&g_54,&g_50,&g_54},{&g_50,&g_54,&g_547},{&g_54,&g_54,&g_547},{&g_50,&g_54,(void*)0},{&g_54,&g_50,&g_50}},{{&g_54,&g_547,&g_54},{(void*)0,&g_54,&g_54},{&g_54,&g_547,&g_54},{&g_54,&g_54,(void*)0},{&g_547,&g_547,&g_50},{&g_54,&g_54,&g_54},{&g_547,&g_547,&g_54},{&g_54,(void*)0,&g_54},{&g_547,(void*)0,&g_50},{&g_54,&g_50,&g_54}}};
                        int64_t l_1643 = 0x38CEDC30B3A81CCDLL;
                        uint64_t *l_1650 = &g_33;
                        int i, j, k;
                        (*l_1597) = (-7L);
                        (*l_1605) = g_122[g_701];
                        (*l_1596) |= ((((safe_sub_func_uint8_t_u_u((--g_54), g_122[g_701])) >= 0xF62DL) | (safe_mod_func_uint32_t_u_u((~((((((((p_22++) <= g_122[l_1602]) ^ (safe_add_func_int64_t_s_s(((((*g_1342) = ((safe_sub_func_uint16_t_u_u((safe_sub_func_int32_t_s_s(0x388008F8L, (l_1643 >= (1L >= ((*l_1650) &= (g_122[g_701] != (safe_mul_func_int64_t_s_s(l_1612, ((safe_add_func_uint32_t_u_u((safe_div_func_uint8_t_u_u(l_1617, l_1613)), 0x24964621L)) < 7L))))))))), l_1627)) , (*g_1007))) , (*l_1598)) , (*g_1074)), l_1612))) , &g_777) != (void*)0) , l_1651) == &g_1341) , l_1601)), l_1601))) < l_1654);
                        return (****g_1191);
                    }
                    if (l_1613)
                        break;
                }
            }
            else
            { /* block id: 680 */
                const int32_t l_1655 = 1L;
                if (l_1655)
                    break;
                for (g_301 = 19; (g_301 != (-8)); g_301 = safe_sub_func_int32_t_s_s(g_301, 3))
                { /* block id: 684 */
                    uint32_t l_1658 = 7UL;
                    l_1658++;
                    if (l_1655)
                        continue;
                }
            }
            return l_1617;
        }
    }
    else
    { /* block id: 691 */
        int16_t l_1680 = 0L;
        float **l_1686 = &l_1653;
        float ***l_1685 = &l_1686;
        float ****l_1684[7];
        uint16_t l_1702 = 0x0FAEL;
        float l_1743[10] = {0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18,0x5.0488B4p+18};
        int32_t l_1752 = 0xA9496FE0L;
        int32_t l_1769 = 0L;
        int32_t l_1771[5] = {9L,9L,9L,9L,9L};
        int64_t l_1772 = 0x5D97186F4D086E2FLL;
        int32_t * const *l_1925 = &g_1497;
        int32_t * const **l_1924 = &l_1925;
        int32_t * const ***l_1923 = &l_1924;
        uint64_t l_1938[5][8] = {{18446744073709551615UL,18446744073709551615UL,0x0B53E8199E078887LL,1UL,0x6386B5A11F4AAF94LL,0x0B53E8199E078887LL,0x6386B5A11F4AAF94LL,1UL},{0x5294307BB4699651LL,1UL,0x5294307BB4699651LL,0xD3C6BCE4B3DC3294LL,1UL,0xADC6DCCDAFB0DC79LL,0xADC6DCCDAFB0DC79LL,1UL},{1UL,0xADC6DCCDAFB0DC79LL,0xADC6DCCDAFB0DC79LL,1UL,0xD3C6BCE4B3DC3294LL,0x5294307BB4699651LL,1UL,0x5294307BB4699651LL},{1UL,0x6386B5A11F4AAF94LL,0x0B53E8199E078887LL,0x6386B5A11F4AAF94LL,1UL,0x0B53E8199E078887LL,18446744073709551615UL,18446744073709551615UL},{0x5294307BB4699651LL,0x6386B5A11F4AAF94LL,0xD3C6BCE4B3DC3294LL,0xD3C6BCE4B3DC3294LL,0x6386B5A11F4AAF94LL,0x5294307BB4699651LL,0xADC6DCCDAFB0DC79LL,0x6386B5A11F4AAF94LL}};
        int32_t *l_1947 = &g_589[0][2][2];
        int i, j;
        for (i = 0; i < 7; i++)
            l_1684[i] = &l_1685;
        for (g_195 = (-26); (g_195 <= (-8)); g_195++)
        { /* block id: 694 */
            int32_t *** const **l_1671 = (void*)0;
            int32_t *** const **l_1672 = &l_1670[7];
            union U0 l_1678 = {1UL};
            uint8_t *** const *l_1683 = &g_1281;
            uint8_t *l_1701 = &g_54;
            uint32_t *****l_1703 = &g_1213;
            int32_t l_1753 = (-1L);
            int32_t l_1754 = 0xB9F56F14L;
            int32_t l_1755 = 0x1BC4C279L;
            int32_t l_1757 = 0xDD71D137L;
            int32_t l_1758[5] = {3L,3L,3L,3L,3L};
            int32_t *l_1801 = &g_118;
            int16_t l_1812 = 0xC874L;
            int32_t l_1831 = 0xF76CFFF2L;
            int32_t *l_1927 = &l_1614;
            int32_t *l_1928 = &g_304;
            int32_t *l_1929 = (void*)0;
            int32_t *l_1930 = &l_1616;
            int32_t *l_1931 = (void*)0;
            int32_t *l_1932 = &g_314;
            int32_t *l_1933 = &g_703[0];
            int32_t *l_1934 = &l_1769;
            int32_t *l_1935 = &g_314;
            int32_t *l_1936 = &l_1758[3];
            int32_t *l_1937[3][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
            int i, j;
            l_1680 &= (l_1614 &= (((safe_add_func_int16_t_s_s(p_22, (+((safe_sub_func_uint16_t_u_u(p_22, (p_22 >= ((safe_div_func_uint32_t_u_u((&g_1323 == ((*l_1672) = l_1670[1])), (((((~0xF8D87D2BL) >= (safe_lshift_func_uint16_t_u_u(p_22, (safe_lshift_func_int8_t_s_s(((l_1678 , l_1612) || 0x48L), 7))))) || p_22) == 0x7F6FC280A2EAED89LL) , p_22))) , 0xF48A235AL)))) < p_22)))) , 0x876F8608D4350FA2LL) , 0x7895C237L));
            if (((((void*)0 != l_1683) > (l_1684[2] == (void*)0)) == (safe_rshift_func_int8_t_s_u((safe_sub_func_int16_t_s_s((l_1680 | ((0x2C81L || (0x5F41L >= 0L)) <= (l_1616 = (((safe_mod_func_uint8_t_u_u(((*l_1701) = (((((safe_sub_func_int16_t_s_s((((*g_739) &= (safe_rshift_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((p_22 || 0x72L), l_1616)), p_22))) , 0xB315L), p_22)) <= p_22) , l_1612) ^ g_1380[2]) <= 0x0BL)), l_1702)) >= p_22) == p_22)))), l_1678.f0)), p_22))))
            { /* block id: 701 */
                int64_t *l_1716 = &g_1717;
                uint32_t *****l_1718 = (void*)0;
                uint64_t ***l_1749 = &g_586;
                int32_t l_1756 = 0L;
                int32_t l_1762 = 0xCD1CF16EL;
                int32_t l_1763 = 0x44AB1AD1L;
                int32_t l_1764 = 0x0ED4DA63L;
                int32_t l_1766[5];
                int32_t *l_1800 = &l_1766[1];
                uint64_t *l_1805[2];
                int i;
                for (i = 0; i < 5; i++)
                    l_1766[i] = 1L;
                for (i = 0; i < 2; i++)
                    l_1805[i] = &g_230[3];
                if ((((((*g_1074) |= l_1702) , (g_1704 = l_1703)) == (void*)0) | ((safe_lshift_func_uint8_t_u_u(((safe_sub_func_int16_t_s_s(((((void*)0 == (*g_1215)) ^ g_50) && (safe_mod_func_uint8_t_u_u(((*g_369) == l_1711), (safe_lshift_func_uint16_t_u_u(((safe_sub_func_int64_t_s_s(((*l_1716) |= (l_1678.f0 <= 0xE81C19989A7133EALL)), p_22)) == p_22), (*g_407)))))), (-6L))) > l_1702), 5)) | p_22)))
                { /* block id: 705 */
                    uint16_t l_1719 = 1UL;
                    int32_t *l_1741 = &g_314;
                    l_1718 = &g_1213;
                    for (g_78 = 0; (g_78 <= 4); g_78 += 1)
                    { /* block id: 709 */
                        int32_t *l_1720 = &g_314;
                        int16_t *l_1737 = &l_1680;
                        int i;
                        (*l_1720) ^= l_1719;
                        (*l_1720) = (safe_rshift_func_int16_t_s_u(((safe_sub_func_uint8_t_u_u((l_1702 | 6L), (safe_mod_func_uint8_t_u_u((((*g_1214) == ((5L != (safe_mod_func_int16_t_s_s((p_22 || l_1719), (safe_sub_func_int8_t_s_s((*g_798), ((safe_div_func_int8_t_s_s(((safe_lshift_func_int16_t_s_s(((safe_add_func_int16_t_s_s(((*l_1737) &= l_1616), ((safe_lshift_func_uint16_t_u_s(((g_195 == g_78) <= 0UL), p_22)) < l_1702))) ^ (-1L)), 9)) , 0xC1L), 0xDCL)) && 0x22L)))))) , l_1740)) , p_22), (-10L))))) == l_1678.f0), p_22));
                        (*g_1742) = l_1741;
                        if (p_22)
                            break;
                    }
                }
                else
                { /* block id: 716 */
                    uint32_t l_1744 = 18446744073709551608UL;
                    uint64_t * const *l_1748 = &g_587;
                    uint64_t * const **l_1747 = &l_1748;
                    uint64_t ****l_1750 = &l_1749;
                    int32_t l_1759 = 0xB079E88FL;
                    int32_t l_1761 = (-1L);
                    int32_t l_1765 = (-1L);
                    int32_t l_1767 = 0x7DF7BC5AL;
                    int32_t l_1768 = 1L;
                    int32_t l_1773 = 0xCC221485L;
                    int32_t l_1774 = 0L;
                    int32_t l_1775 = 3L;
                    int32_t l_1776[10] = {(-10L),(-10L),0xA23C9D1DL,(-10L),(-10L),0xA23C9D1DL,(-10L),(-10L),0xA23C9D1DL,(-10L)};
                    int8_t **l_1785[1];
                    int32_t *l_1795[3][7][6] = {{{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]}},{{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]}},{{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],(void*)0,&l_1760[0][0][3],&l_1760[0][0][3]},{(void*)0,&l_1760[0][0][3],&l_1760[0][0][3],&l_1760[0][0][3],&l_1756,&l_1756},{&l_1760[0][0][3],&l_1756,&l_1756,&l_1760[0][0][3],&l_1756,&l_1756},{&l_1760[0][0][3],&l_1756,&l_1756,&l_1760[0][0][3],&l_1756,&l_1756},{&l_1760[0][0][3],&l_1756,&l_1756,&l_1760[0][0][3],&l_1756,&l_1756},{&l_1760[0][0][3],&l_1756,&l_1756,&l_1760[0][0][3],&l_1756,&l_1756}}};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1785[i] = &g_798;
                    --l_1744;
                    if ((l_1747 != ((*l_1750) = l_1749)))
                    { /* block id: 719 */
                        int32_t *l_1751[3];
                        uint64_t l_1777 = 0x0C81B6470B52A230LL;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1751[i] = &g_304;
                        ++l_1777;
                        l_1763 |= 0L;
                    }
                    else
                    { /* block id: 722 */
                        int16_t *l_1786 = &l_1680;
                        int8_t l_1787 = 0xCFL;
                        uint16_t *l_1788 = (void*)0;
                        uint16_t *l_1789 = &g_1380[2];
                        int32_t *l_1793 = &l_1753;
                        (*l_1793) = ((((*g_407) = (safe_mul_func_uint16_t_u_u((p_21 , ((safe_add_func_int16_t_s_s((((l_1784 != l_1785[0]) & ((*l_1786) = 0xFA25L)) >= ((*g_798) = (p_22 ^ p_22))), (*g_407))) <= ((++(*l_1789)) >= (~l_1756)))), (p_22 <= l_1767)))) == p_22) > 0xD4L);
                        l_1795[0][0][2] = l_1793;
                        if (p_22)
                            break;
                    }
                }
                for (g_314 = (-30); (g_314 < (-26)); g_314++)
                { /* block id: 734 */
                    for (g_1770 = 9; (g_1770 < 18); g_1770 = safe_add_func_int16_t_s_s(g_1770, 7))
                    { /* block id: 737 */
                        l_1801 = l_1800;
                    }
                    (*g_1802) = &l_1616;
                }
                if (p_22)
                    break;
                l_1763 |= ((safe_div_func_int64_t_s_s((*g_1074), (--g_33))) <= (((safe_lshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_s((0xFD3FL ^ l_1812), 1)), 1)) == ((*g_407) = (p_22 <= (((*l_1716) = p_22) , ((safe_add_func_int64_t_s_s(((~(safe_add_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(((safe_mul_func_float_f_f((0x0.9p+1 != (p_22 , 0x4.E8CDB8p-59)), 0x0.9p-1)) , p_22), (*l_1800))), p_22))) >= g_301), p_22)) , l_1822))))) & 1UL));
            }
            else
            { /* block id: 747 */
                uint64_t *l_1827 = &g_230[3];
                int32_t l_1846 = 0xAEB8325BL;
                int32_t l_1865[4];
                float ** const l_1897 = &l_1653;
                int32_t *l_1903 = &g_304;
                int32_t *l_1904[4][1] = {{&l_1846},{&g_589[0][0][2]},{&l_1846},{&g_589[0][0][2]}};
                const int16_t l_1926 = 0L;
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1865[i] = 1L;
                if ((safe_mul_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(((*l_1827) = p_22), (safe_unary_minus_func_uint32_t_u((safe_sub_func_uint16_t_u_u(0x6A3EL, l_1831)))))), (((**g_406)++) | l_1771[0]))))
                { /* block id: 750 */
                    (*l_1801) = p_22;
                }
                else
                { /* block id: 752 */
                    int32_t l_1845[10][6][4] = {{{0x05301B77L,0xCCFBB2BCL,0xC333490EL,0xFE56B5BDL},{(-1L),0x1EEB93C8L,0x00C04F7AL,0x00C04F7AL},{(-1L),(-1L),1L,0x2CBB1405L},{5L,0x138B8520L,0x8A5C3049L,(-1L)},{0xBFB5777BL,(-10L),0xFE56B5BDL,0x8A5C3049L},{0x58AC843AL,(-10L),0xC333490EL,(-1L)}},{{(-10L),0x138B8520L,0xA9EF3FA8L,0x2CBB1405L},{0x05301B77L,(-1L),(-10L),0x00C04F7AL},{0xB6A3ABD9L,0x1EEB93C8L,0xABDCFBDBL,0xFE56B5BDL},{0xBFB5777BL,0xCCFBB2BCL,1L,5L},{(-1L),(-1L),(-3L),(-1L)},{0xCCFBB2BCL,0x630FB682L,0x00C04F7AL,(-3L)}},{{0x58AC843AL,0x05301B77L,(-10L),0x03BFD137L},{0xC493B3C9L,0x138B8520L,0xBFB5777BL,0x60938558L},{0xC493B3C9L,0xCCFBB2BCL,(-10L),0xABDCFBDBL},{0x58AC843AL,0x60938558L,0x00C04F7AL,0xFE56B5BDL},{0xCCFBB2BCL,0x8D0CFE6DL,(-3L),0x2CBB1405L},{(-1L),0x58AC843AL,1L,0x03BFD137L}},{{0xBFB5777BL,0x630FB682L,0xABDCFBDBL,1L},{0xB6A3ABD9L,(-10L),(-10L),0xB6A3ABD9L},{0x05301B77L,(-1L),0xA9EF3FA8L,0x60938558L},{(-10L),0x8D0CFE6DL,0xC333490EL,0x00C04F7AL},{0x58AC843AL,(-1L),0xFE56B5BDL,0x00C04F7AL},{0xBFB5777BL,0x8D0CFE6DL,0x8A5C3049L,0x60938558L}},{{5L,(-1L),1L,0xB6A3ABD9L},{(-1L),(-10L),0x00C04F7AL,1L},{(-1L),0x630FB682L,0xC333490EL,0x03BFD137L},{0x05301B77L,0x58AC843AL,0xCCFBB2BCL,0x2CBB1405L},{0xC493B3C9L,0x8D0CFE6DL,0xC493B3C9L,0xFE56B5BDL},{0xB6A3ABD9L,0x60938558L,0xFE56B5BDL,0xABDCFBDBL}},{{(-1L),0xCCFBB2BCL,(-3L),0x60938558L},{0x60938558L,0x138B8520L,(-3L),0x03BFD137L},{(-1L),0x05301B77L,0xFE56B5BDL,(-3L)},{0xB6A3ABD9L,0x630FB682L,0xC493B3C9L,(-1L)},{0xC493B3C9L,(-1L),0xCCFBB2BCL,5L},{0x05301B77L,0xCCFBB2BCL,0xC333490EL,0xFE56B5BDL}},{{(-1L),0x1EEB93C8L,0x00C04F7AL,0x00C04F7AL},{(-1L),(-1L),1L,0x2CBB1405L},{5L,0x138B8520L,0x8A5C3049L,(-1L)},{0xBFB5777BL,(-10L),0xFE56B5BDL,0x8A5C3049L},{(-10L),5L,0x1EEB93C8L,0xC493B3C9L},{5L,0x05301B77L,0x99A92740L,5L}},{{0x60938558L,0L,5L,0x51A9A35AL},{0xC333490EL,(-3L),(-3L),0x8A5C3049L},{1L,0xD0DF1B5EL,0xBFB5777BL,0x00C04F7AL},{0xFE56B5BDL,0xC493B3C9L,0x8D0CFE6DL,0xC493B3C9L},{0xD0DF1B5EL,(-1L),0x51A9A35AL,0x8D0CFE6DL},{(-10L),0x60938558L,5L,0x630FB682L}},{{0x2CBB1405L,0x05301B77L,1L,0xABDCFBDBL},{0x2CBB1405L,0xD0DF1B5EL,5L,(-3L)},{(-10L),0xABDCFBDBL,0x51A9A35AL,0x8A5C3049L},{0xD0DF1B5EL,(-1L),0x8D0CFE6DL,5L},{0xFE56B5BDL,(-10L),0xBFB5777BL,0x630FB682L},{1L,(-1L),(-3L),0xBFB5777BL}},{{0xC333490EL,5L,5L,0xC333490EL},{0x60938558L,0xC493B3C9L,0x99A92740L,0xABDCFBDBL},{5L,(-1L),0x1EEB93C8L,0x51A9A35AL},{(-10L),0xFE56B5BDL,0x8A5C3049L,0x51A9A35AL},{1L,(-1L),0xA9EF3FA8L,0xABDCFBDBL},{0x00C04F7AL,0xC493B3C9L,0xBFB5777BL,0xC333490EL}}};
                    int16_t l_1866 = 0L;
                    int32_t l_1867 = 0x722B2C1AL;
                    uint64_t l_1890 = 18446744073709551615UL;
                    union U0 l_1901 = {4294967289UL};
                    int i, j, k;
                    if ((safe_mul_func_uint8_t_u_u(((0xE8L & (((-1L) ^ (((*g_1213) != (l_1836[2] = (*g_1213))) >= (((p_22 <= ((((****g_1191) || (safe_mul_func_int8_t_s_s((((safe_sub_func_int64_t_s_s(((*g_1074) &= (-1L)), (-1L))) && ((((safe_div_func_uint16_t_u_u((((safe_rshift_func_uint16_t_u_s((l_1614 && l_1822), l_1845[6][4][0])) <= l_1845[0][3][2]) > p_22), g_589[0][2][2])) > l_1846) | 0x5D678024L) | p_22)) & p_22), 0xE2L))) & (*l_1801)) < l_1846)) ^ l_1616) < p_22))) && p_22)) || 0L), 0xF1L)))
                    { /* block id: 755 */
                        uint32_t *l_1860[9] = {&g_791,(void*)0,&g_791,&g_791,(void*)0,&g_791,&g_791,(void*)0,&g_791};
                        uint16_t *l_1861[9][7] = {{&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78},{&l_1702,(void*)0,(void*)0,&l_1702,&l_1702,(void*)0,(void*)0},{&g_1380[2],&g_78,&g_1380[2],&g_78,&g_1380[2],&g_78,&g_1380[2]},{&l_1702,&l_1702,(void*)0,(void*)0,&l_1702,&l_1702,(void*)0},{&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78},{&l_1702,(void*)0,(void*)0,&l_1702,&l_1702,(void*)0,(void*)0},{&g_1380[2],&g_78,&g_1380[2],&g_78,&g_1380[2],&g_78,&g_1380[2]},{&l_1702,&l_1702,(void*)0,(void*)0,&l_1702,&l_1702,(void*)0},{&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78}};
                        int32_t l_1862 = 0L;
                        uint32_t **l_1863 = &g_739;
                        int32_t *l_1864[6][6] = {{&l_1755,&g_37,&g_304,&g_304,&g_37,&l_1755},{&l_1755,&l_1845[7][0][2],(void*)0,&g_304,&l_1845[7][0][2],&g_304},{&l_1755,&l_1769,&l_1755,&g_304,&l_1769,(void*)0},{&l_1755,&g_37,&g_304,&g_304,&g_37,&l_1755},{&l_1755,&l_1845[7][0][2],(void*)0,&g_304,&l_1845[7][0][2],&g_304},{&l_1755,&l_1769,&l_1755,&g_304,&l_1769,(void*)0}};
                        int16_t l_1868 = 0x3F4AL;
                        int i, j;
                        (*l_1801) = (p_22 || l_1752);
                        (*l_1801) = (~(((safe_sub_func_int16_t_s_s(p_22, (((((*l_1701)++) | (*g_798)) , ((*l_1711)++)) || (((*g_1074) != (safe_sub_func_uint32_t_u_u((((safe_mod_func_uint16_t_u_u((****g_1191), ((safe_rshift_func_uint16_t_u_u((l_1862 = ((l_1846 , l_1801) != (l_1846 , l_1860[4]))), p_22)) ^ (*l_1801)))) , (***l_1703)) == l_1863), 4294967287UL))) <= 6L)))) | (*l_1801)) && 0UL));
                        ++l_1869;
                    }
                    else
                    { /* block id: 762 */
                        return p_22;
                    }
                    for (l_1678.f1 = (-15); (l_1678.f1 > 10); l_1678.f1 = safe_add_func_uint32_t_u_u(l_1678.f1, 4))
                    { /* block id: 767 */
                        if (l_1680)
                            break;
                    }
                    for (l_1612 = 13; (l_1612 < (-30)); l_1612 = safe_sub_func_uint32_t_u_u(l_1612, 4))
                    { /* block id: 772 */
                        uint64_t l_1876 = 0x3FD88B7051128492LL;
                        int16_t *l_1877 = &l_1680;
                        uint16_t **l_1879 = &g_407;
                        (*g_1887) = ((((l_1772 < ((((*l_1877) |= l_1876) , l_1878[0]) == l_1879)) ^ l_1866) , (safe_mul_func_int16_t_s_s(((safe_sub_func_int8_t_s_s(0xC4L, (safe_mod_func_int32_t_s_s(l_1845[6][4][0], ((*l_1801) = (((*l_1877) = ((!(-0x1.Dp+1)) , p_22)) , p_22)))))) != p_22), l_1876))) & l_1769);
                    }
                    for (g_301 = 0; (g_301 > 16); g_301++)
                    { /* block id: 780 */
                        int32_t **l_1902 = &l_1801;
                        (*l_1801) = ((l_1614 , (((((l_1890 != (safe_mod_func_int8_t_s_s((safe_div_func_int8_t_s_s((safe_rshift_func_int16_t_s_u((l_1897 == (*l_1651)), (safe_rshift_func_uint16_t_u_u(0x1D84L, 13)))), (l_1900 , ((l_1901 , 0x8EL) ^ ((p_22 , (void*)0) == (void*)0))))), 0x93L))) == (**g_1215)) && (*l_1801)) ^ l_1845[6][5][3]) == (-1L))) , p_22);
                        if (p_22)
                            break;
                        (*l_1902) = &l_1831;
                    }
                }
                --g_1905;
                (*g_1887) |= (safe_sub_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((((*l_1801) || (*l_1903)) <= ((**g_1215) |= ((((safe_rshift_func_uint16_t_u_s((l_1769 &= ((-4L) != (p_22 ^ (+(safe_div_func_uint16_t_u_u((((g_1160 , l_1771[2]) == (safe_add_func_uint64_t_u_u(((*l_1827)++), (safe_lshift_func_int16_t_s_u((*l_1801), (l_1616 = ((****g_1191) = (5L != (l_1923 == (void*)0))))))))) < p_22), 0x9933L)))))), p_22)) ^ 0x7BD1D6F7FCE6D2E1LL) & p_22) || p_22))), l_1900.f0)), l_1926));
            }
            l_1938[4][6]++;
            for (l_1757 = 0; (l_1757 < 29); l_1757++)
            { /* block id: 797 */
                uint64_t l_1943[9] = {0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL,0xB9C835576CEB5D82LL};
                int32_t **l_1946 = &l_1930;
                int i;
                l_1943[4]++;
                (*l_1946) = &l_1831;
            }
        }
        (*l_1947) |= 1L;
    }
    for (g_314 = 1; (g_314 >= 0); g_314 -= 1)
    { /* block id: 806 */
        int32_t l_1959 = 0x5001AC6EL;
        int32_t l_1960 = 0xE134B9FEL;
        int32_t l_1961 = (-10L);
        int32_t l_1962 = 0xA55DDD2EL;
        int32_t l_1963 = 0L;
        uint32_t *l_1992 = &g_892;
        uint64_t **l_2125 = &g_587;
        uint16_t **l_2147 = &g_407;
        int32_t ****l_2171 = (void*)0;
        int16_t l_2200 = 0xB486L;
        int i;
        for (g_777 = 3; (g_777 >= 0); g_777 -= 1)
        { /* block id: 809 */
            int i, j;
            return g_1326[g_777][g_777];
        }
        if (l_1760[0][0][3])
            continue;
        for (g_425 = 3; (g_425 >= 0); g_425 -= 1)
        { /* block id: 815 */
            uint8_t l_1964[5];
            uint32_t l_1979 = 0x2FB03000L;
            int32_t *l_1993 = (void*)0;
            int32_t *l_2014 = &g_1770;
            int32_t ** const l_2013 = &l_2014;
            int32_t ** const *l_2012 = &l_2013;
            int32_t ** const **l_2011 = &l_2012;
            int32_t ** const ***l_2010 = &l_2011;
            int i;
            for (i = 0; i < 5; i++)
                l_1964[i] = 0xB7L;
            for (g_54 = 0; (g_54 <= 4); g_54 += 1)
            { /* block id: 818 */
                int32_t l_1948 = 0xCD18F331L;
                int32_t *l_1949 = &g_589[1][3][6];
                int32_t *l_1950 = &g_703[0];
                int32_t *l_1951 = &g_589[g_314][(g_314 + 1)][(g_425 + 3)];
                int32_t l_1952 = 5L;
                int32_t *l_1953 = &l_1948;
                int32_t *l_1954 = &l_1760[0][1][4];
                int32_t l_1955 = 0x0BD508F9L;
                int32_t *l_1956 = &g_703[0];
                int32_t *l_1957 = &l_1948;
                int32_t *l_1958[10];
                int i, j, k;
                for (i = 0; i < 10; i++)
                    l_1958[i] = &g_37;
                ++l_1964[1];
            }
            for (g_301 = 0; (g_301 <= 1); g_301 += 1)
            { /* block id: 823 */
                uint8_t l_1987 = 0x72L;
                for (l_1900.f1 = 0; (l_1900.f1 <= 1); l_1900.f1 += 1)
                { /* block id: 826 */
                    uint64_t ***l_1986 = (void*)0;
                    uint64_t ****l_1985 = &l_1986;
                    int32_t l_1988 = 3L;
                    uint64_t *l_1989 = (void*)0;
                    uint64_t *l_1990 = &g_230[3];
                    int32_t *l_1991 = (void*)0;
                    int32_t **l_1994 = &g_1057[3];
                    const int32_t *l_2008 = &g_301;
                    const int32_t **l_2007 = &l_2008;
                    const int32_t *** const l_2006 = &l_2007;
                    const int32_t *** const *l_2005[9][1][6] = {{{&l_2006,&l_2006,&l_2006,&l_2006,(void*)0,&l_2006}},{{&l_2006,(void*)0,(void*)0,&l_2006,(void*)0,(void*)0}},{{(void*)0,&l_2006,&l_2006,&l_2006,&l_2006,&l_2006}},{{&l_2006,&l_2006,&l_2006,&l_2006,&l_2006,(void*)0}},{{&l_2006,&l_2006,(void*)0,&l_2006,&l_2006,&l_2006}},{{&l_2006,&l_2006,&l_2006,&l_2006,(void*)0,&l_2006}},{{&l_2006,(void*)0,(void*)0,&l_2006,(void*)0,(void*)0}},{{(void*)0,&l_2006,&l_2006,&l_2006,&l_2006,&l_2006}},{{&l_2006,&l_2006,&l_2006,&l_2006,&l_2006,(void*)0}}};
                    const int32_t *** const **l_2004 = &l_2005[7][0][5];
                    int32_t ** const ***l_2009 = (void*)0;
                    int32_t *l_2015 = &g_589[1][1][1];
                    int i, j, k;
                    (*g_117) = (l_1760[0][0][3] = (((*l_1990) = (safe_div_func_int64_t_s_s((safe_mod_func_int32_t_s_s((g_589[g_301][(g_425 + 1)][(g_314 + 1)] , (l_1988 |= ((safe_sub_func_int16_t_s_s((safe_mul_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(g_1326[g_425][g_425], g_589[g_301][(g_425 + 1)][(g_314 + 1)])), (safe_mul_func_uint16_t_u_u(((((l_1979 , (safe_lshift_func_int16_t_s_u((!0xCFL), (safe_lshift_func_uint8_t_u_s(p_22, (((l_1985 != &l_1986) < ((*g_1282) != (void*)0)) > g_589[g_301][(g_425 + 1)][(g_314 + 1)])))))) == 0x0B7DA256L) , 0x0.Bp+1) , p_22), l_1987)))), p_22)) != 1UL))), p_22)), p_22))) != 1L));
                    for (l_1614 = 0; (l_1614 <= 0); l_1614 += 1)
                    { /* block id: 833 */
                        int i, j, k;
                        (***l_1651) = ((g_703[l_1614] , (void*)0) == (l_1760[l_1614][(g_301 + 2)][(g_425 + 1)] , l_1992));
                    }
                    (*l_1994) = (p_22 , l_1993);
                    (*l_2015) ^= (safe_lshift_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(0xB61CL, (safe_div_func_int64_t_s_s(((***g_409) <= (((+(((*l_1985) = (*l_1985)) == &g_143[3])) , l_2004) != (l_2010 = l_2009))), ((l_1963 == ((((**g_1281) = (*g_1282)) == &p_22) <= (-8L))) || p_22))))), 1));
                }
            }
            for (g_1905 = 0; (g_1905 <= 3); g_1905 += 1)
            { /* block id: 845 */
                int16_t *l_2034 = &g_2035;
                int32_t **l_2039 = &l_1993;
                int i, j, k;
                if ((safe_add_func_int16_t_s_s(((*l_2034) = (safe_div_func_uint32_t_u_u(((g_589[g_314][g_314][(g_314 + 2)] > (g_1326[g_1905][g_425] , (safe_lshift_func_int16_t_s_s(p_22, (safe_div_func_uint32_t_u_u((safe_mul_func_int8_t_s_s((*g_798), (((-1L) <= ((safe_lshift_func_uint16_t_u_s(((safe_rshift_func_int8_t_s_s(p_22, (((*g_144) != 0x1B6E03A42D08D150LL) == ((((((*g_407) = (safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((***g_1214) == p_22), 6)), 11))) && 0x3F0BL) < p_22) > p_22) & l_1616)))) == p_22), p_22)) ^ p_22)) >= p_22))), 4294967291UL)))))) >= 0x1F0BF0A3936F20DBLL), p_22))), p_22)))
                { /* block id: 848 */
                    uint8_t l_2036[8];
                    int i;
                    for (i = 0; i < 8; i++)
                        l_2036[i] = 5UL;
                    for (l_1822 = 0; (l_1822 <= 3); l_1822 += 1)
                    { /* block id: 851 */
                        if (l_1616)
                            break;
                        l_2036[2]--;
                    }
                    if (p_22)
                        continue;
                }
                else
                { /* block id: 856 */
                    return p_22;
                }
                (*l_2039) = (void*)0;
            }
            for (g_1905 = 0; (g_1905 <= 3); g_1905 += 1)
            { /* block id: 863 */
                (*g_1342) = ((**l_1652) = p_22);
                if (p_22)
                    break;
            }
        }
        for (g_133.f1 = 0; (g_133.f1 <= 3); g_133.f1 += 1)
        { /* block id: 871 */
            int32_t l_2042 = 0xBBAFFBE8L;
            int8_t **l_2047[1];
            int8_t l_2050 = 0L;
            int32_t l_2053 = (-1L);
            uint16_t l_2118 = 0x67B9L;
            int16_t *l_2123 = &g_425;
            int16_t *l_2124[9][4][6] = {{{&g_2035,(void*)0,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,(void*)0,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,(void*)0,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,(void*)0,&g_2035},{&g_2035,&g_2035,(void*)0,&g_2035,(void*)0,&g_2035}},{{&g_2035,&g_2035,(void*)0,&g_2035,(void*)0,&g_2035},{&g_2035,&g_2035,&g_2035,(void*)0,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,(void*)0,&g_2035},{&g_2035,&g_2035,&g_2035,(void*)0,(void*)0,&g_2035}},{{&g_2035,&g_2035,&g_2035,(void*)0,(void*)0,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,(void*)0,(void*)0,&g_2035,&g_2035,&g_2035}},{{&g_2035,(void*)0,(void*)0,&g_2035,&g_2035,&g_2035},{&g_2035,(void*)0,&g_2035,&g_2035,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,(void*)0,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,(void*)0,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}}};
            int32_t l_2128 = 0xB8AD74A2L;
            int32_t *l_2130 = &g_703[0];
            int32_t *l_2131[6] = {&g_314,&g_314,&g_314,&g_314,&g_314,&g_314};
            uint8_t * const *l_2140 = (void*)0;
            uint8_t * const **l_2139 = &l_2140;
            uint8_t * const ***l_2138 = &l_2139;
            int32_t l_2207 = (-1L);
            uint64_t l_2209 = 18446744073709551615UL;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_2047[i] = (void*)0;
            for (g_118 = 0; (g_118 <= 3); g_118 += 1)
            { /* block id: 874 */
                uint64_t l_2041 = 0xA83F4CEF1CE6FE2ALL;
                int8_t ***l_2044 = &l_1784;
                int8_t **l_2046 = &g_798;
                int8_t ***l_2045 = &l_2046;
                int8_t ***l_2048 = &l_2047[0];
                uint32_t l_2049 = 18446744073709551615UL;
                int64_t l_2054[10] = {6L,0xE0DD902AB32F1E4CLL,6L,0xE0DD902AB32F1E4CLL,6L,0xE0DD902AB32F1E4CLL,6L,0xE0DD902AB32F1E4CLL,6L,0xE0DD902AB32F1E4CLL};
                int32_t l_2073[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_2073[i] = 0L;
                (*g_2043) &= ((((+l_2041) < p_22) == (p_22 , (l_2042 | ((1UL & l_2041) | 0x0ED57FA1L)))) && ((****g_1213) || 0x4CD02815L));
                if ((g_425 != (((((*l_2045) = ((*l_2044) = (p_22 , (void*)0))) == ((*l_2048) = l_2047[0])) <= p_22) > (((*g_798) |= ((((l_2049 & ((l_2050 > (p_22 == (safe_mod_func_uint16_t_u_u((l_2053 = l_1612), g_1770)))) == l_2054[7])) > p_22) > p_22) == (*g_144))) , (*g_144)))))
                { /* block id: 881 */
                    uint16_t ***l_2064 = &g_406;
                    int32_t l_2086 = 1L;
                    int32_t **l_2089 = &g_39[0];
                    uint32_t l_2092 = 18446744073709551615UL;
                    for (l_2049 = 0; (l_2049 <= 3); l_2049 += 1)
                    { /* block id: 884 */
                        int16_t *l_2059 = &g_425;
                        const int32_t ****l_2072 = (void*)0;
                        const int32_t *****l_2071[5];
                        int32_t l_2074 = (-8L);
                        int32_t *l_2075 = &l_2053;
                        int32_t **l_2078 = &g_1057[7];
                        int32_t ***l_2077 = &l_2078;
                        int16_t *l_2088[3][1][8] = {{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}}};
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_2071[i] = &l_2072;
                        if (p_22)
                            break;
                        (*g_2076) = ((*l_2075) = (safe_add_func_uint8_t_u_u(p_22, (safe_unary_minus_func_int16_t_s((((((*l_2059) = (+l_1900.f0)) != (((l_2073[0] |= ((safe_rshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u(((***l_2064) |= (((void*)0 == l_2064) <= (p_22 || 0xB7CB6314D43286CFLL))), 65535UL)), (safe_div_func_uint32_t_u_u((((-1L) < (((p_22 , g_2067[6]) != l_2071[1]) >= 0xBFC17921L)) <= (*g_1074)), p_22)))) & 0x0E57280F19D99AB6LL)) > 0L) <= p_22)) != p_22) != l_2074))))));
                        (*l_2077) = &l_2075;
                        (***l_2077) |= ((safe_add_func_uint64_t_u_u((0xD4A3L & 65531UL), (safe_add_func_int64_t_s_s((((((g_2035 = (0UL ^ ((~(((safe_mod_func_uint64_t_u_u((((**g_406) |= (((*l_2059) ^= 0xF433L) ^ 65534UL)) <= g_301), p_22)) <= l_2086) , (safe_unary_minus_func_int8_t_s(0L)))) <= l_1900.f0))) | 1UL) , p_22) <= l_2049) < l_2086), (-8L))))) || (*g_798));
                    }
                    (*l_2089) = &l_1616;
                    l_2053 ^= (safe_sub_func_uint32_t_u_u(l_2092, 0xE321C8A5L));
                    (*l_2089) = &l_2086;
                }
                else
                { /* block id: 900 */
                    uint64_t l_2115 = 18446744073709551615UL;
                    if (l_1614)
                    { /* block id: 901 */
                        if (g_37)
                            goto lbl_2093;
                    }
                    else
                    { /* block id: 903 */
                        int32_t *l_2094 = &g_703[0];
                        int32_t *l_2095 = &g_703[0];
                        int32_t *l_2096 = &l_1960;
                        int32_t *l_2097 = &l_1962;
                        int32_t *l_2098 = &l_1960;
                        int32_t *l_2099 = &l_1961;
                        int32_t *l_2100 = &g_589[1][0][7];
                        int32_t *l_2101 = (void*)0;
                        int32_t l_2102 = 0L;
                        int32_t *l_2103 = &l_1616;
                        int32_t *l_2104 = &l_1760[0][0][4];
                        int32_t *l_2105 = (void*)0;
                        int32_t *l_2106 = &l_1760[0][4][1];
                        int32_t *l_2107 = &l_1960;
                        int32_t *l_2108 = (void*)0;
                        int32_t *l_2109 = &l_1963;
                        int32_t *l_2110 = &l_1760[0][0][3];
                        int32_t *l_2111 = &l_1961;
                        int32_t *l_2112 = &l_1962;
                        int32_t *l_2113 = &l_1962;
                        int32_t *l_2114[3][10] = {{&l_2102,&l_2102,(void*)0,&l_2102,&l_2102,(void*)0,&l_2102,&l_2102,(void*)0,&l_2102},{&l_2102,&l_2073[0],&l_2073[0],&l_2102,&l_2073[0],&l_2073[0],&l_2102,&l_2073[0],&l_2073[0],&l_2102},{&l_2073[0],&l_2102,&l_2073[0],&l_2073[0],&l_2102,&l_2073[0],&l_2073[0],&l_2102,&l_2073[0],&l_2073[0]}};
                        int i, j;
                        --l_2115;
                        (*l_2113) |= (*g_182);
                    }
                }
            }
            if (l_2118)
                break;
            (*l_2130) |= (p_22 != (p_22 && ((safe_mod_func_int64_t_s_s(((l_2053 < (g_701 , ((g_2035 = ((*l_2123) |= 0xC1B2L)) , ((l_2128 ^= ((*l_2123) = ((l_2125 != (*g_585)) , (safe_div_func_uint16_t_u_u((0xF7162986L != p_22), p_22))))) , g_2129[0][1])))) != l_1959), l_1961)) >= p_22)));
            l_2132[0][0][4]--;
            for (g_54 = 0; (g_54 <= 3); g_54 += 1)
            { /* block id: 918 */
                uint16_t l_2148[7][5][2] = {{{1UL,65526UL},{0xBEE6L,65526UL},{1UL,65528UL},{1UL,65526UL},{0xBEE6L,65526UL}},{{1UL,65528UL},{1UL,65526UL},{0xBEE6L,65526UL},{1UL,65528UL},{1UL,65526UL}},{{0xBEE6L,65526UL},{1UL,65528UL},{1UL,65526UL},{0xBEE6L,65526UL},{1UL,65528UL}},{{1UL,65526UL},{0xBEE6L,65526UL},{1UL,0xB712L},{0xBEE6L,65528UL},{0x403BL,65528UL}},{{0xBEE6L,0xB712L},{0xBEE6L,65528UL},{0x403BL,65528UL},{0xBEE6L,0xB712L},{0xBEE6L,65528UL}},{{0x403BL,65528UL},{0xBEE6L,0xB712L},{0xBEE6L,65528UL},{0x403BL,65528UL},{0xBEE6L,0xB712L}},{{0xBEE6L,65528UL},{0x403BL,65528UL},{0xBEE6L,0xB712L},{0xBEE6L,65528UL},{0x403BL,65528UL}}};
                union U0 l_2161[7][8][4] = {{{{0UL},{0xC8ED7700L},{0xD018A321L},{0UL}},{{0UL},{1UL},{0UL},{0xD81DA1D9L}},{{8UL},{0UL},{1UL},{0UL}},{{0xED2CED73L},{1UL},{1UL},{0xA0BFAB51L}},{{8UL},{4294967292UL},{0UL},{0UL}},{{0UL},{1UL},{0xD018A321L},{1UL}},{{0UL},{0UL},{0UL},{4294967291UL}},{{8UL},{1UL},{1UL},{1UL}}},{{{0xED2CED73L},{0xC8ED7700L},{1UL},{0UL}},{{8UL},{4294967295UL},{0UL},{0xA0BFAB51L}},{{0UL},{0xC8ED7700L},{0xD018A321L},{0UL}},{{0UL},{1UL},{0UL},{0xD81DA1D9L}},{{8UL},{0UL},{1UL},{0UL}},{{0xED2CED73L},{1UL},{1UL},{0xA0BFAB51L}},{{8UL},{4294967292UL},{0UL},{0UL}},{{0UL},{1UL},{0xD018A321L},{1UL}}},{{{0UL},{0UL},{0UL},{4294967291UL}},{{8UL},{1UL},{1UL},{1UL}},{{0xED2CED73L},{0xC8ED7700L},{1UL},{0UL}},{{8UL},{4294967295UL},{0UL},{0xA0BFAB51L}},{{0UL},{0xC8ED7700L},{0xD018A321L},{0UL}},{{0UL},{1UL},{0UL},{0xD81DA1D9L}},{{8UL},{0UL},{1UL},{0UL}},{{0xED2CED73L},{1UL},{1UL},{0xA0BFAB51L}}},{{{8UL},{4294967292UL},{0UL},{0UL}},{{0UL},{1UL},{0xD018A321L},{1UL}},{{0UL},{0UL},{0UL},{4294967291UL}},{{8UL},{1UL},{1UL},{1UL}},{{0xED2CED73L},{0xC8ED7700L},{1UL},{0UL}},{{8UL},{4294967295UL},{0UL},{0xA0BFAB51L}},{{0UL},{0xC8ED7700L},{0xD018A321L},{0UL}},{{0UL},{1UL},{0UL},{0xD81DA1D9L}}},{{{8UL},{0UL},{1UL},{0UL}},{{0xED2CED73L},{1UL},{1UL},{0xA0BFAB51L}},{{8UL},{4294967292UL},{0UL},{0UL}},{{0UL},{1UL},{0xD018A321L},{1UL}},{{0UL},{0UL},{0UL},{4294967291UL}},{{8UL},{1UL},{1UL},{1UL}},{{0xED2CED73L},{0xC8ED7700L},{1UL},{0UL}},{{0xED2CED73L},{1UL},{0xD018A321L},{1UL}}},{{{1UL},{4294967292UL},{1UL},{4294967291UL}},{{1UL},{0xD81DA1D9L},{0xD018A321L},{0xA0BFAB51L}},{{0xED2CED73L},{4294967291UL},{0x2B28D6CFL},{4294967291UL}},{{0UL},{4294967295UL},{0x2B28D6CFL},{1UL}},{{0xED2CED73L},{0UL},{0xD018A321L},{0xC8ED7700L}},{{1UL},{4294967295UL},{1UL},{0xD81DA1D9L}},{{1UL},{4294967291UL},{0xD018A321L},{0UL}},{{0xED2CED73L},{0xD81DA1D9L},{0x2B28D6CFL},{0xD81DA1D9L}}},{{{0UL},{4294967292UL},{0x2B28D6CFL},{0xC8ED7700L}},{{0xED2CED73L},{1UL},{0xD018A321L},{1UL}},{{1UL},{4294967292UL},{1UL},{4294967291UL}},{{1UL},{0xD81DA1D9L},{0xD018A321L},{0xA0BFAB51L}},{{0xED2CED73L},{4294967291UL},{0x2B28D6CFL},{4294967291UL}},{{0UL},{4294967295UL},{0x2B28D6CFL},{1UL}},{{0xED2CED73L},{0UL},{0xD018A321L},{0xC8ED7700L}},{{1UL},{4294967295UL},{1UL},{0xD81DA1D9L}}}};
                int32_t l_2164 = 0x9D5A4A74L;
                int32_t l_2198 = 0x7A984136L;
                int32_t l_2202 = 0x6F1AC4F7L;
                int32_t l_2204 = 0x92F6E801L;
                int32_t l_2205 = 0xB599A44DL;
                int32_t l_2206 = 0x8FEB88BEL;
                int32_t l_2208 = 0xD6F82172L;
                int i, j, k;
                if (((safe_mul_func_int8_t_s_s(p_22, p_22)) || (l_2137[1][2][0] != l_2138)))
                { /* block id: 919 */
                    int64_t l_2162[8][6][2] = {{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}},{{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L},{0xD7FEF74D893C4A75LL,1L},{0x298842A79564C31DLL,1L}}};
                    int32_t *l_2167 = (void*)0;
                    int32_t *l_2168[9] = {&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2]};
                    int i, j, k;
                    if (((l_2164 = ((0xE2451AF17C641F11LL != (0x6DE7077AL < ((*l_2130) = (safe_mod_func_uint64_t_u_u(((safe_mul_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(((*l_2123) = ((0x3191535CL | (l_2148[0][1][0] = (l_2147 == (void*)0))) ^ (!(safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s((((safe_lshift_func_int8_t_s_u((1UL && p_22), (safe_mul_func_uint8_t_u_u(((l_1760[0][0][3] |= ((safe_mod_func_uint16_t_u_u(((**l_2147) = (~(g_29 != ((l_2161[2][3][2] , g_2035) && (**g_406))))), p_22)) | l_2162[0][1][1])) , l_2162[0][1][1]), p_22)))) > p_22) , (-1L)), l_2132[2][0][4])), 254UL))))), p_22)), p_22)) < l_2161[2][3][2].f0), l_1962))))) < (*g_798))) <= 0x8B2A9E25L))
                    { /* block id: 926 */
                        if (p_22)
                            break;
                    }
                    else
                    { /* block id: 928 */
                        int32_t *l_2165 = &l_2053;
                        int32_t **l_2166[9][5] = {{(void*)0,&l_2131[3],&l_2131[3],(void*)0,(void*)0},{(void*)0,&g_39[0],(void*)0,&g_39[0],(void*)0},{(void*)0,(void*)0,&l_2131[3],&l_2131[3],(void*)0},{(void*)0,&g_39[0],(void*)0,&g_39[0],(void*)0},{(void*)0,&l_2131[3],&l_2131[3],(void*)0,(void*)0},{(void*)0,&g_39[0],(void*)0,&g_39[0],(void*)0},{(void*)0,(void*)0,&l_2131[3],&l_2131[3],(void*)0},{(void*)0,&g_39[0],(void*)0,&g_39[0],(void*)0},{(void*)0,&l_2131[3],&l_2131[3],(void*)0,(void*)0}};
                        int i, j;
                        l_2168[4] = (l_2167 = l_2165);
                    }
                }
                else
                { /* block id: 932 */
                    (*g_397) = &l_1614;
                }
                if (((*g_144) > ((((l_2164 = (*g_798)) == (&g_2069 == l_2171)) , (safe_lshift_func_int8_t_s_u((1UL != ((*l_2130) = (safe_mul_func_int16_t_s_s(((*l_2123) = ((((safe_div_func_uint32_t_u_u(8UL, l_1962)) == (*g_798)) < l_1962) > p_22)), 0xDCD4L)))), p_22))) >= 1L)))
                { /* block id: 938 */
                    uint32_t *l_2178 = &g_1160.f1;
                    int32_t l_2191 = 0xF709F0C7L;
                    int32_t l_2192 = 0xE4C4E455L;
                    if (p_22)
                        break;
                    if (g_167)
                        goto lbl_2093;
                    if ((((*l_2178) = p_22) , (safe_mul_func_uint16_t_u_u((1L || (((safe_rshift_func_int16_t_s_s((g_2185 , (p_22 >= ((void*)0 != &l_2125))), l_1612)) , p_22) <= 0xA85C191EL)), 0x110CL))))
                    { /* block id: 942 */
                        int32_t **l_2190 = &l_2131[3];
                        (*l_2190) = ((safe_rshift_func_uint16_t_u_u((((safe_add_func_float_f_f(p_22, ((***l_1651) = (**g_1344)))) , &g_425) == &g_100), 5)) , (void*)0);
                    }
                    else
                    { /* block id: 945 */
                        uint8_t l_2193[10] = {255UL,0x9BL,255UL,0x9BL,255UL,0x9BL,255UL,0x9BL,255UL,0x9BL};
                        int i;
                        l_2193[1]++;
                        if (g_701)
                            goto lbl_2093;
                        (*l_2130) = (*g_182);
                    }
                }
                else
                { /* block id: 950 */
                    int8_t l_2199[4][3][10] = {{{(-8L),0x14L,0xC3L,0xBEL,1L,0xBEL,0xC3L,0x14L,(-8L),(-9L)},{0xBEL,0xC3L,0x14L,(-8L),(-9L),1L,0x9EL,1L,1L,1L},{0x6BL,0x22L,(-6L),(-8L),1L,1L,1L,1L,(-8L),(-6L)}},{{1L,1L,(-1L),0xBEL,0x22L,1L,(-9L),0xB4L,(-4L),0x1EL},{0xC3L,0xB4L,0x9EL,0x6BL,1L,(-4L),(-9L),0xBEL,(-9L),(-4L)},{0x1EL,1L,0x6BL,1L,0x1EL,0x9EL,1L,0xC3L,0x22L,(-1L)}},{{1L,0x22L,(-8L),0xC3L,0x6BL,0x14L,0x9EL,(-6L),(-1L),(-1L)},{1L,0xC3L,1L,0x1EL,0x1EL,1L,0xC3L,1L,1L,(-4L)},{(-4L),0x14L,1L,1L,1L,(-6L),1L,0xC0L,0xBEL,0x1EL}},{{1L,(-6L),1L,1L,0x22L,0xC0L,0x22L,1L,1L,(-6L)},{0xC3L,(-1L),1L,(-4L),1L,0x1EL,0x14L,(-6L),0xB4L,1L},{1L,0x9EL,(-8L),1L,(-9L),0x1EL,0xC3L,0xC3L,0x1EL,(-9L)}}};
                    int32_t l_2201 = (-3L);
                    int32_t l_2203[1];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_2203[i] = (-1L);
                    for (g_33 = 0; (g_33 <= 3); g_33 += 1)
                    { /* block id: 953 */
                        (**l_1652) = (safe_div_func_float_f_f(p_22, (**g_1344)));
                        return p_22;
                    }
                    l_2198 = ((l_2164 = p_22) , 1L);
                    l_2209++;
                    (***l_1651) = 0x8.F397B3p-58;
                }
                if (l_2205)
                    continue;
                if (l_1616)
                    continue;
            }
        }
    }
    for (g_791 = (-14); (g_791 == 34); ++g_791)
    { /* block id: 969 */
        int32_t *l_2214 = (void*)0;
        int32_t *l_2215 = (void*)0;
        int32_t *l_2216 = &l_1616;
        int32_t **l_2217 = (void*)0;
        int32_t **l_2218 = &l_2214;
        (*l_2216) = p_22;
        (*l_2218) = (void*)0;
        for (l_1612 = 0; (l_1612 >= 23); l_1612++)
        { /* block id: 974 */
            (*l_2218) = &l_1614;
        }
    }
    (*g_1342) = (safe_div_func_float_f_f(((***l_1651) = (((l_1784 != l_1784) ^ (((safe_rshift_func_uint16_t_u_s(l_2132[0][0][4], (((safe_add_func_uint32_t_u_u(p_22, (safe_div_func_int32_t_s_s((((p_22 , (safe_add_func_int32_t_s_s(0xB25B0434L, l_1614))) , 0x69C8L) ^ l_2132[1][3][3]), l_1616)))) , 0L) , g_1770))) > 1UL) > (****g_1213))) , (-0x1.Ep+1))), (*g_1060)));
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_38 g_37 g_230 g_314 g_320 g_179 g_791 g_458 g_407 g_78 g_406 g_701 g_812 g_147 g_39 g_122 g_425 g_777 g_586 g_29 g_798 g_167 g_589 g_739 g_461 g_144 g_145 g_358 g_950 g_117 g_118 g_133.f0 g_985 g_409 g_703 g_1007 g_304 g_741 g_54 g_1056 g_175 g_1060 g_619 g_620 g_1191 g_1192 g_1214 g_1215 g_1074 g_547 g_1342
 * writes: g_39 g_122 g_314 g_791 g_777 g_798 g_37 g_701 g_425 g_587 g_167 g_179 g_464 g_892 g_461 g_301 g_133 g_78 g_304 g_1057 g_175 g_1074 g_406 g_54 g_703 g_547 g_1343
 */
static float  func_24(int8_t  p_25, int16_t  p_26)
{ /* block id: 2 */
    int32_t *l_36 = &g_37;
    int32_t l_710 = 0x9129A6F3L;
    int32_t l_743 = 1L;
    int32_t l_744 = (-7L);
    int32_t l_745 = 3L;
    int32_t l_746 = 0xA35E2B17L;
    int32_t l_747[2];
    uint16_t l_748 = 1UL;
    uint32_t l_753[7];
    int64_t *l_768 = &g_122[3];
    const uint8_t *l_810 = (void*)0;
    const uint8_t **l_809 = &l_810;
    uint32_t l_811 = 0x417BA6F0L;
    int8_t l_842 = 1L;
    int32_t l_862 = (-9L);
    uint64_t l_1037 = 9UL;
    uint64_t *l_1053 = &g_230[3];
    uint16_t l_1091 = 0x56A7L;
    int32_t *l_1095[1];
    int32_t **l_1094 = &l_1095[0];
    union U0 *l_1136 = &g_133;
    uint32_t l_1206[10] = {0xC3A1AF65L,0xE7EB4E35L,0xC3A1AF65L,0xE7EB4E35L,0xC3A1AF65L,0xE7EB4E35L,0xC3A1AF65L,0xE7EB4E35L,0xC3A1AF65L,0xE7EB4E35L};
    int32_t *l_1220 = (void*)0;
    int8_t l_1223[5] = {0x26L,0x26L,0x26L,0x26L,0x26L};
    int8_t l_1259 = 1L;
    uint64_t *l_1275 = (void*)0;
    const uint8_t l_1412 = 0xDBL;
    int32_t ** const l_1432[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t ** const * const l_1431 = &l_1432[3];
    uint16_t **l_1436 = &g_407;
    const uint16_t **l_1472 = (void*)0;
    uint16_t l_1503 = 0x606BL;
    uint32_t l_1569 = 4294967295UL;
    int i;
    for (i = 0; i < 2; i++)
        l_747[i] = (-8L);
    for (i = 0; i < 7; i++)
        l_753[i] = 0x977D5399L;
    for (i = 0; i < 1; i++)
        l_1095[i] = (void*)0;
    for (p_26 = (-17); (p_26 == (-18)); p_26 = safe_sub_func_uint64_t_u_u(p_26, 1))
    { /* block id: 5 */
        int32_t l_46 = (-2L);
        uint8_t *l_49 = &g_50;
        uint8_t *l_53[10];
        uint64_t *l_700 = &g_701;
        int32_t *l_702 = &g_703[0];
        int32_t l_711 = 1L;
        int32_t l_713 = 1L;
        int32_t l_715 = 0x0CE79F17L;
        int32_t l_716 = 0xD4C390E7L;
        int i;
        for (i = 0; i < 10; i++)
            l_53[i] = &g_54;
        (*g_38) = l_36;
    }
    if ((((*l_36) <= (safe_div_func_uint64_t_u_u((l_753[6] ^ (safe_rshift_func_uint8_t_u_u((p_25 & ((((!((safe_sub_func_int32_t_s_s((((((*l_768) = (safe_sub_func_int16_t_s_s((((safe_unary_minus_func_int64_t_s((*l_36))) <= (-7L)) < p_26), ((((safe_mod_func_uint8_t_u_u((*l_36), (0xA594D8EBBAA5A801LL || ((safe_div_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_u(0xF7L, 2)) <= 0L), g_230[3])) || (*l_36))))) & (-7L)) <= (-9L)) && p_25)))) && 9L) ^ p_25) || 0UL), 0UL)) , 0x4F194AC20D003FF8LL)) != (*l_36)) | (*l_36)) < (*l_36))), 4))), (*l_36)))) > 0L))
    { /* block id: 295 */
        uint32_t *l_776 = &g_133.f1;
        uint32_t *l_778 = &g_133.f1;
        uint32_t *l_779 = &g_133.f1;
        uint32_t *l_780 = &g_133.f1;
        uint32_t *l_781 = &g_133.f1;
        uint32_t *l_782 = &g_133.f1;
        uint32_t *l_783 = &g_133.f1;
        uint32_t *l_784 = (void*)0;
        uint32_t *l_785 = &g_133.f1;
        uint32_t *l_786 = &g_133.f1;
        uint32_t *l_787 = &g_133.f1;
        int32_t l_788[3];
        uint32_t *l_789 = &g_133.f1;
        uint32_t *l_790[10][7] = {{&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791},{&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791},{&g_133.f1,(void*)0,&g_791,&g_791,&g_791,&g_791,(void*)0},{&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791},{&g_133.f1,(void*)0,&g_791,&g_791,&g_791,&g_791,(void*)0},{&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791},{&g_133.f1,(void*)0,&g_791,&g_791,&g_791,&g_791,(void*)0},{&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791},{&g_133.f1,(void*)0,&g_791,&g_791,&g_791,&g_791,(void*)0},{&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791}};
        int8_t *l_794 = &g_777;
        int8_t **l_797[8][1];
        uint64_t l_906 = 18446744073709551615UL;
        const float * const *l_947[2];
        float l_970 = (-0x1.Dp+1);
        uint16_t l_983 = 3UL;
        uint16_t ***l_1001[5] = {&g_406,&g_406,&g_406,&g_406,&g_406};
        uint16_t **** const l_1000[5] = {&l_1001[2],&l_1001[2],&l_1001[2],&l_1001[2],&l_1001[2]};
        int32_t *l_1035 = &l_862;
        int32_t l_1048 = 0x8133A4E4L;
        int32_t l_1050 = 7L;
        int i, j;
        for (i = 0; i < 3; i++)
            l_788[i] = (-1L);
        for (i = 0; i < 8; i++)
        {
            for (j = 0; j < 1; j++)
                l_797[i][j] = &l_794;
        }
        for (i = 0; i < 2; i++)
            l_947[i] = (void*)0;
        for (g_314 = 0; (g_314 <= (-5)); --g_314)
        { /* block id: 298 */
            return (*g_320);
        }
        if ((safe_sub_func_uint16_t_u_u((~(8L || ((safe_div_func_int16_t_s_s((((--g_791) , (((((((*l_794) = (*l_36)) || (safe_rshift_func_int8_t_s_s((&g_777 == (g_798 = &g_777)), (safe_mul_func_int8_t_s_s((*l_36), (0x9C235FA4L <= (safe_lshift_func_uint16_t_u_u(((safe_lshift_func_int16_t_s_u(g_458[5][3], 5)) != (1UL != ((safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u(((l_809 != &g_370) && l_811), 4)), (*g_407))) > g_37))), (**g_406))))))))) < p_26) >= 0x7FFBF7254297A381LL) , l_788[0]) && (*l_36))) && p_26), p_26)) , 1UL))), p_25)))
        { /* block id: 304 */
            (*l_36) = p_26;
        }
        else
        { /* block id: 306 */
            int32_t l_813[2];
            int8_t l_843 = 0L;
            int8_t l_845 = 1L;
            int32_t l_894 = 0xF60F34C6L;
            const int32_t l_948 = 0xC34EF966L;
            int32_t **l_953 = &g_39[0];
            uint16_t l_1002 = 65535UL;
            uint8_t l_1024[8][3] = {{255UL,7UL,7UL},{0x4AL,0x97L,0UL},{255UL,7UL,7UL},{0x4AL,0x97L,0UL},{255UL,7UL,7UL},{0x4AL,0x97L,0UL},{255UL,7UL,7UL},{0x4AL,0x97L,0UL}};
            uint64_t *l_1052 = &g_230[3];
            uint32_t *l_1069 = &g_741;
            int8_t l_1089 = 0x0FL;
            int i, j;
            for (i = 0; i < 2; i++)
                l_813[i] = 1L;
            for (g_701 = 1; (g_701 <= 4); g_701 += 1)
            { /* block id: 309 */
                (*l_36) ^= g_812[0][1][6];
            }
            if (((**g_147) == l_813[0]))
            { /* block id: 312 */
                uint32_t l_827 = 18446744073709551615UL;
                int32_t l_841[10] = {0x9EFA89D7L,0x1F2193F4L,0xAE6B5BC8L,0xAE6B5BC8L,0x1F2193F4L,0x9EFA89D7L,0x1F2193F4L,0xAE6B5BC8L,0xAE6B5BC8L,0x1F2193F4L};
                int32_t l_844 = 0xB5CEFA4EL;
                uint32_t * const *l_882[6][5][7] = {{{&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739,&g_739},{&g_739,(void*)0,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0},{&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0,(void*)0},{&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0,(void*)0}},{{&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739}},{{&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739}},{{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0},{&g_739,&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739},{(void*)0,(void*)0,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0}},{{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,(void*)0,(void*)0,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,(void*)0,&g_739},{&g_739,(void*)0,(void*)0,&g_739,&g_739,&g_739,&g_739}},{{&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,&g_739,&g_739,&g_739,&g_739},{&g_739,&g_739,&g_739,(void*)0,&g_739,&g_739,&g_739}}};
                uint32_t * const **l_881 = &l_882[5][3][0];
                int32_t l_926 = 0x7DB71687L;
                int16_t *l_929 = &g_425;
                int32_t *l_949 = &l_841[0];
                union U0 l_963 = {4294967295UL};
                int32_t l_1025 = 0xAC45F214L;
                int32_t *l_1026 = (void*)0;
                int32_t *l_1027 = (void*)0;
                int32_t *l_1028 = &g_304;
                uint64_t *l_1036[5];
                uint8_t *l_1049[2];
                int32_t l_1051 = 0x148D1721L;
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_1036[i] = &l_906;
                for (i = 0; i < 2; i++)
                    l_1049[i] = &g_50;
                if (((*l_36) = (safe_sub_func_uint16_t_u_u(65534UL, g_122[3]))))
                { /* block id: 314 */
                    uint64_t *l_828 = &g_33;
                    int32_t l_831[2];
                    int32_t *l_832 = (void*)0;
                    int32_t *l_833 = &l_747[0];
                    int32_t *l_834 = &l_744;
                    int32_t *l_835 = &g_703[0];
                    int32_t *l_836 = &l_744;
                    int32_t *l_837 = (void*)0;
                    int32_t *l_838 = &l_747[1];
                    int32_t *l_839 = (void*)0;
                    int32_t *l_840[3][4] = {{&g_314,&g_314,&g_314,&g_314},{&g_314,&g_314,&g_314,&g_314},{&g_314,&g_314,&g_314,&g_314}};
                    uint32_t l_846[7];
                    float *l_861 = &g_179;
                    float *l_865 = (void*)0;
                    float *l_866 = &g_464;
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_831[i] = 0x93452BE1L;
                    for (i = 0; i < 7; i++)
                        l_846[i] = 0x46AE2529L;
                    for (g_425 = (-8); (g_425 > 19); g_425 = safe_add_func_int16_t_s_s(g_425, 7))
                    { /* block id: 317 */
                        (*l_36) ^= (safe_rshift_func_uint16_t_u_s((((safe_lshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(((*l_794) ^= (safe_lshift_func_int16_t_s_u(((!l_827) && p_25), 11))), 7)), 0)) <= l_813[0]) , (((*g_586) = l_828) == l_828)), 8));
                    }
                    for (g_425 = 0; (g_425 >= (-19)); g_425--)
                    { /* block id: 324 */
                        return (*g_320);
                    }
                    --l_846[5];
                    (*l_866) = (safe_mul_func_float_f_f(((0x52F0L <= g_29) , (safe_add_func_float_f_f(((safe_div_func_float_f_f((safe_mul_func_float_f_f(l_845, (safe_add_func_float_f_f(((*l_861) = ((((((*g_798) = (*g_798)) >= l_788[0]) == l_827) < (safe_rshift_func_int16_t_s_u(((0L != g_589[0][2][2]) && ((l_841[0] < (-1L)) > l_841[5])), l_844))) , 0x5.BE287Bp-92)), g_458[0][2])))), 0x9.29D765p-10)) > p_26), l_862))), p_26));
                }
                else
                { /* block id: 331 */
                    uint8_t l_880 = 0x76L;
                    int64_t *l_893 = (void*)0;
                    uint16_t l_895 = 0x87A0L;
                    int32_t l_902 = 0x6BE43DD3L;
                    int32_t l_904 = (-1L);
                    int32_t l_905 = 8L;
                    int32_t *l_919 = (void*)0;
                    uint32_t * const *l_930 = &g_739;
                    if (((safe_lshift_func_int8_t_s_s((((safe_sub_func_uint64_t_u_u(0x3A5C18932DBC34EFLL, ((((!(safe_mod_func_uint64_t_u_u((l_894 = (safe_lshift_func_uint16_t_u_u(((l_788[0] = (safe_sub_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((g_78 ^ (l_880 & ((void*)0 != l_881))), (safe_mul_func_int8_t_s_s(((~(safe_mod_func_int64_t_s_s(((l_813[0] &= (l_841[0] = (0xC3B70F9EL != ((((g_892 = ((*l_768) = (safe_mul_func_uint8_t_u_u(((safe_add_func_int8_t_s_s((*g_798), p_26)) == g_458[0][2]), 0x0FL)))) < l_841[7]) != 0x9068L) >= (*g_739))))) < p_26), l_788[1]))) == 0UL), p_25)))), 0x98L))) & l_880), 7))), p_25))) <= l_843) , (*g_144)) > 0x89DBEC30EBA05068LL))) & p_25) < l_895), 1)) & 0xF4L))
                    { /* block id: 338 */
                        int16_t l_896 = 6L;
                        int32_t *l_897 = &l_813[1];
                        int32_t *l_898 = &l_747[0];
                        int32_t *l_899 = &l_744;
                        int32_t *l_900 = &g_314;
                        int32_t *l_901 = &g_314;
                        int32_t *l_903[3][7] = {{&l_744,&g_589[0][2][2],&g_589[0][2][2],&l_744,&l_902,&l_744,&g_589[0][2][2]},{&l_788[0],&l_788[0],&l_813[0],(void*)0,&l_813[0],&l_788[0],&l_788[0]},{(void*)0,&g_589[0][2][2],&g_589[0][2][2],&g_589[0][2][2],(void*)0,(void*)0,&g_589[0][2][2]}};
                        int i, j;
                        l_906--;
                        l_926 |= (((safe_mod_func_uint8_t_u_u(((*g_739) && ((safe_add_func_int32_t_s_s(((l_895 >= ((g_425 = ((safe_rshift_func_uint16_t_u_u(0x048EL, 1)) > (safe_mod_func_int64_t_s_s((safe_rshift_func_uint16_t_u_u((p_25 & (((void*)0 == l_919) <= (safe_add_func_int32_t_s_s(((*l_36) = ((safe_mod_func_int32_t_s_s((((*l_897) = 0xBE8800AAL) , ((l_841[0] > (safe_rshift_func_uint16_t_u_s((((*g_739) = ((l_788[0] , p_25) != 0xC098F11838293460LL)) != p_25), p_26))) >= p_26)), p_25)) != 0x6DL)), p_25)))), p_25)), l_788[0])))) || l_906)) | 1L), 0xB5BA0434L)) <= p_26)), p_26)) < l_845) >= p_25);
                        l_902 = (((**g_38) || l_788[0]) ^ (safe_add_func_int64_t_s_s((((void*)0 == l_929) == ((((void*)0 == l_930) >= ((safe_mod_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(((safe_rshift_func_int8_t_s_u(((-8L) >= (safe_rshift_func_uint8_t_u_s(((safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(((*l_929) = (safe_mul_func_uint8_t_u_u((*l_36), ((((safe_add_func_uint16_t_u_u((**g_406), (*g_407))) , l_947[1]) != (void*)0) , (*g_798))))), 11)), p_25)) >= p_26), 6))), (*l_36))) <= l_948), 0)), l_894)) <= (**g_406))) < p_25)), p_26)));
                    }
                    else
                    { /* block id: 347 */
                        int32_t **l_952 = &g_39[0];
                        int32_t ***l_951[2][1];
                        int i, j;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_951[i][j] = &l_952;
                        }
                        l_949 = (*g_358);
                        (*g_950) = &l_813[0];
                        l_953 = &l_949;
                    }
                    (**l_953) &= (safe_rshift_func_uint16_t_u_u((0x2644B4194451E00FLL || 0xF4EF6FDED0024AC9LL), 6));
                    (*l_953) = &l_841[0];
                }
                for (g_777 = (-16); (g_777 != (-22)); g_777 = safe_sub_func_int16_t_s_s(g_777, 3))
                { /* block id: 357 */
                    int32_t *l_960 = &g_301;
                    union U0 *l_964 = (void*)0;
                    union U0 *l_965 = &g_133;
                    const int32_t l_984 = (-1L);
                    if ((((((safe_mul_func_int8_t_s_s((((*l_960) = 0xE91D29DAL) , 0x74L), ((safe_sub_func_uint32_t_u_u((((*l_965) = l_963) , (safe_div_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_s((((*l_949) || (p_26 , (g_791 ^ 65529UL))) < (safe_add_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s(1UL, 1)), ((((((safe_div_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s((safe_lshift_func_uint16_t_u_s(((((safe_mod_func_int16_t_s_s(((*l_949) == (*l_36)), (*l_949))) || (*g_117)) < (*l_949)) < (*g_739)), 12)), g_812[1][2][0])) != 0x79E42ECFD58F25C7LL), l_983)) < g_133.f0) >= l_788[0]) , p_26) == 0x1DBEEC87L) , 65532UL)))), (*g_798))) , l_984), p_26))), 0x3B525316L)) && 0L))) , &g_182) == (void*)0) , (void*)0) != g_985))
                    { /* block id: 360 */
                        return l_984;
                    }
                    else
                    { /* block id: 362 */
                        uint32_t **l_986 = &g_739;
                        const int32_t l_997 = 0xEE96D576L;
                        l_1002 |= (((void*)0 != l_986) || (safe_lshift_func_uint16_t_u_s(((safe_lshift_func_int16_t_s_u((safe_sub_func_int64_t_s_s((safe_sub_func_uint32_t_u_u((safe_lshift_func_int8_t_s_s(l_997, ((0xA7FD514CL ^ ((*l_36) < (((p_25 >= (**l_953)) || ((((2L ^ (safe_add_func_uint16_t_u_u(((void*)0 == l_1000[3]), (***g_409)))) | 0xFCL) , l_984) && 1L)) , g_701))) && g_703[0]))), 0UL)), 2UL)), 8)) <= p_26), p_26)));
                        if (p_25)
                            continue;
                    }
                    (*g_1007) = ((safe_div_func_float_f_f(0x4.Dp-1, p_26)) > (safe_sub_func_float_f_f((*l_949), l_984)));
                    for (g_133.f1 = 0; g_133.f1 < 7; g_133.f1 += 1)
                    {
                        l_753[g_133.f1] = 0x494078D9L;
                    }
                }
                (*l_1028) |= (((*g_1007) , (~(safe_add_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(((1UL != (safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((**g_406) = ((*l_36) != ((void*)0 != (*l_953)))), (!(safe_mul_func_int8_t_s_s((0x7D7CL && (((1L >= (**l_953)) & ((safe_mul_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(0xFDE9050A82475C4ALL, (*l_36))), 0L)) && 5UL)) & g_703[0])), p_26))))), l_788[0]))) < l_1024[2][0]), l_1025)), 0x03BEL)))) & (*l_949));
                if ((7L | (safe_mul_func_int8_t_s_s(((l_788[0] = (((((safe_rshift_func_int16_t_s_u((((safe_sub_func_uint64_t_u_u((l_1037 = (&l_788[0] == (l_1035 = (void*)0))), (safe_lshift_func_uint8_t_u_s(((safe_mod_func_int16_t_s_s(((((safe_mod_func_uint64_t_u_u(l_788[2], ((((l_1050 = (((safe_mul_func_int16_t_s_s((&g_586 == (void*)0), (safe_mul_func_uint16_t_u_u(65535UL, (l_1048 &= 0x6B38L))))) & ((*l_36) = ((**l_953) || p_26))) ^ (**g_406))) && l_788[1]) & g_741) ^ 0x061FL))) , &g_370) == (void*)0) , (*l_36)), g_54)) && l_1051), 6)))) ^ l_983) != (-1L)), 11)) > p_26) , l_1052) == l_1053) || p_26)) && 0x6729L), 0xE5L))))
                { /* block id: 377 */
                    for (l_710 = 10; (l_710 <= (-13)); l_710--)
                    { /* block id: 380 */
                        (*g_1056) = ((*l_953) = &l_747[0]);
                        if (p_26)
                            continue;
                        return p_26;
                    }
                    for (g_461 = 0; (g_461 <= 0); g_461 += 1)
                    { /* block id: 388 */
                        return p_26;
                    }
                    for (g_175 = 0; (g_175 >= 24); g_175 = safe_add_func_uint8_t_u_u(g_175, 3))
                    { /* block id: 393 */
                        (*l_36) |= 0xEEA4488EL;
                        (*g_1060) = p_26;
                    }
                    for (l_906 = 0; (l_906 == 30); l_906 = safe_add_func_uint32_t_u_u(l_906, 1))
                    { /* block id: 399 */
                        if ((*l_36))
                            break;
                    }
                }
                else
                { /* block id: 402 */
                    int64_t *l_1073 = &g_122[2];
                    uint32_t *l_1075 = (void*)0;
                    int32_t l_1085 = 1L;
                    for (g_777 = 0; (g_777 != 16); ++g_777)
                    { /* block id: 405 */
                        uint32_t *l_1068[8][4][8] = {{{&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461,(void*)0,&g_461},{&g_892,(void*)0,&g_175,(void*)0,&g_892,&g_892,(void*)0,&g_175},{&g_892,&g_892,(void*)0,&g_175,(void*)0,&g_892,&g_892,(void*)0},{&g_461,(void*)0,(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_175,&g_175,(void*)0,(void*)0,&g_892,(void*)0},{&g_175,&g_892,&g_175,&g_461,&g_461,&g_175,&g_892,&g_175},{(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461,(void*)0},{(void*)0,(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461}},{{&g_175,&g_461,&g_461,&g_175,&g_892,&g_175,&g_461,&g_461},{&g_461,&g_892,(void*)0,(void*)0,&g_892,&g_461,&g_892,(void*)0},{&g_175,&g_892,&g_175,&g_461,&g_461,&g_175,&g_892,&g_175},{(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461,(void*)0}},{{(void*)0,(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461},{&g_175,&g_461,&g_461,&g_175,&g_892,&g_175,&g_461,&g_461},{&g_461,&g_892,(void*)0,(void*)0,&g_892,&g_461,&g_892,(void*)0},{&g_175,&g_892,&g_175,&g_461,&g_461,&g_175,&g_892,&g_175}},{{(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461,(void*)0},{(void*)0,(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461},{&g_175,&g_461,&g_461,&g_175,&g_892,&g_175,&g_461,&g_461},{&g_461,&g_892,(void*)0,(void*)0,&g_892,&g_461,&g_892,(void*)0}},{{&g_175,&g_892,&g_175,&g_461,&g_461,&g_175,&g_892,&g_175},{(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461,(void*)0},{(void*)0,(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461},{&g_175,&g_461,&g_461,&g_175,&g_892,&g_175,&g_461,&g_461}},{{&g_461,&g_892,(void*)0,(void*)0,&g_892,&g_461,&g_892,(void*)0},{&g_175,&g_892,&g_175,&g_461,&g_461,&g_175,&g_892,&g_175},{(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461,(void*)0},{(void*)0,(void*)0,&g_461,(void*)0,&g_461,(void*)0,(void*)0,&g_461}},{{&g_175,&g_461,&g_461,&g_175,&g_892,(void*)0,&g_175,&g_175},{&g_175,(void*)0,&g_892,&g_892,(void*)0,&g_175,(void*)0,&g_892},{(void*)0,(void*)0,(void*)0,&g_175,&g_175,(void*)0,(void*)0,(void*)0},{&g_461,&g_175,&g_892,&g_175,&g_461,&g_461,&g_175,&g_892}}};
                        int32_t l_1070 = 0x59E294FEL;
                        uint8_t * const *l_1082 = (void*)0;
                        uint32_t **l_1084 = &g_739;
                        uint32_t ***l_1083 = &l_1084;
                        int i, j, k;
                        (*l_1028) |= (((safe_lshift_func_int8_t_s_u((safe_unary_minus_func_uint64_t_u(((*g_619) == (l_1069 = l_1068[0][1][0])))), 2)) | ((((l_894 |= ((**l_1084) = ((18446744073709551614UL > (((l_1070 , (safe_lshift_func_int8_t_s_u(((((((g_1074 = l_1073) == &g_122[3]) , l_1075) != ((l_788[0] = ((((safe_rshift_func_int16_t_s_u((safe_div_func_uint64_t_u_u((safe_add_func_int16_t_s_s((l_1082 == &l_810), g_589[0][2][2])), 0x70831264CCD51D0BLL)), p_25)) | (*g_739)) >= 1L) > 65535UL)) , &g_461)) , (void*)0) != l_1083), 5))) == (*g_798)) != (**l_953))) == l_1070))) && p_26) < (**l_953)) , 0UL)) && l_1085);
                    }
                    return p_25;
                }
            }
            else
            { /* block id: 415 */
                int32_t *l_1086 = &l_744;
                int32_t *l_1087[2][3][9] = {{{&l_710,&l_710,&l_710,&l_746,(void*)0,&l_788[0],&l_862,&l_862,&l_788[0]},{(void*)0,&l_788[2],&l_747[0],&l_788[2],(void*)0,&g_304,(void*)0,&l_710,&l_710},{&l_743,&l_894,&l_788[0],&l_746,&l_788[0],&l_894,&l_743,(void*)0,&l_743}},{{&l_813[0],&l_1050,&l_788[2],&g_304,&l_744,&g_304,&l_788[2],&l_1050,&l_813[0]},{&l_894,&l_1048,&l_862,(void*)0,(void*)0,&l_788[0],(void*)0,(void*)0,&l_862},{(void*)0,(void*)0,&l_710,&l_747[0],&l_1050,&l_710,&l_813[0],&l_710,&l_1050}}};
                int32_t l_1088 = 0xE677C40DL;
                int16_t l_1090 = 3L;
                int32_t ***l_1096 = &l_1094;
                int i, j, k;
                l_1091++;
                (*l_1096) = l_1094;
            }
        }
    }
    else
    { /* block id: 420 */
        const int8_t l_1121 = 0xADL;
        int32_t l_1147[6] = {0xFBDB92B6L,0xFBDB92B6L,0xFBDB92B6L,0xFBDB92B6L,0xFBDB92B6L,0xFBDB92B6L};
        int32_t *l_1155 = &l_710;
        int32_t * const l_1196 = (void*)0;
        uint32_t ****l_1218 = &g_1214;
        const uint64_t l_1278 = 0x09ABB14803AA5D64LL;
        uint8_t ***l_1284 = &g_1282;
        const float **l_1346 = (void*)0;
        int16_t l_1376 = 0L;
        uint32_t l_1430 = 0xBED28CDFL;
        const float ***l_1443 = (void*)0;
        uint32_t l_1490 = 1UL;
        uint16_t * const **l_1549 = (void*)0;
        uint16_t * const l_1552 = &g_1553;
        uint16_t * const *l_1551 = &l_1552;
        uint16_t * const **l_1550 = &l_1551;
        const uint64_t *l_1560[3];
        const uint64_t **l_1559[9][1] = {{&l_1560[1]},{(void*)0},{&l_1560[1]},{(void*)0},{&l_1560[1]},{(void*)0},{&l_1560[1]},{(void*)0},{&l_1560[1]}};
        const uint64_t ***l_1558 = &l_1559[6][0];
        const int8_t l_1563 = (-8L);
        int32_t l_1564 = 0xEAA49229L;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1560[i] = &g_701;
        for (l_746 = (-9); (l_746 < 0); l_746 = safe_add_func_uint16_t_u_u(l_746, 3))
        { /* block id: 423 */
            int32_t l_1103[6] = {0x2FAE1ABBL,0x2FAE1ABBL,0x2FAE1ABBL,0x2FAE1ABBL,0x2FAE1ABBL,0x2FAE1ABBL};
            int8_t *l_1116 = &g_777;
            int32_t l_1119 = 0x6444AFF6L;
            uint8_t l_1120 = 253UL;
            int16_t *l_1122[10][7] = {{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,(void*)0,&g_425,&g_425,(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,(void*)0,&g_425,&g_425,(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,(void*)0,&g_425,&g_425,(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425}};
            uint32_t l_1129 = 0x21219C39L;
            union U0 *l_1137 = &g_133;
            uint16_t l_1148[1][10] = {{0x951FL,0xA896L,0xA896L,0x951FL,0xA896L,0xA896L,0x951FL,0xA896L,0xA896L,0x951FL}};
            int32_t l_1164[10] = {0x1F04AC62L,0x1F04AC62L,1L,0L,1L,0x1F04AC62L,0x1F04AC62L,1L,0L,0xC98B6A83L};
            int32_t l_1199 = (-4L);
            int32_t l_1238 = 0x6014D29DL;
            int32_t l_1239 = 0xC83AB383L;
            int32_t l_1240 = 0x06FBFE15L;
            int32_t l_1241[7];
            uint32_t l_1243 = 0x34AFABD8L;
            uint8_t **l_1291 = &g_370;
            float * const l_1339 = &g_464;
            float * const *l_1338 = &l_1339;
            int8_t l_1379 = 0x14L;
            int32_t *l_1445[2];
            uint32_t ** const l_1519 = (void*)0;
            uint32_t ** const *l_1536[4] = {&l_1519,&l_1519,&l_1519,&l_1519};
            uint32_t ** const **l_1535 = &l_1536[2];
            uint8_t l_1543 = 0x8EL;
            int i, j;
            for (i = 0; i < 7; i++)
                l_1241[i] = 0L;
            for (i = 0; i < 2; i++)
                l_1445[i] = (void*)0;
        }
        l_1564 ^= ((l_1147[0] |= (((((**g_1191) = l_1436) != ((*l_1550) = l_1436)) | (((safe_mod_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u(((((*l_1155) = p_25) > (0UL | (((0x53FCFD12L != ((****l_1218) &= (0x08B4FF84L >= (((*g_798) , l_1558) != ((((safe_mod_func_uint16_t_u_u(p_26, 0xDF23L)) ^ p_25) >= (*g_144)) , (void*)0))))) > 0x3EL) & 2UL))) && (*l_1155)), l_1563)), 0x652BE4AFL)) <= p_26) && 0x01L)) , 0xF5B3168AL)) <= p_25);
    }
    for (g_54 = 0; (g_54 > 24); g_54++)
    { /* block id: 627 */
        int32_t l_1567[2];
        int i;
        for (i = 0; i < 2; i++)
            l_1567[i] = 0L;
        (*l_36) &= (p_26 <= p_26);
        for (g_314 = 0; (g_314 >= 0); g_314 -= 1)
        { /* block id: 631 */
            const float l_1568 = 0x2.4FF543p-55;
            uint8_t l_1571 = 0xBDL;
            int i;
            l_1571 = ((((*g_798) = ((g_703[g_314] = l_1567[0]) , (((0x5517L && (((-1L) & (8UL >= (*g_1074))) || p_25)) < ((l_1569 < (*g_1074)) , (+253UL))) < g_589[1][0][7]))) < p_26) , p_25);
        }
    }
    for (g_547 = 0; (g_547 == 4); ++g_547)
    { /* block id: 639 */
        uint16_t ***l_1575[1][8][9] = {{{(void*)0,(void*)0,(void*)0,&l_1436,&l_1436,&l_1436,&l_1436,(void*)0,(void*)0},{&g_406,&g_406,&g_406,&l_1436,&l_1436,&l_1436,(void*)0,&g_406,(void*)0},{&l_1436,(void*)0,&l_1436,&l_1436,(void*)0,&l_1436,&l_1436,&l_1436,(void*)0},{(void*)0,&g_406,(void*)0,&g_406,&g_406,&g_406,&g_406,&g_406,(void*)0},{(void*)0,(void*)0,&l_1436,(void*)0,&g_406,(void*)0,&l_1436,(void*)0,(void*)0},{(void*)0,&g_406,&g_406,&g_406,&g_406,&g_406,(void*)0,&g_406,(void*)0},{(void*)0,&l_1436,&l_1436,&l_1436,(void*)0,&l_1436,&l_1436,(void*)0,&l_1436},{(void*)0,&g_406,(void*)0,&l_1436,&l_1436,&l_1436,&g_406,&g_406,&g_406}}};
        uint16_t ****l_1574 = &l_1575[0][3][8];
        uint16_t ***l_1577 = (void*)0;
        uint16_t ****l_1576 = &l_1577;
        int32_t *l_1578[9] = {&l_744,&l_744,&l_744,&l_744,&l_744,&l_744,&l_744,&l_744,&l_744};
        int i, j, k;
        (*l_1576) = ((*l_1574) = &g_406);
        (*l_36) &= (l_1220 == ((****g_1191) , l_1095[0]));
        l_1578[3] = l_1578[2];
        (*g_1342) = p_25;
    }
    return p_26;
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_54 g_50 g_78 g_33 g_117 g_29 g_133 g_122 g_143 g_118 g_147 g_167 g_182 g_39 g_133.f0 g_38 g_230 g_235 g_145 g_195 g_175 g_304 g_320 g_425 g_144 g_407 g_301 g_179 g_458 g_406 g_314 g_409 g_369 g_370 g_100 g_585 g_619 g_587 g_589
 * writes: g_78 g_118 g_122 g_39 g_33 g_167 g_175 g_179 g_50 g_230 g_301 g_314 g_461 g_195 g_547 g_586 g_464
 */
static int64_t  func_42(uint32_t  p_43, uint8_t  p_44, uint32_t  p_45)
{ /* block id: 9 */
    uint64_t l_64 = 0x4F7A324C99FFFBC9LL;
    union U0 l_75 = {4294967295UL};
    uint16_t *l_77 = &g_78;
    int32_t *l_79[6];
    float *l_675 = &g_464;
    int i;
    for (i = 0; i < 6; i++)
        l_79[i] = &g_37;
    (*l_675) = (safe_add_func_float_f_f((g_37 >= (g_54 == ((safe_mul_func_float_f_f(func_59(l_64, func_65(func_69(l_64, (l_75 , g_50), ((&g_33 != (((((*l_77) ^= (!0L)) , g_37) < 0xE.4E412Cp-21) , (void*)0)) | 0x2DL), l_79[4], g_37), &g_304, g_587), g_304, g_304), 0x9.DAFB6Dp-61)) >= g_589[0][2][2]))), g_133.f0));
    for (g_167 = 0; (g_167 < 29); g_167 = safe_add_func_int16_t_s_s(g_167, 6))
    { /* block id: 258 */
        int16_t *l_678 = &g_425;
        int32_t l_681 = 0L;
        int32_t l_682 = 0x74818453L;
        int32_t l_683 = 0x40F65552L;
        int32_t l_684 = 0xF23357B2L;
        int32_t l_685 = 0xB23EFED8L;
        int32_t l_686 = (-3L);
        int32_t l_687 = (-2L);
        int32_t l_688 = 0xAE8C211CL;
        uint64_t l_689 = 0xE49C02516C90904ELL;
        (*g_182) = ((l_678 != (void*)0) != (safe_sub_func_int64_t_s_s(l_681, (-1L))));
        --l_689;
        return p_43;
    }
    return g_133.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_320 g_179
 * writes:
 */
static float  func_59(int32_t  p_60, uint32_t  p_61, uint8_t  p_62, int8_t  p_63)
{ /* block id: 253 */
    return (*g_320);
}


/* ------------------------------------------ */
/* 
 * reads : g_33
 * writes: g_33
 */
static uint32_t  func_65(int32_t ** p_66, int32_t * p_67, uint64_t * p_68)
{ /* block id: 243 */
    uint64_t l_671 = 18446744073709551608UL;
    for (g_33 = 2; (g_33 <= 1); g_33 = safe_sub_func_int8_t_s_s(g_33, 2))
    { /* block id: 246 */
        uint16_t l_669 = 6UL;
        uint32_t l_670 = 4294967288UL;
        l_670 = (l_669 &= (&g_370 != &g_370));
        if (l_669)
            break;
    }
    ++l_671;
    return l_671;
}


/* ------------------------------------------ */
/* 
 * reads : g_50 g_78 g_37 g_33 g_117 g_29 g_133 g_122 g_143 g_118 g_147 g_54 g_167 g_182 g_39 g_133.f0 g_38 g_230 g_235 g_145 g_195 g_175 g_304 g_320 g_425 g_144 g_407 g_301 g_179 g_458 g_406 g_314 g_409 g_369 g_370 g_100 g_585 g_619
 * writes: g_118 g_122 g_78 g_39 g_33 g_167 g_175 g_179 g_50 g_230 g_301 g_314 g_461 g_195 g_547 g_586
 */
static int32_t ** func_69(const int32_t  p_70, uint32_t  p_71, uint8_t  p_72, int32_t * p_73, uint32_t  p_74)
{ /* block id: 11 */
    int32_t l_85 = 0x04A8F9B1L;
    uint64_t *l_86 = (void*)0;
    int32_t l_88 = (-1L);
    int32_t l_89 = 1L;
    int32_t l_91 = 0x7F693945L;
    int32_t l_94 = 1L;
    int32_t l_95[10][3][8] = {{{0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L},{0x86F5C60DL,3L,0L,3L,0x86F5C60DL,(-9L),0x86F5C60DL,3L},{0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L}},{{0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L},{0x86F5C60DL,3L,0L,3L,0x86F5C60DL,(-9L),0x86F5C60DL,3L},{0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L}},{{0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L},{0x86F5C60DL,3L,0L,3L,0x86F5C60DL,(-9L),0x86F5C60DL,3L},{0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L}},{{0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L},{0x86F5C60DL,3L,0L,3L,0x86F5C60DL,(-9L),0x86F5C60DL,3L},{0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L}},{{0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L,0x86F5C60DL,0L},{0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L},{0L,0L,0L,(-9L),0xD07FB140L,(-9L),0L,0L}},{{0xD07FB140L,(-9L),0L,0L,0L,(-9L),0xD07FB140L,(-9L)},{0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L},{0L,0L,0L,(-9L),0xD07FB140L,(-9L),0L,0L}},{{0xD07FB140L,(-9L),0L,0L,0L,(-9L),0xD07FB140L,(-9L)},{0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L},{0L,0L,0L,(-9L),0xD07FB140L,(-9L),0L,0L}},{{0xD07FB140L,(-9L),0L,0L,0L,(-9L),0xD07FB140L,(-9L)},{0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L},{0L,0L,0L,(-9L),0xD07FB140L,(-9L),0L,0L}},{{0xD07FB140L,(-9L),0L,0L,0L,(-9L),0xD07FB140L,(-9L)},{0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L},{0L,0L,0L,(-9L),0xD07FB140L,(-9L),0L,0L}},{{0xD07FB140L,(-9L),0L,0L,0L,(-9L),0xD07FB140L,(-9L)},{0xD07FB140L,0L,0x86F5C60DL,0L,0xD07FB140L,3L,0xD07FB140L,0L},{0L,0L,0L,(-9L),0xD07FB140L,(-9L),0L,0L}}};
    uint8_t l_101[1];
    int16_t l_123[4] = {(-2L),(-2L),(-2L),(-2L)};
    union U0 l_130 = {7UL};
    float *l_192 = &g_179;
    uint16_t l_575[4][1];
    uint32_t l_590 = 1UL;
    uint16_t l_635 = 65529UL;
    uint32_t l_642 = 1UL;
    int32_t *l_645 = &l_95[6][1][3];
    int32_t *l_646 = &g_314;
    int32_t *l_647 = (void*)0;
    int32_t *l_648 = &l_95[6][2][1];
    int32_t *l_649 = &g_304;
    float l_650[8][8] = {{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)},{(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1),(-0x1.8p+1),(-0x1.9p+1)}};
    int32_t *l_651 = &l_89;
    int32_t *l_652 = &g_304;
    int32_t *l_653 = &g_589[0][2][2];
    int32_t *l_654 = &g_589[1][1][1];
    int32_t *l_655 = &g_314;
    int32_t *l_656 = &g_118;
    int32_t *l_657 = (void*)0;
    int32_t *l_658 = &g_118;
    int32_t *l_659[1][5][1];
    int32_t l_660[9] = {(-3L),(-1L),(-3L),(-1L),(-3L),(-1L),(-3L),(-1L),(-3L)};
    int32_t l_661 = 4L;
    int32_t l_662 = 0xFC575566L;
    uint8_t l_663 = 252UL;
    int32_t **l_666 = &l_654;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_101[i] = 1UL;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
            l_575[i][j] = 65535UL;
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 1; k++)
                l_659[i][j][k] = (void*)0;
        }
    }
    for (p_71 = 0; (p_71 <= 14); p_71 = safe_add_func_int16_t_s_s(p_71, 4))
    { /* block id: 14 */
        int64_t l_82 = 0x1C9C8A038AA138A8LL;
        int32_t l_90 = (-1L);
        int32_t l_92 = 0x14D86134L;
        int32_t l_93 = 0xCC04BECFL;
        int32_t l_96 = 0x7022C981L;
        int32_t l_97 = 0L;
        int32_t l_98 = 0L;
        int32_t l_99[10] = {0x068EADFDL,0x068EADFDL,0x83BFB6ABL,0x068EADFDL,0x068EADFDL,0x83BFB6ABL,0x068EADFDL,0x068EADFDL,0x83BFB6ABL,0x068EADFDL};
        int i;
        if (l_82)
            break;
        if ((((safe_rshift_func_uint8_t_u_s(l_85, 1)) >= ((void*)0 == l_86)) && p_70))
        { /* block id: 16 */
            int32_t *l_87[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            ++l_101[0];
        }
        else
        { /* block id: 18 */
            int32_t l_110 = 0x9D46BF4AL;
            uint64_t **l_115 = &l_86;
            uint64_t *l_116 = &g_33;
            for (l_92 = 0; (l_92 <= (-19)); l_92 = safe_sub_func_uint32_t_u_u(l_92, 5))
            { /* block id: 21 */
                int32_t *l_106 = &l_95[0][2][7];
                (*l_106) = 0x9CABD979L;
            }
            if (l_88)
                break;
            (*g_117) = ((0x0368L < (safe_rshift_func_int16_t_s_u(g_50, (l_88 > 0L)))) == (((((((!l_110) == g_78) | ((safe_sub_func_int64_t_s_s((((*l_115) = (((*p_73) | (safe_add_func_uint16_t_u_u((p_72 , g_33), l_92))) , l_86)) != l_116), 18446744073709551615UL)) || 1L)) >= g_78) <= l_110) >= 0x24L) != p_74));
        }
    }
    if ((*p_73))
    { /* block id: 29 */
        int64_t *l_121 = &g_122[3];
        int32_t **l_125 = &g_39[0];
        int32_t ***l_124 = &l_125;
        uint16_t *l_139 = &g_78;
        int32_t *l_142 = &l_94;
        int32_t *l_146 = &l_89;
        (*l_124) = ((safe_sub_func_int64_t_s_s(((*l_121) = 0x0FF8D21F61EE9ED1LL), l_123[1])) , (void*)0);
        (*l_146) |= (safe_sub_func_uint16_t_u_u((safe_add_func_int32_t_s_s(((l_130 , 0x02A9B449L) ^ (((safe_mul_func_int8_t_s_s(g_29, (((g_37 , g_133) , ((*l_142) = (((safe_div_func_uint32_t_u_u((p_74 < (((safe_mod_func_int32_t_s_s((*p_73), g_78)) , (~(((*l_139)--) <= 0x33B9L))) > 3L)), g_122[3])) , p_71) & p_70))) | (*p_73)))) , g_143[1]) != &l_86)), (*p_73))), g_118));
    }
    else
    { /* block id: 35 */
        const uint8_t l_168[8][6] = {{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L},{0xF2L,0xF2L,0x68L,0xF2L,0xF2L,0x68L}};
        int32_t l_196 = 6L;
        int32_t l_219 = 0x27EE8240L;
        int32_t l_252 = 0L;
        int32_t l_254 = 0xEBF65EF6L;
        int32_t l_255[2][4] = {{0x29CBA65DL,0x29CBA65DL,1L,0x29CBA65DL},{0x29CBA65DL,(-1L),(-1L),0x29CBA65DL}};
        uint32_t l_268[7][3];
        uint32_t l_299 = 4294967294UL;
        int32_t *l_351 = &l_219;
        uint64_t *l_427 = &g_230[3];
        uint64_t *l_436 = &g_33;
        int32_t ***l_437 = (void*)0;
        int32_t *l_438 = &l_255[1][2];
        float l_482 = 0x7.2p+1;
        float *l_485[1][1][10] = {{{&l_482,&l_482,&l_482,&l_482,&l_482,&l_482,&l_482,&l_482,&l_482,&l_482}}};
        int8_t l_486 = 5L;
        uint64_t l_509 = 0xCCD08C5B8456DB66LL;
        int32_t **l_517 = &l_438;
        int8_t l_550 = 0x8DL;
        int i, j, k;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 3; j++)
                l_268[i][j] = 0xF5A4CC54L;
        }
        (*g_147) = &l_95[6][1][3];
        for (g_118 = 0; (g_118 != (-18)); g_118--)
        { /* block id: 39 */
            uint64_t *l_154 = &g_33;
            int8_t *l_165 = (void*)0;
            int8_t *l_166 = &g_167;
            uint32_t *l_174 = &g_175;
            int32_t l_176[4];
            float *l_178 = &g_179;
            int i;
            for (i = 0; i < 4; i++)
                l_176[i] = 0x6BA350D5L;
            (*l_178) = ((((0x5100L == (((((safe_mod_func_uint8_t_u_u(((safe_sub_func_uint64_t_u_u(((*l_154)--), ((l_101[0] , ((safe_mod_func_uint16_t_u_u((((*l_174) = ((safe_mod_func_int16_t_s_s(((safe_add_func_uint8_t_u_u(((safe_mod_func_int64_t_s_s((((*l_166) = l_94) == l_168[2][1]), (safe_lshift_func_int16_t_s_u(0x9067L, ((safe_mod_func_uint64_t_u_u(0xB725B5AC07EA974BLL, p_70)) , (((g_37 && (safe_unary_minus_func_int64_t_s((&p_72 == &l_101[0])))) < 0xE5L) ^ l_88)))))) == p_72), 0xB1L)) >= p_74), 65534UL)) & 0L)) || 0x2D8F591BL), l_176[0])) ^ l_168[2][1])) != (*p_73)))) , l_176[0]), l_176[2])) || (-7L)) <= 0x2CL) >= p_70) , p_74)) && 0x2277L) , g_54) <= l_123[1]);
        }
        for (g_167 = 2; (g_167 >= 0); g_167 -= 1)
        { /* block id: 47 */
            union U0 l_185 = {0UL};
            int32_t l_220 = 0x78321BB0L;
            uint64_t l_223[5];
            int32_t l_253 = 0xFF4D61BFL;
            int32_t l_262 = 0xF8220F61L;
            int8_t l_263 = 1L;
            int32_t l_265 = 0x1F0ADB63L;
            int64_t l_266[6][6][2] = {{{0xBC4FD47F257AD213LL,(-6L)},{0xE0209112D69AF1FDLL,0x3F12BD3F51311250LL},{0x9CA68F6F8AA92CA9LL,0x3F12BD3F51311250LL},{0xE0209112D69AF1FDLL,(-6L)},{0xBC4FD47F257AD213LL,(-1L)},{0x3BFBB5E838CCD62BLL,0x3BFBB5E838CCD62BLL}},{{(-6L),0x9CA68F6F8AA92CA9LL},{7L,1L},{(-4L),0x1D48B72D27B8E15BLL},{2L,(-4L)},{7L,0xE0209112D69AF1FDLL},{7L,(-4L)}},{{2L,0x1D48B72D27B8E15BLL},{(-4L),1L},{7L,0x9CA68F6F8AA92CA9LL},{(-6L),0x3BFBB5E838CCD62BLL},{0x3BFBB5E838CCD62BLL,(-1L)},{0xBC4FD47F257AD213LL,(-6L)}},{{0xE0209112D69AF1FDLL,0x3F12BD3F51311250LL},{0x9CA68F6F8AA92CA9LL,0x3F12BD3F51311250LL},{0xE0209112D69AF1FDLL,(-6L)},{0xBC4FD47F257AD213LL,(-1L)},{0x3BFBB5E838CCD62BLL,0x3BFBB5E838CCD62BLL},{(-6L),0x9CA68F6F8AA92CA9LL}},{{7L,1L},{(-4L),0x1D48B72D27B8E15BLL},{2L,(-4L)},{7L,0xE0209112D69AF1FDLL},{7L,(-4L)},{2L,0x1D48B72D27B8E15BLL}},{{(-4L),1L},{7L,0x9CA68F6F8AA92CA9LL},{(-6L),0x3BFBB5E838CCD62BLL},{0x3BFBB5E838CCD62BLL,(-1L)},{0xBC4FD47F257AD213LL,(-6L)},{0xE0209112D69AF1FDLL,0x3F12BD3F51311250LL}}};
            int32_t l_267 = 0xD5C616B5L;
            const uint64_t *l_293 = &l_223[3];
            const uint64_t * const * const l_292 = &l_293;
            uint8_t l_313 = 255UL;
            int32_t l_347 = 0L;
            uint8_t l_348 = 0x2EL;
            float *l_366 = &g_179;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_223[i] = 0x2D74987338087D1ALL;
            for (g_50 = 0; (g_50 <= 6); g_50 += 1)
            { /* block id: 50 */
                int64_t *l_193[5][2] = {{&g_122[(g_167 + 4)],&g_122[(g_167 + 4)]},{&g_122[(g_167 + 4)],&g_122[(g_167 + 4)]},{&g_122[(g_167 + 4)],&g_122[(g_167 + 4)]},{&g_122[(g_167 + 4)],&g_122[(g_167 + 4)]},{&g_122[(g_167 + 4)],&g_122[(g_167 + 4)]}};
                int8_t *l_194[8][7][4] = {{{&g_195,&g_167,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,&g_195,&g_167}},{{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0}},{{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167}},{{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167}},{{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0}},{{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167}},{{&g_167,(void*)0,&g_195,&g_167},{(void*)0,&g_167,(void*)0,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,(void*)0,(void*)0,&g_167},{&g_167,&g_167,&g_195,(void*)0},{&g_167,&g_167,&g_195,&g_167},{&g_167,&g_195,&g_195,(void*)0}},{{(void*)0,(void*)0,&g_195,(void*)0},{&g_167,(void*)0,&g_167,(void*)0},{(void*)0,&g_195,&g_195,(void*)0},{&g_195,(void*)0,&g_195,(void*)0},{(void*)0,(void*)0,&g_167,(void*)0},{&g_167,&g_195,&g_195,(void*)0},{(void*)0,(void*)0,&g_195,(void*)0}}};
                int32_t *l_197 = (void*)0;
                int32_t *l_198 = (void*)0;
                int32_t *l_199 = &g_118;
                uint16_t l_221 = 65529UL;
                int32_t l_245 = 1L;
                int32_t l_250[4][10][6] = {{{(-7L),0x2670D765L,0x47AB9F0DL,0x01D9B859L,1L,0xB2E8BE2CL},{0x18DA6E6BL,0xBB69EA11L,0L,0x2670D765L,0xFB4CFB21L,(-7L)},{(-6L),1L,0x600257ECL,7L,1L,0x47AB9F0DL},{0xFB4CFB21L,(-6L),(-1L),0x2670D765L,0x2D29FB02L,0x7B5E379BL},{0L,1L,(-1L),0x01D9B859L,7L,0xCAD3B194L},{0xBB69EA11L,0x18DA6E6BL,(-1L),0x67BD36F7L,0xFB4CFB21L,0x7B5E379BL},{1L,0x67BD36F7L,(-1L),0xBB69EA11L,0x7B5E379BL,0x47AB9F0DL},{(-7L),1L,0x600257ECL,0x600257ECL,1L,(-7L)},{0L,0x67BD36F7L,0L,1L,(-6L),0xB2E8BE2CL},{(-6L),0x18DA6E6BL,0x47AB9F0DL,(-1L),1L,0x9386137AL}},{{(-6L),1L,(-1L),1L,1L,0L},{0L,(-6L),1L,0x600257ECL,7L,1L},{(-7L),1L,(-1L),0xBB69EA11L,(-6L),0L},{1L,0xBB69EA11L,0xCAD3B194L,0x67BD36F7L,0x7B5E379BL,0x9386137AL},{0xBB69EA11L,0x2670D765L,0x600257ECL,0x01D9B859L,0x7B5E379BL,0xB2E8BE2CL},{0L,0xBB69EA11L,1L,0x2670D765L,(-6L),(-7L)},{0xFB4CFB21L,1L,0x47AB9F0DL,7L,7L,0x47AB9F0DL},{(-6L),(-6L),0xCAD3B194L,0x2670D765L,1L,0x7B5E379BL},{0x18DA6E6BL,1L,1L,0x01D9B859L,1L,0xCAD3B194L},{(-7L),0x18DA6E6BL,1L,0x67BD36F7L,(-6L),0x7B5E379BL}},{{0x7B5E379BL,0x67BD36F7L,0xCAD3B194L,0xBB69EA11L,1L,0x47AB9F0DL},{0xBB69EA11L,1L,0x47AB9F0DL,0x600257ECL,0x7B5E379BL,(-7L)},{0x18DA6E6BL,0x67BD36F7L,1L,1L,0xFB4CFB21L,0xB2E8BE2CL},{0xFB4CFB21L,0x18DA6E6BL,0x600257ECL,(-1L),7L,0x9386137AL},{0xFB4CFB21L,1L,0xCAD3B194L,1L,0x2D29FB02L,0L},{0x18DA6E6BL,(-6L),(-1L),0x600257ECL,1L,1L},{0xBB69EA11L,1L,1L,0xBB69EA11L,1L,0x47AB9F0DL},{0x9386137AL,0x18DA6E6BL,1L,1L,0x01D9B859L,1L},{0L,0x600257ECL,0xCAD3B194L,7L,0x01D9B859L,(-1L)},{1L,0x18DA6E6BL,0L,0x600257ECL,1L,0L}},{{0x2D29FB02L,0x2670D765L,(-1L),(-6L),0xFB4CFB21L,0xCAD3B194L},{1L,0x2D29FB02L,1L,0x600257ECL,0xBB69EA11L,0x9386137AL},{0x7B5E379BL,9L,0L,7L,(-6L),0L},{0x18DA6E6BL,1L,0L,1L,1L,0x9386137AL},{0x01D9B859L,1L,1L,0x18DA6E6BL,0x9386137AL,0xCAD3B194L},{0L,0x01D9B859L,(-1L),(-1L),0x01D9B859L,0L},{0x7B5E379BL,1L,0L,0x01D9B859L,0x2D29FB02L,(-1L)},{0x2D29FB02L,1L,0xCAD3B194L,1L,0xFB4CFB21L,1L},{0x2D29FB02L,9L,1L,0x01D9B859L,(-7L),0x47AB9F0DL},{0x7B5E379BL,0x2D29FB02L,0x2670D765L,(-1L),(-6L),0xFB4CFB21L}}};
                int16_t l_264 = 5L;
                int i, j, k;
                (*g_182) |= (safe_rshift_func_uint16_t_u_u(8UL, g_122[(g_167 + 2)]));
                for (g_175 = 0; (g_175 <= 6); g_175 += 1)
                { /* block id: 54 */
                    return &g_39[0];
                }
                (*l_199) ^= (safe_mod_func_uint64_t_u_u((((void*)0 != (*g_147)) == ((l_185 , 0x3AL) != ((((safe_mod_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((l_196 &= (safe_mul_func_uint8_t_u_u((p_73 != l_192), ((((g_122[(g_167 + 4)] , g_37) , (g_122[1] = p_70)) >= g_133.f0) != 0UL)))) <= p_74), (-1L))), g_133.f0)) & 0x83478F36L) && p_74) || 1UL))), g_37));
                for (l_94 = 0; (l_94 <= 2); l_94 += 1)
                { /* block id: 62 */
                    uint32_t l_231 = 0x14B757B2L;
                    int32_t l_236 = 0xBAB55B0BL;
                    int32_t l_246 = 0x97EB43B8L;
                    int32_t l_247 = 0x418C01C7L;
                    int32_t l_248 = (-9L);
                    int32_t l_249 = 0xD86165CEL;
                    int32_t l_251[3];
                    int16_t l_261 = 1L;
                    int32_t l_272[6];
                    uint64_t **l_294 = &l_86;
                    int16_t *l_307 = (void*)0;
                    int16_t *l_308 = &l_123[1];
                    int32_t **l_342 = &l_197;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_251[i] = (-7L);
                    for (i = 0; i < 6; i++)
                        l_272[i] = 0L;
                    if ((((safe_div_func_int16_t_s_s((+(+(((((((l_95[(g_50 + 1)][g_167][(g_167 + 1)] | 1L) , ((safe_div_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((l_220 = (safe_rshift_func_uint8_t_u_u(((void*)0 != &g_54), (((((((0UL > (l_196 = (g_122[3] = (l_219 &= (safe_div_func_int8_t_s_s((((~(0x6C7CL | (*l_199))) == (p_74 != (safe_lshift_func_uint8_t_u_u((((safe_sub_func_int8_t_s_s((safe_mod_func_int16_t_s_s((l_130 , l_196), 0xA74DL)), l_185.f0)) <= l_123[3]) , l_95[(g_50 + 1)][g_167][(g_167 + 1)]), 3)))) ^ 0x0DL), g_78)))))) , g_37) ^ p_72) || (-10L)) , (*g_38)) != &g_118) == p_72)))), 14)), g_167)) < (-1L))) ^ (*p_73)) & 0xC8A46905L) | p_71) > l_168[2][1]) < l_221))), g_133.f0)) > p_72) , (*p_73)))
                    { /* block id: 67 */
                        const float *l_224 = &g_179;
                        const float **l_225 = &l_224;
                        float **l_226 = &l_192;
                        int32_t l_227 = 0xE52F0B0BL;
                        int16_t *l_228 = &l_123[1];
                        uint8_t *l_229[10][10][2] = {{{&l_101[0],&l_101[0]},{&l_101[0],&l_101[0]},{&l_101[0],&g_54},{&g_54,&l_101[0]},{&l_101[0],&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&g_54},{(void*)0,&g_54},{(void*)0,&l_101[0]}},{{(void*)0,&g_54},{(void*)0,&g_54},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&l_101[0],&l_101[0]},{&g_54,&g_54},{&l_101[0],&l_101[0]},{&l_101[0],&l_101[0]},{&l_101[0],&g_54}},{{&g_54,&l_101[0]},{&l_101[0],&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&g_54},{(void*)0,&g_54},{(void*)0,&l_101[0]},{(void*)0,&g_54},{(void*)0,&g_54},{&g_54,&l_101[0]}},{{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&l_101[0],&l_101[0]},{&g_54,&g_54},{&l_101[0],&l_101[0]},{&l_101[0],&l_101[0]},{&l_101[0],&g_54},{&g_54,&l_101[0]},{&l_101[0],&l_101[0]},{&g_54,&l_101[0]}},{{&g_54,&l_101[0]},{&g_54,&g_54},{(void*)0,&g_54},{(void*)0,&l_101[0]},{(void*)0,&g_54},{(void*)0,&g_54},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&l_101[0],&l_101[0]}},{{&g_54,&g_54},{&l_101[0],&l_101[0]},{&l_101[0],&l_101[0]},{&l_101[0],&g_54},{&g_54,&l_101[0]},{(void*)0,(void*)0},{&l_101[0],&g_54},{&l_101[0],&l_101[0]},{(void*)0,&l_101[0]},{&g_54,(void*)0}},{{&l_101[0],&g_54},{&l_101[0],(void*)0},{&g_54,&l_101[0]},{(void*)0,&l_101[0]},{&l_101[0],&g_54},{&l_101[0],(void*)0},{(void*)0,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]}},{{&g_54,&l_101[0]},{&g_54,&l_101[0]},{(void*)0,(void*)0},{&l_101[0],&g_54},{&l_101[0],&l_101[0]},{(void*)0,&l_101[0]},{&g_54,(void*)0},{&l_101[0],&g_54},{&l_101[0],(void*)0},{&g_54,&l_101[0]}},{{(void*)0,&l_101[0]},{&l_101[0],&g_54},{&l_101[0],(void*)0},{(void*)0,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{&g_54,&l_101[0]},{(void*)0,(void*)0}},{{&l_101[0],&g_54},{&l_101[0],&l_101[0]},{(void*)0,&l_101[0]},{&g_54,(void*)0},{&l_101[0],&g_54},{&l_101[0],(void*)0},{&g_54,&l_101[0]},{(void*)0,&l_101[0]},{&l_101[0],&g_54},{&l_101[0],(void*)0}}};
                        int32_t **l_232 = (void*)0;
                        int32_t **l_233 = (void*)0;
                        int i, j, k;
                        l_95[(g_50 + 2)][g_167][(l_94 + 1)] = ((-10L) != ((*l_228) = (+(l_227 = (((l_89 = (((0x84ABL == g_78) || (((-2L) == (&g_38 != (void*)0)) == l_223[3])) != (((*l_225) = l_224) != ((*l_226) = p_73)))) && p_72) != g_37)))));
                        g_230[3] ^= ((void*)0 == l_229[6][1][0]);
                        if (l_231)
                            break;
                        (*g_235) = l_198;
                    }
                    else
                    { /* block id: 77 */
                        int32_t *l_237 = (void*)0;
                        int32_t *l_238 = &l_219;
                        int32_t *l_239 = &g_118;
                        int32_t *l_240 = &l_88;
                        int32_t *l_241 = &l_236;
                        int32_t *l_242 = &l_220;
                        int32_t *l_243[1];
                        float l_244[10] = {(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1),(-0x3.Bp+1)};
                        uint16_t l_256[2];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_243[i] = &l_236;
                        for (i = 0; i < 2; i++)
                            l_256[i] = 1UL;
                        l_256[1]++;
                        (*l_240) |= (((safe_lshift_func_int8_t_s_u(g_54, p_72)) == (p_70 ^ (l_247 <= 8L))) && g_145);
                        --l_268[0][1];
                    }
                    for (l_85 = 5; (l_85 >= 0); l_85 -= 1)
                    { /* block id: 84 */
                        int32_t **l_271 = &l_199;
                        (*l_271) = &l_252;
                    }
                    if (l_272[3])
                    { /* block id: 87 */
                        uint32_t l_279 = 1UL;
                        uint8_t *l_300[7];
                        int32_t **l_302 = &l_197;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_300[i] = &l_101[0];
                        (*l_302) = (((l_262 |= (-1L)) == (p_71 | ((((l_95[6][1][3] && (safe_mod_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(g_167, ((l_279 != ((safe_add_func_uint8_t_u_u((((g_301 = (p_72 = (((g_50 < (safe_div_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mod_func_int32_t_s_s((((((safe_rshift_func_uint16_t_u_s(((safe_div_func_uint16_t_u_u((l_292 != l_294), (safe_sub_func_int64_t_s_s(((safe_sub_func_int16_t_s_s((l_299 ^ (*l_199)), p_72)) < l_272[1]), l_94)))) >= l_272[3]), 5)) , p_71) >= g_145) != g_195) , 0xA33A6AFCL), (*p_73))) != g_175), 3)), l_95[6][1][3]))) , (void*)0) == (void*)0))) & 7UL) == 0UL), l_279)) <= 0x0D972C2FL)) < l_279))), 1L)), l_101[0]))) || p_74) >= p_71) <= p_74))) , (void*)0);
                        (*l_199) = l_255[1][1];
                    }
                    else
                    { /* block id: 93 */
                        uint32_t l_303 = 0xE17FFDABL;
                        l_303 ^= (-1L);
                        l_255[1][2] = g_304;
                    }
                    if ((safe_lshift_func_int16_t_s_u(((g_175 = (((g_314 = (((0x216F0C6BL == ((((*l_308) = p_70) , ((void*)0 == &l_250[2][0][1])) | ((l_220 = (p_74 ^ g_175)) | (safe_rshift_func_int16_t_s_s(((l_247 , ((g_122[6] , (safe_div_func_uint32_t_u_u(((l_262 = ((l_313 , l_308) == &g_78)) , p_71), 0x9666D4F1L))) , (void*)0)) != (void*)0), p_74))))) , 0xD5070CE3L) != p_74)) > 0xD9L) >= (*g_182))) || 0xF446E6B2L), p_70)))
                    { /* block id: 102 */
                        const int32_t ***l_316 = (void*)0;
                        int32_t l_317 = (-7L);
                        uint8_t *l_330 = &l_101[0];
                        union U0 l_340 = {4294967286UL};
                        int32_t *l_341 = &l_236;
                        (*g_117) ^= (l_317 ^= (!(l_316 == (void*)0)));
                        (*g_320) = (safe_div_func_float_f_f(0x7.3p-1, (((*l_199) = 1L) , p_71)));
                        (*l_341) |= ((safe_mul_func_int8_t_s_s((p_71 && (safe_sub_func_uint8_t_u_u((((g_29 , (p_74 , (safe_mul_func_int8_t_s_s(p_71, (safe_rshift_func_int16_t_s_u((l_267 >= (~(((++(*l_330)) , (((safe_div_func_int64_t_s_s(0x957603C5B6B452FFLL, (safe_unary_minus_func_uint8_t_u(((safe_mul_func_uint16_t_u_u((safe_sub_func_int8_t_s_s(g_133.f0, (l_340 , p_70))), g_118)) ^ 252UL))))) , p_72) , 0UL)) , g_230[4]))), g_78)))))) , (void*)0) != &l_123[1]), 0x04L))), p_74)) != g_230[3]);
                    }
                    else
                    { /* block id: 109 */
                        return &g_39[0];
                    }
                }
            }
        }
        if (((*l_438) ^= ((safe_mod_func_uint16_t_u_u((*l_351), (safe_add_func_int8_t_s_s((((+(p_70 != (((((p_72 < (safe_sub_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u(g_425, (((safe_unary_minus_func_int16_t_s((*l_351))) , (((*g_117) , (*g_144)) > (((((p_71 <= ((--(*l_427)) == ((*l_436) = (safe_div_func_uint64_t_u_u((safe_add_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_u((*l_351), l_88)) , (*l_351)), 0L)), (*l_351)))))) , (-1L)) == 0xA436L) | l_123[1]) > (*g_407)))) , 0x53C9L))), g_301))) == (*l_351)) , l_437) != l_437) > (*l_351)))) & 0x58L) == p_70), 0xC0L)))) <= l_123[1])))
        { /* block id: 168 */
            int32_t l_462 = 0x4399D19EL;
            float *l_463[1][10] = {{&g_464,&g_464,(void*)0,&g_464,&g_464,&g_464,&g_464,(void*)0,&g_464,&g_464}};
            int i, j;
            (*g_320) = (safe_mul_func_float_f_f(((void*)0 == &g_175), (((safe_sub_func_float_f_f((0x0.Fp+1 <= ((safe_div_func_float_f_f(((safe_sub_func_float_f_f(((((*g_320) > g_37) <= ((safe_mul_func_float_f_f((((l_88 = ((-(safe_sub_func_float_f_f((l_462 = (safe_mul_func_float_f_f(((((((safe_sub_func_float_f_f(((g_461 = (((safe_lshift_func_uint8_t_u_u(0x4FL, 5)) >= ((((g_458[0][2] >= (safe_mul_func_uint8_t_u_u(((**g_406) >= (p_71 >= l_130.f0)), 5L))) > 0x81DB3B173A381EC4LL) | p_70) != 1UL)) , l_101[0])) <= p_74), l_462)) >= 0x9.7p-1) == (-0x4.Dp+1)) != (-0x1.5p+1)) > (*l_438)) != 0x4.Ap+1), 0x0.3p+1))), g_179))) > g_122[3])) >= 0x2.873C90p+6) > p_70), p_74)) != (*l_351))) <= p_71), 0x0.9p-1)) < 0x9.08F129p+87), g_167)) < 0x1.Ap-1)), g_230[3])) < 0x3.5AFADDp-66) < (-0x5.Cp-1))));
        }
        else
        { /* block id: 173 */
            uint32_t *l_479 = &g_461;
            uint32_t **l_478 = &l_479;
            int8_t *l_481 = &g_195;
            int32_t l_483 = 0x12EF268FL;
            int32_t *l_484[5][10][5] = {{{&g_301,&g_301,&g_301,&g_301,&g_301},{&g_301,&g_301,(void*)0,(void*)0,&g_301},{&g_301,&g_301,(void*)0,(void*)0,&g_301},{&l_85,&l_85,&g_301,&l_85,&g_301},{&l_85,(void*)0,(void*)0,&g_301,&l_85},{&g_301,&l_85,(void*)0,&l_85,&l_85},{&l_85,(void*)0,&g_301,&g_301,&l_85},{&l_85,&l_85,(void*)0,&l_85,&g_301},{&g_301,&l_85,(void*)0,&l_85,&g_301},{&g_301,&l_85,&g_301,&g_301,&l_85}},{{&g_301,&g_301,&g_301,(void*)0,&l_85},{&g_301,&l_85,&l_85,&l_85,(void*)0},{(void*)0,&g_301,&g_301,&l_85,(void*)0},{&l_85,&g_301,&l_85,&g_301,(void*)0},{&g_301,&l_85,&l_85,(void*)0,(void*)0},{&g_301,&g_301,&g_301,&g_301,(void*)0},{&l_85,&l_85,&l_85,&g_301,&g_301},{&l_85,&g_301,&g_301,&l_85,&g_301},{(void*)0,(void*)0,&g_301,&g_301,&g_301},{&g_301,(void*)0,(void*)0,&g_301,&g_301}},{{&l_85,(void*)0,&g_301,&g_301,&l_85},{&g_301,&g_301,&g_301,&g_301,&g_301},{&g_301,&l_85,&l_85,&l_85,&g_301},{&l_85,&l_85,&l_85,&g_301,&g_301},{&g_301,&g_301,(void*)0,&l_85,&l_85},{&l_85,&g_301,&g_301,&g_301,(void*)0},{&l_85,&g_301,&g_301,&g_301,&g_301},{&g_301,&l_85,&g_301,&g_301,&g_301},{&g_301,&g_301,&g_301,&g_301,&g_301},{&l_85,(void*)0,&g_301,&l_85,(void*)0}},{{&l_85,&g_301,&g_301,&g_301,&l_85},{&g_301,&l_85,&g_301,&l_85,&g_301},{&l_85,&g_301,&l_85,&g_301,&g_301},{&l_85,(void*)0,&l_85,(void*)0,(void*)0},{&g_301,(void*)0,&g_301,&l_85,&l_85},{&g_301,&g_301,&g_301,&g_301,&g_301},{(void*)0,&g_301,&g_301,&g_301,&l_85},{&l_85,&l_85,&g_301,(void*)0,&g_301},{&g_301,&l_85,&g_301,&g_301,&l_85},{&l_85,(void*)0,&g_301,(void*)0,&g_301}},{{&g_301,&g_301,&g_301,(void*)0,(void*)0},{&g_301,&l_85,(void*)0,&l_85,&g_301},{&l_85,&g_301,&l_85,&g_301,&g_301},{&g_301,&l_85,&l_85,&l_85,&g_301},{&l_85,&g_301,&g_301,&l_85,&l_85},{&g_301,(void*)0,&g_301,&l_85,&g_301},{&l_85,&l_85,(void*)0,&l_85,&g_301},{(void*)0,&l_85,&g_301,&g_301,&g_301},{&g_301,&g_301,&g_301,&l_85,&g_301},{(void*)0,&g_301,&l_85,&g_301,(void*)0}}};
            int32_t l_554[8][3][3] = {{{(-1L),0xC7A6836BL,0xB23D29D2L},{1L,0xC52643BBL,1L},{(-1L),(-1L),0xB23D29D2L}},{{(-1L),0xC52643BBL,(-1L)},{(-1L),0xC7A6836BL,0xB23D29D2L},{1L,0xC52643BBL,1L}},{{(-1L),(-1L),0xB23D29D2L},{(-1L),(-1L),0x4A9BE675L},{0xC3EB8064L,(-1L),(-5L)}},{{0x19501758L,(-1L),0x19501758L},{0xC3EB8064L,0xB23D29D2L,(-5L)},{0x4A9BE675L,(-1L),0x4A9BE675L}},{{0xC3EB8064L,(-1L),(-5L)},{0x19501758L,(-1L),0x19501758L},{0xC3EB8064L,0xB23D29D2L,(-5L)}},{{0x4A9BE675L,(-1L),0x4A9BE675L},{0xC3EB8064L,(-1L),(-5L)},{0x19501758L,(-1L),0x19501758L}},{{0xC3EB8064L,0xB23D29D2L,(-5L)},{0x4A9BE675L,(-1L),0x4A9BE675L},{0xC3EB8064L,(-1L),(-5L)}},{{0x19501758L,(-1L),0x19501758L},{0xC3EB8064L,0xB23D29D2L,(-5L)},{0x4A9BE675L,(-1L),0x4A9BE675L}}};
            uint64_t **l_584 = (void*)0;
            uint8_t *l_602 = &l_101[0];
            uint8_t *l_605 = &g_50;
            int64_t l_641 = 0x6210FBB0381AC4EFLL;
            int i, j, k;
            if ((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_s(((~(g_122[3] , (g_425 | (safe_mod_func_uint64_t_u_u(((((g_301 |= ((safe_sub_func_uint16_t_u_u(((p_70 <= ((safe_mul_func_int8_t_s_s((((((*l_438) > (l_130 , (&g_461 == ((*l_478) = &g_175)))) <= ((g_314 , (~((*l_481) = (l_101[0] == 9UL)))) , l_95[6][1][3])) && (*l_438)) < p_70), 0x80L)) ^ l_95[2][0][0])) != (*p_73)), l_483)) < 0xF008L)) , l_484[0][3][0]) == l_485[0][0][7]) | (*l_351)), g_50))))) > l_486), 7)), p_71)))
            { /* block id: 177 */
                uint64_t l_508 = 1UL;
                int32_t l_516[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
                uint8_t *l_546 = &g_547;
                int i;
                if ((*p_73))
                { /* block id: 178 */
                    const int64_t l_489 = (-5L);
                    uint32_t * const *l_512 = (void*)0;
                    union U0 l_528[1] = {{0UL}};
                    int32_t l_555 = 0L;
                    int32_t l_556 = 0x073718EFL;
                    int32_t l_557 = 0xA6291E91L;
                    int32_t l_558 = 0xFA9F35B0L;
                    int i;
                    (*l_351) &= (*p_73);
                    for (g_50 = 2; (g_50 <= 5); g_50 = safe_add_func_uint8_t_u_u(g_50, 1))
                    { /* block id: 182 */
                        (*l_351) = (l_94 & p_70);
                    }
                    (*l_438) = (p_72 & ((((((((l_483 && (l_489 != (safe_mod_func_uint8_t_u_u((safe_add_func_int32_t_s_s((*p_73), g_301)), (safe_add_func_int32_t_s_s((safe_add_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u((((g_33--) == (-9L)) != 0x78E59418L), (((*l_481) = ((safe_rshift_func_uint16_t_u_u((1UL >= ((((safe_rshift_func_int16_t_s_s((g_133 , p_70), 5)) < l_508) != (*g_407)) || p_72)), (**g_406))) ^ l_85)) >= 4L))), 0x2CL)), (*g_144))), 0x6A88A9FBL)))))) , g_425) < g_458[1][2]) , l_91) > 18446744073709551615UL) , l_509) & g_78) <= p_70));
                    if (((-1L) > (*p_73)))
                    { /* block id: 188 */
                        uint32_t l_515 = 0x6B54B42EL;
                        l_516[1] = ((((&p_71 == &g_175) , (safe_mul_func_uint8_t_u_u(p_70, ((((l_512 == l_512) ^ 0x8183L) , l_427) == (void*)0)))) , (safe_div_func_int16_t_s_s(l_515, l_123[1]))) , 0x76E81FCCL);
                        return &g_39[0];
                    }
                    else
                    { /* block id: 191 */
                        uint8_t *l_539 = (void*)0;
                        uint8_t *l_540 = &g_50;
                        uint8_t *l_543 = &l_101[0];
                        uint8_t **l_545[9];
                        int32_t l_548 = 0x36187295L;
                        int32_t l_549 = 0xE6D4EA6BL;
                        int16_t *l_551 = &l_123[2];
                        int32_t *l_552 = (void*)0;
                        int32_t *l_553[10][2][5] = {{{&l_91,&l_516[1],&l_94,&l_548,&l_516[1]},{&l_252,&l_94,&l_94,&l_252,&l_548}},{{&g_37,&l_252,&l_516[1],&l_516[1],&l_516[1]},{&l_91,&l_252,&l_91,&l_548,&l_252}},{{&l_516[1],&l_94,&l_548,&l_516[1],&l_548},{&l_516[1],&l_516[1],&l_516[1],&l_252,&g_37}},{{&l_91,&g_37,&l_548,&l_548,&g_37},{&g_37,&l_94,&l_91,&g_37,&l_548}},{{&l_252,&g_37,&l_516[1],&g_37,&l_252},{&l_91,&l_516[1],&l_94,&l_548,&l_516[1]}},{{&l_252,&l_94,&l_94,&l_252,&l_548},{&g_37,&l_252,&l_516[1],&l_516[1],&l_516[1]}},{{&l_91,&l_252,&l_91,&l_548,&l_252},{&l_516[1],&l_94,&l_548,&l_516[1],&l_548}},{{&l_516[1],&l_516[1],&l_516[1],&l_252,&g_37},{&l_91,&g_37,&l_548,&l_548,&g_37}},{{&g_37,&l_94,&l_91,&g_37,&l_548},{&l_252,&g_37,&l_516[1],&g_37,&l_252}},{{&l_91,&l_516[1],&l_94,&l_548,&l_516[1]},{&l_252,&l_94,&l_94,&l_252,&l_548}}};
                        uint32_t l_559[4][3] = {{7UL,7UL,7UL},{18446744073709551615UL,0x4FE66289L,18446744073709551615UL},{7UL,7UL,7UL},{18446744073709551615UL,0x4FE66289L,18446744073709551615UL}};
                        int i, j, k;
                        for (i = 0; i < 9; i++)
                            l_545[i] = &l_539;
                        (*l_351) &= (safe_lshift_func_uint16_t_u_u(((0x95E7C46AL ^ 0xBE53F387L) < (p_71 , (safe_mod_func_uint16_t_u_u((((safe_rshift_func_uint16_t_u_u((g_304 , (safe_div_func_uint16_t_u_u((((++(**l_478)) , l_528[0]) , (((**l_517) = ((p_72--) ^ (l_548 = ((p_73 == ((l_95[9][0][4] ^= ((safe_mod_func_int16_t_s_s(((*l_551) = (safe_sub_func_int16_t_s_s((((safe_rshift_func_int8_t_s_s((-8L), (safe_lshift_func_uint8_t_u_s(((*l_543) = ((*l_540)++)), ((*l_481) = ((~(((l_546 = &g_54) != (((((((((((g_458[2][2] , 0x56B23FA6L) > p_74) , (**g_406)) ^ l_548) < (***g_409)) , l_528[0].f0) | (-8L)) < (-8L)) && 0x62D4E129L) | l_549) , (*g_369))) ^ l_550)) != g_458[0][2])))))) > l_508) & (*p_73)), 0x426CL))), g_314)) & l_528[0].f0)) , p_73)) > (*p_73))))) && g_100)), 0xA447L))), l_508)) >= p_70) >= 1UL), 1L)))), 0));
                        l_559[1][1]++;
                    }
                }
                else
                { /* block id: 205 */
                    uint8_t l_577 = 0xC8L;
                    int32_t l_578 = 0x6C9ECB0FL;
                    uint32_t l_580 = 0x15BE0E0AL;
                    (**l_517) = ((void*)0 == &g_177);
                    if (((*l_351) &= (+(safe_mul_func_int8_t_s_s(9L, (safe_mod_func_int64_t_s_s(g_314, g_133.f0)))))))
                    { /* block id: 208 */
                        int64_t *l_576 = &g_122[3];
                        int32_t *l_579[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_579[i] = &g_118;
                        l_578 |= ((((*l_438) = (l_101[0] ^ ((((*l_576) |= (safe_lshift_func_uint8_t_u_s((l_516[3] | ((**l_517) , (*g_144))), (safe_rshift_func_int16_t_s_s((safe_mod_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s(((*l_546) = (l_101[0] , (0xF7L > p_72))), l_575[0][0])), 0xCA30D54192D94DD9LL)), 12))))) >= g_37) != (*p_73)))) || l_577) ^ g_301);
                        (*l_351) ^= (*p_73);
                        l_580++;
                    }
                    else
                    { /* block id: 215 */
                        int32_t *l_583 = &l_255[1][2];
                        (*l_517) = p_73;
                        (*l_351) |= (-1L);
                        (*l_517) = (*l_517);
                        (*l_517) = l_583;
                    }
                    for (g_167 = 0; g_167 < 4; g_167 += 1)
                    {
                        l_123[g_167] = 0x81B0L;
                    }
                }
            }
            else
            { /* block id: 223 */
                int32_t *l_588[1][4][4] = {{{&l_254,&l_94,&g_118,&l_94},{&l_94,&l_255[1][2],&g_118,&g_118},{&l_254,&l_254,&l_94,&g_118},{(void*)0,&l_255[1][2],(void*)0,&l_94}}};
                int i, j, k;
                (*g_585) = l_584;
                l_590++;
            }
            if ((p_71 , (&g_458[1][0] == ((p_71 ^ (safe_mul_func_uint8_t_u_u(((((safe_lshift_func_uint8_t_u_u(((0x106BL < (!(safe_rshift_func_uint8_t_u_s(((*l_605) |= (((*l_602) = (safe_mod_func_int64_t_s_s((p_72 == (0xF9900CCAL > p_72)), l_88))) > (((safe_add_func_uint32_t_u_u((((l_554[5][2][2] , p_70) > 0xF0BB8D3DL) < 0x7FL), (*p_73))) , &p_72) != (*g_369)))), g_167)))) || 0x716BL), p_74)) != p_70) , (*p_73)) > (*p_73)), 1UL))) , &l_299))))
            { /* block id: 229 */
                uint16_t ***l_610 = &g_406;
                uint16_t ****l_611 = &l_610;
                int32_t l_612 = 3L;
                uint32_t **l_617 = &l_479;
                uint32_t ***l_618 = &l_617;
                l_554[7][2][1] = ((((*g_407)++) , (safe_mod_func_uint64_t_u_u(((((*l_611) = l_610) == (void*)0) == l_612), p_70))) ^ (((safe_lshift_func_int8_t_s_s(((safe_sub_func_int32_t_s_s((((*l_618) = l_617) != g_619), (safe_lshift_func_uint8_t_u_s(((*l_602) &= g_54), 3)))) > (safe_mod_func_int32_t_s_s((safe_mod_func_uint32_t_u_u((++(*l_479)), 0x93590B49L)), (safe_lshift_func_int8_t_s_u(((((safe_mul_func_int8_t_s_s(((safe_div_func_uint32_t_u_u((l_554[4][1][2] <= 5UL), 4294967293UL)) , p_72), l_635)) <= 0xE50AL) && (-1L)) < 0x736F8EE613D668B3LL), 3))))), l_483)) >= 0L) ^ (**l_517)));
            }
            else
            { /* block id: 236 */
                int32_t *l_636 = &l_219;
                int32_t *l_637 = &l_554[0][2][1];
                int32_t *l_638 = (void*)0;
                int32_t *l_639 = (void*)0;
                int32_t *l_640[10][8][3] = {{{&l_255[0][1],(void*)0,&l_95[6][1][3]},{&l_95[6][1][3],&l_254,&l_95[6][1][3]},{(void*)0,&l_254,&l_255[1][2]},{&l_554[7][0][0],(void*)0,&l_483},{&l_255[1][2],&l_196,(void*)0},{(void*)0,(void*)0,&l_196},{&l_255[1][2],&l_483,(void*)0},{&l_554[7][0][0],&l_255[1][2],&l_254}},{{(void*)0,&g_118,&l_95[7][0][6]},{&g_118,&g_118,&l_483},{&l_95[6][1][3],&l_255[1][2],&l_219},{&l_196,&l_483,&l_255[0][1]},{&l_95[7][0][6],(void*)0,&g_37},{&l_255[1][2],&l_196,&l_255[0][1]},{&l_254,(void*)0,&l_219},{(void*)0,&l_254,&l_483}},{{&l_88,&l_95[7][0][6],&l_95[7][0][6]},{&l_88,&l_483,&l_254},{(void*)0,&l_219,(void*)0},{&l_254,&l_255[0][1],&l_196},{&l_255[1][2],&g_37,(void*)0},{&l_95[7][0][6],&l_255[0][1],&l_483},{&l_196,&l_219,&l_255[1][2]},{&l_95[6][1][3],&l_483,&g_118}},{{&g_118,&l_95[7][0][6],&g_118},{(void*)0,&l_254,&l_255[1][2]},{&l_554[7][0][0],(void*)0,&l_483},{&l_255[1][2],&l_196,(void*)0},{(void*)0,(void*)0,&l_196},{&l_255[1][2],&l_483,(void*)0},{&l_554[7][0][0],&l_255[1][2],&l_254},{(void*)0,&g_118,&l_95[7][0][6]}},{{&g_118,&g_118,&l_483},{&l_95[6][1][3],&l_255[1][2],&l_219},{&l_196,&l_483,&l_255[0][1]},{&l_95[7][0][6],(void*)0,&g_37},{&l_255[1][2],&l_196,&l_255[0][1]},{&l_254,(void*)0,&l_219},{(void*)0,&l_254,&l_483},{&l_88,&l_95[7][0][6],&l_95[7][0][6]}},{{&l_88,&l_483,&l_254},{(void*)0,&l_219,(void*)0},{&l_254,&l_255[0][1],&l_196},{&l_255[1][2],&g_37,(void*)0},{&l_95[7][0][6],&l_255[0][1],&l_483},{&l_196,&l_219,&l_255[1][2]},{&l_95[6][1][3],&l_483,&g_118},{&g_118,&l_95[7][0][6],&g_118}},{{(void*)0,&l_254,&l_255[1][2]},{&l_554[7][0][0],(void*)0,&l_483},{&l_255[1][2],&l_196,(void*)0},{(void*)0,(void*)0,&l_196},{&l_255[1][2],&l_483,(void*)0},{&l_554[7][0][0],&l_255[1][2],&l_254},{(void*)0,&g_118,&l_95[7][0][6]},{&g_118,&g_118,&l_483}},{{&l_95[6][1][3],&l_255[1][2],&l_219},{&l_196,&l_483,&l_255[0][1]},{&l_95[7][0][6],(void*)0,&g_37},{&l_255[1][2],&l_196,&l_255[0][1]},{&l_254,(void*)0,&l_219},{(void*)0,&l_254,&l_483},{&l_88,&l_95[7][0][6],&l_95[7][0][6]},{&l_88,&l_483,&l_254}},{{(void*)0,&l_219,(void*)0},{&l_254,&l_255[0][1],&l_554[7][0][0]},{(void*)0,&l_255[1][2],&l_483},{&l_483,&l_95[6][1][3],&l_196},{&l_554[7][0][0],(void*)0,(void*)0},{&g_118,&g_37,&g_589[1][1][2]},{&g_589[1][1][2],&l_483,&g_589[1][1][2]},{(void*)0,&l_95[7][0][6],(void*)0}},{{&l_255[1][2],&l_219,&l_196},{&l_94,&l_554[7][0][0],&l_483},{&l_219,&l_483,&l_554[7][0][0]},{&l_94,&l_196,&l_219},{&l_255[1][2],(void*)0,&l_95[7][0][6]},{(void*)0,&g_589[1][1][2],&l_483},{&g_589[1][1][2],&g_589[1][1][2],&g_37},{&g_118,(void*)0,(void*)0}}};
                int i, j, k;
                l_642++;
            }
        }
    }
    --l_663;
    return &g_39[0];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_29, "g_29", print_hash_value);
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_37, "g_37", print_hash_value);
    transparent_crc(g_50, "g_50", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_118, "g_118", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_122[i], "g_122[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_133.f0, "g_133.f0", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    transparent_crc(g_167, "g_167", print_hash_value);
    transparent_crc(g_175, "g_175", print_hash_value);
    transparent_crc_bytes (&g_179, sizeof(g_179), "g_179", print_hash_value);
    transparent_crc(g_195, "g_195", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_230[i], "g_230[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_301, "g_301", print_hash_value);
    transparent_crc(g_304, "g_304", print_hash_value);
    transparent_crc(g_314, "g_314", print_hash_value);
    transparent_crc(g_425, "g_425", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_458[i][j], "g_458[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_461, "g_461", print_hash_value);
    transparent_crc_bytes (&g_464, sizeof(g_464), "g_464", print_hash_value);
    transparent_crc(g_547, "g_547", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_589[i][j][k], "g_589[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_701, "g_701", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_703[i], "g_703[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_741, "g_741", print_hash_value);
    transparent_crc(g_777, "g_777", print_hash_value);
    transparent_crc(g_791, "g_791", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_812[i][j][k], "g_812[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_892, "g_892", print_hash_value);
    transparent_crc(g_1160.f0, "g_1160.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1326[i][j], "g_1326[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1343, sizeof(g_1343), "g_1343", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1380[i], "g_1380[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1513, "g_1513", print_hash_value);
    transparent_crc(g_1553, "g_1553", print_hash_value);
    transparent_crc_bytes (&g_1615, sizeof(g_1615), "g_1615", print_hash_value);
    transparent_crc(g_1717, "g_1717", print_hash_value);
    transparent_crc(g_1770, "g_1770", print_hash_value);
    transparent_crc(g_1905, "g_1905", print_hash_value);
    transparent_crc(g_2035, "g_2035", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2129[i][j], "g_2129[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2185, "g_2185", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2410[i][j][k], "g_2410[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2414, "g_2414", print_hash_value);
    transparent_crc(g_2418, "g_2418", print_hash_value);
    transparent_crc(g_2512, "g_2512", print_hash_value);
    transparent_crc(g_2558, "g_2558", print_hash_value);
    transparent_crc(g_2773, "g_2773", print_hash_value);
    transparent_crc(g_2955, "g_2955", print_hash_value);
    transparent_crc(g_3005, "g_3005", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_3138[i], "g_3138[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 839
XXX total union variables: 18

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 21
breakdown:
   indirect level: 0, occurrence: 18
   indirect level: 1, occurrence: 3
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 6
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 29
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 24

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 339
   depth: 2, occurrence: 85
   depth: 3, occurrence: 9
   depth: 4, occurrence: 9
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 2
   depth: 9, occurrence: 1
   depth: 11, occurrence: 3
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 3
   depth: 17, occurrence: 3
   depth: 18, occurrence: 7
   depth: 19, occurrence: 2
   depth: 20, occurrence: 7
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 27, occurrence: 2
   depth: 28, occurrence: 5
   depth: 30, occurrence: 2
   depth: 31, occurrence: 5
   depth: 32, occurrence: 1
   depth: 34, occurrence: 2
   depth: 35, occurrence: 2
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1
   depth: 49, occurrence: 1

XXX total number of pointers: 750

XXX times a variable address is taken: 1545
XXX times a pointer is dereferenced on RHS: 397
breakdown:
   depth: 1, occurrence: 312
   depth: 2, occurrence: 55
   depth: 3, occurrence: 16
   depth: 4, occurrence: 11
   depth: 5, occurrence: 3
XXX times a pointer is dereferenced on LHS: 384
breakdown:
   depth: 1, occurrence: 330
   depth: 2, occurrence: 32
   depth: 3, occurrence: 15
   depth: 4, occurrence: 6
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 51
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 13407

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1556
   level: 2, occurrence: 418
   level: 3, occurrence: 177
   level: 4, occurrence: 63
   level: 5, occurrence: 17
XXX number of pointers point to pointers: 295
XXX number of pointers point to scalars: 449
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 27.3
XXX average alias set size: 1.44

XXX times a non-volatile is read: 2425
XXX times a non-volatile is write: 1189
XXX times a volatile is read: 114
XXX    times read thru a pointer: 21
XXX times a volatile is write: 51
XXX    times written thru a pointer: 4
XXX times a volatile is available for access: 3.02e+03
XXX percentage of non-volatile access: 95.6

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 339
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 39
   depth: 2, occurrence: 42
   depth: 3, occurrence: 48
   depth: 4, occurrence: 79
   depth: 5, occurrence: 97

XXX percentage a fresh-made variable is used: 15.8
XXX percentage an existing variable is used: 84.2
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


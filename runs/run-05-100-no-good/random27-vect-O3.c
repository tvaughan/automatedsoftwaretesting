/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3198228010
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile uint8_t  f0;
   int64_t  f1;
   uint32_t  f2;
   uint16_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static volatile uint64_t g_2 = 2UL;/* VOLATILE GLOBAL g_2 */
static volatile int32_t g_4[5][5] = {{0x07E17F75L,0x07E17F75L,0x07E17F75L,0x07E17F75L,0x07E17F75L},{0x3FBB4EBBL,0x3FBB4EBBL,0x3FBB4EBBL,0x3FBB4EBBL,0x3FBB4EBBL},{0x07E17F75L,0x07E17F75L,0x07E17F75L,0x07E17F75L,0x07E17F75L},{0x3FBB4EBBL,0x3FBB4EBBL,0x3FBB4EBBL,0x3FBB4EBBL,0x3FBB4EBBL},{0x07E17F75L,0x07E17F75L,0x07E17F75L,0x07E17F75L,0x07E17F75L}};
static volatile int32_t * volatile g_3 = &g_4[1][1];/* VOLATILE GLOBAL g_3 */
static uint32_t g_29 = 0x2758E92EL;
static int32_t g_41[8][7][4] = {{{(-7L),(-7L),1L,0x656540F2L},{1L,0xC9E4A043L,0x656540F2L,(-7L)},{1L,0L,1L,1L},{(-7L),(-7L),0x8B15B45BL,0xE00457A2L},{7L,0x8B15B45BL,0x42C3B03CL,(-6L)},{1L,(-9L),0x656540F2L,(-6L)},{(-6L),(-9L),0x42C3B03CL,0x255D5800L}},{{(-9L),0xC9E4A043L,(-9L),0L},{(-7L),0x42C3B03CL,0x56095DBCL,0x9733147FL},{7L,(-7L),0L,0x42C3B03CL},{0x255D5800L,1L,0L,1L},{7L,0x56095DBCL,0x56095DBCL,7L},{(-7L),0x255D5800L,(-9L),(-7L)},{(-9L),(-7L),0x42C3B03CL,1L}},{{(-6L),0x73598422L,0x656540F2L,1L},{0x9733147FL,(-7L),(-6L),(-7L)},{0xE00457A2L,0x255D5800L,0xC9E4A043L,7L},{0x42C3B03CL,0x56095DBCL,0x9733147FL,1L},{0x73598422L,1L,1L,0x42C3B03CL},{0x73598422L,(-7L),0x9733147FL,0x9733147FL},{0x42C3B03CL,0x42C3B03CL,0xC9E4A043L,0L}},{{0xE00457A2L,0xC9E4A043L,(-6L),0x255D5800L},{0x9733147FL,(-9L),0x656540F2L,(-6L)},{(-6L),(-9L),0x42C3B03CL,0x255D5800L},{(-9L),0xC9E4A043L,(-9L),0L},{(-7L),0x42C3B03CL,0x56095DBCL,0x9733147FL},{7L,(-7L),0L,0x42C3B03CL},{0x255D5800L,1L,0L,1L}},{{7L,0x56095DBCL,0x56095DBCL,7L},{(-7L),0x255D5800L,(-9L),(-7L)},{(-9L),(-7L),0x42C3B03CL,1L},{(-6L),0x73598422L,0x656540F2L,1L},{0x9733147FL,(-7L),(-6L),(-7L)},{0xE00457A2L,0x255D5800L,0xC9E4A043L,7L},{0x42C3B03CL,0x56095DBCL,0x9733147FL,1L}},{{0x73598422L,1L,1L,0x42C3B03CL},{0x73598422L,(-7L),0x9733147FL,0x9733147FL},{0x42C3B03CL,0x42C3B03CL,0xC9E4A043L,0L},{0xE00457A2L,0xC9E4A043L,(-6L),0x255D5800L},{0x9733147FL,(-9L),0x656540F2L,(-6L)},{(-6L),(-9L),0x42C3B03CL,0x255D5800L},{(-9L),0xC9E4A043L,(-9L),0L}},{{(-7L),0x42C3B03CL,0x56095DBCL,0x9733147FL},{7L,(-7L),0L,0x42C3B03CL},{0x255D5800L,1L,0L,1L},{7L,0x56095DBCL,0x56095DBCL,7L},{(-7L),0x255D5800L,(-9L),(-7L)},{(-9L),(-7L),0x42C3B03CL,1L},{(-6L),0x73598422L,0x656540F2L,1L}},{{0x9733147FL,(-7L),(-6L),(-7L)},{0xE00457A2L,0x255D5800L,0xC9E4A043L,7L},{0x42C3B03CL,0x56095DBCL,0x9733147FL,1L},{0x73598422L,1L,1L,0x42C3B03CL},{(-7L),0x56095DBCL,(-9L),(-9L)},{(-6L),(-6L),1L,(-7L)},{0L,1L,0x255D5800L,7L}}};
static int32_t * const g_40 = &g_41[4][3][2];
static int32_t *g_42 = &g_41[4][3][2];
static float g_86 = 0x9.B6842Ap+15;
static int32_t g_92[4][1] = {{0x10273394L},{0x10273394L},{0x10273394L},{0x10273394L}};
static int32_t g_93 = (-1L);
static int32_t g_97 = 0L;
static const int32_t *g_99[2] = {&g_92[2][0],&g_92[2][0]};
static const int32_t **g_98[8][8][3] = {{{&g_99[0],&g_99[1],&g_99[0]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[0],&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],(void*)0},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[0],&g_99[1],&g_99[1]}},{{&g_99[0],&g_99[1],&g_99[1]},{&g_99[0],&g_99[1],(void*)0},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],&g_99[1],(void*)0}},{{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[0],&g_99[1],&g_99[0]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[0],&g_99[0],&g_99[1]},{&g_99[0],(void*)0,&g_99[0]}},{{&g_99[1],&g_99[0],&g_99[0]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]}},{{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[0],&g_99[1],&g_99[0]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],&g_99[1],&g_99[0]}},{{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[0],(void*)0,&g_99[1]},{&g_99[1],&g_99[0],(void*)0},{&g_99[0],&g_99[0],&g_99[0]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[0],&g_99[1],&g_99[0]},{&g_99[1],&g_99[0],&g_99[1]}},{{&g_99[0],&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],(void*)0},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[0],&g_99[1],(void*)0}},{{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[0],&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[0],(void*)0,&g_99[1]},{(void*)0,&g_99[1],&g_99[1]},{&g_99[1],&g_99[0],&g_99[0]},{&g_99[1],&g_99[1],&g_99[1]}}};
static int32_t g_100 = 0x7A36831AL;
static uint64_t g_121 = 18446744073709551607UL;
static int16_t g_123[8] = {0xD012L,6L,0xD012L,0xD012L,6L,0xD012L,0xD012L,6L};
static uint32_t g_126[7] = {0xD30447F0L,0x6F7154ECL,0x6F7154ECL,0xD30447F0L,0x6F7154ECL,0x6F7154ECL,0xD30447F0L};
static int32_t g_133 = (-1L);
static int64_t g_135 = 0xC62DB5C1620E1215LL;
static int32_t g_156 = 0x21D6552DL;
static int16_t g_177 = (-2L);
static uint8_t g_196 = 255UL;
static uint32_t g_198 = 0xA9F6D975L;
static uint16_t g_200 = 4UL;
static int8_t g_202 = (-9L);
static uint32_t g_216 = 0x804B5BECL;
static float *g_224 = &g_86;
static float **g_223 = &g_224;
static int32_t *g_228 = (void*)0;
static int32_t **g_227 = &g_228;
static const uint32_t g_270 = 4294967295UL;
static float g_353 = 0xB.A78A47p+70;
static volatile uint32_t g_419[6] = {1UL,4294967293UL,1UL,1UL,4294967293UL,1UL};
static volatile uint32_t *g_418 = &g_419[3];
static volatile uint32_t **g_417 = &g_418;
static volatile uint16_t * const g_467 = (void*)0;
static volatile uint16_t * const *g_466[3] = {&g_467,&g_467,&g_467};
static const uint64_t g_493 = 18446744073709551611UL;
static const uint64_t *g_532 = &g_121;
static const uint64_t * volatile *g_531 = &g_532;
static uint64_t *g_534 = &g_121;
static uint64_t **g_533[8][2][7] = {{{&g_534,&g_534,&g_534,&g_534,&g_534,&g_534,&g_534},{(void*)0,&g_534,(void*)0,&g_534,&g_534,&g_534,&g_534}},{{&g_534,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_534,&g_534,(void*)0,(void*)0,&g_534,&g_534}},{{&g_534,(void*)0,&g_534,&g_534,&g_534,(void*)0,(void*)0},{(void*)0,&g_534,(void*)0,(void*)0,(void*)0,(void*)0,&g_534}},{{(void*)0,(void*)0,&g_534,(void*)0,(void*)0,&g_534,(void*)0},{&g_534,(void*)0,&g_534,&g_534,(void*)0,(void*)0,&g_534}},{{&g_534,&g_534,&g_534,&g_534,(void*)0,(void*)0,(void*)0},{(void*)0,&g_534,(void*)0,&g_534,&g_534,&g_534,&g_534}},{{&g_534,&g_534,&g_534,&g_534,(void*)0,(void*)0,&g_534},{(void*)0,(void*)0,(void*)0,&g_534,(void*)0,(void*)0,&g_534}},{{&g_534,&g_534,&g_534,&g_534,&g_534,&g_534,&g_534},{&g_534,&g_534,&g_534,&g_534,&g_534,(void*)0,&g_534}},{{(void*)0,&g_534,&g_534,&g_534,&g_534,(void*)0,&g_534},{&g_534,(void*)0,&g_534,&g_534,(void*)0,&g_534,(void*)0}}};
static int32_t g_561 = 0x946C6E2EL;
static const union U0 g_565 = {0x54L};/* VOLATILE GLOBAL g_565 */
static int16_t g_638 = 0x542AL;
static uint32_t * const *g_671 = (void*)0;
static const int32_t **g_737[2][3][6] = {{{&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]}},{{&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]}}};
static int8_t g_770[7] = {4L,4L,4L,4L,4L,4L,4L};
static union U0 g_827 = {0UL};/* VOLATILE GLOBAL g_827 */
static union U0 *g_826 = &g_827;
static union U0 **g_825 = &g_826;
static uint32_t g_938 = 0UL;
static int32_t * const g_1166 = &g_41[4][3][2];
static union U0 g_1169 = {0x13L};/* VOLATILE GLOBAL g_1169 */
static union U0 g_1171 = {0x24L};/* VOLATILE GLOBAL g_1171 */
static float g_1184 = 0xD.E94783p+83;
static int16_t g_1212 = 0x21BFL;
static int32_t * volatile g_1237[2] = {&g_133,&g_133};
static int32_t * volatile *g_1236 = &g_1237[0];
static float * const *g_1243[9] = {(void*)0,(void*)0,&g_224,(void*)0,(void*)0,&g_224,(void*)0,(void*)0,&g_224};
static float * const **g_1242 = &g_1243[2];
static const float *g_1247 = &g_86;
static const float **g_1246 = &g_1247;
static const float ***g_1245[10] = {&g_1246,&g_1246,&g_1246,&g_1246,&g_1246,&g_1246,&g_1246,&g_1246,&g_1246,&g_1246};
static volatile float ** volatile * const *g_1387 = (void*)0;
static volatile float ** volatile * const **g_1386 = &g_1387;
static union U0 g_1391 = {1UL};/* VOLATILE GLOBAL g_1391 */
static float ***g_1447 = &g_223;
static float ****g_1446 = &g_1447;
static float *****g_1445 = &g_1446;
static uint16_t *g_1476 = (void*)0;
static int64_t g_1560 = 0xA9652CDA2716F31ALL;
static int8_t g_1584 = 6L;
static float *** const *g_1593 = &g_1447;
static float *** const **g_1592[4][6][10] = {{{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593},{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,(void*)0},{&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,(void*)0,(void*)0,&g_1593,&g_1593,&g_1593},{&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593},{&g_1593,&g_1593,(void*)0,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593},{&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593}},{{&g_1593,(void*)0,&g_1593,&g_1593,(void*)0,(void*)0,&g_1593,(void*)0,&g_1593,&g_1593},{&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593},{&g_1593,(void*)0,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,(void*)0},{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,(void*)0},{&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593},{&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0}},{{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593},{&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593},{(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593},{&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0},{&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0},{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,(void*)0}},{{&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593},{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593},{(void*)0,(void*)0,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593},{&g_1593,(void*)0,(void*)0,&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,(void*)0,(void*)0},{&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0,(void*)0,&g_1593,&g_1593,(void*)0},{&g_1593,&g_1593,&g_1593,(void*)0,&g_1593,&g_1593,&g_1593,&g_1593,&g_1593,(void*)0}}};
static union U0 g_1657 = {0x3DL};/* VOLATILE GLOBAL g_1657 */
static union U0 g_1659 = {0x9DL};/* VOLATILE GLOBAL g_1659 */
static union U0 g_1660 = {254UL};/* VOLATILE GLOBAL g_1660 */
static union U0 g_1661 = {0xA6L};/* VOLATILE GLOBAL g_1661 */
static union U0 g_1662 = {254UL};/* VOLATILE GLOBAL g_1662 */
static union U0 g_1663 = {2UL};/* VOLATILE GLOBAL g_1663 */
static int64_t *g_1726[7][3][2] = {{{&g_135,&g_135},{&g_1560,&g_1560},{&g_135,(void*)0}},{{&g_135,&g_1560},{(void*)0,&g_135},{&g_135,(void*)0}},{{&g_1560,&g_135},{&g_1560,(void*)0},{&g_135,&g_135}},{{(void*)0,&g_1560},{&g_135,(void*)0},{&g_135,&g_1560}},{{&g_1560,&g_135},{&g_135,&g_135},{&g_135,&g_135}},{{&g_135,&g_135},{&g_1560,&g_1560},{&g_135,(void*)0}},{{&g_135,&g_1560},{(void*)0,&g_135},{&g_135,(void*)0}}};
static int64_t **g_1725 = &g_1726[4][2][1];
static int32_t g_1753[3] = {(-1L),(-1L),(-1L)};
static uint32_t * const ****g_1763 = (void*)0;
static uint32_t g_1769[10] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static int16_t g_1811 = 0x9B7CL;
static uint8_t g_1854 = 0xC4L;
static uint32_t g_1875 = 4294967295UL;
static uint32_t g_1876 = 0x90ECD2E0L;
static uint32_t g_1877 = 0x6E4D7A06L;
static int32_t *g_1924 = &g_133;
static int16_t g_1945[3] = {0x9C94L,0x9C94L,0x9C94L};
static uint32_t g_2004[10] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static const uint8_t g_2047 = 0x7AL;
static int8_t g_2089 = 0x3AL;
static uint64_t g_2095 = 0xDF51A1F9FF96B0D3LL;
static volatile union U0 * volatile **g_2099[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile union U0 * volatile ** volatile *g_2098 = &g_2099[5];
static union U0 g_2173 = {0x24L};/* VOLATILE GLOBAL g_2173 */
static int16_t **g_2345 = (void*)0;
static int16_t ***g_2344[3] = {&g_2345,&g_2345,&g_2345};
static uint16_t g_2362 = 9UL;
static int8_t *g_2384 = &g_770[2];
static int8_t **g_2383 = &g_2384;
static const float ****g_2411 = &g_1245[7];
static int32_t * const g_2413[10][1][3] = {{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}},{{&g_156,&g_156,&g_93}}};
static uint32_t **g_2415 = (void*)0;
static uint32_t g_2435 = 4294967287UL;
static float g_2447 = 0x3.926446p-80;
static int64_t g_2503 = 3L;
static float g_2513 = (-0x5.Dp+1);
static int32_t g_2665 = 0xC7A72363L;
static float g_2779 = 0x2.D65BAFp+50;
static volatile uint64_t * volatile ** const *g_2806 = (void*)0;
static const uint64_t g_2810 = 0xF376CDB4EF9EB8CFLL;
static uint16_t g_2813[10][5][5] = {{{0xB5E2L,0xA906L,2UL,0x1080L,1UL},{0x35B0L,0x9453L,0xBD79L,0x7671L,65528UL},{0x35B0L,65535UL,0x7278L,0UL,65534UL},{0x607DL,0x35B0L,9UL,0x8631L,0x6C8AL},{8UL,0x6C8AL,65534UL,65534UL,0x6C8AL}},{{0x0630L,2UL,0x40D1L,0x3EB1L,65534UL},{0x9453L,0x3EB1L,1UL,0x0630L,65528UL},{0x40D1L,0x607DL,2UL,65534UL,0x35B0L},{0x9453L,65535UL,65535UL,0x9453L,0x7671L},{0x0630L,0x8631L,1UL,0x607DL,0x607DL}},{{8UL,0x8631L,8UL,0x7671L,0x9453L},{0x607DL,65535UL,0x7671L,0x35B0L,65534UL},{0x35B0L,0x607DL,9UL,65528UL,0x0630L},{8UL,0x3EB1L,0x7671L,65534UL,0x3EB1L},{0x3EB1L,2UL,8UL,0x6C8AL,65534UL}},{{0x8631L,0x6C8AL,1UL,0x6C8AL,0x8631L},{0x40D1L,0x35B0L,65535UL,65534UL,0UL},{65528UL,65535UL,2UL,65528UL,0x7671L},{0x6C8AL,0x9453L,1UL,0x35B0L,0UL},{8UL,65528UL,0x40D1L,0x7671L,0x8631L}},{{0UL,65535UL,65534UL,0x607DL,65534UL},{0UL,0UL,9UL,0x9453L,0x3EB1L},{8UL,0x0630L,0x7278L,65534UL,0x0630L},{0x6C8AL,2UL,0xBD79L,0x0630L,65534UL},{65528UL,0x0630L,1UL,0x3EB1L,0x9453L}},{{0x40D1L,0UL,65535UL,65534UL,0x607DL},{0x8631L,65535UL,65535UL,0x8631L,0x7671L},{0x3EB1L,65528UL,1UL,0UL,0x35B0L},{8UL,0x9453L,0xBD79L,0x7671L,65528UL},{0x35B0L,65535UL,0x7278L,0UL,65534UL}},{{0x607DL,0x35B0L,9UL,0x8631L,0x6C8AL},{8UL,0x6C8AL,65534UL,65534UL,0x6C8AL},{0x0630L,2UL,0x40D1L,0x3EB1L,65534UL},{0x9453L,0x3EB1L,1UL,0x0630L,65528UL},{0x40D1L,0x607DL,2UL,65534UL,0x35B0L}},{{0x9453L,65535UL,65535UL,0x9453L,0x7671L},{0x0630L,0x8631L,1UL,0x607DL,0x607DL},{8UL,0x8631L,8UL,0x7671L,0x9453L},{0x607DL,65535UL,0x7671L,0x35B0L,65534UL},{0x35B0L,0x40D1L,0x607DL,2UL,65534UL}},{{65528UL,0x7671L,65535UL,0x8EC2L,0x7671L},{0x7671L,1UL,65528UL,0x7278L,0x8EC2L},{65535UL,0x7278L,0x35B0L,0x7278L,65535UL},{0xB5E2L,8UL,9UL,0x8EC2L,0xBD79L},{2UL,0x1080L,1UL,2UL,65535UL}},{{0x7278L,65535UL,0x35B0L,8UL,0xBD79L},{65528UL,2UL,0xB5E2L,65535UL,65535UL},{0xBD79L,0x1080L,0x8EC2L,0x40D1L,0x8EC2L},{0xBD79L,0xBD79L,0x607DL,65535UL,0x7671L},{65528UL,65534UL,0xA906L,0x8EC2L,65534UL}}};
static int32_t g_2862 = (-3L);
static int64_t g_2905 = 8L;
static int64_t ***g_2987 = &g_1725;
static uint32_t g_3030 = 1UL;
static uint32_t ***g_3040 = &g_2415;
static float g_3060 = 0x6.2p+1;
static uint8_t *g_3067 = &g_196;
static uint32_t **g_3102 = (void*)0;
static uint64_t g_3130 = 0xFE2208E5871159F2LL;
static uint32_t ****g_3140 = (void*)0;
static uint32_t *****g_3139 = &g_3140;
static int32_t **g_3181 = &g_1924;
static int64_t ****g_3236 = &g_2987;
static int64_t *****g_3235 = &g_3236;
static volatile uint16_t g_3255 = 0x5A80L;/* VOLATILE GLOBAL g_3255 */
static uint16_t g_3292 = 1UL;
static const uint32_t g_3312 = 18446744073709551615UL;
static volatile int32_t ** volatile g_3370 = (void*)0;/* VOLATILE GLOBAL g_3370 */
static volatile int32_t ** volatile *g_3369 = &g_3370;
static volatile int32_t ** volatile **g_3368 = &g_3369;
static uint32_t g_3389[4] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
static uint32_t ** const g_3405 = (void*)0;
static uint32_t ** const *g_3404 = &g_3405;
static uint32_t * const **g_3448 = &g_671;
static uint32_t * const ***g_3447[7] = {&g_3448,&g_3448,&g_3448,&g_3448,&g_3448,&g_3448,&g_3448};
static uint32_t *g_3481 = &g_1769[4];
static union U0 ***g_3525 = &g_825;
static union U0 ****g_3524 = &g_3525;
static union U0 *****g_3523 = &g_3524;
static uint32_t g_3526 = 4UL;
static int32_t * volatile * volatile *g_3572 = (void*)0;
static int32_t * volatile * volatile **g_3571 = &g_3572;
static int32_t * volatile * volatile ** volatile *g_3570 = &g_3571;
static uint32_t ** const **g_3649 = &g_3404;
static int32_t ** volatile g_3777[10][10] = {{&g_42,&g_228,&g_42,&g_42,&g_228,&g_42,&g_228,&g_228,&g_42,&g_228},{&g_42,&g_228,&g_228,(void*)0,&g_228,&g_228,&g_42,(void*)0,&g_42,&g_228},{&g_228,&g_228,&g_42,(void*)0,&g_42,&g_42,&g_42,&g_42,&g_228,(void*)0},{&g_42,&g_228,&g_228,&g_228,&g_228,(void*)0,&g_42,&g_228,&g_42,&g_42},{&g_228,&g_228,&g_228,&g_228,&g_42,&g_42,&g_228,&g_228,&g_228,&g_228},{&g_228,&g_228,&g_42,(void*)0,&g_228,&g_228,(void*)0,&g_228,&g_42,&g_228},{&g_42,&g_42,&g_42,&g_228,&g_228,&g_228,(void*)0,&g_228,(void*)0,&g_228},{&g_228,(void*)0,&g_228,&g_42,&g_42,&g_228,&g_42,&g_228,&g_42,&g_42},{(void*)0,(void*)0,(void*)0,&g_228,&g_228,&g_228,&g_42,&g_42,&g_228,(void*)0},{&g_42,&g_228,(void*)0,&g_42,&g_42,&g_228,(void*)0,&g_42,&g_228,&g_228}};
static uint64_t g_3824[5][5][6] = {{{0xC176B0F73838A1F9LL,0x5967CFA83F2798B3LL,0xECAD2D32ADD0CFAALL,0xD464BE0BAF8F0B02LL,0xCDBDA3AD61BB9668LL,0x5967CFA83F2798B3LL},{0x3CB07AC6E970252DLL,0x62DC539D4DB68638LL,0x82CAA735210F6328LL,0xD464BE0BAF8F0B02LL,0x82CAA735210F6328LL,0x62DC539D4DB68638LL},{0xC176B0F73838A1F9LL,0x62DC539D4DB68638LL,0xECAD2D32ADD0CFAALL,0x1D59F84B0ED30478LL,0xCDBDA3AD61BB9668LL,0x62DC539D4DB68638LL},{0x3CB07AC6E970252DLL,0x5967CFA83F2798B3LL,0x82CAA735210F6328LL,0x1D59F84B0ED30478LL,0x82CAA735210F6328LL,0x5967CFA83F2798B3LL},{0xC176B0F73838A1F9LL,0x5967CFA83F2798B3LL,1UL,0x5967CFA83F2798B3LL,0UL,0xB79D55D7F03E2F31LL}},{{0x82CAA735210F6328LL,1UL,0UL,0x5967CFA83F2798B3LL,0UL,1UL},{0xCDBDA3AD61BB9668LL,1UL,1UL,0x62DC539D4DB68638LL,0UL,1UL},{0x82CAA735210F6328LL,0xB79D55D7F03E2F31LL,0UL,0x62DC539D4DB68638LL,0UL,0xB79D55D7F03E2F31LL},{0xCDBDA3AD61BB9668LL,0xB79D55D7F03E2F31LL,1UL,0x5967CFA83F2798B3LL,0UL,0xB79D55D7F03E2F31LL},{0x82CAA735210F6328LL,1UL,0UL,0x5967CFA83F2798B3LL,0UL,1UL}},{{0xCDBDA3AD61BB9668LL,1UL,1UL,0x62DC539D4DB68638LL,0UL,1UL},{0x82CAA735210F6328LL,0xB79D55D7F03E2F31LL,0UL,0x62DC539D4DB68638LL,0UL,0xB79D55D7F03E2F31LL},{0xCDBDA3AD61BB9668LL,0xB79D55D7F03E2F31LL,1UL,0x5967CFA83F2798B3LL,0UL,0xB79D55D7F03E2F31LL},{0x82CAA735210F6328LL,1UL,0UL,0x5967CFA83F2798B3LL,0UL,1UL},{0xCDBDA3AD61BB9668LL,1UL,1UL,0x62DC539D4DB68638LL,0UL,1UL}},{{0x82CAA735210F6328LL,0xB79D55D7F03E2F31LL,0UL,0x62DC539D4DB68638LL,0UL,0xB79D55D7F03E2F31LL},{0xCDBDA3AD61BB9668LL,0xB79D55D7F03E2F31LL,1UL,0x5967CFA83F2798B3LL,0UL,0xB79D55D7F03E2F31LL},{0x82CAA735210F6328LL,1UL,0UL,0x5967CFA83F2798B3LL,0UL,1UL},{0xCDBDA3AD61BB9668LL,1UL,1UL,0x62DC539D4DB68638LL,0UL,1UL},{0x82CAA735210F6328LL,0xB79D55D7F03E2F31LL,0UL,0x62DC539D4DB68638LL,0UL,0xB79D55D7F03E2F31LL}},{{0xCDBDA3AD61BB9668LL,0xB79D55D7F03E2F31LL,1UL,0x5967CFA83F2798B3LL,0UL,0xB79D55D7F03E2F31LL},{0x82CAA735210F6328LL,1UL,0UL,0x5967CFA83F2798B3LL,0UL,1UL},{0xCDBDA3AD61BB9668LL,1UL,1UL,0x62DC539D4DB68638LL,0UL,1UL},{0x82CAA735210F6328LL,0xB79D55D7F03E2F31LL,0UL,0x62DC539D4DB68638LL,0UL,0xB79D55D7F03E2F31LL},{0xCDBDA3AD61BB9668LL,0xB79D55D7F03E2F31LL,1UL,0x5967CFA83F2798B3LL,0UL,0xB79D55D7F03E2F31LL}}};
static uint8_t g_3858 = 1UL;
static const int16_t ****g_3913 = (void*)0;
static const int16_t *****g_3912 = &g_3913;
static union U0 g_3949 = {0xCEL};/* VOLATILE GLOBAL g_3949 */
static volatile int32_t g_4017 = 2L;/* VOLATILE GLOBAL g_4017 */


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static int32_t ** func_5(uint32_t  p_6, float  p_7, uint32_t  p_8, int32_t * p_9);
static uint32_t  func_11(int32_t  p_12, int64_t  p_13);
static int32_t  func_14(const float  p_15, float  p_16, const int32_t  p_17);
static float  func_26(int16_t  p_27, int32_t  p_28);
static uint8_t  func_32(int32_t * p_33, int32_t ** p_34, int32_t  p_35, int32_t * p_36);
static int32_t * func_37(int32_t * const  p_38, const int32_t ** p_39);
static float  func_50(uint32_t  p_51, int32_t  p_52);
static uint8_t  func_55(const uint16_t  p_56, const uint8_t  p_57, int32_t ** p_58, const int32_t  p_59);
static int64_t  func_62(const uint64_t  p_63, int32_t ** p_64);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_3 g_29 g_40 g_41 g_92 g_93 g_98 g_97 g_42 g_133 g_126 g_121 g_156 g_99 g_123 g_100 g_216 g_223 g_227 g_198 g_228 g_202 g_196 g_224 g_86 g_270 g_177 g_200 g_417 g_466 g_135 g_561 g_531 g_532 g_534 g_493 g_671 g_737 g_825 g_638 g_770 g_938 g_1166 g_1171.f2 g_1212 g_418 g_1386 g_1387 g_1445 g_1446 g_1560 g_1584 g_1447 g_1592 g_826 g_1246 g_1247 g_2047 g_1243 g_2095 g_2098 g_1876 g_2004 g_2089 g_1811 g_1753 g_1945 g_1769 g_1877 g_1854 g_2344 g_2362 g_1662.f1 g_1661.f3 g_1924 g_2173 g_827 g_1391 g_2862 g_2384 g_3481 g_3067 g_3030
 * writes: g_4 g_42 g_86 g_92 g_93 g_97 g_100 g_41 g_121 g_135 g_156 g_123 g_177 g_196 g_126 g_198 g_200 g_202 g_216 g_228 g_133 g_561 g_534 g_353 g_99 g_227 g_223 g_770 g_533 g_737 g_638 g_827.f3 g_1171.f2 g_1171.f1 g_1212 g_826 g_827.f2 g_1445 g_1476 g_938 g_1391.f1 g_1560 g_1662.f3 g_671 g_2095 g_2098 g_1171.f3 g_1659.f2 g_1854 g_1945 g_1387 g_1662.f1 g_1661.f3 g_2383 g_98 g_2089 g_1769 g_3030
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int8_t l_10[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
    const int32_t *l_44 = &g_41[3][0][3];
    const int32_t **l_43 = &l_44;
    int32_t *l_47 = (void*)0;
    int32_t **l_46[10] = {&l_47,&l_47,(void*)0,&l_47,&l_47,(void*)0,&l_47,&l_47,(void*)0,&l_47};
    uint32_t l_2957 = 0UL;
    const float l_2958 = 0xE.272B39p-51;
    const int8_t l_2959 = 0x9AL;
    uint32_t l_3068 = 0x5169FBCEL;
    int32_t ***l_4036 = (void*)0;
    int32_t ***l_4037 = &g_227;
    int i;
    (*g_3) = g_2;
    (*l_4037) = func_5(g_2, l_10[2], func_11(func_14(l_10[4], (safe_add_func_float_f_f((safe_div_func_float_f_f(((safe_sub_func_float_f_f(0x1.Ap-1, (safe_add_func_float_f_f(func_26(g_29, ((safe_sub_func_uint8_t_u_u(func_32(func_37((g_42 = g_40), l_43), l_46[6], g_41[4][3][2], &g_41[4][0][2]), 0x6AL)) >= g_938)), l_2957)))) , 0xD.B284C2p+29), l_2958)), 0x0.38AF70p-38)), l_2959), l_3068), &g_2862);
    return (**l_43);
}


/* ------------------------------------------ */
/* 
 * reads : g_826 g_2173 g_827 g_1391 g_532 g_121 g_2862 g_2384 g_770 g_3481 g_3067 g_196 g_1166 g_1560 g_531 g_40 g_41 g_3030 g_638 g_224 g_1171.f2
 * writes: g_1769 g_41 g_1560 g_770 g_1171.f2 g_133 g_3030 g_638 g_86
 */
static int32_t ** func_5(uint32_t  p_6, float  p_7, uint32_t  p_8, int32_t * p_9)
{ /* block id: 1610 */
    union U0 *l_3661 = (void*)0;
    const uint32_t *l_3672 = &g_3030;
    const uint32_t ** const l_3671 = &l_3672;
    int32_t l_3673[9][3] = {{0x800CB925L,6L,(-10L)},{0x1E867C29L,0x78FB7985L,0x78FB7985L},{0x800CB925L,6L,(-10L)},{0x1E867C29L,0x78FB7985L,0x78FB7985L},{0x800CB925L,6L,(-10L)},{0x1E867C29L,0x78FB7985L,0x78FB7985L},{0x800CB925L,6L,(-10L)},{0x1E867C29L,0x78FB7985L,0x78FB7985L},{0x800CB925L,6L,(-10L)}};
    float l_3685 = 0x8.2B2A8Cp+47;
    const int32_t *l_3688 = &g_1753[1];
    const int32_t **l_3687 = &l_3688;
    const int32_t ***l_3686 = &l_3687;
    int16_t *l_3700 = (void*)0;
    int32_t l_3732 = 9L;
    int32_t l_3733 = 0xCCE9C988L;
    int32_t *l_3736[7];
    uint64_t **l_3742 = &g_534;
    uint16_t *l_3749 = (void*)0;
    int64_t l_3775 = 0x72A34CBB4AC70049LL;
    uint32_t ***l_3776 = &g_3102;
    uint8_t l_3790 = 255UL;
    int64_t l_3843[3];
    int8_t **l_3861 = &g_2384;
    uint16_t l_3867 = 65535UL;
    const int16_t *****l_3914 = (void*)0;
    int32_t l_3925 = (-6L);
    int i, j;
    for (i = 0; i < 7; i++)
        l_3736[i] = &g_92[3][0];
    for (i = 0; i < 3; i++)
        l_3843[i] = 0xF7C2F5A260DF3928LL;
    if (((safe_mul_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(((((void*)0 == &g_2344[1]) , (safe_add_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(p_8, ((((((safe_add_func_uint16_t_u_u((l_3661 != (void*)0), ((p_6 , ((*g_3481) = (p_8 != (safe_div_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u((safe_div_func_int16_t_s_s((~((safe_sub_func_int64_t_s_s((p_8 , (((*g_826) , (void*)0) == l_3671)), (*g_532))) | 0x3EL)), p_8)), (*p_9))), (*g_2384)))))) & 0x979005EFL))) | l_3673[4][1]) | p_8) != p_8) >= p_6) , l_3673[4][1]))), l_3673[4][1]))) ^ p_8), 6)), l_3673[6][0])) <= 8UL))
    { /* block id: 1612 */
        int8_t l_3676 = 0x20L;
        int32_t l_3677 = 3L;
        int64_t *l_3684 = &g_1560;
        int16_t l_3692[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t *l_3706 = &g_93;
        int8_t *l_3730[5];
        uint8_t l_3731[1][3][10] = {{{1UL,1UL,252UL,0xD6L,0x47L,0xD6L,252UL,1UL,1UL,1UL},{255UL,251UL,0xABL,1UL,255UL,255UL,1UL,0xABL,0x5CL,1UL},{0x5CL,251UL,1UL,1UL,0xA8L,1UL,0xA8L,1UL,1UL,251UL}}};
        int32_t **l_3734 = (void*)0;
        int32_t **l_3735 = (void*)0;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_3730[i] = &g_202;
lbl_3711:
        (*g_1166) = (l_3677 |= (safe_mul_func_uint8_t_u_u((((*g_3067) , l_3673[3][0]) , (*g_3067)), (l_3676 = 0xE0L))));
        if (((safe_sub_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_u(((0x93L || (safe_mul_func_int16_t_s_s(((((*l_3684) &= p_8) < (**g_531)) , l_3677), p_6))) >= (l_3686 == &l_3687)), 3)) , (+0xED1E0CF7C2E66663LL)), (safe_rshift_func_uint8_t_u_s((*g_3067), 3)))) && p_8))
        { /* block id: 1617 */
            int64_t l_3701 = (-2L);
            l_3677 = (((l_3692[5] ^ ((safe_rshift_func_uint16_t_u_s((((((*g_1166) = 0x07A299E7L) <= ((*g_3481) = ((safe_lshift_func_int8_t_s_u((-1L), 1)) < (((*g_531) != l_3684) & (safe_lshift_func_int16_t_s_s((l_3677 != (!(((*g_2384) &= l_3673[4][1]) | (l_3700 != (void*)0)))), 10)))))) == p_6) <= 1L), 5)) & l_3701)) , 0x3.2DA43Dp-56) == 0xF.0342BBp-75);
        }
        else
        { /* block id: 1622 */
            for (g_1171.f2 = (-15); (g_1171.f2 <= 21); g_1171.f2 = safe_add_func_uint32_t_u_u(g_1171.f2, 9))
            { /* block id: 1625 */
                for (g_133 = 20; (g_133 >= 5); g_133--)
                { /* block id: 1628 */
                    volatile uint16_t * const **l_3708 = &g_466[0];
                    volatile uint16_t * const ***l_3707 = &l_3708;
                    (*g_40) &= (-8L);
                    l_3706 = (void*)0;
                    (*l_3707) = &g_466[2];
                }
            }
            for (g_3030 = (-21); (g_3030 <= 47); g_3030++)
            { /* block id: 1636 */
                int16_t l_3716 = 0xF9A0L;
                if (l_3677)
                    break;
                if (p_6)
                    goto lbl_3711;
                l_3716 |= (safe_rshift_func_uint8_t_u_s(0xDEL, (safe_rshift_func_int8_t_s_u((p_6 , 0xFEL), 7))));
            }
        }
        l_3733 ^= ((!l_3673[4][1]) , (+((((safe_mod_func_int16_t_s_s(((safe_sub_func_int8_t_s_s(((safe_mod_func_uint8_t_u_u((0xA3533CF5L && 0xB1FA85C4L), p_6)) > ((0xD42EL ^ p_6) <= (~(safe_lshift_func_int8_t_s_s((*g_2384), (safe_sub_func_int8_t_s_s((l_3673[5][2] |= p_6), (65535UL <= l_3731[0][2][6])))))))), l_3732)) > l_3732), p_8)) , l_3732) == 255UL) > p_8)));
        l_3736[2] = (void*)0;
    }
    else
    { /* block id: 1645 */
        uint32_t l_3741 = 0xC11EC27EL;
        uint64_t ***l_3743 = (void*)0;
        uint64_t **l_3744 = &g_534;
        int16_t **l_3750 = &l_3700;
        int16_t *l_3751 = &g_638;
        int32_t l_3752 = 0xC62AFA1DL;
        uint64_t *l_3756 = &g_3130;
        uint64_t *l_3759 = &g_121;
        int64_t **l_3763 = &g_1726[1][2][0];
        int64_t *** const l_3762 = &l_3763;
        int64_t *** const *l_3761 = &l_3762;
        int64_t *** const **l_3760 = &l_3761;
        uint32_t ***** const l_3770[6] = {&g_3140,&g_3140,&g_3140,&g_3140,&g_3140,&g_3140};
        const uint8_t l_3771 = 0x4DL;
        const uint64_t l_3794 = 0xE72CEB9072504390LL;
        uint16_t l_3795 = 0x8CA7L;
        uint32_t ***l_3798 = &g_2415;
        int32_t l_3863 = 3L;
        const int32_t **l_3866 = &g_99[1];
        int32_t ***l_3930 = (void*)0;
        uint8_t l_3994 = 0x94L;
        uint16_t **l_4011 = &l_3749;
        uint32_t l_4012 = 1UL;
        int32_t l_4015[6][4][7] = {{{0x506466B9L,0xF7B596F5L,(-1L),(-1L),0xF7B596F5L,0x506466B9L,(-8L)},{9L,0L,0xE1A50952L,(-3L),(-1L),0x3D315932L,(-3L)},{0xF1D640A9L,0x98B66C8AL,0x046C6167L,0xA9AB88E0L,(-8L),(-9L),0xC0A8395AL},{0xFF91A108L,0L,(-9L),(-1L),9L,(-1L),0xFF91A108L}},{{0L,0xF7B596F5L,0L,(-3L),(-1L),(-1L),(-1L)},{0L,0xD1B266F6L,0xD1B266F6L,0L,0x046C6167L,0L,0xA9AB88E0L},{0xFF91A108L,(-1L),0x506466B9L,0x046C6167L,0xF1D640A9L,0x506466B9L,0L},{0xF1D640A9L,1L,(-1L),0xF7B596F5L,(-1L),(-7L),0xA9AB88E0L}},{{9L,0xFF91A108L,0x046C6167L,1L,(-9L),(-5L),(-1L)},{0x506466B9L,(-1L),0xFF91A108L,(-5L),(-3L),(-5L),0xFF91A108L},{(-1L),(-1L),0L,0xF1D640A9L,(-1L),(-7L),0xC0A8395AL},{0xC0A8395AL,(-5L),(-1L),0L,(-5L),0x506466B9L,(-3L)}},{{(-9L),0L,0xFF91A108L,(-8L),(-1L),0L,(-8L)},{0xF1D640A9L,(-9L),(-1L),0xA9AB88E0L,(-3L),(-1L),0xC0A8395AL},{1L,(-1L),(-9L),0xA9AB88E0L,(-9L),(-1L),1L},{(-1L),0xF7B596F5L,0x506466B9L,(-8L),(-1L),(-9L),0xD1B266F6L}},{{0L,(-1L),(-2L),0L,0xF1D640A9L,0x3D315932L,0xA9AB88E0L},{1L,0L,0x506466B9L,0xF1D640A9L,0x046C6167L,0x506466B9L,(-1L)},{0x046C6167L,1L,(-9L),(-5L),(-1L),(-1L),(-1L)},{9L,1L,(-1L),1L,9L,0x76564831L,(-1L)}},{{0xC0A8395AL,0L,0xFF91A108L,0xF7B596F5L,(-8L),(-5L),1L},{0xD1B266F6L,(-1L),(-1L),0x046C6167L,(-1L),(-1L),0x506466B9L},{0xC0A8395AL,0xF7B596F5L,0L,0L,0xF7B596F5L,0L,(-3L)},{9L,(-1L),0xFF91A108L,(-3L),0L,(-1L),0L}}};
        int32_t **l_4027 = &l_3736[2];
        uint64_t l_4032 = 0x78092CFC96928D4BLL;
        int i, j, k;
        l_3752 = (p_7 = ((safe_div_func_float_f_f((p_6 , ((*g_224) = (((safe_sub_func_int16_t_s_s(((l_3744 = ((l_3741 || p_6) , l_3742)) == (((*l_3751) ^= (((safe_rshift_func_int16_t_s_u((p_6 , 0xDBB8L), 8)) , l_3741) > ((((safe_sub_func_uint32_t_u_u(((l_3700 == (l_3749 = l_3700)) >= 0xB8L), (*p_9))) , (*g_2384)) , (void*)0) != l_3750))) , &g_532)), (-1L))) && 6L) , p_7))), p_6)) != l_3741));
    }
    return &g_228;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint32_t  func_11(int32_t  p_12, int64_t  p_13)
{ /* block id: 1371 */
    uint16_t l_3069 = 65535UL;
    int32_t *l_3071 = &g_156;
    int32_t *l_3097 = &g_92[3][0];
    int32_t *l_3098[4][7][1] = {{{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97}},{{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97},{&g_41[4][3][2]}},{{&g_41[4][3][2]},{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]}},{{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97},{&g_41[4][3][2]},{&g_41[4][3][2]},{&g_97}}};
    float l_3112 = (-0x7.Bp+1);
    uint8_t l_3113 = 0x2AL;
    uint32_t l_3133 = 0UL;
    int32_t l_3155 = 7L;
    int16_t *l_3157[1];
    int16_t **l_3156 = &l_3157[0];
    uint8_t l_3209[1];
    const int32_t **l_3256 = &g_99[1];
    union U0 ***l_3284[1][1][3];
    union U0 ****l_3283 = &l_3284[0][0][0];
    union U0 **** const *l_3282[9][5][5] = {{{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{(void*)0,(void*)0,(void*)0,(void*)0,&l_3283},{&l_3283,&l_3283,(void*)0,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,(void*)0,&l_3283,&l_3283}},{{&l_3283,(void*)0,(void*)0,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,(void*)0,(void*)0},{(void*)0,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,(void*)0,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283}},{{&l_3283,(void*)0,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,(void*)0,&l_3283,(void*)0},{&l_3283,&l_3283,&l_3283,&l_3283,(void*)0},{(void*)0,&l_3283,(void*)0,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283}},{{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,(void*)0,(void*)0},{&l_3283,&l_3283,(void*)0,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{(void*)0,&l_3283,&l_3283,&l_3283,&l_3283}},{{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,(void*)0,&l_3283,&l_3283},{(void*)0,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,(void*)0,(void*)0,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,(void*)0}},{{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{(void*)0,&l_3283,(void*)0,(void*)0,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,(void*)0},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283}},{{&l_3283,&l_3283,&l_3283,&l_3283,(void*)0},{(void*)0,&l_3283,&l_3283,(void*)0,&l_3283},{(void*)0,(void*)0,&l_3283,&l_3283,&l_3283},{(void*)0,&l_3283,&l_3283,&l_3283,&l_3283},{(void*)0,&l_3283,(void*)0,&l_3283,&l_3283}},{{(void*)0,&l_3283,(void*)0,&l_3283,(void*)0},{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,(void*)0,(void*)0,&l_3283},{&l_3283,&l_3283,(void*)0,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,(void*)0}},{{&l_3283,&l_3283,&l_3283,&l_3283,&l_3283},{&l_3283,(void*)0,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,(void*)0,&l_3283},{&l_3283,(void*)0,&l_3283,&l_3283,&l_3283},{&l_3283,&l_3283,&l_3283,&l_3283,(void*)0}}};
    int64_t *l_3290 = &g_1560;
    const uint32_t *l_3311 = &g_3312;
    const uint32_t **l_3310 = &l_3311;
    int32_t l_3387 = 0xF0F82B94L;
    uint32_t ***l_3425 = &g_3102;
    uint32_t *** const *l_3424[2][3];
    uint32_t l_3519 = 0x704A2957L;
    uint32_t ** const **l_3520 = &g_3404;
    float **l_3561 = (void*)0;
    float **l_3566 = (void*)0;
    int32_t *l_3582 = &g_92[3][0];
    int8_t l_3617 = (-4L);
    float l_3644 = 0xA.480937p-4;
    int8_t l_3645[2];
    uint16_t **l_3647 = &g_1476;
    uint16_t ***l_3646 = &l_3647;
    uint32_t ** const ***l_3648[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_3650[5][3];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_3157[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_3209[i] = 255UL;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
                l_3284[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
            l_3424[i][j] = &l_3425;
    }
    for (i = 0; i < 2; i++)
        l_3645[i] = 1L;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
            l_3650[i][j] = 0x1FB45119L;
    }
    return l_3650[0][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_2095 g_1584 g_2047 g_40 g_41 g_2089 g_42 g_1924 g_133
 * writes: g_41 g_42 g_98 g_2089
 */
static int32_t  func_14(const float  p_15, float  p_16, const int32_t  p_17)
{ /* block id: 1314 */
    uint16_t *l_2971 = (void*)0;
    uint32_t ****l_2972[1][2];
    int32_t l_2973 = (-1L);
    int32_t l_2974 = 0x8ABC0850L;
    int8_t l_2975[5] = {0xADL,0xADL,0xADL,0xADL,0xADL};
    int32_t **l_2976 = &g_42;
    const int32_t **l_2977[10] = {&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1],&g_99[1]};
    const int32_t ***l_2978[9];
    int16_t l_2997[4];
    uint32_t ***l_3007 = &g_2415;
    uint64_t *l_3063 = &g_2095;
    uint8_t *l_3065[4][3][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_2972[i][j] = (void*)0;
    }
    for (i = 0; i < 9; i++)
        l_2978[i] = &g_737[1][0][3];
    for (i = 0; i < 4; i++)
        l_2997[i] = 0xC3C1L;
    (*g_40) ^= (((+0x54CC64B481C7B635LL) & 5L) == (p_17 == ((safe_add_func_uint16_t_u_u((((l_2975[4] = (0x6DB84C448546BEFCLL || (safe_mod_func_uint8_t_u_u(((0x4D585E2225B1B686LL != ((safe_mul_func_uint8_t_u_u((((safe_add_func_int8_t_s_s(5L, p_17)) <= (safe_div_func_int64_t_s_s(((l_2973 = (l_2971 != (((((((p_17 != 65530UL) || 0x82DE9A32L) <= p_17) , l_2972[0][1]) == l_2972[0][1]) <= 0xC7613B0FL) , l_2971))) != g_2095), l_2974))) != l_2974), l_2974)) && g_1584)) < 0xB745L), p_17)))) ^ 0x7CL) <= 0UL), 1UL)) == g_2047)));
    (*l_2976) = func_37(func_37(((*l_2976) = &l_2973), &g_99[1]), (g_98[0][5][1] = l_2977[7]));
    for (g_2089 = 0; (g_2089 > (-15)); --g_2089)
    { /* block id: 1323 */
        int16_t l_2983 = (-3L);
        int64_t ** const *l_2984 = &g_1725;
        int64_t ***l_2985[6] = {&g_1725,&g_1725,&g_1725,&g_1725,&g_1725,&g_1725};
        int64_t ****l_2986[10][7][2] = {{{&l_2985[5],(void*)0},{(void*)0,&l_2985[5]},{&l_2985[5],&l_2985[5]},{&l_2985[0],&l_2985[5]},{(void*)0,(void*)0},{(void*)0,&l_2985[5]},{&l_2985[0],&l_2985[5]}},{{&l_2985[5],&l_2985[5]},{(void*)0,(void*)0},{&l_2985[5],(void*)0},{(void*)0,&l_2985[5]},{&l_2985[1],&l_2985[4]},{&l_2985[5],(void*)0},{&l_2985[5],&l_2985[5]}},{{(void*)0,&l_2985[5]},{&l_2985[5],&l_2985[1]},{&l_2985[5],&l_2985[1]},{&l_2985[5],&l_2985[5]},{(void*)0,&l_2985[5]},{&l_2985[5],(void*)0},{&l_2985[5],&l_2985[4]}},{{&l_2985[1],&l_2985[5]},{(void*)0,(void*)0},{&l_2985[5],(void*)0},{(void*)0,&l_2985[5]},{&l_2985[5],&l_2985[5]},{&l_2985[0],&l_2985[5]},{(void*)0,(void*)0}},{{(void*)0,&l_2985[5]},{&l_2985[0],&l_2985[5]},{&l_2985[5],&l_2985[1]},{&l_2985[5],&l_2985[5]},{&l_2985[5],(void*)0},{(void*)0,&l_2985[1]},{&l_2985[1],(void*)0}},{{(void*)0,&l_2985[5]},{(void*)0,&l_2985[5]},{&l_2985[5],&l_2985[5]},{&l_2985[1],&l_2985[4]},{&l_2985[5],&l_2985[4]},{&l_2985[1],&l_2985[5]},{&l_2985[5],&l_2985[5]}},{{(void*)0,&l_2985[5]},{(void*)0,(void*)0},{&l_2985[1],&l_2985[1]},{(void*)0,(void*)0},{&l_2985[5],&l_2985[5]},{&l_2985[5],&l_2985[1]},{&l_2985[5],&l_2985[5]}},{{&l_2985[3],&l_2985[5]},{&l_2985[5],&l_2985[0]},{&l_2985[5],&l_2985[5]},{&l_2985[3],&l_2985[5]},{&l_2985[5],&l_2985[1]},{&l_2985[5],&l_2985[5]},{&l_2985[5],(void*)0}},{{(void*)0,&l_2985[1]},{&l_2985[1],(void*)0},{(void*)0,&l_2985[5]},{(void*)0,&l_2985[5]},{&l_2985[5],&l_2985[5]},{&l_2985[1],&l_2985[4]},{&l_2985[5],&l_2985[4]}},{{&l_2985[1],&l_2985[5]},{&l_2985[5],&l_2985[5]},{(void*)0,&l_2985[5]},{(void*)0,(void*)0},{&l_2985[1],&l_2985[1]},{(void*)0,(void*)0},{&l_2985[5],&l_2985[5]}}};
        uint8_t *l_2988[9][5][5] = {{{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196}},{{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196}},{{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196}},{{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196}},{{&g_1854,&g_1854,&g_1854,&g_196,&g_196},{(void*)0,&g_1854,(void*)0,&g_196,&g_196},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854}},{{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0}},{{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854}},{{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0}},{{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854},{&g_1854,&g_1854,&g_1854,(void*)0,(void*)0},{&g_1854,(void*)0,&g_1854,&g_196,&g_1854}}};
        uint64_t ***l_3011 = &g_533[4][0][5];
        uint64_t ****l_3010 = &l_3011;
        uint32_t ***l_3037[6] = {&g_2415,&g_2415,&g_2415,&g_2415,&g_2415,&g_2415};
        int32_t l_3055 = 0x2353E754L;
        uint8_t **l_3066[1][7][6] = {{{&l_2988[4][4][1],&l_3065[3][0][0],(void*)0,(void*)0,&l_3065[0][1][1],&l_2988[4][4][1]},{(void*)0,&l_2988[4][4][1],(void*)0,(void*)0,&l_2988[4][4][1],(void*)0},{&l_2988[4][4][1],&l_3065[0][1][1],(void*)0,(void*)0,&l_3065[3][0][0],&l_2988[4][4][1]},{&l_2988[3][4][3],&l_3065[0][1][1],(void*)0,&l_2988[3][4][3],&l_2988[4][4][1],&l_2988[3][4][3]},{&l_2988[3][4][3],&l_2988[4][4][1],&l_2988[3][4][3],(void*)0,&l_3065[0][1][1],&l_2988[3][4][3]},{&l_2988[4][4][1],&l_3065[3][0][0],(void*)0,(void*)0,&l_3065[0][1][1],&l_2988[4][4][1]},{(void*)0,&l_2988[4][4][1],(void*)0,(void*)0,&l_2988[4][4][1],(void*)0}}};
        int i, j, k;
    }
    (*l_2976) = func_37((*l_2976), &g_99[1]);
    return (*g_1924);
}


/* ------------------------------------------ */
/* 
 * reads : g_133 g_1166 g_41 g_40 g_1386 g_825 g_1387 g_92 g_126 g_97 g_123 g_93 g_198 g_202 g_638 g_938 g_561 g_224 g_86 g_532 g_121 g_177 g_200 g_196 g_1212 g_1445 g_1446 g_1560 g_493 g_135 g_1584 g_228 g_1447 g_223 g_1592 g_531 g_770 g_216 g_826 g_156 g_1246 g_1247 g_671 g_2047 g_1243 g_2095 g_2098 g_1876 g_2004 g_2089 g_1811 g_1753 g_1945 g_1769 g_1877 g_1854 g_100 g_2344 g_2362 g_1662.f1 g_1661.f3
 * writes: g_1171.f1 g_1212 g_41 g_228 g_196 g_826 g_99 g_200 g_198 g_827.f2 g_177 g_1445 g_121 g_1476 g_126 g_938 g_123 g_1391.f1 g_1560 g_770 g_561 g_92 g_1662.f3 g_86 g_135 g_671 g_2095 g_2098 g_1171.f3 g_1659.f2 g_638 g_156 g_1854 g_1945 g_100 g_1387 g_1662.f1 g_1661.f3 g_2383
 */
static float  func_26(int16_t  p_27, int32_t  p_28)
{ /* block id: 662 */
    const int32_t **l_1327 = &g_99[1];
    int32_t **l_1328 = (void*)0;
    int32_t l_1333[4] = {0x7361579DL,0x7361579DL,0x7361579DL,0x7361579DL};
    uint16_t l_1339 = 8UL;
    float *l_1368[9][2][4] = {{{&g_1184,&g_86,&g_1184,&g_86},{&g_353,&g_1184,&g_1184,&g_1184}},{{&g_353,&g_353,&g_86,&g_353},{&g_86,&g_353,&g_1184,&g_353}},{{&g_86,&g_1184,&g_86,(void*)0},{&g_353,&g_353,&g_1184,&g_86}},{{&g_353,&g_1184,&g_86,&g_1184},{&g_1184,(void*)0,(void*)0,&g_1184}},{{&g_1184,&g_353,&g_1184,(void*)0},{&g_1184,&g_86,&g_86,&g_353}},{{&g_86,&g_1184,&g_353,&g_353},{&g_1184,&g_86,&g_353,(void*)0}},{{&g_1184,&g_353,&g_353,&g_1184},{&g_353,(void*)0,(void*)0,&g_1184}},{{&g_1184,&g_1184,&g_1184,&g_353},{&g_86,&g_1184,&g_86,&g_353}},{{&g_1184,(void*)0,&g_1184,&g_1184},{&g_1184,&g_1184,&g_1184,(void*)0}}};
    float ****l_1383 = (void*)0;
    int64_t l_1520[1];
    int16_t l_1541[2];
    uint32_t l_1543 = 4294967295UL;
    uint16_t *l_1551 = &g_1171.f3;
    uint64_t *l_1557 = &g_121;
    uint32_t *l_1582 = &g_216;
    uint32_t **l_1581 = &l_1582;
    uint32_t ***l_1580[5][2][4] = {{{&l_1581,&l_1581,&l_1581,&l_1581},{&l_1581,&l_1581,&l_1581,&l_1581}},{{&l_1581,&l_1581,&l_1581,&l_1581},{&l_1581,&l_1581,&l_1581,&l_1581}},{{&l_1581,&l_1581,&l_1581,&l_1581},{&l_1581,&l_1581,&l_1581,&l_1581}},{{&l_1581,&l_1581,&l_1581,&l_1581},{&l_1581,&l_1581,&l_1581,&l_1581}},{{&l_1581,&l_1581,&l_1581,&l_1581},{&l_1581,&l_1581,&l_1581,&l_1581}}};
    float * const ***l_1596 = &g_1242;
    float * const ****l_1595 = &l_1596;
    int16_t l_1597 = 0x08D0L;
    int8_t *l_1650[10][10] = {{(void*)0,&g_202,&g_770[2],&g_770[2],&g_770[2],&g_770[2],&g_770[2],&g_770[2],&g_770[2],&g_202},{&g_1584,&g_202,&g_770[2],(void*)0,&g_1584,&g_1584,&g_770[2],&g_770[2],&g_202,&g_202},{&g_202,&g_202,&g_1584,&g_770[2],&g_770[2],&g_1584,&g_202,&g_202,(void*)0,&g_770[2]},{&g_1584,&g_770[2],&g_770[2],&g_202,(void*)0,&g_770[2],(void*)0,&g_770[2],(void*)0,&g_770[2]},{(void*)0,(void*)0,&g_770[2],(void*)0,(void*)0,&g_770[2],(void*)0,&g_202,&g_202,&g_1584},{(void*)0,&g_1584,&g_1584,&g_770[2],&g_770[2],&g_202,&g_202,&g_770[2],&g_770[2],&g_1584},{&g_770[2],&g_770[2],&g_770[2],&g_1584,(void*)0,&g_202,&g_770[2],&g_770[2],&g_770[2],&g_770[2]},{&g_770[2],(void*)0,&g_770[2],&g_202,(void*)0,&g_202,&g_770[2],(void*)0,&g_770[2],&g_770[2]},{&g_1584,&g_770[2],&g_770[2],(void*)0,&g_770[2],&g_1584,&g_202,(void*)0,(void*)0,&g_202},{(void*)0,&g_1584,(void*)0,(void*)0,&g_1584,(void*)0,(void*)0,&g_1584,&g_770[2],&g_202}};
    int64_t l_1698[2];
    union U0 **l_1781 = (void*)0;
    union U0 **l_1787 = &g_826;
    int64_t **l_1821 = &g_1726[4][2][1];
    uint32_t l_1864[5][5] = {{0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L},{2UL,2UL,2UL,2UL,2UL},{0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L},{2UL,2UL,2UL,2UL,2UL},{0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L,0x0E10BFB4L}};
    uint32_t * const l_1874[8] = {&g_1877,(void*)0,&g_1877,(void*)0,&g_1877,(void*)0,&g_1877,(void*)0};
    uint32_t * const *l_1873[5];
    int8_t l_1946 = 0xE5L;
    int32_t l_1948 = 0x6B9BDF73L;
    uint32_t l_1997 = 0UL;
    int32_t ***l_2060 = &l_1328;
    uint8_t l_2069 = 0x57L;
    int32_t * const l_2156 = &g_92[0][0];
    union U0 *l_2172 = &g_2173;
    int32_t l_2245 = 0x9527A1DAL;
    uint32_t *l_2274 = &l_1864[2][0];
    uint32_t **l_2273 = &l_2274;
    float l_2300 = (-0x1.1p-1);
    uint8_t l_2361 = 0UL;
    uint32_t l_2377 = 0x14FCB8ACL;
    uint64_t ***l_2577 = (void*)0;
    uint64_t ****l_2576 = &l_2577;
    float **l_2617 = &l_1368[7][1][1];
    int32_t l_2628 = 5L;
    float l_2629 = 0x1.9p-1;
    const int8_t l_2649[7] = {0x26L,0L,0x26L,0x26L,0L,0x26L,0x26L};
    const int32_t l_2679 = 0x221DEE41L;
    uint8_t l_2691 = 0UL;
    uint16_t l_2764 = 0x45ABL;
    int16_t *l_2854[7][4][3] = {{{&g_1811,&g_123[4],&l_1541[0]},{&l_1541[1],(void*)0,(void*)0},{&g_1945[0],&g_177,&g_1212},{&g_1945[0],&g_1212,&g_1811}},{{&l_1541[1],&g_1945[0],&g_1212},{&g_1811,&g_1212,&l_1541[1]},{&g_1212,&g_1945[0],&g_1212},{&g_123[4],&g_1212,&g_123[4]}},{{&g_1945[0],&g_177,&g_123[4]},{&l_1541[0],(void*)0,&g_1212},{&g_1212,&g_123[4],&l_1541[1]},{&g_177,(void*)0,&g_1212}},{{&g_1212,&g_1212,&g_1811},{&l_1541[0],&l_1541[0],&g_1212},{&g_1945[0],&l_1541[0],(void*)0},{&g_123[4],&g_1212,&l_1541[0]}},{{&g_1212,(void*)0,&g_177},{&g_1811,&g_123[4],&l_1541[0]},{&l_1541[1],(void*)0,(void*)0},{&g_1945[0],&g_177,&g_1212}},{{&g_1945[0],&g_1212,&g_1811},{&l_1541[1],&g_1945[0],&g_1212},{&g_1811,&g_1212,&l_1541[1]},{&g_1212,&g_1945[0],&g_1212}},{{&g_123[4],&g_1212,&g_123[4]},{&g_1945[0],&g_177,&g_123[4]},{&l_1541[0],(void*)0,&g_1212},{&g_1212,&g_123[4],&l_1541[1]}}};
    float l_2861[7][2][1];
    uint32_t l_2907 = 0x79C2B7BAL;
    uint64_t **l_2910 = &g_534;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1520[i] = 0xDB6B0B0319E8B073LL;
    for (i = 0; i < 2; i++)
        l_1541[i] = 0xA92DL;
    for (i = 0; i < 2; i++)
        l_1698[i] = 0xAC510A4D75389B49LL;
    for (i = 0; i < 5; i++)
        l_1873[i] = &l_1874[4];
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
                l_2861[i][j][k] = (-0x3.Bp+1);
        }
    }
    if ((l_1327 != (l_1328 = &g_228)))
    { /* block id: 664 */
        int32_t l_1334 = (-7L);
        int32_t l_1336 = 0xF15DC928L;
        int32_t l_1338 = (-1L);
        const float *l_1370 = (void*)0;
        volatile float ** volatile * const **l_1388 = &g_1387;
        float *****l_1444 = &l_1383;
        int64_t *l_1448 = &g_135;
        uint8_t l_1455 = 3UL;
        uint16_t * const l_1472 = &g_200;
        uint16_t *l_1473 = &g_200;
        uint32_t l_1483 = 0x15437C27L;
        float *l_1519 = &g_86;
        int32_t l_1535 = 0L;
        int64_t l_1536 = (-5L);
        int32_t l_1537 = 0x6D70BEB8L;
        int32_t l_1538 = 0L;
        int32_t l_1539 = 0x8B78DB4BL;
        int32_t l_1540[10][10] = {{0xD587A51CL,0x911965DAL,0x02EC1690L,6L,7L,6L,0x02EC1690L,0x911965DAL,0xD587A51CL,0x6B2C9883L},{0L,(-1L),0x02EC1690L,7L,0x911965DAL,0x911965DAL,7L,0x02EC1690L,(-1L),0L},{(-1L),6L,0xD587A51CL,7L,(-5L),0L,(-5L),7L,0xD587A51CL,6L},{0x6B2C9883L,0x02EC1690L,0L,6L,(-5L),1L,1L,(-5L),6L,0L},{(-5L),(-5L),(-1L),0x6B2C9883L,0x911965DAL,1L,0xD587A51CL,1L,0x911965DAL,0x6B2C9883L},{0x6B2C9883L,5L,0x6B2C9883L,1L,7L,0L,0xD587A51CL,0xD587A51CL,0L,7L},{(-1L),(-5L),(-5L),(-1L),0x6B2C9883L,0x911965DAL,1L,0xD587A51CL,1L,0x911965DAL},{0L,5L,6L,5L,0xD587A51CL,(-1L),1L,0x02EC1690L,0x02EC1690L,1L},{0x6B2C9883L,(-1L),(-5L),(-5L),(-1L),0x6B2C9883L,0x911965DAL,1L,0xD587A51CL,1L},{5L,(-5L),0xD587A51CL,0x02EC1690L,0xD587A51CL,(-5L),5L,0x911965DAL,0L,0L}};
        int32_t l_1542 = 0xF713809EL;
        uint64_t ***l_1550[4];
        float *** const **l_1594[3];
        uint8_t l_1612 = 2UL;
        int32_t *l_1637[9] = {&g_133,&g_133,&g_133,&g_133,&g_133,&g_133,&g_133,&g_133,&g_133};
        int32_t **l_1636[3];
        int64_t l_1685[4];
        int32_t l_1687[9][10][2] = {{{(-4L),(-9L)},{0xA2501861L,(-1L)},{0x36A97903L,(-1L)},{0xA2501861L,(-9L)},{(-4L),0xEC9F8ED2L},{0x228B1026L,(-1L)},{(-4L),0x719E529FL},{0xA2501861L,0xEC9F8ED2L},{0x36A97903L,0xEC9F8ED2L},{0xA2501861L,0x719E529FL}},{{(-4L),(-1L)},{0x228B1026L,0xEC9F8ED2L},{(-4L),(-9L)},{0xA2501861L,(-1L)},{0x36A97903L,(-1L)},{0xA2501861L,(-9L)},{(-4L),0xEC9F8ED2L},{0x228B1026L,(-1L)},{(-4L),0x719E529FL},{0xA2501861L,0xEC9F8ED2L}},{{0x36A97903L,0xEC9F8ED2L},{0xA2501861L,0x719E529FL},{(-4L),(-1L)},{0x228B1026L,0xEC9F8ED2L},{(-4L),(-9L)},{0xA2501861L,(-1L)},{0x36A97903L,(-1L)},{0xA2501861L,(-9L)},{(-4L),0xEC9F8ED2L},{0x228B1026L,(-1L)}},{{(-4L),0x719E529FL},{0xA2501861L,0xEC9F8ED2L},{0x36A97903L,0xEC9F8ED2L},{0xA2501861L,0x719E529FL},{(-4L),(-1L)},{0x228B1026L,0xEC9F8ED2L},{(-4L),(-9L)},{0xA2501861L,(-1L)},{0x36A97903L,(-1L)},{0xA2501861L,(-9L)}},{{(-4L),0xEC9F8ED2L},{0x228B1026L,(-1L)},{(-4L),0x719E529FL},{0xA2501861L,0xEC9F8ED2L},{0x36A97903L,0xEC9F8ED2L},{0xA2501861L,0x719E529FL},{(-4L),(-1L)},{0x228B1026L,0xEC9F8ED2L},{(-4L),(-9L)},{0xA2501861L,(-1L)}},{{0x36A97903L,(-1L)},{0xA2501861L,(-9L)},{(-4L),0xEC9F8ED2L},{0x228B1026L,(-1L)},{(-4L),0x719E529FL},{0xA2501861L,0xEC9F8ED2L},{0x36A97903L,0xEC9F8ED2L},{0xA2501861L,0x719E529FL},{(-4L),(-1L)},{0x228B1026L,0xEC9F8ED2L}},{{(-4L),(-9L)},{0xA2501861L,(-1L)},{0x36A97903L,(-1L)},{0xA2501861L,(-9L)},{(-4L),0xEC9F8ED2L},{0x228B1026L,(-1L)},{(-4L),0x719E529FL},{0xA2501861L,0xEC9F8ED2L},{0x36A97903L,0xEC9F8ED2L},{0xA2501861L,0x719E529FL}},{{(-4L),(-1L)},{0x228B1026L,0xEC9F8ED2L},{(-4L),(-9L)},{0xA2501861L,(-1L)},{0x36A97903L,(-1L)},{0xA2501861L,(-9L)},{0x36A97903L,(-9L)},{(-1L),0x719E529FL},{0x36A97903L,0x441CABDAL},{0x228B1026L,(-9L)}},{{(-4L),(-9L)},{0x228B1026L,0x441CABDAL},{0x36A97903L,0x719E529FL},{(-1L),(-9L)},{0x36A97903L,0x3782DD34L},{0x228B1026L,0x719E529FL},{(-4L),0x719E529FL},{0x228B1026L,0x3782DD34L},{0x36A97903L,(-9L)},{(-1L),0x719E529FL}}};
        uint16_t l_1690 = 0x277AL;
        uint64_t l_1711[6] = {18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL};
        int64_t **l_1724 = (void*)0;
        uint32_t l_1761 = 0x338B0C2BL;
        int32_t *l_1802 = &l_1336;
        uint32_t * const l_1872 = &g_126[5];
        uint32_t * const *l_1871 = &l_1872;
        uint32_t *** const l_1905 = &l_1581;
        uint32_t l_1915 = 8UL;
        const int32_t **l_1949[10][7][3] = {{{&g_99[0],&g_99[1],&g_99[0]},{(void*)0,&g_99[1],&g_99[1]},{&g_99[0],&g_99[0],&g_99[1]},{(void*)0,&g_99[1],(void*)0},{(void*)0,&g_99[1],&g_99[0]},{(void*)0,&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],(void*)0}},{{&g_99[0],(void*)0,(void*)0},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[1],(void*)0},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[0],(void*)0},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],&g_99[0],&g_99[1]}},{{&g_99[1],(void*)0,&g_99[0]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],(void*)0,&g_99[0]},{&g_99[1],(void*)0,(void*)0}},{{&g_99[1],&g_99[1],&g_99[0]},{&g_99[0],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{(void*)0,&g_99[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_99[1],&g_99[1]},{&g_99[0],(void*)0,(void*)0}},{{(void*)0,&g_99[1],(void*)0},{&g_99[0],&g_99[1],&g_99[1]},{(void*)0,&g_99[0],&g_99[1]},{&g_99[0],&g_99[1],&g_99[0]},{(void*)0,&g_99[1],(void*)0},{&g_99[0],&g_99[0],&g_99[0]},{&g_99[1],&g_99[1],&g_99[1]}},{{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],(void*)0,&g_99[1]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[0],&g_99[0],&g_99[0]},{&g_99[0],&g_99[0],&g_99[1]},{(void*)0,&g_99[0],&g_99[0]},{&g_99[1],(void*)0,(void*)0}},{{&g_99[0],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],(void*)0},{(void*)0,&g_99[0],&g_99[1]},{&g_99[1],&g_99[1],(void*)0},{(void*)0,&g_99[1],(void*)0},{&g_99[1],&g_99[0],&g_99[1]},{(void*)0,&g_99[1],&g_99[0]}},{{&g_99[1],&g_99[1],(void*)0},{&g_99[0],(void*)0,&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[0],&g_99[0],(void*)0},{(void*)0,&g_99[1],(void*)0},{&g_99[0],(void*)0,(void*)0},{&g_99[1],&g_99[1],(void*)0}},{{&g_99[0],&g_99[0],&g_99[0]},{&g_99[1],&g_99[1],&g_99[0]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[1],&g_99[1]},{&g_99[1],&g_99[0],&g_99[0]},{&g_99[1],&g_99[0],(void*)0},{&g_99[1],&g_99[0],&g_99[0]}},{{&g_99[0],&g_99[1],&g_99[0]},{&g_99[1],&g_99[0],(void*)0},{(void*)0,&g_99[1],&g_99[0]},{&g_99[1],&g_99[0],&g_99[1]},{&g_99[0],&g_99[1],&g_99[1]},{(void*)0,&g_99[1],&g_99[0]},{(void*)0,&g_99[0],&g_99[0]}}};
        uint32_t *l_1996[2];
        uint32_t **l_1995[4];
        uint32_t ***l_1994 = &l_1995[3];
        uint32_t ****l_1993 = &l_1994;
        int8_t l_2005 = 0xC1L;
        int64_t l_2019 = 5L;
        uint16_t l_2118 = 0UL;
        uint64_t l_2145 = 0xB6E743FD0416CBB4LL;
        float l_2167 = 0xA.CA3631p-27;
        int16_t **l_2230 = (void*)0;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1550[i] = &g_533[3][1][2];
        for (i = 0; i < 3; i++)
            l_1594[i] = &g_1593;
        for (i = 0; i < 3; i++)
            l_1636[i] = &l_1637[8];
        for (i = 0; i < 4; i++)
            l_1685[i] = 0xFE18065ABCAE42BFLL;
        for (i = 0; i < 2; i++)
            l_1996[i] = &g_1877;
        for (i = 0; i < 4; i++)
            l_1995[i] = &l_1996[0];
lbl_1693:
        if (p_27)
        { /* block id: 665 */
            int32_t l_1331[3];
            int32_t l_1335 = 1L;
            int32_t l_1337 = 0L;
            int8_t *l_1342 = &g_202;
            const int32_t **l_1428[1];
            int64_t *l_1449 = &g_1171.f1;
            int32_t l_1458 = 0x791BD8CBL;
            int32_t l_1460[6][4] = {{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)}};
            uint16_t *l_1475[10][7][3] = {{{&g_1171.f3,(void*)0,&g_200},{&g_200,&g_827.f3,&g_200},{&g_1171.f3,&g_200,&g_200},{(void*)0,&g_200,(void*)0},{&g_1171.f3,(void*)0,&l_1339},{&g_1171.f3,&l_1339,&l_1339},{&g_1171.f3,&g_827.f3,&l_1339}},{{(void*)0,&l_1339,&g_200},{&g_1171.f3,&g_200,(void*)0},{&g_200,&g_200,&g_827.f3},{&g_1171.f3,&g_200,&g_200},{&g_1171.f3,&g_200,&g_827.f3},{&g_200,&g_200,&l_1339},{&l_1339,&g_200,&g_1171.f3}},{{&l_1339,&l_1339,&l_1339},{(void*)0,&g_827.f3,&g_200},{&g_200,&l_1339,&l_1339},{(void*)0,(void*)0,&g_200},{&l_1339,&g_200,&l_1339},{&g_827.f3,&g_200,&g_1171.f3},{&g_1171.f3,&g_827.f3,&l_1339}},{{&l_1339,(void*)0,&g_827.f3},{&g_1171.f3,&g_827.f3,&g_200},{&g_1171.f3,&g_827.f3,&g_827.f3},{&l_1339,(void*)0,(void*)0},{&g_1171.f3,&g_200,&g_200},{&g_827.f3,&l_1339,&l_1339},{&l_1339,&g_827.f3,&l_1339}},{{(void*)0,&g_827.f3,&l_1339},{&g_200,&g_827.f3,(void*)0},{(void*)0,&l_1339,&g_200},{&l_1339,&g_200,&g_200},{&l_1339,(void*)0,&g_200},{&g_200,&g_827.f3,&g_1171.f3},{&g_1171.f3,&g_827.f3,&g_1171.f3}},{{&g_1171.f3,(void*)0,&g_200},{&g_200,&g_827.f3,&g_200},{&g_1171.f3,&g_200,&g_200},{(void*)0,&g_200,&g_200},{(void*)0,&g_200,&g_200},{&g_827.f3,&l_1339,&l_1339},{(void*)0,(void*)0,&g_1171.f3}},{{(void*)0,&g_200,&l_1339},{&l_1339,&g_827.f3,&g_200},{&g_1171.f3,(void*)0,&g_1171.f3},{&g_200,&g_827.f3,(void*)0},{(void*)0,&g_827.f3,&l_1339},{&l_1339,(void*)0,&l_1339},{&l_1339,&g_827.f3,(void*)0}},{{&g_200,&g_200,&g_200},{(void*)0,(void*)0,(void*)0},{&g_1171.f3,&l_1339,&g_827.f3},{(void*)0,&g_200,(void*)0},{&l_1339,&g_200,&g_200},{&g_200,(void*)0,(void*)0},{&g_827.f3,(void*)0,&l_1339}},{{&g_200,(void*)0,&l_1339},{&l_1339,&g_1171.f3,(void*)0},{&l_1339,(void*)0,&g_1171.f3},{&g_200,&g_200,&g_200},{&g_827.f3,&g_827.f3,&l_1339},{&g_200,&l_1339,&g_1171.f3},{&l_1339,&g_1171.f3,&l_1339}},{{(void*)0,(void*)0,&g_200},{&g_1171.f3,&g_1171.f3,&g_200},{(void*)0,&l_1339,&g_200},{&g_200,&g_827.f3,&g_827.f3},{&l_1339,&g_200,&l_1339},{&l_1339,(void*)0,(void*)0},{(void*)0,&g_1171.f3,(void*)0}}};
            uint16_t **l_1474[5][5][3] = {{{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[2][6][1],&l_1475[2][5][2],&l_1475[2][6][1]},{&l_1475[0][6][0],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[5][5][1],&l_1475[5][5][1],&l_1475[5][5][2]},{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]}},{{&l_1475[5][5][2],&l_1475[2][5][2],(void*)0},{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[5][5][1],&l_1475[5][5][2],(void*)0},{&l_1475[0][6][0],&l_1475[0][6][0],&l_1475[2][6][1]},{&l_1475[2][6][1],&l_1475[5][5][2],&l_1475[5][5][2]}},{{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[2][6][1],&l_1475[2][5][2],&l_1475[2][6][1]},{&l_1475[0][6][0],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[5][5][1],&l_1475[5][5][1],&l_1475[5][5][2]},{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]}},{{&l_1475[5][5][2],&l_1475[2][5][2],(void*)0},{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[5][5][1],&l_1475[5][5][2],(void*)0},{&l_1475[0][6][0],&l_1475[0][6][0],&l_1475[2][6][1]},{&l_1475[2][6][1],&l_1475[5][5][2],&l_1475[5][5][2]}},{{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[2][6][1],&l_1475[2][5][2],&l_1475[2][6][1]},{&l_1475[0][6][0],&l_1475[2][6][1],&l_1475[2][6][1]},{&l_1475[5][5][1],&l_1475[5][5][1],&l_1475[5][5][2]},{&l_1475[2][6][1],&l_1475[2][6][1],&l_1475[2][6][1]}}};
            const float * const *l_1486 = &l_1370;
            const float * const **l_1485[3][8] = {{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486},{&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486,&l_1486}};
            uint32_t *l_1492 = &g_126[4];
            uint8_t *l_1493[1][3][3] = {{{&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1331[i] = 1L;
            for (i = 0; i < 1; i++)
                l_1428[i] = &g_99[1];
lbl_1416:
            for (g_1171.f1 = (-14); (g_1171.f1 < (-10)); g_1171.f1 = safe_add_func_int8_t_s_s(g_1171.f1, 5))
            { /* block id: 668 */
                int32_t *l_1332[6] = {&g_41[4][3][2],&g_41[4][3][2],&g_41[4][3][2],&g_41[4][3][2],&g_41[4][3][2],&g_41[4][3][2]};
                int8_t *l_1343 = &g_202;
                uint64_t ***l_1375[4][10][2] = {{{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]},{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]}},{{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]},{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]}},{{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]},{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]}},{{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]},{&g_533[3][0][2],(void*)0},{&g_533[3][0][2],&g_533[3][0][2]},{&g_533[0][1][3],&g_533[3][1][2]},{&g_533[3][0][2],&g_533[3][1][2]},{&g_533[0][1][3],&g_533[3][0][2]}}};
                uint32_t l_1384[3];
                union U0 *l_1390 = &g_1391;
                int64_t l_1406 = 1L;
                float ***l_1414 = &g_223;
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_1384[i] = 0UL;
                ++l_1339;
                if (p_27)
                    break;
                if ((0xCC2BF059L > 0x84F7E90DL))
                { /* block id: 671 */
                    uint8_t l_1350 = 255UL;
                    float *l_1369[3];
                    union U0 **l_1389[4][3] = {{&g_826,&g_826,&g_826},{&g_826,&g_826,&g_826},{&g_826,&g_826,&g_826},{&g_826,&g_826,&g_826}};
                    float * const ***l_1394 = &g_1242;
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1369[i] = &g_1184;
                    if ((p_28 <= (l_1342 != l_1343)))
                    { /* block id: 672 */
                        uint64_t *l_1348 = (void*)0;
                        uint64_t *l_1349[9] = {&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121};
                        uint16_t *l_1357[3];
                        int32_t l_1358 = 1L;
                        int16_t *l_1361 = (void*)0;
                        int16_t *l_1362 = &g_1212;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1357[i] = &g_1169.f3;
                        (*g_1166) |= (safe_mod_func_uint64_t_u_u((((safe_add_func_int16_t_s_s(0xD7CEL, ((--l_1350) || p_28))) == (safe_mul_func_int16_t_s_s(p_28, 0xF7EDL))) , (((p_28 <= ((safe_add_func_uint16_t_u_u((l_1358 = l_1350), ((safe_mul_func_int16_t_s_s(((*l_1362) = ((&g_135 == (void*)0) , ((p_28 || p_27) < p_28))), 0UL)) & p_27))) && g_133)) < 1L) , l_1337)), p_28));
                    }
                    else
                    { /* block id: 677 */
                        uint32_t l_1363 = 0x19B92EEBL;
                        uint64_t ***l_1377[9] = {(void*)0,(void*)0,&g_533[3][0][2],(void*)0,(void*)0,&g_533[3][0][2],(void*)0,(void*)0,&g_533[3][0][2]};
                        uint64_t ****l_1376 = &l_1377[2];
                        uint8_t *l_1385[5][8][6] = {{{&l_1350,(void*)0,&l_1350,&g_196,(void*)0,&l_1350},{&g_196,(void*)0,&g_196,&l_1350,&l_1350,&g_196},{&g_196,&g_196,&l_1350,&l_1350,(void*)0,(void*)0},{&g_196,(void*)0,&l_1350,&g_196,&g_196,&l_1350},{&l_1350,&g_196,&l_1350,(void*)0,&g_196,(void*)0},{&g_196,(void*)0,&l_1350,(void*)0,(void*)0,&g_196},{(void*)0,(void*)0,&g_196,(void*)0,&g_196,&l_1350},{&g_196,&g_196,&l_1350,&g_196,&g_196,&l_1350}},{{&g_196,(void*)0,&l_1350,(void*)0,(void*)0,&g_196},{(void*)0,&g_196,(void*)0,(void*)0,&l_1350,&g_196},{&g_196,&l_1350,(void*)0,&l_1350,&l_1350,(void*)0},{&g_196,&l_1350,(void*)0,&g_196,&l_1350,&g_196},{&l_1350,&l_1350,&l_1350,&g_196,&g_196,&l_1350},{&l_1350,&l_1350,&g_196,&g_196,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&g_196,&l_1350,&g_196},{&g_196,&l_1350,&g_196,&l_1350,&l_1350,&g_196}},{{&l_1350,&l_1350,&g_196,&l_1350,&l_1350,&l_1350},{&l_1350,&l_1350,&l_1350,&l_1350,&l_1350,&g_196},{&l_1350,&l_1350,(void*)0,&l_1350,&l_1350,(void*)0},{&l_1350,&l_1350,(void*)0,&l_1350,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&l_1350,&g_196,&g_196},{&l_1350,&l_1350,(void*)0,&l_1350,&l_1350,(void*)0},{&g_196,&l_1350,(void*)0,&g_196,&l_1350,&g_196},{&l_1350,&l_1350,&l_1350,&g_196,&g_196,&l_1350}},{{&l_1350,&l_1350,&g_196,&g_196,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&g_196,&l_1350,&g_196},{&g_196,&l_1350,&g_196,&l_1350,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&l_1350,&l_1350,&l_1350},{&l_1350,&l_1350,&l_1350,&l_1350,&l_1350,&g_196},{&l_1350,&l_1350,(void*)0,&l_1350,&l_1350,(void*)0},{&l_1350,&l_1350,(void*)0,&l_1350,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&l_1350,&g_196,&g_196}},{{&l_1350,&l_1350,(void*)0,&l_1350,&l_1350,(void*)0},{&g_196,&l_1350,(void*)0,&g_196,&l_1350,&g_196},{&l_1350,&l_1350,&l_1350,&g_196,&g_196,&l_1350},{&l_1350,&l_1350,&g_196,&g_196,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&g_196,&l_1350,&g_196},{&g_196,&l_1350,&g_196,&l_1350,&l_1350,&g_196},{&l_1350,&l_1350,&g_196,&l_1350,&l_1350,&l_1350},{&l_1350,&l_1350,&l_1350,&l_1350,&l_1350,&g_196}}};
                        int i, j, k;
                        (*l_1328) = (void*)0;
                        l_1363--;
                        (*g_40) ^= ((safe_lshift_func_uint8_t_u_s(((l_1369[0] = l_1368[2][1][1]) == l_1370), 4)) == (safe_sub_func_int8_t_s_s((safe_mul_func_int8_t_s_s(0x4BL, (g_196 = (((l_1375[2][7][1] == ((*l_1376) = &g_533[5][1][1])) , (safe_lshift_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s(p_28)), ((safe_sub_func_uint16_t_u_u(((p_27 != 0xC46549EDL) != ((l_1383 != (void*)0) ^ l_1363)), p_27)) ^ l_1384[1])))) != 0x4AL)))), (-1L))));
                        if (p_27)
                            break;
                    }
                    if (p_27)
                    { /* block id: 686 */
                        l_1388 = g_1386;
                        if (p_27)
                            goto lbl_1416;
                        return p_28;
                    }
                    else
                    { /* block id: 689 */
                        l_1389[0][2] = l_1389[0][0];
                        (*g_1166) &= l_1350;
                    }
                    if (p_27)
                    { /* block id: 693 */
                        uint32_t l_1399 = 0xBE653E23L;
                        uint64_t ****l_1402 = &l_1375[0][5][0];
                        if (l_1337)
                            break;
                        (*g_825) = l_1390;
                        (*g_40) &= ((safe_mod_func_int64_t_s_s(((*g_1386) == (p_28 , l_1394)), (safe_sub_func_uint8_t_u_u(((g_92[3][0] < (p_28 , (safe_lshift_func_uint16_t_u_u(l_1399, 7)))) , (safe_mul_func_int16_t_s_s((&g_216 == (g_126[4] , func_37(func_37(l_1369[0], &g_99[1]), l_1327))), l_1335))), g_97)))) != l_1334);
                        (*l_1402) = l_1375[2][7][1];
                    }
                    else
                    { /* block id: 698 */
                        (*l_1327) = l_1369[0];
                    }
                    (*g_40) |= p_28;
                }
                else
                { /* block id: 702 */
                    int32_t *l_1405 = &l_1331[1];
                    uint16_t *l_1412 = (void*)0;
                    uint16_t *l_1413 = &g_200;
                    uint32_t *l_1415 = &g_198;
                    if ((*g_1166))
                        break;
                    (*g_40) ^= (((safe_mod_func_uint16_t_u_u(0xC88FL, p_27)) && (((*l_1405) &= (-1L)) , ((*l_1415) ^= (((l_1406 != (((!8UL) || g_123[4]) ^ ((0x94D8L | (safe_div_func_int16_t_s_s(p_28, (safe_lshift_func_uint16_t_u_u((((*l_1413) = ((l_1331[0] | 0xF0E01C36569CF05BLL) , 0x93BAL)) & 65529UL), 1))))) <= g_93))) , l_1414) == (void*)0)))) <= g_202);
                    (*g_40) ^= 0xB56218AFL;
                }
            }
            for (g_827.f2 = 0; (g_827.f2 <= 40); g_827.f2 = safe_add_func_uint64_t_u_u(g_827.f2, 7))
            { /* block id: 714 */
                uint64_t l_1421 = 18446744073709551608UL;
                int64_t **l_1450 = (void*)0;
                int64_t *l_1451 = &g_1391.f1;
                uint64_t *l_1454 = &g_121;
                int32_t l_1457[5] = {0x265DA3F3L,0x265DA3F3L,0x265DA3F3L,0x265DA3F3L,0x265DA3F3L};
                int64_t l_1459 = 6L;
                int i;
                for (g_177 = 11; (g_177 < (-25)); g_177--)
                { /* block id: 717 */
                    ++l_1421;
                }
                l_1337 = (safe_sub_func_uint16_t_u_u(0UL, 0x25C6L));
                for (g_1212 = (-2); (g_1212 <= 23); g_1212++)
                { /* block id: 723 */
                    (*l_1328) = func_37(&l_1334, l_1428[0]);
                }
                if ((((safe_rshift_func_int16_t_s_s(((((safe_rshift_func_int16_t_s_s(((safe_unary_minus_func_int8_t_s((safe_rshift_func_uint16_t_u_s((((safe_div_func_int8_t_s_s(((((1L <= (safe_add_func_uint32_t_u_u(0UL, (0UL >= (safe_lshift_func_uint16_t_u_s(g_638, 3)))))) <= ((g_1445 = l_1444) == (void*)0)) ^ (l_1448 != (l_1451 = l_1449))) <= (((*l_1454) = ((safe_sub_func_uint64_t_u_u(0x304C56C1378383EFLL, 0x1453135611F512A4LL)) < g_938)) ^ p_28)), g_41[4][3][2])) , g_126[1]) || l_1455), 4)))) , p_28), l_1421)) == (-9L)) || 0UL) <= p_28), l_1336)) > g_123[0]) || g_561))
                { /* block id: 729 */
                    return (*g_224);
                }
                else
                { /* block id: 731 */
                    int32_t *l_1456[9] = {&l_1335,&l_1335,&l_1335,&l_1335,&l_1335,&l_1335,&l_1335,&l_1335,&l_1335};
                    uint32_t l_1461 = 0xA0B0C3B4L;
                    int i;
                    ++l_1461;
                }
            }
            (*g_1166) &= (safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((p_27 >= ((safe_sub_func_int8_t_s_s(l_1336, (safe_add_func_int32_t_s_s((l_1472 != (g_1476 = (l_1473 = &l_1339))), ((safe_add_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s(((safe_sub_func_uint64_t_u_u(((l_1483 , g_638) != ((l_1338 = (l_1458 = (!(l_1485[0][3] == ((~((*l_1472) = ((safe_rshift_func_int16_t_s_s(g_938, (safe_div_func_uint32_t_u_u((((*l_1492) ^= (((((1UL < p_27) & g_97) == l_1483) || 0xAAE220A92607D2CALL) , p_28)) ^ 0x152653A9L), l_1338)))) , p_27))) , (void*)0))))) && 0xE6L)), g_202)) < 0x9A36D79275B18E3DLL), p_27)), (*g_532))) < g_638))))) , (-1L))), 0UL)), g_202));
        }
        else
        { /* block id: 742 */
            uint64_t *l_1498 = &g_121;
            int32_t l_1499 = 2L;
            uint32_t *l_1500 = &g_126[4];
            uint32_t *l_1501 = &g_938;
            uint16_t **l_1503 = &l_1473;
            int16_t *l_1506 = &g_123[4];
            int32_t *l_1521 = &l_1336;
            int32_t *l_1522 = &g_41[4][3][2];
            int32_t *l_1523 = &g_561;
            int32_t *l_1524 = (void*)0;
            int32_t *l_1525 = &g_92[3][0];
            int32_t *l_1526 = &g_92[3][0];
            int32_t *l_1527 = &g_156;
            int32_t l_1528[10][3][2] = {{{6L,0x473E4B82L},{1L,(-10L)},{0L,0x473E4B82L}},{{0L,0x473E4B82L},{0L,(-10L)},{1L,0x473E4B82L}},{{6L,0x473E4B82L},{1L,(-10L)},{0L,0x473E4B82L}},{{0L,0x473E4B82L},{0L,(-10L)},{1L,0x473E4B82L}},{{6L,0x473E4B82L},{1L,(-10L)},{0L,0x473E4B82L}},{{0L,0x473E4B82L},{0L,(-10L)},{1L,0x473E4B82L}},{{6L,0x473E4B82L},{1L,(-10L)},{0L,0x473E4B82L}},{{0L,0x473E4B82L},{0L,(-10L)},{1L,0x473E4B82L}},{{6L,0x473E4B82L},{1L,(-10L)},{0L,0x473E4B82L}},{{0L,0x473E4B82L},{0L,(-10L)},{1L,0x473E4B82L}}};
            int32_t *l_1529 = &l_1336;
            int32_t *l_1530 = &l_1528[0][2][0];
            int32_t *l_1531 = &l_1333[1];
            int32_t *l_1532 = &g_561;
            int32_t *l_1533 = &g_93;
            int32_t *l_1534[6][2][8] = {{{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0},{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0}},{{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0},{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0}},{{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0},{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0}},{{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0},{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0}},{{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0},{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0}},{{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0},{&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0,&g_561,(void*)0}}};
            int i, j, k;
            (*g_40) = (g_198 & (((safe_lshift_func_uint16_t_u_s(g_177, 15)) > ((l_1368[2][1][1] == (void*)0) ^ l_1455)) <= ((*l_1501) |= ((*l_1500) = (((safe_rshift_func_int8_t_s_s(0L, 1)) != g_200) , (((*l_1498) = p_27) >= ((((((0L ^ p_27) > l_1336) <= l_1499) <= p_28) | p_27) | p_27)))))));
            (*g_1166) = ((((~(0x4CC7E26AL < ((void*)0 != l_1503))) || ((safe_lshift_func_int16_t_s_u(((*l_1506) &= p_27), l_1499)) ^ ((safe_mul_func_uint8_t_u_u((safe_mod_func_int32_t_s_s(p_28, ((l_1499 , (safe_sub_func_int32_t_s_s((((safe_lshift_func_uint16_t_u_s((safe_add_func_uint8_t_u_u(2UL, (safe_mul_func_int16_t_s_s((g_196 || (*g_532)), 0x2C54L)))), 11)) , l_1519) != (void*)0), p_28))) && l_1334))), p_27)) && l_1520[0]))) , p_28) , p_28);
            --l_1543;
            for (g_1391.f1 = 0; (g_1391.f1 != 9); g_1391.f1 = safe_add_func_int16_t_s_s(g_1391.f1, 3))
            { /* block id: 752 */
                if (l_1540[5][2])
                    break;
            }
        }
        for (l_1542 = 0; (l_1542 <= 1); l_1542 += 1)
        { /* block id: 758 */
            int64_t l_1558 = 6L;
            uint16_t *l_1559[7] = {&l_1339,(void*)0,&l_1339,&l_1339,(void*)0,&l_1339,&l_1339};
            int32_t * const l_1561 = &g_41[4][6][3];
            int32_t l_1586 = 0x7B1ED54EL;
            int32_t l_1602 = 0x632E5AF2L;
            int32_t l_1603 = 0x127B8A39L;
            int32_t l_1609[6] = {0x6D6ADCB9L,0x6D6ADCB9L,0x6D6ADCB9L,0x6D6ADCB9L,0x6D6ADCB9L,0x6D6ADCB9L};
            union U0 *l_1656[3];
            int32_t *l_1694 = &g_93;
            const float * const *l_1709 = &l_1370;
            const float * const **l_1708 = &l_1709;
            uint32_t l_1762 = 0xA72AA227L;
            uint32_t * const l_1768 = &g_1769[4];
            uint32_t * const *l_1767[3][8][6] = {{{&l_1768,&l_1768,&l_1768,(void*)0,&l_1768,&l_1768},{(void*)0,&l_1768,&l_1768,(void*)0,&l_1768,&l_1768},{&l_1768,(void*)0,&l_1768,(void*)0,&l_1768,(void*)0},{(void*)0,&l_1768,&l_1768,&l_1768,&l_1768,&l_1768},{(void*)0,&l_1768,(void*)0,&l_1768,&l_1768,&l_1768},{&l_1768,(void*)0,&l_1768,(void*)0,(void*)0,&l_1768},{&l_1768,&l_1768,&l_1768,&l_1768,(void*)0,(void*)0},{(void*)0,&l_1768,&l_1768,&l_1768,(void*)0,&l_1768}},{{&l_1768,(void*)0,&l_1768,&l_1768,&l_1768,(void*)0},{&l_1768,&l_1768,&l_1768,&l_1768,&l_1768,&l_1768},{&l_1768,&l_1768,&l_1768,&l_1768,&l_1768,&l_1768},{(void*)0,&l_1768,(void*)0,&l_1768,&l_1768,&l_1768},{&l_1768,(void*)0,&l_1768,(void*)0,(void*)0,&l_1768},{&l_1768,&l_1768,&l_1768,&l_1768,(void*)0,(void*)0},{(void*)0,&l_1768,&l_1768,&l_1768,(void*)0,&l_1768},{&l_1768,(void*)0,&l_1768,&l_1768,&l_1768,(void*)0}},{{&l_1768,&l_1768,&l_1768,&l_1768,&l_1768,&l_1768},{&l_1768,&l_1768,&l_1768,&l_1768,&l_1768,&l_1768},{(void*)0,&l_1768,(void*)0,&l_1768,&l_1768,&l_1768},{&l_1768,(void*)0,&l_1768,(void*)0,(void*)0,&l_1768},{&l_1768,&l_1768,&l_1768,&l_1768,(void*)0,(void*)0},{(void*)0,&l_1768,&l_1768,&l_1768,(void*)0,&l_1768},{&l_1768,(void*)0,&l_1768,&l_1768,&l_1768,(void*)0},{&l_1768,&l_1768,&l_1768,&l_1768,&l_1768,&l_1768}}};
            uint32_t * const **l_1766 = &l_1767[1][3][2];
            uint32_t * const ***l_1765 = &l_1766;
            uint32_t * const ****l_1764 = &l_1765;
            union U0 **l_1786 = &l_1656[2];
            uint8_t l_1861 = 252UL;
            float *****l_1944[10] = {&g_1446,&g_1446,&l_1383,&g_1446,&g_1446,&l_1383,&g_1446,&g_1446,&l_1383,&g_1446};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1656[i] = &g_1657;
            (*g_40) |= ((&g_531 != l_1550[0]) >= ((g_1560 &= ((255UL ^ 0x11L) || (l_1334 = ((((((((l_1551 = &l_1339) == ((0x1F9786458C791DF8LL == (safe_lshift_func_int16_t_s_u(((safe_mod_func_int16_t_s_s((((p_28 && (l_1537 &= (!(((void*)0 != l_1557) | 0L)))) , 3UL) <= l_1558), g_1212)) == g_123[4]), 6))) , l_1559[1])) , p_27) , g_638) <= 4294967293UL) , (void*)0) != (*g_1445)) , g_200)))) <= p_28));
            for (g_198 = 0; (g_198 <= 1); g_198 += 1)
            { /* block id: 766 */
                int8_t *l_1583 = &g_770[2];
                int32_t l_1585 = 0xDAAF6E44L;
                const int32_t **l_1599 = (void*)0;
                int32_t l_1604 = 0x59D4D5B4L;
                int32_t l_1605 = (-8L);
                int32_t l_1606 = 0xE7E735E1L;
                int32_t l_1607 = 0x572C3277L;
                int32_t l_1608 = 0xB60F84F0L;
                int32_t l_1610 = (-1L);
                int32_t l_1611 = (-7L);
                float l_1684[6][3] = {{0x9.4A07B5p+2,0xF.2474F2p+1,0xF.2474F2p+1},{0x9.4A07B5p+2,0xF.2474F2p+1,0xF.2474F2p+1},{0x9.4A07B5p+2,0xF.2474F2p+1,0xF.2474F2p+1},{0x9.4A07B5p+2,0xF.2474F2p+1,0xF.2474F2p+1},{0x9.4A07B5p+2,0xF.2474F2p+1,0xF.2474F2p+1},{0x9.4A07B5p+2,0xF.2474F2p+1,0xF.2474F2p+1}};
                uint16_t l_1705 = 1UL;
                int i, j;
                (*l_1328) = func_37(l_1561, l_1327);
                (**l_1328) = (((((safe_mod_func_int16_t_s_s((g_198 ^ (safe_add_func_uint64_t_u_u((++(*l_1557)), ((((((safe_lshift_func_int8_t_s_s(((safe_add_func_uint32_t_u_u((((0xBB2DL <= ((g_493 | ((safe_mul_func_uint16_t_u_u(0x70E7L, (((safe_add_func_int64_t_s_s((safe_sub_func_uint16_t_u_u(((0xF559DC24L || (0L != p_28)) , (p_28 <= ((*l_1583) = (l_1580[0][0][2] != (void*)0)))), g_135)), 1L)) > (*l_1561)) > g_1212))) | g_1584)) >= (*l_1561))) | (**l_1328)) >= l_1585), l_1334)) , 0L), l_1455)) && p_27) && 0xE5L) == (-9L)) | 0x07L) > 4294967291UL)))), (-2L))) & p_28) ^ 0xDC80DC61L) ^ g_1584) & p_27);
                l_1586 |= (*g_228);
                for (g_561 = 1; (g_561 >= 0); g_561 -= 1)
                { /* block id: 774 */
                    uint8_t l_1598 = 0x28L;
                    int32_t *l_1600 = &g_41[5][2][3];
                    int32_t *l_1601[1][4];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 4; j++)
                            l_1601[i][j] = &l_1539;
                    }
                    (*l_1328) = func_37(&l_1338, ((g_202 ^ (((((!(l_1585 , ((*l_1583) = ((**g_1446) == ((safe_div_func_int32_t_s_s(0x12BE199FL, (safe_sub_func_int64_t_s_s((l_1585 & (((p_27 < ((l_1594[0] = g_1592[3][2][5]) != (l_1595 = l_1595))) , &l_1585) != &l_1586)), (*l_1561))))) , (void*)0))))) < l_1597) , l_1598) ^ g_561) , l_1598)) , l_1599));
                    --l_1612;
                    (**l_1328) = (6UL >= (safe_add_func_int64_t_s_s(g_93, (**l_1328))));
                    (*l_1600) ^= (p_27 , ((safe_rshift_func_uint8_t_u_u(255UL, 2)) != p_27));
                    for (l_1606 = 1; (l_1606 >= 0); l_1606 -= 1)
                    { /* block id: 784 */
                        g_92[0][0] = ((*g_1166) = (p_28 | (*l_1561)));
                        if (p_28)
                            continue;
                        l_1604 |= (safe_div_func_int32_t_s_s((safe_sub_func_int32_t_s_s(((((*l_1600) ^ (**l_1328)) , 3UL) | 1UL), ((0x39L == g_41[4][3][2]) , ((g_126[4] , (((*l_1583) = (safe_add_func_uint8_t_u_u(g_196, (~((safe_sub_func_int64_t_s_s(((0x5D6DL == p_28) != g_493), p_28)) | p_28))))) >= p_27)) <= g_92[2][0])))), l_1539));
                    }
                }
                for (l_1483 = 0; (l_1483 <= 1); l_1483 += 1)
                { /* block id: 794 */
                    uint32_t l_1644 = 3UL;
                    uint32_t *l_1647 = &l_1543;
                    int32_t l_1686 = (-1L);
                    int32_t l_1688 = (-1L);
                    int32_t l_1689 = 0xD629D012L;
                    int32_t l_1699 = 0L;
                    int32_t l_1700 = 0x06EF99E6L;
                    int32_t l_1701 = 0x063B76A1L;
                    int32_t l_1702 = 0x72259483L;
                    int32_t l_1703 = 1L;
                    int32_t l_1704[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1704[i] = 0L;
                    (*l_1327) = &l_1334;
                    if (((safe_mul_func_int16_t_s_s((((safe_div_func_uint32_t_u_u(0UL, (safe_lshift_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s(((((0x1946L > g_493) , g_196) && (l_1636[2] != (((g_135 < g_135) < ((*l_1647) = (safe_mod_func_uint16_t_u_u(((safe_div_func_int32_t_s_s((((safe_lshift_func_int16_t_s_u(((l_1644 | (safe_lshift_func_int8_t_s_u(((((*l_1561) = p_28) != 1L) , p_27), 1))) || p_28), p_27)) | l_1644) || 7UL), (-7L))) , p_27), 1UL)))) , (void*)0))) <= p_28), 0)), p_27)))) < (**g_531)) < (-1L)), p_27)) | p_27))
                    { /* block id: 798 */
                        const float l_1655 = 0x9.F9FD9Bp+34;
                        union U0 *l_1658[9][5] = {{&g_1662,&g_1659,&g_1659,&g_1662,&g_1659},{&g_1660,&g_1660,&g_1663,&g_1660,&g_1660},{&g_1659,&g_1662,&g_1659,&g_1659,&g_1662},{&g_1660,(void*)0,(void*)0,&g_1660,(void*)0},{&g_1662,&g_1662,&g_1661,&g_1662,&g_1662},{(void*)0,&g_1660,(void*)0,(void*)0,&g_1660},{&g_1662,&g_1659,&g_1659,&g_1662,&g_1659},{&g_1660,&g_1660,&g_1663,(void*)0,(void*)0},{&g_1661,&g_1659,&g_1661,&g_1661,&g_1659}};
                        int i, j;
                        (*l_1561) = ((((((l_1658[6][1] = ((((safe_div_func_int64_t_s_s(((l_1650[0][7] != (void*)0) && ((safe_sub_func_uint32_t_u_u(g_770[3], (p_28 > (((safe_sub_func_uint8_t_u_u(g_93, (0x45D6L > ((((g_493 , p_27) ^ ((g_202 & 0xA9L) ^ p_28)) < g_1584) || (*l_1561))))) > 1UL) && g_97)))) , p_27)), g_216)) , (*l_1561)) & g_196) , l_1656[2])) != (*g_825)) < p_27) != g_156) ^ p_28) , l_1540[5][2]);
                        if (p_28)
                            continue;
                    }
                    else
                    { /* block id: 802 */
                        int32_t *l_1664 = &l_1586;
                        int32_t *l_1665 = &l_1611;
                        int32_t *l_1666 = &g_97;
                        int32_t *l_1667 = (void*)0;
                        int32_t *l_1668 = &g_41[2][5][0];
                        int32_t *l_1669 = &g_156;
                        int32_t *l_1670 = &g_93;
                        int32_t *l_1671 = &g_41[2][2][1];
                        int32_t *l_1672 = &l_1338;
                        int32_t *l_1673 = (void*)0;
                        int32_t *l_1674 = &l_1605;
                        int32_t *l_1675 = &g_100;
                        int32_t *l_1676 = &l_1537;
                        int32_t *l_1677 = (void*)0;
                        int32_t *l_1678 = (void*)0;
                        int32_t *l_1679 = &g_156;
                        int32_t *l_1680 = &l_1608;
                        int32_t *l_1681 = &l_1607;
                        int32_t *l_1682 = &l_1603;
                        int32_t *l_1683[9][2] = {{&l_1603,&l_1338},{&l_1539,&l_1338},{&l_1603,&l_1338},{&l_1539,&l_1338},{&l_1603,&l_1338},{&l_1539,&l_1338},{&l_1603,&l_1338},{&l_1539,&l_1338},{&l_1603,&l_1338}};
                        int i, j;
                        l_1690++;
                    }
                    for (g_1662.f3 = 0; (g_1662.f3 <= 1); g_1662.f3 += 1)
                    { /* block id: 807 */
                        if (g_135)
                            goto lbl_1693;
                        if (l_1336)
                            goto lbl_1693;
                    }
                    for (g_1662.f3 = 0; (g_1662.f3 <= 1); g_1662.f3 += 1)
                    { /* block id: 813 */
                        int32_t *l_1695 = &l_1333[1];
                        int32_t *l_1696 = (void*)0;
                        int32_t *l_1697[9][9] = {{&l_1333[1],&l_1610,&g_100,&g_100,&l_1610,&l_1333[1],&l_1609[3],&l_1333[1],&l_1610},{&l_1333[1],&g_100,&g_100,&l_1333[1],&l_1540[0][5],&l_1610,&l_1540[0][5],&l_1333[1],&g_100},{&l_1540[0][5],&l_1540[0][5],&l_1609[3],&l_1610,&l_1338,&l_1610,&l_1609[3],&l_1540[0][5],&l_1540[0][5]},{&g_100,&l_1333[1],&l_1540[0][5],&l_1610,&l_1540[0][5],&l_1333[1],&g_100,&g_100,&l_1333[1]},{&l_1610,&l_1333[1],&l_1609[3],&l_1333[1],&l_1610,&g_100,&g_100,&l_1610,&l_1333[1]},{&g_100,&l_1540[0][5],&g_100,&g_100,&l_1609[3],&l_1609[3],&g_100,&g_100,&l_1540[0][5]},{&l_1540[0][5],&g_100,&g_100,&l_1609[3],&l_1609[3],&g_100,&g_100,&l_1540[0][5],&g_100},{&l_1333[1],&l_1610,&g_100,&g_100,&l_1610,&l_1333[1],&l_1609[3],&l_1333[1],&l_1610},{&l_1333[1],&g_100,&g_100,&l_1333[1],&l_1540[0][5],&l_1610,&l_1540[0][5],&l_1333[1],&g_100}};
                        int i, j;
                        (*l_1328) = l_1694;
                        --l_1705;
                    }
                }
            }
        }
        for (l_1537 = 0; (l_1537 >= 0); l_1537 -= 1)
        { /* block id: 928 */
            int32_t l_1956 = 2L;
            int32_t l_1970[10] = {0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L,0x99974AE4L};
            uint32_t * const ** const l_1992 = &l_1873[3];
            uint32_t * const ** const *l_1991 = &l_1992;
            float l_2021 = 0xC.F3CD07p-28;
            int64_t l_2091 = 0xC5A8F4AD7B252C96LL;
            const int32_t **l_2100 = (void*)0;
            int64_t l_2119 = 0x17245C2F1FCC1031LL;
            uint32_t l_2140 = 0x7132C267L;
            int32_t *l_2157 = (void*)0;
            int64_t **l_2171 = &g_1726[4][1][0];
            int32_t l_2203[2];
            int i;
            for (i = 0; i < 2; i++)
                l_2203[i] = 0x6ABDE1DBL;
            if (((((((p_27 , ((safe_div_func_float_f_f(((0x8.82D5CBp-12 <= 0xB.8CD130p+78) != ((l_1956 = (safe_add_func_float_f_f(p_28, (*l_1802)))) > (safe_div_func_float_f_f(((*g_224) = ((**g_1246) <= (safe_sub_func_float_f_f((safe_add_func_float_f_f(((safe_add_func_float_f_f((-0x4.9p+1), (p_27 , (-(safe_add_func_float_f_f((((((safe_mul_func_uint16_t_u_u(0UL, p_27)) > p_28) >= g_1560) >= (*l_1802)) , (-0x2.Ep-1)), p_28)))))) <= 0x6.FA9243p-55), 0xD.5792F7p-8)), p_27)))), p_27)))), p_28)) , l_1956)) & 1UL) != l_1970[6]) & g_41[4][3][2]) , g_671) != g_671))
            { /* block id: 931 */
                int16_t l_1976[9][8][3] = {{{0xC182L,0xBABEL,(-1L)},{0x8759L,0L,0x0ED9L},{0x67E1L,0x41C3L,0xFF32L},{(-1L),0x8759L,0x0ED9L},{1L,2L,(-1L)},{0L,0L,0xD745L},{(-1L),(-10L),9L},{0x9760L,0L,(-1L)}},{{0L,0x50EAL,0xA07CL},{0xBABEL,(-1L),1L},{(-2L),0x21F4L,(-1L)},{0x05B2L,0xF10FL,0xBCC3L},{0x8D98L,0L,(-7L)},{0x17CBL,(-1L),0x515BL},{1L,0xD16CL,0x0826L},{0x82D7L,4L,(-1L)}},{{0x700DL,0x0ED9L,0L},{0xB04FL,0xE044L,0xEFBAL},{0x0826L,(-2L),0x57A4L},{0x7C44L,(-2L),1L},{0L,0xE044L,0x5CD2L},{0xF10FL,0x0ED9L,0xE53FL},{0x515BL,4L,0x6060L},{0xD76BL,0xD16CL,0xF3D5L}},{{0x41C3L,(-1L),0xD76BL},{1L,0x2947L,0xE044L},{0xC6BEL,(-1L),0L},{1L,1L,0x8D98L},{0xF10FL,(-7L),0x0826L},{(-7L),0xC6DDL,(-7L)},{0xDA24L,0L,0xC6DDL},{1L,4L,1L}},{{2L,0x5CD2L,0x15FAL},{(-1L),0x82D7L,0x8759L},{(-7L),0xB152L,4L},{0L,0x58A2L,(-1L)},{(-7L),1L,0x24D9L},{(-1L),0xD76BL,0x7C44L},{2L,0L,(-1L)},{1L,0x15FAL,0x82D7L}},{{0xDA24L,0x700DL,(-1L)},{(-7L),1L,1L},{0xF10FL,0xD16CL,0x0ED9L},{1L,(-6L),0xB04FL},{0xC6BEL,9L,0xC32DL},{1L,0xC32DL,0xE3F3L},{0x58A2L,(-1L),0xBABEL},{1L,0xBCC3L,(-7L)}},{{(-5L),0x24D9L,0xB122L},{(-1L),1L,0x7369L},{0x5CD2L,0x21F4L,0x58A2L},{(-1L),0xC488L,0x58A2L},{0xFF32L,(-7L),0x7369L},{0xC7C8L,0x67E1L,0xB122L},{(-1L),0L,(-7L)},{0x7C44L,1L,0xBABEL}},{{7L,0x8D98L,0xE3F3L},{0x0ED9L,(-1L),0xC32DL},{0xBABEL,0xF3D5L,0xB04FL},{0x8D98L,0xA07CL,0x0ED9L},{0x16ECL,(-2L),1L},{0xD76BL,0x50EAL,(-1L)},{0x1FFCL,0xE044L,0x82D7L},{0L,(-1L),(-1L)}},{{0x21F4L,(-1L),0x7C44L},{(-8L),0x41C3L,0x24D9L},{0xA226L,0xE53FL,(-1L)},{0xE3F3L,1L,4L},{0x515BL,0xE53FL,0x8759L},{0xB152L,0x41C3L,0x15FAL},{(-6L),(-1L),1L},{0L,(-1L),0xC6DDL}}};
                float l_1998 = 0x1.1994F4p-83;
                uint8_t l_1999 = 0xD1L;
                uint32_t * const **l_2000 = &g_671;
                uint32_t * const l_2003 = &g_2004[0];
                uint32_t * const *l_2002[3];
                uint32_t * const **l_2001 = &l_2002[1];
                int32_t l_2020[4][7][6] = {{{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)}},{{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL}},{{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)}},{{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL},{0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL,(-1L)},{0x886F4E8AL,0x886F4E8AL,(-1L),(-1L),0x886F4E8AL,0x886F4E8AL}}};
                int32_t ** const *l_2065[4][10] = {{&g_227,&l_1328,&g_227,&g_227,&l_1328,&g_227,&g_227,&l_1328,&g_227,&g_227},{&l_1328,&l_1328,(void*)0,&l_1328,&l_1328,(void*)0,&l_1328,&l_1328,(void*)0,&l_1328},{&l_1328,&g_227,&g_227,&l_1328,&g_227,&g_227,&l_1328,&g_227,&g_227,&l_1328},{&g_227,&l_1328,&g_227,&g_227,&l_1328,&g_227,&g_227,&l_1328,&g_227,&g_227}};
                int32_t l_2068 = (-8L);
                int32_t *l_2072[7] = {&l_1687[8][5][1],&l_1687[8][5][1],&g_97,&l_1687[8][5][1],&l_1687[8][5][1],&g_97,&l_1687[8][5][1]};
                float l_2076 = 0x1.1p+1;
                int16_t l_2083 = 0L;
                int32_t ***l_2135 = (void*)0;
                uint16_t l_2154[9][2] = {{1UL,0xFC6CL},{1UL,0UL},{0xAE63L,1UL},{0UL,0xFC6CL},{0UL,0UL},{0xAE63L,0UL},{0UL,0xFC6CL},{0UL,1UL},{0xAE63L,0UL}};
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_2002[i] = &l_2003;
                if ((((*l_2001) = ((*l_2000) = ((0xED26BC131BF1ACD2LL != (((safe_sub_func_uint64_t_u_u(((((*l_1557) = 0xF8060A3F3DBF5873LL) | (safe_lshift_func_uint16_t_u_s((l_1970[6] == 6UL), ((safe_unary_minus_func_int64_t_s(((*l_1448) = l_1976[7][3][1]))) , (((safe_div_func_int8_t_s_s((safe_sub_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((p_27 <= p_28), (((safe_rshift_func_int8_t_s_u(((*l_1802) = ((safe_rshift_func_int16_t_s_u(((safe_mod_func_uint64_t_u_u((safe_div_func_int32_t_s_s((l_1991 != l_1993), 0xCCD40A5BL)), l_1956)) & p_27), l_1970[8])) || l_1997)), g_135)) != 4294967295UL) == p_28))), l_1956)), 0xB9L)) , 0xE1L) >= g_156))))) | 1L), p_27)) == l_1999) , l_1956)) , (void*)0))) != (*l_1905)))
                { /* block id: 937 */
                    int32_t *l_2006 = (void*)0;
                    int32_t *l_2007 = &l_1539;
                    int32_t *l_2008 = &g_41[4][3][2];
                    int32_t *l_2009 = &l_1687[2][9][0];
                    int32_t *l_2010 = &l_1538;
                    int32_t *l_2011 = &l_1540[9][5];
                    int32_t *l_2012 = &l_1956;
                    int32_t l_2013 = 9L;
                    int32_t *l_2014 = (void*)0;
                    int32_t *l_2015 = &l_1542;
                    int32_t *l_2016 = &g_92[3][0];
                    int32_t *l_2017 = &l_1970[0];
                    int32_t *l_2018[4] = {&g_561,&g_561,&g_561,&g_561};
                    uint32_t l_2022 = 0x7BEB0DE0L;
                    union U0 ***l_2034 = &l_1781;
                    const uint32_t *l_2045 = &g_2004[9];
                    const uint32_t **l_2046 = &l_2045;
                    float ** const l_2057 = &l_1519;
                    const int32_t **l_2067 = &g_99[1];
                    int i;
                    l_2022--;
                    if ((((*l_1519) = (safe_div_func_float_f_f((-(((p_28 <= (safe_mod_func_int8_t_s_s((((safe_sub_func_int16_t_s_s(((safe_sub_func_uint8_t_u_u((l_2034 == l_2034), (18446744073709551615UL != (safe_mul_func_int16_t_s_s((safe_div_func_int16_t_s_s(p_28, ((((safe_div_func_uint32_t_u_u((*l_2007), 1UL)) , ((safe_rshift_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(0x91B69B73L, (((*l_2046) = l_2045) == (void*)0))), 12)) & l_1956)) | p_27) && 1L))), 0x0164L))))) & p_28), 1UL)) , (-1L)) > 0xFC0C377CECE94762LL), g_123[7]))) != g_2047) , 0x5.900A2Ep+98)), 0x2.0p+1))) , l_1970[6]))
                    { /* block id: 941 */
                        int32_t ***l_2059 = (void*)0;
                        int32_t ****l_2058[7][3];
                        uint16_t l_2066 = 0xD856L;
                        int i, j;
                        for (i = 0; i < 7; i++)
                        {
                            for (j = 0; j < 3; j++)
                                l_2058[i][j] = &l_2059;
                        }
                        (*l_2067) = func_37(&l_1539, ((((safe_mod_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((safe_add_func_int8_t_s_s(((p_28 ^ ((((((65528UL && ((0x8D0D4CA4L || ((((~g_92[2][0]) , g_1243[(l_1537 + 5)]) != l_2057) <= ((l_2060 = &g_227) != ((safe_lshift_func_int16_t_s_s(((safe_sub_func_int16_t_s_s(g_123[3], 0UL)) || (*l_1802)), 12)) , l_2065[2][4])))) < l_2066)) & 0xB6L) < g_493) < p_28) < l_1970[6]) && p_28)) | 0x5C2C821E18A32389LL), 0x99L)) & 1L), p_27)), 0x6697L)) != 0xF854L) ^ 0x50L) , l_2067));
                        ++l_2069;
                        if (l_1970[6])
                            break;
                        if ((*l_2016))
                            break;
                    }
                    else
                    { /* block id: 947 */
                        int16_t l_2073 = 0xC25DL;
                        (*l_1327) = l_2072[2];
                        if (l_2073)
                            break;
                    }
                }
                else
                { /* block id: 951 */
                    const union U0 **l_2074 = (void*)0;
                    int32_t l_2075 = 0x5C019D2EL;
                    int32_t l_2078 = 0L;
                    int8_t l_2080 = (-2L);
                    int32_t l_2087 = 0L;
                    int32_t l_2088 = 0xC925BA6DL;
                    int32_t l_2090 = 0xD4CC7427L;
                    int32_t l_2092 = 0L;
                    int32_t l_2093 = 1L;
                    int32_t l_2094 = (-10L);
                    const int32_t **l_2102 = (void*)0;
                    int32_t ***l_2116 = &g_227;
                    if ((l_2074 != &g_826))
                    { /* block id: 952 */
                        int8_t l_2077 = 0x29L;
                        int32_t l_2079 = 0x99A12F94L;
                        int32_t l_2081 = 0x3730F6F1L;
                        int32_t l_2082 = (-1L);
                        int32_t l_2084 = (-1L);
                        int32_t l_2085 = 0x420B4178L;
                        int32_t l_2086[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_2086[i] = 2L;
                        g_2095++;
                        if (p_27)
                            continue;
                        (*l_1328) = &l_2079;
                    }
                    else
                    { /* block id: 956 */
                        const int32_t ***l_2101[2];
                        int32_t ****l_2117 = &l_2060;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2101[i] = (void*)0;
                        g_2098 = g_2098;
                        (*l_1327) = func_37(&g_92[3][0], (l_2102 = l_2100));
                        l_1687[3][9][0] ^= ((*l_1802) = (((safe_rshift_func_uint16_t_u_u(0x196DL, (safe_rshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((g_1876 >= ((((~(safe_sub_func_int64_t_s_s((((safe_mul_func_int8_t_s_s((((((*l_1872) = p_28) >= ((*g_1166) = (&l_2100 == ((*l_2117) = ((safe_lshift_func_uint16_t_u_u(((p_27 > p_27) != 0xE6E6652CL), (l_2090 = ((*l_1551) = ((*l_1472) |= (((((-6L) <= p_28) , p_27) , &l_1383) == (void*)0)))))) , l_2116))))) & p_28) ^ p_27), 0x20L)) , l_2118) >= p_27), p_27))) == 0x52835357F451D52ALL) == p_27) < p_27)), 13)), 12)))) | p_27) , p_28));
                        if (p_28)
                            continue;
                    }
                    for (g_1659.f2 = 0; (g_1659.f2 <= 1); g_1659.f2 += 1)
                    { /* block id: 972 */
                        return p_27;
                    }
                }
                for (g_638 = 0; (g_638 <= 1); g_638 += 1)
                { /* block id: 978 */
                    int32_t ****l_2134 = &l_2060;
                    int32_t ****l_2136 = (void*)0;
                    int32_t ****l_2137 = &l_2135;
                    int16_t *l_2138 = (void*)0;
                    int16_t *l_2139 = &g_1212;
                    int i, j;
                    if (((l_2119 && (g_92[g_638][l_1537] = p_28)) , (safe_lshift_func_int8_t_s_s((safe_div_func_uint32_t_u_u(((safe_mul_func_uint16_t_u_u(((((safe_add_func_uint8_t_u_u(((((g_2004[1] >= (safe_div_func_int64_t_s_s(p_27, (safe_rshift_func_uint16_t_u_s((((safe_div_func_uint64_t_u_u(((*l_1802) &= (((*l_2139) = (p_28 > (((((p_28 , ((*l_2134) = &l_1328)) == ((*l_2137) = l_2135)) & (p_27 , g_2089)) , (*g_40)) & (*g_1166)))) , 0x0F55F5DDA3018177LL)), 0x7253245E34605DD8LL)) , l_2140) > p_27), g_1811))))) , 2L) & p_28) , 0xB7L), g_126[6])) <= g_938) < g_1753[0]) & p_27), p_27)) | p_28), g_1811)), g_1945[2]))))
                    { /* block id: 984 */
                        int i, j;
                        g_92[(l_1537 + 2)][l_1537] |= (safe_rshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_s(l_2145, g_2089)), (safe_sub_func_int32_t_s_s(p_28, ((safe_mod_func_uint32_t_u_u((((safe_lshift_func_int8_t_s_u(p_28, g_1945[0])) == (safe_mod_func_uint16_t_u_u(l_2154[8][0], ((*l_1472) &= ((-1L) < g_93))))) <= p_28), 4294967286UL)) >= (*g_532))))));
                        if (p_28)
                            continue;
                    }
                    else
                    { /* block id: 988 */
                        uint8_t l_2155 = 255UL;
                        if (l_2155)
                            break;
                        l_2157 = func_37(func_37(l_2156, &g_99[1]), l_2100);
                    }
                    (*l_1328) = &l_1970[6];
                    if ((*g_40))
                        continue;
                    (*g_40) ^= (((&l_1994 != (void*)0) <= (!((safe_sub_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s(5L, ((*l_1557) = (safe_lshift_func_uint8_t_u_s((((g_126[3] & (((p_27 , p_27) == 0x844C383CL) , (safe_mul_func_int8_t_s_s((p_28 < 0x4CD9654EL), p_27)))) != g_1584) | 0xDDA3L), 0))))) == p_27), 0x25L)) & p_27))) < p_27);
                    for (l_1543 = 0; (l_1543 <= 1); l_1543 += 1)
                    { /* block id: 998 */
                        uint16_t l_2168[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_2168[i] = 0x8D2CL;
                        ++l_2168[1];
                        (*g_40) |= 0x1A0996A0L;
                    }
                }
                if (p_28)
                    break;
            }
            else
            { /* block id: 1004 */
                int32_t **l_2176 = &g_228;
                int32_t l_2183 = (-5L);
                int32_t l_2189 = 0x462BFC98L;
                int32_t l_2198 = (-1L);
                int32_t l_2199 = 0xBA19E5FBL;
                int32_t l_2201[4][10] = {{2L,1L,2L,1L,2L,1L,2L,1L,2L,1L},{0xCBB49EDBL,1L,0xCBB49EDBL,1L,0xCBB49EDBL,1L,0xCBB49EDBL,1L,0xCBB49EDBL,1L},{2L,1L,2L,1L,2L,1L,2L,1L,2L,1L},{0xCBB49EDBL,1L,0xCBB49EDBL,1L,0xCBB49EDBL,1L,0xCBB49EDBL,1L,0xCBB49EDBL,1L}};
                int64_t l_2202 = 0x6E363805CA1C995BLL;
                uint32_t ***l_2292 = &l_2273;
                int i, j;
                if (p_28)
                { /* block id: 1005 */
                    int64_t l_2187 = 0xA86ABA2F2E681A9BLL;
                    int32_t l_2194 = (-2L);
                    int32_t l_2195 = 0x2C3E9071L;
                    int32_t l_2196 = 0L;
                    int32_t l_2197[3][5];
                    int8_t l_2200 = 1L;
                    uint16_t l_2205 = 65528UL;
                    const uint16_t *l_2294[5][6][4] = {{{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118}},{{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118}},{{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118}},{{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118}},{{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118},{(void*)0,&l_2118,(void*)0,&l_2118}}};
                    const uint16_t **l_2293[1];
                    int32_t l_2295 = (-1L);
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_2197[i][j] = 4L;
                    }
                    for (i = 0; i < 1; i++)
                        l_2293[i] = &l_2294[0][5][2];
                    for (l_1483 = 0; (l_1483 <= 0); l_1483 += 1)
                    { /* block id: 1008 */
                        (*l_1802) = ((void*)0 == l_2171);
                        if (p_27)
                            continue;
                        (*l_1787) = l_2172;
                    }
                    if ((safe_lshift_func_int8_t_s_u(((*l_1802) || ((*l_1802) && (&l_1581 == (void*)0))), ((8L >= (((g_770[3] = ((void*)0 == l_2176)) , (~(safe_mul_func_int8_t_s_s(((safe_lshift_func_uint8_t_u_s(0UL, (+g_1769[4]))) ^ p_28), 0x38L)))) & g_200)) != p_28))))
                    { /* block id: 1014 */
                        int32_t *l_2184 = &l_1540[2][1];
                        int32_t *l_2185 = &l_1687[3][9][0];
                        int32_t l_2186 = (-1L);
                        int32_t *l_2188 = (void*)0;
                        int32_t *l_2190 = &l_1333[0];
                        int32_t *l_2191 = (void*)0;
                        int32_t *l_2192 = &l_1956;
                        int32_t *l_2193[3];
                        int32_t l_2204 = 2L;
                        uint32_t ***l_2227 = &l_1581;
                        int64_t l_2228 = 0L;
                        int16_t *l_2229 = &l_1541[1];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_2193[i] = &l_2189;
                        (*l_1802) ^= 0x514738E2L;
                        ++l_2205;
                        (*g_40) = ((safe_add_func_uint32_t_u_u(((0UL || ((((safe_mod_func_uint64_t_u_u((safe_div_func_int64_t_s_s(((*l_1802) = (safe_mod_func_uint8_t_u_u(((void*)0 == &g_1584), ((((p_28 != (safe_div_func_uint16_t_u_u((*l_2192), ((safe_div_func_int64_t_s_s(p_28, p_28)) | (!((safe_sub_func_uint64_t_u_u((safe_div_func_int16_t_s_s((((((0x96L <= (((*l_1472) |= (safe_rshift_func_int8_t_s_u((((g_1877 && p_27) != 5L) >= (-1L)), 6))) >= p_28)) , &l_1581) == l_2227) , 0xC59CL) < 0x82D6L), p_27)), p_27)) && 0xC0L)))))) , 4294967287UL) | p_27) & p_28)))), 0xF49760B95517ECD9LL)), l_2228)) , (void*)0) == l_2229) , 0L)) == 65535UL), l_2195)) == p_27);
                    }
                    else
                    { /* block id: 1020 */
                        int16_t ***l_2231 = &l_2230;
                        (*l_2231) = l_2230;
                        (*g_224) = p_27;
                        (*l_1802) = ((g_177 != p_27) , (~(l_2201[2][9] |= ((*l_1557) = (p_27 < (*l_2156))))));
                        (*g_1166) = 0L;
                    }
                    for (g_156 = 0; (g_156 >= 0); g_156 -= 1)
                    { /* block id: 1030 */
                        uint32_t l_2241 = 0x01001B9EL;
                        uint8_t *l_2246 = &l_2069;
                        int16_t *l_2281 = &l_1597;
                        uint8_t *l_2284 = &g_1854;
                        int16_t *l_2287 = &g_1945[2];
                        int i, j;
                        (*g_1166) ^= (((safe_mul_func_uint8_t_u_u(p_28, ((g_123[4] = (*l_1802)) >= (safe_add_func_uint64_t_u_u(p_28, l_2197[1][1]))))) , 0x0F885C304239EF30LL) == (((*l_2246) ^= ((safe_add_func_int32_t_s_s(0x29DB5C07L, (((safe_add_func_uint64_t_u_u(l_2241, (((l_2195 = ((g_92[(g_156 + 1)][l_1537] = ((safe_sub_func_uint16_t_u_u(((*l_1473) = (safe_unary_minus_func_uint16_t_u(l_2245))), g_1854)) ^ 0x9831B067L)) < 0L)) , (void*)0) != (void*)0))) && g_1854) || g_92[(g_156 + 1)][l_1537]))) , 2UL)) > g_1876));
                        (*g_1166) = ((safe_sub_func_int32_t_s_s((safe_sub_func_int64_t_s_s((((*l_1802) = l_2202) > p_27), ((safe_mod_func_uint8_t_u_u(((*l_2246)++), 1UL)) < g_493))), (safe_lshift_func_uint8_t_u_s(((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u(65530UL, (safe_add_func_uint16_t_u_u(p_27, (p_28 >= (((*l_1473)--) < (0x6147B0E61FE24285LL < p_27))))))) | p_28), l_2197[0][1])) >= g_92[(g_156 + 1)][l_1537]), l_2196)))) , 3L);
                        if (p_28)
                            continue;
                        (*g_40) = (l_2194 |= (safe_mod_func_uint32_t_u_u((p_27 & (safe_add_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u(p_27, (((**l_1993) = l_2273) == &l_2274))) != g_41[4][3][2]), ((((*l_2246) = p_28) ^ ((safe_lshift_func_uint16_t_u_s((*l_1802), (safe_mod_func_int32_t_s_s((safe_div_func_int32_t_s_s(((((*l_2281) = p_28) != (((*l_2287) = (((safe_lshift_func_uint8_t_u_s((++(*l_2284)), (((0L <= 0x8EEF4AF9L) || (-3L)) & p_28))) != 0x5CL) <= (*l_1802))) != 0xA2A9L)) != (*g_532)), p_28)), p_27)))) , 7L)) , g_2095)))), p_28)));
                    }
                    (*g_1166) = (((safe_mod_func_uint32_t_u_u(4294967295UL, (*g_1166))) | ((++(*l_1557)) >= (-1L))) | (((((0x615CL < ((void*)0 != l_2292)) , (l_2293[0] != (((p_28 >= (p_28 >= l_2295)) > 0xA1L) , &l_1472))) | l_2295) , (void*)0) != &l_2172));
                }
                else
                { /* block id: 1052 */
                    return (**g_1246);
                }
            }
        }
    }
    else
    { /* block id: 1057 */
        int32_t *l_2296 = &g_100;
        int32_t l_2298 = 0x9FE280CDL;
        int32_t l_2302 = 0xA6DD1231L;
        int32_t l_2303 = 0x3681C16DL;
        int32_t l_2304 = (-7L);
        int32_t l_2305 = (-6L);
        int32_t l_2306 = (-1L);
        int32_t l_2307 = 1L;
        int32_t l_2308 = 0x24EC8674L;
        int32_t l_2309 = 0xFE181E67L;
        int32_t l_2310[8] = {0L,0L,0L,0L,0L,0L,0L,0L};
        uint32_t l_2311[7] = {18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL};
        const uint16_t *l_2330 = (void*)0;
        const uint16_t **l_2329 = &l_2330;
        uint32_t l_2337 = 7UL;
        const int64_t *l_2360 = (void*)0;
        int i;
        if (((void*)0 == (*l_2060)))
        { /* block id: 1058 */
            float l_2297 = 0x4.A4EC99p+51;
            int32_t l_2299 = 0x53911B73L;
            int32_t *l_2301[2][1][5] = {{{&l_1333[1],&l_2298,&l_2299,&l_2298,&l_1333[1]}},{{&l_1333[1],&l_2298,&l_2299,&l_2298,&l_1333[1]}}};
            int i, j, k;
            (*l_1328) = l_2296;
            --l_2311[0];
        }
        else
        { /* block id: 1061 */
            int32_t *l_2314[6][9][4] = {{{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]}},{{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]}},{{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]}},{{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]}},{{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]}},{{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]},{&l_1333[3],&l_1333[3],&l_1333[3],&l_1333[3]}}};
            int16_t ***l_2346 = (void*)0;
            int i, j, k;
            (**l_2060) = l_2314[2][6][1];
            (*l_2156) = (safe_div_func_float_f_f((safe_add_func_float_f_f((((safe_add_func_float_f_f(((((*g_224) = (safe_div_func_float_f_f(((safe_add_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u(0xCFL, 7)), (0L & ((*l_2296) = (safe_rshift_func_int8_t_s_u((((void*)0 != l_2329) <= ((safe_unary_minus_func_uint8_t_u(((*l_2156) && ((safe_sub_func_uint32_t_u_u(p_27, (g_1584 != p_27))) == (safe_unary_minus_func_uint32_t_u((p_27 > (*l_2296)))))))) < 0xB522290BL)), 6)))))) , p_27), p_28))) <= p_27) < p_28), 0x1.2p+1)) < p_27) != 0x1.5BE1C9p-42), p_28)), l_2337));
            (*g_40) ^= (((((l_2304 = (safe_add_func_float_f_f((safe_mul_func_float_f_f((((safe_mul_func_float_f_f(0xC.BA161Ep+22, (g_2344[1] != l_2346))) <= p_28) > (safe_sub_func_float_f_f((safe_div_func_float_f_f(((*g_224) = ((*l_2296) = (((safe_unary_minus_func_uint64_t_u(((void*)0 != l_2314[2][6][1]))) && ((((safe_sub_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_s(0x32CFL, ((safe_mul_func_int8_t_s_s(((safe_rshift_func_int16_t_s_s((((l_2360 == (void*)0) == 0L) | g_126[2]), 4)) == p_28), 0L)) | p_27))) | (**g_531)), 0x08L)) >= 0L) ^ 4UL) == 0L)) , p_27))), 0x1.Dp+1)), p_28))), p_27)), (-0x6.5p+1)))) > (-0x1.Cp+1)) != l_2361) >= g_2362) , 1L);
        }
        (*g_1386) = (*g_1386);
    }
    for (g_1662.f1 = 0; (g_1662.f1 != (-12)); g_1662.f1 = safe_sub_func_uint64_t_u_u(g_1662.f1, 4))
    { /* block id: 1075 */
        uint64_t l_2365 = 0xF32D104460A15022LL;
        l_2365++;
    }
    if (p_27)
    { /* block id: 1078 */
        int16_t l_2380 = 0xB869L;
        int32_t *l_2382 = &l_1333[1];
        for (g_1661.f3 = 0; (g_1661.f3 <= 9); g_1661.f3 = safe_add_func_int16_t_s_s(g_1661.f3, 6))
        { /* block id: 1081 */
            uint8_t l_2370 = 2UL;
            int32_t *l_2381 = (void*)0;
            l_2370--;
            (*l_2156) ^= 0x3286DA74L;
            (*l_2156) ^= ((g_41[1][1][3] , ((safe_sub_func_int32_t_s_s((((((p_27 && p_27) && (safe_sub_func_int32_t_s_s(l_2377, (6L | (18446744073709551613UL <= ((safe_lshift_func_int16_t_s_u((((l_2380 >= ((0x0.7p+1 < (((0x6.E42188p-22 == p_27) < p_28) > p_28)) > p_27)) > p_28) , (-7L)), g_1212)) >= 0UL)))))) , p_27) || p_27) | l_2380), 0x4CFC0B07L)) || p_27)) | 1UL);
            l_2382 = l_2381;
        }
        g_2383 = &l_1650[0][7];
    }
    else
    { /* block id: 1088 */
        uint32_t ***l_2389 = &l_2273;
        int32_t l_2405 = 7L;
        int64_t *l_2406[10] = {&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1,&g_1391.f1};
        int32_t l_2407 = 0L;
        float ****l_2412 = &g_1447;
        int32_t l_2431 = (-1L);
        int32_t l_2432 = (-8L);
        int32_t l_2433 = 1L;
        int32_t l_2434[6] = {(-8L),0L,(-8L),(-8L),0L,(-8L)};
        uint16_t l_2540 = 0xCD74L;
        float l_2564 = (-0x5.5p+1);
        uint64_t ***l_2575 = &g_533[2][0][1];
        uint64_t ****l_2574 = &l_2575;
        int32_t l_2650 = (-7L);
        uint8_t l_2727 = 0xA1L;
        int32_t *l_2741 = &l_2434[4];
        int32_t l_2753 = 0x7F70DAA2L;
        int64_t l_2754 = 0x01D0532EB4C5C12CLL;
        int8_t l_2763 = 0x69L;
        const int32_t **l_2794 = (void*)0;
        float l_2831 = 0x0.Cp+1;
        int32_t l_2857 = (-5L);
        int16_t l_2863 = 9L;
        int64_t l_2904 = 0xB2268FB4842BBA1CLL;
        uint32_t l_2955 = 18446744073709551612UL;
        int64_t l_2956 = 4L;
        int i;
    }
    return p_28;
}


/* ------------------------------------------ */
/* 
 * reads : g_41 g_29 g_92 g_93 g_98 g_97 g_42 g_40 g_133 g_126 g_121 g_156 g_99 g_123 g_100 g_216 g_223 g_227 g_198 g_228 g_202 g_196 g_224 g_86 g_270 g_177 g_200 g_417 g_466 g_135 g_561 g_531 g_532 g_534 g_493 g_671 g_737 g_825 g_638 g_770 g_938 g_1166 g_1171.f2 g_1212 g_418
 * writes: g_86 g_92 g_93 g_97 g_42 g_100 g_41 g_121 g_135 g_156 g_123 g_177 g_196 g_126 g_198 g_200 g_202 g_216 g_228 g_133 g_561 g_534 g_353 g_99 g_227 g_223 g_770 g_533 g_737 g_638 g_827.f3 g_1171.f2
 */
static uint8_t  func_32(int32_t * p_33, int32_t ** p_34, int32_t  p_35, int32_t * p_36)
{ /* block id: 5 */
    int8_t l_60 = 4L;
    float *l_84 = (void*)0;
    float *l_85 = &g_86;
    int32_t l_89 = 4L;
    uint8_t l_90[9][8] = {{255UL,252UL,255UL,255UL,252UL,255UL,255UL,252UL},{252UL,255UL,255UL,252UL,255UL,255UL,252UL,255UL},{252UL,252UL,0UL,252UL,252UL,0UL,252UL,252UL},{255UL,252UL,255UL,255UL,252UL,255UL,255UL,252UL},{252UL,255UL,255UL,252UL,255UL,255UL,252UL,255UL},{252UL,252UL,0UL,252UL,252UL,0UL,252UL,252UL},{255UL,252UL,255UL,255UL,252UL,255UL,255UL,252UL},{252UL,255UL,255UL,252UL,255UL,255UL,252UL,255UL},{252UL,252UL,0UL,252UL,252UL,0UL,252UL,252UL}};
    int32_t **l_91 = &g_42;
    uint32_t l_253[5] = {0x82C875A0L,0x82C875A0L,0x82C875A0L,0x82C875A0L,0x82C875A0L};
    uint32_t l_560 = 0x775E8C8AL;
    int32_t * const l_616 = &g_100;
    int32_t l_628 = 0x11ADC630L;
    int32_t l_633 = 0L;
    int32_t l_639 = 0xA1025375L;
    int32_t l_640 = 3L;
    int32_t l_643[3];
    uint32_t *l_670 = &g_216;
    uint32_t **l_669 = &l_670;
    int32_t l_682 = (-2L);
    uint8_t l_694 = 0xEEL;
    int16_t l_731 = 1L;
    const int32_t **l_861 = (void*)0;
    float *** const l_962 = &g_223;
    int32_t * const l_983 = &l_639;
    uint16_t *l_996 = &g_200;
    uint32_t l_1009 = 0x447ED113L;
    int32_t l_1010 = 0xE0434E4CL;
    int8_t *l_1019 = &g_202;
    float l_1024 = 0xF.D83286p+28;
    int16_t l_1072 = (-6L);
    const uint32_t l_1106 = 0xF22608B3L;
    uint64_t *l_1129 = (void*)0;
    uint32_t l_1165 = 0xBF60EE8EL;
    union U0 **l_1230 = (void*)0;
    int64_t *l_1281 = &g_135;
    int64_t **l_1280 = &l_1281;
    uint8_t l_1300 = 0xD1L;
    int i, j;
    for (i = 0; i < 3; i++)
        l_643[i] = 0L;
    if ((g_561 &= ((safe_add_func_uint32_t_u_u((((func_50((g_41[4][3][2] , ((safe_rshift_func_uint16_t_u_u(((func_55(((((l_60 > (~(func_62(((l_60 <= (safe_rshift_func_uint16_t_u_s((safe_add_func_uint8_t_u_u((!((safe_div_func_float_f_f((safe_add_func_float_f_f((l_89 = (safe_sub_func_float_f_f((g_41[6][3][1] < ((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((p_35 > ((safe_add_func_float_f_f((p_35 < (p_35 == (safe_div_func_float_f_f((((*l_85) = 0x0.Dp-1) > (safe_mul_func_float_f_f(g_29, p_35))), p_35)))), 0xF.3F1863p-29)) >= g_41[0][5][0])) >= 0x4.A86307p-0), p_35)), p_35)) , g_41[7][3][1])), l_60))), l_60)), l_60)) , 0xCC9DF78A42286E0DLL)), l_60)), p_35))) <= l_90[7][3]), l_91) >= p_35))) , (**l_91)) , p_33) == (void*)0), p_35, g_227, l_60) != 2L) , 0xCB4CL), p_35)) <= 0UL)), l_253[0]) == p_35) , g_200) & l_560), l_60)) | 0xBAL)))
    { /* block id: 227 */
        const union U0 *l_564 = &g_565;
        int32_t l_592[10] = {(-1L),0xD85B4F84L,1L,1L,0xD85B4F84L,(-1L),0xD85B4F84L,1L,1L,0xD85B4F84L};
        int32_t l_644 = (-3L);
        uint32_t l_645 = 0x47402522L;
        int16_t l_654 = (-9L);
        float *l_689 = &g_353;
        uint16_t *l_690 = (void*)0;
        uint16_t *l_691 = (void*)0;
        uint16_t *l_692 = &g_200;
        uint8_t *l_721 = (void*)0;
        uint32_t *l_793 = &l_253[0];
        uint32_t **l_792 = &l_793;
        uint32_t ***l_791 = &l_792;
        int16_t l_850 = 0x3DEBL;
        uint32_t ***l_935[7];
        int8_t l_961 = 7L;
        int32_t l_963 = 0x0474F1A0L;
        int i;
        for (i = 0; i < 7; i++)
            l_935[i] = &l_669;
        for (l_560 = 0; (l_560 <= 2); l_560 += 1)
        { /* block id: 230 */
            uint64_t l_562 = 7UL;
            uint64_t **l_563 = &g_534;
            uint32_t l_591[1][6] = {{1UL,4UL,1UL,1UL,4UL,1UL}};
            int32_t l_623 = (-10L);
            int32_t l_627 = 0L;
            int32_t l_629 = (-6L);
            int32_t l_632 = 0x5C0A1A08L;
            int32_t l_635 = (-7L);
            int32_t l_636[8] = {0x4CC4A5C9L,(-5L),0x4CC4A5C9L,0x4CC4A5C9L,(-5L),0x4CC4A5C9L,0x4CC4A5C9L,(-5L)};
            int32_t l_642 = 0L;
            uint32_t l_655 = 6UL;
            int32_t l_663 = 0xC2B1FD03L;
            int i, j;
            if ((*g_42))
            { /* block id: 231 */
                int16_t l_594 = (-1L);
                const int32_t **l_617 = &g_99[0];
                int32_t l_624 = (-1L);
                int32_t l_626 = (-6L);
                int32_t l_631 = 0xD7E5718DL;
                int32_t l_641[9][6][4] = {{{0xA9371976L,0x2C2E80D8L,0xC7E0C254L,0L},{1L,0x2583626CL,1L,0x2F30BECCL},{0x8F0C790FL,0x81D2273CL,0x4A549D66L,(-1L)},{0xAC4C70C9L,1L,0L,0x81D2273CL},{(-1L),0x751765F9L,0L,0L},{0xAC4C70C9L,0x59C30125L,0x4A549D66L,0x8918C9AFL}},{{0x8F0C790FL,0xBC11E2F0L,1L,(-1L)},{1L,(-1L),0xC7E0C254L,(-1L)},{0xA9371976L,0x2CDEC9C5L,0x1F1E1EB5L,0xC7E0C254L},{1L,0xA1820F47L,0x53F35C37L,0xA4D26B60L},{1L,0x5F710F09L,0x2F30BECCL,5L},{4L,0xBC11E2F0L,(-1L),0x4A549D66L}},{{0xD44976E0L,0xEABCC89EL,0x497E44A6L,2L},{(-1L),0x497E44A6L,0x2518EAECL,0x81D2273CL},{0x4A549D66L,0L,0x674CB722L,8L},{1L,0x2535B0AAL,0x8F0C790FL,0xDD250ABBL},{1L,0x0A31D7DDL,0L,0xABEC5AC5L},{5L,1L,(-1L),0L}},{{0L,0x70BB11E1L,(-6L),0x02E80430L},{0xA3401194L,0xA9FC7590L,0xA1820F47L,0xE4F3BCB9L},{(-10L),0x497E44A6L,4L,0xDD250ABBL},{0x0707D300L,0xA4B94901L,0xFF69B6EEL,1L},{0L,4L,0xABEC5AC5L,0x1F1E1EB5L},{0x81D2273CL,0x3B0D9BCCL,0xE4F3BCB9L,0x70BB11E1L}},{{0x02E80430L,0x816F2E11L,(-1L),(-1L)},{1L,1L,4L,(-7L)},{0L,0x02F5D07CL,0x2518EAECL,0xA3401194L},{0x5D36813AL,0x6BB6B5DEL,0L,0x2518EAECL},{0L,0x6BB6B5DEL,0x2583626CL,0xA3401194L},{0x6BB6B5DEL,0x02F5D07CL,0xDD250ABBL,(-7L)}},{{0xF9166503L,1L,0x8F0C790FL,(-1L)},{0L,0x816F2E11L,(-9L),0x70BB11E1L},{0xFF69B6EEL,0x3B0D9BCCL,0x6BB6B5DEL,0x1F1E1EB5L},{0x2F30BECCL,4L,0x674CB722L,1L},{1L,0xA4B94901L,0xBC11E2F0L,0xDD250ABBL},{0xF9166503L,0x497E44A6L,1L,0xE4F3BCB9L}},{{5L,0xA9FC7590L,0xA4D26B60L,0x02E80430L},{1L,0x70BB11E1L,0L,0L},{0x497E44A6L,1L,0xA1820F47L,0xABEC5AC5L},{4L,0x0A31D7DDL,0xEA08BB6EL,0xDD250ABBL},{1L,0x2535B0AAL,0xD44976E0L,8L},{0L,0L,0x8918C9AFL,0x8F0C790FL}},{{1L,0x3B0D9BCCL,0xABEC5AC5L,0x53F35C37L},{(-1L),2L,(-1L),0xD44976E0L},{0L,1L,0xEA08BB6EL,(-7L)},{(-10L),0x81D2273CL,(-7L),0x751765F9L},{0x5D36813AL,0x5F710F09L,0x0707D300L,(-7L)},{1L,0x6BB6B5DEL,(-1L),0x5D36813AL}},{{(-1L),(-1L),0xDD250ABBL,(-1L)},{(-1L),1L,0xBC11E2F0L,(-1L)},{1L,0x197443A7L,0x3B0D9BCCL,(-9L)},{0xFF69B6EEL,0xAC4C70C9L,5L,0x8F0C790FL},{0xFF69B6EEL,4L,0x3B0D9BCCL,0xB23E6CF3L},{1L,0x8F0C790FL,0xBC11E2F0L,1L}}};
                int32_t *l_648 = &l_640;
                int32_t *l_649 = &l_643[1];
                int32_t *l_650 = &l_633;
                int32_t *l_651 = &l_636[1];
                int32_t *l_652 = (void*)0;
                int32_t *l_653[9][4] = {{&l_643[2],&l_643[1],&l_643[1],&l_643[2]},{&l_643[1],&l_643[2],&l_643[1],&l_643[1]},{&l_643[2],&l_643[2],(void*)0,&l_643[2]},{&l_643[2],&l_643[1],&l_643[1],&l_643[2]},{&l_643[1],&l_643[2],&l_643[1],&l_643[1]},{&l_643[2],&l_643[2],(void*)0,&l_643[2]},{&l_643[2],&l_643[1],&l_643[1],&l_643[2]},{&l_643[1],&l_643[2],&l_643[1],(void*)0},{&l_643[1],&l_643[1],&l_643[2],&l_643[1]}};
                int i, j, k;
                (*p_33) = l_562;
                if (((void*)0 == l_563))
                { /* block id: 233 */
                    int32_t l_611 = (-3L);
                    uint32_t *l_614 = &l_591[0][2];
                    int32_t **l_618[5][1][2] = {{{&g_42,&g_42}},{{&g_42,&g_42}},{{&g_42,&g_42}},{{&g_42,&g_42}},{{&g_42,&g_42}}};
                    int32_t **l_619 = (void*)0;
                    int32_t **l_620 = &g_228;
                    int i, j, k;
                    for (g_561 = 0; (g_561 <= 2); g_561 += 1)
                    { /* block id: 236 */
                        const union U0 **l_566 = &l_564;
                        int32_t *l_593 = &g_93;
                        (*p_36) |= 0L;
                        (*g_224) = (**g_223);
                        (*l_566) = l_564;
                        (*l_593) |= ((safe_lshift_func_int8_t_s_u((+((safe_sub_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(g_198, ((g_561 != g_561) >= ((((safe_sub_func_uint8_t_u_u(g_270, (((safe_mod_func_int8_t_s_s((g_202 = (p_35 == (+(safe_mul_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(((*g_534) = (g_198 <= ((safe_add_func_int32_t_s_s((((safe_mod_func_uint8_t_u_u((((safe_mod_func_uint8_t_u_u(0x4AL, p_35)) , (((safe_mod_func_uint32_t_u_u(((l_562 , (**g_531)) & 0L), (*g_40))) || (**l_91)) != l_591[0][2])) == p_35), 0xD0L)) < p_35) >= (*p_33)), (-1L))) ^ p_35))), p_35)), l_592[2]))))), p_35)) >= p_35) , g_133))) >= 0x90L) && 18446744073709551613UL) , l_592[5])))), 0x4DL)) >= g_200)), 1)) || 0x9D116FFCL);
                    }
                    if (l_594)
                        break;
                    (*g_42) = (safe_sub_func_int16_t_s_s((safe_rshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(4L, 0)), 10)), (safe_mod_func_uint64_t_u_u(p_35, (safe_add_func_int64_t_s_s(g_493, ((((safe_mod_func_int64_t_s_s((((*l_614) = (safe_mul_func_int16_t_s_s((l_594 && (safe_mod_func_int32_t_s_s(l_611, l_562))), (safe_lshift_func_uint8_t_u_s(((void*)0 != &g_196), 1))))) >= (safe_unary_minus_func_int16_t_s((-7L)))), 3L)) > g_123[5]) >= p_35) > 0x1F0F3C9611D21008LL)))))));
                    (*l_91) = func_37(((*l_620) = func_37(l_616, l_617)), &g_99[1]);
                }
                else
                { /* block id: 249 */
                    int8_t l_621[3][4] = {{1L,0x44L,1L,1L},{0x44L,0x44L,0x9AL,0x44L},{0x44L,1L,1L,0x44L}};
                    int32_t l_625 = 2L;
                    int32_t l_630 = 0x2A2E1C39L;
                    int32_t l_634 = 0xF7BF4A2FL;
                    int32_t l_637[9] = {1L,(-1L),1L,1L,(-1L),1L,1L,(-1L),1L};
                    int i, j;
                    for (l_562 = 0; (l_562 <= 2); l_562 += 1)
                    { /* block id: 252 */
                        int32_t *l_622[3];
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_622[i] = &g_41[4][3][2];
                        (*g_42) = 6L;
                        if (l_90[(l_562 + 2)][(l_560 + 3)])
                            break;
                        --l_645;
                    }
                    g_353 = (l_627 = ((((*l_563) = (void*)0) == (void*)0) != ((**g_223) = (-0x3.Ap+1))));
                }
                (*l_617) = ((*l_91) = ((**l_617) , &l_592[2]));
                --l_655;
            }
            else
            { /* block id: 265 */
                int32_t l_662 = 1L;
                for (g_156 = 3; (g_156 >= 0); g_156 -= 1)
                { /* block id: 268 */
                    int32_t ***l_658[1];
                    uint32_t *l_660 = (void*)0;
                    uint32_t **l_659 = &l_660;
                    uint32_t ***l_661 = &l_659;
                    uint64_t l_664 = 0xD8E2B98AC7B02F87LL;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_658[i] = &g_227;
                    g_227 = &p_33;
                    (*l_661) = l_659;
                    ++l_664;
                    for (l_628 = 2; (l_628 >= 0); l_628 -= 1)
                    { /* block id: 274 */
                        uint16_t *l_677 = &g_200;
                        int16_t *l_681 = &l_654;
                        int i, j, k;
                        (*l_616) |= (((((((((((safe_div_func_uint64_t_u_u((l_669 != g_671), (*g_534))) || (safe_mul_func_int8_t_s_s((p_35 > (((safe_div_func_uint8_t_u_u((((~(((*l_677) = g_126[0]) && ((*l_681) ^= (safe_mul_func_uint16_t_u_u(((!g_177) || 1UL), ((void*)0 != &g_202)))))) , 0xE1BFL) < g_121), p_35)) < 1UL) || 0x28D076BA9E9AE5BCLL)), p_35))) < l_623) ^ 0x77437EDF75F7AAC2LL) >= l_592[0]) < 0xF2B9L) != p_35) >= l_562) > (**l_91)) && p_35) > g_177);
                    }
                }
            }
            (*p_33) = l_682;
            return p_35;
        }
        if (((safe_lshift_func_int16_t_s_u((*l_616), (((*l_692) = (p_35 >= ((((safe_mul_func_float_f_f((0x1.5p+1 >= ((**l_91) > p_35)), (safe_div_func_float_f_f(((l_592[2] != ((*l_85) = ((**g_223) < ((*l_689) = (**g_223))))) == (((**l_91) >= (*l_616)) != p_35)), p_35)))) > 0xC.148369p+30) , 0x8EL) ^ 1UL))) , 0x6235L))) , (*p_33)))
        { /* block id: 287 */
            int32_t l_695 = (-7L);
            l_592[2] ^= ((65530UL && (((**l_91) = 0xDB86CECBL) ^ ((*l_616) = (+(l_694 != l_695))))) , ((0xD1E11C01581EC597LL < ((!((safe_rshift_func_int16_t_s_u((safe_mod_func_uint64_t_u_u(0x172D7973AEEB22B1LL, p_35)), ((((p_35 , (((65528UL && l_654) , 0x9955L) < 6L)) > g_123[4]) > l_695) && (*p_36)))) == 1UL)) < 0x10E88E2BL)) , (**l_91)));
            for (l_682 = (-3); (l_682 == 25); l_682 = safe_add_func_uint8_t_u_u(l_682, 1))
            { /* block id: 293 */
                return p_35;
            }
        }
        else
        { /* block id: 296 */
            uint32_t l_723[2];
            int32_t l_732 = 0x40D12F2EL;
            int32_t l_733[8][9] = {{(-4L),0xD7803B80L,0xDDC051F6L,0x9B4D9FEDL,(-3L),0x55E02593L,(-1L),0xEB85D5EFL,0xE18E46D1L},{(-9L),(-6L),(-3L),(-1L),0x97101012L,(-7L),0x5CE49E2DL,(-1L),0xEB85D5EFL},{0x5CE49E2DL,0xEB85D5EFL,0x05FF6FFAL,0xD7803B80L,0xE18E46D1L,0xE18E46D1L,0xD7803B80L,0x05FF6FFAL,0xEB85D5EFL},{(-3L),(-9L),0xD7803B80L,0x6F8D14E3L,1L,(-7L),(-1L),(-1L),0xE18E46D1L},{(-1L),0x97101012L,0x3079AC1FL,0xE18E46D1L,(-1L),(-1L),(-7L),1L,0x6F8D14E3L},{0x55E02593L,(-9L),0x5CE49E2DL,0xEB85D5EFL,0x05FF6FFAL,0xD7803B80L,0xE18E46D1L,0xE18E46D1L,0xD7803B80L},{(-1L),0xEB85D5EFL,0x55E02593L,0xEB85D5EFL,(-1L),0x5CE49E2DL,(-7L),0x97101012L,(-1L)},{0x6F8D14E3L,(-6L),0x29C2525FL,0xE18E46D1L,0xEB85D5EFL,(-1L),0x55E02593L,(-3L),0x9B4D9FEDL}};
            uint8_t *l_749 = &l_90[6][0];
            uint64_t **l_824 = &g_534;
            uint32_t l_855 = 0x9D402282L;
            uint16_t *l_913 = &g_200;
            uint8_t l_958 = 9UL;
            int i, j;
            for (i = 0; i < 2; i++)
                l_723[i] = 4294967295UL;
            if ((*l_616))
            { /* block id: 297 */
                int32_t l_722 = 1L;
                int32_t l_729[6][4][2] = {{{(-1L),0x04A992F6L},{(-1L),(-1L)},{0L,0L},{0x8DB1885CL,(-7L)}},{{0x6D0ECCBCL,1L},{0xEDD48E09L,0x6D0ECCBCL},{(-1L),(-4L)},{(-1L),0x6D0ECCBCL}},{{0xEDD48E09L,1L},{0x6D0ECCBCL,(-7L)},{0x8DB1885CL,0L},{0L,(-1L)}},{{(-1L),0x04A992F6L},{(-1L),0xCCDE5E97L},{0L,(-1L)},{0x92D7E86FL,5L}},{{0L,1L},{0x04A992F6L,0x616A1ECBL},{5L,0x616A1ECBL},{0x04A992F6L,1L}},{{0L,5L},{0x92D7E86FL,(-1L)},{0L,0xCCDE5E97L},{(-1L),0x04A992F6L}}};
                float ** const l_746 = &l_689;
                int64_t *l_803 = (void*)0;
                int64_t *l_804 = (void*)0;
                int64_t *l_805[9] = {&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135};
                int i, j, k;
                for (g_196 = 0; (g_196 <= 0); g_196 += 1)
                { /* block id: 300 */
                    int32_t *l_724 = &l_643[1];
                    int32_t l_728[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
                    int32_t l_730 = 1L;
                    uint32_t l_734[9];
                    int i;
                    for (i = 0; i < 9; i++)
                        l_734[i] = 4UL;
                    if (((g_126[(g_196 + 4)] & ((*l_692) = (safe_add_func_int32_t_s_s(((safe_rshift_func_int16_t_s_s((+((0x30L == (safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s((((safe_sub_func_int8_t_s_s((safe_unary_minus_func_int8_t_s((l_722 = (safe_sub_func_uint64_t_u_u(((&l_689 == (void*)0) || ((p_35 , ((safe_sub_func_uint16_t_u_u(0x9892L, g_126[4])) && g_126[4])) & (l_721 != (void*)0))), g_216))))), p_35)) == 0x0322L) && (*l_616)), l_723[0])) && (*l_616)), 0x97L)), 0))) & 0x7EBE4E5297D2B27FLL)), l_654)) | (*l_616)), 0x5630AF40L)))) < 0x94L))
                    { /* block id: 303 */
                        (*g_227) = l_724;
                    }
                    else
                    { /* block id: 305 */
                        int32_t *l_725 = &g_92[3][0];
                        int32_t *l_726 = &l_592[5];
                        int32_t *l_727[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_727[i] = &l_643[1];
                        ++l_734[6];
                    }
                    for (g_216 = 0; (g_216 <= 0); g_216 += 1)
                    { /* block id: 310 */
                        int32_t *l_738 = &l_640;
                        int16_t *l_745 = &g_177;
                        float ***l_747 = &g_223;
                        int16_t *l_748[7][7] = {{&g_638,&l_654,&g_638,&g_638,&l_654,&g_638,&g_123[4]},{(void*)0,&g_123[4],&g_123[5],&g_123[0],&l_731,&l_731,&g_123[0]},{&g_123[5],&g_638,&l_654,&g_123[4],&g_638,&g_638,&g_123[5]},{(void*)0,&g_638,(void*)0,&g_123[5],(void*)0,&g_638,(void*)0},{&l_654,&g_638,&g_123[5],&g_638,&g_123[4],&g_638,&g_123[4]},{&g_638,&l_654,&l_654,&g_638,&l_654,(void*)0,&g_123[5]},{&g_638,&g_638,&g_123[5],&l_654,&l_654,&g_123[5],&g_638}};
                        int64_t *l_750[3][7][7] = {{{&g_135,&g_135,(void*)0,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_135,&g_135,(void*)0,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135}},{{&g_135,&g_135,(void*)0,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_135,&g_135,(void*)0,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135}},{{&g_135,&g_135,(void*)0,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_135,&g_135,(void*)0,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135}}};
                        int i, j, k;
                        (*g_227) = (l_738 = func_37(&l_728[6], g_737[1][0][4]));
                        g_92[(g_196 + 3)][g_216] = (safe_lshift_func_uint16_t_u_s((safe_mul_func_int16_t_s_s(((*l_745) = (safe_rshift_func_int8_t_s_s((0x54L >= g_92[g_196][g_196]), 1))), ((*l_724) = (l_746 == ((*l_747) = (void*)0))))), (l_749 != &g_196)));
                        g_92[(g_216 + 3)][g_216] = ((*l_724) && (g_135 = (l_732 & (((*l_749) |= p_35) | (l_592[2] | 0L)))));
                        (*l_85) = ((**l_746) = (*l_738));
                    }
                }
                for (l_722 = (-27); (l_722 > 18); l_722 = safe_add_func_uint8_t_u_u(l_722, 1))
                { /* block id: 326 */
                    int16_t *l_757 = &g_123[6];
                    int8_t *l_766 = &g_202;
                    int8_t *l_767 = &l_60;
                    int32_t l_768 = 1L;
                    int8_t *l_769 = &g_770[2];
                    l_592[7] = (safe_sub_func_int64_t_s_s(((safe_add_func_int8_t_s_s((((*l_757) = l_723[0]) & p_35), (safe_div_func_int8_t_s_s(g_92[3][0], (safe_add_func_int8_t_s_s(g_92[3][0], ((*l_769) = ((p_35 < (safe_sub_func_int8_t_s_s((**l_91), (((*l_767) = ((*l_766) = (safe_rshift_func_uint8_t_u_s((p_35 || (g_216 , l_733[1][4])), g_270)))) & 0xE9L)))) || l_768)))))))) , l_768), 0L));
                    (*l_91) = &l_729[1][2][1];
                    (*l_616) = ((0xA.B4CFCDp-54 <= (safe_sub_func_float_f_f((*g_224), (safe_mul_func_float_f_f(((((safe_div_func_float_f_f(p_35, (safe_div_func_float_f_f(((safe_div_func_float_f_f((safe_div_func_float_f_f((-l_723[0]), (!(*g_224)))), ((*l_689) = 0x4.2AE431p-61))) != (((*g_42) || (l_768 > ((*g_40) = (((g_97 , l_729[4][3][0]) || (-9L)) > 0xEC51L)))) , 0xC.CB319Fp+84)), p_35)))) <= 0x0.359392p-23) < 0x1.4p-1) > (*g_224)), 0xD.B089B7p-8))))) == (*g_224));
                    (*l_616) |= l_654;
                }
                (**l_746) = ((*g_224) < p_35);
                (*l_689) = (safe_div_func_float_f_f(0x9.CB8456p+81, ((((*l_749) = (safe_mul_func_uint16_t_u_u(((**g_531) , ((safe_sub_func_uint8_t_u_u(((l_791 = l_791) != ((((**l_91) = g_493) <= (safe_div_func_int8_t_s_s(((safe_sub_func_int16_t_s_s((!(((safe_rshift_func_int8_t_s_u(p_35, 5)) > (safe_sub_func_int64_t_s_s((l_732 = g_93), 0xB99E28FD3C3772CCLL))) <= (g_123[4] < (g_123[6] | p_35)))), 0UL)) ^ l_644), p_35))) , (void*)0)), 1L)) & 0UL)), 0x6826L))) < p_35) , l_722)));
            }
            else
            { /* block id: 344 */
                uint64_t ***l_814 = &g_533[4][0][2];
                int32_t l_817 = 0L;
                int32_t *l_818 = &g_100;
                int32_t l_843 = 0x07FF16ABL;
                int32_t l_845 = (-2L);
                int32_t l_846 = 0x8B1F54DAL;
                int32_t l_847 = (-2L);
                int32_t l_851 = 0xA20F3496L;
                int32_t l_853 = 0L;
                if (((((*l_749) = p_35) & ((&g_671 != (void*)0) > (-1L))) && (((g_133 = (l_817 = ((((safe_div_func_float_f_f((safe_mul_func_float_f_f((((((((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u(251UL, 6)), l_645)) , &g_534) == ((*l_814) = &g_534)) , (((safe_sub_func_float_f_f(((*g_224) > l_592[2]), p_35)) , 1L) && (-2L))) >= (**g_531)) != (*l_616)) , (*g_224)), l_733[3][8])), (**g_223))) != 0x0.3p-1) , (**l_91)) == (**g_531)))) , p_35) | p_35)))
                { /* block id: 349 */
                    (*l_616) |= (*p_36);
                    return p_35;
                }
                else
                { /* block id: 352 */
                    l_818 = (void*)0;
                }
                if (((safe_unary_minus_func_int8_t_s((safe_lshift_func_int8_t_s_s(l_644, (g_29 & ((((safe_rshift_func_int16_t_s_u((**l_91), 5)) ^ (((g_92[3][0] | l_723[0]) | (**g_531)) , (*l_616))) , l_824) == ((*l_814) = &g_534))))))) < 0xC429L))
                { /* block id: 356 */
                    uint16_t l_828 = 1UL;
                    int32_t l_844 = (-1L);
                    int32_t l_848 = 0x1231E8B4L;
                    int32_t l_849 = 0L;
                    int32_t l_852 = 0x8F4D7ED2L;
                    int32_t l_854 = 0x8DFE9484L;
                    uint16_t l_866 = 0UL;
                    for (g_100 = 0; (g_100 <= 2); g_100 += 1)
                    { /* block id: 359 */
                        const uint64_t l_829 = 9UL;
                        int32_t *l_830 = &l_640;
                        int32_t *l_831 = &g_156;
                        int32_t *l_832 = &l_682;
                        int32_t *l_833 = &l_643[1];
                        int32_t *l_834 = (void*)0;
                        int32_t *l_835 = (void*)0;
                        int32_t *l_836 = &l_639;
                        int32_t *l_837 = &g_156;
                        int32_t *l_838 = &g_41[4][3][2];
                        int32_t *l_839 = &l_640;
                        int32_t *l_840 = &g_93;
                        int32_t *l_841 = &l_628;
                        int32_t *l_842[4][4];
                        int i, j;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 4; j++)
                                l_842[i][j] = &l_639;
                        }
                        (*g_40) |= (g_825 != &g_826);
                        (**l_91) = (l_828 && l_829);
                        l_855--;
                    }
                    for (l_846 = 0; (l_846 < 21); ++l_846)
                    { /* block id: 366 */
                        int32_t l_860 = 9L;
                        const int32_t ***l_862 = (void*)0;
                        const int32_t ***l_863 = &g_737[1][2][0];
                        int32_t *l_864 = &g_561;
                        int32_t *l_865[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_865[i] = (void*)0;
                        (*l_91) = func_37(&g_41[2][2][3], ((*l_863) = ((1UL == (l_860 = (*l_616))) , l_861)));
                        --l_866;
                    }
                }
                else
                { /* block id: 372 */
                    (*g_42) ^= 0xC4E460ACL;
                }
                if (l_850)
                { /* block id: 375 */
                    uint16_t l_906 = 0x6F3EL;
                    int32_t **l_920 = &l_818;
                    int16_t *l_927 = &g_638;
                    for (g_216 = 0; (g_216 <= 7); g_216 += 1)
                    { /* block id: 378 */
                        int64_t *l_875[8] = {&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135};
                        int32_t l_876[10] = {0x611C9789L,0x7619AB23L,(-6L),0x7619AB23L,1L,0L,1L,0x7619AB23L,0x7619AB23L,1L};
                        uint16_t **l_914 = &l_691;
                        int i;
                        (*l_616) &= ((safe_rshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_s(p_35, 15)), 13)) <= (safe_lshift_func_uint16_t_u_u(((l_876[4] = (l_732 |= ((**l_91) ^= g_270))) || (safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s(((*l_749)++), 4)) , (safe_add_func_int64_t_s_s((0x8CL && ((safe_mod_func_int8_t_s_s((8UL > ((safe_lshift_func_uint16_t_u_u((((safe_mod_func_int16_t_s_s(((((safe_mul_func_int8_t_s_s((((((((g_135 = ((safe_mul_func_float_f_f((((--(**l_792)) , (safe_sub_func_float_f_f(((safe_add_func_float_f_f(((void*)0 == &g_196), ((**g_223) < (safe_sub_func_float_f_f((p_35 , (!(+(-(*g_224))))), (*g_224)))))) == 0xE.1BF6ADp+59), (**g_223)))) < (-0x1.2p-1)), l_906)) , g_493)) <= 0UL) , (**g_531)) | 0x3745E85687F8AAD4LL) , &p_36) != &p_36) >= 0x31CB9C56L), p_35)) <= p_35) & (**g_531)) && l_876[8]), g_156)) <= p_35) <= p_35), p_35)) & 0x9C6CL)), p_35)) , g_92[3][0])), p_35))), 0x4FL))), g_196)));
                        if (g_198)
                            goto lbl_921;
                        (*p_33) = (safe_div_func_uint32_t_u_u((((+((l_592[2] , g_93) , ((safe_rshift_func_int16_t_s_s(g_126[4], (((safe_unary_minus_func_uint8_t_u(p_35)) < ((l_913 == ((*l_914) = l_690)) <= p_35)) != (safe_rshift_func_uint8_t_u_u(248UL, 1))))) | g_41[6][1][3]))) && 0x04DEEEC7L) , 4294967295UL), 0x8B9D34F9L));
                    }
                    for (g_200 = 23; (g_200 <= 35); g_200 = safe_add_func_uint64_t_u_u(g_200, 4))
                    { /* block id: 391 */
                        int16_t l_919 = 3L;
                        (*l_91) = p_33;
                        if (l_919)
                            continue;
                    }
lbl_921:
                    (*l_91) = func_37(func_37(((*l_920) = (void*)0), l_861), &g_99[1]);
                    if (((safe_mul_func_int16_t_s_s((safe_unary_minus_func_uint16_t_u((safe_mod_func_uint32_t_u_u(((*l_793) = (((*l_927) ^= p_35) , ((safe_unary_minus_func_uint64_t_u(((((*l_670)--) , ((safe_add_func_int32_t_s_s((((safe_lshift_func_uint16_t_u_s((((void*)0 != l_935[2]) && 8UL), (g_770[1] && ((((*g_534) <= (*g_532)) , ((p_35 >= (safe_sub_func_int64_t_s_s(0x0E8945CABE41C8EBLL, (-10L)))) < (**g_531))) | g_561)))) & 0x745A97D2L) | 4294967290UL), l_733[4][8])) , 0xEDL)) | g_202))) , g_156))), g_938)))), p_35)) , (*p_36)))
                    { /* block id: 401 */
                        uint32_t l_950 = 0x8327DA26L;
                        (*p_33) = (safe_lshift_func_uint8_t_u_u(((g_770[3] = (safe_div_func_int64_t_s_s(((p_35 , ((((g_827.f3 = ((safe_mul_func_uint8_t_u_u((safe_add_func_int32_t_s_s((l_814 == &g_531), ((18446744073709551607UL < 0UL) > (((*l_692) |= 0UL) < (safe_sub_func_int64_t_s_s((~(*g_40)), l_950)))))), (l_950 && l_950))) && p_35)) >= 0x11DFL) > p_35) && g_156)) <= g_938), l_654))) ^ p_35), 0));
                    }
                    else
                    { /* block id: 406 */
                        return g_938;
                    }
                }
                else
                { /* block id: 409 */
                    int16_t *l_957[3];
                    int32_t *l_964 = &l_963;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_957[i] = &l_731;
                    for (g_177 = 0; (g_177 <= 1); g_177 += 1)
                    { /* block id: 412 */
                        int i;
                        return g_123[(g_177 + 5)];
                    }
                    (*g_42) &= 0x6A7B7ED2L;
                    l_963 ^= ((safe_add_func_uint32_t_u_u(0xC22B8CA7L, ((((safe_rshift_func_uint8_t_u_u(g_92[3][0], ((*l_749) = ((l_733[3][8] = (l_732 = (safe_div_func_uint8_t_u_u((l_592[2] |= g_216), 0xACL)))) , 255UL)))) < ((*g_40) = (*p_33))) ^ ((((l_958 | (((((&g_223 == ((((safe_sub_func_uint32_t_u_u(((l_961 > l_850) , g_177), g_126[1])) <= 0L) , p_35) , l_962)) >= 0x68L) < g_126[4]) | p_35) == (*g_534))) , p_35) && (-1L)) , g_97)) & l_644))) ^ l_723[0]);
                    l_964 = p_33;
                }
            }
        }
        for (g_561 = 28; (g_561 >= (-28)); g_561 = safe_sub_func_uint8_t_u_u(g_561, 1))
        { /* block id: 428 */
            uint32_t **l_967 = (void*)0;
            int8_t *l_981 = &l_60;
            int32_t l_982 = 0x0F21C662L;
            l_669 = l_967;
            (*g_40) = (safe_sub_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((1UL && ((!(((*l_981) &= (safe_div_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u((*g_532), g_938)) , ((0xEFE0L && (((safe_rshift_func_uint16_t_u_u((((*p_36) > (*p_33)) , ((*l_692) = (g_100 == (safe_mod_func_int32_t_s_s((&g_533[3][0][2] == (void*)0), (*p_36)))))), 8)) , (void*)0) == (void*)0)) == p_35)) == p_35), g_196))) < l_982)) <= g_92[0][0])), g_123[6])), p_35));
            (*g_227) = func_37(l_983, &g_99[1]);
        }
    }
    else
    { /* block id: 435 */
        uint32_t l_988[6] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
        int16_t *l_995 = &g_123[4];
        int32_t l_1020 = 0x9093321BL;
        int64_t l_1025 = 0L;
        uint64_t *l_1026 = (void*)0;
        uint16_t *l_1033 = &g_200;
        int32_t *l_1128 = &l_639;
        uint32_t l_1164 = 18446744073709551615UL;
        union U0 *l_1168[5] = {&g_1169,&g_1169,&g_1169,&g_1169,&g_1169};
        int8_t *l_1200[8][6] = {{&g_202,&g_770[2],&g_770[2],&g_770[2],&g_202,&g_202},{&l_60,&g_770[2],&g_770[2],&l_60,(void*)0,&l_60},{&l_60,(void*)0,&l_60,&g_770[2],&g_770[2],&l_60},{&g_202,&g_202,&g_770[2],&g_770[2],&g_770[2],&g_202},{&g_770[2],(void*)0,&g_770[2],&g_770[2],(void*)0,&g_770[2]},{&g_202,&g_770[2],&g_770[2],&g_770[2],&g_202,&g_202},{&l_60,&g_770[2],&g_770[2],&l_60,(void*)0,&l_60},{&l_60,(void*)0,&l_60,&g_770[2],&g_770[2],&l_60}};
        int32_t l_1220[10][3] = {{0L,1L,0xB10D163CL},{1L,0L,0xB10D163CL},{1L,1L,0xB10D163CL},{0L,1L,0xB10D163CL},{1L,0L,0xB10D163CL},{1L,1L,0xB10D163CL},{0L,1L,0xB10D163CL},{1L,0L,0xB10D163CL},{1L,1L,0xB10D163CL},{0L,1L,0xB10D163CL}};
        uint8_t l_1225[2];
        int64_t *l_1278 = &l_1025;
        int i, j;
        for (i = 0; i < 2; i++)
            l_1225[i] = 0UL;
        (*l_983) |= ((safe_lshift_func_uint16_t_u_u((((*l_995) = (safe_lshift_func_uint16_t_u_s(l_988[2], ((safe_mod_func_uint8_t_u_u(0xDEL, (safe_lshift_func_uint16_t_u_s(0xC6D9L, 2)))) < ((((void*)0 != &l_670) , &g_224) == (*l_962)))))) != ((((p_35 , g_216) , l_996) != l_996) || (*l_616))), 10)) >= g_100);
    }
    if ((*g_1166))
    { /* block id: 621 */
        int64_t ***l_1282 = &l_1280;
        int64_t **l_1284 = &l_1281;
        int64_t ***l_1283 = &l_1284;
        int64_t **l_1286 = (void*)0;
        int64_t ***l_1285 = &l_1286;
        (*g_1166) ^= (((*l_1282) = l_1280) != ((*l_1285) = (p_35 , ((*l_1283) = (void*)0))));
    }
    else
    { /* block id: 626 */
        uint32_t *l_1289 = &l_253[0];
        int32_t l_1292[1][1];
        uint32_t *l_1293 = &l_560;
        int32_t *l_1301 = &l_643[1];
        int32_t *l_1302[1];
        float l_1303[7][9][2] = {{{0x2.Dp-1,0x2.Dp-1},{0xE.3320FCp+93,0x6.E33120p-88},{(-0x7.Ap+1),0x8.Ep-1},{0x4.E91E12p-71,0xE.C94C0Dp-43},{0x1.0p-1,0x4.E91E12p-71},{0xF.B3A383p-66,0x5.4p+1},{0xF.B3A383p-66,0x4.E91E12p-71},{0x1.0p-1,0xE.C94C0Dp-43},{0x4.E91E12p-71,0x8.Ep-1}},{{(-0x7.Ap+1),0x6.E33120p-88},{0xE.3320FCp+93,0x2.Dp-1},{0x2.Dp-1,(-0x7.Ap+1)},{0x3.1BA9CCp+71,0x4.48C27Ap+22},{0x1.Dp+1,0x7.90BC62p+15},{0x8.Ep-1,0xB.CC6E39p+35},{(-0x10.Dp+1),0x7.C993E3p+37},{0x5.4p+1,0x0.7p+1},{0x1.8p-1,0xF.221014p-38}},{{0x7.90BC62p+15,0xF.B3A383p-66},{0x2.59D194p-90,0x1.Dp+1},{0xB.8A8A23p+34,0x1.Dp+1},{0x2.59D194p-90,0xF.B3A383p-66},{0x7.90BC62p+15,0xF.221014p-38},{0x1.8p-1,0x0.7p+1},{0x5.4p+1,0x7.C993E3p+37},{(-0x10.Dp+1),0xB.CC6E39p+35},{0x8.Ep-1,0x7.90BC62p+15}},{{0x1.Dp+1,0x4.48C27Ap+22},{0x3.1BA9CCp+71,(-0x7.Ap+1)},{0x2.Dp-1,0x2.Dp-1},{0xE.3320FCp+93,0x6.E33120p-88},{(-0x7.Ap+1),0x8.Ep-1},{0x4.E91E12p-71,0xE.C94C0Dp-43},{0x1.0p-1,0x4.E91E12p-71},{0xF.B3A383p-66,0x5.4p+1},{0xF.B3A383p-66,0x4.E91E12p-71}},{{0x1.0p-1,0xE.C94C0Dp-43},{0x4.E91E12p-71,0x8.Ep-1},{(-0x7.Ap+1),0x6.E33120p-88},{0xE.3320FCp+93,0x2.Dp-1},{0x2.Dp-1,(-0x7.Ap+1)},{0x3.1BA9CCp+71,0x4.48C27Ap+22},{0x1.Dp+1,0x7.90BC62p+15},{0x8.Ep-1,0xB.CC6E39p+35},{(-0x10.Dp+1),0x7.C993E3p+37}},{{0x5.4p+1,0x0.7p+1},{0x1.8p-1,0xF.221014p-38},{0xB.8A8A23p+34,0x4.48C27Ap+22},{0xF.221014p-38,0x5.4p+1},{(-0x10.Dp+1),0x5.4p+1},{0xF.221014p-38,0x4.48C27Ap+22},{0xB.8A8A23p+34,0xF.B3A383p-66},{(-0x7.Ap+1),0x4.E91E12p-71},{0xD.B6D1FAp-88,0x8.Ep-1}},{{0xB.CC6E39p+35,(-0x5.Ep-1)},{0x0.7p+1,0xB.8A8A23p+34},{0x5.4p+1,0x6.E33120p-88},{0xE.C94C0Dp-43,0x2.59D194p-90},{0x8.C07ADDp+87,0x8.C07ADDp+87},{0x1.8p-1,(-0x5.4p+1)},{0x2.59D194p-90,0x0.7p+1},{0x1.0p-1,0x5.4p+1},{0x1.Dp+1,0x1.0p-1}}};
        uint16_t l_1304 = 1UL;
        uint64_t **l_1313 = &l_1129;
        const int32_t *l_1326 = &l_1292[0][0];
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_1292[i][j] = 0L;
        }
        for (i = 0; i < 1; i++)
            l_1302[i] = (void*)0;
        (*p_36) = (safe_div_func_uint16_t_u_u(((**g_531) , ((--(*l_1289)) ^ (g_200 , ((*g_534) != g_200)))), (((++(*l_1293)) >= (((((((g_202 , (0x98EAL == (safe_mod_func_int32_t_s_s((safe_lshift_func_int16_t_s_s((0x6012L != (((void*)0 == &l_616) != 0xB98FA567L)), 15)), l_1292[0][0])))) <= (*l_616)) , &g_228) != &g_1166) || 6UL) <= g_121) | l_1292[0][0])) , l_1300)));
        l_1304++;
        for (g_1171.f2 = 1; (g_1171.f2 <= 4); g_1171.f2 += 1)
        { /* block id: 633 */
            uint32_t l_1324 = 1UL;
            uint64_t **l_1325[7];
            int i;
            for (i = 0; i < 7; i++)
                l_1325[i] = &l_1129;
            for (g_156 = 1; (g_156 >= 0); g_156 -= 1)
            { /* block id: 636 */
                uint64_t ***l_1314 = &g_533[3][0][2];
                uint64_t ***l_1315[5][7][3] = {{{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313}},{{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0}},{{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313}},{{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313}},{{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,&l_1313},{&l_1313,&l_1313,(void*)0},{&l_1313,&l_1313,&l_1313}}};
                uint8_t *l_1320 = &l_90[7][3];
                int i, j, k;
                for (l_1304 = 0; (l_1304 <= 1); l_1304 += 1)
                { /* block id: 639 */
                    int i;
                    return l_253[(g_156 + 1)];
                }
                if (((safe_div_func_int8_t_s_s(l_253[(g_156 + 2)], (safe_add_func_int64_t_s_s(0x1D2961A72336114ELL, (safe_rshift_func_int8_t_s_s(((l_1313 = ((*l_1314) = l_1313)) == ((safe_mod_func_int16_t_s_s((safe_mod_func_int32_t_s_s(((((*l_1320) |= (((**l_1280) = ((void*)0 == &l_1289)) != (l_253[(g_156 + 2)] | ((*g_40) = (0x647AD728L != l_253[(g_156 + 2)]))))) , ((safe_mul_func_uint8_t_u_u((+p_35), l_1324)) == (**g_531))) && p_35), 0xE62B1B72L)), p_35)) , l_1325[3])), p_35)))))) < g_1212))
                { /* block id: 647 */
                    int i;
                    l_1326 = g_99[g_156];
                    (*l_616) |= ((&g_770[2] != &g_770[2]) , ((*l_983) < 0x25C8L));
                }
                else
                { /* block id: 650 */
                    for (g_135 = 0; (g_135 <= 1); g_135 += 1)
                    { /* block id: 653 */
                        int i;
                        (*g_224) = p_35;
                        (*g_1166) |= (g_99[g_156] == (*g_417));
                    }
                }
            }
        }
    }
    return g_198;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_37(int32_t * const  p_38, const int32_t ** p_39)
{ /* block id: 3 */
    int32_t *l_45 = &g_41[4][3][2];
    return l_45;
}


/* ------------------------------------------ */
/* 
 * reads : g_202 g_196 g_123 g_41 g_156 g_228 g_227 g_92 g_224 g_86 g_133 g_99 g_270 g_177 g_200 g_126 g_223 g_417 g_466 g_135
 * writes: g_202 g_126 g_196 g_156 g_177 g_228 g_41 g_133 g_121 g_135 g_86 g_123 g_92
 */
static float  func_50(uint32_t  p_51, int32_t  p_52)
{ /* block id: 90 */
    int32_t l_258 = 0L;
    int8_t *l_265 = &g_202;
    uint32_t *l_267 = &g_126[4];
    const uint32_t *l_269[10] = {&g_270,&g_270,&g_270,&g_270,&g_270,&g_270,&g_270,&g_270,&g_270,&g_270};
    const uint32_t **l_268 = &l_269[0];
    uint64_t *l_273 = &g_121;
    float *l_297 = &g_86;
    float **l_296 = &l_297;
    int32_t l_331 = 4L;
    uint32_t l_334 = 4UL;
    int16_t *l_374 = (void*)0;
    int32_t l_423 = 6L;
    int32_t l_425 = (-1L);
    int32_t l_434[4][1][7] = {{{0x6E92700AL,0x99B90E49L,0x99B90E49L,0x6E92700AL,0x99B90E49L,0x99B90E49L,0x6E92700AL}},{{(-1L),0L,(-1L),(-1L),0L,(-1L),(-1L)}},{{0x6E92700AL,0x6E92700AL,1L,0x6E92700AL,0x6E92700AL,1L,0x6E92700AL}},{{0L,(-1L),(-1L),0L,(-1L),(-1L),0L}}};
    float l_443 = 0xB.0EF4E7p+80;
    int8_t l_449 = 0xD2L;
    uint16_t l_463 = 0x0319L;
    const uint16_t *l_496 = &g_200;
    const uint16_t **l_495 = &l_496;
    int i, j, k;
    if ((((safe_div_func_int32_t_s_s(3L, g_202)) , ((((*l_268) = (((*l_267) = (((g_196 & ((*l_265) = ((safe_add_func_int16_t_s_s(l_258, (safe_div_func_int16_t_s_s((safe_add_func_int32_t_s_s(l_258, l_258)), (safe_rshift_func_uint8_t_u_s(g_123[7], 0)))))) < p_51))) , (!(0x17L | (-1L)))) || p_52)) , l_267)) == &g_270) , l_258)) || l_258))
    { /* block id: 94 */
        float * const l_271[2] = {&g_86,&g_86};
        uint64_t **l_272[1];
        uint8_t *l_284 = (void*)0;
        uint8_t *l_285 = &g_196;
        int32_t *l_286 = &l_258;
        uint16_t *l_300[10] = {&g_200,&g_200,&g_200,&g_200,&g_200,&g_200,&g_200,&g_200,&g_200,&g_200};
        int32_t l_327[6] = {6L,0x7B6874CDL,6L,6L,6L,1L};
        int8_t l_448 = 0L;
        int i;
        for (i = 0; i < 1; i++)
            l_272[i] = (void*)0;
        (*l_286) |= (((*l_285) = (((((((void*)0 != l_271[1]) , 6L) , (l_273 = (void*)0)) != ((p_51 & (((safe_mod_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((1UL && (p_52 , ((g_41[4][3][2] != (safe_rshift_func_int8_t_s_u(0L, 6))) < p_51))) , p_51), (-8L))), (-2L))) | g_123[7]) >= 4294967290UL)) , (void*)0)) > p_52) , p_51)) , 0x4FA2660AL);
        if (((void*)0 == l_272[0]))
        { /* block id: 98 */
            return l_258;
        }
        else
        { /* block id: 100 */
            int32_t *l_293[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint64_t l_349 = 0x6027655B96C97FDCLL;
            int32_t l_354 = 0xBF856E9AL;
            int16_t *l_357 = &g_177;
            int64_t l_379[2];
            uint16_t l_451 = 0x2394L;
            int i;
            for (i = 0; i < 2; i++)
                l_379[i] = 0xF8E4FD7F2F9AD659LL;
            for (g_156 = 0; (g_156 >= 2); g_156 = safe_add_func_uint8_t_u_u(g_156, 3))
            { /* block id: 103 */
                const int32_t **l_291 = (void*)0;
                const int64_t l_309 = (-1L);
                uint16_t **l_313 = &l_300[2];
                int32_t *l_314 = &g_92[3][0];
                const float *l_321 = &g_86;
                const float **l_320 = &l_321;
                int32_t l_329 = 9L;
                int32_t l_330 = 0xFFB36650L;
                int32_t l_332 = 0x1CA1425DL;
                uint8_t l_377 = 0xC1L;
                int32_t l_435 = 0xA46653E8L;
                int32_t l_441[2];
                uint32_t l_445 = 18446744073709551615UL;
                int i;
                for (i = 0; i < 2; i++)
                    l_441[i] = 1L;
                for (g_177 = 0; (g_177 <= (-3)); g_177 = safe_sub_func_int32_t_s_s(g_177, 1))
                { /* block id: 106 */
                    const int32_t **l_292 = &g_99[0];
                    float **l_308[10][4] = {{&l_297,(void*)0,&l_297,(void*)0},{&g_224,&g_224,&l_297,&l_297},{(void*)0,&g_224,&g_224,(void*)0},{&g_224,(void*)0,&g_224,&l_297},{&g_224,&g_224,&g_224,&l_297},{(void*)0,&l_297,&l_297,&l_297},{&g_224,&g_224,&l_297,&l_297},{&l_297,(void*)0,&l_297,(void*)0},{&g_224,&g_224,&l_297,&l_297},{(void*)0,&g_224,&g_224,(void*)0}};
                    int i, j;
                    l_293[3] = func_37(func_37(&l_258, l_291), l_292);
                    if ((*g_228))
                    { /* block id: 108 */
                        if (p_52)
                            break;
                        if (p_52)
                            continue;
                        (*g_227) = &p_52;
                        if (p_52)
                            break;
                    }
                    else
                    { /* block id: 113 */
                        uint16_t **l_301 = &l_300[2];
                        (**g_227) = (safe_sub_func_uint16_t_u_u((((*l_267) = g_92[0][0]) <= ((*g_224) , 1UL)), (((l_296 = (void*)0) == (void*)0) > (((((safe_add_func_uint16_t_u_u((((*l_301) = l_300[2]) == &g_200), (safe_rshift_func_int8_t_s_u(((safe_mul_func_uint8_t_u_u(((safe_add_func_int32_t_s_s(p_52, (l_308[5][0] != &g_224))) , g_133), (**l_292))) && g_270), 1)))) < l_258) , g_92[1][0]) <= (**l_292)) != l_309))));
                        p_52 = 0x1770DDCEL;
                        return p_51;
                    }
                }
                for (g_133 = 5; (g_133 >= 0); g_133 -= 1)
                { /* block id: 124 */
                    uint64_t l_324 = 1UL;
                    int32_t l_326[7];
                    const int32_t **l_337[5];
                    uint64_t l_358[3][6][2] = {{{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL},{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL},{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL}},{{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL},{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL},{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL}},{{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL},{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL},{0x2A3C2728AC29C0D1LL,18446744073709551615UL},{0UL,18446744073709551615UL}}};
                    int16_t l_373 = (-6L);
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_326[i] = 1L;
                    for (i = 0; i < 5; i++)
                        l_337[i] = &g_99[1];
                }
                for (g_121 = (-19); (g_121 > 7); ++g_121)
                { /* block id: 154 */
                    uint16_t l_392 = 65526UL;
                    uint64_t *l_421[8][8] = {{(void*)0,(void*)0,&g_121,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_121,(void*)0,&g_121,(void*)0,(void*)0,(void*)0,&g_121,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0,&g_121,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_121,(void*)0,&g_121,(void*)0,(void*)0,(void*)0,&g_121,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0,&g_121,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_121,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_121,(void*)0,&g_121,(void*)0,(void*)0,(void*)0,&g_121,(void*)0}};
                    int32_t l_422 = 1L;
                    int32_t l_426 = 0L;
                    int32_t l_427 = 0x35FF06FBL;
                    int32_t l_430 = 0L;
                    int32_t l_431 = 0x00AB5CC9L;
                    int32_t l_436 = 0x3A5DBFB5L;
                    int32_t l_438 = 1L;
                    int32_t l_440 = 0xF757C66EL;
                    int32_t l_450 = 0xEF0EC3F2L;
                    int i, j;
                    if ((safe_mul_func_int8_t_s_s(((0x2758ED415E4E6D5ALL < (((p_52 > (safe_add_func_uint64_t_u_u(0UL, g_177))) != (l_392 = (safe_mod_func_int16_t_s_s(((p_52 , g_200) , (safe_add_func_uint64_t_u_u(p_51, (0x9A41F489L || (((safe_mod_func_int64_t_s_s((g_135 = g_123[4]), g_41[5][0][0])) <= g_133) ^ g_41[3][6][0]))))), g_126[4])))) <= 0xBDB6L)) ^ 0xE5A02E60L), g_92[3][0])))
                    { /* block id: 157 */
                        uint64_t l_420 = 18446744073709551610UL;
                        l_331 = (safe_mul_func_float_f_f((safe_mul_func_float_f_f((p_51 != ((*g_228) , ((*g_224) = (((l_273 = (((safe_div_func_uint32_t_u_u((safe_rshift_func_int16_t_s_u((g_177 ^= ((0x52L & (((safe_mul_func_float_f_f((safe_div_func_float_f_f((0x5.F97AF9p+55 == (l_258 = (safe_mul_func_float_f_f(0x8.077E66p-32, (safe_mul_func_float_f_f((safe_add_func_float_f_f(((safe_div_func_float_f_f(((l_258 != (**g_223)) , (safe_add_func_float_f_f((safe_add_func_float_f_f((&l_269[0] != g_417), 0x5.A37D3Ap-43)), p_52))), 0x9.0FA34Fp+26)) < p_51), l_420)), (**g_223))))))), p_51)), 0x0.Bp-1)) , p_51) != p_52)) , (*l_314))), 4)), l_331)) >= 0x57E2185CL) , (void*)0)) == l_421[1][0]) > p_52)))), p_51)), 0xE.49803Cp-71));
                    }
                    else
                    { /* block id: 163 */
                        int16_t l_424 = (-5L);
                        int32_t l_428 = (-9L);
                        int32_t l_429 = 0xD6430BFFL;
                        int32_t l_432 = (-1L);
                        int32_t l_433 = 0L;
                        int32_t l_437 = 0x44B05EEBL;
                        int32_t l_439 = 0L;
                        int32_t l_442 = (-8L);
                        int32_t l_444[4] = {(-5L),(-5L),(-5L),(-5L)};
                        int i;
                        ++l_445;
                        ++l_451;
                        (*g_227) = (p_51 , &l_327[0]);
                        (*l_286) = p_51;
                    }
                    for (g_177 = 0; (g_177 <= 0); g_177 += 1)
                    { /* block id: 171 */
                        int16_t *l_456 = &g_123[1];
                        int i;
                        (*l_314) |= (safe_mod_func_int16_t_s_s((-1L), ((*l_456) = g_126[(g_177 + 2)])));
                    }
                }
            }
        }
    }
    else
    { /* block id: 178 */
        const uint16_t l_457 = 0x4740L;
        uint64_t *l_460 = &g_121;
        uint8_t l_470 = 0x9FL;
        int64_t *l_471 = (void*)0;
        int64_t *l_472 = &g_135;
        int32_t *l_473 = (void*)0;
        int32_t l_517 = 2L;
        int32_t l_519 = 0x7F8A7A63L;
        int32_t l_520 = 0x196074E6L;
        int32_t l_521 = 8L;
        int32_t l_523 = 0L;
        int32_t l_524 = 3L;
        (*g_228) ^= l_457;
        if (((((*g_228) = 0x6CEC3D57L) || (safe_sub_func_uint16_t_u_u((l_460 == l_460), (safe_sub_func_int64_t_s_s((l_463 != l_457), ((*l_472) ^= (((1L && (((safe_rshift_func_int16_t_s_s((((g_466[2] != &g_467) > (safe_mod_func_int8_t_s_s(((*l_265) = l_331), l_457))) & l_463), l_470)) > p_52) < g_196)) && g_126[4]) <= (-5L)))))))) <= l_457))
        { /* block id: 183 */
lbl_474:
            (*g_227) = l_473;
        }
        else
        { /* block id: 185 */
            uint16_t **l_494[3];
            int32_t l_507[1][8][10] = {{{0xB5BA8775L,(-1L),0xA443AD81L,0x0E1F32FCL,0x0E1F32FCL,0xA443AD81L,(-1L),0xB5BA8775L,1L,0x60EA6CC2L},{0x38FDF035L,0x0E1F32FCL,(-1L),0x2B8D7A3DL,0x0545102FL,(-6L),0x0545102FL,0x2B8D7A3DL,(-1L),0x0E1F32FCL},{0xA443AD81L,0L,(-1L),0x0545102FL,0x722F9254L,0x2B8D7A3DL,0xB5BA8775L,0xB5BA8775L,0x2B8D7A3DL,0x722F9254L},{0x60EA6CC2L,0xA443AD81L,0xA443AD81L,0x60EA6CC2L,0x723A1F39L,0x2B8D7A3DL,0x38FDF035L,(-1L),1L,0x723A1F39L},{0x60EA6CC2L,0xB5BA8775L,0x38FDF035L,0x723A1F39L,0x38FDF035L,0xB5BA8775L,0x60EA6CC2L,0x0545102FL,1L,0x722F9254L},{0x0545102FL,0L,(-6L),(-1L),0x60EA6CC2L,0x60EA6CC2L,(-1L),(-6L),0L,0x0545102FL},{0x2B8D7A3DL,0L,1L,0x0E1F32FCL,0x723A1F39L,0x722F9254L,0x60EA6CC2L,0x722F9254L,0x723A1F39L,0x0E1F32FCL},{0x0E1F32FCL,0xB5BA8775L,0x0E1F32FCL,0L,0x723A1F39L,0xA443AD81L,0x0545102FL,0x38FDF035L,0x38FDF035L,0x0545102FL}}};
            uint16_t l_559 = 1UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_494[i] = (void*)0;
            p_52 |= (*g_228);
            if (l_470)
                goto lbl_474;
            for (g_156 = (-1); (g_156 < (-26)); g_156--)
            { /* block id: 190 */
                const uint64_t *l_486[2][3][9] = {{{&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121}},{{&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121},{&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121}}};
                int32_t l_512 = 0L;
                int32_t l_522 = 0xA5636752L;
                int32_t l_525 = (-9L);
                uint64_t ***l_535 = &g_533[3][0][2];
                uint64_t **l_537 = &l_460;
                uint64_t ***l_536 = &l_537;
                uint32_t l_552 = 0x639893BEL;
                int i, j, k;
            }
        }
        return (*g_224);
    }
    return (**g_223);
}


/* ------------------------------------------ */
/* 
 * reads : g_41 g_156 g_198 g_93 g_40 g_123 g_92 g_228
 * writes: g_228 g_198 g_93 g_196 g_41
 */
static uint8_t  func_55(const uint16_t  p_56, const uint8_t  p_57, int32_t ** p_58, const int32_t  p_59)
{ /* block id: 80 */
    int32_t **l_229[1];
    int32_t **l_230 = &g_228;
    uint32_t *l_232 = &g_198;
    uint8_t *l_251 = &g_196;
    int i;
    for (i = 0; i < 1; i++)
        l_229[i] = (void*)0;
    (*l_230) = func_37(((*l_230) = (void*)0), (p_56 , (void*)0));
lbl_252:
    (*g_40) = (((*l_251) = (~(l_232 == ((safe_sub_func_uint16_t_u_u((safe_div_func_int32_t_s_s(0x659339ABL, ((*l_232) |= (safe_add_func_int8_t_s_s(p_59, (safe_rshift_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(g_41[2][6][1], 0x5DL)) , g_156), 2))))))), (safe_sub_func_int32_t_s_s((g_93 ^= (safe_mod_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u(p_59, (safe_lshift_func_uint16_t_u_u(0x730BL, 13)))), p_59))), 0xE244C01EL)))) , &g_198)))) ^ p_57);
    (**p_58) = (g_123[4] & g_92[0][0]);
    if (g_198)
        goto lbl_252;
    return p_57;
}


/* ------------------------------------------ */
/* 
 * reads : g_92 g_93 g_41 g_98 g_97 g_42 g_40 g_133 g_126 g_121 g_156 g_99 g_123 g_29 g_100 g_216 g_223
 * writes: g_92 g_93 g_97 g_42 g_100 g_41 g_121 g_135 g_156 g_123 g_177 g_196 g_126 g_198 g_200 g_202 g_216
 */
static int64_t  func_62(const uint64_t  p_63, int32_t ** p_64)
{ /* block id: 8 */
    const int8_t l_108[9][9][3] = {{{(-6L),(-1L),0x75L},{8L,3L,0x20L},{0x0CL,(-1L),(-1L)},{0x67L,1L,0xE4L},{(-1L),(-1L),1L},{(-6L),3L,(-8L)},{0x6FL,(-1L),0x20L},{1L,6L,0x05L},{1L,1L,(-6L)}},{{0x6FL,0xE4L,(-1L)},{(-6L),0xE2L,0L},{(-1L),0x3FL,0x20L},{0x67L,(-6L),0L},{0x0CL,1L,(-1L)},{8L,1L,(-6L)},{(-6L),(-8L),0x05L},{0x20L,(-8L),0x20L},{1L,1L,(-8L)}},{{0xBCL,1L,1L},{0xFAL,(-6L),0xE4L},{(-6L),0x3FL,(-1L)},{0xFAL,0xE2L,0x20L},{0xBCL,0xE4L,0x75L},{1L,1L,6L},{0x20L,6L,6L},{(-6L),(-1L),0x75L},{8L,3L,0x20L}},{{0x0CL,(-1L),(-1L)},{0x67L,1L,0xE4L},{(-1L),(-1L),1L},{(-6L),3L,(-8L)},{0x6FL,(-1L),(-1L)},{(-6L),0x20L,(-8L)},{(-6L),(-6L),(-1L)},{(-8L),8L,0xFAL},{(-1L),0x0CL,3L}},{{0x75L,0x67L,(-1L)},{(-1L),(-1L),3L},{0xE4L,(-6L),0xFAL},{0L,0x6FL,(-1L)},{(-1L),1L,(-8L)},{(-1L),1L,(-1L)},{1L,0x6FL,0x3FL},{6L,(-6L),0x6FL},{0x05L,(-1L),8L}},{{(-1L),0x67L,(-1L)},{0x05L,0x0CL,(-1L)},{6L,8L,0xE2L},{1L,(-6L),0x20L},{(-1L),0x20L,0x20L},{(-1L),1L,0xE2L},{0L,0xBCL,(-1L)},{0xE4L,0xFAL,(-1L)},{(-1L),(-6L),8L}},{{0x75L,0xFAL,0x6FL},{(-1L),0xBCL,0x3FL},{(-8L),1L,(-1L)},{(-6L),0x20L,(-8L)},{(-6L),(-6L),(-1L)},{(-8L),8L,0xFAL},{(-1L),0x0CL,3L},{0x75L,0x67L,(-1L)},{(-1L),(-1L),3L}},{{0xE4L,(-6L),0xFAL},{0L,0x6FL,(-1L)},{(-1L),1L,(-8L)},{(-1L),1L,(-1L)},{1L,0x6FL,0x3FL},{6L,(-6L),0x6FL},{0x05L,(-1L),8L},{(-1L),0x67L,(-1L)},{0x05L,0x0CL,(-1L)}},{{6L,8L,0xE2L},{1L,(-6L),0x20L},{(-1L),0x20L,0x20L},{(-1L),1L,0xE2L},{0L,0xBCL,(-1L)},{0xE4L,0xFAL,(-1L)},{(-1L),(-6L),8L},{0x75L,0xFAL,0x6FL},{(-1L),0xBCL,0x3FL}}};
    int32_t **l_111[6];
    float *l_116 = &g_86;
    const int32_t **l_118 = &g_99[1];
    int32_t l_124 = 0xB3EA340AL;
    int32_t l_127 = 1L;
    uint8_t l_218 = 255UL;
    int64_t l_225 = 0x278B1CBCA9DBBE3BLL;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_111[i] = (void*)0;
    for (g_92[3][0] = 0; (g_92[3][0] <= 3); g_92[3][0] += 1)
    { /* block id: 11 */
        int32_t * const *l_95[7][3][1] = {{{&g_40},{(void*)0},{&g_40}},{{&g_40},{(void*)0},{&g_40}},{{&g_40},{(void*)0},{&g_40}},{{&g_40},{(void*)0},{&g_40}},{{&g_40},{(void*)0},{&g_40}},{{&g_40},{(void*)0},{&g_40}},{{&g_40},{(void*)0},{&g_40}}};
        uint8_t l_128 = 0xB0L;
        uint32_t *l_203 = &g_126[4];
        int i, j, k;
        for (g_93 = 0; (g_93 <= 3); g_93 += 1)
        { /* block id: 14 */
            const int64_t l_94[10][7][3] = {{{0x330F0864F5ABE080LL,0x37C2AE6BA1441E23LL,(-1L)},{0xC22C9C813C75F927LL,1L,0x8D6BFD67550B1092LL},{0x8FDA5E0CF2F28949LL,0x274D830BE999AA20LL,(-5L)},{(-3L),1L,0x5CAB9BE056EFF523LL},{0xBB161CA2EBD359D2LL,0x37C2AE6BA1441E23LL,0x37C2AE6BA1441E23LL},{0L,(-1L),0x40197F5CE9472023LL},{0L,0x37C2AE6BA1441E23LL,1L}},{{(-7L),(-7L),0x98811FBA6500C247LL},{(-1L),0x0B5F9D9D8AD31AC0LL,5L},{(-4L),(-7L),(-4L)},{0x37C2AE6BA1441E23LL,(-1L),(-1L)},{1L,0x98811FBA6500C247LL,(-2L)},{0x29C6149AF40839CALL,(-1L),5L},{0x8D6BFD67550B1092LL,(-7L),(-7L)}},{{0x1BA6C3C0C88640E9LL,0x0B5F9D9D8AD31AC0LL,0x08984295AD7CBC17LL},{0x40197F5CE9472023LL,(-7L),(-7L)},{0x274D830BE999AA20LL,(-1L),0x0B5F9D9D8AD31AC0LL},{(-1L),0x98811FBA6500C247LL,(-1L)},{(-5L),(-1L),1L},{1L,(-7L),(-1L)},{1L,0x0B5F9D9D8AD31AC0LL,0xC290A7FA1A718AF6LL}},{{0xE24EB764FAD62130LL,(-7L),0x10A650A9706E1857LL},{(-1L),(-1L),(-1L)},{0x5CAB9BE056EFF523LL,0x98811FBA6500C247LL,0x6A58E0DE0C47FC0BLL},{0x78952EB5F757037ALL,(-1L),0x698822BDA668E547LL},{(-1L),(-7L),0x98811FBA6500C247LL},{(-1L),0x0B5F9D9D8AD31AC0LL,5L},{(-4L),(-7L),(-4L)}},{{0x37C2AE6BA1441E23LL,(-1L),(-1L)},{1L,0x98811FBA6500C247LL,(-2L)},{0x29C6149AF40839CALL,(-1L),5L},{0x8D6BFD67550B1092LL,(-7L),(-7L)},{0x1BA6C3C0C88640E9LL,0x0B5F9D9D8AD31AC0LL,0x08984295AD7CBC17LL},{0x40197F5CE9472023LL,(-7L),(-7L)},{0x274D830BE999AA20LL,(-1L),0x0B5F9D9D8AD31AC0LL}},{{(-1L),0x98811FBA6500C247LL,(-1L)},{(-5L),(-1L),1L},{1L,(-7L),(-1L)},{1L,0x0B5F9D9D8AD31AC0LL,0xC290A7FA1A718AF6LL},{0xE24EB764FAD62130LL,(-7L),0x10A650A9706E1857LL},{(-1L),(-1L),(-1L)},{0x5CAB9BE056EFF523LL,0x98811FBA6500C247LL,0x6A58E0DE0C47FC0BLL}},{{0x78952EB5F757037ALL,(-1L),0x698822BDA668E547LL},{(-1L),(-7L),0x98811FBA6500C247LL},{(-1L),0x0B5F9D9D8AD31AC0LL,5L},{(-4L),(-7L),(-4L)},{0x37C2AE6BA1441E23LL,(-1L),(-1L)},{1L,0x98811FBA6500C247LL,(-2L)},{0x29C6149AF40839CALL,(-1L),5L}},{{0x8D6BFD67550B1092LL,(-7L),(-7L)},{0x1BA6C3C0C88640E9LL,0x0B5F9D9D8AD31AC0LL,0x08984295AD7CBC17LL},{0x40197F5CE9472023LL,(-7L),(-7L)},{0x274D830BE999AA20LL,(-1L),0x0B5F9D9D8AD31AC0LL},{(-1L),0x98811FBA6500C247LL,(-1L)},{(-5L),(-1L),1L},{1L,(-7L),(-1L)}},{{1L,0x0B5F9D9D8AD31AC0LL,0xC290A7FA1A718AF6LL},{0xE24EB764FAD62130LL,(-7L),0x10A650A9706E1857LL},{(-1L),(-1L),(-1L)},{0x5CAB9BE056EFF523LL,0x98811FBA6500C247LL,0x6A58E0DE0C47FC0BLL},{0x78952EB5F757037ALL,(-1L),0x698822BDA668E547LL},{(-1L),(-7L),0x98811FBA6500C247LL},{(-1L),0x0B5F9D9D8AD31AC0LL,5L}},{{(-4L),(-7L),(-4L)},{0x37C2AE6BA1441E23LL,(-1L),(-1L)},{1L,0x98811FBA6500C247LL,(-2L)},{0x29C6149AF40839CALL,(-1L),5L},{0x8D6BFD67550B1092LL,(-7L),(-7L)},{0x1BA6C3C0C88640E9LL,0x0B5F9D9D8AD31AC0LL,0x08984295AD7CBC17LL},{0x40197F5CE9472023LL,(-7L),(-7L)}}};
            int32_t * const *l_96 = &g_42;
            const float *l_119 = &g_86;
            uint8_t l_153 = 0UL;
            int32_t l_158[3];
            uint8_t l_217[4];
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_158[i] = 0xBC4FE304L;
            for (i = 0; i < 4; i++)
                l_217[i] = 0x44L;
            if (l_94[5][1][2])
                break;
            l_96 = l_95[3][1][0];
            for (g_97 = 0; (g_97 <= 3); g_97 += 1)
            { /* block id: 19 */
                const int32_t ***l_117 = &g_98[2][5][1];
                uint8_t *l_129 = &l_128;
                int32_t l_130 = 0x199855EAL;
                int i, j, k;
                if (g_41[(g_93 + 4)][(g_93 + 3)][g_92[3][0]])
                { /* block id: 20 */
                    (*p_64) = func_37(&g_97, g_98[2][5][1]);
                    for (g_100 = 0; (g_100 <= 3); g_100 += 1)
                    { /* block id: 24 */
                        int i, j, k;
                        g_41[(g_92[3][0] + 4)][g_97][g_93] |= 0xFFFF0E59L;
                        return g_41[(g_92[3][0] + 4)][g_97][g_93];
                    }
                    (*g_40) = (**p_64);
                    return p_63;
                }
                else
                { /* block id: 30 */
                    int32_t **l_110 = &g_42;
                    int32_t ***l_109[2][2][8] = {{{&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110},{&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110}},{{(void*)0,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,(void*)0},{(void*)0,&l_110,(void*)0,&l_110,&l_110,&l_110,&l_110,&l_110}}};
                    uint64_t *l_120[2];
                    int16_t *l_122[3];
                    uint32_t *l_125[9] = {&g_126[4],&g_126[4],&g_126[4],&g_126[4],&g_126[4],&g_126[4],&g_126[4],&g_126[4],&g_126[4]};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_120[i] = &g_121;
                    for (i = 0; i < 3; i++)
                        l_122[i] = &g_123[4];
                    if ((**p_64))
                        break;
                    g_100 = ((*g_40) = ((l_127 = (safe_lshift_func_int16_t_s_u((l_124 = (g_97 , (((safe_rshift_func_uint8_t_u_u((!(safe_sub_func_uint64_t_u_u((g_121 = ((l_108[1][2][0] , ((((((l_111[3] = &g_42) == p_64) , func_37(&g_41[2][4][0], ((safe_mul_func_int16_t_s_s((((((safe_sub_func_int16_t_s_s(0xB234L, (l_116 != (void*)0))) & (l_117 != l_117)) < 0x1AB63E39L) > 0xF4E0B4B7F8BE070ELL) , 9L), 0x0FC2L)) , l_118))) != l_119) < 9L) , (void*)0)) != (void*)0)), 0x3CF297919FD84452LL))), g_41[3][1][0])) != 0xCA65C328A76B0EF2LL) , p_63))), 7))) && g_41[6][0][0]));
                }
                l_130 ^= (g_41[6][2][0] != ((*l_129) = (((void*)0 == (*p_64)) != l_128)));
            }
            if ((*g_42))
                continue;
            for (l_128 = 0; (l_128 <= 3); l_128 += 1)
            { /* block id: 45 */
                int64_t *l_134 = &g_135;
                int64_t *l_154 = (void*)0;
                int64_t *l_155 = (void*)0;
                int32_t l_157 = (-2L);
                uint32_t l_226 = 0xFF088BA0L;
                int i, j, k;
                l_158[1] = ((*g_40) = ((g_41[(g_92[3][0] + 2)][l_128][g_92[3][0]] > ((safe_rshift_func_uint16_t_u_u((((g_133 || ((*l_134) = 0x44DF11EF485BC65ALL)) ^ (~(safe_mul_func_uint8_t_u_u((((safe_mod_func_int64_t_s_s(((safe_div_func_int8_t_s_s((((((safe_mod_func_int32_t_s_s(((!(p_63 >= g_126[4])) >= ((safe_add_func_uint8_t_u_u(((g_156 &= (+(g_41[(g_92[3][0] + 2)][l_128][g_92[3][0]] ^ ((p_63 , (safe_add_func_int64_t_s_s((safe_lshift_func_int8_t_s_u(l_153, 7)), (g_121 , p_63)))) == p_63)))) <= p_63), 0x5AL)) == 7UL)), (*g_42))) , &p_64) == &l_118) >= p_63) & (-2L)), l_157)) , p_63), 9L)) ^ 1UL) == l_157), (**l_118))))) || p_63), 0)) && 2L)) != l_157));
                for (l_127 = 9; (l_127 > 6); l_127 = safe_sub_func_uint32_t_u_u(l_127, 8))
                { /* block id: 52 */
                    int16_t *l_175 = (void*)0;
                    int16_t *l_176 = &g_177;
                    int32_t l_178 = (-4L);
                    int32_t * const **l_193 = &l_95[3][1][0];
                    uint8_t *l_194 = &l_153;
                    uint8_t *l_195 = &g_196;
                    uint32_t *l_197 = &g_126[4];
                    uint16_t *l_199 = &g_200;
                    int8_t *l_201 = &g_202;
                    if (((*g_42) = ((g_133 && ((safe_mod_func_int8_t_s_s(((*l_201) = ((safe_div_func_uint32_t_u_u(0xF0A2765BL, (safe_rshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s((safe_sub_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s((g_123[7] |= 0x3846L), (l_178 = ((*l_176) = p_63)))) == (safe_lshift_func_uint16_t_u_s(((*l_199) = (g_198 = ((((p_63 || (safe_add_func_uint64_t_u_u((((*l_197) &= (((*l_195) = ((*l_194) = ((-1L) ^ (safe_mul_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(((safe_mul_func_float_f_f((safe_div_func_float_f_f(g_41[7][1][0], (g_29 > ((((*l_193) = &g_40) == (void*)0) != 0x7.8p+1)))), p_63)) , p_63), g_29)), (-5L)))))) != l_128)) , g_121), p_63))) == 0L) > (**p_64)) ^ 0x82C662BAD161D8D2LL))), g_100))), (*g_42))), 4)), 0x8CL)), 12)))) , 0x30L)), p_63)) == (**p_64))) || 0x5C2F4BCEL)))
                    { /* block id: 64 */
                        int32_t ***l_206 = &l_111[5];
                        float *l_212 = &g_86;
                        float **l_213 = (void*)0;
                        float **l_214 = &l_116;
                        uint32_t *l_215 = &g_216;
                        (**p_64) = (((((((void*)0 != l_203) > (safe_rshift_func_int8_t_s_s(((((l_178 || g_92[3][0]) > (g_92[3][0] , (&g_98[6][5][0] == l_206))) & 65529UL) != (((+((((safe_mul_func_int8_t_s_s((safe_add_func_int32_t_s_s(((((*l_215) ^= (((*l_214) = l_212) == (void*)0)) , l_194) != &l_108[6][8][1]), 1L)), 0xDFL)) , 0xD2L) < g_29) | l_217[2])) , (**l_118)) >= (**l_118))), g_100))) < (-1L)) , (**p_64)) == 0xD3D55A55L) <= p_63);
                        l_157 &= ((g_156 || (g_133 && p_63)) > ((0UL ^ ((((((l_218 , (((safe_sub_func_int16_t_s_s(((1L | 4294967295UL) && p_63), (safe_sub_func_int32_t_s_s(0L, g_92[3][0])))) ^ p_63) , &l_119)) == g_223) <= p_63) != g_123[6]) != p_63) ^ 1L)) ^ g_123[4]));
                        if ((**p_64))
                            continue;
                        return p_63;
                    }
                    else
                    { /* block id: 71 */
                        return l_225;
                    }
                }
                return l_226;
            }
        }
    }
    return p_63;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_4[i][j], "g_4[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_29, "g_29", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_41[i][j][k], "g_41[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_86, sizeof(g_86), "g_86", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_92[i][j], "g_92[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_123[i], "g_123[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_126[i], "g_126[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_133, "g_133", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc(g_177, "g_177", print_hash_value);
    transparent_crc(g_196, "g_196", print_hash_value);
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_200, "g_200", print_hash_value);
    transparent_crc(g_202, "g_202", print_hash_value);
    transparent_crc(g_216, "g_216", print_hash_value);
    transparent_crc(g_270, "g_270", print_hash_value);
    transparent_crc_bytes (&g_353, sizeof(g_353), "g_353", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_419[i], "g_419[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_493, "g_493", print_hash_value);
    transparent_crc(g_561, "g_561", print_hash_value);
    transparent_crc(g_565.f0, "g_565.f0", print_hash_value);
    transparent_crc(g_638, "g_638", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_770[i], "g_770[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_827.f0, "g_827.f0", print_hash_value);
    transparent_crc(g_938, "g_938", print_hash_value);
    transparent_crc(g_1169.f0, "g_1169.f0", print_hash_value);
    transparent_crc_bytes (&g_1184, sizeof(g_1184), "g_1184", print_hash_value);
    transparent_crc(g_1212, "g_1212", print_hash_value);
    transparent_crc(g_1391.f0, "g_1391.f0", print_hash_value);
    transparent_crc(g_1560, "g_1560", print_hash_value);
    transparent_crc(g_1584, "g_1584", print_hash_value);
    transparent_crc(g_1657.f0, "g_1657.f0", print_hash_value);
    transparent_crc(g_1659.f0, "g_1659.f0", print_hash_value);
    transparent_crc(g_1660.f0, "g_1660.f0", print_hash_value);
    transparent_crc(g_1661.f0, "g_1661.f0", print_hash_value);
    transparent_crc(g_1662.f0, "g_1662.f0", print_hash_value);
    transparent_crc(g_1663.f0, "g_1663.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1753[i], "g_1753[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1769[i], "g_1769[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1811, "g_1811", print_hash_value);
    transparent_crc(g_1854, "g_1854", print_hash_value);
    transparent_crc(g_1875, "g_1875", print_hash_value);
    transparent_crc(g_1876, "g_1876", print_hash_value);
    transparent_crc(g_1877, "g_1877", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1945[i], "g_1945[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2004[i], "g_2004[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2047, "g_2047", print_hash_value);
    transparent_crc(g_2089, "g_2089", print_hash_value);
    transparent_crc(g_2095, "g_2095", print_hash_value);
    transparent_crc(g_2173.f0, "g_2173.f0", print_hash_value);
    transparent_crc(g_2362, "g_2362", print_hash_value);
    transparent_crc(g_2435, "g_2435", print_hash_value);
    transparent_crc_bytes (&g_2447, sizeof(g_2447), "g_2447", print_hash_value);
    transparent_crc(g_2503, "g_2503", print_hash_value);
    transparent_crc_bytes (&g_2513, sizeof(g_2513), "g_2513", print_hash_value);
    transparent_crc(g_2665, "g_2665", print_hash_value);
    transparent_crc_bytes (&g_2779, sizeof(g_2779), "g_2779", print_hash_value);
    transparent_crc(g_2810, "g_2810", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_2813[i][j][k], "g_2813[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2862, "g_2862", print_hash_value);
    transparent_crc(g_2905, "g_2905", print_hash_value);
    transparent_crc(g_3030, "g_3030", print_hash_value);
    transparent_crc_bytes (&g_3060, sizeof(g_3060), "g_3060", print_hash_value);
    transparent_crc(g_3130, "g_3130", print_hash_value);
    transparent_crc(g_3255, "g_3255", print_hash_value);
    transparent_crc(g_3292, "g_3292", print_hash_value);
    transparent_crc(g_3312, "g_3312", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_3389[i], "g_3389[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3526, "g_3526", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_3824[i][j][k], "g_3824[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3858, "g_3858", print_hash_value);
    transparent_crc(g_3949.f0, "g_3949.f0", print_hash_value);
    transparent_crc(g_4017, "g_4017", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1092
XXX total union variables: 1

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 337
   depth: 2, occurrence: 69
   depth: 3, occurrence: 11
   depth: 4, occurrence: 5
   depth: 5, occurrence: 4
   depth: 6, occurrence: 6
   depth: 7, occurrence: 3
   depth: 10, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 6
   depth: 18, occurrence: 4
   depth: 19, occurrence: 3
   depth: 20, occurrence: 6
   depth: 21, occurrence: 5
   depth: 22, occurrence: 5
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 6
   depth: 27, occurrence: 1
   depth: 28, occurrence: 5
   depth: 29, occurrence: 4
   depth: 31, occurrence: 1
   depth: 32, occurrence: 3
   depth: 33, occurrence: 4
   depth: 34, occurrence: 3
   depth: 41, occurrence: 1
   depth: 46, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 879

XXX times a variable address is taken: 2280
XXX times a pointer is dereferenced on RHS: 367
breakdown:
   depth: 1, occurrence: 267
   depth: 2, occurrence: 92
   depth: 3, occurrence: 3
   depth: 4, occurrence: 4
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 551
breakdown:
   depth: 1, occurrence: 511
   depth: 2, occurrence: 37
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 79
XXX times a pointer is compared with address of another variable: 26
XXX times a pointer is compared with another pointer: 22
XXX times a pointer is qualified to be dereferenced: 19265

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1904
   level: 2, occurrence: 488
   level: 3, occurrence: 139
   level: 4, occurrence: 95
   level: 5, occurrence: 49
XXX number of pointers point to pointers: 427
XXX number of pointers point to scalars: 442
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32
XXX average alias set size: 1.47

XXX times a non-volatile is read: 3100
XXX times a non-volatile is write: 1604
XXX times a volatile is read: 33
XXX    times read thru a pointer: 31
XXX times a volatile is write: 3
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 409
XXX percentage of non-volatile access: 99.2

XXX forward jumps: 5
XXX backward jumps: 19

XXX stmts: 332
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 27
   depth: 2, occurrence: 43
   depth: 3, occurrence: 44
   depth: 4, occurrence: 82
   depth: 5, occurrence: 108

XXX percentage a fresh-made variable is used: 14.8
XXX percentage an existing variable is used: 85.2
********************* end of statistics **********************/


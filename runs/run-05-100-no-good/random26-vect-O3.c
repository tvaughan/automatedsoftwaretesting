/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1010872780
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   const int32_t  f0;
   int64_t  f1;
   const float  f2;
   const int16_t  f3;
   signed f4 : 3;
};

union U1 {
   uint8_t  f0;
   uint32_t  f1;
   uint16_t  f2;
   int32_t  f3;
   uint16_t  f4;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0L;
static uint8_t g_4 = 0x4BL;
static float * volatile g_5 = (void*)0;/* VOLATILE GLOBAL g_5 */
static float g_7[6] = {0xF.ECAFE8p-56,0xA.BFC393p-72,0xF.ECAFE8p-56,0xF.ECAFE8p-56,0xA.BFC393p-72,0xF.ECAFE8p-56};
static float * volatile g_6 = &g_7[4];/* VOLATILE GLOBAL g_6 */
static int32_t g_40 = 0x854668DAL;
static uint32_t g_53 = 4294967295UL;
static uint64_t g_66 = 0UL;
static int32_t g_70 = 0x2C102D1FL;
static int32_t g_77[7][5] = {{(-1L),0xBF624E19L,8L,0xFA57A880L,0xBCB8161DL},{0xFA57A880L,0xC20E5E2EL,0x1B4ADC7CL,0L,8L},{0xE4064544L,(-1L),0xFA57A880L,0xFA57A880L,(-1L)},{0xBAB2BD68L,(-9L),0x95A4792DL,0x942075D7L,(-1L)},{0xC20E5E2EL,1L,0xBF624E19L,0x9A628767L,8L},{(-9L),0x9A628767L,7L,0xBCB8161DL,0xBCB8161DL},{0xC20E5E2EL,0x95A4792DL,0xC20E5E2EL,7L,0xE3B61EA5L}};
static volatile int64_t g_85 = (-1L);/* VOLATILE GLOBAL g_85 */
static volatile int8_t g_86[7] = {4L,4L,4L,4L,4L,4L,4L};
static int8_t g_87 = 0x36L;
static int16_t g_88[2] = {(-1L),(-1L)};
static volatile uint32_t g_89 = 8UL;/* VOLATILE GLOBAL g_89 */
static int32_t *g_107 = &g_70;
static int8_t g_127 = 0x7CL;
static uint16_t g_130 = 0xDFA9L;
static float *g_134 = &g_7[4];
static float **g_133[7][5][7] = {{{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{(void*)0,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,(void*)0,&g_134},{(void*)0,&g_134,&g_134,&g_134,&g_134,(void*)0,&g_134}},{{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,(void*)0,&g_134,&g_134,&g_134,&g_134},{(void*)0,&g_134,(void*)0,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134}},{{(void*)0,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,(void*)0,&g_134},{(void*)0,&g_134,&g_134,&g_134,&g_134,(void*)0,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134}},{{&g_134,&g_134,(void*)0,&g_134,&g_134,&g_134,&g_134},{(void*)0,&g_134,(void*)0,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{(void*)0,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134}},{{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,(void*)0,&g_134},{(void*)0,&g_134,&g_134,&g_134,&g_134,(void*)0,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,(void*)0,&g_134,&g_134,&g_134,&g_134}},{{(void*)0,&g_134,(void*)0,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,(void*)0},{(void*)0,&g_134,&g_134,(void*)0,&g_134,&g_134,(void*)0},{&g_134,(void*)0,&g_134,&g_134,(void*)0,&g_134,&g_134},{(void*)0,(void*)0,&g_134,&g_134,&g_134,&g_134,&g_134}},{{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,(void*)0,(void*)0,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134,&g_134,&g_134}}};
static int16_t g_164 = 0x03A9L;
static float g_171 = 0x2.091717p+36;
static uint8_t g_172[8] = {0xD7L,0xD7L,255UL,0xD7L,0xD7L,255UL,0xD7L,0xD7L};
static volatile int8_t g_177 = 0xCCL;/* VOLATILE GLOBAL g_177 */
static volatile uint32_t g_179 = 0x61E7CC75L;/* VOLATILE GLOBAL g_179 */
static uint8_t g_186[9][10][2] = {{{0UL,255UL},{0x72L,0xACL},{1UL,0xACL},{0x72L,255UL},{0UL,0x9FL},{9UL,247UL},{0x4EL,0x9FL},{0xC6L,255UL},{0xDCL,0xACL},{0xBEL,0xACL}},{{0xDCL,255UL},{0xC6L,0x9FL},{0x4EL,247UL},{9UL,0x9FL},{0UL,255UL},{0x72L,0xACL},{1UL,0xACL},{0x72L,255UL},{0UL,0x9FL},{9UL,247UL}},{{0x4EL,0x9FL},{0xC6L,255UL},{0xDCL,0xACL},{0xBEL,0xACL},{0xDCL,255UL},{0xC6L,0x9FL},{0x4EL,247UL},{9UL,0x9FL},{0UL,255UL},{0x72L,0xACL}},{{1UL,0xACL},{0x72L,255UL},{0UL,0x9FL},{9UL,247UL},{0x4EL,0x9FL},{0xC6L,255UL},{0xDCL,0xACL},{0xBEL,0xACL},{0xDCL,255UL},{0xC6L,0x9FL}},{{0x4EL,247UL},{9UL,0x9FL},{0UL,255UL},{0x72L,0xACL},{1UL,0xACL},{0x72L,255UL},{0UL,0x9FL},{9UL,247UL},{0x4EL,0x9FL},{0xC6L,255UL}},{{0xDCL,0xACL},{0xBEL,0xACL},{0xDCL,255UL},{0xC6L,0x9FL},{0x4EL,247UL},{9UL,0x9FL},{0UL,255UL},{0x72L,0xACL},{1UL,0xACL},{0x72L,255UL}},{{0UL,0x9FL},{9UL,247UL},{0xDCL,247UL},{0x70L,0xC0L},{1UL,0x9FL},{0x38L,0x9FL},{1UL,0xC0L},{0x70L,247UL},{0xDCL,0x81L},{0x72L,247UL}},{{1UL,0xC0L},{0xBEL,0x9FL},{0xF5L,0x9FL},{0xBEL,0xC0L},{1UL,247UL},{0x72L,0x81L},{0xDCL,247UL},{0x70L,0xC0L},{1UL,0x9FL},{0x38L,0x9FL}},{{1UL,0xC0L},{0x70L,247UL},{0xDCL,0x81L},{0x72L,247UL},{1UL,0xC0L},{0xBEL,0x9FL},{0xF5L,0x9FL},{0xBEL,0xC0L},{1UL,247UL},{0x72L,0x81L}}};
static int32_t ** volatile g_190 = &g_107;/* VOLATILE GLOBAL g_190 */
static volatile uint16_t g_232 = 7UL;/* VOLATILE GLOBAL g_232 */
static int32_t g_238 = (-1L);
static uint32_t g_244 = 0UL;
static int16_t *g_254 = &g_164;
static int64_t g_256[9][1] = {{(-2L)},{(-1L)},{(-1L)},{(-2L)},{(-1L)},{(-1L)},{(-2L)},{(-1L)},{(-1L)}};
static uint32_t g_257 = 4294967295UL;
static uint64_t g_260 = 18446744073709551615UL;
static int32_t ** const  volatile g_264[9][10][2] = {{{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0}},{{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{(void*)0,&g_107}},{{&g_107,&g_107},{&g_107,&g_107},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_107},{&g_107,&g_107},{&g_107,&g_107},{(void*)0,&g_107},{&g_107,&g_107},{&g_107,(void*)0}},{{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107}},{{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107}},{{(void*)0,&g_107},{&g_107,&g_107},{&g_107,&g_107},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_107},{&g_107,&g_107},{&g_107,&g_107},{(void*)0,&g_107},{&g_107,&g_107}},{{&g_107,(void*)0},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107}},{{&g_107,&g_107},{&g_107,(void*)0},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{(void*)0,&g_107},{(void*)0,(void*)0},{&g_107,&g_107}},{{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107},{&g_107,&g_107},{(void*)0,&g_107},{&g_107,&g_107},{(void*)0,&g_107},{&g_107,&g_107},{&g_107,(void*)0},{&g_107,&g_107}}};
static int32_t ** volatile g_265 = &g_107;/* VOLATILE GLOBAL g_265 */
static int32_t ** volatile g_270 = (void*)0;/* VOLATILE GLOBAL g_270 */
static union U1 g_287[8] = {{0x23L},{0x72L},{0x23L},{0x23L},{0x72L},{0x23L},{0x23L},{0x72L}};
static const int16_t g_293 = 1L;
static const int16_t g_295 = 1L;
static const int16_t *g_294 = &g_295;
static uint32_t * volatile * volatile g_324 = (void*)0;/* VOLATILE GLOBAL g_324 */
static uint32_t * volatile * volatile * volatile g_323 = &g_324;/* VOLATILE GLOBAL g_323 */
static const uint8_t g_360 = 255UL;
static float * const  volatile g_430 = &g_171;/* VOLATILE GLOBAL g_430 */
static const int32_t ** volatile g_433 = (void*)0;/* VOLATILE GLOBAL g_433 */
static const int32_t g_435 = 0x5396BA6FL;
static const int8_t g_440 = (-2L);
static const int8_t g_442 = 1L;
static const int8_t *g_441 = &g_442;
static struct S0 g_564 = {0L,0L,-0x1.0p+1,-8L,-0};
static int32_t *g_582 = &g_40;
static int32_t ** volatile g_584 = &g_582;/* VOLATILE GLOBAL g_584 */
static int32_t * volatile g_622 = (void*)0;/* VOLATILE GLOBAL g_622 */
static int32_t * volatile g_623 = &g_70;/* VOLATILE GLOBAL g_623 */
static uint32_t *g_649 = &g_53;
static uint32_t **g_648 = &g_649;
static uint32_t ***g_647[5][8] = {{&g_648,&g_648,(void*)0,&g_648,(void*)0,&g_648,&g_648,&g_648},{&g_648,&g_648,&g_648,&g_648,&g_648,&g_648,&g_648,&g_648},{&g_648,&g_648,&g_648,(void*)0,&g_648,(void*)0,&g_648,&g_648},{&g_648,&g_648,&g_648,&g_648,(void*)0,(void*)0,&g_648,&g_648},{&g_648,&g_648,&g_648,&g_648,&g_648,&g_648,&g_648,&g_648}};
static uint32_t ****g_646 = &g_647[2][4];
static int32_t ** volatile g_667 = &g_582;/* VOLATILE GLOBAL g_667 */
static volatile uint8_t g_694[2][5] = {{0UL,0x26L,9UL,9UL,0x26L},{0UL,0x26L,9UL,9UL,0x26L}};
static volatile uint8_t *g_693 = &g_694[1][3];
static volatile uint8_t * volatile *g_692 = &g_693;
static uint64_t *g_728 = (void*)0;
static uint64_t ** volatile g_727 = &g_728;/* VOLATILE GLOBAL g_727 */
static const uint8_t g_745 = 0xD1L;
static uint8_t *g_753 = &g_287[6].f0;
static volatile uint8_t g_764 = 0UL;/* VOLATILE GLOBAL g_764 */
static volatile int64_t g_801 = (-1L);/* VOLATILE GLOBAL g_801 */
static float g_824 = 0x8.F7D124p-48;
static uint16_t g_828 = 0x3D0BL;
static int16_t * volatile * volatile g_850 = &g_254;/* VOLATILE GLOBAL g_850 */
static int16_t * volatile * volatile *g_849 = &g_850;
static int16_t **g_852 = &g_254;
static int16_t ***g_851 = &g_852;
static uint32_t g_899 = 0UL;
static int8_t *g_1058 = &g_127;
static int8_t ** const g_1057 = &g_1058;
static int8_t ** const *g_1056 = &g_1057;
static int8_t ** const ** volatile g_1055 = &g_1056;/* VOLATILE GLOBAL g_1055 */
static const int32_t *g_1061 = &g_564.f0;
static const int32_t ** volatile g_1060 = &g_1061;/* VOLATILE GLOBAL g_1060 */
static int32_t ** volatile g_1117 = &g_107;/* VOLATILE GLOBAL g_1117 */
static int32_t ** volatile g_1154[7] = {&g_107,&g_107,&g_107,&g_107,&g_107,&g_107,&g_107};
static int32_t ** volatile g_1155 = &g_582;/* VOLATILE GLOBAL g_1155 */
static uint64_t g_1197 = 18446744073709551613UL;
static int64_t g_1298 = 0x557776C49BFFED2BLL;
static uint64_t ***g_1329 = (void*)0;
static uint64_t ****g_1328 = &g_1329;
static uint64_t *****g_1327 = &g_1328;
static uint64_t *g_1336 = &g_260;
static float * const *g_1459 = &g_134;
static float * const **g_1458 = &g_1459;
static float * const ***g_1457 = &g_1458;
static float * const **** volatile g_1456 = &g_1457;/* VOLATILE GLOBAL g_1456 */
static volatile uint32_t g_1475 = 0x65501253L;/* VOLATILE GLOBAL g_1475 */
static int32_t * const **** volatile g_1478 = (void*)0;/* VOLATILE GLOBAL g_1478 */
static int32_t * const *g_1482 = &g_107;
static int32_t * const **g_1481 = &g_1482;
static int32_t * const ***g_1480 = &g_1481;
static int32_t * const **** volatile g_1479 = &g_1480;/* VOLATILE GLOBAL g_1479 */
static uint16_t *g_1557 = &g_130;
static uint16_t ** volatile g_1556[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile uint32_t g_1617[5] = {1UL,1UL,1UL,1UL,1UL};
static int8_t g_1764 = 0x28L;
static volatile uint32_t g_1770 = 0x311929C1L;/* VOLATILE GLOBAL g_1770 */
static struct S0 g_1820[6] = {{-4L,-6L,0x1.7p+1,0x969AL,0},{-4L,-6L,0x1.7p+1,0x969AL,0},{-4L,-6L,0x1.7p+1,0x969AL,0},{-4L,-6L,0x1.7p+1,0x969AL,0},{-4L,-6L,0x1.7p+1,0x969AL,0},{-4L,-6L,0x1.7p+1,0x969AL,0}};
static struct S0 g_1845[2][5] = {{{-1L,0x57B68D3B705C4D99LL,0x9.112A2Fp+28,0L,1},{-5L,0x62334BFAEDFA23B9LL,0x1.Ep+1,0x9C78L,0},{-5L,0x62334BFAEDFA23B9LL,0x1.Ep+1,0x9C78L,0},{-1L,0x57B68D3B705C4D99LL,0x9.112A2Fp+28,0L,1},{-5L,0x62334BFAEDFA23B9LL,0x1.Ep+1,0x9C78L,0}},{{-1L,0x57B68D3B705C4D99LL,0x9.112A2Fp+28,0L,1},{-1L,0x57B68D3B705C4D99LL,0x9.112A2Fp+28,0L,1},{6L,0L,-0x1.Dp-1,0xD6EDL,-0},{-1L,0x57B68D3B705C4D99LL,0x9.112A2Fp+28,0L,1},{-1L,0x57B68D3B705C4D99LL,0x9.112A2Fp+28,0L,1}}};
static int32_t ** volatile g_1965 = (void*)0;/* VOLATILE GLOBAL g_1965 */
static uint32_t g_2150 = 1UL;
static int32_t ** volatile g_2165 = &g_107;/* VOLATILE GLOBAL g_2165 */
static uint64_t g_2168[6][3] = {{0xEFEAE397E131A41FLL,18446744073709551614UL,0xEFEAE397E131A41FLL},{0xEFEAE397E131A41FLL,18446744073709551614UL,0xEFEAE397E131A41FLL},{0xEFEAE397E131A41FLL,18446744073709551614UL,0xEFEAE397E131A41FLL},{0xEFEAE397E131A41FLL,18446744073709551614UL,0xEFEAE397E131A41FLL},{0xEFEAE397E131A41FLL,18446744073709551614UL,0xEFEAE397E131A41FLL},{0xEFEAE397E131A41FLL,18446744073709551614UL,0xEFEAE397E131A41FLL}};
static volatile int32_t g_2201[1][6][5] = {{{5L,5L,5L,5L,5L},{0L,0L,0L,0L,0L},{5L,5L,5L,5L,5L},{0L,0L,0L,0L,0L},{5L,5L,5L,5L,5L},{0L,0L,0L,0L,0L}}};
static const volatile uint32_t *g_2238 = &g_1617[2];
static const volatile uint32_t **g_2237 = &g_2238;
static const int32_t **g_2246 = (void*)0;
static const int32_t ***g_2245 = &g_2246;
static const int32_t ****g_2244 = &g_2245;
static const int32_t *****g_2243[9] = {&g_2244,&g_2244,&g_2244,&g_2244,&g_2244,&g_2244,&g_2244,&g_2244,&g_2244};
static union U1 *g_2370 = &g_287[6];
static union U1 ** const  volatile g_2369 = &g_2370;/* VOLATILE GLOBAL g_2369 */
static int32_t ** volatile g_2397 = &g_582;/* VOLATILE GLOBAL g_2397 */
static const uint32_t *g_2404[3][7] = {{&g_244,&g_53,&g_53,&g_244,&g_53,&g_244,&g_53},{&g_53,&g_53,&g_53,&g_53,&g_53,&g_53,&g_53},{(void*)0,&g_53,&g_257,&g_53,(void*)0,(void*)0,&g_53}};
static const uint32_t **g_2403 = &g_2404[2][2];
static volatile int16_t g_2451 = 0x8D6CL;/* VOLATILE GLOBAL g_2451 */
static volatile uint16_t g_2497 = 0x69B4L;/* VOLATILE GLOBAL g_2497 */
static int32_t **g_2642 = &g_582;
static int32_t ***g_2641[9] = {&g_2642,&g_2642,&g_2642,&g_2642,&g_2642,&g_2642,&g_2642,&g_2642,&g_2642};
static struct S0 g_2723 = {0xFC385BDEL,0L,-0x1.9p+1,0xD7C6L,1};
static struct S0 *g_2722[6] = {&g_2723,&g_2723,&g_2723,&g_2723,&g_2723,&g_2723};
static const uint32_t ** const **g_2738 = (void*)0;
static const uint32_t ** const ***g_2737 = &g_2738;
static struct S0 ** volatile g_2743 = &g_2722[4];/* VOLATILE GLOBAL g_2743 */
static uint8_t g_2763 = 0UL;
static volatile int16_t g_2782 = 0xE92AL;/* VOLATILE GLOBAL g_2782 */
static struct S0 * volatile * volatile g_2801 = &g_2722[2];/* VOLATILE GLOBAL g_2801 */
static int8_t ****g_2807[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int8_t **** const *g_2806 = &g_2807[2];
static const union U1 *g_2830 = &g_287[6];
static const union U1 **g_2829 = &g_2830;
static struct S0 * volatile * volatile g_2965[10][4][6] = {{{&g_2722[3],&g_2722[3],&g_2722[0],&g_2722[3],&g_2722[1],&g_2722[5]},{(void*)0,&g_2722[1],&g_2722[0],&g_2722[0],&g_2722[2],&g_2722[0]},{&g_2722[0],(void*)0,&g_2722[0],(void*)0,&g_2722[3],&g_2722[5]},{(void*)0,(void*)0,&g_2722[0],(void*)0,&g_2722[1],&g_2722[1]}},{{(void*)0,&g_2722[1],&g_2722[1],(void*)0,&g_2722[0],(void*)0},{(void*)0,&g_2722[5],&g_2722[3],(void*)0,&g_2722[0],(void*)0},{&g_2722[0],&g_2722[0],&g_2722[2],&g_2722[0],&g_2722[0],&g_2722[1]},{(void*)0,&g_2722[5],&g_2722[1],&g_2722[3],&g_2722[0],&g_2722[3]}},{{&g_2722[3],&g_2722[1],&g_2722[5],&g_2722[5],&g_2722[1],&g_2722[3]},{&g_2722[3],(void*)0,&g_2722[1],&g_2722[0],&g_2722[3],&g_2722[1]},{&g_2722[1],(void*)0,&g_2722[2],&g_2722[3],&g_2722[2],(void*)0},{&g_2722[1],&g_2722[1],&g_2722[3],&g_2722[0],&g_2722[1],(void*)0}},{{&g_2722[3],&g_2722[3],&g_2722[1],&g_2722[5],&g_2722[5],&g_2722[1]},{&g_2722[3],&g_2722[3],&g_2722[0],&g_2722[3],&g_2722[1],&g_2722[5]},{(void*)0,&g_2722[1],&g_2722[0],&g_2722[0],&g_2722[2],&g_2722[0]},{&g_2722[0],(void*)0,&g_2722[0],(void*)0,&g_2722[3],&g_2722[5]}},{{(void*)0,(void*)0,&g_2722[0],(void*)0,&g_2722[1],&g_2722[1]},{(void*)0,&g_2722[1],&g_2722[1],(void*)0,&g_2722[0],(void*)0},{(void*)0,&g_2722[5],&g_2722[3],(void*)0,&g_2722[0],(void*)0},{&g_2722[0],&g_2722[0],&g_2722[2],&g_2722[0],&g_2722[0],&g_2722[1]}},{{(void*)0,&g_2722[5],&g_2722[1],&g_2722[3],&g_2722[0],&g_2722[3]},{&g_2722[3],&g_2722[1],&g_2722[5],&g_2722[5],&g_2722[1],&g_2722[3]},{&g_2722[3],(void*)0,&g_2722[1],&g_2722[0],&g_2722[3],&g_2722[1]},{(void*)0,&g_2722[3],(void*)0,&g_2722[0],(void*)0,&g_2722[3]}},{{(void*)0,&g_2722[1],&g_2722[0],&g_2722[5],&g_2722[2],&g_2722[1]},{(void*)0,&g_2722[0],(void*)0,&g_2722[1],&g_2722[1],(void*)0},{&g_2722[0],&g_2722[0],(void*)0,(void*)0,&g_2722[2],&g_2722[1]},{&g_2722[3],&g_2722[1],&g_2722[5],(void*)0,(void*)0,(void*)0}},{{&g_2722[5],&g_2722[3],&g_2722[5],&g_2722[1],&g_2722[0],&g_2722[1]},{&g_2722[0],&g_2722[1],(void*)0,&g_2722[3],(void*)0,(void*)0},{&g_2722[3],(void*)0,(void*)0,&g_2722[3],(void*)0,&g_2722[1]},{&g_2722[0],&g_2722[1],&g_2722[0],&g_2722[1],&g_2722[5],&g_2722[3]}},{{&g_2722[5],(void*)0,(void*)0,(void*)0,&g_2722[5],&g_2722[1]},{&g_2722[3],&g_2722[1],&g_2722[2],(void*)0,(void*)0,&g_2722[0]},{&g_2722[0],(void*)0,&g_2722[1],&g_2722[1],(void*)0,&g_2722[0]},{(void*)0,&g_2722[1],&g_2722[2],&g_2722[5],&g_2722[0],&g_2722[1]}},{{(void*)0,&g_2722[3],(void*)0,&g_2722[0],(void*)0,&g_2722[3]},{(void*)0,&g_2722[1],&g_2722[0],&g_2722[5],&g_2722[2],&g_2722[1]},{(void*)0,&g_2722[0],(void*)0,&g_2722[1],&g_2722[1],(void*)0},{&g_2722[0],&g_2722[0],(void*)0,(void*)0,&g_2722[2],&g_2722[1]}}};
static struct S0 g_2968 = {9L,-1L,0x7.Dp+1,-5L,0};
static const struct S0 ** volatile g_2984 = (void*)0;/* VOLATILE GLOBAL g_2984 */
static const struct S0 g_2986 = {0x874A23D1L,4L,0x2.6D7F67p+18,3L,0};
static int16_t * volatile ** volatile ** volatile g_2987 = (void*)0;/* VOLATILE GLOBAL g_2987 */
static int32_t g_3008 = 0x96F2AC13L;
static int32_t *g_3009 = &g_70;
static const union U1 *g_3128 = &g_287[6];
static const union U1 ** volatile g_3127[8] = {&g_3128,&g_3128,&g_3128,&g_3128,&g_3128,&g_3128,&g_3128,&g_3128};
static const union U1 ** volatile g_3129[7][2][4] = {{{&g_3128,&g_3128,(void*)0,&g_3128},{(void*)0,&g_3128,(void*)0,&g_3128}},{{&g_3128,&g_3128,&g_3128,&g_3128},{&g_3128,(void*)0,&g_3128,&g_3128}},{{&g_3128,&g_3128,&g_3128,(void*)0},{&g_3128,&g_3128,&g_3128,&g_3128}},{{&g_3128,&g_3128,(void*)0,&g_3128},{(void*)0,&g_3128,(void*)0,&g_3128}},{{&g_3128,(void*)0,&g_3128,(void*)0},{&g_3128,(void*)0,&g_3128,&g_3128}},{{(void*)0,&g_3128,&g_3128,&g_3128},{&g_3128,&g_3128,&g_3128,&g_3128}},{{&g_3128,&g_3128,&g_3128,(void*)0},{&g_3128,&g_3128,(void*)0,&g_3128}}};
static uint32_t *****g_3158 = &g_646;
static float g_3184 = 0x5.447027p-76;
static uint32_t ** const g_3195[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint32_t ** const *g_3194 = &g_3195[1];
static volatile struct S0 g_3214 = {0L,7L,0xD.40D4EFp+49,0x502BL,-1};/* VOLATILE GLOBAL g_3214 */
static volatile struct S0 *g_3213 = &g_3214;
static volatile struct S0 * const *g_3212[4] = {&g_3213,&g_3213,&g_3213,&g_3213};
static volatile struct S0 * const * volatile *g_3211 = &g_3212[2];
static int32_t g_3220[7] = {0x939BE959L,0x939BE959L,0x939BE959L,0x939BE959L,0x939BE959L,0x939BE959L,0x939BE959L};
static uint16_t g_3299[8][1][8] = {{{65531UL,0x2DA3L,7UL,0x3FF9L,0UL,0x2DE8L,0x2DE8L,0x2DE8L}},{{0UL,0x2DE8L,0x2DE8L,0UL,0x3FF9L,7UL,0x2DA3L,65531UL}},{{4UL,0UL,0UL,0x2DA3L,0UL,0UL,4UL,0xFBD6L}},{{0UL,0UL,4UL,0xFBD6L,7UL,7UL,0xFBD6L,4UL}},{{0x2DE8L,0x2DE8L,0UL,0x3FF9L,7UL,0x2DA3L,65531UL,0x2DA3L}},{{0UL,4UL,0x3FF9L,4UL,0UL,6UL,0x2DE8L,0x2DA3L}},{{4UL,7UL,65531UL,0x3FF9L,0x3FF9L,65531UL,7UL,4UL}},{{0UL,6UL,65531UL,0xFBD6L,0x2DE8L,0UL,0x2DE8L,0xFBD6L}}};


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static float * func_11(uint8_t * p_12, int32_t  p_13, uint8_t  p_14, uint8_t * p_15, uint32_t  p_16);
static int32_t  func_18(int32_t  p_19, const uint64_t  p_20, uint32_t  p_21, uint32_t  p_22, int64_t  p_23);
static uint16_t  func_26(uint8_t * p_27, const uint64_t  p_28, const uint64_t  p_29, int64_t  p_30, float * p_31);
static uint8_t * func_32(uint8_t * p_33, union U1  p_34);
static uint8_t * func_35(const uint16_t  p_36);
static union U1  func_48(uint64_t  p_49, uint32_t  p_50, union U1  p_51);
static int32_t * func_63(uint64_t  p_64);
static const int32_t  func_92(const int64_t  p_93, union U1  p_94, uint8_t  p_95, float * p_96);
static const int64_t  func_97(int32_t * p_98, float * p_99, int64_t  p_100);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_6 g_4 g_40 g_1482 g_107 g_70
 * writes: g_4 g_7 g_70
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint8_t *l_3 = &g_4;
    float *l_10 = &g_7[2];
    int32_t l_1917[10][2][5] = {{{0x0F398CA3L,(-1L),1L,0x4094858EL,0xADE60B96L},{0x4094858EL,0x3885CB74L,0x14E93C4AL,(-1L),1L}},{{0L,0x0F398CA3L,0x4094858EL,0x4094858EL,0x0F398CA3L},{0xC94E020CL,(-1L),5L,0L,0x0F398CA3L}},{{0x3885CB74L,0x96CEC540L,(-1L),0x6EF57A86L,1L},{(-1L),0x6EF57A86L,0x6F37CE84L,0xADE60B96L,0xADE60B96L}},{{0x3885CB74L,5L,0x3885CB74L,0x6F37CE84L,0x1BC76DBCL},{0xC94E020CL,5L,(-1L),(-1L),0x96CEC540L}},{{0L,0x6EF57A86L,0xADE60B96L,(-1L),0xC94E020CL},{0x4094858EL,0x96CEC540L,(-1L),0x96CEC540L,0x4094858EL}},{{0x0F398CA3L,(-1L),0x3885CB74L,0x96CEC540L,(-1L)},{0L,0x0F398CA3L,0x6F37CE84L,(-1L),0x14E93C4AL}},{{(-1L),0x3885CB74L,(-1L),(-1L),(-1L)},{(-1L),(-1L),5L,0x6F37CE84L,0x4094858EL}},{{(-1L),1L,0x4094858EL,0xADE60B96L,0xC94E020CL},{(-1L),0L,0x14E93C4AL,0x6EF57A86L,0x96CEC540L}},{{0L,1L,1L,0L,0x1BC76DBCL},{0x0F398CA3L,(-1L),1L,0x4094858EL,0xADE60B96L}},{{0x4094858EL,0x3885CB74L,0x14E93C4AL,(-1L),1L},{0L,0x0F398CA3L,0x4094858EL,0x4094858EL,0x0F398CA3L}}};
    int32_t l_1922 = 0x9FB72E03L;
    const uint8_t l_2127 = 0xBAL;
    float *l_2128[8] = {&g_824,&g_824,&g_824,&g_824,&g_824,&g_824,&g_824,&g_824};
    int64_t l_2129 = 0x10CA352D48D0ACC4LL;
    const uint32_t ** const ***l_2739 = (void*)0;
    uint32_t *****l_2740 = &g_646;
    uint8_t *l_2803 = &g_172[1];
    int8_t **** const *l_2808 = (void*)0;
    union U1 **l_2832 = &g_2370;
    struct S0 **l_2877 = (void*)0;
    struct S0 * volatile l_2967 = &g_2968;/* VOLATILE GLOBAL l_2967 */
    int16_t l_2993 = 0x329EL;
    int16_t ** const l_3000 = &g_254;
    const int32_t *l_3056 = &g_40;
    uint8_t l_3071 = 0xFCL;
    int8_t l_3228 = 0x59L;
    uint32_t l_3259 = 4294967288UL;
    int32_t l_3282 = 1L;
    uint16_t l_3311 = 1UL;
    uint64_t l_3312 = 0x3D1E04C3CBC53DE3LL;
    int i, j, k;
    (*g_6) = (((*l_3) = g_2) , g_2);
    for (g_4 = 13; (g_4 < 43); g_4 = safe_add_func_int16_t_s_s(g_4, 8))
    { /* block id: 5 */
        uint8_t *l_17 = (void*)0;
        float * const l_37 = &g_7[4];
        float ***l_1915[5] = {&g_133[6][0][2],&g_133[6][0][2],&g_133[6][0][2],&g_133[6][0][2],&g_133[6][0][2]};
        float ****l_1914 = &l_1915[2];
        float *****l_1913 = &l_1914;
        int32_t l_1916 = 0x70012885L;
        int32_t *l_1923 = (void*)0;
        int32_t *l_1924 = &g_77[3][3];
        union U1 l_1925 = {9UL};
        uint64_t **** const l_2676 = &g_1329;
        uint32_t l_2731 = 0x6230A635L;
        int32_t l_2781 = 0xA053FD55L;
        int32_t l_2786 = 0xCE124508L;
        int32_t l_2787 = 1L;
        int32_t l_2788 = 0x135518BDL;
        int32_t l_2882 = 1L;
        int32_t *l_2887 = &l_2787;
        int32_t l_2924 = 9L;
        int16_t l_2925 = (-1L);
        uint32_t l_2998 = 0x01809C0BL;
        int32_t *l_3023 = &l_1922;
        uint8_t l_3039[9][9] = {{255UL,0xB3L,0x96L,0xB3L,255UL,6UL,1UL,249UL,253UL},{0UL,0UL,0x24L,253UL,0x5DL,9UL,0x8DL,9UL,0x5DL},{1UL,0x40L,0x40L,247UL,1UL,0x96L,249UL,0xB3L,0x8DL},{253UL,0x5DL,248UL,0x3CL,0x99L,9UL,9UL,0x99L,0x3CL},{1UL,253UL,1UL,0xADL,1UL,0xB3L,0xB7L,255UL,0x96L},{0UL,255UL,0x99L,250UL,2UL,4UL,2UL,250UL,0x99L},{0x96L,0x96L,253UL,0xADL,0xC5L,1UL,0x8DL,249UL,0x83L},{0UL,0x8DL,4UL,0x3CL,0x4BL,0x4BL,0x3CL,4UL,0x8DL},{0x07L,1UL,253UL,247UL,0x40L,0xADL,0x96L,0x8DL,1UL}};
        int8_t l_3137[10];
        struct S0 l_3181 = {0x34044754L,0x8502E5A57EF8D098LL,0x1.0F5ECBp-66,0x9A40L,0};
        uint64_t l_3199[10] = {0x7BB4BCC1B5D5F0A5LL,1UL,0x7BB4BCC1B5D5F0A5LL,1UL,0x7BB4BCC1B5D5F0A5LL,1UL,0x7BB4BCC1B5D5F0A5LL,1UL,0x7BB4BCC1B5D5F0A5LL,1UL};
        int8_t l_3222[5][9][4] = {{{0x53L,0x92L,(-5L),0xEBL},{0x59L,0x4DL,0x92L,0x4DL},{0xD7L,0xFCL,0x85L,0L},{0x3AL,8L,(-1L),2L},{(-4L),1L,0x19L,0x0AL},{(-4L),0x90L,(-1L),7L},{0x3AL,0x0AL,0x85L,0xBFL},{0xD7L,0xA2L,0x92L,1L},{0x59L,5L,(-5L),0L}},{{0x53L,0x94L,2L,1L},{0L,0x13L,1L,0x3EL},{0x92L,(-3L),(-1L),(-5L)},{8L,0xF1L,0x29L,(-1L)},{(-7L),(-1L),(-1L),(-1L)},{0xFCL,2L,(-7L),0xB9L},{(-5L),0x4DL,0xBFL,0xA2L},{0xB9L,0x23L,0x90L,0x90L},{(-1L),(-1L),0xBEL,0L}},{{0x5FL,0L,0x0FL,1L},{0x0AL,(-4L),0xA6L,0x0FL},{0x13L,(-4L),0L,1L},{(-4L),0L,(-1L),0L},{0xA2L,(-1L),(-1L),0x90L},{0x85L,0x23L,(-4L),0xA2L},{0xA6L,0x4DL,0xD7L,0xB9L},{0x30L,2L,1L,(-1L)},{1L,(-1L),0xFCL,(-1L)}},{{0x60L,0xF1L,(-1L),(-5L)},{0x0EL,(-3L),0x5FL,0x3EL},{(-2L),0x13L,0x4DL,1L},{0x23L,0xA2L,0xF1L,0L},{0x94L,0xF1L,0x43L,(-1L)},{(-1L),1L,(-1L),(-1L)},{0x4DL,0x59L,0x3EL,0x87L},{0xA2L,0x23L,0x0EL,0x59L},{(-4L),(-1L),0x0EL,0x36L}},{{0xA2L,0xEBL,0x3EL,0x43L},{0x4DL,0xA6L,(-1L),0L},{(-1L),0L,0x43L,0x2BL},{0x94L,0x0AL,0xF1L,0x4DL},{0x65L,0xD7L,0x19L,0L},{0x29L,0x4DL,0L,0x0AL},{(-5L),(-3L),7L,(-7L)},{0x0EL,0x94L,0xA6L,2L},{0x53L,0x0EL,0xBFL,0x0FL}}};
        int32_t ****l_3254 = &g_2641[8];
        uint64_t l_3274 = 18446744073709551615UL;
        int i, j, k;
        for (i = 0; i < 10; i++)
            l_3137[i] = 0x0DL;
    }
    (**g_1482) |= (*l_3056);
    return l_3312;
}


/* ------------------------------------------ */
/* 
 * reads : g_87 g_130 g_186 g_582 g_40
 * writes: g_87 g_130 g_186 g_40
 */
static float * func_11(uint8_t * p_12, int32_t  p_13, uint8_t  p_14, uint8_t * p_15, uint32_t  p_16)
{ /* block id: 1151 */
    int8_t l_2512[1];
    int32_t **l_2520[8][4][8] = {{{(void*)0,&g_582,&g_107,&g_107,&g_582,&g_582,&g_582,(void*)0},{&g_582,&g_107,&g_582,&g_582,&g_107,&g_582,(void*)0,&g_107},{&g_582,&g_582,(void*)0,(void*)0,&g_582,&g_107,&g_107,(void*)0},{&g_107,&g_582,&g_107,&g_582,&g_582,&g_582,&g_582,&g_582}},{{&g_107,&g_582,&g_582,&g_107,&g_582,&g_582,&g_107,&g_107},{&g_582,&g_582,&g_582,&g_582,&g_107,&g_582,(void*)0,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_107,&g_582,&g_107,&g_107,(void*)0,&g_107,&g_582,&g_582}},{{&g_582,&g_107,&g_582,&g_107,&g_582,&g_582,&g_582,&g_107},{&g_582,&g_582,&g_107,(void*)0,&g_582,&g_582,&g_107,&g_107},{&g_582,&g_107,&g_107,&g_582,(void*)0,&g_582,&g_582,&g_107},{(void*)0,&g_107,&g_582,&g_582,&g_107,&g_107,&g_582,&g_582}},{{&g_582,(void*)0,(void*)0,&g_107,(void*)0,&g_582,&g_582,(void*)0},{(void*)0,(void*)0,&g_582,(void*)0,&g_582,(void*)0,&g_582,(void*)0},{&g_107,&g_107,&g_582,&g_582,&g_107,&g_582,(void*)0,&g_582},{&g_107,&g_582,&g_107,&g_582,&g_107,&g_107,(void*)0,(void*)0}},{{&g_582,&g_582,&g_582,&g_107,&g_107,&g_107,&g_107,&g_582},{&g_107,&g_107,&g_107,&g_582,&g_582,(void*)0,&g_107,&g_582},{&g_582,&g_107,&g_582,&g_582,&g_107,&g_107,&g_582,(void*)0},{&g_107,&g_107,&g_582,(void*)0,&g_107,&g_107,(void*)0,&g_582}},{{&g_107,&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_107,&g_107,(void*)0,&g_582,&g_582,&g_107,&g_107},{(void*)0,&g_107,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_107,&g_107,(void*)0,(void*)0,(void*)0,&g_107,(void*)0,(void*)0}},{{&g_582,&g_582,&g_582,(void*)0,&g_107,(void*)0,&g_582,&g_582},{&g_582,&g_107,&g_107,(void*)0,&g_582,&g_107,&g_107,&g_582},{&g_582,&g_582,&g_107,&g_107,&g_107,&g_582,&g_582,&g_582},{&g_582,&g_107,&g_107,&g_107,&g_582,&g_582,&g_107,&g_107}},{{&g_107,&g_107,&g_582,&g_107,&g_582,&g_582,&g_582,&g_107},{&g_107,&g_582,(void*)0,&g_582,(void*)0,&g_582,&g_582,&g_107},{&g_582,&g_107,&g_107,&g_107,&g_582,&g_582,&g_582,&g_107},{&g_582,&g_107,&g_582,&g_107,&g_582,&g_582,&g_582,&g_582}}};
    int32_t ***l_2519 = &l_2520[5][1][5];
    int32_t l_2527 = 0x45AFC85EL;
    uint8_t *l_2559 = &g_4;
    uint64_t *** const * const l_2650[10] = {&g_1329,&g_1329,&g_1329,&g_1329,&g_1329,&g_1329,&g_1329,&g_1329,&g_1329,&g_1329};
    uint64_t *** const * const *l_2649 = &l_2650[0];
    uint32_t l_2656 = 4294967289UL;
    float *l_2668[6][9] = {{&g_7[2],(void*)0,&g_7[4],&g_7[5],&g_171,&g_7[2],&g_7[3],(void*)0,&g_7[2]},{&g_824,&g_7[4],(void*)0,(void*)0,&g_7[2],&g_7[2],(void*)0,(void*)0,&g_7[4]},{(void*)0,&g_7[4],(void*)0,&g_824,&g_824,(void*)0,&g_171,&g_7[3],&g_171},{&g_171,&g_7[2],&g_7[4],&g_7[3],(void*)0,&g_824,&g_7[2],(void*)0,&g_7[3]},{&g_7[4],&g_7[4],&g_7[5],&g_7[3],&g_7[3],&g_7[5],&g_7[4],&g_7[4],(void*)0},{&g_7[4],&g_7[4],(void*)0,&g_7[5],&g_824,&g_7[2],(void*)0,&g_7[3],&g_7[2]}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_2512[i] = 0L;
    for (g_87 = (-13); (g_87 >= 6); g_87++)
    { /* block id: 1154 */
        uint64_t l_2528 = 3UL;
        uint32_t l_2544[5];
        int32_t l_2545 = (-6L);
        uint16_t l_2546 = 65533UL;
        uint16_t l_2549 = 65535UL;
        struct S0 l_2553 = {0x2E6BC3FCL,0x6E431BEC191E6A7FLL,0xC.64A1B4p-6,-1L,0};
        uint8_t *l_2562 = &g_186[4][6][0];
        uint32_t **l_2582 = (void*)0;
        int32_t l_2584 = 0x5F292E68L;
        int32_t l_2589 = 4L;
        int8_t l_2591 = 1L;
        int32_t l_2593[10][7] = {{1L,0xC06DED90L,0L,0xD49D56FFL,0x24DB70BDL,0xC06DED90L,0xD49D56FFL},{0x96303E4DL,0x064BBA38L,0L,(-1L),(-3L),0L,0xFB355C91L},{0x879EAA1DL,0xD52091D0L,(-4L),(-4L),0xD52091D0L,0x879EAA1DL,0xD4E5047FL},{0x96303E4DL,(-1L),2L,0x96303E4DL,0xFB355C91L,0L,(-1L)},{0x24DB70BDL,1L,0x879EAA1DL,0xFE5F7989L,0x879EAA1DL,1L,0x24DB70BDL},{(-7L),(-1L),4L,(-3L),0xDFC47681L,(-7L),(-3L)},{0x07C66225L,0xD52091D0L,(-7L),0xD49D56FFL,0xD49D56FFL,(-7L),0xD52091D0L},{(-1L),0x064BBA38L,4L,0L,0x064BBA38L,(-3L),0xFB355C91L},{(-1L),0xD49D56FFL,0x879EAA1DL,(-1L),0xD52091D0L,(-1L),0x879EAA1DL},{0xDFC47681L,0xDFC47681L,2L,0L,(-1L),0x5632A555L,0xDFC47681L}};
        int32_t ** const *l_2644 = &g_2642;
        int i, j;
        for (i = 0; i < 5; i++)
            l_2544[i] = 18446744073709551615UL;
        for (g_130 = 0; (g_130 <= 1); g_130 += 1)
        { /* block id: 1157 */
            uint64_t l_2515 = 18446744073709551610UL;
            int32_t l_2530 = 0xCD4FCE62L;
            int16_t l_2541 = 3L;
            float *l_2550 = &g_824;
            int32_t l_2590 = (-1L);
            const struct S0 l_2618[9][5] = {{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}},{{0x4C072960L,-1L,0x1.7p+1,-8L,-0},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{4L,-4L,0x2.823A56p-9,-1L,1},{0x589615D4L,-1L,0x4.0p+1,0xA366L,0},{0x4C072960L,-1L,0x1.7p+1,-8L,-0}}};
            int8_t l_2652 = (-8L);
            int i, j;
        }
        (*g_582) &= (safe_lshift_func_int8_t_s_u(0xECL, (--(*l_2562))));
    }
    return l_2668[5][7];
}


/* ------------------------------------------ */
/* 
 * reads : g_1197 g_2150 g_323 g_324 g_2403 g_1458 g_1459 g_134 g_7 g_1557 g_1457 g_430 g_171 g_1058 g_127 g_753 g_287.f0 g_2451 g_1117 g_107 g_70 g_294 g_295 g_130 g_1057 g_582 g_40 g_1155
 * writes: g_1197 g_2150 g_2403 g_7 g_130 g_287.f0 g_40 g_899 g_70
 */
static int32_t  func_18(int32_t  p_19, const uint64_t  p_20, uint32_t  p_21, uint32_t  p_22, int64_t  p_23)
{ /* block id: 1009 */
    uint64_t l_2130 = 18446744073709551615UL;
    int32_t l_2136 = 8L;
    int32_t l_2142 = 0xE72946C4L;
    int32_t l_2144 = 0xF1E18D69L;
    int32_t l_2145 = (-1L);
    int32_t l_2146[1];
    int32_t l_2147[4];
    float l_2199[3];
    uint8_t *l_2229 = &g_172[2];
    const int32_t *****l_2247 = &g_2244;
    uint8_t l_2304 = 254UL;
    uint64_t ****l_2319 = &g_1329;
    uint32_t **l_2327 = &g_649;
    int64_t l_2328 = 0xBF6A5DD8F7702DBBLL;
    uint32_t l_2339 = 4294967295UL;
    uint16_t l_2364[6];
    int8_t **l_2373 = (void*)0;
    uint32_t l_2402 = 0x0DCB6DC4L;
    const uint32_t ***l_2405 = &g_2403;
    float ***l_2408 = &g_133[0][0][4];
    float ****l_2407[7] = {&l_2408,&l_2408,&l_2408,&l_2408,&l_2408,&l_2408,&l_2408};
    float *****l_2406[4][5] = {{&l_2407[4],&l_2407[0],&l_2407[4],&l_2407[4],&l_2407[4]},{&l_2407[4],&l_2407[0],&l_2407[4],&l_2407[4],&l_2407[4]},{&l_2407[4],&l_2407[0],&l_2407[4],&l_2407[4],&l_2407[4]},{&l_2407[4],&l_2407[0],&l_2407[4],&l_2407[4],&l_2407[4]}};
    uint16_t l_2420 = 0x46A0L;
    uint32_t l_2423[6];
    uint32_t l_2467 = 0xA7E9EA0EL;
    union U1 l_2479 = {0UL};
    int32_t *l_2509 = (void*)0;
    int i, j;
    for (i = 0; i < 1; i++)
        l_2146[i] = 0x9C497A56L;
    for (i = 0; i < 4; i++)
        l_2147[i] = 0x19E85795L;
    for (i = 0; i < 3; i++)
        l_2199[i] = 0x0.Dp+1;
    for (i = 0; i < 6; i++)
        l_2364[i] = 65535UL;
    for (i = 0; i < 6; i++)
        l_2423[i] = 0x6D1C27F0L;
    l_2130++;
    for (g_1197 = 10; (g_1197 != 28); g_1197 = safe_add_func_int16_t_s_s(g_1197, 3))
    { /* block id: 1013 */
        float l_2135[5][2] = {{0xA.123C4Fp-15,0xA.123C4Fp-15},{0xA.123C4Fp-15,0xA.123C4Fp-15},{0xA.123C4Fp-15,0xA.123C4Fp-15},{0xA.123C4Fp-15,0xA.123C4Fp-15},{0xA.123C4Fp-15,0xA.123C4Fp-15}};
        int32_t *l_2137 = &g_40;
        int32_t *l_2138 = &g_70;
        int32_t *l_2139 = &g_70;
        int32_t *l_2140 = &g_70;
        int32_t *l_2141[9][5][5] = {{{&g_70,&g_70,&g_70,&g_70,&g_40},{&l_2136,&l_2136,(void*)0,&l_2136,&g_40},{&l_2136,&l_2136,&l_2136,&g_40,&l_2136},{&g_40,&g_40,&g_70,(void*)0,&l_2136},{&l_2136,(void*)0,&g_40,(void*)0,&l_2136}},{{&g_70,&l_2136,&g_70,&g_40,&g_70},{(void*)0,&l_2136,&g_40,&l_2136,&g_40},{&g_70,&g_70,&l_2136,&g_70,&l_2136},{(void*)0,&g_70,&g_40,&g_70,&l_2136},{&g_40,&l_2136,&l_2136,&l_2136,&g_40}},{{&g_40,&g_40,&g_40,&l_2136,(void*)0},{(void*)0,&l_2136,&g_40,&l_2136,&g_70},{&l_2136,&g_40,&g_70,&g_70,&g_40},{&g_70,&g_40,&g_40,&g_40,&g_70},{&g_70,&l_2136,&g_70,(void*)0,&g_40}},{{&l_2136,&l_2136,&l_2136,&g_40,&g_70},{&g_70,(void*)0,(void*)0,(void*)0,&g_70},{&g_70,(void*)0,&g_40,&l_2136,&g_70},{&l_2136,(void*)0,&g_40,&g_70,&g_70},{(void*)0,&g_70,(void*)0,&g_70,&g_40}},{{&g_40,&g_40,(void*)0,(void*)0,&l_2136},{&g_40,(void*)0,&g_70,&g_70,&g_70},{&g_70,&g_40,&l_2136,&g_70,&g_70},{&g_70,&g_40,&g_40,&l_2136,&l_2136},{(void*)0,&g_70,(void*)0,&g_40,&l_2136}},{{&g_40,&g_40,&g_70,(void*)0,(void*)0},{&g_70,&l_2136,&g_70,&l_2136,&g_40},{&g_70,&g_70,(void*)0,(void*)0,&l_2136},{&g_70,&g_70,&g_40,&g_40,&l_2136},{&g_70,&l_2136,&l_2136,(void*)0,&g_70}},{{&g_70,&l_2136,&g_70,&l_2136,&g_40},{&g_40,&g_70,(void*)0,&g_70,&g_40},{&l_2136,(void*)0,(void*)0,&g_70,&l_2136},{(void*)0,&g_40,&g_40,&g_70,&g_40},{&g_70,&g_40,&g_40,&g_40,&g_70}},{{&g_40,&g_70,(void*)0,&l_2136,&g_40},{&g_70,&g_40,&l_2136,&l_2136,&g_70},{(void*)0,&g_70,&g_70,&g_70,&g_40},{&g_70,&l_2136,&g_40,&g_40,&g_70},{&g_40,(void*)0,&g_70,&l_2136,&g_40}},{{&l_2136,(void*)0,&g_40,&g_40,&l_2136},{(void*)0,(void*)0,&g_40,&g_70,&g_40},{&l_2136,&g_70,&l_2136,&l_2136,&g_40},{&g_40,&g_40,&g_40,&g_70,&g_70},{&g_40,&g_70,(void*)0,&g_40,&l_2136}}};
        float l_2143 = 0x5.C41A7Ap-16;
        int8_t l_2148 = 0x18L;
        int16_t l_2149 = 0xF2AEL;
        const uint32_t * const l_2164 = &g_257;
        const uint32_t * const *l_2163 = &l_2164;
        int16_t ***l_2280[4][5][6] = {{{(void*)0,&g_852,(void*)0,&g_852,&g_852,(void*)0},{&g_852,&g_852,(void*)0,&g_852,(void*)0,&g_852},{(void*)0,&g_852,&g_852,&g_852,&g_852,(void*)0},{&g_852,(void*)0,&g_852,(void*)0,&g_852,&g_852},{&g_852,(void*)0,(void*)0,&g_852,&g_852,&g_852}},{{&g_852,&g_852,&g_852,(void*)0,(void*)0,&g_852},{&g_852,&g_852,(void*)0,&g_852,(void*)0,&g_852},{(void*)0,&g_852,&g_852,&g_852,&g_852,(void*)0},{&g_852,(void*)0,&g_852,(void*)0,&g_852,&g_852},{&g_852,(void*)0,(void*)0,&g_852,&g_852,&g_852}},{{&g_852,&g_852,&g_852,(void*)0,(void*)0,&g_852},{&g_852,&g_852,(void*)0,&g_852,(void*)0,&g_852},{(void*)0,&g_852,&g_852,&g_852,&g_852,(void*)0},{&g_852,(void*)0,&g_852,(void*)0,&g_852,&g_852},{&g_852,(void*)0,(void*)0,&g_852,&g_852,&g_852}},{{&g_852,&g_852,&g_852,(void*)0,(void*)0,&g_852},{&g_852,&g_852,(void*)0,&g_852,(void*)0,&g_852},{(void*)0,&g_852,&g_852,&g_852,&g_852,(void*)0},{&g_852,(void*)0,&g_852,(void*)0,&g_852,&g_852},{&g_852,(void*)0,(void*)0,&g_852,&g_852,&g_852}}};
        const int8_t **l_2374 = &g_441;
        int i, j, k;
        ++g_2150;
        for (l_2136 = 0; (l_2136 != (-9)); l_2136--)
        { /* block id: 1017 */
            int32_t l_2200 = 0x7D380060L;
            float l_2207 = 0xA.3498B2p-27;
            int32_t l_2210 = 1L;
            int32_t l_2212 = 9L;
            int32_t l_2218 = (-1L);
            uint8_t l_2220 = 0x31L;
            union U1 l_2248[4][8][8] = {{{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}}},{{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}}},{{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}}},{{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}},{{0UL},{0x30L},{0x30L},{0UL},{0UL},{0x30L},{0x30L},{0UL}}}};
            uint32_t l_2264 = 18446744073709551610UL;
            float ***l_2318 = (void*)0;
            float ****l_2317 = &l_2318;
            uint32_t l_2362 = 0x116BD47FL;
            int32_t l_2383 = 0xEC730714L;
            int32_t l_2391 = 0x8487A6C2L;
            int32_t l_2393 = (-2L);
            int i, j, k;
        }
    }
    l_2145 = (safe_sub_func_float_f_f((((p_20 , 0xE.984076p+32) > (l_2146[0] = (0xA.1AA71Bp+65 < (((*g_134) = ((((safe_div_func_float_f_f(((l_2402 , (*g_323)) == ((*l_2405) = g_2403)), ((void*)0 != l_2406[2][4]))) == p_19) != (***g_1458)) < (*g_134))) != p_19)))) < p_21), 0x3.2656BFp+7));
    if (((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((((*g_1557) = p_22) >= (((-(safe_add_func_float_f_f(((***g_1458) = (safe_sub_func_float_f_f(((safe_mul_func_float_f_f((l_2420 <= (****g_1457)), (((((0x9.27683Bp+0 >= (safe_div_func_float_f_f(0xE.91A94Bp-97, l_2423[0]))) < (safe_div_func_float_f_f(0x1.9p-1, (*g_430)))) <= ((l_2147[1] = p_21) >= (-0x10.6p+1))) >= p_19) != (****g_1457)))) >= (**g_1459)), 0x1.2p+1))), p_21))) <= p_22) , (-7L))), p_19)), 0x1991L)) == 0x4235L))
    { /* block id: 1125 */
        int8_t l_2438[1];
        struct S0 l_2450[10] = {{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1},{0x4476F5E7L,0x1F4F81B9D4B95A21LL,-0x10.7p-1,0x91D5L,1}};
        int64_t l_2480[8] = {0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL,0x3425C51B7EA3B75DLL};
        int32_t l_2481 = 0x67E307B5L;
        int i;
        for (i = 0; i < 1; i++)
            l_2438[i] = 0xADL;
        if (((p_21 || ((safe_mul_func_uint16_t_u_u(65527UL, (safe_lshift_func_uint8_t_u_u(((safe_mod_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_u((safe_div_func_int16_t_s_s((l_2438[0] & (safe_lshift_func_int16_t_s_s((p_21 | ((!(safe_mul_func_int8_t_s_s((*g_1058), ((*g_753)++)))) != (safe_add_func_uint16_t_u_u((((safe_div_func_uint32_t_u_u((l_2450[3] , g_2451), p_22)) && (((safe_lshift_func_int16_t_s_s((safe_add_func_uint16_t_u_u((((p_19 = (**g_1117)) >= p_21) <= (*g_1058)), l_2438[0])), 8)) , p_21) || 0xF1CEL)) ^ p_23), (*g_294))))), (*g_294)))), 0x95DAL)), p_22)) < g_130), p_23)), p_21)) ^ 0xE924B870D84588ECLL), 2)))) == (**g_1057))) , 0xBB69CDD7L))
        { /* block id: 1128 */
            int16_t l_2456 = 1L;
            return l_2456;
        }
        else
        { /* block id: 1130 */
            int32_t l_2471 = 0x960FFA28L;
            struct S0 *l_2483[2][1][1];
            struct S0 **l_2482 = &l_2483[0][0][0];
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_2483[i][j][k] = &g_1845[0][3];
                }
            }
            l_2481 &= (safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s((safe_sub_func_int32_t_s_s(((l_2467 >= (safe_add_func_uint64_t_u_u((safe_unary_minus_func_int32_t_s(((*g_582) |= (l_2450[3].f4 = 0L)))), l_2471))) != (0x4FD3L <= (+(safe_rshift_func_int16_t_s_u(p_23, 10))))), p_19)), ((0L <= (((func_48(p_22, (safe_rshift_func_uint16_t_u_u(l_2438[0], 6)), l_2479) , 1L) , p_21) ^ 0UL)) && p_20))), l_2480[5])), l_2471)), 0L));
            (*l_2482) = &g_1845[1][2];
        }
    }
    else
    { /* block id: 1136 */
        int32_t l_2486 = 4L;
        int32_t l_2498 = (-2L);
        uint16_t *l_2507[5];
        int32_t l_2508 = 0x3CBFB41CL;
        int i;
        for (i = 0; i < 5; i++)
            l_2507[i] = (void*)0;
        for (l_2339 = 9; (l_2339 >= 32); ++l_2339)
        { /* block id: 1139 */
            return l_2486;
        }
        l_2509 = &p_19;
    }
    return (**g_1155);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t  func_26(uint8_t * p_27, const uint64_t  p_28, const uint64_t  p_29, int64_t  p_30, float * p_31)
{ /* block id: 1007 */
    return p_30;
}


/* ------------------------------------------ */
/* 
 * reads : g_190 g_648 g_649 g_53 g_1057 g_1058 g_692 g_693 g_694 g_1061 g_564.f0 g_582 g_40 g_852 g_753 g_287.f0 g_1197 g_1482 g_107 g_70 g_1117 g_164 g_127 g_1481 g_1557 g_130 g_1457 g_1458 g_1459 g_134 g_7 g_85 g_1298 g_172 g_1480 g_849 g_850 g_851 g_1820 g_441 g_442
 * writes: g_107 g_53 g_254 g_287.f0 g_70 g_40 g_164 g_7 g_130 g_1298 g_238
 */
static uint8_t * func_32(uint8_t * p_33, union U1  p_34)
{ /* block id: 913 */
    int32_t *l_1926 = &g_70;
    int32_t **l_1927[8];
    const int8_t *l_1932 = &g_442;
    int64_t l_1933 = (-7L);
    int32_t l_1940[10] = {0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L,0xF8F42712L};
    int16_t ***l_1981[1];
    uint32_t **l_2002 = &g_649;
    uint32_t l_2010[4] = {0x7CDF3F18L,0x7CDF3F18L,0x7CDF3F18L,0x7CDF3F18L};
    float ***l_2089 = &g_133[6][0][2];
    int32_t l_2102 = (-1L);
    int i;
    for (i = 0; i < 8; i++)
        l_1927[i] = &g_582;
    for (i = 0; i < 1; i++)
        l_1981[i] = (void*)0;
    (*g_190) = (p_34.f0 , l_1926);
    (*g_582) = ((**g_1117) = (((++(**g_648)) , (safe_lshift_func_uint8_t_u_u((((l_1932 == (*g_1057)) >= (**g_692)) <= (p_34.f0 , (l_1933 = p_34.f0))), 0))) < (((((*g_1061) <= (safe_mul_func_uint8_t_u_u(((*g_753) &= ((safe_rshift_func_uint16_t_u_u((((*g_582) | 0xE3218CF4L) > ((((*g_852) = (((safe_lshift_func_int8_t_s_s(p_34.f0, 6)) == 0xACBAE4C5L) , (void*)0)) != (void*)0) , 0x6A0701C3L)), l_1940[7])) < p_34.f0)), g_1197))) > 1UL) < p_34.f0) & (**g_1482))));
    for (g_164 = 11; (g_164 != (-16)); --g_164)
    { /* block id: 923 */
        struct S0 l_1953 = {0L,0x5200C1AC0EDA9618LL,0x5.6p-1,0xE26EL,0};
        int32_t l_1960 = 0x815DD74BL;
        int32_t *l_1966 = &l_1940[7];
        int32_t l_1970 = (-1L);
        int32_t l_1972 = (-2L);
        int32_t l_1974 = 1L;
        uint8_t l_1975 = 1UL;
        const float l_2003[7][2] = {{0xD.9E770Cp+21,0xD.9E770Cp+21},{0xD.9E770Cp+21,0xD.9E770Cp+21},{0xD.9E770Cp+21,0xD.9E770Cp+21},{0xD.9E770Cp+21,0xD.9E770Cp+21},{0xD.9E770Cp+21,0xD.9E770Cp+21},{0xD.9E770Cp+21,0xD.9E770Cp+21},{0xD.9E770Cp+21,0xD.9E770Cp+21}};
        uint16_t ** const l_2012 = &g_1557;
        uint16_t ** const *l_2011 = &l_2012;
        int32_t l_2101 = (-1L);
        int32_t l_2103[5];
        uint32_t l_2106 = 3UL;
        int32_t ***l_2121 = &l_1927[0];
        int32_t ****l_2120 = &l_2121;
        uint32_t *** const *l_2122 = &g_647[2][4];
        int i, j;
        for (i = 0; i < 5; i++)
            l_2103[i] = (-1L);
        if (((((***g_1481) &= (+(**g_1057))) <= (safe_sub_func_int16_t_s_s(((((safe_mod_func_uint16_t_u_u((*g_1557), (safe_mul_func_uint8_t_u_u(1UL, (*g_1058))))) < (((0x0.Cp+1 >= (safe_add_func_float_f_f(((+0x1.8p-1) <= (l_1960 = (l_1953 , (safe_sub_func_float_f_f((l_1953.f4 = ((safe_add_func_float_f_f((safe_div_func_float_f_f(((**g_1459) = (****g_1457)), ((l_1953.f3 , p_34.f0) < 0x1.Cp-1))), 0x2.1p+1)) >= 0x8.186B7Ep-28)), 0x7.5004E1p-92))))), p_34.f0))) , 0x9004L) & p_34.f0)) , p_34.f0) > g_85), (*g_1557)))) & 0xFFL))
        { /* block id: 928 */
            int32_t * const l_1963 = &l_1940[9];
            int32_t *l_1991 = &g_238;
            uint16_t l_1992 = 65535UL;
            for (p_34.f2 = 4; (p_34.f2 > 1); p_34.f2 = safe_sub_func_int32_t_s_s(p_34.f2, 7))
            { /* block id: 931 */
                int32_t **l_1964 = (void*)0;
                int32_t l_1967 = 0x0B4E629FL;
                int32_t l_1968 = 1L;
                int32_t l_1969 = 0x682E9C04L;
                int32_t l_1971[1][1][6] = {{{0xB7DB5E66L,(-10L),(-10L),0xB7DB5E66L,(-10L),(-10L)}}};
                int32_t l_1973 = 0x8FF0E673L;
                struct S0 l_1980[7] = {{-7L,-1L,0xA.73267Fp-92,-1L,-1},{-7L,-1L,0xA.73267Fp-92,-1L,-1},{-7L,-1L,0xA.73267Fp-92,-1L,-1},{-7L,-1L,0xA.73267Fp-92,-1L,-1},{-7L,-1L,0xA.73267Fp-92,-1L,-1},{-7L,-1L,0xA.73267Fp-92,-1L,-1},{-7L,-1L,0xA.73267Fp-92,-1L,-1}};
                int i, j, k;
                l_1966 = l_1963;
                if ((*l_1963))
                    continue;
                ++l_1975;
                for (g_130 = 5; (g_130 >= 58); ++g_130)
                { /* block id: 937 */
                    for (g_1298 = 4; (g_1298 >= 0); g_1298 -= 1)
                    { /* block id: 940 */
                        int i;
                        if (g_172[(g_1298 + 1)])
                            break;
                        (*l_1966) = ((****g_1480) = (l_1980[5] , (l_1981[0] == &g_852)));
                        (*g_582) = ((*g_849) != (*g_851));
                    }
                }
            }
            (**g_1459) = ((p_34.f4 == p_34.f2) != (-((safe_sub_func_uint32_t_u_u((4UL ^ (*l_1966)), (safe_div_func_int64_t_s_s((safe_mod_func_uint16_t_u_u((((*l_1991) = (safe_lshift_func_int16_t_s_s(0x1FEAL, (*l_1966)))) , ((l_1992 , 0xC3F5E285CDFE3A46LL) > 0x89B8CD05B4894766LL)), (*l_1926))), 0x902CAEB24FC4C39DLL)))) , 0x7.Cp-1)));
            (*l_1926) &= ((*l_1966) = (((~(safe_mul_func_uint16_t_u_u(((safe_div_func_uint64_t_u_u((g_1820[1] , (((((((safe_add_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s((0xDCL > ((void*)0 != l_2002)), ((p_34.f2 , (safe_add_func_uint16_t_u_u((*g_1557), (safe_lshift_func_int16_t_s_s(p_34.f2, 8))))) , (-1L)))), (safe_rshift_func_int8_t_s_s((*g_441), (**g_1057))))) == 1UL) , 0xB4L) | 0xD1L) < (*g_1557)) || 0xFB779C5CL) ^ 0x67C1213D34742D98LL)), l_2010[1])) && p_34.f0), p_34.f2))) , 0L) & (*l_1963)));
        }
        else
        { /* block id: 952 */
            uint16_t **l_2014[7][3][10] = {{{&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,(void*)0,(void*)0,&g_1557,&g_1557,&g_1557},{(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557},{&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557}},{{(void*)0,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557},{(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557},{&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557}},{{&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,(void*)0,(void*)0,&g_1557,(void*)0},{&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557},{&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557}},{{(void*)0,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0},{(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557},{&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557}},{{&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,(void*)0},{(void*)0,(void*)0,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,(void*)0,&g_1557},{&g_1557,&g_1557,&g_1557,(void*)0,(void*)0,&g_1557,(void*)0,&g_1557,(void*)0,(void*)0}},{{&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,(void*)0},{&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557},{&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557}},{{&g_1557,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557},{(void*)0,(void*)0,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,&g_1557,(void*)0,&g_1557},{&g_1557,&g_1557,&g_1557,(void*)0,&g_1557,&g_1557,(void*)0,(void*)0,(void*)0,&g_1557}}};
            uint16_t *** const l_2013 = &l_2014[0][2][5];
            int i, j, k;
            (*l_1966) &= (l_2011 == l_2013);
        }
        for (p_34.f3 = (-18); (p_34.f3 > (-2)); p_34.f3 = safe_add_func_uint64_t_u_u(p_34.f3, 2))
        { /* block id: 957 */
            uint32_t l_2039 = 0x4EC08E2FL;
            const union U1 l_2068[9][1][7] = {{{{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L}}},{{{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L}}},{{{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L}}},{{{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L}}},{{{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L}}},{{{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L}}},{{{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L}}},{{{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L},{0xD6L}}},{{{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L},{0xF9L}}}};
            int32_t l_2076[9];
            const int16_t l_2083 = 0xF0FBL;
            float *l_2095 = &g_171;
            int16_t l_2100 = 0x2E31L;
            int32_t l_2104 = (-1L);
            int i, j, k;
            for (i = 0; i < 9; i++)
                l_2076[i] = 0L;
        }
        (***g_1458) = (safe_mul_func_float_f_f((safe_add_func_float_f_f(((p_34.f0 != (safe_sub_func_float_f_f((((safe_rshift_func_int8_t_s_s((safe_add_func_int8_t_s_s((*g_1058), (safe_unary_minus_func_uint32_t_u(((((void*)0 == (***g_1457)) | ((*l_1966) |= (((*l_2120) = &l_1927[4]) == (void*)0))) <= (&g_647[4][6] != l_2122)))))), (safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((*g_693), (*g_753))), (*g_753))))) < 4L) , p_34.f3), p_34.f3))) != p_34.f4), 0x1.6p+1)), (*g_134)));
    }
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads : g_40 g_2 g_107 g_441 g_442 g_430 g_171 g_254 g_70 g_1055 g_1060 g_849 g_850 g_164 g_564.f0 g_753 g_287.f0 g_186 g_564 g_257 g_134 g_7 g_6 g_1117 g_828 g_172 g_190 g_647 g_693 g_694 g_1457 g_1458 g_1459 g_1328 g_1329 g_582 g_1820.f1 g_244
 * writes: g_40 g_2 g_53 g_899 g_70 g_87 g_164 g_1056 g_1061 g_260 g_7 g_107 g_828 g_244 g_287.f0
 */
static uint8_t * func_35(const uint16_t  p_36)
{ /* block id: 6 */
    int64_t l_38 = 0x81C1D99EA5D840DFLL;
    int32_t *l_39 = &g_40;
    uint32_t ****l_1040 = &g_647[2][4];
    const int16_t **l_1116 = &g_294;
    const int16_t ***l_1115 = &l_1116;
    uint8_t *l_1153[9];
    int8_t **l_1219 = &g_1058;
    uint64_t *l_1245[1][2][4] = {{{&g_260,&g_260,&g_260,&g_260},{&g_260,&g_260,&g_260,&g_260}}};
    uint16_t l_1282 = 0x1C64L;
    int32_t l_1289 = 0x7BD366F5L;
    int32_t l_1296 = 0x060AD034L;
    int32_t l_1299 = 0xE1F33678L;
    int32_t l_1302[9][8] = {{0x6AD97254L,1L,1L,0x6AD97254L,1L,1L,0x6AD97254L,1L},{0x6AD97254L,0x6AD97254L,0x03544D81L,0x6AD97254L,0x6AD97254L,0x03544D81L,0x6AD97254L,0x6AD97254L},{1L,0x6AD97254L,1L,1L,0x6AD97254L,1L,1L,0x6AD97254L},{0x6AD97254L,1L,1L,0x6AD97254L,1L,1L,0x6AD97254L,1L},{0x6AD97254L,0x6AD97254L,0x03544D81L,0x6AD97254L,0x6AD97254L,0x03544D81L,0x6AD97254L,0x6AD97254L},{1L,0x6AD97254L,1L,1L,0x6AD97254L,1L,1L,0x6AD97254L},{0x6AD97254L,1L,1L,0x6AD97254L,1L,0x03544D81L,1L,0x03544D81L},{1L,1L,0x6AD97254L,1L,1L,0x6AD97254L,1L,1L},{0x03544D81L,1L,0x03544D81L,0x03544D81L,1L,0x03544D81L,0x03544D81L,1L}};
    uint64_t *****l_1331 = &g_1328;
    int16_t ***l_1357[8] = {&g_852,&g_852,&g_852,&g_852,&g_852,&g_852,&g_852,&g_852};
    float l_1508 = 0x8.C8421Dp+19;
    int32_t l_1509[7][2] = {{0xC7FAAF21L,0xC7FAAF21L},{0x87BF2457L,0xC7FAAF21L},{0xC7FAAF21L,0x87BF2457L},{0xC7FAAF21L,0xC7FAAF21L},{0x87BF2457L,0xC7FAAF21L},{0xC7FAAF21L,0x87BF2457L},{0xC7FAAF21L,0xC7FAAF21L}};
    struct S0 l_1532 = {-1L,0x202488CF2AD8FC77LL,0x2.C1909Ep+26,9L,-1};
    uint16_t **l_1606 = &g_1557;
    uint16_t ** const *l_1605 = &l_1606;
    uint32_t *l_1643 = (void*)0;
    int8_t l_1652 = 0L;
    const float *l_1663[4];
    const float **l_1662[7][6] = {{&l_1663[3],(void*)0,&l_1663[3],&l_1663[3],&l_1663[3],&l_1663[3]},{(void*)0,(void*)0,&l_1663[3],(void*)0,&l_1663[3],(void*)0},{&l_1663[3],(void*)0,(void*)0,(void*)0,(void*)0,&l_1663[3]},{(void*)0,&l_1663[3],(void*)0,&l_1663[3],(void*)0,(void*)0},{&l_1663[3],&l_1663[3],&l_1663[3],&l_1663[3],(void*)0,&l_1663[3]},{&l_1663[3],(void*)0,&l_1663[3],&l_1663[3],&l_1663[3],&l_1663[3]},{(void*)0,(void*)0,&l_1663[3],(void*)0,&l_1663[3],(void*)0}};
    const float ***l_1661 = &l_1662[1][4];
    uint32_t l_1698 = 0x3B54410EL;
    uint8_t l_1750[8] = {0x0FL,0x0FL,0x0FL,0x0FL,0x0FL,0x0FL,0x0FL,0x0FL};
    struct S0 *l_1844 = &g_1845[1][2];
    union U1 *l_1846 = &g_287[2];
    uint32_t l_1886 = 6UL;
    int32_t *l_1891 = &l_1299;
    int32_t *l_1892 = &l_1302[6][5];
    int32_t *l_1893 = &l_1299;
    int32_t *l_1894 = &l_1302[1][0];
    int32_t *l_1895 = (void*)0;
    int32_t *l_1896 = &l_1289;
    int32_t *l_1897 = (void*)0;
    int32_t *l_1898 = &l_1302[1][0];
    int32_t *l_1899 = &l_1299;
    int32_t *l_1900 = (void*)0;
    int32_t *l_1901 = &l_1296;
    int32_t *l_1902[8][2] = {{&g_70,&l_1302[5][1]},{&g_70,&g_70},{&l_1302[5][1],&g_70},{&g_70,&l_1302[5][1]},{&g_70,&g_70},{&l_1302[5][1],&g_70},{&g_70,&l_1302[5][1]},{&g_70,&g_70}};
    int16_t l_1903 = (-9L);
    uint32_t l_1904 = 0xCBC4C932L;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_1153[i] = &g_287[6].f0;
    for (i = 0; i < 4; i++)
        l_1663[i] = (void*)0;
    (*l_39) ^= l_38;
    if ((g_40 , (*l_39)))
    { /* block id: 8 */
        uint8_t l_1015 = 247UL;
        union U1 l_1027 = {0xF6L};
        union U1 *l_1052 = (void*)0;
        const int32_t *l_1059 = &g_564.f0;
        float l_1084 = 0x5.51174Ep-37;
        const int16_t **l_1114 = &g_294;
        const int16_t ***l_1113[3][9][9] = {{{&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{(void*)0,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,(void*)0},{&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114},{(void*)0,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114}},{{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{(void*)0,(void*)0,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114}},{{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114},{(void*)0,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,&l_1114,&l_1114,(void*)0},{&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114},{&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114},{&l_1114,&l_1114,&l_1114,&l_1114,(void*)0,&l_1114,&l_1114,&l_1114,(void*)0}}};
        const uint16_t *l_1142 = &g_828;
        const uint16_t **l_1141[4][3][6] = {{{&l_1142,&l_1142,&l_1142,(void*)0,(void*)0,(void*)0},{&l_1142,&l_1142,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1142,(void*)0,&l_1142,&l_1142,(void*)0,&l_1142}},{{&l_1142,(void*)0,(void*)0,&l_1142,&l_1142,(void*)0},{&l_1142,(void*)0,&l_1142,&l_1142,&l_1142,&l_1142},{&l_1142,&l_1142,&l_1142,&l_1142,&l_1142,&l_1142}},{{&l_1142,&l_1142,&l_1142,(void*)0,(void*)0,&l_1142},{&l_1142,&l_1142,(void*)0,&l_1142,&l_1142,&l_1142},{&l_1142,&l_1142,&l_1142,&l_1142,&l_1142,(void*)0}},{{&l_1142,&l_1142,&l_1142,(void*)0,&l_1142,&l_1142},{(void*)0,(void*)0,(void*)0,&l_1142,&l_1142,&l_1142},{&l_1142,&l_1142,&l_1142,&l_1142,&l_1142,&l_1142}}};
        const uint16_t ***l_1140 = &l_1141[3][1][3];
        int32_t l_1149 = 0x83E059BDL;
        uint64_t *l_1269 = &g_1197;
        float l_1284 = 0x8.Cp-1;
        int32_t l_1301 = 9L;
        int32_t l_1311 = 0L;
        int32_t l_1312 = 0x974904BAL;
        int32_t l_1313 = 1L;
        int32_t l_1314 = 0x12FA2EA8L;
        int32_t l_1316 = 0x06B1BC29L;
        int32_t l_1318 = (-9L);
        int32_t l_1320 = 0xD9000582L;
        int32_t l_1321[8][4] = {{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL},{0xDEA6460FL,0xE14851BCL,0xDEA6460FL,0xE14851BCL}};
        uint64_t *****l_1330 = &g_1328;
        float ***l_1365[7][3] = {{(void*)0,(void*)0,&g_133[6][0][2]},{&g_133[6][0][2],&g_133[6][0][2],&g_133[2][2][0]},{&g_133[6][0][2],(void*)0,(void*)0},{&g_133[2][2][0],&g_133[3][3][1],&g_133[3][3][4]},{&g_133[6][0][2],&g_133[6][0][2],&g_133[6][0][2]},{&g_133[6][0][2],&g_133[2][2][0],&g_133[3][3][4]},{(void*)0,(void*)0,(void*)0}};
        uint64_t * const *l_1390[2][4][6] = {{{&g_1336,&l_1245[0][1][2],&l_1245[0][1][2],&g_1336,&l_1245[0][0][3],&g_1336},{&g_1336,&l_1245[0][0][3],&g_1336,&l_1245[0][1][2],&l_1245[0][1][2],&g_1336},{&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2]},{&l_1245[0][1][2],&l_1245[0][0][3],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][0][3],&l_1245[0][1][2]}},{{&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2]},{&g_1336,&l_1245[0][1][2],&l_1245[0][1][2],&g_1336,&l_1245[0][0][3],&g_1336},{&g_1336,&l_1245[0][0][3],&g_1336,&l_1245[0][1][2],&l_1245[0][1][2],&g_1336},{&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2],&l_1245[0][1][2]}}};
        uint64_t * const **l_1389 = &l_1390[1][2][5];
        uint64_t * const ***l_1388[6][6][7] = {{{&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,(void*)0,(void*)0,&l_1389},{&l_1389,(void*)0,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,(void*)0}},{{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,(void*)0,(void*)0,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,(void*)0,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389}},{{&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389},{&l_1389,(void*)0,&l_1389,(void*)0,&l_1389,(void*)0,&l_1389},{(void*)0,(void*)0,(void*)0,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{(void*)0,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389}},{{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,(void*)0,(void*)0},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,(void*)0,(void*)0,&l_1389,&l_1389}},{{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,(void*)0},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389}},{{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,(void*)0,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389},{&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389,&l_1389}}};
        uint64_t * const ****l_1387 = &l_1388[3][0][6];
        int8_t ***l_1394 = &l_1219;
        int32_t * const **l_1477 = (void*)0;
        int32_t * const *** const l_1476 = &l_1477;
        uint16_t l_1552 = 0UL;
        uint8_t l_1593 = 0x1EL;
        uint64_t l_1636 = 0UL;
        int i, j, k;
        for (g_2 = 1; (g_2 >= (-8)); g_2 = safe_sub_func_int64_t_s_s(g_2, 2))
        { /* block id: 11 */
            union U1 l_56[2][7] = {{{4UL},{0UL},{0UL},{4UL},{0UL},{0UL},{4UL}},{{1UL},{0UL},{1UL},{1UL},{0UL},{1UL},{1UL}}};
            int32_t l_1026 = 1L;
            float *l_1051 = &g_7[1];
            const int8_t l_1085 = 0x59L;
            uint64_t l_1110 = 1UL;
            uint8_t *l_1118 = (void*)0;
            int i, j;
            if ((((*l_39) , g_2) ^ p_36))
            { /* block id: 12 */
                int32_t *l_1002 = &g_70;
                struct S0 l_1083 = {0xCFA89CA6L,0x9BD0990BF20E146ALL,0x6.5p-1,0x856DL,0};
                for (l_38 = 23; (l_38 > 6); l_38--)
                { /* block id: 15 */
                    int32_t l_47 = 0xA7C76986L;
                    for (g_40 = 0; (g_40 != 19); ++g_40)
                    { /* block id: 18 */
                        uint32_t *l_52 = &g_53;
                        union U1 *l_1001 = &l_56[1][1];
                        (*g_107) = ((l_47 & p_36) ^ (((*l_1001) = func_48(p_36, ((*l_52) = 0xB13B1067L), ((0xB7BFL > (safe_mul_func_int16_t_s_s(p_36, 0xE6B8L))) , l_56[0][3]))) , p_36));
                    }
                }
                for (g_87 = 0; (g_87 < (-30)); --g_87)
                { /* block id: 538 */
                    int8_t *l_1041 = (void*)0;
                    int8_t *l_1042 = (void*)0;
                    int32_t l_1043 = 1L;
                    int8_t ** const l_1054 = &l_1041;
                    int8_t ** const *l_1053 = &l_1054;
                    uint64_t *l_1067 = &g_260;
                    uint8_t *l_1082 = &l_1027.f0;
                    int16_t **l_1086 = &g_254;
                    if ((safe_mul_func_int8_t_s_s((*g_441), (safe_mod_func_uint8_t_u_u((0xAB03L | (safe_div_func_uint64_t_u_u((&g_647[0][3] == l_1040), (((l_1043 = p_36) | (((((*g_430) , (safe_div_func_int64_t_s_s(((((*l_39) ^= ((safe_lshift_func_int16_t_s_s(((~((*l_1002) &= ((*g_254) = (p_36 & 0x1C9B7B7523CF9AD2LL)))) <= (l_1051 == (void*)0)), l_1026)) == l_1027.f0)) < p_36) >= (*g_441)), 18446744073709551615UL))) < 18446744073709551613UL) && 18446744073709551615UL) , p_36)) || p_36)))), 1L)))))
                    { /* block id: 543 */
                        l_1052 = &l_56[1][4];
                    }
                    else
                    { /* block id: 545 */
                        (*g_1055) = l_1053;
                        (*g_1060) = l_1059;
                    }
                    (*g_107) &= (((safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((~((((*l_1067) = p_36) | (!((safe_mul_func_uint16_t_u_u(((((safe_div_func_uint8_t_u_u(0x9EL, (~(0xB2B286FB82C16174LL ^ (safe_mul_func_uint16_t_u_u(((((safe_lshift_func_int16_t_s_s(((***g_849) ^= l_1043), 7)) || ((*l_1059) | ((*l_39) < ((safe_mul_func_int8_t_s_s(p_36, (safe_div_func_uint8_t_u_u(((*l_1082) |= (*g_753)), (l_1083 , (-2L)))))) | p_36)))) > 0UL) > 0x343A6767L), g_186[4][6][0])))))) < p_36) , 0x0D16L) & p_36), p_36)) > 0x8706L))) ^ p_36)) ^ l_56[0][3].f0), l_1085)), p_36)) , (*g_849)) != l_1086);
                }
                (*g_6) = (safe_sub_func_float_f_f(0x9.BD0A79p+67, (safe_add_func_float_f_f((safe_mul_func_float_f_f((-0x4.5p+1), (safe_mul_func_float_f_f((((safe_mul_func_float_f_f(((p_36 >= (safe_sub_func_float_f_f((g_564 , (l_1085 != (safe_add_func_float_f_f(p_36, p_36)))), (safe_add_func_float_f_f(((p_36 < (safe_div_func_float_f_f(((((((+(safe_div_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((0UL <= (-9L)), 4UL)), g_257))) || (-6L)) , l_1026) != l_1110) > (*g_134)) == l_1110), 0x9.41AD59p-83))) == (*l_39)), (*g_134)))))) <= l_1026), (*g_6))) < p_36) > p_36), (*g_134))))), p_36))));
            }
            else
            { /* block id: 555 */
                const int16_t **l_1112 = &g_294;
                const int16_t ***l_1111 = &l_1112;
                l_1115 = (l_1113[2][0][0] = l_1111);
                (*g_1117) = l_39;
                return l_1118;
            }
        }
    }
    else
    { /* block id: 790 */
        uint32_t *l_1641 = &g_257;
        uint32_t *l_1642[9][2] = {{&g_257,&g_257},{&g_257,(void*)0},{&g_257,&g_257},{&g_257,(void*)0},{&g_257,&g_257},{&g_257,(void*)0},{&g_257,&g_257},{&g_257,(void*)0},{&g_257,&g_257}};
        struct S0 l_1649 = {0x0CC02513L,2L,0x0.6030EDp+19,0xAC22L,1};
        float ** const *l_1664 = &g_133[6][0][2];
        int32_t l_1694 = 1L;
        uint64_t l_1734 = 0UL;
        uint8_t **l_1744[4][5][1] = {{{&l_1153[5]},{&l_1153[5]},{&g_753},{(void*)0},{&g_753}},{{&g_753},{&g_753},{(void*)0},{&g_753},{&l_1153[5]}},{{&l_1153[5]},{&g_753},{(void*)0},{&g_753},{&g_753}},{{&g_753},{(void*)0},{&g_753},{&l_1153[5]},{&l_1153[5]}}};
        int32_t l_1746 = 0x8F57EB1AL;
        int32_t l_1749 = 0xEEEAFB5CL;
        uint32_t l_1784 = 0x78C80BB8L;
        struct S0 *l_1819 = &g_1820[1];
        int32_t *l_1822 = (void*)0;
        uint32_t ***l_1840 = &g_648;
        int i, j, k;
    }
    for (g_828 = 0; (g_828 <= 7); g_828 += 1)
    { /* block id: 895 */
        struct S0 l_1859 = {0x099442E3L,-2L,0x9.Ep-1,0x54BEL,-0};
        uint64_t ** const l_1861[8][8][4] = {{{&g_1336,&g_1336,&g_728,&l_1245[0][1][2]},{(void*)0,&g_1336,&l_1245[0][1][2],&g_1336},{&g_1336,&g_1336,&l_1245[0][1][2],&l_1245[0][0][3]},{(void*)0,&g_1336,&g_728,&g_1336},{&g_1336,&g_728,&l_1245[0][1][2],&l_1245[0][1][2]},{&l_1245[0][1][2],&l_1245[0][1][2],&g_728,&g_1336},{&g_1336,&g_728,&g_1336,(void*)0},{&l_1245[0][0][3],&l_1245[0][1][2],&g_1336,&g_1336}},{{&g_1336,&l_1245[0][1][2],&g_1336,(void*)0},{&l_1245[0][1][2],&g_728,&g_1336,&g_1336},{&g_1336,&l_1245[0][1][2],&l_1245[0][0][3],&l_1245[0][1][2]},{&l_1245[0][0][3],&g_728,(void*)0,&g_1336},{&l_1245[0][1][3],&g_1336,&l_1245[0][1][2],&l_1245[0][0][3]},{&l_1245[0][0][3],&g_1336,&g_728,&g_1336},{&l_1245[0][0][3],&g_1336,&l_1245[0][1][2],&l_1245[0][1][2]},{&l_1245[0][1][3],&g_1336,(void*)0,&g_1336}},{{&l_1245[0][0][3],&l_1245[0][0][3],&l_1245[0][0][3],&l_1245[0][0][3]},{&g_1336,(void*)0,&g_1336,&l_1245[0][1][3]},{&l_1245[0][1][2],&l_1245[0][1][2],&g_1336,&l_1245[0][0][3]},{&g_1336,&g_728,&g_1336,&l_1245[0][0][3]},{&l_1245[0][0][3],&l_1245[0][1][2],&g_1336,&l_1245[0][1][3]},{&g_1336,(void*)0,&g_728,&l_1245[0][0][3]},{&l_1245[0][1][2],&l_1245[0][0][3],&l_1245[0][1][2],&g_1336},{&g_1336,&g_1336,&g_728,&l_1245[0][1][2]}},{{(void*)0,&g_1336,&l_1245[0][1][2],&g_1336},{&g_1336,&g_1336,&l_1245[0][1][2],&l_1245[0][0][3]},{(void*)0,&g_1336,&g_728,&g_1336},{&g_1336,&g_728,&l_1245[0][1][2],&l_1245[0][1][2]},{&l_1245[0][1][2],&l_1245[0][1][2],&g_728,&g_1336},{&g_1336,&g_728,&g_1336,(void*)0},{&l_1245[0][0][3],&l_1245[0][1][2],&g_1336,&g_1336},{&g_1336,&l_1245[0][1][2],&g_1336,&l_1245[0][0][3]}},{{&l_1245[0][1][3],&g_1336,&g_1336,&l_1245[0][1][2]},{&g_1336,&l_1245[0][1][2],&g_728,&l_1245[0][1][2]},{&l_1245[0][1][2],&g_728,&l_1245[0][0][3],&l_1245[0][1][2]},{&g_1336,&g_728,&l_1245[0][1][2],&g_1336},{&g_728,&g_728,&l_1245[0][0][3],&g_1336},{&g_728,&g_1336,&l_1245[0][1][2],&l_1245[0][1][3]},{&g_1336,&g_1336,&l_1245[0][0][3],&g_1336},{&l_1245[0][1][2],&g_728,&g_728,&l_1245[0][1][2]}},{{&g_1336,&l_1245[0][0][3],&g_1336,&g_1336},{&l_1245[0][1][3],&l_1245[0][1][2],&g_1336,&g_728},{&g_1336,&l_1245[0][0][3],&g_728,&g_728},{&g_1336,&l_1245[0][1][2],&g_728,&g_1336},{&l_1245[0][1][2],&l_1245[0][0][3],&g_728,&l_1245[0][1][2]},{&l_1245[0][1][2],&g_728,&l_1245[0][1][2],&g_1336},{&l_1245[0][1][2],&g_1336,&g_1336,&l_1245[0][1][3]},{&l_1245[0][0][3],&g_1336,&l_1245[0][1][3],&g_1336}},{{&g_728,&g_728,&l_1245[0][1][3],&g_1336},{&l_1245[0][0][3],&g_728,&g_1336,&l_1245[0][1][2]},{&l_1245[0][1][2],&g_728,&l_1245[0][1][2],&l_1245[0][1][2]},{&l_1245[0][1][2],&l_1245[0][1][2],&g_728,&l_1245[0][1][2]},{&l_1245[0][1][2],&g_1336,&g_728,&l_1245[0][0][3]},{&g_1336,&l_1245[0][1][3],&g_728,&g_728},{&g_1336,&l_1245[0][1][3],&g_1336,&l_1245[0][0][3]},{&l_1245[0][1][3],&g_1336,&g_1336,&l_1245[0][1][2]}},{{&g_1336,&l_1245[0][1][2],&g_728,&l_1245[0][1][2]},{&l_1245[0][1][2],&g_728,&l_1245[0][0][3],&l_1245[0][1][2]},{&g_1336,&g_728,&l_1245[0][1][2],&g_1336},{&g_728,&g_728,&l_1245[0][0][3],&g_1336},{&g_728,&g_1336,&l_1245[0][1][2],&l_1245[0][1][3]},{&g_1336,&g_1336,&l_1245[0][0][3],&g_1336},{&l_1245[0][1][2],&g_728,&g_728,&l_1245[0][1][2]},{&g_1336,&l_1245[0][0][3],&g_1336,&g_1336}}};
        uint64_t ** const *l_1860 = &l_1861[7][6][2];
        const uint64_t ****l_1867 = (void*)0;
        const uint64_t *****l_1866 = &l_1867;
        int8_t **l_1881 = &g_1058;
        const uint32_t l_1890 = 4294967295UL;
        int i, j, k;
        if (g_172[g_828])
            break;
        if ((**g_190))
            break;
        (****g_1457) = (((*l_1040) == (((0UL < (*g_693)) > (p_36 ^ (((!(*l_39)) != ((+(g_172[g_828] , ((&g_287[4] == l_1846) > (((safe_rshift_func_uint16_t_u_u((((safe_mul_func_int16_t_s_s((p_36 == 0x8BBF1C3DFC9EC105LL), 0xE07CL)) && (*l_39)) || 0x57C1L), p_36)) && g_172[g_828]) != 6L)))) == 0x09L)) | 0xA14090BCL))) , (void*)0)) == p_36);
        for (g_244 = 1; (g_244 <= 6); g_244 += 1)
        { /* block id: 901 */
            const uint64_t * const l_1872 = &g_260;
            const uint64_t * const *l_1871 = &l_1872;
            const uint64_t * const **l_1870[7][4][1] = {{{&l_1871},{&l_1871},{&l_1871},{&l_1871}},{{&l_1871},{&l_1871},{&l_1871},{&l_1871}},{{&l_1871},{&l_1871},{&l_1871},{&l_1871}},{{&l_1871},{&l_1871},{&l_1871},{&l_1871}},{{&l_1871},{&l_1871},{&l_1871},{&l_1871}},{{&l_1871},{&l_1871},{&l_1871},{&l_1871}},{{&l_1871},{&l_1871},{&l_1871},{&l_1871}}};
            const uint64_t * const *** const l_1869 = &l_1870[0][1][0];
            const uint64_t * const *** const * const l_1868 = &l_1869;
            int32_t l_1873[4] = {6L,6L,6L,6L};
            int8_t ***l_1880 = &l_1219;
            int32_t l_1889 = 0x3210F56FL;
            int i, j, k;
            l_1873[0] = ((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_div_func_float_f_f(((((l_1859 , (*g_1328)) != l_1860) > (*g_582)) , (safe_sub_func_float_f_f((safe_mul_func_float_f_f(((l_1866 == l_1868) < (*l_39)), (p_36 == ((*l_39) < 0x1.3DA789p-3)))), (*l_39)))), p_36)), (****g_1457))), p_36)) <= 0xB.1C565Cp+1);
            l_1302[(g_244 + 2)][(g_244 + 1)] = (safe_rshift_func_int16_t_s_u(((safe_unary_minus_func_uint64_t_u(((((!(-7L)) == (safe_div_func_int8_t_s_s((((((((*l_1880) = (void*)0) != l_1881) <= 0xEBC6420FL) | (safe_rshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((((*l_1866) != (void*)0) , l_1886), (l_1873[1] = ((*g_753) &= (safe_rshift_func_int8_t_s_u((*l_39), 0)))))), 12))) && ((&l_1115 != (void*)0) == p_36)) == (*l_39)), l_1889))) | l_1890) ^ g_1820[1].f1))) ^ 1L), 9));
        }
    }
    l_1904--;
    return &g_186[7][8][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_899 g_107
 * writes: g_899 g_70
 */
static union U1  func_48(uint64_t  p_49, uint32_t  p_50, union U1  p_51)
{ /* block id: 20 */
    float *l_601[10];
    float *l_602 = (void*)0;
    uint32_t *l_616 = &g_53;
    int32_t l_642 = (-1L);
    uint64_t **l_761 = &g_728;
    uint64_t ***l_760 = &l_761;
    uint64_t ****l_759 = &l_760;
    uint16_t l_780 = 0x24FAL;
    uint32_t l_797 = 0x3B3DFAF6L;
    int32_t l_804 = 0L;
    int32_t l_809 = 0x08A46F1AL;
    int32_t l_813 = (-1L);
    int32_t l_814 = 0L;
    int32_t l_822 = 1L;
    int32_t l_827 = 1L;
    uint16_t *l_915 = &l_780;
    uint16_t **l_914 = &l_915;
    int32_t *l_939[6][8][5] = {{{&l_827,&g_40,(void*)0,&g_70,&l_809},{&g_40,(void*)0,&l_813,&g_70,&l_813},{&l_642,&g_40,&l_804,&l_813,&l_809},{&l_642,&l_822,&l_814,&l_642,&g_40},{&l_822,&g_40,&l_827,&l_809,&l_809},{(void*)0,&g_40,&l_827,&g_40,&l_809},{&l_809,&g_40,&l_809,(void*)0,&g_70},{&l_813,&l_813,&l_822,&l_814,&l_813}},{{&g_70,&g_40,&l_642,&g_40,&g_70},{&l_642,&l_814,(void*)0,&l_804,&l_809},{&g_70,&l_642,&l_809,&g_70,(void*)0},{(void*)0,&l_822,&g_40,(void*)0,(void*)0},{&l_642,&g_40,&l_642,&l_804,&l_809},{&l_813,(void*)0,&l_642,&l_642,(void*)0},{(void*)0,&g_70,&g_40,(void*)0,&g_70},{(void*)0,&l_814,&l_642,(void*)0,&l_822}},{{(void*)0,(void*)0,&l_642,&g_40,&l_642},{&l_814,&l_642,&g_40,&l_809,(void*)0},{&l_809,&g_40,&l_809,&l_809,&g_40},{(void*)0,&l_642,(void*)0,&l_642,&g_70},{&g_40,(void*)0,&l_642,&l_827,(void*)0},{&g_70,(void*)0,&l_822,&l_809,&l_822},{&g_40,(void*)0,&l_809,&g_70,&l_642},{(void*)0,&l_813,(void*)0,&l_804,&l_809}},{{&l_809,&l_813,(void*)0,&g_40,&l_809},{&l_814,&g_70,&g_70,&l_814,&l_809},{(void*)0,(void*)0,&l_642,&l_809,&l_813},{(void*)0,(void*)0,&l_809,&l_642,&l_809},{(void*)0,&g_40,&l_809,&l_809,&l_642},{&l_813,&l_642,&l_822,&l_814,&l_642},{&l_642,&g_40,&l_642,&g_40,&g_70},{(void*)0,&l_822,(void*)0,&l_804,(void*)0}},{{&g_70,&g_70,(void*)0,&g_70,&l_813},{&l_642,&l_822,&l_827,&l_809,(void*)0},{&g_70,&l_822,&l_642,&l_827,(void*)0},{&l_813,&l_822,&l_642,&l_642,&l_809},{&l_809,&g_70,&l_642,&l_809,&g_70},{&l_809,&l_822,&l_642,&l_809,(void*)0},{(void*)0,&g_40,&l_813,&g_40,(void*)0},{&l_822,&l_642,&l_827,(void*)0,(void*)0}},{{&g_70,&g_40,&l_809,(void*)0,&g_40},{(void*)0,(void*)0,(void*)0,&l_642,(void*)0},{(void*)0,(void*)0,&l_642,&l_804,(void*)0},{(void*)0,&g_70,&l_822,(void*)0,(void*)0},{&l_822,&l_642,&g_70,&g_40,&g_40},{&l_813,(void*)0,&l_813,&l_642,(void*)0},{&l_642,&l_642,&l_809,&l_809,&l_822},{&g_70,&l_804,&l_804,&g_70,&l_813}}};
    int16_t l_997 = 0x0266L;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_601[i] = &g_171;
    for (p_51.f4 = 0; (p_51.f4 == 26); p_51.f4++)
    { /* block id: 23 */
        float *l_597 = &g_171;
        int32_t l_615 = 0x177B1677L;
        int8_t l_617 = (-1L);
        int32_t l_643 = 0xE9CF6B4AL;
        uint32_t l_682 = 0xF803319EL;
        union U1 l_690[4] = {{0xCDL},{0xCDL},{0xCDL},{0xCDL}};
        uint64_t *l_705[1][2];
        uint64_t **l_704 = &l_705[0][0];
        int32_t *l_735 = (void*)0;
        uint32_t ****l_740[10] = {&g_647[4][3],&g_647[2][4],&g_647[2][4],&g_647[2][4],&g_647[4][3],&g_647[4][3],&g_647[2][4],&g_647[2][4],&g_647[2][4],&g_647[4][3]};
        uint64_t ** const *l_757 = &l_704;
        uint64_t ** const **l_756 = &l_757;
        int32_t l_810 = 4L;
        int32_t l_816 = 5L;
        int32_t l_820[2][7] = {{0x592F4CDAL,0x592F4CDAL,0x592F4CDAL,0x592F4CDAL,0x592F4CDAL,0x592F4CDAL,0x592F4CDAL},{0x522372EDL,(-1L),0x522372EDL,(-1L),0x522372EDL,(-1L),0x522372EDL}};
        int16_t *l_839 = (void*)0;
        struct S0 l_916[4][4][3] = {{{{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1},{2L,-1L,0xC.112F95p-58,0xCF06L,-1},{2L,-1L,0xC.112F95p-58,0xCF06L,-1}},{{-1L,1L,0x0.6p+1,0L,-0},{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0},{0x41BB4846L,-2L,0x1.5p-1,1L,-0}},{{0x6C581771L,0xEFE2670225957D3FLL,0x2.Dp-1,-8L,-0},{1L,4L,0x0.6p+1,1L,-0},{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0}},{{0x6C581771L,0xEFE2670225957D3FLL,0x2.Dp-1,-8L,-0},{-1L,1L,0x9.872919p-23,0L,0},{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1}}},{{{-1L,1L,0x0.6p+1,0L,-0},{0xC9FF9B43L,-10L,0x0.399E2Bp-9,0x6389L,1},{-1L,1L,0x0.6p+1,0L,-0}},{{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1},{-1L,1L,0x9.872919p-23,0L,0},{0x6C581771L,0xEFE2670225957D3FLL,0x2.Dp-1,-8L,-0}},{{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0},{1L,4L,0x0.6p+1,1L,-0},{0x6C581771L,0xEFE2670225957D3FLL,0x2.Dp-1,-8L,-0}},{{0x41BB4846L,-2L,0x1.5p-1,1L,-0},{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0},{-1L,1L,0x0.6p+1,0L,-0}}},{{{2L,-1L,0xC.112F95p-58,0xCF06L,-1},{2L,-1L,0xC.112F95p-58,0xCF06L,-1},{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1}},{{0x41BB4846L,-2L,0x1.5p-1,1L,-0},{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1},{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0}},{{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0},{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1},{0x41BB4846L,-2L,0x1.5p-1,1L,-0}},{{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1},{2L,-1L,0xC.112F95p-58,0xCF06L,-1},{2L,-1L,0xC.112F95p-58,0xCF06L,-1}}},{{{-1L,1L,0x0.6p+1,0L,-0},{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0},{0x41BB4846L,-2L,0x1.5p-1,1L,-0}},{{0x6C581771L,0xEFE2670225957D3FLL,0x2.Dp-1,-8L,-0},{1L,4L,0x0.6p+1,1L,-0},{1L,0x06E659B6BFA21E50LL,0x6.0p+1,0x2CCCL,0}},{{0x6C581771L,0xEFE2670225957D3FLL,0x2.Dp-1,-8L,-0},{-1L,1L,0x9.872919p-23,0L,0},{0x9F2F6EA7L,-6L,-0x5.9p+1,0x0701L,-1}},{{-1L,1L,0x0.6p+1,0L,-0},{0xC9FF9B43L,-10L,0x0.399E2Bp-9,0x6389L,1},{-1L,1L,0x0.6p+1,0L,-0}}}};
        const uint64_t ***l_933 = (void*)0;
        const uint64_t ****l_932 = &l_933;
        const uint64_t *****l_931 = &l_932;
        int64_t l_946 = 0xC181D667D3863CC3LL;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_705[i][j] = &g_260;
        }
    }
    for (p_51.f3 = 0; (p_51.f3 != 26); ++p_51.f3)
    { /* block id: 505 */
        uint8_t l_998 = 0x39L;
        for (g_899 = 0; (g_899 != 42); g_899 = safe_add_func_uint8_t_u_u(g_899, 6))
        { /* block id: 508 */
            (*g_107) = p_51.f1;
        }
        ++l_998;
    }
    return p_51;
}


/* ------------------------------------------ */
/* 
 * reads : g_89 g_87 g_88 g_86 g_172 g_256 g_257 g_127 g_260 g_265 g_70 g_134 g_287 g_164 g_293 g_66 g_254 g_323 g_186 g_238 g_294 g_177 g_85 g_6 g_582
 * writes: g_89 g_87 g_66 g_107 g_244 g_254 g_130 g_256 g_257 g_260 g_186 g_7 g_294 g_238 g_172 g_70 g_88 g_164 g_127
 */
static int32_t * func_63(uint64_t  p_64)
{ /* block id: 32 */
    int32_t *l_69 = &g_70;
    int32_t *l_71 = &g_70;
    int32_t *l_72 = &g_70;
    int32_t l_73 = 0x35309F33L;
    int32_t l_74 = (-1L);
    int32_t *l_75 = &l_73;
    int32_t *l_76 = &g_70;
    int32_t *l_78 = &l_73;
    int32_t *l_79 = &l_73;
    int32_t *l_80 = &g_70;
    int32_t l_81 = 0L;
    int32_t *l_82 = &g_70;
    int32_t *l_83 = &l_74;
    int32_t *l_84[5][8][2] = {{{&l_73,&l_73},{&g_70,&l_81},{(void*)0,&g_70},{&l_73,(void*)0},{&l_73,&g_70},{(void*)0,&l_81},{&g_70,&l_73},{&l_73,&l_74}},{{(void*)0,(void*)0},{(void*)0,&g_70},{(void*)0,(void*)0},{&l_74,&g_70},{(void*)0,(void*)0},{(void*)0,&l_73},{(void*)0,&l_74},{&l_73,&l_81}},{{(void*)0,&g_70},{(void*)0,&l_74},{&l_73,(void*)0},{(void*)0,(void*)0},{&l_73,&l_73},{&l_74,&l_73},{&l_73,(void*)0},{(void*)0,(void*)0}},{{&l_73,&l_74},{(void*)0,&g_70},{(void*)0,&l_81},{&l_73,&l_74},{(void*)0,&l_73},{(void*)0,(void*)0},{(void*)0,&g_70},{&l_74,(void*)0}},{{(void*)0,&g_70},{(void*)0,(void*)0},{(void*)0,&l_74},{&l_73,&l_73},{&g_70,&l_81},{(void*)0,&g_70},{&l_73,(void*)0},{&l_73,&g_70}}};
    int i, j, k;
    ++g_89;
    for (g_87 = 1; (g_87 >= 0); g_87 -= 1)
    { /* block id: 36 */
        float *l_101[10] = {(void*)0,&g_7[3],(void*)0,&g_7[2],&g_7[2],(void*)0,&g_7[3],(void*)0,&g_7[2],&g_7[2]};
        float **l_102 = &l_101[5];
        union U1 l_266 = {0x8AL};
        float *l_267 = &g_7[2];
        uint8_t *l_268 = &g_186[7][6][0];
        int32_t l_581 = 1L;
        int i;
        (*g_6) = (((func_92(func_97(&l_81, ((*l_102) = l_101[3]), g_88[g_87]), l_266, ((*l_268) = (l_267 != l_76)), ((3UL > p_64) , l_84[2][1][0])) , 0xC121528BL) != (-1L)) , l_581);
    }
    return g_582;
}


/* ------------------------------------------ */
/* 
 * reads : g_70 g_134 g_287 g_257 g_260 g_86 g_164 g_293 g_66 g_254 g_88 g_323 g_186 g_172 g_238 g_294 g_265 g_177 g_85
 * writes: g_7 g_294 g_257 g_244 g_260 g_130 g_238 g_172 g_70 g_88 g_164 g_107 g_127 g_186 g_171
 */
static const int32_t  func_92(const int64_t  p_93, union U1  p_94, uint8_t  p_95, float * p_96)
{ /* block id: 136 */
    int32_t *l_269 = &g_70;
    int32_t **l_271[3][5][2] = {{{&g_107,&l_269},{&l_269,&l_269},{&g_107,&g_107},{&l_269,&l_269},{&l_269,&g_107}},{{&l_269,&l_269},{&g_107,&l_269},{&l_269,&g_107},{&l_269,&l_269},{&l_269,&g_107}},{{&l_269,&l_269},{&g_107,&l_269},{&l_269,&g_107},{&l_269,&l_269},{&l_269,&g_107}}};
    int32_t *l_272 = (void*)0;
    uint64_t *l_277 = (void*)0;
    uint32_t l_278[4];
    int16_t *l_288 = &g_88[0];
    int16_t **l_289 = &l_288;
    const int16_t **l_290 = (void*)0;
    const int16_t *l_292 = &g_293;
    const int16_t **l_291[8] = {&l_292,&l_292,&l_292,&l_292,&l_292,&l_292,&l_292,&l_292};
    uint32_t *l_296 = (void*)0;
    uint32_t *l_297 = (void*)0;
    uint32_t *l_298 = &g_257;
    uint8_t *l_356 = &g_287[6].f0;
    uint32_t **l_366 = &l_298;
    uint32_t ***l_365 = &l_366;
    const uint32_t l_392[7][8][4] = {{{2UL,0UL,0x694FDEB0L,1UL},{4294967287UL,0x237D19F8L,0xB16D8A70L,0x324EDFC1L},{0x1642DF28L,0x3490F24CL,1UL,0x3490F24CL},{0x06A90F7BL,1UL,0x3490F24CL,0x05339CE0L},{0x05339CE0L,4294967289UL,4294967295UL,4294967287UL},{0x35DAB81BL,1UL,0xD7E7F9EFL,4294967286UL},{0x35DAB81BL,0x278CB8FEL,4294967295UL,0x4A002CF8L},{0x05339CE0L,4294967286UL,0x3490F24CL,0xB593FC4EL}},{{0x06A90F7BL,4294967295UL,1UL,2UL},{0x1642DF28L,5UL,0xB16D8A70L,0x278CB8FEL},{4294967287UL,0xB16D8A70L,0x694FDEB0L,0x06A90F7BL},{2UL,0x3B1CA12DL,4294967289UL,0x58BECFD6L},{1UL,0x4A002CF8L,0xFB4B6336L,0xFB4B6336L},{4294967289UL,4294967289UL,0xB3C30FAAL,4294967295UL},{0xFB4B6336L,4294967292UL,3UL,0xD7E7F9EFL},{0xA53F2285L,0xF3008731L,0x35DAB81BL,3UL}},{{0xA6782EF5L,0xF3008731L,0x4A002CF8L,0xD7E7F9EFL},{0xF3008731L,4294967292UL,0UL,4294967295UL},{4UL,4294967289UL,0xA6782EF5L,0xFB4B6336L},{4294967289UL,0x4A002CF8L,0x237D19F8L,0x58BECFD6L},{0xB3C30FAAL,0x3B1CA12DL,0x05339CE0L,0x06A90F7BL},{1UL,0xB16D8A70L,4UL,0x278CB8FEL},{4294967295UL,5UL,0x0D868227L,2UL},{1UL,4294967295UL,1UL,0xB593FC4EL}},{{0x237D19F8L,4294967286UL,4294967289UL,0x4A002CF8L},{4294967292UL,0x278CB8FEL,0x324EDFC1L,4294967286UL},{5UL,1UL,0x324EDFC1L,4294967287UL},{4294967292UL,4294967289UL,4294967289UL,0x05339CE0L},{0x237D19F8L,1UL,1UL,0x3490F24CL},{1UL,0x3490F24CL,0x0D868227L,0x324EDFC1L},{4294967295UL,0x237D19F8L,4UL,1UL},{1UL,0UL,0x05339CE0L,1UL}},{{0xB3C30FAAL,1UL,0x237D19F8L,4294967295UL},{4294967289UL,0xA6782EF5L,0xA6782EF5L,4294967289UL},{4UL,0xD7E7F9EFL,0UL,0x237D19F8L},{0xF3008731L,4294967287UL,0x4A002CF8L,0x1642DF28L},{0xA6782EF5L,1UL,0x35DAB81BL,0x1642DF28L},{0xA53F2285L,4294967287UL,3UL,0x237D19F8L},{0xFB4B6336L,0xD7E7F9EFL,0xB3C30FAAL,4294967289UL},{4294967289UL,4294967289UL,0x4A002CF8L,0x237D19F8L}},{{1UL,0x0D868227L,0xA53F2285L,3UL},{4294967287UL,0x4E22AA32L,0x58BECFD6L,1UL},{5UL,0xB593FC4EL,0x3B1CA12DL,0UL},{0x05339CE0L,0x278CB8FEL,3UL,0x278CB8FEL},{1UL,1UL,0x278CB8FEL,4UL},{4UL,0xA53F2285L,0x237D19F8L,5UL},{0xFB4B6336L,0x3490F24CL,0x324EDFC1L,2UL},{0xFB4B6336L,0x694FDEB0L,0x237D19F8L,4294967292UL}},{{4UL,2UL,0x278CB8FEL,1UL},{1UL,0x237D19F8L,3UL,4294967287UL},{0x05339CE0L,0x35DAB81BL,0x3B1CA12DL,0x694FDEB0L},{5UL,0x3B1CA12DL,0x58BECFD6L,1UL},{4294967287UL,1UL,0xA53F2285L,0xA6782EF5L},{1UL,4294967292UL,0x4A002CF8L,0x4A002CF8L},{0xB3C30FAAL,0xB3C30FAAL,1UL,4294967289UL},{0x4A002CF8L,1UL,0x1642DF28L,0x324EDFC1L}}};
    uint64_t l_416 = 18446744073709551615UL;
    const int32_t *l_434 = &g_435;
    uint32_t ***l_520 = &l_366;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_278[i] = 0x817C311DL;
    l_272 = l_269;
    if ((safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_s((((l_277 != &g_66) , (((0x2.3p+1 == (*l_269)) >= l_278[1]) != (safe_add_func_float_f_f(((safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f(((*g_134) = (*l_272)), (g_287[6] , ((g_244 = ((*l_298) |= ((((*l_289) = l_288) == (g_294 = &g_164)) , p_93))) , 0x0.0p+1)))), p_93)), 0x3.35672Ap+71)) >= p_93), p_95)))) , 1L), 7)), 1)))
    { /* block id: 143 */
        int32_t *l_299 = &g_70;
        struct S0 l_315 = {0x13EEEFE5L,-1L,0x5.FD5CD2p+32,0xCA8EL,0};
        int32_t l_318 = (-4L);
        const uint8_t **l_357 = (void*)0;
        const uint8_t *l_359 = &g_360;
        const uint8_t **l_358 = &l_359;
        uint32_t **l_364 = (void*)0;
        uint32_t ***l_363 = &l_364;
        uint32_t ****l_367 = (void*)0;
        uint32_t ****l_368 = &l_365;
        int8_t l_369 = 0x0AL;
        uint64_t *l_382[5];
        uint64_t **l_383 = &l_382[0];
        int i;
        for (i = 0; i < 5; i++)
            l_382[i] = &g_260;
        l_299 = (void*)0;
        for (p_94.f3 = 2; (p_94.f3 <= 6); p_94.f3 += 1)
        { /* block id: 147 */
            int8_t l_317 = 1L;
            int32_t l_335 = 5L;
            int8_t l_338 = 1L;
            const int8_t l_339 = 0x63L;
            for (g_260 = 1; (g_260 <= 6); g_260 += 1)
            { /* block id: 150 */
                int i;
                return g_86[g_260];
            }
            for (g_130 = 0; (g_130 <= 6); g_130 += 1)
            { /* block id: 155 */
                float l_336[9] = {(-0x4.0p+1),0x1.0p+1,0x1.0p+1,(-0x4.0p+1),0x1.0p+1,0x1.0p+1,(-0x4.0p+1),0x1.0p+1,0x1.0p+1};
                int32_t l_337 = 0x0B88CEE2L;
                uint32_t **l_343 = &l_296;
                int i;
                for (g_260 = 1; (g_260 <= 6); g_260 += 1)
                { /* block id: 158 */
                    int8_t l_303 = 0x4AL;
                    int32_t *l_310 = &g_70;
                    for (g_238 = 0; (g_238 <= 7); g_238 += 1)
                    { /* block id: 161 */
                        uint8_t *l_302[10] = {&g_172[5],&g_186[5][7][1],&g_186[5][7][1],&g_172[5],&g_186[5][7][1],&g_186[5][7][1],&g_172[5],&g_186[5][7][1],&g_186[5][7][1],&g_172[5]};
                        struct S0 l_316[6][9] = {{{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1}},{{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1}},{{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1}},{{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1}},{{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1},{-9L,0xA0CBD0F30A5ECD7FLL,0x4.A106F1p+79,-8L,0},{-1L,7L,-0x5.6p+1,0L,1}},{{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1},{1L,-1L,0xD.A630ADp+31,0x8A76L,-1}}};
                        int i, j;
                        (*l_269) |= (safe_rshift_func_uint8_t_u_u(((-6L) >= (-3L)), ((g_172[5] = g_86[p_94.f3]) , 0UL)));
                        (*l_272) ^= (l_303 &= g_86[p_94.f3]);
                        l_318 |= (safe_add_func_uint64_t_u_u(g_86[g_260], (safe_sub_func_uint16_t_u_u((((p_94.f0 >= (safe_sub_func_uint8_t_u_u((l_299 != (l_310 = (void*)0)), (((*l_269) = (safe_div_func_int64_t_s_s((safe_add_func_uint16_t_u_u(p_94.f1, (l_315 , (l_316[3][1] , (p_94.f2 && (g_164 && 0UL)))))), g_293))) ^ l_317)))) , g_70) >= g_66), (*g_254)))));
                        l_337 = ((safe_rshift_func_uint8_t_u_u((l_338 = (safe_add_func_uint8_t_u_u(((void*)0 != g_323), ((safe_lshift_func_int8_t_s_s(((+(g_186[4][6][0] >= ((((safe_mul_func_uint16_t_u_u((l_316[3][1].f4 ^= ((~18446744073709551615UL) && ((*l_288) &= ((*l_272) = (l_335 = (g_172[6] >= ((safe_div_func_int8_t_s_s(g_257, (safe_div_func_uint32_t_u_u((((p_94.f2 | p_95) ^ (g_287[(p_94.f3 + 1)] , 1L)) != g_186[5][6][0]), p_94.f4)))) ^ (-1L)))))))), 65535UL)) | 246UL) > p_94.f4) == l_337))) <= p_95), g_238)) < l_316[3][1].f1)))), 7)) <= 0xCB9BC567L);
                    }
                    for (g_164 = 6; (g_164 >= 0); g_164 -= 1)
                    { /* block id: 178 */
                        const uint32_t **l_342[2][2];
                        int32_t l_346 = 0x7BF5D305L;
                        int i, j;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_342[i][j] = (void*)0;
                        }
                        (*l_272) |= l_339;
                        (*l_269) ^= (safe_sub_func_int64_t_s_s(((l_342[0][0] != l_343) == ((*l_298)++)), (((*g_294) && 0x51EDL) < (1L <= 1UL))));
                        if (p_95)
                            goto lbl_384;
                        if (l_346)
                            break;
                        if (l_337)
                            break;
                    }
                    if (p_94.f4)
                        continue;
                }
            }
            (*g_265) = p_96;
        }
lbl_384:
        (*l_269) = ((+(safe_rshift_func_uint8_t_u_u(p_94.f4, 1))) <= (safe_add_func_int8_t_s_s((safe_mod_func_int8_t_s_s((((safe_rshift_func_int8_t_s_s(((g_70 , l_356) != ((*l_358) = &g_4)), (safe_sub_func_int16_t_s_s((*g_294), ((*g_294) ^ ((l_369 = (l_363 == ((*l_368) = l_365))) ^ (((safe_div_func_uint8_t_u_u((safe_add_func_int8_t_s_s((safe_sub_func_int32_t_s_s((safe_rshift_func_int8_t_s_s(((safe_rshift_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((((*l_383) = l_382[0]) == (void*)0), p_94.f2)), 8)) >= p_93), 7)), p_93)), 0xE3L)), g_177)) <= 0x05EAL) <= p_94.f1))))))) & 1UL) == p_93), p_94.f2)), 0xA4L)));
        (*l_272) = (-1L);
    }
    else
    { /* block id: 197 */
        uint32_t l_411 = 6UL;
        int32_t l_462 = 3L;
        uint32_t l_463 = 0x7F9BC2ABL;
        uint32_t *** const *l_501 = (void*)0;
        float * const *l_514 = &g_134;
        int32_t *l_546 = &g_70;
        int32_t l_569[1];
        int i;
        for (i = 0; i < 1; i++)
            l_569[i] = 0x0EEA8E05L;
        for (g_70 = 0; (g_70 < (-22)); g_70 = safe_sub_func_uint64_t_u_u(g_70, 3))
        { /* block id: 200 */
            uint16_t l_387 = 0x9349L;
            int32_t l_388 = 0x096630E1L;
            const int32_t *l_389 = &l_388;
            l_388 = l_387;
            l_389 = (void*)0;
        }
        for (p_95 = 0; (p_95 <= 40); ++p_95)
        { /* block id: 206 */
            return l_392[6][0][3];
        }
        for (g_127 = 0; (g_127 <= (-9)); g_127--)
        { /* block id: 211 */
            int16_t l_399 = (-9L);
            int16_t **l_404[1];
            int16_t ***l_405 = &l_404[0];
            int16_t ***l_406 = &l_289;
            int i;
            for (i = 0; i < 1; i++)
                l_404[i] = (void*)0;
            (*l_272) &= (safe_sub_func_uint8_t_u_u(0xFCL, (((((safe_rshift_func_int8_t_s_u(((l_399 ^ ((*g_294) ^ ((safe_mod_func_int16_t_s_s(p_93, ((*l_288) = 0x05F0L))) < (safe_mul_func_int8_t_s_s(((((*l_405) = l_404[0]) == ((*l_406) = &g_254)) | ((p_95 || (((((g_186[8][4][0] ^= (safe_sub_func_int64_t_s_s(g_85, p_94.f0))) , p_93) , (void*)0) == &p_95) , 1UL)) > 0x0F6ABB0AD70011B5LL)), 0x61L))))) ^ l_399), 6)) , 0x4299B8E3C385C190LL) > p_93) , p_94.f0) || l_399)));
        }
    }
    return p_93;
}


/* ------------------------------------------ */
/* 
 * reads : g_66 g_86 g_172 g_256 g_257 g_127 g_260 g_265
 * writes: g_66 g_107 g_244 g_254 g_130 g_256 g_257 g_260
 */
static const int64_t  func_97(int32_t * p_98, float * p_99, int64_t  p_100)
{ /* block id: 38 */
    int32_t *l_105 = (void*)0;
    struct S0 l_108[6][9][4] = {{{{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0},{1L,0x2D3DACF38B77237ELL,0x1.Dp-1,1L,0},{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0},{0x60882539L,0x82EC59D2773D60B3LL,-0x10.5p-1,0xC227L,-1}},{{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1},{0x1B1B7DE0L,-2L,0x0.3p+1,0x202AL,0},{0xA8CD555FL,1L,0x2.1E4F29p-53,-4L,-0},{0L,1L,0x1.Cp-1,0x6C13L,-0}},{{1L,0L,-0x5.Fp-1,-7L,0},{-1L,0x6F2DC12F6744ED4CLL,0xA.6105BDp-7,0x7F54L,-1},{0L,1L,0x7.Fp-1,0xB6E6L,-0},{0x1B1B7DE0L,-2L,0x0.3p+1,0x202AL,0}},{{0xE40E7BB6L,0x3936D73F18804D59LL,-0x5.2p+1,0x8D45L,0},{-1L,0x81B6A48933AC5CEDLL,0xA.C48AE5p-81,-8L,1},{0L,1L,0x7.Fp-1,0xB6E6L,-0},{-1L,-3L,0x0.Bp-1,2L,1}},{{1L,0L,-0x5.Fp-1,-7L,0},{1L,0xB14249AA0F805AAALL,0xD.7F8389p-8,0x964CL,1},{0xA8CD555FL,1L,0x2.1E4F29p-53,-4L,-0},{0xD51E7DF7L,5L,-0x1.9p-1,-3L,-1}},{{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1},{-2L,-1L,-0x1.4p-1,0x48DCL,-1},{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0},{0xE40E7BB6L,0x3936D73F18804D59LL,-0x5.2p+1,0x8D45L,0}},{{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0},{0xE40E7BB6L,0x3936D73F18804D59LL,-0x5.2p+1,0x8D45L,0},{0x9FC80027L,0xE99C33BC337E5C35LL,-0x1.7p+1,-9L,-1},{0x2AE489F4L,0x95DAE69F5B7F4FEELL,0x1.5p+1,0x3AC8L,1}},{{0xEA10F584L,1L,0x0.677A08p+79,0x8F7CL,-0},{0x1F537715L,1L,-0x1.Cp+1,0xCFAAL,-0},{0L,1L,-0x9.Ap+1,-2L,-0},{0x9FC80027L,0xE99C33BC337E5C35LL,-0x1.7p+1,-9L,-1}},{{0xAA5094A5L,2L,0x3.2663EEp+29,0L,0},{0xDF585002L,-5L,0x1.7EF146p-51,-1L,0},{0xC23E9608L,0x0B4F93AD4E58FD31LL,0x8.27AFF6p-61,0L,-1},{0x233E6B62L,-1L,0x5.Ep-1,0x4033L,0}}},{{{0xC119CD2CL,1L,0x4.024FFAp-66,0x2A9EL,1},{0x7E7461C8L,0x171E5B3C0A783F81LL,0x1.Dp+1,3L,0},{0x60882539L,0x82EC59D2773D60B3LL,-0x10.5p-1,0xC227L,-1},{0xBF50803CL,0xA28DA27851ED01B0LL,0x3.182763p+24,0x30C2L,0}},{{-5L,3L,-0x1.0p+1,0L,-0},{-2L,-1L,-0x1.4p-1,0x48DCL,-1},{-1L,0xEA63F3D54CDCAC19LL,0xA.C0CAA6p-38,-1L,0},{0xA8CD555FL,1L,0x2.1E4F29p-53,-4L,-0}},{{0x506916A6L,3L,0xE.A194CDp-76,8L,-1},{0x222A015EL,1L,0xF.3958A3p-15,0x7497L,-0},{0x62CF2873L,0x6C39F9920B58E85FLL,0x0.85BF0Bp-72,3L,0},{0x6A6E12AAL,1L,0x8.BF106Cp-25,-7L,-1}},{{-1L,-1L,0xA.4E0B66p+99,0x11ADL,-0},{0x62CF2873L,0x6C39F9920B58E85FLL,0x0.85BF0Bp-72,3L,0},{6L,1L,-0x5.2p+1,0L,1},{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1}},{{0x506916A6L,3L,0xE.A194CDp-76,8L,-1},{0x6617FA17L,3L,0x8.9p+1,0L,-0},{-1L,7L,-0x3.Dp+1,8L,1},{0x14784E49L,1L,0x5.Dp+1,0L,-1}},{{-1L,0x6F2DC12F6744ED4CLL,0xA.6105BDp-7,0x7F54L,-1},{1L,-1L,0x7.3p-1,0xD10CL,0},{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1},{-1L,1L,0x0.Fp+1,0x34E9L,-1}},{{0L,0xF2D4CE2787FF9E38LL,0x6.0138ADp-85,0x2706L,0},{0x1F1ADF2DL,-8L,0x4.D4EC0Bp+6,1L,1},{0x66566E71L,0x45DB4FB469CFB4F4LL,0x8.2B3DB3p+28,-4L,0},{0xB2BBD4C0L,0x339178996BAC349CLL,0x1.A072CEp-48,1L,1}},{{0xBF50803CL,0xA28DA27851ED01B0LL,0x3.182763p+24,0x30C2L,0},{4L,-3L,-0x1.Ap+1,-1L,0},{0x2AE489F4L,0x95DAE69F5B7F4FEELL,0x1.5p+1,0x3AC8L,1},{0L,1L,0x7.Fp-1,0xB6E6L,-0}},{{0x4DCDAB58L,0x7A6358B857EB286DLL,0x1.Fp-1,0x6003L,-1},{0L,0L,0x9.6FBAD4p+61,0xC809L,-0},{-10L,0x368D4868E2EF6293LL,0x0.9p-1,0x77E7L,-1},{0x92A7B370L,0L,0x3.CB28DBp-86,0x227DL,0}}},{{{0x77A2EF53L,0xB736387BD1CFCE02LL,-0x1.2p+1,0x1D0CL,-1},{0L,9L,0x8.A05123p-99,-2L,-1},{0xDF585002L,-5L,0x1.7EF146p-51,-1L,0},{-1L,1L,0x6.Cp+1,4L,-0}},{{-8L,0x55A95E6A16A95B5FLL,0xE.ACEA08p-2,-4L,0},{0x62CF2873L,0x6C39F9920B58E85FLL,0x0.85BF0Bp-72,3L,0},{-5L,3L,-0x1.0p+1,0L,-0},{-1L,1L,0x0.Fp+1,0x34E9L,-1}},{{1L,9L,0x1.Bp-1,0xA093L,0},{0x3B74EAE5L,-1L,0x8.9p-1,0xF35EL,1},{0x53CB79D8L,-1L,0x3.5p+1,1L,0},{4L,-3L,-0x1.Ap+1,-1L,0}},{{0L,1L,0x7.Fp-1,0xB6E6L,-0},{-5L,3L,-0x1.0p+1,0L,-0},{0xB2BBD4C0L,0x339178996BAC349CLL,0x1.A072CEp-48,1L,1},{0L,1L,-0x9.Ap+1,-2L,-0}},{{0x1B1B7DE0L,-2L,0x0.3p+1,0x202AL,0},{0L,0xE6343A15A7CFDFC4LL,0x7.Ep-1,1L,-0},{-1L,1L,0x6.Cp+1,4L,-0},{0L,0L,0x9.6FBAD4p+61,0xC809L,-0}},{{0x92A7B370L,0L,0x3.CB28DBp-86,0x227DL,0},{0x1EFE91F0L,-6L,0x0.Fp+1,5L,0},{0xC6276A10L,1L,0x9.7A455Dp-58,0xB54DL,1},{0xC6276A10L,1L,0x9.7A455Dp-58,0xB54DL,1}},{{0xAA5094A5L,2L,0x3.2663EEp+29,0L,0},{0xAA5094A5L,2L,0x3.2663EEp+29,0L,0},{-5L,3L,-0x1.0p+1,0L,-0},{0L,0x25CFA702CAF5ADCELL,0xD.404005p-71,0x08D5L,-0}},{{-6L,7L,0x0.0p-1,0x13C7L,1},{-1L,4L,0x7.1D16DBp-70,-3L,-1},{6L,1L,-0x5.2p+1,0L,1},{0x77A2EF53L,0xB736387BD1CFCE02LL,-0x1.2p+1,0x1D0CL,-1}},{{0L,-1L,0x9.101CFDp-45,0xFC3CL,1},{0xEB367289L,0xA24D72531C418A46LL,0x1.7p-1,4L,-0},{2L,-10L,0x1.5p-1,-1L,0},{6L,1L,-0x5.2p+1,0L,1}}},{{{0x4DCDAB58L,0x7A6358B857EB286DLL,0x1.Fp-1,0x6003L,-1},{0xEB367289L,0xA24D72531C418A46LL,0x1.7p-1,4L,-0},{1L,0x2D3DACF38B77237ELL,0x1.Dp-1,1L,0},{0x77A2EF53L,0xB736387BD1CFCE02LL,-0x1.2p+1,0x1D0CL,-1}},{{0xEB367289L,0xA24D72531C418A46LL,0x1.7p-1,4L,-0},{-1L,4L,0x7.1D16DBp-70,-3L,-1},{-1L,1L,0x0.Fp+1,0x34E9L,-1},{0L,0x25CFA702CAF5ADCELL,0xD.404005p-71,0x08D5L,-0}},{{0x417E072EL,0L,0x8.BA2E11p-59,0x5BBDL,-0},{0xAA5094A5L,2L,0x3.2663EEp+29,0L,0},{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1},{0xC6276A10L,1L,0x9.7A455Dp-58,0xB54DL,1}},{{0x66566E71L,0x45DB4FB469CFB4F4LL,0x8.2B3DB3p+28,-4L,0},{0x1EFE91F0L,-6L,0x0.Fp+1,5L,0},{5L,1L,0x1.5p+1,9L,-1},{0L,0L,0x9.6FBAD4p+61,0xC809L,-0}},{{0x53CB79D8L,-1L,0x3.5p+1,1L,0},{0L,0xE6343A15A7CFDFC4LL,0x7.Ep-1,1L,-0},{0xEB367289L,0xA24D72531C418A46LL,0x1.7p-1,4L,-0},{0L,1L,-0x9.Ap+1,-2L,-0}},{{0x60882539L,0x82EC59D2773D60B3LL,-0x10.5p-1,0xC227L,-1},{-5L,3L,-0x1.0p+1,0L,-0},{-1L,7L,-0x3.Dp+1,8L,1},{4L,-3L,-0x1.Ap+1,-1L,0}},{{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0},{0x3B74EAE5L,-1L,0x8.9p-1,0xF35EL,1},{-2L,-1L,-0x1.4p-1,0x48DCL,-1},{-1L,1L,0x0.Fp+1,0x34E9L,-1}},{{0x417E072EL,0L,0x8.BA2E11p-59,0x5BBDL,-0},{0x62CF2873L,0x6C39F9920B58E85FLL,0x0.85BF0Bp-72,3L,0},{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0},{-1L,1L,0x6.Cp+1,4L,-0}},{{0xBF50803CL,0xA28DA27851ED01B0LL,0x3.182763p+24,0x30C2L,0},{0L,9L,0x8.A05123p-99,-2L,-1},{0x233E6B62L,-1L,0x5.Ep-1,0x4033L,0},{0x92A7B370L,0L,0x3.CB28DBp-86,0x227DL,0}}},{{{-1L,0x8952B2BCD35E6B1BLL,0x1.Fp-1,0x0CF9L,1},{0L,0L,0x9.6FBAD4p+61,0xC809L,-0},{2L,-10L,0x1.5p-1,-1L,0},{0L,1L,0x7.Fp-1,0xB6E6L,-0}},{{0x62CF2873L,0x6C39F9920B58E85FLL,0x0.85BF0Bp-72,3L,0},{4L,-3L,-0x1.Ap+1,-1L,0},{0xDF585002L,-5L,0x1.7EF146p-51,-1L,0},{0xB2BBD4C0L,0x339178996BAC349CLL,0x1.A072CEp-48,1L,1}},{{-5L,3L,-0x1.0p+1,0L,-0},{0x1F1ADF2DL,-8L,0x4.D4EC0Bp+6,1L,1},{-4L,0x4FF34EAFFB5D179ALL,0x4.12C06Dp+67,0xFC1BL,1},{-1L,1L,0x0.Fp+1,0x34E9L,-1}},{{0xAA5094A5L,2L,0x3.2663EEp+29,0L,0},{1L,-1L,0x7.3p-1,0xD10CL,0},{0x506916A6L,3L,0xE.A194CDp-76,8L,-1},{0x14784E49L,1L,0x5.Dp+1,0L,-1}},{{0L,1L,0x7.Fp-1,0xB6E6L,-0},{0x6617FA17L,3L,0x8.9p+1,0L,-0},{0xD51E7DF7L,5L,-0x1.9p-1,-3L,-1},{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1}},{{0x47A2C698L,0xCFA7979A64F93B81LL,0xE.BA6CA5p-34,1L,0},{0L,0xE6343A15A7CFDFC4LL,0x7.Ep-1,1L,-0},{0xB2BBD4C0L,0x339178996BAC349CLL,0x1.A072CEp-48,1L,1},{0xC23E9608L,0x0B4F93AD4E58FD31LL,0x8.27AFF6p-61,0L,-1}},{{0xB5E53D9BL,0xDC16D461820E1BCBLL,0xF.8C8FF6p-16,0x105BL,-0},{0x6A6E12AAL,1L,0x8.BF106Cp-25,-7L,-1},{0xC6276A10L,1L,0x9.7A455Dp-58,0xB54DL,1},{0x506916A6L,3L,0xE.A194CDp-76,8L,-1}},{{2L,-10L,0x1.5p-1,-1L,0},{3L,-5L,0xA.71EFC3p-67,-1L,-0},{-4L,0x4FF34EAFFB5D179ALL,0x4.12C06Dp+67,0xFC1BL,1},{0L,0x25CFA702CAF5ADCELL,0xD.404005p-71,0x08D5L,-0}},{{-8L,0x55A95E6A16A95B5FLL,0xE.ACEA08p-2,-4L,0},{0x1B1B7DE0L,-2L,0x0.3p+1,0x202AL,0},{0x879B0709L,0L,0x1.4ADC3Bp-37,6L,0},{-1L,0x81B6A48933AC5CEDLL,0xA.C48AE5p-81,-8L,1}}},{{{0L,-1L,0x9.101CFDp-45,0xFC3CL,1},{0x7E7461C8L,0x171E5B3C0A783F81LL,0x1.Dp+1,3L,0},{1L,9L,0x1.Bp-1,0xA093L,0},{0x879B0709L,0L,0x1.4ADC3Bp-37,6L,0}},{{-1L,0x8952B2BCD35E6B1BLL,0x1.Fp-1,0x0CF9L,1},{0xEB367289L,0xA24D72531C418A46LL,0x1.7p-1,4L,-0},{0x2AE489F4L,0x95DAE69F5B7F4FEELL,0x1.5p+1,0x3AC8L,1},{0L,-1L,0x9.101CFDp-45,0xFC3CL,1}},{{0xE40E7BB6L,0x3936D73F18804D59LL,-0x5.2p+1,0x8D45L,0},{-1L,0xEA63F3D54CDCAC19LL,0xA.C0CAA6p-38,-1L,0},{-1L,1L,0x0.Fp+1,0x34E9L,-1},{-1L,0xEA63F3D54CDCAC19LL,0xA.C0CAA6p-38,-1L,0}},{{0L,1L,0x1.Cp-1,0x6C13L,-0},{3L,-5L,0xA.71EFC3p-67,-1L,-0},{-2L,-1L,-0x1.4p-1,0x48DCL,-1},{0xC6276A10L,1L,0x9.7A455Dp-58,0xB54DL,1}},{{-1L,0x6F2DC12F6744ED4CLL,0xA.6105BDp-7,0x7F54L,-1},{0L,0xFC78DC00671E2B22LL,-0x1.5p-1,2L,0},{0L,0xE6343A15A7CFDFC4LL,0x7.Ep-1,1L,-0},{-8L,0xC008CE353FEDF429LL,0x3.9p+1,0x6317L,-0}},{{0x53CB79D8L,-1L,0x3.5p+1,1L,0},{1L,0L,-0x5.Fp-1,-7L,0},{0xBF50803CL,0xA28DA27851ED01B0LL,0x3.182763p+24,0x30C2L,0},{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1}},{{0x53CB79D8L,-1L,0x3.5p+1,1L,0},{-5L,3L,-0x1.0p+1,0L,-0},{0L,0xE6343A15A7CFDFC4LL,0x7.Ep-1,1L,-0},{4L,0x2F1836A7604D3D9FLL,0x3.F65977p+73,3L,-0}},{{-1L,0x6F2DC12F6744ED4CLL,0xA.6105BDp-7,0x7F54L,-1},{0xF0E199F5L,0x489776ACFB1E1F64LL,-0x4.Fp-1,1L,1},{-2L,-1L,-0x1.4p-1,0x48DCL,-1},{3L,0x968FDB46EF48D875LL,0x3.016EBDp-19,0x155CL,-0}},{{0L,1L,0x1.Cp-1,0x6C13L,-0},{0x1F1ADF2DL,-8L,0x4.D4EC0Bp+6,1L,1},{-1L,1L,0x0.Fp+1,0x34E9L,-1},{-1L,1L,0x6.Cp+1,4L,-0}}}};
    const uint64_t *l_116 = (void*)0;
    const float l_170 = 0xC.CF7767p+33;
    int32_t l_230 = 0xD5952602L;
    int32_t l_231 = 0xD7978221L;
    uint8_t l_239 = 0x67L;
    float **l_242[10][5] = {{&g_134,&g_134,&g_134,&g_134,(void*)0},{&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134},{(void*)0,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,&g_134,(void*)0,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134},{&g_134,&g_134,(void*)0,&g_134,&g_134},{&g_134,&g_134,&g_134,&g_134,&g_134}};
    int16_t *l_253 = &g_88[1];
    uint16_t *l_255[3][1];
    float l_258 = 0xE.0FF557p-84;
    int32_t *l_259[8][10][2] = {{{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0}},{{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231}},{{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231}},{{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0}},{{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231}},{{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231}},{{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0}},{{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231},{(void*)0,&l_231},{&l_231,(void*)0},{&l_231,&l_231}}};
    int32_t *l_263 = &l_231;
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_255[i][j] = &g_130;
    }
    for (g_66 = 26; (g_66 > 33); g_66 = safe_add_func_uint32_t_u_u(g_66, 9))
    { /* block id: 41 */
        int32_t **l_106 = (void*)0;
        uint8_t *l_112[4];
        int32_t l_113 = 0L;
        const int32_t l_119 = 0x46D85FAAL;
        int32_t l_129 = 0xE7146C40L;
        float **l_136 = &g_134;
        uint64_t *l_149 = (void*)0;
        float *l_203 = (void*)0;
        int i;
        for (i = 0; i < 4; i++)
            l_112[i] = (void*)0;
        g_107 = l_105;
    }
    (*p_98) = ((g_244 = g_86[0]) >= (0x0BEEL && (((safe_div_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((safe_unary_minus_func_uint32_t_u(((((((0x0FL ^ ((g_257 ^= (g_256[8][0] ^= ((safe_add_func_uint16_t_u_u(((safe_unary_minus_func_int16_t_s(g_172[1])) , p_100), (g_130 = (((0x22CEAE8BL <= 0x833B5928L) , ((((g_254 = (l_253 = &g_88[1])) != (void*)0) & p_100) && p_100)) == 0x7ED6L)))) <= g_172[3]))) == 1UL)) == 0x915950CD63C0136FLL) || p_100) ^ p_100) != 0x181D2BCC292E9B0BLL) <= g_127))), 3)), 0xEAL)) , l_231) <= l_108[4][5][3].f1)));
    ++g_260;
    (*g_265) = l_263;
    return p_100;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_7[i], sizeof(g_7[i]), "g_7[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_40, "g_40", print_hash_value);
    transparent_crc(g_53, "g_53", print_hash_value);
    transparent_crc(g_66, "g_66", print_hash_value);
    transparent_crc(g_70, "g_70", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_77[i][j], "g_77[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_85, "g_85", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_86[i], "g_86[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_87, "g_87", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_88[i], "g_88[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_130, "g_130", print_hash_value);
    transparent_crc(g_164, "g_164", print_hash_value);
    transparent_crc_bytes (&g_171, sizeof(g_171), "g_171", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_172[i], "g_172[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_177, "g_177", print_hash_value);
    transparent_crc(g_179, "g_179", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_186[i][j][k], "g_186[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_232, "g_232", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    transparent_crc(g_244, "g_244", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_256[i][j], "g_256[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_257, "g_257", print_hash_value);
    transparent_crc(g_260, "g_260", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_287[i].f0, "g_287[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_293, "g_293", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_360, "g_360", print_hash_value);
    transparent_crc(g_435, "g_435", print_hash_value);
    transparent_crc(g_440, "g_440", print_hash_value);
    transparent_crc(g_442, "g_442", print_hash_value);
    transparent_crc(g_564.f0, "g_564.f0", print_hash_value);
    transparent_crc(g_564.f1, "g_564.f1", print_hash_value);
    transparent_crc_bytes (&g_564.f2, sizeof(g_564.f2), "g_564.f2", print_hash_value);
    transparent_crc(g_564.f3, "g_564.f3", print_hash_value);
    transparent_crc(g_564.f4, "g_564.f4", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_694[i][j], "g_694[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_745, "g_745", print_hash_value);
    transparent_crc(g_764, "g_764", print_hash_value);
    transparent_crc(g_801, "g_801", print_hash_value);
    transparent_crc_bytes (&g_824, sizeof(g_824), "g_824", print_hash_value);
    transparent_crc(g_828, "g_828", print_hash_value);
    transparent_crc(g_899, "g_899", print_hash_value);
    transparent_crc(g_1197, "g_1197", print_hash_value);
    transparent_crc(g_1298, "g_1298", print_hash_value);
    transparent_crc(g_1475, "g_1475", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1617[i], "g_1617[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1764, "g_1764", print_hash_value);
    transparent_crc(g_1770, "g_1770", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1820[i].f0, "g_1820[i].f0", print_hash_value);
        transparent_crc(g_1820[i].f1, "g_1820[i].f1", print_hash_value);
        transparent_crc_bytes(&g_1820[i].f2, sizeof(g_1820[i].f2), "g_1820[i].f2", print_hash_value);
        transparent_crc(g_1820[i].f3, "g_1820[i].f3", print_hash_value);
        transparent_crc(g_1820[i].f4, "g_1820[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1845[i][j].f0, "g_1845[i][j].f0", print_hash_value);
            transparent_crc(g_1845[i][j].f1, "g_1845[i][j].f1", print_hash_value);
            transparent_crc_bytes(&g_1845[i][j].f2, sizeof(g_1845[i][j].f2), "g_1845[i][j].f2", print_hash_value);
            transparent_crc(g_1845[i][j].f3, "g_1845[i][j].f3", print_hash_value);
            transparent_crc(g_1845[i][j].f4, "g_1845[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2150, "g_2150", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_2168[i][j], "g_2168[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_2201[i][j][k], "g_2201[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2451, "g_2451", print_hash_value);
    transparent_crc(g_2497, "g_2497", print_hash_value);
    transparent_crc(g_2723.f0, "g_2723.f0", print_hash_value);
    transparent_crc(g_2723.f1, "g_2723.f1", print_hash_value);
    transparent_crc_bytes (&g_2723.f2, sizeof(g_2723.f2), "g_2723.f2", print_hash_value);
    transparent_crc(g_2723.f3, "g_2723.f3", print_hash_value);
    transparent_crc(g_2723.f4, "g_2723.f4", print_hash_value);
    transparent_crc(g_2763, "g_2763", print_hash_value);
    transparent_crc(g_2782, "g_2782", print_hash_value);
    transparent_crc(g_2968.f0, "g_2968.f0", print_hash_value);
    transparent_crc(g_2968.f1, "g_2968.f1", print_hash_value);
    transparent_crc_bytes (&g_2968.f2, sizeof(g_2968.f2), "g_2968.f2", print_hash_value);
    transparent_crc(g_2968.f3, "g_2968.f3", print_hash_value);
    transparent_crc(g_2968.f4, "g_2968.f4", print_hash_value);
    transparent_crc(g_2986.f0, "g_2986.f0", print_hash_value);
    transparent_crc(g_2986.f1, "g_2986.f1", print_hash_value);
    transparent_crc_bytes (&g_2986.f2, sizeof(g_2986.f2), "g_2986.f2", print_hash_value);
    transparent_crc(g_2986.f3, "g_2986.f3", print_hash_value);
    transparent_crc(g_2986.f4, "g_2986.f4", print_hash_value);
    transparent_crc(g_3008, "g_3008", print_hash_value);
    transparent_crc_bytes (&g_3184, sizeof(g_3184), "g_3184", print_hash_value);
    transparent_crc(g_3214.f0, "g_3214.f0", print_hash_value);
    transparent_crc(g_3214.f1, "g_3214.f1", print_hash_value);
    transparent_crc_bytes (&g_3214.f2, sizeof(g_3214.f2), "g_3214.f2", print_hash_value);
    transparent_crc(g_3214.f3, "g_3214.f3", print_hash_value);
    transparent_crc(g_3214.f4, "g_3214.f4", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_3220[i], "g_3220[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_3299[i][j][k], "g_3299[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 848
   depth: 1, occurrence: 21
XXX total union variables: 17

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 34
breakdown:
   indirect level: 0, occurrence: 21
   indirect level: 1, occurrence: 9
   indirect level: 2, occurrence: 2
   indirect level: 3, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 17
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 35
XXX times a single bitfield on LHS: 6
XXX times a single bitfield on RHS: 6

XXX max expression depth: 30
breakdown:
   depth: 1, occurrence: 103
   depth: 2, occurrence: 37
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
   depth: 8, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 22, occurrence: 2
   depth: 23, occurrence: 3
   depth: 24, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 2
   depth: 27, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1

XXX total number of pointers: 718

XXX times a variable address is taken: 1974
XXX times a pointer is dereferenced on RHS: 515
breakdown:
   depth: 1, occurrence: 401
   depth: 2, occurrence: 77
   depth: 3, occurrence: 26
   depth: 4, occurrence: 9
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 424
breakdown:
   depth: 1, occurrence: 349
   depth: 2, occurrence: 36
   depth: 3, occurrence: 24
   depth: 4, occurrence: 12
   depth: 5, occurrence: 3
XXX times a pointer is compared with null: 62
XXX times a pointer is compared with address of another variable: 19
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 9917

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2063
   level: 2, occurrence: 333
   level: 3, occurrence: 214
   level: 4, occurrence: 126
   level: 5, occurrence: 64
XXX number of pointers point to pointers: 357
XXX number of pointers point to scalars: 341
XXX number of pointers point to structs: 12
XXX percent of pointers has null in alias set: 32.3
XXX average alias set size: 1.44

XXX times a non-volatile is read: 2631
XXX times a non-volatile is write: 1354
XXX times a volatile is read: 165
XXX    times read thru a pointer: 73
XXX times a volatile is write: 44
XXX    times written thru a pointer: 4
XXX times a volatile is available for access: 2.11e+03
XXX percentage of non-volatile access: 95

XXX forward jumps: 1
XXX backward jumps: 13

XXX stmts: 115
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 26
   depth: 2, occurrence: 19
   depth: 3, occurrence: 12
   depth: 4, occurrence: 7
   depth: 5, occurrence: 16

XXX percentage a fresh-made variable is used: 20.1
XXX percentage an existing variable is used: 79.9
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


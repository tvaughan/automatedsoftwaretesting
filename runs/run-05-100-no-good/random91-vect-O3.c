/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      4244548794
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S1 {
   volatile unsigned f0 : 15;
   int32_t  f1;
};
#pragma pack(pop)

struct S0 {
   const volatile int32_t  f0;
   const uint64_t  f1;
   int64_t  f2;
   signed f3 : 2;
   int32_t  f4;
   volatile uint8_t  f5;
   volatile float  f6;
   volatile int8_t  f7;
   unsigned f8 : 4;
   uint8_t  f9;
};

#pragma pack(push)
#pragma pack(1)
struct S2 {
   int32_t  f0;
   volatile int32_t  f1;
   volatile int16_t  f2;
   float  f3;
   volatile int8_t  f4;
   volatile struct S0  f5;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 6L;
static const struct S1 g_8 = {59,-1L};/* VOLATILE GLOBAL g_8 */
static struct S1 g_10 = {177,2L};/* VOLATILE GLOBAL g_10 */
static struct S1 * volatile g_9 = &g_10;/* VOLATILE GLOBAL g_9 */
static struct S2 g_11 = {1L,0x9BE99B96L,-1L,0x9.Ap+1,0x79L,{0xA4087B33L,0xE0C280563ADE45A9LL,0L,0,0x8B5A47B6L,7UL,-0x1.9p+1,-4L,3,0x87L}};/* VOLATILE GLOBAL g_11 */


/* --- FORWARD DECLARATIONS --- */
static struct S2  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_8 g_9 g_11
 * writes: g_10
 */
static struct S2  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = &g_3;
    int32_t *l_4[5][9] = {{(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3},{(void*)0,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,(void*)0,&g_3,&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,&g_3,(void*)0,&g_3,&g_3,&g_3,(void*)0}};
    uint8_t l_5[3][5][5] = {{{255UL,0x7AL,0xFFL,1UL,255UL},{251UL,0x9EL,0x9EL,251UL,0x7AL},{255UL,0x9EL,0x2BL,249UL,251UL},{1UL,0x7AL,255UL,0x03L,6UL},{0x2BL,2UL,1UL,249UL,249UL}},{{2UL,1UL,2UL,251UL,253UL},{2UL,0x69L,0x03L,1UL,0UL},{0x2BL,1UL,0x7AL,0x9EL,0x24L},{1UL,249UL,0x03L,0UL,0x03L},{255UL,255UL,2UL,0x69L,0x03L}},{{251UL,253UL,1UL,6UL,0x24L},{1UL,6UL,255UL,249UL,0UL},{249UL,253UL,0x2BL,0x2BL,253UL},{0x24L,255UL,0x9EL,0x2BL,249UL},{0x7AL,249UL,0xFFL,249UL,6UL}}};
    int i, j, k;
    l_5[1][4][3]--;
    (*g_9) = g_8;
    return g_11;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_8.f0, "g_8.f0", print_hash_value);
    transparent_crc(g_8.f1, "g_8.f1", print_hash_value);
    transparent_crc(g_10.f0, "g_10.f0", print_hash_value);
    transparent_crc(g_10.f1, "g_10.f1", print_hash_value);
    transparent_crc(g_11.f0, "g_11.f0", print_hash_value);
    transparent_crc(g_11.f1, "g_11.f1", print_hash_value);
    transparent_crc(g_11.f2, "g_11.f2", print_hash_value);
    transparent_crc_bytes (&g_11.f3, sizeof(g_11.f3), "g_11.f3", print_hash_value);
    transparent_crc(g_11.f4, "g_11.f4", print_hash_value);
    transparent_crc(g_11.f5.f0, "g_11.f5.f0", print_hash_value);
    transparent_crc(g_11.f5.f1, "g_11.f5.f1", print_hash_value);
    transparent_crc(g_11.f5.f2, "g_11.f5.f2", print_hash_value);
    transparent_crc(g_11.f5.f3, "g_11.f5.f3", print_hash_value);
    transparent_crc(g_11.f5.f4, "g_11.f5.f4", print_hash_value);
    transparent_crc(g_11.f5.f5, "g_11.f5.f5", print_hash_value);
    transparent_crc_bytes (&g_11.f5.f6, sizeof(g_11.f5.f6), "g_11.f5.f6", print_hash_value);
    transparent_crc(g_11.f5.f7, "g_11.f5.f7", print_hash_value);
    transparent_crc(g_11.f5.f8, "g_11.f5.f8", print_hash_value);
    transparent_crc(g_11.f5.f9, "g_11.f5.f9", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 2
breakdown:
   depth: 0, occurrence: 1
   depth: 1, occurrence: 1
   depth: 2, occurrence: 1
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 2
breakdown:
   indirect level: 0, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 1
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 2
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 1
breakdown:
   depth: 1, occurrence: 5

XXX total number of pointers: 3

XXX times a variable address is taken: 19
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 1
breakdown:
   depth: 1, occurrence: 1
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 34

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 2
XXX number of pointers point to structs: 1
XXX percent of pointers has null in alias set: 33.3
XXX average alias set size: 1.33

XXX times a non-volatile is read: 2
XXX times a non-volatile is write: 2
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 80

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 3
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 3

XXX percentage a fresh-made variable is used: 75
XXX percentage an existing variable is used: 25
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


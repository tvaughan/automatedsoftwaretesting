/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1881360666
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   volatile signed f0 : 27;
   unsigned f1 : 4;
   signed f2 : 22;
   unsigned f3 : 10;
};

union U1 {
   volatile int8_t * volatile  f0;
   volatile int64_t  f1;
   volatile uint16_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static int8_t g_7 = 0L;
static int8_t *g_6 = &g_7;
static int16_t g_15[1][4] = {{0xA519L,0xA519L,0xA519L,0xA519L}};
static int16_t g_46 = (-6L);
static int16_t *g_45[7][8] = {{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46}};
static int32_t g_48 = 1L;
static float g_56 = (-0x1.5p-1);
static volatile uint32_t g_57 = 0xC08A59C2L;/* VOLATILE GLOBAL g_57 */
static int32_t g_65[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static int32_t g_98 = (-4L);
static int32_t g_111 = 0L;
static int32_t *g_110 = &g_111;
static int32_t * volatile * const  volatile g_109[9][1] = {{(void*)0},{&g_110},{(void*)0},{&g_110},{(void*)0},{&g_110},{(void*)0},{&g_110},{(void*)0}};
static int16_t g_129 = 0x1C18L;
static uint8_t g_141[5][2] = {{0x33L,0xC8L},{0x33L,0xC8L},{0x33L,0xC8L},{0x33L,0xC8L},{0x33L,0xC8L}};
static float g_142 = 0x0.8p-1;
static int32_t g_143 = (-3L);
static uint32_t g_146[1] = {0x388EEC89L};
static int32_t ** volatile g_155 = (void*)0;/* VOLATILE GLOBAL g_155 */
static int32_t *g_157[5][4][6] = {{{&g_143,&g_65[3],&g_48,&g_143,&g_143,(void*)0},{&g_65[3],(void*)0,&g_65[3],(void*)0,&g_143,&g_143},{&g_65[1],&g_65[3],&g_65[9],(void*)0,&g_65[3],&g_65[3]},{&g_65[3],(void*)0,&g_65[3],(void*)0,(void*)0,&g_65[3]}},{{&g_65[3],&g_65[3],&g_48,(void*)0,&g_143,&g_143},{&g_65[1],&g_65[9],(void*)0,(void*)0,(void*)0,&g_48},{&g_65[3],&g_65[1],(void*)0,&g_143,&g_65[3],&g_143},{&g_143,&g_143,&g_48,&g_65[3],&g_143,&g_65[3]}},{{&g_65[3],&g_143,&g_65[3],&g_65[3],&g_143,&g_65[3]},{&g_65[1],&g_143,&g_65[9],&g_143,&g_65[3],&g_143},{&g_65[3],&g_65[1],&g_65[3],&g_48,(void*)0,(void*)0},{&g_65[3],&g_65[9],&g_48,&g_143,&g_143,(void*)0}},{{&g_65[1],&g_65[3],(void*)0,&g_65[3],(void*)0,(void*)0},{&g_65[3],(void*)0,(void*)0,&g_65[3],&g_65[3],(void*)0},{&g_143,&g_65[3],&g_48,&g_143,&g_143,(void*)0},{&g_65[3],(void*)0,&g_65[3],(void*)0,&g_143,&g_143}},{{&g_65[1],&g_65[3],&g_65[9],(void*)0,&g_65[3],&g_65[3]},{&g_65[3],(void*)0,&g_65[3],(void*)0,(void*)0,&g_65[3]},{&g_65[3],&g_65[3],&g_48,(void*)0,&g_143,&g_143},{&g_65[1],&g_65[9],(void*)0,(void*)0,(void*)0,&g_48}}};
static int32_t ** volatile g_156 = &g_157[2][1][4];/* VOLATILE GLOBAL g_156 */
static uint16_t g_162 = 65526UL;
static struct S0 g_188 = {9999,3,1907,20};/* VOLATILE GLOBAL g_188 */
static struct S0 g_189 = {1028,3,-878,30};/* VOLATILE GLOBAL g_189 */
static int64_t * const  volatile g_237 = (void*)0;/* VOLATILE GLOBAL g_237 */
static int16_t **g_266 = &g_45[3][6];
static const volatile int16_t g_268 = 0x331AL;/* VOLATILE GLOBAL g_268 */
static int32_t ** volatile g_274[1] = {&g_157[2][1][4]};
static int32_t ** const  volatile g_276 = &g_157[2][1][4];/* VOLATILE GLOBAL g_276 */
static float * volatile g_297 = (void*)0;/* VOLATILE GLOBAL g_297 */
static int32_t ** volatile g_299 = &g_157[2][1][4];/* VOLATILE GLOBAL g_299 */
static volatile struct S0 g_300 = {-1366,0,147,9};/* VOLATILE GLOBAL g_300 */
static volatile union U1 g_344 = {0};/* VOLATILE GLOBAL g_344 */
static uint64_t g_351 = 0xF0DC391DA64C13B6LL;
static uint64_t g_353 = 0x3D0493B6507706C7LL;
static volatile struct S0 g_376[5] = {{-6862,1,-676,0},{-6862,1,-676,0},{-6862,1,-676,0},{-6862,1,-676,0},{-6862,1,-676,0}};
static struct S0 g_382 = {-4162,0,1625,30};/* VOLATILE GLOBAL g_382 */
static union U1 g_426 = {0};/* VOLATILE GLOBAL g_426 */
static struct S0 g_431 = {-10233,3,1027,3};/* VOLATILE GLOBAL g_431 */
static int32_t **g_433[10] = {&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110};
static int32_t *** volatile g_432[8][6] = {{&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4]},{&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4]},{&g_433[1],&g_433[1],&g_433[4],&g_433[1],&g_433[1],&g_433[4]},{&g_433[1],&g_433[1],&g_433[4],&g_433[1],&g_433[1],&g_433[4]},{&g_433[1],&g_433[1],&g_433[4],&g_433[1],&g_433[1],&g_433[4]},{&g_433[1],&g_433[1],&g_433[4],&g_433[1],&g_433[1],&g_433[4]},{&g_433[1],&g_433[1],&g_433[4],&g_433[1],&g_433[1],&g_433[4]},{&g_433[1],&g_433[1],&g_433[4],&g_433[1],&g_433[1],&g_433[4]}};
static int32_t *** volatile g_434 = &g_433[6];/* VOLATILE GLOBAL g_434 */
static int32_t ** volatile g_435 = (void*)0;/* VOLATILE GLOBAL g_435 */
static int8_t *g_450 = &g_7;
static int8_t **g_449 = &g_450;
static int32_t * volatile g_451 = &g_65[8];/* VOLATILE GLOBAL g_451 */
static int32_t ** volatile g_459 = &g_157[2][2][4];/* VOLATILE GLOBAL g_459 */
static int8_t g_480 = 0x60L;
static int64_t g_487 = 2L;
static int64_t *g_490 = &g_487;
static int64_t **g_489 = &g_490;
static int64_t *** volatile g_488 = &g_489;/* VOLATILE GLOBAL g_488 */
static volatile uint8_t g_548 = 8UL;/* VOLATILE GLOBAL g_548 */
static volatile uint8_t *g_547 = &g_548;
static volatile uint8_t **g_546[5][7] = {{&g_547,&g_547,&g_547,&g_547,&g_547,&g_547,(void*)0},{&g_547,&g_547,&g_547,(void*)0,&g_547,&g_547,&g_547},{&g_547,&g_547,&g_547,&g_547,&g_547,&g_547,&g_547},{&g_547,&g_547,&g_547,&g_547,&g_547,&g_547,&g_547},{&g_547,&g_547,&g_547,&g_547,&g_547,&g_547,&g_547}};
static uint16_t g_604 = 5UL;
static volatile union U1 g_605 = {0};/* VOLATILE GLOBAL g_605 */
static uint8_t ***g_620 = (void*)0;
static struct S0 g_625 = {3288,0,539,23};/* VOLATILE GLOBAL g_625 */
static volatile union U1 g_729[2][5][1] = {{{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}}}};
static volatile struct S0 g_730[10] = {{-4613,0,67,28},{-4613,0,67,28},{10422,2,1342,24},{-4613,0,67,28},{-4613,0,67,28},{10422,2,1342,24},{-4613,0,67,28},{-4613,0,67,28},{10422,2,1342,24},{-4613,0,67,28}};
static int64_t g_753[2] = {0x97283BB9934402E0LL,0x97283BB9934402E0LL};
static volatile struct S0 g_758 = {5578,0,-1244,24};/* VOLATILE GLOBAL g_758 */
static volatile struct S0 * volatile g_765[3][3] = {{&g_376[4],&g_376[4],&g_376[4]},{&g_376[4],&g_376[4],&g_376[4]},{&g_376[4],&g_376[4],&g_376[4]}};
static volatile struct S0 * volatile g_766 = &g_376[4];/* VOLATILE GLOBAL g_766 */
static int32_t ** volatile g_767[10] = {&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1],&g_157[2][0][1]};
static int8_t g_776 = 0L;
static struct S0 g_809 = {2711,0,-1500,3};/* VOLATILE GLOBAL g_809 */
static volatile struct S0 g_851 = {6714,2,696,11};/* VOLATILE GLOBAL g_851 */
static int32_t * const  volatile g_859 = &g_143;/* VOLATILE GLOBAL g_859 */
static float * const  volatile g_860 = &g_56;/* VOLATILE GLOBAL g_860 */
static int32_t * volatile g_862[1][9][8] = {{{&g_65[7],&g_48,(void*)0,(void*)0,&g_48,(void*)0,&g_48,(void*)0},{&g_143,&g_48,&g_143,&g_65[7],&g_65[7],&g_143,&g_48,&g_143},{&g_143,&g_65[7],(void*)0,(void*)0,&g_143,&g_143,(void*)0,(void*)0},{&g_143,&g_143,(void*)0,(void*)0,(void*)0,&g_143,&g_143,(void*)0},{&g_143,&g_65[7],&g_65[3],&g_143,&g_48,&g_143,&g_65[7],&g_65[7]},{(void*)0,&g_48,(void*)0,(void*)0,&g_48,(void*)0,&g_48,(void*)0},{&g_143,&g_48,&g_65[3],&g_65[7],(void*)0,&g_65[3],&g_48,&g_65[3]},{&g_143,&g_65[7],&g_48,&g_65[7],&g_143,&g_143,&g_65[7],(void*)0},{&g_143,&g_143,(void*)0,(void*)0,&g_65[7],&g_143,&g_143,&g_65[7]}}};
static int32_t * volatile g_863 = &g_48;/* VOLATILE GLOBAL g_863 */
static volatile struct S0 g_898 = {9978,3,471,11};/* VOLATILE GLOBAL g_898 */
static volatile struct S0 g_899 = {1007,2,-1185,18};/* VOLATILE GLOBAL g_899 */
static volatile int64_t g_920 = 0L;/* VOLATILE GLOBAL g_920 */
static uint16_t g_930 = 0xB9ABL;
static struct S0 g_940[9][2][2] = {{{{-10833,0,-46,10},{-10833,0,-46,10}},{{-3387,2,1046,23},{-1714,1,-786,15}}},{{{7291,1,155,19},{-3387,2,1046,23}},{{7291,1,155,19},{10437,0,1288,3}}},{{{10437,0,1288,3},{7291,1,155,19}},{{-3387,2,1046,23},{3150,2,1857,12}}},{{{-3387,2,1046,23},{7291,1,155,19}},{{10437,0,1288,3},{10437,0,1288,3}}},{{{7291,1,155,19},{-3387,2,1046,23}},{{3150,2,1857,12},{-3387,2,1046,23}}},{{{7291,1,155,19},{10437,0,1288,3}},{{10437,0,1288,3},{7291,1,155,19}}},{{{-3387,2,1046,23},{3150,2,1857,12}},{{-3387,2,1046,23},{7291,1,155,19}}},{{{10437,0,1288,3},{10437,0,1288,3}},{{7291,1,155,19},{-3387,2,1046,23}}},{{{3150,2,1857,12},{-3387,2,1046,23}},{{7291,1,155,19},{10437,0,1288,3}}}};
static uint32_t g_970 = 18446744073709551610UL;
static int32_t g_983 = 0xEB717DE4L;
static volatile struct S0 g_1024[1] = {{8956,0,-1783,17}};
static struct S0 g_1037[9] = {{7211,1,-401,27},{-10149,3,-351,15},{7211,1,-401,27},{-10149,3,-351,15},{7211,1,-401,27},{-10149,3,-351,15},{7211,1,-401,27},{-10149,3,-351,15},{7211,1,-401,27}};
static struct S0 *g_1036 = &g_1037[5];
static int64_t *** volatile g_1054 = (void*)0;/* VOLATILE GLOBAL g_1054 */
static union U1 g_1194 = {0};/* VOLATILE GLOBAL g_1194 */
static struct S0 g_1246 = {5200,3,33,30};/* VOLATILE GLOBAL g_1246 */
static int64_t g_1283 = 1L;
static union U1 g_1296 = {0};/* VOLATILE GLOBAL g_1296 */
static uint64_t g_1330 = 0x8C46E88C2323344ALL;
static union U1 g_1343 = {0};/* VOLATILE GLOBAL g_1343 */
static float * volatile g_1346 = &g_56;/* VOLATILE GLOBAL g_1346 */
static volatile union U1 g_1361 = {0};/* VOLATILE GLOBAL g_1361 */
static const union U1 g_1376 = {0};/* VOLATILE GLOBAL g_1376 */
static volatile struct S0 g_1377[2][3] = {{{2096,2,1311,10},{2096,2,1311,10},{2096,2,1311,10}},{{2096,2,1311,10},{2096,2,1311,10},{2096,2,1311,10}}};
static int32_t ***g_1389 = (void*)0;
static int32_t * volatile g_1423[4][10] = {{&g_983,&g_65[7],&g_48,&g_65[3],&g_48,&g_65[7],&g_983,&g_48,&g_983,&g_48},{&g_48,&g_65[2],&g_48,&g_983,&g_48,&g_48,&g_48,&g_48,&g_48,&g_48},{&g_983,&g_65[2],&g_65[2],&g_983,&g_48,&g_48,&g_983,&g_48,&g_65[6],&g_983},{&g_48,&g_65[7],&g_65[6],&g_983,&g_65[2],&g_983,&g_65[2],&g_983,&g_65[6],&g_65[7]}};
static volatile union U1 g_1463[8] = {{0},{0},{0},{0},{0},{0},{0},{0}};
static int8_t ***g_1470 = &g_449;
static float * volatile g_1471[6] = {&g_56,&g_56,&g_56,&g_56,&g_56,&g_56};
static float * volatile g_1472 = &g_142;/* VOLATILE GLOBAL g_1472 */
static int16_t g_1544 = 0xA977L;
static struct S0 g_1550 = {-9516,3,1457,0};/* VOLATILE GLOBAL g_1550 */
static volatile union U1 g_1590[8][4][8] = {{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}}};
static const uint32_t g_1593[3][9][2] = {{{6UL,6UL},{6UL,1UL},{0xC633DCCEL,0x970ABC2FL},{1UL,0x970ABC2FL},{0xC633DCCEL,1UL},{6UL,6UL},{6UL,1UL},{0xC633DCCEL,0x970ABC2FL},{1UL,0x970ABC2FL}},{{0xC633DCCEL,1UL},{6UL,6UL},{6UL,1UL},{0xC633DCCEL,6UL},{0xC633DCCEL,6UL},{0x32257A42L,0xC633DCCEL},{1UL,1UL},{1UL,0xC633DCCEL},{0x32257A42L,6UL}},{{0xC633DCCEL,6UL},{0x32257A42L,0xC633DCCEL},{1UL,1UL},{1UL,0xC633DCCEL},{0x32257A42L,6UL},{0xC633DCCEL,6UL},{0x32257A42L,0xC633DCCEL},{1UL,1UL},{1UL,0xC633DCCEL}}};
static uint32_t *g_1602 = &g_970;
static uint32_t **g_1601 = &g_1602;
static uint16_t g_1618 = 0UL;
static volatile uint64_t g_1624 = 0UL;/* VOLATILE GLOBAL g_1624 */
static volatile uint32_t g_1639 = 7UL;/* VOLATILE GLOBAL g_1639 */
static uint8_t g_1673 = 1UL;
static struct S0 g_1674 = {741,1,-1192,7};/* VOLATILE GLOBAL g_1674 */
static struct S0 ** volatile g_1682 = &g_1036;/* VOLATILE GLOBAL g_1682 */
static float * volatile g_1687 = &g_56;/* VOLATILE GLOBAL g_1687 */
static union U1 g_1709 = {0};/* VOLATILE GLOBAL g_1709 */
static volatile struct S0 g_1710 = {-11050,0,-526,25};/* VOLATILE GLOBAL g_1710 */
static volatile struct S0 g_1711 = {-1297,1,-1410,31};/* VOLATILE GLOBAL g_1711 */
static struct S0 g_1712 = {8704,3,1482,31};/* VOLATILE GLOBAL g_1712 */
static struct S0 g_1714 = {8865,0,-1539,11};/* VOLATILE GLOBAL g_1714 */
static struct S0 g_1715 = {3732,1,1233,9};/* VOLATILE GLOBAL g_1715 */
static struct S0 g_1716 = {-5556,3,1765,7};/* VOLATILE GLOBAL g_1716 */
static uint32_t g_1791 = 1UL;
static struct S0 g_1819[1][2] = {{{10005,2,1388,13},{10005,2,1388,13}}};
static float g_1861 = 0x6.97F7D1p+64;
static volatile struct S0 g_1876 = {353,2,-746,23};/* VOLATILE GLOBAL g_1876 */
static uint8_t *g_1926 = &g_1673;
static uint8_t **g_1925 = &g_1926;
static uint8_t ** const *g_1924 = &g_1925;
static uint8_t ** const **g_1923[9][4][5] = {{{(void*)0,&g_1924,(void*)0,&g_1924,&g_1924},{(void*)0,(void*)0,(void*)0,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,(void*)0,&g_1924,(void*)0}},{{&g_1924,&g_1924,(void*)0,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,(void*)0,(void*)0},{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,(void*)0,&g_1924,&g_1924,&g_1924}},{{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,(void*)0,&g_1924,&g_1924,(void*)0},{&g_1924,&g_1924,&g_1924,&g_1924,(void*)0},{&g_1924,&g_1924,&g_1924,(void*)0,(void*)0}},{{&g_1924,(void*)0,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,&g_1924,(void*)0},{&g_1924,&g_1924,&g_1924,(void*)0,&g_1924}},{{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{(void*)0,&g_1924,&g_1924,&g_1924,&g_1924},{(void*)0,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,(void*)0,&g_1924}},{{&g_1924,&g_1924,(void*)0,&g_1924,(void*)0},{(void*)0,(void*)0,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,(void*)0,&g_1924,&g_1924},{(void*)0,(void*)0,&g_1924,&g_1924,(void*)0}},{{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,(void*)0,(void*)0,(void*)0},{&g_1924,&g_1924,(void*)0,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,&g_1924,(void*)0}},{{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,&g_1924,(void*)0},{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{(void*)0,&g_1924,&g_1924,&g_1924,(void*)0}},{{&g_1924,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,&g_1924,&g_1924,&g_1924,(void*)0},{(void*)0,&g_1924,&g_1924,&g_1924,&g_1924},{&g_1924,(void*)0,(void*)0,&g_1924,&g_1924}}};
static uint8_t ** const ***g_1922 = &g_1923[7][3][1];
static volatile uint8_t g_1978[6][10][4] = {{{0UL,255UL,255UL,255UL},{250UL,255UL,254UL,255UL},{1UL,255UL,0x2FL,0x64L},{0x9CL,0x0BL,0UL,0x2FL},{255UL,0x22L,255UL,0x22L},{0UL,1UL,2UL,0UL},{0x0DL,255UL,0x0BL,255UL},{0x6FL,0x9FL,0x2FL,254UL},{0x6FL,0x64L,0x0BL,0x2FL},{0x0DL,254UL,2UL,0UL}},{{0UL,0x00L,255UL,255UL},{255UL,255UL,0UL,2UL},{0x9CL,0xADL,0x2FL,0x0BL},{1UL,0UL,254UL,0x2FL},{250UL,0UL,255UL,0x0BL},{0UL,0xADL,0UL,2UL},{248UL,255UL,0x22L,255UL},{0x2FL,0x00L,0x2FL,0UL},{0x4CL,254UL,0x64L,0x2FL},{0xAEL,0x64L,255UL,254UL}},{{0UL,0x9FL,255UL,255UL},{0xAEL,255UL,0x64L,0UL},{0x4CL,1UL,0x2FL,0x22L},{0x2FL,0x22L,0x22L,0x2FL},{248UL,0x0BL,0UL,0x64L},{0UL,255UL,255UL,255UL},{250UL,255UL,254UL,255UL},{1UL,255UL,0x2FL,0x64L},{0x9CL,0x0BL,0UL,0x2FL},{255UL,0x22L,255UL,0x22L}},{{0UL,1UL,2UL,0UL},{0x0DL,255UL,0x0BL,255UL},{0x6FL,0x9FL,0x2FL,254UL},{0x6FL,0x64L,0x0BL,0x2FL},{0x0DL,254UL,2UL,0UL},{0UL,0x00L,255UL,255UL},{255UL,255UL,0UL,2UL},{0x9CL,0xADL,0x2FL,0x0BL},{1UL,0UL,254UL,0x2FL},{250UL,0UL,255UL,0x0BL}},{{0UL,0xADL,0UL,2UL},{248UL,255UL,0x22L,255UL},{0x2FL,0x00L,255UL,0x6FL},{0UL,0x9CL,1UL,255UL},{254UL,1UL,1UL,0x9CL},{0x6FL,250UL,1UL,0xADL},{254UL,0UL,1UL,0x9FL},{0UL,248UL,255UL,0x2FL},{255UL,0x2FL,0x2FL,255UL},{0x0BL,0x4CL,0x9FL,1UL}},{{0x6FL,0xAEL,0xADL,1UL},{0x64L,0UL,0x9CL,1UL},{255UL,0xAEL,255UL,1UL},{255UL,0x4CL,0x6FL,255UL},{0UL,0x2FL,0x00L,0x2FL},{0x6FL,248UL,255UL,0x9FL},{0x22L,0UL,0x4CL,0xADL},{2UL,250UL,255UL,0x9CL},{2UL,1UL,0x4CL,255UL},{0x22L,0x9CL,255UL,0x6FL}}};
static int8_t g_1982 = 1L;
static struct S0 g_1988[8] = {{-10118,3,-924,2},{-10118,3,-924,2},{-10118,3,-924,2},{-10118,3,-924,2},{-10118,3,-924,2},{-10118,3,-924,2},{-10118,3,-924,2},{-10118,3,-924,2}};
static int32_t ** volatile g_1991 = (void*)0;/* VOLATILE GLOBAL g_1991 */
static uint64_t g_2033 = 18446744073709551615UL;
static volatile struct S0 * volatile g_2066 = &g_1377[0][0];/* VOLATILE GLOBAL g_2066 */
static int16_t g_2068 = 0x35BAL;
static volatile struct S0 g_2100[3][8] = {{{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20}},{{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20}},{{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20},{10443,0,-664,20}}};
static const union U1 g_2137[5][4] = {{{0},{0},{0},{0}},{{0},{0},{0},{0}},{{0},{0},{0},{0}},{{0},{0},{0},{0}},{{0},{0},{0},{0}}};
static const union U1 g_2139[8] = {{0},{0},{0},{0},{0},{0},{0},{0}};
static const union U1 *g_2138 = &g_2139[5];
static volatile union U1 g_2141 = {0};/* VOLATILE GLOBAL g_2141 */
static volatile union U1 *g_2140 = &g_2141;
static volatile struct S0 g_2158 = {9118,3,-1034,23};/* VOLATILE GLOBAL g_2158 */
static volatile struct S0 g_2191 = {4737,0,-2020,26};/* VOLATILE GLOBAL g_2191 */
static int16_t g_2230[6][5][8] = {{{1L,2L,0x5E46L,0x30C7L,0x8EF5L,0x7B52L,(-1L),0x5023L},{0x7B52L,2L,3L,(-6L),0L,0x30C7L,3L,4L},{3L,0L,0x8EF5L,0x263EL,0x54E4L,0x263EL,0x8EF5L,0L},{(-1L),0x30C7L,(-1L),0x54E4L,3L,3L,0x049EL,0x5E46L},{0x263EL,(-6L),0x30C7L,(-1L),(-1L),(-6L),0x049EL,3L}},{{0x32C8L,(-1L),(-1L),3L,(-6L),6L,0x8EF5L,0x8EF5L},{(-6L),6L,0x8EF5L,0x8EF5L,6L,(-6L),3L,(-1L)},{0L,4L,3L,0x049EL,(-6L),0xC069L,1L,4L},{6L,0x7B52L,2L,3L,(-6L),0L,0x30C7L,3L},{0x32C8L,(-6L),0x263EL,0xEE1BL,0L,0x30C7L,0L,0xEE1BL}},{{(-1L),0x8EF5L,(-1L),(-6L),4L,(-1L),0x54E4L,0x5E46L},{0L,6L,4L,1L,(-6L),0xEE1BL,4L,2L},{0L,(-7L),0L,0x30C7L,4L,3L,2L,0x263EL},{(-1L),0L,(-1L),0L,0L,(-1L),0L,(-1L)},{0x32C8L,3L,0x5023L,0x54E4L,(-6L),0x7B52L,0x263EL,4L}},{{6L,3L,0L,4L,0x54E4L,0x7B52L,0L,0L},{0x263EL,3L,(-6L),2L,0x049EL,(-1L),0x5023L,(-1L)},{0L,0L,0x049EL,0L,0L,3L,(-7L),0x5023L},{0L,(-7L),0x30C7L,0x263EL,0xC069L,0xEE1BL,0x5E46L,0L},{0L,6L,0x30C7L,0L,0L,(-1L),(-7L),(-6L)}},{{0xC069L,0x8EF5L,0x049EL,0x5023L,0x30C7L,0x30C7L,0x5023L,0x049EL},{(-6L),(-6L),(-6L),(-7L),(-1L),0L,0L,0x30C7L},{(-6L),0x7B52L,0L,0x5E46L,0xEE1BL,0xC069L,0x263EL,0x30C7L},{0x7B52L,(-1L),0x5023L,(-7L),3L,0L,0L,0x049EL},{2L,0x049EL,(-1L),0x5023L,(-1L),0x049EL,2L,(-6L)}},{{0x5023L,1L,0L,0L,0x7B52L,0x54E4L,4L,0L},{(-1L),0L,4L,0x263EL,0x7B52L,(-6L),0x54E4L,0x5023L},{0x5023L,4L,(-1L),0L,(-1L),0L,0L,(-1L)},{2L,0x263EL,0x263EL,2L,3L,4L,0x30C7L,0L},{0x7B52L,(-1L),2L,4L,0xEE1BL,(-6L),1L,4L}}};
static struct S0 g_2238 = {-10921,2,-1727,24};/* VOLATILE GLOBAL g_2238 */


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static struct S0  func_2(int8_t * p_3, int8_t * p_4, const int8_t * const  p_5);
static const int8_t * const  func_8(int16_t  p_9, int8_t * p_10, int8_t * p_11, int8_t * p_12, int8_t  p_13);
static union U1  func_16(uint8_t  p_17, int8_t * p_18, int8_t  p_19, const uint32_t  p_20, const int8_t  p_21);
static int8_t * func_23(int16_t  p_24, int16_t * const  p_25, uint32_t  p_26, float  p_27);
static uint64_t  func_34(uint64_t  p_35, uint16_t  p_36, int32_t  p_37, int8_t * p_38);
static uint16_t  func_39(int32_t  p_40, int64_t  p_41, int32_t  p_42, int64_t  p_43, int16_t * p_44);
static const int32_t  func_73(int8_t * p_74);
static uint32_t  func_79(uint8_t  p_80, int8_t * p_81, int16_t  p_82);
static int16_t * func_99(int32_t  p_100, int64_t  p_101, uint16_t  p_102, int64_t  p_103);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    int16_t *l_14 = &g_15[0][1];
    int32_t l_22 = (-6L);
    float l_60 = 0x1.4p+1;
    uint16_t l_61[9] = {4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL};
    uint16_t l_847 = 0x0FB4L;
    int8_t l_1995 = 0xB5L;
    int32_t l_2032[10][5][5] = {{{0x24ADC9FEL,0x1676492AL,0xEABFD090L,0xA9A18045L,1L},{1L,(-2L),1L,(-7L),1L},{0L,0xE3D0E72EL,0x72C34CCBL,8L,5L},{0x6C745D07L,(-2L),0x790F9584L,0x0F6B34C2L,0x0F6B34C2L},{(-7L),0xEF51AE9AL,(-7L),0xEA5F34BDL,1L}},{{0xE3D0E72EL,0xCE34AAFBL,0xFDDF34FFL,(-7L),0x352FB29DL},{1L,0xEBE66343L,0L,0x6D8FF8CDL,(-2L)},{0x4E8FE752L,1L,0xFDDF34FFL,0x352FB29DL,1L},{0x86CAE8EFL,0xC870AF03L,(-7L),0x24ADC9FEL,1L},{0xEABFD090L,0xCB5C7460L,0x790F9584L,0x1676492AL,0x72C34CCBL}},{{0x0F6B34C2L,0x45AD33D2L,0x72C34CCBL,8L,0xE3D0E72EL},{0xCE34AAFBL,5L,1L,1L,0x19195C58L},{8L,0x165F5C9BL,0x1A078351L,1L,0x19195C58L},{1L,1L,0x1676492AL,0x74498B94L,0xE3D0E72EL},{0x45AD33D2L,0x74498B94L,0xEABFD090L,0xBF0A0CD9L,0x72C34CCBL}},{{1L,0xFDDF34FFL,1L,0xEE3199F2L,1L},{1L,1L,5L,0x308FAA83L,1L},{0x165F5C9BL,0x352FB29DL,0x3F3AC9D7L,5L,(-2L)},{0x3F3AC9D7L,1L,0xF885EA6EL,0L,0x352FB29DL},{0xCCE45D76L,0x352FB29DL,1L,0x1A078351L,1L}},{{0xEBE66343L,1L,0x0498D20BL,0x165F5C9BL,0x0F6B34C2L},{5L,0xFDDF34FFL,0x86CAE8EFL,1L,5L},{5L,0x74498B94L,0x24ADC9FEL,0xEBE66343L,1L},{0xF885EA6EL,1L,0L,0x72C34CCBL,1L},{0x308FAA83L,0x165F5C9BL,0xEE3199F2L,0x72C34CCBL,0x86CAE8EFL}},{{1L,5L,1L,0xEBE66343L,(-3L)},{0x352FB29DL,0x45AD33D2L,0L,1L,0xCE34AAFBL},{(-2L),0xCB5C7460L,0x165F5C9BL,0x165F5C9BL,0xCB5C7460L},{(-2L),0xC870AF03L,0xE3D0E72EL,0x1A078351L,0L},{0xA9A18045L,1L,0x6C745D07L,0L,8L}},{{0x1A078351L,0xEBE66343L,(-2L),5L,0x6C745D07L},{0xA9A18045L,0xCE34AAFBL,0x162C5D17L,0x308FAA83L,0x1A078351L},{(-2L),0xEF51AE9AL,0x6D8FF8CDL,0xEE3199F2L,1L},{(-2L),(-2L),8L,0xBF0A0CD9L,0L},{0x352FB29DL,0xE3D0E72EL,1L,0x74498B94L,0x6F444694L}},{{1L,(-2L),(-3L),1L,(-1L)},{0x308FAA83L,8L,(-3L),1L,0x24ADC9FEL},{0xF885EA6EL,5L,1L,8L,0xEF51AE9AL},{5L,0xACE4B0ADL,8L,8L,0x162C5D17L},{1L,1L,5L,1L,1L}},{{0x1676492AL,0x162C5D17L,1L,1L,0x6D8FF8CDL},{0xEF51AE9AL,8L,1L,5L,0xF885EA6EL},{0x4E8FE752L,0x790F9584L,0xEE3199F2L,0x162C5D17L,0x6D8FF8CDL},{0L,5L,0xCE34AAFBL,0xCB5C7460L,1L},{0x6D8FF8CDL,(-7L),0L,1L,0x162C5D17L}},{{0x45AD33D2L,0x6F444694L,0xEBE66343L,0xA9A18045L,0xEA5F34BDL},{0x165F5C9BL,(-2L),0xACE4B0ADL,1L,1L},{1L,0xACE4B0ADL,0xFDDF34FFL,0x790F9584L,0x3F3AC9D7L},{0xA9A18045L,0xACE4B0ADL,1L,0x6C745D07L,0x72C34CCBL},{(-3L),(-2L),1L,0x86CAE8EFL,(-1L)}}};
    uint64_t l_2038 = 18446744073709551615UL;
    uint16_t l_2039 = 0x28FFL;
    union U1 *l_2040 = &g_1194;
    int32_t l_2042 = (-1L);
    uint16_t * const *l_2047 = (void*)0;
    int32_t ***l_2052 = (void*)0;
    const int8_t *l_2109[6][8][5] = {{{&g_776,&g_776,&l_1995,(void*)0,&g_776},{&g_1982,&l_1995,(void*)0,&g_480,(void*)0},{&l_1995,&g_776,&g_480,(void*)0,(void*)0},{&l_1995,&l_1995,(void*)0,&g_1982,&l_1995},{&g_1982,&g_776,&g_776,&g_1982,&l_1995},{&g_776,(void*)0,&l_1995,&l_1995,&g_480},{(void*)0,&g_1982,&g_480,&g_776,&g_776},{&l_1995,&g_480,&g_1982,&g_776,&g_776}},{{(void*)0,(void*)0,&g_776,&g_480,&g_480},{&l_1995,(void*)0,&g_480,&g_776,&g_776},{(void*)0,&g_776,&g_1982,(void*)0,&g_776},{&l_1995,&l_1995,&l_1995,&l_1995,&l_1995},{&g_776,&g_776,&l_1995,&l_1995,&g_776},{&g_1982,&g_776,&l_1995,&g_1982,(void*)0},{&g_480,(void*)0,&g_480,&l_1995,&l_1995},{(void*)0,&g_480,&l_1995,&l_1995,&l_1995}},{{(void*)0,(void*)0,&g_1982,(void*)0,&g_480},{&g_1982,(void*)0,&l_1995,&g_776,&g_480},{&g_480,&l_1995,&g_480,&g_480,&g_776},{&l_1995,&l_1995,&g_776,&g_776,&l_1995},{&g_776,&g_1982,&g_480,&g_776,&g_1982},{&g_776,(void*)0,&g_1982,&l_1995,(void*)0},{&g_1982,&l_1995,&g_1982,&g_1982,&l_1995},{(void*)0,&l_1995,&l_1995,&g_1982,(void*)0}},{{&g_776,&l_1995,&g_480,(void*)0,&g_776},{&g_776,(void*)0,&g_776,&g_480,&l_1995},{&g_776,&l_1995,&g_776,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1995,(void*)0},{&g_480,&g_1982,&l_1995,&l_1995,&g_776},{&g_1982,&l_1995,&g_1982,(void*)0,&g_1982},{(void*)0,&g_480,&g_776,(void*)0,&l_1995},{&g_1982,&l_1995,&g_776,&g_1982,&g_776}},{{&g_480,(void*)0,&g_1982,(void*)0,&g_480},{&g_1982,&g_480,&l_1995,&g_1982,&g_1982},{(void*)0,&g_480,(void*)0,&l_1995,&g_776},{&g_480,(void*)0,&g_776,(void*)0,(void*)0},{&g_776,&g_1982,(void*)0,&g_480,&l_1995},{(void*)0,&g_1982,&g_776,(void*)0,&l_1995},{(void*)0,&g_1982,(void*)0,&g_776,&g_776},{&g_776,&g_480,&l_1995,&l_1995,(void*)0}},{{&g_480,&g_1982,&g_1982,&g_480,&g_1982},{&g_776,&g_480,&g_776,&g_1982,&g_480},{&l_1995,(void*)0,&g_776,&g_776,&g_1982},{&l_1995,&g_1982,&g_1982,&g_480,&l_1995},{&g_776,&g_776,&l_1995,&g_1982,&l_1995},{&g_776,&l_1995,(void*)0,&l_1995,&g_776},{&g_480,&g_480,&l_1995,&l_1995,(void*)0},{&g_776,&g_480,(void*)0,&l_1995,&l_1995}}};
    const int8_t **l_2108 = &l_2109[2][4][1];
    const int8_t ** const *l_2107 = &l_2108;
    int16_t l_2111 = 0x90B1L;
    uint32_t l_2174[5][9][5] = {{{1UL,0x474F29AAL,0x7F08318BL,0x474F29AAL,1UL},{3UL,1UL,0xEEC759BFL,18446744073709551615UL,18446744073709551612UL},{18446744073709551615UL,0x9C8A9460L,1UL,18446744073709551612UL,0x75569470L},{1UL,0xAFE2B711L,0x6F60FE78L,1UL,18446744073709551612UL},{0xFBB120FDL,18446744073709551612UL,1UL,18446744073709551614UL,1UL},{18446744073709551612UL,18446744073709551612UL,3UL,1UL,18446744073709551615UL},{0xFBB120FDL,18446744073709551612UL,18446744073709551615UL,0UL,0x7F08318BL},{1UL,0x0F3E8564L,1UL,0x80F72A84L,18446744073709551615UL},{18446744073709551615UL,18446744073709551612UL,0xFBB120FDL,0x9C8A9460L,18446744073709551608UL}},{{3UL,18446744073709551612UL,18446744073709551612UL,3UL,1UL},{1UL,18446744073709551612UL,0xFBB120FDL,0UL,0UL},{0x6F60FE78L,0xAFE2B711L,1UL,0xAFE2B711L,0x6F60FE78L},{1UL,0x9C8A9460L,18446744073709551615UL,0UL,0xE534E1FAL},{0xEEC759BFL,1UL,3UL,3UL,1UL},{0x7F08318BL,0x474F29AAL,1UL,0x9C8A9460L,0xE534E1FAL},{0xAFE2B711L,3UL,0x6F60FE78L,0x80F72A84L,0x6F60FE78L},{0xE534E1FAL,18446744073709551615UL,1UL,0UL,0UL},{0x0F3E8564L,0x6F60FE78L,1UL,1UL,1UL}},{{0x75569470L,0UL,0x75569470L,0x474F29AAL,0UL},{1UL,0x6F60FE78L,0x0F3E8564L,3UL,18446744073709551615UL},{0xE534E1FAL,18446744073709551614UL,0x9A293F61L,18446744073709551615UL,0x75569470L},{0xEEC759BFL,18446744073709551612UL,0x0F3E8564L,18446744073709551615UL,0x6F60FE78L},{18446744073709551615UL,0xB6089E21L,0x75569470L,0xB6089E21L,18446744073709551615UL},{18446744073709551612UL,3UL,1UL,18446744073709551615UL,0x80F72A84L},{0x7F08318BL,18446744073709551612UL,0xE534E1FAL,18446744073709551615UL,1UL},{1UL,0x0F3E8564L,0xEEC759BFL,3UL,0x80F72A84L},{1UL,18446744073709551615UL,18446744073709551615UL,0x474F29AAL,18446744073709551615UL}},{{0x80F72A84L,0x80F72A84L,18446744073709551612UL,1UL,0x6F60FE78L},{1UL,0xBFC259F6L,0x7F08318BL,0x9C8A9460L,0x75569470L},{1UL,18446744073709551615UL,1UL,0xAFE2B711L,18446744073709551615UL},{0x7F08318BL,0xBFC259F6L,1UL,18446744073709551612UL,0UL},{18446744073709551612UL,0x80F72A84L,0x80F72A84L,18446744073709551612UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551612UL,1UL},{0xEEC759BFL,0x0F3E8564L,1UL,0x0F3E8564L,0xEEC759BFL},{0xE534E1FAL,18446744073709551612UL,0x7F08318BL,18446744073709551612UL,0x9A293F61L},{1UL,3UL,18446744073709551612UL,18446744073709551612UL,3UL}},{{0x75569470L,0xB6089E21L,18446744073709551615UL,18446744073709551612UL,0x9A293F61L},{0x0F3E8564L,18446744073709551612UL,0xEEC759BFL,0xAFE2B711L,0xEEC759BFL},{0x9A293F61L,18446744073709551614UL,0xE534E1FAL,0x9C8A9460L,1UL},{0x0F3E8564L,0x6F60FE78L,1UL,1UL,1UL},{0x75569470L,0UL,0x75569470L,0x474F29AAL,0UL},{1UL,0x6F60FE78L,0x0F3E8564L,3UL,18446744073709551615UL},{0xE534E1FAL,18446744073709551614UL,0x9A293F61L,18446744073709551615UL,0x75569470L},{0xEEC759BFL,18446744073709551612UL,0x0F3E8564L,18446744073709551615UL,0x6F60FE78L},{18446744073709551615UL,0xB6089E21L,0x75569470L,0xB6089E21L,18446744073709551615UL}}};
    uint8_t l_2198 = 255UL;
    uint64_t *l_2258 = &g_1330;
    uint32_t ***l_2260 = &g_1601;
    uint32_t ****l_2259 = &l_2260;
    int32_t l_2261 = 1L;
    int32_t l_2262 = 1L;
    int i, j, k;
    return l_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_48 g_1343 g_188.f2 g_1389 g_129 g_1463 g_1377.f0 g_6 g_7 g_1036 g_1037 g_776 g_1472 g_141 g_1024.f1 g_729 g_382.f2 g_753 g_351 g_188.f3 g_851.f1 g_487 g_1550 g_983 g_730.f2 g_353 g_1590 g_490 g_1601 g_143 g_547 g_548 g_1024.f2 g_1618 g_1470 g_449 g_450 g_489 g_1624 g_1639 g_46 g_431.f3 g_189.f3 g_1593 g_488 g_1674 g_766 g_376 g_863 g_1714 g_157 g_1716 g_1710.f2 g_162 g_1330 g_1673 g_1712.f1 g_15 g_382.f1 g_1791 g_930 g_1819 g_1602 g_970 g_1876 g_1922 g_1926 g_480 g_65 g_859 g_860 g_1296 g_146 g_276 g_268 g_1924 g_1925 g_142 g_1978 g_1982 g_451 g_1682 g_1283 g_111 g_604
 * writes: g_48 g_776 g_129 g_142 g_1470 g_487 g_1330 g_1283 g_15 g_157 g_111 g_46 g_353 g_1544 g_1624 g_1639 g_141 g_1673 g_1715 g_930 g_162 g_604 g_480 g_1791 g_970 g_449 g_56 g_1861 g_1037 g_1922 g_7 g_65 g_143 g_1978 g_983
 */
static struct S0  func_2(int8_t * p_3, int8_t * p_4, const int8_t * const  p_5)
{ /* block id: 610 */
    const float l_1418 = 0xD.47D42Fp-77;
    int32_t l_1419 = 0x57A2C42DL;
    int32_t **l_1420 = &g_157[0][0][2];
    int32_t ** const l_1421 = (void*)0;
    int32_t ***l_1441 = &l_1420;
    int32_t ***l_1443[8] = {&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4],&g_433[4]};
    const int64_t *l_1486 = &g_1283;
    const int64_t * const *l_1485 = &l_1486;
    const int64_t * const **l_1484[2][8][3] = {{{&l_1485,&l_1485,&l_1485},{&l_1485,&l_1485,(void*)0},{&l_1485,&l_1485,(void*)0},{&l_1485,&l_1485,&l_1485},{&l_1485,&l_1485,&l_1485},{&l_1485,&l_1485,&l_1485},{&l_1485,&l_1485,&l_1485},{&l_1485,&l_1485,&l_1485}},{{&l_1485,&l_1485,&l_1485},{(void*)0,&l_1485,&l_1485},{(void*)0,&l_1485,&l_1485},{&l_1485,&l_1485,&l_1485},{&l_1485,&l_1485,(void*)0},{&l_1485,&l_1485,(void*)0},{(void*)0,&l_1485,&l_1485},{&l_1485,&l_1485,&l_1485}}};
    const int64_t * const ***l_1483 = &l_1484[1][4][2];
    int32_t l_1496 = 1L;
    int32_t l_1506 = 9L;
    int32_t l_1507 = 5L;
    int32_t l_1508 = 1L;
    int16_t l_1595 = 0x4440L;
    int32_t l_1611 = 0xC42CC276L;
    uint32_t l_1617 = 0x1C9559ADL;
    uint64_t l_1627 = 18446744073709551609UL;
    uint32_t l_1670 = 0UL;
    struct S0 *l_1681[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_1754 = (-2L);
    int32_t l_1765[3][6][9] = {{{0x27688B44L,1L,9L,0x36668B64L,0x36668B64L,9L,1L,0x27688B44L,(-5L)},{0xF366DFFEL,0x8A23E271L,0x27688B44L,0x36668B64L,4L,(-5L),(-8L),0L,0x6D913369L},{(-5L),0xC81D6A85L,0x6D913369L,0xD3B00C66L,0xF366DFFEL,0xD3B00C66L,0x6D913369L,0xC81D6A85L,(-5L)},{0x7520A8E3L,0xD3B00C66L,4L,(-6L),0xF366DFFEL,(-8L),0x8A23E271L,9L,0x089C2D8BL},{0L,9L,(-6L),0x90E3DDA9L,4L,4L,0x90E3DDA9L,(-6L),9L},{0x7520A8E3L,0xF366DFFEL,0x8A23E271L,0x27688B44L,0x36668B64L,4L,(-5L),(-8L),0L}},{{(-5L),0x90E3DDA9L,0xF366DFFEL,0x089C2D8BL,(-6L),(-8L),(-6L),0x089C2D8BL,0xF366DFFEL},{0xF366DFFEL,0xF366DFFEL,1L,0L,(-1L),0xD3B00C66L,(-6L),0x6D913369L,0x7520A8E3L},{0x27688B44L,9L,0xC81D6A85L,0x8A23E271L,0x7520A8E3L,(-5L),(-5L),0x7520A8E3L,0x8A23E271L},{1L,0xD3B00C66L,1L,4L,(-8L),9L,0x90E3DDA9L,0x7520A8E3L,(-1L)},{0xD3B00C66L,0xC81D6A85L,0xF366DFFEL,(-1L),0x90E3DDA9L,0x6D913369L,0x8A23E271L,0x6D913369L,0x90E3DDA9L},{4L,0x8A23E271L,0x8A23E271L,4L,0xC81D6A85L,0x36668B64L,0x6D913369L,0x089C2D8BL,0x90E3DDA9L}},{{0x089C2D8BL,1L,(-6L),0x8A23E271L,(-5L),(-1L),(-8L),(-8L),(-1L)},{0xC81D6A85L,0L,4L,0L,0xC81D6A85L,0x089C2D8BL,1L,(-6L),0x8A23E271L},{0xC81D6A85L,0x36668B64L,0x6D913369L,0x089C2D8BL,0x90E3DDA9L,1L,0x7520A8E3L,9L,0x7520A8E3L},{0x089C2D8BL,(-8L),0x27688B44L,0x27688B44L,(-8L),0x089C2D8BL,0xD3B00C66L,0xC81D6A85L,0xF366DFFEL},{4L,(-8L),9L,0x90E3DDA9L,0x7520A8E3L,(-1L),0x27688B44L,0L,0L},{0xD3B00C66L,0x36668B64L,(-1L),(-6L),(-1L),0x36668B64L,0xD3B00C66L,0x27688B44L,9L}}};
    uint8_t ****l_1784 = &g_620;
    uint64_t l_1904[2];
    uint32_t l_1940 = 8UL;
    int16_t **l_1963[5] = {&g_45[2][5],&g_45[2][5],&g_45[2][5],&g_45[2][5],&g_45[2][5]};
    uint32_t **l_1987 = &g_1602;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1904[i] = 18446744073709551607UL;
    if ((l_1419 ^ (l_1419 , ((l_1420 = l_1420) != l_1421))))
    { /* block id: 612 */
        const uint32_t l_1422 = 0x173BD6BBL;
        int32_t *l_1424 = &g_48;
        int32_t ****l_1439 = (void*)0;
        int32_t ****l_1440[6][9] = {{&g_1389,&g_1389,&g_1389,(void*)0,(void*)0,&g_1389,&g_1389,&g_1389,(void*)0},{&g_1389,&g_1389,&g_1389,&g_1389,(void*)0,&g_1389,&g_1389,&g_1389,&g_1389},{(void*)0,&g_1389,&g_1389,&g_1389,&g_1389,&g_1389,&g_1389,&g_1389,&g_1389},{&g_1389,&g_1389,&g_1389,&g_1389,&g_1389,&g_1389,&g_1389,(void*)0,&g_1389},{&g_1389,&g_1389,&g_1389,(void*)0,&g_1389,&g_1389,&g_1389,&g_1389,(void*)0},{&g_1389,&g_1389,(void*)0,(void*)0,&g_1389,(void*)0,(void*)0,&g_1389,&g_1389}};
        volatile union U1 *l_1444 = &g_344;
        int8_t **l_1493 = &g_6;
        int i, j;
lbl_1551:
        (*l_1424) = l_1422;
        if ((safe_add_func_uint16_t_u_u(0x82EDL, ((((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((*l_1424), 15)), (safe_sub_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((g_1343 , g_188.f2), 5L)), (safe_lshift_func_int16_t_s_s((((l_1441 = g_1389) != (void*)0) | (*l_1424)), (safe_unary_minus_func_int16_t_s(l_1419)))))))), 0L)) > 0L) , &g_109[3][0]) == l_1443[1]))))
        { /* block id: 615 */
            int8_t l_1461 = 0xE1L;
            float l_1466 = 0x1.Ep+1;
            int8_t ***l_1469[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1469[i] = &g_449;
            l_1444 = &g_729[1][3][0];
            for (g_776 = 26; (g_776 != (-25)); --g_776)
            { /* block id: 619 */
                int8_t l_1460 = 7L;
                for (l_1419 = 0; (l_1419 <= (-24)); --l_1419)
                { /* block id: 622 */
                    int8_t l_1462 = 0xE2L;
                    uint8_t l_1464[3];
                    uint32_t l_1465 = 0x6DE669DDL;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1464[i] = 0x8FL;
                    for (g_129 = (-13); (g_129 == (-12)); g_129 = safe_add_func_int8_t_s_s(g_129, 8))
                    { /* block id: 625 */
                        float *l_1451 = &g_142;
                        (*l_1451) = 0x1.Ap+1;
                    }
                    (*l_1424) ^= ((l_1465 |= ((!(((~((safe_lshift_func_uint8_t_u_u(0x33L, (safe_mul_func_int16_t_s_s(((safe_add_func_int64_t_s_s(l_1460, 18446744073709551614UL)) == (g_1463[6] , g_1377[1][0].f0)), 65535UL)))) && l_1461)) || l_1464[2]) != l_1464[1])) || l_1464[2])) & (*g_6));
                    if (l_1464[2])
                        break;
                }
                (*g_1472) = (0x3.Cp-1 >= ((safe_add_func_float_f_f(l_1460, ((&g_146[0] != ((((l_1461 || (((l_1461 <= ((*l_1424) = l_1461)) ^ ((g_1470 = l_1469[0]) != (void*)0)) , ((((*g_1036) , (*p_5)) == 0xF1L) == (-8L)))) != 2L) != l_1461) , (void*)0)) <= l_1461))) <= 0x0.7p+1));
                if (l_1461)
                    continue;
            }
            (*l_1424) = (g_141[0][1] >= l_1461);
        }
        else
        { /* block id: 638 */
            uint64_t l_1480 = 1UL;
            int32_t l_1505[6] = {1L,1L,1L,1L,1L,1L};
            int i;
            l_1419 &= (~g_1024[0].f1);
            for (g_776 = (-9); (g_776 != (-2)); g_776 = safe_add_func_int16_t_s_s(g_776, 7))
            { /* block id: 642 */
                uint32_t l_1494 = 18446744073709551610UL;
                float *l_1495 = &g_142;
                int32_t l_1501 = 0x64003D74L;
                int32_t l_1503 = 0x121E1DA8L;
                int32_t l_1504[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
                int i;
                for (g_487 = 0; (g_487 > (-16)); --g_487)
                { /* block id: 645 */
                    int16_t * const **l_1479 = (void*)0;
                    int16_t * const ***l_1478 = &l_1479;
                    (*l_1478) = (void*)0;
                    l_1480++;
                }
                l_1496 = ((((g_729[1][0][0] , l_1483) != (void*)0) <= (+((*l_1495) = ((!(-0x1.6p-1)) > (((safe_mul_func_float_f_f((-0x1.8p+1), (((safe_add_func_float_f_f((((((void*)0 != l_1493) ^ 0x0490L) , l_1494) >= 0x1.Ep-1), l_1480)) > 0x8.Bp-1) < g_382.f2))) >= g_753[0]) > g_351))))) <= g_188.f3);
                for (g_1330 = 0; (g_1330 != 16); g_1330 = safe_add_func_int8_t_s_s(g_1330, 8))
                { /* block id: 653 */
                    float l_1499 = 0x1.Dp-1;
                    int32_t l_1500[7] = {0xCDFA4BAFL,0xCDFA4BAFL,1L,0xCDFA4BAFL,0xCDFA4BAFL,1L,0xCDFA4BAFL};
                    uint16_t l_1509 = 0UL;
                    int64_t l_1534 = 0xB59011482DB6A137LL;
                    int i;
                    for (g_1283 = 0; (g_1283 <= 4); g_1283 += 1)
                    { /* block id: 656 */
                        int32_t l_1502 = (-1L);
                        int16_t *l_1535 = &g_15[0][3];
                        int32_t l_1536 = 0x3DB3E62BL;
                        int i, j;
                        if (l_1494)
                            break;
                        l_1509--;
                        l_1536 ^= (l_1480 , ((safe_mul_func_uint16_t_u_u(((safe_div_func_int32_t_s_s((*l_1424), g_851.f1)) <= 1UL), ((l_1505[3] = (((*l_1535) = (safe_div_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_s((+(((safe_sub_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s((((18446744073709551608UL && (-8L)) < ((safe_lshift_func_int16_t_s_s(l_1500[0], 0)) < l_1505[4])) == (((safe_add_func_uint8_t_u_u((((+(safe_div_func_float_f_f((l_1534 , g_487), l_1502))) , 0xDAL) || l_1480), 0xB3L)) | g_351) <= l_1504[6])), l_1503)), l_1504[3])) && l_1534) && 0x2CL)), l_1509)), 0x9A27DEC1L))) | (-5L))) > l_1480))) <= 65535UL));
                    }
                }
                for (l_1501 = (-18); (l_1501 >= (-12)); l_1501 = safe_add_func_int32_t_s_s(l_1501, 1))
                { /* block id: 666 */
                    int32_t *l_1539 = &l_1504[3];
                    int32_t l_1540 = 1L;
                    int32_t l_1541 = 0L;
                    int32_t l_1542 = (-1L);
                    int32_t l_1543 = 0x428A47E7L;
                    int32_t l_1545 = 0x62DEF1D2L;
                    int32_t l_1546[4][4][3] = {{{0x76908ED2L,0L,0L},{0x78366755L,0xA47F1E3FL,0x78366755L},{0x76908ED2L,0x76908ED2L,0L},{(-3L),0xA47F1E3FL,(-3L)}},{{0x76908ED2L,0L,0L},{0x78366755L,0xA47F1E3FL,0x78366755L},{0x76908ED2L,0x76908ED2L,0L},{(-3L),0xA47F1E3FL,(-3L)}},{{0x76908ED2L,0L,0L},{0x78366755L,0xA47F1E3FL,0x78366755L},{0x76908ED2L,0x76908ED2L,0L},{(-3L),0xA47F1E3FL,(-3L)}},{{0x76908ED2L,0L,0L},{0x78366755L,0xA47F1E3FL,0x78366755L},{0x76908ED2L,0x76908ED2L,0L},{(-3L),0xA47F1E3FL,(-3L)}}};
                    uint8_t l_1547 = 252UL;
                    int i, j, k;
                    (*l_1420) = l_1539;
                    l_1547++;
                    return g_1550;
                }
            }
        }
        if (g_382.f2)
            goto lbl_1551;
    }
    else
    { /* block id: 674 */
        int32_t ****l_1563[10][9][2] = {{{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389}},{{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389}},{{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389}},{{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389}},{{&g_1389,&l_1441},{&g_1389,&g_1389},{&g_1389,&g_1389},{&g_1389,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441}},{{&l_1441,&l_1441},{&g_1389,&g_1389},{&l_1441,&g_1389},{&g_1389,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441}},{{&l_1441,&l_1441},{&g_1389,&g_1389},{&l_1441,&g_1389},{&g_1389,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441}},{{&l_1441,&l_1441},{&g_1389,&g_1389},{&l_1441,&g_1389},{&g_1389,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441}},{{&l_1441,&l_1441},{&g_1389,&g_1389},{&l_1441,&g_1389},{&g_1389,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441}},{{&l_1441,&l_1441},{&g_1389,&g_1389},{&l_1441,&g_1389},{&g_1389,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441},{&l_1441,&l_1441}}};
        const int32_t **l_1566[2][2];
        const int32_t *** const l_1565 = &l_1566[0][0];
        float l_1637[10][6] = {{0x5.2p+1,0x9.4p-1,0x9.EB1F21p-80,0x0.Ap+1,0x0.Ap+1,0x9.EB1F21p-80},{0x8.3E1524p+88,0x8.3E1524p+88,0x6.D90539p+4,(-0x1.4p+1),0x6.8p+1,0x9.4p-1},{(-0x1.Dp-1),0x9.EB1F21p-80,0x3.499275p+48,(-0x6.3p+1),0xA.626944p-76,0x6.D90539p+4},{0x2.60612Ep-89,(-0x1.Dp-1),0x3.499275p+48,(-0x1.4p-1),0x8.3E1524p+88,0x9.4p-1},{0x6.Bp+1,(-0x1.4p-1),0x6.D90539p+4,0x3.499275p+48,0x2.26642Dp-47,0x9.EB1F21p-80},{0x3.499275p+48,0x2.26642Dp-47,0x9.EB1F21p-80,0x1.3p-1,0x9.EB1F21p-80,0x2.26642Dp-47},{(-0x4.9p+1),(-0x6.3p+1),0x0.0p+1,0x3.A4BC6Dp+8,0x9.4p-1,0x8.3E1524p+88},{0x9.4p-1,0x2.60612Ep-89,0x2.6C0594p-77,0xF.07CF5Fp-5,0x6.D90539p+4,0xA.626944p-76},{0x0.Ap+1,0x2.60612Ep-89,0x3.A4BC6Dp+8,0x7.5p-1,0x9.4p-1,0x6.8p+1},{0x1.3p-1,(-0x6.3p+1),0x5.2p+1,0x9.4p-1,0x9.EB1F21p-80,0x0.Ap+1}};
        int8_t ***l_1652 = &g_449;
        uint32_t * const l_1686 = (void*)0;
        uint32_t * const *l_1685 = &l_1686;
        uint8_t ** const l_1788 = (void*)0;
        uint8_t ** const *l_1787 = &l_1788;
        uint8_t ** const **l_1786 = &l_1787;
        int16_t ***l_1834 = (void*)0;
        int16_t l_1859 = 0L;
        uint16_t l_1864 = 0x5723L;
        uint64_t l_1913 = 4UL;
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 2; j++)
                l_1566[i][j] = (void*)0;
        }
lbl_1950:
        for (g_111 = 3; (g_111 != 11); g_111++)
        { /* block id: 677 */
            const int32_t *l_1569 = (void*)0;
            const int32_t **l_1568[3][1];
            const int32_t ***l_1567[1][5];
            const uint32_t *l_1621[2];
            const uint32_t **l_1620 = &l_1621[1];
            int32_t l_1623 = 0xFAAD1C0BL;
            int32_t l_1634 = (-7L);
            int32_t l_1635 = 1L;
            int32_t l_1638[1];
            const int64_t l_1666 = (-2L);
            int32_t l_1671 = 0L;
            uint32_t l_1708 = 1UL;
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1568[i][j] = &l_1569;
            }
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 5; j++)
                    l_1567[i][j] = &l_1568[0][0];
            }
            for (i = 0; i < 2; i++)
                l_1621[i] = (void*)0;
            for (i = 0; i < 1; i++)
                l_1638[i] = 0x56AF2445L;
            for (l_1496 = 10; (l_1496 != 14); ++l_1496)
            { /* block id: 680 */
                uint16_t l_1560 = 65531UL;
                int32_t ** const *l_1564[2][8][1] = {{{(void*)0},{&g_433[7]},{(void*)0},{&g_433[7]},{(void*)0},{&g_433[7]},{(void*)0},{&g_433[7]}},{{(void*)0},{&g_433[7]},{(void*)0},{&g_433[7]},{(void*)0},{&g_433[7]},{(void*)0},{&g_433[7]}}};
                const int32_t ***l_1570[6] = {&l_1568[0][0],&l_1568[0][0],&l_1568[0][0],&l_1568[0][0],&l_1568[0][0],&l_1568[0][0]};
                uint32_t l_1571[1];
                const int64_t * const l_1587[9] = {&g_753[1],(void*)0,&g_753[1],&g_753[1],(void*)0,&g_753[1],&g_753[1],(void*)0,&g_753[1]};
                int32_t l_1596[9] = {0xB70ECA10L,0x743DFE17L,0xB70ECA10L,0x743DFE17L,0xB70ECA10L,0x743DFE17L,0xB70ECA10L,0x743DFE17L,0xB70ECA10L};
                int8_t l_1636 = 0L;
                int32_t l_1655[6] = {0x65C8B90FL,0xF220E7ECL,0x65C8B90FL,0x65C8B90FL,0xF220E7ECL,0x65C8B90FL};
                uint8_t *l_1656 = &g_141[0][1];
                int8_t ***l_1663 = &g_449;
                uint8_t *l_1672 = &g_1673;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_1571[i] = 0xD5C5F8E4L;
                if ((((((safe_mod_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(l_1560, (((safe_div_func_float_f_f((-0x7.Dp-1), (g_983 , (l_1563[2][5][1] == &l_1441)))) > ((((l_1564[1][0][0] != l_1565) , (l_1570[1] = l_1567[0][2])) != (void*)0) , l_1571[0])) , 8UL))), (-9L))) > g_1550.f2) > 0xB4AEA9F56E410742LL) | 0x90E9L) < g_730[2].f2))
                { /* block id: 682 */
                    uint16_t l_1574 = 1UL;
                    int32_t l_1575 = 0xBC42C9EEL;
                    for (g_46 = 0; (g_46 != 8); g_46++)
                    { /* block id: 685 */
                        uint64_t *l_1582 = &g_353;
                        const uint32_t *l_1592 = &g_1593[0][5][0];
                        const uint32_t **l_1591 = &l_1592;
                        int16_t *l_1594 = &g_1544;
                        union U1 *l_1613 = (void*)0;
                        union U1 **l_1612 = &l_1613;
                        int32_t l_1614 = 0x90C748B2L;
                        uint16_t l_1619 = 0x17FBL;
                        int32_t l_1622 = (-1L);
                        l_1575 &= l_1574;
                        l_1596[7] &= (safe_div_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s((safe_mod_func_uint64_t_u_u((--(*l_1582)), (safe_add_func_int16_t_s_s((((void*)0 == l_1587[4]) , ((-1L) || (l_1574 , ((safe_sub_func_int16_t_s_s(((*l_1594) = ((0x54DC23F76DDFC7FELL ^ 0x717D7487C65FACA9LL) , (g_1590[0][1][3] , (((*l_1591) = &g_970) != l_1569)))), l_1595)) ^ (*g_490))))), 0x9DD3L)))), 5)), 0x83L));
                        l_1622 |= (safe_add_func_int64_t_s_s(((safe_add_func_int64_t_s_s(((g_1601 != (((((((((((safe_rshift_func_uint16_t_u_s(((safe_mod_func_uint16_t_u_u((((-10L) ^ (safe_add_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(l_1611, g_143)), ((*l_1594) = ((((*l_1582) = (((*l_1612) = &g_426) != (void*)0)) & l_1614) > (*g_547)))))) , (safe_lshift_func_uint16_t_u_u((l_1617 == l_1614), g_1024[0].f2))), l_1574)) ^ 0UL), l_1574)) >= (*p_3)) & 4294967295UL) | g_1618) | 0x8135B216L) == 0xDC61L) >= l_1574) && 0x641D16DD8EE69E3ALL) != l_1619) == l_1614) , l_1620)) < 5UL), (*g_490))) > (***g_1470)), (**g_489)));
                        l_1575 ^= l_1574;
                    }
                    --g_1624;
                    if (l_1574)
                        break;
                    --l_1627;
                }
                else
                { /* block id: 700 */
                    float l_1630[2];
                    int32_t l_1631 = 9L;
                    int32_t l_1632 = 0x0013C805L;
                    int32_t l_1633[5] = {0x2C4EED0EL,0x2C4EED0EL,0x2C4EED0EL,0x2C4EED0EL,0x2C4EED0EL};
                    int16_t *l_1653 = &g_46;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1630[i] = 0x3.4p-1;
                    --g_1639;
                    l_1596[5] = (safe_mul_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s(0x6718L, (safe_sub_func_uint32_t_u_u(((l_1631 && ((safe_lshift_func_int16_t_s_u(((*l_1653) |= (&g_449 != l_1652)), 9)) | (+(g_353 < (-7L))))) , ((((*p_3) && (0xC69CF1DD22AE9981LL < (8UL < 4294967289UL))) , 0x0EL) || (*p_4))), l_1633[4])))) , 0xE7L), 0)) >= 5UL) , l_1655[3]), 0x2FA2L));
                }
                if ((g_431.f3 , (((*l_1672) = (((*l_1656) = (*g_547)) , (safe_add_func_uint64_t_u_u((safe_div_func_int64_t_s_s((((g_189.f3 > (((g_1593[1][2][0] > ((void*)0 == l_1663)) | (l_1670 = (1UL > (((safe_rshift_func_int16_t_s_s(l_1666, ((((~(safe_sub_func_uint32_t_u_u((65527UL != g_1550.f2), 0UL))) == (***g_488)) != 0x45949359BBE37B92LL) <= 0x4EC698DAL))) || 0x17B9L) <= 4L)))) && l_1671)) <= 8UL) && 0xB37A50212EEB4D5DLL), g_431.f3)), 0xE2EF4E4CF1D0E2C6LL)))) != g_776)))
                { /* block id: 708 */
                    return g_1674;
                }
                else
                { /* block id: 710 */
                    l_1638[0] = 0xB945B8F2L;
                    for (l_1636 = 21; (l_1636 == 13); --l_1636)
                    { /* block id: 714 */
                        return (*g_766);
                    }
                }
                l_1623 |= 0x67D48219L;
            }
        }
        if (l_1611)
        { /* block id: 747 */
            uint64_t l_1713 = 18446744073709551615UL;
            int32_t l_1722 = 0x90F4CC07L;
            int32_t l_1723 = 0L;
            int32_t l_1724[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1724[i] = 3L;
            if (((*g_863) = l_1713))
            { /* block id: 749 */
                g_1715 = g_1714;
                for (g_930 = 0; (g_930 <= 4); g_930 += 1)
                { /* block id: 753 */
                    (*l_1420) = (*l_1420);
                    return g_1716;
                }
            }
            else
            { /* block id: 757 */
                uint16_t *l_1721[1][8] = {{&g_1618,&g_604,&g_1618,&g_604,&g_1618,&g_604,&g_1618,&g_604}};
                uint8_t *l_1727 = &g_141[0][0];
                int32_t l_1728 = (-1L);
                int i, j;
                l_1728 = (safe_add_func_int32_t_s_s((g_1710.f2 > (((!(~(++g_162))) || 0x10L) || ((*l_1727) = 255UL))), l_1728));
            }
            for (l_1722 = 0; (l_1722 > 18); ++l_1722)
            { /* block id: 764 */
                int8_t l_1731[7] = {1L,1L,(-1L),1L,1L,(-1L),1L};
                int i;
                l_1731[1] |= (*g_863);
            }
            for (g_1283 = 0; (g_1283 > (-21)); g_1283 = safe_sub_func_uint8_t_u_u(g_1283, 2))
            { /* block id: 769 */
                uint64_t *l_1742[9] = {&l_1713,&l_1713,&l_1713,&l_1713,&l_1713,&l_1713,&l_1713,&l_1713,&l_1713};
                int32_t l_1749 = (-4L);
                int16_t *l_1760 = &g_46;
                uint8_t *l_1761[8][2][1] = {{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}},{{&g_141[0][1]},{&g_1673}}};
                int32_t l_1762 = (-1L);
                int32_t l_1763 = 0x0809855DL;
                int32_t l_1764 = 0x791F39F9L;
                int32_t l_1766 = (-1L);
                uint32_t l_1767 = 0xD2F1A645L;
                int i, j, k;
                l_1762 &= (safe_sub_func_int8_t_s_s(((safe_div_func_int64_t_s_s((((((safe_lshift_func_uint16_t_u_u((1L | (((safe_sub_func_uint64_t_u_u(l_1722, (g_353 |= (g_1330++)))) < 0UL) & (safe_rshift_func_int16_t_s_s((((l_1724[0] = (((g_431.f3 , ((safe_rshift_func_uint8_t_u_s((l_1749 >= ((*p_3) & (safe_mul_func_int8_t_s_s((((safe_lshift_func_uint16_t_u_u(((l_1754 && (*p_3)) && (safe_lshift_func_uint8_t_u_s((~(((g_1673 ^= (((((*l_1760) |= ((safe_sub_func_uint32_t_u_u((l_1722 , l_1724[0]), 6L)) , l_1749)) >= l_1723) >= l_1749) ^ 0UL)) != (*p_5)) < (*p_5))), 4))), 7)) && g_1712.f1) & l_1749), 1L)))), 1)) ^ (*p_4))) < 4294967286UL) , l_1749)) || l_1713) & l_1723), l_1723)))), 3)) > l_1749) , (*l_1565)) == (void*)0) || l_1749), l_1749)) < l_1749), 2UL));
                ++l_1767;
            }
        }
        else
        { /* block id: 778 */
            int32_t l_1778 = 0x10326211L;
            int16_t *l_1781 = &g_15[0][1];
            uint8_t *****l_1785 = &l_1784;
            int32_t l_1789 = (-1L);
            uint32_t l_1790 = 4294967295UL;
            int16_t *l_1792 = &g_129;
            int32_t l_1793 = 0x6D48E17FL;
            uint64_t l_1830 = 0UL;
            const int8_t *l_1851 = &g_480;
            const int8_t **l_1850 = &l_1851;
            uint32_t l_1865 = 0xDFBA8655L;
            int32_t ****l_1879 = &l_1441;
            int32_t *l_1881 = &l_1765[2][2][4];
            int64_t *l_1898[5][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1283,(void*)0,&g_1283,(void*)0,&g_1283,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1283,(void*)0,&g_1283,(void*)0,&g_1283,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
            const struct S0 *l_1900 = &g_1819[0][0];
            const struct S0 **l_1899 = &l_1900;
            int32_t l_1911 = (-1L);
            int32_t l_1912[2][8][5] = {{{1L,0xAEE9868FL,0x8A633029L,(-2L),(-10L)},{5L,9L,(-6L),(-2L),(-6L)},{7L,7L,1L,0xFA6FD151L,(-2L)},{9L,(-1L),9L,1L,1L},{1L,(-2L),(-1L),0L,0xF03C1B88L},{1L,(-1L),0xAEE9868FL,0x4FF84041L,9L},{(-8L),7L,1L,(-1L),4L},{1L,9L,(-2L),4L,4L}},{{0x0D786614L,0xAEE9868FL,0x0D786614L,0xE5C7BD48L,9L},{0xAEE9868FL,0xE5C7BD48L,1L,(-6L),0xF03C1B88L},{(-10L),8L,(-10L),5L,1L},{0xFA6FD151L,0xB434B391L,1L,0xF03C1B88L,(-2L)},{0x8A633029L,0xFA6FD151L,0x0D786614L,(-10L),(-6L)},{(-1L),0xF79B60FDL,(-2L),(-1L),(-10L)},{(-1L),1L,1L,(-1L),8L},{0x8A633029L,5L,0xAEE9868FL,1L,(-8L)}}};
            uint8_t ** const ***l_1927 = (void*)0;
            uint32_t ***l_1948[4];
            int16_t * const **l_1958 = (void*)0;
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_1948[i] = &g_1601;
            l_1793 &= (safe_mod_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(0xC188L, ((*l_1792) &= (safe_mod_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u(((*l_1781) &= (l_1778 || (safe_rshift_func_int16_t_s_u(l_1778, 8)))), 11)), (safe_div_func_int8_t_s_s(((0UL || (((l_1789 |= ((g_382.f1 , ((*l_1785) = l_1784)) == l_1786)) , (*g_490)) , (((250UL | 0x59L) & (*g_6)) , l_1789))) , l_1790), g_1791))))))), l_1790));
            for (g_604 = 0; (g_604 > 31); g_604 = safe_add_func_uint32_t_u_u(g_604, 7))
            { /* block id: 786 */
                uint8_t *l_1829 = &g_141[1][1];
                int32_t l_1831 = 0xBED6A3C8L;
                uint32_t l_1863 = 4UL;
                const struct S0 **l_1902 = (void*)0;
                int32_t l_1903[10] = {6L,6L,6L,6L,6L,6L,6L,6L,6L,6L};
                uint32_t *l_1928 = &l_1863;
                uint64_t l_1941 = 0xA8F904BE5F107466LL;
                int i;
                for (l_1508 = 0; (l_1508 <= (-16)); l_1508--)
                { /* block id: 789 */
                    int64_t l_1812 = 1L;
                    int64_t l_1844[9] = {0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL,0xAF39DAFAC5182AAELL};
                    uint32_t l_1862 = 18446744073709551615UL;
                    int i;
                    for (l_1793 = 0; (l_1793 == (-3)); l_1793 = safe_sub_func_int32_t_s_s(l_1793, 1))
                    { /* block id: 792 */
                        uint8_t l_1800 = 255UL;
                        int8_t *l_1820 = &g_776;
                        int8_t *l_1821 = &g_480;
                        int32_t l_1822 = 3L;
                        int32_t l_1823 = 0x29C05DF1L;
                        l_1800 ^= (*g_863);
                        l_1823 |= (safe_lshift_func_uint8_t_u_s((safe_div_func_int64_t_s_s(((*g_490) = ((((safe_lshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(0x46L, (*g_450))), 6)) , (*p_3)) <= (safe_unary_minus_func_uint32_t_u((safe_mul_func_uint16_t_u_u(l_1800, l_1812))))) <= (l_1822 = (safe_div_func_uint64_t_u_u(g_930, (safe_sub_func_int8_t_s_s(((*l_1821) = (((safe_lshift_func_uint16_t_u_u(l_1812, 3)) , g_1819[0][0]) , ((*l_1820) = 0L))), (*g_6)))))))), l_1800)), 7));
                    }
                    for (g_1791 = 0; (g_1791 < 27); g_1791 = safe_add_func_int32_t_s_s(g_1791, 4))
                    { /* block id: 802 */
                        float l_1826 = 0xB.6A39D3p+33;
                        int32_t l_1832 = (-9L);
                        float *l_1843 = (void*)0;
                        float *l_1852 = &l_1637[6][1];
                        float *l_1855 = (void*)0;
                        float *l_1856 = &l_1826;
                        float *l_1857 = &g_142;
                        float *l_1858 = &g_56;
                        float *l_1860 = &g_1861;
                        l_1831 |= ((*g_1602) , (l_1830 = (safe_rshift_func_int8_t_s_s((p_4 == (l_1829 = p_3)), (*p_3)))));
                        l_1831 |= l_1812;
                        if (l_1832)
                            break;
                        l_1865 = (!((((void*)0 != l_1834) > (l_1812 >= 0xC.798706p+99)) <= (safe_add_func_float_f_f(((((*g_1602)--) , (safe_sub_func_float_f_f((safe_add_func_float_f_f((((*l_1860) = ((l_1844[8] = l_1812) != (safe_div_func_float_f_f((safe_add_func_float_f_f((!(((*l_1852) = (l_1850 == ((*g_1470) = &p_3))) > ((*l_1858) = (safe_add_func_float_f_f(0x3.9AA967p+12, (((*l_1857) = ((*l_1856) = 0x4.Fp+1)) >= 0x1.50FECCp-5)))))), l_1832)), l_1859)))) >= l_1778), l_1812)), l_1862))) > l_1863), l_1864))));
                    }
                    for (g_1673 = (-23); (g_1673 != 10); g_1673 = safe_add_func_int16_t_s_s(g_1673, 1))
                    { /* block id: 820 */
                        int64_t l_1868[5] = {1L,1L,1L,1L,1L};
                        int i;
                        l_1868[0] |= (l_1831 = (l_1789 = l_1812));
                    }
                    for (g_1791 = (-17); (g_1791 < 21); g_1791 = safe_add_func_uint8_t_u_u(g_1791, 8))
                    { /* block id: 827 */
                        uint32_t l_1871 = 0xFE547D70L;
                        float *l_1874 = &g_142;
                        float *l_1875 = &l_1637[0][1];
                        l_1871--;
                        if (l_1831)
                            continue;
                        (*l_1875) = ((*l_1874) = l_1871);
                        (**l_1441) = l_1875;
                    }
                }
                (*g_1036) = g_1876;
                for (g_970 = 0; (g_970 <= 1); g_970 += 1)
                { /* block id: 838 */
                    uint16_t l_1880 = 0x45AEL;
                    int32_t *l_1883 = &l_1789;
                    int32_t l_1890 = 0xF93AB1FAL;
                    int32_t l_1907 = 1L;
                    int32_t l_1908 = 4L;
                    int32_t l_1910[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_1910[i] = 0L;
                }
                if ((safe_mul_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s(((*l_1792) = ((safe_sub_func_int32_t_s_s((((*l_1781) = 6L) < (1UL | ((l_1927 = (g_1922 = g_1922)) == l_1785))), ((*l_1928) = 0x88332602L))) , (*l_1881))), (*l_1881))) <= (*g_6)), (*l_1881))))
                { /* block id: 866 */
                    uint16_t l_1929[8];
                    int i;
                    for (i = 0; i < 8; i++)
                        l_1929[i] = 65529UL;
                    --l_1929[7];
                }
                else
                { /* block id: 868 */
                    float l_1932 = 0xF.E2708Bp+74;
                    int32_t l_1933 = 0xEC0019DEL;
                    int32_t l_1934 = 0x546F655DL;
                    int32_t l_1935 = 9L;
                    int32_t l_1936[2][8];
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 8; j++)
                            l_1936[i][j] = 8L;
                    }
                    if ((((((**g_489) = (**g_489)) , func_16((((***g_1470) = (((*g_1926) >= l_1933) == ((*l_1792) = (0x6DB77FEEL ^ (l_1928 != (void*)0))))) || 0x3EL), (*g_449), (*p_5), l_1903[4], l_1903[8])) , (*g_490)) & g_146[0]))
                    { /* block id: 872 */
                        uint32_t l_1937 = 1UL;
                        if ((*l_1881))
                            break;
                        l_1937++;
                    }
                    else
                    { /* block id: 875 */
                        (*g_276) = (***l_1879);
                        l_1940 &= (0xD53BL || (g_268 , 1UL));
                        --l_1941;
                        if (l_1831)
                            continue;
                    }
                }
            }
            for (l_1595 = 0; (l_1595 >= (-18)); --l_1595)
            { /* block id: 885 */
                uint32_t ****l_1949 = &l_1948[1];
                int16_t **l_1964 = &g_45[5][2];
                int32_t l_1968 = 0x316836ACL;
                const int32_t *l_1971[6][4][10] = {{{(void*)0,&g_98,&l_1611,&l_1778,(void*)0,&g_98,&l_1778,&l_1611,&g_111,(void*)0},{&l_1611,&l_1778,&l_1778,&l_1778,&l_1611,&g_98,&g_98,&g_98,&g_98,&l_1611},{(void*)0,&l_1611,&l_1778,&g_98,(void*)0,&g_98,&g_98,(void*)0,&g_98,&l_1778},{&l_1611,&l_1611,&l_1611,&g_98,&g_98,&g_98,&g_98,&l_1611,&l_1778,&l_1778}},{{&l_1611,&g_98,(void*)0,&g_111,&l_1611,&l_1778,&g_98,(void*)0,&l_1778,&l_1611},{&l_1611,&l_1611,&g_111,(void*)0,&l_1778,&g_111,&g_98,&g_111,&l_1778,(void*)0},{&g_98,&l_1611,&g_98,&g_98,&l_1778,&l_1611,&g_98,&l_1611,&l_1611,&l_1778},{&g_111,&l_1778,(void*)0,(void*)0,(void*)0,&g_111,&l_1778,&l_1611,&l_1611,&l_1611}},{{&l_1778,&g_98,&g_98,&g_111,&l_1611,&l_1611,&g_111,&g_111,&l_1611,&l_1611},{(void*)0,&g_111,&g_111,(void*)0,&l_1778,&l_1778,&l_1611,(void*)0,&l_1611,&g_98},{&g_98,&l_1778,(void*)0,(void*)0,&g_111,&l_1778,&g_111,&l_1611,&l_1611,&l_1611},{&l_1611,&l_1611,&l_1611,(void*)0,&l_1611,&g_98,&l_1611,(void*)0,&l_1611,&g_98}},{{&g_111,&g_111,&l_1778,&g_111,&g_111,&g_98,&l_1778,&g_98,&l_1611,(void*)0},{&l_1778,&l_1778,&l_1778,(void*)0,&l_1778,&l_1778,&l_1778,&l_1611,&l_1611,(void*)0},{&l_1778,(void*)0,&l_1611,&g_98,&g_111,&g_98,&g_98,&l_1778,&l_1778,&g_98},{(void*)0,&l_1611,(void*)0,(void*)0,&l_1611,(void*)0,&g_98,&l_1778,&l_1778,&l_1611}},{{&l_1778,&g_98,&l_1778,&g_111,&g_111,&l_1611,&l_1778,(void*)0,&l_1778,&g_98},{&l_1778,&g_98,&l_1611,&g_98,&l_1778,(void*)0,&g_98,&l_1611,&g_98,&l_1611},{(void*)0,&g_98,&l_1611,&g_98,&l_1611,&g_98,(void*)0,&l_1611,&l_1778,(void*)0},{&l_1611,&g_111,&l_1778,&l_1611,&l_1778,&g_98,&l_1611,&l_1611,(void*)0,&l_1611}},{{&l_1611,&g_111,(void*)0,&g_98,&l_1611,&l_1778,&g_111,(void*)0,&l_1611,&l_1611},{&g_98,&l_1778,&l_1778,(void*)0,(void*)0,&l_1778,&l_1778,&g_98,&l_1778,&g_111},{(void*)0,&l_1778,&l_1778,&l_1611,(void*)0,&l_1611,&g_98,&l_1778,&g_111,(void*)0},{&l_1778,&l_1778,&l_1778,(void*)0,&l_1778,&l_1778,&l_1778,&g_98,&l_1611,&g_98}}};
                int32_t l_1976[10][2] = {{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L},{(-1L),0x1B369346L}};
                int i, j, k;
                if ((safe_mul_func_uint8_t_u_u((((*l_1949) = l_1948[2]) != &g_1601), 0xCEL)))
                { /* block id: 887 */
                    int8_t l_1957[1];
                    const uint8_t *l_1967 = &g_141[4][1];
                    const uint8_t **l_1966 = &l_1967;
                    const uint8_t ***l_1965 = &l_1966;
                    int32_t l_1974[8] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
                    int32_t l_1977 = (-7L);
                    int16_t l_1981 = (-1L);
                    int64_t *l_1985[5][7] = {{&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1]},{&g_753[0],&g_753[0],&g_753[0],&g_753[0],&g_753[0],&g_753[0],&g_753[0]},{&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1]},{&g_753[0],&g_753[0],&g_753[0],&g_753[0],&g_753[0],&g_753[0],&g_753[0]},{&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1],&g_753[1]}};
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_1957[i] = 0x87L;
                    if (l_1754)
                        goto lbl_1950;
                    if ((((safe_div_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((-8L), ((safe_add_func_int64_t_s_s((((**g_489) ^= l_1957[0]) || ((void*)0 != l_1958)), (safe_sub_func_uint8_t_u_u(((((*l_1881) ^ (safe_rshift_func_uint8_t_u_u(((l_1963[1] != (l_1957[0] , l_1964)) <= ((l_1957[0] || (-1L)) < l_1957[0])), (***g_1924)))) , l_1965) != (void*)0), l_1957[0])))) , l_1957[0]))), (*l_1881))) >= l_1968) & 2L))
                    { /* block id: 890 */
                        float *l_1972 = &g_56;
                        int32_t l_1973 = 0x4B0F32F3L;
                        int32_t l_1975[5] = {0xB1066093L,0xB1066093L,0xB1066093L,0xB1066093L,0xB1066093L};
                        int i;
                        (*l_1881) = (*l_1881);
                        (*l_1972) = (safe_sub_func_float_f_f((*g_1472), ((void*)0 != l_1971[4][3][1])));
                        g_1978[5][6][1]++;
                    }
                    else
                    { /* block id: 894 */
                        int64_t *l_1986 = &g_753[1];
                        (*l_1881) = ((((**g_1925) = (l_1981 ^ g_1982)) != ((safe_mul_func_uint16_t_u_u((((*l_1881) <= ((l_1985[4][5] != l_1986) <= (l_1974[1] > (l_1987 != (void*)0)))) > ((18446744073709551615UL || (-8L)) && 0x581878DD412DB206LL)), l_1974[6])) || g_15[0][1])) & l_1976[1][1]);
                        (*l_1881) = l_1976[4][1];
                    }
                    for (g_983 = 0; (g_983 <= 1); g_983 += 1)
                    { /* block id: 901 */
                        (*l_1881) ^= (*g_451);
                        if (l_1974[3])
                            break;
                    }
                }
                else
                { /* block id: 905 */
                    (*l_1881) &= 0xF59C4CA9L;
                }
            }
        }
    }
    return (**g_1682);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int8_t * const  func_8(int16_t  p_9, int8_t * p_10, int8_t * p_11, int8_t * p_12, int8_t  p_13)
{ /* block id: 607 */
    int32_t *l_1400 = &g_65[5];
    int32_t *l_1401 = &g_65[3];
    int32_t *l_1402 = &g_983;
    int32_t *l_1403 = &g_48;
    int32_t *l_1404 = (void*)0;
    int32_t *l_1405 = &g_983;
    int32_t l_1406 = 0x9DEFC0E4L;
    int32_t l_1407[2];
    int32_t *l_1408 = (void*)0;
    int32_t *l_1409 = &g_65[4];
    int32_t *l_1410 = (void*)0;
    int32_t *l_1411 = &g_65[3];
    int32_t *l_1412 = &g_143;
    int32_t *l_1413[1];
    uint32_t l_1414[9] = {0x8EC92BD9L,0x8EC92BD9L,0UL,0x8EC92BD9L,0x8EC92BD9L,0UL,0x8EC92BD9L,0x8EC92BD9L,0UL};
    const int8_t * const l_1417 = &g_776;
    int i;
    for (i = 0; i < 2; i++)
        l_1407[i] = (-1L);
    for (i = 0; i < 1; i++)
        l_1413[i] = &g_48;
    ++l_1414[8];
    return l_1417;
}


/* ------------------------------------------ */
/* 
 * reads : g_7 g_489 g_490 g_480 g_65 g_859 g_143 g_860 g_863 g_48 g_1296
 * writes: g_480 g_46 g_65 g_143 g_56 g_48
 */
static union U1  func_16(uint8_t  p_17, int8_t * p_18, int8_t  p_19, const uint32_t  p_20, const int8_t  p_21)
{ /* block id: 357 */
    int64_t l_855 = 0xC61E00BA5E3FB2CCLL;
    int32_t l_857 = 9L;
    uint64_t l_861 = 1UL;
    float l_904 = 0x0.Dp-1;
    int32_t l_905 = (-1L);
    int32_t l_910 = 1L;
    int32_t l_913[3][7] = {{0x2BEE1975L,0x2BEE1975L,0L,0x2BEE1975L,0x2BEE1975L,0L,0x2BEE1975L},{0x4A8A05ACL,0x4E2C7EF8L,0x4E2C7EF8L,0x4A8A05ACL,0x4E2C7EF8L,0x4E2C7EF8L,0x4A8A05ACL},{0x6E960D46L,0x2BEE1975L,0x6E960D46L,0x6E960D46L,0x2BEE1975L,0x6E960D46L,0x6E960D46L}};
    const int64_t *l_956 = (void*)0;
    const int64_t **l_955 = &l_956;
    const int64_t ***l_954[1];
    int8_t l_981 = (-6L);
    uint64_t l_984 = 0x7CA325816C57CC89LL;
    int8_t **l_1025 = (void*)0;
    struct S0 *l_1038 = (void*)0;
    int32_t *l_1128[3];
    uint32_t l_1181 = 0x0276DBF0L;
    float l_1230[4][7] = {{0xF.EEBD52p+10,0xE.243CB0p-8,0xF.EEBD52p+10,0xE.243CB0p-8,0xF.EEBD52p+10,0xE.243CB0p-8,0xF.EEBD52p+10},{0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39},{0xF.EEBD52p+10,0xE.243CB0p-8,0xF.EEBD52p+10,0xE.243CB0p-8,0xF.EEBD52p+10,0xE.243CB0p-8,0xF.EEBD52p+10},{0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39,0x0.AF1763p+39}};
    uint8_t *l_1235 = &g_141[0][1];
    uint8_t l_1276[2];
    int32_t *l_1293 = &g_143;
    int8_t l_1362 = 0xB4L;
    int32_t *l_1364 = (void*)0;
    uint16_t *l_1370 = &g_930;
    int i, j;
    for (i = 0; i < 1; i++)
        l_954[i] = &l_955;
    for (i = 0; i < 3; i++)
        l_1128[i] = (void*)0;
    for (i = 0; i < 2; i++)
        l_1276[i] = 1UL;
    for (g_480 = 9; (g_480 >= 0); g_480 -= 1)
    { /* block id: 360 */
        int64_t *l_856 = &g_487;
        int16_t *l_858 = &g_46;
        int i;
        (*g_859) ^= (g_65[g_480] &= (safe_mod_func_uint16_t_u_u(l_855, (((((*p_18) , (l_855 || ((*g_489) != l_856))) == ((((*l_858) = (l_857 = 0xBFF3L)) ^ l_855) , (&l_858 != (void*)0))) == 0xE041L) && 7L))));
        (*g_860) = (g_65[g_480] < p_17);
    }
    (*g_863) ^= l_861;
    return g_1296;
}


/* ------------------------------------------ */
/* 
 * reads : g_143 g_459 g_162 g_851 g_766 g_449 g_450
 * writes: g_143 g_157 g_162 g_376
 */
static int8_t * func_23(int16_t  p_24, int16_t * const  p_25, uint32_t  p_26, float  p_27)
{ /* block id: 342 */
    volatile struct S0 *l_852[9] = {&g_300,&g_730[7],&g_300,&g_730[7],&g_300,&g_730[7],&g_300,&g_730[7],&g_300};
    int i;
    for (g_143 = 1; (g_143 >= 0); g_143 -= 1)
    { /* block id: 345 */
        int16_t l_848[8] = {0xB8DDL,0xB8DDL,0xB8DDL,0xB8DDL,0xB8DDL,0xB8DDL,0xB8DDL,0xB8DDL};
        int32_t *l_849[10] = {&g_65[3],&g_65[3],&g_65[3],&g_65[3],&g_65[3],&g_65[3],&g_65[3],&g_65[3],&g_65[3],&g_65[3]};
        int8_t *l_850 = &g_7;
        int i;
        if (l_848[5])
            break;
        if (p_24)
            continue;
        (*g_459) = l_849[1];
        for (g_162 = 3; (g_162 <= 9); g_162 += 1)
        { /* block id: 351 */
            return l_850;
        }
    }
    (*g_766) = g_851;
    return (*g_449);
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_451 g_65 g_188 g_487 g_157 g_146 g_143 g_351 g_189.f0 g_48 g_449 g_450 g_7 g_110 g_729 g_730 g_98 g_604 g_489 g_490 g_758 g_766 g_299 g_459 g_162 g_809 g_141 g_376.f2 g_625.f1 g_480
 * writes: g_48 g_625 g_487 g_143 g_266 g_129 g_480 g_146 g_604 g_376 g_157 g_351 g_162
 */
static uint64_t  func_34(uint64_t  p_35, uint16_t  p_36, int32_t  p_37, int8_t * p_38)
{ /* block id: 5 */
    uint32_t l_68 = 0xDCB3DF1CL;
    int32_t l_630 = 0xB74F6348L;
    int32_t l_632 = (-1L);
    int32_t l_635[10];
    int16_t l_638 = 0xDFB5L;
    uint16_t l_641 = 65533UL;
    int32_t *l_645[4][4][5] = {{{&l_632,&g_143,&g_143,&l_630,&g_65[3]},{&l_635[2],(void*)0,&g_143,&l_635[0],&g_143},{&g_65[3],&g_48,&l_630,&g_48,&g_65[3]},{&g_65[3],&l_635[2],&l_632,&g_65[3],&g_48}},{{&l_635[2],&l_630,&g_65[3],&l_632,&l_630},{&l_632,&g_65[3],&l_630,&l_635[2],&g_48},{&g_65[3],&l_632,&l_635[2],&g_65[3],&g_65[3]},{&g_48,&l_630,&g_48,&g_65[3],&g_143}},{{&l_635[0],&g_143,(void*)0,&l_635[2],&g_65[3]},{&l_630,&g_143,&g_143,&l_632,(void*)0},{&l_635[7],&l_632,(void*)0,&g_65[3],&l_630},{&l_635[7],&g_65[3],&g_48,&g_48,&g_65[3]}},{{(void*)0,&g_65[3],&l_635[2],&l_635[0],&g_143},{&g_65[3],&l_632,&l_630,&l_630,&l_632},{(void*)0,&g_143,&g_65[3],&l_635[7],&l_632},{&g_65[3],&g_143,&l_632,&l_635[7],&l_635[2]}}};
    uint8_t l_654[2][1][2] = {{{0xC6L,0xC6L}},{{0xC6L,0xC6L}}};
    float l_658 = (-0x7.Ap+1);
    int32_t l_659 = 0xA8AD96C6L;
    int8_t ***l_752[8][7][4] = {{{&g_449,(void*)0,(void*)0,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{(void*)0,(void*)0,&g_449,&g_449}},{{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,(void*)0,(void*)0,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449}},{{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{(void*)0,(void*)0,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449}},{{&g_449,&g_449,&g_449,&g_449},{&g_449,(void*)0,(void*)0,(void*)0},{(void*)0,&g_449,&g_449,&g_449},{&g_449,(void*)0,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,(void*)0,&g_449,&g_449},{&g_449,&g_449,(void*)0,(void*)0}},{{&g_449,&g_449,(void*)0,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{(void*)0,&g_449,&g_449,(void*)0},{(void*)0,&g_449,&g_449,&g_449}},{{&g_449,(void*)0,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,(void*)0,&g_449,&g_449},{&g_449,&g_449,(void*)0,(void*)0},{&g_449,&g_449,(void*)0,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449}},{{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{(void*)0,&g_449,&g_449,(void*)0},{(void*)0,&g_449,&g_449,&g_449},{&g_449,(void*)0,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,(void*)0,&g_449,&g_449}},{{&g_449,&g_449,(void*)0,(void*)0},{&g_449,&g_449,(void*)0,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{&g_449,&g_449,&g_449,&g_449},{(void*)0,&g_449,&g_449,(void*)0}}};
    uint8_t ****l_757 = &g_620;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_635[i] = (-9L);
lbl_764:
    for (g_48 = 9; (g_48 == 2); g_48--)
    { /* block id: 8 */
        int32_t *l_64 = &g_65[3];
        int32_t *l_66 = &g_65[2];
        int32_t *l_67 = (void*)0;
        if (g_57)
            break;
        l_68--;
        if (p_37)
            break;
    }
    for (l_68 = 0; (l_68 <= 1); l_68++)
    { /* block id: 15 */
        int8_t *l_75[4][1][2] = {{{&g_7,&g_7}},{{&g_7,&g_7}},{{&g_7,&g_7}},{{&g_7,&g_7}}};
        int8_t **l_76 = &l_75[2][0][0];
        int32_t l_623[9] = {0x2E20175EL,0x2E20175EL,0x2E20175EL,0x2E20175EL,0x2E20175EL,0x2E20175EL,0x2E20175EL,0x2E20175EL,0x2E20175EL};
        float l_633 = (-0x1.Bp+1);
        float l_636 = 0x2.E60316p+5;
        int64_t l_646 = 0x4B89AD34F684ADCELL;
        int32_t l_652 = 0xDBFD92EBL;
        uint8_t l_667 = 0x53L;
        int64_t ** const *l_728 = &g_489;
        int32_t l_733 = 0L;
        int i, j, k;
        if (func_73(((*l_76) = l_75[2][0][0])))
        { /* block id: 235 */
            struct S0 *l_624[10][10][2] = {{{(void*)0,&g_382},{&g_382,&g_189},{&g_188,(void*)0},{&g_189,&g_189},{(void*)0,&g_189},{&g_189,(void*)0},{&g_189,&g_431},{&g_382,&g_431},{&g_188,&g_431},{(void*)0,&g_431}},{{&g_188,&g_431},{&g_382,&g_431},{&g_189,(void*)0},{&g_189,&g_189},{(void*)0,&g_189},{&g_189,(void*)0},{&g_188,&g_189},{&g_382,&g_382},{(void*)0,&g_189},{&g_189,&g_431}},{{&g_189,&g_382},{(void*)0,&g_431},{(void*)0,(void*)0},{&g_382,&g_189},{(void*)0,&g_188},{&g_189,(void*)0},{(void*)0,&g_189},{&g_189,&g_188},{&g_382,&g_188},{&g_382,&g_382}},{{&g_431,&g_431},{&g_188,(void*)0},{&g_188,&g_189},{&g_188,&g_188},{&g_189,&g_188},{&g_189,&g_382},{&g_189,(void*)0},{&g_382,&g_188},{&g_189,(void*)0},{&g_188,&g_431}},{{&g_431,(void*)0},{&g_189,&g_188},{&g_431,&g_382},{&g_431,&g_188},{&g_431,&g_431},{&g_189,&g_189},{(void*)0,(void*)0},{&g_431,&g_431},{(void*)0,&g_431},{&g_382,(void*)0}},{{&g_382,&g_431},{(void*)0,&g_431},{&g_431,(void*)0},{(void*)0,&g_189},{&g_189,&g_431},{&g_431,&g_188},{&g_431,&g_382},{&g_431,&g_188},{&g_189,(void*)0},{&g_431,&g_431}},{{&g_188,(void*)0},{&g_189,&g_188},{&g_382,(void*)0},{&g_189,&g_382},{&g_189,&g_188},{&g_189,&g_188},{&g_188,&g_189},{&g_188,(void*)0},{&g_188,&g_431},{&g_431,&g_382}},{{&g_382,&g_188},{&g_382,&g_188},{&g_189,&g_189},{(void*)0,(void*)0},{&g_189,&g_188},{(void*)0,&g_189},{&g_382,(void*)0},{(void*)0,&g_431},{(void*)0,&g_382},{&g_431,(void*)0}},{{&g_382,&g_382},{&g_189,&g_382},{&g_188,&g_188},{&g_188,(void*)0},{&g_431,&g_382},{(void*)0,(void*)0},{&g_382,&g_189},{&g_188,&g_189},{&g_431,&g_188},{&g_189,(void*)0}},{{&g_189,(void*)0},{&g_189,&g_188},{&g_431,&g_189},{&g_188,&g_189},{&g_382,(void*)0},{(void*)0,&g_382},{&g_431,(void*)0},{&g_188,&g_188},{&g_188,&g_382},{&g_189,&g_382}}};
            int32_t l_631 = 7L;
            int32_t l_634 = 7L;
            int32_t l_637 = 0xD880A685L;
            int32_t l_639[6] = {0xA4BE7DDBL,0xA4BE7DDBL,0xA4BE7DDBL,0xA4BE7DDBL,0xA4BE7DDBL,0xA4BE7DDBL};
            int32_t l_657 = 3L;
            uint8_t l_662 = 255UL;
            int i, j, k;
            if (l_623[8])
                break;
            g_625 = g_188;
            for (g_487 = 3; (g_487 >= 0); g_487 -= 1)
            { /* block id: 240 */
                int32_t *l_626 = &g_48;
                int32_t *l_627 = &g_143;
                int32_t *l_628 = &g_48;
                int32_t *l_629[6] = {&g_65[5],&g_65[5],&g_65[5],&g_65[5],&g_65[5],&g_65[5]};
                int16_t l_640 = (-1L);
                int64_t l_649 = 0xA3244E3B8AD0BEAELL;
                int i;
                --l_641;
                if (l_638)
                    continue;
                for (p_35 = 0; (p_35 <= 0); p_35 += 1)
                { /* block id: 245 */
                    int32_t **l_644 = (void*)0;
                    int32_t l_651 = (-1L);
                    int16_t l_653 = 0L;
                    int32_t l_660[3][1];
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_660[i][j] = 0xA054C0E0L;
                    }
                    l_645[2][2][1] = g_157[g_487][(p_35 + 2)][(g_487 + 2)];
                    for (l_638 = 0; (l_638 <= 3); l_638 += 1)
                    { /* block id: 249 */
                        int32_t l_647 = 4L;
                        int64_t l_648 = 0L;
                        int32_t l_650[2];
                        int8_t l_661 = 0x0DL;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_650[i] = (-1L);
                        l_628 = g_157[(l_638 + 1)][l_638][(p_35 + 4)];
                        l_654[0][0][0]--;
                        l_662--;
                        (*l_627) ^= g_146[p_35];
                    }
                }
            }
            if (p_36)
                break;
        }
        else
        { /* block id: 258 */
            float l_674 = 0x7.Cp+1;
            int32_t l_682 = 1L;
            uint8_t **l_684 = (void*)0;
            uint8_t l_697 = 0x46L;
            uint32_t l_717[2][10][9] = {{{0x6A95F08BL,0xD5FA3EB1L,0xD46C16A5L,1UL,0xD2B3BC5BL,0x6A95F08BL,3UL,0xAE7DD68AL,0UL},{0UL,0x87C45106L,6UL,0xB0070D3AL,0xB1FB2E55L,0xD5FA3EB1L,0x5D2B8F12L,6UL,0xF8688ABEL},{0xD2B3BC5BL,18446744073709551613UL,0xA14ADAD7L,0x68CF38AAL,1UL,0x9A7A6903L,0UL,18446744073709551615UL,0x13C51765L},{0xD46C16A5L,0UL,0x04321C18L,0x223D95CDL,0x1511349EL,0x9D30082BL,18446744073709551610UL,6UL,0x7EDF961CL},{0xEE5AFECAL,18446744073709551615UL,0x1511349EL,18446744073709551606UL,1UL,18446744073709551606UL,0x1511349EL,18446744073709551615UL,0xEE5AFECAL},{0UL,1UL,18446744073709551615UL,0x1511349EL,18446744073709551608UL,0xF388D77EL,0x8898E36BL,0x69848416L,0xA14ADAD7L},{0x7EDF961CL,0x8890E3E8L,0x5D2B8F12L,1UL,0UL,1UL,1UL,0xF49B2444L,18446744073709551615UL},{0UL,18446744073709551608UL,0x8898E36BL,0xB1FB2E55L,7UL,0UL,18446744073709551608UL,18446744073709551615UL,0xF82392E7L},{0xEE5AFECAL,0xD2B3BC5BL,0xAE7DD68AL,18446744073709551615UL,0x223D95CDL,6UL,0x97D6906CL,18446744073709551606UL,18446744073709551615UL},{0xD46C16A5L,0x4E012196L,0x1511349EL,0UL,0x97D6906CL,0UL,0x3674EC76L,18446744073709551615UL,0x04321C18L}},{{0UL,0x4E012196L,0xF82392E7L,0xA14ADAD7L,0UL,3UL,0x8898E36BL,18446744073709551615UL,1UL},{0x3674EC76L,0xD2B3BC5BL,0xF49B2444L,0x13C51765L,18446744073709551615UL,0x4E012196L,18446744073709551615UL,0x13C51765L,0xF49B2444L},{18446744073709551608UL,18446744073709551608UL,0x223D95CDL,0x87C45106L,1UL,0xD5FA3EB1L,3UL,0x3674EC76L,0xB1341458L},{6UL,0x8890E3E8L,0UL,7UL,0x8898E36BL,6UL,18446744073709551612UL,0x04321C18L,0x7EDF961CL},{18446744073709551609UL,1UL,0x223D95CDL,0x67F21555L,0x8890E3E8L,0xB0070D3AL,0x7EDF961CL,0x223D95CDL,0UL},{0UL,18446744073709551615UL,0xF49B2444L,0x1511349EL,0xC7FE0F6BL,0x9A7A6903L,18446744073709551615UL,0x87C45106L,1UL},{0x1511349EL,0UL,0xF82392E7L,1UL,18446744073709551615UL,18446744073709551615UL,18446744073709551610UL,0xF49B2444L,1UL},{3UL,0xF388D77EL,0x1511349EL,1UL,18446744073709551615UL,6UL,1UL,18446744073709551615UL,0xB1341458L},{0xF8688ABEL,18446744073709551613UL,0xAE7DD68AL,0x1511349EL,0xA14ADAD7L,0x9D30082BL,0x8890E3E8L,0x9D30082BL,0xA14ADAD7L},{0x67F21555L,0x8898E36BL,0x8898E36BL,0x67F21555L,0UL,0UL,0x3674EC76L,0xA14ADAD7L,0xAE7DD68AL}}};
            uint8_t *l_721 = &l_654[0][0][0];
            uint8_t **l_720 = &l_721;
            int32_t l_735 = 4L;
            int32_t l_736 = 0x58938826L;
            int32_t l_739 = 6L;
            int32_t l_740 = 0x29423028L;
            int32_t l_741 = 1L;
            int32_t l_742 = 2L;
            int32_t l_744 = 1L;
            int32_t l_774 = 0x516F48FBL;
            int32_t l_775 = 0x18CC7B28L;
            uint8_t ****l_819 = &g_620;
            int8_t l_842 = (-9L);
            int32_t l_843 = 0L;
            int i, j, k;
            for (g_48 = 0; (g_48 <= (-16)); g_48 = safe_sub_func_int16_t_s_s(g_48, 3))
            { /* block id: 261 */
                int16_t **l_672 = (void*)0;
                uint8_t ***l_685 = &l_684;
                uint8_t *l_687[1][5][3] = {{{&l_654[0][0][0],&l_654[0][0][1],&l_654[0][0][0]},{&l_654[0][0][0],&l_654[0][0][0],&l_654[0][0][0]},{&l_654[0][0][0],&l_654[0][0][1],&l_654[0][0][0]},{&l_654[0][0][0],&l_654[0][0][0],&l_654[0][0][0]},{&l_654[0][0][0],&l_654[0][0][1],&l_654[0][0][0]}}};
                uint8_t **l_686[9][8][3] = {{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}},{{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]},{&l_687[0][0][0],&l_687[0][0][0],&l_687[0][0][0]},{&l_687[0][2][0],&l_687[0][2][0],&l_687[0][2][0]}}};
                int32_t l_694 = 0x539AE094L;
                uint16_t *l_700[1];
                int32_t l_715 = 0x47DA4B9FL;
                int16_t *l_716 = &g_129;
                uint32_t *l_718[1][3];
                uint32_t l_719[1];
                uint32_t *l_723 = &l_719[0];
                uint32_t **l_722 = &l_723;
                int32_t l_743 = 0L;
                uint8_t l_747 = 254UL;
                int32_t **l_771 = &l_645[2][2][1];
                uint64_t *l_783 = &g_351;
                int8_t ***l_827 = &l_76;
                int32_t l_835 = 0x78C78A85L;
                int32_t l_836 = 0x28B806A2L;
                int32_t l_837 = (-1L);
                int32_t l_838 = 0L;
                int32_t l_839 = (-6L);
                int32_t l_841 = 0x53C5A45FL;
                uint64_t l_844 = 0UL;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_700[i] = &g_604;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_718[i][j] = &g_146[0];
                }
                for (i = 0; i < 1; i++)
                    l_719[i] = 1UL;
                ++l_667;
                for (l_632 = 0; (l_632 == 15); ++l_632)
                { /* block id: 265 */
                    int16_t ***l_673 = &g_266;
                    int32_t l_675 = 0x08C3D787L;
                    (*l_673) = l_672;
                    p_37 ^= 0x61E100B1L;
                    for (g_129 = 0; (g_129 <= 3); g_129 += 1)
                    { /* block id: 270 */
                        uint32_t l_676 = 18446744073709551607UL;
                        int i, j, k;
                        ++l_676;
                        if (p_37)
                            continue;
                        if (p_37)
                            break;
                        return g_351;
                    }
                }
                if ((((&l_68 == ((*l_722) = (((safe_div_func_uint16_t_u_u(p_37, ((~l_682) , ((safe_unary_minus_func_uint64_t_u(((((l_686[2][2][0] = ((*l_685) = l_684)) != ((safe_mod_func_int32_t_s_s((((safe_add_func_uint8_t_u_u(((safe_add_func_int8_t_s_s((l_694 = l_694), ((safe_add_func_int32_t_s_s(l_697, ((safe_sub_func_uint16_t_u_u((p_36 ^= g_189.f0), ((p_37 || (g_146[0] |= ((safe_add_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(((((((safe_rshift_func_int8_t_s_u((g_480 = (((safe_lshift_func_int16_t_s_u(((*l_716) = ((l_715 = ((safe_rshift_func_int16_t_s_u(((safe_add_func_int64_t_s_s((((safe_mod_func_uint8_t_u_u(p_37, l_652)) , 4UL) != l_697), p_35)) & 255UL), p_37)) & g_48)) >= 1UL)), 1)) == p_37) < 0x96310131L)), p_35)) , (**g_449)) > 0UL) < 0x9643L) | p_37) && l_682), 0x9812L)), 0xCDD4L)) || l_717[1][1][4]))) , p_35))) == l_646))) ^ 4L))) , l_719[0]), (*p_38))) < 1L) , 0L), 0x7190D0B8L)) , l_720)) , 0xE0EEL) & 0xFFE9L))) , p_37)))) & 4294967295UL) , g_110))) && l_694) , (-1L)))
                { /* block id: 286 */
                    int64_t l_734[1];
                    int32_t l_738[8][2] = {{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)}};
                    uint8_t ****l_756 = &g_620;
                    int32_t l_763 = 0x47B61107L;
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_734[i] = 9L;
                    if (((((((p_35 , (safe_div_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_u((l_728 != l_728), (((((0x289C3B88L >= ((g_729[1][3][0] , (g_730[2] , &g_450)) != &g_450)) , (((safe_add_func_uint16_t_u_u(1UL, g_98)) && p_37) | (*p_38))) , &g_451) != (void*)0) & 0x73D4B6C4ACD21107LL))) > 0x0009L), 0x7E4AL))) , l_733) , 0x94L) || (**g_449)) , l_682) | l_623[8]))
                    { /* block id: 287 */
                        float l_737[3];
                        int32_t l_745 = 0xF18108C6L;
                        int32_t l_746[1];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_737[i] = 0x7.51EBDBp+61;
                        for (i = 0; i < 1; i++)
                            l_746[i] = 0L;
                        ++l_747;
                    }
                    else
                    { /* block id: 289 */
                        int64_t l_761 = 0xC2B9A66D0885A4E3LL;
                        int32_t l_762[3][7] = {{0L,0L,0L,0L,0L,0L,0L},{0x67B30E89L,0x67B30E89L,0x67B30E89L,0x67B30E89L,0x67B30E89L,0x67B30E89L,0x67B30E89L},{0L,0L,0L,0L,0L,0L,0L}};
                        int i, j;
                        l_763 |= ((safe_sub_func_int64_t_s_s((l_623[8] = ((***l_728) = (((p_37 = ((p_36 = (l_752[2][1][1] == &l_76)) <= (--g_604))) , l_756) != l_757))), (g_758 , (safe_add_func_int32_t_s_s(((l_762[0][0] = ((l_738[7][1] >= 0x0BD0L) ^ ((4294967289UL || (l_740 , l_761)) ^ p_35))) >= 0x3E30FA9CL), l_738[7][1]))))) >= p_35);
                        if (l_763)
                            goto lbl_764;
                    }
                    (*g_766) = g_730[2];
                }
                else
                { /* block id: 300 */
                    int32_t **l_768 = &g_157[4][0][0];
                    (*l_768) = (*g_299);
                    l_623[8] &= (((*l_768) = (void*)0) == &p_37);
                    return p_35;
                }
                if (((((safe_lshift_func_uint8_t_u_u((((((*l_771) = &p_37) != (*g_459)) <= (safe_lshift_func_int8_t_s_u((4294967295UL ^ (l_774 , (++g_146[0]))), (safe_mul_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((((--(*l_783)) || l_697) && (*p_38)), (1L ^ (*p_38)))), (*p_38)))))) | (-1L)), 6)) , g_604) , 0xC9A43995L) > l_623[8]))
                { /* block id: 309 */
                    int8_t l_798 = 1L;
                    int32_t l_810 = 0x5BC715B1L;
                    l_774 |= (safe_mod_func_int8_t_s_s((l_810 |= (safe_mul_func_uint16_t_u_u((safe_div_func_uint32_t_u_u(g_604, (safe_lshift_func_int8_t_s_s((((++g_162) , (void*)0) == (void*)0), 3)))), ((((safe_mod_func_uint64_t_u_u(l_798, (safe_mod_func_uint64_t_u_u(((safe_sub_func_int32_t_s_s(l_798, p_36)) >= ((safe_div_func_uint8_t_u_u((((((((**l_720) = (((((safe_rshift_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(((g_809 , (l_741 ^ p_35)) || l_743), (**g_449))), 12)) != 0xF63579307F33C422LL) ^ 248UL) < (*p_38)) || 0xA3L)) , p_37) < p_37) ^ p_35) | p_36) == g_141[0][1]), (*p_38))) < p_37)), g_141[3][0])))) != p_37) , p_35) == 0x858FL)))), p_35));
                    for (l_659 = 0; (l_659 > 12); l_659++)
                    { /* block id: 316 */
                        return g_376[4].f2;
                    }
                }
                else
                { /* block id: 319 */
                    uint16_t l_823 = 0x214DL;
                    int32_t l_832 = 0x4286E19DL;
                    int32_t l_833 = 0x2D3CD2E0L;
                    int32_t l_834[5][1];
                    int i, j;
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_834[i][j] = (-7L);
                    }
                    for (l_715 = 0; (l_715 <= 0); l_715 += 1)
                    { /* block id: 322 */
                        return g_809.f2;
                    }
                    for (p_35 = 0; (p_35 < 14); ++p_35)
                    { /* block id: 327 */
                        float *l_820 = &l_633;
                        float *l_821 = &l_674;
                        float *l_822[2];
                        int32_t l_828 = 0L;
                        uint16_t l_829[4][1];
                        int32_t l_840 = 0x29C6A70BL;
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_822[i] = &g_56;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_829[i][j] = 6UL;
                        }
                        l_828 = (safe_sub_func_float_f_f(((((l_823 = (safe_mul_func_float_f_f(((*l_820) = (p_36 , (l_819 != &g_620))), ((*l_821) = (g_625.f1 , (0xD.C953A3p+84 >= (0x3.9p-1 != l_740))))))) > p_37) != ((((safe_unary_minus_func_uint16_t_u((safe_mul_func_int8_t_s_s((((void*)0 != p_38) , l_744), 1UL)))) && (*g_450)) , (void*)0) != l_827)) == p_35), 0x0.Cp-1));
                        if (l_828)
                            continue;
                        l_829[2][0]++;
                        --l_844;
                    }
                }
            }
        }
    }
    return g_480;
}


/* ------------------------------------------ */
/* 
 * reads : g_57
 * writes: g_57
 */
static uint16_t  func_39(int32_t  p_40, int64_t  p_41, int32_t  p_42, int64_t  p_43, int16_t * p_44)
{ /* block id: 2 */
    int32_t *l_47 = &g_48;
    int32_t l_49 = (-10L);
    int32_t l_50 = 0x4B72B416L;
    int32_t *l_51 = &l_49;
    int32_t *l_52 = &g_48;
    int32_t *l_53 = &l_50;
    int32_t *l_54 = &l_49;
    int32_t *l_55[2];
    int i;
    for (i = 0; i < 2; i++)
        l_55[i] = &g_48;
    g_57--;
    return p_40;
}


/* ------------------------------------------ */
/* 
 * reads : g_451 g_65
 * writes:
 */
static const int32_t  func_73(int8_t * p_74)
{ /* block id: 17 */
    float l_83 = (-0x3.6p+1);
    int32_t l_84 = 0x9A43D547L;
    int32_t *l_97 = &g_98;
    volatile struct S0 *l_301 = &g_300;
    uint8_t *l_326 = (void*)0;
    int32_t l_405 = 0L;
    int32_t l_406 = 0L;
    int32_t l_409 = 1L;
    int32_t l_510 = 0xFC5A2E17L;
    int32_t l_511 = 1L;
    int32_t l_516 = 1L;
    int32_t l_517 = 2L;
    int32_t l_518[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    uint16_t l_519[9][7] = {{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L},{0xDDF9L,65535UL,0xDDF9L,0xE8C0L,1UL,1UL,0xE8C0L}};
    uint8_t ***l_562 = (void*)0;
    uint8_t ***l_563 = (void*)0;
    uint8_t ***l_564 = (void*)0;
    uint8_t **l_566[4][2][8] = {{{&l_326,(void*)0,&l_326,&l_326,&l_326,&l_326,&l_326,(void*)0},{&l_326,&l_326,&l_326,(void*)0,&l_326,&l_326,&l_326,&l_326}},{{&l_326,(void*)0,(void*)0,(void*)0,&l_326,&l_326,&l_326,(void*)0},{&l_326,(void*)0,&l_326,&l_326,&l_326,&l_326,&l_326,(void*)0}},{{&l_326,&l_326,&l_326,(void*)0,&l_326,&l_326,&l_326,&l_326},{&l_326,(void*)0,(void*)0,(void*)0,&l_326,&l_326,&l_326,(void*)0}},{{&l_326,(void*)0,&l_326,&l_326,&l_326,&l_326,&l_326,(void*)0},{&l_326,&l_326,&l_326,(void*)0,&l_326,&l_326,&l_326,&l_326}}};
    uint8_t ***l_565 = &l_566[3][0][0];
    uint16_t *l_593 = &l_519[4][6];
    uint32_t l_600 = 1UL;
    int8_t l_601 = 0xEBL;
    uint64_t *l_602 = &g_351;
    uint16_t *l_603 = &g_604;
    int i, j, k;
    return (*g_451);
}


/* ------------------------------------------ */
/* 
 * reads : g_143 g_129 g_48 g_156 g_188 g_189.f2 g_111 g_57 g_189.f1 g_98 g_65 g_7 g_146 g_237 g_268 g_46 g_276 g_141 g_157 g_299
 * writes: g_143 g_129 g_48 g_157 g_162 g_189 g_146 g_111 g_266 g_56
 */
static uint32_t  func_79(uint8_t  p_80, int8_t * p_81, int16_t  p_82)
{ /* block id: 37 */
    const int16_t *l_159[7] = {&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46};
    const int16_t **l_158 = &l_159[0];
    uint16_t *l_161 = &g_162;
    int32_t l_174 = 0x471B29B6L;
    int32_t l_175 = 0x70171C97L;
    int32_t l_176 = 0L;
    int32_t l_177 = 0L;
    int32_t l_178 = 0x754EA53DL;
    int32_t l_179[5][8] = {{(-8L),(-3L),7L,0x6A96E86DL,7L,(-3L),0x6A96E86DL,(-5L)},{0xFD98BFE2L,0x7FC80EF8L,(-8L),0x6A96E86DL,0x6A96E86DL,(-8L),0x7FC80EF8L,0xFD98BFE2L},{(-5L),0x6A96E86DL,(-3L),7L,0x6A96E86DL,7L,(-3L),0x6A96E86DL},{0xFD98BFE2L,(-3L),(-5L),0xFD98BFE2L,7L,7L,0xFD98BFE2L,(-5L)},{0x6A96E86DL,0x6A96E86DL,(-8L),0x7FC80EF8L,0xFD98BFE2L,(-8L),0xFD98BFE2L,0x7FC80EF8L}};
    int64_t l_182[4];
    float l_267 = 0x6.Ap-1;
    int32_t **l_271 = &g_110;
    int i, j;
    for (i = 0; i < 4; i++)
        l_182[i] = 0x75C3E23C1EDEDEB6LL;
    for (g_143 = 0; (g_143 != 29); g_143++)
    { /* block id: 40 */
        const int16_t ***l_160 = &l_158;
        for (g_129 = (-22); (g_129 == 8); ++g_129)
        { /* block id: 43 */
            for (g_48 = 0; (g_48 > 6); ++g_48)
            { /* block id: 46 */
                (*g_156) = &g_65[0];
            }
        }
        (*l_160) = l_158;
    }
    if ((p_82 < ((*l_161) = ((void*)0 == &l_159[0]))))
    { /* block id: 53 */
        int32_t *l_163 = (void*)0;
        int32_t l_164[1];
        int32_t *l_165 = &g_143;
        int32_t *l_166 = &l_164[0];
        int32_t *l_167 = (void*)0;
        int32_t *l_168 = &g_65[4];
        int32_t *l_169 = &g_65[3];
        int32_t *l_170 = &g_143;
        int32_t *l_171 = &g_143;
        int32_t *l_172 = (void*)0;
        int32_t *l_173[1][9] = {{&l_164[0],&l_164[0],&l_164[0],&l_164[0],&l_164[0],&l_164[0],&l_164[0],&l_164[0],&l_164[0]}};
        int16_t l_180 = (-1L);
        float l_181[5][6] = {{0x6.4p+1,0x1.Ap-1,0x8.AAE12Bp-48,0xA.C9DC74p+63,0xA.F3C820p-32,0x6.4p+1},{0x0.3p-1,0x1.Ap-1,0xA.C9DC74p+63,0x0.3p-1,0x2.5314CBp-21,0x0.3p-1},{0x0.3p-1,0x2.5314CBp-21,0x0.3p-1,0xA.C9DC74p+63,0x1.Ap-1,0x0.3p-1},{0x6.4p+1,0xA.F3C820p-32,0xA.C9DC74p+63,0x8.AAE12Bp-48,0x1.Ap-1,0x6.4p+1},{0xA.C9DC74p+63,0x2.5314CBp-21,0x8.AAE12Bp-48,0x8.AAE12Bp-48,0x2.5314CBp-21,0xA.C9DC74p+63}};
        uint16_t l_183[2][4] = {{65527UL,0x460DL,65527UL,65527UL},{0x460DL,0x460DL,0xBA44L,0x460DL}};
        int i, j;
        for (i = 0; i < 1; i++)
            l_164[i] = 0L;
        ++l_183[0][1];
    }
    else
    { /* block id: 55 */
        int64_t *l_186 = (void*)0;
        int64_t *l_187 = &l_182[2];
        g_189 = ((p_82 != ((*l_187) |= (p_82 ^ 0L))) , g_188);
    }
    for (l_177 = (-20); (l_177 >= 23); l_177 = safe_add_func_int32_t_s_s(l_177, 7))
    { /* block id: 61 */
        uint32_t *l_205 = &g_146[0];
        int32_t l_206 = 4L;
        int32_t *l_207 = &g_143;
        if (((*l_207) = (0x91BFD640L & (((p_80 > ((*l_205) = ((&l_158 == (void*)0) < ((safe_rshift_func_uint8_t_u_s(((safe_add_func_int64_t_s_s((safe_sub_func_uint64_t_u_u((((safe_rshift_func_uint16_t_u_u(l_178, (((safe_unary_minus_func_int32_t_s((l_179[4][1] ^= (((safe_add_func_uint32_t_u_u((g_189.f2 || (g_111 | (0UL >= ((((safe_lshift_func_uint16_t_u_u(((*l_161) = p_82), 6)) || g_57) || g_189.f1) , p_82)))), 4294967293UL)) != l_177) , p_82)))) == l_174) > l_176))) , l_179[4][1]) > g_98), g_65[3])), p_80)) | 0x253F6F1915A3E27ELL), 6)) , g_188.f0)))) < l_206) | (-8L)))))
        { /* block id: 66 */
            int32_t l_208[4][6][6] = {{{1L,0x8DD240B2L,1L,0x1243610AL,0x1EE1E09EL,0x2D01FADEL},{(-6L),0x2D01FADEL,0x3B8A34B8L,1L,(-1L),(-5L)},{(-1L),0x7D3C9431L,(-1L),7L,(-5L),(-5L)},{0x22C1D3B6L,0x3B8A34B8L,0x3B8A34B8L,0x22C1D3B6L,0x41646E61L,0x2D01FADEL},{(-1L),0L,1L,(-8L),0x8DD240B2L,(-1L)},{0xF83EBBB2L,(-1L),(-1L),0xBA6A255FL,0x8DD240B2L,0x7D3C9431L}},{{1L,0L,0L,0xBEE1F3E7L,0x41646E61L,(-1L)},{7L,0x3B8A34B8L,0x1EE1E09EL,(-2L),(-5L),0x41646E61L},{(-8L),0x7D3C9431L,0x869ADC50L,(-2L),(-1L),0x869ADC50L},{7L,0x2D01FADEL,0L,0xBEE1F3E7L,0x1EE1E09EL,0L},{1L,0x8DD240B2L,0x2D01FADEL,0xBA6A255FL,0x3B8A34B8L,0L},{0xF83EBBB2L,0L,0x2D01FADEL,(-8L),0x2D01FADEL,0L}},{{(-1L),0L,0L,0x22C1D3B6L,0L,0x869ADC50L},{0x22C1D3B6L,0L,0x869ADC50L,7L,(-1L),0x41646E61L},{(-1L),0L,0x1EE1E09EL,1L,0L,(-1L)},{(-6L),0L,0L,0x1243610AL,0x2D01FADEL,0x7D3C9431L},{1L,0L,(-1L),0xF83EBBB2L,0x3B8A34B8L,(-1L)},{1L,0x8DD240B2L,0x3C1BFE7BL,0x2D01FADEL,(-1L),(-9L)}},{{0x3B8A34B8L,(-9L),1L,0L,0x37AEE301L,0xD82929E3L},{0x8DD240B2L,0xD2A3B7E9L,0x37AEE301L,1L,0xD82929E3L,0xD82929E3L},{(-5L),1L,1L,(-5L),0xF88B38F8L,(-9L)},{0x869ADC50L,0xF7DB8A07L,0x3C1BFE7BL,0xC97EF018L,0L,0x37AEE301L},{0L,5L,0x81482DDDL,(-1L),0L,0xD2A3B7E9L},{0x41646E61L,0xF7DB8A07L,(-5L),0L,0xF88B38F8L,5L}}};
            int64_t *l_235 = &l_182[0];
            int8_t *l_238 = &g_7;
            int16_t **l_265 = (void*)0;
            int32_t *l_273 = &l_179[1][7];
            int i, j, k;
            if (l_179[4][1])
                break;
            if (p_80)
                continue;
            if (p_80)
            { /* block id: 69 */
                return l_208[2][5][0];
            }
            else
            { /* block id: 71 */
                uint64_t l_215 = 0xC5C242C3BEC00692LL;
                float l_224[3][1];
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_224[i][j] = 0x6.53BD56p+44;
                }
                if (p_82)
                { /* block id: 72 */
                    float l_221 = 0xD.C45B85p-30;
                    if (((1L >= (safe_lshift_func_int16_t_s_s(8L, 0))) >= ((0xC7C72AEE829FB855LL == (safe_sub_func_int8_t_s_s((safe_sub_func_int8_t_s_s(0x36L, (*p_81))), ((*p_81) < l_215)))) , ((++(*l_205)) != ((-1L) && 0L)))))
                    { /* block id: 74 */
                        if (p_80)
                            break;
                    }
                    else
                    { /* block id: 76 */
                        int32_t l_218 = 0L;
                        (*l_207) = l_218;
                    }
                    for (g_129 = (-9); (g_129 == (-16)); g_129 = safe_sub_func_int32_t_s_s(g_129, 3))
                    { /* block id: 81 */
                        return (*l_207);
                    }
                    (*l_207) &= p_82;
                }
                else
                { /* block id: 85 */
                    uint32_t l_228 = 0xD5C75A84L;
                    for (g_162 = 0; (g_162 >= 24); g_162++)
                    { /* block id: 88 */
                        int32_t *l_225 = (void*)0;
                        int32_t *l_226 = &l_179[2][6];
                        int32_t *l_227[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int64_t **l_236 = &l_235;
                        int8_t *l_240 = &g_7;
                        int8_t **l_239 = &l_240;
                        int8_t l_259 = (-9L);
                        int i;
                        --l_228;
                        (*l_207) = (safe_div_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((((((*l_236) = l_235) == g_237) , l_238) == ((*l_239) = &g_7)), ((((l_215 < 0x048FL) , ((((0UL & ((safe_mul_func_int8_t_s_s(((safe_add_func_int64_t_s_s(l_228, (safe_mul_func_uint8_t_u_u(((((safe_div_func_int32_t_s_s(((safe_rshift_func_uint16_t_u_u(((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u((((safe_div_func_int8_t_s_s((((safe_rshift_func_int16_t_s_s(((((void*)0 != &g_7) >= 0xEE9B8D37L) || l_215), 3)) || g_188.f3) || l_175), 8UL)) , l_208[2][5][0]) & p_80), 12)) != 0x556C42D9L), p_82)) , 0xA8D9L), p_82)) < p_80), p_82)) , l_175) ^ 0xEAB52121716B6ADDLL) | l_259), 0xAFL)))) , (*p_81)), 0xD6L)) && 1L)) & g_188.f2) != g_7) && 0L)) <= 0x66006056L) | 0x9888L))), l_215));
                    }
                    for (g_111 = 0; (g_111 < 17); g_111++)
                    { /* block id: 96 */
                        return p_82;
                    }
                }
            }
            for (p_80 = 0; (p_80 < 47); p_80 = safe_add_func_int16_t_s_s(p_80, 5))
            { /* block id: 103 */
                int32_t l_264 = 0L;
                int8_t **l_269 = (void*)0;
                int8_t **l_270 = &l_238;
                int32_t ***l_272[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_272[i] = &l_271;
                if (l_264)
                    break;
                (*l_207) = ((&g_45[6][3] == (g_266 = l_265)) | ((g_268 , (&g_7 != ((*l_270) = p_81))) , ((((l_271 = l_271) == &g_110) <= l_208[1][0][3]) > (((g_188 , g_46) < 0xC9CF4CB1A89167F1LL) > p_80))));
                for (g_143 = 0; (g_143 <= 1); g_143 += 1)
                { /* block id: 111 */
                    int32_t **l_275 = (void*)0;
                    int i, j;
                    (*g_276) = l_273;
                    return g_141[g_143][g_143];
                }
            }
        }
        else
        { /* block id: 116 */
            uint64_t l_280 = 0xCC6AA533A6A4DE43LL;
            int64_t l_296 = (-1L);
            float *l_298 = &g_56;
            (*l_298) = ((safe_mul_func_float_f_f((+l_280), (((safe_div_func_float_f_f((safe_sub_func_float_f_f((p_80 , ((((safe_div_func_float_f_f((!(*l_207)), (safe_sub_func_float_f_f((safe_mul_func_float_f_f(g_98, (safe_add_func_float_f_f(((g_237 != &l_182[1]) > (((((((safe_lshift_func_int8_t_s_u((0xF6BFL ^ p_80), 2)) , p_80) < p_80) <= 0x1.Dp-1) >= l_296) < l_177) >= (*l_207))), 0x5.1p+1)))), (*l_207))))) < g_189.f1) <= g_189.f1) <= 0x0.2p+1)), 0x4.0539E5p-48)), l_177)) >= (*l_207)) == g_98))) != g_146[0]);
            (*g_156) = &l_178;
        }
        (*g_299) = (*g_156);
    }
    return l_174;
}


/* ------------------------------------------ */
/* 
 * reads : g_109 g_6 g_7 g_129 g_141 g_46 g_111 g_143 g_146 g_57 g_48
 * writes: g_65 g_129 g_141 g_143 g_46 g_48
 */
static int16_t * func_99(int32_t  p_100, int64_t  p_101, uint16_t  p_102, int64_t  p_103)
{ /* block id: 19 */
    int32_t *l_104 = &g_65[3];
    int32_t **l_105 = &l_104;
    int32_t *l_108 = (void*)0;
    int32_t * volatile * volatile l_113 = (void*)0;/* VOLATILE GLOBAL l_113 */
    int32_t *l_123 = &g_65[3];
    int8_t *l_138 = &g_7;
    int32_t l_139[7] = {0L,(-6L),0L,0L,(-6L),0L,0L};
    uint8_t *l_140[6][2] = {{&g_141[0][1],&g_141[0][1]},{&g_141[0][1],&g_141[0][1]},{&g_141[0][1],&g_141[0][1]},{&g_141[0][1],&g_141[0][1]},{&g_141[0][1],&g_141[0][1]},{&g_141[0][1],&g_141[0][1]}};
    int16_t *l_144 = (void*)0;
    int16_t *l_145 = &g_46;
    int8_t l_147[10][6][4] = {{{5L,0xA2L,0x3CL,(-8L)},{(-2L),2L,(-8L),0L},{(-8L),0L,0L,0L},{(-7L),2L,9L,(-8L)},{0x14L,0xA2L,0L,0x0FL},{9L,(-7L),(-1L),(-1L)}},{{9L,9L,0L,5L},{0x14L,(-1L),9L,0xA2L},{(-7L),(-2L),0L,9L},{(-8L),(-2L),(-8L),0xA2L},{(-2L),(-1L),0x3CL,5L},{5L,9L,(-7L),(-1L)}},{{0x0FL,(-7L),(-7L),0x0FL},{5L,0xA2L,0x3CL,(-8L)},{(-2L),2L,(-8L),0L},{(-8L),0L,0L,0L},{(-7L),2L,9L,(-8L)},{0x14L,0xA2L,0L,0x0FL}},{{9L,(-7L),(-1L),(-1L)},{9L,9L,0L,5L},{0x14L,(-1L),9L,0xA2L},{(-7L),(-2L),0L,9L},{(-8L),(-2L),(-8L),0xA2L},{(-2L),(-1L),0x3CL,5L}},{{5L,9L,(-7L),(-1L)},{0x0FL,(-7L),(-7L),0x0FL},{5L,0xA2L,0x3CL,(-8L)},{(-2L),2L,(-8L),0L},{(-8L),0L,0L,0L},{(-7L),2L,9L,(-8L)}},{{0x14L,0xA2L,(-2L),9L},{0L,0x0FL,(-7L),(-7L)},{0L,0L,(-2L),0xA2L},{(-8L),(-7L),0L,2L},{0x0FL,5L,0x14L,0L},{(-1L),5L,(-1L),2L}},{{5L,(-7L),0L,0xA2L},{0xA2L,0L,0x0FL,(-7L)},{9L,0x0FL,0x0FL,9L},{0xA2L,2L,0L,(-1L)},{5L,0x3CL,(-1L),(-2L)},{(-1L),(-2L),0x14L,(-2L)}},{{0x0FL,0x3CL,0L,(-1L)},{(-8L),2L,(-2L),9L},{0L,0x0FL,(-7L),(-7L)},{0L,0L,(-2L),0xA2L},{(-8L),(-7L),0L,2L},{0x0FL,5L,0x14L,0L}},{{(-1L),5L,(-1L),2L},{5L,(-7L),0L,0xA2L},{0xA2L,0L,0x0FL,(-7L)},{9L,0x0FL,0x0FL,9L},{0xA2L,2L,0L,(-1L)},{5L,0x3CL,(-1L),(-2L)}},{{(-1L),(-2L),0x14L,(-2L)},{0x0FL,0x3CL,0L,(-1L)},{(-8L),2L,(-2L),9L},{0L,0x0FL,(-7L),(-7L)},{0L,0L,(-2L),0xA2L},{(-8L),(-7L),0L,2L}}};
    int32_t *l_148 = &g_48;
    int i, j, k;
    (*l_105) = l_104;
    for (p_101 = 0; (p_101 > 26); p_101++)
    { /* block id: 23 */
        int32_t * volatile * volatile *l_112 = (void*)0;
        int32_t *l_114 = (void*)0;
        (*l_105) = l_108;
        l_113 = g_109[0][0];
        (*l_105) = l_114;
    }
    (*l_148) |= (safe_rshift_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u(((safe_mod_func_uint64_t_u_u((((safe_mul_func_int16_t_s_s((((*g_6) || ((((((~((*l_123) = p_100)) == ((safe_mul_func_uint16_t_u_u((((*l_145) = (p_100 && ((0xD71AF1E100AA352ELL <= ((safe_unary_minus_func_int64_t_s((g_129 ^= p_100))) >= (safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s(((((&g_110 == ((g_143 |= (((safe_lshift_func_uint16_t_u_u(((((6UL ^ ((g_141[0][1] &= (l_139[5] |= (safe_sub_func_uint32_t_u_u((0x9027L && (l_138 != l_138)), 4UL)))) , g_141[0][1])) , 0x61A385A4L) > g_46) != 0xF4DBL), 8)) && g_141[1][0]) | g_111)) , (void*)0)) == p_101) > p_102) && g_111), 250UL)), 0x194EL)))) ^ p_101))) ^ g_146[0]), 1UL)) || 4UL)) | 0xCB73A7ADL) ^ 0L) ^ g_57) && g_146[0])) && p_101), 0x8BB0L)) ^ l_147[2][3][3]) || 0x39AE860EE8D33E78LL), p_103)) , g_141[0][1]))), p_100));
    (*l_105) = &g_65[3];
    return &g_46;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_7, "g_7", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_15[i][j], "g_15[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_46, "g_46", print_hash_value);
    transparent_crc(g_48, "g_48", print_hash_value);
    transparent_crc_bytes (&g_56, sizeof(g_56), "g_56", print_hash_value);
    transparent_crc(g_57, "g_57", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_65[i], "g_65[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_98, "g_98", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_141[i][j], "g_141[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_142, sizeof(g_142), "g_142", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_146[i], "g_146[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc(g_188.f0, "g_188.f0", print_hash_value);
    transparent_crc(g_188.f1, "g_188.f1", print_hash_value);
    transparent_crc(g_188.f2, "g_188.f2", print_hash_value);
    transparent_crc(g_188.f3, "g_188.f3", print_hash_value);
    transparent_crc(g_189.f0, "g_189.f0", print_hash_value);
    transparent_crc(g_189.f1, "g_189.f1", print_hash_value);
    transparent_crc(g_189.f2, "g_189.f2", print_hash_value);
    transparent_crc(g_189.f3, "g_189.f3", print_hash_value);
    transparent_crc(g_268, "g_268", print_hash_value);
    transparent_crc(g_300.f0, "g_300.f0", print_hash_value);
    transparent_crc(g_300.f1, "g_300.f1", print_hash_value);
    transparent_crc(g_300.f2, "g_300.f2", print_hash_value);
    transparent_crc(g_300.f3, "g_300.f3", print_hash_value);
    transparent_crc(g_351, "g_351", print_hash_value);
    transparent_crc(g_353, "g_353", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_376[i].f0, "g_376[i].f0", print_hash_value);
        transparent_crc(g_376[i].f1, "g_376[i].f1", print_hash_value);
        transparent_crc(g_376[i].f2, "g_376[i].f2", print_hash_value);
        transparent_crc(g_376[i].f3, "g_376[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_382.f0, "g_382.f0", print_hash_value);
    transparent_crc(g_382.f1, "g_382.f1", print_hash_value);
    transparent_crc(g_382.f2, "g_382.f2", print_hash_value);
    transparent_crc(g_382.f3, "g_382.f3", print_hash_value);
    transparent_crc(g_431.f0, "g_431.f0", print_hash_value);
    transparent_crc(g_431.f1, "g_431.f1", print_hash_value);
    transparent_crc(g_431.f2, "g_431.f2", print_hash_value);
    transparent_crc(g_431.f3, "g_431.f3", print_hash_value);
    transparent_crc(g_480, "g_480", print_hash_value);
    transparent_crc(g_487, "g_487", print_hash_value);
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc(g_604, "g_604", print_hash_value);
    transparent_crc(g_625.f0, "g_625.f0", print_hash_value);
    transparent_crc(g_625.f1, "g_625.f1", print_hash_value);
    transparent_crc(g_625.f2, "g_625.f2", print_hash_value);
    transparent_crc(g_625.f3, "g_625.f3", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_730[i].f0, "g_730[i].f0", print_hash_value);
        transparent_crc(g_730[i].f1, "g_730[i].f1", print_hash_value);
        transparent_crc(g_730[i].f2, "g_730[i].f2", print_hash_value);
        transparent_crc(g_730[i].f3, "g_730[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_753[i], "g_753[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_758.f0, "g_758.f0", print_hash_value);
    transparent_crc(g_758.f1, "g_758.f1", print_hash_value);
    transparent_crc(g_758.f2, "g_758.f2", print_hash_value);
    transparent_crc(g_758.f3, "g_758.f3", print_hash_value);
    transparent_crc(g_776, "g_776", print_hash_value);
    transparent_crc(g_809.f0, "g_809.f0", print_hash_value);
    transparent_crc(g_809.f1, "g_809.f1", print_hash_value);
    transparent_crc(g_809.f2, "g_809.f2", print_hash_value);
    transparent_crc(g_809.f3, "g_809.f3", print_hash_value);
    transparent_crc(g_851.f0, "g_851.f0", print_hash_value);
    transparent_crc(g_851.f1, "g_851.f1", print_hash_value);
    transparent_crc(g_851.f2, "g_851.f2", print_hash_value);
    transparent_crc(g_851.f3, "g_851.f3", print_hash_value);
    transparent_crc(g_898.f0, "g_898.f0", print_hash_value);
    transparent_crc(g_898.f1, "g_898.f1", print_hash_value);
    transparent_crc(g_898.f2, "g_898.f2", print_hash_value);
    transparent_crc(g_898.f3, "g_898.f3", print_hash_value);
    transparent_crc(g_899.f0, "g_899.f0", print_hash_value);
    transparent_crc(g_899.f1, "g_899.f1", print_hash_value);
    transparent_crc(g_899.f2, "g_899.f2", print_hash_value);
    transparent_crc(g_899.f3, "g_899.f3", print_hash_value);
    transparent_crc(g_920, "g_920", print_hash_value);
    transparent_crc(g_930, "g_930", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_940[i][j][k].f0, "g_940[i][j][k].f0", print_hash_value);
                transparent_crc(g_940[i][j][k].f1, "g_940[i][j][k].f1", print_hash_value);
                transparent_crc(g_940[i][j][k].f2, "g_940[i][j][k].f2", print_hash_value);
                transparent_crc(g_940[i][j][k].f3, "g_940[i][j][k].f3", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_970, "g_970", print_hash_value);
    transparent_crc(g_983, "g_983", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1024[i].f0, "g_1024[i].f0", print_hash_value);
        transparent_crc(g_1024[i].f1, "g_1024[i].f1", print_hash_value);
        transparent_crc(g_1024[i].f2, "g_1024[i].f2", print_hash_value);
        transparent_crc(g_1024[i].f3, "g_1024[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1037[i].f0, "g_1037[i].f0", print_hash_value);
        transparent_crc(g_1037[i].f1, "g_1037[i].f1", print_hash_value);
        transparent_crc(g_1037[i].f2, "g_1037[i].f2", print_hash_value);
        transparent_crc(g_1037[i].f3, "g_1037[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1246.f0, "g_1246.f0", print_hash_value);
    transparent_crc(g_1246.f1, "g_1246.f1", print_hash_value);
    transparent_crc(g_1246.f2, "g_1246.f2", print_hash_value);
    transparent_crc(g_1246.f3, "g_1246.f3", print_hash_value);
    transparent_crc(g_1283, "g_1283", print_hash_value);
    transparent_crc(g_1330, "g_1330", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1377[i][j].f0, "g_1377[i][j].f0", print_hash_value);
            transparent_crc(g_1377[i][j].f1, "g_1377[i][j].f1", print_hash_value);
            transparent_crc(g_1377[i][j].f2, "g_1377[i][j].f2", print_hash_value);
            transparent_crc(g_1377[i][j].f3, "g_1377[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1544, "g_1544", print_hash_value);
    transparent_crc(g_1550.f0, "g_1550.f0", print_hash_value);
    transparent_crc(g_1550.f1, "g_1550.f1", print_hash_value);
    transparent_crc(g_1550.f2, "g_1550.f2", print_hash_value);
    transparent_crc(g_1550.f3, "g_1550.f3", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1593[i][j][k], "g_1593[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1618, "g_1618", print_hash_value);
    transparent_crc(g_1624, "g_1624", print_hash_value);
    transparent_crc(g_1639, "g_1639", print_hash_value);
    transparent_crc(g_1673, "g_1673", print_hash_value);
    transparent_crc(g_1674.f0, "g_1674.f0", print_hash_value);
    transparent_crc(g_1674.f1, "g_1674.f1", print_hash_value);
    transparent_crc(g_1674.f2, "g_1674.f2", print_hash_value);
    transparent_crc(g_1674.f3, "g_1674.f3", print_hash_value);
    transparent_crc(g_1710.f0, "g_1710.f0", print_hash_value);
    transparent_crc(g_1710.f1, "g_1710.f1", print_hash_value);
    transparent_crc(g_1710.f2, "g_1710.f2", print_hash_value);
    transparent_crc(g_1710.f3, "g_1710.f3", print_hash_value);
    transparent_crc(g_1711.f0, "g_1711.f0", print_hash_value);
    transparent_crc(g_1711.f1, "g_1711.f1", print_hash_value);
    transparent_crc(g_1711.f2, "g_1711.f2", print_hash_value);
    transparent_crc(g_1711.f3, "g_1711.f3", print_hash_value);
    transparent_crc(g_1712.f0, "g_1712.f0", print_hash_value);
    transparent_crc(g_1712.f1, "g_1712.f1", print_hash_value);
    transparent_crc(g_1712.f2, "g_1712.f2", print_hash_value);
    transparent_crc(g_1712.f3, "g_1712.f3", print_hash_value);
    transparent_crc(g_1714.f0, "g_1714.f0", print_hash_value);
    transparent_crc(g_1714.f1, "g_1714.f1", print_hash_value);
    transparent_crc(g_1714.f2, "g_1714.f2", print_hash_value);
    transparent_crc(g_1714.f3, "g_1714.f3", print_hash_value);
    transparent_crc(g_1715.f0, "g_1715.f0", print_hash_value);
    transparent_crc(g_1715.f1, "g_1715.f1", print_hash_value);
    transparent_crc(g_1715.f2, "g_1715.f2", print_hash_value);
    transparent_crc(g_1715.f3, "g_1715.f3", print_hash_value);
    transparent_crc(g_1716.f0, "g_1716.f0", print_hash_value);
    transparent_crc(g_1716.f1, "g_1716.f1", print_hash_value);
    transparent_crc(g_1716.f2, "g_1716.f2", print_hash_value);
    transparent_crc(g_1716.f3, "g_1716.f3", print_hash_value);
    transparent_crc(g_1791, "g_1791", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1819[i][j].f0, "g_1819[i][j].f0", print_hash_value);
            transparent_crc(g_1819[i][j].f1, "g_1819[i][j].f1", print_hash_value);
            transparent_crc(g_1819[i][j].f2, "g_1819[i][j].f2", print_hash_value);
            transparent_crc(g_1819[i][j].f3, "g_1819[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1861, sizeof(g_1861), "g_1861", print_hash_value);
    transparent_crc(g_1876.f0, "g_1876.f0", print_hash_value);
    transparent_crc(g_1876.f1, "g_1876.f1", print_hash_value);
    transparent_crc(g_1876.f2, "g_1876.f2", print_hash_value);
    transparent_crc(g_1876.f3, "g_1876.f3", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1978[i][j][k], "g_1978[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1982, "g_1982", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1988[i].f0, "g_1988[i].f0", print_hash_value);
        transparent_crc(g_1988[i].f1, "g_1988[i].f1", print_hash_value);
        transparent_crc(g_1988[i].f2, "g_1988[i].f2", print_hash_value);
        transparent_crc(g_1988[i].f3, "g_1988[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2033, "g_2033", print_hash_value);
    transparent_crc(g_2068, "g_2068", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2100[i][j].f0, "g_2100[i][j].f0", print_hash_value);
            transparent_crc(g_2100[i][j].f1, "g_2100[i][j].f1", print_hash_value);
            transparent_crc(g_2100[i][j].f2, "g_2100[i][j].f2", print_hash_value);
            transparent_crc(g_2100[i][j].f3, "g_2100[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2158.f0, "g_2158.f0", print_hash_value);
    transparent_crc(g_2158.f1, "g_2158.f1", print_hash_value);
    transparent_crc(g_2158.f2, "g_2158.f2", print_hash_value);
    transparent_crc(g_2158.f3, "g_2158.f3", print_hash_value);
    transparent_crc(g_2191.f0, "g_2191.f0", print_hash_value);
    transparent_crc(g_2191.f1, "g_2191.f1", print_hash_value);
    transparent_crc(g_2191.f2, "g_2191.f2", print_hash_value);
    transparent_crc(g_2191.f3, "g_2191.f3", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_2230[i][j][k], "g_2230[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2238.f0, "g_2238.f0", print_hash_value);
    transparent_crc(g_2238.f1, "g_2238.f1", print_hash_value);
    transparent_crc(g_2238.f2, "g_2238.f2", print_hash_value);
    transparent_crc(g_2238.f3, "g_2238.f3", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 597
   depth: 1, occurrence: 32
XXX total union variables: 12

XXX non-zero bitfields defined in structs: 4
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 38
breakdown:
   indirect level: 0, occurrence: 32
   indirect level: 1, occurrence: 4
   indirect level: 2, occurrence: 2
XXX full-bitfields structs in the program: 32
breakdown:
   indirect level: 0, occurrence: 32
XXX times a bitfields struct's address is taken: 76
XXX times a bitfields struct on LHS: 8
XXX times a bitfields struct on RHS: 30
XXX times a single bitfield on LHS: 2
XXX times a single bitfield on RHS: 76

XXX max expression depth: 54
breakdown:
   depth: 1, occurrence: 222
   depth: 2, occurrence: 52
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 12, occurrence: 2
   depth: 14, occurrence: 2
   depth: 15, occurrence: 3
   depth: 17, occurrence: 3
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 3
   depth: 21, occurrence: 2
   depth: 22, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 28, occurrence: 1
   depth: 31, occurrence: 2
   depth: 33, occurrence: 1
   depth: 40, occurrence: 1
   depth: 43, occurrence: 1
   depth: 45, occurrence: 1
   depth: 54, occurrence: 1

XXX total number of pointers: 491

XXX times a variable address is taken: 1064
XXX times a pointer is dereferenced on RHS: 184
breakdown:
   depth: 1, occurrence: 149
   depth: 2, occurrence: 22
   depth: 3, occurrence: 13
XXX times a pointer is dereferenced on LHS: 235
breakdown:
   depth: 1, occurrence: 219
   depth: 2, occurrence: 14
   depth: 3, occurrence: 2
XXX times a pointer is compared with null: 30
XXX times a pointer is compared with address of another variable: 8
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 8256

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1070
   level: 2, occurrence: 229
   level: 3, occurrence: 102
   level: 4, occurrence: 9
   level: 5, occurrence: 3
XXX number of pointers point to pointers: 184
XXX number of pointers point to scalars: 291
XXX number of pointers point to structs: 10
XXX percent of pointers has null in alias set: 31.4
XXX average alias set size: 1.37

XXX times a non-volatile is read: 1512
XXX times a non-volatile is write: 760
XXX times a volatile is read: 96
XXX    times read thru a pointer: 9
XXX times a volatile is write: 47
XXX    times written thru a pointer: 5
XXX times a volatile is available for access: 6.9e+03
XXX percentage of non-volatile access: 94.1

XXX forward jumps: 0
XXX backward jumps: 8

XXX stmts: 216
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 26
   depth: 1, occurrence: 24
   depth: 2, occurrence: 25
   depth: 3, occurrence: 34
   depth: 4, occurrence: 51
   depth: 5, occurrence: 56

XXX percentage a fresh-made variable is used: 16.4
XXX percentage an existing variable is used: 83.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3803133741
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int32_t  f0;
   int32_t  f1;
   volatile int32_t  f2;
   int64_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2[3][1] = {{0x0BF34FDAL},{0x0BF34FDAL},{0x0BF34FDAL}};
static int8_t g_12 = 0xA3L;
static uint8_t g_60 = 6UL;
static int32_t g_75 = 0L;
static int8_t g_78 = (-2L);
static uint64_t g_81 = 0x30BEC3A435FA7DC0LL;
static int64_t g_108 = 1L;
static int32_t g_135 = (-1L);
static uint8_t *g_145 = &g_60;
static uint8_t **g_144 = &g_145;
static float g_151 = 0x0.DC95FAp-10;
static uint32_t g_163 = 0xD4B72C14L;
static int32_t *g_181 = &g_135;
static int32_t **g_180[7] = {&g_181,&g_181,&g_181,&g_181,&g_181,&g_181,&g_181};
static int32_t *** volatile g_203 = &g_180[0];/* VOLATILE GLOBAL g_203 */
static int32_t g_237 = 0xFBA13450L;
static int32_t g_239[1] = {(-3L)};
static int16_t g_254 = 4L;
static uint8_t ***g_258 = &g_144;
static volatile uint16_t *g_271 = (void*)0;
static volatile int16_t g_283[4][3][2] = {{{0x1A3DL,0x51E4L},{0xAD14L,0x51E4L},{0x1A3DL,0x51E4L}},{{0xAD14L,0x51E4L},{0x1A3DL,0x51E4L},{0xAD14L,0x51E4L}},{{0x1A3DL,0x51E4L},{0xAD14L,0x51E4L},{0x1A3DL,0x51E4L}},{{0xAD14L,0x51E4L},{0x1A3DL,0x51E4L},{0xAD14L,0x51E4L}}};
static const union U0 g_312 = {-5L};/* VOLATILE GLOBAL g_312 */
static volatile union U0 g_313[9] = {{0L},{0L},{0L},{0L},{0L},{0L},{0L},{0L},{0L}};
static volatile int32_t g_328 = 1L;/* VOLATILE GLOBAL g_328 */
static uint8_t g_332 = 0x6FL;
static int32_t ** volatile g_356[1] = {&g_181};
static int32_t ** volatile g_357 = &g_181;/* VOLATILE GLOBAL g_357 */
static int8_t *g_397[2][8][10] = {{{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78}},{{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78},{&g_78,&g_78,(void*)0,(void*)0,&g_78,&g_78,&g_78,(void*)0,(void*)0,&g_78}}};
static int8_t * volatile *g_396 = &g_397[0][6][7];
static int32_t ***g_477 = (void*)0;
static volatile float g_493 = 0x7.Cp-1;/* VOLATILE GLOBAL g_493 */
static uint64_t g_495 = 18446744073709551615UL;
static int64_t g_535 = 0x59E1873707056FC0LL;
static float g_549 = (-0x6.2p-1);
static float * volatile g_548 = &g_549;/* VOLATILE GLOBAL g_548 */
static union U0 g_554 = {1L};/* VOLATILE GLOBAL g_554 */
static int64_t *g_561[5][3] = {{&g_535,&g_535,&g_108},{&g_108,&g_535,&g_108},{&g_108,&g_535,&g_108},{&g_108,&g_108,&g_535},{&g_108,&g_535,&g_535}};
static uint32_t g_562 = 0xB7A2FF9EL;
static uint16_t g_582 = 9UL;
static int16_t *g_633 = &g_254;
static union U0 g_680 = {0x22C1AF23L};/* VOLATILE GLOBAL g_680 */
static volatile uint32_t g_681 = 0xA91434B8L;/* VOLATILE GLOBAL g_681 */
static float g_794 = 0x4.946A3Dp+31;
static uint16_t *g_800 = &g_582;
static uint16_t **g_799 = &g_800;
static uint16_t *** volatile g_798 = &g_799;/* VOLATILE GLOBAL g_798 */
static union U0 g_895[1] = {{0x896F58A1L}};
static float *g_911 = &g_794;
static int32_t g_933 = 0x4B044176L;
static int32_t ** volatile g_935[8][10][3] = {{{&g_181,(void*)0,&g_181},{&g_181,(void*)0,&g_181},{(void*)0,&g_181,&g_181},{&g_181,(void*)0,&g_181},{&g_181,(void*)0,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181}},{{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{(void*)0,&g_181,&g_181}},{{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181}},{{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{(void*)0,&g_181,&g_181},{(void*)0,&g_181,&g_181},{&g_181,&g_181,&g_181},{&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181}}};
static volatile int8_t g_980 = (-1L);/* VOLATILE GLOBAL g_980 */
static int32_t * volatile g_1025 = (void*)0;/* VOLATILE GLOBAL g_1025 */
static int32_t * volatile g_1026 = &g_933;/* VOLATILE GLOBAL g_1026 */
static volatile union U0 g_1029 = {-9L};/* VOLATILE GLOBAL g_1029 */
static uint16_t g_1033 = 8UL;
static uint16_t g_1035[1] = {4UL};
static union U0 g_1060 = {9L};/* VOLATILE GLOBAL g_1060 */
static int32_t * volatile g_1084 = (void*)0;/* VOLATILE GLOBAL g_1084 */
static int32_t * volatile g_1086 = &g_135;/* VOLATILE GLOBAL g_1086 */
static union U0 g_1113[3] = {{0xB7BED18CL},{0xB7BED18CL},{0xB7BED18CL}};
static int16_t g_1127 = 0x10E3L;
static int32_t ** volatile g_1150[1] = {&g_181};
static int32_t ** volatile g_1151 = &g_181;/* VOLATILE GLOBAL g_1151 */
static float * volatile g_1163 = &g_549;/* VOLATILE GLOBAL g_1163 */
static int8_t ***g_1206[2] = {(void*)0,(void*)0};
static int8_t **** volatile g_1205 = &g_1206[1];/* VOLATILE GLOBAL g_1205 */
static union U0 *g_1227 = &g_895[0];
static union U0 ** volatile g_1226 = &g_1227;/* VOLATILE GLOBAL g_1226 */
static float * volatile g_1229 = &g_151;/* VOLATILE GLOBAL g_1229 */
static uint64_t g_1261 = 5UL;
static const volatile uint8_t g_1268 = 1UL;/* VOLATILE GLOBAL g_1268 */
static union U0 g_1302 = {-1L};/* VOLATILE GLOBAL g_1302 */
static const int8_t g_1314 = 8L;
static float **g_1344[2][9][2] = {{{&g_911,&g_911},{(void*)0,&g_911},{&g_911,(void*)0},{&g_911,&g_911},{&g_911,(void*)0},{&g_911,&g_911},{(void*)0,&g_911},{&g_911,(void*)0},{&g_911,&g_911}},{{&g_911,(void*)0},{&g_911,&g_911},{(void*)0,&g_911},{&g_911,(void*)0},{&g_911,&g_911},{&g_911,(void*)0},{&g_911,&g_911},{(void*)0,&g_911},{&g_911,(void*)0}}};
static const int8_t g_1351 = 0x64L;
static const int8_t *g_1350 = &g_1351;
static int32_t * volatile g_1421 = &g_554.f1;/* VOLATILE GLOBAL g_1421 */
static int32_t * volatile g_1442 = &g_933;/* VOLATILE GLOBAL g_1442 */
static volatile uint16_t g_1459 = 4UL;/* VOLATILE GLOBAL g_1459 */
static union U0 g_1465 = {2L};/* VOLATILE GLOBAL g_1465 */
static int32_t ** volatile g_1569 = &g_181;/* VOLATILE GLOBAL g_1569 */
static uint8_t * const *g_1584 = (void*)0;
static uint8_t * const **g_1583 = &g_1584;
static int32_t * const *g_1592 = (void*)0;
static int32_t * const * volatile *g_1591 = &g_1592;
static int32_t * const * volatile ** volatile g_1590 = &g_1591;/* VOLATILE GLOBAL g_1590 */
static int32_t * const * volatile ** volatile *g_1589 = &g_1590;
static const volatile uint8_t g_1631 = 0x72L;/* VOLATILE GLOBAL g_1631 */
static int8_t * volatile **g_1722 = &g_396;
static int64_t **g_1747 = (void*)0;
static int64_t *** const  volatile g_1746 = &g_1747;/* VOLATILE GLOBAL g_1746 */
static volatile union U0 g_1901 = {0x38269781L};/* VOLATILE GLOBAL g_1901 */
static volatile union U0 g_1913 = {-1L};/* VOLATILE GLOBAL g_1913 */
static uint32_t g_1922 = 0x6EF92EDFL;
static int16_t ***g_1925 = (void*)0;
static union U0 g_1932 = {0xA1213A06L};/* VOLATILE GLOBAL g_1932 */
static volatile union U0 g_1966 = {0x88CA65FDL};/* VOLATILE GLOBAL g_1966 */
static union U0 g_2068[1] = {{4L}};
static volatile float g_2128 = 0x1.4p-1;/* VOLATILE GLOBAL g_2128 */
static uint8_t * const ***g_2218 = &g_1583;
static int32_t ** volatile g_2294 = &g_181;/* VOLATILE GLOBAL g_2294 */
static int8_t *g_2336 = (void*)0;
static const int32_t *g_2343 = &g_2068[0].f1;
static const int32_t ** volatile g_2342[3][10] = {{&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343},{&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343},{&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343,&g_2343}};
static const int32_t ** volatile g_2344 = &g_2343;/* VOLATILE GLOBAL g_2344 */


/* --- FORWARD DECLARATIONS --- */
static const union U0  func_1(void);
static const int32_t * func_5(uint32_t  p_6, int8_t  p_7, uint32_t  p_8);
static uint16_t  func_14(int32_t * p_15);
static int32_t * func_16(int8_t * p_17, int8_t * p_18);
static int8_t * func_20(const int8_t * p_21);
static int8_t * func_22(int8_t * p_23, const uint32_t  p_24, uint8_t  p_25, uint32_t  p_26);
static uint32_t  func_34(const uint64_t  p_35, int32_t * p_36, int32_t * p_37);
static int32_t * func_40(uint32_t  p_41, const uint32_t  p_42);
static const uint32_t  func_43(int16_t  p_44, int32_t ** p_45, uint16_t  p_46, const int32_t ** p_47);
static int32_t ** func_49(uint8_t  p_50, int32_t * p_51, int16_t  p_52, uint32_t  p_53, uint64_t  p_54);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_237 g_145 g_60 g_554.f1 g_203 g_180 g_1226 g_1227 g_895
 * writes: g_2 g_60 g_554.f1 g_181
 */
static const union U0  func_1(void)
{ /* block id: 0 */
    uint32_t l_10[5][6] = {{0x97A61EEFL,0x896E6A37L,0x7B051AF7L,4294967295UL,0x896E6A37L,4294967295UL},{0x6E5B14F4L,0x97A61EEFL,0x7B051AF7L,0x97A61EEFL,0x6E5B14F4L,1UL},{0xD2038E85L,0x97A61EEFL,4294967295UL,0xD2038E85L,0x896E6A37L,0x6F65E790L},{0xD2038E85L,0x896E6A37L,0x6F65E790L,0x97A61EEFL,0x97A61EEFL,0x6F65E790L},{0x6E5B14F4L,0x6E5B14F4L,4294967295UL,4294967295UL,0x97A61EEFL,1UL}};
    uint32_t l_13[9];
    int32_t **l_2356 = (void*)0;
    int32_t **l_2357[9][10][2] = {{{&g_181,(void*)0},{(void*)0,&g_181},{&g_181,&g_181},{&g_181,&g_181},{(void*)0,(void*)0},{&g_181,(void*)0},{(void*)0,&g_181},{&g_181,&g_181},{&g_181,&g_181},{(void*)0,(void*)0}},{{&g_181,(void*)0},{(void*)0,&g_181},{&g_181,&g_181},{&g_181,&g_181},{(void*)0,(void*)0},{&g_181,(void*)0},{(void*)0,&g_181},{&g_181,&g_181},{&g_181,&g_181},{(void*)0,(void*)0}},{{&g_181,(void*)0},{(void*)0,&g_181},{&g_181,&g_181},{&g_181,&g_181},{(void*)0,(void*)0},{&g_181,(void*)0},{(void*)0,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}},{{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}},{{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}},{{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}},{{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}},{{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}},{{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{(void*)0,&g_181},{&g_181,(void*)0},{&g_181,&g_181},{&g_181,(void*)0},{&g_181,&g_181}}};
    int32_t *l_2369 = &g_554.f0;
    int8_t *l_2373 = &g_12;
    uint16_t l_2374 = 0xC8F2L;
    int8_t l_2376 = 0x87L;
    float l_2377 = 0x6.1p-1;
    uint64_t l_2378 = 18446744073709551615UL;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_13[i] = 5UL;
    for (g_2[1][0] = 0; (g_2[1][0] >= (-29)); g_2[1][0] = safe_sub_func_int64_t_s_s(g_2[1][0], 5))
    { /* block id: 3 */
        float l_9 = (-0x9.Cp-1);
        int8_t *l_11 = &g_12;
        int16_t l_2347 = 0x92A9L;
        int16_t *l_2353 = &g_1127;
        int32_t **l_2354 = &g_181;
        int32_t ***l_2355 = (void*)0;
        uint32_t l_2358 = 18446744073709551615UL;
        int32_t *l_2375 = &g_1302.f1;
    }
    --l_2378;
    (**g_203) = func_16(&l_2376, &l_2376);
    return (**g_1226);
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_60 g_12 g_81 g_75 g_78 g_203 g_180 g_163 g_181 g_135 g_357 g_328 g_258 g_144 g_145 g_312.f2 g_313.f1 g_396 g_239 g_313 g_332 g_495 g_477 g_254 g_108 g_312.f1 g_548 g_554 g_562 g_549 g_582 g_633 g_680 g_681 g_680.f0 g_535 g_680.f1 g_313.f2 g_554.f1 g_798 g_799 g_800 g_237 g_312.f0 g_283 g_397 g_911 g_794 g_933 g_1026 g_1029 g_1033 g_895 g_1060 g_1035 g_1086 g_554.f0 g_1113 g_1151 g_1163 g_1205 g_1226 g_1229 g_895.f0 g_1261 g_151 g_1268 g_1302 g_1314 g_1113.f0 g_1350 g_1569 g_1060.f1 g_1589 g_1631 g_1060.f2 g_895.f1 g_1029.f1 g_1060.f3 g_1901 g_1913 g_1922 g_1925 g_1302.f0 g_1932 g_1966 g_680.f3 g_1966.f0 g_1060.f0 g_1583 g_1584 g_1127 g_1442 g_2068 g_980 g_1459 g_2336 g_1932.f1
 * writes: g_60 g_75 g_78 g_81 g_151 g_135 g_181 g_258 g_12 g_254 g_108 g_239 g_477 g_495 g_332 g_549 g_561 g_163 g_562 g_582 g_633 g_493 g_237 g_799 g_794 g_554.f1 g_535 g_680.f0 g_933 g_1033 g_1035 g_554.f0 g_1206 g_1227 g_1344 g_1350 g_1583 g_554.f3 g_895.f1 g_1060.f3 g_1922 g_1925 g_1302.f0 g_680.f3 g_800 g_1127 g_1261
 */
static const int32_t * func_5(uint32_t  p_6, int8_t  p_7, uint32_t  p_8)
{ /* block id: 5 */
    int8_t *l_19[6];
    int32_t l_29 = 0xC3B7C39CL;
    int32_t *l_39[8][10] = {{(void*)0,&l_29,&g_2[2][0],&g_2[2][0],&l_29,&l_29,&g_2[1][0],&l_29,&g_2[1][0],&l_29},{&l_29,&l_29,&l_29,&l_29,&l_29,&l_29,&g_2[2][0],&l_29,&g_2[1][0],&g_2[2][0]},{(void*)0,&l_29,&l_29,&l_29,&g_2[1][0],&l_29,&l_29,&l_29,(void*)0,&g_2[2][0]},{&g_2[1][0],&l_29,&g_2[2][0],&l_29,&l_29,&l_29,&l_29,&l_29,&l_29,&l_29},{&g_2[1][0],&l_29,&g_2[1][0],&l_29,&l_29,&g_2[2][0],&g_2[2][0],&l_29,(void*)0,&l_29},{&g_2[1][0],&l_29,(void*)0,&l_29,(void*)0,&l_29,&g_2[1][0],&l_29,&g_2[1][0],&l_29},{&g_2[1][0],&l_29,&g_2[1][0],&l_29,&g_2[1][0],&l_29,&g_2[1][0],&l_29,&l_29,&l_29},{&g_2[1][0],&l_29,&l_29,&l_29,(void*)0,&g_2[2][0],&l_29,&l_29,&l_29,&g_2[2][0]}};
    int32_t **l_38 = &l_39[1][5];
    uint16_t l_48 = 0UL;
    uint8_t *l_59 = &g_60;
    const int32_t *l_835 = &g_554.f1;
    const int32_t **l_834 = &l_835;
    const int8_t *l_1349 = &g_1314;
    const int8_t **l_1348[9][5][2] = {{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}},{{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349},{&l_1349,&l_1349}}};
    uint64_t *l_2340 = &g_81;
    const int32_t *l_2341[3];
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_19[i] = &g_12;
    for (i = 0; i < 3; i++)
        l_2341[i] = &g_1060.f1;
    (*l_834) = (((*l_2340) = (func_14(func_16(l_19[1], func_20((g_1350 = func_22(l_19[1], (safe_add_func_int16_t_s_s(8L, l_29)), (safe_add_func_int64_t_s_s((safe_sub_func_uint32_t_u_u(func_34(l_29, ((*l_38) = (void*)0), func_40(g_2[0][0], func_43(l_48, func_49(((*l_59) |= ((safe_rshift_func_int8_t_s_u((safe_mod_func_uint64_t_u_u((0x9757L <= p_8), 0x56979F9F4AEC7014LL)), 1)) ^ 0x9E0CD1AD702C9107LL)), &l_29, g_12, g_2[2][0], p_8), g_554.f1, l_834))), p_7)), p_8)), g_1113[0].f0))))) > p_8)) , (*l_834));
    return l_2341[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_1350 g_12 g_145 g_895.f0 g_535 g_800 g_582 g_332 g_239 g_1569 g_144 g_60 g_1060.f1 g_1589 g_1631 g_78 g_911 g_1060.f2 g_237 g_895.f1 g_1163 g_549 g_794 g_135 g_1029.f1 g_1060.f3 g_562 g_1901 g_1913 g_1922 g_1925 g_1302.f0 g_1932 g_1966 g_680.f3 g_1966.f0 g_1035 g_1060.f0 g_312.f0 g_81 g_1583 g_1584 g_1229 g_151 g_1127 g_1442 g_933 g_1261 g_2068 g_980 g_1459 g_313.f2 g_477 g_2336 g_554.f1 g_254 g_1932.f1
 * writes: g_60 g_535 g_332 g_239 g_181 g_1583 g_582 g_562 g_933 g_554.f3 g_1033 g_151 g_254 g_794 g_895.f1 g_135 g_163 g_549 g_1060.f3 g_1922 g_1925 g_1302.f0 g_680.f3 g_800 g_1127 g_1261 g_1035 g_81 g_237 g_554.f1
 */
static uint16_t  func_14(int32_t * p_15)
{ /* block id: 635 */
    uint8_t l_1536 = 0x1AL;
    int32_t l_1537 = 0x91EDD967L;
    int64_t l_1547 = 0xA14752F767AE6712LL;
    int8_t l_1548 = (-4L);
    int32_t l_1558 = 0L;
    int32_t l_1560[1][7][3] = {{{4L,0x8BCEB1BBL,4L},{1L,1L,1L},{4L,0x8BCEB1BBL,4L},{1L,1L,1L},{4L,0x8BCEB1BBL,4L},{1L,1L,1L},{4L,0x8BCEB1BBL,4L}}};
    uint32_t l_1561 = 0UL;
    uint8_t * const *l_1582 = &g_145;
    uint8_t * const **l_1581[8];
    int32_t l_1608 = 9L;
    int16_t * const *l_1615 = &g_633;
    int16_t * const **l_1614 = &l_1615;
    int16_t * const ***l_1613 = &l_1614;
    uint8_t l_1623 = 0x53L;
    float *l_1630 = &g_151;
    int16_t l_1651[9][3] = {{0xD9BFL,0xD9BFL,0xD9BFL},{0xB152L,0xB152L,0xB152L},{0xD9BFL,0xD9BFL,0xD9BFL},{0xB152L,0xB152L,0xB152L},{0xD9BFL,0xD9BFL,0xD9BFL},{0xB152L,0xB152L,0xB152L},{0xD9BFL,0xD9BFL,0xD9BFL},{0xB152L,0xB152L,0xB152L},{0xD9BFL,0xD9BFL,0xD9BFL}};
    uint16_t **l_1704 = &g_800;
    int16_t l_1800 = (-3L);
    int8_t l_1845 = 0x5EL;
    int32_t l_1846 = (-6L);
    int8_t **l_1857 = &g_397[0][1][1];
    union U0 **l_1935 = &g_1227;
    uint32_t l_1967[2];
    int32_t l_2109 = 0L;
    const int32_t *l_2116 = &g_1932.f1;
    int8_t *** const *l_2137 = &g_1206[1];
    int8_t *** const **l_2136[10] = {(void*)0,&l_2137,(void*)0,&l_2137,(void*)0,&l_2137,(void*)0,&l_2137,(void*)0,&l_2137};
    const int64_t l_2229 = 0x31760F631DE5048DLL;
    uint8_t l_2233 = 0UL;
    int32_t l_2330 = 0x3736D8A4L;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_1581[i] = &l_1582;
    for (i = 0; i < 2; i++)
        l_1967[i] = 18446744073709551606UL;
    if (((((l_1536 > ((*g_1350) >= 0x8AL)) , (l_1537 = l_1536)) || l_1537) == (safe_rshift_func_uint16_t_u_s(((+(safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_u(((*g_145) = (safe_sub_func_uint16_t_u_u(l_1536, (l_1536 ^ l_1547)))), (l_1547 != 0x8F06E553L))) == l_1547), l_1547))) & g_895[0].f0), l_1548))))
    { /* block id: 638 */
        int8_t l_1555 = 1L;
        int32_t l_1556 = (-1L);
        int32_t l_1557 = (-5L);
        int32_t l_1559 = (-10L);
        float l_1567 = 0x5.8DC828p-2;
        for (g_535 = 9; (g_535 <= (-17)); --g_535)
        { /* block id: 641 */
            int32_t *l_1551 = &g_680.f1;
            int32_t *l_1552 = &g_75;
            int32_t *l_1553 = &g_239[0];
            int32_t *l_1554[2][4][5] = {{{&g_1302.f1,&g_75,&g_75,&g_933,&g_933},{(void*)0,&g_239[0],&g_239[0],(void*)0,&g_239[0]},{&g_75,(void*)0,&g_239[0],&g_1113[0].f1,&g_1113[0].f1},{(void*)0,&g_75,(void*)0,&g_239[0],&g_933}},{{&g_1302.f1,&g_75,&g_1060.f1,&g_75,&g_239[0]},{(void*)0,(void*)0,&g_239[0],&g_1060.f1,&g_239[0]},{(void*)0,&g_239[0],&g_75,&g_239[0],(void*)0},{&g_239[0],&g_933,&g_933,&g_239[0],&g_1060.f1}}};
            int i, j, k;
            --l_1561;
            return (*g_800);
        }
        for (g_332 = 0; (g_332 >= 14); g_332 = safe_add_func_uint64_t_u_u(g_332, 4))
        { /* block id: 647 */
            int32_t *l_1568 = &g_2[1][0];
            (*p_15) = (!(*p_15));
            (*g_1569) = l_1568;
        }
        for (l_1559 = 0; (l_1559 == 28); ++l_1559)
        { /* block id: 653 */
            (*p_15) &= l_1561;
        }
    }
    else
    { /* block id: 656 */
        int16_t l_1572 = (-10L);
        return l_1572;
    }
    if ((((l_1560[0][5][1] && ((((((~l_1560[0][2][2]) && ((l_1537 = (safe_mod_func_int64_t_s_s(l_1536, ((safe_rshift_func_int8_t_s_u(((safe_unary_minus_func_uint32_t_u(4294967288UL)) | (&p_15 != (void*)0)), (safe_lshift_func_int16_t_s_s((l_1558 = (1L == (**g_144))), (((((g_1583 = l_1581[0]) != &l_1582) > l_1560[0][2][2]) , 0xF9D3L) , l_1536))))) , g_1060.f1)))) || l_1560[0][2][2])) , l_1537) <= l_1536) , l_1536) | l_1547)) , l_1536) || 0x7BF9L))
    { /* block id: 662 */
        const int32_t *****l_1593 = (void*)0;
        int32_t l_1599 = (-8L);
        uint32_t *l_1609 = (void*)0;
        uint32_t *l_1610 = &g_562;
        int32_t l_1611 = 1L;
        int32_t l_1612 = 1L;
        float * const l_1655 = &g_549;
        int32_t l_1671 = 0xF96F497BL;
        int16_t *l_1680 = &g_254;
        uint16_t **l_1705 = &g_800;
        int16_t **l_1743 = &g_633;
        int16_t *** const l_1742 = &l_1743;
        int32_t l_1820 = (-1L);
        int8_t **l_1858[8][2][7] = {{{&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9]},{&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7]}},{{&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7]},{&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7]}},{{&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9]},{&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7]}},{{&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7]},{&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7]}},{{&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9]},{&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7]}},{{&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7]},{&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7]}},{{&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9],&g_397[0][7][8],&g_397[0][6][9]},{&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7]}},{{&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7],&g_397[0][7][8],&g_397[0][6][7]},{&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7],&g_397[0][7][7],&g_397[1][1][3],&g_397[1][1][3],&g_397[0][7][7]}}};
        uint8_t *l_1929 = &l_1623;
        int32_t l_1973 = 0xFAE821D3L;
        int32_t l_1975 = (-4L);
        int32_t l_1976 = 0x5DDE496CL;
        uint8_t l_2005[4] = {0UL,0UL,0UL,0UL};
        uint8_t * const **l_2019 = &g_1584;
        const uint8_t l_2091 = 0xFDL;
        int32_t l_2157 = 1L;
        int32_t l_2158 = (-8L);
        int32_t l_2159 = (-4L);
        int8_t l_2160[6] = {0xE6L,0xE6L,0xE6L,0xE6L,0xE6L,0xE6L};
        int32_t l_2161[9][2][7] = {{{0xAEB97285L,(-1L),0L,(-1L),0xAEB97285L,0xC6032D65L,0xAEB97285L},{0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L}},{{0xCE2DD79BL,(-1L),0xCE2DD79BL,1L,0xAEB97285L,1L,0xCE2DD79BL},{0x1C5DB043L,0x1C5DB043L,0x134A8F5DL,0x1C5DB043L,0x1C5DB043L,0x134A8F5DL,0x1C5DB043L}},{{0xAEB97285L,1L,0xCE2DD79BL,(-1L),0xCE2DD79BL,1L,0xAEB97285L},{0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL}},{{0xAEB97285L,(-1L),0L,(-1L),0xAEB97285L,0xC6032D65L,0xAEB97285L},{0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0xECCF9BCAL}},{{0L,1L,0L,0xC6032D65L,0xCE2DD79BL,0xC6032D65L,0L},{0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL}},{{0xCE2DD79BL,0xC6032D65L,0L,1L,0L,0xC6032D65L,0xCE2DD79BL},{0x134A8F5DL,0xECCF9BCAL,0x134A8F5DL,0x134A8F5DL,0xECCF9BCAL,0x134A8F5DL,0x134A8F5DL}},{{0xCE2DD79BL,1L,0xAEB97285L,1L,0xCE2DD79BL,(-1L),0xCE2DD79BL},{0xECCF9BCAL,0x134A8F5DL,0x134A8F5DL,0xECCF9BCAL,0x134A8F5DL,0x134A8F5DL,0xECCF9BCAL}},{{0L,1L,0L,0xC6032D65L,0xCE2DD79BL,0xC6032D65L,0L},{0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL,0xECCF9BCAL,0x1C5DB043L,0xECCF9BCAL}},{{0xCE2DD79BL,0xC6032D65L,0L,1L,0L,0xC6032D65L,0xCE2DD79BL},{0x134A8F5DL,0xECCF9BCAL,0x134A8F5DL,0x134A8F5DL,0xECCF9BCAL,0x134A8F5DL,0x134A8F5DL}}};
        uint32_t l_2166 = 0x7BF3C222L;
        int8_t l_2332 = 1L;
        int i, j, k;
        if ((safe_mul_func_uint8_t_u_u(((((safe_mul_func_uint16_t_u_u(((*g_800) = l_1560[0][3][1]), (g_1589 == l_1593))) < (safe_lshift_func_uint8_t_u_u((((safe_mod_func_int8_t_s_s(l_1536, l_1536)) & (~((l_1599 ^ (safe_div_func_uint32_t_u_u(((*l_1610) = (safe_lshift_func_int8_t_s_u((l_1558 = (safe_add_func_uint8_t_u_u(0UL, (safe_lshift_func_int16_t_s_u((l_1537 = (l_1608 == l_1608)), 8))))), 4))), (-1L)))) || l_1548))) == l_1548), l_1611))) & (*p_15)) >= l_1612), l_1561)))
        { /* block id: 667 */
            int16_t * const ***l_1616 = &l_1614;
            int32_t l_1633 = 3L;
            float l_1634 = 0x0.2p+1;
            int32_t l_1635 = 0xCC154F33L;
            uint8_t l_1727 = 0x98L;
            int8_t **l_1763 = &g_397[0][6][7];
            uint32_t *l_1787 = &g_562;
            int32_t *l_1847 = &g_895[0].f1;
            int32_t l_1974[7][8] = {{3L,0x3BB9C842L,0x75F47E93L,0xE4020441L,0xB20A1C02L,0x6FA351C2L,0xB20A1C02L,0xE4020441L},{(-5L),(-3L),(-5L),0xC1732A72L,0xE4020441L,0x6FA351C2L,0x75F47E93L,0x75F47E93L},{0x75F47E93L,0x3BB9C842L,3L,3L,0x3BB9C842L,0x75F47E93L,0xE4020441L,0xB20A1C02L},{0x75F47E93L,0x852296BAL,(-1L),0x3BB9C842L,0xE4020441L,0x3BB9C842L,(-1L),0x852296BAL},{(-5L),(-1L),0x6FA351C2L,0x3BB9C842L,0xB20A1C02L,0xC1732A72L,0xC1732A72L,0xB20A1C02L},{3L,0xB20A1C02L,0xB20A1C02L,3L,(-5L),0x852296BAL,0xC1732A72L,0x75F47E93L},{(-1L),3L,0x6FA351C2L,0xC1732A72L,0x6FA351C2L,3L,(-1L),0xE4020441L}};
            uint32_t l_1977 = 0x4D95045DL;
            uint16_t *l_2010 = (void*)0;
            int32_t l_2012 = 0L;
            int i, j;
            l_1616 = l_1613;
            for (l_1612 = (-15); (l_1612 <= 3); l_1612 = safe_add_func_int32_t_s_s(l_1612, 2))
            { /* block id: 671 */
                int32_t l_1625 = 3L;
                int8_t l_1650 = 0xE0L;
                uint16_t l_1652 = 8UL;
                int16_t * const *l_1656 = &g_633;
                int32_t l_1681[9][3] = {{0L,(-9L),0L},{6L,6L,6L},{0L,(-9L),0L},{6L,6L,6L},{0L,(-9L),0L},{6L,6L,6L},{0L,(-9L),0L},{6L,6L,6L},{0L,(-9L),0L}};
                int32_t l_1683 = 9L;
                uint64_t l_1718 = 2UL;
                float **l_1726 = (void*)0;
                int32_t l_1801 = 0xA26399E9L;
                uint64_t l_1807 = 1UL;
                int8_t *l_1818 = &l_1548;
                uint16_t ** const l_1819[10][10] = {{&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800},{(void*)0,(void*)0,&g_800,(void*)0,(void*)0,&g_800,(void*)0,(void*)0,&g_800,(void*)0},{(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0},{&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800,&g_800,&g_800,&g_800},{&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800},{&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800},{&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800},{&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800,&g_800,(void*)0,&g_800},{&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800},{&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800,&g_800}};
                int i, j;
                for (g_933 = 0; (g_933 <= 2); g_933 += 1)
                { /* block id: 674 */
                    uint64_t l_1624 = 0xDCFEF9EFA40340F3LL;
                    int8_t *l_1632[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1632[i] = &l_1548;
                    if (((safe_add_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_s((l_1633 = ((l_1623 <= (l_1624 , (l_1625 ^ 0L))) | (safe_mod_func_uint16_t_u_u((0x3969L ^ l_1558), (((*p_15) = ((((-9L) <= (safe_add_func_int32_t_s_s(((l_1630 != (void*)0) ^ 0x3918L), l_1561))) != 250UL) || g_1631)) ^ 0x45DAC451L))))), l_1635)) <= 0x2705L), 0x805C8B06L)) , (*p_15)))
                    { /* block id: 677 */
                        if (l_1560[0][2][2])
                            break;
                    }
                    else
                    { /* block id: 679 */
                        uint16_t l_1636 = 0xFB1DL;
                        l_1633 ^= l_1636;
                    }
                    for (g_554.f3 = 0; (g_554.f3 >= 0); g_554.f3 -= 1)
                    { /* block id: 684 */
                        return l_1624;
                    }
                    for (g_1033 = 0; (g_1033 <= 2); g_1033 += 1)
                    { /* block id: 689 */
                        int64_t l_1639 = (-1L);
                        l_1639 = (safe_div_func_float_f_f(0x0.4p-1, ((*l_1630) = l_1625)));
                    }
                }
            }
            (*l_1847) ^= (l_1560[0][2][2] &= ((((((((safe_mul_func_uint8_t_u_u((+(safe_sub_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((((safe_rshift_func_uint16_t_u_s(((l_1611 = (1L > ((safe_mod_func_int16_t_s_s(((*l_1680) = (g_78 , (l_1537 ^= l_1608))), (safe_lshift_func_uint16_t_u_s((safe_div_func_int64_t_s_s((safe_lshift_func_int16_t_s_s(((((((*g_911) = 0x1.8p-1) , (safe_div_func_uint16_t_u_u(((safe_div_func_int8_t_s_s(0L, l_1633)) | (~((g_1060.f2 > ((*p_15) & (safe_mod_func_int8_t_s_s(l_1635, l_1845)))) , l_1845))), 0xABB0L))) , l_1727) || l_1846) < g_237), 11)), 1L)), l_1611)))) < l_1611))) || 0x66L), 2)) <= (*p_15)) , l_1611), l_1558)), l_1651[6][2]))), 0x9DL)) , l_1635) & (-5L)) , (**g_144)) , 1L) >= l_1633) != l_1727) == (-3L)));
            if (l_1560[0][2][2])
            { /* block id: 776 */
                const int32_t l_1852 = 0xF647F6F5L;
                int8_t ****l_1854 = &g_1206[1];
                int8_t *****l_1853 = &l_1854;
                float *l_1855 = &l_1634;
                int32_t *l_1856 = &g_135;
                (*l_1856) ^= ((safe_div_func_float_f_f(((0xF.434B43p+38 <= ((*g_1163) != (*g_911))) != (((safe_div_func_float_f_f(l_1558, (*l_1847))) == l_1852) > ((*l_1855) = ((((*l_1853) = &g_1206[1]) == (void*)0) , ((*l_1630) = (l_1852 == l_1852)))))), l_1560[0][2][1])) , (*p_15));
                l_1858[5][0][2] = l_1857;
            }
            else
            { /* block id: 782 */
                int32_t l_1862 = (-1L);
                uint32_t *l_1886[6][1];
                uint16_t *l_1914 = &g_1033;
                int i, j;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1886[i][j] = &l_1561;
                }
                (*l_1655) = (!(((*g_145) = ((safe_div_func_uint8_t_u_u(l_1862, (safe_sub_func_uint64_t_u_u((safe_add_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((safe_unary_minus_func_uint64_t_u((((*p_15) = ((safe_mul_func_int16_t_s_s((0xEF9E54C540980A1FLL ^ ((*l_1847) && ((safe_lshift_func_uint8_t_u_s(((safe_mod_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((safe_div_func_int16_t_s_s((((safe_rshift_func_int16_t_s_u(2L, 9)) , (safe_mod_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_s(((l_1886[4][0] != (void*)0) , ((g_163 = 4294967295UL) <= (l_1548 < ((((**l_1705) = 65528UL) <= (*l_1847)) > l_1862)))), l_1671)), g_1029.f1))) , (*l_1847)), l_1862)), 0x972A6552L)), 8L)) , l_1547), l_1558)) ^ 0x022CL))), l_1862)) & (-8L))) ^ l_1862))), 0xBC704C8FC1159AA8LL)), 1UL)), 4L)))) , 0x76L)) , 0x1.4p-1));
                for (g_1060.f3 = 0; (g_1060.f3 == 29); g_1060.f3 = safe_add_func_uint8_t_u_u(g_1060.f3, 7))
                { /* block id: 790 */
                    uint16_t l_1889[6] = {0x248CL,0x248CL,0x248CL,0x248CL,0x248CL,0x248CL};
                    int32_t l_1892 = 0xC579D37FL;
                    uint8_t *l_1911[4];
                    int16_t ****l_1926 = &g_1925;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1911[i] = &g_332;
                    (*p_15) ^= 0x22EBA2B2L;
                    if (((++l_1889[5]) ^ (l_1892 > (safe_div_func_uint64_t_u_u((l_1846 || l_1560[0][2][2]), (safe_sub_func_uint32_t_u_u(g_562, ((*l_1847) , (safe_mod_func_int8_t_s_s((((l_1862 != l_1892) && 2L) > ((l_1892 ^ l_1862) >= 255UL)), 0xE1L))))))))))
                    { /* block id: 793 */
                        uint64_t *l_1904[8][9] = {{(void*)0,&g_495,(void*)0,(void*)0,&g_1261,&g_81,&g_81,&g_1261,(void*)0},{&g_495,(void*)0,&g_495,(void*)0,&g_81,&g_81,&g_495,&g_495,&g_1261},{(void*)0,(void*)0,&g_81,&g_81,&g_81,(void*)0,(void*)0,(void*)0,&g_81},{&g_81,(void*)0,&g_81,(void*)0,(void*)0,&g_81,(void*)0,&g_81,&g_495},{&g_495,(void*)0,(void*)0,(void*)0,&g_1261,&g_81,&g_81,&g_495,&g_1261},{(void*)0,(void*)0,&g_495,&g_81,&g_495,&g_81,(void*)0,&g_81,&g_495},{&g_495,(void*)0,(void*)0,&g_495,&g_81,&g_81,(void*)0,&g_495,&g_81},{&g_81,&g_495,(void*)0,(void*)0,(void*)0,(void*)0,&g_81,(void*)0,&g_1261}};
                        uint64_t **l_1903 = &l_1904[5][6];
                        uint64_t **l_1905 = (void*)0;
                        uint64_t *l_1907 = &g_81;
                        uint64_t **l_1906 = &l_1907;
                        uint8_t *l_1910 = (void*)0;
                        uint8_t l_1912 = 0xE5L;
                        int32_t l_1915 = 0L;
                        int i, j;
                        (*g_911) = (safe_mul_func_float_f_f(l_1558, ((g_1901 , 0x7.E14F39p+81) > (((l_1912 ^= ((((!((*g_800) = (l_1862 < (((*l_1903) = &g_81) != ((*l_1906) = &g_1261))))) || ((safe_add_func_uint8_t_u_u(((((l_1911[3] = l_1910) != &l_1727) ^ ((*g_800) = (*l_1847))) , 0xBBL), 5L)) , l_1889[2])) <= 0x277D34BAL) ^ l_1862)) , (*g_911)) < l_1862))));
                        (*p_15) = l_1862;
                        (*p_15) = ((g_1913 , ((void*)0 == l_1914)) & (g_163 = l_1915));
                    }
                    else
                    { /* block id: 804 */
                        int32_t *l_1916 = &l_1635;
                        int32_t *l_1917 = &g_680.f1;
                        int32_t *l_1918 = &g_554.f1;
                        int32_t *l_1919 = &g_239[0];
                        int32_t *l_1920 = &l_1558;
                        int32_t *l_1921[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1921[i] = (void*)0;
                        ++g_1922;
                    }
                    (*l_1847) = ((5L | (l_1536 , ((((l_1892 != (((*l_1926) = g_1925) == (void*)0)) >= ((*l_1655) = (*g_911))) >= ((l_1892 , (-0x3.Bp+1)) <= 0x7.AB21C1p-80)) , 0x24L))) , 0xD71E1AECL);
                    for (g_1302.f0 = 0; (g_1302.f0 <= 0); g_1302.f0 += 1)
                    { /* block id: 812 */
                        int64_t *l_1936 = &l_1547;
                        uint64_t l_1943 = 0UL;
                        int32_t *l_1968 = &g_1113[0].f1;
                        int32_t *l_1969 = &l_1862;
                        int32_t *l_1970 = (void*)0;
                        int32_t *l_1971 = &l_1862;
                        int32_t *l_1972[8][4] = {{&l_1635,&l_1635,&g_933,&g_933},{&l_1635,&l_1820,&g_239[0],&l_1635},{&g_1302.f1,&g_933,&g_1302.f1,&g_239[0]},{&g_1465.f1,&g_933,&g_933,&l_1635},{&g_933,&l_1820,&l_1820,&g_933},{&g_1302.f1,&l_1635,&l_1820,&g_239[0]},{&g_933,&g_1465.f1,&g_933,&g_1465.f1},{&g_1465.f1,&l_1820,&g_1302.f1,&g_1465.f1}};
                        int i, j;
                        g_239[g_1302.f0] ^= ((*l_1847) == (safe_add_func_uint64_t_u_u((l_1911[(g_1302.f0 + 3)] == l_1929), (safe_sub_func_int16_t_s_s(l_1862, ((l_1889[5] , g_1932) , 1UL))))));
                        (*l_1655) = ((((safe_add_func_int64_t_s_s(((void*)0 != l_1935), ((*l_1936) = 0xF602DCF6503CF8B7LL))) , g_239[g_1302.f0]) == ((((*l_1847) = (((*p_15) , l_1914) != (void*)0)) || (safe_rshift_func_uint8_t_u_u((*g_145), 2))) || ((safe_add_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u(((l_1889[5] > 0UL) | 0x7BL), l_1892)), l_1651[3][1])) & l_1608))) , l_1943);
                        l_1611 |= ((-8L) > ((safe_lshift_func_int8_t_s_s(l_1892, l_1889[5])) & (safe_rshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_s((safe_sub_func_int64_t_s_s(((((((l_1889[5] , (safe_mod_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s(((safe_rshift_func_uint16_t_u_u(((*g_800) | (safe_lshift_func_int16_t_s_u(l_1846, 4))), 3)) <= (safe_mul_func_uint8_t_u_u((safe_div_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(4UL, 10)), l_1862)), (((*l_1929) = ((**g_144) |= (g_1966 , 0x39L))) < l_1862)))), 10)), l_1967[1]))) , l_1862) | g_895[0].f1) & l_1862) & l_1889[5]) <= l_1889[5]), g_78)), 12)), l_1892))));
                        --l_1977;
                    }
                }
                for (g_680.f3 = 0; (g_680.f3 >= 12); ++g_680.f3)
                { /* block id: 825 */
                    int32_t l_2008 = 0xCA6663D8L;
                    for (l_1623 = 0; (l_1623 <= 0); l_1623 += 1)
                    { /* block id: 828 */
                        uint16_t *l_2000 = &g_1035[0];
                        uint16_t **l_2009[5][4] = {{&l_1914,&l_1914,&l_1914,&l_1914},{&l_1914,&l_1914,&l_1914,&l_1914},{&l_1914,&l_1914,&l_1914,&l_1914},{&l_1914,&l_1914,&l_1914,&l_1914},{&l_1914,&l_1914,&l_1914,&l_1914}};
                        const int32_t l_2011 = 0x4B100682L;
                        int i, j;
                        (*p_15) &= (safe_mul_func_int16_t_s_s((safe_add_func_uint64_t_u_u(l_1536, (((g_1966.f0 & (((safe_mul_func_int16_t_s_s((((((*g_800) | g_1060.f0) >= ((0xF58FB2EEL || (*l_1847)) & 65535UL)) > ((void*)0 == &l_1581[2])) >= l_1862), 0UL)) & g_312.f0) > (*l_1847))) || l_1608) > 0xEF79L))), (*g_800)));
                        (*p_15) = (safe_sub_func_uint16_t_u_u(((safe_sub_func_int64_t_s_s(0L, ((safe_add_func_int8_t_s_s((l_1561 >= ((+0x3481L) > (((*l_1847) < ((((safe_rshift_func_int8_t_s_u((safe_sub_func_int16_t_s_s((+g_81), ((((*l_1705) = l_2000) != (l_2010 = ((safe_add_func_int32_t_s_s((safe_mod_func_uint32_t_u_u(l_2005[2], ((((safe_mod_func_int16_t_s_s(((**g_144) <= l_2008), 0x60E5L)) > (*l_1847)) == 2UL) ^ l_1671))), l_1862)) , l_2000))) <= l_1862))), 1)) , &l_1929) != (*g_1583)) , 1L)) | l_2011))), l_1560[0][2][2])) && (*l_1847)))) , 0xC5FCL), l_2012));
                    }
                }
            }
        }
        else
        { /* block id: 836 */
            uint8_t ***l_2018 = &g_144;
            int32_t l_2020 = 0x0D549D6EL;
            int32_t ****l_2052 = &g_477;
            int32_t *****l_2051 = &l_2052;
            uint8_t * const ***l_2085 = (void*)0;
            for (l_1846 = 0; (l_1846 <= 2); l_1846 += 1)
            { /* block id: 839 */
                int64_t l_2015[6][7][3] = {{{(-1L),(-1L),1L},{3L,1L,3L},{(-1L),(-2L),(-2L)},{1L,1L,0xBF5E898F1F5FE97DLL},{0x437AC5B6F77E53A8LL,(-1L),(-2L)},{3L,0xFF3BCEBB5B307BA6LL,3L},{0x437AC5B6F77E53A8LL,(-2L),1L}},{{1L,0xFF3BCEBB5B307BA6LL,0xBF5E898F1F5FE97DLL},{(-1L),(-1L),1L},{3L,1L,3L},{(-1L),(-2L),(-2L)},{1L,1L,0xBF5E898F1F5FE97DLL},{0x437AC5B6F77E53A8LL,(-1L),(-2L)},{3L,0xFF3BCEBB5B307BA6LL,3L}},{{0x437AC5B6F77E53A8LL,(-2L),1L},{1L,0xFF3BCEBB5B307BA6LL,0xBF5E898F1F5FE97DLL},{(-1L),(-1L),1L},{3L,1L,3L},{(-1L),(-2L),(-2L)},{1L,1L,0xBF5E898F1F5FE97DLL},{0x437AC5B6F77E53A8LL,(-1L),(-2L)}},{{3L,0xFF3BCEBB5B307BA6LL,3L},{0x437AC5B6F77E53A8LL,(-2L),1L},{1L,0xFF3BCEBB5B307BA6LL,0xBF5E898F1F5FE97DLL},{(-1L),(-1L),1L},{3L,1L,3L},{(-1L),(-2L),(-2L)},{1L,1L,0xBF5E898F1F5FE97DLL}},{{0x437AC5B6F77E53A8LL,(-1L),(-2L)},{3L,0xFF3BCEBB5B307BA6LL,3L},{0x437AC5B6F77E53A8LL,(-2L),1L},{1L,0xFF3BCEBB5B307BA6LL,0xBF5E898F1F5FE97DLL},{(-1L),(-1L),1L},{3L,1L,3L},{(-1L),(-2L),(-2L)}},{{1L,1L,0xBF5E898F1F5FE97DLL},{0x437AC5B6F77E53A8LL,(-1L),(-2L)},{3L,0xFF3BCEBB5B307BA6LL,3L},{0x437AC5B6F77E53A8LL,(-2L),1L},{1L,0xFF3BCEBB5B307BA6LL,0xBF5E898F1F5FE97DLL},{(-1L),(-1L),1L},{3L,1L,3L}}};
                int32_t l_2040 = 0x79327288L;
                int32_t l_2044 = 0xDB961E20L;
                int32_t l_2074 = 0x2FCFFDEFL;
                uint64_t l_2110 = 18446744073709551608UL;
                int i, j, k;
                l_2020 = ((l_1623 > ((safe_sub_func_float_f_f(l_2015[3][5][1], (safe_add_func_float_f_f((l_2018 == l_2019), l_2020)))) > l_2020)) == (l_1846 != (((*l_1655) = (((safe_sub_func_float_f_f(l_2015[3][5][1], l_2015[0][5][0])) > 0x1.Ep+1) == (*g_1229))) < l_2020)));
                for (g_1127 = 0; (g_1127 <= 2); g_1127 += 1)
                { /* block id: 844 */
                    uint64_t *l_2031 = &g_1261;
                    int32_t l_2034 = (-1L);
                    int64_t *l_2035 = &l_2015[3][5][1];
                    int32_t *l_2039[5][2] = {{&g_895[0].f0,&g_895[0].f0},{&g_895[0].f0,&g_895[0].f0},{&g_895[0].f0,&g_895[0].f0},{&g_895[0].f0,&g_895[0].f0},{&g_895[0].f0,&g_895[0].f0}};
                    int32_t l_2043 = 1L;
                    int16_t l_2073 = 0L;
                    int16_t ***l_2090 = &l_1743;
                    int i, j;
                    if ((((safe_add_func_int16_t_s_s((l_2043 = (0x4D62281E8657D2D3LL < (((*g_1442) < (safe_sub_func_uint64_t_u_u((((((*p_15) >= (0L & (safe_sub_func_uint64_t_u_u(((*l_2031)++), ((*l_2035) = l_2034))))) || ((safe_add_func_float_f_f(((l_2040 = (~l_2034)) , (safe_mul_func_float_f_f(l_1846, ((((*g_911) = ((l_2034 = l_1623) > (((*l_1655) = (((((*l_1630) = l_2020) >= 0x7.Bp-1) < l_2020) != l_2020)) < (*g_911)))) < 0x6.9C4CE5p-34) == l_2043)))), l_2020)) , l_2015[0][2][0])) | 0xDD35C534L) > l_2044), l_2020))) != l_2043))), l_2044)) , l_2020) , l_1846))
                    { /* block id: 853 */
                        uint32_t l_2067[3];
                        uint16_t *l_2069 = (void*)0;
                        int32_t **l_2070 = &l_2039[1][1];
                        uint16_t *l_2071 = &g_1035[0];
                        int32_t l_2072 = 0x62416B30L;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_2067[i] = 18446744073709551609UL;
                        if ((*p_15))
                            break;
                        l_2074 |= ((*g_800) >= ((l_2072 = (((*l_2071) &= (safe_sub_func_uint32_t_u_u(((((*l_2070) = func_40((safe_add_func_uint16_t_u_u((((safe_mul_func_uint8_t_u_u(((void*)0 == l_2051), ((safe_add_func_int64_t_s_s((g_135 , ((safe_rshift_func_int16_t_s_s(((safe_div_func_uint8_t_u_u((((l_1608 ^= (l_1560[0][4][0] = (safe_mul_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(0xC0958ED74499E559LL, (safe_mod_func_uint16_t_u_u(((safe_div_func_int32_t_s_s(((((l_2044 < (l_2067[0] > (((((((*p_15) != 1L) >= (-9L)) ^ 0x7C223F95L) , 0x44E5C37051C83FACLL) > 18446744073709551612UL) & (*g_800)))) , g_2068[0]) , l_1558) , (-1L)), g_239[0])) != l_1560[0][2][2]), l_2015[3][5][1])))), 0UL)))) ^ l_2015[3][0][0]) , 0x78L), l_2040)) , l_2034), 14)) <= 0x4B9C84125F2A8981LL)), l_2067[0])) , l_1608))) ^ 0xDBL) != l_2067[0]), l_2015[3][5][1])), g_1127)) == p_15) > l_1558), 0x57465ECAL))) == (-1L))) > l_2073));
                        l_1560[0][2][2] ^= ((safe_rshift_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((((safe_lshift_func_uint8_t_u_s((*g_145), (safe_lshift_func_int16_t_s_u(((((*p_15) >= (g_980 == ((*l_2035) = ((0x5F350EFAL != 0x3D079EE7L) | (((safe_mul_func_int16_t_s_s(l_2043, (&g_258 != (l_2085 = (void*)0)))) && (safe_mul_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u(((void*)0 != l_2090), 0x04L)), 0x546EL))) && 18446744073709551615UL))))) != l_2073) > (-2L)), l_2044)))) , g_1459) , g_313[1].f2), l_1800)), l_2091)) > l_1846);
                    }
                    else
                    { /* block id: 864 */
                        int64_t l_2092 = 0L;
                        uint64_t l_2098 = 1UL;
                        int32_t ***l_2106[8][3][6] = {{{&g_180[6],&g_180[6],&g_180[6],&g_180[0],(void*)0,&g_180[6]},{&g_180[3],&g_180[5],&g_180[2],&g_180[6],(void*)0,&g_180[6]},{&g_180[6],&g_180[6],&g_180[3],(void*)0,&g_180[6],&g_180[6]}},{{&g_180[6],(void*)0,&g_180[3],&g_180[6],(void*)0,(void*)0},{&g_180[5],&g_180[4],&g_180[6],&g_180[6],&g_180[3],&g_180[6]},{&g_180[1],(void*)0,(void*)0,&g_180[0],&g_180[5],(void*)0}},{{&g_180[4],&g_180[6],&g_180[6],&g_180[5],(void*)0,&g_180[0]},{&g_180[6],&g_180[0],&g_180[6],&g_180[6],(void*)0,&g_180[6]},{&g_180[6],&g_180[6],(void*)0,&g_180[6],&g_180[6],&g_180[6]}},{{&g_180[6],&g_180[6],&g_180[6],&g_180[6],&g_180[1],&g_180[6]},{&g_180[6],&g_180[5],&g_180[5],&g_180[5],&g_180[6],&g_180[6]},{(void*)0,&g_180[4],(void*)0,(void*)0,&g_180[3],&g_180[6]}},{{(void*)0,&g_180[6],&g_180[6],&g_180[4],&g_180[6],&g_180[6]},{&g_180[6],&g_180[6],(void*)0,&g_180[0],&g_180[1],&g_180[6]},{&g_180[6],&g_180[6],&g_180[5],&g_180[1],&g_180[3],&g_180[6]}},{{&g_180[6],(void*)0,&g_180[6],&g_180[6],(void*)0,&g_180[6]},{&g_180[6],&g_180[6],&g_180[1],&g_180[3],&g_180[6],(void*)0},{&g_180[6],&g_180[6],&g_180[0],&g_180[5],&g_180[2],&g_180[6]}},{{&g_180[1],&g_180[6],&g_180[6],&g_180[6],&g_180[3],&g_180[5]},{&g_180[3],(void*)0,&g_180[3],(void*)0,&g_180[6],&g_180[0]},{&g_180[6],(void*)0,&g_180[4],&g_180[6],(void*)0,&g_180[6]}},{{&g_180[3],&g_180[6],(void*)0,(void*)0,&g_180[6],&g_180[3]},{&g_180[6],&g_180[6],&g_180[1],&g_180[5],&g_180[5],&g_180[6]},{&g_180[6],&g_180[5],&g_180[3],&g_180[1],&g_180[6],&g_180[6]}}};
                        int32_t ****l_2105 = &l_2106[2][0][0];
                        int i, j, k;
                        l_2034 = ((l_2092 , ((((+((safe_mod_func_int8_t_s_s((l_1976 ^= ((((safe_mod_func_uint8_t_u_u((l_2098 < (safe_lshift_func_uint16_t_u_s((((l_1558 = (-1L)) , ((*p_15) = 0x12CFC526L)) && (((safe_lshift_func_uint8_t_u_u(((((safe_mul_func_int16_t_s_s(((**l_2051) == ((*l_2105) = (void*)0)), 0x6DD6L)) >= l_2015[1][5][1]) == ((safe_mul_func_uint16_t_u_u(0x901AL, l_2109)) > l_2074)) & l_2073), 5)) == (*p_15)) ^ 0x56L)), l_1599))), l_2110)) & (-1L)) < l_2040) | (*g_145))), (**g_144))) & 0x46162031L)) == l_2043) | l_2043) && (-4L))) < l_2044);
                    }
                    return l_2020;
                }
            }
        }
        for (g_81 = 0; (g_81 <= 1); g_81 = safe_add_func_int32_t_s_s(g_81, 6))
        { /* block id: 877 */
            uint32_t l_2113 = 0UL;
            int64_t **l_2117 = &g_561[0][1];
            uint32_t l_2129 = 0x21B59740L;
            uint16_t l_2147 = 0xF2D1L;
            int32_t l_2150[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            uint8_t *l_2238 = &g_60;
            uint32_t l_2269 = 7UL;
            uint8_t l_2282 = 255UL;
            int32_t l_2314 = 0x0235A618L;
            int i;
        }
        for (g_237 = 0; (g_237 != (-8)); --g_237)
        { /* block id: 949 */
            const int32_t **l_2323 = &l_2116;
            int32_t l_2328 = 5L;
            int32_t *l_2329[6] = {&l_1560[0][4][0],&l_1560[0][4][0],&g_239[0],&l_1560[0][4][0],&l_1560[0][4][0],&g_239[0]};
            int64_t l_2331[3];
            uint64_t l_2333 = 0UL;
            int i;
            for (i = 0; i < 3; i++)
                l_2331[i] = 0x9DF9F358EBB2506DLL;
        }
    }
    else
    { /* block id: 955 */
        int8_t *l_2337 = &l_1845;
        p_15 = func_16(g_2336, l_2337);
        for (g_254 = 12; (g_254 >= (-28)); g_254--)
        { /* block id: 959 */
            return (*l_2116);
        }
    }
    return (*g_800);
}


/* ------------------------------------------ */
/* 
 * reads : g_237 g_145 g_60 g_554.f1
 * writes: g_60 g_554.f1
 */
static int32_t * func_16(int8_t * p_17, int8_t * p_18)
{ /* block id: 629 */
    int32_t l_1530 = 0x54F1FC35L;
    int32_t *l_1531 = &g_554.f1;
    int16_t **l_1533 = &g_633;
    int16_t ***l_1532[10] = {&l_1533,&l_1533,&l_1533,&l_1533,(void*)0,&l_1533,&l_1533,&l_1533,&l_1533,(void*)0};
    int16_t ****l_1534 = &l_1532[5];
    int32_t *l_1535 = &g_239[0];
    int i;
    l_1530 = ((*l_1531) &= (g_237 > (((*g_145)--) , ((0xB086DF169F3496C9LL == (safe_lshift_func_uint16_t_u_u(1UL, 7))) , l_1530))));
    (*l_1534) = l_1532[5];
    return l_1535;
}


/* ------------------------------------------ */
/* 
 * reads : g_81 g_397
 * writes: g_81
 */
static int8_t * func_20(const int8_t * p_21)
{ /* block id: 567 */
    int32_t l_1356 = 0xA74515DAL;
    int8_t **l_1362 = &g_397[0][6][7];
    int16_t *l_1364 = &g_1127;
    int32_t l_1389 = (-5L);
    int8_t l_1391[7][8] = {{0xE2L,(-1L),(-1L),0xE2L,0xE8L,1L,0L,(-1L)},{0L,(-5L),0x3AL,0x5AL,(-5L),1L,(-5L),0x5AL},{1L,(-5L),1L,(-1L),0L,1L,0xE8L,0xE2L},{0L,(-1L),0x9EL,0L,0xFFL,0xFFL,0L,0x9EL},{0L,0L,1L,0x5AL,0L,1L,0L,0L},{1L,0L,(-1L),1L,(-5L),1L,(-1L),0L},{0L,0xE8L,0x9EL,0x5AL,0xE8L,(-1L),(-5L),0x9EL}};
    int32_t l_1392[7][5] = {{0xB944F4A3L,(-1L),0x9E5400E7L,0L,3L},{0x9E5400E7L,3L,(-1L),(-1L),3L},{3L,1L,0x9E5400E7L,3L,0xE98A003DL},{0x9EE00302L,1L,3L,1L,0x2C2B7500L},{0x9E5400E7L,1L,1L,0x9E5400E7L,0x9EE00302L},{0xF11E78C6L,3L,0x2C2B7500L,0L,0x9EE00302L},{1L,0xF11E78C6L,0xB944F4A3L,1L,0xB944F4A3L}};
    uint8_t ***l_1393 = &g_144;
    float l_1394[5];
    uint32_t l_1420 = 4294967295UL;
    int32_t l_1448 = (-1L);
    int32_t l_1451 = (-7L);
    int32_t l_1453 = (-7L);
    int32_t l_1454 = 0x8ABEC5B2L;
    int32_t l_1456 = 0x92A9B423L;
    int32_t l_1458 = 0xFAA3396FL;
    int32_t l_1492 = 0x199B808CL;
    int32_t l_1493 = 1L;
    int32_t l_1494 = 0xF47E30C6L;
    int32_t l_1498 = (-1L);
    uint32_t l_1523 = 0x49A8EBAAL;
    int i, j;
    for (i = 0; i < 5; i++)
        l_1394[i] = (-0x1.Ap+1);
    for (g_81 = 0; (g_81 <= 28); g_81++)
    { /* block id: 570 */
        int32_t l_1361 = 0x22412F02L;
        uint8_t l_1363 = 0xDEL;
        int32_t l_1365 = 0xA7FE5668L;
        int32_t ****l_1373 = &g_477;
        int32_t *****l_1372 = &l_1373;
        int64_t *l_1379 = &g_680.f3;
        float *l_1386 = (void*)0;
        float **l_1390 = &g_911;
        int32_t l_1450[5][7][7] = {{{0xE6EC1E20L,0L,0L,0xA1CD4D61L,1L,0L,(-1L)},{0x8C75A3B3L,(-1L),(-6L),0xB177BF85L,0L,0x75593D57L,(-3L)},{0x558BE347L,0x9CCC69F8L,0xF9B8DDCDL,0x3EA445BFL,0x2F18A8E4L,0L,(-3L)},{(-3L),0L,(-7L),0x558BE347L,0xD5AEC42BL,0xF8D342DBL,0xA1CD4D61L},{(-3L),0xEBEB721DL,0xB177BF85L,0x9CCC69F8L,0x9CCC69F8L,0xB177BF85L,0xEBEB721DL},{0x558BE347L,0xBA0CEDBDL,0xB2C3A522L,(-7L),0x8C75A3B3L,0xA7C387DFL,(-8L)},{0x8C75A3B3L,0x75593D57L,0xD5AEC42BL,(-6L),0xAB029CC0L,0L,(-7L)}},{{0xE6EC1E20L,0x2F18A8E4L,(-3L),(-7L),(-1L),(-7L),0L},{(-3L),(-7L),(-8L),0x9CCC69F8L,0xBA17641BL,0xE772573EL,0xF8D342DBL},{(-1L),(-2L),0xBA17641BL,0x558BE347L,0L,(-3L),0xF8D342DBL},{0L,(-8L),0xF8D342DBL,0x3EA445BFL,0xF8D342DBL,(-8L),0L},{0L,0L,(-7L),0xB177BF85L,(-3L),0x558BE347L,(-7L)},{0xD5AEC42BL,0L,0L,0xA1CD4D61L,0x558BE347L,(-7L),(-8L)},{0x75593D57L,0xE6EC1E20L,(-7L),0L,(-2L),0xBA0CEDBDL,0xEBEB721DL}},{{0x475AB14EL,0xB2C3A522L,0xF8D342DBL,0L,0xF9B8DDCDL,0xD5AEC42BL,0xA1CD4D61L},{0x3EA445BFL,(-7L),0xBA17641BL,0xBA0CEDBDL,0L,0xD5AEC42BL,(-3L)},{0xBA0CEDBDL,0xE772573EL,(-8L),(-8L),0xE772573EL,0xBA0CEDBDL,(-3L)},{0xB177BF85L,0xA1CD4D61L,(-3L),0xE772573EL,0L,(-7L),(-1L)},{(-7L),0x475AB14EL,0xD5AEC42BL,(-3L),0xB177BF85L,0x558BE347L,0L},{(-7L),0xA1CD4D61L,0xB2C3A522L,0xA7C387DFL,(-3L),(-8L),(-2L)},{0x2F18A8E4L,0xE772573EL,0xB177BF85L,(-1L),0xE6EC1E20L,(-3L),0x75593D57L}},{{0xEBEB721DL,(-7L),(-7L),0xF9B8DDCDL,0xE6EC1E20L,0xE772573EL,0xE772573EL},{(-3L),0xB2C3A522L,0xF9B8DDCDL,0xB2C3A522L,(-3L),(-7L),0xAB029CC0L},{(-8L),0xE6EC1E20L,(-6L),0L,0xB177BF85L,0L,0xBA17641BL},{(-8L),0L,0L,(-1L),0L,0xA7C387DFL,(-6L)},{(-8L),0L,0L,0x8C75A3B3L,0xE6EC1E20L,0xA7C387DFL,(-7L)},{(-7L),0xD5AEC42BL,0L,0xBA17641BL,1L,(-3L),0xA7C387DFL},{0xF8D342DBL,0L,0L,0xBA17641BL,(-6L),0xB2C3A522L,(-6L)}},{{0x8C75A3B3L,0x2F18A8E4L,0x2F18A8E4L,0x8C75A3B3L,0L,0xA1CD4D61L,0xD5AEC42BL},{0L,0x8C75A3B3L,0xB177BF85L,(-8L),0L,0L,(-1L)},{0x2F18A8E4L,0xA1CD4D61L,0xE6EC1E20L,0L,(-3L),(-7L),0xD5AEC42BL},{0xA7C387DFL,0xF9B8DDCDL,0x75593D57L,0L,(-3L),(-7L),(-6L)},{0xF9B8DDCDL,0xF8D342DBL,0x8C75A3B3L,(-6L),0L,(-1L),0xA7C387DFL},{(-1L),0x475AB14EL,0x8C75A3B3L,(-7L),(-1L),(-1L),(-7L)},{0x75593D57L,0xB177BF85L,0x75593D57L,0xEBEB721DL,(-8L),1L,(-3L)}}};
        uint64_t l_1502[7][9] = {{0xA8FE316A39006054LL,2UL,0xA8FE316A39006054LL,0xA9B183A474148122LL,0xA8FE316A39006054LL,2UL,0xA8FE316A39006054LL,0xA9B183A474148122LL,0xA8FE316A39006054LL},{0xBEA556C209C348BBLL,0xBEA556C209C348BBLL,3UL,0xC4C35322FA35D19CLL,0UL,0UL,0xC4C35322FA35D19CLL,3UL,0xBEA556C209C348BBLL},{0x8D61B712E15EBC8DLL,0xA9B183A474148122LL,1UL,0xA9B183A474148122LL,0x8D61B712E15EBC8DLL,0xA9B183A474148122LL,1UL,0xA9B183A474148122LL,0x8D61B712E15EBC8DLL},{0UL,0xC4C35322FA35D19CLL,3UL,0xBEA556C209C348BBLL,0xBEA556C209C348BBLL,3UL,0xC4C35322FA35D19CLL,0UL,0UL},{0xA8FE316A39006054LL,0xA9B183A474148122LL,0xA8FE316A39006054LL,2UL,0xA8FE316A39006054LL,0xA9B183A474148122LL,0xA8FE316A39006054LL,2UL,0xA8FE316A39006054LL},{0UL,0xBEA556C209C348BBLL,0xC4C35322FA35D19CLL,0xC4C35322FA35D19CLL,0xBEA556C209C348BBLL,0UL,3UL,3UL,0UL},{0x8D61B712E15EBC8DLL,2UL,1UL,2UL,0x8D61B712E15EBC8DLL,2UL,1UL,2UL,0x8D61B712E15EBC8DLL}};
        int32_t *l_1522 = &g_135;
        int i, j, k;
    }
    return (*l_1362);
}


/* ------------------------------------------ */
/* 
 * reads : g_1086 g_12 g_911
 * writes: g_135 g_1344 g_794
 */
static int8_t * func_22(int8_t * p_23, const uint32_t  p_24, uint8_t  p_25, uint32_t  p_26)
{ /* block id: 561 */
    int32_t *l_1329[7];
    float **l_1345 = (void*)0;
    int64_t l_1346 = 0xD2C7A6D947F5B318LL;
    uint8_t *l_1347 = &g_332;
    int i;
    for (i = 0; i < 7; i++)
        l_1329[i] = &g_895[0].f1;
    (*g_1086) = (safe_unary_minus_func_int16_t_s(p_24));
    (*g_911) = (((p_24 & (safe_mul_func_int16_t_s_s(p_24, (safe_sub_func_int64_t_s_s(p_26, ((safe_mod_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s(p_26, p_26)) != (safe_lshift_func_int8_t_s_s((*p_23), 0))), (safe_mod_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u(((0xCAEDL == (((g_1344[0][3][1] = &g_911) == l_1345) <= p_25)) == l_1346), p_26)), 254UL)))) == p_25)))))) , l_1347) != &p_25);
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_495 g_554.f1 g_535 g_582 g_283 g_680.f0 g_396 g_397 g_548 g_549 g_135 g_911 g_794 g_680.f1 g_800 g_12 g_933 g_312.f0 g_1026 g_239 g_2 g_1029 g_1033 g_237 g_895 g_1060 g_312.f1 g_1035 g_1086 g_554.f0 g_254 g_1113 g_312.f2 g_144 g_145 g_163 g_78 g_181 g_1151 g_1163 g_357 g_554 g_108 g_60 g_1205 g_328 g_1226 g_1229 g_895.f0 g_1261 g_151 g_1268 g_1302 g_1314
 * writes: g_163 g_12 g_495 g_554.f1 g_535 g_582 g_680.f0 g_135 g_549 g_794 g_933 g_239 g_1033 g_1035 g_181 g_254 g_60 g_332 g_78 g_554.f0 g_1206 g_562 g_1227 g_151 g_75
 */
static uint32_t  func_34(const uint64_t  p_35, int32_t * p_36, int32_t * p_37)
{ /* block id: 317 */
    uint32_t *l_848 = &g_163;
    int32_t l_849[2];
    int8_t *l_853 = &g_12;
    uint64_t *l_854 = &g_495;
    int16_t **l_882 = &g_633;
    uint32_t l_945[8][6] = {{7UL,0xA4D5815FL,4294967295UL,0x2B3A4963L,0x5FBED003L,0x6BDEDDBAL},{0x385F3E9CL,4UL,0x293357CAL,0x6BDEDDBAL,0x6BDEDDBAL,0x293357CAL},{0x9F0C58EBL,0x9F0C58EBL,4294967295UL,4294967295UL,1UL,7UL},{4294967295UL,0xE788D646L,1UL,1UL,0x78952AA9L,4294967295UL},{4UL,4294967295UL,1UL,0x293357CAL,0x9F0C58EBL,7UL},{0x6BB113EAL,0x293357CAL,4294967295UL,0x25679D4FL,4294967295UL,0x293357CAL},{0x25679D4FL,4294967295UL,0x293357CAL,0x6BB113EAL,0UL,0x6BDEDDBAL},{0x293357CAL,1UL,4294967295UL,4UL,1UL,0UL}};
    uint8_t ***l_1075 = &g_144;
    int64_t *l_1117 = (void*)0;
    int8_t **l_1204 = &l_853;
    int8_t ***l_1203 = &l_1204;
    float l_1295 = 0x0.5p-1;
    uint64_t l_1298 = 0xDB41A00DF13C271ALL;
    union U0 **l_1307 = &g_1227;
    uint16_t l_1313 = 1UL;
    uint8_t l_1325[2][8][1] = {{{3UL},{0x69L},{3UL},{0xECL},{3UL},{0x69L},{3UL},{0xECL}},{{3UL},{0x69L},{3UL},{0xECL},{3UL},{0x69L},{3UL},{0xECL}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_849[i] = 7L;
    if ((safe_sub_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((((*l_848) = 0xEE6F1DD5L) != l_849[1]), ((safe_rshift_func_uint16_t_u_s((~(l_849[1] , (l_849[0] | (-1L)))), 10)) == p_35))), (l_849[1] , ((*l_854) |= (p_35 | (((*l_853) = p_35) , l_849[1])))))), p_35)) >= 0x8BECL), 254UL)))
    { /* block id: 321 */
        int64_t l_861 = 0x168D88BD7C0B786FLL;
        int64_t l_881 = 0xDE0472C3938BC10CLL;
        const uint8_t *l_917[3];
        const uint8_t **l_916[4][10][6] = {{{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[2]},{(void*)0,&l_917[1],&l_917[1],(void*)0,&l_917[1],&l_917[1]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[2]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[0]},{&l_917[1],&l_917[2],(void*)0,&l_917[0],&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[1],&l_917[2],&l_917[1],&l_917[1]},{&l_917[1],&l_917[1],(void*)0,&l_917[1],&l_917[1],(void*)0},{&l_917[1],&l_917[1],&l_917[1],&l_917[2],&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[1],&l_917[0],&l_917[1],&l_917[1]},{&l_917[1],(void*)0,&l_917[1],&l_917[1],&l_917[1],&l_917[1]}},{{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],(void*)0},{&l_917[1],&l_917[1],(void*)0,(void*)0,&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1]},{&l_917[1],(void*)0,(void*)0,&l_917[1],&l_917[1],&l_917[0]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[2]},{(void*)0,&l_917[1],&l_917[1],(void*)0,&l_917[1],&l_917[1]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[2]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[0]},{&l_917[1],&l_917[2],(void*)0,&l_917[0],&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[1],&l_917[2],&l_917[1],&l_917[1]}},{{&l_917[1],&l_917[1],(void*)0,&l_917[1],&l_917[1],(void*)0},{&l_917[1],&l_917[1],&l_917[1],&l_917[2],&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[1],&l_917[0],&l_917[1],&l_917[1]},{&l_917[1],(void*)0,&l_917[1],&l_917[1],&l_917[1],&l_917[1]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],(void*)0},{&l_917[1],&l_917[1],(void*)0,(void*)0,&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1]},{&l_917[1],(void*)0,(void*)0,&l_917[1],&l_917[1],&l_917[0]},{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[2]},{(void*)0,&l_917[1],&l_917[1],(void*)0,&l_917[1],&l_917[1]}},{{&l_917[1],&l_917[1],&l_917[1],&l_917[1],&l_917[1],(void*)0},{&l_917[0],(void*)0,&l_917[1],&l_917[1],&l_917[0],&l_917[1]},{(void*)0,(void*)0,&l_917[1],&l_917[1],&l_917[0],&l_917[1]},{&l_917[1],(void*)0,(void*)0,(void*)0,&l_917[1],(void*)0},{&l_917[1],&l_917[2],&l_917[1],&l_917[1],&l_917[1],&l_917[1]},{&l_917[1],&l_917[1],&l_917[1],(void*)0,&l_917[1],&l_917[0]},{&l_917[1],(void*)0,&l_917[2],&l_917[1],&l_917[1],&l_917[1]},{(void*)0,&l_917[1],&l_917[2],&l_917[1],&l_917[1],&l_917[0]},{&l_917[0],&l_917[1],&l_917[1],(void*)0,&l_917[0],&l_917[1]},{(void*)0,&l_917[0],&l_917[1],&l_917[1],&l_917[0],(void*)0}}};
        const uint8_t ** const *l_915 = &l_916[1][0][3];
        uint16_t l_920 = 0UL;
        int32_t l_943 = 0x48342A8BL;
        int32_t l_944 = 1L;
        int32_t l_968 = (-1L);
        int32_t l_969 = 0xBE258DC7L;
        int32_t l_970 = 0xB6259CE1L;
        int32_t l_971 = 0x9A4D7621L;
        int32_t l_972 = 0xC8299343L;
        int32_t l_973 = (-1L);
        int32_t l_974 = 0xD761AEC3L;
        int32_t l_975 = 0L;
        int32_t l_976 = 0xA3F3E577L;
        int32_t l_977 = 0xEE112E20L;
        int32_t l_978 = 0x8C86F296L;
        int32_t l_979 = (-3L);
        float l_981[3][7][5] = {{{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5}},{{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)}},{{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5},{0xD.185C93p+45,(-0x10.5p-1),(-0x6.9p-1),(-0x6.9p-1),(-0x10.5p-1)},{0x8.9p-1,0x0.404A24p+5,0x5.BBEE1Ap+97,0x5.BBEE1Ap+97,0x0.404A24p+5}}};
        int32_t l_982[10][9] = {{0x86D895D4L,(-9L),0xCE03C578L,0xCE03C578L,(-9L),0x86D895D4L,0x9F4FD807L,0x8F4E1D89L,0L},{1L,(-1L),0xCE03C578L,3L,0x8F4E1D89L,(-7L),(-5L),(-5L),(-7L)},{0xCE03C578L,(-5L),0L,(-5L),0xCE03C578L,1L,0x9F4FD807L,1L,0x4E23795FL},{0x33203465L,(-5L),1L,0x4E23795FL,0xE9AB75EEL,0x4E23795FL,(-5L),0x4E23795FL,0xE9AB75EEL},{0x8F4E1D89L,0xE9AB75EEL,0xE9AB75EEL,0x8F4E1D89L,0xCE527E59L,0L,1L,(-5L),3L},{0x8F4E1D89L,(-1L),(-9L),0L,3L,0xCE527E59L,0xCE527E59L,3L,0L},{(-7L),0x4E23795FL,(-7L),(-9L),0xCE527E59L,(-5L),0x86D895D4L,0xCE03C578L,0L},{0x33203465L,(-7L),3L,1L,0xE9AB75EEL,0xCE03C578L,0xE9AB75EEL,1L,3L},{0L,0L,0x4E23795FL,(-9L),0x33203465L,(-7L),3L,1L,0xE9AB75EEL},{(-5L),0x86D895D4L,0xCE03C578L,0L,(-3L),(-3L),0L,0xCE03C578L,0x86D895D4L}};
        uint64_t l_983[10];
        int8_t ** const l_1024 = (void*)0;
        int32_t l_1066 = 0x989395EDL;
        int32_t l_1068 = 0xA0DA3876L;
        uint8_t l_1083[10] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
        int16_t *l_1124 = &g_254;
        int32_t *l_1164[1][6][3] = {{{&g_933,(void*)0,&g_135},{&l_849[1],&l_849[1],&g_135},{(void*)0,&g_933,(void*)0},{&g_1060.f1,&l_849[1],&g_1060.f1},{&g_1060.f1,(void*)0,&l_849[1]},{(void*)0,&g_1060.f1,&g_1060.f1}}};
        int16_t l_1200 = 0x51E0L;
        union U0 *l_1225 = &g_680;
        int32_t ****l_1247[2];
        float **l_1260 = &g_911;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_917[i] = &g_332;
        for (i = 0; i < 10; i++)
            l_983[i] = 1UL;
        for (i = 0; i < 2; i++)
            l_1247[i] = &g_477;
lbl_940:
        for (g_554.f1 = 0; (g_554.f1 != 17); ++g_554.f1)
        { /* block id: 324 */
            for (g_535 = 0; (g_535 == (-17)); --g_535)
            { /* block id: 327 */
                for (g_582 = (-26); (g_582 > 35); g_582 = safe_add_func_uint8_t_u_u(g_582, 3))
                { /* block id: 330 */
                    return g_283[1][1][0];
                }
            }
        }
        if (l_861)
        { /* block id: 335 */
            float *l_875 = &g_549;
            float **l_874[2][1][3];
            const int16_t *l_884 = &g_254;
            const int16_t * const *l_883 = &l_884;
            int32_t l_918 = 0x15B6E211L;
            int32_t *l_941 = &g_680.f1;
            int32_t *l_942[1][2];
            uint16_t *l_961 = &l_920;
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 3; k++)
                        l_874[i][j][k] = &l_875;
                }
            }
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_942[i][j] = &g_680.f1;
            }
            for (g_680.f0 = 0; (g_680.f0 < (-18)); g_680.f0--)
            { /* block id: 338 */
                uint32_t l_878 = 4294967295UL;
                int32_t *l_885 = &g_135;
                float **l_926 = &l_875;
                (*l_885) |= (!(!(((safe_lshift_func_int8_t_s_u((safe_rshift_func_uint16_t_u_s(p_35, (safe_mod_func_uint64_t_u_u((p_35 ^ ((safe_mod_func_int32_t_s_s(((void*)0 != l_874[0][0][2]), (safe_rshift_func_int8_t_s_s(l_878, (p_35 != ((((((*g_396) == (*g_396)) == (safe_mul_func_float_f_f(l_861, (*g_548)))) > l_881) , l_882) == l_883)))))) == l_849[1])), l_849[1])))), 2)) >= 3UL) | l_881)));
                for (g_554.f1 = 0; (g_554.f1 <= (-20)); g_554.f1 = safe_sub_func_uint8_t_u_u(g_554.f1, 7))
                { /* block id: 342 */
                    int16_t l_890[4][7][3] = {{{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL}},{{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L}},{{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL}},{{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L},{0xC543L,1L,0x866AL},{0x818DL,0x818DL,0x0916L}}};
                    int64_t *l_910 = &g_108;
                    uint64_t *l_919 = &g_81;
                    float **l_925 = &l_875;
                    int8_t *l_934 = &g_78;
                    int32_t **l_936 = (void*)0;
                    int32_t **l_937[5][2] = {{&g_181,&g_181},{&g_181,&g_181},{&g_181,&g_181},{&g_181,&l_885},{&g_181,&l_885}};
                    int i, j, k;
                }
                if (l_881)
                    goto lbl_940;
            }
            l_945[1][1]--;
            (*l_875) = (safe_add_func_float_f_f(((safe_add_func_float_f_f(((((*g_911) < p_35) == (0x6.766F6Ap-4 != ((safe_sub_func_float_f_f((((safe_mul_func_int8_t_s_s((*l_941), ((((~((safe_mul_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(l_861, 1UL)), ((*g_800) = p_35))) & 65527UL)) <= (((((*l_961) = ((l_849[1] &= g_12) || 0xE7B1L)) <= l_861) != 9UL) == 0xB43AL)) > p_35) & 2UL))) == l_944) , p_35), l_881)) >= (*g_911)))) == p_35), (*g_911))) >= l_945[1][1]), 0x5.2708B1p+8));
        }
        else
        { /* block id: 371 */
            uint8_t l_962 = 1UL;
            int32_t *l_965 = &g_933;
            int32_t *l_966 = &g_554.f1;
            int32_t *l_967[8][3][3] = {{{&g_2[1][0],&g_554.f1,&g_554.f1},{&g_2[1][0],&g_239[0],&l_849[1]},{&l_943,&g_554.f1,(void*)0}},{{&g_554.f1,&g_933,&g_933},{(void*)0,&g_895[0].f1,&l_944},{&l_849[1],&g_933,&g_554.f1}},{{&g_554.f1,&g_895[0].f1,&g_895[0].f1},{&g_554.f1,&g_933,&g_554.f1},{&l_944,&g_895[0].f1,&g_75}},{{&g_239[0],&g_933,&g_933},{(void*)0,&g_895[0].f1,&l_944},{&l_849[1],&g_933,&g_554.f1}},{{&g_554.f1,&g_895[0].f1,&g_895[0].f1},{&g_554.f1,&g_933,&g_554.f1},{&l_944,&g_895[0].f1,&g_75}},{{&g_239[0],&g_933,&g_933},{(void*)0,&g_895[0].f1,&l_944},{&l_849[1],&g_933,&g_554.f1}},{{&g_554.f1,&g_895[0].f1,&g_895[0].f1},{&g_554.f1,&g_933,&g_554.f1},{&l_944,&g_895[0].f1,&g_75}},{{&g_239[0],&g_933,&g_933},{(void*)0,&g_895[0].f1,&l_944},{&l_849[1],&g_933,&g_554.f1}}};
            int i, j, k;
            l_962--;
            l_983[1]++;
            (*g_911) = ((safe_mul_func_int16_t_s_s((p_35 && ((void*)0 != l_966)), (safe_div_func_int32_t_s_s((((*g_800) = (safe_mul_func_int16_t_s_s((l_979 == (*l_965)), (safe_rshift_func_uint16_t_u_u((*g_800), 9))))) , (safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((((safe_mod_func_int64_t_s_s((!((safe_lshift_func_int8_t_s_u((l_968 = ((void*)0 == &g_258)), 4)) >= (safe_add_func_uint8_t_u_u(((((*l_966) <= l_849[1]) != g_495) & 0x1CL), 0x1FL)))), p_35)) > 0xD292L) | l_972), 5)), (*l_965)))), (*l_966))))) , l_971);
            (*l_965) &= l_971;
        }
        for (l_970 = (-25); (l_970 != (-25)); l_970 = safe_add_func_uint16_t_u_u(l_970, 6))
        { /* block id: 381 */
            uint32_t l_1007 = 2UL;
            int8_t **l_1023 = &l_853;
            int32_t l_1112[5] = {1L,1L,1L,1L,1L};
            int i;
            --l_1007;
            (*g_1026) ^= (p_35 & (l_975 != ((+l_1007) >= (((((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int8_t_s_u((safe_add_func_uint32_t_u_u((((*l_853) = l_978) | ((safe_mul_func_uint8_t_u_u(l_849[1], (safe_add_func_int16_t_s_s(((((p_35 <= (((safe_lshift_func_uint8_t_u_s(p_35, 6)) ^ ((*l_848) = (g_312.f0 | (*g_800)))) > p_35)) ^ 0x08579C75L) < l_849[0]) | l_945[4][2]), p_35)))) >= p_35)), p_35)), p_35)), 1)) , l_1023) == l_1024) == l_945[4][1]) >= 0x4E57529DL))));
            for (g_535 = 18; (g_535 >= 18); --g_535)
            { /* block id: 388 */
                int64_t l_1036 = 0x83734D29E35DD1D1LL;
                uint8_t ***l_1080 = &g_144;
                const uint8_t ***l_1081 = &l_916[2][6][2];
                uint16_t *l_1109 = &g_1035[0];
                float *l_1110[8][9][3] = {{{&l_981[0][4][0],&l_981[0][0][3],&g_549},{(void*)0,(void*)0,&g_549},{&l_981[0][0][3],&l_981[0][4][0],&g_549},{&l_981[1][1][4],(void*)0,&l_981[1][1][4]},{&l_981[1][1][4],&l_981[0][0][3],(void*)0},{&l_981[0][0][3],&l_981[1][1][4],&l_981[1][1][4]},{(void*)0,&l_981[1][1][4],&g_549},{&l_981[0][4][0],&l_981[0][0][3],&g_549},{(void*)0,(void*)0,&g_549}},{{&l_981[0][0][3],&l_981[0][4][0],&g_549},{&l_981[1][1][4],(void*)0,&l_981[1][1][4]},{&l_981[1][1][4],&l_981[0][0][3],(void*)0},{&l_981[0][0][3],&l_981[1][1][4],&l_981[1][1][4]},{(void*)0,&l_981[1][1][4],&g_549},{&l_981[0][4][0],&l_981[0][0][3],&g_549},{(void*)0,(void*)0,&g_549},{&l_981[0][0][3],&l_981[0][4][0],&g_549},{&l_981[1][1][4],(void*)0,&l_981[1][1][4]}},{{&l_981[1][1][4],&l_981[0][0][3],(void*)0},{&l_981[0][0][3],&l_981[1][1][4],&l_981[1][1][4]},{(void*)0,&l_981[1][1][4],&g_549},{&l_981[0][4][0],&l_981[0][0][3],&g_549},{(void*)0,(void*)0,&g_549},{&l_981[0][0][3],&l_981[0][4][0],&g_549},{&l_981[1][1][4],(void*)0,&l_981[1][1][4]},{&l_981[1][1][4],&l_981[0][0][3],(void*)0},{&l_981[0][0][3],&l_981[1][1][4],&l_981[1][1][4]}},{{(void*)0,&l_981[1][1][4],&g_549},{&l_981[0][4][0],&l_981[0][0][3],&g_549},{(void*)0,(void*)0,&g_549},{&l_981[0][0][3],&l_981[0][4][0],&g_549},{&l_981[1][1][4],(void*)0,&l_981[1][1][4]},{&l_981[1][1][4],&l_981[0][0][3],(void*)0},{&l_981[0][0][3],&l_981[1][1][4],&l_981[1][1][4]},{(void*)0,&l_981[1][1][4],&g_549},{&l_981[0][4][0],&l_981[0][0][3],&g_549}},{{(void*)0,(void*)0,&g_549},{&l_981[0][0][3],&l_981[0][4][0],&g_549},{&l_981[1][1][4],(void*)0,&l_981[1][1][4]},{&l_981[1][1][4],&l_981[0][0][3],(void*)0},{&l_981[0][0][3],&l_981[1][1][4],&l_981[1][1][4]},{(void*)0,&l_981[0][4][0],&g_549},{&g_549,&l_981[1][1][4],(void*)0},{(void*)0,(void*)0,(void*)0},{&l_981[1][1][4],&g_549,&g_549}},{{&l_981[0][4][0],(void*)0,&l_981[0][4][0]},{&l_981[0][4][0],&l_981[1][1][4],(void*)0},{&l_981[1][1][4],&l_981[0][4][0],&l_981[0][4][0]},{(void*)0,&l_981[0][4][0],&g_549},{&g_549,&l_981[1][1][4],(void*)0},{(void*)0,(void*)0,(void*)0},{&l_981[1][1][4],&g_549,&g_549},{&l_981[0][4][0],(void*)0,&l_981[0][4][0]},{&l_981[0][4][0],&l_981[1][1][4],(void*)0}},{{&l_981[1][1][4],&l_981[0][4][0],&l_981[0][4][0]},{(void*)0,&l_981[0][4][0],&g_549},{&g_549,&l_981[1][1][4],(void*)0},{(void*)0,(void*)0,(void*)0},{&l_981[1][1][4],&g_549,&g_549},{&l_981[0][4][0],(void*)0,&l_981[0][4][0]},{&l_981[0][4][0],&l_981[1][1][4],(void*)0},{&l_981[1][1][4],&l_981[0][4][0],&l_981[0][4][0]},{(void*)0,&l_981[0][4][0],&g_549}},{{&g_549,&l_981[1][1][4],(void*)0},{(void*)0,(void*)0,(void*)0},{&l_981[1][1][4],&g_549,&g_549},{&l_981[0][4][0],(void*)0,&l_981[0][4][0]},{&l_981[0][4][0],&l_981[1][1][4],(void*)0},{&l_981[1][1][4],&l_981[0][4][0],&l_981[0][4][0]},{(void*)0,&l_981[0][4][0],&g_549},{&g_549,&l_981[1][1][4],(void*)0},{(void*)0,(void*)0,(void*)0}}};
                int32_t l_1111 = (-8L);
                int64_t **l_1114 = &g_561[4][2];
                int64_t **l_1115 = &g_561[2][1];
                int64_t **l_1116[9][6][4] = {{{&g_561[4][2],(void*)0,&g_561[3][1],&g_561[2][0]},{&g_561[4][2],&g_561[3][1],&g_561[3][1],&g_561[2][0]},{&g_561[0][1],(void*)0,&g_561[4][2],&g_561[3][1]},{&g_561[0][2],&g_561[4][2],&g_561[3][1],&g_561[0][2]},{&g_561[3][1],&g_561[4][2],&g_561[3][1],&g_561[3][1]},{&g_561[2][0],(void*)0,&g_561[2][0],&g_561[2][0]}},{{&g_561[3][1],&g_561[3][1],&g_561[4][2],&g_561[2][0]},{&g_561[0][2],(void*)0,&g_561[0][2],&g_561[3][1]},{&g_561[0][1],&g_561[4][2],&g_561[4][2],&g_561[0][2]},{&g_561[4][2],&g_561[4][2],&g_561[2][0],&g_561[3][1]},{&g_561[4][2],(void*)0,&g_561[3][1],&g_561[2][0]},{&g_561[4][2],&g_561[3][1],&g_561[3][1],&g_561[2][0]}},{{&g_561[0][1],(void*)0,&g_561[4][2],&g_561[3][1]},{&g_561[0][2],&g_561[4][2],&g_561[3][1],&g_561[0][2]},{&g_561[3][1],&g_561[4][2],&g_561[3][1],&g_561[3][1]},{&g_561[2][0],(void*)0,&g_561[2][0],&g_561[2][0]},{&g_561[3][1],&g_561[3][1],&g_561[4][2],&g_561[2][0]},{&g_561[0][2],(void*)0,&g_561[0][2],&g_561[3][1]}},{{&g_561[0][1],&g_561[4][2],&g_561[4][2],&g_561[0][2]},{&g_561[4][2],&g_561[4][2],&g_561[2][0],&g_561[3][1]},{&g_561[4][2],(void*)0,&g_561[3][1],&g_561[2][0]},{&g_561[4][2],&g_561[3][1],&g_561[3][1],&g_561[2][0]},{&g_561[0][1],(void*)0,&g_561[4][2],&g_561[3][1]},{&g_561[0][2],&g_561[4][2],&g_561[3][1],&g_561[0][2]}},{{&g_561[3][1],&g_561[4][2],&g_561[4][2],&g_561[0][2]},{&g_561[0][2],&g_561[3][1],&g_561[4][2],&g_561[4][2]},{&g_561[4][2],&g_561[4][2],&g_561[4][2],&g_561[4][2]},{&g_561[4][2],&g_561[3][1],&g_561[2][0],&g_561[0][2]},{&g_561[4][2],&g_561[0][1],&g_561[4][2],&g_561[2][0]},{&g_561[2][0],&g_561[0][1],&g_561[4][2],&g_561[0][2]}},{{&g_561[0][1],&g_561[3][1],&g_561[4][2],&g_561[4][2]},{&g_561[2][0],&g_561[4][2],&g_561[0][2],&g_561[4][2]},{&g_561[4][2],&g_561[3][1],&g_561[3][1],&g_561[0][2]},{&g_561[4][2],&g_561[0][1],&g_561[0][2],&g_561[2][0]},{&g_561[4][2],&g_561[0][1],&g_561[4][2],&g_561[0][2]},{&g_561[0][2],&g_561[3][1],&g_561[4][2],&g_561[4][2]}},{{&g_561[4][2],&g_561[4][2],&g_561[4][2],&g_561[4][2]},{&g_561[4][2],&g_561[3][1],&g_561[2][0],&g_561[0][2]},{&g_561[4][2],&g_561[0][1],&g_561[4][2],&g_561[2][0]},{&g_561[2][0],&g_561[0][1],&g_561[4][2],&g_561[0][2]},{&g_561[0][1],&g_561[3][1],&g_561[4][2],&g_561[4][2]},{&g_561[2][0],&g_561[4][2],&g_561[0][2],&g_561[4][2]}},{{&g_561[4][2],&g_561[3][1],&g_561[3][1],&g_561[0][2]},{&g_561[4][2],&g_561[0][1],&g_561[0][2],&g_561[2][0]},{&g_561[4][2],&g_561[0][1],&g_561[4][2],&g_561[0][2]},{&g_561[0][2],&g_561[3][1],&g_561[4][2],&g_561[4][2]},{&g_561[4][2],&g_561[4][2],&g_561[4][2],&g_561[4][2]},{&g_561[4][2],&g_561[3][1],&g_561[2][0],&g_561[0][2]}},{{&g_561[4][2],&g_561[0][1],&g_561[4][2],&g_561[2][0]},{&g_561[2][0],&g_561[0][1],&g_561[4][2],&g_561[0][2]},{&g_561[0][1],&g_561[3][1],&g_561[4][2],&g_561[4][2]},{&g_561[2][0],&g_561[4][2],&g_561[0][2],&g_561[4][2]},{&g_561[4][2],&g_561[3][1],&g_561[3][1],&g_561[0][2]},{&g_561[4][2],&g_561[0][1],&g_561[0][2],&g_561[2][0]}}};
                int32_t *l_1118 = &l_849[1];
                int i, j, k;
                for (g_582 = 0; (g_582 <= 0); g_582 += 1)
                { /* block id: 391 */
                    uint16_t *l_1030 = &l_920;
                    uint16_t *l_1031 = (void*)0;
                    uint16_t *l_1032 = &g_1033;
                    uint16_t *l_1034[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_1034[i] = &g_1035[0];
                    g_239[g_582] ^= l_979;
                    if (((g_2[1][0] == (g_1029 , (l_849[0] = (g_1035[0] = ((*l_1032) &= ((*l_1030) ^= 0x6EB6L)))))) < p_35))
                    { /* block id: 397 */
                        const int32_t l_1047 = (-10L);
                        int32_t **l_1048 = &g_181;
                        (*l_1048) = func_40(((l_975 != l_1036) , (safe_sub_func_int64_t_s_s(l_944, ((((*g_911) = (0xC.23B76Ep+12 >= (safe_sub_func_float_f_f((safe_div_func_float_f_f(((((((g_237 , (safe_div_func_float_f_f(l_1007, (safe_mul_func_float_f_f((g_895[0] , (((*g_396) != l_853) , 0x6.B8FE78p-91)), (-0x6.0p+1)))))) != (*g_911)) , (-1L)) == 3L) , 0xE.D82066p+41) <= l_979), 0x1.6p-1)), p_35)))) , (void*)0) != (void*)0)))), l_1047);
                    }
                    else
                    { /* block id: 400 */
                        int64_t *l_1061 = &l_881;
                        int32_t l_1067 = 1L;
                        uint8_t ****l_1076 = &g_258;
                        uint8_t ****l_1077 = (void*)0;
                        uint8_t ****l_1078 = (void*)0;
                        uint8_t ****l_1079 = &g_258;
                        const uint8_t ****l_1082 = &l_1081;
                        int32_t *l_1085 = (void*)0;
                        p_37 = p_36;
                        l_849[1] &= (safe_rshift_func_int16_t_s_u(((safe_mod_func_uint8_t_u_u(p_35, (safe_add_func_uint32_t_u_u((!((safe_mul_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((g_1060 , ((p_35 <= (((((*l_1061) = p_35) , 1L) <= ((((p_35 , (safe_mod_func_uint16_t_u_u(0x9892L, l_1036))) , (safe_lshift_func_uint16_t_u_u((l_1066 , l_1067), 1))) & l_1067) ^ l_1068)) , 0xBE85L)) < l_945[1][1])), l_1007)) ^ g_312.f1), 0xB7L)) < l_1007)), l_945[1][1])))) == g_1035[0]), 4));
                        l_1083[1] = (((safe_add_func_uint16_t_u_u(l_1067, (l_1007 | (((*l_1030) = ((~18446744073709551615UL) < (!l_945[6][0]))) > (l_849[1] &= ((*l_1032) = l_1036)))))) , (l_1075 = (l_1080 = l_1075))) == ((*l_1082) = l_1081));
                        (*g_1086) &= l_849[1];
                    }
                    return g_554.f1;
                }
                l_943 = (safe_mul_func_float_f_f(((*g_911) = 0x5.718B06p+74), (l_1112[1] = (safe_sub_func_float_f_f((safe_add_func_float_f_f(((((l_1111 = (l_849[0] = (safe_sub_func_float_f_f((safe_add_func_float_f_f((*g_548), (((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f(p_35, 0xD.692359p-70)), (safe_mul_func_float_f_f(g_554.f0, ((0x2.81C67Ep-37 == (safe_sub_func_float_f_f(0xD.D1D61Ap-57, (safe_sub_func_float_f_f((((p_35 >= (l_1109 != (void*)0)) > 0x2.6EA721p+84) < 0xC.1A6473p+21), p_35))))) <= p_35))))), (-0x3.1p-1))) >= l_1036) < p_35))), g_495)))) > p_35) >= p_35) <= 0xC.D42519p-32), 0x0.E7058Fp-18)), g_254)))));
                (*l_1118) |= ((g_1113[0] , l_854) == (l_1117 = l_854));
                if (l_1083[1])
                    continue;
            }
        }
        for (g_254 = (-25); (g_254 != (-30)); g_254 = safe_sub_func_int32_t_s_s(g_254, 1))
        { /* block id: 427 */
            int16_t *l_1126 = &g_1127;
            int32_t l_1128 = (-1L);
            int32_t ***l_1156 = (void*)0;
            int8_t ***l_1217 = &l_1204;
            int32_t l_1222 = (-1L);
            uint16_t **l_1228 = (void*)0;
            uint8_t l_1264 = 0x80L;
            float l_1273[9][10] = {{0x5.4E2143p+15,0x1.Bp-1,(-0x3.4p+1),(-0x1.Fp-1),(-0x1.9p-1),(-0x1.Dp-1),(-0x1.9p-1),(-0x1.Fp-1),(-0x3.4p+1),0x1.Bp-1},{0x6.4F51C0p-2,(-0x1.8p-1),(-0x1.Dp-1),0x1.Bp-1,(-0x1.9p-1),0x2.941484p-0,0x2.941484p-0,(-0x1.9p-1),0x1.Bp-1,(-0x1.Dp-1)},{(-0x1.9p-1),(-0x1.9p-1),0x5.4E2143p+15,0x6.4F51C0p-2,0x1.7p-1,0x2.941484p-0,(-0x3.4p+1),0x2.941484p-0,0x1.7p-1,0x6.4F51C0p-2},{0x6.4F51C0p-2,(-0x1.Cp-1),0x6.4F51C0p-2,0x2.941484p-0,(-0x1.Fp-1),(-0x1.Dp-1),(-0x3.4p+1),(-0x3.4p+1),(-0x1.Dp-1),(-0x1.Fp-1)},{0x5.4E2143p+15,(-0x1.9p-1),(-0x1.9p-1),0x5.4E2143p+15,0x6.4F51C0p-2,0x1.7p-1,0x2.941484p-0,(-0x3.4p+1),0x2.941484p-0,0x1.7p-1},{(-0x1.Dp-1),(-0x1.8p-1),0x6.4F51C0p-2,(-0x1.8p-1),(-0x1.Dp-1),0x1.Bp-1,(-0x1.9p-1),0x2.941484p-0,0x2.941484p-0,(-0x1.9p-1)},{(-0x3.4p+1),0x1.Bp-1,0x5.4E2143p+15,0x5.4E2143p+15,0x1.Bp-1,(-0x3.4p+1),(-0x1.Fp-1),(-0x1.9p-1),(-0x1.Dp-1),(-0x1.9p-1)},{(-0x1.8p-1),0x5.4E2143p+15,(-0x1.Dp-1),(-0x1.8p-1),(-0x3.4p+1),(-0x1.9p-1),(-0x1.Cp-1),0x1.7p-1,(-0x1.Dp-1),(-0x1.Dp-1)},{(-0x1.Cp-1),(-0x1.Dp-1),0x6.4F51C0p-2,0x1.Bp-1,0x1.Bp-1,0x6.4F51C0p-2,(-0x1.Dp-1),(-0x1.Cp-1),0x5.4E2143p+15,0x1.7p-1}};
            int i, j;
            for (g_12 = 24; (g_12 <= (-24)); g_12 = safe_sub_func_uint16_t_u_u(g_12, 1))
            { /* block id: 430 */
                int16_t **l_1125[7];
                int32_t *l_1129 = &l_849[1];
                uint64_t l_1144 = 0UL;
                uint8_t *l_1145[1][9][5] = {{{&g_332,&l_1083[0],&l_1083[1],(void*)0,&g_332},{&l_1083[1],(void*)0,&l_1083[1],&l_1083[4],&l_1083[3]},{&l_1083[4],&l_1083[0],&g_332,&g_332,&l_1083[0]},{&g_332,&l_1083[1],&l_1083[1],&g_332,&l_1083[1]},{(void*)0,&l_1083[1],&l_1083[1],&l_1083[4],(void*)0},{&l_1083[1],&l_1083[1],&l_1083[3],(void*)0,&g_332},{(void*)0,&l_1083[1],&g_332,&l_1083[1],(void*)0},{&g_332,(void*)0,&g_332,&l_1083[3],&l_1083[1]},{&l_1083[4],&l_1083[5],&l_1083[3],&l_1083[1],&l_1083[1]}}};
                uint32_t *l_1146 = &l_945[1][1];
                int i, j, k;
                for (i = 0; i < 7; i++)
                    l_1125[i] = &l_1124;
                (*g_1026) = (g_1029 , ((*l_1129) = ((l_1128 = (safe_unary_minus_func_uint8_t_u((((l_1126 = l_1124) == (void*)0) <= p_35)))) , 0L)));
                (*l_1129) = (safe_sub_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((*l_1129), (((safe_unary_minus_func_int8_t_s(((p_35 , (+((g_332 = ((g_312.f2 | (safe_sub_func_uint8_t_u_u((((safe_add_func_int8_t_s_s(((((*l_848) ^= ((((***l_1075) = (safe_add_func_int32_t_s_s(0x586FF5C1L, p_35))) || (safe_sub_func_int16_t_s_s((&g_81 == &g_495), (&g_535 == (p_35 , (void*)0))))) > p_35)) <= 0x6253203BL) , p_35), l_1144)) && (-1L)) , 0xFCL), l_849[1]))) && l_1128)) || 0xF9L))) <= g_78))) < l_1128) && l_945[1][1]))), 0xA18B78D9E0FF1CC8LL));
                if ((p_35 , ((((*l_1146) = ((*l_848) = 4294967292UL)) && l_944) <= p_35)))
                { /* block id: 441 */
                    for (l_972 = 0; (l_972 >= (-20)); l_972--)
                    { /* block id: 444 */
                        int32_t **l_1149 = &g_181;
                        (*l_1149) = &l_849[0];
                        (*g_181) = (0UL | p_35);
                    }
                    return l_1128;
                }
                else
                { /* block id: 449 */
                    int64_t l_1162 = 0L;
                    for (g_1033 = 0; (g_1033 <= 8); g_1033 += 1)
                    { /* block id: 452 */
                        int i, j;
                        (*g_1151) = &l_982[g_1033][g_1033];
                        (*g_1163) = ((*g_911) = (safe_div_func_float_f_f((safe_sub_func_float_f_f((((void*)0 == l_1156) , l_849[1]), ((&g_562 != p_36) == (safe_add_func_float_f_f((safe_div_func_float_f_f((0x2.C1B6CFp+75 >= ((!((((-0x1.0p+1) > (*g_548)) < (*g_911)) > l_982[5][5])) < l_1162)), 0x0.Fp-1)), (*g_911)))))), p_35)));
                        l_1164[0][3][1] = (*g_357);
                    }
                }
            }
            for (g_12 = (-21); (g_12 != (-3)); g_12 = safe_add_func_uint32_t_u_u(g_12, 6))
            { /* block id: 462 */
                int32_t ****l_1172 = &l_1156;
                int32_t **l_1189 = &g_181;
                int64_t l_1211 = 0x75C14286173AA59DLL;
                uint32_t l_1246 = 0xB4EC0B0AL;
                int32_t ****l_1250 = &g_477;
                int32_t l_1271 = (-1L);
                int32_t l_1272 = 9L;
                int32_t l_1274[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_1274[i] = 5L;
                for (l_944 = 0; (l_944 <= 28); ++l_944)
                { /* block id: 465 */
                    int32_t ****l_1173 = &l_1156;
                    for (l_1128 = 0; (l_1128 <= 0); l_1128 += 1)
                    { /* block id: 468 */
                        return p_35;
                    }
                    for (l_974 = 24; (l_974 != 8); l_974 = safe_sub_func_uint32_t_u_u(l_974, 4))
                    { /* block id: 473 */
                        int32_t ****l_1171 = (void*)0;
                        int8_t *l_1186 = &g_78;
                        (*g_911) = ((-0x1.Bp+1) == (((l_1173 = (l_1172 = l_1171)) != ((safe_add_func_uint16_t_u_u(l_945[1][1], (safe_add_func_int8_t_s_s(((*l_1186) = (((*g_1026) < ((*l_848)++)) ^ ((~(safe_add_func_uint32_t_u_u(l_945[1][1], ((~0xDAEFL) , l_945[1][1])))) | (((*l_854) = (safe_rshift_func_uint16_t_u_s((g_239[0] , (*g_800)), 0))) >= 1L)))), 0x6BL)))) , l_1171)) > 0x3.50624Ap-84));
                        l_1200 &= (safe_rshift_func_uint16_t_u_u((((void*)0 != l_1189) , ((*g_800) = (safe_rshift_func_int16_t_s_s((((0xB5DA52DCC1B803B4LL & p_35) == (g_554 , (*g_800))) && (safe_mod_func_uint64_t_u_u((safe_mul_func_int8_t_s_s(((p_35 <= (safe_mod_func_int16_t_s_s((((safe_div_func_uint64_t_u_u(g_108, ((**g_144) | (*g_145)))) , (void*)0) == l_1126), (-9L)))) <= 0xC7BAL), p_35)), 0xA3D0EE696D5A28ADLL))), p_35)))), p_35));
                    }
                }
                for (g_554.f0 = 0; (g_554.f0 == 16); g_554.f0++)
                { /* block id: 486 */
                    int32_t l_1220 = 0xACFF773FL;
                    uint64_t l_1221[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1221[i] = 1UL;
                    (*g_1205) = l_1203;
                    l_1222 &= ((safe_add_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((l_1211 , (((4294967286UL != ((~(((**g_144) = ((((g_562 = p_35) == ((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_u((((void*)0 != l_1217) & 0L), (*g_800))) ^ (safe_rshift_func_int8_t_s_s((p_35 > p_35), 1))), l_1220)) <= p_35)) , l_1220) , l_1221[1])) , g_328)) , g_12)) > p_35) , p_35)), (*g_800))), l_1128)) != p_35);
                    for (g_60 = (-27); (g_60 == 36); ++g_60)
                    { /* block id: 493 */
                        (*g_1226) = l_1225;
                        (*g_1229) = ((*g_911) = ((void*)0 != l_1228));
                    }
                    return l_1220;
                }
                if ((((*g_145)++) & p_35))
                { /* block id: 501 */
                    uint16_t l_1241 = 0x76EFL;
                    int32_t l_1242 = 0xC966BF20L;
                    const uint64_t l_1243 = 0x15A23CB36DDAD8A0LL;
                    uint64_t *l_1257 = &l_983[1];
                    float **l_1259[4];
                    float ***l_1258[1][5][10] = {{{&l_1259[1],&l_1259[3],&l_1259[1],&l_1259[3],&l_1259[1],&l_1259[2],&l_1259[2],&l_1259[1],&l_1259[3],&l_1259[1]},{&l_1259[1],&l_1259[1],&l_1259[1],&l_1259[1],(void*)0,&l_1259[1],&l_1259[1],&l_1259[1],&l_1259[1],&l_1259[1]},{&l_1259[3],&l_1259[1],&l_1259[2],&l_1259[2],&l_1259[1],&l_1259[3],&l_1259[1],&l_1259[3],&l_1259[1],&l_1259[2]},{(void*)0,&l_1259[1],(void*)0,&l_1259[2],&l_1259[1],&l_1259[1],&l_1259[2],(void*)0,&l_1259[1],(void*)0},{(void*)0,&l_1259[3],&l_1259[1],&l_1259[1],&l_1259[1],&l_1259[3],(void*)0,(void*)0,&l_1259[3],&l_1259[1]}}};
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_1259[i] = &g_911;
                    (*l_1189) = func_40((!((safe_mul_func_uint8_t_u_u((p_35 > ((65534UL && 0xDF3FL) ^ g_328)), 0x19L)) != ((safe_mod_func_int16_t_s_s((p_35 == (safe_mod_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((0xC9608F1CAF02F229LL != (((l_1242 &= l_1241) , (void*)0) != (**l_1203))), (*g_800))), g_895[0].f0))), 0x8618L)) & l_945[1][1]))), l_1243);
                    for (l_978 = 0; (l_978 < (-13)); l_978 = safe_sub_func_uint32_t_u_u(l_978, 9))
                    { /* block id: 506 */
                        int32_t *****l_1248 = (void*)0;
                        int32_t *****l_1249 = &l_1247[1];
                        l_1246 |= 0xE3C3617AL;
                        l_1250 = ((*l_1249) = l_1247[1]);
                    }
                    if (l_945[4][4])
                        continue;
                    if ((65529UL || ((((~(!(((((safe_add_func_int8_t_s_s((g_312.f2 && ((*l_848) = p_35)), (p_35 , 250UL))) && ((**g_144) = 0x0FL)) , ((*l_1257) = ((*l_854) = 0xF266DDE5AC822457LL))) <= ((l_1260 = &g_911) == &g_911)) > (*g_800)))) ^ g_1261) & 3L) <= 0L)))
                    { /* block id: 517 */
                        l_1242 ^= (safe_lshift_func_uint8_t_u_s((*g_145), 6));
                        return l_1242;
                    }
                    else
                    { /* block id: 520 */
                        (**l_1260) = (*g_1229);
                        return l_1241;
                    }
                }
                else
                { /* block id: 524 */
                    uint8_t l_1267 = 0x80L;
                    int32_t l_1269 = 0xD2780E90L;
                    int32_t l_1270 = 5L;
                    float l_1281 = 0xC.DC99E1p-6;
                    ++l_1264;
                    if (l_1267)
                    { /* block id: 526 */
                        uint8_t l_1275 = 0x15L;
                        (*l_1189) = p_36;
                        if (g_1268)
                            break;
                        l_1275++;
                    }
                    else
                    { /* block id: 530 */
                        uint32_t l_1278[7][7][2] = {{{0x837162D2L,8UL},{4294967295UL,0xA059D9EBL},{0xC38F4C7CL,0UL},{1UL,4294967295UL},{0x6EC31318L,0x6EC31318L},{0xC4CD68F6L,0xEF5721ABL},{8UL,0x3AAE548BL}},{{0x6209BAB9L,8UL},{4294967292UL,0x6209BAB9L},{4UL,1UL},{4UL,0x6209BAB9L},{4294967292UL,8UL},{0x6209BAB9L,0x3AAE548BL},{8UL,0xEF5721ABL}},{{0xC4CD68F6L,0x6EC31318L},{0x6EC31318L,4294967295UL},{1UL,0UL},{0xC38F4C7CL,0xA059D9EBL},{4294967295UL,8UL},{0x837162D2L,9UL},{0x87825FB8L,0x960F7275L}},{{0xEF5721ABL,0xE3B04A3BL},{4294967290UL,0xF210436BL},{4294967287UL,0x837162D2L},{0x93B1FA2DL,4294967290UL},{0x47319AC3L,4294967292UL},{1UL,4294967295UL},{0UL,0x75CC3B27L}},{{8UL,0x75CC3B27L},{0UL,4294967295UL},{1UL,4294967292UL},{0x47319AC3L,4294967290UL},{0x93B1FA2DL,0x837162D2L},{4294967287UL,0xF210436BL},{4294967290UL,0xE3B04A3BL}},{{0xEF5721ABL,0x960F7275L},{0x87825FB8L,9UL},{0x837162D2L,8UL},{4294967295UL,0xA059D9EBL},{4294967290UL,0x6209BAB9L},{9UL,0x6EC31318L},{0xC38F4C7CL,0xC38F4C7CL}},{{8UL,0UL},{0xE3B04A3BL,0UL},{0xA059D9EBL,4294967295UL},{4294967294UL,0xA059D9EBL},{0xF210436BL,9UL},{0xF210436BL,0xA059D9EBL},{4294967294UL,4294967295UL}}};
                        int32_t l_1280 = 0L;
                        int i, j, k;
                        l_1278[2][0][1] = p_35;
                        l_1280 = (g_495 ^ (~((*g_548) , l_1269)));
                        return p_35;
                    }
                    l_1281 = ((*g_548) == l_849[1]);
                    l_1269 ^= (((l_1128 = ((**g_144) | 249UL)) , l_945[1][1]) >= (safe_div_func_uint8_t_u_u(p_35, 1UL)));
                }
                for (l_969 = 0; (l_969 != 8); ++l_969)
                { /* block id: 541 */
                    float l_1291[9];
                    int32_t l_1292 = 0x2126677AL;
                    int32_t l_1293 = 0xC2CEEB3AL;
                    int32_t l_1294 = 0xEABCC723L;
                    int32_t l_1296 = 0xEEE2050FL;
                    int32_t l_1297 = (-4L);
                    int i;
                    for (i = 0; i < 9; i++)
                        l_1291[i] = 0xF.71FC7Ap+6;
                    if (p_35)
                        break;
                    for (g_535 = 0; (g_535 > 28); ++g_535)
                    { /* block id: 545 */
                        int8_t **l_1288[10] = {&g_397[0][6][7],&l_853,&g_397[0][6][7],&l_853,&g_397[0][6][7],&l_853,&g_397[0][6][7],&l_853,&g_397[0][6][7],&l_853};
                        int i;
                        (*l_1217) = l_1288[0];
                        (*g_911) = 0x1.Ep+1;
                        (**l_1260) = (safe_add_func_float_f_f((-0x8.2p-1), p_35));
                    }
                    l_1298++;
                }
            }
        }
    }
    else
    { /* block id: 554 */
        uint16_t l_1312 = 0xF974L;
        int16_t *l_1315 = (void*)0;
        int16_t *l_1316 = &g_254;
        float l_1317 = 0x0.709192p+1;
        int32_t *l_1318 = &g_75;
        int32_t *l_1319 = &g_75;
        int32_t l_1320 = 0x14712F39L;
        int32_t *l_1321 = (void*)0;
        int32_t *l_1322 = &l_849[1];
        int32_t *l_1323 = &g_239[0];
        int32_t *l_1324 = &g_1060.f1;
        (*l_1318) = (!(g_1302 , (((0x4015D6E6L == (((*l_1316) = (((safe_sub_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u(0x90L, 7)), ((***l_1075) |= (l_1307 != ((safe_add_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u(((l_849[1] || l_945[1][1]) , (((l_1312 , l_1313) , 1L) >= 0x1DL)), 0xC6L)), g_1314)) , (void*)0))))) , g_1035[0]) != l_849[1])) < l_945[4][1])) & 18446744073709551611UL) >= 250UL)));
        ++l_1325[1][1][0];
    }
    return p_35;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_40(uint32_t  p_41, const uint32_t  p_42)
{ /* block id: 315 */
    int32_t *l_839 = (void*)0;
    return l_839;
}


/* ------------------------------------------ */
/* 
 * reads : g_495 g_239
 * writes: g_495 g_239
 */
static const uint32_t  func_43(int16_t  p_44, int32_t ** p_45, uint16_t  p_46, const int32_t ** p_47)
{ /* block id: 306 */
    int32_t *l_836 = &g_239[0];
    l_836 = l_836;
    for (g_495 = 0; (g_495 < 48); ++g_495)
    { /* block id: 310 */
        (*l_836) = (&g_633 == (void*)0);
        (*l_836) = (*l_836);
    }
    return (*l_836);
}


/* ------------------------------------------ */
/* 
 * reads : g_81 g_2 g_75 g_78 g_203 g_180 g_163 g_181 g_135 g_357 g_328 g_258 g_144 g_145 g_60 g_312.f2 g_313.f1 g_396 g_239 g_313 g_332 g_495 g_477 g_254 g_108 g_312.f1 g_548 g_554 g_562 g_549 g_582 g_12 g_633 g_680 g_681 g_680.f0 g_535 g_680.f1 g_313.f2 g_554.f1 g_798 g_799 g_800 g_237 g_312.f0
 * writes: g_75 g_78 g_81 g_151 g_135 g_181 g_258 g_12 g_254 g_108 g_60 g_239 g_477 g_495 g_332 g_549 g_561 g_163 g_562 g_582 g_633 g_493 g_237 g_799 g_794
 */
static int32_t ** func_49(uint8_t  p_50, int32_t * p_51, int16_t  p_52, uint32_t  p_53, uint64_t  p_54)
{ /* block id: 8 */
    uint8_t *l_62 = &g_60;
    uint8_t **l_61 = &l_62;
    int32_t l_63 = 8L;
    int32_t *l_74 = &g_75;
    uint64_t l_79 = 18446744073709551615UL;
    uint64_t *l_80[4] = {&g_81,&g_81,&g_81,&g_81};
    uint8_t l_96[5] = {255UL,255UL,255UL,255UL,255UL};
    int16_t l_106[7][5] = {{1L,(-6L),1L,0x9E7DL,7L},{7L,(-6L),(-5L),1L,(-8L)},{(-2L),3L,7L,1L,7L},{(-1L),(-8L),(-5L),(-8L),(-1L)},{0xA751L,1L,1L,(-8L),1L},{(-5L),0xA751L,0x9E7DL,1L,(-6L)},{(-5L),1L,1L,1L,1L}};
    int32_t l_109 = 0x4C941E8EL;
    int16_t l_110 = (-9L);
    int32_t l_284 = 1L;
    int32_t l_289[1];
    int8_t *l_347 = &g_12;
    int8_t **l_346 = &l_347;
    uint8_t ****l_432 = &g_258;
    int32_t ***l_478[2];
    int8_t l_491 = (-1L);
    uint32_t l_536 = 0x3EB7830CL;
    int16_t l_563 = 0x9FADL;
    uint8_t l_614 = 0x38L;
    uint16_t *l_651 = &g_582;
    uint64_t l_671 = 18446744073709551606UL;
    int32_t **l_706 = (void*)0;
    int64_t l_743 = 0x90FCFD14D2385EB6LL;
    uint16_t l_748 = 65533UL;
    const uint32_t l_779 = 0UL;
    const float l_810 = 0x8.5580BDp-96;
    int i, j;
    for (i = 0; i < 1; i++)
        l_289[i] = 0x31E8423BL;
    for (i = 0; i < 2; i++)
        l_478[i] = &g_180[6];
    if (((((*l_61) = &g_60) == &g_60) , (l_63 , ((safe_add_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mod_func_int32_t_s_s((safe_mul_func_int16_t_s_s((((g_81 |= ((safe_mod_func_uint32_t_u_u(l_63, ((*l_74) = 0xD0E131A3L))) <= (safe_div_func_int16_t_s_s(((g_78 = p_52) ^ p_54), l_79)))) ^ g_2[1][0]) , (*l_74)), g_2[1][0])), g_2[1][0])) != 0xA897151158E58409LL), p_52)), (*p_51))) & l_63))))
    { /* block id: 13 */
        int8_t l_87 = (-10L);
        uint64_t *l_116 = (void*)0;
        int32_t l_136 = 0L;
        uint16_t l_137 = 0x8D20L;
        int32_t **l_138 = (void*)0;
        int32_t l_286 = 0x9824FAF9L;
        int32_t l_287 = 0x893A8870L;
        int32_t l_288[1];
        uint16_t l_308 = 0x8EC3L;
        float l_351 = (-0x10.1p-1);
        int i;
        for (i = 0; i < 1; i++)
            l_288[i] = 0x3230D2C0L;
        for (g_78 = 0; (g_78 <= 3); g_78 += 1)
        { /* block id: 16 */
            int32_t *l_97 = &l_63;
            int64_t *l_107[9] = {(void*)0,&g_108,&g_108,(void*)0,&g_108,&g_108,(void*)0,&g_108,&g_108};
            int16_t *l_153[9][5][3] = {{{&l_106[5][3],&l_106[6][1],&l_106[6][1]},{(void*)0,&l_110,&l_106[1][2]},{&l_106[1][1],&l_106[3][4],(void*)0},{&l_110,&l_106[4][0],(void*)0},{&l_106[2][3],&l_110,(void*)0}},{{&l_110,&l_106[4][0],&l_106[4][4]},{&l_106[4][0],&l_106[3][4],&l_110},{&l_106[4][0],&l_110,&l_106[3][0]},{&l_110,&l_106[6][1],&l_110},{&l_106[1][1],&l_106[2][3],&l_106[2][1]}},{{&l_110,(void*)0,(void*)0},{&l_106[3][4],&l_110,&l_110},{&l_110,&l_106[4][3],&l_110},{&l_110,&l_106[4][0],&l_110},{&l_106[3][4],&l_110,&l_106[4][0]}},{{&l_110,&l_110,&l_106[6][1]},{&l_106[1][1],(void*)0,&l_110},{&l_110,&l_110,(void*)0},{&l_106[4][0],&l_110,(void*)0},{&l_106[4][0],(void*)0,&l_110}},{{&l_110,&l_106[5][3],&l_110},{&l_106[2][3],&l_106[4][0],&l_110},{&l_110,&l_106[3][0],(void*)0},{&l_106[1][1],&l_106[1][4],(void*)0},{(void*)0,&l_110,&l_110}},{{&l_106[5][3],&l_110,&l_106[6][1]},{&l_106[4][4],&l_110,&l_106[4][0]},{&l_106[6][1],(void*)0,&l_110},{(void*)0,&l_106[4][4],&l_110},{(void*)0,&l_106[4][4],&l_110}},{{&l_106[1][1],(void*)0,(void*)0},{&l_106[4][0],&l_110,&l_106[2][1]},{(void*)0,&l_110,&l_110},{&l_106[3][0],&l_110,&l_106[3][0]},{&l_110,&l_106[1][4],&l_110}},{{&l_106[1][4],&l_106[3][0],&l_106[4][4]},{&l_106[4][2],&l_106[4][0],&l_106[2][1]},{&l_106[1][2],&l_110,&l_106[4][3]},{&l_106[4][2],&l_106[4][0],&l_106[4][0]},{&l_106[3][0],(void*)0,(void*)0}},{{&l_110,&l_110,&l_106[5][1]},{(void*)0,&l_110,&l_106[1][1]},{&l_110,&l_110,&l_106[1][1]},{&l_106[4][0],&l_106[1][1],(void*)0},{&l_106[1][2],&l_106[4][0],&l_110}}};
            int32_t l_184 = 0xEA2471E6L;
            int32_t l_213 = 0xCD150227L;
            uint16_t l_215 = 6UL;
            int32_t l_282 = 2L;
            int32_t l_285 = 0L;
            int32_t l_290 = 0x81F3CD54L;
            int32_t l_291 = 1L;
            int32_t l_292 = 0L;
            int32_t l_293 = 0L;
            int32_t l_294[7];
            int32_t ***l_311 = &g_180[5];
            uint8_t *l_350 = &g_332;
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_294[i] = 1L;
        }
        g_151 = p_53;
        return (*g_203);
    }
    else
    { /* block id: 106 */
        int16_t *l_354 = (void*)0;
        int32_t l_355 = 0x58B053FAL;
        uint8_t ****l_360 = &g_258;
        int16_t *l_365 = &g_254;
        int64_t *l_417 = &g_108;
        int8_t *l_469 = (void*)0;
        int32_t l_486 = 1L;
        int32_t l_487 = 0xF0450EE6L;
        int32_t l_488[8] = {0x2DD1F76FL,(-9L),0x2DD1F76FL,(-9L),0x2DD1F76FL,(-9L),0x2DD1F76FL,(-9L)};
        int64_t l_597 = (-1L);
        int32_t l_705 = (-4L);
        float *l_774 = &g_549;
        float **l_773 = &l_774;
        uint32_t l_780 = 0xE73E2B48L;
        int i;
lbl_400:
        (*g_181) ^= (safe_mul_func_int16_t_s_s(g_163, (l_355 = (*l_74))));
lbl_471:
        (*g_357) = (**g_203);
        if ((safe_div_func_uint8_t_u_u((p_52 || ((((**l_346) = ((l_74 != (void*)0) < (((*l_360) = &l_61) == (void*)0))) , g_328) , (safe_rshift_func_int16_t_s_s(((*l_365) = (safe_div_func_int8_t_s_s(((*l_74) > 0UL), (0x54DA2DE2E828B441LL || p_54)))), g_75)))), p_53)))
        { /* block id: 113 */
            int16_t **l_370 = (void*)0;
            int16_t **l_371 = &l_354;
            int32_t l_387 = 5L;
            int64_t *l_401 = &g_108;
            int32_t *l_472[3][8] = {{&l_284,&l_284,(void*)0,&l_284,&l_284,(void*)0,&l_284,&l_284},{&g_2[1][0],&l_284,&g_2[1][0],&g_2[1][0],&l_284,&g_2[1][0],&g_2[1][0],&l_284},{&l_284,&g_2[1][0],&g_2[1][0],&l_284,&g_2[1][0],&g_2[1][0],&l_284,&g_2[1][0]}};
            int32_t l_515 = 0L;
            uint32_t l_542 = 18446744073709551615UL;
            uint16_t *l_581[10][3][8] = {{{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582}},{{(void*)0,&g_582,(void*)0,&g_582,&g_582,(void*)0,&g_582,(void*)0},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582}},{{(void*)0,&g_582,&g_582,(void*)0,&g_582,(void*)0,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{(void*)0,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582}},{{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{(void*)0,&g_582,&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582}},{{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,(void*)0},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582}},{{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,(void*)0,&g_582},{&g_582,&g_582,&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582}},{{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582,(void*)0,&g_582},{&g_582,&g_582,(void*)0,&g_582,(void*)0,&g_582,&g_582,(void*)0}},{{&g_582,&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582,&g_582},{(void*)0,&g_582,&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582},{&g_582,&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582,&g_582}},{{&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582,&g_582},{&g_582,(void*)0,(void*)0,&g_582,&g_582,&g_582,&g_582,(void*)0}},{{&g_582,&g_582,&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582},{&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582},{&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582,&g_582,&g_582}}};
            const float l_613 = (-0x1.0p-1);
            int64_t l_640 = 0xEB3A8C486B5046B7LL;
            float *l_711[4][7] = {{&g_151,&g_151,&g_151,&g_151,&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151,&g_151,&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151,&g_151,&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151,&g_151,&g_151,&g_151,&g_151}};
            const float l_712 = 0xE.342925p-51;
            const int32_t l_723 = 1L;
            int i, j, k;
            if ((safe_mul_func_uint8_t_u_u((***g_258), (safe_div_func_uint32_t_u_u((((*l_371) = &p_52) != &p_52), (((safe_unary_minus_func_int8_t_s(p_54)) , (((safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s(0x28L, (((g_108 = ((safe_div_func_int64_t_s_s((0xDCD74454L <= (safe_sub_func_int64_t_s_s(((*l_74) , (0x543B839793986B40LL == (safe_add_func_int32_t_s_s((safe_lshift_func_int8_t_s_s((safe_add_func_uint64_t_u_u((p_53 > p_53), g_312.f2)), 4)), (*p_51))))), p_54))), 18446744073709551606UL)) <= l_355)) && p_50) != 6L))), 0x1CDFL)) , 1UL) , g_2[1][0])) && l_387))))))
            { /* block id: 116 */
                const int16_t l_399[10][8][3] = {{{(-1L),(-1L),2L},{1L,0xAF54L,0xFFD2L},{0xCE14L,0x915EL,0xAE10L},{0x0AE2L,(-1L),0x8F9AL},{1L,0x5CEAL,1L},{0xF846L,(-1L),(-5L)},{(-1L),(-3L),8L},{0xAE10L,0x915EL,0xCE14L}},{{0L,7L,1L},{0L,0x8691L,(-1L)},{0xAE10L,0x13C4L,0x3985L},{(-1L),1L,0xFFD2L},{0xF846L,0xAF54L,(-4L)},{1L,1L,0L},{0x0AE2L,0x0AE2L,(-1L)},{0xCE14L,(-1L),1L}},{{1L,0x76F2L,0L},{(-1L),(-3L),0x4DF9L},{(-1L),(-1L),(-2L)},{0x3985L,(-5L),(-1L)},{0L,(-1L),0x4DF9L},{0xD051L,0xA63BL,1L},{8L,0x5CEAL,0L},{0x13C4L,0xCE14L,0x41B8L}},{{2L,0x13C4L,0xD051L},{0xF560L,0x3322L,0x4DF9L},{(-4L),0x0AE2L,0xE97DL},{(-2L),0x0AE2L,0xFFD2L},{(-1L),0x3322L,1L},{(-1L),0x13C4L,(-4L)},{0L,0xCE14L,(-1L)},{0xAE10L,0x5CEAL,(-3L)}},{{0L,0xA63BL,0L},{(-1L),(-1L),0x41B8L},{0x13C4L,(-5L),0xAE10L},{0xFFD2L,(-1L),0xD051L},{0x3322L,0x5FD2L,(-3L)},{0xFFD2L,0x0AE2L,(-2L)},{0x13C4L,0x4DF9L,(-2L)},{(-1L),1L,0x3322L}},{{0L,(-1L),(-4L)},{0xAE10L,1L,0xE97DL},{0L,0x5CEAL,8L},{(-1L),0xADC6L,1L},{(-1L),(-1L),0xAE10L},{(-2L),0xCE14L,0xAE10L},{(-4L),(-1L),1L},{0xF560L,0x5FD2L,8L}},{{2L,0x4DF9L,0xE97DL},{0x13C4L,(-1L),(-4L)},{8L,0x3322L,0x3322L},{0xD051L,(-1L),(-2L)},{0L,1L,(-2L)},{0x3985L,(-1L),(-3L)},{(-1L),0x8F57L,0xD051L},{8L,(-1L),0xAE10L}},{{(-1L),1L,0x41B8L},{0xFFD2L,(-1L),0L},{1L,0x3322L,(-3L)},{2L,(-1L),(-1L)},{(-2L),0x4DF9L,(-4L)},{0x4DF9L,0x5FD2L,1L},{0L,(-1L),0xFFD2L},{0x3985L,0xCE14L,0xE97DL}},{{0x3985L,(-1L),0x4DF9L},{0L,0xADC6L,0xD051L},{0x4DF9L,0x5CEAL,0x41B8L},{(-2L),1L,0L},{2L,(-1L),1L},{1L,1L,0x4DF9L},{0xFFD2L,0x4DF9L,(-1L)},{(-1L),0x0AE2L,(-2L)}},{{8L,0x5FD2L,0xF560L},{(-1L),(-1L),(-2L)},{0x3985L,(-5L),(-1L)},{0L,(-1L),0x4DF9L},{0xD051L,0xA63BL,1L},{8L,0x5CEAL,0L},{0x13C4L,0xCE14L,0x41B8L},{2L,0x13C4L,0xD051L}}};
                int32_t l_480 = 0L;
                int32_t l_483 = (-4L);
                int32_t l_484[6][9];
                int64_t l_494 = 0x649E106DE9C09DDFLL;
                uint32_t *l_564 = &g_163;
                int i, j, k;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 9; j++)
                        l_484[i][j] = 0xBEAD8993L;
                }
                if ((*p_51))
                { /* block id: 117 */
                    int8_t **l_398 = (void*)0;
                    int64_t *l_402 = &g_108;
                    int32_t l_431 = 4L;
                    uint8_t *l_433 = (void*)0;
                    uint8_t *l_434[9];
                    uint64_t *l_438 = &l_79;
                    int32_t l_481 = 0x8C86D933L;
                    int32_t l_482 = 0x0B541A92L;
                    int32_t l_485 = 0xC8EC8402L;
                    int32_t l_489 = 4L;
                    int32_t l_490 = 1L;
                    int32_t l_492[9] = {2L,(-9L),(-9L),2L,(-9L),(-7L),(-9L),(-7L),(-7L)};
                    int i;
                    for (i = 0; i < 9; i++)
                        l_434[i] = &g_332;
                    if ((safe_mod_func_int8_t_s_s(g_313[1].f1, ((*g_145) ^= (safe_lshift_func_uint8_t_u_s((safe_sub_func_int64_t_s_s(((*l_74) = (3L == p_54)), (0xB8DCL >= (((((g_396 != l_398) != l_399[4][5][0]) != ((*g_181) = (*p_51))) & (p_50 > g_239[0])) , p_50)))), 6))))))
                    { /* block id: 121 */
                        uint8_t l_405 = 1UL;
                        if (g_60)
                            goto lbl_400;
                        (*g_181) = ((((l_399[4][5][0] & ((g_313[1] , l_401) != l_402)) >= ((p_54 > (safe_rshift_func_int16_t_s_u(l_405, (p_53 || (safe_rshift_func_uint8_t_u_u((((safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(l_405, p_52)), g_2[1][0])) , g_135) && l_399[4][5][0]), 6)))))) | (-1L))) != p_54) & p_50);
                    }
                    else
                    { /* block id: 124 */
                        int32_t **l_412[4][1][5];
                        int64_t *l_418 = &g_108;
                        int64_t **l_419 = &l_401;
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 1; j++)
                            {
                                for (k = 0; k < 5; k++)
                                    l_412[i][j][k] = &g_181;
                            }
                        }
                        (*g_357) = (*g_357);
                        (*g_181) = (((safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint8_t_u_s(((l_417 != (g_313[1] , ((*l_419) = l_418))) ^ 0L), (safe_mul_func_int8_t_s_s(p_52, (safe_mul_func_int8_t_s_s(0x2AL, ((g_254 = (((p_53 = p_53) & (g_78 > (+p_52))) && 0x0326L)) != (-1L)))))))), g_332)) != (-1L)) , (-4L));
                    }
                    if (((safe_lshift_func_uint8_t_u_u((p_50 = (((((***g_258) = (((*l_74) | l_355) | (safe_rshift_func_int16_t_s_s(l_355, (((safe_lshift_func_int16_t_s_s(l_399[4][5][0], 5)) , (*g_258)) != (**l_360)))))) & ((g_75 ^ ((((l_431 , (l_432 != l_432)) & 0x19A1L) , l_431) & l_355)) && 0x97F1FA50L)) & p_54) == l_431)), 6)) , (*p_51)))
                    { /* block id: 133 */
                        const uint8_t l_447[2] = {1UL,1UL};
                        int32_t *l_470 = &g_239[0];
                        int i;
                        (*l_74) = (safe_rshift_func_uint8_t_u_s(((+((&g_81 != l_438) > g_135)) || (p_53 || 0x1DL)), (((p_52 >= p_52) , ((safe_mul_func_uint8_t_u_u((***g_258), ((p_54 ^ 0xD00BL) < l_399[4][5][0]))) > g_239[0])) & l_387)));
                        (*l_470) &= (safe_div_func_uint64_t_u_u((p_52 < l_431), (((safe_add_func_int32_t_s_s((*p_51), ((safe_add_func_int16_t_s_s(((l_431 <= l_447[1]) == (safe_div_func_uint64_t_u_u(((safe_add_func_float_f_f(((safe_sub_func_float_f_f(0x6.7p+1, ((((-(safe_add_func_float_f_f(p_53, (((safe_div_func_int64_t_s_s(5L, (safe_add_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((safe_sub_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(((*g_145) & p_54), g_328)), p_52)), (*p_51))), 0xF86CB509AED94B71LL)), (*l_74))))) , l_469) == (void*)0)))) <= l_447[1]) != 0x2.AB8E56p-55) < 0x9.8BB63Fp-64))) < g_135), (-0x6.4p-1))) , g_60), (*l_74)))), 0x22B2L)) && 0xBEL))) >= 0x3CL) ^ (***g_258))));
                        if (g_81)
                            goto lbl_471;
                        (*g_181) &= (p_51 == (l_472[1][1] = &l_387));
                    }
                    else
                    { /* block id: 139 */
                        int32_t **l_473 = &l_472[1][1];
                        float *l_474[10][2][9] = {{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}},{{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0,&g_151,&g_151,(void*)0}}};
                        int32_t ***l_476 = (void*)0;
                        int32_t ****l_475[6] = {&l_476,&l_476,&l_476,&l_476,&l_476,&l_476};
                        int i, j, k;
                        (*l_473) = (void*)0;
                        if (g_135)
                            goto lbl_479;
lbl_479:
                        g_151 = (((*l_74) = 0x2.Ap-1) > ((g_477 = &g_180[0]) != l_478[0]));
                        --g_495;
                        return (*g_477);
                    }
                    for (g_495 = 9; (g_495 != 25); g_495 = safe_add_func_uint16_t_u_u(g_495, 1))
                    { /* block id: 150 */
                        return (*g_203);
                    }
                    p_51 = (*g_357);
                }
                else
                { /* block id: 154 */
                    int32_t **l_500 = (void*)0;
                    int32_t **l_505 = (void*)0;
                    uint8_t *l_513 = (void*)0;
                    uint8_t *l_514[9][4][7] = {{{&g_60,&l_96[0],&g_332,&g_332,&g_60,&g_332,&g_332},{&g_332,&g_332,&l_96[4],&l_96[2],&g_332,&l_96[0],&g_332},{&g_60,&g_332,&l_96[2],&l_96[2],&g_60,(void*)0,&l_96[2]},{&l_96[0],&g_332,&g_332,&g_332,&g_332,&g_332,&l_96[0]}},{{&g_332,&g_332,&l_96[2],&l_96[2],&g_60,&l_96[2],&l_96[2]},{(void*)0,&g_332,&l_96[2],(void*)0,&l_96[1],&g_60,&g_60},{&l_96[0],&l_96[2],&l_96[2],&l_96[2],&l_96[0],&g_60,&g_332},{&l_96[4],&l_96[2],&g_332,&l_96[0],&g_332,&l_96[3],&l_96[1]}},{{&l_96[2],&l_96[2],&l_96[2],&g_60,&l_96[2],&l_96[2],&g_60},{&l_96[4],&l_96[0],&l_96[4],(void*)0,&g_332,&g_332,&g_332},{&l_96[0],&g_332,&g_332,&g_60,&g_60,&g_332,&g_332},{(void*)0,&l_96[4],&g_332,&g_332,&g_332,&g_332,&l_96[4]}},{{&g_332,&g_60,&l_96[2],&g_332,&g_332,&l_96[2],&g_60},{&l_96[0],&l_96[2],&g_332,&g_332,&g_332,&l_96[3],(void*)0},{&g_60,&l_96[2],&g_332,&g_332,&l_96[2],&g_60,&g_332},{&g_332,(void*)0,(void*)0,&g_332,(void*)0,&g_60,&g_332}},{{&g_60,&g_60,&g_60,&g_60,&l_96[2],&l_96[2],&l_96[2]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_96[2],&g_332,&l_96[3]},{&l_96[2],&l_96[2],&l_96[2],&g_60,&g_332,(void*)0,&g_332},{&g_60,&l_96[2],&l_96[3],&l_96[0],&g_332,&l_96[0],&l_96[3]}},{{&g_60,&g_60,&g_332,&l_96[2],(void*)0,&g_332,&l_96[2]},{&g_332,&l_96[4],&g_332,(void*)0,&g_60,&g_332,&g_332},{&g_332,&g_332,(void*)0,&l_96[2],(void*)0,&g_332,&g_332},{&l_96[1],&l_96[0],(void*)0,&g_332,&g_332,&g_332,(void*)0}},{{&g_332,&l_96[2],&l_96[0],&l_96[2],&g_332,&g_60,&g_60},{&l_96[2],&l_96[2],(void*)0,&l_96[2],&l_96[2],&g_332,&l_96[4]},{&g_332,&l_96[2],(void*)0,&g_332,&l_96[2],&g_332,&g_332},{(void*)0,&g_332,(void*)0,&l_96[3],&l_96[4],&l_96[4],&l_96[3]}},{{&g_332,&l_96[2],&g_332,&g_332,&g_60,&g_60,&g_332},{&l_96[2],&g_332,&l_96[0],&g_332,&l_96[3],&l_96[1],&g_332},{&l_96[2],&g_332,&g_60,(void*)0,(void*)0,&g_60,&g_332},{&g_332,&g_60,&g_332,(void*)0,&g_332,&l_96[4],&g_60}},{{&l_96[2],&l_96[2],&g_60,&l_96[0],&g_332,&g_332,&g_60},{&g_332,&g_332,(void*)0,(void*)0,&g_332,&g_332,&g_332},{&g_60,&g_60,&l_96[2],(void*)0,&g_332,&l_96[4],&g_60},{&g_60,&l_96[3],&g_60,&g_332,(void*)0,&g_332,(void*)0}}};
                    uint8_t l_532[4];
                    int32_t l_539 = 0x7F564847L;
                    int32_t l_540 = 0x277DAA5FL;
                    int32_t l_541 = 0xBCFC4B32L;
                    float *l_547 = &g_151;
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_532[i] = 1UL;
                    for (g_254 = 0; (g_254 <= 1); g_254 += 1)
                    { /* block id: 157 */
                        p_51 = p_51;
                    }
                    for (l_491 = 4; (l_491 >= 0); l_491 -= 1)
                    { /* block id: 162 */
                        return l_500;
                    }
                    if ((safe_rshift_func_int8_t_s_u(p_53, ((**l_61) = (safe_add_func_int32_t_s_s((((void*)0 != l_505) > 0x09D4L), (p_52 ^ ((safe_mul_func_int16_t_s_s((!((p_52 ^ 8L) <= (safe_mul_func_uint8_t_u_u(((safe_div_func_int8_t_s_s(((**l_346) = (((g_332 ^= (***g_258)) , 0x336CL) ^ 0L)), p_53)) != p_54), l_515)))), 1L)) && 18446744073709551610UL))))))))
                    { /* block id: 168 */
                        float l_524 = 0x1.3p-1;
                        int32_t l_533 = 0xD389D43FL;
                        (*l_360) = &g_144;
                        l_480 &= (((((((((safe_add_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_sub_func_uint16_t_u_u(0x99EBL, (l_486 , ((l_483 &= ((*l_347) = (((*g_145) < (g_108 & g_312.f2)) <= (((((safe_mod_func_uint8_t_u_u(l_486, (safe_sub_func_int16_t_s_s(((l_484[4][3] , p_52) || ((((safe_unary_minus_func_uint8_t_u((safe_sub_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s(p_50, l_399[4][5][0])) == 18446744073709551606UL), (*p_51))))) >= l_532[3]) , (*p_51)) == g_312.f1)), g_495)))) , 0x0C3E9FADL) != (*p_51)) == l_533) < p_53)))) < 0x92L)))), 15)) , 0x204EL), g_135)) >= g_2[1][0]) < 0xB16999D877BF354BLL) ^ p_54) <= (*p_51)) & p_52) && 0xDE9CL) < g_2[2][0]) & 18446744073709551615UL);
                    }
                    else
                    { /* block id: 173 */
                        int32_t *l_534[8][4][1] = {{{&l_355},{&l_289[0]},{&l_355},{&l_488[7]}},{{(void*)0},{&l_289[0]},{(void*)0},{&l_488[7]}},{{&l_355},{&l_289[0]},{&l_355},{&l_488[7]}},{{(void*)0},{&l_289[0]},{(void*)0},{&l_488[7]}},{{&l_355},{&l_289[0]},{&l_355},{&l_488[7]}},{{(void*)0},{&l_289[0]},{(void*)0},{&l_488[7]}},{{&l_355},{&l_289[0]},{&l_355},{&l_488[7]}},{{(void*)0},{&l_289[0]},{(void*)0},{&l_488[7]}}};
                        int i, j, k;
                        l_534[1][0][0] = (p_51 = (*g_357));
                        --l_536;
                        ++l_542;
                        return (*g_203);
                    }
                    (*g_548) = (safe_add_func_float_f_f(0x7.1F1304p-95, ((*l_547) = p_50)));
                }
                (*l_74) = (0x0C93FF78L < ((*l_564) |= (safe_add_func_int64_t_s_s(((*l_417) = (1UL <= ((*l_354) = ((*l_365) = ((((void*)0 != &g_258) , ((safe_add_func_int8_t_s_s((g_554 , (safe_mul_func_uint16_t_u_u(l_486, (safe_sub_func_int64_t_s_s(((-1L) <= l_486), (((safe_sub_func_int16_t_s_s(((g_561[4][2] = &l_494) == (void*)0), l_487)) >= g_562) || 0xFAL)))))), l_563)) , l_401)) == (void*)0))))), p_54))));
                return (*g_203);
            }
            else
            { /* block id: 190 */
                uint32_t *l_576 = &g_163;
                uint8_t * const *l_590[8] = {&l_62,&l_62,&l_62,&l_62,&l_62,&l_62,&l_62,&l_62};
                int32_t l_595 = 0xF214EE4CL;
                uint32_t *l_596 = &g_562;
                int32_t l_598 = (-4L);
                float *l_599 = &g_151;
                int i;
                (*l_74) = ((*l_599) = (safe_add_func_float_f_f(((((((safe_sub_func_float_f_f((!0x9.AE72D6p+66), (safe_mul_func_float_f_f(((safe_sub_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u(((g_332 = (((((*l_576)++) > (((**g_144) = (l_598 |= (safe_add_func_uint32_t_u_u((l_581[0][2][0] == l_354), (safe_rshift_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((safe_add_func_int8_t_s_s(((+(((p_53 ^ ((*g_181) = (((((l_590[3] == ((safe_lshift_func_int8_t_s_u((safe_sub_func_uint64_t_u_u(((l_597 = (p_52 ^ (g_239[0] ^ ((*l_596) |= (l_595 || l_486))))) | (-2L)), l_486)), 6)) , (**l_432))) & 0x38E4DFDAL) , (*g_145)) > 0x0AL) , (-1L)))) , p_54) || p_52)) != l_595), (*l_74))), 18446744073709551608UL)), p_50)))))) , l_595)) && 1L) > (*l_74))) , g_332), 10)), 0L)) , (*g_548)), 0xB.FD1D98p+25)))) , l_486) , 0x2B85L) > p_54) , 0x6.48D0FCp-57) < p_52), 0x1.Ap-1)));
                for (l_355 = 0; (l_355 <= 4); l_355 += 1)
                { /* block id: 202 */
                    int64_t l_608 = 0x142E51CA6127EC57LL;
                    uint32_t l_619 = 0xB52D4AADL;
                    int16_t **l_632[6][7] = {{&l_365,&l_365,&l_365,&l_365,&l_365,&l_365,&l_365},{&l_365,&l_365,&l_365,&l_365,&l_365,&l_365,&l_365},{&l_365,&l_365,&l_365,&l_365,&l_365,&l_365,&l_365},{&l_365,&l_365,&l_365,&l_365,&l_365,&l_365,&l_365},{&l_365,&l_365,&l_365,&l_365,&l_365,&l_365,&l_365},{&l_365,&l_365,&l_365,&l_365,&l_365,&l_365,&l_365}};
                    uint16_t l_639 = 1UL;
                    int32_t l_643 = 0x440506F4L;
                    uint16_t l_646 = 0UL;
                    int i, j;
                    (*g_357) = (void*)0;
                    l_488[l_355] = (((safe_sub_func_int8_t_s_s(p_52, (((((safe_mod_func_int16_t_s_s(((safe_mod_func_int32_t_s_s(l_595, (((*l_401) &= ((safe_div_func_int32_t_s_s((*p_51), (l_608 ^ (safe_rshift_func_uint16_t_u_s(((p_52 , (void*)0) == &g_582), 15))))) & (safe_mul_func_uint16_t_u_u((l_74 != p_51), 0x63B1L)))) , l_597))) > (*p_51)), 0xF6ADL)) || l_598) == p_54) == p_54) == p_52))) | p_53) ^ l_614);
                    if (((((((*g_203) != &p_51) & ((safe_lshift_func_uint8_t_u_s(((*p_51) , ((g_582 |= 0xAD59L) <= ((safe_rshift_func_int8_t_s_u(((((l_619 <= (safe_unary_minus_func_uint32_t_u((((safe_mod_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((safe_sub_func_uint32_t_u_u(((*l_596) = ((safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(p_53, 1)), ((((~(((*l_371) = l_354) == (g_633 = &p_52))) < (safe_mul_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u(((+(((1L || 0xA6L) , 9L) , p_53)) | p_54), l_608)) > g_239[0]), (*g_145)))) , l_639) & g_12))) != p_54)), p_52)), (-4L))), p_50)) <= 0x45EC1ACBL) <= (*p_51))))) & g_332) || (***g_258)) , p_50), (**g_144))) || l_640))), 6)) , p_52)) , &g_283[3][0][1]) == &g_254) != l_595))
                    { /* block id: 210 */
                        (**g_203) = p_51;
                    }
                    else
                    { /* block id: 212 */
                        float l_641 = 0x3.09BA22p-86;
                        int32_t l_642 = 1L;
                        int32_t l_644 = 7L;
                        int32_t l_645 = 1L;
                        l_646++;
                        (*l_599) = (((g_313[1] , (((safe_mod_func_uint32_t_u_u(((((l_651 == l_354) , (((((*l_74) |= (~(~((((!((*l_596) = p_53)) , p_52) || 0x17C8L) , (safe_mod_func_uint8_t_u_u((safe_sub_func_int16_t_s_s((*g_633), (((*l_62) = (safe_sub_func_int8_t_s_s((((p_50 = (***g_258)) && l_644) == p_52), p_54))) < 6UL))), l_355)))))) != 0x83DC50E2L) != p_52) == 0x3BL)) < 0x63L) , p_53), g_239[0])) < l_643) != 0xAAL)) , (*g_548)) == l_619);
                        if (l_488[7])
                            break;
                        return (*g_203);
                    }
                    if ((safe_sub_func_int32_t_s_s((*p_51), ((((safe_div_func_float_f_f(((g_312.f2 , (((safe_mul_func_uint8_t_u_u(0xACL, ((safe_mod_func_int8_t_s_s((safe_lshift_func_int8_t_s_s(((((((l_671 , (safe_mul_func_int16_t_s_s((safe_mod_func_uint32_t_u_u((--(*l_596)), 0x30D94DBCL)), (((((safe_sub_func_uint16_t_u_u(p_54, ((((g_680 , (*g_548)) , p_53) & (*g_145)) >= (*l_74)))) , (*g_548)) == 0x2.D160CBp+41) , g_313[1].f1) >= (*l_74))))) , l_488[l_355]) & g_582) && p_53) == 1UL) >= p_54), l_488[7])), (*g_145))) < p_52))) <= 18446744073709551610UL) , g_78)) , (-0x4.6p+1)), 0x2.666B4Ap-98)) <= p_53) <= p_53) , g_681))))
                    { /* block id: 223 */
                        return (*g_203);
                    }
                    else
                    { /* block id: 225 */
                        int16_t l_703[9] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
                        uint16_t *l_704 = &l_646;
                        int i;
                        l_705 &= ((((safe_rshift_func_int16_t_s_s((safe_mod_func_int32_t_s_s(((*l_74) = 0L), (safe_sub_func_int16_t_s_s((((&l_365 == &l_365) , (safe_lshift_func_int8_t_s_s(((+(safe_add_func_uint64_t_u_u((safe_sub_func_int64_t_s_s(l_595, p_50)), (p_54 , ((safe_sub_func_int16_t_s_s((((**g_203) == &l_387) | (((safe_lshift_func_int16_t_s_s((safe_div_func_uint16_t_u_u((g_582 = (safe_mod_func_int8_t_s_s(l_608, l_703[3]))), p_54)), 15)) , &g_397[0][6][4]) == &l_347)), p_53)) & 0x5F75C708L))))) >= 6UL), p_50))) , (*g_633)), 0L)))), 11)) , l_704) == g_633) & g_680.f0);
                    }
                    for (l_595 = 0; (l_595 <= 4); l_595 += 1)
                    { /* block id: 232 */
                        int i, j;
                        (*l_599) = ((void*)0 != (*l_432));
                        (*l_599) = l_106[(l_595 + 1)][l_355];
                        return l_706;
                    }
                }
            }
            (*g_548) = (safe_add_func_float_f_f((safe_add_func_float_f_f((*g_548), (0x8.BFA491p-12 <= (l_488[7] = (((g_151 = (-0x9.5p-1)) < g_535) >= l_712))))), (((safe_sub_func_float_f_f(g_108, (l_486 != (safe_sub_func_float_f_f((g_254 != 0x3.FDC0E0p+58), g_108))))) >= 0xD.F76E03p+73) == p_53)));
            for (g_108 = 0; (g_108 != 0); g_108++)
            { /* block id: 244 */
                int32_t l_733 = 1L;
                int32_t l_736 = (-8L);
                int32_t l_737 = 1L;
                for (l_110 = 0; (l_110 >= 20); l_110++)
                { /* block id: 247 */
                    uint8_t l_734 = 0x57L;
                    int32_t l_735 = 0L;
                    g_493 = (safe_mul_func_float_f_f((l_737 = (l_736 = (((l_723 > ((l_735 = (0x0.8DC379p-35 >= (((safe_mul_func_float_f_f((safe_div_func_float_f_f((p_53 >= ((*g_548) = ((safe_div_func_float_f_f(((((safe_lshift_func_int16_t_s_u(((*g_633) = 0x57CEL), 15)) , (&g_633 == (void*)0)) >= 0UL) , ((!(l_733 = (0x0.3p-1 >= (*g_548)))) != p_52)), 0x2.B9A094p+9)) != g_239[0]))), g_680.f1)), g_495)) > g_135) < l_734))) > l_597)) >= 0x0.Dp+1) < g_239[0]))), p_53));
                }
            }
        }
        else
        { /* block id: 257 */
            uint32_t l_747 = 0x92404360L;
            float * const l_772 = &g_549;
            float * const * const l_771 = &l_772;
            float *l_833 = &g_794;
            (**g_357) = ((*l_74) |= (+(p_54 > 246UL)));
            if ((p_50 && ((safe_sub_func_int8_t_s_s((((p_50 <= ((safe_mul_func_uint64_t_u_u((l_743 &= (g_313[5] , (((void*)0 != &g_258) , g_313[1].f2))), ((~p_53) < ((safe_mod_func_uint8_t_u_u((*g_145), l_747)) , g_554.f1)))) && g_2[1][0])) , 65535UL) & l_747), p_54)) && p_54)))
            { /* block id: 261 */
                (*l_74) |= ((l_748 < (l_488[7] , (safe_add_func_uint64_t_u_u(((safe_div_func_uint16_t_u_u((((l_747 <= (safe_add_func_uint64_t_u_u((g_495--), ((*l_417) |= l_747)))) | p_50) > ((safe_mul_func_uint16_t_u_u(((***g_258) > (((((safe_mul_func_int16_t_s_s(p_50, ((g_81 , (~((safe_mul_func_uint8_t_u_u(9UL, 255UL)) < p_52))) <= p_52))) >= 0UL) , p_53) , p_52) < 0xB59151FB835E6036LL)), p_54)) > p_52)), l_747)) , p_54), g_535)))) < p_54);
                (**g_357) |= ((*l_74) = (-9L));
            }
            else
            { /* block id: 267 */
                uint16_t l_783[5] = {1UL,1UL,1UL,1UL,1UL};
                int16_t *l_795[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t *l_796 = &g_237;
                uint16_t **l_797 = &l_651;
                const int64_t l_807 = 0x292049506BAF2D05LL;
                int32_t **l_811 = (void*)0;
                int32_t l_812 = 0x93CCB2A3L;
                int i;
                (*l_74) |= ((+(safe_sub_func_int64_t_s_s(((l_487 >= (safe_div_func_uint32_t_u_u((((p_54 & ((void*)0 == (*l_61))) , p_53) & (((*g_633) = (safe_add_func_uint16_t_u_u(l_747, (((l_771 != l_773) > (safe_add_func_uint32_t_u_u((((safe_mod_func_int16_t_s_s((*g_633), p_54)) & 7L) != g_60), l_779))) || (*p_51))))) != p_52)), 4294967287UL))) < p_53), p_53))) ^ (**g_144));
lbl_813:
                (*g_798) = (((*l_796) = ((((l_780 || (safe_lshift_func_uint16_t_u_s((l_783[3] >= (((l_486 &= (((((p_53 || ((safe_rshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_s((safe_div_func_uint64_t_u_u(l_355, (safe_mul_func_int8_t_s_s((p_52 ^ ((****l_432) = ((*g_633) , l_780))), ((**l_346) ^= ((((*l_74) = ((l_783[3] | (-4L)) && p_53)) & 5UL) <= p_50)))))), p_54)), 4)), p_50)) != p_52)) >= (*p_51)) || g_108) , 0x1B18L) , p_52)) <= (*g_633)) >= 0xEE7DL)), l_783[3]))) != l_747) < 0xB4L) , g_12)) , l_797);
                for (p_50 = 0; (p_50 <= 4); p_50 += 1)
                { /* block id: 278 */
                    for (p_52 = 1; (p_52 <= 4); p_52 += 1)
                    { /* block id: 281 */
                        int i;
                        (*l_74) = (((((*l_347) = l_783[p_50]) ^ (safe_add_func_uint8_t_u_u(0x77L, ((*g_145)--)))) , ((safe_sub_func_int16_t_s_s(l_783[p_50], ((l_807 & (0x8258L ^ (safe_sub_func_uint64_t_u_u((p_53 < (**g_799)), ((p_50 > 255UL) <= 0x4AL))))) ^ l_747))) == 249UL)) | p_50);
                        (**l_771) = p_53;
                        return l_811;
                    }
                    for (g_332 = 0; (g_332 <= 0); g_332 += 1)
                    { /* block id: 290 */
                        int i;
                        l_812 ^= (g_239[g_332] &= ((void*)0 == p_51));
                        if (g_239[g_332])
                            continue;
                    }
                }
                if (l_563)
                    goto lbl_813;
            }
            (*g_181) |= (safe_rshift_func_int8_t_s_s(((((***g_798) == (safe_sub_func_int16_t_s_s((safe_mod_func_uint32_t_u_u(p_53, (g_239[0] , (((****l_432) |= ((safe_mul_func_uint8_t_u_u((safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u(((safe_sub_func_float_f_f(g_75, (safe_mul_func_float_f_f((g_151 = (safe_mul_func_float_f_f(((**l_771) = (0x1.9p-1 >= (!((-0x1.3p+1) >= g_495)))), ((*l_833) = (g_254 != (-0x3.Ap+1)))))), g_237)))) , g_312.f0), 0x71C1CFB4DFED1ABALL)), g_75)), (*l_74))) & 0xB1E477A8388818B7LL)) || 2L)))), (*g_800)))) == p_53) <= p_53), l_747));
        }
    }
    return (*g_203);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_2[i][j], "g_2[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_75, "g_75", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_81, "g_81", print_hash_value);
    transparent_crc(g_108, "g_108", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc_bytes (&g_151, sizeof(g_151), "g_151", print_hash_value);
    transparent_crc(g_163, "g_163", print_hash_value);
    transparent_crc(g_237, "g_237", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_239[i], "g_239[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_254, "g_254", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_283[i][j][k], "g_283[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_312.f0, "g_312.f0", print_hash_value);
    transparent_crc(g_312.f1, "g_312.f1", print_hash_value);
    transparent_crc(g_312.f2, "g_312.f2", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_313[i].f0, "g_313[i].f0", print_hash_value);
        transparent_crc(g_313[i].f1, "g_313[i].f1", print_hash_value);
        transparent_crc(g_313[i].f2, "g_313[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_328, "g_328", print_hash_value);
    transparent_crc(g_332, "g_332", print_hash_value);
    transparent_crc_bytes (&g_493, sizeof(g_493), "g_493", print_hash_value);
    transparent_crc(g_495, "g_495", print_hash_value);
    transparent_crc(g_535, "g_535", print_hash_value);
    transparent_crc_bytes (&g_549, sizeof(g_549), "g_549", print_hash_value);
    transparent_crc(g_554.f0, "g_554.f0", print_hash_value);
    transparent_crc(g_554.f1, "g_554.f1", print_hash_value);
    transparent_crc(g_554.f2, "g_554.f2", print_hash_value);
    transparent_crc(g_562, "g_562", print_hash_value);
    transparent_crc(g_582, "g_582", print_hash_value);
    transparent_crc(g_680.f0, "g_680.f0", print_hash_value);
    transparent_crc(g_680.f1, "g_680.f1", print_hash_value);
    transparent_crc(g_680.f2, "g_680.f2", print_hash_value);
    transparent_crc(g_681, "g_681", print_hash_value);
    transparent_crc_bytes (&g_794, sizeof(g_794), "g_794", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_895[i].f0, "g_895[i].f0", print_hash_value);
        transparent_crc(g_895[i].f1, "g_895[i].f1", print_hash_value);
        transparent_crc(g_895[i].f2, "g_895[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_933, "g_933", print_hash_value);
    transparent_crc(g_980, "g_980", print_hash_value);
    transparent_crc(g_1029.f0, "g_1029.f0", print_hash_value);
    transparent_crc(g_1029.f1, "g_1029.f1", print_hash_value);
    transparent_crc(g_1029.f2, "g_1029.f2", print_hash_value);
    transparent_crc(g_1033, "g_1033", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1035[i], "g_1035[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1060.f0, "g_1060.f0", print_hash_value);
    transparent_crc(g_1060.f1, "g_1060.f1", print_hash_value);
    transparent_crc(g_1060.f2, "g_1060.f2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1113[i].f0, "g_1113[i].f0", print_hash_value);
        transparent_crc(g_1113[i].f1, "g_1113[i].f1", print_hash_value);
        transparent_crc(g_1113[i].f2, "g_1113[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1127, "g_1127", print_hash_value);
    transparent_crc(g_1261, "g_1261", print_hash_value);
    transparent_crc(g_1268, "g_1268", print_hash_value);
    transparent_crc(g_1302.f0, "g_1302.f0", print_hash_value);
    transparent_crc(g_1302.f1, "g_1302.f1", print_hash_value);
    transparent_crc(g_1302.f2, "g_1302.f2", print_hash_value);
    transparent_crc(g_1314, "g_1314", print_hash_value);
    transparent_crc(g_1351, "g_1351", print_hash_value);
    transparent_crc(g_1459, "g_1459", print_hash_value);
    transparent_crc(g_1465.f0, "g_1465.f0", print_hash_value);
    transparent_crc(g_1465.f1, "g_1465.f1", print_hash_value);
    transparent_crc(g_1465.f2, "g_1465.f2", print_hash_value);
    transparent_crc(g_1631, "g_1631", print_hash_value);
    transparent_crc(g_1901.f0, "g_1901.f0", print_hash_value);
    transparent_crc(g_1901.f1, "g_1901.f1", print_hash_value);
    transparent_crc(g_1901.f2, "g_1901.f2", print_hash_value);
    transparent_crc(g_1913.f0, "g_1913.f0", print_hash_value);
    transparent_crc(g_1913.f1, "g_1913.f1", print_hash_value);
    transparent_crc(g_1913.f2, "g_1913.f2", print_hash_value);
    transparent_crc(g_1922, "g_1922", print_hash_value);
    transparent_crc(g_1932.f0, "g_1932.f0", print_hash_value);
    transparent_crc(g_1932.f1, "g_1932.f1", print_hash_value);
    transparent_crc(g_1932.f2, "g_1932.f2", print_hash_value);
    transparent_crc(g_1966.f0, "g_1966.f0", print_hash_value);
    transparent_crc(g_1966.f1, "g_1966.f1", print_hash_value);
    transparent_crc(g_1966.f2, "g_1966.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2068[i].f0, "g_2068[i].f0", print_hash_value);
        transparent_crc(g_2068[i].f1, "g_2068[i].f1", print_hash_value);
        transparent_crc(g_2068[i].f2, "g_2068[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_2128, sizeof(g_2128), "g_2128", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 558
XXX total union variables: 15

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 46
breakdown:
   depth: 1, occurrence: 231
   depth: 2, occurrence: 60
   depth: 3, occurrence: 11
   depth: 4, occurrence: 1
   depth: 5, occurrence: 2
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 8, occurrence: 3
   depth: 13, occurrence: 4
   depth: 14, occurrence: 2
   depth: 15, occurrence: 4
   depth: 16, occurrence: 5
   depth: 17, occurrence: 3
   depth: 18, occurrence: 5
   depth: 19, occurrence: 1
   depth: 20, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 23, occurrence: 4
   depth: 24, occurrence: 4
   depth: 25, occurrence: 2
   depth: 26, occurrence: 4
   depth: 27, occurrence: 4
   depth: 28, occurrence: 1
   depth: 30, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 44, occurrence: 1
   depth: 46, occurrence: 1

XXX total number of pointers: 469

XXX times a variable address is taken: 1284
XXX times a pointer is dereferenced on RHS: 288
breakdown:
   depth: 1, occurrence: 248
   depth: 2, occurrence: 32
   depth: 3, occurrence: 8
XXX times a pointer is dereferenced on LHS: 306
breakdown:
   depth: 1, occurrence: 274
   depth: 2, occurrence: 25
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
XXX times a pointer is compared with null: 47
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 23
XXX times a pointer is qualified to be dereferenced: 6804

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1088
   level: 2, occurrence: 227
   level: 3, occurrence: 123
   level: 4, occurrence: 62
   level: 5, occurrence: 43
XXX number of pointers point to pointers: 198
XXX number of pointers point to scalars: 268
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.4
XXX average alias set size: 1.46

XXX times a non-volatile is read: 1895
XXX times a non-volatile is write: 903
XXX times a volatile is read: 89
XXX    times read thru a pointer: 4
XXX times a volatile is write: 34
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 4.1e+03
XXX percentage of non-volatile access: 95.8

XXX forward jumps: 1
XXX backward jumps: 7

XXX stmts: 248
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 23
   depth: 2, occurrence: 30
   depth: 3, occurrence: 35
   depth: 4, occurrence: 50
   depth: 5, occurrence: 85

XXX percentage a fresh-made variable is used: 16
XXX percentage an existing variable is used: 84
********************* end of statistics **********************/


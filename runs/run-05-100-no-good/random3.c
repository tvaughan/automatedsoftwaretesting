/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3251330196
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile int32_t  f0;
   uint16_t  f1;
   const int64_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static volatile int16_t g_8 = 1L;/* VOLATILE GLOBAL g_8 */
static float g_9[2] = {(-0x9.7p-1),(-0x9.7p-1)};
static int64_t g_17 = 1L;
static int32_t g_23 = (-8L);
static int32_t *g_42 = (void*)0;
static int32_t * const *g_41 = &g_42;
static volatile int32_t g_51 = 7L;/* VOLATILE GLOBAL g_51 */
static int32_t g_70 = 0x8988ED9EL;
static int32_t *g_69 = &g_70;
static uint64_t g_77 = 0x5F31A33FFEA361F8LL;
static uint16_t g_83 = 0UL;
static uint32_t g_93 = 0x7386F731L;
static int32_t g_105 = 0xA5C6425FL;
static uint32_t g_127 = 18446744073709551615UL;
static int8_t g_137[7] = {0xD5L,0xD5L,0xD5L,0xD5L,0xD5L,0xD5L,0xD5L};
static uint8_t g_157[9][8][3] = {{{0xC2L,255UL,255UL},{8UL,1UL,1UL},{0UL,246UL,0x48L},{0x48L,0xBDL,8UL},{0xBCL,0xC5L,0xEEL},{0x9EL,8UL,0xD3L},{0x10L,8UL,255UL},{0x8BL,0xC5L,0x8BL}},{{0x72L,0xBDL,1UL},{250UL,246UL,255UL},{4UL,1UL,0x9EL},{1UL,255UL,250UL},{4UL,0xEEL,0UL},{0x93L,0xC2L,1UL},{0x72L,0xE8L,254UL},{255UL,1UL,253UL}},{{2UL,7UL,253UL},{0UL,0x45L,254UL},{0UL,250UL,1UL},{0xBBL,1UL,0UL},{0xFDL,0x2BL,4UL},{7UL,250UL,0UL},{8UL,0x2BL,0x88L},{0x2FL,1UL,0xE9L}},{{246UL,250UL,255UL},{255UL,0x45L,4UL},{254UL,7UL,0x0EL},{254UL,1UL,246UL},{255UL,0xE8L,7UL},{246UL,0xC2L,0xBBL},{0x2FL,0xEEL,255UL},{8UL,0x48L,0x88L}},{{7UL,0xD3L,255UL},{0xFDL,0x53L,0xBBL},{0xBBL,0x72L,7UL},{0UL,0x9EL,246UL},{0UL,255UL,0x0EL},{2UL,255UL,4UL},{255UL,0x9EL,255UL},{0x72L,0x72L,0xE9L}},{{0x93L,0x53L,0x88L},{0UL,0xD3L,0UL},{0xE9L,0x48L,4UL},{0UL,0xEEL,0UL},{0x93L,0xC2L,1UL},{0x72L,0xE8L,254UL},{255UL,1UL,253UL},{2UL,7UL,253UL}},{{0UL,0x45L,254UL},{0UL,250UL,1UL},{0xBBL,1UL,0UL},{0xFDL,0x2BL,4UL},{7UL,250UL,0UL},{8UL,0x2BL,0x88L},{0x2FL,1UL,0xE9L},{246UL,250UL,255UL}},{{255UL,0x45L,4UL},{254UL,7UL,0x0EL},{254UL,1UL,246UL},{255UL,0xE8L,7UL},{246UL,0xC2L,0xBBL},{0x2FL,0xEEL,255UL},{8UL,0x48L,0x88L},{7UL,0xD3L,255UL}},{{0xFDL,0x53L,0xBBL},{0xBBL,0x72L,7UL},{0UL,0x9EL,246UL},{0UL,255UL,0x0EL},{2UL,255UL,4UL},{255UL,0x9EL,255UL},{0x72L,0x72L,0xE9L},{0x93L,0x53L,0x88L}}};
static int64_t g_187 = 0x3D7FC5022FEA0E90LL;
static float g_277 = 0x1.Cp+1;
static float g_279 = (-0x3.5p-1);
static uint8_t g_284 = 2UL;
static uint32_t g_312 = 0x7F203773L;
static union U0 g_319[10][4][6] = {{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}},{{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}},{{0L},{-3L},{0L},{-3L},{0L},{-3L}},{{0x924034DBL},{-3L},{0x924034DBL},{-3L},{0x924034DBL},{-3L}}}};
static union U0 *g_318 = &g_319[6][0][1];
static union U0 g_322[5] = {{-6L},{-6L},{-6L},{-6L},{-6L}};
static uint8_t g_340 = 0UL;
static int32_t g_397 = (-4L);
static int16_t g_399 = (-5L);
static int16_t *g_427[5][2][6] = {{{&g_399,&g_399,&g_399,&g_399,&g_399,&g_399},{&g_399,(void*)0,&g_399,&g_399,&g_399,&g_399}},{{&g_399,&g_399,(void*)0,(void*)0,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399,&g_399,&g_399}},{{&g_399,&g_399,&g_399,&g_399,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399,&g_399,&g_399}},{{&g_399,&g_399,&g_399,(void*)0,(void*)0,&g_399},{&g_399,&g_399,&g_399,&g_399,&g_399,(void*)0}},{{&g_399,&g_399,&g_399,&g_399,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399,&g_399,(void*)0}}};
static uint32_t * volatile *g_449 = (void*)0;
static int64_t g_462 = 1L;
static uint16_t g_463 = 0xCCD2L;
static int8_t g_476 = 0x93L;
static union U0 g_525 = {0L};/* VOLATILE GLOBAL g_525 */
static union U0 g_526 = {0x33F8F73EL};/* VOLATILE GLOBAL g_526 */
static union U0 g_527 = {2L};/* VOLATILE GLOBAL g_527 */
static const union U0 g_528 = {1L};/* VOLATILE GLOBAL g_528 */
static const union U0 g_529 = {0x024FE2DFL};/* VOLATILE GLOBAL g_529 */
static union U0 g_530[10][2][9] = {{{{0xF30D6A8AL},{-2L},{0L},{0x532F8F7EL},{-6L},{0x3194C8E7L},{2L},{-1L},{1L}},{{0x790126FDL},{0x5D25ACF4L},{7L},{-5L},{0x7CF84BC5L},{-5L},{7L},{0x5D25ACF4L},{0x790126FDL}}},{{{-2L},{0L},{-6L},{0L},{0xF30D6A8AL},{2L},{-2L},{0x4AA67286L},{-1L}},{{0xB27F0A4EL},{-4L},{3L},{-1L},{-8L},{1L},{-4L},{-1L},{0x996EDF04L}}},{{{-2L},{0xF30D6A8AL},{-2L},{0L},{0x532F8F7EL},{-6L},{0x3194C8E7L},{2L},{-1L}},{{0x790126FDL},{-1L},{0x7CF84BC5L},{0L},{3L},{5L},{3L},{0L},{0x7CF84BC5L}}},{{{0xF30D6A8AL},{0xF30D6A8AL},{7L},{-1L},{0L},{0L},{0L},{1L},{-2L}},{{-1L},{-4L},{-4L},{0L},{0x455F90A5L},{0L},{0x790126FDL},{0xF3C31E11L},{0x89CD1DF2L}}},{{{7L},{0L},{7L},{-6L},{2L},{0x4AA67286L},{-4L},{-2L},{0L}},{{0x7B15D3C8L},{0x5D25ACF4L},{0x7CF84BC5L},{0xA1B2EFBEL},{-4L},{1L},{0x89CD1DF2L},{-5L},{7L}}},{{{7L},{0xF30D6A8AL},{0xF30D6A8AL},{7L},{-1L},{0L},{0L},{0L},{1L}},{{3L},{0xF3C31E11L},{-1L},{1L},{-3L},{0x5D25ACF4L},{-4L},{-1L},{-4L}}},{{{-1L},{-2L},{7L},{-2L},{-1L},{0L},{-2L},{0L},{0xF30D6A8AL}},{{0xB27F0A4EL},{0xA1B2EFBEL},{0x7B15D3C8L},{-1L},{7L},{0xF3C31E11L},{0x05D4C12DL},{0L},{0x05D4C12DL}}},{{{0L},{-4L},{-6L},{-6L},{-4L},{0L},{0x4AA67286L},{-1L},{0x3194C8E7L}},{{1L},{-1L},{-1L},{1L},{0x05D4C12DL},{0x5D25ACF4L},{-8L},{0L},{0x89CD1DF2L}}},{{{0x4AA67286L},{0L},{5L},{0L},{5L},{0L},{0x4AA67286L},{-6L},{-1L}},{{0x455F90A5L},{0L},{-4L},{-4L},{-1L},{-5L},{0x05D4C12DL},{0xA1B2EFBEL},{3L}}},{{{-6L},{-2L},{-1L},{0L},{0L},{-1L},{-2L},{-6L},{1L}},{{0x790126FDL},{1L},{-8L},{0xA1B2EFBEL},{1L},{-1L},{-4L},{0L},{0x7B15D3C8L}}}};
static const union U0 g_531[9][3][9] = {{{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}}},{{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}}},{{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}}},{{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL},{0x287D8BD3L},{0xCA02927DL},{0xCA02927DL}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}}},{{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}}},{{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}}},{{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}}},{{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}}},{{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}},{{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L},{0xCA02927DL},{1L},{1L}}}};
static const union U0 g_532 = {0xC13AE37CL};/* VOLATILE GLOBAL g_532 */
static union U0 g_533[4][10] = {{{-5L},{1L},{-5L},{0L},{-1L},{-1L},{0L},{-5L},{1L},{-5L}},{{-5L},{0L},{1L},{1L},{1L},{0L},{-5L},{-5L},{0L},{1L}},{{0L},{-5L},{-5L},{0L},{1L},{1L},{1L},{0L},{-5L},{-5L}},{{1L},{-5L},{0L},{-1L},{-1L},{0L},{-5L},{1L},{-5L},{0L}}};
static const union U0 g_534[5][5] = {{{-10L},{1L},{-10L},{0L},{0L}},{{-1L},{-8L},{0x8E9D3171L},{-1L},{-1L}},{{0x530D714CL},{0xC9D0F213L},{0x530D714CL},{-10L},{-10L}},{{0x8E9D3171L},{0x5A27F58DL},{0x8E9D3171L},{-1L},{-1L}},{{0x530D714CL},{0xC9D0F213L},{0x530D714CL},{-10L},{-10L}}};
static const union U0 g_535[2][3][2] = {{{{-7L},{-7L}},{{-4L},{-7L}},{{-7L},{-4L}}},{{{-7L},{-7L}},{{-4L},{-7L}},{{-7L},{-4L}}}};
static const union U0 g_536 = {-4L};/* VOLATILE GLOBAL g_536 */
static union U0 g_537 = {0xA99C4CE3L};/* VOLATILE GLOBAL g_537 */
static union U0 g_538 = {0L};/* VOLATILE GLOBAL g_538 */
static union U0 g_539 = {0xC2B119FBL};/* VOLATILE GLOBAL g_539 */
static const union U0 g_540 = {-8L};/* VOLATILE GLOBAL g_540 */
static union U0 g_541 = {9L};/* VOLATILE GLOBAL g_541 */
static union U0 g_542 = {1L};/* VOLATILE GLOBAL g_542 */
static union U0 g_543[10] = {{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L},{0xCE549A20L}};
static const union U0 g_544 = {-10L};/* VOLATILE GLOBAL g_544 */
static union U0 g_545 = {-6L};/* VOLATILE GLOBAL g_545 */
static const union U0 g_546[10][10] = {{{1L},{1L},{-1L},{0x0224942BL},{0L},{0xA661446CL},{0x82E6787CL},{3L},{-7L},{0x0CD1E362L}},{{3L},{4L},{0x82E6787CL},{1L},{0x36D8EE51L},{1L},{0x82E6787CL},{4L},{3L},{1L}},{{0x8CC61894L},{1L},{-7L},{0L},{0x0CD1E362L},{0xF5824F43L},{5L},{0x36D8EE51L},{0x36D8EE51L},{5L}},{{4L},{0x8CC61894L},{0L},{0L},{0x8CC61894L},{4L},{0x510125AFL},{0xF5824F43L},{3L},{0xA661446CL}},{{0xA661446CL},{0x36D8EE51L},{1L},{1L},{-7L},{-1L},{4L},{-1L},{-7L},{1L}},{{0xA661446CL},{-1L},{0xA661446CL},{0x0224942BL},{5L},{4L},{1L},{0x0CD1E362L},{1L},{0xF5824F43L}},{{4L},{1L},{0x0CD1E362L},{1L},{0xF5824F43L},{0xF5824F43L},{1L},{0x0CD1E362L},{1L},{4L}},{{0x8CC61894L},{-1L},{0xA661446CL},{0x36D8EE51L},{1L},{1L},{-7L},{-1L},{4L},{-1L}},{{3L},{0xA661446CL},{1L},{-1L},{1L},{0xA661446CL},{3L},{0xF5824F43L},{0x510125AFL},{4L}},{{1L},{-7L},{0L},{0x0CD1E362L},{0xF5824F43L},{5L},{0x36D8EE51L},{0x36D8EE51L},{5L},{0xF5824F43L}}};
static const union U0 g_547 = {-3L};/* VOLATILE GLOBAL g_547 */
static union U0 g_548 = {-9L};/* VOLATILE GLOBAL g_548 */
static union U0 g_549 = {0L};/* VOLATILE GLOBAL g_549 */
static const union U0 g_550[3] = {{0x75825DFEL},{0x75825DFEL},{0x75825DFEL}};
static union U0 g_551[10] = {{0xF40DA0BFL},{0xDE2CA2B7L},{0xF40DA0BFL},{0L},{0L},{0xF40DA0BFL},{0xDE2CA2B7L},{0xF40DA0BFL},{0L},{0L}};
static union U0 g_552 = {-1L};/* VOLATILE GLOBAL g_552 */
static union U0 g_553 = {0L};/* VOLATILE GLOBAL g_553 */
static union U0 g_554 = {0x25B2839AL};/* VOLATILE GLOBAL g_554 */
static union U0 g_555 = {0x23A96139L};/* VOLATILE GLOBAL g_555 */
static union U0 g_556 = {-7L};/* VOLATILE GLOBAL g_556 */
static union U0 g_557 = {-1L};/* VOLATILE GLOBAL g_557 */
static union U0 g_558[7] = {{-1L},{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}};
static const union U0 g_559 = {1L};/* VOLATILE GLOBAL g_559 */
static const union U0 g_560[9] = {{1L},{0L},{0L},{1L},{0L},{0L},{1L},{0L},{0L}};
static const union U0 g_561[10] = {{1L},{-5L},{1L},{-5L},{1L},{-5L},{1L},{-5L},{1L},{-5L}};
static union U0 g_562[1] = {{0x5928EBB8L}};
static const union U0 g_563 = {-10L};/* VOLATILE GLOBAL g_563 */
static const union U0 g_564 = {0xED40E39DL};/* VOLATILE GLOBAL g_564 */
static union U0 g_565 = {0xD3A76F4CL};/* VOLATILE GLOBAL g_565 */
static union U0 g_566 = {0x3E0AC787L};/* VOLATILE GLOBAL g_566 */
static union U0 g_567 = {1L};/* VOLATILE GLOBAL g_567 */
static union U0 g_568 = {-7L};/* VOLATILE GLOBAL g_568 */
static union U0 g_569 = {-9L};/* VOLATILE GLOBAL g_569 */
static union U0 g_570 = {0xC612AB8AL};/* VOLATILE GLOBAL g_570 */
static const union U0 g_571 = {0x69AB098BL};/* VOLATILE GLOBAL g_571 */
static union U0 g_572 = {0x11AE61D4L};/* VOLATILE GLOBAL g_572 */
static union U0 g_573 = {0x8868C3DFL};/* VOLATILE GLOBAL g_573 */
static const union U0 g_574 = {0x74AA712AL};/* VOLATILE GLOBAL g_574 */
static const union U0 g_575 = {0xB5E08B1FL};/* VOLATILE GLOBAL g_575 */
static const union U0 g_576 = {-1L};/* VOLATILE GLOBAL g_576 */
static union U0 g_577 = {0x4CF04D7CL};/* VOLATILE GLOBAL g_577 */
static union U0 g_578 = {0xC47BDB97L};/* VOLATILE GLOBAL g_578 */
static union U0 g_579 = {-1L};/* VOLATILE GLOBAL g_579 */
static union U0 g_580 = {0x1DFA212DL};/* VOLATILE GLOBAL g_580 */
static union U0 g_581 = {0x04779259L};/* VOLATILE GLOBAL g_581 */
static union U0 g_582 = {0L};/* VOLATILE GLOBAL g_582 */
static union U0 g_583 = {0L};/* VOLATILE GLOBAL g_583 */
static union U0 g_584[4] = {{0x7D9D1A16L},{0x7D9D1A16L},{0x7D9D1A16L},{0x7D9D1A16L}};
static union U0 g_585 = {8L};/* VOLATILE GLOBAL g_585 */
static union U0 g_586 = {0xF41D7D2CL};/* VOLATILE GLOBAL g_586 */
static union U0 g_587 = {0xA14611E9L};/* VOLATILE GLOBAL g_587 */
static union U0 g_588 = {0x103A3114L};/* VOLATILE GLOBAL g_588 */
static const union U0 g_589 = {0L};/* VOLATILE GLOBAL g_589 */
static union U0 g_591 = {0x50530FA9L};/* VOLATILE GLOBAL g_591 */
static const union U0 *g_590 = &g_591;
static uint8_t g_592 = 0x8EL;
static volatile int32_t ** volatile **g_622 = (void*)0;
static volatile int32_t ** volatile ***g_621 = &g_622;
static int32_t **g_626 = &g_69;
static int32_t ***g_625 = &g_626;
static int32_t ****g_624 = &g_625;
static int32_t *****g_623 = &g_624;
static int32_t g_686[2] = {0xCCCA1DE5L,0xCCCA1DE5L};
static uint8_t g_736[8] = {0xB5L,0xB5L,0UL,0xB5L,1UL,0xB5L,1UL,1UL};
static const uint64_t g_795 = 0xE16E0D96C5AB0F76LL;
static const uint64_t *g_794 = &g_795;
static int32_t g_818 = (-1L);
static int8_t g_833 = 0x23L;
static int32_t ** volatile g_835[4] = {&g_42,&g_42,&g_42,&g_42};
static volatile union U0 g_852[7][5][7] = {{{{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L}},{{5L},{0x293242C4L},{-7L},{-7L},{0x293242C4L},{5L},{0x293242C4L}},{{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L}},{{9L},{9L},{5L},{-7L},{5L},{9L},{9L}},{{9L},{5L},{-7L},{5L},{9L},{9L},{5L}}},{{{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L}},{{5L},{0x293242C4L},{-7L},{-7L},{0x293242C4L},{5L},{0x293242C4L}},{{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L}},{{9L},{9L},{5L},{-7L},{5L},{9L},{9L}},{{9L},{5L},{-7L},{5L},{9L},{9L},{5L}}},{{{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L}},{{5L},{0x293242C4L},{-7L},{-7L},{0x293242C4L},{5L},{0x293242C4L}},{{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L}},{{9L},{9L},{5L},{-7L},{5L},{9L},{9L}},{{9L},{5L},{-7L},{5L},{9L},{9L},{5L}}},{{{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L}},{{5L},{0x293242C4L},{-7L},{-7L},{0x293242C4L},{5L},{0x293242C4L}},{{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L}},{{9L},{9L},{5L},{-7L},{5L},{9L},{9L}},{{9L},{5L},{-7L},{5L},{9L},{9L},{5L}}},{{{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L}},{{5L},{0x293242C4L},{-7L},{-7L},{0x293242C4L},{5L},{0x293242C4L}},{{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L}},{{9L},{9L},{5L},{-7L},{5L},{9L},{9L}},{{9L},{5L},{-7L},{5L},{9L},{9L},{5L}}},{{{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL},{0x293242C4L}},{{5L},{9L},{0x293242C4L},{0x293242C4L},{9L},{0x7327F15AL},{9L}},{{-7L},{0x7327F15AL},{0x7327F15AL},{-7L},{9L},{-7L},{0x7327F15AL}},{{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L}},{{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL}}},{{{-7L},{9L},{-7L},{0x7327F15AL},{0x7327F15AL},{-7L},{9L}},{{0x7327F15AL},{9L},{0x293242C4L},{0x293242C4L},{9L},{0x7327F15AL},{9L}},{{-7L},{0x7327F15AL},{0x7327F15AL},{-7L},{9L},{-7L},{0x7327F15AL}},{{5L},{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L}},{{5L},{0x7327F15AL},{0x293242C4L},{0x7327F15AL},{5L},{5L},{0x7327F15AL}}}};
static volatile union U0 g_861 = {0xDD959B29L};/* VOLATILE GLOBAL g_861 */
static uint32_t *g_877 = (void*)0;
static uint8_t g_899[9][4] = {{255UL,0x3FL,0x6DL,0x3FL},{0x6DL,0x3FL,255UL,0UL},{0x3FL,1UL,9UL,0x6DL},{0UL,0x99L,0x99L,0UL},{0UL,0UL,9UL,0x3BL},{0x3FL,0UL,255UL,0x3AL},{0x6DL,0xC2L,0x6DL,0x3AL},{255UL,0UL,0x3FL,0x3BL},{9UL,0UL,0UL,0UL}};
static int32_t g_943 = 0x200F602FL;
static int64_t g_944 = (-7L);
static volatile uint64_t g_950 = 0x045993A076E72991LL;/* VOLATILE GLOBAL g_950 */
static int8_t * const g_974 = &g_137[3];
static int8_t * const *g_973 = &g_974;
static int8_t g_978 = 0xD1L;
static int32_t * volatile g_1007 = &g_686[0];/* VOLATILE GLOBAL g_1007 */
static const int32_t g_1036 = 0L;
static const int32_t g_1038 = (-1L);
static uint64_t g_1098[3][9][1] = {{{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL}},{{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL}},{{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL},{0x6EFC9C4707B2BAC5LL},{2UL}}};
static volatile union U0 g_1103 = {0x330B05C2L};/* VOLATILE GLOBAL g_1103 */
static volatile uint32_t g_1106 = 4294967287UL;/* VOLATILE GLOBAL g_1106 */
static union U0 *g_1151 = &g_538;
static union U0 ** volatile g_1150 = &g_1151;/* VOLATILE GLOBAL g_1150 */
static union U0 g_1181 = {-3L};/* VOLATILE GLOBAL g_1181 */
static int64_t *g_1186[10][10][2] = {{{&g_462,&g_17},{&g_944,&g_17},{&g_944,&g_187},{&g_462,&g_944},{&g_944,&g_944},{&g_462,&g_944},{&g_187,&g_944},{&g_944,&g_944},{&g_944,&g_944},{&g_187,&g_17}},{{&g_187,&g_944},{&g_944,&g_944},{&g_944,&g_944},{&g_187,&g_944},{&g_462,&g_944},{&g_944,&g_944},{&g_462,&g_187},{&g_944,&g_17},{&g_944,&g_17},{&g_462,&g_187}},{{&g_17,&g_944},{&g_17,&g_944},{&g_17,&g_17},{&g_462,&g_462},{&g_187,&g_462},{&g_944,&g_17},{&g_17,&g_944},{&g_462,&g_944},{&g_17,&g_17},{&g_944,&g_462}},{{&g_187,&g_462},{&g_462,&g_17},{&g_17,&g_944},{&g_462,&g_17},{&g_17,&g_944},{&g_462,(void*)0},{&g_944,&g_17},{&g_17,&g_462},{&g_462,&g_187},{(void*)0,(void*)0}},{{&g_462,&g_17},{(void*)0,&g_462},{&g_187,&g_944},{&g_944,&g_187},{&g_944,&g_17},{&g_944,&g_187},{&g_944,&g_944},{&g_187,&g_462},{(void*)0,&g_17},{&g_462,(void*)0}},{{(void*)0,&g_187},{&g_462,&g_462},{&g_17,&g_17},{&g_944,(void*)0},{&g_462,&g_944},{&g_17,&g_17},{&g_462,&g_944},{(void*)0,&g_17},{&g_462,(void*)0},{&g_944,&g_462}},{{(void*)0,(void*)0},{&g_462,&g_462},{(void*)0,&g_462},{&g_462,(void*)0},{(void*)0,&g_462},{&g_944,(void*)0},{&g_462,&g_17},{(void*)0,&g_944},{&g_462,&g_17},{&g_17,&g_944}},{{&g_462,(void*)0},{&g_944,&g_17},{&g_17,&g_462},{&g_462,&g_187},{(void*)0,(void*)0},{&g_462,&g_17},{(void*)0,&g_462},{&g_187,&g_944},{&g_944,&g_187},{&g_944,&g_17}},{{&g_944,&g_187},{&g_944,&g_944},{&g_187,&g_462},{(void*)0,&g_17},{&g_462,(void*)0},{(void*)0,&g_187},{&g_462,&g_462},{&g_17,&g_17},{&g_944,(void*)0},{&g_462,&g_944}},{{&g_17,&g_17},{&g_462,&g_944},{(void*)0,&g_17},{&g_462,(void*)0},{&g_944,&g_462},{(void*)0,(void*)0},{&g_462,&g_462},{(void*)0,&g_462},{&g_462,(void*)0},{(void*)0,&g_462}}};
static int64_t * const *g_1185 = &g_1186[3][2][0];
static int64_t * const ** volatile g_1184 = &g_1185;/* VOLATILE GLOBAL g_1184 */
static volatile float g_1227[8][10][3] = {{{0x1.6p+1,0xC.B86117p-58,0x9.09B24Bp+27},{(-0x4.9p-1),0x3.254141p-91,(-0x1.Fp+1)},{(-0x4.Ap+1),0x6.0p-1,0x0.D7D58Bp-31},{0x6.00CC32p+54,0x3.254141p-91,0x7.CDD917p-96},{0x4.5D64EDp-89,0xC.B86117p-58,(-0x7.4p+1)},{0x5.B3A571p+80,0x7.2p-1,(-0x1.Fp+1)},{0x4.5D64EDp-89,0x9.09B24Bp+27,0x6.0p-1},{0x6.00CC32p+54,0xE.D481AAp-68,0x1.Cp+1},{(-0x4.Ap+1),0xC.B86117p-58,0x6.0p-1},{(-0x4.9p-1),0x7.1AF85Cp-40,(-0x1.Fp+1)}},{{0x1.6p+1,0x6.0p-1,(-0x7.4p+1)},{0x6.00CC32p+54,0x7.1AF85Cp-40,0x7.CDD917p-96},{(-0x1.1p+1),0xC.B86117p-58,0x0.D7D58Bp-31},{0x5.B3A571p+80,0xE.D481AAp-68,(-0x1.Fp+1)},{(-0x1.1p+1),0x9.09B24Bp+27,0x9.09B24Bp+27},{0x6.00CC32p+54,0x7.2p-1,0x1.Cp+1},{0x1.6p+1,0xC.B86117p-58,0x9.09B24Bp+27},{(-0x4.9p-1),0x3.254141p-91,(-0x1.Fp+1)},{(-0x4.Ap+1),0x6.0p-1,0x8.076116p+5},{0x4.AC7656p+22,0x6.EDA322p-51,0x0.DC29CCp-32}},{{0xC.B86117p-58,0xD.18B90Bp-51,(-0x2.Dp-1)},{0x1.9p-1,0xF.A8BA71p+77,(-0x3.3p-1)},{0xC.B86117p-58,0x6.BED57Bp+78,0x3.Fp+1},{0x4.AC7656p+22,0x2.EEB876p+95,(-0x1.2p+1)},{0x0.1p+1,0xD.18B90Bp-51,0x3.Fp+1},{(-0x1.Fp+1),0x0.Ep+1,(-0x3.3p-1)},{0x4.BC1626p+14,0x3.Fp+1,(-0x2.Dp-1)},{0x4.AC7656p+22,0x0.Ep+1,0x0.DC29CCp-32},{(-0x9.Ep+1),0xD.18B90Bp-51,0x8.076116p+5},{0x1.9p-1,0x2.EEB876p+95,(-0x3.3p-1)}},{{(-0x9.Ep+1),0x6.BED57Bp+78,0x6.BED57Bp+78},{0x4.AC7656p+22,0xF.A8BA71p+77,(-0x1.2p+1)},{0x4.BC1626p+14,0xD.18B90Bp-51,0x6.BED57Bp+78},{(-0x1.Fp+1),0x6.EDA322p-51,(-0x3.3p-1)},{0x0.1p+1,0x3.Fp+1,0x8.076116p+5},{0x4.AC7656p+22,0x6.EDA322p-51,0x0.DC29CCp-32},{0xC.B86117p-58,0xD.18B90Bp-51,(-0x2.Dp-1)},{0x1.9p-1,0xF.A8BA71p+77,(-0x3.3p-1)},{0xC.B86117p-58,0x6.BED57Bp+78,0x3.Fp+1},{0x4.AC7656p+22,0x2.EEB876p+95,(-0x1.2p+1)}},{{0x0.1p+1,0xD.18B90Bp-51,0x3.Fp+1},{(-0x1.Fp+1),0x0.Ep+1,(-0x3.3p-1)},{0x4.BC1626p+14,0x3.Fp+1,(-0x2.Dp-1)},{0x4.AC7656p+22,0x0.Ep+1,0x0.DC29CCp-32},{(-0x9.Ep+1),0xD.18B90Bp-51,0x8.076116p+5},{0x1.9p-1,0x2.EEB876p+95,(-0x3.3p-1)},{(-0x9.Ep+1),0x6.BED57Bp+78,0x6.BED57Bp+78},{0x4.AC7656p+22,0xF.A8BA71p+77,(-0x1.2p+1)},{0x4.BC1626p+14,0xD.18B90Bp-51,0x6.BED57Bp+78},{(-0x1.Fp+1),0x6.EDA322p-51,(-0x3.3p-1)}},{{0x0.1p+1,0x3.Fp+1,0x8.076116p+5},{0x4.AC7656p+22,0x6.EDA322p-51,0x0.DC29CCp-32},{0xC.B86117p-58,0xD.18B90Bp-51,(-0x2.Dp-1)},{0x1.9p-1,0xF.A8BA71p+77,(-0x3.3p-1)},{0xC.B86117p-58,0x6.BED57Bp+78,0x3.Fp+1},{0x4.AC7656p+22,0x2.EEB876p+95,(-0x1.2p+1)},{0x0.1p+1,0xD.18B90Bp-51,0x3.Fp+1},{(-0x1.Fp+1),0x0.Ep+1,(-0x3.3p-1)},{0x4.BC1626p+14,0x3.Fp+1,(-0x2.Dp-1)},{0x4.AC7656p+22,0x0.Ep+1,0x0.DC29CCp-32}},{{(-0x9.Ep+1),0xD.18B90Bp-51,0x8.076116p+5},{0x1.9p-1,0x2.EEB876p+95,(-0x3.3p-1)},{(-0x9.Ep+1),0x6.BED57Bp+78,0x6.BED57Bp+78},{0x4.AC7656p+22,0xF.A8BA71p+77,(-0x1.2p+1)},{0x4.BC1626p+14,0xD.18B90Bp-51,0x6.BED57Bp+78},{(-0x1.Fp+1),0x6.EDA322p-51,(-0x3.3p-1)},{0x0.1p+1,0x3.Fp+1,0x8.076116p+5},{0x4.AC7656p+22,0x6.EDA322p-51,0x0.DC29CCp-32},{0xC.B86117p-58,0xD.18B90Bp-51,(-0x2.Dp-1)},{0x1.9p-1,0xF.A8BA71p+77,(-0x3.3p-1)}},{{0xC.B86117p-58,0x6.BED57Bp+78,0x3.Fp+1},{0x4.AC7656p+22,0x2.EEB876p+95,(-0x1.2p+1)},{0x0.1p+1,0xD.18B90Bp-51,0x3.Fp+1},{(-0x1.Fp+1),0x0.Ep+1,(-0x3.3p-1)},{0x4.BC1626p+14,0x3.Fp+1,(-0x2.Dp-1)},{0x4.AC7656p+22,0x0.Ep+1,0x0.DC29CCp-32},{(-0x9.Ep+1),0xD.18B90Bp-51,0x8.076116p+5},{0x1.9p-1,0x2.EEB876p+95,(-0x3.3p-1)},{(-0x9.Ep+1),0x6.BED57Bp+78,0x6.BED57Bp+78},{0x4.AC7656p+22,0xF.A8BA71p+77,(-0x1.2p+1)}}};
static int64_t ***g_1247 = (void*)0;
static int8_t g_1349[4][5] = {{0xA3L,0xA3L,0xA3L,0xA3L,0xA3L},{0xA3L,0xA3L,0xA3L,0xA3L,0xA3L},{0xA3L,0xA3L,0xA3L,0xA3L,0xA3L},{0xA3L,0xA3L,0xA3L,0xA3L,0xA3L}};
static volatile uint32_t g_1356 = 2UL;/* VOLATILE GLOBAL g_1356 */
static uint32_t g_1369 = 0x2F35DC9BL;
static uint64_t g_1501[4] = {0x368CE0FFBDEAC135LL,0x368CE0FFBDEAC135LL,0x368CE0FFBDEAC135LL,0x368CE0FFBDEAC135LL};
static int16_t g_1537 = 1L;
static uint8_t * volatile g_1539 = &g_592;/* VOLATILE GLOBAL g_1539 */
static uint8_t * volatile *g_1538[9] = {&g_1539,&g_1539,&g_1539,&g_1539,&g_1539,&g_1539,&g_1539,&g_1539,&g_1539};
static int32_t **g_1542 = &g_69;
static int64_t *g_1586 = &g_17;
static int64_t *g_1587 = (void*)0;
static int64_t *g_1588 = &g_17;
static int64_t *g_1589 = &g_462;
static int64_t *g_1590 = &g_187;
static int64_t *g_1591[1] = {&g_944};
static int64_t *g_1592[3][9][5] = {{{&g_944,(void*)0,&g_462,&g_17,&g_187},{&g_944,(void*)0,(void*)0,(void*)0,(void*)0},{&g_944,&g_187,&g_17,&g_462,(void*)0},{&g_944,(void*)0,&g_187,(void*)0,&g_187},{&g_944,&g_187,(void*)0,&g_187,(void*)0},{&g_944,(void*)0,&g_462,&g_17,&g_187},{&g_944,(void*)0,(void*)0,(void*)0,(void*)0},{&g_944,&g_187,&g_17,&g_462,(void*)0},{&g_944,(void*)0,&g_187,(void*)0,&g_187}},{{&g_944,&g_187,(void*)0,&g_187,(void*)0},{&g_944,(void*)0,&g_462,&g_17,&g_187},{&g_944,(void*)0,(void*)0,(void*)0,(void*)0},{&g_944,&g_187,&g_17,&g_462,(void*)0},{&g_944,(void*)0,&g_187,(void*)0,&g_187},{&g_944,&g_187,(void*)0,&g_187,(void*)0},{&g_944,(void*)0,&g_462,&g_17,&g_187},{&g_944,(void*)0,(void*)0,(void*)0,(void*)0},{&g_944,&g_187,&g_17,&g_462,(void*)0}},{{&g_944,(void*)0,&g_187,(void*)0,&g_187},{&g_944,&g_187,(void*)0,&g_187,(void*)0},{&g_944,(void*)0,&g_462,&g_17,&g_187},{&g_944,(void*)0,(void*)0,(void*)0,(void*)0},{&g_944,&g_187,&g_17,&g_462,(void*)0},{&g_944,(void*)0,&g_187,(void*)0,&g_187},{&g_944,&g_187,(void*)0,&g_187,(void*)0},{&g_944,(void*)0,&g_462,&g_17,&g_187},{&g_944,(void*)0,(void*)0,(void*)0,(void*)0}}};
static int64_t *g_1593 = &g_187;
static int64_t *g_1594[4][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int64_t *g_1595 = &g_17;
static int64_t *g_1596 = &g_462;
static int64_t *g_1597 = (void*)0;
static int64_t *g_1598 = &g_17;
static int64_t *g_1599 = (void*)0;
static int64_t *g_1600 = (void*)0;
static int64_t *g_1601 = (void*)0;
static int64_t *g_1602 = &g_462;
static int64_t *g_1603 = &g_462;
static int64_t *g_1604 = (void*)0;
static int64_t *g_1605[2] = {(void*)0,(void*)0};
static int64_t *g_1606 = &g_187;
static int64_t *g_1607 = &g_187;
static int64_t ** const g_1585[2][8][9] = {{{&g_1595,&g_1589,&g_1599,&g_1604,&g_1604,&g_1599,&g_1589,&g_1595,&g_1597},{&g_1590,(void*)0,(void*)0,(void*)0,&g_1594[3][3],&g_1600,&g_1600,&g_1594[3][3],(void*)0},{&g_1587,&g_1601,&g_1587,&g_1591[0],&g_1589,&g_1604,&g_1592[2][5][4],&g_1597,&g_1597},{&g_1588,&g_1590,&g_1586,&g_1600,&g_1586,&g_1590,&g_1588,&g_1603,(void*)0},{&g_1592[2][5][4],&g_1604,&g_1589,&g_1591[0],&g_1587,&g_1601,&g_1587,&g_1591[0],&g_1589},{&g_1600,&g_1600,&g_1594[3][3],(void*)0,(void*)0,(void*)0,&g_1590,&g_1603,&g_1590},{&g_1589,&g_1599,&g_1604,&g_1604,&g_1599,&g_1589,&g_1595,&g_1597,&g_1593},{&g_1603,(void*)0,&g_1594[3][3],&g_1605[1],&g_1588,&g_1588,&g_1605[1],&g_1594[3][3],(void*)0}},{{&g_1599,&g_1607,&g_1589,&g_1587,&g_1602,&g_1591[0],&g_1595,&g_1595,&g_1591[0]},{&g_1598,&g_1594[3][3],&g_1586,&g_1594[3][3],&g_1598,&g_1596,&g_1590,&g_1600,&g_1605[1]},{&g_1601,&g_1607,&g_1587,&g_1606,&g_1597,&g_1606,&g_1587,&g_1607,&g_1601},{(void*)0,(void*)0,(void*)0,&g_1598,&g_1600,&g_1596,&g_1588,&g_1596,&g_1600},{&g_1593,&g_1599,&g_1599,&g_1593,&g_1606,&g_1591[0],&g_1592[2][5][4],&g_1589,&g_1602},{(void*)0,&g_1600,&g_1588,(void*)0,(void*)0,&g_1588,&g_1600,(void*)0,&g_1586},{&g_1601,&g_1604,&g_1602,&g_1595,&g_1606,&g_1589,&g_1589,&g_1606,&g_1595},{&g_1598,&g_1590,&g_1598,(void*)0,&g_1600,(void*)0,&g_1603,&g_1586,&g_1586}}};
static int64_t ** const *g_1584 = &g_1585[0][1][3];
static int32_t g_1692 = 0L;
static int32_t g_1696 = 0x0FB57FD4L;
static volatile union U0 g_1707[5] = {{0xFC619B6AL},{0xFC619B6AL},{0xFC619B6AL},{0xFC619B6AL},{0xFC619B6AL}};
static const int64_t **g_1733 = (void*)0;
static const int64_t ***g_1732 = &g_1733;
static const int64_t ****g_1731[7][7] = {{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732},{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732},{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732},{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732},{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732},{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732},{&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732,&g_1732}};
static const int64_t *****g_1730 = &g_1731[6][1];
static uint64_t g_1765 = 0xDB36DE44A0300677LL;
static int32_t ** volatile g_1866 = &g_69;/* VOLATILE GLOBAL g_1866 */
static volatile union U0 g_1869[2][8][10] = {{{{0x2CE66BB9L},{0x0A598A11L},{0xA0C92FA5L},{0xA18A4F3AL},{0x8142BF60L},{0x60823D11L},{0x0A598A11L},{0x345141F0L},{0x653C7007L},{0x62222E3FL}},{{0xD66EC68EL},{-1L},{0xA18A4F3AL},{0x7E03095DL},{5L},{0x60823D11L},{0x62222E3FL},{0L},{0x7E03095DL},{0x8142BF60L}},{{0x2CE66BB9L},{0xEF03E0AEL},{0xA18A4F3AL},{0L},{1L},{0xD66EC68EL},{0x8142BF60L},{0x345141F0L},{0x345141F0L},{0x8142BF60L}},{{0xCBBE9676L},{5L},{0xA0C92FA5L},{0xA0C92FA5L},{5L},{0xCBBE9676L},{0x8142BF60L},{0x7E03095DL},{0L},{0x62222E3FL}},{{0xC1DAA156L},{0xEF03E0AEL},{0x653C7007L},{0xA0C92FA5L},{0x8142BF60L},{0L},{0x62222E3FL},{0x653C7007L},{0x345141F0L},{0x0A598A11L}},{{0xC1DAA156L},{-1L},{0x345141F0L},{0L},{0xEF03E0AEL},{0xCBBE9676L},{0x0A598A11L},{0x653C7007L},{0x7E03095DL},{0xEF03E0AEL}},{{0xCBBE9676L},{0x0A598A11L},{0x653C7007L},{0x7E03095DL},{0xEF03E0AEL},{0xD66EC68EL},{0xEF03E0AEL},{0x7E03095DL},{0x653C7007L},{0x0A598A11L}},{{0x2CE66BB9L},{0x0A598A11L},{0xA0C92FA5L},{0xA18A4F3AL},{0x8142BF60L},{0x60823D11L},{0x0A598A11L},{0x345141F0L},{0x653C7007L},{0x62222E3FL}}},{{{0xD66EC68EL},{-1L},{0xA18A4F3AL},{0x7E03095DL},{5L},{0x60823D11L},{0x62222E3FL},{0L},{0x7E03095DL},{0x8142BF60L}},{{0x2CE66BB9L},{0xEF03E0AEL},{0xA18A4F3AL},{0L},{1L},{0xD66EC68EL},{0x8142BF60L},{0x345141F0L},{0x345141F0L},{0x8142BF60L}},{{0xCBBE9676L},{5L},{0xA0C92FA5L},{0xA0C92FA5L},{5L},{0xCBBE9676L},{0x8142BF60L},{0x7E03095DL},{0L},{0x62222E3FL}},{{0xC1DAA156L},{0xEF03E0AEL},{0x653C7007L},{0xA0C92FA5L},{0x8142BF60L},{0L},{0x62222E3FL},{0x653C7007L},{0x345141F0L},{0x0A598A11L}},{{0xC1DAA156L},{-1L},{0x345141F0L},{0L},{0xEF03E0AEL},{0xCBBE9676L},{0x0A598A11L},{0x653C7007L},{0x7E03095DL},{0xEF03E0AEL}},{{0xCBBE9676L},{0x0A598A11L},{0x653C7007L},{0x7E03095DL},{0xEF03E0AEL},{0xD66EC68EL},{0xEF03E0AEL},{0x7E03095DL},{0x653C7007L},{0x0A598A11L}},{{0x2CE66BB9L},{0x0A598A11L},{0xA0C92FA5L},{0xA18A4F3AL},{0x8142BF60L},{0x60823D11L},{0x0A598A11L},{0x345141F0L},{0x653C7007L},{0x62222E3FL}},{{0xD66EC68EL},{-1L},{0xA18A4F3AL},{0x7E03095DL},{5L},{0x60823D11L},{0x62222E3FL},{0L},{0x7E03095DL},{0x8142BF60L}}}};
static uint8_t *g_1897 = &g_736[3];
static uint8_t **g_1896 = &g_1897;
static volatile union U0 g_1951 = {0x4EA8205CL};/* VOLATILE GLOBAL g_1951 */
static volatile int8_t ** volatile *g_1991[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static volatile int8_t ** volatile ** volatile g_1990 = &g_1991[0];/* VOLATILE GLOBAL g_1990 */
static int16_t *g_1999[3][4] = {{&g_399,&g_399,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399}};
static volatile union U0 g_2047 = {1L};/* VOLATILE GLOBAL g_2047 */
static union U0 **g_2064 = &g_1151;
static union U0 *** volatile g_2063 = &g_2064;/* VOLATILE GLOBAL g_2063 */
static volatile union U0 g_2069 = {0xE8A41CDFL};/* VOLATILE GLOBAL g_2069 */
static int32_t ** const  volatile g_2121 = &g_42;/* VOLATILE GLOBAL g_2121 */
static volatile uint32_t * volatile g_2166 = &g_1106;/* VOLATILE GLOBAL g_2166 */
static volatile uint32_t * volatile * volatile g_2165 = &g_2166;/* VOLATILE GLOBAL g_2165 */
static volatile int8_t g_2219 = 0xB1L;/* VOLATILE GLOBAL g_2219 */
static uint8_t g_2270 = 0x22L;
static uint8_t g_2316[4][4][4] = {{{0xE0L,1UL,2UL,1UL},{0xE0L,0x8CL,0xA9L,0xFEL},{0xE0L,0x05L,2UL,0x05L},{0xE0L,0xFEL,0xA9L,0x8CL}},{{0xE0L,1UL,2UL,1UL},{0xE0L,0x8CL,0xA9L,0xFEL},{0xE0L,0x05L,2UL,0x05L},{0xE0L,0xFEL,0xA9L,0x8CL}},{{0xE0L,1UL,2UL,1UL},{0xE0L,0x8CL,0xA9L,0xFEL},{0xE0L,0x05L,2UL,0x05L},{0xE0L,0xFEL,0xA9L,0x8CL}},{{0xE0L,1UL,2UL,1UL},{0xE0L,0x8CL,0xA9L,0xFEL},{0xE0L,0x05L,2UL,0x05L},{0xE0L,0xFEL,0xA9L,0x8CL}}};
static uint32_t g_2318 = 0xB1FC3106L;
static int64_t ** const **g_2322 = &g_1584;
static volatile uint16_t * volatile *g_2373 = (void*)0;
static uint8_t g_2392 = 0x40L;
static union U0 g_2507[9][1][2] = {{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}},{{{0x1F740794L},{0x1F740794L}}}};


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static const int8_t  func_4(uint64_t  p_5, int8_t  p_6);
static int8_t  func_27(int32_t ** p_28, float  p_29);
static int32_t ** func_30(const uint32_t  p_31);
static int64_t  func_32(int32_t * p_33, uint16_t  p_34);
static int32_t * func_35(int32_t * const * p_36, int32_t ** p_37, int32_t  p_38, int32_t * const  p_39, int16_t  p_40);
static int16_t  func_43(float  p_44, uint16_t  p_45, int32_t * p_46, uint64_t  p_47, float  p_48);
static uint8_t  func_52(int64_t  p_53, int32_t  p_54, int32_t ** p_55);
static int64_t  func_56(uint32_t  p_57, int32_t ** const  p_58);
static int32_t  func_61(float  p_62);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_8 g_17 g_23 g_41 g_9 g_51 g_42 g_77 g_83 g_70 g_69 g_105 g_93 g_137 g_157 g_127 g_187 g_284 g_318 g_340 g_449 g_463 g_580.f1 g_525.f1 g_319.f0 g_590 g_591 g_686 g_576.f1 g_530.f0 g_1007 g_794 g_566.f1 g_624 g_625 g_626 g_1356 g_588.f1 g_1369 g_550.f0 g_569.f1 g_1181.f0 g_974 g_795 g_1539 g_592 g_567.f1 g_556.f0 g_623 g_973 g_1765 g_591.f0 g_529.f1 g_1692 g_1150 g_1151 g_538 g_1990 g_577.f1 g_1501 g_1589 g_538.f1 g_1098 g_1595 g_1897 g_736 g_1896 g_1349 g_555.f0 g_2047 g_833 g_562.f0 g_397 g_2063 g_2064 g_2069 g_568.f1 g_1588 g_1991 g_581.f0 g_563.f1 g_2121 g_553.f0 g_312 g_1696 g_462 g_2165 g_575.f1 g_1593 g_583.f1 g_1596 g_2219 g_1590 g_570.f1 g_559.f0 g_2047.f1 g_1586 g_1181.f1 g_2166 g_1106 g_526.f1 g_399 g_557.f1 g_2270 g_528.f1 g_537.f1 g_527.f1 g_529.f0 g_2318 g_1038 g_2373 g_1602 g_2392 g_899 g_563.f0 g_556.f1 g_586.f1 g_584.f1 g_578.f1 g_319.f1 g_2507 g_560.f0
 * writes: g_17 g_23 g_9 g_69 g_77 g_83 g_93 g_105 g_137 g_42 g_157 g_70 g_187 g_127 g_277 g_279 g_284 g_399 g_427 g_463 g_794 g_686 g_539.f1 g_537.f1 g_1369 g_565.f1 g_573.f1 g_818 g_462 g_592 g_1730 g_567.f1 g_1537 g_1692 g_573.f0 g_568.f0 g_1999 g_577.f1 g_1098 g_538.f1 g_585.f1 g_736 g_625 g_2064 g_1151 g_530.f1 g_569.f1 g_312 g_1501 g_1349 g_557.f1 g_555.f1 g_2318 g_2322 g_556.f1 g_319.f1
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int8_t l_7[8] = {0xA6L,5L,0xA6L,5L,0xA6L,5L,0xA6L,5L};
    uint32_t l_10 = 0xA754320EL;
    int16_t l_2059 = 0xD048L;
    uint32_t l_2082 = 0x1A903FBBL;
    uint8_t ***l_2084[4][5][10] = {{{&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,&g_1896},{&g_1896,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896},{&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0},{&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896},{&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,(void*)0}},{{&g_1896,(void*)0,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896},{&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,(void*)0,&g_1896,&g_1896},{&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0},{&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896},{&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,(void*)0,(void*)0}},{{&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,&g_1896},{(void*)0,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896},{(void*)0,&g_1896,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,&g_1896},{&g_1896,(void*)0,&g_1896,(void*)0,&g_1896,&g_1896,(void*)0,&g_1896,(void*)0,&g_1896},{(void*)0,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896}},{{&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,(void*)0,(void*)0,&g_1896,&g_1896,&g_1896},{&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896},{&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896},{&g_1896,&g_1896,&g_1896,&g_1896,(void*)0,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896},{&g_1896,&g_1896,(void*)0,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,&g_1896,(void*)0}}};
    int32_t l_2111 = 0xFB4C2D59L;
    int32_t l_2112 = 1L;
    int32_t l_2113 = (-10L);
    int32_t l_2114[8][2] = {{0x3210E623L,0x3210E623L},{0x7F1F1AB8L,0x3210E623L},{0x3210E623L,0x7F1F1AB8L},{0x3210E623L,0x3210E623L},{0x7F1F1AB8L,0x3210E623L},{0x3210E623L,0x7F1F1AB8L},{0x3210E623L,0x3210E623L},{0x7F1F1AB8L,0x3210E623L}};
    uint8_t l_2115 = 5UL;
    uint8_t l_2133 = 0xC1L;
    int64_t ****l_2158 = &g_1247;
    int64_t *****l_2157 = &l_2158;
    int8_t *l_2160 = &g_1349[2][4];
    int8_t **l_2159[8][7] = {{&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{(void*)0,(void*)0,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{(void*)0,(void*)0,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{(void*)0,(void*)0,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160},{(void*)0,(void*)0,&l_2160,&l_2160,&l_2160,&l_2160,&l_2160}};
    float l_2189 = (-0x3.5p-1);
    uint64_t *l_2198 = &g_1501[2];
    int8_t **l_2203 = (void*)0;
    const uint8_t l_2218 = 0UL;
    const uint8_t *l_2231 = &g_340;
    const uint8_t **l_2230 = &l_2231;
    uint32_t **l_2252 = &g_877;
    uint32_t *** const l_2251 = &l_2252;
    int32_t l_2253[1];
    uint64_t l_2254 = 18446744073709551610UL;
    int16_t l_2256 = 0x9502L;
    int64_t l_2400 = 0x6503F52795AC83D1LL;
    int32_t l_2418 = (-1L);
    float *l_2463 = &g_9[0];
    float **l_2462 = &l_2463;
    uint32_t l_2490 = 0x20F55D18L;
    union U0 * const **l_2508 = (void*)0;
    float l_2510 = 0x9.Cp-1;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_2253[i] = (-1L);
    if ((safe_div_func_int8_t_s_s(func_4(((l_7[6] > g_8) ^ l_10), (7L <= (safe_rshift_func_uint16_t_u_u((((((safe_lshift_func_int8_t_s_s((g_17 & (g_17 < 0xDFAD5BEFL)), 6)) <= 0x5AL) || ((g_17 >= g_17) >= 0x3D2717F4L)) > g_17) | g_17), 10)))), 0x81L)))
    { /* block id: 944 */
        int32_t l_2054 = 0x64BEEEDEL;
        float *l_2060 = &g_279;
        const int32_t ** const *l_2061 = (void*)0;
        uint16_t l_2062[6][5][8] = {{{65534UL,1UL,9UL,0xC321L,65535UL,0x129DL,0xE5E9L,65532UL},{65531UL,0xC321L,0xE95DL,1UL,4UL,0xC05DL,9UL,0x9468L},{0xB861L,0UL,9UL,0x9468L,9UL,0UL,0xB861L,0x129DL},{0xE95DL,0x8BEEL,65534UL,0x46BDL,65531UL,65535UL,0x0BBEL,0x8BEEL},{9UL,0xC05DL,65530UL,65535UL,65531UL,0x129DL,65535UL,0x9468L}},{{0xE95DL,0xA5DBL,65533UL,0x8BEEL,9UL,65526UL,65531UL,65526UL},{0xB861L,0x8BEEL,0xE5E9L,0x8BEEL,0xB861L,0xC321L,0x0BBEL,0x9468L},{0xED83L,1UL,9UL,65535UL,0x0BBEL,0UL,0xED83L,0x8BEEL},{0xE95DL,0x129DL,9UL,0x46BDL,65535UL,65526UL,0x0BBEL,0x129DL},{0x0BBEL,0xC05DL,0xE5E9L,0x9468L,65531UL,65532UL,65531UL,0x9468L}},{{65533UL,0xC05DL,65533UL,0x129DL,0x0BBEL,65526UL,65535UL,0x46BDL},{0xB861L,0x129DL,65530UL,0x8BEEL,0xED83L,0UL,0x0BBEL,65535UL},{0xB861L,1UL,65534UL,0x9468L,0x0BBEL,0xC321L,0xB861L,0x8BEEL},{65533UL,0x8BEEL,9UL,65526UL,65531UL,65526UL,9UL,0x8BEEL},{0x0BBEL,0xA5DBL,65530UL,0x9468L,65535UL,0x129DL,65531UL,65535UL}},{{0xE95DL,0xC05DL,0x9042L,0x8BEEL,0x0BBEL,65535UL,65531UL,0x46BDL},{0xED83L,0x8BEEL,65530UL,0x129DL,0xB861L,0UL,9UL,0x9468L},{0xB861L,0UL,9UL,0x9468L,9UL,0UL,0xB861L,0x129DL},{0xE95DL,0x8BEEL,65534UL,0x46BDL,65531UL,65535UL,0x0BBEL,0x8BEEL},{9UL,0xC05DL,65530UL,65535UL,65531UL,0x129DL,65535UL,0x9468L}},{{0xE95DL,0xA5DBL,65533UL,0x8BEEL,9UL,65526UL,65531UL,65526UL},{0xB861L,0x8BEEL,0xE5E9L,0x8BEEL,0xB861L,0xC321L,0x0BBEL,0x9468L},{0xED83L,1UL,9UL,65535UL,0x0BBEL,0UL,0xED83L,0x8BEEL},{0xE95DL,0x129DL,9UL,0x46BDL,65535UL,65526UL,0x0BBEL,0x129DL},{0x0BBEL,0xC05DL,0xE5E9L,0x9468L,65531UL,65532UL,65531UL,0x9468L}},{{65533UL,0xC05DL,65533UL,0x129DL,65533UL,1UL,65535UL,65535UL},{9UL,65535UL,4UL,65526UL,65534UL,65532UL,65533UL,0xBD8CL},{9UL,0x129DL,0xE95DL,0xA5DBL,65533UL,0x8BEEL,9UL,65526UL},{0xE5E9L,65526UL,0x9042L,1UL,0xED83L,1UL,0x9042L,65526UL},{65533UL,0xC321L,4UL,0xA5DBL,65535UL,65535UL,0xED83L,0xBD8CL}}};
        int8_t ***l_2081 = (void*)0;
        int32_t l_2083 = 0xDC6D656BL;
        int64_t l_2087 = (-1L);
        int32_t l_2106 = 0xC29DA918L;
        int32_t *l_2107 = &g_23;
        int32_t *l_2108 = (void*)0;
        int32_t *l_2109 = &g_686[0];
        int32_t *l_2110[8][8] = {{&g_105,&g_1692,&l_2083,&g_1696,&g_1696,&l_2083,&g_1692,&g_105},{&g_70,&g_1696,&l_2083,&g_686[1],&g_105,&g_1692,&g_1696,&l_2083},{(void*)0,&g_105,(void*)0,&g_686[1],&g_686[1],&g_1692,&g_70,&l_2106},{&g_1692,&g_1696,&l_2106,&l_2083,&g_23,&l_2083,&l_2106,&g_1696},{&l_2106,&g_1692,&l_2106,&l_2083,&g_23,&l_2106,(void*)0,&g_686[1]},{&g_70,(void*)0,&l_2083,(void*)0,&l_2106,&g_1696,(void*)0,&g_105},{&g_686[1],(void*)0,&l_2106,&l_2106,&g_105,&g_105,&l_2106,&l_2106},{&g_105,&g_105,&l_2106,&l_2106,(void*)0,&g_686[1],&g_70,&g_686[1]}};
        int i, j, k;
        if (((safe_rshift_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((((((**g_623) = (*g_624)) != (((safe_add_func_float_f_f(l_7[6], ((g_9[1] != 0xC.5CA4EDp-3) >= (g_2047 , (((*l_2060) = ((safe_mul_func_float_f_f((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(l_2054, (0x9.1D1C39p-28 <= ((((safe_sub_func_uint8_t_u_u((((((*g_794) , (safe_sub_func_uint32_t_u_u(((0L >= 0xC9L) <= l_2059), l_2054))) , 0x79FFL) || g_833) ^ 0x153F130703AF365ALL), 0UL)) ^ 0xB6L) == 4294967295UL) , l_7[3])))) < l_2054), 0x9.Fp+1)), 0x1.Ap+1)) != 0x0.3p+1)) > g_562[0].f0))))) , 18446744073709551615UL) , l_2061)) , g_397) >= 0x159BL) & g_538.f1), 0x39L)), l_2062[4][1][5])) != 0xE68CL))
        { /* block id: 947 */
            uint32_t l_2065 = 0xE9572595L;
            int32_t l_2066 = 5L;
            uint16_t *l_2074 = (void*)0;
            uint16_t *l_2075 = &g_530[7][1][1].f1;
            const uint32_t *l_2080 = &g_312;
            const uint32_t **l_2079 = &l_2080;
            const uint32_t ***l_2078 = &l_2079;
            (*g_2063) = &g_1151;
            (*g_2064) = (*g_2064);
            l_2066 = l_2065;
            l_2083 = (safe_rshift_func_int8_t_s_s((g_2069 , ((safe_div_func_uint64_t_u_u(0x44C4DD460E2F6830LL, (safe_div_func_uint16_t_u_u(((*l_2075) = (l_2065 > g_568.f1)), ((l_2066 >= ((((((l_2065 < (((safe_mod_func_int64_t_s_s(((*g_1588) ^= ((l_2078 == ((0xE.11D2BBp+55 < l_7[7]) , &g_449)) & 1L)), l_2066)) , 0L) & l_7[2])) != l_2066) == 0UL) & 0UL) , (*g_1990)) == l_2081)) , 0xB15BL))))) < l_2082)), 0));
        }
        else
        { /* block id: 954 */
            float l_2098 = 0xA.CA9462p+76;
            uint32_t *l_2099 = (void*)0;
            uint32_t *l_2100 = &l_10;
            const int32_t l_2101[4] = {0xEAEDAEC9L,0xEAEDAEC9L,0xEAEDAEC9L,0xEAEDAEC9L};
            int32_t l_2102[10] = {4L,0L,4L,4L,0L,4L,4L,0L,4L,4L};
            int32_t l_2103[2];
            int32_t l_2104[10];
            int32_t *l_2105 = &g_105;
            int i;
            for (i = 0; i < 2; i++)
                l_2103[i] = 0xBF1C7783L;
            for (i = 0; i < 10; i++)
                l_2104[i] = 0xC71A4496L;
            (*l_2105) = ((l_2104[3] = (((**g_1896) , l_2084[2][0][1]) == ((safe_rshift_func_uint16_t_u_s(((l_2087 , (safe_add_func_uint32_t_u_u(((l_2103[1] |= (safe_rshift_func_uint8_t_u_u(((g_581.f0 < (safe_mod_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u(((l_2083 = (((g_93 |= ((l_2102[4] = ((**g_973) = ((safe_div_func_int32_t_s_s(l_2083, l_2059)) ^ (((*l_2100) = 4294967295UL) > (((l_7[6] , (l_7[7] & l_2101[2])) , 0xC2L) != (*g_1897)))))) || 0x66L)) == g_563.f1) > l_2101[3])) > 18446744073709551611UL), l_2082)) , 0x4898L), (-9L)))) , l_7[0]), 0))) != l_2101[1]), 0L))) , 0UL), l_2101[2])) , l_2084[0][3][3]))) < 1L);
        }
        l_2115--;
        for (g_569.f1 = 0; (g_569.f1 > 2); ++g_569.f1)
        { /* block id: 967 */
            int32_t * const l_2120[9] = {&l_2083,&g_23,&l_2083,&g_23,&l_2083,&g_23,&l_2083,&g_23,&l_2083};
            int i;
            (*g_2121) = l_2120[0];
        }
    }
    else
    { /* block id: 970 */
        return g_553.f0;
    }
    for (g_1369 = 0; (g_1369 <= 42); g_1369++)
    { /* block id: 975 */
        int32_t *l_2124 = &l_2113;
        int32_t *l_2125 = (void*)0;
        int32_t *l_2126 = &g_1696;
        int32_t l_2127 = 0x8B458789L;
        int32_t *l_2128 = (void*)0;
        int32_t l_2129 = (-1L);
        int32_t *l_2130 = &g_686[0];
        int32_t *l_2131 = &g_686[1];
        int32_t *l_2132[2];
        uint32_t **l_2187 = (void*)0;
        uint32_t ***l_2186 = &l_2187;
        uint32_t *l_2188 = &l_10;
        uint64_t *l_2190 = (void*)0;
        uint64_t *l_2191 = (void*)0;
        int i;
        for (i = 0; i < 2; i++)
            l_2132[i] = &l_2129;
        l_2133--;
        for (g_312 = 11; (g_312 < 35); g_312++)
        { /* block id: 979 */
            uint8_t l_2138 = 6UL;
            l_2138++;
        }
        (*l_2131) = (((((void*)0 == (*g_1896)) == (safe_rshift_func_uint8_t_u_s(((*g_1897)++), 3))) && (safe_div_func_uint8_t_u_u(((l_2115 , (((((((l_2114[3][1] ^ (((safe_lshift_func_uint8_t_u_s((*g_1539), ((l_2115 > (((safe_mul_func_int8_t_s_s(((safe_mod_func_uint8_t_u_u((*g_1897), ((((safe_mod_func_uint64_t_u_u((safe_div_func_uint16_t_u_u((0xAD7194924B88A5AALL && ((l_2157 == &g_1731[3][1]) != 5L)), l_2114[5][0])), 0xC86A4DD70348D8DALL)) , (void*)0) != &g_1896) , 1UL))) ^ (*l_2126)), (*g_1897))) || (*g_1589)) <= 246UL)) , l_10))) , l_2159[3][3]) != (void*)0)) , (-0x1.Ap+1)) , l_2111) > g_563.f1) == 0x4.24A09Ep+6) , (void*)0) != (void*)0)) && l_2082), 0x10L))) && 3L);
        (*l_2131) = (((((safe_lshift_func_int16_t_s_u(l_2114[7][1], 0)) , (((safe_rshift_func_int8_t_s_u((65535UL & ((void*)0 != g_2165)), 1)) <= ((safe_mod_func_uint32_t_u_u(0UL, ((safe_mul_func_uint8_t_u_u(((void*)0 != &g_1897), (((0x44L <= (safe_div_func_int64_t_s_s((l_2112 = ((*g_1596) |= (safe_div_func_int8_t_s_s(((**g_973) |= ((((*l_2124) = (~(((+((safe_mod_func_uint64_t_u_u((safe_sub_func_int64_t_s_s(((((*g_1593) = (((g_77 = (safe_div_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((~(((((*l_2188) = ((((*l_2186) = &g_877) == &g_877) & 0xC8L)) & l_2114[4][0]) , l_2059) < l_2082)), g_575.f1)), (*l_2124)))) & l_2082) < l_2111)) | l_2115) , 0L), g_583.f1)), l_2115)) , l_2059)) & (*l_2124)) != 0x3FBCC3F5FFBE8CDCLL))) == (-9L)) != l_2111)), l_7[2])))), (-1L)))) && 0x1FL) != 0UL))) && (*l_2126)))) || l_2112)) && l_10)) == l_2133) , g_8) != l_7[4]);
    }
    for (g_567.f1 = 0; (g_567.f1 > 21); g_567.f1++)
    { /* block id: 996 */
        uint16_t l_2194[7][5][3] = {{{0x9EB0L,0UL,1UL},{0UL,4UL,0UL},{1UL,0x9EB0L,1UL},{5UL,0xC74BL,0x1122L},{65530UL,4UL,4UL}},{{65528UL,4UL,1UL},{65530UL,5UL,4UL},{5UL,65535UL,0UL},{1UL,0x8684L,4UL},{0UL,65535UL,65528UL}},{{0x9EB0L,5UL,1UL},{65535UL,4UL,65535UL},{1UL,4UL,1UL},{65528UL,0xC74BL,65528UL},{65530UL,0x9EB0L,4UL}},{{0x1122L,4UL,0UL},{65530UL,0UL,4UL},{65528UL,65535UL,1UL},{1UL,1UL,4UL},{65535UL,65535UL,0x1122L}},{{0x9EB0L,0UL,1UL},{0UL,4UL,0UL},{1UL,0x9EB0L,0UL},{0x1122L,65535UL,65535UL},{0xCB42L,4UL,4UL}},{{0UL,0xC74BL,0x832BL},{0xCB42L,1UL,1UL},{0x1122L,65535UL,3UL},{0x9EB0L,4UL,4UL},{1UL,65535UL,0UL}},{{65530UL,1UL,0UL},{0UL,0xC74BL,0UL},{0x9EB0L,4UL,0UL},{65528UL,65535UL,0UL},{0xCB42L,65530UL,4UL}}};
        int32_t *l_2195[6][1][7] = {{{&l_2111,&l_2113,&g_686[0],&l_2114[7][1],(void*)0,&g_686[0],&g_686[0]}},{{&g_686[1],(void*)0,&g_23,(void*)0,&g_686[1],&l_2114[7][1],&l_2111}},{{(void*)0,&g_1696,&g_23,&l_2112,(void*)0,&g_23,&l_2112}},{{&l_2112,&g_23,&g_686[0],&g_23,&g_23,&g_686[0],&g_23}},{{(void*)0,&l_2112,&g_1696,&l_2111,&l_2113,&g_686[0],&l_2114[7][1]}},{{&g_686[1],&l_2114[7][1],(void*)0,&g_23,(void*)0,&g_23,(void*)0}}};
        int i, j, k;
        l_2111 = ((*g_1007) &= l_2194[5][2][0]);
    }
    if (((safe_lshift_func_int16_t_s_s(((l_10 >= (((*l_2198) |= (*g_794)) && (safe_rshift_func_uint16_t_u_s((l_2115 | ((*g_1590) = (safe_add_func_int8_t_s_s((l_2203 != (void*)0), ((*l_2160) |= ((((safe_rshift_func_int8_t_s_s((safe_mod_func_uint32_t_u_u((l_7[4] || ((0L && (safe_div_func_uint64_t_u_u(((safe_rshift_func_uint8_t_u_s(0x21L, (safe_mul_func_int16_t_s_s((safe_add_func_uint64_t_u_u((((safe_add_func_uint64_t_u_u(l_2218, g_2219)) || l_2133) >= 4L), 0xC0D99D073BE02D87LL)), l_2113)))) | l_2082), 6UL))) <= (*g_794))), 4294967295UL)), l_2059)) && 0x9EF4E27AA78F9FF8LL) & l_2082) && l_7[6])))))), 5)))) != 0L), 14)) >= g_570.f1))
    { /* block id: 1003 */
        uint8_t **l_2232 = &g_1897;
        int32_t ** const **l_2243 = (void*)0;
        int32_t ** const *** const l_2242[7] = {&l_2243,&l_2243,&l_2243,&l_2243,&l_2243,&l_2243,&l_2243};
        int32_t ** const *** const l_2244 = &l_2243;
        float l_2255 = 0x1.2E89C4p+81;
        const uint32_t *l_2302 = &l_2082;
        const uint32_t **l_2301 = &l_2302;
        const uint32_t *** const l_2300 = &l_2301;
        uint8_t * const l_2315 = &g_2316[1][1][3];
        uint8_t * const *l_2314 = &l_2315;
        uint8_t * const **l_2313[5];
        int32_t l_2325 = 5L;
        uint8_t l_2326 = 0x95L;
        int32_t * const *l_2329 = &g_69;
        float l_2341 = (-0x8.8p-1);
        uint32_t l_2401[9][10][1] = {{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}},{{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L},{0x71B47B37L},{0xC5E8F6BFL},{0x71B47B37L},{0x15F26BC2L},{0x15F26BC2L}}};
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_2313[i] = &l_2314;
        if ((safe_mul_func_uint8_t_u_u(((0x9A85531E4746F022LL <= (((safe_lshift_func_int16_t_s_s(0x24E9L, 7)) == (((*g_794) >= (safe_sub_func_uint32_t_u_u(((safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((((l_2230 != l_2232) || ((*l_2160) = (safe_rshift_func_uint16_t_u_s((((((safe_rshift_func_int16_t_s_s((((!(safe_rshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s((l_2242[2] == l_2244), (safe_mul_func_uint16_t_u_u((l_2218 || (((((safe_rshift_func_uint16_t_u_u(((safe_sub_func_int32_t_s_s(((*g_1007) &= (l_2253[0] = ((void*)0 == l_2251))), l_2254)) , g_559.f0), 7)) , g_2047.f1) , l_7[6]) , 9UL) >= 0x192473D817024210LL)), g_833)))), 6))) & (*g_1586)) ^ l_2082), l_2059)) , 1UL) & l_2114[1][0]) < (-6L)) ^ g_1181.f1), l_2112)))) & (*g_1897)) , l_2253[0]), (*g_974))), l_2256)) , (**g_2165)), 1L))) >= l_7[1])) != g_526.f1)) | l_2218), (*g_974))))
        { /* block id: 1007 */
            return g_399;
        }
        else
        { /* block id: 1009 */
            float l_2259[7] = {(-0x7.3p-1),(-0x7.3p-1),(-0x7.3p-1),(-0x7.3p-1),(-0x7.3p-1),(-0x7.3p-1),(-0x7.3p-1)};
            int32_t l_2262 = 0xFAB0424BL;
            int32_t l_2275[7] = {0xBB939DD3L,9L,0xBB939DD3L,0xBB939DD3L,9L,0xBB939DD3L,0xBB939DD3L};
            uint32_t ***l_2303 = &l_2252;
            int16_t l_2377[5];
            uint16_t l_2399 = 0UL;
            uint64_t *l_2404 = &g_1098[1][7][0];
            uint64_t l_2413[3];
            int32_t **l_2416 = &g_42;
            int i;
            for (i = 0; i < 5; i++)
                l_2377[i] = 0L;
            for (i = 0; i < 3; i++)
                l_2413[i] = 0UL;
            for (g_557.f1 = 0; (g_557.f1 <= 1); g_557.f1 += 1)
            { /* block id: 1012 */
                uint32_t *l_2263[1][3];
                uint32_t l_2271 = 0x97D8C0E5L;
                int32_t l_2276[2][2] = {{(-6L),(-6L)},{(-6L),(-6L)}};
                int32_t l_2297[5] = {0x10710B50L,0x10710B50L,0x10710B50L,0x10710B50L,0x10710B50L};
                int32_t l_2308 = 0x9F63118FL;
                int64_t l_2323 = 0xDAFF255768C8D9C1LL;
                uint32_t ***l_2346 = &l_2252;
                uint16_t *l_2364 = &g_537.f1;
                uint16_t **l_2363 = &l_2364;
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_2263[i][j] = &g_312;
                }
                for (g_577.f1 = 0; (g_577.f1 <= 1); g_577.f1 += 1)
                { /* block id: 1015 */
                    int i;
                    return g_686[g_557.f1];
                }
                if (((safe_sub_func_int8_t_s_s((*g_974), (((*g_1595) = (safe_mod_func_int16_t_s_s(((g_312 &= l_2262) , ((&g_463 == ((**g_1896) , &g_463)) & l_2262)), (safe_add_func_int64_t_s_s((safe_div_func_uint64_t_u_u((safe_add_func_uint8_t_u_u(((((*g_1539) = (l_2271 = (l_2256 >= g_2270))) ^ (~(safe_sub_func_int8_t_s_s((l_2275[4] ^= (((**g_623) = (**g_623)) == &g_1542)), l_2276[0][0])))) || g_528.f1), 0UL)), (*g_794))), l_2111))))) == 0x64A7F877C81690F0LL))) == l_2262))
                { /* block id: 1024 */
                    uint16_t *l_2282[6] = {&g_572.f1,&g_572.f1,(void*)0,&g_572.f1,&g_572.f1,(void*)0};
                    int32_t l_2296 = 0x062B6619L;
                    uint32_t *l_2307 = &g_1369;
                    uint8_t * const *l_2310 = (void*)0;
                    uint8_t * const **l_2309 = &l_2310;
                    uint8_t * const **l_2317 = (void*)0;
                    int16_t l_2324[1];
                    int32_t **l_2332 = &g_69;
                    int8_t l_2352[8][3][8] = {{{1L,7L,1L,(-1L),(-1L),1L,7L,1L},{1L,(-1L),(-3L),(-1L),1L,1L,(-1L),(-3L)},{1L,1L,(-1L),(-3L),(-1L),1L,1L,(-1L)}},{{1L,(-1L),(-1L),1L,7L,1L,(-1L),(-1L)},{(-1L),7L,(-3L),(-3L),7L,(-1L),7L,(-3L)},{1L,7L,1L,(-1L),(-1L),1L,7L,1L}},{{1L,(-1L),(-3L),(-1L),1L,1L,(-1L),(-3L)},{1L,1L,(-1L),(-3L),(-1L),1L,1L,(-1L)},{1L,(-1L),(-1L),1L,7L,1L,(-1L),(-1L)}},{{(-1L),7L,(-3L),(-3L),7L,(-1L),7L,(-3L)},{1L,7L,1L,(-1L),(-1L),1L,7L,1L},{1L,(-1L),(-3L),(-1L),1L,1L,(-1L),(-3L)}},{{1L,1L,(-1L),(-3L),(-1L),1L,1L,(-1L)},{1L,(-1L),(-1L),1L,7L,1L,(-1L),(-1L)},{(-1L),7L,(-3L),(-3L),7L,(-1L),7L,(-3L)}},{{1L,7L,1L,(-1L),(-1L),1L,7L,1L},{1L,(-1L),(-3L),(-1L),1L,1L,(-1L),(-3L)},{1L,1L,(-1L),(-3L),(-1L),1L,1L,(-1L)}},{{1L,(-1L),(-1L),1L,7L,1L,(-1L),(-1L)},{(-1L),7L,(-3L),(-3L),7L,(-1L),7L,(-3L)},{1L,7L,1L,(-1L),(-1L),1L,7L,1L}},{{1L,(-1L),(-3L),(-1L),1L,1L,(-1L),(-3L)},{(-1L),(-1L),1L,7L,1L,(-1L),(-1L),1L},{(-3L),1L,1L,(-3L),1L,(-3L),1L,1L}}};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_2324[i] = 0x9479L;
                    g_686[g_557.f1] ^= (~((safe_rshift_func_uint8_t_u_s((((l_2296 = ((safe_lshift_func_uint16_t_u_u(((*g_1897) <= (((g_537.f1++) >= (safe_div_func_int64_t_s_s(0L, (*g_794)))) & ((*g_590) , (~(0xD7DAL & ((safe_add_func_uint16_t_u_u((g_555.f1 = 65535UL), g_2270)) >= ((safe_mul_func_uint16_t_u_u(((((safe_lshift_func_int8_t_s_s(0L, 3)) || 1L) , g_527.f1) < l_2275[4]), 0xAC01L)) >= 0xB1F3L))))))), l_2296)) ^ (*g_794))) == l_2297[4]) != 4294967295UL), l_2059)) | g_70));
                    if ((safe_mul_func_uint8_t_u_u(((l_2300 != l_2303) && ((safe_unary_minus_func_uint16_t_u(((((((safe_rshift_func_uint16_t_u_u(0x09D2L, 2)) | (-1L)) < ((*g_794) == (((*l_2307) |= 0x7C4EACE1L) | l_2133))) , ((l_2308 > 1L) | (**g_973))) , 65527UL) >= (-7L)))) | g_529.f0)), (**g_973))))
                    { /* block id: 1030 */
                        uint8_t * const **l_2312[5][10][5] = {{{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,(void*)0,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,(void*)0,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,(void*)0,&l_2310}},{{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,(void*)0,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,(void*)0,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310}},{{&l_2310,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,(void*)0,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310}},{{&l_2310,(void*)0,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,(void*)0,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,(void*)0,&l_2310,(void*)0},{&l_2310,&l_2310,&l_2310,&l_2310,(void*)0},{&l_2310,(void*)0,(void*)0,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310}},{{(void*)0,&l_2310,(void*)0,(void*)0,&l_2310},{&l_2310,(void*)0,&l_2310,&l_2310,(void*)0},{&l_2310,&l_2310,&l_2310,&l_2310,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,(void*)0},{&l_2310,(void*)0,&l_2310,&l_2310,&l_2310},{(void*)0,(void*)0,&l_2310,&l_2310,&l_2310},{&l_2310,(void*)0,&l_2310,(void*)0,&l_2310},{&l_2310,&l_2310,&l_2310,&l_2310,(void*)0},{&l_2310,(void*)0,(void*)0,&l_2310,(void*)0},{&l_2310,(void*)0,&l_2310,&l_2310,&l_2310}}};
                        uint8_t * const ***l_2311[1][8][6] = {{{&l_2312[0][2][4],(void*)0,&l_2312[3][6][4],&l_2312[0][2][4],&l_2312[3][6][4],&l_2312[3][6][4]},{&l_2312[3][6][4],(void*)0,(void*)0,&l_2312[3][6][4],&l_2312[3][6][4],&l_2312[3][6][4]},{&l_2312[1][2][1],(void*)0,&l_2312[3][6][4],&l_2312[1][2][1],&l_2312[3][6][4],(void*)0},{&l_2312[0][2][4],(void*)0,&l_2312[3][6][4],&l_2312[0][2][4],&l_2312[3][6][4],&l_2312[3][6][4]},{&l_2312[3][6][4],(void*)0,(void*)0,&l_2312[3][6][4],&l_2312[3][6][4],&l_2312[3][6][4]},{&l_2312[1][2][1],(void*)0,&l_2312[3][6][4],&l_2312[1][2][1],&l_2312[3][6][4],(void*)0},{&l_2312[0][2][4],(void*)0,&l_2312[3][6][4],&l_2312[0][2][4],&l_2312[3][6][4],&l_2312[3][6][4]},{&l_2312[3][6][4],(void*)0,(void*)0,&l_2312[3][6][4],&l_2312[3][6][4],&l_2312[3][6][4]}}};
                        int i, j, k;
                        l_2317 = (l_2313[4] = (l_2309 = (void*)0));
                        --g_2318;
                        (*g_2064) = (**g_2063);
                    }
                    else
                    { /* block id: 1036 */
                        int64_t ** const ***l_2321 = (void*)0;
                        int32_t * const *l_2330[9][2][10] = {{{&g_42,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_42,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_42,&g_69,&g_69,&g_69,&g_42,&g_69,&g_69}},{{&g_42,&g_42,&g_69,&g_69,&g_42,&g_42,&g_42,&g_42,&g_42,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_42,&g_69,&g_69,&g_42,&g_69,&g_42}},{{&g_69,&g_42,&g_69,&g_42,&g_69,&g_42,&g_42,&g_42,&g_42,&g_42},{&g_42,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_42,&g_69,&g_69}},{{&g_69,&g_69,&g_69,&g_42,&g_69,&g_69,&g_69,&g_42,&g_69,&g_69},{&g_42,&g_69,(void*)0,&g_42,&g_69,(void*)0,&g_69,(void*)0,&g_69,&g_42}},{{(void*)0,&g_69,(void*)0,&g_69,&g_42,&g_69,&g_69,(void*)0,&g_69,&g_69},{&g_42,&g_69,(void*)0,&g_69,&g_69,&g_42,&g_69,(void*)0,&g_42,&g_42}},{{(void*)0,&g_42,(void*)0,&g_69,&g_69,(void*)0,&g_42,(void*)0,&g_42,&g_69},{&g_69,&g_69,(void*)0,&g_42,&g_42,&g_42,&g_69,(void*)0,&g_69,&g_69}},{{&g_42,&g_69,(void*)0,&g_42,&g_69,(void*)0,&g_69,(void*)0,&g_69,&g_42},{(void*)0,&g_69,(void*)0,&g_69,&g_42,&g_69,&g_69,(void*)0,&g_69,&g_69}},{{&g_42,&g_69,(void*)0,&g_69,&g_69,&g_42,&g_69,(void*)0,&g_42,&g_42},{(void*)0,&g_42,(void*)0,&g_69,&g_69,(void*)0,&g_42,(void*)0,&g_42,&g_69}},{{&g_69,&g_69,(void*)0,&g_42,&g_42,&g_42,&g_69,(void*)0,&g_69,&g_69},{&g_42,&g_69,(void*)0,&g_42,&g_69,(void*)0,&g_69,(void*)0,&g_69,&g_42}}};
                        int32_t ***l_2331[9][8] = {{(void*)0,&g_626,&g_626,(void*)0,&g_626,&g_626,&g_1542,&g_626},{&g_1542,&g_626,(void*)0,&g_626,&g_626,&g_626,(void*)0,&g_626},{&g_626,&g_626,&g_626,(void*)0,&g_626,&g_626,&g_1542,&g_1542},{&g_626,&g_626,&g_1542,&g_1542,&g_626,&g_626,&g_1542,&g_626},{&g_1542,&g_1542,&g_626,&g_626,(void*)0,&g_1542,(void*)0,&g_626},{(void*)0,&g_1542,(void*)0,&g_626,&g_626,&g_1542,&g_1542,&g_626},{&g_1542,&g_626,&g_626,&g_1542,&g_1542,&g_626,&g_626,&g_1542},{&g_1542,&g_626,&g_626,(void*)0,&g_626,&g_626,&g_626,&g_626},{(void*)0,&g_626,&g_626,&g_626,(void*)0,&g_626,&g_1542,&g_626}};
                        uint32_t ****l_2347 = &l_2346;
                        int i, j, k;
                        g_2322 = &g_1584;
                        l_2326++;
                        (***g_624) = func_35(l_2329, (*g_625), (l_7[6] < (1L >= l_2324[0])), func_35(l_2330[1][1][4], (l_2332 = (void*)0), (((l_2275[4] , ((safe_sub_func_uint64_t_u_u((safe_add_func_uint32_t_u_u(((safe_sub_func_uint32_t_u_u(((safe_div_func_int8_t_s_s((((l_2059 == l_2324[0]) , 4294967292UL) | 4UL), 0x78L)) , 0x228DD22BL), (**g_2165))) , 0x00C1501BL), g_77)), (*g_794))) != 1UL)) , g_686[g_557.f1]) <= (-5L)), &l_2275[6], l_2271), l_2113);
                        l_2276[0][0] |= (safe_div_func_uint16_t_u_u((((*g_626) = &l_2262) != ((((safe_lshift_func_uint8_t_u_s((**g_1896), 0)) <= (((((*l_2347) = l_2346) == &l_2301) , &l_2114[2][0]) != &l_2114[7][1])) > (0L < ((*g_2166) <= (safe_sub_func_uint64_t_u_u(((*l_2198) = (safe_mul_func_int8_t_s_s(l_2256, 0xACL))), l_2352[5][1][6]))))) , &l_2275[6])), g_1038));
                    }
                }
                else
                { /* block id: 1046 */
                    uint32_t *l_2366 = &l_10;
                    int32_t l_2374 = 0x0F0817A6L;
                    float l_2378 = 0x4.F8FB1Bp+81;
                    int32_t l_2380 = 0x25E738FCL;
                    int32_t l_2381 = 0x145C3B2CL;
                    if ((safe_add_func_int32_t_s_s(l_2275[4], (((safe_rshift_func_uint8_t_u_u((((*l_2198) = l_2276[1][0]) ^ (safe_lshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u((safe_mod_func_int32_t_s_s(((**g_2064) , (l_2363 == (((~1UL) == (((*l_2366) = 1UL) < (((safe_add_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(18446744073709551608UL, (safe_mod_func_int8_t_s_s(1L, 0x0AL)))), (*g_1897))) & l_2262) <= (*g_2166)))) , g_2373))), l_2374)), 8L)), l_2374))), l_2308)) >= (**g_1896)) != (*g_1602)))))
                    { /* block id: 1049 */
                        int16_t l_2375 = 6L;
                        (*g_1007) ^= l_2375;
                    }
                    else
                    { /* block id: 1051 */
                        int32_t l_2376 = (-7L);
                        int32_t l_2379 = 0x84E55AE3L;
                        int32_t l_2382 = 5L;
                        uint8_t l_2383 = 0x94L;
                        l_2383--;
                    }
                    for (l_2115 = 0; (l_2115 <= 23); l_2115++)
                    { /* block id: 1056 */
                        uint64_t l_2388 = 1UL;
                        if (l_2388)
                            break;
                        if (l_10)
                            break;
                        l_2374 = (((safe_div_func_int8_t_s_s((+0xF9L), (g_2392 , (safe_sub_func_uint32_t_u_u((l_2380 | ((((((safe_lshift_func_uint8_t_u_u((((((l_2388 == g_899[8][1]) | (((*g_590) , l_2275[1]) == (((safe_div_func_uint8_t_u_u(l_2111, (*g_974))) ^ (-1L)) != l_2388))) != (**g_973)) , l_2374) , l_2133), 7)) | l_2308) , l_2297[2]) & l_2399) == l_2400) || l_2388)), l_2297[3]))))) && 0L) , l_2401[3][0][0]);
                    }
                }
            }
            if (((l_2399 & (safe_sub_func_int32_t_s_s((0xF708C8CFA0B5EE04LL < ((*l_2404) |= ((*l_2198) = (*g_794)))), l_10))) & (l_2275[4] = l_2377[3])))
            { /* block id: 1066 */
                float l_2406[8][7] = {{0x2.FAC14Ap+10,(-0x1.Dp-1),0x2.FAC14Ap+10,(-0x1.Dp-1),0x2.FAC14Ap+10,(-0x1.Dp-1),0x2.FAC14Ap+10},{(-0x9.Dp+1),(-0x9.Dp+1),0x0.2p-1,0x0.2p-1,(-0x9.Dp+1),(-0x9.Dp+1),0x0.2p-1},{(-0x10.2p+1),(-0x1.Dp-1),(-0x10.2p+1),(-0x1.Dp-1),(-0x10.2p+1),(-0x1.Dp-1),(-0x10.2p+1)},{(-0x9.Dp+1),0x0.2p-1,0x0.2p-1,(-0x9.Dp+1),(-0x9.Dp+1),0x0.2p-1,0x0.2p-1},{0x2.FAC14Ap+10,(-0x1.Dp-1),0x2.FAC14Ap+10,(-0x1.Dp-1),0x2.FAC14Ap+10,(-0x1.Dp-1),0x2.FAC14Ap+10},{(-0x9.Dp+1),(-0x9.Dp+1),0x0.2p-1,0x0.2p-1,(-0x9.Dp+1),(-0x9.Dp+1),0x0.2p-1},{(-0x10.2p+1),(-0x1.Dp-1),(-0x10.2p+1),(-0x1.Dp-1),(-0x10.2p+1),(-0x1.Dp-1),(-0x10.2p+1)},{(-0x9.Dp+1),0x0.2p-1,0x0.2p-1,(-0x9.Dp+1),(-0x9.Dp+1),0x0.2p-1,0x0.2p-1}};
                int32_t l_2408 = 0x73CB7F2AL;
                int32_t l_2411 = 0xA9E7D6A4L;
                int32_t l_2412 = (-9L);
                uint16_t l_2440 = 0x4731L;
                int i, j;
                for (g_557.f1 = 0; (g_557.f1 <= 3); g_557.f1 += 1)
                { /* block id: 1069 */
                    float l_2405 = 0x8.0E41C5p+92;
                    int32_t l_2407 = 0x62F3755EL;
                    int32_t l_2410 = 0x5F381645L;
                    uint32_t *l_2438[4] = {&l_2401[3][0][0],&l_2401[3][0][0],&l_2401[3][0][0],&l_2401[3][0][0]};
                    uint8_t l_2442 = 1UL;
                    int i;
                    for (g_567.f1 = 0; (g_567.f1 <= 3); g_567.f1 += 1)
                    { /* block id: 1072 */
                        int8_t l_2409 = (-1L);
                        int32_t **l_2417 = &g_42;
                        int i, j;
                        l_2413[2]++;
                        (****g_623) = func_35(func_30(g_1349[g_567.f1][g_557.f1]), l_2416, l_2114[7][1], ((*l_2417) = &l_2410), l_2407);
                    }
                    if (l_2418)
                        continue;
                    l_2275[1] &= (l_2113 = ((*g_1007) = l_2408));
                    for (l_2082 = 0; (l_2082 <= 3); l_2082 += 1)
                    { /* block id: 1083 */
                        int16_t **l_2432 = &g_1999[1][1];
                        int16_t ***l_2431 = &l_2432;
                        int64_t **l_2437 = &g_1595;
                        uint16_t *l_2439 = (void*)0;
                        int32_t l_2441 = 0L;
                        (***g_624) = func_35((**g_624), (((*l_2404) = (((l_2412 , ((*l_2198) = (safe_div_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_u((safe_mod_func_int32_t_s_s(((safe_sub_func_uint8_t_u_u(((g_556.f1 ^= (((safe_mul_func_int8_t_s_s(l_2411, (safe_rshift_func_uint16_t_u_u((((((*l_2431) = &g_1999[1][1]) != (void*)0) , ((l_2410 = (safe_rshift_func_int8_t_s_s(((((void*)0 != l_2437) , l_2410) | ((l_2438[3] == (*l_2301)) >= 0xB37F6C35L)), (**g_973)))) & 0xFF7BL)) , g_563.f0), 14)))) && l_2112) && 0xC6ECEA61L)) , 0x33L), l_2411)) != l_2407), 0x73A6C0A4L)), l_2408)) | l_2407), l_2112)))) | (*g_1595)) ^ (**g_1896))) , (**g_624)), l_2440, &l_2408, l_2441);
                        if (l_2442)
                            break;
                    }
                }
                l_2113 &= 1L;
                (*g_626) = &l_2113;
            }
            else
            { /* block id: 1095 */
                uint16_t l_2443 = 0UL;
                l_2443++;
            }
        }
    }
    else
    { /* block id: 1099 */
        uint8_t l_2452 = 0x55L;
        uint32_t l_2464 = 0x2F04D20AL;
        int32_t l_2501 = (-5L);
        int32_t l_2502 = (-5L);
        int32_t l_2503 = 0x77DDE24FL;
        for (l_2133 = 0; (l_2133 == 23); ++l_2133)
        { /* block id: 1102 */
            const uint32_t l_2453 = 0UL;
            int32_t l_2476 = 0x2DA7F7F3L;
            uint64_t l_2504 = 18446744073709551615UL;
            if ((safe_lshift_func_uint16_t_u_s((((safe_sub_func_int8_t_s_s((**g_973), l_2452)) | (((**g_1896) != (l_2453 , 0xC4L)) | (1UL > (safe_add_func_int32_t_s_s(((((safe_div_func_uint32_t_u_u(((l_2452 , ((safe_mul_func_float_f_f((safe_mul_func_float_f_f(l_10, g_1692)), l_2452)) , l_2462)) == (void*)0), l_2452)) > 0x99FE6CB4L) | (*g_794)) , 0x201F69B4L), (*g_2166)))))) > l_2464), l_2464)))
            { /* block id: 1103 */
                int32_t l_2469 = 0xFDB26C32L;
                int32_t l_2475 = 0xD37F1CBFL;
                int64_t l_2489 = 0x74CFBE1AB0F5CC74LL;
                int32_t *l_2491 = &l_2112;
                int32_t *l_2492 = (void*)0;
                int32_t *l_2493 = &g_23;
                int32_t *l_2494 = &l_2113;
                int32_t *l_2495 = &l_2112;
                int32_t *l_2496 = &l_2114[7][1];
                int32_t *l_2497 = &l_2111;
                int32_t *l_2498 = &g_23;
                int32_t *l_2499 = &l_2475;
                int32_t *l_2500[1][10][7] = {{{&g_70,(void*)0,(void*)0,(void*)0,&l_2476,&l_2476,&l_2113},{&l_2111,&l_2476,&g_105,&l_2475,&l_2476,&l_2114[7][1],&g_70},{&g_105,&l_2475,&g_105,(void*)0,&g_105,&l_2475,&g_105},{&g_105,&l_2476,(void*)0,&g_23,&g_686[0],&l_2475,&g_70},{&l_2111,(void*)0,(void*)0,&g_23,&g_105,&l_2114[7][1],&l_2113},{&g_70,(void*)0,(void*)0,(void*)0,&l_2476,&l_2476,&l_2113},{&l_2111,&l_2476,&g_105,&l_2475,&l_2476,&l_2114[7][1],&g_70},{&g_105,&l_2475,&g_105,(void*)0,&g_105,&l_2475,&g_105},{&g_105,&l_2476,(void*)0,&g_23,&g_686[0],&l_2475,&g_70},{&l_2111,(void*)0,(void*)0,&g_23,&g_105,&l_2114[7][1],&l_2113}}};
                int i, j, k;
                l_2490 = (safe_lshift_func_int8_t_s_u((safe_div_func_int8_t_s_s(l_2469, ((((**g_973) &= 0xBCL) >= l_2453) | (1UL < (l_2453 , (l_2469 , (g_319[6][0][1].f1 &= (safe_sub_func_uint64_t_u_u(((safe_unary_minus_func_uint8_t_u(((safe_div_func_uint16_t_u_u((((((((l_2476 = l_2475) || ((g_586.f1 | ((safe_mod_func_int64_t_s_s(((l_2476 = (safe_lshift_func_uint16_t_u_s((((safe_mul_func_int8_t_s_s((safe_rshift_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((l_2452 < 0x08L), l_2452)) == l_2453), l_2475)), 0x1AL)) ^ l_2218) | 0xB11D606CCDB7B7D4LL), 2))) , l_2464), 7L)) <= 0xF0AB89CCL)) && l_2489)) != (*g_2166)) >= 1L) == l_2469) != l_2453) , 8UL), g_584[3].f1)) , l_2475))) < g_578.f1), 0x461FF5636EB31BADLL))))))))), 7));
                (**l_2462) = (l_2253[0] , (-0x1.Bp+1));
                --l_2504;
            }
            else
            { /* block id: 1111 */
                union U0 * const ***l_2509 = &l_2508;
                l_2501 = (g_2507[5][0][1] , (((*l_2509) = l_2508) != &g_1150));
            }
        }
    }
    return g_560[4].f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_17 g_23 g_41 g_9 g_51 g_42 g_77 g_83 g_70 g_69 g_105 g_93 g_137 g_157 g_127 g_187 g_284 g_318 g_340 g_449 g_463 g_580.f1 g_525.f1 g_319.f0 g_590 g_591 g_686 g_576.f1 g_530.f0 g_1007 g_794 g_566.f1 g_624 g_625 g_626 g_1356 g_588.f1 g_1369 g_550.f0 g_569.f1 g_1181.f0 g_974 g_795 g_1539 g_592 g_567.f1 g_556.f0 g_623 g_973 g_1765 g_591.f0 g_529.f1 g_1692 g_1150 g_1151 g_538 g_1990 g_577.f1 g_1501 g_1589 g_538.f1 g_1098 g_1595 g_1897 g_736 g_1896 g_1349 g_555.f0
 * writes: g_17 g_23 g_9 g_69 g_77 g_83 g_93 g_105 g_137 g_42 g_157 g_70 g_187 g_127 g_277 g_279 g_284 g_399 g_427 g_463 g_794 g_686 g_539.f1 g_537.f1 g_1369 g_565.f1 g_573.f1 g_818 g_462 g_592 g_1730 g_567.f1 g_1537 g_1692 g_573.f0 g_568.f0 g_1999 g_577.f1 g_1098 g_538.f1 g_585.f1 g_736
 */
static const int8_t  func_4(uint64_t  p_5, int8_t  p_6)
{ /* block id: 1 */
    const uint32_t l_20 = 0xDD93BA6AL;
    int32_t *l_24[9] = {&g_23,&g_23,&g_23,&g_23,&g_23,&g_23,&g_23,&g_23,&g_23};
    uint64_t l_1424 = 1UL;
    int16_t **l_1445 = &g_427[4][1][5];
    const union U0 *l_1457[10] = {&g_577,&g_577,&g_577,&g_577,&g_577,&g_577,&g_577,&g_577,&g_577,&g_577};
    uint8_t *l_1471 = (void*)0;
    int32_t * const l_1502 = &g_686[1];
    int64_t ** const *l_1608 = &g_1585[1][7][5];
    uint16_t l_1654 = 0UL;
    int32_t l_1689[6][4] = {{0xDA5F3852L,0xDA5F3852L,0xDA5F3852L,0xDA5F3852L},{0xDA5F3852L,0xDA5F3852L,0xDA5F3852L,0xDA5F3852L},{0xDA5F3852L,0xDA5F3852L,0xDA5F3852L,0xDA5F3852L},{0xDA5F3852L,0xDA5F3852L,0xDA5F3852L,0xDA5F3852L},{0xDA5F3852L,0xDA5F3852L,0xDA5F3852L,0xDA5F3852L},{0xDA5F3852L,0xDA5F3852L,0xDA5F3852L,0xDA5F3852L}};
    int32_t * const l_1691 = &g_1692;
    int32_t * const *l_1690 = &l_1691;
    uint32_t l_1786 = 8UL;
    int32_t *** const l_1829 = (void*)0;
    const uint64_t l_1834[7][9][1] = {{{1UL},{0xE278B997C47C8354LL},{0UL},{0UL},{0xE278B997C47C8354LL},{1UL},{0xD8FB09AE988E5D1DLL},{0x89EF797C4B11AE10LL},{0xC6AC6689BDC2B949LL}},{{0x89EF797C4B11AE10LL},{0xD8FB09AE988E5D1DLL},{1UL},{0xE278B997C47C8354LL},{0UL},{0UL},{0xE278B997C47C8354LL},{1UL},{0xD8FB09AE988E5D1DLL}},{{0x89EF797C4B11AE10LL},{0xC6AC6689BDC2B949LL},{0x89EF797C4B11AE10LL},{0xD8FB09AE988E5D1DLL},{1UL},{0xE278B997C47C8354LL},{5UL},{5UL},{0x89EF797C4B11AE10LL}},{{0xE278B997C47C8354LL},{0xC6AC6689BDC2B949LL},{0x70FAAE51E4817B5CLL},{0UL},{0x70FAAE51E4817B5CLL},{0xC6AC6689BDC2B949LL},{0xE278B997C47C8354LL},{0x89EF797C4B11AE10LL},{5UL}},{{5UL},{0x89EF797C4B11AE10LL},{0xE278B997C47C8354LL},{0xC6AC6689BDC2B949LL},{0x70FAAE51E4817B5CLL},{0UL},{0x70FAAE51E4817B5CLL},{0xC6AC6689BDC2B949LL},{0xE278B997C47C8354LL}},{{0x89EF797C4B11AE10LL},{5UL},{5UL},{0x89EF797C4B11AE10LL},{0xE278B997C47C8354LL},{0xC6AC6689BDC2B949LL},{0x70FAAE51E4817B5CLL},{0UL},{0x70FAAE51E4817B5CLL}},{{0xC6AC6689BDC2B949LL},{0xE278B997C47C8354LL},{0x89EF797C4B11AE10LL},{5UL},{5UL},{0x89EF797C4B11AE10LL},{0xE278B997C47C8354LL},{0xC6AC6689BDC2B949LL},{0x70FAAE51E4817B5CLL}}};
    uint16_t l_1903[1];
    uint8_t l_1920[4][2] = {{0x6AL,0x6AL},{246UL,0x6AL},{0x6AL,246UL},{0x6AL,0x6AL}};
    int32_t l_1950 = 1L;
    const int16_t l_2027 = 0x7896L;
    int16_t l_2039 = 1L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1903[i] = 65535UL;
    for (p_5 = (-24); (p_5 != 22); p_5++)
    { /* block id: 4 */
        int32_t *l_22 = &g_23;
        int32_t **l_21 = &l_22;
        for (g_17 = 1; (g_17 >= 0); g_17 -= 1)
        { /* block id: 7 */
            return l_20;
        }
        (*l_21) = (void*)0;
    }
lbl_1721:
    for (p_5 = 0; (p_5 <= 1); p_5 += 1)
    { /* block id: 14 */
        int32_t ** const l_59 = &g_42;
        uint8_t *l_781 = &g_157[0][7][2];
        int32_t **l_787 = &g_69;
        int8_t l_1410[3][4] = {{3L,0x84L,3L,0x84L},{3L,0x84L,3L,0x84L},{3L,0x84L,3L,0x84L}};
        float *l_1423 = &g_9[0];
        uint8_t l_1495 = 0x85L;
        int8_t l_1497[8][1][6] = {{{0x8CL,0x02L,0x30L,8L,0x30L,0x02L}},{{8L,0x30L,0x02L,0x8CL,5L,0x30L}},{{0x02L,8L,0x52L,(-9L),0L,0L}},{{0L,8L,0xCDL,0x52L,5L,0x6EL}},{{0x7CL,0x30L,0xABL,0xABL,0x30L,0x7CL}},{{0x30L,0x02L,(-9L),0x6EL,0xABL,0x8CL}},{{(-1L),0x52L,0x66L,0xF9L,0x54L,0xCDL}},{{(-1L),0xCDL,0xF9L,0x6EL,8L,0x54L}}};
        int32_t l_1543 = 7L;
        int32_t l_1641 = 0x96DB842CL;
        int32_t l_1642 = 0xC2FE283CL;
        int32_t l_1644 = 0xC3535896L;
        int32_t l_1648[7][5] = {{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL},{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL},{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL},{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL},{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL},{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL},{0x2FD270B3L,0x2FD270B3L,0xE365A13FL,9L,0xE365A13FL}};
        int i, j, k;
        if (g_23)
            break;
        for (g_23 = 0; (g_23 <= 1); g_23 += 1)
        { /* block id: 18 */
            int i;
            g_9[p_5] = 0x0.8p-1;
        }
        g_9[0] = ((l_24[7] == ((*l_59) = ((***g_624) = ((l_24[1] != (((safe_rshift_func_int8_t_s_u(func_27(func_30((((((((func_32(func_35(g_41, (((func_43(g_9[p_5], (safe_mod_func_uint8_t_u_u(g_51, ((*l_781) = func_52(func_56(g_17, l_59), g_127, &l_24[6])))), l_24[8], g_580.f1, g_525.f1) >= p_6) , g_319[6][0][1].f0) , l_787), p_5, l_24[7], p_5), g_576.f1) < 0x2DFCBDE2ABB46FBALL) > 0x5EC6L) && p_6) , g_1356) == 3UL) , p_5) < p_6)), g_588.f1), g_340)) == 0x1E8EL) , (void*)0)) , (void*)0)))) == g_795);
        for (g_573.f1 = (-26); (g_573.f1 == 41); g_573.f1++)
        { /* block id: 651 */
            const uint8_t l_1417[8] = {0x3BL,0x3BL,0x3BL,0x3BL,0x3BL,0x3BL,0x3BL,0x3BL};
            uint32_t *l_1420 = &g_1369;
            int64_t *l_1425 = &g_944;
            int32_t l_1443 = 1L;
            uint8_t l_1444 = 0xE0L;
            uint16_t l_1453 = 0x6308L;
            int32_t l_1541 = 0x1AE5D419L;
            uint32_t **l_1558 = &g_877;
            int32_t l_1563 = 0x9453CC4AL;
            const int16_t *l_1632 = &g_399;
            int32_t l_1640 = 0xC8A7E251L;
            int32_t l_1649[1];
            int32_t **l_1676 = &l_24[7];
            const uint8_t *l_1705[10];
            const uint8_t **l_1704 = &l_1705[8];
            int i;
            for (i = 0; i < 1; i++)
                l_1649[i] = 4L;
            for (i = 0; i < 10; i++)
                l_1705[i] = &g_736[5];
            (*g_1007) ^= p_5;
        }
    }
    for (p_5 = 1; (p_5 <= 6); p_5 += 1)
    { /* block id: 769 */
        int64_t * const **l_1727 = (void*)0;
        int32_t l_1770 = 0xE1A8BAE5L;
        uint32_t l_1781 = 4294967286UL;
        int32_t l_1784 = 0x9209762AL;
        uint8_t l_1787 = 255UL;
        int32_t l_1865 = 0xCADDD0E1L;
        uint16_t l_1883[2][10] = {{0UL,0x19C5L,65532UL,0UL,65532UL,0x19C5L,0UL,0UL,0x19C5L,65532UL},{0x19C5L,0UL,0UL,0x19C5L,65532UL,0UL,65532UL,0x19C5L,0UL,0UL}};
        uint16_t l_1913[8][8][4] = {{{1UL,0x2D38L,0xBC35L,1UL},{1UL,0x0F54L,0x0D69L,0xA739L},{0x3732L,1UL,1UL,0x3732L},{65535UL,0UL,0x7951L,0xABF3L},{0xF926L,0xA739L,0x4D57L,65531UL},{65527UL,0x4D57L,6UL,0xABF3L},{0x0D69L,0xE2E0L,0x9931L,0x2343L},{65535UL,0x52B7L,0xAC54L,65527UL}},{{4UL,0xCCB7L,65535UL,0xE2E0L},{8UL,65535UL,0x2343L,0xCCB7L},{1UL,0x13C6L,1UL,0x1240L},{0xABF3L,0x283CL,65535UL,0x7951L},{6UL,4UL,0x9596L,0x7951L},{65535UL,1UL,0xF926L,0x52B7L},{0x52B7L,2UL,6UL,0xBC35L},{1UL,0xA1D7L,0x8CA8L,0x0D69L}},{{0xA1D7L,1UL,0x0F54L,1UL},{2UL,6UL,0xCCB7L,0x7951L},{0UL,0UL,65535UL,0x4D57L},{0x7951L,0x13C6L,1UL,65530UL},{0x7951L,0x9596L,65535UL,0xE2E0L},{0UL,65530UL,0xCCB7L,0UL},{2UL,0x52B7L,0x0F54L,1UL},{0xA1D7L,65532UL,0x8CA8L,0xABF3L}},{{1UL,0x1240L,6UL,8UL},{0x52B7L,0xE2E0L,0xF926L,1UL},{65535UL,0x0D69L,0x9596L,65527UL},{6UL,65530UL,65535UL,65532UL},{0xABF3L,65535UL,1UL,65530UL},{1UL,65535UL,0x2343L,0x1240L},{8UL,0UL,65535UL,65535UL},{4UL,4UL,0xAC54L,1UL}},{{65535UL,0x7951L,0x9931L,0x52B7L},{0x0D69L,0xA1D7L,6UL,0x9931L},{0x8CF1L,0xA1D7L,0UL,0x52B7L},{0xA1D7L,0x7951L,0UL,1UL},{0UL,4UL,0xCCB7L,65535UL},{65527UL,0UL,65531UL,0x1240L},{0x7951L,65535UL,2UL,65530UL},{65535UL,65535UL,65535UL,65532UL}},{{65527UL,65530UL,5UL,65527UL},{2UL,0x0D69L,0UL,1UL},{2UL,0xE2E0L,0x8CA8L,8UL},{0x8CF1L,0x1240L,65527UL,0xABF3L},{0x52B7L,65532UL,0x9931L,1UL},{0x9596L,0x52B7L,0x9596L,0UL},{4UL,65530UL,0x3732L,0xE2E0L},{0xABF3L,0x9596L,0x2343L,65530UL}},{{0x7951L,0x13C6L,0x2343L,0x4D57L},{0xABF3L,0UL,0x3732L,0x7951L},{4UL,6UL,0x9596L,1UL},{0x9596L,1UL,0x9931L,0x0D69L},{0x52B7L,0xA1D7L,65527UL,2UL},{0x0822L,6UL,0x2D38L,65535UL},{6UL,0xCCB7L,0xAC54L,5UL},{65532UL,1UL,4UL,0UL}},{{0x9596L,0x7564L,0xDADEL,0x8CA8L},{0x0F54L,8UL,6UL,65527UL},{0UL,0UL,0xABF3L,0x9931L},{0x9596L,65527UL,65527UL,0x9596L},{0xD1BAL,65535UL,0xAC54L,0x3732L},{0x7951L,0x9931L,0x52B7L,0x2343L},{0x0822L,0UL,0UL,0x2343L},{65531UL,0x9931L,1UL,0x3732L}}};
        float *l_1921 = &g_277;
        uint64_t l_1935 = 0UL;
        const uint32_t l_1970[1][1] = {{0UL}};
        uint64_t *l_1985[5][8];
        uint8_t ***l_1986 = &g_1896;
        uint16_t *l_1987 = &g_463;
        int32_t * const *l_1993 = &l_24[5];
        int32_t **l_1994 = &l_24[7];
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 8; j++)
                l_1985[i][j] = &g_77;
        }
        for (g_818 = 8; (g_818 >= 0); g_818 -= 1)
        { /* block id: 772 */
            const int64_t *****l_1729 = (void*)0;
            int32_t ** const **l_1760 = (void*)0;
            int32_t l_1762 = (-9L);
            uint8_t l_1763[10][5][1] = {{{0UL},{0x47L},{0x46L},{0xE8L},{0x6DL}},{{0xE8L},{0x46L},{0x47L},{0UL},{0xE8L}},{{0x2BL},{0xE8L},{0UL},{0x47L},{0x46L}},{{0xE8L},{0x6DL},{0xE8L},{0x46L},{0x47L}},{{0UL},{0xE8L},{0x2BL},{0xE8L},{0UL}},{{0x47L},{0x46L},{0xE8L},{0x6DL},{0xE8L}},{{0x46L},{0x47L},{0UL},{0xE8L},{0x2BL}},{{0xE8L},{0UL},{0x47L},{0x46L},{0xE8L}},{{0x6DL},{0xE8L},{0x46L},{0x47L},{0UL}},{{0xE8L},{0x2BL},{0xE8L},{0UL},{0x47L}}};
            int8_t l_1767 = 0xE1L;
            uint32_t *l_1808[1];
            int32_t *l_1875 = &g_686[0];
            int32_t l_1893 = 0x62B9AA79L;
            int8_t l_1949[3];
            int8_t *l_1975[4][2][4] = {{{&g_978,&g_1349[2][2],&g_978,&g_1349[2][2]},{&g_978,&l_1949[2],&l_1949[2],&g_1349[2][2]}},{{&l_1949[2],&g_1349[2][2],&l_1949[2],&l_1949[2]},{&g_978,&g_1349[2][2],&g_978,&g_1349[2][2]}},{{&g_978,&l_1949[2],&l_1949[2],&g_1349[2][2]},{&l_1949[2],&g_1349[2][2],&l_1949[2],&l_1949[2]}},{{&g_978,&g_1349[2][2],&g_978,&g_1349[2][2]},{&g_978,&l_1949[2],&l_1949[2],&g_1349[2][2]}}};
            int16_t l_1976 = 0xEA8AL;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1808[i] = &l_1781;
            for (i = 0; i < 3; i++)
                l_1949[i] = 0x95L;
            for (g_539.f1 = 0; (g_539.f1 <= 8); g_539.f1 += 1)
            { /* block id: 775 */
                int32_t * const l_1723[1] = {&g_23};
                int32_t ** const *l_1759 = &g_1542;
                int32_t ** const **l_1758 = &l_1759;
                int16_t l_1764 = 4L;
                uint32_t *l_1805 = &l_1786;
                const int64_t *l_1816 = &g_462;
                int8_t l_1819 = 6L;
                int16_t *l_1878 = (void*)0;
                int16_t *l_1879[2][4] = {{&g_399,&g_399,(void*)0,&g_399},{&g_399,&l_1764,&l_1764,&g_399}};
                uint16_t l_1880[1][4] = {{7UL,7UL,7UL,7UL}};
                const uint8_t *l_1918 = (void*)0;
                const uint8_t **l_1917 = &l_1918;
                float **l_1938 = (void*)0;
                int i, j;
                for (g_17 = 0; (g_17 <= 8); g_17 += 1)
                { /* block id: 778 */
                    int32_t **l_1722[1];
                    uint8_t **l_1726 = &l_1471;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1722[i] = &l_24[7];
                    for (g_462 = 0; (g_462 <= 2); g_462 += 1)
                    { /* block id: 781 */
                        int i;
                        if (g_137[p_5])
                            break;
                        if (g_566.f1)
                            goto lbl_1721;
                    }
                    for (g_399 = 8; (g_399 >= 0); g_399 -= 1)
                    { /* block id: 787 */
                        int i;
                        return g_137[p_5];
                    }
                    if (g_137[p_5])
                    { /* block id: 790 */
                        int64_t * const ***l_1728 = &l_1727;
                        uint16_t *l_1736 = &g_567.f1;
                        int16_t *l_1747 = &g_1537;
                        int32_t l_1761 = 0x97410740L;
                        int16_t *l_1766 = &g_399;
                        int i;
                        (**g_625) = func_35(&l_1691, l_1722[0], p_6, l_1723[0], (safe_rshift_func_uint8_t_u_u(((*g_1539) ^= ((p_6 >= (l_1726 == l_1726)) == (0L != (((*l_1728) = l_1727) == (void*)0)))), 6)));
                        g_1730 = l_1729;
                        l_1767 ^= (safe_lshift_func_uint8_t_u_s((((((*l_1736)++) , (safe_sub_func_int16_t_s_s((0x5AL < ((safe_mod_func_uint32_t_u_u(p_6, (safe_sub_func_uint8_t_u_u(((((*l_1766) = (safe_div_func_int32_t_s_s((((l_1764 = (((*l_1747) = (g_556.f0 , p_6)) , (safe_mod_func_uint64_t_u_u((safe_sub_func_int32_t_s_s(0x30E8BD4CL, ((safe_div_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((safe_mod_func_uint64_t_u_u(((l_1760 = l_1758) != (*g_623)), (l_1761 = p_5))), 2)), l_1762)) ^ l_1763[4][2][0]))), p_5)))) > l_1763[1][0][0]) && (**g_973)), g_1765))) >= p_5) <= p_5), p_5)))) <= l_1763[4][2][0])), p_6))) < p_6) >= 0xC3L), 6));
                    }
                    else
                    { /* block id: 802 */
                        int32_t l_1785[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_1785[i] = 0x9722C1C6L;
                        (*l_1691) ^= (~(((+p_6) == l_1770) && (safe_div_func_int32_t_s_s(p_6, (safe_rshift_func_uint16_t_u_u((((((*g_794) & (0L >= 1L)) <= (safe_rshift_func_uint16_t_u_u(((((((safe_rshift_func_int16_t_s_u((((safe_sub_func_uint8_t_u_u(l_1781, ((((safe_div_func_uint16_t_u_u(0xA8FFL, g_591.f0)) & l_1784) >= 0x0EDBL) , 1L))) <= p_6) , l_1785[1]), 13)) && p_6) < 0L) || 0xF8L) | p_6) , g_529.f1), 10))) ^ (*g_794)) && l_1786), 5))))));
                        return p_5;
                    }
                }
            }
            (****g_623) = func_35(&l_1875, &l_1875, (((safe_mod_func_int32_t_s_s((safe_rshift_func_int8_t_s_s((p_6 = ((*l_1875) ^= ((**g_973) = p_5))), l_1976)), p_5)) >= (((safe_sub_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((((safe_add_func_uint32_t_u_u((l_1770 >= ((safe_lshift_func_int16_t_s_s(((void*)0 == (*g_1150)), ((&l_1949[1] == l_1471) > p_5))) || (*l_1691))), (-5L))) > p_5) , (*g_794)), p_5)), 9UL)) && (*g_1539)) < p_5)) <= l_1784), &l_1762, l_1787);
            for (p_6 = 0; p_6 < 9; p_6 += 1)
            {
                for (g_573.f0 = 0; g_573.f0 < 8; g_573.f0 += 1)
                {
                    for (g_568.f0 = 0; g_568.f0 < 3; g_568.f0 += 1)
                    {
                        g_157[p_6][g_573.f0][g_568.f0] = 1UL;
                    }
                }
            }
        }
        (**g_625) = (*g_41);
        if (((((0x55D3L && (((*l_1502) = ((**g_1150) , p_6)) >= 0x32E2F806DC2A6577LL)) , ((l_1986 != (void*)0) != ((*l_1987) = g_1181.f0))) >= (l_1865 = ((**l_1690) = ((safe_add_func_uint8_t_u_u((&g_974 == (void*)0), (**l_1690))) <= 9L)))) || 0x1BL))
        { /* block id: 893 */
            (***g_624) = &l_1770;
        }
        else
        { /* block id: 895 */
            volatile int8_t ** volatile ** volatile l_1992 = &g_1991[1];/* VOLATILE GLOBAL l_1992 */
            int16_t *l_2000 = &g_399;
            int32_t l_2003[5];
            int i;
            for (i = 0; i < 5; i++)
                l_2003[i] = 0x1AA1B551L;
            l_1992 = g_1990;
            (*l_1502) = ((((((((*l_1994) = func_35(l_1993, l_1994, (safe_sub_func_int64_t_s_s(((safe_div_func_int32_t_s_s(((*g_1007) ^= ((**l_1690) |= p_5)), 0x11113136L)) , ((p_5 , 0x6CL) > ((g_1999[1][1] = &g_1537) != l_2000))), (safe_add_func_uint8_t_u_u(0x64L, 0L)))), (*l_1993), p_6)) != (**g_625)) || 4UL) < (-8L)) , (**g_624)) != (void*)0) >= 0UL);
            if (l_2003[2])
                continue;
        }
        for (g_577.f1 = 0; (g_577.f1 <= 1); g_577.f1 += 1)
        { /* block id: 906 */
            int64_t l_2018 = 0x0CF4CA303BA7898ALL;
            int8_t * const **l_2031 = &g_973;
            int8_t * const ***l_2030 = &l_2031;
            int32_t l_2040 = 0xCB2D411FL;
            int i, j;
            for (l_1424 = 3; (l_1424 <= 8); l_1424 += 1)
            { /* block id: 909 */
                int64_t ****l_2004[9][10] = {{&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247},{&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247},{&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247},{&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247},{&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247},{&g_1247,&g_1247,(void*)0,&g_1247,(void*)0,&g_1247,&g_1247,(void*)0,&g_1247,(void*)0},{&g_1247,&g_1247,(void*)0,&g_1247,(void*)0,&g_1247,&g_1247,(void*)0,&g_1247,(void*)0},{&g_1247,&g_1247,(void*)0,&g_1247,(void*)0,&g_1247,&g_1247,(void*)0,&g_1247,(void*)0},{&g_1247,&g_1247,(void*)0,&g_1247,(void*)0,&g_1247,&g_1247,(void*)0,&g_1247,(void*)0}};
                int64_t ****l_2006 = &g_1247;
                int64_t *****l_2005 = &l_2006;
                int i, j;
                (*l_1502) = ((((g_1501[g_577.f1] != p_5) < ((*g_1589) = 4L)) >= (g_1098[0][8][0] = (l_2004[2][0] == ((*l_2005) = l_2004[2][0])))) >= 8UL);
            }
            for (g_567.f1 = 0; (g_567.f1 <= 3); g_567.f1 += 1)
            { /* block id: 917 */
                uint32_t l_2010 = 1UL;
                float l_2017 = 0x1.8p+1;
                int32_t l_2019 = 0xAF3D3C38L;
                for (g_538.f1 = 0; (g_538.f1 <= 2); g_538.f1 += 1)
                { /* block id: 920 */
                    uint32_t *l_2023 = &g_1369;
                    int32_t l_2026 = 0x4F2EC5C0L;
                    for (g_585.f1 = 0; (g_585.f1 <= 2); g_585.f1 += 1)
                    { /* block id: 923 */
                        int32_t l_2007 = 0x06AE25A0L;
                        int32_t l_2024 = 3L;
                        uint64_t l_2025[7] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL};
                        int i;
                        (*l_1502) |= ((((g_1501[g_538.f1] >= ((l_2007 = p_5) & ((l_2024 = (safe_div_func_uint64_t_u_u(l_2010, (safe_lshift_func_uint8_t_u_u((g_1501[g_538.f1] == (safe_lshift_func_uint16_t_u_u((((safe_lshift_func_int8_t_s_s(l_2010, (0UL || p_5))) <= (5UL & (--g_1098[1][7][0]))) , (~((((((p_6 || (*g_1595)) , p_5) == 0x5.24A77Dp+39) > 0x1.Cp+1) , &g_1369) != l_2023))), l_2019))), p_6))))) == 0x6DL))) == 0xE3BEEDA7L) < l_2025[2]) && l_2026);
                        if ((*g_1007))
                            continue;
                    }
                }
                return l_2027;
            }
            l_2040 |= (safe_add_func_uint16_t_u_u(((*l_1987) = (l_1883[g_577.f1][(p_5 + 3)] |= ((((*l_2030) = &g_973) == (void*)0) < (g_1501[(g_577.f1 + 1)] | 0L)))), (g_1501[g_577.f1] <= (g_1098[1][7][0] = ((((*g_974) = (safe_lshift_func_uint8_t_u_s(((**g_1896) = (*g_1897)), 1))) != (0xB2L != ((((safe_div_func_float_f_f(g_1501[(g_577.f1 + 1)], ((!(((*l_1921) = (safe_add_func_float_f_f((l_2018 < (**l_1690)), l_2039))) > g_1349[3][2])) < 0x0.69A0E0p-12))) != g_555.f0) == p_6) , 0x33L))) != 0xC0L)))));
        }
    }
    return p_6;
}


/* ------------------------------------------ */
/* 
 * reads : g_93 g_1369 g_70 g_550.f0 g_794 g_77 g_569.f1 g_1181.f0 g_686 g_974 g_137
 * writes: g_93 g_1369 g_70 g_277 g_279 g_9 g_565.f1
 */
static int8_t  func_27(int32_t ** p_28, float  p_29)
{ /* block id: 632 */
    uint32_t *l_1364 = &g_93;
    int32_t l_1367[10] = {0xD5B79A41L,8L,(-5L),8L,0xD5B79A41L,0xD5B79A41L,8L,(-5L),8L,0xD5B79A41L};
    uint32_t *l_1368 = &g_1369;
    int32_t *l_1370 = &g_70;
    float *l_1378 = &g_277;
    uint32_t *l_1387 = &g_312;
    float *l_1389[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t *l_1394 = &g_565.f1;
    uint16_t *l_1396 = &g_545.f1;
    uint16_t **l_1395 = &l_1396;
    int i;
    (*l_1370) &= (safe_mod_func_uint32_t_u_u(((*l_1364)++), ((*l_1368) ^= l_1367[5])));
    (*l_1370) = (safe_add_func_uint32_t_u_u((((safe_div_func_int8_t_s_s((+((*l_1370) <= ((safe_sub_func_float_f_f((g_9[0] = (((*l_1378) = 0x3.964CECp+6) >= (((safe_mul_func_float_f_f((g_279 = (0x5.7DB3D1p+82 > (safe_mul_func_float_f_f((((*l_1370) , ((safe_sub_func_int64_t_s_s((l_1367[5] ^= ((g_550[2].f0 > (safe_sub_func_uint16_t_u_u((((((((l_1387 = &g_312) != &g_127) && ((*l_1370) , ((!1UL) & 0x67793603E0D24A99LL))) > (*l_1370)) ^ (*l_1370)) < (*g_794)) ^ (*l_1370)), g_569.f1))) ^ (*l_1370))), (*g_794))) , p_29)) == p_29), (*l_1370))))), (*l_1370))) == (*l_1370)) >= (*l_1370)))), (*l_1370))) , (*l_1370)))), (*l_1370))) , 0x69L) <= 1L), (*l_1370)));
    l_1367[1] |= (safe_mod_func_int64_t_s_s((safe_mod_func_int8_t_s_s((((g_1181.f0 , &g_1247) != &g_1247) || ((((l_1394 == ((*l_1395) = l_1394)) , 0xDDB103F747276B36LL) > (*l_1370)) | (safe_div_func_uint16_t_u_u(((*l_1394) = (safe_mul_func_int16_t_s_s(((*l_1370) == ((safe_mod_func_uint8_t_u_u(((*l_1370) , (*l_1370)), (-2L))) > (*l_1370))), (*l_1370)))), (*l_1370))))), g_686[0])), (*l_1370)));
    return (*g_974);
}


/* ------------------------------------------ */
/* 
 * reads : g_590 g_591 g_686 g_626 g_625
 * writes: g_23 g_794 g_686 g_69
 */
static int32_t ** func_30(const uint32_t  p_31)
{ /* block id: 626 */
    int32_t *l_1357 = &g_23;
    int16_t *l_1360[6][9] = {{&g_399,&g_399,&g_399,(void*)0,&g_399,&g_399,&g_399,&g_399,(void*)0},{&g_399,&g_399,&g_399,&g_399,(void*)0,&g_399,&g_399,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399,&g_399,(void*)0,&g_399,&g_399,&g_399},{&g_399,&g_399,&g_399,&g_399,&g_399,&g_399,&g_399,(void*)0,&g_399},{&g_399,&g_399,&g_399,(void*)0,(void*)0,(void*)0,&g_399,&g_399,&g_399},{&g_399,(void*)0,&g_399,(void*)0,&g_399,&g_399,&g_399,(void*)0,&g_399}};
    int32_t l_1361[6] = {0L,1L,1L,0L,1L,1L};
    int i, j;
    l_1357 = l_1357;
    (*g_626) = func_35(&l_1357, (((*l_1357) = (-9L)) , &l_1357), (safe_sub_func_int8_t_s_s(0L, (-1L))), l_1357, (l_1361[5] = p_31));
    return (*g_625);
}


/* ------------------------------------------ */
/* 
 * reads : g_530.f0 g_1007 g_686 g_539.f1 g_794 g_77 g_566.f1 g_624 g_625 g_626
 * writes: g_9 g_686 g_539.f1 g_69 g_537.f1
 */
static int64_t  func_32(int32_t * p_33, uint16_t  p_34)
{ /* block id: 454 */
    float l_990 = 0x2.3E57EEp-40;
    int32_t l_1005 = 0xB27312A1L;
    float *l_1006 = &g_9[1];
    int64_t * const l_1056 = (void*)0;
    int64_t * const *l_1055 = &l_1056;
    uint8_t *l_1061 = &g_284;
    uint16_t l_1071 = 0x1B7CL;
    union U0 *l_1102 = &g_554;
    uint8_t l_1155 = 0x44L;
    int8_t l_1201 = 1L;
    int32_t l_1206 = (-10L);
    int32_t l_1207 = (-1L);
    int32_t l_1212 = 0x47194A4DL;
    int32_t l_1215 = (-6L);
    int32_t l_1218 = 1L;
    int32_t l_1220 = 0xB6DF8026L;
    int32_t l_1228 = (-1L);
    int32_t l_1229[8] = {9L,2L,2L,9L,2L,2L,9L,2L};
    uint32_t l_1230[5];
    uint32_t l_1321[3];
    int i;
    for (i = 0; i < 5; i++)
        l_1230[i] = 0x44EEB048L;
    for (i = 0; i < 3; i++)
        l_1321[i] = 0x1A316A08L;
    (*l_1006) = (safe_div_func_float_f_f((!0x0.Fp+1), ((safe_mul_func_float_f_f((safe_sub_func_float_f_f((p_34 == (safe_add_func_float_f_f((p_34 >= l_990), 0xD.386DB7p-51))), (&p_33 != (void*)0))), (safe_sub_func_float_f_f((safe_mul_func_float_f_f(((safe_div_func_float_f_f((safe_sub_func_float_f_f(((safe_div_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f(0xC.269218p-99, l_1005)), 0x9.7p+1)), p_34)) < (-0x1.3p+1)), p_34)), 0xC.F5DAA8p+5)) == 0x6.2A748Dp+52), l_1005)), l_1005)))) >= g_530[7][1][1].f0)));
    (*g_1007) &= l_1005;
    for (g_539.f1 = 0; (g_539.f1 <= 1); g_539.f1 += 1)
    { /* block id: 459 */
        const union U0 **l_1018 = (void*)0;
        int32_t l_1032 = 0x734645ECL;
        const int32_t **l_1033 = (void*)0;
        const int32_t *l_1035 = &g_1036;
        const int32_t **l_1034 = &l_1035;
        const int32_t *l_1037 = &g_1038;
        uint32_t l_1039 = 1UL;
        uint64_t *l_1051[7] = {&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77};
        int32_t l_1091 = 0x3248715FL;
        int32_t l_1092 = 0x62E96699L;
        const float l_1139 = 0x1.B01C90p+35;
        int32_t *****l_1143[3];
        int8_t l_1148[8];
        uint8_t *l_1153 = &g_899[2][1];
        const int32_t l_1176 = 0L;
        int32_t l_1304[10] = {0x484AD7AAL,0L,0L,0x484AD7AAL,0L,0L,0x484AD7AAL,0L,0L,0x484AD7AAL};
        const int64_t *l_1354 = &g_187;
        int i;
        for (i = 0; i < 3; i++)
            l_1143[i] = (void*)0;
        for (i = 0; i < 8; i++)
            l_1148[i] = 0L;
        if ((p_34 != (((safe_sub_func_uint64_t_u_u(((safe_add_func_int16_t_s_s(p_34, ((safe_lshift_func_int8_t_s_u((safe_mod_func_uint8_t_u_u(1UL, ((safe_mul_func_int8_t_s_s((l_1018 == (void*)0), (+(safe_div_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s(((((0xB1E3D086L > (l_1032 = ((safe_div_func_uint16_t_u_u(0x7897L, (safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((p_33 != (l_1037 = ((*l_1034) = (l_1032 , (void*)0)))), p_34)), l_1005)), l_1005)))) < 0x4E694DD8AA6E1BB7LL))) || l_1005) | l_1005) || l_1005), p_34)), l_1039))))) , 255UL))), p_34)) > p_34))) , (*g_794)), p_34)) , g_566.f1) > l_1005)))
        { /* block id: 463 */
            (***g_624) = p_33;
        }
        else
        { /* block id: 465 */
            if (l_1005)
                break;
        }
        for (g_537.f1 = 0; (g_537.f1 <= 1); g_537.f1 += 1)
        { /* block id: 470 */
            uint8_t l_1046 = 0UL;
            int16_t **l_1057 = &g_427[4][0][5];
            int64_t *l_1059 = &g_944;
            int64_t **l_1058 = &l_1059;
            uint32_t *l_1060 = &l_1039;
            int32_t l_1063 = 1L;
            int32_t l_1067[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
            uint32_t l_1081 = 0xA7F0E9DEL;
            union U0 **l_1152 = &g_318;
            uint32_t l_1263[8][1];
            int i, j;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1263[i][j] = 0xFFDE1B83L;
            }
            (*g_626) = &l_1005;
        }
    }
    return p_34;
}


/* ------------------------------------------ */
/* 
 * reads : g_590 g_591 g_686
 * writes: g_794 g_686
 */
static int32_t * func_35(int32_t * const * p_36, int32_t ** p_37, int32_t  p_38, int32_t * const  p_39, int16_t  p_40)
{ /* block id: 337 */
    uint64_t l_788 = 0xA2C68EF3284EDE8DLL;
    int32_t l_791 = 0L;
    float *l_792[7][8][4] = {{{&g_277,&g_9[1],&g_9[0],&g_9[1]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_277,&g_9[0],&g_9[0],&g_279},{(void*)0,&g_9[0],(void*)0,&g_9[1]},{(void*)0,&g_9[1],&g_9[0],&g_279},{&g_277,&g_9[1],&g_9[0],&g_9[1]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_277,&g_9[0],&g_9[0],&g_279}},{{(void*)0,&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279}},{{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279}},{{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]}},{{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279}},{{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]}},{{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279},{(void*)0,&g_9[0],&g_9[0],&g_9[0]},{&g_9[0],&g_9[1],(void*)0,&g_9[0]},{&g_9[0],&g_9[0],&g_9[0],&g_279},{&g_9[0],&g_279,(void*)0,&g_279},{&g_9[0],&g_279,&g_9[0],&g_279}}};
    int32_t l_793 = (-1L);
    int64_t l_796 = (-1L);
    int32_t *l_797 = &g_686[1];
    int32_t l_802 = 1L;
    union U0 **l_805 = &g_318;
    union U0 **l_807 = &g_318;
    union U0 ***l_806 = &l_807;
    uint64_t *l_808 = (void*)0;
    uint64_t *l_809[1][3][2];
    int16_t *l_810 = &g_399;
    int32_t *l_811 = &g_23;
    int32_t *l_834 = &l_791;
    uint8_t l_842 = 0x84L;
    int32_t *l_846[3];
    const uint32_t *l_887 = &g_312;
    int8_t *l_905 = &g_476;
    uint16_t l_945 = 0xD10BL;
    uint16_t *l_972 = &g_543[0].f1;
    int8_t * const **l_975[2][4][7] = {{{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973},{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973},{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973},{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973}},{{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973},{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973},{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973},{(void*)0,(void*)0,&g_973,(void*)0,(void*)0,(void*)0,&g_973}}};
    int8_t * const l_977 = &g_978;
    int8_t * const *l_976 = &l_977;
    int32_t *l_979 = &l_793;
    int32_t *l_980 = (void*)0;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
                l_809[i][j][k] = &l_788;
        }
    }
    for (i = 0; i < 3; i++)
        l_846[i] = &g_70;
    --l_788;
    (*l_797) &= (((((l_791 = l_791) == (l_793 = p_38)) , ((l_793 , l_793) & l_788)) == (1UL >= ((((*g_590) , &l_788) == (g_794 = &g_77)) || l_788))) & l_796);
    return l_980;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int16_t  func_43(float  p_44, uint16_t  p_45, int32_t * p_46, uint64_t  p_47, float  p_48)
{ /* block id: 334 */
    int32_t *l_782[1][7];
    int16_t l_783 = 5L;
    uint64_t l_784 = 18446744073709551606UL;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
            l_782[i][j] = &g_686[0];
    }
    --l_784;
    return p_45;
}


/* ------------------------------------------ */
/* 
 * reads : g_127 g_41 g_42 g_69 g_105 g_23 g_17 g_70 g_137 g_157 g_83 g_284 g_93 g_318 g_340 g_449 g_463 g_187
 * writes: g_127 g_42 g_69 g_70 g_77 g_277 g_279 g_284 g_93 g_105 g_23 g_399 g_427 g_463 g_187
 */
static uint8_t  func_52(int64_t  p_53, int32_t  p_54, int32_t ** p_55)
{ /* block id: 109 */
    int16_t l_257 = (-1L);
    uint32_t *l_262 = &g_93;
    int32_t l_292 = (-9L);
    int32_t *l_294 = &l_292;
    uint32_t *l_311[3];
    union U0 *l_321 = &g_322[3];
    int32_t l_348[8][5] = {{1L,0x4696B21AL,1L,0x4696B21AL,1L},{(-2L),(-2L),(-2L),(-2L),(-2L)},{1L,0x4696B21AL,1L,0x4696B21AL,1L},{(-2L),(-2L),(-2L),(-2L),(-2L)},{1L,0x4696B21AL,1L,0x4696B21AL,1L},{(-2L),(-2L),(-2L),(-2L),(-2L)},{1L,0x4696B21AL,1L,0x4696B21AL,1L},{(-2L),(-2L),(-2L),(-2L),(-2L)}};
    int64_t *l_450 = &g_187;
    uint8_t l_452 = 255UL;
    int16_t *l_508 = &g_399;
    int8_t *l_521 = (void*)0;
    uint8_t l_636 = 0xBBL;
    const uint32_t l_668[10] = {0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L,0xF1F72CB4L};
    const int16_t l_677 = (-1L);
    int i, j;
    for (i = 0; i < 3; i++)
        l_311[i] = &g_312;
    for (g_127 = 0; (g_127 >= 37); g_127 = safe_add_func_uint8_t_u_u(g_127, 2))
    { /* block id: 112 */
        int32_t * const l_251 = (void*)0;
        uint8_t *l_283 = &g_284;
        uint64_t *l_285 = &g_77;
        int32_t *l_291 = &g_23;
        int64_t l_300 = 2L;
        int32_t l_349 = (-1L);
        uint32_t l_350 = 0xE5FE33B1L;
        int16_t *l_425 = (void*)0;
        int16_t **l_426 = (void*)0;
        int64_t l_453 = (-3L);
        int32_t l_461 = (-10L);
        const int16_t l_466 = 0x34FCL;
        for (p_54 = 0; (p_54 < (-16)); p_54 = safe_sub_func_int32_t_s_s(p_54, 6))
        { /* block id: 115 */
            for (p_53 = 4; (p_53 >= 1); p_53 -= 1)
            { /* block id: 118 */
                int32_t **l_250 = &g_42;
                int32_t **l_252 = &g_69;
                (*l_250) = (*g_41);
                (*l_252) = l_251;
            }
            return p_54;
        }
        for (g_70 = 0; (g_70 == 28); g_70 = safe_add_func_uint8_t_u_u(g_70, 4))
        { /* block id: 126 */
            uint32_t l_263 = 8UL;
            uint32_t l_275[6][10][4] = {{{4294967292UL,0UL,8UL,0UL},{3UL,0xD3885CE2L,1UL,4UL},{1UL,0x718F321BL,4294967295UL,8UL},{0x2E4A64B0L,0x2E5EF055L,1UL,0x02F4191AL},{0x2E4A64B0L,4294967295UL,4294967295UL,5UL},{1UL,0x02F4191AL,1UL,3UL},{3UL,8UL,8UL,0xC1276B36L},{4294967292UL,0xFDFF5523L,0UL,0UL},{0x9A5BFF3AL,0x0C7240D8L,0x2E4A64B0L,0x02F4191AL},{1UL,1UL,0x02F4191AL,1UL}},{{4294967289UL,4294967287UL,1UL,4294967295UL},{4294967294UL,0xD3885CE2L,8UL,1UL},{0xA4FAB115L,1UL,4294967294UL,4UL},{4294967286UL,0x4323D4C4L,0x9A5BFF3AL,3UL},{0x2E4A64B0L,1UL,1UL,0x2E4A64B0L},{0xFDFF5523L,4294967295UL,5UL,0xEFFD9D72L},{0UL,0xB5558DF0L,1UL,0xC1276B36L},{0xA4FAB115L,4294967295UL,3UL,0xC1276B36L},{0x76EF8A68L,0xB5558DF0L,4294967286UL,0xEFFD9D72L},{0x9A5BFF3AL,4294967295UL,0x02F4191AL,0x2E4A64B0L}},{{0x39B1B88CL,1UL,0UL,3UL},{4294967295UL,4294967292UL,8UL,0xB5558DF0L},{4294967295UL,0UL,2UL,8UL},{4UL,4294967295UL,5UL,0x2E4A64B0L},{0xB5BBEE72L,0x76EF8A68L,1UL,4UL},{1UL,0x224D5146L,8UL,1UL},{1UL,0xFDFF5523L,0x1A88DF3BL,0x4323D4C4L},{0UL,1UL,0x9A5BFF3AL,0xD3885CE2L},{4UL,0x1B85FDC6L,4UL,4294967286UL},{5UL,1UL,0xDF25EA7EL,0x18BA939DL}},{{3UL,0x2E4A64B0L,0x2E5EF055L,1UL},{0x224D5146L,0x068D3858L,0x2E5EF055L,0x0C7240D8L},{3UL,4294967294UL,0xDF25EA7EL,0xB5558DF0L},{5UL,4294967295UL,4UL,0xDF25EA7EL},{4UL,0xDF25EA7EL,0x9A5BFF3AL,0UL},{0UL,0x76EF8A68L,0x1A88DF3BL,2UL},{1UL,0x068D3858L,8UL,0x2E5EF055L},{1UL,0x02F4191AL,1UL,0x4323D4C4L},{0xB5BBEE72L,0x39B1B88CL,5UL,4294967286UL},{4UL,5UL,2UL,1UL}},{{4294967295UL,1UL,8UL,0UL},{1UL,0xFDFF5523L,0x2E5EF055L,0x2E5EF055L},{3UL,3UL,1UL,0x0C7240D8L},{8UL,4294967292UL,0xB5BBEE72L,0x2E4A64B0L},{5UL,0xDF25EA7EL,2UL,0xB5BBEE72L},{4294967295UL,0xDF25EA7EL,4294967295UL,0x2E4A64B0L},{0xDF25EA7EL,4294967292UL,0x1A88DF3BL,0x0C7240D8L},{1UL,3UL,0x224D5146L,0x2E5EF055L},{5UL,0xFDFF5523L,8UL,0UL},{0xB5BBEE72L,1UL,0x9A5BFF3AL,1UL}},{{4294967295UL,5UL,0x0C7240D8L,4294967286UL},{0x9A5BFF3AL,0x39B1B88CL,8UL,0x4323D4C4L},{3UL,0x02F4191AL,1UL,0x2E5EF055L},{0xC1276B36L,0x068D3858L,5UL,2UL},{8UL,0x76EF8A68L,0xDF25EA7EL,0UL},{4294967295UL,0xDF25EA7EL,0x0C7240D8L,0xDF25EA7EL},{2UL,4294967295UL,4294967295UL,0xB5558DF0L},{0UL,4294967294UL,1UL,0x0C7240D8L},{5UL,0x068D3858L,0xC1276B36L,1UL},{5UL,0x2E4A64B0L,1UL,0x18BA939DL}}};
            int i, j, k;
            for (g_77 = (-20); (g_77 != 40); g_77 = safe_add_func_uint32_t_u_u(g_77, 2))
            { /* block id: 129 */
                uint64_t l_258 = 0xD7966237C08B2C5ELL;
                uint32_t *l_261[4] = {&g_93,&g_93,&g_93,&g_93};
                float *l_276 = &g_277;
                float *l_278 = &g_279;
                int i;
                l_258--;
                l_263 &= (l_261[0] == l_262);
                (*l_278) = ((((*l_276) = ((&g_42 != &g_42) > (safe_sub_func_float_f_f((safe_sub_func_float_f_f(l_263, (safe_sub_func_float_f_f(p_53, ((((!0x5.579BDAp+3) != (0x1.6p+1 <= ((((*g_69) , (((safe_mul_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(p_53, (((l_275[1][0][3] && p_54) , l_257) , l_258))), 0xC07BL)) , g_17) != g_70)) < p_54) >= 0x6.D20A70p+92))) > 0x3.F104CFp-85) <= g_137[3]))))), g_127)))) < (-0x1.9p+1)) >= p_53);
            }
            return g_157[4][6][1];
        }
        if ((l_292 &= ((((((+((*l_285) = (((void*)0 != &l_251) , (safe_rshift_func_uint8_t_u_u(g_157[4][6][1], (((*l_283) = p_53) == 0x57L)))))) != 0x59B879C4L) , (+((safe_sub_func_int32_t_s_s(0x7D9984E9L, (safe_sub_func_uint64_t_u_u(((l_262 == l_291) , g_157[4][6][1]), 0x3BD74A38D7EE5ECCLL)))) <= 0L))) && p_54) , &g_157[6][7][0]) != &g_157[4][6][1])))
        { /* block id: 140 */
            int32_t **l_293 = (void*)0;
            l_294 = (*p_55);
            return g_105;
        }
        else
        { /* block id: 143 */
            int32_t l_317 = (-1L);
            int32_t l_373 = 0xA4C17077L;
            for (g_93 = 0; (g_93 <= 2); g_93 += 1)
            { /* block id: 146 */
                uint32_t *l_310 = (void*)0;
                uint32_t **l_309 = &l_310;
                int32_t l_344[10][1] = {{0x09F10D2EL},{0x228F6ABDL},{0x09F10D2EL},{0x228F6ABDL},{0x09F10D2EL},{0x228F6ABDL},{0x09F10D2EL},{0x228F6ABDL},{0x09F10D2EL},{0x228F6ABDL}};
                int32_t l_345 = 0x5B617F0DL;
                int32_t l_366 = 0x7F9DEC07L;
                uint16_t l_394 = 0x468BL;
                int i, j;
                if ((p_53 > (safe_lshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u((~((*g_69) < ((*l_294) = l_300))), ((safe_mul_func_uint8_t_u_u(((safe_add_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((((((*l_309) = l_291) == (l_311[2] = l_291)) != ((safe_lshift_func_int8_t_s_u(((safe_mod_func_uint64_t_u_u((p_54 == (((g_105 , p_54) , (g_157[4][7][2] < p_54)) & 0x17L)), p_54)) & l_317), g_83)) || p_53)) , 65532UL), g_157[4][6][1])), p_53)) >= l_317), g_127)) < p_54), p_53)) , 255UL))), g_284))))
                { /* block id: 150 */
                    for (l_300 = 0; (l_300 <= 2); l_300 += 1)
                    { /* block id: 153 */
                        (*l_294) |= ((void*)0 == &g_17);
                    }
                }
                else
                { /* block id: 156 */
                    int32_t l_343 = 0x9229954BL;
                    for (g_105 = 0; (g_105 <= 2); g_105 += 1)
                    { /* block id: 159 */
                        union U0 **l_320 = (void*)0;
                        uint8_t l_346 = 0x2FL;
                        int32_t *l_347[10][2] = {{&g_105,&g_105},{&g_70,&g_105},{(void*)0,(void*)0},{(void*)0,&g_105},{&g_70,&g_105},{&g_105,&g_105},{&g_70,&g_105},{(void*)0,(void*)0},{(void*)0,&g_105},{&g_70,&g_105}};
                        int i, j, k;
                        if (g_157[(g_93 + 6)][(g_93 + 4)][g_93])
                            break;
                        l_321 = g_318;
                        g_70 |= (l_346 = (l_345 &= ((*l_291) = (p_53 , (+(safe_mod_func_int8_t_s_s(((~((safe_mod_func_uint8_t_u_u((p_53 ^ ((*l_294) && ((g_137[5] , (*l_294)) , ((safe_add_func_int8_t_s_s((safe_lshift_func_int16_t_s_s(((safe_sub_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_s(((safe_unary_minus_func_uint16_t_u(p_53)) <= g_340), 3)), 7)) , ((*l_294) = (((safe_add_func_uint8_t_u_u(((void*)0 != &g_17), l_343)) , 0x76L) >= l_317))), 0xE963DF35L)) & g_157[(g_93 + 6)][(g_93 + 4)][g_93]), 11)), p_54)) == p_53)))), 0xA9L)) <= 0xB1L)) <= p_53), l_344[0][0])))))));
                        l_350++;
                    }
                    for (g_284 = 0; (g_284 <= 2); g_284 += 1)
                    { /* block id: 171 */
                        uint64_t l_372 = 18446744073709551610UL;
                        int i, j;
                        g_69 = ((~((void*)0 == g_318)) , &l_348[(g_93 + 3)][g_284]);
                        l_349 ^= (((g_137[(g_284 + 2)] < (safe_div_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u((safe_mod_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((((g_17 | l_317) <= ((l_366 , (safe_mod_func_int32_t_s_s(((l_317 , ((safe_mod_func_int8_t_s_s(((0x43L ^ ((+(((p_54 , (l_373 = ((&l_321 == &g_318) == l_372))) & 0x8844D54A62370B22LL) , p_53)) < (*l_294))) ^ (-1L)), g_23)) <= g_23)) ^ (*g_69)), p_53))) & 1UL)) ^ 1L), 0x2C3C9639DA54A31DLL)), p_53)), 13)) && 0x7D01A9C0D5D7587FLL), 0L)), 0x442C3B54L)) <= l_345), l_366))) , (void*)0) == &g_312);
                    }
                }
                for (g_23 = 2; (g_23 >= 0); g_23 -= 1)
                { /* block id: 179 */
                    int16_t *l_382 = &l_257;
                    int16_t *l_398 = &g_399;
                    int32_t l_400[7][6] = {{4L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L}};
                    int i, j, k;
                }
            }
            for (g_399 = 7; (g_399 >= (-17)); g_399 = safe_sub_func_int32_t_s_s(g_399, 1))
            { /* block id: 199 */
                return g_340;
            }
        }
        if ((&g_399 == (g_427[4][0][5] = l_425)))
        { /* block id: 204 */
            int32_t **l_440 = &l_291;
            int32_t ***l_439 = &l_440;
            int32_t ****l_438 = &l_439;
            int64_t **l_451 = &l_450;
            int32_t *l_454 = &l_292;
            int32_t *l_455 = &l_349;
            int32_t *l_456 = &l_292;
            int32_t *l_457 = &l_348[2][4];
            int32_t *l_458 = &l_349;
            int32_t *l_459 = &l_349;
            int32_t *l_460[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            l_349 |= ((safe_sub_func_int32_t_s_s((((((((*l_451) = ((safe_rshift_func_uint16_t_u_u(((((safe_rshift_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(((((p_53 , &g_318) == (void*)0) , (p_53 <= (-2L))) <= p_53), (safe_rshift_func_int16_t_s_s((((*l_438) = &p_55) == ((safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s((((((((safe_mod_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u(((void*)0 != g_449), 2)), 1L)) > (*l_291)) , 1L) && (**l_440)) , &l_300) == &g_187) < p_53), p_54)), 0x4908L)) , (void*)0)), p_54)))), 7)) || g_127) , l_450) != (void*)0), g_105)) , &p_53)) != (void*)0) < (*l_294)) && 0x37361870L) == p_53) == l_452), l_453)) != 0L);
            g_463--;
        }
        else
        { /* block id: 209 */
            if (l_466)
                break;
        }
    }
    for (g_187 = (-8); (g_187 <= 0); g_187 = safe_add_func_int16_t_s_s(g_187, 8))
    { /* block id: 215 */
        int8_t *l_475[5][7] = {{(void*)0,&g_476,(void*)0,&g_137[3],(void*)0,&g_137[3],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_476,&g_137[3],(void*)0},{&g_137[5],&g_137[3],(void*)0,&g_476,&g_476,(void*)0,&g_137[3]},{&g_476,&g_137[5],&g_137[3],&g_476,(void*)0,(void*)0,(void*)0},{&g_137[3],&g_137[5],&g_476,&g_137[5],&g_137[3],&g_476,(void*)0}};
        const int32_t l_485 = 0x9F95C6FDL;
        int32_t l_504 = 0x3F3C6F0DL;
        int16_t **l_602 = (void*)0;
        int32_t l_689[7];
        int32_t *** const *l_723 = &g_625;
        uint32_t l_778 = 0UL;
        uint16_t l_779 = 0x57ECL;
        int i, j;
        for (i = 0; i < 7; i++)
            l_689[i] = 0L;
        for (l_292 = 0; (l_292 <= 4); l_292 += 1)
        { /* block id: 218 */
            int i, j;
            return l_348[l_292][l_292];
        }
    }
    return g_187;
}


/* ------------------------------------------ */
/* 
 * reads : g_23 g_41 g_42 g_77 g_17 g_83 g_70 g_69 g_105 g_93 g_137 g_157 g_127 g_187 g_9
 * writes: g_69 g_77 g_83 g_93 g_105 g_137 g_42 g_157 g_23 g_70 g_187
 */
static int64_t  func_56(uint32_t  p_57, int32_t ** const  p_58)
{ /* block id: 21 */
    float l_60 = 0xA.DF6F6Dp+57;
    int32_t l_71[7][6] = {{0x3D4D864BL,0xC0642926L,0xA52E963EL,0xA52E963EL,0xC0642926L,0x3D4D864BL},{(-1L),0x3D4D864BL,8L,0xC0642926L,8L,0x3D4D864BL},{8L,(-1L),0xA52E963EL,0xF814D196L,0xF814D196L,0xA52E963EL},{8L,8L,0xF814D196L,0xC0642926L,0xEDB9CC51L,0xC0642926L},{(-1L),8L,(-1L),0xA52E963EL,0xF814D196L,0xF814D196L},{0x3D4D864BL,(-1L),(-1L),0x3D4D864BL,8L,0xC0642926L},{0xC0642926L,0x3D4D864BL,0xF814D196L,0x3D4D864BL,0xC0642926L,0xA52E963EL}};
    uint8_t l_74 = 0x51L;
    uint64_t *l_75 = (void*)0;
    uint64_t *l_76 = &g_77;
    int64_t l_101 = 0x36B4ADBCF703B84DLL;
    float l_130 = 0xD.F0E7D1p+97;
    int64_t *l_207 = &g_187;
    int i, j;
lbl_208:
    if ((g_23 , func_61((safe_mul_func_float_f_f((safe_mul_func_float_f_f(g_23, (safe_add_func_float_f_f(((((l_71[6][5] = ((*g_41) != (g_69 = &g_23))) <= ((((safe_add_func_uint32_t_u_u((((void*)0 == p_58) <= (((*l_76) = l_74) ^ (safe_lshift_func_int16_t_s_u(0x9B75L, ((void*)0 == (*g_41)))))), l_74)) <= 4UL) , g_77) > g_23)) >= p_57) > g_17), 0x2.6B3FA4p-76)))), 0xE.E091E6p-90)))))
    { /* block id: 28 */
        uint32_t *l_92 = &g_93;
        int32_t l_102 = 1L;
        int64_t *l_103[2][10] = {{&l_101,(void*)0,(void*)0,&l_101,&l_101,&l_101,(void*)0,(void*)0,&l_101,&l_101},{&l_101,(void*)0,(void*)0,&l_101,&l_101,&l_101,(void*)0,(void*)0,&l_101,&l_101}};
        int32_t l_104 = (-1L);
        int32_t * const l_108 = (void*)0;
        int32_t * const *l_107 = &l_108;
        int32_t * const **l_106 = &l_107;
        int32_t * const *l_110 = &g_42;
        int32_t * const **l_109 = &l_110;
        int32_t *l_111[2];
        uint32_t l_129 = 5UL;
        int i, j;
        for (i = 0; i < 2; i++)
            l_111[i] = &l_71[6][0];
lbl_132:
        g_105 ^= (safe_sub_func_uint16_t_u_u(l_71[0][2], ((l_104 = (safe_div_func_uint64_t_u_u(((((*l_92) = p_57) <= (l_102 = (g_17 >= (0xA1L == (((*l_76) = (l_71[6][5] | ((!(g_70 <= (safe_mod_func_int64_t_s_s((safe_sub_func_int16_t_s_s(((((*g_69) < p_57) == ((g_70 && 0x2076L) >= l_101)) || g_70), 6L)), 0x737198010E05D1EELL)))) && l_71[6][3]))) | 0UL))))) >= 0x87A386FCL), p_57))) | 0L)));
        (*l_109) = ((*l_106) = &g_69);
lbl_138:
        l_111[1] = (*p_58);
        if ((l_101 && ((safe_div_func_uint64_t_u_u(p_57, (g_70 , ((*l_76) = ((((g_17 == (safe_unary_minus_func_int16_t_s(0x1657L))) , (((g_70 && (0x3AL ^ (g_83 < (safe_lshift_func_uint16_t_u_s(((p_57 ^ 0xE182L) < l_71[6][5]), 8))))) != p_57) <= (***l_109))) , l_74) || 0x9521L))))) <= g_93)))
        { /* block id: 38 */
            uint32_t *l_126[3];
            const int32_t l_128 = 1L;
            int32_t l_131 = (-8L);
            int i;
            for (i = 0; i < 3; i++)
                l_126[i] = &g_127;
            if ((safe_div_func_int32_t_s_s((((g_23 && ((+(((safe_sub_func_uint16_t_u_u(((l_131 = ((((l_74 , (*g_41)) == (void*)0) < 1L) == ((g_23 || (l_101 , (safe_sub_func_uint32_t_u_u((((((safe_lshift_func_uint16_t_u_u((((p_57 = ((l_71[3][4] ^ (p_57 , 1L)) , p_57)) , (**l_107)) & l_128), 15)) & l_101) <= l_129) , p_57) , g_83), l_71[6][5])))) >= 255UL))) <= g_83), (-7L))) && (-3L)) != g_17)) , l_71[6][5])) < (-2L)) & g_93), l_71[6][3])))
            { /* block id: 41 */
                return g_83;
            }
            else
            { /* block id: 43 */
                if (g_105)
                    goto lbl_132;
                for (g_83 = (-29); (g_83 != 25); g_83 = safe_add_func_uint32_t_u_u(g_83, 9))
                { /* block id: 47 */
                    if (l_128)
                        break;
                }
            }
lbl_139:
            g_137[3] |= ((g_23 < g_70) | (l_71[6][5] |= ((safe_lshift_func_int8_t_s_s(g_105, 0)) < (l_128 ^ (-1L)))));
            if (g_83)
                goto lbl_138;
            if (g_105)
                goto lbl_139;
        }
        else
        { /* block id: 55 */
            int16_t l_155 = 0xBCA0L;
            int32_t l_199 = 0xBD07CE89L;
            for (l_102 = 13; (l_102 == 6); l_102--)
            { /* block id: 58 */
                int32_t **l_146 = &l_111[1];
                for (p_57 = 24; (p_57 >= 54); ++p_57)
                { /* block id: 61 */
                    uint16_t l_154 = 7UL;
                    (*p_58) = (*p_58);
                    for (g_77 = 0; (g_77 > 37); g_77 = safe_add_func_uint32_t_u_u(g_77, 1))
                    { /* block id: 65 */
                        int32_t ***l_147 = &l_146;
                        uint8_t *l_156 = &g_157[4][6][1];
                        l_71[4][2] ^= ((((((*l_147) = l_146) != &g_69) , (((***l_109) = ((safe_rshift_func_uint16_t_u_s((g_17 | ((*l_156) |= (safe_mod_func_int16_t_s_s(g_77, ((((((p_57 , (p_57 >= ((4UL == ((((g_17 | ((((((((safe_mod_func_uint64_t_u_u(g_105, g_105)) , l_154) , g_137[3]) , 0x4.782D3Bp+85) , l_155) , g_137[3]) | (*g_69)) <= l_74)) && l_155) , p_57) , 0x37D7A1B735CC4819LL)) && (-5L)))) | g_105) , &l_107) == &l_110) > g_77) , l_155))))), 2)) & 0x6635L)) == 255UL)) >= p_57) != p_57);
                    }
                }
                for (l_74 = (-30); (l_74 <= 55); l_74 = safe_add_func_uint64_t_u_u(l_74, 3))
                { /* block id: 74 */
                    uint8_t *l_174 = (void*)0;
                    uint8_t *l_175[10][8][3] = {{{&g_157[1][1][0],&l_74,&g_157[4][6][1]},{&g_157[4][6][1],(void*)0,&g_157[6][3][1]},{&g_157[1][1][0],(void*)0,&l_74},{&g_157[1][1][1],&l_74,&l_74},{&g_157[7][6][2],&g_157[4][6][1],&g_157[4][6][1]},{(void*)0,&g_157[4][6][1],(void*)0},{&g_157[4][6][2],&g_157[1][1][2],&g_157[2][6][1]},{(void*)0,&l_74,&g_157[4][6][1]}},{{&g_157[7][4][1],&g_157[1][1][2],&g_157[4][6][1]},{&g_157[4][6][1],&g_157[4][6][1],&g_157[1][1][1]},{&g_157[4][6][1],&g_157[4][6][1],&g_157[1][1][2]},{&g_157[4][6][1],&l_74,(void*)0},{&l_74,(void*)0,&g_157[4][4][2]},{(void*)0,(void*)0,&g_157[4][6][1]},{&g_157[4][6][1],&l_74,&g_157[4][4][2]},{&g_157[6][3][1],&l_74,&g_157[1][1][1]}},{{&g_157[7][6][2],&g_157[4][6][1],&g_157[7][6][2]},{(void*)0,(void*)0,(void*)0},{&g_157[1][1][2],(void*)0,&g_157[4][6][2]},{(void*)0,&g_157[4][6][1],(void*)0},{&g_157[4][6][1],&g_157[4][6][2],&g_157[7][4][1]},{(void*)0,(void*)0,&g_157[4][6][1]},{&g_157[1][1][2],&g_157[4][6][1],&g_157[4][6][1]},{(void*)0,(void*)0,&g_157[4][6][1]}},{{&g_157[7][6][2],&g_157[4][6][1],&l_74},{&g_157[6][3][1],&l_74,(void*)0},{&l_74,&g_157[4][4][2],&g_157[4][6][1]},{&g_157[1][1][1],&l_74,&g_157[0][1][1]},{&g_157[4][6][1],&g_157[4][6][1],&g_157[1][1][2]},{(void*)0,(void*)0,&l_74},{&g_157[4][6][1],&g_157[4][6][1],&l_74},{&g_157[4][6][1],(void*)0,&l_74}},{{&l_74,&g_157[4][6][2],&g_157[4][6][1]},{&l_74,&g_157[4][6][1],&l_74},{&g_157[2][6][1],(void*)0,&l_74},{&l_74,(void*)0,&l_74},{(void*)0,&g_157[4][6][1],&g_157[1][1][2]},{(void*)0,&l_74,&g_157[0][1][1]},{&g_157[4][6][1],&g_157[4][6][1],&g_157[4][6][1]},{&l_74,&g_157[0][1][1],(void*)0}},{{&g_157[4][6][1],&l_74,&l_74},{(void*)0,&g_157[4][6][1],&g_157[4][6][1]},{(void*)0,&g_157[4][6][1],&g_157[4][6][1]},{&l_74,&l_74,&g_157[4][6][1]},{&g_157[2][6][1],&g_157[7][6][2],&g_157[7][4][1]},{&l_74,(void*)0,(void*)0},{&l_74,&g_157[7][6][2],&g_157[4][6][2]},{&g_157[4][6][1],&l_74,(void*)0}},{{&g_157[4][6][1],&g_157[4][6][1],&g_157[7][6][2]},{(void*)0,&g_157[4][6][1],&g_157[1][1][1]},{&g_157[4][6][1],&l_74,&g_157[1][1][0]},{&g_157[1][1][1],&g_157[0][1][1],&g_157[4][6][1]},{&l_74,&g_157[4][6][1],&g_157[1][1][0]},{&g_157[6][3][1],&l_74,&g_157[1][1][1]},{&g_157[7][6][2],&g_157[4][6][1],&g_157[7][6][2]},{(void*)0,(void*)0,(void*)0}},{{&g_157[1][1][2],(void*)0,&g_157[4][6][2]},{(void*)0,&g_157[4][6][1],(void*)0},{&g_157[4][6][1],&g_157[4][6][2],&g_157[7][4][1]},{(void*)0,(void*)0,&g_157[4][6][1]},{&g_157[1][1][2],&g_157[4][6][1],&g_157[4][6][1]},{(void*)0,(void*)0,&g_157[4][6][1]},{&g_157[7][6][2],&g_157[4][6][1],&l_74},{&g_157[6][3][1],&l_74,(void*)0}},{{&l_74,&g_157[4][4][2],&g_157[4][6][1]},{&g_157[1][1][1],&l_74,&g_157[0][1][1]},{&g_157[4][6][1],&g_157[4][6][1],&g_157[1][1][2]},{(void*)0,(void*)0,&l_74},{&g_157[4][6][1],&g_157[4][6][1],&l_74},{&g_157[4][6][1],(void*)0,&l_74},{&l_74,&g_157[4][6][2],&g_157[4][6][1]},{&l_74,&g_157[4][6][1],&l_74}},{{&g_157[2][6][1],(void*)0,&l_74},{&l_74,(void*)0,&l_74},{(void*)0,&g_157[4][6][1],&g_157[1][1][2]},{(void*)0,&l_74,&g_157[0][1][1]},{&g_157[4][6][1],&g_157[4][6][1],&g_157[4][6][1]},{&l_74,&g_157[0][1][1],(void*)0},{&g_157[4][6][1],&l_74,&g_157[4][6][1]},{&l_74,(void*)0,(void*)0}}};
                    int32_t l_176 = 0xF1CC1EF9L;
                    int32_t l_188 = 0x0E703EE8L;
                    int32_t l_189 = 0x29CE55DAL;
                    uint32_t l_200[1][5] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}};
                    int i, j, k;
                    if ((((l_189 &= ((safe_mul_func_uint8_t_u_u((g_23 ^ g_127), g_83)) ^ ((safe_sub_func_uint64_t_u_u(((safe_mul_func_uint8_t_u_u((safe_mod_func_int16_t_s_s((safe_sub_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u(p_57, ((safe_lshift_func_uint8_t_u_s((l_176 = l_155), (safe_rshift_func_uint16_t_u_s(((+(safe_add_func_int32_t_s_s(3L, (g_70 = ((0UL == 0xE4CAB560D96E28CDLL) , (safe_div_func_int32_t_s_s(((~(((safe_mod_func_int32_t_s_s(((g_187 ^ 4294967295UL) , 0xBAE54040L), (*g_69))) , (**l_107)) == g_137[3])) > 0x534F4898C3569B45LL), l_188))))))) , l_188), g_83)))) , g_17))), (*g_69))), g_17)), g_77)) , 0x35E1361682E68182LL), p_57)) , (**l_110)))) && 0UL) ^ g_137[4]))
                    { /* block id: 78 */
                        uint32_t l_196 = 0xEFB666E4L;
                        (*g_69) = ((safe_add_func_uint8_t_u_u(g_137[3], (1L ^ (safe_rshift_func_int16_t_s_u((((*p_58) != ((*l_146) = &g_23)) , g_23), 5))))) && ((((safe_mod_func_int32_t_s_s(l_196, ((l_196 , (l_199 = (safe_lshift_func_int8_t_s_s(4L, 7)))) && p_57))) , 0UL) > l_71[4][0]) , 0x23A6D7AE1D0CBB2BLL));
                        ++l_200[0][1];
                        return l_196;
                    }
                    else
                    { /* block id: 84 */
                        return p_57;
                    }
                }
            }
        }
    }
    else
    { /* block id: 90 */
        for (g_77 = 0; (g_77 == 23); ++g_77)
        { /* block id: 93 */
            int32_t **l_205 = &g_69;
            (*l_205) = ((*p_58) = &g_105);
        }
    }
    if (((*g_69) = (safe_unary_minus_func_int64_t_s(((*l_207) ^= p_57)))))
    { /* block id: 100 */
        int64_t *l_218 = &g_187;
        int32_t l_219 = 0x6F6A9FABL;
        uint32_t *l_236[1][10][3] = {{{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93},{&g_93,&g_93,&g_93}}};
        int32_t l_237[5][3][7] = {{{0x26CC6B5DL,0xA8C1E0F0L,0x535DD0E7L,1L,(-9L),6L,0xF9DA3947L},{1L,0x1FEC4C8CL,0x0658B699L,0x69E38AFDL,0xB023FBB9L,0x6ABD6A1CL,0xD9387172L},{0x535DD0E7L,(-1L),(-2L),0xA8C1E0F0L,(-2L),(-1L),0x535DD0E7L}},{{0xD9387172L,0x6ABD6A1CL,0xB023FBB9L,0x69E38AFDL,0x0658B699L,0x1FEC4C8CL,1L},{0xF9DA3947L,6L,(-9L),1L,0x535DD0E7L,0xA8C1E0F0L,0x26CC6B5DL},{0L,0L,0xB023FBB9L,0xD9387172L,1L,0x0658B699L,0x0658B699L}},{{0xB3587C03L,1L,(-2L),1L,0xB3587C03L,0L,0x2A048263L},{(-5L),0L,0x0658B699L,0x6ABD6A1CL,0x69E38AFDL,0L,0x69E38AFDL},{0xF3828B4FL,6L,0x535DD0E7L,0xCBBB26B0L,(-4L),0L,0L}},{{(-5L),0x6ABD6A1CL,1L,0xFA5EDEFCL,1L,1L,0xFA5EDEFCL},{0xB3587C03L,(-1L),0xB3587C03L,0L,0xF9DA3947L,1L,0L},{0L,0x1FEC4C8CL,0x69E38AFDL,1L,0L,1L,0x69E38AFDL}},{{0xF9DA3947L,0xA8C1E0F0L,(-4L),0x94F7204BL,0x1FC27931L,1L,0x2A048263L},{0xD9387172L,0x0658B699L,1L,0L,0L,1L,0x0658B699L},{0x535DD0E7L,0L,0xF9DA3947L,0xB8E079F3L,0x1FC27931L,0L,0x26CC6B5DL}}};
        uint8_t *l_238 = (void*)0;
        uint8_t *l_239 = (void*)0;
        uint8_t *l_240 = (void*)0;
        uint8_t *l_241[3][6] = {{&l_74,&l_74,&l_74,&l_74,&l_74,&l_74},{&l_74,&l_74,&l_74,&l_74,&l_74,&l_74},{&l_74,&l_74,&l_74,&l_74,&l_74,&l_74}};
        float *l_242 = &l_60;
        int i, j, k;
        if (g_127)
            goto lbl_208;
        (*l_242) = (safe_add_func_float_f_f(((safe_rshift_func_int16_t_s_s(p_57, 2)) , (safe_div_func_float_f_f((((!(safe_mul_func_float_f_f((&l_101 == l_218), 0x4.BB8DE7p+77))) < l_219) > (p_57 != (safe_div_func_float_f_f((safe_div_func_float_f_f(((safe_mul_func_float_f_f(((((safe_mod_func_uint8_t_u_u(((g_157[0][5][1] = (((((((safe_add_func_uint32_t_u_u((l_237[2][0][6] |= ((safe_mul_func_int16_t_s_s(g_93, (safe_mod_func_int8_t_s_s((safe_lshift_func_int8_t_s_u(((void*)0 != l_236[0][4][2]), l_71[6][0])), p_57)))) , 0UL)), l_219)) == l_219) <= g_187) , (-7L)) != g_157[8][1][1]) , 0x1661L) > 8UL)) <= 0xF9L), g_93)) | l_219) , p_57) != 0x1.Cp-1), l_219)) <= p_57), (-0x1.5p+1))), g_9[0])))), 0x1.7p-1))), p_57));
    }
    else
    { /* block id: 105 */
        uint64_t l_243 = 1UL;
        ++l_243;
    }
    return p_57;
}


/* ------------------------------------------ */
/* 
 * reads : g_83 g_70
 * writes: g_83
 */
static int32_t  func_61(float  p_62)
{ /* block id: 25 */
    int32_t *l_80 = &g_70;
    int32_t *l_81 = &g_70;
    int32_t *l_82[2][9] = {{&g_70,&g_23,&g_70,&g_70,&g_70,&g_70,&g_23,&g_70,&g_23},{&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70}};
    int i, j;
    ++g_83;
    return (*l_80);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_8, "g_8", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_9[i], sizeof(g_9[i]), "g_9[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_17, "g_17", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    transparent_crc(g_51, "g_51", print_hash_value);
    transparent_crc(g_70, "g_70", print_hash_value);
    transparent_crc(g_77, "g_77", print_hash_value);
    transparent_crc(g_83, "g_83", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_137[i], "g_137[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_157[i][j][k], "g_157[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_187, "g_187", print_hash_value);
    transparent_crc_bytes (&g_277, sizeof(g_277), "g_277", print_hash_value);
    transparent_crc_bytes (&g_279, sizeof(g_279), "g_279", print_hash_value);
    transparent_crc(g_284, "g_284", print_hash_value);
    transparent_crc(g_312, "g_312", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_319[i][j][k].f1, "g_319[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_322[i].f0, "g_322[i].f0", print_hash_value);
        transparent_crc(g_322[i].f1, "g_322[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_340, "g_340", print_hash_value);
    transparent_crc(g_397, "g_397", print_hash_value);
    transparent_crc(g_399, "g_399", print_hash_value);
    transparent_crc(g_462, "g_462", print_hash_value);
    transparent_crc(g_463, "g_463", print_hash_value);
    transparent_crc(g_476, "g_476", print_hash_value);
    transparent_crc(g_525.f0, "g_525.f0", print_hash_value);
    transparent_crc(g_525.f1, "g_525.f1", print_hash_value);
    transparent_crc(g_526.f0, "g_526.f0", print_hash_value);
    transparent_crc(g_526.f1, "g_526.f1", print_hash_value);
    transparent_crc(g_527.f0, "g_527.f0", print_hash_value);
    transparent_crc(g_527.f1, "g_527.f1", print_hash_value);
    transparent_crc(g_528.f0, "g_528.f0", print_hash_value);
    transparent_crc(g_528.f1, "g_528.f1", print_hash_value);
    transparent_crc(g_529.f0, "g_529.f0", print_hash_value);
    transparent_crc(g_529.f1, "g_529.f1", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_530[i][j][k].f1, "g_530[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_531[i][j][k].f0, "g_531[i][j][k].f0", print_hash_value);
                transparent_crc(g_531[i][j][k].f1, "g_531[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_532.f0, "g_532.f0", print_hash_value);
    transparent_crc(g_532.f1, "g_532.f1", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_533[i][j].f0, "g_533[i][j].f0", print_hash_value);
            transparent_crc(g_533[i][j].f1, "g_533[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_534[i][j].f0, "g_534[i][j].f0", print_hash_value);
            transparent_crc(g_534[i][j].f1, "g_534[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_535[i][j][k].f0, "g_535[i][j][k].f0", print_hash_value);
                transparent_crc(g_535[i][j][k].f1, "g_535[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_536.f0, "g_536.f0", print_hash_value);
    transparent_crc(g_536.f1, "g_536.f1", print_hash_value);
    transparent_crc(g_537.f1, "g_537.f1", print_hash_value);
    transparent_crc(g_538.f1, "g_538.f1", print_hash_value);
    transparent_crc(g_539.f1, "g_539.f1", print_hash_value);
    transparent_crc(g_540.f0, "g_540.f0", print_hash_value);
    transparent_crc(g_540.f1, "g_540.f1", print_hash_value);
    transparent_crc(g_541.f0, "g_541.f0", print_hash_value);
    transparent_crc(g_541.f1, "g_541.f1", print_hash_value);
    transparent_crc(g_542.f0, "g_542.f0", print_hash_value);
    transparent_crc(g_542.f1, "g_542.f1", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_543[i].f0, "g_543[i].f0", print_hash_value);
        transparent_crc(g_543[i].f1, "g_543[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_544.f0, "g_544.f0", print_hash_value);
    transparent_crc(g_544.f1, "g_544.f1", print_hash_value);
    transparent_crc(g_545.f0, "g_545.f0", print_hash_value);
    transparent_crc(g_545.f1, "g_545.f1", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_546[i][j].f0, "g_546[i][j].f0", print_hash_value);
            transparent_crc(g_546[i][j].f1, "g_546[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_547.f0, "g_547.f0", print_hash_value);
    transparent_crc(g_547.f1, "g_547.f1", print_hash_value);
    transparent_crc(g_548.f0, "g_548.f0", print_hash_value);
    transparent_crc(g_548.f1, "g_548.f1", print_hash_value);
    transparent_crc(g_549.f0, "g_549.f0", print_hash_value);
    transparent_crc(g_549.f1, "g_549.f1", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_550[i].f0, "g_550[i].f0", print_hash_value);
        transparent_crc(g_550[i].f1, "g_550[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_551[i].f0, "g_551[i].f0", print_hash_value);
        transparent_crc(g_551[i].f1, "g_551[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_552.f0, "g_552.f0", print_hash_value);
    transparent_crc(g_552.f1, "g_552.f1", print_hash_value);
    transparent_crc(g_553.f0, "g_553.f0", print_hash_value);
    transparent_crc(g_553.f1, "g_553.f1", print_hash_value);
    transparent_crc(g_554.f0, "g_554.f0", print_hash_value);
    transparent_crc(g_554.f1, "g_554.f1", print_hash_value);
    transparent_crc(g_555.f1, "g_555.f1", print_hash_value);
    transparent_crc(g_556.f1, "g_556.f1", print_hash_value);
    transparent_crc(g_557.f1, "g_557.f1", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_558[i].f0, "g_558[i].f0", print_hash_value);
        transparent_crc(g_558[i].f1, "g_558[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_559.f0, "g_559.f0", print_hash_value);
    transparent_crc(g_559.f1, "g_559.f1", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_560[i].f0, "g_560[i].f0", print_hash_value);
        transparent_crc(g_560[i].f1, "g_560[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_561[i].f0, "g_561[i].f0", print_hash_value);
        transparent_crc(g_561[i].f1, "g_561[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_562[i].f0, "g_562[i].f0", print_hash_value);
        transparent_crc(g_562[i].f1, "g_562[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_563.f0, "g_563.f0", print_hash_value);
    transparent_crc(g_563.f1, "g_563.f1", print_hash_value);
    transparent_crc(g_564.f0, "g_564.f0", print_hash_value);
    transparent_crc(g_564.f1, "g_564.f1", print_hash_value);
    transparent_crc(g_565.f1, "g_565.f1", print_hash_value);
    transparent_crc(g_566.f0, "g_566.f0", print_hash_value);
    transparent_crc(g_566.f1, "g_566.f1", print_hash_value);
    transparent_crc(g_567.f1, "g_567.f1", print_hash_value);
    transparent_crc(g_568.f0, "g_568.f0", print_hash_value);
    transparent_crc(g_568.f1, "g_568.f1", print_hash_value);
    transparent_crc(g_569.f1, "g_569.f1", print_hash_value);
    transparent_crc(g_570.f0, "g_570.f0", print_hash_value);
    transparent_crc(g_570.f1, "g_570.f1", print_hash_value);
    transparent_crc(g_571.f0, "g_571.f0", print_hash_value);
    transparent_crc(g_571.f1, "g_571.f1", print_hash_value);
    transparent_crc(g_572.f0, "g_572.f0", print_hash_value);
    transparent_crc(g_572.f1, "g_572.f1", print_hash_value);
    transparent_crc(g_573.f1, "g_573.f1", print_hash_value);
    transparent_crc(g_574.f0, "g_574.f0", print_hash_value);
    transparent_crc(g_574.f1, "g_574.f1", print_hash_value);
    transparent_crc(g_575.f0, "g_575.f0", print_hash_value);
    transparent_crc(g_575.f1, "g_575.f1", print_hash_value);
    transparent_crc(g_576.f0, "g_576.f0", print_hash_value);
    transparent_crc(g_576.f1, "g_576.f1", print_hash_value);
    transparent_crc(g_577.f1, "g_577.f1", print_hash_value);
    transparent_crc(g_578.f0, "g_578.f0", print_hash_value);
    transparent_crc(g_578.f1, "g_578.f1", print_hash_value);
    transparent_crc(g_579.f0, "g_579.f0", print_hash_value);
    transparent_crc(g_579.f1, "g_579.f1", print_hash_value);
    transparent_crc(g_580.f0, "g_580.f0", print_hash_value);
    transparent_crc(g_580.f1, "g_580.f1", print_hash_value);
    transparent_crc(g_581.f0, "g_581.f0", print_hash_value);
    transparent_crc(g_581.f1, "g_581.f1", print_hash_value);
    transparent_crc(g_582.f0, "g_582.f0", print_hash_value);
    transparent_crc(g_582.f1, "g_582.f1", print_hash_value);
    transparent_crc(g_583.f0, "g_583.f0", print_hash_value);
    transparent_crc(g_583.f1, "g_583.f1", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_584[i].f0, "g_584[i].f0", print_hash_value);
        transparent_crc(g_584[i].f1, "g_584[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_585.f1, "g_585.f1", print_hash_value);
    transparent_crc(g_586.f0, "g_586.f0", print_hash_value);
    transparent_crc(g_586.f1, "g_586.f1", print_hash_value);
    transparent_crc(g_587.f0, "g_587.f0", print_hash_value);
    transparent_crc(g_587.f1, "g_587.f1", print_hash_value);
    transparent_crc(g_588.f0, "g_588.f0", print_hash_value);
    transparent_crc(g_588.f1, "g_588.f1", print_hash_value);
    transparent_crc(g_589.f0, "g_589.f0", print_hash_value);
    transparent_crc(g_589.f1, "g_589.f1", print_hash_value);
    transparent_crc(g_591.f0, "g_591.f0", print_hash_value);
    transparent_crc(g_591.f1, "g_591.f1", print_hash_value);
    transparent_crc(g_592, "g_592", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_686[i], "g_686[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_736[i], "g_736[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_795, "g_795", print_hash_value);
    transparent_crc(g_818, "g_818", print_hash_value);
    transparent_crc(g_833, "g_833", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_852[i][j][k].f0, "g_852[i][j][k].f0", print_hash_value);
                transparent_crc(g_852[i][j][k].f1, "g_852[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_861.f0, "g_861.f0", print_hash_value);
    transparent_crc(g_861.f1, "g_861.f1", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_899[i][j], "g_899[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_943, "g_943", print_hash_value);
    transparent_crc(g_944, "g_944", print_hash_value);
    transparent_crc(g_950, "g_950", print_hash_value);
    transparent_crc(g_978, "g_978", print_hash_value);
    transparent_crc(g_1036, "g_1036", print_hash_value);
    transparent_crc(g_1038, "g_1038", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1098[i][j][k], "g_1098[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1103.f0, "g_1103.f0", print_hash_value);
    transparent_crc(g_1103.f1, "g_1103.f1", print_hash_value);
    transparent_crc(g_1106, "g_1106", print_hash_value);
    transparent_crc(g_1181.f0, "g_1181.f0", print_hash_value);
    transparent_crc(g_1181.f1, "g_1181.f1", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc_bytes(&g_1227[i][j][k], sizeof(g_1227[i][j][k]), "g_1227[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1349[i][j], "g_1349[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1356, "g_1356", print_hash_value);
    transparent_crc(g_1369, "g_1369", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1501[i], "g_1501[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1537, "g_1537", print_hash_value);
    transparent_crc(g_1692, "g_1692", print_hash_value);
    transparent_crc(g_1696, "g_1696", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1707[i].f0, "g_1707[i].f0", print_hash_value);
        transparent_crc(g_1707[i].f1, "g_1707[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1765, "g_1765", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_1869[i][j][k].f0, "g_1869[i][j][k].f0", print_hash_value);
                transparent_crc(g_1869[i][j][k].f1, "g_1869[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1951.f0, "g_1951.f0", print_hash_value);
    transparent_crc(g_1951.f1, "g_1951.f1", print_hash_value);
    transparent_crc(g_2047.f0, "g_2047.f0", print_hash_value);
    transparent_crc(g_2047.f1, "g_2047.f1", print_hash_value);
    transparent_crc(g_2069.f0, "g_2069.f0", print_hash_value);
    transparent_crc(g_2069.f1, "g_2069.f1", print_hash_value);
    transparent_crc(g_2219, "g_2219", print_hash_value);
    transparent_crc(g_2270, "g_2270", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_2316[i][j][k], "g_2316[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2318, "g_2318", print_hash_value);
    transparent_crc(g_2392, "g_2392", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2507[i][j][k].f0, "g_2507[i][j][k].f0", print_hash_value);
                transparent_crc(g_2507[i][j][k].f1, "g_2507[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 553
XXX total union variables: 9

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 47
breakdown:
   depth: 1, occurrence: 175
   depth: 2, occurrence: 54
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
   depth: 7, occurrence: 1
   depth: 8, occurrence: 2
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 14, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 5
   depth: 24, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 3
   depth: 29, occurrence: 2
   depth: 30, occurrence: 3
   depth: 31, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 4
   depth: 35, occurrence: 2
   depth: 36, occurrence: 2
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1
   depth: 47, occurrence: 1

XXX total number of pointers: 483

XXX times a variable address is taken: 1092
XXX times a pointer is dereferenced on RHS: 249
breakdown:
   depth: 1, occurrence: 181
   depth: 2, occurrence: 53
   depth: 3, occurrence: 11
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 306
breakdown:
   depth: 1, occurrence: 260
   depth: 2, occurrence: 29
   depth: 3, occurrence: 9
   depth: 4, occurrence: 7
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 35
XXX times a pointer is compared with address of another variable: 18
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 4947

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1451
   level: 2, occurrence: 502
   level: 3, occurrence: 133
   level: 4, occurrence: 86
   level: 5, occurrence: 107
XXX number of pointers point to pointers: 193
XXX number of pointers point to scalars: 282
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.8
XXX average alias set size: 1.75

XXX times a non-volatile is read: 2027
XXX times a non-volatile is write: 982
XXX times a volatile is read: 90
XXX    times read thru a pointer: 8
XXX times a volatile is write: 24
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 2.94e+03
XXX percentage of non-volatile access: 96.3

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 199
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 36
   depth: 2, occurrence: 45
   depth: 3, occurrence: 27
   depth: 4, occurrence: 20
   depth: 5, occurrence: 38

XXX percentage a fresh-made variable is used: 16.4
XXX percentage an existing variable is used: 83.6
********************* end of statistics **********************/


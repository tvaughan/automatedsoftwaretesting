/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3657010358
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int64_t g_4 = 1L;
static int32_t g_31[7] = {8L,8L,8L,8L,8L,8L,8L};
static int64_t g_33 = (-1L);
static uint8_t g_52 = 4UL;
static int16_t g_62 = 5L;
static uint64_t g_74 = 0UL;
static int16_t g_75 = 0L;
static uint8_t g_85[9] = {248UL,248UL,1UL,248UL,248UL,1UL,248UL,248UL,1UL};
static uint64_t g_91 = 0xE44E384C83B0F605LL;
static uint64_t g_96 = 0x7DE7E8591F28F681LL;
static volatile int32_t g_101[10][5] = {{0L,5L,(-6L),5L,0L},{0x060950C4L,5L,0L,6L,0L},{0L,6L,0L,5L,0x060950C4L},{0L,5L,(-6L),5L,0L},{0x060950C4L,5L,0L,6L,0L},{0L,6L,0L,5L,0x060950C4L},{6L,1L,(-1L),1L,6L},{0x0F30BDA1L,1L,8L,1L,6L},{6L,1L,8L,1L,0x0F30BDA1L},{6L,1L,(-1L),1L,6L}};
static volatile int32_t *g_100 = &g_101[5][1];
static float g_128[7] = {0x7.FB1DA9p+35,0x7.FB1DA9p+35,0x7.FB1DA9p+35,0x7.FB1DA9p+35,0x7.FB1DA9p+35,0x7.FB1DA9p+35,0x7.FB1DA9p+35};
static int32_t *g_131[1] = {&g_31[1]};
static int32_t **g_130 = &g_131[0];
static int32_t ***g_129[4] = {&g_130,&g_130,&g_130,&g_130};
static int16_t g_140 = 1L;
static int32_t ****g_153 = &g_129[1];
static int32_t ***** volatile g_152 = &g_153;/* VOLATILE GLOBAL g_152 */
static uint16_t g_195 = 0x4983L;
static float g_221 = 0xC.F97334p-36;
static float g_223 = 0x9.0A6FF8p+25;
static int32_t g_224 = 0x85C5DCDBL;
static uint32_t g_225 = 0x0DEC96D4L;
static int64_t *g_244[7] = {&g_33,(void*)0,&g_33,&g_33,(void*)0,&g_33,&g_33};
static int64_t ** volatile g_243 = &g_244[0];/* VOLATILE GLOBAL g_243 */
static int32_t *g_261 = &g_31[1];
static int32_t g_282 = 0x08088E2FL;
static int8_t g_333 = 0xB8L;
static int32_t *****g_347 = &g_153;
static int64_t **g_397 = &g_244[0];
static int16_t g_409 = 0xD728L;
static int32_t g_439[8] = {5L,5L,0L,5L,5L,0L,5L,5L};
static float * const  volatile g_481[2][4][3] = {{{&g_221,&g_221,&g_221},{&g_221,&g_221,&g_221},{&g_221,&g_221,&g_221},{&g_221,&g_221,&g_221}},{{&g_221,&g_221,&g_221},{&g_221,&g_221,&g_221},{&g_221,&g_221,&g_221},{&g_221,&g_221,&g_221}}};
static const uint64_t g_500 = 0xC236999E5A16D031LL;
static float *g_505 = &g_128[1];
static float **g_504 = &g_505;
static float *** volatile g_503 = &g_504;/* VOLATILE GLOBAL g_503 */
static int64_t g_569 = 9L;
static uint32_t g_594 = 0xAE953FBFL;
static volatile int32_t g_627 = 0L;/* VOLATILE GLOBAL g_627 */
static const int16_t g_636 = (-10L);
static const int16_t *g_635[6] = {(void*)0,&g_636,(void*)0,(void*)0,&g_636,(void*)0};
static uint8_t **g_650 = (void*)0;
static uint8_t ***g_649 = &g_650;
static uint32_t g_665 = 0x1915685BL;
static int32_t * volatile g_746 = &g_31[1];/* VOLATILE GLOBAL g_746 */
static volatile int16_t g_754 = 9L;/* VOLATILE GLOBAL g_754 */
static int32_t ** volatile g_851[2][9] = {{&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261},{&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261}};
static int32_t ** volatile g_878 = (void*)0;/* VOLATILE GLOBAL g_878 */
static const uint8_t g_912[8][1][6] = {{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}},{{0UL,248UL,0UL,0UL,248UL,0UL}}};
static float *** volatile g_928 = &g_504;/* VOLATILE GLOBAL g_928 */
static int32_t *** const **g_941 = (void*)0;
static const int32_t g_954 = 0xCB6185D6L;
static const int32_t *g_956[6] = {&g_954,&g_954,&g_954,&g_954,&g_954,&g_954};
static const int32_t ** volatile g_955[6][2][4] = {{{&g_956[5],(void*)0,&g_956[3],&g_956[3]},{&g_956[1],(void*)0,&g_956[1],&g_956[3]}},{{(void*)0,&g_956[3],&g_956[3],&g_956[3]},{&g_956[3],&g_956[3],&g_956[5],&g_956[3]}},{{&g_956[3],&g_956[5],&g_956[5],&g_956[3]},{&g_956[3],&g_956[3],&g_956[3],&g_956[1]}},{{(void*)0,&g_956[3],&g_956[1],&g_956[0]},{&g_956[1],&g_956[0],&g_956[3],&g_956[0]}},{{&g_956[5],&g_956[3],&g_956[3],&g_956[1]},{&g_956[5],&g_956[3],&g_956[0],&g_956[3]}},{{&g_956[3],&g_956[5],&g_956[5],&g_956[5]},{&g_956[0],&g_956[0],(void*)0,&g_956[3]}}};
static const int64_t ** const g_962 = (void*)0;
static float * const **g_1070 = (void*)0;
static volatile int16_t * volatile * volatile *g_1080 = (void*)0;
static volatile uint32_t g_1089[10] = {6UL,1UL,6UL,0x4D193329L,0x4D193329L,6UL,1UL,6UL,0x4D193329L,0x4D193329L};
static uint32_t *g_1102 = &g_225;
static volatile float g_1112 = (-0x1.9p-1);/* VOLATILE GLOBAL g_1112 */
static volatile float * const  volatile g_1111 = &g_1112;/* VOLATILE GLOBAL g_1111 */
static volatile float * const  volatile *g_1110 = &g_1111;
static volatile float * const  volatile * volatile * volatile g_1109 = &g_1110;/* VOLATILE GLOBAL g_1109 */
static uint8_t * const *g_1154 = (void*)0;
static uint8_t * const **g_1153[7] = {&g_1154,&g_1154,&g_1154,&g_1154,&g_1154,&g_1154,&g_1154};
static uint8_t * const ***g_1152 = &g_1153[6];
static uint32_t g_1246 = 5UL;
static uint8_t ** const **g_1264 = (void*)0;
static uint8_t ** const ***g_1263 = &g_1264;
static volatile uint16_t g_1271[6][5][3] = {{{0x35A3L,65535UL,65527UL},{0UL,0x55DAL,0UL},{65535UL,0x35A3L,65527UL},{0x7313L,0x2564L,0UL},{0x54F7L,0x35A3L,0x35A3L}},{{0UL,0x55DAL,0x170BL},{0x54F7L,65535UL,0x54F7L},{0x7313L,8UL,0x170BL},{65535UL,65535UL,0x35A3L},{0UL,8UL,0UL}},{{0x35A3L,65535UL,65527UL},{0UL,0x55DAL,0UL},{65535UL,0x35A3L,65527UL},{0x7313L,0x2564L,0UL},{0x54F7L,0x35A3L,0x35A3L}},{{0UL,0x55DAL,0x170BL},{0x54F7L,65535UL,0x54F7L},{0x7313L,8UL,0x170BL},{65535UL,65535UL,0x35A3L},{0UL,8UL,0UL}},{{0x35A3L,65535UL,65527UL},{0UL,0x55DAL,0UL},{65535UL,0x35A3L,65527UL},{0x7313L,0x2564L,0UL},{0x54F7L,0x35A3L,0x35A3L}},{{0UL,0x55DAL,0x170BL},{0x54F7L,65535UL,0x54F7L},{0x7313L,8UL,0x170BL},{65535UL,65535UL,0x35A3L},{0UL,8UL,0UL}}};
static volatile uint16_t * const  volatile g_1270[6][2][10] = {{{&g_1271[4][4][2],(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,&g_1271[4][4][2],&g_1271[2][1][0]},{(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0}},{{&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[4][4][2],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[4][4][2],&g_1271[2][1][0]},{&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],(void*)0}},{{&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[4][4][2],(void*)0,(void*)0,&g_1271[2][1][0]},{&g_1271[2][1][0],(void*)0,&g_1271[2][1][0],(void*)0,&g_1271[2][1][0],(void*)0,&g_1271[4][4][2],&g_1271[4][4][2],(void*)0,&g_1271[2][1][0]}},{{&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[4][4][2],(void*)0,(void*)0},{(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0}},{{&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[4][4][2],(void*)0},{(void*)0,&g_1271[2][1][0],&g_1271[4][4][2],&g_1271[2][1][0],&g_1271[4][4][2],&g_1271[2][1][0],(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[2][1][0],(void*)0},{&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[2][1][0],(void*)0,&g_1271[2][1][0],(void*)0,(void*)0,&g_1271[2][1][0],&g_1271[2][1][0]}}};
static volatile uint16_t * const  volatile * volatile g_1269 = &g_1270[5][0][2];/* VOLATILE GLOBAL g_1269 */
static int8_t g_1285 = 0x55L;
static volatile int8_t g_1297 = (-1L);/* VOLATILE GLOBAL g_1297 */
static volatile int8_t *g_1296 = &g_1297;
static volatile int8_t ** volatile g_1295 = &g_1296;/* VOLATILE GLOBAL g_1295 */
static volatile int8_t ** volatile * const g_1294 = &g_1295;
static int64_t g_1307 = 0x5BFFE50D39F05FB8LL;
static volatile uint32_t g_1313 = 0x4A5A5653L;/* VOLATILE GLOBAL g_1313 */
static int32_t g_1384 = (-10L);
static volatile uint16_t g_1389 = 65535UL;/* VOLATILE GLOBAL g_1389 */
static int8_t *g_1394 = &g_1285;
static int8_t *g_1395 = &g_1285;
static volatile uint16_t * const  volatile * volatile *g_1431 = (void*)0;
static volatile uint16_t * const  volatile * volatile ** const  volatile g_1430 = &g_1431;/* VOLATILE GLOBAL g_1430 */
static volatile uint32_t g_1498[6] = {5UL,0x2BA6701CL,0x2BA6701CL,5UL,0x2BA6701CL,0x2BA6701CL};
static uint32_t g_1540[3][9] = {{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL}};
static uint16_t g_1556 = 0UL;
static volatile int64_t g_1608 = 0xE379D34C9BE97EB5LL;/* VOLATILE GLOBAL g_1608 */
static volatile int64_t *g_1607[7][3][3] = {{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}},{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}},{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}},{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}},{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}},{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}},{{&g_1608,&g_1608,&g_1608},{&g_1608,&g_1608,(void*)0},{&g_1608,&g_1608,&g_1608}}};
static volatile int64_t ** volatile g_1606[2] = {&g_1607[5][2][1],&g_1607[5][2][1]};
static volatile int64_t ** volatile * volatile g_1605 = &g_1606[1];/* VOLATILE GLOBAL g_1605 */
static volatile int64_t ** volatile * volatile * volatile g_1604[5] = {&g_1605,&g_1605,&g_1605,&g_1605,&g_1605};
static uint32_t g_1647 = 0x02084D91L;
static int8_t g_1682 = 0L;
static int32_t g_1683 = 0xE1F5CCF8L;
static uint32_t *g_1727 = &g_1246;
static uint32_t *g_1728 = (void*)0;
static volatile int64_t g_1751 = 0x5DAEA7BF8CA96840LL;/* VOLATILE GLOBAL g_1751 */
static float ****g_1780 = (void*)0;
static float ** const **g_1783 = (void*)0;
static float g_1964 = 0x9.5p+1;
static int32_t *g_2064 = (void*)0;
static int32_t ** const g_2063 = &g_2064;
static const int32_t **g_2144[4] = {&g_956[1],&g_956[1],&g_956[1],&g_956[1]};
static const int32_t ***g_2143 = &g_2144[3];
static uint16_t *g_2181 = &g_195;
static uint16_t **g_2180 = &g_2181;
static uint16_t ***g_2179[2][7] = {{&g_2180,&g_2180,&g_2180,&g_2180,&g_2180,&g_2180,&g_2180},{&g_2180,&g_2180,&g_2180,&g_2180,&g_2180,&g_2180,&g_2180}};
static uint8_t ****g_2338 = (void*)0;
static uint8_t *****g_2337 = &g_2338;
static int32_t *g_2377[3][3][1] = {{{(void*)0},{&g_31[1]},{(void*)0}},{{&g_31[1]},{(void*)0},{&g_31[1]}},{{(void*)0},{&g_31[1]},{(void*)0}}};
static volatile int8_t ** volatile ***g_2463 = (void*)0;
static int8_t **g_2467[1] = {&g_1395};
static int8_t ***g_2466 = &g_2467[0];
static int8_t **** const g_2465 = &g_2466;
static int8_t **** const *g_2464[4][4][1] = {{{&g_2465},{&g_2465},{&g_2465},{&g_2465}},{{&g_2465},{&g_2465},{&g_2465},{&g_2465}},{{&g_2465},{&g_2465},{&g_2465},{&g_2465}},{{&g_2465},{&g_2465},{&g_2465},{&g_2465}}};
static volatile int8_t ** volatile *g_2504 = (void*)0;
static volatile int8_t ** volatile **g_2503 = &g_2504;
static float g_2517 = 0x5.F98B19p-42;
static int64_t g_2542 = 0x57B0DF1BF687C1ADLL;
static volatile int32_t g_2560 = 0x98EE419AL;/* VOLATILE GLOBAL g_2560 */
static uint32_t g_2652 = 0x9BB2DED9L;
static int8_t g_2653 = 0xC6L;
static float g_2724 = 0xF.C3D366p+88;
static uint16_t g_2746 = 0x99A9L;
static int16_t *g_2762 = (void*)0;
static int32_t g_2957[5][5][1] = {{{(-9L)},{0x933CFFB8L},{0L},{0x20BC3168L},{0x35BE4E44L}},{{0x20BC3168L},{0L},{0x933CFFB8L},{0L},{0x20BC3168L}},{{0x35BE4E44L},{0x20BC3168L},{0L},{0x933CFFB8L},{0L}},{{0x20BC3168L},{0x35BE4E44L},{0x20BC3168L},{0L},{0x933CFFB8L}},{{0L},{0x20BC3168L},{0x35BE4E44L},{0x20BC3168L},{0L}}};
static volatile int32_t g_3038 = 0xBA2AED66L;/* VOLATILE GLOBAL g_3038 */
static const uint64_t g_3078 = 0xCB651077630EE18BLL;
static const float ***g_3137 = (void*)0;
static const float ****g_3136 = &g_3137;
static int8_t ****g_3157 = &g_2466;
static int8_t *****g_3156 = &g_3157;
static volatile float g_3177 = 0x1.2p+1;/* VOLATILE GLOBAL g_3177 */
static volatile int16_t g_3179 = 0x068FL;/* VOLATILE GLOBAL g_3179 */
static int16_t **g_3188 = (void*)0;
static int16_t ***g_3187 = &g_3188;
static int16_t ****g_3186 = &g_3187;
static int16_t *****g_3185[4] = {&g_3186,&g_3186,&g_3186,&g_3186};
static volatile uint16_t g_3273 = 1UL;/* VOLATILE GLOBAL g_3273 */
static volatile uint8_t g_3336 = 1UL;/* VOLATILE GLOBAL g_3336 */
static int32_t g_3540 = 3L;


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static uint32_t  func_5(uint16_t  p_6, int8_t  p_7, int16_t  p_8, int32_t  p_9, uint32_t  p_10);
static uint64_t  func_16(uint32_t  p_17, uint8_t  p_18);
static uint8_t  func_23(uint32_t  p_24, uint32_t  p_25);
static int64_t  func_43(int8_t  p_44, int16_t  p_45, const int8_t  p_46, const uint32_t  p_47);
static int8_t  func_48(uint8_t  p_49, int64_t * const  p_50);
static uint8_t  func_67(uint64_t  p_68, uint8_t * p_69, int16_t * const  p_70, int16_t  p_71, const int64_t ** const  p_72);
static const int64_t ** const  func_78(int64_t  p_79, int32_t * p_80);
static const uint8_t  func_86(uint64_t  p_87, uint64_t  p_88, uint64_t  p_89);
static uint8_t  func_97(int32_t  p_98);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_31 g_52 g_75 g_962 g_261 g_101 g_85 g_225 g_224 g_130 g_131 g_153 g_129 g_504 g_505 g_140 g_33 g_333 g_100 g_649 g_650 g_152 g_409 g_665 g_96 g_128 g_347 g_282 g_1102 g_954 g_594 g_439 g_1246 g_1269 g_1109 g_1110 g_1111 g_746 g_1294 g_1307 g_1313 g_569 g_1296 g_1297 g_912 g_1384 g_1389 g_1285 g_1430 g_1395 g_195 g_1394 g_1498 g_62 g_1112 g_1540 g_1556 g_91 g_1089 g_1604 g_1647 g_1295 g_1682 g_1683 g_503 g_2143 g_2144 g_1271 g_1727 g_956 g_2181 g_74 g_2463 g_2464 g_500 g_928 g_2503 g_2180 g_1080 g_1431 g_2466 g_2467 g_2652 g_2653 g_2465 g_1605 g_1606 g_2724 g_2746 g_2762 g_2560 g_2957
 * writes: g_31 g_33 g_52 g_62 g_74 g_75 g_224 g_131 g_261 g_505 g_101 g_594 g_244 g_128 g_665 g_96 g_91 g_225 g_282 g_1263 g_195 g_1112 g_439 g_569 g_140 g_1384 g_1389 g_1394 g_1395 g_1431 g_1285 g_409 g_153 g_1556 g_85 g_1246 g_1727 g_1728 g_1683 g_956 g_2337 g_2377 g_2542 g_2466 g_2746 g_1607 g_2762 g_2653 g_221 g_333 g_2957
 */
static float  func_1(void)
{ /* block id: 0 */
    uint32_t l_13 = 4294967290UL;
    int32_t *l_30 = &g_31[1];
    int64_t *l_32 = &g_33;
    uint16_t l_2955 = 3UL;
    int32_t *l_2956 = &g_2957[3][3][0];
    int16_t **l_2972[10] = {&g_2762,&g_2762,&g_2762,&g_2762,&g_2762,&g_2762,&g_2762,&g_2762,&g_2762,&g_2762};
    int16_t ***l_2971 = &l_2972[7];
    uint32_t **l_2974 = (void*)0;
    int32_t l_3093[10][6][4] = {{{0xF3A842F7L,0x923E3370L,(-5L),0x0B05923EL},{0x58C9DABAL,1L,0xA0F9961EL,(-9L)},{6L,0x8E02A663L,(-1L),(-8L)},{1L,(-1L),1L,0xACED1330L},{0x7976B33CL,0x5620F72FL,(-1L),(-4L)},{0x01A9305AL,0x60E05A06L,0L,1L}},{{(-1L),0L,0L,0xC1BB8B94L},{0L,0xCFA5E0E6L,0xA52678DAL,0x01A9305AL},{0xCD50B0D2L,0x03A6C43FL,0x58C9DABAL,9L},{(-1L),0x8E903D0EL,0xACED1330L,8L},{0L,0x177C6DFBL,0x0B1C5B76L,1L},{0xC1BB8B94L,0xF832A2E2L,0x796DCDECL,0x7976B33CL}},{{1L,0x183D04C8L,0L,0x474D0F33L},{1L,0xF3A842F7L,0xA2C17809L,(-1L)},{1L,0x39C4C9CDL,0xCCFE0DD9L,0xA52678DAL},{(-4L),0x6B1416DFL,0xCDC9FE02L,0x0B1C5B76L},{0x2DC55247L,9L,0x903391BAL,0x57760BAEL},{0x2BA82B99L,0xCDC9FE02L,0x2BA82B99L,2L}},{{(-1L),0x796DCDECL,0x4895F84AL,(-1L)},{(-1L),0x2DC55247L,0x349DCE7CL,0x796DCDECL},{0xACED1330L,0L,0x349DCE7CL,0x183D04C8L},{(-1L),(-1L),0x4895F84AL,7L},{(-1L),0L,0x2BA82B99L,0x8FC2E56FL},{0x2BA82B99L,0x8FC2E56FL,0x903391BAL,1L}},{{0x2DC55247L,(-8L),0xCDC9FE02L,0x2C8A797BL},{(-4L),0x72929495L,0xCCFE0DD9L,(-1L)},{1L,0xCE395A9FL,0xA2C17809L,1L},{1L,0xB42E6AE3L,0L,0x58C9DABAL},{1L,0xD510D308L,0x796DCDECL,0L},{0xC1BB8B94L,(-1L),0x0B1C5B76L,0xC680CBDFL}},{{0L,1L,0xACED1330L,0L},{(-1L),0x57760BAEL,0x58C9DABAL,0L},{0xCD50B0D2L,8L,0xA52678DAL,0xB42E6AE3L},{0L,7L,0L,0x6B1416DFL},{(-1L),1L,0L,0x923E3370L},{0x01A9305AL,0x58C9DABAL,(-1L),0x03A6C43FL}},{{0x7976B33CL,1L,1L,0xF367A4E5L},{1L,5L,(-1L),0xA9B2194DL},{6L,0xCFA5E0E6L,0x4895F84AL,0x7976B33CL},{0L,0xA52678DAL,0x72929495L,0x1238AC8BL},{0x07EF05B7L,0x57760BAEL,(-1L),8L},{(-1L),0xFA188A09L,0x1238AC8BL,0L}},{{0xCD50B0D2L,(-1L),0L,(-1L)},{0L,0x1A7A5CAAL,0x968D8BE6L,0xF367A4E5L},{0x923E3370L,0x5620F72FL,1L,0xCFA5E0E6L},{0L,5L,5L,0L},{1L,1L,0x8FC2E56FL,0xBCE6C1A6L},{0x0B05923EL,0x07373913L,2L,0x2467C34EL}},{{0x39C4C9CDL,7L,0xCDC9FE02L,0x2467C34EL},{1L,0x07373913L,0xC1BB8B94L,0xBCE6C1A6L},{(-1L),1L,1L,0L},{0L,5L,(-8L),0xCFA5E0E6L},{0xA0F9961EL,0x5620F72FL,0x2C8A797BL,0xF367A4E5L},{0x474D0F33L,0x1A7A5CAAL,(-1L),(-1L)}},{{(-4L),(-1L),1L,0L},{0x5620F72FL,0xFA188A09L,0x183D04C8L,8L},{1L,0x57760BAEL,0x72B3687DL,0x1238AC8BL},{0x1238AC8BL,0xA52678DAL,1L,0x7976B33CL},{5L,0xCFA5E0E6L,0x903391BAL,0x0B05923EL},{0xA9B2194DL,9L,0x0B1C5B76L,0L}}};
    int16_t *****l_3184 = (void*)0;
    int32_t *****l_3191 = (void*)0;
    float l_3205 = 0x1.Ep+1;
    uint8_t l_3219[10][6] = {{0x7DL,0UL,0UL,0x7DL,0x7DL,0UL},{0x7DL,0x7DL,0UL,0UL,0x7DL,0x7DL},{0x7DL,0UL,0UL,0x7DL,0x7DL,0UL},{0x7DL,0x7DL,0UL,0UL,0x7DL,0x7DL},{0x7DL,0UL,0UL,0x7DL,0x7DL,0UL},{0x7DL,0x7DL,0UL,0UL,0x7DL,0x7DL},{0x7DL,0UL,0UL,0x7DL,0x7DL,0UL},{0x7DL,0x7DL,0UL,0UL,0x7DL,0x7DL},{0x7DL,0UL,0UL,0x7DL,0x7DL,0UL},{0x7DL,0x7DL,0UL,0UL,0x7DL,0x7DL}};
    uint32_t l_3245 = 0x7AEF3532L;
    uint32_t l_3291[2];
    int32_t l_3305 = 0x5D9F57E8L;
    float l_3306 = 0x4.3358FFp-37;
    const uint32_t l_3316 = 0xA0DB0D6CL;
    uint32_t l_3327 = 0x07D608F6L;
    int64_t **l_3393 = &g_244[0];
    const int64_t l_3400[3][3][6] = {{{(-1L),0x2D5F1B263ADA4349LL,(-1L),0xBC65A84099CB98DFLL,0xBC65A84099CB98DFLL,(-1L)},{0x7C9DBC93E4FC2FD5LL,0x7C9DBC93E4FC2FD5LL,0x8FD542F83E347A58LL,0xBC65A84099CB98DFLL,0x2D5F1B263ADA4349LL,0xC3A2A481733E9E94LL},{(-1L),0x8FD542F83E347A58LL,0x26AF3B1D5E065200LL,0xC3A2A481733E9E94LL,0x26AF3B1D5E065200LL,0x8FD542F83E347A58LL}},{{0xBC65A84099CB98DFLL,(-1L),0x26AF3B1D5E065200LL,(-4L),0x7C9DBC93E4FC2FD5LL,0xC3A2A481733E9E94LL},{(-1L),(-4L),0x8FD542F83E347A58LL,0x8FD542F83E347A58LL,(-4L),(-1L)},{0x8FD542F83E347A58LL,(-4L),(-1L),7L,0x7C9DBC93E4FC2FD5LL,0x26AF3B1D5E065200LL}},{{0x26AF3B1D5E065200LL,(-1L),0xBC65A84099CB98DFLL,(-1L),0x26AF3B1D5E065200LL,(-4L)},{0x26AF3B1D5E065200LL,0x8FD542F83E347A58LL,(-2L),0x2D5F1B263ADA4349LL,0x26AF3B1D5E065200LL,0x26AF3B1D5E065200LL},{0x7C9DBC93E4FC2FD5LL,0xC3A2A481733E9E94LL,0xC3A2A481733E9E94LL,0x7C9DBC93E4FC2FD5LL,(-4L),0x26AF3B1D5E065200LL}}};
    int16_t l_3488[5] = {0L,0L,0L,0L,0L};
    int16_t l_3556 = 0x6C7AL;
    int32_t l_3567 = 0x969F73DEL;
    uint64_t l_3568[7] = {5UL,5UL,5UL,5UL,5UL,5UL,5UL};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_3291[i] = 7UL;
    (*l_2956) ^= (safe_mul_func_int16_t_s_s((g_4 > (func_5((((safe_rshift_func_int8_t_s_s(l_13, ((((safe_sub_func_int64_t_s_s(l_13, func_16((safe_add_func_int64_t_s_s((safe_mod_func_uint8_t_u_u(func_23((0xD2D1A4E665969A22LL & (safe_lshift_func_int8_t_s_s((l_13 <= (((*l_30) |= 0x6AEBA4D3L) >= l_13)), (((*l_32) = g_4) < (safe_sub_func_int64_t_s_s((safe_mod_func_uint64_t_u_u((!((l_13 >= g_4) || g_4)), g_4)), g_4)))))), l_13), l_13)), l_13)), l_13))) , g_2652) < l_13) >= l_13))) | g_500) & l_13), g_2653, l_13, g_2653, l_13) != (-3L))), l_2955));
    for (g_1683 = 0; (g_1683 == 0); g_1683 = safe_add_func_int64_t_s_s(g_1683, 1))
    { /* block id: 1317 */
        float l_2960[6] = {0xC.075156p+54,0x1.1p-1,0x1.1p-1,0xC.075156p+54,0x1.1p-1,0x1.1p-1};
        int32_t l_2961[6];
        int32_t l_2970 = 0L;
        int16_t l_3007 = 0x7525L;
        int32_t *l_3013 = &l_2961[3];
        uint32_t l_3028 = 6UL;
        const uint16_t l_3039 = 65535UL;
        uint64_t l_3075 = 0x89619ECB3BC8D84DLL;
        int16_t l_3076 = (-1L);
        float l_3077 = 0x2.517446p-25;
        const float l_3092[9] = {0x5.5p-1,0x5.5p-1,0x5.5p-1,0x5.5p-1,0x5.5p-1,0x5.5p-1,0x5.5p-1,0x5.5p-1,0x5.5p-1};
        float l_3094 = 0x1.8AA032p+88;
        const int64_t *l_3103 = (void*)0;
        const int64_t **l_3102[4] = {&l_3103,&l_3103,&l_3103,&l_3103};
        const int64_t ***l_3101 = &l_3102[0];
        const uint8_t *l_3133 = &g_912[1][0][3];
        const uint8_t ** const l_3132[8][6][5] = {{{(void*)0,(void*)0,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{(void*)0,(void*)0,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{(void*)0,(void*)0,&l_3133,&l_3133,(void*)0},{&l_3133,(void*)0,&l_3133,&l_3133,&l_3133},{&l_3133,(void*)0,&l_3133,(void*)0,&l_3133},{&l_3133,&l_3133,&l_3133,(void*)0,&l_3133},{(void*)0,&l_3133,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{(void*)0,(void*)0,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{(void*)0,(void*)0,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{(void*)0,(void*)0,&l_3133,&l_3133,(void*)0},{&l_3133,(void*)0,&l_3133,&l_3133,&l_3133},{&l_3133,(void*)0,&l_3133,(void*)0,&l_3133},{&l_3133,&l_3133,&l_3133,(void*)0,&l_3133},{(void*)0,&l_3133,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{(void*)0,(void*)0,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{(void*)0,(void*)0,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,(void*)0,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{&l_3133,&l_3133,(void*)0,&l_3133,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{(void*)0,&l_3133,(void*)0,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{(void*)0,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,(void*)0,&l_3133,&l_3133,&l_3133},{(void*)0,&l_3133,&l_3133,&l_3133,(void*)0},{&l_3133,&l_3133,&l_3133,(void*)0,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}},{{&l_3133,&l_3133,(void*)0,&l_3133,(void*)0},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133},{(void*)0,&l_3133,(void*)0,&l_3133,&l_3133},{&l_3133,&l_3133,&l_3133,&l_3133,&l_3133}}};
        uint16_t l_3170 = 7UL;
        uint16_t l_3181 = 0x3B83L;
        int32_t l_3208 = (-5L);
        int16_t l_3215 = (-3L);
        uint16_t ****l_3223 = &g_2179[0][3];
        uint16_t l_3252[1];
        int64_t l_3318 = (-8L);
        int32_t l_3333[3];
        int32_t * const *l_3335 = &g_131[0];
        int32_t * const ** const l_3334 = &l_3335;
        uint32_t l_3434[5][7] = {{0x5D85CC73L,2UL,0x5D85CC73L,0x5D85CC73L,2UL,0x5D85CC73L,0x5D85CC73L},{7UL,7UL,0x173ED3EFL,7UL,7UL,0x173ED3EFL,7UL},{2UL,0x5D85CC73L,0x5D85CC73L,2UL,0x5D85CC73L,0x5D85CC73L,2UL},{1UL,7UL,1UL,1UL,7UL,1UL,1UL},{2UL,2UL,1UL,2UL,2UL,1UL,2UL}};
        int64_t l_3460 = 0L;
        int32_t ** const l_3461 = &g_2064;
        uint16_t l_3489[4][8][4] = {{{0x2F26L,1UL,1UL,0x2F26L},{0x1D25L,65532UL,0UL,0x2F86L},{0x4341L,65529UL,0x2F86L,0xCB30L},{0x2F86L,0xCB30L,65535UL,0xCB30L},{1UL,65529UL,1UL,0x2F86L},{0x8076L,65532UL,0xCB30L,0x2F26L},{1UL,1UL,3UL,3UL},{1UL,1UL,0xCB30L,0x1D25L}},{{0x8076L,3UL,1UL,65532UL},{1UL,0x4341L,65535UL,1UL},{0x2F86L,0x4341L,0x2F86L,65532UL},{0x4341L,3UL,0UL,0x1D25L},{0x1D25L,1UL,1UL,3UL},{0x2F26L,1UL,1UL,0x2F26L},{0x1D25L,65532UL,0UL,0x2F86L},{0x4341L,65529UL,0x2F86L,0xCB30L}},{{0x2F86L,0x4341L,0x8076L,0x4341L},{0x2F26L,0UL,0xCB30L,3UL},{0x2F86L,65529UL,0x4341L,1UL},{0xCB30L,0x2F26L,1UL,1UL},{0xCB30L,0xCB30L,0x4341L,65532UL},{0x2F86L,1UL,0xCB30L,65529UL},{0x2F26L,0x1D25L,0x8076L,0xCB30L},{3UL,0x1D25L,3UL,65529UL}},{{0x1D25L,1UL,65535UL,65532UL},{65532UL,0xCB30L,0x2F26L,1UL},{1UL,0x2F26L,0x2F26L,1UL},{65532UL,65529UL,65535UL,3UL},{0x1D25L,0UL,3UL,0x4341L},{3UL,0x4341L,0x8076L,0x4341L},{0x2F26L,0UL,0xCB30L,3UL},{0x2F86L,65529UL,0x4341L,1UL}}};
        int32_t l_3494 = 0x60BD53E8L;
        int32_t l_3537 = 0xF8C1268BL;
        uint8_t l_3553 = 0x1DL;
        int8_t l_3557 = 0xA5L;
        int32_t *l_3558 = &l_3305;
        int32_t *l_3559 = (void*)0;
        int32_t *l_3560 = (void*)0;
        int32_t *l_3561 = &l_3093[5][1][0];
        int32_t *l_3562 = &g_224;
        int32_t *l_3563 = &l_3305;
        int32_t *l_3564 = (void*)0;
        int32_t *l_3565 = &l_3093[0][5][3];
        int32_t *l_3566[7][9] = {{&g_31[1],&g_31[1],&g_224,&l_3093[4][4][3],&l_2961[1],&l_3093[5][4][1],&g_31[1],&l_2961[1],(void*)0},{&g_224,&g_439[0],(void*)0,&l_2961[1],&l_2961[1],(void*)0,&g_439[0],&g_224,&g_224},{&g_224,&l_2961[1],&g_224,&g_224,&g_439[0],(void*)0,&l_2961[1],&l_2961[1],(void*)0},{&g_31[1],&l_2961[1],(void*)0,&l_2961[1],&g_31[1],&l_3093[5][4][1],&l_2961[1],&l_3093[4][4][3],&g_224},{&l_2961[1],&g_439[0],(void*)0,&l_3093[4][4][3],&g_439[0],&g_224,&g_439[0],&l_3093[4][4][3],(void*)0},{&g_31[1],&g_31[1],&g_224,&l_3093[4][4][3],&l_2961[1],&l_3093[5][4][1],&g_31[1],&l_2961[1],(void*)0},{&g_224,&g_439[0],(void*)0,&l_2961[1],&l_2961[1],(void*)0,&g_439[0],&g_224,&g_224}};
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_2961[i] = 0xDD985025L;
        for (i = 0; i < 1; i++)
            l_3252[i] = 0UL;
        for (i = 0; i < 3; i++)
            l_3333[i] = 1L;
    }
    return (*l_30);
}


/* ------------------------------------------ */
/* 
 * reads : g_2465 g_1605 g_1606 g_505 g_128 g_569 g_52 g_100 g_101 g_261 g_439 g_31 g_91 g_2466 g_2467 g_1395 g_1285 g_1294 g_1295 g_1296 g_1297 g_2181 g_195 g_1109 g_1110 g_1111 g_1112 g_2724 g_504 g_2180 g_2746 g_1727 g_665 g_1246 g_224 g_1683 g_1394 g_140 g_746 g_2762 g_409 g_503 g_954 g_2560 g_1556 g_1430 g_1431 g_130
 * writes: g_2466 g_569 g_52 g_101 g_74 g_439 g_31 g_91 g_1285 g_409 g_128 g_195 g_2746 g_665 g_1246 g_1389 g_1607 g_2762 g_2653 g_224 g_140 g_1683 g_221 g_225 g_1384 g_333 g_131
 */
static uint32_t  func_5(uint16_t  p_6, int8_t  p_7, int16_t  p_8, int32_t  p_9, uint32_t  p_10)
{ /* block id: 1157 */
    int8_t ***l_2660 = &g_2467[0];
    int32_t l_2664[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    int32_t **l_2665 = &g_2064;
    int32_t ***l_2666 = &l_2665;
    const int64_t *l_2670 = &g_569;
    const int64_t **l_2669 = &l_2670;
    const int64_t ***l_2671 = (void*)0;
    const int64_t ***l_2672 = (void*)0;
    uint8_t **l_2673 = (void*)0;
    int64_t *l_2674 = &g_569;
    int16_t *l_2699 = &g_409;
    int16_t **l_2698[10] = {&l_2699,(void*)0,&l_2699,(void*)0,&l_2699,(void*)0,&l_2699,(void*)0,&l_2699,(void*)0};
    int16_t ***l_2697 = &l_2698[4];
    int16_t ****l_2696 = &l_2697;
    uint16_t **** const l_2715[5] = {&g_2179[1][2],&g_2179[1][2],&g_2179[1][2],&g_2179[1][2],&g_2179[1][2]};
    int16_t *l_2763 = (void*)0;
    int32_t *l_2764 = &g_224;
    int32_t *l_2765 = (void*)0;
    int32_t *l_2766 = (void*)0;
    int32_t *l_2767 = (void*)0;
    int32_t *l_2768 = (void*)0;
    int32_t *l_2769[10] = {(void*)0,&g_31[6],&l_2664[6],&l_2664[6],&g_31[6],(void*)0,&g_31[6],&l_2664[6],&l_2664[6],&g_31[6]};
    uint64_t l_2770 = 18446744073709551615UL;
    const uint32_t *l_2784 = &g_665;
    const uint32_t **l_2783 = &l_2784;
    uint32_t *l_2785 = (void*)0;
    int8_t *l_2786 = &g_2653;
    int32_t l_2833 = 0xC295EA58L;
    int32_t *l_2866 = &g_439[0];
    float l_2901 = 0xD.6B4B4Cp+28;
    int i;
    if ((safe_rshift_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u((((((safe_sub_func_int32_t_s_s(((((*g_2465) = l_2660) != l_2660) , (p_10 && ((*l_2674) ^= (+(((((((safe_mul_func_int16_t_s_s(l_2664[6], (((*l_2666) = l_2665) != (((safe_add_func_int32_t_s_s(((l_2669 = l_2669) == (*g_1605)), (l_2673 != l_2673))) , p_6) , (void*)0)))) ^ l_2664[5]) , p_6) > 0xC.EED2D1p+31) < (*g_505)) >= p_7) , p_9))))), p_7)) , 253UL) , 0xAC3BF1E5L) < l_2664[4]) < l_2664[3]), 0xB76B29C0L)) < 0xAAL), 7)))
    { /* block id: 1162 */
        uint8_t *****l_2685 = (void*)0;
        int32_t l_2686 = 0xB04419CAL;
        int32_t l_2701 = 0x0F54DF8DL;
        int32_t l_2735 = (-1L);
        int32_t l_2736 = 0xCB74FB2AL;
        int32_t l_2737[6][1] = {{2L},{2L},{(-7L)},{2L},{2L},{(-7L)}};
        int i, j;
        for (g_52 = 0; (g_52 < 32); g_52 = safe_add_func_int32_t_s_s(g_52, 1))
        { /* block id: 1165 */
            int64_t l_2679 = 0xC68255CD4D5ADA8BLL;
            uint8_t *****l_2684 = &g_2338;
            uint64_t *l_2687[9];
            uint32_t l_2713[8] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
            int32_t l_2731 = 0L;
            int32_t l_2732 = 0x63FC5F2FL;
            int32_t l_2733 = 0x77B43028L;
            int32_t l_2734[8][4][2] = {{{0x4DBCA335L,(-4L)},{0x7F28C464L,0x02123E1AL},{0xC6A01FFEL,0xC6A01FFEL},{0x4DBCA335L,0xC6A01FFEL}},{{0xC6A01FFEL,0x02123E1AL},{0x7F28C464L,(-4L)},{0x4DBCA335L,0x7F28C464L},{(-4L),0x02123E1AL}},{{(-4L),0x7F28C464L},{0x5060506FL,0x006CE610L},{0x02123E1AL,0xC01C9519L},{0x4DBCA335L,0x4DBCA335L}},{{0x5060506FL,0x4DBCA335L},{0x4DBCA335L,0xC01C9519L},{0x02123E1AL,0x006CE610L},{0x5060506FL,0x02123E1AL}},{{0x006CE610L,0xC01C9519L},{0x006CE610L,0x02123E1AL},{0x5060506FL,0x006CE610L},{0x02123E1AL,0xC01C9519L}},{{0x4DBCA335L,0x4DBCA335L},{0x5060506FL,0x4DBCA335L},{0x4DBCA335L,0xC01C9519L},{0x02123E1AL,0x006CE610L}},{{0x5060506FL,0x02123E1AL},{0x006CE610L,0xC01C9519L},{0x006CE610L,0x02123E1AL},{0x5060506FL,0x006CE610L}},{{0x02123E1AL,0xC01C9519L},{0x4DBCA335L,0x4DBCA335L},{0x5060506FL,0x4DBCA335L},{0x4DBCA335L,0xC01C9519L}}};
            uint16_t l_2758[3][7] = {{0xAF4FL,0UL,0xAF4FL,0x0319L,0x0319L,0xAF4FL,0UL},{0x0319L,0UL,1UL,1UL,0UL,0x0319L,0UL},{0xAF4FL,0x0319L,0x0319L,0xAF4FL,0UL,0xAF4FL,0x0319L}};
            int64_t l_2759 = (-1L);
            const int32_t **l_2761 = (void*)0;
            const int32_t ***l_2760 = &l_2761;
            int i, j, k;
            for (i = 0; i < 9; i++)
                l_2687[i] = &g_91;
            (*g_100) &= p_7;
            if (((*g_261) ^= (0x93L <= ((safe_rshift_func_int8_t_s_s(p_10, l_2679)) == ((safe_div_func_int8_t_s_s(0x62L, p_10)) >= (g_74 = (((((l_2684 = (l_2685 = l_2684)) == &g_2338) >= ((l_2686 = (-5L)) , p_7)) || 0x4D0BL) == p_8)))))))
            { /* block id: 1172 */
                int32_t l_2706 = 0x009216C6L;
                for (g_91 = 0; (g_91 <= 6); g_91 += 1)
                { /* block id: 1175 */
                    uint32_t l_2700 = 4294967288UL;
                    (*g_261) ^= p_10;
                    if ((((((((((safe_mul_func_int8_t_s_s(((void*)0 == &g_503), (l_2686 || (l_2686 ^= (safe_add_func_int16_t_s_s(((p_10 , (safe_rshift_func_int8_t_s_u((safe_add_func_int8_t_s_s(((****g_2465) ^= ((void*)0 == l_2696)), p_6)), 0))) || ((((*l_2674) = l_2700) , 0x9CL) >= (***g_1294))), (*g_2181))))))) < 0xCEL) && p_8) != p_7) != 0x5B8744D3L) && 0xC7B2E89AL) & p_8) | (-1L)) != l_2701))
                    { /* block id: 1180 */
                        float l_2707 = 0xC.CB2E47p-90;
                        int32_t l_2708 = (-1L);
                        int64_t l_2714 = 0x846FE1F281B2A453LL;
                        l_2664[6] = ((((p_9 != ((safe_add_func_float_f_f(0x1.8p-1, ((0x3.DEB568p+10 != ((safe_mul_func_float_f_f((((((l_2708 = ((0UL ^ p_10) && (l_2706 = p_9))) <= (((safe_div_func_uint16_t_u_u(p_10, 0xC074L)) != ((****g_2465) = (l_2713[7] > p_10))) , p_9)) ^ 18446744073709551614UL) , p_10) , p_8), p_6)) > 0x0.2p+1)) != (***g_1109)))) , l_2714)) <= 1UL) , (void*)0) == l_2715[4]);
                        (*g_100) = (0x835232A8L > (safe_sub_func_uint64_t_u_u(p_7, p_8)));
                        (*g_261) |= (((safe_rshift_func_int16_t_s_s(((((safe_rshift_func_int8_t_s_u((&g_1080 == (void*)0), 1)) != p_10) > p_9) < ((p_10 ^ (l_2700 == ((*l_2699) = (l_2706 < ((safe_div_func_uint32_t_u_u(((p_8 >= p_7) , l_2700), l_2714)) == p_8))))) & l_2701)), 12)) || p_6) >= 0xCBL);
                        if (l_2679)
                            break;
                    }
                    else
                    { /* block id: 1189 */
                        return l_2664[5];
                    }
                    (*g_100) &= 0xF2143F08L;
                }
            }
            else
            { /* block id: 1194 */
                int64_t l_2725[3][5][2];
                int32_t l_2726 = 0L;
                int32_t *l_2727 = &g_439[7];
                int32_t l_2728 = (-1L);
                int32_t *l_2729 = (void*)0;
                int32_t *l_2730[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t l_2738 = 2L;
                uint64_t l_2739 = 0x1EDD34CAE9A5A581LL;
                int i, j, k;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        for (k = 0; k < 2; k++)
                            l_2725[i][j][k] = 0xEBCD65C0A1C734B7LL;
                    }
                }
                (**g_504) = g_2724;
                l_2739--;
                (*l_2727) &= ((safe_rshift_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u(((**g_2180) = (0x0730E376L != ((void*)0 == l_2673))), (((0xC08434A7L > (p_8 , ((*g_1727) = ((g_2746++) ^ (safe_add_func_uint64_t_u_u(p_10, ((*l_2674) = (safe_mul_func_uint16_t_u_u(l_2734[4][2][1], 0x9AA6L))))))))) >= (safe_sub_func_uint8_t_u_u((((~(safe_mod_func_uint32_t_u_u(l_2758[2][4], p_7))) , p_7) <= l_2664[6]), l_2759))) , p_6))) >= p_6), l_2664[6])) < 0UL);
            }
            l_2737[2][0] = (l_2760 != &l_2665);
        }
    }
    else
    { /* block id: 1205 */
lbl_2951:
        for (g_409 = 0; g_409 < 7; g_409 += 1)
        {
            for (g_665 = 0; g_665 < 3; g_665 += 1)
            {
                for (g_1389 = 0; g_1389 < 3; g_1389 += 1)
                {
                    g_1607[g_409][g_665][g_1389] = &g_1608;
                }
            }
        }
    }
    (*g_100) |= ((l_2763 = (g_2762 = &g_140)) != &p_8);
    ++l_2770;
    if ((((safe_add_func_uint32_t_u_u(((safe_sub_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_u((4294967288UL & ((*g_1727) = (*g_1727))), (--(**g_2180)))) , (((((*l_2764) = ((safe_mod_func_int8_t_s_s((p_7 = ((*l_2786) = ((((*g_1727) = (&g_1728 == l_2783)) , ((*l_2764) != ((((*g_1395) = (&l_2670 != (*g_1605))) < (*l_2764)) >= ((*l_2674) |= ((l_2785 = l_2769[2]) == l_2769[4]))))) , 0xCDL))), 255UL)) | 1UL)) , 18446744073709551607UL) < 2UL) , g_1683)) >= p_9), 0xFFL)) , 0UL), 0xF99DD78EL)) > p_10) != p_6))
    { /* block id: 1221 */
        uint64_t l_2792[5][4] = {{0xB51253AE0011B722LL,0x67230C83D3B63744LL,0xB51253AE0011B722LL,0xB51253AE0011B722LL},{0x67230C83D3B63744LL,0x67230C83D3B63744LL,0xF0C2259AF4B20657LL,0x67230C83D3B63744LL},{0x67230C83D3B63744LL,0xB51253AE0011B722LL,0xB51253AE0011B722LL,0x67230C83D3B63744LL},{0xB51253AE0011B722LL,0x67230C83D3B63744LL,0xB51253AE0011B722LL,0xB51253AE0011B722LL},{0x67230C83D3B63744LL,0x67230C83D3B63744LL,0xF0C2259AF4B20657LL,0x67230C83D3B63744LL}};
        int32_t l_2810[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int16_t **l_2817 = (void*)0;
        int32_t l_2818 = (-7L);
        int32_t l_2819 = 0x430ABCFFL;
        int32_t *l_2834 = &g_1683;
        int8_t ****l_2843 = &l_2660;
        int i, j;
        (*g_100) = ((*g_746) = (safe_mul_func_int16_t_s_s(((((!((*g_1394) | ((safe_mod_func_int16_t_s_s(0x365EL, ((*g_2181) = 0x8FD4L))) | l_2792[3][2]))) , ((0x0DL < (!(((p_8 , (p_6 > ((p_9 | ((*l_2763) |= l_2792[2][3])) >= p_6))) ^ p_10) <= 0x70L))) , l_2792[4][2])) || p_6) >= 9L), p_7)));
        l_2819 ^= ((safe_lshift_func_uint16_t_u_s(((((*l_2699) |= (safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(((*g_2181) = 0x3123L), ((*l_2764) = (safe_add_func_uint64_t_u_u((((*l_2674) = ((safe_sub_func_int64_t_s_s(l_2792[1][0], p_6)) , 1L)) , (safe_sub_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u((l_2810[6] = (((--p_6) > ((safe_add_func_int16_t_s_s((*g_2762), (((safe_mod_func_uint32_t_u_u(((*g_1727) = 0x16D778ECL), 0xE1BC07C0L)) , l_2817) == (*l_2697)))) != (*g_100))) <= 0x4B8B38DFL)), l_2818)), 0x8B02CAC6L))), p_10))))), p_9))) > l_2818) == 0x363CE0B0L), p_8)) > 0x5EL);
        if ((((-9L) | (0xE2E6BB46L | 0xEF5D8865L)) ^ (((safe_lshift_func_uint8_t_u_u(((((*l_2834) ^= ((!(((safe_add_func_int8_t_s_s((*g_1394), (((safe_div_func_uint32_t_u_u((safe_add_func_int64_t_s_s(p_8, (((((&l_2817 == ((*l_2696) = (*l_2696))) | (+(safe_add_func_uint64_t_u_u((!p_8), l_2833)))) == (*l_2764)) , 0x8DCD458C813BA26ELL) || (-7L)))), (*g_1727))) , &g_140) == &p_8))) == l_2792[3][2]) > 0UL)) & l_2810[0])) , &g_1264) == (void*)0), 3)) , 0x57L) & p_10)))
        { /* block id: 1236 */
            (**g_504) = (safe_sub_func_float_f_f((l_2819 = (***g_503)), (*g_505)));
        }
        else
        { /* block id: 1239 */
            uint16_t l_2861 = 0UL;
            int32_t l_2862 = (-7L);
            float *l_2863[3][4][6] = {{{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724}},{{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724}},{{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724},{&g_2517,&g_2724,&g_2724,&g_2517,&g_2724,&g_2724}}};
            int i, j, k;
            l_2810[4] = (safe_div_func_float_f_f(((l_2818 = (safe_div_func_float_f_f((safe_div_func_float_f_f((l_2843 != (void*)0), 0x0.5p-1)), (-(l_2862 = (safe_add_func_float_f_f((p_10 >= (p_7 < (safe_mul_func_float_f_f(p_10, ((safe_add_func_float_f_f((safe_mul_func_float_f_f((((safe_mul_func_float_f_f(l_2792[3][2], (*g_1111))) <= ((safe_div_func_float_f_f((((*g_505) = (l_2819 = ((safe_div_func_float_f_f((safe_add_func_float_f_f((**g_504), (**g_504))), (**g_504))) == p_10))) == p_9), 0xD.39C2D0p+80)) != l_2861)) > 0x1.2p+1), 0x8.E7DEECp-11)), p_6)) == 0x5.6A75B5p-27))))), l_2861))))))) > l_2861), l_2792[3][2]));
            return p_8;
        }
    }
    else
    { /* block id: 1247 */
        uint16_t l_2873 = 1UL;
        int64_t **l_2881 = (void*)0;
        const int32_t l_2882 = (-2L);
        int32_t l_2902 = 0x51D7FB2EL;
        float *l_2905 = &g_1964;
        float l_2921 = 0x7.EAA6EEp-11;
        int32_t l_2922 = 0x8FF49971L;
        int32_t l_2924 = 0xE4455B0DL;
        int32_t l_2925 = 1L;
        int64_t l_2950[9];
        int i;
        for (i = 0; i < 9; i++)
            l_2950[i] = 1L;
        for (g_91 = 2; (g_91 == 57); g_91 = safe_add_func_uint8_t_u_u(g_91, 9))
        { /* block id: 1250 */
            int64_t l_2899 = 1L;
            int32_t l_2907[10][2][2] = {{{0x1381959BL,(-7L)},{0x1381959BL,(-1L)}},{{0x85D4C982L,0x1381959BL},{(-1L),(-7L)}},{{6L,6L},{0x85D4C982L,6L}},{{6L,(-7L)},{(-7L),0x45F09888L}},{{0x0C790C2AL,(-7L)},{0x45F09888L,0xD8F5C03BL}},{{0x45F09888L,(-7L)},{0x0C790C2AL,0x45F09888L}},{{(-7L),0xD8F5C03BL},{0x85D4C982L,0x85D4C982L}},{{0x0C790C2AL,0x85D4C982L},{0x85D4C982L,0xD8F5C03BL}},{{(-7L),0x45F09888L},{0x0C790C2AL,(-7L)}},{{0x45F09888L,0xD8F5C03BL},{0x45F09888L,(-7L)}}};
            uint32_t l_2927 = 18446744073709551606UL;
            int i, j, k;
            (*g_100) |= 0xA830EA8CL;
            l_2764 = l_2866;
            if ((safe_add_func_int64_t_s_s((((*l_2763) = ((((((*g_2181) = 0xB9F8L) | ((safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(((l_2873 = p_7) && (((safe_sub_func_int8_t_s_s((safe_unary_minus_func_int8_t_s(((*l_2786) = ((((((safe_add_func_uint64_t_u_u(((((safe_mul_func_uint8_t_u_u(p_8, p_9)) < ((*g_1394) = (p_7 ^ (((void*)0 == l_2881) & p_7)))) , (((*g_2762) == 0x26D0L) , &p_8)) == &p_8), p_8)) <= (*l_2866)) ^ (-7L)) | 0x64L) , g_954) , l_2873)))), 0UL)) , (-4L)) || p_7)), p_9)), 0x4E8AL)) ^ g_2560)) < l_2882) ^ 0xD7L) > g_1556)) || 1UL), (-1L))))
            { /* block id: 1258 */
                float *l_2897 = &g_221;
                int32_t l_2900 = 0x48D1648CL;
                l_2902 = (safe_mul_func_float_f_f((safe_add_func_float_f_f(((**g_504) != (safe_div_func_float_f_f(((((safe_add_func_float_f_f((safe_add_func_float_f_f((0xA.1417B6p+39 > ((safe_div_func_float_f_f((safe_sub_func_float_f_f(((*l_2897) = (p_10 >= (0xE.7DBE5Dp+40 == ((&g_1152 != (void*)0) == (**g_504))))), 0x5.0p+1)), (!p_8))) != p_8)), l_2899)), (-0x1.Ep-1))) , l_2900) <= p_10) > 0xD.3802D5p-69), l_2901))), p_9)), l_2882));
                return (*g_1727);
            }
            else
            { /* block id: 1262 */
                float *l_2906 = &l_2901;
                for (p_7 = 26; (p_7 < (-3)); --p_7)
                { /* block id: 1265 */
                    l_2907[7][0][0] &= (l_2905 != l_2906);
                    return (*g_1727);
                }
                for (g_225 = (-20); (g_225 > 44); g_225 = safe_add_func_uint16_t_u_u(g_225, 6))
                { /* block id: 1271 */
                    for (g_1384 = 0; (g_1384 >= 0); g_1384 -= 1)
                    { /* block id: 1274 */
                        int i, j;
                        l_2866 = l_2906;
                    }
                    for (g_333 = 0; (g_333 > (-16)); g_333 = safe_sub_func_uint16_t_u_u(g_333, 1))
                    { /* block id: 1279 */
                        return p_9;
                    }
                    if ((*g_261))
                        break;
                }
            }
            if (p_6)
            { /* block id: 1285 */
                uint8_t l_2914[1][10] = {{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL}};
                int32_t l_2919 = (-1L);
                int i, j;
                l_2902 = (l_2919 ^= ((safe_sub_func_int8_t_s_s((-5L), (0xA2C4D427L | ((*g_261) |= l_2914[0][9])))) | (safe_rshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_u(((*g_1430) == &g_2180), 2)), 0))));
            }
            else
            { /* block id: 1289 */
                float l_2920 = 0x0.6p+1;
                int32_t l_2923 = (-2L);
                int32_t l_2926 = 0L;
                int8_t ****l_2945 = &g_2466;
                --l_2927;
                for (p_7 = (-27); (p_7 > (-24)); ++p_7)
                { /* block id: 1293 */
                    uint16_t l_2932 = 65533UL;
                    uint32_t l_2949 = 0xBD6ED059L;
                    if (p_6)
                        break;
                    l_2932--;
                    for (l_2925 = 0; (l_2925 <= 24); ++l_2925)
                    { /* block id: 1298 */
                        int16_t l_2948 = 0xE495L;
                        l_2923 |= (safe_mul_func_int16_t_s_s(((safe_add_func_int16_t_s_s(((safe_add_func_float_f_f((-0x7.6p-1), (***g_1109))) , (p_10 < ((safe_rshift_func_int8_t_s_s((0xA49F119E50391338LL > (l_2945 != (void*)0)), ((*g_2762) || ((((((safe_rshift_func_uint16_t_u_u(l_2932, 10)) & l_2948) , p_10) , l_2925) < p_9) > l_2949)))) & l_2948))), l_2950[5])) | 18446744073709551615UL), p_6));
                        if (p_6)
                            continue;
                    }
                    if (l_2770)
                        goto lbl_2951;
                }
                for (g_52 = 0; (g_52 <= 36); g_52++)
                { /* block id: 1306 */
                    int32_t *l_2954 = (void*)0;
                    l_2954 = &l_2664[2];
                    (*g_130) = &l_2907[7][0][0];
                }
            }
        }
    }
    return p_9;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t  func_16(uint32_t  p_17, uint8_t  p_18)
{ /* block id: 1153 */
    int32_t *l_2641 = &g_439[0];
    int32_t *l_2642 = &g_439[0];
    int32_t *l_2643 = &g_31[1];
    int32_t *l_2644 = &g_31[1];
    int32_t *l_2645 = &g_439[0];
    int32_t *l_2646[2][7][1] = {{{&g_439[0]},{&g_439[0]},{&g_224},{&g_439[0]},{&g_439[0]},{&g_224},{&g_439[0]}},{{&g_439[0]},{&g_224},{&g_439[0]},{&g_439[0]},{&g_224},{&g_439[0]},{&g_439[0]}}};
    int16_t l_2647 = (-1L);
    int32_t l_2648 = 0xD6AD2785L;
    uint64_t l_2649 = 3UL;
    int i, j, k;
    l_2649--;
    l_2645 = (void*)0;
    return p_17;
}


/* ------------------------------------------ */
/* 
 * reads : g_52 g_4 g_75 g_962 g_261 g_101 g_31 g_85 g_225 g_224 g_130 g_131 g_153 g_129 g_504 g_505 g_140 g_33 g_333 g_100 g_649 g_650 g_152 g_409 g_665 g_96 g_128 g_347 g_282 g_1102 g_954 g_594 g_439 g_1246 g_1269 g_1109 g_1110 g_1111 g_746 g_1294 g_1307 g_1313 g_569 g_1296 g_1297 g_912 g_1384 g_1389 g_1285 g_1430 g_1395 g_195 g_1394 g_1498 g_62 g_1112 g_1540 g_1556 g_91 g_1089 g_1604 g_1647 g_1295 g_1682 g_1683 g_503 g_2143 g_2144 g_1271 g_1727 g_956 g_2181 g_74 g_2463 g_2464 g_500 g_928 g_2503 g_2180 g_1080 g_1431 g_2466 g_2467
 * writes: g_52 g_62 g_74 g_75 g_31 g_224 g_131 g_261 g_505 g_101 g_594 g_244 g_128 g_665 g_96 g_91 g_225 g_282 g_1263 g_195 g_1112 g_33 g_439 g_569 g_140 g_1384 g_1389 g_1394 g_1395 g_1431 g_1285 g_409 g_153 g_1556 g_85 g_1246 g_1727 g_1728 g_1683 g_956 g_2337 g_2377 g_2542
 */
static uint8_t  func_23(uint32_t  p_24, uint32_t  p_25)
{ /* block id: 3 */
    uint8_t *l_51 = &g_52;
    int64_t * const l_55 = &g_4;
    int64_t *l_57 = &g_33;
    int64_t **l_56 = &l_57;
    int8_t *l_1393[6][2][7] = {{{&g_1285,&g_333,&g_1285,&g_1285,&g_333,&g_1285,&g_1285},{(void*)0,&g_333,(void*)0,&g_333,(void*)0,&g_333,(void*)0}},{{&g_333,&g_1285,&g_1285,&g_333,&g_1285,&g_1285,&g_333},{&g_1285,&g_333,&g_1285,&g_1285,(void*)0,&g_1285,&g_1285}},{{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333},{(void*)0,&g_1285,&g_1285,&g_333,&g_1285,&g_1285,(void*)0}},{{&g_1285,&g_333,&g_1285,&g_1285,&g_333,&g_1285,&g_1285},{(void*)0,&g_333,(void*)0,&g_333,(void*)0,&g_333,(void*)0}},{{&g_333,&g_1285,&g_1285,&g_333,&g_1285,&g_1285,&g_333},{&g_1285,&g_333,&g_1285,&g_1285,(void*)0,&g_1285,&g_1285}},{{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333},{(void*)0,&g_1285,&g_1285,&g_333,&g_1285,&g_1285,(void*)0}}};
    int8_t **l_1392[1];
    int32_t l_1403[6][1];
    int32_t l_2226 = 0L;
    int32_t l_2235 = (-10L);
    int32_t * const *l_2327 = &g_261;
    int32_t * const **l_2326[5][1][5] = {{{(void*)0,&l_2327,(void*)0,(void*)0,&l_2327}},{{(void*)0,&l_2327,&l_2327,&l_2327,&l_2327}},{{&l_2327,&l_2327,&l_2327,&l_2327,&l_2327}},{{&l_2327,&l_2327,&l_2327,&l_2327,(void*)0}},{{&l_2327,(void*)0,(void*)0,&l_2327,(void*)0}}};
    uint16_t l_2328 = 65535UL;
    uint64_t l_2344[7] = {0x69BAE02229A4CA65LL,0x69BAE02229A4CA65LL,0x69BAE02229A4CA65LL,0x69BAE02229A4CA65LL,0x69BAE02229A4CA65LL,0x69BAE02229A4CA65LL,0x69BAE02229A4CA65LL};
    int32_t l_2360[7];
    uint32_t l_2369 = 0xDE45221BL;
    int8_t * const *l_2392 = (void*)0;
    int8_t * const **l_2391[2];
    int8_t * const *** const l_2390 = &l_2391[1];
    int8_t ****l_2393 = (void*)0;
    uint16_t l_2411 = 65528UL;
    const uint16_t l_2432 = 1UL;
    int32_t l_2433 = (-1L);
    int32_t l_2434 = 0L;
    int32_t l_2481 = 5L;
    int32_t **l_2488[4] = {&g_2064,&g_2064,&g_2064,&g_2064};
    int64_t ***l_2582[7];
    int64_t *** const *l_2581 = &l_2582[2];
    int64_t *** const ** const l_2580[1] = {&l_2581};
    int64_t l_2602 = 0L;
    int32_t *l_2603 = &g_224;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1392[i] = &l_1393[0][0][6];
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
            l_1403[i][j] = 0xEEAE9D68L;
    }
    for (i = 0; i < 7; i++)
        l_2360[i] = 0xBA99FE1BL;
    for (i = 0; i < 2; i++)
        l_2391[i] = &l_2392;
    for (i = 0; i < 7; i++)
        l_2582[i] = &g_397;
    if (((((safe_div_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((&g_31[6] == (void*)0), func_43(func_48(((*l_51)++), ((*l_56) = l_55)), ((g_1394 = l_51) != (g_1395 = &g_1285)), (((safe_div_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u((((safe_div_func_uint64_t_u_u(g_1285, (~(((((((g_1307 , (-1L)) <= 7L) | g_85[5]) & l_1403[3][0]) , 0xD59085BA6E238C35LL) , l_1403[0][0]) , 1L)))) , l_1403[3][0]) && 0UL), l_1403[3][0])), l_1403[3][0])) , l_1403[2][0]) | 0UL), p_24))), l_1403[3][0])) , (*g_100)) != 1UL) || 4294967294UL))
    { /* block id: 956 */
        int64_t l_2225 = (-1L);
        int32_t l_2236[6][2] = {{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L}};
        uint8_t *l_2237 = &g_85[6];
        int16_t *l_2254 = &g_62;
        int32_t *l_2256[2][8] = {{&g_31[1],&l_1403[3][0],&g_31[1],&l_1403[3][0],&g_31[1],&l_1403[3][0],&g_31[1],&l_1403[3][0]},{&g_31[1],&l_1403[3][0],&g_31[1],&l_1403[3][0],&g_31[1],&l_1403[3][0],&g_31[1],&l_1403[3][0]}};
        int8_t *** const l_2297[2][6] = {{&l_1392[0],&l_1392[0],&l_1392[0],&l_1392[0],&l_1392[0],&l_1392[0]},{&l_1392[0],&l_1392[0],&l_1392[0],&l_1392[0],&l_1392[0],&l_1392[0]}};
        int8_t *** const *l_2296 = &l_2297[0][3];
        uint32_t l_2298 = 0xA6D72611L;
        uint16_t ** const *l_2299[3][5][5] = {{{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0}},{{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0}},{{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0},{&g_2180,(void*)0,&g_2180,(void*)0,(void*)0}}};
        uint64_t *l_2341[3];
        const uint8_t *** const *l_2415 = (void*)0;
        const uint8_t *** const **l_2414[5][8] = {{&l_2415,&l_2415,(void*)0,&l_2415,(void*)0,&l_2415,&l_2415,&l_2415},{&l_2415,&l_2415,(void*)0,(void*)0,&l_2415,&l_2415,&l_2415,&l_2415},{&l_2415,&l_2415,&l_2415,&l_2415,&l_2415,(void*)0,(void*)0,&l_2415},{&l_2415,&l_2415,&l_2415,&l_2415,(void*)0,&l_2415,(void*)0,&l_2415},{&l_2415,(void*)0,&l_2415,(void*)0,&l_2415,&l_2415,(void*)0,&l_2415}};
        int8_t l_2457 = 1L;
        int8_t ***l_2470 = &g_2467[0];
        int8_t **** const l_2469[1] = {&l_2470};
        int8_t **** const *l_2468 = &l_2469[0];
        int32_t l_2555 = (-1L);
        int8_t l_2566[4][9] = {{(-1L),1L,0xE7L,1L,0xE7L,1L,(-1L),0L,1L},{0L,0L,(-1L),1L,0L,(-7L),0L,0xE7L,0L},{0L,7L,(-1L),(-1L),7L,0L,1L,0L,(-1L)},{0xE7L,3L,(-1L),(-7L),0L,4L,0L,0L,4L}};
        int64_t ***l_2579 = &g_397;
        uint64_t l_2611 = 1UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_2341[i] = &g_91;
lbl_2220:
        for (g_225 = 0; (g_225 <= 8); g_225 += 1)
        { /* block id: 959 */
            int32_t *l_2219 = &g_224;
            (**g_2143) = l_2219;
            if (g_569)
                goto lbl_2220;
        }
        if ((safe_lshift_func_uint8_t_u_s((247UL >= ((safe_add_func_int64_t_s_s((l_1403[3][0] = l_2225), (l_2226 == (((p_24 == (((((((p_24 , (safe_sub_func_uint64_t_u_u(g_1271[2][1][0], ((safe_lshift_func_uint16_t_u_s(((++(*l_51)) <= ((*g_1395) |= (safe_add_func_uint8_t_u_u(((l_2225 , l_2225) , (++(*l_2237))), ((((~(safe_mul_func_uint8_t_u_u(((0L || p_25) , p_24), p_24))) , l_2235) >= l_2236[3][1]) != l_2235))))), l_2235)) , (-1L))))) ^ (-1L)) || p_25) , l_2235) | p_24) <= 0xB986BAFEEF07D7DELL) && l_2236[2][1])) , p_25) , l_2236[0][1])))) != l_2236[3][0])), l_2226)))
        { /* block id: 967 */
            int64_t l_2245 = 0x3427FCA12A20520BLL;
            int16_t *l_2253[5][1][10] = {{{&g_140,&g_140,&g_62,&g_140,&g_140,(void*)0,&g_62,(void*)0,&g_75,&g_140}},{{&g_62,&g_75,&g_140,(void*)0,(void*)0,&g_75,&g_75,(void*)0,(void*)0,&g_75}},{{&g_62,&g_62,&g_75,&g_75,&g_140,&g_75,&g_62,(void*)0,(void*)0,&g_140}},{{(void*)0,&g_140,&g_62,&g_140,(void*)0,&g_140,&g_62,&g_140,(void*)0,&g_62}},{{&g_140,&g_62,(void*)0,&g_140,&g_140,&g_140,(void*)0,(void*)0,(void*)0,(void*)0}}};
            int32_t l_2302 = 0x288E897CL;
            int32_t l_2305[6];
            int8_t l_2359 = 0L;
            int8_t **l_2410[8] = {&g_1395,&g_1394,&g_1395,&g_1394,&g_1395,&g_1394,&g_1395,&g_1394};
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_2305[i] = 0x0205045BL;
            (*g_261) ^= (safe_add_func_uint32_t_u_u((l_2245 >= l_1403[3][0]), ((**g_1295) != (~l_2236[0][1]))));
            if ((*g_261))
            { /* block id: 969 */
                int16_t *l_2251 = &g_75;
                int16_t **l_2252[6][8][5] = {{{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,(void*)0},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,(void*)0,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,(void*)0,&l_2251,&l_2251,(void*)0}},{{&l_2251,&l_2251,(void*)0,(void*)0,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,(void*)0},{&l_2251,&l_2251,(void*)0,&l_2251,(void*)0},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,(void*)0,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,(void*)0},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251}},{{(void*)0,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,(void*)0,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,(void*)0,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,&l_2251,(void*)0}},{{(void*)0,&l_2251,(void*)0,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,(void*)0,&l_2251,(void*)0},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,(void*)0},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251}},{{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,(void*)0,&l_2251},{&l_2251,&l_2251,&l_2251,(void*)0,&l_2251},{(void*)0,&l_2251,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,(void*)0,(void*)0,&l_2251},{&l_2251,&l_2251,(void*)0,&l_2251,(void*)0},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,(void*)0,&l_2251}},{{(void*)0,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,(void*)0,&l_2251},{(void*)0,(void*)0,&l_2251,&l_2251,&l_2251},{&l_2251,&l_2251,&l_2251,(void*)0,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,&l_2251},{(void*)0,&l_2251,(void*)0,&l_2251,&l_2251},{(void*)0,&l_2251,&l_2251,(void*)0,&l_2251},{&l_2251,&l_2251,&l_2251,&l_2251,(void*)0}}};
                int32_t *l_2255 = &l_1403[3][0];
                int32_t l_2301[4][4][8] = {{{0xA0651C12L,1L,1L,(-1L),(-8L),(-8L),(-1L),1L},{8L,8L,(-9L),(-6L),(-8L),(-1L),0xA8E0DCA8L,8L},{1L,0xA0651C12L,0x1D7E26FBL,(-8L),0xA8E0DCA8L,0x1D7E26FBL,1L,8L},{0xA0651C12L,0x94E153E5L,1L,(-6L),1L,0x94E153E5L,0xA0651C12L,1L}},{{0L,(-6L),1L,(-1L),(-6L),0L,0xA8E0DCA8L,0L},{0x7737D7E0L,0L,(-8L),0x94E153E5L,(-6L),0x1D7E26FBL,0x1D7E26FBL,(-6L)},{0L,1L,1L,0L,1L,0x7737D7E0L,8L,0x94E153E5L},{0xA0651C12L,8L,7L,(-1L),0xA8E0DCA8L,(-9L),(-1L),0xA0651C12L}},{{1L,8L,(-8L),0x7737D7E0L,(-8L),0x7737D7E0L,(-8L),8L},{8L,1L,0x1D7E26FBL,0xA8E0DCA8L,(-8L),0x1D7E26FBL,0xA0651C12L,1L},{0xA0651C12L,0L,(-5L),(-6L),8L,0L,0xA0651C12L,0xA0651C12L},{0x94E153E5L,(-6L),0x1D7E26FBL,0x1D7E26FBL,(-6L),0x94E153E5L,(-8L),0L}},{{(-6L),0x94E153E5L,(-8L),0L,0x7737D7E0L,0x1D7E26FBL,(-8L),1L},{1L,0x7737D7E0L,(-6L),1L,0x3C913A0FL,(-1L),0x3C913A0FL,1L},{(-1L),0x3C913A0FL,(-1L),(-9L),1L,(-5L),(-9L),0x7737D7E0L},{0x3C913A0FL,(-1L),(-5L),0x1D7E26FBL,0L,1L,1L,(-1L)}}};
                int i, j, k;
                for (g_1683 = 0; (g_1683 == (-21)); g_1683 = safe_sub_func_uint64_t_u_u(g_1683, 1))
                { /* block id: 972 */
                    (*g_261) = 0L;
                }
                if (((p_24 || ((p_25 != (safe_rshift_func_int8_t_s_u(((l_2253[4][0][0] = l_2251) == l_2254), 5))) & (&l_2236[0][1] != (l_2256[0][7] = l_2255)))) | (((*g_1727)++) , (!((safe_rshift_func_int16_t_s_u(((((*l_2255) = (safe_mul_func_int8_t_s_s((((p_24 < (safe_sub_func_uint16_t_u_u(((safe_unary_minus_func_uint16_t_u(p_24)) <= (((safe_lshift_func_uint8_t_u_u((((safe_rshift_func_uint8_t_u_s(2UL, l_2245)) & p_24) != 1L), l_2245)) || p_24) <= 0x1E58C9CFEC5C8566LL)), p_24))) >= l_2245) < 18446744073709551613UL), l_2236[0][1]))) & 0L) & g_1556), 8)) == 2L)))))
                { /* block id: 979 */
                    int32_t *l_2273 = &l_2235;
                    int32_t l_2304[8][3][7] = {{{0xEA7FA23FL,(-3L),1L,(-8L),(-6L),0x198C0267L,0L},{(-9L),4L,0xFA4632EBL,(-10L),(-10L),0xFA4632EBL,4L},{0x41C4F2F2L,0L,0xC30F491AL,0x198C0267L,0xE7A07627L,(-1L),0x972E32B3L}},{{0xF63FB55FL,0x618B27E2L,0xB3A81646L,0x424A48ACL,1L,(-4L),1L},{0xC30F491AL,6L,0x972E32B3L,0x198C0267L,1L,1L,0x198C0267L},{(-1L),1L,(-1L),(-10L),0xB3A81646L,(-9L),(-7L)}},{{4L,1L,0x0DC8185DL,(-8L),0x65283056L,0L,6L},{1L,0x4AC9DF6BL,(-4L),0x618B27E2L,0xED35570AL,(-9L),(-9L)},{(-1L),0x51435DECL,(-8L),0x51435DECL,(-1L),1L,(-6L)}},{{(-8L),0x9BAEA53CL,0x424A48ACL,0xFA4632EBL,(-9L),(-4L),6L},{6L,1L,4L,(-1L),0L,(-1L),0x41C4F2F2L},{(-8L),0xFA4632EBL,0xD5C60EA7L,(-7L),0xD5C60EA7L,0xFA4632EBL,(-8L)}},{{(-1L),0xC30F491AL,0xEA7FA23FL,(-6L),0x51435DECL,0x198C0267L,(-8L)},{1L,0xB3A81646L,1L,4L,(-7L),6L,0x9BAEA53CL},{4L,0x972E32B3L,0xEA7FA23FL,0L,0xC30F491AL,0L,0xEA7FA23FL}},{{(-1L),(-1L),0xD5C60EA7L,(-1L),0x4AC9DF6BL,0x424A48ACL,1L},{0xC30F491AL,0x0DC8185DL,4L,0xEA7FA23FL,(-8L),0x972E32B3L,1L},{0xF63FB55FL,(-4L),0x424A48ACL,(-9L),0x4AC9DF6BL,1L,0x4AC9DF6BL}},{{0x41C4F2F2L,(-8L),(-8L),0x41C4F2F2L,0xC30F491AL,0x65283056L,(-3L)},{(-9L),0x424A48ACL,(-4L),0xF63FB55FL,(-7L),0x9BAEA53CL,(-1L)},{0xEA7FA23FL,4L,0x0DC8185DL,0xC30F491AL,0x51435DECL,6L,(-3L)}},{{(-1L),0xD5C60EA7L,(-1L),(-1L),0xD5C60EA7L,(-1L),0x4AC9DF6BL},{0L,0xEA7FA23FL,0x972E32B3L,4L,0L,6L,1L},{4L,1L,0xB3A81646L,1L,4L,(-4L),(-7L)}}};
                    uint32_t l_2307 = 4294967295UL;
                    int32_t **l_2317 = &g_2064;
                    int32_t ***l_2318 = (void*)0;
                    int32_t ***l_2319 = &l_2317;
                    int i, j, k;
                    for (g_409 = 23; (g_409 <= 7); g_409 = safe_sub_func_uint8_t_u_u(g_409, 4))
                    { /* block id: 982 */
                        uint64_t *l_2285[1][10] = {{&g_91,&g_91,&g_96,&g_91,&g_91,&g_96,&g_91,&g_91,&g_96,&g_91}};
                        const int32_t l_2300[1] = {9L};
                        int32_t l_2303[10] = {0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL,0x6B22AE8DL};
                        int32_t l_2306 = 0x787AE50CL;
                        int i, j;
                        (*g_261) = (-2L);
                        (**g_2143) = l_2273;
                        (*g_261) = (((+(safe_mod_func_uint32_t_u_u(((*g_1727) ^= (safe_mod_func_int64_t_s_s((safe_rshift_func_uint16_t_u_s(0UL, (safe_add_func_uint32_t_u_u(((*g_1395) == 1UL), (***g_2143))))), (safe_div_func_int32_t_s_s(((p_25 < (*g_746)) <= ((((((g_91++) | (~((*g_1394) = ((safe_unary_minus_func_int16_t_s((((safe_add_func_int64_t_s_s((safe_rshift_func_int16_t_s_u((safe_sub_func_uint8_t_u_u(((((*l_2273) | (l_2296 != (void*)0)) , 0xABL) | l_2298), 7UL)), 15)), 0UL)) , (void*)0) == l_2299[0][1][0]))) < 2UL)))) != l_2300[0]) , 0x7424286DL) , p_25) >= (*g_2181))), (*l_2255)))))), 0x92FD3DBEL))) <= 0xFDD2L) & g_62);
                        l_2307--;
                    }
                    (*g_100) |= ((*g_1727) != (!(p_25 != ((safe_mod_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_s(((&g_153 == (void*)0) <= p_25), 4)) ^ ((safe_rshift_func_uint16_t_u_s((((*l_2319) = l_2317) == ((safe_lshift_func_int8_t_s_s((((*g_2181) = (safe_rshift_func_uint16_t_u_s(((safe_sub_func_int64_t_s_s((p_25 == ((void*)0 == l_2326[4][0][0])), (*l_2273))) < l_2328), 10))) == (-8L)), 6)) , (void*)0)), 9)) , (**l_2327))) == (*l_2273)), (*l_2273))) || p_25))));
                }
                else
                { /* block id: 994 */
                    uint8_t ****l_2336 = &g_649;
                    uint8_t *****l_2335 = &l_2336;
                    const int32_t l_2342 = 0x0B027C4EL;
                    uint32_t l_2343 = 0xE2DBA0A4L;
                    int32_t l_2361 = (-1L);
                    if (((void*)0 != &g_153))
                    { /* block id: 995 */
                        (**g_1110) = (safe_sub_func_float_f_f(((safe_mul_func_float_f_f(p_24, 0x1.8p+1)) <= (((*l_2237) = (safe_div_func_uint8_t_u_u(l_2305[1], (((((((0UL ^ ((*l_2251) = p_25)) , (g_2337 = l_2335)) == (void*)0) && (((safe_rshift_func_int16_t_s_u(((void*)0 != l_2341[1]), 13)) , (***g_1294)) | l_2342)) || p_24) , 1L) | l_2343)))) , (*l_2255))), p_24));
                        l_2344[1]++;
                    }
                    else
                    { /* block id: 1001 */
                        int8_t **l_2362 = (void*)0;
                        (*g_261) ^= (safe_add_func_uint32_t_u_u(((safe_div_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u((safe_div_func_int64_t_s_s((((safe_sub_func_int32_t_s_s((safe_div_func_uint32_t_u_u((*l_2255), (l_2359 && ((p_24 , ((p_25 , l_2360[1]) , (**g_1294))) != (((l_2361 = 18446744073709551615UL) , (0x0F57L == (l_2362 == l_2362))) , (void*)0))))), p_25)) ^ 4294967290UL) > l_2305[0]), (*l_2255))), p_25)), p_25)) < 0xF27C8FEDL), 4294967291UL));
                    }
                    (*l_2255) &= (*g_746);
                }
                (*g_100) = 1L;
                for (g_74 = 11; (g_74 == 59); ++g_74)
                { /* block id: 1010 */
                    int8_t l_2368 = 0x2FL;
                    uint32_t l_2372 = 4294967287UL;
                    for (g_195 = 7; (g_195 > 8); g_195 = safe_add_func_uint64_t_u_u(g_195, 1))
                    { /* block id: 1013 */
                        int32_t l_2367[8][7][1] = {{{0x18F2CCCCL},{7L},{0x000D4E79L},{(-1L)},{(-6L)},{(-1L)},{(-6L)}},{{(-1L)},{0x000D4E79L},{7L},{0x18F2CCCCL},{7L},{0x000D4E79L},{(-1L)}},{{(-6L)},{(-1L)},{(-6L)},{(-1L)},{0x000D4E79L},{7L},{0x18F2CCCCL}},{{7L},{0x000D4E79L},{(-1L)},{(-6L)},{(-1L)},{(-6L)},{(-1L)}},{{0x000D4E79L},{7L},{0x18F2CCCCL},{7L},{0x000D4E79L},{(-1L)},{(-6L)}},{{(-1L)},{(-6L)},{(-1L)},{0x000D4E79L},{7L},{0x18F2CCCCL},{7L}},{{0x000D4E79L},{(-1L)},{(-6L)},{(-1L)},{(-6L)},{(-1L)},{0x000D4E79L}},{{7L},{0x18F2CCCCL},{7L},{0x000D4E79L},{(-1L)},{(-6L)},{(-1L)}}};
                        int i, j, k;
                        ++l_2369;
                    }
                    return l_2372;
                }
            }
            else
            { /* block id: 1018 */
                int8_t *****l_2394 = &l_2393;
                const int32_t l_2409 = 0xBC34589AL;
                for (g_195 = (-8); (g_195 >= 38); ++g_195)
                { /* block id: 1021 */
                    for (l_2359 = 0; (l_2359 >= (-24)); --l_2359)
                    { /* block id: 1024 */
                        (*g_130) = (void*)0;
                    }
                    (*g_100) = 0xF50E4B46L;
                    g_2377[1][1][0] = &l_2236[0][1];
                    return l_2302;
                }
                l_2411 ^= (safe_rshift_func_int16_t_s_s(((p_24 > (0x237E2CA82E93B6ABLL <= (safe_lshift_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s(((p_24 != (safe_lshift_func_int8_t_s_u(((safe_div_func_int32_t_s_s((safe_mod_func_int32_t_s_s(((l_2390 != ((*l_2394) = l_2393)) >= ((((((p_24 , ((((((safe_add_func_int64_t_s_s((safe_mod_func_int16_t_s_s(p_24, (safe_add_func_int8_t_s_s((safe_div_func_uint64_t_u_u(((*g_1727) && ((*g_1727) = (safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_u(1UL, 1)) < l_2409) , l_2409), p_25)), l_2409)))), p_25)), 0UL)))), g_31[1])) | p_25) , l_2245) , 0x8EL) && 0xD0L) != l_2409)) , (void*)0) != l_2410[0]) , (void*)0) == (void*)0) <= 0x65L)), p_25)), p_25)) != (*g_746)), 4))) < 0x0B38F1CEL), 1)), p_24)))) , 0x71A3L), p_25));
            }
        }
        else
        { /* block id: 1035 */
            const uint8_t *** const **l_2416 = &l_2415;
            int32_t l_2417[8] = {0xDB6AB314L,0xDB6AB314L,0xDB6AB314L,0xDB6AB314L,0xDB6AB314L,0xDB6AB314L,0xDB6AB314L,0xDB6AB314L};
            uint32_t *l_2426 = &g_665;
            int64_t l_2435 = 0x45870CA50A4B5A34LL;
            uint16_t * const *l_2454 = &g_2181;
            float **l_2473 = &g_505;
            const float l_2493 = 0x7.726CC1p+83;
            int64_t *****l_2578 = (void*)0;
            int i;
lbl_2494:
            for (p_24 = 0; (p_24 > 22); p_24 = safe_add_func_int32_t_s_s(p_24, 1))
            { /* block id: 1038 */
                uint32_t l_2438[7] = {0xF80B5AD6L,0xF80B5AD6L,0xF80B5AD6L,0xF80B5AD6L,0xF80B5AD6L,0xF80B5AD6L,0xF80B5AD6L};
                int32_t l_2455 = 8L;
                float ***l_2456 = &g_504;
                int i;
                l_2416 = l_2414[1][4];
                if ((**l_2327))
                    continue;
                if (l_2417[6])
                    break;
                if (((safe_mod_func_int64_t_s_s((safe_add_func_int8_t_s_s(((*g_1394) = ((*g_1395) |= (safe_div_func_int32_t_s_s((l_2434 &= (6UL && ((safe_rshift_func_uint8_t_u_s((&g_1089[8] == l_2426), ((safe_add_func_int8_t_s_s(((((void*)0 == &p_24) == (((0xBD3F66FFE49794E5LL | ((*g_261) <= ((((*g_2181) && (safe_mod_func_int16_t_s_s((+l_2432), l_2433))) & p_24) == p_24))) , (void*)0) != (void*)0)) >= 4294967292UL), (***g_1294))) > (-4L)))) <= (***g_2143)))), l_2435)))), l_2417[5])), 0xF5299BB118774359LL)) == p_24))
                { /* block id: 1045 */
                    int16_t l_2458 = (-6L);
                    for (g_225 = (-12); (g_225 == 30); g_225 = safe_add_func_uint8_t_u_u(g_225, 8))
                    { /* block id: 1048 */
                        l_2438[4]++;
                        if (p_24)
                            break;
                    }
                    l_2417[6] = ((((((((*l_51) = (((*l_2254) = (safe_rshift_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(l_2417[6], 1)), ((***g_1294) != ((*g_1395) = ((safe_sub_func_int16_t_s_s((safe_mod_func_uint32_t_u_u(((safe_div_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(((((l_2438[1] | l_2438[4]) == (!(0x5C06L >= (l_2455 = ((*g_2181) = (l_2454 != (void*)0)))))) <= 0x3C27L) & (l_2456 != (void*)0)), p_25)), 0x686351EF715B9461LL)) , l_2457), 0xB6D27D1EL)), 0x93A1L)) & p_24)))))) >= 3UL)) && 0x1FL) >= p_24) , l_2435) & p_24) <= l_2417[6]) == l_2458);
                    (*g_100) |= (***g_2143);
                    if (p_25)
                        break;
                }
                else
                { /* block id: 1060 */
                    if (l_2438[0])
                        break;
                }
            }
            if ((((safe_rshift_func_uint16_t_u_s(((safe_mul_func_int16_t_s_s((g_2463 != (l_2468 = g_2464[1][1][0])), l_2417[6])) && (g_500 >= ((((safe_mul_func_int16_t_s_s(((l_2435 , (*g_928)) != l_2473), (0x5B4EFAD5L & ((safe_sub_func_int32_t_s_s((+(safe_add_func_int16_t_s_s(((*l_2254) |= 3L), 65529UL))), (-1L))) <= l_2417[6])))) , (void*)0) != (void*)0) < (*g_1727)))), 10)) || 0x55EA1934B4E14D0DLL) <= 252UL))
            { /* block id: 1066 */
                float l_2483 = 0x5.2DC146p+49;
                uint8_t *****l_2489 = &g_2338;
                int32_t l_2492[4][9][7] = {{{0xCA9B584FL,0xFB26F9E2L,8L,0xE45E2122L,(-1L),1L,0x1DAD36DDL},{1L,(-9L),(-1L),0x9B9AA81EL,0x92AB72F0L,1L,5L},{(-9L),0L,8L,5L,0x95BD7499L,(-9L),9L},{0x154AAB11L,0x79352E0FL,0xBF27C866L,0xED2B4DC3L,0L,(-9L),(-2L)},{1L,0x95BD7499L,(-1L),0x3E690648L,(-7L),1L,1L},{(-1L),0xBF27C866L,0x49CAA41AL,0xBF27C866L,(-1L),1L,0x79352E0FL},{9L,(-2L),0xCA9B584FL,0x6E4EEA45L,1L,9L,1L},{0x9B9AA81EL,0x3E690648L,0x5175A4C1L,(-1L),(-9L),0x1DAD36DDL,5L},{9L,0x6E4EEA45L,1L,1L,0xED2B4DC3L,(-2L),0xA37D6F5CL}},{{(-1L),(-1L),(-7L),(-6L),9L,0xD4C6E647L,(-7L)},{1L,3L,9L,0xD4C6E647L,0x6E4EEA45L,(-9L),(-1L)},{0x154AAB11L,(-7L),9L,9L,0xA37D6F5CL,0x5175A4C1L,(-6L)},{(-9L),0L,(-7L),0x49CAA41AL,8L,0x49CAA41AL,(-7L)},{1L,1L,1L,8L,0L,0xEBA01D3DL,5L},{8L,6L,(-9L),3L,(-1L),0xBF27C866L,0xE45E2122L},{8L,0x5175A4C1L,8L,1L,0x95BD7499L,6L,0L},{0xED2B4DC3L,1L,(-6L),(-1L),(-1L),9L,1L},{(-7L),(-1L),0L,0x92AB72F0L,(-9L),(-1L),0xA37D6F5CL}},{{(-2L),5L,0x154AAB11L,(-7L),9L,9L,0xA37D6F5CL},{(-9L),9L,0xBF27C866L,1L,0xA37D6F5CL,(-1L),1L},{0x42DA0BF8L,0xCA9B584FL,0xE45E2122L,0xE45E2122L,0xCA9B584FL,0x42DA0BF8L,0L},{0x3E690648L,3L,(-1L),1L,1L,0L,0xE45E2122L},{0xE45E2122L,0x6E4EEA45L,0x92AB72F0L,0xBF27C866L,1L,0xFB26F9E2L,5L},{1L,3L,0xEBA01D3DL,0L,0x5175A4C1L,0L,0xFB26F9E2L},{5L,0xCA9B584FL,1L,(-9L),(-1L),0L,1L},{(-6L),9L,0xD4C6E647L,(-7L),1L,1L,0x5175A4C1L},{1L,5L,0x5175A4C1L,(-7L),0x79352E0FL,1L,(-1L)}},{{(-1L),(-1L),(-7L),(-9L),(-7L),(-7L),(-9L)},{9L,1L,9L,0L,0xE45E2122L,(-7L),0x1DAD36DDL},{0L,0x5175A4C1L,0L,0xBF27C866L,0L,3L,0xD4C6E647L},{1L,6L,(-9L),1L,(-1L),(-7L),1L},{0L,(-7L),1L,0xE45E2122L,0L,(-7L),5L},{0L,0x92AB72F0L,0x79352E0FL,1L,3L,1L,1L},{1L,(-1L),0x1DAD36DDL,(-7L),6L,1L,(-1L)},{1L,0L,1L,0x92AB72F0L,0x1DAD36DDL,0L,0x6E4EEA45L},{0L,0L,0L,(-1L),0L,0L,0L}}};
                float ***l_2515 = &l_2473;
                int i, j, k;
                l_2483 = (safe_sub_func_float_f_f(l_2481, (!(**g_504))));
                for (l_2411 = 0; (l_2411 != 50); ++l_2411)
                { /* block id: 1070 */
                    int8_t ***l_2506 = &g_2467[0];
                    int32_t l_2512 = (-6L);
                    if (((((safe_add_func_int32_t_s_s(l_2435, (0xB43EL <= 65529UL))) , l_2488[1]) != (void*)0) && ((void*)0 == l_2489)))
                    { /* block id: 1071 */
                        (*g_505) = ((*g_1111) < (safe_add_func_float_f_f((**g_504), ((l_2492[0][1][6] = (*g_1727)) , (-0x2.1p+1)))));
                        if (l_2492[0][4][5])
                            continue;
                    }
                    else
                    { /* block id: 1075 */
                        int8_t ****l_2502 = &l_2470;
                        int32_t l_2505 = 0x248B4876L;
                        int32_t l_2509 = 0x4838739BL;
                        int32_t l_2511[4][6] = {{0xF3850F38L,0xF3850F38L,5L,(-1L),0xF549EFD0L,0L},{0xEC5AFB68L,5L,(-1L),0L,(-1L),5L},{(-1L),0xEC5AFB68L,(-1L),0L,0xF3850F38L,0L},{0xEBA428A3L,0L,5L,5L,0L,0xEBA428A3L}};
                        int i, j;
                        if (g_665)
                            goto lbl_2494;
                        (*g_130) = &l_2236[0][1];
                        l_2511[3][4] &= (((safe_rshift_func_uint16_t_u_u(p_25, 12)) <= 0L) < (~(((safe_lshift_func_uint16_t_u_u(((l_2502 != g_2503) == (l_2505 &= (*g_1727))), 8)) != (l_2506 != (*l_2390))) > (safe_add_func_uint16_t_u_u(l_2509, (((**g_504) = ((((~p_24) , &g_2063) != &g_2063) >= p_24)) , p_25))))));
                        (*g_130) = &l_2417[6];
                    }
                    return l_2512;
                }
                for (g_225 = 0; (g_225 >= 54); ++g_225)
                { /* block id: 1087 */
                    int32_t l_2520 = 0L;
                    for (g_665 = 0; (g_665 <= 0); g_665 += 1)
                    { /* block id: 1090 */
                        float ****l_2516 = &l_2515;
                        (*l_2516) = l_2515;
                        if (p_24)
                            continue;
                    }
                    if (l_2417[6])
                        break;
                    (*g_100) |= 0L;
                    for (g_1683 = (-24); (g_1683 == (-1)); g_1683 = safe_add_func_uint32_t_u_u(g_1683, 3))
                    { /* block id: 1098 */
                        if (l_2520)
                            break;
                    }
                }
                (*g_505) = ((***g_503) == p_24);
            }
            else
            { /* block id: 1103 */
                int32_t l_2526 = (-7L);
                uint16_t ***l_2533 = &g_2180;
                int8_t l_2547[10] = {9L,9L,9L,9L,9L,9L,9L,9L,9L,9L};
                int64_t *l_2554 = &l_2435;
                int32_t l_2564[9][3][7] = {{{0xCC1E2D72L,0x16C57B5CL,0xFB94D7B6L,0x16C57B5CL,0xCC1E2D72L,0x669B678CL,0xDDA87D9EL},{0x8C40D4FEL,0x171566F9L,0L,0xD4469016L,0L,0xC85DF84BL,0xFB94D7B6L},{0L,0xD842213CL,1L,0x2A1F5AE9L,(-6L),0x171566F9L,0xE7C74476L}},{{0x8C40D4FEL,0xD4469016L,3L,0xEF035144L,0L,1L,1L},{0xCC1E2D72L,0x14DFAFC3L,1L,1L,1L,1L,0x14DFAFC3L},{0L,(-8L),1L,1L,0x46A04E36L,0x171566F9L,1L}},{{(-8L),1L,0xEF035144L,0L,0x16C57B5CL,0xC85DF84BL,0xCC1E2D72L},{0x2A1F5AE9L,1L,0x8C40D4FEL,1L,0xC85DF84BL,0x669B678CL,0x46A04E36L},{0x6CBF27FBL,0xCC1E2D72L,0x15E02292L,1L,0xBC0E26F0L,(-8L),0xBC0E26F0L}},{{0xEF035144L,0xCC1E2D72L,0xCC1E2D72L,0xEF035144L,0xD842213CL,0xDDA87D9EL,(-6L)},{0xC85DF84BL,1L,0x14DFAFC3L,0x2A1F5AE9L,0L,0xFB94D7B6L,3L},{3L,1L,0xD842213CL,0xD4469016L,0x6CBF27FBL,0xE7C74476L,(-6L)}},{{0xFB94D7B6L,(-8L),0x669B678CL,0x16C57B5CL,0xD4469016L,1L,0xBC0E26F0L},{0x46A04E36L,0x14DFAFC3L,0xD4469016L,1L,0xD4469016L,0x14DFAFC3L,0x46A04E36L},{(-6L),0xD4469016L,0L,0L,0x6CBF27FBL,1L,0xCC1E2D72L}},{{0x16C57B5CL,0xD842213CL,0x7EEBF2CDL,0x46A04E36L,0L,0xCC1E2D72L,1L},{1L,0x171566F9L,0L,(-8L),0xD842213CL,0x46A04E36L,0x14DFAFC3L},{0x669B678CL,0x16C57B5CL,0xD4469016L,1L,0xBC0E26F0L,0xBC0E26F0L,1L}},{{0x669B678CL,0x8C40D4FEL,0x669B678CL,0x171566F9L,0xC85DF84BL,(-6L),0xE7C74476L},{1L,0xE7C74476L,0xD842213CL,0xBC0E26F0L,0x16C57B5CL,3L,0xFB94D7B6L},{0x16C57B5CL,0xDBF9BAFFL,0x14DFAFC3L,3L,0x46A04E36L,(-6L),0xDDA87D9EL}},{{(-6L),0x6CBF27FBL,0xCC1E2D72L,0x15E02292L,1L,0xBC0E26F0L,(-8L)},{0x46A04E36L,0L,0x15E02292L,0x15E02292L,0L,0x46A04E36L,0x669B678CL},{0xFB94D7B6L,0x46A04E36L,0x8C40D4FEL,3L,(-6L),0xCC1E2D72L,0xC85DF84BL}},{{3L,1L,0xEF035144L,0xBC0E26F0L,0L,1L,0x171566F9L},{0xC85DF84BL,0x46A04E36L,1L,0x171566F9L,0xCC1E2D72L,0x14DFAFC3L,1L},{0xEF035144L,0L,1L,1L,0x7EEBF2CDL,1L,1L}}};
                int16_t l_2565 = (-1L);
                int i, j, k;
                g_2542 = (safe_lshift_func_int16_t_s_u((+(safe_sub_func_int32_t_s_s((l_2526 = (p_24 <= ((l_2526 <= ((*g_505) = ((safe_div_func_float_f_f((safe_add_func_float_f_f(((***g_928) == (safe_sub_func_float_f_f((&g_2180 != l_2533), (-0x1.0p+1)))), (safe_div_func_float_f_f(p_24, (safe_div_func_float_f_f(((((safe_rshift_func_uint16_t_u_u(0xEAA1L, 10)) & p_24) != 0UL) , p_25), p_25)))))), p_25)) == p_24))) , p_25))), (**l_2327)))), (*g_2181)));
                (**l_2327) = (safe_add_func_int16_t_s_s((g_140 = p_24), l_2526));
                (*g_505) = (*g_1111);
                if (((((((safe_lshift_func_int8_t_s_u(l_2547[9], (&g_500 != &l_2344[1]))) && (**l_2327)) < ((((safe_add_func_uint16_t_u_u((p_25 != ((*l_51) = ((*l_2237) = (1UL > p_25)))), ((***l_2533) = ((safe_lshift_func_uint8_t_u_s((safe_sub_func_int64_t_s_s((p_24 , ((*l_2554) = l_2547[0])), 7UL)), 0)) | 1UL)))) < p_24) && p_25) == p_24)) | 1L) , (**l_2327)) || g_96))
                { /* block id: 1114 */
                    float l_2556 = (-0x4.Ap-1);
                    int32_t l_2557 = 0xA62BE1D4L;
                    int32_t l_2558 = 0x12A8F30DL;
                    int32_t l_2559 = 0x37B5AFB5L;
                    int32_t l_2561 = 0xFCB9DA4BL;
                    int32_t l_2562 = (-1L);
                    int32_t l_2563[2][7][5] = {{{0xEB81EC1EL,1L,0L,0x42C2FB35L,1L},{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL},{1L,0x42C2FB35L,0L,0x42C2FB35L,1L},{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL},{1L,0x42C2FB35L,0L,0x42C2FB35L,1L},{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL},{1L,0x42C2FB35L,0L,0x42C2FB35L,1L}},{{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL},{1L,0x42C2FB35L,0L,0x42C2FB35L,1L},{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL},{1L,0x42C2FB35L,0L,0x42C2FB35L,1L},{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL},{1L,0x42C2FB35L,0L,0x42C2FB35L,1L},{0xC34AAEEBL,7L,0xD4F43555L,7L,0xC34AAEEBL}}};
                    uint8_t l_2567 = 0xA5L;
                    int32_t ***l_2572 = (void*)0;
                    int i, j, k;
                    l_2567--;
                    for (l_2411 = (-4); (l_2411 <= 28); l_2411 = safe_add_func_int8_t_s_s(l_2411, 6))
                    { /* block id: 1118 */
                        int8_t l_2573[7] = {1L,1L,1L,1L,1L,1L,1L};
                        int i;
                        (**l_2327) = (l_2572 != &l_2327);
                        (**g_1110) = p_25;
                        if (l_2573[2])
                            continue;
                    }
                    (**l_2473) = (safe_add_func_float_f_f((((safe_lshift_func_uint8_t_u_s((l_2578 == (((void*)0 == l_2579) , l_2580[0])), (((p_25 > (safe_lshift_func_uint16_t_u_s((l_2256[0][7] == (*l_2473)), 5))) & 0x3EL) , 0xEAL))) , 0x6.9p+1) < p_25), (**g_504)));
                }
                else
                { /* block id: 1124 */
                    int32_t l_2585 = 0L;
                    int32_t l_2608 = 0x91972728L;
                    int32_t l_2610 = (-1L);
                    (**g_504) = (l_2585 < l_2435);
                    l_2603 = ((*g_130) = ((safe_add_func_float_f_f(((safe_add_func_float_f_f(((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((**g_504) = ((***g_1109) == (safe_sub_func_float_f_f(((((safe_rshift_func_int8_t_s_u(0xCEL, ((safe_lshift_func_int8_t_s_s((safe_mul_func_int32_t_s_s(((*g_1727) >= (*g_1727)), (g_1080 != g_1080))), 6)) > l_2417[6]))) , l_2585) >= ((((((0x0.DC721Ep-98 <= p_24) != (*g_505)) > p_25) > p_24) == 0xB.8DF6ADp-64) != p_25)) >= 0xB.82B3B6p+56), l_2417[6])))), l_2435)), 0x6.B01905p-8)) == 0x4.CC9D79p+0), (**l_2327))) != p_24), l_2602)) , &l_2555));
                    for (l_2235 = 12; (l_2235 < (-27)); --l_2235)
                    { /* block id: 1131 */
                        float l_2606 = 0xC.F9AAC9p+4;
                        int32_t l_2607 = (-3L);
                        int32_t l_2609[4] = {(-1L),(-1L),(-1L),(-1L)};
                        int i;
                        if ((*g_100))
                            break;
                        l_2611++;
                    }
                }
            }
        }
        l_2603 = (void*)0;
        return p_24;
    }
    else
    { /* block id: 1140 */
        uint16_t ***l_2628 = &g_2180;
        const int32_t l_2633 = 0xAA4533AEL;
        int32_t l_2636[7][10][2] = {{{0x22B3ADCDL,(-7L)},{2L,1L},{6L,0x186CB05AL},{0x3B48486EL,6L},{0xCB992B39L,1L},{0xCB992B39L,6L},{0x3B48486EL,0x186CB05AL},{6L,1L},{2L,(-7L)},{0x22B3ADCDL,0x158E6579L}},{{0x158E6579L,0xB0EA905FL},{6L,3L},{0x186CB05AL,0xDEC48675L},{0L,0L},{0L,7L},{0x601BA04EL,0x63536DD3L},{0x50F36B81L,0xB167A158L},{(-4L),0L},{0xB0EA905FL,(-8L)},{0xDEC48675L,6L}},{{1L,0xF1DAA0DAL},{0L,2L},{0x26B5F2B5L,0xCB992B39L},{(-8L),0L},{(-2L),(-2L)},{0x46F174DFL,5L},{0L,0L},{0x5D142CD6L,0x990BC95FL},{(-5L),0x990BC95FL},{0x5D142CD6L,0L}},{{0L,5L},{0x46F174DFL,(-2L)},{(-2L),0L},{(-8L),0xCB992B39L},{0x26B5F2B5L,2L},{0L,0xF1DAA0DAL},{1L,6L},{0xDEC48675L,(-8L)},{0xB0EA905FL,0L},{(-4L),0xB167A158L}},{{0x50F36B81L,0x63536DD3L},{0x601BA04EL,7L},{0L,0L},{0L,0xDEC48675L},{0x186CB05AL,3L},{6L,0xB0EA905FL},{0x158E6579L,0x158E6579L},{0x22B3ADCDL,(-7L)},{2L,1L},{6L,0x186CB05AL}},{{0x3B48486EL,6L},{0xCB992B39L,1L},{0xCB992B39L,6L},{0x3B48486EL,0x186CB05AL},{6L,1L},{2L,(-7L)},{0L,0L},{0L,3L},{0xB167A158L,0x22B3ADCDL},{0xDEC48675L,(-5L)}},{{0x50F36B81L,1L},{0x186CB05AL,0x26B5F2B5L},{(-2L),0x27834784L},{(-2L),(-7L)},{(-1L),0xCD5EACECL},{3L,0xF1DAA0DAL},{(-5L),0xB167A158L},{0L,0L},{0L,0L},{0L,(-8L)}}};
        uint32_t l_2638 = 0x143F756FL;
        int i, j, k;
        (**g_2143) = ((*g_130) = &l_1403[3][0]);
        for (p_24 = 0; (p_24 <= 4); p_24 += 1)
        { /* block id: 1145 */
            uint32_t l_2622 = 0x0CD92A08L;
            const uint8_t *l_2625 = &g_85[4];
            int32_t l_2634 = 0x7233AEBEL;
            int32_t l_2635 = (-8L);
            int32_t l_2637 = 8L;
            (**l_2327) = ((safe_rshift_func_uint16_t_u_u(p_25, ((safe_lshift_func_int16_t_s_u((safe_add_func_int32_t_s_s((safe_div_func_int64_t_s_s((((*l_51) |= l_2622) != (0xA5675856F491FD31LL & ((safe_mul_func_uint8_t_u_u((l_2625 == (void*)0), ((***g_2466) = ((l_2628 != (*g_1430)) < (((safe_div_func_uint64_t_u_u(((safe_mod_func_int64_t_s_s(p_24, (l_2622 | 4UL))) | p_24), p_25)) , l_2633) == l_2634))))) <= p_25))), p_25)), 4UL)), p_24)) > (*l_2603)))) | l_2633);
            --l_2638;
        }
        return l_2638;
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_347 g_153 g_129 g_130 g_131 g_439 g_224 g_31 g_1384 g_1430 g_1395 g_409 g_152 g_954 g_569 g_195 g_225 g_912 g_1394 g_52 g_282 g_504 g_505 g_1285 g_665 g_1498 g_261 g_1111 g_1112 g_128 g_1540 g_1556 g_1246 g_91 g_1089 g_85 g_1604 g_1647 g_1109 g_1110 g_1296 g_1297 g_1294 g_1295 g_1682 g_1683 g_100 g_503 g_96 g_101 g_62
 * writes: g_1395 g_1384 g_131 g_1431 g_1285 g_439 g_224 g_31 g_409 g_153 g_140 g_569 g_128 g_62 g_665 g_52 g_195 g_225 g_1556 g_594 g_1389 g_85 g_1246 g_91 g_101 g_96 g_1727 g_1728 g_1683
 */
static int64_t  func_43(int8_t  p_44, int16_t  p_45, const int8_t  p_46, const uint32_t  p_47)
{ /* block id: 646 */
    int64_t l_1408 = 0x69D2A8C9C7B84664LL;
    float ***l_1409 = &g_504;
    int8_t *l_1416 = &g_1285;
    int8_t **l_1417[7] = {&l_1416,&l_1416,&l_1416,&l_1416,&l_1416,&l_1416,&l_1416};
    int32_t l_1418 = 2L;
    int32_t l_1420 = 2L;
    int32_t l_1421 = 0xAF4B22C0L;
    int32_t l_1422 = 0xD4EECE5BL;
    int32_t l_1423 = (-8L);
    int32_t l_1424[10][4] = {{3L,1L,0x48D27FFEL,0x48D27FFEL},{1L,1L,5L,0x48D27FFEL},{3L,1L,0x48D27FFEL,0x48D27FFEL},{1L,1L,5L,0x48D27FFEL},{3L,1L,0x48D27FFEL,0x48D27FFEL},{1L,1L,5L,0x48D27FFEL},{3L,1L,0x48D27FFEL,0x48D27FFEL},{1L,1L,5L,0x48D27FFEL},{3L,1L,0x48D27FFEL,0x48D27FFEL},{1L,1L,5L,0x48D27FFEL}};
    uint32_t l_1425 = 0x234E20BCL;
    int32_t ** const *l_1434 = &g_130;
    int32_t ** const **l_1433 = &l_1434;
    int32_t ** const ***l_1432 = &l_1433;
    int32_t ****l_1470 = (void*)0;
    float ****l_1485 = &l_1409;
    int16_t l_1494 = 0x8FB0L;
    int16_t *l_1543 = &g_409;
    int16_t **l_1542 = &l_1543;
    uint32_t l_1545 = 0x623BF554L;
    int32_t ***l_1658 = (void*)0;
    int32_t l_1696[8][4][8] = {{{(-1L),0x050FEDBDL,(-8L),(-8L),0x050FEDBDL,(-1L),0x099AC862L,0x680700DCL},{(-1L),1L,1L,0x050FEDBDL,0x099AC862L,0x050FEDBDL,1L,1L},{0x9637C715L,1L,0x8B93EB91L,0x050FEDBDL,0x680700DCL,5L,5L,0x680700DCL},{(-8L),0x680700DCL,0x680700DCL,(-8L),0x9637C715L,1L,5L,(-1L)}},{{1L,(-8L),0x8B93EB91L,5L,0x8B93EB91L,(-8L),1L,0x099AC862L},{0x8B93EB91L,(-8L),1L,0x099AC862L,1L,1L,0x099AC862L,1L},{0x680700DCL,0x680700DCL,(-8L),0x9637C715L,1L,5L,(-1L),5L},{0x8B93EB91L,1L,0x9637C715L,1L,0x8B93EB91L,0x050FEDBDL,0x680700DCL,5L}},{{1L,1L,(-1L),0x9637C715L,0x9637C715L,(-1L),1L,1L},{(-8L),0x050FEDBDL,(-1L),0x099AC862L,0x680700DCL,0x8B93EB91L,0x680700DCL,0x099AC862L},{0x9637C715L,0xC6C79464L,0x9637C715L,5L,0x099AC862L,0x8B93EB91L,(-1L),(-1L)},{(-1L),0x050FEDBDL,(-8L),(-8L),0x050FEDBDL,(-1L),0x099AC862L,0x680700DCL}},{{(-1L),1L,1L,0x050FEDBDL,0x099AC862L,0x050FEDBDL,1L,1L},{0x9637C715L,1L,0x8B93EB91L,0x050FEDBDL,0x680700DCL,5L,5L,0x680700DCL},{(-8L),0x680700DCL,0x680700DCL,0x680700DCL,0x050FEDBDL,0x8B93EB91L,1L,0x9637C715L},{0xC6C79464L,0x680700DCL,(-1L),1L,(-1L),0x680700DCL,0xC6C79464L,1L}},{{(-1L),0x680700DCL,0xC6C79464L,1L,0x8B93EB91L,0x8B93EB91L,1L,0xC6C79464L},{5L,5L,0x680700DCL,0x050FEDBDL,0x8B93EB91L,1L,0x9637C715L,1L},{(-1L),0xC6C79464L,0x050FEDBDL,0xC6C79464L,(-1L),(-8L),5L,1L},{0xC6C79464L,0x8B93EB91L,0x9637C715L,0x050FEDBDL,0x050FEDBDL,0x9637C715L,0x8B93EB91L,0xC6C79464L}},{{0x680700DCL,(-8L),0x9637C715L,1L,5L,(-1L),5L,1L},{0x050FEDBDL,0x099AC862L,0x050FEDBDL,1L,1L,(-1L),0x9637C715L,0x9637C715L},{0x9637C715L,(-8L),0x680700DCL,0x680700DCL,(-8L),0x9637C715L,1L,5L},{0x9637C715L,0x8B93EB91L,0xC6C79464L,(-8L),1L,(-8L),0xC6C79464L,0x8B93EB91L}},{{0x050FEDBDL,0xC6C79464L,(-1L),(-8L),5L,1L,1L,5L},{0x680700DCL,5L,5L,0x680700DCL,0x050FEDBDL,0x8B93EB91L,1L,0x9637C715L},{0xC6C79464L,0x680700DCL,(-1L),1L,(-1L),0x680700DCL,0xC6C79464L,1L},{(-1L),0x680700DCL,0xC6C79464L,1L,0x8B93EB91L,0x8B93EB91L,1L,0xC6C79464L}},{{5L,5L,0x680700DCL,0x050FEDBDL,0x8B93EB91L,1L,0x9637C715L,1L},{(-1L),0xC6C79464L,0x050FEDBDL,0xC6C79464L,(-1L),(-8L),5L,1L},{0xC6C79464L,0x8B93EB91L,0x9637C715L,0x050FEDBDL,0x050FEDBDL,0x9637C715L,0x8B93EB91L,0xC6C79464L},{0x680700DCL,(-8L),0x9637C715L,1L,5L,(-1L),5L,1L}}};
    uint32_t l_1723 = 0xD26447FDL;
    uint32_t l_1742 = 0x5DB045A7L;
    int8_t ***l_1772[9][9] = {{&l_1417[1],&l_1417[5],(void*)0,&l_1417[5],&l_1417[1],(void*)0,&l_1417[5],&l_1417[6],&l_1417[5]},{&l_1417[3],&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5],(void*)0,&l_1417[5],&l_1417[5]},{&l_1417[5],&l_1417[0],&l_1417[5],&l_1417[3],&l_1417[5],(void*)0,&l_1417[3],(void*)0,(void*)0},{&l_1417[5],(void*)0,&l_1417[6],&l_1417[0],&l_1417[5],&l_1417[0],&l_1417[6],(void*)0,&l_1417[5]},{&l_1417[5],&l_1417[3],(void*)0,&l_1417[5],(void*)0,&l_1417[5],&l_1417[1],&l_1417[5],&l_1417[3]},{(void*)0,&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5],(void*)0,&l_1417[5],&l_1417[5]},{&l_1417[0],&l_1417[5],&l_1417[5],&l_1417[3],&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5]},{&l_1417[1],&l_1417[5],&l_1417[5],&l_1417[1],&l_1417[5],&l_1417[3],&l_1417[0],&l_1417[5],(void*)0},{&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[5],&l_1417[3],&l_1417[5],&l_1417[5],&l_1417[3],&l_1417[5]}};
    uint32_t l_1801 = 0xEAFCBEC3L;
    uint16_t ** const *l_1805 = (void*)0;
    uint8_t l_1862 = 0UL;
    uint64_t l_1928 = 0UL;
    uint8_t l_1929 = 0xA5L;
    int64_t **l_1956 = (void*)0;
    uint8_t *l_1998 = (void*)0;
    int8_t l_2032[7];
    float l_2145 = 0xD.3E887Fp-40;
    int8_t l_2212 = 0x21L;
    int16_t l_2213 = 0x7402L;
    uint8_t l_2216 = 0UL;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_2032[i] = 0x71L;
    if (((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_s(l_1408, 5)), 7)) | (((((((((void*)0 == l_1409) , ((safe_div_func_uint8_t_u_u(l_1408, (+((p_45 , (safe_add_func_int64_t_s_s((+(((g_1395 = l_1416) == (void*)0) , (((l_1408 >= (0UL == 0xAC65L)) == 0UL) , l_1408))), l_1408))) != (*****g_347))))) , (void*)0)) == &g_1270[5][0][2]) < 0xD3B41625D9EEACB9LL) && p_47) <= 0L) , 0x168236E6A5203472LL) & 7UL)))
    { /* block id: 648 */
        int32_t *l_1419[8];
        uint8_t l_1440 = 0xAEL;
        int i;
        for (i = 0; i < 8; i++)
            l_1419[i] = &g_439[0];
        ++l_1425;
        for (g_1384 = 0; (g_1384 >= (-4)); g_1384--)
        { /* block id: 652 */
            int16_t l_1437 = 0xD70FL;
            int32_t l_1438 = 0x5F1323A4L;
            int32_t l_1439[1][8];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 8; j++)
                    l_1439[i][j] = 0x2E4FB035L;
            }
            (*g_130) = l_1419[7];
            (*g_1430) = &g_1269;
            (*****l_1432) = (((void*)0 == l_1432) > (safe_mul_func_int8_t_s_s(l_1437, ((*g_1395) = p_45))));
            ++l_1440;
        }
    }
    else
    { /* block id: 659 */
        int32_t l_1450[1];
        uint8_t ****l_1460[3][3] = {{&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649}};
        int32_t ****l_1468[9][9][3] = {{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}},{{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]},{&g_129[1],&g_129[1],&g_129[1]}}};
        uint32_t *l_1471[10][7] = {{&g_594,&g_594,&g_594,&g_594,(void*)0,&g_594,&l_1425},{&g_225,(void*)0,&g_225,&g_225,(void*)0,(void*)0,(void*)0},{&g_594,&g_594,&g_594,&g_594,&l_1425,&g_594,(void*)0},{&l_1425,(void*)0,&g_225,&g_225,&g_225,&l_1425,&l_1425},{&g_594,(void*)0,&g_594,(void*)0,&g_594,&g_225,(void*)0},{(void*)0,&g_225,&l_1425,&g_225,&g_225,&l_1425,(void*)0},{&g_594,&g_594,&g_225,&g_225,&g_594,&g_594,&l_1425},{(void*)0,&g_225,(void*)0,(void*)0,(void*)0,&l_1425,&g_225},{&g_594,&l_1425,&g_594,&l_1425,&g_594,&l_1425,&g_594},{&l_1425,&g_225,&g_225,&g_225,(void*)0,&l_1425,&g_225}};
        int32_t l_1478 = 7L;
        int32_t *l_1483 = (void*)0;
        int32_t *l_1484[5][8][6];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1450[i] = 0x62BE7DADL;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 8; j++)
            {
                for (k = 0; k < 6; k++)
                    l_1484[i][j][k] = (void*)0;
            }
        }
        for (g_409 = (-30); (g_409 == 19); g_409 = safe_add_func_int64_t_s_s(g_409, 9))
        { /* block id: 662 */
            uint8_t ****l_1461 = &g_649;
            int16_t *l_1474 = &g_140;
            int32_t l_1475[2];
            int32_t l_1476 = 0x1940D806L;
            int32_t l_1477 = 8L;
            int64_t *l_1479 = (void*)0;
            int64_t *l_1480 = &g_569;
            uint32_t l_1481 = 18446744073709551615UL;
            int32_t l_1482 = 6L;
            int i;
            for (i = 0; i < 2; i++)
                l_1475[i] = 0L;
            l_1482 |= ((((safe_lshift_func_int8_t_s_s((-1L), ((safe_lshift_func_uint8_t_u_u((l_1450[0] = (safe_unary_minus_func_uint8_t_u(255UL))), (safe_add_func_int64_t_s_s((safe_add_func_int64_t_s_s((((safe_unary_minus_func_uint16_t_u(((((*l_1480) ^= ((safe_rshift_func_uint8_t_u_u((((safe_div_func_int64_t_s_s(((l_1460[0][2] != l_1461) , (safe_div_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s((safe_add_func_uint32_t_u_u(2UL, ((*****l_1432) = (l_1476 |= ((l_1468[1][5][1] == (((l_1475[0] ^= (+((((((*g_347) = l_1470) != (((*l_1474) = (l_1471[3][2] != ((safe_lshift_func_uint16_t_u_s(p_47, 4)) , (void*)0))) , (*l_1432))) | (-6L)) , (void*)0) != (void*)0))) , (****l_1433)) , (*g_152))) != p_47))))), p_45)), 0x699D06BA058BE660LL))), l_1477)) ^ l_1478) & g_954), 1)) | p_46)) || g_195) > l_1477))) , p_44) != p_47), g_225)), g_912[5][0][2])))) == (*g_1394)))) != g_282) <= l_1481) , 0L);
            return p_47;
        }
        l_1484[0][7][3] = ((***l_1433) = l_1483);
        (****l_1432) = (***l_1433);
    }
    (**g_504) = 0x1.2p-1;
    if ((&g_504 != ((*l_1485) = l_1409)))
    { /* block id: 679 */
        int64_t *l_1495 = &l_1408;
        int16_t *l_1496 = &g_62;
        uint32_t *l_1497 = &g_665;
        uint16_t l_1506 = 0x3848L;
        int32_t l_1538[9] = {0xF9694DA2L,0xF9694DA2L,0x229C80B0L,0xF9694DA2L,0xF9694DA2L,0x229C80B0L,0xF9694DA2L,0xF9694DA2L,0x229C80B0L};
        int32_t l_1541 = 5L;
        uint8_t *l_1640 = &g_85[6];
        uint8_t **l_1639[9][7][4] = {{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,(void*)0},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,(void*)0,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,&l_1640},{&l_1640,&l_1640,(void*)0,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,&l_1640},{(void*)0,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,(void*)0},{(void*)0,&l_1640,&l_1640,&l_1640},{(void*)0,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,(void*)0,&l_1640,(void*)0},{&l_1640,(void*)0,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,(void*)0},{(void*)0,(void*)0,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,(void*)0,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}},{{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,(void*)0,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640},{&l_1640,&l_1640,&l_1640,&l_1640}}};
        int16_t l_1718 = 0xE9A2L;
        int32_t *l_1755 = (void*)0;
        uint16_t l_1766 = 65526UL;
        int8_t ***l_1769 = &l_1417[6];
        int8_t ****l_1770[5][7][3] = {{{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,(void*)0},{&l_1769,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,&l_1769}},{{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,(void*)0},{&l_1769,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,(void*)0}},{{&l_1769,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,(void*)0},{&l_1769,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769}},{{(void*)0,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,(void*)0},{&l_1769,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769}},{{(void*)0,&l_1769,(void*)0},{&l_1769,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,&l_1769},{&l_1769,&l_1769,&l_1769},{(void*)0,&l_1769,(void*)0},{&l_1769,&l_1769,&l_1769}}};
        int8_t ***l_1771 = &l_1417[4];
        int i, j, k;
        (****l_1485) = (safe_div_func_float_f_f((p_47 < p_44), (((*l_1497) |= (0UL < ((-10L) && (safe_sub_func_uint16_t_u_u(((void*)0 != &p_44), (((*l_1496) = (safe_mod_func_uint64_t_u_u(((safe_add_func_int64_t_s_s(((*l_1495) = (p_47 & l_1494)), (-3L))) | (*g_1395)), 0xA68761698E5C922DLL))) < 1UL)))))) , g_1498[1])));
lbl_1559:
        (*g_261) |= p_47;
        for (g_62 = 0; (g_62 == (-7)); g_62 = safe_sub_func_int32_t_s_s(g_62, 2))
        { /* block id: 687 */
            int32_t *l_1518 = &g_282;
            uint16_t *l_1539 = &g_195;
            int32_t l_1544 = 0xEF173FE4L;
            int32_t l_1553 = (-2L);
            int32_t l_1554 = 0L;
            int32_t l_1555 = (-1L);
            uint8_t ***l_1659 = &g_650;
            uint8_t l_1661 = 0UL;
            int32_t *l_1668 = &g_282;
            float *l_1697[5] = {&g_128[5],&g_128[5],&g_128[5],&g_128[5],&g_128[5]};
            uint32_t **l_1704[2];
            int32_t l_1720 = 0xE727275CL;
            int32_t l_1722[7][5][7] = {{{(-1L),5L,0x4BB24538L,0x17C986B9L,0x43CC4FB3L,(-3L),0x43CC4FB3L},{(-1L),(-2L),(-2L),(-1L),1L,0x329B266BL,(-1L)},{(-2L),0x8161B548L,0x329B266BL,0xD6F46E43L,0x17C986B9L,(-1L),0xCA293BDEL},{0x296C8D8AL,1L,0x1ADB380DL,4L,0xD6F46E43L,0x0C148D7AL,(-1L)},{1L,(-1L),0xC14B90A1L,0L,1L,(-2L),0x43CC4FB3L}},{{0x1ADB380DL,0xE573DD85L,0x0C148D7AL,(-3L),0xFE990A63L,(-2L),(-1L)},{5L,0L,(-6L),(-1L),1L,1L,1L},{0x43CC4FB3L,(-6L),0L,(-6L),0x43CC4FB3L,0L,0x0C148D7AL},{(-1L),0x0866B0B7L,0x1ADB380DL,0xA7FAA0B9L,0x8161B548L,0xCA293BDEL,0L},{0xCA293BDEL,(-3L),0x0C148D7AL,0xE573DD85L,0x1ADB380DL,1L,0xD30A40C1L}},{{(-1L),0xA7FAA0B9L,0xF0D2AA3AL,0x4BB24538L,0L,5L,0x43CC4FB3L},{0x43CC4FB3L,0xC14B90A1L,0xA7FAA0B9L,0x0866B0B7L,0x329B266BL,(-1L),0x4BB24538L},{5L,(-1L),0x0866B0B7L,0xCA293BDEL,0x329B266BL,0xFE990A63L,0xA81F00EDL},{0x27A7237EL,0L,1L,1L,0L,0x27A7237EL,(-3L)},{1L,0xFE990A63L,1L,0xD6F46E43L,0x1ADB380DL,0x0C148D7AL,0xC14B90A1L}},{{(-1L),1L,0L,0x17C986B9L,0x8161B548L,0x99E276CCL,(-2L)},{(-1L),0xFE990A63L,(-3L),1L,0x43CC4FB3L,0xC14B90A1L,0xA7FAA0B9L},{0x4BB24538L,0L,5L,0x43CC4FB3L,1L,0x8161B548L,(-1L)},{(-1L),(-1L),0x99E276CCL,0xF0D2AA3AL,0xFE990A63L,0x8161B548L,4L},{1L,0xC14B90A1L,0xA81F00EDL,(-1L),0xA81F00EDL,0xC14B90A1L,1L}},{{(-1L),0xA7FAA0B9L,0x47211B5DL,0x8161B548L,0xE573DD85L,0x99E276CCL,(-1L)},{(-4L),(-3L),(-1L),0xFE990A63L,0x0866B0B7L,0x0C148D7AL,0xCA293BDEL},{0xA81F00EDL,0x0866B0B7L,0x47211B5DL,(-4L),0x0C148D7AL,0x27A7237EL,0x8161B548L},{4L,(-6L),0xA81F00EDL,(-1L),0xD6F46E43L,0xFE990A63L,0x47211B5DL},{0x296C8D8AL,0L,0x99E276CCL,1L,(-1L),(-1L),0x47211B5DL}},{{0xC14B90A1L,0x8161B548L,5L,0x47211B5DL,0x47211B5DL,5L,0x8161B548L},{0x0866B0B7L,0L,(-3L),(-1L),0xE3C9A789L,1L,0xCA293BDEL},{(-2L),0xA81F00EDL,0L,0x1ADB380DL,5L,0xCA293BDEL,(-1L)},{0L,0xE3C9A789L,1L,(-1L),0xC14B90A1L,0L,1L},{0xE573DD85L,(-1L),1L,0x47211B5DL,0xD30A40C1L,1L,4L}},{{(-1L),(-1L),0x0866B0B7L,1L,1L,(-2L),(-1L)},{(-1L),(-2L),0xA7FAA0B9L,(-1L),0L,(-1L),0xA7FAA0B9L},{0xE573DD85L,0xE573DD85L,0xF0D2AA3AL,(-4L),0L,0L,(-1L)},{0xCA293BDEL,0x0866B0B7L,(-1L),5L,1L,0x43CC4FB3L,4L},{(-1L),1L,0x27A7237EL,0xE3C9A789L,0xCA293BDEL,(-1L),(-1L)}}};
            uint32_t **l_1726[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int32_t *l_1747 = &l_1696[2][1][3];
            int32_t *l_1756 = &l_1720;
            int32_t *l_1757 = &g_1384;
            int32_t *l_1758 = &l_1541;
            int32_t *l_1759 = &l_1541;
            int32_t *l_1760 = &l_1421;
            int32_t *l_1761 = &l_1722[2][3][1];
            int32_t *l_1762 = (void*)0;
            int32_t *l_1763 = &l_1421;
            int32_t *l_1764 = &l_1722[2][1][5];
            int32_t *l_1765[6][9] = {{&l_1423,&l_1423,(void*)0,&l_1554,(void*)0,&l_1423,&l_1423,(void*)0,&l_1554},{&l_1554,&l_1418,&l_1554,&l_1553,&l_1553,&l_1554,&l_1418,&l_1554,&l_1553},{(void*)0,(void*)0,(void*)0,(void*)0,&g_439[0],(void*)0,(void*)0,(void*)0,(void*)0},{&g_31[1],&l_1553,&l_1538[4],&l_1553,&g_31[1],&g_31[1],&l_1553,&l_1538[4],&l_1553},{(void*)0,&g_439[0],&l_1554,&l_1554,&g_439[0],(void*)0,&g_439[0],&l_1554,&l_1554},{&g_31[1],&g_31[1],&l_1553,&l_1538[4],&l_1553,&g_31[1],&g_31[1],&l_1553,&l_1538[4]}};
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1704[i] = &g_1102;
            if (((((safe_mul_func_uint8_t_u_u((((*g_1394) = (+l_1506)) ^ (((safe_add_func_int64_t_s_s((safe_add_func_uint8_t_u_u((!(safe_sub_func_int32_t_s_s((safe_mul_func_uint16_t_u_u(1UL, ((safe_mul_func_int16_t_s_s((&g_282 != l_1518), (safe_lshift_func_int16_t_s_s(((((((safe_mod_func_int16_t_s_s((p_46 & ((l_1541 = (safe_add_func_int32_t_s_s((l_1506 || (safe_sub_func_uint32_t_u_u(((void*)0 == &l_1496), (((safe_add_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_u(((+((*l_1539) |= ((safe_rshift_func_int16_t_s_u(((safe_div_func_float_f_f((l_1538[4] = ((p_46 , (*g_1111)) < 0x4.1p+1)), (**g_504))) , p_46), p_44)) > p_46))) != l_1506), 7)) > g_1540[2][8]), 0L)) || 0x93921944L) | p_47)))), 3L))) != g_225)), p_47)) == p_45) > p_46) <= g_224) , (void*)0) == l_1542), l_1506)))) || 0x41E1E9FFCE965906LL))), p_46))), l_1544)), l_1545)) , 65531UL) == p_44)), l_1506)) , 0x7981L) || p_44) ^ l_1544))
            { /* block id: 692 */
                int64_t l_1549 = (-10L);
                int32_t l_1550 = 0xF9833600L;
                int32_t *l_1551 = &g_439[4];
                int32_t *l_1552[6];
                int i;
                for (i = 0; i < 6; i++)
                    l_1552[i] = &l_1538[6];
                for (p_44 = 0; (p_44 > (-9)); p_44 = safe_sub_func_uint16_t_u_u(p_44, 7))
                { /* block id: 695 */
                    for (g_225 = 0; (g_225 <= 8); g_225 += 1)
                    { /* block id: 698 */
                        int32_t *l_1548[6] = {&l_1421,&l_1421,&l_1421,&l_1421,&l_1421,&l_1421};
                        int i;
                        (*g_130) = l_1548[0];
                    }
                }
                g_1556--;
                if (g_1556)
                    goto lbl_1559;
            }
            else
            { /* block id: 704 */
                int32_t l_1572 = 0x8E7A6572L;
                uint64_t *l_1582 = &g_91;
                int32_t l_1588 = 0xA9842B89L;
                uint32_t l_1589[2][1][7] = {{{4294967295UL,4294967295UL,4294967291UL,9UL,4294967291UL,4294967295UL,4294967295UL}},{{4294967295UL,4294967291UL,9UL,4294967291UL,4294967295UL,4294967295UL,4294967291UL}}};
                uint8_t *l_1590 = (void*)0;
                uint8_t *l_1591 = &g_85[6];
                uint64_t l_1617 = 3UL;
                int32_t *l_1666[2];
                int32_t *l_1670 = &g_282;
                int32_t l_1719 = 0xF1FC42E8L;
                int32_t l_1721[8];
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_1666[i] = &g_282;
                for (i = 0; i < 8; i++)
                    l_1721[i] = 0x51C5BA1BL;
                for (g_594 = 0; (g_594 < 20); ++g_594)
                { /* block id: 707 */
                    for (g_1389 = 0; g_1389 < 9; g_1389 += 1)
                    {
                        g_85[g_1389] = 0x97L;
                    }
                }
                if (((((((safe_div_func_uint16_t_u_u(p_45, 0x5738L)) ^ (safe_lshift_func_int8_t_s_s(((((((*l_1591) ^= (safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_s((safe_mod_func_int16_t_s_s(l_1572, ((**l_1542) = (((255UL < (safe_div_func_uint16_t_u_u((g_282 && ((((safe_div_func_uint8_t_u_u(((l_1588 = (p_45 <= (safe_mul_func_uint8_t_u_u(((g_1246 &= p_44) | (!((--(*l_1582)) & ((((+p_45) <= (safe_div_func_int64_t_s_s(p_46, g_569))) != 0UL) , g_1498[1])))), p_47)))) , 0xC9L), p_47)) < l_1506) && 65531UL) , g_1089[2])), l_1572))) > l_1589[0][0][1]) || l_1553)))), p_45)), l_1544))) ^ p_45) | (-1L)) , (void*)0) == &g_1080), 7))) < l_1541) || l_1541) > p_47) >= 2UL))
                { /* block id: 715 */
                    int64_t ***l_1611 = &g_397;
                    int64_t ****l_1610 = &l_1611;
                    int64_t *****l_1609 = &l_1610;
                    int32_t l_1616 = (-6L);
                    uint32_t l_1634 = 0xF238A23EL;
                    int32_t l_1642[4];
                    int8_t ***l_1671 = (void*)0;
                    int32_t *l_1705 = &l_1422;
                    int32_t *l_1706 = &l_1616;
                    int32_t *l_1707 = &g_31[1];
                    int32_t *l_1708 = &l_1696[5][0][0];
                    int32_t *l_1709 = &l_1538[1];
                    int32_t *l_1710 = &l_1421;
                    int32_t *l_1711 = &l_1424[7][0];
                    int32_t *l_1712 = &l_1642[0];
                    int32_t *l_1713 = &l_1572;
                    int32_t *l_1714 = &g_224;
                    int32_t *l_1715 = (void*)0;
                    int32_t *l_1716[6] = {&l_1555,&l_1538[0],&l_1555,&l_1555,&l_1538[0],&l_1555};
                    int8_t l_1717 = 1L;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1642[i] = (-10L);
                    if ((((p_46 & ((((safe_div_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s((safe_add_func_uint32_t_u_u((safe_div_func_uint64_t_u_u((safe_add_func_uint8_t_u_u(((g_912[1][0][0] , g_1604[0]) == ((*l_1609) = (void*)0)), (safe_mul_func_uint16_t_u_u((1UL < (((*l_1497) = ((((safe_sub_func_int16_t_s_s((p_44 == (p_46 ^ l_1538[4])), g_91)) , p_47) , (-1L)) != 0x47170F5FL)) > l_1616)), p_46)))), p_47)), 0x66845F95L)), p_46)), 0x27L)) || p_44), p_46)) & (-1L)) & l_1617) , p_45)) ^ p_47) || 0x54L))
                    { /* block id: 718 */
                        int16_t l_1618 = 0x89EBL;
                        int32_t *l_1619 = &l_1572;
                        int32_t *l_1620 = &l_1424[5][0];
                        int32_t *l_1621 = &l_1554;
                        int32_t *l_1622 = (void*)0;
                        int32_t *l_1623 = &l_1588;
                        int32_t *l_1624 = &l_1553;
                        int32_t *l_1625 = &l_1616;
                        int32_t *l_1626 = (void*)0;
                        int32_t *l_1627 = &g_31[6];
                        int32_t *l_1628 = (void*)0;
                        int32_t l_1629 = (-1L);
                        int32_t *l_1630 = &l_1424[2][3];
                        int32_t *l_1631 = &l_1555;
                        int32_t *l_1632 = &g_31[1];
                        int32_t *l_1633 = &l_1424[7][0];
                        uint16_t *l_1660 = &g_1556;
                        l_1634--;
                        l_1642[0] = ((****l_1485) = ((safe_sub_func_float_f_f(((-0x10.Ap+1) <= 0xC.174548p-64), ((p_45 , ((l_1616 , l_1639[0][5][1]) == (void*)0)) < (-(l_1588 , (-0x1.Ap-1)))))) != (p_45 , p_46)));
                        l_1555 &= ((*l_1630) = ((safe_rshift_func_uint16_t_u_s(((*l_1660) = ((((p_46 >= (safe_sub_func_int64_t_s_s((l_1553 , (g_1647 || ((((safe_sub_func_float_f_f((((safe_lshift_func_int16_t_s_u((~((!(safe_mul_func_uint8_t_u_u(249UL, 0x30L))) < (((++(*l_1539)) , (**g_152)) != (l_1658 = (*g_153))))), 6)) || (p_44 < ((((void*)0 == l_1659) <= 0xBBE4L) == l_1506))) , (***g_1109)), 0x5.Ap+1)) , (*g_1296)) ^ 0x3CL) != l_1642[0]))), p_47))) > 255UL) | (*g_1394)) , p_47)), p_45)) > l_1661));
                    }
                    else
                    { /* block id: 727 */
                        int32_t **l_1667 = &l_1518;
                        int32_t **l_1669[4][5][4] = {{{&l_1666[0],&l_1666[0],(void*)0,&l_1668},{&l_1666[0],&l_1666[0],(void*)0,&l_1666[0]},{&l_1668,&l_1666[0],&l_1666[0],(void*)0},{(void*)0,&l_1666[0],(void*)0,&l_1668},{&l_1666[0],&l_1666[0],(void*)0,&l_1666[0]}},{{(void*)0,(void*)0,&l_1666[0],&l_1666[0]},{(void*)0,&l_1666[0],(void*)0,&l_1666[0]},{&l_1666[0],&l_1668,(void*)0,&l_1666[0]},{&l_1666[0],&l_1668,(void*)0,&l_1666[1]},{(void*)0,&l_1666[0],&l_1666[0],&l_1666[0]}},{{(void*)0,&l_1666[0],(void*)0,&l_1666[0]},{&l_1666[0],&l_1666[0],(void*)0,&l_1666[1]},{(void*)0,&l_1666[0],&l_1666[0],&l_1666[0]},{&l_1668,&l_1668,(void*)0,(void*)0},{&l_1666[0],&l_1666[0],(void*)0,&l_1666[0]}},{{&l_1666[0],&l_1666[0],&l_1666[0],(void*)0},{(void*)0,&l_1666[0],(void*)0,&l_1666[0]},{&l_1666[0],&l_1666[0],(void*)0,(void*)0},{&l_1666[0],&l_1668,&l_1666[0],&l_1666[0]},{&l_1666[0],&l_1666[0],&l_1666[0],&l_1666[1]}}};
                        int i, j, k;
                        (*g_100) = (!(safe_lshift_func_int16_t_s_u(((!(((*l_1667) = l_1666[0]) == (l_1670 = l_1668))) < ((void*)0 == l_1671)), (((safe_mod_func_uint32_t_u_u((((safe_div_func_uint32_t_u_u(((*l_1497) = ((p_47 , (((((safe_add_func_int16_t_s_s((18446744073709551607UL <= ((safe_lshift_func_int16_t_s_s(0x5B8EL, 10)) >= (l_1588 &= ((*l_1591) = l_1589[0][0][0])))), (safe_div_func_int8_t_s_s((((0x54L == (***g_1294)) < p_47) | g_439[0]), l_1555)))) & p_44) == g_1682) & p_47) , (**g_1295))) && (**g_1295))), 3UL)) ^ p_45) && 8UL), p_44)) | g_1683) < l_1506))));
                        if (p_44)
                            break;
                    }
                    for (g_96 = (-30); (g_96 == 42); ++g_96)
                    { /* block id: 738 */
                        const uint32_t *l_1703 = &l_1425;
                        const uint32_t ** const l_1702 = &l_1703;
                        (**l_1434) = (((safe_div_func_uint32_t_u_u((((p_45 ^ ((**l_1542) ^= (((++g_91) | (safe_sub_func_int16_t_s_s((safe_sub_func_uint64_t_u_u((p_46 && (p_46 >= (safe_lshift_func_int16_t_s_u(l_1696[5][0][0], 5)))), (l_1555 < ((p_44 , ((**g_503) == (l_1697[4] = l_1518))) && (safe_add_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(0x27L, p_45)), p_46)))))), g_96))) | 65535UL))) ^ 0x2E512C523F0C9381LL) > p_47), 0xC1EC2789L)) || 1UL) , &l_1572);
                        (****l_1485) = (l_1702 == l_1704[1]);
                    }
                    --l_1723;
                    if ((*l_1708))
                        break;
                }
                else
                { /* block id: 747 */
                    for (l_1545 = 0; (l_1545 <= 5); l_1545 += 1)
                    { /* block id: 750 */
                        return l_1538[4];
                    }
                }
            }
            l_1544 &= ((g_1727 = l_1497) != (g_1728 = &g_1246));
            if ((((safe_mul_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(p_46, (safe_sub_func_int8_t_s_s((p_46 ^ (safe_mod_func_int64_t_s_s((((((((*l_1539) |= l_1538[3]) >= (safe_unary_minus_func_uint16_t_u(p_46))) == (((l_1554 &= (l_1541 = 255UL)) ^ ((((safe_sub_func_uint16_t_u_u((((l_1742 ^ 0xEF5DL) <= p_44) == (safe_div_func_int8_t_s_s(((*l_1416) = ((p_45 < 0xEFL) , 0x94L)), l_1718))), 0x8CBFL)) < l_1506) , 4294967295UL) != 1UL)) < l_1718)) >= l_1718) != p_47) && 0xA7L), 3UL))), 7L)))), l_1538[4])) >= l_1722[1][4][0]) , (*g_100)))
            { /* block id: 762 */
                if (p_47)
                    goto lbl_1559;
                for (g_1683 = 0; (g_1683 < (-8)); g_1683 = safe_sub_func_uint32_t_u_u(g_1683, 4))
                { /* block id: 766 */
                    l_1747 = (void*)0;
                    (****l_1485) = (-0x1.Fp-1);
                    (*g_261) ^= 1L;
                }
            }
            else
            { /* block id: 771 */
                int32_t *l_1748 = &g_1384;
                int32_t *l_1749 = (void*)0;
                int32_t *l_1750[5];
                uint32_t l_1752 = 0x6A183B66L;
                int i;
                for (i = 0; i < 5; i++)
                    l_1750[i] = &l_1544;
                ++l_1752;
                (*g_100) &= p_44;
                l_1749 = (l_1755 = ((**l_1434) = &l_1538[4]));
            }
            --l_1766;
        }
        (*g_261) = ((p_44 < ((p_44 > ((l_1772[2][0] = (l_1771 = l_1769)) != (void*)0)) != ((*l_1497) |= ((0x4FEEL ^ p_46) > ((p_45 > ((*g_1395) = p_46)) < (!((*l_1496) = p_47))))))) != l_1766);
    }
    else
    { /* block id: 786 */
        uint16_t l_1796 = 0x8ABBL;
        int32_t l_1797 = 0xF4DC0B41L;
        uint16_t *l_1804[10] = {&g_195,&l_1796,&l_1796,&g_195,&l_1796,&l_1796,&g_195,&l_1796,&l_1796,&g_195};
        uint16_t **l_1803 = &l_1804[8];
        uint16_t ***l_1802[5][7][4] = {{{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,(void*)0,&l_1803,&l_1803},{&l_1803,(void*)0,(void*)0,&l_1803},{(void*)0,&l_1803,&l_1803,&l_1803},{(void*)0,&l_1803,(void*)0,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803}},{{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,(void*)0,&l_1803,&l_1803},{&l_1803,(void*)0,(void*)0,&l_1803},{(void*)0,&l_1803,&l_1803,&l_1803},{(void*)0,&l_1803,(void*)0,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803}},{{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,(void*)0,&l_1803,&l_1803},{&l_1803,(void*)0,(void*)0,&l_1803},{(void*)0,&l_1803,&l_1803,&l_1803},{(void*)0,&l_1803,(void*)0,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803}},{{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,(void*)0,&l_1803,&l_1803},{&l_1803,(void*)0,(void*)0,&l_1803},{(void*)0,&l_1803,&l_1803,&l_1803},{(void*)0,&l_1803,(void*)0,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803}},{{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,(void*)0,&l_1803,&l_1803},{&l_1803,(void*)0,(void*)0,&l_1803},{(void*)0,&l_1803,&l_1803,&l_1803},{(void*)0,&l_1803,(void*)0,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803},{&l_1803,&l_1803,&l_1803,&l_1803}}};
        uint64_t l_1812 = 1UL;
        uint32_t l_1831[3][7] = {{0xA2E79719L,0x3EDDB1A4L,0x3EDDB1A4L,0xA2E79719L,0x3EDDB1A4L,0x3EDDB1A4L,0xA2E79719L},{0x3EDDB1A4L,0xA2E79719L,0x3EDDB1A4L,0x3EDDB1A4L,0xA2E79719L,0x3EDDB1A4L,0x3EDDB1A4L},{0xA2E79719L,0xA2E79719L,0x3C2195E3L,0xA2E79719L,0xA2E79719L,0x3C2195E3L,0xA2E79719L}};
        int32_t l_1867 = 0x246A9CEEL;
        int32_t l_1884[10] = {0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L,0x9FA20404L};
        int32_t l_1891 = (-1L);
        uint64_t l_1895 = 18446744073709551615UL;
        int32_t ***l_1902 = (void*)0;
        int32_t l_1930 = 1L;
        int8_t l_1968 = 0x03L;
        uint8_t l_2000[1];
        float *l_2204 = &g_128[5];
        int32_t *l_2208 = &l_1423;
        float l_2215 = (-0x1.0p+1);
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2000[i] = 0x64L;
    }
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_75 g_52 g_962 g_261 g_101 g_31 g_85 g_225 g_224 g_130 g_131 g_153 g_129 g_504 g_505 g_140 g_33 g_333 g_100 g_649 g_650 g_152 g_409 g_665 g_96 g_128 g_347 g_282 g_1102 g_954 g_594 g_439 g_1246 g_1269 g_1109 g_1110 g_1111 g_746 g_1294 g_1307 g_1313 g_569 g_1296 g_1297 g_912 g_1384 g_1389
 * writes: g_62 g_74 g_75 g_52 g_31 g_224 g_131 g_261 g_505 g_101 g_594 g_244 g_128 g_665 g_96 g_91 g_225 g_282 g_1263 g_195 g_1112 g_33 g_439 g_569 g_140 g_1384 g_1389
 */
static int8_t  func_48(uint8_t  p_49, int64_t * const  p_50)
{ /* block id: 6 */
    int32_t l_60 = 0x3C10B42BL;
    int16_t *l_61 = &g_62;
    uint64_t *l_73[4] = {&g_74,&g_74,&g_74,&g_74};
    int16_t * const l_76[1] = {(void*)0};
    int32_t l_77[9][4] = {{0x0C255375L,(-10L),(-5L),0x36C8609EL},{0x0C255375L,0x4C38DB07L,0x21ADF561L,0x21ADF561L},{0x36C8609EL,0x36C8609EL,(-1L),1L},{1L,(-1L),0L,0xF60700C0L},{(-7L),0xDCF8610FL,(-5L),0L},{0L,0xDCF8610FL,(-1L),0xF60700C0L},{0xDCF8610FL,(-10L),0xDCF8610FL,0x4C38DB07L},{(-1L),(-1L),0L,(-7L)},{1L,(-1L),0x4C38DB07L,(-1L)}};
    int32_t *l_1383 = &g_1384;
    int32_t *l_1385 = &g_31[6];
    int32_t *l_1386[9][10][2] = {{{&g_31[1],&l_77[7][1]},{(void*)0,&g_1384},{&g_31[1],&g_31[1]},{&g_1384,&l_77[4][3]},{&g_31[5],&g_31[0]},{(void*)0,&g_31[1]},{&l_77[4][3],(void*)0},{&l_60,&g_1384},{&l_60,(void*)0},{&l_77[4][3],&g_31[1]}},{{(void*)0,&g_31[0]},{&g_31[5],&l_77[4][3]},{&g_1384,&g_31[1]},{&g_31[1],&g_1384},{(void*)0,&l_77[7][1]},{&g_31[1],&g_31[5]},{&l_60,(void*)0},{&g_31[0],(void*)0},{&l_60,&g_31[5]},{&g_31[1],&l_77[7][1]}},{{(void*)0,&g_1384},{&g_31[1],&g_31[1]},{&g_1384,&l_77[4][3]},{&g_31[5],&g_31[0]},{(void*)0,&g_31[1]},{&l_77[4][3],(void*)0},{&l_60,&g_1384},{&l_60,(void*)0},{&l_77[4][3],&g_31[1]},{(void*)0,&g_31[0]}},{{&g_31[5],&l_77[4][3]},{&g_1384,&g_31[1]},{&g_31[1],&g_1384},{(void*)0,&l_77[7][1]},{&g_31[1],&g_31[5]},{&l_60,(void*)0},{&g_31[0],(void*)0},{&l_60,&g_31[5]},{&g_31[1],&l_77[7][1]},{(void*)0,&g_1384}},{{&g_31[1],&g_31[1]},{&g_1384,&l_77[4][3]},{&g_31[5],&g_31[0]},{(void*)0,&g_31[1]},{&l_77[4][3],(void*)0},{&l_60,&g_1384},{&l_60,(void*)0},{&l_77[4][3],&g_31[1]},{(void*)0,&g_31[0]},{&g_31[5],&l_77[4][3]}},{{&g_1384,&g_31[1]},{&g_31[1],&g_1384},{(void*)0,&l_77[7][1]},{&g_31[1],&g_31[5]},{&l_60,(void*)0},{&g_31[0],(void*)0},{&l_60,&g_31[5]},{&g_31[1],&l_77[7][1]},{(void*)0,&g_1384},{&g_31[1],&g_31[1]}},{{&g_1384,&l_77[4][3]},{&g_31[5],&g_31[0]},{(void*)0,&g_31[1]},{&l_77[4][3],(void*)0},{&l_60,&g_1384},{&l_60,(void*)0},{&l_77[4][3],&g_31[1]},{(void*)0,&g_31[0]},{&g_31[5],&l_77[4][3]},{&g_1384,&g_31[1]}},{{&g_31[1],&g_1384},{(void*)0,&l_77[7][1]},{&g_31[1],&g_31[5]},{&l_60,(void*)0},{&g_31[0],(void*)0},{&l_60,&g_31[5]},{&g_31[1],&l_77[7][1]},{(void*)0,&g_1384},{&g_31[1],&g_31[1]},{&g_1384,&l_77[4][3]}},{{&g_31[5],&g_31[0]},{(void*)0,&g_31[1]},{&l_77[4][3],(void*)0},{&l_60,&g_1384},{&l_60,&g_31[0]},{(void*)0,(void*)0},{&g_31[0],&g_1384},{&g_1384,(void*)0},{&g_31[1],&l_60},{&l_60,&g_31[1]}}};
    int16_t l_1387 = 0xB687L;
    int8_t l_1388 = 0x8DL;
    int i, j, k;
    (*l_1383) &= (((0xF890L >= ((*l_61) = (safe_mod_func_uint16_t_u_u(l_60, (-1L))))) ^ (*p_50)) > (safe_mod_func_int8_t_s_s((safe_mod_func_uint8_t_u_u(func_67((g_75 &= (g_74 = l_60)), &g_52, l_76[0], (l_77[4][1] = 1L), func_78(l_60, &l_60)), 0x92L)), p_49)));
    --g_1389;
    return p_49;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_67(uint64_t  p_68, uint8_t * p_69, int16_t * const  p_70, int16_t  p_71, const int64_t ** const  p_72)
{ /* block id: 638 */
    uint64_t l_1380 = 0xA8C01BF9DBD1D53ALL;
    l_1380--;
    return l_1380;
}


/* ------------------------------------------ */
/* 
 * reads : g_52 g_962 g_261 g_101 g_31 g_85 g_225 g_224 g_130 g_131 g_153 g_129 g_504 g_505 g_140 g_33 g_333 g_100 g_649 g_650 g_152 g_409 g_4 g_665 g_96 g_128 g_347 g_282 g_1102 g_954 g_594 g_439 g_1246 g_1269 g_1109 g_1110 g_1111 g_746 g_1294 g_1307 g_1313 g_569 g_1296 g_1297 g_912
 * writes: g_52 g_31 g_224 g_131 g_261 g_505 g_101 g_594 g_244 g_128 g_665 g_96 g_91 g_225 g_282 g_1263 g_195 g_1112 g_33 g_439 g_569 g_140
 */
static const int64_t ** const  func_78(int64_t  p_79, int32_t * p_80)
{ /* block id: 11 */
    int32_t **l_945 = &g_261;
    int32_t *** const l_944 = &l_945;
    int32_t *** const *l_943[5][1][9] = {{{&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944}},{{&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944}},{{&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944}},{{&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944}},{{&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944,&l_944}}};
    int32_t *** const **l_942 = &l_943[4][0][1];
    int32_t *****l_946 = &g_153;
    const int64_t *l_961 = &g_4;
    const int64_t ** const l_960 = &l_961;
    int32_t l_990 = 0x87A1F6FEL;
    uint32_t l_997 = 0xA1AD9F22L;
    float ***l_1071 = &g_504;
    uint16_t *l_1196 = (void*)0;
    int32_t *l_1205[3];
    int16_t *l_1208 = &g_409;
    uint16_t l_1275 = 0x37F4L;
    uint16_t l_1312 = 4UL;
    int32_t *l_1321 = &g_439[5];
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1205[i] = &g_31[1];
    for (g_52 = 0; (g_52 <= 6); g_52 += 1)
    { /* block id: 14 */
        int32_t *l_82[9][3][6] = {{{&g_31[1],&g_31[1],&g_31[1],(void*)0,(void*)0,&g_31[1]},{&g_31[1],(void*)0,&g_31[1],(void*)0,&g_31[1],(void*)0},{&g_31[1],&g_31[1],(void*)0,(void*)0,&g_31[1],&g_31[1]}},{{&g_31[1],(void*)0,&g_31[1],&g_31[1],&g_31[1],&g_31[1]},{(void*)0,(void*)0,(void*)0,&g_31[1],&g_31[1],&g_31[1]},{(void*)0,&g_31[1],(void*)0,(void*)0,&g_31[1],(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_31[1],(void*)0,&g_31[1]},{&g_31[1],&g_31[1],(void*)0,&g_31[1],&g_31[1],&g_31[1]},{&g_31[1],&g_31[1],&g_31[1],&g_31[1],(void*)0,&g_31[1]}},{{&g_31[1],&g_31[1],(void*)0,&g_31[1],(void*)0,(void*)0},{(void*)0,(void*)0,&g_31[1],(void*)0,(void*)0,&g_31[1]},{(void*)0,&g_31[1],&g_31[1],&g_31[1],(void*)0,(void*)0}},{{(void*)0,&g_31[1],&g_31[1],&g_31[1],&g_31[1],(void*)0},{&g_31[1],&g_31[1],&g_31[1],(void*)0,(void*)0,&g_31[1]},{&g_31[1],(void*)0,&g_31[1],(void*)0,&g_31[1],(void*)0}},{{&g_31[1],&g_31[1],(void*)0,(void*)0,&g_31[1],&g_31[1]},{&g_31[1],(void*)0,&g_31[1],&g_31[1],&g_31[1],&g_31[1]},{(void*)0,(void*)0,(void*)0,&g_31[1],&g_31[1],&g_31[1]}},{{(void*)0,&g_31[1],(void*)0,(void*)0,&g_31[1],(void*)0},{(void*)0,(void*)0,(void*)0,&g_31[1],(void*)0,&g_31[1]},{&g_31[1],&g_31[1],(void*)0,&g_31[1],&g_31[1],&g_31[1]}},{{&g_31[1],&g_31[1],&g_31[1],&g_31[1],(void*)0,&g_31[1]},{&g_31[1],&g_31[1],(void*)0,&g_31[1],&g_31[1],&g_31[1]},{&g_31[1],(void*)0,&g_31[1],(void*)0,&g_31[1],&g_31[1]}},{{&g_31[1],&g_31[1],&g_31[1],&g_31[1],(void*)0,(void*)0},{(void*)0,&g_31[1],&g_31[1],&g_31[1],&g_31[1],(void*)0},{&g_31[1],&g_31[1],&g_31[1],&g_31[1],(void*)0,&g_31[1]}}};
        int32_t **l_81 = &l_82[4][1][2];
        uint8_t *l_84 = &g_85[6];
        uint16_t l_94 = 0UL;
        int64_t *l_949 = &g_4;
        int64_t **l_948 = &l_949;
        int64_t ***l_947 = &l_948;
        const int32_t *l_953 = &g_954;
        const int32_t **l_952 = &l_953;
        const int32_t **l_957 = &g_956[3];
        int i, j, k;
        (*l_81) = (void*)0;
    }
    if ((*p_80))
    { /* block id: 470 */
        const int64_t *l_959 = &g_33;
        const int64_t ** const l_958[2] = {&l_959,&l_959};
        int i;
        return g_962;
    }
    else
    { /* block id: 472 */
        int64_t l_963 = 9L;
        int32_t l_989 = 1L;
        const uint32_t l_991[10][2][10] = {{{0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L,0x41CEE4F4L,0xF9A7093EL,0xD4356B3DL,0UL,0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L},{0UL,1UL,0UL,0x41CEE4F4L,0UL,0UL,0x41CEE4F4L,0UL,1UL,0UL}},{{0UL,0xD4356B3DL,1UL,0xF9A7093EL,1UL,0xD4356B3DL,0UL,0UL,0xD4356B3DL,1UL},{0xD4356B3DL,0UL,0UL,0xD4356B3DL,1UL,0xF9A7093EL,1UL,0xD4356B3DL,0UL,0UL}},{{1UL,0UL,0x41CEE4F4L,0UL,0UL,0x41CEE4F4L,0UL,1UL,0UL,0x41CEE4F4L},{0xF9A7093EL,0xD4356B3DL,0UL,0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L,0x41CEE4F4L,0xF9A7093EL,0xD4356B3DL,0UL}},{{1UL,1UL,0UL,0xF9A7093EL,0xED1B4909L,0xF9A7093EL,0UL,1UL,1UL,0UL},{0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L,0x41CEE4F4L,0xF9A7093EL,0xD4356B3DL,0UL,0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L}},{{0UL,1UL,0UL,0x41CEE4F4L,0UL,0UL,0x41CEE4F4L,0UL,1UL,0UL},{0UL,0xD4356B3DL,1UL,0xF9A7093EL,1UL,0xD4356B3DL,0UL,0UL,0xD4356B3DL,1UL}},{{0xD4356B3DL,0UL,0UL,0xD4356B3DL,1UL,0xF9A7093EL,1UL,0xD4356B3DL,0UL,0UL},{1UL,0UL,0x41CEE4F4L,0UL,0UL,0x41CEE4F4L,0UL,1UL,0UL,0x41CEE4F4L}},{{0xF9A7093EL,0xD4356B3DL,0UL,0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L,0x41CEE4F4L,0xF9A7093EL,0xD4356B3DL,0UL},{1UL,1UL,0UL,0xF9A7093EL,0xED1B4909L,0xF9A7093EL,0UL,1UL,1UL,0UL}},{{0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L,0x41CEE4F4L,0xF9A7093EL,0xD4356B3DL,0UL,0xD4356B3DL,0xF9A7093EL,0x41CEE4F4L},{0UL,1UL,0UL,0x41CEE4F4L,0UL,0UL,0x41CEE4F4L,0UL,1UL,0UL}},{{0UL,0UL,0xED1B4909L,0x41CEE4F4L,0xED1B4909L,0UL,0xF9A7093EL,0xF9A7093EL,0UL,0xED1B4909L},{0UL,0xF9A7093EL,0xF9A7093EL,0UL,0xED1B4909L,0x41CEE4F4L,0xED1B4909L,0UL,0xF9A7093EL,0xF9A7093EL}},{{0xED1B4909L,0xF9A7093EL,0UL,1UL,1UL,0UL,0xF9A7093EL,0xED1B4909L,0xF9A7093EL,0UL},{0x41CEE4F4L,0UL,1UL,0UL,0x41CEE4F4L,0UL,0UL,0x41CEE4F4L,0UL,1UL}}};
        int32_t l_996 = (-6L);
        uint16_t * const l_1002 = &g_195;
        const int64_t ** const l_1008[4][9] = {{&l_961,(void*)0,&l_961,(void*)0,&l_961,&l_961,&l_961,&l_961,&l_961},{&l_961,&l_961,&l_961,&l_961,&l_961,&l_961,&l_961,&l_961,&l_961},{(void*)0,&l_961,&l_961,&l_961,(void*)0,(void*)0,&l_961,(void*)0,(void*)0},{&l_961,(void*)0,(void*)0,&l_961,&l_961,&l_961,&l_961,&l_961,(void*)0}};
        uint8_t l_1029[9] = {252UL,252UL,252UL,252UL,252UL,252UL,252UL,252UL,252UL};
        uint32_t *l_1075 = &g_665;
        int32_t l_1083 = 0x2CA5B3ECL;
        int32_t l_1250 = 0xA16E16CDL;
        uint8_t ** const *l_1262[2];
        uint8_t ** const **l_1261 = &l_1262[1];
        uint8_t ** const ***l_1260 = &l_1261;
        uint8_t ****l_1268 = &g_649;
        uint8_t *****l_1267 = &l_1268;
        uint64_t l_1281 = 0xF2FFA4DA0E542BCBLL;
        int32_t l_1316 = 1L;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1262[i] = &g_650;
        (***l_944) = l_963;
        if ((safe_lshift_func_int16_t_s_u((p_79 | (g_101[5][1] && (1UL ^ ((p_79 , ((-7L) <= ((safe_mod_func_uint64_t_u_u(l_963, (((safe_lshift_func_int16_t_s_u((safe_mod_func_int16_t_s_s((safe_add_func_uint16_t_u_u((***l_944), p_79)), g_85[6])), 4)) && (-2L)) && p_79))) > 0x6C9BL))) == g_225)))), g_85[6])))
        { /* block id: 474 */
            int16_t ***l_992 = (void*)0;
            int32_t l_994 = (-10L);
            int32_t l_995[5][6] = {{(-1L),1L,(-1L),(-1L),1L,(-1L)},{(-1L),1L,(-1L),(-1L),1L,(-1L)},{(-1L),1L,(-1L),(-1L),1L,(-1L)},{(-1L),1L,(-1L),(-1L),1L,(-1L)},{(-1L),1L,(-1L),(-1L),1L,(-1L)}};
            uint16_t *l_1003 = &g_195;
            uint16_t **l_1004 = &l_1003;
            int i, j;
            for (g_224 = 0; (g_224 <= 3); g_224 += 1)
            { /* block id: 477 */
                float *l_980 = &g_221;
                int16_t *l_988 = (void*)0;
                int16_t ****l_993 = &l_992;
                int i;
                (*g_261) = (*p_80);
                (**l_944) = ((***g_153) = (*g_130));
                (*g_100) = (((((safe_add_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s((((((((*g_504) = (*g_504)) == ((safe_mul_func_uint8_t_u_u(g_140, (p_79 , 0xB2L))) , l_980)) && (safe_lshift_func_int16_t_s_s((l_989 = (safe_div_func_int32_t_s_s(l_963, (safe_lshift_func_int8_t_s_u(g_33, (((safe_unary_minus_func_uint8_t_u(((((void*)0 == &g_635[1]) <= l_963) & 0UL))) , l_963) & (*****l_946))))))), 14))) , 4294967295UL) < g_333) , p_79), l_990)), l_963)) , p_79) ^ l_991[5][0][6]) , l_991[4][1][3]) > l_963);
                (*l_993) = l_992;
            }
            l_997--;
            (****g_153) = ((****g_153) < (l_1002 == ((*l_1004) = l_1003)));
        }
        else
        { /* block id: 489 */
            float **l_1005 = &g_505;
            const uint16_t *l_1020 = &g_195;
            const uint16_t **l_1019 = &l_1020;
            int32_t l_1023 = 8L;
            int32_t *l_1025[10];
            int64_t *l_1028 = &g_569;
            int8_t l_1072 = 0x3FL;
            int32_t l_1082 = 0x261D89D9L;
            int32_t l_1084 = 0x97C85779L;
            int32_t l_1085 = (-1L);
            int32_t l_1087[4][6] = {{(-3L),0xC0C7B055L,0xC0C7B055L,(-3L),(-4L),0xC820B76FL},{0xC820B76FL,(-3L),0xBF792DCEL,(-3L),0xC820B76FL,(-6L)},{(-3L),0xC820B76FL,(-6L),(-6L),0xC820B76FL,(-3L)},{0xC0C7B055L,(-3L),(-4L),0xC820B76FL,(-4L),(-3L)}};
            float l_1247[5][2] = {{0x5.Dp-1,0x5.Dp-1},{0x5.Dp-1,0x5.Dp-1},{0x5.Dp-1,0x5.Dp-1},{0x5.Dp-1,0x5.Dp-1},{0x5.Dp-1,0x5.Dp-1}};
            uint32_t l_1248[4] = {0x6B81CAF0L,0x6B81CAF0L,0x6B81CAF0L,0x6B81CAF0L};
            uint8_t ****l_1266 = &g_649;
            uint8_t *****l_1265 = &l_1266;
            uint32_t l_1337[8] = {4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL};
            int16_t *l_1349 = &g_140;
            uint64_t l_1353 = 18446744073709551612UL;
            int i, j;
            for (i = 0; i < 10; i++)
                l_1025[i] = &g_282;
            for (g_52 = 0; (g_52 <= 0); g_52 += 1)
            { /* block id: 492 */
                const int64_t ** const l_1009 = (void*)0;
                int32_t *l_1011 = &g_224;
                int32_t l_1031 = 0x5D3D0B6EL;
                uint16_t *l_1076 = &g_195;
                uint8_t *** const *l_1156[3][8][9] = {{{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649}},{{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649}},{{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649},{&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649},{(void*)0,(void*)0,&g_649,&g_649,(void*)0,&g_649,&g_649,&g_649,&g_649},{&g_649,&g_649,&g_649,(void*)0,&g_649,&g_649,(void*)0,(void*)0,&g_649}}};
                uint64_t *l_1164 = &g_96;
                uint64_t *l_1165 = &g_91;
                int32_t **l_1224[9][5] = {{&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1],&l_1205[1]}};
                int32_t l_1238 = (-1L);
                const int64_t l_1243 = 1L;
                uint32_t l_1249[10] = {0xE9986912L,18446744073709551615UL,0xE9986912L,18446744073709551615UL,0xE9986912L,18446744073709551615UL,0xE9986912L,18446744073709551615UL,0xE9986912L,18446744073709551615UL};
                int i, j, k;
                for (g_594 = 0; (g_594 <= 0); g_594 += 1)
                { /* block id: 495 */
                    float **l_1006 = &g_505;
                    const int64_t ** const l_1007 = &l_961;
                    int32_t l_1010 = 0xD4061613L;
                    uint16_t *l_1018 = &g_195;
                    uint16_t **l_1017[6][8][1] = {{{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018}},{{(void*)0},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018}},{{&l_1018},{&l_1018},{(void*)0},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018}},{{&l_1018},{&l_1018},{&l_1018},{&l_1018},{(void*)0},{&l_1018},{&l_1018},{&l_1018}},{{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{(void*)0},{&l_1018}},{{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018},{&l_1018}}};
                    int32_t l_1024[2][8] = {{0L,(-3L),(-3L),0L,(-3L),(-3L),0L,(-3L)},{0L,0L,(-1L),0L,0L,(-1L),0L,0L}};
                    uint32_t *l_1074[3][1][8] = {{{&g_665,(void*)0,&g_665,(void*)0,&g_665,&g_665,(void*)0,&g_665}},{{&g_665,&g_665,(void*)0,&g_665,(void*)0,&g_665,&g_665,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_665,(void*)0,(void*)0,(void*)0}}};
                    uint32_t **l_1073[6] = {&l_1074[2][0][4],&l_1074[2][0][4],&l_1074[2][0][4],&l_1074[2][0][4],&l_1074[2][0][4],&l_1074[2][0][4]};
                    int16_t **l_1079 = (void*)0;
                    int16_t ***l_1078 = &l_1079;
                    int16_t ****l_1077 = &l_1078;
                    uint64_t *l_1081 = (void*)0;
                    int i, j, k;
                    if ((((((void*)0 == (*g_649)) >= 3L) , l_1005) == l_1006))
                    { /* block id: 496 */
                        return l_1009;
                    }
                    else
                    { /* block id: 498 */
                        int32_t *l_1013 = &l_996;
                        int32_t **l_1014 = &l_1013;
                        int32_t l_1030[4] = {0xE1EAB4C4L,0xE1EAB4C4L,0xE1EAB4C4L,0xE1EAB4C4L};
                        int i;
                        if (l_1010)
                            break;
                        (****l_946) = l_1011;
                        l_1031 ^= ((+(((*l_1014) = l_1013) == ((safe_div_func_int8_t_s_s(((g_85[8] | ((l_1017[5][5][0] != l_1019) <= ((((safe_mul_func_int8_t_s_s(((l_1024[1][7] = l_1023) ^ ((((void*)0 == l_1025[7]) , (safe_add_func_int32_t_s_s((((g_244[(g_52 + 5)] = &l_963) != ((((*g_505) = l_963) >= (-0x9.Ep+1)) , l_1028)) != l_1029[4]), (*****g_152)))) > l_1023)), l_963)) | (*****l_946)) < l_1030[2]) != g_409))) <= (*l_1011)), p_79)) , p_80))) != (*l_1011));
                        if (l_1023)
                            continue;
                    }
                }
                if ((*p_80))
                    break;
                (***l_1071) = (((*l_1165) = ((*l_1164) = ((safe_sub_func_uint32_t_u_u(g_4, (((*l_1075)--) ^ g_33))) <= ((*p_80) < (safe_lshift_func_uint8_t_u_u(p_79, 1)))))) , 0x0.B4AEC0p+61);
                for (l_1083 = 0; (l_1083 >= 0); l_1083 -= 1)
                { /* block id: 551 */
                    int8_t l_1203 = 1L;
                    int32_t **l_1223[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1223[i] = &l_1205[0];
                    for (l_1023 = 0; (l_1023 >= 0); l_1023 -= 1)
                    { /* block id: 554 */
                        uint16_t **l_1195[3][3][8] = {{{(void*)0,&l_1076,&l_1076,(void*)0,(void*)0,(void*)0,&l_1076,&l_1076},{(void*)0,(void*)0,&l_1076,&l_1076,(void*)0,&l_1076,&l_1076,(void*)0},{(void*)0,&l_1076,&l_1076,(void*)0,(void*)0,(void*)0,&l_1076,&l_1076}},{{(void*)0,(void*)0,&l_1076,&l_1076,(void*)0,&l_1076,&l_1076,(void*)0},{(void*)0,&l_1076,&l_1076,(void*)0,(void*)0,(void*)0,&l_1076,&l_1076},{(void*)0,(void*)0,&l_1076,&l_1076,(void*)0,&l_1076,&l_1076,(void*)0}},{{(void*)0,&l_1076,&l_1076,(void*)0,(void*)0,(void*)0,&l_1076,&l_1076},{(void*)0,(void*)0,&l_1076,&l_1076,(void*)0,&l_1076,&l_1076,(void*)0},{(void*)0,&l_1076,&l_1076,(void*)0,(void*)0,(void*)0,&l_1076,&l_1076}}};
                        const uint64_t l_1204 = 0x1EBBB1F66F9F5F7FLL;
                        int32_t l_1206 = 0xC23FF189L;
                        int16_t *l_1207 = (void*)0;
                        int8_t *l_1244 = &l_1072;
                        uint32_t *l_1245[9][10][2] = {{{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246}},{{&g_1246,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{(void*)0,&g_1246}},{{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246}},{{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246}},{{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{(void*)0,&g_1246}},{{&g_1246,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,(void*)0}},{{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246}},{{&g_1246,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,&g_1246},{(void*)0,&g_1246}},{{&g_1246,&g_1246},{(void*)0,&g_1246},{&g_1246,(void*)0},{(void*)0,(void*)0},{&g_1246,&g_1246},{&g_1246,&g_1246},{&g_1246,(void*)0},{(void*)0,(void*)0},{&g_1246,&g_1246},{(void*)0,&g_1246}}};
                        uint8_t l_1251 = 1UL;
                        int i, j, k;
                        l_1031 ^= (+(safe_rshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_u(((safe_div_func_int32_t_s_s((safe_sub_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_add_func_int32_t_s_s((safe_sub_func_uint8_t_u_u((((((*g_1102) = (safe_mul_func_uint16_t_u_u((((l_1082 && (*p_80)) & ((*l_1164)++)) < (safe_lshift_func_int8_t_s_u(((((((safe_mul_func_uint8_t_u_u(((p_79 == ((((((0x7.28FFDDp+93 != ((safe_div_func_float_f_f((((safe_add_func_float_f_f((((((((safe_div_func_float_f_f(((l_1196 = &g_195) != &g_195), (safe_mul_func_float_f_f((safe_div_func_float_f_f((((((*l_944) = (p_79 , (***g_152))) != ((((safe_lshift_func_int16_t_s_s(p_79, 14)) && l_1072) & l_1203) , &p_80)) == (**g_504)) > p_79), p_79)), l_963)))) < l_1204) > 0xA.E8D0B5p+67) != p_79) >= (*g_505)) > l_996) <= (*g_505)), l_1023)) == 0xB.AA690Ep+36) >= 0x7.C2477Dp-50), (*g_505))) <= 0x1.0766D8p-11)) > l_1084) , l_1205[1]) == (****g_347)) >= 0L) & p_79)) < (*l_1011)), l_1206)) < p_79) != g_282) <= p_79) & p_79) && l_991[5][0][6]), l_1203))), l_989))) , l_1207) == l_1208) >= g_954), p_79)), (*p_80))), g_594)), p_79)), (-10L))) <= l_996), p_79)), l_1203)));
                        (*g_100) = (safe_mod_func_uint64_t_u_u((~((~((-1L) == (safe_sub_func_uint64_t_u_u((((((*l_1011) = ((safe_mul_func_int8_t_s_s((*l_1011), (safe_rshift_func_uint8_t_u_s(((g_282 = (safe_rshift_func_int8_t_s_u(((safe_mul_func_uint8_t_u_u(l_1203, (((((l_1224[3][0] = l_1223[0]) == &p_80) && ((safe_lshift_func_int16_t_s_s((((((((safe_div_func_uint16_t_u_u((((+(safe_mul_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((safe_sub_func_int64_t_s_s((safe_div_func_uint32_t_u_u((l_1085 |= (p_79 & (((((((*l_1075) = (l_1238 >= ((~((*g_261) = (safe_unary_minus_func_int64_t_s((-4L))))) || ((*l_1244) &= (safe_sub_func_uint64_t_u_u(l_1243, p_79)))))) | 0x5480614AL) < l_1084) != p_79) <= l_1087[0][5]) == p_79))), g_439[0])), 18446744073709551608UL)), 2)), p_79))) != 0xE6L) , g_101[5][1]), l_1023)) || 0xED21B353L) ^ p_79) < l_1082) <= l_1029[4]) < l_1248[2]) , p_79), 11)) & 0x32L)) >= 0x0889L) , g_1246))) && p_79), 2))) , p_79), l_1087[0][5])))) != l_1249[5])) <= l_1250) , p_79) & l_1251), 18446744073709551615UL)))) > p_79)), p_79));
                        (***g_1109) = (((***l_1071) = (-0x1.1p-1)) <= (safe_sub_func_float_f_f(p_79, (((safe_mul_func_uint8_t_u_u(l_1029[4], ((safe_mod_func_uint8_t_u_u((l_989 = (l_1087[2][2] , (p_79 , ((l_963 > (((g_1263 = l_1260) != (l_1267 = l_1265)) >= ((*l_1076) = (g_1269 != (void*)0)))) > p_79)))), 9UL)) & 0xA334L))) , 0x8.5p+1) <= (-0x1.7p+1)))));
                        if ((*g_746))
                            continue;
                    }
                }
            }
            (*g_100) ^= (safe_lshift_func_int8_t_s_u(((*p_80) & (safe_unary_minus_func_int16_t_s(p_79))), (((l_1083 , (l_1275 && 0x2EL)) > (l_1023 &= (p_79 | p_79))) > ((+(((safe_add_func_int16_t_s_s(p_79, p_79)) >= 0x6E7BEE5BE441DE01LL) ^ (***l_944))) == l_989))));
            for (g_33 = 0; (g_33 == (-12)); g_33--)
            { /* block id: 582 */
                int32_t l_1304 = 9L;
                int16_t * const *l_1335 = &l_1208;
                int16_t * const **l_1334 = &l_1335;
                int16_t **l_1350 = &l_1349;
                int8_t *l_1356 = &l_1072;
                uint64_t *l_1357 = &g_91;
                if (l_1281)
                { /* block id: 583 */
                    int64_t l_1282 = (-7L);
                    int64_t l_1283 = 0xBCD97A17E5B54B00LL;
                    int32_t l_1284 = 0x9BD70304L;
                    uint8_t l_1286 = 0UL;
                    int8_t l_1315[5][9][5] = {{{0L,1L,0x13L,0x7BL,0x7BL},{0x0CL,0x41L,0x0CL,1L,0xB4L},{0x7BL,0x0EL,1L,0x9DL,(-4L)},{0xFDL,0L,0x7BL,1L,0xBBL},{(-1L),0xCCL,1L,(-4L),0xCCL},{0x75L,1L,0x0CL,0x20L,8L},{1L,(-8L),0x13L,(-8L),1L},{6L,0xFDL,(-6L),8L,0x8BL},{0xFBL,0x0EL,0x3EL,0xFBL,1L}},{{0x20L,0xB4L,0xCCL,0xFDL,0x8BL},{(-1L),0xFBL,0x33L,1L,1L},{0x8BL,0x64L,8L,0L,8L},{0xCBL,0xCBL,(-1L),0L,0xCCL},{0x0CL,0xBBL,6L,8L,0xBBL},{(-8L),0x3EL,2L,0L,(-4L)},{1L,0xBBL,0xCCL,0x75L,0xB4L},{0x33L,0xCBL,0x0EL,(-4L),0x7BL},{0x41L,0x64L,0x64L,0x41L,1L}},{{0xCCL,0xFBL,0x13L,0xCBL,0x9DL},{0x0CL,0xB4L,0x4BL,1L,1L},{0x9DL,0x0EL,3L,0xCBL,(-4L)},{0L,0xFDL,0x7BL,0x41L,0x20L},{(-1L),(-8L),(-4L),(-4L),(-8L)},{0xBBL,1L,6L,0x75L,8L},{0L,0xCCL,0x13L,0L,0xFBL},{6L,0L,1L,8L,0xFDL},{0L,0x0EL,0x6AL,0L,1L}},{{0xBBL,0x41L,0xCCL,0L,0L},{(-1L),1L,(-1L),1L,0L},{0L,0x64L,1L,0xFDL,8L},{0x9DL,0x7BL,(-1L),0xFBL,0L},{0x0CL,0x75L,1L,8L,0x75L},{0xCCL,0x3EL,(-1L),(-8L),(-4L)},{0x41L,0x20L,0xCCL,0x20L,0x41L},{0x33L,0x9DL,(-1L),0xA0L,2L},{1L,1L,0xCCL,1L,0L}},{{3L,0x0EL,0x9DL,(-1L),2L},{8L,1L,(-1L),0L,(-6L)},{2L,1L,0xA0L,0x33L,0xA0L},{0x4BL,0x4BL,0L,0x64L,1L},{0L,(-4L),0x69L,0xA0L,(-4L)},{6L,0xCCL,0xE8L,8L,0L},{0x3EL,(-4L),0x9DL,1L,0x0EL},{(-1L),0x4BL,1L,0L,6L},{0x6AL,1L,1L,0x6AL,(-1L)}}};
                    const int64_t ** const l_1318 = &l_961;
                    int i, j, k;
                    l_1282 = p_79;
                    if ((*p_80))
                    { /* block id: 585 */
                        l_1283 = (*****g_347);
                    }
                    else
                    { /* block id: 587 */
                        int8_t *l_1293 = &g_1285;
                        int8_t **l_1292 = &l_1293;
                        int8_t ***l_1291 = &l_1292;
                        const int32_t l_1314 = 0x7AA8523AL;
                        ++l_1286;
                        if (l_1248[2])
                            continue;
                        (***l_944) = (safe_mul_func_int16_t_s_s((l_1291 == g_1294), ((*l_1002) = (safe_div_func_int32_t_s_s(0xC7F1E0B7L, ((*g_100) | p_79))))));
                        l_1316 = (l_1084 | ((l_1087[2][3] = (safe_mod_func_int8_t_s_s(p_79, p_79))) >= (((*****l_946) |= (((*l_1028) |= ((l_1315[1][7][3] = ((safe_sub_func_uint16_t_u_u(0x743AL, (((l_1304 > ((((8L <= (safe_div_func_int8_t_s_s((g_1307 <= (((safe_div_func_int16_t_s_s(l_1284, (safe_sub_func_int32_t_s_s((251UL >= l_1281), (*p_80))))) ^ p_79) && 0x37FD5226F0F64D86LL)), 0x53L))) && p_79) & p_79) , l_1312)) ^ 0x39CC65FEL) == g_1313))) || l_1314)) != l_1304)) , (-4L))) && l_1314)));
                    }
                    if ((*p_80))
                        continue;
                    if (((*g_261) = 0xC42F5E0AL))
                    { /* block id: 600 */
                        const int64_t ** const l_1317[3] = {&l_961,&l_961,&l_961};
                        int i;
                        return g_962;
                    }
                    else
                    { /* block id: 602 */
                        if ((*g_746))
                            break;
                    }
                }
                else
                { /* block id: 605 */
                    int32_t l_1323 = 0x5882367DL;
                    int16_t **l_1332 = &l_1208;
                    int16_t ***l_1331 = &l_1332;
                    int16_t ***l_1333 = &l_1332;
                    int32_t l_1336 = (-5L);
                    if ((**l_945))
                        break;
                    for (g_140 = 0; (g_140 < 20); g_140 = safe_add_func_uint16_t_u_u(g_140, 4))
                    { /* block id: 609 */
                        uint8_t l_1322 = 0xC4L;
                        (**l_944) = l_1321;
                        l_1323 = l_1322;
                        l_1316 |= (l_1336 |= ((*p_80) ^ (safe_sub_func_int8_t_s_s((+(safe_add_func_int32_t_s_s((0x3202C066L && ((safe_mod_func_int16_t_s_s(p_79, 0xECF5L)) ^ ((l_1333 = l_1331) != l_1334))), (***l_944)))), l_1323))));
                    }
                }
                l_1082 |= ((l_1337[5]--) || (safe_mul_func_uint8_t_u_u((p_79 & ((((*l_1002) = (+((safe_div_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(((*l_1028) = (((*l_1350) = l_1349) != (*l_1335))), (safe_add_func_int32_t_s_s(((***l_944) = l_1353), (((*l_1357) = (safe_mul_func_int8_t_s_s(((*l_1356) = 0xA4L), l_1087[0][5]))) || (!(safe_add_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u(((((p_79 <= (((1UL <= (((0xBCL >= p_79) || (*p_80)) , p_79)) || p_79) , p_79)) <= 0xC5699C7DL) | 1UL) | l_1083), l_1316)), (*g_1296))), p_79)))))))), g_912[4][0][1])) | 255UL), p_79)) & p_79))) >= l_1029[4]) == p_79)), 0x67L)));
                if (l_1304)
                    continue;
            }
        }
    }
    for (g_282 = 0; (g_282 == (-18)); g_282--)
    { /* block id: 631 */
        uint8_t ***l_1372 = (void*)0;
        int32_t l_1373[3][5] = {{0xD28D5097L,0x2F63C37FL,0xD660E3FDL,0x2F63C37FL,0xD28D5097L},{0x2CCF884AL,0x2F63C37FL,0xA3E1B454L,0xD28D5097L,0xA3E1B454L},{0xA3E1B454L,0xA3E1B454L,0xD660E3FDL,0xD28D5097L,(-1L)}};
        int64_t l_1374 = 0L;
        uint8_t *l_1375[3][6] = {{&g_85[6],&g_85[6],&g_52,&g_52,&g_85[6],&g_85[6]},{&g_85[6],&g_52,&g_52,&g_85[6],&g_85[6],&g_52},{&g_85[6],&g_85[6],&g_52,&g_52,&g_85[6],&g_85[6]}};
        int64_t l_1376 = 0x0494B90052A9FE85LL;
        uint64_t l_1377 = 0xE793345236B03A39LL;
        int i, j;
        l_1373[0][1] &= ((*****g_347) = (safe_mul_func_uint8_t_u_u((!(safe_mod_func_int64_t_s_s(((void*)0 == l_1372), 0x8BD29BFE02D48EB6LL))), (--l_1377))));
        return g_962;
    }
    return g_962;
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_101 g_33 g_91 g_52 g_129 g_130 g_131 g_195 g_85 g_4 g_152 g_153 g_140 g_96 g_243 g_225 g_282 g_333 g_224 g_347 g_409 g_439 g_500 g_503 g_505 g_569 g_627 g_649 g_665 g_244 g_636 g_746 g_754 g_128 g_650 g_928
 * writes: g_100 g_101 g_33 g_31 g_96 g_91 g_195 g_131 g_128 g_221 g_223 g_224 g_225 g_261 g_282 g_85 g_140 g_333 g_347 g_397 g_409 g_439 g_504 g_129 g_244 g_569 g_153 g_635 g_649 g_665
 */
static const uint8_t  func_86(uint64_t  p_87, uint64_t  p_88, uint64_t  p_89)
{ /* block id: 21 */
    uint16_t l_99 = 0x2837L;
    int64_t *l_544 = &g_33;
    int64_t **l_545 = &g_244[0];
    int32_t ****l_548 = &g_129[1];
    int32_t l_613 = 5L;
    int32_t l_628 = 0x6EF7D80EL;
    const int16_t *l_637 = (void*)0;
    int32_t l_666 = 0L;
    int8_t l_785 = 0xBBL;
    uint64_t l_791[5][5] = {{7UL,0x1896F860E5B62D30LL,7UL,0xA81540891377F51DLL,0xA81540891377F51DLL},{0x6F94093A0571E6D7LL,3UL,0x6F94093A0571E6D7LL,18446744073709551606UL,18446744073709551606UL},{7UL,0x1896F860E5B62D30LL,7UL,0xA81540891377F51DLL,0xA81540891377F51DLL},{0x6F94093A0571E6D7LL,3UL,0x6F94093A0571E6D7LL,18446744073709551606UL,18446744073709551606UL},{7UL,0x1896F860E5B62D30LL,7UL,0xA81540891377F51DLL,0xA81540891377F51DLL}};
    uint32_t *l_812 = (void*)0;
    uint32_t l_814 = 0x2FA694F0L;
    int32_t *l_852 = &g_224;
    uint8_t l_899 = 7UL;
    int32_t *l_901 = &g_31[1];
    uint8_t ****l_923 = &g_649;
    int64_t * const *l_939 = &l_544;
    int64_t * const **l_938 = &l_939;
    int i, j;
    if (((func_97(l_99) , l_544) != ((*l_545) = l_544)))
    { /* block id: 264 */
        int32_t ****l_549 = &g_129[1];
        uint8_t ****l_651[10] = {&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649,&g_649};
        uint32_t *l_662 = (void*)0;
        int32_t l_663 = 1L;
        uint32_t *l_664 = &g_665;
        const int32_t l_772 = 0L;
        int i;
        for (g_225 = 20; (g_225 == 12); --g_225)
        { /* block id: 267 */
            uint8_t l_564 = 0UL;
            int32_t l_567 = 0xB3DFD3F3L;
            float l_605[8][1];
            const int16_t *l_633 = &g_409;
            int i, j;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 1; j++)
                    l_605[i][j] = 0x9.8p-1;
            }
            (*g_505) = (l_548 == l_549);
            for (p_88 = 5; (p_88 != 1); p_88--)
            { /* block id: 271 */
                uint16_t *l_570 = &l_99;
                int32_t l_571 = 0L;
                int8_t l_577 = 3L;
                uint32_t *l_593 = &g_594;
                uint32_t l_606 = 0x4E430FA7L;
                uint32_t l_616 = 0xFC579BE2L;
                int32_t *l_642[8] = {&l_567,&l_571,&l_567,&l_571,&l_567,&l_571,&l_567,&l_571};
                int i;
                for (g_33 = 0; (g_33 != 0); g_33 = safe_add_func_uint16_t_u_u(g_33, 9))
                { /* block id: 274 */
                    int32_t *l_565 = &g_439[2];
                    int16_t *l_566[3][2] = {{&g_140,&g_140},{&g_140,&g_140},{&g_140,&g_140}};
                    int64_t *l_568 = &g_569;
                    int64_t l_572 = 0x6FDE1AA9ABAD372BLL;
                    uint32_t *l_595 = &g_594;
                    int i, j;
                    if ((((safe_lshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((((((safe_mod_func_uint64_t_u_u(g_52, (safe_mul_func_uint16_t_u_u(((safe_div_func_uint64_t_u_u((p_87 , (((*l_568) = ((((((l_564 & p_89) , ((*l_565) = p_87)) ^ (((p_87 | p_87) < 4UL) ^ (l_567 = ((g_33 , 0xFB78D9F6A1AED2F8LL) > 0x9718CF2BC24AE679LL)))) ^ g_225) > g_333) || p_87)) >= 7UL)), p_87)) | l_564), 65535UL)))) , (void*)0) == l_570) ^ 0xCAE00610L) , 0x681FL), l_571)), 2)) < 0UL) >= l_572))
                    { /* block id: 278 */
                        uint32_t *l_592 = &g_225;
                        uint32_t **l_591[8] = {&l_592,&l_592,&l_592,&l_592,&l_592,&l_592,&l_592,&l_592};
                        int8_t *l_600 = (void*)0;
                        int8_t *l_601 = (void*)0;
                        int32_t l_602[7] = {0xA3E21C75L,0xA3E21C75L,0L,0xA3E21C75L,0xA3E21C75L,0L,0xA3E21C75L};
                        uint16_t *l_603 = &g_195;
                        int32_t l_604 = 0x45210291L;
                        int16_t l_626 = 0x08BAL;
                        int32_t ****l_629 = &g_129[0];
                        int32_t *****l_630 = &g_153;
                        int i;
                        (*g_130) = (((safe_add_func_uint64_t_u_u((safe_mod_func_int16_t_s_s((l_577 <= (safe_div_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_u((p_87 , ((safe_unary_minus_func_int8_t_s(g_101[5][1])) & (!p_87))), 3)) & (((((safe_mod_func_int64_t_s_s(p_88, ((*l_568) ^= ((((safe_add_func_uint8_t_u_u(0x13L, (+(safe_mod_func_uint16_t_u_u(((*l_603) |= ((l_595 = (l_593 = &g_225)) == (((safe_sub_func_uint8_t_u_u(0x84L, (((*l_570) = (safe_sub_func_int8_t_s_s((l_602[2] |= p_87), p_87))) >= 0UL))) == p_87) , &g_225))), g_33))))) , l_602[1]) & p_89) | p_89)))) ^ 0xDC1AL) != l_604) , l_600) == &g_85[6])), p_89))), 0x5B3CL)), l_606)) , 18446744073709551615UL) , (void*)0);
                        if (p_89)
                            break;
                        l_628 &= (p_89 != (~((safe_div_func_uint16_t_u_u(((((((((p_88 , (safe_unary_minus_func_uint16_t_u(((*l_570) &= (safe_rshift_func_int16_t_s_s(p_87, ((((l_613 > (safe_sub_func_uint64_t_u_u(((void*)0 == &l_577), (p_88 & (l_616 , (safe_sub_func_uint64_t_u_u((+(((safe_div_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((safe_div_func_int8_t_s_s(0xD8L, p_89)), p_89)), (-1L))) , 4L) || g_85[6])), p_88))))))) & l_567) <= l_626) <= g_91))))))) | p_89) && p_88) , p_89) ^ g_627) <= 6UL) && p_87) != (-3L)), (*l_565))) != l_626)));
                        (*l_630) = l_629;
                    }
                    else
                    { /* block id: 290 */
                        const int16_t **l_634[4] = {&l_633,&l_633,&l_633,&l_633};
                        int32_t l_640 = (-8L);
                        int32_t l_641 = 0x951B8FC7L;
                        int i;
                        l_641 &= (((safe_mod_func_int64_t_s_s(p_87, 1UL)) > (((l_637 = (g_635[2] = l_633)) != ((safe_rshift_func_uint16_t_u_u(p_87, 2)) , &g_636)) , l_640)) < p_89);
                    }
                    l_642[0] = l_565;
                    if (p_89)
                        continue;
                }
            }
        }
        l_666 |= (safe_mod_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(((*l_664) = (safe_rshift_func_int8_t_s_u(((g_649 = g_649) == (void*)0), (g_85[6] > (((p_88 && (((safe_add_func_int16_t_s_s(l_628, ((safe_sub_func_uint16_t_u_u(((((*g_505) = p_89) > (safe_add_func_float_f_f((((((safe_add_func_int16_t_s_s(l_613, p_88)) != (safe_lshift_func_uint8_t_u_s(((void*)0 == l_662), g_85[8]))) , p_88) , p_89) , 0x1.Dp-1), (-0x1.5p+1)))) , g_569), 0x4F27L)) != p_88))) , l_637) == l_637)) | l_663) == p_88))))), p_88)), g_85[6]));
        for (g_569 = 0; (g_569 <= 0); g_569 += 1)
        { /* block id: 306 */
            return p_89;
        }
        for (p_88 = 0; (p_88 > 8); p_88 = safe_add_func_uint16_t_u_u(p_88, 5))
        { /* block id: 311 */
            uint16_t l_691 = 0x39FDL;
            int32_t l_706 = 0x74400B5FL;
            int32_t l_788 = 0xDD9AEDD3L;
            int32_t *l_789 = &l_666;
            for (l_628 = (-4); (l_628 < 13); l_628 = safe_add_func_int64_t_s_s(l_628, 9))
            { /* block id: 314 */
                uint8_t l_699 = 4UL;
                float **l_759[10] = {&g_505,&g_505,&g_505,&g_505,&g_505,&g_505,&g_505,&g_505,&g_505,&g_505};
                int16_t * const * const l_762 = (void*)0;
                int32_t l_763 = 0x72D260B0L;
                int32_t l_766[10][3][4] = {{{0x859B1A19L,0x0867A904L,0L,6L},{3L,7L,6L,0L},{0x9D89B9B7L,7L,0x9D89B9B7L,6L}},{{7L,0x0867A904L,1L,0x3E862A48L},{0x3E862A48L,0L,3L,0x0867A904L},{1L,3L,3L,1L}},{{6L,(-1L),6L,0x0867A904L},{0x3E862A48L,1L,0x0867A904L,7L},{0x0867A904L,7L,0x859B1A19L,7L}},{{1L,1L,(-1L),0x0867A904L},{0x9D89B9B7L,(-1L),7L,0L},{(-1L),1L,3L,3L}},{{(-1L),(-1L),7L,6L},{0x9D89B9B7L,3L,(-1L),(-1L)},{1L,0x3E862A48L,0x859B1A19L,(-1L)}},{{0x0867A904L,0x3E862A48L,0x0867A904L,(-1L)},{0x3E862A48L,3L,6L,6L},{6L,(-1L),1L,3L}},{{0L,1L,1L,0L},{6L,(-1L),6L,0x0867A904L},{0x3E862A48L,1L,0x0867A904L,7L}},{{0x0867A904L,7L,0x859B1A19L,7L},{1L,1L,(-1L),0x0867A904L},{0x9D89B9B7L,(-1L),7L,0L}},{{(-1L),1L,3L,3L},{(-1L),(-1L),7L,6L},{0x9D89B9B7L,3L,(-1L),(-1L)}},{{1L,0x3E862A48L,0x859B1A19L,(-1L)},{0x0867A904L,0x3E862A48L,0x0867A904L,(-1L)},{0x3E862A48L,3L,6L,6L}}};
                int32_t l_784 = 0xEF7822B4L;
                int i, j, k;
                for (l_99 = 0; (l_99 < 5); ++l_99)
                { /* block id: 317 */
                    float *l_686 = &g_221;
                    int32_t l_692 = 0xE59D8017L;
                    int8_t *l_693 = &g_333;
                    int32_t l_694 = 0x8BD77C6BL;
                    int16_t *l_695[3];
                    int32_t *l_696 = &g_439[2];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_695[i] = (void*)0;
                    (*l_696) = ((p_88 != ((*l_664) |= (~((p_87 , (g_140 = (safe_add_func_int32_t_s_s((l_694 |= (safe_add_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s(((*l_693) = (safe_div_func_int32_t_s_s((0xB8429671189AF48ALL <= ((*l_544) = (p_87 <= (safe_lshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_u((l_686 == (((((*g_505) = p_87) <= (safe_mul_func_float_f_f(p_87, (safe_add_func_float_f_f((p_87 != (0x7.7p-1 <= 0x1.9p+1)), 0x6.B3AE59p+44))))) < (-0x1.Ep+1)) , (void*)0)), l_691)), p_87))))), l_692))), p_88)), p_87))), p_88)))) ^ 65535UL)))) != p_88);
                    (*g_505) = (safe_sub_func_float_f_f(((l_699 = ((void*)0 == &g_153)) < (safe_sub_func_float_f_f(p_89, (safe_sub_func_float_f_f(p_87, (p_87 >= ((0xCD8CL <= (safe_rshift_func_int8_t_s_s(0xABL, 6))) , (((void*)0 != (*g_243)) < 0x3.931E08p-72)))))))), p_88));
                    if (l_706)
                        break;
                }
                for (g_224 = 0; (g_224 <= 4); g_224 = safe_add_func_uint16_t_u_u(g_224, 8))
                { /* block id: 331 */
                    uint64_t l_730 = 0UL;
                    uint64_t l_744 = 18446744073709551608UL;
                    int8_t *l_758 = &g_333;
                    int8_t ** const l_757 = &l_758;
                    int32_t l_764[5];
                    int32_t **l_769[1];
                    int64_t l_782 = 0x15B0296603566077LL;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_764[i] = (-1L);
                    for (i = 0; i < 1; i++)
                        l_769[i] = &g_131[0];
                    for (p_87 = 0; (p_87 > 45); p_87++)
                    { /* block id: 334 */
                        int32_t l_742[3];
                        int64_t l_743 = 0x4BA2C5623B028E6ELL;
                        uint64_t *l_745[4][7][1] = {{{&l_744},{&g_96},{&g_96},{&g_96},{&l_744},{&g_96},{&g_96}},{{&g_96},{&l_744},{&g_91},{&g_91},{&g_91},{&g_91},{&l_744}},{{&g_96},{&g_96},{&g_96},{&l_744},{&g_96},{&g_96},{&g_96}},{{&l_744},{&g_91},{&g_91},{&g_91},{&g_91},{&l_744},{&g_96}}};
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_742[i] = 9L;
                        if (l_699)
                            break;
                        (*g_746) = ((p_89 && (((p_89 = ((((safe_div_func_int8_t_s_s((((safe_div_func_int64_t_s_s(l_613, p_87)) , p_89) , (safe_mod_func_int16_t_s_s((l_699 || (safe_sub_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(((*l_664) = (+(safe_add_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_s(l_730, ((safe_add_func_int8_t_s_s(l_699, (!(safe_rshift_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((safe_add_func_uint16_t_u_u((safe_add_func_int8_t_s_s((p_87 & (1UL && g_439[3])), g_333)), l_742[0])) || p_87), l_699)), l_699))))) , l_743))) >= p_89), l_613)))), (-1L))), g_101[1][0])), 0x2020L)), l_744))), g_636))), p_87)) , (void*)0) != (void*)0) != 1UL)) == 0xCA0F6E31064E7544LL) ^ l_706)) , l_743);
                        return l_706;
                    }
                    for (g_91 = (-10); (g_91 > 1); g_91 = safe_add_func_uint16_t_u_u(g_91, 5))
                    { /* block id: 343 */
                        int32_t l_753 = 0L;
                        int16_t *l_761 = &g_140;
                        int16_t **l_760 = &l_761;
                        int32_t *l_765 = (void*)0;
                        float l_783[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_783[i] = 0xF.7A720Ep-6;
                        l_766[1][2][0] ^= ((l_764[0] = (p_87 <= (((safe_sub_func_uint8_t_u_u(p_87, (l_763 = (((safe_rshift_func_int16_t_s_u(p_88, 0)) != 9L) <= (((p_87 > (((((((*l_544) = (((l_753 , (((*l_664) &= 1UL) == (((g_754 != (safe_div_func_uint32_t_u_u((((l_757 == (void*)0) || p_89) && 8UL), 0x48BFF250L))) && l_730) , p_87))) , l_759[0]) == &g_505)) , 0UL) || g_101[0][2]) || 4294967288UL) <= p_87) >= (-10L))) , l_760) == l_762))))) & 0x049A7B92L) && p_89))) , l_753);
                        (***g_153) = &l_764[2];
                        l_784 = (safe_mul_func_float_f_f((l_769[0] == (void*)0), (safe_mul_func_float_f_f((l_772 > (safe_mul_func_float_f_f((((safe_sub_func_float_f_f(p_88, ((l_666 = ((((*g_505) = (*g_505)) < l_699) != (+(safe_mul_func_float_f_f(p_89, ((l_763 = l_706) >= (((safe_div_func_int32_t_s_s(l_706, 0x92B0CABAL)) == p_89) , l_782))))))) > l_766[7][0][0]))) >= p_88) <= 0xB.792EAFp+4), l_783[0]))), l_691))));
                        l_785 = l_784;
                    }
                }
            }
            for (l_666 = (-9); (l_666 != 1); ++l_666)
            { /* block id: 360 */
                l_788 = p_89;
                return g_195;
            }
            (*l_789) |= p_87;
        }
    }
    else
    { /* block id: 366 */
        int32_t *l_792 = &g_439[7];
        int64_t *** const l_793[10] = {&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545};
        uint32_t *l_811[7][5] = {{&g_594,&g_594,&g_225,&g_225,&g_225},{&g_594,&g_594,&g_225,&g_225,&g_225},{&g_594,&g_594,&g_225,&g_225,&g_225},{&g_594,&g_594,&g_225,&g_225,&g_225},{&g_594,&g_594,&g_225,&g_225,&g_225},{&g_594,&g_594,&g_225,&g_225,&g_225},{&g_594,&g_594,&g_225,&g_225,&g_225}};
        int32_t l_836 = 0L;
        int16_t l_843[8][4] = {{0L,(-10L),0x6EF1L,0x186AL},{8L,1L,8L,0x6EF1L},{0x43C0L,(-3L),1L,0x43C0L},{0L,0x6EF1L,0xEEA4L,(-3L)},{0x6EF1L,1L,0xEEA4L,0xEEA4L},{0L,0L,0x43C0L,0x2CBBL},{0xEEA4L,4L,(-1L),8L},{(-1L),8L,7L,(-1L)}};
        int16_t l_848 = 0L;
        int32_t *l_850 = (void*)0;
        const int16_t l_854 = 8L;
        int32_t l_866 = 0x9EEE3A77L;
        int32_t l_867 = 0L;
        int32_t l_868 = 4L;
        int32_t l_869[5] = {0xDCD58A32L,0xDCD58A32L,0xDCD58A32L,0xDCD58A32L,0xDCD58A32L};
        const float ***l_924 = (void*)0;
        int i, j;
        if ((l_791[4][3] |= (safe_unary_minus_func_uint32_t_u(p_89))))
        { /* block id: 368 */
            int64_t ***l_795 = &l_545;
            int64_t ****l_794 = &l_795;
            l_792 = l_792;
            (*l_794) = l_793[3];
        }
        else
        { /* block id: 371 */
            int32_t l_805[3][7] = {{0L,0x2954B55BL,0L,0x2954B55BL,0L,0L,0x2954B55BL},{0xD80A8105L,0x106F3E37L,0xD80A8105L,0x2954B55BL,0x2954B55BL,0xD80A8105L,0x106F3E37L},{0x2954B55BL,0x106F3E37L,0L,0L,0x106F3E37L,0x2954B55BL,0x106F3E37L}};
            uint32_t *l_810[4][1][9];
            uint32_t **l_809[3][1];
            uint32_t l_813 = 0x6AE5DA8AL;
            int32_t * const *l_840 = &g_131[0];
            int32_t * const **l_839 = &l_840;
            int32_t * const ** const *l_838 = &l_839;
            int64_t l_841[9] = {(-3L),9L,(-3L),9L,(-3L),9L,(-3L),9L,(-3L)};
            int32_t l_863[8][6] = {{1L,0xA76C4EA5L,8L,0xA76C4EA5L,1L,0x0AC6F8FFL},{0xA76C4EA5L,1L,0x0AC6F8FFL,0x0AC6F8FFL,1L,0xA76C4EA5L},{(-1L),0xA76C4EA5L,0L,1L,0L,0xA76C4EA5L},{0L,(-1L),0x0AC6F8FFL,8L,8L,0x0AC6F8FFL},{0L,0L,8L,1L,0xE802E6FEL,1L},{(-1L),0L,(-1L),0x0AC6F8FFL,8L,8L},{0xA76C4EA5L,(-1L),(-1L),0xA76C4EA5L,0L,1L},{1L,0xA76C4EA5L,8L,0xA76C4EA5L,1L,0x0AC6F8FFL}};
            const uint8_t *l_911 = &g_912[1][0][0];
            uint8_t *** const *l_920 = &g_649;
            float ** const l_927 = &g_505;
            int16_t *l_937 = &l_848;
            int64_t * const **l_940 = (void*)0;
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 9; k++)
                        l_810[i][j][k] = &g_225;
                }
            }
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_809[i][j] = &l_810[2][0][5];
            }
            if ((((((safe_div_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u(((g_225 >= ((safe_add_func_int64_t_s_s(((((~1UL) != 0xE8423A1DL) != 0x5B0BL) != l_805[0][3]), 18446744073709551614UL)) < (((((~p_88) & ((l_811[2][4] = (l_805[0][3] , &g_225)) == l_812)) , (*l_792)) <= g_636) || 2L))) ^ (*l_792)), p_88)) == (*l_792)) >= l_813), 0x17L)) || l_813) == l_813) >= l_814) != 4294967295UL))
            { /* block id: 373 */
                int16_t *l_837 = &g_140;
                const int32_t l_842 = 0L;
                int32_t l_849 = (-1L);
            }
            else
            { /* block id: 407 */
                int32_t l_864 = (-9L);
                int32_t l_871 = 7L;
                int32_t l_872 = 0xB2F86642L;
                int32_t l_873 = 9L;
                int32_t l_874 = 0L;
                uint64_t l_875 = 0xE618E4D880397E46LL;
                for (g_282 = 2; (g_282 <= 8); g_282 += 1)
                { /* block id: 410 */
                    float ***l_861[10][7][2] = {{{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504}},{{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0}},{{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504}},{{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0}},{{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504}},{{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0}},{{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504}},{{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0}},{{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504}},{{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0},{&g_504,&g_504},{&g_504,(void*)0}}};
                    int32_t l_865 = 0x5E06DAA3L;
                    int32_t l_870 = 0xC34F8453L;
                    int64_t *l_900 = &g_33;
                    int16_t *l_904 = &l_843[6][3];
                    uint32_t *l_915 = &g_665;
                    uint8_t *l_916[1][5];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_916[i][j] = &g_85[8];
                    }
                    if ((l_861[8][6][1] == &g_504))
                    { /* block id: 411 */
                        int32_t *l_862[9][1] = {{&g_31[6]},{&l_666},{&g_31[6]},{&l_666},{&g_31[6]},{&l_666},{&g_31[6]},{&l_666},{&g_31[6]}};
                        int32_t **l_879 = &l_862[5][0];
                        uint16_t *l_890 = &l_99;
                        uint32_t *l_895[3];
                        uint8_t *l_898[8] = {(void*)0,&g_85[g_282],&g_85[g_282],(void*)0,&g_85[g_282],&g_85[g_282],(void*)0,&g_85[g_282]};
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_895[i] = &g_665;
                        (*g_505) = (-0x1.3p+1);
                        l_875++;
                        (*l_879) = ((****g_152) = (void*)0);
                        l_870 = (((((g_85[g_282] && l_875) , (safe_sub_func_int16_t_s_s(l_864, (safe_rshift_func_uint8_t_u_s((g_85[6] = (safe_mul_func_uint16_t_u_u((safe_div_func_int16_t_s_s(l_875, (safe_add_func_int8_t_s_s(((--(*l_890)) && p_88), (safe_add_func_uint32_t_u_u((g_665 = g_33), p_87)))))), (((safe_mod_func_int32_t_s_s(l_865, 0xC48F7EFCL)) , (-7L)) && p_88)))), l_899))))) < g_636) , (void*)0) != &g_153);
                    }
                    else
                    { /* block id: 420 */
                        (*l_792) ^= (l_873 |= (l_900 == (*g_243)));
                    }
                    (***g_153) = l_901;
                    if ((safe_mod_func_int8_t_s_s(((p_88 > ((*l_904) = (*l_852))) & (((*l_544) &= p_88) && (safe_mul_func_int8_t_s_s(1L, (g_85[6] |= ((safe_lshift_func_uint16_t_u_s((safe_sub_func_uint64_t_u_u(((&l_899 != (l_911 = &l_899)) & (~((safe_unary_minus_func_int64_t_s((*l_901))) != ((*l_915) = ((void*)0 == (*g_649)))))), p_89)), p_88)) | g_333)))))), p_88)))
                    { /* block id: 430 */
                        uint8_t l_917 = 0UL;
                        uint8_t *** const l_922 = (void*)0;
                        uint8_t *** const *l_921[8][2] = {{&l_922,(void*)0},{&l_922,(void*)0},{&l_922,(void*)0},{&l_922,(void*)0},{&l_922,(void*)0},{&l_922,(void*)0},{&l_922,(void*)0},{&l_922,(void*)0}};
                        int i, j;
                        (*l_792) = (((*l_900) = (p_88 <= 0x0152B2A9L)) && l_917);
                        (*l_852) &= ((safe_add_func_int32_t_s_s(l_917, ((l_921[1][1] = l_920) != l_923))) || 18446744073709551615UL);
                        return l_917;
                    }
                    else
                    { /* block id: 436 */
                        const float ****l_925 = &l_924;
                        (*l_925) = l_924;
                    }
                    l_871 ^= (~p_87);
                    for (g_33 = 0; (g_33 <= 4); g_33 += 1)
                    { /* block id: 442 */
                        (*g_928) = l_927;
                    }
                }
            }
            (*l_792) &= (safe_mod_func_int64_t_s_s(((p_87 & ((((p_89 && (p_89 > (safe_mul_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u(((((*l_937) = (*l_901)) <= p_88) & (((((((l_940 = l_938) == &g_397) , &l_927) == &g_504) < 0x6D9CFE90L) > g_500) , (-10L))), p_89)), 7L)), g_754)))) , 0x2AL) >= 1L) & g_282)) <= g_282), (*l_852)));
        }
    }
    return (*l_901);
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_101 g_33 g_91 g_52 g_129 g_130 g_131 g_195 g_85 g_4 g_152 g_153 g_140 g_96 g_243 g_225 g_282 g_333 g_224 g_347 g_409 g_439 g_500 g_503
 * writes: g_100 g_101 g_33 g_31 g_96 g_91 g_195 g_131 g_128 g_221 g_223 g_224 g_225 g_261 g_282 g_85 g_140 g_333 g_347 g_397 g_409 g_439 g_504 g_129
 */
static uint8_t  func_97(int32_t  p_98)
{ /* block id: 22 */
    int32_t *l_126 = &g_31[1];
    int32_t **l_125[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t ***l_133 = &g_130;
    int32_t ***l_134 = (void*)0;
    int16_t *l_139 = &g_140;
    const int32_t l_142 = 0x4E1783C2L;
    uint16_t l_193 = 0UL;
    uint64_t l_201[6][7][6] = {{{0x2B0C0EAC7FC37B35LL,18446744073709551612UL,0xBF5BA501070E863BLL,18446744073709551612UL,0x2B0C0EAC7FC37B35LL,0x4B936DA785FE2E02LL},{18446744073709551610UL,0xD9C689FF31343A82LL,0x2B0C0EAC7FC37B35LL,0x8469119564559AEFLL,18446744073709551614UL,18446744073709551615UL},{0xEB66CF8E74D01EFDLL,0x2B0C0EAC7FC37B35LL,7UL,0xD9C689FF31343A82LL,0x42C4FA8936D0663BLL,18446744073709551615UL},{18446744073709551612UL,2UL,0x2B0C0EAC7FC37B35LL,18446744073709551606UL,7UL,0x4B936DA785FE2E02LL},{0x42C4FA8936D0663BLL,8UL,0xBF5BA501070E863BLL,18446744073709551610UL,18446744073709551606UL,18446744073709551606UL},{1UL,18446744073709551615UL,18446744073709551615UL,1UL,0x4B936DA785FE2E02LL,7UL},{18446744073709551615UL,0x6AE4A769E6F5A32CLL,1UL,18446744073709551614UL,18446744073709551615UL,0x42C4FA8936D0663BLL}},{{0x5B9073D22E32B807LL,0xB88990D2993C0D73LL,18446744073709551612UL,7UL,18446744073709551615UL,18446744073709551614UL},{18446744073709551614UL,0x6AE4A769E6F5A32CLL,18446744073709551610UL,2UL,0x4B936DA785FE2E02LL,0x2B0C0EAC7FC37B35LL},{1UL,18446744073709551615UL,0xCC7ADD9185341A82LL,0xEB66CF8E74D01EFDLL,18446744073709551606UL,0xEB66CF8E74D01EFDLL},{0x6AE4A769E6F5A32CLL,8UL,0x6AE4A769E6F5A32CLL,1UL,7UL,7UL},{0x8469119564559AEFLL,2UL,18446744073709551606UL,18446744073709551615UL,0x42C4FA8936D0663BLL,0xCC7ADD9185341A82LL},{7UL,0x2B0C0EAC7FC37B35LL,18446744073709551607UL,18446744073709551615UL,18446744073709551614UL,1UL},{0x8469119564559AEFLL,0xD9C689FF31343A82LL,0xB88990D2993C0D73LL,1UL,0x2B0C0EAC7FC37B35LL,0xBF5BA501070E863BLL}},{{0x6AE4A769E6F5A32CLL,18446744073709551612UL,0x6A4908DBA9805CD8LL,0xEB66CF8E74D01EFDLL,0xEB66CF8E74D01EFDLL,0x6A4908DBA9805CD8LL},{1UL,1UL,8UL,2UL,7UL,18446744073709551612UL},{18446744073709551614UL,0x6A4908DBA9805CD8LL,1UL,7UL,0xCC7ADD9185341A82LL,8UL},{0x5B9073D22E32B807LL,18446744073709551614UL,1UL,18446744073709551614UL,1UL,18446744073709551612UL},{18446744073709551615UL,18446744073709551614UL,0x2B0C0EAC7FC37B35LL,0x6A4908DBA9805CD8LL,18446744073709551614UL,0xB88990D2993C0D73LL},{0x6A4908DBA9805CD8LL,18446744073709551614UL,0xB88990D2993C0D73LL,18446744073709551607UL,0xB88990D2993C0D73LL,18446744073709551614UL},{8UL,0x6AE4A769E6F5A32CLL,1UL,7UL,7UL,18446744073709551615UL}},{{7UL,0xBF5BA501070E863BLL,0x4B936DA785FE2E02LL,18446744073709551614UL,0x2B0C0EAC7FC37B35LL,0x36CA140F996A6281LL},{2UL,0xBF5BA501070E863BLL,7UL,18446744073709551615UL,7UL,0x42C4FA8936D0663BLL},{18446744073709551607UL,0x6AE4A769E6F5A32CLL,0xD9C689FF31343A82LL,7UL,0xB88990D2993C0D73LL,2UL},{18446744073709551610UL,18446744073709551614UL,0x36CA140F996A6281LL,0x36CA140F996A6281LL,18446744073709551614UL,18446744073709551610UL},{0x8469119564559AEFLL,1UL,18446744073709551607UL,18446744073709551606UL,18446744073709551615UL,8UL},{0x2B0C0EAC7FC37B35LL,8UL,7UL,1UL,0x36CA140F996A6281LL,8UL},{0x2B0C0EAC7FC37B35LL,0xB88990D2993C0D73LL,1UL,18446744073709551606UL,0x42C4FA8936D0663BLL,0x6AE4A769E6F5A32CLL}},{{0x8469119564559AEFLL,18446744073709551615UL,0xEB66CF8E74D01EFDLL,0x36CA140F996A6281LL,2UL,7UL},{18446744073709551610UL,7UL,18446744073709551614UL,7UL,18446744073709551610UL,18446744073709551612UL},{18446744073709551607UL,18446744073709551614UL,18446744073709551610UL,18446744073709551615UL,8UL,18446744073709551606UL},{2UL,18446744073709551610UL,0x6AE4A769E6F5A32CLL,18446744073709551614UL,8UL,18446744073709551606UL},{7UL,0x5B9073D22E32B807LL,18446744073709551610UL,7UL,0x6AE4A769E6F5A32CLL,18446744073709551612UL},{8UL,0xCC7ADD9185341A82LL,18446744073709551614UL,18446744073709551607UL,7UL,7UL},{0x6A4908DBA9805CD8LL,0xEB66CF8E74D01EFDLL,0xEB66CF8E74D01EFDLL,0x6A4908DBA9805CD8LL,18446744073709551612UL,0x6AE4A769E6F5A32CLL}},{{0xEB66CF8E74D01EFDLL,0xD9C689FF31343A82LL,1UL,1UL,18446744073709551606UL,8UL},{0xBF5BA501070E863BLL,1UL,7UL,0x6AE4A769E6F5A32CLL,18446744073709551606UL,8UL},{8UL,0xD9C689FF31343A82LL,18446744073709551607UL,0x5B9073D22E32B807LL,18446744073709551612UL,18446744073709551610UL},{18446744073709551615UL,0xEB66CF8E74D01EFDLL,0x36CA140F996A6281LL,2UL,7UL,2UL},{0xD9C689FF31343A82LL,0xCC7ADD9185341A82LL,0xD9C689FF31343A82LL,18446744073709551615UL,0x6AE4A769E6F5A32CLL,0x42C4FA8936D0663BLL},{18446744073709551615UL,0x5B9073D22E32B807LL,7UL,0xEB66CF8E74D01EFDLL,8UL,0x36CA140F996A6281LL},{0x42C4FA8936D0663BLL,18446744073709551610UL,0x4B936DA785FE2E02LL,0xEB66CF8E74D01EFDLL,8UL,18446744073709551615UL}}};
    int64_t *l_249 = (void*)0;
    int64_t l_259[2];
    int16_t *l_279 = &g_140;
    int16_t **l_278 = &l_279;
    int64_t **l_280 = &g_244[0];
    int32_t *l_281 = &g_282;
    uint16_t *l_283 = &l_193;
    uint64_t l_284[3][4][6] = {{{0xB7E7533F8854D64DLL,0xB7E7533F8854D64DLL,18446744073709551615UL,0x5CCB47C0F13302F4LL,18446744073709551606UL,18446744073709551615UL},{0x5CCB47C0F13302F4LL,18446744073709551606UL,18446744073709551615UL,18446744073709551606UL,0x5CCB47C0F13302F4LL,18446744073709551615UL},{18446744073709551606UL,0x5CCB47C0F13302F4LL,18446744073709551615UL,0xB7E7533F8854D64DLL,0xB7E7533F8854D64DLL,18446744073709551615UL},{0xB7E7533F8854D64DLL,0xB7E7533F8854D64DLL,18446744073709551615UL,0x5CCB47C0F13302F4LL,18446744073709551606UL,18446744073709551615UL}},{{0x5CCB47C0F13302F4LL,18446744073709551606UL,18446744073709551615UL,18446744073709551606UL,0x5CCB47C0F13302F4LL,18446744073709551615UL},{18446744073709551606UL,0x5CCB47C0F13302F4LL,18446744073709551615UL,0xB7E7533F8854D64DLL,0xB7E7533F8854D64DLL,18446744073709551615UL},{0xB7E7533F8854D64DLL,0xB7E7533F8854D64DLL,18446744073709551615UL,0x5CCB47C0F13302F4LL,18446744073709551606UL,18446744073709551615UL},{0x5CCB47C0F13302F4LL,18446744073709551606UL,18446744073709551615UL,18446744073709551606UL,0x5CCB47C0F13302F4LL,18446744073709551615UL}},{{18446744073709551606UL,0xEDDB29E4BBC024C7LL,0xB7E7533F8854D64DLL,0xC46ECEB4F8A2E917LL,0xC46ECEB4F8A2E917LL,0xB7E7533F8854D64DLL},{0xC46ECEB4F8A2E917LL,0xC46ECEB4F8A2E917LL,0xB7E7533F8854D64DLL,0xEDDB29E4BBC024C7LL,0UL,0xB7E7533F8854D64DLL},{0xEDDB29E4BBC024C7LL,0UL,0xB7E7533F8854D64DLL,0UL,0xEDDB29E4BBC024C7LL,0xB7E7533F8854D64DLL},{0UL,0xEDDB29E4BBC024C7LL,0xB7E7533F8854D64DLL,0xC46ECEB4F8A2E917LL,0xC46ECEB4F8A2E917LL,0xB7E7533F8854D64DLL}}};
    uint32_t l_285 = 0x94154AC2L;
    int32_t l_286 = (-7L);
    float l_287[3][10][6] = {{{0x1.8p+1,0xD.EF939Cp+80,0x6.D22B69p-20,0x8.6p+1,0x6.D22B69p-20,0xD.EF939Cp+80},{0x1.8p+1,(-0x4.0p+1),0x8.6p+1,(-0x1.Bp-1),0x1.Bp+1,0x0.8p+1},{0x0.9p+1,0x8.6p+1,0x1.8p+1,0x8.C09AC6p-66,0x8.C09AC6p-66,0x1.8p+1},{0x8.6p+1,0x8.6p+1,0xE.E1DC77p-28,0x0.9p+1,0x1.Bp+1,0x8.C09AC6p-66},{0xD.EF939Cp+80,(-0x4.0p+1),(-0x1.Bp-1),0xE.E1DC77p-28,0x6.D22B69p-20,0xE.E1DC77p-28},{(-0x1.Bp-1),0xD.EF939Cp+80,(-0x1.Bp-1),0x0.8p+1,0x8.6p+1,0x8.C09AC6p-66},{0x9.58F17Fp+15,0x0.8p+1,0xE.E1DC77p-28,0x8.98FADCp+9,0x1.8p+1,0x1.8p+1},{0x8.98FADCp+9,0x1.8p+1,0x1.8p+1,0x8.98FADCp+9,0xE.E1DC77p-28,0x0.8p+1},{0x9.58F17Fp+15,0x8.C09AC6p-66,0x8.6p+1,0x0.8p+1,(-0x1.Bp-1),0xD.EF939Cp+80},{(-0x1.Bp-1),0xE.E1DC77p-28,0x6.D22B69p-20,0xE.E1DC77p-28,(-0x1.Bp-1),(-0x4.0p+1)}},{{0xD.EF939Cp+80,0x8.C09AC6p-66,0x1.Bp+1,0x0.9p+1,0xE.E1DC77p-28,0x8.6p+1},{0x8.6p+1,0x1.8p+1,0x8.C09AC6p-66,0x8.C09AC6p-66,0x1.8p+1,0x8.6p+1},{0x0.9p+1,0x0.8p+1,0x1.Bp+1,(-0x1.Bp-1),0x8.6p+1,(-0x4.0p+1)},{0x1.8p+1,0xD.EF939Cp+80,0x6.D22B69p-20,0x8.6p+1,0x6.D22B69p-20,0xD.EF939Cp+80},{0x1.8p+1,(-0x4.0p+1),0x8.6p+1,(-0x1.Bp-1),0x1.Bp+1,0x0.8p+1},{0x0.9p+1,0x8.6p+1,0x1.8p+1,0x8.C09AC6p-66,0x8.C09AC6p-66,0x1.8p+1},{0x8.6p+1,0x8.6p+1,0xE.E1DC77p-28,0x0.9p+1,0x1.Bp+1,0x8.C09AC6p-66},{0xD.EF939Cp+80,(-0x4.0p+1),(-0x1.Bp-1),0xE.E1DC77p-28,0x6.D22B69p-20,0xE.E1DC77p-28},{(-0x1.Bp-1),0xD.EF939Cp+80,(-0x1.Bp-1),0x0.8p+1,0x8.6p+1,0x8.C09AC6p-66},{0x9.58F17Fp+15,0x0.8p+1,0xE.E1DC77p-28,0x8.98FADCp+9,0x1.8p+1,0x1.8p+1}},{{0x8.98FADCp+9,0x1.8p+1,0x1.8p+1,0x8.98FADCp+9,0xE.E1DC77p-28,0x0.8p+1},{0x9.58F17Fp+15,0x8.C09AC6p-66,0x8.6p+1,0x0.8p+1,(-0x1.Bp-1),0xD.EF939Cp+80},{(-0x1.Bp-1),0xE.E1DC77p-28,0x6.D22B69p-20,0xE.E1DC77p-28,(-0x1.Bp-1),(-0x4.0p+1)},{0xD.EF939Cp+80,0x8.C09AC6p-66,0x1.Bp+1,0x0.9p+1,0xE.E1DC77p-28,0x8.6p+1},{0x8.6p+1,0x1.8p+1,0x8.C09AC6p-66,0x8.C09AC6p-66,0x1.8p+1,0x8.6p+1},{0x0.9p+1,0x0.8p+1,0x1.Bp+1,(-0x1.Bp-1),0x8.6p+1,(-0x4.0p+1)},{0x1.8p+1,0xD.EF939Cp+80,0x6.D22B69p-20,0x8.6p+1,0x6.D22B69p-20,0xD.EF939Cp+80},{0x1.8p+1,(-0x4.0p+1),0x8.6p+1,(-0x1.Bp-1),0x1.Bp+1,(-0x4.0p+1)},{0x0.8p+1,0xE.E1DC77p-28,0x8.98FADCp+9,0x1.8p+1,0x1.8p+1,0x8.98FADCp+9},{0xE.E1DC77p-28,0xE.E1DC77p-28,0xD.EF939Cp+80,0x0.8p+1,0x6.D22B69p-20,0x1.8p+1}}};
    const int8_t l_344 = 1L;
    int8_t l_350 = (-1L);
    uint8_t l_353 = 0x78L;
    uint32_t l_387 = 0x5C2108A7L;
    float l_407 = 0xA.E64B51p-45;
    int8_t l_440 = (-1L);
    float l_486 = 0x6.43CA9Ap-34;
    int32_t l_510[3];
    int64_t l_538 = 0x287797C078D39F6ELL;
    int64_t l_543 = 0L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_259[i] = 7L;
    for (i = 0; i < 3; i++)
        l_510[i] = 0xD7F5DF77L;
    g_100 = (void*)0;
    g_101[5][0] ^= g_31[2];
    for (g_33 = 0; (g_33 == (-27)); g_33--)
    { /* block id: 27 */
        uint64_t l_104 = 0x0E46F67A21E2C279LL;
        int32_t *l_105 = &g_31[1];
        int32_t ****l_132[2];
        int16_t *l_141 = &g_140;
        uint8_t l_196[1][8][5] = {{{1UL,1UL,1UL,1UL,1UL},{0xDAL,9UL,0xDAL,9UL,0xDAL},{1UL,1UL,1UL,1UL,1UL},{0xDAL,9UL,0xDAL,9UL,0xDAL},{1UL,1UL,1UL,1UL,1UL},{0xDAL,9UL,0xDAL,9UL,0xDAL},{1UL,1UL,1UL,1UL,1UL},{0xDAL,9UL,0xDAL,9UL,0xDAL}}};
        int32_t *****l_199 = (void*)0;
        uint32_t l_230 = 0x8102D8EAL;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_132[i] = &g_129[3];
        (*l_105) |= (l_104 , 0xAFFF67D6L);
        for (l_104 = 0; (l_104 > 19); l_104 = safe_add_func_uint8_t_u_u(l_104, 9))
        { /* block id: 31 */
            int64_t *l_119 = &g_33;
            int64_t ** const l_118[2] = {&l_119,&l_119};
            int32_t **l_124 = &l_105;
            int32_t ***l_123[5][1][8] = {{{&l_124,&l_124,(void*)0,&l_124,(void*)0,&l_124,&l_124,&l_124}},{{&l_124,&l_124,&l_124,(void*)0,&l_124,&l_124,&l_124,&l_124}},{{&l_124,&l_124,&l_124,&l_124,&l_124,&l_124,(void*)0,&l_124}},{{&l_124,&l_124,&l_124,&l_124,&l_124,(void*)0,&l_124,&l_124}},{{&l_124,(void*)0,&l_124,&l_124,&l_124,&l_124,(void*)0,&l_124}}};
            float *l_127[7][7][5] = {{{&g_128[5],(void*)0,&g_128[5],&g_128[4],&g_128[4]},{&g_128[5],&g_128[5],&g_128[5],&g_128[1],(void*)0},{&g_128[1],&g_128[3],&g_128[3],&g_128[4],(void*)0},{&g_128[5],(void*)0,&g_128[5],(void*)0,&g_128[5]},{&g_128[5],(void*)0,&g_128[5],(void*)0,&g_128[4]},{&g_128[1],&g_128[5],&g_128[6],&g_128[6],&g_128[5]},{&g_128[5],&g_128[5],&g_128[4],(void*)0,&g_128[4]}},{{&g_128[6],&g_128[6],&g_128[5],&g_128[6],&g_128[5]},{&g_128[4],(void*)0,&g_128[2],&g_128[3],(void*)0},{&g_128[5],&g_128[2],&g_128[5],&g_128[5],(void*)0},{&g_128[5],(void*)0,(void*)0,&g_128[5],&g_128[4]},{&g_128[5],&g_128[6],&g_128[5],&g_128[5],&g_128[1]},{&g_128[2],&g_128[5],&g_128[5],&g_128[5],(void*)0},{&g_128[2],&g_128[5],&g_128[5],&g_128[5],&g_128[5]}},{{(void*)0,(void*)0,(void*)0,&g_128[5],&g_128[5]},{&g_128[5],(void*)0,&g_128[6],&g_128[5],&g_128[5]},{(void*)0,&g_128[3],&g_128[4],&g_128[3],&g_128[3]},{&g_128[5],&g_128[5],&g_128[5],&g_128[6],&g_128[5]},{(void*)0,(void*)0,&g_128[5],(void*)0,(void*)0},{&g_128[2],&g_128[2],&g_128[5],&g_128[5],&g_128[6]},{&g_128[5],&g_128[4],(void*)0,&g_128[4],&g_128[5]}},{{&g_128[5],&g_128[1],&g_128[5],&g_128[6],&g_128[5]},{(void*)0,(void*)0,&g_128[5],&g_128[5],&g_128[3]},{&g_128[1],&g_128[5],&g_128[5],&g_128[1],&g_128[5]},{&g_128[5],&g_128[5],&g_128[3],&g_128[5],&g_128[5]},{&g_128[5],&g_128[5],&g_128[5],&g_128[5],&g_128[6]},{(void*)0,(void*)0,&g_128[3],&g_128[1],&g_128[4]},{&g_128[5],&g_128[5],&g_128[5],&g_128[5],&g_128[5]}},{{(void*)0,&g_128[5],&g_128[5],&g_128[5],&g_128[2]},{&g_128[5],&g_128[5],&g_128[1],&g_128[1],&g_128[5]},{(void*)0,(void*)0,&g_128[5],&g_128[5],(void*)0},{&g_128[6],&g_128[1],&g_128[5],&g_128[5],&g_128[5]},{&g_128[4],&g_128[4],&g_128[5],&g_128[1],&g_128[5]},{&g_128[1],&g_128[2],&g_128[5],&g_128[5],&g_128[5]},{&g_128[4],&g_128[3],&g_128[4],&g_128[5],(void*)0}},{{&g_128[6],&g_128[5],&g_128[1],&g_128[1],&g_128[6]},{(void*)0,&g_128[1],&g_128[2],&g_128[5],&g_128[4]},{&g_128[5],&g_128[6],&g_128[1],&g_128[6],&g_128[5]},{(void*)0,&g_128[5],&g_128[4],&g_128[4],&g_128[5]},{&g_128[5],&g_128[5],&g_128[5],&g_128[5],&g_128[1]},{(void*)0,&g_128[4],&g_128[5],&g_128[5],&g_128[5]},{&g_128[5],&g_128[5],&g_128[5],&g_128[5],&g_128[5]}},{{&g_128[5],&g_128[5],&g_128[5],&g_128[2],&g_128[4]},{&g_128[1],(void*)0,&g_128[1],&g_128[2],&g_128[6]},{(void*)0,&g_128[5],&g_128[5],(void*)0,(void*)0},{&g_128[5],&g_128[5],&g_128[5],&g_128[5],&g_128[5]},{&g_128[5],&g_128[4],&g_128[3],(void*)0,&g_128[5]},{(void*)0,&g_128[5],&g_128[5],&g_128[5],&g_128[5]},{&g_128[4],&g_128[5],&g_128[3],(void*)0,(void*)0}}};
            int i, j, k;
            (*l_126) = (safe_add_func_float_f_f((((safe_sub_func_float_f_f((safe_add_func_float_f_f((g_31[2] <= (safe_add_func_float_f_f(g_91, ((safe_div_func_uint8_t_u_u(((void*)0 != l_118[0]), (+((safe_sub_func_int32_t_s_s(0xBED38BC6L, g_31[1])) && ((g_33 || ((l_125[3] = &l_105) == (g_91 , (void*)0))) | 0x0916D66CL))))) , g_52)))), (*l_105))), 0x0.8p+1)) >= (*l_126)) == g_101[5][1]), (-0x4.2p-1)));
        }
        (**g_130) &= (((-1L) > ((l_133 = g_129[1]) == (l_134 = &l_125[3]))) >= (((((((safe_mod_func_uint64_t_u_u(p_98, 0x43B2DE0ECAC02D6BLL)) || (safe_mul_func_uint8_t_u_u(g_101[2][3], 0x9FL))) , 0xD16FL) , l_139) != l_141) ^ l_142) == 0x9EL));
        for (g_96 = (-8); (g_96 != 40); g_96 = safe_add_func_int16_t_s_s(g_96, 3))
        { /* block id: 40 */
            int8_t l_162 = 1L;
            int32_t l_164 = (-2L);
            uint64_t l_166 = 0x6FF316C32C6D1ADBLL;
            const float *l_185 = (void*)0;
            int32_t l_226 = 0x1F0A48D6L;
            int32_t l_228 = (-3L);
            int32_t l_229 = (-1L);
            (**g_130) ^= (0x059AB7399A04901ELL | (!65535UL));
            for (p_98 = (-13); (p_98 > (-10)); p_98 = safe_add_func_uint8_t_u_u(p_98, 7))
            { /* block id: 44 */
                uint64_t l_158 = 0x429669FAE0D986BDLL;
                int32_t l_161 = 0x8CC8C47DL;
                int32_t l_165 = 0xDF424E93L;
                float *l_184 = &g_128[5];
                uint16_t *l_194[10][1][5] = {{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}},{{&g_195,&g_195,&g_195,&g_195,&g_195}}};
                int64_t *l_250[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int i, j, k;
                for (g_91 = 0; (g_91 <= 25); g_91++)
                { /* block id: 47 */
                    uint16_t l_150[1];
                    int32_t ****l_151 = &g_129[1];
                    int32_t l_157 = (-1L);
                    int i;
                    for (i = 0; i < 1; i++)
                        l_150[i] = 2UL;
                }
                if ((l_164 , (!(((*l_126) , (((safe_mod_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(p_98, (((safe_div_func_uint16_t_u_u((l_164 = (safe_add_func_int64_t_s_s(((((*l_105) = 0x45573AC2L) >= (p_98 , (safe_lshift_func_uint8_t_u_u((((((safe_mul_func_uint8_t_u_u((l_184 != l_185), (~(0L | (safe_rshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_u((safe_mod_func_uint16_t_u_u((g_195 |= ((l_193 & l_158) != p_98)), l_158)), 12)) , 0xA109L), 5)))))) , l_196[0][3][0]) == p_98) < p_98) == 0xE4L), g_91)))) >= 0x5C8E435DL), g_85[6]))), l_162)) <= 0xDC8AE0DA6F0B9C9ALL) <= g_4))), l_158)) > p_98), g_85[6])) & 18446744073709551615UL) , g_33)) >= l_166))))
                { /* block id: 69 */
                    for (l_193 = 0; (l_193 == 38); ++l_193)
                    { /* block id: 72 */
                        (**g_130) |= 0xCBF0C4E8L;
                    }
                }
                else
                { /* block id: 75 */
                    int32_t *****l_200 = &g_153;
                    int32_t l_227[10] = {0x606F9668L,8L,0x606F9668L,0xFB89C854L,0xFB89C854L,0x606F9668L,8L,0x606F9668L,0xFB89C854L,0xFB89C854L};
                    int i;
                    l_200 = l_199;
                    if (l_201[3][6][0])
                        break;
                    for (l_158 = 0; (l_158 <= 3); l_158 += 1)
                    { /* block id: 80 */
                        int32_t *l_202 = (void*)0;
                        float *l_220 = &g_221;
                        float *l_222 = &g_223;
                        int i, j, k;
                        l_226 = ((((g_128[6] = (l_202 != ((*g_130) = (****g_152)))) <= (l_201[(l_158 + 2)][(l_158 + 3)][(l_158 + 1)] < (g_225 = ((g_224 = (safe_div_func_float_f_f((safe_div_func_float_f_f(((((safe_div_func_float_f_f(g_85[(l_158 + 3)], (((((g_85[(l_158 + 4)] != ((safe_mul_func_float_f_f(((void*)0 == &g_129[1]), ((+((((*l_222) = ((*l_220) = (l_164 = ((safe_div_func_float_f_f((safe_div_func_float_f_f((0x8.05535Bp+32 >= ((((safe_rshift_func_uint16_t_u_s(((safe_sub_func_float_f_f((-0x5.0p+1), (*l_105))) , g_140), 9)) == g_96) , l_165) != 0x7.4p+1)), g_31[1])), (*l_126))) < g_52)))) > (-0x10.Ap+1)) >= l_161)) <= g_85[2]))) , p_98)) ^ g_140) == 0xCA9E05B609BCA0A6LL) | (-8L)) , p_98))) == 0x1.8p+1) < g_96) > g_33), g_96)), (-0x1.Ap+1)))) >= 0x2.0p-1)))) >= g_33) == 0xA.9956A4p-22);
                    }
                    l_230--;
                }
                for (g_224 = 0; (g_224 < (-10)); g_224 = safe_sub_func_int64_t_s_s(g_224, 6))
                { /* block id: 94 */
                    const int64_t l_247 = 0L;
                    for (g_225 = (-14); (g_225 <= 23); g_225 = safe_add_func_uint16_t_u_u(g_225, 1))
                    { /* block id: 97 */
                        int64_t **l_248 = (void*)0;
                        int64_t ***l_251 = &l_248;
                        int64_t **l_253 = &l_250[3];
                        int64_t ***l_252 = &l_253;
                        int32_t l_260[1][2][5] = {{{(-9L),(-9L),(-9L),(-9L),(-9L)},{0L,0L,0L,0L,0L}}};
                        int i, j, k;
                        (*l_184) = ((safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((((((g_243 != (void*)0) == ((safe_lshift_func_uint8_t_u_s((l_247 , ((l_249 = &g_33) == l_250[3])), 5)) <= (***l_133))) && (((*l_252) = ((*l_251) = &g_244[1])) == &l_249)) & (!(safe_rshift_func_int8_t_s_s((((safe_mod_func_int64_t_s_s(l_259[0], p_98)) && 0x61L) , l_260[0][1][2]), 4)))) , g_85[6]), 0)), p_98)) , 0x3.185A65p+63);
                    }
                    (*l_126) ^= (-2L);
                    g_261 = &l_165;
                }
            }
            if (p_98)
                continue;
        }
    }
    if ((p_98 = (safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((((safe_mod_func_uint32_t_u_u(((l_139 != l_139) <= (((safe_mul_func_float_f_f((((*l_126) = g_85[1]) <= (g_33 == ((safe_sub_func_uint16_t_u_u((((((safe_add_func_uint64_t_u_u(p_98, (((*l_283) = (safe_add_func_uint8_t_u_u(g_33, (((((*l_281) = ((safe_rshift_func_int8_t_s_s((((*l_278) = &g_140) == ((&g_244[0] != l_280) , &g_140)), 5)) == p_98)) , p_98) < 0x5883E4B1L) != p_98)))) != 1L))) < p_98) >= g_52) | p_98) || p_98), g_91)) , 0x6.CF9099p-15))), g_225)) <= p_98) , (*****g_152))), p_98)) ^ 0UL) >= p_98) , l_284[2][0][4]), l_285)), l_286))))
    { /* block id: 115 */
        uint32_t l_288 = 0x6F657B89L;
        int32_t ****l_301[1];
        uint8_t *l_307 = &g_85[6];
        int16_t **l_317 = &l_139;
        int64_t **l_394 = (void*)0;
        int32_t l_441 = 0x1DFB3079L;
        float *l_456 = &l_287[2][5][3];
        int8_t **l_460 = (void*)0;
        uint32_t l_467 = 0UL;
        int32_t l_483 = 0x233032BEL;
        int32_t l_493 = 0xAF0201BEL;
        uint8_t **l_517[1][4];
        const int8_t l_526 = 7L;
        int8_t l_539 = 0L;
        int i, j;
        for (i = 0; i < 1; i++)
            l_301[i] = &l_134;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 4; j++)
                l_517[i][j] = &l_307;
        }
        l_288++;
        for (g_96 = 0; (g_96 >= 12); g_96++)
        { /* block id: 119 */
            return p_98;
        }
        if ((safe_add_func_int16_t_s_s(0x838CL, (safe_rshift_func_uint8_t_u_u(6UL, (safe_rshift_func_int8_t_s_u((***l_133), ((*l_307) ^= (((safe_sub_func_int32_t_s_s((*l_126), ((*g_152) == (l_301[0] = &g_129[0])))) <= ((safe_rshift_func_uint8_t_u_u((~((0xADL == (safe_lshift_func_int8_t_s_s(p_98, 2))) > p_98)), g_31[1])) ^ p_98)) || p_98)))))))))
        { /* block id: 124 */
            uint16_t *l_314 = &l_193;
            uint32_t *l_315 = (void*)0;
            uint32_t *l_316 = &l_285;
            int32_t l_318 = 2L;
            const int32_t l_323 = 0x28E41555L;
            int64_t *l_324 = &l_259[1];
            int32_t l_349 = 0xEAA28D7FL;
            int32_t l_351 = 0xC5A30377L;
            int32_t l_352[5][9][5] = {{{0x31BC2CC6L,7L,0L,0L,0xAA22D6C9L},{5L,1L,0x61AEF186L,0L,0L},{7L,0xAB54C76CL,0xAB54C76CL,7L,0xAA22D6C9L},{0L,0L,1L,0xB96777C8L,1L},{0xAA22D6C9L,0x31BC2CC6L,0L,0L,1L},{0x52766ABEL,1L,0x52766ABEL,0xB96777C8L,0x61AEF186L},{0x8B28E14CL,0L,0L,7L,0x8B28E14CL},{0x8346D769L,(-5L),(-3L),0L,(-3L)},{0xAA22D6C9L,0xAA22D6C9L,0L,0L,0L}},{{0x614EF865L,2L,0x52766ABEL,(-5L),5L},{7L,0L,0L,0L,0x8B28E14CL},{(-3L),2L,1L,7L,0xFB1C5541L},{0x31BC2CC6L,0xAA22D6C9L,0xAB54C76CL,0L,1L},{(-4L),(-5L),0x61AEF186L,(-5L),(-4L)},{0x31BC2CC6L,0L,0L,0x8B28E14CL,0xAA22D6C9L},{(-3L),1L,0xFB1C5541L,0L,(-8L)},{7L,0x31BC2CC6L,0xAB54C76CL,0L,0xAA22D6C9L},{0x614EF865L,0L,0x83BF8C48L,0xB96777C8L,(-4L)}},{{0xAA22D6C9L,0xAB54C76CL,0L,1L,1L},{0x8346D769L,1L,0x8346D769L,0xB96777C8L,0xFB1C5541L},{0x8B28E14CL,7L,0L,0L,0x8B28E14CL},{0x52766ABEL,(-5L),5L,0L,5L},{0xAA22D6C9L,0L,0L,0x8B28E14CL,0L},{0L,2L,0x8346D769L,(-5L),(-3L)},{7L,0x8B28E14CL,0L,0L,0x8B28E14CL},{5L,2L,0x83BF8C48L,7L,0x61AEF186L},{0x31BC2CC6L,0L,0xAB54C76CL,0L,1L}},{{1L,(-5L),0xFB1C5541L,(-5L),1L},{0x31BC2CC6L,7L,0L,0L,0xAA22D6C9L},{5L,1L,0x61AEF186L,0L,0L},{7L,0xAB54C76CL,0xAB54C76CL,7L,0xAA22D6C9L},{0L,0L,0xFB1C5541L,7L,1L},{0L,0x8B28E14CL,0L,0L,0xAA22D6C9L},{(-4L),1L,(-4L),7L,5L},{0L,0xAB54C76CL,0L,0x31BC2CC6L,0L},{1L,2L,(-8L),0xB96777C8L,(-8L)}},{{0L,0L,0L,0L,7L},{0x52766ABEL,0L,(-4L),2L,0L},{0x31BC2CC6L,0L,0L,1L,0L},{(-8L),0L,0xFB1C5541L,1L,(-3L)},{0x8B28E14CL,0L,0L,0L,0xAA22D6C9L},{0x83BF8C48L,2L,5L,2L,0x83BF8C48L},{0x8B28E14CL,0xAB54C76CL,0xFC099468L,0L,0L},{(-8L),1L,(-3L),0xB96777C8L,0L},{0x31BC2CC6L,0x8B28E14CL,0L,0xAB54C76CL,0L}}};
            int i, j, k;
            if (((((*l_324) = (safe_mod_func_uint16_t_u_u((((**g_130) ^= (safe_sub_func_int8_t_s_s(p_98, (safe_sub_func_uint32_t_u_u(((*l_316) = (&g_195 != l_314)), ((void*)0 == l_317)))))) > (l_318 = 0L)), (safe_add_func_int16_t_s_s(((safe_lshift_func_int8_t_s_u(l_323, 7)) , ((*l_139) |= p_98)), l_323))))) ^ g_33) > g_282))
            { /* block id: 130 */
lbl_348:
                (**g_130) = (safe_add_func_int64_t_s_s((*l_126), 18446744073709551615UL));
            }
            else
            { /* block id: 132 */
                uint32_t l_345 = 0x30E99D11L;
                for (g_96 = 1; (g_96 <= 4); g_96 += 1)
                { /* block id: 135 */
                    uint64_t *l_331 = &g_96;
                    const int16_t * const l_340 = &g_140;
                    int32_t l_346 = 3L;
                    int i;
                    g_31[g_96] = g_31[g_96];
                    for (g_195 = 0; (g_195 <= 4); g_195 += 1)
                    { /* block id: 139 */
                        int8_t *l_332[2][9][10] = {{{(void*)0,(void*)0,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333},{&g_333,&g_333,&g_333,(void*)0,&g_333,(void*)0,&g_333,(void*)0,&g_333,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,(void*)0,&g_333},{(void*)0,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,(void*)0,(void*)0,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,(void*)0,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,(void*)0,&g_333,&g_333}},{{&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,&g_333,&g_333,&g_333},{&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,&g_333,&g_333,(void*)0},{&g_333,(void*)0,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333},{&g_333,&g_333,&g_333,(void*)0,(void*)0,&g_333,(void*)0,(void*)0,(void*)0,&g_333},{&g_333,&g_333,&g_333,&g_333,(void*)0,(void*)0,&g_333,&g_333,(void*)0,(void*)0},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333},{&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,&g_333,(void*)0},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,(void*)0,(void*)0},{&g_333,&g_333,&g_333,&g_333,&g_333,&g_333,(void*)0,&g_333,&g_333,&g_333}}};
                        int64_t *l_343 = &g_33;
                        int i, j, k;
                        l_318 = (l_346 = (((safe_mul_func_int16_t_s_s((safe_div_func_int8_t_s_s((g_333 = (l_331 == &l_284[1][1][3])), ((*l_307) = (((p_98 = (g_31[g_96] = (((safe_rshift_func_uint16_t_u_s(0UL, 7)) || (((void*)0 == &g_153) , (safe_mod_func_int8_t_s_s((safe_sub_func_int64_t_s_s(((((l_340 == l_340) & ((safe_div_func_int64_t_s_s(((*l_343) = ((*l_324) = (g_31[g_96] , (p_98 != g_85[2])))), 8UL)) != g_31[g_96])) ^ g_85[0]) & l_344), l_345)), 7L)))) ^ 1L))) , g_4) && l_318)))), l_346)) , g_33) > g_101[8][3]));
                        l_318 &= (**g_130);
                        g_128[(g_195 + 1)] = ((g_347 = (void*)0) == (void*)0);
                    }
                    for (l_318 = 4; (l_318 >= 1); l_318 -= 1)
                    { /* block id: 154 */
                        if (l_318)
                            goto lbl_348;
                    }
                }
                return p_98;
            }
            l_353++;
        }
        else
        { /* block id: 161 */
            int32_t l_362 = 0xBF6BB9B5L;
            uint64_t *l_363 = &g_91;
            uint64_t *l_368 = (void*)0;
            uint64_t *l_369[1];
            int16_t *l_372 = &g_140;
            uint16_t l_378[8] = {0x15A8L,0x15A8L,6UL,0x15A8L,0x15A8L,6UL,0x15A8L,0x15A8L};
            int32_t l_388[1];
            int32_t *l_459 = &g_439[0];
            int32_t ***l_530 = &g_130;
            int i;
            for (i = 0; i < 1; i++)
                l_369[i] = &l_201[1][2][3];
            for (i = 0; i < 1; i++)
                l_388[i] = 1L;
            for (g_96 = 2; (g_96 <= 6); g_96 += 1)
            { /* block id: 164 */
                if (p_98)
                    break;
                return g_333;
            }
            if ((((safe_lshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_u(0xD824L, (safe_add_func_int16_t_s_s(l_362, p_98)))), 6)) && ((*l_363) = g_224)) == (safe_mod_func_int8_t_s_s((-1L), (l_362 , (safe_rshift_func_uint8_t_u_u(((*l_307) = (g_31[3] || ((g_96--) , (((g_140 ^= (((l_372 == ((*l_317) = (*l_317))) > 0xFF77F35DL) & p_98)) & 65531UL) || p_98)))), l_362)))))))
            { /* block id: 173 */
                uint64_t l_389 = 18446744073709551615UL;
                int64_t ***l_395 = (void*)0;
                int64_t ***l_396[4][9][2] = {{{&l_394,&l_394},{&l_394,&l_280},{&l_280,&l_280},{&l_394,&l_394},{&l_394,(void*)0},{&l_394,&l_394},{&l_280,&l_394},{&l_280,(void*)0},{&l_280,&l_394}},{{&l_280,&l_394},{&l_394,(void*)0},{&l_394,&l_394},{&l_394,&l_280},{&l_280,&l_280},{&l_394,&l_394},{&l_394,(void*)0},{&l_394,&l_394},{&l_280,&l_394}},{{&l_280,(void*)0},{&l_280,&l_394},{&l_280,&l_394},{&l_394,(void*)0},{&l_394,&l_394},{&l_394,&l_280},{&l_280,&l_280},{&l_394,&l_394},{&l_394,(void*)0}},{{&l_394,&l_394},{&l_280,&l_394},{&l_280,(void*)0},{&l_280,&l_394},{&l_280,&l_394},{&l_394,(void*)0},{&l_394,&l_394},{&l_394,&l_280},{&l_280,&l_280}}};
                uint32_t *l_406 = &l_387;
                int16_t *l_408 = &g_409;
                int8_t *l_411[3];
                int8_t **l_410 = &l_411[1];
                int32_t l_442 = 0xF023F1CEL;
                int32_t ** const *l_457 = (void*)0;
                float l_458[6] = {0x0.7p-1,0x0.7p-1,0x0.7p-1,0x0.7p-1,0x0.7p-1,0x0.7p-1};
                int32_t l_484 = 0x160C5FCFL;
                int8_t l_487 = 0xE5L;
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_411[i] = &l_350;
                l_389 &= ((((*l_283) = (safe_mul_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u(0x9AL, (((!(l_378[7] != p_98)) , (l_388[0] = (safe_sub_func_uint64_t_u_u((++(*l_363)), (safe_sub_func_uint32_t_u_u((p_98 | (((void*)0 == &g_140) < ((*****g_347) = (g_101[5][1] ^ 0xF60AL)))), (safe_rshift_func_int8_t_s_u(l_387, p_98)))))))) | 0xE411ACD9326DD496LL))) ^ 0xCA9DB045L), 0xD998L))) | g_4) | p_98);
                (****g_153) = ((*l_126) & (safe_sub_func_uint8_t_u_u(p_98, (safe_mul_func_int8_t_s_s(((g_397 = l_394) != (void*)0), (((*l_408) |= (safe_lshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((safe_sub_func_int32_t_s_s(p_98, 0x2D05298BL)) < (l_388[0] = 1L)) >= ((0x03246391L || (((void*)0 != l_406) && g_52)) ^ l_362)), p_98)), g_140)), p_98))) , p_98))))));
                if ((((*l_410) = &g_333) == &g_333))
                { /* block id: 184 */
                    int32_t *l_412 = &l_388[0];
                    if ((p_98 &= (****g_153)))
                    { /* block id: 186 */
                        return g_140;
                    }
                    else
                    { /* block id: 188 */
                        (**l_133) = l_412;
                    }
                    (****g_347) = (****g_152);
                    for (g_282 = (-22); (g_282 <= (-4)); g_282++)
                    { /* block id: 194 */
                        int32_t *l_425 = &g_282;
                        int32_t l_438 = 0x1416812DL;
                        (****g_347) = (p_98 , &p_98);
                        l_441 = ((safe_lshift_func_int8_t_s_s(((p_98 | (safe_sub_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(l_389, g_31[1])), ((safe_add_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(((((l_425 == (void*)0) > ((**l_410) ^= (safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_u(p_98, 7)) <= (((safe_add_func_uint16_t_u_u(g_409, (safe_mul_func_int8_t_s_s(g_282, (((g_439[0] |= ((((((safe_lshift_func_int16_t_s_s(((**l_317) &= l_389), l_438)) == p_98) , g_101[5][1]) | 0xB308L) < 4294967288UL) | (-1L))) || (***l_133)) && 0x9137L))))) <= 0xDEBCD60BL) >= g_52)), l_440)), p_98)))) & 0x7B1ED3E3549D8BB8LL) | 0xA051EA069F6AD7B2LL), l_389)), p_98)) , 0xF65FL)))) || 0xC578L), 0)) || p_98);
                    }
                    for (l_350 = 0; (l_350 <= 8); l_350 += 1)
                    { /* block id: 203 */
                        l_442 |= p_98;
                        (****g_153) = 0xBDBDECFFL;
                        (****g_347) = &p_98;
                    }
                }
                else
                { /* block id: 208 */
                    int32_t l_451 = (-3L);
                    int8_t **l_462[10][6] = {{&l_411[1],&l_411[2],&l_411[0],&l_411[2],&l_411[1],&l_411[1]},{&l_411[1],&l_411[0],&l_411[2],&l_411[0],&l_411[1],&l_411[1]},{&l_411[0],&l_411[0],&l_411[0],&l_411[0],&l_411[0],&l_411[1]},{&l_411[0],&l_411[1],&l_411[2],&l_411[1],&l_411[0],&l_411[1]},{&l_411[0],&l_411[1],&l_411[0],(void*)0,&l_411[0],&l_411[1]},{&l_411[2],&l_411[1],&l_411[0],&l_411[0],&l_411[0],&l_411[0]},{&l_411[0],&l_411[0],&l_411[0],&l_411[0],&l_411[1],(void*)0},{&l_411[2],&l_411[0],&l_411[1],(void*)0,&l_411[1],&l_411[0]},{&l_411[1],&l_411[1],&l_411[0],&l_411[0],(void*)0,&l_411[2]},{&l_411[0],&l_411[0],&l_411[0],&l_411[0],&l_411[0],&l_411[0]}};
                    int32_t * const * const **l_466[7];
                    int32_t * const * const ***l_465 = &l_466[1];
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_466[i] = (void*)0;
                    if ((((safe_mul_func_int16_t_s_s((&g_244[3] == &g_244[1]), (p_98 == (safe_mul_func_uint16_t_u_u((safe_div_func_int32_t_s_s((((l_388[0] = p_98) , (safe_mod_func_int64_t_s_s((l_451 < ((*l_307) = ((safe_div_func_uint32_t_u_u((((safe_add_func_int64_t_s_s(((void*)0 != l_456), ((**g_152) == (l_457 = (**g_347))))) > 0x3452D26409F8CBE5LL) | l_389), (*****g_347))) ^ 0x3F37L))), p_98))) < g_4), (-1L))), g_439[0]))))) | 0xBBL) , p_98))
                    { /* block id: 212 */
                        l_459 = (**l_457);
                        return (***l_457);
                    }
                    else
                    { /* block id: 215 */
                        int8_t ***l_461 = (void*)0;
                        uint8_t **l_468 = &l_307;
                        float *l_479 = &g_128[5];
                        int32_t l_480 = 0x3D3A7B81L;
                        int32_t l_482 = 0xEA9184F1L;
                        int32_t l_485[8] = {0xD4531A81L,0x973A436AL,0xD4531A81L,0x973A436AL,0xD4531A81L,0x973A436AL,0xD4531A81L,0x973A436AL};
                        uint16_t l_488[10][5][5] = {{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL}},{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL}},{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL}},{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL}},{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL}},{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL}},{{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x1FD7L,65535UL,65535UL,0x1FD7L,0x5B8AL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL}},{{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL}},{{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL}},{{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL},{0x82AFL,0x1FD7L,0x1FD7L,0x82AFL,65535UL}}};
                        int i, j, k;
                        l_462[4][3] = l_460;
                        (*l_459) = ((safe_mul_func_float_f_f((g_224 == ((((g_85[6] &= g_333) , (((void*)0 != l_465) < l_467)) , (void*)0) != ((*l_468) = &g_52))), ((((*l_479) = ((safe_mul_func_float_f_f((((*l_408) = 0L) , (safe_mul_func_float_f_f((((*l_456) = (safe_mul_func_float_f_f(((*l_126) = (safe_div_func_float_f_f((((safe_mul_func_float_f_f(g_195, p_98)) != (-0x3.2p-1)) == g_333), p_98))), p_98))) >= 0xD.2AD68Bp+66), g_409))), (-0x7.1p+1))) != g_33)) >= p_98) != l_480))) != g_225);
                        --l_488[5][1][3];
                    }
                    (*l_459) = p_98;
                    return g_195;
                }
            }
            else
            { /* block id: 229 */
                int64_t *l_496 = &g_33;
                int32_t l_497 = 0xADE59E50L;
                int32_t l_498 = 0xE29BB100L;
                uint32_t *l_499 = &l_387;
                int32_t l_501 = 0x3F00FE59L;
                float **l_502 = &l_456;
                uint8_t **l_519[5];
                uint8_t ***l_518 = &l_519[2];
                int32_t l_527 = 1L;
                int i;
                for (i = 0; i < 5; i++)
                    l_519[i] = &l_307;
                (*g_503) = ((safe_mul_func_uint8_t_u_u((g_409 , ((((((void*)0 != &l_362) ^ ((g_333 = (l_493 && ((*****g_152) < (((((l_498 = (l_497 = ((*l_496) = p_98))) && p_98) > (((0x03L ^ ((((*l_499) = (***l_133)) < (*****g_347)) && g_500)) < p_98) <= p_98)) , g_333) != g_91)))) > 1UL)) > l_501) && 0xFB8938E93CABC9E6LL) , 2UL)), 1L)) , l_502);
                if ((safe_sub_func_uint8_t_u_u(p_98, (((safe_div_func_uint16_t_u_u(l_510[0], (((**l_317) = (l_499 != &l_142)) ^ ((l_501 ^ (l_527 &= (safe_rshift_func_int8_t_s_u(((safe_mod_func_uint32_t_u_u(0xF8AF6F20L, ((safe_mod_func_uint8_t_u_u(((l_517[0][1] = &l_307) == ((*l_518) = (void*)0)), (safe_div_func_int64_t_s_s(((safe_rshift_func_int16_t_s_u((safe_add_func_int32_t_s_s((p_98 && p_98), l_526)), 3)) || p_98), 1L)))) && g_85[1]))) & p_98), 6)))) == (****g_153))))) > l_498) , (*l_459)))))
                { /* block id: 240 */
                    (**g_347) = ((safe_sub_func_int16_t_s_s(g_96, p_98)) , l_530);
                }
                else
                { /* block id: 242 */
                    for (g_224 = 0; (g_224 <= 4); g_224 += 1)
                    { /* block id: 245 */
                        return p_98;
                    }
                }
            }
            for (g_96 = 0; (g_96 != 46); g_96++)
            { /* block id: 252 */
                uint32_t *l_533 = &g_225;
            }
        }
        (*l_126) = (((p_98 && (safe_add_func_int32_t_s_s((((p_98 & (l_538 , p_98)) || g_140) == (((l_394 != (void*)0) , l_539) , (p_98 < p_98))), g_85[8]))) , p_98) <= p_98);
    }
    else
    { /* block id: 259 */
        uint16_t l_540[2];
        int i;
        for (i = 0; i < 2; i++)
            l_540[i] = 0x0C20L;
        --l_540[1];
    }
    return l_543;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_31[i], "g_31[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_52, "g_52", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_75, "g_75", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_85[i], "g_85[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_91, "g_91", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_101[i][j], "g_101[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc_bytes(&g_128[i], sizeof(g_128[i]), "g_128[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_140, "g_140", print_hash_value);
    transparent_crc(g_195, "g_195", print_hash_value);
    transparent_crc_bytes (&g_221, sizeof(g_221), "g_221", print_hash_value);
    transparent_crc_bytes (&g_223, sizeof(g_223), "g_223", print_hash_value);
    transparent_crc(g_224, "g_224", print_hash_value);
    transparent_crc(g_225, "g_225", print_hash_value);
    transparent_crc(g_282, "g_282", print_hash_value);
    transparent_crc(g_333, "g_333", print_hash_value);
    transparent_crc(g_409, "g_409", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_439[i], "g_439[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_500, "g_500", print_hash_value);
    transparent_crc(g_569, "g_569", print_hash_value);
    transparent_crc(g_594, "g_594", print_hash_value);
    transparent_crc(g_627, "g_627", print_hash_value);
    transparent_crc(g_636, "g_636", print_hash_value);
    transparent_crc(g_665, "g_665", print_hash_value);
    transparent_crc(g_754, "g_754", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_912[i][j][k], "g_912[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_954, "g_954", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1089[i], "g_1089[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1112, sizeof(g_1112), "g_1112", print_hash_value);
    transparent_crc(g_1246, "g_1246", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1271[i][j][k], "g_1271[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1285, "g_1285", print_hash_value);
    transparent_crc(g_1297, "g_1297", print_hash_value);
    transparent_crc(g_1307, "g_1307", print_hash_value);
    transparent_crc(g_1313, "g_1313", print_hash_value);
    transparent_crc(g_1384, "g_1384", print_hash_value);
    transparent_crc(g_1389, "g_1389", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1498[i], "g_1498[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1540[i][j], "g_1540[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1556, "g_1556", print_hash_value);
    transparent_crc(g_1608, "g_1608", print_hash_value);
    transparent_crc(g_1647, "g_1647", print_hash_value);
    transparent_crc(g_1682, "g_1682", print_hash_value);
    transparent_crc(g_1683, "g_1683", print_hash_value);
    transparent_crc(g_1751, "g_1751", print_hash_value);
    transparent_crc_bytes (&g_1964, sizeof(g_1964), "g_1964", print_hash_value);
    transparent_crc_bytes (&g_2517, sizeof(g_2517), "g_2517", print_hash_value);
    transparent_crc(g_2542, "g_2542", print_hash_value);
    transparent_crc(g_2560, "g_2560", print_hash_value);
    transparent_crc(g_2652, "g_2652", print_hash_value);
    transparent_crc(g_2653, "g_2653", print_hash_value);
    transparent_crc_bytes (&g_2724, sizeof(g_2724), "g_2724", print_hash_value);
    transparent_crc(g_2746, "g_2746", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_2957[i][j][k], "g_2957[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3038, "g_3038", print_hash_value);
    transparent_crc(g_3078, "g_3078", print_hash_value);
    transparent_crc_bytes (&g_3177, sizeof(g_3177), "g_3177", print_hash_value);
    transparent_crc(g_3179, "g_3179", print_hash_value);
    transparent_crc(g_3273, "g_3273", print_hash_value);
    transparent_crc(g_3336, "g_3336", print_hash_value);
    transparent_crc(g_3540, "g_3540", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 928
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 58
breakdown:
   depth: 1, occurrence: 386
   depth: 2, occurrence: 97
   depth: 3, occurrence: 9
   depth: 4, occurrence: 5
   depth: 5, occurrence: 5
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 10, occurrence: 3
   depth: 11, occurrence: 1
   depth: 13, occurrence: 4
   depth: 14, occurrence: 1
   depth: 15, occurrence: 3
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 5
   depth: 19, occurrence: 4
   depth: 20, occurrence: 7
   depth: 21, occurrence: 2
   depth: 22, occurrence: 5
   depth: 23, occurrence: 4
   depth: 24, occurrence: 3
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 27, occurrence: 7
   depth: 28, occurrence: 1
   depth: 29, occurrence: 7
   depth: 30, occurrence: 2
   depth: 32, occurrence: 4
   depth: 33, occurrence: 4
   depth: 34, occurrence: 2
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 38, occurrence: 2
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1
   depth: 44, occurrence: 1
   depth: 50, occurrence: 1
   depth: 58, occurrence: 1

XXX total number of pointers: 711

XXX times a variable address is taken: 1775
XXX times a pointer is dereferenced on RHS: 379
breakdown:
   depth: 1, occurrence: 272
   depth: 2, occurrence: 43
   depth: 3, occurrence: 37
   depth: 4, occurrence: 14
   depth: 5, occurrence: 13
XXX times a pointer is dereferenced on LHS: 463
breakdown:
   depth: 1, occurrence: 359
   depth: 2, occurrence: 58
   depth: 3, occurrence: 17
   depth: 4, occurrence: 22
   depth: 5, occurrence: 7
XXX times a pointer is compared with null: 72
XXX times a pointer is compared with address of another variable: 21
XXX times a pointer is compared with another pointer: 25
XXX times a pointer is qualified to be dereferenced: 12819

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1309
   level: 2, occurrence: 435
   level: 3, occurrence: 321
   level: 4, occurrence: 284
   level: 5, occurrence: 152
XXX number of pointers point to pointers: 363
XXX number of pointers point to scalars: 348
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 33.1
XXX average alias set size: 1.4

XXX times a non-volatile is read: 2804
XXX times a non-volatile is write: 1511
XXX times a volatile is read: 163
XXX    times read thru a pointer: 72
XXX times a volatile is write: 54
XXX    times written thru a pointer: 41
XXX times a volatile is available for access: 2.02e+03
XXX percentage of non-volatile access: 95.2

XXX forward jumps: 3
XXX backward jumps: 14

XXX stmts: 395
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 42
   depth: 2, occurrence: 54
   depth: 3, occurrence: 70
   depth: 4, occurrence: 93
   depth: 5, occurrence: 104

XXX percentage a fresh-made variable is used: 15.3
XXX percentage an existing variable is used: 84.7
********************* end of statistics **********************/


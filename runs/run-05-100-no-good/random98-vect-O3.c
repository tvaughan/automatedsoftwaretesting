/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      908515154
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile uint64_t  f0;
   int32_t  f1;
   uint16_t  f2;
   volatile int64_t  f3;
   uint8_t  f4;
};

union U1 {
   int8_t  f0;
   volatile int8_t * volatile  f1;
};

union U2 {
   const int8_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_3[5] = {(-7L),(-7L),(-7L),(-7L),(-7L)};
static int32_t g_4 = 9L;
static int8_t g_17[10] = {0x35L,0L,0x35L,0L,0x35L,0L,0x35L,0L,0x35L,0L};
static int8_t *g_16 = &g_17[7];
static int32_t g_25 = (-1L);
static union U1 g_58 = {0x6AL};/* VOLATILE GLOBAL g_58 */
static union U1 g_71 = {0x71L};/* VOLATILE GLOBAL g_71 */
static int32_t *g_76 = &g_4;
static int32_t **g_75 = &g_76;
static uint64_t g_90 = 0xA233967BE2D7E08CLL;
static int16_t g_94[4] = {1L,1L,1L,1L};
static float g_97 = 0x1.E92403p-31;
static union U0 g_117[9][8] = {{{0x98B4E4A0A60341E9LL},{0x98B4E4A0A60341E9LL},{0x44AA845225C753BBLL},{0x95E34458F42BE7ACLL},{0x91CC17125C8CE6C9LL},{0x44AA845225C753BBLL},{0x91CC17125C8CE6C9LL},{0x95E34458F42BE7ACLL}},{{0x103C12D237583F02LL},{0x95E34458F42BE7ACLL},{0x103C12D237583F02LL},{6UL},{0x95E34458F42BE7ACLL},{0x457E6E873F63695FLL},{0x457E6E873F63695FLL},{0x95E34458F42BE7ACLL}},{{0x95E34458F42BE7ACLL},{0x457E6E873F63695FLL},{0x457E6E873F63695FLL},{0x95E34458F42BE7ACLL},{6UL},{0x103C12D237583F02LL},{0x95E34458F42BE7ACLL},{0x103C12D237583F02LL}},{{0x95E34458F42BE7ACLL},{0x91CC17125C8CE6C9LL},{0x44AA845225C753BBLL},{6UL},{0x457E6E873F63695FLL},{0x98B4E4A0A60341E9LL},{0x103C12D237583F02LL},{0x103C12D237583F02LL}},{{18446744073709551607UL},{6UL},{18446744073709551615UL},{18446744073709551615UL},{6UL},{18446744073709551607UL},{0x44AA845225C753BBLL},{6UL}},{{0x103C12D237583F02LL},{0x44AA845225C753BBLL},{18446744073709551615UL},{0x103C12D237583F02LL},{18446744073709551615UL},{0x44AA845225C753BBLL},{0x103C12D237583F02LL},{18446744073709551607UL}},{{6UL},{0x457E6E873F63695FLL},{0x98B4E4A0A60341E9LL},{0x103C12D237583F02LL},{0x103C12D237583F02LL},{0x98B4E4A0A60341E9LL},{0x457E6E873F63695FLL},{6UL}},{{18446744073709551607UL},{0x103C12D237583F02LL},{0x44AA845225C753BBLL},{18446744073709551615UL},{0x103C12D237583F02LL},{18446744073709551615UL},{0x44AA845225C753BBLL},{0x103C12D237583F02LL}},{{6UL},{0x44AA845225C753BBLL},{18446744073709551607UL},{6UL},{18446744073709551615UL},{18446744073709551615UL},{6UL},{18446744073709551607UL}}};
static volatile uint8_t g_131 = 0xAAL;/* VOLATILE GLOBAL g_131 */
static uint64_t g_154 = 0x3F393C2DF5847FBBLL;
static union U0 g_164 = {0UL};/* VOLATILE GLOBAL g_164 */
static union U2 g_167[6][6] = {{{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}},{{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}},{{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}},{{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}},{{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}},{{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}}};
static int64_t g_218 = (-1L);
static uint16_t * volatile g_255 = &g_117[4][6].f2;/* VOLATILE GLOBAL g_255 */
static uint64_t * const *g_257 = (void*)0;
static volatile uint64_t * volatile g_260 = &g_117[4][6].f0;/* VOLATILE GLOBAL g_260 */
static volatile uint64_t * volatile * volatile g_259 = &g_260;/* VOLATILE GLOBAL g_259 */
static union U2 g_333 = {-2L};
static union U1 g_345[5][4][5] = {{{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}},{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}},{{0xEDL},{0x00L},{0x00L},{0xEDL},{0x00L}},{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}}},{{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}},{{0xEDL},{0x00L},{0x00L},{0xEDL},{0x00L}},{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}},{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}}},{{{0xEDL},{0x00L},{0x00L},{0xEDL},{0x00L}},{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}},{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}},{{0xEDL},{0x00L},{0x00L},{0xEDL},{0x00L}}},{{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}},{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}},{{0xEDL},{0x00L},{0x00L},{0xEDL},{0x00L}},{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}}},{{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}},{{0xEDL},{0x00L},{0x00L},{0xEDL},{0x00L}},{{0xEDL},{0xEDL},{0x72L},{0xEDL},{0xEDL}},{{0x00L},{0xEDL},{0x00L},{0x00L},{0xEDL}}}};
static uint32_t g_350 = 4294967286UL;
static int64_t g_388 = 0xCB82A1ADDF37BF06LL;
static union U1 g_401 = {-1L};/* VOLATILE GLOBAL g_401 */
static float * volatile g_416 = (void*)0;/* VOLATILE GLOBAL g_416 */
static float g_418 = 0x2.Cp-1;
static float * volatile g_417 = &g_418;/* VOLATILE GLOBAL g_417 */
static int8_t *g_422 = &g_71.f0;
static uint64_t *****g_450 = (void*)0;
static const union U1 g_489 = {0L};/* VOLATILE GLOBAL g_489 */
static union U0 g_527 = {0x645FC40BD5225C5ELL};/* VOLATILE GLOBAL g_527 */
static volatile union U0 g_571 = {1UL};/* VOLATILE GLOBAL g_571 */
static uint16_t g_574 = 65535UL;
static union U0 * volatile g_594 = &g_527;/* VOLATILE GLOBAL g_594 */
static union U0 * volatile * volatile g_593 = &g_594;/* VOLATILE GLOBAL g_593 */
static int64_t g_635 = (-9L);
static int64_t *g_672 = &g_218;
static union U1 g_692 = {0x40L};/* VOLATILE GLOBAL g_692 */
static uint64_t ***g_703 = (void*)0;
static int32_t g_712 = 0L;
static int32_t *g_727 = &g_527.f1;
static int32_t g_766 = 0x111B5F83L;
static int32_t **g_782[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t g_791 = 0x91L;
static int32_t g_828 = 0L;
static volatile uint32_t g_913 = 0x49CE0AB9L;/* VOLATILE GLOBAL g_913 */
static uint64_t *g_922[2] = {(void*)0,(void*)0};
static uint64_t **g_921 = &g_922[0];
static uint64_t *** const g_920 = &g_921;
static uint64_t *** const *g_919 = &g_920;
static volatile int16_t g_929[10][10] = {{0x58D7L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L},{0x6348L,0x50EDL,1L,0x50EDL,0x6348L,0x6348L,0x50EDL,1L,0x50EDL,0x6348L}};
static union U0 g_951 = {1UL};/* VOLATILE GLOBAL g_951 */
static union U1 g_982 = {1L};/* VOLATILE GLOBAL g_982 */
static volatile union U1 g_993 = {0L};/* VOLATILE GLOBAL g_993 */
static volatile int32_t g_1042 = (-3L);/* VOLATILE GLOBAL g_1042 */
static volatile uint32_t g_1071[5][2] = {{0xA7A9FE63L,0xA7A9FE63L},{0xA7A9FE63L,0xA7A9FE63L},{0xA7A9FE63L,0xA7A9FE63L},{0xA7A9FE63L,0xA7A9FE63L},{0xA7A9FE63L,0xA7A9FE63L}};
static union U1 g_1115 = {0xCEL};/* VOLATILE GLOBAL g_1115 */
static uint64_t * const  volatile ** volatile g_1118[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint64_t * const  volatile ** volatile * volatile g_1117 = &g_1118[9];/* VOLATILE GLOBAL g_1117 */
static volatile union U0 g_1139 = {18446744073709551615UL};/* VOLATILE GLOBAL g_1139 */
static volatile int32_t *g_1140[6] = {&g_3[2],(void*)0,(void*)0,&g_3[2],(void*)0,(void*)0};
static volatile uint32_t g_1150 = 0xD9F8B4ECL;/* VOLATILE GLOBAL g_1150 */
static union U0 *g_1196[1][6] = {{&g_164,&g_164,&g_164,&g_164,&g_164,&g_164}};
static uint8_t g_1210 = 1UL;
static union U1 *g_1288 = &g_345[2][1][2];
static float * volatile g_1297 = &g_97;/* VOLATILE GLOBAL g_1297 */
static uint16_t *g_1319 = (void*)0;
static const volatile union U1 g_1362 = {-5L};/* VOLATILE GLOBAL g_1362 */
static const uint16_t *g_1408[8][1][9] = {{{(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2}},{{&g_117[4][6].f2,&g_527.f2,&g_527.f2,&g_117[4][6].f2,&g_527.f2,&g_527.f2,&g_117[4][6].f2,&g_527.f2,&g_527.f2}},{{(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2}},{{&g_117[4][6].f2,&g_527.f2,&g_527.f2,&g_117[4][6].f2,&g_527.f2,&g_527.f2,&g_117[4][6].f2,&g_527.f2,&g_527.f2}},{{(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2}},{{&g_117[4][6].f2,&g_527.f2,&g_527.f2,&g_117[4][6].f2,&g_527.f2,&g_527.f2,&g_117[4][6].f2,&g_527.f2,&g_527.f2}},{{(void*)0,&g_527.f2,&g_527.f2,(void*)0,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2}},{{&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2}}};
static const uint16_t **g_1407 = &g_1408[3][0][4];
static uint16_t g_1418 = 0x2DBFL;
static int8_t * volatile * volatile g_1431 = &g_16;/* VOLATILE GLOBAL g_1431 */
static int8_t * volatile * volatile * volatile g_1430 = &g_1431;/* VOLATILE GLOBAL g_1430 */
static int8_t * volatile * volatile * volatile * volatile g_1429 = &g_1430;/* VOLATILE GLOBAL g_1429 */
static int8_t * volatile * volatile * volatile * volatile * volatile g_1432 = &g_1429;/* VOLATILE GLOBAL g_1432 */
static int32_t g_1448 = 0xC051DEA0L;
static int16_t g_1449 = 1L;
static int64_t g_1470 = 0xABD9E8E579140F36LL;
static union U1 g_1530 = {5L};/* VOLATILE GLOBAL g_1530 */
static volatile union U0 g_1551 = {0xE9BD97B56981BE3BLL};/* VOLATILE GLOBAL g_1551 */
static int8_t **g_1601 = (void*)0;
static int8_t ***g_1600 = &g_1601;
static int8_t ****g_1599 = &g_1600;
static uint64_t g_1628 = 18446744073709551615UL;
static volatile union U0 g_1646 = {0x2502490652539992LL};/* VOLATILE GLOBAL g_1646 */
static union U0 g_1662 = {0x0B3C87D167875DC0LL};/* VOLATILE GLOBAL g_1662 */
static volatile union U2 * volatile *g_1665 = (void*)0;
static union U1 g_1675 = {1L};/* VOLATILE GLOBAL g_1675 */
static uint16_t **g_1716 = &g_1319;
static volatile int32_t *** volatile g_1721 = (void*)0;/* VOLATILE GLOBAL g_1721 */
static volatile int32_t **g_1723 = &g_1140[0];
static volatile int32_t *** volatile g_1722 = &g_1723;/* VOLATILE GLOBAL g_1722 */
static int32_t *g_1728 = &g_1662.f1;
static uint8_t *g_1735 = &g_527.f4;
static uint8_t ** const g_1734[7][6] = {{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{(void*)0,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{(void*)0,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735}};
static uint8_t ** const *g_1733 = &g_1734[3][2];
static int32_t g_1749 = 1L;
static union U0 g_1750 = {0x19D5B1D0F84DBC28LL};/* VOLATILE GLOBAL g_1750 */
static const int32_t g_1772 = 0L;
static union U1 g_1788[7][4][2] = {{{{0x15L},{-2L}},{{0x11L},{0x11L}},{{0x11L},{-2L}},{{0x15L},{-2L}}},{{{-2L},{-2L}},{{0x15L},{-2L}},{{0x11L},{0x11L}},{{0x11L},{-2L}}},{{{0x15L},{-2L}},{{-2L},{-2L}},{{0x15L},{-2L}},{{0x11L},{0x11L}}},{{{0x11L},{-2L}},{{0x15L},{-2L}},{{-2L},{-2L}},{{0x15L},{-2L}}},{{{0x11L},{0x11L}},{{0x11L},{-2L}},{{0x15L},{-2L}},{{-2L},{-2L}}},{{{0x15L},{-2L}},{{0x11L},{0x11L}},{{0x11L},{-2L}},{{0x15L},{-2L}}},{{{-2L},{-2L}},{{0x15L},{-2L}},{{0x11L},{0x11L}},{{0x11L},{-2L}}}};
static uint8_t g_1789 = 0UL;
static volatile int8_t g_1843 = 1L;/* VOLATILE GLOBAL g_1843 */
static uint8_t g_1861 = 255UL;
static int32_t *g_1864[3] = {&g_25,&g_25,&g_25};
static uint16_t g_1870 = 1UL;
static uint8_t **g_1905 = &g_1735;
static uint8_t *** const  volatile g_1904 = &g_1905;/* VOLATILE GLOBAL g_1904 */
static uint64_t g_1927[10][4] = {{0x2C37C6DCBDF79AAALL,0x2C37C6DCBDF79AAALL,0x3FB5BABD6343385CLL,0x2C37C6DCBDF79AAALL},{0x2C37C6DCBDF79AAALL,1UL,1UL,0x2C37C6DCBDF79AAALL},{1UL,0x2C37C6DCBDF79AAALL,1UL,1UL},{0x2C37C6DCBDF79AAALL,0x2C37C6DCBDF79AAALL,0x3FB5BABD6343385CLL,0x2C37C6DCBDF79AAALL},{0x2C37C6DCBDF79AAALL,1UL,1UL,0x2C37C6DCBDF79AAALL},{1UL,0x2C37C6DCBDF79AAALL,1UL,1UL},{0x2C37C6DCBDF79AAALL,0x2C37C6DCBDF79AAALL,0x3FB5BABD6343385CLL,0x2C37C6DCBDF79AAALL},{0x2C37C6DCBDF79AAALL,1UL,1UL,0x2C37C6DCBDF79AAALL},{1UL,0x2C37C6DCBDF79AAALL,1UL,1UL},{0x2C37C6DCBDF79AAALL,0x2C37C6DCBDF79AAALL,0x3FB5BABD6343385CLL,0x2C37C6DCBDF79AAALL}};
static int32_t g_1967 = 0x780C0AF9L;
static uint8_t ***g_1978 = &g_1905;
static uint8_t ****g_1977 = &g_1978;
static int32_t g_1989[4][6] = {{0L,0x77CABB5CL,0x77CABB5CL,0L,0x486E5DD2L,8L},{8L,0L,0L,0L,8L,0L},{0L,8L,0L,0L,8L,0L},{0x77CABB5CL,0L,0x486E5DD2L,8L,0x486E5DD2L,0L}};
static float * volatile g_1998 = &g_97;/* VOLATILE GLOBAL g_1998 */
static uint8_t ***** volatile g_2005 = &g_1977;/* VOLATILE GLOBAL g_2005 */
static int32_t ** volatile g_2006[8][7] = {{(void*)0,&g_1864[2],&g_1728,&g_727,&g_727,&g_1728,&g_1864[2]},{(void*)0,&g_1864[2],&g_1728,&g_727,&g_727,&g_1728,&g_1864[1]},{(void*)0,&g_1864[1],&g_727,(void*)0,(void*)0,&g_727,&g_1864[1]},{(void*)0,&g_1864[1],&g_727,(void*)0,(void*)0,&g_727,&g_1864[1]},{(void*)0,&g_1864[1],&g_727,(void*)0,(void*)0,&g_727,&g_1864[1]},{(void*)0,&g_1864[1],&g_727,(void*)0,(void*)0,&g_727,&g_1864[1]},{(void*)0,&g_1864[1],&g_727,(void*)0,(void*)0,&g_727,&g_1864[1]},{(void*)0,&g_1864[1],&g_727,(void*)0,(void*)0,&g_727,&g_1864[1]}};
static const int32_t g_2031[7] = {(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)};
static float * volatile g_2032 = &g_97;/* VOLATILE GLOBAL g_2032 */
static uint32_t *g_2043 = &g_350;
static uint32_t **g_2042 = &g_2043;
static float * volatile g_2050 = &g_418;/* VOLATILE GLOBAL g_2050 */
static volatile union U1 g_2087 = {-1L};/* VOLATILE GLOBAL g_2087 */
static int32_t g_2141 = 9L;
static uint16_t g_2163 = 6UL;
static int8_t * volatile * volatile * volatile * volatile * volatile g_2192 = &g_1429;/* VOLATILE GLOBAL g_2192 */
static int32_t * volatile g_2255 = (void*)0;/* VOLATILE GLOBAL g_2255 */
static union U2 g_2258 = {0x5CL};
static volatile uint32_t g_2333 = 0x3465385CL;/* VOLATILE GLOBAL g_2333 */
static int32_t g_2397[10][2][6] = {{{1L,0xE5A52F96L,0xAD957603L,4L,(-1L),6L},{1L,0x666FD07EL,4L,(-7L),4L,0x666FD07EL}},{{0x40985B58L,0xAD957603L,7L,(-10L),0x666FD07EL,0L},{0L,0xDD8FAB95L,(-7L),7L,1L,4L}},{{(-10L),0xDD8FAB95L,0xBB88A4C0L,0x666FD07EL,0x666FD07EL,0xBB88A4C0L},{0xAD957603L,0xAD957603L,0xDD8FAB95L,0xB538A22CL,4L,1L}},{{0xE5A52F96L,0x666FD07EL,6L,0L,(-1L),0xDD8FAB95L},{1L,0xE5A52F96L,6L,1L,0xAD957603L,1L}},{{0xDD8FAB95L,1L,0xDD8FAB95L,0L,(-1L),0xBB88A4C0L},{0L,(-1L),0xBB88A4C0L,(-1L),0x40985B58L,4L}},{{0xB538A22CL,0xBB88A4C0L,(-7L),(-1L),0L,0L},{0L,7L,7L,0L,0xE5A52F96L,0x666FD07EL}},{{0xDD8FAB95L,0xB538A22CL,4L,1L,(-7L),6L},{1L,(-1L),0xAD957603L,0L,(-7L),(-1L)}},{{0xE5A52F96L,0xB538A22CL,0L,0xB538A22CL,0xE5A52F96L,0L},{0xAD957603L,7L,(-10L),0x666FD07EL,0L,1L}},{{(-10L),0xBB88A4C0L,0xE5A52F96L,7L,0x40985B58L,1L},{0L,(-1L),(-10L),(-10L),(-1L),0L}},{{0x40985B58L,1L,0L,(-7L),0xAD957603L,(-1L)},{1L,0xE5A52F96L,0xAD957603L,4L,(-1L),6L}}};
static uint32_t g_2408 = 3UL;
static uint32_t g_2421[7] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static uint8_t g_2422 = 9UL;
static float g_2424 = 0xD.94167Dp+94;
static const union U1 ** volatile g_2427 = (void*)0;/* VOLATILE GLOBAL g_2427 */
static float * volatile g_2540 = &g_418;/* VOLATILE GLOBAL g_2540 */
static int64_t g_2557[4][3] = {{0L,0x4426C7D89A73C911LL,0L},{(-1L),(-1L),(-1L)},{0L,0x4426C7D89A73C911LL,0L},{(-1L),(-1L),(-1L)}};
static union U0 g_2593 = {1UL};/* VOLATILE GLOBAL g_2593 */
static volatile union U0 g_2625 = {18446744073709551615UL};/* VOLATILE GLOBAL g_2625 */
static volatile int64_t g_2638 = 0L;/* VOLATILE GLOBAL g_2638 */
static volatile float g_2648[4][3][6] = {{{(-0x4.9p+1),0x0.Bp+1,0x4.7p+1,0xC.CD23C4p+15,(-0x1.5p+1),0xE.500940p-4},{0x4.29F6B8p-50,(-0x1.4p+1),0x3.CE7E93p+50,(-0x1.5p+1),0x9.Ap-1,(-0x1.8p-1)},{0x0.8p-1,(-0x1.4p+1),0x3.7B369Ep+7,0x1.DB0B8Bp-15,(-0x1.5p+1),0x3.2E09FAp-5}},{{(-0x1.8p-1),0x1.4p-1,0x1.7096FBp+96,0x0.8p-1,0xF.FC2FD9p-21,0x4.7p+1},{0x4.2D4E2Bp+91,0x3.7B369Ep+7,(-0x1.4p+1),0x3.CE7E93p+50,0x3.CE7E93p+50,(-0x1.4p+1)},{0x4.C404B6p+55,0x4.C404B6p+55,0x1.DB0B8Bp-15,0x1.4p-1,(-0x1.4p+1),0xB.A6F509p-81}},{{0xC.CD23C4p+15,0xB.A6F509p-81,(-0x9.Ap+1),(-0x4.9p+1),0x0.8p-1,0x1.DB0B8Bp-15},{0x1.DB0B8Bp-15,0xC.CD23C4p+15,(-0x9.Ap+1),0xF.FC2FD9p-21,0x4.C404B6p+55,0xB.A6F509p-81},{(-0x1.7p+1),0xF.FC2FD9p-21,0x1.DB0B8Bp-15,0x1.3p+1,0x7.077422p-92,(-0x1.4p+1)}},{{0x1.3p+1,0x7.077422p-92,(-0x1.4p+1),0x4.7p+1,0x0.Bp+1,0x4.7p+1},{0x1.7096FBp+96,(-0x1.7p+1),0x1.7096FBp+96,0xE.500940p-4,0x8.7p+1,0x3.2E09FAp-5},{0x8.7p+1,0x4.7p+1,0x3.7B369Ep+7,0x4.29F6B8p-50,0x1.8BFEE1p+34,(-0x1.8p-1)}}};
static volatile union U1 g_2713 = {1L};/* VOLATILE GLOBAL g_2713 */
static int16_t g_2727 = 0xBA6BL;
static const union U2 **g_2821 = (void*)0;
static const union U2 ***g_2820 = &g_2821;
static volatile union U1 g_2839 = {0x4BL};/* VOLATILE GLOBAL g_2839 */
static volatile union U0 g_2850 = {0UL};/* VOLATILE GLOBAL g_2850 */
static union U0 * volatile ** volatile g_2865 = (void*)0;/* VOLATILE GLOBAL g_2865 */
static union U0 * volatile *g_2867 = &g_594;
static union U0 * volatile ** volatile g_2866 = &g_2867;/* VOLATILE GLOBAL g_2866 */
static int32_t g_2910 = 6L;
static volatile union U1 g_2942 = {-1L};/* VOLATILE GLOBAL g_2942 */
static float *g_2947[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t ** volatile * volatile g_2950 = &g_2006[4][1];/* VOLATILE GLOBAL g_2950 */
static int32_t ** volatile * volatile *g_2949 = &g_2950;
static int32_t ** volatile * volatile ** volatile g_2948 = &g_2949;/* VOLATILE GLOBAL g_2948 */
static union U0 g_3000 = {0x9772038600D043ECLL};/* VOLATILE GLOBAL g_3000 */
static int32_t *g_3006 = &g_1448;
static int32_t **g_3005 = &g_3006;
static int32_t ***g_3004 = &g_3005;
static int32_t g_3019 = 0x3F9A84A4L;
static float g_3024 = 0x6.C238ADp-2;
static const int32_t *g_3108[3] = {&g_25,&g_25,&g_25};
static const int32_t **g_3107 = &g_3108[2];
static union U2 *g_3147 = &g_167[1][4];
static union U2 **g_3146[2] = {&g_3147,&g_3147};
static union U2 ***g_3145 = &g_3146[1];
static uint16_t g_3153 = 0x4E82L;
static union U0 g_3157 = {0x3209A635BE71CEA9LL};/* VOLATILE GLOBAL g_3157 */
static int8_t g_3192 = 1L;
static union U0 g_3210[10] = {{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL},{0x50276388FBDB92B6LL}};
static const volatile union U0 g_3228 = {0x5091A8D760B6E1D2LL};/* VOLATILE GLOBAL g_3228 */
static union U2 ****g_3261[7] = {&g_3145,&g_3145,&g_3145,&g_3145,&g_3145,&g_3145,&g_3145};
static int32_t **g_3342 = &g_3006;
static float g_3365 = 0x9.B964F0p-72;
static volatile union U0 g_3416[9][4][1] = {{{{18446744073709551612UL}},{{0xD7157B67414364E0LL}},{{18446744073709551615UL}},{{0xD7157B67414364E0LL}}},{{{18446744073709551612UL}},{{0x3A90A27F74AE7F57LL}},{{0x6EF27A390C0E2D51LL}},{{0x3DB401F994067972LL}}},{{{0x3DB401F994067972LL}},{{0x6EF27A390C0E2D51LL}},{{0x3A90A27F74AE7F57LL}},{{18446744073709551612UL}}},{{{0xD7157B67414364E0LL}},{{18446744073709551615UL}},{{0xD7157B67414364E0LL}},{{18446744073709551612UL}}},{{{0x3A90A27F74AE7F57LL}},{{0x6EF27A390C0E2D51LL}},{{0x3DB401F994067972LL}},{{0x3DB401F994067972LL}}},{{{0x6EF27A390C0E2D51LL}},{{0x3A90A27F74AE7F57LL}},{{18446744073709551612UL}},{{0xD7157B67414364E0LL}}},{{{18446744073709551615UL}},{{0xD7157B67414364E0LL}},{{18446744073709551612UL}},{{0x3A90A27F74AE7F57LL}}},{{{0x6EF27A390C0E2D51LL}},{{0x3DB401F994067972LL}},{{0x3DB401F994067972LL}},{{0x6EF27A390C0E2D51LL}}},{{{0x3A90A27F74AE7F57LL}},{{18446744073709551612UL}},{{0xD7157B67414364E0LL}},{{18446744073709551615UL}}}};
static union U0 g_3430 = {18446744073709551612UL};/* VOLATILE GLOBAL g_3430 */
static int32_t g_3481[4][8][1] = {{{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L}},{{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L}},{{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L}},{{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L},{5L},{0xE2C1E0B8L}}};
static volatile union U0 g_3505[8] = {{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL},{0xB4CFE6AF65EB496ELL}};
static const volatile uint8_t g_3536 = 0xB4L;/* VOLATILE GLOBAL g_3536 */
static volatile union U1 g_3583 = {0xEDL};/* VOLATILE GLOBAL g_3583 */
static int64_t **g_3595 = &g_672;
static int64_t **g_3596 = &g_672;
static uint8_t g_3598 = 0x1BL;
static int64_t g_3607 = 0x358404C35AD435B4LL;
static volatile int32_t g_3608 = 0L;/* VOLATILE GLOBAL g_3608 */
static int64_t g_3611 = 0x28D9C26CD1CF16ECLL;
static int32_t *** const **g_3675 = (void*)0;
static float ** const *g_3733 = (void*)0;
static uint64_t * const **g_3780 = &g_257;
static uint64_t * const ***g_3779 = &g_3780;
static uint64_t * const ****g_3778 = &g_3779;
static uint32_t *** volatile *g_3785 = (void*)0;
static const volatile int32_t g_3791 = 0x64FBA751L;/* VOLATILE GLOBAL g_3791 */
static float * volatile g_3792[6][1] = {{&g_418},{&g_97},{&g_97},{&g_418},{&g_97},{&g_97}};
static float * volatile g_3793 = &g_97;/* VOLATILE GLOBAL g_3793 */
static float g_3845 = 0x1.521A83p-9;
static float g_3848 = 0xD.BF7BFCp-83;
static int64_t g_3869[5][8][6] = {{{0x1709C51A3CED0372LL,0xD0B6AE686BCEA823LL,0x1709C51A3CED0372LL,0x1709C51A3CED0372LL,0xD0B6AE686BCEA823LL,0x1709C51A3CED0372LL},{0x1709C51A3CED0372LL,0xD0B6AE686BCEA823LL,0x1709C51A3CED0372LL,0x1709C51A3CED0372LL,0xD0B6AE686BCEA823LL,0x1709C51A3CED0372LL},{0x1709C51A3CED0372LL,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L}},{{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L}},{{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L}},{{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L}},{{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,8L},{8L,0x1709C51A3CED0372LL,8L,8L,0x1709C51A3CED0372LL,0xD0B6AE686BCEA823LL},{0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL,0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL},{0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL,0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL},{0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL,0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL},{0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL,0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL},{0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL,0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL},{0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL,0xD0B6AE686BCEA823LL,8L,0xD0B6AE686BCEA823LL}}};
static volatile union U1 g_3873 = {0x3FL};/* VOLATILE GLOBAL g_3873 */
static union U0 g_3887 = {0x90F35E8F1683B8C1LL};/* VOLATILE GLOBAL g_3887 */
static uint32_t g_3907[5][7][7] = {{{0x596401C5L,5UL,18446744073709551615UL,0xA643AE91L,0x20237982L,1UL,0x4470A9CBL},{0xADE2AAE1L,0x7AA68A83L,0x1489D624L,0xFC3F8451L,0x6B2A9ECDL,0x4CD02815L,0UL},{0x6B2A9ECDL,7UL,0xE964875BL,0UL,0x8ED6C329L,18446744073709551611UL,5UL},{5UL,0x3BE16A21L,18446744073709551615UL,6UL,0x56FB3594L,0x139CD2BDL,6UL},{18446744073709551612UL,0x3BE16A21L,1UL,5UL,0UL,6UL,0UL},{18446744073709551613UL,7UL,0UL,0UL,0x1F47C1ADL,18446744073709551615UL,0xFC3F8451L},{0x6A61184DL,0x7AA68A83L,0x139CD2BDL,0x4470A9CBL,0xADE2AAE1L,0x8ED6C329L,0xA643AE91L}},{{0xBBAFFBE8L,5UL,0UL,6UL,0x11151D97L,0xF86D5808L,0x11151D97L},{0xEE856859L,1UL,1UL,0xEE856859L,0x0E5ED51FL,18446744073709551611UL,0x56FB3594L},{0x4470A9CBL,0x894E4E2FL,0x139CD2BDL,1UL,18446744073709551611UL,0x1F47C1ADL,0xE964875BL},{0x7AA68A83L,18446744073709551613UL,0x4CEF1CE6L,18446744073709551609UL,5UL,0x3D7FEBB5L,0x56FB3594L},{0x30EFF360L,18446744073709551611UL,0UL,0xADE2AAE1L,7UL,0x6A61184DL,0x11151D97L},{0xD57FA182L,0xADE2AAE1L,18446744073709551615UL,1UL,0x890516B1L,0x0E5ED51FL,0xA643AE91L},{0x54CFAFA2L,18446744073709551612UL,0xDCB07496L,18446744073709551609UL,0UL,0x04B4FBD0L,0xFC3F8451L}},{{0x20237982L,0xD57FA182L,0x94F564CDL,0x54CFAFA2L,1UL,0x596401C5L,0UL},{1UL,18446744073709551609UL,18446744073709551615UL,0UL,0x2AB32F1EL,7UL,6UL},{0x890516B1L,0x2AB32F1EL,0x4CD02815L,6UL,0x2AB32F1EL,0xAA81B3DBL,5UL},{0x30EFF360L,0xF6B23C4CL,0xD57FA182L,0x9FA27B0FL,1UL,1UL,0UL},{18446744073709551611UL,0x08016C15L,1UL,18446744073709551611UL,0UL,1UL,0x4470A9CBL},{0x9FA27B0FL,0xFC3F8451L,6UL,5UL,0x890516B1L,0xE197626EL,6UL},{0xEE856859L,18446744073709551615UL,7UL,0x30EFF360L,7UL,18446744073709551615UL,0xEE856859L}},{{3UL,18446744073709551609UL,6UL,1UL,5UL,1UL,1UL},{0xA643AE91L,6UL,18446744073709551612UL,1UL,0x3FB9FBB0L,0x56FB3594L,0xE964875BL},{18446744073709551615UL,0x1980AEA3L,0x1489D624L,18446744073709551611UL,0x3D7FEBB5L,0x9C4D9B6FL,0x8ED6C329L},{0x1980AEA3L,0x8ED6C329L,6UL,0xB0F7F215L,0xA643AE91L,6UL,6UL},{0x3FB9FBB0L,0UL,5UL,18446744073709551611UL,0x8ED6C329L,0UL,0xE964875BL},{0xD57FA182L,4UL,6UL,0UL,9UL,0UL,0x6A61184DL},{6UL,18446744073709551611UL,0x9C4D9B6FL,0xD8AC0BB4L,0UL,0x3D7FEBB5L,0xC90B834BL}},{{1UL,0x6A61184DL,0xBBAFFBE8L,0xA643AE91L,18446744073709551611UL,0x894E4E2FL,3UL},{1UL,0x81C35B07L,0xAA81B3DBL,18446744073709551611UL,0x04B4FBD0L,6UL,1UL},{6UL,18446744073709551615UL,18446744073709551615UL,0x9C407149L,0xD57FA182L,9UL,1UL},{0xD57FA182L,0x90222CBCL,0xEE856859L,0x139CD2BDL,5UL,18446744073709551612UL,1UL},{0x3FB9FBB0L,0x4CD02815L,6UL,0x94F564CDL,0xD8AC0BB4L,0xD8AC0BB4L,0x94F564CDL},{0x1980AEA3L,0x1F47C1ADL,0x1980AEA3L,0x81C35B07L,0UL,0x04B4FBD0L,0xF86D5808L},{18446744073709551615UL,0x90222CBCL,0xF9551B95L,0UL,3UL,0x890516B1L,1UL}}};
static const volatile union U0 g_3972 = {0x59872F8551772CBCLL};/* VOLATILE GLOBAL g_3972 */
static volatile uint32_t g_4110 = 4294967293UL;/* VOLATILE GLOBAL g_4110 */
static volatile uint32_t *g_4109 = &g_4110;
static volatile union U0 g_4124 = {0x2751B8D994F87C28LL};/* VOLATILE GLOBAL g_4124 */
static int16_t g_4152[3][4] = {{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L)}};
static uint32_t g_4164 = 0x4D6DA33BL;
static union U0 g_4180 = {0x6B2B8A50C05B529FLL};/* VOLATILE GLOBAL g_4180 */
static int32_t *g_4344 = &g_164.f1;
static int32_t *g_4345[9][6][2] = {{{&g_117[4][6].f1,&g_1967},{&g_4,&g_828},{(void*)0,&g_828},{&g_4,&g_1967},{&g_117[4][6].f1,&g_3481[2][7][0]},{&g_3210[0].f1,&g_117[4][6].f1}},{{&g_3481[0][4][0],(void*)0},{&g_3481[2][7][0],&g_4180.f1},{&g_3210[0].f1,&g_3481[0][4][0]},{&g_4180.f1,&g_1967},{&g_951.f1,&g_951.f1},{(void*)0,&g_4}},{{&g_828,&g_1967},{&g_117[4][6].f1,&g_3481[0][4][0]},{&g_3210[0].f1,&g_117[4][6].f1},{&g_3481[0][4][0],(void*)0},{&g_3481[0][4][0],&g_117[4][6].f1},{&g_3210[0].f1,&g_3481[0][4][0]}},{{&g_117[4][6].f1,&g_1967},{&g_828,&g_4},{(void*)0,&g_951.f1},{&g_951.f1,&g_1967},{&g_4180.f1,&g_3481[0][4][0]},{&g_3210[0].f1,&g_4180.f1}},{{&g_3481[2][7][0],(void*)0},{&g_3481[0][4][0],&g_117[4][6].f1},{&g_3210[0].f1,&g_3481[2][7][0]},{&g_117[4][6].f1,&g_1967},{&g_4,&g_828},{(void*)0,&g_828}},{{&g_4,&g_1967},{&g_117[4][6].f1,&g_3481[2][7][0]},{&g_3210[0].f1,&g_117[4][6].f1},{&g_3481[0][4][0],(void*)0},{&g_3481[2][7][0],&g_4180.f1},{&g_3210[0].f1,&g_3481[0][4][0]}},{{&g_4180.f1,&g_1967},{&g_951.f1,&g_951.f1},{(void*)0,&g_4},{&g_828,&g_1967},{&g_117[4][6].f1,&g_3481[0][4][0]},{&g_3210[0].f1,&g_117[4][6].f1}},{{&g_3481[0][4][0],(void*)0},{&g_3481[0][4][0],&g_117[4][6].f1},{&g_951.f1,(void*)0},{&g_1967,&g_2141},{(void*)0,&g_3210[0].f1},{&g_1989[1][1],&g_25}},{{&g_25,&g_2141},{&g_4,&g_3210[0].f1},{&g_951.f1,&g_4},{&g_3481[0][5][0],&g_1662.f1},{(void*)0,(void*)0},{&g_951.f1,&g_3481[0][5][0]}}};
static int32_t ** const g_4343[10] = {&g_4344,&g_4344,&g_4344,&g_4344,&g_4344,&g_4344,&g_4344,&g_4344,&g_4344,&g_4344};
static int32_t ** const *g_4342 = &g_4343[0];
static union U1 ** volatile g_4348[7] = {&g_1288,&g_1288,&g_1288,&g_1288,&g_1288,&g_1288,&g_1288};
static volatile union U1 *g_4356 = &g_2839;
static volatile union U1 **g_4355 = &g_4356;
static volatile union U1 ***g_4354[6] = {&g_4355,&g_4355,&g_4355,&g_4355,&g_4355,&g_4355};
static const uint32_t g_4439 = 0x67B3243CL;
static volatile uint64_t g_4525 = 18446744073709551615UL;/* VOLATILE GLOBAL g_4525 */
static int8_t g_4603 = 0x5BL;
static int64_t * const *g_4619 = &g_672;
static int64_t * const **g_4618[10][5] = {{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619},{(void*)0,&g_4619,(void*)0,(void*)0,&g_4619},{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619},{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619},{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619},{&g_4619,&g_4619,(void*)0,&g_4619,(void*)0},{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619},{(void*)0,&g_4619,&g_4619,(void*)0,&g_4619},{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619},{&g_4619,&g_4619,&g_4619,&g_4619,&g_4619}};


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static int32_t * func_5(int64_t  p_6, int32_t  p_7, float  p_8, const int8_t * p_9);
static uint8_t  func_13(int8_t * p_14, int32_t  p_15);
static int32_t  func_18(int32_t * p_19, int8_t * p_20);
static int32_t * func_21(union U2  p_22);
static int32_t * func_34(uint16_t  p_35, float  p_36, int8_t  p_37, int64_t  p_38, int8_t  p_39);
static int32_t * func_42(int32_t  p_43, int8_t * p_44);
static uint16_t  func_47(float  p_48, const int8_t * p_49, const int32_t  p_50, int8_t * p_51);
static int8_t * func_52(int8_t  p_53, int32_t * p_54, int64_t  p_55, const int8_t * p_56);
static int16_t  func_68(int8_t  p_69, int32_t * p_70);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_16 g_593 g_594 g_1196 g_1978 g_1905 g_1735 g_527.f4 g_1210 g_2043 g_350 g_388 g_3005 g_3006 g_1448 g_2949 g_2950 g_2042 g_3785 g_3145 g_3791 g_3793 g_75 g_76 g_727 g_527.f1 g_672 g_218 g_17 g_2005 g_1977 g_255 g_117.f2 g_1722 g_1723 g_1140 g_1728 g_3107 g_154 g_2947 g_3 g_1628 g_828
 * writes: g_4 g_3778 g_350 g_3146 g_1750.f4 g_97 g_527.f1 g_1977 g_3261 g_2141 g_1662.f1 g_3108 g_154 g_1662.f4 g_3 g_1628
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_2[2];
    uint8_t *****l_3765[8][2][2] = {{{&g_1977,&g_1977},{(void*)0,&g_1977}},{{(void*)0,(void*)0},{&g_1977,&g_1977}},{{(void*)0,&g_1977},{&g_1977,&g_1977}},{{&g_1977,&g_1977},{(void*)0,&g_1977}},{{(void*)0,&g_1977},{&g_1977,&g_1977}},{{&g_1977,&g_1977},{(void*)0,&g_1977}},{{&g_1977,(void*)0},{(void*)0,&g_1977}},{{(void*)0,&g_1977},{&g_1977,&g_1977}}};
    int32_t *l_3807 = &g_1989[0][0];
    uint32_t l_3826[5][1];
    float **l_3840 = &g_2947[0];
    float * const l_3847 = &g_3848;
    float * const *l_3846 = &l_3847;
    union U2 l_3879 = {0x68L};
    int32_t l_3913 = 0L;
    int32_t l_3916 = (-10L);
    uint32_t l_3917 = 0x44D300D0L;
    float **l_3935 = &g_2947[2];
    int32_t *l_3969 = &g_828;
    uint64_t * const **l_3988 = &g_257;
    uint64_t l_4008 = 0x53EC9083FEE0F0CALL;
    int32_t l_4042 = 0L;
    int32_t l_4047 = (-2L);
    int32_t l_4048 = 0x46514F97L;
    int32_t l_4049[2][10] = {{0L,4L,0L,4L,0L,4L,0L,4L,0L,4L},{0L,4L,0L,4L,0L,4L,0L,4L,0L,4L}};
    int64_t l_4093 = 1L;
    int32_t l_4143 = 0xF7BB3F60L;
    uint32_t l_4165[10][5] = {{4UL,0x6036CB78L,1UL,1UL,0x6036CB78L},{0UL,0UL,0xC72A9CDDL,0x6036CB78L,0xD0589183L},{0UL,1UL,0xC72A9CDDL,0x3DD64F9CL,0xC72A9CDDL},{0xD0589183L,0xD0589183L,1UL,0UL,0UL},{0UL,0UL,4UL,0UL,0UL},{0UL,0x197F755CL,0UL,0x3DD64F9CL,0xC7811917L},{4UL,0UL,0UL,0x6036CB78L,0xC7811917L},{1UL,0xD0589183L,0xD0589183L,1UL,0UL},{0xC72A9CDDL,1UL,0UL,0xC7811917L,0UL},{4UL,0x197F755CL,0x6036CB78L,0x197F755CL,4UL}};
    int8_t l_4194 = 0x7DL;
    uint64_t l_4197 = 0x185DD0340F112F4CLL;
    uint64_t * const ***l_4250 = &g_3780;
    uint64_t l_4285 = 0x54587319399761EELL;
    int16_t l_4315[8] = {2L,0x95B8L,2L,2L,0x95B8L,2L,2L,0x95B8L};
    int8_t l_4316 = (-4L);
    uint32_t l_4320 = 3UL;
    uint32_t l_4330[3][8][3];
    uint64_t l_4395 = 18446744073709551610UL;
    const int32_t *l_4409 = (void*)0;
    const int32_t **l_4408 = &l_4409;
    const int32_t ***l_4407 = &l_4408;
    const int32_t ****l_4406 = &l_4407;
    int32_t * const **l_4421 = (void*)0;
    int32_t * const ***l_4420 = &l_4421;
    int32_t * const ****l_4419 = &l_4420;
    uint32_t l_4452 = 0xA16A3827L;
    const int64_t l_4489 = (-1L);
    union U0 *l_4503 = &g_4180;
    uint16_t l_4588 = 6UL;
    int64_t l_4605 = (-1L);
    union U0 **l_4627 = &g_1196[0][1];
    union U0 ***l_4626 = &l_4627;
    union U0 ****l_4625 = &l_4626;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2[i] = 2UL;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
            l_3826[i][j] = 4294967295UL;
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
                l_4330[i][j][k] = 0UL;
        }
    }
    for (g_4 = 1; (g_4 >= 0); g_4 -= 1)
    { /* block id: 3 */
        const int8_t *l_3772 = &g_17[7];
        int i;
        l_3807 = func_5((safe_div_func_uint8_t_u_u((safe_unary_minus_func_uint8_t_u(func_13(g_16, l_2[g_4]))), (safe_mod_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((l_3765[0][1][0] != l_3765[0][1][0]) != (((*g_593) != g_1196[0][3]) | ((((l_2[1] , ((safe_add_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s((l_2[g_4] || l_2[0]), l_2[0])) != (***g_1978)), 0xFEL)) ^ g_1210)) > (*g_2043)) || l_2[g_4]) ^ 0UL))), (-5L))), l_2[g_4])), g_388)) != l_2[g_4]), l_2[g_4])))), (**g_3005), l_2[1], l_3772);
        for (g_1662.f4 = 0; (g_1662.f4 <= 6); g_1662.f4 += 1)
        { /* block id: 1621 */
            int i;
            g_3[(g_4 + 1)] &= (*g_76);
        }
    }
    for (g_1628 = 14; (g_1628 > 56); ++g_1628)
    { /* block id: 1627 */
        int32_t ****l_3814 = (void*)0;
        uint16_t l_3815 = 65527UL;
        int32_t l_3827 = 0xCF7F1D05L;
        float *l_3828 = &g_418;
        uint8_t l_3829 = 0xB3L;
        uint32_t l_3908 = 0x50D0296AL;
        int32_t l_3911 = 0x51EBA598L;
        int32_t l_3914 = 0x2ADC767AL;
        int32_t l_3915 = 1L;
        union U2 *l_3959 = &l_3879;
        uint64_t l_3989 = 18446744073709551606UL;
        int32_t l_4053 = 0x66151959L;
        int32_t l_4054[5];
        float l_4055[1];
        int8_t l_4062 = 0xFEL;
        int32_t *l_4066 = &g_2141;
        uint8_t l_4077[1];
        int32_t l_4116 = 0x952145F4L;
        uint32_t l_4144[2];
        uint32_t l_4249 = 0x2D46C6D6L;
        uint64_t l_4259 = 0x5BD1A025D2DD311DLL;
        uint64_t *l_4264[5][3][8] = {{{&l_4008,&l_4259,&l_4008,&l_4259,&l_4259,&l_3989,&g_90,(void*)0},{&g_90,&g_154,&g_90,&g_1927[2][2],(void*)0,(void*)0,&l_4259,(void*)0},{&g_90,&g_154,(void*)0,&g_154,&l_4259,&g_154,(void*)0,&g_154}},{{&l_4008,&l_2[1],&l_4008,&g_154,&g_90,&l_4259,&l_2[0],(void*)0},{&g_90,(void*)0,&g_90,&g_1927[2][2],&l_4008,&g_154,&l_2[0],(void*)0},{(void*)0,&g_1927[2][2],&l_4008,&l_4259,&l_4008,&g_1927[2][2],(void*)0,&g_154}},{{&l_4008,&g_1927[2][2],(void*)0,&g_154,&g_90,&g_154,&l_4259,&l_2[1]},{&g_90,(void*)0,&g_90,(void*)0,&g_90,&l_4259,&g_90,&l_4259},{&l_4008,&l_2[1],&l_4008,&l_2[1],&l_4008,&g_154,&g_90,&l_4259}},{{(void*)0,&g_154,&g_90,(void*)0,&l_4008,(void*)0,&g_90,&l_2[1]},{&g_90,&g_154,&g_90,&g_154,&g_90,&l_3989,&g_90,&g_154},{&l_4008,&l_4259,&l_4008,&l_4259,&l_4259,&l_3989,&g_90,(void*)0}},{{&g_90,&g_154,&g_90,&g_1927[2][2],(void*)0,(void*)0,&l_4259,(void*)0},{&g_90,&g_154,(void*)0,&g_154,&l_4259,&g_154,(void*)0,&g_154},{&l_4008,&l_2[1],&l_4008,&g_154,&g_90,&l_4259,&l_2[0],(void*)0}}};
        uint32_t *l_4274 = &g_2408;
        uint32_t **l_4273 = &l_4274;
        const uint8_t l_4279 = 0xDDL;
        uint16_t l_4280 = 0UL;
        int16_t l_4284 = 0x3EDEL;
        uint32_t l_4384 = 0xC449CC03L;
        int8_t l_4455 = 0L;
        int64_t l_4458 = 0x35E8EA5440B15052LL;
        uint32_t ***l_4511 = &g_2042;
        uint32_t ****l_4510[1][5] = {{&l_4511,&l_4511,&l_4511,&l_4511,&l_4511}};
        uint32_t *****l_4509 = &l_4510[0][2];
        union U0 **l_4624 = &l_4503;
        union U0 ***l_4623[5][9][2] = {{{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624}},{{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624}},{{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,&l_4624},{&l_4624,(void*)0}},{{&l_4624,&l_4624},{(void*)0,&l_4624},{&l_4624,(void*)0},{&l_4624,&l_4624},{(void*)0,&l_4624},{&l_4624,(void*)0},{&l_4624,&l_4624},{(void*)0,&l_4624},{&l_4624,(void*)0}},{{&l_4624,&l_4624},{(void*)0,&l_4624},{&l_4624,(void*)0},{&l_4624,&l_4624},{(void*)0,&l_4624},{&l_4624,(void*)0},{&l_4624,&l_4624},{(void*)0,&l_4624},{&l_4624,(void*)0}}};
        union U0 ****l_4622 = &l_4623[4][7][1];
        union U0 *****l_4621[1];
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_4054[i] = 0x907C8A6EL;
        for (i = 0; i < 1; i++)
            l_4055[i] = 0x4.Cp-1;
        for (i = 0; i < 1; i++)
            l_4077[i] = 0UL;
        for (i = 0; i < 2; i++)
            l_4144[i] = 0xBDB4F01BL;
        for (i = 0; i < 1; i++)
            l_4621[i] = &l_4622;
    }
    return (*l_3969);
}


/* ------------------------------------------ */
/* 
 * reads : g_2949 g_2950 g_2042 g_2043 g_3785 g_3145 g_1750.f4 g_3791 g_3793 g_75 g_76 g_4 g_727 g_527.f1 g_672 g_218 g_17 g_2005 g_1977 g_255 g_117.f2 g_1722 g_1723 g_1140 g_1728 g_3107 g_154 g_2947
 * writes: g_3778 g_350 g_3146 g_1750.f4 g_97 g_527.f1 g_1977 g_3261 g_2141 g_1662.f1 g_3108 g_154
 */
static int32_t * func_5(int64_t  p_6, int32_t  p_7, float  p_8, const int8_t * p_9)
{ /* block id: 1592 */
    uint64_t * const **l_3777 = &g_257;
    uint64_t * const ***l_3776 = &l_3777;
    uint64_t * const ****l_3775 = &l_3776;
    int32_t ***l_3783 = &g_75;
    int32_t ****l_3784 = &l_3783;
    union U2 **l_3788 = &g_3147;
    uint8_t ****l_3800[8] = {&g_1978,&g_1978,&g_1978,&g_1978,&g_1978,&g_1978,&g_1978,&g_1978};
    union U2 ****l_3804 = &g_3145;
    int i;
    if (((safe_add_func_uint64_t_u_u(((g_3778 = l_3775) == &l_3776), ((safe_rshift_func_int8_t_s_u((((*l_3784) = l_3783) == (*g_2949)), 6)) ^ (((((**g_2042) = 0x47B964F9L) , g_3785) != (void*)0) ^ (safe_add_func_int32_t_s_s(0L, (((*g_3145) = l_3788) != l_3788))))))) != p_6))
    { /* block id: 1597 */
        for (g_1750.f4 = (-12); (g_1750.f4 != 17); g_1750.f4 = safe_add_func_uint32_t_u_u(g_1750.f4, 1))
        { /* block id: 1600 */
            (*g_3793) = ((-0x1.7p+1) > g_3791);
        }
    }
    else
    { /* block id: 1603 */
        uint8_t *****l_3801 = &g_1977;
        union U2 ****l_3802[4] = {&g_3145,&g_3145,&g_3145,&g_3145};
        union U2 *****l_3803 = &g_3261[1];
        int32_t *l_3805 = &g_2141;
        int i;
lbl_3806:
        (*g_727) |= ((***l_3783) , p_7);
        (*g_3107) = ((safe_lshift_func_int16_t_s_s(((0x4EBFL < p_6) <= ((*g_1728) = (((safe_sub_func_uint16_t_u_u(((*g_672) > ((*l_3805) = (((***l_3783) <= (*p_9)) < (((safe_mod_func_uint16_t_u_u((****l_3784), (((l_3800[4] == ((*l_3801) = (*g_2005))) & (((*l_3803) = l_3802[1]) == l_3804)) , (*g_255)))) , (**g_1722)) == l_3805)))), 0x3A27L)) == 0xDFCD4CA2L) | 0x4BC4L))), 0)) , (void*)0);
        if (g_4)
            goto lbl_3806;
    }
    for (g_154 = 0; (g_154 <= 0); g_154 += 1)
    { /* block id: 1614 */
        int i;
        return g_2947[g_154];
    }
    return (**l_3783);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_13(int8_t * p_14, int32_t  p_15)
{ /* block id: 4 */
    union U2 l_23 = {0xD6L};
    int32_t ****l_2265 = (void*)0;
    const int64_t l_2286[10] = {0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL,0x32BE280712D52900LL};
    uint32_t * const l_2322 = &g_350;
    uint64_t *l_2323 = &g_1927[8][3];
    int32_t l_2362 = (-9L);
    int32_t l_2363 = 0x4F1CAD6BL;
    int32_t l_2365 = 0xF1BCAB05L;
    int32_t l_2366 = 0L;
    uint64_t l_2433 = 0x4B403FD4E721EED9LL;
    int32_t l_2457[4];
    int16_t l_2458 = 0xD747L;
    uint16_t ***l_2468[2];
    uint16_t ****l_2467[9][10][2] = {{{&l_2468[1],&l_2468[1]},{&l_2468[1],(void*)0},{&l_2468[1],&l_2468[0]},{(void*)0,&l_2468[0]},{&l_2468[0],(void*)0},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[1]},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[1]}},{{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[1],&l_2468[1]},{&l_2468[1],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],(void*)0},{&l_2468[0],(void*)0},{&l_2468[0],&l_2468[0]}},{{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],(void*)0},{&l_2468[0],(void*)0},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[1],&l_2468[1]},{&l_2468[1],&l_2468[0]},{&l_2468[0],&l_2468[0]}},{{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{(void*)0,&l_2468[1]},{&l_2468[0],&l_2468[0]},{(void*)0,(void*)0},{&l_2468[0],&l_2468[0]},{(void*)0,&l_2468[0]}},{{&l_2468[1],(void*)0},{&l_2468[1],&l_2468[1]},{&l_2468[1],(void*)0},{&l_2468[1],&l_2468[0]},{(void*)0,&l_2468[0]},{&l_2468[0],(void*)0},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[1]},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[0]}},{{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[1]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[1],&l_2468[1]},{&l_2468[1],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],(void*)0},{&l_2468[0],(void*)0}},{{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{&l_2468[0],(void*)0},{&l_2468[0],(void*)0},{&l_2468[0],&l_2468[0]},{&l_2468[0],&l_2468[0]},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[0]}},{{&l_2468[1],&l_2468[0]},{&l_2468[0],&l_2468[1]},{(void*)0,(void*)0},{&l_2468[0],&l_2468[1]},{&l_2468[0],(void*)0},{&l_2468[0],&l_2468[0]},{&l_2468[1],(void*)0},{&l_2468[0],&l_2468[0]},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[0]}},{{(void*)0,&l_2468[0]},{&l_2468[1],(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_2468[1],&l_2468[0]},{(void*)0,&l_2468[0]},{&l_2468[0],&l_2468[0]},{(void*)0,&l_2468[0]},{&l_2468[0],(void*)0},{&l_2468[1],&l_2468[0]}}};
    uint16_t *****l_2466 = &l_2467[6][8][0];
    int8_t l_2513 = 1L;
    uint32_t ***l_2518 = &g_2042;
    uint32_t l_2686 = 2UL;
    uint16_t l_2728 = 0xAF32L;
    uint8_t **l_2786 = &g_1735;
    uint32_t l_2793 = 0xC1DF0C11L;
    uint32_t l_2807 = 0x7020A090L;
    union U2 *l_2824 = &g_167[0][4];
    union U2 * const *l_2823 = &l_2824;
    union U2 * const ** const l_2822 = &l_2823;
    uint8_t * const * const *** const l_2830 = (void*)0;
    int8_t l_2840 = 1L;
    int8_t l_2841 = (-1L);
    int32_t l_2864 = 1L;
    uint32_t l_2880 = 0x8F9A8F21L;
    int32_t l_2895 = 0x005FF071L;
    float *l_2944[2][2] = {{&g_418,&g_418},{&g_418,&g_418}};
    int32_t l_3016[7] = {0xFA1C1645L,0x2BBEAEAAL,0xFA1C1645L,0xFA1C1645L,0x2BBEAEAAL,0xFA1C1645L,0xFA1C1645L};
    int64_t l_3021[2][9];
    uint8_t l_3116 = 0x36L;
    uint64_t * const **l_3233[4][7][8] = {{{(void*)0,(void*)0,&g_257,&g_257,(void*)0,(void*)0,(void*)0,(void*)0},{&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257},{&g_257,&g_257,(void*)0,&g_257,&g_257,&g_257,&g_257,(void*)0},{&g_257,&g_257,&g_257,&g_257,(void*)0,&g_257,(void*)0,&g_257},{&g_257,(void*)0,(void*)0,(void*)0,&g_257,&g_257,&g_257,&g_257},{(void*)0,&g_257,&g_257,(void*)0,&g_257,&g_257,(void*)0,&g_257},{&g_257,&g_257,&g_257,(void*)0,&g_257,&g_257,(void*)0,&g_257}},{{&g_257,&g_257,(void*)0,(void*)0,&g_257,&g_257,&g_257,(void*)0},{&g_257,(void*)0,&g_257,&g_257,&g_257,(void*)0,&g_257,&g_257},{(void*)0,&g_257,&g_257,(void*)0,&g_257,(void*)0,&g_257,&g_257},{&g_257,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,&g_257},{&g_257,&g_257,&g_257,&g_257,&g_257,(void*)0,&g_257,&g_257},{&g_257,&g_257,&g_257,(void*)0,&g_257,(void*)0,(void*)0,(void*)0},{&g_257,&g_257,&g_257,(void*)0,(void*)0,&g_257,&g_257,&g_257}},{{(void*)0,(void*)0,(void*)0,&g_257,&g_257,(void*)0,&g_257,(void*)0},{(void*)0,&g_257,&g_257,(void*)0,(void*)0,(void*)0,&g_257,&g_257},{(void*)0,(void*)0,&g_257,&g_257,&g_257,(void*)0,(void*)0,&g_257},{&g_257,&g_257,(void*)0,(void*)0,&g_257,&g_257,(void*)0,&g_257},{&g_257,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,&g_257},{(void*)0,&g_257,(void*)0,&g_257,&g_257,&g_257,&g_257,(void*)0},{(void*)0,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257}},{{&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257},{&g_257,&g_257,&g_257,(void*)0,(void*)0,(void*)0,(void*)0,&g_257},{&g_257,(void*)0,&g_257,&g_257,(void*)0,&g_257,&g_257,&g_257},{&g_257,&g_257,&g_257,&g_257,(void*)0,&g_257,&g_257,(void*)0},{&g_257,(void*)0,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257},{&g_257,&g_257,&g_257,(void*)0,&g_257,&g_257,(void*)0,&g_257},{(void*)0,&g_257,&g_257,(void*)0,(void*)0,(void*)0,&g_257,&g_257}}};
    uint64_t * const ***l_3232 = &l_3233[1][4][1];
    uint64_t l_3254 = 1UL;
    uint8_t l_3288 = 0xECL;
    int32_t **l_3343 = &g_3006;
    int64_t ** const l_3362 = (void*)0;
    int8_t l_3366 = 0L;
    int32_t l_3368 = 3L;
    uint8_t *l_3385 = &g_791;
    uint8_t *l_3386 = &g_1750.f4;
    uint32_t l_3425 = 0x79372EBEL;
    int32_t l_3483[2];
    int32_t l_3520 = 0x416C96F6L;
    uint8_t l_3555 = 255UL;
    uint32_t l_3566 = 8UL;
    int64_t **l_3597 = &g_672;
    int32_t l_3600[7];
    int32_t l_3609 = 6L;
    uint64_t l_3612 = 0x8B89E5B345FA244ELL;
    int32_t l_3626 = (-8L);
    int32_t *** const *l_3677[3][7] = {{&g_3004,(void*)0,&g_3004,&g_3004,(void*)0,&g_3004,&g_3004},{(void*)0,(void*)0,&g_3004,(void*)0,(void*)0,&g_3004,(void*)0},{(void*)0,&g_3004,&g_3004,(void*)0,&g_3004,&g_3004,(void*)0}};
    int32_t *** const **l_3676[5][3][7] = {{{&l_3677[2][5],(void*)0,&l_3677[0][4],&l_3677[2][2],&l_3677[0][4],(void*)0,&l_3677[2][5]},{&l_3677[1][4],&l_3677[0][4],&l_3677[0][0],&l_3677[1][0],&l_3677[0][4],&l_3677[2][6],&l_3677[1][6]},{&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[2][6],&l_3677[0][0],(void*)0}},{{&l_3677[0][4],&l_3677[2][1],&l_3677[0][0],&l_3677[2][5],(void*)0,(void*)0,&l_3677[0][4]},{&l_3677[2][6],&l_3677[0][4],&l_3677[0][4],&l_3677[0][3],(void*)0,&l_3677[0][4],&l_3677[0][4]},{&l_3677[2][2],&l_3677[0][0],&l_3677[0][6],(void*)0,&l_3677[2][5],&l_3677[0][4],(void*)0}},{{(void*)0,&l_3677[0][4],(void*)0,(void*)0,&l_3677[0][4],(void*)0,&l_3677[1][2]},{&l_3677[1][0],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][0],&l_3677[0][4]},{&l_3677[1][6],&l_3677[2][6],&l_3677[0][4],&l_3677[1][2],&l_3677[1][0],&l_3677[2][6],&l_3677[2][5]}},{{&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[1][2],(void*)0,(void*)0},{&l_3677[0][4],&l_3677[0][4],&l_3677[1][0],&l_3677[0][4],&l_3677[2][1],&l_3677[0][4],&l_3677[0][4]},{&l_3677[0][4],&l_3677[0][0],&l_3677[1][4],&l_3677[0][4],(void*)0,&l_3677[2][1],&l_3677[2][1]}},{{&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],&l_3677[0][4],(void*)0},{&l_3677[0][4],(void*)0,&l_3677[1][2],&l_3677[0][3],&l_3677[0][4],&l_3677[2][5],&l_3677[0][4]},{&l_3677[0][4],&l_3677[0][4],&l_3677[0][0],(void*)0,&l_3677[0][6],&l_3677[0][4],&l_3677[1][2]}}};
    uint32_t l_3723 = 0x1DB7A6EBL;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_2457[i] = 0x3639AE4BL;
    for (i = 0; i < 2; i++)
        l_2468[i] = &g_1716;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
            l_3021[i][j] = 0xFC960E94C30A6950LL;
    }
    for (i = 0; i < 2; i++)
        l_3483[i] = 0x74DA3568L;
    for (i = 0; i < 7; i++)
        l_3600[i] = 0xA044D28CL;
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_75 g_1288 g_345 g_2042 g_2043 g_350 g_1722 g_1723 g_1140 g_1432 g_1429 g_2192 g_1735 g_527.f4 g_17 g_982.f0 g_727 g_255 g_117.f2 g_1448 g_417 g_418 g_1449 g_76 g_574 g_527.f1 g_164.f1 g_1870
 * writes: g_164.f1 g_76 g_350 g_1140 g_527.f4 g_2163 g_1429 g_1870 g_94 g_1449 g_527.f1 g_117.f2 g_1448 g_418
 */
static int32_t  func_18(int32_t * p_19, int8_t * p_20)
{ /* block id: 893 */
    const union U0 *l_2167 = (void*)0;
    int32_t l_2168 = 0x03458E53L;
    union U2 l_2169 = {0xE1L};
    int32_t l_2188[5];
    uint16_t l_2216 = 1UL;
    int32_t ** const *l_2243 = &g_75;
    int8_t *****l_2252 = &g_1599;
    union U2 *l_2257 = &g_2258;
    uint32_t l_2259[1][7][8] = {{{0xEBE62376L,0xEBE62376L,0x6F7DA083L,0xA1530813L,1UL,0x917601D9L,0x7FF88ED9L,0UL},{1UL,0x917601D9L,0x7FF88ED9L,0UL,0x0F90F1C0L,3UL,0x0F90F1C0L,0UL},{0x917601D9L,0x6F7DA083L,0x917601D9L,0xA1530813L,0x23408CDEL,0UL,1UL,3UL},{4294967295UL,5UL,0xA1530813L,0xEBE62376L,0x7FF88ED9L,0x23408CDEL,0x23408CDEL,0x7FF88ED9L},{4294967295UL,0x0F90F1C0L,0x0F90F1C0L,4294967295UL,0x23408CDEL,0xEBE62376L,0UL,0UL},{0x917601D9L,0x7D4496F3L,0xEBE62376L,1UL,0x0F90F1C0L,0x6F7DA083L,0UL,0x6F7DA083L},{1UL,0x7D4496F3L,0UL,0x7D4496F3L,1UL,0xEBE62376L,4294967295UL,0xF34F1A14L}}};
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_2188[i] = 5L;
lbl_2224:
    l_2167 = l_2167;
    if ((*p_19))
    { /* block id: 895 */
        uint8_t l_2170 = 255UL;
        int32_t l_2172[3];
        int32_t l_2187 = 0xE56A0368L;
        uint32_t l_2189 = 0UL;
        int i;
        for (i = 0; i < 3; i++)
            l_2172[i] = 0L;
        for (g_164.f1 = 0; (g_164.f1 <= 9); g_164.f1 += 1)
        { /* block id: 898 */
            int64_t l_2181 = 1L;
            int32_t l_2185 = 0xFECAEFE6L;
            int32_t l_2186[3];
            uint16_t * const *l_2207 = (void*)0;
            uint16_t * const **l_2206 = &l_2207;
            uint16_t * const ***l_2205[7][6][1] = {{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}},{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}},{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}},{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}},{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}},{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}},{{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206},{&l_2206}}};
            int16_t *l_2222 = (void*)0;
            int16_t *l_2223 = &g_1449;
            float *l_2250 = &g_418;
            int64_t l_2254 = (-1L);
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2186[i] = 1L;
            if (l_2168)
                break;
            (*g_75) = p_19;
            if (((*g_1288) , (((**g_2042) |= l_2170) ^ 3UL)))
            { /* block id: 902 */
                return (*p_19);
            }
            else
            { /* block id: 904 */
                union U2 l_2171 = {-1L};
                int32_t l_2178 = 0x88C485E2L;
                int32_t l_2179 = 0xA51F8956L;
                int32_t l_2180 = (-1L);
                int32_t l_2183[9] = {0xC7C9CA9EL,(-5L),(-5L),0xC7C9CA9EL,(-5L),(-5L),0xC7C9CA9EL,(-5L),(-5L)};
                int32_t l_2215 = 0x75C898DBL;
                int i;
                (*g_1723) = (l_2171 , (**g_1722));
                for (g_527.f4 = 0; (g_527.f4 <= 0); g_527.f4 += 1)
                { /* block id: 908 */
                    int8_t l_2173 = 0x82L;
                    int32_t l_2182 = 0L;
                    int32_t l_2184[6][7] = {{8L,8L,8L,8L,8L,8L,8L},{8L,0xD30483F3L,8L,0xD30483F3L,8L,0xD30483F3L,8L},{8L,8L,8L,8L,8L,8L,8L},{8L,0xD30483F3L,8L,0xD30483F3L,8L,0xD30483F3L,8L},{8L,8L,8L,8L,8L,8L,8L},{8L,0xD30483F3L,8L,0xD30483F3L,8L,0xD30483F3L,8L}};
                    uint16_t l_2209 = 5UL;
                    int32_t *l_2212 = &l_2186[1];
                    int32_t *l_2213 = &g_1989[0][0];
                    int32_t *l_2214[6][10][4] = {{{&g_4,&g_828,&l_2179,&l_2184[5][0]},{&l_2179,&l_2184[5][0],&l_2180,&l_2184[5][0]},{&l_2180,&g_828,&g_117[4][6].f1,&l_2179},{&l_2184[5][0],(void*)0,&g_4,&g_117[4][6].f1},{&l_2184[5][0],(void*)0,&l_2180,&l_2180},{&l_2184[5][0],&l_2184[5][0],&g_4,(void*)0},{&l_2179,&l_2180,&l_2184[5][0],&g_828},{(void*)0,&l_2183[7],&l_2184[5][0],&l_2184[5][0]},{&l_2183[7],&l_2183[7],&l_2183[7],&g_828},{&l_2183[7],&l_2180,&l_2180,(void*)0}},{{(void*)0,&l_2184[5][0],(void*)0,&l_2180},{&g_117[4][6].f1,(void*)0,(void*)0,&g_117[4][6].f1},{(void*)0,&g_828,&l_2180,&l_2183[7]},{&l_2183[7],&g_1989[1][1],&l_2183[7],&g_4},{&l_2183[7],&g_4,&l_2184[5][0],&g_4},{(void*)0,&g_1989[1][1],&l_2184[5][0],&l_2183[7]},{&l_2179,&g_828,&g_4,&g_117[4][6].f1},{&l_2184[5][0],(void*)0,&l_2180,&l_2180},{&l_2184[5][0],&l_2184[5][0],&g_4,(void*)0},{&l_2179,&l_2180,&l_2184[5][0],&g_828}},{{(void*)0,&l_2183[7],&l_2184[5][0],&l_2184[5][0]},{&l_2183[7],&l_2183[7],&l_2183[7],&g_828},{&l_2183[7],&l_2180,&l_2180,(void*)0},{(void*)0,&l_2184[5][0],(void*)0,&l_2180},{&g_117[4][6].f1,(void*)0,(void*)0,&g_117[4][6].f1},{(void*)0,&g_828,&l_2180,&l_2183[7]},{&l_2183[7],&g_1989[1][1],&l_2183[7],&g_4},{&l_2183[7],&g_4,&l_2184[5][0],&g_4},{(void*)0,&g_1989[1][1],&l_2184[5][0],&l_2183[7]},{&l_2179,&g_828,&g_4,&g_117[4][6].f1}},{{&l_2184[5][0],(void*)0,&l_2180,&l_2180},{&l_2184[5][0],&l_2184[5][0],&g_4,(void*)0},{&l_2179,&l_2180,&l_2184[5][0],&g_828},{(void*)0,&l_2183[7],&l_2184[5][0],&l_2184[5][0]},{&l_2183[7],&l_2183[7],&l_2183[7],&g_828},{&l_2183[7],&l_2180,&l_2180,(void*)0},{(void*)0,&l_2184[5][0],(void*)0,&l_2180},{&g_117[4][6].f1,(void*)0,(void*)0,&g_117[4][6].f1},{(void*)0,&g_828,&l_2180,&l_2183[7]},{&l_2183[7],&g_1989[1][1],&l_2183[7],&g_4}},{{&l_2183[7],&g_4,&l_2184[5][0],&g_4},{(void*)0,&g_1989[1][1],&l_2184[5][0],&l_2183[7]},{&l_2179,&g_828,&g_4,&g_117[4][6].f1},{&l_2184[5][0],(void*)0,&l_2180,&l_2180},{&l_2184[5][0],&l_2184[5][0],&g_4,(void*)0},{&l_2179,&l_2180,&l_2184[5][0],&g_828},{(void*)0,&l_2183[7],&l_2184[5][0],&l_2184[5][0]},{&l_2183[7],&l_2183[7],&l_2183[7],&g_828},{&l_2183[7],&l_2180,&l_2180,(void*)0},{(void*)0,&l_2184[5][0],(void*)0,&l_2180}},{{&g_117[4][6].f1,(void*)0,(void*)0,&g_117[4][6].f1},{(void*)0,&g_1989[1][1],&l_2184[5][0],&l_2180},{(void*)0,&l_2180,&l_2180,&l_2183[7]},{&l_2180,&l_2183[7],&l_2179,&l_2183[7]},{&g_117[4][6].f1,&l_2180,&g_4,&l_2180},{&l_2183[7],&g_1989[1][1],&l_2183[7],&l_2184[5][0]},{&g_4,&g_117[4][6].f1,(void*)0,(void*)0},{&g_4,&g_4,&l_2183[7],&g_828},{&l_2183[7],(void*)0,&g_4,&g_1989[1][1]},{&g_117[4][6].f1,(void*)0,&l_2179,&g_4}}};
                    int i, j, k;
                    if (l_2170)
                        break;
                    for (g_2163 = 0; (g_2163 <= 9); g_2163 += 1)
                    { /* block id: 912 */
                        int32_t *l_2174 = &g_117[4][6].f1;
                        int32_t *l_2175 = &l_2168;
                        int32_t *l_2176 = &l_2172[0];
                        int32_t *l_2177[6][10][4] = {{{&g_25,(void*)0,&g_2141,(void*)0},{&g_25,&g_117[4][6].f1,&g_4,&g_2141},{&l_2172[2],&g_117[4][6].f1,&g_164.f1,(void*)0},{&g_117[4][6].f1,(void*)0,&l_2168,&g_117[4][6].f1},{(void*)0,&g_2141,&g_951.f1,&g_164.f1},{(void*)0,&g_4,&g_164.f1,&g_2141},{&g_4,&g_25,(void*)0,&g_2141},{&l_2168,&l_2172[0],(void*)0,&g_828},{&g_1967,&l_2172[2],&g_1967,&l_2172[0]},{&g_951.f1,(void*)0,&l_2168,&g_25}},{{&g_4,&g_117[4][6].f1,(void*)0,&g_1989[0][0]},{&g_2141,&g_1967,&g_4,&g_1967},{&g_4,&g_4,(void*)0,&g_2141},{&g_828,&g_25,&g_951.f1,&g_828},{&g_2141,(void*)0,&g_25,(void*)0},{&g_117[4][6].f1,(void*)0,&g_828,&g_1967},{&g_1989[0][5],&g_951.f1,&g_4,&g_1967},{&l_2168,&g_828,&l_2172[0],(void*)0},{&g_25,&g_1967,&g_25,(void*)0},{&g_1989[1][3],&g_25,&g_164.f1,&l_2168}},{{&l_2172[0],&g_1967,(void*)0,&g_1967},{&g_951.f1,&g_1967,(void*)0,(void*)0},{&g_828,&g_117[4][6].f1,&g_1989[0][0],&g_951.f1},{&g_2141,&l_2172[0],&l_2168,&l_2172[0]},{&g_2141,&g_1989[0][5],&g_1989[0][0],&g_2141},{&g_828,&l_2172[0],(void*)0,&g_1989[1][3]},{&g_951.f1,&g_164.f1,(void*)0,&g_2141},{&l_2172[0],(void*)0,&g_164.f1,&g_4},{&g_1989[1][3],&g_2141,&g_25,&g_2141},{&g_25,&g_4,&l_2172[0],(void*)0}},{{&l_2168,&g_1989[0][0],&g_4,&l_2168},{&g_1989[0][5],&g_117[4][6].f1,&g_828,&g_1967},{&g_117[4][6].f1,&g_4,&g_25,&g_117[4][6].f1},{&g_2141,&g_1989[1][3],&g_951.f1,&g_4},{&g_828,&g_4,(void*)0,&l_2168},{&g_4,&g_164.f1,&g_4,&g_2141},{&g_2141,(void*)0,(void*)0,&g_2141},{&g_4,&l_2172[2],&l_2168,(void*)0},{&g_951.f1,&l_2172[0],&g_1967,&l_2168},{&g_1989[1][3],&g_25,&g_25,&g_1967}},{{&g_1967,(void*)0,&g_828,&l_2172[1]},{(void*)0,&g_1989[1][3],&g_951.f1,&g_2141},{(void*)0,&g_117[4][6].f1,&g_117[4][6].f1,(void*)0},{&g_2141,&g_2141,&g_1967,(void*)0},{&g_25,(void*)0,&g_828,(void*)0},{(void*)0,(void*)0,&l_2172[1],(void*)0},{&g_4,(void*)0,&g_164.f1,(void*)0},{&l_2168,&g_2141,&l_2172[0],(void*)0},{&g_4,&g_117[4][6].f1,&l_2172[0],&g_2141},{&g_25,&g_1989[1][3],&g_2141,&l_2172[1]}},{{&g_25,(void*)0,&g_2141,&g_1967},{&g_951.f1,&g_25,&l_2168,&l_2168},{&g_1989[1][0],&l_2168,&g_4,(void*)0},{&g_2141,(void*)0,&g_1967,&g_951.f1},{&g_951.f1,(void*)0,(void*)0,&g_4},{&l_2172[0],&g_164.f1,&g_2141,&g_1967},{&l_2168,&g_1967,&g_2141,(void*)0},{&g_4,&g_951.f1,&g_4,&g_1989[0][5]},{&g_117[4][6].f1,(void*)0,&g_164.f1,&l_2172[0]},{(void*)0,&g_25,&g_951.f1,&g_828}}};
                        int i, j, k;
                        ++l_2189;
                    }
                    for (l_2180 = 3; (l_2180 <= 9); l_2180 += 1)
                    { /* block id: 917 */
                        uint16_t * const ****l_2208 = &l_2205[5][1][0];
                        float l_2210 = (-0x1.9p-1);
                        int32_t l_2211 = (-4L);
                        if ((*p_19))
                            break;
                        (*g_2192) = (*g_1432);
                        (*g_75) = &l_2187;
                        if ((*p_19))
                            continue;
                    }
                    ++l_2216;
                    for (g_1870 = 0; (g_1870 <= 9); g_1870 += 1)
                    { /* block id: 928 */
                        return l_2180;
                    }
                }
            }
            (*g_727) = ((((l_2188[4] || ((safe_mul_func_uint8_t_u_u((*g_1735), (*p_20))) >= l_2169.f0)) , ((*l_2223) = (g_94[2] = (+0x0B0BL)))) || g_982.f0) && 1L);
            for (l_2216 = 0; (l_2216 <= 0); l_2216 += 1)
            { /* block id: 938 */
                const int8_t l_2229 = 0L;
                if (g_527.f4)
                    goto lbl_2224;
                l_2186[0] = (safe_add_func_uint16_t_u_u(((*g_255)--), l_2229));
                for (g_1448 = 0; (g_1448 <= 0); g_1448 += 1)
                { /* block id: 944 */
                    int16_t *l_2242 = &g_94[2];
                    int32_t l_2251 = 0x59B8D13DL;
                    int32_t l_2253 = (-10L);
                    (*g_727) ^= ((safe_mul_func_float_f_f((((*l_2250) = (safe_div_func_float_f_f(((*g_417) > ((safe_sub_func_float_f_f(((((safe_sub_func_uint64_t_u_u(((l_2252 = ((safe_lshift_func_int16_t_s_s((((safe_mul_func_int16_t_s_s(0x6A71L, ((*l_2242) = ((*l_2223) ^= l_2172[0])))) ^ (l_2243 != (void*)0)) || (safe_add_func_int8_t_s_s((((((*l_2223) = (safe_add_func_int16_t_s_s(0x46CBL, l_2181))) >= (safe_mod_func_int32_t_s_s((l_2251 = (0x1BBFE586E74E6FDFLL < (l_2250 != (void*)0))), (***l_2243)))) , 0x28FAL) && l_2229), l_2186[0]))), l_2170)) , (void*)0)) == (void*)0), l_2229)) != 0xBD4E5E22L) , 0x0.Ap+1) < l_2186[2]), 0xB.C51397p+58)) < l_2253)), (***l_2243)))) == g_574), l_2229)) , l_2254);
                    return l_2186[1];
                }
            }
        }
    }
    else
    { /* block id: 956 */
        int32_t *l_2256[9][8][3] = {{{&g_951.f1,&g_828,&g_25},{&g_164.f1,&l_2168,&g_1967},{(void*)0,&g_117[4][6].f1,&g_25},{&l_2168,&g_164.f1,&g_1967},{&l_2168,&l_2168,&g_951.f1},{&g_4,(void*)0,&g_1967},{(void*)0,&g_117[4][6].f1,&g_1989[0][0]},{&g_1989[3][0],&l_2168,&g_1989[0][0]}},{{&g_1967,(void*)0,&g_1989[0][0]},{&g_25,(void*)0,&g_1967},{&g_1989[0][0],&g_828,&g_951.f1},{&l_2188[4],&g_117[4][6].f1,&g_1967},{(void*)0,&l_2168,&g_25},{(void*)0,&l_2188[4],&g_1967},{&l_2188[2],&g_1967,&g_25},{&g_4,&g_164.f1,&g_164.f1}},{{&g_25,&g_117[4][6].f1,&l_2188[3]},{&g_828,&l_2168,&g_117[4][6].f1},{&l_2168,&g_25,&l_2188[3]},{&l_2188[4],&l_2168,&g_1967},{&g_951.f1,&g_25,(void*)0},{&g_164.f1,&l_2168,(void*)0},{(void*)0,&g_117[4][6].f1,&g_951.f1},{&g_25,&g_164.f1,&g_1989[0][1]}},{{(void*)0,&g_1967,&g_951.f1},{&g_1989[0][0],&l_2188[4],&g_25},{&g_828,&l_2168,&l_2188[3]},{&g_1989[3][0],&g_117[4][6].f1,&g_1989[3][0]},{&l_2188[2],&g_828,&g_951.f1},{&l_2188[4],(void*)0,&g_164.f1},{(void*)0,(void*)0,(void*)0},{(void*)0,&l_2168,&g_1967}},{{(void*)0,&g_117[4][6].f1,(void*)0},{&l_2188[4],(void*)0,&g_1989[0][1]},{&l_2188[2],&l_2168,(void*)0},{&g_1989[3][0],&g_1989[3][0],&l_2188[3]},{&g_951.f1,(void*)0,&l_2168},{&g_951.f1,&l_2168,&g_1967},{(void*)0,&g_951.f1,&g_117[4][6].f1},{&l_2188[3],&l_2188[3],&g_1989[0][0]}},{{&g_1989[0][0],&g_25,(void*)0},{&g_828,&l_2188[0],(void*)0},{(void*)0,&l_2188[2],&l_2168},{&g_164.f1,&g_828,(void*)0},{&g_1989[2][3],(void*)0,(void*)0},{&g_1989[0][1],&g_1989[3][0],&g_1989[0][0]},{(void*)0,&g_828,&g_117[4][6].f1},{&g_1967,&g_2141,&g_1967}},{{(void*)0,(void*)0,&l_2168},{(void*)0,&g_164.f1,&l_2188[3]},{&g_1989[0][0],&g_1989[0][0],&g_1989[0][0]},{&l_2188[3],&l_2168,(void*)0},{&l_2188[3],&l_2188[2],&g_951.f1},{&l_2188[3],&l_2188[3],&g_117[4][6].f1},{(void*)0,(void*)0,(void*)0},{&g_1989[2][1],&l_2188[3],&g_828}},{{&g_1989[0][0],&l_2188[2],&g_951.f1},{&g_1967,&l_2168,&g_1989[2][1]},{&g_1989[2][3],&g_1989[0][0],&g_4},{&g_1967,&g_164.f1,&g_1989[0][0]},{&l_2168,(void*)0,(void*)0},{&g_1989[3][0],&g_2141,(void*)0},{(void*)0,&g_828,(void*)0},{(void*)0,&g_1989[3][0],&g_1989[3][5]}},{{(void*)0,(void*)0,&g_1989[0][0]},{&g_951.f1,&g_828,&g_117[4][6].f1},{&g_25,&l_2188[2],&g_117[4][6].f1},{&g_951.f1,&l_2188[0],(void*)0},{(void*)0,&g_25,&g_4},{(void*)0,&l_2188[3],&g_828},{(void*)0,&g_951.f1,(void*)0},{&g_1989[3][0],&l_2168,(void*)0}}};
        int i, j, k;
        l_2168 &= (*p_19);
    }
    l_2257 = &l_2169;
    return l_2259[0][5][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_255 g_117.f2 g_25 g_1662.f2 g_76 g_4 g_1788.f0 g_1288 g_345 g_672 g_1735 g_75 g_90 g_1628 g_94 g_1998 g_1870 g_1750.f4
 * writes: g_25 g_1662.f2 g_1750.f4 g_1977 g_218 g_527.f4 g_1989 g_76 g_90 g_1628 g_97 g_1870
 */
static int32_t * func_21(union U2  p_22)
{ /* block id: 5 */
    int32_t *l_24 = &g_25;
    int32_t l_26[6];
    int32_t *l_27[5] = {&l_26[4],&l_26[4],&l_26[4],&l_26[4],&l_26[4]};
    int8_t l_28[5][5] = {{1L,1L,0x3CL,1L,1L},{0x69L,5L,0x69L,0x69L,5L},{1L,0x8AL,0x8AL,1L,0x8AL},{5L,5L,3L,5L,5L},{0x8AL,1L,0x8AL,0x8AL,1L}};
    uint16_t l_29 = 8UL;
    const int32_t *l_1771 = &g_1772;
    int32_t l_1793 = 0xA45E2904L;
    uint16_t l_1796[10] = {0x3426L,0x8DE1L,0x8DE1L,0x3426L,0x8DE1L,0x8DE1L,0x3426L,0x8DE1L,0x8DE1L,0x3426L};
    uint16_t ***l_1805 = &g_1716;
    uint16_t ****l_1804[10] = {(void*)0,&l_1805,(void*)0,&l_1805,&l_1805,(void*)0,&l_1805,(void*)0,&l_1805,&l_1805};
    int16_t l_1806 = 0x9288L;
    uint64_t l_1829 = 0x162F0078F5665231LL;
    int64_t **l_1851[1][5][9] = {{{&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672},{&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672},{&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672},{&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672},{&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672,&g_672}}};
    uint32_t *l_1962[9][1][6] = {{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}}};
    uint32_t ** const l_1961[4][2] = {{&l_1962[3][0][4],&l_1962[3][0][4]},{&l_1962[3][0][5],&l_1962[3][0][4]},{&l_1962[3][0][4],&l_1962[3][0][5]},{&l_1962[3][0][4],&l_1962[3][0][4]}};
    int16_t l_1969 = 2L;
    uint16_t l_2025 = 65535UL;
    uint8_t l_2028 = 0xF6L;
    const union U2 l_2078 = {0x64L};
    uint8_t l_2121 = 0x5FL;
    int32_t *l_2158 = &g_766;
    uint16_t * const l_2162 = &g_2163;
    uint16_t * const *l_2161 = &l_2162;
    int32_t *l_2166 = &g_4;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_26[i] = 0xCA6C8490L;
    l_29++;
lbl_1992:
    for (g_25 = 0; (g_25 < (-7)); g_25 = safe_sub_func_int64_t_s_s(g_25, 7))
    { /* block id: 9 */
        const int64_t l_57 = 1L;
        uint32_t *l_746[7][5] = {{&g_350,(void*)0,(void*)0,(void*)0,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,(void*)0,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,(void*)0,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,(void*)0,&g_350}};
        int32_t l_747[7] = {0x6EF5730FL,0x6EF5730FL,0x6EF5730FL,0x6EF5730FL,0x6EF5730FL,0x6EF5730FL,0x6EF5730FL};
        const int8_t *l_748 = &g_333.f0;
        int8_t *l_774 = &g_17[5];
        const int32_t *l_1770 = (void*)0;
        const int32_t **l_1769[1];
        uint32_t l_1786 = 1UL;
        int8_t l_1819 = 0x5CL;
        const int8_t * const *l_1909 = (void*)0;
        const int8_t * const **l_1908 = &l_1909;
        const int8_t * const ***l_1907 = &l_1908;
        int32_t *l_1913 = &l_26[4];
        int8_t **l_1923 = (void*)0;
        int8_t **l_1924 = &g_422;
        int i, j;
        for (i = 0; i < 1; i++)
            l_1769[i] = &l_1770;
    }
    for (g_1662.f2 = 24; (g_1662.f2 == 48); ++g_1662.f2)
    { /* block id: 798 */
        float l_1936[2][9] = {{0xB.641C2Ep+66,(-0x7.Fp-1),0x0.1p-1,0x0.1p-1,(-0x7.Fp-1),0xB.641C2Ep+66,0xB.03B455p+47,(-0x7.Fp-1),0xB.03B455p+47},{0xB.641C2Ep+66,(-0x7.Fp-1),0x0.1p-1,0x0.1p-1,(-0x7.Fp-1),0xB.641C2Ep+66,0xB.03B455p+47,(-0x7.Fp-1),0xB.03B455p+47}};
        int32_t l_1938 = 0x2CB9261EL;
        uint64_t *** const **l_1948[4][1];
        uint32_t **l_1963 = (void*)0;
        int32_t l_1964 = 9L;
        int32_t l_1965 = 0xE6287A2FL;
        int32_t l_1966[10][1] = {{(-1L)},{0x94EAD9BCL},{0x94EAD9BCL},{(-1L)},{0x94EAD9BCL},{0x94EAD9BCL},{(-1L)},{0x94EAD9BCL},{0x94EAD9BCL},{(-1L)}};
        float l_1968 = (-0x5.1p+1);
        uint32_t l_1970 = 0x9662B17DL;
        uint8_t ***l_1976[6][8][5] = {{{&g_1905,&g_1905,(void*)0,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{(void*)0,&g_1905,(void*)0,&g_1905,(void*)0},{&g_1905,&g_1905,(void*)0,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905}},{{(void*)0,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,(void*)0,(void*)0,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905}},{{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{(void*)0,&g_1905,&g_1905,&g_1905,(void*)0},{&g_1905,&g_1905,(void*)0,&g_1905,&g_1905},{(void*)0,&g_1905,(void*)0,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,(void*)0},{&g_1905,(void*)0,&g_1905,&g_1905,&g_1905}},{{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{(void*)0,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,(void*)0,&g_1905,&g_1905},{&g_1905,&g_1905,(void*)0,&g_1905,&g_1905},{(void*)0,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905}},{{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,(void*)0},{&g_1905,(void*)0,&g_1905,&g_1905,&g_1905},{(void*)0,&g_1905,&g_1905,&g_1905,(void*)0},{&g_1905,&g_1905,(void*)0,&g_1905,&g_1905},{(void*)0,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905}},{{&g_1905,&g_1905,&g_1905,&g_1905,(void*)0},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,(void*)0},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905},{&g_1905,&g_1905,&g_1905,&g_1905,&g_1905}}};
        uint8_t ****l_1975 = &l_1976[4][7][3];
        uint8_t ****l_1979 = (void*)0;
        uint32_t *l_1988[10] = {&l_1970,&l_1970,&l_1970,&l_1970,&l_1970,&l_1970,&l_1970,&l_1970,&l_1970,&l_1970};
        int8_t l_1990 = 1L;
        float l_1991 = (-0x2.3p-1);
        int32_t *l_2008[1];
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
                l_1948[i][j] = &g_919;
        }
        for (i = 0; i < 1; i++)
            l_2008[i] = (void*)0;
        for (g_1750.f4 = 0; (g_1750.f4 <= 56); ++g_1750.f4)
        { /* block id: 801 */
            float l_1932 = 0x1.6p-1;
            int32_t l_1933 = 0x4AD2D663L;
            int32_t l_1934 = (-10L);
            int32_t l_1935[7][3][4] = {{{(-6L),0xBF2A3946L,0xBF2A3946L,(-6L)},{(-6L),1L,0x21A54D21L,0x2C1544AFL},{(-4L),(-6L),1L,(-6L)}},{{0L,0x57942738L,0L,(-6L)},{1L,(-6L),(-4L),0x2C1544AFL},{0x21A54D21L,1L,(-6L),(-6L)}},{{0xBF2A3946L,0xBF2A3946L,(-6L),0L},{0x21A54D21L,0xC29CE15BL,(-4L),1L},{1L,(-4L),0L,(-4L)}},{{0L,(-4L),1L,1L},{(-4L),0xC29CE15BL,0x21A54D21L,0L},{(-6L),0xBF2A3946L,0xBF2A3946L,(-6L)}},{{(-6L),1L,0x21A54D21L,0x2C1544AFL},{(-4L),(-6L),1L,(-6L)},{0L,0x57942738L,0L,(-6L)}},{{1L,(-6L),(-4L),0x2C1544AFL},{0x21A54D21L,1L,(-6L),(-6L)},{0xBF2A3946L,0xBF2A3946L,(-6L),0L}},{{0x21A54D21L,0xC29CE15BL,(-4L),1L},{1L,(-4L),0L,(-4L)},{0L,(-4L),1L,1L}}};
            float l_1937 = 0x1.3p+1;
            uint32_t l_1939 = 0xF4A18379L;
            int16_t *l_1946 = &l_1806;
            int i, j, k;
            l_1939--;
            l_1938 = ((p_22.f0 ^ (safe_add_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((*g_255), ((*l_1946) = g_25))), ((((+((&g_919 == l_1948[3][0]) , 1UL)) > (safe_div_func_int32_t_s_s(p_22.f0, (safe_div_func_uint8_t_u_u((((safe_mod_func_uint16_t_u_u((((safe_rshift_func_int8_t_s_s(l_1934, 6)) , (safe_div_func_uint32_t_u_u((safe_sub_func_int16_t_s_s((65531UL <= g_1662.f2), l_1939)), 0x3ED9684BL))) | p_22.f0), p_22.f0)) , l_1961[3][0]) == l_1963), p_22.f0))))) >= (*g_76)) , g_1788[3][3][1].f0)))) != p_22.f0);
        }
        --l_1970;
        if ((safe_sub_func_uint8_t_u_u(((*g_1288) , (((*g_672) = ((g_1977 = l_1975) != (l_1979 = &l_1976[4][7][3]))) || p_22.f0)), ((safe_rshift_func_uint8_t_u_u(((*g_1735) = (l_1965 < p_22.f0)), ((safe_sub_func_int64_t_s_s(((safe_mul_func_float_f_f(((g_1989[0][0] = (0xFBE8FF40E134ECDALL | ((safe_lshift_func_uint16_t_u_s(65535UL, l_1966[6][0])) , l_1966[0][0]))) , l_1990), 0xD.5EA5C7p-51)) , l_1965), p_22.f0)) | 0x70A6L))) ^ 0x9C86L))))
        { /* block id: 812 */
            if (l_29)
                goto lbl_1992;
            if ((*l_24))
                break;
        }
        else
        { /* block id: 815 */
            uint64_t l_1999 = 0UL;
            int32_t l_2018 = 0L;
            int32_t l_2051 = 8L;
            int32_t l_2059 = 1L;
            int32_t l_2060 = 0xBB78260EL;
            int32_t l_2063 = (-4L);
            int32_t *l_2159 = &g_712;
            (*g_75) = &l_1793;
            for (g_90 = (-1); (g_90 >= 54); g_90++)
            { /* block id: 819 */
                int8_t l_2026 = 0x96L;
                int32_t l_2029 = 0x491C0BDDL;
                int32_t l_2057[1];
                const union U0 *l_2085 = &g_164;
                uint16_t ****l_2086 = &l_1805;
                uint64_t *l_2092[1];
                uint32_t l_2142[3][4][7] = {{{0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L},{0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L},{0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L},{0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L}},{{0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L},{0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L},{0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L},{0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L}},{{0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L},{0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L},{0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L,0x07AF9CA3L,0x77E06985L},{0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L,0x68906353L}}};
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_2057[i] = 0x2F547772L;
                for (i = 0; i < 1; i++)
                    l_2092[i] = &g_1927[1][0];
                for (g_1628 = (-29); (g_1628 != 32); g_1628++)
                { /* block id: 822 */
                    for (l_1990 = 3; (l_1990 >= 0); l_1990 -= 1)
                    { /* block id: 825 */
                        int32_t *l_1997 = &g_828;
                        int i;
                        if (g_94[l_1990])
                            break;
                        (*g_1998) = ((l_1964 , l_1997) == (void*)0);
                        if (l_1999)
                            continue;
                        if (g_1750.f4)
                            goto lbl_1992;
                    }
                }
                for (g_1870 = 7; (g_1870 == 17); g_1870 = safe_add_func_uint8_t_u_u(g_1870, 2))
                { /* block id: 834 */
                    uint16_t l_2027 = 1UL;
                    int32_t l_2056 = 0xB590C097L;
                    int32_t l_2058 = (-1L);
                    uint8_t l_2064[10] = {4UL,255UL,4UL,1UL,1UL,4UL,255UL,4UL,4UL,4UL};
                    const uint16_t **l_2105 = &g_1408[4][0][2];
                    int32_t *l_2155 = &g_1448;
                    int32_t l_2165 = 0x7E6DE11DL;
                    int i;
                }
            }
        }
    }
    return l_2166;
}


/* ------------------------------------------ */
/* 
 * reads : g_1728 g_1662.f1
 * writes: g_1662.f1
 */
static int32_t * func_34(uint16_t  p_35, float  p_36, int8_t  p_37, int64_t  p_38, int8_t  p_39)
{ /* block id: 717 */
    const int64_t l_1776 = (-1L);
    int32_t l_1777 = 0x709BFEF6L;
    int32_t *l_1778 = &g_25;
    l_1777 = ((*g_1728) |= (safe_mul_func_uint16_t_u_u((!l_1776), p_37)));
    return l_1778;
}


/* ------------------------------------------ */
/* 
 * reads : g_527.f1 g_782 g_75 g_76 g_4 g_131 g_117.f3 g_791 g_17 g_635 g_117.f0 g_164.f4 g_574 g_154 g_422 g_71.f0 g_25 g_90 g_255 g_117.f2 g_828 g_672 g_218 g_167.f0 g_727 g_913 g_401.f0 g_594 g_527 g_350 g_593 g_58.f0 g_527.f4 g_951.f4 g_345.f0 g_1407 g_1418 g_94 g_1429 g_1432 g_1139.f4 g_1470 g_1449 g_164.f1 g_1408 g_1530 g_1115.f0 g_1551 g_388 g_571.f4 g_1551.f1 g_1288 g_345 g_920 g_921 g_766 g_1628 g_1646 g_1662 g_703 g_1665 g_929 g_1675 g_417 g_418 g_1319 g_16 g_1150 g_1722 g_1210 g_1728 g_1733 g_1448 g_1750 g_1662.f1
 * writes: g_527.f1 g_782 g_635 g_131 g_17 g_164.f4 g_574 g_154 g_791 g_90 g_913 g_919 g_350 g_951.f4 g_94 g_1418 g_1429 g_76 g_117.f2 g_164.f1 g_1449 g_71.f0 g_1470 g_1599 g_1628 g_218 g_1319 g_418 g_1716 g_527.f4 g_1723 g_1210 g_1733 g_828 g_1662.f1 g_1749
 */
static int32_t * func_42(int32_t  p_43, int8_t * p_44)
{ /* block id: 301 */
    int32_t ***l_777[4];
    uint64_t l_866 = 18446744073709551613UL;
    int8_t *l_875 = &g_401.f0;
    const uint16_t *l_902 = &g_574;
    const uint16_t **l_901 = &l_902;
    const uint16_t ***l_900 = &l_901;
    int16_t l_912 = 0x6E57L;
    const union U2 l_924 = {-1L};
    int32_t l_928 = (-2L);
    uint16_t l_930 = 0x685EL;
    union U0 *l_941[8];
    uint64_t l_970 = 0x7709E665D3A66A20LL;
    int8_t l_1064 = (-1L);
    int32_t *l_1168 = &g_4;
    int32_t *l_1193[8] = {&g_25,&g_25,&g_25,&g_25,&g_25,&g_25,&g_25,&g_25};
    uint32_t l_1255 = 6UL;
    uint8_t l_1271[2];
    uint64_t ****l_1368 = &g_703;
    uint64_t *****l_1367 = &l_1368;
    float l_1369 = 0x1.0p+1;
    uint32_t l_1395 = 8UL;
    int8_t l_1442[7][6] = {{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L},{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L},{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L},{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L},{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L},{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L},{0x04L,0x04L,0x04L,0x04L,0x04L,0x04L}};
    uint8_t l_1443 = 255UL;
    uint16_t l_1469 = 0x747CL;
    uint64_t l_1582 = 0x388BE168CC35E8C3LL;
    uint16_t l_1759 = 0xDBAAL;
    uint32_t l_1765 = 3UL;
    int32_t *l_1768 = &g_4;
    int i, j;
    for (i = 0; i < 4; i++)
        l_777[i] = &g_75;
    for (i = 0; i < 8; i++)
        l_941[i] = &g_117[4][6];
    for (i = 0; i < 2; i++)
        l_1271[i] = 0xA3L;
    for (g_527.f1 = 0; (g_527.f1 != (-10)); g_527.f1 = safe_sub_func_int64_t_s_s(g_527.f1, 5))
    { /* block id: 304 */
        p_43 |= (l_777[1] == l_777[1]);
    }
    if (((safe_mod_func_int64_t_s_s(((safe_div_func_uint8_t_u_u(((g_782[6] = g_782[6]) != &g_76), (safe_div_func_uint32_t_u_u((((((**g_75) || (safe_mod_func_uint32_t_u_u(g_131, g_527.f1))) , (p_43 || (safe_rshift_func_uint8_t_u_u(g_117[4][6].f3, (safe_mul_func_uint8_t_u_u(((((p_43 == 0xA9FC5149L) && p_43) && p_43) >= g_791), 0UL)))))) & 255UL) & p_43), 0xB97A2E3FL)))) & p_43), (-1L))) || (*p_44)))
    { /* block id: 308 */
        uint64_t l_808 = 0x2146CDEF6767AD7DLL;
        int32_t l_824 = (-1L);
        for (g_635 = 2; (g_635 != 2); g_635 = safe_add_func_uint8_t_u_u(g_635, 3))
        { /* block id: 311 */
            uint8_t *l_806 = (void*)0;
            uint8_t *l_807 = &g_164.f4;
            uint16_t *l_811 = &g_574;
            uint64_t *l_823 = &g_154;
            for (g_131 = 0; g_131 < 10; g_131 += 1)
            {
                g_17[g_131] = 0L;
            }
            l_824 = ((safe_add_func_uint32_t_u_u((g_17[7] || (safe_lshift_func_uint8_t_u_s((~((safe_div_func_uint16_t_u_u(p_43, (252UL && ((safe_rshift_func_uint16_t_u_s((safe_mod_func_uint8_t_u_u(((safe_unary_minus_func_uint32_t_u(g_117[4][6].f0)) || ((*l_807) &= ((-1L) > p_43))), l_808)), ((((safe_lshift_func_uint16_t_u_u(((*l_811) = p_43), 15)) , ((safe_mod_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u((g_791 = (+(safe_div_func_uint8_t_u_u((((*l_823) &= g_574) || 0x051ABB42D82A2AECLL), 0x24L)))), (*g_422))), g_25)), p_43)), l_808)) , 0x0.AEC916p+87)) , &l_808) == l_823))) == (**g_75))))) , (-1L))), 6))), 1L)) || 0x48L);
        }
    }
    else
    { /* block id: 319 */
        uint8_t l_825[6] = {0x77L,0x91L,0x91L,0x77L,0x91L,0x91L};
        int32_t l_829 = 6L;
        int32_t l_830 = 8L;
        int32_t l_831 = 0x344683F3L;
        int32_t l_832 = 8L;
        int64_t l_833 = 0x77F642B3EF5D0524LL;
        int32_t l_834 = (-1L);
        int64_t l_835 = 0x3043AE8023543647LL;
        int32_t l_836 = (-10L);
        int32_t l_837 = 0xE28BF938L;
        int32_t l_838[4];
        uint32_t l_839 = 18446744073709551611UL;
        uint16_t *l_855[6][6] = {{&g_117[4][6].f2,&g_527.f2,&g_574,&g_574,&g_574,&g_527.f2},{&g_117[4][6].f2,&g_527.f2,&g_574,&g_574,&g_574,&g_527.f2},{&g_117[4][6].f2,&g_527.f2,&g_574,&g_574,&g_574,&g_527.f2},{&g_117[4][6].f2,&g_527.f2,&g_574,&g_574,&g_574,&g_527.f2},{&g_117[4][6].f2,&g_527.f2,&g_574,&g_574,&g_574,&g_527.f2},{&g_117[4][6].f2,&g_527.f2,&g_574,&g_574,&g_574,&g_527.f2}};
        uint16_t **l_854 = &l_855[4][3];
        int i, j;
        for (i = 0; i < 4; i++)
            l_838[i] = 1L;
        --l_825[4];
        l_839++;
        for (g_90 = (-19); (g_90 >= 58); ++g_90)
        { /* block id: 324 */
            int32_t *l_846 = (void*)0;
        }
    }
    l_866--;
    if ((p_43 && ((safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(((safe_add_func_uint16_t_u_u((*g_255), p_43)) || ((l_875 != ((~(safe_lshift_func_uint8_t_u_s(p_43, 2))) , &g_17[7])) == 1UL)), 2)), 1)) | (((p_43 || (safe_mul_func_int8_t_s_s(((safe_mul_func_int8_t_s_s((((safe_mul_func_int8_t_s_s(((((safe_sub_func_uint64_t_u_u((((0x3C815466L > p_43) & p_43) > p_43), p_43)) != (-1L)) , g_828) <= p_43), (*p_44))) != (*g_672)) < 0xD7CBAC2FL), 0xA9L)) & (*g_672)), g_167[3][1].f0))) , p_43) < (*g_672)))))
    { /* block id: 348 */
        uint64_t l_887 = 18446744073709551615UL;
        uint16_t **l_898[2];
        uint16_t ***l_897 = &l_898[0];
        int32_t l_903 = (-1L);
        int32_t l_910 = 0L;
        int32_t l_911 = 0x64F52422L;
        uint64_t *** const *l_918 = (void*)0;
        int32_t l_925 = (-2L);
        int32_t l_926[7] = {0L,0xEFA9D206L,0xEFA9D206L,0L,0xEFA9D206L,0xEFA9D206L,0L};
        int i;
        for (i = 0; i < 2; i++)
            l_898[i] = (void*)0;
        if ((**g_75))
        { /* block id: 349 */
            uint32_t l_895 = 0x530F2E25L;
            int32_t l_904 = 0xB218D014L;
lbl_905:
            l_887 |= p_43;
            for (g_90 = 0; (g_90 > 53); ++g_90)
            { /* block id: 353 */
                const int8_t *l_892 = &g_333.f0;
                int32_t l_896 = 0x31DD78B2L;
                uint16_t ****l_899 = &l_897;
                (*g_727) ^= (safe_mod_func_uint8_t_u_u((l_892 == (void*)0), (safe_mod_func_int8_t_s_s(((l_904 &= ((l_895 && (l_903 |= ((l_896 , 255UL) >= ((((*l_899) = l_897) != l_900) , l_896)))) & 0x24L)) < l_896), 0xAEL))));
            }
            if (p_43)
                goto lbl_905;
        }
        else
        { /* block id: 360 */
            return (*g_75);
        }
        for (g_164.f4 = 4; (g_164.f4 < 56); g_164.f4++)
        { /* block id: 365 */
            int32_t l_908 = 0xE8A78436L;
            int32_t l_909[1];
            uint64_t ****l_923 = (void*)0;
            int32_t l_927 = 0x6EE0B449L;
            int i;
            for (i = 0; i < 1; i++)
                l_909[i] = (-10L);
            g_913++;
            (*g_727) |= ((-8L) == (p_43 != (safe_add_func_int16_t_s_s(((g_919 = l_918) != l_923), ((g_401.f0 ^ 0x4C54L) , ((l_909[0] && ((l_924 , g_117[4][6].f3) , (*g_672))) ^ (*p_44)))))));
            l_930--;
        }
    }
    else
    { /* block id: 371 */
        uint64_t l_952[7][8][4] = {{{0x1366A4CF73EBC2DELL,0UL,0UL,0x2916D89AD92F08EBLL},{0UL,0x6F445D56CA661B07LL,0x23707203D665F37FLL,0x6F445D56CA661B07LL},{0x6F445D56CA661B07LL,0x547F6D3A95D342F9LL,0x31C5B948C26F65E9LL,0x7002501BDBFB532DLL},{0xE2490EFEDC971129LL,18446744073709551615UL,18446744073709551610UL,0xA67D171DB317D88ALL},{0xB636451A218162D5LL,0xB117510360F5DC6DLL,0x907EDE7AAD53182ALL,0x2157813E47237C55LL},{0xB636451A218162D5LL,0UL,18446744073709551610UL,0xB636451A218162D5LL},{0xE2490EFEDC971129LL,0x2157813E47237C55LL,0x31C5B948C26F65E9LL,18446744073709551610UL},{0x6F445D56CA661B07LL,0x290646C168E8340ALL,0x23707203D665F37FLL,18446744073709551613UL}},{{0UL,1UL,0UL,0x4EF63AF939796494LL},{0x1366A4CF73EBC2DELL,0xB117510360F5DC6DLL,0xFFBE610E66E9A8B5LL,18446744073709551610UL},{0x7D30BC29CC8E8D0ALL,0x6F445D56CA661B07LL,0x63F2610511EB3DF4LL,0UL},{18446744073709551615UL,0UL,0x31C5B948C26F65E9LL,0x6F445D56CA661B07LL},{0x3FA49EEBA6411B1BLL,0x209D5FE7A987422FLL,0xA67D171DB317D88ALL,0xA67D171DB317D88ALL},{5UL,5UL,0x63F2610511EB3DF4LL,0x4EF63AF939796494LL},{0xB636451A218162D5LL,0x547F6D3A95D342F9LL,0x2916D89AD92F08EBLL,0UL},{0x1366A4CF73EBC2DELL,0x2157813E47237C55LL,0x3FA49EEBA6411B1BLL,0x2916D89AD92F08EBLL}},{{18446744073709551615UL,0x2157813E47237C55LL,0x23707203D665F37FLL,0UL},{0x2157813E47237C55LL,0x547F6D3A95D342F9LL,0x1963F46B3D2A52C9LL,0x4EF63AF939796494LL},{0xE2490EFEDC971129LL,5UL,0UL,0xA67D171DB317D88ALL},{0x7D30BC29CC8E8D0ALL,0x209D5FE7A987422FLL,0x907EDE7AAD53182ALL,0x6F445D56CA661B07LL},{0UL,0UL,0x3FA49EEBA6411B1BLL,0UL},{0xE2490EFEDC971129LL,0x6F445D56CA661B07LL,0xA67D171DB317D88ALL,18446744073709551610UL},{0UL,0xE2490EFEDC971129LL,0xB117510360F5DC6DLL,0x547F6D3A95D342F9LL},{0x3FA49EEBA6411B1BLL,0x63F2610511EB3DF4LL,0xC65136A77EE2148ELL,0UL}},{{0xE6AC9EA6F0EBE1E5LL,1UL,0xE6AC9EA6F0EBE1E5LL,0xC65136A77EE2148ELL},{0xA67D171DB317D88ALL,0xFFBE610E66E9A8B5LL,0xB636451A218162D5LL,0x31C5B948C26F65E9LL},{0UL,0x907EDE7AAD53182ALL,0x46454ACDE86768C1LL,0xFFBE610E66E9A8B5LL},{4UL,0xE2490EFEDC971129LL,0x46454ACDE86768C1LL,0xE284179C0A47B9DCLL},{0UL,18446744073709551610UL,0xB636451A218162D5LL,1UL},{0xA67D171DB317D88ALL,1UL,0xE6AC9EA6F0EBE1E5LL,0x2916D89AD92F08EBLL},{0xE6AC9EA6F0EBE1E5LL,0x2916D89AD92F08EBLL,0xC65136A77EE2148ELL,1UL},{0x3FA49EEBA6411B1BLL,0x1366A4CF73EBC2DELL,0xB117510360F5DC6DLL,0xFFBE610E66E9A8B5LL}},{{0x1366A4CF73EBC2DELL,1UL,0xE284179C0A47B9DCLL,0UL},{0x34E7A27262954AD3LL,0UL,4UL,0xE284179C0A47B9DCLL},{0xA67D171DB317D88ALL,1UL,18446744073709551613UL,0x1366A4CF73EBC2DELL},{0x1963F46B3D2A52C9LL,0x907EDE7AAD53182ALL,0x907EDE7AAD53182ALL,0x1963F46B3D2A52C9LL},{0x34E7A27262954AD3LL,0x1366A4CF73EBC2DELL,0x46454ACDE86768C1LL,0xC65136A77EE2148ELL},{0xFFBE610E66E9A8B5LL,0x35308888C28D71CBLL,0xB117510360F5DC6DLL,1UL},{18446744073709551610UL,0x63F2610511EB3DF4LL,4UL,1UL},{0xE6AC9EA6F0EBE1E5LL,0x35308888C28D71CBLL,1UL,0xC65136A77EE2148ELL}},{{0x31C5B948C26F65E9LL,0x1366A4CF73EBC2DELL,0xB636451A218162D5LL,0x1963F46B3D2A52C9LL},{0x3FA49EEBA6411B1BLL,0x907EDE7AAD53182ALL,0xE284179C0A47B9DCLL,0x1366A4CF73EBC2DELL},{4UL,1UL,0xA2DD76B533243607LL,0xE284179C0A47B9DCLL},{18446744073709551610UL,0UL,0xB636451A218162D5LL,0UL},{0x1963F46B3D2A52C9LL,1UL,0x23707203D665F37FLL,0xFFBE610E66E9A8B5LL},{0xE6AC9EA6F0EBE1E5LL,0x1366A4CF73EBC2DELL,0x907EDE7AAD53182ALL,1UL},{0UL,0x2916D89AD92F08EBLL,0xB117510360F5DC6DLL,0x2916D89AD92F08EBLL},{0x2916D89AD92F08EBLL,1UL,0xA2DD76B533243607LL,1UL}},{{0x34E7A27262954AD3LL,18446744073709551610UL,0xC65136A77EE2148ELL,0xE284179C0A47B9DCLL},{0x31C5B948C26F65E9LL,0xE2490EFEDC971129LL,18446744073709551613UL,0xFFBE610E66E9A8B5LL},{0x31C5B948C26F65E9LL,0x907EDE7AAD53182ALL,0xC65136A77EE2148ELL,0x31C5B948C26F65E9LL},{0x34E7A27262954AD3LL,0xFFBE610E66E9A8B5LL,0xA2DD76B533243607LL,0xC65136A77EE2148ELL},{0x2916D89AD92F08EBLL,1UL,0xB117510360F5DC6DLL,0UL},{0UL,0x63F2610511EB3DF4LL,0x907EDE7AAD53182ALL,0x547F6D3A95D342F9LL},{0xE6AC9EA6F0EBE1E5LL,0xE2490EFEDC971129LL,0x23707203D665F37FLL,0xC65136A77EE2148ELL},{0x1963F46B3D2A52C9LL,0x2916D89AD92F08EBLL,0xB636451A218162D5LL,0xA67D171DB317D88ALL}}};
        int32_t l_958 = 0xD3A1BFC1L;
        int32_t l_991 = 0x603911E8L;
        int32_t l_1046 = 0x307A192BL;
        int32_t l_1047 = 0x5AE4297EL;
        int32_t l_1057[5][5][5] = {{{0x9A3A7273L,0x3989E930L,0x4BC46C9DL,0xC01351CDL,0xE00BB21CL},{5L,0xA0A9DB86L,0x6032EC14L,4L,0x9A3A7273L},{0xE6A85054L,0xE00BB21CL,0x4BC46C9DL,0xE00BB21CL,0xE6A85054L},{(-1L),0xC01351CDL,(-1L),0xE00BB21CL,4L},{0x65DBB00DL,(-1L),0x58106ABBL,4L,0xA95557D9L}},{{0x4BC46C9DL,(-1L),4L,0xC01351CDL,4L},{4L,4L,0x3989E930L,0x58106ABBL,0xE6A85054L},{4L,1L,0xE6A85054L,0x6032EC14L,0x9A3A7273L},{0x4BC46C9DL,5L,0xA95557D9L,0xA0A9DB86L,0xE00BB21CL},{0x65DBB00DL,1L,1L,0x65DBB00DL,0x367DC8A2L}},{{(-1L),4L,1L,0xE6A85054L,0x6032EC14L},{0xE6A85054L,0x367DC8A2L,0x6032EC14L,0xC01351CDL,0xA95557D9L},{0xE00BB21CL,1L,(-1L),(-1L),1L},{0x58106ABBL,0x65DBB00DL,0xA0A9DB86L,0x9A3A7273L,1L},{0x367DC8A2L,4L,0x4BC46C9DL,5L,0xA95557D9L}},{{0x65DBB00DL,5L,(-1L),0xE6A85054L,0xE6A85054L},{0x367DC8A2L,0xA0A9DB86L,0x367DC8A2L,(-1L),0x3989E930L},{0x58106ABBL,0xA0A9DB86L,0xC01351CDL,0x65DBB00DL,4L},{0xE00BB21CL,5L,0xE6A85054L,0x4BC46C9DL,0x58106ABBL},{(-1L),4L,0xC01351CDL,4L,(-1L)}},{{1L,0x65DBB00DL,0x367DC8A2L,4L,0x4BC46C9DL},{0x9A3A7273L,1L,(-1L),0x4BC46C9DL,0x6032EC14L},{0xC01351CDL,0x367DC8A2L,0x4BC46C9DL,0x65DBB00DL,0x4BC46C9DL},{0x4BC46C9DL,0x4BC46C9DL,0xA0A9DB86L,(-1L),(-1L)},{0x4BC46C9DL,0xA95557D9L,(-1L),0xE6A85054L,0x58106ABBL}}};
        uint64_t ****l_1119[7] = {&g_703,&g_703,(void*)0,&g_703,&g_703,(void*)0,&g_703};
        int16_t *l_1120 = &g_94[0];
        int32_t *l_1169 = &g_164.f1;
        float l_1209[6][10] = {{(-0x5.Bp+1),(-0x5.Bp+1),(-0x1.Fp-1),(-0x4.9p-1),0x2.9164ADp+3,0xC.7704B0p-29,(-0x1.Dp+1),0x0.Ap-1,(-0x7.Bp+1),0xF.A83433p-20},{0x0.Ap-1,(-0x8.7p+1),(-0x1.Dp+1),0x9.EBE516p-51,0x3.32358Ap+78,0x9.EBE516p-51,(-0x1.Dp+1),(-0x8.7p+1),0x0.Ap-1,(-0x5.Bp+1)},{0x0.6p-1,(-0x5.Bp+1),(-0x7.Bp+1),0x2.9164ADp+3,0xF.A83433p-20,0x1.2p+1,0xF.476D7Dp+95,0x3.32358Ap+78,0x3.32358Ap+78,0xF.476D7Dp+95},{(-0x8.7p+1),0x0.6p-1,0x2.9164ADp+3,0x2.9164ADp+3,0x0.6p-1,(-0x8.7p+1),(-0x4.9p-1),0x1.2p+1,0x0.Ap-1,0xC.7704B0p-29},{0xC.7704B0p-29,0x3.32358Ap+78,(-0x5.Bp+1),0x9.EBE516p-51,(-0x7.Bp+1),(-0x1.Fp-1),(-0x8.7p+1),(-0x1.Fp-1),(-0x7.Bp+1),0x9.EBE516p-51},{0xC.7704B0p-29,(-0x1.Fp-1),0xC.7704B0p-29,(-0x4.9p-1),0xF.476D7Dp+95,(-0x8.7p+1),0x9.EBE516p-51,0xF.A83433p-20,(-0x5.Bp+1),0x1.2p+1}};
        uint16_t *l_1251[10][2][10] = {{{&g_527.f2,&l_930,&g_164.f2,&l_930,&g_951.f2,&l_930,&l_930,&g_574,&g_951.f2,&g_164.f2},{(void*)0,&g_164.f2,&g_164.f2,&g_164.f2,&g_574,&g_164.f2,&g_574,&g_164.f2,&g_164.f2,&g_164.f2}},{{&g_164.f2,&g_527.f2,&g_527.f2,(void*)0,&g_951.f2,&g_527.f2,&l_930,&g_527.f2,&g_527.f2,&l_930},{&g_527.f2,&g_574,&g_164.f2,&l_930,(void*)0,&g_527.f2,(void*)0,&g_951.f2,&g_164.f2,&g_574}},{{&g_164.f2,&g_164.f2,&g_527.f2,&g_951.f2,&g_527.f2,&g_164.f2,&g_951.f2,&g_527.f2,&l_930,(void*)0},{(void*)0,&g_164.f2,(void*)0,(void*)0,&g_527.f2,&g_951.f2,&g_951.f2,&l_930,&l_930,&l_930}},{{&l_930,&g_527.f2,&g_164.f2,&g_527.f2,&g_527.f2,&g_164.f2,&g_527.f2,&l_930,&l_930,&g_164.f2},{&g_527.f2,&g_164.f2,&l_930,&g_951.f2,&l_930,&g_527.f2,&g_164.f2,&g_164.f2,&g_951.f2,&g_527.f2}},{{&g_527.f2,&g_951.f2,&l_930,&l_930,&g_527.f2,&g_164.f2,&g_527.f2,&l_930,&g_164.f2,(void*)0},{&g_527.f2,(void*)0,&g_164.f2,&g_164.f2,&g_527.f2,&l_930,(void*)0,&l_930,&l_930,&l_930}},{{&g_527.f2,&g_951.f2,&l_930,&l_930,&l_930,&g_164.f2,&g_164.f2,&g_574,&g_164.f2,&g_164.f2},{&g_574,&g_527.f2,(void*)0,&g_527.f2,&g_574,&g_574,&l_930,&g_527.f2,&l_930,&g_527.f2}},{{&g_164.f2,&g_527.f2,&g_164.f2,&l_930,&g_527.f2,&l_930,(void*)0,&g_164.f2,&g_527.f2,&g_527.f2},{&g_527.f2,&l_930,&g_527.f2,&g_527.f2,&g_574,&l_930,&g_574,(void*)0,&g_951.f2,&g_164.f2}},{{&l_930,&g_527.f2,&g_164.f2,&l_930,&l_930,&g_527.f2,(void*)0,(void*)0,&g_164.f2,&l_930},{&g_574,&g_951.f2,(void*)0,&g_574,&g_527.f2,&g_527.f2,&g_527.f2,&g_527.f2,&g_574,(void*)0}},{{&g_574,&g_574,&g_951.f2,&g_164.f2,&g_527.f2,&g_527.f2,&l_930,&g_574,&g_951.f2,&g_527.f2},{&g_164.f2,&l_930,&g_951.f2,&l_930,&l_930,(void*)0,&l_930,&g_951.f2,&g_951.f2,&g_164.f2}},{{&g_164.f2,&g_574,&g_164.f2,&l_930,&g_527.f2,&g_164.f2,&g_527.f2,&l_930,&g_164.f2,&l_930},{&l_930,&g_951.f2,&g_951.f2,&g_527.f2,&g_527.f2,&g_164.f2,(void*)0,(void*)0,(void*)0,&l_930}}};
        uint16_t **l_1250[4][7] = {{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],(void*)0,(void*)0,&l_1251[2][0][0]},{&l_1251[2][1][5],&l_1251[2][0][0],&l_1251[2][1][5],&l_1251[6][0][1],&l_1251[8][0][5],&l_1251[8][0][5],&l_1251[6][0][1]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],(void*)0,(void*)0,&l_1251[2][0][0]},{&l_1251[2][1][5],&l_1251[2][0][0],&l_1251[2][1][5],&l_1251[6][0][1],&l_1251[8][0][5],&l_1251[8][0][5],&l_1251[6][0][1]}};
        uint32_t l_1267[1];
        union U2 *l_1277 = &g_167[4][5];
        union U1 *l_1287 = &g_345[2][1][2];
        int32_t l_1322 = 5L;
        uint16_t * const l_1339 = (void*)0;
        uint16_t * const *l_1338 = &l_1339;
        union U2 *l_1349 = (void*)0;
        int32_t l_1351[3][6][10] = {{{8L,0xB9174E26L,0L,0L,0L,0xC8121B1DL,0L,(-4L),0L,8L},{(-4L),0x9AAC3006L,1L,(-1L),1L,0L,0L,1L,(-7L),0xF7281CEFL},{(-4L),(-3L),0xF1C82F09L,0L,0x80963A85L,(-1L),0x80963A85L,0L,0xF1C82F09L,(-3L)},{0x80963A85L,(-1L),1L,0L,0x36BD48C3L,0x74DEAEC8L,(-1L),0xF7281CEFL,(-1L),0L},{(-4L),(-4L),0x324DA19BL,(-7L),0x9D673578L,0x74DEAEC8L,0x6F25F285L,0x9D673578L,(-3L),0x5183C548L},{0x80963A85L,0x93042789L,0xF3863E3EL,0x9D673578L,0x2716988FL,(-1L),0xB9174E26L,0x80963A85L,(-1L),0L}},{{(-4L),0x46CB9E6EL,0x74DEAEC8L,0xC8121B1DL,0xF42176E7L,0L,1L,(-3L),(-4L),0xC8121B1DL},{0L,0x80963A85L,0x9647F27CL,0x74DEAEC8L,0L,0x5183C548L,0x6F25F285L,(-1L),(-8L),0L},{1L,(-2L),(-1L),(-1L),0x93042789L,0xE900251EL,(-3L),(-3L),0xE900251EL,0x93042789L},{0L,(-1L),(-1L),0L,0L,(-4L),0x93042789L,(-3L),0x9647F27CL,1L},{0xF7281CEFL,1L,1L,0x36BD48C3L,0xB9174E26L,0x6F25F285L,(-1L),0x93042789L,0x9647F27CL,1L},{0x9D673578L,(-4L),0xF3863E3EL,0L,(-1L),0x47745C5AL,0L,0xCDA451EBL,0xE900251EL,1L}},{{0x80963A85L,(-8L),0x5183C548L,(-1L),0x69F4CF02L,0xF42176E7L,0L,0xF7281CEFL,(-8L),0L},{(-3L),0x9D673578L,0x6F25F285L,0x74DEAEC8L,0x9D673578L,(-7L),0x324DA19BL,(-4L),(-4L),1L},{(-1L),0x93042789L,0xF1C82F09L,0xC8121B1DL,1L,0xF3863E3EL,(-10L),(-1L),(-1L),(-1L)},{(-3L),0L,0L,0x9D673578L,0L,0L,(-3L),0L,(-3L),(-3L)},{(-3L),(-1L),0x9647F27CL,(-7L),0x80963A85L,(-4L),(-8L),(-2L),(-1L),0L},{0x93042789L,(-1L),0x47745C5AL,0L,0xB9174E26L,0x47745C5AL,(-3L),(-3L),0xF1C82F09L,(-10L)}}};
        union U2 l_1382 = {-1L};
        uint8_t *l_1396 = &g_951.f4;
        const uint64_t l_1468 = 18446744073709551615UL;
        uint64_t l_1518 = 0UL;
        const float l_1527 = 0x2.C000FEp+16;
        uint32_t l_1578 = 18446744073709551615UL;
        int8_t * const *l_1587 = &g_422;
        int8_t * const **l_1586 = &l_1587;
        int8_t * const ***l_1585 = &l_1586;
        uint64_t ***l_1661 = &g_921;
        float l_1726 = 0x3.F8F6BDp+0;
        union U2 **l_1752[1][6];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1267[i] = 0xFEBE4D22L;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 6; j++)
                l_1752[i][j] = &l_1277;
        }
        for (g_154 = 17; (g_154 < 45); g_154 = safe_add_func_uint32_t_u_u(g_154, 4))
        { /* block id: 374 */
            uint64_t l_939 = 18446744073709551606UL;
            union U0 *l_940 = (void*)0;
            int32_t l_959 = (-8L);
            int32_t l_960 = (-9L);
            int32_t l_964 = (-2L);
            int32_t l_965 = 0xF809384CL;
            int32_t l_966 = 5L;
            int32_t l_969 = 0xAA1BBCDFL;
            int32_t **l_990[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int64_t l_1011 = (-1L);
            float l_1032 = 0xE.11AAEBp-57;
            int32_t l_1090 = (-6L);
            union U2 l_1097 = {0x8CL};
            int8_t l_1189 = 0x39L;
            int8_t l_1248 = 0xAFL;
            uint16_t **l_1252[9][4] = {{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]},{&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0],&l_1251[2][0][0]}};
            int32_t *l_1258 = (void*)0;
            int i, j;
        }
        if ((safe_mul_func_int16_t_s_s(((safe_rshift_func_int8_t_s_s((l_924 , (((((safe_mod_func_int64_t_s_s((((safe_add_func_uint8_t_u_u(((*l_1168) || (safe_rshift_func_int16_t_s_s(((safe_lshift_func_int16_t_s_s((l_1382 , 0x7C4DL), 5)) , ((*l_1120) = (((*l_1396) = (safe_rshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s(((*g_594) , ((g_350 |= p_43) != (safe_div_func_int32_t_s_s((safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u(((l_924 , (**g_593)) , p_43), g_167[3][1].f0)), 6L)), p_43)))), l_1395)) <= 0x2A0512D3L), 7)), g_58.f0))) , p_43))), g_527.f4))), 0xD4L)) && g_951.f4) , (*g_672)), 18446744073709551610UL)) >= p_43) < 0L) & p_43) >= p_43)), 5)) || p_43), p_43)))
        { /* block id: 545 */
            uint32_t l_1415 = 18446744073709551615UL;
            int32_t l_1436[5];
            uint32_t *l_1453 = &l_1267[0];
            int8_t l_1472 = (-1L);
            uint16_t ***l_1488 = &l_1250[3][3];
            uint64_t ***l_1497 = (void*)0;
            uint64_t * const **l_1498 = (void*)0;
            const int16_t l_1500 = 0xC96BL;
            int32_t *l_1602[3];
            uint64_t * const l_1627 = &g_1628;
            uint64_t * const * const l_1626 = &l_1627;
            uint64_t * const * const * const l_1625 = &l_1626;
            uint64_t * const * const * const *l_1624 = &l_1625;
            uint64_t * const * const * const **l_1623 = &l_1624;
            float l_1630 = 0x1.0p-1;
            union U0 *l_1631[2][7][7] = {{{&g_117[4][6],(void*)0,&g_951,&g_164,(void*)0,(void*)0,&g_164},{&g_164,&g_117[1][0],(void*)0,&g_951,&g_164,&g_164,&g_164},{(void*)0,&g_164,&g_164,(void*)0,(void*)0,&g_951,&g_164},{&g_117[2][3],&g_117[1][0],&g_527,&g_117[4][6],&g_164,&g_117[4][6],&g_527},{(void*)0,(void*)0,&g_164,(void*)0,(void*)0,&g_951,&g_164},{&g_164,(void*)0,&g_951,&g_117[1][0],&g_951,(void*)0,&g_164},{(void*)0,(void*)0,&g_951,&g_117[4][6],(void*)0,&g_951,&g_164}},{{&g_164,&g_951,&g_527,&g_951,&g_164,(void*)0,&g_951},{(void*)0,&g_117[4][6],&g_951,(void*)0,(void*)0,(void*)0,(void*)0},{&g_951,&g_117[1][0],&g_951,(void*)0,&g_164,&g_117[4][6],&g_951},{(void*)0,(void*)0,&g_164,(void*)0,(void*)0,&g_527,&g_164},{&g_164,&g_117[4][6],&g_527,&g_117[1][0],&g_117[2][3],&g_117[4][6],&g_951},{(void*)0,(void*)0,&g_164,&g_164,(void*)0,(void*)0,&g_951},{&g_164,&g_951,(void*)0,&g_117[1][0],&g_164,(void*)0,&g_164}}};
            uint64_t l_1656[4];
            uint8_t ** const l_1694 = &l_1396;
            uint8_t ** const *l_1693[1];
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_1436[i] = 0x429F067DL;
            for (i = 0; i < 3; i++)
                l_1602[i] = &g_828;
            for (i = 0; i < 4; i++)
                l_1656[i] = 0UL;
            for (i = 0; i < 1; i++)
                l_1693[i] = &l_1694;
            l_1046 |= ((safe_mul_func_uint8_t_u_u(g_913, (safe_lshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s(((((void*)0 != &l_1047) <= g_345[2][1][2].f0) <= ((((((safe_unary_minus_func_int16_t_s((safe_add_func_int8_t_s_s((*p_44), ((+(((*l_900) = g_1407) == &l_902)) || (safe_mod_func_uint16_t_u_u(((*p_44) , (((safe_rshift_func_int16_t_s_s(((((safe_sub_func_int64_t_s_s((*g_672), 0xBC8FCAA42F65559ALL)) && 0x0910L) < 0x7E6DA42AL) || p_43), p_43)) == g_574) , p_43)), p_43))))))) | (*g_672)) == (-5L)) < p_43) > l_1415) && (*g_672))), 0xB7L)), (*p_44))))) > 1L);
            if (p_43)
            { /* block id: 548 */
                int64_t l_1424 = 1L;
                int32_t l_1435 = 1L;
                int32_t l_1437 = 1L;
                int32_t l_1439[2];
                uint32_t l_1450 = 0UL;
                int i;
                for (i = 0; i < 2; i++)
                    l_1439[i] = 0xDA721768L;
                (*g_727) |= (**g_75);
                g_1418 ^= (safe_lshift_func_int8_t_s_u((*p_44), p_43));
                if (((safe_add_func_int64_t_s_s(((*p_44) <= (safe_sub_func_int16_t_s_s(0x962EL, (*g_255)))), ((((*g_422) < l_1415) , (+l_1424)) == (*g_672)))) || ((((safe_lshift_func_int8_t_s_s(l_1415, (safe_div_func_uint32_t_u_u(g_94[2], p_43)))) && l_1415) > 0xF6L) | (*p_44))))
                { /* block id: 551 */
                    (*g_1432) = g_1429;
                }
                else
                { /* block id: 553 */
                    int8_t l_1440 = 0L;
                    int32_t l_1441 = 3L;
                    int32_t l_1446[5][6][6] = {{{0L,(-1L),0x05C8F085L,(-5L),0L,0x41330B2BL},{0xAA67FBFDL,0L,(-5L),0x14A06884L,(-9L),6L},{0L,0L,0x7D59281FL,0x516F4D07L,0L,(-10L)},{0xB5EE9D18L,(-1L),0L,0x3031F0AEL,0L,1L},{6L,(-1L),0L,0L,0x14A06884L,(-1L)},{0x3031F0AEL,1L,0x05C8F085L,0x05C8F085L,1L,0x3031F0AEL}},{{0x6B11BDC4L,0x3031F0AEL,(-1L),0x14A06884L,0xAA67FBFDL,(-1L)},{0x058C2309L,0x7D59281FL,0x41330B2BL,(-10L),0L,0x516F4D07L},{0x058C2309L,0x05E4472FL,(-10L),0x14A06884L,0x28EA1DBAL,1L},{0x6B11BDC4L,0xAA67FBFDL,0x7D59281FL,0x05C8F085L,0L,0x25F27405L},{0x3031F0AEL,(-1L),0x74819F21L,0L,0x05E4472FL,0x14A06884L},{6L,0x3031F0AEL,(-5L),0x3031F0AEL,6L,(-1L)}},{{0xB5EE9D18L,0x14A06884L,(-10L),(-5L),(-1L),0x74819F21L},{0x206B5FF8L,0x6B11BDC4L,(-3L),0x516F4D07L,8L,0x74819F21L},{0x28EA1DBAL,0x14A06884L,(-10L),0x05E4472FL,0x058C2309L,0L},{8L,(-9L),0x05E4472FL,(-8L),(-1L),0x516F4D07L},{0x14A06884L,8L,1L,(-10L),0L,0L},{0x74819F21L,1L,1L,0x74819F21L,(-9L),0x05C8F085L}},{{0x05C8F085L,0x6B11BDC4L,(-1L),0L,0x516F4D07L,(-5L)},{0x7D59281FL,0x41330B2BL,(-10L),0L,0x516F4D07L,0x7D59281FL},{1L,0x6B11BDC4L,(-1L),(-8L),(-9L),0L},{0x28EA1DBAL,1L,0L,0x058C2309L,0L,0L},{(-3L),8L,0x058C2309L,0x05C8F085L,(-1L),0x05C8F085L},{0x206B5FF8L,(-9L),0x206B5FF8L,(-10L),0x058C2309L,(-1L)}},{{0x7D59281FL,0x14A06884L,1L,(-1L),8L,0x41330B2BL},{0x516F4D07L,0x6B11BDC4L,0x05E4472FL,(-1L),(-1L),(-10L)},{0x7D59281FL,0x516F4D07L,0L,(-10L),0x41330B2BL,0x7D59281FL},{0x206B5FF8L,0L,8L,0x05C8F085L,(-9L),0x74819F21L},{(-3L),6L,(-5L),0x058C2309L,0x058C2309L,(-5L)},{0x28EA1DBAL,0x28EA1DBAL,0x058C2309L,(-8L),0xB5EE9D18L,0x41330B2BL}}};
                    int16_t l_1447 = 1L;
                    int i, j, k;
                    for (l_928 = 0; (l_928 != (-24)); l_928 = safe_sub_func_uint16_t_u_u(l_928, 8))
                    { /* block id: 556 */
                        int32_t l_1438[9] = {(-9L),0xFBF52198L,0xFBF52198L,(-9L),0xFBF52198L,0xFBF52198L,(-9L),0xFBF52198L,0xFBF52198L};
                        int i;
                        l_1443++;
                        l_1450++;
                        (*g_727) &= (((void*)0 == l_1453) <= (g_1139.f4 , 0x5721L));
                    }
                }
                (*g_75) = &l_1436[0];
            }
            else
            { /* block id: 563 */
                int8_t l_1471 = 0xCBL;
                uint16_t *l_1505 = (void*)0;
                uint64_t ****l_1506 = &l_1497;
                int32_t l_1510 = 0x8A65CF26L;
                int32_t l_1512 = 0x4BDBB540L;
                int32_t l_1517 = (-7L);
                uint32_t *l_1571[6];
                union U2 l_1595 = {9L};
                int i;
                for (i = 0; i < 6; i++)
                    l_1571[i] = &l_1415;
                if (((*l_1169) = (safe_mul_func_int16_t_s_s(p_43, (safe_sub_func_int16_t_s_s((g_94[3] = (((*g_76) < p_43) ^ 0xF6D7B941L)), (safe_mod_func_uint8_t_u_u(0xA3L, ((safe_lshift_func_uint8_t_u_s((((*g_255) = (7UL >= (((((&g_1117 == (void*)0) , ((((((safe_sub_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((((((+(safe_unary_minus_func_uint32_t_u((g_117[4][6].f3 | p_43)))) <= 0xF9L) & 0x33894F11L) != p_43) || l_1468), l_1469)), 0x4477L)) && l_1436[0]) & g_1470) > 0x9774L) | l_1471) && p_43)) <= l_1472) , 0xF34F2231L) > 0xD976894DL))) >= l_1436[1]), l_1471)) , (*p_44))))))))))
                { /* block id: 567 */
                    uint8_t l_1477 = 254UL;
                    int32_t l_1508 = 0x2A14DF17L;
                    int32_t l_1511 = 0x58C5B698L;
                    int32_t l_1513 = 0x2249EF60L;
                    int32_t l_1515 = 0L;
                    for (g_1449 = 0; (g_1449 != (-23)); g_1449--)
                    { /* block id: 570 */
                        int32_t l_1475 = 0xE883EE23L;
                        int16_t l_1476 = 0x86BEL;
                        uint16_t l_1499[5] = {0xEE13L,0xEE13L,0xEE13L,0xEE13L,0xEE13L};
                        int i;
                        l_1477--;
                        (*l_1169) = (safe_mod_func_uint8_t_u_u(((p_43 , ((safe_lshift_func_int16_t_s_u((safe_add_func_uint64_t_u_u(p_43, (safe_sub_func_int8_t_s_s((((void*)0 == l_1488) , (safe_mul_func_uint16_t_u_u(((safe_add_func_int8_t_s_s((((((*g_422) = (((safe_mul_func_int16_t_s_s((safe_add_func_uint16_t_u_u(((*g_255) ^= (((((*l_1169) != 0x0CL) && (l_1497 == ((l_1476 && (p_43 , p_43)) , l_1498))) <= 0L) , p_43)), p_43)), (-1L))) | 0x4589839CL) || (*g_255))) <= l_1499[0]) && (-1L)) , (*p_44)), p_43)) != 1UL), 0L))), l_1471)))), 10)) & p_43)) | l_1500), 0xEBL));
                        if (g_58.f0)
                            goto lbl_1501;
                    }
                    if ((*g_727))
                    { /* block id: 576 */
lbl_1501:
                        (*g_75) = (*g_75);
                        (*l_1169) = ((~p_43) != (safe_rshift_func_int16_t_s_s((l_1505 == (*g_1407)), 12)));
                        (*l_1169) = (&g_1118[9] == l_1506);
                    }
                    else
                    { /* block id: 581 */
                        int8_t l_1507[10][9][2] = {{{(-1L),0x85L},{0xA1L,0xB9L},{0x91L,0x91L},{0xA1L,0x0AL},{(-1L),4L},{0x2AL,1L},{0L,0x2AL},{0L,0x8AL},{0L,0x2AL}},{{0L,1L},{0x2AL,4L},{(-1L),0x0AL},{0xA1L,0x91L},{0x91L,0xB9L},{0xA1L,0x85L},{(-1L),0x9FL},{0x2AL,0xA1L},{0L,(-1L)}},{{0L,5L},{0L,(-1L)},{0L,0xA1L},{0x2AL,0x9FL},{(-1L),0x85L},{0xA1L,0xB9L},{0x91L,0x91L},{0xA1L,0x0AL},{(-1L),4L}},{{0x2AL,1L},{0L,0x2AL},{0L,0x8AL},{0L,0x2AL},{0L,1L},{0x2AL,4L},{(-1L),0x0AL},{0xA1L,0x91L},{0x91L,0xF8L}},{{0x91L,0xA1L},{0L,0xE2L},{5L,0x91L},{(-1L),0x8AL},{0x85L,0xA1L},{0x85L,0x8AL},{(-1L),0x91L},{5L,0xE2L},{0L,0xA1L}},{{0x91L,0xF8L},{0x43L,0x43L},{0x91L,1L},{0L,0L},{5L,0xB9L},{(-1L),5L},{0x85L,0L},{0x85L,5L},{(-1L),0xB9L}},{{5L,0L},{0L,1L},{0x91L,0x43L},{0x43L,0xF8L},{0x91L,0xA1L},{0L,0xE2L},{5L,0x91L},{(-1L),0x8AL},{0x85L,0xA1L}},{{0x85L,0x8AL},{(-1L),0x91L},{5L,0xE2L},{0L,0xA1L},{0x91L,0xF8L},{0x43L,0x43L},{0x91L,1L},{0L,0L},{5L,0xB9L}},{{(-1L),5L},{0x85L,0L},{0x85L,5L},{(-1L),0xB9L},{5L,0L},{0L,1L},{0x91L,0x43L},{0x43L,0xF8L},{0x91L,0xA1L}},{{0L,0xE2L},{5L,0x91L},{(-1L),0x8AL},{0x85L,0xA1L},{0x85L,0x8AL},{(-1L),0x91L},{5L,0xE2L},{0L,0xA1L},{0x91L,0xF8L}}};
                        int32_t l_1509[4] = {0xF17A08A5L,0xF17A08A5L,0xF17A08A5L,0xF17A08A5L};
                        float l_1514 = (-0x1.Ap+1);
                        int32_t l_1516 = 1L;
                        int i, j, k;
                        (*g_727) &= 0x12E3610BL;
                        l_1518++;
                        (*g_727) |= (l_1509[1] = (safe_lshift_func_int16_t_s_u(5L, 11)));
                        (*g_727) = (*g_76);
                    }
                }
                else
                { /* block id: 588 */
                    float l_1547 = (-0x5.5p-1);
                    int64_t *l_1548 = &g_1470;
                    int32_t l_1549 = 0x50772C07L;
                    int32_t l_1550 = 0xDE668F41L;
                    uint32_t l_1574[7][8] = {{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL},{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL},{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL},{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL},{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL},{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL},{0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL,0x3FAF942BL}};
                    uint64_t **l_1594 = &g_922[1];
                    uint32_t l_1604 = 1UL;
                    uint32_t l_1617 = 0x5BFB3AFCL;
                    int32_t l_1622 = 0x015B179BL;
                    int i, j;
                    l_1550 ^= (safe_sub_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(l_1471, ((*l_1169) & (safe_div_func_int64_t_s_s((((*g_672) , l_1471) | (g_1530 , ((*l_1548) &= (safe_mul_func_uint16_t_u_u(p_43, (safe_rshift_func_int16_t_s_u((((safe_mul_func_uint8_t_u_u(((safe_mod_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_mod_func_int16_t_s_s(((((((*l_1396) = p_43) <= (safe_lshift_func_int16_t_s_u(((safe_sub_func_uint8_t_u_u(p_43, (9UL < g_345[2][1][2].f0))) <= 0L), l_1512))) && 1UL) , p_43) || 0xF54AB313L), 0xA0C3L)), (-7L))), g_1115.f0)) , (*l_1169)), p_43)) <= 0x7CL) | l_1510), (*l_1169)))))))), (-1L)))))), l_1549));
                    for (l_1255 = 0; (l_1255 <= 1); l_1255 += 1)
                    { /* block id: 594 */
                        int32_t l_1562 = 6L;
                        uint16_t * const **l_1573[5] = {&l_1338,&l_1338,&l_1338,&l_1338,&l_1338};
                        uint16_t * const ***l_1572 = &l_1573[2];
                        int i;
                        (*l_1169) = p_43;
                        l_1517 &= (((18446744073709551612UL | (g_1551 , (l_1436[0] ^= (l_1415 && p_43)))) , (l_1512 = (safe_div_func_int32_t_s_s(l_1549, (((((safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u((((safe_lshift_func_uint8_t_u_u((((*g_727) = l_1562) , (safe_add_func_int8_t_s_s((*p_44), (p_43 <= ((safe_lshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s((((safe_sub_func_uint16_t_u_u(((void*)0 != l_1571[2]), 3L)) || 8UL) || l_1549), p_43)), g_388)) , 0x6C25E8C3L))))), g_571.f4)) && 1L) , (*l_1169)), (*l_1169))), 3)), l_1562)) , l_1572) != &l_900) < 4294967288UL) ^ (*g_672)))))) , l_1574[6][4]);
                        if (g_527.f1)
                            goto lbl_1607;
                    }
lbl_1607:
                    if ((((safe_mul_func_int16_t_s_s(g_1551.f1, (~p_43))) , (1UL > ((*g_255) = (*l_1169)))) , l_1578))
                    { /* block id: 602 */
                        uint64_t l_1579 = 18446744073709551615UL;
                        int8_t ***l_1598[4][2];
                        int8_t ****l_1597 = &l_1598[1][1];
                        int8_t *****l_1596[7][5][6] = {{{(void*)0,&l_1597,&l_1597,&l_1597,(void*)0,&l_1597},{&l_1597,&l_1597,&l_1597,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597,(void*)0},{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597,&l_1597}},{{(void*)0,&l_1597,&l_1597,(void*)0,&l_1597,(void*)0},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,(void*)0},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,(void*)0},{(void*)0,(void*)0,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}},{{(void*)0,(void*)0,&l_1597,&l_1597,(void*)0,(void*)0},{(void*)0,&l_1597,&l_1597,(void*)0,(void*)0,(void*)0},{&l_1597,&l_1597,&l_1597,&l_1597,(void*)0,(void*)0},{&l_1597,&l_1597,&l_1597,(void*)0,(void*)0,&l_1597},{(void*)0,(void*)0,(void*)0,&l_1597,&l_1597,&l_1597}},{{(void*)0,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,(void*)0,&l_1597,&l_1597,&l_1597,&l_1597},{(void*)0,(void*)0,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,(void*)0,(void*)0,(void*)0,&l_1597}},{{(void*)0,(void*)0,&l_1597,&l_1597,(void*)0,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,(void*)0,&l_1597,&l_1597,(void*)0,(void*)0},{(void*)0,&l_1597,(void*)0,&l_1597,(void*)0,&l_1597},{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597,&l_1597}},{{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,(void*)0},{(void*)0,(void*)0,&l_1597,&l_1597,&l_1597,(void*)0},{&l_1597,(void*)0,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,(void*)0,(void*)0,&l_1597,(void*)0,&l_1597},{(void*)0,&l_1597,(void*)0,&l_1597,(void*)0,(void*)0}},{{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,(void*)0,&l_1597},{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}}};
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_1598[i][j] = (void*)0;
                        }
                        l_1579--;
                        --l_1582;
                        (*l_1169) &= (l_1585 == (g_1599 = (((&l_912 != ((safe_lshift_func_uint16_t_u_s((l_1436[4] |= (((l_1579 > ((*g_1288) , (safe_sub_func_uint16_t_u_u((l_1549 ^= ((l_924 , (((safe_add_func_uint32_t_u_u(((void*)0 != l_1506), 0x0B600332L)) , l_1594) == (*g_920))) , (*l_1168))), p_43)))) != g_94[2]) != p_43)), l_1500)) , (void*)0)) , l_1595) , (void*)0)));
                        l_1602[1] = (*g_75);
                    }
                    else
                    { /* block id: 610 */
                        int32_t l_1603 = (-6L);
                        ++l_1604;
                    }
                    for (l_1582 = 0; (l_1582 <= 7); l_1582 += 1)
                    { /* block id: 616 */
                        int8_t l_1608 = 0xF4L;
                        uint64_t * const * const * const **l_1629 = &l_1624;
                        uint8_t **l_1632[4][8] = {{&l_1396,(void*)0,&l_1396,&l_1396,(void*)0,&l_1396,&l_1396,(void*)0},{(void*)0,&l_1396,&l_1396,(void*)0,&l_1396,&l_1396,(void*)0,&l_1396},{(void*)0,(void*)0,&l_1396,(void*)0,(void*)0,&l_1396,&l_1396,&l_1396},{&l_1396,(void*)0,&l_1396,&l_1396,(void*)0,&l_1396,&l_1396,(void*)0}};
                        uint8_t ***l_1633 = &l_1632[1][2];
                        int i, j;
                        (*g_727) = (l_1608 & (((safe_div_func_uint32_t_u_u((safe_add_func_int8_t_s_s((((safe_lshift_func_int8_t_s_s((((l_1617 != (safe_lshift_func_uint16_t_u_u(0xB84FL, (g_951.f4 || (safe_rshift_func_int16_t_s_u(g_766, l_1622)))))) == (l_1471 < ((p_43 , ((l_1629 = (l_1623 = (void*)0)) != &g_919)) >= p_43))) ^ 0xEBL), 6)) != (*l_1169)) , (*p_44)), 4UL)), p_43)) , l_1631[0][2][5]) != (void*)0));
                        (*l_1633) = l_1632[1][2];
                        if (p_43)
                            continue;
                    }
                }
                (*g_75) = (*g_75);
            }
            for (g_635 = 0; (g_635 < 6); g_635 = safe_add_func_uint8_t_u_u(g_635, 1))
            { /* block id: 628 */
                int8_t l_1649[2];
                int64_t l_1697 = 0x179C8C2CF2DE057CLL;
                int32_t l_1709 = 0x266E19E1L;
                int i;
                for (i = 0; i < 2; i++)
                    l_1649[i] = 0xBCL;
                if ((safe_add_func_int64_t_s_s(0xE7090AC2FE30494FLL, ((safe_rshift_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(4294967292UL, ((((*l_1627) &= (safe_mul_func_uint8_t_u_u(p_43, ((*l_1396) = (safe_rshift_func_uint16_t_u_u(0x9503L, 7)))))) != (((*l_1169) = 0x469E1F25ACFFEE46LL) >= (((*l_1120) = ((g_1646 , (g_1551.f1 == (((safe_div_func_uint16_t_u_u((p_43 < (((g_117[4][6].f2 ^ 0x04EC68D0L) , 0x79026E35058B1352LL) >= 0L)), 5UL)) | (*p_44)) | l_1649[0]))) != (*g_672))) ^ (*l_1168)))) < (-6L)))), 10)) >= p_43))))
                { /* block id: 633 */
                    union U2 **l_1664 = &l_1349;
                    union U2 ***l_1663 = &l_1664;
                    int32_t * const l_1667 = &g_828;
                    uint8_t **l_1696[3][10][6] = {{{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396}},{{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396}},{{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,&l_1396,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,(void*)0,&l_1396,(void*)0,&l_1396},{&l_1396,&l_1396,(void*)0,&l_1396,&l_1396,&l_1396},{&l_1396,&l_1396,(void*)0,&l_1396,(void*)0,&l_1396},{&l_1396,&l_1396,(void*)0,&l_1396,&l_1396,&l_1396}}};
                    uint8_t ***l_1695 = &l_1696[2][1][2];
                    int i, j, k;
                    if ((safe_add_func_int64_t_s_s(((((g_1319 = ((safe_mul_func_uint16_t_u_u((l_1656[2] | (((*l_1396) = (safe_mod_func_uint8_t_u_u(((safe_add_func_int64_t_s_s(((*g_672) = (l_1661 != (g_1662 , (**l_1367)))), (((*l_1663) = (void*)0) == g_1665))) && (!((((void*)0 == l_1667) < (safe_sub_func_int64_t_s_s((((safe_mod_func_uint64_t_u_u((((p_43 == (*l_1667)) < 0UL) | p_43), p_43)) != g_929[2][4]) > 0UL), p_43))) >= 0x5CB0L))), 0x3CL))) >= 8L)), p_43)) , &g_1418)) == (*l_1338)) , (*p_44)) != 0x78L), p_43)))
                    { /* block id: 638 */
                        int32_t *l_1672 = (void*)0;
                        float *l_1698 = &l_1630;
                        float *l_1699 = &l_1209[2][4];
                        l_1672 = ((*g_75) = &p_43);
                        (*l_1699) = (((*l_1698) = (safe_sub_func_float_f_f(((*g_417) = ((g_1675 , (*g_417)) <= (safe_add_func_float_f_f(((((((*g_672) = (safe_rshift_func_uint16_t_u_u(p_43, p_43))) & ((*l_1627) = (safe_lshift_func_uint8_t_u_s((safe_lshift_func_uint16_t_u_u((*g_1319), (l_1697 = (((safe_rshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_s((((*g_16) = (!(((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_u(((((((*l_1453) = p_43) != 0x6158B9CCL) , l_1693[0]) == l_1695) != p_43), 11)) > p_43), p_43)) ^ 1L) < (*l_1672)))) || (*l_1169)), p_43)), 5)) , 0x6EL) ^ p_43)))), 3)))) == (*g_76)) , (-0x10.3p+1)) <= 0x1.EAFC94p+66), g_766)))), 0x3.Dp-1))) < g_1449);
                    }
                    else
                    { /* block id: 649 */
                        if (p_43)
                            break;
                    }
                }
                else
                { /* block id: 652 */
                    float *l_1714 = &l_1630;
                    int32_t *l_1715 = &l_1047;
                    l_1715 = ((safe_unary_minus_func_uint8_t_u(g_1150)) , ((safe_add_func_float_f_f((safe_div_func_float_f_f(((*l_1714) = ((safe_div_func_float_f_f((p_43 < p_43), l_1697)) >= (safe_sub_func_float_f_f(l_1709, (safe_mul_func_float_f_f(((l_924 , (-0x6.6p+1)) >= (*l_1169)), (safe_sub_func_float_f_f(p_43, p_43)))))))), (-0x1.Bp-1))), 0x8.E8AC3Bp+35)) , &p_43));
                }
                if (((g_1716 = &l_1251[1][1][0]) == ((*l_1488) = &g_1319)))
                { /* block id: 658 */
                    for (g_527.f4 = 0; (g_527.f4 <= 1); g_527.f4 += 1)
                    { /* block id: 661 */
                        int32_t ****l_1718 = &l_777[1];
                        int32_t *****l_1717 = &l_1718;
                        int32_t ****l_1720 = &l_777[1];
                        int32_t *****l_1719 = &l_1720;
                        int i;
                        (*g_75) = &l_1436[(g_527.f4 + 3)];
                        (*l_1719) = ((*l_1717) = &l_777[1]);
                    }
                }
                else
                { /* block id: 666 */
                    l_1602[1] = (void*)0;
                }
                for (l_1472 = 2; (l_1472 >= 0); l_1472 -= 1)
                { /* block id: 671 */
                    int i;
                    return l_1602[l_1472];
                }
                (*g_1722) = &g_1140[5];
            }
        }
        else
        { /* block id: 676 */
            (*g_727) |= 0xFEC743BFL;
            for (g_1210 = 0; (g_1210 <= 17); g_1210 = safe_add_func_uint32_t_u_u(g_1210, 8))
            { /* block id: 680 */
                union U2 **l_1727 = &l_1277;
                (*g_727) = 5L;
                if (p_43)
                    break;
                if (p_43)
                    continue;
                (*l_1727) = &g_333;
            }
            for (g_164.f1 = 1; (g_164.f1 >= 0); g_164.f1 -= 1)
            { /* block id: 688 */
                return g_1728;
            }
        }
        for (l_1046 = 0; (l_1046 <= 8); l_1046++)
        { /* block id: 694 */
            int16_t l_1731 = 0x86A0L;
            int32_t *l_1732 = &g_828;
            uint8_t ** const l_1738[4][6] = {{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735},{&g_1735,&g_1735,&g_1735,&g_1735,&g_1735,&g_1735}};
            uint8_t ** const *l_1737 = &l_1738[2][3];
            int8_t ****l_1747[3][7] = {{&g_1600,&g_1600,&g_1600,&g_1600,&g_1600,&g_1600,&g_1600},{&g_1600,&g_1600,&g_1600,&g_1600,&g_1600,&g_1600,&g_1600},{&g_1600,&g_1600,&g_1600,&g_1600,&g_1600,&g_1600,&g_1600}};
            int i, j;
            if (l_1731)
            { /* block id: 695 */
                return l_1732;
            }
            else
            { /* block id: 697 */
                uint8_t ** const **l_1736 = &g_1733;
                int32_t l_1741 = 0x589457A6L;
                int32_t *l_1748 = &l_928;
                int32_t l_1764 = 1L;
                l_1737 = ((*l_1736) = g_1733);
                p_43 = (g_1749 = (p_43 , ((*g_1728) = ((0x0C4B70298DCF9C41LL <= (0xEAA1D5B234F21B5CLL <= (((*l_1748) = (safe_mod_func_int8_t_s_s((((*l_1169) = (*p_44)) <= (l_1741 <= ((p_43 , (((*l_1732) = ((*g_727) = ((safe_unary_minus_func_int8_t_s((safe_add_func_uint16_t_u_u(((0x2DL >= (safe_add_func_int64_t_s_s((l_1747[1][3] == &l_1586), 1L))) == p_43), l_1741)))) , l_1741))) ^ p_43)) == g_1448))), (*p_44)))) , l_1741))) > l_1741))));
                (*g_1728) |= (g_1750 , (!((((*l_1732) , &l_1349) == (l_1752[0][5] = &l_1277)) & (l_1764 = ((safe_div_func_uint64_t_u_u((1UL ^ (((*l_1169) &= ((safe_sub_func_int8_t_s_s(((safe_sub_func_float_f_f(l_1759, (((!((((safe_mul_func_float_f_f((+l_1741), p_43)) != 0x0.Ap+1) < p_43) < (*g_417))) != p_43) == g_388))) , (*l_1732)), (*g_422))) >= p_43)) >= l_1741)), (*l_1732))) , 65535UL)))));
            }
            l_1765++;
        }
    }
    return l_1768;
}


/* ------------------------------------------ */
/* 
 * reads : g_255 g_117.f2
 * writes:
 */
static uint16_t  func_47(float  p_48, const int8_t * p_49, const int32_t  p_50, int8_t * p_51)
{ /* block id: 298 */
    const uint16_t *l_772 = &g_574;
    const uint16_t **l_771 = &l_772;
    const uint16_t ***l_773 = &l_771;
    (*l_773) = l_771;
    return (*g_255);
}


/* ------------------------------------------ */
/* 
 * reads : g_527.f1
 * writes:
 */
static int8_t * func_52(int8_t  p_53, int32_t * p_54, int64_t  p_55, const int8_t * p_56)
{ /* block id: 294 */
    int32_t *l_749 = &g_527.f1;
    int32_t *l_750 = (void*)0;
    int32_t *l_751 = &g_164.f1;
    int32_t *l_752 = &g_527.f1;
    int32_t *l_753 = &g_527.f1;
    int32_t *l_754 = (void*)0;
    int32_t *l_755 = &g_527.f1;
    int32_t *l_756 = &g_527.f1;
    int32_t *l_757 = &g_527.f1;
    int32_t l_758 = (-1L);
    int32_t *l_759 = &l_758;
    int32_t *l_760 = &g_117[4][6].f1;
    int32_t *l_761 = &l_758;
    int32_t *l_762 = &g_164.f1;
    int32_t l_763 = 0L;
    int32_t *l_764[7][3] = {{&l_763,(void*)0,&g_117[4][6].f1},{&l_763,&l_763,&g_117[4][6].f1},{(void*)0,&l_763,&l_758},{&l_758,&l_763,&l_758},{&l_758,(void*)0,&l_763},{(void*)0,&l_758,&l_758},{&l_763,&l_758,&l_758}};
    int32_t l_765 = 0x23D5C9C7L;
    uint8_t l_767 = 0x67L;
    int8_t *l_770 = &g_17[7];
    int i, j;
    l_767++;
    (*l_759) = (*l_749);
    return l_770;
}


/* ------------------------------------------ */
/* 
 * reads : g_75 g_3 g_4 g_25 g_71.f0 g_90 g_76
 * writes: g_75 g_94 g_97 g_90 g_164.f4
 */
static int16_t  func_68(int8_t  p_69, int32_t * p_70)
{ /* block id: 10 */
    uint16_t l_72[2][1];
    const uint32_t l_85 = 0x4C7D0E75L;
    int64_t l_86 = (-7L);
    int32_t l_91 = 1L;
    int8_t *l_95 = &g_17[7];
    int32_t l_187 = 0xE951E9A9L;
    int32_t l_189 = (-6L);
    int32_t l_191 = 0xA4EC0DFEL;
    int32_t l_222[9] = {0x1E270983L,0L,0L,0x1E270983L,0L,0L,0x1E270983L,0L,0L};
    int32_t **l_269 = &g_76;
    union U2 *l_332 = &g_333;
    uint16_t l_338 = 0xA99EL;
    int32_t l_357 = 4L;
    uint64_t l_425 = 0UL;
    const int8_t *l_431 = &g_17[7];
    const int8_t **l_430[7][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_431,(void*)0,&l_431,(void*)0,&l_431,(void*)0,&l_431},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_431,(void*)0,&l_431,(void*)0,&l_431,(void*)0,&l_431},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_431,(void*)0,&l_431,(void*)0,&l_431,(void*)0,&l_431},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    int8_t *l_464 = &g_58.f0;
    int8_t l_478 = 0xAFL;
    uint32_t l_487 = 0x64724527L;
    int16_t l_509 = 0x52B3L;
    uint64_t *l_524 = &g_90;
    uint64_t **l_523 = &l_524;
    uint64_t *** const l_562 = &l_523;
    uint64_t *** const *l_561 = &l_562;
    union U0 *l_577 = &g_117[5][3];
    float l_611 = (-0x8.8p-1);
    float l_641[6] = {0x0.2p-1,0x1.6p-1,0x0.2p-1,0x0.2p-1,0x1.6p-1,0x0.2p-1};
    float l_681 = 0x0.Ap+1;
    uint16_t l_744 = 7UL;
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_72[i][j] = 1UL;
    }
    l_72[0][0]--;
    for (p_69 = 0; (p_69 <= 0); p_69 += 1)
    { /* block id: 14 */
        int8_t *l_82 = &g_58.f0;
        int8_t **l_81 = &l_82;
        uint64_t *l_89[6][2] = {{&g_90,&g_90},{&g_90,&g_90},{&g_90,&g_90},{&g_90,&g_90},{&g_90,&g_90},{&g_90,&g_90}};
        const int32_t l_92 = 0x0BBC0977L;
        int16_t *l_93 = &g_94[2];
        float *l_96 = &g_97;
        const int32_t *l_104 = &l_91;
        int32_t l_118 = 1L;
        int32_t l_119[3][3] = {{0x3429DD6FL,(-5L),0x3429DD6FL},{0x3429DD6FL,(-5L),0x3429DD6FL},{0x3429DD6FL,(-5L),0x3429DD6FL}};
        uint8_t l_122 = 0x48L;
        int16_t l_231[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
        uint8_t l_295 = 0xF3L;
        int i, j;
        g_75 = g_75;
        (*l_96) = (0x1.D4F00Dp+89 <= (safe_add_func_float_f_f((safe_div_func_float_f_f((((*l_81) = &g_17[9]) == (((g_3[(p_69 + 1)] && (((g_3[(p_69 + 1)] == (safe_rshift_func_int16_t_s_s(((*l_93) = (((((*p_70) | (((l_91 = (l_85 , (l_86 || (safe_add_func_int64_t_s_s((((&g_17[8] == &p_69) < 0UL) && (*p_70)), l_86))))) == 1UL) && p_69)) | g_25) , 0xF93D1D1CE175E042LL) & l_92)), g_71.f0))) <= 0x2D11L) | g_4)) , l_85) , l_95)), (-0x8.Fp+1))), l_72[0][0])));
        for (g_90 = 0; (g_90 <= 0); g_90 += 1)
        { /* block id: 22 */
            int32_t l_100 = 1L;
            int i, j;
            l_100 = (safe_div_func_float_f_f((0x5.9p+1 == l_72[(p_69 + 1)][g_90]), l_72[(p_69 + 1)][g_90]));
            (*l_96) = ((void*)0 != l_93);
        }
        if ((**g_75))
            break;
        for (l_86 = 0; (l_86 >= 0); l_86 -= 1)
        { /* block id: 29 */
            const int32_t **l_101 = (void*)0;
            const int32_t *l_103 = &l_92;
            const int32_t **l_102 = &l_103;
            int8_t *l_107[4][10] = {{&g_71.f0,(void*)0,&g_17[4],&g_17[7],&g_58.f0,&g_71.f0,&g_17[7],&g_58.f0,&g_17[7],&g_17[6]},{&g_17[7],&g_17[7],&g_17[9],&g_17[7],&g_17[7],(void*)0,&g_17[7],&g_58.f0,(void*)0,&g_17[7]},{&g_17[7],&g_17[7],(void*)0,&g_17[7],&g_17[6],&g_17[6],&g_17[7],(void*)0,&g_17[7],&g_17[7]},{&g_17[6],&g_17[7],(void*)0,&g_17[7],&g_17[7],&g_17[6],&g_17[7],&g_58.f0,&g_17[7],&g_17[6]}};
            int32_t l_112 = 0x91AF2E20L;
            int32_t l_186 = 1L;
            uint64_t l_213 = 0x195C60AA6FE15A51LL;
            int32_t l_223 = (-1L);
            int32_t l_225 = 0xAAD057FEL;
            int32_t l_226[8] = {0x9048F6AEL,0x9048F6AEL,9L,0x9048F6AEL,0x9048F6AEL,9L,0x9048F6AEL,0x9048F6AEL};
            int32_t l_230[7];
            int8_t l_234 = 0xF7L;
            uint8_t l_319[6] = {0xCEL,0xCEL,0xCEL,0xCEL,0xCEL,0xCEL};
            int16_t *l_329[10] = {&l_231[2],&l_231[2],&l_231[2],&l_231[2],&l_231[2],&l_231[2],&l_231[2],&l_231[2],&l_231[2],&l_231[2]};
            int i, j;
            for (i = 0; i < 7; i++)
                l_230[i] = 1L;
            l_104 = ((*l_102) = (*g_75));
        }
    }
    for (l_189 = 0; (l_189 >= 18); l_189++)
    { /* block id: 112 */
        int32_t *l_336 = &g_164.f1;
        int32_t *l_337[7];
        int16_t l_370[5] = {0xC967L,0xC967L,0xC967L,0xC967L,0xC967L};
        int8_t **l_377 = &g_16;
        float l_415 = 0xE.79A3B4p+20;
        union U2 l_479 = {5L};
        uint16_t *l_484[10][4] = {{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338},{&l_338,&l_338,&l_338,&l_338}};
        uint8_t *l_518 = (void*)0;
        int64_t l_528 = 8L;
        int32_t **l_542[10][4] = {{(void*)0,&l_337[2],&l_337[0],&l_336},{&l_336,&g_76,&l_337[0],&l_337[0]},{&l_336,&g_76,(void*)0,&l_336},{&g_76,&l_337[2],&l_337[5],&l_337[2]},{&g_76,&l_337[0],(void*)0,&l_336},{&l_337[5],&l_337[0],&g_76,&l_337[0]},{&l_336,&g_76,&g_76,(void*)0},{&l_336,(void*)0,&g_76,&l_337[0]},{&l_337[5],(void*)0,(void*)0,&l_337[5]},{&g_76,&l_336,&l_337[5],&l_336}};
        int32_t l_637[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        float l_676[9] = {0x1.3p+1,0x1.3p+1,0x1.3p+1,0x1.3p+1,0x1.3p+1,0x1.3p+1,0x1.3p+1,0x1.3p+1,0x1.3p+1};
        float l_683 = 0xA.3562DAp-22;
        int i, j;
        for (i = 0; i < 7; i++)
            l_337[i] = &l_191;
        l_338--;
        for (g_164.f4 = 0; (g_164.f4 <= 7); g_164.f4 += 1)
        { /* block id: 116 */
            uint8_t l_351 = 0x7CL;
            int8_t * const *l_375 = &g_16;
            uint32_t l_382[10][3][8] = {{{4294967295UL,0xDFF2E105L,0xBB45C835L,0x5C95F3D1L,0x777306DEL,0xD1DCE0CCL,7UL,4294967295UL},{0x5735E194L,0x5469DEE0L,1UL,1UL,0UL,3UL,1UL,4294967294UL},{0UL,0UL,1UL,0x5FB3322AL,0UL,0x00B9685FL,0UL,0x5C95F3D1L}},{{0x43F70AA8L,0x777306DEL,0UL,4294967295UL,4294967290UL,0x43F70AA8L,0x43F70AA8L,4294967295UL},{0UL,0UL,0xD2512C9CL,0x67ACE969L,0UL,0UL,4294967293UL,0xDFF2E105L},{0x2CFFF82BL,0UL,4294967295UL,0x62BC178BL,4294967295UL,1UL,0x01C12E97L,1UL}},{{4294967292UL,4294967295UL,4294967292UL,4294967295UL,4294967292UL,0x777306DEL,0x7DB4C886L,0x90CB32F4L},{0x5469DEE0L,0x5A17D55DL,0UL,0x87789C42L,0xF904CA89L,1UL,0UL,4294967295UL},{4294967295UL,0x43F70AA8L,0UL,1UL,4294967291UL,4294967295UL,0x7DB4C886L,0x9F9BF1DCL}},{{0xF904CA89L,0x39DE4035L,4294967292UL,0x90CB32F4L,0xF24AFC16L,0xD1DCE0CCL,0x01C12E97L,0x4E5E474BL},{1UL,4294967295UL,4294967295UL,4294967293UL,0xD1DCE0CCL,0x7CE5D51AL,4294967293UL,0x7AF10A22L},{0UL,0UL,0xD2512C9CL,0x7DB4C886L,0x0A073962L,0x6B4FDA9AL,0x43F70AA8L,0x15E0B628L}},{{0xA431C954L,1UL,4294967295UL,4294967291UL,0xBB45C835L,0x5469DEE0L,0x0E99FCE0L,0x43F70AA8L},{0x0EA9EC9EL,0x43F70AA8L,8UL,0x2CFFF82BL,0x39DE4035L,0xFBA9004AL,0xBB45C835L,0UL},{0x62BC178BL,0xB1E80A6BL,0x1D132825L,0xF325F45BL,4294967295UL,0xBB45C835L,0x91B3C2D2L,0x1D132825L}},{{0x37F66250L,0UL,0x7CE5D51AL,4294967295UL,0x43F70AA8L,1UL,0UL,0x5FB3322AL},{0UL,8UL,4294967290UL,4294967295UL,0x6B4FDA9AL,1UL,4294967293UL,4294967295UL},{0xC56AF300L,1UL,4294967295UL,0x4E5E474BL,0x62BC178BL,0UL,0x7D985C54L,0x9337297BL}},{{0xBF52D40BL,0x9A634C52L,0x62BC178BL,4294967295UL,4294967291UL,1UL,0x9337297BL,0UL},{4294967293UL,4294967295UL,4294967295UL,0UL,3UL,4294967289UL,0UL,0xA107BAB8L},{4294967295UL,0x00B9685FL,0x0EA9EC9EL,4294967287UL,4294967287UL,0x0EA9EC9EL,0x00B9685FL,4294967295UL}},{{4294967292UL,0x62BC178BL,0UL,0x7DB4C886L,0xA431C954L,0xEBA8F660L,4294967293UL,0x87789C42L},{0x7D985C54L,4294967290UL,0xBB45C835L,4294967292UL,0x7DB4C886L,0xEBA8F660L,0x5C95F3D1L,4294967295UL},{4294967295UL,0x62BC178BL,0x9A634C52L,0xBF52D40BL,4294967295UL,0x0EA9EC9EL,0UL,0x9F9BF1DCL}},{{4294967295UL,0x00B9685FL,0x1D132825L,0xB46AF863L,0UL,4294967289UL,4294967295UL,1UL},{1UL,4294967295UL,1UL,4294967293UL,0xB39D5645L,1UL,3UL,0xBF52D40BL},{0UL,0x9A634C52L,1UL,4294967295UL,2UL,0UL,0x43F70AA8L,0xD2512C9CL}},{{0xEBA8F660L,1UL,4294967295UL,0x67ACE969L,0x7DB4C886L,8UL,0x43F70AA8L,0x0EA9EC9EL},{4294967291UL,4294967295UL,4294967294UL,4294967292UL,0x859DBF21L,0x1D132825L,0x7AF10A22L,0xFBA9004AL},{1UL,0x859DBF21L,4294967295UL,4294967291UL,0x5A17D55DL,0x7CE5D51AL,0x7D985C54L,4294967293UL}}};
            int32_t l_408[10] = {0xD6E719C7L,2L,2L,0xD6E719C7L,2L,2L,0xD6E719C7L,2L,2L,0xD6E719C7L};
            int32_t l_423 = 0xC23D63E7L;
            int32_t l_424 = 1L;
            uint64_t *l_449 = &g_90;
            uint64_t **l_448 = &l_449;
            uint64_t ***l_447[10][2];
            uint64_t ****l_446 = &l_447[4][1];
            uint64_t *****l_445 = &l_446;
            int64_t l_486 = 0x1A48D71EEB2332D1LL;
            int32_t l_510 = 0x3315F5FEL;
            int i, j, k;
            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 2; j++)
                    l_447[i][j] = &l_448;
            }
        }
    }
    return (**l_269);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_3[i], "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_4, "g_4", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_17[i], "g_17[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_25, "g_25", print_hash_value);
    transparent_crc(g_58.f0, "g_58.f0", print_hash_value);
    transparent_crc(g_71.f0, "g_71.f0", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_94[i], "g_94[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_97, sizeof(g_97), "g_97", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_117[i][j].f0, "g_117[i][j].f0", print_hash_value);
            transparent_crc(g_117[i][j].f1, "g_117[i][j].f1", print_hash_value);
            transparent_crc(g_117[i][j].f2, "g_117[i][j].f2", print_hash_value);
            transparent_crc(g_117[i][j].f3, "g_117[i][j].f3", print_hash_value);
            transparent_crc(g_117[i][j].f4, "g_117[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_131, "g_131", print_hash_value);
    transparent_crc(g_154, "g_154", print_hash_value);
    transparent_crc(g_164.f0, "g_164.f0", print_hash_value);
    transparent_crc(g_164.f1, "g_164.f1", print_hash_value);
    transparent_crc(g_164.f2, "g_164.f2", print_hash_value);
    transparent_crc(g_164.f3, "g_164.f3", print_hash_value);
    transparent_crc(g_164.f4, "g_164.f4", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_167[i][j].f0, "g_167[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_218, "g_218", print_hash_value);
    transparent_crc(g_333.f0, "g_333.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_345[i][j][k].f0, "g_345[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_350, "g_350", print_hash_value);
    transparent_crc(g_388, "g_388", print_hash_value);
    transparent_crc(g_401.f0, "g_401.f0", print_hash_value);
    transparent_crc_bytes (&g_418, sizeof(g_418), "g_418", print_hash_value);
    transparent_crc(g_489.f0, "g_489.f0", print_hash_value);
    transparent_crc(g_527.f1, "g_527.f1", print_hash_value);
    transparent_crc(g_527.f2, "g_527.f2", print_hash_value);
    transparent_crc(g_527.f4, "g_527.f4", print_hash_value);
    transparent_crc(g_571.f0, "g_571.f0", print_hash_value);
    transparent_crc(g_571.f1, "g_571.f1", print_hash_value);
    transparent_crc(g_571.f2, "g_571.f2", print_hash_value);
    transparent_crc(g_571.f3, "g_571.f3", print_hash_value);
    transparent_crc(g_571.f4, "g_571.f4", print_hash_value);
    transparent_crc(g_574, "g_574", print_hash_value);
    transparent_crc(g_635, "g_635", print_hash_value);
    transparent_crc(g_692.f0, "g_692.f0", print_hash_value);
    transparent_crc(g_712, "g_712", print_hash_value);
    transparent_crc(g_766, "g_766", print_hash_value);
    transparent_crc(g_791, "g_791", print_hash_value);
    transparent_crc(g_828, "g_828", print_hash_value);
    transparent_crc(g_913, "g_913", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_929[i][j], "g_929[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_951.f0, "g_951.f0", print_hash_value);
    transparent_crc(g_951.f1, "g_951.f1", print_hash_value);
    transparent_crc(g_951.f2, "g_951.f2", print_hash_value);
    transparent_crc(g_951.f3, "g_951.f3", print_hash_value);
    transparent_crc(g_951.f4, "g_951.f4", print_hash_value);
    transparent_crc(g_982.f0, "g_982.f0", print_hash_value);
    transparent_crc(g_993.f0, "g_993.f0", print_hash_value);
    transparent_crc(g_1042, "g_1042", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1071[i][j], "g_1071[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1115.f0, "g_1115.f0", print_hash_value);
    transparent_crc(g_1139.f0, "g_1139.f0", print_hash_value);
    transparent_crc(g_1139.f1, "g_1139.f1", print_hash_value);
    transparent_crc(g_1139.f2, "g_1139.f2", print_hash_value);
    transparent_crc(g_1139.f3, "g_1139.f3", print_hash_value);
    transparent_crc(g_1139.f4, "g_1139.f4", print_hash_value);
    transparent_crc(g_1150, "g_1150", print_hash_value);
    transparent_crc(g_1210, "g_1210", print_hash_value);
    transparent_crc(g_1362.f0, "g_1362.f0", print_hash_value);
    transparent_crc(g_1418, "g_1418", print_hash_value);
    transparent_crc(g_1448, "g_1448", print_hash_value);
    transparent_crc(g_1449, "g_1449", print_hash_value);
    transparent_crc(g_1470, "g_1470", print_hash_value);
    transparent_crc(g_1530.f0, "g_1530.f0", print_hash_value);
    transparent_crc(g_1551.f0, "g_1551.f0", print_hash_value);
    transparent_crc(g_1551.f1, "g_1551.f1", print_hash_value);
    transparent_crc(g_1551.f2, "g_1551.f2", print_hash_value);
    transparent_crc(g_1551.f3, "g_1551.f3", print_hash_value);
    transparent_crc(g_1551.f4, "g_1551.f4", print_hash_value);
    transparent_crc(g_1628, "g_1628", print_hash_value);
    transparent_crc(g_1646.f0, "g_1646.f0", print_hash_value);
    transparent_crc(g_1646.f1, "g_1646.f1", print_hash_value);
    transparent_crc(g_1646.f2, "g_1646.f2", print_hash_value);
    transparent_crc(g_1646.f3, "g_1646.f3", print_hash_value);
    transparent_crc(g_1646.f4, "g_1646.f4", print_hash_value);
    transparent_crc(g_1662.f4, "g_1662.f4", print_hash_value);
    transparent_crc(g_1675.f0, "g_1675.f0", print_hash_value);
    transparent_crc(g_1749, "g_1749", print_hash_value);
    transparent_crc(g_1750.f4, "g_1750.f4", print_hash_value);
    transparent_crc(g_1772, "g_1772", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1788[i][j][k].f0, "g_1788[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1789, "g_1789", print_hash_value);
    transparent_crc(g_1843, "g_1843", print_hash_value);
    transparent_crc(g_1861, "g_1861", print_hash_value);
    transparent_crc(g_1870, "g_1870", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1927[i][j], "g_1927[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1967, "g_1967", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_1989[i][j], "g_1989[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2031[i], "g_2031[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2087.f0, "g_2087.f0", print_hash_value);
    transparent_crc(g_2141, "g_2141", print_hash_value);
    transparent_crc(g_2163, "g_2163", print_hash_value);
    transparent_crc(g_2258.f0, "g_2258.f0", print_hash_value);
    transparent_crc(g_2333, "g_2333", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_2397[i][j][k], "g_2397[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2408, "g_2408", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2421[i], "g_2421[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2422, "g_2422", print_hash_value);
    transparent_crc_bytes (&g_2424, sizeof(g_2424), "g_2424", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_2557[i][j], "g_2557[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2593.f0, "g_2593.f0", print_hash_value);
    transparent_crc(g_2593.f1, "g_2593.f1", print_hash_value);
    transparent_crc(g_2593.f2, "g_2593.f2", print_hash_value);
    transparent_crc(g_2593.f3, "g_2593.f3", print_hash_value);
    transparent_crc(g_2593.f4, "g_2593.f4", print_hash_value);
    transparent_crc(g_2625.f0, "g_2625.f0", print_hash_value);
    transparent_crc(g_2625.f1, "g_2625.f1", print_hash_value);
    transparent_crc(g_2625.f2, "g_2625.f2", print_hash_value);
    transparent_crc(g_2625.f3, "g_2625.f3", print_hash_value);
    transparent_crc(g_2625.f4, "g_2625.f4", print_hash_value);
    transparent_crc(g_2638, "g_2638", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_2648[i][j][k], sizeof(g_2648[i][j][k]), "g_2648[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2713.f0, "g_2713.f0", print_hash_value);
    transparent_crc(g_2727, "g_2727", print_hash_value);
    transparent_crc(g_2839.f0, "g_2839.f0", print_hash_value);
    transparent_crc(g_2850.f0, "g_2850.f0", print_hash_value);
    transparent_crc(g_2850.f1, "g_2850.f1", print_hash_value);
    transparent_crc(g_2850.f2, "g_2850.f2", print_hash_value);
    transparent_crc(g_2850.f3, "g_2850.f3", print_hash_value);
    transparent_crc(g_2850.f4, "g_2850.f4", print_hash_value);
    transparent_crc(g_2910, "g_2910", print_hash_value);
    transparent_crc(g_2942.f0, "g_2942.f0", print_hash_value);
    transparent_crc(g_3000.f0, "g_3000.f0", print_hash_value);
    transparent_crc(g_3000.f1, "g_3000.f1", print_hash_value);
    transparent_crc(g_3000.f2, "g_3000.f2", print_hash_value);
    transparent_crc(g_3000.f3, "g_3000.f3", print_hash_value);
    transparent_crc(g_3000.f4, "g_3000.f4", print_hash_value);
    transparent_crc(g_3019, "g_3019", print_hash_value);
    transparent_crc_bytes (&g_3024, sizeof(g_3024), "g_3024", print_hash_value);
    transparent_crc(g_3153, "g_3153", print_hash_value);
    transparent_crc(g_3157.f0, "g_3157.f0", print_hash_value);
    transparent_crc(g_3157.f1, "g_3157.f1", print_hash_value);
    transparent_crc(g_3157.f2, "g_3157.f2", print_hash_value);
    transparent_crc(g_3157.f3, "g_3157.f3", print_hash_value);
    transparent_crc(g_3157.f4, "g_3157.f4", print_hash_value);
    transparent_crc(g_3192, "g_3192", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_3210[i].f0, "g_3210[i].f0", print_hash_value);
        transparent_crc(g_3210[i].f1, "g_3210[i].f1", print_hash_value);
        transparent_crc(g_3210[i].f2, "g_3210[i].f2", print_hash_value);
        transparent_crc(g_3210[i].f3, "g_3210[i].f3", print_hash_value);
        transparent_crc(g_3210[i].f4, "g_3210[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3228.f0, "g_3228.f0", print_hash_value);
    transparent_crc(g_3228.f1, "g_3228.f1", print_hash_value);
    transparent_crc(g_3228.f2, "g_3228.f2", print_hash_value);
    transparent_crc(g_3228.f3, "g_3228.f3", print_hash_value);
    transparent_crc(g_3228.f4, "g_3228.f4", print_hash_value);
    transparent_crc_bytes (&g_3365, sizeof(g_3365), "g_3365", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_3416[i][j][k].f0, "g_3416[i][j][k].f0", print_hash_value);
                transparent_crc(g_3416[i][j][k].f1, "g_3416[i][j][k].f1", print_hash_value);
                transparent_crc(g_3416[i][j][k].f2, "g_3416[i][j][k].f2", print_hash_value);
                transparent_crc(g_3416[i][j][k].f3, "g_3416[i][j][k].f3", print_hash_value);
                transparent_crc(g_3416[i][j][k].f4, "g_3416[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3430.f0, "g_3430.f0", print_hash_value);
    transparent_crc(g_3430.f1, "g_3430.f1", print_hash_value);
    transparent_crc(g_3430.f2, "g_3430.f2", print_hash_value);
    transparent_crc(g_3430.f3, "g_3430.f3", print_hash_value);
    transparent_crc(g_3430.f4, "g_3430.f4", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_3481[i][j][k], "g_3481[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_3505[i].f0, "g_3505[i].f0", print_hash_value);
        transparent_crc(g_3505[i].f1, "g_3505[i].f1", print_hash_value);
        transparent_crc(g_3505[i].f2, "g_3505[i].f2", print_hash_value);
        transparent_crc(g_3505[i].f3, "g_3505[i].f3", print_hash_value);
        transparent_crc(g_3505[i].f4, "g_3505[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3536, "g_3536", print_hash_value);
    transparent_crc(g_3583.f0, "g_3583.f0", print_hash_value);
    transparent_crc(g_3598, "g_3598", print_hash_value);
    transparent_crc(g_3607, "g_3607", print_hash_value);
    transparent_crc(g_3608, "g_3608", print_hash_value);
    transparent_crc(g_3611, "g_3611", print_hash_value);
    transparent_crc(g_3791, "g_3791", print_hash_value);
    transparent_crc_bytes (&g_3845, sizeof(g_3845), "g_3845", print_hash_value);
    transparent_crc_bytes (&g_3848, sizeof(g_3848), "g_3848", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_3869[i][j][k], "g_3869[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3873.f0, "g_3873.f0", print_hash_value);
    transparent_crc(g_3887.f0, "g_3887.f0", print_hash_value);
    transparent_crc(g_3887.f1, "g_3887.f1", print_hash_value);
    transparent_crc(g_3887.f2, "g_3887.f2", print_hash_value);
    transparent_crc(g_3887.f3, "g_3887.f3", print_hash_value);
    transparent_crc(g_3887.f4, "g_3887.f4", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_3907[i][j][k], "g_3907[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3972.f0, "g_3972.f0", print_hash_value);
    transparent_crc(g_3972.f1, "g_3972.f1", print_hash_value);
    transparent_crc(g_3972.f2, "g_3972.f2", print_hash_value);
    transparent_crc(g_3972.f3, "g_3972.f3", print_hash_value);
    transparent_crc(g_3972.f4, "g_3972.f4", print_hash_value);
    transparent_crc(g_4110, "g_4110", print_hash_value);
    transparent_crc(g_4124.f0, "g_4124.f0", print_hash_value);
    transparent_crc(g_4124.f1, "g_4124.f1", print_hash_value);
    transparent_crc(g_4124.f2, "g_4124.f2", print_hash_value);
    transparent_crc(g_4124.f3, "g_4124.f3", print_hash_value);
    transparent_crc(g_4124.f4, "g_4124.f4", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_4152[i][j], "g_4152[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_4164, "g_4164", print_hash_value);
    transparent_crc(g_4180.f0, "g_4180.f0", print_hash_value);
    transparent_crc(g_4180.f1, "g_4180.f1", print_hash_value);
    transparent_crc(g_4180.f2, "g_4180.f2", print_hash_value);
    transparent_crc(g_4180.f3, "g_4180.f3", print_hash_value);
    transparent_crc(g_4180.f4, "g_4180.f4", print_hash_value);
    transparent_crc(g_4439, "g_4439", print_hash_value);
    transparent_crc(g_4525, "g_4525", print_hash_value);
    transparent_crc(g_4603, "g_4603", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1282
XXX total union variables: 64

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 34
breakdown:
   depth: 1, occurrence: 178
   depth: 2, occurrence: 51
   depth: 3, occurrence: 5
   depth: 4, occurrence: 4
   depth: 6, occurrence: 1
   depth: 9, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 16, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 24, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 2
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 3
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1

XXX total number of pointers: 938

XXX times a variable address is taken: 2368
XXX times a pointer is dereferenced on RHS: 655
breakdown:
   depth: 1, occurrence: 503
   depth: 2, occurrence: 117
   depth: 3, occurrence: 28
   depth: 4, occurrence: 4
   depth: 5, occurrence: 3
XXX times a pointer is dereferenced on LHS: 550
breakdown:
   depth: 1, occurrence: 515
   depth: 2, occurrence: 27
   depth: 3, occurrence: 6
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 75
XXX times a pointer is compared with address of another variable: 21
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 20234

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2197
   level: 2, occurrence: 597
   level: 3, occurrence: 245
   level: 4, occurrence: 129
   level: 5, occurrence: 35
XXX number of pointers point to pointers: 419
XXX number of pointers point to scalars: 475
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.9
XXX average alias set size: 1.54

XXX times a non-volatile is read: 3616
XXX times a non-volatile is write: 1648
XXX times a volatile is read: 227
XXX    times read thru a pointer: 59
XXX times a volatile is write: 61
XXX    times written thru a pointer: 8
XXX times a volatile is available for access: 1.54e+04
XXX percentage of non-volatile access: 94.8

XXX forward jumps: 2
XXX backward jumps: 19

XXX stmts: 181
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 29
   depth: 2, occurrence: 33
   depth: 3, occurrence: 28
   depth: 4, occurrence: 21
   depth: 5, occurrence: 39

XXX percentage a fresh-made variable is used: 15.6
XXX percentage an existing variable is used: 84.4
********************* end of statistics **********************/


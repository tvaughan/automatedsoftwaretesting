/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1259513914
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 1L;
static float g_6 = 0x4.Cp+1;
static volatile float g_7 = 0x7.689A5Ap-93;/* VOLATILE GLOBAL g_7 */
static volatile uint32_t g_9[5][2] = {{0UL,4294967293UL},{4294967293UL,0UL},{4294967293UL,4294967293UL},{0UL,4294967293UL},{4294967293UL,0UL}};
static int64_t g_17 = 0L;
static uint16_t g_29[9][2] = {{65526UL,65526UL},{6UL,65526UL},{65526UL,0xCD53L},{1UL,6UL},{6UL,1UL},{6UL,0xCD53L},{6UL,1UL},{6UL,6UL},{1UL,0xCD53L}};
static int64_t g_32 = 0L;
static int8_t g_45 = 0L;
static float g_46 = 0x1.Ep+1;
static float g_47 = 0x6.969E96p+58;
static int32_t g_48 = (-4L);
static uint64_t g_49 = 1UL;
static int32_t g_56 = 0x42B930EEL;
static uint32_t g_57 = 4294967288UL;
static uint64_t g_99 = 0x35BD15A5982ED821LL;
static int16_t g_108[7] = {8L,8L,8L,8L,8L,8L,8L};
static uint8_t g_111 = 255UL;
static float g_112[8] = {0x0.Ap+1,0x0.Ap+1,0x0.Ap+1,0x0.Ap+1,0x0.Ap+1,0x0.Ap+1,0x0.Ap+1,0x0.Ap+1};
static uint8_t g_115 = 0x4EL;
static int32_t g_123 = (-2L);
static int32_t *g_167 = &g_123;
static volatile int32_t * volatile * const g_177 = (void*)0;
static volatile int32_t * volatile * const  volatile *g_176 = &g_177;
static volatile int32_t g_181 = 0x5D277E2DL;/* VOLATILE GLOBAL g_181 */
static volatile int32_t *g_180 = &g_181;
static volatile int32_t * volatile * const  volatile g_179 = &g_180;/* VOLATILE GLOBAL g_179 */
static int16_t g_191 = 1L;
static uint32_t g_193 = 0x1CDAEA04L;
static int8_t g_255 = 4L;
static uint32_t g_280 = 0xF25BC62CL;
static int8_t g_379 = 0x55L;
static uint32_t g_436 = 1UL;
static uint64_t g_508 = 0xF873B4EECD880B42LL;
static uint64_t g_519 = 0x86B34E7BD40F7638LL;
static uint32_t g_521 = 0x6D6C550FL;
static uint16_t g_529[9] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
static uint16_t g_559 = 0x5D23L;
static uint16_t g_560 = 0x15B4L;
static uint8_t *g_574 = &g_111;
static int32_t *g_585 = (void*)0;
static int64_t g_643 = (-1L);
static int64_t g_644 = 7L;
static uint16_t g_646 = 0x50E4L;
static uint32_t g_651 = 4294967288UL;
static int32_t * const  volatile *g_655 = &g_585;
static int8_t g_700 = 0x4CL;
static int32_t g_701 = 6L;
static uint32_t g_702 = 2UL;
static int8_t g_706 = 0xBDL;
static int16_t g_707 = 0L;
static uint32_t g_708 = 0x82522365L;
static uint8_t **g_745 = &g_574;
static volatile int64_t g_754 = 1L;/* VOLATILE GLOBAL g_754 */
static volatile int64_t * volatile g_753 = &g_754;/* VOLATILE GLOBAL g_753 */
static volatile int64_t * volatile * const g_752 = &g_753;
static volatile int64_t g_758 = 0x1D5FBB19E27D1C35LL;/* VOLATILE GLOBAL g_758 */
static volatile int64_t * volatile g_757 = &g_758;/* VOLATILE GLOBAL g_757 */
static volatile int64_t g_760 = (-5L);/* VOLATILE GLOBAL g_760 */
static volatile int64_t * volatile g_759[4] = {&g_760,&g_760,&g_760,&g_760};
static volatile int64_t g_762 = 0x9002E4D6FE1EA730LL;/* VOLATILE GLOBAL g_762 */
static int8_t g_854 = (-1L);
static uint32_t g_857 = 5UL;
static float g_876 = 0x8.170C6Ap+55;
static uint32_t g_877[10] = {0UL,0xE74F90EDL,1UL,0xE74F90EDL,0UL,0UL,0xE74F90EDL,1UL,0xE74F90EDL,0UL};
static int8_t g_960 = 0xE8L;
static uint16_t g_962 = 0UL;
static int8_t g_1010 = 5L;
static uint8_t g_1011 = 1UL;
static uint64_t g_1061 = 0x7D344EEC4AB33098LL;
static uint16_t *g_1137 = &g_962;
static volatile uint64_t g_1140 = 0xF9E1256C5A756001LL;/* VOLATILE GLOBAL g_1140 */
static volatile uint64_t *g_1139 = &g_1140;
static volatile uint64_t **g_1138 = &g_1139;
static uint16_t g_1146 = 65529UL;
static int64_t **g_1178 = (void*)0;
static uint32_t g_1276 = 0x12BEE197L;
static int8_t g_1335 = (-5L);
static int16_t g_1338[10][8][3] = {{{0x1EADL,0L,0x3B36L},{0x1EADL,0xC273L,0L},{(-1L),(-1L),0xA94AL},{0L,(-8L),(-10L)},{0x5E7BL,0x5E7BL,4L},{3L,0xB0B0L,(-2L)},{0x0EE1L,(-2L),5L},{0L,0x41E7L,1L}},{{1L,0x0EE1L,5L},{(-10L),0x84BAL,(-2L)},{0x6870L,(-6L),4L},{(-1L),0x1EADL,(-10L)},{0L,(-1L),0xA94AL},{(-6L),0xA9A6L,0L},{0x78FFL,3L,0x3B36L},{0xA94AL,3L,(-8L)}},{{0x69D1L,0xA9A6L,(-6L)},{5L,(-1L),(-6L)},{0xF422L,0x1EADL,0xF422L},{0xC273L,(-6L),0x385AL},{(-4L),0x84BAL,0x78FFL},{0xA9A6L,0x0EE1L,(-1L)},{0xFA90L,0x41E7L,(-1L)},{0xA9A6L,(-2L),0x67ADL}},{{(-4L),0xB0B0L,0x41E7L},{0xC273L,0x5E7BL,(-4L)},{0xF422L,(-8L),0x0EE1L},{5L,(-1L),0L},{0x69D1L,0xC273L,(-1L)},{0xA94AL,0L,(-1L)},{0x78FFL,1L,0L},{(-6L),(-1L),0x0EE1L}},{{0L,(-4L),(-4L)},{(-1L),(-10L),0x41E7L},{0x6870L,(-1L),0x67ADL},{(-10L),1L,(-1L)},{1L,3L,(-1L)},{0L,1L,0x78FFL},{0x0EE1L,(-1L),0x385AL},{3L,(-10L),0xF422L}},{{0x5E7BL,(-4L),(-6L)},{0L,(-1L),(-6L)},{(-1L),1L,(-8L)},{0x1EADL,0L,0x3B36L},{0x1EADL,0xC273L,0L},{(-1L),(-1L),0xA94AL},{0L,(-8L),(-10L)},{0x5E7BL,0x5E7BL,4L}},{{3L,0xB0B0L,(-2L)},{0x0EE1L,(-2L),5L},{0L,0x41E7L,1L},{1L,0x0EE1L,5L},{(-10L),0x84BAL,(-2L)},{0x6870L,(-6L),4L},{(-1L),0x1EADL,(-10L)},{0L,(-1L),0xA94AL}},{{(-6L),0xA9A6L,0L},{1L,(-8L),4L},{0xC273L,(-8L),(-1L)},{0x385AL,(-4L),0x67ADL},{(-2L),0x9B6BL,0x0EE1L},{(-6L),0L,(-6L)},{0x41E7L,0x67ADL,3L},{0x09BEL,1L,1L}},{{(-4L),0xA9A6L,0x9B6BL},{0xB0B0L,0x6870L,0L},{(-4L),3L,5L},{0x09BEL,0xA94AL,0x6870L},{0x41E7L,(-1L),0x09BEL},{(-6L),(-1L),0xA9A6L},{(-2L),0x84BAL,(-10L)},{0x385AL,0x41E7L,(-6L)}},{{0xC273L,(-10L),(-6L)},{1L,0xF366L,(-10L)},{0x0EE1L,0xF422L,0xA9A6L},{0L,0x09BEL,0x09BEL},{(-6L),0xFA90L,0x6870L},{0xC438L,(-6L),5L},{0xFA90L,(-1L),0L},{0xF366L,(-1L),0x9B6BL}}};
static uint32_t g_1341 = 0UL;
static int32_t g_1345[5] = {0x877E877EL,0x877E877EL,0x877E877EL,0x877E877EL,0x877E877EL};
static int16_t *g_1383 = &g_108[2];
static int64_t ***g_1418 = &g_1178;
static int64_t ****g_1417 = &g_1418;
static int64_t **** const *g_1416[5][10] = {{&g_1417,(void*)0,&g_1417,(void*)0,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417},{&g_1417,&g_1417,&g_1417,&g_1417,(void*)0,&g_1417,&g_1417,&g_1417,&g_1417,(void*)0},{&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,(void*)0,&g_1417},{&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,&g_1417,(void*)0,&g_1417},{(void*)0,&g_1417,(void*)0,&g_1417,(void*)0,&g_1417,(void*)0,&g_1417,&g_1417,&g_1417}};
static uint32_t *g_1441 = &g_877[1];
static uint32_t ** const g_1440 = &g_1441;
static int8_t *g_1450 = &g_706;
static int8_t **g_1449 = &g_1450;
static uint64_t *g_1461 = &g_99;
static const uint64_t g_1495 = 18446744073709551615UL;
static int64_t * const * const *g_1551[1] = {(void*)0};
static uint16_t **g_1670 = &g_1137;
static uint16_t ** const *g_1669 = &g_1670;
static float *g_1769 = &g_46;
static int32_t **g_1864 = (void*)0;
static int32_t g_1930[5][8][6] = {{{1L,0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L},{1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L,1L},{0xA829EA5AL,0xA829EA5AL,0xBD7ACFC7L,0L,0xBD7ACFC7L,0xA829EA5AL},{0xBD7ACFC7L,1L,0L,0L,1L,0xBD7ACFC7L},{0xA829EA5AL,0xBD7ACFC7L,0L,0xBD7ACFC7L,0xA829EA5AL,0xA829EA5AL},{1L,0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L},{1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L,1L},{0xA829EA5AL,0xA829EA5AL,0xBD7ACFC7L,0L,0xBD7ACFC7L,0xA829EA5AL}},{{0xBD7ACFC7L,1L,0L,0L,1L,0xBD7ACFC7L},{0xA829EA5AL,0xBD7ACFC7L,0L,0xBD7ACFC7L,0xA829EA5AL,0xA829EA5AL},{1L,0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,0L},{0L,0xA829EA5AL,0L,1L,1L,0L},{0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L},{1L,0xA829EA5AL,1L,1L,0xA829EA5AL,1L},{0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L},{0L,1L,1L,0L,0xA829EA5AL,0L}},{{0L,0xA829EA5AL,0L,1L,1L,0L},{0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L},{1L,0xA829EA5AL,1L,1L,0xA829EA5AL,1L},{0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L},{0L,1L,1L,0L,0xA829EA5AL,0L},{0L,0xA829EA5AL,0L,1L,1L,0L},{0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L},{1L,0xA829EA5AL,1L,1L,0xA829EA5AL,1L}},{{0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L},{0L,1L,1L,0L,0xA829EA5AL,0L},{0L,0xA829EA5AL,0L,1L,1L,0L},{0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L},{1L,0xA829EA5AL,1L,1L,0xA829EA5AL,1L},{0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L},{0L,1L,1L,0L,0xA829EA5AL,0L},{0L,0xA829EA5AL,0L,1L,1L,0L}},{{0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L},{1L,0xA829EA5AL,1L,1L,0xA829EA5AL,1L},{0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L},{0L,1L,1L,0L,0xA829EA5AL,0L},{0L,0xA829EA5AL,0L,1L,1L,0L},{0xBD7ACFC7L,0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L},{1L,0xA829EA5AL,1L,1L,0xA829EA5AL,1L},{0xBD7ACFC7L,1L,1L,1L,0xBD7ACFC7L,0xBD7ACFC7L}}};
static const int32_t *g_1955 = &g_1345[2];
static uint64_t * const *g_2190[10][1][6] = {{{&g_1461,&g_1461,&g_1461,(void*)0,&g_1461,&g_1461}},{{&g_1461,(void*)0,&g_1461,&g_1461,(void*)0,(void*)0}},{{&g_1461,(void*)0,(void*)0,&g_1461,&g_1461,(void*)0}},{{&g_1461,(void*)0,&g_1461,(void*)0,&g_1461,&g_1461}},{{&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461}},{{&g_1461,(void*)0,&g_1461,(void*)0,&g_1461,&g_1461}},{{&g_1461,&g_1461,(void*)0,(void*)0,&g_1461,&g_1461}},{{(void*)0,(void*)0,&g_1461,&g_1461,&g_1461,&g_1461}},{{&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461}},{{&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,(void*)0}}};
static uint64_t * const **g_2189 = &g_2190[1][0][5];
static float * const **g_2288[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t *g_2490 = &g_123;
static const uint64_t * volatile g_2535 = &g_1061;/* VOLATILE GLOBAL g_2535 */
static const uint64_t * volatile *g_2534[3][1] = {{&g_2535},{&g_2535},{&g_2535}};
static const uint64_t * volatile **g_2533 = &g_2534[1][0];
static const uint64_t * volatile *** volatile g_2532 = &g_2533;/* VOLATILE GLOBAL g_2532 */
static const uint64_t * volatile *** volatile *g_2531 = &g_2532;
static int32_t * volatile *g_2579 = (void*)0;
static volatile int32_t * volatile ***g_2585 = (void*)0;
static volatile uint32_t g_2607 = 18446744073709551615UL;/* VOLATILE GLOBAL g_2607 */
static volatile uint32_t *g_2606 = &g_2607;
static volatile uint32_t **g_2605[9][10] = {{&g_2606,&g_2606,&g_2606,(void*)0,&g_2606,(void*)0,&g_2606,&g_2606,&g_2606,&g_2606},{&g_2606,&g_2606,&g_2606,(void*)0,&g_2606,&g_2606,&g_2606,&g_2606,&g_2606,&g_2606},{&g_2606,(void*)0,&g_2606,(void*)0,(void*)0,&g_2606,(void*)0,&g_2606,&g_2606,&g_2606},{(void*)0,&g_2606,&g_2606,(void*)0,&g_2606,&g_2606,(void*)0,(void*)0,&g_2606,(void*)0},{(void*)0,(void*)0,&g_2606,&g_2606,&g_2606,(void*)0,(void*)0,&g_2606,&g_2606,&g_2606},{&g_2606,&g_2606,&g_2606,&g_2606,(void*)0,&g_2606,&g_2606,&g_2606,&g_2606,&g_2606},{&g_2606,&g_2606,&g_2606,&g_2606,&g_2606,&g_2606,(void*)0,&g_2606,&g_2606,&g_2606},{&g_2606,(void*)0,&g_2606,&g_2606,&g_2606,(void*)0,(void*)0,&g_2606,(void*)0,(void*)0},{&g_2606,&g_2606,&g_2606,&g_2606,&g_2606,&g_2606,(void*)0,&g_2606,&g_2606,&g_2606}};
static int16_t **g_2678 = (void*)0;
static int8_t g_2683 = (-1L);
static int16_t g_2687 = 0x4AA4L;
static int32_t ***g_2715 = (void*)0;
static int32_t ****g_2714 = &g_2715;
static int32_t *g_2758 = &g_56;
static int32_t **g_2757[8][7] = {{(void*)0,(void*)0,(void*)0,&g_2758,(void*)0,(void*)0,&g_2758},{(void*)0,(void*)0,(void*)0,&g_2758,(void*)0,&g_2758,(void*)0},{&g_2758,&g_2758,&g_2758,(void*)0,&g_2758,&g_2758,&g_2758},{(void*)0,&g_2758,&g_2758,&g_2758,&g_2758,&g_2758,&g_2758},{&g_2758,(void*)0,(void*)0,(void*)0,&g_2758,(void*)0,&g_2758},{&g_2758,(void*)0,(void*)0,(void*)0,&g_2758,&g_2758,&g_2758},{(void*)0,(void*)0,&g_2758,&g_2758,(void*)0,(void*)0,&g_2758},{&g_2758,(void*)0,&g_2758,&g_2758,&g_2758,(void*)0,&g_2758}};
static int32_t ***g_2756[5][5][8] = {{{&g_2757[4][3],&g_2757[0][6],&g_2757[6][5],(void*)0,&g_2757[0][5],&g_2757[6][5],&g_2757[1][6],&g_2757[4][3]},{&g_2757[6][5],&g_2757[6][5],(void*)0,(void*)0,&g_2757[6][5],&g_2757[0][1],&g_2757[6][5],&g_2757[6][5]},{&g_2757[6][4],&g_2757[6][5],&g_2757[0][6],&g_2757[0][6],&g_2757[6][5],&g_2757[3][5],(void*)0,(void*)0},{&g_2757[1][6],&g_2757[5][3],&g_2757[7][2],&g_2757[6][5],&g_2757[1][4],&g_2757[5][1],&g_2757[1][1],&g_2757[6][5]},{&g_2757[6][5],(void*)0,&g_2757[0][6],&g_2757[6][5],&g_2757[6][5],(void*)0,&g_2757[2][6],&g_2757[1][6]}},{{&g_2757[0][4],&g_2757[6][5],&g_2757[6][5],(void*)0,&g_2757[6][5],&g_2757[6][5],&g_2757[0][4],&g_2757[6][5]},{&g_2757[6][5],&g_2757[0][5],&g_2757[6][5],&g_2757[6][2],&g_2757[6][5],&g_2757[0][6],(void*)0,&g_2757[6][5]},{&g_2757[6][5],&g_2757[3][4],&g_2757[6][5],(void*)0,&g_2757[6][5],&g_2757[1][0],&g_2757[6][5],(void*)0},{&g_2757[6][5],&g_2757[6][5],&g_2757[6][4],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5]},{&g_2757[0][4],(void*)0,(void*)0,&g_2757[5][6],&g_2757[6][5],&g_2757[6][5],&g_2757[6][2],&g_2757[6][5]}},{{&g_2757[6][5],&g_2757[6][5],&g_2757[5][5],&g_2757[0][5],&g_2757[1][4],(void*)0,&g_2757[6][5],&g_2757[6][5]},{&g_2757[1][6],&g_2757[1][2],&g_2757[1][6],&g_2757[6][5],&g_2757[6][5],(void*)0,&g_2757[6][4],&g_2757[5][3]},{&g_2757[6][4],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],(void*)0,&g_2757[6][5],&g_2757[0][6]},{&g_2757[6][5],(void*)0,&g_2757[6][5],(void*)0,&g_2757[0][5],&g_2757[3][4],(void*)0,&g_2757[6][5]},{&g_2757[4][3],&g_2757[6][5],&g_2757[6][5],(void*)0,(void*)0,&g_2757[6][5],&g_2757[6][5],&g_2757[0][1]}},{{(void*)0,&g_2757[3][1],&g_2757[0][6],&g_2757[6][5],&g_2757[6][5],&g_2757[0][1],&g_2757[6][6],&g_2757[0][6]},{&g_2757[6][5],&g_2757[5][3],&g_2757[3][1],&g_2757[4][3],&g_2757[6][5],&g_2757[6][5],&g_2757[4][3],&g_2757[3][1]},{&g_2757[6][5],&g_2757[6][5],&g_2757[7][2],&g_2757[4][3],&g_2757[6][5],&g_2757[0][5],&g_2757[5][6],&g_2757[1][6]},{&g_2757[6][5],&g_2757[2][6],&g_2757[1][4],(void*)0,&g_2757[6][5],(void*)0,&g_2757[0][4],&g_2757[1][6]},{&g_2757[2][6],(void*)0,&g_2757[7][3],&g_2757[4][3],&g_2757[6][5],&g_2757[0][6],(void*)0,&g_2757[3][1]}},{{(void*)0,&g_2757[5][1],&g_2757[6][5],&g_2757[4][3],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[0][6]},{(void*)0,&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[0][6],&g_2757[5][6],&g_2757[0][1]},{&g_2757[3][4],&g_2757[6][5],(void*)0,(void*)0,&g_2757[7][3],&g_2757[6][5],&g_2757[1][1],&g_2757[6][5]},{&g_2757[6][5],&g_2757[6][5],&g_2757[5][1],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],&g_2757[6][5],(void*)0},{(void*)0,&g_2757[1][4],&g_2757[6][5],&g_2757[6][5],(void*)0,&g_2757[6][2],&g_2757[6][5],&g_2757[2][6]}}};
static int32_t ****g_2755[10][5][3] = {{{(void*)0,(void*)0,&g_2756[0][4][5]},{&g_2756[2][2][1],&g_2756[0][2][0],&g_2756[4][0][5]},{&g_2756[1][3][7],&g_2756[3][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]}},{{&g_2756[0][2][0],&g_2756[4][4][6],&g_2756[4][0][5]},{&g_2756[3][1][7],&g_2756[2][3][3],&g_2756[0][4][5]},{&g_2756[4][4][6],&g_2756[3][4][2],&g_2756[4][0][5]},{&g_2756[4][3][2],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]}},{{&g_2756[3][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[2][2][1],&g_2756[4][0][5]},{&g_2756[2][3][3],&g_2756[3][1][7],&g_2756[0][4][5]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[4][0][5]},{&g_2756[1][3][7],&g_2756[3][0][6],&g_2756[1][3][7]}},{{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[3][0][6],&g_2756[4][3][2],&g_2756[1][3][7]},{&g_2756[3][4][2],&g_2756[1][3][7],&g_2756[4][0][5]},{(void*)0,(void*)0,&g_2756[0][4][5]},{&g_2756[2][2][1],&g_2756[0][2][0],&g_2756[4][0][5]}},{{&g_2756[1][3][7],&g_2756[3][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[0][2][0],&g_2756[4][4][6],&g_2756[4][0][5]},{&g_2756[3][1][7],&g_2756[2][3][3],&g_2756[0][4][5]}},{{&g_2756[4][4][6],&g_2756[3][4][2],&g_2756[4][0][5]},{&g_2756[4][3][2],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[3][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[2][2][1],&g_2756[4][0][5]}},{{&g_2756[2][3][3],&g_2756[3][1][7],&g_2756[0][4][5]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[4][0][5]},{&g_2756[1][3][7],&g_2756[3][0][6],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[3][0][6],&g_2756[4][3][2],&g_2756[1][3][7]}},{{&g_2756[3][4][2],&g_2756[1][3][7],&g_2756[4][0][5]},{(void*)0,(void*)0,&g_2756[0][4][5]},{&g_2756[2][2][1],&g_2756[0][2][0],&g_2756[4][0][5]},{&g_2756[1][3][7],&g_2756[3][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]}},{{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[0][2][0],&g_2756[4][4][6],&g_2756[4][0][5]},{&g_2756[3][1][7],&g_2756[2][3][3],&g_2756[0][4][5]},{&g_2756[4][4][6],&g_2756[3][4][2],&g_2756[4][0][5]},{&g_2756[4][3][2],&g_2756[1][3][7],&g_2756[1][3][7]}},{{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[3][3][7],&g_2756[1][3][7],&g_2756[1][3][7]},{&g_2756[1][3][7],&g_2756[2][2][1],&g_2756[4][0][5]},{&g_2756[2][3][3],&g_2756[3][1][7],&g_2756[0][4][5]},{&g_2756[1][3][7],&g_2756[1][3][7],&g_2756[4][0][5]}}};
static uint64_t **g_2768[10] = {&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461,&g_1461};
static uint64_t *** const g_2767 = &g_2768[2];
static uint64_t *** const *g_2766 = &g_2767;
static uint64_t *** const **g_2765 = &g_2766;
static uint32_t g_2828 = 4294967295UL;
static uint32_t g_2871[4] = {4UL,4UL,4UL,4UL};
static volatile uint8_t g_3050 = 1UL;/* VOLATILE GLOBAL g_3050 */
static int32_t ** volatile g_3057 = &g_585;/* VOLATILE GLOBAL g_3057 */
static int32_t * volatile *g_3116 = &g_167;
static int32_t * volatile * volatile *g_3115 = &g_3116;
static int32_t * volatile * volatile ** volatile g_3114 = &g_3115;/* VOLATILE GLOBAL g_3114 */


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static int32_t * const  func_12(float  p_13, int32_t * p_14, int32_t * p_15, uint8_t  p_16);
static int32_t * func_18(int32_t * p_19, int16_t  p_20, int32_t  p_21, uint32_t  p_22, int16_t  p_23);
static int32_t * func_24(int16_t  p_25, float  p_26);
static int16_t  func_60(int8_t  p_61, uint32_t  p_62, uint32_t  p_63, uint32_t  p_64);
static uint32_t  func_69(int32_t * p_70, uint8_t  p_71, uint16_t * p_72, uint16_t * p_73, int64_t  p_74);
static int32_t * func_75(int32_t * p_76, const float  p_77, const int32_t * p_78, int32_t * const  p_79);
static uint16_t  func_81(uint64_t  p_82, uint32_t  p_83, uint16_t * p_84);
static int8_t  func_85(uint32_t  p_86, float  p_87, uint8_t  p_88, int32_t  p_89);
static uint32_t  func_90(uint16_t * p_91, int32_t * p_92);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_9 g_17 g_3 g_49 g_167 g_123 g_1441 g_877 g_1383 g_108 g_191 g_574 g_111 g_3050 g_1450 g_706 g_2767 g_2768 g_1461 g_99 g_585 g_3057 g_1670 g_1137 g_962 g_1955 g_1345 g_2766 g_1449 g_1669 g_1146 g_1440 g_2758 g_176 g_177 g_3114 g_180 g_2765 g_2532 g_2533 g_2534 g_2535 g_1061 g_745 g_655 g_701 g_2531 g_3115 g_3116 g_1138 g_1139 g_1140
 * writes: g_9 g_29 g_49 g_191 g_123 g_585 g_3 g_706 g_379 g_1146 g_56 g_57 g_111 g_181 g_99 g_17 g_701 g_2534 g_108
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = &g_3;
    int32_t *l_4 = &g_3;
    int32_t *l_5[2][10][8] = {{{(void*)0,&g_3,&g_3,(void*)0,(void*)0,(void*)0,(void*)0,&g_3},{&g_3,&g_3,&g_3,&g_3,(void*)0,&g_3,(void*)0,&g_3},{(void*)0,&g_3,(void*)0,&g_3,&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,(void*)0,&g_3,(void*)0,&g_3,(void*)0},{(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,(void*)0},{&g_3,&g_3,&g_3,(void*)0,&g_3,&g_3,&g_3,(void*)0},{(void*)0,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3,(void*)0,(void*)0,&g_3,&g_3},{&g_3,(void*)0,&g_3,&g_3,(void*)0,(void*)0,(void*)0,&g_3},{(void*)0,&g_3,(void*)0,&g_3,(void*)0,&g_3,&g_3,(void*)0}},{{&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3,(void*)0,(void*)0,(void*)0,&g_3,&g_3},{(void*)0,&g_3,&g_3,&g_3,(void*)0,(void*)0,(void*)0,&g_3},{&g_3,&g_3,(void*)0,(void*)0,(void*)0,&g_3,&g_3,(void*)0},{&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,(void*)0,&g_3},{(void*)0,(void*)0,(void*)0,(void*)0,&g_3,&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3,&g_3,&g_3,&g_3,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_3,&g_3,&g_3,&g_3},{&g_3,(void*)0,(void*)0,(void*)0,&g_3,(void*)0,&g_3,&g_3},{(void*)0,&g_3,&g_3,(void*)0,(void*)0,&g_3,(void*)0,&g_3}}};
    int32_t l_8[3];
    uint64_t l_27 = 18446744073709551609UL;
    uint16_t *l_28 = &g_29[1][0];
    int8_t *l_3064[4][9] = {{(void*)0,(void*)0,&g_1010,&g_854,&g_700,&g_700,&g_854,&g_1010,(void*)0},{&g_960,&g_960,&g_854,&g_960,&g_960,&g_960,&g_854,&g_960,&g_960},{&g_700,&g_854,&g_1010,(void*)0,(void*)0,&g_1010,&g_854,&g_700,&g_700},{&g_379,&g_960,&g_379,&g_1335,&g_379,&g_960,&g_379,&g_1335,&g_379}};
    const uint16_t *l_3081 = (void*)0;
    const uint16_t **l_3080[3];
    const uint16_t *** const l_3079[2][3] = {{&l_3080[2],&l_3080[2],&l_3080[2]},{&l_3080[2],&l_3080[2],&l_3080[2]}};
    uint64_t **l_3082 = &g_1461;
    int64_t **** const *l_3083 = &g_1417;
    int64_t l_3084 = 0xB9A65BB81BC1A614LL;
    int16_t *l_3163 = &g_2687;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_8[i] = 0x0C8D4675L;
    for (i = 0; i < 3; i++)
        l_3080[i] = &l_3081;
    --g_9[4][1];
    (*g_3057) = func_12(g_9[3][0], &l_8[2], &g_3, ((&g_3 != (g_17 , func_18(func_24((l_27 <= ((*l_28) = 65526UL)), g_3), (*l_2), (*g_167), (*g_1441), (*g_1383)))) , (*g_574)));
    l_3084 = ((((safe_mul_func_uint16_t_u_u(((((safe_add_func_int16_t_s_s((((((safe_mul_func_uint8_t_u_u((*l_4), (g_379 = ((*g_1450) = 1L)))) > ((-1L) < (((!(safe_lshift_func_int16_t_s_s((*l_4), 13))) , (((safe_rshift_func_int8_t_s_u((*l_4), 4)) | (~(safe_mul_func_uint16_t_u_u(((18446744073709551615UL > (((0x6FL && (safe_div_func_int16_t_s_s((~(safe_add_func_uint32_t_u_u((*g_1441), (safe_unary_minus_func_int64_t_s((l_3079[1][2] == (void*)0)))))), (**g_1670)))) && (*l_4)) == (*g_1955))) , 1UL), (*l_4))))) && (*l_2))) ^ (****g_2766)))) >= (*g_574)) , l_3082) != l_3082), 0x9A4BL)) , (**g_1449)) , l_3083) == (void*)0), (***g_1669))) < (*l_2)) , (void*)0) != &l_5[1][2][3]);
    for (g_1146 = 0; (g_1146 <= 3); g_1146 += 1)
    { /* block id: 1384 */
        uint32_t l_3097 = 0x55887EF7L;
        int32_t *l_3098[6][6][5] = {{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,(void*)0,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,(void*)0},{&g_48,(void*)0,&g_48,&g_48,&g_48},{&g_48,(void*)0,&g_48,(void*)0,&g_48}},{{(void*)0,(void*)0,&g_48,(void*)0,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,(void*)0,&g_48,&g_48,&g_48},{&g_48,&g_48,(void*)0,&g_48,&g_48},{(void*)0,&g_48,&g_48,(void*)0,&g_48},{&g_48,(void*)0,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,(void*)0,(void*)0,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,(void*)0},{&g_48,&g_48,&g_48,(void*)0,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,(void*)0,(void*)0},{&g_48,&g_48,&g_48,(void*)0,&g_48},{(void*)0,(void*)0,&g_48,(void*)0,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,&g_48,(void*)0,&g_48,&g_48},{&g_48,(void*)0,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,(void*)0,&g_48,&g_48,(void*)0},{&g_48,(void*)0,(void*)0,(void*)0,&g_48},{(void*)0,(void*)0,&g_48,&g_48,&g_48}}};
        int32_t l_3099 = 0xAA353BB1L;
        uint32_t *l_3112 = (void*)0;
        uint32_t *l_3113 = &g_57;
        int16_t l_3117 = (-4L);
        const uint32_t l_3118 = 4UL;
        int64_t *l_3134 = &g_17;
        int32_t l_3135 = 1L;
        int i, j, k;
        (*g_180) = (safe_lshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_u(((*g_574) = (safe_sub_func_int32_t_s_s(((*l_4) = ((*l_4) ^ (**g_1440))), ((safe_rshift_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((**g_1670), ((safe_mul_func_uint8_t_u_u((((l_3099 = ((*g_2758) = l_3097)) , (safe_mod_func_uint32_t_u_u(0UL, ((((void*)0 != (*g_176)) , (((safe_sub_func_int32_t_s_s(((((safe_mul_func_int8_t_s_s((**g_1449), (safe_lshift_func_int8_t_s_s((((((safe_rshift_func_int8_t_s_u((safe_mod_func_int32_t_s_s(((((*l_3113) = 0UL) ^ (**g_1440)) ^ (*g_1383)), (**g_1440))), l_3099)) , 0L) != 0x3DB6L) , g_3114) == &g_3115), (**g_1449))))) , (-6L)) && l_3099) >= l_3099), (*g_1441))) <= (*g_1137)) <= l_3097)) , 9L)))) || 0xA183L), l_3097)) != l_3117))), l_3118)) && 4UL)))), 3)), l_3118));
        (**g_655) ^= (safe_mod_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((((*****g_2765) = (*****g_2765)) == (((((****g_2532) , (*l_2)) , (((((~(**g_745)) | (0x09L ^ ((**g_1670) & l_3097))) < (safe_div_func_int64_t_s_s(((safe_div_func_uint16_t_u_u((((safe_add_func_int16_t_s_s((safe_mod_func_int64_t_s_s(((*l_3134) = (safe_unary_minus_func_uint16_t_u((+0x21L)))), 0x73A50A3243FA6A3ELL)), (*g_1137))) , (**g_1670)) && (*l_2)), 6UL)) == (*l_4)), 0x945E94C0E257E39CLL))) <= l_3117) ^ 0x063CL)) & l_3099) >= (**g_745))), (-1L))), (**g_745)));
        for (g_706 = 0; (g_706 <= 3); g_706 += 1)
        { /* block id: 1396 */
            uint32_t l_3136 = 0x848885EFL;
            int16_t *l_3162 = &g_2687;
            (***g_2531) = (**g_2532);
            if ((*g_1955))
                break;
            ++l_3136;
            for (g_191 = 6; (g_191 >= 0); g_191 -= 1)
            { /* block id: 1402 */
                int32_t l_3175 = 0xE422897FL;
                int i, j;
                (*g_585) ^= ((safe_mod_func_int8_t_s_s(((safe_add_func_int16_t_s_s(0x7733L, (safe_lshift_func_uint8_t_u_s(((safe_div_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((((safe_lshift_func_uint8_t_u_u(((safe_div_func_uint32_t_u_u((safe_add_func_int16_t_s_s((!(((safe_mul_func_int16_t_s_s(((*g_1383) ^= 9L), ((safe_add_func_uint16_t_u_u(((l_3163 = l_3162) == (void*)0), ((((safe_lshift_func_uint8_t_u_u(((*g_574) = (safe_mod_func_uint64_t_u_u(((((*l_3113) = ((((((!((safe_add_func_uint64_t_u_u(l_3117, (safe_rshift_func_uint16_t_u_s((((*g_167) != 0x06C586DEL) | (((safe_unary_minus_func_uint16_t_u((***g_1669))) , ((safe_unary_minus_func_uint8_t_u((*l_4))) ^ l_3136)) <= (****g_3114))), 4)))) || (**g_1138))) != l_3135) < 254UL) != l_3136) > (*g_1450)) || (-1L))) | (*g_1441)) | l_3136), l_3175))), 4)) , &g_1383) != &l_3162) <= (**g_1670)))) && l_3097))) , 0x00EB78F0A2C3AA84LL) != 18446744073709551614UL)), (**g_1670))), 0xEA924A53L)) || (***g_1669)), 4)) ^ 0L) | (*l_4)), 2)), (-1L))) == 0UL), l_3118)))) , l_3097), 0x45L)) == (*g_1441));
            }
        }
    }
    return (*l_2);
}


/* ------------------------------------------ */
/* 
 * reads : g_3050 g_1441 g_877 g_1450 g_706 g_49 g_2767 g_2768 g_1461 g_99 g_585
 * writes: g_49 g_585 g_3
 */
static int32_t * const  func_12(float  p_13, int32_t * p_14, int32_t * p_15, uint8_t  p_16)
{ /* block id: 1374 */
    int32_t ***l_3049 = &g_2757[6][5];
    int32_t l_3055 = 0L;
    int32_t **l_3056 = &g_585;
    (*l_3056) = func_24((safe_div_func_uint32_t_u_u(p_16, (safe_mod_func_int64_t_s_s((safe_rshift_func_int8_t_s_u(p_16, (p_16 && (l_3049 == (void*)0)))), g_3050)))), ((safe_mod_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((0xE965270BL || ((0x7E4EC12FDFB7BA82LL != (((*g_1441) , 0x34BD2FC5819B6158LL) && l_3055)) == l_3055)), (*g_1450))), 0x52L)) , 0x0.Cp+1));
    (*p_15) = (0x81A24A7510449334LL < (p_16 && (***g_2767)));
    return (*l_3056);
}


/* ------------------------------------------ */
/* 
 * reads : g_191 g_1383 g_108 g_167 g_123
 * writes: g_191 g_123
 */
static int32_t * func_18(int32_t * p_19, int16_t  p_20, int32_t  p_21, uint32_t  p_22, int16_t  p_23)
{ /* block id: 927 */
    int8_t l_1984 = (-1L);
    int32_t l_1997 = 0L;
    uint64_t **l_2004 = (void*)0;
    int64_t l_2021 = 0xFE17C49418B31EDBLL;
    float **l_2038 = &g_1769;
    int64_t ** const **l_2064 = (void*)0;
    int64_t ** const ***l_2063 = &l_2064;
    int32_t *l_2082[5][4][6] = {{{&g_3,&g_1930[3][7][1],&g_123,&g_1930[3][7][1],&g_3,&g_3},{&g_3,&g_701,&g_701,&g_1930[0][3][3],&g_1345[2],&g_3},{(void*)0,&g_123,&g_1930[4][6][3],(void*)0,&g_701,&g_3},{&g_1930[4][5][0],&g_1345[2],&g_701,&g_701,&g_1930[0][3][3],&g_701}},{{&g_701,(void*)0,&g_123,&g_701,&g_1930[0][3][3],&g_1930[1][4][0]},{&g_1930[4][6][3],&g_1345[2],&l_1997,&g_701,&g_701,&l_1997},{&g_123,&g_123,&g_3,&g_701,&g_1345[2],&g_1930[3][7][1]},{&g_1930[4][6][3],(void*)0,&g_1930[1][4][0],&g_701,(void*)0,&g_3}},{{&g_701,&g_1930[4][6][3],&g_1930[1][4][0],&g_701,&g_123,&g_1930[3][7][1]},{&g_1930[4][5][0],&g_701,&g_3,(void*)0,&g_701,&l_1997},{(void*)0,&g_701,&l_1997,&g_1930[0][3][3],&g_123,&g_1930[1][4][0]},{(void*)0,&g_1930[4][6][3],&g_123,&g_1930[4][6][3],(void*)0,&g_701}},{{(void*)0,(void*)0,&g_701,&g_1930[0][3][3],&g_1345[2],&g_3},{(void*)0,&g_123,&g_1930[4][6][3],(void*)0,&g_701,&g_3},{&g_1930[4][5][0],&g_1345[2],&g_701,&g_701,&g_1930[0][3][3],&g_701},{&g_701,(void*)0,&g_123,&g_701,&g_1930[0][3][3],&g_1930[1][4][0]}},{{&g_1930[4][6][3],&g_1345[2],&l_1997,&g_701,&g_701,&l_1997},{&g_123,&g_123,&g_3,&g_701,&g_1345[2],&g_1930[3][7][1]},{&g_1930[4][6][3],(void*)0,&g_1930[1][4][0],&g_701,(void*)0,&g_3},{&g_701,&g_1930[4][6][3],&g_1930[1][4][0],&g_701,&g_123,&g_1930[3][7][1]}}};
    int32_t *l_2085 = &g_1345[2];
    uint16_t **l_2160 = &g_1137;
    int8_t * const *l_2170[10] = {&g_1450,&g_1450,&g_1450,&g_1450,&g_1450,&g_1450,&g_1450,&g_1450,&g_1450,&g_1450};
    int8_t * const **l_2169[1][5] = {{&l_2170[9],&l_2170[9],&l_2170[9],&l_2170[9],&l_2170[9]}};
    uint8_t l_2236 = 1UL;
    float l_2239 = 0x1.B900D8p-6;
    int8_t **l_2299 = &g_1450;
    uint8_t l_2334 = 0x31L;
    int64_t *l_2369 = &g_32;
    int64_t **l_2368 = &l_2369;
    uint32_t **l_2370 = (void*)0;
    uint32_t l_2372 = 0x53F57CB0L;
    uint32_t l_2381 = 9UL;
    uint8_t l_2411 = 1UL;
    const uint8_t *l_2487 = &l_2411;
    const uint8_t **l_2486 = &l_2487;
    uint16_t l_2514 = 65531UL;
    uint8_t l_2560 = 0xEAL;
    int16_t l_2587[7][9] = {{(-1L),(-1L),8L,0xD3DBL,0x1829L,1L,(-1L),0x1829L,0xF1D1L},{0x407FL,0L,0x6638L,0x1829L,0x1829L,0x6638L,0L,0x407FL,8L},{0x407FL,0x1829L,8L,0x407FL,0L,0x6638L,0x1829L,0x1829L,0x6638L},{(-1L),0x1829L,0xF1D1L,0x1829L,(-1L),1L,0x1829L,0xD3DBL,8L},{0x1829L,0L,0xF1D1L,0xD3DBL,0L,8L,0L,0xD3DBL,0xF1D1L},{(-1L),(-1L),8L,0xD3DBL,0x1829L,1L,(-1L),0x1829L,0xF1D1L},{0x407FL,0L,0x6638L,0x1829L,0x1829L,0x6638L,0L,0x407FL,8L}};
    int8_t l_2727 = 0xBBL;
    uint32_t *l_2746 = &g_857;
    uint32_t **l_2745 = &l_2746;
    uint64_t l_2778 = 0x92314B838E247480LL;
    float *l_2862 = &g_112[2];
    int32_t *l_2866 = (void*)0;
    uint32_t l_2868 = 1UL;
    const int32_t l_2888 = 4L;
    uint32_t l_2909 = 0xDBC194D6L;
    int16_t l_2988 = 3L;
    int32_t *l_3040 = &g_3;
    int i, j, k;
    for (g_191 = 0; (g_191 == (-11)); --g_191)
    { /* block id: 930 */
        int64_t l_1983 = 0xC3EE4E21AAA4641DLL;
        int32_t l_1998[1];
        uint64_t **l_2002 = &g_1461;
        int8_t * const l_2022 = &g_45;
        uint32_t **l_2094 = (void*)0;
        uint8_t l_2133 = 255UL;
        uint16_t * const *l_2155[10][7][3] = {{{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137}},{{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137}},{{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137}},{{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137}},{{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137}},{{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137}},{{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137}},{{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137}},{{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137},{&g_1137,&g_1137,&g_1137},{(void*)0,(void*)0,&g_1137}},{{&g_1137,&g_1137,&g_1137},{&g_1137,&g_1137,(void*)0},{&g_1137,&g_1137,&g_1137},{&g_1137,&g_1137,(void*)0},{&g_1137,&g_1137,&g_1137},{&g_1137,&g_1137,(void*)0},{&g_1137,&g_1137,&g_1137}}};
        int32_t l_2178 = 0x1948E2AAL;
        int32_t ***l_2195 = &g_1864;
        int64_t **** const l_2196 = (void*)0;
        const float l_2260 = (-0x5.7p+1);
        uint8_t **l_2269 = &g_574;
        int8_t l_2586[10][3][2] = {{{0xD5L,0xAFL},{0x7BL,0xAFL},{0xD5L,7L}},{{0xC6L,0L},{0xFFL,0xC6L},{0xFDL,1L}},{{0L,0x2AL},{0xFFL,(-6L)},{0x2AL,7L}},{{0x56L,0x56L},{0x7BL,0xD5L},{0xAFL,7L}},{{0x0DL,0xFDL},{0xFFL,0x0DL},{(-6L),1L}},{{(-6L),0x0DL},{0xFFL,0xFDL},{0x0DL,7L}},{{0xAFL,0xD5L},{0x7BL,0x56L},{0x56L,7L}},{{0x2AL,(-6L)},{0xFFL,0x2AL},{0L,1L}},{{0xFDL,0xC6L},{0xFFL,0L},{0xC6L,7L}},{{0xD5L,0xAFL},{0x7BL,0xAFL},{0xD5L,7L}}};
        float ***l_2609[9];
        int32_t l_2636 = (-1L);
        uint16_t l_2642 = 65528UL;
        int16_t **l_2677 = &g_1383;
        int16_t l_2690[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        uint16_t l_2769 = 0xE245L;
        int64_t * const ***l_2773 = (void*)0;
        const int16_t l_2827 = 0xE01AL;
        int64_t l_2857 = 0x6690C44D8147ED2ALL;
        int32_t l_3035[2][9] = {{0xBECD53A4L,0L,0x3ED4A91EL,0x19EED757L,0x3ED4A91EL,0L,0xBECD53A4L,0xBECD53A4L,0L},{0x19EED757L,0L,0xE665230BL,0L,0x19EED757L,0xC5C8972EL,0xC5C8972EL,0x19EED757L,0L}};
        uint32_t l_3036 = 0xBB6F4670L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1998[i] = 0L;
        for (i = 0; i < 9; i++)
            l_2609[i] = &l_2038;
    }
    l_3040 = &p_21;
    (*g_167) &= ((safe_mod_func_int16_t_s_s(((p_23 && 1UL) , (*g_1383)), 1L)) == p_23);
    return &g_1345[2];
}


/* ------------------------------------------ */
/* 
 * reads : g_49
 * writes: g_49
 */
static int32_t * func_24(int16_t  p_25, float  p_26)
{ /* block id: 3 */
    int32_t *l_30 = &g_3;
    int32_t *l_31 = &g_3;
    int32_t *l_33 = (void*)0;
    int32_t *l_34 = &g_3;
    int32_t *l_35 = &g_3;
    int32_t l_36 = (-9L);
    int32_t *l_37 = &g_3;
    int32_t *l_38 = &g_3;
    int32_t *l_39 = &g_3;
    int32_t *l_40 = &l_36;
    int32_t *l_41 = &g_3;
    int32_t *l_42 = &g_3;
    int32_t *l_43 = (void*)0;
    int32_t *l_44[2][10] = {{&g_3,(void*)0,(void*)0,(void*)0,&g_3,(void*)0,&g_3,&g_3,(void*)0,&g_3},{&g_3,&l_36,&l_36,&g_3,&g_3,(void*)0,&g_3,(void*)0,&g_3,&g_3}};
    int64_t **l_1171 = (void*)0;
    int64_t **l_1179 = (void*)0;
    uint32_t l_1201 = 9UL;
    int32_t l_1316 = 1L;
    uint8_t l_1393 = 1UL;
    uint64_t **l_1457 = (void*)0;
    uint64_t **l_1458 = (void*)0;
    uint64_t *l_1460 = &g_508;
    uint64_t **l_1459[1];
    int8_t l_1464 = 0x61L;
    uint16_t *l_1475 = &g_559;
    uint16_t *l_1478[7] = {&g_529[3],&g_29[1][0],&g_529[3],&g_529[3],&g_29[1][0],&g_529[3],&g_529[3]};
    int16_t **l_1530 = &g_1383;
    uint32_t l_1566 = 1UL;
    uint8_t l_1610 = 1UL;
    int32_t **l_1650 = &l_41;
    int32_t *** const l_1649 = &l_1650;
    uint64_t l_1684 = 1UL;
    int64_t ***l_1708[2][6] = {{(void*)0,&l_1171,&g_1178,&l_1171,(void*)0,(void*)0},{&l_1179,&l_1171,&l_1171,&l_1179,(void*)0,&l_1179}};
    uint32_t l_1711 = 4UL;
    int64_t l_1715[7][5][7] = {{{0xA158FC2FD8CFE86FLL,0xA158FC2FD8CFE86FLL,0x5C9B25D8A2BA3F92LL,0xA158FC2FD8CFE86FLL,0xA158FC2FD8CFE86FLL,0x5C9B25D8A2BA3F92LL,0xA158FC2FD8CFE86FLL},{0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L),0x7A1E770C99C55D4ELL,(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL},{(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L)},{0xD3D4D61F439B68C5LL,0x7A1E770C99C55D4ELL,0xCE2D6C78C04884D2LL,0x7A1E770C99C55D4ELL,0xD3D4D61F439B68C5LL,(-4L),0xD3D4D61F439B68C5LL},{0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL}},{{(-7L),0x7A1E770C99C55D4ELL,(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L)},{0xA158FC2FD8CFE86FLL,0xA158FC2FD8CFE86FLL,0x5C9B25D8A2BA3F92LL,0xA158FC2FD8CFE86FLL,(-10L),0xA158FC2FD8CFE86FLL,(-10L)},{(-7L),(-4L),0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L)},{0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL},{(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L),0x7A1E770C99C55D4ELL,(-7L)}},{{(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L)},{0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L),(-4L),0xCE2D6C78C04884D2LL},{(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L)},{(-7L),(-4L),0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L)},{0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL}},{{(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L),0x7A1E770C99C55D4ELL,(-7L)},{(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L)},{0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L),(-4L),0xCE2D6C78C04884D2LL},{(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L)},{(-7L),(-4L),0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L)}},{{0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL},{(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L),0x7A1E770C99C55D4ELL,(-7L)},{(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L)},{0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L),(-4L),0xCE2D6C78C04884D2LL},{(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L)}},{{(-7L),(-4L),0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L)},{0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL},{(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L),0x7A1E770C99C55D4ELL,(-7L)},{(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L)},{0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L),(-4L),0xCE2D6C78C04884D2LL}},{{(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L),(-10L),0xA158FC2FD8CFE86FLL,(-10L)},{(-7L),(-4L),0xCE2D6C78C04884D2LL,0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL,(-4L),(-7L)},{0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL,(-10L),0x5C9B25D8A2BA3F92LL,0x5C9B25D8A2BA3F92LL},{(-7L),0x862113DCFB6C30ABLL,0xD3D4D61F439B68C5LL,0x862113DCFB6C30ABLL,(-7L),0x862113DCFB6C30ABLL,0xCE2D6C78C04884D2LL},{0x5C9B25D8A2BA3F92LL,0xA158FC2FD8CFE86FLL,0xA158FC2FD8CFE86FLL,0x5C9B25D8A2BA3F92LL,0xA158FC2FD8CFE86FLL,0xA158FC2FD8CFE86FLL,0x5C9B25D8A2BA3F92LL}}};
    uint64_t l_1733 = 1UL;
    uint8_t **l_1883[7][3] = {{&g_574,(void*)0,&g_574},{&g_574,&g_574,&g_574},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_574,&g_574},{(void*)0,(void*)0,&g_574},{(void*)0,&g_574,(void*)0},{(void*)0,(void*)0,&g_574}};
    uint32_t l_1959 = 2UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1459[i] = &l_1460;
    g_49++;
    for (l_36 = 1; (l_36 >= 0); l_36 -= 1)
    { /* block id: 7 */
        int64_t l_52 = 0x35F447FE6D7BFF02LL;
        int32_t l_53 = (-4L);
        int32_t l_751 = 0x8674C67CL;
        uint16_t *l_1150 = &g_646;
        uint8_t * const l_1202 = (void*)0;
        int32_t l_1248[5][10] = {{0xEE731F28L,0x702192E5L,0x332F6751L,0xAD478F0FL,0xAD478F0FL,0x332F6751L,0x702192E5L,0xEE731F28L,0x655580CFL,0x276C0740L},{0x702192E5L,3L,0xCB4CD1EFL,(-1L),(-6L),0L,1L,0xAD478F0FL,0x735B5652L,0xAD478F0FL},{(-1L),0xAD478F0FL,0xCB4CD1EFL,(-6L),0xCB4CD1EFL,0xAD478F0FL,(-1L),0xEE731F28L,0x332F6751L,1L},{(-1L),(-8L),0x332F6751L,0x276C0740L,1L,0xCB4CD1EFL,0xEE731F28L,3L,3L,0xEE731F28L},{0x735B5652L,(-8L),0xEE731F28L,0xEE731F28L,(-8L),0x735B5652L,(-1L),1L,(-1L),0xCB4CD1EFL}};
        uint8_t l_1331 = 251UL;
        int32_t l_1352 = 0xF8A0B5FDL;
        int32_t l_1353 = 0L;
        int32_t l_1354[7] = {0x2AD86C0DL,1L,1L,0x2AD86C0DL,1L,1L,0x2AD86C0DL};
        int32_t l_1359 = 0xE94BD317L;
        uint8_t l_1363 = 248UL;
        uint32_t l_1407 = 0x1484DECFL;
        int64_t l_1444 = 0x1DB89ECD829A71DBLL;
        int i, j;
    }
    return &g_701;
}


/* ------------------------------------------ */
/* 
 * reads : g_280 g_752 g_112 g_708 g_99 g_123 g_29 g_379 g_700 g_745 g_574 g_3 g_701 g_48 g_529 g_57 g_108 g_111 g_857 g_56 g_644 g_560 g_877 g_255 g_45 g_854 g_191 g_519 g_962 g_702 g_960 g_643 g_508 g_49 g_17 g_167 g_1011 g_707 g_436 g_521 g_1061 g_1010 g_559 g_651 g_1138 g_585
 * writes: g_280 g_112 g_123 g_99 g_700 g_111 g_701 g_379 g_529 g_585 g_857 g_56 g_46 g_48 g_877 g_644 g_646 g_255 g_57 g_519 g_962 g_708 g_702 g_559 g_29 g_17 g_167 g_1011 g_1061 g_1010 g_191 g_108 g_1137
 */
static int16_t  func_60(int8_t  p_61, uint32_t  p_62, uint32_t  p_63, uint32_t  p_64)
{ /* block id: 319 */
    uint16_t l_763[10][9][2] = {{{0x991DL,0xC706L},{0UL,0xE1BBL},{0UL,0x1982L},{0UL,65535UL},{65535UL,65535UL},{0UL,0x1982L},{0UL,0xE1BBL},{0UL,0xC706L},{0x991DL,0UL}},{{0x9205L,2UL},{0xBBA8L,0UL},{65531UL,1UL},{0xC706L,65535UL},{65530UL,65531UL},{0xAA31L,0xAA31L},{6UL,65531UL},{2UL,65530UL},{65535UL,0x7F56L}},{{0UL,65535UL},{0UL,0x4953L},{0UL,65535UL},{0UL,0x7F56L},{65535UL,65530UL},{2UL,65531UL},{6UL,0xAA31L},{0xAA31L,65531UL},{65530UL,65535UL}},{{0xC706L,1UL},{65531UL,0UL},{0xBBA8L,2UL},{0x9205L,0UL},{0x991DL,0xC706L},{0UL,0xE1BBL},{0UL,0x1982L},{0UL,65535UL},{65535UL,65535UL}},{{0UL,0x1982L},{0UL,0xE1BBL},{0UL,0xC706L},{0x991DL,0UL},{0x9205L,2UL},{0xBBA8L,0UL},{65531UL,1UL},{0xC706L,65535UL},{65530UL,65531UL}},{{0xAA31L,0xAA31L},{6UL,65531UL},{2UL,65530UL},{65535UL,0x7F56L},{0UL,65535UL},{0UL,0x4953L},{0UL,65535UL},{0UL,0x991DL},{0x4953L,65535UL}},{{65530UL,6UL},{0xE1BBL,1UL},{1UL,0x7F56L},{65531UL,0xEA94L},{0UL,65535UL},{0x7F56L,65535UL},{0UL,65530UL},{65535UL,0UL},{1UL,0UL}},{{0UL,0x4640L},{0UL,65530UL},{65535UL,2UL},{0xEA94L,2UL},{65535UL,65530UL},{0UL,0x4640L},{0UL,0UL},{1UL,0UL},{65535UL,65530UL}},{{0UL,65535UL},{0x7F56L,65535UL},{0UL,0xEA94L},{65531UL,0x7F56L},{1UL,1UL},{0xE1BBL,6UL},{65530UL,65535UL},{0x4953L,0x991DL},{0UL,0x4953L}},{{0xAA31L,65534UL},{0xAA31L,0x4953L},{0UL,0x991DL},{0x4953L,65535UL},{65530UL,6UL},{0xE1BBL,1UL},{1UL,0x7F56L},{65531UL,0xEA94L},{0UL,65535UL}}};
    uint64_t *l_765 = &g_508;
    uint64_t **l_764 = &l_765;
    int64_t *l_769 = &g_644;
    int64_t * const * const l_768[2] = {&l_769,&l_769};
    int64_t * const * const * const l_767 = &l_768[1];
    int32_t **l_770 = (void*)0;
    uint8_t **l_816 = &g_574;
    int32_t l_848 = 3L;
    int32_t l_849 = 0L;
    int32_t l_850 = 5L;
    int32_t l_851 = 0xDE5A5934L;
    int32_t l_856[6];
    int32_t *l_872 = &l_849;
    int32_t l_901 = 0x0677D209L;
    uint32_t l_1082 = 0x6E409A25L;
    int8_t l_1104 = 0xF8L;
    uint16_t *l_1121[5][5] = {{&l_763[1][5][1],&g_529[1],&l_763[1][5][1],&g_529[1],&l_763[1][5][1]},{&l_763[9][6][1],&l_763[9][6][1],&l_763[9][6][1],&l_763[9][6][1],&l_763[9][6][1]},{&l_763[1][5][1],&g_529[1],&l_763[1][5][1],&g_529[1],&l_763[1][5][1]},{&l_763[9][6][1],&l_763[9][6][1],&l_763[9][6][1],&l_763[9][6][1],&l_763[9][6][1]},{&l_763[1][5][1],&g_529[1],&l_763[1][5][1],&g_529[1],&l_763[1][5][1]}};
    uint16_t **l_1120[6][3][6] = {{{&l_1121[1][4],&l_1121[3][4],&l_1121[3][2],&l_1121[1][4],&l_1121[1][2],&l_1121[1][4]},{&l_1121[1][4],(void*)0,&l_1121[1][4],&l_1121[1][4],(void*)0,&l_1121[1][4]},{&l_1121[3][2],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[2][4],(void*)0}},{{&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[3][4],&l_1121[1][4]},{&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[3][2]},{&l_1121[3][2],&l_1121[2][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4]}},{{&l_1121[1][4],&l_1121[1][2],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4]},{&l_1121[1][4],&l_1121[1][4],(void*)0,&l_1121[1][4],&l_1121[1][4],&l_1121[1][4]},{&l_1121[3][2],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4]}},{{&l_1121[1][4],&l_1121[3][4],&l_1121[3][2],&l_1121[1][4],&l_1121[1][2],&l_1121[1][4]},{&l_1121[1][4],(void*)0,&l_1121[1][4],&l_1121[1][4],(void*)0,&l_1121[1][4]},{&l_1121[3][2],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[2][4],(void*)0}},{{&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[3][4],&l_1121[1][4]},{&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[3][2]},{&l_1121[3][2],&l_1121[2][4],&l_1121[1][4],&l_1121[1][4],&l_1121[3][2],&l_1121[1][4]}},{{&l_1121[1][4],&l_1121[1][4],&l_1121[1][4],&l_1121[1][1],&l_1121[1][4],&l_1121[2][3]},{&l_1121[1][4],&l_1121[1][4],&l_1121[3][2],&l_1121[1][4],&l_1121[1][4],&l_1121[3][4]},{&l_1121[1][4],&l_1121[3][2],&l_1121[1][0],&l_1121[1][4],&l_1121[1][4],&l_1121[1][1]}}};
    uint16_t *l_1122 = &g_529[1];
    int32_t *l_1123 = &l_856[3];
    int32_t **l_1124[7][6] = {{&l_1123,(void*)0,&l_1123,(void*)0,&l_1123,&l_1123},{&g_585,(void*)0,(void*)0,&g_585,&l_872,&g_585},{&g_585,&l_872,&g_585,(void*)0,(void*)0,&g_585},{&l_1123,&l_1123,(void*)0,&l_1123,(void*)0,&l_1123},{(void*)0,&l_872,&l_1123,&l_1123,&l_872,(void*)0},{&l_1123,(void*)0,&l_1123,(void*)0,&l_1123,&l_1123},{&g_585,(void*)0,(void*)0,&g_585,&l_872,&g_585}};
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_856[i] = 0L;
    for (g_280 = 0; (g_280 <= 7); g_280 += 1)
    { /* block id: 322 */
        volatile int64_t *l_761 = &g_762;
        volatile int64_t * volatile *l_756[8] = {&l_761,&g_757,&l_761,&l_761,&g_757,&l_761,&l_761,&g_757};
        volatile int64_t * volatile **l_755 = &l_756[0];
        int i;
        (*l_755) = g_752;
        if (p_61)
            break;
        g_112[g_280] = g_112[g_280];
    }
    if (((p_63 ^= l_763[1][5][1]) >= (((l_763[8][2][0] , ((*l_764) = &g_99)) == &g_49) <= p_61)))
    { /* block id: 329 */
        int32_t ***l_771 = &l_770;
        int32_t *l_772 = &g_123;
        const int64_t *l_792 = &g_32;
        uint8_t * const *l_818 = (void*)0;
        int32_t **l_847 = &g_585;
        int32_t *l_852 = &g_123;
        int32_t *l_853[4][10][3] = {{{&l_848,&g_3,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_123,&g_701},{&l_851,(void*)0,&g_3},{&l_851,(void*)0,&g_701},{(void*)0,&l_848,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_849,&l_851,(void*)0},{&g_701,(void*)0,&g_701}},{{&g_3,&g_701,(void*)0},{&l_851,(void*)0,(void*)0},{(void*)0,&l_849,(void*)0},{(void*)0,&l_848,(void*)0},{(void*)0,&g_701,&g_3},{&l_851,(void*)0,&g_123},{&g_3,&l_848,&g_3},{&g_701,(void*)0,(void*)0},{&l_849,&l_850,&l_848},{(void*)0,&l_851,&g_701}},{{(void*)0,(void*)0,&l_850},{&l_851,(void*)0,(void*)0},{&l_851,(void*)0,&g_701},{(void*)0,&l_851,&g_701},{(void*)0,&l_850,&g_3},{(void*)0,(void*)0,(void*)0},{&l_848,&l_848,&l_849},{&g_123,(void*)0,&l_850},{&l_849,&g_701,&g_3},{&l_849,&l_848,&g_701}},{{&g_3,&l_849,&g_3},{&g_3,(void*)0,&l_850},{&l_848,&g_701,&l_849},{(void*)0,(void*)0,(void*)0},{&g_3,&l_851,&g_3},{&g_701,(void*)0,&g_701},{&l_850,&l_848,&g_701},{&g_701,(void*)0,(void*)0},{&l_849,(void*)0,&l_850},{&g_701,&g_123,&g_701}}};
        int32_t l_855 = 1L;
        int i, j, k;
        if (((((**l_764) ^= ((safe_unary_minus_func_int32_t_s(((*l_772) = (((void*)0 != l_767) || (((*l_771) = l_770) == (l_763[5][2][1] , &g_180)))))) & ((safe_div_func_int32_t_s_s(l_763[1][2][1], l_763[6][0][1])) || (safe_sub_func_uint32_t_u_u(g_708, ((l_763[8][1][0] && 0x62L) != p_62)))))) | p_61) | 0UL))
        { /* block id: 333 */
            int8_t *l_785 = &g_700;
            uint32_t *l_793 = (void*)0;
            int32_t l_794 = 1L;
            int32_t l_795[3][3][3] = {{{(-3L),1L,0L},{0L,(-3L),1L},{(-3L),0L,0L}},{{1L,0L,0x2CA88871L},{4L,(-3L),0xE6AD17FCL},{1L,1L,0xE6AD17FCL}},{{(-3L),4L,0x2CA88871L},{0L,1L,0L},{0L,(-3L),1L}}};
            int32_t *l_796 = &g_701;
            int i, j, k;
            (*l_796) ^= (0x5BC4CB3EL < (((safe_div_func_uint64_t_u_u(((safe_sub_func_uint64_t_u_u((*l_772), ((0xBDL && (safe_add_func_int16_t_s_s(0xF684L, (((g_29[6][0] , (l_794 = (((-2L) == (safe_mul_func_int8_t_s_s(((*l_785) |= g_379), (safe_sub_func_uint8_t_u_u(((((**g_745) = ((safe_lshift_func_uint16_t_u_s((((g_280 ^= (safe_mul_func_int16_t_s_s(((void*)0 == l_792), 0xD921L))) | p_64) != l_794), p_61)) >= l_794)) , 0x19EED8B2L) , 0xC8L), (*l_772)))))) == 0x63DB55ADL))) & 0xBAL) != l_795[2][0][2])))) && 0x3FA4A4AD375B8894LL))) <= 0UL), (*l_772))) >= g_3) ^ p_61));
            for (g_379 = (-2); (g_379 < 0); g_379 = safe_add_func_int16_t_s_s(g_379, 1))
            { /* block id: 341 */
                float l_803 = 0xB.90A7F9p+36;
                int32_t l_812 = 1L;
                (*l_796) = (safe_sub_func_uint16_t_u_u(((((safe_add_func_uint16_t_u_u(((4294967295UL & 0UL) ^ g_48), (p_62 || g_3))) , l_771) != (((safe_mod_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_div_func_uint32_t_u_u((((0xA1A0DA107E92F90ELL < (safe_mod_func_int32_t_s_s(p_63, ((255UL == p_64) & p_64)))) >= l_812) != p_64), 4294967288UL)), p_62)), 0xC521A06EL)) , g_3) , &g_177)) > 0x6A42L), 0x9DEBL));
                if (p_61)
                    break;
                (*l_772) |= p_63;
                if (p_62)
                    break;
            }
        }
        else
        { /* block id: 347 */
            int8_t l_817 = (-10L);
            int16_t *l_819[3][8][8] = {{{&g_707,&g_707,&g_707,&g_191,&g_191,&g_108[0],&g_191,&g_707},{&g_108[5],&g_108[6],&g_707,&g_191,&g_707,&g_191,&g_108[6],&g_108[4]},{&g_108[1],&g_191,&g_108[0],&g_108[5],(void*)0,&g_707,&g_108[2],&g_108[3]},{&g_707,&g_108[3],&g_108[4],(void*)0,&g_108[4],&g_108[4],&g_191,&g_191},{&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],(void*)0,&g_108[4]},{&g_108[2],&g_707,&g_108[0],&g_191,&g_108[0],&g_707,&g_707,&g_108[4]},{&g_108[2],&g_707,(void*)0,&g_191,&g_108[4],&g_108[0],&g_108[6],&g_108[4]},{&g_707,&g_108[4],&g_108[6],&g_108[4],&g_191,&g_108[4],&g_108[4],&g_191}},{{&g_707,&g_191,&g_707,(void*)0,&g_191,&g_191,&g_108[6],&g_108[3]},{&g_108[0],&g_191,&g_108[4],&g_108[5],&g_707,&g_108[3],(void*)0,&g_108[4]},{&g_108[2],&g_707,&g_707,&g_191,&g_108[6],&g_707,&g_191,&g_707},{&g_108[4],&g_108[4],&g_108[5],&g_191,&g_108[2],&g_108[0],&g_707,(void*)0},{&g_108[4],&g_108[1],(void*)0,&g_108[4],(void*)0,&g_108[1],&g_108[4],&g_108[6]},{&g_707,&g_191,&g_707,&g_707,&g_191,&g_108[0],&g_108[4],&g_191},{&g_191,&g_108[4],&g_707,(void*)0,&g_191,&g_191,(void*)0,&g_108[4]},{&g_707,&g_707,(void*)0,&g_191,(void*)0,&g_707,&g_707,&g_191}},{{&g_108[4],&g_108[3],&g_108[4],&g_108[0],&g_108[2],&g_191,&g_191,&g_191},{&g_108[4],&g_108[4],&g_191,&g_108[4],&g_108[6],&g_108[4],&g_707,(void*)0},{&g_108[2],&g_191,&g_108[4],&g_108[4],&g_707,&g_707,&g_108[0],&g_191},{&g_108[0],(void*)0,(void*)0,&g_191,&g_191,&g_191,(void*)0,&g_108[4]},{&g_707,&g_191,&g_707,&g_191,&g_707,(void*)0,&g_707,&g_191},{&g_191,&g_191,&g_707,&g_707,&g_191,&g_707,&g_707,&g_108[4]},{&g_191,&g_707,&g_108[2],(void*)0,&g_707,&g_707,&g_191,&g_707},{(void*)0,&g_191,&g_707,&g_108[4],&g_108[5],(void*)0,&g_108[0],&g_707}}};
            int32_t l_820[7];
            uint16_t *l_825 = (void*)0;
            uint16_t *l_826 = &l_763[7][3][0];
            uint8_t *l_833 = &g_115;
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_820[i] = 0x0B53A6F1L;
            if ((((safe_mul_func_int8_t_s_s((p_61 = (safe_unary_minus_func_int16_t_s((l_820[4] &= (l_816 == (l_817 , l_818)))))), (4294967291UL && ((safe_mul_func_int16_t_s_s((safe_lshift_func_int16_t_s_u((p_63 , g_529[7]), 4)), p_63)) & ((*l_772) == ((++(*l_826)) < (((safe_rshift_func_uint8_t_u_u((((safe_div_func_uint16_t_u_u((((0UL != l_817) , (void*)0) == (void*)0), 0xC610L)) >= 3L) <= l_817), 3)) != 0xCCADL) && g_3))))))) , 0x73DBCBB536695154LL) > p_63))
            { /* block id: 351 */
                (*l_772) &= ((void*)0 != l_833);
            }
            else
            { /* block id: 353 */
                for (g_379 = 5; (g_379 >= 2); g_379 -= 1)
                { /* block id: 356 */
                    int64_t l_834 = (-1L);
                    l_834 = 1L;
                }
            }
            return (*l_772);
        }
        (*l_772) ^= 0x02B16352L;
        (*l_847) = (((g_57 <= (safe_sub_func_float_f_f((safe_add_func_float_f_f((safe_div_func_float_f_f(((((-0x1.3p-1) != ((((safe_mul_func_int8_t_s_s(((((void*)0 != (*g_745)) & 65533UL) != ((((safe_lshift_func_int8_t_s_u((((g_529[0] &= g_108[4]) && ((safe_mod_func_int64_t_s_s(g_57, (0xC0L | ((*g_574) < p_63)))) <= 0x58L)) != 0xDDEFL), 7)) , (*l_764)) == l_792) <= g_700)), (**g_745))) > p_63) , 1L) , (-0x7.Dp-1))) >= 0x0.Dp-1) == (*l_772)), 0x1.5p+1)), p_61)), g_700))) < 0x4.7p-1) , &g_123);
        ++g_857;
    }
    else
    { /* block id: 366 */
        int32_t *l_862 = &l_851;
        int32_t * const l_871 = (void*)0;
        int32_t l_875[6][2] = {{0xF47C3595L,0x482F9468L},{0xF47C3595L,0xF47C3595L},{0x482F9468L,0xF47C3595L},{0xF47C3595L,0x482F9468L},{0xF47C3595L,0xF47C3595L},{0x482F9468L,0xF47C3595L}};
        int8_t l_987 = 7L;
        uint64_t *l_991 = &g_508;
        int i, j;
        for (g_56 = 22; (g_56 >= (-21)); g_56 = safe_sub_func_int32_t_s_s(g_56, 5))
        { /* block id: 369 */
            float *l_863 = &g_46;
            float *l_869 = &g_112[2];
            int32_t l_870 = 8L;
            int32_t *l_873 = &l_850;
            int32_t *l_874[1];
            uint8_t **l_893 = &g_574;
            uint8_t ***l_894 = &l_893;
            int32_t **l_897[6];
            uint8_t **l_900 = &g_574;
            int32_t l_902 = (-1L);
            uint16_t *l_903[7][9] = {{&l_763[8][8][0],&g_529[6],&l_763[8][8][0],&g_559,&l_763[8][8][0],&g_529[6],&l_763[8][8][0],&g_559,&l_763[8][8][0]},{&g_529[1],&g_646,&l_763[4][2][1],&l_763[4][2][1],&g_646,&g_529[1],&g_29[1][0],&g_29[1][0],&g_529[1]},{&g_529[1],&g_559,(void*)0,&g_559,&g_529[1],&g_559,(void*)0,&g_559,&g_529[1]},{&g_646,&l_763[4][2][1],&l_763[4][2][1],&g_646,&g_529[1],&g_29[1][0],&g_29[1][0],&g_529[1],&g_646},{&l_763[8][8][0],&g_559,&l_763[8][8][0],&g_529[6],&l_763[8][8][0],&g_559,&l_763[8][8][0],&g_529[6],&l_763[8][8][0]},{&g_646,&g_646,&g_29[1][0],&l_763[4][2][1],&g_529[1],&g_529[1],&l_763[4][2][1],&g_29[1][0],&g_646},{&g_529[1],&g_529[6],(void*)0,&g_529[6],&g_529[1],&g_529[6],(void*)0,&g_529[6],&g_529[1]}};
            int32_t l_954 = 0x90E3E878L;
            int i, j;
            for (i = 0; i < 1; i++)
                l_874[i] = &l_850;
            for (i = 0; i < 6; i++)
                l_897[i] = &l_873;
            l_872 = func_75(l_862, (((*l_863) = (-0x1.Cp+1)) <= (((((*l_862) = ((safe_sub_func_float_f_f(((+(safe_mul_func_float_f_f(((g_644 == ((*l_869) = (-0x2.9p-1))) >= 0x1.EAA186p-38), g_111))) <= g_560), l_870)) == (*l_862))) > (-0x1.4p-1)) >= 0x2.5A058Fp-93) > g_99)), &l_870, l_871);
            ++g_877[6];
            if ((*l_872))
                break;
            if ((safe_mul_func_int8_t_s_s((((safe_add_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u((g_646 = (safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint8_t_u_u(((~0xEAL) <= (((safe_lshift_func_uint16_t_u_u((&g_574 == ((*l_894) = l_893)), (((safe_mul_func_uint16_t_u_u(((*l_862) = (((***l_767) &= ((((&l_872 == (l_897[0] = &l_862)) != (safe_add_func_int16_t_s_s((l_900 == (void*)0), g_700))) <= ((-1L) && 0xEFF10A7EL)) >= l_901)) ^ l_902)), p_63)) > p_61) && g_708))) & 0xFFA3A10DL) < p_64)), 2)), p_63))), g_700)) , p_62), 0xAEL)) ^ g_877[7]) && 0x779757B0L), p_61)))
            { /* block id: 381 */
                uint32_t *l_955 = &g_57;
                int32_t l_956 = (-4L);
                for (g_255 = (-25); (g_255 > (-7)); g_255 = safe_add_func_int64_t_s_s(g_255, 9))
                { /* block id: 384 */
                    int8_t *l_916[4][3][9] = {{{(void*)0,(void*)0,&g_255,&g_45,&g_45,&g_255,(void*)0,(void*)0,&g_854},{(void*)0,&g_854,&g_706,&g_379,&g_706,&g_854,(void*)0,&g_379,(void*)0},{(void*)0,&g_854,(void*)0,(void*)0,&g_854,(void*)0,&g_255,&g_706,&g_854}},{{&g_45,&g_255,&g_379,&g_255,&g_45,&g_379,&g_379,&g_379,&g_45},{&g_45,(void*)0,(void*)0,&g_45,&g_854,&g_706,&g_255,(void*)0,&g_854},{&g_706,&g_379,&g_706,&g_854,(void*)0,&g_379,(void*)0,&g_854,&g_706}},{{&g_854,&g_854,&g_706,(void*)0,&g_854,(void*)0,(void*)0,&g_255,&g_45},{&g_45,&g_379,&g_255,&g_854,&g_45,&g_854,&g_255,&g_379,&g_45},{&g_854,(void*)0,&g_706,&g_45,&g_854,&g_255,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_379,&g_706,&g_255,&g_706,&g_379,(void*)0,&g_255,(void*)0},{&g_854,&g_854,(void*)0,(void*)0,&g_45,(void*)0,&g_706,&g_706,(void*)0},{&g_45,&g_379,&g_379,&g_379,&g_45,&g_255,&g_379,&g_255,&g_45}}};
                    int32_t l_917 = 0x894D9C2FL;
                    int i, j, k;
                    (*l_872) = (safe_mod_func_uint8_t_u_u(((**l_900) ^= 1UL), (safe_mul_func_uint16_t_u_u((((1L & 0x0C4F06AE8B054D44LL) , (safe_add_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u(((g_700 = (l_917 = g_529[1])) ^ (((0L >= (~((safe_sub_func_uint32_t_u_u(0x5E764F6EL, (safe_mod_func_int16_t_s_s(((safe_rshift_func_int8_t_s_u((((safe_mod_func_uint64_t_u_u(g_379, (safe_mul_func_uint8_t_u_u((0xE494314BL & p_61), 1L)))) || p_61) || (-10L)), 6)) | (*l_862)), g_45)))) & p_61))) == p_62) >= g_529[8])), p_63)), (*l_862)))) >= g_57), 0xBA5AL))));
                }
                (*l_872) = (safe_sub_func_int16_t_s_s(((((((((safe_div_func_uint8_t_u_u((((((safe_sub_func_int8_t_s_s((~(!((safe_sub_func_uint64_t_u_u(((((*l_955) = ((safe_sub_func_int64_t_s_s((((p_63 > (((safe_mul_func_int16_t_s_s(0x8416L, ((p_64 || (safe_sub_func_uint16_t_u_u(((~g_854) > (p_61 ^ (safe_div_func_uint8_t_u_u((safe_add_func_int16_t_s_s(0x4321L, (safe_mod_func_uint8_t_u_u(l_954, p_61)))), p_62)))), g_45))) ^ 0x96L))) > 0xF0L) >= p_64)) > g_701) != (**g_745)), p_61)) && p_64)) < g_379) != p_62), p_61)) != (*g_574)))), 3UL)) | l_956) , (void*)0) == (void*)0) <= p_64), g_857)) | p_63) , g_877[6]) , (void*)0) != &g_123) > g_191) ^ p_63) && p_64), 0x72CDL));
            }
            else
            { /* block id: 392 */
                int32_t l_961 = 1L;
                for (g_519 = 10; (g_519 >= 48); ++g_519)
                { /* block id: 395 */
                    int8_t l_959 = (-4L);
                    --g_962;
                }
                for (g_708 = 0; (g_708 <= 9); g_708 += 1)
                { /* block id: 400 */
                    for (g_702 = 0; (g_702 <= 6); g_702 += 1)
                    { /* block id: 403 */
                        int i;
                        return g_108[g_702];
                    }
                    if (p_62)
                        break;
                }
            }
        }
        if (p_64)
        { /* block id: 410 */
            uint32_t l_965[6][7] = {{1UL,9UL,0xB3FD8257L,18446744073709551609UL,0xB3FD8257L,9UL,1UL},{18446744073709551614UL,18446744073709551615UL,0xB3FD8257L,0x58BA5779L,3UL,9UL,18446744073709551614UL},{1UL,18446744073709551615UL,0x76647699L,18446744073709551609UL,3UL,1UL,1UL},{1UL,9UL,0xB3FD8257L,18446744073709551609UL,0xB3FD8257L,9UL,1UL},{18446744073709551614UL,18446744073709551615UL,0xB3FD8257L,0x58BA5779L,3UL,9UL,18446744073709551614UL},{1UL,18446744073709551615UL,0x76647699L,18446744073709551609UL,3UL,1UL,1UL}};
            int32_t l_974 = 0x1A8FBDECL;
            int i, j;
            ++l_965[0][1];
            g_701 ^= ((g_29[4][1] == ((*l_765) = ((((0xDB10E897L || (safe_sub_func_int64_t_s_s((((p_63 , (void*)0) != ((((l_974 = (p_61 || (0x4518L >= (p_62 == 0L)))) == 0xABL) , g_191) , (void*)0)) || 4L), p_62))) | p_64) && (*l_862)) , l_965[5][1]))) != (*l_872));
            return l_965[0][1];
        }
        else
        { /* block id: 416 */
            uint16_t *l_988 = &g_559;
            int32_t l_989[3];
            uint16_t *l_990 = &g_29[3][1];
            int i;
            for (i = 0; i < 3; i++)
                l_989[i] = 4L;
            if (((((((((safe_div_func_int32_t_s_s((safe_mod_func_uint16_t_u_u(((*l_990) = ((l_989[0] = ((*l_988) = (p_61 , ((+(((**g_745)++) > (+(p_62 ^ ((((((void*)0 == &g_177) && ((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_u(0xC17DL, ((0x56A23C47L | 0x621E17D0L) >= g_960))), 0)) , (((*l_862) < g_643) > g_962))) == p_63) || 6L) , l_987))))) >= 0x6CB9L)))) > 0xE5DCL)), g_508)), 0x605FA9BBL)) >= g_57) && g_49) & g_45) , p_61) < p_62) > p_63) & p_64))
            { /* block id: 421 */
                int64_t l_998[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
                int32_t *l_999 = &l_875[4][1];
                int32_t l_1009 = 7L;
                float * const l_1064 = &g_876;
                int i;
                (*l_999) |= ((((l_989[0] |= ((*l_769) = ((&g_99 == l_991) != ((void*)0 != l_862)))) == p_61) && p_64) < ((((*l_862) , ((safe_lshift_func_uint8_t_u_s(0x69L, 4)) , (safe_rshift_func_uint8_t_u_s(((((l_998[2] = (--(**l_816))) >= 255UL) <= (*l_862)) || p_64), p_63)))) != p_61) >= p_61));
                for (g_17 = 0; (g_17 <= 1); g_17 += 1)
                { /* block id: 429 */
                    int32_t **l_1000 = &g_167;
                    int i;
                    (*l_1000) = func_75(&g_701, g_877[(g_17 + 7)], l_999, &g_3);
                    if (l_989[0])
                    { /* block id: 431 */
                        int32_t *l_1006 = &g_701;
                        int32_t *l_1007 = &l_989[2];
                        int32_t *l_1008[10][3][4] = {{{&l_989[2],&l_989[2],&l_850,(void*)0},{&g_701,&l_856[1],&g_123,(void*)0},{&l_856[1],&l_989[2],&l_989[0],&g_123}},{{&g_701,&l_989[2],&l_850,(void*)0},{&l_989[2],&l_856[1],&l_989[0],(void*)0},{&l_989[0],&l_989[2],&g_123,&g_123}},{{&l_989[2],&l_989[2],&l_850,(void*)0},{&g_701,&l_856[1],&g_123,(void*)0},{&l_856[1],&l_989[2],&l_989[0],&g_123}},{{&g_701,&l_989[2],&l_850,(void*)0},{&l_989[2],&l_856[1],&l_989[0],(void*)0},{&l_989[0],&l_989[2],&g_123,&g_123}},{{&l_989[2],&l_989[2],&l_850,(void*)0},{&g_701,&l_856[1],&g_123,(void*)0},{&l_856[1],&l_989[2],&l_989[0],&g_123}},{{&g_701,&l_989[2],&l_850,(void*)0},{&l_989[2],&l_856[1],&l_989[0],(void*)0},{&l_989[0],&l_989[2],&g_123,&g_123}},{{&l_989[2],&l_989[2],&l_850,(void*)0},{&g_701,&l_856[1],&g_123,(void*)0},{&l_856[1],&l_989[2],&l_989[0],&g_123}},{{&g_701,&l_989[2],&l_850,(void*)0},{&l_989[2],&l_856[1],&l_989[0],(void*)0},{&l_989[0],&l_989[2],&g_123,&g_123}},{{&l_989[2],&l_989[2],&l_850,(void*)0},{&g_701,&l_856[1],&g_123,(void*)0},{&l_856[1],&l_989[2],&l_989[0],&g_123}},{{&g_701,&l_989[2],&l_850,(void*)0},{&l_989[2],&l_856[1],&l_989[0],(void*)0},{&l_989[0],&l_989[2],&g_123,&g_123}}};
                        int i, j, k;
                        (*g_167) = (safe_mul_func_uint16_t_u_u((~(safe_lshift_func_uint8_t_u_u(p_64, 6))), g_701));
                        --g_1011;
                    }
                    else
                    { /* block id: 434 */
                        uint32_t l_1030 = 1UL;
                        float *l_1052 = &g_112[3];
                        int32_t *l_1059 = &l_989[0];
                        int32_t *l_1060[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_1060[i] = (void*)0;
                        (*l_1000) = func_75(&l_1009, (safe_div_func_float_f_f((((safe_div_func_float_f_f((**l_1000), (safe_add_func_float_f_f(l_989[0], ((safe_rshift_func_int16_t_s_s((safe_mod_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u(((((safe_sub_func_uint16_t_u_u(g_854, p_63)) || (g_1011 <= 4294967295UL)) > (safe_div_func_uint32_t_u_u((((*l_872) &= 0x49073F77L) || p_61), l_989[0]))) || 8L), l_989[1])) <= p_64), p_63)), 6)) , 0xC.581255p+74))))) < p_62) == l_1030), l_1030)), &g_701, &l_875[0][1]);
                        l_989[0] = ((safe_sub_func_float_f_f((-((*l_1052) = ((safe_mod_func_int32_t_s_s((safe_lshift_func_uint16_t_u_s(p_64, (safe_mul_func_int8_t_s_s((((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_u((safe_add_func_int64_t_s_s((*l_999), p_63)), ((**g_745) = (safe_rshift_func_int16_t_s_s((p_64 >= (p_64 != (((*l_999) && ((void*)0 == &l_901)) <= ((*l_862) = (((***l_767) ^= ((((safe_lshift_func_int8_t_s_s(p_62, 4)) & p_63) ^ (-2L)) < g_707)) > (-1L)))))), g_854))))) & 0x4A32L), 0x9FCFL)) | 0x8E34A2EDL) ^ p_63), p_61)))), p_61)) , (*l_999)))), p_64)) > g_436);
                        (*g_167) ^= ((*l_999) = ((g_521 ^ (*l_862)) != (p_64 && (safe_mul_func_uint16_t_u_u(0x6D08L, (((4294967295UL ^ (((g_529[1] ^ 0x9C2895C3E30FA9CBLL) == ((*l_990) &= (safe_rshift_func_int8_t_s_u(2L, (safe_lshift_func_int16_t_s_s((((*l_862) , 0x69E1837CL) != p_64), 6)))))) & g_379)) , (-1L)) , 0xFC70L))))));
                        g_1061--;
                    }
                    for (g_1010 = 0; (g_1010 <= 1); g_1010 += 1)
                    { /* block id: 449 */
                        uint32_t *l_1081 = (void*)0;
                        int i, j;
                        g_167 = func_75(&g_701, g_529[(g_1010 + 2)], ((((l_1064 == ((safe_mul_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s(g_877[(g_1010 + 7)], 4L)) , ((p_63 = (g_29[g_1010][g_1010] > (safe_mod_func_int64_t_s_s((((safe_mul_func_uint8_t_u_u((safe_div_func_uint32_t_u_u((g_877[(g_17 + 2)] = p_63), (((safe_mod_func_int32_t_s_s((l_875[(g_1010 + 3)][g_1010] = (safe_lshift_func_uint16_t_u_u(((*l_988) |= g_560), (((((void*)0 == &l_989[2]) != g_436) == g_112[2]) , 0xF320L)))), g_436)) && g_701) | (*g_167)))), (*l_872))) | p_64) , 0L), g_857)))) || p_63)), g_29[2][1])) , (void*)0)) == l_1082) && 3UL) , &l_1009), (*l_1000));
                    }
                    for (g_123 = 0; (g_123 <= 9); g_123 += 1)
                    { /* block id: 458 */
                        (*l_1000) = (void*)0;
                        return g_559;
                    }
                }
            }
            else
            { /* block id: 463 */
                int16_t *l_1087 = (void*)0;
                int16_t *l_1088 = &g_191;
                int16_t *l_1089 = (void*)0;
                int16_t *l_1090 = (void*)0;
                int16_t *l_1091 = &g_108[4];
                int32_t l_1105 = (-7L);
                int32_t *l_1106 = (void*)0;
                int32_t *l_1107 = &l_856[4];
                (*l_1107) ^= ((safe_mul_func_uint16_t_u_u(1UL, ((*l_1091) = ((*l_1088) = (safe_sub_func_int32_t_s_s(0x4C6CE824L, 4L)))))) && (((p_62 , (((**l_764) = p_62) <= (((((safe_add_func_uint32_t_u_u(((((safe_rshift_func_int16_t_s_u(((-2L) || (safe_div_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u(g_123, 11)), (safe_div_func_uint64_t_u_u((*l_862), (((p_61 != p_61) , g_29[1][0]) ^ 0x35D1434FL)))))), g_651)) || p_63) && (*g_574)) == l_989[0]), l_1104)) >= p_63) & l_1105) != g_56) == 0x433AF9E4693074E2LL))) , p_63) && (-3L)));
            }
        }
    }
    g_585 = (((((safe_lshift_func_uint16_t_u_s(((g_877[3] ^= (*l_872)) >= p_62), 2)) || p_61) < ((*l_872) >= ((*l_769) |= p_61))) , (g_529[1] ^= (g_29[4][1] == ((safe_mul_func_int16_t_s_s((((&l_856[5] != (((safe_add_func_float_f_f((((l_856[2] = ((*l_872) = (safe_mod_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s(((*g_574) || ((l_1122 = &g_646) != &g_29[5][1])), 0x15L)) && g_857), p_63)))) | 5UL) , p_63), 0x1.8p-1)) , 0xDD4AL) , l_1123)) == g_857) & g_48), 0L)) >= g_519)))) , &g_3);
    for (g_379 = 18; (g_379 < (-9)); --g_379)
    { /* block id: 480 */
        uint8_t ***l_1131 = (void*)0;
        int32_t l_1134[9][2][4] = {{{0x409333C8L,0x21687E82L,0x1640D507L,0xD646FE03L},{0L,0xD5F072BFL,0x767BBCBDL,0xD646FE03L}},{{(-3L),0x21687E82L,0x409333C8L,(-3L)},{(-1L),(-3L),(-1L),(-1L)}},{{0x767BBCBDL,1L,(-1L),0x21687E82L},{0xD646FE03L,(-1L),(-3L),1L}},{{(-1L),0xD91655ABL,(-3L),0xD5F072BFL},{0xD646FE03L,0x82BDEBFAL,(-1L),(-1L)}},{{0x767BBCBDL,0x767BBCBDL,(-1L),(-1L)},{(-1L),(-1L),0x409333C8L,(-3L)}},{{(-3L),0x70C85934L,0x767BBCBDL,0x409333C8L},{0L,0x70C85934L,0x1640D507L,(-3L)}},{{0x70C85934L,(-1L),(-1L),(-1L)},{0x1ACA7307L,0x767BBCBDL,1L,(-1L)}},{{(-1L),0x82BDEBFAL,0xD91655ABL,0xD5F072BFL},{(-1L),0xD91655ABL,0x70C85934L,1L}},{{(-1L),(-1L),0xD91655ABL,0x21687E82L},{(-1L),1L,1L,(-1L)}}};
        int i, j, k;
        (*l_1123) = ((safe_sub_func_int64_t_s_s((safe_lshift_func_uint16_t_u_u(((void*)0 != l_1131), 1)), (l_1134[8][1][1] = (l_1134[8][1][1] >= ((safe_add_func_int32_t_s_s(p_63, ((((g_1137 = (void*)0) == (void*)0) || (**g_745)) <= (g_1138 != ((+(*g_585)) , &l_765))))) == g_17))))) , p_64);
        g_167 = (((p_64 <= g_108[4]) > ((7L != 0xF3L) ^ p_62)) , (void*)0);
    }
    return p_62;
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_48 g_49 g_57 g_111 g_29 g_108 g_115 g_255 g_191 g_32 g_17 g_99 g_280 g_123 g_379 g_45 g_56 g_436 g_112 g_508 g_521 g_529 g_559 g_560 g_193 g_574 g_167 g_646 g_651 g_655 g_708 g_745 g_643
 * writes: g_48 g_32 g_17 g_115 g_111 g_57 g_280 g_167 g_255 g_123 g_379 g_56 g_112 g_46 g_47 g_191 g_436 g_99 g_508 g_519 g_521 g_529 g_6 g_574 g_585 g_560 g_559 g_646 g_651 g_701
 */
static uint32_t  func_69(int32_t * p_70, uint8_t  p_71, uint16_t * p_72, uint16_t * p_73, int64_t  p_74)
{ /* block id: 117 */
    int16_t l_253 = 0L;
    int32_t l_256 = 0x8F628736L;
    int32_t l_260 = (-4L);
    int32_t l_317 = (-2L);
    int32_t **l_343 = &g_167;
    int32_t ***l_342[9][2][1] = {{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}},{{&l_343},{&l_343}}};
    int32_t l_346 = 0x60569A44L;
    uint64_t *l_412 = &g_49;
    uint8_t l_421[3];
    uint16_t *l_501 = (void*)0;
    const int64_t l_562 = (-10L);
    uint32_t l_588[5] = {0x533807A9L,0x533807A9L,0x533807A9L,0x533807A9L,0x533807A9L};
    int8_t l_598 = 0x9CL;
    const int8_t *l_602 = (void*)0;
    int8_t l_609 = 2L;
    int8_t l_610 = 0x06L;
    uint64_t *l_611 = &g_519;
    uint8_t **l_744 = &g_574;
    uint8_t ** const l_747 = &g_574;
    uint8_t l_749 = 0x3CL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_421[i] = 255UL;
    if ((*p_70))
    { /* block id: 118 */
        int8_t l_257 = (-8L);
        int32_t l_262 = (-1L);
        int32_t *l_263[6] = {(void*)0,&l_262,(void*)0,(void*)0,&l_262,(void*)0};
        const int16_t *l_301 = (void*)0;
        int64_t *l_344 = (void*)0;
        int16_t l_358 = 0xE151L;
        int32_t ***l_424 = &l_343;
        uint8_t *l_483 = &l_421[2];
        uint8_t **l_482 = &l_483;
        int i;
        for (g_48 = 0; (g_48 <= 5); g_48 = safe_add_func_int16_t_s_s(g_48, 1))
        { /* block id: 121 */
            uint32_t l_241 = 4294967295UL;
            int64_t *l_252 = &g_32;
            int8_t *l_254[1][8] = {{&g_255,(void*)0,&g_255,(void*)0,&g_255,(void*)0,&g_255,(void*)0}};
            int16_t *l_258 = &l_253;
            int32_t l_259 = 0x5ECBE261L;
            uint8_t *l_261 = &g_111;
            int i, j;
            l_262 = (safe_rshift_func_uint8_t_u_s(((*l_261) = (g_115 |= ((safe_div_func_int8_t_s_s((safe_mod_func_int8_t_s_s(((g_17 = (safe_div_func_uint8_t_u_u(l_241, ((g_49 || g_57) || (safe_sub_func_uint16_t_u_u((0x521AF03943421533LL & ((safe_lshift_func_int8_t_s_u((l_259 = (+(safe_rshift_func_int16_t_s_u(((*l_258) = (((l_256 &= (((*l_252) = ((~(safe_mul_func_int16_t_s_s(0x543BL, l_241))) && 0xDB36F912L)) , l_253)) & ((p_74 , 4L) , l_257)) == g_111)), (*p_73))))), 2)) , 18446744073709551608UL)), g_108[4])))))) & 0x6407CE13FBB33ED7LL), l_260)), g_111)) | g_48))), 0));
            return g_108[6];
        }
        if ((l_256 = l_256))
        { /* block id: 133 */
            uint8_t l_264 = 248UL;
            int32_t ***l_273 = (void*)0;
            uint32_t *l_276 = (void*)0;
            uint32_t *l_277 = &g_57;
            int64_t *l_278 = &g_17;
            uint32_t *l_279 = &g_280;
            int32_t l_281 = 0xDD9B1EE1L;
            l_264++;
            if (g_111)
                goto lbl_282;
lbl_282:
            l_281 = ((p_74 && (+((*l_279) = ((g_255 <= p_74) | ((*l_278) = (safe_add_func_int16_t_s_s((((p_71 & (safe_rshift_func_int16_t_s_s(g_191, (((*l_277) = ((+g_48) & (l_273 == ((safe_rshift_func_uint16_t_u_u((p_71 , (&g_3 == &g_123)), (*p_72))) , (void*)0)))) == (*p_70))))) <= 0L) && (*p_70)), g_32))))))) ^ g_32);
            g_167 = p_70;
        }
        else
        { /* block id: 141 */
            uint16_t l_312 = 0UL;
            uint16_t l_316 = 0xDD05L;
            uint8_t *l_318 = &g_115;
            int64_t *l_319 = &g_17;
            int8_t *l_320[5];
            int32_t l_321 = 0x5A846277L;
            int32_t l_322 = 0x12024B52L;
            int32_t **l_347 = (void*)0;
            uint8_t l_423 = 246UL;
            int i;
            for (i = 0; i < 5; i++)
                l_320[i] = &l_257;
            l_260 = ((l_322 |= ((safe_mul_func_int8_t_s_s(g_191, (l_321 = (safe_sub_func_uint16_t_u_u(l_256, (((*l_319) = (safe_sub_func_uint64_t_u_u((safe_sub_func_uint16_t_u_u(((((*l_318) ^= (safe_sub_func_int32_t_s_s((safe_mod_func_uint64_t_u_u(((((g_17 | (l_317 ^= (safe_sub_func_int64_t_s_s(((safe_rshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((p_72 == l_301), ((safe_lshift_func_int8_t_s_s(g_3, 5)) & (l_260 && ((((safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(l_312, ((safe_rshift_func_uint8_t_u_s(((((~(g_99 || 0xFCL)) , 1L) || g_108[4]) != 1UL), l_316)) == l_253))), 2)) <= 0x54L), g_255)), l_316)) , p_70) != (void*)0) < 4294967295UL))))), l_253)) , 0xA575AA0C160E5AE0LL), p_74)))) && l_312) & p_71) || g_280), l_260)), l_316))) ^ l_312) == l_316), 0x76D0L)), 0x8B8C2CD7695A7DD3LL))) | l_260)))))) , 0L)) == p_74);
            if ((*p_70))
            { /* block id: 148 */
                float l_345 = 0xC.7C274Ep+81;
                int8_t *l_377 = (void*)0;
                int32_t l_378 = 0x952F90A0L;
                int32_t l_383 = 0x0E3F7F69L;
                uint8_t *l_393 = &g_111;
                uint16_t l_401 = 65527UL;
lbl_384:
                g_123 &= ((l_316 == (+(safe_add_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s(((((g_255 = (((*l_319) = p_74) > (safe_sub_func_int32_t_s_s(((safe_div_func_uint8_t_u_u(((*l_318) |= (safe_div_func_int64_t_s_s(((safe_div_func_uint64_t_u_u(p_71, 0x0C1789064FA25865LL)) == (safe_sub_func_int32_t_s_s((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s(((g_32 = ((&g_32 == ((((void*)0 == l_342[4][0][0]) == g_280) , l_344)) == 9L)) != l_316), 9)) ^ l_316), 3)), 0xAB521217L))), 0xC1910D5989F5D12ALL))), g_111)) && 0xC2E8F7DCB865D62CLL), (*p_70))))) <= l_346) , (void*)0) != l_347), 6)), 6UL)))) , 2L);
                for (g_280 = 0; (g_280 <= 0); g_280 += 1)
                { /* block id: 156 */
                    int8_t l_361[4][2][9] = {{{1L,(-1L),0x8DL,(-1L),1L,0x8DL,0xF4L,4L,(-1L)},{(-1L),0xC9L,0x8DL,0x00L,0xC9L,3L,(-1L),3L,0xC9L}},{{(-1L),(-1L),(-1L),(-1L),4L,0xC9L,1L,3L,(-1L)},{1L,4L,1L,0x57L,0x0CL,0xC9L,(-1L),4L,4L}},{{0xF4L,3L,4L,0x57L,4L,3L,0xF4L,(-1L),3L},{0x57L,3L,1L,(-1L),0xC9L,0x8DL,0x00L,0xC9L,3L}},{{0xF4L,4L,(-1L),0x00L,1L,1L,0x00L,(-1L),4L},{1L,(-1L),0x8DL,(-1L),1L,0x8DL,0xF4L,4L,(-1L)}}};
                    int32_t l_380 = 0L;
                    int32_t *l_381 = &g_56;
                    int32_t *l_382 = &g_48;
                    uint8_t *l_391 = (void*)0;
                    int i, j, k;
                    l_383 |= (safe_mul_func_uint8_t_u_u((((*l_382) &= ((*l_381) = ((safe_lshift_func_uint16_t_u_u(((l_321 = (safe_mod_func_int8_t_s_s(((safe_sub_func_int32_t_s_s((safe_rshift_func_int16_t_s_u(l_358, ((safe_add_func_int8_t_s_s((p_74 | (l_361[0][1][5] = g_108[4])), (safe_lshift_func_int16_t_s_s(((g_379 &= (safe_mul_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(0x8181665E0BEF41ADLL, 0xDF6E22EF36695570LL)), (!((safe_lshift_func_int16_t_s_s(((safe_div_func_uint8_t_u_u((safe_add_func_int8_t_s_s((0x33A6A4DEL != ((l_377 != (void*)0) , (p_74 != l_378))), g_29[1][0])), 0x6EL)) > g_29[1][0]), 6)) <= 0xC08FL))))) >= g_191), 1)))) ^ p_74))), (*p_70))) | l_380), 0x44L))) && g_280), 4)) & (*p_70)))) , 0x1AL), l_378));
                    if (g_280)
                        goto lbl_384;
                    for (g_32 = 0; (g_32 <= 7); g_32 += 1)
                    { /* block id: 166 */
                        uint8_t **l_392[8] = {&l_318,&l_318,&l_318,&l_318,&l_318,&l_318,&l_318,&l_318};
                        int32_t l_396 = 0xFADD72D6L;
                        uint8_t l_404 = 1UL;
                        int i;
                        g_123 &= (l_404 = (safe_sub_func_int64_t_s_s((safe_sub_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(g_45, ((((l_391 = &g_115) == (l_393 = &g_115)) && (((((safe_lshift_func_uint8_t_u_u(l_396, (safe_add_func_int64_t_s_s(((*l_319) |= (safe_rshift_func_int16_t_s_u(g_111, l_401))), ((g_255 ^= (l_383 == (safe_lshift_func_int8_t_s_u(p_71, 5)))) , (g_280 >= 1L)))))) < g_280) | l_396) , p_71) & 4294967286UL)) >= l_396))), g_99)), p_71)));
                        return g_191;
                    }
                }
                l_383 |= g_123;
            }
            else
            { /* block id: 177 */
                uint8_t l_419 = 0xDBL;
                uint64_t *l_420 = &g_49;
                int32_t l_422 = 0L;
                int64_t l_425 = 0xC9C3BC8424BEAB7BLL;
                int32_t **l_481 = (void*)0;
                if ((*p_70))
                { /* block id: 178 */
                    uint64_t l_405 = 0x9A7F4A1E99C80B20LL;
                    int64_t l_426 = 1L;
                    for (l_346 = 0; (l_346 >= 0); l_346 -= 1)
                    { /* block id: 181 */
                        uint64_t *l_411 = &g_99;
                        uint64_t **l_410 = &l_411;
                        float *l_413 = (void*)0;
                        float *l_414 = &g_46;
                        float *l_415 = &g_47;
                        int16_t *l_418 = &g_191;
                        uint32_t *l_434[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_434[i] = &g_57;
                        --l_405;
                        (*l_415) = (safe_mul_func_float_f_f((((*l_414) = (g_112[(l_346 + 2)] = (((*l_410) = (void*)0) != l_412))) >= 0xE.35BE2Cp-24), (g_123 >= p_71)));
                        l_426 = (((g_29[1][0] >= (((((l_422 &= ((safe_lshift_func_uint16_t_u_s((((((*l_418) = 0x58CFL) , ((l_419 , (*l_410)) == l_420)) <= 0xD065C9D6L) & ((p_71 != (0x4DB70A726BBF8BFFLL || g_56)) == l_405)), 2)) >= l_421[2])) , l_423) | 0x42F112EAL) , l_424) == &l_347)) == l_419) <= l_425);
                        g_123 &= ((l_322 = (safe_lshift_func_uint8_t_u_u(250UL, (safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(p_71, 0x93L)), (safe_unary_minus_func_uint32_t_u(4294967287UL))))))) | g_56);
                    }
                }
                else
                { /* block id: 193 */
                    uint16_t l_451 = 0x60F8L;
                    int32_t l_455 = 0x40780A0FL;
                    uint8_t **l_474[8] = {(void*)0,(void*)0,(void*)0,&l_318,&l_318,(void*)0,&l_318,&l_318};
                    int16_t **l_478 = (void*)0;
                    int16_t *l_480[2][8][10] = {{{&g_108[3],&l_358,&g_108[4],&l_358,&l_253,(void*)0,&l_253,&g_191,&l_253,&g_191},{&l_253,&g_108[3],&g_108[5],&l_253,&g_108[5],&g_108[3],&l_253,(void*)0,&l_358,&g_108[3]},{&g_108[1],&g_108[4],&l_358,&g_108[4],&l_358,&l_253,&g_191,&g_108[4],(void*)0,(void*)0},{&l_253,&g_108[4],(void*)0,&l_253,&g_108[3],&g_191,&l_253,&g_108[3],(void*)0,(void*)0},{(void*)0,&g_108[3],&g_108[4],&l_358,&g_108[3],(void*)0,&l_253,(void*)0,(void*)0,&l_253},{&l_358,&l_358,&l_253,(void*)0,(void*)0,(void*)0,(void*)0,&l_253,&l_358,&l_358},{&l_253,&g_108[4],(void*)0,&g_108[4],&g_108[4],(void*)0,&g_108[1],&l_253,&l_358,&g_108[4]},{&l_358,&g_108[5],&l_358,&g_108[4],&g_108[4],&l_253,&g_108[3],&l_253,(void*)0,&l_358}},{{&g_108[4],&g_108[1],&l_253,(void*)0,(void*)0,(void*)0,&g_108[4],&l_253,&g_108[4],&l_253},{(void*)0,&l_253,&l_358,(void*)0,&g_108[3],&g_191,&g_108[5],&g_191,&g_108[3],(void*)0},{&g_108[4],(void*)0,&g_108[4],&l_253,&g_108[3],&g_191,(void*)0,&g_108[4],&g_108[5],(void*)0},{(void*)0,&g_108[4],&l_358,&g_191,&l_358,&g_108[4],(void*)0,&g_108[4],&g_191,&g_108[3]},{&l_253,&l_253,&g_108[4],&g_108[4],&g_108[5],&g_108[1],&g_108[6],&g_191,(void*)0,&g_191},{&l_253,&l_253,&l_358,(void*)0,&l_253,&g_108[3],&l_358,&l_253,&l_253,&l_358},{&l_358,&g_191,&l_253,&l_253,&g_191,&l_358,&g_108[4],&l_253,&g_108[4],&l_358},{&g_108[5],(void*)0,&l_358,&l_358,(void*)0,&l_358,(void*)0,&l_253,&g_108[4],&g_108[4]}}};
                    int16_t **l_479 = &l_480[0][2][8];
                    int i, j, k;
                    for (l_260 = 7; (l_260 >= 0); l_260 -= 1)
                    { /* block id: 196 */
                        int8_t l_435 = 0xD6L;
                        int i;
                        ++g_436;
                        if (g_115)
                            continue;
                        l_455 = ((((((!g_112[l_260]) , (((((((2UL < (+(l_435 || ((safe_rshift_func_uint8_t_u_u(((*l_318) = (safe_add_func_int32_t_s_s(((((((((safe_sub_func_int32_t_s_s((safe_rshift_func_uint16_t_u_u((safe_sub_func_int64_t_s_s(l_451, ((((g_123 = g_280) , p_71) , g_280) != ((safe_unary_minus_func_int16_t_s(p_71)) < (p_74 = (safe_mul_func_int16_t_s_s(l_425, 0x0D42L))))))), 0)), (*p_70))) < p_71) && p_74) & p_71) , l_451) > l_451) < (*p_72)) , (-7L)), p_71))), 1)) , 0xF2FACD075D00FB13LL)))) <= g_108[4]) < 0x95B3L) , g_48) ^ g_108[4]) == l_435) >= 0x8CC5L)) & p_71) != (*p_70)) < g_108[4]) >= (*p_70));
                    }
                    (*l_343) = func_75(p_70, ((((safe_sub_func_float_f_f((-0x5.9p-1), g_379)) >= (safe_sub_func_float_f_f((((safe_sub_func_int8_t_s_s((safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((safe_sub_func_int32_t_s_s((safe_lshift_func_int8_t_s_u(l_455, 1)), (safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_s((l_474[6] != ((safe_mul_func_int8_t_s_s((((safe_unary_minus_func_int8_t_s(((l_422 & (((*l_479) = &g_108[4]) == &l_253)) > (l_481 == (*l_424))))) , &l_253) != p_72), 0xD6L)) , l_482)), 10)), p_71)))) , 253UL), 3)), g_48)), p_74)) , (void*)0) == p_70), (-0x1.1p+1)))) >= p_71) != g_29[1][0]), &g_3, p_70);
                }
                if (l_425)
                { /* block id: 207 */
                    uint64_t *l_492 = &g_99;
                    uint64_t *l_507 = &g_508;
                    int32_t l_511[4];
                    int i;
                    for (i = 0; i < 4; i++)
                        l_511[i] = (-1L);
                    g_123 |= ((((safe_div_func_int64_t_s_s((safe_div_func_uint8_t_u_u(((!(safe_rshift_func_int8_t_s_s(((safe_unary_minus_func_uint64_t_u(((*l_492) = g_191))) | p_74), (safe_div_func_uint16_t_u_u(((safe_mod_func_uint8_t_u_u(((*l_483) = ((safe_lshift_func_int8_t_s_s(p_71, (safe_sub_func_int64_t_s_s(((&g_29[4][1] == l_501) < ((safe_unary_minus_func_uint32_t_u(p_71)) < (+18446744073709551608UL))), (safe_unary_minus_func_int16_t_s((((safe_sub_func_uint16_t_u_u(((1L & ((*l_507)--)) > p_74), p_71)) >= 0xF05BCFAF82755D5FLL) != p_71))))))) || 0xE4L)), g_108[4])) >= g_108[4]), 5L))))) > g_32), g_57)), 0x2DB6BABF7D19A895LL)) > g_56) ^ g_115) , g_32);
                    if ((*p_70))
                    { /* block id: 212 */
                        l_511[1] ^= (0xF215CE01L || (p_71 < p_74));
                    }
                    else
                    { /* block id: 214 */
                        uint8_t l_512 = 255UL;
                        --l_512;
                    }
                }
                else
                { /* block id: 217 */
                    const uint64_t l_520 = 18446744073709551614UL;
                    int32_t l_526[1];
                    uint16_t *l_527 = &l_312;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_526[i] = 2L;
                    g_123 = (g_529[1] ^= (safe_add_func_int32_t_s_s((g_521 &= (safe_lshift_func_uint16_t_u_s((((g_519 = g_436) , (g_436 != g_123)) != l_520), 6))), (((*l_527) = (safe_mod_func_int8_t_s_s((safe_div_func_int8_t_s_s((l_526[0] |= g_17), p_74)), p_71))) | (!((void*)0 != &g_123))))));
                }
                return p_71;
            }
            return g_17;
        }
    }
    else
    { /* block id: 229 */
        const int32_t *l_530 = &l_260;
        int32_t * const l_531 = &l_256;
        uint16_t *l_554 = &g_529[4];
        float *l_561 = &g_6;
        uint8_t *l_573 = &g_111;
        l_530 = func_75(p_70, g_29[8][0], l_530, l_531);
        (*l_561) = ((((((safe_rshift_func_int16_t_s_u((((0xBFBEE096L ^ (*l_531)) < ((safe_div_func_uint16_t_u_u(g_280, ((*p_70) , (safe_sub_func_uint16_t_u_u(((*p_70) | (safe_rshift_func_uint8_t_u_u(4UL, (((safe_rshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((safe_add_func_int64_t_s_s(((((((g_255 != (safe_mod_func_uint16_t_u_u((safe_add_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((*l_554)++), ((safe_add_func_int8_t_s_s((g_559 | g_17), g_560)) > g_32))), 9L)), 0x823CL))) , 1L) , 0x22942BDEL) > g_280) , 0x5D849070L) && 4294967295UL), g_559)), 14)), g_379)), (*l_531))) , (*l_530)) < 0xEBL)))), p_74))))) > p_74)) && 252UL), g_280)) , g_508) <= 0xB.07585Fp-89) == (*l_530)) <= g_123) != 0x0.1p+1);
        if (l_562)
        { /* block id: 233 */
            uint32_t l_570 = 0x91A2BF89L;
            const int32_t * const l_586 = (void*)0;
            int16_t *l_587 = &g_191;
            g_123 = (l_588[4] |= ((*l_531) ^= ((safe_lshift_func_uint16_t_u_u((0x18L > p_74), (safe_mod_func_uint16_t_u_u((!(safe_mul_func_int16_t_s_s(((4UL || l_570) && (safe_div_func_uint16_t_u_u((&l_421[2] != (g_574 = l_573)), (safe_div_func_int16_t_s_s(2L, (((safe_add_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((safe_mul_func_int16_t_s_s(((*l_587) = ((((g_585 = func_75(p_70, (((((*l_554) = (safe_div_func_int16_t_s_s(2L, g_560))) , l_570) ^ p_71) , 0x1.1p+1), p_70, &g_3)) != l_586) ^ 0x7CL) , g_529[5])), 0x742BL)), 0x261E464B5DF1EF3BLL)), 9UL)) , 0x9C59L) & (*l_530))))))), (*p_73)))), g_560)))) | p_71)));
            return p_71;
        }
        else
        { /* block id: 242 */
            return g_193;
        }
    }
    g_123 |= (((safe_mod_func_uint64_t_u_u(((*l_611) = (g_99 = (safe_rshift_func_int16_t_s_s(((*g_574) , (safe_lshift_func_int8_t_s_u((~((((safe_rshift_func_uint16_t_u_u((((l_598 <= (((**l_343) || 1L) != (g_508 = ((&g_48 != ((!(((--g_529[1]) , &g_45) == (l_602 = &g_45))) , (void*)0)) != (safe_sub_func_uint8_t_u_u((safe_sub_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(((**l_343) , (**l_343)), 4)), g_48)), g_193)))))) ^ l_609) != 0xCC7CL), 10)) ^ l_610) == 4294967293UL) | (*g_167))), (**l_343)))), 10)))), g_17)) , &l_611) != (void*)0);
    for (g_560 = 0; (g_560 > 41); g_560++)
    { /* block id: 254 */
        uint16_t l_616[8][10][1] = {{{65534UL},{0x8091L},{8UL},{65527UL},{65527UL},{8UL},{0x8091L},{65534UL},{0x8091L},{8UL}},{{65527UL},{65527UL},{8UL},{0x8091L},{65534UL},{0x8091L},{8UL},{65527UL},{65527UL},{8UL}},{{0x8091L},{65534UL},{0x8091L},{8UL},{65527UL},{2UL},{65534UL},{8UL},{0xF4C9L},{8UL}},{{65534UL},{2UL},{2UL},{65534UL},{8UL},{0xF4C9L},{8UL},{65534UL},{2UL},{2UL}},{{65534UL},{8UL},{0xF4C9L},{8UL},{65534UL},{2UL},{2UL},{65534UL},{8UL},{0xF4C9L}},{{8UL},{65534UL},{2UL},{2UL},{65534UL},{8UL},{0xF4C9L},{8UL},{65534UL},{2UL}},{{2UL},{65534UL},{8UL},{0xF4C9L},{8UL},{65534UL},{2UL},{2UL},{65534UL},{8UL}},{{0xF4C9L},{8UL},{65534UL},{2UL},{2UL},{65534UL},{8UL},{0xF4C9L},{8UL},{65534UL}}};
        int8_t l_636 = 0xD3L;
        int32_t l_641 = 0x244177C9L;
        int64_t *l_675 = (void*)0;
        int32_t l_699 = 0xDC275228L;
        int32_t *l_711 = &g_56;
        int16_t *l_737 = &g_707;
        uint8_t **l_746 = (void*)0;
        float *l_748 = &g_47;
        int i, j, k;
        for (g_57 = (-26); (g_57 <= 52); g_57 = safe_add_func_uint32_t_u_u(g_57, 2))
        { /* block id: 257 */
            int16_t *l_623 = (void*)0;
            int8_t *l_637 = &l_609;
            float l_638[9][10][2] = {{{0x7.61588Ap+99,0x1.5p-1},{0x0.3p-1,(-0x6.3p+1)},{0xD.45989Fp+30,0xE.52C00Ep+8},{0x1.F998BFp-68,0x5.53EE9Ap+2},{0x8.88785Ep-27,0x1.FE7959p+42},{(-0x1.Fp+1),0x0.Bp+1},{0x7.97F2CDp-36,0xA.4B506Ap+45},{0x8.C1B522p-94,(-0x1.Bp+1)},{0x6.6p+1,0x1.Bp-1},{0x9.5473A0p-54,0x7.61588Ap+99}},{{0x0.Bp+1,0xB.117442p-28},{0x8.14FAB2p+8,0x0.9FAEB5p-17},{(-0x2.6p+1),0x1.6p-1},{0x0.7p+1,(-0x1.Fp+1)},{0x3.8DCD98p-77,0xC.98D766p-24},{0x7.4p+1,0x0.7p+1},{(-0x6.3p+1),0xE.CD0F43p-17},{0x8.2p+1,0x4.9p+1},{(-0x1.Ep+1),0x0.0p-1},{(-0x6.4p+1),0xD.82D669p-1}},{{0x7.Cp+1,0x8.C1B522p-94},{(-0x1.Ep-1),0x5.Bp+1},{0x5.782915p-15,0x5.782915p-15},{0xE.52C00Ep+8,0x0.F89DCAp+20},{0x1.4p-1,0x6.6p+1},{0x2.B6A4CCp+89,0xC.1B5781p+87},{0x1.5p-1,0x2.B6A4CCp+89},{0x1.Bp-1,0xC.E4011Cp+19},{0x1.Bp-1,0x2.B6A4CCp+89},{0x1.5p-1,0xC.1B5781p+87}},{{0x2.B6A4CCp+89,0x6.6p+1},{0x1.4p-1,0x0.F89DCAp+20},{0xE.52C00Ep+8,0x5.782915p-15},{0x5.782915p-15,0x5.Bp+1},{(-0x1.Ep-1),0x8.C1B522p-94},{0x7.Cp+1,0xD.82D669p-1},{(-0x6.4p+1),0x0.0p-1},{(-0x1.Ep+1),0x4.9p+1},{0x8.2p+1,0xE.CD0F43p-17},{(-0x6.3p+1),0x0.7p+1}},{{0x7.4p+1,0xC.98D766p-24},{0x3.8DCD98p-77,(-0x1.Fp+1)},{0x0.7p+1,0x1.6p-1},{(-0x2.6p+1),0x0.9FAEB5p-17},{0x8.14FAB2p+8,0xB.117442p-28},{0x0.Bp+1,0x7.61588Ap+99},{0x9.5473A0p-54,0x1.Bp-1},{0x6.6p+1,(-0x1.Bp+1)},{0x8.C1B522p-94,0xA.4B506Ap+45},{0x7.97F2CDp-36,0x0.Bp+1}},{{(-0x1.Fp+1),0x1.FE7959p+42},{0x8.88785Ep-27,0x5.53EE9Ap+2},{0x1.F998BFp-68,0xE.52C00Ep+8},{0xD.45989Fp+30,(-0x6.3p+1)},{0x0.3p-1,0x1.5p-1},{0x7.61588Ap+99,(-0x2.6p+1)},{0xA.4B506Ap+45,0xC.5CEB63p+88},{(-0x10.3p-1),0x1.F998BFp-68},{0xF.1A5535p+5,0x1.F998BFp-68},{(-0x10.3p-1),0xC.5CEB63p+88}},{{0xA.4B506Ap+45,(-0x2.6p+1)},{0x7.61588Ap+99,0x1.5p-1},{0x0.3p-1,(-0x6.3p+1)},{0xD.45989Fp+30,0xE.52C00Ep+8},{0x1.F998BFp-68,0x5.53EE9Ap+2},{0x8.88785Ep-27,0x1.FE7959p+42},{(-0x1.Fp+1),0x0.Bp+1},{0x7.97F2CDp-36,0xA.4B506Ap+45},{0x0.Ep+1,0xD.45989Fp+30},{0xF.1A5535p+5,0x0.Bp+1}},{{0xB.DB2929p-42,0x0.0p-1},{0x1.5p-1,0x1.F998BFp-68},{(-0x1.Ep+1),(-0x1.Fp+1)},{0x6.5p-1,0x1.1p-1},{0x8.14FAB2p+8,0xA.4B506Ap+45},{0x1.Bp-1,0xC.5CEB63p+88},{0x7.97F2CDp-36,0x8.14FAB2p+8},{(-0x6.4p+1),0xC.E4011Cp+19},{0xB.117442p-28,0x0.AFDA2Fp+41},{0x5.782915p-15,0xA.71ADCBp-10}},{{0x1.4p-1,0xC.1B5781p+87},{(-0x2.6p+1),0x0.Ep+1},{(-0x1.Ep+1),(-0x1.Ep-1)},{0x0.3p-1,0x0.3p-1},{0x0.F89DCAp+20,0x1.Bp-1},{0x3.C01947p+40,0xF.1A5535p+5},{0x0.DDAB6Fp+83,(-0x6.3p+1)},{0x8.88785Ep-27,0x0.DDAB6Fp+83},{0x0.Bp+1,0x2.B6A4CCp+89},{0x0.Bp+1,0x0.DDAB6Fp+83}}};
            int32_t l_642 = (-3L);
            int32_t l_645 = 0x522DC87EL;
            int32_t l_698[5][6][8] = {{{0xAF4199FDL,0x5047B8A7L,1L,0L,0x8BF59D9AL,0x1A404466L,0x3B1ABABCL,(-1L)},{7L,5L,1L,0x5047B8A7L,(-1L),0L,0x4AFBDAF2L,0x4AFBDAF2L},{0xA5938989L,0x341830C2L,0x909D220BL,0x909D220BL,0x341830C2L,0xA5938989L,0x1CE4E6A6L,0x62589685L},{6L,0xAF4199FDL,(-1L),0x7FBD5766L,1L,0x1CA79965L,(-4L),0L},{1L,9L,5L,0x7FBD5766L,0xFB26F787L,0xBA7ADA6CL,0x75145D9FL,0x62589685L},{0L,0xFB26F787L,1L,0x909D220BL,0x113D3C45L,3L,0x875928F8L,0x4AFBDAF2L}},{{0x6A6A11F1L,0x7C1CE8F4L,(-2L),0x5047B8A7L,0x7FBD5766L,(-4L),0x2A7826DBL,(-1L)},{0L,0xC640093DL,3L,0L,1L,0xFB26F787L,0x909D220BL,0L},{0L,0xD60402B8L,(-1L),0L,0xA50E4DF2L,0x909D220BL,3L,1L},{2L,0xFE322E20L,0x7FBD5766L,0x5D013DC9L,(-1L),1L,1L,0x2A7826DBL},{3L,5L,(-7L),4L,0xB132E945L,0x3B1ABABCL,0x5047B8A7L,0xBA7ADA6CL},{0x7FBD5766L,0x60BBB03FL,0L,5L,0xA0BF4D67L,7L,0xAF4199FDL,0x341830C2L}},{{(-4L),0L,0L,0L,2L,5L,0xA5938989L,0L},{(-3L),(-7L),0xC640093DL,(-6L),(-7L),0xC61AE91AL,(-1L),5L},{0x2A7826DBL,0L,0xF834C3C5L,0x7C1CE8F4L,0x84340749L,0x6A6A11F1L,0x2BA5CD0CL,(-6L)},{0x1CE4E6A6L,0x93DDF9D1L,0L,9L,(-4L),0x8BF59D9AL,3L,(-1L)},{0L,2L,7L,0x4F63483BL,7L,2L,0L,(-4L)},{1L,0x88B658DFL,0xB132E945L,1L,0xA5938989L,1L,0xD60402B8L,7L}},{{1L,0L,0L,0L,0xB8A9DB2CL,1L,0x6A6A11F1L,0xA50E4DF2L},{0xBA7ADA6CL,0x047A6F2CL,(-1L),4L,2L,1L,0L,0xF8F7B318L},{7L,0xBD4BF200L,0L,1L,3L,0x93DDF9D1L,0xF8F7B318L,0x82A8B266L},{0x60BBB03FL,0x8303B74AL,(-1L),0x341830C2L,(-7L),(-7L),(-6L),(-1L)},{0x341830C2L,7L,0x10FABF97L,5L,2L,0L,0xB10621D9L,0xA5938989L},{0xB10621D9L,0x341830C2L,0xCCB5C9ECL,1L,3L,0x909D220BL,0xA50E4DF2L,0L}},{{3L,1L,0x875928F8L,0xD8CD2DE1L,0L,0x69FC8382L,(-1L),0x8BF59D9AL},{(-1L),0xFE322E20L,0xB10621D9L,0x20301755L,1L,0xEA71DBD8L,7L,0x18753A93L},{0x4F63483BL,6L,0xFE322E20L,0L,8L,0x1CE4E6A6L,0xBD4BF200L,0x1CA79965L},{0L,(-6L),0xB132E945L,3L,0x69FC8382L,6L,0L,0x047A6F2CL},{0x21300CB9L,0x4F63483BL,0x7FBD5766L,6L,0x5047B8A7L,0xF46BCD95L,0L,9L},{(-4L),0x1CA79965L,1L,0x7FBD5766L,(-1L),0xAF4199FDL,6L,0x62585660L}}};
            int i, j, k;
            g_123 &= (&g_48 != ((g_508 <= (((1UL < l_616[5][5][0]) && (*p_70)) >= (safe_mod_func_int16_t_s_s((safe_unary_minus_func_uint16_t_u((*p_73))), (safe_sub_func_uint32_t_u_u((((g_191 = (~g_193)) <= (safe_add_func_int8_t_s_s(((*l_637) = (safe_mul_func_uint8_t_u_u((((l_636 &= (~((~(safe_mul_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_s((((safe_sub_func_uint8_t_u_u((((0x5132B258L > (&g_180 == &p_70)) & 0x897FL) == 0L), (*g_574))) && 0x5FBE4C1A5A1103E7LL) | p_71), p_74)) != (-7L)), (-6L)))) , l_616[6][5][0]))) == 18446744073709551609UL) & g_508), 0x83L))), l_616[5][1][0]))) && 6L), p_71)))))) , (void*)0));
            for (g_255 = 7; (g_255 >= 0); g_255 -= 1)
            { /* block id: 264 */
                uint64_t l_640[2];
                int32_t l_656 = (-1L);
                int i;
                for (i = 0; i < 2; i++)
                    l_640[i] = 18446744073709551615UL;
                if ((safe_unary_minus_func_uint8_t_u(l_640[1])))
                { /* block id: 265 */
                    int32_t l_654 = 0x9CEB2EA8L;
                    int i;
                    for (g_559 = 0; (g_559 <= 0); g_559 += 1)
                    { /* block id: 268 */
                        int32_t l_649 = 0xE340C847L;
                        int32_t l_650[5][6][2] = {{{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)},{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)}},{{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)},{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)}},{{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)},{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)}},{{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)},{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)}},{{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)},{0x397FD97CL,(-1L)},{(-1L),0x397FD97CL},{(-1L),(-1L)}}};
                        int i, j, k;
                        --g_646;
                        --g_651;
                    }
                    g_112[g_255] = (0x8.86CC48p-68 < g_99);
                    l_654 = 0x75939323L;
                }
                else
                { /* block id: 274 */
                    for (l_317 = 0; (l_317 <= 0); l_317 += 1)
                    { /* block id: 277 */
                        int i, j, k;
                        if ((*p_70))
                            break;
                        l_656 &= (&g_180 != g_655);
                    }
                }
            }
        }
        g_701 = ((safe_div_func_int32_t_s_s(((safe_mod_func_uint64_t_u_u((((safe_sub_func_float_f_f((safe_add_func_float_f_f(((*l_748) = (safe_sub_func_float_f_f(((+(p_74 <= (safe_add_func_float_f_f(((safe_add_func_float_f_f((safe_div_func_float_f_f((((safe_sub_func_float_f_f((safe_mul_func_float_f_f((0x1.1237BDp+13 <= ((safe_div_func_float_f_f((safe_sub_func_float_f_f(((((((p_71 , l_737) == &l_253) == (safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f((((l_746 = (l_744 = ((((l_744 == &g_574) >= (((void*)0 == p_70) || g_708)) <= 0x3FL) , g_745))) != l_747) > g_112[2]), g_708)), 0xA.694B18p-70)), p_74))) >= p_71) == (-0x5.Cp-1)) != (-0x1.1p+1)), l_616[5][5][0])), (-0x5.2p-1))) < g_32)), (-0x5.2p+1))), p_71)) == p_74) == 0x4.B8CDE0p+39), g_379)), 0x6.4p-1)) <= (**l_343)), 0x5.Fp-1)))) <= g_193), 0x3.747FB9p+63))), p_71)), p_74)) < p_74) , 0UL), l_749)) | g_29[1][0]), 0x2D41EF6BL)) >= l_616[5][5][0]);
    }
    return g_643;
}


/* ------------------------------------------ */
/* 
 * reads : g_48
 * writes: g_48
 */
static int32_t * func_75(int32_t * p_76, const float  p_77, const int32_t * p_78, int32_t * const  p_79)
{ /* block id: 110 */
    uint8_t l_228 = 0x5AL;
    for (g_48 = 7; (g_48 <= (-14)); g_48 = safe_sub_func_int16_t_s_s(g_48, 4))
    { /* block id: 113 */
        int32_t *l_219 = &g_123;
        int32_t *l_220 = &g_123;
        int32_t *l_221 = &g_123;
        int32_t l_222[6][4][10] = {{{1L,0L,0x91C5D00AL,1L,0xDFECA674L,1L,0x91C5D00AL,0L,1L,0x9FACEB1DL},{0xAC68DCB9L,8L,0xC956EA2FL,0x3FDB6C3EL,(-1L),0x9DE11BDCL,0L,0xFB1311ADL,(-2L),1L},{0xFB1311ADL,0x01672B09L,0L,0x3FDB6C3EL,6L,0xF8A5A028L,0xB0A0D92CL,(-7L),1L,8L},{6L,0xFB1311ADL,0xEAAB0956L,1L,9L,6L,1L,0xF8A5A028L,0xEC0E49BBL,0xA6EAA2CCL}},{{1L,0xF8A5A028L,0xDD955504L,6L,0L,0xDFECA674L,0x1DB001E9L,2L,0xFB1311ADL,0L},{0x06E1C7F3L,0xA490F008L,0xC956EA2FL,0xF8A5A028L,(-6L),(-1L),1L,9L,7L,6L},{(-6L),0x1DB001E9L,0L,0x74FDF766L,(-2L),6L,(-2L),0x962B6702L,0L,(-1L)},{2L,(-6L),9L,0x91C5D00AL,0xE103523AL,0xBA217E51L,0x962B6702L,6L,0x84ECE7D4L,0xE0C9667DL}},{{0L,0xC956EA2FL,0x74FDF766L,0x8DBCCD91L,0x117DD735L,7L,0x728D4A7CL,1L,6L,0x06E1C7F3L},{0xD05821FDL,1L,0xDB0B19E2L,1L,0L,6L,6L,0L,1L,0xDB0B19E2L},{0L,0L,4L,(-6L),(-7L),0xAC68DCB9L,(-7L),1L,0x9A7A5F8AL,0x962B6702L},{0x06E1C7F3L,1L,0x9DE11BDCL,(-2L),0xBA7642D4L,0x74FDF766L,(-7L),0xFF507D8CL,0xA490F008L,6L}},{{0x117DD735L,0L,(-1L),0L,0x962B6702L,(-2L),6L,(-2L),0x74FDF766L,0L},{6L,1L,0xC2981755L,(-7L),0x9FACEB1DL,0L,0x728D4A7CL,0L,(-1L),0L},{0x21574C0DL,0xC956EA2FL,0xCD1A432AL,0xE103523AL,0xD05821FDL,(-1L),0x962B6702L,(-6L),0xBA7642D4L,0L},{0xDB0B19E2L,(-6L),0xA6EAA2CCL,0xEC0E49BBL,0xF8A5A028L,0x287B45A8L,(-2L),(-2L),0x287B45A8L,0xD47A3A9AL}},{{0xE0C9667DL,0x1DB001E9L,(-1L),0xE0C9667DL,0L,0x4602AEC4L,1L,0xE103523AL,4L,0xE9505AA5L},{(-6L),0xA490F008L,0x9DE11BDCL,2L,0xEC0E49BBL,9L,0x1DB001E9L,0x117DD735L,(-9L),1L},{(-8L),0xF8A5A028L,(-9L),0x9DE11BDCL,0L,1L,0x287B45A8L,0L,0x287B45A8L,1L},{0x1DB001E9L,0x21574C0DL,(-1L),0x21574C0DL,0x1DB001E9L,0xC2981755L,0x01672B09L,(-7L),8L,2L}},{{0x8DBCCD91L,1L,0x74FDF766L,0xE103523AL,0x9261D216L,0xDB0B19E2L,0x9FACEB1DL,0xBA7642D4L,0xD47A3A9AL,2L},{9L,0xE103523AL,0x287B45A8L,7L,0x1DB001E9L,1L,1L,0x962B6702L,0x74FDF766L,1L},{0xE103523AL,0L,0L,0xEAAB0956L,0L,0xFB1311ADL,6L,0x9FACEB1DL,(-7L),0x9DE11BDCL},{1L,(-1L),1L,0xD05821FDL,0xDD955504L,(-3L),1L,0x4602AEC4L,8L,0xA490F008L}}};
        int32_t *l_223 = &l_222[4][3][5];
        int32_t *l_224 = &g_123;
        int32_t *l_225 = &l_222[1][2][5];
        int32_t *l_226 = &l_222[1][2][5];
        int32_t *l_227[6] = {&l_222[1][2][5],&l_222[1][2][5],&l_222[1][2][5],&l_222[1][2][5],&l_222[1][2][5],&l_222[1][2][5]};
        int i, j, k;
        l_228--;
    }
    return p_76;
}


/* ------------------------------------------ */
/* 
 * reads : g_115 g_56 g_29 g_111 g_108 g_112 g_17 g_3 g_49 g_167 g_123 g_32 g_176 g_193 g_6 g_191
 * writes: g_115 g_111 g_57 g_32 g_47 g_112 g_6 g_46 g_56 g_123 g_48 g_167 g_193
 */
static uint16_t  func_81(uint64_t  p_82, uint32_t  p_83, uint16_t * p_84)
{ /* block id: 40 */
    uint64_t *l_125 = &g_99;
    int32_t l_154 = 5L;
    int32_t **l_196 = (void*)0;
    int32_t **l_197 = &g_167;
    int32_t **l_198 = (void*)0;
    int32_t *l_199 = &l_154;
    uint64_t *l_202 = &g_99;
    float *l_213 = (void*)0;
    float *l_214 = &g_112[2];
    float *l_215 = &g_47;
    float *l_216 = &g_46;
    for (g_115 = 0; (g_115 <= 6); g_115 += 1)
    { /* block id: 43 */
        uint32_t l_124 = 0x346DDCF8L;
        uint16_t *l_126 = (void*)0;
        uint64_t *l_152 = (void*)0;
        uint8_t *l_153 = &g_115;
        int32_t l_164 = 9L;
        int32_t **l_172 = (void*)0;
        int32_t **l_173 = &g_167;
        if ((l_124 > ((((l_125 = &p_82) != (void*)0) , l_126) == p_84)))
        { /* block id: 45 */
            int16_t l_134[2][8] = {{0x56BEL,1L,1L,0x56BEL,0x68EFL,0x8CBAL,0x68EFL,0x56BEL},{1L,0x68EFL,1L,0x00EFL,0x8CC5L,0x8CC5L,0x00EFL,1L}};
            uint8_t *l_139 = &g_111;
            int32_t l_158 = 2L;
            int32_t l_159 = 0xFE3867A3L;
            int i, j;
            for (g_111 = 1; (g_111 <= 6); g_111 += 1)
            { /* block id: 48 */
                uint64_t l_160 = 0xC589AC8C392C9DD3LL;
                int32_t l_162[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_162[i] = 0L;
                for (p_82 = 0; (p_82 <= 6); p_82 += 1)
                { /* block id: 51 */
                    int16_t l_133[3];
                    uint8_t *l_150 = (void*)0;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_133[i] = 2L;
                    for (g_57 = 0; (g_57 <= 6); g_57 += 1)
                    { /* block id: 54 */
                        if (g_56)
                            break;
                        return (*p_84);
                    }
                    for (g_32 = 5; (g_32 >= 2); g_32 -= 1)
                    { /* block id: 60 */
                        float *l_155 = &g_47;
                        float *l_156 = &g_112[0];
                        float *l_157[4] = {&g_46,&g_46,&g_46,&g_46};
                        int32_t *l_161[5][3][9] = {{{&l_154,&l_158,&g_123,&g_123,&l_154,&g_123,&l_158,&g_3,&l_159},{&g_123,&g_3,&l_154,&l_159,&g_123,&l_154,&l_154,&g_123,&l_159},{&l_159,(void*)0,&l_159,(void*)0,&l_158,&l_158,&l_154,&g_3,&l_154}},{{&l_154,&g_3,&g_123,&g_3,&l_154,&l_159,&g_123,&l_154,&l_154},{&g_123,&l_158,&l_154,(void*)0,&l_154,&l_158,&g_123,&g_123,&l_154},{&g_3,(void*)0,&g_3,&l_159,&l_154,&l_159,&g_3,(void*)0,&g_3}},{{&l_159,&g_3,&l_154,&g_123,&l_159,&l_158,&l_159,&g_123,&l_154},{&g_123,&g_123,&l_158,&g_123,(void*)0,&l_154,&g_3,&l_154,(void*)0},{&l_159,&l_154,&l_159,&l_158,&l_159,&g_123,(void*)0,&g_3,(void*)0}},{{&g_3,&l_159,&l_158,&l_158,&l_159,&g_3,&l_154,&g_123,&l_154},{&g_123,&g_123,&l_154,&l_154,&l_159,&g_3,&l_154,&g_3,(void*)0},{&l_154,&l_158,&g_3,&l_154,(void*)0,(void*)0,&l_154,&g_3,&l_158}},{{&l_159,&g_3,&l_154,&g_3,&l_159,(void*)0,(void*)0,&l_154,(void*)0},{&g_123,&l_158,&g_123,&l_154,&l_154,&l_154,&g_3,&g_3,&l_154},{&l_154,&g_3,&l_159,&g_3,&l_154,&g_3,&l_159,(void*)0,(void*)0}}};
                        int i, j, k;
                        g_46 = (safe_add_func_float_f_f((l_160 = (safe_mul_func_float_f_f(((g_6 = ((safe_sub_func_uint64_t_u_u(l_133[2], l_134[1][2])) , (l_159 = ((g_56 > (l_158 = (safe_sub_func_float_f_f((l_154 = (((*l_156) = ((safe_sub_func_float_f_f(((&g_115 == l_139) != (((*l_155) = (!((((p_83 != (((safe_add_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((~0x60L), (((((l_150 == ((~(&g_49 != l_152)) , l_153)) > (-5L)) & p_83) | 4294967293UL) , l_154))), l_154)) >= l_134[1][2]), (*p_84))) <= g_111) == l_154)) < 1UL) , (-0x1.8p+1)) != g_108[4]))) > p_83)), 0x5.26D78Dp-29)) <= g_112[6])) <= g_17)), 0x3.891D30p-13)))) <= g_3)))) > g_115), l_124))), g_49));
                        l_162[4] |= l_124;
                    }
                }
                for (g_56 = 5; (g_56 >= 0); g_56 -= 1)
                { /* block id: 74 */
                    int32_t *l_163[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_163[i] = (void*)0;
                    l_164 = ((((void*)0 == p_84) , &g_29[0][0]) == (void*)0);
                }
                for (g_123 = 0; (g_123 <= 6); g_123 += 1)
                { /* block id: 79 */
                    return l_164;
                }
            }
            if (p_82)
                continue;
            for (g_48 = 6; (g_48 >= 0); g_48 -= 1)
            { /* block id: 86 */
                int32_t *l_166[4][4][3] = {{{&l_154,&l_159,&l_159},{&g_123,&g_3,&l_159},{&l_154,&g_3,&l_159},{&l_164,&g_3,&g_123}},{{&l_154,&l_159,&l_159},{&g_123,&g_3,&g_123},{&g_3,&l_164,&g_123},{&g_123,&l_164,(void*)0}},{{&g_3,&g_123,&g_123},{&l_159,&l_164,&g_123},{&g_3,&l_164,&g_123},{&g_123,&l_164,(void*)0}},{{&g_3,&g_123,&g_123},{&l_159,&l_164,&g_123},{&g_3,&l_164,&g_123},{&g_123,&l_164,(void*)0}}};
                int32_t **l_165[3][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_166[3][3][2],(void*)0,&l_166[3][3][2],(void*)0,&l_166[3][3][2],(void*)0,&l_166[3][3][2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                int i, j, k;
                g_167 = &g_123;
                l_164 ^= (safe_lshift_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(l_159, ((*g_167) = (65535UL | p_82)))), g_108[4]));
            }
        }
        else
        { /* block id: 91 */
            for (g_123 = 0; g_123 < 8; g_123 += 1)
            {
                g_112[g_123] = 0xF.FD700Ep+34;
            }
            if ((*g_167))
                break;
            (*g_167) = l_164;
        }
        (*l_173) = &l_154;
    }
    for (g_32 = 0; (g_32 >= (-22)); g_32--)
    { /* block id: 100 */
        volatile int32_t * volatile * const  volatile *l_178 = &g_179;
        int32_t *l_182 = (void*)0;
        int32_t *l_183 = &l_154;
        int32_t *l_184 = &g_123;
        int32_t *l_185 = (void*)0;
        int32_t *l_186 = &g_123;
        int32_t *l_187 = &g_123;
        int32_t *l_188 = &l_154;
        int32_t *l_189 = (void*)0;
        int32_t *l_190 = &l_154;
        int32_t *l_192[4][4][4] = {{{&l_154,(void*)0,(void*)0,&l_154},{(void*)0,&l_154,&g_123,&g_3},{(void*)0,&g_123,(void*)0,&g_123},{&l_154,&g_3,&g_123,&g_123}},{{&g_123,&g_123,(void*)0,&g_3},{&g_3,&l_154,(void*)0,&l_154},{&g_123,(void*)0,&g_123,(void*)0},{&l_154,(void*)0,(void*)0,&l_154}},{{(void*)0,&l_154,&g_123,&g_3},{(void*)0,&g_123,(void*)0,&g_123},{&l_154,&g_3,&g_123,&g_123},{&g_123,&g_123,(void*)0,&g_3}},{{&g_3,&l_154,(void*)0,&l_154},{&g_123,(void*)0,&g_123,(void*)0},{&l_154,(void*)0,(void*)0,&l_154},{(void*)0,&l_154,&g_123,&g_3}}};
        int i, j, k;
        l_178 = g_176;
        ++g_193;
    }
    l_199 = ((*l_197) = (void*)0);
    (*l_216) = (safe_add_func_float_f_f(((void*)0 != l_202), ((*l_215) = (((safe_sub_func_float_f_f(p_83, ((safe_mul_func_float_f_f((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(0x1.9p-1, (0xA.E19F66p+0 > (safe_div_func_float_f_f(p_83, g_111))))) <= ((*l_214) = (((p_82 != (0x0.7p-1 >= 0x7.9p+1)) != 0xC.2BD872p-53) == (-0x1.Ep+1)))), 0x6.450A63p+20)), p_82)) == g_6))) < 0x8.D01BA1p-92) == p_83))));
    return g_191;
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_123
 * writes: g_123
 */
static int8_t  func_85(uint32_t  p_86, float  p_87, uint8_t  p_88, int32_t  p_89)
{ /* block id: 37 */
    int32_t *l_122 = &g_123;
    (*l_122) |= g_57;
    return (*l_122);
}


/* ------------------------------------------ */
/* 
 * reads : g_49 g_99 g_29 g_3 g_57 g_108 g_111 g_32 g_17
 * writes: g_99 g_108 g_111 g_115 g_57
 */
static uint32_t  func_90(uint16_t * p_91, int32_t * p_92)
{ /* block id: 18 */
    int32_t *l_95 = (void*)0;
    uint64_t *l_98[5];
    int32_t l_106 = 0xDBF4B608L;
    int16_t *l_107 = &g_108[4];
    uint8_t *l_109 = (void*)0;
    uint8_t *l_110 = &g_111;
    uint8_t *l_113 = (void*)0;
    uint8_t *l_114 = &g_115;
    int32_t *l_116 = &l_106;
    int i;
    for (i = 0; i < 5; i++)
        l_98[i] = &g_99;
    (*l_116) = ((~((p_92 = l_95) != (g_49 , l_95))) > ((*l_114) = ((*l_110) |= (safe_mod_func_uint64_t_u_u((g_99++), ((((*l_107) &= ((safe_div_func_uint8_t_u_u(g_29[0][0], (((*p_91) > (g_29[1][0] , (p_91 == &g_29[2][1]))) ^ (safe_sub_func_uint8_t_u_u(l_106, g_3))))) > g_57)) >= g_3) && 0xBA83BDBBL))))));
    for (g_111 = 0; (g_111 <= 19); g_111 = safe_add_func_uint64_t_u_u(g_111, 2))
    { /* block id: 27 */
        int32_t **l_121 = &l_116;
        for (g_57 = 25; (g_57 >= 6); g_57--)
        { /* block id: 30 */
            return g_57;
        }
        (*l_121) = &l_106;
        return g_32;
    }
    return g_17;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc_bytes (&g_6, sizeof(g_6), "g_6", print_hash_value);
    transparent_crc_bytes (&g_7, sizeof(g_7), "g_7", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_9[i][j], "g_9[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_17, "g_17", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_29[i][j], "g_29[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_45, "g_45", print_hash_value);
    transparent_crc_bytes (&g_46, sizeof(g_46), "g_46", print_hash_value);
    transparent_crc_bytes (&g_47, sizeof(g_47), "g_47", print_hash_value);
    transparent_crc(g_48, "g_48", print_hash_value);
    transparent_crc(g_49, "g_49", print_hash_value);
    transparent_crc(g_56, "g_56", print_hash_value);
    transparent_crc(g_57, "g_57", print_hash_value);
    transparent_crc(g_99, "g_99", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_108[i], "g_108[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_111, "g_111", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc_bytes(&g_112[i], sizeof(g_112[i]), "g_112[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_123, "g_123", print_hash_value);
    transparent_crc(g_181, "g_181", print_hash_value);
    transparent_crc(g_191, "g_191", print_hash_value);
    transparent_crc(g_193, "g_193", print_hash_value);
    transparent_crc(g_255, "g_255", print_hash_value);
    transparent_crc(g_280, "g_280", print_hash_value);
    transparent_crc(g_379, "g_379", print_hash_value);
    transparent_crc(g_436, "g_436", print_hash_value);
    transparent_crc(g_508, "g_508", print_hash_value);
    transparent_crc(g_519, "g_519", print_hash_value);
    transparent_crc(g_521, "g_521", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_529[i], "g_529[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_559, "g_559", print_hash_value);
    transparent_crc(g_560, "g_560", print_hash_value);
    transparent_crc(g_643, "g_643", print_hash_value);
    transparent_crc(g_644, "g_644", print_hash_value);
    transparent_crc(g_646, "g_646", print_hash_value);
    transparent_crc(g_651, "g_651", print_hash_value);
    transparent_crc(g_700, "g_700", print_hash_value);
    transparent_crc(g_701, "g_701", print_hash_value);
    transparent_crc(g_702, "g_702", print_hash_value);
    transparent_crc(g_706, "g_706", print_hash_value);
    transparent_crc(g_707, "g_707", print_hash_value);
    transparent_crc(g_708, "g_708", print_hash_value);
    transparent_crc(g_754, "g_754", print_hash_value);
    transparent_crc(g_758, "g_758", print_hash_value);
    transparent_crc(g_760, "g_760", print_hash_value);
    transparent_crc(g_762, "g_762", print_hash_value);
    transparent_crc(g_854, "g_854", print_hash_value);
    transparent_crc(g_857, "g_857", print_hash_value);
    transparent_crc_bytes (&g_876, sizeof(g_876), "g_876", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_877[i], "g_877[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_960, "g_960", print_hash_value);
    transparent_crc(g_962, "g_962", print_hash_value);
    transparent_crc(g_1010, "g_1010", print_hash_value);
    transparent_crc(g_1011, "g_1011", print_hash_value);
    transparent_crc(g_1061, "g_1061", print_hash_value);
    transparent_crc(g_1140, "g_1140", print_hash_value);
    transparent_crc(g_1146, "g_1146", print_hash_value);
    transparent_crc(g_1276, "g_1276", print_hash_value);
    transparent_crc(g_1335, "g_1335", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1338[i][j][k], "g_1338[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1341, "g_1341", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1345[i], "g_1345[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1495, "g_1495", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1930[i][j][k], "g_1930[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2607, "g_2607", print_hash_value);
    transparent_crc(g_2683, "g_2683", print_hash_value);
    transparent_crc(g_2687, "g_2687", print_hash_value);
    transparent_crc(g_2828, "g_2828", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2871[i], "g_2871[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3050, "g_3050", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 730
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 172
   depth: 2, occurrence: 44
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
   depth: 5, occurrence: 4
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 1
   depth: 19, occurrence: 3
   depth: 20, occurrence: 4
   depth: 23, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 3
   depth: 29, occurrence: 3
   depth: 30, occurrence: 4
   depth: 31, occurrence: 2
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 43, occurrence: 1
   depth: 44, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 577

XXX times a variable address is taken: 1768
XXX times a pointer is dereferenced on RHS: 534
breakdown:
   depth: 1, occurrence: 402
   depth: 2, occurrence: 108
   depth: 3, occurrence: 19
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 402
breakdown:
   depth: 1, occurrence: 337
   depth: 2, occurrence: 54
   depth: 3, occurrence: 9
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 65
XXX times a pointer is compared with address of another variable: 18
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 9330

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1486
   level: 2, occurrence: 405
   level: 3, occurrence: 156
   level: 4, occurrence: 37
   level: 5, occurrence: 24
XXX number of pointers point to pointers: 251
XXX number of pointers point to scalars: 326
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 34.3
XXX average alias set size: 1.71

XXX times a non-volatile is read: 2851
XXX times a non-volatile is write: 1308
XXX times a volatile is read: 11
XXX    times read thru a pointer: 5
XXX times a volatile is write: 5
XXX    times written thru a pointer: 3
XXX times a volatile is available for access: 44
XXX percentage of non-volatile access: 99.6

XXX forward jumps: 1
XXX backward jumps: 6

XXX stmts: 183
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 36
   depth: 1, occurrence: 29
   depth: 2, occurrence: 37
   depth: 3, occurrence: 26
   depth: 4, occurrence: 26
   depth: 5, occurrence: 29

XXX percentage a fresh-made variable is used: 16.3
XXX percentage an existing variable is used: 83.7
********************* end of statistics **********************/


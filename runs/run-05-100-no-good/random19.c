/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3407454546
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   int16_t  f0;
   volatile int64_t  f1;
   volatile int64_t  f2;
   volatile uint32_t  f3;
   volatile uint8_t  f4;
   const volatile int32_t  f5;
};
#pragma pack(pop)

struct S1 {
   uint32_t  f0;
   int64_t  f1;
   signed f2 : 26;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0xB9DA4D07L;
static volatile int32_t g_5 = 0x2759951EL;/* VOLATILE GLOBAL g_5 */
static volatile int32_t g_6 = (-1L);/* VOLATILE GLOBAL g_6 */
static volatile int32_t g_7[1][2] = {{0x20BD2AB6L,0x20BD2AB6L}};
static int32_t g_8 = (-4L);
static int32_t g_12 = (-1L);
static uint32_t g_33 = 0xE384C7D0L;
static uint8_t g_35 = 0UL;
static volatile uint8_t g_37 = 0xE7L;/* VOLATILE GLOBAL g_37 */
static volatile uint8_t *g_36 = &g_37;
static int8_t g_80[8] = {(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L)};
static uint64_t g_97 = 1UL;
static int32_t *g_109 = &g_12;
static int32_t ** volatile g_117 = &g_109;/* VOLATILE GLOBAL g_117 */
static int32_t ** volatile g_118 = &g_109;/* VOLATILE GLOBAL g_118 */
static uint64_t **g_142 = (void*)0;
static uint64_t ***g_141 = &g_142;
static const struct S1 g_148[7] = {{18446744073709551615UL,0xA9E87E310A61F905LL,-7818},{18446744073709551615UL,0xA9E87E310A61F905LL,-7818},{18446744073709551615UL,0xA9E87E310A61F905LL,-7818},{18446744073709551615UL,0xA9E87E310A61F905LL,-7818},{18446744073709551615UL,0xA9E87E310A61F905LL,-7818},{18446744073709551615UL,0xA9E87E310A61F905LL,-7818},{18446744073709551615UL,0xA9E87E310A61F905LL,-7818}};
static uint16_t g_150 = 0x53E7L;
static int64_t g_162 = 0x2C77FF3C2B3F84D8LL;
static float g_169 = 0x0.BAC05Bp-84;
static float * volatile g_168 = &g_169;/* VOLATILE GLOBAL g_168 */
static uint8_t g_178[9][2][1] = {{{0x32L},{0x32L}},{{248UL},{255UL}},{{248UL},{0x32L}},{{0x32L},{248UL}},{{255UL},{248UL}},{{0x32L},{0x32L}},{{248UL},{255UL}},{{248UL},{0x32L}},{{0x32L},{248UL}}};
static struct S1 g_211[8][9] = {{{0x289C804FL,-4L,2431},{1UL,0x8150FF4D6F548918LL,1320},{18446744073709551614UL,0xD237605CBCC48F9FLL,4162},{18446744073709551615UL,-1L,1535},{1UL,0x8150FF4D6F548918LL,1320},{0xE88F52E7L,0x3CEFFE510EADCF1ELL,-2351},{0UL,0x686D0EF2AE6F9C14LL,-2350},{18446744073709551614UL,0xD237605CBCC48F9FLL,4162},{18446744073709551614UL,0xD237605CBCC48F9FLL,4162}},{{1UL,-1L,-4465},{0x1659E81EL,5L,752},{5UL,0x4B68249875E5E2D9LL,-814},{1UL,0x8150FF4D6F548918LL,1320},{5UL,0x4B68249875E5E2D9LL,-814},{0xE79D9D36L,5L,838},{0xE88F52E7L,0x3CEFFE510EADCF1ELL,-2351},{5UL,0x4B68249875E5E2D9LL,-814},{18446744073709551609UL,1L,3142}},{{18446744073709551614UL,0xD237605CBCC48F9FLL,4162},{0x9FED1308L,0x9016659D9380DC62LL,-554},{1UL,0x99E99429188FF261LL,1166},{1UL,0L,-674},{5UL,0x4B68249875E5E2D9LL,-814},{1UL,0x99E99429188FF261LL,1166},{0x1659E81EL,5L,752},{1UL,0x99E99429188FF261LL,1166},{5UL,0x4B68249875E5E2D9LL,-814}},{{18446744073709551614UL,0xD237605CBCC48F9FLL,4162},{5UL,0x4B68249875E5E2D9LL,-814},{5UL,0x4B68249875E5E2D9LL,-814},{18446744073709551614UL,0xD237605CBCC48F9FLL,4162},{0x9FED1308L,0x9016659D9380DC62LL,-554},{1UL,0x99E99429188FF261LL,1166},{1UL,0L,-674},{5UL,0x4B68249875E5E2D9LL,-814},{1UL,0x99E99429188FF261LL,1166}},{{0xE88F52E7L,0x3CEFFE510EADCF1ELL,-2351},{5UL,0x4B68249875E5E2D9LL,-814},{18446744073709551609UL,1L,3142},{1UL,0L,-674},{0xE79D9D36L,5L,838},{0xE79D9D36L,5L,838},{1UL,0L,-674},{18446744073709551609UL,1L,3142},{5UL,0x4B68249875E5E2D9LL,-814}},{{1UL,0L,-674},{0x9FED1308L,0x9016659D9380DC62LL,-554},{18446744073709551609UL,1L,3142},{1UL,0x8150FF4D6F548918LL,1320},{0x9FED1308L,0x9016659D9380DC62LL,-554},{5UL,0x4B68249875E5E2D9LL,-814},{0x1659E81EL,5L,752},{18446744073709551609UL,1L,3142},{18446744073709551609UL,1L,3142}},{{0xE88F52E7L,0x3CEFFE510EADCF1ELL,-2351},{0xE79D9D36L,5L,838},{5UL,0x4B68249875E5E2D9LL,-814},{1UL,0x8150FF4D6F548918LL,1320},{5UL,0x4B68249875E5E2D9LL,-814},{0xE79D9D36L,5L,838},{0xE88F52E7L,0x3CEFFE510EADCF1ELL,-2351},{5UL,0x4B68249875E5E2D9LL,-814},{18446744073709551609UL,1L,3142}},{{18446744073709551614UL,0xD237605CBCC48F9FLL,4162},{0x9FED1308L,0x9016659D9380DC62LL,-554},{1UL,0x99E99429188FF261LL,1166},{1UL,0L,-674},{5UL,0x4B68249875E5E2D9LL,-814},{1UL,0x99E99429188FF261LL,1166},{0x1659E81EL,5L,752},{1UL,0x99E99429188FF261LL,1166},{5UL,0x4B68249875E5E2D9LL,-814}}};
static uint32_t g_220 = 4294967295UL;
static uint64_t g_230 = 0xB9CEF92868C946F7LL;
static int16_t g_238 = (-2L);
static float * volatile g_279 = &g_169;/* VOLATILE GLOBAL g_279 */
static int32_t g_290[8][5][6] = {{{1L,(-6L),0xEA60532FL,0xC63E277BL,0L,0xC63E277BL},{(-1L),(-5L),(-1L),(-6L),1L,0xC63E277BL},{(-1L),0x86EF7DE8L,0xEA60532FL,0x72F012C4L,0xEA60532FL,0x86EF7DE8L},{1L,(-5L),(-5L),0x72F012C4L,0xFC8B555AL,(-6L)},{(-1L),(-6L),1L,(-6L),(-1L),0x86EF7DE8L}},{{(-1L),(-6L),1L,0xC63E277BL,0xFC8B555AL,0xC63E277BL},{1L,(-5L),1L,(-6L),0xEA60532FL,0xC63E277BL},{1L,0x86EF7DE8L,1L,0x72F012C4L,1L,0x86EF7DE8L},{0xEA60532FL,(-5L),1L,0x72F012C4L,0L,(-6L)},{1L,(-6L),(-5L),(-6L),1L,0x86EF7DE8L}},{{1L,(-6L),0xEA60532FL,0xC63E277BL,0L,0xC63E277BL},{(-1L),(-5L),(-1L),(-6L),1L,0xC63E277BL},{(-1L),0x86EF7DE8L,0xEA60532FL,0x72F012C4L,0xEA60532FL,0x86EF7DE8L},{1L,(-5L),(-5L),0x72F012C4L,0xFC8B555AL,(-6L)},{(-1L),(-6L),1L,(-6L),(-1L),0x86EF7DE8L}},{{(-1L),(-6L),1L,0xC63E277BL,0xFC8B555AL,0xC63E277BL},{1L,(-5L),1L,(-6L),0xEA60532FL,0xC63E277BL},{1L,0x86EF7DE8L,1L,0x72F012C4L,1L,0x86EF7DE8L},{0xEA60532FL,(-5L),1L,0x72F012C4L,0L,(-6L)},{1L,(-6L),(-5L),(-6L),1L,0x86EF7DE8L}},{{1L,(-6L),0xEA60532FL,0xC63E277BL,0L,0xC63E277BL},{(-1L),(-5L),(-1L),(-6L),(-1L),0x72F012C4L},{0xEA60532FL,(-6L),1L,(-5L),1L,(-6L)},{(-1L),0x86EF7DE8L,0xFC8B555AL,(-5L),1L,0xC63E277BL},{0xEA60532FL,0xC63E277BL,0L,0xC63E277BL,0xEA60532FL,(-6L)}},{{(-5L),0xC63E277BL,(-1L),0x72F012C4L,1L,0x72F012C4L},{1L,0x86EF7DE8L,1L,0xC63E277BL,1L,0x72F012C4L},{1L,(-6L),(-1L),(-5L),(-1L),(-6L)},{1L,0x86EF7DE8L,0L,(-5L),(-1L),0xC63E277BL},{1L,0xC63E277BL,0xFC8B555AL,0xC63E277BL,1L,(-6L)}},{{1L,0xC63E277BL,1L,0x72F012C4L,(-1L),0x72F012C4L},{(-5L),0x86EF7DE8L,(-5L),0xC63E277BL,(-1L),0x72F012C4L},{0xEA60532FL,(-6L),1L,(-5L),1L,(-6L)},{(-1L),0x86EF7DE8L,0xFC8B555AL,(-5L),1L,0xC63E277BL},{0xEA60532FL,0xC63E277BL,0L,0xC63E277BL,0xEA60532FL,(-6L)}},{{(-5L),0xC63E277BL,(-1L),0x72F012C4L,1L,0x72F012C4L},{1L,0x86EF7DE8L,1L,0xC63E277BL,1L,0x72F012C4L},{1L,(-6L),(-1L),(-5L),(-1L),(-6L)},{1L,0x86EF7DE8L,0L,(-5L),(-1L),0xC63E277BL},{1L,0xC63E277BL,0xFC8B555AL,0xC63E277BL,1L,(-6L)}}};
static uint8_t * const  volatile g_293[2] = {&g_178[0][0][0],&g_178[0][0][0]};
static uint8_t * const  volatile *g_292 = &g_293[0];
static uint8_t * const  volatile * const  volatile *g_291 = &g_292;
static uint8_t * const  volatile * const  volatile ** volatile g_294 = (void*)0;/* VOLATILE GLOBAL g_294 */
static uint8_t * const  volatile * const  volatile ** volatile g_295 = &g_291;/* VOLATILE GLOBAL g_295 */
static struct S0 g_300 = {0xD118L,0xE8F93DBE3D278928LL,0L,0x4BB2AAA4L,0x64L,1L};/* VOLATILE GLOBAL g_300 */
static int32_t **g_306 = &g_109;
static int32_t ***g_305 = &g_306;
static int32_t **** volatile g_304 = &g_305;/* VOLATILE GLOBAL g_304 */
static int8_t g_333 = 1L;
static volatile struct S0 g_337 = {0x616EL,5L,0x4EF3E42780EB62A1LL,4UL,0x91L,6L};/* VOLATILE GLOBAL g_337 */
static volatile int32_t ** volatile * volatile g_351 = (void*)0;/* VOLATILE GLOBAL g_351 */
static const uint64_t g_370[2] = {0xD7D85E815F2CB282LL,0xD7D85E815F2CB282LL};
static struct S0 g_405[1][3][8] = {{{{0xFEDFL,0x2E9D8B5A059E8808LL,-1L,9UL,0xCAL,0x21D1AD1EL},{0xC5D9L,0x0EF71F2B993456D0LL,-1L,0x89462392L,0xFCL,0x235A545EL},{0xFEDFL,0x2E9D8B5A059E8808LL,-1L,9UL,0xCAL,0x21D1AD1EL},{0x9B98L,0x9EF578FA5C16FCF6LL,0x17AA485D9883886ELL,1UL,5UL,8L},{0x40F0L,0L,1L,9UL,1UL,0L},{0x40F0L,0L,1L,9UL,1UL,0L},{0x9B98L,0x9EF578FA5C16FCF6LL,0x17AA485D9883886ELL,1UL,5UL,8L},{0xFEDFL,0x2E9D8B5A059E8808LL,-1L,9UL,0xCAL,0x21D1AD1EL}},{{0xC5D9L,0x0EF71F2B993456D0LL,-1L,0x89462392L,0xFCL,0x235A545EL},{0xC5D9L,0x0EF71F2B993456D0LL,-1L,0x89462392L,0xFCL,0x235A545EL},{0x40F0L,0L,1L,9UL,1UL,0L},{0L,0x7C98DB7E6CE024D8LL,0xD0E1C288ED9C10AELL,1UL,1UL,0L},{-1L,-7L,-5L,1UL,255UL,0x543CEF21L},{0L,0x7C98DB7E6CE024D8LL,0xD0E1C288ED9C10AELL,1UL,1UL,0L},{0x40F0L,0L,1L,9UL,1UL,0L},{0xC5D9L,0x0EF71F2B993456D0LL,-1L,0x89462392L,0xFCL,0x235A545EL}},{{0xC5D9L,0x0EF71F2B993456D0LL,-1L,0x89462392L,0xFCL,0x235A545EL},{0xFEDFL,0x2E9D8B5A059E8808LL,-1L,9UL,0xCAL,0x21D1AD1EL},{0x9B98L,0x9EF578FA5C16FCF6LL,0x17AA485D9883886ELL,1UL,5UL,8L},{0x40F0L,0L,1L,9UL,1UL,0L},{0x40F0L,0L,1L,9UL,1UL,0L},{0x9B98L,0x9EF578FA5C16FCF6LL,0x17AA485D9883886ELL,1UL,5UL,8L},{0xFEDFL,0x2E9D8B5A059E8808LL,-1L,9UL,0xCAL,0x21D1AD1EL},{0xC5D9L,0x0EF71F2B993456D0LL,-1L,0x89462392L,0xFCL,0x235A545EL}}}};
static uint32_t * const g_410 = &g_211[0][0].f0;
static uint32_t * const *g_409 = &g_410;
static struct S1 * volatile g_416 = &g_211[0][0];/* VOLATILE GLOBAL g_416 */
static volatile uint32_t *g_432 = (void*)0;
static volatile uint32_t **g_431 = &g_432;
static int32_t g_441 = 1L;
static uint32_t g_474 = 4294967295UL;
static struct S1 * volatile g_528 = (void*)0;/* VOLATILE GLOBAL g_528 */
static struct S1 * volatile g_529 = &g_211[0][0];/* VOLATILE GLOBAL g_529 */
static int16_t *g_546 = &g_238;
static const int32_t *g_549 = &g_290[7][4][3];
static volatile struct S0 g_614 = {-7L,1L,0x7DBEC23B2F28296DLL,4294967292UL,0UL,-9L};/* VOLATILE GLOBAL g_614 */
static struct S0 g_704 = {0xBFA5L,1L,0x7269488562798055LL,0x8AE70DD8L,248UL,1L};/* VOLATILE GLOBAL g_704 */
static struct S1 *g_709 = &g_211[0][0];
static struct S1 ** volatile g_708 = &g_709;/* VOLATILE GLOBAL g_708 */
static int32_t g_745 = 9L;
static uint8_t g_780[1][7] = {{0xC8L,0xC8L,0xC8L,0xC8L,0xC8L,0xC8L,0xC8L}};
static int64_t g_817[2] = {(-1L),(-1L)};
static uint16_t g_859[4][6] = {{6UL,0xB58BL,0x0AE9L,0x0AE9L,0xB58BL,6UL},{6UL,65530UL,1UL,0x0AE9L,65530UL,0x0AE9L},{6UL,65532UL,6UL,0x0AE9L,65532UL,1UL},{6UL,0xB58BL,0x0AE9L,0x0AE9L,0xB58BL,6UL}};
static uint64_t g_860 = 18446744073709551609UL;
static uint64_t *g_862 = &g_97;
static uint64_t * volatile *g_861[8][9][3] = {{{&g_862,&g_862,&g_862},{&g_862,(void*)0,(void*)0},{&g_862,(void*)0,&g_862},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862},{(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,(void*)0,&g_862},{(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_862,&g_862},{&g_862,(void*)0,&g_862},{&g_862,&g_862,&g_862},{(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862},{(void*)0,(void*)0,(void*)0},{&g_862,&g_862,&g_862},{&g_862,(void*)0,&g_862},{(void*)0,(void*)0,&g_862}},{{&g_862,&g_862,(void*)0},{(void*)0,(void*)0,&g_862},{&g_862,&g_862,(void*)0},{&g_862,(void*)0,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862}},{{(void*)0,&g_862,(void*)0},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862},{&g_862,(void*)0,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862}},{{&g_862,&g_862,&g_862},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,(void*)0,&g_862},{(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,(void*)0,&g_862},{&g_862,(void*)0,(void*)0}},{{&g_862,(void*)0,&g_862},{&g_862,(void*)0,(void*)0},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862},{(void*)0,(void*)0,&g_862},{&g_862,&g_862,&g_862},{(void*)0,&g_862,(void*)0},{&g_862,&g_862,&g_862},{&g_862,&g_862,(void*)0}},{{(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862},{(void*)0,(void*)0,&g_862},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862},{(void*)0,&g_862,(void*)0},{(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,&g_862}},{{&g_862,&g_862,(void*)0},{&g_862,&g_862,(void*)0},{&g_862,&g_862,&g_862},{(void*)0,&g_862,(void*)0},{(void*)0,&g_862,&g_862},{&g_862,(void*)0,&g_862},{&g_862,&g_862,&g_862},{&g_862,&g_862,(void*)0},{&g_862,(void*)0,&g_862}}};
static float * volatile g_933[5] = {&g_169,&g_169,&g_169,&g_169,&g_169};
static float * volatile g_934 = (void*)0;/* VOLATILE GLOBAL g_934 */
static float * const  volatile g_935[4][10] = {{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,(void*)0,&g_169,(void*)0,&g_169,&g_169,&g_169},{(void*)0,&g_169,&g_169,(void*)0,&g_169,&g_169,(void*)0,&g_169,&g_169,(void*)0}};
static volatile struct S0 g_984[1] = {{0xCB66L,0xF7FECC2EAA25AEC2LL,0L,0x8FB46FD4L,0x22L,0x0A6A5FA1L}};
static struct S0 g_987 = {0xDDA5L,-4L,0xCFFCBB7F6F289DAELL,0xEF80FF09L,0x79L,0x36D2BAFAL};/* VOLATILE GLOBAL g_987 */
static const int32_t **g_1002 = (void*)0;


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static int32_t  func_40(uint8_t * p_41, uint8_t * p_42, int32_t * p_43, struct S1  p_44);
static uint8_t * func_45(int16_t  p_46, int64_t  p_47, const int32_t * p_48, uint8_t  p_49, const uint8_t * p_50);
static uint8_t * func_51(int32_t * p_52, uint8_t * p_53, uint8_t * p_54, int16_t  p_55);
static int32_t * func_56(int32_t * p_57, uint8_t  p_58, uint64_t  p_59);
static int32_t * func_60(const int32_t * p_61, uint8_t  p_62, int16_t  p_63, float  p_64, uint8_t * p_65);
static const int32_t  func_70(const uint8_t * p_71, int32_t  p_72, struct S1  p_73);
static uint8_t * func_74(struct S1  p_75, uint64_t  p_76, int32_t * p_77, int32_t  p_78);
static int32_t * func_84(uint8_t  p_85, uint32_t  p_86);
static uint8_t  func_87(uint8_t * p_88, uint32_t  p_89);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_7 g_35 g_36 g_80 g_8 g_97 g_37 g_117 g_118 g_141 g_148 g_142 g_33 g_150 g_12 g_168 g_162 g_178 g_109 g_6 g_211.f2 g_230 g_211 g_220 g_279 g_169 g_291 g_295 g_300 g_304 g_333 g_337 g_305 g_306 g_351 g_370 g_238 g_290 g_405 g_416 g_431 g_441 g_529 g_614 g_410 g_474 g_292 g_293 g_704 g_708 g_709 g_745 g_780 g_817 g_859 g_860 g_861 g_862 g_549 g_546 g_987.f0 g_987.f4
 * writes: g_2 g_8 g_33 g_35 g_12 g_97 g_80 g_109 g_141 g_150 g_162 g_169 g_211 g_220 g_238 g_290 g_291 g_305 g_409 g_230 g_300.f0 g_474 g_333 g_441 g_546 g_549 g_178 g_142 g_709 g_745 g_780 g_817 g_987.f0 g_1002
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int64_t l_16 = 0x594D0357257EA0D0LL;
    int32_t l_27[3][8][7] = {{{0xA0C09020L,0x87F9BD85L,9L,0xE4F6F484L,0L,1L,0xBB6AF833L},{0x8CAFACA8L,0x37FFEAC8L,0x7AD251A6L,(-2L),0x5D57D38CL,0L,0x7AD251A6L},{0x72793645L,0x85B5F732L,(-10L),0x6CF6D707L,1L,0L,0L},{0x7B88C8ECL,0x37FFEAC8L,0x951F1B00L,0x37FFEAC8L,0x7B88C8ECL,(-10L),0xEB1ED309L},{0x45545654L,0x87F9BD85L,1L,(-10L),0xA0C09020L,0x45545654L,0xBA2937BBL},{0x7AD251A6L,7L,0x165D3E8EL,0L,0x5D57D38CL,0xC12A55B9L,0x5D57D38CL},{0x45545654L,(-10L),1L,0L,0x87F9BD85L,1L,(-10L)},{0x7B88C8ECL,0x54656E2EL,0xEB1ED309L,1L,0x6CA241E2L,0x54656E2EL,0xA770AACCL}},{{0x72793645L,0xA0C09020L,9L,(-10L),0x6158E9D2L,0x6158E9D2L,(-10L)},{0x8CAFACA8L,0xBDFAC86EL,0x8CAFACA8L,0xC12A55B9L,0x7AD251A6L,0L,0x5D57D38CL},{0xA0C09020L,0xBB6AF833L,0xE4F6F484L,0x6CF6D707L,0L,1L,0xBA2937BBL},{0x43DEE6EEL,0x37FFEAC8L,0xEB1ED309L,0L,0x722525A5L,0L,0xEB1ED309L},{0x6158E9D2L,0x72793645L,(-10L),0xE4F6F484L,0xA0C09020L,0x6158E9D2L,0L},{0xB395B915L,0xC12A55B9L,0x165D3E8EL,0x37FFEAC8L,0xB395B915L,0x54656E2EL,0x7AD251A6L},{0x45545654L,0xBB6AF833L,1L,0xA0C09020L,0xA0C09020L,1L,0xBB6AF833L},{0x6CA241E2L,7L,0x951F1B00L,1L,0x722525A5L,0xC12A55B9L,0xE769035AL}},{{0L,0xA0C09020L,1L,1L,0L,0x45545654L,(-10L)},{0xB395B915L,(-10L),0x7AD251A6L,1L,0x7AD251A6L,(-10L),0xB395B915L},{0x72793645L,(-10L),0xE4F6F484L,0xA0C09020L,0x6158E9D2L,0L,1L},{0x43DEE6EEL,0xBDFAC86EL,(-1L),0x37FFEAC8L,0x6CA241E2L,0L,0xA770AACCL},{0x45545654L,0x72793645L,0xBB6AF833L,0xBB6AF833L,0x72793645L,0x45545654L,0L},{0x165D3E8EL,(-2L),0x8CAFACA8L,0L,0xB395B915L,1L,0x8CAFACA8L},{1L,(-10L),1L,0x87F9BD85L,0L,1L,(-10L)},{0x6CA241E2L,(-2L),0xE769035AL,(-2L),0x6CA241E2L,0xC12A55B9L,(-1L)}}};
    uint8_t *l_531 = &g_178[3][1][0];
    int8_t l_778 = (-6L);
    uint64_t l_803 = 0x35EFCC589A6050C6LL;
    int32_t l_814 = 0x0A242A77L;
    uint32_t *l_882 = &g_33;
    int32_t ****l_893[1][8] = {{&g_305,&g_305,&g_305,&g_305,&g_305,&g_305,&g_305,&g_305}};
    struct S1 **l_937 = &g_709;
    int64_t l_944 = 0xEC66A04277BFC7C8LL;
    const int16_t l_990 = 0x6184L;
    int8_t l_991 = 0x2DL;
    uint32_t l_994 = 1UL;
    const uint8_t l_995 = 0UL;
    int i, j, k;
    for (g_2 = 0; (g_2 != 5); ++g_2)
    { /* block id: 3 */
        int8_t l_25[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t l_26 = 0x89BE67E7L;
        uint64_t l_28 = 0x6B77D8654EFD0E68LL;
        int32_t *l_82 = &g_12;
        struct S1 l_729 = {0xA41DA778L,0x6206FE42715B8BACLL,-6968};
        uint64_t l_823 = 18446744073709551615UL;
        struct S1 **l_936 = (void*)0;
        uint32_t l_959 = 0xEF07BE6BL;
        int16_t l_978 = 4L;
        int32_t l_982 = 0x86C7ED16L;
        uint16_t l_983[4][2][9] = {{{0x25BCL,65535UL,0x25BCL,65535UL,0x25BCL,65535UL,0x25BCL,65535UL,0x25BCL},{65527UL,0x3B82L,0x3B82L,65527UL,65527UL,0x3B82L,0x3B82L,65527UL,65527UL}},{{0xAA1BL,65535UL,0xAA1BL,65535UL,0xAA1BL,65535UL,0xAA1BL,65535UL,0xAA1BL},{65527UL,65527UL,0x3B82L,0x3B82L,65527UL,65527UL,0x3B82L,0x3B82L,65527UL}},{{0x25BCL,65535UL,0x25BCL,65535UL,0x25BCL,65535UL,0x25BCL,65535UL,0x25BCL},{65527UL,0x3B82L,0x3B82L,65527UL,65527UL,0x3B82L,0x3B82L,65527UL,65527UL}},{{0xAA1BL,65535UL,0xAA1BL,65535UL,0xAA1BL,65535UL,0xAA1BL,65535UL,0xAA1BL},{65527UL,65527UL,0x3B82L,0x3B82L,65527UL,65527UL,0x3B82L,0x3B82L,65527UL}}};
        const struct S0 *l_986 = &g_987;
        const struct S0 **l_985 = &l_986;
        int i, j, k;
        for (g_8 = 0; (g_8 <= (-15)); --g_8)
        { /* block id: 6 */
            int32_t *l_11 = &g_12;
            int32_t *l_13 = (void*)0;
            int32_t *l_14 = &g_12;
            int32_t *l_15 = &g_12;
            int32_t *l_17 = &g_12;
            int32_t *l_18 = &g_12;
            int32_t *l_19 = &g_12;
            int32_t *l_20 = &g_12;
            int32_t *l_21 = &g_12;
            int32_t *l_22 = (void*)0;
            int32_t *l_23 = &g_12;
            int32_t *l_24[10] = {(void*)0,&g_12,(void*)0,(void*)0,&g_12,(void*)0,(void*)0,&g_12,(void*)0,(void*)0};
            uint8_t *l_34 = &g_35;
            uint32_t *l_716 = &g_474;
            uint32_t **l_715[6][9][2] = {{{&l_716,&l_716},{&l_716,&l_716},{&l_716,&l_716},{&l_716,(void*)0},{&l_716,(void*)0},{(void*)0,(void*)0},{&l_716,(void*)0},{&l_716,&l_716},{&l_716,(void*)0}},{{&l_716,(void*)0},{(void*)0,(void*)0},{&l_716,(void*)0},{&l_716,&l_716},{&l_716,&l_716},{&l_716,&l_716},{&l_716,&l_716},{&l_716,(void*)0},{&l_716,(void*)0}},{{(void*)0,(void*)0},{&l_716,(void*)0},{&l_716,&l_716},{&l_716,(void*)0},{&l_716,(void*)0},{(void*)0,(void*)0},{&l_716,(void*)0},{&l_716,&l_716},{&l_716,&l_716}},{{&l_716,&l_716},{&l_716,&l_716},{&l_716,(void*)0},{&l_716,(void*)0},{(void*)0,&l_716},{&l_716,&l_716},{(void*)0,(void*)0},{(void*)0,&l_716},{&l_716,&l_716}},{{&l_716,&l_716},{&l_716,(void*)0},{(void*)0,&l_716},{&l_716,&l_716},{&l_716,&l_716},{&l_716,&l_716},{(void*)0,(void*)0},{&l_716,&l_716},{&l_716,&l_716}},{{&l_716,&l_716},{(void*)0,(void*)0},{(void*)0,&l_716},{&l_716,&l_716},{&l_716,&l_716},{&l_716,(void*)0},{(void*)0,&l_716},{&l_716,&l_716},{&l_716,&l_716}}};
            uint32_t l_718 = 0xF4E04FA3L;
            int8_t l_785 = 0xCEL;
            int64_t l_835[10][10] = {{0x85F1027181E760B9LL,0x85F1027181E760B9LL,0x8EFD78C33B97B139LL,0x035B02DFFE0667B4LL,1L,(-1L),0x27A7847BE737C8CALL,1L,0x09C97A76C37F38C7LL,(-1L)},{0x2A43CBBF44C2A194LL,(-4L),(-3L),0xBC09C8B4149470E4LL,0xF76D8D06B3EC918ELL,0L,0x27A7847BE737C8CALL,0xFF730B86839C624ELL,0L,0x3F8603EE25513D7ELL},{0x43FD5C9457277823LL,0x85F1027181E760B9LL,(-5L),0xB5ADE12816312CCDLL,(-1L),(-5L),0xFF730B86839C624ELL,0x035B02DFFE0667B4LL,(-3L),0L},{1L,(-1L),(-1L),0x066FC0EBD63834D4LL,0x2A43CBBF44C2A194LL,0x09C97A76C37F38C7LL,0x2A43CBBF44C2A194LL,0x066FC0EBD63834D4LL,(-1L),(-1L)},{0x0431E9AEBFB216DBLL,0L,0xFF730B86839C624ELL,0x2A43CBBF44C2A194LL,1L,(-1L),0x035B02DFFE0667B4LL,0x43FD5C9457277823LL,(-1L),1L},{0L,0x0431E9AEBFB216DBLL,(-5L),0L,0xBC09C8B4149470E4LL,(-1L),(-10L),0x85F1027181E760B9LL,1L,0xFF730B86839C624ELL},{0x0431E9AEBFB216DBLL,0x27A7847BE737C8CALL,5L,0x85F1027181E760B9LL,0xDFFB7960E17046C7LL,0x09C97A76C37F38C7LL,0x0431E9AEBFB216DBLL,1L,0x8FB4E564AD1A4FA6LL,0xC12E15DA3CF4F51ELL},{1L,(-1L),(-4L),0x3F8603EE25513D7ELL,(-5L),(-5L),0x3F8603EE25513D7ELL,(-4L),(-1L),1L},{0x43FD5C9457277823LL,0x066FC0EBD63834D4LL,1L,0xF76D8D06B3EC918ELL,0x85F1027181E760B9LL,0L,(-5L),0xC12E15DA3CF4F51ELL,0xFF730B86839C624ELL,0xF76D8D06B3EC918ELL},{0x2A43CBBF44C2A194LL,0x27A7847BE737C8CALL,0L,0x066FC0EBD63834D4LL,0x85F1027181E760B9LL,(-1L),1L,0xDFFB7960E17046C7LL,5L,1L}};
            uint64_t ** const l_850 = (void*)0;
            uint64_t ** const *l_849[6][9][4] = {{{&l_850,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,(void*)0},{&l_850,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850}},{{&l_850,(void*)0,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,(void*)0,&l_850,(void*)0},{&l_850,(void*)0,&l_850,(void*)0},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,(void*)0,&l_850,&l_850}},{{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,(void*)0},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,(void*)0,&l_850,(void*)0},{&l_850,(void*)0,&l_850,(void*)0},{(void*)0,&l_850,&l_850,&l_850}},{{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,(void*)0,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,(void*)0},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850}},{{(void*)0,(void*)0,&l_850,(void*)0},{&l_850,(void*)0,&l_850,(void*)0},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,(void*)0,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,(void*)0}},{{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,(void*)0,&l_850,(void*)0},{&l_850,(void*)0,&l_850,(void*)0},{(void*)0,&l_850,&l_850,&l_850},{&l_850,&l_850,&l_850,&l_850},{(void*)0,&l_850,&l_850,&l_850},{&l_850,(void*)0,&l_850,&l_850}}};
            struct S1 l_873 = {0x37149AA8L,-5L,2921};
            float l_874 = 0x1.63A8D4p+88;
            int i, j, k;
            l_28++;
            if (((g_7[0][1] && (safe_lshift_func_uint8_t_u_s(((*l_34) ^= (g_33 = l_25[6])), 2))) == ((void*)0 != g_36)))
            { /* block id: 10 */
                struct S1 l_79 = {0xA677434CL,0x839EF24C43DCC553LL,-3183};
                int32_t * const l_83[9] = {&g_2,&l_27[2][6][4],&l_27[2][6][4],&g_2,&l_27[2][6][4],&l_27[2][6][4],&g_2,&l_27[2][6][4],&l_27[2][6][4]};
                int32_t *l_551 = &g_2;
                int32_t *** const *l_717 = &g_305;
                int16_t **l_821 = (void*)0;
                struct S0 *l_866[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_866[i] = &g_405[0][1][0];
                if (((*l_11) = (safe_div_func_int32_t_s_s(7L, 0xF05BA845L))))
                { /* block id: 12 */
                    int32_t **l_81[8][5] = {{&l_15,&l_18,&l_23,&l_15,&l_23},{(void*)0,&l_19,(void*)0,&l_19,(void*)0},{&l_23,&l_23,&l_18,&l_14,&l_23},{&l_18,(void*)0,(void*)0,&l_18,&l_22},{&l_23,&l_18,(void*)0,&l_23,&l_23},{(void*)0,&l_18,(void*)0,&l_22,&l_18},{&l_23,&l_18,&l_14,&l_23,&l_14},{&l_21,&l_21,&l_13,&l_18,&l_14}};
                    int16_t *l_237 = &g_238;
                    uint32_t l_532 = 0xAEE9AAD6L;
                    uint8_t *l_550 = (void*)0;
                    int i, j;
                    (*l_23) = (func_40(func_45(((l_34 = func_51(func_56(func_60(&g_8, l_25[6], ((((((*l_237) = (safe_lshift_func_uint8_t_u_s(250UL, (safe_div_func_int32_t_s_s(func_70(func_74(l_79, g_80[6], ((((((&l_27[2][1][5] == &g_8) || ((l_82 = ((1UL == g_7[0][0]) , (void*)0)) == l_83[8])) || g_8) | 0xF2E2L) ^ 0x2BL) , &g_12), g_2), g_178[3][1][0], g_148[0]), g_178[3][1][0]))))) ^ g_148[0].f1) && g_35) & g_230) == g_148[0].f0), l_27[1][7][5], &g_178[3][1][0]), g_178[6][0][0], g_148[0].f1), &g_178[7][1][0], &g_178[3][1][0], g_405[0][2][4].f0)) != l_531), l_532, l_24[8], l_27[2][6][4], &g_178[2][0][0]), l_550, l_551, g_148[0]) ^ l_16);
                    (*l_19) = (*l_551);
                    (*g_306) = (*g_118);
                    (*g_708) = &l_79;
                }
                else
                { /* block id: 341 */
                    return g_405[0][2][4].f5;
                }
                if ((safe_rshift_func_int16_t_s_s(g_35, ((0xC0L < ((g_211[0][0].f1 == (((+(((safe_sub_func_int32_t_s_s((l_715[3][2][1] == (void*)0), ((void*)0 != l_717))) , (0x3F63E61EL ^ 0L)) & g_441)) != l_718) <= g_80[5])) == g_474)) <= l_16))))
                { /* block id: 344 */
                    uint32_t l_744 = 0UL;
                    int32_t l_747 = 1L;
                    int8_t *l_766 = &l_25[6];
                    uint16_t *l_779[2];
                    uint32_t l_822[8] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
                    uint64_t * const l_834 = &g_230;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_779[i] = &g_150;
                    for (g_150 = 29; (g_150 > 47); g_150 = safe_add_func_int16_t_s_s(g_150, 5))
                    { /* block id: 347 */
                        const uint32_t l_742 = 0xAD4F38C2L;
                        uint64_t *l_743[2];
                        int16_t *l_746 = &g_238;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_743[i] = &g_230;
                        g_745 &= (((((g_290[0][0][0] = (safe_sub_func_uint8_t_u_u((+(~((safe_rshift_func_int16_t_s_s(((&l_26 == (((((safe_lshift_func_uint16_t_u_s((((l_729 = (**g_708)) , (safe_mul_func_int16_t_s_s(((((0x7B96L ^ 0x2663L) | ((*l_11) = (((safe_rshift_func_uint16_t_u_s(((safe_mul_func_int8_t_s_s((g_80[7] = g_614.f0), (safe_sub_func_uint32_t_u_u((--(*l_716)), (((g_290[0][0][0] || l_27[0][0][5]) > ((*l_531) = 0xB6L)) == (safe_sub_func_int16_t_s_s(l_742, g_290[5][0][3]))))))) >= l_742), g_162)) || l_742) >= l_16))) || l_744) == g_290[0][0][0]), 0x241AL))) <= 1L), 13)) > l_742) != g_162) || l_744) , (*g_117))) > g_211[0][0].f2), 11)) ^ 0xB41138420372DDC3LL))), 1L))) >= l_744) > 0x4712F4F8L) <= l_27[0][6][5]) || l_744);
                        l_747 ^= ((*l_17) = (((*l_746) = 0L) ^ (l_744 < l_742)));
                    }
                    if ((safe_add_func_uint32_t_u_u(((!(*l_19)) | ((((safe_rshift_func_int8_t_s_s(0x52L, (safe_lshift_func_int8_t_s_u((safe_rshift_func_uint16_t_u_s((l_747 != ((safe_rshift_func_int8_t_s_u(g_178[3][1][0], ((((~(0xE7231A8A9FC5149ALL >= (g_162 = (((-((!((*l_716) = (safe_sub_func_uint16_t_u_u((g_780[0][5] = ((safe_mod_func_uint8_t_u_u(((g_150 = (l_16 != (((*l_766) = 0x55L) , (((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((-9L), 1)), (safe_add_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((!(l_27[0][7][6] = (safe_add_func_uint32_t_u_u(0x3B619126L, (*l_20))))), g_405[0][2][4].f4)), l_778)))) && (-9L)) && g_745)))) | l_16), 0x19L)) ^ g_162)), g_333)))) , (-0x3.6p+1))) <= l_744) , g_300.f1)))) ^ 0xEEEBL) || 0xABD8L) != l_16))) , 0xC808C6DEL)), 5)), 6)))) < (*l_11)) & 0x27L) != 0x40CFL)), g_290[0][2][2])))
                    { /* block id: 365 */
                        const uint32_t l_783 = 4294967295UL;
                        float *l_791 = &g_169;
                        const int16_t l_802 = 1L;
                        int16_t *l_812 = (void*)0;
                        int16_t *l_813 = &g_300.f0;
                        const float *l_816 = (void*)0;
                        const float **l_815 = &l_816;
                        (*l_791) = (((safe_sub_func_uint8_t_u_u((l_783 | l_16), (65531UL == 0L))) < ((~(l_785 | g_337.f5)) , (safe_add_func_int64_t_s_s(g_150, (safe_mul_func_int16_t_s_s((safe_unary_minus_func_uint16_t_u((l_27[2][6][4] , 1UL))), l_747)))))) , (*l_17));
                        g_817[0] &= (((*l_815) = func_84(((*l_34) = ((((*l_11) == ((safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(0x3BFFL, (safe_mod_func_int16_t_s_s(g_35, (safe_div_func_int32_t_s_s((safe_div_func_uint64_t_u_u(((l_802 <= (l_783 != (l_803 ^ ((g_704.f3 ^ ((safe_lshift_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_s(0UL, ((*l_813) &= (((safe_add_func_uint16_t_u_u(((safe_add_func_int8_t_s_s(2L, 0x36L)) && 65529UL), g_780[0][0])) == l_744) , g_333)))) || g_12), l_814)) < l_747)) , l_814)))) & g_474), l_783)), g_290[0][0][0])))))), 255UL)) , (-8L))) == g_290[3][4][3]) != 0L)), l_783)) != (void*)0);
                    }
                    else
                    { /* block id: 371 */
                        uint64_t l_818 = 0x4E7ED09B3893BE45LL;
                        uint64_t *l_832 = &g_97;
                        uint64_t **l_833 = &l_832;
                        int16_t ***l_844 = &l_821;
                        uint64_t *l_848 = &l_28;
                        uint64_t ** const l_847 = &l_848;
                        uint64_t ** const *l_846[5][8][5] = {{{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0},{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0}},{{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0},{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0}},{{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0},{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0}},{{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0},{(void*)0,(void*)0,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847},{(void*)0,&l_847,&l_847,&l_847,(void*)0},{&l_847,&l_847,&l_847,&l_847,&l_847}},{{&l_847,&l_847,&l_847,&l_847,(void*)0},{&l_847,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,(void*)0,(void*)0},{&l_847,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,(void*)0},{&l_847,&l_847,&l_847,&l_847,&l_847},{&l_847,&l_847,&l_847,(void*)0,(void*)0},{&l_847,&l_847,&l_847,&l_847,&l_847}}};
                        uint64_t ** const **l_845[2];
                        int64_t *l_863 = (void*)0;
                        int64_t *l_864 = &l_835[6][7];
                        struct S1 **l_865 = &g_709;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_845[i] = &l_846[1][0][0];
                        (*g_306) = l_17;
                        g_290[0][0][0] &= ((((*l_864) = (((**g_292) = 0xE8L) == (safe_mod_func_uint8_t_u_u(((((safe_add_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u((((safe_lshift_func_int8_t_s_s((((((*l_844) = &g_546) == (void*)0) , ((l_849[0][5][0] = &g_142) == ((safe_mod_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s((l_747 |= (l_27[2][6][4] || (safe_sub_func_int32_t_s_s(((l_16 != (0x02L && ((safe_mul_func_int16_t_s_s((((&g_109 == &g_549) & l_778) , g_333), g_211[0][0].f2)) <= g_859[1][1]))) , l_818), g_860)))), l_27[0][0][5])), g_238)) , (void*)0))) & g_745), g_704.f0)) ^ g_80[6]) && l_822[2]), l_822[2])), (*l_23))) , l_814) , g_861[5][4][2]) == (void*)0), (*l_23))))) > (*g_862)) == g_220);
                        (*l_865) = (*g_708);
                        (*g_709) = l_729;
                    }
                }
                else
                { /* block id: 383 */
                    struct S0 **l_867 = &l_866[1];
                    (*l_867) = l_866[1];
                    for (l_79.f0 = 11; (l_79.f0 < 31); l_79.f0 = safe_add_func_uint64_t_u_u(l_79.f0, 4))
                    { /* block id: 387 */
                        uint8_t l_870 = 249UL;
                        (*g_168) = (*l_18);
                        --l_870;
                    }
                    (*g_709) = l_873;
                }
                (*g_709) = (g_300 , l_873);
                return (*l_551);
            }
            else
            { /* block id: 395 */
                int64_t l_896 = 0x02DB9BDF3193CC09LL;
                uint8_t ** const l_924 = &l_34;
                uint8_t ** const *l_923[6][10][4] = {{{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,(void*)0},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,(void*)0,&l_924},{(void*)0,(void*)0,(void*)0,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{(void*)0,&l_924,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924}},{{&l_924,&l_924,(void*)0,&l_924},{(void*)0,&l_924,(void*)0,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,(void*)0,&l_924},{&l_924,&l_924,(void*)0,&l_924},{&l_924,&l_924,&l_924,&l_924}},{{(void*)0,&l_924,&l_924,(void*)0},{&l_924,(void*)0,&l_924,&l_924},{(void*)0,(void*)0,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,(void*)0},{(void*)0,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924}},{{(void*)0,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,(void*)0},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,(void*)0,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,(void*)0,&l_924},{&l_924,&l_924,&l_924,&l_924}},{{&l_924,&l_924,&l_924,&l_924},{(void*)0,&l_924,&l_924,&l_924},{(void*)0,&l_924,(void*)0,&l_924},{&l_924,&l_924,&l_924,&l_924},{(void*)0,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,&l_924,&l_924,&l_924},{&l_924,&l_924,&l_924,(void*)0}},{{&l_924,&l_924,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,(void*)0,&l_924,&l_924},{&l_924,&l_924,&l_924,(void*)0},{&l_924,&l_924,&l_924,&l_924},{(void*)0,&l_924,&l_924,&l_924},{&l_924,&l_924,(void*)0,&l_924},{(void*)0,(void*)0,&l_924,&l_924},{(void*)0,&l_924,&l_924,(void*)0},{&l_924,&l_924,&l_924,&l_924}}};
                uint8_t ** const ** const l_922 = &l_923[1][4][3];
                uint32_t *l_951 = &g_474;
                int32_t l_956 = 0xE83AB456L;
                int32_t l_958 = (-10L);
                int i, j, k;
                for (g_12 = 0; (g_12 <= (-2)); g_12--)
                { /* block id: 398 */
                    uint64_t l_877[8][1];
                    int64_t *l_891[2][9] = {{&l_835[8][8],&l_835[8][8],&l_835[8][8],&l_835[8][8],&l_835[8][8],&l_835[8][8],&l_835[8][8],&l_835[8][8],&l_835[8][8]},{&l_729.f1,&l_729.f1,&l_729.f1,&l_729.f1,&l_729.f1,&l_729.f1,&l_729.f1,&l_729.f1,&l_729.f1}};
                    int32_t l_892 = 0xE146AFD3L;
                    int8_t *l_897 = &g_80[6];
                    int32_t l_898 = (-1L);
                    int i, j;
                    for (i = 0; i < 8; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_877[i][j] = 0x5B76F3849E8E48FCLL;
                    }
                    if (((((**g_292) = ((l_898 &= (l_877[0][0] != (safe_div_func_int8_t_s_s(((((safe_lshift_func_uint16_t_u_s(((l_882 != (void*)0) , (safe_mul_func_int8_t_s_s(((*l_897) = (((((g_7[0][1] , (safe_mod_func_int64_t_s_s((safe_mul_func_int8_t_s_s(0x92L, (safe_div_func_int8_t_s_s((((l_892 = 0x821F81601B1F2D66LL) || (l_893[0][6] == (((safe_div_func_uint32_t_u_u(2UL, 9UL)) , g_12) , (void*)0))) != (*l_82)), 8UL)))), (*l_19)))) || l_896) == g_220) , 0x6BL) != l_877[0][0])), 1L))), l_896)) >= g_211[0][0].f1) == g_780[0][4]) != l_877[5][0]), (*l_82))))) > 0x784E0737L)) & (*l_23)) | l_896))
                    { /* block id: 403 */
                        int64_t l_903 = 1L;
                        struct S1 **l_904 = &g_709;
                        if ((**g_117))
                            break;
                        l_729.f2 = ((***g_291) ^ 0xCEL);
                        l_903 = (safe_mul_func_uint8_t_u_u((****g_295), (safe_sub_func_uint16_t_u_u(0x0FDEL, g_441))));
                        (*l_904) = (*g_708);
                    }
                    else
                    { /* block id: 408 */
                        uint64_t l_913 = 0x8714583F5BC5B311LL;
                        l_729.f2 = (safe_mul_func_float_f_f((safe_div_func_float_f_f(((safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((((*l_716) = 4294967292UL) || (l_913 = (*g_549))), 4)), 3)) , ((safe_sub_func_uint32_t_u_u(((((safe_mod_func_int64_t_s_s(g_337.f1, (safe_mod_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((l_922 != &l_923[2][7][2]), 65535UL)), ((safe_lshift_func_int8_t_s_u(((*l_82) <= ((l_896 | ((((*l_716)--) | ((safe_sub_func_uint8_t_u_u((safe_mod_func_int8_t_s_s((((*g_709) = (*g_709)) , l_896), g_80[6])), l_896)) != g_780[0][4])) ^ (-1L))) && 0x0EDF69A8L)), l_913)) & 65530UL))))) | (-1L)) && (*l_82)) != g_704.f0), l_892)) , 0xF.277CBAp-39)), 0x1.3p+1)), g_780[0][5]));
                        l_937 = l_936;
                        if ((*g_109))
                            continue;
                        (**g_708) = (*g_529);
                    }
                    return l_896;
                }
                for (g_230 = 11; (g_230 <= 20); g_230 = safe_add_func_uint32_t_u_u(g_230, 5))
                { /* block id: 422 */
                    int16_t l_954 = 0x1739L;
                    int32_t l_955[7][2][6] = {{{0x7D171DB3L,0xE7A98742L,0xE5221578L,1L,(-6L),(-1L)},{1L,(-7L),(-6L),0xE7A98742L,(-1L),(-1L)}},{{0L,0xE5221578L,0xE5221578L,0L,(-4L),0xE7A98742L},{(-1L),(-1L),(-10L),0x8A3209D5L,0x55C65136L,(-6L)}},{{(-10L),1L,(-1L),(-7L),0x55C65136L,(-7L)},{0xACDE8676L,(-1L),0xACDE8676L,0L,(-4L),1L}},{{0xE7A98742L,0xE5221578L,1L,(-6L),(-1L),(-4L)},{0x8A3209D5L,(-7L),0L,(-6L),(-6L),0L}},{{0xE7A98742L,0xE7A98742L,(-1L),0L,1L,0xACDE8676L},{0xACDE8676L,0x55C65136L,0xE7A98742L,(-7L),0xE5221578L,(-1L)}},{{(-10L),0xACDE8676L,0xE7A98742L,0x8A3209D5L,0xE7A98742L,0xACDE8676L},{(-1L),0x8A3209D5L,(-1L),0L,0x7D171DB3L,0L}},{{0L,0x7D171DB3L,0L,0xE7A98742L,(-1L),(-1L)},{0x55C65136L,0xE5221578L,0xACDE8676L,0xACDE8676L,0xE5221578L,0x55C65136L}}};
                    int i, j, k;
                    g_211[0][0].f2 |= (safe_div_func_uint8_t_u_u((((g_405[0][2][4].f0 || g_337.f3) & ((*l_82) | (safe_rshift_func_int16_t_s_s(l_944, 7)))) | ((safe_mul_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u(((255UL || ((safe_rshift_func_int16_t_s_u((((void*)0 != l_951) || (g_162 ^ 1L)), l_896)) <= 0x4274F3F73AD4EF08LL)) , 0x01A9B130F079059CLL), g_704.f0)) | 0x038178BAL), l_896)) | g_745)), l_896));
                    for (l_28 = 0; (l_28 == 6); ++l_28)
                    { /* block id: 426 */
                        float l_957 = 0x8.Bp-1;
                        l_959--;
                        if ((***g_305))
                            break;
                    }
                }
            }
        }
        l_82 = (void*)0;
        for (g_238 = (-12); (g_238 >= (-4)); g_238 = safe_add_func_uint32_t_u_u(g_238, 1))
        { /* block id: 436 */
            uint64_t l_969 = 0x271FADC54A30408BLL;
            uint16_t *l_980 = (void*)0;
            uint16_t *l_981[8][1][8] = {{{&g_859[1][1],&g_859[1][1],&g_859[2][0],&g_859[1][1],&g_859[1][1],&g_859[2][0],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[2][0],&g_859[1][1],&g_859[1][1],&g_859[2][0],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[2][0],&g_859[1][1],&g_859[1][1],&g_859[2][0],&g_859[1][1],&g_859[1][1]}},{{&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1],&g_859[1][1]}}};
            int i, j, k;
        }
    }
    g_8 ^= (((((safe_lshift_func_int16_t_s_s((0x171D1B89L >= l_990), (((l_882 == l_882) , (g_162 &= (((*g_546) = (g_300.f1 && ((**g_306) | (g_2 = (l_991 | (-7L)))))) <= (safe_lshift_func_int16_t_s_s(3L, 11))))) , 1L))) == g_148[0].f0) , 0xBF22L) == l_994) <= g_333);
    (***g_305) &= (l_995 || (safe_rshift_func_uint16_t_u_s(g_405[0][2][4].f1, 5)));
    for (g_987.f0 = 0; (g_987.f0 != 14); g_987.f0 = safe_add_func_int16_t_s_s(g_987.f0, 7))
    { /* block id: 451 */
        const int32_t **l_1000 = (void*)0;
        const int32_t ***l_1001[3][2] = {{(void*)0,&l_1000},{(void*)0,(void*)0},{&l_1000,(void*)0}};
        int i, j;
        g_1002 = l_1000;
        return g_8;
    }
    return g_987.f4;
}


/* ------------------------------------------ */
/* 
 * reads : g_178 g_80 g_8 g_405.f2 g_2 g_300 g_148.f1 g_150 g_211.f2 g_97 g_614 g_117 g_109 g_118 g_337 g_410 g_474 g_306 g_7 g_33 g_211.f1 g_12 g_529 g_211 g_162 g_295 g_291 g_292 g_293 g_333 g_405.f0 g_230 g_290 g_220 g_141 g_142 g_704
 * writes: g_333 g_97 g_178 g_150 g_109 g_211.f0 g_12 g_162 g_211 g_142
 */
static int32_t  func_40(uint8_t * p_41, uint8_t * p_42, int32_t * p_43, struct S1  p_44)
{ /* block id: 272 */
    int32_t l_552 = 6L;
    int32_t l_553 = 0x0C8043A9L;
    int32_t *l_554 = &l_553;
    int32_t *l_555[5];
    int64_t l_556 = 0xFE7CDE46CB9C0316LL;
    uint16_t l_557 = 0UL;
    int8_t *l_570 = &g_333;
    uint64_t ***l_577 = &g_142;
    int i;
    for (i = 0; i < 5; i++)
        l_555[i] = &g_12;
lbl_630:
    l_557--;
    if ((5L | (safe_mod_func_int64_t_s_s(((-7L) || (&g_142 == ((safe_rshift_func_uint8_t_u_u(((safe_sub_func_int8_t_s_s((g_178[3][1][0] & (safe_rshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s(((*l_570) = 0x05L), (((void*)0 != p_43) == ((*l_554) = (((safe_add_func_uint16_t_u_u((g_80[6] == (safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u(p_44.f0, 1)), 0xEDL))), p_44.f2)) || g_8) ^ (*l_554)))))), p_44.f0))), 255UL)) ^ g_405[0][2][4].f2), (*p_41))) , l_577))), 0x024A97C6047D224BLL))))
    { /* block id: 276 */
        float l_578 = 0x5.B2A634p+79;
        uint16_t l_579 = 0x2FC8L;
        l_579--;
        return (*p_43);
    }
    else
    { /* block id: 279 */
        uint8_t ****l_599 = (void*)0;
        int32_t l_607[10][3][3] = {{{0L,(-2L),(-6L)},{0x44D7FA12L,1L,0x4FBB6F6DL},{0x56EB2554L,0x94C58456L,0x56AC50B0L}},{{(-7L),0x46FDF361L,(-7L)},{0x56AC50B0L,0x94C58456L,0x56EB2554L},{0x4FBB6F6DL,1L,0x44D7FA12L}},{{(-6L),(-2L),0L},{0xE5FB6B7CL,0xE5FB6B7CL,0xC235BA34L},{(-6L),0x56AC50B0L,(-2L)}},{{0x4FBB6F6DL,0xC235BA34L,0xA2733531L},{0x56AC50B0L,0x02CB336CL,0x02CB336CL},{(-7L),0x4FBB6F6DL,0xA2733531L}},{{0x56EB2554L,0x32854B60L,(-2L)},{0x44D7FA12L,0x0744D68AL,0xC235BA34L},{0L,0x5F008A19L,0L}},{{0xC235BA34L,0x0744D68AL,0x44D7FA12L},{(-2L),0x32854B60L,0x56EB2554L},{0xA2733531L,0x4FBB6F6DL,(-7L)}},{{0x02CB336CL,0x02CB336CL,0x56AC50B0L},{0xA2733531L,0xC235BA34L,0x4FBB6F6DL},{(-2L),0x56AC50B0L,(-6L)}},{{0xC235BA34L,0xE5FB6B7CL,0xE5FB6B7CL},{0L,(-2L),(-6L)},{0x44D7FA12L,1L,0x4FBB6F6DL}},{{0x56EB2554L,0x94C58456L,0x56AC50B0L},{(-7L),0x46FDF361L,(-7L)},{0x56AC50B0L,0x94C58456L,0x56EB2554L}},{{0x4FBB6F6DL,1L,0x44D7FA12L},{(-6L),(-2L),0L},{0xE5FB6B7CL,0xE5FB6B7CL,0xC235BA34L}}};
        int32_t l_611[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t l_697[3][5];
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 5; j++)
                l_697[i][j] = 0xCEC485A1L;
        }
        for (l_556 = 0; (l_556 >= (-27)); l_556--)
        { /* block id: 282 */
            float l_610[9] = {0x1.6p-1,0x1.6p-1,0x1.6p-1,0x1.6p-1,0x1.6p-1,0x1.6p-1,0x1.6p-1,0x1.6p-1,0x1.6p-1};
            int32_t l_627 = 0L;
            struct S1 l_655 = {18446744073709551608UL,0xC28565BCF967C3A4LL,4352};
            int i;
            for (g_97 = 0; (g_97 <= 4); g_97 += 1)
            { /* block id: 285 */
                int32_t l_584 = 0xF53C582CL;
                uint8_t *l_597 = &g_178[3][1][0];
                uint8_t **l_596 = &l_597;
                uint8_t ***l_595 = &l_596;
                uint8_t ****l_594 = &l_595;
                uint8_t *****l_598 = (void*)0;
                int16_t *l_606[4][9] = {{(void*)0,&g_405[0][2][4].f0,&g_405[0][2][4].f0,(void*)0,(void*)0,(void*)0,&g_405[0][2][4].f0,(void*)0,&g_405[0][2][4].f0},{&g_238,&g_405[0][2][4].f0,&g_300.f0,&g_405[0][2][4].f0,&g_238,&g_238,&g_405[0][2][4].f0,&g_300.f0,&g_405[0][2][4].f0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_238,&g_238,&g_405[0][2][4].f0,&g_300.f0,&g_405[0][2][4].f0,&g_238,&g_238,&g_405[0][2][4].f0,&g_300.f0}};
                struct S1 *l_613 = &g_211[0][0];
                struct S1 **l_612 = &l_613;
                int32_t l_632 = 0x05AE7CC3L;
                int32_t l_633 = 0x98D59FCCL;
                int32_t l_634 = 0x16375BB2L;
                int32_t l_635 = 0xED111049L;
                int32_t l_636 = 0x5C28A15AL;
                uint64_t *l_703 = &g_97;
                uint64_t **l_702 = &l_703;
                int i, j;
                (*l_612) = (l_584 , ((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((safe_sub_func_int64_t_s_s((p_44.f1 = (p_44.f0 ^ (safe_rshift_func_uint8_t_u_s(((**l_596) = (g_300 , (~(&g_291 != (l_599 = l_594))))), 6)))), (safe_lshift_func_uint16_t_u_s(g_148[0].f1, 5)))) > (safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((l_607[3][1][2] = (*l_554)), (g_150++))), 0x7C10L))), (g_211[0][0].f2 , l_611[6]))), g_97)) , (void*)0));
                if ((*p_43))
                { /* block id: 292 */
                    int i;
                    (*g_118) = (g_614 , (l_555[g_97] = (*g_117)));
                }
                else
                { /* block id: 295 */
                    uint16_t *l_621 = (void*)0;
                    uint16_t *l_622 = &g_150;
                    uint16_t *l_623 = &l_557;
                    int32_t l_628 = 0xA4D87F57L;
                    uint64_t l_629[8] = {0x7DF0D014BBF183BALL,0x7DF0D014BBF183BALL,0x0F230A8324700B54LL,0x7DF0D014BBF183BALL,0x7DF0D014BBF183BALL,0x0F230A8324700B54LL,0x7DF0D014BBF183BALL,0x7DF0D014BBF183BALL};
                    uint8_t *l_685 = &g_35;
                    int i;
                    if (((((safe_mod_func_uint64_t_u_u(((((p_44.f2 , (safe_sub_func_uint16_t_u_u(((*l_623) = (((*g_410) = ((g_337 , p_44.f1) <= (p_44.f2 < ((*l_622) = (l_607[3][1][2] = g_300.f0))))) , 0UL)), 1L))) != (((*l_554) = ((((((safe_sub_func_int32_t_s_s((+(l_627 || (g_474 <= l_628))), p_44.f1)) < g_80[2]) < (-1L)) , 0x62118C24D9EF54DELL) <= p_44.f1) > (*p_43))) >= l_611[6])) | p_44.f0) | l_628), 0x54040D2E9A3CC844LL)) & l_629[6]) ^ 0x42L) >= l_611[4]))
                    { /* block id: 301 */
                        if (p_44.f0)
                            goto lbl_630;
                        return l_627;
                    }
                    else
                    { /* block id: 304 */
                        int32_t *l_631 = &l_611[6];
                        uint8_t l_637 = 0x59L;
                        (*g_306) = l_631;
                        --l_637;
                        (*g_306) = (void*)0;
                        g_12 &= (safe_sub_func_int64_t_s_s((safe_rshift_func_int8_t_s_s(((((((l_627 ^ p_44.f0) , (safe_rshift_func_int16_t_s_s((safe_unary_minus_func_int64_t_s(((*p_43) || ((*l_554) = (safe_mul_func_int16_t_s_s((safe_add_func_int64_t_s_s((safe_mul_func_int8_t_s_s((safe_mod_func_int8_t_s_s(((l_655 , 0x03L) , (safe_div_func_uint16_t_u_u((((safe_unary_minus_func_int32_t_s((safe_add_func_int16_t_s_s(g_7[0][1], ((!p_44.f1) > ((l_629[6] , p_44.f2) || p_44.f0)))))) , (*l_554)) , p_44.f2), p_44.f0))), g_33)), (*l_631))), p_44.f1)), (*l_631))))))), 3))) == (*l_631)) || p_44.f1) | (*p_43)) , (*l_631)), 0)), g_211[0][0].f1));
                    }
                    for (g_162 = 7; (g_162 >= 1); g_162 -= 1)
                    { /* block id: 313 */
                        int64_t *l_673 = &l_655.f1;
                        int64_t *l_674 = &g_211[g_162][(g_162 + 1)].f1;
                        int32_t **l_686 = &l_555[2];
                        int i, j;
                        g_211[g_162][(g_162 + 1)] = (*g_529);
                        (*l_686) = ((*g_306) = func_60(&l_552, ((*p_41) = ((safe_rshift_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((((safe_rshift_func_int8_t_s_u((safe_sub_func_int32_t_s_s(((p_44.f2 = ((safe_add_func_int16_t_s_s(((*l_554) = l_632), (((****g_295) == (safe_unary_minus_func_int8_t_s((l_635 = (((*l_674) = ((*l_673) = l_655.f0)) & (safe_add_func_int8_t_s_s((safe_add_func_uint16_t_u_u(((*l_623) = ((safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((*p_41), 3)), ((l_611[6] | ((6UL < (((((safe_rshift_func_int16_t_s_u(6L, l_636)) == l_611[6]) ^ 0xEAAFA5921D6D86E2LL) <= g_333) || g_80[0])) | 0xFADAE7DFA5FA198BLL)) && 0xD95BL))) || g_12)), g_405[0][2][4].f0)), g_474))))))) >= (*p_41)))) == p_44.f2)) > (*p_43)), 0x4CBC981FL)), (*p_41))) >= l_655.f2) && 0x2229145F44E2758CLL) > g_211[g_162][(g_162 + 1)].f0), g_230)), p_44.f0)) , l_627)), p_44.f0, p_44.f1, l_685));
                        (*l_686) = func_60(func_60(&l_552, (((*p_41) < (safe_add_func_uint16_t_u_u((!(safe_mod_func_uint16_t_u_u((safe_add_func_int64_t_s_s(l_611[3], (((-10L) >= (p_44.f0 && (~0x647E5636A9097DCELL))) , ((l_607[3][1][2] >= (l_697[2][4] = (g_290[1][2][3] < ((safe_sub_func_uint16_t_u_u(((*g_529) , g_148[0].f1), p_44.f0)) == g_220)))) || 0xD798F567L)))), p_44.f1))), g_230))) && p_44.f0), l_635, g_333, p_42), l_627, l_632, p_44.f0, &g_178[0][1][0]);
                    }
                    (*g_306) = (*g_306);
                }
                l_607[3][1][2] &= (((safe_div_func_uint64_t_u_u(l_655.f2, (*l_554))) <= ((safe_div_func_int64_t_s_s(p_44.f2, 0x2BCFA00FC2522D85LL)) , ((((l_702 == ((*g_141) = (*g_141))) != ((g_704 , (((****l_599) = (safe_add_func_uint8_t_u_u(0x4CL, (~(l_599 == l_599))))) & 2L)) <= l_697[2][4])) <= p_44.f1) || p_44.f1))) >= (*p_43));
            }
            if (l_655.f0)
                break;
        }
        return l_697[0][0];
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_290 g_405.f3 g_238 g_6 g_211.f2
 * writes: g_290 g_546 g_211.f2 g_549
 */
static uint8_t * func_45(int16_t  p_46, int64_t  p_47, const int32_t * p_48, uint8_t  p_49, const uint8_t * p_50)
{ /* block id: 265 */
    uint32_t l_539 = 0UL;
    int32_t *l_540 = &g_290[6][2][1];
    int64_t *l_541 = &g_162;
    int16_t *l_545 = &g_405[0][2][4].f0;
    int16_t **l_544[3][10] = {{&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545},{&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545},{&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545,&l_545}};
    uint16_t *l_547[5];
    int32_t *l_548[4][7] = {{&g_8,&g_8,(void*)0,(void*)0,(void*)0,&g_8,&g_8},{&g_8,&g_8,(void*)0,&g_12,&g_8,(void*)0,&g_8},{&g_8,&g_8,&g_8,&g_8,(void*)0,&g_12,(void*)0},{&g_8,&g_12,(void*)0,(void*)0,(void*)0,(void*)0,&g_12}};
    int i, j;
    for (i = 0; i < 5; i++)
        l_547[i] = &g_150;
    g_211[0][0].f2 &= ((1UL & p_49) >= (((safe_mul_func_uint8_t_u_u((((safe_rshift_func_uint16_t_u_s((safe_div_func_uint16_t_u_u((((p_47 <= ((*l_540) = l_539)) || ((void*)0 != l_541)) , ((*l_540) = (safe_add_func_uint16_t_u_u((((g_546 = (void*)0) == (void*)0) != 6L), g_290[0][0][0])))), g_405[0][2][4].f3)), g_238)) , g_6) , 3UL), 1UL)) < 0x4E2CL) , 0x53B32DB1L));
    g_549 = p_48;
    return &g_178[1][0][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_416 g_230 g_97 g_37 g_431 g_405.f3 g_290 g_148.f0 g_33 g_441 g_211 g_168 g_169 g_279 g_12 g_333 g_80 g_7 g_370 g_36 g_178 g_238 g_300.f0 g_306 g_117 g_109 g_35 g_529
 * writes: g_211 g_230 g_97 g_12 g_300.f0 g_238 g_220 g_33 g_474 g_169 g_333 g_162 g_150 g_290 g_109 g_441
 */
static uint8_t * func_51(int32_t * p_52, uint8_t * p_53, uint8_t * p_54, int16_t  p_55)
{ /* block id: 175 */
    int32_t *l_411[7];
    uint32_t l_412 = 18446744073709551613UL;
    struct S1 l_415 = {0x72452704L,0L,7857};
    int32_t l_485 = 0x29BAED8CL;
    uint8_t *l_530 = &g_178[1][0][0];
    int i;
    for (i = 0; i < 7; i++)
        l_411[i] = &g_290[2][0][3];
    l_412++;
    (*g_416) = l_415;
    for (g_230 = (-15); (g_230 <= 50); ++g_230)
    { /* block id: 180 */
        uint8_t *l_427 = &g_35;
        int32_t l_435 = 0x15E6D43FL;
        uint32_t l_439 = 0xA8CCEFCAL;
        int32_t l_462 = 4L;
        int32_t l_463[8][9][2] = {{{(-10L),0xB34E902BL},{(-4L),1L},{0xAE4B43ABL,0x3BF303B7L},{0xAE4B43ABL,1L},{(-4L),0xB34E902BL},{(-10L),0x3BF303B7L},{0x36EEE3BFL,5L},{(-4L),5L},{0x36EEE3BFL,0x3BF303B7L}},{{(-10L),0xB34E902BL},{(-4L),1L},{0xAE4B43ABL,0x3BF303B7L},{0xAE4B43ABL,1L},{(-4L),0xB34E902BL},{(-10L),0x3BF303B7L},{0x36EEE3BFL,5L},{(-4L),5L},{0x36EEE3BFL,0x3BF303B7L}},{{(-10L),0xB34E902BL},{(-4L),1L},{0xAE4B43ABL,0x3BF303B7L},{0xAE4B43ABL,1L},{(-4L),0xB34E902BL},{(-10L),0x3BF303B7L},{0x36EEE3BFL,5L},{(-4L),5L},{0x36EEE3BFL,0x3BF303B7L}},{{(-10L),0xB34E902BL},{(-4L),1L},{0xAE4B43ABL,0x3BF303B7L},{0xAE4B43ABL,1L},{(-4L),0xB34E902BL},{(-10L),0x3BF303B7L},{0x36EEE3BFL,5L},{(-4L),5L},{0x36EEE3BFL,0x3BF303B7L}},{{(-10L),0xB34E902BL},{(-4L),1L},{0xAE4B43ABL,0x3BF303B7L},{0xAE4B43ABL,1L},{(-4L),0xB34E902BL},{(-10L),0x3BF303B7L},{0x36EEE3BFL,5L},{(-4L),5L},{0x36EEE3BFL,0x3BF303B7L}},{{(-10L),0xB34E902BL},{(-4L),1L},{0xAE4B43ABL,(-1L)},{(-8L),0x3BF303B7L},{(-8L),1L},{1L,(-1L)},{(-4L),0L},{(-8L),0L},{(-4L),(-1L)}},{{1L,1L},{(-8L),0x3BF303B7L},{(-8L),(-1L)},{(-8L),0x3BF303B7L},{(-8L),1L},{1L,(-1L)},{(-4L),0L},{(-8L),0L},{(-4L),(-1L)}},{{1L,1L},{(-8L),0x3BF303B7L},{(-8L),(-1L)},{(-8L),0x3BF303B7L},{(-8L),1L},{1L,(-1L)},{(-4L),0L},{(-8L),0L},{(-4L),(-1L)}}};
        uint8_t l_464 = 0x9FL;
        uint32_t l_467 = 0x5525967EL;
        uint16_t *l_475 = &g_150;
        int8_t l_496 = 0x3FL;
        int32_t *** const l_527 = &g_306;
        int i, j, k;
        for (g_97 = 0; (g_97 <= 7); g_97 += 1)
        { /* block id: 183 */
            int64_t l_428[1][4][7];
            uint16_t l_429 = 0xB37CL;
            int32_t l_433 = 0xDFD5236AL;
            int32_t *l_438 = &g_290[6][0][5];
            int32_t l_454 = 4L;
            int32_t l_457 = 0x46597E81L;
            int64_t l_459 = 0xCFD2496DDCA573D8LL;
            int32_t l_460 = 0L;
            int32_t l_461[10][7][3] = {{{0L,(-7L),0L},{0L,0xF8A7B10BL,(-2L)},{0xF5893BC0L,(-2L),0xCE19B71EL},{0x6CD42D93L,3L,3L},{0x16136CC2L,(-7L),(-5L)},{0x6CD42D93L,1L,0x4DDDEF37L},{0xF5893BC0L,0x1CCA4534L,0xF4C48109L}},{{0L,(-1L),3L},{0L,0x1CCA4534L,0x16136CC2L},{0xF8A7B10BL,1L,(-2L)},{0x84DC0C2BL,(-7L),0x84DC0C2BL},{0L,3L,(-2L)},{(-1L),(-2L),0x16136CC2L},{0x6CD42D93L,0xF8A7B10BL,3L}},{{0xCE19B71EL,(-7L),0xF4C48109L},{0x6CD42D93L,0L,0x4DDDEF37L},{(-1L),0x1CCA4534L,(-5L)},{0L,0L,3L},{0x84DC0C2BL,0x1CCA4534L,0xCE19B71EL},{0xF8A7B10BL,0L,(-2L)},{0L,(-7L),0L}},{{0L,0xF8A7B10BL,(-2L)},{0xF5893BC0L,(-2L),0xCE19B71EL},{0x6CD42D93L,3L,3L},{0x16136CC2L,(-7L),(-5L)},{0x6CD42D93L,1L,0x4DDDEF37L},{0xF5893BC0L,0x1CCA4534L,0xF4C48109L},{0L,(-1L),3L}},{{0L,0x1CCA4534L,0x16136CC2L},{0xF8A7B10BL,1L,(-2L)},{0x84DC0C2BL,(-7L),0x84DC0C2BL},{0L,3L,(-2L)},{(-1L),(-2L),0x16136CC2L},{0x6CD42D93L,0xF8A7B10BL,3L},{0xCE19B71EL,(-7L),0xF4C48109L}},{{0x6CD42D93L,0L,0x4DDDEF37L},{(-1L),0x1CCA4534L,(-5L)},{0L,0L,3L},{0x84DC0C2BL,0x1CCA4534L,0xCE19B71EL},{0xF8A7B10BL,0L,(-2L)},{0L,(-7L),0L},{0L,0xF8A7B10BL,(-2L)}},{{0xF5893BC0L,(-2L),0xCE19B71EL},{0x6CD42D93L,3L,3L},{0x16136CC2L,(-7L),(-5L)},{0x6CD42D93L,1L,0x4DDDEF37L},{0xF5893BC0L,0x1CCA4534L,0xF4C48109L},{0L,3L,0x4DDDEF37L},{(-5L),3L,0L}},{{0x6CD42D93L,0L,0L},{0xF4C48109L,(-2L),0xF4C48109L},{0xF8A7B10BL,0x4DDDEF37L,0L},{0x16136CC2L,0x1CCA4534L,0L},{0x7EEED8A7L,0x6CD42D93L,0x4DDDEF37L},{0x84DC0C2BL,(-2L),(-9L)},{0x7EEED8A7L,(-1L),(-2L)}},{{0x16136CC2L,3L,0xA94CE4CAL},{0xF8A7B10BL,0xF8A7B10BL,0x4DDDEF37L},{0xF4C48109L,3L,0x84DC0C2BL},{0x6CD42D93L,(-1L),0L},{(-5L),(-2L),(-5L)},{0xF8A7B10BL,0x6CD42D93L,0L},{0xCE19B71EL,0x1CCA4534L,0x84DC0C2BL}},{{0x7EEED8A7L,0x4DDDEF37L,0x4DDDEF37L},{0L,(-2L),0xA94CE4CAL},{0x7EEED8A7L,0L,(-2L)},{0xCE19B71EL,3L,(-9L)},{0xF8A7B10BL,3L,0x4DDDEF37L},{(-5L),3L,0L},{0x6CD42D93L,0L,0L}}};
            uint8_t l_476[1];
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    for (k = 0; k < 7; k++)
                        l_428[i][j][k] = 0x4711B27F4ED5EFBBLL;
                }
            }
            for (i = 0; i < 1; i++)
                l_476[i] = 0xC0L;
            for (g_12 = 2; (g_12 <= 7); g_12 += 1)
            { /* block id: 186 */
                uint32_t l_440 = 0x3DF2D228L;
                int32_t *l_453 = (void*)0;
                int32_t l_456[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_456[i] = (-2L);
                for (g_300.f0 = 0; (g_300.f0 <= 7); g_300.f0 += 1)
                { /* block id: 189 */
                    uint16_t l_423 = 65535UL;
                    int16_t *l_430 = &g_238;
                    int32_t *l_437 = &g_12;
                    int32_t * const *l_445 = &g_109;
                    int32_t * const **l_444[10][1] = {{&l_445},{(void*)0},{&l_445},{(void*)0},{&l_445},{(void*)0},{&l_445},{(void*)0},{&l_445},{(void*)0}};
                    int32_t l_458 = 0xD16E80BBL;
                    int i, j;
                    if (((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u(((l_423 | ((~(l_433 ^= (safe_rshift_func_int16_t_s_u((p_55 & (l_427 != (((((l_428[0][0][5] &= (-3L)) < ((*l_430) = ((l_423 , ((((void*)0 == &g_238) | l_429) ^ (g_37 && 1UL))) ^ l_429))) , (void*)0) != g_431) , (void*)0))), p_55)))) || g_405[0][2][4].f3)) & g_290[0][0][0]), 0)), p_55)) == g_148[0].f0))
                    { /* block id: 193 */
                        uint8_t **l_434 = &l_427;
                        uint32_t *l_436 = &g_220;
                        l_435 = (((*l_434) = p_54) != (void*)0);
                        l_438 = (((*l_436) = 0x9F06D6DAL) , l_437);
                        l_440 |= l_439;
                    }
                    else
                    { /* block id: 199 */
                        return p_53;
                    }
                    for (g_33 = 0; (g_33 <= 6); g_33 += 1)
                    { /* block id: 204 */
                        uint32_t *l_452 = &g_220;
                        int32_t l_455[7][7][5] = {{{0xEAC6DC5DL,0x63B5B553L,0x393E002EL,0x2A3BEC1AL,0xF306ABCFL},{0L,0x9D0CDD38L,0xA2E8FD7EL,0xE5C6EB18L,4L},{0xEA3C16EDL,0x63B5B553L,0x439B70E3L,0x186D5F52L,0x13389BE3L},{0x711B70DEL,0xA2E8FD7EL,(-1L),0x9F5BF237L,0xCE5B5985L},{4L,0xB6D14BA4L,0xBD9F51E2L,0L,0x6CEA97A7L},{1L,3L,4L,0x9D0CDD38L,0x9EF36716L},{4L,0x0D1626BAL,0xEAC6DC5DL,0x0D1626BAL,4L}},{{0x711B70DEL,0xFEF8A76CL,0x9EF36716L,4L,0xA2E8FD7EL},{0xEA3C16EDL,1L,0x47878FD8L,(-3L),0x17B8356BL},{0L,0L,(-10L),0xFEF8A76CL,0xA2E8FD7EL},{0xEAC6DC5DL,(-3L),0x0A96AC54L,(-1L),4L},{0xA2E8FD7EL,(-1L),0x9F5BF237L,0xCE5B5985L,0x9EF36716L},{0L,1L,0xC3BF3500L,0xC7AF61AAL,0x6CEA97A7L},{0x9F5BF237L,(-5L),3L,0xCE5B5985L,0xCE5B5985L}},{{(-2L),0xB60FE3DEL,(-2L),(-1L),0x13389BE3L},{(-5L),1L,0x71D9B614L,0xFEF8A76CL,4L},{0x439B70E3L,0x65880C79L,4L,(-3L),0xF306ABCFL},{0xFEF8A76CL,0xCE5B5985L,0x71D9B614L,4L,0x71D9B614L},{0xC3BF3500L,0x6BC76516L,(-2L),0x0D1626BAL,0x15C780BEL},{0x9EF36716L,0L,3L,0x9D0CDD38L,0x711B70DEL},{0x17B8356BL,0x2A3BEC1AL,0xC3BF3500L,0L,0x47878FD8L}},{{0xE5C6EB18L,0L,0x9F5BF237L,0x9F5BF237L,0L},{0xF306ABCFL,0x6BC76516L,0x0A96AC54L,(-1L),0x15C780BEL},{1L,0x71D9B614L,0xFEF8A76CL,4L,(-10L)},{0x0A96AC54L,0L,0x439B70E3L,(-5L),(-2L)},{1L,(-1L),(-5L),(-1L),1L},{9L,0x65880C79L,(-2L),0x186D5F52L,0x17B8356BL},{4L,0x711B70DEL,0x9F5BF237L,0xA2E8FD7EL,(-1L)}},{{0x5E83600FL,0x0D1626BAL,0L,0x65880C79L,0x17B8356BL},{(-5L),0xA2E8FD7EL,0xA2E8FD7EL,(-5L),1L},{0x17B8356BL,0x6BC76516L,0xEAC6DC5DL,0xC7AF61AAL,(-2L)},{3L,1L,0L,0L,(-10L)},{0xEAC6DC5DL,0xAD6E3870L,0xEA3C16EDL,0xC7AF61AAL,0x15C780BEL},{0x711B70DEL,3L,0x711B70DEL,(-5L),0x9D0CDD38L},{0xF306ABCFL,(-3L),4L,0x65880C79L,0x439B70E3L}},{{0x9EF36716L,0xE5C6EB18L,1L,0xA2E8FD7EL,0xCE5B5985L},{0x393E002EL,0xC7AF61AAL,4L,0x186D5F52L,4L},{0L,0L,0x711B70DEL,(-1L),0L},{(-2L),0x2A3BEC1AL,0xEA3C16EDL,(-5L),9L},{(-1L),(-10L),0L,4L,0x9F5BF237L},{0x47878FD8L,0x2A3BEC1AL,0xEAC6DC5DL,(-1L),0x6CEA97A7L},{0xCE5B5985L,0L,0xA2E8FD7EL,0x9EF36716L,0x71D9B614L}},{{0xBC7F94B2L,0xC7AF61AAL,0L,0x8A8B31EBL,0xBD9F51E2L},{(-1L),0xE5C6EB18L,0x9F5BF237L,(-10L),(-5L)},{0xBC7F94B2L,(-3L),(-2L),(-3L),0xBC7F94B2L},{0xCE5B5985L,3L,(-5L),0x9F5BF237L,0L},{0x47878FD8L,0xAD6E3870L,0x439B70E3L,0x6BC76516L,0x5E83600FL},{(-1L),1L,0xFEF8A76CL,3L,0L},{(-2L),0x6BC76516L,0xC3BF3500L,1L,0xBC7F94B2L}}};
                        int i, j, k;
                        l_453 = func_60(l_411[g_33], g_441, (safe_mod_func_uint8_t_u_u((((void*)0 == l_444[8][0]) , 1UL), ((((safe_rshift_func_int8_t_s_u(p_55, 3)) , (safe_sub_func_int32_t_s_s((safe_rshift_func_uint8_t_u_u(0x62L, 4)), ((*l_452) = (p_55 >= (p_55 , g_148[0].f0)))))) ^ 0xD01495B1L) & l_440))), p_55, l_427);
                        if (p_55)
                            continue;
                        --l_464;
                        l_467++;
                    }
                    if (p_55)
                        continue;
                    for (l_464 = 0; (l_464 <= 6); l_464 += 1)
                    { /* block id: 214 */
                        float *l_473[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_473[i] = (void*)0;
                        (*g_279) = ((!(*g_168)) <= (safe_add_func_float_f_f((g_474 = p_55), ((void*)0 != l_475))));
                    }
                }
                for (l_457 = 0; (l_457 <= 6); l_457 += 1)
                { /* block id: 221 */
                    int i;
                    if ((*l_438))
                        break;
                    if (l_476[0])
                        break;
                    for (l_459 = 5; (l_459 >= 0); l_459 -= 1)
                    { /* block id: 226 */
                        return p_53;
                    }
                }
            }
            for (g_333 = 7; (g_333 >= 0); g_333 -= 1)
            { /* block id: 233 */
                if ((*l_438))
                    break;
            }
        }
        for (g_12 = 0; (g_12 >= 0); g_12 -= 1)
        { /* block id: 239 */
            int8_t *l_483 = (void*)0;
            int64_t *l_494 = &g_162;
            int64_t *l_495 = &l_415.f1;
            uint32_t *l_510 = &l_415.f0;
            uint32_t **l_509 = &l_510;
            float *l_511[9][8] = {{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169},{&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169,&g_169}};
            int32_t l_512 = (-9L);
            int16_t *l_513 = &g_300.f0;
            int32_t ***l_524 = &g_306;
            int i, j;
            g_290[1][0][5] ^= (l_462 = ((safe_add_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u((g_80[(g_12 + 5)] >= (safe_div_func_int8_t_s_s(7L, (l_435 = g_7[g_12][(g_12 + 1)])))), (!g_370[1]))) , (l_485 > (safe_div_func_int32_t_s_s((((safe_mod_func_uint16_t_u_u(((*l_475) = (((*l_495) |= (g_7[g_12][(g_12 + 1)] | ((safe_sub_func_uint16_t_u_u((safe_sub_func_int64_t_s_s(((*l_494) = p_55), g_80[(g_12 + 6)])), 0x5B2AL)) == l_462))) , p_55)), l_496)) ^ 1UL) >= p_55), 0x9934F174L)))), g_441)) , 0x66F5F7CAL));
            (*g_306) = func_60((((void*)0 == p_52) , p_52), (((*l_513) ^= (g_238 |= (safe_mod_func_int16_t_s_s(0L, ((l_463[0][1][1] &= (safe_mod_func_int8_t_s_s((safe_div_func_int64_t_s_s((safe_lshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u((*g_36), (((((l_512 = (safe_add_func_float_f_f((((((*l_509) = p_52) != p_52) <= (g_178[3][1][0] == p_55)) > 0x6.03360Fp-89), 0xD.F8DD96p-43))) >= (-0x1.2p+1)) >= 0x4.BA6274p-34) , 0x71L) ^ 248UL))), g_230)), l_467)), p_55))) || (*p_53)))))) >= g_290[0][0][0]), p_55, l_467, &g_35);
            for (g_441 = 3; (g_441 >= 0); g_441 -= 1)
            { /* block id: 254 */
                int32_t ****l_525 = &g_305;
                int32_t ****l_526[7][8] = {{&l_524,&l_524,&l_524,&g_305,&g_305,&g_305,&g_305,&g_305},{&g_305,&g_305,&g_305,&g_305,&g_305,&g_305,&g_305,&l_524},{&l_524,&l_524,&l_524,(void*)0,&g_305,(void*)0,&g_305,&g_305},{&l_524,&l_524,&g_305,(void*)0,&g_305,&l_524,&g_305,&l_524},{&g_305,&g_305,&g_305,&g_305,&g_305,&l_524,&g_305,&g_305},{&g_305,&l_524,(void*)0,&g_305,&g_305,&l_524,&g_305,&g_305},{&g_305,&g_305,(void*)0,&l_524,&g_305,&g_305,&g_305,&g_305}};
                int i, j, k;
                (*g_306) = func_60((*g_117), g_290[(g_441 + 3)][(g_441 + 1)][(g_441 + 2)], (safe_add_func_int64_t_s_s((safe_div_func_uint16_t_u_u((g_211[0][0].f0 <= (((safe_div_func_int16_t_s_s((safe_div_func_int16_t_s_s(g_148[0].f0, ((*l_513) = (safe_lshift_func_int8_t_s_s((((l_524 = l_524) == l_527) >= ((((void*)0 != &g_150) == 0xD10C8E0BL) >= l_512)), g_300.f0))))), 0x6727L)) , p_55) ^ 0xB65B6D0BL)), 0x01B4L)), 0xC41F6534FEE1058DLL)), g_35, p_54);
            }
        }
        (*g_529) = (*g_416);
        return l_530;
    }
    return &g_178[3][0][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_8 g_12 g_7 g_80 g_211.f1 g_220 g_211 g_279 g_169 g_168 g_291 g_295 g_300 g_162 g_148.f1 g_33 g_117 g_109 g_304 g_150 g_333 g_337 g_305 g_306 g_118 g_351 g_370 g_35 g_238 g_6 g_290 g_405
 * writes: g_80 g_162 g_211.f1 g_220 g_169 g_12 g_290 g_291 g_211 g_33 g_305 g_150 g_238 g_109 g_409
 */
static int32_t * func_56(int32_t * p_57, uint8_t  p_58, uint64_t  p_59)
{ /* block id: 76 */
    int32_t *l_240 = &g_12;
    int32_t *l_241 = &g_12;
    int32_t *l_242 = (void*)0;
    int32_t *l_243[10] = {&g_12,(void*)0,(void*)0,(void*)0,&g_12,&g_12,(void*)0,(void*)0,(void*)0,&g_12};
    int32_t l_244 = 0xE867B30FL;
    float l_245 = 0x9.4A2A38p+76;
    int16_t l_246 = 0x33F7L;
    uint64_t l_247 = 0x052ACF594EB2906ALL;
    int8_t *l_262[9][7] = {{(void*)0,(void*)0,&g_80[7],(void*)0,(void*)0,&g_80[7],(void*)0},{&g_80[1],&g_80[6],&g_80[6],&g_80[1],&g_80[6],&g_80[6],&g_80[1]},{&g_80[5],(void*)0,&g_80[5],&g_80[5],(void*)0,&g_80[5],&g_80[5]},{&g_80[1],&g_80[1],&g_80[6],&g_80[1],&g_80[1],&g_80[6],&g_80[1]},{(void*)0,&g_80[5],&g_80[5],(void*)0,&g_80[5],&g_80[5],(void*)0},{&g_80[6],&g_80[1],&g_80[6],&g_80[6],&g_80[1],&g_80[6],&g_80[6]},{(void*)0,(void*)0,&g_80[7],(void*)0,(void*)0,&g_80[7],(void*)0},{&g_80[1],&g_80[6],&g_80[6],&g_80[1],&g_80[6],&g_80[6],&g_80[1]},{&g_80[5],(void*)0,&g_80[5],&g_80[5],(void*)0,&g_80[5],&g_80[5]}};
    int64_t *l_269 = &g_162;
    int64_t *l_270[1];
    uint64_t ***l_275[3];
    uint32_t l_276 = 0xD129D965L;
    uint32_t *l_277 = &g_220;
    uint32_t l_278 = 1UL;
    struct S1 *l_280 = &g_211[3][2];
    int32_t **l_303[5][3] = {{&l_243[6],&l_243[6],&l_243[6]},{&g_109,&g_109,&g_109},{&l_243[6],&l_243[6],&l_243[6]},{&g_109,&g_109,&g_109},{&l_243[6],&l_243[6],&l_243[6]}};
    int32_t ***l_302 = &l_303[0][2];
    int8_t l_309 = 1L;
    uint32_t l_323 = 0x4C886D2CL;
    int i, j;
    for (i = 0; i < 1; i++)
        l_270[i] = &g_211[0][0].f1;
    for (i = 0; i < 3; i++)
        l_275[i] = &g_142;
    l_247--;
    if (((safe_rshift_func_int16_t_s_s(((safe_add_func_int8_t_s_s(((*p_57) >= (safe_sub_func_int16_t_s_s((*l_240), (0UL | 4L)))), ((((*l_277) &= ((((safe_rshift_func_int16_t_s_u((safe_div_func_int8_t_s_s((-1L), (g_80[3] &= g_7[0][1]))), ((safe_rshift_func_int16_t_s_s((safe_add_func_int8_t_s_s(g_8, (safe_mod_func_int64_t_s_s((g_211[0][0].f1 ^= ((*l_269) = p_59)), (safe_mod_func_int64_t_s_s((safe_lshift_func_int8_t_s_s((l_275[2] != (void*)0), 4)), l_276)))))), 7)) ^ 1L))) , p_58) | 0x06B6CE06L) == (*l_241))) >= 0x5B1FA440L) & 0L))) && l_278), 15)) > (*l_241)))
    { /* block id: 82 */
        uint8_t *l_285 = &g_35;
        uint8_t **l_284 = &l_285;
        uint8_t *l_287 = &g_178[3][1][0];
        uint8_t **l_286 = &l_287;
        uint32_t **l_288 = &l_277;
        float *l_289[1][7];
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 7; j++)
                l_289[i][j] = &g_169;
        }
        (*g_279) = ((g_211[7][7] , p_59) == p_59);
        (*l_241) = 0x3D3DFE0FL;
        g_290[0][0][0] = (((*g_168) = ((l_280 == l_280) >= ((safe_unary_minus_func_uint8_t_u((g_211[0][0].f2 | 0L))) , (((+(+(*g_279))) <= (((*l_286) = ((*l_284) = l_262[1][2])) == &g_35)) != (((*l_288) = &g_220) != (void*)0))))) <= p_59);
        (*g_295) = (((l_288 != (void*)0) , (*l_240)) , g_291);
    }
    else
    { /* block id: 91 */
        uint32_t *l_296 = (void*)0;
        uint32_t **l_297 = &l_277;
        int32_t l_301 = 0x5BC5F2CBL;
        int32_t l_308 = 0x125AE986L;
        int32_t l_310 = (-8L);
        int32_t l_311 = 0xA4E99E25L;
        int32_t l_313 = 0x9E28F916L;
        int32_t l_314 = 0xA99EF825L;
        int32_t l_315[5][4][7] = {{{0xA6CE6C36L,0xA8F50893L,1L,(-1L),0x1C781FFBL,0xFD428D75L,0xFD428D75L},{0x39B853AAL,0x52B811C7L,(-1L),0x52B811C7L,0x39B853AAL,0xC42C5ECDL,0x1C781FFBL},{0L,0xEAD3A62DL,0x292A1E58L,(-1L),0x70E43FECL,1L,(-8L)},{0x1C781FFBL,(-1L),0x983F3DA2L,(-1L),0xEAD3A62DL,0x5E8AA972L,0xEB88B96EL}},{{0L,(-1L),0x3003008EL,(-6L),(-1L),(-1L),8L},{1L,0x1C781FFBL,0x91F3F97BL,0xEAD3A62DL,(-1L),1L,5L},{(-1L),(-1L),(-1L),0x292A1E58L,5L,1L,7L},{0L,(-1L),0x7C967F03L,0x292A1E58L,0L,(-2L),0L}},{{0xF6269C12L,0xF4922FB7L,0xB503DA07L,0xEAD3A62DL,0xFD428D75L,0L,0xFD428D75L},{(-2L),1L,1L,(-2L),0xC42C5ECDL,0xBA70BE18L,0x292A1E58L},{0x72BF05E4L,1L,7L,(-1L),1L,(-1L),1L},{(-1L),0x91F3F97BL,0xEB88B96EL,(-6L),0x5E8AA972L,0x52B811C7L,0x292A1E58L}},{{0x7C967F03L,0L,(-1L),0x91F3F97BL,0L,0x983F3DA2L,0xFD428D75L},{7L,0x39B853AAL,2L,(-2L),0xC52FBDF9L,(-6L),0L},{0xDC9B0EAFL,1L,0xF6269C12L,(-1L),0x39B853AAL,0xD85B41F0L,7L},{(-1L),0x3003008EL,0xF6269C12L,(-1L),(-6L),(-1L),5L}},{{1L,0xF6269C12L,2L,8L,1L,1L,8L},{(-1L),(-1L),(-1L),0x292A1E58L,8L,0x7C967F03L,0xA6CE6C36L},{0x91F3F97BL,(-1L),0xEB88B96EL,(-8L),0L,0xF6269C12L,0L},{(-2L),(-6L),7L,(-1L),0xFD428D75L,0x7C967F03L,1L}}};
        int32_t ***l_399 = &l_303[0][2];
        int8_t l_400[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
        struct S1 l_404 = {1UL,0xC58335E80B8834E9LL,5546};
        int i, j, k;
        if (((((*l_297) = l_296) != ((0x6BL ^ p_58) , func_60(p_57, (safe_rshift_func_int8_t_s_u((g_300 , (65535UL & g_162)), 3)), p_58, g_148[0].f1, &g_178[3][1][0]))) ^ l_301))
        { /* block id: 93 */
            struct S1 l_307 = {1UL,-9L,3036};
            for (l_244 = 0; (l_244 <= 0); l_244 += 1)
            { /* block id: 96 */
                for (g_33 = 0; (g_33 <= 0); g_33 += 1)
                { /* block id: 99 */
                    int i, j;
                    if (g_7[l_244][(g_33 + 1)])
                        break;
                    return (*g_117);
                }
                (*g_304) = l_302;
                for (g_150 = 0; (g_150 <= 0); g_150 += 1)
                { /* block id: 106 */
                    return p_57;
                }
            }
            (*l_280) = l_307;
            (*l_280) = l_307;
            (*l_240) ^= 0x25CABD7EL;
        }
        else
        { /* block id: 113 */
            float l_312 = 0x9.Ep+1;
            int32_t l_316 = 0L;
            int32_t l_317 = 0xCA33B9B0L;
            int32_t l_318 = 0xC1D7F27EL;
            int32_t l_319 = 0xA5D215F1L;
            int32_t l_320 = (-10L);
            int32_t l_321 = 0x2E972767L;
            int32_t l_322 = 0xBEBA8F66L;
            struct S1 l_403 = {0UL,1L,-9};
            uint32_t * const l_407 = &l_276;
            uint32_t * const *l_406 = &l_407;
lbl_402:
            l_323++;
            for (g_238 = 0; (g_238 == 25); g_238 = safe_add_func_uint32_t_u_u(g_238, 3))
            { /* block id: 117 */
                uint64_t l_328 = 0x57F7A5D91100AEC0LL;
                for (l_311 = 2; (l_311 >= 0); l_311 -= 1)
                { /* block id: 120 */
                    if ((*p_57))
                        break;
                    --l_328;
                    return p_57;
                }
                if (l_315[4][2][5])
                    continue;
                for (p_58 = 1; (p_58 <= 7); p_58 += 1)
                { /* block id: 128 */
                    for (l_247 = 0; (l_247 <= 7); l_247 += 1)
                    { /* block id: 131 */
                        int8_t l_336 = 0xF3L;
                        (*l_241) ^= (safe_rshift_func_uint8_t_u_s(g_333, 4));
                        (*l_280) = (p_59 , ((p_59 | (safe_mod_func_uint16_t_u_u(0UL, l_336))) , (g_337 , g_211[7][1])));
                    }
                    return (***g_304);
                }
            }
            (*l_240) ^= (safe_lshift_func_uint16_t_u_u(g_148[0].f1, 13));
            for (g_12 = 0; (g_12 < (-10)); g_12 = safe_sub_func_uint16_t_u_u(g_12, 3))
            { /* block id: 141 */
                int32_t l_359 = 0L;
                (*g_306) = (*g_118);
                for (l_278 = 16; (l_278 <= 52); l_278++)
                { /* block id: 145 */
                    struct S1 **l_358 = &l_280;
                    int32_t l_362[7][2] = {{0x95D8A33CL,0x95D8A33CL},{0x95D8A33CL,0x95D8A33CL},{0x95D8A33CL,0x95D8A33CL},{0x95D8A33CL,0x95D8A33CL},{0x95D8A33CL,0x95D8A33CL},{0x95D8A33CL,0x95D8A33CL},{0x95D8A33CL,0x95D8A33CL}};
                    const uint64_t *l_369 = &g_370[1];
                    int8_t l_395 = 1L;
                    uint32_t l_397 = 0x60A36105L;
                    int32_t l_401 = 0x83EAA215L;
                    int i, j;
                    for (l_246 = 1; (l_246 <= 9); l_246 += 1)
                    { /* block id: 148 */
                        int32_t l_357 = 0x74EF8BDCL;
                        int i;
                        l_357 |= ((safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(0x0318L, (g_333 , (safe_rshift_func_uint16_t_u_u(((+(g_351 != (void*)0)) ^ (!((safe_div_func_int32_t_s_s((((safe_div_func_uint32_t_u_u((l_320 & (g_12 != ((((-7L) ^ (((0x8AF836D3L > (*p_57)) >= p_58) ^ l_321)) || 0x453C175AL) && 0xA77AAFB623810E5ALL))), p_59)) != (-8L)) , 0xE1DD00A6L), (-2L))) ^ p_59))), 2))))), p_59)) , 0L);
                    }
                    (*l_358) = &g_211[0][0];
                    l_359 ^= 0x9EE5B194L;
                    if (((safe_lshift_func_int8_t_s_u((g_80[7] = (l_362[6][0] <= (((l_308 = p_59) | ((p_58 | ((safe_div_func_uint32_t_u_u((safe_add_func_uint8_t_u_u((safe_add_func_uint16_t_u_u(1UL, 0xC83FL)), p_58)), l_362[6][0])) ^ (((l_369 = &p_59) != (void*)0) == l_314))) | 1UL)) >= g_162))), p_58)) && 9UL))
                    { /* block id: 156 */
                        return p_57;
                    }
                    else
                    { /* block id: 158 */
                        uint32_t *l_371 = (void*)0;
                        uint32_t *l_372 = &g_211[0][0].f0;
                        float *l_393 = (void*)0;
                        float *l_394 = &g_169;
                        float *l_396 = &l_245;
                        uint16_t *l_398 = &g_150;
                        uint32_t * const **l_408[2][8];
                        int i, j;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 8; j++)
                                l_408[i][j] = &l_406;
                        }
                        l_401 = (l_362[3][0] = ((((((*l_372)--) , p_59) && (safe_mul_func_uint8_t_u_u(p_59, ((safe_sub_func_uint32_t_u_u(g_300.f0, (((safe_mul_func_int8_t_s_s((((((p_59 == g_370[1]) <= (g_35 < ((*l_398) = ((l_314 = (safe_sub_func_int16_t_s_s((((safe_mul_func_int16_t_s_s((((safe_mul_func_uint8_t_u_u(((((*l_396) = (((((safe_add_func_float_f_f(((safe_div_func_float_f_f(l_359, ((*l_394) = (safe_sub_func_float_f_f((*g_279), 0x1.DAF607p+81))))) >= p_59), g_162)) <= l_362[6][0]) == l_395) <= (-0x1.Ap+1)) >= 0x9.F8796Bp-37)) , p_59) , 0x2BL), l_315[4][2][5])) , 0x65L) == 0x5EL), l_318)) ^ 0x4BL) == l_397), g_238))) || g_300.f0)))) != 0UL) , l_399) != &l_303[0][2]), (-3L))) ^ l_400[4]) >= p_59))) || 0x5898E8FEL)))) || g_6) < g_290[7][2][0]));
                        if (g_33)
                            goto lbl_402;
                        l_404 = l_403;
                        g_409 = (g_405[0][2][4] , l_406);
                    }
                }
            }
        }
    }
    return (*g_118);
}


/* ------------------------------------------ */
/* 
 * reads : g_211
 * writes: g_211
 */
static int32_t * func_60(const int32_t * p_61, uint8_t  p_62, int16_t  p_63, float  p_64, uint8_t * p_65)
{ /* block id: 73 */
    int32_t *l_239 = &g_8;
    g_211[0][0] = g_211[0][0];
    return l_239;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int32_t  func_70(const uint8_t * p_71, int32_t  p_72, struct S1  p_73)
{ /* block id: 70 */
    const uint8_t l_236 = 255UL;
    return l_236;
}


/* ------------------------------------------ */
/* 
 * reads : g_97 g_80 g_2 g_35 g_36 g_37 g_33 g_117 g_118 g_141 g_148 g_142 g_150 g_12 g_168 g_162 g_178 g_109 g_6 g_211.f2
 * writes: g_97 g_80 g_109 g_35 g_33 g_141 g_150 g_162 g_12 g_169 g_211 g_220
 */
static uint8_t * func_74(struct S1  p_75, uint64_t  p_76, int32_t * p_77, int32_t  p_78)
{ /* block id: 14 */
    uint64_t *l_96 = &g_97;
    uint8_t *l_100 = &g_35;
    int32_t l_101[10][3] = {{(-7L),0x33429DD6L,0xE8F17059L},{(-7L),(-7L),0x33429DD6L},{(-1L),0x33429DD6L,0x33429DD6L},{0x33429DD6L,0x09EFF974L,0xE8F17059L},{(-1L),0x09EFF974L,(-1L)},{(-7L),0x33429DD6L,0xE8F17059L},{(-7L),(-7L),0x33429DD6L},{(-1L),0x33429DD6L,0x33429DD6L},{0x33429DD6L,0x09EFF974L,0xE8F17059L},{(-1L),0x09EFF974L,(-1L)}};
    int32_t *l_108 = &g_2;
    int32_t **l_107[2][5];
    uint32_t *l_219 = &g_220;
    uint64_t *l_229[9][8] = {{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0},{(void*)0,&g_230,&g_230,(void*)0,(void*)0,&g_230,&g_230,(void*)0}};
    uint32_t l_231 = 2UL;
    int16_t l_232 = 9L;
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
            l_107[i][j] = &l_108;
    }
    (*g_118) = (p_77 = func_84((func_87(&g_35, (safe_rshift_func_uint8_t_u_s(((p_76 <= (safe_mod_func_uint8_t_u_u(((*l_100) |= (safe_mod_func_int8_t_s_s(((l_101[9][1] = (0x45225C75L >= (&g_37 != (((*l_96)--) , l_100)))) , (((safe_unary_minus_func_uint16_t_u((((safe_sub_func_uint64_t_u_u((((9L != (g_80[6] ^= 0xB4L)) , ((safe_mul_func_int8_t_s_s((((g_109 = (void*)0) == &g_5) || p_75.f1), p_76)) > (-1L))) > p_78), 7UL)) , p_75) , 0xAA49L))) || 0x4A9EDB9B1DFB6871LL) ^ p_75.f1)), (*l_108)))), 0xA8L))) > p_78), 7))) == g_2), p_75.f2));
    p_75.f2 = (l_232 |= (safe_add_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(p_75.f2, (safe_unary_minus_func_int8_t_s((-1L))))), (((safe_mod_func_uint64_t_u_u(((*l_96) ^= p_76), ((((*l_219) = p_78) <= (safe_sub_func_int32_t_s_s(0x40D8B7E6L, (g_6 & (safe_add_func_int32_t_s_s(3L, (safe_mul_func_int16_t_s_s(((safe_mul_func_int16_t_s_s(g_178[0][0][0], (((l_231 = 0xF71ED02848F1E6F9LL) > g_178[3][1][0]) <= 1L))) && 1UL), 1UL)))))))) ^ p_75.f2))) != 0x1B2FL) > 1L))));
    p_77 = (*g_117);
    g_211[0][0].f2 &= (+(safe_rshift_func_uint16_t_u_s(0x464BL, 9)));
    return l_100;
}


/* ------------------------------------------ */
/* 
 * reads : g_33 g_117 g_118 g_141 g_148 g_80 g_36 g_37 g_142 g_2 g_150 g_12 g_35 g_168 g_162 g_97 g_178 g_109
 * writes: g_33 g_109 g_141 g_150 g_162 g_12 g_35 g_169 g_211
 */
static int32_t * func_84(uint8_t  p_85, uint32_t  p_86)
{ /* block id: 22 */
    int32_t *l_116 = &g_2;
    uint64_t *l_127 = &g_97;
    uint64_t ***l_147 = &g_142;
    int64_t *l_161 = &g_162;
    for (g_33 = (-7); (g_33 > 18); g_33++)
    { /* block id: 25 */
        uint64_t * const l_113[1][1][6] = {{{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97}}};
        uint64_t * const * const l_112 = &l_113[0][0][1];
        uint64_t * const *l_115 = &l_113[0][0][1];
        uint64_t * const **l_114 = &l_115;
        uint8_t *l_128 = &g_35;
        uint8_t **l_129 = &l_128;
        uint8_t *l_131 = &g_35;
        uint8_t **l_130 = &l_131;
        int64_t l_132 = 1L;
        int8_t *l_133[3][10][8] = {{{&g_80[6],&g_80[6],&g_80[6],&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6]},{&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],(void*)0},{&g_80[6],&g_80[6],&g_80[6],&g_80[2],&g_80[6],&g_80[6],&g_80[6],(void*)0},{&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],&g_80[2],&g_80[6]},{&g_80[6],(void*)0,(void*)0,&g_80[0],&g_80[0],&g_80[6],&g_80[6],(void*)0},{(void*)0,&g_80[6],(void*)0,&g_80[2],&g_80[6],&g_80[6],&g_80[2],(void*)0},{&g_80[6],&g_80[6],&g_80[6],&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6]},{&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],(void*)0}},{{&g_80[6],&g_80[6],&g_80[6],&g_80[2],&g_80[6],&g_80[6],&g_80[6],(void*)0},{&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],&g_80[2],&g_80[6]},{&g_80[6],(void*)0,(void*)0,&g_80[0],&g_80[0],&g_80[6],&g_80[6],(void*)0},{(void*)0,&g_80[6],(void*)0,&g_80[2],&g_80[6],&g_80[6],&g_80[2],(void*)0},{&g_80[6],&g_80[6],&g_80[6],&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6]},{&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],(void*)0},{&g_80[6],&g_80[6],&g_80[6],&g_80[2],&g_80[6],&g_80[6],&g_80[6],(void*)0},{&g_80[6],(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{(void*)0,&g_80[6],&g_80[6],&g_80[6],(void*)0,&g_80[6],&g_80[6],(void*)0}},{{&g_80[6],&g_80[1],&g_80[6],&g_80[6],&g_80[0],&g_80[0],&g_80[6],&g_80[6]},{&g_80[1],&g_80[1],&g_80[6],&g_80[2],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{&g_80[1],&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],&g_80[6]},{&g_80[6],(void*)0,&g_80[6],&g_80[2],(void*)0,&g_80[6],&g_80[6],&g_80[6]},{(void*)0,&g_80[2],&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6],(void*)0},{&g_80[2],(void*)0,&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{(void*)0,&g_80[6],&g_80[6],&g_80[6],(void*)0,&g_80[6],&g_80[6],(void*)0},{&g_80[6],&g_80[1],&g_80[6],&g_80[6],&g_80[0],&g_80[0],&g_80[6],&g_80[6]},{&g_80[1],&g_80[1],&g_80[6],&g_80[2],&g_80[6],&g_80[6],&g_80[6],&g_80[6]},{&g_80[1],&g_80[6],&g_80[6],&g_80[6],&g_80[0],&g_80[6],&g_80[6],&g_80[6]}}};
        int32_t l_134 = 1L;
        uint64_t ****l_143 = (void*)0;
        uint64_t ****l_144 = &g_141;
        uint64_t ***l_146 = &g_142;
        uint64_t ****l_145[1][10] = {{(void*)0,(void*)0,&l_146,(void*)0,(void*)0,&l_146,(void*)0,(void*)0,&l_146,(void*)0}};
        uint16_t *l_149 = &g_150;
        const uint64_t **l_151 = (void*)0;
        const uint64_t *l_153 = (void*)0;
        const uint64_t **l_152 = &l_153;
        int32_t l_154 = (-1L);
        int i, j, k;
        (*l_114) = l_112;
        (*g_117) = l_116;
        (*g_118) = l_116;
        l_154 ^= (safe_mul_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((safe_rshift_func_int16_t_s_s(((((safe_div_func_int16_t_s_s(((l_127 != ((*l_152) = (((l_134 = ((((*l_129) = l_128) == ((*l_130) = &p_85)) , l_132)) ^ (((*l_149) = (safe_lshift_func_uint8_t_u_s((((safe_lshift_func_int16_t_s_s(0xE9F4L, 10)) , (((((*l_144) = g_141) == (l_147 = &g_142)) != ((((g_148[0] , g_80[6]) , 0xD6L) & (*g_36)) < (-10L))) || l_132)) , 1UL), p_85))) , g_148[0].f0)) , (void*)0))) , g_148[0].f1), l_132)) , (*l_146)) != (void*)0) | g_148[0].f0), 2)), g_33)), (*l_116)));
    }
    g_12 ^= (safe_div_func_int16_t_s_s((((*g_36) >= (0x92AD2651L | (safe_mod_func_int32_t_s_s((safe_sub_func_int64_t_s_s(((*l_161) = (l_116 != (void*)0)), p_86)), (safe_lshift_func_int8_t_s_u((0UL >= ((!(g_148[0].f0 && p_86)) && 5L)), (*l_116))))))) && (*l_116)), g_150));
    for (g_35 = 0; (g_35 <= 7); g_35 += 1)
    { /* block id: 42 */
        uint8_t *l_175 = (void*)0;
        uint8_t *l_176 = (void*)0;
        uint8_t *l_177[10] = {&g_35,&g_178[3][1][0],&g_178[3][1][0],&g_35,&g_178[3][1][0],&g_178[3][1][0],&g_35,&g_178[3][1][0],&g_178[3][1][0],&g_35};
        int32_t l_179 = 0x7EEBA36BL;
        int32_t *l_188 = (void*)0;
        int32_t *l_189 = &g_12;
        int32_t l_196 = 0x08AE7450L;
        uint64_t ** const *l_198 = (void*)0;
        uint64_t ** const ** const l_197 = &l_198;
        const struct S1 l_201 = {0UL,0x0F59ADD7983F0AB7LL,2157};
        int i;
        (*g_168) = (safe_mul_func_float_f_f(g_80[g_35], (p_85 < (*l_116))));
        if (((~(safe_lshift_func_int16_t_s_u((0x5FA4EC0DL <= ((l_179 = (safe_mul_func_uint16_t_u_u(p_85, 0x5E35L))) && 0xEAL)), 10))) <= (safe_mul_func_int8_t_s_s((safe_div_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u((0x877F8201L && ((*l_189) = p_86)), (((((*g_36) > p_86) < (safe_add_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((safe_div_func_uint8_t_u_u(((*g_141) == (void*)0), l_196)), p_86)), 1L))) , l_197) != (void*)0))) > p_86), g_80[1])), 0xCC642A9DD3993F16LL)), p_86))))
        { /* block id: 46 */
            uint8_t *l_199 = &g_178[3][1][0];
            uint8_t **l_200 = &l_177[7];
            (*l_189) &= (((*l_200) = l_199) != &p_85);
        }
        else
        { /* block id: 49 */
            float *l_202 = (void*)0;
            float *l_203[3];
            int32_t l_208 = (-5L);
            int32_t l_209 = 0x52239B40L;
            struct S1 *l_210 = &g_211[0][0];
            int i;
            for (i = 0; i < 3; i++)
                l_203[i] = &g_169;
            (*l_210) = (g_162 , ((l_201 , ((((*g_168) = 0x7.4E8889p+88) <= ((void*)0 == l_203[0])) < (safe_div_func_float_f_f((l_209 = ((l_208 = (((*l_161) = (safe_rshift_func_uint8_t_u_u((((g_97 < (*l_116)) & p_85) >= ((*l_116) || (*l_116))), g_2))) , g_178[3][1][0])) != (-0x6.Bp+1))), 0x7.Cp-1)))) , g_148[1]));
            (*l_189) = (*l_189);
            return (*g_117);
        }
    }
    return l_116;
}


/* ------------------------------------------ */
/* 
 * reads : g_36 g_37
 * writes:
 */
static uint8_t  func_87(uint8_t * p_88, uint32_t  p_89)
{ /* block id: 20 */
    return (*g_36);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_7[i][j], "g_7[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_35, "g_35", print_hash_value);
    transparent_crc(g_37, "g_37", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_80[i], "g_80[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_97, "g_97", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_148[i].f0, "g_148[i].f0", print_hash_value);
        transparent_crc(g_148[i].f1, "g_148[i].f1", print_hash_value);
        transparent_crc(g_148[i].f2, "g_148[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_150, "g_150", print_hash_value);
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc_bytes (&g_169, sizeof(g_169), "g_169", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_178[i][j][k], "g_178[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_211[i][j].f0, "g_211[i][j].f0", print_hash_value);
            transparent_crc(g_211[i][j].f1, "g_211[i][j].f1", print_hash_value);
            transparent_crc(g_211[i][j].f2, "g_211[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_220, "g_220", print_hash_value);
    transparent_crc(g_230, "g_230", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_290[i][j][k], "g_290[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_300.f0, "g_300.f0", print_hash_value);
    transparent_crc(g_300.f1, "g_300.f1", print_hash_value);
    transparent_crc(g_300.f2, "g_300.f2", print_hash_value);
    transparent_crc(g_300.f3, "g_300.f3", print_hash_value);
    transparent_crc(g_300.f4, "g_300.f4", print_hash_value);
    transparent_crc(g_300.f5, "g_300.f5", print_hash_value);
    transparent_crc(g_333, "g_333", print_hash_value);
    transparent_crc(g_337.f0, "g_337.f0", print_hash_value);
    transparent_crc(g_337.f1, "g_337.f1", print_hash_value);
    transparent_crc(g_337.f2, "g_337.f2", print_hash_value);
    transparent_crc(g_337.f3, "g_337.f3", print_hash_value);
    transparent_crc(g_337.f4, "g_337.f4", print_hash_value);
    transparent_crc(g_337.f5, "g_337.f5", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_370[i], "g_370[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_405[i][j][k].f0, "g_405[i][j][k].f0", print_hash_value);
                transparent_crc(g_405[i][j][k].f1, "g_405[i][j][k].f1", print_hash_value);
                transparent_crc(g_405[i][j][k].f2, "g_405[i][j][k].f2", print_hash_value);
                transparent_crc(g_405[i][j][k].f3, "g_405[i][j][k].f3", print_hash_value);
                transparent_crc(g_405[i][j][k].f4, "g_405[i][j][k].f4", print_hash_value);
                transparent_crc(g_405[i][j][k].f5, "g_405[i][j][k].f5", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_441, "g_441", print_hash_value);
    transparent_crc(g_474, "g_474", print_hash_value);
    transparent_crc(g_614.f0, "g_614.f0", print_hash_value);
    transparent_crc(g_614.f1, "g_614.f1", print_hash_value);
    transparent_crc(g_614.f2, "g_614.f2", print_hash_value);
    transparent_crc(g_614.f3, "g_614.f3", print_hash_value);
    transparent_crc(g_614.f4, "g_614.f4", print_hash_value);
    transparent_crc(g_614.f5, "g_614.f5", print_hash_value);
    transparent_crc(g_704.f0, "g_704.f0", print_hash_value);
    transparent_crc(g_704.f1, "g_704.f1", print_hash_value);
    transparent_crc(g_704.f2, "g_704.f2", print_hash_value);
    transparent_crc(g_704.f3, "g_704.f3", print_hash_value);
    transparent_crc(g_704.f4, "g_704.f4", print_hash_value);
    transparent_crc(g_704.f5, "g_704.f5", print_hash_value);
    transparent_crc(g_745, "g_745", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_780[i][j], "g_780[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_817[i], "g_817[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_859[i][j], "g_859[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_860, "g_860", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_984[i].f0, "g_984[i].f0", print_hash_value);
        transparent_crc(g_984[i].f1, "g_984[i].f1", print_hash_value);
        transparent_crc(g_984[i].f2, "g_984[i].f2", print_hash_value);
        transparent_crc(g_984[i].f3, "g_984[i].f3", print_hash_value);
        transparent_crc(g_984[i].f4, "g_984[i].f4", print_hash_value);
        transparent_crc(g_984[i].f5, "g_984[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_987.f0, "g_987.f0", print_hash_value);
    transparent_crc(g_987.f1, "g_987.f1", print_hash_value);
    transparent_crc(g_987.f2, "g_987.f2", print_hash_value);
    transparent_crc(g_987.f3, "g_987.f3", print_hash_value);
    transparent_crc(g_987.f4, "g_987.f4", print_hash_value);
    transparent_crc(g_987.f5, "g_987.f5", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 205
   depth: 1, occurrence: 16
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 13
breakdown:
   indirect level: 0, occurrence: 10
   indirect level: 1, occurrence: 1
   indirect level: 2, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 8
XXX times a bitfields struct on LHS: 4
XXX times a bitfields struct on RHS: 20
XXX times a single bitfield on LHS: 7
XXX times a single bitfield on RHS: 16

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 182
   depth: 2, occurrence: 41
   depth: 3, occurrence: 9
   depth: 4, occurrence: 1
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 11, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 3
   depth: 17, occurrence: 2
   depth: 18, occurrence: 3
   depth: 19, occurrence: 1
   depth: 20, occurrence: 3
   depth: 21, occurrence: 2
   depth: 22, occurrence: 3
   depth: 23, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 31, occurrence: 3
   depth: 33, occurrence: 2
   depth: 35, occurrence: 1
   depth: 38, occurrence: 2
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1
   depth: 49, occurrence: 1

XXX total number of pointers: 243

XXX times a variable address is taken: 635
XXX times a pointer is dereferenced on RHS: 100
breakdown:
   depth: 1, occurrence: 90
   depth: 2, occurrence: 4
   depth: 3, occurrence: 4
   depth: 4, occurrence: 2
XXX times a pointer is dereferenced on LHS: 126
breakdown:
   depth: 1, occurrence: 118
   depth: 2, occurrence: 6
   depth: 3, occurrence: 1
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 20
XXX times a pointer is compared with address of another variable: 1
XXX times a pointer is compared with another pointer: 5
XXX times a pointer is qualified to be dereferenced: 3445

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 888
   level: 2, occurrence: 94
   level: 3, occurrence: 55
   level: 4, occurrence: 24
XXX number of pointers point to pointers: 86
XXX number of pointers point to scalars: 148
XXX number of pointers point to structs: 9
XXX percent of pointers has null in alias set: 31.7
XXX average alias set size: 1.4

XXX times a non-volatile is read: 732
XXX times a non-volatile is write: 381
XXX times a volatile is read: 67
XXX    times read thru a pointer: 11
XXX times a volatile is write: 19
XXX    times written thru a pointer: 3
XXX times a volatile is available for access: 1.62e+03
XXX percentage of non-volatile access: 92.8

XXX forward jumps: 0
XXX backward jumps: 2

XXX stmts: 180
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 24
   depth: 2, occurrence: 21
   depth: 3, occurrence: 21
   depth: 4, occurrence: 37
   depth: 5, occurrence: 47

XXX percentage a fresh-made variable is used: 14.7
XXX percentage an existing variable is used: 85.3
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


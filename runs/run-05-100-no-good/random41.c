/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3981550542
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_4 = 1L;
static int32_t g_14 = 0x4891023FL;
static float g_15 = (-0x4.Cp+1);
static int8_t g_16 = 1L;
static volatile uint8_t g_18 = 0xA7L;/* VOLATILE GLOBAL g_18 */
static volatile int16_t g_21[9][10] = {{0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL},{0x85F1L,0x85F1L,0xDFF6L,0x85F1L,0x85F1L,0xDFF6L,0x85F1L,0x85F1L,0xDFF6L,0x85F1L},{0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L},{0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL},{0x85F1L,0x85F1L,0xDFF6L,0x85F1L,0x85F1L,0xDFF6L,0x739BL,0x739BL,0x85F1L,0x739BL},{0x739BL,0xDFF6L,0xDFF6L,0x739BL,0xDFF6L,0xDFF6L,0x739BL,0xDFF6L,0xDFF6L,0x739BL},{0xDFF6L,0x739BL,0xDFF6L,0xDFF6L,0x739BL,0xDFF6L,0xDFF6L,0x739BL,0xDFF6L,0xDFF6L},{0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL,0x739BL,0x85F1L,0x739BL},{0x739BL,0xDFF6L,0xDFF6L,0x739BL,0xDFF6L,0xDFF6L,0x739BL,0xDFF6L,0xDFF6L,0x739BL}};
static volatile float g_22 = 0xF.1D91A4p-36;/* VOLATILE GLOBAL g_22 */
static int32_t g_23 = 3L;
static int8_t g_24 = 0x8BL;
static volatile int32_t g_25[10][6] = {{0x88197C51L,1L,(-1L),1L,0x88197C51L,0xE356D6BBL},{1L,0x88197C51L,0xE356D6BBL,0xE356D6BBL,0x88197C51L,1L},{(-3L),1L,0xA02B236EL,0x88197C51L,0xA02B236EL,1L},{0xA02B236EL,(-3L),0xE356D6BBL,(-1L),(-1L),0xE356D6BBL},{0xA02B236EL,0xA02B236EL,(-1L),0x88197C51L,0x3EA4B544L,0x88197C51L},{(-3L),0xA02B236EL,(-3L),0xE356D6BBL,(-1L),(-1L)},{1L,(-3L),(-3L),1L,0xA02B236EL,0x88197C51L},{0x88197C51L,1L,(-1L),1L,0x88197C51L,0xE356D6BBL},{1L,0x88197C51L,0xE356D6BBL,0xE356D6BBL,0x88197C51L,1L},{(-3L),1L,0xA02B236EL,0x88197C51L,0xA02B236EL,1L}};
static volatile uint64_t g_26[1] = {0xCC885CB6E384D564LL};


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_18 g_26 g_4
 * writes: g_18 g_26
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    int16_t l_2 = 0x18D5L;
    int32_t *l_3 = &g_4;
    int32_t *l_5 = (void*)0;
    int32_t *l_6 = &g_4;
    int32_t *l_7 = &g_4;
    int32_t *l_8 = &g_4;
    int32_t *l_9 = &g_4;
    int32_t *l_10 = &g_4;
    int32_t *l_11 = &g_4;
    int32_t *l_12 = &g_4;
    int32_t *l_13[1][6][4] = {{{&g_4,&g_4,&g_4,&g_4},{&g_4,(void*)0,(void*)0,&g_4},{(void*)0,&g_4,(void*)0,(void*)0},{&g_4,&g_4,&g_4,&g_4},{&g_4,(void*)0,(void*)0,&g_4},{(void*)0,&g_4,(void*)0,(void*)0}}};
    int64_t l_17 = 0xEF7E291920B2455FLL;
    int i, j, k;
    ++g_18;
    ++g_26[0];
    return (*l_10);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc_bytes (&g_15, sizeof(g_15), "g_15", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_21[i][j], "g_21[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_22, sizeof(g_22), "g_22", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    transparent_crc(g_24, "g_24", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_25[i][j], "g_25[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_26[i], "g_26[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 12
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 1
breakdown:
   depth: 1, occurrence: 5

XXX total number of pointers: 10

XXX times a variable address is taken: 12
XXX times a pointer is dereferenced on RHS: 1
breakdown:
   depth: 1, occurrence: 1
XXX times a pointer is dereferenced on LHS: 0
breakdown:
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 210

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 10
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 20
XXX average alias set size: 1.1

XXX times a non-volatile is read: 2
XXX times a non-volatile is write: 0
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 2
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 8
XXX percentage of non-volatile access: 50

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 3
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 3

XXX percentage a fresh-made variable is used: 37.5
XXX percentage an existing variable is used: 62.5
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


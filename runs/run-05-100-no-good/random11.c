/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      254312604
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const volatile unsigned f0 : 28;
   signed f1 : 1;
   int8_t * f2;
   volatile uint64_t  f3;
   const volatile uint8_t  f4;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_6[2] = {9L,9L};
static volatile int32_t * const g_5 = &g_6[0];
static volatile int32_t *g_7[4][4][8] = {{{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],(void*)0,(void*)0,&g_6[0],(void*)0,&g_6[0],(void*)0},{&g_6[0],(void*)0,&g_6[0],&g_6[0],(void*)0,&g_6[0],&g_6[0],(void*)0},{(void*)0,&g_6[0],&g_6[0],(void*)0,&g_6[0],&g_6[0],(void*)0,&g_6[0]}},{{(void*)0,&g_6[0],(void*)0,&g_6[0],(void*)0,(void*)0,&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],(void*)0,&g_6[0]},{&g_6[0],(void*)0,&g_6[0],&g_6[0],&g_6[0],(void*)0,&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0]}},{{&g_6[0],&g_6[0],(void*)0,&g_6[0],&g_6[0],&g_6[0],(void*)0,&g_6[0]},{&g_6[0],(void*)0,&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],(void*)0,(void*)0,&g_6[0]}},{{&g_6[0],(void*)0,(void*)0,&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0]},{&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],&g_6[0],(void*)0,&g_6[0]},{&g_6[0],(void*)0,&g_6[0],&g_6[0],&g_6[0],(void*)0,&g_6[0],&g_6[0]}}};
static int32_t g_37 = 0L;
static int32_t *g_36 = &g_37;
static int32_t g_47[7][9][4] = {{{1L,0xC16B405BL,0x9934B93DL,1L},{1L,0x33B8FBA3L,0x3F3734BDL,(-8L)},{1L,0x2A25C95EL,1L,0xFFA36FE5L},{0x5372A718L,0xFDE63B69L,0xB9567464L,0L},{0x215F8F50L,0L,0L,8L},{0x4D21FD00L,4L,0xD5376E30L,0x32E7A53CL},{0xBDCF28D0L,0x215F8F50L,0L,4L},{0L,0x61BBCB56L,0L,0xBDB817BBL},{(-1L),8L,1L,(-10L)}},{{0xA1B14898L,0x4D21FD00L,0L,(-4L)},{9L,0xA77236C3L,(-1L),0xF8A3E09EL},{1L,1L,0x4D21FD00L,(-1L)},{1L,0x5DF1563CL,0x79942796L,0L},{0xC02AC6AAL,(-4L),8L,0L},{1L,0x4D21FD00L,4L,1L},{8L,0L,1L,(-1L)},{2L,0x8693C254L,4L,4L},{(-10L),0x0CE61FA7L,6L,0xFDE63B69L}},{{(-2L),0xE10EF7D5L,0xE10EF7D5L,(-2L)},{1L,0L,0x9934B93DL,0L},{4L,(-6L),0xF93C5815L,(-8L)},{0xD6A6C343L,0x4D12EA0FL,0x4D21FD00L,(-8L)},{0x5372A718L,(-6L),0x1C2610AAL,0L},{(-9L),0L,9L,(-2L)},{0xA1B14898L,0xE10EF7D5L,0xD5376E30L,0xFDE63B69L},{0xFFA36FE5L,0x0CE61FA7L,1L,4L},{0xC691C6C8L,0x8693C254L,0L,(-1L)}},{{1L,0x4D12EA0FL,0x9934B93DL,(-2L)},{0L,1L,1L,4L},{1L,0x0C971708L,0x5DF1563CL,6L},{6L,0xC691C6C8L,0xA2A42895L,0x5DF1563CL},{0xAB6AC39BL,(-4L),0x5372A718L,0x61BBCB56L},{0x450C1979L,0x518DB068L,0L,0x0C971708L},{0x32E7A53CL,1L,1L,0x9934B93DL},{9L,(-1L),0xF1A504A8L,0x79942796L},{(-2L),0xBDCF28D0L,0xA77236C3L,0x4BD1A6F3L}},{{8L,1L,(-1L),(-3L)},{0L,0xA1B14898L,1L,(-9L)},{0x32E7A53CL,6L,(-9L),0xD5376E30L},{(-4L),0x1C2610AAL,0xC16B405BL,0L},{0x79375E77L,0xB34E4A53L,0xA2A42895L,1L},{0x3A35E06CL,0xB9567464L,0xC02AC6AAL,(-1L)},{1L,0x8693C254L,1L,(-9L)},{0L,1L,(-2L),(-1L)},{0L,1L,(-1L),1L}},{{0x0C971708L,0xBDCF28D0L,(-1L),(-1L)},{0L,0xBDB817BBL,(-2L),0xFD5F3964L},{0L,0xD6A6C343L,1L,0x0C971708L},{1L,0x0C971708L,0xC02AC6AAL,1L},{0x3A35E06CL,0L,0xA2A42895L,0xC02AC6AAL},{0x79375E77L,0xC691C6C8L,0xC16B405BL,0x61BBCB56L},{(-4L),0xE10EF7D5L,(-9L),4L},{0x32E7A53CL,0xD6A6C343L,1L,(-2L)},{0L,(-1L),(-1L),0xF93C5815L}},{{8L,(-1L),0xA77236C3L,1L},{(-2L),1L,0xF1A504A8L,(-3L)},{9L,0x4D21FD00L,1L,0x0CE61FA7L},{0x32E7A53CL,0x8693C254L,0L,(-10L)},{0x450C1979L,0x1C2610AAL,0x5372A718L,1L},{0xAB6AC39BL,0x79942796L,0xA2A42895L,0L},{6L,9L,0x5DF1563CL,(-1L)},{1L,0x3A35E06CL,1L,0x0CE61FA7L},{0L,0xA1B14898L,0x9934B93DL,0x1C2610AAL}}};
static int64_t g_49 = 0x1FD024CA9A92ED16LL;
static uint32_t g_50 = 4294967290UL;
static uint64_t g_72 = 8UL;
static uint64_t *g_71 = &g_72;
static int8_t g_76 = 0L;
static int8_t *g_75 = &g_76;
static uint8_t g_96 = 1UL;
static union U0 g_99 = {0x2852A3B4L};/* VOLATILE GLOBAL g_99 */
static uint32_t g_100 = 0xF0DE344EL;
static uint16_t g_103 = 0x1F03L;
static int32_t g_140 = 1L;
static int8_t g_241 = 0xBEL;
static uint64_t g_248 = 18446744073709551615UL;
static uint64_t g_252 = 2UL;
static float g_274 = 0x4.0B6841p+77;
static float *g_273 = &g_274;
static float * const *g_272 = &g_273;
static float **g_278 = &g_273;
static volatile uint8_t g_346 = 0xEAL;/* VOLATILE GLOBAL g_346 */
static volatile uint8_t *g_345 = &g_346;
static volatile uint8_t * volatile * const  volatile g_344 = &g_345;/* VOLATILE GLOBAL g_344 */
static int32_t *g_352 = &g_47[3][5][3];
static int32_t ** volatile g_351 = &g_352;/* VOLATILE GLOBAL g_351 */
static uint64_t g_374 = 0xBE2AC025E0DA8BD9LL;
static int64_t g_376 = 0xCA53A14165B2EF04LL;
static uint32_t g_381 = 0UL;
static volatile union U0 g_384 = {9UL};/* VOLATILE GLOBAL g_384 */
static const uint16_t g_387 = 0x38B2L;
static uint16_t *g_400 = (void*)0;
static uint16_t **g_399 = &g_400;
static int32_t g_454[10] = {(-1L),2L,2L,(-1L),3L,(-1L),2L,2L,(-1L),3L};
static volatile union U0 g_455[8] = {{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}};
static uint8_t **g_456 = (void*)0;
static uint8_t *g_458 = &g_96;
static uint8_t **g_457 = &g_458;
static int8_t g_491 = (-1L);
static int32_t ** volatile g_511 = &g_352;/* VOLATILE GLOBAL g_511 */
static int32_t ** volatile g_513 = &g_352;/* VOLATILE GLOBAL g_513 */
static uint8_t g_614 = 8UL;
static uint32_t g_616 = 0UL;
static union U0 g_618 = {0UL};/* VOLATILE GLOBAL g_618 */
static int8_t g_729 = 4L;
static float g_732 = 0x0.C9F80Ap-55;
static union U0 g_753 = {0xEEC1C356L};/* VOLATILE GLOBAL g_753 */
static union U0 *g_752[6] = {&g_753,&g_753,&g_753,&g_753,&g_753,&g_753};
static volatile union U0 g_755 = {0x1967D1A1L};/* VOLATILE GLOBAL g_755 */
static union U0 g_767 = {0xA18562D0L};/* VOLATILE GLOBAL g_767 */
static volatile float g_778 = 0x3.40D004p-72;/* VOLATILE GLOBAL g_778 */
static int32_t g_799 = 0x43A390A6L;
static uint16_t *** volatile g_808 = &g_399;/* VOLATILE GLOBAL g_808 */
static int16_t g_851 = 1L;
static uint32_t * volatile g_880 = &g_616;/* VOLATILE GLOBAL g_880 */
static uint32_t * volatile * volatile g_879 = &g_880;/* VOLATILE GLOBAL g_879 */
static uint32_t * volatile * volatile * volatile g_878[9][4][7] = {{{(void*)0,&g_879,(void*)0,(void*)0,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879,&g_879},{(void*)0,&g_879,(void*)0,&g_879,(void*)0,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879}},{{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,(void*)0,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879}},{{&g_879,&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879},{&g_879,&g_879,(void*)0,(void*)0,&g_879,(void*)0,&g_879},{&g_879,(void*)0,&g_879,&g_879,(void*)0,(void*)0,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879}},{{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{(void*)0,&g_879,&g_879,(void*)0,(void*)0,&g_879,(void*)0},{&g_879,&g_879,(void*)0,&g_879,&g_879,&g_879,&g_879},{(void*)0,&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879}},{{&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879,(void*)0},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{(void*)0,(void*)0,(void*)0,&g_879,&g_879,&g_879,&g_879},{(void*)0,&g_879,&g_879,&g_879,(void*)0,(void*)0,&g_879}},{{&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879,&g_879}},{{&g_879,&g_879,(void*)0,&g_879,&g_879,&g_879,&g_879},{&g_879,(void*)0,&g_879,(void*)0,(void*)0,(void*)0,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,(void*)0},{&g_879,(void*)0,(void*)0,&g_879,&g_879,&g_879,(void*)0}},{{&g_879,&g_879,(void*)0,&g_879,(void*)0,&g_879,&g_879},{(void*)0,&g_879,(void*)0,(void*)0,&g_879,(void*)0,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,&g_879},{&g_879,&g_879,&g_879,(void*)0,&g_879,(void*)0,(void*)0}},{{&g_879,&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879},{&g_879,&g_879,&g_879,&g_879,&g_879,&g_879,(void*)0},{&g_879,&g_879,&g_879,&g_879,&g_879,(void*)0,&g_879},{&g_879,(void*)0,&g_879,&g_879,&g_879,&g_879,&g_879}}};
static volatile uint32_t * volatile *g_882 = (void*)0;
static volatile uint32_t * volatile **g_881 = &g_882;
static volatile union U0 g_894 = {0xBEBA9DFBL};/* VOLATILE GLOBAL g_894 */
static const int32_t g_913 = 0x460FCA55L;
static const int32_t *g_914 = (void*)0;
static volatile union U0 g_935 = {2UL};/* VOLATILE GLOBAL g_935 */
static int32_t *g_943 = &g_454[8];
static int32_t **g_942 = &g_943;
static volatile uint8_t g_965 = 4UL;/* VOLATILE GLOBAL g_965 */
static uint32_t *g_1122[1][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static uint32_t **g_1121[3] = {&g_1122[0][3],&g_1122[0][3],&g_1122[0][3]};
static volatile union U0 g_1148 = {0x0202093AL};/* VOLATILE GLOBAL g_1148 */
static float g_1190 = (-0x1.4p-1);
static int32_t g_1221 = 0xB34BA233L;
static volatile float *g_1237 = &g_778;
static volatile float **g_1236 = &g_1237;
static volatile float *** volatile g_1235[2] = {&g_1236,&g_1236};
static volatile float *** volatile *g_1234 = &g_1235[1];
static volatile float *** volatile * volatile *g_1233 = &g_1234;
static volatile int16_t **g_1244 = (void*)0;
static union U0 *g_1275 = (void*)0;
static union U0 **g_1274 = &g_1275;
static int32_t g_1279[3] = {(-1L),(-1L),(-1L)};
static int64_t g_1314 = 8L;
static uint32_t **g_1338 = &g_1122[0][0];
static int32_t ** volatile g_1347 = &g_36;/* VOLATILE GLOBAL g_1347 */
static int32_t g_1362[6] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
static volatile union U0 g_1380[2][7][1] = {{{{0x1B0C8748L}},{{0x1B0C8748L}},{{0xE297B0B0L}},{{0UL}},{{0xE297B0B0L}},{{0x1B0C8748L}},{{0x1B0C8748L}}},{{{0xE297B0B0L}},{{0UL}},{{0xE297B0B0L}},{{0x1B0C8748L}},{{0x1B0C8748L}},{{0xE297B0B0L}},{{0UL}}}};
static union U0 g_1411 = {1UL};/* VOLATILE GLOBAL g_1411 */
static float ***g_1491 = &g_278;
static float ****g_1490 = &g_1491;
static float *****g_1489 = &g_1490;
static float *****g_1493 = (void*)0;
static int32_t ** volatile g_1496 = &g_352;/* VOLATILE GLOBAL g_1496 */
static uint32_t g_1520 = 8UL;
static int32_t g_1540 = (-10L);
static volatile union U0 g_1546 = {9UL};/* VOLATILE GLOBAL g_1546 */
static volatile union U0 *g_1548 = &g_1380[0][2][0];
static volatile union U0 * volatile * volatile g_1547[1] = {&g_1548};
static int32_t ** volatile g_1584 = &g_36;/* VOLATILE GLOBAL g_1584 */
static int32_t ** volatile g_1585 = &g_352;/* VOLATILE GLOBAL g_1585 */
static const uint8_t *g_1691 = &g_614;
static const uint8_t **g_1690 = &g_1691;
static union U0 ***g_1725 = &g_1274;
static uint8_t g_1728 = 0x28L;
static uint8_t ** const *g_1778[6][9][3] = {{{&g_456,&g_457,&g_457},{&g_457,&g_457,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_457,(void*)0,&g_457},{&g_457,&g_457,&g_456},{&g_457,&g_457,&g_456},{(void*)0,&g_457,&g_457},{&g_457,&g_457,&g_456},{&g_456,(void*)0,&g_457}},{{&g_456,(void*)0,&g_456},{&g_456,&g_457,&g_456},{&g_456,&g_457,&g_457},{&g_457,&g_457,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_457,(void*)0,&g_457},{&g_457,&g_457,&g_456},{&g_457,&g_457,&g_456},{(void*)0,&g_457,&g_457}},{{&g_457,&g_457,&g_456},{&g_456,(void*)0,&g_457},{&g_456,(void*)0,&g_456},{&g_456,&g_457,&g_456},{&g_456,&g_457,&g_457},{&g_457,&g_457,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_457,(void*)0,&g_457},{&g_457,&g_457,&g_456}},{{&g_457,&g_457,&g_456},{(void*)0,&g_457,&g_457},{&g_457,&g_457,&g_456},{&g_456,(void*)0,&g_457},{&g_456,(void*)0,&g_456},{&g_456,&g_457,&g_456},{&g_456,&g_457,&g_457},{&g_457,&g_457,(void*)0},{(void*)0,(void*)0,(void*)0}},{{&g_457,(void*)0,&g_457},{&g_457,&g_456,&g_457},{&g_456,&g_456,&g_457},{&g_456,&g_456,&g_456},{&g_456,&g_456,&g_457},{&g_456,&g_456,&g_456},{&g_456,&g_457,&g_457},{&g_456,&g_456,&g_457},{&g_456,&g_457,&g_457}},{{&g_456,&g_456,&g_456},{&g_456,&g_457,&g_456},{&g_456,&g_456,&g_457},{&g_456,&g_456,&g_457},{&g_456,&g_456,&g_457},{&g_456,&g_456,&g_456},{&g_456,&g_456,&g_457},{&g_456,&g_456,&g_456},{&g_456,&g_457,&g_457}}};
static uint8_t ** const ** volatile g_1777 = &g_1778[2][4][1];/* VOLATILE GLOBAL g_1777 */
static int32_t ** volatile g_1789[1] = {&g_352};
static uint32_t *g_1802[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static uint32_t * volatile *g_1801 = &g_1802[2];
static const int32_t g_1820 = 0x5A376CE6L;
static int16_t g_1867 = 0xD1D2L;
static volatile int64_t * volatile g_1949 = (void*)0;/* VOLATILE GLOBAL g_1949 */
static volatile int64_t * volatile *g_1948 = &g_1949;
static int64_t *g_1972[6][2] = {{&g_376,&g_376},{&g_376,&g_376},{&g_376,&g_376},{&g_376,&g_376},{&g_376,&g_376},{&g_376,&g_376}};
static int64_t **g_1971[5] = {&g_1972[4][0],&g_1972[4][0],&g_1972[4][0],&g_1972[4][0],&g_1972[4][0]};
static uint32_t ***g_2050 = &g_1121[0];
static uint32_t ****g_2049 = &g_2050;
static union U0 g_2114 = {0x8689A25CL};/* VOLATILE GLOBAL g_2114 */
static volatile union U0 g_2178[4] = {{0x69CA5472L},{0x69CA5472L},{0x69CA5472L},{0x69CA5472L}};
static const int32_t *g_2194[5] = {&g_1820,&g_1820,&g_1820,&g_1820,&g_1820};
static const int32_t ** volatile g_2193[4][1] = {{&g_2194[3]},{&g_2194[3]},{&g_2194[3]},{&g_2194[3]}};
static const int32_t ** const  volatile g_2195 = &g_2194[4];/* VOLATILE GLOBAL g_2195 */
static union U0 g_2225 = {0x9510DDF4L};/* VOLATILE GLOBAL g_2225 */
static uint32_t g_2248 = 3UL;
static int32_t **g_2260[2][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static volatile uint8_t g_2427 = 0xFEL;/* VOLATILE GLOBAL g_2427 */
static const int32_t ** volatile g_2434 = &g_2194[3];/* VOLATILE GLOBAL g_2434 */
static union U0 * volatile * volatile * volatile * volatile g_2443 = (void*)0;/* VOLATILE GLOBAL g_2443 */
static union U0 * volatile * volatile * volatile * volatile * volatile g_2442 = &g_2443;/* VOLATILE GLOBAL g_2442 */
static volatile union U0 *g_2480 = (void*)0;
static volatile uint16_t g_2518 = 0UL;/* VOLATILE GLOBAL g_2518 */
static volatile union U0 g_2539[5][2][10] = {{{{1UL},{0xDE7727DEL},{0xDE7727DEL},{1UL},{0UL},{4294967295UL},{0xFAC4DE87L},{0x04C3BB1EL},{0x24D52F2CL},{4294967292UL}},{{0xABE1F7FAL},{5UL},{4294967292UL},{4294967288UL},{0xB861DB02L},{4294967292UL},{0xDE7727DEL},{0xFAC4DE87L},{0x24D52F2CL},{0x7D776E0CL}}},{{{0xDE7727DEL},{0UL},{0xABE1F7FAL},{1UL},{0xAE3E0946L},{4294967295UL},{4294967288UL},{4294967295UL},{0xAE3E0946L},{1UL}},{{0x04923C0AL},{0x2FB15A63L},{0x04923C0AL},{0xA1A797CDL},{4294967295UL},{1UL},{1UL},{4294967288UL},{4294967295UL},{0xABE1F7FAL}}},{{{0xC4ABF24CL},{0x04C3BB1EL},{4294967295UL},{4294967295UL},{4294967292UL},{1UL},{0UL},{4294967288UL},{0x04923C0AL},{0xDE7727DEL}},{{4294967288UL},{4294967295UL},{0x04923C0AL},{4294967292UL},{4294967295UL},{0xC4ABF24CL},{0xC4ABF24CL},{4294967295UL},{4294967292UL},{0x04923C0AL}}},{{{0xA1A797CDL},{0xA1A797CDL},{0xABE1F7FAL},{0x24D52F2CL},{1UL},{4294967292UL},{0x7D776E0CL},{0xFAC4DE87L},{0x82F3B43DL},{0xC4ABF24CL}},{{0x82F3B43DL},{0UL},{4294967292UL},{4294967295UL},{1UL},{5UL},{0x7D776E0CL},{0x04C3BB1EL},{4294967295UL},{4294967288UL}}},{{{4294967287UL},{0xA1A797CDL},{0xDE7727DEL},{0xB861DB02L},{0xC4ABF24CL},{0xFAC4DE87L},{0xC4ABF24CL},{0xB861DB02L},{0xDE7727DEL},{0xA1A797CDL}},{{4294967292UL},{4294967295UL},{0UL},{0xFAC4DE87L},{1UL},{1UL},{4294967288UL},{4294967295UL},{0xABE1F7FAL},{0xAE3E0946L}}}};
static uint16_t ** const *g_2542 = (void*)0;
static uint16_t ** const g_2547 = (void*)0;
static uint16_t ** const *g_2546 = &g_2547;
static volatile union U0 **g_2552 = &g_1548;
static volatile union U0 ** volatile *g_2551 = &g_2552;
static volatile union U0 ** volatile * volatile *g_2550 = &g_2551;
static volatile union U0 ** volatile * volatile **g_2549 = &g_2550;
static uint64_t * volatile *g_2576 = &g_71;
static uint64_t * volatile **g_2575 = &g_2576;
static uint64_t * volatile ** volatile *g_2574 = &g_2575;
static uint64_t * volatile ** volatile * volatile *g_2573 = &g_2574;
static float g_2652 = 0x0.4p-1;
static const int8_t * const  volatile g_2685 = (void*)0;/* VOLATILE GLOBAL g_2685 */
static const int8_t * const  volatile * volatile g_2684 = &g_2685;/* VOLATILE GLOBAL g_2684 */
static int32_t ***g_2690[8][9][1] = {{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}},{{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942},{&g_942}}};
static const volatile union U0 g_2702 = {4294967290UL};/* VOLATILE GLOBAL g_2702 */
static uint16_t g_2751[9] = {0x26EFL,0x26EFL,0xA3F6L,0x26EFL,0x26EFL,0xA3F6L,0x26EFL,0x26EFL,0xA3F6L};
static int32_t ** volatile g_2768 = &g_36;/* VOLATILE GLOBAL g_2768 */
static int16_t g_2794 = 0xF5FCL;
static int32_t ** const  volatile g_2800 = &g_36;/* VOLATILE GLOBAL g_2800 */
static int32_t ** volatile g_2801[6][10] = {{&g_352,&g_352,&g_352,&g_36,(void*)0,&g_36,&g_352,&g_352,&g_352,&g_352},{&g_36,&g_36,&g_352,&g_352,&g_36,&g_36,&g_352,&g_36,&g_36,&g_352},{(void*)0,&g_352,(void*)0,&g_352,&g_352,&g_352,&g_352,(void*)0,&g_352,(void*)0},{(void*)0,&g_36,&g_352,&g_36,&g_352,&g_36,(void*)0,(void*)0,&g_36,&g_352},{&g_36,(void*)0,(void*)0,&g_36,&g_352,&g_36,&g_352,&g_36,(void*)0,(void*)0},{&g_352,(void*)0,&g_352,&g_352,&g_352,&g_352,(void*)0,&g_352,(void*)0,&g_352}};
static int32_t ** const  volatile g_2802 = &g_36;/* VOLATILE GLOBAL g_2802 */
static int32_t ** volatile g_2812 = &g_36;/* VOLATILE GLOBAL g_2812 */
static uint8_t ***g_2904 = &g_456;
static uint8_t ****g_2903[8][10][2] = {{{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904}},{{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904},{&g_2904,&g_2904}},{{&g_2904,&g_2904},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0}},{{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904}},{{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904}},{{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0}},{{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904}},{{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904},{&g_2904,(void*)0},{&g_2904,&g_2904},{(void*)0,&g_2904}}};
static int32_t ** const  volatile g_3001[4] = {&g_36,&g_36,&g_36,&g_36};
static int32_t ** volatile g_3002 = &g_352;/* VOLATILE GLOBAL g_3002 */
static volatile union U0 g_3041 = {0UL};/* VOLATILE GLOBAL g_3041 */
static volatile union U0 g_3084 = {4294967295UL};/* VOLATILE GLOBAL g_3084 */
static const int32_t ** volatile g_3114[1][3] = {{&g_2194[2],&g_2194[2],&g_2194[2]}};
static const int32_t ** volatile g_3115 = &g_2194[3];/* VOLATILE GLOBAL g_3115 */
static volatile union U0 g_3145 = {4294967287UL};/* VOLATILE GLOBAL g_3145 */
static volatile union U0 g_3210 = {1UL};/* VOLATILE GLOBAL g_3210 */
static union U0 ****g_3262[9] = {&g_1725,&g_1725,&g_1725,&g_1725,&g_1725,&g_1725,&g_1725,&g_1725,&g_1725};
static union U0 *****g_3261 = &g_3262[2];
static volatile union U0 g_3320 = {1UL};/* VOLATILE GLOBAL g_3320 */
static volatile union U0 g_3385 = {4294967293UL};/* VOLATILE GLOBAL g_3385 */
static volatile uint16_t g_3392 = 65532UL;/* VOLATILE GLOBAL g_3392 */
static volatile union U0 g_3404 = {0xE9A2484EL};/* VOLATILE GLOBAL g_3404 */
static int64_t **g_3414 = &g_1972[4][0];
static int32_t ***** volatile g_3453 = (void*)0;/* VOLATILE GLOBAL g_3453 */
static int32_t ***g_3455 = (void*)0;
static int32_t ****g_3454 = &g_3455;
static int8_t ** const g_3467 = (void*)0;
static int8_t ** const *g_3466 = &g_3467;
static uint32_t *g_3486 = &g_1520;
static uint32_t ** const g_3485 = &g_3486;
static uint32_t ** const *g_3484[2] = {&g_3485,&g_3485};
static int32_t ** volatile g_3488 = &g_36;/* VOLATILE GLOBAL g_3488 */
static uint64_t *g_3543 = &g_72;
static union U0 g_3580[6][8][5] = {{{{0x4E99C8EAL},{1UL},{4294967291UL},{0x4B29A305L},{4294967295UL}},{{1UL},{1UL},{0x4F3B3D84L},{0xEBABB49EL},{4294967295UL}},{{4294967288UL},{1UL},{4294967295UL},{4294967295UL},{0x151C47F7L}},{{0xCBF3425AL},{1UL},{0x151C47F7L},{0xD8FF990DL},{4294967291UL}},{{4294967291UL},{0UL},{4294967295UL},{4294967295UL},{1UL}},{{0x48E472B3L},{0xEBABB49EL},{4294967295UL},{0x7255547EL},{4294967292UL}},{{0x11438F35L},{4294967292UL},{0x151C47F7L},{0UL},{4294967295UL}},{{7UL},{0x151C47F7L},{4294967295UL},{4294967295UL},{4294967295UL}}},{{{0x7255547EL},{0x7255547EL},{0x4F3B3D84L},{1UL},{1UL}},{{0xC70199F6L},{0x90800A71L},{4294967291UL},{4294967292UL},{4294967293UL}},{{4294967287UL},{0xD8549482L},{0xD8FF990DL},{0UL},{8UL}},{{0xCDE45E14L},{0x4E99C8EAL},{4294967288UL},{0xA50235C9L},{4294967294UL}},{{1UL},{4294967295UL},{0x11438F35L},{4294967294UL},{0xBA56251EL}},{{0UL},{0xD8FF990DL},{0x90800A71L},{0x1F24DACFL},{4294967291UL}},{{4294967287UL},{0x3D1A3607L},{0x4F3B3D84L},{0x151C47F7L},{9UL}},{{0x4E99C8EAL},{0xD8549482L},{0x3BBDE044L},{4294967295UL},{9UL}}},{{{0x4E99C8EAL},{1UL},{0x751695DCL},{7UL},{1UL}},{{4294967287UL},{4294967288UL},{4294967295UL},{2UL},{4294967295UL}},{{0UL},{0x3BBDE044L},{0x48E472B3L},{0x3BBDE044L},{0UL}},{{1UL},{9UL},{0x1F24DACFL},{0xEBABB49EL},{4294967295UL}},{{1UL},{0x4B29A305L},{0x10D05438L},{4294967295UL},{4294967287UL}},{{4294967294UL},{0xC70199F6L},{0xBA56251EL},{9UL},{4294967295UL}},{{4294967293UL},{4294967295UL},{0x0C6951FDL},{6UL},{0UL}},{{4294967295UL},{0x11438F35L},{4294967294UL},{0xBA56251EL},{4294967295UL}}},{{{6UL},{0x3AAA92F7L},{0x3D1A3607L},{0x11438F35L},{1UL}},{{4294967295UL},{1UL},{4294967287UL},{0UL},{9UL}},{{0x1F24DACFL},{0x4F3B3D84L},{7UL},{0UL},{9UL}},{{2UL},{1UL},{1UL},{0x11438F35L},{4294967291UL}},{{4294967295UL},{0xA50235C9L},{0x7255547EL},{0xBA56251EL},{0xBA56251EL}},{{1UL},{4294967293UL},{1UL},{6UL},{4294967294UL}},{{4294967291UL},{0x7255547EL},{1UL},{9UL},{0xEBABB49EL}},{{0x7255547EL},{5UL},{6UL},{4294967295UL},{4294967295UL}}},{{{0UL},{0UL},{1UL},{0xEBABB49EL},{4294967292UL}},{{0UL},{4294967291UL},{1UL},{0x3BBDE044L},{1UL}},{{0xD8549482L},{0xCDE45E14L},{0x7255547EL},{2UL},{4294967292UL}},{{0x11438F35L},{0UL},{1UL},{7UL},{0x3D1A3607L}},{{0xEBABB49EL},{4294967292UL},{7UL},{4294967295UL},{0x4B29A305L}},{{9UL},{4294967292UL},{4294967287UL},{0x151C47F7L},{2UL}},{{0x48E472B3L},{0UL},{0x3D1A3607L},{0x1F24DACFL},{0xD8FF990DL}},{{0xCBF3425AL},{0xCDE45E14L},{4294967294UL},{4294967294UL},{0xCDE45E14L}}},{{{4294967295UL},{4294967291UL},{0x0C6951FDL},{0xA50235C9L},{4294967295UL}},{{0xC70199F6L},{0UL},{0xBA56251EL},{1UL},{6UL}},{{0xBA56251EL},{5UL},{0x10D05438L},{0xC70199F6L},{0x11438F35L}},{{0xC70199F6L},{0x7255547EL},{0x1F24DACFL},{4294967288UL},{4294967295UL}},{{4294967295UL},{4294967293UL},{0x48E472B3L},{4294967295UL},{4294967288UL}},{{0xCBF3425AL},{0xA50235C9L},{4294967295UL},{5UL},{7UL}},{{0x48E472B3L},{1UL},{0x751695DCL},{4294967295UL},{4294967293UL}},{{9UL},{0x4F3B3D84L},{0x3BBDE044L},{0xCDE45E14L},{4294967293UL}}}};
static volatile union U0 g_3590[7] = {{0xA3AFF1B9L},{0xA3AFF1B9L},{0xA3AFF1B9L},{0xA3AFF1B9L},{0xA3AFF1B9L},{0xA3AFF1B9L},{0xA3AFF1B9L}};
static uint16_t g_3624 = 0UL;
static int32_t g_3684 = 9L;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t  func_2(int8_t  p_3);
static uint32_t  func_8(int32_t * p_9, const int64_t  p_10, float  p_11, int8_t * p_12, int8_t  p_13);
static int32_t * func_14(int8_t * const  p_15, int16_t  p_16);
static int8_t * func_17(uint8_t  p_18, int32_t  p_19, int32_t * p_20, const uint64_t  p_21, uint64_t  p_22);
static int16_t  func_30(int32_t * p_31, const int8_t  p_32, int64_t  p_33, uint16_t  p_34, int32_t * p_35);
static uint32_t  func_42(int32_t  p_43);
static int32_t  func_53(uint64_t * const  p_54, int64_t  p_55);
static int8_t * func_60(int32_t * p_61, int16_t  p_62, const uint8_t  p_63, float  p_64, int64_t  p_65);
static int32_t * func_66(uint64_t * p_67, int32_t * p_68, int8_t * p_69, int8_t  p_70);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_36 g_50 g_71 g_75 g_76 g_47 g_96 g_99 g_72 g_100 g_37 g_49 g_6 g_140 g_248 g_241 g_272 g_99.f0 g_103 g_252 g_376 g_616 g_455 g_346 g_374 g_352 g_273 g_274 g_458 g_755 g_767 g_511 g_799 g_1274 g_614 g_1279 g_387 g_851 g_1221 g_879 g_880 g_1233 g_1234 g_1235 g_1236 g_1237 g_778 g_345 g_881 g_882 g_1347 g_1362 g_1380 g_1244 g_1496 g_1493 g_1490 g_344 g_1540 g_752 g_753 g_1690 g_943 g_454 g_1520 g_755.f0 g_1777 g_1728 g_894.f0 g_1801 g_1584 g_1691 g_1148.f0 g_808 g_399 g_381 g_1820 g_400 g_1948 g_1867 g_1971 g_457 g_491 g_513 g_1338 g_1122 g_2114 g_942 g_1489 g_1491 g_278 g_2178 g_1725 g_2195 g_1972 g_2225 g_2248 g_2427 g_2194 g_2434 g_2442 g_2480 g_2518 g_2539 g_2549 g_2573 g_2576 g_2574 g_2575 g_2550 g_2551 g_2552 g_1548 g_1275 g_2684 g_2702 g_2751 g_2768 g_2794 g_2800 g_2802 g_2812 g_2903 g_3002 g_2690 g_3084 g_3115 g_3145 g_3210 g_3466 g_351 g_3488 g_3454 g_3485 g_3486 g_3543 g_3580 g_3590 g_1314 g_3624 g_3414 g_3455 g_1585 g_3684
 * writes: g_7 g_50 g_96 g_100 g_76 g_103 g_37 g_72 g_6 g_49 g_140 g_241 g_248 g_272 g_278 g_47 g_616 g_374 g_352 g_729 g_732 g_274 g_752 g_799 g_1279 g_614 g_376 g_1314 g_1121 g_1338 g_36 g_252 g_851 g_778 g_1489 g_1493 g_1520 g_1221 g_1690 g_491 g_1725 g_1728 g_381 g_1867 g_1971 g_2049 g_454 g_1275 g_2194 g_2260 g_399 g_1540 g_942 g_2518 g_2542 g_2546 g_2690 g_2751 g_2794 g_2903 g_2248 g_943 g_3484 g_2652 g_3455 g_1802 g_3543 g_1190 g_3684
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_4 = 18446744073709551614UL;
    int32_t *l_3683 = &g_3684;
    int32_t **l_3688[6];
    uint32_t l_3689 = 6UL;
    int i;
    for (i = 0; i < 6; i++)
        l_3688[i] = &g_36;
    (*l_3683) |= func_2(l_4);
    for (g_1540 = 0; (g_1540 < (-28)); --g_1540)
    { /* block id: 1739 */
        int64_t l_3687[10] = {0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL,0x74499E559ADF45B9LL};
        int i;
        if (l_3687[7])
            break;
    }
    (*g_2434) = l_3683;
    return l_3689;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_36 g_50 g_71 g_75 g_76 g_47 g_96 g_99 g_72 g_100 g_37 g_49 g_6 g_140 g_248 g_241 g_272 g_99.f0 g_103 g_252 g_376 g_616 g_455 g_346 g_374 g_352 g_273 g_274 g_458 g_755 g_767 g_511 g_799 g_1274 g_614 g_1279 g_387 g_851 g_1221 g_879 g_880 g_1233 g_1234 g_1235 g_1236 g_1237 g_778 g_345 g_881 g_882 g_1347 g_1362 g_1380 g_1244 g_1496 g_1493 g_1490 g_344 g_1540 g_752 g_753 g_1690 g_943 g_454 g_1520 g_755.f0 g_1777 g_1728 g_894.f0 g_1801 g_1584 g_1691 g_1148.f0 g_808 g_399 g_381 g_1820 g_400 g_1948 g_1867 g_1971 g_457 g_491 g_513 g_1338 g_1122 g_2114 g_942 g_1489 g_1491 g_278 g_2178 g_1725 g_2195 g_1972 g_2225 g_2248 g_2427 g_2194 g_2434 g_2442 g_2480 g_2518 g_2539 g_2549 g_2573 g_2576 g_2574 g_2575 g_2550 g_2551 g_2552 g_1548 g_1275 g_2684 g_2702 g_2751 g_2768 g_2794 g_2800 g_2802 g_2812 g_2903 g_3002 g_2690 g_3084 g_3115 g_3145 g_3210 g_3466 g_351 g_3488 g_3454 g_3485 g_3486 g_3543 g_3580 g_3590 g_1314 g_3624 g_3414 g_3455 g_1585
 * writes: g_7 g_50 g_96 g_100 g_76 g_103 g_37 g_72 g_6 g_49 g_140 g_241 g_248 g_272 g_278 g_47 g_616 g_374 g_352 g_729 g_732 g_274 g_752 g_799 g_1279 g_614 g_376 g_1314 g_1121 g_1338 g_36 g_252 g_851 g_778 g_1489 g_1493 g_1520 g_1221 g_1690 g_491 g_1725 g_1728 g_381 g_1867 g_1971 g_2049 g_454 g_1275 g_2194 g_2260 g_399 g_1540 g_942 g_2518 g_2542 g_2546 g_2690 g_2751 g_2794 g_2903 g_2248 g_943 g_3484 g_2652 g_3455 g_1802 g_3543 g_1190
 */
static int32_t  func_2(int8_t  p_3)
{ /* block id: 1 */
    uint32_t l_44[1][1][2];
    int32_t *l_1278 = &g_1279[1];
    uint8_t *l_1280 = (void*)0;
    uint8_t *l_1281 = &g_614;
    uint32_t l_1299 = 0x839AC033L;
    const uint32_t l_1300 = 0x9E398F90L;
    int32_t l_3201[7][7][5] = {{{0x9E257F13L,0xDC676E10L,0x548DA899L,(-3L),0x820E1544L},{0x9E257F13L,(-1L),0xB13263B4L,0x7FAA3396L,(-1L)},{0x31BB3278L,0x548DA899L,0xDF7CDC1EL,(-2L),(-2L)},{1L,0x31BB3278L,1L,0xB13263B4L,(-1L)},{0x8E4164A0L,(-3L),(-2L),(-10L),1L},{0x22B157EBL,1L,0x31BB3278L,(-1L),0x116C03A5L},{6L,0x820E1544L,(-2L),1L,0x6D384D0EL}},{{0x35585BFCL,0x22B157EBL,1L,0x9E257F13L,0x31BB3278L},{1L,(-1L),0xDF7CDC1EL,0L,0x68F279A7L},{0x6D2AC650L,(-3L),0xB13263B4L,(-2L),(-3L)},{(-3L),6L,0x548DA899L,(-2L),(-3L)},{0x8E4164A0L,2L,(-1L),0x26D3E951L,0x68F279A7L},{0xDC676E10L,0x26D3E951L,0x31BB3278L,0x68F279A7L,0x31BB3278L},{0x39427250L,0x39427250L,0x7A03BD1BL,(-6L),0x6D384D0EL}},{{(-10L),1L,0xDC676E10L,2L,0x116C03A5L},{0x26D3E951L,(-1L),(-7L),0xDF7CDC1EL,1L},{0x8D5EF216L,1L,0L,(-2L),(-1L)},{0x548DA899L,0x39427250L,0x5845684AL,1L,(-2L)},{0x8E4164A0L,0x26D3E951L,8L,0x35585BFCL,(-1L)},{0x5845684AL,2L,0x31BB3278L,(-1L),0x820E1544L},{0x820E1544L,6L,0x18B6A2E5L,(-1L),0x6D384D0EL}},{{(-3L),(-3L),(-3L),0x35585BFCL,6L},{2L,(-1L),1L,1L,(-1L)},{0x116C03A5L,0x22B157EBL,(-2L),(-2L),0x18B6A2E5L},{(-3L),0x820E1544L,0x22B157EBL,0xDF7CDC1EL,6L},{0x8E4164A0L,1L,6L,2L,(-1L)},{(-3L),(-3L),0x31BB3278L,(-6L),0x6D2AC650L},{0x116C03A5L,0x31BB3278L,(-3L),0x68F279A7L,0x6D384D0EL}},{{2L,0x548DA899L,(-3L),0x26D3E951L,0x8D5EF216L},{(-3L),(-1L),0x7FAA3396L,(-2L),(-6L)},{0x820E1544L,0xDC676E10L,0x7FAA3396L,(-2L),8L},{0x5845684AL,0x116C03A5L,(-3L),0L,0x7A03BD1BL},{0x8E4164A0L,0x9E257F13L,(-3L),0x9E257F13L,0x8E4164A0L},{0x548DA899L,(-10L),0x31BB3278L,1L,0x39427250L},{0x8D5EF216L,0x6D2AC650L,6L,(-1L),0x6D384D0EL}},{{0x26D3E951L,(-3L),0x22B157EBL,(-10L),0x39427250L},{1L,0L,(-2L),(-1L),(-2L)},{0x8162A236L,(-1L),0x7A03BD1BL,0x6D2AC650L,0x820E1544L},{(-1L),(-4L),(-6L),6L,0x8D5EF216L},{(-2L),(-3L),0x31BB3278L,0x5845684AL,(-7L)},{0x8E4164A0L,(-3L),0xDF074290L,0x7FAA3396L,(-4L)},{0xB47BFFDEL,(-4L),0x8D5EF216L,(-2L),(-3L)}},{{0x548DA899L,(-1L),(-1L),0x548DA899L,0xB47BFFDEL},{(-3L),0L,(-3L),8L,0xDF7CDC1EL},{4L,(-6L),8L,0x6D2AC650L,6L},{(-1L),0xB47BFFDEL,(-1L),8L,0x31BB3278L},{(-2L),1L,0x820E1544L,0x548DA899L,0L},{1L,0x22B157EBL,0xDF074290L,(-2L),4L},{0xDF074290L,0x2B252635L,0x39427250L,0x7FAA3396L,(-3L)}}};
    int8_t l_3211[3][9][6] = {{{1L,0x93L,(-1L),(-3L),2L,(-3L)},{0x20L,0x6AL,0x20L,(-3L),0x68L,1L},{1L,0x6AL,(-1L),1L,2L,1L},{0x20L,0x93L,0x20L,1L,0x68L,(-3L)},{1L,0x93L,(-1L),(-3L),2L,(-3L)},{0x20L,0x6AL,0x20L,(-3L),0x68L,1L},{1L,0x6AL,(-1L),1L,2L,1L},{0x20L,0x93L,0x20L,1L,0x68L,(-3L)},{1L,0x93L,(-1L),(-3L),2L,(-3L)}},{{0x20L,0x6AL,0x20L,(-3L),0x68L,1L},{1L,0x6AL,(-1L),1L,2L,1L},{0x20L,0x93L,0x20L,1L,0x68L,(-3L)},{1L,0x93L,(-1L),(-3L),2L,(-3L)},{0x20L,0x6AL,0x20L,(-3L),0x68L,1L},{1L,0x6AL,(-1L),1L,2L,1L},{0x20L,0x93L,0x20L,1L,0x68L,(-3L)},{1L,0x93L,(-1L),(-3L),2L,(-3L)},{0x20L,0x6AL,0x20L,(-3L),0x68L,1L}},{{1L,0x6AL,(-1L),1L,2L,1L},{0x20L,0x93L,0x20L,1L,0x68L,(-3L)},{1L,0x93L,(-1L),(-3L),2L,(-3L)},{(-6L),1L,(-6L),0xA7L,0x20L,(-9L)},{1L,1L,0xBFL,(-9L),1L,(-9L)},{(-6L),(-3L),(-6L),(-9L),0x20L,0xA7L},{1L,(-3L),0xBFL,0xA7L,1L,0xA7L},{(-6L),1L,(-6L),0xA7L,0x20L,(-9L)},{1L,1L,0xBFL,(-9L),1L,(-9L)}}};
    uint16_t l_3212 = 0x8DDAL;
    int16_t l_3216 = 5L;
    uint32_t l_3217 = 18446744073709551615UL;
    int32_t l_3220 = 0L;
    union U0 *****l_3263 = &g_3262[3];
    int32_t l_3271 = 0x75CAD273L;
    union U0 *l_3344 = (void*)0;
    uint16_t *** const l_3386 = (void*)0;
    int64_t **l_3413 = &g_1972[4][0];
    uint32_t ** const *l_3481 = (void*)0;
    uint32_t l_3502[4][2] = {{0xC3D99A80L,0xC3D99A80L},{0xC3D99A80L,0xC3D99A80L},{0xC3D99A80L,0xC3D99A80L},{0xC3D99A80L,0xC3D99A80L}};
    int32_t l_3528 = (-1L);
    uint64_t *l_3541 = &g_248;
    int32_t l_3542[3];
    uint64_t **l_3587[10][8][2] = {{{&g_71,&l_3541},{&l_3541,&g_71},{&g_71,&l_3541},{&g_71,(void*)0},{&l_3541,(void*)0},{&g_71,&l_3541},{&g_71,&g_71},{&l_3541,&l_3541}},{{&g_71,&g_71},{&g_3543,&l_3541},{(void*)0,&l_3541},{&g_71,&g_3543},{&g_71,(void*)0},{&g_71,&g_3543},{&g_71,(void*)0},{(void*)0,&l_3541}},{{(void*)0,&g_71},{&g_71,&g_3543},{&g_71,&l_3541},{&l_3541,(void*)0},{&l_3541,&l_3541},{&g_3543,&g_3543},{&g_71,&g_71},{(void*)0,&g_3543}},{{&l_3541,&l_3541},{&g_71,(void*)0},{&g_3543,&l_3541},{&l_3541,&g_3543},{(void*)0,&l_3541},{&l_3541,(void*)0},{&g_71,&g_3543},{(void*)0,(void*)0}},{{&g_71,&g_3543},{&l_3541,&g_3543},{&g_71,&g_71},{&g_71,&g_71},{&g_71,&g_71},{&g_71,&g_71},{&g_71,&g_71},{&g_71,&g_3543}},{{&l_3541,&g_3543},{&g_71,(void*)0},{(void*)0,&g_3543},{&g_71,(void*)0},{&l_3541,&l_3541},{(void*)0,&g_3543},{&l_3541,&l_3541},{&g_3543,(void*)0}},{{&g_71,&l_3541},{&l_3541,&g_3543},{(void*)0,&g_71},{&g_71,&g_3543},{&g_3543,&l_3541},{&l_3541,(void*)0},{&l_3541,&l_3541},{&g_71,&g_3543}},{{&g_71,&g_71},{(void*)0,&l_3541},{(void*)0,(void*)0},{&g_71,&g_3543},{&g_71,(void*)0},{&g_71,&g_3543},{&g_71,&l_3541},{(void*)0,&l_3541}},{{&g_3543,&g_71},{&g_71,&l_3541},{&l_3541,&g_71},{&g_71,&l_3541},{&g_71,(void*)0},{&l_3541,(void*)0},{&g_71,&l_3541},{&g_71,&g_71}},{{&l_3541,&l_3541},{&g_71,(void*)0},{&g_71,&g_71},{&g_3543,&g_71},{&l_3541,&l_3541},{&l_3541,&g_71},{&g_3543,&g_71},{(void*)0,&g_3543}}};
    uint64_t ** const *l_3586 = &l_3587[1][0][0];
    uint64_t ** const **l_3585 = &l_3586;
    const int16_t l_3603[4] = {0L,0L,0L,0L};
    int32_t **l_3626 = &g_943;
    int16_t *l_3646 = &g_1867;
    int16_t **l_3645 = &l_3646;
    int16_t ***l_3644[5];
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
                l_44[i][j][k] = 4294967286UL;
        }
    }
    for (i = 0; i < 3; i++)
        l_3542[i] = 0x81997350L;
    for (i = 0; i < 5; i++)
        l_3644[i] = &l_3645;
    g_7[3][2][6] = g_5;
    l_1278 = ((*g_513) = (func_8(func_14(func_17((safe_unary_minus_func_uint16_t_u(((safe_mod_func_uint32_t_u_u(1UL, (safe_rshift_func_int16_t_s_u((safe_add_func_int32_t_s_s((func_30(g_36, (p_3 = 0xCAL), (((((safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s((func_42(l_44[0][0][0]) || l_44[0][0][0]), (((g_1274 == ((safe_div_func_uint16_t_u_u((((*l_1278) = l_44[0][0][0]) ^ (((*l_1281) &= ((void*)0 != &g_252)) | l_44[0][0][1])), 1UL)) , &g_1275)) , (*l_1278)) , 255UL))), g_387)) && (*l_1278)) == 0x5B59L) & g_851) & 4294967295UL), l_44[0][0][0], g_273) , 0L), l_44[0][0][0])), l_44[0][0][0])))) == l_1299))), g_1221, l_1278, l_1300, l_44[0][0][0]), g_1362[0]), l_1300, l_44[0][0][0], l_1280, l_44[0][0][1]) , l_1278));
    if ((p_3 | ((safe_rshift_func_uint16_t_u_u(((((safe_sub_func_uint64_t_u_u((p_3 ^ (safe_sub_func_int8_t_s_s((*g_75), ((*l_1278) = l_3201[1][1][0])))), ((((((((safe_add_func_int32_t_s_s((p_3 || ((p_3 , ((safe_sub_func_uint16_t_u_u(65535UL, (safe_add_func_int32_t_s_s((safe_div_func_uint16_t_u_u(((l_44[0][0][1] , g_3210) , 1UL), p_3)), p_3)))) != p_3)) != p_3)), l_3201[4][6][2])) >= (*g_71)) == 0L) >= 0x1F6933D9E6E5BAEELL) , 0x61ACC7EEL) , l_1300) >= l_3211[1][2][2]) & l_3212))) && p_3) == p_3) , p_3), 13)) < l_3212)))
    { /* block id: 1469 */
        int32_t *l_3213 = &g_799;
        int32_t *l_3214 = (void*)0;
        int32_t *l_3215[5][9] = {{&g_37,&g_47[1][2][0],&g_799,&g_47[1][2][0],&g_37,&g_37,&g_47[1][2][0],&g_799,&g_47[1][2][0]},{(void*)0,(void*)0,&g_1279[2],&g_1279[2],(void*)0,(void*)0,(void*)0,&g_1279[2],&g_1279[2]},{&g_37,&g_37,&g_47[1][2][0],&g_799,&g_47[1][2][0],&g_37,&g_37,&g_47[1][2][0],&g_799},{&g_1279[1],(void*)0,&g_1279[1],(void*)0,(void*)0,&g_1279[1],(void*)0,&g_1279[1],(void*)0},{&g_1279[0],&g_47[1][2][0],&g_47[1][2][0],&g_1279[0],(void*)0,&g_1279[0],&g_47[1][2][0],&g_47[1][2][0],&g_1279[0]}};
        uint32_t l_3221[1][10] = {{18446744073709551615UL,18446744073709551615UL,0xFE4744B2L,18446744073709551615UL,18446744073709551615UL,0xFE4744B2L,18446744073709551615UL,18446744073709551615UL,0xFE4744B2L,18446744073709551615UL}};
        int32_t *l_3252[4][10][6] = {{{&g_1279[0],&g_47[3][5][1],(void*)0,&l_3201[1][1][0],&l_3201[1][1][0],&g_1279[0]},{&g_1279[1],&g_47[0][7][3],&l_3201[1][1][0],&l_3201[1][1][0],&l_3201[1][5][4],&g_47[3][5][3]},{&g_47[5][6][0],&l_3201[1][1][0],&g_1279[1],&g_1279[1],&l_3201[1][1][0],&l_3201[1][1][0]},{&l_3201[1][1][0],&g_1279[0],&g_37,&l_3201[1][1][0],&g_47[3][5][3],&g_47[3][5][3]},{&g_47[3][5][0],&g_47[3][5][3],&g_47[3][5][3],&g_47[3][5][0],(void*)0,&g_37},{&l_3201[1][1][0],&l_3201[1][1][0],&g_47[3][5][3],&g_1279[2],&g_47[3][5][3],&g_47[6][8][0]},{&l_3201[1][1][0],&l_3201[1][5][4],&g_37,&l_3201[5][3][1],&g_47[3][5][3],(void*)0},{&g_47[3][5][3],&l_3201[1][1][0],&l_3201[1][1][0],(void*)0,(void*)0,&l_3201[2][0][2]},{(void*)0,&g_47[3][5][3],&l_3201[1][1][0],&g_47[3][5][3],&g_47[3][5][3],&l_3201[1][1][0]},{&l_3201[1][5][4],&g_1279[0],&l_3201[1][1][0],&g_47[3][5][3],&l_3201[1][1][0],&l_3201[5][3][1]}},{{(void*)0,&l_3201[1][1][0],(void*)0,&g_1279[1],&l_3201[1][5][4],&g_47[3][5][3]},{&g_1279[1],&g_47[0][7][3],&g_47[3][5][3],&l_3201[1][5][4],&l_3201[1][1][0],&g_1279[1]},{&l_3201[1][1][0],&g_47[3][5][1],&l_3201[5][3][1],&g_47[2][7][3],&g_47[4][2][3],&g_47[0][7][3]},{&l_3201[5][3][1],(void*)0,&g_1279[1],&g_47[6][8][0],&g_47[3][5][1],&g_1279[1]},{&g_37,&l_3201[1][1][0],(void*)0,&g_1279[1],&g_1279[1],&l_3201[4][3][2]},{&g_47[6][8][0],&l_3201[1][1][0],(void*)0,&l_3201[1][1][0],&g_47[6][8][0],&g_37},{&g_47[3][5][3],&l_3201[1][1][0],(void*)0,(void*)0,&g_37,&g_47[3][5][3]},{&g_47[2][3][0],&g_47[5][6][0],&g_47[3][5][1],&l_3201[1][1][0],(void*)0,&g_47[3][5][3]},{&g_47[4][6][3],&l_3201[1][1][0],(void*)0,&g_47[3][5][3],&l_3201[1][1][0],&g_37},{(void*)0,&g_1279[1],(void*)0,&g_1279[1],(void*)0,&l_3201[1][1][0]}},{{&g_47[5][6][0],&g_47[4][2][3],&g_47[3][5][3],&g_47[6][8][0],&l_3201[1][1][0],&g_47[3][5][3]},{&l_3201[1][1][0],(void*)0,&g_47[3][5][3],(void*)0,&g_47[3][5][3],&g_1279[1]},{&l_3201[1][1][0],&g_47[3][5][3],&l_3201[5][1][4],&l_3220,&l_3201[4][3][2],&l_3201[1][1][0]},{&g_47[3][5][3],(void*)0,&g_47[4][8][2],&g_37,(void*)0,&g_37},{&l_3201[1][1][0],&l_3201[1][1][0],(void*)0,&g_47[4][2][3],&l_3201[1][1][0],&l_3201[5][1][4]},{&l_3201[5][3][1],&g_47[3][5][3],&g_1279[1],&g_47[3][5][3],&g_1279[1],(void*)0},{(void*)0,&g_47[0][7][3],&l_3201[1][1][0],&l_3201[1][1][0],&g_47[0][7][3],(void*)0},{(void*)0,&g_47[3][1][2],(void*)0,&g_37,&l_3201[1][5][4],&l_3201[0][2][1]},{(void*)0,&l_3220,&l_3201[1][1][0],&g_47[3][5][3],&g_47[3][5][3],&g_1279[0]},{(void*)0,&g_47[2][3][0],&g_47[3][5][3],&g_37,&g_37,&g_47[5][6][0]}},{{(void*)0,&l_3201[1][5][4],&g_37,&l_3201[1][1][0],&g_47[3][5][3],&l_3201[1][1][0]},{(void*)0,(void*)0,(void*)0,&g_47[3][5][3],&g_47[5][6][0],&g_1279[1]},{&l_3201[5][3][1],&l_3201[1][1][0],&g_47[2][3][0],&g_47[4][2][3],&g_1279[2],&g_47[3][5][3]},{&l_3201[1][1][0],&g_47[3][5][0],(void*)0,&g_37,&l_3201[1][1][0],&g_47[3][5][1]},{&g_47[3][5][3],&g_37,&g_1279[1],&l_3220,&g_47[3][5][3],&g_47[6][8][0]},{&l_3201[1][1][0],&l_3201[1][1][0],&g_1279[1],(void*)0,&g_47[3][5][3],&g_1279[2]},{&l_3201[1][1][0],(void*)0,&g_47[4][2][3],&g_47[6][8][0],&g_47[3][1][2],&l_3201[1][1][0]},{&g_47[5][6][0],&l_3201[1][1][0],&l_3201[5][3][1],&g_47[2][3][0],&l_3201[5][3][1],&l_3201[1][1][0]},{&g_47[3][5][3],&g_47[3][5][3],&g_1279[1],&g_1279[2],(void*)0,&g_47[0][7][3]},{&g_47[0][7][3],&g_47[3][5][3],&l_3201[4][3][2],&g_1279[1],(void*)0,&l_3201[1][1][0]}}};
        const int16_t l_3317 = (-7L);
        int i, j, k;
        l_3217--;
        --l_3221[0][6];
        for (g_1867 = 0; (g_1867 < 28); g_1867 = safe_add_func_uint32_t_u_u(g_1867, 2))
        { /* block id: 1474 */
            const int32_t l_3226[9][8] = {{(-1L),0x28FCA9B1L,0xB3ABC4C3L,(-4L),0x5711B5A2L,0x5711B5A2L,(-4L),0xB3ABC4C3L},{(-1L),(-1L),0L,0xADE31D9EL,(-4L),0xFA35D19CL,(-1L),(-4L)},{0x0ECB3BDEL,(-4L),0x28FCA9B1L,0x0ECB3BDEL,4L,0x0ECB3BDEL,0x28FCA9B1L,(-4L)},{(-4L),0x3616BB95L,0xB3ABC4C3L,0xADE31D9EL,0x3616BB95L,0x28FCA9B1L,4L,0xB3ABC4C3L},{0x3A474148L,4L,(-10L),(-4L),(-4L),(-10L),4L,0x3A474148L},{0x5711B5A2L,(-4L),0xB3ABC4C3L,0x28FCA9B1L,(-1L),0x5711B5A2L,0x28FCA9B1L,0xADE31D9EL},{(-1L),0x5711B5A2L,0x28FCA9B1L,0xADE31D9EL,0x28FCA9B1L,0x5711B5A2L,(-1L),0x28FCA9B1L},{0x3A474148L,(-4L),0L,0x3A474148L,4L,(-10L),(-4L),(-4L)},{0x28FCA9B1L,4L,0xB3ABC4C3L,0xB3ABC4C3L,4L,(-10L),0xFA35D19CL,0x3616BB95L}};
            float l_3232 = 0x1.Ap+1;
            int32_t l_3250[8][4] = {{0xC49D2BF1L,1L,1L,0xC49D2BF1L},{1L,0xC49D2BF1L,1L,1L},{0xC49D2BF1L,0xC49D2BF1L,1L,0xC49D2BF1L},{0xC49D2BF1L,1L,1L,0xC49D2BF1L},{1L,0xC49D2BF1L,1L,1L},{0xC49D2BF1L,0xC49D2BF1L,1L,0xC49D2BF1L},{0xC49D2BF1L,1L,1L,0xC49D2BF1L},{1L,0xC49D2BF1L,1L,1L}};
            int16_t *l_3253 = &l_3216;
            int16_t *l_3254 = &g_851;
            int32_t l_3255 = 0xD631B835L;
            union U0 *****l_3264 = &g_3262[2];
            int8_t **l_3323 = &g_75;
            int64_t l_3326 = 0x8ABFA32C82F9C13ALL;
            uint16_t l_3369[5] = {65535UL,65535UL,65535UL,65535UL,65535UL};
            const uint16_t *l_3383 = &l_3369[3];
            const uint16_t **l_3382 = &l_3383;
            const uint16_t ** const *l_3381 = &l_3382;
            int32_t l_3387 = 7L;
            int32_t l_3390[6];
            int32_t ***l_3452 = (void*)0;
            int32_t **** const l_3451[3] = {&l_3452,&l_3452,&l_3452};
            int i, j;
            for (i = 0; i < 6; i++)
                l_3390[i] = 0x586EFA2EL;
        }
    }
    else
    { /* block id: 1586 */
        int8_t *l_3470 = (void*)0;
        int8_t ** const l_3469 = &l_3470;
        int8_t ** const *l_3468 = &l_3469;
        int32_t l_3476 = 2L;
        int32_t *l_3479 = (void*)0;
        int32_t *l_3480 = &g_37;
        uint32_t ** const **l_3482 = (void*)0;
        uint32_t ** const **l_3483[5];
        int16_t *l_3487 = &g_851;
        int32_t l_3499 = 0x0C3244F0L;
        int32_t l_3500 = (-1L);
        int32_t l_3501[3];
        int32_t ***l_3533[5][2];
        int16_t **l_3643 = &l_3487;
        int16_t ***l_3642[5];
        float l_3649 = 0x1.9p-1;
        float *****l_3672 = &g_1490;
        int32_t l_3681 = 0x44B3BCB0L;
        int i, j;
        for (i = 0; i < 5; i++)
            l_3483[i] = &l_3481;
        for (i = 0; i < 3; i++)
            l_3501[i] = 0x5F89CCB7L;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 2; j++)
                l_3533[i][j] = (void*)0;
        }
        for (i = 0; i < 5; i++)
            l_3642[i] = &l_3643;
        (*g_5) = (safe_add_func_int32_t_s_s(0x99714DC2L, (safe_rshift_func_uint8_t_u_u(((l_3468 = g_3466) == (((**l_3413) ^= (safe_div_func_int32_t_s_s(((!(safe_div_func_int16_t_s_s(((*g_943) , ((*l_3487) = ((g_3484[1] = (((*l_3480) ^= ((*g_352) = ((((p_3 || ((l_3476 , (safe_sub_func_uint64_t_u_u((*l_1278), 8UL))) && 0x12L)) , (l_3476 , p_3)) & l_3476) >= p_3))) , l_3481)) != (void*)0))), l_3216))) & p_3), l_44[0][0][0]))) , &g_3467)), p_3))));
        (*g_3488) = (*g_351);
        for (g_1728 = 0; (g_1728 <= 9); g_1728 += 1)
        { /* block id: 1597 */
            uint16_t l_3491 = 0UL;
            int32_t l_3496 = 0x9444986AL;
            int32_t l_3498[7] = {1L,0x30E46AB3L,1L,1L,0x30E46AB3L,1L,1L};
            uint8_t l_3537 = 3UL;
            int8_t *l_3544 = &g_241;
            uint32_t *l_3569 = &l_3217;
            uint64_t l_3570 = 0xF772966F4B289983LL;
            float *l_3608 = &g_1190;
            int32_t ***l_3658[3];
            int i;
            for (i = 0; i < 3; i++)
                l_3658[i] = &g_2260[0][0];
            (*l_3480) &= ((18446744073709551606UL <= (*g_71)) , (255UL & (safe_lshift_func_int16_t_s_s(p_3, l_3491))));
            for (p_3 = 0; (p_3 <= 0); p_3 += 1)
            { /* block id: 1601 */
                return p_3;
            }
            if ((*l_1278))
            { /* block id: 1604 */
                int8_t l_3492 = (-1L);
                int32_t *l_3493 = (void*)0;
                int32_t l_3494 = 0x1F21D319L;
                int32_t *l_3495[5][9] = {{&g_1279[1],(void*)0,&g_47[3][5][3],(void*)0,&g_1279[1],&g_47[3][5][3],&g_1279[0],&g_1279[1],&g_47[3][5][3]},{(void*)0,&g_47[3][5][3],&g_47[3][5][3],&g_1279[0],(void*)0,&g_47[3][5][3],&g_1279[0],(void*)0,&g_47[4][3][1]},{&g_47[3][5][3],(void*)0,&g_47[3][5][3],(void*)0,(void*)0,&g_47[3][5][3],(void*)0,&g_47[3][5][3],&g_47[3][5][3]},{(void*)0,&g_1279[1],&g_47[3][5][3],&g_1279[0],&g_1279[1],&g_47[3][5][3],(void*)0,(void*)0,&l_3201[1][1][0]},{(void*)0,(void*)0,&g_47[3][5][3],(void*)0,&g_47[3][5][3],&l_3201[1][1][0],&g_47[3][5][3],(void*)0,&g_47[3][5][3]}};
                float l_3497 = (-0x3.8p+1);
                uint64_t l_3529 = 0x2F64AFDF3F3DBA3CLL;
                int i, j;
                l_3502[1][1]--;
                for (l_3492 = 9; (l_3492 >= 0); l_3492 -= 1)
                { /* block id: 1608 */
                    int64_t l_3508 = 0x9C769C4DDFF22D07LL;
                    uint32_t l_3509 = 5UL;
                    int32_t l_3524 = 6L;
                    int32_t l_3525 = 0x27B89A8AL;
                    int8_t l_3526 = 0x0BL;
                    int32_t l_3527 = 0xC25E6C5BL;
                    if (p_3)
                    { /* block id: 1609 */
                        int16_t l_3505 = (-5L);
                        int32_t l_3506 = 0x53A4C54EL;
                        int32_t l_3507[3];
                        uint32_t ** const *l_3521 = &g_1338;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_3507[i] = 0x405D3F2AL;
                        ++l_3509;
                        l_3506 = (safe_div_func_float_f_f((l_3524 = ((((*g_273) = p_3) > ((void*)0 != &g_2684)) >= ((g_2652 = (+((g_732 = (safe_add_func_float_f_f(((safe_add_func_float_f_f(l_3498[2], (safe_mul_func_float_f_f(l_3509, (l_3521 != (void*)0))))) < p_3), (safe_add_func_float_f_f((*g_1237), 0xD.401C56p-33))))) != l_3509))) < p_3))), p_3));
                        (*g_5) |= l_3509;
                        l_3529++;
                    }
                    else
                    { /* block id: 1618 */
                        int32_t *l_3532 = (void*)0;
                        (*g_352) ^= (p_3 == ((void*)0 == l_3532));
                    }
                    for (g_252 = 0; (g_252 <= 0); g_252 += 1)
                    { /* block id: 1623 */
                        l_3525 = 0x0.6p-1;
                        (*l_3480) &= (*l_1278);
                        (*g_5) = 0x47C30F15L;
                    }
                    for (g_252 = 1; (g_252 <= 9); g_252 += 1)
                    { /* block id: 1630 */
                        int32_t l_3536 = (-1L);
                        if (p_3)
                            break;
                        (*g_3454) = l_3533[4][0];
                        if (p_3)
                            continue;
                        (*l_3480) |= ((l_3536 >= ((l_3496 = ((0xE2L || (l_3498[5] && (**g_3485))) , (**g_344))) > ((*l_1278) = (*g_1691)))) && l_3498[5]);
                    }
                    for (l_3491 = 0; (l_3491 <= 9); l_3491 += 1)
                    { /* block id: 1640 */
                        int32_t l_3538 = 2L;
                        (**g_272) = l_3537;
                        if (p_3)
                            continue;
                        if (l_3538)
                            continue;
                        if (p_3)
                            continue;
                    }
                }
                if (((safe_mul_func_int16_t_s_s((-5L), 0x496AL)) || (l_3491 && p_3)))
                { /* block id: 1647 */
                    uint16_t l_3545[8] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
                    int32_t *l_3546 = &l_3501[0];
                    int i;
                    l_3546 = func_66(l_3541, func_66((l_3542[1] , g_3543), &l_3220, l_3544, p_3), &p_3, (l_3545[3] , p_3));
                    for (l_3212 = 0; (l_3212 <= 2); l_3212 += 1)
                    { /* block id: 1651 */
                        (*g_273) = ((-0x6.3p+1) > ((*l_3546) = (safe_div_func_float_f_f((**g_1236), 0x1.Ep+1))));
                    }
                    l_3496 &= (safe_sub_func_uint32_t_u_u(((**g_3485) = (+(((++(**g_457)) , 0xB429L) , (safe_add_func_uint64_t_u_u((1L & (safe_mod_func_int64_t_s_s(0xCFBC452C87FC1AA7LL, (safe_mul_func_uint8_t_u_u((4294967291UL | (((g_454[g_1728] |= (0x6DA3C36BL < p_3)) , (*g_3543)) != ((safe_add_func_int8_t_s_s(((*l_1278) = 0xE0L), ((((0x0.2p+1 < 0x0.5p+1) , 0L) < p_3) > (****g_2574)))) || p_3))), p_3))))), (*l_3546)))))), l_3498[1]));
                }
                else
                { /* block id: 1660 */
                    uint32_t *l_3568 = &l_3217;
                    int32_t l_3579 = (-2L);
                    int32_t l_3581 = 0x578BC970L;
                    if ((0L != (safe_lshift_func_int16_t_s_u(((p_3 & ((((safe_div_func_uint64_t_u_u((((**l_3413) = (l_3479 == (g_1802[1] = (p_3 , (l_3569 = l_3568))))) , ((l_3570 <= ((((safe_mod_func_int32_t_s_s(((((safe_lshift_func_uint16_t_u_u((p_3 , ((((((((*l_1278) = (safe_lshift_func_int8_t_s_s((safe_mod_func_uint32_t_u_u((p_3 , l_3579), 4L)), p_3))) >= p_3) , 0xC97EL) != (-7L)) , g_3580[2][5][0]) , (*****g_2573)) , 0xDD64L)), l_3579)) , (void*)0) == &l_3491) ^ p_3), p_3)) , 0UL) >= (*g_71)) | p_3)) >= 9UL)), l_3579)) | p_3) , 0x15L) > 246UL)) == 0L), 15))))
                    { /* block id: 1665 */
                        uint64_t l_3582 = 1UL;
                        l_3582--;
                    }
                    else
                    { /* block id: 1667 */
                        uint64_t ***l_3589 = &l_3587[1][0][0];
                        uint64_t ****l_3588 = &l_3589;
                        uint8_t **l_3600 = (void*)0;
                        uint16_t *l_3601 = &g_2751[7];
                        int32_t l_3602 = 0L;
                        (*g_273) = l_3491;
                        (*g_5) = (l_3585 != ((**g_942) , l_3588));
                        (*g_351) = func_66((g_3543 = &g_252), (g_3590[4] , (*g_1347)), &l_3492, (safe_rshift_func_uint16_t_u_u((safe_add_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s((safe_sub_func_int32_t_s_s(((!(((*l_3601) = ((void*)0 == l_3600)) | l_3602)) >= ((l_3603[1] == (safe_sub_func_int64_t_s_s(((&g_3454 == &g_3454) , p_3), l_3602))) >= (*l_1278))), l_3581)), (*l_3480))), 0L)), 1)));
                    }
                }
            }
            else
            { /* block id: 1675 */
                (*l_1278) = 0L;
            }
            (****g_1234) = ((*l_1278) = (l_3570 > (safe_mul_func_float_f_f(((*l_3608) = (((*g_273) = (l_3569 != l_3569)) == p_3)), p_3))));
            for (g_1314 = 0; (g_1314 >= 0); g_1314 -= 1)
            { /* block id: 1684 */
                int32_t l_3609 = 0x0A4E51F1L;
                int32_t l_3610 = 7L;
                int32_t l_3611 = (-1L);
                int32_t l_3612[7][7] = {{0x408E463EL,0xB5F99F40L,0x804CEB5CL,(-9L),1L,0x804CEB5CL,0xAE2B3FA1L},{0xF48695A3L,0xB5F99F40L,0xD7473DA6L,1L,1L,0xD7473DA6L,0xB5F99F40L},{0xF48695A3L,0xAE2B3FA1L,0x804CEB5CL,1L,(-9L),0x804CEB5CL,0xB5F99F40L},{0x408E463EL,0xB5F99F40L,0x804CEB5CL,(-9L),1L,0x804CEB5CL,0xAE2B3FA1L},{0xF48695A3L,0xB5F99F40L,0xD7473DA6L,1L,1L,0xD7473DA6L,0xB5F99F40L},{0xF48695A3L,0xAE2B3FA1L,0x804CEB5CL,1L,(-9L),0x804CEB5CL,0xB5F99F40L},{0x408E463EL,0xB5F99F40L,0x804CEB5CL,(-9L),1L,0x804CEB5CL,0xAE2B3FA1L}};
                uint32_t l_3613 = 0x2C1732A7L;
                int32_t *l_3617 = &l_3501[2];
                int64_t l_3619 = 7L;
                union U0 *l_3623 = &g_99;
                int32_t ****l_3651[4] = {&l_3533[4][0],&l_3533[4][0],&l_3533[4][0],&l_3533[4][0]};
                int i, j;
                --l_3613;
                if ((+l_3498[1]))
                { /* block id: 1686 */
                    uint64_t ***l_3622 = &l_3587[1][0][0];
                    uint64_t ****l_3621 = &l_3622;
                    int32_t l_3650 = 0xC7CAC4C6L;
                    int32_t l_3652 = 2L;
                    l_3617 = &l_3609;
                    for (g_37 = 0; (g_37 >= 0); g_37 -= 1)
                    { /* block id: 1690 */
                        int32_t *l_3618 = &l_3496;
                        l_1278 = l_3618;
                        if (l_3619)
                            continue;
                    }
                    for (g_252 = 0; (g_252 <= 2); g_252 += 1)
                    { /* block id: 1696 */
                        int32_t **l_3625 = &g_943;
                        int32_t ***l_3627 = &l_3625;
                        int i;
                        (*l_3617) = (((((*l_3569) |= g_1279[(g_1314 + 1)]) , p_3) | ((*l_1278) , (g_1279[(g_1314 + 1)] <= ((((!0x1.94F129p+3) , (((((void*)0 == l_3621) != ((l_3623 == (*g_1274)) <= 0x2.1p-1)) != (-0x3.2p+1)) , 0UL)) , p_3) || g_3624)))) == 0x4D32B6C114A2C55ALL);
                        (*l_3627) = (l_3626 = l_3625);
                        l_3652 ^= (safe_sub_func_uint32_t_u_u(0x62CA2221L, ((((5UL < (safe_sub_func_uint16_t_u_u(((((((*l_1278) == (safe_div_func_uint64_t_u_u(((((safe_add_func_int64_t_s_s((1L & (((safe_lshift_func_int8_t_s_u(p_3, 7)) <= (safe_unary_minus_func_uint64_t_u((+((safe_div_func_int64_t_s_s(((((l_3644[0] = l_3642[1]) == (void*)0) , ((safe_div_func_int8_t_s_s((0x8D2FFD58L == p_3), 0xE6L)) <= p_3)) , p_3), (***g_2575))) && g_1279[(g_1314 + 1)]))))) == 0x63L)), (*l_3617))) ^ 0x00574D4DL) ^ l_3650) >= (**g_2812)), p_3))) , 0L) , (void*)0) == l_3651[0]) & l_3498[3]), (*l_3480)))) != (**g_3414)) , 0x992EL) , l_3650)));
                    }
                }
                else
                { /* block id: 1704 */
                    uint64_t l_3662 = 0x74ECFBDFF54F82C9LL;
                    int32_t *l_3663 = &g_47[3][5][3];
                    (*g_36) |= 7L;
                    for (g_96 = 0; (g_96 <= 0); g_96 += 1)
                    { /* block id: 1708 */
                        uint16_t l_3661 = 9UL;
                        (*g_352) = (safe_unary_minus_func_int32_t_s((((*l_3480) , ((((&l_3537 == (*g_344)) | ((*l_3617) ^= p_3)) != (((safe_sub_func_uint16_t_u_u((safe_add_func_int16_t_s_s((((**g_3414) &= (0x07BAL || ((((*g_3486) = (((l_3658[2] != (*g_3454)) & p_3) && ((((+(!1UL)) >= p_3) & g_1279[2]) < l_3661))) < l_3662) < 0xA778A9A1L))) , l_3662), p_3)), 0x90DCL)) == p_3) != (*g_75))) == 65534UL)) || 0xD2D3C5CA800AD866LL)));
                        if (p_3)
                            break;
                    }
                    (*g_2812) = (l_3663 = &l_3201[1][1][0]);
                }
                return (**g_511);
            }
        }
        for (g_96 = 0; (g_96 <= 46); g_96++)
        { /* block id: 1723 */
            uint32_t l_3668 = 18446744073709551609UL;
            float *****l_3671 = &g_1490;
            for (g_1867 = 0; (g_1867 <= 12); g_1867 = safe_add_func_int16_t_s_s(g_1867, 5))
            { /* block id: 1726 */
                int32_t l_3682[1][3];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_3682[i][j] = 1L;
                }
                l_3682[0][2] = (((0L == (p_3 ^ 1L)) < ((((((l_3668 && (safe_rshift_func_uint8_t_u_s(((l_3672 = l_3671) == &g_1234), 5))) && (1L && 0xDB961E20L)) && ((*****g_2573) = (safe_div_func_int32_t_s_s((((**l_3643) = (safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((((safe_lshift_func_uint16_t_u_u(p_3, 9)) > 0x5D1EEB24EAF77649LL) , p_3), p_3)), p_3))) & 65535UL), 0x25D759CBL)))) || p_3) , l_3681) & (**g_879))) < p_3);
            }
            (*g_352) = (**g_1585);
        }
    }
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_2574 g_2575 g_2576 g_71 g_72 g_75 g_76 g_808 g_399 g_1274 g_1275 g_2551 g_2552 g_1548 g_457 g_458 g_96 g_2684 g_5 g_2702 g_2573 g_6 g_103 g_2751 g_140 g_2768 g_1496 g_879 g_880 g_616 g_942 g_943 g_454 g_352 g_1867 g_2794 g_2195 g_1972 g_344 g_345 g_346 g_2225 g_1234 g_1235 g_1236 g_1237 g_778 g_252 g_273 g_274 g_272 g_2248 g_50 g_513 g_36 g_799 g_47 g_1690 g_1691 g_614 g_2427 g_2194 g_2434 g_2442 g_1540 g_1122 g_2480 g_2518 g_2539 g_2549 g_2550 g_1380 g_2800 g_2802 g_376 g_2812 g_1584 g_2903 g_1362 g_100 g_49 g_374 g_37 g_248 g_241 g_99.f0 g_99 g_491 g_1347 g_3002 g_2690 g_3084 g_1490 g_1491 g_3115 g_3145 g_1279
 * writes: g_1275 g_103 g_376 g_2690 g_6 g_72 g_50 g_76 g_2751 g_140 g_36 g_352 g_851 g_1867 g_2794 g_96 g_2194 g_49 g_778 g_2260 g_381 g_274 g_614 g_399 g_491 g_799 g_1540 g_1221 g_942 g_752 g_2518 g_2542 g_2546 g_2903 g_252 g_374 g_37 g_241 g_248 g_272 g_278 g_100 g_47 g_2248 g_943 g_1971 g_616 g_1279
 */
static uint32_t  func_8(int32_t * p_9, const int64_t  p_10, float  p_11, int8_t * p_12, int8_t  p_13)
{ /* block id: 1248 */
    int32_t l_2658 = 7L;
    int8_t **l_2686 = &g_75;
    uint64_t l_2687 = 1UL;
    int32_t l_2709 = 0x2F5F1C1CL;
    int32_t l_2710 = 0xB9A4D778L;
    int32_t l_2711 = 0x0E5033ACL;
    int32_t l_2712 = 0x50FB7624L;
    int32_t l_2713 = (-5L);
    int32_t l_2714 = (-1L);
    int32_t l_2715 = 0x1B61C34EL;
    int32_t l_2716 = 0xFE1FC0A3L;
    int32_t l_2717 = 1L;
    int32_t l_2718 = 0x8806660CL;
    int32_t l_2719 = (-2L);
    int32_t l_2720[7] = {(-1L),0x4FE8095EL,0x4FE8095EL,(-1L),0x4FE8095EL,0x4FE8095EL,(-1L)};
    float l_2722 = 0x0.A04094p-51;
    int64_t l_2728[7];
    uint8_t ** const l_2738 = &g_458;
    uint32_t l_2745 = 0x168D88BDL;
    uint32_t l_2748 = 0x66A3D019L;
    uint16_t l_2799 = 0x2B6BL;
    int32_t *** const *l_2834 = &g_2690[7][3][0];
    int32_t l_2881 = 0xC5860149L;
    float **l_2997 = (void*)0;
    int32_t *l_3004 = &l_2720[5];
    uint64_t **l_3015[6][5] = {{&g_71,(void*)0,(void*)0,&g_71,&g_71},{&g_71,&g_71,&g_71,&g_71,&g_71},{&g_71,&g_71,(void*)0,(void*)0,&g_71},{&g_71,&g_71,&g_71,&g_71,&g_71},{&g_71,(void*)0,(void*)0,&g_71,&g_71},{&g_71,&g_71,&g_71,&g_71,&g_71}};
    uint64_t ***l_3014[5] = {&l_3015[0][1],&l_3015[0][1],&l_3015[0][1],&l_3015[0][1],&l_3015[0][1]};
    int64_t l_3033 = 0xF858077DE485C050LL;
    int16_t l_3042 = 0xA10FL;
    int32_t l_3081 = 0x86E90B79L;
    uint8_t ** const **l_3093 = &g_1778[2][7][2];
    uint8_t ** const *** const l_3092 = &l_3093;
    uint16_t l_3106[10] = {0x7435L,0x7435L,0x7435L,0x7435L,0x7435L,0x7435L,0x7435L,0x7435L,0x7435L,0x7435L};
    int64_t l_3129 = 0xCD887D9FE4289F79LL;
    int i, j;
    for (i = 0; i < 7; i++)
        l_2728[i] = 1L;
    if (((-1L) && ((safe_rshift_func_uint8_t_u_u((l_2658 == l_2658), 1)) ^ ((safe_lshift_func_uint8_t_u_s(0xF6L, 7)) | (safe_add_func_int64_t_s_s(((((safe_sub_func_uint64_t_u_u((****g_2574), (safe_mod_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((((((((safe_unary_minus_func_int32_t_s(l_2658)) ^ (l_2658 , (*g_75))) , (safe_div_func_uint8_t_u_u(((*g_808) != (void*)0), p_10))) < 1L) , l_2658) == l_2658) && l_2658), 6UL)), l_2658)))) != (*g_71)) == 0xA5D5AAC2DC8A8A75LL) , 1L), p_13))))))
    { /* block id: 1249 */
        uint64_t l_2676 = 0UL;
        uint16_t *l_2677 = &g_103;
        int32_t l_2678 = (-1L);
        int64_t *l_2679 = &g_376;
        int32_t ***l_2688 = &g_942;
        int32_t ****l_2689[3];
        int32_t l_2691[2];
        int32_t l_2700 = 0xAF835EABL;
        int32_t l_2707 = 0x80968FE1L;
        int64_t l_2721 = 0x8165194C85F62088LL;
        uint8_t * const *l_2739 = &g_458;
        float l_2746[3][8][10] = {{{(-0x7.5p+1),0x5.D4052Cp+27,0x6.388EEAp-73,0x4.65A181p+81,0x9.8C8703p+37,(-0x1.4p-1),0x5.0BDFCCp-93,(-0x1.4p-1),0x9.8C8703p+37,0x4.65A181p+81},{0x7.C3C84Ep-68,0x1.0p+1,0x7.C3C84Ep-68,(-0x1.4p-1),0x1.6p+1,0xF.01D16Cp-4,0x5.0BDFCCp-93,0x4.72C393p-68,(-0x10.Ep+1),(-0x1.5p+1)},{0x6.388EEAp-73,0x5.D4052Cp+27,(-0x7.5p+1),0x4.C65947p-74,0x7.C3C84Ep-68,0x7.6p+1,0x0.3p-1,0x4.72C393p-68,0x0.3p-1,0x7.6p+1},{(-0x10.Ep+1),(-0x4.7p-1),0x7.C3C84Ep-68,(-0x4.7p-1),(-0x10.Ep+1),0x2.FBD65Fp+26,(-0x7.5p+1),(-0x1.4p-1),0x0.3p-1,0x5.D4052Cp+27},{0x5.0BDFCCp-93,0x2.FBD65Fp+26,0x6.388EEAp-73,0x4.C65947p-74,0x1.B7ACD4p-90,0x4.72C393p-68,0x1.6p+1,0x5.D4052Cp+27,(-0x10.Ep+1),0x5.D4052Cp+27},{(-0x1.Dp+1),0x4.C65947p-74,(-0x10.Ep+1),(-0x1.4p-1),(-0x10.Ep+1),0x4.C65947p-74,(-0x1.Dp+1),(-0x1.5p+1),0x9.8C8703p+37,0x7.6p+1},{(-0x1.Dp+1),0x7.6p+1,0x5.0BDFCCp-93,0x4.65A181p+81,0x7.C3C84Ep-68,0x4.72C393p-68,0x9.8C8703p+37,(-0x4.7p-1),0x1.B7ACD4p-90,(-0x1.5p+1)},{0x5.0BDFCCp-93,0x7.6p+1,(-0x1.Dp+1),0x2.FBD65Fp+26,0x1.6p+1,0x2.FBD65Fp+26,(-0x1.Dp+1),0x7.6p+1,0x5.0BDFCCp-93,0x4.65A181p+81}},{{(-0x10.Ep+1),0x4.C65947p-74,(-0x1.Dp+1),(-0x1.5p+1),0x9.8C8703p+37,0x7.6p+1,0x1.6p+1,(-0x4.7p-1),0x6.388EEAp-73,0xF.01D16Cp-4},{0x6.388EEAp-73,0x2.FBD65Fp+26,0x5.0BDFCCp-93,(-0x1.5p+1),(-0x7.5p+1),0xF.01D16Cp-4,(-0x7.5p+1),(-0x1.5p+1),0x5.0BDFCCp-93,0x2.FBD65Fp+26},{0x7.C3C84Ep-68,(-0x4.7p-1),(-0x10.Ep+1),0x2.FBD65Fp+26,(-0x7.5p+1),(-0x1.4p-1),0x0.3p-1,0x5.D4052Cp+27,0x1.B7ACD4p-90,0xF.01D16Cp-4},{(-0x7.5p+1),0x5.D4052Cp+27,0x6.388EEAp-73,0x4.65A181p+81,0x9.8C8703p+37,(-0x1.4p-1),0x5.0BDFCCp-93,(-0x1.4p-1),0x9.8C8703p+37,0x4.65A181p+81},{0x7.C3C84Ep-68,0x1.0p+1,0x7.C3C84Ep-68,(-0x1.4p-1),0x1.6p+1,0xF.01D16Cp-4,0x5.0BDFCCp-93,0x4.72C393p-68,(-0x10.Ep+1),(-0x1.5p+1)},{0x6.388EEAp-73,0x5.D4052Cp+27,(-0x7.5p+1),0x4.C65947p-74,0x7.C3C84Ep-68,0x7.6p+1,0x0.3p-1,0x4.72C393p-68,0x0.3p-1,0x7.6p+1},{(-0x10.Ep+1),(-0x4.7p-1),0x7.C3C84Ep-68,(-0x4.7p-1),(-0x10.Ep+1),0x2.FBD65Fp+26,(-0x7.5p+1),(-0x1.4p-1),0x0.3p-1,0x5.D4052Cp+27},{0x5.0BDFCCp-93,0x2.FBD65Fp+26,0x6.388EEAp-73,0x4.C65947p-74,0x1.B7ACD4p-90,0x4.72C393p-68,0x1.6p+1,0x5.D4052Cp+27,(-0x10.Ep+1),0x5.D4052Cp+27}},{{(-0x1.Dp+1),0x4.C65947p-74,(-0x10.Ep+1),(-0x1.4p-1),(-0x10.Ep+1),0x4.C65947p-74,(-0x1.Dp+1),(-0x1.5p+1),0x9.8C8703p+37,0x7.6p+1},{(-0x1.Dp+1),0x7.6p+1,0x5.0BDFCCp-93,0x4.65A181p+81,0x1.B7ACD4p-90,0x4.65A181p+81,(-0x10.Ep+1),0x1.0p+1,0x6.388EEAp-73,0x7.6p+1},{0x7.C3C84Ep-68,0xF.01D16Cp-4,(-0x7.1p-1),0x4.C65947p-74,0x9.8C8703p+37,0x4.C65947p-74,(-0x7.1p-1),0xF.01D16Cp-4,0x7.C3C84Ep-68,0x2.FBD65Fp+26},{0x5.0BDFCCp-93,0x5.D4052Cp+27,(-0x7.1p-1),0x7.6p+1,(-0x10.Ep+1),0xF.01D16Cp-4,0x9.8C8703p+37,0x1.0p+1,(-0x7.5p+1),0x4.72C393p-68},{(-0x7.5p+1),0x4.C65947p-74,0x7.C3C84Ep-68,0x7.6p+1,0x0.3p-1,0x4.72C393p-68,0x0.3p-1,0x7.6p+1,0x7.C3C84Ep-68,0x4.C65947p-74},{0x1.B7ACD4p-90,0x1.0p+1,0x5.0BDFCCp-93,0x4.C65947p-74,0x0.3p-1,(-0x4.7p-1),(-0x1.Dp+1),(-0x1.4p-1),0x6.388EEAp-73,0x4.72C393p-68},{0x0.3p-1,(-0x1.4p-1),(-0x7.5p+1),0x2.FBD65Fp+26,(-0x10.Ep+1),(-0x4.7p-1),0x7.C3C84Ep-68,(-0x4.7p-1),(-0x10.Ep+1),0x2.FBD65Fp+26},{0x1.B7ACD4p-90,(-0x1.5p+1),0x1.B7ACD4p-90,(-0x4.7p-1),0x9.8C8703p+37,0x4.72C393p-68,0x7.C3C84Ep-68,0x4.65A181p+81,0x5.0BDFCCp-93,0x7.6p+1}}};
        float l_2749 = 0x0.Fp-1;
        int32_t l_2752 = 0L;
        int32_t l_2764 = 1L;
        float *****l_2782[10][7][3] = {{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}},{{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490},{&g_1490,&g_1490,&g_1490}}};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_2689[i] = &l_2688;
        for (i = 0; i < 2; i++)
            l_2691[i] = 0x506EF295L;
        (*g_5) = (l_2691[1] &= ((safe_add_func_uint64_t_u_u((((*g_1274) = (*g_1274)) == (**g_2551)), (safe_mul_func_uint8_t_u_u(((((((*l_2677) = l_2676) , (l_2678 = p_10)) , p_10) & (-9L)) < ((**g_457) > ((g_2690[7][0][0] = (((*l_2679) = l_2676) , ((((safe_rshift_func_int8_t_s_s(((safe_div_func_uint16_t_u_u((g_2684 == l_2686), l_2687)) <= l_2676), (*g_75))) && 0xCEABL) , (*g_75)) , l_2688))) == (void*)0))), l_2687)))) > (****g_2574)));
        if ((safe_add_func_int8_t_s_s((p_10 > (safe_div_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((((((((l_2700 | (l_2707 |= ((safe_unary_minus_func_uint64_t_u(((g_2702 , p_13) , (l_2691[1] = ((****g_2574) |= l_2678))))) || (((1UL && (safe_mod_func_int64_t_s_s(l_2676, ((*****g_2573) &= ((l_2687 < (safe_add_func_uint8_t_u_u(0x79L, p_10))) && 0x16L))))) & p_10) , (*g_5))))) & 4294967295UL) , l_2678) ^ l_2687) <= p_10) >= l_2687) > 6UL), p_13)), p_13)) >= p_10) && (*g_71)), l_2687))), (*g_75))))
        { /* block id: 1261 */
            int32_t *l_2708[8][8] = {{&l_2707,(void*)0,&g_47[0][0][0],&g_47[0][0][0],(void*)0,&l_2707,&g_47[3][5][3],&g_37},{(void*)0,&l_2707,&g_47[3][5][3],&g_37,&l_2678,&g_47[0][0][0],(void*)0,&l_2678},{&g_37,&g_47[5][5][3],&l_2700,&g_37,&g_47[3][5][3],&g_1279[2],&g_47[3][5][3],&g_37},{&g_1279[2],&g_47[3][5][3],&g_1279[2],&g_47[0][0][0],&l_2691[0],&g_47[3][5][3],&g_47[0][0][0],&g_47[5][5][3]},{&g_47[3][5][3],(void*)0,&g_47[3][5][3],(void*)0,&g_37,&l_2678,&l_2691[0],(void*)0},{&g_47[3][5][3],&g_47[5][5][3],(void*)0,&l_2691[0],&l_2691[0],(void*)0,&g_47[5][5][3],&g_47[3][5][3]},{&g_1279[2],&g_37,(void*)0,&l_2678,&g_47[3][5][3],&l_2707,&g_1279[2],&g_47[5][5][3]},{&g_37,&l_2678,&g_47[0][0][0],(void*)0,&l_2678,&l_2707,&l_2691[0],&l_2707}};
            uint32_t l_2723 = 0UL;
            int64_t **l_2731 = &l_2679;
            uint32_t *l_2747 = &g_50;
            uint16_t *l_2750 = &g_2751[4];
            int i, j;
            l_2723++;
            (*g_5) = (((((l_2752 = (safe_mod_func_int32_t_s_s(((18446744073709551608UL != ((l_2728[6] || ((safe_sub_func_uint16_t_u_u((l_2731 == l_2731), ((*l_2750) ^= ((((*l_2677) |= ((safe_rshift_func_int8_t_s_u(((l_2717 = (safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(((((l_2700 | ((l_2738 == l_2739) <= (safe_lshift_func_int8_t_s_u((((!(l_2714 |= ((**l_2686) = (((*l_2747) = (safe_div_func_int32_t_s_s(l_2745, p_13))) && p_13)))) <= 0L) , 1L), 1)))) || l_2678) == p_10) == p_13), l_2748)), 0x8DL))) < 3UL), 2)) > p_13)) >= 0xF87BL) & l_2700)))) < p_13)) ^ p_13)) < p_10), 0x2DF1CEC0L))) & p_13) || 9UL) || 0xE10DE847L) >= (*g_5));
        }
        else
        { /* block id: 1271 */
            uint32_t l_2755 = 18446744073709551615UL;
            int32_t l_2760 = 1L;
            uint8_t l_2765 = 5UL;
            int32_t l_2776 = 0x0ABF55A4L;
            const uint32_t **l_2791 = (void*)0;
            const uint32_t ***l_2790 = &l_2791;
            for (g_140 = (-30); (g_140 != 3); g_140 = safe_add_func_int32_t_s_s(g_140, 1))
            { /* block id: 1274 */
                float l_2759 = 0x0.605DC5p-40;
                int32_t l_2761 = 0x6D023F95L;
                int32_t l_2762 = 0x7D22F872L;
                int32_t l_2763[5] = {1L,1L,1L,1L,1L};
                int i;
                (*g_5) = l_2755;
                if (((void*)0 == &g_1949))
                { /* block id: 1276 */
                    int32_t *l_2756 = (void*)0;
                    int32_t *l_2757 = &l_2720[6];
                    int32_t *l_2758[6] = {&l_2711,(void*)0,&l_2711,&l_2711,(void*)0,&l_2711};
                    int i;
                    l_2765++;
                    if (p_10)
                        continue;
                    (*g_2768) = &l_2761;
                }
                else
                { /* block id: 1280 */
                    int8_t l_2784 = 0x7CL;
                    int16_t *l_2788 = &g_851;
                    int16_t *l_2792 = &g_1867;
                    int16_t *l_2793 = &g_2794;
                    for (l_2755 = 0; (l_2755 <= 1); l_2755 += 1)
                    { /* block id: 1283 */
                        int32_t l_2783 = 0x235E1699L;
                        int i;
                        (*g_1496) = &l_2691[l_2755];
                        (*g_352) = (safe_mod_func_int16_t_s_s((safe_mod_func_uint64_t_u_u((l_2691[l_2755] >= (+(((l_2776 ^= (safe_div_func_uint64_t_u_u((0xA84B01FCL >= (**g_879)), p_10))) | (l_2783 &= (&g_1234 != (((safe_add_func_uint64_t_u_u((1L && ((**l_2686) = p_13)), (safe_sub_func_uint64_t_u_u((safe_unary_minus_func_uint32_t_u((p_11 , p_10))), 5L)))) , (**g_942)) , l_2782[0][3][1])))) && p_10))), 0x996981D8AE6931A9LL)), 1UL));
                        (*g_5) &= l_2691[l_2755];
                    }
                    l_2784 = l_2776;
                    (*g_2800) = func_14(&p_13, (~(l_2700 = ((*g_71) = ((((safe_rshift_func_uint16_t_u_s(l_2763[0], ((*l_2788) = (-1L)))) > (~((((*l_2793) &= ((*l_2792) |= ((((void*)0 != l_2790) , 0x0CB8DB67L) & 0xC1E7B1A1L))) | (((++(**l_2738)) & ((((safe_mul_func_int16_t_s_s(((3L != (l_2799 , l_2760)) < p_10), 4UL)) < 0x629F2C3E885339A0LL) , l_2691[1]) , 0x77L)) == 0x2294AE1FL)) == p_13))) , l_2784) , p_13)))));
                }
                if (l_2691[1])
                    break;
                p_9 = p_9;
            }
            (*g_2802) = &l_2752;
        }
    }
    else
    { /* block id: 1305 */
        uint64_t *l_2804 = &g_252;
        int32_t l_2808 = 0x36D7EFC3L;
        int8_t l_2833 = (-1L);
        const uint32_t *l_2844[10] = {&g_1520,&g_1520,&g_1520,&g_1520,&g_1520,&g_1520,&g_1520,&g_1520,&g_1520,&g_1520};
        const uint32_t ** const l_2843[4] = {&l_2844[9],&l_2844[9],&l_2844[9],&l_2844[9]};
        uint64_t **l_2879 = &l_2804;
        uint64_t ***l_2878[2][7][1] = {{{&l_2879},{&l_2879},{&l_2879},{&l_2879},{&l_2879},{&l_2879},{&l_2879}},{{&l_2879},{&l_2879},{&l_2879},{&l_2879},{&l_2879},{&l_2879},{&l_2879}}};
        uint32_t l_2998 = 18446744073709551608UL;
        uint64_t l_2999 = 0x7788247E7AEB027BLL;
        int8_t *l_3017[4];
        int32_t l_3026 = 0xE2E89037L;
        int32_t l_3030 = 1L;
        int32_t l_3034 = 0x5C142861L;
        int32_t l_3035[10][9][2] = {{{(-4L),(-6L)},{0xFF773F5FL,(-4L)},{0xC6C2AAE0L,0xE57676B5L},{0xDAD72072L,(-8L)},{(-6L),1L},{(-1L),0x99E76B70L},{0xCEB9A570L,(-2L)},{0xC6C2AAE0L,(-2L)},{0xCEB9A570L,0x99E76B70L}},{{(-1L),1L},{(-6L),(-8L)},{0xDAD72072L,0xE57676B5L},{0xC6C2AAE0L,(-4L)},{0xFF773F5FL,0xA739F945L},{1L,1L},{(-5L),0x9F0FF4D6L},{0x421113EEL,0x1540F62BL},{0xC6C2AAE0L,0x9F76E514L}},{{0x10856574L,0xC6C2AAE0L},{0x9360C3DAL,1L},{0x9360C3DAL,0xC6C2AAE0L},{0x10856574L,0x9F76E514L},{0xC6C2AAE0L,0x1540F62BL},{0x421113EEL,0x9F0FF4D6L},{(-5L),1L},{1L,0xA739F945L},{0xFF773F5FL,(-4L)}},{{0xC6C2AAE0L,0xE57676B5L},{0xDAD72072L,(-8L)},{(-6L),1L},{(-1L),0x99E76B70L},{0xCEB9A570L,(-2L)},{0xC6C2AAE0L,(-2L)},{0xCEB9A570L,0x99E76B70L},{(-1L),1L},{(-6L),(-8L)}},{{0xDAD72072L,0xE57676B5L},{0xC6C2AAE0L,(-4L)},{0xFF773F5FL,0xA739F945L},{1L,1L},{(-5L),0x9F0FF4D6L},{0x421113EEL,0x1540F62BL},{0xC6C2AAE0L,0x9F76E514L},{0x10856574L,0xC6C2AAE0L},{0x9360C3DAL,1L}},{{0x9360C3DAL,0xC6C2AAE0L},{0x10856574L,0x9F76E514L},{0xC6C2AAE0L,0x1540F62BL},{0x421113EEL,0x9F0FF4D6L},{(-5L),1L},{1L,0xA739F945L},{0xFF773F5FL,(-4L)},{0xC6C2AAE0L,0xE57676B5L},{0xDAD72072L,(-8L)}},{{(-6L),1L},{(-1L),0x99E76B70L},{0xCEB9A570L,(-2L)},{0xC6C2AAE0L,(-2L)},{0xCEB9A570L,0x99E76B70L},{(-1L),1L},{(-6L),(-8L)},{0xDAD72072L,0xE57676B5L},{0xC6C2AAE0L,(-4L)}},{{0xFF773F5FL,0xA739F945L},{1L,1L},{(-5L),0x9F0FF4D6L},{0x421113EEL,0x1540F62BL},{0xC6C2AAE0L,0x9F76E514L},{0x10856574L,0xC6C2AAE0L},{0x9360C3DAL,1L},{0x9360C3DAL,0xC6C2AAE0L},{0x10856574L,0x9F76E514L}},{{0xC6C2AAE0L,0x1540F62BL},{0x421113EEL,0x9F0FF4D6L},{(-5L),1L},{1L,0xA739F945L},{0xFF773F5FL,(-4L)},{0xC6C2AAE0L,0xE57676B5L},{0xDAD72072L,(-8L)},{(-6L),1L},{(-1L),0x99E76B70L}},{{0xCEB9A570L,(-2L)},{0xC6C2AAE0L,(-2L)},{0xCEB9A570L,0x99E76B70L},{(-1L),1L},{(-6L),(-8L)},{0xDAD72072L,0xE57676B5L},{0xC6C2AAE0L,(-4L)},{0xFF773F5FL,0xA739F945L},{1L,1L}}};
        union U0 **l_3052 = &g_1275;
        uint32_t l_3067[2];
        uint32_t l_3071 = 0UL;
        int32_t *l_3077 = (void*)0;
        int16_t *l_3079 = &g_851;
        int32_t **l_3080 = &l_3004;
        int64_t l_3107 = 0x66A9810F331E3727LL;
        float ****l_3112 = &g_1491;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_3017[i] = (void*)0;
        for (i = 0; i < 2; i++)
            l_3067[i] = 0x2BA88BEFL;
        for (g_376 = 4; (g_376 >= 0); g_376 -= 1)
        { /* block id: 1308 */
            uint64_t *l_2803 = &g_252;
            int32_t *l_2805 = &l_2719;
            int8_t *l_2811 = &g_76;
            int32_t l_2884 = 9L;
            int32_t l_3027 = 0L;
            int32_t l_3028 = (-1L);
            int32_t l_3029 = (-5L);
            float l_3031 = 0xD.AA3FB8p+23;
            int32_t l_3032[4] = {0x1EFE27F1L,0x1EFE27F1L,0x1EFE27F1L,0x1EFE27F1L};
            int64_t l_3043 = 0L;
            int i;
            (*g_2812) = l_2805;
            if (((((**g_2576) = (safe_sub_func_int8_t_s_s((safe_sub_func_int16_t_s_s((g_851 = 1L), 65529UL)), ((((safe_div_func_uint8_t_u_u((0x2F4AF993205E8783LL >= ((safe_add_func_int8_t_s_s((safe_div_func_int8_t_s_s((p_10 && (((((safe_sub_func_uint32_t_u_u(p_13, 0x945C9EBBL)) != ((((((safe_div_func_int16_t_s_s((p_10 | 0x9CE1B060E0886481LL), (((*l_2811) = (safe_rshift_func_int16_t_s_s((((safe_div_func_uint8_t_u_u(((((safe_add_func_int32_t_s_s((l_2808 == 4294967289UL), l_2833)) > (*l_2805)) > 9L) | (**g_1584)), 1L)) || l_2717) != l_2808), g_2751[4]))) ^ 0xC6L))) != 1L) > (*l_2805)) > 0xAF912EE9L) ^ l_2710) < l_2808)) , p_13) , 1UL) != p_10)), 0x4FL)), 0x20L)) > 65526UL)), (-2L))) , l_2834) != (void*)0) || p_13)))) ^ p_13) , (-5L)))
            { /* block id: 1313 */
                return p_10;
            }
            else
            { /* block id: 1315 */
                const uint32_t l_2852 = 18446744073709551615UL;
                int16_t *l_2858 = &g_851;
                uint64_t **l_2877 = &l_2803;
                uint64_t ***l_2876[2][9] = {{&l_2877,&l_2877,(void*)0,(void*)0,&l_2877,&l_2877,(void*)0,(void*)0,&l_2877},{&l_2877,&l_2877,&l_2877,&l_2877,&l_2877,&l_2877,&l_2877,&l_2877,&l_2877}};
                int32_t l_2888 = 0x0EB561FDL;
                int8_t l_2941[8][1][3] = {{{1L,0L,0L}},{{9L,1L,0x2BL}},{{1L,0L,0L}},{{9L,1L,0x2BL}},{{1L,0L,0L}},{{9L,1L,0x2BL}},{{1L,0L,0L}},{{9L,1L,0x2BL}}};
                int i, j, k;
                if ((**g_2800))
                    break;
                if (((*g_36) = ((safe_sub_func_int8_t_s_s(((((safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_div_func_float_f_f((((void*)0 != l_2843[3]) > (((**g_272) = l_2833) == l_2720[1])), (l_2716 > (safe_mul_func_float_f_f(((((safe_mod_func_int16_t_s_s(p_10, (safe_unary_minus_func_uint64_t_u((4294967295UL < (safe_add_func_int16_t_s_s(l_2808, ((void*)0 == &g_1949)))))))) <= 0x55A361D3L) , l_2852) != 0x5.62FE52p+73), p_10))))), l_2852)), p_10)) == (****g_1234)) , l_2833) >= p_10), 0x35L)) > (-1L))))
                { /* block id: 1319 */
                    uint8_t l_2857[9] = {0xB7L,0xB7L,0xB7L,0xB7L,0xB7L,0xB7L,0xB7L,0xB7L,0xB7L};
                    int32_t ***l_2865 = &g_942;
                    uint32_t l_2866 = 0x279AE30DL;
                    uint8_t l_2880 = 0x5DL;
                    int32_t l_2882 = 0xC433ABE5L;
                    int32_t l_2883 = 0xE03C578AL;
                    int i;
                    (*g_5) &= (safe_sub_func_uint8_t_u_u((safe_add_func_int16_t_s_s(l_2857[0], (((l_2883 = (((void*)0 != l_2858) ^ ((safe_sub_func_uint64_t_u_u((((safe_div_func_int32_t_s_s(((*g_36) = (safe_mul_func_uint16_t_u_u((l_2866 = (l_2865 == (void*)0)), ((l_2882 = (((safe_add_func_int64_t_s_s((((0xA9L || (safe_mul_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((((*l_2811) &= ((((l_2852 <= (safe_mod_func_uint32_t_u_u((((~(l_2876[0][8] == l_2878[1][0][0])) || l_2880) || 0xDBE48EBAL), (-8L)))) || l_2881) > p_10) == l_2833)) <= (-1L)), l_2857[0])), p_10))) , 0xF7L) <= (-1L)), (***g_2575))) , 255UL) ^ l_2852)) , p_10)))), 0xB48443B2L)) < p_13) > 1L), 1UL)) || (*g_458)))) ^ l_2884) | (**g_2576)))), p_13));
                }
                else
                { /* block id: 1326 */
                    uint8_t *****l_2905 = &g_2903[6][3][1];
                    int64_t *l_2906 = &l_2728[2];
                    int32_t l_2939 = 0x0765C9EEL;
                    int8_t l_2940 = 0x83L;
                    if (l_2852)
                        break;
                    for (g_96 = 0; (g_96 <= 3); g_96 += 1)
                    { /* block id: 1330 */
                        float l_2887 = 0xD.4A52B2p-17;
                        (*g_36) |= 6L;
                        (**g_2800) = (safe_rshift_func_int16_t_s_s((l_2833 && l_2888), 3));
                    }
                    (*g_5) = (safe_rshift_func_uint8_t_u_s(p_10, (((safe_div_func_float_f_f((((**g_1236) >= (safe_div_func_float_f_f(((**g_272) = (+l_2852)), ((safe_sub_func_float_f_f(((((((-0x9.9p+1) > p_13) <= (((safe_add_func_uint64_t_u_u((+((safe_lshift_func_int8_t_s_u((((*l_2905) = g_2903[6][3][1]) != (void*)0), 1)) & (p_13 != ((*l_2906) = 0xDE6DB3CA381D43F9LL)))), 0x64BA8D472BE0656DLL)) , (**g_942)) , p_11)) <= 0xD.03D70Cp+75) < p_10) <= l_2852), (-0x7.1p+1))) > l_2808)))) == l_2687), p_13)) , 3L) > (-9L))));
                    l_2941[5][0][1] |= ((((((safe_lshift_func_uint16_t_u_s((safe_mod_func_int16_t_s_s((~(safe_sub_func_uint64_t_u_u(l_2808, ((((safe_div_func_uint32_t_u_u((safe_mod_func_int32_t_s_s(((safe_add_func_int8_t_s_s(((safe_mod_func_uint64_t_u_u(((**l_2877) ^= (****g_2574)), (safe_unary_minus_func_uint8_t_u((l_2748 , (safe_sub_func_uint16_t_u_u(((((((safe_lshift_func_int16_t_s_u((g_1867 = ((*l_2858) = (safe_sub_func_int64_t_s_s(((safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(6UL, 13)), 5)) , ((safe_add_func_int64_t_s_s((l_2833 < 18446744073709551615UL), (((((safe_rshift_func_uint16_t_u_s(((safe_rshift_func_uint8_t_u_u((0xD747L || (l_2833 >= 18446744073709551610UL)), p_13)) && p_10), 12)) | l_2888) >= l_2939) == 1L) == 0L))) < 0x0FA5L)), 0x0D28EBF38F10998CLL)))), 12)) && 6L) ^ (*l_2805)) == 0UL) , l_2808) ^ l_2716), p_10))))))) , p_13), p_10)) && l_2881), l_2709)), l_2717)) | l_2940) < p_10) > l_2713)))), 0x6701L)), 10)) ^ p_13) == (**g_2576)) <= l_2833) <= 0xB76EL) , (**g_2800));
                }
            }
            for (l_2714 = 3; (l_2714 >= 0); l_2714 -= 1)
            { /* block id: 1346 */
                uint8_t l_2944 = 246UL;
                uint8_t *l_2945[10] = {(void*)0,&l_2944,(void*)0,&l_2944,(void*)0,&l_2944,(void*)0,&l_2944,(void*)0,&l_2944};
                uint8_t *l_2946 = (void*)0;
                uint8_t *l_2947 = &g_96;
                int32_t l_2988 = (-1L);
                int32_t *l_3020 = &l_2808;
                int32_t *l_3021 = &g_1279[2];
                int32_t *l_3022 = &g_37;
                int32_t *l_3023 = &l_2710;
                int32_t *l_3024 = &l_2716;
                int32_t *l_3025[4][7] = {{(void*)0,&l_2720[0],&l_2884,&l_2712,&l_2884,&l_2720[0],(void*)0},{&l_2720[0],(void*)0,&l_2720[4],&l_2988,&g_1279[1],&l_2884,&g_1279[1]},{&l_2720[0],&g_1279[1],&g_1279[1],&l_2720[0],&l_2711,&l_2710,&l_2712},{(void*)0,&l_2710,&l_2720[4],&l_2711,&l_2711,&l_2720[4],&l_2710}};
                uint32_t l_3036 = 4294967290UL;
                int8_t l_3068 = 0L;
                int i, j;
                (*g_1496) = func_66(&g_374, func_66(&g_374, p_9, &p_13, l_2808), &g_491, l_2944);
                if ((safe_unary_minus_func_uint8_t_u((safe_sub_func_int8_t_s_s((safe_mod_func_int16_t_s_s((((*g_1237) , (((safe_div_func_int32_t_s_s((safe_sub_func_int8_t_s_s(((((((+(safe_sub_func_int8_t_s_s(p_13, l_2713))) < ((void*)0 != (*g_2573))) == 0x77L) && (safe_rshift_func_uint8_t_u_u(((void*)0 != &g_1778[2][4][1]), 3))) , (*l_2805)) > p_13), p_13)), (**g_1347))) <= 0L) ^ 4294967288UL)) ^ p_10), g_454[7])), (-6L))))))
                { /* block id: 1349 */
                    uint8_t l_2966[9] = {0x5EL,0x5EL,0x5EL,0x5EL,0x5EL,0x5EL,0x5EL,0x5EL,0x5EL};
                    int32_t **l_3003[9][3][9] = {{{(void*)0,&g_36,&g_36,&l_2805,&g_36,&g_36,&g_352,&g_352,(void*)0},{&l_2805,&g_352,&l_2805,&g_352,&l_2805,&g_352,&g_36,&g_36,&l_2805},{(void*)0,&l_2805,&g_352,(void*)0,&g_36,&g_352,&g_36,&g_352,&g_36}},{{&l_2805,&l_2805,(void*)0,&g_36,&l_2805,(void*)0,&g_352,&g_36,&g_36},{(void*)0,&g_352,&g_352,&g_352,(void*)0,(void*)0,&l_2805,&g_36,&g_36},{(void*)0,&g_36,&l_2805,&g_36,&g_352,&g_36,&l_2805,&g_36,&g_352}},{{&g_36,(void*)0,(void*)0,&g_36,&g_36,&g_352,(void*)0,&g_36,(void*)0},{&g_352,&g_352,&g_352,(void*)0,&l_2805,&g_36,(void*)0,&g_36,&g_36},{&g_36,&g_36,&g_352,&l_2805,&g_36,&g_36,&g_36,&g_352,&g_36}},{{&l_2805,&g_352,(void*)0,&l_2805,&g_352,(void*)0,&g_36,&g_352,&g_36},{&g_352,&g_352,(void*)0,&l_2805,(void*)0,&g_352,&g_36,&l_2805,&l_2805},{&g_36,(void*)0,&g_352,&g_352,&g_352,(void*)0,&g_36,&g_36,&g_36}},{{&g_352,&g_352,&g_36,(void*)0,&g_36,&g_36,(void*)0,&l_2805,&g_352},{(void*)0,&g_36,&g_36,&l_2805,&g_36,(void*)0,&g_352,&g_352,&l_2805},{&g_352,&g_36,&g_352,&g_352,&g_352,&g_36,&g_36,&g_36,&g_36}},{{&g_36,(void*)0,&g_352,&g_352,&g_36,&g_36,&l_2805,&g_36,(void*)0},{&g_36,&l_2805,(void*)0,&g_36,&l_2805,&g_36,(void*)0,&l_2805,&g_36},{&g_36,(void*)0,&g_36,(void*)0,(void*)0,&g_352,&l_2805,&g_36,&l_2805}},{{&g_36,&g_352,&g_352,&g_352,(void*)0,&g_36,&l_2805,&g_352,&g_36},{&g_36,(void*)0,&l_2805,&l_2805,&g_352,&g_352,&g_352,(void*)0,&l_2805},{&g_36,&l_2805,&g_36,&l_2805,(void*)0,&g_36,&g_36,&g_352,&l_2805}},{{&g_36,&l_2805,(void*)0,&g_36,&l_2805,&g_36,(void*)0,(void*)0,(void*)0},{&g_352,&g_352,&g_36,&l_2805,&g_36,&g_352,(void*)0,&g_36,&l_2805},{(void*)0,(void*)0,&l_2805,&g_36,&l_2805,&g_352,&l_2805,&g_36,&g_36}},{{&g_352,(void*)0,&l_2805,&g_36,&g_36,&l_2805,&l_2805,(void*)0,&g_352},{&g_352,(void*)0,(void*)0,&g_36,&l_2805,&g_36,&g_352,&g_352,&g_36},{&g_352,(void*)0,(void*)0,(void*)0,&g_36,(void*)0,(void*)0,(void*)0,(void*)0}}};
                    int i, j, k;
                    (*g_36) = (safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_u((((l_2966[5] < (safe_add_func_int8_t_s_s((l_2799 == (((((((l_2716 && (!(l_2808 ^ 0x11730DF7L))) & ((safe_rshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_u(((safe_mul_func_int16_t_s_s(1L, l_2808)) ^ ((safe_mod_func_int8_t_s_s(0x70L, l_2833)) != 255UL)), 9)), l_2833)) >= l_2799)) <= 0UL) != p_13) == 0xD307DD75AA659F79LL) | 0UL) , p_13)), p_13))) > (-3L)) , 0x6923L), 9)) , 0x0677L), 14));
                    for (l_2719 = 3; (l_2719 >= 0); l_2719 -= 1)
                    { /* block id: 1353 */
                        int32_t *l_2989 = &l_2718;
                        int8_t *l_3000[10][10][2] = {{{&g_76,&g_729},{&g_76,(void*)0},{&g_76,&g_729},{&g_76,(void*)0},{&g_729,&g_76},{&l_2833,&g_729},{&l_2833,(void*)0},{(void*)0,&g_491},{(void*)0,(void*)0},{&g_491,&l_2833}},{{&g_491,&g_491},{&g_491,&g_491},{&l_2833,(void*)0},{&g_729,&g_76},{(void*)0,&g_241},{&g_241,(void*)0},{&g_76,&g_729},{&g_491,&g_491},{&g_241,&g_76},{&l_2833,&g_241}},{{(void*)0,&g_491},{&g_491,&g_76},{(void*)0,(void*)0},{&g_729,(void*)0},{&g_491,&g_729},{&g_729,&l_2833},{&g_241,&l_2833},{&g_729,&g_729},{&g_241,&l_2833},{(void*)0,(void*)0}},{{&g_729,&g_729},{&g_76,&l_2833},{&l_2833,&g_76},{&g_241,&g_241},{(void*)0,&g_241},{&g_729,&l_2833},{&g_491,(void*)0},{&g_76,(void*)0},{&g_241,&g_241},{&l_2833,&g_729}},{{&g_241,&g_729},{&l_2833,&g_76},{&l_2833,&g_491},{&g_729,(void*)0},{&l_2833,&l_2833},{(void*)0,(void*)0},{&g_491,(void*)0},{&g_241,&g_491},{&g_491,&g_241},{&g_241,&g_491}},{{&g_241,(void*)0},{&g_241,&g_491},{&g_241,&g_241},{&g_491,&g_491},{&g_241,(void*)0},{&g_491,(void*)0},{(void*)0,&l_2833},{&l_2833,(void*)0},{&g_729,&g_491},{&l_2833,&g_76}},{{&l_2833,&g_729},{&g_241,&g_729},{&l_2833,&g_241},{&g_241,(void*)0},{&g_76,(void*)0},{&g_491,&l_2833},{&g_729,&g_241},{(void*)0,&g_241},{&g_241,&g_76},{&l_2833,&l_2833}},{{&g_76,&g_729},{&g_729,(void*)0},{(void*)0,&l_2833},{&g_241,&g_729},{&g_729,&l_2833},{&g_241,&l_2833},{&g_729,&g_729},{&g_241,&l_2833},{(void*)0,(void*)0},{&g_729,&g_729}},{{&g_76,&l_2833},{&l_2833,&g_76},{&g_241,&g_241},{(void*)0,&g_241},{&g_729,&l_2833},{&g_491,(void*)0},{&g_76,(void*)0},{&g_241,&g_241},{&l_2833,&g_729},{&g_241,&g_729}},{{&l_2833,&g_76},{&l_2833,&g_491},{&g_729,(void*)0},{&l_2833,&l_2833},{(void*)0,(void*)0},{&g_491,(void*)0},{&g_241,&g_491},{&g_491,&g_241},{&g_241,&g_491},{&g_241,(void*)0}}};
                        int i, j, k;
                        p_9 = l_2989;
                    }
                    (*g_3002) = p_9;
                    l_3004 = (*g_513);
                }
                else
                { /* block id: 1364 */
                    for (l_2687 = 0; (l_2687 <= 3); l_2687 += 1)
                    { /* block id: 1367 */
                        return p_10;
                    }
                }
                for (g_2794 = 5; (g_2794 >= 1); g_2794 -= 1)
                { /* block id: 1373 */
                    int32_t *l_3005 = &g_1362[0];
                    uint64_t * const **l_3016 = (void*)0;
                    int32_t l_3018 = 0x727DA4ACL;
                    for (g_2248 = 0; (g_2248 <= 1); g_2248 += 1)
                    { /* block id: 1376 */
                        uint16_t *l_3006 = &g_103;
                        const int32_t l_3009 = 0L;
                        int32_t **l_3019 = &g_352;
                        int i, j, k;
                        (*l_3019) = ((((*l_2804) = (((***l_2834) = l_3005) != (p_13 , func_66(&g_252, l_3005, (l_3017[3] = ((++(*l_3006)) , &p_13)), l_3018)))) < 18446744073709551615UL) , (void*)0);
                    }
                }
                --l_3036;
                for (l_2709 = 0; (l_2709 <= 3); l_2709 += 1)
                { /* block id: 1387 */
                    uint32_t *l_3044 = &l_3036;
                    int16_t *l_3055[6][2] = {{(void*)0,&g_851},{(void*)0,&g_851},{(void*)0,&g_851},{(void*)0,&g_851},{(void*)0,&g_851},{(void*)0,&g_851}};
                    int32_t l_3056 = 0x5B5B9825L;
                    int32_t l_3059 = 0x40BC073DL;
                    int32_t l_3060 = 0xF65D0DE2L;
                    int32_t l_3061 = (-1L);
                    int32_t l_3062 = 0L;
                    int32_t l_3063 = 0xC6D81945L;
                    int i, j, k;
                    for (g_851 = 0; (g_851 <= 3); g_851 += 1)
                    { /* block id: 1390 */
                        uint64_t l_3039 = 18446744073709551615UL;
                        int32_t **l_3040 = &l_3025[1][1];
                        (*l_2805) = l_3039;
                        l_3004 = (void*)0;
                        (*l_3040) = (*g_1496);
                    }
                    p_9 = p_9;
                    for (l_2711 = 0; (l_2711 >= (-16)); l_2711 = safe_sub_func_int16_t_s_s(l_2711, 5))
                    { /* block id: 1401 */
                        uint16_t l_3064[9] = {0xDFC5L,0x41BEL,0xDFC5L,0x41BEL,0xDFC5L,0x41BEL,0xDFC5L,0x41BEL,0xDFC5L};
                        int32_t l_3069 = 0x08EE40B0L;
                        int32_t l_3070 = 0x17EEC76FL;
                        int i;
                        l_3064[5]++;
                        if (l_3067[0])
                            continue;
                        ++l_3071;
                        if ((*l_2805))
                            continue;
                    }
                    for (g_241 = 0; (g_241 == 7); g_241 = safe_add_func_int64_t_s_s(g_241, 9))
                    { /* block id: 1409 */
                        int32_t **l_3076 = (void*)0;
                        float *** const l_3078 = &l_2997;
                        l_3077 = &l_2711;
                        (*g_36) = (l_3078 != (void*)0);
                        return p_13;
                    }
                }
            }
        }
        (*l_3080) = func_14(&p_13, ((*l_3079) = 0x4114L));
        if (l_3081)
        { /* block id: 1419 */
            return p_10;
        }
        else
        { /* block id: 1421 */
            int64_t **l_3097[9] = {&g_1972[4][1],&g_1972[4][1],&g_1972[4][1],&g_1972[4][1],&g_1972[4][1],&g_1972[4][1],&g_1972[4][1],&g_1972[4][1],&g_1972[4][1]};
            int64_t ***l_3098 = &g_1971[1];
            int32_t l_3104 = (-8L);
            int32_t *l_3108 = &l_2720[4];
            int i;
            (*l_3108) |= (safe_rshift_func_uint8_t_u_s(((((**g_457) = ((0x97938E3B57EF581ALL | (((g_3084 , (safe_unary_minus_func_int64_t_s((((((safe_lshift_func_uint8_t_u_u(((safe_sub_func_int16_t_s_s((safe_add_func_uint8_t_u_u(((l_3092 == (void*)0) , (+((safe_mod_func_uint64_t_u_u(((((p_10 , &g_1972[1][0]) != ((*l_3098) = l_3097[4])) != (((safe_mod_func_int64_t_s_s((safe_unary_minus_func_uint8_t_u((safe_add_func_uint16_t_u_u(((((l_3104 , (+1L)) | l_3104) & 0x91L) ^ 4294967291UL), l_2710)))), 0xDF13C271A0752D16LL)) == 0UL) & p_13)) & 4294967295UL), 0xE85FBE6374015D6ELL)) >= 253UL))), p_10)), l_3104)) & 0x74E391D1L), 5)) | g_376) | 1L) & p_10) <= 0L)))) >= l_3106[6]) || p_10)) || l_3107)) >= l_3104) , 250UL), (*g_75)));
        }
        for (l_3030 = 0; (l_3030 != 7); l_3030 = safe_add_func_uint64_t_u_u(l_3030, 1))
        { /* block id: 1428 */
            float **l_3111 = (void*)0;
            int32_t l_3127[6] = {0xFEFB163AL,0xFEFB163AL,0xFEFB163AL,0xFEFB163AL,0xFEFB163AL,0xFEFB163AL};
            int32_t l_3128 = 0x324A2449L;
            int i;
            for (g_616 = 3; (g_616 <= 9); g_616 += 1)
            { /* block id: 1431 */
                uint8_t l_3113 = 0UL;
                int i;
                (*g_5) ^= ((p_10 <= (**g_457)) , (l_3111 != ((**g_1490) = (void*)0)));
                l_3113 |= ((void*)0 != l_3112);
                (*g_3115) = l_2844[g_616];
            }
            for (g_248 = (-3); (g_248 <= 18); g_248 = safe_add_func_int8_t_s_s(g_248, 6))
            { /* block id: 1439 */
                int32_t *l_3118 = &l_2710;
                int32_t *l_3119 = &g_1279[1];
                int32_t *l_3120 = &l_3026;
                int32_t *l_3121 = &l_2710;
                int32_t *l_3122 = &g_47[3][5][3];
                int32_t *l_3123 = (void*)0;
                int32_t *l_3124 = &g_47[3][5][3];
                int32_t *l_3125 = &l_2709;
                int32_t *l_3126[8][9][3] = {{{&l_2712,(void*)0,(void*)0},{(void*)0,&l_2718,&l_2710},{(void*)0,&l_2718,&l_2711},{(void*)0,&l_2713,&g_1279[1]},{(void*)0,&l_2808,(void*)0},{&l_3026,&l_2713,&l_3030},{&l_2719,&l_2718,&g_1279[1]},{&l_2714,&l_2718,&l_2711},{(void*)0,&l_2717,&l_2712}},{{&g_37,(void*)0,(void*)0},{&g_37,&l_3026,&g_47[3][5][3]},{&l_3030,&l_2715,&l_2714},{&l_2716,(void*)0,&g_799},{(void*)0,&g_37,&l_2711},{(void*)0,&l_2717,&g_37},{&l_3026,&l_3026,&l_2718},{&l_2808,&l_2719,(void*)0},{&l_2716,(void*)0,&l_2715}},{{&g_47[3][7][1],&l_2712,&l_2710},{&g_47[3][7][1],&g_799,&l_2808},{&l_2716,&l_2713,&g_47[3][5][3]},{&l_2808,&l_3026,&l_2714},{&l_3026,&l_2713,&l_2716},{(void*)0,&l_3081,&g_1279[1]},{(void*)0,&g_1279[1],&l_2719},{&l_2716,&l_2717,&l_3030},{&l_3030,&l_2719,&l_2714}},{{&g_37,&g_37,&l_2711},{&g_37,(void*)0,&l_2714},{(void*)0,(void*)0,&l_3081},{&l_2714,&g_37,&l_2717},{&l_2719,&l_2715,(void*)0},{&l_3026,&g_37,&g_799},{(void*)0,&l_2719,(void*)0},{(void*)0,&g_1279[1],&l_2717},{(void*)0,&l_2712,&l_3081}},{{(void*)0,&l_2718,&l_2714},{&l_2716,&l_2713,&l_2711},{(void*)0,&l_3026,&l_2714},{&l_2808,(void*)0,&l_3030},{&l_2711,&l_3081,&l_2719},{&l_2713,&l_2718,&g_1279[1]},{&l_2716,(void*)0,&l_2716},{&g_37,&l_2719,&l_2714},{&g_1279[1],&l_3030,&g_47[3][5][3]}},{{&l_2716,(void*)0,&l_2808},{&g_37,(void*)0,&l_2710},{&l_2714,(void*)0,&l_2715},{(void*)0,(void*)0,(void*)0},{&l_2808,&l_3030,&l_2718},{&l_2711,&l_2719,&g_37},{&l_2713,(void*)0,&l_2711},{(void*)0,&l_2718,&g_799},{&g_47[3][7][1],&l_3081,&l_2714}},{{(void*)0,(void*)0,&g_47[3][5][3]},{&l_2711,&l_3026,(void*)0},{&l_3026,&l_2713,&l_2712},{&l_2711,&l_2718,&l_2711},{(void*)0,&l_2712,&g_1279[1]},{(void*)0,&g_1279[1],&l_3030},{&l_2716,&l_2719,(void*)0},{&l_2712,&g_37,&g_1279[1]},{&l_2716,&l_2715,&l_2711}},{{(void*)0,&g_37,&l_2710},{(void*)0,(void*)0,&g_1279[1]},{(void*)0,&l_2716,&l_2711},{&l_2808,&l_2808,&g_47[6][8][1]},{&l_2718,&l_2711,&l_2714},{(void*)0,&g_1279[1],(void*)0},{&g_47[3][5][3],&l_2713,&g_1279[1]},{&g_1279[1],&g_1279[1],&g_37},{(void*)0,&g_1279[1],&l_2713}}};
                uint16_t l_3130 = 3UL;
                int64_t l_3161 = (-5L);
                int32_t l_3164 = (-5L);
                int16_t **l_3182 = (void*)0;
                int i, j, k;
                ++l_3130;
                for (l_3026 = 0; (l_3026 == (-16)); l_3026 = safe_sub_func_uint16_t_u_u(l_3026, 7))
                { /* block id: 1443 */
                    uint8_t l_3154[3][2];
                    int64_t l_3162 = 0x20B23D41EF03A304LL;
                    uint64_t l_3163 = 5UL;
                    int8_t * const l_3165 = (void*)0;
                    int32_t l_3186 = 0L;
                    int32_t l_3188 = (-5L);
                    const int16_t *l_3190 = &g_2794;
                    const int16_t **l_3189 = &l_3190;
                    int i, j;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_3154[i][j] = 255UL;
                    }
                    if ((safe_rshift_func_int8_t_s_s(((l_2718 &= (-2L)) && ((safe_rshift_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((((safe_add_func_uint64_t_u_u((safe_div_func_int8_t_s_s((((((g_3145 , (safe_mod_func_uint16_t_u_u((((*****g_2573) , (((safe_mod_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(7UL, (((((((safe_mul_func_int8_t_s_s(l_3154[2][1], ((((safe_lshift_func_int16_t_s_u(((((p_10 <= 0xCCEF670EB3BFB2CFLL) | ((((safe_sub_func_uint32_t_u_u((safe_sub_func_int8_t_s_s(p_13, p_13)), p_13)) , l_3154[0][0]) , (*l_3121)) > (-6L))) ^ 0x5160A01DL) | (-9L)), 3)) != (****g_2574)) > 0xCB36BC5D202AEC00LL) & l_3161))) != 0xC6L) & (*g_75)) || 0UL) & 1UL) <= (**g_2576)) && l_2720[4]))), l_3162)) & l_3154[1][0]) == (*g_75))) ^ l_3154[2][1]), 6UL))) || l_3163) , p_10) & p_10) > 0UL), (**g_1690))), p_10)) != 0xC42B66EBL) > l_3164), 0)), p_13)) , p_13)), (*g_75))))
                    { /* block id: 1445 */
                        uint32_t *l_3179 = &g_381;
                        int8_t ***l_3185 = &l_2686;
                        uint16_t *l_3187[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int32_t l_3191 = 0x01E5108FL;
                        int i;
                        (*l_3080) = func_14((*l_2686), l_3191);
                        return p_10;
                    }
                    else
                    { /* block id: 1455 */
                        uint8_t l_3192 = 0UL;
                        l_3186 = ((*l_3121) = 0x8B8CD416L);
                        if (l_3192)
                            continue;
                        (*l_3119) |= 3L;
                    }
                }
            }
        }
    }
    return p_13;
}


/* ------------------------------------------ */
/* 
 * reads : g_2195 g_458 g_880 g_616 g_5 g_75 g_76 g_879 g_1972 g_71 g_72 g_344 g_345 g_346 g_2225 g_942 g_943 g_454 g_457 g_96 g_1234 g_1235 g_1236 g_1237 g_778 g_252 g_273 g_274 g_272 g_2248 g_50 g_513 g_36 g_799 g_47 g_808 g_399 g_1690 g_1691 g_614 g_103 g_2427 g_2194 g_2434 g_2442 g_1540 g_1122 g_6 g_2480 g_2518 g_2539 g_2549 g_2573 g_2576 g_2574 g_2575 g_2550 g_2551 g_2552 g_1548 g_1380 g_381 g_1362
 * writes: g_2194 g_96 g_6 g_49 g_376 g_778 g_2260 g_36 g_352 g_381 g_76 g_274 g_614 g_399 g_491 g_799 g_851 g_1540 g_103 g_1221 g_942 g_752 g_2518 g_2542 g_2546 g_50 g_1867
 */
static int32_t * func_14(int8_t * const  p_15, int16_t  p_16)
{ /* block id: 1119 */
    const int32_t * const l_2191 = (void*)0;
    const int32_t **l_2192 = (void*)0;
    int64_t **l_2202 = &g_1972[0][1];
    int32_t l_2207 = 0x3C94681CL;
    int64_t l_2213 = 0x1A3A4AA3E85184BFLL;
    int32_t l_2395[1][9][4] = {{{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)}}};
    uint32_t l_2471 = 0xEC31C5FEL;
    uint16_t *l_2545[5][10][5] = {{{&g_103,(void*)0,&g_103,&g_103,(void*)0},{(void*)0,&g_103,&g_103,&g_103,&g_103},{(void*)0,&g_103,&g_103,&g_103,&g_103},{(void*)0,&g_103,&g_103,&g_103,(void*)0},{&g_103,&g_103,(void*)0,&g_103,(void*)0},{&g_103,(void*)0,(void*)0,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,(void*)0,&g_103},{&g_103,&g_103,&g_103,(void*)0,(void*)0},{(void*)0,&g_103,&g_103,(void*)0,&g_103}},{{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,(void*)0,&g_103},{(void*)0,&g_103,&g_103,(void*)0,(void*)0}},{{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{(void*)0,(void*)0,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,(void*)0}},{{&g_103,(void*)0,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,&g_103,(void*)0,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,(void*)0,&g_103},{(void*)0,(void*)0,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,(void*)0,&g_103},{&g_103,&g_103,&g_103,(void*)0,(void*)0},{&g_103,&g_103,&g_103,&g_103,&g_103}},{{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,(void*)0,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,(void*)0,(void*)0,(void*)0,&g_103},{&g_103,&g_103,&g_103,(void*)0,(void*)0},{&g_103,(void*)0,&g_103,(void*)0,(void*)0},{&g_103,&g_103,&g_103,&g_103,(void*)0}}};
    uint16_t ** const l_2544 = &l_2545[0][4][4];
    uint16_t ** const *l_2543[7];
    uint64_t l_2553[8][3] = {{0UL,0UL,1UL},{0x4E599353283B95BELL,1UL,0x4E599353283B95BELL},{0UL,1UL,1UL},{18446744073709551615UL,1UL,18446744073709551615UL},{0UL,0UL,1UL},{0x4E599353283B95BELL,1UL,0x4E599353283B95BELL},{0UL,1UL,1UL},{18446744073709551615UL,1UL,18446744073709551615UL}};
    int64_t l_2653 = 0xA117FF4FCCE8B3C0LL;
    int32_t *l_2654 = (void*)0;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_2543[i] = &l_2544;
lbl_2457:
    (*g_2195) = l_2191;
lbl_2459:
    (*g_5) = (safe_add_func_int32_t_s_s((safe_div_func_int16_t_s_s((safe_add_func_int16_t_s_s((l_2202 != (void*)0), (safe_rshift_func_int16_t_s_u((safe_lshift_func_int16_t_s_u(l_2207, 7)), 5)))), ((((safe_rshift_func_uint16_t_u_u(((p_16 , p_16) > (((safe_unary_minus_func_uint8_t_u((p_16 , p_16))) || l_2207) & ((*g_458) = p_16))), p_16)) ^ 0x5EL) , 0x6.9p+1) , 1UL))), (*g_880)));
    if (((p_16 == (((*g_75) ^ (l_2213 > (*g_75))) ^ (((**g_879) == 0xEB13E27CL) == 18446744073709551610UL))) && ((safe_div_func_uint64_t_u_u((+(safe_lshift_func_int8_t_s_u(((safe_rshift_func_int8_t_s_u(((((**l_2202) = 0L) != ((l_2207 = ((p_16 , (void*)0) == (void*)0)) , (*g_71))) || p_16), (**g_344))) != 1UL), 1))), p_16)) < 0xB15EL)))
    { /* block id: 1125 */
        int32_t **l_2226 = (void*)0;
        int32_t l_2227 = 0xFFE8FED2L;
        int32_t *l_2268 = &g_799;
        uint64_t l_2302[10] = {0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL,0xA33BA3285CC4140FLL};
        int16_t l_2345 = 0xF62BL;
        int32_t l_2393[7] = {0xC0F54646L,0xC0F54646L,0xC0F54646L,0xC0F54646L,0xC0F54646L,0xC0F54646L,0xC0F54646L};
        uint32_t l_2407[1];
        uint32_t **l_2425[5][6] = {{&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2]},{&g_1802[2],&g_1802[2],&g_1802[2],&g_1802[2],&g_1802[2],&g_1802[2]},{&g_1802[1],&g_1802[2],&g_1802[2],&g_1802[1],&g_1802[0],&g_1802[1]},{&g_1802[1],&g_1802[0],&g_1802[1],&g_1802[2],&g_1802[2],&g_1802[1]},{&g_1802[2],&g_1802[2],&g_1802[2],&g_1802[2],&g_1802[1],&g_1802[2]}};
        uint32_t * const *l_2426 = (void*)0;
        uint64_t *l_2430 = &l_2302[8];
        int32_t *l_2431 = (void*)0;
        union U0 *l_2479 = &g_2225;
        int64_t *l_2577 = &g_376;
        int32_t **l_2626 = &g_943;
        int8_t l_2627 = (-10L);
        int i, j;
        for (i = 0; i < 1; i++)
            l_2407[i] = 1UL;
lbl_2521:
        l_2227 ^= (safe_lshift_func_int16_t_s_s(((safe_sub_func_int64_t_s_s((((-1L) != (g_2225 , ((**g_942) , (l_2226 == ((*g_71) , &g_352))))) , (0xE276980504C99CC0LL == (*g_71))), (*g_71))) , 0L), g_616));
        if ((safe_unary_minus_func_int32_t_s(((++(**g_457)) > 0xCDL))))
        { /* block id: 1128 */
            int32_t l_2233 = 0x29999451L;
            uint64_t l_2242[4];
            int16_t l_2252 = 4L;
            int32_t l_2381[9][1][7] = {{{1L,(-4L),0x967DA76CL,1L,(-4L),0x20C6C6CDL,(-1L)}},{{4L,1L,0x20C6C6CDL,4L,(-1L),4L,0x20C6C6CDL}},{{0xAAAB65A9L,0xAAAB65A9L,1L,1L,1L,1L,0xAAAB65A9L}},{{0xAAAB65A9L,0x20C6C6CDL,0x967DA76CL,1L,0x9190030CL,0x9190030CL,1L}},{{4L,(-1L),4L,0x20C6C6CDL,1L,4L,(-4L)}},{{1L,(-1L),0x07D1EAEEL,1L,(-1L),1L,(-1L)}},{{0x4C1BA859L,0x20C6C6CDL,0x20C6C6CDL,0x4C1BA859L,(-4L),4L,1L}},{{0x9190030CL,0xAAAB65A9L,0x20C6C6CDL,0x967DA76CL,1L,0x9190030CL,0x9190030CL}},{{0xAAAB65A9L,1L,0x07D1EAEEL,1L,0xAAAB65A9L,1L,1L}}};
            uint64_t **l_2429 = &g_71;
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_2242[i] = 0UL;
            (*g_1237) = (safe_mul_func_float_f_f((l_2233 != (-(((!(((**l_2202) = (((safe_lshift_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(p_16, 2)), (p_16 != ((-7L) > ((safe_rshift_func_uint16_t_u_u(((l_2242[0] &= 0xEFL) < ((safe_sub_func_uint32_t_u_u(((safe_unary_minus_func_uint32_t_u(((****g_1234) , (((**g_942) , p_16) <= (safe_mod_func_uint16_t_u_u(((((p_16 & 0x7CL) | p_16) < p_16) < p_16), 0xD999L)))))) & g_252), p_16)) >= 0UL)), p_16)) <= p_16))))) && p_16) , l_2227)) , (*g_273))) < (**g_272)) <= p_16))), g_2248));
            if (p_16)
            { /* block id: 1132 */
                int32_t ***l_2259[7][10] = {{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226},{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226},{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226},{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226},{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226},{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226},{&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226,&l_2226}};
                int i, j;
                (*g_1237) = (((safe_sub_func_int16_t_s_s((+(l_2252 &= (*g_75))), (((l_2227 = (safe_sub_func_uint8_t_u_u(((**g_457) = (safe_div_func_uint32_t_u_u(((((safe_sub_func_uint16_t_u_u(65535UL, ((p_16 < ((g_2260[0][4] = (void*)0) == &g_7[3][3][4])) || p_16))) == (safe_mul_func_uint16_t_u_u(8UL, ((+(safe_add_func_uint64_t_u_u(0x806A2966C64A2822LL, (*g_71)))) == 1UL)))) , l_2233) , 0x20E24C2FL), p_16))), (*g_75)))) , g_50) & p_16))) < 0x4ADDL) , 0x9.Ep-1);
            }
            else
            { /* block id: 1138 */
                uint8_t l_2295 = 255UL;
                int32_t l_2307 = 0x82C06C68L;
                uint16_t **l_2332[6];
                int32_t l_2344 = 0x7E58AAE4L;
                int32_t l_2396 = 0xB6D299ECL;
                int32_t l_2397 = 0xBDAB279BL;
                int32_t l_2398 = (-1L);
                int32_t l_2400 = 0x0318DEAAL;
                int32_t l_2401 = (-6L);
                int32_t l_2402 = 0L;
                int32_t l_2403 = 0x5FC61F8FL;
                int32_t l_2404 = (-10L);
                int32_t l_2406[7];
                int i;
                for (i = 0; i < 6; i++)
                    l_2332[i] = &g_400;
                for (i = 0; i < 7; i++)
                    l_2406[i] = 0x4358F7A9L;
                if (((safe_lshift_func_uint8_t_u_u(((void*)0 != &g_71), p_16)) , p_16))
                { /* block id: 1139 */
                    int32_t **l_2269 = &g_36;
                    const uint16_t *l_2308 = &g_387;
                    (*g_513) = ((*l_2269) = l_2268);
                    for (g_381 = 17; (g_381 <= 12); g_381 = safe_sub_func_int64_t_s_s(g_381, 2))
                    { /* block id: 1144 */
                        union U0 ****l_2301 = &g_1725;
                        union U0 *****l_2300 = &l_2301;
                        uint64_t l_2303 = 0xDFF3C42993B24359LL;
                        uint64_t l_2304 = 4UL;
                        uint8_t *l_2305 = (void*)0;
                        uint8_t *l_2306 = &g_614;
                        int32_t *l_2309 = (void*)0;
                        int32_t *l_2310 = &l_2227;
                        (*l_2310) ^= ((++(*g_458)) < (safe_mod_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_u(((((l_2307 = (safe_sub_func_int8_t_s_s(0x84L, ((*l_2306) = (((safe_mul_func_float_f_f((+(l_2207 = ((p_16 == (safe_mul_func_float_f_f((((safe_div_func_float_f_f(((*g_273) = ((((*g_75) = ((&g_1778[2][6][1] != &g_1778[0][7][1]) == l_2242[0])) , (safe_mul_func_float_f_f(p_16, (safe_sub_func_float_f_f(((safe_mul_func_float_f_f((safe_mul_func_float_f_f(((((l_2295 > (0x8.0E0316p+5 == ((safe_lshift_func_int8_t_s_u(((safe_mod_func_int16_t_s_s((((*g_75) = (l_2233 = (((((*l_2300) = &g_1725) != (void*)0) < 6UL) || l_2242[2]))) , p_16), (**l_2269))) < 0x515F4A009C7E4F71LL), p_16)) , 0x3.A0AE14p-75))) < 0x6.E00AB6p-18) <= (-0x3.2p+1)) != 0x8.6691D7p-33), (**g_1236))), p_16)) >= (**l_2269)), l_2302[8]))))) == l_2303)), l_2295)) > 0x8.Ap+1) > (-0x8.5p-1)), l_2304))) <= p_16))), 0xC.687DC4p+80)) , l_2303) | g_47[4][1][0]))))) , l_2308) == l_2308) , 253UL), 2)) | 0xCA177F500FD4B72CLL), p_16)));
                    }
                }
                else
                { /* block id: 1156 */
                    float l_2350 = 0x2.Fp+1;
                    int32_t l_2386 = 1L;
                    int32_t l_2392 = 0L;
                    int32_t l_2394[4];
                    int32_t *l_2428[7][2] = {{&l_2394[2],&l_2394[2]},{&l_2394[2],&l_2394[2]},{&l_2394[2],&l_2394[2]},{&l_2394[2],&l_2394[2]},{&l_2394[2],&l_2394[2]},{&l_2394[2],&l_2394[2]},{&l_2394[2],&l_2394[2]}};
                    int i, j;
                    for (i = 0; i < 4; i++)
                        l_2394[i] = 0x4E1DA1D9L;
                    for (l_2213 = 0; (l_2213 > (-17)); l_2213--)
                    { /* block id: 1159 */
                        uint16_t ***l_2333 = &g_399;
                        uint16_t **l_2335 = &g_400;
                        uint16_t ***l_2334 = &l_2335;
                        int8_t *l_2346 = (void*)0;
                        int8_t *l_2347 = &g_491;
                        int32_t *l_2348 = &l_2207;
                        int8_t l_2349 = 0xA2L;
                        int32_t *l_2351 = &l_2307;
                        int32_t *l_2352 = &l_2233;
                        int32_t *l_2353 = &l_2207;
                        int32_t *l_2354 = (void*)0;
                        int32_t *l_2355 = &l_2344;
                        int32_t *l_2356 = &l_2233;
                        int32_t *l_2357 = (void*)0;
                        int32_t *l_2358 = &l_2344;
                        int32_t *l_2359 = &g_47[0][5][3];
                        int32_t *l_2360 = &g_799;
                        int32_t *l_2361 = &l_2233;
                        int32_t *l_2362 = &l_2344;
                        int32_t *l_2363 = (void*)0;
                        int32_t *l_2364 = &g_799;
                        int32_t *l_2365 = &l_2227;
                        int32_t *l_2366 = &l_2233;
                        int32_t *l_2367 = &l_2344;
                        int32_t *l_2368 = &g_1279[1];
                        int32_t *l_2369 = &l_2207;
                        int32_t *l_2370 = &g_47[3][3][2];
                        int32_t *l_2371 = (void*)0;
                        int32_t *l_2372 = &l_2233;
                        int32_t *l_2373 = &g_47[3][5][3];
                        int32_t *l_2374 = &g_47[3][5][3];
                        int32_t *l_2375 = (void*)0;
                        int32_t *l_2376 = &g_1279[1];
                        int32_t *l_2377 = &g_1279[0];
                        int32_t *l_2378 = &g_1279[1];
                        int32_t *l_2379 = &l_2207;
                        int32_t *l_2380 = &g_1279[1];
                        int32_t *l_2382 = (void*)0;
                        int32_t *l_2383 = &g_1279[2];
                        int32_t *l_2384 = &l_2227;
                        int32_t *l_2385 = (void*)0;
                        int32_t l_2387[1][7][4] = {{{(-6L),(-1L),(-1L),(-6L)},{(-1L),(-6L),(-1L),(-1L)},{(-6L),(-6L),(-1L),(-6L)},{(-6L),(-1L),(-1L),(-6L)},{(-1L),(-6L),(-1L),(-1L)},{(-6L),(-6L),(-1L),(-6L)},{(-6L),(-1L),(-1L),(-6L)}}};
                        int32_t *l_2388 = &g_1279[1];
                        int32_t *l_2389 = (void*)0;
                        int32_t *l_2390 = &g_47[4][2][1];
                        int32_t *l_2391[9];
                        float l_2399[5][7] = {{0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1},{0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73},{0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1},{0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73,0x3.5E2950p+73},{0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1,0x6.1p+1}};
                        int32_t l_2405 = 0L;
                        int i, j, k;
                        for (i = 0; i < 9; i++)
                            l_2391[i] = &g_1279[1];
                        (*l_2348) |= ((!(safe_add_func_uint16_t_u_u(((((safe_lshift_func_uint8_t_u_u(((~(safe_div_func_int64_t_s_s(1L, (safe_mod_func_int64_t_s_s(((safe_rshift_func_int8_t_s_u((*g_75), 3)) , p_16), ((safe_sub_func_int64_t_s_s(p_16, (safe_mod_func_int32_t_s_s(((*l_2268) = ((((~(safe_rshift_func_int8_t_s_s((l_2332[4] == ((*l_2334) = ((*l_2333) = (*g_808)))), (((((safe_rshift_func_int8_t_s_s((safe_div_func_int8_t_s_s(((*l_2347) = (safe_sub_func_int16_t_s_s((l_2344 <= (0x5C3078386D6A5671LL < ((l_2242[3] < l_2307) ^ 0xCDCF8F5F89A50454LL))), l_2345))), p_16)), (*g_75))) > 4294967295UL) >= (*g_71)) == p_16) >= (*g_71))))) | p_16) || p_16) & (**g_1690))), p_16)))) | l_2344)))))) < 0L), 5)) == p_16) > 4UL) < p_16), g_103))) && 65535UL);
                        ++l_2407[0];
                    }
                    l_2395[0][6][0] &= (!((((*l_2268) ^ ((safe_lshift_func_int16_t_s_s((g_851 = ((*g_458) | (safe_div_func_uint64_t_u_u(((1L <= (safe_lshift_func_uint16_t_u_u((safe_add_func_int16_t_s_s(0x22ABL, (safe_mod_func_int16_t_s_s((safe_div_func_uint64_t_u_u(p_16, (((((p_16 < 7UL) < ((safe_mul_func_int16_t_s_s((l_2425[0][0] != (l_2426 = (void*)0)), 0x7532L)) & 1UL)) , p_16) >= p_16) || p_16))), p_16)))), p_16))) > p_16), p_16)))), p_16)) != g_2427)) <= l_2386) , l_2392));
                }
            }
            (*g_2434) = (*g_2195);
        }
        else
        { /* block id: 1175 */
            uint16_t l_2436 = 65527UL;
            int32_t l_2455 = 0x46A76BEBL;
            int32_t **l_2464 = (void*)0;
            const uint64_t l_2472 = 7UL;
            float l_2487 = 0xF.C8F240p-54;
            int32_t l_2489 = 0xB502D3D8L;
            int32_t l_2498 = 0x4363F4B6L;
            int32_t l_2499 = 0xB2973525L;
            int32_t l_2502 = 0xB75DD5B5L;
            int32_t l_2507 = 0x45373958L;
            int32_t l_2508[10] = {3L,0L,(-1L),(-1L),0L,3L,0L,(-1L),(-1L),0L};
            int8_t l_2511[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            uint32_t l_2530 = 0UL;
            uint16_t ** const *l_2548 = (void*)0;
            const uint16_t l_2559[2] = {65535UL,65535UL};
            int i;
            for (g_1540 = 0; (g_1540 >= 0); g_1540 -= 1)
            { /* block id: 1178 */
                int32_t *l_2435[10] = {(void*)0,&g_1279[1],(void*)0,&g_1279[1],(void*)0,&g_1279[1],(void*)0,&g_1279[1],(void*)0,&g_1279[1]};
                float ***l_2467 = &g_278;
                int32_t l_2517[10] = {0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L,0xC12C3677L};
                int16_t l_2523 = 0x74A7L;
                const union U0 *l_2535 = &g_767;
                uint8_t l_2578 = 248UL;
                uint32_t **l_2581 = &g_1802[2];
                uint64_t *l_2631 = &g_72;
                int i;
                l_2436--;
                for (l_2345 = 0; (l_2345 <= 5); l_2345 += 1)
                { /* block id: 1182 */
                    int8_t l_2444 = 0x8DL;
                    uint16_t *l_2445 = &g_103;
                    union U0 ****l_2447 = &g_1725;
                    union U0 *****l_2446[9][10][2] = {{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0}},{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0}},{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0}},{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0}},{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0}},{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0}},{{&l_2447,&l_2447},{(void*)0,(void*)0},{(void*)0,&l_2447},{&l_2447,(void*)0},{&l_2447,(void*)0},{(void*)0,&l_2447},{&l_2447,&l_2447},{&l_2447,&l_2447},{(void*)0,(void*)0},{&l_2447,(void*)0}},{{(void*)0,&l_2447},{&l_2447,&l_2447},{&l_2447,&l_2447},{(void*)0,(void*)0},{&l_2447,(void*)0},{(void*)0,&l_2447},{&l_2447,&l_2447},{&l_2447,&l_2447},{(void*)0,(void*)0},{&l_2447,(void*)0}},{{(void*)0,&l_2447},{&l_2447,&l_2447},{&l_2447,&l_2447},{(void*)0,(void*)0},{&l_2447,(void*)0},{(void*)0,&l_2447},{&l_2447,&l_2447},{&l_2447,&l_2447},{(void*)0,(void*)0},{&l_2447,(void*)0}}};
                    uint32_t ** const *l_2454 = &g_1338;
                    uint32_t ** const **l_2453[9][3] = {{&l_2454,&l_2454,&l_2454},{&l_2454,&l_2454,&l_2454},{&l_2454,&l_2454,&l_2454},{&l_2454,&l_2454,&l_2454},{&l_2454,&l_2454,&l_2454},{(void*)0,&l_2454,&l_2454},{&l_2454,&l_2454,&l_2454},{&l_2454,&l_2454,&l_2454},{&l_2454,&l_2454,(void*)0}};
                    uint32_t ** const ***l_2452 = &l_2453[7][0];
                    int32_t *l_2456 = &g_1279[1];
                    int32_t l_2470 = 1L;
                    int32_t l_2491 = 0xCDB3F5A9L;
                    int32_t l_2493 = 0L;
                    int32_t l_2495 = 0x051BC106L;
                    int32_t l_2496 = 0xF5465FD2L;
                    int32_t l_2497 = (-3L);
                    int32_t l_2501 = 0L;
                    int32_t l_2506 = 1L;
                    int32_t l_2509 = 0x6454BA2AL;
                    int32_t l_2512 = 0xAC3C3839L;
                    int32_t l_2514 = (-1L);
                    int32_t l_2515 = 0xE2EEE1C8L;
                    int32_t l_2516[10][1][7] = {{{(-1L),0xADE9C6A2L,0x52631DDCL,0x4E90041EL,0x52631DDCL,0xADE9C6A2L,(-1L)}},{{0xADE9C6A2L,0L,3L,0x52631DDCL,(-1L),0xADE9C6A2L,0x52631DDCL}},{{1L,0xF8AF2F0BL,0x2EC9CF85L,0L,0L,0x2EC9CF85L,0xF8AF2F0BL}},{{0L,(-1L),3L,0x4E90041EL,(-1L),0x52631DDCL,0xF8AF2F0BL}},{{0x362B936CL,0L,0x52631DDCL,0x362B936CL,0xF8AF2F0BL,0x362B936CL,0x52631DDCL}},{{(-1L),(-1L),(-1L),0x4E90041EL,0L,4L,(-1L)}},{{(-1L),0x52631DDCL,3L,0L,0xADE9C6A2L,0xADE9C6A2L,0L}},{{0x362B936CL,0xF8AF2F0BL,0x362B936CL,0x52631DDCL,0L,0x362B936CL,(-1L)}},{{0L,0xF8AF2F0BL,0xE5E2D772L,0x4E90041EL,0xF8AF2F0BL,(-1L),0xF8AF2F0BL}},{{1L,0x52631DDCL,0x52631DDCL,1L,(-1L),0x362B936CL,0L}}};
                    int i, j, k;
                    l_2455 = (safe_add_func_float_f_f((!((0x1.F4F6CBp+72 > (g_2442 == ((p_16 == ((*l_2445) &= l_2444)) , l_2446[5][4][1]))) > ((safe_add_func_float_f_f((safe_add_func_float_f_f((p_16 != (l_2444 >= p_16)), ((&g_2049 == l_2452) <= p_16))), p_16)) > p_16))), (*l_2268)));
                    for (l_2227 = 0; (l_2227 >= 0); l_2227 -= 1)
                    { /* block id: 1187 */
                        int32_t ***l_2458 = &l_2226;
                        int i, j, k;
                        l_2456 = g_1122[g_1540][(g_1540 + 1)];
                        if (g_47[l_2345][(g_1540 + 1)][(l_2227 + 3)])
                            break;
                        if (l_2345)
                            goto lbl_2457;
                        (*l_2458) = (void*)0;
                    }
                    for (g_1221 = 5; (g_1221 >= 0); g_1221 -= 1)
                    { /* block id: 1195 */
                        int32_t ***l_2462 = (void*)0;
                        int32_t ***l_2463 = &g_942;
                        const int32_t l_2473 = 1L;
                        int32_t l_2478 = 1L;
                        if (g_2248)
                            goto lbl_2459;
                        (*g_5) |= (safe_div_func_uint64_t_u_u(((((*l_2463) = &g_943) == l_2464) ^ ((((0UL && (safe_mul_func_uint16_t_u_u((((l_2455 = ((*g_75) = ((void*)0 == l_2467))) , (((l_2455 = (safe_sub_func_int64_t_s_s((l_2395[0][6][1] |= ((-5L) != (l_2470 , 0xAEL))), (l_2471 | l_2472)))) <= (-9L)) & 18446744073709551607UL)) & 0xFEL), l_2473))) && (*l_2268)) , l_2436) >= l_2473)), 0x5599A07CED22EAF4LL));
                        l_2478 &= (safe_sub_func_uint16_t_u_u(p_16, ((safe_add_func_uint64_t_u_u((&p_15 != &p_15), p_16)) | p_16)));
                    }
                    if (((g_752[(g_1540 + 4)] = l_2479) != g_2480))
                    { /* block id: 1206 */
                        int32_t l_2481[5];
                        int32_t l_2482 = 4L;
                        int32_t l_2483 = 3L;
                        int32_t l_2484 = 7L;
                        int32_t l_2485 = 0xAB73C1A8L;
                        int32_t l_2486 = (-5L);
                        int32_t l_2488 = 0x74454D52L;
                        int32_t l_2490 = 0x0ECA9D41L;
                        int32_t l_2492 = 0x9777C8B3L;
                        int32_t l_2494 = 0x0E41B89AL;
                        int32_t l_2500 = 0x3FBE186BL;
                        int32_t l_2503 = 9L;
                        int32_t l_2504 = (-1L);
                        int32_t l_2505 = (-1L);
                        int32_t l_2510 = 5L;
                        int32_t l_2513[5][8] = {{(-1L),(-1L),1L,0L,1L,(-1L),(-1L),1L},{0x216E67A4L,1L,1L,0x216E67A4L,(-1L),0x216E67A4L,1L,1L},{1L,(-1L),0L,0L,(-1L),1L,(-1L),0L},{0x216E67A4L,(-1L),0x216E67A4L,1L,1L,0x216E67A4L,(-1L),0x216E67A4L},{(-1L),1L,0L,1L,(-1L),(-1L),1L,0L}};
                        int i, j;
                        for (i = 0; i < 5; i++)
                            l_2481[i] = 0xEE030863L;
                        g_2518++;
                        if (p_16)
                            continue;
                        if (l_2436)
                            goto lbl_2521;
                    }
                    else
                    { /* block id: 1210 */
                        float l_2522 = 0xE.586949p-40;
                        int32_t l_2524 = 0L;
                        int32_t l_2525 = (-6L);
                        int32_t l_2526 = 0xE7DAE0BDL;
                        int32_t l_2527 = (-10L);
                        int32_t l_2528 = 0x2F0685F3L;
                        int32_t l_2529[7] = {6L,6L,1L,6L,6L,1L,6L};
                        uint16_t ** const *l_2540 = &g_399;
                        uint16_t ** const **l_2541[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2541[i] = &l_2540;
                        l_2395[0][6][1] |= (((--l_2530) , (*g_75)) , (safe_mul_func_int16_t_s_s(((void*)0 != l_2535), (safe_rshift_func_int16_t_s_s(((((~((((g_2539[0][0][7] , &l_2447) == (((l_2548 = (g_2546 = (l_2543[6] = (g_2542 = l_2540)))) == (void*)0) , g_2549)) <= ((*g_71) ^ (*g_71))) != 0x951D44C4L)) >= 18446744073709551615UL) == (*g_71)) == 18446744073709551608UL), 2)))));
                        if (l_2527)
                            break;
                        if (l_2553[4][1])
                            continue;
                    }
                }
                for (l_2207 = 0; (l_2207 <= 5); l_2207 += 1)
                { /* block id: 1223 */
                    int8_t l_2560[4];
                    union U0 ** const *l_2603 = &g_1274;
                    union U0 ** const **l_2602[7][3] = {{&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603}};
                    int32_t l_2607 = 0xD9B15B36L;
                    uint64_t l_2629 = 0x8D48908148308D4FLL;
                    int i, j;
                    for (i = 0; i < 4; i++)
                        l_2560[i] = 0xA4L;
                    l_2395[0][6][1] |= (safe_sub_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u(((((!l_2559[1]) < l_2560[0]) && (p_16 ^ (((safe_div_func_int32_t_s_s((((l_2530 < ((*g_458) = (safe_div_func_int16_t_s_s((((safe_sub_func_int32_t_s_s((safe_sub_func_uint32_t_u_u(p_16, (safe_div_func_uint64_t_u_u(((l_2560[0] , ((((((safe_mul_func_int8_t_s_s(((g_2573 != (void*)0) , (((*l_2268) >= (*l_2268)) && (-1L))), 0x3AL)) , 0x0C93FF78F4561DA4LL) , l_2559[1]) != p_16) , l_2577) != (*l_2202))) & 0xF946L), p_16)))), 0x44ED3D02L)) , (*l_2268)) <= p_16), l_2560[0])))) >= 5L) != p_16), l_2578)) , (*l_2268)) > p_16))) >= p_16), p_16)), (*g_75)));
                    for (g_799 = 3; (g_799 >= 0); g_799 -= 1)
                    { /* block id: 1228 */
                        int16_t l_2598 = 0x230DL;
                        const union U0 **l_2601 = &l_2535;
                        const union U0 ***l_2600 = &l_2601;
                        const union U0 **** const l_2599 = &l_2600;
                        uint32_t *l_2608 = &g_50;
                        int16_t *l_2613 = (void*)0;
                        int16_t *l_2614[6][1][9] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_2598,&l_2598,&g_851,&l_2523,&l_2523,&l_2523,&g_851,&l_2598,&l_2598}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_2523,&l_2523,&g_851,&l_2523,&l_2523,&l_2598,&l_2598,&l_2523,&l_2523}},{{(void*)0,(void*)0,(void*)0,(void*)0,&l_2345,&l_2345,(void*)0,(void*)0,(void*)0}},{{&l_2598,&g_851,&l_2598,&g_851,&g_851,&l_2598,&g_851,&l_2598,&g_851}}};
                        uint16_t l_2621 = 1UL;
                        uint32_t l_2628[6][2][6] = {{{1UL,0x72530C57L,18446744073709551615UL,18446744073709551615UL,0x47F8A6D3L,1UL},{18446744073709551615UL,0xA93CEC5FL,18446744073709551613UL,18446744073709551612UL,0xF6F3142EL,0UL}},{{0x0BFB3527L,8UL,0x0BA164ECL,0xEA1CDF07L,0xEA1CDF07L,0x0BA164ECL},{0x4424549DL,0x4424549DL,0xEA1CDF07L,18446744073709551613UL,0xDC2945AEL,0x47F8A6D3L}},{{18446744073709551609UL,0xF6F3142EL,0x570AC04BL,18446744073709551615UL,0x692BACF9L,0xEA1CDF07L},{1UL,18446744073709551609UL,0x570AC04BL,0xA93CEC5FL,0x4424549DL,0x47F8A6D3L}},{{18446744073709551612UL,0xA93CEC5FL,0xEA1CDF07L,0UL,0x5CBBB91DL,0x0BA164ECL},{0UL,0x5CBBB91DL,0x0BA164ECL,18446744073709551615UL,0x72530C57L,0UL}},{{18446744073709551615UL,1UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,1UL},{18446744073709551613UL,8UL,18446744073709551615UL,0UL,0x692BACF9L,18446744073709551615UL}},{{0x4424549DL,18446744073709551613UL,0UL,0x72530C57L,0xA76985ACL,0x47F8A6D3L},{18446744073709551615UL,18446744073709551613UL,18446744073709551609UL,0x0BA164ECL,0x692BACF9L,0xE3A2FFEBL}}};
                        uint64_t **l_2632[5][5] = {{&l_2430,&l_2430,&l_2631,&l_2430,(void*)0},{&l_2430,&l_2430,&l_2631,&l_2430,&l_2430},{&l_2631,(void*)0,&l_2430,(void*)0,(void*)0},{&l_2430,&l_2430,&l_2430,&l_2430,(void*)0},{&l_2430,&l_2430,&l_2631,(void*)0,(void*)0}};
                        int i, j, k;
                        (*g_5) |= (((safe_mul_func_uint16_t_u_u(9UL, (((l_2581 == &g_1802[3]) || (safe_rshift_func_uint16_t_u_u(((((l_2560[0] < (l_2498 > (safe_lshift_func_int16_t_s_s((safe_add_func_uint8_t_u_u(((safe_mod_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(((p_16 != (safe_mod_func_uint32_t_u_u(l_2598, p_16))) > p_16), p_16)), p_16)) == (*g_75)), (**g_2576))) > l_2436), (*g_75))), l_2598)))) , l_2599) == l_2602[2][0]) <= 0L), 0))) , p_16))) && p_16) >= 0x31D7ECC63A4A63D3LL);
                        l_2628[2][1][2] ^= ((~(safe_div_func_uint32_t_u_u((--(*l_2608)), 0xEC9F6EF8L))) , (((safe_mul_func_uint8_t_u_u(p_16, (**g_457))) >= ((l_2395[0][6][1] = (-3L)) , (((safe_rshift_func_uint8_t_u_s(((**g_879) ^ (safe_sub_func_uint8_t_u_u(((l_2507 = (safe_lshift_func_int16_t_s_u(l_2621, ((*g_458) && ((safe_sub_func_uint32_t_u_u(((safe_mod_func_int16_t_s_s(((p_16 , ((void*)0 != l_2626)) && 0x43L), p_16)) || g_47[3][8][1]), l_2627)) && 0xC8FA95F1L))))) ^ 0x59CE29AAL), p_16))), (*g_75))) >= p_16) < (-7L)))) , (*g_5)));
                        if (l_2629)
                            continue;
                        l_2499 |= ((~(l_2560[3] <= ((l_2430 = l_2631) != (***g_2574)))) == (safe_mul_func_int8_t_s_s((!(safe_mod_func_int32_t_s_s((l_2455 = (0x6547L | (safe_div_func_uint16_t_u_u(0x48A0L, (g_1867 = (safe_sub_func_int32_t_s_s(p_16, (safe_lshift_func_int8_t_s_u(9L, ((l_2607 = 0xB915AE76L) && (safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((((p_16 >= p_16) , (*****g_2549)) , 1UL) && p_16), p_16)), p_16)) != 0x9420L), l_2629)), p_16)))))))))))), l_2629))), l_2653)));
                    }
                }
            }
        }
        return l_2654;
    }
    else
    { /* block id: 1245 */
        int32_t *l_2655 = &g_47[3][5][3];
        return l_2655;
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_1279 g_879 g_880 g_616 g_75 g_76 g_49 g_71 g_36 g_37 g_252 g_273 g_274 g_1233 g_1234 g_1235 g_1236 g_1237 g_778 g_72 g_100 g_47 g_96 g_248 g_5 g_6 g_140 g_241 g_272 g_99.f0 g_103 g_99 g_345 g_346 g_881 g_882 g_614 g_458 g_1347 g_1362 g_1380 g_851 g_1244 g_1496 g_1493 g_1490 g_344 g_376 g_1540 g_1221 g_799 g_752 g_753 g_1690 g_943 g_454 g_1520 g_755.f0 g_1777 g_1728 g_894.f0 g_1801 g_1584 g_1691 g_1148.f0 g_374 g_808 g_399 g_381 g_1820 g_400 g_1948 g_1867 g_1971 g_457 g_491 g_513 g_1338 g_1122 g_2114 g_942 g_1489 g_1491 g_278 g_2178 g_1725 g_1274
 * writes: g_376 g_49 g_1314 g_37 g_248 g_6 g_96 g_140 g_241 g_72 g_103 g_272 g_278 g_100 g_47 g_76 g_616 g_1121 g_1338 g_36 g_252 g_614 g_851 g_778 g_1489 g_1493 g_274 g_352 g_1520 g_1221 g_799 g_1690 g_491 g_1725 g_1728 g_1279 g_381 g_374 g_1867 g_1971 g_2049 g_454 g_1275 g_752
 */
static int8_t * func_17(uint8_t  p_18, int32_t  p_19, int32_t * p_20, const uint64_t  p_21, uint64_t  p_22)
{ /* block id: 680 */
    int64_t *l_1305 = &g_376;
    int64_t *l_1306 = &g_49;
    int32_t l_1309 = 0x9CA9169CL;
    int64_t *l_1313 = &g_1314;
    int64_t **l_1312 = &l_1313;
    int32_t l_1315 = 1L;
    uint64_t *l_1316 = &g_248;
    int64_t l_1322 = 0x7BB32AF3FBE0C9DELL;
    uint32_t **l_1336 = &g_1122[0][3];
    int16_t *l_1367 = &g_851;
    int16_t **l_1366 = &l_1367;
    int16_t ***l_1365 = &l_1366;
    float ***l_1402[2];
    float ****l_1401 = &l_1402[1];
    float *****l_1400 = &l_1401;
    uint64_t l_1408[8] = {0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL,0x5E3380893CC0BDF2LL};
    int32_t l_1433 = 0xC3DD7576L;
    int32_t l_1434 = 0xDFE41218L;
    int32_t l_1435[3][5][7] = {{{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)},{8L,8L,8L,8L,8L,8L,8L},{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)},{8L,8L,8L,8L,8L,8L,8L},{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)}},{{8L,8L,8L,8L,8L,8L,8L},{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)},{8L,8L,8L,8L,8L,8L,8L},{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)},{8L,8L,8L,8L,8L,8L,8L}},{{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)},{8L,8L,8L,8L,8L,8L,8L},{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)},{8L,8L,8L,8L,8L,8L,8L},{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)}}};
    int8_t *l_1473 = &g_491;
    int32_t * const **l_1597 = (void*)0;
    union U0 **l_1630 = &g_752[0];
    uint32_t l_1633 = 0x5471D3D5L;
    uint32_t l_1648 = 4294967295UL;
    uint32_t l_1658 = 0xD1BBBD33L;
    uint32_t l_1671 = 0x18EDEDC6L;
    uint32_t *l_1678 = (void*)0;
    uint32_t *l_1679 = &g_1520;
    uint16_t *l_1680 = &g_103;
    int64_t l_1681 = 1L;
    int64_t l_1682 = 0xC71414A058D2BD6BLL;
    int32_t *l_1683 = &l_1435[2][0][3];
    const uint8_t ***l_1692 = (void*)0;
    int32_t l_1695 = (-6L);
    uint64_t l_1711 = 0x309F1806F457B790LL;
    uint32_t l_1742 = 0xF7F3980DL;
    const int32_t *l_1819 = &g_1820;
    uint32_t **l_1827[1][6][8] = {{{&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2]},{&g_1802[0],&g_1802[0],&g_1802[2],&g_1802[0],&g_1802[0],&g_1802[2],&g_1802[0],&g_1802[0]},{&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0]},{&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2]},{&g_1802[0],&g_1802[0],&g_1802[2],&g_1802[0],&g_1802[0],&g_1802[2],&g_1802[0],&g_1802[0]},{&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0],&g_1802[2],&g_1802[2],&g_1802[0]}}};
    uint32_t l_1839 = 0x36E9EDFBL;
    int8_t l_1864 = 0xA4L;
    uint16_t ***l_1887 = (void*)0;
    uint32_t l_1890 = 4294967290UL;
    float l_1979 = 0x7.F831F6p+48;
    uint8_t l_2044 = 255UL;
    uint64_t l_2185[5][10][5] = {{{0xE60F951FE77DD688LL,18446744073709551610UL,0xB3FD5A5FE0A8EAC8LL,0x597A61EEFB6F65E7LL,0xB3FD5A5FE0A8EAC8LL},{18446744073709551610UL,18446744073709551610UL,18446744073709551612UL,1UL,1UL},{0x85AF1FBCAD4D93F6LL,18446744073709551610UL,0x6942A1E948708EF6LL,0xD52025C9DDB98143LL,18446744073709551612UL},{0UL,18446744073709551610UL,1UL,0UL,5UL},{1UL,18446744073709551610UL,5UL,0xC90D29729CF30836LL,0x6942A1E948708EF6LL},{0xE60F951FE77DD688LL,18446744073709551610UL,0xB3FD5A5FE0A8EAC8LL,0x597A61EEFB6F65E7LL,0xB3FD5A5FE0A8EAC8LL},{18446744073709551610UL,18446744073709551610UL,18446744073709551612UL,1UL,1UL},{0x85AF1FBCAD4D93F6LL,18446744073709551610UL,0x6942A1E948708EF6LL,0xD52025C9DDB98143LL,18446744073709551612UL},{0UL,18446744073709551610UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL}},{{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL},{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL}},{{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL},{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL}},{{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL},{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL}},{{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,6UL,0x7C14B383E537BFB6LL,18446744073709551612UL},{0xB3FD5A5FE0A8EAC8LL,18446744073709551612UL,0x8F3D4210E164CAF1LL,18446744073709551610UL,6UL},{5UL,18446744073709551612UL,18446744073709551612UL,0xF833FCB50BF34FDALL,6UL},{1UL,18446744073709551612UL,6UL,18446744073709551612UL,0x8F3D4210E164CAF1LL},{0x6942A1E948708EF6LL,18446744073709551612UL,18446744073709551613UL,8UL,18446744073709551613UL},{18446744073709551612UL,18446744073709551612UL,1UL,1UL,0xD52025C9DDB98143LL},{18446744073709551613UL,6UL,0xC90D29729CF30836LL,0x85AF1FBCAD4D93F6LL,1UL},{6UL,6UL,0xD52025C9DDB98143LL,0xE60F951FE77DD688LL,0x597A61EEFB6F65E7LL},{18446744073709551612UL,6UL,0x597A61EEFB6F65E7LL,0UL,0xC90D29729CF30836LL}}};
    float ** const **l_2186 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1402[i] = &g_278;
    if (((*p_20) & (l_1315 ^= (safe_mul_func_int8_t_s_s(((0x56L & (+(!(**g_879)))) <= (((((*l_1305) = ((*g_75) != (*g_75))) != ((*l_1306) ^= p_19)) ^ (g_1314 = (safe_add_func_uint16_t_u_u((((((l_1309 ^ (((-2L) > (safe_add_func_uint16_t_u_u((((*l_1312) = g_71) != (void*)0), l_1309))) & 0x5E1BL)) && p_19) != (*g_36)) > 0x01F5L) || l_1309), l_1309)))) , g_252)), 0xADL)))))
    { /* block id: 686 */
        uint32_t *l_1319[7] = {&g_381,(void*)0,&g_381,&g_381,(void*)0,&g_381,&g_381};
        const int32_t l_1324 = (-1L);
        int16_t *l_1325[10][7] = {{(void*)0,&g_851,&g_851,(void*)0,&g_851,&g_851,&g_851},{&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851},{&g_851,&g_851,&g_851,(void*)0,&g_851,&g_851,(void*)0},{&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851},{&g_851,(void*)0,&g_851,&g_851,(void*)0,&g_851,&g_851},{(void*)0,&g_851,&g_851,&g_851,(void*)0,(void*)0,&g_851},{&g_851,(void*)0,&g_851,&g_851,(void*)0,&g_851,(void*)0},{&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0,&g_851},{&g_851,&g_851,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_851,(void*)0,(void*)0,&g_851,&g_851,&g_851}};
        uint32_t *l_1330 = &g_616;
        int32_t l_1331 = 0x7D7382AAL;
        uint32_t ***l_1337 = &g_1121[0];
        int32_t l_1343 = 9L;
        int32_t l_1344[9];
        union U0 ** const *l_1346 = &g_1274;
        union U0 ** const **l_1345 = &l_1346;
        int16_t l_1379 = 1L;
        int16_t l_1463 = 0x3CEAL;
        float *****l_1488 = &l_1401;
        int8_t *l_1495 = &g_491;
        int i, j;
        for (i = 0; i < 9; i++)
            l_1344[i] = 0xA9289F74L;
        p_20 = func_66(l_1316, &l_1315, &g_241, (((safe_mul_func_float_f_f(((void*)0 == l_1319[0]), ((((safe_mul_func_float_f_f((((l_1322 != (+(((0L != p_18) < (((3UL == 65535UL) != l_1322) ^ 0xCC92L)) , p_22))) <= p_19) <= p_18), l_1324)) <= (*g_273)) != (*****g_1233)) != p_21))) , l_1325[4][1]) != (void*)0));
        (*l_1345) = ((((safe_mul_func_uint8_t_u_u((l_1344[1] = (safe_div_func_uint8_t_u_u(((l_1324 ^ (l_1331 = ((*l_1330) = 0x1DE068B3L))) <= ((*l_1306) = (safe_lshift_func_uint8_t_u_u((((l_1343 &= ((l_1324 && (safe_sub_func_int8_t_s_s((((((*l_1337) = l_1336) != (g_1338 = &g_1122[0][6])) == (safe_mul_func_int16_t_s_s((l_1324 >= (*g_345)), (safe_lshift_func_uint16_t_u_s(((*g_75) < ((((*g_881) == (*g_881)) ^ p_21) , l_1309)), l_1324))))) && 0UL), p_18))) > (*p_20))) , p_22) ^ g_614), (*g_458))))), p_18))), l_1322)) != l_1309) < l_1322) , (void*)0);
lbl_1410:
        (*g_1347) = &p_19;
        for (g_252 = 0; (g_252 <= 3); g_252 += 1)
        { /* block id: 699 */
            uint8_t ***l_1361[8][3][10] = {{{&g_457,&g_456,(void*)0,&g_457,&g_456,(void*)0,&g_457,&g_457,&g_456,&g_457},{&g_456,&g_456,&g_456,&g_457,&g_456,&g_457,&g_457,&g_457,&g_456,(void*)0},{&g_457,&g_457,&g_456,&g_456,(void*)0,(void*)0,(void*)0,&g_457,&g_457,&g_457}},{{&g_457,&g_457,&g_456,&g_456,&g_456,&g_457,&g_457,&g_456,&g_456,(void*)0},{&g_456,&g_457,&g_457,&g_457,&g_457,&g_456,&g_456,&g_456,&g_457,&g_456},{&g_457,&g_457,&g_456,&g_457,&g_457,&g_456,&g_457,(void*)0,&g_456,(void*)0}},{{&g_456,&g_457,&g_457,&g_457,(void*)0,(void*)0,(void*)0,&g_456,&g_457,&g_457},{&g_457,&g_457,(void*)0,(void*)0,&g_457,&g_457,&g_457,&g_457,&g_456,(void*)0},{&g_457,&g_456,&g_456,(void*)0,&g_457,&g_456,&g_457,&g_457,&g_456,&g_456}},{{&g_457,&g_456,&g_456,(void*)0,&g_456,&g_456,&g_456,(void*)0,(void*)0,(void*)0},{(void*)0,&g_456,(void*)0,&g_456,&g_456,&g_457,&g_456,&g_456,&g_457,&g_456},{&g_457,&g_457,&g_456,&g_456,&g_456,&g_456,&g_456,&g_457,&g_456,(void*)0}},{{&g_456,&g_456,&g_456,&g_457,&g_456,(void*)0,&g_456,&g_456,(void*)0,(void*)0},{(void*)0,&g_457,&g_456,&g_457,&g_457,&g_457,(void*)0,&g_457,&g_456,&g_457},{&g_456,&g_456,(void*)0,&g_457,&g_457,&g_456,&g_457,&g_456,(void*)0,&g_456}},{{&g_457,&g_456,(void*)0,&g_457,&g_456,&g_457,&g_456,&g_457,&g_456,&g_457},{(void*)0,&g_456,(void*)0,&g_456,&g_456,&g_457,(void*)0,&g_457,&g_457,(void*)0},{&g_456,&g_456,(void*)0,&g_456,&g_456,&g_457,(void*)0,&g_457,&g_457,&g_456}},{{&g_457,&g_456,(void*)0,&g_457,&g_457,&g_456,&g_456,&g_457,&g_456,(void*)0},{&g_456,&g_456,(void*)0,&g_456,&g_457,&g_456,&g_456,&g_456,(void*)0,&g_457},{&g_457,&g_457,(void*)0,&g_456,(void*)0,(void*)0,&g_457,&g_457,&g_457,&g_456}},{{&g_456,(void*)0,&g_456,(void*)0,&g_457,&g_456,&g_457,&g_456,&g_456,&g_457},{&g_457,&g_457,&g_456,&g_456,&g_457,&g_457,&g_457,&g_457,&g_456,&g_457},{&g_457,&g_456,&g_456,&g_456,&g_456,(void*)0,(void*)0,&g_456,(void*)0,&g_457}}};
            uint8_t ****l_1360 = &l_1361[5][2][0];
            int32_t l_1368 = 0xD7AFB120L;
            uint16_t ***l_1421[5] = {&g_399,&g_399,&g_399,&g_399,&g_399};
            int32_t l_1439 = 0x2FE01541L;
            int32_t l_1441 = 0x4C7F069DL;
            int32_t l_1448 = 6L;
            int32_t l_1449[9][1];
            float *****l_1492 = &g_1490;
            int8_t *l_1497 = &g_241;
            int i, j, k;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1449[i][j] = 0x9EB72015L;
            }
            if ((safe_lshift_func_int16_t_s_u((safe_sub_func_int8_t_s_s((-4L), (p_19 == (safe_mod_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u(l_1322, ((safe_mod_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((-1L), ((p_19 , l_1360) != (void*)0))), p_18)) , ((65535UL && g_1362[0]) & 0x2DL)))), p_18))))), 13)))
            { /* block id: 700 */
                int16_t **l_1364 = &l_1325[4][1];
                int16_t ***l_1363 = &l_1364;
                l_1365 = l_1363;
            }
            else
            { /* block id: 702 */
                int8_t l_1422 = 0L;
                int32_t l_1423 = 1L;
                int32_t l_1426 = 0L;
                int32_t l_1427 = 1L;
                int32_t l_1428 = 0xB0C798E6L;
                int32_t l_1430 = (-1L);
                int32_t l_1431 = (-3L);
                int32_t l_1432 = 0L;
                int32_t l_1436 = 0x3F2572C4L;
                int32_t l_1440 = (-4L);
                float l_1442 = 0x0.1B6837p-28;
                int32_t l_1443 = 0L;
                int32_t l_1445 = 0xC3108D87L;
                int32_t l_1446 = (-5L);
                int32_t l_1447 = 0L;
                int32_t l_1451 = (-3L);
                int32_t l_1452 = 0xE90BB7A2L;
                int32_t l_1453 = 0L;
                int8_t l_1466 = (-2L);
                union U0 ***l_1480 = &g_1274;
                union U0 ***l_1482 = &g_1274;
                union U0 ****l_1481 = &l_1482;
                float *****l_1494[6] = {&l_1401,&l_1401,(void*)0,&l_1401,&l_1401,(void*)0};
                int32_t *l_1518 = &l_1433;
                int i;
                for (g_614 = 0; (g_614 <= 3); g_614 += 1)
                { /* block id: 705 */
                    uint32_t l_1382 = 0xD1836738L;
                    int32_t l_1429 = 0L;
                    int32_t l_1437 = 0x371AB332L;
                    int32_t l_1444[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1444[i] = 0x4E9DCE55L;
                    if ((*g_36))
                    { /* block id: 706 */
                        uint64_t l_1378 = 5UL;
                        (*p_20) |= ((((g_1279[1] == l_1368) || ((safe_sub_func_int32_t_s_s((safe_div_func_int8_t_s_s((safe_mod_func_int32_t_s_s(1L, ((l_1368 , (((~p_21) ^ (-1L)) <= (safe_add_func_int64_t_s_s(l_1368, (&g_5 != &g_352))))) , l_1378))), l_1379)), l_1322)) >= (**g_1347))) , g_1380[0][6][0]) , 0xA8E98079L);
                        if ((*g_36))
                            continue;
                        (*g_5) &= (0x4896L | 65535UL);
                    }
                    else
                    { /* block id: 710 */
                        int32_t *l_1381[9][10][2] = {{{(void*)0,&l_1344[6]},{&l_1309,(void*)0},{(void*)0,&g_1279[1]},{&g_37,&l_1315},{&l_1309,(void*)0},{&l_1315,&g_1279[1]},{&l_1315,(void*)0},{&l_1309,&l_1315},{&g_37,&g_1279[1]},{(void*)0,(void*)0}},{{&l_1309,&l_1344[6]},{(void*)0,&g_1279[1]},{&g_37,(void*)0},{&l_1309,&g_37},{&l_1315,&g_1279[1]},{&l_1315,&g_37},{&l_1309,(void*)0},{&g_37,&g_1279[1]},{&l_1309,&g_37},{&g_37,&l_1309}},{{&l_1309,&l_1344[1]},{&g_1279[1],&l_1315},{&g_37,&g_1279[1]},{&l_1315,&l_1309},{&l_1315,&g_1279[1]},{&g_37,&l_1315},{&g_1279[1],&l_1344[1]},{&l_1309,&l_1309},{&g_37,&g_37},{&l_1309,&l_1309}},{{&g_1279[1],(void*)0},{&g_37,&g_1279[1]},{&l_1315,&l_1344[1]},{&l_1315,&g_1279[1]},{&g_37,(void*)0},{&g_1279[1],&l_1309},{&l_1309,&g_37},{&g_37,&l_1309},{&l_1309,&l_1344[1]},{&g_1279[1],&l_1315}},{{&g_37,&g_1279[1]},{&l_1315,&l_1309},{&l_1315,&g_1279[1]},{&g_37,&l_1315},{&g_1279[1],&l_1344[1]},{&l_1309,&l_1309},{&g_37,&g_37},{&l_1309,&l_1309},{&g_1279[1],(void*)0},{&g_37,&g_1279[1]}},{{&l_1315,&l_1344[1]},{&l_1315,&g_1279[1]},{&g_37,(void*)0},{&g_1279[1],&l_1309},{&l_1309,&g_37},{&g_37,&l_1309},{&l_1309,&l_1344[1]},{&g_1279[1],&l_1315},{&g_37,&g_1279[1]},{&l_1315,&l_1309}},{{&l_1315,&g_1279[1]},{&g_37,&l_1315},{&g_1279[1],&l_1344[1]},{&l_1309,&l_1309},{&g_37,&g_37},{&l_1309,&l_1309},{&g_1279[1],(void*)0},{&g_37,&g_1279[1]},{&l_1315,&l_1344[1]},{&l_1315,&g_1279[1]}},{{&g_37,(void*)0},{&g_1279[1],&l_1309},{&l_1309,&g_37},{&g_37,&l_1309},{&l_1309,&l_1344[1]},{&g_1279[1],&l_1315},{&g_37,&g_1279[1]},{&l_1315,&l_1309},{&l_1315,&g_1279[1]},{&g_37,&l_1315}},{{&g_1279[1],&l_1344[1]},{&l_1309,&l_1309},{&g_37,&g_37},{&l_1309,&l_1309},{&g_1279[1],(void*)0},{&g_37,&g_1279[1]},{&l_1315,&l_1344[1]},{&l_1315,&g_1279[1]},{&g_37,(void*)0},{&g_1279[1],&l_1309}}};
                        float ***l_1399 = &g_278;
                        float ****l_1398[5];
                        float *****l_1397 = &l_1398[2];
                        uint16_t *l_1407 = &g_103;
                        int32_t l_1409 = 0x14DB6D0CL;
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_1398[i] = &l_1399;
                        l_1382--;
                        (*g_1237) = ((l_1368 < (safe_add_func_float_f_f(((0x8.BD7293p-56 > (p_21 <= ((safe_sub_func_float_f_f(((0x93DD8DB9L >= ((safe_mul_func_int16_t_s_s(((***l_1365) ^= (((*p_20) > ((safe_add_func_int16_t_s_s((-1L), (((*l_1407) = (safe_lshift_func_int8_t_s_u((((((safe_rshift_func_int16_t_s_u(((l_1400 = l_1397) == ((safe_lshift_func_int8_t_s_s((((p_18 ^ ((safe_mul_func_uint16_t_u_u((p_18 != p_21), l_1309)) > p_21)) > 0xD9L) != p_19), 0)) , (void*)0)), 14)) & l_1309) | (*p_20)) | 1L) == p_19), p_22))) == (-1L)))) != l_1408[5])) > l_1382)), l_1368)) <= p_21)) , (*g_1237)), l_1343)) <= l_1382))) < p_19), 0x2.Dp+1))) == l_1409);
                    }
                    if (g_851)
                        goto lbl_1410;
                    (*g_5) = l_1382;
                    if (l_1368)
                        continue;
                    for (p_22 = 0; (p_22 <= 3); p_22 += 1)
                    { /* block id: 722 */
                        int32_t *l_1424 = &g_799;
                        int32_t *l_1425[8][10] = {{&g_37,&l_1315,&l_1344[1],&l_1315,&g_37,&l_1344[2],&l_1344[2],&g_37,&l_1315,&l_1344[1]},{&g_47[3][5][3],&g_47[3][5][3],&l_1344[1],&g_37,&l_1423,&g_37,&l_1344[1],&g_47[3][5][3],&g_47[3][5][3],&l_1344[1]},{&l_1315,&g_37,&l_1344[2],&l_1344[2],&g_37,&l_1315,&l_1344[1],&l_1315,&g_37,&l_1344[2]},{&g_1279[2],&g_47[3][5][3],&g_1279[2],&l_1344[2],&l_1344[1],&l_1344[1],&l_1344[2],&g_1279[2],&g_47[3][5][3],&g_1279[2]},{&g_1279[2],&l_1315,&g_47[3][5][3],&g_37,&g_47[3][5][3],&l_1315,&g_37,&g_37,&g_1279[2],&l_1423},{&g_1279[2],&g_37,&g_37,&g_1279[2],&l_1423,&l_1344[2],&l_1423,&g_1279[2],&g_37,&g_37},{&l_1423,&g_37,&l_1344[1],&g_47[3][5][3],&g_47[3][5][3],&l_1344[1],&g_37,&l_1423,&g_37,&l_1344[1]},{&l_1344[2],&g_1279[2],&g_47[3][5][3],&g_1279[2],&l_1344[2],&l_1344[1],&l_1344[1],&l_1344[2],&g_1279[2],&g_47[3][5][3]}};
                        int32_t l_1438 = 0L;
                        int16_t l_1450 = 0x5C44L;
                        uint16_t l_1454 = 0xA6A7L;
                        int i, j, k;
                    }
                }
                if ((safe_rshift_func_uint8_t_u_u((l_1473 != ((((safe_sub_func_int32_t_s_s((safe_add_func_int32_t_s_s(9L, (safe_lshift_func_int16_t_s_s(p_19, (l_1480 != ((*l_1481) = l_1480)))))), (safe_div_func_int32_t_s_s((safe_add_func_int64_t_s_s((~((((((g_1489 = l_1488) != (l_1494[5] = (g_1493 = l_1492))) > (0x5915L | l_1309)) , l_1449[2][0]) > 0x5716L) || 0x5F9DCF0AF9F168E0LL)), 1L)), (*p_20))))) , g_1244) != g_1244) , &l_1466)), l_1344[0])))
                { /* block id: 735 */
                    for (g_103 = 0; (g_103 <= 3); g_103 += 1)
                    { /* block id: 738 */
                        (*g_273) = l_1408[5];
                    }
                    if ((*p_20))
                        continue;
                    return l_1473;
                }
                else
                { /* block id: 743 */
                    for (g_76 = 3; (g_76 >= 0); g_76 -= 1)
                    { /* block id: 746 */
                        return l_1495;
                    }
                    for (l_1436 = 2; (l_1436 >= 0); l_1436 -= 1)
                    { /* block id: 751 */
                        int i;
                        (*g_1496) = &g_1279[l_1436];
                    }
                    if (l_1449[2][0])
                        break;
                }
                if ((*p_20))
                { /* block id: 756 */
                    return l_1497;
                }
                else
                { /* block id: 758 */
                    int32_t l_1519 = 0x0A2C6706L;
                    for (l_1453 = 0; l_1453 < 8; l_1453 += 1)
                    {
                        for (l_1315 = 0; l_1315 < 3; l_1315 += 1)
                        {
                            for (l_1309 = 0; l_1309 < 10; l_1309 += 1)
                            {
                                l_1361[l_1453][l_1315][l_1309] = &g_457;
                            }
                        }
                    }
                    (*g_36) ^= (6L > 5L);
                    if (l_1343)
                        continue;
                    for (l_1432 = 0; (l_1432 <= 3); l_1432 += 1)
                    { /* block id: 764 */
                        uint8_t l_1516 = 0x71L;
                        l_1331 |= (safe_rshift_func_int8_t_s_s(0xE2L, ((safe_sub_func_float_f_f(p_22, ((safe_sub_func_float_f_f((****g_1234), (safe_mul_func_float_f_f((safe_mul_func_float_f_f(((g_1520 = (l_1449[0][0] <= ((safe_add_func_float_f_f((safe_div_func_float_f_f((safe_div_func_float_f_f((l_1519 = ((*g_273) = ((safe_mul_func_float_f_f(l_1516, (((*l_1488) = (*g_1493)) == (*l_1492)))) > (!((((void*)0 != l_1518) != l_1322) <= 0x1.Ep+1))))), 0x7.555691p-83)), p_22)), l_1344[1])) != (-0x8.Ap-1)))) != l_1516), p_19)), l_1516)))) <= p_22))) , 7L)));
                    }
                }
                (*l_1518) |= 4L;
            }
            for (p_19 = 3; (p_19 >= 0); p_19 -= 1)
            { /* block id: 776 */
                int64_t l_1539 = 0L;
                uint32_t **l_1543[3][3];
                int32_t *l_1544 = &l_1331;
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_1543[i][j] = (void*)0;
                }
                l_1435[0][0][2] = ((safe_sub_func_int16_t_s_s(((safe_lshift_func_int8_t_s_s(((l_1309 ^ (((l_1449[0][0] >= ((safe_mul_func_float_f_f(((((safe_mul_func_uint16_t_u_u(p_19, p_22)) | (safe_mod_func_uint16_t_u_u((((((((void*)0 == &p_18) != (((safe_rshift_func_int8_t_s_u((safe_add_func_uint64_t_u_u((*g_71), g_1279[1])), 4)) == ((((*p_20) = (safe_mul_func_uint16_t_u_u(((((*l_1497) = ((safe_mod_func_uint32_t_u_u((((((void*)0 == &g_345) & (**g_344)) >= p_21) ^ 0xBDL), (*p_20))) == p_22)) != p_22) | p_21), l_1309))) ^ p_19) || l_1539)) > g_376)) , 249UL) >= (*g_75)) >= g_1540) > p_19), 0xD471L))) , l_1433) , l_1449[2][0]), 0xD.C1B297p-79)) > 0x3.4F812Bp+23)) , p_18) ^ l_1408[5])) < (*g_36)), (*g_75))) , l_1343), p_22)) != p_22);
                for (p_18 = 0; (p_18 <= 3); p_18 += 1)
                { /* block id: 782 */
                    uint32_t l_1541 = 3UL;
                    for (l_1448 = 0; (l_1448 <= 3); l_1448 += 1)
                    { /* block id: 785 */
                        (*p_20) = (-3L);
                    }
                    if (((*p_20) &= (l_1541 & ((void*)0 == p_20))))
                    { /* block id: 789 */
                        uint32_t **l_1542 = (void*)0;
                        int32_t **l_1545 = &g_36;
                        l_1543[0][2] = l_1542;
                        (*l_1545) = l_1544;
                    }
                    else
                    { /* block id: 792 */
                        if ((*p_20))
                            break;
                    }
                }
            }
            return l_1495;
        }
    }
    else
    { /* block id: 799 */
        uint16_t l_1551 = 65527UL;
        int32_t l_1634 = (-9L);
        int32_t l_1639 = 0x6AB798A5L;
        int32_t l_1643[8][2][4] = {{{0L,(-9L),7L,(-1L)},{(-1L),(-9L),(-9L),(-1L)}},{{0xBE465EA6L,0L,0xFA24B15CL,(-1L)},{0xFA24B15CL,(-1L),(-2L),(-9L)}},{{0xE3D2CE7AL,0x2032C11BL,0x2178FDA1L,(-9L)},{(-2L),(-1L),0xE3D2CE7AL,(-1L)}},{{0xCEF0F968L,0L,0L,(-1L)},{(-2L),(-9L),(-2L),(-1L)}},{{0x2032C11BL,(-9L),(-1L),(-2L)},{0x2032C11BL,0xBE465EA6L,(-2L),(-2L)}},{{(-2L),(-2L),0L,7L},{0xCEF0F968L,0L,0xE3D2CE7AL,0L}},{{(-2L),0xFA24B15CL,0x2178FDA1L,0xE3D2CE7AL},{0xE3D2CE7AL,0xFA24B15CL,(-2L),0L}},{{0xFA24B15CL,0L,0xFA24B15CL,7L},{0xBE465EA6L,(-2L),(-9L),(-2L)}}};
        int i, j, k;
        for (g_614 = 0; (g_614 <= 3); g_614 += 1)
        { /* block id: 802 */
            union U0 ***l_1567 = (void*)0;
            uint16_t ***l_1593 = &g_399;
            for (g_72 = 0; (g_72 <= 3); g_72 += 1)
            { /* block id: 805 */
                int32_t l_1572[5];
                uint64_t **l_1576 = &l_1316;
                int32_t *l_1583 = (void*)0;
                int i;
                for (i = 0; i < 5; i++)
                    l_1572[i] = 1L;
            }
            if ((*p_20))
                continue;
            for (g_1221 = 1; (g_1221 >= 0); g_1221 -= 1)
            { /* block id: 841 */
                uint64_t **l_1600 = &g_71;
                uint64_t ***l_1599 = &l_1600;
                uint64_t **** const l_1598 = &l_1599;
                uint64_t ****l_1602 = &l_1599;
                uint64_t *****l_1601 = &l_1602;
                (*l_1601) = l_1598;
                for (g_248 = 0; (g_248 <= 1); g_248 += 1)
                { /* block id: 845 */
                    return &g_729;
                }
            }
        }
        for (g_799 = 0; (g_799 >= 0); g_799 = safe_add_func_int32_t_s_s(g_799, 3))
        { /* block id: 852 */
            int16_t **l_1624[10][4][6] = {{{&l_1367,(void*)0,(void*)0,&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367,&l_1367,(void*)0,&l_1367}},{{(void*)0,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,(void*)0,&l_1367,&l_1367,(void*)0},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,(void*)0,&l_1367,(void*)0,&l_1367,&l_1367}},{{(void*)0,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{(void*)0,&l_1367,&l_1367,(void*)0,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367,&l_1367,(void*)0,&l_1367},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,(void*)0}},{{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,(void*)0},{(void*)0,&l_1367,&l_1367,&l_1367,&l_1367,(void*)0},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,(void*)0},{&l_1367,(void*)0,&l_1367,&l_1367,&l_1367,&l_1367}},{{&l_1367,(void*)0,(void*)0,&l_1367,&l_1367,(void*)0},{(void*)0,&l_1367,&l_1367,&l_1367,&l_1367,(void*)0},{&l_1367,&l_1367,&l_1367,(void*)0,&l_1367,(void*)0},{(void*)0,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367}},{{(void*)0,(void*)0,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367,(void*)0,(void*)0,&l_1367},{&l_1367,&l_1367,&l_1367,(void*)0,(void*)0,&l_1367},{&l_1367,&l_1367,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_1367,&l_1367,&l_1367,(void*)0,&l_1367,(void*)0},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{(void*)0,(void*)0,&l_1367,&l_1367,&l_1367,&l_1367},{(void*)0,&l_1367,&l_1367,(void*)0,&l_1367,&l_1367}},{{&l_1367,(void*)0,&l_1367,&l_1367,(void*)0,&l_1367},{(void*)0,&l_1367,&l_1367,&l_1367,(void*)0,&l_1367},{&l_1367,(void*)0,&l_1367,&l_1367,&l_1367,(void*)0},{(void*)0,(void*)0,&l_1367,&l_1367,&l_1367,(void*)0}},{{&l_1367,&l_1367,(void*)0,&l_1367,&l_1367,&l_1367},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,(void*)0,&l_1367,&l_1367,&l_1367,&l_1367},{&l_1367,(void*)0,&l_1367,&l_1367,(void*)0,&l_1367}},{{&l_1367,&l_1367,&l_1367,&l_1367,(void*)0,(void*)0},{&l_1367,(void*)0,&l_1367,&l_1367,&l_1367,(void*)0},{&l_1367,&l_1367,&l_1367,&l_1367,&l_1367,(void*)0},{&l_1367,(void*)0,(void*)0,&l_1367,&l_1367,&l_1367}}};
            int32_t l_1631 = 0x13588E02L;
            int32_t l_1632 = 0x01E5CA6CL;
            int32_t l_1640 = 0x7EBEB81BL;
            int32_t l_1641 = 0x8A1DD40CL;
            int32_t l_1642 = 5L;
            int32_t l_1644 = (-1L);
            int32_t l_1645 = 0xAB7A5C83L;
            int32_t l_1646 = 0x24A2E397L;
            int32_t l_1647 = 0x01B94FF3L;
            int i, j, k;
            for (l_1315 = 0; (l_1315 > 9); l_1315 = safe_add_func_int16_t_s_s(l_1315, 5))
            { /* block id: 855 */
                int32_t l_1618 = 0xDFF31AF5L;
                int32_t *l_1635 = &l_1433;
                int32_t *l_1636 = &l_1434;
                int32_t *l_1637 = (void*)0;
                int32_t *l_1638[7][6] = {{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634}};
                int i, j;
                l_1634 = ((+(l_1633 = (safe_sub_func_float_f_f((safe_add_func_float_f_f(((safe_sub_func_float_f_f((l_1631 = ((((((((safe_div_func_int8_t_s_s(((((safe_mod_func_uint8_t_u_u(((65527UL || l_1618) >= ((+((safe_mul_func_uint8_t_u_u(p_21, ((safe_lshift_func_int16_t_s_s(((*l_1367) = (((*l_1365) = (*l_1365)) == l_1624[3][3][5])), 1)) , (safe_rshift_func_int16_t_s_u(p_21, 13))))) && (safe_unary_minus_func_uint64_t_u((*g_71))))) || (((safe_div_func_uint8_t_u_u(((void*)0 == l_1630), 0x72L)) > 0xAEB8L) & 0x7FE72875941BDB10LL))), 0x5AL)) <= g_252) != 0x9EAA6E5345F1435FLL) | p_21), 255UL)) != 0xD5B891C8A333DBACLL) == p_19) > l_1618) > 5L) , p_22) <= 0x28754D43L) , p_18)), 0x6.5p+1)) > l_1632), p_18)), 0x6.FA4D50p+7)))) != p_19);
                ++l_1648;
                for (l_1634 = 0; (l_1634 == 22); l_1634 = safe_add_func_uint8_t_u_u(l_1634, 1))
                { /* block id: 864 */
                    int8_t *l_1653[9][8] = {{&g_241,&g_241,&g_729,&g_241,&g_76,&g_241,&g_241,&g_491},{(void*)0,&g_491,&g_76,&g_241,&g_76,&g_491,&g_491,&g_729},{&g_241,&g_241,&g_241,&g_491,(void*)0,(void*)0,&g_491,&g_491},{&g_491,&g_76,(void*)0,&g_491,&g_491,(void*)0,&g_76,&g_491},{&g_76,&g_241,&g_491,&g_241,&g_491,(void*)0,(void*)0,&g_241},{&g_491,&g_241,&g_241,&g_729,&g_241,(void*)0,&g_491,&g_491},{&g_491,&g_241,(void*)0,&g_241,&g_241,(void*)0,&g_241,&g_491},{&g_491,&g_76,&g_241,(void*)0,&g_241,(void*)0,&g_241,&g_76},{&g_491,&g_241,(void*)0,&g_241,(void*)0,&g_491,&g_729,(void*)0}};
                    int i, j;
                    return l_1653[8][7];
                }
                (*l_1635) |= ((((safe_sub_func_uint8_t_u_u((p_18 = 0x04L), (safe_rshift_func_uint8_t_u_u((l_1658 , (*g_458)), p_22)))) == ((safe_lshift_func_int8_t_s_u((safe_div_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u(((safe_rshift_func_uint16_t_u_u(0xEE68L, 4)) != (((g_799 == 0xB6D0L) | p_21) >= l_1646)), p_22)) , (*l_1636)), (*p_20))), 2)) == p_19)) > 0xDFL) < 0x228C348EL);
            }
            if ((*p_20))
                break;
        }
    }
    (*l_1683) |= (safe_sub_func_int16_t_s_s((p_21 ^ ((*g_71) = (((p_22 , ((*p_20) , (safe_lshift_func_int8_t_s_s(((((**l_1630) , l_1408[5]) > l_1671) > g_1362[0]), 5)))) == ((safe_add_func_uint64_t_u_u((((((*l_1680) |= (0x7AL >= ((safe_mod_func_uint32_t_u_u(((*l_1679) = (safe_div_func_uint8_t_u_u(l_1408[5], p_19))), p_22)) | p_21))) >= l_1681) == l_1682) > p_22), p_18)) , p_21)) == l_1658))), p_22));
    if ((safe_div_func_int16_t_s_s((0x71355D03F6EC9CB2LL < (safe_lshift_func_int8_t_s_s((0x4576B2CEL ^ (((safe_rshift_func_int8_t_s_u(((g_1690 = g_1690) != &g_345), 3)) | ((*l_1367) &= (safe_rshift_func_uint16_t_u_s(0x1604L, 3)))) && (((l_1695 && (*p_20)) != p_19) | ((*l_1313) ^= 1L)))), p_21))), (*l_1683))))
    { /* block id: 880 */
        uint32_t l_1700 = 1UL;
        union U0 ***l_1706 = &l_1630;
        union U0 ****l_1705 = &l_1706;
        union U0 ***l_1708 = (void*)0;
        union U0 ****l_1707 = &l_1708;
        union U0 ***l_1713 = &l_1630;
        union U0 ****l_1712 = &l_1713;
        int32_t l_1724 = 0xA927BFAFL;
        float ** const l_1729 = &g_273;
        int32_t l_1731 = 0xEC0ABB13L;
        int32_t l_1732 = 0x8DA5FED6L;
        int32_t l_1733 = (-3L);
        int32_t l_1734[1];
        int16_t l_1741 = 0x5C02L;
        int16_t l_1756 = 1L;
        int8_t l_1772 = 0x64L;
        uint32_t l_1809 = 0x9C3EB9FAL;
        int i;
        for (i = 0; i < 1; i++)
            l_1734[i] = 3L;
        if ((safe_sub_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u(l_1700, (safe_rshift_func_uint16_t_u_s((((*l_1473) = ((safe_rshift_func_uint16_t_u_u((*l_1683), 13)) == ((&g_1547[0] == ((*l_1705) = ((*g_943) , &l_1630))) ^ (p_18 ^ (((*l_1707) = &l_1630) != ((*l_1712) = ((safe_lshift_func_uint8_t_u_s(p_21, ((*g_75) = l_1711))) , (void*)0))))))) ^ 1L), 5)))) , 0x0C6B7FD7E282EEABLL) < l_1700), p_22)))
        { /* block id: 886 */
            int32_t l_1726 = 8L;
            int32_t l_1736 = 1L;
            int8_t l_1737 = 0x96L;
            int32_t l_1738 = 0x6D8D707BL;
            int32_t l_1739 = 0xA9424B0DL;
            int32_t l_1740[2];
            float *l_1775 = (void*)0;
            uint32_t *l_1800 = &l_1700;
            uint32_t **l_1799 = &l_1800;
            int32_t *l_1806 = &g_1279[1];
            int32_t *l_1807[10] = {&l_1724,&l_1740[1],&l_1724,&l_1724,&l_1740[1],&l_1724,&l_1740[1],&l_1724,&l_1724,&l_1740[1]};
            int64_t l_1808[1][9] = {{(-2L),0xD4701BA5CC499BB2LL,0xD4701BA5CC499BB2LL,(-2L),0xD4701BA5CC499BB2LL,0xD4701BA5CC499BB2LL,(-2L),0xD4701BA5CC499BB2LL,0xD4701BA5CC499BB2LL}};
            int i, j;
            for (i = 0; i < 2; i++)
                l_1740[i] = 0xE00AF8A1L;
            for (g_96 = 0; (g_96 <= 1); g_96 += 1)
            { /* block id: 889 */
                int32_t l_1716 = (-1L);
                uint8_t *l_1717 = &g_614;
                uint8_t *l_1727 = &g_1728;
                int32_t l_1735[9][1] = {{(-1L)},{1L},{(-1L)},{(-1L)},{1L},{(-1L)},{(-1L)},{1L},{(-1L)}};
                int i, j;
                if ((g_6[g_96] <= (((*l_1679) ^= (((safe_mul_func_uint8_t_u_u(((*l_1717) = l_1716), (((safe_lshift_func_int16_t_s_u(0L, 4)) == ((*l_1727) = ((((*g_75) = (l_1724 = (safe_sub_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u(l_1700, (g_100 > p_18))), 8L)))) <= ((((g_1725 = &l_1630) == ((*l_1705) = (*l_1707))) < p_19) & p_18)) , l_1726))) >= l_1716))) , (void*)0) == l_1729)) || (-5L))))
                { /* block id: 897 */
                    int32_t *l_1730[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1730[i] = &l_1434;
                    l_1742++;
                }
                else
                { /* block id: 899 */
                    uint64_t l_1776 = 0x9858F200D399A42ALL;
                    uint32_t *l_1787 = &g_381;
                    (*p_20) ^= (safe_add_func_int64_t_s_s((safe_mod_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((safe_unary_minus_func_int16_t_s(0L)), ((safe_rshift_func_int8_t_s_s((safe_add_func_int64_t_s_s(((*l_1305) = ((l_1756 > (((*l_1313) = (((safe_lshift_func_uint8_t_u_u(((~(g_6[0] , ((safe_lshift_func_int8_t_s_s(((*l_1473) = ((safe_unary_minus_func_int8_t_s((safe_rshift_func_int16_t_s_s((((*l_1717) ^= ((((*l_1679) = p_19) == ((safe_rshift_func_uint8_t_u_u((!((safe_div_func_uint16_t_u_u(l_1772, p_22)) ^ ((safe_div_func_uint64_t_u_u((p_22 && (((((void*)0 == l_1775) , (*g_943)) , &g_511) == &g_1584)), g_755.f0)) , 1L))), l_1776)) >= l_1735[8][0])) & (-1L))) == 0L), l_1738)))) != p_19)), (*g_75))) == (-1L)))) , 1UL), 0)) , g_1777) == (void*)0)) > (-1L))) , g_1728)), l_1776)), 2)) , 253UL))), g_1362[4])), p_18));
                    if ((safe_mod_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((((safe_sub_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(0xAF069DFA92AC60A6LL, l_1716)), (p_19 > ((***l_1365) = ((((*l_1787) = 0x95BAE680L) , (*g_881)) == (((*l_1473) = ((*l_1683) >= ((void*)0 == &g_1778[2][4][1]))) , (*g_881))))))) ^ 254UL) != 255UL), (*g_345))), p_22)))
                    { /* block id: 909 */
                        (*g_36) ^= l_1740[0];
                    }
                    else
                    { /* block id: 911 */
                        int32_t **l_1788 = &g_943;
                        union U0 ****l_1794 = &g_1725;
                        l_1788 = &g_943;
                        l_1683 = &p_19;
                        (*p_20) = ((4294967286UL > 8L) & ((safe_rshift_func_uint16_t_u_s(((void*)0 == l_1794), g_851)) < (l_1716 = g_894.f0)));
                        (*p_20) = (*p_20);
                    }
                    (*p_20) = (safe_lshift_func_int8_t_s_s((l_1738 = (safe_add_func_uint64_t_u_u((l_1799 == g_1801), ((*l_1305) = (safe_sub_func_uint16_t_u_u((((*l_1683) ^ (((((l_1716 &= (!p_21)) ^ (-1L)) > (p_21 || l_1776)) | l_1735[4][0]) && 0x02771ABF7285A991LL)) || (**g_1347)), p_22)))))), 7));
                    (*p_20) |= 0xBEEBC3D3L;
                }
                if ((*p_20))
                    break;
                return l_1473;
            }
            ++l_1809;
            return &g_76;
        }
        else
        { /* block id: 929 */
            int64_t l_1821 = 7L;
            int8_t *l_1833 = &l_1772;
            int8_t *l_1834[9] = {&g_729,&g_729,&g_729,&g_729,&g_729,&g_729,&g_729,&g_729,&g_729};
            int32_t l_1835 = 0L;
            int32_t l_1837 = 0xDF83CB91L;
            int32_t l_1838[10][10][2] = {{{0x90029471L,(-6L)},{(-9L),2L},{0xF5374BBCL,0xB2C0F3DBL},{0xD8F8C078L,1L},{0x959657E5L,1L},{1L,1L},{2L,4L},{3L,0x7EB9FB4DL},{1L,(-1L)},{1L,0L}},{{0x77D4B7B1L,0x3D3922BEL},{0x6FAA5D2AL,0xA823CEF7L},{1L,0xA823CEF7L},{0x6FAA5D2AL,0x3D3922BEL},{0x77D4B7B1L,0L},{1L,(-1L)},{1L,0x7EB9FB4DL},{3L,4L},{2L,1L},{1L,1L}},{{0x959657E5L,1L},{0xD8F8C078L,0xB2C0F3DBL},{0xF5374BBCL,2L},{(-9L),(-6L)},{0x90029471L,0xF5374BBCL},{0x7EB9FB4DL,0x2464E3EBL},{0L,0xDA4E0A93L},{0L,0L},{0xA2CAAC15L,(-4L)},{0xB67239FFL,0x77D4B7B1L}},{{0x233C0856L,0xD8F8C078L},{(-5L),0x233C0856L},{0xDA4E0A93L,2L},{0xDA4E0A93L,0x233C0856L},{(-5L),0xD8F8C078L},{0x233C0856L,0x77D4B7B1L},{0xB67239FFL,(-4L)},{0xA2CAAC15L,0L},{0L,0xDA4E0A93L},{0L,0x2464E3EBL}},{{0x7EB9FB4DL,0xF5374BBCL},{0x90029471L,2L},{0xD8F8C078L,0L},{0xB754B54FL,(-6L)},{1L,0x34FD98D9L},{(-10L),0x6FAA5D2AL},{1L,0x3D3922BEL},{0x233C0856L,1L},{0xA823CEF7L,9L},{0x34FD98D9L,(-9L)}},{{0xA2CAAC15L,(-5L)},{0x7EB9FB4DL,1L},{0xB67239FFL,1L},{0x6FAA5D2AL,1L},{0xB67239FFL,1L},{0x7EB9FB4DL,(-5L)},{0xA2CAAC15L,(-9L)},{0x34FD98D9L,9L},{0xA823CEF7L,1L},{0x233C0856L,0x3D3922BEL}},{{1L,0x6FAA5D2AL},{(-10L),0x34FD98D9L},{1L,(-6L)},{0xB754B54FL,0L},{0xD8F8C078L,2L},{0x1E5DB14AL,0xB754B54FL},{9L,0x77D4B7B1L},{(-5L),0L},{0x90029471L,0x90029471L},{(-4L),0xDE04519FL}},{{0x2464E3EBL,0x7EB9FB4DL},{(-1L),1L},{0xB2C0F3DBL,(-1L)},{0L,0x233C0856L},{0L,(-1L)},{0xB2C0F3DBL,1L},{(-1L),0x7EB9FB4DL},{0x2464E3EBL,0xDE04519FL},{(-4L),0x90029471L},{0x90029471L,0L}},{{(-5L),0x77D4B7B1L},{9L,0xB754B54FL},{0x1E5DB14AL,2L},{0xD8F8C078L,0L},{0xB754B54FL,(-6L)},{1L,0x34FD98D9L},{(-10L),0x6FAA5D2AL},{1L,0x3D3922BEL},{0x233C0856L,1L},{0xA823CEF7L,9L}},{{0x34FD98D9L,(-9L)},{0xA2CAAC15L,(-5L)},{0x7EB9FB4DL,1L},{0xB67239FFL,1L},{0x6FAA5D2AL,1L},{0xB67239FFL,1L},{0x7EB9FB4DL,(-5L)},{0xA2CAAC15L,(-9L)},{0x34FD98D9L,9L},{0xA823CEF7L,1L}}};
            int i, j, k;
            for (l_1633 = (-25); (l_1633 != 56); ++l_1633)
            { /* block id: 932 */
                const uint64_t l_1826[4][1] = {{0xB468654209DF982BLL},{0xB468654209DF982BLL},{0xB468654209DF982BLL},{0xB468654209DF982BLL}};
                int i, j;
                (*g_36) = (((*l_1683) |= (&g_1802[2] == ((((safe_div_func_uint32_t_u_u(((!(p_21 < ((*g_75) | (safe_mod_func_uint16_t_u_u(((*g_1584) != (l_1819 = &l_1731)), p_19))))) && (l_1821 > p_21)), (safe_div_func_uint16_t_u_u((--(*l_1680)), p_18)))) && (*g_71)) != l_1826[1][0]) , l_1827[0][2][0]))) | (*g_1691));
            }
            if ((((p_21 && 1L) && (*g_75)) | (18446744073709551608UL < g_1148.f0)))
            { /* block id: 938 */
                int32_t **l_1828 = &l_1683;
                (*l_1828) = &l_1315;
            }
            else
            { /* block id: 940 */
                int32_t *l_1836[6][5][4] = {{{(void*)0,&l_1724,(void*)0,&l_1734[0]},{&l_1732,&l_1434,&l_1309,&l_1835},{&g_37,&l_1732,&l_1724,(void*)0},{&l_1315,&l_1731,&l_1731,&l_1315},{(void*)0,&l_1435[2][0][3],&l_1434,&l_1724}},{{&l_1435[1][4][4],&g_37,(void*)0,&l_1433},{&l_1731,&g_799,&g_37,&l_1433},{&l_1731,&g_37,&g_1279[2],&l_1724},{(void*)0,&l_1435[2][0][3],&g_37,&l_1315},{&l_1309,&l_1731,(void*)0,(void*)0}},{{&l_1734[0],&l_1732,&l_1315,&l_1835},{(void*)0,&l_1434,(void*)0,&l_1734[0]},{&g_37,&l_1724,&g_1279[1],(void*)0},{&l_1433,&l_1435[0][0][2],&l_1835,&l_1435[0][0][2]},{&l_1731,&l_1734[0],&l_1435[0][0][2],&l_1309}},{{&l_1309,&l_1315,(void*)0,&g_37},{&g_37,&l_1733,&l_1435[2][0][3],&g_47[3][5][3]},{&g_37,&l_1435[1][2][2],&l_1724,(void*)0},{(void*)0,(void*)0,&l_1435[1][2][2],&l_1732},{&l_1733,&l_1724,&g_1279[2],&g_37}},{{&l_1309,&g_37,&l_1734[0],(void*)0},{&l_1433,&l_1734[0],&l_1309,&l_1733},{&g_37,&g_799,&l_1731,&l_1731},{&l_1835,(void*)0,(void*)0,(void*)0},{&g_37,&g_37,&l_1435[0][0][2],&l_1433}},{{(void*)0,&l_1734[0],&l_1433,(void*)0},{&g_1279[1],&g_47[3][5][3],(void*)0,&l_1433},{&l_1315,&g_47[3][5][3],(void*)0,(void*)0},{&g_47[3][5][3],&l_1734[0],&l_1734[0],&l_1433},{&l_1435[2][0][3],&g_37,&l_1315,(void*)0}}};
                int i, j, k;
                for (g_374 = 13; (g_374 < 26); g_374++)
                { /* block id: 943 */
                    for (g_72 = (-3); (g_72 != 45); g_72 = safe_add_func_int32_t_s_s(g_72, 4))
                    { /* block id: 946 */
                        (**l_1729) = (**g_272);
                        (*g_36) |= (*p_20);
                        return l_1834[3];
                    }
                }
                l_1839--;
            }
        }
    }
    else
    { /* block id: 955 */
        const int32_t *l_1848 = &l_1435[1][4][1];
        const uint64_t l_1862 = 0x032BC103A228F34CLL;
        int16_t l_1868 = 0x139FL;
        int32_t l_1914 = (-1L);
        int32_t l_1920 = (-8L);
        int32_t l_1923 = 0xC03E4F92L;
        int32_t l_1924 = 8L;
        int32_t l_1925 = 0xDD3B11A9L;
        int32_t l_1933 = (-9L);
        float l_1962 = 0x7.4F849Dp+3;
        int64_t **l_1974 = &g_1972[2][1];
        int32_t l_1980 = 3L;
        int32_t l_1981[10] = {0xED3B14D7L,0x665B840FL,(-1L),0x665B840FL,0xED3B14D7L,0xED3B14D7L,0x665B840FL,(-1L),0x665B840FL,0xED3B14D7L};
        int16_t **l_2109 = &l_1367;
        int64_t l_2115 = 0x085451369A452336LL;
        int8_t *l_2162 = (void*)0;
        union U0 *l_2188[7];
        int i;
        for (i = 0; i < 7; i++)
            l_2188[i] = &g_618;
        if ((((void*)0 != (*g_808)) || p_21))
        { /* block id: 956 */
            (*p_20) |= (((*l_1473) = (safe_lshift_func_uint8_t_u_s((safe_mod_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s((g_1362[0] | p_18), 5)), ((*l_1683) = 0x33L))), p_21))) && p_22);
        }
        else
        { /* block id: 960 */
            int64_t l_1858 = 0xA8AC5ADE034466F0LL;
            union U0 * const *l_1861 = &g_752[5];
            int32_t l_1863 = (-1L);
            int16_t *l_1865 = (void*)0;
            int16_t *l_1866 = &g_1867;
            int8_t *l_1892 = &g_241;
            int32_t l_1922 = 9L;
            int32_t l_1928 = 6L;
            int32_t l_1929 = (-4L);
            int32_t l_1931 = 0xA0D714E2L;
            uint32_t l_1934[9] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
            uint16_t *l_1941[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t ***l_1952 = &g_1338;
            int64_t l_1984[7][4][2] = {{{3L,(-3L)},{0xB7600BE5C6C1A5C4LL,0x56A54B29CA6E1509LL},{0x77DB06E72BE6747ELL,0L},{(-1L),0x77DB06E72BE6747ELL}},{{(-3L),8L},{(-3L),0x77DB06E72BE6747ELL},{(-1L),0L},{0x77DB06E72BE6747ELL,0x56A54B29CA6E1509LL}},{{0xB7600BE5C6C1A5C4LL,(-3L)},{3L,0L},{0L,3L},{(-3L),0xB7600BE5C6C1A5C4LL}},{{0x56A54B29CA6E1509LL,0x77DB06E72BE6747ELL},{0L,(-1L)},{0x77DB06E72BE6747ELL,(-3L)},{8L,(-3L)}},{{0x77DB06E72BE6747ELL,(-1L)},{0L,0x77DB06E72BE6747ELL},{0x56A54B29CA6E1509LL,0xB7600BE5C6C1A5C4LL},{(-3L),3L}},{{0L,0L},{3L,(-3L)},{0xB7600BE5C6C1A5C4LL,0x56A54B29CA6E1509LL},{0x77DB06E72BE6747ELL,0L}},{{(-1L),0x77DB06E72BE6747ELL},{(-3L),8L},{(-3L),0x77DB06E72BE6747ELL},{(-1L),0L}}};
            int32_t l_1988 = 0x73385A3DL;
            int32_t l_1990 = 1L;
            int32_t l_1992 = (-5L);
            uint64_t *l_2105 = &g_248;
            int64_t l_2158 = 6L;
            int8_t *l_2159 = &g_491;
            uint8_t ***l_2175 = &g_457;
            uint8_t ****l_2174 = &l_2175;
            int i, j, k;
            if ((((l_1848 != ((safe_mul_func_uint16_t_u_u((((*l_1866) = (safe_unary_minus_func_uint16_t_u((safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_u(((safe_div_func_int16_t_s_s(((l_1858 && ((*g_71) |= (((safe_sub_func_int64_t_s_s((0xE54500B7L && 0x245B5FF1L), (((void*)0 == l_1861) >= ((*l_1848) == l_1862)))) == (((**l_1366) = ((l_1863 |= (p_19 || p_18)) > p_21)) , (*g_75))) , p_19))) == g_381), 65530UL)) & l_1864), l_1858)), p_21))))) , p_19), l_1868)) , (void*)0)) < l_1858) , l_1858))
            { /* block id: 965 */
                int64_t l_1875 = 2L;
                uint16_t ***l_1888 = &g_399;
                int32_t l_1921 = 1L;
                int32_t l_1926 = 9L;
                int32_t l_1927 = 0xA0E48F11L;
                int32_t l_1932 = (-6L);
                uint32_t ***l_1950 = &g_1338;
                uint32_t ****l_1951 = &l_1950;
                int32_t l_1965 = 0xB74DF21FL;
                int32_t l_1985 = 0xD789BBC5L;
                int32_t l_1987[8][8] = {{0L,(-7L),0x939A917AL,2L,0L,0L,6L,0xFBCA1FD7L},{8L,1L,0x939A917AL,0L,0xFBCA1FD7L,7L,(-7L),0x085319FEL},{0L,0x9C2DBC53L,1L,0x74FF0598L,0x939A917AL,0x939A917AL,0x74FF0598L,1L},{0x13CD6430L,0x13CD6430L,0x085319FEL,(-7L),7L,0xFBCA1FD7L,0L,0x939A917AL},{0x085319FEL,0L,0xFBCA1FD7L,6L,0L,0L,2L,0x939A917AL},{0L,(-6L),0x74FF0598L,(-7L),0x134EE702L,0x70CE8312L,0xFBCA1FD7L,1L},{3L,1L,(-1L),0x74FF0598L,(-1L),1L,3L,0x085319FEL},{0x45C39DEBL,0L,0x74FF0598L,2L,0x085319FEL,0x939A917AL,(-6L),7L}};
                int16_t ***l_2011 = &l_1366;
                int8_t *l_2015 = &l_1864;
                int32_t *l_2106 = (void*)0;
                int i, j;
                for (g_374 = 5; (g_374 != 28); g_374++)
                { /* block id: 968 */
                    uint16_t ***l_1889 = (void*)0;
                    int32_t l_1891 = 1L;
                    int32_t l_1893[5][5] = {{1L,0xB0B67A02L,1L,1L,0xB0B67A02L},{9L,0x8E2B9DC8L,1L,0L,0L},{0x8E2B9DC8L,9L,0x8E2B9DC8L,1L,0L},{0xB0B67A02L,1L,0L,1L,0xB0B67A02L},{0x8E2B9DC8L,1L,9L,0xB0B67A02L,9L}};
                    int64_t l_1930 = 0x3A5EDA1124F5281DLL;
                    int i, j;
                    (*g_5) &= ((*p_20) = ((safe_mul_func_int8_t_s_s((safe_add_func_int64_t_s_s(g_1221, (((l_1875 == (safe_lshift_func_uint8_t_u_s((l_1893[0][3] |= (((safe_unary_minus_func_int64_t_s((safe_mul_func_uint8_t_u_u(0x9BL, (((safe_sub_func_int32_t_s_s(((safe_lshift_func_uint8_t_u_s(0xFDL, (safe_rshift_func_int16_t_s_u(((l_1887 != (l_1889 = l_1888)) || (p_22 | (l_1890 != (p_18 & 0x71FB1F856000C36DLL)))), p_22)))) , l_1891), p_18)) & l_1863) >= p_18))))) , l_1892) == &g_729)), (*g_75)))) || (*l_1819)) , p_22))), 0x53L)) | 0x4FL));
                    for (l_1864 = 9; (l_1864 >= 0); l_1864 -= 1)
                    { /* block id: 975 */
                        uint8_t *l_1902 = &g_1728;
                        union U0 *l_1908 = &g_753;
                        int32_t l_1913 = 1L;
                        int32_t *l_1915 = &l_1435[2][1][0];
                        int32_t *l_1916 = &l_1309;
                        int32_t *l_1917 = &g_47[3][5][3];
                        int32_t *l_1918 = &l_1315;
                        int32_t *l_1919[6][2][8] = {{{&l_1863,(void*)0,(void*)0,(void*)0,&l_1863,(void*)0,(void*)0,(void*)0},{&g_47[3][5][3],(void*)0,(void*)0,(void*)0,(void*)0,&g_47[3][5][3],&l_1433,(void*)0}},{{(void*)0,&l_1433,(void*)0,(void*)0,(void*)0,&l_1433,(void*)0,&g_47[3][5][3]},{(void*)0,&l_1863,(void*)0,(void*)0,(void*)0,(void*)0,&l_1863,(void*)0}},{{&g_47[3][5][3],(void*)0,&l_1433,(void*)0,(void*)0,(void*)0,&l_1433,(void*)0},{(void*)0,&l_1433,&g_47[3][5][3],(void*)0,(void*)0,(void*)0,(void*)0,&g_47[3][5][3]}},{{(void*)0,(void*)0,(void*)0,&l_1863,(void*)0,(void*)0,(void*)0,&l_1863},{&g_47[3][5][3],&l_1863,&g_47[3][5][3],(void*)0,&l_1863,&l_1433,&l_1433,&l_1863}},{{&l_1863,&l_1433,&l_1433,&l_1863,(void*)0,&g_47[3][5][3],&l_1863,&g_47[3][5][3]},{&l_1863,(void*)0,(void*)0,(void*)0,&l_1863,(void*)0,(void*)0,(void*)0}},{{&g_47[3][5][3],(void*)0,(void*)0,(void*)0,(void*)0,&g_47[3][5][3],&l_1433,(void*)0},{(void*)0,&l_1433,(void*)0,(void*)0,(void*)0,&l_1433,(void*)0,&g_47[3][5][3]}}};
                        const union U0 *l_1939 = &g_1411;
                        const union U0 **l_1938[1];
                        const union U0 ***l_1937 = &l_1938[0];
                        const union U0 ****l_1940 = &l_1937;
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                            l_1938[i] = &l_1939;
                        (*g_273) = ((safe_add_func_float_f_f((p_21 == ((safe_add_func_float_f_f(p_21, (((safe_mod_func_int16_t_s_s(((((*g_458)--) > ((*l_1902)++)) && (safe_mod_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u(((l_1908 != ((0xEAL & ((*l_1902) = (safe_add_func_uint8_t_u_u(p_21, 0x8CL)))) , (*l_1861))) , (((safe_add_func_uint32_t_u_u((4UL | (g_851 &= l_1913)), 0x33C1BF8FL)) ^ 0x50L) != 4L)))), l_1858))), l_1914)) & p_18) , 0x1.4EFC88p+59))) >= 0xA.7692B7p+58)), (**g_272))) == (*****g_1233));
                        l_1934[1]++;
                        (*l_1940) = l_1937;
                    }
                    if ((*p_20))
                        break;
                }
                if (((((((((*g_399) != l_1941[2]) > ((safe_mul_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s((*g_5), (safe_add_func_int8_t_s_s((((*l_1305) = (((void*)0 == g_1948) , (((((*l_1951) = l_1950) == (l_1952 = &g_1338)) & ((*l_1316) = (((void*)0 != p_20) ^ p_19))) < (-8L)))) != (*g_71)), (*g_458))))) & 4294967295UL), (*g_75))) , l_1928)) || p_22) <= l_1863) || (*g_1691)) <= (*l_1848)) > (-3L)))
                { /* block id: 990 */
                    int64_t l_1968 = 0xA839D7071A13B8F4LL;
                    int64_t ***l_1973 = &g_1971[4];
                    int32_t l_1976 = 0x631F745AL;
                    int32_t l_1978 = (-1L);
                    int32_t l_1982 = 0x6E35EABDL;
                    int32_t l_1986 = (-1L);
                    int32_t l_1989 = 0x0102135AL;
                    int32_t l_1991 = 0x9F1BD813L;
                    int8_t *l_2016[1][3];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_2016[i][j] = (void*)0;
                    }
                    if ((~(safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(p_22, (safe_sub_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((((*l_1848) <= (safe_add_func_uint32_t_u_u((**g_879), (l_1965 , 4294967287UL)))) | (((l_1312 = ((*l_1973) = (((safe_rshift_func_int16_t_s_u(((l_1968 != (((((+p_22) && 0xAA7219E4CE6F3100LL) && ((((safe_unary_minus_func_int16_t_s(p_19)) != 0xED8A6DB573A302C2LL) <= 0x7FL) ^ (*g_75))) , (*l_1848)) | 0x57E6L)) , g_1867), p_18)) || 0x66E0L) , g_1971[1]))) == l_1974) , p_21)), l_1968)), p_22)))), g_47[0][2][2]))))
                    { /* block id: 993 */
                        int16_t l_1975 = 0xEE4EL;
                        int32_t *l_1977[10] = {&g_799,(void*)0,&l_1695,&l_1695,(void*)0,&g_799,(void*)0,&l_1695,&l_1695,(void*)0};
                        int32_t l_1983[9];
                        uint16_t l_1993 = 5UL;
                        int32_t **l_1996 = &g_36;
                        int i;
                        for (i = 0; i < 9; i++)
                            l_1983[i] = 0L;
                        (*g_5) ^= 0x237CF195L;
                        ++l_1993;
                        (*g_5) = (*l_1848);
                        (*l_1996) = (void*)0;
                    }
                    else
                    { /* block id: 998 */
                        int32_t l_2010 = (-1L);
                        int32_t **l_2012 = (void*)0;
                        int32_t **l_2013 = (void*)0;
                        int32_t **l_2014 = &g_352;
                        (*l_2014) = (((+((safe_sub_func_float_f_f((!(((l_1932 = ((((((safe_mul_func_int16_t_s_s(((**l_1366) = ((0xAFL & (**g_457)) > 0x4EEBBBAFD01AE642LL)), (g_1867 = (safe_sub_func_int32_t_s_s((*p_20), ((~p_18) < 0x1B93L)))))) != (safe_add_func_uint32_t_u_u(((*l_1679)++), l_2010))) > (l_2011 != (void*)0)) & 1UL) <= (*g_75)) , 0x1.0p+1)) >= l_1965) < 0xD.F9F035p-28)), (*l_1848))) , 0xD.1C4266p+12)) >= 0x0.0p+1) , (void*)0);
                        return l_2016[0][1];
                    }
                }
                else
                { /* block id: 1006 */
                    int64_t l_2043 = (-2L);
                    for (l_1980 = 2; (l_1980 < (-17)); l_1980 = safe_sub_func_uint8_t_u_u(l_1980, 2))
                    { /* block id: 1009 */
                        int64_t ***l_2020 = &g_1971[1];
                        int64_t ****l_2019 = &l_2020;
                        const int64_t *l_2023 = &l_1858;
                        const int64_t **l_2022 = &l_2023;
                        const int64_t ***l_2021 = &l_2022;
                        int32_t l_2038 = 0x39F5AFF8L;
                        (*l_1683) = (p_22 | (((*l_2019) = (void*)0) == l_2021));
                        (*g_5) ^= ((safe_div_func_uint64_t_u_u((((((*l_2015) = ((safe_lshift_func_int16_t_s_s(l_1929, 7)) <= (*g_75))) > ((0x5587L > (safe_div_func_int64_t_s_s(((*g_71) & (safe_rshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u(((safe_sub_func_uint64_t_u_u(((((*l_1848) != l_2038) , ((*l_1473) |= ((*l_1892) = ((((safe_div_func_float_f_f((safe_div_func_float_f_f((l_2038 , p_18), 0x7.6DE54Ep-51)), 0x3.403EFEp+68)) != l_2043) <= l_1926) , (*l_1848))))) || (*l_1848)), 0UL)) , p_21), p_21)) || (*g_71)), 11)), 1))), p_19))) , p_22)) == 0x0C75L) == (*l_1683)), (*g_71))) < l_2044);
                    }
                }
                if ((*l_1848))
                { /* block id: 1018 */
                    int32_t **l_2045 = (void*)0;
                    int32_t **l_2046 = &g_36;
                    (*l_2046) = (void*)0;
                }
                else
                { /* block id: 1020 */
                    for (l_1932 = 0; (l_1932 <= 3); l_1932 += 1)
                    { /* block id: 1023 */
                        uint32_t *****l_2048 = (void*)0;
                        l_1923 |= ((*l_1683) = ((*l_1819) , ((void*)0 == (*g_344))));
                        l_1922 = (!((*l_1848) != ((g_2049 = &l_1952) == &g_881)));
                    }
                    return &g_241;
                }
                for (g_1867 = 0; (g_1867 <= 15); g_1867 = safe_add_func_int8_t_s_s(g_1867, 9))
                { /* block id: 1033 */
                    uint8_t l_2055 = 255UL;
                    uint8_t l_2080 = 0x7AL;
                    int32_t l_2081 = 0xFA308360L;
                    uint8_t *l_2082 = (void*)0;
                    uint8_t *l_2083 = &g_1728;
                    uint64_t *l_2084 = &l_1408[5];
                    uint8_t *l_2104 = &g_614;
                    if (l_1988)
                        break;
                    (*l_1683) = (safe_mod_func_uint64_t_u_u((*g_71), ((*l_1306) = (((((p_22 , l_2055) , (safe_mod_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s(0x5F07L, (safe_rshift_func_uint16_t_u_u(1UL, 8)))), (safe_mod_func_int8_t_s_s((safe_lshift_func_int16_t_s_s(((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(((*l_2083) = ((((5L <= 2L) ^ (l_1863 |= ((+((((*g_458) &= ((l_2081 &= (safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((p_22 | (((!0xB24CBB0DL) <= (*g_5)) <= l_2080)), l_1932)), p_21)), g_1540))) | 0x8359L)) , p_21) < p_18)) | (*l_1848)))) > 0x13F227B0L) != 250UL)), (*g_75))), p_21)) && p_22), 15)), p_22)))), p_21))) , 0xDE4E6F38L) && (*p_20)) || (*g_1691)))));
                    (*g_513) = &p_19;
                    return l_1892;
                }
            }
            else
            { /* block id: 1051 */
                int16_t l_2126[6] = {0L,(-6L),(-6L),0L,(-6L),(-6L)};
                uint8_t l_2127 = 9UL;
                uint8_t * const * const l_2130 = &g_458;
                int8_t *l_2139 = &g_491;
                int i;
                (*g_36) |= ((safe_mul_func_uint16_t_u_u((g_1244 == ((*l_1365) = l_2109)), (((void*)0 != (**l_1952)) || (*l_1848)))) > ((*l_2105) = p_22));
                l_1923 = ((((safe_lshift_func_uint8_t_u_u(((**l_1861) , (((safe_div_func_float_f_f((l_1981[8] = (g_2114 , p_19)), l_2115)) , (((*l_1367) = ((safe_mod_func_int64_t_s_s(((*l_1306) &= ((p_19 , (safe_lshift_func_int16_t_s_s((safe_div_func_uint8_t_u_u(((((*p_20) = (safe_sub_func_uint8_t_u_u((((*l_1866) |= l_2126[2]) != (((((*l_1848) , ((l_2127 < (safe_sub_func_int64_t_s_s(0x79D323D61921C39ALL, 1L))) ^ (*l_1848))) != (*p_20)) , p_21) != p_18)), (*g_75)))) < (*l_1848)) < l_2126[2]), (*l_1848))), 10))) , 0L)), (*l_1819))) > (*g_71))) != p_19)) || 1L)), 2)) <= (*g_458)) , (void*)0) == l_2130);
                for (g_1221 = 0; (g_1221 >= (-24)); --g_1221)
                { /* block id: 1063 */
                    int8_t l_2135 = 0xE5L;
                    for (l_1863 = 0; (l_1863 <= (-29)); --l_1863)
                    { /* block id: 1066 */
                        if ((*p_20))
                            break;
                    }
                    if (l_2135)
                        break;
                    for (l_1992 = 0; (l_1992 == 0); ++l_1992)
                    { /* block id: 1072 */
                        int32_t **l_2138[8] = {&g_36,&g_36,&g_36,&g_36,&g_36,&g_36,&g_36,&g_36};
                        int i;
                        l_1683 = &p_19;
                        return l_2139;
                    }
                }
            }
            if ((((**g_942) = (*l_1848)) , ((*l_1683) , ((((((safe_unary_minus_func_uint64_t_u((*g_71))) >= p_19) , (***g_1489)) == (void*)0) == (((safe_rshift_func_int8_t_s_s(l_1928, 0)) , ((*l_1848) != ((((safe_lshift_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(l_1984[1][2][1], l_1922)), 15)) ^ 9UL) != l_1922) != l_1988))) , (*g_71))) & (*l_1848)))))
            { /* block id: 1079 */
                int8_t *l_2151 = &g_491;
                int32_t *l_2157 = &g_47[3][5][3];
                uint32_t l_2165 = 0x9CA61AC3L;
                int16_t *** const l_2184 = &l_2109;
                for (l_1648 = (-20); (l_1648 > 32); l_1648++)
                { /* block id: 1082 */
                    return l_2151;
                }
                for (g_1867 = (-5); (g_1867 != (-28)); g_1867 = safe_sub_func_int64_t_s_s(g_1867, 1))
                { /* block id: 1087 */
                    int8_t **l_2160 = &l_2159;
                    int32_t **l_2161 = &g_36;
                    for (l_1864 = 0; (l_1864 > (-16)); --l_1864)
                    { /* block id: 1090 */
                        int32_t **l_2156 = &l_1683;
                        (*l_2156) = &p_19;
                        return &g_729;
                    }
                    p_20 = &p_19;
                    for (l_1890 = (-10); (l_1890 < 34); ++l_1890)
                    { /* block id: 1099 */
                        l_2165--;
                        if ((*p_20))
                            break;
                    }
                }
                p_20 = ((safe_div_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((((void*)0 != l_2174) != (((((((g_2178[2] , (((**l_1366) = ((safe_mul_func_int8_t_s_s((-1L), l_1992)) <= (((safe_unary_minus_func_uint8_t_u((p_18 |= (safe_rshift_func_int8_t_s_u(((((*l_2157) = (4UL && ((void*)0 != l_2184))) | l_2185[1][8][1]) && l_1929), (**g_457)))))) , (*l_1848)) || p_21))) , 65535UL)) == p_22) , 2L) == (*g_75)) & 0UL) , 0x80031AA08ECB302ALL) == p_19)) > g_1362[0]), (*l_1848))), 0x11L)) , &p_19);
            }
            else
            { /* block id: 1108 */
                float ****l_2187 = &l_1402[1];
                (*l_1683) = (l_2186 != ((((*l_1679) = p_18) , p_19) , l_2187));
                (*l_1630) = ((**g_1725) = l_2188[0]);
            }
        }
    }
    (*g_5) = (safe_mod_func_int8_t_s_s(((*l_1473) |= (*g_75)), 0xB8L));
    return &g_729;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int16_t  func_30(int32_t * p_31, const int8_t  p_32, int64_t  p_33, uint16_t  p_34, int32_t * p_35)
{ /* block id: 677 */
    int8_t l_1282 = 0x80L;
    int32_t l_1283 = 0x294A960AL;
    int32_t *l_1284 = (void*)0;
    int32_t *l_1285 = &l_1283;
    int32_t l_1286 = 0x3A546BE5L;
    int32_t *l_1287 = &g_47[3][5][3];
    int32_t *l_1288 = (void*)0;
    int32_t *l_1289 = &l_1283;
    int32_t *l_1290 = &g_799;
    int32_t *l_1291 = (void*)0;
    int32_t *l_1292 = &g_47[3][5][3];
    int32_t *l_1293 = &l_1286;
    int32_t *l_1294 = &g_47[5][8][0];
    int32_t *l_1295[9][9] = {{(void*)0,&g_799,&g_1279[1],&g_1279[0],&g_1279[1],&g_799,(void*)0,&g_37,&g_37},{&g_1279[0],&l_1283,&g_47[3][5][3],(void*)0,&g_1279[1],&g_47[3][5][3],&l_1283,&g_1279[1],&l_1283},{&g_1279[1],&g_1279[1],&g_37,&g_37,&g_1279[1],&g_1279[1],&g_37,&g_37,(void*)0},{&g_1279[1],&l_1283,&g_47[3][5][3],&g_47[3][5][3],&g_1279[0],(void*)0,&g_1279[1],&g_1279[1],(void*)0},{&g_37,&g_1279[0],&g_1279[0],&g_1279[1],&g_799,(void*)0,&g_37,&g_37,&g_1279[1]},{(void*)0,&g_1279[1],&g_47[3][5][3],&l_1283,&g_1279[0],&g_1279[2],&g_1279[1],&g_1279[2],&g_1279[0]},{&g_37,&l_1283,&l_1283,&g_37,&g_799,(void*)0,&g_1279[0],&g_1279[0],&g_1279[2]},{&g_1279[0],&l_1283,&g_47[3][5][3],&g_47[3][5][3],&g_47[3][5][3],&l_1283,&l_1283,&g_47[3][5][3],&g_47[3][5][3]},{(void*)0,&l_1286,(void*)0,&g_1279[1],&g_799,&g_1279[0],&g_1279[1],&g_37,(void*)0}};
    uint32_t l_1296 = 18446744073709551615UL;
    int i, j;
    --l_1296;
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads : g_50 g_71 g_75 g_76 g_47 g_96 g_99 g_72 g_100 g_36 g_37 g_49 g_5 g_6 g_140 g_248 g_241 g_272 g_99.f0 g_103 g_252 g_376 g_616 g_455 g_346 g_374 g_352 g_273 g_274 g_458 g_755 g_767 g_511 g_799
 * writes: g_50 g_96 g_100 g_76 g_103 g_37 g_72 g_6 g_49 g_140 g_241 g_248 g_272 g_278 g_47 g_616 g_374 g_352 g_729 g_732 g_274 g_752 g_799
 */
static uint32_t  func_42(int32_t  p_43)
{ /* block id: 4 */
    uint64_t *l_48[3];
    int32_t *l_73 = (void*)0;
    int32_t **l_74 = &l_73;
    int8_t *l_77[8][5] = {{&g_76,&g_76,&g_76,&g_76,&g_76},{(void*)0,&g_76,(void*)0,(void*)0,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,(void*)0,(void*)0,&g_76,(void*)0},{&g_76,&g_76,&g_76,&g_76,&g_76},{(void*)0,&g_76,(void*)0,(void*)0,&g_76}};
    uint8_t *l_94 = (void*)0;
    uint8_t *l_95 = &g_96;
    const int32_t l_101[4] = {0x29FA49BFL,0x29FA49BFL,0x29FA49BFL,0x29FA49BFL};
    uint16_t *l_102[1];
    int32_t l_104 = 0x7B5DC454L;
    float l_639 = 0xD.D54132p+6;
    int8_t *l_683[2][1][7];
    uint32_t l_684 = 2UL;
    int32_t *l_798 = &g_799;
    uint32_t l_810 = 0UL;
    int32_t l_968 = 0x111338B6L;
    int32_t l_969 = 0x5502A877L;
    int32_t l_970 = 0L;
    int32_t l_971 = 0xE69D1081L;
    int32_t l_975 = 1L;
    int32_t l_985 = 0x4B176969L;
    int32_t l_986 = 6L;
    int32_t l_987 = (-1L);
    int32_t l_990 = 0L;
    int32_t l_991 = (-1L);
    int32_t l_993[6] = {0x1D429655L,0x1D429655L,0x1D429655L,0x1D429655L,0x1D429655L,0x1D429655L};
    int8_t l_994 = 1L;
    int64_t l_995 = 1L;
    uint32_t l_996 = 0x832685A5L;
    uint64_t l_1008 = 0xDC549CE65E96C1EBLL;
    uint32_t *l_1047 = &g_100;
    uint32_t **l_1046 = &l_1047;
    uint32_t ***l_1045 = &l_1046;
    uint32_t l_1081 = 18446744073709551615UL;
    union U0 *l_1106 = (void*)0;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_48[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_102[i] = &g_103;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 7; k++)
                l_683[i][j][k] = &g_76;
        }
    }
    (*l_798) &= (safe_add_func_uint64_t_u_u((g_50++), (func_53(l_48[0], ((safe_sub_func_uint8_t_u_u(p_43, (safe_sub_func_uint8_t_u_u(((l_683[1][0][4] = func_60(func_66(g_71, ((*l_74) = l_73), (l_77[5][4] = g_75), (safe_div_func_uint32_t_u_u((((safe_mul_func_uint16_t_u_u((g_103 = (l_104 ^= (safe_sub_func_int8_t_s_s(((*g_75) = (((safe_rshift_func_int16_t_s_u((safe_sub_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((*g_75), (-1L))), (g_47[4][1][0] <= ((safe_unary_minus_func_uint32_t_u((~((safe_lshift_func_int16_t_s_s((((*l_95)--) && (g_100 = ((*l_95) = (g_99 , p_43)))), 0)) && 0xD4L)))) > p_43)))), l_101[2])) || 0x0BL) > (*g_75))), p_43)))), 0x89C3L)) & 0x43F1L) <= p_43), 4294967292UL))), p_43, p_43, l_101[2], g_376)) != l_94), 0x06L)))) , l_684)) & p_43)));
    for (l_684 = 26; (l_684 <= 51); ++l_684)
    { /* block id: 366 */
        uint8_t l_802 = 255UL;
        uint8_t **l_805 = &l_95;
        int32_t l_813 = 0x7938127DL;
        int32_t l_815 = 0x2F4C5D9FL;
        int32_t l_816 = 1L;
        int32_t *** const l_837 = (void*)0;
        int32_t l_869 = 1L;
        int8_t *l_930 = (void*)0;
        int8_t **l_931 = &g_75;
        int16_t *l_932[9][2][9] = {{{&g_851,(void*)0,&g_851,&g_851,&g_851,&g_851,(void*)0,(void*)0,&g_851},{(void*)0,&g_851,&g_851,&g_851,(void*)0,&g_851,(void*)0,&g_851,(void*)0}},{{&g_851,(void*)0,(void*)0,&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0},{&g_851,&g_851,&g_851,(void*)0,(void*)0,&g_851,&g_851,&g_851,&g_851}},{{&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0},{&g_851,&g_851,&g_851,&g_851,(void*)0,&g_851,(void*)0,&g_851,(void*)0}},{{&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0,&g_851,&g_851},{&g_851,&g_851,&g_851,(void*)0,(void*)0,&g_851,&g_851,&g_851,&g_851}},{{(void*)0,&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0,&g_851,&g_851},{&g_851,&g_851,&g_851,(void*)0,&g_851,&g_851,(void*)0,&g_851,&g_851}},{{&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851},{(void*)0,&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0,(void*)0,(void*)0}},{{&g_851,&g_851,(void*)0,&g_851,(void*)0,&g_851,(void*)0,&g_851,&g_851},{&g_851,&g_851,&g_851,&g_851,(void*)0,(void*)0,&g_851,&g_851,&g_851}},{{(void*)0,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851,&g_851},{&g_851,(void*)0,&g_851,&g_851,&g_851,&g_851,&g_851,(void*)0,(void*)0}},{{&g_851,&g_851,(void*)0,&g_851,&g_851,(void*)0,&g_851,&g_851,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_851,&g_851,&g_851,(void*)0,&g_851}}};
        int32_t l_972 = 9L;
        int32_t l_973 = 2L;
        int32_t l_974 = 6L;
        int32_t l_976 = 0L;
        int32_t l_977 = 0L;
        int32_t l_978 = 0xD4BEAF3AL;
        int32_t l_979 = 4L;
        int32_t l_980 = 0x1798C7E8L;
        int32_t l_981 = 6L;
        int32_t l_982 = 1L;
        int32_t l_983 = 0xD9093816L;
        int32_t l_984 = (-1L);
        int32_t l_988 = 0x0D2086C8L;
        int32_t l_989 = 0x33BD060AL;
        int32_t l_992[5][1][1] = {{{0x32E8FE68L}},{{0x4F48451EL}},{{0x32E8FE68L}},{{0x4F48451EL}},{{0x32E8FE68L}}};
        uint32_t *l_1026 = &l_810;
        uint32_t ** const l_1025 = &l_1026;
        uint32_t ** const *l_1024 = &l_1025;
        int64_t *l_1031 = &g_49;
        const uint16_t *l_1035 = &g_387;
        const uint16_t * const *l_1034[2][2][6] = {{{&l_1035,&l_1035,&l_1035,&l_1035,&l_1035,&l_1035},{&l_1035,&l_1035,&l_1035,&l_1035,&l_1035,&l_1035}},{{&l_1035,&l_1035,&l_1035,&l_1035,&l_1035,&l_1035},{&l_1035,&l_1035,&l_1035,&l_1035,&l_1035,&l_1035}}};
        uint16_t **l_1036 = &l_102[0];
        uint32_t l_1056 = 0x48E4D3FDL;
        union U0 *l_1084 = &g_99;
        int8_t l_1273 = 0xD3L;
        int i, j, k;
    }
    return p_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_76 g_100 g_616 g_99.f0 g_75 g_71 g_72 g_36 g_37 g_49 g_96 g_374 g_5 g_6 g_140 g_248 g_241 g_272 g_103 g_99 g_252 g_352 g_273 g_274 g_458 g_755 g_346 g_767 g_511
 * writes: g_47 g_616 g_76 g_37 g_374 g_6 g_96 g_49 g_140 g_241 g_72 g_248 g_103 g_272 g_278 g_100 g_352 g_729 g_732 g_274 g_752
 */
static int32_t  func_53(uint64_t * const  p_54, int64_t  p_55)
{ /* block id: 289 */
    uint64_t *l_685 = &g_374;
    int32_t *l_686 = &g_47[3][2][0];
    int32_t l_687[3];
    uint32_t *l_694 = &g_616;
    int8_t **l_697 = (void*)0;
    int32_t **l_698 = (void*)0;
    int32_t **l_699 = &g_352;
    int32_t l_734 = 0xCB76E912L;
    union U0 *l_751 = &g_99;
    int8_t l_777 = 1L;
    int32_t l_783 = 0x29461E15L;
    int64_t l_784 = 0xED552C0BE492ECD5LL;
    int i;
    for (i = 0; i < 3; i++)
        l_687[i] = (-7L);
    (*l_699) = func_66(l_685, l_686, &g_76, ((*g_75) = ((p_55 ^ ((*l_686) = (*l_686))) | (l_687[2] , (safe_mul_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u(g_76, ((*l_694) &= g_100))) != ((safe_rshift_func_int16_t_s_u(p_55, 15)) <= g_99.f0)), l_687[1])) >= 8UL) , &g_75) == l_697), 0xA9L))))));
    for (g_72 = 6; (g_72 != 45); g_72++)
    { /* block id: 296 */
        float *l_721[10][4][6] = {{{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0}},{{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0}},{{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0}},{{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0},{&g_274,(void*)0,(void*)0,&g_274,(void*)0,(void*)0}},{{&g_274,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
        float **l_720 = &l_721[6][2][1];
        float *l_722 = &g_274;
        float *l_730 = (void*)0;
        float *l_731 = &g_732;
        int32_t l_733[8][4] = {{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L},{0xF72BACEDL,0xF72BACEDL,2L,2L}};
        int32_t *l_774 = &g_47[3][5][3];
        int32_t *l_782[1];
        uint8_t l_785 = 0xF9L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_782[i] = &g_37;
        if ((safe_sub_func_int32_t_s_s((safe_unary_minus_func_int64_t_s((p_55 = (safe_lshift_func_uint8_t_u_s(((safe_mod_func_int32_t_s_s((((safe_mul_func_int16_t_s_s((safe_add_func_int32_t_s_s((((((((**l_699) < ((**g_272) = ((-(*g_273)) == ((safe_div_func_float_f_f((safe_div_func_float_f_f((safe_add_func_float_f_f((((*l_720) = l_686) != (l_722 = l_722)), (((**l_699) != 0xC.B782ADp+82) == (*g_273)))), ((safe_div_func_float_f_f(p_55, ((*l_731) = (safe_div_func_float_f_f((g_729 = ((++(*l_685)) , (**l_699))), p_55))))) == (**g_272)))), 0xD.4B6416p-60)) < (*g_273))))) > l_733[1][2]) <= 0x1.1p+1) == (-0x1.6p+1)) , p_55) && l_733[2][3]), (**l_699))), 0x866CL)) < (*g_458)) | p_55), g_248)) | l_734), (*g_75)))))), (*l_686))))
        { /* block id: 304 */
            int8_t l_737 = 0x15L;
            float ***l_738 = &g_278;
            (*l_731) = (((*l_722) = 0x0.3p+1) < ((p_55 != ((safe_mod_func_uint8_t_u_u(((*g_458) |= (p_55 && l_737)), l_737)) , (((void*)0 != l_738) <= (safe_add_func_float_f_f((*l_686), (safe_add_func_float_f_f((safe_mul_func_float_f_f(((safe_div_func_int64_t_s_s(p_55, 0x1EB0EADBE76D1DCCLL)) , l_737), 0x2.A0B54Dp+90)), 0x4.0FE10Ap+15))))))) >= 0x0.A3F6D2p+98));
        }
        else
        { /* block id: 308 */
            uint32_t *l_760 = &g_100;
            int32_t l_766 = 0x5300E790L;
            uint8_t l_779 = 0xBAL;
            for (g_37 = (-1); (g_37 >= 28); g_37 = safe_add_func_int64_t_s_s(g_37, 2))
            { /* block id: 311 */
                int32_t l_768 = (-8L);
                for (g_100 = 0; (g_100 >= 12); ++g_100)
                { /* block id: 314 */
                    uint32_t l_754 = 0xD2CE971AL;
                    g_752[0] = l_751;
                    for (g_374 = 0; (g_374 <= 5); g_374 += 1)
                    { /* block id: 318 */
                        if (l_754)
                            break;
                        (*g_5) ^= p_55;
                    }
                    l_768 = (g_755 , (((safe_mod_func_int32_t_s_s(((((*l_694) = l_733[1][2]) | (((safe_sub_func_uint64_t_u_u(((void*)0 != l_760), ((((+(safe_mul_func_uint16_t_u_u(((g_140 ^ p_55) && ((void*)0 != &g_71)), (safe_lshift_func_int8_t_s_s(l_766, 3))))) < (*g_75)) == 0UL) , g_346))) || 0x9CL) ^ 0x682357CFL)) , p_55), p_55)) , g_767) , 0x1.9p+1));
                    if ((**l_699))
                        break;
                }
            }
            for (l_766 = 0; (l_766 <= 5); l_766 += 1)
            { /* block id: 329 */
                int32_t l_773 = 0xB342D12EL;
                int32_t *l_775 = &l_733[1][2];
                int32_t *l_776[9][9][3] = {{{&g_37,&l_766,&l_733[0][1]},{&l_733[1][2],&l_733[1][2],&l_733[1][2]},{&g_37,&l_766,(void*)0},{&g_47[3][5][3],&l_773,&l_766},{&l_733[1][2],&l_733[1][2],&g_47[3][5][3]},{&l_687[1],&l_773,&l_733[2][2]},{&l_687[2],(void*)0,(void*)0},{&l_733[1][2],&l_766,&l_733[6][2]},{&l_733[1][2],(void*)0,&l_733[1][2]}},{{&l_687[0],&l_687[1],&l_687[1]},{&l_687[1],&l_687[2],&l_687[1]},{(void*)0,&l_687[2],(void*)0},{(void*)0,&l_733[1][2],&l_773},{&l_687[2],&l_773,&l_733[1][2]},{&g_47[0][8][0],(void*)0,&l_687[1]},{&l_733[1][2],(void*)0,&l_733[1][2]},{&g_47[3][5][3],&l_773,(void*)0},{(void*)0,&l_733[1][2],(void*)0}},{{(void*)0,&l_687[2],&l_766},{&l_733[1][2],&l_687[2],&l_733[1][2]},{(void*)0,&l_687[1],&l_733[0][1]},{&l_766,(void*)0,(void*)0},{&l_733[7][2],&l_766,(void*)0},{(void*)0,(void*)0,&l_733[1][2]},{&g_47[3][5][3],&l_773,(void*)0},{(void*)0,(void*)0,&l_733[1][2]},{&l_773,(void*)0,&l_773}},{{&l_766,&g_37,&g_37},{&l_687[2],(void*)0,(void*)0},{(void*)0,&l_766,&g_37},{&l_733[1][2],&g_47[3][5][3],&l_773},{&l_687[2],(void*)0,&l_733[1][2]},{&l_733[3][1],&l_687[2],(void*)0},{&g_37,&l_687[2],&l_733[1][2]},{(void*)0,&l_687[0],(void*)0},{&l_733[6][2],&g_47[3][5][3],(void*)0}},{{&l_687[2],&l_733[4][0],&l_733[0][1]},{(void*)0,&g_47[3][5][3],&l_733[1][2]},{&g_37,&l_733[1][2],&l_766},{&g_47[3][7][2],&l_687[2],(void*)0},{&l_733[1][2],(void*)0,(void*)0},{&l_766,(void*)0,&l_733[1][2]},{(void*)0,&l_733[1][2],&l_687[1]},{(void*)0,&g_47[3][7][2],&l_733[1][2]},{&l_766,(void*)0,&l_773}},{{&l_733[1][2],&l_766,(void*)0},{&g_47[3][7][2],&l_733[1][2],&l_687[1]},{&g_37,&g_37,&l_687[1]},{(void*)0,&l_733[1][2],&l_733[1][2]},{&l_687[2],(void*)0,&l_733[6][2]},{&l_733[6][2],&l_687[2],(void*)0},{(void*)0,&l_773,&l_733[2][2]},{&g_37,(void*)0,(void*)0},{&l_733[3][1],&l_766,&l_766}},{{&l_687[2],(void*)0,&l_687[2]},{&l_733[1][2],&g_47[3][5][3],&g_47[3][7][2]},{(void*)0,&l_733[1][2],&l_733[6][3]},{&l_687[2],&g_47[3][5][3],(void*)0},{&l_766,(void*)0,(void*)0},{&l_773,&l_766,&l_687[0]},{(void*)0,(void*)0,(void*)0},{&g_47[3][5][3],&l_773,&l_687[2]},{(void*)0,&l_687[2],&l_687[1]}},{{&l_733[7][2],(void*)0,&g_47[3][5][3]},{&l_766,&l_733[1][2],&l_733[3][2]},{(void*)0,&g_37,&g_37},{&l_733[1][2],&l_733[1][2],(void*)0},{(void*)0,&l_766,(void*)0},{(void*)0,(void*)0,&l_733[1][2]},{&l_733[0][1],(void*)0,&l_687[1]},{&g_47[0][8][0],&g_47[3][7][2],&l_687[1]},{&g_47[3][5][3],(void*)0,&l_733[1][2]}},{{(void*)0,(void*)0,(void*)0},{&l_733[1][2],&l_687[1],&l_733[1][2]},{&g_47[3][5][3],(void*)0,&l_687[2]},{&l_733[1][2],&l_733[0][1],&l_733[1][2]},{(void*)0,&l_687[2],(void*)0},{&l_733[1][2],(void*)0,&g_37},{&g_37,(void*)0,(void*)0},{&l_687[1],(void*)0,&g_37},{(void*)0,&l_687[1],(void*)0}}};
                int i, j, k;
                if ((p_55 ^ (safe_mod_func_int8_t_s_s((safe_add_func_int8_t_s_s(0L, l_773)), p_55))))
                { /* block id: 330 */
                    l_774 = (*g_511);
                    return (*l_774);
                }
                else
                { /* block id: 333 */
                    if ((*l_774))
                        break;
                }
                --l_779;
            }
        }
        l_785++;
    }
    for (g_72 = 0; (g_72 >= 35); ++g_72)
    { /* block id: 343 */
        uint16_t l_794 = 0xAF20L;
        int32_t *l_797 = &l_687[2];
        (*l_699) = (*l_699);
        for (p_55 = 0; (p_55 >= 23); p_55++)
        { /* block id: 347 */
            for (l_777 = 0; (l_777 >= 1); l_777++)
            { /* block id: 350 */
                if (p_55)
                    break;
                for (g_616 = 0; (g_616 <= 3); g_616 += 1)
                { /* block id: 354 */
                    int i, j, k;
                    return g_47[(g_616 + 3)][(g_616 + 5)][g_616];
                }
            }
            ++l_794;
            (*l_699) = l_797;
        }
    }
    return p_55;
}


/* ------------------------------------------ */
/* 
 * reads : g_616 g_455 g_252 g_75 g_76 g_36 g_37 g_140 g_346 g_374 g_71 g_72 g_352 g_47 g_273 g_274
 * writes: g_616 g_103 g_37 g_47
 */
static int8_t * func_60(int32_t * p_61, int16_t  p_62, const uint8_t  p_63, float  p_64, int64_t  p_65)
{ /* block id: 272 */
    float l_646 = 0x1.3p-1;
    int32_t l_647 = (-1L);
    int8_t *l_659 = &g_241;
    uint32_t *l_675 = &g_100;
    int32_t l_678 = 0x92F29CEAL;
    const uint8_t *l_682 = (void*)0;
    const uint8_t **l_681 = &l_682;
    if (((safe_lshift_func_int16_t_s_s(0xDE44L, 6)) == g_616))
    { /* block id: 273 */
        uint32_t *l_654 = &g_616;
        uint16_t *l_657 = &g_103;
        int32_t l_658 = 2L;
        (*g_36) ^= ((safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_s((l_658 = (l_647 < (((g_616 != 5L) | 65533UL) && ((safe_mul_func_int16_t_s_s((((safe_add_func_uint32_t_u_u(p_62, (safe_rshift_func_int8_t_s_s(p_63, 1)))) ^ ((g_455[2] , ((*l_654)++)) < ((((p_62 <= (((*l_657) = (p_65 ^ 65535UL)) == 0x6FB8L)) , l_658) & 0xBA5DL) , 1L))) , g_252), p_62)) && 0L)))), 13)), (*g_75))) || 0x1D4A5D54L);
        return l_659;
    }
    else
    { /* block id: 279 */
        const int8_t l_664 = 0xE9L;
        uint16_t *l_676 = &g_103;
        int32_t *l_677[9] = {&g_454[5],&g_454[5],&g_454[5],&g_454[5],&g_454[5],&g_454[5],&g_454[5],&g_454[5],&g_454[5]};
        int i;
        (*g_352) &= ((l_678 ^= (safe_sub_func_uint16_t_u_u(((((g_140 || (safe_div_func_uint32_t_u_u(l_664, (safe_rshift_func_uint8_t_u_u((g_346 , (((l_647 = ((safe_mul_func_int8_t_s_s((l_664 >= p_65), (safe_lshift_func_int16_t_s_s(((p_63 , l_647) || (((*l_676) = (safe_rshift_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(((((p_61 == l_675) | l_664) || l_647) ^ g_346), 18446744073709551612UL)), 7))) ^ l_647)), 8)))) , p_63)) , g_374) , p_62)), 2))))) >= (*g_36)) >= l_664) | (*g_71)), p_63))) && p_62);
    }
    p_64 = (safe_mul_func_float_f_f((l_678 > ((l_659 != ((*l_681) = &p_63)) , l_647)), (*g_273)));
    return l_659;
}


/* ------------------------------------------ */
/* 
 * reads : g_71 g_72 g_100 g_36 g_37 g_49 g_47 g_75 g_76 g_96 g_5 g_6 g_140 g_248 g_241 g_272 g_99.f0 g_103 g_99 g_252 g_374 g_491 g_1279
 * writes: g_37 g_72 g_6 g_96 g_49 g_140 g_241 g_248 g_103 g_272 g_278 g_100 g_47 g_76 g_374 g_252 g_1279
 */
static int32_t * func_66(uint64_t * p_67, int32_t * p_68, int8_t * p_69, int8_t  p_70)
{ /* block id: 14 */
    float l_108 = 0xA.869343p+8;
    int32_t l_109 = (-8L);
    uint8_t *l_114[7] = {&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96};
    uint8_t **l_113 = &l_114[3];
    uint8_t *l_116[1];
    uint8_t **l_115 = &l_116[0];
    int8_t *l_130[6];
    int8_t **l_129 = &l_130[5];
    int32_t **l_131 = &g_36;
    int32_t l_135 = 0L;
    int32_t l_136 = (-10L);
    int32_t l_137 = (-1L);
    int32_t l_138 = 0xB09B1B02L;
    int32_t l_139[1];
    const uint64_t *l_189 = &g_72;
    uint64_t * const l_190 = &g_72;
    int32_t *l_229 = &l_137;
    uint64_t l_300 = 0x8D9BF796855F1F0ALL;
    uint16_t *l_516[9][5][5] = {{{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,&g_103,(void*)0,&g_103,(void*)0},{(void*)0,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103}},{{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,&g_103}},{{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,(void*)0,&g_103},{&g_103,(void*)0,&g_103,&g_103,(void*)0},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103}},{{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,(void*)0},{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,&g_103},{&g_103,(void*)0,(void*)0,&g_103,&g_103}},{{&g_103,&g_103,(void*)0,(void*)0,(void*)0},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103}},{{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,(void*)0},{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,(void*)0,&g_103,(void*)0,(void*)0}},{{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,(void*)0,(void*)0},{(void*)0,&g_103,&g_103,&g_103,&g_103},{(void*)0,&g_103,&g_103,(void*)0,&g_103},{(void*)0,&g_103,&g_103,&g_103,&g_103}},{{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_103,&g_103},{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,(void*)0,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103}},{{(void*)0,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103},{&g_103,&g_103,&g_103,&g_103,&g_103}}};
    int32_t l_575 = (-7L);
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_116[i] = (void*)0;
    for (i = 0; i < 6; i++)
        l_130[i] = &g_76;
    for (i = 0; i < 1; i++)
        l_139[i] = 0xB4FB0B04L;
    (*g_36) |= (65535UL || (safe_lshift_func_int16_t_s_u((((0x6BAB3D6DL || (safe_unary_minus_func_int8_t_s(l_109))) != (safe_mul_func_int16_t_s_s(0xEF9FL, ((((~(8UL & p_70)) , ((((*l_113) = &g_96) != ((*l_115) = &g_96)) > (*g_71))) && (*g_71)) , p_70)))) >= (*g_71)), g_100)));
lbl_132:
    (*g_5) ^= (safe_div_func_int16_t_s_s(g_49, ((safe_add_func_uint8_t_u_u(l_109, (safe_rshift_func_int8_t_s_u((l_109 ^ ((*p_67) &= ((((safe_add_func_float_f_f(((safe_div_func_float_f_f(((g_47[3][5][3] >= (((safe_rshift_func_int16_t_s_u(0x9BD5L, 12)) , &p_70) == ((*g_75) , &p_70))) == ((((*l_129) = l_114[3]) != &p_70) >= p_70)), 0x6.D4BD0Dp+91)) == l_109), 0x2.9D2F47p-3)) , g_96) , l_131) == (void*)0))), g_47[3][5][3])))) | (*g_75))));
    for (g_37 = 0; (g_37 <= 6); g_37 += 1)
    { /* block id: 23 */
        if (g_100)
            goto lbl_132;
    }
    for (p_70 = 0; (p_70 <= 1); p_70 += 1)
    { /* block id: 28 */
        int32_t *l_133 = &l_109;
        int32_t *l_134[3][9] = {{&g_37,&g_37,&g_37,&g_37,(void*)0,&g_37,&g_37,&g_37,&g_37},{(void*)0,&g_37,&g_47[6][3][1],&g_37,(void*)0,(void*)0,&g_37,&g_47[6][3][1],&g_37},{&g_37,(void*)0,&g_47[6][3][1],&g_47[6][3][1],(void*)0,&g_37,(void*)0,&g_47[6][3][1],&g_47[6][3][1]}};
        uint8_t l_141 = 0xCDL;
        int16_t l_162[1];
        float *l_165 = (void*)0;
        float *l_166 = &l_108;
        int8_t *l_181 = &g_76;
        const int8_t l_184 = (-1L);
        int16_t l_203 = 0xD3EFL;
        int16_t l_239 = (-7L);
        uint64_t *l_251 = &g_252;
        uint8_t l_297 = 0x4BL;
        int16_t l_353 = 4L;
        uint16_t *l_397 = (void*)0;
        uint16_t **l_396[1];
        uint16_t **l_401 = &l_397;
        int16_t l_443 = (-1L);
        uint16_t l_518 = 0x2C6BL;
        uint64_t l_522 = 18446744073709551615UL;
        int32_t *l_549[6][10][2] = {{{&g_47[3][5][3],&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&l_109}},{{&l_109,&g_47[3][5][3]},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]}},{{&l_109,&l_109},{&g_47[3][5][3],&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&g_47[3][5][3]},{&l_109,&l_109}},{{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&g_47[3][5][3],&l_109},{&l_109,&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&g_47[3][5][3]},{&l_109,&l_109},{&g_47[3][5][3],&l_109},{&g_47[3][5][3],(void*)0},{(void*)0,&g_47[3][5][3]}},{{&l_109,&l_139[0]},{&l_109,&g_47[3][5][3]},{(void*)0,(void*)0},{&g_47[3][5][3],&l_109},{&l_139[0],&l_109},{&g_47[3][5][3],(void*)0},{(void*)0,&g_47[3][5][3]},{&l_109,&l_139[0]},{&l_109,&g_47[3][5][3]},{(void*)0,(void*)0}},{{&g_47[3][5][3],&l_109},{&l_139[0],&l_109},{&g_47[3][5][3],(void*)0},{(void*)0,&g_47[3][5][3]},{&l_109,&l_139[0]},{&l_109,&g_47[3][5][3]},{(void*)0,(void*)0},{&g_47[3][5][3],&l_109},{&l_139[0],&l_109},{&g_47[3][5][3],(void*)0}}};
        uint16_t l_585 = 0xACD6L;
        int32_t **l_590 = &g_352;
        uint64_t l_615 = 18446744073709551615UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_162[i] = 5L;
        for (i = 0; i < 1; i++)
            l_396[i] = &l_397;
        ++l_141;
        if (g_6[p_70])
            continue;
        if (((*g_36) = ((*l_133) = (safe_sub_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((((p_70 , (safe_sub_func_int8_t_s_s((((((safe_mod_func_int8_t_s_s((safe_div_func_uint8_t_u_u((++g_96), (((safe_add_func_float_f_f(((safe_sub_func_float_f_f(g_76, l_162[0])) == p_70), (((((*l_166) = ((**l_131) == (((*p_67) >= (255UL != (safe_sub_func_uint16_t_u_u(1UL, p_70)))) , 0x7.B66246p-29))) , (**l_131)) < p_70) > 0x1.D526E1p-90))) > g_76) , (*g_75)))), p_70)) ^ g_100) | (**l_131)) | p_70) , 0xF1L), (*g_75)))) , 0L) <= (*g_75)) , g_47[1][6][2]), 1L)), (*l_133))) && p_70), g_47[3][5][3])))))
        { /* block id: 35 */
            int32_t l_167 = 1L;
            int64_t l_182[8] = {0x19DD48334AB61E80LL,0x19DD48334AB61E80LL,0x19DD48334AB61E80LL,0x19DD48334AB61E80LL,0x19DD48334AB61E80LL,0x19DD48334AB61E80LL,0x19DD48334AB61E80LL,0x19DD48334AB61E80LL};
            int32_t l_183 = 5L;
            int32_t l_227 = (-6L);
            int i;
            (*l_133) ^= (l_167 >= (((1L != (**l_131)) & ((safe_lshift_func_int16_t_s_s(p_70, 1)) , (safe_add_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u((safe_div_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((safe_unary_minus_func_int16_t_s((((l_167 & ((((*l_129) = l_181) == &g_76) , (((*g_75) == (l_182[5] , (*g_75))) || l_183))) < p_70) | (*g_71)))) , p_70), 0xDEEFCC7DL)), l_184)), 0xD7L)), (*g_71))), l_183)))) == 0x21L));
            for (l_183 = 0; (l_183 <= 3); l_183 += 1)
            { /* block id: 40 */
                uint8_t l_192 = 253UL;
                int32_t **l_208 = (void*)0;
                for (l_137 = 3; (l_137 >= 0); l_137 -= 1)
                { /* block id: 43 */
                    int32_t *l_228 = &l_227;
                    int i, j, k;
                    if (((safe_mod_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(g_47[(p_70 + 5)][(p_70 + 7)][l_183], l_182[(l_183 + 1)])), (-1L))) != (l_189 == l_190)))
                    { /* block id: 44 */
                        int32_t l_191[1];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                            l_191[i] = 0x222FE821L;
                        if ((*g_36))
                            break;
                        l_192++;
                    }
                    else
                    { /* block id: 47 */
                        int64_t *l_206 = (void*)0;
                        int64_t *l_207 = &g_49;
                        int32_t l_211 = 0x0B913E92L;
                        l_211 = (g_6[0] , ((safe_div_func_float_f_f(((((safe_mul_func_float_f_f((safe_add_func_float_f_f((**l_131), (safe_div_func_float_f_f(l_203, g_37)))), ((((*l_207) = ((safe_lshift_func_int8_t_s_u(0xE5L, 5)) < g_76)) , ((*l_166) = ((((((void*)0 != l_208) <= (safe_mul_func_uint16_t_u_u(g_49, g_100))) , (*g_75)) | l_182[5]) , p_70))) <= l_182[2]))) <= 0x8.2p-1) > p_70) > l_182[(l_183 + 1)]), p_70)) != (-0x6.Fp-1)));
                        if (g_47[(p_70 + 5)][(p_70 + 7)][l_183])
                            continue;
                    }
                    for (g_96 = 0; (g_96 != 53); g_96++)
                    { /* block id: 55 */
                        int16_t *l_218 = &l_162[0];
                        int16_t *l_226 = &l_203;
                        (*g_5) |= (safe_rshift_func_uint16_t_u_u(((((*l_218) = g_96) <= ((l_182[5] ^ p_70) > ((((l_227 = (safe_unary_minus_func_uint16_t_u((safe_add_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((0x06BD755EL >= (safe_rshift_func_uint8_t_u_s((&l_184 != (void*)0), 4))), ((*l_226) ^= ((((*g_36) = p_70) <= 4294967295UL) < (0x7830L && 0xFA38L))))), 0xBAL))))) || (**l_131)) != 0x9CL) && (*p_67)))) | 0xF70DL), 10));
                        p_68 = l_228;
                    }
                }
                return &g_47[0][4][2];
            }
        }
        else
        { /* block id: 66 */
            uint16_t l_290 = 0x5FA4L;
            int32_t l_293 = (-4L);
            int32_t l_294 = 4L;
            int32_t l_295[8][5][6] = {{{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L}},{{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L}},{{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L}},{{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L}},{{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L}},{{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L}},{{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L}},{{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L},{0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L,0x4130F57FL,0x126A5E22L},{(-3L),0x126A5E22L,(-3L),0x126A5E22L,(-3L),0x126A5E22L}}};
            int16_t l_296 = (-1L);
            int i, j, k;
            for (g_140 = 5; (g_140 >= 0); g_140 -= 1)
            { /* block id: 69 */
                const int16_t l_233 = 0x935BL;
                int64_t *l_238 = &g_49;
                uint32_t *l_240[2][1];
                int32_t l_292 = 0L;
                int i, j;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_240[i][j] = (void*)0;
                }
                (*l_133) ^= (((g_241 = (safe_add_func_int64_t_s_s((safe_unary_minus_func_uint32_t_u(l_233)), (+(((*g_71) >= ((*l_238) |= (safe_rshift_func_int16_t_s_u(((~((*p_67) , l_233)) || ((*g_36) = 2L)), 15)))) , (l_239 >= ((*g_5) > ((*l_229) = ((void*)0 != &l_162[0]))))))))) ^ (*p_67)) , (*g_5));
                for (g_96 = 0; (g_96 <= 3); g_96 += 1)
                { /* block id: 77 */
                    uint64_t **l_253[3];
                    uint64_t *l_254 = &g_252;
                    float **l_255 = &l_166;
                    uint16_t *l_267 = &g_103;
                    float * const **l_275 = &g_272;
                    float * const *l_277 = (void*)0;
                    float * const **l_276 = &l_277;
                    uint16_t *l_291 = &l_290;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_253[i] = &g_71;
                    (*l_133) |= (g_47[p_70][(g_140 + 3)][p_70] != (safe_rshift_func_int16_t_s_s(g_47[(g_96 + 3)][(g_96 + 3)][(p_70 + 1)], (((g_248 |= ((*l_190)--)) , (*g_75)) >= (((((((safe_rshift_func_uint16_t_u_u(((*l_267) = ((((l_254 = l_251) == (p_70 , p_67)) && ((((*l_255) = g_36) != (void*)0) | (g_241 ^= (safe_lshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_s((safe_div_func_int8_t_s_s((~(safe_sub_func_int64_t_s_s(((safe_add_func_int16_t_s_s(0x7E96L, g_47[(g_96 + 3)][(g_96 + 3)][(p_70 + 1)])) > g_6[1]), 0xAA63995447FFEF46LL))), 0xF8L)), 1)), 3))))) ^ 0x92FDL)), (**l_131))) , 0x0.Fp+1) , 0x61524D6DL) | g_100) , 1L) <= g_49) <= 0xDEF5F233L)))));
                    l_292 ^= (((((*l_291) = (((safe_div_func_int16_t_s_s(g_96, (safe_sub_func_int64_t_s_s(0x43D2243094950BB0LL, (((*l_276) = ((*l_275) = g_272)) != (g_278 = (void*)0)))))) & (safe_add_func_uint32_t_u_u((((((safe_rshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(((!(p_70 && ((*l_267) = (((*l_229) ^ (g_100 = (safe_mul_func_int16_t_s_s(((l_251 != (void*)0) == (0xC931L != (**l_131))), g_47[(p_70 + 5)][(g_96 + 2)][g_96])))) ^ (*l_133))))) | (*g_75)), (-1L))), p_70)), 8)) > g_99.f0) , 0UL) != (*p_69)) && g_6[0]), l_290))) == 0x36292349L)) == g_241) | 0UL) ^ g_72);
                    if ((*g_5))
                        break;
                }
            }
            l_297--;
            l_300--;
            (**l_131) &= (*g_5);
        }
        for (l_136 = 3; (l_136 >= 0); l_136 -= 1)
        { /* block id: 101 */
            (**l_131) &= (0x7ABAL || p_70);
            for (l_138 = 3; (l_138 >= 0); l_138 -= 1)
            { /* block id: 105 */
                uint16_t *l_303[7] = {&g_103,&g_103,&g_103,&g_103,&g_103,&g_103,&g_103};
                uint32_t *l_316 = &g_100;
                int i, j, k;
                g_47[(p_70 + 3)][(p_70 + 3)][l_136] = (((--g_103) != g_47[(p_70 + 1)][(l_136 + 1)][l_138]) , (0x00B1F0B624E792C0LL ^ ((*g_71) ^= (safe_sub_func_int32_t_s_s(g_47[(p_70 + 2)][(l_136 + 2)][(p_70 + 1)], ((*l_316) = ((safe_div_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(0xBAL, ((g_99 , p_70) < (safe_mod_func_uint16_t_u_u(((safe_div_func_uint16_t_u_u((g_140 == (((0x8CB4L || (((0xFFL == g_252) >= 9UL) || (-8L))) , p_70) , p_70)), 0x3AEFL)) == 0x2C4D3541L), (**l_131)))))), 1L)) != (*l_229))))))));
            }
        }
        for (g_76 = 3; (g_76 >= 0); g_76 -= 1)
        { /* block id: 114 */
            int64_t *l_325[10] = {&g_49,&g_49,&g_49,&g_49,&g_49,&g_49,&g_49,&g_49,&g_49,&g_49};
            int32_t l_326 = (-10L);
            uint8_t **l_375 = &l_116[0];
            float * const ** const l_377 = &g_272;
            int32_t l_378 = 2L;
            int32_t l_379 = 0xDE4E45C5L;
            uint16_t ***l_398 = &l_396[0];
            int32_t *l_517[10];
            int8_t l_584 = 5L;
            float * const **l_607 = (void*)0;
            int i;
            for (i = 0; i < 10; i++)
                l_517[i] = &l_135;
        }
    }
    return p_68;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_6[i], "g_6[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_37, "g_37", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_47[i][j][k], "g_47[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_49, "g_49", print_hash_value);
    transparent_crc(g_50, "g_50", print_hash_value);
    transparent_crc(g_72, "g_72", print_hash_value);
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    transparent_crc(g_99.f0, "g_99.f0", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_140, "g_140", print_hash_value);
    transparent_crc(g_241, "g_241", print_hash_value);
    transparent_crc(g_248, "g_248", print_hash_value);
    transparent_crc(g_252, "g_252", print_hash_value);
    transparent_crc_bytes (&g_274, sizeof(g_274), "g_274", print_hash_value);
    transparent_crc(g_346, "g_346", print_hash_value);
    transparent_crc(g_374, "g_374", print_hash_value);
    transparent_crc(g_376, "g_376", print_hash_value);
    transparent_crc(g_381, "g_381", print_hash_value);
    transparent_crc(g_384.f0, "g_384.f0", print_hash_value);
    transparent_crc(g_387, "g_387", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_454[i], "g_454[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_455[i].f0, "g_455[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_491, "g_491", print_hash_value);
    transparent_crc(g_614, "g_614", print_hash_value);
    transparent_crc(g_616, "g_616", print_hash_value);
    transparent_crc(g_618.f0, "g_618.f0", print_hash_value);
    transparent_crc(g_729, "g_729", print_hash_value);
    transparent_crc_bytes (&g_732, sizeof(g_732), "g_732", print_hash_value);
    transparent_crc(g_753.f0, "g_753.f0", print_hash_value);
    transparent_crc(g_755.f0, "g_755.f0", print_hash_value);
    transparent_crc(g_767.f0, "g_767.f0", print_hash_value);
    transparent_crc_bytes (&g_778, sizeof(g_778), "g_778", print_hash_value);
    transparent_crc(g_799, "g_799", print_hash_value);
    transparent_crc(g_851, "g_851", print_hash_value);
    transparent_crc(g_894.f0, "g_894.f0", print_hash_value);
    transparent_crc(g_913, "g_913", print_hash_value);
    transparent_crc(g_935.f0, "g_935.f0", print_hash_value);
    transparent_crc(g_965, "g_965", print_hash_value);
    transparent_crc(g_1148.f0, "g_1148.f0", print_hash_value);
    transparent_crc_bytes (&g_1190, sizeof(g_1190), "g_1190", print_hash_value);
    transparent_crc(g_1221, "g_1221", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1279[i], "g_1279[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1314, "g_1314", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1362[i], "g_1362[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1380[i][j][k].f0, "g_1380[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1411.f0, "g_1411.f0", print_hash_value);
    transparent_crc(g_1520, "g_1520", print_hash_value);
    transparent_crc(g_1540, "g_1540", print_hash_value);
    transparent_crc(g_1546.f0, "g_1546.f0", print_hash_value);
    transparent_crc(g_1728, "g_1728", print_hash_value);
    transparent_crc(g_1820, "g_1820", print_hash_value);
    transparent_crc(g_1867, "g_1867", print_hash_value);
    transparent_crc(g_2114.f0, "g_2114.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2178[i].f0, "g_2178[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2225.f0, "g_2225.f0", print_hash_value);
    transparent_crc(g_2248, "g_2248", print_hash_value);
    transparent_crc(g_2427, "g_2427", print_hash_value);
    transparent_crc(g_2518, "g_2518", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_2539[i][j][k].f0, "g_2539[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_2652, sizeof(g_2652), "g_2652", print_hash_value);
    transparent_crc(g_2702.f0, "g_2702.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2751[i], "g_2751[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2794, "g_2794", print_hash_value);
    transparent_crc(g_3041.f0, "g_3041.f0", print_hash_value);
    transparent_crc(g_3084.f0, "g_3084.f0", print_hash_value);
    transparent_crc(g_3145.f0, "g_3145.f0", print_hash_value);
    transparent_crc(g_3210.f0, "g_3210.f0", print_hash_value);
    transparent_crc(g_3320.f0, "g_3320.f0", print_hash_value);
    transparent_crc(g_3385.f0, "g_3385.f0", print_hash_value);
    transparent_crc(g_3392, "g_3392", print_hash_value);
    transparent_crc(g_3404.f0, "g_3404.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_3580[i][j][k].f0, "g_3580[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_3590[i].f0, "g_3590[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3624, "g_3624", print_hash_value);
    transparent_crc(g_3684, "g_3684", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 993
XXX total union variables: 26

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 57
breakdown:
   indirect level: 0, occurrence: 26
   indirect level: 1, occurrence: 12
   indirect level: 2, occurrence: 5
   indirect level: 3, occurrence: 5
   indirect level: 4, occurrence: 3
   indirect level: 5, occurrence: 6
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 14
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 34
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 10

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 401
   depth: 2, occurrence: 105
   depth: 3, occurrence: 9
   depth: 4, occurrence: 9
   depth: 5, occurrence: 6
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 8, occurrence: 3
   depth: 9, occurrence: 2
   depth: 11, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 4
   depth: 16, occurrence: 5
   depth: 17, occurrence: 3
   depth: 18, occurrence: 4
   depth: 20, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 7
   depth: 23, occurrence: 7
   depth: 24, occurrence: 8
   depth: 25, occurrence: 9
   depth: 27, occurrence: 3
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 3
   depth: 31, occurrence: 2
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1
   depth: 35, occurrence: 4
   depth: 36, occurrence: 2
   depth: 37, occurrence: 1
   depth: 38, occurrence: 2
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 43, occurrence: 1
   depth: 44, occurrence: 2
   depth: 45, occurrence: 1
   depth: 49, occurrence: 1

XXX total number of pointers: 791

XXX times a variable address is taken: 2159
XXX times a pointer is dereferenced on RHS: 545
breakdown:
   depth: 1, occurrence: 427
   depth: 2, occurrence: 91
   depth: 3, occurrence: 5
   depth: 4, occurrence: 11
   depth: 5, occurrence: 11
XXX times a pointer is dereferenced on LHS: 528
breakdown:
   depth: 1, occurrence: 459
   depth: 2, occurrence: 60
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 69
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 22
XXX times a pointer is qualified to be dereferenced: 15448

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 4398
   level: 2, occurrence: 925
   level: 3, occurrence: 130
   level: 4, occurrence: 93
   level: 5, occurrence: 38
XXX number of pointers point to pointers: 380
XXX number of pointers point to scalars: 396
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 27.1
XXX average alias set size: 1.48

XXX times a non-volatile is read: 3105
XXX times a non-volatile is write: 1521
XXX times a volatile is read: 218
XXX    times read thru a pointer: 111
XXX times a volatile is write: 83
XXX    times written thru a pointer: 49
XXX times a volatile is available for access: 1.16e+04
XXX percentage of non-volatile access: 93.9

XXX forward jumps: 0
XXX backward jumps: 8

XXX stmts: 423
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 39
   depth: 2, occurrence: 52
   depth: 3, occurrence: 73
   depth: 4, occurrence: 104
   depth: 5, occurrence: 120

XXX percentage a fresh-made variable is used: 15.7
XXX percentage an existing variable is used: 84.3
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


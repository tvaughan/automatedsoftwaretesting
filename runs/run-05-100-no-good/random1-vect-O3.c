/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1222298897
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile uint32_t g_2 = 0x7CA0029BL;/* VOLATILE GLOBAL g_2 */
static int32_t g_4 = 0x438BEA12L;
static int32_t g_7 = 0xBAFCB143L;
static volatile int32_t g_10 = 8L;/* VOLATILE GLOBAL g_10 */
static int32_t g_11 = (-2L);
static int32_t g_29 = (-1L);
static int32_t *g_28 = &g_29;
static uint32_t g_41 = 0xBAB1F356L;
static volatile uint8_t g_78[3] = {0x1BL,0x1BL,0x1BL};
static float g_91[10] = {0x0.2BA9A6p-57,0x3.5p-1,0x0.2BA9A6p-57,0x3.5p-1,0x0.2BA9A6p-57,0x3.5p-1,0x0.2BA9A6p-57,0x3.5p-1,0x0.2BA9A6p-57,0x3.5p-1};
static float g_93 = (-0x5.1p-1);
static float *g_97 = &g_91[3];
static int32_t *g_101 = &g_7;
static int32_t ** volatile g_100 = &g_101;/* VOLATILE GLOBAL g_100 */
static uint32_t g_125 = 0xFCAC0D74L;
static const int32_t g_132 = (-1L);
static float g_135[6][1][5] = {{{0x4.Cp-1,0x2.32BF5Cp-15,0x5.B29D1Cp-41,0x0.9p+1,0x0.9p+1}},{{0x1.9p+1,0x2.32BF5Cp-15,0x1.9p+1,0xB.D0260Cp-7,0x0.9p+1}},{{0x4.Cp-1,0x7.Ep-1,0x1.9p+1,0x0.9p+1,0xB.D0260Cp-7}},{{0x4.Cp-1,0x2.32BF5Cp-15,0x5.B29D1Cp-41,0x0.9p+1,0x0.9p+1}},{{0x1.9p+1,0x2.32BF5Cp-15,0x1.9p+1,0xB.D0260Cp-7,0x0.9p+1}},{{0x4.Cp-1,0x7.Ep-1,0x1.9p+1,0x0.9p+1,0xB.D0260Cp-7}}};
static uint16_t g_146 = 0xAAAAL;
static int32_t g_163 = 0x79BCA128L;
static int8_t g_174[10] = {0x5CL,0x5CL,0x5CL,0x5CL,0x5CL,0x5CL,0x5CL,0x5CL,0x5CL,0x5CL};
static int32_t **g_180 = (void*)0;
static int64_t g_183[6] = {(-1L),0L,0L,(-1L),0L,0L};
static uint32_t g_197 = 4UL;
static int8_t *g_227 = &g_174[7];
static uint16_t g_249[4] = {0x7E1DL,0x7E1DL,0x7E1DL,0x7E1DL};
static int8_t g_253 = 0x46L;
static int32_t * volatile g_297 = &g_163;/* VOLATILE GLOBAL g_297 */
static int32_t ** volatile g_299 = (void*)0;/* VOLATILE GLOBAL g_299 */
static uint64_t g_305 = 0xB77CFFBB7CE84866LL;
static uint8_t g_311 = 4UL;
static int32_t ** volatile g_335 = (void*)0;/* VOLATILE GLOBAL g_335 */
static int32_t ** const *g_369 = (void*)0;
static int32_t ** const **g_368[3][8][6] = {{{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,(void*)0,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,(void*)0},{&g_369,&g_369,&g_369,(void*)0,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369}},{{(void*)0,&g_369,&g_369,&g_369,(void*)0,(void*)0},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{(void*)0,(void*)0,&g_369,&g_369,&g_369,&g_369},{&g_369,(void*)0,&g_369,(void*)0,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,(void*)0,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369}},{{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,(void*)0},{&g_369,&g_369,&g_369,(void*)0,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{(void*)0,&g_369,&g_369,&g_369,(void*)0,(void*)0},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369},{&g_369,&g_369,&g_369,&g_369,&g_369,&g_369}}};
static int32_t ** const *** volatile g_367[4] = {&g_368[1][2][1],&g_368[1][2][1],&g_368[1][2][1],&g_368[1][2][1]};
static int32_t ** const *** volatile g_370[9] = {&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3],&g_368[0][0][3]};
static int32_t ** const *** volatile g_371 = &g_368[0][3][0];/* VOLATILE GLOBAL g_371 */
static int32_t ** volatile g_375 = &g_101;/* VOLATILE GLOBAL g_375 */
static volatile int32_t g_414 = 0x38F1172DL;/* VOLATILE GLOBAL g_414 */
static float * const  volatile * volatile g_424 = &g_97;/* VOLATILE GLOBAL g_424 */
static float * const  volatile * volatile *g_423 = &g_424;
static int32_t g_428 = 0L;
static int32_t * volatile g_427 = &g_428;/* VOLATILE GLOBAL g_427 */
static int64_t g_465 = (-5L);
static int16_t g_470 = 1L;
static int32_t * const *g_487[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t * const ** volatile g_486 = &g_487[1];/* VOLATILE GLOBAL g_486 */
static uint8_t g_667 = 1UL;
static uint32_t g_679 = 1UL;
static volatile int32_t *g_850 = &g_414;
static volatile int32_t * volatile *g_849 = &g_850;
static volatile int32_t * volatile ** volatile g_848 = &g_849;/* VOLATILE GLOBAL g_848 */
static float g_856 = 0x0.Dp-1;
static const int32_t *g_859 = &g_11;
static const int32_t ** volatile g_858 = &g_859;/* VOLATILE GLOBAL g_858 */
static volatile float g_997[2] = {0x4.89E1F2p+65,0x4.89E1F2p+65};
static volatile uint8_t **g_1021 = (void*)0;
static volatile uint32_t *g_1054 = (void*)0;
static volatile uint32_t * const *g_1053 = &g_1054;
static const uint64_t g_1095[3] = {0x1DD601BC59505B26LL,0x1DD601BC59505B26LL,0x1DD601BC59505B26LL};
static const uint64_t *g_1094[10][10][2] = {{{(void*)0,(void*)0},{(void*)0,&g_1095[0]},{&g_1095[1],&g_1095[0]},{&g_1095[0],&g_1095[0]},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]}},{{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{(void*)0,&g_1095[1]},{&g_1095[0],&g_1095[0]},{&g_1095[0],&g_1095[1]},{(void*)0,(void*)0},{&g_1095[1],(void*)0},{&g_1095[0],(void*)0}}};
static const uint32_t g_1164[4] = {0x72928455L,0x72928455L,0x72928455L,0x72928455L};
static const int8_t g_1180 = 0x80L;
static uint8_t g_1216[1][10][10] = {{{0x5DL,0x6CL,3UL,0x6CL,0x5DL,253UL,253UL,0x5DL,0x6CL,3UL},{0x9BL,0x9BL,3UL,0x5DL,0xFCL,0x5DL,3UL,0x9BL,0x9BL,3UL},{0x6CL,0x5DL,253UL,253UL,0x5DL,0x6CL,3UL,0x6CL,0x5DL,253UL},{0x8AL,0x9BL,0x8AL,253UL,3UL,3UL,253UL,0x8AL,0x9BL,0x8AL},{0x8AL,0x6CL,0x9BL,0x5DL,0x9BL,0x6CL,0x8AL,0x8AL,0x6CL,0x9BL},{0x6CL,0x8AL,0x8AL,0x6CL,0x9BL,0x5DL,0x9BL,0x6CL,0x8AL,0x8AL},{0x9BL,0x8AL,253UL,3UL,3UL,253UL,0x8AL,0x9BL,0x8AL,253UL},{0x5DL,0x6CL,3UL,0x6CL,0x5DL,253UL,253UL,0x5DL,0x6CL,3UL},{0x9BL,0x9BL,3UL,0x5DL,0xFCL,0x5DL,3UL,0x9BL,0x9BL,3UL},{0x6CL,0x5DL,253UL,253UL,0x5DL,0x6CL,3UL,0x6CL,0x5DL,253UL}}};
static int8_t **g_1237 = &g_227;
static int8_t ** const  volatile * volatile g_1236 = &g_1237;/* VOLATILE GLOBAL g_1236 */
static float * const ***g_1240 = (void*)0;
static float * const **** volatile g_1239[8] = {&g_1240,&g_1240,&g_1240,&g_1240,&g_1240,&g_1240,&g_1240,&g_1240};
static float * const **** volatile g_1241 = (void*)0;/* VOLATILE GLOBAL g_1241 */
static float * const **** const  volatile g_1242 = &g_1240;/* VOLATILE GLOBAL g_1242 */
static const volatile int32_t * const g_1265[6] = {&g_10,&g_10,&g_10,&g_10,&g_10,&g_10};
static const volatile int32_t ** volatile g_1266 = (void*)0;/* VOLATILE GLOBAL g_1266 */
static uint64_t g_1290[7] = {0UL,0UL,18446744073709551615UL,0UL,0UL,18446744073709551615UL,0UL};
static int8_t *** volatile g_1313 = &g_1237;/* VOLATILE GLOBAL g_1313 */
static const uint32_t *g_1380 = &g_41;
static const uint32_t **g_1379 = &g_1380;
static int32_t g_1386 = 0x04F9C8E8L;
static int32_t *g_1387[3][7][3] = {{{&g_1386,&g_1386,&g_1386},{(void*)0,(void*)0,(void*)0},{&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386}},{{(void*)0,(void*)0,(void*)0},{&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386},{(void*)0,(void*)0,(void*)0}},{{&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386},{(void*)0,(void*)0,(void*)0},{&g_1386,&g_1386,&g_1386}}};
static volatile int16_t g_1428 = (-9L);/* VOLATILE GLOBAL g_1428 */
static volatile int16_t *g_1427 = &g_1428;
static volatile int16_t ** volatile g_1426 = &g_1427;/* VOLATILE GLOBAL g_1426 */
static int16_t *g_1620 = (void*)0;
static int16_t **g_1619 = &g_1620;
static int8_t * volatile * volatile g_1674 = &g_227;/* VOLATILE GLOBAL g_1674 */
static int8_t * volatile * volatile *g_1673 = &g_1674;
static int8_t * volatile * volatile **g_1672 = &g_1673;
static float * volatile g_1716 = &g_856;/* VOLATILE GLOBAL g_1716 */
static int32_t g_1744 = 0x700C7C6FL;
static volatile int32_t *g_1773 = (void*)0;
static int32_t ** volatile g_1788 = &g_101;/* VOLATILE GLOBAL g_1788 */
static int32_t ** volatile g_1861 = &g_101;/* VOLATILE GLOBAL g_1861 */
static int32_t ** const **g_1904[9] = {&g_369,&g_369,&g_369,&g_369,&g_369,&g_369,&g_369,&g_369,&g_369};
static const volatile uint8_t *g_1910 = &g_78[2];
static const volatile uint8_t **g_1909 = &g_1910;
static const volatile uint8_t ***g_1908 = &g_1909;
static const volatile uint8_t *** volatile * volatile g_1907 = &g_1908;/* VOLATILE GLOBAL g_1907 */
static uint32_t **** volatile g_1979 = (void*)0;/* VOLATILE GLOBAL g_1979 */
static const volatile uint32_t g_2062[8] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
static float ** const g_2093 = &g_97;
static float ** const *g_2092 = &g_2093;
static int16_t g_2098 = 0x47C7L;
static int8_t g_2180 = 1L;
static int16_t g_2181 = 0x6CD0L;
static uint16_t g_2184 = 65535UL;
static uint8_t *g_2193[4][2] = {{&g_667,&g_667},{(void*)0,&g_667},{&g_667,(void*)0},{&g_667,&g_667}};
static uint8_t **g_2192 = &g_2193[3][0];
static uint8_t **g_2198 = (void*)0;
static int16_t **** volatile g_2211 = (void*)0;/* VOLATILE GLOBAL g_2211 */
static int16_t ***g_2213 = &g_1619;
static int16_t **** volatile g_2212 = &g_2213;/* VOLATILE GLOBAL g_2212 */
static int64_t g_2239[3] = {1L,1L,1L};
static int64_t *g_2253 = &g_183[4];
static const int64_t g_2284 = 3L;
static int32_t ** volatile g_2319[1] = {(void*)0};
static int32_t ** volatile g_2320 = &g_101;/* VOLATILE GLOBAL g_2320 */
static uint32_t *g_2371 = &g_125;
static uint32_t *g_2372 = &g_41;
static int8_t ** volatile * volatile g_2460 = &g_1237;/* VOLATILE GLOBAL g_2460 */
static int8_t ***g_2495 = &g_1237;
static int8_t *** const *g_2494 = &g_2495;
static volatile uint32_t g_2526 = 0xBCEE8E53L;/* VOLATILE GLOBAL g_2526 */
static const uint8_t *g_2543 = &g_667;
static const uint8_t **g_2542 = &g_2543;
static const uint8_t ***g_2541 = &g_2542;
static const int32_t ** volatile g_2594 = &g_859;/* VOLATILE GLOBAL g_2594 */
static int8_t ****g_2614 = &g_2495;
static int8_t *****g_2613 = &g_2614;
static const int32_t g_2646 = 0x88B93832L;
static int8_t ***g_2665 = &g_1237;
static float **g_2676 = &g_97;
static float ***g_2675[6][7] = {{&g_2676,&g_2676,(void*)0,&g_2676,&g_2676,(void*)0,&g_2676},{&g_2676,(void*)0,&g_2676,&g_2676,&g_2676,(void*)0,&g_2676},{&g_2676,&g_2676,&g_2676,&g_2676,&g_2676,&g_2676,&g_2676},{&g_2676,&g_2676,&g_2676,&g_2676,&g_2676,&g_2676,&g_2676},{&g_2676,&g_2676,&g_2676,&g_2676,&g_2676,&g_2676,&g_2676},{&g_2676,&g_2676,&g_2676,(void*)0,&g_2676,(void*)0,&g_2676}};
static float ****g_2674[6][7][6] = {{{&g_2675[3][4],&g_2675[3][4],(void*)0,&g_2675[5][0],&g_2675[3][4],&g_2675[3][4]},{&g_2675[2][2],(void*)0,&g_2675[3][3],&g_2675[4][2],&g_2675[0][2],&g_2675[0][2]},{(void*)0,&g_2675[3][4],&g_2675[3][4],(void*)0,&g_2675[3][4],&g_2675[3][4]},{&g_2675[0][5],(void*)0,(void*)0,(void*)0,&g_2675[5][0],(void*)0},{&g_2675[3][4],(void*)0,(void*)0,&g_2675[3][4],&g_2675[5][0],(void*)0},{&g_2675[2][2],(void*)0,&g_2675[3][4],(void*)0,&g_2675[3][4],&g_2675[3][4]},{(void*)0,&g_2675[3][4],&g_2675[4][2],&g_2675[3][0],&g_2675[0][2],(void*)0}},{{&g_2675[3][4],(void*)0,(void*)0,(void*)0,&g_2675[3][4],&g_2675[0][5]},{&g_2675[3][6],&g_2675[3][4],&g_2675[3][3],&g_2675[3][4],&g_2675[3][0],(void*)0},{(void*)0,&g_2675[2][2],&g_2675[3][3],(void*)0,&g_2675[3][4],&g_2675[0][5]},{&g_2675[5][0],(void*)0,(void*)0,(void*)0,&g_2675[0][5],(void*)0},{&g_2675[3][4],&g_2675[5][0],&g_2675[4][2],&g_2675[4][2],&g_2675[5][0],&g_2675[3][4]},{&g_2675[3][6],(void*)0,&g_2675[3][4],&g_2675[5][0],(void*)0,(void*)0},{(void*)0,&g_2675[2][2],(void*)0,(void*)0,&g_2675[0][2],(void*)0}},{{(void*)0,&g_2675[3][4],(void*)0,&g_2675[5][0],(void*)0,&g_2675[3][4]},{&g_2675[3][6],(void*)0,&g_2675[3][4],&g_2675[4][2],&g_2675[3][0],&g_2675[0][2]},{&g_2675[3][4],&g_2675[3][4],&g_2675[3][3],(void*)0,(void*)0,&g_2675[3][4]},{&g_2675[5][0],(void*)0,(void*)0,(void*)0,&g_2675[0][5],(void*)0},{(void*)0,(void*)0,&g_2675[4][2],&g_2675[3][4],&g_2675[0][5],(void*)0},{&g_2675[3][6],(void*)0,&g_2675[3][4],(void*)0,(void*)0,&g_2675[3][4]},{&g_2675[3][4],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_2675[3][4],&g_2675[3][4],&g_2675[3][4],&g_2675[4][2],&g_2675[3][4],(void*)0},{&g_2675[5][0],&g_2675[2][1],(void*)0,&g_2675[3][0],&g_2675[3][4],&g_2675[3][4]},{&g_2675[3][6],&g_2675[5][0],(void*)0,&g_2675[3][4],&g_2675[3][4],(void*)0},{(void*)0,&g_2675[3][4],&g_2675[3][4],&g_2675[2][2],&g_2675[3][4],(void*)0},{&g_2675[2][2],&g_2675[3][4],(void*)0,(void*)0,(void*)0,&g_2675[3][6]},{&g_2675[5][0],&g_2675[3][4],&g_2675[2][1],&g_2675[3][4],(void*)0,&g_2675[3][3]},{(void*)0,&g_2675[5][0],(void*)0,&g_2675[3][4],(void*)0,&g_2675[0][2]}},{{(void*)0,&g_2675[2][1],&g_2675[3][4],&g_2675[3][4],(void*)0,(void*)0},{&g_2675[5][0],&g_2675[3][4],&g_2675[3][4],(void*)0,&g_2675[3][4],&g_2675[3][4]},{&g_2675[2][2],(void*)0,(void*)0,&g_2675[2][2],(void*)0,(void*)0},{(void*)0,&g_2675[2][2],&g_2675[3][4],&g_2675[3][4],&g_2675[3][4],&g_2675[0][2]},{&g_2675[3][6],&g_2675[4][2],(void*)0,&g_2675[3][0],&g_2675[3][4],&g_2675[3][3]},{&g_2675[5][0],&g_2675[2][2],&g_2675[1][4],&g_2675[4][2],(void*)0,&g_2675[3][6]},{&g_2675[3][4],(void*)0,(void*)0,(void*)0,&g_2675[3][4],(void*)0}},{{(void*)0,&g_2675[3][4],&g_2675[3][4],&g_2675[4][2],(void*)0,(void*)0},{&g_2675[0][5],&g_2675[2][1],&g_2675[3][4],&g_2675[3][0],(void*)0,&g_2675[3][4]},{&g_2675[2][2],&g_2675[5][0],&g_2675[3][4],&g_2675[3][4],(void*)0,(void*)0},{&g_2675[3][4],&g_2675[3][4],&g_2675[3][4],&g_2675[2][2],(void*)0,(void*)0},{&g_2675[3][6],&g_2675[3][4],(void*)0,(void*)0,&g_2675[3][4],&g_2675[3][6]},{&g_2675[0][5],&g_2675[3][4],&g_2675[1][4],&g_2675[3][4],&g_2675[3][4],&g_2675[3][3]},{&g_2675[3][4],&g_2675[5][0],(void*)0,&g_2675[3][4],&g_2675[3][4],&g_2675[0][2]}}};
static float *****g_2673 = &g_2674[5][1][1];
static int8_t g_2679 = 1L;
static int32_t ***g_2725 = &g_180;
static int32_t ***g_2726[9][4][1] = {{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}},{{&g_180},{&g_180},{&g_180},{&g_180}}};
static uint32_t **g_2743[5][9][5] = {{{(void*)0,(void*)0,&g_2371,&g_2372,&g_2372},{&g_2371,&g_2371,&g_2371,&g_2372,&g_2371},{&g_2371,&g_2372,&g_2372,(void*)0,&g_2372},{(void*)0,(void*)0,(void*)0,&g_2372,(void*)0},{&g_2372,(void*)0,&g_2372,&g_2372,&g_2372},{&g_2371,&g_2372,&g_2372,&g_2371,&g_2372},{&g_2372,&g_2372,(void*)0,&g_2372,&g_2372},{&g_2371,&g_2371,(void*)0,&g_2371,&g_2372},{&g_2372,&g_2372,&g_2371,&g_2372,(void*)0}},{{&g_2371,&g_2371,&g_2372,&g_2371,&g_2371},{&g_2371,&g_2372,&g_2372,&g_2372,&g_2372},{&g_2372,&g_2372,(void*)0,&g_2372,&g_2371},{&g_2372,&g_2372,(void*)0,&g_2372,&g_2372},{&g_2372,&g_2372,&g_2372,(void*)0,&g_2371},{&g_2372,(void*)0,&g_2371,&g_2372,&g_2372},{&g_2372,&g_2372,&g_2372,&g_2372,&g_2372},{&g_2371,&g_2371,&g_2371,(void*)0,&g_2372},{&g_2371,(void*)0,&g_2372,&g_2372,&g_2372}},{{&g_2372,&g_2372,(void*)0,&g_2371,&g_2372},{&g_2371,&g_2372,(void*)0,&g_2372,&g_2372},{&g_2372,&g_2372,&g_2372,&g_2372,&g_2372},{&g_2371,&g_2371,&g_2372,&g_2371,&g_2371},{(void*)0,&g_2372,&g_2371,&g_2372,&g_2372},{&g_2372,&g_2371,(void*)0,&g_2371,&g_2371},{&g_2372,&g_2371,&g_2372,&g_2372,&g_2372},{&g_2372,&g_2371,&g_2372,&g_2372,&g_2371},{&g_2372,(void*)0,&g_2372,&g_2371,(void*)0}},{{&g_2372,&g_2371,&g_2372,&g_2372,&g_2372},{(void*)0,&g_2372,&g_2372,(void*)0,&g_2372},{&g_2371,(void*)0,&g_2372,&g_2372,&g_2372},{&g_2372,&g_2372,&g_2372,&g_2372,&g_2372},{&g_2371,&g_2372,(void*)0,(void*)0,&g_2372},{&g_2372,&g_2372,&g_2371,&g_2372,(void*)0},{&g_2371,&g_2372,&g_2372,&g_2372,&g_2371},{&g_2372,&g_2372,&g_2372,&g_2372,&g_2372},{&g_2372,(void*)0,(void*)0,&g_2371,&g_2371}},{{&g_2372,&g_2372,(void*)0,&g_2372,&g_2372},{&g_2372,&g_2371,&g_2372,&g_2371,&g_2371},{&g_2372,(void*)0,&g_2371,&g_2372,&g_2371},{&g_2372,&g_2371,&g_2372,&g_2371,&g_2372},{&g_2372,&g_2371,&g_2371,(void*)0,&g_2372},{&g_2371,&g_2371,&g_2372,&g_2371,&g_2372},{&g_2372,&g_2372,(void*)0,&g_2371,(void*)0},{(void*)0,&g_2372,&g_2372,(void*)0,&g_2371},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
static int16_t g_2764 = 0xDE2DL;
static int32_t g_2769 = (-1L);
static float g_2865 = 0x3.9p-1;
static uint16_t g_2901[4] = {0x7FE5L,0x7FE5L,0x7FE5L,0x7FE5L};
static uint32_t g_2935 = 0UL;
static uint8_t *** const g_2975 = (void*)0;
static uint8_t *** const *g_2974 = &g_2975;
static uint8_t *** const **g_2973[10][3] = {{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974},{&g_2974,&g_2974,&g_2974}};
static int8_t g_2991 = 0xE3L;
static uint8_t ***g_3000[2][10][9] = {{{&g_2198,&g_2198,&g_2192,&g_2198,&g_2198,&g_2192,&g_2198,(void*)0,&g_2198},{&g_2198,&g_2192,&g_2198,&g_2198,&g_2192,&g_2192,&g_2192,&g_2198,&g_2198},{(void*)0,(void*)0,&g_2192,&g_2198,&g_2192,&g_2198,&g_2192,&g_2198,&g_2198},{&g_2198,(void*)0,&g_2198,&g_2198,&g_2198,&g_2198,&g_2192,&g_2192,&g_2192},{&g_2198,&g_2198,&g_2192,&g_2198,&g_2192,&g_2198,&g_2198,&g_2192,(void*)0},{&g_2198,&g_2192,&g_2198,&g_2198,&g_2198,&g_2192,&g_2198,&g_2192,&g_2198},{&g_2198,&g_2192,&g_2192,&g_2192,&g_2192,&g_2198,&g_2198,&g_2192,&g_2192},{(void*)0,&g_2198,(void*)0,(void*)0,(void*)0,&g_2198,&g_2198,&g_2192,&g_2192},{&g_2198,&g_2192,(void*)0,&g_2198,&g_2192,&g_2192,&g_2198,&g_2198,&g_2198},{&g_2198,&g_2198,&g_2198,(void*)0,(void*)0,&g_2198,&g_2198,&g_2198,(void*)0}},{{&g_2198,&g_2198,&g_2192,&g_2192,&g_2192,&g_2198,(void*)0,(void*)0,&g_2192},{&g_2198,&g_2192,&g_2192,&g_2198,(void*)0,&g_2198,&g_2198,&g_2198,(void*)0},{&g_2198,&g_2198,&g_2198,&g_2198,&g_2198,&g_2192,&g_2198,&g_2198,&g_2198},{&g_2198,&g_2192,&g_2192,&g_2198,&g_2198,&g_2192,(void*)0,&g_2192,&g_2192},{&g_2192,&g_2192,&g_2198,&g_2198,&g_2198,&g_2198,&g_2198,&g_2192,&g_2192},{(void*)0,&g_2198,&g_2198,&g_2198,(void*)0,&g_2198,&g_2198,&g_2198,&g_2198},{&g_2198,(void*)0,&g_2198,&g_2192,&g_2198,&g_2198,&g_2198,&g_2198,&g_2198},{&g_2198,&g_2192,&g_2192,&g_2192,&g_2198,&g_2192,&g_2198,&g_2198,&g_2198},{&g_2198,&g_2192,&g_2198,&g_2192,&g_2198,&g_2198,&g_2192,&g_2192,&g_2192},{&g_2198,&g_2192,(void*)0,&g_2192,&g_2192,&g_2192,&g_2192,(void*)0,&g_2192}}};
static uint8_t ****g_2999 = &g_3000[1][3][4];
static uint8_t *****g_2998 = &g_2999;
static uint64_t g_3065 = 0UL;
static volatile int32_t g_3074[9][7] = {{0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL},{0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L},{0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL},{0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L},{0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL},{0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L},{0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL},{0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L,0x633DC264L},{0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL,0x808DC0EEL}};


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static uint64_t  func_14(uint32_t  p_15, int32_t * p_16);
static int32_t  func_19(int32_t * p_20, int32_t  p_21);
static int8_t  func_22(const int32_t * p_23, int32_t * p_24);
static const uint32_t  func_34(int32_t ** p_35, const int64_t  p_36, uint32_t  p_37, int32_t  p_38, const int32_t * p_39);
static const int16_t  func_44(uint32_t * p_45, uint32_t * const  p_46, uint32_t  p_47, int8_t  p_48, int64_t  p_49);
static uint32_t * func_50(uint32_t * p_51, const float  p_52, int32_t * p_53, int32_t  p_54);
static uint32_t * func_55(uint32_t * p_56, uint32_t * p_57, int16_t  p_58, int32_t ** p_59);
static uint32_t * func_61(int32_t * p_62, uint32_t * p_63, int16_t  p_64);
static int32_t * const  func_71(uint32_t * p_72, int32_t * const  p_73);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_29 g_849 g_850 g_1236 g_1237 g_227 g_174 g_2253 g_183 g_1216 g_163 g_2239 g_253 g_470 g_414 g_2093 g_97 g_1716 g_2460 g_4 g_423 g_424 g_2494 g_249 g_101 g_1672 g_1673 g_1674 g_2181 g_78 g_2092 g_91 g_2526 g_2541 g_1907 g_1908 g_7 g_1379 g_1380 g_41 g_2542 g_2543 g_667 g_856 g_2495 g_197 g_125 g_1313 g_1095 g_2320 g_100 g_1290 g_146 g_2594 g_1619 g_371 g_368 g_2613 g_428 g_679 g_2614 g_2213 g_1620 g_2192 g_2193 g_2646 g_1909 g_1910 g_2372 g_2673 g_1426 g_1427 g_1428 g_1788 g_28 g_2665 g_2764 g_2769 g_427 g_375 g_2676 g_859 g_11
 * writes: g_850 g_1216 g_253 g_29 g_91 g_856 g_2184 g_414 g_1237 g_311 g_2180 g_125 g_7 g_174 g_249 g_2181 g_163 g_146 g_2098 g_2495 g_2541 g_183 g_1379 g_470 g_305 g_101 g_1387 g_859 g_4 g_428 g_679 g_1290 g_1620 g_2665 g_135 g_2679 g_2725 g_2726 g_2743
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    int32_t l_1851 = 0x5761555BL;
    int32_t l_1856 = 0L;
    int32_t l_1869 = 9L;
    int32_t l_1871 = 0xEE9DA894L;
    int32_t l_1872 = 0x3695E640L;
    uint32_t l_1873 = 3UL;
    float *l_1881 = &g_91[3];
    int8_t *l_1906 = &g_253;
    uint64_t *l_1911 = &g_305;
    uint64_t l_2041 = 0UL;
    int16_t l_2061 = 0xCB17L;
    uint64_t *l_2082 = &g_1290[1];
    int16_t ***l_2097 = &g_1619;
    int64_t l_2112 = (-1L);
    uint32_t l_2113[3][2][5] = {{{4294967295UL,0xF60F842BL,0xD99C77CCL,0xD95F7C3BL,0UL},{0xD99C77CCL,0x9ACA440BL,0xF60F842BL,0xF60F842BL,0x9ACA440BL}},{{0x9ACA440BL,4294967295UL,0xD99C77CCL,0UL,4294967295UL},{4294967295UL,4294967295UL,0UL,0x5B7625BCL,2UL}},{{0x7ABC1152L,0x9ACA440BL,0x9ACA440BL,0x7ABC1152L,0x5B7625BCL},{4294967295UL,0xF60F842BL,4294967295UL,0xA97DE0B6L,0x5B7625BCL}}};
    int32_t l_2138[4][9][3] = {{{0x31DCEAADL,0x5CD3453EL,0L},{0xF32D7D29L,(-1L),0x5BC88B56L},{0xB9F5F286L,(-4L),0xB9F5F286L},{1L,0L,(-1L)},{0x0C96C819L,(-4L),0xB4F22F31L},{0L,0x472EEE04L,(-10L)},{1L,0xB4F22F31L,(-2L)},{0L,0xB0808668L,0xD8FE1FADL},{0x0C96C819L,0x2DA42E53L,0x2DA42E53L}},{{1L,(-10L),1L},{0xB9F5F286L,1L,1L},{0xF32D7D29L,(-1L),(-1L)},{0x31DCEAADL,0xB66AB215L,(-1L)},{0x5BC88B56L,(-1L),(-10L)},{0x5CD3453EL,1L,0x0C96C819L},{(-10L),(-10L),(-1L)},{(-1L),0x2DA42E53L,0x5A7B1143L},{0x472EEE04L,0xB0808668L,1L}},{{0x5A7B1143L,0xB4F22F31L,0x5CD3453EL},{0xD4E7F277L,0x472EEE04L,1L},{0x7A569511L,(-4L),0x5A7B1143L},{(-1L),(-10L),1L},{(-1L),0xB4F22F31L,0xB9F5F286L},{(-1L),0x6DA356E8L,0xF32D7D29L},{0xB4F22F31L,1L,0x31DCEAADL},{1L,0xF32D7D29L,0x5BC88B56L},{0xB4F22F31L,1L,0x5CD3453EL}},{{(-1L),(-10L),(-10L)},{(-1L),0x31DCEAADL,(-1L)},{1L,1L,0x472EEE04L},{(-2L),0x88935C33L,0x5A7B1143L},{0x6D21460BL,1L,0xD4E7F277L},{0x0C96C819L,0x88935C33L,0x7A569511L},{0x5D881865L,1L,(-1L)},{0x31DCEAADL,0x31DCEAADL,0x88935C33L},{0xD4E7F277L,(-10L),(-1L)}}};
    int32_t *l_2144 = &g_4;
    int32_t *l_2146 = &g_29;
    uint16_t l_2188 = 0xD459L;
    uint32_t *l_2201 = &l_1873;
    uint32_t **l_2200 = &l_2201;
    uint32_t ***l_2199[1][6] = {{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200}};
    float **l_2205 = &l_1881;
    float ***l_2204 = &l_2205;
    int32_t l_2238[10][4][5] = {{{(-1L),0L,(-1L),1L,0L},{0x5007C8A8L,(-1L),1L,(-1L),0x5007C8A8L},{0x9E842908L,3L,0xA128409DL,0L,0L},{(-1L),(-1L),0x1C653C2AL,0x0DAEFF8EL,0xEF36A5F6L}},{{(-6L),(-1L),1L,3L,0L},{(-10L),0x0DAEFF8EL,8L,(-1L),0x5007C8A8L},{0L,7L,7L,0L,0L},{(-1L),0xC8B7CF17L,0x1EF3A5E7L,0xFF731867L,(-1L)}},{{0L,(-1L),0xA128409DL,7L,0L},{0x5007C8A8L,6L,(-1L),0xFF731867L,(-10L)},{(-6L),3L,0L,0L,(-1L)},{0x1C653C2AL,0xFF731867L,0x1C653C2AL,(-1L),(-1L)}},{{0L,0L,0xC7A55566L,3L,0L},{0x5007C8A8L,0xC8B7CF17L,0x5EB77D2DL,0x0DAEFF8EL,0x5007C8A8L},{0L,1L,0xC7A55566L,0L,0L},{(-1L),1L,0x1C653C2AL,(-1L),0x1C653C2AL}},{{(-1L),(-1L),0L,1L,(-6L)},{(-10L),(-1L),(-1L),(-1L),0x5007C8A8L},{0L,0xC7A55566L,0xA128409DL,0L,0L},{(-1L),(-1L),0x1EF3A5E7L,0x0DAEFF8EL,(-1L)}},{{0L,(-1L),7L,0xC7A55566L,0L},{0x5007C8A8L,1L,8L,0xC8B7CF17L,(-10L)},{0L,1L,1L,0L,(-6L)},{0xEF36A5F6L,0xC8B7CF17L,0x1C653C2AL,6L,(-1L)}},{{0L,0L,0xA128409DL,1L,0x9E842908L},{0x5007C8A8L,0xFF731867L,1L,0xFF731867L,0x5007C8A8L},{0L,3L,(-1L),0L,(-1L)},{(-1L),6L,0x1C653C2AL,0xC8B7CF17L,0xEF36A5F6L}},{{0x9E842908L,0L,3L,0xC7A55566L,0L},{2L,(-1L),0x5EB77D2DL,0x052C4540L,(-10L)},{0L,1L,0L,0L,0xE0A7320BL},{0xEF36A5F6L,1L,(-1L),(-1L),0x1C653C2AL}},{{0x5AED0E34L,0L,0L,1L,0L},{(-10L),0xF0833481L,1L,(-1L),2L},{0x9E842908L,0xC7A55566L,0xC420B167L,0L,0L},{0x1EF3A5E7L,(-1L),0x1EF3A5E7L,0x052C4540L,0xEF36A5F6L}},{{0L,0x5AED0E34L,1L,0xC7A55566L,(-5L)},{(-10L),1L,0xE4CBE064L,(-1L),(-10L)},{(-5L),7L,1L,(-5L),0L},{0xEF36A5F6L,7L,0x1EF3A5E7L,6L,0x1EF3A5E7L}}};
    uint16_t l_2250 = 0x8FAEL;
    int32_t ***l_2307 = (void*)0;
    int32_t ****l_2306[7][1];
    int32_t *****l_2305[6][3] = {{(void*)0,(void*)0,(void*)0},{&l_2306[2][0],&l_2306[2][0],&l_2306[5][0]},{(void*)0,(void*)0,(void*)0},{&l_2306[2][0],&l_2306[2][0],&l_2306[5][0]},{(void*)0,(void*)0,(void*)0},{&l_2306[2][0],&l_2306[2][0],&l_2306[5][0]}};
    int32_t * const l_2317 = &g_163;
    uint16_t l_2343[3];
    int32_t * const l_2374 = &l_2238[6][1][3];
    uint32_t l_2389[3][7] = {{18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL},{18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL,18446744073709551609UL}};
    int16_t ***l_2401 = &g_1619;
    float ****l_2477 = &l_2204;
    float *****l_2476 = &l_2477;
    int32_t *l_2553[7] = {&g_428,&g_428,&l_1871,&g_428,&g_428,&l_1871,&g_428};
    int32_t l_2586 = 0x87A08BDBL;
    int16_t l_2595 = 0x3566L;
    int8_t * const *l_2612 = &l_1906;
    int8_t * const ** const l_2611 = &l_2612;
    int8_t * const ** const *l_2610 = &l_2611;
    int8_t * const ** const **l_2609 = &l_2610;
    int8_t *****l_2615 = &g_2614;
    uint16_t l_2618 = 65535UL;
    int32_t l_2645 = 0xDE4C97F8L;
    uint8_t ***l_2707 = &g_2192;
    uint8_t ****l_2706[6][10];
    int8_t l_2761 = 1L;
    int32_t l_2965 = 0x3ACEE6AFL;
    const int8_t l_2992 = (-3L);
    uint64_t l_3075 = 0xE2B5C3BCCAF10E9BLL;
    float *l_3098 = &g_856;
    float **l_3097 = &l_3098;
    float *l_3100 = (void*)0;
    float **l_3099 = &l_3100;
    uint16_t l_3101 = 1UL;
    int32_t *l_3102[1][3];
    uint16_t l_3103 = 0x6A7BL;
    int i, j, k;
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
            l_2306[i][j] = &l_2307;
    }
    for (i = 0; i < 3; i++)
        l_2343[i] = 0xF64EL;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
            l_2706[i][j] = &l_2707;
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_3102[i][j] = (void*)0;
    }
    if ((((-1L) && 255UL) != g_2))
    { /* block id: 1 */
        int32_t *l_3 = &g_4;
        int32_t l_1864[7][4][6] = {{{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L}},{{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L}},{{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L}},{{8L,1L,1L,8L,1L,1L},{8L,1L,1L,8L,1L,1L},{8L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL}},{{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL}},{{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL}},{{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL},{1L,0xC5C6CECFL,0xC5C6CECFL,1L,0xC5C6CECFL,0xC5C6CECFL}}};
        int32_t l_1924 = (-3L);
        int64_t l_2023[7] = {0xD87A466C16B26F80LL,0xD87A466C16B26F80LL,0xD87A466C16B26F80LL,0xD87A466C16B26F80LL,0xD87A466C16B26F80LL,0xD87A466C16B26F80LL,0xD87A466C16B26F80LL};
        int32_t l_2026[1];
        int32_t ***l_2030[6];
        int16_t l_2039 = 1L;
        uint32_t l_2081 = 0x69BB489BL;
        int32_t *l_2145 = &g_4;
        uint16_t *l_2158 = &g_146;
        uint32_t ***l_2167 = (void*)0;
        uint32_t *l_2170 = &l_1873;
        uint32_t **l_2169 = &l_2170;
        uint32_t ***l_2168 = &l_2169;
        int32_t *l_2171 = (void*)0;
        int32_t *l_2172 = &l_1851;
        uint16_t *l_2178 = &g_249[3];
        int8_t *l_2179 = &g_2180;
        uint8_t l_2182 = 0UL;
        uint16_t *l_2183 = &g_2184;
        int16_t l_2185 = 3L;
        uint8_t *l_2186 = &l_2182;
        int8_t l_2187[4][3][8] = {{{0xD2L,0x87L,0L,1L,7L,1L,0L,0x87L},{7L,1L,0L,0x87L,0xD2L,1L,0xC7L,1L},{7L,0x87L,0L,0x87L,7L,0x20L,0xC7L,0x87L}},{{0xD2L,0x87L,0L,1L,7L,1L,0L,0x87L},{7L,1L,0L,0x87L,0xD2L,1L,0xC7L,1L},{7L,0x87L,0L,0x87L,7L,0x20L,0xC7L,0x87L}},{{0xD2L,0x87L,0L,1L,7L,1L,0L,0x87L},{7L,1L,0L,0x87L,0xD2L,1L,0xC7L,1L},{7L,0x87L,0L,0x87L,7L,0x20L,0xC7L,0x87L}},{{0xD2L,0x87L,0L,1L,7L,1L,0L,0x87L},{7L,1L,0L,0x87L,0xD2L,1L,0xC7L,1L},{7L,0x87L,0L,0x87L,7L,0x20L,0xC7L,0x87L}}};
        float *l_2189 = &g_856;
        uint8_t ***l_2194 = &g_2192;
        uint8_t ***l_2195 = (void*)0;
        uint8_t **l_2197[7][10];
        uint8_t ***l_2196[5][2][6] = {{{(void*)0,(void*)0,&l_2197[4][0],&l_2197[3][9],&l_2197[4][0],&l_2197[4][0]},{&l_2197[3][2],(void*)0,&l_2197[3][9],&l_2197[3][9],(void*)0,&l_2197[3][2]}},{{&l_2197[4][0],&l_2197[3][2],&l_2197[3][9],&l_2197[3][2],&l_2197[4][0],&l_2197[4][0]},{&l_2197[3][9],&l_2197[3][2],&l_2197[3][2],&l_2197[3][9],(void*)0,&l_2197[3][9]}},{{&l_2197[3][9],(void*)0,&l_2197[3][9],&l_2197[3][2],&l_2197[3][2],&l_2197[3][9]},{&l_2197[4][0],&l_2197[4][0],&l_2197[3][2],&l_2197[3][9],&l_2197[3][2],&l_2197[4][0]}},{{&l_2197[3][2],(void*)0,&l_2197[3][9],&l_2197[3][9],(void*)0,&l_2197[3][2]},{&l_2197[4][0],&l_2197[3][2],&l_2197[3][9],&l_2197[3][2],&l_2197[4][0],&l_2197[4][0]}},{{&l_2197[3][9],&l_2197[3][2],&l_2197[3][2],&l_2197[3][9],(void*)0,&l_2197[3][9]},{&l_2197[3][9],(void*)0,&l_2197[3][9],&l_2197[3][2],&l_2197[3][2],&l_2197[3][9]}}};
        int16_t *l_2206 = &l_2039;
        int8_t l_2207 = (-6L);
        int16_t ***l_2210[8];
        const float *l_2221[2][1][6] = {{{&g_135[2][0][1],&g_135[2][0][1],&g_135[3][0][4],&g_135[3][0][4],&g_135[3][0][4],&g_135[2][0][1]}},{{&g_135[3][0][4],&g_93,&g_135[3][0][4],&g_135[3][0][4],&g_93,&g_135[3][0][4]}}};
        const float * const *l_2220 = &l_2221[0][0][2];
        uint64_t l_2236 = 0xEE42AC96AAE11E54LL;
        float l_2242 = 0x0.0p-1;
        int32_t *l_2243 = &l_1871;
        uint64_t l_2262 = 0x44AF1E809F826083LL;
        uint32_t l_2296 = 0UL;
        const int64_t l_2300 = 0xBA2161173E69D149LL;
        uint8_t l_2325 = 255UL;
        int8_t **l_2332 = &l_1906;
        uint16_t l_2349 = 0x7F89L;
        uint32_t l_2354 = 18446744073709551611UL;
        uint8_t **l_2417 = &l_2186;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2026[i] = 0x36563488L;
        for (i = 0; i < 6; i++)
            l_2030[i] = &g_180;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 10; j++)
                l_2197[i][j] = &l_2186;
        }
        for (i = 0; i < 8; i++)
            l_2210[i] = &g_1619;
    }
    else
    { /* block id: 1028 */
        uint32_t *l_2425 = (void*)0;
        int32_t *l_2426 = &l_2238[7][1][2];
        int16_t l_2452 = 6L;
        uint32_t l_2455 = 0xD5E1A435L;
        int16_t ****l_2511 = &l_2401;
        float **l_2515 = &l_1881;
        int32_t l_2578 = 0x468AC505L;
        int32_t l_2585 = 0L;
        int64_t l_2587 = 3L;
        int32_t l_2588 = (-1L);
        int64_t l_2616 = 6L;
        uint32_t l_2617 = 4294967295UL;
        if ((*l_2146))
        { /* block id: 1029 */
            int16_t l_2453[3];
            uint32_t l_2475 = 0xCB720087L;
            const uint8_t l_2528 = 0x1CL;
            int32_t *l_2556 = (void*)0;
            uint64_t **l_2559 = &l_1911;
            uint32_t l_2566 = 0xCF49092BL;
            int32_t l_2580 = 0xCEAB1A17L;
            int32_t l_2583[4];
            int i;
            for (i = 0; i < 3; i++)
                l_2453[i] = 1L;
            for (i = 0; i < 4; i++)
                l_2583[i] = 0x3AAA544EL;
lbl_2554:
            (*g_849) = (*g_849);
            l_2426 = l_2426;
            for (l_2188 = 0; (l_2188 <= 2); l_2188 += 1)
            { /* block id: 1034 */
                int8_t l_2448[3];
                uint8_t *l_2449 = (void*)0;
                uint8_t *l_2450 = &g_1216[0][8][7];
                uint8_t *l_2451[7][8] = {{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311}};
                int32_t l_2454[7] = {0L,0L,0L,0L,0L,0L,0L};
                int8_t l_2458 = 5L;
                uint32_t l_2461 = 0xB7B52AA7L;
                int64_t l_2479 = (-8L);
                int8_t **l_2499 = &g_227;
                int8_t **l_2500 = &g_227;
                int8_t **l_2501[7] = {&l_1906,&l_1906,&l_1906,&l_1906,&l_1906,&l_1906,&l_1906};
                int8_t *** const l_2498[3][9] = {{&l_2501[3],&l_2499,&l_2501[3],&l_2499,&l_2501[3],&l_2499,&l_2501[3],&l_2499,&l_2501[3]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2501[3],&l_2499,&l_2501[3],&l_2499,&l_2501[3],&l_2499,&l_2501[3],&l_2499,&l_2501[3]}};
                int8_t *** const *l_2497 = &l_2498[1][3];
                float l_2503 = 0xA.C0BDFBp+75;
                uint32_t *l_2505 = (void*)0;
                int16_t *****l_2512 = (void*)0;
                int16_t *****l_2513 = &l_2511;
                int i, j;
                for (i = 0; i < 3; i++)
                    l_2448[i] = 8L;
                if (((*l_2146) = (((safe_mul_func_uint8_t_u_u(((***g_1236) > (safe_rshift_func_int8_t_s_s(((*l_2374) = ((*l_1906) |= ((safe_mod_func_int8_t_s_s((-5L), (safe_sub_func_int16_t_s_s((((*g_2253) & (safe_add_func_uint64_t_u_u(3UL, ((safe_add_func_uint32_t_u_u((safe_unary_minus_func_int8_t_s(((safe_mod_func_uint32_t_u_u(((safe_lshift_func_int16_t_s_s(((*l_2374) , (((((safe_lshift_func_int8_t_s_u((((*l_2450) &= l_2448[2]) > ((void*)0 == (*g_1237))), (l_2455--))) & l_2453[2]) && 0L) | l_2453[2]) != l_2458)), (*l_2317))) >= (*l_2426)), (*l_2426))) < l_2453[0]))), 0x106C009CL)) < l_2458)))) == l_2454[0]), g_2239[1])))) <= 0x263AL))), 2))), l_2453[0])) > g_470) , (*g_850))))
                { /* block id: 1040 */
                    (*g_1716) = ((**g_2093) = (-0x1.6p+1));
                    for (g_2184 = 0; (g_2184 <= 2); g_2184 += 1)
                    { /* block id: 1045 */
                        (*g_850) = l_2453[2];
                        (*g_849) = (*g_849);
                        (**g_2093) = (*l_2426);
                        return (*l_2146);
                    }
                }
                else
                { /* block id: 1051 */
                    int8_t ** volatile *l_2459[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2459[i] = (void*)0;
                    (*g_2460) = (*g_1236);
                    for (l_2458 = 0; (l_2458 <= 0); l_2458 += 1)
                    { /* block id: 1055 */
                        int32_t l_2472 = 0xA5B64EEEL;
                        int i;
                        ++l_2461;
                        l_2472 = (g_174[(l_2188 + 1)] >= (0x0.Bp+1 > (((***l_2204) = (safe_add_func_float_f_f(g_174[(l_2188 + 1)], ((*l_2144) , l_2453[2])))) >= (0x8.E4C5F0p+22 == (safe_div_func_float_f_f(((*l_2426) != (*l_2426)), ((safe_add_func_float_f_f((safe_sub_func_float_f_f((-0x1.1p-1), l_2453[2])), (*l_2144))) != g_174[(l_2188 + 1)])))))));
                    }
                }
                if ((safe_mul_func_uint64_t_u_u(l_2475, (l_2476 != (void*)0))))
                { /* block id: 1061 */
                    uint64_t l_2502 = 7UL;
                    float ***l_2504 = &l_2205;
                    uint32_t *l_2506[5] = {&l_2113[2][1][3],&l_2113[2][1][3],&l_2113[2][1][3],&l_2113[2][1][3],&l_2113[2][1][3]};
                    uint16_t *l_2507 = &g_249[3];
                    int i;
                    for (g_311 = 0; (g_311 <= 2); g_311 += 1)
                    { /* block id: 1064 */
                        int8_t ***l_2493[6];
                        int8_t ****l_2492 = &l_2493[1];
                        int8_t *** const **l_2496[8] = {&g_2494,&g_2494,&g_2494,&g_2494,&g_2494,&g_2494,&g_2494,&g_2494};
                        int i;
                        for (i = 0; i < 6; i++)
                            l_2493[i] = &g_1237;
                        (***g_423) = ((!l_2479) , (-0x1.8p+1));
                        (*g_101) = (safe_rshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_s((((safe_mul_func_int8_t_s_s((***g_1236), (safe_lshift_func_int16_t_s_s(0x3230L, l_2475)))) == ((safe_add_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((*l_2144), (g_2180 = 0L))), ((*l_2477) == ((((((l_2492 == (l_2497 = g_2494)) , &g_2372) == (void*)0) >= l_2502) & l_2453[1]) , l_2504)))) | 0L)) < g_249[1]), l_2502)), 6));
                    }
                    if (((*l_2426) = (((****g_1672) = l_2475) != ((6UL ^ (l_2425 == (l_2506[1] = l_2505))) > ((*l_2507) = g_249[3])))))
                    { /* block id: 1074 */
                        return (*g_2253);
                    }
                    else
                    { /* block id: 1076 */
                        return (*g_2253);
                    }
                }
                else
                { /* block id: 1079 */
                    for (l_2250 = 0; (l_2250 <= 2); l_2250 += 1)
                    { /* block id: 1082 */
                        uint32_t l_2508 = 0x7701155FL;
                        l_2508++;
                    }
                    for (g_2181 = 0; (g_2181 <= 2); g_2181 += 1)
                    { /* block id: 1087 */
                        int i;
                        return g_78[g_2181];
                    }
                    (*l_2317) |= 0x88BF53C4L;
                }
                for (l_2061 = 8; (l_2061 >= 0); l_2061 -= 1)
                { /* block id: 1094 */
                    int i;
                    return l_2448[l_2188];
                }
                (*g_850) ^= ((((*l_2513) = l_2511) == &l_2401) | 0UL);
                for (g_146 = 0; (g_146 <= 2); g_146 += 1)
                { /* block id: 1101 */
                    const uint32_t l_2527 = 7UL;
                    int8_t ***l_2539 = &l_2500;
                    uint32_t *l_2555 = &l_2113[1][1][0];
                    int32_t l_2577 = 0x7EFF7BA5L;
                    int32_t l_2582 = 0x4693693FL;
                    int32_t l_2584 = 1L;
                    int32_t l_2589 = 0xAE7ECF55L;
                    uint32_t l_2590 = 0xEF867AAEL;
                    for (l_1851 = 5; (l_1851 >= 0); l_1851 -= 1)
                    { /* block id: 1104 */
                        int32_t l_2519[6][9] = {{0x73707B3AL,0x23A13F9EL,0xB086EDBBL,0x6CAF0863L,0xB086EDBBL,0x23A13F9EL,0x73707B3AL,0x23A13F9EL,0xB086EDBBL},{0L,0xB9D73764L,5L,1L,0x85561B8BL,0x85561B8BL,5L,0x2E191613L,0x85561B8BL},{6L,0xBA7F1445L,0L,0x23A13F9EL,0L,0xBA7F1445L,6L,0xBA7F1445L,0L},{0xB9D73764L,0x2E191613L,0x2E191613L,0xB9D73764L,0x85561B8BL,0x2E191613L,5L,0x85561B8BL,0x85561B8BL},{0xB086EDBBL,0xBA7F1445L,0xEEF6C079L,0x23A13F9EL,0xEEF6C079L,0xBA7F1445L,0xB086EDBBL,0xBA7F1445L,0xEEF6C079L},{0xB9D73764L,0x85561B8BL,0x2E191613L,5L,0x85561B8BL,0x85561B8BL,5L,0x2E191613L,0x85561B8BL}};
                        int16_t *l_2529 = &g_2098;
                        int8_t ****l_2538 = &g_2495;
                        int16_t l_2540 = 0x986DL;
                        const uint8_t ****l_2544 = &g_2541;
                        int i, j;
                        l_2454[0] &= ((*l_2426) > (+((((*l_2204) = (*l_2204)) == l_2515) < ((*l_2529) = (0UL || (((*l_2450) = ((((-(*l_2426)) < (safe_mul_func_float_f_f((***g_2092), ((l_2519[4][6] == ((safe_add_func_float_f_f((((safe_lshift_func_int16_t_s_u((safe_mod_func_uint16_t_u_u(g_2526, ((*l_2426) , l_2519[4][6]))), 6)) >= 0xBF15L) , 0x0.Dp+1), l_2453[1])) > 0x1.Dp+1)) < l_2527)))) , g_174[5]) & (*l_2144))) ^ l_2528))))));
                        (*g_101) = (((safe_mul_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(((safe_div_func_uint32_t_u_u((0x4DAA72126D909BDCLL & (safe_div_func_uint16_t_u_u((((((*g_2253) = (((((*l_2538) = (void*)0) != l_2539) || (l_2540 != (((*l_2544) = g_2541) == (*g_1907)))) != ((safe_add_func_int32_t_s_s(l_2540, (l_2540 , (safe_div_func_int8_t_s_s((safe_div_func_int32_t_s_s((safe_add_func_uint64_t_u_u(l_2453[2], 0x349389C57FDE84D4LL)), (-9L))), (*g_227)))))) ^ (*l_2374)))) && 0xA73619E2B90EBF48LL) | 1UL) & 4294967291UL), g_7))), (**g_1379))) & 0xC350L), 0xF0B4L)), l_2540)) < (**g_2542)) != (-2L));
                        l_2426 = l_2553[6];
                        return l_2519[4][6];
                    }
                    if (g_163)
                        goto lbl_2554;
                    (*g_2320) = func_50(l_2555, (*g_1716), l_2556, ((*g_101) = ((safe_sub_func_uint32_t_u_u(((((void*)0 == l_2559) > (safe_lshift_func_int8_t_s_s((safe_sub_func_int32_t_s_s(((((safe_rshift_func_int16_t_s_u((((*g_1379) != (void*)0) , ((*g_2543) ^ ((g_1216[0][6][3] ^ l_2566) == (***g_2495)))), g_197)) > (**g_2542)) , 0UL) >= l_2527), l_2461)), (*l_2426)))) && 0x7E8FL), (-1L))) & (*g_2253))));
                    for (l_1856 = 2; (l_1856 >= 0); l_1856 -= 1)
                    { /* block id: 1121 */
                        float *l_2567 = &l_2503;
                        uint16_t *l_2576[10][2][4] = {{{&g_146,&g_249[3],&l_2343[1],&l_2188},{&l_2343[1],&l_2188,&l_2250,&g_2184}},{{&g_249[1],&l_2343[0],(void*)0,(void*)0},{&l_2343[0],&l_2343[0],&l_2250,&g_249[3]}},{{&g_2184,(void*)0,&l_2250,&l_2250},{&g_249[3],&l_2343[0],(void*)0,&l_2250}},{{&l_2250,&l_2343[0],&l_2343[0],&l_2250},{&l_2343[0],(void*)0,&l_2343[0],&g_249[3]}},{{&g_249[3],&l_2343[0],&l_2188,(void*)0},{(void*)0,&l_2343[0],(void*)0,&g_2184}},{{&l_2188,&l_2188,&g_146,&l_2188},{&l_2250,&g_249[3],&g_2184,&l_2343[0]}},{{&g_146,&g_2184,&g_2184,(void*)0},{&l_2188,&l_2343[0],&l_2250,&g_249[1]}},{{&l_2188,&g_2184,&g_2184,&l_2250},{&g_146,&g_249[1],&g_2184,(void*)0}},{{&l_2250,&g_2184,&g_146,(void*)0},{&l_2188,&g_2184,(void*)0,&l_2343[0]}},{{(void*)0,&l_2188,&l_2188,(void*)0},{&g_249[3],&l_2250,&l_2343[0],&g_146}}};
                        int32_t l_2579 = 0L;
                        int32_t l_2581[9];
                        int i, j, k;
                        for (i = 0; i < 9; i++)
                            l_2581[i] = 0x2173A13CL;
                        if (g_146)
                            goto lbl_2554;
                        (*g_1716) = ((*l_2146) = (((**l_2205) = (l_2527 == (*l_2146))) <= (((*l_2567) = 0x1.Cp+1) > (((safe_mod_func_uint32_t_u_u((*l_2426), ((*l_2317) = (l_2578 = (safe_mod_func_uint64_t_u_u((*l_2426), (((void*)0 == (*g_1907)) , (safe_mod_func_uint16_t_u_u((l_2527 , (l_2577 = (((safe_lshift_func_uint16_t_u_s(0UL, (*l_2146))) ^ 0x1264D085L) > (**g_100)))), g_1290[5]))))))))) , 0x7.E285C4p+7) > l_2579))));
                        l_2590++;
                    }
                }
            }
        }
        else
        { /* block id: 1134 */
            const int32_t *l_2593[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            (*g_2594) = l_2593[2];
            return (*g_2253);
        }
        (*l_2144) |= l_2595;
        (***g_2092) = (safe_mul_func_float_f_f((safe_div_func_float_f_f((!((((safe_mul_func_float_f_f((*l_2426), ((safe_lshift_func_uint16_t_u_s(((void*)0 != (*l_2401)), 9)) , ((safe_add_func_float_f_f((*l_2426), (((((*g_97) < (safe_div_func_float_f_f(((*g_371) != (((l_2609 != (l_2615 = g_2613)) | (((0xDFL | (*l_2426)) , (*g_2253)) != (*l_2426))) , &g_369)), (***g_2092)))) == 0x4.3E9F32p+47) >= (*l_2144)) > (*l_2426)))) < 0x3.2p-1)))) != l_2616) < 0x4.8p+1) < (*l_2146))), (-0x1.9p+1))), (*l_2146)));
        l_2617 = (0x7.13DE20p-13 >= (0x2.3B4636p-12 <= ((****l_2477) = (*l_2317))));
    }
    --l_2618;
    for (g_428 = 2; (g_428 >= 0); g_428 -= 1)
    { /* block id: 1147 */
        uint32_t l_2629 = 0x780203B9L;
        uint8_t *l_2640 = (void*)0;
        int32_t **l_2687[8] = {&g_1387[0][6][2],&g_1387[0][6][2],&g_1387[0][6][2],&g_1387[0][6][2],&g_1387[0][6][2],&g_1387[0][6][2],&g_1387[0][6][2],&g_1387[0][6][2]};
        int32_t l_2747 = 0L;
        int32_t l_2748 = 0x33E66164L;
        int32_t l_2749 = 0xF4F97EA3L;
        int32_t l_2750 = 1L;
        int32_t l_2753 = 8L;
        int8_t l_2754 = 0x6EL;
        float l_2755 = 0x2.3p+1;
        uint64_t l_2756 = 0UL;
        int32_t l_2800 = 7L;
        int32_t l_2804 = 0x568DB1ADL;
        int32_t l_2807 = 0x492CAED7L;
        int32_t l_2808 = 0x48FDA661L;
        int32_t l_2809 = 1L;
        int32_t l_2810 = 0x71EF4B2CL;
        int32_t l_2811 = 0xCD07FCC6L;
        int32_t l_2812 = 0x760CFE24L;
        int32_t l_2814[2][9] = {{0L,0x79B28EBEL,0x79B28EBEL,0L,6L,0L,6L,0L,0x79B28EBEL},{6L,6L,0x6554BD24L,0L,(-1L),0L,0x6554BD24L,6L,6L}};
        int8_t l_2819[3][1][9] = {{{(-1L),(-1L),(-1L),0x3BL,(-1L),(-1L),(-1L),(-1L),0x3BL}},{{0x22L,0L,0x22L,0x56L,0x56L,0x22L,0L,0x22L,0x56L}},{{0xF6L,(-1L),(-1L),0xF6L,0xCCL,0xF6L,(-1L),(-1L),0xF6L}}};
        int8_t l_2825 = 0L;
        int32_t l_2830 = (-1L);
        int32_t *l_2832 = &l_2238[7][1][2];
        uint32_t l_2852[4][7] = {{4294967295UL,0xBF6C46B4L,0xBF6C46B4L,4294967295UL,0xBF6C46B4L,0xBF6C46B4L,4294967295UL},{0x19431F2AL,0UL,0UL,0UL,0x19431F2AL,0UL,0UL},{0xBF6C46B4L,0xBF6C46B4L,4294967295UL,0xBF6C46B4L,0xBF6C46B4L,4294967295UL,0xBF6C46B4L},{0x19431F2AL,0UL,0UL,0x19431F2AL,0UL,0UL,0x19431F2AL}};
        int64_t l_2874[1];
        int8_t * const ****l_2892 = (void*)0;
        int16_t l_2910 = 0x2986L;
        float l_2938 = 0xF.DC0D3Ap+33;
        int8_t *****l_2948 = &g_2614;
        uint32_t ***l_2954 = &g_2743[1][5][3];
        int32_t l_2986[1];
        uint32_t l_3025 = 1UL;
        int16_t l_3050 = 0xF5E3L;
        uint32_t l_3052 = 3UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2874[i] = 1L;
        for (i = 0; i < 1; i++)
            l_2986[i] = 0xDB90A219L;
        for (l_1869 = 5; (l_1869 >= 0); l_1869 -= 1)
        { /* block id: 1150 */
            int16_t * const l_2636 = &l_2061;
            int16_t * const *l_2635 = &l_2636;
            int16_t * const **l_2634 = &l_2635;
            int32_t l_2655[6][9] = {{1L,(-9L),(-1L),1L,(-1L),(-1L),(-1L),1L,(-1L)},{5L,3L,(-1L),(-1L),3L,5L,1L,0x40FDBA4FL,1L},{5L,(-9L),(-1L),5L,1L,1L,1L,0x40FDBA4FL,(-1L)},{1L,1L,1L,1L,0L,1L,1L,1L,1L},{(-1L),0x40FDBA4FL,1L,1L,1L,5L,(-1L),(-9L),5L},{1L,0x40FDBA4FL,1L,5L,3L,(-1L),(-1L),3L,5L}};
            const uint8_t l_2659 = 0x39L;
            int32_t ****l_2694[1];
            int8_t *****l_2740 = &g_2614;
            uint8_t **** const l_2772 = &l_2707;
            int i, j;
            for (i = 0; i < 1; i++)
                l_2694[i] = &l_2307;
            for (g_679 = 0; (g_679 <= 2); g_679 += 1)
            { /* block id: 1153 */
                int16_t * const ***l_2637 = &l_2634;
                uint16_t *l_2638[8][10] = {{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146},{&l_2188,(void*)0,&l_2188,&g_146,&g_146,&l_2188,(void*)0,&l_2188,&g_146,&g_146}};
                int32_t l_2639[3];
                int64_t **l_2651 = &g_2253;
                uint8_t l_2670 = 0x07L;
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_2639[i] = 0x8D85A294L;
                if (((l_2138[(g_679 + 1)][(g_428 + 1)][g_679] & ((((*****g_2613) > ((safe_sub_func_int16_t_s_s(((18446744073709551612UL == ((*l_2082)++)) <= (safe_mod_func_int64_t_s_s(((*g_2253) = (((**g_2213) = (**g_2213)) == (void*)0)), ((*l_1911) = (safe_mul_func_uint8_t_u_u(l_2629, ((l_2639[2] = (safe_sub_func_int32_t_s_s((l_2629 && (safe_sub_func_uint16_t_u_u((g_2184 = (((((*l_2637) = l_2634) != (l_2138[(g_679 + 1)][(g_428 + 1)][g_679] , (void*)0)) , (**g_849)) == (*g_1380))), l_2138[(g_679 + 1)][(g_428 + 1)][g_679]))), l_2629))) , (*l_2146)))))))), 0UL)) >= (****g_2494))) , l_2640) != (*g_2192))) ^ l_2629))
                { /* block id: 1161 */
                    int64_t l_2644[8][5] = {{0x3191B7B8E2347160LL,(-2L),0x3191B7B8E2347160LL,0L,0L},{3L,(-10L),3L,(-1L),(-1L)},{0x3191B7B8E2347160LL,(-2L),0x3191B7B8E2347160LL,0L,0L},{3L,(-10L),3L,(-1L),(-1L)},{0x3191B7B8E2347160LL,(-2L),0x3191B7B8E2347160LL,0L,0L},{3L,(-10L),3L,(-1L),(-1L)},{0x3191B7B8E2347160LL,(-2L),0x3191B7B8E2347160LL,0L,0L},{3L,(-10L),3L,3L,3L}};
                    int64_t **l_2652 = &g_2253;
                    int i, j;
                    for (g_311 = 0; (g_311 <= 2); g_311 += 1)
                    { /* block id: 1164 */
                        (*l_2144) = ((((~0xDFE6414F8D1C9859LL) , (**g_2460)) != (void*)0) ^ (((safe_mod_func_uint64_t_u_u(((*l_1911) = l_2629), l_2644[5][0])) < ((*g_2253) = (l_2138[(g_679 + 1)][(g_428 + 1)][g_679] <= l_2645))) <= g_2646));
                    }
                    (*g_100) = func_50(&g_125, (*g_97), (((1UL != ((****g_2614) , (safe_lshift_func_uint16_t_u_u(((((1L <= ((l_2651 != l_2652) < (((****g_1907) && (safe_mul_func_int16_t_s_s(((((*g_2372) , l_2644[2][2]) < (**g_1909)) > l_2629), l_2655[0][4]))) != l_2644[0][1]))) != l_2629) >= l_2629) < l_2639[2]), 0)))) == (*g_2253)) , (void*)0), l_2138[(g_679 + 1)][(g_428 + 1)][g_679]);
                }
                else
                { /* block id: 1170 */
                    int32_t l_2656 = 0xC9AED5A1L;
                    float *****l_2677 = &l_2477;
                    for (l_2112 = 0; (l_2112 <= 7); l_2112 += 1)
                    { /* block id: 1173 */
                        int8_t ****l_2664[6][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                        float *l_2678 = &g_135[2][0][1];
                        int i, j;
                        (*l_2144) = l_2639[2];
                        (*l_2146) &= l_2656;
                        g_2679 = (safe_sub_func_float_f_f(0xB.145076p-22, (l_2659 == ((safe_div_func_float_f_f(((*l_2678) = (safe_div_func_float_f_f(((**g_2613) == (g_2665 = (**l_2615))), (safe_add_func_float_f_f((safe_div_func_float_f_f(l_2670, 0x0.C7C62Ep-59)), ((-0x9.Cp-1) > ((**g_2093) = (safe_div_func_float_f_f(((l_2677 = g_2673) == &g_2674[5][1][1]), l_2629))))))))), l_2629)) != (*g_1716)))));
                    }
                }
            }
            for (l_2618 = 0; (l_2618 <= 2); l_2618 += 1)
            { /* block id: 1186 */
                uint32_t *l_2683 = &l_1873;
                int32_t **l_2686 = (void*)0;
                for (l_2645 = 0; (l_2645 <= 2); l_2645 += 1)
                { /* block id: 1189 */
                    uint32_t l_2680 = 4294967292UL;
                    int i, j, k;
                    l_2680++;
                    if (l_2138[l_2618][(l_2645 + 5)][l_2618])
                        break;
                    (*g_2320) = func_55((*l_2200), l_2683, (**g_1426), &g_101);
                }
                (*g_1788) = (((safe_sub_func_uint64_t_u_u(l_2629, 0xBF71CC4EAC902F46LL)) , (l_2686 == l_2687[5])) , l_2683);
            }
            for (l_2595 = 0; (l_2595 <= 2); l_2595 += 1)
            { /* block id: 1198 */
                uint8_t ****l_2705 = (void*)0;
                int32_t l_2718[2][5][4] = {{{(-4L),0xF7288E85L,0x91FA09ADL,0xDCB27B06L},{0x0D301AFDL,0xF7288E85L,0xF7288E85L,0x0D301AFDL},{0xF7288E85L,0x0D301AFDL,(-4L),0xAE10E6B8L},{0xF7288E85L,(-4L),0xF7288E85L,0x91FA09ADL},{0x0D301AFDL,0xAE10E6B8L,0x91FA09ADL,0x91FA09ADL}},{{(-4L),(-4L),0xDCB27B06L,0xAE10E6B8L},{0xAE10E6B8L,0x0D301AFDL,0xDCB27B06L,0x0D301AFDL},{(-4L),0xF7288E85L,0x91FA09ADL,0xDCB27B06L},{0x0D301AFDL,0xF7288E85L,0xF7288E85L,0x0D301AFDL},{0xF7288E85L,0x0D301AFDL,(-4L),0xAE10E6B8L}}};
                int32_t l_2719 = 0xB1507987L;
                int32_t l_2727 = (-1L);
                int i, j, k;
                (*g_28) &= ((*l_2374) = (safe_add_func_uint64_t_u_u(0x22F00C5484A3AF66LL, (safe_rshift_func_int16_t_s_s(((safe_add_func_int32_t_s_s((-1L), ((l_2138[l_2595][(g_428 + 6)][l_2595] >= ((void*)0 == l_2694[0])) > 0xDBCFL))) <= ((*****g_2613) == (safe_lshift_func_uint16_t_u_s(((safe_div_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((safe_mod_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(((*l_2636) = (**g_1426)), ((l_2705 != l_2706[1][9]) , l_2138[l_2595][(g_428 + 6)][l_2595]))), 2UL)), (****g_2494))), l_2629)) == (**g_1379)), 13)))), 9)))));
                for (g_29 = 0; (g_29 <= 1); g_29 += 1)
                { /* block id: 1204 */
                    uint16_t l_2708 = 0x9BE6L;
                    uint64_t l_2715 = 0x4E31F368AB13BBE7LL;
                    uint16_t *l_2720 = (void*)0;
                    uint16_t *l_2721 = (void*)0;
                    uint16_t *l_2722 = (void*)0;
                    uint16_t *l_2723 = &l_2188;
                    int32_t l_2733 = 0x272C0C72L;
                    int16_t l_2751 = 0x0FF9L;
                    int32_t l_2752[1][9];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 9; j++)
                            l_2752[i][j] = 0xA79604E5L;
                    }
                    for (l_1871 = 1; (l_1871 <= 7); l_1871 += 1)
                    { /* block id: 1207 */
                        int i, j;
                        ++l_2708;
                    }
                    l_2727 |= (((~l_2138[(g_29 + 1)][l_2595][(g_29 + 1)]) , ((((((!((safe_add_func_int16_t_s_s((l_2715 = (*l_2374)), ((*l_2723) = ((***g_1673) && (0x51F50913L != (safe_add_func_int32_t_s_s((l_2719 = (l_2718[1][2][2] = (**g_100))), 2L))))))) || (l_2719 < ((*l_2723) = (+((g_2725 = &g_180) != (g_2726[1][0][0] = &g_180))))))) < (*g_2543)) <= 251UL) | 0x7FFEC3E8F36E4110LL) <= (*g_2253)) != (*l_2374))) <= (*l_2374));
                    if (l_2719)
                    { /* block id: 1218 */
                        uint32_t l_2728[8] = {8UL,8UL,8UL,8UL,8UL,8UL,8UL,8UL};
                        int i;
                        (****l_2477) = l_2728[3];
                        if (l_2629)
                            continue;
                    }
                    else
                    { /* block id: 1221 */
                        uint8_t l_2734 = 250UL;
                        uint32_t **l_2741 = &g_2371;
                        uint32_t **l_2742 = &g_2372;
                        int32_t l_2744 = 0xE281CEB1L;
                        int32_t l_2745 = 0xBFC7A88EL;
                        int32_t l_2746[4][4][5] = {{{0xD424474DL,0xA39B1151L,3L,(-1L),5L},{0x5E41E54BL,(-1L),(-1L),0x5E41E54BL,0xD424474DL},{0x5E41E54BL,1L,(-9L),1L,1L},{0xD424474DL,0xB4FD4EB3L,0x757EA8ACL,3L,0xB9C286DCL}},{{0xB4FD4EB3L,0xD424474DL,(-1L),1L,3L},{1L,0x5E41E54BL,(-1L),0x5E41E54BL,1L},{(-1L),0x5E41E54BL,0xD424474DL,(-1L),0xA39B1151L},{0xA39B1151L,0xD424474DL,3L,1L,(-8L)}},{{0x5E41E54BL,0xB4FD4EB3L,0xDB611BC7L,0x5E41E54BL,0xA39B1151L},{0xFCA22B19L,1L,0x757EA8ACL,0x757EA8ACL,1L},{0xA39B1151L,(-1L),0x757EA8ACL,0xB9C286DCL,3L},{0xB4FD4EB3L,0xA39B1151L,0xDB611BC7L,1L,0xB9C286DCL}},{{(-1L),0x5E41E54BL,3L,0xFCA22B19L,1L},{0xB4FD4EB3L,0xFCA22B19L,0xD424474DL,1L,0xD424474DL},{0xA39B1151L,0xA39B1151L,(-1L),1L,5L},{0xFCA22B19L,0xB4FD4EB3L,(-1L),0xFCA22B19L,0xA39B1151L}}};
                        int i, j, k;
                        (*l_2374) = (((safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_s((-5L), 5)), (((((++l_2734) , l_2138[l_2595][(g_428 + 6)][l_2595]) || (safe_mul_func_uint8_t_u_u(((safe_unary_minus_func_int8_t_s(((***g_2665) = ((void*)0 == (*g_1426))))) < 0xEA4C0FC31B998AC6LL), ((void*)0 != l_2740)))) , l_2741) == (g_2743[1][5][3] = l_2742)))) < l_2718[0][3][3]) == 4294967287UL);
                        l_2756++;
                    }
                    (*l_2144) |= ((safe_rshift_func_uint8_t_u_u((l_2138[l_2595][(g_428 + 6)][l_2595] , 0x73L), l_2761)) && (safe_add_func_uint64_t_u_u(g_2764, (safe_sub_func_int8_t_s_s((***g_2665), ((0x23L < (g_2769 , (((safe_mul_func_int16_t_s_s(((((*g_2253) &= ((void*)0 == l_2772)) || (*g_2253)) > 0x5537B845L), l_2752[0][3])) && (*g_227)) >= 0L))) != 1UL))))));
                }
            }
        }
        for (l_2749 = 0; (l_2749 <= 2); l_2749 += 1)
        { /* block id: 1235 */
            const uint32_t l_2783[7] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
            int32_t l_2788 = (-4L);
            int32_t l_2789 = 0x8D04F25EL;
            int i;
            (**g_849) = (safe_mod_func_int64_t_s_s((((safe_div_func_int64_t_s_s((((safe_mod_func_uint8_t_u_u(((((void*)0 != &g_1290[4]) ^ ((**g_1426) , (((safe_mul_func_int16_t_s_s((((*l_2317) <= l_2748) < (*l_2374)), (safe_add_func_uint8_t_u_u((l_2783[5] == (*g_427)), (l_2789 |= ((l_2788 = (safe_add_func_int8_t_s_s(((((safe_lshift_func_int8_t_s_u((-3L), l_2783[3])) , (*g_2253)) ^ (*g_2253)) && 0xC438L), 0x79L))) == 0UL)))))) && g_1095[0]) ^ 255UL))) ^ (***g_2495)), 0x2EL)) , (*g_2253)) > (*l_2146)), (*l_2317))) ^ 65535UL) > (*g_227)), l_2747));
            if ((**g_375))
                break;
            return l_2788;
        }
        for (l_2756 = 0; (l_2756 <= 2); l_2756 += 1)
        { /* block id: 1244 */
            int64_t l_2790 = 0L;
            int32_t l_2791 = 0xC4C9596DL;
            int32_t l_2792 = 2L;
            int32_t l_2793 = 1L;
            int32_t l_2794 = 0xC290F230L;
            int32_t l_2795 = 0x5D945FBFL;
            int32_t l_2796 = 1L;
            int32_t l_2797 = 0x875E93A4L;
            int32_t l_2798 = (-4L);
            int32_t l_2799 = 1L;
            int16_t l_2801 = (-1L);
            int32_t l_2802 = 5L;
            int32_t l_2803 = 0xE1E1BD22L;
            int32_t l_2805 = (-6L);
            int32_t l_2806 = 0x19A4F71BL;
            int32_t l_2813 = 0x4916F74FL;
            int32_t l_2815 = 0xED900EABL;
            int32_t l_2816 = 1L;
            int32_t l_2817 = 4L;
            int32_t l_2818 = 0x73BB9EFFL;
            uint8_t l_2820 = 0x9DL;
            int32_t *l_2831[8][7][4] = {{{&g_4,&g_7,&l_2814[0][2],&g_11},{&l_2797,&l_2792,&l_2800,(void*)0},{(void*)0,&l_2814[0][2],&l_2796,&l_2816},{&l_2814[1][3],(void*)0,&l_2753,&l_1871},{(void*)0,&l_2792,(void*)0,(void*)0},{&l_1871,&l_2816,&l_2800,(void*)0},{(void*)0,&g_4,(void*)0,&l_2802}},{{(void*)0,(void*)0,(void*)0,&l_2802},{&l_2796,&l_2807,&l_2817,(void*)0},{&l_2802,&l_2814[1][3],&l_2817,&l_2803},{&l_2796,&l_2814[0][2],(void*)0,&l_2807},{(void*)0,&l_2806,(void*)0,&l_2805},{(void*)0,&l_2805,&l_2800,(void*)0},{&l_1871,(void*)0,(void*)0,&l_2798}},{{&l_2238[7][1][2],&l_2792,&l_2803,&g_29},{&l_2797,&l_2814[0][2],(void*)0,&l_2817},{&l_2806,(void*)0,&g_7,&l_2798},{&l_2753,&l_2804,&l_2802,&l_2802},{&l_2816,&l_2805,&l_2792,&l_2800},{&l_2238[7][1][2],&l_2804,&l_1872,&l_2807},{&l_2813,&l_2816,&l_2804,&l_2806}},{{&g_428,&l_2814[1][3],(void*)0,(void*)0},{&g_11,&l_2798,&l_2796,&l_2802},{&l_2813,(void*)0,&l_2806,&l_2238[7][1][2]},{&l_2802,&g_4,&l_2792,&l_2792},{&l_2814[0][2],&l_2814[0][2],&l_1856,(void*)0},{&l_2753,&l_2793,(void*)0,(void*)0},{&g_29,&l_2238[7][1][2],(void*)0,(void*)0}},{{&g_4,&l_2238[7][1][2],&l_2792,(void*)0},{&l_2238[7][1][2],&l_2793,&g_163,(void*)0},{&l_2794,&l_2814[0][2],&l_2800,&l_2792},{&l_2802,&g_4,&g_7,&l_2238[7][1][2]},{(void*)0,(void*)0,&l_2807,&l_2802},{&g_2769,&l_2798,&l_2817,(void*)0},{&l_2238[7][1][2],&l_2814[1][3],(void*)0,&l_2806}},{{&l_2796,&l_2816,&l_2807,&l_2807},{&l_2803,&l_2804,(void*)0,&l_2800},{&l_2802,&l_2805,&l_2816,&l_2802},{&l_1871,&l_2804,&g_163,&l_2798},{&l_2802,(void*)0,&l_2803,&l_2817},{&g_4,&l_2814[0][2],&l_2797,&g_29},{&l_2806,&l_2792,(void*)0,&l_2798}},{{&g_29,(void*)0,&l_2802,(void*)0},{&l_2814[0][2],&l_2805,&l_2809,&l_2805},{&l_2238[7][1][2],&l_2806,&l_2806,&l_2807},{&l_2792,&l_2814[0][2],&l_2804,&l_2803},{&g_11,&l_2814[1][3],&g_7,(void*)0},{&g_11,&l_2807,&l_2804,&l_2802},{&l_2792,(void*)0,&l_2806,&l_2802}},{{&l_2238[7][1][2],&g_4,&l_2809,(void*)0},{&l_2814[0][2],&l_2816,&l_2802,(void*)0},{&g_29,&l_2813,(void*)0,&l_2804},{&l_2792,&g_428,&l_2753,&l_2817},{&l_2807,&l_2813,(void*)0,&l_2803},{&l_2813,&l_2792,&l_2810,&l_1856},{&l_2804,&l_2802,&l_2802,&l_1856}}};
            int32_t l_2843[8] = {0xCE10137DL,(-7L),(-7L),0xCE10137DL,(-7L),(-7L),0xCE10137DL,(-7L)};
            uint64_t l_2855 = 0x19E063A408762829LL;
            int32_t l_2869 = 0x9514C21AL;
            int8_t * const **l_2895 = (void*)0;
            int8_t * const ***l_2894 = &l_2895;
            int8_t * const ****l_2893 = &l_2894;
            uint64_t l_2911 = 0UL;
            int32_t l_2912 = (-3L);
            uint64_t l_2936 = 0xC4AD54ECA94F456ALL;
            int32_t l_2966[1];
            uint16_t l_2967 = 0x19CAL;
            uint32_t l_2983[5][5] = {{0UL,0xA01C60B2L,0xFF88AD89L,0xA01C60B2L,0UL},{1UL,0xA01C60B2L,0x251A7806L,0UL,0x251A7806L},{0x251A7806L,0x251A7806L,0xFF88AD89L,0UL,0xF2A19E50L},{0xA01C60B2L,1UL,1UL,0xA01C60B2L,0x251A7806L},{0xA01C60B2L,0UL,0x5373DBA5L,0x5373DBA5L,0UL}};
            int8_t l_3080 = 5L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_2966[i] = (-1L);
            l_2820--;
            l_2832 = (l_2831[6][5][3] = (((safe_lshift_func_int16_t_s_s((((l_2825 ^ l_2809) != (((safe_mod_func_uint16_t_u_u(((((****g_1907) , 0x3.71A43Bp-35) , (void*)0) != &g_2371), ((safe_rshift_func_int8_t_s_u(l_2830, ((*g_2543) == 0x8EL))) && (*g_2372)))) && (*g_1380)) == l_2825)) || 0xE6861F46A37A3350LL), 10)) , (*l_2144)) , &l_2792));
            for (l_1851 = 7; (l_1851 >= 0); l_1851 -= 1)
            { /* block id: 1250 */
                int32_t *****l_2854 = &l_2306[1][0];
                int32_t l_2862 = 0x4EB79158L;
                int32_t l_2875 = 0L;
                uint32_t l_2876 = 0x2B3E4322L;
                int32_t ***l_2879 = (void*)0;
                int32_t *l_2880 = (void*)0;
            }
        }
        for (l_2965 = 0; (l_2965 <= 7); l_2965 += 1)
        { /* block id: 1362 */
            return (*l_2144);
        }
    }
    (*g_2320) = func_50((*l_2200), ((+((safe_mul_func_float_f_f(((**g_2676) = (safe_div_func_float_f_f((-0x8.3p-1), (+0x9.4C4E4Ap+54)))), (((((((*l_2374) != (*g_1716)) <= (((safe_sub_func_float_f_f((((((****l_2476) == ((*l_3099) = ((*l_3097) = (*g_2676)))) , &g_2239[1]) == &g_183[4]) , (*l_2317)), (*l_2146))) <= (-0x1.Ap+1)) < 0x2.F923C3p-25)) <= l_3101) >= (*l_2374)) >= (*l_2374)) >= (*l_2374)))) < (*l_2317))) <= (*l_2374)), l_3102[0][2], (*g_859));
    return l_3103;
}


/* ------------------------------------------ */
/* 
 * reads : g_7 g_2 g_29 g_78 g_91 g_41 g_100 g_101 g_10 g_132 g_97 g_11 g_4 g_297 g_375 g_679 g_1236 g_1237 g_227 g_423 g_424 g_305 g_1290 g_428 g_1313 g_1095 g_850 g_1379 g_470 g_125 g_849 g_174 g_183 g_427 g_311 g_1426 g_1180 g_1387 g_180 g_667 g_1164 g_414 g_253 g_1216 g_163 g_1427 g_1428 g_858 g_859 g_1380 g_1619 g_1672 g_1716 g_1386 g_465 g_249 g_1773 g_1788
 * writes: g_28 g_41 g_91 g_93 g_97 g_29 g_101 g_146 g_253 g_163 g_679 g_174 g_1290 g_428 g_180 g_1237 g_305 g_414 g_1379 g_470 g_1387 g_78 g_311 g_1216 g_183 g_1619 g_135 g_249 g_487 g_1386 g_856 g_125 g_1239 g_227 g_667
 */
static uint64_t  func_14(uint32_t  p_15, int32_t * p_16)
{ /* block id: 13 */
    uint32_t l_18[3];
    const int32_t *l_25 = &g_4;
    int32_t *l_27 = (void*)0;
    int32_t **l_26[7] = {&l_27,(void*)0,(void*)0,&l_27,(void*)0,(void*)0,&l_27};
    uint32_t *l_40 = &g_41;
    uint64_t *l_1826 = &g_305;
    uint16_t *l_1831 = &g_146;
    uint16_t l_1836[2][6][2];
    uint8_t *l_1837 = &g_1216[0][5][0];
    uint8_t *l_1838 = &g_667;
    int64_t l_1841[10][5][5] = {{{3L,0x2AEADA85C3281FDDLL,0xDE6109B7A0608088LL,0x709A4C524D08ED4DLL,(-3L)},{6L,0x56A718360B6A0BDDLL,0x35143E27273787ECLL,0x02D2BE90AF3ABBB3LL,0x709A4C524D08ED4DLL},{1L,0xE512882076C4B046LL,0xA2A70D7F7999AA1ALL,6L,0x9A75F2120FE9C90ELL},{0x35143E27273787ECLL,0xE512882076C4B046LL,3L,0x846E21178F77B58DLL,0L},{0L,0x56A718360B6A0BDDLL,(-7L),0x9A75F2120FE9C90ELL,0x0651B5FC339A56D8LL}},{{0x89BA9190A1666D63LL,0x2AEADA85C3281FDDLL,(-1L),6L,0L},{0L,(-3L),0xDE6109B7A0608088LL,0xABFE4C8720C88244LL,0x9A75F2120FE9C90ELL},{0L,1L,0xDE6109B7A0608088LL,(-3L),0x709A4C524D08ED4DLL},{6L,(-5L),(-1L),0x02D2BE90AF3ABBB3LL,(-3L)},{4L,0xE512882076C4B046LL,(-7L),2L,0x9A75F2120FE9C90ELL}},{{6L,1L,3L,0x9A75F2120FE9C90ELL,0xD92349EEB908F8A0LL},{0L,(-5L),0xA2A70D7F7999AA1ALL,0x9A75F2120FE9C90ELL,0x98FEC4DB580745ECLL},{0L,0x2AEADA85C3281FDDLL,0x35143E27273787ECLL,2L,0L},{0x89BA9190A1666D63LL,(-1L),0xDE6109B7A0608088LL,0x02D2BE90AF3ABBB3LL,0x846E21178F77B58DLL},{0L,0x2AEADA85C3281FDDLL,(-7L),(-3L),(-3L)}},{{0x35143E27273787ECLL,(-5L),0x35143E27273787ECLL,0xABFE4C8720C88244LL,(-3L)},{1L,1L,(-7L),6L,0x846E21178F77B58DLL},{6L,0xE512882076C4B046LL,0L,0x9A75F2120FE9C90ELL,0L},{3L,(-5L),(-7L),0x846E21178F77B58DLL,0x98FEC4DB580745ECLL},{0x89BA9190A1666D63LL,1L,0x35143E27273787ECLL,6L,0xD92349EEB908F8A0LL}},{{0x89BA9190A1666D63LL,(-3L),(-7L),0x02D2BE90AF3ABBB3LL,0x9A75F2120FE9C90ELL},{3L,0x2AEADA85C3281FDDLL,0xDE6109B7A0608088LL,0x709A4C524D08ED4DLL,(-3L)},{6L,0x56A718360B6A0BDDLL,0x35143E27273787ECLL,0x02D2BE90AF3ABBB3LL,0x709A4C524D08ED4DLL},{1L,0xE512882076C4B046LL,0xA2A70D7F7999AA1ALL,6L,0x9A75F2120FE9C90ELL},{0x35143E27273787ECLL,0xE512882076C4B046LL,3L,0x846E21178F77B58DLL,0L}},{{0L,0x56A718360B6A0BDDLL,(-7L),0x9A75F2120FE9C90ELL,0x0651B5FC339A56D8LL},{0x89BA9190A1666D63LL,0x2AEADA85C3281FDDLL,(-1L),6L,0L},{(-1L),0x62D51A1A5D062904LL,0x35E24B61AB00877FLL,0xA2A70D7F7999AA1ALL,0xDE6109B7A0608088LL},{0xC4D19A0317C84871LL,0x078BD756B9D4B43FLL,0x35E24B61AB00877FLL,3L,0L},{0x3F35AC2B53F2F364LL,3L,0L,(-7L),3L}},{{3L,1L,(-5L),(-1L),0xDE6109B7A0608088LL},{0x3F35AC2B53F2F364LL,0x78F5BD2AB1F37C8CLL,0x11EE3F448B4CFAAFLL,0xDE6109B7A0608088LL,0x710904B1728434FFLL},{0xC4D19A0317C84871LL,3L,(-10L),0xDE6109B7A0608088LL,0L},{(-1L),0xB52C204A4D590AB6LL,0x5F1CE615DC7D38F1LL,(-1L),0xCC4205482A9D047CLL},{0L,0x42AB5F56A0520056LL,0x35E24B61AB00877FLL,(-7L),(-7L)}},{{0xC4D19A0317C84871LL,0xB52C204A4D590AB6LL,(-8L),3L,3L},{0x5F1CE615DC7D38F1LL,3L,0x5F1CE615DC7D38F1LL,0xA2A70D7F7999AA1ALL,3L},{0L,0x78F5BD2AB1F37C8CLL,(-5L),0x35143E27273787ECLL,(-7L)},{0x3F35AC2B53F2F364LL,1L,1L,0xDE6109B7A0608088LL,0xCC4205482A9D047CLL},{0x11EE3F448B4CFAAFLL,3L,(-5L),(-7L),0L}},{{0L,0x078BD756B9D4B43FLL,0x5F1CE615DC7D38F1LL,0x35143E27273787ECLL,0x710904B1728434FFLL},{0L,0x62D51A1A5D062904LL,(-8L),(-7L),0xDE6109B7A0608088LL},{0x11EE3F448B4CFAAFLL,0xB52C204A4D590AB6LL,0x35E24B61AB00877FLL,0L,3L},{0x3F35AC2B53F2F364LL,0x61364312B1D4AB3BLL,0x5F1CE615DC7D38F1LL,(-7L),0L},{0L,1L,(-10L),0x35143E27273787ECLL,0xDE6109B7A0608088LL}},{{0x5F1CE615DC7D38F1LL,1L,0x11EE3F448B4CFAAFLL,(-7L),0xCC4205482A9D047CLL},{0xC4D19A0317C84871LL,0x61364312B1D4AB3BLL,(-5L),0xDE6109B7A0608088LL,0xDF9BF6E0132010B6LL},{0L,0xB52C204A4D590AB6LL,0L,0x35143E27273787ECLL,0xCC4205482A9D047CLL},{(-1L),0x62D51A1A5D062904LL,0x35E24B61AB00877FLL,0xA2A70D7F7999AA1ALL,0xDE6109B7A0608088LL},{0xC4D19A0317C84871LL,0x078BD756B9D4B43FLL,0x35E24B61AB00877FLL,3L,0L}}};
    uint16_t l_1842 = 0x36EEL;
    float l_1843 = (-0x5.2p+1);
    int32_t l_1844 = (-1L);
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_18[i] = 18446744073709551615UL;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 2; k++)
                l_1836[i][j][k] = 65529UL;
        }
    }
    for (p_15 = 0; (p_15 <= 2); p_15 += 1)
    { /* block id: 16 */
        int i;
        return l_18[p_15];
    }
    (**g_849) = (func_19(p_16, (func_22(l_25, ((((g_28 = (void*)0) == ((safe_mul_func_int8_t_s_s(((safe_rshift_func_int16_t_s_u(g_7, (((0x21494FB3L < func_34(&l_27, g_2, ((*l_40) = p_15), (p_15 , g_7), p_16)) || (*p_16)) && p_15))) || p_15), p_15)) , (void*)0)) , 9L) , p_16)) == 0x9AL)) & (*g_859));
    (**g_849) = 1L;
    l_1844 &= (((((((safe_div_func_float_f_f((safe_sub_func_float_f_f(((void*)0 == l_1826), (safe_sub_func_float_f_f(((p_15 != ((1UL < (l_1831 != ((((*l_1826) = (((safe_rshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_u(((*l_1838) = ((*l_1837) |= l_1836[0][4][1])), ((safe_add_func_int16_t_s_s((*l_25), ((*l_1831) = (*l_25)))) < p_15))), 3)) , (-1L)) > (*l_25))) > (*l_25)) , (void*)0))) , (-0x8.1p+1))) >= (**g_424)), (*l_25))))), 0x0.8p-1)) <= p_15) , l_1841[7][0][0]) ^ 7L) , 0x7DL) <= 1L) != l_1842);
    return (*l_25);
}


/* ------------------------------------------ */
/* 
 * reads : g_667 g_11
 * writes: g_667
 */
static int32_t  func_19(int32_t * p_20, int32_t  p_21)
{ /* block id: 763 */
    int32_t l_1805 = 0xC64FC9D3L;
    int32_t l_1807 = 0xDEFEEDACL;
    int32_t l_1808 = 0L;
    int32_t l_1809 = 0x53C33F6CL;
    int32_t l_1810 = 0L;
    int32_t l_1811 = 0x94A0C591L;
    int32_t l_1812 = 5L;
    int32_t l_1813 = 0x92A4C375L;
    int32_t l_1814[5];
    int8_t l_1815[9] = {0x1AL,0x1AL,0x74L,0x1AL,0x1AL,0x74L,0x1AL,0x1AL,0x74L};
    int16_t l_1817 = 0xB298L;
    uint32_t l_1818 = 0xEBC7D5DEL;
    int i;
    for (i = 0; i < 5; i++)
        l_1814[i] = 1L;
    if (((+p_21) == p_21))
    { /* block id: 764 */
        uint32_t l_1792 = 0xAEAFB0B7L;
        int32_t l_1793 = 0x47F67E96L;
        int32_t *l_1794 = (void*)0;
        int32_t *l_1795 = (void*)0;
        int32_t *l_1796 = &g_428;
        int32_t *l_1797 = &g_29;
        int32_t *l_1798 = &g_29;
        int32_t *l_1799 = &g_428;
        int32_t *l_1800 = &l_1793;
        int32_t *l_1801 = &g_29;
        int32_t l_1802 = (-4L);
        int32_t *l_1803 = &g_163;
        int32_t *l_1804 = &g_428;
        int32_t *l_1806[1][5] = {{&l_1793,&l_1793,&l_1793,&l_1793,&l_1793}};
        int32_t l_1816 = 0x2E715ABFL;
        int i, j;
        for (g_667 = 0; (g_667 > 52); ++g_667)
        { /* block id: 767 */
            return l_1792;
        }
        --l_1818;
    }
    else
    { /* block id: 771 */
        float l_1821 = 0x0.7p-1;
        return (*p_20);
    }
    return l_1809;
}


/* ------------------------------------------ */
/* 
 * reads : g_849 g_850 g_414 g_1773 g_1237 g_227 g_249 g_163 g_1386 g_1788 g_1236 g_174
 * writes: g_227 g_41 g_305 g_101
 */
static int8_t  func_22(const int32_t * p_23, int32_t * p_24)
{ /* block id: 754 */
    int16_t ***l_1765 = &g_1619;
    int32_t l_1770 = (-1L);
    int64_t *l_1776 = &g_465;
    float **l_1781 = (void*)0;
    float ***l_1780 = &l_1781;
    float ****l_1779 = &l_1780;
    float *****l_1778 = &l_1779;
    uint32_t *l_1782 = &g_41;
    int64_t l_1783 = 1L;
    uint64_t *l_1784 = &g_305;
    int8_t *l_1785[10][10][2] = {{{&g_174[7],&g_253},{&g_174[7],&g_174[7]},{(void*)0,&g_253},{&g_174[2],&g_253},{(void*)0,&g_174[7]},{&g_174[7],&g_253},{&g_174[7],&g_253},{&g_174[7],&g_174[7]},{(void*)0,&g_253},{&g_174[2],&g_253}},{{(void*)0,&g_174[7]},{&g_174[7],&g_253},{&g_174[7],&g_253},{&g_174[7],&g_174[7]},{(void*)0,&g_253},{&g_174[2],&g_253},{(void*)0,&g_174[7]},{&g_174[7],&g_253},{&g_174[7],&g_253},{&g_174[7],&g_174[7]}},{{(void*)0,&g_253},{&g_174[2],&g_253},{(void*)0,&g_174[7]},{&g_174[7],&g_253},{&g_174[7],&g_253},{&g_174[7],&g_174[7]},{(void*)0,&g_253},{&g_174[2],&g_253},{(void*)0,&g_174[7]},{&g_174[7],&g_253}},{{&g_174[7],&g_253},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]}},{{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]}},{{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]}},{{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]}},{{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]}},{{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]}},{{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]},{&g_174[7],&g_174[2]},{&g_174[2],&g_174[7]},{&g_174[7],&g_174[7]},{&g_174[2],&g_174[2]},{&g_174[7],&g_174[7]},{&g_253,&g_174[7]}}};
    int32_t l_1786 = (-1L);
    int32_t l_1787 = (-10L);
    int i, j, k;
    l_1787 ^= (safe_mod_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((((safe_mod_func_int8_t_s_s((l_1786 = ((safe_lshift_func_int8_t_s_u((safe_div_func_uint64_t_u_u((l_1765 != l_1765), ((safe_mod_func_int32_t_s_s((**g_849), (safe_add_func_uint16_t_u_u((((*l_1784) = ((l_1770 = 0UL) || (safe_sub_func_uint64_t_u_u((g_1773 != &g_1386), (safe_mul_func_int8_t_s_s((((*l_1782) = (l_1776 != (((~(((((((((*g_1237) = (*g_1237)) != &g_1180) , l_1770) && 4L) <= l_1770) , (void*)0) == l_1778) & l_1770)) | l_1770) , &g_183[4]))) != 0xF69F8B0BL), l_1783)))))) <= 0x6D7917C271225A67LL), g_249[3])))) | 0UL))), l_1783)) && l_1783)), g_163)) || g_1386) ^ 0xB4L), 3)) < 0L), l_1783));
    (*g_1788) = p_24;
    return (***g_1236);
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_78 g_7 g_91 g_41 g_100 g_101 g_10 g_132 g_97 g_11 g_4 g_297 g_375 g_679 g_1236 g_1237 g_227 g_423 g_424 g_305 g_1290 g_428 g_1313 g_1095 g_850 g_1379 g_470 g_125 g_849 g_174 g_183 g_427 g_311 g_1426 g_1180 g_1387 g_180 g_667 g_1164 g_414 g_253 g_1216 g_163 g_1427 g_1428 g_858 g_859 g_1380 g_1619 g_1672 g_1716 g_1386 g_465 g_249
 * writes: g_91 g_93 g_97 g_41 g_29 g_101 g_146 g_253 g_163 g_679 g_174 g_1290 g_428 g_180 g_1237 g_305 g_414 g_1379 g_470 g_1387 g_78 g_311 g_1216 g_183 g_1619 g_135 g_249 g_487 g_1386 g_856 g_125 g_1239
 */
static const uint32_t  func_34(int32_t ** p_35, const int64_t  p_36, uint32_t  p_37, int32_t  p_38, const int32_t * p_39)
{ /* block id: 21 */
    uint32_t *l_60[5][4][6] = {{{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{&g_41,&g_41,&g_41,&g_41,&g_41,(void*)0},{&g_41,&g_41,&g_41,&g_41,&g_41,(void*)0}},{{&g_41,&g_41,(void*)0,(void*)0,&g_41,&g_41},{&g_41,&g_41,&g_41,(void*)0,(void*)0,&g_41},{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{&g_41,&g_41,&g_41,(void*)0,&g_41,&g_41}},{{&g_41,(void*)0,&g_41,(void*)0,&g_41,&g_41},{(void*)0,&g_41,&g_41,(void*)0,&g_41,&g_41},{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{&g_41,(void*)0,&g_41,&g_41,&g_41,&g_41}},{{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{&g_41,&g_41,(void*)0,&g_41,&g_41,&g_41},{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{(void*)0,&g_41,&g_41,(void*)0,&g_41,(void*)0}},{{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41},{&g_41,(void*)0,&g_41,(void*)0,&g_41,&g_41},{(void*)0,&g_41,(void*)0,(void*)0,&g_41,&g_41},{&g_41,&g_41,&g_41,&g_41,&g_41,&g_41}}};
    int32_t l_67[8] = {(-2L),(-2L),(-2L),(-2L),(-2L),(-2L),(-2L),(-2L)};
    uint32_t l_68[9][3] = {{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL},{4294967295UL,4294967295UL,0UL}};
    uint32_t *l_1295[10][2] = {{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125},{&g_125,&g_125}};
    uint32_t **l_1294 = &l_1295[8][0];
    int32_t *l_1297 = &g_11;
    int32_t **l_1296 = &l_1297;
    int32_t ***l_1298[7][3] = {{(void*)0,&l_1296,&l_1296},{(void*)0,&l_1296,&g_180},{(void*)0,&l_1296,(void*)0},{&l_1296,(void*)0,&g_180},{&g_180,&g_180,&l_1296},{&g_180,(void*)0,(void*)0},{&l_1296,&l_1296,&g_180}};
    uint16_t l_1613 = 0UL;
    float l_1740 = 0x1.Ap+1;
    int i, j, k;
    if ((safe_div_func_int16_t_s_s(func_44(func_50(((*l_1294) = func_55(l_60[1][1][3], ((*l_1294) = func_61(&g_11, l_60[1][1][3], (safe_add_func_int16_t_s_s((l_67[0] , p_38), l_68[4][2])))), p_36, (g_180 = l_1296))), p_37, (*p_35), (*p_39)), &l_68[4][0], p_38, p_36, p_36), l_1613)))
    { /* block id: 700 */
        int32_t *l_1614 = &l_67[0];
        (*l_1296) = l_1614;
        (*l_1296) = (*g_375);
    }
    else
    { /* block id: 703 */
        int32_t l_1626 = 0xB18FDD3FL;
        int32_t **l_1669 = &g_101;
        int8_t ***l_1671 = &g_1237;
        int8_t ****l_1670 = &l_1671;
        int64_t l_1675 = 0xF789A48B0E5A4C93LL;
        uint8_t l_1695 = 3UL;
        uint16_t l_1702[7] = {0xC7C0L,65535UL,0xC7C0L,0xC7C0L,65535UL,0xC7C0L,0xC7C0L};
        uint32_t l_1751 = 0xCD390935L;
        int i;
        for (g_311 = 0; (g_311 > 27); ++g_311)
        { /* block id: 706 */
            int16_t ***l_1621 = &g_1619;
            int16_t ***l_1622 = (void*)0;
            int16_t **l_1624 = (void*)0;
            int16_t ***l_1623 = &l_1624;
            float *l_1635 = &g_91[3];
            int32_t l_1636 = 0x8327AF01L;
            float *l_1637 = &g_135[2][0][1];
            uint32_t l_1686 = 0xEF4226E7L;
            uint8_t l_1714 = 247UL;
            int32_t l_1749[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
            int i;
            if ((((((safe_div_func_int8_t_s_s((((*l_1621) = g_1619) == ((*l_1623) = (void*)0)), (~(p_37 < 0x28L)))) , ((*l_1637) = (l_1626 != (p_37 <= (safe_mul_func_float_f_f(((*g_97) = (safe_sub_func_float_f_f((***g_423), ((safe_sub_func_float_f_f((((*l_1635) = (safe_mul_func_float_f_f((-0x8.2p+1), p_36))) != p_36), l_1626)) > l_1636)))), p_38)))))) , (*g_227)) | 0xECL) , 0xB79FE5D5L))
            { /* block id: 712 */
                int16_t l_1660[5];
                uint64_t l_1676 = 0x60F72433094CBF93LL;
                int i;
                for (i = 0; i < 5; i++)
                    l_1660[i] = 0xFB08L;
                for (l_1613 = 21; (l_1613 <= 16); --l_1613)
                { /* block id: 715 */
                    uint16_t *l_1653 = &g_249[3];
                    int64_t *l_1654 = (void*)0;
                    int64_t *l_1655 = &g_183[4];
                    int32_t * const *l_1667[8][10][1] = {{{&l_1297},{&l_1297},{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&l_1297}},{{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&l_1297}},{{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&l_1297},{&l_1297},{&g_101}},{{&l_1297},{&g_101},{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&g_101}},{{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&g_101}},{{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&g_101},{&l_1297},{&l_1297}},{{&l_1297},{&g_101},{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&g_101}},{{&l_1297},{&l_1297},{&l_1297},{&g_101},{&l_1297},{&g_101},{&l_1297},{&l_1297},{&l_1297},{&g_101}}};
                    int32_t * const **l_1668 = &g_487[1];
                    int i, j, k;
                    l_1675 = (safe_div_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((+((safe_add_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((safe_div_func_uint64_t_u_u((l_1636 , 1UL), ((*l_1655) = (safe_mul_func_uint16_t_u_u(65527UL, ((*l_1653) = g_78[2])))))), (safe_rshift_func_int8_t_s_u((safe_sub_func_uint16_t_u_u(0xDBDBL, p_36)), l_1660[3])))), ((((9L > (safe_add_func_int16_t_s_s(((((safe_div_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(((((*l_1668) = l_1667[3][3][0]) == l_1669) | p_38), p_37)) , 0x43465803L), l_1660[3])) ^ l_1660[3]) , l_1670) != g_1672), g_470))) , 0x1E0DL) || g_125) >= l_1636))) >= p_36)) >= p_37), l_1636)), 255UL));
                    ++l_1676;
                    if ((*p_39))
                        break;
                }
                if ((**g_849))
                    break;
            }
            else
            { /* block id: 724 */
                int8_t l_1679 = 0x0EL;
                uint64_t l_1690 = 0x3433A6E08E62049CLL;
                int32_t *l_1696 = &g_1386;
                int32_t l_1697 = 0x2CC7F1D7L;
                l_1636 ^= l_1679;
                l_1697 = ((((g_41 = (safe_div_func_int64_t_s_s((((*l_1696) = ((safe_add_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(p_37, (p_37 , (((l_1636 == ((p_36 ^ l_1686) , p_37)) >= 0xD214D2AAL) , ((+((safe_rshift_func_int8_t_s_s(l_1690, 2)) , (safe_rshift_func_int8_t_s_u((safe_mod_func_int32_t_s_s(l_1679, l_1695)), l_1679)))) & l_1686))))), 0xD2L)) , 0xD072D3CBL)) , p_37), p_36))) >= 0x08EA6513L) != g_428) && p_38);
            }
            (*g_1716) = (safe_sub_func_float_f_f((((*l_1635) = (safe_mul_func_float_f_f(l_1702[3], (safe_add_func_float_f_f((safe_sub_func_float_f_f(((safe_sub_func_float_f_f(l_1686, ((*g_97) = ((((~(safe_sub_func_int16_t_s_s((-3L), p_38))) && ((*p_39) >= 4294967294UL)) , ((*l_1637) = p_38)) >= (safe_add_func_float_f_f(l_1714, (((-(p_38 == l_1686)) < (***g_423)) == 0x8.125142p-28))))))) != 0x0.0p-1), p_37)), 0x1.Ap+1))))) < p_37), 0x8.18F013p-51));
            if (l_1695)
            { /* block id: 734 */
                uint8_t l_1730 = 0xF9L;
                uint8_t *l_1737 = &l_1730;
                uint8_t **l_1736[1][1][3];
                uint8_t ***l_1735[2];
                int32_t l_1747 = 0L;
                int i, j, k;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 3; k++)
                            l_1736[i][j][k] = &l_1737;
                    }
                }
                for (i = 0; i < 2; i++)
                    l_1735[i] = &l_1736[0][0][2];
                for (g_1386 = 0; (g_1386 <= 2); g_1386 += 1)
                { /* block id: 737 */
                    uint8_t *l_1723 = &g_1216[0][5][0];
                    uint8_t *l_1726 = (void*)0;
                    uint8_t *l_1727 = &l_1695;
                    uint8_t * const *l_1739 = (void*)0;
                    uint8_t * const **l_1738 = &l_1739;
                    uint64_t l_1741 = 0UL;
                    uint16_t *l_1742[9];
                    int32_t *l_1743[6] = {&g_1386,&g_1744,&g_1386,&g_1386,&g_1744,&g_1386};
                    int32_t l_1745 = 0xB18CE6BDL;
                    int32_t l_1746 = (-8L);
                    int32_t l_1748 = 1L;
                    int32_t l_1750 = (-10L);
                    int i;
                    for (i = 0; i < 9; i++)
                        l_1742[i] = &l_1613;
                    l_1626 |= (g_78[g_1386] && (((((***l_1671) &= (safe_sub_func_uint64_t_u_u(((((((p_38 = (safe_sub_func_int8_t_s_s((((p_38 != ((g_78[g_1386] | (g_249[3] &= (g_667 ^ (((l_1636 || (safe_lshift_func_uint8_t_u_u(((*l_1723)--), ((*l_1727)++)))) , ((**l_1294) &= (p_38 || (l_1730 < (((safe_mod_func_uint32_t_u_u(((safe_mul_func_uint8_t_u_u((((l_1735[1] != l_1738) , 0x61ACD10EL) | l_1741), g_1216[0][3][7])) > l_1730), (*p_39))) <= g_465) != 255UL))))) , l_1730)))) ^ 1UL)) , g_78[g_1386]) && g_667), p_37))) , 0x142F96FC63A0BB9FLL) < p_37) ^ (*g_859)) >= 65535UL) < l_1741), l_1730))) != l_1741) || g_414) <= p_36));
                    l_1751++;
                }
            }
            else
            { /* block id: 747 */
                float * const ****l_1754 = &g_1240;
                g_1239[7] = l_1754;
                return p_38;
            }
        }
    }
    return (*g_1380);
}


/* ------------------------------------------ */
/* 
 * reads : g_428 g_305 g_125 g_174 g_183 g_427 g_97 g_470 g_311 g_29 g_1426 g_132 g_1180 g_849 g_850 g_1387 g_180 g_227 g_667 g_1164 g_414 g_253 g_1237 g_1216 g_163 g_375 g_101 g_1313 g_1427 g_1428 g_1095 g_858 g_859 g_11 g_1236 g_1380 g_41 g_78 g_679 g_423 g_424
 * writes: g_428 g_253 g_78 g_305 g_29 g_470 g_311 g_414 g_101 g_1216 g_183 g_163 g_1237 g_146
 */
static const int16_t  func_44(uint32_t * p_45, uint32_t * const  p_46, uint32_t  p_47, int8_t  p_48, int64_t  p_49)
{ /* block id: 568 */
    int8_t l_1392 = 0x56L;
    int32_t l_1432 = 0x5F79966CL;
    int32_t l_1440 = 0xE679CCC9L;
    int32_t l_1441[4] = {0x1E563AC1L,0x1E563AC1L,0x1E563AC1L,0x1E563AC1L};
    int32_t *l_1493[9][10][2] = {{{&l_1440,&l_1441[0]},{&g_428,&l_1441[2]},{&g_163,&g_11},{&l_1432,&g_4},{&g_163,&g_4},{&l_1432,&g_11},{&g_163,&l_1441[2]},{&g_428,&l_1441[0]},{&l_1440,&g_11},{&g_11,&l_1441[0]}},{{&g_163,&l_1441[2]},{&g_163,&g_11},{&g_163,&g_4},{&g_428,&g_4},{&g_163,&g_11},{&g_163,&l_1441[2]},{&g_163,&l_1441[0]},{&g_11,&g_11},{&l_1440,&l_1441[0]},{&g_428,&l_1441[2]}},{{&g_163,&g_11},{&l_1432,&g_4},{&g_163,&g_4},{&l_1432,&g_11},{&g_163,&l_1441[2]},{&g_428,&l_1441[0]},{&l_1440,&g_11},{&g_11,&l_1441[0]},{&g_163,&l_1441[2]},{&g_163,&g_11}},{{&g_163,&g_4},{&g_428,&g_4},{&g_163,&g_11},{&g_163,&l_1441[2]},{&g_163,&l_1441[0]},{&g_11,&g_11},{&l_1440,&l_1441[0]},{&g_428,&l_1441[2]},{&g_163,&g_11},{&l_1432,&g_4}},{{&g_163,&g_4},{&l_1432,&g_11},{&g_163,&l_1441[2]},{&g_428,&l_1441[0]},{&l_1440,&g_11},{&g_11,&l_1441[0]},{&g_163,&l_1441[2]},{&g_163,&g_11},{&g_163,&g_4},{&g_428,&g_4}},{{&g_163,&g_11},{&g_163,&l_1441[2]},{&g_163,&l_1441[0]},{&g_11,&g_11},{&l_1440,&l_1441[0]},{&g_428,&l_1441[2]},{&g_163,&g_11},{&l_1432,&g_4},{&g_163,&g_4},{&l_1432,&g_11}},{{&g_163,&l_1441[2]},{&g_428,&l_1441[0]},{&l_1440,&g_11},{&g_11,&l_1441[0]},{&g_163,&l_1441[2]},{&g_163,&g_11},{&g_163,&l_1441[0]},{&g_11,&l_1441[0]},{&g_163,&g_7},{(void*)0,&g_428}},{{&l_1440,&g_11},{&l_1432,&g_7},{&l_1441[0],&g_11},{&g_11,&g_428},{&g_428,&g_7},{&g_428,&l_1441[0]},{&l_1440,&l_1441[0]},{&g_428,&g_7},{&g_428,&g_428},{&g_11,&g_11}},{{&l_1441[0],&g_7},{&l_1432,&g_11},{&l_1440,&g_428},{(void*)0,&g_7},{&g_163,&l_1441[0]},{&g_11,&l_1441[0]},{&g_163,&g_7},{(void*)0,&g_428},{&l_1440,&g_11},{&l_1432,&g_7}}};
    uint32_t l_1510 = 0x4227EC22L;
    uint16_t l_1536[5][3][1] = {{{0xF3FDL},{0x870CL},{0x4E17L}},{{0x870CL},{0xF3FDL},{65534UL}},{{65534UL},{0xF3FDL},{0x870CL}},{{0x4E17L},{0x870CL},{0xF3FDL}},{{65534UL},{65534UL},{0xF3FDL}}};
    uint8_t *l_1542 = (void*)0;
    float * const *l_1593 = &g_97;
    float * const **l_1592[3];
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1592[i] = &l_1593;
    for (g_428 = 2; (g_428 >= 0); g_428 -= 1)
    { /* block id: 571 */
        int64_t l_1396[4];
        int32_t l_1407 = 0x35F6D919L;
        float **l_1425 = &g_97;
        float ***l_1424 = &l_1425;
        float ****l_1423 = &l_1424;
        float *****l_1422 = &l_1423;
        int32_t l_1438[3];
        int32_t l_1465[9][1][9] = {{{0xC4E528AAL,0xC4E528AAL,1L,0x637C03D3L,0xC4E528AAL,0xA0B75F3DL,0x637C03D3L,0x637C03D3L,0xA0B75F3DL}},{{0x6A2BA225L,0x83D57EEEL,1L,0x83D57EEEL,0x6A2BA225L,0x8CF97880L,0x6A2BA225L,0x83D57EEEL,1L}},{{0xC4E528AAL,0x637C03D3L,1L,0xC4E528AAL,0xC4E528AAL,1L,0x637C03D3L,0xC4E528AAL,0xA0B75F3DL}},{{(-1L),0x83D57EEEL,0x802B3FC4L,0x83D57EEEL,(-1L),0x8CF97880L,(-1L),0x83D57EEEL,0x802B3FC4L}},{{0xC4E528AAL,0xC4E528AAL,1L,0x637C03D3L,0xC4E528AAL,0xA0B75F3DL,0x637C03D3L,0x637C03D3L,0xA0B75F3DL}},{{0x6A2BA225L,0x83D57EEEL,1L,0x83D57EEEL,0x6A2BA225L,0x8CF97880L,0x6A2BA225L,0x83D57EEEL,1L}},{{0xC4E528AAL,0x637C03D3L,1L,0xC4E528AAL,0xC4E528AAL,1L,0x637C03D3L,0xC4E528AAL,0xA0B75F3DL}},{{(-1L),0x83D57EEEL,0x802B3FC4L,0x83D57EEEL,(-1L),0x8CF97880L,(-1L),0x83D57EEEL,0x802B3FC4L}},{{0xC4E528AAL,0xC4E528AAL,1L,0x637C03D3L,0xC4E528AAL,0xA0B75F3DL,0x637C03D3L,0x637C03D3L,0xA0B75F3DL}}};
        uint8_t l_1499 = 0UL;
        uint32_t l_1502 = 7UL;
        int32_t ***l_1515 = &g_180;
        int32_t ****l_1514 = &l_1515;
        int32_t **** const *l_1513[6][3][10] = {{{(void*)0,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514},{&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,&l_1514},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514}},{{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,(void*)0,&l_1514,(void*)0,&l_1514},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514}},{{&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,(void*)0},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,(void*)0,(void*)0,(void*)0,&l_1514,(void*)0},{&l_1514,&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514}},{{(void*)0,&l_1514,&l_1514,&l_1514,(void*)0,(void*)0,&l_1514,&l_1514,&l_1514,&l_1514},{&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,&l_1514},{&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,(void*)0,(void*)0,&l_1514,&l_1514}},{{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514},{&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,(void*)0,&l_1514},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,(void*)0,&l_1514,&l_1514,&l_1514,&l_1514}},{{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,(void*)0,&l_1514,&l_1514},{&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514,&l_1514}}};
        uint32_t *l_1564[1];
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1396[i] = 0L;
        for (i = 0; i < 3; i++)
            l_1438[i] = 0xD19C65B7L;
        for (i = 0; i < 1; i++)
            l_1564[i] = &g_125;
        for (g_253 = 0; g_253 < 3; g_253 += 1)
        {
            g_78[g_253] = 0x41L;
        }
        for (g_305 = 0; (g_305 <= 7); g_305 += 1)
        { /* block id: 575 */
            uint16_t l_1393 = 0x3328L;
            int32_t l_1408[7][7][5] = {{{0x03619421L,0xBA5D5409L,(-1L),5L,4L},{0x0582C90BL,0xF48BB3CAL,(-1L),(-2L),0x7EBA8BBDL},{0xC00FBD1BL,(-3L),1L,(-3L),0xC00FBD1BL},{0x52D9B84AL,(-4L),(-2L),0xCFD6172AL,0L},{(-3L),0x0C22657DL,(-3L),0x3DC6A914L,(-1L)},{1L,0x789A12C2L,(-4L),(-4L),0L},{6L,0x3DC6A914L,6L,8L,0xC00FBD1BL}},{{0L,0x36D971D7L,0x8E633116L,0x1DEABC3FL,0x7EBA8BBDL},{0xA1350BC9L,0L,0L,0xA1350BC9L,4L},{(-2L),0xFCE30FB8L,1L,0x0D1340D0L,0x789A12C2L},{(-1L),0x03619421L,(-3L),(-10L),(-5L)},{0x1DEABC3FL,1L,1L,0x36D971D7L,0xCBD3497AL},{1L,(-10L),0xC00FBD1BL,4L,0xD349D0DDL},{0xFCE30FB8L,0xCBD3497AL,0x32129EE6L,0xB034DABAL,0xB034DABAL}},{{0x3DC6A914L,(-5L),0x3DC6A914L,0x90042C13L,0xBA5D5409L},{0x1DEABC3FL,0xFCE30FB8L,(-4L),(-6L),0x7EBA8BBDL},{5L,(-1L),0xBA5D5409L,0x03619421L,(-3L)},{1L,0x32129EE6L,(-4L),0x7EBA8BBDL,0x0582C90BL},{(-3L),0xBA5D5409L,0x3DC6A914L,(-8L),6L},{0xC2B3DB7CL,(-4L),0x32129EE6L,(-1L),0x32129EE6L},{0x90042C13L,0x90042C13L,0xC00FBD1BL,0x0C22657DL,0L}},{{0x36D971D7L,1L,1L,0x1DEABC3FL,0xF48BB3CAL},{1L,8L,(-8L),0xBA5D5409L,(-1L)},{0xCBD3497AL,1L,(-2L),1L,3L},{6L,0x90042C13L,1L,0xE3E1FCD3L,5L},{(-2L),(-4L),0x52D9B84AL,0x52D9B84AL,(-4L)},{(-3L),0xBA5D5409L,8L,0x3F8AFC79L,(-8L)},{(-1L),0x32129EE6L,0xC2B3DB7CL,3L,0x52D9B84AL}},{{0L,(-1L),4L,1L,(-1L)},{(-1L),0xFCE30FB8L,(-1L),1L,0xE9CA4F9EL},{(-3L),(-5L),0xD349D0DDL,(-3L),0x3F8AFC79L},{(-2L),0xCBD3497AL,(-4L),0xCBD3497AL,(-2L)},{6L,(-10L),(-3L),(-1L),0x90042C13L},{0xCBD3497AL,(-1L),0xFF559825L,0x789A12C2L,1L},{1L,(-1L),4L,(-10L),0x90042C13L}},{{0x36D971D7L,0x789A12C2L,0L,0xCFD6172AL,(-2L)},{0x90042C13L,1L,0xE3E1FCD3L,5L,0x3F8AFC79L},{0xC2B3DB7CL,0x1DEABC3FL,0x1DEABC3FL,0xC2B3DB7CL,0xE9CA4F9EL},{(-3L),0x03619421L,(-1L),0L,(-1L)},{1L,(-1L),0xCBD3497AL,(-4L),0x52D9B84AL},{5L,1L,0x3F8AFC79L,0L,(-8L)},{0x1DEABC3FL,(-6L),1L,0xC2B3DB7CL,(-4L)}},{{0x3DC6A914L,(-8L),6L,5L,5L},{0xFCE30FB8L,0x8E633116L,0xFCE30FB8L,0xCFD6172AL,3L},{1L,0x3DC6A914L,0xA1350BC9L,(-10L),(-1L)},{0xB034DABAL,(-2L),3L,0x789A12C2L,0xF48BB3CAL},{0xC00FBD1BL,6L,0xA1350BC9L,(-1L),0L},{(-1L),3L,0xFCE30FB8L,0xCBD3497AL,0x32129EE6L},{4L,0xD349D0DDL,6L,(-3L),6L}}};
            uint16_t l_1409[4][3][4] = {{{65528UL,3UL,65528UL,5UL},{65528UL,65528UL,3UL,65528UL},{65528UL,5UL,5UL,65528UL}},{{5UL,65528UL,5UL,5UL},{65528UL,65528UL,3UL,65528UL},{65528UL,5UL,5UL,65528UL}},{{5UL,65528UL,5UL,5UL},{65528UL,65528UL,3UL,65528UL},{65528UL,5UL,5UL,65528UL}},{{5UL,65528UL,5UL,5UL},{65528UL,65528UL,3UL,65528UL},{65528UL,5UL,5UL,65528UL}}};
            int16_t *l_1430[2];
            int16_t ** const l_1429 = &l_1430[1];
            int64_t l_1439 = 8L;
            int16_t l_1460 = 3L;
            int8_t **l_1497[9] = {&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227};
            uint32_t l_1524 = 0xCD2F88E7L;
            uint32_t l_1589 = 0x9EFEF9AAL;
            const float *l_1596 = &g_91[1];
            const float **l_1595[5][3] = {{(void*)0,&l_1596,(void*)0},{&l_1596,&l_1596,&l_1596},{(void*)0,&l_1596,(void*)0},{&l_1596,&l_1596,&l_1596},{(void*)0,&l_1596,(void*)0}};
            const float ***l_1594 = &l_1595[4][2];
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1430[i] = &g_470;
            if (((safe_lshift_func_uint16_t_u_s((((((l_1392 ^= (*p_45)) < p_49) && 0L) , (l_1393 , ((safe_mod_func_int64_t_s_s(0L, l_1396[1])) , p_47))) && (safe_mul_func_int16_t_s_s(((((g_174[7] <= p_48) || p_49) != 0UL) | l_1396[3]), l_1392))), g_183[4])) >= (*g_427)))
            { /* block id: 577 */
                int32_t l_1399 = 0x8DCD3842L;
                int32_t l_1442[7][3] = {{9L,9L,0x3D1A4230L},{9L,0x0219EB2EL,0x36C198B1L},{(-1L),9L,(-1L)},{(-1L),9L,9L},{9L,(-1L),(-1L)},{9L,(-1L),0x36C198B1L},{0x0219EB2EL,9L,0x3D1A4230L}};
                uint16_t l_1443 = 0x6B02L;
                uint8_t l_1490 = 1UL;
                int i, j;
                (*g_97) = p_48;
                if (l_1399)
                { /* block id: 579 */
                    for (g_470 = 0; (g_470 <= 2); g_470 += 1)
                    { /* block id: 582 */
                        int32_t *l_1400 = &g_163;
                        int32_t *l_1401 = &l_1399;
                        int32_t *l_1402 = &g_29;
                        int32_t *l_1403 = &l_1399;
                        int32_t *l_1404 = (void*)0;
                        int32_t *l_1405 = &l_1399;
                        int32_t *l_1406[7] = {(void*)0,&l_1399,(void*)0,(void*)0,&l_1399,(void*)0,(void*)0};
                        int i;
                        ++l_1409[3][0][1];
                        return g_311;
                    }
                }
                else
                { /* block id: 586 */
                    int32_t *l_1433 = &g_29;
                    int32_t *l_1434 = &l_1399;
                    int32_t *l_1435 = &g_29;
                    int32_t *l_1436 = &l_1408[5][0][1];
                    int32_t *l_1437[1][5][8] = {{{(void*)0,&g_29,&l_1407,&l_1408[6][4][3],&l_1408[6][4][3],&l_1407,&g_29,(void*)0},{&g_163,&g_7,(void*)0,(void*)0,&g_428,&g_163,&l_1408[6][4][3],&l_1407},{(void*)0,&g_428,&g_428,&g_7,(void*)0,&g_163,(void*)0,&g_7},{&g_29,&g_7,&g_29,(void*)0,&g_163,&l_1407,&g_428,&g_163},{&g_7,&g_29,(void*)0,&l_1408[6][4][3],(void*)0,&l_1408[6][4][3],&g_163,&g_163}}};
                    int32_t l_1448 = 0x6825784AL;
                    int i, j, k;
                    for (g_29 = 0; (g_29 <= 2); g_29 += 1)
                    { /* block id: 589 */
                        uint8_t *l_1412 = (void*)0;
                        uint8_t *l_1413[9][5] = {{(void*)0,&g_667,&g_667,(void*)0,&g_1216[0][5][0]},{&g_1216[0][5][0],(void*)0,(void*)0,&g_667,&g_667},{&g_311,(void*)0,&g_311,&g_1216[0][5][0],(void*)0},{&g_667,&g_667,&g_1216[0][5][0],&g_667,&g_1216[0][5][0]},{&g_667,&g_667,(void*)0,(void*)0,&g_1216[0][5][0]},{&g_311,&g_1216[0][5][0],&g_1216[0][5][0],&g_1216[0][5][0],&g_1216[0][5][0]},{&g_1216[0][5][0],&g_667,&g_311,&g_1216[0][5][0],&g_1216[0][5][0]},{(void*)0,&g_1216[0][5][0],(void*)0,&g_1216[0][5][0],(void*)0},{&g_311,&g_667,&g_667,&g_1216[0][5][0],&g_667}};
                        float *****l_1421 = (void*)0;
                        int32_t *l_1431[8] = {&g_7,(void*)0,(void*)0,&g_7,(void*)0,(void*)0,&g_7,(void*)0};
                        int i, j, k;
                        (**g_849) = ((g_311++) > ((((safe_add_func_uint8_t_u_u((((+(-1L)) , p_49) | p_47), ((safe_sub_func_int32_t_s_s((((((l_1421 != l_1422) , g_1426) == l_1429) != ((l_1408[2][1][0] = p_49) > (((p_48 , 0L) >= l_1392) , p_49))) > g_132), 0xAAED3781L)) >= p_47))) != l_1392) <= p_48) > g_1180));
                        if (l_1409[1][1][3])
                            break;
                        (*g_180) = g_1387[g_29][(g_29 + 4)][g_29];
                    }
                    l_1443--;
                    for (g_470 = 0; (g_470 >= (-26)); g_470--)
                    { /* block id: 599 */
                        uint32_t l_1449[6][8][5] = {{{0xA2710C30L,0x11D421EBL,4294967295UL,4294967293UL,1UL},{0x951AE12AL,4294967295UL,0UL,0x5BB702BAL,4294967295UL},{0xA2710C30L,4294967289UL,0UL,1UL,0xE84DDD5CL},{0xD950E5F8L,0x13C5FDF2L,0x0A3D8C52L,4294967295UL,4294967295UL},{4294967295UL,1UL,4294967295UL,0xE84DDD5CL,1UL},{1UL,0x6C89FE07L,0xFD6DC0B6L,4294967295UL,0x5BB702BAL},{1UL,0x3FFFC4F2L,4294967295UL,1UL,4294967293UL},{0x951AE12AL,0x501C3614L,0xFD6DC0B6L,0x5BB702BAL,0x86CDF2A5L}},{{0xC061DF93L,4294967289UL,4294967295UL,4294967293UL,0xE84DDD5CL},{1UL,0x501C3614L,0x0A3D8C52L,0x86CDF2A5L,1UL},{4294967295UL,0x3FFFC4F2L,0UL,0xE84DDD5CL,4294967295UL},{1UL,0x6C89FE07L,0UL,1UL,0x5BB702BAL},{0xC061DF93L,1UL,4294967295UL,4294967295UL,4294967295UL},{0x951AE12AL,0x13C5FDF2L,0x951AE12AL,0x5BB702BAL,1UL},{1UL,4294967289UL,0x4E3B3A7BL,4294967295UL,0xE84DDD5CL},{1UL,4294967295UL,0x0A3D8C52L,1UL,0x86CDF2A5L}},{{4294967295UL,0x11D421EBL,0x4E3B3A7BL,0xE84DDD5CL,4294967293UL},{0xD950E5F8L,0x6C89FE07L,0x951AE12AL,0x86CDF2A5L,0x5BB702BAL},{0xA2710C30L,0x11D421EBL,4294967295UL,4294967293UL,1UL},{0x951AE12AL,4294967295UL,0UL,0x5BB702BAL,4294967295UL},{0xA2710C30L,4294967289UL,0UL,1UL,0xE84DDD5CL},{0xD950E5F8L,0x13C5FDF2L,0x0A3D8C52L,4294967295UL,4294967295UL},{4294967295UL,1UL,4294967295UL,0xE84DDD5CL,1UL},{1UL,0x6C89FE07L,0xFD6DC0B6L,4294967295UL,0x5BB702BAL}},{{1UL,0x3FFFC4F2L,4294967295UL,1UL,4294967293UL},{0x951AE12AL,0x501C3614L,0xFD6DC0B6L,0x5BB702BAL,0x86CDF2A5L},{0xC061DF93L,4294967289UL,4294967295UL,4294967293UL,0xE84DDD5CL},{1UL,0x501C3614L,0x0A3D8C52L,0x86CDF2A5L,1UL},{4294967295UL,0x3FFFC4F2L,0UL,0xE84DDD5CL,4294967295UL},{1UL,0x6C89FE07L,0UL,1UL,0x5BB702BAL},{0xC061DF93L,1UL,4294967295UL,4294967295UL,4294967295UL},{0x951AE12AL,0x13C5FDF2L,0x951AE12AL,0x5BB702BAL,1UL}},{{1UL,4294967289UL,0x4E3B3A7BL,4294967295UL,0xE84DDD5CL},{1UL,4294967295UL,0x0A3D8C52L,1UL,0x86CDF2A5L},{4294967295UL,0x11D421EBL,0x4E3B3A7BL,0xE84DDD5CL,4294967293UL},{0xD950E5F8L,0x6C89FE07L,0x951AE12AL,0x86CDF2A5L,0x5BB702BAL},{0xA2710C30L,0x11D421EBL,4294967295UL,4294967293UL,1UL},{0x951AE12AL,4294967295UL,0UL,0x5BB702BAL,4294967295UL},{0xA2710C30L,0xD7E02673L,0xDA2CCB3DL,0UL,0xB4E29BFCL},{0x7BB7D1FDL,0xA5F6147AL,0xA0C8C8A9L,0x951AE12AL,0x951AE12AL}},{{0x08EF81CFL,4294967295UL,0x08EF81CFL,0xB4E29BFCL,0UL},{0x1F58BF9FL,4294967295UL,5UL,0x951AE12AL,3UL},{1UL,0x36C83DB3L,6UL,0UL,0x4E3B3A7BL},{0x4BFD3308L,0xB63D2ACFL,5UL,3UL,0xFD6DC0B6L},{6UL,0xD7E02673L,0x08EF81CFL,0x4E3B3A7BL,0xB4E29BFCL},{0xD329AB92L,0xB63D2ACFL,0xA0C8C8A9L,0xFD6DC0B6L,0UL},{0x08EF81CFL,0x36C83DB3L,0xDA2CCB3DL,0xB4E29BFCL,4294967295UL},{0xD329AB92L,4294967295UL,0x0EA7EBA5L,0UL,3UL}}};
                        const float ****l_1457 = (void*)0;
                        uint16_t *l_1466 = &l_1393;
                        uint16_t *l_1467 = &l_1409[1][0][1];
                        int32_t l_1468 = 0L;
                        int i, j, k;
                        l_1449[2][6][2]--;
                        (*g_850) &= (l_1468 = (g_132 ^ ((!(safe_mul_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(((l_1457 == ((safe_sub_func_uint32_t_u_u((*p_46), ((p_48 == (((*l_1467) = ((*l_1466) = (((*l_1433) = l_1460) , (safe_div_func_int16_t_s_s((((*p_45) != l_1465[5][0][2]) ^ ((*p_46) , ((*g_227) < p_48))), g_667))))) | g_1164[3])) >= 3L))) , (void*)0)) , l_1408[6][4][3]), 4)), 255UL))) , p_49)));
                    }
                }
                for (g_253 = 3; (g_253 >= 0); g_253 -= 1)
                { /* block id: 610 */
                    int32_t l_1488 = 0x610ABEDEL;
                    int32_t l_1489[2];
                    int8_t l_1498 = 0x81L;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1489[i] = 0x4A4EFB15L;
                    if (l_1441[g_253])
                    { /* block id: 611 */
                        int32_t **l_1469 = (void*)0;
                        int32_t **l_1470 = &g_101;
                        int i;
                        (*g_180) = &l_1441[(g_428 + 1)];
                        (*g_850) &= (l_1441[g_253] = l_1438[0]);
                        (*l_1470) = p_46;
                        if ((**g_180))
                            break;
                    }
                    else
                    { /* block id: 617 */
                        int64_t *l_1477 = &g_183[4];
                        uint8_t *l_1481 = &g_311;
                        uint8_t **l_1480 = &l_1481;
                        uint8_t ***l_1483[6];
                        uint8_t ****l_1482 = &l_1483[4];
                        int32_t *l_1484 = &l_1438[2];
                        int32_t *l_1485 = (void*)0;
                        int32_t *l_1486 = &l_1441[2];
                        int32_t *l_1487[5][8] = {{&l_1408[6][4][3],(void*)0,&l_1408[6][4][3],(void*)0,&l_1408[6][4][3],&l_1438[0],&l_1432,&g_11},{&l_1465[5][0][2],&l_1408[3][2][0],&l_1438[0],(void*)0,(void*)0,(void*)0,&l_1438[0],&l_1408[3][2][0]},{&l_1432,(void*)0,&l_1408[6][4][3],(void*)0,&g_11,&g_4,&l_1465[5][0][2],&g_163},{(void*)0,&g_11,&l_1408[3][2][0],&l_1408[6][4][3],&l_1432,&l_1465[5][0][2],&l_1465[5][0][2],&l_1432},{&g_4,&l_1408[6][4][3],&l_1408[6][4][3],&g_4,&l_1408[3][2][0],&g_11,&l_1438[0],&l_1442[2][1]}};
                        int i, j;
                        for (i = 0; i < 6; i++)
                            l_1483[i] = &l_1480;
                        (**g_849) = (safe_div_func_int16_t_s_s(((*g_227) && (((safe_add_func_int64_t_s_s((((*l_1477) = (p_49 , (p_47 >= ((**g_1237) != ((&g_1237 == &g_1237) , (g_1216[0][5][0]--)))))) , (safe_div_func_int32_t_s_s((g_174[7] < ((((void*)0 != l_1480) | l_1465[5][0][2]) || p_49)), 1L))), l_1465[4][0][2])) , 0xD5L) , l_1441[g_253])), 0xFF84L));
                        (*l_1482) = (void*)0;
                        l_1490++;
                        (*g_180) = &l_1438[2];
                    }
                    l_1493[6][9][1] = p_45;
                    for (g_163 = (-22); (g_163 > (-7)); g_163 = safe_add_func_uint16_t_u_u(g_163, 1))
                    { /* block id: 628 */
                        int32_t **l_1496 = &g_101;
                        if (p_48)
                            break;
                        (*l_1496) = p_46;
                        if ((**g_375))
                            break;
                        if (p_47)
                            continue;
                    }
                    if ((p_48 ^ 2UL))
                    { /* block id: 634 */
                        (*g_1313) = l_1497[0];
                        l_1499--;
                        --l_1502;
                    }
                    else
                    { /* block id: 638 */
                        int64_t l_1505 = 1L;
                        int32_t l_1506 = (-1L);
                        int32_t l_1507 = 0L;
                        int32_t l_1508 = 0L;
                        int32_t l_1509 = 0x33B72818L;
                        l_1510++;
                        if (l_1442[2][1])
                            break;
                        l_1488 &= ((***l_1515) &= (l_1513[4][2][4] != &g_368[0][0][1]));
                    }
                }
                for (l_1440 = 0; (l_1440 > 25); l_1440 = safe_add_func_uint32_t_u_u(l_1440, 6))
                { /* block id: 647 */
                    return p_49;
                }
            }
            else
            { /* block id: 650 */
                uint16_t *l_1535 = &g_146;
                int32_t l_1537 = 0x50FC69F0L;
                int8_t *l_1548 = &g_174[7];
                int32_t l_1562[1][10] = {{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}};
                const uint8_t *l_1576 = (void*)0;
                const uint8_t **l_1575 = &l_1576;
                const uint8_t ***l_1574 = &l_1575;
                uint8_t l_1584 = 246UL;
                int i, j;
                l_1537 = ((p_48 == ((safe_mul_func_uint16_t_u_u((safe_add_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((l_1524 | (((0x9F27EBEAL | (((safe_add_func_int8_t_s_s((((*l_1535) = ((safe_mod_func_uint32_t_u_u(l_1409[3][0][1], (safe_div_func_int32_t_s_s(((l_1393 , (safe_sub_func_int64_t_s_s((safe_sub_func_uint64_t_u_u((((p_48 | ((&g_1386 != &g_1386) & (*g_1427))) >= (**g_1237)) > p_48), 0L)), p_49))) && g_1216[0][1][7]), (*p_45))))) , 0xC890L)) ^ 0x4CFEL), g_1164[3])) , (*g_1427)) ^ p_49)) >= (-1L)) != p_48)), g_1095[1])), l_1536[3][0][0])), 1UL)) < 0x71L)) ^ 1L);
                for (l_1392 = 0; (l_1392 != (-18)); --l_1392)
                { /* block id: 655 */
                    uint8_t **l_1543 = &l_1542;
                    uint8_t *l_1545 = &g_1216[0][5][2];
                    uint8_t **l_1544 = &l_1545;
                    int32_t l_1561 = 0xDC6F379BL;
                    int32_t l_1563 = 0xD7E9A7B2L;
                    int32_t l_1565 = 0xD418E0CFL;
                    (*g_850) = (l_1565 &= (((((safe_mul_func_int8_t_s_s((0x1B6326D2L >= (**g_858)), (((*l_1543) = l_1542) != ((*l_1544) = (void*)0)))) , (safe_mod_func_int16_t_s_s((((l_1548 = (*g_1237)) == (**g_1236)) < p_48), (((safe_add_func_uint64_t_u_u(((((safe_add_func_int32_t_s_s(((safe_rshift_func_uint8_t_u_s((safe_div_func_int32_t_s_s((l_1562[0][0] = (safe_add_func_int32_t_s_s(((l_1537 &= ((l_1561 = 0x15A2D294L) != ((l_1439 , (void*)0) == (void*)0))) , p_48), 0xD992A893L))), l_1439)), p_47)) , 0x483E56B9L), p_48)) | l_1563) , (*g_1380)) , l_1562[0][7]), 1UL)) == 0xF636E03D8C16DEB8LL) , p_49)))) < g_1095[0]) , (void*)0) != l_1564[0]));
                }
                for (p_47 = 0; p_47 < 4; p_47 += 1)
                {
                    l_1441[p_47] = 0x3F05C22BL;
                }
                for (g_253 = 10; (g_253 < 5); g_253--)
                { /* block id: 668 */
                    uint64_t l_1569 = 1UL;
                    int32_t l_1578 = 0xC46359E3L;
                    int32_t l_1580 = 1L;
                    int32_t l_1583 = 0x75A02E4AL;
                    int8_t * const *l_1605[9][9] = {{(void*)0,(void*)0,&g_227,(void*)0,&l_1548,&g_227,&l_1548,&l_1548,(void*)0},{(void*)0,&l_1548,&g_227,(void*)0,&g_227,&l_1548,&l_1548,&g_227,(void*)0},{(void*)0,&g_227,(void*)0,(void*)0,(void*)0,(void*)0,&l_1548,(void*)0,&l_1548},{(void*)0,(void*)0,&g_227,(void*)0,&l_1548,&g_227,&l_1548,&l_1548,(void*)0},{(void*)0,&l_1548,&g_227,(void*)0,&g_227,&l_1548,&l_1548,&g_227,(void*)0},{(void*)0,&g_227,(void*)0,(void*)0,(void*)0,(void*)0,&l_1548,(void*)0,&l_1548},{(void*)0,(void*)0,&g_227,(void*)0,&l_1548,&g_227,&l_1548,&l_1548,(void*)0},{(void*)0,&l_1548,&g_227,(void*)0,&g_227,&l_1548,&l_1548,&g_227,(void*)0},{(void*)0,&g_227,(void*)0,(void*)0,(void*)0,(void*)0,&l_1548,(void*)0,&l_1548}};
                    int8_t * const **l_1604 = &l_1605[6][4];
                    int i, j;
                    for (l_1407 = 0; (l_1407 <= 2); l_1407 += 1)
                    { /* block id: 671 */
                        int32_t l_1568[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1568[i] = 0xAFF78F5BL;
                        l_1568[0] &= (l_1408[0][6][2] = ((p_45 == (void*)0) , ((l_1409[(l_1407 + 1)][l_1407][(l_1407 + 1)] , l_1542) != (void*)0)));
                        l_1569 &= 0xA6073A2CL;
                        (***l_1514) = p_45;
                    }
                    for (l_1392 = 2; (l_1392 >= 0); l_1392 -= 1)
                    { /* block id: 679 */
                        int32_t l_1577 = 0xA08C19F4L;
                        int64_t l_1579 = 0L;
                        int32_t l_1581 = 0x07F1E159L;
                        int32_t l_1582 = 0x96283745L;
                        int64_t *l_1599 = (void*)0;
                        int64_t *l_1600 = (void*)0;
                        int64_t *l_1601 = &l_1579;
                        int i;
                        l_1408[3][0][1] &= (safe_mod_func_int8_t_s_s(1L, (safe_lshift_func_int8_t_s_u((65535UL ^ ((l_1574 != (void*)0) & g_78[g_428])), 4))));
                        if (p_49)
                            break;
                        ++l_1584;
                        l_1580 = ((((2UL > (((safe_mul_func_int16_t_s_s(l_1589, ((safe_sub_func_int32_t_s_s((((l_1592[2] == l_1594) >= ((*l_1535) = g_679)) , (safe_div_func_int64_t_s_s(((*l_1601) &= p_49), p_49))), (safe_rshift_func_uint16_t_u_u((((void*)0 != l_1604) < (-1L)), 9)))) , (-4L)))) , l_1493[5][7][1]) != p_45)) && (*g_227)) >= g_125) > p_47);
                    }
                }
            }
            l_1408[6][4][3] = (~(*p_45));
            return (**g_1426);
        }
    }
    (***g_423) = (*g_97);
    for (l_1392 = 29; (l_1392 > 8); l_1392 = safe_sub_func_int8_t_s_s(l_1392, 8))
    { /* block id: 696 */
        float l_1609 = 0xC.FBB27Ap+83;
        uint16_t l_1610[3];
        int i;
        for (i = 0; i < 3; i++)
            l_1610[i] = 0x7E56L;
        l_1610[2]--;
    }
    return p_47;
}


/* ------------------------------------------ */
/* 
 * reads : g_1379 g_470 g_125 g_1313 g_423 g_424 g_97 g_1095 g_850 g_101 g_41 g_227 g_849
 * writes: g_1379 g_470 g_1237 g_29 g_305 g_414 g_101 g_1387 g_174 g_91
 */
static uint32_t * func_50(uint32_t * p_51, const float  p_52, int32_t * p_53, int32_t  p_54)
{ /* block id: 558 */
    int8_t l_1373 = 1L;
    uint32_t *l_1378 = &g_41;
    uint32_t **l_1377 = &l_1378;
    const uint32_t ***l_1381[1];
    int16_t *l_1382 = &g_470;
    int32_t *l_1384 = (void*)0;
    int32_t **l_1383[8][5] = {{&l_1384,&l_1384,&l_1384,&l_1384,(void*)0},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,(void*)0,&l_1384,&l_1384},{&l_1384,&l_1384,(void*)0,&l_1384,&l_1384},{(void*)0,&l_1384,&l_1384,&l_1384,&l_1384},{(void*)0,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,(void*)0,&l_1384}};
    int32_t *l_1385 = &g_1386;
    uint32_t l_1388 = 1UL;
    int32_t l_1389 = 0x1817DF9DL;
    int i, j;
    for (i = 0; i < 1; i++)
        l_1381[i] = &g_1379;
    (**g_849) = ((l_1389 = (safe_mul_func_int8_t_s_s((((safe_rshift_func_int16_t_s_s(p_54, 2)) || ((((*g_227) = ((safe_add_func_uint32_t_u_u(((g_1387[0][6][2] = (l_1385 = func_55(&g_125, p_51, (((safe_unary_minus_func_uint64_t_u((safe_div_func_uint16_t_u_u(((((((void*)0 != &g_1094[9][2][1]) != l_1373) , (-1L)) < (~((*l_1382) &= (safe_mul_func_uint8_t_u_u(((l_1377 = &p_51) == (g_1379 = g_1379)), (-8L)))))) <= 0x171C3D2031262FFCLL), g_125)))) == 9UL) , 1L), &g_101))) != (void*)0), (*p_51))) , (-1L))) == l_1373) == 0x8A8227BA31A5AECBLL)) == l_1388), 0x46L))) ^ 0L);
    return &g_125;
}


/* ------------------------------------------ */
/* 
 * reads : g_1313 g_423 g_424 g_97 g_1095 g_850 g_101
 * writes: g_1237 g_29 g_305 g_414 g_101 g_91
 */
static uint32_t * func_55(uint32_t * p_56, uint32_t * p_57, int16_t  p_58, int32_t ** p_59)
{ /* block id: 538 */
    uint32_t l_1299 = 0xC2544F69L;
    int32_t l_1300 = 0xDB3304FCL;
    int32_t l_1307 = (-6L);
    int32_t l_1308 = 0x3151B76CL;
    int8_t **l_1312 = &g_227;
    int32_t l_1344 = (-4L);
    int32_t l_1345 = 0x6B71C385L;
    int32_t l_1346 = 8L;
    int32_t l_1351 = 1L;
    int32_t l_1353 = 0xB9E1FF42L;
    int32_t l_1354 = 6L;
    int32_t l_1355 = 5L;
    int32_t l_1356 = (-6L);
    int32_t l_1357[1];
    int32_t l_1358 = (-2L);
    int64_t l_1359 = 0x0BBAC1493229DED4LL;
    int64_t l_1360 = 1L;
    int i;
    for (i = 0; i < 1; i++)
        l_1357[i] = 1L;
    if ((0x6A7C9CB36BDB8493LL & l_1299))
    { /* block id: 539 */
        int32_t *l_1301 = &l_1300;
        int32_t *l_1302 = &l_1300;
        int32_t *l_1303 = (void*)0;
        int32_t *l_1304 = (void*)0;
        int32_t *l_1305 = &g_163;
        int32_t *l_1306[6];
        uint64_t l_1309 = 18446744073709551613UL;
        uint64_t l_1314 = 1UL;
        int i;
        for (i = 0; i < 6; i++)
            l_1306[i] = (void*)0;
        l_1309--;
        (*g_1313) = l_1312;
        (***g_423) = l_1314;
    }
    else
    { /* block id: 543 */
        float **l_1317 = &g_97;
        const int32_t l_1320 = 9L;
        float **l_1322 = (void*)0;
        float ***l_1321 = &l_1322;
        uint64_t *l_1325 = &g_305;
        int32_t *l_1327 = &l_1307;
        int32_t l_1339 = 0xF877C7BAL;
        int32_t l_1341 = 4L;
        int32_t l_1342 = (-9L);
        int32_t l_1343 = (-9L);
        int32_t l_1347 = (-10L);
        int32_t l_1348 = (-7L);
        int32_t l_1349 = 0x2E3F5CE5L;
        int32_t l_1350[8];
        int8_t l_1352 = (-1L);
        int i;
        for (i = 0; i < 8; i++)
            l_1350[i] = 0L;
        (*g_850) = ((safe_rshift_func_uint8_t_u_u((((l_1308 , l_1317) == ((p_58 , ((safe_sub_func_uint16_t_u_u((l_1320 > 0xF9F81E9AL), p_58)) ^ l_1320)) , ((*l_1321) = &g_97))) & ((*l_1325) = ((safe_lshift_func_int16_t_s_u(p_58, g_1095[1])) && 0x535BF022L))), 4)) <= p_58);
        for (l_1299 = 0; (l_1299 <= 9); l_1299 += 1)
        { /* block id: 549 */
            int32_t *l_1326 = &l_1307;
            int32_t *l_1328 = &g_428;
            int32_t *l_1329 = &g_29;
            int32_t *l_1330 = (void*)0;
            int32_t *l_1331 = &g_428;
            int32_t *l_1332 = &l_1307;
            int32_t *l_1333 = &l_1300;
            int32_t *l_1334 = &l_1300;
            int32_t *l_1335 = &g_428;
            int32_t *l_1336 = &l_1308;
            int32_t *l_1337 = &g_163;
            int32_t *l_1338 = &g_29;
            int32_t *l_1340[10][10] = {{&l_1300,&l_1300,&g_428,&l_1308,&g_163,&g_11,&l_1300,&g_163,&l_1308,&g_163},{&l_1300,&l_1339,&g_4,&l_1308,&g_4,&l_1339,&l_1300,&g_4,(void*)0,&g_163},{&l_1339,&l_1300,&g_4,(void*)0,&g_163,&l_1339,&l_1339,&g_163,(void*)0,&g_4},{&l_1300,&l_1300,&g_428,&l_1308,&g_163,&g_11,&l_1300,&g_163,&l_1308,&g_163},{&l_1300,&l_1339,&g_4,&l_1308,&g_4,&l_1339,&l_1300,&g_4,(void*)0,&g_163},{&l_1339,&l_1300,&g_4,(void*)0,&g_163,&l_1339,&l_1339,&g_163,(void*)0,&g_4},{&l_1300,&l_1300,&g_428,&l_1308,&g_163,&g_11,&l_1300,&g_163,&l_1308,&g_163},{&l_1300,&l_1339,&g_4,&l_1308,&g_4,&l_1339,&l_1300,&g_4,(void*)0,&g_163},{&l_1339,&l_1300,&g_4,(void*)0,&g_163,&l_1339,&l_1339,&g_163,(void*)0,&g_4},{&l_1300,&l_1300,&g_428,&l_1308,&g_163,&g_11,&l_1300,&g_163,&l_1308,&g_163}};
            uint8_t l_1361 = 0x31L;
            int i, j;
            l_1327 = l_1326;
            l_1361--;
            (*p_59) = (*p_59);
            (*p_59) = (void*)0;
        }
    }
    return &g_41;
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_78 g_7 g_91 g_100 g_101 g_10 g_41 g_132 g_97 g_11 g_4 g_297 g_375 g_679 g_1236 g_1237 g_227 g_423 g_424 g_305 g_1290 g_428
 * writes: g_91 g_93 g_97 g_41 g_29 g_101 g_146 g_253 g_163 g_679 g_174 g_1290 g_428
 */
static uint32_t * func_61(int32_t * p_62, uint32_t * p_63, int16_t  p_64)
{ /* block id: 22 */
    int32_t *l_70 = &g_29;
    int32_t **l_69[10][7][3] = {{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,(void*)0}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,(void*)0}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,(void*)0},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,(void*)0,&l_70},{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,(void*)0,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,(void*)0},{&l_70,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{&l_70,(void*)0,&l_70},{&l_70,&l_70,(void*)0},{&l_70,(void*)0,&l_70},{&l_70,&l_70,&l_70},{&l_70,(void*)0,&l_70},{&l_70,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,(void*)0,&l_70},{&l_70,&l_70,&l_70},{(void*)0,&l_70,&l_70}},{{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70},{&l_70,(void*)0,&l_70},{&l_70,&l_70,&l_70},{&l_70,&l_70,&l_70}}};
    int32_t **l_1271 = &g_101;
    int8_t *l_1276 = &g_253;
    int8_t l_1292 = 5L;
    int i, j, k;
    p_62 = &g_4;
lbl_1293:
    (*l_1271) = func_71(l_70, &g_11);
    for (g_679 = 24; (g_679 > 4); g_679--)
    { /* block id: 521 */
        int32_t *l_1274 = &g_428;
        int8_t *l_1275 = &g_174[7];
        uint64_t *l_1291[5][1];
        int i, j;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
                l_1291[i][j] = &g_1290[1];
        }
        (*l_1271) = l_1274;
        if (((((((***g_423) = (p_64 > ((l_1275 = (**g_1236)) != l_1276))) == (((safe_div_func_float_f_f(0xA.BEAD53p-45, (safe_div_func_float_f_f(((safe_sub_func_uint64_t_u_u((safe_mod_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_s((g_305 > (((g_1290[2] = (0x17F2L | (safe_mul_func_int16_t_s_s(0x86E6L, (((((*g_227) = ((+7L) , g_1290[4])) , 0x9204L) ^ 0x2D58L) && 1UL))))) ^ (*l_1274)) , p_64)), p_64)) || (*l_1274)), (*p_62))), g_428)) , p_64), 0x2.1p-1)))) <= p_64) > (-0x8.Cp+1))) , 18446744073709551607UL) > l_1292) , (*p_62)))
        { /* block id: 527 */
            (*g_101) ^= (&g_470 == (void*)0);
        }
        else
        { /* block id: 529 */
            return &g_125;
        }
        if (l_1292)
            goto lbl_1293;
        (**l_1271) = (*l_1274);
    }
    return l_70;
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_78 g_7 g_91 g_41 g_100 g_101 g_10 g_132 g_97 g_11 g_4 g_297 g_375 g_125
 * writes: g_91 g_93 g_97 g_41 g_29 g_101 g_146 g_253 g_163
 */
static int32_t * const  func_71(uint32_t * p_72, int32_t * const  p_73)
{ /* block id: 24 */
    uint64_t l_81 = 18446744073709551615UL;
    int32_t l_87 = 0L;
    float *l_90 = &g_91[3];
    float *l_92 = &g_93;
    int32_t l_94[2][8] = {{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}};
    float **l_95 = (void*)0;
    float **l_96[1];
    uint32_t *l_124 = &g_125;
    int32_t **l_181 = &g_101;
    int32_t l_296 = 0x8E438067L;
    int32_t ** const *l_354 = &g_180;
    int32_t ** const **l_353 = &l_354;
    uint32_t l_386[6] = {0UL,0UL,0UL,0UL,0UL,0UL};
    int32_t ***l_509 = &g_180;
    int32_t **** const l_508 = &l_509;
    int32_t **** const *l_507 = &l_508;
    int32_t l_522 = 0x79D84CD1L;
    uint32_t l_663 = 4294967295UL;
    uint8_t *l_737 = &g_311;
    const float l_795 = 0x1.A661B4p+95;
    float * const *l_834 = &l_92;
    float * const **l_833[6][6];
    int32_t *l_913 = &l_94[1][4];
    int16_t *l_924 = &g_470;
    int16_t l_949 = 8L;
    int8_t **l_951 = &g_227;
    int8_t ***l_950 = &l_951;
    int32_t ****l_969[8][5] = {{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509},{&l_509,&l_509,&l_509,&l_509,&l_509}};
    int32_t *****l_968 = &l_969[5][1];
    int32_t *l_1036 = &g_428;
    int16_t l_1110 = 0x89CFL;
    int32_t l_1122 = 0xA531BF34L;
    uint8_t l_1123 = 0xF2L;
    int16_t l_1138[4][5] = {{(-1L),(-1L),(-1L),(-1L),(-1L)},{0xFD9AL,0xFD9AL,0xFD9AL,0xFD9AL,0xFD9AL},{(-1L),(-1L),(-1L),(-1L),(-1L)},{0xFD9AL,0xFD9AL,0xFD9AL,0xFD9AL,0xFD9AL}};
    int32_t l_1179 = 0x679EE7F3L;
    int64_t l_1259 = 0x941077EAF03F313ELL;
    int32_t **l_1269[2];
    int32_t **l_1270 = &l_1036;
    int i, j;
    for (i = 0; i < 1; i++)
        l_96[i] = &l_92;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
            l_833[i][j] = &l_834;
    }
    for (i = 0; i < 2; i++)
        l_1269[i] = (void*)0;
    l_94[1][0] = (((safe_div_func_float_f_f(g_29, (safe_mul_func_float_f_f(g_78[1], (safe_div_func_float_f_f(l_81, l_81)))))) < (l_81 == g_7)) <= (((*l_92) = (safe_sub_func_float_f_f((+(safe_sub_func_float_f_f((l_87 = l_81), l_81))), ((*l_90) = (safe_div_func_float_f_f(l_81, l_81)))))) != 0x4.183AA7p-28));
    (*l_92) = g_91[3];
    if (((g_97 = p_72) != (void*)0))
    { /* block id: 31 */
        uint64_t l_117 = 1UL;
        const int32_t *l_131 = &g_132;
        uint32_t l_136 = 0xA98BAAD8L;
        int32_t l_185 = 8L;
        uint32_t l_186 = 4UL;
        float ***l_275[5][9] = {{&l_96[0],&l_96[0],&l_96[0],(void*)0,&l_96[0],(void*)0,&l_96[0],&l_96[0],&l_96[0]},{&l_95,&l_96[0],&l_96[0],(void*)0,&l_96[0],&l_96[0],&l_95,&l_95,&l_96[0]},{(void*)0,&l_96[0],&l_96[0],&l_96[0],(void*)0,(void*)0,(void*)0,(void*)0,&l_96[0]},{&l_95,&l_96[0],&l_95,(void*)0,&l_96[0],&l_96[0],(void*)0,&l_95,&l_96[0]},{&l_96[0],&l_95,(void*)0,&l_96[0],&l_96[0],(void*)0,&l_95,&l_96[0],&l_95}};
        int64_t * const l_282 = (void*)0;
        int32_t l_332 = 1L;
        int32_t l_352 = 4L;
        int8_t *l_364 = &g_174[7];
        int32_t *l_376 = (void*)0;
        int32_t *l_377 = &l_87;
        int32_t *l_378 = &l_296;
        int32_t *l_379 = &l_94[0][7];
        int32_t *l_380 = (void*)0;
        int32_t *l_381 = &l_185;
        int32_t *l_382 = (void*)0;
        int32_t *l_383 = &l_94[1][0];
        int32_t *l_384 = (void*)0;
        int32_t *l_385[10] = {(void*)0,&l_296,(void*)0,&l_296,(void*)0,&l_296,(void*)0,&l_296,(void*)0,&l_296};
        int i, j;
        for (g_41 = 0; (g_41 >= 53); g_41 = safe_add_func_int8_t_s_s(g_41, 8))
        { /* block id: 34 */
            return p_73;
        }
        for (g_41 = 0; (g_41 <= 0); g_41 += 1)
        { /* block id: 39 */
            const int32_t l_147[8] = {0x2DF0D848L,0x2DF0D848L,0xCB49D4ABL,0x2DF0D848L,0x2DF0D848L,0xCB49D4ABL,0x2DF0D848L,0x2DF0D848L};
            int32_t l_164 = 1L;
            int16_t l_210 = 0x5A58L;
            uint32_t *l_217 = (void*)0;
            int32_t l_232[6][2][4] = {{{(-1L),(-1L),0x02FEDD84L,(-2L)},{(-1L),(-1L),(-1L),0x02FEDD84L}},{{(-1L),0x02FEDD84L,0x02FEDD84L,(-1L)},{(-1L),0x02FEDD84L,(-2L),0x02FEDD84L}},{{0x02FEDD84L,(-1L),(-2L),(-2L)},{(-1L),(-1L),0x02FEDD84L,(-2L)}},{{(-1L),(-1L),(-1L),0x02FEDD84L},{(-1L),0x02FEDD84L,0x02FEDD84L,(-1L)}},{{(-1L),0x02FEDD84L,(-2L),0x02FEDD84L},{0x02FEDD84L,(-1L),(-2L),(-2L)}},{{(-1L),(-1L),0x02FEDD84L,(-2L)},{(-1L),(-1L),(-1L),0x02FEDD84L}}};
            uint16_t l_246 = 6UL;
            const float * const l_351 = &g_93;
            const float * const *l_350[2][2];
            int32_t l_365 = (-1L);
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 2; j++)
                    l_350[i][j] = &l_351;
            }
            for (g_29 = 0; (g_29 >= 0); g_29 -= 1)
            { /* block id: 42 */
                const int32_t *l_130[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_130[i] = &g_29;
                (*g_100) = &l_94[1][0];
                if ((*g_101))
                    continue;
                (*g_101) = (**g_100);
                if ((safe_add_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u(g_10, 7)), (safe_mod_func_uint64_t_u_u(((((safe_add_func_float_f_f(((safe_mul_func_float_f_f(((safe_mul_func_uint16_t_u_u((!(l_94[1][0] = ((((g_29 != l_117) && (safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(0x97E7L, g_41)), (safe_mul_func_uint8_t_u_u(((l_124 = &g_41) == ((safe_mul_func_uint16_t_u_u((((safe_mul_func_int8_t_s_s(((l_131 = l_130[0]) != (g_101 = p_72)), (g_41 && 1UL))) , l_94[1][4]) , 0x693EL), l_94[0][3])) , (void*)0)), l_117))))) , (*l_131)) <= (-1L)))), 0x0566L)) , (*g_97)), 0x4.0p+1)) > (*g_97)), (*g_97))) , l_81) , g_132) || (*p_72)), 0x2068ECBDE39F7431LL)))) ^ g_11), g_29)))
                { /* block id: 50 */
                    int32_t l_133[9][8][1] = {{{8L},{1L},{0x96EF0398L},{4L},{(-2L)},{0x649A3464L},{(-1L)},{0L}},{{0x50E27E8FL},{0L},{(-10L)},{(-10L)},{0L},{0x50E27E8FL},{0L},{(-1L)}},{{0x649A3464L},{(-2L)},{4L},{0x96EF0398L},{1L},{8L},{0xB0EADF19L},{8L}},{{1L},{0x96EF0398L},{4L},{(-2L)},{0x649A3464L},{(-1L)},{0L},{0x50E27E8FL}},{{0L},{(-10L)},{(-10L)},{0L},{0x50E27E8FL},{0L},{(-1L)},{8L}},{{0x649A3464L},{1L},{0x6713F97CL},{(-10L)},{0x50E27E8FL},{0x1DE55174L},{0x50E27E8FL},{(-10L)}},{{0x6713F97CL},{1L},{0x649A3464L},{8L},{0L},{(-10L)},{0xB0EADF19L},{0xC007EE71L}},{{0xC47384F1L},{0xC47384F1L},{0xC007EE71L},{0xB0EADF19L},{(-10L)},{0L},{8L},{0x649A3464L}},{{1L},{0x6713F97CL},{(-10L)},{0x50E27E8FL},{0x1DE55174L},{0x50E27E8FL},{(-10L)},{0x6713F97CL}}};
                    int32_t *l_134 = &l_94[0][6];
                    int i, j, k;
                    (*l_92) = 0x0.2p+1;
                    (*l_134) ^= ((*l_131) == l_133[5][5][0]);
                    for (l_81 = 0; (l_81 <= 0); l_81 += 1)
                    { /* block id: 55 */
                        int i;
                        (*l_134) ^= g_78[l_81];
                        if (g_78[g_29])
                            continue;
                        if (g_78[l_81])
                            continue;
                    }
                    if (l_136)
                        break;
                }
                else
                { /* block id: 61 */
                    int32_t *l_143 = &g_4;
                    l_94[1][3] = (safe_sub_func_int16_t_s_s((g_11 > (safe_mul_func_uint8_t_u_u((safe_sub_func_int32_t_s_s((g_78[0] , (l_143 != (*g_100))), (g_146 = ((safe_sub_func_uint16_t_u_u((&g_97 == (g_11 , &g_97)), ((p_72 != (void*)0) == 0x79CBL))) | 8L)))), (*l_143)))), l_147[7]));
                    if ((*l_131))
                        break;
                }
            }
            for (l_136 = 17; (l_136 != 25); l_136++)
            { /* block id: 69 */
                uint32_t l_184 = 0x809ED89AL;
                int32_t l_196 = 0L;
                float **l_200[8][1] = {{&l_92},{&l_92},{&l_92},{&l_92},{&l_92},{&l_92},{&l_92},{&l_92}};
                int i, j;
            }
            for (g_253 = 0; (g_253 < (-14)); --g_253)
            { /* block id: 172 */
                int32_t *l_374 = &l_164;
                (*g_297) = ((*l_374) = 1L);
            }
            (*g_375) = p_73;
        }
        l_386[4]++;
    }
    else
    { /* block id: 179 */
        float ***l_420 = &l_96[0];
        int32_t l_425 = 0x761D3EE7L;
        uint32_t l_446 = 0xB24755AEL;
        float l_463 = 0xE.60E6F7p+6;
        int8_t *l_510 = &g_253;
        int32_t l_591 = 0xAE2BD94CL;
        int32_t l_662 = 0xF899499EL;
        uint32_t l_684 = 18446744073709551615UL;
        uint16_t l_692 = 8UL;
        uint64_t *l_712 = &g_305;
        int32_t l_721 = 0x52F2C552L;
        int32_t l_726[8];
        int32_t ****l_732 = &l_509;
        uint32_t l_748 = 9UL;
        uint32_t l_801 = 0x11DC52E8L;
        int8_t **l_806 = &g_227;
        float *l_854 = &g_135[1][0][0];
        uint8_t **l_877 = &l_737;
        const uint8_t l_918 = 248UL;
        int32_t *****l_957[8][9][1] = {{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}},{{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732},{&l_732}}};
        int32_t l_1063[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
        uint8_t l_1140 = 254UL;
        uint8_t l_1195 = 0x69L;
        uint64_t l_1235[7][4][3] = {{{0xDCFD459FB347711BLL,0x6E108886FFF294EELL,0x8633355B63474E96LL},{0UL,0x9D73A0A794B48AD8LL,0xC5D3A04B10BD57B8LL},{1UL,0x6E108886FFF294EELL,1UL},{18446744073709551607UL,0UL,0xEBF03D2EE0708B01LL}},{{18446744073709551615UL,18446744073709551610UL,1UL},{0xEBF03D2EE0708B01LL,18446744073709551607UL,0xC5D3A04B10BD57B8LL},{18446744073709551615UL,0x1FA8CE8CF57A53CELL,0x8633355B63474E96LL},{0xEBF03D2EE0708B01LL,0xEBF03D2EE0708B01LL,0x57A46841EF60A9D6LL}},{{18446744073709551615UL,1UL,0x72348967190F76A5LL},{18446744073709551607UL,0xEBF03D2EE0708B01LL,18446744073709551607UL},{1UL,0x1FA8CE8CF57A53CELL,0xDCFD459FB347711BLL},{0UL,18446744073709551607UL,18446744073709551607UL}},{{0xDCFD459FB347711BLL,18446744073709551610UL,0x72348967190F76A5LL},{0x9D73A0A794B48AD8LL,0UL,0x57A46841EF60A9D6LL},{0xDCFD459FB347711BLL,0x6E108886FFF294EELL,0x8633355B63474E96LL},{0UL,0x9D73A0A794B48AD8LL,0xC5D3A04B10BD57B8LL}},{{1UL,0x6E108886FFF294EELL,1UL},{18446744073709551607UL,0UL,0xEBF03D2EE0708B01LL},{18446744073709551615UL,18446744073709551610UL,1UL},{0xEBF03D2EE0708B01LL,18446744073709551607UL,0xC5D3A04B10BD57B8LL}},{{18446744073709551615UL,0x1FA8CE8CF57A53CELL,0x8633355B63474E96LL},{0xEBF03D2EE0708B01LL,0xEBF03D2EE0708B01LL,0x57A46841EF60A9D6LL},{18446744073709551615UL,1UL,0x8633355B63474E96LL},{0x9D73A0A794B48AD8LL,18446744073709551615UL,0x9D73A0A794B48AD8LL}},{{18446744073709551615UL,18446744073709551610UL,0x925B5DA658DAD5A3LL},{18446744073709551607UL,0x9D73A0A794B48AD8LL,0x9D73A0A794B48AD8LL},{0x925B5DA658DAD5A3LL,1UL,0x8633355B63474E96LL},{0xC5D3A04B10BD57B8LL,18446744073709551607UL,0xEBF03D2EE0708B01LL}}};
        int i, j, k;
        for (i = 0; i < 8; i++)
            l_726[i] = (-1L);
    }
    (*l_1270) = p_73;
    return p_72;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_10, "g_10", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_29, "g_29", print_hash_value);
    transparent_crc(g_41, "g_41", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_78[i], "g_78[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_91[i], sizeof(g_91[i]), "g_91[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_93, sizeof(g_93), "g_93", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_132, "g_132", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc_bytes(&g_135[i][j][k], sizeof(g_135[i][j][k]), "g_135[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_146, "g_146", print_hash_value);
    transparent_crc(g_163, "g_163", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_174[i], "g_174[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_183[i], "g_183[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_197, "g_197", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_249[i], "g_249[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_253, "g_253", print_hash_value);
    transparent_crc(g_305, "g_305", print_hash_value);
    transparent_crc(g_311, "g_311", print_hash_value);
    transparent_crc(g_414, "g_414", print_hash_value);
    transparent_crc(g_428, "g_428", print_hash_value);
    transparent_crc(g_465, "g_465", print_hash_value);
    transparent_crc(g_470, "g_470", print_hash_value);
    transparent_crc(g_667, "g_667", print_hash_value);
    transparent_crc(g_679, "g_679", print_hash_value);
    transparent_crc_bytes (&g_856, sizeof(g_856), "g_856", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_997[i], sizeof(g_997[i]), "g_997[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1095[i], "g_1095[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1164[i], "g_1164[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1180, "g_1180", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_1216[i][j][k], "g_1216[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1290[i], "g_1290[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1386, "g_1386", print_hash_value);
    transparent_crc(g_1428, "g_1428", print_hash_value);
    transparent_crc(g_1744, "g_1744", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2062[i], "g_2062[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2098, "g_2098", print_hash_value);
    transparent_crc(g_2180, "g_2180", print_hash_value);
    transparent_crc(g_2181, "g_2181", print_hash_value);
    transparent_crc(g_2184, "g_2184", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2239[i], "g_2239[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2284, "g_2284", print_hash_value);
    transparent_crc(g_2526, "g_2526", print_hash_value);
    transparent_crc(g_2646, "g_2646", print_hash_value);
    transparent_crc(g_2679, "g_2679", print_hash_value);
    transparent_crc(g_2764, "g_2764", print_hash_value);
    transparent_crc(g_2769, "g_2769", print_hash_value);
    transparent_crc_bytes (&g_2865, sizeof(g_2865), "g_2865", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2901[i], "g_2901[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2935, "g_2935", print_hash_value);
    transparent_crc(g_2991, "g_2991", print_hash_value);
    transparent_crc(g_3065, "g_3065", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_3074[i][j], "g_3074[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 836
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 38
breakdown:
   depth: 1, occurrence: 243
   depth: 2, occurrence: 59
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
   depth: 6, occurrence: 2
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 4
   depth: 17, occurrence: 1
   depth: 19, occurrence: 4
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 6
   depth: 23, occurrence: 1
   depth: 25, occurrence: 3
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 4
   depth: 29, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 35, occurrence: 1
   depth: 38, occurrence: 1

XXX total number of pointers: 688

XXX times a variable address is taken: 1692
XXX times a pointer is dereferenced on RHS: 603
breakdown:
   depth: 1, occurrence: 434
   depth: 2, occurrence: 132
   depth: 3, occurrence: 26
   depth: 4, occurrence: 9
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 429
breakdown:
   depth: 1, occurrence: 363
   depth: 2, occurrence: 35
   depth: 3, occurrence: 25
   depth: 4, occurrence: 5
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 63
XXX times a pointer is compared with address of another variable: 17
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 12200

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2134
   level: 2, occurrence: 525
   level: 3, occurrence: 328
   level: 4, occurrence: 195
   level: 5, occurrence: 99
XXX number of pointers point to pointers: 333
XXX number of pointers point to scalars: 355
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.1
XXX average alias set size: 1.47

XXX times a non-volatile is read: 2730
XXX times a non-volatile is write: 1224
XXX times a volatile is read: 212
XXX    times read thru a pointer: 80
XXX times a volatile is write: 103
XXX    times written thru a pointer: 65
XXX times a volatile is available for access: 2.4e+03
XXX percentage of non-volatile access: 92.6

XXX forward jumps: 4
XXX backward jumps: 11

XXX stmts: 237
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 30
   depth: 2, occurrence: 33
   depth: 3, occurrence: 30
   depth: 4, occurrence: 47
   depth: 5, occurrence: 63

XXX percentage a fresh-made variable is used: 15.9
XXX percentage an existing variable is used: 84.1
********************* end of statistics **********************/


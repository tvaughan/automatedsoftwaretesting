/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3720890019
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   uint32_t  f0;
};

union U1 {
   uint32_t  f0;
   uint16_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static uint32_t g_14 = 0x8373B2C5L;
static int32_t g_16 = 0L;
static int16_t g_19 = 0x1D6AL;
static int64_t g_39 = 1L;
static uint64_t g_40[1] = {0x6DAAFD55BE9843C6LL};
static union U1 g_44 = {1UL};
static union U1 * const g_43 = &g_44;
static union U1 ** volatile g_45 = (void*)0;/* VOLATILE GLOBAL g_45 */
static union U1 *g_58 = (void*)0;
static union U1 **g_57 = &g_58;
static uint64_t g_94 = 18446744073709551615UL;
static uint32_t g_103 = 2UL;
static uint32_t g_105 = 0x2603458EL;
static uint64_t g_108[2][9][3] = {{{7UL,1UL,0xF8FE0BD880D91824LL},{18446744073709551615UL,1UL,18446744073709551615UL},{18446744073709551612UL,0x279EC88C485E2798LL,0x3C7D9ACC98CB2CABLL},{18446744073709551615UL,0x415B52D1A88BB409LL,1UL},{1UL,18446744073709551615UL,18446744073709551615UL},{6UL,18446744073709551615UL,18446744073709551615UL},{1UL,0x2C1609A65E11ED7ALL,7UL},{18446744073709551615UL,2UL,18446744073709551609UL},{18446744073709551612UL,7UL,1UL}},{{18446744073709551615UL,1UL,0xD755E4DDAE416057LL},{7UL,7UL,2UL},{1UL,2UL,0x93945F7E1E514D86LL},{18446744073709551615UL,0x2C1609A65E11ED7ALL,0x9552485C7020513FLL},{18446744073709551609UL,18446744073709551615UL,2UL},{0x9552485C7020513FLL,18446744073709551615UL,0x9552485C7020513FLL},{0x415B52D1A88BB409LL,18446744073709551613UL,0xD755E4DDAE416057LL},{0x9552485C7020513FLL,0x2C1609A65E11ED7ALL,18446744073709551615UL},{6UL,0x4BECF96537570C98LL,18446744073709551615UL}}};
static float g_111[1][1] = {{(-0x4.3p-1)}};
static int64_t g_155 = (-2L);
static int32_t g_156 = 0x30BC2C7DL;
static int32_t *g_165 = &g_156;
static int32_t * volatile * volatile g_164 = &g_165;/* VOLATILE GLOBAL g_164 */
static uint8_t g_182 = 248UL;
static int32_t g_198 = (-5L);
static volatile int64_t g_203 = 1L;/* VOLATILE GLOBAL g_203 */
static int8_t g_204 = 0x31L;
static volatile uint16_t g_205 = 2UL;/* VOLATILE GLOBAL g_205 */
static int32_t ** const  volatile g_231 = &g_165;/* VOLATILE GLOBAL g_231 */
static uint8_t g_254 = 8UL;
static int32_t ** volatile g_258[2] = {&g_165,&g_165};
static int32_t ** volatile g_259[6][10] = {{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165}};
static float *g_287 = &g_111[0][0];
static int16_t g_308[2][6] = {{0xE8A5L,0xE8A5L,0xE8A5L,0xE8A5L,0xE8A5L,0xE8A5L},{0xE8A5L,0xE8A5L,0xE8A5L,0xE8A5L,0xE8A5L,0xE8A5L}};
static uint8_t * const *g_314 = (void*)0;
static int64_t *g_340 = &g_155;
static uint16_t *g_350 = &g_44.f1;
static uint8_t *g_372 = (void*)0;
static uint8_t **g_371[10] = {(void*)0,(void*)0,&g_372,(void*)0,(void*)0,&g_372,(void*)0,(void*)0,&g_372,(void*)0};
static uint8_t g_413[8] = {0xF0L,255UL,255UL,0xF0L,255UL,255UL,0xF0L,255UL};
static uint32_t g_455[10] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static int16_t *g_501 = (void*)0;
static union U0 g_506 = {1UL};
static int32_t ** volatile g_574[1] = {(void*)0};
static int32_t ** volatile g_575 = &g_165;/* VOLATILE GLOBAL g_575 */
static float g_599 = 0x0.3p-1;
static volatile int32_t g_606 = 1L;/* VOLATILE GLOBAL g_606 */
static volatile int32_t g_608 = 9L;/* VOLATILE GLOBAL g_608 */
static int16_t g_611 = 8L;
static volatile uint8_t g_612 = 0x6FL;/* VOLATILE GLOBAL g_612 */
static int32_t **g_641 = &g_165;
static volatile uint8_t *g_647 = &g_612;
static volatile uint8_t **g_646 = &g_647;
static volatile uint8_t ** volatile *g_645 = &g_646;
static volatile uint8_t ** volatile ** volatile g_644[7] = {&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645};
static uint16_t *** volatile g_650 = (void*)0;/* VOLATILE GLOBAL g_650 */
static const int32_t *g_717 = &g_16;
static const int32_t **g_716 = &g_717;
static const int32_t ***g_715 = &g_716;
static int8_t *g_719 = &g_204;
static union U0 *g_739[3][3][9] = {{{&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,(void*)0},{&g_506,&g_506,&g_506,&g_506,(void*)0,&g_506,&g_506,(void*)0,&g_506},{&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,(void*)0,(void*)0,&g_506}},{{&g_506,&g_506,&g_506,&g_506,(void*)0,&g_506,&g_506,&g_506,(void*)0},{&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,(void*)0},{&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506}},{{&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506,&g_506},{&g_506,&g_506,&g_506,&g_506,(void*)0,&g_506,(void*)0,&g_506,&g_506},{&g_506,(void*)0,&g_506,&g_506,(void*)0,&g_506,&g_506,&g_506,&g_506}}};
static int32_t g_765 = 7L;
static uint8_t * const **g_802 = (void*)0;
static int32_t * volatile g_850 = (void*)0;/* VOLATILE GLOBAL g_850 */
static int32_t * volatile g_851 = (void*)0;/* VOLATILE GLOBAL g_851 */
static int32_t * volatile g_852 = &g_765;/* VOLATILE GLOBAL g_852 */
static int64_t g_857 = 1L;
static int8_t g_888 = (-1L);
static float **g_967 = &g_287;
static float ***g_966[10][4] = {{&g_967,&g_967,&g_967,&g_967},{&g_967,&g_967,&g_967,&g_967},{(void*)0,&g_967,&g_967,&g_967},{&g_967,&g_967,(void*)0,&g_967},{&g_967,&g_967,&g_967,&g_967},{&g_967,&g_967,&g_967,&g_967},{&g_967,&g_967,&g_967,&g_967},{&g_967,&g_967,(void*)0,&g_967},{&g_967,(void*)0,&g_967,&g_967},{(void*)0,&g_967,&g_967,&g_967}};
static int32_t ***g_982 = (void*)0;
static int32_t ****g_981[8] = {&g_982,&g_982,&g_982,&g_982,&g_982,&g_982,&g_982,&g_982};
static int64_t *g_1121 = &g_857;
static int64_t **g_1165 = &g_340;
static union U1 ***g_1223 = (void*)0;
static union U1 ****g_1222 = &g_1223;
static union U0 *g_1227 = &g_506;
static union U0 ** volatile g_1226 = &g_1227;/* VOLATILE GLOBAL g_1226 */
static int8_t *g_1273 = (void*)0;
static int8_t g_1406 = 0L;
static int32_t g_1412 = 0x98D364C8L;
static volatile int16_t g_1414 = 0L;/* VOLATILE GLOBAL g_1414 */
static int32_t ** volatile g_1461 = &g_165;/* VOLATILE GLOBAL g_1461 */
static int64_t ***g_1481 = &g_1165;
static int64_t ****g_1480 = &g_1481;
static int32_t g_1487 = 0x854D94BBL;
static uint64_t g_1495 = 0x221CCE67B8324355LL;
static uint16_t g_1503 = 0xA49CL;
static int16_t g_1512 = 0x6B15L;
static float ****g_1521 = (void*)0;
static union U1 ** const  volatile g_1561 = (void*)0;/* VOLATILE GLOBAL g_1561 */
static union U1 ** volatile g_1562 = &g_58;/* VOLATILE GLOBAL g_1562 */
static const union U1 g_1618[6] = {{0x19DBC56EL},{0x19DBC56EL},{0x19DBC56EL},{0x19DBC56EL},{0x19DBC56EL},{0x19DBC56EL}};
static volatile uint8_t g_1635[5][2] = {{0x05L,0x05L},{255UL,0x05L},{0x05L,255UL},{0x05L,0x05L},{255UL,0x05L}};
static volatile uint32_t g_1682[8][1][6] = {{{0xE610DB90L,18446744073709551615UL,0x6B39DBCAL,18446744073709551615UL,0xE610DB90L,0xE610DB90L}},{{8UL,18446744073709551615UL,18446744073709551615UL,8UL,0x4B35922CL,8UL}},{{8UL,0x4B35922CL,8UL,18446744073709551615UL,18446744073709551615UL,8UL}},{{0xE610DB90L,0xE610DB90L,18446744073709551615UL,0x6B39DBCAL,18446744073709551615UL,0xE610DB90L}},{{18446744073709551615UL,0x4B35922CL,0x6B39DBCAL,0x6B39DBCAL,0x4B35922CL,18446744073709551615UL}},{{0xE610DB90L,18446744073709551615UL,0x6B39DBCAL,18446744073709551615UL,0xE610DB90L,0xE610DB90L}},{{8UL,18446744073709551615UL,18446744073709551615UL,8UL,0x4B35922CL,8UL}},{{8UL,0x4B35922CL,8UL,18446744073709551615UL,18446744073709551615UL,8UL}}};
static int64_t g_1710[8][1] = {{0x6FFE62ECBB0029FCLL},{(-3L)},{0x6FFE62ECBB0029FCLL},{(-3L)},{0x6FFE62ECBB0029FCLL},{(-3L)},{0x6FFE62ECBB0029FCLL},{(-3L)}};
static int32_t g_1732 = 0x58CA3222L;
static uint64_t *g_1868 = &g_40[0];
static uint64_t **g_1867 = &g_1868;
static uint64_t ***g_1866 = &g_1867;
static float g_1931 = 0xB.3E93EFp+98;
static int32_t g_2024 = 1L;
static float * volatile g_2063 = &g_599;/* VOLATILE GLOBAL g_2063 */
static volatile uint8_t g_2134 = 0x4EL;/* VOLATILE GLOBAL g_2134 */
static uint32_t g_2167[7] = {0x68747274L,0x68747274L,0x68747274L,0x68747274L,0x68747274L,0x68747274L,0x68747274L};
static volatile uint32_t g_2273 = 0x690FF2BBL;/* VOLATILE GLOBAL g_2273 */
static int64_t g_2294 = 0L;
static int32_t g_2311[4][8][3] = {{{0xE9A5DD67L,1L,0x7135A326L},{0xAD16C5A3L,9L,1L},{6L,(-4L),0x17409743L},{0x9C1BF340L,3L,0x6ECAD85AL},{9L,0x7135A326L,0L},{1L,0L,0xE7FFA859L},{0xBA799DC2L,0xF650C8FEL,0L},{0xBA799DC2L,0xB3D30209L,9L}},{{1L,0x49E87A50L,(-1L)},{9L,0xE9A5DD67L,0xE9A5DD67L},{0x9C1BF340L,0L,(-6L)},{6L,0xAD16C5A3L,(-1L)},{0xAD16C5A3L,0xC7EE07D6L,0L},{0xE9A5DD67L,0x35589DBCL,0x2AA19317L},{0x2AA19317L,0xC7EE07D6L,3L},{1L,0xAD16C5A3L,0xF650C8FEL}},{{0L,0L,0x35589DBCL},{1L,0xE9A5DD67L,0x7135A326L},{1L,0x2AA19317L,0xB3D30209L},{0xBD6F8217L,0x35589DBCL,0xE7FFA859L},{(-1L),(-2L),0xE7FFA859L},{0x49E87A50L,1L,0xB3D30209L},{0x6ECAD85AL,0xAD16C5A3L,0x7135A326L},{1L,0L,1L}},{{(-4L),0L,(-2L)},{0xE7FFA859L,(-2L),0L},{0L,(-8L),6L},{0x2AA19317L,(-6L),0xF650C8FEL},{0L,0x49E87A50L,(-1L)},{0xE7FFA859L,0xBA799DC2L,0xC7EE07D6L},{(-4L),0x17409743L,9L},{1L,0xC7EE07D6L,1L}}};
static int32_t * volatile g_2328 = &g_2311[0][6][1];/* VOLATILE GLOBAL g_2328 */
static uint64_t ** const **g_2343 = (void*)0;
static uint64_t ** const *** volatile g_2342[6] = {&g_2343,(void*)0,(void*)0,&g_2343,(void*)0,(void*)0};
static int32_t * volatile g_2347 = (void*)0;/* VOLATILE GLOBAL g_2347 */
static uint16_t g_2371 = 0xCA7BL;
static int64_t *****g_2383 = &g_1480;
static volatile int8_t g_2398 = 0L;/* VOLATILE GLOBAL g_2398 */
static int8_t **g_2419 = &g_1273;
static uint8_t ***g_2450 = &g_371[6];
static uint8_t ****g_2449[2] = {&g_2450,&g_2450};
static int16_t **g_2493 = &g_501;
static int16_t *** volatile g_2492 = &g_2493;/* VOLATILE GLOBAL g_2492 */
static uint64_t g_2544 = 8UL;
static uint64_t * const g_2543 = &g_2544;
static uint64_t * const *g_2542[4] = {&g_2543,&g_2543,&g_2543,&g_2543};
static uint64_t * const **g_2541 = &g_2542[3];
static uint64_t * const ***g_2540[2] = {&g_2541,&g_2541};
static uint64_t * const ****g_2539[5] = {&g_2540[0],&g_2540[0],&g_2540[0],&g_2540[0],&g_2540[0]};
static int64_t g_2755 = 0xA7518A81A47376F4LL;
static int32_t * const  volatile g_2863 = (void*)0;/* VOLATILE GLOBAL g_2863 */
static union U1 ** const **g_2900 = (void*)0;
static union U1 ** volatile g_3129[1] = {&g_58};
static union U1 ** volatile g_3131[3][1] = {{(void*)0},{(void*)0},{(void*)0}};
static uint64_t ****g_3154 = &g_1866;
static uint64_t *****g_3153[9] = {&g_3154,&g_3154,&g_3154,&g_3154,&g_3154,&g_3154,&g_3154,&g_3154,&g_3154};
static volatile float g_3167 = 0x4.Ep-1;/* VOLATILE GLOBAL g_3167 */
static volatile float * const  volatile g_3166 = &g_3167;/* VOLATILE GLOBAL g_3166 */
static int16_t g_3176 = 0x3D02L;
static const volatile int16_t g_3178[2] = {(-1L),(-1L)};
static volatile uint32_t g_3184 = 4294967289UL;/* VOLATILE GLOBAL g_3184 */
static uint16_t g_3264[10] = {3UL,3UL,3UL,3UL,3UL,3UL,3UL,3UL,3UL,3UL};
static volatile int32_t g_3345 = 0xF5C92F9AL;/* VOLATILE GLOBAL g_3345 */
static uint16_t g_3432 = 3UL;
static int16_t g_3744 = 0xD6F8L;
static volatile int32_t *g_3787 = (void*)0;
static volatile int32_t ** volatile g_3786 = &g_3787;/* VOLATILE GLOBAL g_3786 */
static int32_t * volatile g_3835 = &g_765;/* VOLATILE GLOBAL g_3835 */
static uint8_t *****g_3881 = &g_2449[0];
static uint16_t g_3896 = 0x519EL;
static uint32_t g_3902 = 1UL;
static uint64_t g_3941 = 0xF1B65F3FCE4E5E35LL;
static uint16_t **g_3956 = (void*)0;
static uint16_t ***g_3955 = &g_3956;
static uint16_t ****g_3954 = &g_3955;
static uint16_t *****g_3953 = &g_3954;
static volatile int32_t g_4124 = 0x57E1C4DEL;/* VOLATILE GLOBAL g_4124 */
static volatile uint32_t g_4127 = 2UL;/* VOLATILE GLOBAL g_4127 */


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static int32_t  func_2(uint8_t  p_3);
static int16_t  func_7(uint16_t  p_8, int8_t  p_9);
static union U1 * const  func_23(uint32_t  p_24);
static int32_t * func_48(int32_t  p_49, int32_t  p_50, float  p_51);
static uint32_t  func_54(union U1 ** p_55, int32_t * const  p_56);
static int32_t  func_64(int64_t  p_65, const union U1  p_66, int32_t * p_67, int32_t * p_68);
static int32_t  func_75(int32_t * p_76);
static uint16_t  func_79(union U1 * const * p_80, uint16_t  p_81, union U0  p_82);
static union U1 ** func_83(uint32_t  p_84, int32_t  p_85);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    int64_t l_4 = 0xCCCE1E2C0D37AD43LL;
    int32_t l_3965 = 5L;
    int32_t l_3966 = 1L;
    uint16_t ****l_3967 = &g_3955;
    uint16_t *****l_3968 = &g_3954;
    uint16_t * const *l_3973[2][7] = {{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}};
    uint64_t ***l_3978 = &g_1867;
    uint32_t l_3990 = 0x34B96DD7L;
    uint16_t **l_4025 = &g_350;
    uint16_t *** const l_4024 = &l_4025;
    uint16_t *** const *l_4023 = &l_4024;
    int32_t l_4062 = 0x73AA65A8L;
    uint32_t l_4070 = 7UL;
    uint8_t **l_4099 = &g_372;
    uint64_t l_4110[2];
    int32_t l_4123 = 0x1B2B62AAL;
    int32_t l_4126[8] = {0x99586B11L,0x99586B11L,0x99586B11L,0x99586B11L,0x99586B11L,0x99586B11L,0x99586B11L,0x99586B11L};
    int i, j;
    for (i = 0; i < 2; i++)
        l_4110[i] = 0x92EDD4D0CA96EDEALL;
    return l_3965;
}


/* ------------------------------------------ */
/* 
 * reads : g_16 g_2273 g_350 g_44.f1 g_108 g_765 g_1866 g_1867 g_1868 g_40 g_506 g_719 g_204 g_2383 g_1480 g_1481 g_1165 g_340 g_715 g_716 g_2543 g_2544 g_3166 g_3167 g_287 g_717 g_165 g_156 g_1222 g_111 g_647 g_612 g_413 g_155 g_641 g_3264 g_103 g_254 g_1227 g_1121 g_857 g_3345 g_3184 g_3154 g_2328 g_2311 g_1461 g_231 g_43 g_44 g_3178 g_3432 g_646 g_314 g_182 g_1503 g_967 g_575 g_2063 g_599 g_1412 g_2371 g_2541 g_2542 g_506.f0 g_3786 g_2167 g_3835 g_1406 g_3941 g_2450 g_371 g_164 g_1487
 * writes: g_16 g_40 g_155 g_1487 g_717 g_111 g_156 g_1223 g_44.f1 g_204 g_254 g_103 g_44 g_165 g_94 g_182 g_198 g_308 g_340 g_1481 g_857 g_506 g_1503 g_2294 g_2371 g_716 g_105 g_765 g_2167 g_1406 g_1866 g_3432 g_3941 g_3953
 */
static int32_t  func_2(uint8_t  p_3)
{ /* block id: 1 */
    uint8_t l_10 = 0x4DL;
    float **l_2266[2][10][8] = {{{&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{(void*)0,&g_287,&g_287,(void*)0,&g_287,(void*)0,&g_287,&g_287},{&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{&g_287,&g_287,&g_287,&g_287,&g_287,(void*)0,&g_287,(void*)0},{&g_287,&g_287,&g_287,(void*)0,&g_287,&g_287,&g_287,&g_287},{&g_287,(void*)0,(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287},{(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287,(void*)0,&g_287},{&g_287,&g_287,&g_287,(void*)0,&g_287,&g_287,&g_287,&g_287}},{{&g_287,(void*)0,(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287},{&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{(void*)0,(void*)0,&g_287,&g_287,&g_287,(void*)0,(void*)0,&g_287},{&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,(void*)0,&g_287},{&g_287,(void*)0,&g_287,(void*)0,&g_287,&g_287,&g_287,&g_287},{&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{(void*)0,(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{&g_287,(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287,&g_287},{(void*)0,(void*)0,(void*)0,&g_287,&g_287,&g_287,(void*)0,&g_287},{(void*)0,&g_287,(void*)0,&g_287,&g_287,&g_287,&g_287,&g_287}}};
    uint16_t **l_2372 = (void*)0;
    int32_t l_2373 = 4L;
    int64_t *l_2390 = &g_2294;
    uint8_t **l_2441[5];
    int16_t l_2451 = 1L;
    int32_t l_2498 = 0x9D5BE663L;
    int32_t l_2499[7][6] = {{9L,0x9CDD84AAL,0L,0x9CDD84AAL,9L,9L},{(-6L),0x9CDD84AAL,0x9CDD84AAL,(-6L),0x3928CDBBL,(-6L)},{(-6L),0x3928CDBBL,(-6L),0x9CDD84AAL,0x9CDD84AAL,(-6L)},{9L,9L,0x9CDD84AAL,0L,0x9CDD84AAL,9L},{0x9CDD84AAL,0x3928CDBBL,0L,0L,0x3928CDBBL,0x9CDD84AAL},{9L,0x9CDD84AAL,0L,0x9CDD84AAL,9L,9L},{(-6L),0x9CDD84AAL,0x9CDD84AAL,(-6L),0x3928CDBBL,(-6L)}};
    union U1 ***l_2592 = &g_57;
    const union U1 l_2647 = {7UL};
    union U0 *l_2671 = &g_506;
    int32_t l_2687 = 1L;
    union U1 *****l_2764[7][8] = {{&g_1222,&g_1222,&g_1222,(void*)0,(void*)0,&g_1222,&g_1222,&g_1222},{&g_1222,(void*)0,(void*)0,(void*)0,&g_1222,&g_1222,(void*)0,(void*)0},{&g_1222,&g_1222,(void*)0,(void*)0,(void*)0,&g_1222,&g_1222,(void*)0},{&g_1222,(void*)0,(void*)0,&g_1222,&g_1222,&g_1222,(void*)0,(void*)0},{(void*)0,&g_1222,(void*)0,(void*)0,&g_1222,(void*)0,&g_1222,(void*)0},{&g_1222,&g_1222,&g_1222,(void*)0,(void*)0,&g_1222,&g_1222,&g_1222},{&g_1222,(void*)0,(void*)0,(void*)0,&g_1222,&g_1222,(void*)0,(void*)0}};
    uint64_t ****l_2822 = &g_1866;
    uint64_t *****l_2821 = &l_2822;
    uint16_t l_2841 = 1UL;
    int16_t ** const *l_2861[1][8][1] = {{{&g_2493},{&g_2493},{&g_2493},{&g_2493},{&g_2493},{&g_2493},{&g_2493},{&g_2493}}};
    uint32_t l_2920 = 0x150522D7L;
    int8_t **l_2930 = (void*)0;
    uint64_t ****l_2934 = &g_1866;
    int32_t l_2962 = 0xDA3E527DL;
    int32_t l_2963 = 0xA78EC2E4L;
    uint64_t ****l_2969 = &g_1866;
    uint32_t l_3070 = 0x4A710649L;
    const float *l_3097 = &g_599;
    const float ** const l_3096 = &l_3097;
    const float ** const *l_3095[7][4] = {{&l_3096,&l_3096,&l_3096,&l_3096},{&l_3096,&l_3096,&l_3096,&l_3096},{&l_3096,&l_3096,&l_3096,&l_3096},{&l_3096,&l_3096,&l_3096,&l_3096},{&l_3096,&l_3096,&l_3096,&l_3096},{&l_3096,&l_3096,&l_3096,&l_3096},{&l_3096,&l_3096,&l_3096,&l_3096}};
    const float ** const ** const l_3094[1] = {&l_3095[6][1]};
    const float ** const ** const *l_3093 = &l_3094[0];
    const float * const ****l_3098 = (void*)0;
    uint32_t l_3156 = 4294967295UL;
    uint16_t l_3202[1][5];
    uint16_t l_3205 = 1UL;
    int16_t l_3252[4];
    int64_t l_3253 = 8L;
    uint32_t l_3254 = 1UL;
    uint32_t l_3294 = 4294967295UL;
    int32_t * const *l_3297 = &g_165;
    int32_t l_3321 = 0x1D21C1C6L;
    uint64_t l_3324 = 0UL;
    int8_t l_3353[10] = {0x64L,0x25L,0x92L,0x25L,0x64L,0x64L,0x25L,0x92L,0x25L,0x64L};
    const uint8_t **l_3547 = (void*)0;
    const uint8_t ***l_3546 = &l_3547;
    const uint8_t *** const *l_3545 = &l_3546;
    uint64_t l_3574 = 18446744073709551608UL;
    uint32_t l_3598[7][6][6] = {{{18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL,18446744073709551615UL},{0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L},{0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L},{18446744073709551615UL,18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL},{0xED05C0CEL,0xED7D6587L,0x1619F60CL,0x1619F60CL,0xED7D6587L,0xED05C0CEL},{18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL,18446744073709551615UL}},{{0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L},{0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L},{18446744073709551615UL,18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL},{0xED05C0CEL,0xED7D6587L,0x1619F60CL,0x1619F60CL,0xED7D6587L,0xED05C0CEL},{18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL,18446744073709551615UL},{0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L}},{{0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L},{18446744073709551615UL,18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL},{0xED05C0CEL,0xED7D6587L,0x1619F60CL,0x1619F60CL,0xED7D6587L,0xED05C0CEL},{18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL,18446744073709551615UL},{0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L},{0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L}},{{18446744073709551615UL,18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL},{0xED05C0CEL,0xED7D6587L,0x1619F60CL,0x1619F60CL,0xED7D6587L,0xED05C0CEL},{18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL,18446744073709551615UL},{0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L},{0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0xDD9183C9L},{18446744073709551615UL,18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL}},{{0xED05C0CEL,0xED7D6587L,0x1619F60CL,0x1619F60CL,0xED7D6587L,0xED05C0CEL},{18446744073709551615UL,0xED05C0CEL,0x1619F60CL,0xED05C0CEL,18446744073709551615UL,18446744073709551615UL},{0xDD9183C9L,0xED05C0CEL,0xED05C0CEL,0x1619F60CL,18446744073709551615UL,0x1619F60CL},{0x1619F60CL,18446744073709551615UL,0x1619F60CL,0xDD9183C9L,0xDD9183C9L,0x1619F60CL},{0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL},{0xDD9183C9L,18446744073709551615UL,0xED7D6587L,0xED7D6587L,18446744073709551615UL,0xDD9183C9L}},{{0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL},{0x1619F60CL,0xDD9183C9L,0xDD9183C9L,0x1619F60CL,18446744073709551615UL,0x1619F60CL},{0x1619F60CL,18446744073709551615UL,0x1619F60CL,0xDD9183C9L,0xDD9183C9L,0x1619F60CL},{0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL},{0xDD9183C9L,18446744073709551615UL,0xED7D6587L,0xED7D6587L,18446744073709551615UL,0xDD9183C9L},{0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL}},{{0x1619F60CL,0xDD9183C9L,0xDD9183C9L,0x1619F60CL,18446744073709551615UL,0x1619F60CL},{0x1619F60CL,18446744073709551615UL,0x1619F60CL,0xDD9183C9L,0xDD9183C9L,0x1619F60CL},{0xED05C0CEL,0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL},{0xDD9183C9L,18446744073709551615UL,0xED7D6587L,0xED7D6587L,18446744073709551615UL,0xDD9183C9L},{0xED05C0CEL,0xDD9183C9L,0xED7D6587L,0xDD9183C9L,0xED05C0CEL,0xED05C0CEL},{0x1619F60CL,0xDD9183C9L,0xDD9183C9L,0x1619F60CL,18446744073709551615UL,0x1619F60CL}}};
    int64_t l_3599[9] = {(-1L),(-1L),0x00419EC2552A1208LL,(-1L),(-1L),0x00419EC2552A1208LL,(-1L),(-1L),0x00419EC2552A1208LL};
    int32_t l_3706 = 0xA3E6BA5DL;
    float l_3727 = 0x3.58174Cp-68;
    uint64_t l_3754 = 5UL;
    int32_t l_3790 = 0xC7F06761L;
    uint16_t l_3791[5][3][4] = {{{65530UL,0xE05EL,0x1FC0L,2UL},{0x0ABBL,0x2213L,65535UL,65535UL},{0x0ABBL,0x0ABBL,0x1FC0L,0x9860L}},{{65530UL,65535UL,0x0ABBL,0xE05EL},{0x2213L,2UL,65535UL,0x0ABBL},{2UL,2UL,2UL,0xE05EL}},{{2UL,65535UL,65535UL,0x9860L},{0x9860L,0x0ABBL,0x2213L,65535UL},{2UL,0x2213L,0x2213L,2UL}},{{0x9860L,0xE05EL,65535UL,2UL},{2UL,0x9FB6L,2UL,0x1FC0L},{2UL,0x1FC0L,65535UL,0x1FC0L}},{{0x2213L,0x9FB6L,0x0ABBL,2UL},{65530UL,0xE05EL,0x1FC0L,2UL},{0x0ABBL,0x2213L,65535UL,65535UL}}};
    int32_t *l_3794 = &l_2373;
    union U1 ****l_3826 = &l_2592;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_2441[i] = &g_372;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
            l_3202[i][j] = 4UL;
    }
    for (i = 0; i < 4; i++)
        l_3252[i] = (-1L);
lbl_3618:
    if ((safe_div_func_int64_t_s_s((((func_7(l_10, l_10) > (p_3 ^ (p_3 <= (safe_add_func_uint16_t_u_u((((void*)0 != l_2266[0][3][3]) , ((safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(l_10, (((((safe_div_func_uint8_t_u_u(p_3, 0xBDL)) , 0x964DL) && 0xBFACL) > g_2273) < p_3))), (*g_350))) > 0xF8L)), g_108[1][7][2]))))) < (*g_350)) , 0x28D96123EFC1C862LL), l_10)))
    { /* block id: 975 */
        int8_t l_2284[6][5] = {{(-4L),0L,0x25L,0x6BL,5L},{0x25L,0L,(-4L),(-4L),0L},{9L,0xBEL,0L,0L,5L},{0xBEL,(-4L),0L,(-1L),0L},{5L,5L,(-4L),9L,1L},{0xBEL,1L,0x25L,0L,0L}};
        int64_t l_2315 = (-1L);
        int32_t l_2327 = 0x5839907CL;
        uint64_t ** const *l_2341[2];
        uint64_t ** const **l_2340[5][1][9] = {{{(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0]}},{{(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0]}},{{(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0]}},{{(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0]}},{{(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0],(void*)0,(void*)0,&l_2341[0]}}};
        float ****l_2408 = (void*)0;
        const uint8_t *l_2437 = &g_182;
        const uint8_t **l_2436 = &l_2437;
        const uint8_t ** const * const l_2435[10] = {&l_2436,&l_2436,&l_2436,&l_2436,&l_2436,&l_2436,&l_2436,&l_2436,&l_2436,&l_2436};
        int32_t *l_2453 = &g_765;
        int32_t l_2494 = (-1L);
        int32_t l_2495 = 0L;
        int32_t l_2496 = 0x5E8C976AL;
        int32_t l_2500 = (-9L);
        int32_t l_2501 = 1L;
        int32_t l_2502 = 0x5B529F70L;
        int32_t l_2503 = 0x52C5253EL;
        uint16_t ***l_2521 = (void*)0;
        union U1 l_2569 = {0x17608B0EL};
        int16_t l_2608 = (-5L);
        int32_t l_2664 = 0x1A69871AL;
        const int64_t l_2688 = (-1L);
        int64_t *** const *l_2718[4];
        int32_t l_2757 = 0xC3151456L;
        union U1 *****l_2763 = &g_1222;
        int32_t *l_2957 = &l_2687;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2341[i] = (void*)0;
        for (i = 0; i < 4; i++)
            l_2718[i] = &g_1481;
        for (g_16 = 21; (g_16 != 17); g_16 = safe_sub_func_uint64_t_u_u(g_16, 1))
        { /* block id: 978 */
            union U0 l_2291 = {0x7A13B07AL};
            int64_t ***l_2303 = &g_1165;
            int32_t l_2310 = 3L;
            const uint16_t l_2312 = 0xD79AL;
            int32_t ** const *l_2337 = &g_641;
            float ***l_2388 = &l_2266[0][3][3];
            const float l_2393[2] = {0x1.Fp-1,0x1.Fp-1};
            int32_t l_2402 = 7L;
            int8_t l_2468[7][9] = {{7L,(-6L),3L,0x17L,0xD3L,(-1L),(-1L),0xD3L,0x17L},{7L,1L,7L,(-1L),0xA5L,3L,0x8DL,0x8DL,3L},{3L,(-6L),7L,(-6L),3L,0x17L,0xD3L,(-1L),(-1L)},{(-6L),0xD5L,3L,(-1L),3L,0xD5L,(-6L),0xA5L,(-2L)},{0x8DL,(-2L),(-6L),0x17L,0xA5L,0x17L,(-6L),(-2L),0x8DL},{0xD5L,0x17L,0x8DL,0xA5L,0xD3L,3L,0xD3L,0xA5L,0x8DL},{0xD3L,0xD3L,0xD5L,7L,(-2L),(-1L),0x8DL,(-1L),(-2L)}};
            uint16_t **l_2473 = &g_350;
            uint64_t l_2504 = 18446744073709551615UL;
            int16_t l_2513 = 0L;
            uint8_t **l_2534 = &g_372;
            int8_t **l_2577 = (void*)0;
            uint64_t ** const *l_2600 = &g_1867;
            uint16_t l_2650[9][10] = {{3UL,1UL,1UL,3UL,0xCAF7L,0UL,0x6A46L,0UL,0x6A46L,0UL},{0xD1F0L,0x9E40L,0xCAF7L,0x9E40L,0xD1F0L,65531UL,1UL,0x6A46L,0x6A46L,1UL},{0UL,65531UL,3UL,3UL,65531UL,0UL,0x9A46L,1UL,0xD1F0L,1UL},{0x9E40L,3UL,0xD1F0L,0x6A46L,0xD1F0L,3UL,0x9E40L,0x9A46L,0UL,0UL},{0x9E40L,0UL,0UL,0xCAF7L,0xCAF7L,0UL,0UL,0x9E40L,65531UL,0x9A46L},{0UL,0UL,0x9E40L,65531UL,0x9A46L,65531UL,0x9E40L,0UL,0UL,0xCAF7L},{0xD1F0L,3UL,0x9E40L,0x9A46L,0UL,0UL,0x9A46L,0x9E40L,3UL,0UL},{1UL,3UL,0xCAF7L,0UL,0x6A46L,0UL,0x6A46L,0UL,0xCAF7L,3UL},{65531UL,65531UL,0UL,3UL,0x6A46L,0x9E40L,0x9E40L,0x6A46L,3UL,0UL}};
            const union U1 *l_2938[5][8] = {{&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4]},{(void*)0,&g_1618[4],(void*)0,(void*)0,&g_1618[4],(void*)0,(void*)0,&g_1618[4]},{&g_1618[4],(void*)0,(void*)0,&g_1618[4],(void*)0,(void*)0,&g_1618[4],(void*)0},{&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4],&g_1618[4]},{(void*)0,&g_1618[4],(void*)0,(void*)0,&g_1618[4],(void*)0,(void*)0,&g_1618[4]}};
            const union U1 **l_2937 = &l_2938[2][1];
            int i, j;
        }
        l_2373 &= (safe_add_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s((-2L), (safe_div_func_int32_t_s_s(0x673C8E27L, 1UL)))) , (((((*l_2453) & 0x201EA4BDFA95DB24LL) <= (safe_div_func_int8_t_s_s(((((!(safe_div_func_uint8_t_u_u((+((*****g_2383) = ((((((****l_2934)++) >= (((safe_sub_func_int16_t_s_s((safe_add_func_uint8_t_u_u((((((l_2957 = (void*)0) != (((0L & (safe_sub_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_s((l_2664 |= (p_3 = ((l_2962 = ((*l_2671) , (*l_2453))) , l_2687))), (*g_719))), 0x45054F20L))) | 18446744073709551613UL) , (void*)0)) > (*l_2453)) & (-1L)) , p_3), (*l_2453))), (*l_2453))) >= 0UL) & 0L)) & 1UL) , l_2499[4][4]) != 0UL))), (*l_2453)))) > 1UL) < (*l_2453)) & (*l_2453)), l_2963))) <= (*l_2453)) <= 0L)), 1UL));
    }
    else
    { /* block id: 1284 */
        int32_t l_2981 = 0xFFD7A1F1L;
        int32_t l_3035 = 0x29D7028CL;
        int32_t l_3059 = 0x047A4CCAL;
        int32_t l_3060 = 5L;
        int32_t l_3066 = 0x8A8A66E4L;
        int32_t l_3069 = 0x6A463B8AL;
        int16_t l_3111 = (-3L);
        int32_t l_3182 = 1L;
        int16_t l_3183 = 0x68B1L;
        int64_t l_3214 = 8L;
        union U1 ***l_3222 = &g_57;
        for (g_1487 = 0; (g_1487 <= 7); g_1487 += 1)
        { /* block id: 1287 */
            uint64_t **** const l_2966 = &g_1866;
            int32_t *l_2978 = &g_1732;
            int32_t *l_2982 = &g_2311[0][7][2];
            union U1 l_2983[1] = {{0xB48E57E9L}};
            int32_t l_3007 = (-1L);
            int32_t l_3058[3][9] = {{0xAB1EB5EDL,0x4E6CEAB7L,0xAB1EB5EDL,(-10L),0x5CE16E0DL,0x5CE16E0DL,(-10L),0xAB1EB5EDL,0x4E6CEAB7L},{0x4E6CEAB7L,0xAB1EB5EDL,(-10L),0x5CE16E0DL,0x5CE16E0DL,(-10L),0xAB1EB5EDL,0x4E6CEAB7L,0xAB1EB5EDL},{0x453B1550L,(-1L),(-10L),(-10L),(-1L),0x453B1550L,0x5CE16E0DL,0x453B1550L,(-1L)}};
            uint64_t l_3133 = 0UL;
            float *l_3165 = &g_111[0][0];
            int16_t l_3180 = 0x2952L;
            int i, j;
        }
        (**g_715) = &l_3066;
        if (((safe_div_func_uint64_t_u_u(((l_3066 > (l_2963 = (safe_rshift_func_int16_t_s_u((+(l_3059 &= (safe_lshift_func_uint8_t_u_s((safe_lshift_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u((((safe_mul_func_int8_t_s_s(0xF3L, (((0x0.5B2AD9p+28 <= (-0x7.7p-1)) > ((((*g_287) = (((((safe_rshift_func_int16_t_s_u(((3UL ^ l_3070) != l_2962), 12)) , (void*)0) != (void*)0) & (*g_2543)) , (*g_3166))) != p_3) < l_3202[0][4])) , 1UL))) < 0x602DL) != l_2373), 14)) == (-1L)), 2)), l_3156)))), 2)))) | l_3035), p_3)) && 0xCC52AD2CL))
        { /* block id: 1372 */
            int32_t *l_3203 = &g_16;
            int32_t *l_3204[8];
            int i;
            for (i = 0; i < 8; i++)
                l_3204[i] = (void*)0;
            (*g_165) |= (***g_715);
            ++l_3205;
        }
        else
        { /* block id: 1375 */
            int32_t l_3210 = 4L;
            float l_3211 = 0x9.0A9B42p-36;
            int32_t l_3212 = 0x64B81B67L;
            int32_t l_3213 = 0xDF82E8E4L;
            int32_t l_3215 = 2L;
            int32_t l_3216 = 1L;
            int16_t l_3217[3];
            int i;
            for (i = 0; i < 3; i++)
                l_3217[i] = 3L;
            if (p_3)
            { /* block id: 1376 */
                int32_t *l_3208 = &l_3066;
                int32_t *l_3209[6][1][7] = {{{(void*)0,&l_2499[4][1],(void*)0,(void*)0,&l_2499[4][1],(void*)0,(void*)0}},{{&g_2311[1][4][0],&g_16,&g_2311[3][2][1],&g_16,&g_2311[1][4][0],&l_2499[4][1],&g_2311[1][4][0]}},{{&l_2499[4][1],(void*)0,(void*)0,&l_2499[4][1],(void*)0,(void*)0,&l_2499[4][1]}},{{(void*)0,&g_16,(void*)0,&l_3059,&g_2311[1][4][0],&l_3059,(void*)0}},{{&l_2499[4][1],&l_2499[4][1],&g_16,&l_2499[4][1],&l_2499[4][1],&g_16,&l_2499[4][1]}},{{&g_2311[1][4][0],&l_3059,(void*)0,&g_16,(void*)0,&l_3059,&g_2311[1][4][0]}}};
                uint8_t l_3218 = 254UL;
                int i, j, k;
                l_3218--;
            }
            else
            { /* block id: 1378 */
                int8_t l_3223 = 6L;
                l_3059 = ((+(l_3223 &= (((*g_1222) = l_3222) == (void*)0))) > (safe_mul_func_int16_t_s_s((safe_sub_func_int8_t_s_s((((*g_350) = (safe_mul_func_uint16_t_u_u((*g_350), l_3182))) & ((void*)0 != &g_719)), ((safe_mul_func_float_f_f(((safe_mul_func_float_f_f(((l_2963 = (safe_sub_func_float_f_f(((0x0.Fp-1 == ((((+(safe_sub_func_float_f_f(((&g_198 != &g_1412) >= p_3), (*g_3166)))) != p_3) > 0x0.6B1C0Dp+96) < 0x0.D3E857p-39)) < l_3182), p_3))) == l_3059), p_3)) == (*g_287)), p_3)) , 0xCDL))), 0x0F40L)));
            }
        }
    }
    if (((safe_div_func_uint8_t_u_u((*g_647), (((l_2498 > (--p_3)) < 0x62D7L) & (safe_add_func_int64_t_s_s(((((safe_lshift_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(((!(safe_add_func_int32_t_s_s(l_2647.f1, (((((0x3DL >= (++l_3254)) & ((((safe_mul_func_int8_t_s_s(l_2647.f0, ((*g_719) = ((safe_mod_func_uint64_t_u_u((g_413[3] <= ((l_3252[3] , 0x0C431FA97963030BLL) > 0L)), l_3202[0][4])) & (**g_1165))))) & 0x8B81L) , 0xC50B2E5EL) < l_2962)) , g_40[0]) < 0x3C426650L) <= l_2451)))) > 0x55B0B8BBC81FA2C2LL), 4)), l_3205)) , l_3070) && 0x5DBFD005EB1CEA2ELL) | (**g_641)), (*****g_2383)))))) != (*g_350)))
    { /* block id: 1390 */
        int32_t l_3261 = 1L;
        int32_t *l_3262 = (void*)0;
        int32_t l_3283 = 2L;
        int32_t l_3284 = (-8L);
        int32_t l_3285 = 0xFDF71A6BL;
        int32_t l_3286 = (-9L);
        int32_t l_3290[4][9][7] = {{{0x612F766AL,1L,0x32E64A03L,0x612F766AL,0x1670A7D9L,0x612F766AL,0x32E64A03L},{0xAAD80AE1L,0xAAD80AE1L,0x1EC340D3L,0x8DCD46E7L,1L,0xC4C1B107L,0xAAD80AE1L},{0xAAD80AE1L,0x32E64A03L,1L,1L,1L,1L,1L},{0x612F766AL,0x1670A7D9L,0x612F766AL,0x32E64A03L,1L,0x612F766AL,0x1404E88AL},{1L,0x1670A7D9L,2L,0x8DCD46E7L,0x1670A7D9L,0x1EC340D3L,0x1670A7D9L},{1L,0x32E64A03L,0x32E64A03L,1L,0x1404E88AL,0x612F766AL,1L},{1L,0xAAD80AE1L,0x32E64A03L,1L,1L,1L,1L},{0xAAD80AE1L,1L,2L,1L,0xAAD80AE1L,0xC4C1B107L,1L},{1L,0x1404E88AL,0x612F766AL,1L,(-1L),2L,1L}},{{(-1L),1L,0xA4C4836DL,0xA4C4836DL,1L,(-1L),0xC4C1B107L},{1L,0x612F766AL,1L,1L,1L,0x8DCD46E7L,0x612F766AL},{0x32E64A03L,0x1EC340D3L,(-1L),0x1404E88AL,(-1L),0x1EC340D3L,0x32E64A03L},{0x1EC340D3L,0x612F766AL,0xA4C4836DL,(-1L),0x32E64A03L,0x1EC340D3L,(-1L)},{1L,1L,0x8DCD46E7L,0x612F766AL,0x612F766AL,0x8DCD46E7L,1L},{0x612F766AL,0xC4C1B107L,0xA4C4836DL,0x1404E88AL,0xC4C1B107L,(-1L),1L},{2L,0x612F766AL,(-1L),2L,1L,2L,(-1L)},{0x32E64A03L,0x32E64A03L,1L,0x1404E88AL,0x612F766AL,1L,0x32E64A03L},{0x32E64A03L,(-1L),0xA4C4836DL,0x612F766AL,0x1EC340D3L,0x1EC340D3L,0x612F766AL}},{{2L,1L,2L,(-1L),0x612F766AL,2L,0xC4C1B107L},{0x612F766AL,1L,0x1670A7D9L,0x1404E88AL,1L,1L,1L},{1L,(-1L),(-1L),1L,0xC4C1B107L,2L,0x612F766AL},{0x1EC340D3L,0x32E64A03L,(-1L),0xA4C4836DL,0x612F766AL,0x1EC340D3L,0x1EC340D3L},{0x32E64A03L,0x612F766AL,0x1670A7D9L,0x612F766AL,0x32E64A03L,1L,0x612F766AL},{1L,0xC4C1B107L,2L,0x612F766AL,(-1L),2L,1L},{(-1L),1L,0xA4C4836DL,0xA4C4836DL,1L,(-1L),0xC4C1B107L},{1L,0x612F766AL,1L,1L,1L,0x8DCD46E7L,0x612F766AL},{0x32E64A03L,0x1EC340D3L,(-1L),0x1404E88AL,(-1L),0x1EC340D3L,0x32E64A03L}},{{0x1EC340D3L,0x612F766AL,0xA4C4836DL,(-1L),0x32E64A03L,0x1EC340D3L,(-1L)},{1L,1L,0x8DCD46E7L,0x612F766AL,0x612F766AL,0x8DCD46E7L,1L},{0x612F766AL,0xC4C1B107L,0xA4C4836DL,0x1404E88AL,0xC4C1B107L,(-1L),1L},{2L,0x612F766AL,(-1L),2L,1L,2L,(-1L)},{0x32E64A03L,0x32E64A03L,1L,0x1404E88AL,0x612F766AL,1L,0x32E64A03L},{0x32E64A03L,(-1L),0xA4C4836DL,0x612F766AL,0x1EC340D3L,0x1EC340D3L,0x612F766AL},{2L,1L,2L,(-1L),0x612F766AL,2L,0xC4C1B107L},{0x612F766AL,1L,0x1670A7D9L,0x1404E88AL,1L,1L,1L},{1L,(-1L),(-1L),1L,0xC4C1B107L,0x1670A7D9L,2L}}};
        int64_t l_3311[9];
        int32_t l_3327 = 0x682A5439L;
        int32_t l_3328 = 0x03B71071L;
        uint64_t ***l_3339 = &g_1867;
        uint32_t l_3401 = 9UL;
        int64_t ***l_3463 = (void*)0;
        uint32_t l_3469[6][5][1] = {{{0x39A78DA0L},{0x7E8E38EDL},{0x7E8E38EDL},{0x39A78DA0L},{0x7E8E38EDL}},{{0x7E8E38EDL},{0x39A78DA0L},{0x7E8E38EDL},{0x7E8E38EDL},{0x39A78DA0L}},{{0x7E8E38EDL},{0x7E8E38EDL},{0x39A78DA0L},{0x7E8E38EDL},{0x7E8E38EDL}},{{0x39A78DA0L},{0x7E8E38EDL},{0x7E8E38EDL},{0x39A78DA0L},{0x7E8E38EDL}},{{0x7E8E38EDL},{0x39A78DA0L},{0x7E8E38EDL},{0x7E8E38EDL},{0x39A78DA0L}},{{0x7E8E38EDL},{0x7E8E38EDL},{0x39A78DA0L},{0x7E8E38EDL},{0x7E8E38EDL}}};
        union U1 l_3544 = {18446744073709551615UL};
        uint8_t ****l_3548[2][7][1] = {{{&g_2450},{(void*)0},{&g_2450},{(void*)0},{&g_2450},{(void*)0},{&g_2450}},{{(void*)0},{&g_2450},{(void*)0},{&g_2450},{(void*)0},{&g_2450},{(void*)0}}};
        int32_t ****l_3677[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint64_t l_3681 = 0xA6C7465255A7CBE4LL;
        int32_t l_3705 = 9L;
        uint16_t l_3729 = 1UL;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_3311[i] = 0xDB5EB6DF80263817LL;
        if ((l_3261 , p_3))
        { /* block id: 1391 */
            const int8_t l_3280 = 0L;
            int32_t l_3287 = 0x7EB74A10L;
            int32_t l_3288 = 1L;
            int32_t l_3289 = 0L;
            int32_t l_3291 = 1L;
            int32_t l_3292 = 0x1CEC2335L;
            int32_t l_3293 = 0xC1EF758DL;
            int32_t l_3322 = 0x5E318F9FL;
            int32_t l_3323 = 0x33A65E01L;
            uint32_t l_3331[1];
            int i;
            for (i = 0; i < 1; i++)
                l_3331[i] = 0UL;
lbl_3334:
            for (g_254 = 1; (g_254 <= 6); g_254 += 1)
            { /* block id: 1394 */
                const uint32_t l_3272 = 0UL;
                int32_t l_3282[2][1][2];
                int32_t **l_3298[10][4] = {{&l_3262,&g_165,&g_165,&l_3262},{&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&l_3262},{&g_165,&l_3262,&g_165,&l_3262},{(void*)0,&l_3262,&l_3262,&l_3262},{&g_165,&l_3262,&g_165,&l_3262},{&g_165,&g_165,&g_165,&g_165},{&l_3262,&g_165,&l_3262,&l_3262},{&l_3262,&g_165,&l_3262,&g_165},{(void*)0,&g_165,&l_3262,&l_3262}};
                int i, j, k;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 2; k++)
                            l_3282[i][j][k] = 0x61A2CF42L;
                    }
                }
                (*g_716) = l_3262;
                if (l_2962)
                    goto lbl_3334;
                for (g_155 = 4; (g_155 >= 1); g_155 -= 1)
                { /* block id: 1398 */
                    uint32_t *l_3267 = &g_103;
                    int32_t l_3275 = 0xEB9E6F02L;
                    uint32_t l_3278 = 0x86C49FCAL;
                    int32_t *l_3281[2];
                    int8_t l_3329 = 0x0FL;
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_3281[i] = &l_2962;
                    if (((safe_unary_minus_func_int8_t_s(g_3264[6])) && ((safe_sub_func_uint8_t_u_u(((((*g_340) != ((-1L) & (l_2373 ^= ((*g_340) ^ ((--(*l_3267)) <= 0L))))) != 0xE488L) >= (safe_div_func_uint64_t_u_u((((p_3 = (*g_647)) <= ((*g_719) > (l_3202[0][4] < l_3272))) > 1L), 0x179EAAF657D2FA77LL))), 0x97L)) , 6L)))
                    { /* block id: 1402 */
                        int i, j;
                        (*g_165) &= l_2499[4][5];
                        (**g_641) = (((l_2963 &= ((safe_mul_func_float_f_f((l_3275 = p_3), (p_3 == ((*g_287) = p_3)))) , (safe_sub_func_int16_t_s_s(l_3278, 1UL)))) & p_3) <= ((+(((p_3 , (p_3 , l_3280)) , l_2373) , 5L)) && 0xEA1A2C96372209F3LL));
                        (**g_715) = &l_2499[4][1];
                        if (l_3278)
                            continue;
                    }
                    else
                    { /* block id: 1410 */
                        return l_10;
                    }
                    l_3294++;
                    if ((l_3297 != l_3298[2][1]))
                    { /* block id: 1414 */
                        int16_t l_3299 = 0xFF15L;
                        union U1 l_3306[6][5] = {{{1UL},{18446744073709551615UL},{7UL},{7UL},{18446744073709551615UL}},{{18446744073709551610UL},{0UL},{0xB320D609L},{0xCFD2EADCL},{0xCFD2EADCL}},{{0UL},{3UL},{0UL},{7UL},{0UL}},{{1UL},{0xCB91811BL},{0xCFD2EADCL},{0xCB91811BL},{1UL}},{{0UL},{1UL},{3UL},{18446744073709551615UL},{3UL}},{{18446744073709551610UL},{18446744073709551610UL},{0xCFD2EADCL},{1UL},{1UL}}};
                        int32_t l_3312 = 0x272491C2L;
                        int i, j;
                        l_3299 = (**l_3297);
                        (**l_3297) &= ((safe_sub_func_uint64_t_u_u(((*g_1227) , p_3), (*g_1121))) >= (safe_mod_func_uint16_t_u_u(((*g_2543) != (safe_div_func_uint32_t_u_u(4294967293UL, ((l_3306[4][0] , (l_3312 = (safe_lshift_func_uint16_t_u_u(((void*)0 == l_3262), (((safe_add_func_int8_t_s_s(((9L < l_3311[1]) , p_3), 0L)) <= (*g_1121)) & 0xBDL))))) | 0x2093L)))), p_3)));
                        if ((*g_717))
                            break;
                    }
                    else
                    { /* block id: 1419 */
                        int64_t l_3313 = 1L;
                        int32_t l_3314 = (-9L);
                        int32_t l_3315 = 0x03563E0EL;
                        int32_t l_3316 = (-8L);
                        int32_t l_3317 = 0L;
                        int32_t l_3318 = 0x8AE6FD93L;
                        int32_t l_3319 = 0xC64200AEL;
                        int32_t l_3320[8][1][6] = {{{0xC8E222E1L,0L,0x85C79CFDL,0L,0x66C3E988L,0L}},{{0x85C79CFDL,0xC8E222E1L,0x85C79CFDL,0L,1L,(-1L)}},{{0x4D26A9DAL,0L,0L,1L,(-1L),(-1L)}},{{1L,(-1L),(-1L),1L,0L,0L}},{{0x4D26A9DAL,(-1L),1L,0L,0x85C79CFDL,0xC8E222E1L}},{{0x85C79CFDL,0L,0x66C3E988L,0L,0x85C79CFDL,0L}},{{0xC8E222E1L,(-1L),0x81536E1BL,0x8FC9E34BL,0L,1L}},{{1L,(-1L),(-1L),(-1L),(-1L),1L}}};
                        int64_t l_3330 = 0xC3BCBEC75FBD47FELL;
                        int i, j, k;
                        ++l_3324;
                        l_3331[0]++;
                        if (p_3)
                            break;
                    }
                }
            }
            if ((safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_u(0x0484L, 7)), (p_3 <= (((**l_2821) == l_3339) || 65533UL)))))
            { /* block id: 1427 */
                int32_t l_3344 = 0x82E4611AL;
                int32_t l_3364 = (-1L);
                int32_t l_3365[2][6] = {{(-8L),0L,0L,(-8L),0L,0L},{(-8L),0L,0L,(-8L),0L,0L}};
                int i, j;
                for (g_254 = 0; (g_254 > 33); g_254 = safe_add_func_uint8_t_u_u(g_254, 8))
                { /* block id: 1430 */
                    uint16_t l_3366 = 0xF808L;
                    for (l_3324 = 0; (l_3324 != 21); ++l_3324)
                    { /* block id: 1433 */
                        union U0 l_3352[3][5] = {{{18446744073709551615UL},{1UL},{1UL},{18446744073709551615UL},{1UL}},{{18446744073709551615UL},{18446744073709551615UL},{0xBFD0E194L},{18446744073709551615UL},{18446744073709551615UL}},{{0xBFD0E194L},{1UL},{0xBFD0E194L},{0xBFD0E194L},{1UL}}};
                        int32_t l_3354 = 0x6B2141F9L;
                        int32_t *l_3355 = (void*)0;
                        int32_t *l_3356 = &l_3261;
                        int32_t *l_3357 = &l_3285;
                        int32_t *l_3358 = &g_2311[1][4][0];
                        int32_t *l_3359 = &l_3322;
                        int32_t *l_3360 = &l_3321;
                        int32_t *l_3361 = (void*)0;
                        int32_t *l_3362 = &l_3293;
                        int32_t *l_3363[4] = {&l_3322,&l_3322,&l_3322,&l_3322};
                        int i, j;
                        if (l_3344)
                            break;
                        if (g_3345)
                            break;
                        l_3354 &= (((((((**l_3297) = (safe_add_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s(((p_3 || ((l_3352[0][1] , ((*g_350) < (p_3 >= ((g_3184 , (((void*)0 == (**g_3154)) < (p_3 & ((((*g_350) | 0x7103L) != 0x4906L) == p_3)))) | (-3L))))) ^ l_3323)) == 1UL), 15)), p_3)), (**l_3297)))) == p_3) < 1L) < (***g_1481)) || l_3353[7]) == (*g_350));
                        --l_3366;
                    }
                }
            }
            else
            { /* block id: 1441 */
                uint8_t l_3369 = 7UL;
                return l_3369;
            }
        }
        else
        { /* block id: 1444 */
            uint16_t l_3370 = 0xBED6L;
            int32_t l_3385 = 0x5FF6E9F2L;
            int32_t l_3386[9];
            int32_t ****l_3456 = (void*)0;
            union U0 l_3484 = {0x6407A2E9L};
            const int16_t *l_3550[3];
            const int16_t **l_3549 = &l_3550[0];
            int32_t l_3566 = (-1L);
            union U1 l_3575 = {8UL};
            union U1 *****l_3608 = &g_1222;
            uint8_t *****l_3616 = &l_3548[1][5][0];
            uint8_t *****l_3617 = &l_3548[1][4][0];
            uint32_t *l_3761 = &g_103;
            int32_t l_3788 = (-1L);
            int i;
            for (i = 0; i < 9; i++)
                l_3386[i] = 0x5BE79CCCL;
            for (i = 0; i < 3; i++)
                l_3550[i] = &l_3252[0];
            if (l_3370)
            { /* block id: 1445 */
                uint32_t l_3387 = 0x5D786254L;
                int32_t l_3392 = 0xA4ADB8D4L;
                int32_t l_3395 = 0xA29683BEL;
                int32_t l_3396 = (-2L);
                int32_t l_3397 = (-9L);
                int32_t l_3399 = 0L;
                int32_t l_3400 = 1L;
                union U1 l_3404 = {0xEED0F8DDL};
                const float **l_3450[6];
                int64_t **l_3453[10] = {&g_340,&g_340,&g_340,&g_340,&g_340,&g_340,&g_340,&g_340,&g_340,&g_340};
                int32_t l_3500[3];
                int64_t *l_3573 = &g_2294;
                union U0 l_3595 = {0UL};
                int i;
                for (i = 0; i < 6; i++)
                    l_3450[i] = (void*)0;
                for (i = 0; i < 3; i++)
                    l_3500[i] = 1L;
                if ((*g_2328))
                { /* block id: 1446 */
                    int32_t *l_3371 = (void*)0;
                    int32_t *l_3372 = &l_3283;
                    int32_t *l_3373 = &l_3285;
                    int32_t *l_3374 = &l_2499[3][0];
                    int32_t *l_3375 = &l_2962;
                    int32_t *l_3376 = &l_3261;
                    int32_t *l_3377 = &l_3286;
                    int32_t *l_3378 = (void*)0;
                    int32_t *l_3379 = &l_2498;
                    int32_t *l_3380 = (void*)0;
                    int32_t *l_3381 = (void*)0;
                    int32_t *l_3382 = &l_3261;
                    int32_t *l_3383 = &l_3285;
                    int32_t *l_3384[6][8] = {{&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3],&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3]},{&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3],&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3]},{&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3],&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3]},{&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3],&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3]},{&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3],&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3]},{&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3],&l_3290[1][2][3],&l_3321,&l_3321,&l_3290[1][2][3]}};
                    int i, j;
                    ++l_3387;
                    (*g_716) = l_3379;
                    for (l_2963 = 24; (l_2963 != 3); l_2963 = safe_sub_func_int64_t_s_s(l_2963, 7))
                    { /* block id: 1451 */
                        int16_t l_3393 = (-2L);
                        int32_t l_3394 = 0L;
                        int32_t l_3398 = 1L;
                        if (p_3)
                            break;
                        ++l_3401;
                    }
                    if (((l_3400 , l_3404) , ((-1L) >= ((safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(((**l_3297) ^ (safe_lshift_func_uint16_t_u_s((*g_350), 6))), 0x53L)), (p_3 ^ ((l_3386[0] |= ((0x6.FDBDC5p+44 < (safe_sub_func_float_f_f((safe_div_func_float_f_f((*g_3166), 0x2.B88CFAp+42)), p_3))) , l_3396)) ^ l_3392)))) < (**l_3297)))))
                    { /* block id: 1456 */
                        (*g_287) = 0x4.CEC893p+83;
                    }
                    else
                    { /* block id: 1458 */
                        (*l_3377) &= (**g_716);
                        (*l_3383) &= (**g_1461);
                    }
                }
                else
                { /* block id: 1462 */
                    int16_t **l_3425 = (void*)0;
                    int32_t l_3433[3];
                    uint8_t l_3438[9] = {0xFDL,0xFDL,0xFDL,0xFDL,0xFDL,0xFDL,0xFDL,0xFDL,0xFDL};
                    int i;
                    for (i = 0; i < 3; i++)
                        l_3433[i] = 0x6F848ED6L;
                    l_3386[7] ^= (**g_231);
                    for (l_3404.f1 = 23; (l_3404.f1 >= 60); l_3404.f1++)
                    { /* block id: 1466 */
                        int64_t l_3431 = (-1L);
                        l_3395 = (safe_lshift_func_uint8_t_u_s((safe_mod_func_int64_t_s_s((((safe_mod_func_int64_t_s_s((safe_sub_func_uint32_t_u_u(((*g_43) , (l_3425 != (void*)0)), g_3178[1])), p_3)) , (+(safe_mul_func_int8_t_s_s(p_3, (0x7F10L || (((((safe_sub_func_uint32_t_u_u((0x1058D33E46B5E5E4LL >= (((l_3386[2] & p_3) > 0x12L) || 5UL)), l_3431)) & (*g_719)) , p_3) == l_3399) , p_3)))))) & g_3432), (-9L))), 5));
                        if ((**g_641))
                            break;
                        (**g_641) |= (p_3 & ((((((p_3 > ((((((*g_43) = (*g_43)) , p_3) < l_3433[0]) && (**g_646)) | p_3)) >= (safe_mul_func_uint16_t_u_u((l_3385 > (safe_lshift_func_int16_t_s_u((((l_3433[1] != l_3438[6]) || l_3438[6]) && p_3), 9))), l_3399))) > p_3) , 0xC877L) , (void*)0) != (*g_1480)));
                    }
                }
                (*g_641) = &l_3386[3];
                if ((safe_rshift_func_uint8_t_u_u(((safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s((safe_add_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u((p_3 == p_3), 0L)) & (!((void*)0 == l_3450[3]))) , func_83(((**l_3297) != (safe_lshift_func_int8_t_s_s(l_3387, 4))), ((void*)0 == l_3453[9]))) != (void*)0), (-1L))), p_3)), l_3387)) || (*g_350)), 3)))
                { /* block id: 1474 */
                    (*g_716) = ((*g_641) = &l_3285);
                }
                else
                { /* block id: 1477 */
                    uint32_t l_3468 = 0x83CB72DAL;
                    int32_t *l_3470 = &l_2498;
                    const union U1 l_3474 = {0x078CDC72L};
                    const uint32_t l_3480 = 0x92DED8AFL;
                    int32_t l_3524[5] = {(-2L),(-2L),(-2L),(-2L),(-2L)};
                    int32_t l_3525 = 0xFD593DF3L;
                    int16_t **l_3551[9] = {&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501};
                    int i;
                    (*l_3470) &= (((safe_lshift_func_uint8_t_u_s((**l_3297), 7)) == ((((&g_715 == l_3456) && (((((safe_rshift_func_uint16_t_u_u((**l_3297), (safe_sub_func_uint16_t_u_u((safe_div_func_int64_t_s_s(((*g_1121) = ((((*g_1480) = l_3463) != l_3463) != ((safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((l_3468 >= p_3), ((0x85389BF0F0DB6933LL != p_3) == l_3469[1][2][0]))), p_3)) ^ 0xF7CB6AC7L))), 0x47C40128C5447C7ELL)), (*g_350))))) | p_3) <= l_3387) , 0xCD25L) || p_3)) > 0x30811BBCL) , 1UL)) || 0x23L);
                    (**g_641) = (+((safe_rshift_func_uint8_t_u_u(p_3, (((p_3 < (l_3474 , ((*g_719) == (safe_unary_minus_func_int16_t_s(p_3))))) > (**l_3297)) != ((*l_3470) = p_3)))) > 9UL));
                    for (l_3156 = 0; (l_3156 == 2); l_3156 = safe_add_func_int64_t_s_s(l_3156, 4))
                    { /* block id: 1485 */
                        int64_t l_3485 = 1L;
                        uint32_t *l_3499 = &g_506.f0;
                        uint16_t *l_3501[9];
                        int32_t l_3502 = 0L;
                        int32_t *l_3503 = &g_1732;
                        int32_t *l_3504 = &l_2498;
                        int32_t *l_3505 = &l_3386[3];
                        int32_t *l_3506 = (void*)0;
                        int32_t *l_3507 = (void*)0;
                        int32_t *l_3508 = &g_765;
                        int32_t *l_3509 = (void*)0;
                        int32_t *l_3510 = &l_2373;
                        int32_t *l_3511 = &l_3290[0][4][5];
                        int32_t *l_3512 = &l_3397;
                        int32_t *l_3513 = (void*)0;
                        int32_t *l_3514 = &l_3283;
                        int32_t l_3515 = (-4L);
                        int32_t *l_3516 = &l_3283;
                        int32_t *l_3517 = &l_3385;
                        int32_t l_3518 = (-1L);
                        int32_t *l_3519 = (void*)0;
                        int32_t *l_3520 = &l_3400;
                        int32_t *l_3521 = &l_3290[1][2][3];
                        int32_t l_3522 = 0xB80DDF2BL;
                        int32_t *l_3523[6][8] = {{&l_3386[8],&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7]},{&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7],&l_3386[8]},{&l_3386[7],&l_3386[7],&l_2498,&l_3386[7],&l_3386[7],&l_2498,&l_3386[7],&l_3386[7]},{&l_3386[8],&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7]},{&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7],&l_3386[8],&l_3386[8],&l_3386[7],&l_3386[8]},{&l_3386[7],&l_3386[7],&l_2498,&l_3386[7],&l_3386[7],&l_2498,&l_3386[7],&l_3386[7]}};
                        uint32_t l_3526 = 0xB68011F2L;
                        int i, j;
                        for (i = 0; i < 9; i++)
                            l_3501[i] = &l_3370;
                        l_3502 &= ((g_1503 |= (((**l_3297) , (0xC5F64C81L != (safe_lshift_func_uint16_t_u_u((l_3480 , (l_3386[8] = ((+(safe_div_func_int32_t_s_s(((((*g_1227) = l_3484) , l_3485) && ((l_3392 ^ ((safe_div_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((0x9903E24EL & (safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(p_3, 7)), 8)), (safe_unary_minus_func_uint16_t_u(((*g_350) = ((((*l_3499) = (((p_3 , 18446744073709551615UL) < p_3) ^ 7L)) , (*l_3470)) >= p_3))))))), 6)), l_3500[1])), l_3485)) < p_3)) != p_3)), 4294967295UL))) && (*g_719)))), 5)))) != 0x3BD69EFAC89175FELL)) || 3L);
                        l_3526--;
                        (*g_641) = &l_3395;
                        (*l_3512) &= (safe_mod_func_uint32_t_u_u(((((safe_lshift_func_uint16_t_u_s((((*l_3470) >= p_3) <= ((*l_3521) &= (safe_add_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((((((safe_add_func_uint64_t_u_u(l_3396, (*l_3517))) , (safe_lshift_func_int8_t_s_u(((*g_719) &= (safe_rshift_func_int16_t_s_s(l_3566, (0L >= (((**l_3297) |= (safe_sub_func_uint32_t_u_u((((-1L) >= (safe_mul_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_s((l_3573 != (void*)0), (*l_3505))) ^ 0L), p_3))) | p_3), 0x03043534L))) && (*l_3470)))))), l_3400))) && (**l_3297)) > 1UL) || (*l_3470)), 1UL)), p_3)))), p_3)) && 0x3C0C8651C058E437LL) && l_3574) != 0x7F9F96CDL), p_3));
                    }
                    for (l_3396 = 1; (l_3396 >= 0); l_3396 -= 1)
                    { /* block id: 1508 */
                        int16_t l_3588 = 0L;
                        (**l_3297) = (p_3 != ((l_3575 , (safe_mod_func_uint16_t_u_u(0x25A6L, (-1L)))) , ((**g_967) = ((((l_3392 = (safe_mul_func_float_f_f((0x9.2EE791p+91 >= 0xD.F4366Ep-19), (safe_add_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f(0x7.4p+1, (&l_3524[3] == (void*)0))), (*l_3470))), 0x7.2E37B9p-51)), (**g_967)))))) > l_3588) >= p_3) != (-0x6.Fp-1)))));
                        (**l_3297) |= (((*g_350) = (((safe_div_func_uint64_t_u_u((p_3 ^ (*g_2543)), (safe_rshift_func_int16_t_s_u((l_3385 = p_3), (l_3595 , 2UL))))) != 0UL) != (0x174CD455CC86F8BALL < (((safe_mul_func_uint16_t_u_u((&g_2493 == (void*)0), l_3598[0][3][0])) == l_3399) && p_3)))) > 0xC6F7L);
                        if (l_3599[5])
                            break;
                    }
                }
            }
            else
            { /* block id: 1518 */
                int32_t *l_3613 = (void*)0;
                int32_t *l_3614 = (void*)0;
                int32_t *l_3615 = &l_2373;
                int32_t l_3680 = 9L;
                int32_t l_3736 = 0x8AD3AF46L;
                int32_t l_3737 = (-1L);
                int32_t l_3739 = 0L;
                int32_t l_3740 = (-1L);
                int32_t l_3741 = 0L;
                int32_t l_3745 = 0xA998E43EL;
                int32_t l_3749 = 0xBB34C820L;
                int32_t l_3753 = 0L;
                if ((0L | (safe_mul_func_int16_t_s_s(((((((l_3616 = (((**g_1165) = (***g_1481)) , ((safe_unary_minus_func_int64_t_s((safe_div_func_uint32_t_u_u(((p_3 ^ (safe_mul_func_int8_t_s_s(((*g_719) = (!((&g_1222 == (l_3608 = l_3608)) , ((7UL != ((*g_165) = p_3)) >= (((*l_3615) ^= (safe_rshift_func_uint16_t_u_s(3UL, 2))) > (((g_3264[6] , 1L) , 0xA63A16B4F88DA2AFLL) < p_3)))))), 0x10L))) < p_3), p_3)))) , (void*)0))) == l_3617) >= p_3) || 0x470C1A60L) , (**g_575)) > p_3), (*g_350)))))
                { /* block id: 1525 */
                    int32_t l_3624 = 6L;
                    int32_t l_3625 = 3L;
                    int32_t l_3628 = 0x57BD8B17L;
                    uint32_t l_3642 = 6UL;
lbl_3643:
                    if (l_3294)
                        goto lbl_3618;
                    if ((((*g_340) | ((*l_2390) = (!p_3))) | (((safe_mod_func_uint16_t_u_u(65535UL, (l_3625 = (safe_mul_func_int8_t_s_s((p_3 <= l_3624), 0x8DL))))) > (safe_sub_func_uint64_t_u_u((l_3628 ^= p_3), (safe_mul_func_uint8_t_u_u(p_3, (safe_div_func_int32_t_s_s((safe_unary_minus_func_int16_t_s((safe_rshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s((safe_add_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(p_3, p_3)), 0xDDAE2ABEL)), l_3642)), 2)))), l_3624))))))) > 0x36CEL)))
                    { /* block id: 1530 */
                        if (g_857)
                            goto lbl_3643;
                    }
                    else
                    { /* block id: 1532 */
                        int16_t l_3656[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
                        int32_t l_3672 = (-7L);
                        int32_t *l_3673[8] = {&l_2962,&l_2962,&l_2962,&l_2962,&l_2962,&l_2962,&l_2962,&l_2962};
                        uint32_t l_3674 = 0x7B64883BL;
                        int i;
                        (*l_3615) &= ((**l_3297) && 0L);
                        (*g_641) = &l_3290[0][8][5];
                        (*l_3615) ^= (((safe_sub_func_int32_t_s_s(((safe_div_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(4294967294UL, (safe_lshift_func_int16_t_s_u((((safe_unary_minus_func_uint16_t_u((l_3628 = (safe_add_func_int64_t_s_s((safe_unary_minus_func_int8_t_s(0x75L)), (**l_3297)))))) <= (l_3656[3] <= (((((safe_add_func_float_f_f(((safe_div_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f(0x3.Fp-1, (+(((((safe_sub_func_float_f_f((safe_div_func_float_f_f((**g_967), (-(**g_967)))), (!(l_3672 = (l_3625 = p_3))))) == ((-0x1.9p+1) > (**g_967))) == p_3) > 0xE.D77278p-70) == 0x0.Dp+1)))), p_3)), 0x8.Cp-1)) > (*g_2063)), (-0x5.9p-1))) != (-0x5.2p+1)) == (-0x1.5p-1)) == l_3656[3]) , 0xE855L))) < 9UL), (*g_350))))), p_3)) | (-1L)), (*g_165))) >= p_3) || p_3);
                        l_3674--;
                    }
                    l_3677[0] = &g_982;
                    for (l_3205 = (-2); (l_3205 <= 3); l_3205 = safe_add_func_int64_t_s_s(l_3205, 8))
                    { /* block id: 1544 */
                        uint32_t l_3701 = 0x3C45306CL;
                        int32_t *l_3704 = &l_3283;
                        ++l_3681;
                        l_2962 ^= (p_3 , (safe_add_func_uint64_t_u_u((safe_sub_func_int32_t_s_s(((**g_641) ^= (safe_lshift_func_int8_t_s_u((((--p_3) != (**g_646)) ^ l_3625), 3))), ((l_3628 && 2L) > (safe_unary_minus_func_uint64_t_u(((1L & (safe_sub_func_int64_t_s_s(((*l_2390) = (safe_add_func_uint32_t_u_u((((*l_3615) = (safe_add_func_uint64_t_u_u((0x915FL | (safe_mul_func_uint16_t_u_u(((((l_3701 < (safe_mod_func_int64_t_s_s((l_3704 == (void*)0), (*l_3615)))) && l_3705) ^ 5L) >= 0x21L), (*l_3704)))), 9UL))) < l_3706), g_16))), 7L))) <= g_1412)))))), 0x74107404260EDF34LL)));
                        if (l_3575.f1)
                            break;
                        if (p_3)
                            continue;
                    }
                }
                else
                { /* block id: 1554 */
                    uint32_t l_3719 = 0x873A2AFBL;
                    int32_t l_3728 = 1L;
                    int32_t l_3731 = 0x9A96550BL;
                    int32_t l_3732 = 0xE5FBBD3CL;
                    int32_t l_3733 = 1L;
                    int32_t l_3734 = (-1L);
                    int32_t l_3735 = 0x96DD0659L;
                    int32_t l_3738 = 0L;
                    int32_t l_3743 = 0L;
                    int32_t l_3746 = 0x616A247EL;
                    const uint32_t *l_3760 = &g_2167[4];
                    for (l_2963 = 0; (l_2963 != (-16)); --l_2963)
                    { /* block id: 1557 */
                        const uint32_t *l_3720 = &g_506.f0;
                        uint16_t *l_3721 = &g_2371;
                        int32_t l_3730 = 0xF0C23DB8L;
                        int32_t l_3742 = (-6L);
                        int32_t l_3747 = (-1L);
                        int32_t l_3748 = 0x916C5EF5L;
                        int32_t l_3750 = (-9L);
                        int32_t l_3751 = 0xE278B997L;
                        int32_t l_3752 = 0L;
                        (**g_967) = (((safe_lshift_func_int16_t_s_s((**l_3297), 4)) && 0UL) , ((((safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s((((*g_719) = (safe_rshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s((l_3719 = p_3), p_3)), 0))) ^ ((void*)0 == l_3720)), (((*l_3721) ^= (*g_350)) , (((safe_rshift_func_int8_t_s_u(p_3, (safe_div_func_int8_t_s_s((((l_3728 = (safe_unary_minus_func_int32_t_s((((*l_3615) == 255UL) || 0xE35C9999L)))) & p_3) < 0x1DA87090L), l_3729)))) <= (***g_2541)) | p_3)))), (*g_350))) , p_3) == p_3) <= (*g_2063)));
                        --l_3754;
                        if (g_44.f1)
                            goto lbl_3618;
                    }
                    l_3732 = ((+((safe_mul_func_float_f_f(0x8.3C0A30p-66, l_3743)) != ((l_3760 == l_3761) , ((*g_287) = (safe_sub_func_float_f_f(((safe_rshift_func_int8_t_s_s((safe_sub_func_int64_t_s_s((safe_div_func_uint32_t_u_u((65532UL ^ (*g_350)), 0xA0DED4B2L)), 0L)), 1)) , (((**g_967) >= p_3) == (-0x3.Ap+1))), (**l_3297))))))) > p_3);
                }
            }
            for (g_506.f0 = 0; (g_506.f0 != 19); ++g_506.f0)
            { /* block id: 1572 */
                float * const l_3776 = &g_111[0][0];
                int32_t l_3777 = (-7L);
                int64_t l_3778 = 0xAF36228E47CC4E45LL;
                int8_t *l_3789 = &l_3353[2];
                l_2373 = (((*l_3789) = ((p_3 != (safe_div_func_uint16_t_u_u(((((((safe_sub_func_uint16_t_u_u((((p_3 , (((void*)0 != l_3776) < ((l_3777 == l_3778) == (safe_mul_func_int8_t_s_s((((safe_add_func_int8_t_s_s((+(safe_lshift_func_uint8_t_u_s(0x18L, l_3778))), ((*g_719) = l_3777))) > l_3777) < 0x2CL), 7UL))))) != 0x91FD30C00E4057FFLL) >= 0xF0176791L), 0x08E5L)) , (void*)0) == g_3786) | l_3788) | 0x263FD095L) || p_3), 8L))) >= 4294967295UL)) ^ l_3790);
                (*g_715) = (void*)0;
            }
            --l_3791[2][1][3];
            l_3794 = &l_3321;
        }
    }
    else
    { /* block id: 1581 */
        union U1 *****l_3796 = &g_1222;
        int32_t l_3834[6];
        uint64_t **l_3843 = &g_1868;
        union U1 l_3870 = {0x159EF5E3L};
        uint16_t ****l_3907 = (void*)0;
        int32_t l_3924 = 0xA08A4BC6L;
        const uint32_t l_3925 = 0x6DCAD641L;
        int i;
        for (i = 0; i < 6; i++)
            l_3834[i] = 0xBA3CDB6DL;
        for (g_1503 = 0; (g_1503 <= 8); g_1503 += 1)
        { /* block id: 1584 */
            union U1 *****l_3795 = &g_1222;
            int32_t l_3823 = 4L;
            union U1 ****l_3827 = &g_1223;
            uint8_t ****l_3833 = (void*)0;
            int32_t *l_3837 = &g_1732;
            for (g_204 = 5; (g_204 >= 0); g_204 -= 1)
            { /* block id: 1587 */
                uint8_t l_3816 = 0xE3L;
                int32_t * const l_3836 = &l_3834[0];
                l_3796 = l_3795;
                for (g_156 = 1; (g_156 <= 5); g_156 += 1)
                { /* block id: 1591 */
                    if (g_1412)
                        goto lbl_3618;
                }
                for (l_2687 = 0; (l_2687 <= 0); l_2687 += 1)
                { /* block id: 1596 */
                    uint32_t *l_3801 = &g_105;
                    uint32_t l_3825 = 0x6E8DFC83L;
                    int i;
                    if (g_2167[(g_204 + 1)])
                        break;
                    (*l_3794) &= (safe_mod_func_uint8_t_u_u((safe_div_func_int32_t_s_s(((((((*l_3801) = 6UL) , (safe_div_func_uint16_t_u_u(0xB8ECL, (safe_sub_func_int32_t_s_s(g_2167[(l_2687 + 6)], ((~(!(*g_2543))) , (safe_div_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(l_3816, (p_3 != ((safe_sub_func_uint64_t_u_u((safe_mod_func_int8_t_s_s(((((safe_lshift_func_uint8_t_u_u(l_3823, 1)) <= p_3) , (safe_unary_minus_func_int32_t_s(((p_3 , l_3816) ^ 0x82L)))) & (-5L)), 0x4BL)), (*****g_2383))) & 1L)))), l_3825)), p_3)), 6L)))))))) , (**l_3297)) > p_3) ^ p_3), 0x51051741L)), 0x5FL));
                    (*g_3835) &= ((**l_3297) = (l_3816 , ((*****g_2383) || (((((((*l_2671) , ((l_3827 = l_3826) == &g_1223)) == (((p_3 & (((~(safe_div_func_int64_t_s_s((g_413[1] & (safe_sub_func_uint32_t_u_u(0xC9B424D6L, ((void*)0 == l_3833)))), l_3823))) , p_3) <= p_3)) <= (*l_3794)) ^ (****g_1480))) | p_3) , p_3) , l_3834[1]) , p_3))));
                    l_3837 = l_3836;
                }
                if ((**l_3297))
                    break;
            }
        }
        if (((g_2167[3] = p_3) | (**l_3297)))
        { /* block id: 1609 */
            (*l_3794) = 1L;
        }
        else
        { /* block id: 1611 */
            int64_t l_3852 = 9L;
            int32_t l_3858 = 0x6C8D8C06L;
            int32_t l_3859 = (-1L);
            int32_t l_3860[7][8][4] = {{{0x4A09D0C4L,(-1L),(-1L),0x4A09D0C4L},{0x4A09D0C4L,0x3E67FDD4L,0x42768BE2L,0xB49081DDL},{0x2EEC87F0L,0x4A09D0C4L,0x37D6515DL,(-5L)},{0xA21ED456L,3L,0xA21ED456L,(-5L)},{0x37D6515DL,0x4A09D0C4L,0x2EEC87F0L,0xB49081DDL},{0x42768BE2L,0x3E67FDD4L,0x4A09D0C4L,0x4A09D0C4L},{(-1L),(-1L),0x4A09D0C4L,0xA21ED456L},{0x42768BE2L,0x8E73B29FL,0x2EEC87F0L,0x3E67FDD4L}},{{0x37D6515DL,0x2EEC87F0L,0xA21ED456L,0x2EEC87F0L},{0xA21ED456L,0x2EEC87F0L,0x37D6515DL,0x3E67FDD4L},{3L,0x37D6515DL,0xA21ED456L,0x3E67FDD4L},{(-1L),(-5L),(-5L),(-1L)},{(-1L),0x4A09D0C4L,0xA21ED456L,0x8E73B29FL},{3L,(-1L),0x42768BE2L,0x2EEC87F0L},{0x3E67FDD4L,0xB49081DDL,0x3E67FDD4L,0x2EEC87F0L},{0x42768BE2L,(-1L),3L,0x8E73B29FL}},{{0xA21ED456L,0x4A09D0C4L,(-1L),(-1L)},{(-5L),(-5L),(-1L),0x3E67FDD4L},{0xA21ED456L,0x37D6515DL,3L,0x4A09D0C4L},{0x42768BE2L,3L,0x3E67FDD4L,3L},{0x3E67FDD4L,3L,0x42768BE2L,0x4A09D0C4L},{3L,0x37D6515DL,0xA21ED456L,0x3E67FDD4L},{(-1L),(-5L),(-5L),(-1L)},{(-1L),0x4A09D0C4L,0xA21ED456L,0x8E73B29FL}},{{3L,(-1L),0x42768BE2L,0x2EEC87F0L},{0x3E67FDD4L,0xB49081DDL,0x3E67FDD4L,0x2EEC87F0L},{0x42768BE2L,(-1L),3L,0x8E73B29FL},{0xA21ED456L,0x4A09D0C4L,(-1L),(-1L)},{(-5L),(-5L),(-1L),0x3E67FDD4L},{0xA21ED456L,0x37D6515DL,3L,0x4A09D0C4L},{0x42768BE2L,3L,0x3E67FDD4L,3L},{0x3E67FDD4L,3L,0x42768BE2L,0x4A09D0C4L}},{{3L,0x37D6515DL,0xA21ED456L,0x3E67FDD4L},{(-1L),(-5L),(-5L),(-1L)},{(-1L),0x4A09D0C4L,0xA21ED456L,0x8E73B29FL},{3L,(-1L),0x42768BE2L,0x2EEC87F0L},{0x3E67FDD4L,0xB49081DDL,0x3E67FDD4L,0x2EEC87F0L},{0x42768BE2L,(-1L),3L,0x8E73B29FL},{0xA21ED456L,0x4A09D0C4L,(-1L),(-1L)},{(-5L),(-5L),(-1L),0x3E67FDD4L}},{{0xA21ED456L,0x37D6515DL,3L,0x4A09D0C4L},{0x42768BE2L,3L,0x3E67FDD4L,3L},{0x3E67FDD4L,3L,0x42768BE2L,0x4A09D0C4L},{3L,0x37D6515DL,0xA21ED456L,0x3E67FDD4L},{(-1L),(-5L),(-5L),(-1L)},{(-1L),0x4A09D0C4L,0xA21ED456L,0x8E73B29FL},{3L,(-1L),0x42768BE2L,0x2EEC87F0L},{0x3E67FDD4L,0xB49081DDL,0x3E67FDD4L,0x2EEC87F0L}},{{0x42768BE2L,(-1L),3L,0x8E73B29FL},{0xA21ED456L,0x4A09D0C4L,(-1L),(-1L)},{(-5L),(-5L),(-1L),0x3E67FDD4L},{0xA21ED456L,0x37D6515DL,3L,0x4A09D0C4L},{0x42768BE2L,3L,0x3E67FDD4L,3L},{0x3E67FDD4L,3L,0x42768BE2L,0x4A09D0C4L},{3L,0x37D6515DL,0xA21ED456L,0x3E67FDD4L},{(-1L),(-5L),(-5L),(-1L)}}};
            int64_t l_3872 = 0L;
            uint32_t l_3946 = 0UL;
            uint16_t *****l_3952 = &l_3907;
            uint8_t l_3964 = 0UL;
            int i, j, k;
            for (g_1406 = (-6); (g_1406 < 20); g_1406++)
            { /* block id: 1614 */
                uint64_t ***l_3840[10] = {&g_1867,&g_1867,&g_1867,&g_1867,&g_1867,&g_1867,&g_1867,&g_1867,&g_1867,&g_1867};
                int32_t l_3856 = 9L;
                int32_t l_3857[2];
                uint8_t *****l_3880 = &g_2449[0];
                int32_t *** const l_3934 = &g_641;
                int32_t *l_3943 = &g_16;
                int32_t *l_3944 = &l_3790;
                int32_t *l_3945[3];
                int i;
                for (i = 0; i < 2; i++)
                    l_3857[i] = (-1L);
                for (i = 0; i < 3; i++)
                    l_3945[i] = &l_3860[6][0][1];
                (*l_2934) = l_3840[4];
                for (g_3432 = 0; (g_3432 != 60); g_3432++)
                { /* block id: 1618 */
                    if (p_3)
                        break;
                }
                if (((***l_2821) == l_3843))
                { /* block id: 1621 */
                    if ((**g_231))
                        break;
                }
                else
                { /* block id: 1623 */
                    int32_t l_3844 = 0L;
                    int32_t *l_3845 = &l_2373;
                    int32_t *l_3846 = &g_16;
                    int32_t *l_3847 = &l_2499[4][3];
                    int32_t *l_3848 = (void*)0;
                    int32_t *l_3849 = &l_2962;
                    int32_t *l_3850 = (void*)0;
                    int32_t *l_3851 = &l_3834[1];
                    int32_t *l_3853 = &l_3790;
                    int32_t *l_3854 = &g_2311[2][3][0];
                    int32_t *l_3855[10] = {&l_3321,&g_2311[1][4][0],&l_3321,&g_2311[1][4][0],&g_2311[1][4][0],&l_3321,&g_2311[1][4][0],&l_3321,&g_2311[1][4][0],&g_2311[1][4][0]};
                    uint32_t l_3861 = 1UL;
                    int64_t *l_3869 = &l_3599[6];
                    uint32_t *l_3871[3];
                    uint64_t l_3873 = 0UL;
                    int16_t * const *l_3897 = &g_501;
                    uint16_t *****l_3908 = &l_3907;
                    uint8_t l_3911 = 251UL;
                    int32_t ** const *l_3932 = &g_641;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_3871[i] = &g_2167[0];
                    l_3861++;
                    l_3873 = ((safe_sub_func_float_f_f(((+(((p_3 = (safe_div_func_int8_t_s_s(((void*)0 != l_3869), (*g_719)))) < ((l_3870 , (*g_1866)) == (l_3834[2] , (*g_1866)))) , (0x0.8p-1 == (((*****g_2383) |= (((l_3857[1] = ((void*)0 != (**l_2822))) == 0x69B05C39L) | (****g_3154))) , (**g_967))))) > (-0x1.Ep+1)), 0xE.584D4Fp-28)) >= l_3872);
                    for (l_3790 = 0; (l_3790 != (-30)); l_3790 = safe_sub_func_uint64_t_u_u(l_3790, 1))
                    { /* block id: 1631 */
                        int16_t ***l_3878[7][4] = {{&g_2493,&g_2493,&g_2493,&g_2493},{&g_2493,&g_2493,&g_2493,&g_2493},{&g_2493,&g_2493,&g_2493,&g_2493},{&g_2493,&g_2493,&g_2493,&g_2493},{&g_2493,&g_2493,&g_2493,&g_2493},{&g_2493,&g_2493,&g_2493,&g_2493},{&g_2493,&g_2493,&g_2493,&g_2493}};
                        union U1 l_3884 = {0x1A8353A1L};
                        uint32_t l_3887 = 4294967295UL;
                        int32_t l_3898 = 0xAEE3C8B8L;
                        int32_t l_3899 = 0L;
                        int32_t l_3900 = 0xA2A7A762L;
                        int32_t l_3901[3];
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_3901[i] = 0x18CC4B6FL;
                    }
                    if ((l_3870 , (0L <= (safe_rshift_func_int16_t_s_u(((l_3834[1] &= (p_3 > (((((g_308[1][4] = (((*l_3908) = l_3907) == &g_650)) && (((*g_350) &= (*l_3794)) >= (l_3924 = (l_3911 | ((safe_sub_func_uint16_t_u_u((safe_unary_minus_func_int32_t_s((l_3870.f0 ^ (~(safe_sub_func_int64_t_s_s((safe_mod_func_int8_t_s_s((((safe_mul_func_int8_t_s_s(((safe_div_func_int8_t_s_s(1L, l_3870.f1)) >= 0xB3A595C1L), 0L)) | 4294967289UL) ^ p_3), (*l_3846))), 0xED040F92C9776C56LL)))))), l_3924)) < 4294967290UL))))) != 0x65E6L) | p_3) || p_3))) , l_3925), l_3857[1])))))
                    { /* block id: 1647 */
                        int32_t ** const **l_3933 = &l_3932;
                        int8_t l_3942 = 0x8AL;
                        (*g_165) |= (safe_mod_func_int64_t_s_s(((***g_1481) = (l_3942 |= (safe_mod_func_int32_t_s_s((safe_add_func_int64_t_s_s((((*l_3933) = l_3932) == ((*g_1868) , l_3934)), (p_3 , (p_3 && (0x11L == (safe_lshift_func_uint8_t_u_s(((g_3941 &= (safe_rshift_func_uint8_t_u_s((&g_2343 == (p_3 , ((safe_sub_func_uint64_t_u_u(p_3, 0x0D797DCF4B9116D1LL)) , &l_2822))), 5))) < (*l_3847)), 1))))))), (-1L))))), p_3));
                        return (***l_3934);
                    }
                    else
                    { /* block id: 1654 */
                        (**g_715) = ((**l_3932) = (**l_3932));
                        if ((*l_3794))
                            break;
                    }
                }
                l_3946++;
            }
            l_3834[1] = (safe_div_func_float_f_f((!(l_3952 != (g_3953 = l_3952))), (safe_sub_func_float_f_f(0xF.DA168Dp-36, ((**g_967) = (safe_add_func_float_f_f((0x2.2p+1 != (p_3 <= (safe_div_func_float_f_f((0x6.4p-1 < l_3834[1]), (-((((l_3858 == (((((void*)0 != (*g_2450)) | l_3964) , l_3925) && 1L)) < (**g_164)) , l_3925) >= (**l_3297))))))), (-0x1.Bp+1))))))));
            return p_3;
        }
    }
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_16
 * writes:
 */
static int16_t  func_7(uint16_t  p_8, int8_t  p_9)
{ /* block id: 2 */
    uint16_t l_20 = 65531UL;
    int32_t * const l_59 = &g_16;
    uint8_t *l_1851 = &g_413[1];
    int32_t l_1877 = 0x47EC58DDL;
    int32_t l_1880 = 0xB4A9F909L;
    int32_t l_1884 = (-1L);
    int32_t l_1885 = 0x262EC007L;
    int32_t l_1891 = 6L;
    int32_t l_1893[3][5] = {{1L,0xEDDE2415L,0x9EE2D797L,0xEDDE2415L,1L},{1L,0xEDDE2415L,0x9EE2D797L,0xEDDE2415L,1L},{1L,0xEDDE2415L,0x9EE2D797L,0xEDDE2415L,1L}};
    int32_t l_1899[1];
    uint16_t l_1902 = 0x7257L;
    const int64_t *l_1930 = &g_155;
    const int64_t **l_1929 = &l_1930;
    const int64_t ***l_1928 = &l_1929;
    const int64_t ****l_1927[5][1];
    int8_t **l_1957 = (void*)0;
    uint32_t l_1977 = 0x1C645DC0L;
    float l_1981 = 0x0.2p+1;
    uint64_t l_2128 = 0xF7F2153E964875B2LL;
    uint32_t l_2140 = 0x4F5C7C36L;
    uint32_t l_2147[7][2] = {{9UL,9UL},{9UL,9UL},{9UL,9UL},{9UL,9UL},{9UL,9UL},{9UL,9UL},{9UL,9UL}};
    int16_t l_2162 = 0xA6CEL;
    int32_t l_2171 = 0L;
    uint64_t *l_2181 = (void*)0;
    union U1 l_2254[7][7][5] = {{{{0x4FF4E89BL},{0xE6679793L},{5UL},{0x304E453BL},{0x77AD569FL}},{{0UL},{18446744073709551615UL},{5UL},{0xC0C9B50FL},{1UL}},{{18446744073709551615UL},{18446744073709551615UL},{18446744073709551615UL},{0x6EE2F59DL},{18446744073709551615UL}},{{0x731A195CL},{0xE6679793L},{0x77AD569FL},{18446744073709551612UL},{9UL}},{{0UL},{0x9083D744L},{18446744073709551615UL},{0xC31D80FBL},{0x35626E04L}},{{5UL},{0x88AEFED6L},{0x7002C88DL},{0xE6679793L},{1UL}},{{0UL},{0x2CED73F8L},{3UL},{0xC8EFE588L},{18446744073709551615UL}}},{{{1UL},{18446744073709551615UL},{0xECD7248BL},{0x9716219EL},{1UL}},{{0x2CED73F8L},{1UL},{0x2BFA53D6L},{0x2BFA53D6L},{1UL}},{{0xEC86019BL},{2UL},{3UL},{0x88AEFED6L},{0x2CED73F8L}},{{0x88AEFED6L},{0xA163FC35L},{0xA002D0DCL},{1UL},{18446744073709551615UL}},{{0x77AD569FL},{18446744073709551613UL},{0xA227232EL},{0x223D29E8L},{0x731A195CL}},{{0x88AEFED6L},{0xC063642FL},{0x77AD569FL},{18446744073709551615UL},{0x7002C88DL}},{{0xEC86019BL},{0x4FF4E89BL},{0xBFAB5130L},{18446744073709551611UL},{0xB1A0A339L}}},{{{0x2CED73F8L},{0x7EDF3EA4L},{0xE6679793L},{0x5D753F2CL},{1UL}},{{1UL},{9UL},{5UL},{18446744073709551615UL},{0x5D753F2CL}},{{0UL},{0xC0C9B50FL},{0x1D98B8D8L},{0UL},{0xEC86019BL}},{{5UL},{18446744073709551611UL},{1UL},{1UL},{18446744073709551612UL}},{{0UL},{3UL},{1UL},{0x88AEFED6L},{18446744073709551615UL}},{{0x731A195CL},{1UL},{18446744073709551611UL},{1UL},{0x9716219EL}},{{18446744073709551615UL},{0x7EDF3EA4L},{0x5D753F2CL},{0xA227232EL},{0x9716219EL}}},{{{0UL},{1UL},{18446744073709551613UL},{0xC8EFE588L},{18446744073709551615UL}},{{0x4FF4E89BL},{0xC8EFE588L},{0x56986622L},{1UL},{18446744073709551612UL}},{{0x6EE2F59DL},{18446744073709551613UL},{0x4FB96958L},{0x2CED73F8L},{0xEC86019BL}},{{18446744073709551615UL},{0xBFAB5130L},{0xCD018A32L},{0x2CED73F8L},{18446744073709551615UL}},{{18446744073709551615UL},{18446744073709551615UL},{0UL},{0x77AD569FL},{0x8C43C4F8L}},{{18446744073709551611UL},{0x2BFA53D6L},{18446744073709551615UL},{0xA3DE474EL},{2UL}},{{18446744073709551615UL},{0xBCEDCE79L},{0xBCEDCE79L},{18446744073709551615UL},{0x93C4A758L}}},{{{18446744073709551615UL},{0x6EE2F59DL},{0x304E453BL},{9UL},{3UL}},{{0x3A42D2E8L},{0x5D753F2CL},{18446744073709551611UL},{0xCD018A32L},{0x77AD569FL}},{{0x77AD569FL},{18446744073709551615UL},{7UL},{9UL},{0x11FF2986L}},{{1UL},{0x8C43C4F8L},{1UL},{18446744073709551615UL},{0x2BFA53D6L}},{{0xDA921340L},{0x77AD569FL},{0x1361B618L},{0xA3DE474EL},{0x4E34E150L}},{{0x50922AB2L},{8UL},{0x1D98B8D8L},{0x77AD569FL},{0xBCEDCE79L}},{{3UL},{0x8C43C4F8L},{1UL},{0x2CED73F8L},{0x90ED7FEFL}}},{{{0xF8B3A6FBL},{0xC063642FL},{5UL},{0x11FF2986L},{5UL}},{{9UL},{0x6843B818L},{0x93C4A758L},{0x8C43C4F8L},{0x0A09C40EL}},{{5UL},{0x6EE2F59DL},{1UL},{0x7002C88DL},{0x50922AB2L}},{{0x0A09C40EL},{0x50922AB2L},{5UL},{5UL},{0x4E34E150L}},{{0x6EE2F59DL},{0xECD7248BL},{5UL},{0xE6679793L},{0xECD7248BL}},{{0UL},{18446744073709551615UL},{1UL},{0x5D753F2CL},{0x6EE2F59DL}},{{0x6843B818L},{18446744073709551615UL},{0x93C4A758L},{0x56986622L},{0x77AD569FL}}},{{{0x2BFA53D6L},{0x93C4A758L},{5UL},{0x52B28D6CL},{8UL}},{{0x5D753F2CL},{0x7002C88DL},{1UL},{18446744073709551615UL},{1UL}},{{0xCD018A32L},{1UL},{0x1D98B8D8L},{18446744073709551615UL},{2UL}},{{0x6EE2F59DL},{7UL},{0x1361B618L},{0xA227232EL},{0x1361B618L}},{{0x90ED7FEFL},{0x90ED7FEFL},{1UL},{18446744073709551615UL},{0xA227232EL}},{{0xBA429767L},{1UL},{7UL},{0xF8B3A6FBL},{0UL}},{{9UL},{18446744073709551615UL},{18446744073709551611UL},{0x4E34E150L},{0UL}}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1899[i] = 0xD9ECF9A1L;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
            l_1927[i][j] = &l_1928;
    }
    for (p_8 = 0; (p_8 < 51); ++p_8)
    { /* block id: 5 */
        uint8_t l_13[1][2];
        int32_t l_17 = (-2L);
        int32_t l_18 = 1L;
        union U0 l_1506 = {2UL};
        uint16_t l_1513[6][3] = {{0xB480L,0xB480L,0xB480L},{65526UL,0xD79EL,65526UL},{0xB480L,0xB480L,0xB480L},{65526UL,0xD79EL,65526UL},{0xB480L,0xB480L,0xB480L},{65526UL,0xD79EL,65526UL}};
        uint64_t *l_1515 = &g_40[0];
        uint64_t **l_1514 = &l_1515;
        int32_t l_1901[1];
        int32_t ****l_1936 = &g_982;
        int16_t l_1953 = 8L;
        union U1 **l_2110 = &g_58;
        int64_t ***l_2168 = &g_1165;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_13[i][j] = 253UL;
        }
        for (i = 0; i < 1; i++)
            l_1901[i] = 0xCE7F2736L;
    }
    return (*l_59);
}


/* ------------------------------------------ */
/* 
 * reads : g_40 g_43
 * writes: g_40
 */
static union U1 * const  func_23(uint32_t  p_24)
{ /* block id: 11 */
    int32_t *l_25 = (void*)0;
    int32_t l_26 = 0x08AF066BL;
    int32_t *l_27 = &g_16;
    int32_t *l_28 = (void*)0;
    int32_t *l_29 = &g_16;
    int32_t *l_30 = &g_16;
    int32_t l_31 = 6L;
    int32_t *l_32 = &g_16;
    int32_t l_33 = 0x06985BA5L;
    int32_t *l_34 = &l_33;
    int32_t *l_35 = &l_33;
    int32_t *l_36 = &l_31;
    int32_t *l_37 = &l_26;
    int32_t *l_38[1];
    int i;
    for (i = 0; i < 1; i++)
        l_38[i] = &l_33;
    g_40[0]++;
    return g_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_645 g_646 g_575 g_165 g_111 g_156 g_16 g_647 g_612 g_40 g_43 g_1562 g_967 g_287 g_350 g_455 g_1121 g_857 g_204 g_413 g_641 g_765 g_1481 g_1165 g_1635 g_39 g_852 g_719 g_716 g_44.f1 g_198 g_44.f0 g_1406 g_1682 g_715 g_182 g_105 g_1495 g_254 g_44 g_372 g_1732
 * writes: g_1521 g_16 g_40 g_58 g_111 g_44.f1 g_103 g_857 g_204 g_108 g_156 g_1495 g_165 g_308 g_1412 g_717 g_44.f0 g_182 g_254 g_765 g_1732
 */
static int32_t * func_48(int32_t  p_49, int32_t  p_50, float  p_51)
{ /* block id: 650 */
    float ****l_1518 = &g_966[1][3];
    float *****l_1519[7][5][7] = {{{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,(void*)0,(void*)0,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,(void*)0,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{(void*)0,&l_1518,&l_1518,(void*)0,&l_1518,(void*)0,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{(void*)0,&l_1518,(void*)0,&l_1518,&l_1518,(void*)0,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{(void*)0,&l_1518,&l_1518,(void*)0,&l_1518,(void*)0,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{(void*)0,&l_1518,(void*)0,&l_1518,&l_1518,(void*)0,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{(void*)0,&l_1518,&l_1518,(void*)0,&l_1518,(void*)0,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518},{&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}}};
    float ****l_1520 = &g_966[1][1];
    int16_t *l_1530 = &g_308[0][1];
    uint8_t ** const l_1531 = &g_372;
    int32_t l_1532[3][7] = {{0xD49D94B9L,1L,0xD49D94B9L,0xF0518DCEL,0xF0518DCEL,0xD49D94B9L,1L},{0xF0518DCEL,1L,0xD3734A01L,0xD3734A01L,1L,0xF0518DCEL,1L},{0xD49D94B9L,0xF0518DCEL,0xF0518DCEL,0xD49D94B9L,1L,0xD49D94B9L,0xF0518DCEL}};
    uint8_t l_1533[10][10] = {{253UL,1UL,3UL,0x03L,1UL,247UL,1UL,254UL,0xBCL,0x1DL},{3UL,255UL,0xFBL,1UL,1UL,2UL,0x93L,2UL,1UL,1UL},{1UL,1UL,1UL,253UL,0UL,0x03L,0x1DL,0xFBL,1UL,0UL},{1UL,0UL,0x93L,3UL,0x1DL,1UL,0UL,0xFBL,0xFBL,0UL},{0xBCL,1UL,1UL,1UL,1UL,0xBCL,0xFBL,2UL,6UL,0x93L},{255UL,0x67L,0xFBL,1UL,1UL,1UL,1UL,254UL,247UL,3UL},{255UL,0UL,3UL,0xBCL,1UL,0xBCL,3UL,0UL,255UL,6UL},{0xBCL,3UL,0UL,255UL,6UL,1UL,0UL,1UL,0UL,254UL},{1UL,0xFBL,0x67L,255UL,0UL,0x03L,0x03L,0UL,255UL,0x67L},{1UL,1UL,1UL,0xBCL,0xFBL,2UL,6UL,0x93L,247UL,253UL}};
    uint64_t l_1534 = 18446744073709551615UL;
    uint64_t l_1566 = 2UL;
    int8_t *l_1577 = &g_204;
    int32_t l_1585 = 0xFFAD7109L;
    int32_t l_1587 = 4L;
    uint32_t l_1588 = 0x7E5AE18AL;
    const union U1 *l_1615 = &g_44;
    uint32_t l_1619 = 0UL;
    int64_t l_1731 = (-2L);
    int32_t *l_1736 = &l_1532[2][1];
    int64_t l_1755 = 8L;
    int8_t l_1763[4] = {0x23L,0x23L,0x23L,0x23L};
    int16_t l_1765[9][2][5] = {{{0x60DBL,(-9L),0x3BB1L,0x5BCBL,0x2307L},{6L,8L,(-1L),0x983DL,0x07F3L}},{{(-1L),8L,0x71E3L,0x71E3L,8L},{0xD71CL,(-9L),0x2307L,8L,0x3B0EL}},{{0x5BCBL,6L,0x5FDDL,0L,0x60DBL},{0x3B0EL,0x07F3L,0x71E3L,0x3BB1L,0x336FL}},{{0x5BCBL,1L,(-1L),0xD71CL,0x3BB1L},{0xD71CL,0x6470L,0x7824L,0L,0x983DL}},{{(-1L),0x336FL,(-9L),0xE102L,0x983DL},{6L,0x3B0EL,0x3B0EL,6L,0x3BB1L}},{{0x60DBL,6L,0x3E46L,0x983DL,0x336FL},{0x2307L,0x5BCBL,0x3BB1L,(-9L),(-1L)}},{{0x3B0EL,0L,0x7824L,1L,0xF9C0L},{0x3BB1L,0x71E3L,0x07F3L,0x3B0EL,0x71E3L}},{{0x25B1L,2L,(-1L),0L,(-6L)},{2L,0xEB00L,(-1L),(-9L),(-1L)}},{{(-6L),(-6L),0x07F3L,0xE102L,0x2307L},{(-1L),0x2307L,0x7824L,0x25B1L,0xE102L}}};
    uint16_t l_1785 = 5UL;
    uint32_t *l_1786 = &l_1588;
    uint16_t **l_1787 = &g_350;
    int32_t *l_1788 = &g_765;
    int32_t *l_1822 = &l_1532[0][2];
    int32_t *l_1823 = &l_1532[0][2];
    int32_t *l_1824 = &g_1732;
    int i, j, k;
    if (((p_50 <= ((l_1520 = l_1518) != (g_1521 = &g_966[5][0]))) && ((((safe_mul_func_int16_t_s_s(((((safe_add_func_uint8_t_u_u(((safe_div_func_float_f_f((safe_add_func_float_f_f(((l_1532[0][2] = ((l_1530 == l_1530) != ((p_50 , l_1531) != (*g_645)))) < p_49), l_1533[2][4])), 0x9.D83332p-20)) , 0x2DL), p_49)) > 4L) <= 0x38E428B4C0328B79LL) ^ p_50), l_1534)) , 0x2F127E0E133D1689LL) <= 1UL) || p_49)))
    { /* block id: 654 */
        int32_t *l_1535 = &g_16;
        uint8_t *l_1542 = &l_1533[2][4];
        uint8_t *** const l_1560 = &g_371[6];
        int32_t l_1565 = (-1L);
        uint32_t l_1586 = 4294967288UL;
        int32_t l_1592 = 8L;
        int32_t l_1594 = 1L;
        int32_t l_1596 = 0x06C9FABDL;
        int32_t l_1597 = 9L;
        int32_t l_1598 = 0x236973A5L;
        int32_t l_1600 = 1L;
        int32_t l_1601[7] = {0xD1E64235L,0xCFA5C305L,0xCFA5C305L,0xD1E64235L,0xCFA5C305L,0xCFA5C305L,0xD1E64235L};
        union U0 l_1628 = {0x6CA862BAL};
        int32_t l_1663[3][9] = {{(-9L),(-1L),(-9L),(-9L),(-1L),(-9L),(-9L),(-1L),(-9L)},{(-9L),(-1L),(-9L),(-9L),(-1L),(-9L),(-9L),(-1L),(-9L)},{(-9L),(-1L),(-9L),(-9L),(-1L),(-9L),(-9L),(-1L),(-9L)}};
        int32_t *l_1726 = (void*)0;
        int32_t *l_1727 = &l_1587;
        int32_t *l_1728 = &l_1587;
        int32_t *l_1729[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_1730 = 0L;
        uint32_t l_1733 = 0x77DF7BC5L;
        int i, j;
        (*l_1535) &= (**g_575);
        if (p_49)
        { /* block id: 656 */
            uint8_t *l_1541 = (void*)0;
            int32_t l_1552 = 0x03CC64C3L;
            int32_t l_1559[2][6] = {{0x09D43718L,0xDCC24008L,0xDCC24008L,0x09D43718L,0x7B239A8BL,0x09D43718L},{0x09D43718L,0x7B239A8BL,0x09D43718L,0xDCC24008L,0xDCC24008L,0x09D43718L}};
            int32_t *l_1563 = &l_1559[1][1];
            int32_t *l_1564[1];
            int64_t l_1578[9][1] = {{0L},{(-1L)},{0L},{(-1L)},{0L},{(-1L)},{0L},{(-1L)},{0L}};
            uint32_t *l_1584 = &g_103;
            int i, j;
            for (i = 0; i < 1; i++)
                l_1564[i] = &g_16;
            (*g_1562) = func_23((((safe_lshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((+((l_1541 = l_1541) == l_1542)), (+(((safe_lshift_func_int16_t_s_s((safe_div_func_uint8_t_u_u(0x9BL, (((((safe_add_func_uint8_t_u_u(((*l_1535) ^= p_49), ((safe_div_func_uint8_t_u_u(((l_1552 >= (5UL < ((safe_lshift_func_uint16_t_u_s(l_1533[2][4], 2)) || (l_1559[1][1] = ((safe_rshift_func_int8_t_s_u(l_1534, 7)) && (safe_div_func_int8_t_s_s(((l_1519[6][2][2] = &l_1518) == &g_1521), p_50))))))) >= 0xB8L), (**g_646))) , l_1534))) , l_1560) == &g_314) != l_1552) | p_49))), p_50)) ^ 1L) && 4L)))), 9)) , (void*)0) == (void*)0));
            --l_1566;
            p_51 = (safe_mul_func_float_f_f((safe_sub_func_float_f_f((safe_div_func_float_f_f(((safe_sub_func_float_f_f(p_51, (p_49 , ((*l_1535) = ((**g_967) = (l_1578[1][0] = (&g_204 != l_1577))))))) != l_1534), (safe_sub_func_float_f_f((l_1587 = (((l_1532[1][0] = p_50) == ((!(l_1585 = (l_1565 = ((*l_1563) = ((1UL == ((safe_sub_func_int64_t_s_s(((*g_1121) &= (((*g_350) = 0xF1FBL) == (((*l_1584) = (p_49 >= g_455[2])) || p_50))), (-1L))) || (-7L))) , p_50))))) <= 0x8.7A92A0p+99)) > l_1586)), p_49)))), l_1588)), 0x8.0A7658p+46));
            for (g_204 = 1; (g_204 <= 7); g_204 += 1)
            { /* block id: 677 */
                uint64_t *l_1589 = &g_108[1][1][1];
                int32_t l_1591[1][5] = {{1L,1L,1L,1L,1L}};
                uint64_t l_1602 = 18446744073709551615UL;
                const union U1 *l_1617 = &g_1618[5];
                int i, j;
                (**g_641) &= (((*l_1589) = 1UL) == (g_413[g_204] > (5L <= l_1534)));
                for (g_1495 = 0; (g_1495 <= 5); g_1495 += 1)
                { /* block id: 682 */
                    int32_t l_1593 = 0x3C0B8510L;
                    int32_t l_1595 = 0xF543B480L;
                    int32_t l_1599[3];
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1599[i] = 0x23BDCFCBL;
                    (*l_1563) ^= (~g_413[g_204]);
                    --l_1602;
                    for (l_1592 = 0; (l_1592 >= 0); l_1592 -= 1)
                    { /* block id: 687 */
                        const union U1 **l_1616[10];
                        int i;
                        for (i = 0; i < 10; i++)
                            l_1616[i] = (void*)0;
                        (*g_641) = &p_49;
                        (**g_641) = (safe_mod_func_uint16_t_u_u((((safe_mul_func_int8_t_s_s((((*l_1535) = (65533UL < (0xEE03L | ((*l_1535) , ((*l_1530) = (safe_lshift_func_uint16_t_u_u((!((((*l_1535) , (0x2C46627AL ^ p_50)) >= p_50) <= (p_49 || ((+(((safe_add_func_float_f_f(((l_1617 = l_1615) != l_1615), 0x7.Ap-1)) < 0x8.949D52p+19) <= 0x4.8B2EABp-47)) , l_1602)))), l_1619))))))) > p_49), 9L)) & g_765) > (*l_1563)), p_49));
                    }
                }
            }
        }
        else
        { /* block id: 696 */
            int32_t **l_1622 = (void*)0;
            int32_t **l_1623 = (void*)0;
            int32_t *l_1636 = &g_1412;
            union U0 l_1645 = {0x2EF6FABEL};
            uint16_t l_1652 = 0x9777L;
            int32_t l_1662[5][9][5] = {{{1L,9L,1L,1L,9L},{0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL},{9L,9L,0xC5943E76L,9L,9L},{0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L},{9L,1L,1L,9L,1L},{0x2398C8B8L,0x2398C8B8L,0x1010B17EL,0x2398C8B8L,0x2398C8B8L},{1L,9L,1L,1L,9L},{0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL},{9L,9L,0xC5943E76L,9L,9L}},{{0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L},{9L,1L,1L,9L,1L},{0x2398C8B8L,0x2398C8B8L,0x1010B17EL,0x2398C8B8L,0x2398C8B8L},{1L,9L,1L,1L,9L},{0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL},{9L,9L,0xC5943E76L,9L,9L},{0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L},{9L,1L,1L,9L,1L},{0x2398C8B8L,0x2398C8B8L,0x1010B17EL,0x2398C8B8L,0x2398C8B8L}},{{0xC5943E76L,1L,0xC5943E76L,0xC5943E76L,1L},{0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL,0x1010B17EL},{1L,1L,9L,1L,1L},{0x1010B17EL,0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL},{1L,0xC5943E76L,0xC5943E76L,1L,0xC5943E76L},{0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL},{0xC5943E76L,1L,0xC5943E76L,0xC5943E76L,1L},{0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL,0x1010B17EL},{1L,1L,9L,1L,1L}},{{0x1010B17EL,0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL},{1L,0xC5943E76L,0xC5943E76L,1L,0xC5943E76L},{0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL},{0xC5943E76L,1L,0xC5943E76L,0xC5943E76L,1L},{0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL,0x1010B17EL},{1L,1L,9L,1L,1L},{0x1010B17EL,0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL},{1L,0xC5943E76L,0xC5943E76L,1L,0xC5943E76L},{0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL}},{{0xC5943E76L,1L,0xC5943E76L,0xC5943E76L,1L},{0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL,0x1010B17EL},{1L,1L,9L,1L,1L},{0x1010B17EL,0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL},{1L,0xC5943E76L,0xC5943E76L,1L,0xC5943E76L},{0xFD4FF5EDL,0xFD4FF5EDL,0x2398C8B8L,0xFD4FF5EDL,0xFD4FF5EDL},{0xC5943E76L,1L,0xC5943E76L,0xC5943E76L,1L},{0xFD4FF5EDL,0x1010B17EL,0x1010B17EL,0xFD4FF5EDL,0x1010B17EL},{1L,1L,9L,1L,1L}}};
            uint8_t l_1664[4][9] = {{0x2DL,6UL,6UL,0x2DL,1UL,0xAAL,1UL,0x2DL,6UL},{254UL,254UL,0UL,0xD8L,255UL,0xD8L,0UL,254UL,254UL},{6UL,0x2DL,1UL,0xAAL,1UL,0x2DL,6UL,6UL,0x2DL},{0xD8L,0UL,0UL,0UL,0xD8L,9UL,9UL,0xD8L,0UL}};
            uint32_t *l_1665 = &l_1586;
            int32_t *l_1689 = &l_1600;
            uint8_t * const *l_1709 = &g_372;
            int i, j, k;
            (*g_287) = (safe_sub_func_float_f_f(p_49, (((*g_641) = &p_49) == (l_1535 = &l_1585))));
            l_1532[0][2] |= (safe_add_func_int8_t_s_s((((safe_mul_func_uint8_t_u_u(((((void*)0 != (*g_1481)) > ((*l_1535) = (l_1628 , ((-1L) == ((*g_1481) == (void*)0))))) , (safe_rshift_func_uint8_t_u_s((safe_div_func_int64_t_s_s(((((safe_div_func_int64_t_s_s((g_1635[1][1] && p_50), ((((*l_1636) = ((*l_1535) = g_39)) , (*g_1121)) || (*l_1535)))) && (*g_852)) & p_49) | (*g_1121)), p_50)), 1))), (*g_719))) & l_1588) , 0L), 4L));
            (*g_716) = &p_49;
            if ((((safe_rshift_func_uint8_t_u_s((((safe_div_func_uint32_t_u_u((safe_div_func_int16_t_s_s(((g_40[0] = (safe_lshift_func_int8_t_s_u(0x5EL, 6))) == (l_1645 , (safe_mul_func_int16_t_s_s(((((*l_1665) = (safe_div_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((4294967286UL != l_1652), (0x228C77A9L == ((safe_sub_func_int8_t_s_s((*g_719), (-1L))) & ((~((p_49 >= (safe_rshift_func_uint8_t_u_u((((safe_mod_func_uint16_t_u_u(((*g_350) = (((safe_lshift_func_uint16_t_u_s((*g_350), 9)) || 6UL) < l_1662[2][0][4])), g_39)) | l_1663[1][5]) < p_50), p_50))) >= l_1664[1][6])) == (*l_1535)))))), p_50))) == 7L) && l_1533[2][4]), p_49)))), p_49)), g_413[0])) || 3L) & 0x71L), (*l_1535))) & g_198) != p_50))
            { /* block id: 708 */
                int8_t **l_1672[5] = {&g_1273,&g_1273,&g_1273,&g_1273,&g_1273};
                int32_t l_1676 = 0x8F7B1826L;
                int i;
                for (g_44.f0 = 16; (g_44.f0 < 55); g_44.f0 = safe_add_func_int16_t_s_s(g_44.f0, 7))
                { /* block id: 711 */
                    uint64_t *l_1685 = (void*)0;
                    int32_t **l_1688 = &l_1535;
                    uint64_t l_1690 = 0x1AF5B43F48A235A9LL;
                    int32_t l_1724 = (-1L);
                    (*g_641) = &p_49;
                    l_1689 = ((*g_641) = ((safe_div_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((*l_1530) = (l_1672[0] == (((*l_1577) = (!(((((safe_add_func_int64_t_s_s(l_1676, (safe_rshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_u((~(g_1406 > 3L)), g_1682[3][0][3])), (p_49 > (l_1587 ^= (safe_lshift_func_int8_t_s_s(1L, l_1676)))))))) ^ (--(*l_1665))) , l_1688) != l_1688) < p_50))) , (void*)0))), (-7L))), 8L)) , &p_49));
                }
                (*l_1535) ^= p_49;
            }
            else
            { /* block id: 732 */
                int32_t *l_1725 = &l_1585;
                (*g_641) = l_1725;
            }
        }
        --l_1733;
        (**g_715) = (l_1736 = ((*g_641) = (*g_575)));
    }
    else
    { /* block id: 740 */
        union U1 *****l_1746 = &g_1222;
        int32_t l_1748[10][10][2] = {{{(-7L),1L},{(-7L),(-7L)},{1L,(-7L)},{(-7L),1L},{(-7L),(-7L)},{1L,(-7L)},{(-7L),1L},{(-7L),1L},{6L,1L},{1L,6L}},{{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L}},{{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L}},{{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L}},{{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L}},{{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L}},{{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L}},{{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L}},{{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L}},{{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L},{1L,1L},{6L,1L},{1L,6L}}};
        int32_t l_1764 = 0xE1EEE3DAL;
        float l_1766 = 0x0.BC613Bp-15;
        float l_1767 = 0x9.7p-1;
        int i, j, k;
        for (g_182 = 27; (g_182 >= 41); ++g_182)
        { /* block id: 743 */
            union U1 *****l_1747 = &g_1222;
            int32_t l_1751 = 0L;
            int64_t *l_1754 = &l_1731;
            (*g_287) = ((safe_sub_func_uint8_t_u_u((*l_1736), (safe_add_func_int8_t_s_s(((*g_719) = (safe_add_func_uint16_t_u_u((((+(g_105 & ((l_1746 != ((*l_1736) , (l_1747 = &g_1222))) == (l_1748[3][2][0] = 0x443B0CA545E3AB18LL)))) != (safe_rshift_func_uint16_t_u_u((l_1751 || 1L), 12))) != (safe_lshift_func_int16_t_s_u(((void*)0 != l_1754), l_1755))), g_1495))), 251UL)))) , (*l_1736));
        }
        for (g_254 = 0; (g_254 <= 38); g_254++)
        { /* block id: 751 */
            int32_t *l_1758 = &g_1732;
            int32_t *l_1759 = &l_1532[2][3];
            int32_t *l_1760 = (void*)0;
            int32_t *l_1761 = (void*)0;
            int32_t *l_1762[9][9][3] = {{{(void*)0,&g_16,&g_156},{(void*)0,&l_1532[0][1],&g_1732},{&g_16,&l_1748[1][8][1],&g_16},{&g_765,&g_156,&l_1532[0][2]},{&g_1732,&l_1585,(void*)0},{(void*)0,&g_156,&l_1532[0][2]},{(void*)0,&g_156,&g_16},{&g_1732,&l_1532[0][2],&g_156},{&g_156,&g_16,(void*)0}},{{&l_1748[6][5][1],&l_1585,&l_1532[0][2]},{&g_16,(void*)0,&g_1732},{&g_16,&g_156,&g_156},{&l_1748[6][5][1],&g_1732,&l_1585},{&g_156,&g_16,(void*)0},{&g_1732,&l_1587,&g_156},{(void*)0,&l_1748[3][8][0],&g_16},{(void*)0,&l_1748[3][2][0],&l_1748[6][5][1]},{&g_1732,&g_1732,&g_765}},{{&g_765,&l_1532[0][2],&g_1732},{&g_16,&g_1732,&l_1585},{&l_1532[2][4],&l_1532[0][2],&l_1748[4][2][1]},{&l_1587,&g_16,&l_1585},{&l_1587,(void*)0,&g_1732},{&l_1748[3][2][0],(void*)0,&g_765},{&g_16,(void*)0,&l_1748[6][5][1]},{&g_156,&g_1732,&g_16},{&g_156,&l_1748[6][5][1],&g_156}},{{&l_1532[0][2],&g_156,(void*)0},{&l_1748[1][8][1],&l_1587,&l_1585},{&l_1585,&l_1532[0][1],&g_156},{(void*)0,&g_16,&g_1732},{(void*)0,&g_16,&l_1532[0][2]},{(void*)0,&l_1532[0][1],(void*)0},{&g_16,&l_1587,&g_156},{&l_1748[1][5][1],&g_156,&g_16},{&g_1732,&l_1748[6][5][1],&l_1532[0][2]}},{{&g_16,&g_1732,(void*)0},{&l_1532[0][2],(void*)0,&l_1532[0][2]},{(void*)0,(void*)0,&g_16},{&g_1732,(void*)0,&g_1732},{&l_1532[0][2],&g_16,(void*)0},{(void*)0,&l_1532[0][2],&g_16},{&l_1532[0][2],&g_1732,&g_16},{&g_1732,&l_1532[0][2],&g_156},{(void*)0,&g_1732,&l_1585}},{{&l_1532[0][2],&l_1748[3][2][0],&l_1748[1][8][1]},{&g_16,&l_1748[3][8][0],&l_1532[0][2]},{&g_1732,&l_1587,&g_16},{&l_1748[1][5][1],&g_16,(void*)0},{&g_16,&g_1732,&l_1748[3][2][0]},{(void*)0,&g_156,&l_1748[3][5][1]},{(void*)0,(void*)0,&l_1748[3][5][1]},{(void*)0,&l_1585,&l_1748[3][2][0]},{&l_1585,&g_16,(void*)0}},{{&l_1748[1][8][1],&l_1532[0][2],&g_16},{&l_1532[0][2],&g_156,&l_1532[0][2]},{&g_156,&g_156,&l_1748[1][8][1]},{&g_156,&l_1585,&l_1585},{&g_16,&g_156,&g_156},{&l_1748[3][2][0],&l_1748[1][8][1],&g_16},{&l_1587,(void*)0,&g_16},{&l_1587,&l_1587,(void*)0},{&l_1532[2][4],(void*)0,&g_1732}},{{&g_16,&l_1748[1][8][1],&g_16},{&g_765,&g_156,&l_1532[0][2]},{&g_1732,&g_156,&g_1732},{&g_16,(void*)0,&g_1732},{&l_1748[3][2][0],&l_1748[4][2][1],&l_1585},{&g_156,&g_16,&l_1748[4][2][1]},{(void*)0,(void*)0,&g_156},{&l_1532[0][2],&l_1532[0][2],&g_16},{(void*)0,&l_1585,&l_1748[3][8][0]}},{{(void*)0,&l_1587,(void*)0},{&l_1532[0][2],&g_156,&l_1748[1][5][1]},{(void*)0,&l_1587,&l_1587},{&g_156,(void*)0,(void*)0},{&l_1748[3][2][0],&g_1732,&g_1732},{&g_16,&g_16,&l_1532[0][2]},{&g_765,&g_765,&l_1748[3][2][0]},{&l_1748[3][2][0],(void*)0,&l_1532[0][2]},{&g_1732,&g_156,&l_1748[1][8][1]}}};
            uint32_t l_1768 = 0x4C43265CL;
            int i, j, k;
            l_1768++;
        }
    }
    (*l_1788) = ((*g_165) = ((safe_mod_func_uint64_t_u_u(p_49, (safe_sub_func_uint16_t_u_u((((((*l_1786) &= ((4294967295UL > ((safe_rshift_func_int8_t_s_u(((*l_1736) || (safe_sub_func_int32_t_s_s(0x51B5E425L, 0x3BC4BB2FL))), ((*l_1736) >= ((*l_1615) , ((safe_mod_func_uint64_t_u_u((((safe_rshift_func_uint8_t_u_s((safe_mod_func_uint8_t_u_u(((void*)0 != (*l_1531)), l_1785)), (*g_719))) , (*l_1736)) ^ (*g_719)), 0xA813F80C2D623474LL)) != (*l_1736)))))) != p_49)) > (*l_1736))) , l_1787) == l_1787) , p_49), p_49)))) | (*l_1736)));
    for (l_1588 = 0; (l_1588 <= 7); l_1588 += 1)
    { /* block id: 760 */
        int32_t l_1818 = 0x9903E75AL;
        uint16_t l_1821 = 0xAF60L;
        int i;
        (*l_1788) = (safe_rshift_func_int16_t_s_u(((~0x3BA75C98L) || ((((((g_413[l_1588] == p_49) < (*l_1736)) < ((*l_1736) <= ((((safe_add_func_float_f_f((p_51 = ((((safe_rshift_func_int16_t_s_u(0x612EL, (safe_lshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u((safe_mod_func_uint32_t_u_u((((((safe_mul_func_float_f_f(((safe_mod_func_uint16_t_u_u(((*g_350) = ((*g_967) == (((((((safe_rshift_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((((safe_sub_func_float_f_f(g_413[l_1588], (safe_sub_func_float_f_f((safe_mul_func_float_f_f((l_1818 = ((**g_967) <= g_413[l_1588])), 0xC.A47FA6p+54)), (*l_1736))))) <= 0x0.Ap+1) , 18446744073709551612UL), (*l_1788))), p_50)) ^ 0UL), (**g_646))) >= 0L) , 0x092CL) >= g_413[l_1588]) == 0UL) , 65535UL) , (*g_967)))), 1UL)) , (*g_287)), p_50)) == (-0x1.Bp-1)) > (*l_1788)) , (void*)0) == &g_982), (*l_1736))), g_455[6])), (*l_1788))))) != (*l_1736)) , 0xB.CEDFC8p-74) != p_51)), (*l_1788))) <= (*g_287)) <= (*l_1788)) != 0x8.6C5531p-71))) != (**g_967)) <= g_413[l_1588]) , p_49)), p_49));
        (**g_641) &= (safe_sub_func_uint32_t_u_u((g_103 = p_50), l_1821));
    }
    return l_1824;
}


/* ------------------------------------------ */
/* 
 * reads : g_16 g_40 g_103 g_39 g_314 g_165 g_111 g_156 g_182 g_108 g_44.f1 g_967 g_287 g_1412 g_231 g_1461 g_611 g_455 g_105 g_308 g_575 g_641 g_765 g_1487
 * writes: g_16 g_39 g_94 g_103 g_108 g_111 g_182 g_198 g_156 g_308 g_340 g_44.f1 g_204 g_19 g_44.f0 g_57 g_165 g_611 g_105 g_1480 g_765
 */
static uint32_t  func_54(union U1 ** p_55, int32_t * const  p_56)
{ /* block id: 15 */
    int16_t l_69[3];
    int32_t *l_1472[9][6] = {{&g_16,&g_16,&g_16,&g_16,&g_16,&g_16},{(void*)0,&g_16,(void*)0,&g_16,(void*)0,&g_16},{&g_16,&g_16,&g_16,&g_16,&g_16,&g_16},{(void*)0,&g_16,(void*)0,&g_16,(void*)0,&g_16},{&g_16,&g_16,&g_16,&g_16,&g_16,&g_16},{(void*)0,&g_16,(void*)0,&g_16,(void*)0,&g_16},{&g_16,&g_16,&g_16,&g_16,&g_16,&g_16},{(void*)0,&g_16,(void*)0,&g_16,(void*)0,&g_16},{&g_16,&g_16,&g_16,&g_16,&g_16,&g_16}};
    uint64_t l_1474 = 18446744073709551615UL;
    const uint8_t ***l_1475 = (void*)0;
    int64_t ***l_1477 = &g_1165;
    int64_t ****l_1476 = &l_1477;
    int64_t *****l_1478 = (void*)0;
    int64_t *****l_1479[5];
    int i, j;
    for (i = 0; i < 3; i++)
        l_69[i] = 0x5D33L;
    for (i = 0; i < 5; i++)
        l_1479[i] = (void*)0;
    for (g_16 = (-13); (g_16 <= (-1)); ++g_16)
    { /* block id: 18 */
        int32_t * const l_1464[3] = {&g_156,&g_156,&g_156};
        uint32_t *l_1465 = &g_105;
        int16_t *l_1470 = &g_308[1][4];
        const union U1 l_1471 = {0xB89D98F5L};
        int i;
        (*g_287) = (((safe_add_func_int32_t_s_s(func_64((l_69[0] , (+((*l_1470) = ((safe_div_func_uint32_t_u_u((((func_75(&g_16) , l_1464[1]) == l_1464[1]) < ((void*)0 != p_56)), ((*l_1465) &= g_455[4]))) || (safe_lshift_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u(l_69[0], 1UL)) & g_308[1][1]), l_69[0])))))), l_1471, l_1472[4][4], l_1465), (*p_56))) , (**g_967)) > l_1474);
    }
    (**g_641) ^= (l_1475 == l_1475);
    g_1480 = l_1476;
    for (g_765 = 2; (g_765 <= (-3)); g_765--)
    { /* block id: 642 */
        uint16_t l_1484 = 2UL;
        --l_1484;
    }
    return g_1487;
}


/* ------------------------------------------ */
/* 
 * reads : g_575 g_165 g_111 g_156 g_105
 * writes: g_105
 */
static int32_t  func_64(int64_t  p_65, const union U1  p_66, int32_t * p_67, int32_t * p_68)
{ /* block id: 633 */
    int32_t l_1473 = 0x88ECDA3CL;
    (*p_68) |= (l_1473 == ((**g_575) > p_65));
    return (*p_68);
}


/* ------------------------------------------ */
/* 
 * reads : g_39 g_40 g_103 g_16 g_314 g_165 g_156 g_182 g_108 g_44.f1 g_967 g_287 g_1412 g_44.f0 g_231 g_111 g_1461 g_611
 * writes: g_39 g_94 g_103 g_108 g_111 g_182 g_198 g_156 g_308 g_340 g_44.f1 g_204 g_19 g_44.f0 g_57 g_165 g_611
 */
static int32_t  func_75(int32_t * p_76)
{ /* block id: 19 */
    uint8_t l_77[9] = {9UL,9UL,9UL,9UL,9UL,9UL,9UL,9UL,9UL};
    int32_t l_106 = 0x4DC86549L;
    float l_758 = 0xA.FBB0F7p-78;
    union U1 ****l_1411 = &g_1223;
    int32_t l_1420 = 1L;
    int16_t l_1421 = 0xDEB1L;
    int32_t l_1422 = (-1L);
    int32_t l_1423 = 0x223EAE8DL;
    int32_t l_1424 = 0xF6745B0DL;
    int32_t l_1425 = (-1L);
    int32_t l_1426 = (-8L);
    int32_t l_1427 = 0x009BB7F9L;
    const union U0 l_1445 = {0x26A5D275L};
    union U1 **l_1451 = &g_58;
    int i;
    for (g_39 = 2; (g_39 <= 8); g_39 += 1)
    { /* block id: 22 */
        uint16_t l_78 = 1UL;
        uint64_t *l_93[7][9][3] = {{{&g_40[0],(void*)0,(void*)0},{(void*)0,&g_40[0],(void*)0},{&g_40[0],&g_40[0],(void*)0},{&g_94,&g_40[0],&g_40[0]},{&g_94,&g_40[0],&g_94},{(void*)0,&g_94,&g_40[0]},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_94,(void*)0},{(void*)0,&g_40[0],&g_40[0]}},{{&g_94,&g_40[0],&g_40[0]},{&g_94,&g_94,(void*)0},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_94,&g_40[0]},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],(void*)0},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],(void*)0,&g_40[0]}},{{&g_94,&g_40[0],&g_40[0]},{(void*)0,(void*)0,&g_40[0]},{(void*)0,&g_94,(void*)0},{(void*)0,&g_40[0],&g_40[0]},{&g_40[0],(void*)0,&g_40[0]},{&g_40[0],(void*)0,&g_40[0]},{(void*)0,&g_40[0],(void*)0},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],(void*)0,&g_94}},{{&g_40[0],&g_40[0],&g_94},{(void*)0,(void*)0,&g_40[0]},{&g_94,&g_94,(void*)0},{(void*)0,(void*)0,&g_40[0]},{(void*)0,&g_40[0],&g_40[0]},{(void*)0,&g_40[0],(void*)0},{&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],(void*)0,&g_40[0]},{&g_94,&g_94,(void*)0}},{{&g_40[0],(void*)0,(void*)0},{&g_40[0],&g_40[0],&g_94},{(void*)0,&g_40[0],(void*)0},{(void*)0,&g_40[0],&g_94},{(void*)0,(void*)0,(void*)0},{&g_40[0],&g_94,(void*)0},{&g_94,(void*)0,(void*)0},{&g_40[0],&g_40[0],&g_40[0]},{&g_94,(void*)0,&g_40[0]}},{{&g_40[0],&g_40[0],(void*)0},{&g_94,(void*)0,(void*)0},{&g_40[0],(void*)0,(void*)0},{&g_40[0],(void*)0,&g_94},{(void*)0,(void*)0,(void*)0},{&g_40[0],(void*)0,&g_94},{(void*)0,&g_40[0],(void*)0},{&g_40[0],&g_40[0],(void*)0},{&g_40[0],&g_40[0],&g_40[0]}},{{&g_40[0],(void*)0,&g_40[0]},{(void*)0,&g_94,(void*)0},{&g_40[0],(void*)0,&g_40[0]},{(void*)0,&g_94,&g_40[0]},{&g_40[0],&g_40[0],(void*)0},{&g_40[0],(void*)0,&g_40[0]},{&g_94,&g_40[0],&g_94},{&g_40[0],(void*)0,&g_94},{&g_94,(void*)0,(void*)0}}};
        uint32_t *l_102 = &g_103;
        uint32_t *l_104[6][7][2] = {{{&g_105,(void*)0},{&g_105,&g_105},{&g_105,(void*)0},{&g_105,&g_105},{(void*)0,&g_105},{&g_105,&g_105},{&g_105,&g_105}},{{&g_105,&g_105},{&g_105,&g_105},{(void*)0,&g_105},{&g_105,(void*)0},{&g_105,&g_105},{&g_105,(void*)0},{&g_105,&g_105}},{{(void*)0,&g_105},{&g_105,&g_105},{&g_105,&g_105},{&g_105,&g_105},{&g_105,&g_105},{(void*)0,&g_105},{&g_105,(void*)0}},{{&g_105,&g_105},{&g_105,(void*)0},{&g_105,&g_105},{(void*)0,&g_105},{&g_105,&g_105},{&g_105,&g_105},{&g_105,&g_105}},{{&g_105,&g_105},{(void*)0,&g_105},{&g_105,(void*)0},{&g_105,&g_105},{&g_105,(void*)0},{&g_105,&g_105},{(void*)0,&g_105}},{{&g_105,&g_105},{&g_105,&g_105},{&g_105,&g_105},{&g_105,&g_105},{(void*)0,&g_105},{&g_105,&g_105},{(void*)0,&g_105}}};
        int64_t *l_107[6][6][3] = {{{&g_39,&g_39,(void*)0},{&g_39,(void*)0,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39}},{{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,(void*)0},{&g_39,&g_39,&g_39},{&g_39,&g_39,(void*)0},{&g_39,(void*)0,&g_39}},{{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39}},{{&g_39,&g_39,(void*)0},{&g_39,&g_39,&g_39},{&g_39,&g_39,(void*)0},{&g_39,(void*)0,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39}},{{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,(void*)0},{&g_39,&g_39,&g_39}},{{&g_39,&g_39,(void*)0},{&g_39,(void*)0,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39},{&g_39,&g_39,&g_39}}};
        union U0 l_759 = {0UL};
        int32_t l_1419 = 0xC5876146L;
        int32_t l_1428[3][8] = {{0xD0B4E039L,0x03734AE0L,0x03734AE0L,0xD0B4E039L,0x2326E852L,1L,0x2326E852L,0xD0B4E039L},{0x03734AE0L,0x2326E852L,0x03734AE0L,(-10L),(-1L),(-1L),(-10L),0x03734AE0L},{0x2326E852L,0x2326E852L,(-1L),1L,0xC695F41AL,1L,(-1L),0x2326E852L}};
        uint32_t l_1429 = 0UL;
        uint8_t l_1432 = 1UL;
        int i, j, k;
        if ((((l_78 >= (l_77[1] , func_79(func_83((safe_mod_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_s(l_78, 2)), ((g_94 = (safe_mul_func_int16_t_s_s((+0xEC61L), g_40[0]))) | (safe_div_func_uint16_t_u_u(((safe_div_func_int64_t_s_s(l_77[0], (safe_mul_func_int16_t_s_s(((g_108[0][5][1] = (g_40[0] < (~((l_106 &= ((1UL <= ((*l_102) &= l_77[1])) | l_78)) == 0x9A5DE3B0L)))) != 0x3F29A3864406D402LL), l_77[3])))) != l_78), g_39))))), g_16), l_77[2], l_759))) , 0x7E19ABAEB9065179LL) ^ l_78))
        { /* block id: 605 */
            (**g_967) = (l_1411 != l_1411);
            if (g_1412)
                break;
        }
        else
        { /* block id: 608 */
            return l_759.f0;
        }
        for (g_44.f0 = 0; (g_44.f0 <= 8); g_44.f0 += 1)
        { /* block id: 613 */
            int32_t *l_1413 = &g_765;
            int32_t *l_1415 = (void*)0;
            int32_t *l_1416 = &g_765;
            int32_t *l_1417 = &g_765;
            int32_t *l_1418[8][8][4] = {{{&g_156,&g_765,&l_106,&g_16},{&l_106,&g_16,&g_16,&l_106},{&g_765,(void*)0,&l_106,&g_156},{(void*)0,&g_16,(void*)0,&l_106},{&l_106,&g_765,&g_16,&l_106},{(void*)0,&g_16,&g_765,&g_156},{&g_765,(void*)0,&g_765,&l_106},{&g_16,&g_16,&g_156,&g_16}},{{&l_106,&g_765,&g_156,&g_16},{&g_16,&l_106,&g_156,&g_765},{&l_106,&g_765,&g_156,&g_156},{&g_16,&g_16,&g_765,&g_16},{&g_765,&g_16,&g_765,(void*)0},{(void*)0,(void*)0,&l_106,(void*)0},{&g_765,&g_16,(void*)0,&l_106},{&g_16,&g_16,&g_765,&g_16}},{{&g_16,&l_106,&g_16,&l_106},{&g_765,&g_765,&l_106,(void*)0},{&l_106,&l_106,&g_16,&g_16},{&l_106,&g_765,&l_106,&g_765},{&g_765,&g_16,&g_16,&g_765},{&g_16,&l_106,&g_765,&g_156},{&g_16,&g_765,(void*)0,&g_156},{&g_765,(void*)0,&l_106,&g_156}},{{&l_106,&g_765,(void*)0,&g_156},{&g_765,&l_106,&g_765,&g_765},{&l_106,&g_16,&l_106,&g_765},{&g_156,&g_765,&g_156,&g_16},{&g_16,&l_106,&g_156,(void*)0},{&g_156,&g_765,&l_106,&l_106},{&l_106,&l_106,&g_765,&g_16},{&g_765,&g_16,(void*)0,&l_106}},{{&l_106,&g_16,&l_106,(void*)0},{&g_765,&g_16,(void*)0,&l_106},{&g_16,&g_16,&g_765,&g_16},{&g_16,&l_106,&g_16,&l_106},{&g_765,&g_765,&l_106,(void*)0},{&l_106,&l_106,&g_16,&g_16},{&l_106,&g_765,&l_106,&g_765},{&g_765,&g_16,&g_16,&g_765}},{{&g_16,&l_106,&g_765,&g_156},{&g_16,&g_765,(void*)0,&g_156},{&g_765,(void*)0,&l_106,&g_156},{&l_106,&g_765,(void*)0,&g_156},{&g_765,&l_106,&g_765,&g_765},{&l_106,&g_16,&l_106,&g_765},{&g_156,&g_765,&g_156,&g_16},{&g_16,&l_106,&g_156,(void*)0}},{{&g_156,&g_765,&l_106,&l_106},{&l_106,&l_106,&g_765,&g_16},{&g_765,&g_16,(void*)0,&l_106},{&l_106,&g_16,&l_106,(void*)0},{&g_765,&g_16,(void*)0,&l_106},{&g_16,&g_16,&g_765,&g_16},{&g_16,&l_106,&g_16,&l_106},{&g_765,&g_765,&l_106,(void*)0}},{{&l_106,&l_106,&g_16,&g_16},{&l_106,&g_765,&l_106,&g_765},{&g_765,&g_16,&g_16,&g_765},{&g_16,&l_106,&g_765,&g_156},{&g_16,&g_765,(void*)0,&g_156},{&g_765,(void*)0,&g_765,&g_156},{&l_106,(void*)0,&g_16,&l_106},{&g_16,&l_106,&g_16,&g_765}}};
            int i, j, k;
            --l_1429;
            ++l_1432;
        }
    }
    for (l_1427 = (-7); (l_1427 <= 16); l_1427 = safe_add_func_int16_t_s_s(l_1427, 9))
    { /* block id: 620 */
        uint64_t l_1439 = 7UL;
        union U1 * const *l_1450 = &g_58;
        float * const l_1459 = &g_111[0][0];
        int32_t *l_1460 = &l_1422;
        (*l_1460) &= ((safe_div_func_int64_t_s_s(((l_1439 , (safe_mod_func_uint32_t_u_u((+((safe_rshift_func_int8_t_s_s(l_1425, (l_1445 , 0xB0L))) <= (((safe_sub_func_uint64_t_u_u(1UL, ((safe_mod_func_uint64_t_u_u((((l_1450 == (g_57 = l_1451)) , (safe_mul_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_s((l_1439 != (!((*g_967) == l_1459))), 6)) , (-1L)), 0x5AL))) , 0xBC72F23F3380534DLL), l_1439)) || 3UL))) || l_1439) ^ l_1439))), 8UL))) & l_1439), 0xE81798F2F3E1B13CLL)) , (**g_231));
        (*g_1461) = l_1459;
        for (g_611 = 9; (g_611 >= 28); g_611 = safe_add_func_uint8_t_u_u(g_611, 5))
        { /* block id: 626 */
            (**g_967) = (g_1412 , (*l_1460));
        }
    }
    return l_1421;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_19
 */
static uint16_t  func_79(union U1 * const * p_80, uint16_t  p_81, union U0  p_82)
{ /* block id: 293 */
    int32_t l_762 = 1L;
    int32_t l_775[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
    uint8_t *** const l_803 = &g_371[6];
    int32_t l_840[3][2];
    int32_t *l_875 = &l_775[8];
    uint8_t l_876 = 0x9EL;
    int32_t l_907[5] = {0xE59F163EL,0xE59F163EL,0xE59F163EL,0xE59F163EL,0xE59F163EL};
    float l_909 = (-0x4.7p+1);
    int32_t ***l_920 = &g_641;
    const uint32_t l_933[6][8] = {{0xC97A18D9L,1UL,0xD4FF66F8L,0xC97A18D9L,0x662906FBL,0x7F9AB055L,0x662906FBL,0xC97A18D9L},{0x37BAEA9EL,0x662906FBL,0x37BAEA9EL,6UL,0x45149762L,4294967294UL,6UL,1UL},{0x662906FBL,0x252B3352L,4294967294UL,0xA1B3DE17L,0x8A507E9BL,0x0560C70AL,0x45149762L,0x252B3352L},{0x662906FBL,1UL,4294967295UL,0x45149762L,0x45149762L,4294967295UL,1UL,0x662906FBL},{0x37BAEA9EL,0xC97A18D9L,0x252B3352L,0x0560C70AL,0x662906FBL,0x55DFD69AL,0x37BAEA9EL,1UL},{0xC97A18D9L,0x0560C70AL,6UL,0xC1CA52A8L,0x0560C70AL,0x55DFD69AL,0x45149762L,0x55DFD69AL}};
    union U1 **l_940 = &g_58;
    uint16_t **l_980 = (void*)0;
    uint16_t ***l_979 = &l_980;
    int8_t l_1014 = 0x9EL;
    union U0 l_1038 = {3UL};
    const uint8_t l_1085 = 2UL;
    int32_t **l_1092 = &g_165;
    const int8_t l_1096[4][9][2] = {{{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL},{0L,0L},{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL},{0L,0L},{0x00L,0x7FL}},{{0x00L,0L},{0L,0x7FL},{0L,0L},{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL},{0L,0L},{0x00L,0x7FL},{0x00L,0L}},{{0L,0x7FL},{0L,0L},{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL},{0L,0L},{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL}},{{0L,0L},{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL},{0L,0L},{0x00L,0x7FL},{0x00L,0L},{0L,0x7FL},{0L,0L}}};
    int64_t *l_1118[7][6][5] = {{{&g_857,&g_857,&g_155,(void*)0,(void*)0},{(void*)0,&g_39,(void*)0,&g_39,&g_155},{&g_39,&g_39,&g_155,&g_857,&g_39},{&g_155,&g_857,&g_857,&g_155,&g_39},{(void*)0,&g_155,&g_155,&g_39,&g_155},{(void*)0,&g_857,(void*)0,&g_155,&g_39}},{{(void*)0,(void*)0,&g_155,&g_39,(void*)0},{&g_155,&g_857,&g_857,&g_155,&g_857},{(void*)0,&g_857,&g_39,(void*)0,&g_39},{&g_155,(void*)0,&g_857,(void*)0,&g_39},{(void*)0,&g_39,(void*)0,&g_155,&g_39},{&g_155,&g_39,&g_39,&g_857,&g_857}},{{&g_39,(void*)0,&g_39,&g_155,&g_155},{(void*)0,&g_39,(void*)0,&g_39,(void*)0},{&g_39,&g_39,&g_857,&g_857,&g_39},{&g_39,&g_39,&g_857,&g_155,&g_39},{&g_155,&g_155,&g_39,&g_39,&g_39},{(void*)0,&g_155,&g_155,&g_857,(void*)0}},{{&g_39,&g_155,&g_857,(void*)0,&g_155},{&g_155,&g_857,&g_39,&g_39,&g_857},{&g_857,&g_857,&g_155,&g_155,&g_39},{(void*)0,(void*)0,&g_39,&g_39,&g_39},{&g_857,&g_39,(void*)0,&g_39,&g_39},{(void*)0,&g_857,&g_857,&g_155,&g_39}},{{&g_857,(void*)0,&g_857,(void*)0,(void*)0},{&g_155,&g_39,&g_39,&g_39,&g_39},{&g_39,&g_39,&g_155,&g_857,(void*)0},{(void*)0,&g_155,&g_39,&g_39,(void*)0},{&g_155,(void*)0,&g_857,&g_155,&g_857},{&g_39,&g_155,&g_155,(void*)0,(void*)0}},{{&g_39,&g_39,&g_155,&g_857,(void*)0},{(void*)0,&g_39,&g_39,&g_857,&g_857},{&g_39,(void*)0,&g_39,&g_39,(void*)0},{&g_155,&g_857,(void*)0,&g_39,&g_155},{(void*)0,&g_39,(void*)0,&g_39,&g_857},{&g_155,(void*)0,(void*)0,&g_155,&g_39}},{{&g_857,&g_857,&g_39,(void*)0,&g_39},{&g_857,&g_857,&g_39,&g_155,&g_857},{&g_857,&g_155,&g_155,&g_857,&g_39},{(void*)0,&g_155,&g_155,&g_39,&g_39},{&g_857,&g_155,&g_857,&g_857,(void*)0},{&g_39,&g_39,&g_39,&g_39,&g_155}}};
    float l_1132[5] = {0x1.Cp+1,0x1.Cp+1,0x1.Cp+1,0x1.Cp+1,0x1.Cp+1};
    union U1 l_1161 = {8UL};
    int8_t l_1191 = 0xB6L;
    union U1 ****l_1292 = &g_1223;
    uint32_t l_1295 = 8UL;
    int16_t l_1303 = 0L;
    int64_t l_1410 = (-1L);
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
            l_840[i][j] = 0L;
    }
    for (g_19 = 2; (g_19 > 24); g_19 = safe_add_func_int16_t_s_s(g_19, 1))
    { /* block id: 296 */
        return l_762;
    }
    return p_81;
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_314 g_16 g_165 g_156 g_182 g_108 g_40 g_44.f1 g_204 g_111
 * writes: g_111 g_94 g_182 g_198 g_156 g_308 g_340 g_44.f1 g_204
 */
static union U1 ** func_83(uint32_t  p_84, int32_t  p_85)
{ /* block id: 27 */
    int32_t *l_109 = &g_16;
    float *l_110 = &g_111[0][0];
    int8_t l_178 = 0L;
    int32_t l_197[2];
    const union U0 l_223[7] = {{18446744073709551615UL},{1UL},{18446744073709551615UL},{18446744073709551615UL},{1UL},{18446744073709551615UL},{18446744073709551615UL}};
    uint8_t *l_316[4] = {&g_254,&g_254,&g_254,&g_254};
    uint8_t **l_315 = &l_316[2];
    int16_t *l_341 = &g_19;
    uint16_t l_539[2];
    int8_t l_602 = 6L;
    int32_t l_607 = 0x4725F220L;
    int16_t l_609[3];
    int64_t l_610 = 2L;
    uint16_t l_649 = 1UL;
    uint32_t l_659 = 0xA7A02942L;
    union U1 l_702 = {0xA0D97E53L};
    float l_735 = 0xB.9F40AAp+61;
    int i;
    for (i = 0; i < 2; i++)
        l_197[i] = (-1L);
    for (i = 0; i < 2; i++)
        l_539[i] = 0xBA45L;
    for (i = 0; i < 3; i++)
        l_609[i] = 0xAE03L;
    (*l_110) = (l_109 != (void*)0);
    if (p_84)
    { /* block id: 29 */
        float l_114 = (-0x1.Cp-1);
        float **l_117[6];
        int32_t l_153 = 0x97907AF3L;
        uint64_t *l_179[7][9] = {{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]},{&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0],&g_40[0]}};
        float l_180 = 0xA.E6350Ap-77;
        int32_t l_199 = (-1L);
        int32_t l_200[3][8] = {{0xC701984FL,0x6D0D7E74L,0x6D0D7E74L,0xC701984FL,0x6D0D7E74L,0x6D0D7E74L,0xC701984FL,0x6D0D7E74L},{0xC701984FL,0xC701984FL,(-2L),0xC701984FL,0xC701984FL,(-2L),0xC701984FL,0xC701984FL},{0x6D0D7E74L,0xC701984FL,0x6D0D7E74L,0x6D0D7E74L,0xC701984FL,0x6D0D7E74L,0x6D0D7E74L,0xC701984FL}};
        int32_t * volatile l_256 = (void*)0;/* VOLATILE GLOBAL l_256 */
        float *l_313 = &l_180;
        uint8_t *l_319 = &g_182;
        int i, j;
        for (i = 0; i < 6; i++)
            l_117[i] = &l_110;
        for (g_94 = 0; (g_94 != 0); g_94 = safe_add_func_uint16_t_u_u(g_94, 7))
        { /* block id: 32 */
            uint64_t *l_146 = &g_108[0][5][1];
            int32_t l_152 = (-1L);
            float **l_168[2][10] = {{&l_110,(void*)0,&l_110,(void*)0,&l_110,&l_110,(void*)0,&l_110,(void*)0,&l_110},{&l_110,(void*)0,&l_110,(void*)0,&l_110,&l_110,(void*)0,&l_110,(void*)0,&l_110}};
            int32_t l_201 = (-10L);
            int32_t l_202 = 0x0FFB38DBL;
            uint8_t *l_250 = &g_182;
            uint8_t *l_253[1][4];
            uint32_t *l_255 = (void*)0;
            uint16_t *l_303 = &g_44.f1;
            int16_t *l_304 = (void*)0;
            int16_t *l_305 = (void*)0;
            int16_t *l_306 = (void*)0;
            int16_t *l_307 = &g_308[1][4];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 4; j++)
                    l_253[i][j] = &g_254;
            }
        }
        for (g_182 = 0; g_182 < 3; g_182 += 1)
        {
            for (g_198 = 0; g_198 < 8; g_198 += 1)
            {
                l_200[g_182][g_198] = 0xFBF1047AL;
            }
        }
        (*g_165) &= (l_197[1] = (safe_sub_func_int16_t_s_s(4L, ((safe_lshift_func_uint8_t_u_s(((l_313 != l_110) | (g_314 == l_315)), (safe_div_func_uint8_t_u_u((((void*)0 != l_319) <= (safe_mul_func_int8_t_s_s((7UL > ((safe_div_func_uint16_t_u_u(((&l_200[1][5] == (void*)0) & 0x2DL), 0x87CDL)) ^ p_85)), p_85))), (*l_109))))) || (-3L)))));
    }
    else
    { /* block id: 101 */
        int16_t *l_334 = &g_308[0][0];
        int64_t *l_337[10][4][2] = {{{&g_39,&g_39},{&g_39,&g_39},{&g_39,&g_155},{&g_39,&g_39}},{{&g_39,&g_39},{&g_39,&g_155},{(void*)0,(void*)0},{&g_39,(void*)0}},{{(void*)0,&g_155},{&g_39,&g_39},{&g_39,&g_39},{&g_39,&g_155}},{{&g_39,&g_39},{&g_39,&g_39},{&g_39,&g_155},{(void*)0,(void*)0}},{{&g_39,(void*)0},{(void*)0,&g_155},{&g_39,&g_39},{&g_39,&g_39}},{{&g_39,&g_155},{&g_39,&g_39},{&g_39,&g_39},{&g_39,&g_155}},{{(void*)0,(void*)0},{&g_39,(void*)0},{(void*)0,&g_155},{&g_39,&g_39}},{{&g_39,&g_39},{&g_39,&g_155},{&g_39,&g_39},{&g_39,&g_39}},{{&g_39,&g_155},{(void*)0,(void*)0},{&g_39,(void*)0},{(void*)0,&g_155}},{{&g_39,&g_39},{&g_39,&g_39},{&g_39,&g_155},{&g_39,&g_39}}};
        int64_t **l_338 = (void*)0;
        int64_t **l_339[8][6] = {{&l_337[0][0][1],&l_337[1][1][0],&l_337[0][0][1],&l_337[1][2][0],&l_337[0][1][0],&l_337[0][1][0]},{(void*)0,&l_337[0][0][1],&l_337[0][0][1],(void*)0,&l_337[1][1][0],&l_337[4][1][0]},{&l_337[4][1][0],&l_337[0][0][1],&l_337[1][1][0],&l_337[0][0][1],&l_337[1][2][0],&l_337[0][1][0]},{&l_337[0][0][1],&l_337[1][2][0],&l_337[0][1][0],&l_337[0][1][0],&l_337[1][2][0],&l_337[0][0][1]},{&l_337[4][1][0],&l_337[0][0][1],&l_337[4][0][0],&l_337[1][2][0],&l_337[4][0][0],&l_337[0][0][1]},{&l_337[4][0][0],&l_337[4][1][0],&l_337[0][1][0],&l_337[1][1][0],&l_337[1][1][0],&l_337[0][1][0]},{&l_337[4][0][0],&l_337[4][0][0],&l_337[1][1][0],&l_337[1][2][0],(void*)0,&l_337[1][2][0]},{&l_337[4][1][0],&l_337[4][0][0],&l_337[4][1][0],&l_337[0][1][0],&l_337[1][1][0],&l_337[1][1][0]}};
        int32_t l_346 = 0x820CAB86L;
        uint16_t *l_347 = &g_44.f1;
        int i, j, k;
        (*g_165) = (safe_div_func_uint16_t_u_u(((*l_347) ^= (safe_div_func_int64_t_s_s((safe_lshift_func_uint16_t_u_u(((*l_109) || (safe_sub_func_int64_t_s_s(((((safe_lshift_func_uint8_t_u_s((((*l_334) = 0x82FCL) & (safe_add_func_int16_t_s_s(p_84, (l_334 != ((&g_203 != (g_340 = l_337[0][1][0])) , l_341))))), 6)) | (safe_div_func_uint16_t_u_u((((((g_182 != ((safe_mul_func_int16_t_s_s(p_85, 65530UL)) > (*l_109))) ^ p_85) & 65529UL) >= g_108[0][7][2]) , p_85), l_346))) > 0xE325C52AL) != 4294967295UL), 18446744073709551615UL))), g_40[0])), (*l_109)))), (*l_109)));
    }
    for (g_204 = 0; (g_204 < 3); g_204 = safe_add_func_int64_t_s_s(g_204, 2))
    { /* block id: 109 */
        uint16_t **l_351 = &g_350;
        int32_t l_356 = (-1L);
        int8_t *l_357 = &l_178;
        uint64_t *l_358 = &g_108[0][6][1];
        uint16_t *l_359 = &g_44.f1;
        const int32_t *l_390[8][1] = {{&l_197[1]},{&l_356},{&l_197[1]},{&l_356},{&l_197[1]},{&l_356},{&l_197[1]},{&l_356}};
        uint8_t * const l_412 = &g_413[1];
        uint8_t * const *l_411[1];
        int32_t l_432 = 5L;
        union U0 *l_507 = &g_506;
        const uint16_t l_508[5] = {0x8BB8L,0x8BB8L,0x8BB8L,0x8BB8L,0x8BB8L};
        uint32_t l_528 = 1UL;
        int32_t l_603 = 0xE5F3D409L;
        int32_t l_604[3][8][3] = {{{0x88283429L,0L,6L},{0xCF2076DCL,(-5L),1L},{2L,(-2L),0x88283429L},{0xBF4510F9L,0x6E0F4EE4L,0x1D517274L},{0x1D517274L,0xCF2076DCL,0x1D517274L},{0x82F5DE27L,0xFBF66B20L,0x88283429L},{0xAB17E022L,0xB737E82BL,1L},{0xB7408332L,2L,6L}},{{0xB737E82BL,6L,2L},{0xB7408332L,1L,0xB737E82BL},{0xAB17E022L,0x88283429L,0xFBF66B20L},{0x82F5DE27L,0x1D517274L,0xCF2076DCL},{0x1D517274L,0x1D517274L,0x6E0F4EE4L},{0xBF4510F9L,0x88283429L,(-2L)},{2L,1L,(-5L)},{0xCF2076DCL,6L,0L}},{{0x88283429L,2L,(-5L)},{0xFBF66B20L,0xB737E82BL,(-2L)},{(-1L),0xFBF66B20L,0x6E0F4EE4L},{(-1L),0xCF2076DCL,0xCF2076DCL},{(-1L),0x6E0F4EE4L,0xFBF66B20L},{(-1L),(-2L),0xB737E82BL},{0xFBF66B20L,(-5L),2L},{0x88283429L,0L,6L}}};
        float l_687 = 0x5.2E85F3p-54;
        float l_740[9][8] = {{0xE.2D4A44p-81,0x8.74FA96p-95,0x1.Ep+1,0x4.133C90p+22,0x0.E02FD7p+81,0x1.Ep+1,0x4.13B443p+21,0x1.63C75Bp+12},{0x8.BD53C4p+24,0xD.77A31Bp+54,0x1.96F73Fp+87,0x1.Bp+1,0x0.E02FD7p+81,0x1.42F318p-97,0x1.9p+1,0xD.77A31Bp+54},{0xE.2D4A44p-81,0x1.96F73Fp+87,0x0.Dp-1,0x1.63C75Bp+12,0x1.Bp+1,0x1.Bp+1,0x1.63C75Bp+12,0x0.Dp-1},{0x4.133C90p+22,0x4.133C90p+22,0xE.B76EB5p+31,0x4.13B443p+21,0xE.2D4A44p-81,0x1.Ep+1,0x0.Bp-1,0x8.74FA96p-95},{0x0.Dp-1,0x4.13B443p+21,0x7.5p+1,0x1.42F318p-97,0x8.74FA96p-95,0x1.96F73Fp+87,0x1.9p+1,0x8.74FA96p-95},{0x4.13B443p+21,0x1.9p+1,(-0x1.2p-1),0x4.13B443p+21,0x1.42F318p-97,0x0.Dp-1,(-0x8.1p-1),0x0.Dp-1},{(-0x8.1p-1),0x1.63C75Bp+12,0x1.Ep+1,0x1.63C75Bp+12,(-0x8.1p-1),0xE.B76EB5p+31,0x0.E02FD7p+81,0xD.77A31Bp+54},{0x0.Dp-1,0x0.Bp-1,0xA.DE5B3Ap-42,0x1.Bp+1,0x0.Bp-1,0x7.5p+1,0x1.96F73Fp+87,0x1.63C75Bp+12},{0x0.E02FD7p+81,0x1.9p+1,0xA.DE5B3Ap-42,0x4.133C90p+22,0x1.Bp+1,(-0x1.2p-1),0x0.E02FD7p+81,0x0.47813Cp+41}};
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_411[i] = &l_412;
    }
    (*g_165) = (*l_109);
    return &g_58;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    transparent_crc(g_39, "g_39", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_40[i], "g_40[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_44.f0, "g_44.f0", print_hash_value);
    transparent_crc(g_44.f1, "g_44.f1", print_hash_value);
    transparent_crc(g_94, "g_94", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_108[i][j][k], "g_108[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc_bytes(&g_111[i][j], sizeof(g_111[i][j]), "g_111[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_155, "g_155", print_hash_value);
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc(g_182, "g_182", print_hash_value);
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_203, "g_203", print_hash_value);
    transparent_crc(g_204, "g_204", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    transparent_crc(g_254, "g_254", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_308[i][j], "g_308[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_413[i], "g_413[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_455[i], "g_455[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_506.f0, "g_506.f0", print_hash_value);
    transparent_crc_bytes (&g_599, sizeof(g_599), "g_599", print_hash_value);
    transparent_crc(g_606, "g_606", print_hash_value);
    transparent_crc(g_608, "g_608", print_hash_value);
    transparent_crc(g_611, "g_611", print_hash_value);
    transparent_crc(g_612, "g_612", print_hash_value);
    transparent_crc(g_765, "g_765", print_hash_value);
    transparent_crc(g_857, "g_857", print_hash_value);
    transparent_crc(g_888, "g_888", print_hash_value);
    transparent_crc(g_1406, "g_1406", print_hash_value);
    transparent_crc(g_1412, "g_1412", print_hash_value);
    transparent_crc(g_1414, "g_1414", print_hash_value);
    transparent_crc(g_1487, "g_1487", print_hash_value);
    transparent_crc(g_1495, "g_1495", print_hash_value);
    transparent_crc(g_1503, "g_1503", print_hash_value);
    transparent_crc(g_1512, "g_1512", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1618[i].f0, "g_1618[i].f0", print_hash_value);
        transparent_crc(g_1618[i].f1, "g_1618[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1635[i][j], "g_1635[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1682[i][j][k], "g_1682[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_1710[i][j], "g_1710[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1732, "g_1732", print_hash_value);
    transparent_crc_bytes (&g_1931, sizeof(g_1931), "g_1931", print_hash_value);
    transparent_crc(g_2024, "g_2024", print_hash_value);
    transparent_crc(g_2134, "g_2134", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2167[i], "g_2167[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2273, "g_2273", print_hash_value);
    transparent_crc(g_2294, "g_2294", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2311[i][j][k], "g_2311[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2371, "g_2371", print_hash_value);
    transparent_crc(g_2398, "g_2398", print_hash_value);
    transparent_crc(g_2544, "g_2544", print_hash_value);
    transparent_crc(g_2755, "g_2755", print_hash_value);
    transparent_crc_bytes (&g_3167, sizeof(g_3167), "g_3167", print_hash_value);
    transparent_crc(g_3176, "g_3176", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_3178[i], "g_3178[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3184, "g_3184", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_3264[i], "g_3264[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3345, "g_3345", print_hash_value);
    transparent_crc(g_3432, "g_3432", print_hash_value);
    transparent_crc(g_3744, "g_3744", print_hash_value);
    transparent_crc(g_3896, "g_3896", print_hash_value);
    transparent_crc(g_3902, "g_3902", print_hash_value);
    transparent_crc(g_3941, "g_3941", print_hash_value);
    transparent_crc(g_4124, "g_4124", print_hash_value);
    transparent_crc(g_4127, "g_4127", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1023
XXX total union variables: 45

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 198
   depth: 2, occurrence: 47
   depth: 3, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 9, occurrence: 1
   depth: 15, occurrence: 3
   depth: 16, occurrence: 1
   depth: 17, occurrence: 4
   depth: 18, occurrence: 3
   depth: 19, occurrence: 3
   depth: 20, occurrence: 3
   depth: 21, occurrence: 3
   depth: 23, occurrence: 4
   depth: 24, occurrence: 2
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 4
   depth: 33, occurrence: 1
   depth: 39, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 779

XXX times a variable address is taken: 1711
XXX times a pointer is dereferenced on RHS: 554
breakdown:
   depth: 1, occurrence: 375
   depth: 2, occurrence: 110
   depth: 3, occurrence: 56
   depth: 4, occurrence: 8
   depth: 5, occurrence: 5
XXX times a pointer is dereferenced on LHS: 553
breakdown:
   depth: 1, occurrence: 447
   depth: 2, occurrence: 84
   depth: 3, occurrence: 14
   depth: 4, occurrence: 3
   depth: 5, occurrence: 5
XXX times a pointer is compared with null: 69
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 28
XXX times a pointer is qualified to be dereferenced: 14242

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2111
   level: 2, occurrence: 702
   level: 3, occurrence: 233
   level: 4, occurrence: 115
   level: 5, occurrence: 31
XXX number of pointers point to pointers: 348
XXX number of pointers point to scalars: 410
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.9
XXX average alias set size: 1.46

XXX times a non-volatile is read: 3389
XXX times a non-volatile is write: 1709
XXX times a volatile is read: 122
XXX    times read thru a pointer: 41
XXX times a volatile is write: 24
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 4.96e+03
XXX percentage of non-volatile access: 97.2

XXX forward jumps: 2
XXX backward jumps: 11

XXX stmts: 198
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 28
   depth: 2, occurrence: 30
   depth: 3, occurrence: 26
   depth: 4, occurrence: 37
   depth: 5, occurrence: 48

XXX percentage a fresh-made variable is used: 15.9
XXX percentage an existing variable is used: 84.1
********************* end of statistics **********************/


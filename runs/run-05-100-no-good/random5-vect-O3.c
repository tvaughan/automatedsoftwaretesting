/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      686519719
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   int16_t  f0;
   unsigned f1 : 23;
   signed f2 : 17;
   unsigned f3 : 17;
   unsigned f4 : 20;
   const unsigned f5 : 18;
   volatile unsigned f6 : 13;
};

/* --- GLOBAL VARIABLES --- */
static float g_4 = 0x4.65CD38p+74;
static float * volatile g_3 = &g_4;/* VOLATILE GLOBAL g_3 */
static uint64_t g_8[10][10] = {{0xC7A2E428464D91B0LL,0x027682C5A4ECB5CBLL,7UL,0x027682C5A4ECB5CBLL,0xC7A2E428464D91B0LL,0x8ABB75DA2CA335C2LL,7UL,0xB6A69EED08E15976LL,0xC353271639956A21LL,18446744073709551615UL},{7UL,18446744073709551607UL,18446744073709551615UL,0x8ABB75DA2CA335C2LL,0x33B016FEFD52276CLL,0xA424ACDFF9052196LL,0xA424ACDFF9052196LL,0x33B016FEFD52276CLL,0x8ABB75DA2CA335C2LL,18446744073709551615UL},{0x8ABB75DA2CA335C2LL,0x8ABB75DA2CA335C2LL,0UL,18446744073709551615UL,0xC7A2E428464D91B0LL,0x25A33ED863780D77LL,0x33B016FEFD52276CLL,7UL,0xB6EA1325A46E4944LL,0x2BA8D7D2A7E9F21CLL},{7UL,0x685E9D070FEEA382LL,0x33B016FEFD52276CLL,0xC353271639956A21LL,0x027682C5A4ECB5CBLL,0xC353271639956A21LL,0x33B016FEFD52276CLL,0x685E9D070FEEA382LL,7UL,0x8ABB75DA2CA335C2LL},{18446744073709551607UL,0x8ABB75DA2CA335C2LL,0xB6EA1325A46E4944LL,0xC7A2E428464D91B0LL,0x2BA8D7D2A7E9F21CLL,0UL,0UL,0x8ABB75DA2CA335C2LL,0x8ABB75DA2CA335C2LL,0UL},{0x027682C5A4ECB5CBLL,0xB6EA1325A46E4944LL,0x25A33ED863780D77LL,0x25A33ED863780D77LL,0xB6EA1325A46E4944LL,0x027682C5A4ECB5CBLL,0UL,18446744073709551615UL,0xB6A69EED08E15976LL,0xA424ACDFF9052196LL},{0xA424ACDFF9052196LL,0x8ABB75DA2CA335C2LL,7UL,0x685E9D070FEEA382LL,0x33B016FEFD52276CLL,0xC353271639956A21LL,0x027682C5A4ECB5CBLL,0xC353271639956A21LL,0x33B016FEFD52276CLL,0x685E9D070FEEA382LL},{0xA424ACDFF9052196LL,0xC353271639956A21LL,0xA424ACDFF9052196LL,18446744073709551607UL,0UL,0x027682C5A4ECB5CBLL,0x685E9D070FEEA382LL,0xC7A2E428464D91B0LL,7UL,18446744073709551615UL},{0x027682C5A4ECB5CBLL,0x685E9D070FEEA382LL,0xC7A2E428464D91B0LL,7UL,18446744073709551615UL,18446744073709551615UL,7UL,0xC7A2E428464D91B0LL,0x685E9D070FEEA382LL,0x027682C5A4ECB5CBLL},{0xB6EA1325A46E4944LL,7UL,0xA424ACDFF9052196LL,0x8ABB75DA2CA335C2LL,7UL,0x685E9D070FEEA382LL,0x33B016FEFD52276CLL,0xC353271639956A21LL,0x027682C5A4ECB5CBLL,0xC353271639956A21LL}};
static int32_t g_11[1][4][5] = {{{(-10L),1L,0L,0L,1L},{(-10L),1L,0L,0L,1L},{(-10L),1L,0L,0L,1L},{(-10L),1L,0L,0L,1L}}};
static int8_t g_13 = 0L;
static uint32_t g_14 = 0x1947684AL;
static int8_t g_21 = 0x14L;
static int8_t *g_20 = &g_21;
static uint64_t g_35 = 0x968D37D67F3E8E03LL;
static uint64_t *g_34[8][9] = {{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35},{&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35}};
static int32_t g_38[9] = {0xD242EC21L,1L,0xD242EC21L,0xD242EC21L,1L,0xD242EC21L,0xD242EC21L,1L,0xD242EC21L};
static int8_t g_82 = 0x88L;
static uint16_t g_84[5] = {0xE3A6L,0xE3A6L,0xE3A6L,0xE3A6L,0xE3A6L};
static struct S0 g_90 = {0L,1744,343,250,668,112,12};/* VOLATILE GLOBAL g_90 */
static struct S0 g_91 = {0xCA4FL,650,270,300,211,104,0};/* VOLATILE GLOBAL g_91 */
static float g_94 = (-0x3.Bp-1);
static int16_t g_112 = (-1L);
static uint32_t g_114 = 0x2328EFC3L;
static int32_t g_121 = 0x5A347877L;
static int32_t g_123 = 0x67A6C791L;
static int16_t g_131 = 0L;
static int32_t g_133 = 0x3911152AL;
static float g_135 = 0x6.Ap+1;
static int64_t g_138 = 0xF3773156C3613B7DLL;
static uint8_t g_139 = 0x35L;
static int32_t g_158 = 0L;
static int32_t g_159 = 0x4D056A74L;
static int32_t g_165[5][2] = {{0x940D3DB0L,0x21C0A70EL},{0xC8E59B3DL,0L},{0x21C0A70EL,0L},{0xC8E59B3DL,0x21C0A70EL},{0x940D3DB0L,0x940D3DB0L}};
static int32_t g_166 = 0x74EA98C3L;
static uint32_t g_167 = 1UL;
static int16_t *g_188 = (void*)0;
static int64_t g_193 = 0xC40381DB17C0C13FLL;
static float g_194 = (-0x8.3p+1);
static uint8_t g_195 = 0x9CL;
static const int32_t g_225 = (-3L);
static int8_t g_254 = 0x5FL;
static int32_t g_260 = (-1L);
static uint32_t g_261 = 18446744073709551615UL;
static int64_t g_295 = 1L;
static uint32_t g_297 = 4294967291UL;
static int32_t g_324 = 6L;
static int32_t g_326[7] = {0L,0L,0L,0L,0L,0L,0L};
static int32_t g_327 = 0x4E6D32FEL;
static int64_t g_329[10] = {0x00D05015EE5588E2LL,0L,0x00D05015EE5588E2LL,0L,0x00D05015EE5588E2LL,0L,0x00D05015EE5588E2LL,0L,0x00D05015EE5588E2LL,0L};
static struct S0 * volatile *g_422 = (void*)0;
static int8_t g_470 = 0x8AL;
static volatile struct S0 *g_480 = (void*)0;
static int32_t **g_481 = (void*)0;
static struct S0 g_485 = {0xC359L,2696,-156,37,742,484,47};/* VOLATILE GLOBAL g_485 */
static int32_t *g_501 = &g_159;
static int64_t g_550 = 0xAA12D2B3BCA4ED89LL;
static int64_t g_576 = 0x9DD14846FE1F281BLL;
static int8_t **g_587 = (void*)0;
static float *g_592 = &g_94;
static float **g_591 = &g_592;
static int16_t g_600 = (-1L);
static uint32_t g_601 = 4294967288UL;
static int32_t g_671 = 0x3FF8ECC9L;
static uint16_t g_673 = 0x4D7FL;
static float g_674 = 0xA.C0CD4Dp-10;
static uint16_t g_675[5][1][3] = {{{8UL,8UL,8UL}},{{8UL,8UL,8UL}},{{8UL,8UL,8UL}},{{8UL,8UL,8UL}},{{8UL,8UL,8UL}}};
static int64_t g_713[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
static uint8_t g_715[7][2][3] = {{{8UL,1UL,253UL},{0x66L,8UL,253UL}},{{0xC8L,251UL,0UL},{0x10L,8UL,0x10L}},{{0x10L,1UL,0x66L},{0xC8L,2UL,0x10L}},{{0x66L,2UL,0UL},{8UL,1UL,253UL}},{{0x66L,8UL,253UL},{0xC8L,251UL,0UL}},{{0x10L,8UL,0x10L},{0x10L,1UL,0x66L}},{{0xC8L,2UL,0x10L},{0x66L,2UL,0UL}}};
static uint8_t g_775 = 0x56L;
static int8_t g_788[8] = {0xA4L,0xA4L,0xA4L,0xA4L,0xA4L,0xA4L,0xA4L,0xA4L};
static int8_t * const g_787 = &g_788[0];
static int8_t * const *g_786 = &g_787;
static uint32_t g_821 = 18446744073709551608UL;
static uint32_t g_856 = 1UL;
static uint64_t g_862 = 0xCFFB83E9B4BBE1F0LL;
static struct S0 g_970[8][6][5] = {{{{0x68D8L,731,356,216,915,181,83},{0xF446L,603,264,336,305,245,15},{-4L,1572,-141,43,721,404,66},{8L,2702,-302,99,398,238,26},{-4L,1572,-141,43,721,404,66}},{{0x1678L,1736,-202,3,507,319,38},{0x1678L,1736,-202,3,507,319,38},{0xC8A7L,601,307,254,786,429,64},{-10L,2842,263,148,462,401,58},{4L,813,-165,103,575,344,5}},{{0x68D8L,731,356,216,915,181,83},{0x7CA0L,2213,101,23,692,110,81},{-7L,1321,199,97,683,146,57},{-3L,1651,145,209,522,140,53},{-3L,1651,145,209,522,140,53}},{{-10L,2842,263,148,462,401,58},{-9L,2782,-312,186,658,270,83},{-10L,2842,263,148,462,401,58},{0xFA5EL,2560,352,107,1019,355,46},{0x7A5CL,1162,-150,228,757,109,76}},{{-7L,1321,199,97,683,146,57},{0x7CA0L,2213,101,23,692,110,81},{0x68D8L,731,356,216,915,181,83},{0xC6DFL,443,328,109,455,310,72},{-10L,314,-182,119,429,445,46}},{{0xC8A7L,601,307,254,786,429,64},{0x1678L,1736,-202,3,507,319,38},{0x1678L,1736,-202,3,507,319,38},{0xC8A7L,601,307,254,786,429,64},{-10L,2842,263,148,462,401,58}}},{{{-4L,1572,-141,43,721,404,66},{0xF446L,603,264,336,305,245,15},{0x68D8L,731,356,216,915,181,83},{-10L,314,-182,119,429,445,46},{0x7CA0L,2213,101,23,692,110,81}},{{0x43FAL,241,-178,66,160,489,36},{0L,2675,-321,341,793,33,67},{-10L,2842,263,148,462,401,58},{0L,2675,-321,341,793,33,67},{0x43FAL,241,-178,66,160,489,36}},{{0xF446L,603,264,336,305,245,15},{0xC6DFL,443,328,109,455,310,72},{-7L,1321,199,97,683,146,57},{-10L,314,-182,119,429,445,46},{-1L,2444,-325,332,847,356,9}},{{-1L,2681,309,84,407,476,13},{0xB3D5L,1537,240,289,640,264,85},{0xC8A7L,601,307,254,786,429,64},{0xC8A7L,601,307,254,786,429,64},{0xB3D5L,1537,240,289,640,264,85}},{{-3L,1651,145,209,522,140,53},{0x68D8L,731,356,216,915,181,83},{-4L,1572,-141,43,721,404,66},{0xC6DFL,443,328,109,455,310,72},{-1L,2444,-325,332,847,356,9}},{{0L,2675,-321,341,793,33,67},{0xC8A7L,601,307,254,786,429,64},{0x43FAL,241,-178,66,160,489,36},{0xFA5EL,2560,352,107,1019,355,46},{0x43FAL,241,-178,66,160,489,36}}},{{{-1L,2444,-325,332,847,356,9},{-1L,2444,-325,332,847,356,9},{0xF446L,603,264,336,305,245,15},{-3L,1651,145,209,522,140,53},{0x7CA0L,2213,101,23,692,110,81}},{{0L,2675,-321,341,793,33,67},{4L,813,-165,103,575,344,5},{-1L,2681,309,84,407,476,13},{-10L,2842,263,148,462,401,58},{-10L,2842,263,148,462,401,58}},{{-3L,1651,145,209,522,140,53},{0x2B36L,1064,223,100,119,45,69},{-3L,1651,145,209,522,140,53},{8L,2702,-302,99,398,238,26},{-10L,314,-182,119,429,445,46}},{{-1L,2681,309,84,407,476,13},{4L,813,-165,103,575,344,5},{0L,2675,-321,341,793,33,67},{0xB3D5L,1537,240,289,640,264,85},{0x7A5CL,1162,-150,228,757,109,76}},{{0xF446L,603,264,336,305,245,15},{-1L,2444,-325,332,847,356,9},{-1L,2444,-325,332,847,356,9},{0xF446L,603,264,336,305,245,15},{-3L,1651,145,209,522,140,53}},{{0x43FAL,241,-178,66,160,489,36},{0xC8A7L,601,307,254,786,429,64},{0L,2675,-321,341,793,33,67},{0x7A5CL,1162,-150,228,757,109,76},{4L,813,-165,103,575,344,5}}},{{{-4L,1572,-141,43,721,404,66},{0x68D8L,731,356,216,915,181,83},{-3L,1651,145,209,522,140,53},{0x68D8L,731,356,216,915,181,83},{-4L,1572,-141,43,721,404,66}},{{0xC8A7L,601,307,254,786,429,64},{0xB3D5L,1537,240,289,640,264,85},{-1L,2681,309,84,407,476,13},{0x7A5CL,1162,-150,228,757,109,76},{0x1678L,1736,-202,3,507,319,38}},{{-7L,1321,199,97,683,146,57},{0xC6DFL,443,328,109,455,310,72},{0xF446L,603,264,336,305,245,15},{0xF446L,603,264,336,305,245,15},{0xC6DFL,443,328,109,455,310,72}},{{-10L,2842,263,148,462,401,58},{0L,2675,-321,341,793,33,67},{0x43FAL,241,-178,66,160,489,36},{0xB3D5L,1537,240,289,640,264,85},{0x1678L,1736,-202,3,507,319,38}},{{0x68D8L,731,356,216,915,181,83},{0xF446L,603,264,336,305,245,15},{-4L,1572,-141,43,721,404,66},{8L,2702,-302,99,398,238,26},{-4L,1572,-141,43,721,404,66}},{{0x1678L,1736,-202,3,507,319,38},{0x1678L,1736,-202,3,507,319,38},{0xC8A7L,601,307,254,786,429,64},{-10L,2842,263,148,462,401,58},{0x43FAL,241,-178,66,160,489,36}}},{{{0x2B36L,1064,223,100,119,45,69},{-4L,1572,-141,43,721,404,66},{-3L,1651,145,209,522,140,53},{0xC6DFL,443,328,109,455,310,72},{0xC6DFL,443,328,109,455,310,72}},{{0xB3D5L,1537,240,289,640,264,85},{0x7A5CL,1162,-150,228,757,109,76},{0xB3D5L,1537,240,289,640,264,85},{0L,2675,-321,341,793,33,67},{4L,813,-165,103,575,344,5}},{{-3L,1651,145,209,522,140,53},{-4L,1572,-141,43,721,404,66},{0x2B36L,1064,223,100,119,45,69},{0xF446L,603,264,336,305,245,15},{0x7CA0L,2213,101,23,692,110,81}},{{0x1678L,1736,-202,3,507,319,38},{0xFA5EL,2560,352,107,1019,355,46},{0xFA5EL,2560,352,107,1019,355,46},{0x1678L,1736,-202,3,507,319,38},{0xB3D5L,1537,240,289,640,264,85}},{{-7L,1321,199,97,683,146,57},{-1L,2444,-325,332,847,356,9},{0x2B36L,1064,223,100,119,45,69},{0x7CA0L,2213,101,23,692,110,81},{-4L,1572,-141,43,721,404,66}},{{-1L,2681,309,84,407,476,13},{-9L,2782,-312,186,658,270,83},{0xB3D5L,1537,240,289,640,264,85},{-9L,2782,-312,186,658,270,83},{-1L,2681,309,84,407,476,13}}},{{{-1L,2444,-325,332,847,356,9},{0xF446L,603,264,336,305,245,15},{-3L,1651,145,209,522,140,53},{0x7CA0L,2213,101,23,692,110,81},{8L,2702,-302,99,398,238,26}},{{-10L,2842,263,148,462,401,58},{0xC8A7L,601,307,254,786,429,64},{0x1678L,1736,-202,3,507,319,38},{0x1678L,1736,-202,3,507,319,38},{0xC8A7L,601,307,254,786,429,64}},{{0xC6DFL,443,328,109,455,310,72},{0x2B36L,1064,223,100,119,45,69},{-7L,1321,199,97,683,146,57},{0xF446L,603,264,336,305,245,15},{8L,2702,-302,99,398,238,26}},{{-9L,2782,-312,186,658,270,83},{0x1678L,1736,-202,3,507,319,38},{-1L,2681,309,84,407,476,13},{0L,2675,-321,341,793,33,67},{-1L,2681,309,84,407,476,13}},{{8L,2702,-302,99,398,238,26},{8L,2702,-302,99,398,238,26},{-1L,2444,-325,332,847,356,9},{0xC6DFL,443,328,109,455,310,72},{-4L,1572,-141,43,721,404,66}},{{-9L,2782,-312,186,658,270,83},{0x43FAL,241,-178,66,160,489,36},{-10L,2842,263,148,462,401,58},{0xB3D5L,1537,240,289,640,264,85},{0xB3D5L,1537,240,289,640,264,85}}},{{{0xC6DFL,443,328,109,455,310,72},{-10L,314,-182,119,429,445,46},{0xC6DFL,443,328,109,455,310,72},{0x68D8L,731,356,216,915,181,83},{0x7CA0L,2213,101,23,692,110,81}},{{-10L,2842,263,148,462,401,58},{0x43FAL,241,-178,66,160,489,36},{-9L,2782,-312,186,658,270,83},{0xC8A7L,601,307,254,786,429,64},{4L,813,-165,103,575,344,5}},{{-1L,2444,-325,332,847,356,9},{8L,2702,-302,99,398,238,26},{8L,2702,-302,99,398,238,26},{-1L,2444,-325,332,847,356,9},{0xC6DFL,443,328,109,455,310,72}},{{-1L,2681,309,84,407,476,13},{0x1678L,1736,-202,3,507,319,38},{-9L,2782,-312,186,658,270,83},{4L,813,-165,103,575,344,5},{0x43FAL,241,-178,66,160,489,36}},{{-7L,1321,199,97,683,146,57},{0x2B36L,1064,223,100,119,45,69},{0xC6DFL,443,328,109,455,310,72},{0x2B36L,1064,223,100,119,45,69},{-7L,1321,199,97,683,146,57}},{{0x1678L,1736,-202,3,507,319,38},{0xC8A7L,601,307,254,786,429,64},{-10L,2842,263,148,462,401,58},{4L,813,-165,103,575,344,5},{0xFA5EL,2560,352,107,1019,355,46}}},{{{-3L,1651,145,209,522,140,53},{0xF446L,603,264,336,305,245,15},{-1L,2444,-325,332,847,356,9},{-1L,2444,-325,332,847,356,9},{0xF446L,603,264,336,305,245,15}},{{0xB3D5L,1537,240,289,640,264,85},{-9L,2782,-312,186,658,270,83},{-1L,2681,309,84,407,476,13},{0xC8A7L,601,307,254,786,429,64},{0xFA5EL,2560,352,107,1019,355,46}},{{0x2B36L,1064,223,100,119,45,69},{-1L,2444,-325,332,847,356,9},{-7L,1321,199,97,683,146,57},{0x68D8L,731,356,216,915,181,83},{-7L,1321,199,97,683,146,57}},{{0xFA5EL,2560,352,107,1019,355,46},{0xFA5EL,2560,352,107,1019,355,46},{0x1678L,1736,-202,3,507,319,38},{0xB3D5L,1537,240,289,640,264,85},{0x43FAL,241,-178,66,160,489,36}},{{0x2B36L,1064,223,100,119,45,69},{-4L,1572,-141,43,721,404,66},{-3L,1651,145,209,522,140,53},{0xC6DFL,443,328,109,455,310,72},{0xC6DFL,443,328,109,455,310,72}},{{0xB3D5L,1537,240,289,640,264,85},{0x7A5CL,1162,-150,228,757,109,76},{0xB3D5L,1537,240,289,640,264,85},{0L,2675,-321,341,793,33,67},{4L,813,-165,103,575,344,5}}}};
static struct S0 g_973 = {0x2E6AL,1342,-349,259,98,105,22};/* VOLATILE GLOBAL g_973 */
static struct S0 *g_972 = &g_973;
static struct S0 g_1016 = {-6L,2116,111,147,553,273,55};/* VOLATILE GLOBAL g_1016 */
static int64_t * volatile g_1033[6] = {&g_576,&g_576,&g_576,&g_576,&g_576,&g_576};
static int64_t * volatile *g_1032 = &g_1033[5];
static int64_t * volatile **g_1031 = &g_1032;
static float g_1058 = 0x9.2AFF88p+70;
static int32_t *g_1103 = &g_38[3];
static uint8_t g_1219 = 0xE5L;
static volatile int8_t * volatile **g_1223 = (void*)0;
static volatile int8_t * volatile ***g_1222 = &g_1223;
static volatile float g_1235 = 0x8.34EBE2p+5;/* VOLATILE GLOBAL g_1235 */
static volatile float *g_1234 = &g_1235;
static uint8_t *g_1246 = &g_139;
static float * const **g_1314 = (void*)0;
static int32_t g_1591 = (-1L);
static uint8_t g_1646 = 0x38L;
static int32_t * const  volatile *g_1679 = &g_1103;
static int32_t * const  volatile * volatile *g_1678[5][7] = {{&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679},{&g_1679,(void*)0,&g_1679,&g_1679,(void*)0,&g_1679,&g_1679},{&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679},{(void*)0,&g_1679,&g_1679,(void*)0,&g_1679,&g_1679,(void*)0},{&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679}};
static int32_t * const  volatile * volatile * volatile *g_1677 = &g_1678[2][2];
static float g_1756 = 0xC.25101Cp-58;
static int8_t g_1784 = 0x4EL;
static uint8_t g_1791 = 249UL;
static uint16_t *g_1889 = &g_84[2];
static uint16_t **g_1888 = &g_1889;
static uint16_t ***g_1887 = &g_1888;
static float g_1894[2] = {0x0.9p+1,0x0.9p+1};
static int32_t * volatile g_1932 = &g_671;/* VOLATILE GLOBAL g_1932 */
static int32_t * volatile *g_1931[6] = {&g_1932,&g_1932,&g_1932,&g_1932,&g_1932,&g_1932};
static uint64_t **g_2026 = &g_34[6][2];
static uint64_t ***g_2025 = &g_2026;
static struct S0 g_2208 = {0L,540,-306,285,832,339,34};/* VOLATILE GLOBAL g_2208 */
static uint32_t *g_2216 = &g_601;
static uint32_t *g_2217[4] = {&g_297,&g_297,&g_297,&g_297};
static uint64_t **** volatile g_2301 = &g_2025;/* VOLATILE GLOBAL g_2301 */
static struct S0 g_2343 = {0xD6ABL,1934,351,5,608,11,29};/* VOLATILE GLOBAL g_2343 */
static volatile struct S0 g_2385 = {1L,2206,106,210,243,9,29};/* VOLATILE GLOBAL g_2385 */
static volatile int32_t g_2423 = 0xB5D15369L;/* VOLATILE GLOBAL g_2423 */
static struct S0 g_2435 = {0xD02CL,1233,161,19,776,355,14};/* VOLATILE GLOBAL g_2435 */
static int16_t g_2536[5][10] = {{0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L},{0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L},{0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L},{0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L},{0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L,0x5BA7L,0L}};
static volatile uint8_t g_2560 = 0xC6L;/* VOLATILE GLOBAL g_2560 */
static const volatile float ****g_2614 = (void*)0;
static const volatile float **** volatile * volatile g_2613 = &g_2614;/* VOLATILE GLOBAL g_2613 */
static uint32_t g_2619 = 0x8B824882L;
static uint32_t g_2621 = 0xB9B1E83FL;


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static const uint16_t  func_15(int8_t * p_16, uint64_t * p_17, float * p_18, float * p_19);
static uint64_t * func_22(uint16_t  p_23);
static int32_t  func_27(int8_t  p_28, int8_t * p_29);
static int8_t * func_30(uint64_t * p_31, const uint16_t  p_32, uint32_t  p_33);
static int32_t * func_40(int64_t  p_41, uint64_t  p_42, int16_t  p_43, int8_t * p_44);
static int8_t  func_50(float  p_51, uint16_t  p_52, int8_t * const  p_53, uint8_t  p_54);
static int8_t * const  func_56(uint32_t  p_57);
static uint16_t  func_60(float * p_61, int8_t * p_62, uint16_t  p_63);
static uint32_t  func_64(int8_t  p_65, int32_t * const  p_66, int8_t  p_67, int16_t  p_68);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_8 g_11 g_13 g_20 g_4 g_21 g_34 g_1887 g_1888 g_1889 g_84 g_1791 g_592 g_1234 g_501 g_159 g_1679 g_1103 g_786 g_787 g_788 g_972 g_973 g_165 g_1246 g_139 g_1591 g_2025 g_1031 g_1032 g_1033 g_576 g_38 g_713 g_167 g_1016.f1 g_2026 g_35 g_1235 g_591 g_94 g_90.f5 g_550 g_2301 g_2536 g_821 g_2216 g_601 g_131 g_2613
 * writes: g_4 g_8 g_13 g_14 g_38 g_21 g_84 g_94 g_1235 g_485.f0 g_91.f0 g_1591 g_139 g_167 g_35 g_576 g_295 g_193 g_1058 g_254 g_133 g_159 g_821 g_1103 g_601 g_673 g_973.f0 g_297 g_2619
 */
static float  func_1(void)
{ /* block id: 0 */
    int32_t l_2[8][6] = {{(-8L),0x3923CCDEL,1L,5L,0xC76D7BE7L,0xB74C38B6L},{0xBF33880EL,1L,0x33C22515L,0xA82A10AAL,0x34215E1AL,(-1L)},{0xBF33880EL,5L,0xA82A10AAL,5L,0xBF33880EL,0xC76D7BE7L},{(-8L),0xC76D7BE7L,0x34215E1AL,0xBF33880EL,(-1L),(-10L)},{1L,0xB74C38B6L,(-1L),0xC76D7BE7L,(-10L),(-10L)},{0x434678DCL,0x34215E1AL,0x34215E1AL,0x434678DCL,0x0E1B1B55L,0xC76D7BE7L},{(-10L),0xBC588E1BL,0xA82A10AAL,0x3923CCDEL,5L,(-1L)},{0xA82A10AAL,1L,0x33C22515L,0xB74C38B6L,5L,0xB74C38B6L}};
    float *l_5 = &g_4;
    float *l_6 = &g_4;
    uint64_t *l_7[9] = {(void*)0,&g_8[3][3],(void*)0,(void*)0,&g_8[3][3],(void*)0,(void*)0,&g_8[3][3],(void*)0};
    int8_t *l_12 = &g_13;
    uint8_t l_24 = 1UL;
    int64_t l_2509 = 0x86B6EF1402E7910ALL;
    int16_t l_2511 = 1L;
    const int32_t l_2622[7] = {0xADB4F27BL,(-4L),0xADB4F27BL,0xADB4F27BL,(-4L),0xADB4F27BL,0xADB4F27BL};
    uint32_t l_2624 = 4UL;
    int i, j;
lbl_2523:
    (*g_3) = l_2[6][3];
    if (((((0x35L | (l_5 != (l_6 = (void*)0))) > (g_8[3][3]++)) & (g_14 = ((*l_12) ^= (l_2[3][2] != g_11[0][3][2])))) != (func_15(g_20, func_22(l_24), (*g_591), l_5) , l_2[2][4])))
    { /* block id: 1110 */
        uint32_t l_2503 = 18446744073709551614UL;
        ++l_2503;
        return (*g_592);
    }
    else
    { /* block id: 1113 */
        int16_t l_2506 = 1L;
        int32_t l_2517[1][2];
        struct S0 **l_2522[9] = {&g_972,&g_972,&g_972,&g_972,&g_972,&g_972,&g_972,&g_972,&g_972};
        int16_t l_2532 = 0x9342L;
        uint64_t *l_2610[10][7] = {{(void*)0,(void*)0,&g_862,(void*)0,(void*)0,&g_862,(void*)0},{&g_8[3][3],&g_35,&g_35,&g_862,&g_35,&g_35,&g_8[3][3]},{&g_862,(void*)0,&g_862,&g_862,(void*)0,&g_862,&g_862},{&g_8[3][3],&g_862,(void*)0,&g_862,&g_8[3][3],(void*)0,&g_8[3][3]},{(void*)0,&g_862,&g_862,(void*)0,&g_862,&g_862,(void*)0},{&g_35,&g_862,&g_35,&g_35,&g_8[3][3],&g_35,&g_35},{(void*)0,(void*)0,&g_862,(void*)0,(void*)0,&g_862,(void*)0},{&g_8[3][3],&g_35,&g_35,&g_862,&g_35,&g_35,&g_8[3][3]},{&g_862,(void*)0,&g_862,&g_862,(void*)0,&g_862,&g_862},{&g_8[3][3],&g_862,(void*)0,&g_862,&g_8[3][3],(void*)0,&g_8[3][3]}};
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_2517[i][j] = 0xF4A51DBEL;
        }
        if (l_2506)
        { /* block id: 1114 */
            float l_2510 = (-0x10.Ap-1);
            struct S0 ***l_2531[9] = {&l_2522[4],&l_2522[4],&l_2522[4],&l_2522[4],&l_2522[4],&l_2522[4],&l_2522[4],&l_2522[4],&l_2522[4]};
            int32_t l_2537 = 0xBF453DDAL;
            int32_t *l_2564 = &g_159;
            uint16_t **l_2572 = &g_1889;
            const int16_t l_2590 = (-1L);
            int16_t l_2593 = 0x0F1BL;
            int i;
            if ((((*g_501) , ((safe_lshift_func_uint16_t_u_s((l_2509 , ((l_2509 || l_2511) ^ (safe_sub_func_uint16_t_u_u(((*g_20) >= (safe_lshift_func_uint16_t_u_u((***g_1887), (~(l_2517[0][0] ^= (**g_1888)))))), (((safe_lshift_func_int8_t_s_u((**g_786), 0)) > (***g_1887)) & 4294967295UL))))), 1)) && l_2517[0][1])) , l_2511))
            { /* block id: 1116 */
                struct S0 **l_2521 = &g_972;
                struct S0 ***l_2520[10] = {&l_2521,&l_2521,&l_2521,&l_2521,&l_2521,&l_2521,&l_2521,&l_2521,&l_2521,&l_2521};
                uint64_t ***l_2530 = &g_2026;
                uint16_t **l_2571 = &g_1889;
                int32_t l_2581 = 0x9493EADCL;
                int32_t l_2591[9] = {0x92EE31D6L,1L,0x92EE31D6L,0x92EE31D6L,1L,0x92EE31D6L,0x92EE31D6L,1L,0x92EE31D6L};
                uint16_t *l_2592 = &g_673;
                int32_t *l_2594 = &l_2[5][2];
                int i;
                if (((l_2522[4] = &g_972) == &g_972))
                { /* block id: 1118 */
                    if (g_35)
                        goto lbl_2523;
                }
                else
                { /* block id: 1120 */
                    int8_t l_2562 = 0xC1L;
                    (*g_1103) ^= (((safe_div_func_uint32_t_u_u((((safe_lshift_func_uint16_t_u_s((((*g_20) = 0xF5L) | ((l_2[1][1] = ((safe_div_func_uint32_t_u_u(((l_24 && (l_2530 == (*g_2301))) >= (((l_2531[7] == (((0x2099EBF9289730A7LL == ((l_2532 == 6L) ^ (safe_mod_func_uint8_t_u_u(((+1L) > l_2[5][5]), 0x5DL)))) ^ (***g_2025)) , &l_2522[4])) , g_2536[3][2]) , 7L)), 0xC0194270L)) >= 0xD3B1L)) && 0x5FD3A0BC11C586F4LL)), l_2511)) >= l_2517[0][0]) > (*g_1246)), l_2537)) | (-3L)) && 0xB65799C725F5692BLL);
                    (**g_1679) = (((*g_1246) = (*g_1246)) , (!2L));
                    for (g_821 = 0; (g_821 < 60); ++g_821)
                    { /* block id: 1128 */
                        const int32_t l_2545 = 0x954A6F35L;
                        int64_t l_2561[10] = {1L,0xD2C60FB40517B6AFLL,1L,1L,0xD2C60FB40517B6AFLL,1L,1L,0xD2C60FB40517B6AFLL,1L,1L};
                        int32_t **l_2563 = &g_1103;
                        int i;
                        (*l_2563) = &l_2517[0][0];
                        return l_2537;
                    }
                    l_2564 = (void*)0;
                }
                (*l_2594) |= (((*g_2216) || ((safe_div_func_int32_t_s_s(((**g_1679) = ((safe_mod_func_int32_t_s_s((0xFA5E242F35A271B2LL <= (safe_div_func_uint8_t_u_u(((((*l_2592) = ((l_2571 != l_2572) || (safe_div_func_int8_t_s_s((+((*l_12) = (l_2506 & (((safe_div_func_uint32_t_u_u((++(*g_2216)), (~l_2581))) , (l_2532 >= (((((safe_add_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u((safe_div_func_uint16_t_u_u(((**l_2572) = (((++(*g_1246)) & (l_2590 >= l_2581)) || 0x5B3CL)), g_550)), (*g_787))) == 1UL) , (*g_20)) > g_131), l_2591[1])) , l_2532) <= (*g_501)) < l_2517[0][0]) , 0x72L))) & l_2532)))), g_973.f0)))) && 0x3F8CL) == 1L), 3UL))), l_2593)) ^ (***g_2025))), l_2593)) , l_2517[0][0])) & 2UL);
                return (*g_3);
            }
            else
            { /* block id: 1144 */
                int32_t *l_2595[8] = {&g_38[1],&g_38[1],&g_38[1],&g_38[1],&g_38[1],&g_38[1],&g_38[1],&g_38[1]};
                int i;
                l_2564 = l_2595[0];
                (*l_2564) |= l_2532;
                for (g_973.f0 = 0; (g_973.f0 <= 22); g_973.f0++)
                { /* block id: 1149 */
                    int16_t l_2609[7];
                    uint32_t *l_2615 = (void*)0;
                    uint32_t *l_2616 = &g_297;
                    uint32_t *l_2617 = &g_167;
                    uint32_t *l_2618 = &g_2619;
                    uint32_t *l_2620[9];
                    int i;
                    for (i = 0; i < 7; i++)
                        l_2609[i] = (-1L);
                    for (i = 0; i < 9; i++)
                        l_2620[i] = &g_2621;
                    (*g_501) ^= (((safe_unary_minus_func_uint16_t_u((+((*g_2216)++)))) <= l_2[6][3]) >= (safe_add_func_uint32_t_u_u((safe_div_func_int8_t_s_s((l_2[2][5] , (safe_unary_minus_func_uint8_t_u((((safe_mul_func_int16_t_s_s((*l_2564), (l_2609[1] >= ((l_2517[0][0] = (((void*)0 == l_2610[3][1]) , (l_2609[1] , ((*l_2618) = ((*l_2617) = ((*l_2616) = (safe_add_func_int16_t_s_s((g_2613 != &g_2614), l_2609[6])))))))) == (*l_2564))))) != (*g_1246)) == (**g_1679))))), l_2622[5])), 0L)));
                }
                l_2564 = (void*)0;
            }
        }
        else
        { /* block id: 1159 */
            const uint64_t l_2623 = 1UL;
            (*g_501) = l_2623;
        }
    }
    return l_2624;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const uint16_t  func_15(int8_t * p_16, uint64_t * p_17, float * p_18, float * p_19)
{ /* block id: 1108 */
    const float l_2501 = (-0x1.2p-1);
    const uint32_t l_2502 = 0x74181134L;
    return l_2502;
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_4 g_20 g_21 g_34 g_11 g_1887 g_1888 g_1889 g_84 g_1791 g_592 g_1234 g_501 g_159 g_1679 g_1103 g_786 g_787 g_788 g_972 g_973 g_165 g_1246 g_139 g_1591 g_2025 g_1031 g_1032 g_1033 g_576 g_38 g_713 g_167 g_1016.f1 g_2026 g_35 g_1235 g_591 g_94 g_295 g_90.f5 g_193 g_550 g_254 g_133 g_2301
 * writes: g_38 g_21 g_84 g_94 g_1235 g_485.f0 g_91.f0 g_1591 g_139 g_167 g_35 g_576 g_295 g_193 g_1058 g_4 g_254 g_133 g_159
 */
static uint64_t * func_22(uint16_t  p_23)
{ /* block id: 6 */
    const int64_t l_36 = 0L;
    struct S0 * const *l_1988 = (void*)0;
    struct S0 * const **l_1987 = &l_1988;
    struct S0 **l_1989 = (void*)0;
    int16_t *l_2003 = &g_91.f0;
    float l_2004 = 0x7.5B862Ep-97;
    int16_t l_2005 = 0x622AL;
    const int64_t **l_2006 = (void*)0;
    int16_t *l_2007 = (void*)0;
    int16_t *l_2008 = &l_2005;
    int32_t *l_2009 = (void*)0;
    int32_t *l_2010 = &g_1591;
    int32_t l_2011 = 0x1DEB62FFL;
    int64_t * const l_2064[7] = {(void*)0,&g_329[7],(void*)0,(void*)0,&g_329[7],(void*)0,(void*)0};
    int64_t * const *l_2063 = &l_2064[4];
    int64_t * const **l_2062 = &l_2063;
    int64_t * const ***l_2061 = &l_2062;
    uint16_t ****l_2141 = &g_1887;
    int32_t l_2145 = (-6L);
    int32_t l_2155[2];
    uint8_t l_2254 = 0x44L;
    const float *l_2307 = &g_4;
    const float **l_2306 = &l_2307;
    const int8_t *****l_2323 = (void*)0;
    uint32_t l_2450 = 0x10BCF19FL;
    uint64_t l_2479[6][6] = {{0xA4C638299FA9EF81LL,0xA4C638299FA9EF81LL,0xF74F57B17B4D2C29LL,18446744073709551615UL,8UL,0xF74F57B17B4D2C29LL},{18446744073709551615UL,8UL,0xF74F57B17B4D2C29LL,8UL,18446744073709551615UL,0xF74F57B17B4D2C29LL},{8UL,18446744073709551615UL,0xF74F57B17B4D2C29LL,0xA4C638299FA9EF81LL,0xA4C638299FA9EF81LL,0xF74F57B17B4D2C29LL},{0xA4C638299FA9EF81LL,0xA4C638299FA9EF81LL,0xF74F57B17B4D2C29LL,18446744073709551615UL,8UL,0xF74F57B17B4D2C29LL},{18446744073709551615UL,8UL,0xF74F57B17B4D2C29LL,8UL,18446744073709551615UL,0xF74F57B17B4D2C29LL},{8UL,18446744073709551615UL,0xF74F57B17B4D2C29LL,0xA4C638299FA9EF81LL,0xA4C638299FA9EF81LL,0xF74F57B17B4D2C29LL}};
    uint64_t l_2484[6][9][1] = {{{18446744073709551615UL},{0xCBFA339BCA7AF0C8LL},{0xBD28F7D061DAE56BLL},{0xCBFA339BCA7AF0C8LL},{18446744073709551615UL},{0xBD28F7D061DAE56BLL},{0x505FFFC42E2EE0BDLL},{0x505FFFC42E2EE0BDLL},{0xBD28F7D061DAE56BLL}},{{18446744073709551615UL},{0xCBFA339BCA7AF0C8LL},{0xBD28F7D061DAE56BLL},{0xCBFA339BCA7AF0C8LL},{18446744073709551615UL},{0xBD28F7D061DAE56BLL},{0x505FFFC42E2EE0BDLL},{0x505FFFC42E2EE0BDLL},{0xBD28F7D061DAE56BLL}},{{18446744073709551615UL},{0xCBFA339BCA7AF0C8LL},{0xBD28F7D061DAE56BLL},{0xCBFA339BCA7AF0C8LL},{18446744073709551615UL},{0xBD28F7D061DAE56BLL},{0x505FFFC42E2EE0BDLL},{0x505FFFC42E2EE0BDLL},{0xBD28F7D061DAE56BLL}},{{18446744073709551615UL},{0xCBFA339BCA7AF0C8LL},{0xBD28F7D061DAE56BLL},{0xCBFA339BCA7AF0C8LL},{18446744073709551615UL},{0xBD28F7D061DAE56BLL},{0x505FFFC42E2EE0BDLL},{0x505FFFC42E2EE0BDLL},{0xBD28F7D061DAE56BLL}},{{18446744073709551615UL},{0xCBFA339BCA7AF0C8LL},{0xBD28F7D061DAE56BLL},{0xCBFA339BCA7AF0C8LL},{18446744073709551615UL},{0xBD28F7D061DAE56BLL},{0x505FFFC42E2EE0BDLL},{0x505FFFC42E2EE0BDLL},{0xBD28F7D061DAE56BLL}},{{18446744073709551615UL},{0xCBFA339BCA7AF0C8LL},{0xBD28F7D061DAE56BLL},{0xCBFA339BCA7AF0C8LL},{18446744073709551615UL},{0xBD28F7D061DAE56BLL},{0x505FFFC42E2EE0BDLL},{0x505FFFC42E2EE0BDLL},{0xBD28F7D061DAE56BLL}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2155[i] = 0xAB605F86L;
lbl_2056:
    (*g_1234) = (safe_sub_func_float_f_f((*g_3), (((*g_20) , (func_27(p_23, func_30(g_34[5][5], l_36, l_36)) , (safe_mul_func_uint8_t_u_u((((*l_1987) = &g_972) == l_1989), 1L)))) , 0x1.7p+1)));
    if ((1L <= ((safe_lshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((((((**g_1679) = (*g_501)) | (((*l_2010) ^= (safe_unary_minus_func_int64_t_s((((p_23 != (0UL || ((0x5FL ^ (((*l_2008) = ((!(**g_786)) >= ((~((safe_unary_minus_func_uint32_t_u((safe_add_func_int64_t_s_s(((((((((**l_1988) , (!((p_23 & ((*l_2003) = ((g_485.f0 = (safe_sub_func_uint8_t_u_u(((l_36 == l_36) && l_36), (-1L)))) >= 0x6EC8L))) , l_36))) < l_2005) , l_2006) == (void*)0) , 0x58L) < 0xBFL) || p_23), 0xA22387E2D3A4959BLL)))) <= p_23)) , g_165[0][1]))) ^ l_36)) , 0x59C8CCE08D5FBC9BLL))) > (*g_1246)) >= p_23)))) != p_23)) ^ l_2011) > 0xBF46A36FC1FC2BFBLL), (*g_1246))), 8)) == 0x385F84BEL)))
    { /* block id: 905 */
        uint8_t l_2019 = 250UL;
        uint64_t ****l_2027 = (void*)0;
        uint64_t ***l_2028 = (void*)0;
        (**g_1679) ^= ((((*g_1246) ^= ((((safe_add_func_uint32_t_u_u((((p_23 > 0UL) < (((~((*l_2010) = (safe_sub_func_int64_t_s_s((((safe_mod_func_uint16_t_u_u((*l_2010), l_2019)) || ((**g_1888) |= (safe_rshift_func_int8_t_s_u((!9UL), 4)))) || ((p_23 == (safe_add_func_uint64_t_u_u((0x49L <= (((l_2028 = g_2025) != &g_2026) & 0L)), p_23))) >= 0x31L)), 0x275F974E545250E6LL)))) <= l_2019) ^ p_23)) , g_165[4][0]), p_23)) > (***g_1031)) != l_2019) && p_23)) >= 9UL) <= l_2019);
    }
    else
    { /* block id: 911 */
        uint64_t l_2031 = 0x44D24C80C20E2A9BLL;
        int32_t l_2037 = 0x7CB8B11AL;
        uint32_t *l_2038[8];
        int16_t **l_2096 = (void*)0;
        uint64_t l_2133 = 0UL;
        int32_t **l_2138 = &g_1103;
        int32_t l_2154 = (-7L);
        int32_t l_2156 = 0x02BD5A0EL;
        float **l_2170 = &g_592;
        uint32_t l_2180 = 0x710A3896L;
        const uint64_t *l_2227 = (void*)0;
        const uint64_t **l_2226[4] = {&l_2227,&l_2227,&l_2227,&l_2227};
        const uint64_t *** const l_2225 = &l_2226[1];
        int32_t l_2270 = 0x02578CFDL;
        int32_t *l_2317 = &l_2037;
        const int8_t *l_2322 = (void*)0;
        const int8_t **l_2321 = &l_2322;
        const int8_t ***l_2320 = &l_2321;
        const int8_t ****l_2319 = &l_2320;
        const int8_t *****l_2318 = &l_2319;
        int32_t l_2332 = (-1L);
        int16_t l_2333 = 0xDC61L;
        int64_t **l_2420 = (void*)0;
        int32_t l_2482 = 0xBA4BDACAL;
        int32_t l_2483 = 7L;
        int i;
        for (i = 0; i < 8; i++)
            l_2038[i] = &g_297;
        if (((safe_lshift_func_uint8_t_u_u((l_2031 >= ((!(((safe_rshift_func_uint16_t_u_u((g_713[4] <= ((**g_1679) | (safe_lshift_func_uint8_t_u_s(((g_167++) >= ((0x0652L > ((safe_lshift_func_int8_t_s_s(((l_2037 = (safe_lshift_func_uint16_t_u_s((safe_sub_func_int32_t_s_s(((safe_lshift_func_uint8_t_u_s((*g_1246), 7)) != (*l_2010)), (((6L >= (((!((safe_add_func_int16_t_s_s((safe_sub_func_int32_t_s_s((((**g_1032) = (p_23 < ((**g_2026) ^= ((safe_rshift_func_uint16_t_u_u((g_1016.f1 , 0UL), 14)) == 0xBEL)))) & 0x5429C787D0BFBA05LL), p_23)), (*l_2010))) == l_2037)) ^ (*l_2010)) | l_2037)) || 0x8BL) != 0xD08F7E7ABF92282ALL))), p_23))) && (**g_1888)), l_2031)) != (*l_2010))) == p_23)), (*g_787))))), 1)) == p_23) <= (-2L))) || 0xD7L)), 3)) > p_23))
        { /* block id: 916 */
            if (l_36)
                goto lbl_2056;
            if (g_1591)
                goto lbl_2056;
        }
        else
        { /* block id: 919 */
            float l_2065 = 0x2.6p+1;
            int64_t * const *** const l_2068[6][2] = {{(void*)0,(void*)0},{&l_2062,(void*)0},{(void*)0,(void*)0},{&l_2062,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
            int32_t l_2085 = 2L;
            int16_t **l_2095[8][1];
            int32_t l_2097 = 1L;
            int64_t l_2119 = 0x141019E944937572LL;
            int16_t l_2128[4];
            uint16_t l_2157 = 3UL;
            int64_t l_2179 = 0xD4A1C16F4E88DDA8LL;
            float *l_2181 = &g_1058;
            int i, j;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 1; j++)
                    l_2095[i][j] = &l_2008;
            }
            for (i = 0; i < 4; i++)
                l_2128[i] = (-8L);
            l_2037 &= ((safe_add_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u((((**g_1032) < ((void*)0 == l_2061)) == (*g_787)), p_23)), ((*l_2010) & ((void*)0 == l_2068[0][1])))) , (((!(!65535UL)) && p_23) | p_23));
            if (((safe_mul_func_float_f_f(((*g_1234) != (safe_div_func_float_f_f((safe_add_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((safe_div_func_float_f_f(l_2037, (((safe_div_func_int16_t_s_s((l_2085 |= (1UL ^ (l_2031 > 0UL))), (p_23 || 0x970981C378E22ACDLL))) , (((safe_div_func_float_f_f((l_2085 = (safe_div_func_float_f_f(p_23, ((safe_add_func_float_f_f(((!(((*g_592) = (((safe_mul_func_float_f_f((l_2095[6][0] == l_2096), (**g_591))) < (*g_592)) >= p_23)) >= l_2085)) <= p_23), 0x0.7p+1)) == l_2037)))), l_2097)) > (-0x10.5p-1)) > p_23)) >= 0x6.92D301p+87))), (-0x3.8p+1))) != (*l_2010)), 0xA.C0D325p-64)), l_2097)), (-0x1.Bp-1)))), l_2037)) , p_23))
            { /* block id: 924 */
                int64_t l_2111 = 0xEFAE8DC1CB78F8A0LL;
                int32_t l_2112 = (-1L);
                int8_t *l_2120 = (void*)0;
                int32_t **l_2121 = &l_2009;
                (*l_2121) = &l_2037;
            }
            else
            { /* block id: 929 */
                int8_t *l_2127 = &g_82;
                uint8_t **l_2134 = &g_1246;
                int32_t l_2135 = 0xA917CBE5L;
                for (g_295 = 13; (g_295 == (-18)); g_295 = safe_sub_func_int16_t_s_s(g_295, 2))
                { /* block id: 932 */
                    int16_t **l_2125 = &l_2008;
                    int16_t ***l_2126[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    (**g_1679) = (safe_unary_minus_func_int32_t_s((0UL ^ l_2037)));
                }
                (*l_2010) = (&l_2010 != (((((p_23 ^ l_2128[1]) >= (safe_lshift_func_uint8_t_u_s((safe_div_func_uint32_t_u_u(l_2133, ((((l_2134 = &g_1246) != &g_1246) != l_2135) , g_90.f5))), ((safe_rshift_func_uint16_t_u_s(((l_2119 <= 0x309899B6B5B6AD7FLL) & 1L), 2)) && 0x6221B109A9C9D856LL)))) & 18446744073709551612UL) <= p_23) , l_2138));
            }
            for (g_35 = 0; (g_35 != 32); ++g_35)
            { /* block id: 943 */
                uint16_t *****l_2142 = (void*)0;
                uint16_t ****l_2144 = &g_1887;
                uint16_t *****l_2143 = &l_2144;
                int32_t *l_2146 = &g_1591;
                int32_t *l_2147 = &g_159;
                int32_t *l_2148 = &l_2097;
                int32_t *l_2149 = &g_38[3];
                int32_t *l_2150 = &g_38[3];
                int32_t *l_2151 = (void*)0;
                int32_t *l_2152 = &g_1591;
                int32_t *l_2153[7][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2085,&g_123,&l_2085,&g_123,&l_2037},{(void*)0,&g_38[3],&g_38[3],(void*)0,&g_38[3]},{&l_2037,&g_123,(void*)0,&g_123,&l_2037},{&g_38[3],(void*)0,&g_38[3],&g_38[3],(void*)0},{&l_2037,&g_123,&l_2085,&g_123,&l_2085},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                uint64_t *l_2162 = &g_35;
                int i, j;
                (**l_2138) |= (l_2141 != ((*l_2143) = l_2141));
                l_2157++;
                for (g_193 = 0; (g_193 < 15); ++g_193)
                { /* block id: 949 */
                    (*l_2146) = 9L;
                }
                for (l_2031 = 0; (l_2031 <= 7); l_2031 += 1)
                { /* block id: 954 */
                    return l_2162;
                }
            }
            (*g_3) = ((l_2119 >= ((safe_add_func_float_f_f(((**g_591) == (safe_sub_func_float_f_f(l_2157, l_2128[1]))), ((((*l_2181) = (!(((safe_rshift_func_int8_t_s_u((((void*)0 != l_2170) > ((((safe_div_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u(((safe_mod_func_uint32_t_u_u(p_23, ((safe_add_func_uint32_t_u_u((&l_2155[0] != (void*)0), 4294967289UL)) & (**l_2138)))) > 0x32C965B7961ECF2ALL), 7)), 0x660508BEB2FE7AE1LL)) , p_23) > 1UL) || p_23)), l_2179)) , g_550) , l_2180))) > (**g_591)) > (-0x1.1p+1)))) < p_23)) > p_23);
        }
        for (g_254 = 0; (g_254 <= 7); g_254 += 1)
        { /* block id: 963 */
            uint64_t *** const l_2224 = &g_2026;
            int32_t l_2256[9] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
            uint16_t l_2298 = 0xDB86L;
            uint32_t l_2334[10][3][1] = {{{0x8B5D61A0L},{1UL},{0x8B5D61A0L}},{{1UL},{0x8B5D61A0L},{1UL}},{{0x8B5D61A0L},{1UL},{0x8B5D61A0L}},{{1UL},{0x8B5D61A0L},{1UL}},{{0x8B5D61A0L},{1UL},{0x8B5D61A0L}},{{1UL},{0x8B5D61A0L},{1UL}},{{0x8B5D61A0L},{1UL},{0x8B5D61A0L}},{{1UL},{0x8B5D61A0L},{1UL}},{{0x8B5D61A0L},{1UL},{0x8B5D61A0L}},{{1UL},{0x8B5D61A0L},{1UL}}};
            const int32_t ****l_2400 = (void*)0;
            int64_t *l_2419 = &g_713[4];
            int64_t **l_2418 = &l_2419;
            int32_t l_2481 = (-1L);
            int i, j, k;
        }
    }
    for (g_133 = (-16); (g_133 >= (-9)); ++g_133)
    { /* block id: 1097 */
        uint64_t *l_2495 = (void*)0;
        float ** const * const l_2497 = &g_591;
        float ** const * const *l_2496 = &l_2497;
        float ** const * const **l_2498 = (void*)0;
        float ** const * const **l_2499 = &l_2496;
        for (l_2005 = 0; (l_2005 <= 17); l_2005 = safe_add_func_int32_t_s_s(l_2005, 4))
        { /* block id: 1100 */
            return l_2495;
        }
        (*l_2499) = l_2496;
        (*g_501) &= ((*g_1103) = ((*l_2010) | (safe_unary_minus_func_uint32_t_u(p_23))));
    }
    return (***g_2301);
}


/* ------------------------------------------ */
/* 
 * reads : g_1887 g_1888 g_1889 g_84 g_20 g_21 g_1791 g_592
 * writes: g_84 g_94
 */
static int32_t  func_27(int8_t  p_28, int8_t * p_29)
{ /* block id: 890 */
    float l_1954 = 0x8.78DE14p+35;
    int32_t l_1955[5] = {0L,0L,0L,0L,0L};
    float l_1958 = 0x5.B04620p-66;
    uint32_t *l_1982 = (void*)0;
    uint32_t l_1983 = 0UL;
    int i;
    if (l_1955[3])
    { /* block id: 891 */
        return p_28;
    }
    else
    { /* block id: 893 */
        int64_t l_1984 = (-2L);
        (*g_592) = (safe_mul_func_float_f_f(l_1958, ((safe_mod_func_uint64_t_u_u(((((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_s((--(***g_1887)), 3)), ((~0xB6E0L) != l_1955[3]))) == (safe_rshift_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((safe_add_func_int64_t_s_s((((safe_rshift_func_uint8_t_u_u((((((safe_div_func_int32_t_s_s((((*g_20) & (safe_sub_func_int8_t_s_s(((safe_add_func_int64_t_s_s(((((1L != (l_1982 != (((((void*)0 == &l_1982) > l_1955[3]) , p_28) , l_1982))) ^ (-10L)) < 9L) != 0x4928C755L), l_1955[4])) , (*g_20)), p_28))) == 9L), p_28)) , 1UL) || p_28) != p_28) & l_1955[2]), p_28)) | g_1791) && 0x0CC2B5833F5A6FA6LL), 0xE6EAF3C45B14D819LL)), l_1983)) | 0x471EL), 13))) | l_1983) | p_28), 1UL)) , l_1984)));
    }
    return l_1983;
}


/* ------------------------------------------ */
/* 
 * reads : g_11 g_21
 * writes: g_38 g_21
 */
static int8_t * func_30(uint64_t * p_31, const uint16_t  p_32, uint32_t  p_33)
{ /* block id: 7 */
    int32_t *l_37 = &g_38[3];
    float l_48[3];
    int32_t l_1920 = 0x9CF26204L;
    int32_t l_1922 = 0x8B6A4915L;
    int32_t l_1923 = 1L;
    uint32_t l_1950 = 18446744073709551615UL;
    int8_t *l_1953 = (void*)0;
    int i;
    for (i = 0; i < 3; i++)
        l_48[i] = 0x0.6p-1;
lbl_39:
    (*l_37) = g_11[0][3][2];
    for (g_21 = 7; (g_21 >= 0); g_21 -= 1)
    { /* block id: 11 */
        uint64_t l_45 = 1UL;
        int16_t l_85 = 0x67CEL;
        float *l_190 = &g_94;
        int32_t l_1919 = 0xC5C642B7L;
        int32_t l_1924 = (-1L);
        if (g_21)
            goto lbl_39;
        if (p_33)
            break;
        for (p_33 = 0; (p_33 <= 7); p_33 += 1)
        { /* block id: 16 */
            int32_t l_49 = 0x8B939845L;
            float *l_55 = &l_48[2];
            int8_t *l_81 = &g_82;
            uint16_t *l_83 = &g_84[4];
            int32_t **l_1918 = &g_1103;
            int32_t *l_1921[6] = {&g_1591,(void*)0,(void*)0,&g_1591,(void*)0,(void*)0};
            uint16_t l_1925 = 0x4C7FL;
            int i, j;
        }
    }
    return l_1953;
}


/* ------------------------------------------ */
/* 
 * reads : g_138 g_84 g_20 g_21 g_131 g_139 g_112 g_38 g_121 g_788 g_592 g_576 g_11 g_862 g_90.f0 g_970.f5 g_485.f0 g_970.f3 g_91.f3 g_972 g_1031 g_787 g_254 g_1032 g_1016.f4 g_1016.f3 g_591 g_94 g_1103 g_195 g_481 g_193 g_970.f4 g_90.f5 g_295 g_327 g_326 g_167 g_856 g_1219 g_1222 g_1234 g_485.f4 g_133 g_600 g_675 g_297 g_90.f4 g_90.f2 g_90.f1 g_1314 g_973.f3 g_91.f4 g_470 g_601 g_1246 g_261 g_1679 g_260 g_673 g_550 g_34 g_1887 g_159 g_324 g_1016.f0 g_82
 * writes: g_138 g_139 g_715 g_713 g_193 g_485.f0 g_972 g_673 g_94 g_576 g_862 g_84 g_422 g_38 g_195 g_254 g_166 g_1032 g_601 g_970.f0 g_481 g_260 g_131 g_788 g_470 g_112 g_591 g_159 g_324 g_1246 g_297 g_1016.f0 g_1219 g_1058 g_1314 g_600 g_1103 g_261 g_82 g_327 g_675 g_550 g_35 g_1887
 */
static int32_t * func_40(int64_t  p_41, uint64_t  p_42, int16_t  p_43, int8_t * p_44)
{ /* block id: 400 */
    int32_t *l_927[4];
    uint8_t *l_936[1];
    int64_t *l_937 = &g_713[4];
    int64_t *l_942 = &g_193;
    int32_t l_943[5] = {0x5CECDB4AL,0x5CECDB4AL,0x5CECDB4AL,0x5CECDB4AL,0x5CECDB4AL};
    int8_t *** const l_946[7][2] = {{&g_587,&g_587},{&g_587,&g_587},{&g_587,&g_587},{&g_587,&g_587},{&g_587,&g_587},{&g_587,&g_587},{&g_587,&g_587}};
    int8_t l_956 = 5L;
    int64_t l_961 = 1L;
    uint8_t l_964 = 255UL;
    uint64_t l_974 = 18446744073709551615UL;
    uint64_t l_1020[6] = {4UL,4UL,4UL,4UL,4UL,4UL};
    struct S0 **l_1023 = &g_972;
    int64_t l_1040 = 0x4A981DC48DE40E88LL;
    int8_t l_1059 = 1L;
    int16_t l_1087 = 1L;
    int32_t *l_1101 = &g_260;
    int32_t *l_1202 = &g_38[2];
    uint16_t l_1225 = 65535UL;
    const int8_t l_1226 = 0x5AL;
    int16_t l_1280 = 0x0B3CL;
    uint32_t l_1302 = 0x170C0367L;
    uint16_t l_1307 = 0x5F1BL;
    uint32_t l_1503 = 0x245FB63BL;
    int16_t l_1567[7][10][3] = {{{0xEDFEL,0x00F1L,0L},{0x7972L,0x6827L,0x7972L},{0L,0x00F1L,0xEDFEL},{4L,(-1L),0x4F60L},{0x80B0L,8L,0x57FCL},{1L,0L,(-8L)},{0x80B0L,0L,8L},{4L,0xA1B3L,0xD94BL},{0L,(-6L),(-6L)},{0x7972L,0xAC6EL,0xD94BL}},{{0xEDFEL,0L,8L},{0x4F60L,1L,(-8L)},{0x57FCL,0x13F3L,0x57FCL},{(-8L),1L,0x4F60L},{8L,0L,0xEDFEL},{0xD94BL,0xAC6EL,0x7972L},{(-6L),(-6L),0L},{0xD94BL,0xA1B3L,4L},{8L,0L,0x80B0L},{(-8L),0L,1L}},{{0x57FCL,8L,0x80B0L},{0x4F60L,(-1L),4L},{0xEDFEL,0x00F1L,0L},{0x7972L,0x6827L,0x7972L},{0L,0x00F1L,0xEDFEL},{4L,(-1L),0x4F60L},{0x80B0L,8L,0x57FCL},{1L,0L,(-8L)},{0x80B0L,0L,8L},{4L,0xA1B3L,0xD94BL}},{{0L,(-6L),(-6L)},{0x7972L,0xAC6EL,0xD94BL},{0xEDFEL,0L,8L},{0x4F60L,1L,(-8L)},{0x57FCL,0x13F3L,0x57FCL},{(-8L),1L,0x4F60L},{8L,0L,0xEDFEL},{0xD94BL,0xAC6EL,0x7972L},{(-6L),(-6L),0L},{0xD94BL,0xA1B3L,4L}},{{8L,0L,0xEDFEL},{1L,0x6156L,(-5L)},{0x00F1L,0x13F3L,0xEDFEL},{0x7972L,0x2EFEL,0x9DA6L},{0x57FCL,0L,(-6L)},{0xA7FEL,(-8L),0xA7FEL},{(-6L),0L,0x57FCL},{0x9DA6L,0x2EFEL,0x7972L},{0xEDFEL,0x13F3L,0x00F1L},{(-5L),0x6156L,1L}},{{0xEDFEL,(-6L),0x13F3L},{0x9DA6L,0L,0x4F60L},{(-6L),0x4069L,0x4069L},{0xA7FEL,0x6827L,0x4F60L},{0x57FCL,0x80B0L,0x13F3L},{0x7972L,0xA1B3L,1L},{0x00F1L,0x4391L,0x00F1L},{1L,0xA1B3L,0x7972L},{0x13F3L,0x80B0L,0x57FCL},{0x4F60L,0x6827L,0xA7FEL}},{{0x4069L,0x4069L,(-6L)},{0x4F60L,0L,0x9DA6L},{0x13F3L,(-6L),0xEDFEL},{1L,0x6156L,(-5L)},{0x00F1L,0x13F3L,0xEDFEL},{0x7972L,0x2EFEL,0x9DA6L},{0x57FCL,0L,(-6L)},{0xA7FEL,(-8L),0xA7FEL},{(-6L),0L,0x57FCL},{0x9DA6L,0x2EFEL,0x7972L}}};
    uint32_t l_1750 = 2UL;
    uint32_t * const *l_1762 = (void*)0;
    int8_t l_1785 = (-8L);
    const uint16_t *l_1799 = &g_675[3][0][0];
    const uint16_t **l_1798 = &l_1799;
    int32_t l_1821 = 0xBB5597A8L;
    float ***l_1830 = &g_591;
    uint64_t **l_1849 = &g_34[1][8];
    uint16_t ***l_1891 = &g_1888;
    int32_t ***l_1909 = &g_481;
    int32_t l_1912 = 0x461148B7L;
    int16_t l_1914 = 2L;
    uint8_t l_1915[2];
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_927[i] = &g_38[3];
    for (i = 0; i < 1; i++)
        l_936[i] = &g_775;
    for (i = 0; i < 2; i++)
        l_1915[i] = 250UL;
lbl_979:
    for (g_138 = 1; (g_138 >= 0); g_138 -= 1)
    { /* block id: 403 */
        int i;
        if (g_84[g_138])
            break;
        return l_927[0];
    }
    if ((((*l_942) = (safe_sub_func_int32_t_s_s((249UL && (*g_20)), (safe_lshift_func_int8_t_s_s((safe_sub_func_int32_t_s_s((p_42 >= (safe_lshift_func_int16_t_s_s((0L == 0x90F39C94L), (g_131 , (((*l_937) = ((g_715[3][1][0] = (g_139 &= 0xBEL)) < g_112)) <= ((safe_rshift_func_int8_t_s_u(((safe_div_func_int64_t_s_s(0x041D82EA844D096DLL, p_43)) ^ p_42), 3)) || 65535UL)))))), g_38[0])), 1))))) == l_943[4]))
    { /* block id: 411 */
        int32_t l_950 = 4L;
        int32_t l_954 = 0x035DCA87L;
        int32_t l_957[2];
        int32_t l_963 = 0xBE7C956BL;
        const struct S0 *l_969 = &g_970[0][1][0];
        int32_t *l_999 = (void*)0;
        uint64_t l_1011 = 0UL;
        const uint64_t l_1012[4][4][7] = {{{0x9801DE94F0956089LL,0x438BC3B9795C0F1FLL,0xBBD288C66868F0EBLL,0x58678E31D7A13FD9LL,0x20CF8FB59D32F529LL,6UL,6UL},{0xBDC294F2762EA5E7LL,0x27B242D0801C171ELL,18446744073709551612UL,0x27B242D0801C171ELL,0xBDC294F2762EA5E7LL,0x77794DE64F45FA20LL,0x991F9F3E9C0FB525LL},{0x9D56DAA12AC16811LL,6UL,0x58678E31D7A13FD9LL,0xEA7FF9F0B485DF0ALL,0xF3EB8A1E6D019FE6LL,18446744073709551615UL,0x9D56DAA12AC16811LL},{0x7C1E4A947DEC0202LL,18446744073709551607UL,0UL,0UL,0x991F9F3E9C0FB525LL,0xA44726740B872139LL,0x49E65BE844890628LL}},{{0x9D56DAA12AC16811LL,0xEA7FF9F0B485DF0ALL,18446744073709551607UL,0x20CF8FB59D32F529LL,0x84F7F11AAB65C042LL,1UL,0x84F7F11AAB65C042LL},{0xBDC294F2762EA5E7LL,0x49E65BE844890628LL,0x49E65BE844890628LL,0xBDC294F2762EA5E7LL,0xA44726740B872139LL,0x7C1E4A947DEC0202LL,1UL},{0x9801DE94F0956089LL,0xB2AC8C37C164C2A7LL,0xE9F457D103F02EC5LL,0xF3EB8A1E6D019FE6LL,0xB2AC8C37C164C2A7LL,0xEC416EF6839DE429LL,18446744073709551615UL},{0x27B242D0801C171ELL,0x4090EFC4560466FELL,18446744073709551612UL,0x991F9F3E9C0FB525LL,0x41ABE55167DAFB19LL,18446744073709551612UL,1UL}},{{0x1F0C4C9BF7139CD7LL,18446744073709551615UL,0x9801DE94F0956089LL,0x84F7F11AAB65C042LL,0xF3EB8A1E6D019FE6LL,0xF3EB8A1E6D019FE6LL,0x84F7F11AAB65C042LL},{0x9DFD787691089AC3LL,0xBDC294F2762EA5E7LL,0x9DFD787691089AC3LL,0xA44726740B872139LL,0x4090EFC4560466FELL,0xF9CA702A5556B088LL,0x49E65BE844890628LL},{1UL,0x1F0C4C9BF7139CD7LL,1UL,0xB2AC8C37C164C2A7LL,0x438BC3B9795C0F1FLL,18446744073709551607UL,0x9D56DAA12AC16811LL},{0x991F9F3E9C0FB525LL,0x49E65BE844890628LL,18446744073709551612UL,0x41ABE55167DAFB19LL,0UL,0xF9CA702A5556B088LL,0x991F9F3E9C0FB525LL}},{{0xBBD288C66868F0EBLL,0UL,6UL,0xF3EB8A1E6D019FE6LL,0x9D56DAA12AC16811LL,0xF3EB8A1E6D019FE6LL,6UL},{0x41ABE55167DAFB19LL,0x41ABE55167DAFB19LL,0x77794DE64F45FA20LL,0x4090EFC4560466FELL,18446744073709551615UL,18446744073709551612UL,8UL},{0x438BC3B9795C0F1FLL,6UL,18446744073709551615UL,0x438BC3B9795C0F1FLL,0x58678E31D7A13FD9LL,0x48A796EC20845742LL,6UL},{0x3EE909AA7F836FB0LL,0xA44726740B872139LL,0x698E2A120750FB93LL,0UL,0xA44726740B872139LL,1UL,18446744073709551612UL}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_957[i] = 2L;
        for (g_485.f0 = 0; (g_485.f0 <= 6); g_485.f0 = safe_add_func_int8_t_s_s(g_485.f0, 3))
        { /* block id: 414 */
            int8_t ***l_948 = &g_587;
            int8_t ****l_947[2][10];
            int8_t ****l_949 = &l_948;
            int32_t l_951 = (-1L);
            int32_t l_952 = 0x2F4E3941L;
            int32_t l_953 = (-6L);
            int32_t l_955 = 0x0387BC12L;
            int32_t l_958 = (-10L);
            int32_t l_959 = 2L;
            int32_t l_960 = 0xF4B94D5FL;
            int32_t l_962[3];
            struct S0 **l_971 = (void*)0;
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 10; j++)
                    l_947[i][j] = &l_948;
            }
            for (i = 0; i < 3; i++)
                l_962[i] = 0x4497B21DL;
            (*l_949) = l_946[1][0];
            l_964--;
            l_963 = (l_951 < ((((safe_div_func_uint64_t_u_u((g_121 || ((*g_20) > ((l_969 != (g_972 = (void*)0)) == (p_42 > ((-1L) > g_788[5]))))), l_974)) == 0xDF307E60E05A06DELL) > 0UL) == 0xF575F096A6A3CCFELL));
        }
        for (g_673 = 0; (g_673 <= 15); g_673 = safe_add_func_uint32_t_u_u(g_673, 9))
        { /* block id: 422 */
            uint8_t l_978 = 1UL;
            int32_t *l_981 = (void*)0;
            int32_t l_1019[5][8] = {{0L,0x6B768A2DL,0xAE161F2BL,0xBE01829DL,0xA8533A74L,0L,(-4L),0L},{0x6B768A2DL,0xFF1290CBL,0xD7845310L,0xFF1290CBL,0x6B768A2DL,1L,0x0CFCBE22L,1L},{0xD7845310L,0x1D8CFF59L,1L,0xA8533A74L,(-1L),0xBE01829DL,7L,0xFF1290CBL},{7L,0x0CFCBE22L,1L,1L,1L,1L,0x0CFCBE22L,7L},{(-1L),0L,0xD7845310L,1L,0xAE161F2BL,0x1D8CFF59L,(-4L),1L}};
            struct S0 ***l_1024 = (void*)0;
            struct S0 ***l_1025[5];
            int i, j;
            for (i = 0; i < 5; i++)
                l_1025[i] = &l_1023;
            (*g_592) = p_43;
            for (p_41 = 4; (p_41 >= 0); p_41 -= 1)
            { /* block id: 426 */
                uint64_t *l_977 = &g_862;
                int32_t l_1009 = 1L;
                uint32_t l_1010 = 0UL;
                int32_t l_1013 = 7L;
                struct S0 *l_1015 = &g_1016;
                int32_t l_1017 = 0x4696B42DL;
                int32_t l_1018[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
                int i;
                for (g_576 = 3; (g_576 >= 0); g_576 -= 1)
                { /* block id: 429 */
                    int i;
                    l_978 = ((0x0.Fp+1 < ((void*)0 == l_977)) , (g_84[p_41] >= p_43));
                    for (l_961 = 4; (l_961 >= 0); l_961 -= 1)
                    { /* block id: 433 */
                        int32_t *l_980 = &l_950;
                        if (g_576)
                            goto lbl_979;
                        return l_981;
                    }
                    if (p_42)
                    { /* block id: 437 */
                        return l_981;
                    }
                    else
                    { /* block id: 439 */
                        int32_t *l_998 = &g_133;
                        int32_t **l_997 = &l_998;
                        struct S0 **l_1014 = (void*)0;
                        int i;
                        l_1015 = (((((safe_mul_func_int16_t_s_s((-3L), (l_954 <= (safe_sub_func_uint16_t_u_u((p_43 != ((+(safe_mul_func_uint8_t_u_u(g_11[0][2][1], ((safe_div_func_int32_t_s_s((((*l_977)++) || (safe_rshift_func_uint16_t_u_u(((((safe_mul_func_float_f_f(((((*l_997) = l_981) == l_999) < p_42), ((safe_div_func_float_f_f(((+(((((safe_lshift_func_uint16_t_u_s((g_84[(g_576 + 1)] ^= ((safe_div_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u((p_42 <= p_42), g_90.f0)) <= l_1009) , l_1009) , (*p_44)), 0x9BL)) && g_970[0][1][0].f5)), l_1010)) , p_42) | g_485.f0) >= g_970[0][1][0].f3) == 5UL)) , l_1010), l_1011)) != 0xC.DAB139p-7))) , &p_42) == (void*)0) ^ 7L), l_1009))), 0xACE652B8L)) < l_1012[0][0][3])))) <= 0x13C5L)), g_91.f3))))) > l_1013) & 0UL) > 0UL) , g_972);
                    }
                }
                --l_1020[1];
            }
            if (p_41)
                continue;
            g_422 = l_1023;
        }
    }
    else
    { /* block id: 451 */
        int32_t **l_1026 = &l_927[2];
        int8_t **l_1052 = (void*)0;
        int16_t *l_1053[9][1][6] = {{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}},{{&g_1016.f0,&g_485.f0,&g_600,&g_485.f0,&g_1016.f0,&g_600}}};
        uint64_t *l_1054[10] = {(void*)0,(void*)0,&l_974,&g_35,&l_974,(void*)0,(void*)0,&l_974,&g_35,&l_974};
        int32_t l_1055 = 1L;
        int32_t l_1056 = (-9L);
        int32_t l_1057 = (-7L);
        int32_t *l_1099 = &g_324;
        int32_t l_1113 = 0x295D98A6L;
        uint32_t l_1115 = 18446744073709551606UL;
        uint64_t l_1135[5][5];
        int16_t l_1162 = 0xC07AL;
        const uint32_t l_1293 = 0xE4A694EFL;
        int64_t *l_1310 = &g_576;
        int16_t l_1348[2];
        int8_t *****l_1376 = (void*)0;
        uint8_t l_1418[5] = {255UL,255UL,255UL,255UL,255UL};
        uint64_t l_1481 = 0xF9322D9E0C970ADFLL;
        int32_t l_1556 = (-8L);
        int32_t l_1557 = 0xF693B9A6L;
        int32_t l_1559[10] = {0x1DC37A34L,0x1DC37A34L,8L,0xCE4E6B15L,8L,0x1DC37A34L,0x1DC37A34L,8L,0xCE4E6B15L,8L};
        int8_t l_1565 = (-3L);
        int32_t l_1566[9][1] = {{0x3207B936L},{(-1L)},{0x3207B936L},{(-1L)},{0x3207B936L},{(-1L)},{0x3207B936L},{(-1L)},{0x3207B936L}};
        uint32_t l_1674 = 0UL;
        uint16_t l_1758[1];
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 5; j++)
                l_1135[i][j] = 0xCCF83960A84DF471LL;
        }
        for (i = 0; i < 2; i++)
            l_1348[i] = 0xFAFCL;
        for (i = 0; i < 1; i++)
            l_1758[i] = 0x5063L;
        (*l_1026) = l_927[0];
        if ((p_41 , (((safe_mod_func_uint64_t_u_u((((safe_add_func_uint64_t_u_u(((g_1031 != (void*)0) == (((**l_1026) &= 0xFB0F679CL) , (g_254 &= ((safe_add_func_uint32_t_u_u((safe_div_func_uint8_t_u_u(((l_1040 != ((((l_1056 = (p_42 > (l_1055 = (0x8BL | (g_195 = ((safe_add_func_uint32_t_u_u(((p_43 = ((safe_add_func_uint64_t_u_u((!(((**l_1026) = (safe_lshift_func_int16_t_s_u(((safe_rshift_func_uint8_t_u_s(((safe_sub_func_float_f_f((((l_1052 = l_1052) == &p_44) == 0x5.3p-1), (**l_1026))) , p_42), (**l_1026))) > 0x72A5CC308477DEDALL), 1))) != p_43)), 0x3695005AB3914A52LL)) & (*g_787))) , p_41), 1L)) ^ 0xA1L)))))) || p_41) , (**l_1026)) == l_1057)) <= 0L), p_41)), 1UL)) > g_131)))), 0x3FCDCEEEBB1CEE09LL)) >= p_41) < l_1059), p_42)) >= 0x42L) , p_41)))
        { /* block id: 461 */
            uint8_t l_1079 = 5UL;
            float l_1083 = 0x0.7p-1;
            int64_t **l_1086 = &l_937;
            int32_t *l_1102 = (void*)0;
            int32_t l_1105 = 0L;
            int32_t l_1107 = 0x6530CE33L;
            int32_t l_1108 = 0x293AF12EL;
            int32_t l_1109 = (-5L);
            int32_t l_1110 = (-1L);
            int32_t l_1111 = 0x4ADF3829L;
            int32_t l_1114 = 0x30F609F8L;
            int8_t ***l_1139 = (void*)0;
            int8_t ****l_1138 = &l_1139;
            int32_t ***l_1140 = &g_481;
            int8_t l_1155[4][4] = {{7L,0L,7L,0x49L},{7L,0x49L,0x49L,7L},{1L,0x49L,(-1L),0x49L},{0x49L,0L,(-1L),(-1L)}};
            uint64_t *l_1179[8] = {&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35,&g_35};
            uint16_t *l_1192 = &g_675[1][0][1];
            const uint32_t l_1256 = 18446744073709551615UL;
            struct S0 ***l_1257[8][7] = {{&l_1023,&l_1023,&l_1023,&l_1023,&l_1023,&l_1023,&l_1023},{&l_1023,&l_1023,&l_1023,&l_1023,&l_1023,(void*)0,&l_1023},{(void*)0,&l_1023,&l_1023,(void*)0,(void*)0,&l_1023,&l_1023},{&l_1023,&l_1023,&l_1023,&l_1023,(void*)0,&l_1023,&l_1023},{&l_1023,&l_1023,(void*)0,(void*)0,&l_1023,&l_1023,(void*)0},{&l_1023,&l_1023,&l_1023,&l_1023,&l_1023,(void*)0,&l_1023},{&l_1023,&l_1023,&l_1023,&l_1023,(void*)0,(void*)0,(void*)0},{&l_1023,(void*)0,&l_1023,&l_1023,&l_1023,&l_1023,(void*)0}};
            int32_t *l_1306 = &l_1114;
            int32_t **l_1341 = &l_1101;
            int i, j;
            for (g_673 = 0; (g_673 != 47); g_673++)
            { /* block id: 464 */
                uint32_t l_1082 = 0xFF897880L;
                int32_t l_1104 = (-1L);
                int32_t l_1106 = 0x27B19D19L;
                int32_t l_1112[7][3][3] = {{{1L,0L,0xBF492A5FL},{0L,(-1L),0x63C63F1BL},{0L,0xE7E93D62L,(-2L)}},{{1L,1L,0xB05857C6L},{(-2L),0x0C7CB4A6L,1L},{4L,0x38CEA9B6L,6L}},{{0xE7E93D62L,6L,(-1L)},{0xB05857C6L,4L,6L},{0x97DB1D7EL,1L,1L}},{{1L,0x5307FE83L,0xB05857C6L},{(-2L),1L,(-2L)},{1L,1L,0x63C63F1BL}},{{0x0C7CB4A6L,1L,0xBF492A5FL},{0L,1L,0L},{0x8DA9DB28L,0x5307FE83L,1L}},{{0x44038F0DL,1L,(-1L)},{0L,4L,1L},{1L,6L,2L}},{{0L,0x38CEA9B6L,(-2L)},{0x44038F0DL,0x0C7CB4A6L,1L},{0x8DA9DB28L,1L,0L}}};
                uint32_t *l_1133 = &g_601;
                int32_t ***l_1134 = &l_1026;
                int i, j, k;
                if ((safe_sub_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(((safe_sub_func_int64_t_s_s(((safe_unary_minus_func_uint8_t_u(((safe_mod_func_uint32_t_u_u((((*l_937) = ((((((safe_rshift_func_int16_t_s_s((safe_sub_func_int64_t_s_s(((*l_942) = 0x163DBAE7B888A8F0LL), p_42)), 11)) | (&l_937 == (*g_1031))) <= (p_42 > 0L)) >= ((((safe_div_func_int32_t_s_s(((((safe_sub_func_uint8_t_u_u((p_42 | (((((l_1079--) == (**l_1026)) && 0x0CL) != p_43) && l_1082)), (**l_1026))) <= (**l_1026)) != 0xD4CCL) & p_42), p_43)) != p_41) | (**l_1026)) <= g_1016.f4)) > l_1082) != 0xE4AC786800C546DFLL)) || p_41), (**l_1026))) , 0UL))) , p_43), p_41)) != g_1016.f3), p_42)), 0L)))
                { /* block id: 468 */
                    for (g_166 = (-14); (g_166 >= 18); g_166++)
                    { /* block id: 471 */
                        (*g_1031) = l_1086;
                    }
                    if (l_1087)
                        break;
                }
                else
                { /* block id: 475 */
                    int32_t **l_1100 = (void*)0;
                    (*g_592) = ((safe_add_func_float_f_f(((-0x1.Dp-1) == ((**l_1026) != (-(safe_add_func_float_f_f(0xE.E0463Ep+1, (safe_div_func_float_f_f((safe_add_func_float_f_f((safe_mul_func_float_f_f((**g_591), (((p_41 != (((l_1101 = l_1099) != (void*)0) , ((void*)0 == &g_788[5]))) , (*g_592)) > (**l_1026)))), p_42)), (**g_591)))))))), 0x3.Fp+1)) < p_42);
                    (*l_1026) = l_1102;
                    return g_1103;
                }
                ++l_1115;
                (*g_592) = (safe_div_func_float_f_f((-0x1.4p-1), ((safe_lshift_func_uint8_t_u_s((safe_div_func_int64_t_s_s(((void*)0 != &g_121), (safe_sub_func_uint32_t_u_u((((void*)0 == &g_481) > ((((safe_sub_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((g_970[0][1][0].f0 = (!((*l_1133) = ((++g_195) , p_42)))), ((((0x0CL ^ ((l_1134 = &g_481) == &g_481)) && 0xA72A0418L) <= p_42) , 0xB1E2L))), (*g_1103))) | 0xCEDD129BL) , p_41) > (**l_1026))), 0L)))), (*p_44))) , p_42)));
                (*l_1026) = &l_1111;
            }
            l_1135[3][1]--;
            if ((p_42 >= (g_970[0][1][0].f0 = ((((((*l_1138) = &g_587) != &g_587) , (&l_1102 != ((*l_1140) = &g_1103))) , g_90.f0) , (!((**l_1026) | p_41))))))
            { /* block id: 493 */
                int32_t l_1153 = 1L;
                if (p_43)
                { /* block id: 494 */
                    uint32_t l_1142 = 4UL;
                    int32_t l_1161[10][9] = {{0xE83A1840L,(-6L),(-1L),(-6L),0xE83A1840L,1L,0L,0L,0L},{0L,1L,0xBC3E7AA8L,0xF17DAC57L,1L,(-10L),0xA8446BFAL,(-10L),1L},{0L,9L,9L,0L,0xC649E76CL,1L,(-1L),0xF20B834AL,(-1L)},{1L,0xE4E81C81L,0xAB77C308L,0xBC3E7AA8L,0x686214FCL,3L,3L,0x686214FCL,0xBC3E7AA8L},{(-1L),1L,(-1L),0xA03665ACL,0xC649E76CL,0xF20B834AL,0L,(-5L),1L},{3L,0L,0x686214FCL,(-7L),1L,1L,1L,(-7L),0x686214FCL},{1L,1L,1L,0xA03665ACL,0xE83A1840L,(-1L),(-1L),(-1L),9L},{0xA8446BFAL,0xF17DAC57L,1L,0xBC3E7AA8L,0x1B98484CL,0x1B98484CL,0xBC3E7AA8L,1L,0xF17DAC57L},{0xA67C7805L,0L,1L,0L,0L,0xA03665ACL,1L,(-1L),0xC649E76CL},{(-7L),0xA8446BFAL,0x686214FCL,0xF17DAC57L,1L,0xF17DAC57L,0x686214FCL,0xA8446BFAL,(-7L)}};
                    int32_t l_1163 = 0x1B8DA89BL;
                    int i, j;
                    for (g_260 = 4; (g_260 >= 0); g_260 -= 1)
                    { /* block id: 497 */
                        (***l_1140) ^= l_1142;
                    }
                    l_1163 &= (safe_sub_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((((safe_lshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u((safe_add_func_int16_t_s_s(((1UL ^ 1L) & (l_1153 , ((0L != (!0xE7AEF3532B0C5943LL)) != (l_1155[3][0] == p_43)))), (!(((safe_mod_func_int16_t_s_s((1UL == ((((safe_sub_func_uint8_t_u_u((((*l_942) = ((((&p_43 != &l_1087) <= (*p_44)) & l_1142) != (***l_1140))) > p_43), 0xFCL)) & 0xD052L) && g_193) && l_1161[6][2])), p_42)) == 0xE83CL) && (*g_1103))))), 0)), (*g_20))) , 0xF3L) || g_970[0][1][0].f3), l_1162)) != (-1L)), 252UL));
                    for (g_131 = 0; (g_131 != (-12)); g_131 = safe_sub_func_int16_t_s_s(g_131, 6))
                    { /* block id: 504 */
                        float *l_1180 = &l_1083;
                        int16_t l_1181 = 0L;
                        (***l_1140) = ((&g_1103 != (*l_1140)) > (safe_unary_minus_func_uint32_t_u(g_970[0][1][0].f4)));
                        (*g_1103) = (safe_mod_func_uint8_t_u_u(p_42, 0x83L));
                        (***l_1140) = (((safe_rshift_func_uint16_t_u_s((safe_mul_func_int16_t_s_s((g_112 ^= ((safe_mod_func_int32_t_s_s((safe_add_func_uint8_t_u_u(0xE8L, (g_470 = (0xB61BAAFE0B10C742LL && (p_41 == ((safe_rshift_func_int16_t_s_s((((void*)0 != l_1179[4]) , (((((**l_1026) && (0xC6L || ((*g_787) &= (g_121 & ((void*)0 != l_1180))))) && g_90.f5) ^ 0x60E7L) <= l_1181)), 3)) < g_295)))))), p_42)) & p_43)), l_1153)), 14)) & p_42) >= l_1161[5][2]);
                    }
                }
                else
                { /* block id: 512 */
                    int32_t l_1199 = 0x677034D5L;
                    float *l_1200[5] = {&g_94,&g_94,&g_94,&g_94,&g_94};
                    float l_1201 = (-0x1.1p-1);
                    int i;
                    (**g_591) = (safe_div_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f(p_41, p_43)), (((((((g_139 &= g_327) > ((safe_sub_func_uint8_t_u_u(((void*)0 != l_1192), ((p_41 , (((safe_lshift_func_int8_t_s_s(((0xDAA1EAACL && (((((safe_sub_func_uint8_t_u_u(((safe_add_func_int32_t_s_s((((p_44 == (void*)0) > 65529UL) , 0xD46E11C6L), l_1199)) <= (*p_44)), g_326[2])) , l_1200[1]) == (*g_591)) , (*g_787)) <= (*p_44))) ^ l_1153), (*p_44))) >= 0x3AL) & 1UL)) < p_43))) < 1UL)) | 0xAFB478B5L) && 0UL) >= 9L) , (**g_591)) > p_42))), p_41));
                }
            }
            else
            { /* block id: 516 */
                float ***l_1220 = &g_591;
                int8_t ****l_1221 = &l_1139;
                float **l_1224 = (void*)0;
                int32_t l_1227 = 0x3C426BDEL;
                (*l_1026) = l_1202;
                l_1227 = (((((safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((((safe_sub_func_int32_t_s_s((((**l_1086) = (0xE2L == g_167)) , (((1L != (*p_44)) < g_856) != (!(safe_div_func_uint32_t_u_u((!((safe_add_func_uint32_t_u_u(((safe_sub_func_float_f_f(((g_1219 > (((*l_1220) = &g_592) != (((**l_1026) = (((l_1221 == g_1222) == g_576) , 18446744073709551615UL)) , l_1224))) != p_41), 0xC.31314Ap+89)) , p_43), p_42)) <= 0x1DDFL)), l_1225))))), p_42)) != g_84[4]) || p_43), 0xAF8955E6800ECA08LL)), l_1226)) ^ p_43) , (*l_1202)) <= p_41) , (-1L));
                for (g_159 = 5; (g_159 >= 0); g_159 -= 1)
                { /* block id: 524 */
                    (**l_1026) = 0L;
                    for (g_324 = 5; (g_324 >= 0); g_324 -= 1)
                    { /* block id: 528 */
                        (*g_1103) &= (((*l_1026) = (*g_481)) != (void*)0);
                    }
                }
                return (*g_481);
            }
            if ((safe_sub_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u((((safe_add_func_uint8_t_u_u((((void*)0 == g_1234) , 0UL), (*p_44))) && ((p_43 && (((safe_lshift_func_int8_t_s_u((*g_787), 7)) || ((g_485.f0 = p_43) < g_485.f4)) ^ p_41)) && g_133)) | 0x2662L), p_43)), g_600)))
            { /* block id: 536 */
                (*l_1026) = (*l_1026);
            }
            else
            { /* block id: 538 */
                uint8_t **l_1244 = (void*)0;
                uint8_t **l_1245[10][2][4] = {{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}},{{&l_936[0],(void*)0,&l_936[0],(void*)0},{&l_936[0],(void*)0,&l_936[0],(void*)0}}};
                uint32_t *l_1255 = &g_297;
                uint32_t *l_1258[5][3][9] = {{{&g_114,&g_821,&g_821,(void*)0,&l_1115,&g_821,&g_856,&l_1115,(void*)0},{&g_821,&l_1115,&l_1115,&g_856,&g_114,&g_821,&g_821,&g_114,&g_856},{&g_261,(void*)0,&g_261,(void*)0,&l_1115,&g_856,&g_821,&l_1115,(void*)0}},{{&g_821,&g_261,&g_821,&g_821,(void*)0,(void*)0,&g_261,(void*)0,&g_821},{&l_1115,&g_261,&g_821,(void*)0,&l_1115,&g_856,&g_261,&g_261,&g_261},{&l_1115,&l_1115,&l_1115,&g_856,&l_1115,&l_1115,&l_1115,&g_114,&g_821}},{{(void*)0,(void*)0,&l_1115,(void*)0,(void*)0,&l_1115,&g_856,&l_1115,&g_261},{(void*)0,&g_114,&l_1115,&g_821,&l_1115,(void*)0,&g_821,&g_114,&g_821},{&l_1115,&l_1115,&g_114,&g_261,&l_1115,&g_114,&l_1115,&g_261,&g_114}},{{(void*)0,(void*)0,&g_821,&g_821,&g_856,&g_821,(void*)0,(void*)0,(void*)0},{&g_856,&g_821,&l_1115,&g_261,&l_1115,&l_1115,&g_261,&l_1115,(void*)0},{&l_1115,&l_1115,&g_821,&g_821,&g_114,&g_856,(void*)0,&g_114,(void*)0}},{{&g_261,&l_1115,&g_114,&g_114,&g_114,&g_856,&l_1115,&g_114,&g_261},{&g_821,&g_856,&g_261,&g_114,&g_821,&l_1115,(void*)0,&g_114,&g_821},{&l_1115,&g_856,&l_1115,&g_261,&g_261,(void*)0,&l_1115,&g_821,&l_1115}}};
                int32_t l_1259 = 0L;
                int32_t l_1260 = 0x3B851A0FL;
                int32_t l_1261 = 0L;
                uint16_t l_1290[9][8][3] = {{{0x8AD2L,0x48ECL,65533UL},{0x3E74L,0x4912L,0xE37BL},{0UL,0x278EL,65530UL},{0x8D45L,65528UL,0xD01AL},{0x8439L,1UL,0x8D45L},{1UL,65527UL,0xBF57L},{65535UL,0xBF57L,0x9569L},{0xE986L,65535UL,0xFB0DL}},{{0x00F1L,0xD1C0L,65534UL},{65527UL,0xFB0DL,65535UL},{0x9D95L,1UL,65535UL},{0xF8D4L,0x20A1L,65534UL},{0x2BC5L,1UL,0xFB0DL},{0x517EL,65533UL,0x9569L},{0xD01AL,65535UL,0xBF57L},{65534UL,0x00F1L,0x8D45L}},{{0xC1A0L,0x11E9L,0xD01AL},{0x2623L,0x344CL,65530UL},{65526UL,0xE37BL,0xE37BL},{0x344CL,4UL,65533UL},{1UL,0UL,0xC1A0L},{0UL,0xCC42L,0x4912L},{0x0749L,65530UL,1UL},{65526UL,0xCC42L,65535UL}},{{0UL,0UL,65533UL},{0x2513L,4UL,0UL},{0x4912L,0xE37BL,65527UL},{0x5B8EL,0x344CL,0x3E74L},{0x48ECL,0x11E9L,0x574FL},{65533UL,0x00F1L,1UL},{0x2A07L,65535UL,0x2BC5L},{0xE37BL,65533UL,0UL}},{{0x8B45L,1UL,0x19E1L},{65535UL,0x20A1L,0x8439L},{0UL,1UL,0xE30AL},{0UL,0xFB0DL,0x8B45L},{65535UL,0xD1C0L,0UL},{0x8B45L,65535UL,65535UL},{0xE37BL,0xBF57L,0x8AD2L},{0x2A07L,65527UL,0x2947L}},{{65533UL,1UL,1UL},{0x48ECL,65528UL,0x5B8EL},{0x5B8EL,0x278EL,1UL},{0x4912L,0x4912L,65532UL},{0x2513L,0x48ECL,0xB25CL},{0UL,0x2623L,0x11E9L},{65526UL,0UL,0x278EL},{0x0749L,0UL,0x11E9L}},{{0UL,65534UL,0xB25CL},{1UL,0x2BC5L,65532UL},{0x344CL,0xC1A0L,1UL},{65526UL,0UL,0x5B8EL},{0x2623L,0x2A07L,1UL},{0xC1A0L,0x19E1L,0x2947L},{65534UL,65529UL,0x8AD2L},{0xD01AL,0x574FL,65535UL}},{{0UL,0x344CL,0x8AD2L},{0x11E9L,0x2513L,0x8D45L},{4UL,65534UL,65527UL},{65526UL,65534UL,0x18FFL},{65533UL,0x2513L,0x00F1L},{65535UL,0x344CL,65535UL},{0x574FL,0x3E74L,0x11E9L},{0xD1C0L,0xB25CL,65528UL}},{{0x278EL,0x00F1L,0x3E74L},{0x18FFL,1UL,1UL},{0x2947L,65529UL,65533UL},{65529UL,0x2623L,65529UL},{1UL,0x11E9L,0xF8D4L},{65526UL,0xB519L,65535UL},{0x5B8EL,65535UL,65535UL},{0xD01AL,0x9569L,0UL}}};
                int32_t ***l_1313 = &l_1026;
                int i, j, k;
                if ((safe_add_func_int8_t_s_s((l_1261 ^= (((*p_44) , ((*g_20) | ((safe_mod_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_s((((((l_1259 = (((((*g_1103) = ((*g_787) == ((((*l_937) = ((g_1246 = p_44) != &g_715[0][0][1])) || (((((0x23C876A5L && g_675[1][0][1]) ^ ((*l_1255) |= ((p_43 = ((safe_lshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s((safe_add_func_int16_t_s_s((g_970[0][1][0].f0 = 0xD015L), (safe_add_func_uint16_t_u_u(((***l_1140) != 1L), 0x89DDL)))), p_41)), (*g_20))) || (**l_1026))) > (**l_1026)))) > l_1256) == p_42) , 0xBBF9150019DC937CLL)) >= (***l_1140)))) , &g_422) == l_1257[4][5]) , p_41)) , (void*)0) == &g_121) , (**l_1026)) , g_90.f4), 13)), l_1260)) && p_42))) >= p_42)), (*p_44))))
                { /* block id: 547 */
lbl_1264:
                    for (g_1016.f0 = 25; (g_1016.f0 <= 25); g_1016.f0 = safe_add_func_int32_t_s_s(g_1016.f0, 7))
                    { /* block id: 550 */
                        if (p_43)
                            break;
                        if (l_1087)
                            goto lbl_1264;
                    }
                    return (*g_481);
                }
                else
                { /* block id: 555 */
                    const float l_1289[9][7] = {{0x8.Ap+1,0xF.3725EDp-63,0x8.Ap+1,0x9.D94AA3p-11,(-0x4.7p+1),(-0x4.7p+1),0x9.D94AA3p-11},{0x3.16ED8Fp-92,0xC.95C30Dp+43,0x3.16ED8Fp-92,0x8.C1898Cp+75,(-0x10.4p+1),(-0x10.4p+1),0x8.C1898Cp+75},{0x8.Ap+1,0xF.3725EDp-63,0x8.Ap+1,0x9.D94AA3p-11,(-0x4.7p+1),(-0x4.7p+1),0x9.D94AA3p-11},{0x3.16ED8Fp-92,0xC.95C30Dp+43,0x3.16ED8Fp-92,0x8.C1898Cp+75,(-0x10.4p+1),(-0x10.4p+1),0x8.C1898Cp+75},{0x8.Ap+1,0xF.3725EDp-63,0x8.Ap+1,0x9.D94AA3p-11,(-0x4.7p+1),(-0x4.7p+1),0x9.D94AA3p-11},{0x3.16ED8Fp-92,0xC.95C30Dp+43,0x3.16ED8Fp-92,0x8.C1898Cp+75,(-0x10.4p+1),(-0x10.4p+1),0x8.C1898Cp+75},{0x8.Ap+1,0xF.3725EDp-63,0x8.Ap+1,0x9.D94AA3p-11,(-0x4.7p+1),(-0x4.7p+1),0x9.D94AA3p-11},{0x3.16ED8Fp-92,0xC.95C30Dp+43,0x3.16ED8Fp-92,0x8.C1898Cp+75,(-0x10.4p+1),(-0x10.4p+1),0x8.C1898Cp+75},{0x8.Ap+1,0xF.3725EDp-63,0x8.Ap+1,0x9.D94AA3p-11,(-0x4.7p+1),(-0x4.7p+1),0x9.D94AA3p-11}};
                    int32_t l_1303 = 0xED8F6F92L;
                    float * const ***l_1315 = &g_1314;
                    uint8_t * const *l_1334[10][7][3] = {{{&g_1246,&g_1246,&l_936[0]},{&g_1246,&g_1246,&l_936[0]},{&l_936[0],&g_1246,&l_936[0]},{(void*)0,(void*)0,&g_1246},{&g_1246,&g_1246,&l_936[0]},{&l_936[0],&g_1246,&g_1246},{&g_1246,(void*)0,&g_1246}},{{&g_1246,&l_936[0],&l_936[0]},{(void*)0,&g_1246,&l_936[0]},{&l_936[0],&l_936[0],&l_936[0]},{&l_936[0],&g_1246,&l_936[0]},{&g_1246,&g_1246,&l_936[0]},{&l_936[0],&l_936[0],&g_1246},{&l_936[0],&g_1246,&l_936[0]}},{{&g_1246,&l_936[0],&g_1246},{&l_936[0],(void*)0,&l_936[0]},{(void*)0,&g_1246,(void*)0},{&l_936[0],&g_1246,&l_936[0]},{&g_1246,(void*)0,&l_936[0]},{&l_936[0],&g_1246,&l_936[0]},{&l_936[0],&g_1246,&l_936[0]}},{{&l_936[0],&g_1246,&g_1246},{&g_1246,&l_936[0],&l_936[0]},{&l_936[0],&g_1246,&l_936[0]},{(void*)0,&g_1246,&l_936[0]},{&l_936[0],&l_936[0],&l_936[0]},{&g_1246,&l_936[0],&g_1246},{&g_1246,(void*)0,(void*)0}},{{&l_936[0],&l_936[0],(void*)0},{&l_936[0],&l_936[0],(void*)0},{&g_1246,&l_936[0],(void*)0},{&l_936[0],&l_936[0],&g_1246},{&l_936[0],&l_936[0],&g_1246},{(void*)0,&l_936[0],&g_1246},{&g_1246,&l_936[0],&l_936[0]}},{{&l_936[0],&l_936[0],&l_936[0]},{&g_1246,(void*)0,&l_936[0]},{&l_936[0],&g_1246,&g_1246},{&g_1246,(void*)0,&l_936[0]},{&g_1246,&g_1246,(void*)0},{&l_936[0],(void*)0,&g_1246},{&l_936[0],&l_936[0],&g_1246}},{{&l_936[0],&l_936[0],&g_1246},{&l_936[0],&l_936[0],&l_936[0]},{&l_936[0],&l_936[0],&g_1246},{&l_936[0],&l_936[0],&l_936[0]},{&g_1246,&l_936[0],&l_936[0]},{&l_936[0],&l_936[0],&g_1246},{&l_936[0],&l_936[0],&l_936[0]}},{{&g_1246,(void*)0,&l_936[0]},{&l_936[0],&l_936[0],&g_1246},{&l_936[0],(void*)0,&g_1246},{&l_936[0],&l_936[0],(void*)0},{&l_936[0],&l_936[0],&l_936[0]},{&l_936[0],&l_936[0],&l_936[0]},{&l_936[0],&g_1246,&l_936[0]}},{{&g_1246,&l_936[0],&l_936[0]},{&g_1246,&l_936[0],&l_936[0]},{&l_936[0],&l_936[0],&l_936[0]},{&g_1246,&g_1246,&l_936[0]},{&l_936[0],(void*)0,(void*)0},{&g_1246,&l_936[0],&g_1246},{(void*)0,&l_936[0],&g_1246}},{{&l_936[0],(void*)0,&l_936[0]},{&l_936[0],&l_936[0],&l_936[0]},{&g_1246,&g_1246,&g_1246},{&l_936[0],&g_1246,&l_936[0]},{&l_936[0],&l_936[0],&l_936[0]},{&g_1246,(void*)0,&g_1246},{&g_1246,&l_936[0],&l_936[0]}}};
                    int32_t l_1343[7] = {0x77CD0014L,0L,0L,0x77CD0014L,0L,0L,0x77CD0014L};
                    int16_t l_1347[4];
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_1347[i] = 0L;
                    if (p_41)
                    { /* block id: 556 */
                        uint8_t l_1265 = 0xA1L;
                        int32_t l_1273 = 0x6F4429C3L;
                        int32_t l_1274 = 0x7146FE1EL;
                        (**g_481) = (l_1265 >= (safe_rshift_func_int16_t_s_s((**l_1026), (safe_add_func_uint32_t_u_u(0xAE04FCADL, (safe_lshift_func_int16_t_s_u(((((!((((g_139--) < (*p_44)) , l_1274) != ((p_41 , (0x16L > (*p_44))) ^ (safe_lshift_func_uint16_t_u_s((~((*g_787) = (((0L || p_43) && p_43) || l_1280))), p_43))))) > g_90.f2) , 1UL) & (**l_1026)), 11)))))));
                    }
                    else
                    { /* block id: 560 */
                        uint64_t l_1304 = 18446744073709551615UL;
                        float *l_1305 = &g_1058;
                        if (g_485.f4)
                            goto lbl_1264;
                        (*l_1305) = (g_90.f1 , ((*g_592) = ((p_42 = (safe_mul_func_int16_t_s_s((l_1261 &= (safe_div_func_int16_t_s_s((((void*)0 != &g_188) >= (((((*l_1202) || (safe_sub_func_int8_t_s_s(((0xA554L == l_1290[7][2][0]) | (safe_sub_func_int16_t_s_s(l_1293, (3L ^ (((*l_1255) = (safe_lshift_func_uint8_t_u_u((g_1219 = (safe_rshift_func_int8_t_s_s(((safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u(l_1302, 0x95L)), l_1303)) & (***l_1140)), 0))), 2))) != 8UL))))), 0x4BL))) < p_43) , 0x7AF8929F40609423LL) != l_1304)), 0x2A48L))), p_41))) , (-0x1.Dp+1))));
                        l_1306 = &l_1303;
                        l_1307++;
                    }
                    (*l_1306) |= (l_1259 >= (-1L));
                    (*l_1306) = (p_42 , ((**l_1026) &= ((l_1310 = l_1054[0]) != ((*l_1086) = (void*)0))));
                    if ((safe_sub_func_int32_t_s_s(((g_600 = (((void*)0 == l_1313) <= (((*l_1315) = g_1314) != &g_591))) ^ ((**l_1026) == (safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(((safe_unary_minus_func_int32_t_s(((**l_1026) >= (((safe_sub_func_float_f_f(0x8.C32E06p+13, ((p_42 < (***l_1140)) <= p_41))) < (***l_1313)) , 0xF79EDBA4E2CBB563LL)))) , (**l_1026)), 2)), g_973.f3)))), g_91.f4)))
                    { /* block id: 578 */
                        int32_t **l_1339[4][7][9] = {{{&l_1101,(void*)0,&l_1101,(void*)0,&l_1101,&l_1101,&l_1099,&l_1099,&l_1101},{&l_1101,&l_1099,(void*)0,&l_1099,&l_1101,&l_1099,(void*)0,&l_1101,&l_1099},{(void*)0,(void*)0,&l_1101,&l_1099,&l_1101,(void*)0,&l_1101,(void*)0,&l_1101},{&l_1099,&l_1099,&l_1099,&l_1099,&l_1099,&l_1099,&l_1101,(void*)0,&l_1101},{&l_1101,&l_1101,&l_1099,(void*)0,(void*)0,&l_1101,&l_1099,(void*)0,&l_1099},{&l_1099,&l_1101,&l_1101,&l_1099,(void*)0,&l_1099,&l_1099,&l_1099,&l_1099},{(void*)0,(void*)0,&l_1099,&l_1099,&l_1099,&l_1099,&l_1099,(void*)0,&l_1099}},{{&l_1101,&l_1101,&l_1101,(void*)0,&l_1101,&l_1101,&l_1101,&l_1101,&l_1099},{&l_1101,(void*)0,&l_1101,(void*)0,&l_1099,&l_1101,(void*)0,(void*)0,&l_1101},{(void*)0,&l_1101,&l_1099,&l_1099,&l_1099,(void*)0,&l_1101,&l_1101,&l_1101},{&l_1099,&l_1099,&l_1099,&l_1099,&l_1101,(void*)0,&l_1101,&l_1101,&l_1101},{&l_1099,&l_1101,&l_1099,&l_1101,(void*)0,(void*)0,&l_1101,&l_1099,&l_1101},{&l_1099,&l_1101,(void*)0,&l_1101,&l_1101,&l_1099,(void*)0,&l_1101,(void*)0},{&l_1101,&l_1101,&l_1099,(void*)0,&l_1099,(void*)0,&l_1099,&l_1099,&l_1101}},{{&l_1099,&l_1101,&l_1101,(void*)0,&l_1099,(void*)0,&l_1101,&l_1099,&l_1099},{(void*)0,&l_1101,(void*)0,&l_1101,&l_1101,(void*)0,&l_1101,&l_1099,&l_1101},{(void*)0,&l_1099,&l_1101,&l_1099,(void*)0,&l_1101,&l_1101,(void*)0,&l_1099},{&l_1099,&l_1101,&l_1101,&l_1099,&l_1101,&l_1099,&l_1099,&l_1101,&l_1101},{(void*)0,(void*)0,&l_1101,(void*)0,(void*)0,&l_1101,&l_1099,&l_1099,&l_1101},{&l_1101,&l_1101,&l_1099,&l_1101,&l_1101,&l_1099,(void*)0,(void*)0,&l_1099},{&l_1101,(void*)0,&l_1099,&l_1099,&l_1101,&l_1101,(void*)0,&l_1099,&l_1101}},{{&l_1099,&l_1101,&l_1101,&l_1099,&l_1101,(void*)0,&l_1099,&l_1101,&l_1099},{&l_1099,(void*)0,&l_1101,&l_1101,(void*)0,&l_1099,&l_1101,&l_1101,&l_1101},{(void*)0,(void*)0,&l_1101,&l_1101,&l_1099,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1101,&l_1099,&l_1099,&l_1099,&l_1099,&l_1101,(void*)0,&l_1101},{&l_1101,(void*)0,&l_1099,(void*)0,&l_1101,&l_1099,&l_1099,&l_1101,&l_1101},{(void*)0,(void*)0,&l_1101,&l_1101,&l_1101,&l_1101,(void*)0,&l_1101,&l_1101},{&l_1099,&l_1101,&l_1099,&l_1101,(void*)0,&l_1099,&l_1101,&l_1099,&l_1101}}};
                        int32_t ***l_1340[9] = {&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1],&l_1339[1][2][1]};
                        int32_t **l_1342 = &l_1101;
                        int32_t *l_1349 = &l_1057;
                        int i, j, k;
                        l_1348[0] &= ((safe_lshift_func_uint8_t_u_u((((((g_673 = (p_42 != (g_470 == g_601))) & ((safe_sub_func_uint32_t_u_u(((safe_sub_func_uint64_t_u_u((safe_mod_func_uint16_t_u_u(((safe_mod_func_uint32_t_u_u(g_675[1][0][1], ((((!((void*)0 != l_1334[0][0][0])) < ((safe_rshift_func_int8_t_s_s((safe_mul_func_int8_t_s_s((l_1343[5] = ((l_1341 = l_1339[1][2][1]) == l_1342)), ((safe_lshift_func_int8_t_s_s((~(((0L && 18446744073709551615UL) == (*g_20)) < (*g_1246))), 0)) , (*g_1246)))), (*g_787))) != 1UL)) <= (**l_1026)) , g_326[3]))) > (**g_481)), l_1347[0])), 0xF523AD7CA0EDB633LL)) , p_43), 0x5535E09EL)) , 0UL)) | (**l_1026)) > p_41) , (*g_1246)), 6)) , (**l_1026));
                        return l_1101;
                    }
                    else
                    { /* block id: 584 */
                        int32_t l_1350 = 0x97EB7449L;
                        l_1350 = p_42;
                    }
                }
                l_1260 |= (*g_1103);
                (*g_481) = ((*l_1026) = (*g_481));
                for (l_1259 = 26; (l_1259 >= 12); l_1259--)
                { /* block id: 593 */
                    (*l_1026) = (**l_1313);
                    for (l_1114 = (-19); (l_1114 != (-5)); l_1114 = safe_add_func_uint32_t_u_u(l_1114, 7))
                    { /* block id: 597 */
                        (***l_1313) |= 0x2867DAEDL;
                    }
                }
            }
        }
        else
        { /* block id: 602 */
            int16_t l_1367 = 0x76B4L;
            int32_t *l_1370 = &l_1056;
            int8_t ***l_1375[6][1][8] = {{{&l_1052,&l_1052,&g_587,(void*)0,&l_1052,(void*)0,&l_1052,(void*)0}},{{(void*)0,&g_587,(void*)0,&g_587,(void*)0,(void*)0,&l_1052,(void*)0}},{{&g_587,&l_1052,&l_1052,(void*)0,(void*)0,&l_1052,&l_1052,&g_587}},{{&g_587,(void*)0,&l_1052,&l_1052,&l_1052,(void*)0,&l_1052,&l_1052}},{{(void*)0,(void*)0,(void*)0,(void*)0,&l_1052,(void*)0,&l_1052,&l_1052}},{{&l_1052,(void*)0,&g_587,&g_587,(void*)0,&l_1052,&l_1052,&l_1052}}};
            int8_t ****l_1374 = &l_1375[1][0][2];
            int8_t *****l_1373 = &l_1374;
            uint64_t ** const l_1394 = &g_34[5][5];
            int64_t **l_1413[10][2][7] = {{{&l_1310,&l_942,&l_937,(void*)0,&l_1310,(void*)0,&l_937},{&l_942,&l_942,&l_1310,(void*)0,&l_1310,&l_937,(void*)0}},{{&l_942,&l_1310,&l_937,(void*)0,&l_942,(void*)0,(void*)0},{&l_1310,&l_942,&l_937,(void*)0,&l_1310,(void*)0,&l_937}},{{&l_942,&l_942,&l_1310,(void*)0,&l_1310,&l_937,(void*)0},{&l_942,&l_1310,&l_937,(void*)0,&l_942,(void*)0,(void*)0}},{{&l_1310,&l_942,&l_937,(void*)0,&l_1310,(void*)0,&l_937},{&l_942,&l_942,&l_1310,(void*)0,&l_1310,&l_937,(void*)0}},{{&l_942,&l_1310,&l_937,(void*)0,&l_942,(void*)0,(void*)0},{&l_1310,&l_942,&l_937,(void*)0,&l_1310,(void*)0,&l_937}},{{&l_942,&l_942,&l_1310,(void*)0,&l_1310,&l_937,(void*)0},{&l_942,&l_1310,&l_937,(void*)0,&l_942,(void*)0,(void*)0}},{{&l_1310,&l_942,&l_937,(void*)0,&l_1310,(void*)0,&l_937},{&l_942,&l_942,&l_1310,(void*)0,&l_1310,&l_937,(void*)0}},{{&l_942,&l_1310,&l_937,(void*)0,&l_942,(void*)0,(void*)0},{&l_1310,&l_942,&l_937,(void*)0,&l_1310,(void*)0,&l_937}},{{&l_942,&l_942,&l_1310,(void*)0,&l_1310,&l_937,(void*)0},{&l_942,&l_1310,&l_937,(void*)0,&l_942,(void*)0,(void*)0}},{{&l_1310,&l_942,&l_1310,(void*)0,&l_1310,(void*)0,&l_1310},{&l_942,&l_942,&l_942,&l_942,&l_1310,(void*)0,&l_1310}}};
            int64_t ***l_1412 = &l_1413[1][1][0];
            int64_t ****l_1411[1];
            int32_t l_1417[2];
            uint16_t l_1480 = 0xB9CBL;
            const int32_t l_1502 = 1L;
            int32_t l_1555 = 0x4C745B34L;
            int64_t l_1563 = 9L;
            uint32_t l_1568 = 0x5189A1DFL;
            uint32_t l_1605 = 1UL;
            int32_t *l_1617 = &g_38[6];
            const int32_t *l_1652[1][5][3] = {{{(void*)0,&l_1056,&l_1056},{&l_1559[2],&l_1559[8],&l_1559[8]},{(void*)0,&l_1056,&l_1056},{&l_1559[2],&l_1559[8],&l_1559[8]},{(void*)0,&l_1056,&l_1056}}};
            const int32_t **l_1651 = &l_1652[0][1][0];
            uint8_t **l_1683 = &g_1246;
            const uint16_t l_1685 = 0x5180L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1411[i] = &l_1412;
            for (i = 0; i < 2; i++)
                l_1417[i] = 0xAD6FC98BL;
            for (g_195 = 0; (g_195 <= 0); g_195 += 1)
            { /* block id: 605 */
                uint32_t l_1391 = 1UL;
                int64_t * volatile ***l_1393 = &g_1031;
                uint32_t *l_1397 = (void*)0;
                uint32_t *l_1398 = (void*)0;
                uint32_t *l_1399 = &g_601;
                int32_t l_1402[1][8][10] = {{{0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L,(-7L)},{0L,0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L},{0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L,(-7L)},{0L,0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L},{0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L,(-7L)},{0L,0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L},{0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L,(-7L)},{0L,0L,(-7L),(-7L),0L,0L,(-7L),(-7L),0L,0L}}};
                int i, j, k;
                for (g_193 = 3; (g_193 >= 0); g_193 -= 1)
                { /* block id: 608 */
                    const uint32_t *l_1356 = &g_297;
                    const uint32_t **l_1355 = &l_1356;
                    int32_t l_1364 = (-4L);
                }
                (*l_1370) &= ((l_1394 != (((safe_div_func_int16_t_s_s((g_112 = ((*g_20) < ((0x40L >= ((((*g_787) = (*p_44)) && (((*l_1399)++) && ((void*)0 == l_1101))) > l_1391)) >= p_42))), p_43)) > (**l_1026)) , &g_34[0][7])) , p_42);
                for (g_576 = 0; (g_576 <= 0); g_576 += 1)
                { /* block id: 637 */
                    return l_1398;
                }
                for (g_261 = 1; (g_261 <= 4); g_261 += 1)
                { /* block id: 642 */
                    uint8_t l_1403 = 0xB8L;
                    int32_t l_1463 = 0xE456B885L;
                    int i, j;
                    l_1403--;
                    for (g_112 = 0; (g_112 <= 4); g_112 += 1)
                    { /* block id: 646 */
                        return &g_38[3];
                    }
                    if ((safe_rshift_func_uint16_t_u_s(l_1135[(g_195 + 2)][g_261], 6)))
                    { /* block id: 649 */
                        int i, j;
                        if (g_295)
                            goto lbl_979;
                        if (l_1135[(g_195 + 3)][(g_195 + 4)])
                            break;
                    }
                    else
                    { /* block id: 652 */
                        int64_t ****l_1410 = (void*)0;
                        int32_t l_1416 = 0x96FA41B3L;
                        (*l_1202) &= (safe_add_func_int8_t_s_s((((l_1411[0] = l_1410) != &l_1412) > (p_41 = ((safe_rshift_func_uint16_t_u_s(p_42, 12)) < (l_1416 = p_43)))), p_43));
                        --l_1418[0];
                    }
                }
            }
            l_1481++;
            for (p_43 = 1; (p_43 >= 0); p_43 -= 1)
            { /* block id: 677 */
                int64_t ** const *l_1492 = &l_1413[7][0][4];
                uint8_t *l_1495 = &l_1418[0];
                const int32_t l_1504 = (-1L);
                uint64_t l_1525 = 18446744073709551606UL;
                int32_t l_1551 = 0x6BD95BFBL;
                int32_t l_1552[6][4] = {{9L,9L,9L,9L},{9L,9L,9L,9L},{9L,9L,9L,9L},{9L,9L,9L,9L},{9L,9L,9L,9L},{9L,9L,9L,9L}};
                int8_t l_1564 = 0x58L;
                uint16_t *l_1579 = (void*)0;
                uint16_t *l_1580[10][6][4] = {{{&l_1307,&g_675[4][0][2],(void*)0,(void*)0},{&g_675[3][0][1],&l_1225,&g_675[1][0][1],&l_1225},{&l_1480,(void*)0,&g_675[2][0][0],&g_675[0][0][0]},{&g_675[1][0][1],&g_673,&g_675[0][0][0],&g_673},{&l_1480,&g_675[0][0][2],&g_675[1][0][1],(void*)0},{&g_84[4],&g_673,(void*)0,&g_675[1][0][1]}},{{&g_84[4],(void*)0,&g_84[4],&g_84[3]},{&g_673,&l_1307,&l_1480,&l_1307},{&g_675[2][0][0],&g_673,&g_673,&g_675[2][0][0]},{&l_1225,(void*)0,(void*)0,&g_84[4]},{&g_675[1][0][1],&g_673,(void*)0,(void*)0},{&g_84[4],&l_1480,&l_1307,(void*)0}},{{&g_84[3],&g_673,&g_673,&g_84[4]},{&l_1480,(void*)0,&g_675[0][0][0],&g_675[2][0][0]},{&g_673,(void*)0,&g_675[1][0][1],&g_675[1][0][0]},{&g_673,&g_675[3][0][1],&g_675[4][0][2],&g_673},{&g_84[4],&g_84[4],&g_675[1][0][0],&g_675[4][0][2]},{&g_84[3],&l_1480,(void*)0,&g_675[2][0][0]}},{{&g_675[1][0][1],&g_84[3],&g_673,&g_84[3]},{&g_673,(void*)0,&g_84[4],&g_675[1][0][1]},{&g_675[1][0][1],&l_1225,(void*)0,&l_1307},{&g_84[4],&g_673,&l_1480,&g_673},{&g_84[3],&g_675[1][0][1],&g_84[3],&g_673},{(void*)0,&g_675[1][0][1],&g_673,&g_84[4]}},{{&l_1307,&g_675[2][0][0],&g_675[1][0][1],&g_675[1][0][1]},{(void*)0,&g_675[1][0][1],&g_675[1][0][1],(void*)0},{&l_1307,&g_84[4],&g_673,&g_673},{(void*)0,&l_1480,&g_84[3],&g_673},{&g_84[3],&g_673,&l_1480,&g_675[2][0][1]},{&g_84[4],&g_84[3],(void*)0,&g_675[1][0][0]}},{{&g_675[1][0][1],&g_675[1][0][1],&g_84[4],&g_675[1][0][1]},{&g_673,&g_675[0][0][0],&g_673,&l_1307},{&g_675[1][0][1],&g_673,(void*)0,&g_673},{&g_84[3],&l_1480,&g_675[1][0][0],&g_675[1][0][1]},{&g_84[4],&g_675[1][0][1],&g_675[4][0][2],&g_675[1][0][1]},{&g_673,&l_1225,&g_675[1][0][1],&g_675[1][0][1]}},{{&l_1480,&l_1480,&g_675[1][0][1],(void*)0},{(void*)0,&g_675[3][0][1],&g_675[1][0][1],&g_675[2][0][0]},{(void*)0,&g_673,&g_675[1][0][0],&g_675[1][0][1]},{(void*)0,&g_673,&g_675[1][0][1],&g_675[2][0][0]},{&g_673,&g_675[3][0][1],(void*)0,(void*)0},{&g_673,&l_1480,(void*)0,&g_675[1][0][1]}},{{&g_84[0],&l_1225,(void*)0,&g_675[1][0][1]},{&g_675[1][0][1],&g_675[1][0][1],&l_1480,&g_675[1][0][1]},{(void*)0,&l_1480,(void*)0,&g_673},{&l_1480,&g_673,&g_675[4][0][2],&l_1307},{&l_1307,&g_675[0][0][0],&g_675[1][0][1],&g_675[1][0][1]},{(void*)0,&g_675[1][0][1],&g_84[0],&g_675[1][0][0]}},{{(void*)0,&g_84[3],&g_673,&g_675[2][0][1]},{&g_84[4],&g_673,(void*)0,&g_673},{&g_675[1][0][0],&l_1480,&g_675[1][0][1],&g_673},{&g_84[4],&g_84[4],&g_673,(void*)0},{&g_84[0],&g_675[1][0][1],(void*)0,&g_675[1][0][1]},{&g_84[0],&g_675[2][0][0],&g_673,&g_84[4]}},{{&g_84[4],&g_675[1][0][1],&g_675[1][0][1],&g_673},{&g_675[1][0][0],&g_675[1][0][1],(void*)0,&g_673},{&g_84[4],&g_673,&g_673,&l_1307},{(void*)0,&l_1225,&g_84[0],&g_675[1][0][1]},{(void*)0,(void*)0,&g_675[1][0][1],&g_84[3]},{&l_1307,&g_84[3],&g_675[4][0][2],&g_675[2][0][0]}}};
                int i, j, k;
            }
            for (g_82 = 14; (g_82 != 4); g_82 = safe_sub_func_uint32_t_u_u(g_82, 1))
            { /* block id: 723 */
                int32_t l_1614[1];
                float ***l_1640 = &g_591;
                int32_t l_1655 = 6L;
                int32_t * const l_1692 = (void*)0;
                int32_t * const *l_1691 = &l_1692;
                int32_t * const **l_1690 = &l_1691;
                int32_t l_1747 = 0L;
                int32_t l_1753[4] = {0x5A689D5AL,0x5A689D5AL,0x5A689D5AL,0x5A689D5AL};
                int i;
                for (i = 0; i < 1; i++)
                    l_1614[i] = 1L;
            }
        }
        return &g_38[4];
    }
    if ((p_41 , (((void*)0 == l_1762) , (safe_add_func_int16_t_s_s(p_41, (p_43 | p_42))))))
    { /* block id: 795 */
        float **l_1765[3][6][3] = {{{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592},{(void*)0,&g_592,(void*)0},{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592}},{{&g_592,&g_592,&g_592},{(void*)0,&g_592,(void*)0},{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592},{(void*)0,&g_592,(void*)0}},{{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592},{(void*)0,&g_592,(void*)0},{&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592}}};
        int32_t l_1781 = 0xB536408CL;
        int32_t l_1786 = 1L;
        int32_t l_1788 = 9L;
        int32_t l_1790 = 0x3F5E4DA3L;
        uint16_t *l_1795[8] = {&l_1307,&l_1307,&l_1307,&l_1307,&l_1307,&l_1307,&l_1307,&l_1307};
        uint16_t **l_1794 = &l_1795[1];
        int32_t *l_1805 = &l_1790;
        uint64_t **l_1850 = &g_34[4][3];
        uint64_t ***l_1851 = &l_1850;
        int16_t l_1860 = 0x6150L;
        uint64_t l_1867 = 1UL;
        int i, j, k;
        l_1765[0][0][2] = l_1765[0][0][2];
        if (g_91.f4)
            goto lbl_1804;
lbl_1804:
        for (g_327 = 0; (g_327 <= 9); g_327 += 1)
        { /* block id: 799 */
            uint32_t *l_1770 = &g_601;
            int32_t l_1782 = 0x1A363FF4L;
            int32_t l_1783[2];
            float l_1787[8] = {0x0.F867A1p+88,0x0.F867A1p+88,0x0.F867A1p+88,0x0.F867A1p+88,0x0.F867A1p+88,0x0.F867A1p+88,0x0.F867A1p+88,0x0.F867A1p+88};
            float l_1789[5][7][7] = {{{0x0.7p-1,0x4.5p-1,(-0x1.2p-1),0x2.9p-1,0x6.7p+1,0x8.F53919p+88,0x0.5p+1},{0xE.C95F25p+24,0x7.1p-1,0x1.B065D6p+85,(-0x1.2p-1),0xA.08E35Fp+67,0x6.5ADDC2p-87,0xF.58B8ECp-41},{0x2.5p-1,0x0.5p+1,0x0.Ep+1,0x7.1p-1,0x5.E5188Cp-23,0xE.C95F25p+24,0x1.B065D6p+85},{0xA.265AD8p-56,0x0.437D36p+59,0x8.C3DED1p+63,0x3.Ep-1,0x2.9p-1,0x1.Ep+1,(-0x1.Ap+1)},{0x2.1p+1,0xE.6153B7p-98,0x6.F82E76p+11,0x8.23259Bp-95,0x5.9p+1,0xF.EAC638p-0,0x3.9p-1},{(-0x1.Ap+1),(-0x4.Cp+1),0xA.265AD8p-56,0x2.1p+1,0xF.4DB2E2p+59,0x3.3049E2p+41,0x0.Ep+1},{0x5.9p+1,(-0x4.Cp+1),0x7.1p-1,0x9.Ap+1,0x7.483FCBp-54,0x0.437D36p+59,0xE.4F6DE3p-32}},{{0x3.Ep-1,0xE.6153B7p-98,0xC.BE20A1p-60,0x5.9p+1,0x2.1p+1,0x4.5p-1,0xB.AC20DCp-23},{0x8.87DC41p-19,0x0.437D36p+59,0xF.EAC638p-0,0xA.770EC4p+24,0x6.5ADDC2p-87,(-0x1.0p-1),0x6.5ADDC2p-87},{0x5.CEABB2p-19,0x0.5p+1,0x0.5p+1,0x5.CEABB2p-19,0x1.Cp+1,0x6.F82E76p+11,0x0.Bp+1},{0xB.8022FBp-82,0x7.1p-1,0x6.5ADDC2p-87,0xF.4DB2E2p+59,0x2.Cp-1,0x2.1p+1,(-0x6.Ap-1)},{0x3.7p+1,0x4.5p-1,0x2.1p+1,0xC.BE20A1p-60,0x2.5p-1,(-0x4.Cp+1),0x0.Bp+1},{0xE.4F6DE3p-32,0x5.E5188Cp-23,0x6.15BE87p+85,0x5.D55FAAp+66,0xE.51D56Cp+96,(-0x1.Ap+1),0x6.5ADDC2p-87},{0x8.C3DED1p+63,(-0x1.0p-1),0x2.1p+1,0x3.451013p+33,0x0.5p+1,0xA.08E35Fp+67,0xB.AC20DCp-23}},{{0x7.1p-1,0xD.3A3ADBp+41,0xB.8022FBp-82,0xF.58B8ECp-41,0x8.F22F92p-97,0x3.25C541p+9,0xE.4F6DE3p-32},{0x1.E211DBp-75,(-0x1.2p-1),0x8.F53919p+88,0x6.15BE87p+85,0x0.437D36p+59,0xE.6153B7p-98,0x0.Ep+1},{0x7.F47209p-0,0xF.EAC638p-0,0x3.451013p+33,0x9.00B464p+66,0x0.437D36p+59,0x8.002821p-18,0x3.9p-1},{0x6.7p+1,0x0.Bp+1,0x4.5p-1,0x0.57CEE7p-60,0x8.F22F92p-97,0x1.9p+1,0x6.F82E76p+11},{0x0.Bp+1,0x8.87DC41p-19,(-0x4.Cp+1),0x3.Ep-1,0x6.5ADDC2p-87,0x7.691F07p+93,0x2.5p-1},{0xF.4DB2E2p+59,0x0.57CEE7p-60,0x1.047CE2p+71,0x6.5ADDC2p-87,0x1.047CE2p+71,0x0.57CEE7p-60,0xF.4DB2E2p+59},{0xC.D353CDp+78,0x2.1p+1,0x5.9p+1,0xB.BFAF52p-99,0xE.C95F25p+24,0xB.8022FBp-82,0x6.5ADDC2p-87}},{{(-0x1.2p-1),0x1.9p-1,0xC.BE20A1p-60,0x1.E211DBp-75,0x0.57CEE7p-60,0x5.E5188Cp-23,0x0.Ep+1},{0xA.770EC4p+24,0x4.5p-1,0x5.9p+1,0x3.451013p+33,0x3.Ep-1,0x7.483FCBp-54,0xA.52AA22p+0},{0xD.3A3ADBp+41,0xC.BE20A1p-60,0x1.047CE2p+71,0x6.F82E76p+11,0x3.451013p+33,0x0.437D36p+59,(-0x6.Ap-1)},{0xF.58B8ECp-41,0x6.7p+1,(-0x4.Cp+1),(-0x6.Ap-1),0x0.7p-1,0xC.BE20A1p-60,0x0.5p+1},{0x6.5ADDC2p-87,0x5.9p+1,0xA.770EC4p+24,(-0x1.Ap+1),(-0x4.Cp+1),0xA.265AD8p-56,0x2.1p+1},{0x8.87DC41p-19,0x8.23259Bp-95,0x2.9p-1,0x0.5p+1,0x7.1p-1,0xC.BC3EEAp+69,0xD.3A3ADBp+41},{0x8.87DC41p-19,0xE.4F6DE3p-32,0x6.7p+1,0x5.9p+1,0x5.E5188Cp-23,0x3.7p+1,0x8.23259Bp-95}},{{0x6.5ADDC2p-87,0xC.9C7EACp+99,0xC.BC3EEAp+69,0x7.F47209p-0,0xF.58B8ECp-41,0x5.CEABB2p-19,0xB.AC20DCp-23},{0xF.58B8ECp-41,0xE.51D56Cp+96,0x0.7p-1,0x3.7p+1,0x1.Ep+1,0x1.Ep+1,0x3.7p+1},{0xD.3A3ADBp+41,0xF.58B8ECp-41,0xD.3A3ADBp+41,(-0x1.2p-1),0x7.F47209p-0,0x2.9p-1,0xA.08E35Fp+67},{0xA.770EC4p+24,0xF.4DB2E2p+59,0xE.51D56Cp+96,0x2.1p+1,0x6.736D01p-69,0x1.9p+1,0x7.691F07p+93},{(-0x1.2p-1),0x6.15BE87p+85,0x3.451013p+33,0x1.047CE2p+71,0x2.Cp-1,0x2.9p-1,0x8.F22F92p-97},{0xC.D353CDp+78,0xE.C95F25p+24,0x6.5ADDC2p-87,0x8.87DC41p-19,0xA.08E35Fp+67,0x1.Ep+1,0x9.00B464p+66},{0xF.4DB2E2p+59,0x0.Bp+1,0x5.D55FAAp+66,0x5.0D050Cp+32,0xA.52AA22p+0,0x5.CEABB2p-19,0x6.7p+1}}};
            uint16_t ***l_1796 = (void*)0;
            uint16_t ***l_1797 = &l_1794;
            uint64_t *l_1800 = &g_862;
            uint64_t *l_1801[5][6] = {{&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1]},{&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1]},{&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1]},{&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1]},{&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1],&l_1020[1]}};
            uint8_t l_1802 = 0UL;
            int32_t **l_1803[3][10][7] = {{{&l_1202,&l_927[0],&l_927[0],&l_927[0],&l_927[0],&l_927[0],&l_1202},{&l_927[0],&l_927[2],&l_927[0],&g_501,&g_1103,&g_1103,&g_1103},{&g_501,&g_501,&l_927[0],&g_1103,&g_501,&g_501,&l_927[2]},{&g_1103,&l_927[0],&l_927[0],&l_927[0],&l_927[2],&g_501,&g_501},{&l_927[0],&g_501,&l_927[0],&g_501,&g_501,&l_927[0],&g_501},{&g_501,&l_927[0],&g_501,(void*)0,&g_501,&g_501,&l_927[0]},{&g_501,&g_1103,&g_1103,&l_1202,&l_927[0],&l_927[0],&g_501},{&g_1103,&l_927[0],&l_927[0],&l_927[0],&l_927[0],&g_501,&l_927[0]},{&g_501,&g_501,&l_927[0],&l_927[0],&l_927[0],&l_927[0],&l_927[0]},{&g_501,&g_501,&g_501,&l_927[0],&g_501,&g_1103,&g_1103}},{{&g_1103,&l_927[0],&l_927[2],&g_1103,&g_501,&l_927[0],&l_927[0]},{&g_501,&g_501,&l_1202,(void*)0,&g_501,&l_927[0],&g_501},{&l_927[0],&g_1103,&l_927[2],(void*)0,&l_927[0],&l_927[0],(void*)0},{&g_1103,&g_1103,&g_1103,&g_501,&l_927[0],&g_501,(void*)0},{&g_1103,&g_501,&l_927[0],&l_927[0],&l_927[0],&g_501,&g_1103},{&l_927[0],&l_927[0],&l_927[0],&g_1103,&g_1103,&g_501,&l_927[0]},{&g_1103,&g_501,&g_501,&l_927[0],&l_927[0],&l_927[0],&l_927[0]},{&l_927[0],&g_501,&g_501,&l_927[0],&g_501,&l_927[0],&l_927[0]},{&g_1103,&l_927[0],&l_927[0],&g_501,&l_927[0],&l_927[0],&l_1202},{&l_927[0],&g_1103,&l_927[0],&l_927[0],&g_501,&g_1103,&l_927[0]}},{{(void*)0,&l_927[0],&g_1103,&g_1103,&g_501,&l_927[0],&l_927[0]},{&l_927[0],&g_1103,&l_927[2],&g_1103,&l_927[0],&g_501,&l_927[0]},{&g_501,&g_1103,&l_1202,&l_927[0],(void*)0,&l_927[0],&g_1103},{&g_501,&l_927[0],&l_927[2],&g_501,&g_1103,&g_501,(void*)0},{&g_501,&l_927[0],&g_501,&l_927[0],&l_927[2],&g_501,(void*)0},{&l_927[0],&l_927[2],&l_927[0],&l_927[0],&l_927[2],&l_927[0],&g_501},{(void*)0,&g_1103,&l_927[0],&g_1103,&g_1103,(void*)0,&l_927[0]},{&l_927[0],&g_1103,&g_1103,&l_927[0],(void*)0,&g_1103,&g_1103},{&g_1103,&g_1103,&g_1103,&g_501,&l_927[0],&l_927[2],&l_927[0]},{&l_927[0],&l_927[2],&g_501,(void*)0,&g_501,&l_927[2],&l_927[0]}}};
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1783[i] = 0x5246B910L;
        }
        if (((**g_1679) = (*g_1103)))
        { /* block id: 812 */
            const uint32_t l_1812 = 0xAD6BD888L;
            int16_t *l_1841 = (void*)0;
            int16_t *l_1842 = &l_1087;
            int32_t l_1843[5];
            int i;
            for (i = 0; i < 5; i++)
                l_1843[i] = (-3L);
            l_1805 = (g_133 , l_1805);
            (*g_1103) = ((safe_mul_func_int16_t_s_s((p_43 = (((g_675[4][0][0] = p_42) , &l_943[4]) == &g_326[2])), ((safe_mul_func_int8_t_s_s(((p_41 == ((safe_add_func_float_f_f(l_1812, (safe_sub_func_float_f_f((0x4.8C92ECp-7 == ((safe_add_func_float_f_f(0x1.Dp+1, (safe_sub_func_float_f_f(((safe_sub_func_float_f_f(((((*g_787) = (p_42 & (g_260 , 0xB515L))) || l_1812) , (*l_1805)), 0x9.0p+1)) == 0x3.Fp-1), (**g_591))))) == p_42)), (*l_1805))))) == 0x9.7p-1)) , 8L), p_41)) | 0x5C686EE0L))) , l_1821);
            (**g_1679) &= (((g_84[4] |= (safe_mul_func_uint16_t_u_u(p_43, p_43))) , (l_1843[4] = (0L & (safe_sub_func_int64_t_s_s(0x8758FD30B2696968LL, (safe_mod_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_u((((((*p_44) , l_1830) != &g_591) >= ((safe_sub_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u(((*l_1842) = (safe_mod_func_int16_t_s_s(((safe_sub_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((p_42 > 0L), g_193)), (-1L))) != p_41), 7L))), 11)), p_43)) > g_973.f3)) | (*l_1805)), p_43)) && p_42), l_1812))))))) > l_1812);
        }
        else
        { /* block id: 822 */
            int32_t *l_1844 = &l_1781;
            return &g_1591;
        }
        (*g_592) = ((safe_div_func_float_f_f(0x8.7FED8Dp-21, (((safe_add_func_int64_t_s_s(0x0D466E94146AFDDFLL, ((l_1849 == ((*l_1851) = l_1850)) , (safe_sub_func_uint16_t_u_u(((safe_sub_func_int8_t_s_s((*p_44), (((-9L) == (safe_mul_func_uint16_t_u_u((((((safe_sub_func_int16_t_s_s((l_1860 && (+(safe_rshift_func_int8_t_s_u((+(safe_mod_func_uint32_t_u_u((l_1867++), p_43))), 4)))), (((*l_1805) <= (*p_44)) , g_485.f4))) && 0x40E68051L) && 0x1373D967A6A7B79BLL) ^ 0x70L) & 0x78749C5EL), (-6L)))) || p_42))) && (*g_20)), 0xB661L))))) , 0x0.Fp+1) > (*l_1805)))) == 0x0.CEABFEp-18);
    }
    else
    { /* block id: 828 */
        int16_t l_1873 = 2L;
        int32_t ***l_1878 = &g_481;
        int32_t l_1893[10][6] = {{1L,(-6L),0x67C05E91L,0xE7FA6CD4L,1L,(-10L)},{(-1L),1L,0x67C05E91L,3L,3L,3L},{(-10L),3L,(-10L),0x4E4E9BB8L,0xBBDF34C2L,0L},{0x4E4E9BB8L,0xBBDF34C2L,0L,0x67C05E91L,(-6L),1L},{(-2L),0xE7FA6CD4L,(-7L),0x67C05E91L,(-10L),(-10L)},{(-10L),3L,3L,(-10L),3L,2L},{0xD8E4A689L,(-2L),1L,0L,(-7L),3L},{0x069436C6L,0x67C05E91L,(-1L),0xA73D1559L,(-7L),1L},{3L,(-2L),(-10L),(-2L),3L,0xA73D1559L},{(-1L),3L,0x4E4E9BB8L,2L,(-10L),0x069436C6L}};
        int8_t l_1908 = 0x92L;
        int32_t l_1913 = 4L;
        int i, j;
        for (g_673 = (-21); (g_673 <= 50); g_673 = safe_add_func_uint32_t_u_u(g_673, 8))
        { /* block id: 831 */
            int32_t ***l_1876[5][7] = {{(void*)0,&g_481,&g_481,(void*)0,&g_481,&g_481,(void*)0},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{(void*)0,(void*)0,&g_481,(void*)0,(void*)0,&g_481,(void*)0},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,(void*)0,&g_481,&g_481,(void*)0,&g_481,&g_481}};
            float l_1896 = 0x6.221B99p-9;
            uint64_t l_1897[5][6] = {{1UL,1UL,0xC4496B1231FE760ALL,1UL,1UL,0xC4496B1231FE760ALL},{1UL,1UL,0xC4496B1231FE760ALL,1UL,1UL,0xC4496B1231FE760ALL},{1UL,1UL,0xC4496B1231FE760ALL,1UL,1UL,0xC4496B1231FE760ALL},{1UL,1UL,0xC4496B1231FE760ALL,1UL,1UL,0xC4496B1231FE760ALL},{1UL,1UL,0xC4496B1231FE760ALL,1UL,1UL,0xC4496B1231FE760ALL}};
            int i, j;
            for (g_550 = 0; (g_550 <= 4); g_550 += 1)
            { /* block id: 834 */
                int32_t ****l_1877[1];
                uint16_t ****l_1890 = &g_1887;
                uint16_t ****l_1892 = &l_1891;
                int32_t l_1895[9] = {0x5184926CL,0x5184926CL,0x5184926CL,0x5184926CL,0x5184926CL,0x5184926CL,0x5184926CL,0x5184926CL,0x5184926CL};
                int i;
                for (i = 0; i < 1; i++)
                    l_1877[i] = &l_1876[0][6];
                (*l_1202) |= (+l_1873);
                (*g_1103) ^= 1L;
                (**g_591) = ((safe_mul_func_uint16_t_u_u((l_1893[7][2] = (((**l_1849) = ((l_1878 = l_1876[0][6]) != (void*)0)) < (((safe_lshift_func_int16_t_s_u((&l_1877[0] != (void*)0), (((safe_sub_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((-7L), 2)), p_42)) && (((((*l_1890) = g_1887) != ((*l_1892) = l_1891)) > 0UL) , (*g_20))) == 0xFBF5L))) >= p_43) != g_38[3]))), p_43)) , p_41);
                l_1897[4][4]--;
            }
            return l_1101;
        }
        if (p_43)
        { /* block id: 847 */
            int64_t l_1900 = 0L;
            uint64_t l_1901 = 0xC729E107762A3C97LL;
            l_1901--;
        }
        else
        { /* block id: 849 */
            int16_t **l_1910 = &g_188;
            int32_t l_1911[9];
            int i;
            for (i = 0; i < 9; i++)
                l_1911[i] = 0x4039BDC9L;
            (*l_1202) = (safe_unary_minus_func_uint64_t_u(((!(((*g_1246) &= (safe_sub_func_int32_t_s_s((p_41 || (l_1908 > l_1908)), (((&g_1679 == (p_43 , l_1909)) , (((void*)0 == l_1910) <= (-1L))) != l_1911[7])))) , 0x918821A6L)) , l_1911[7])));
        }
        (*g_592) = p_43;
        l_1915[1]--;
    }
    return l_927[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_592 g_159 g_38 g_470 g_260 g_123 g_327 g_856 g_673 g_787 g_788
 * writes: g_94 g_123
 */
static int8_t  func_50(float  p_51, uint16_t  p_52, int8_t * const  p_53, uint8_t  p_54)
{ /* block id: 391 */
    const int32_t *l_887[6] = {&g_159,&g_159,&g_159,&g_159,&g_159,&g_159};
    const int32_t **l_888 = &l_887[5];
    const int8_t l_891 = (-10L);
    uint64_t l_900 = 0x24C6AD56F57FBF8FLL;
    int64_t l_902 = 0x6679CAFB6817A087LL;
    int32_t l_903 = 0x255C9D82L;
    int32_t l_904 = 0xC280BDB9L;
    uint32_t l_905 = 0x94B8C81BL;
    int32_t *l_906 = &g_123;
    int64_t *l_923 = &g_713[4];
    int64_t **l_922 = &l_923;
    const float l_924 = 0x4.796B0Bp-82;
    int32_t *l_925 = (void*)0;
    int32_t *l_926 = &l_903;
    int i;
    (*l_888) = l_887[5];
    (*l_906) &= ((safe_rshift_func_int16_t_s_s((p_52 <= (((l_891 && (safe_lshift_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((((((l_904 &= ((safe_lshift_func_int16_t_s_s((l_903 ^= (((safe_mul_func_uint8_t_u_u(0xA9L, (((l_900 , ((*g_592) = (-(-0x2.Ep-1)))) > l_902) , p_54))) >= ((-5L) >= ((((250UL == 0xBFL) && p_52) == 0x0B83L) & (**l_888)))) == g_470)), 12)) > g_260)) <= l_905) , (**l_888)) , &p_54) != &p_54) & 0x4D7BL), (**l_888))), 6))) < (**l_888)) <= p_54)), p_52)) < p_52);
    (*l_926) |= (((+((safe_lshift_func_int8_t_s_u((safe_lshift_func_int8_t_s_u((safe_div_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(g_327, (p_52 & (safe_lshift_func_uint16_t_u_s(((safe_add_func_uint8_t_u_u(g_856, (((p_54 , (safe_lshift_func_uint8_t_u_u(0x8BL, 7))) <= 5UL) >= (-10L)))) < (((**l_888) | (((*l_922) = &l_902) == &l_902)) ^ (**l_888))), 7))))), 0x7FL)), p_54)), (*l_906))) >= p_54)) && g_673) , (**l_888));
    return (*g_787);
}


/* ------------------------------------------ */
/* 
 * reads : g_139 g_35 g_20 g_21 g_123 g_225 g_91.f4 g_91.f1 g_91.f0 g_91.f3 g_195 g_260 g_138 g_131 g_327 g_90.f2 g_133 g_38 g_329 g_90.f1 g_84 g_324 g_422 g_165 g_166 g_295 g_261 g_470 g_480 g_481 g_501 g_159 g_90.f0 g_254 g_114 g_485.f2 g_90.f4 g_11 g_591 g_601 g_485.f3 g_592 g_158 g_94 g_485.f0 g_326 g_671 g_90.f3 g_673 g_675 g_485.f4 g_188 g_715 g_193 g_82 g_600 g_775 g_821 g_788 g_713 g_121 g_856
 * writes: g_139 g_159 g_84 g_35 g_195 g_82 g_123 g_91.f0 g_90.f2 g_324 g_135 g_131 g_193 g_188 g_90.f0 g_138 g_501 g_297 g_550 g_576 g_587 g_601 g_485.f0 g_94 g_261 g_671 g_673 g_715 g_133 g_775 g_786 g_821 g_114 g_121 g_674 g_34 g_862 g_481
 */
static int8_t * const  func_56(uint32_t  p_57)
{ /* block id: 133 */
    uint16_t l_318[7][8] = {{8UL,0xC8CDL,0xC6CDL,0x42FCL,0x3F8CL,65535UL,65535UL,0x3F8CL},{65527UL,9UL,9UL,65527UL,4UL,1UL,0xC6CDL,5UL},{5UL,5UL,1UL,1UL,7UL,5UL,0xBCC6L,0x42FCL},{0x0E57L,5UL,65527UL,0xC6CDL,0x1FA6L,1UL,5UL,1UL},{0xC8CDL,9UL,0UL,9UL,0xC8CDL,65535UL,0x1FA6L,0UL},{0x5F85L,0xC8CDL,0x3F8CL,1UL,5UL,65528UL,65535UL,9UL},{1UL,0xC6CDL,0x3F8CL,65527UL,0xBCC6L,0x6830L,0x1FA6L,0x1FA6L}};
    int32_t l_319 = 0x4C61F8A7L;
    int32_t l_328 = 0xA108D62EL;
    int64_t *l_336[6];
    uint8_t *l_354 = &g_139;
    int32_t l_365 = 0x52A6E63BL;
    int32_t l_366 = (-9L);
    int32_t l_368 = 0xC71D38BCL;
    int32_t l_369 = 0xB2CE5E1FL;
    int32_t l_370[1][3];
    uint8_t l_373 = 1UL;
    int32_t l_450 = 0x80C9FE62L;
    const float * const l_464 = (void*)0;
    int16_t **l_468 = &g_188;
    int32_t l_590 = 0x797B08F0L;
    int32_t l_609 = 0x5D73DE8CL;
    int8_t l_619 = 0xCEL;
    int32_t *l_642 = &g_260;
    int32_t * const *l_641[3][10] = {{&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642},{&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642},{&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642,&l_642}};
    int8_t **l_797 = &g_20;
    uint64_t *l_861[6][6] = {{&g_862,&g_862,(void*)0,(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862,(void*)0,&g_862,(void*)0},{&g_862,&g_862,&g_862,(void*)0,&g_862,&g_862},{&g_862,&g_862,(void*)0,(void*)0,&g_862,&g_862},{&g_862,&g_862,&g_862,(void*)0,&g_862,(void*)0},{&g_862,&g_862,&g_862,(void*)0,&g_862,&g_862}};
    uint64_t ** const l_860 = &l_861[0][2];
    int32_t *l_879 = &g_123;
    int i, j;
    for (i = 0; i < 6; i++)
        l_336[i] = (void*)0;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_370[i][j] = 0x538E928FL;
    }
    for (g_139 = 0; (g_139 <= 7); g_139 += 1)
    { /* block id: 136 */
        const uint64_t l_316 = 1UL;
        int32_t *l_321 = (void*)0;
        int32_t *l_323 = &l_319;
        int32_t l_325 = 1L;
        int32_t l_330 = 1L;
        uint64_t l_331 = 0UL;
        int8_t * const l_334[9] = {&g_21,(void*)0,&g_21,&g_21,(void*)0,&g_21,&g_21,(void*)0,&g_21};
        int64_t *l_335 = &g_193;
        uint32_t l_353 = 4294967295UL;
        uint16_t *l_357[8] = {&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2]};
        int32_t l_371 = 1L;
        int32_t l_372[1][10] = {{0xD13C091DL,0x8376A847L,0xD13C091DL,0xE7A3A5DBL,0xE7A3A5DBL,0xD13C091DL,0x8376A847L,0xD13C091DL,0xE7A3A5DBL,0xE7A3A5DBL}};
        uint32_t *l_413 = &l_353;
        uint64_t *l_416[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t *l_417 = (void*)0;
        int32_t *l_418 = &g_324;
        int32_t *l_419 = &l_372[0][7];
        uint32_t l_424[6];
        struct S0 *l_484 = &g_485;
        const uint64_t l_496 = 1UL;
        int8_t l_511 = 0x6AL;
        const uint16_t l_575 = 65529UL;
        uint16_t l_610 = 0x67CDL;
        int8_t l_799 = 0L;
        int16_t ** const l_809 = &g_188;
        int i, j;
        for (i = 0; i < 6; i++)
            l_424[i] = 0x43F756F6L;
        for (g_159 = 7; (g_159 >= 0); g_159 -= 1)
        { /* block id: 139 */
            int16_t **l_302 = &g_188;
            uint16_t *l_306 = &g_84[4];
            uint64_t *l_307 = &g_35;
            uint32_t l_317[7][9][2] = {{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}},{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}},{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}},{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}},{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}},{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}},{{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}}};
            int32_t *l_320 = &g_123;
            int32_t **l_322[7] = {&l_321,&l_321,&l_321,&l_321,&l_321,&l_321,&l_321};
            int i, j, k;
            (*l_320) &= (((void*)0 == l_302) < (((safe_mul_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u((l_318[0][4] = (0x99A6F27C5AEF2890LL >= (((((*l_307) &= (((*l_306) = 0xB8B8L) & 0xFB6EL)) >= ((safe_lshift_func_uint16_t_u_s((safe_sub_func_int8_t_s_s(p_57, (g_82 = ((((safe_lshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((*g_20), 2)), 1)) , l_316) ^ ((((g_195 = l_317[0][3][1]) < p_57) ^ l_316) , p_57)) ^ p_57)))), p_57)) <= p_57)) && p_57) , l_316))))), l_319)) <= p_57) , p_57));
            l_323 = l_321;
            l_331--;
            return l_334[7];
        }
        (*l_323) = (((p_57 && ((-1L) || (l_335 == l_336[3]))) < (g_195 = (safe_div_func_uint16_t_u_u(g_225, (safe_div_func_uint64_t_u_u((safe_sub_func_int16_t_s_s(((safe_sub_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((safe_div_func_int64_t_s_s((safe_mod_func_uint64_t_u_u(((safe_mul_func_uint16_t_u_u(l_353, (l_354 == &g_195))) , (+(~(g_84[3] = (p_57 != l_318[0][4]))))), (*l_323))), g_91.f4)), 0x0831A354450C69CELL)), p_57)) ^ g_91.f1), p_57)), 0x727D57A6E24EFC4CLL)))))) >= p_57);
        for (l_325 = 0; (l_325 <= 7); l_325 += 1)
        { /* block id: 155 */
            int32_t l_361 = (-1L);
            uint64_t *l_362[7];
            int32_t l_363 = 0x3313AE28L;
            int32_t l_364 = 0x4706DB24L;
            int32_t l_367[2];
            uint8_t *l_378 = &g_139;
            int32_t *l_379 = &l_370[0][2];
            int16_t *l_384 = &g_91.f0;
            uint8_t *l_386 = (void*)0;
            uint8_t *l_387 = &g_195;
            int8_t *l_389 = &g_21;
            int8_t **l_388 = &l_389;
            uint8_t *l_402 = &l_373;
            int i;
            for (i = 0; i < 7; i++)
                l_362[i] = (void*)0;
            for (i = 0; i < 2; i++)
                l_367[i] = 0x13B4932BL;
            (*l_379) ^= ((g_91.f4 && (((!((g_21 == (0xF67431AA0B9C9FA5LL | ((safe_div_func_uint64_t_u_u((&g_195 == (((p_57 , (*l_323)) || (p_57 < (((++l_373) , (safe_mul_func_int8_t_s_s(p_57, (*g_20)))) <= g_91.f0))) , l_378)), g_91.f3)) == l_361))) == l_363)) || 0xF4L) || 0xFDB39CC4L)) , (-6L));
            (*l_323) = (((((safe_rshift_func_int16_t_s_s(((((*l_384) &= (p_57 <= p_57)) >= ((~((*l_387) ^= (*l_379))) >= g_260)) , (*l_379)), (((*l_388) = &g_254) != ((((safe_div_func_uint16_t_u_u((g_84[4] = (((safe_mod_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((safe_unary_minus_func_uint16_t_u((((*l_402) ^= ((p_57 , ((*l_323) & (safe_lshift_func_uint16_t_u_u((!((safe_mul_func_int8_t_s_s(l_370[0][1], 0x79L)) <= g_138)), 9)))) == g_131)) < (*l_379)))), 5)), p_57)) > g_327) || 0x01F3L)), g_90.f2)) < p_57) , l_366) , &g_82)))) , l_328) , g_133) != l_366) ^ g_133);
            return l_354;
        }
        if (((*l_419) &= (safe_mul_func_uint16_t_u_u((0xFCB1B677L <= ((0xDE97L == ((l_370[0][1] |= p_57) , ((((((*l_418) |= (safe_rshift_func_int8_t_s_s((safe_add_func_int64_t_s_s(((safe_add_func_int64_t_s_s(((l_370[0][1] = ((g_90.f2 = (g_133 , ((safe_mod_func_uint32_t_u_u(((*l_413)++), ((((0xC65250F36B81FDECLL & ((p_57 == ((g_38[1] <= g_35) <= (((-1L) > g_329[0]) , p_57))) && (-1L))) , (void*)0) == (void*)0) | (*l_323)))) != 4294967295UL))) >= g_90.f1)) || l_368), p_57)) || (-2L)), g_84[0])), l_366))) , 0x74DFCF0DL) , l_328) >= (*l_323)) , l_368))) & p_57)), p_57))))
        { /* block id: 172 */
            int32_t l_440 = 0xF4E8CB38L;
            int16_t **l_467 = &g_188;
            int32_t l_522 = (-1L);
            float *l_549 = &g_94;
            int8_t l_577 = 9L;
            int32_t *l_583 = &l_365;
            int8_t **l_585 = &g_20;
            float **l_593 = &g_592;
            int32_t l_599 = 0x66CA3048L;
            if ((0xEBL >= (safe_div_func_int32_t_s_s(((0UL || p_57) >= (((g_38[6] != p_57) , (((void*)0 == l_321) == 0xE9L)) , ((void*)0 == g_422))), p_57))))
            { /* block id: 173 */
                uint32_t l_441 = 0x77F92105L;
                float *l_442 = &g_135;
                (*l_442) = ((((+l_424[1]) <= ((safe_mod_func_int32_t_s_s((((65526UL ^ (!((safe_sub_func_uint64_t_u_u(3UL, ((safe_add_func_uint8_t_u_u(p_57, (safe_sub_func_uint16_t_u_u(p_57, ((safe_sub_func_int16_t_s_s((safe_mod_func_uint8_t_u_u(g_138, (*l_419))), (0x6FF82A61L > (safe_div_func_uint64_t_u_u(l_440, 18446744073709551615UL))))) >= g_260))))) > 0x7189BFEBL))) , l_441))) < l_440) < p_57), 0x3A09A71BL)) >= 1UL)) && 0x47L) , (-0x1.7p-1));
            }
            else
            { /* block id: 175 */
                int32_t l_449[1];
                float *l_465 = (void*)0;
                int32_t **l_466 = &l_417;
                int16_t *l_486 = &g_90.f0;
                const uint8_t l_487[1] = {1UL};
                int32_t l_488 = 6L;
                uint8_t *l_510 = &l_373;
                int8_t *l_589 = (void*)0;
                int8_t **l_588[5][5][4] = {{{&l_589,(void*)0,&g_20,(void*)0},{&g_20,(void*)0,&l_589,(void*)0},{(void*)0,&l_589,&l_589,&g_20},{&l_589,&g_20,&g_20,&l_589},{&l_589,(void*)0,&l_589,&l_589}},{{(void*)0,&l_589,&l_589,&g_20},{&g_20,&g_20,&g_20,&g_20},{&l_589,&l_589,(void*)0,&l_589},{&l_589,(void*)0,&l_589,&l_589},{&g_20,&g_20,&l_589,&g_20}},{{&l_589,&l_589,(void*)0,(void*)0},{&l_589,(void*)0,&g_20,(void*)0},{&g_20,(void*)0,&l_589,(void*)0},{(void*)0,&l_589,&l_589,&g_20},{&l_589,&g_20,&g_20,&l_589}},{{&l_589,(void*)0,&l_589,&l_589},{(void*)0,&l_589,&l_589,&g_20},{&g_20,&g_20,&g_20,&g_20},{&l_589,&l_589,(void*)0,&l_589},{&l_589,(void*)0,&l_589,&l_589}},{{&g_20,&g_20,&l_589,&g_20},{&l_589,&l_589,(void*)0,(void*)0},{&l_589,(void*)0,&g_20,(void*)0},{&g_20,(void*)0,&l_589,(void*)0},{(void*)0,&l_589,&l_589,&g_20}}};
                int32_t l_598[5];
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_449[i] = (-1L);
                for (i = 0; i < 5; i++)
                    l_598[i] = 3L;
                for (g_131 = 0; (g_131 <= 7); g_131 += 1)
                { /* block id: 178 */
                    int16_t l_459 = 0xDBB5L;
                    int32_t l_497[9][1] = {{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)}};
                    int i, j;
                    if ((*l_323))
                        break;
                    (*l_419) |= (((safe_rshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((p_57 & (l_368 , (((g_165[0][1] >= p_57) < ((*l_335) = p_57)) >= g_166))), (l_449[0] = ((g_295 , (*g_20)) >= ((safe_sub_func_uint16_t_u_u(((void*)0 != &g_195), 0UL)) == 6L))))), 7)) && p_57) , l_450);
                    if (((safe_mod_func_int8_t_s_s((*g_20), (safe_add_func_uint64_t_u_u((((((safe_div_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(p_57, 0)), (((((((l_459 && (g_261 | ((*l_413) = ((safe_sub_func_uint8_t_u_u((((((l_449[0] == ((0xE8E0L || (l_464 != l_465)) <= l_449[0])) , 0x4110843CL) , l_466) != (void*)0) | (*l_323)), p_57)) == (*g_20))))) != l_459) , (*g_20)) <= g_329[5]) , l_467) != l_468) , l_459))) & 0x6FL) > 0xEC95L) >= 255UL) | 0x08L), l_449[0])))) ^ p_57))
                    { /* block id: 184 */
                        int32_t **l_469 = &l_321;
                        const int32_t *l_483 = &l_330;
                        const int32_t **l_482 = &l_483;
                        int8_t * const l_489 = &g_21;
                        (*l_469) = &l_370[0][1];
                        l_488 = (((g_470 & (l_366 ^ ((safe_lshift_func_int8_t_s_u(((!(safe_lshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_s(0xA8BBL, ((*l_486) = ((((*l_467) = (((p_57 , (-6L)) , (safe_add_func_uint32_t_u_u((((g_480 == (((g_481 == l_482) > 1UL) , l_484)) | p_57) | (*l_323)), (*l_483)))) , l_486)) != &l_459) ^ l_487[0])))), 3))) || g_35), (*l_323))) >= 0x3F8FL))) | (*l_483)) > p_57);
                        return l_489;
                    }
                    else
                    { /* block id: 190 */
                        uint32_t l_498 = 3UL;
                        int32_t *l_499 = (void*)0;
                        int32_t *l_500 = &l_368;
                        (*l_500) &= ((*l_323) = ((((safe_sub_func_uint16_t_u_u((g_84[4] || ((void*)0 == l_321)), ((g_138 |= l_488) || ((l_497[2][0] = ((*l_335) = (((safe_div_func_int8_t_s_s(((l_465 != (void*)0) != ((g_91.f0 < p_57) || ((*l_419) = (l_496 <= p_57)))), g_133)) == 0x8B8355C1B1496B6BLL) >= p_57))) ^ (*l_323))))) | l_498) && l_440) , l_497[2][0]));
                    }
                    for (l_331 = 0; (l_331 <= 6); l_331 += 1)
                    { /* block id: 200 */
                        int16_t l_503 = 0x30A0L;
                        g_501 = (l_373 , &g_159);
                        l_503 = (!p_57);
                    }
                }
                (*g_501) = ((safe_div_func_int32_t_s_s(p_57, ((safe_mul_func_int8_t_s_s(1L, (safe_rshift_func_uint8_t_u_s(((*l_510) = 0xA5L), 1)))) | l_511))) >= (safe_lshift_func_uint16_t_u_s((((((*l_323) ^= ((g_297 = ((l_522 = (safe_mod_func_int32_t_s_s((((*l_413) = l_449[0]) || ((0x11L < 0xE4L) || (((*l_335) = (((*l_486) = ((g_131 > ((((((safe_rshift_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_s(((safe_div_func_int16_t_s_s((((p_57 | l_487[0]) < l_440) && l_440), 0x020FL)) != 0xD3L), 0)), p_57)) , (void*)0) == (void*)0) >= l_440) < l_440) | l_318[6][4])) , l_487[0])) ^ 0xC96CL)) >= 18446744073709551610UL))), p_57))) & l_487[0])) >= g_38[3])) >= (*g_501)) , 0x65353571L) <= l_449[0]), p_57)));
                if ((safe_mod_func_uint8_t_u_u(((safe_div_func_uint64_t_u_u(((l_440 | (((safe_add_func_uint8_t_u_u(((*l_510) = (&g_84[4] != (((safe_rshift_func_int16_t_s_s(((*l_486) |= (*l_323)), 5)) != (((safe_add_func_uint16_t_u_u((l_488 |= g_260), ((((*g_501) = (safe_unary_minus_func_uint64_t_u(p_57))) | (safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((~(safe_add_func_uint8_t_u_u(((0x9CL > ((safe_lshift_func_uint8_t_u_u(p_57, 6)) && (safe_add_func_int64_t_s_s(((g_550 = (safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s((((void*)0 == l_549) & l_450), p_57)), g_166))) <= 0xE961L), p_57)))) >= p_57), (*g_20)))), 1)), (*g_20)))) || l_522))) || l_366) | (*l_323))) , (void*)0))), 0x5EL)) , 0UL) , p_57)) & l_370[0][1]), p_57)) | 0xAAB5B98E4DBC960CLL), p_57)))
                { /* block id: 218 */
                    uint32_t l_563 = 0x47D2F654L;
                    l_563 = (((safe_add_func_uint64_t_u_u(7UL, p_57)) , (g_254 <= l_449[0])) , (safe_div_func_int32_t_s_s(((((((*l_486) |= 0L) | (safe_div_func_uint32_t_u_u((((*l_323) = (+((safe_add_func_uint16_t_u_u((g_84[4] = (p_57 && (l_465 == &g_167))), ((!((((safe_add_func_uint8_t_u_u(g_114, p_57)) == l_366) <= g_485.f2) < 0x94C583BFL)) == l_449[0]))) & 0x37E3A84446A3DD33LL))) ^ p_57), p_57))) & g_90.f4) == 0x0CCCL) & p_57), (*g_501))));
                }
                else
                { /* block id: 223 */
                    const int8_t *l_579 = &g_254;
                    const int8_t **l_578 = &l_579;
                    const int8_t ***l_580 = &l_578;
                    const int8_t **l_582 = &l_579;
                    const int8_t ***l_581 = &l_582;
                    int32_t l_595[1][7] = {{0L,0L,0L,0L,0L,0L,0L}};
                    int i, j;
                    if ((l_370[0][2] |= ((*l_323) = (safe_lshift_func_int8_t_s_u(l_328, ((safe_mul_func_int8_t_s_s(((((void*)0 == &l_366) > (l_450 != (l_488 = (safe_mul_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u((safe_unary_minus_func_int32_t_s((safe_div_func_int64_t_s_s(g_114, l_575)))), (l_577 = (g_576 = g_11[0][0][4])))) & (((*l_580) = l_578) != ((*l_581) = (void*)0))), l_440))))) , 0x55L), p_57)) , 0x8FL))))))
                    { /* block id: 231 */
                        int32_t **l_584 = &l_323;
                        int8_t ***l_586[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_586[i] = &l_585;
                        (*l_584) = l_583;
                        (*g_501) ^= ((l_590 = ((g_587 = l_585) != (l_588[0][0][3] = &g_20))) < (*l_583));
                    }
                    else
                    { /* block id: 237 */
                        int32_t *l_594 = &l_370[0][0];
                        int32_t *l_596 = &l_369;
                        int32_t *l_597[6][7] = {{&g_159,&g_159,&l_595[0][5],&l_595[0][5],&g_159,&g_159,&l_595[0][5]},{&l_371,(void*)0,&l_371,(void*)0,&l_371,(void*)0,&l_371},{&g_159,&l_595[0][5],&l_595[0][5],&g_159,&g_159,&l_595[0][5],&l_595[0][5]},{&l_488,(void*)0,&l_488,(void*)0,&l_488,(void*)0,&l_488},{&g_159,&g_159,&l_595[0][5],&l_595[0][5],&g_159,&g_159,&l_595[0][5]},{&l_371,(void*)0,&l_371,(void*)0,&l_371,(void*)0,&l_371}};
                        int i, j;
                        l_593 = g_591;
                        ++g_601;
                    }
                    if (((*l_419) |= l_370[0][1]))
                    { /* block id: 242 */
                        (*l_583) |= (*g_501);
                        (*l_583) ^= 0x434ADA71L;
                        if (p_57)
                            break;
                        return (*l_585);
                    }
                    else
                    { /* block id: 247 */
                        return &g_470;
                    }
                }
            }
        }
        else
        { /* block id: 252 */
            uint8_t *l_604 = &g_195;
            int32_t l_607 = 0x9F24AA2CL;
            int32_t l_608 = 0x1F5BFCE8L;
            int16_t *l_615 = (void*)0;
            int16_t *l_616 = &g_485.f0;
            const int8_t * const *l_621 = (void*)0;
            const int8_t * const **l_620 = &l_621;
            (**g_591) = (((l_608 = ((((*l_604) ^= g_485.f3) | 1UL) ^ (((++l_610) & p_57) != (&g_324 == &g_166)))) ^ ((*l_616) = (safe_add_func_int8_t_s_s((-1L), (0UL || l_609))))) , (((safe_mod_func_int64_t_s_s((((l_619 , (void*)0) != l_620) | (*l_419)), l_366)) ^ (*g_501)) , l_608));
            (*l_419) ^= ((*l_323) = 0xF8AE85D3L);
        }
        for (g_261 = 0; (g_261 <= 7); g_261 += 1)
        { /* block id: 263 */
            int64_t l_630 = 0xA581FA7C76EA2730LL;
            const int32_t l_637[7][1] = {{0x363FF28CL},{0x6F8B911EL},{0x363FF28CL},{0x6F8B911EL},{0x363FF28CL},{0x6F8B911EL},{0x363FF28CL}};
            int32_t l_640 = 0x0B4E7FC8L;
            int8_t ***l_677[1];
            int16_t * const l_689 = &g_112;
            int32_t l_712 = 0xB051AE13L;
            int32_t l_714 = 0L;
            int16_t l_761 = (-1L);
            int64_t **l_835 = &l_336[3];
            int8_t **l_845[7][3] = {{(void*)0,&g_20,(void*)0},{&g_20,(void*)0,(void*)0},{(void*)0,(void*)0,&g_20},{&g_20,&g_20,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_20,&g_20,&g_20},{(void*)0,(void*)0,(void*)0}};
            float **l_877 = &g_592;
            int i, j;
            for (i = 0; i < 1; i++)
                l_677[i] = &g_587;
            if (((*l_323) = 0L))
            { /* block id: 265 */
                int32_t l_636 = (-6L);
                int8_t **l_672 = &g_20;
                int16_t *l_676 = &g_91.f0;
                int32_t l_710[2];
                int32_t l_762 = (-1L);
                int i;
                for (i = 0; i < 2; i++)
                    l_710[i] = (-2L);
                (*l_323) = ((safe_sub_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_u(g_470, l_370[0][0])) <= ((safe_rshift_func_int8_t_s_s((0x1E8BL > (((safe_lshift_func_uint16_t_u_u(g_35, 0)) , (l_630 = (l_328 |= g_158))) != (((safe_lshift_func_uint8_t_u_u(((~((safe_lshift_func_int8_t_s_u((((l_640 = ((l_636 != ((((l_637[4][0] , ((safe_div_func_float_f_f((**g_591), (*g_592))) , &l_424[1])) != (void*)0) ^ 8UL) || p_57)) <= l_636)) != g_261) , 0xAFL), g_260)) | l_637[2][0])) ^ g_225), g_485.f0)) || g_138) != g_326[2]))), p_57)) != l_637[4][0])), 0x5EA57A1EL)) != g_329[8]);
                if ((*g_501))
                { /* block id: 270 */
                    int32_t l_643 = 3L;
                    int16_t * const l_652 = (void*)0;
                    uint8_t *l_655[10] = {&l_373,&l_373,&l_373,&l_373,&l_373,&l_373,&l_373,&l_373,&l_373,&l_373};
                    uint64_t l_693[9][6][4] = {{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL}},{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL}},{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL}},{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL}},{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL}},{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL}},{{1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL},{0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL},{1UL,1UL,0UL,1UL},{1UL,0UL,0UL,0x389BD78D6FBD9EF4LL},{0UL,0x389BD78D6FBD9EF4LL,0UL,0UL},{0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL}},{{0x389BD78D6FBD9EF4LL,0UL,0UL,0x389BD78D6FBD9EF4LL},{0UL,0x389BD78D6FBD9EF4LL,0UL,0UL},{0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL},{0x389BD78D6FBD9EF4LL,0UL,0UL,0x389BD78D6FBD9EF4LL},{0UL,0x389BD78D6FBD9EF4LL,0UL,0UL},{0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL}},{{0x389BD78D6FBD9EF4LL,0UL,0UL,0x389BD78D6FBD9EF4LL},{0UL,0x389BD78D6FBD9EF4LL,0UL,0UL},{0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL},{0x389BD78D6FBD9EF4LL,0UL,0UL,0x389BD78D6FBD9EF4LL},{0UL,0x389BD78D6FBD9EF4LL,0UL,0UL},{0x389BD78D6FBD9EF4LL,0x389BD78D6FBD9EF4LL,1UL,0x389BD78D6FBD9EF4LL}}};
                    int32_t l_711 = 0L;
                    float *l_765 = (void*)0;
                    int8_t l_766 = 0xB0L;
                    int i, j, k;
                    if ((((((((void*)0 != l_641[1][8]) || p_57) == ((l_643 | (safe_mod_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((g_82 = (((((safe_div_func_float_f_f((safe_sub_func_float_f_f((((l_652 == (((0x310CD9B9L == (safe_lshift_func_uint8_t_u_u((g_195++), (((((*g_20) , (safe_add_func_int64_t_s_s((g_550 = (g_673 ^= (((((((((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s((((*l_419) |= ((*g_501) = (safe_lshift_func_int16_t_s_u(((safe_mul_func_uint8_t_u_u((g_671 &= ((((g_131 = (safe_unary_minus_func_uint8_t_u(g_35))) || (safe_lshift_func_int16_t_s_u(l_643, 15))) || l_590) > p_57)), 0x72L)) || 0x63DAL), g_90.f3)))) == p_57), 0)), l_609)) || p_57) & 0UL) , 4294967295UL) , l_672) == &g_20) < p_57) , (void*)0) != (void*)0))), l_637[4][0]))) | g_133) & g_326[6]) , g_675[1][0][1])))) || p_57) , l_676)) , l_643) , p_57), 0x8.C17038p-9)), p_57)) >= (*g_592)) , (*g_592)) , &g_587) != l_677[0])), g_324)), l_643))) ^ 0x5F4F89C629497943LL)) , p_57) < p_57) || 0xCCL))
                    { /* block id: 279 */
                        uint16_t l_682 = 0x259AL;
                        (*g_501) = (safe_rshift_func_int16_t_s_u(((safe_mul_func_int16_t_s_s(((((l_682--) , ((safe_mod_func_int64_t_s_s((safe_lshift_func_int8_t_s_u(p_57, ((p_57 && (l_689 != l_676)) && 1L))), (safe_lshift_func_int8_t_s_s(((*l_419) = (~(-1L))), (l_640 = l_693[1][0][2]))))) & ((*l_323) = (safe_add_func_uint32_t_u_u((safe_mod_func_int8_t_s_s(((g_195 = (l_369 = (l_693[1][0][2] <= p_57))) == 0x31L), p_57)), 4294967287UL))))) | (*g_20)) == 0x2172L), g_324)) & g_485.f4), 14));
                        (*l_323) |= l_636;
                    }
                    else
                    { /* block id: 288 */
                        int32_t * const l_702[6][2][5] = {{{&l_370[0][1],&l_319,&l_319,&l_370[0][1],&l_319},{&l_370[0][1],&l_370[0][1],&g_38[2],&l_370[0][1],&l_370[0][1]}},{{&l_319,&l_370[0][1],&l_319,&l_319,&l_370[0][1]},{&l_370[0][1],&l_319,&l_319,&l_370[0][1],&l_319}},{{&l_370[0][1],&l_370[0][1],&g_38[2],&l_370[0][1],&l_370[0][1]},{&l_319,&l_370[0][1],&l_319,&l_319,&l_370[0][1]}},{{&l_370[0][1],&l_319,&l_319,&l_370[0][1],&l_319},{&l_370[0][1],&l_370[0][1],&g_38[2],&l_370[0][1],&l_370[0][1]}},{{&l_319,&l_370[0][1],&l_319,&l_319,&l_370[0][1]},{&l_370[0][1],&l_319,&l_319,&l_370[0][1],&l_319}},{{&l_370[0][1],&l_370[0][1],&g_38[2],&l_370[0][1],&l_370[0][1]},{&l_319,&l_370[0][1],&l_319,&l_319,&l_370[0][1]}}};
                        int8_t *l_709 = &l_511;
                        int32_t **l_718 = &l_419;
                        int32_t **l_719[8][5][6] = {{{&g_501,&l_321,(void*)0,&g_501,&l_321,&l_321},{&l_323,(void*)0,(void*)0,&l_323,&g_501,&g_501},{&l_323,&g_501,&l_321,&g_501,&l_321,&l_321},{&l_323,&g_501,&g_501,&l_321,&l_321,&g_501},{&l_323,&g_501,&l_323,&l_323,&g_501,&l_321}},{{(void*)0,(void*)0,(void*)0,&l_323,&l_321,&g_501},{&l_323,&l_321,&l_323,&g_501,&g_501,(void*)0},{(void*)0,&l_323,(void*)0,&l_321,&l_321,&l_323},{&g_501,&l_323,&g_501,&l_323,&l_323,(void*)0},{&l_323,&l_323,&g_501,(void*)0,(void*)0,&l_323}},{{(void*)0,&l_323,&l_323,&l_321,&l_321,&l_321},{&l_323,&l_321,&l_321,&l_321,&l_323,&l_323},{&l_321,&l_323,(void*)0,&g_501,(void*)0,(void*)0},{&l_321,&l_323,&g_501,&l_321,&g_501,&g_501},{&l_321,(void*)0,&g_501,(void*)0,&l_321,&l_323}},{{&l_323,(void*)0,&l_321,&l_323,&g_501,&l_321},{&l_323,&g_501,&l_321,(void*)0,&l_321,&l_321},{&g_501,&l_321,&l_321,&l_323,&g_501,&l_323},{&l_321,&l_321,&g_501,&l_321,&l_321,&g_501},{&g_501,(void*)0,&g_501,&l_321,&g_501,(void*)0}},{{&g_501,&l_321,(void*)0,(void*)0,&l_323,&l_323},{(void*)0,(void*)0,&l_323,&l_323,&l_323,&l_321},{&l_323,&l_323,&l_321,&g_501,&l_323,&l_323},{&l_323,&g_501,(void*)0,&g_501,(void*)0,&l_323},{(void*)0,&l_323,(void*)0,&l_321,&g_501,&l_321}},{{&l_323,&l_321,&l_323,(void*)0,(void*)0,&g_501},{&l_321,(void*)0,&g_501,(void*)0,&g_501,&l_321},{(void*)0,&g_501,&l_321,&l_321,&g_501,(void*)0},{&g_501,&l_321,&l_323,(void*)0,&l_321,(void*)0},{(void*)0,&l_323,&g_501,&g_501,&l_323,&l_323}},{{(void*)0,&l_323,&g_501,(void*)0,&l_323,&g_501},{&g_501,&l_321,&g_501,&l_321,&g_501,&l_321},{(void*)0,&l_321,&l_323,(void*)0,(void*)0,&l_323},{&l_321,&g_501,&l_323,(void*)0,&g_501,&l_323},{&l_323,&l_323,&g_501,&l_321,&g_501,&g_501}},{{(void*)0,&g_501,&l_321,&g_501,&g_501,&g_501},{&l_323,&g_501,&l_323,&g_501,&l_321,&g_501},{&l_323,&l_323,&g_501,&l_323,&l_323,&l_323},{(void*)0,&l_323,(void*)0,(void*)0,&l_321,&g_501},{&g_501,(void*)0,&l_321,&l_321,&l_323,&g_501}}};
                        int i, j, k;
                        (*l_323) = (safe_lshift_func_uint16_t_u_s((l_357[3] != (*l_468)), ((p_57 == (l_710[1] = (((((void*)0 != l_702[2][0][2]) < (safe_sub_func_int8_t_s_s(((safe_div_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(0x616DL, l_319)), ((l_693[1][0][2] , ((void*)0 != l_709)) , 0x16B7C64A19E6D4C1LL))) != 0xD64B8B38DFB0CFC8LL), g_35))) >= 0UL) , l_619))) > 0x4FL)));
                        --g_715[4][0][1];
                        g_501 = ((*l_718) = &g_123);
                    }
                    for (g_133 = 4; (g_133 >= 0); g_133 -= 1)
                    { /* block id: 297 */
                        const uint32_t l_752 = 0x32680FABL;
                        int32_t *l_767 = (void*)0;
                        int32_t *l_768 = &l_710[1];
                        int32_t *l_769 = &l_368;
                        int32_t *l_770 = &l_710[1];
                        int32_t *l_771 = &l_372[0][8];
                        int32_t *l_772 = &l_640;
                        int32_t *l_773 = (void*)0;
                        int32_t *l_774[3][4] = {{&l_371,&l_371,&l_371,&l_371},{&l_371,&l_371,&l_371,&l_371},{&l_371,&l_371,&l_371,&l_371}};
                        int i, j;
                        (*l_419) = (safe_mod_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u((((safe_add_func_int8_t_s_s((g_82 &= (safe_lshift_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(0x12C17E97L, (safe_div_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((safe_add_func_uint32_t_u_u(((g_84[g_133] | (0L <= (safe_rshift_func_int8_t_s_u((*g_20), (((((*l_413) &= (safe_lshift_func_uint16_t_u_s((safe_div_func_uint8_t_u_u((g_715[6][1][2] = ((safe_div_func_uint8_t_u_u(((p_57 < ((safe_rshift_func_int16_t_s_u((safe_div_func_int32_t_s_s(((safe_mod_func_uint8_t_u_u((0x75B5L != (l_710[1] , (0x3A5E380BL || (((((*l_335) ^= 0x70B43B126E887D22LL) && p_57) <= 65535UL) && 0x80A5L)))), 0x7AL)) > p_57), 0x169A830EL)), 8)) != l_752)) | (*g_20)), 0xB4L)) | 0x6973L)), 0x06L)), 11))) != p_57) || (-1L)) , g_90.f2))))) && p_57), (*l_323))), p_57)), l_368)))), 3))), (*l_419))) , 0x389DE9CCA23263F1LL) && p_57), 0)), p_57));
                        l_712 &= (safe_div_func_uint64_t_u_u(l_711, (safe_mod_func_int8_t_s_s((p_57 > ((l_762 = (l_761 = ((safe_rshift_func_uint16_t_u_u(0x2666L, 0)) < (safe_mul_func_int8_t_s_s((&g_84[g_133] == l_689), 0x37L))))) == ((g_600 , (safe_add_func_int16_t_s_s((((((l_765 == (*g_591)) < g_327) , l_328) , l_766) < l_370[0][0]), 1UL))) <= 0xD9L))), p_57))));
                        l_711 = l_710[1];
                        ++g_775;
                    }
                }
                else
                { /* block id: 309 */
                    int16_t l_778 = 1L;
                    int8_t * const *l_783 = &l_334[3];
                    int8_t * const **l_784 = (void*)0;
                    int8_t * const **l_785[7][6] = {{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783},{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783},{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783},{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783},{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783},{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783},{&l_783,&l_783,&l_783,&l_783,&l_783,&l_783}};
                    int16_t l_798[1][4] = {{(-8L),(-8L),(-8L),(-8L)}};
                    int32_t *l_810 = &l_370[0][2];
                    int32_t *l_811 = (void*)0;
                    int32_t *l_812 = &l_762;
                    int32_t *l_813 = &l_640;
                    int32_t *l_814 = &l_762;
                    int32_t *l_815 = &l_365;
                    int32_t *l_816 = (void*)0;
                    int32_t *l_817 = (void*)0;
                    int32_t *l_818 = &l_762;
                    int32_t *l_819 = &l_368;
                    int32_t *l_820[2];
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_820[i] = &l_366;
                    if (((*l_323) = (((l_778 >= (((**g_591) , ((safe_sub_func_uint64_t_u_u((*l_419), (safe_sub_func_int8_t_s_s((((g_786 = l_783) != (((safe_mod_func_uint64_t_u_u(((p_57 == ((l_710[1] |= 0x14251D7FB2EAC16BLL) < (((*g_501) |= (safe_mod_func_int32_t_s_s(p_57, 4294967289UL))) <= (&g_480 == (void*)0)))) == l_778), (-8L))) , 1L) , l_797)) ^ p_57), 6UL)))) <= l_636)) == (-1L))) >= l_778) ^ l_373)))
                    { /* block id: 314 */
                        (*g_592) = p_57;
                        if (l_798[0][3])
                            continue;
                        if (l_799)
                            break;
                    }
                    else
                    { /* block id: 318 */
                        int64_t l_804 = 0x5EA5EEC752383E9CLL;
                        uint32_t *l_805 = &l_353;
                        uint32_t **l_806 = &l_413;
                        (*g_592) = (safe_sub_func_float_f_f((safe_sub_func_float_f_f(l_804, ((l_762 > (l_642 == ((*l_806) = l_805))) <= (((safe_add_func_float_f_f(l_804, (-0x1.Dp+1))) >= (((void*)0 != &l_353) == (l_809 != (void*)0))) > l_778)))), (**g_591)));
                    }
                    (*g_501) |= (l_712 |= (-1L));
                    g_821--;
                }
            }
            else
            { /* block id: 326 */
                uint16_t l_827[9] = {0xBB54L,0xBB54L,0xBB54L,0xBB54L,0xBB54L,0xBB54L,0xBB54L,0xBB54L,0xBB54L};
                int32_t l_833[3];
                int64_t *l_852 = &g_329[5];
                int i;
                for (i = 0; i < 3; i++)
                    l_833[i] = (-10L);
                l_419 = &g_38[0];
                for (l_369 = 7; (l_369 >= 0); l_369 -= 1)
                { /* block id: 330 */
                    int32_t **l_824 = &g_501;
                    int32_t *l_825 = &l_368;
                    int32_t *l_826[10][9] = {{&l_368,&g_123,&l_371,(void*)0,&l_371,&g_123,&l_368,&l_328,(void*)0},{&l_365,(void*)0,&l_368,&g_159,&l_328,&g_159,&l_368,(void*)0,&l_365},{&g_123,&g_159,&l_365,&l_328,&l_372[0][8],&l_371,&l_372[0][8],&l_328,&l_365},{&l_372[0][8],&l_372[0][8],&g_123,&l_330,(void*)0,(void*)0,&l_365,(void*)0,(void*)0},{&g_123,&l_372[0][8],&l_372[0][8],&g_123,&l_330,(void*)0,(void*)0,&l_365,(void*)0},{&l_365,&g_159,&g_123,&g_123,&g_159,&l_365,&l_328,&l_372[0][8],&l_371},{&l_368,(void*)0,&l_365,&l_330,&l_330,&l_365,(void*)0,&l_368,&g_159},{&l_371,&g_123,&l_368,&l_328,(void*)0,(void*)0,&l_328,&l_368,&g_123},{&l_330,&l_368,&l_371,&g_159,&l_372[0][8],(void*)0,(void*)0,&l_372[0][8],&g_159},{&l_330,&l_328,&l_330,(void*)0,&l_328,&l_371,&l_365,&l_365,&l_371}};
                    int64_t *l_834 = &g_329[2];
                    int i, j;
                    (*l_824) = &l_369;
                    ++l_827[1];
                    for (l_511 = 0; (l_511 <= 7); l_511 += 1)
                    { /* block id: 335 */
                        uint8_t *l_832[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_832[i] = &g_715[4][0][1];
                        if (g_788[l_511])
                            break;
                        (*l_323) |= (safe_lshift_func_uint8_t_u_s((g_788[l_511] >= (g_788[l_511] >= ((l_833[2] = (g_195 |= g_713[5])) , (p_57 ^ (p_57 & ((g_485.f0 , l_834) == l_416[7])))))), (g_91.f3 ^ 7UL)));
                    }
                    for (g_114 = 0; (g_114 <= 0); g_114 += 1)
                    { /* block id: 343 */
                        int64_t ***l_836 = &l_835;
                        int i, j;
                        l_370[g_114][g_114] = 1L;
                        (*l_836) = l_835;
                    }
                }
                for (g_121 = 0; (g_121 <= 7); g_121 += 1)
                { /* block id: 350 */
                    float *l_850 = &g_674;
                    float *l_851 = &g_135;
                    uint64_t **l_855 = &l_416[2];
                    uint8_t *l_857[5][6][2] = {{{&l_373,&g_775},{&g_775,&g_775},{&g_775,&g_775},{&l_373,&g_775},{&g_775,&g_775},{&g_715[0][1][0],&g_715[0][1][0]}},{{&l_373,&g_715[0][1][0]},{&g_715[0][1][0],&g_775},{&g_775,&g_775},{&l_373,&g_775},{&g_775,&g_775},{&g_775,&g_775}},{{&l_373,&g_775},{&g_775,&g_775},{&g_715[0][1][0],&g_715[0][1][0]},{&l_373,&g_715[0][1][0]},{&g_715[0][1][0],&g_775},{&g_775,&g_775}},{{&l_373,&g_775},{&g_775,&g_775},{&g_775,&g_775},{&l_373,&g_775},{&g_775,&g_775},{&g_715[0][1][0],&g_715[0][1][0]}},{{&l_373,&g_715[0][1][0]},{&g_715[0][1][0],&g_775},{&g_775,&g_775},{&l_373,&g_775},{&g_139,&g_139},{&g_139,&g_775}}};
                    uint64_t *l_858 = (void*)0;
                    uint64_t *l_859 = &g_35;
                    int32_t **l_875 = (void*)0;
                    int32_t **l_876 = &l_321;
                    int32_t **l_878[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j, k;
                    (**g_591) = l_637[2][0];
                    (*l_851) = (safe_div_func_float_f_f(((*g_592) = 0x0.3p-1), ((safe_add_func_float_f_f((((safe_div_func_float_f_f((&l_417 != (void*)0), (safe_sub_func_float_f_f((((l_797 != l_845[1][0]) >= p_57) > (((*l_850) = (((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(0x66L, (l_369 = ((((0x0DC3L & ((*l_323) && 5L)) || l_833[2]) <= p_57) == l_833[2])))), g_675[1][0][1])) >= (-1L)) , 0xB.D237CEp-70)) <= p_57)), p_57)))) > p_57) > p_57), g_485.f4)) <= (*l_323))));
                    g_501 = (((((&g_550 == ((*l_835) = l_852)) , (safe_lshift_func_uint8_t_u_u(((p_57 < ((g_34[g_121][g_139] = &g_35) != ((*l_855) = &l_331))) , (l_833[2] = g_856)), (((*l_859) = 0x20EC36263135BEC9LL) <= (g_862 = (l_860 != ((safe_add_func_int64_t_s_s(((g_195 = (safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((((safe_mod_func_uint64_t_u_u(((((((((safe_add_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((((((*l_876) = &l_368) == (void*)0) ^ l_827[6]) < g_91.f1), p_57)), p_57)) & l_630) && g_470) , 65529UL) != g_91.f3) <= g_675[1][0][1]) <= (*l_419)) && l_712), (-6L))) , (*l_797)) == (void*)0) | p_57) < (*l_419)), p_57)), p_57))) , g_165[0][1]), l_640)) , &l_859))))))) , l_877) != (void*)0) , (void*)0);
                }
            }
            for (l_609 = 0; (l_609 <= 9); l_609 += 1)
            { /* block id: 369 */
                int32_t *l_880[10] = {&l_328,&l_328,&l_328,&l_328,&l_328,&l_328,&l_328,&l_328,&l_328,&l_328};
                int32_t ***l_881 = &g_481;
                int i;
                for (l_450 = 9; (l_450 >= 1); l_450 -= 1)
                { /* block id: 372 */
                    l_879 = &l_366;
                    for (l_368 = 2; (l_368 >= 0); l_368 -= 1)
                    { /* block id: 376 */
                        l_880[1] = &l_372[0][1];
                    }
                }
                (*l_881) = &g_501;
            }
            for (l_714 = (-15); (l_714 >= (-11)); l_714++)
            { /* block id: 384 */
                uint64_t l_884 = 18446744073709551613UL;
                l_884--;
            }
            return (*l_797);
        }
    }
    return (*l_797);
}


/* ------------------------------------------ */
/* 
 * reads : g_195 g_94 g_84 g_91.f4 g_20 g_21 g_159 g_123 g_114 g_90.f1 g_193 g_11 g_91.f3 g_38 g_91.f1 g_166 g_82 g_91.f2 g_261 g_35 g_90.f2 g_297
 * writes: g_195 g_159 g_123 g_193 g_114 g_135 g_94 g_254 g_131 g_139 g_91.f2 g_261 g_133 g_158 g_297
 */
static uint16_t  func_60(float * p_61, int8_t * p_62, uint16_t  p_63)
{ /* block id: 69 */
    int32_t *l_191 = &g_123;
    int32_t *l_192[2][7] = {{&g_38[3],(void*)0,&g_123,(void*)0,&g_38[3],&g_38[3],(void*)0},{(void*)0,&g_38[3],(void*)0,(void*)0,(void*)0,(void*)0,&g_38[3]}};
    int64_t l_258 = 1L;
    uint32_t l_301 = 0UL;
    int i, j;
lbl_266:
    --g_195;
lbl_300:
    if ((l_192[1][2] == l_192[1][0]))
    { /* block id: 71 */
        uint8_t l_204 = 0x89L;
        int32_t l_205 = 0x3CF96F6EL;
        (*l_191) = 0L;
        (*l_191) = ((p_63 ^ ((*p_61) , (safe_sub_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((((((void*)0 != p_62) <= (((safe_mod_func_int16_t_s_s((((l_204 &= g_84[2]) >= (0xA3A7L && l_205)) ^ (p_61 != l_192[1][3])), l_205)) , l_204) , g_91.f4)) ^ l_205) ^ l_205), (*g_20))), 0x0CF3L)))) | (-1L));
        return (*l_191);
    }
    else
    { /* block id: 76 */
        int16_t *l_212[7] = {&g_131,&g_131,&g_131,&g_131,&g_131,&g_131,&g_131};
        int32_t l_216 = 3L;
        int32_t l_294 = (-1L);
        int i;
        for (p_63 = (-1); (p_63 < 58); p_63++)
        { /* block id: 79 */
            int16_t **l_213 = (void*)0;
            int16_t **l_214 = &l_212[1];
            int16_t *l_215 = &g_131;
            const int32_t *l_224 = &g_225;
            const int32_t **l_223 = &l_224;
            int64_t *l_226 = &g_193;
            int32_t l_227 = 1L;
            int32_t *l_231 = &l_216;
            uint8_t l_277[8];
            int32_t l_293 = 0L;
            int32_t l_296[7] = {0xCB991EF0L,0xCB991EF0L,0xCB991EF0L,0xCB991EF0L,0xCB991EF0L,0xCB991EF0L,0xCB991EF0L};
            int i;
            for (i = 0; i < 8; i++)
                l_277[i] = 0x6FL;
            if ((safe_rshift_func_int16_t_s_s(((safe_add_func_uint16_t_u_u(((6L | (((((((*l_214) = l_212[1]) != (g_114 , l_215)) != (((((*l_226) |= ((l_216 , (((safe_mod_func_int64_t_s_s((safe_add_func_int32_t_s_s(((*l_191) = (safe_div_func_int32_t_s_s(((p_63 , ((*l_223) = p_61)) != p_61), p_63))), g_90.f1)), 0xEDA7472D96CCF5C8LL)) || (*g_20)) ^ l_216)) || (*l_191))) <= l_227) != g_11[0][0][4]) && 0x02L)) == 1L) ^ l_216) != p_63)) <= 0x0E3EL), 0x5311L)) ^ p_63), p_63)))
            { /* block id: 84 */
                int32_t **l_228 = (void*)0;
                int32_t **l_229 = (void*)0;
                int32_t **l_230[2][4][8] = {{{(void*)0,&l_192[0][3],&l_191,&l_191,&l_192[1][3],&l_192[0][1],(void*)0,&l_192[1][0]},{(void*)0,&l_191,&l_191,&l_191,&l_192[0][0],&l_192[1][0],&l_191,&l_191},{&l_192[1][3],&l_192[0][4],&l_192[1][4],&l_192[1][4],&l_192[0][4],&l_192[1][3],(void*)0,&l_191},{&l_192[1][0],&l_192[0][0],&l_191,&l_191,&l_191,(void*)0,&l_192[0][0],&l_192[1][0]}},{{&l_192[0][1],&l_192[1][3],&l_191,&l_191,&l_192[0][3],(void*)0,&l_191,&l_191},{(void*)0,&l_192[0][3],&l_192[1][0],&l_192[1][4],&l_192[1][0],&l_192[0][3],(void*)0,&l_191},{&l_192[1][0],&l_191,&l_191,&l_191,(void*)0,&l_191,&l_191,&l_192[1][0]},{&l_192[1][3],(void*)0,(void*)0,&l_191,(void*)0,&l_192[1][3],&l_192[0][4],&l_192[1][4]}}};
                int32_t **l_255 = (void*)0;
                int i, j, k;
                l_231 = &g_38[3];
                for (g_114 = 0; (g_114 <= 39); ++g_114)
                { /* block id: 88 */
                    float *l_249 = &g_135;
                    int32_t l_253 = 0L;
                    uint8_t *l_256 = &g_195;
                    uint8_t *l_257 = &g_139;
                    int32_t l_259 = 0x52DD46F2L;
                    g_91.f2 &= (((+((safe_mul_func_uint8_t_u_u((l_216 = ((*l_257) = (+(((*l_256) = (safe_mul_func_int8_t_s_s(((((safe_unary_minus_func_uint8_t_u(p_63)) == (((((safe_sub_func_int16_t_s_s(((*l_215) = (g_254 = (safe_mod_func_uint64_t_u_u((safe_rshift_func_int16_t_s_s((safe_add_func_uint8_t_u_u(g_91.f3, ((((*p_61) = ((*p_61) < (((*l_249) = (l_216 , g_159)) > 0xD.9D4DF7p-41))) >= (((safe_sub_func_int64_t_s_s((((((-10L) < 1UL) != (!l_253)) >= 0x7D7F2AF5L) , l_253), (*l_231))) , l_212[1]) != (void*)0)) , (*g_20)))), 15)), p_63)))), p_63)) , (void*)0) == l_255) , &g_84[4]) != (void*)0)) & g_91.f1) ^ g_166), (*p_62)))) | (*l_231))))), (*p_62))) < 0x39D34687L)) != 0UL) & p_63);
                    g_261++;
                }
                for (g_139 = 9; (g_139 < 54); ++g_139)
                { /* block id: 101 */
                    return g_11[0][3][0];
                }
            }
            else
            { /* block id: 104 */
                int32_t *l_273 = &g_159;
                int32_t **l_274 = &l_191;
                if (g_91.f2)
                    goto lbl_266;
                (*l_274) = ((safe_add_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u((safe_lshift_func_int8_t_s_u(0x5AL, 3)), 0)), (-10L))) , l_273);
                if (g_35)
                    continue;
                return g_195;
            }
            if (g_91.f1)
                goto lbl_300;
            for (g_133 = (-17); (g_133 >= 27); g_133 = safe_add_func_uint16_t_u_u(g_133, 3))
            { /* block id: 112 */
                int32_t l_288[10] = {0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL,0xAF4A9DEEL};
                int64_t l_290 = 0xFE23A00BB90F556ALL;
                int32_t l_292 = 8L;
                int i;
                l_277[3]++;
                for (g_158 = 16; (g_158 == 16); ++g_158)
                { /* block id: 116 */
                    uint32_t l_285 = 0xA4BAC16EL;
                    int32_t l_291[3][5][8] = {{{(-4L),(-4L),1L,0L,0x27EF45F2L,0xBC7DE2BDL,0x47C74D67L,0xBC7DE2BDL},{0xB19304D9L,9L,0L,9L,0xB19304D9L,0x7BAF9C4BL,(-4L),0xBC7DE2BDL},{9L,0x27EF45F2L,0x47C74D67L,0L,0L,0x47C74D67L,0x27EF45F2L,9L},{1L,0x7BAF9C4BL,0x47C74D67L,7L,(-4L),0xB19304D9L,(-4L),7L},{0L,0xD4783017L,0L,0xBC7DE2BDL,7L,0xB19304D9L,0x47C74D67L,0x47C74D67L}},{{0x47C74D67L,0x7BAF9C4BL,1L,1L,0x7BAF9C4BL,0x47C74D67L,7L,(-4L)},{0x47C74D67L,0x27EF45F2L,9L,0x7BAF9C4BL,7L,0x7BAF9C4BL,9L,0x27EF45F2L},{0L,9L,0xB19304D9L,0x7BAF9C4BL,(-4L),0xBC7DE2BDL,0xBC7DE2BDL,(-4L)},{1L,(-4L),(-4L),(-4L),0x7BAF9C4BL,0xB19304D9L,9L,0L},{0xD4783017L,(-4L),0x47C74D67L,9L,0x47C74D67L,(-4L),0xD4783017L,0x27EF45F2L}},{{0x47C74D67L,(-4L),0xD4783017L,0x27EF45F2L,0xB19304D9L,0xB19304D9L,0x27EF45F2L,0xD4783017L},{0xBC7DE2BDL,0xBC7DE2BDL,(-4L),0x7BAF9C4BL,0xB19304D9L,9L,0L,9L},{0x47C74D67L,0xD4783017L,0x7BAF9C4BL,0xD4783017L,0x47C74D67L,1L,0xBC7DE2BDL,9L},{0xD4783017L,0xB19304D9L,0L,0x7BAF9C4BL,0x7BAF9C4BL,0L,0xB19304D9L,0xD4783017L},{(-4L),1L,0L,0x27EF45F2L,0xBC7DE2BDL,0x47C74D67L,0xBC7DE2BDL,0x27EF45F2L}}};
                    int i, j, k;
                    for (g_139 = 0; (g_139 >= 57); g_139 = safe_add_func_uint16_t_u_u(g_139, 3))
                    { /* block id: 119 */
                        float l_284 = 0x6.1240B9p+94;
                        int32_t l_289[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_289[i] = (-8L);
                        l_285++;
                        if (g_90.f2)
                            break;
                        g_297--;
                    }
                    if (l_292)
                        break;
                }
                if (g_91.f4)
                    goto lbl_300;
            }
            if (l_294)
                continue;
        }
    }
    return l_301;
}


/* ------------------------------------------ */
/* 
 * reads : g_82 g_84 g_91.f0 g_21 g_38 g_90.f2 g_91.f2 g_114 g_35 g_139 g_11 g_167 g_91.f3
 * writes: g_82 g_94 g_91.f0 g_90.f0 g_90.f2 g_112 g_114 g_35 g_121 g_139 g_167 g_188 g_123
 */
static uint32_t  func_64(int8_t  p_65, int32_t * const  p_66, int8_t  p_67, int16_t  p_68)
{ /* block id: 20 */
    int32_t *l_96 = &g_38[7];
    int32_t **l_95 = &l_96;
    uint8_t l_111 = 0xA0L;
    int32_t l_128 = 0x2C58D3A2L;
    int32_t l_132 = 1L;
    int32_t l_134 = (-1L);
    int32_t l_137[6];
    int i;
    for (i = 0; i < 6; i++)
        l_137[i] = (-2L);
    for (g_82 = 0; (g_82 <= 4); g_82 += 1)
    { /* block id: 23 */
        int32_t l_86[8] = {0x2F3D1A1AL,0x2F3D1A1AL,0x2F3D1A1AL,0x2F3D1A1AL,0x2F3D1A1AL,0x2F3D1A1AL,0x2F3D1A1AL,0x2F3D1A1AL};
        struct S0 **l_87 = (void*)0;
        const struct S0 *l_89[5] = {&g_90,&g_90,&g_90,&g_90,&g_90};
        const struct S0 **l_88 = &l_89[1];
        float *l_92 = (void*)0;
        float *l_93 = &g_94;
        int i;
        l_86[2] = g_84[g_82];
        (*l_88) = (void*)0;
        if (g_82)
            goto lbl_97;
        (*l_93) = (-0x8.8p-1);
    }
lbl_97:
    (*l_95) = p_66;
    for (g_91.f0 = 4; (g_91.f0 >= 0); g_91.f0 -= 1)
    { /* block id: 32 */
        int16_t *l_108[1];
        int8_t l_113 = 0L;
        int32_t l_130 = 8L;
        int32_t l_136 = 0xC398B08BL;
        float *l_157 = &g_94;
        int32_t l_164 = 0L;
        int i;
        for (i = 0; i < 1; i++)
            l_108[i] = (void*)0;
        g_114 |= (((g_84[g_91.f0] == p_67) & (safe_add_func_uint8_t_u_u((((g_112 = (((((safe_mul_func_uint16_t_u_u(((-7L) ^ (p_65 >= (safe_mul_func_int8_t_s_s((-4L), ((safe_lshift_func_int16_t_s_u((g_90.f0 = (safe_mod_func_int16_t_s_s(g_84[g_91.f0], p_65))), g_21)) == ((safe_mul_func_int16_t_s_s(((g_90.f2 &= ((((((*p_66) != 0x54A15197L) || g_84[g_91.f0]) & p_67) , (void*)0) != &g_38[6])) & 0L), p_68)) , 0xEF93L)))))), 4L)) && 0xE1E60B5F8E08A2E4LL) != g_84[g_91.f0]) > l_111) != 0x643AL)) < g_91.f2) , g_38[3]), p_67))) , l_113);
        if (p_65)
            goto lbl_97;
        for (g_82 = 0; (g_82 == (-22)); g_82 = safe_sub_func_uint8_t_u_u(g_82, 5))
        { /* block id: 40 */
            int32_t l_129 = (-1L);
            float *l_156 = &g_94;
            int32_t l_161 = 0x3201E628L;
            int32_t l_162 = (-1L);
            int16_t *l_187 = &g_131;
            (*l_95) = &g_38[3];
            for (g_35 = 0; (g_35 != 57); ++g_35)
            { /* block id: 44 */
                int32_t *l_119 = (void*)0;
                int32_t *l_120 = &g_121;
                int32_t *l_122 = &g_123;
                int32_t *l_124 = (void*)0;
                int32_t *l_125 = &g_123;
                int32_t *l_126 = &g_123;
                int32_t *l_127[9] = {&g_123,&g_38[6],&g_123,&g_38[6],&g_123,&g_38[6],&g_123,&g_38[6],&g_123};
                uint8_t *l_146 = &l_111;
                int32_t l_154 = 0x8166B7E9L;
                float l_155 = (-0x1.6p-1);
                int i;
                (*l_95) = (((*l_120) = p_65) , &g_38[4]);
                ++g_139;
                if (((((safe_sub_func_int64_t_s_s((safe_mul_func_int16_t_s_s(((l_129 == (((*l_146) = (*l_96)) <= 0x8FL)) ^ 255UL), (g_11[0][3][2] || (safe_mul_func_int8_t_s_s((+l_113), ((l_136 = (safe_lshift_func_uint16_t_u_u(((safe_div_func_uint64_t_u_u(p_67, p_65)) ^ (((((g_84[2] > l_129) , 1UL) | l_154) ^ g_139) == p_65)), 15))) | p_65)))))), l_130)) , l_156) != l_157) < 65535UL))
                { /* block id: 50 */
                    int16_t l_160[3];
                    int32_t l_163 = (-1L);
                    int16_t **l_184 = (void*)0;
                    int16_t **l_185 = (void*)0;
                    int16_t **l_186[7][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                    int32_t l_189 = 0x1B18042EL;
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_160[i] = 0xA290L;
                    ++g_167;
                    (*l_125) = ((safe_div_func_int32_t_s_s(((safe_sub_func_uint64_t_u_u((((safe_sub_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u((&l_163 != l_124), (&g_84[3] != (void*)0))), ((l_163 <= ((l_129 = (p_66 == (void*)0)) > (((l_189 = (safe_rshift_func_int8_t_s_s(((l_187 = &p_68) == (g_188 = (void*)0)), 4))) || g_91.f3) && 65532UL))) | l_160[0]))), 4)), (-1L))) != g_21) , l_162), p_68)) , 1L), p_65)) && 0x3DC7L);
                }
                else
                { /* block id: 57 */
                    (*l_95) = (void*)0;
                    for (l_113 = 0; (l_113 <= 0); l_113 += 1)
                    { /* block id: 61 */
                        int i;
                        return g_84[g_91.f0];
                    }
                }
            }
        }
    }
    return p_67;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc_bytes (&g_4, sizeof(g_4), "g_4", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_8[i][j], "g_8[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_11[i][j][k], "g_11[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_35, "g_35", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_38[i], "g_38[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_82, "g_82", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_84[i], "g_84[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_90.f0, "g_90.f0", print_hash_value);
    transparent_crc(g_90.f1, "g_90.f1", print_hash_value);
    transparent_crc(g_90.f2, "g_90.f2", print_hash_value);
    transparent_crc(g_90.f3, "g_90.f3", print_hash_value);
    transparent_crc(g_90.f4, "g_90.f4", print_hash_value);
    transparent_crc(g_90.f5, "g_90.f5", print_hash_value);
    transparent_crc(g_90.f6, "g_90.f6", print_hash_value);
    transparent_crc(g_91.f0, "g_91.f0", print_hash_value);
    transparent_crc(g_91.f1, "g_91.f1", print_hash_value);
    transparent_crc(g_91.f2, "g_91.f2", print_hash_value);
    transparent_crc(g_91.f3, "g_91.f3", print_hash_value);
    transparent_crc(g_91.f4, "g_91.f4", print_hash_value);
    transparent_crc(g_91.f5, "g_91.f5", print_hash_value);
    transparent_crc(g_91.f6, "g_91.f6", print_hash_value);
    transparent_crc_bytes (&g_94, sizeof(g_94), "g_94", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    transparent_crc(g_123, "g_123", print_hash_value);
    transparent_crc(g_131, "g_131", print_hash_value);
    transparent_crc(g_133, "g_133", print_hash_value);
    transparent_crc_bytes (&g_135, sizeof(g_135), "g_135", print_hash_value);
    transparent_crc(g_138, "g_138", print_hash_value);
    transparent_crc(g_139, "g_139", print_hash_value);
    transparent_crc(g_158, "g_158", print_hash_value);
    transparent_crc(g_159, "g_159", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_165[i][j], "g_165[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_166, "g_166", print_hash_value);
    transparent_crc(g_167, "g_167", print_hash_value);
    transparent_crc(g_193, "g_193", print_hash_value);
    transparent_crc_bytes (&g_194, sizeof(g_194), "g_194", print_hash_value);
    transparent_crc(g_195, "g_195", print_hash_value);
    transparent_crc(g_225, "g_225", print_hash_value);
    transparent_crc(g_254, "g_254", print_hash_value);
    transparent_crc(g_260, "g_260", print_hash_value);
    transparent_crc(g_261, "g_261", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_297, "g_297", print_hash_value);
    transparent_crc(g_324, "g_324", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_326[i], "g_326[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_327, "g_327", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_329[i], "g_329[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_470, "g_470", print_hash_value);
    transparent_crc(g_485.f0, "g_485.f0", print_hash_value);
    transparent_crc(g_485.f1, "g_485.f1", print_hash_value);
    transparent_crc(g_485.f2, "g_485.f2", print_hash_value);
    transparent_crc(g_485.f3, "g_485.f3", print_hash_value);
    transparent_crc(g_485.f4, "g_485.f4", print_hash_value);
    transparent_crc(g_485.f5, "g_485.f5", print_hash_value);
    transparent_crc(g_485.f6, "g_485.f6", print_hash_value);
    transparent_crc(g_550, "g_550", print_hash_value);
    transparent_crc(g_576, "g_576", print_hash_value);
    transparent_crc(g_600, "g_600", print_hash_value);
    transparent_crc(g_601, "g_601", print_hash_value);
    transparent_crc(g_671, "g_671", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc_bytes (&g_674, sizeof(g_674), "g_674", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_675[i][j][k], "g_675[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_713[i], "g_713[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_715[i][j][k], "g_715[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_775, "g_775", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_788[i], "g_788[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_821, "g_821", print_hash_value);
    transparent_crc(g_856, "g_856", print_hash_value);
    transparent_crc(g_862, "g_862", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_970[i][j][k].f0, "g_970[i][j][k].f0", print_hash_value);
                transparent_crc(g_970[i][j][k].f1, "g_970[i][j][k].f1", print_hash_value);
                transparent_crc(g_970[i][j][k].f2, "g_970[i][j][k].f2", print_hash_value);
                transparent_crc(g_970[i][j][k].f3, "g_970[i][j][k].f3", print_hash_value);
                transparent_crc(g_970[i][j][k].f4, "g_970[i][j][k].f4", print_hash_value);
                transparent_crc(g_970[i][j][k].f5, "g_970[i][j][k].f5", print_hash_value);
                transparent_crc(g_970[i][j][k].f6, "g_970[i][j][k].f6", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_973.f0, "g_973.f0", print_hash_value);
    transparent_crc(g_973.f1, "g_973.f1", print_hash_value);
    transparent_crc(g_973.f2, "g_973.f2", print_hash_value);
    transparent_crc(g_973.f3, "g_973.f3", print_hash_value);
    transparent_crc(g_973.f4, "g_973.f4", print_hash_value);
    transparent_crc(g_973.f5, "g_973.f5", print_hash_value);
    transparent_crc(g_973.f6, "g_973.f6", print_hash_value);
    transparent_crc(g_1016.f0, "g_1016.f0", print_hash_value);
    transparent_crc(g_1016.f1, "g_1016.f1", print_hash_value);
    transparent_crc(g_1016.f2, "g_1016.f2", print_hash_value);
    transparent_crc(g_1016.f3, "g_1016.f3", print_hash_value);
    transparent_crc(g_1016.f4, "g_1016.f4", print_hash_value);
    transparent_crc(g_1016.f5, "g_1016.f5", print_hash_value);
    transparent_crc(g_1016.f6, "g_1016.f6", print_hash_value);
    transparent_crc_bytes (&g_1058, sizeof(g_1058), "g_1058", print_hash_value);
    transparent_crc(g_1219, "g_1219", print_hash_value);
    transparent_crc_bytes (&g_1235, sizeof(g_1235), "g_1235", print_hash_value);
    transparent_crc(g_1591, "g_1591", print_hash_value);
    transparent_crc(g_1646, "g_1646", print_hash_value);
    transparent_crc_bytes (&g_1756, sizeof(g_1756), "g_1756", print_hash_value);
    transparent_crc(g_1784, "g_1784", print_hash_value);
    transparent_crc(g_1791, "g_1791", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_1894[i], sizeof(g_1894[i]), "g_1894[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2208.f0, "g_2208.f0", print_hash_value);
    transparent_crc(g_2208.f1, "g_2208.f1", print_hash_value);
    transparent_crc(g_2208.f2, "g_2208.f2", print_hash_value);
    transparent_crc(g_2208.f3, "g_2208.f3", print_hash_value);
    transparent_crc(g_2208.f4, "g_2208.f4", print_hash_value);
    transparent_crc(g_2208.f5, "g_2208.f5", print_hash_value);
    transparent_crc(g_2208.f6, "g_2208.f6", print_hash_value);
    transparent_crc(g_2343.f0, "g_2343.f0", print_hash_value);
    transparent_crc(g_2343.f1, "g_2343.f1", print_hash_value);
    transparent_crc(g_2343.f2, "g_2343.f2", print_hash_value);
    transparent_crc(g_2343.f3, "g_2343.f3", print_hash_value);
    transparent_crc(g_2343.f4, "g_2343.f4", print_hash_value);
    transparent_crc(g_2343.f5, "g_2343.f5", print_hash_value);
    transparent_crc(g_2343.f6, "g_2343.f6", print_hash_value);
    transparent_crc(g_2385.f0, "g_2385.f0", print_hash_value);
    transparent_crc(g_2385.f1, "g_2385.f1", print_hash_value);
    transparent_crc(g_2385.f2, "g_2385.f2", print_hash_value);
    transparent_crc(g_2385.f3, "g_2385.f3", print_hash_value);
    transparent_crc(g_2385.f4, "g_2385.f4", print_hash_value);
    transparent_crc(g_2385.f5, "g_2385.f5", print_hash_value);
    transparent_crc(g_2385.f6, "g_2385.f6", print_hash_value);
    transparent_crc(g_2423, "g_2423", print_hash_value);
    transparent_crc(g_2435.f0, "g_2435.f0", print_hash_value);
    transparent_crc(g_2435.f1, "g_2435.f1", print_hash_value);
    transparent_crc(g_2435.f2, "g_2435.f2", print_hash_value);
    transparent_crc(g_2435.f3, "g_2435.f3", print_hash_value);
    transparent_crc(g_2435.f4, "g_2435.f4", print_hash_value);
    transparent_crc(g_2435.f5, "g_2435.f5", print_hash_value);
    transparent_crc(g_2435.f6, "g_2435.f6", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2536[i][j], "g_2536[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2560, "g_2560", print_hash_value);
    transparent_crc(g_2619, "g_2619", print_hash_value);
    transparent_crc(g_2621, "g_2621", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 632
   depth: 1, occurrence: 4
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 6
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 18
breakdown:
   indirect level: 0, occurrence: 4
   indirect level: 1, occurrence: 6
   indirect level: 2, occurrence: 4
   indirect level: 3, occurrence: 2
   indirect level: 4, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 7
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 4
XXX times a single bitfield on LHS: 3
XXX times a single bitfield on RHS: 54

XXX max expression depth: 55
breakdown:
   depth: 1, occurrence: 316
   depth: 2, occurrence: 76
   depth: 3, occurrence: 7
   depth: 5, occurrence: 2
   depth: 6, occurrence: 3
   depth: 9, occurrence: 1
   depth: 11, occurrence: 3
   depth: 12, occurrence: 1
   depth: 13, occurrence: 4
   depth: 15, occurrence: 2
   depth: 16, occurrence: 2
   depth: 17, occurrence: 4
   depth: 18, occurrence: 2
   depth: 19, occurrence: 3
   depth: 20, occurrence: 6
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 24, occurrence: 7
   depth: 25, occurrence: 4
   depth: 26, occurrence: 2
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 29, occurrence: 3
   depth: 30, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 2
   depth: 38, occurrence: 3
   depth: 39, occurrence: 4
   depth: 42, occurrence: 2
   depth: 55, occurrence: 1

XXX total number of pointers: 500

XXX times a variable address is taken: 1131
XXX times a pointer is dereferenced on RHS: 355
breakdown:
   depth: 1, occurrence: 252
   depth: 2, occurrence: 85
   depth: 3, occurrence: 17
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 340
breakdown:
   depth: 1, occurrence: 289
   depth: 2, occurrence: 38
   depth: 3, occurrence: 11
   depth: 4, occurrence: 2
XXX times a pointer is compared with null: 57
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 7560

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1823
   level: 2, occurrence: 468
   level: 3, occurrence: 131
   level: 4, occurrence: 33
   level: 5, occurrence: 2
XXX number of pointers point to pointers: 230
XXX number of pointers point to scalars: 263
XXX number of pointers point to structs: 7
XXX percent of pointers has null in alias set: 33.8
XXX average alias set size: 1.48

XXX times a non-volatile is read: 2254
XXX times a non-volatile is write: 1065
XXX times a volatile is read: 19
XXX    times read thru a pointer: 10
XXX times a volatile is write: 20
XXX    times written thru a pointer: 17
XXX times a volatile is available for access: 912
XXX percentage of non-volatile access: 98.8

XXX forward jumps: 5
XXX backward jumps: 14

XXX stmts: 321
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 46
   depth: 2, occurrence: 52
   depth: 3, occurrence: 63
   depth: 4, occurrence: 65
   depth: 5, occurrence: 65

XXX percentage a fresh-made variable is used: 16.6
XXX percentage an existing variable is used: 83.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


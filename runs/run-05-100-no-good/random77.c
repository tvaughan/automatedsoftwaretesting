/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2601375785
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int8_t g_4 = 0x36L;/* VOLATILE GLOBAL g_4 */
static uint16_t g_59 = 1UL;
static int32_t g_63[3][4] = {{1L,0xF5B50694L,1L,1L},{0xF5B50694L,0xF5B50694L,0x5FDFDA84L,0xF5B50694L},{0xF5B50694L,1L,1L,0xF5B50694L}};
static volatile int32_t g_71 = 0x48320013L;/* VOLATILE GLOBAL g_71 */
static volatile int32_t *g_70[4] = {&g_71,&g_71,&g_71,&g_71};
static volatile int32_t **g_69[7][1] = {{&g_70[3]},{&g_70[3]},{&g_70[3]},{&g_70[3]},{&g_70[3]},{&g_70[3]},{&g_70[3]}};
static int8_t g_92 = (-1L);
static float g_93 = 0x1.Dp+1;
static int8_t g_95 = 0x73L;
static int32_t g_96[8] = {(-10L),0x6B3DAD93L,(-10L),0x6B3DAD93L,(-10L),0x6B3DAD93L,(-10L),0x6B3DAD93L};
static uint32_t g_117 = 0xC4EB09A5L;
static uint16_t g_121 = 65535UL;
static int32_t g_123 = 1L;
static int32_t * volatile g_122 = &g_123;/* VOLATILE GLOBAL g_122 */
static uint8_t g_129[6] = {0x74L,0x74L,0x8AL,0x74L,0x74L,0x8AL};
static uint32_t g_172 = 4294967295UL;
static uint8_t g_180 = 0xBEL;
static int64_t g_205[7] = {6L,6L,6L,6L,6L,6L,6L};
static int64_t g_220[1] = {9L};
static int16_t g_262[2] = {1L,1L};
static int32_t g_267 = 0x37B2804DL;
static int32_t g_274 = 0xDC2693B0L;
static float * volatile g_290 = &g_93;/* VOLATILE GLOBAL g_290 */
static uint64_t g_297 = 1UL;
static int32_t ** volatile g_308 = (void*)0;/* VOLATILE GLOBAL g_308 */
static int32_t *g_320 = &g_96[5];
static int32_t ** volatile g_319 = &g_320;/* VOLATILE GLOBAL g_319 */
static float g_340 = 0x1.8p+1;
static int32_t **g_380 = &g_320;
static int32_t ***g_379 = &g_380;
static float * volatile g_384 = &g_340;/* VOLATILE GLOBAL g_384 */
static volatile uint32_t * volatile g_386 = (void*)0;/* VOLATILE GLOBAL g_386 */
static volatile uint32_t * volatile * volatile g_385 = &g_386;/* VOLATILE GLOBAL g_385 */
static volatile uint32_t * volatile * volatile * volatile g_387 = &g_385;/* VOLATILE GLOBAL g_387 */
static int32_t * volatile g_392 = &g_63[2][2];/* VOLATILE GLOBAL g_392 */
static uint8_t g_409 = 0x14L;
static int16_t g_433 = 2L;
static int32_t * const  volatile g_434[2][6][10] = {{{&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267},{&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3]},{&g_96[1],&g_267,(void*)0,&g_267,&g_96[1],(void*)0,&g_96[1],&g_267,(void*)0,&g_267},{&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267},{&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3]},{&g_96[1],&g_267,(void*)0,&g_267,&g_96[1],(void*)0,&g_96[1],&g_267,(void*)0,&g_267}},{{&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267},{&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3]},{&g_96[1],&g_267,(void*)0,&g_267,&g_96[1],(void*)0,&g_96[1],&g_267,(void*)0,&g_267},{&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267},{&g_96[1],&g_267,&g_96[1],&g_63[1][3],&g_96[1],&g_63[1][3],&g_96[1],&g_267,&g_96[1],&g_63[1][3]},{&g_96[1],&g_267,(void*)0,&g_267,&g_96[1],(void*)0,&g_96[1],&g_267,(void*)0,&g_267}}};
static int32_t * volatile g_435 = (void*)0;/* VOLATILE GLOBAL g_435 */
static int32_t * const  volatile g_436 = &g_63[1][0];/* VOLATILE GLOBAL g_436 */
static uint32_t g_445[3][7][6] = {{{0x790FA329L,1UL,0xE29CE6E5L,0xAF753D0AL,0xD20985EAL,0xFCBEEDD7L},{0UL,0x67AB1739L,4294967294UL,0xEFFEF48FL,4294967290UL,1UL},{0x3D15847EL,0xD3E57F90L,0x3BC90DC9L,0x9BFA86D8L,4294967295UL,0xEFFEF48FL},{4294967291UL,0x185E6C30L,6UL,0x61E1F90EL,0x3D15847EL,0xC7BE1F42L},{6UL,0xD20985EAL,0x650A6B04L,0x3D15847EL,0x55DD7B72L,0x55DD7B72L},{0x31B403E6L,0xEFFEF48FL,0xEFFEF48FL,0x31B403E6L,4294967295UL,4294967295UL},{0x2B4BA6EBL,0xA685396CL,1UL,0xFCBEEDD7L,0xE8A0C153L,0x61E1F90EL}},{{0x74E9810CL,0x123B6697L,0x67AB1739L,0xD3E57F90L,0xE8A0C153L,0UL},{0xED8070E8L,0xA685396CL,0x7B1FD80FL,0xD20985EAL,4294967295UL,2UL},{0x650A6B04L,0xEFFEF48FL,0x74E9810CL,0x8E438EFCL,0x55DD7B72L,0x31B403E6L},{1UL,0xD20985EAL,4294967287UL,0x67AB1739L,0x3D15847EL,0xAF753D0AL},{0xE8A0C153L,0x185E6C30L,0xCA821C16L,0x55DD7B72L,4294967290UL,4294967290UL},{0xA685396CL,1UL,0xFC73D9B7L,0UL,4294967295UL,0xD3E57F90L},{0x8C87D4D6L,0x55DD7B72L,0UL,0x7B1FD80FL,4294967295UL,0x7B1FD80FL}},{{0x2B4BA6EBL,0x790FA329L,0x2B4BA6EBL,3UL,0xA9396B7FL,0xAF753D0AL},{0x790FA329L,0xA685396CL,4294967295UL,1UL,0x9BFA86D8L,1UL},{0xCA85B6FEL,0xCA821C16L,0xEFFEF48FL,1UL,0x8C87D4D6L,3UL},{0x790FA329L,0UL,2UL,3UL,0xC7BE1F42L,0x67AB1739L},{0x2B4BA6EBL,4294967291UL,0x376DFDD3L,0x7B1FD80FL,0x67AB1739L,0x74E9810CL},{0x8C87D4D6L,0x09E82F9DL,1UL,0UL,0xA64A254CL,0xE29CE6E5L},{0xA685396CL,0x123B6697L,4294967291UL,0x185E6C30L,6UL,0x61E1F90EL}}};
static float * volatile g_456[4] = {&g_93,&g_93,&g_93,&g_93};
static float * volatile g_457 = &g_93;/* VOLATILE GLOBAL g_457 */
static volatile int32_t g_503 = 1L;/* VOLATILE GLOBAL g_503 */
static float g_511 = (-0x1.8p-1);
static volatile uint64_t g_521 = 1UL;/* VOLATILE GLOBAL g_521 */
static uint64_t g_532 = 0xC988D5C145E63291LL;
static int32_t ** const *g_537 = &g_380;
static int32_t ** const **g_536 = &g_537;
static int32_t ** const *** volatile g_535 = &g_536;/* VOLATILE GLOBAL g_535 */
static int32_t g_557 = (-5L);
static int32_t g_560 = (-5L);
static uint64_t g_572 = 0xC285DBC07557998CLL;
static int64_t g_610[6][1] = {{0L},{0x96F4230E89E83BEDLL},{0L},{0x96F4230E89E83BEDLL},{0L},{0x96F4230E89E83BEDLL}};
static float * const  volatile g_613 = &g_93;/* VOLATILE GLOBAL g_613 */
static volatile int64_t * volatile * volatile g_681 = (void*)0;/* VOLATILE GLOBAL g_681 */
static volatile int64_t * volatile * volatile * volatile g_680 = &g_681;/* VOLATILE GLOBAL g_680 */
static uint16_t g_731 = 1UL;
static const uint64_t g_771 = 5UL;
static const uint64_t g_773 = 1UL;
static const uint64_t *g_772 = &g_773;
static const int32_t *g_881 = (void*)0;
static const int32_t ** volatile g_880 = &g_881;/* VOLATILE GLOBAL g_880 */
static volatile int64_t g_961 = 0x0937391918949B34LL;/* VOLATILE GLOBAL g_961 */
static float * volatile g_998 = &g_340;/* VOLATILE GLOBAL g_998 */
static float * volatile g_1037 = &g_93;/* VOLATILE GLOBAL g_1037 */
static volatile uint64_t * volatile *g_1061[3] = {(void*)0,(void*)0,(void*)0};
static volatile uint64_t * volatile **g_1060 = &g_1061[0];
static int32_t ****g_1089[3][10] = {{&g_379,&g_379,&g_379,&g_379,&g_379,&g_379,&g_379,&g_379,&g_379,&g_379},{&g_379,(void*)0,&g_379,&g_379,&g_379,&g_379,&g_379,&g_379,(void*)0,&g_379},{&g_379,&g_379,(void*)0,&g_379,(void*)0,&g_379,&g_379,&g_379,&g_379,(void*)0}};
static int32_t ***** volatile g_1088 = &g_1089[1][1];/* VOLATILE GLOBAL g_1088 */
static float * volatile g_1113 = &g_93;/* VOLATILE GLOBAL g_1113 */
static volatile uint8_t g_1140[10] = {0x58L,255UL,0xB9L,255UL,0x58L,0x58L,255UL,0xB9L,255UL,0x58L};
static int8_t **g_1166 = (void*)0;
static int8_t ***g_1165 = &g_1166;
static int8_t **** const  volatile g_1164 = &g_1165;/* VOLATILE GLOBAL g_1164 */
static uint32_t **g_1275 = (void*)0;
static uint32_t ***g_1274[6] = {&g_1275,&g_1275,&g_1275,&g_1275,&g_1275,&g_1275};
static uint32_t ****g_1273 = &g_1274[1];
static int32_t g_1311 = 0xF6700400L;
static int8_t ** const *g_1357[3] = {&g_1166,&g_1166,&g_1166};
static int8_t ** const * const *g_1356 = &g_1357[2];
static int8_t ** const * const **g_1355 = &g_1356;
static const int32_t **g_1402 = &g_881;
static const int32_t ***g_1401 = &g_1402;
static const int32_t ****g_1400 = &g_1401;
static const int32_t *****g_1399 = &g_1400;
static int32_t g_1420[10][1] = {{0xB7D2DDA6L},{1L},{0xB7D2DDA6L},{1L},{0xB7D2DDA6L},{1L},{0xB7D2DDA6L},{1L},{0xB7D2DDA6L},{1L}};
static int64_t *g_1624 = &g_205[0];
static int64_t **g_1623 = &g_1624;
static int64_t ***g_1622 = &g_1623;
static int32_t ** volatile g_1675 = &g_320;/* VOLATILE GLOBAL g_1675 */
static int64_t * volatile *g_1685 = &g_1624;
static int64_t * volatile * volatile *g_1684 = &g_1685;
static int64_t * volatile * volatile * volatile *g_1683 = &g_1684;
static int64_t * volatile * volatile * volatile **g_1682 = &g_1683;
static float * volatile g_1774 = (void*)0;/* VOLATILE GLOBAL g_1774 */
static int32_t ** volatile g_1800[8] = {&g_320,&g_320,&g_320,&g_320,&g_320,&g_320,&g_320,&g_320};
static int32_t ** volatile g_1801 = (void*)0;/* VOLATILE GLOBAL g_1801 */
static int32_t ** volatile g_1802 = &g_320;/* VOLATILE GLOBAL g_1802 */
static int32_t **** const *g_1863 = (void*)0;
static float * volatile g_1888 = (void*)0;/* VOLATILE GLOBAL g_1888 */
static float * volatile g_1889 = (void*)0;/* VOLATILE GLOBAL g_1889 */
static float * volatile g_1890 = &g_93;/* VOLATILE GLOBAL g_1890 */
static const int32_t g_1939 = 0x45BB2E71L;
static float * volatile g_1968[10] = {&g_340,(void*)0,&g_340,(void*)0,&g_340,(void*)0,&g_340,(void*)0,&g_340,(void*)0};
static int8_t g_1991[4][7] = {{0x43L,0x47L,0x43L,0x47L,0x43L,0x47L,0x43L},{0x0CL,0x0CL,0x0CL,0x0CL,0x0CL,0x0CL,0x0CL},{0x43L,0x47L,0x43L,0x47L,0x43L,0x47L,0x43L},{0x0CL,0x0CL,0x0CL,0x0CL,0x0CL,0x0CL,0x0CL}};
static int32_t g_2018 = 0xC6E9D003L;
static int32_t g_2019 = 0xC0254749L;
static float * volatile g_2021 = &g_511;/* VOLATILE GLOBAL g_2021 */
static int32_t g_2028[2] = {1L,1L};
static uint16_t g_2085[6] = {65531UL,0x1ABEL,0x1ABEL,65531UL,0x1ABEL,0x1ABEL};
static int32_t ** volatile g_2091 = &g_320;/* VOLATILE GLOBAL g_2091 */
static volatile uint64_t * volatile ***g_2193 = (void*)0;
static volatile uint64_t * volatile *** volatile *g_2192 = &g_2193;
static uint32_t *g_2246[1] = {&g_117};
static uint32_t * const *g_2245 = &g_2246[0];
static uint32_t * const **g_2244[7][6][1] = {{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}},{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}},{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}},{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}},{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}},{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}},{{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245},{&g_2245}}};
static int8_t * const g_2337 = (void*)0;
static int8_t * const *g_2336 = &g_2337;
static float * volatile g_2343 = &g_511;/* VOLATILE GLOBAL g_2343 */
static int64_t ****g_2351 = &g_1622;
static int64_t ** const *g_2353 = (void*)0;
static int64_t ** const **g_2352 = &g_2353;
static float *g_2383[2][9][6] = {{{(void*)0,&g_511,&g_511,(void*)0,&g_340,(void*)0},{(void*)0,&g_340,(void*)0,&g_511,&g_511,(void*)0},{&g_93,&g_93,&g_511,&g_93,&g_511,&g_93},{&g_511,&g_340,&g_93,&g_93,&g_340,&g_511},{&g_93,&g_511,&g_93,&g_511,&g_93,&g_93},{(void*)0,&g_511,&g_511,(void*)0,&g_340,(void*)0},{(void*)0,&g_340,(void*)0,&g_511,&g_511,(void*)0},{&g_93,&g_93,&g_511,&g_93,&g_511,&g_93},{&g_511,&g_340,&g_93,&g_93,&g_340,&g_511}},{{&g_93,&g_511,&g_93,&g_511,&g_93,&g_93},{(void*)0,&g_511,&g_511,(void*)0,&g_340,(void*)0},{(void*)0,&g_340,(void*)0,&g_511,&g_511,(void*)0},{&g_93,&g_93,&g_511,&g_93,&g_511,&g_93},{&g_511,&g_340,&g_93,&g_93,&g_340,&g_511},{&g_93,&g_511,&g_93,&g_511,&g_93,&g_93},{(void*)0,&g_511,&g_511,(void*)0,&g_340,(void*)0},{(void*)0,&g_340,(void*)0,&g_511,&g_511,(void*)0},{&g_93,&g_93,&g_511,&g_93,&g_511,&g_93}}};
static float **g_2382 = &g_2383[1][0][3];
static float **g_2384 = &g_2383[0][0][1];
static int32_t g_2434[1][4] = {{0x7930D901L,0x7930D901L,0x7930D901L,0x7930D901L}};
static uint64_t *g_2440 = &g_532;
static uint64_t **g_2439 = &g_2440;
static uint64_t ***g_2438 = &g_2439;
static uint64_t ****g_2437 = &g_2438;
static const volatile int8_t g_2499 = 0L;/* VOLATILE GLOBAL g_2499 */
static uint32_t *****g_2552 = &g_1273;
static volatile int32_t * volatile g_2562 = &g_71;/* VOLATILE GLOBAL g_2562 */
static volatile int32_t * volatile * volatile g_2564 = (void*)0;/* VOLATILE GLOBAL g_2564 */
static volatile int32_t * volatile * volatile g_2565 = (void*)0;/* VOLATILE GLOBAL g_2565 */
static const uint64_t ***g_2590 = (void*)0;
static const uint64_t **g_2596 = &g_772;
static const uint64_t ***g_2595 = &g_2596;
static const uint64_t ***g_2597 = &g_2596;
static int16_t * volatile *g_2635 = (void*)0;
static uint32_t g_2684[7] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static uint32_t *g_2683 = &g_2684[6];
static int64_t ** const ***g_2708[2][2] = {{&g_2352,&g_2352},{&g_2352,&g_2352}};
static const uint32_t g_2735 = 0x58E34A2EL;
static uint32_t g_2754[7][10] = {{0xCC7B2802L,0x50FDBE62L,4294967290UL,0x041AD407L,4294967295UL,2UL,4294967295UL,0x041AD407L,4294967290UL,0x50FDBE62L},{0xCC7B2802L,0x50FDBE62L,4294967290UL,0x041AD407L,4294967295UL,2UL,4294967295UL,0x041AD407L,4294967290UL,0x50FDBE62L},{0xCC7B2802L,0x50FDBE62L,4294967290UL,0x041AD407L,4294967295UL,2UL,4294967295UL,0x041AD407L,4294967290UL,0x50FDBE62L},{0xCC7B2802L,0x50FDBE62L,4294967290UL,0x50FDBE62L,4294967295UL,6UL,4294967295UL,0x50FDBE62L,4294967295UL,0xF6C57FA5L},{1UL,0xF6C57FA5L,4294967295UL,0x50FDBE62L,4294967295UL,6UL,4294967295UL,0x50FDBE62L,4294967295UL,0xF6C57FA5L},{1UL,0xF6C57FA5L,4294967295UL,0x50FDBE62L,4294967295UL,6UL,4294967295UL,0x50FDBE62L,4294967295UL,0xF6C57FA5L},{1UL,0xF6C57FA5L,4294967295UL,0x50FDBE62L,4294967295UL,6UL,4294967295UL,0x50FDBE62L,4294967295UL,0xF6C57FA5L}};


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t  func_6(int16_t  p_7, uint16_t  p_8);
static int16_t  func_9(uint8_t  p_10, uint64_t  p_11, uint64_t  p_12);
static uint8_t  func_13(uint8_t  p_14, const int32_t  p_15, int8_t  p_16);
static uint8_t  func_17(int32_t  p_18, int16_t  p_19, uint64_t  p_20, int32_t  p_21);
static int32_t  func_22(int32_t  p_23, uint16_t  p_24, uint32_t  p_25, uint32_t  p_26);
static int16_t  func_29(uint64_t  p_30, uint8_t  p_31, uint64_t  p_32, uint32_t  p_33);
static uint64_t  func_34(int8_t  p_35, uint16_t  p_36, int64_t  p_37, float  p_38);
static int64_t  func_41(uint8_t  p_42);
static int16_t  func_47(int8_t  p_48);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_59 g_63 g_69 g_96 g_93 g_95 g_117 g_121 g_71 g_122 g_92 g_129 g_123 g_172 g_205 g_180 g_220 g_290 g_262 g_319 g_267 g_274 g_379 g_384 g_385 g_387 g_392 g_320 g_380 g_409 g_436 g_560 g_297 g_731 g_386 g_610 g_433 g_1140 g_772 g_773 g_572 g_445 g_1273 g_557 g_1060 g_1061 g_961 g_1311 g_537 g_521 g_1420 g_536 g_532 g_1400 g_1401 g_1402 g_1675 g_1274 g_1682 g_1684 g_1685 g_1624 g_457 g_535 g_1939 g_613 g_1622 g_1623 g_1991 g_2018 g_2019 g_2021 g_2028 g_880 g_881 g_2085 g_2091 g_2244 g_2245 g_2246 g_1113 g_1683 g_1165 g_1166 g_2343 g_2352 g_2336 g_2337 g_2735 g_2596 g_2754 g_2192 g_2193 g_2440
 * writes: g_63 g_59 g_92 g_95 g_96 g_117 g_121 g_123 g_129 g_172 g_205 g_180 g_220 g_262 g_267 g_274 g_93 g_297 g_122 g_320 g_340 g_379 g_385 g_433 g_572 g_560 g_1089 g_1273 g_1061 g_1355 g_1311 g_881 g_409 g_557 g_1275 g_1682 g_1624 g_511 g_2018 g_445 g_731 g_1274 g_2244 g_2246 g_1623 g_1685 g_2336 g_2351 g_2437
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_5[9][10] = {{0UL,0xF5L,2UL,5UL,2UL,0xF5L,0UL,0UL,0xF5L,2UL},{0xF5L,0UL,0UL,0xF5L,2UL,5UL,2UL,0xF5L,0UL,0UL},{2UL,0UL,8UL,0xE5L,0xE5L,8UL,0UL,2UL,0UL,8UL},{5UL,0xF5L,0xE5L,0xF5L,5UL,8UL,8UL,5UL,0xF5L,0xE5L},{2UL,2UL,0xE5L,5UL,255UL,5UL,0xE5L,2UL,2UL,0xE5L},{0xF5L,5UL,8UL,8UL,5UL,0xF5L,0xE5L,0xF5L,5UL,8UL},{0UL,2UL,0UL,8UL,0xE5L,0xE5L,8UL,0UL,2UL,0UL},{0UL,0xF5L,2UL,5UL,2UL,0xF5L,0UL,0UL,0xF5L,2UL},{0xF5L,0UL,0UL,0xF5L,2UL,5UL,2UL,0xF5L,0UL,0UL}};
    int32_t l_39 = 0L;
    int32_t * const *l_401 = (void*)0;
    int32_t * const **l_400 = &l_401;
    int32_t ***l_404 = &g_380;
    int32_t ***l_406 = (void*)0;
    uint64_t l_1374 = 0xCF9367E9BE5E2798LL;
    uint16_t l_1651[1];
    float l_1666 = 0xD.6FC6CCp-7;
    int32_t l_1901 = 0xFE8AF038L;
    int16_t l_1915 = (-10L);
    int32_t l_1942 = (-1L);
    const int64_t *l_1949 = &g_205[4];
    int8_t *l_1979 = (void*)0;
    int8_t **l_1978[1];
    int32_t ***l_1999[10][8][3] = {{{&g_380,&g_380,(void*)0},{(void*)0,(void*)0,&g_380},{&g_380,&g_380,(void*)0},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0},{&g_380,(void*)0,&g_380},{&g_380,(void*)0,&g_380}},{{&g_380,&g_380,(void*)0},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{(void*)0,(void*)0,&g_380},{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{(void*)0,(void*)0,(void*)0}},{{&g_380,&g_380,&g_380},{(void*)0,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380}},{{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0},{&g_380,&g_380,(void*)0}},{{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380},{(void*)0,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{(void*)0,&g_380,&g_380},{(void*)0,&g_380,&g_380}},{{&g_380,(void*)0,(void*)0},{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380}},{{&g_380,&g_380,&g_380},{(void*)0,&g_380,&g_380},{(void*)0,&g_380,(void*)0},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380}},{{&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0},{&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0},{&g_380,&g_380,&g_380},{(void*)0,(void*)0,&g_380},{(void*)0,&g_380,&g_380},{&g_380,&g_380,&g_380}},{{&g_380,&g_380,&g_380},{&g_380,(void*)0,&g_380},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380}},{{(void*)0,&g_380,&g_380},{(void*)0,&g_380,&g_380},{&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0},{&g_380,&g_380,&g_380},{(void*)0,&g_380,(void*)0},{&g_380,(void*)0,&g_380},{&g_380,&g_380,&g_380}}};
    int16_t l_2006 = 0x94FAL;
    int16_t l_2016 = 0x4831L;
    int32_t l_2026[5] = {0xD0076462L,0xD0076462L,0xD0076462L,0xD0076462L,0xD0076462L};
    uint64_t *l_2061[8] = {&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532};
    uint64_t **l_2060[7][5][7] = {{{(void*)0,&l_2061[4],(void*)0,(void*)0,(void*)0,(void*)0,&l_2061[0]},{&l_2061[1],&l_2061[7],&l_2061[0],&l_2061[2],&l_2061[0],&l_2061[5],(void*)0},{&l_2061[2],&l_2061[0],&l_2061[0],&l_2061[2],&l_2061[0],&l_2061[0],&l_2061[2]},{&l_2061[5],(void*)0,&l_2061[0],(void*)0,&l_2061[2],&l_2061[5],(void*)0},{&l_2061[5],(void*)0,&l_2061[2],&l_2061[4],&l_2061[0],(void*)0,(void*)0}},{{&l_2061[2],(void*)0,&l_2061[0],&l_2061[0],&l_2061[0],&l_2061[0],&l_2061[0]},{&l_2061[1],(void*)0,&l_2061[0],&l_2061[5],&l_2061[2],&l_2061[2],(void*)0},{&l_2061[0],(void*)0,&l_2061[0],&l_2061[0],&l_2061[0],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2061[5],&l_2061[0],(void*)0,&l_2061[2]},{(void*)0,(void*)0,&l_2061[5],&l_2061[0],(void*)0,&l_2061[2],(void*)0}},{{(void*)0,(void*)0,&l_2061[5],&l_2061[4],&l_2061[4],&l_2061[0],&l_2061[0]},{&l_2061[0],&l_2061[0],(void*)0,(void*)0,(void*)0,(void*)0,&l_2061[0]},{&l_2061[1],&l_2061[7],&l_2061[0],&l_2061[2],&l_2061[0],&l_2061[5],(void*)0},{&l_2061[2],&l_2061[0],&l_2061[0],&l_2061[2],&l_2061[0],&l_2061[0],&l_2061[2]},{&l_2061[5],(void*)0,&l_2061[0],(void*)0,&l_2061[2],&l_2061[5],(void*)0}},{{&l_2061[5],(void*)0,&l_2061[2],&l_2061[4],&l_2061[0],(void*)0,(void*)0},{&l_2061[2],(void*)0,&l_2061[0],&l_2061[0],&l_2061[0],&l_2061[0],&l_2061[0]},{&l_2061[1],(void*)0,&l_2061[0],&l_2061[5],&l_2061[2],&l_2061[2],(void*)0},{&l_2061[0],(void*)0,&l_2061[0],&l_2061[0],&l_2061[0],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2061[5],&l_2061[0],(void*)0,&l_2061[2]}},{{(void*)0,(void*)0,&l_2061[5],&l_2061[0],(void*)0,&l_2061[2],(void*)0},{(void*)0,(void*)0,&l_2061[5],&l_2061[4],&l_2061[4],&l_2061[0],&l_2061[0]},{&l_2061[0],&l_2061[0],(void*)0,(void*)0,(void*)0,(void*)0,&l_2061[0]},{&l_2061[1],&l_2061[7],&l_2061[0],&l_2061[2],&l_2061[0],&l_2061[5],(void*)0},{&l_2061[2],&l_2061[0],&l_2061[0],&l_2061[2],&l_2061[0],&l_2061[0],&l_2061[2]}},{{&l_2061[5],(void*)0,&l_2061[0],(void*)0,&l_2061[2],&l_2061[5],(void*)0},{&l_2061[5],(void*)0,&l_2061[2],&l_2061[4],&l_2061[0],(void*)0,(void*)0},{&l_2061[2],(void*)0,&l_2061[0],&l_2061[3],&l_2061[4],&l_2061[1],(void*)0},{&l_2061[5],&l_2061[6],&l_2061[6],&l_2061[6],&l_2061[0],&l_2061[2],&l_2061[0]},{&l_2061[0],&l_2061[6],&l_2061[4],(void*)0,&l_2061[6],&l_2061[5],&l_2061[2]}},{{&l_2061[0],&l_2061[0],&l_2061[3],&l_2061[6],&l_2061[0],&l_2061[5],&l_2061[0]},{&l_2061[2],(void*)0,&l_2061[0],&l_2061[3],&l_2061[0],&l_2061[2],&l_2061[0]},{&l_2061[0],&l_2061[5],&l_2061[0],&l_2061[0],&l_2061[5],&l_2061[1],&l_2061[0]},{&l_2061[0],(void*)0,&l_2061[3],(void*)0,&l_2061[0],&l_2061[0],&l_2061[0]},{&l_2061[5],&l_2061[0],&l_2061[4],&l_2061[4],&l_2061[0],(void*)0,&l_2061[0]}}};
    uint64_t ***l_2059 = &l_2060[4][0][3];
    uint64_t ****l_2058 = &l_2059;
    int32_t l_2066 = (-1L);
    const float *l_2070 = (void*)0;
    const uint8_t l_2071[8] = {0x89L,0x89L,0x89L,0x89L,0x89L,0x89L,0x89L,0x89L};
    int64_t ***l_2084[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t l_2087[2];
    int8_t * const ***l_2111 = (void*)0;
    int8_t * const *** const * const l_2110 = &l_2111;
    int32_t l_2178 = 0xB1841E32L;
    int8_t l_2238[7][7][4] = {{{0xD6L,0xF9L,9L,0x59L},{0x59L,0L,0xFAL,0xF9L},{(-1L),0xFAL,0xFAL,(-1L)},{0x59L,0x32L,9L,(-1L)},{0xD6L,(-1L),(-1L),0x59L},{(-1L),0x59L,0x1AL,0x59L},{0xFAL,(-1L),0L,(-1L)}},{{0xFCL,0x32L,0x59L,(-1L)},{0L,0xFAL,0xF9L,0xF9L},{0L,0L,0x59L,0x59L},{0xFCL,0xF9L,0L,0x32L},{0xFAL,0xD6L,0x1AL,0L},{(-1L),0xD6L,(-1L),0x32L},{0xD6L,0xF9L,9L,0x59L}},{{0x59L,0L,0xFAL,0xF9L},{(-1L),0xFAL,0xFAL,(-1L)},{0x59L,0x32L,9L,(-1L)},{0xD6L,(-1L),(-1L),0x59L},{(-1L),0x59L,0x1AL,0x59L},{0xFAL,(-1L),0L,(-1L)},{0xFCL,0x32L,0x59L,(-1L)}},{{0L,0xFAL,0xF9L,0xF9L},{0L,0L,0x59L,0x59L},{0xFCL,0xF9L,0L,0x32L},{0xFAL,0xD6L,0x1AL,0L},{(-1L),0xD6L,(-1L),0x32L},{0xD6L,0xF9L,9L,0x59L},{0x59L,0L,0xFAL,0xF9L}},{{(-1L),0xFAL,0xFAL,(-1L)},{0x59L,0x32L,9L,(-1L)},{0xD6L,(-1L),(-1L),0x59L},{(-1L),0xD6L,0xFCL,0xD6L},{(-1L),9L,0x59L,0xF9L},{(-1L),(-1L),0xD6L,0L},{0x59L,(-1L),0xFAL,0xFAL}},{{0x59L,0x59L,0xD6L,0x32L},{(-1L),0xFAL,0x59L,(-1L)},{(-1L),0x59L,0xFCL,0x59L},{0xF9L,0x59L,0xF9L,(-1L)},{0x59L,0xFAL,0x1AL,0x32L},{0x32L,0x59L,(-1L),0xFAL},{0L,(-1L),(-1L),0L}},{{0x32L,(-1L),0x1AL,0xF9L},{0x59L,9L,0xF9L,0xD6L},{0xF9L,0xD6L,0xFCL,0xD6L},{(-1L),9L,0x59L,0xF9L},{(-1L),(-1L),0xD6L,0L},{0x59L,(-1L),0xFAL,0xFAL},{0x59L,0x59L,0xD6L,0x32L}}};
    int32_t l_2243 = (-5L);
    uint32_t * const ***l_2247 = &g_2244[1][3][0];
    int64_t l_2248 = 0xEA404D1562EF163ELL;
    int64_t l_2249 = 0x560898B524430BABLL;
    int32_t l_2254 = 0xA5828DF3L;
    uint8_t l_2255[6] = {0x9FL,0x9FL,0x9FL,0x9FL,0x9FL,0x9FL};
    uint8_t *l_2256 = (void*)0;
    uint8_t *l_2257 = (void*)0;
    uint8_t *l_2258 = &l_5[7][5];
    float l_2259 = 0x1.Dp-1;
    uint32_t l_2260 = 0x9C965924L;
    const int16_t l_2308 = 0x26EBL;
    int8_t l_2312[10];
    uint32_t *** const *l_2359 = &g_1274[2];
    uint32_t *** const ** const l_2358[6][10][4] = {{{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,(void*)0},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,(void*)0,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359}},{{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,(void*)0,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,(void*)0},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359}},{{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,(void*)0},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359}},{{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,(void*)0,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359}},{{&l_2359,(void*)0,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,(void*)0,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,(void*)0,&l_2359,&l_2359}},{{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{&l_2359,(void*)0,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359},{(void*)0,&l_2359,&l_2359,&l_2359},{&l_2359,&l_2359,&l_2359,&l_2359}}};
    int64_t **l_2418 = &g_1624;
    int32_t l_2428[3][4];
    int16_t l_2491 = 9L;
    uint64_t l_2608 = 0x7E909CEC6FB6896DLL;
    uint16_t l_2644 = 65527UL;
    uint32_t l_2647 = 0x85152969L;
    uint32_t *l_2682 = &l_2260;
    int64_t *****l_2687 = &g_2351;
    uint64_t *****l_2720 = &g_2437;
    float l_2733 = 0xA.F6F0A9p-88;
    int32_t l_2734 = 0L;
    const float l_2749 = (-0x2.4p-1);
    const uint32_t l_2750[4] = {0xAFE333ECL,0xAFE333ECL,0xAFE333ECL,0xAFE333ECL};
    int32_t l_2751 = 7L;
    uint32_t l_2752[4] = {0xD344EEC4L,0xD344EEC4L,0xD344EEC4L,0xD344EEC4L};
    int32_t l_2753 = 1L;
    int16_t *l_2755 = &g_262[0];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1651[i] = 0xD856L;
    for (i = 0; i < 1; i++)
        l_1978[i] = &l_1979;
    for (i = 0; i < 2; i++)
        l_2087[i] = 0x98A97D63L;
    for (i = 0; i < 10; i++)
        l_2312[i] = (-8L);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
            l_2428[i][j] = (-10L);
    }
    if ((safe_mod_func_uint32_t_u_u((g_4 & l_5[7][5]), l_5[3][0])))
    { /* block id: 1 */
        uint32_t l_40[7][6] = {{0xD6273D59L,0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L},{0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L,0UL},{0xD6273D59L,0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L},{0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L,0UL},{0xD6273D59L,0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L},{0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L,0UL},{0xD6273D59L,0xD6273D59L,0UL,0UL,0xD6273D59L,0xD6273D59L}};
        uint16_t *l_403[1];
        int32_t ****l_405[8];
        uint8_t *l_407[3][3][1] = {{{&l_5[7][5]},{&g_180},{&l_5[7][5]}},{{&g_180},{&l_5[7][5]},{&g_180}},{{&l_5[7][5]},{&g_180},{&l_5[7][5]}}};
        uint8_t l_408[10][9][2] = {{{252UL,0x08L},{255UL,0x07L},{0x16L,0x08L},{0xA7L,0x3FL},{0x0BL,0x0BL},{0xC3L,0x08L},{0xB3L,255UL},{0x96L,0xA7L},{0xAFL,0x96L}},{{0xA2L,0UL},{0xA2L,0x96L},{0xAFL,0xA7L},{0x96L,255UL},{0xB3L,0x08L},{0xC3L,0x0BL},{0x0BL,0x3FL},{0xA7L,0x08L},{0x3BL,255UL}},{{0x96L,0xC3L},{0x16L,0x96L},{0x64L,3UL},{0xA2L,0xCFL},{0x16L,0xA7L},{0xCFL,255UL},{0xB3L,0x60L},{0xA7L,0x0BL},{0x3FL,0x0BL}},{{0xA7L,0x60L},{0xB3L,255UL},{0xCFL,0xA7L},{0x16L,0xCFL},{0xA2L,3UL},{0x64L,0x96L},{0x16L,0xC3L},{0x96L,255UL},{0x3BL,0x08L}},{{0xA7L,0x3FL},{0x0BL,0x0BL},{0xC3L,0x08L},{0xB3L,255UL},{0x96L,0xA7L},{0xAFL,0x96L},{0xA2L,0UL},{0xA2L,0x96L},{0xAFL,0xA7L}},{{0x96L,255UL},{0xB3L,0x08L},{0xC3L,0x0BL},{0x0BL,0x3FL},{0xA7L,0x08L},{0x3BL,255UL},{0x96L,0xC3L},{0x16L,0x96L},{0x64L,3UL}},{{0xA2L,0xCFL},{0x16L,0xA7L},{0xCFL,255UL},{0xB3L,0x60L},{0xA7L,0x0BL},{0x3FL,0x0BL},{0xA7L,0x60L},{0xB3L,255UL},{0xCFL,0xA7L}},{{0x16L,0xCFL},{0xA2L,3UL},{0x64L,0x96L},{0x16L,0xC3L},{0x96L,255UL},{0x3BL,0x08L},{0xA7L,0x3FL},{0x0BL,0x0BL},{0xC3L,0x08L}},{{0xB3L,255UL},{0x96L,0xA7L},{0xAFL,0x96L},{0xA2L,0UL},{0xA2L,0x96L},{0xAFL,0xA7L},{0x96L,255UL},{0xB3L,0x08L},{0xC3L,0x0BL}},{{0x0BL,0x3FL},{0xA7L,0x08L},{0x3BL,255UL},{0x96L,0xC3L},{0x16L,0x96L},{0x64L,3UL},{0xA2L,0xCFL},{0x16L,0xA7L},{0xCFL,255UL}}};
        uint8_t l_1373 = 0x53L;
        int16_t l_1654 = 0x010AL;
        uint16_t l_1937 = 0x35A7L;
        int32_t *l_1938 = &g_1420[7][0];
        uint64_t l_1943 = 0x56DF128DE5BA78BBLL;
        uint64_t l_1967 = 0xCCD12AF8E4084DB5LL;
        int8_t **l_1980 = (void*)0;
        int64_t l_1981 = (-6L);
        int8_t l_1990 = 0x06L;
        uint32_t *l_2027 = &g_172;
        uint32_t l_2029 = 18446744073709551611UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_403[i] = &g_59;
        for (i = 0; i < 8; i++)
            l_405[i] = &g_379;
        if (func_6(func_9(func_13(func_17(func_22((safe_lshift_func_int16_t_s_u(func_29(func_34((l_39 = 1L), l_40[2][0], func_41(((g_4 , ((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((func_47(l_5[7][5]) , (safe_rshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u(((g_262[0] , (g_129[4] = ((safe_mod_func_uint64_t_u_u((l_400 != (l_406 = ((+(g_121 |= g_92)) , l_404))), 0x5CD88ABFC873EBF1LL)) > 250UL))) != g_274), 0UL)) != g_4), 15))), l_408[5][8][1])), g_409)) == g_409)) ^ 5L)), g_409), l_1373, (*g_772), l_1374), 0)), g_610[2][0], g_1420[7][0], l_1651[0]), g_1420[1][0], (*g_772), l_1651[0]), g_532, l_1654), (*g_772), (*g_772)), g_445[1][1][1]))
        { /* block id: 924 */
            int8_t l_1902 = 0xAAL;
            int32_t l_1903[3];
            const uint32_t l_1936 = 0xC7FCC999L;
            float *l_1940[6] = {&g_340,&g_340,&g_340,&g_340,&g_340,&g_340};
            int i;
            for (i = 0; i < 3; i++)
                l_1903[i] = 0x0C86161FL;
            l_1903[1] = (l_1901 || (9L ^ l_1902));
            l_1903[0] &= 0xCBA16E23L;
            (*g_457) = (((safe_mul_func_float_f_f((-0x9.5p+1), (safe_mul_func_float_f_f((safe_add_func_float_f_f(((+((g_205[6] , ((safe_rshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_s(l_1902, 1)), l_1915)) != (((((((~(((!(l_1902 , (((safe_rshift_func_int8_t_s_s((safe_add_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((safe_mod_func_int64_t_s_s(((1UL ^ (safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(l_1903[0], 0xCC7146B7L)), ((safe_rshift_func_int8_t_s_s(l_1902, 5)) || g_773))), g_560))) , 0x92ADFE5965FB13C1LL), l_1936)), l_1937)) && g_610[0][0]), (-1L))) <= 1L), l_1936)), 1)) , g_1420[7][0]) , l_1903[1]))) , (void*)0) != l_1938)) == l_1903[1]) || g_1939) == (*g_772)) || l_1903[1]) && l_1936) != l_1903[2]))) != g_731)) , l_1936), l_1903[0])), (-0x8.Ap+1))))) >= l_1903[1]) > (*g_613));
        }
        else
        { /* block id: 928 */
            int8_t l_1941 = 0x15L;
            int64_t *l_1948 = &g_610[0][0];
            int16_t *l_1950 = &l_1915;
            uint8_t l_1951 = 0x13L;
            int32_t l_1957 = 0L;
            int8_t l_1992 = 0xDAL;
            int32_t ***l_1998 = &g_380;
            uint64_t l_2010[10] = {18446744073709551611UL,0x5E169F026E8CC69BLL,0x5E169F026E8CC69BLL,18446744073709551611UL,0x5E169F026E8CC69BLL,0x5E169F026E8CC69BLL,18446744073709551611UL,0x5E169F026E8CC69BLL,0x5E169F026E8CC69BLL,18446744073709551611UL};
            int i;
            l_1943++;
            if ((((*l_1950) = (safe_mul_func_uint16_t_u_u(g_96[5], (((**g_1622) = l_1948) == l_1949)))) > ((249UL | (l_1951 | 4294967288UL)) ^ 9UL)))
            { /* block id: 932 */
                int8_t *l_1954 = &l_1941;
                int8_t *l_1955 = &g_95;
                int8_t *l_1956[5];
                const int32_t l_1960 = 6L;
                float *l_1969 = &g_511;
                int32_t l_1993 = 0xF488A95DL;
                int i;
                for (i = 0; i < 5; i++)
                    l_1956[i] = &g_92;
                (*l_1969) = (l_1941 , ((((safe_rshift_func_int16_t_s_s(((l_1957 ^= ((*l_1955) = ((*l_1954) &= 0x9EL))) && ((((((safe_rshift_func_uint16_t_u_u(l_1960, 11)) || (safe_div_func_uint8_t_u_u((0xF7L && (safe_div_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((**g_1622) != (void*)0), ((((**l_404) = (**l_404)) != ((g_409 = ((*g_1273) != (l_1967 , (*g_1273)))) , &l_1957)) & 1L))), l_1960))), l_1960))) , &g_1061[0]) == (void*)0) , 0x67EF59ED942428C5LL) >= l_1960)), 9)) <= 0L) , l_1960) < (*g_290)));
                l_1992 ^= ((safe_sub_func_int32_t_s_s(l_1960, (safe_mul_func_int32_t_s_s((safe_lshift_func_int16_t_s_s(g_1420[1][0], (safe_add_func_uint8_t_u_u(0UL, 1L)))), (l_1981 = (l_1978[0] != l_1980)))))) ^ (safe_rshift_func_int8_t_s_u(((safe_add_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((safe_lshift_func_int8_t_s_s(l_1960, ((((l_1960 >= ((g_409 > (-1L)) , l_1960)) >= l_1957) & 3UL) & 0x388DF3A610BABDD0LL))), g_1420[7][0])) < 2L), l_1990)) , g_1991[0][4]), g_445[2][4][5])));
                l_1993 = l_1992;
            }
            else
            { /* block id: 942 */
                uint32_t l_2017 = 0x41732E17L;
                for (l_1992 = (-16); (l_1992 == (-6)); l_1992++)
                { /* block id: 945 */
                    uint32_t l_2009[2][1][6] = {{{0x4CD3D192L,0x4CD3D192L,0x4CD3D192L,0x4CD3D192L,0x4CD3D192L,0x4CD3D192L}},{{0x4CD3D192L,0x4CD3D192L,0x4CD3D192L,0x4CD3D192L,0x4CD3D192L,0x4CD3D192L}}};
                    int32_t l_2020 = 0xDB1B14DBL;
                    int i, j, k;
                    (*g_2021) = (safe_sub_func_float_f_f(((l_1999[5][5][2] = l_1998) == (*g_1400)), (safe_add_func_float_f_f(((((l_2020 = (safe_sub_func_uint64_t_u_u(((***g_1622) >= (safe_sub_func_int32_t_s_s((*g_392), l_2006))), (((((safe_mul_func_int8_t_s_s(l_2009[1][0][3], (l_2010[3] |= g_1939))) <= ((((((safe_mod_func_uint32_t_u_u((((g_2018 &= (+(safe_add_func_int8_t_s_s(l_2016, (l_2017 | 0x6539AC72L))))) & l_2009[1][0][3]) && 0x9738C0A643CE5A97LL), l_2017)) ^ 0x745B22CFL) , (**g_1685)) == (*g_772)) <= g_2019) <= l_2009[1][0][3])) , l_2017) && 0xF8267A2C5F0FEC7CLL) == 4294967290UL)))) > g_96[5]) , (*g_613)) <= 0x6.7D93E3p-73), 0x6.906230p-41))));
                }
            }
        }
        if ((safe_sub_func_int32_t_s_s(((safe_add_func_uint32_t_u_u(((*l_2027) ^= l_2026[3]), ((&l_1374 == ((g_2028[1] < l_2029) , &l_1374)) <= (*g_772)))) < 0x4BADL), (*g_436))))
        { /* block id: 955 */
            const int32_t *l_2035 = &g_96[5];
            int32_t l_2036 = 0L;
            for (l_1937 = (-2); (l_1937 <= 22); ++l_1937)
            { /* block id: 958 */
                uint32_t l_2032 = 4294967295UL;
                ++l_2032;
                l_2035 = (*g_880);
            }
            return l_2036;
        }
        else
        { /* block id: 963 */
            uint64_t l_2037 = 0UL;
            int32_t l_2038[5][2][5];
            int i, j, k;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 2; j++)
                {
                    for (k = 0; k < 5; k++)
                        l_2038[i][j][k] = (-8L);
                }
            }
            l_2038[4][0][3] |= l_2037;
        }
    }
    else
    { /* block id: 966 */
        uint8_t *l_2047 = &l_5[1][7];
        uint32_t *l_2053 = &g_445[0][1][2];
        float *l_2062 = (void*)0;
        float *l_2063[3];
        int32_t l_2064 = 0x1822795EL;
        int32_t l_2065 = 0xA7276F57L;
        int32_t l_2067 = 2L;
        float **l_2068 = &l_2063[0];
        float *l_2069 = &l_1666;
        int64_t ***l_2158 = &g_1623;
        int16_t l_2161[9];
        float l_2163 = 0x3.A56911p-86;
        int32_t l_2168[8][10][3] = {{{0x2071F0EEL,0x82BD8845L,1L},{0L,0xC384FE04L,1L},{0xC86490B8L,0x019615F9L,(-5L)},{(-9L),(-9L),(-1L)},{0L,0x2071F0EEL,0x019615F9L},{0x9745CED4L,5L,8L},{1L,(-1L),0xEA04E116L},{1L,0x9745CED4L,8L},{0L,0x41B38AE1L,0x019615F9L},{1L,0x0C0423ACL,(-1L)}},{{7L,1L,(-5L)},{0x7D4B22FEL,0x492790C0L,1L},{0x86CB6A13L,0xC734EFB1L,1L},{0xB72E676BL,0xC34602AEL,0xA647BA95L},{0xEA04E116L,1L,0x2071F0EEL},{(-7L),5L,0xDBCCD91BL},{1L,0xDB001E96L,0L},{1L,(-1L),0xA1BEE393L},{(-2L),0x86CB6A13L,0xE5156C43L},{1L,0x669C2BD8L,0x9745CED4L}},{{(-1L),0x82BD8845L,0x9AFEEB84L},{0L,(-2L),0L},{0xF1287B45L,9L,1L},{0xC34602AEL,0x90A4EEA3L,0x82CCCD25L},{0L,0x019615F9L,7L},{(-1L),0x7E51FD47L,(-10L)},{0L,0xDB001E96L,1L},{0xC34602AEL,(-1L),8L},{0xF1287B45L,2L,1L},{0L,0x9745CED4L,(-7L)}},{{(-1L),(-3L),0L},{1L,0x492790C0L,0x7D4B22FEL},{(-2L),0L,(-5L)},{1L,0L,0x82CCCD25L},{1L,0xC734EFB1L,(-1L)},{(-7L),0xC9E5138AL,0xDD124E34L},{0xEA04E116L,(-1L),1L},{0xB72E676BL,(-1L),0xDBCCD91BL},{0x86CB6A13L,0xF1287B45L,0xE5156C43L},{0x7D4B22FEL,(-1L),(-1L)}},{{7L,0xA67439DEL,0xAF752ECEL},{1L,0xC384FE04L,0L},{0L,9L,0x9AFEEB84L},{1L,0L,0xA647BA95L},{1L,9L,(-1L)},{0x9745CED4L,0xC384FE04L,1L},{0L,0xA67439DEL,(-8L)},{(-9L),(-1L),(-10L)},{0xC86490B8L,0xF1287B45L,0x019615F9L},{0L,(-1L),(-7L)}},{{0x2071F0EEL,(-1L),(-1L)},{0L,0xC9E5138AL,0xB782AFEDL},{0L,0xC734EFB1L,0L},{0x9520BB0CL,0L,(-1L)},{(-8L),0L,7L},{(-1L),0x492790C0L,1L},{1L,(-3L),5L},{0xB72E676BL,0x9745CED4L,0xDD124E34L},{2L,2L,0x2071F0EEL},{0xB782AFEDL,(-1L),0xC6450A63L}},{{0x0276F7CFL,2L,(-1L)},{0xA1BEE393L,0x9FDD9555L,1L},{9L,0x0276F7CFL,(-1L)},{0xA647BA95L,(-7L),0xC6450A63L},{(-5L),1L,0xEA04E116L},{0x8B71BD96L,0xF8ACA71BL,0x492790C0L},{(-1L),0xF1287B45L,(-5L)},{(-2L),0xB782AFEDL,0xEA2FDC87L},{(-1L),0L,(-1L)},{1L,0x11AD5033L,0xCE2B894FL}},{{5L,2L,1L},{0xD1A432A4L,(-9L),1L},{1L,1L,(-5L)},{0x0C0423ACL,(-2L),1L},{(-5L),0L,0x0276F7CFL},{0L,1L,0x7E51FD47L},{0xB8C84E1CL,1L,0xB8C84E1CL},{(-1L),0x7D4B22FEL,0xEA2FDC87L},{0L,0xE5156C43L,(-8L)},{1L,0xC6450A63L,0x8B71BD96L}}};
        int32_t l_2175[5][1] = {{1L},{0x4EF6050AL},{1L},{0x4EF6050AL},{1L}};
        uint16_t l_2229 = 1UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_2063[i] = &g_511;
        for (i = 0; i < 9; i++)
            l_2161[i] = (-8L);
        if (((safe_lshift_func_uint8_t_u_u(((*l_2047) = (safe_add_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u(((void*)0 != l_2047), (~(((****g_536) = (safe_mod_func_uint64_t_u_u(((((--(*l_2053)) <= 4294967295UL) & ((((((safe_add_func_float_f_f(((*g_384) = (l_2065 = ((l_2064 = (l_2058 == &g_1060)) >= (*g_613)))), (((g_511 = g_267) < l_2066) <= ((*l_2069) = ((((*l_2068) = ((g_2028[1] | l_2067) , (void*)0)) == (void*)0) > 0xE.B6B9DDp-18))))) , (void*)0) != l_2070) < l_2067) && 5L) <= g_2018)) != g_123), l_2067))) > l_2067)))), 253UL)) || 0x4688L), l_2071[5]))), 6)) > g_1991[0][4]))
        { /* block id: 976 */
            float l_2072 = 0x9.D70CADp-12;
            uint64_t l_2083 = 1UL;
            uint32_t l_2086 = 4294967295UL;
            int32_t l_2093 = 0L;
            int32_t l_2094 = 2L;
            uint16_t l_2095[8] = {0xF2B3L,7UL,7UL,0xF2B3L,7UL,7UL,0xF2B3L,7UL};
            int32_t l_2169 = (-1L);
            int32_t l_2170 = 1L;
            int32_t l_2173 = 0x28ED0E52L;
            int32_t l_2174[9][4][4] = {{{(-1L),1L,0x364F4DC6L,1L},{1L,0L,0x4930A91DL,1L},{0L,1L,0xEDBE4E10L,0xEDBE4E10L},{1L,1L,0x2C276686L,1L}},{{(-1L),0L,0xEDBE4E10L,1L},{0L,1L,0x4930A91DL,0xEDBE4E10L},{(-1L),1L,0x364F4DC6L,1L},{1L,0L,0x4930A91DL,1L}},{{0L,1L,0xEDBE4E10L,0xEDBE4E10L},{1L,1L,0x2C276686L,1L},{(-1L),0L,0xEDBE4E10L,1L},{0L,1L,0x4930A91DL,0xEDBE4E10L}},{{(-1L),1L,0x364F4DC6L,1L},{1L,0L,0x4930A91DL,1L},{0L,1L,0xEDBE4E10L,0xEDBE4E10L},{1L,1L,0x2C276686L,1L}},{{(-1L),0L,0xEDBE4E10L,1L},{0L,1L,0x4930A91DL,0xEDBE4E10L},{(-1L),1L,0x364F4DC6L,1L},{1L,0L,0x4930A91DL,1L}},{{0L,1L,0xEDBE4E10L,0xEDBE4E10L},{1L,1L,0x2C276686L,1L},{(-1L),0L,0xEDBE4E10L,1L},{0L,1L,0x4930A91DL,0xEDBE4E10L}},{{(-1L),1L,0x364F4DC6L,1L},{1L,0L,0x4930A91DL,1L},{0L,1L,0xEDBE4E10L,0xEDBE4E10L},{1L,1L,0x2C276686L,1L}},{{(-1L),0L,0xEDBE4E10L,1L},{0x2F065795L,0L,0x2C276686L,0x364F4DC6L},{0L,0L,1L,0xEDBE4E10L},{0L,0x2F065795L,0x2C276686L,0xEDBE4E10L}},{{0x1D4CAB2AL,0L,0x364F4DC6L,0x364F4DC6L},{0L,0L,0xF1915A3EL,0xEDBE4E10L},{0L,0x2F065795L,0x364F4DC6L,0xEDBE4E10L},{0x2F065795L,0L,0x2C276686L,0x364F4DC6L}}};
            uint16_t l_2183 = 0UL;
            int i, j, k;
            if (((l_2087[0] = ((*g_772) || ((((*l_2047) |= (l_2061[2] == l_2061[0])) , g_1420[5][0]) & (safe_add_func_int32_t_s_s((safe_sub_func_uint8_t_u_u((((safe_lshift_func_int16_t_s_u((l_2064 == (safe_add_func_uint16_t_u_u(((((***g_1684) == (safe_div_func_uint8_t_u_u((l_2083 < (((l_2064 || ((((l_2067 , l_2084[4]) != l_2084[2]) <= g_2028[1]) || l_2064)) ^ l_2083) != g_172)), g_2085[0]))) != l_2086) ^ l_2067), g_262[1]))), g_71)) & 1UL) & (*g_772)), 0xC7L)), l_2086))))) , 0x1B4BC71FL))
            { /* block id: 979 */
                for (l_2067 = 0; (l_2067 < 2); l_2067 = safe_add_func_int64_t_s_s(l_2067, 2))
                { /* block id: 982 */
                    int32_t * const l_2090 = (void*)0;
                    (*g_2091) = l_2090;
                }
            }
            else
            { /* block id: 985 */
                float l_2092[6][6] = {{0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64,0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64},{0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64,0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64},{0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64,0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64},{0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64,0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64},{0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64,0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64},{0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64,0xF.274C11p-81,0xF.274C11p-81,0x4.6ED9A8p-64}};
                int i, j;
                --l_2095[0];
                return l_2094;
            }
            for (g_267 = 0; (g_267 <= 7); g_267 += 1)
            { /* block id: 991 */
                int32_t l_2112[2][5];
                int8_t **l_2138[1][2];
                int32_t ** const **l_2194 = &g_537;
                int i, j;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 5; j++)
                        l_2112[i][j] = 1L;
                }
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2138[i][j] = &l_1979;
                }
                l_2064 ^= (safe_unary_minus_func_int8_t_s(((((l_2095[g_267] , (&g_1061[0] != &g_1061[1])) | (safe_mod_func_int8_t_s_s(((g_4 <= ((g_731 = (!(*g_772))) > l_2094)) , (l_2112[1][2] = (safe_lshift_func_int8_t_s_u((safe_div_func_int16_t_s_s((l_2067 && (safe_mul_func_uint16_t_u_u((((safe_mul_func_uint8_t_u_u(g_1420[7][0], (((void*)0 == l_2110) , l_2065))) != 6UL) == l_2093), g_4))), g_129[1])), 2)))), l_2095[g_267]))) != l_2094) == (*g_772))));
            }
        }
        else
        { /* block id: 1060 */
            int32_t l_2228 = 0xAFE0297EL;
            --l_2229;
        }
        return l_2161[4];
    }
lbl_2266:
    l_2260 = ((safe_rshift_func_uint8_t_u_s(((((*g_772) ^ (safe_rshift_func_uint16_t_u_u(((((safe_sub_func_uint8_t_u_u(((*l_2258) = (((l_2238[6][0][1] , (safe_sub_func_uint16_t_u_u(g_560, (((((*g_392) = (l_2248 = (((*g_1273) = (l_2243 , (*g_1273))) == ((*l_2247) = g_2244[1][3][0])))) || l_2249) || (safe_div_func_uint64_t_u_u((((safe_add_func_uint64_t_u_u(((*g_772) & 1UL), 5UL)) & (**g_2245)) , l_2254), (**g_1623)))) <= 0L)))) , g_205[6]) , l_2255[5])), 0x5BL)) , 0x2BC7L) == 0x65DBL) < 6L), g_262[1]))) , &l_1999[5][5][2]) != &l_1999[5][5][2]), 5)) == (***g_1622));
    for (l_1942 = (-28); (l_1942 == 22); ++l_1942)
    { /* block id: 1073 */
        uint32_t *l_2263 = &g_117;
        int32_t l_2272 = 1L;
        int8_t l_2275 = 8L;
        int16_t l_2310 = 1L;
        int32_t ***l_2317 = &g_380;
        uint64_t *****l_2321 = &l_2058;
        int32_t l_2339[3];
        int8_t l_2509 = 1L;
        int8_t l_2528 = 0xADL;
        uint16_t l_2559 = 1UL;
        uint32_t l_2605 = 18446744073709551613UL;
        int32_t *l_2650 = &g_557;
        uint64_t l_2705 = 6UL;
        int32_t l_2706 = 0x59D0B230L;
        int32_t *l_2709 = &g_560;
        int32_t l_2719 = (-8L);
        int i;
        for (i = 0; i < 3; i++)
            l_2339[i] = 1L;
        for (l_1915 = 0; (l_1915 <= 1); l_1915 += 1)
        { /* block id: 1076 */
            uint32_t l_2265 = 4294967295UL;
            int8_t ****l_2295 = &g_1165;
            int8_t *****l_2294 = &l_2295;
            int32_t l_2338[10];
            uint32_t l_2356 = 0x1DBFDB99L;
            float *l_2357[7] = {&g_93,&g_93,(void*)0,&g_93,&g_93,(void*)0,&g_93};
            int16_t *l_2360 = &l_2310;
            int64_t l_2387 = 0xFEF1D39526B3615CLL;
            uint64_t l_2389 = 0xE264A91AF208BCBCLL;
            int32_t l_2390 = (-3L);
            int32_t l_2495 = 0x3C40093EL;
            int8_t l_2558 = 3L;
            int32_t ***l_2561[1][10][9] = {{{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,(void*)0,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380},{&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380,&g_380}}};
            uint16_t l_2587 = 0x7832L;
            uint32_t l_2588 = 18446744073709551609UL;
            uint64_t ***l_2589 = (void*)0;
            uint32_t *l_2681 = (void*)0;
            uint8_t *l_2702 = &g_129[0];
            int64_t l_2703[10][2] = {{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL},{0x527BB633B86B3903LL,0x837E25891E10B2E2LL}};
            float l_2714[5][5][3];
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_2338[i] = 0xE892552BL;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 5; j++)
                {
                    for (k = 0; k < 3; k++)
                        l_2714[i][j][k] = 0x4.8FBEFBp+34;
                }
            }
            for (g_409 = 0; (g_409 <= 2); g_409 += 1)
            { /* block id: 1079 */
                int32_t **l_2268[10][3] = {{&g_320,&g_320,&g_320},{&g_320,&g_320,&g_320},{&g_320,&g_320,&g_320},{&g_320,&g_320,(void*)0},{&g_320,&g_320,&g_320},{(void*)0,&g_320,(void*)0},{&g_320,&g_320,&g_320},{(void*)0,(void*)0,(void*)0},{&g_320,&g_320,&g_320},{&g_320,(void*)0,&g_320}};
                int32_t l_2307 = 0x251144EAL;
                uint16_t *l_2309 = &g_731;
                int32_t l_2311 = 0x9319E89AL;
                int8_t * const *l_2331 = &l_1979;
                int i, j;
                for (l_2248 = 1; (l_2248 >= 0); l_2248 -= 1)
                { /* block id: 1082 */
                    uint32_t **l_2264 = &g_2246[0];
                    int32_t *l_2267 = (void*)0;
                    l_2265 ^= (((*l_2264) = l_2263) == (**g_387));
                    for (l_2178 = 0; (l_2178 >= 0); l_2178 -= 1)
                    { /* block id: 1087 */
                        int i;
                        if (g_262[l_2178])
                            break;
                        if (g_557)
                            goto lbl_2266;
                        l_2267 = g_2246[l_2178];
                        (*l_2267) ^= (((((**g_387) != (void*)0) , l_2268[2][2]) != (void*)0) || ((g_572--) || (*g_772)));
                    }
                    if ((((!l_2272) && l_2265) != (safe_sub_func_int32_t_s_s(l_2272, l_2265))))
                    { /* block id: 1094 */
                        uint32_t l_2276 = 4UL;
                        l_2276--;
                        (**l_404) = (void*)0;
                        if (l_2272)
                            continue;
                        (*g_1113) = (-l_2276);
                    }
                    else
                    { /* block id: 1099 */
                        (**g_1683) = ((*g_1622) = (*g_1622));
                    }
                    return (*g_436);
                }
                if ((safe_mod_func_int64_t_s_s(l_2265, (safe_rshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((0x9CABB522L | ((l_2265 >= (safe_mul_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((safe_add_func_uint32_t_u_u((((((l_2294 == (void*)0) , (safe_mod_func_uint8_t_u_u(((++g_731) , (((!(((*l_2258) &= (g_409 & (safe_rshift_func_int16_t_s_u(((((g_2028[1] && ((*l_2309) ^= (0x2EEDA3B7L | (((safe_mul_func_float_f_f((((l_2272 |= (safe_mod_func_int16_t_s_s(0xE3C7L, g_95))) , l_2307) , l_2275), l_2308)) , (**g_1623)) && 0x088E5D2391D87734LL)))) <= l_2265) & l_2310) < 0xB823CF101AF51229LL), l_2265)))) > (-1L))) != l_2275) || l_2310)), 0x6FL))) || l_2265) , l_2311) ^ g_129[3]), l_2275)), 2UL)), g_1311))) && l_2310)), g_92)) < (*g_1624)), 6)), l_2312[8])))))
                { /* block id: 1109 */
                    uint32_t *l_2318 = &l_2260;
                    uint64_t ** const **l_2320 = (void*)0;
                    uint64_t ** const ***l_2319[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int32_t l_2330[8] = {9L,0xC5D67E79L,9L,0xC5D67E79L,9L,0xC5D67E79L,9L,0xC5D67E79L};
                    int8_t * const **l_2332 = &l_2331;
                    int8_t * const l_2335 = (void*)0;
                    int8_t * const *l_2334[6];
                    int8_t * const **l_2333 = &l_2334[0];
                    uint8_t l_2340[2];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_2334[i] = &l_2335;
                    for (i = 0; i < 2; i++)
                        l_2340[i] = 0xA9L;
                    l_2338[0] |= (safe_div_func_uint8_t_u_u(g_433, (safe_sub_func_int32_t_s_s(((((*l_2318) ^= (l_2317 == (*g_536))) , l_2319[9]) != l_2321), (safe_rshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_s((safe_sub_func_int16_t_s_s(g_1311, (((((((safe_mod_func_uint16_t_u_u((l_2330[1] != ((*l_2263) = ((((g_2336 = ((*l_2333) = ((*l_2332) = l_2331))) == (***l_2294)) == (-5L)) | 0xBFL))), 0x3A4BL)) != l_2265) , &g_274) == l_2318) , 0x8B91L) >= g_172) | g_2028[1]))), l_2310)), g_59))))));
                    l_2340[1]--;
                    (*g_2343) = l_2338[0];
                }
                else
                { /* block id: 1118 */
                    int8_t l_2346 = 0xBAL;
                    int64_t ** const **l_2354 = (void*)0;
                    int32_t l_2355 = 0L;
                    l_2355 ^= (((safe_sub_func_int16_t_s_s(l_2346, g_409)) || ((safe_mul_func_uint8_t_u_u(g_95, (l_2265 != 0x8D7666A7L))) | ((safe_add_func_uint8_t_u_u(((g_2351 = &g_1622) == (l_2354 = g_2352)), g_2028[0])) ^ l_2265))) <= g_445[1][1][1]);
                }
                if (l_2338[5])
                    continue;
                (*g_436) = (((((void*)0 != &g_387) > l_2356) > (0xC31D6C16A3DAB9A7LL > (***g_1622))) , (l_2338[3] != 0x12L));
            }
            (*g_457) = 0x0.FA5BFEp-31;
        }
        return l_2719;
    }
    (*g_436) = ((((*l_2720) = &l_2059) == ((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(((safe_sub_func_uint64_t_u_u(((safe_sub_func_uint64_t_u_u((safe_div_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((*g_2336) == (void*)0), (**g_1623))), (1L && l_2734))), g_2735)) || (8L != (safe_add_func_int64_t_s_s((((!(safe_rshift_func_int16_t_s_s(((*l_2755) = (safe_rshift_func_uint8_t_u_s((((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((((((((safe_add_func_int32_t_s_s(l_2750[3], (*g_392))) == l_2751) > l_2752[0]) != 0x7356L) || l_2753) & g_1991[3][4]) | (**g_2596)), 0)), 0UL)) >= g_2085[0]) < g_2019), g_2754[3][6]))), 11))) , g_262[0]) || 0xD3L), 0xC26FB53E2E3DCF37LL)))), (***g_1684))) && g_121), 6)), g_1420[1][0])) , (*g_2192))) || (*g_2440));
    return (*g_392);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_262
 */
static int32_t  func_6(int16_t  p_7, uint16_t  p_8)
{ /* block id: 920 */
    uint32_t ****l_1898[7];
    int16_t *l_1899 = &g_262[1];
    int32_t l_1900 = 0x36B5FE0BL;
    int i;
    for (i = 0; i < 7; i++)
        l_1898[i] = (void*)0;
    l_1900 |= (safe_lshift_func_int16_t_s_u(((*l_1899) = (safe_rshift_func_int8_t_s_s(((void*)0 == l_1898[6]), 2))), 15));
    return p_7;
}


/* ------------------------------------------ */
/* 
 * reads : g_274 g_1400 g_1401 g_1402 g_59 g_1675 g_436 g_117 g_172 g_1420 g_1273 g_1274 g_1682 g_572 g_1684 g_1685 g_1624 g_205 g_1140 g_445 g_180 g_772 g_773 g_123 g_457 g_93 g_535 g_536 g_537 g_380 g_320 g_1311 g_560 g_63 g_4
 * writes: g_274 g_881 g_59 g_557 g_320 g_63 g_117 g_172 g_1275 g_1682 g_262 g_572 g_129 g_180 g_1311
 */
static int16_t  func_9(uint8_t  p_10, uint64_t  p_11, uint64_t  p_12)
{ /* block id: 815 */
    uint16_t l_1671 = 65535UL;
    int32_t * const l_1674[8] = {&g_1311,&g_96[5],&g_1311,&g_96[5],&g_1311,&g_96[5],&g_1311,&g_96[5]};
    const uint8_t l_1729 = 9UL;
    int64_t l_1730 = (-1L);
    uint32_t l_1771 = 3UL;
    uint32_t l_1852[9];
    int32_t **** const *l_1862 = &g_1089[2][1];
    int32_t *l_1864 = &g_63[2][2];
    int16_t l_1887 = 2L;
    int32_t **l_1893 = &l_1864;
    int i;
    for (i = 0; i < 9; i++)
        l_1852[i] = 0x8A31C3FDL;
    for (g_274 = 0; (g_274 >= 0); g_274 -= 1)
    { /* block id: 818 */
        const int32_t *l_1667[6][7][2] = {{{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]}},{{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]}},{{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]}},{{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]}},{{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]}},{{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]},{(void*)0,&g_63[1][2]},{&g_267,&g_63[1][2]},{(void*)0,&g_63[2][2]},{&g_267,&g_63[2][2]}}};
        int32_t l_1668 = 0x631BEDECL;
        uint64_t *l_1679 = &g_572;
        uint64_t **l_1678[7][6] = {{&l_1679,&l_1679,&l_1679,(void*)0,&l_1679,(void*)0},{(void*)0,&l_1679,&l_1679,&l_1679,&l_1679,&l_1679},{&l_1679,&l_1679,&l_1679,&l_1679,&l_1679,(void*)0},{(void*)0,&l_1679,(void*)0,(void*)0,(void*)0,&l_1679},{&l_1679,(void*)0,(void*)0,&l_1679,&l_1679,(void*)0},{&l_1679,&l_1679,&l_1679,&l_1679,&l_1679,&l_1679},{&l_1679,&l_1679,&l_1679,&l_1679,&l_1679,(void*)0}};
        uint8_t *l_1727 = &g_129[1];
        uint32_t *l_1728 = &g_117;
        uint32_t l_1737[2][5] = {{1UL,0xBFCD793DL,0xBFCD793DL,1UL,0UL},{1UL,0xBFCD793DL,0xBFCD793DL,1UL,0UL}};
        int32_t l_1772 = 8L;
        uint32_t l_1850 = 0UL;
        uint8_t l_1871 = 0xCFL;
        uint32_t l_1874 = 0xDC4B161BL;
        int i, j, k;
        for (p_11 = 0; (p_11 <= 2); p_11 += 1)
        { /* block id: 821 */
            uint32_t **l_1681 = (void*)0;
            int32_t l_1707[1][8];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 8; j++)
                    l_1707[i][j] = 0x6EEF6C01L;
            }
            (***g_1400) = l_1667[4][1][0];
            for (g_59 = 0; (g_59 <= 2); g_59 += 1)
            { /* block id: 825 */
                float *l_1676[8] = {&g_340,&g_340,&g_340,&g_340,&g_340,&g_340,&g_340,&g_340};
                int32_t l_1688 = 1L;
                float l_1708 = 0x6.6p-1;
                int i, j;
                for (g_557 = 0; (g_557 <= 0); g_557 += 1)
                { /* block id: 828 */
                    int32_t *l_1669 = &l_1668;
                    int32_t *l_1670[8];
                    int i;
                    for (i = 0; i < 8; i++)
                        l_1670[i] = &g_63[1][2];
                    l_1671++;
                    (*g_1675) = l_1674[7];
                }
                (*g_436) = ((void*)0 != l_1676[6]);
                for (g_117 = 0; (g_117 <= 0); g_117 += 1)
                { /* block id: 835 */
                    int8_t ***l_1677 = &g_1166;
                    int i, j;
                    for (g_172 = 0; (g_172 <= 0); g_172 += 1)
                    { /* block id: 838 */
                        int i, j;
                        return g_1420[(g_172 + 7)][g_117];
                    }
                    if ((0x00L < (l_1668 &= g_1420[(g_274 + 5)][g_274])))
                    { /* block id: 842 */
                        uint64_t ***l_1680 = &l_1678[1][1];
                        l_1668 |= ((((0xC8B3DC5C07D3E40FLL | ((void*)0 == l_1677)) , (p_10 , (void*)0)) == ((*l_1680) = l_1678[1][2])) , 0x15145A35L);
                        (**g_1273) = l_1681;
                        g_1682 = g_1682;
                        if (g_1420[(g_274 + 5)][g_274])
                            break;
                    }
                    else
                    { /* block id: 848 */
                        int16_t *l_1702 = &g_262[0];
                        int32_t l_1703 = (-10L);
                        l_1668 ^= (safe_add_func_int32_t_s_s((p_10 | l_1688), (safe_sub_func_int32_t_s_s(((!(p_10 & ((safe_sub_func_int64_t_s_s(((((((safe_div_func_int64_t_s_s((((-1L) | (((((safe_mod_func_int64_t_s_s((safe_div_func_int16_t_s_s((((((g_1420[(g_274 + 5)][g_274] || ((((*l_1702) = (-1L)) , ((((l_1703 != (((++(*l_1679)) && (~(0x18FEL || (18446744073709551615UL | 18446744073709551613UL)))) >= 0xE072D67CC433B303LL)) < l_1688) , p_11) , p_10)) > p_12)) && 0x59L) || l_1707[0][6]) > (***g_1684)) , g_1140[3]), 0xDE44L)), 0x807C0212DF20FDE7LL)) > 0x7695EF05L) & g_445[1][1][1]) > g_1420[5][0]) <= 8L)) , l_1688), l_1703)) ^ g_180) && p_10) && p_11) == (*g_772)) != 2L), l_1703)) , (-1L)))) ^ g_123), l_1688))));
                    }
                }
                (*g_436) = g_1420[(p_11 + 6)][g_274];
            }
            return g_1420[(p_11 + 6)][g_274];
        }
        (**g_1401) = &l_1668;
        l_1730 ^= (((*g_772) && p_11) < (255UL | (safe_lshift_func_uint8_t_u_u(((*g_457) , (safe_lshift_func_uint16_t_u_u((((safe_sub_func_int64_t_s_s((safe_div_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((l_1668 ^= ((p_10 <= ((((*l_1728) |= (safe_div_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(((((*l_1727) = (((((p_12 >= ((p_10 & ((safe_mul_func_int8_t_s_s((-7L), (safe_rshift_func_uint16_t_u_u(g_205[6], 3)))) , 65530UL)) , p_12)) <= p_12) == (*****g_535)) != 8L) & p_12)) == 0xD5L) || 0x705AL), 0xE34E373CL)), p_11))) <= p_11) , l_1729)) , p_10)), 0L)), 0x92L)), p_12)) & g_560) , g_172), g_1311))), 2))));
        for (g_180 = 0; (g_180 <= 0); g_180 += 1)
        { /* block id: 865 */
            uint64_t l_1744[1][1];
            uint16_t l_1745 = 0x07E6L;
            int32_t l_1857 = 0xBA01D261L;
            int64_t *l_1884 = &g_610[5][0];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1744[i][j] = 0x1D3AFDFFC057E33ELL;
            }
        }
    }
    (***g_537) = (safe_mul_func_int8_t_s_s((p_11 | (*l_1864)), p_12));
    (*g_1402) = ((*l_1893) = ((*g_380) = l_1864));
    return g_4;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_13(uint8_t  p_14, const int32_t  p_15, int8_t  p_16)
{ /* block id: 812 */
    int32_t *l_1655 = &g_96[5];
    int32_t *l_1656 = &g_63[2][2];
    int32_t *l_1657 = &g_1311;
    int32_t *l_1658 = &g_63[2][3];
    int32_t *l_1659 = (void*)0;
    int32_t l_1660 = 0x4B871CDBL;
    int32_t *l_1661 = &l_1660;
    int32_t *l_1662[10][8][3] = {{{&g_560,&g_267,&g_96[5]},{&g_96[0],&g_557,(void*)0},{&g_560,&g_1311,&g_96[5]},{&g_96[0],&g_1311,&g_1311},{&g_560,&g_63[0][3],&g_1311},{&g_560,&g_557,&g_63[2][2]},{&g_96[0],(void*)0,&g_96[5]},{&g_123,&g_96[5],&l_1660}},{{(void*)0,(void*)0,&g_96[5]},{&g_557,&g_557,&g_96[3]},{&g_560,&g_63[0][3],&g_96[0]},{&g_557,&g_1311,&g_96[5]},{(void*)0,&g_1311,&g_1311},{&g_123,&g_557,&g_96[5]},{&g_96[0],&g_267,&g_96[0]},{&l_1660,&g_96[5],&g_96[3]}},{{&g_560,&g_63[0][1],&g_96[5]},{&g_1311,&g_557,&l_1660},{&g_560,&g_96[2],&g_96[5]},{&g_1311,&g_1311,&g_63[2][2]},{&g_560,(void*)0,&g_1311},{&l_1660,&g_557,&g_1311},{&g_96[0],&g_96[5],&g_96[5]},{&g_123,&g_96[5],(void*)0}},{{(void*)0,&g_96[5],&g_96[5]},{&g_557,&g_557,&g_123},{&g_560,(void*)0,&g_96[0]},{&g_557,&g_1311,&g_1311},{(void*)0,&g_96[2],&g_1311},{&g_123,&g_557,&g_1311},{&g_96[0],&g_63[0][1],&g_96[0]},{&g_560,&g_96[5],&g_123}},{{&g_560,&g_267,&g_96[5]},{&g_96[0],&g_557,(void*)0},{&g_560,&g_1311,&g_96[5]},{&g_96[0],&g_1311,&g_1311},{&g_560,&g_63[0][3],&g_1311},{&g_560,&g_557,&g_63[2][2]},{&g_96[0],(void*)0,&g_96[5]},{&g_123,&g_96[5],&l_1660}},{{(void*)0,(void*)0,&g_96[5]},{&g_557,&g_557,&g_96[3]},{&g_560,&g_63[0][3],&g_96[0]},{&g_557,&g_1311,&g_96[5]},{(void*)0,&g_1311,&g_1311},{&g_123,&g_557,&g_96[5]},{&g_96[0],&g_267,&g_96[0]},{&l_1660,&g_96[5],&g_96[3]}},{{&g_560,&g_63[0][1],&g_96[5]},{&g_1311,&g_557,&l_1660},{&g_560,&g_96[2],&g_96[5]},{&g_1311,&g_1311,&g_63[2][2]},{&g_560,(void*)0,&g_1311},{&l_1660,&g_557,&g_1311},{&g_96[0],&g_96[5],&g_96[5]},{&g_123,&g_96[5],(void*)0}},{{(void*)0,&g_96[5],&g_96[5]},{&g_557,&g_557,&g_123},{&g_560,(void*)0,&g_96[0]},{&g_557,&g_1311,&g_1311},{(void*)0,&g_96[2],&g_1311},{&g_123,&g_557,&g_1311},{&g_96[0],&g_63[0][1],&g_96[0]},{&g_560,&g_96[5],&g_123}},{{&g_560,&g_267,&g_96[5]},{&g_96[0],&g_557,(void*)0},{&g_560,&g_1311,&g_96[5]},{&g_96[0],&g_1311,&g_1311},{&g_560,&g_63[0][3],&g_1311},{&g_560,&g_557,&g_63[2][2]},{&g_96[0],(void*)0,&g_96[5]},{&g_123,&g_96[5],&l_1660}},{{(void*)0,(void*)0,&g_96[5]},{&g_557,&g_557,&g_96[3]},{&g_560,&g_63[0][3],&g_96[0]},{&g_557,&g_1311,&g_96[5]},{(void*)0,&g_1311,&g_1311},{&g_123,&g_557,&g_96[5]},{&g_96[0],&g_267,&g_96[0]},{&l_1660,&g_96[5],&g_96[3]}}};
    uint16_t l_1663 = 0UL;
    int i, j, k;
    --l_1663;
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads : g_536 g_537 g_380 g_320 g_1311 g_521
 * writes: g_1311
 */
static uint8_t  func_17(int32_t  p_18, int16_t  p_19, uint64_t  p_20, int32_t  p_21)
{ /* block id: 809 */
    uint32_t l_1653 = 0xA0705E60L;
    (****g_536) |= l_1653;
    return g_521;
}


/* ------------------------------------------ */
/* 
 * reads : g_379 g_380 g_320 g_1311
 * writes: g_320 g_1311
 */
static int32_t  func_22(int32_t  p_23, uint16_t  p_24, uint32_t  p_25, uint32_t  p_26)
{ /* block id: 805 */
    int32_t *l_1652 = &g_1311;
    (**g_379) = l_1652;
    (***g_379) |= (-6L);
    return (*l_1652);
}


/* ------------------------------------------ */
/* 
 * reads : g_560 g_319 g_320 g_379 g_380 g_521 g_1311 g_409 g_557
 * writes: g_560 g_320 g_1311 g_881 g_409
 */
static int16_t  func_29(uint64_t  p_30, uint8_t  p_31, uint64_t  p_32, uint32_t  p_33)
{ /* block id: 687 */
    const int32_t *l_1381 = &g_1311;
    int32_t *l_1386 = &g_557;
    uint16_t l_1405 = 65534UL;
    uint32_t l_1425[2];
    int8_t ** const **l_1471 = (void*)0;
    int32_t l_1509[3][8][2] = {{{0L,0L},{0L,1L},{0L,3L},{0L,0L},{3L,1L},{3L,0L},{0L,3L},{0L,1L}},{{0L,0L},{0L,0L},{0L,1L},{0L,3L},{0L,0L},{3L,1L},{3L,0L},{0L,3L}},{{0L,1L},{0L,0L},{0L,0L},{0L,1L},{0L,3L},{0L,0L},{3L,1L},{3L,0L}}};
    float l_1513 = 0xC.337F36p+64;
    uint64_t l_1545 = 0UL;
    uint32_t **l_1551 = (void*)0;
    int32_t l_1573 = 0L;
    int32_t ****l_1580 = &g_379;
    int32_t l_1636 = 9L;
    int64_t l_1640 = 0L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1425[i] = 4UL;
    for (g_560 = 0; (g_560 <= 9); g_560 += 1)
    { /* block id: 690 */
        int32_t l_1375 = 0xB3B9F08DL;
        int32_t l_1376 = 0xCA08AF7AL;
        int32_t l_1379[6] = {(-7L),(-1L),(-7L),(-7L),(-1L),(-7L)};
        int32_t *l_1380 = &g_1311;
        const int32_t **l_1382 = &g_881;
        const int32_t ***l_1398 = &l_1382;
        const int32_t ****l_1397 = &l_1398;
        const int32_t *****l_1396 = &l_1397;
        int8_t * const l_1475[2] = {&g_92,&g_92};
        int8_t * const *l_1474 = &l_1475[0];
        int8_t * const **l_1473[8][10][3] = {{{(void*)0,&l_1474,(void*)0},{(void*)0,(void*)0,&l_1474},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,&l_1474},{(void*)0,&l_1474,&l_1474},{&l_1474,(void*)0,(void*)0},{&l_1474,(void*)0,&l_1474}},{{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,&l_1474},{(void*)0,&l_1474,(void*)0},{&l_1474,(void*)0,(void*)0},{(void*)0,&l_1474,&l_1474},{&l_1474,(void*)0,&l_1474},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,&l_1474},{&l_1474,&l_1474,&l_1474},{(void*)0,(void*)0,&l_1474}},{{&l_1474,&l_1474,(void*)0},{&l_1474,(void*)0,(void*)0},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,(void*)0},{(void*)0,(void*)0,&l_1474},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0}},{{&l_1474,&l_1474,(void*)0},{(void*)0,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,(void*)0}},{{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,&l_1474},{&l_1474,&l_1474,&l_1474},{&l_1474,&l_1474,(void*)0},{(void*)0,(void*)0,&l_1474},{&l_1474,&l_1474,(void*)0},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0}},{{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,(void*)0},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0},{(void*)0,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,(void*)0},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,&l_1474},{(void*)0,&l_1474,&l_1474},{&l_1474,&l_1474,&l_1474},{&l_1474,&l_1474,(void*)0}},{{(void*)0,(void*)0,&l_1474},{&l_1474,&l_1474,(void*)0},{&l_1474,(void*)0,&l_1474},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,&l_1474},{&l_1474,(void*)0,(void*)0},{&l_1474,&l_1474,(void*)0},{&l_1474,&l_1474,(void*)0}}};
        int8_t * const ***l_1472 = &l_1473[7][7][0];
        int32_t l_1507 = (-1L);
        int32_t l_1519 = 0x7F637573L;
        float l_1538 = 0x0.01189Cp+40;
        int32_t l_1543 = 0x92935759L;
        uint32_t *l_1646 = &g_445[2][1][4];
        uint32_t **l_1645 = &l_1646;
        int i, j, k;
        (**g_379) = (*g_319);
        (*l_1380) &= ((l_1376 = (p_30 & l_1375)) && (p_31 , (l_1379[0] ^= (l_1376 = (safe_mul_func_uint8_t_u_u(g_521, p_30))))));
        (*l_1382) = l_1381;
        for (g_409 = 0; (g_409 <= 9); g_409 += 1)
        { /* block id: 699 */
            int32_t *l_1383[2];
            int32_t *****l_1395 = &g_1089[2][3];
            uint64_t *l_1403 = &g_532;
            int8_t ****l_1477 = &g_1165;
            int8_t *****l_1476[8][9][3] = {{{&l_1477,&l_1477,&l_1477},{&l_1477,(void*)0,&l_1477},{&l_1477,&l_1477,&l_1477},{(void*)0,(void*)0,(void*)0},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,(void*)0,&l_1477},{&l_1477,(void*)0,&l_1477}},{{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,(void*)0,&l_1477},{&l_1477,&l_1477,&l_1477},{(void*)0,&l_1477,(void*)0},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477}},{{&l_1477,&l_1477,(void*)0},{&l_1477,(void*)0,&l_1477},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{(void*)0,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477}},{{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,(void*)0,&l_1477},{&l_1477,&l_1477,&l_1477},{(void*)0,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477}},{{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{(void*)0,(void*)0,(void*)0},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{(void*)0,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477}},{{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,(void*)0,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{(void*)0,&l_1477,&l_1477}},{{&l_1477,&l_1477,&l_1477},{(void*)0,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,(void*)0},{&l_1477,(void*)0,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,(void*)0},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477}},{{(void*)0,&l_1477,(void*)0},{(void*)0,(void*)0,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477},{&l_1477,&l_1477,&l_1477}}};
            const int16_t l_1478 = 0L;
            int8_t * const l_1479 = &g_92;
            uint64_t l_1514 = 1UL;
            uint64_t l_1540 = 18446744073709551615UL;
            float l_1569[9] = {0x3.612DE9p-24,(-0x10.4p-1),0x3.612DE9p-24,(-0x10.4p-1),0x3.612DE9p-24,(-0x10.4p-1),0x3.612DE9p-24,(-0x10.4p-1),0x3.612DE9p-24};
            uint8_t l_1591 = 0xC3L;
            uint64_t l_1604 = 18446744073709551609UL;
            uint32_t l_1619 = 18446744073709551613UL;
            int8_t l_1639 = 0xAAL;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1383[i] = &g_96[0];
        }
    }
    return (*l_1386);
}


/* ------------------------------------------ */
/* 
 * reads : g_274 g_560 g_297 g_731 g_63 g_385 g_386 g_610 g_433 g_379 g_1140 g_96 g_772 g_773 g_380 g_59 g_220 g_572 g_262 g_445 g_1273 g_557 g_320 g_1060 g_1061 g_384 g_961 g_1311 g_129 g_267 g_290 g_205 g_537 g_121
 * writes: g_274 g_572 g_560 g_1089 g_117 g_320 g_205 g_262 g_1273 g_1061 g_340 g_93 g_1355
 */
static uint64_t  func_34(int8_t  p_35, uint16_t  p_36, int64_t  p_37, float  p_38)
{ /* block id: 191 */
    const int8_t l_440 = 0xFEL;
    int32_t l_453 = 0x9DC33B45L;
    int32_t l_474[9][3] = {{0x0096FDA9L,0x69907E11L,0x5D09DC03L},{0x5D09DC03L,0x0A624B7FL,0xA08FE13AL},{0x69907E11L,0x0A624B7FL,0x0A624B7FL},{0x0781BA64L,0x0A624B7FL,0xBFECA446L},{0x5D09DC03L,0x0A624B7FL,0xA08FE13AL},{0x69907E11L,0x0A624B7FL,0x0A624B7FL},{0x0781BA64L,0x0A624B7FL,0xBFECA446L},{0x5D09DC03L,0x0A624B7FL,0xA08FE13AL},{0x69907E11L,0x0A624B7FL,0x0A624B7FL}};
    int32_t l_486 = (-1L);
    int32_t l_487 = 0x3ED9309CL;
    int32_t l_488 = 0x35DC7E0BL;
    int32_t l_489 = 1L;
    int32_t l_490 = (-1L);
    int32_t l_493 = 0x3D82ECB3L;
    int32_t l_495 = 0x3CDB0C69L;
    int32_t l_497 = 2L;
    int32_t l_498 = 0x44DC1553L;
    int32_t l_500 = 0x1C0FA9CAL;
    int32_t l_502 = 0xC7E63F5BL;
    int32_t l_504 = 1L;
    int8_t l_505 = 0xE2L;
    int32_t l_506 = 0x6F312882L;
    int32_t l_507 = 0L;
    int32_t l_508[1];
    int32_t * const l_559 = &g_560;
    int32_t * const *l_558 = &l_559;
    const int16_t l_574 = (-4L);
    int8_t *l_585 = &l_505;
    uint64_t *l_620[6][9][4] = {{{&g_297,(void*)0,&g_572,&g_297},{&g_297,(void*)0,(void*)0,&g_572},{(void*)0,&g_532,&g_297,&g_572},{&g_532,&g_297,(void*)0,&g_532},{&g_297,&g_572,&g_572,&g_532},{&g_297,&g_572,&g_532,&g_297},{&g_532,&g_572,&g_532,(void*)0},{(void*)0,&g_297,&g_572,&g_297},{&g_297,&g_297,(void*)0,&g_297}},{{&g_297,&g_532,(void*)0,&g_297},{&g_297,&g_572,&g_572,(void*)0},{(void*)0,&g_572,&g_532,&g_572},{&g_532,&g_572,&g_532,&g_572},{&g_297,(void*)0,&g_572,&g_297},{&g_297,&g_572,(void*)0,&g_572},{&g_532,(void*)0,&g_297,&g_572},{(void*)0,&g_297,(void*)0,(void*)0},{&g_297,&g_532,&g_572,&g_297}},{{&g_297,(void*)0,&g_297,&g_572},{(void*)0,&g_532,&g_572,&g_532},{(void*)0,&g_297,&g_297,&g_572},{&g_297,&g_572,(void*)0,&g_572},{&g_297,&g_532,&g_532,(void*)0},{&g_532,&g_532,&g_572,&g_297},{&g_572,&g_532,(void*)0,&g_572},{&g_572,&g_572,&g_297,&g_297},{&g_572,(void*)0,(void*)0,&g_532}},{{&g_572,&g_297,(void*)0,&g_532},{(void*)0,&g_572,&g_532,(void*)0},{&g_297,(void*)0,&g_572,(void*)0},{&g_297,&g_572,&g_572,&g_297},{&g_532,(void*)0,(void*)0,&g_532},{&g_532,(void*)0,&g_572,&g_572},{(void*)0,&g_297,(void*)0,(void*)0},{&g_572,&g_532,&g_297,(void*)0},{(void*)0,&g_297,&g_297,&g_572}},{{(void*)0,(void*)0,&g_532,&g_532},{(void*)0,(void*)0,(void*)0,&g_297},{(void*)0,&g_572,&g_297,(void*)0},{&g_572,(void*)0,&g_532,(void*)0},{&g_532,&g_572,&g_297,&g_532},{&g_532,&g_297,(void*)0,&g_572},{(void*)0,&g_532,&g_532,&g_572},{&g_572,&g_297,&g_572,&g_532},{&g_297,&g_532,&g_572,(void*)0}},{{(void*)0,&g_532,&g_297,&g_297},{(void*)0,(void*)0,&g_297,(void*)0},{&g_532,&g_532,&g_572,&g_572},{(void*)0,&g_297,&g_297,&g_572},{&g_572,&g_532,&g_297,(void*)0},{&g_297,(void*)0,&g_532,&g_297},{&g_297,&g_572,&g_572,(void*)0},{&g_532,&g_297,(void*)0,(void*)0},{&g_532,(void*)0,&g_297,(void*)0}}};
    uint32_t *l_641[5];
    uint32_t * const *l_640 = &l_641[3];
    uint32_t **l_648 = &l_641[3];
    uint32_t ***l_647[1];
    const int8_t l_653 = 0L;
    int32_t *l_656 = &l_453;
    int32_t *l_658 = &l_498;
    uint8_t l_659[8] = {0xA2L,0x6FL,0xA2L,0x6FL,0xA2L,0x6FL,0xA2L,0x6FL};
    int64_t *l_707 = &g_205[6];
    int64_t **l_706 = &l_707;
    float l_780 = 0x5.2B4B70p+21;
    uint8_t l_781 = 255UL;
    int8_t l_809 = 0L;
    int16_t l_837 = 0x600FL;
    int8_t l_838 = 1L;
    uint32_t l_885[5] = {0xC55F781FL,0xC55F781FL,0xC55F781FL,0xC55F781FL,0xC55F781FL};
    uint32_t l_904 = 18446744073709551612UL;
    int32_t l_952 = 0x0D9D94B7L;
    uint16_t l_956 = 0x5EB7L;
    uint32_t l_1003 = 0x087C9AB1L;
    const uint8_t l_1056[10] = {2UL,2UL,2UL,2UL,2UL,2UL,2UL,2UL,2UL,2UL};
    int32_t *l_1122 = (void*)0;
    const int32_t *l_1135 = (void*)0;
    int64_t l_1161 = (-9L);
    int32_t ****l_1177 = &g_379;
    int32_t *****l_1178 = &g_1089[2][7];
    int64_t l_1183 = 0xB6DA5631A039E3B6LL;
    int32_t l_1184 = 0x72A24FDEL;
    const int32_t * const **l_1213 = (void*)0;
    uint64_t l_1221 = 0UL;
    const uint32_t l_1252 = 0x379CAAC0L;
    int64_t l_1295 = 4L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_508[i] = 0xE271C434L;
    for (i = 0; i < 5; i++)
        l_641[i] = &g_172;
    for (i = 0; i < 1; i++)
        l_647[i] = &l_648;
    for (g_274 = 0; (g_274 <= 2); g_274 += 1)
    { /* block id: 194 */
        int32_t l_437 = 0xEFB1E847L;
        int32_t l_472[4];
        int32_t ** const *l_534 = &g_380;
        int32_t ** const **l_533 = &l_534;
        const int8_t *l_551 = &l_505;
        uint64_t *l_621 = (void*)0;
        float *l_644 = &g_93;
        int32_t *l_657 = &l_504;
        int32_t *l_688 = &l_490;
        int32_t *l_689 = &l_493;
        int32_t *l_690 = &l_497;
        int32_t *l_691[3];
        float l_692 = 0xB.2038E2p-19;
        uint8_t l_693 = 1UL;
        int i;
        for (i = 0; i < 4; i++)
            l_472[i] = 0x000F6DF1L;
        for (i = 0; i < 3; i++)
            l_691[i] = &l_486;
    }
    for (l_490 = 0; (l_490 <= 2); l_490 += 1)
    { /* block id: 311 */
        int64_t **l_708 = &l_707;
        int32_t l_714[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
        int32_t l_804 = 6L;
        int64_t l_806 = (-6L);
        uint32_t *l_830[1][3][7] = {{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117,&g_117},{&g_445[1][1][5],&g_445[1][1][5],&g_445[1][1][5],&g_445[1][1][5],&g_445[1][1][5],&g_445[1][1][5],&g_445[1][1][5]},{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}}};
        uint8_t l_859 = 0xB6L;
        const int32_t l_1036 = 1L;
        uint64_t ***l_1086 = (void*)0;
        uint64_t *** const *l_1085 = &l_1086;
        uint64_t *** const **l_1084[4][4] = {{&l_1085,&l_1085,&l_1085,&l_1085},{&l_1085,&l_1085,&l_1085,&l_1085},{&l_1085,&l_1085,&l_1085,&l_1085},{&l_1085,&l_1085,&l_1085,&l_1085}};
        uint8_t l_1098[2][3][7] = {{{255UL,0xAAL,255UL,251UL,251UL,255UL,0xAAL},{0xA4L,0x03L,246UL,255UL,9UL,250UL,9UL},{255UL,251UL,251UL,255UL,0xAAL,255UL,251UL}},{{0xBAL,0UL,0xA4L,255UL,0xA4L,0UL,0xBAL},{0x0CL,251UL,1UL,251UL,0x0CL,0x0CL,251UL},{0x1BL,0x03L,0x1BL,250UL,0xA4L,0xA9L,9UL}}};
        const int32_t **l_1120 = &g_881;
        const int32_t ** const *l_1119[2];
        const int32_t ** const **l_1118 = &l_1119[1];
        int32_t l_1130 = 0xBEAFFFE8L;
        float l_1131 = 0x1.Cp+1;
        uint32_t l_1132[6];
        int32_t l_1147[9][1] = {{1L},{(-5L)},{1L},{(-5L)},{1L},{(-5L)},{1L},{(-5L)},{1L}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1119[i] = &l_1120;
        for (i = 0; i < 6; i++)
            l_1132[i] = 0xE6DB82D8L;
        for (g_572 = 0; (g_572 <= 3); g_572 += 1)
        { /* block id: 314 */
            int i, j;
            return l_474[(l_490 + 4)][l_490];
        }
        (*l_658) ^= (((safe_sub_func_int16_t_s_s((p_35 && (**l_558)), (g_297 , 0UL))) < (safe_div_func_int16_t_s_s(((void*)0 == &p_37), (((safe_sub_func_int32_t_s_s((safe_sub_func_int16_t_s_s(((safe_sub_func_int16_t_s_s((l_706 == l_708), (!(safe_mod_func_uint16_t_u_u(((safe_div_func_int32_t_s_s(0x2FA2C2C5L, 1UL)) , p_37), l_714[7]))))) != (*l_559)), 0x978EL)), 4294967295UL)) & l_714[8]) || p_35)))) < (-1L));
    }
    if (((((*l_558) == (void*)0) <= (255UL | (safe_mod_func_uint32_t_u_u((g_117 = ((safe_mod_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(((p_35 < ((((**l_558) = p_35) , (safe_lshift_func_int16_t_s_u((**l_558), ((((*l_1178) = l_1177) == l_1177) , (safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s((p_36 ^ l_1183), (**l_558))), p_36)))))) , 65534UL)) , 0UL), p_37)), g_731)) , g_63[2][2]), p_35)) < l_1184)), p_37)))) <= (-1L)))
    { /* block id: 623 */
        uint16_t l_1193 = 1UL;
        const int32_t * const ***l_1214 = &l_1213;
        uint64_t l_1215 = 18446744073709551606UL;
        int32_t l_1216 = (-1L);
        uint16_t *l_1217[6];
        int32_t l_1218 = 0xA8AA8649L;
        uint64_t l_1219 = 1UL;
        int32_t l_1220 = 1L;
        int32_t *l_1222[6][3];
        int64_t l_1223 = 0x633651F90C112FFDLL;
        uint32_t ***l_1271 = &l_648;
        int8_t ****l_1325[10][8] = {{(void*)0,&g_1165,&g_1165,&g_1165,&g_1165,&g_1165,(void*)0,&g_1165},{&g_1165,&g_1165,(void*)0,&g_1165,&g_1165,&g_1165,&g_1165,(void*)0},{&g_1165,&g_1165,&g_1165,(void*)0,&g_1165,(void*)0,(void*)0,(void*)0},{&g_1165,(void*)0,(void*)0,(void*)0,&g_1165,&g_1165,&g_1165,(void*)0},{(void*)0,&g_1165,(void*)0,(void*)0,(void*)0,(void*)0,&g_1165,(void*)0},{&g_1165,&g_1165,(void*)0,&g_1165,&g_1165,&g_1165,&g_1165,&g_1165},{(void*)0,&g_1165,(void*)0,(void*)0,&g_1165,&g_1165,(void*)0,(void*)0},{(void*)0,&g_1165,&g_1165,&g_1165,&g_1165,(void*)0,&g_1165,&g_1165},{(void*)0,&g_1165,(void*)0,&g_1165,&g_1165,&g_1165,(void*)0,&g_1165},{(void*)0,(void*)0,&g_1165,&g_1165,&g_1165,(void*)0,(void*)0,&g_1165}};
        int i, j;
        for (i = 0; i < 6; i++)
            l_1217[i] = (void*)0;
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 3; j++)
                l_1222[i][j] = (void*)0;
        }
        if ((l_1223 ^= (((safe_sub_func_int32_t_s_s((l_1220 = (((safe_sub_func_int32_t_s_s(((*l_656) &= (safe_div_func_uint64_t_u_u((safe_div_func_int32_t_s_s(l_1193, ((safe_sub_func_uint64_t_u_u((g_572 = l_1193), ((*l_658) > (((l_1218 |= ((*g_385) != ((((safe_lshift_func_int8_t_s_s(p_37, 2)) , g_610[0][0]) | (((+((l_1193 != (((g_433 | (((l_1216 |= (safe_add_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((safe_mod_func_uint64_t_u_u((safe_add_func_int64_t_s_s((((*l_1214) = l_1213) == (*l_1177)), l_1215)), p_37)), p_37)), 4)), 0L))) , p_37) != p_36)) >= (*l_658)) >= p_35)) || p_37)) == p_37) >= (*l_658))) , (void*)0))) , l_1219) < p_37)))) && g_1140[8]))), 1L))), g_96[5])) , (*g_772)) < (*l_559))), l_1221)) || p_36) && 0xAE3EL)))
        { /* block id: 631 */
lbl_1358:
            (***l_1177) = (void*)0;
        }
        else
        { /* block id: 633 */
            int8_t l_1251[4][7][3] = {{{0x50L,0xF0L,0xF0L},{0x50L,0L,9L},{0x9FL,0x21L,0x84L},{9L,9L,0xDCL},{0x74L,(-1L),0x47L},{0xF0L,9L,0xC0L},{0xDCL,0x21L,0x74L}},{{0x55L,0L,0x4DL},{0x4DL,0xF0L,0x4DL},{0x43L,9L,0x74L},{0x91L,0x84L,0xC0L},{0L,0xDCL,0x47L},{0x84L,0x47L,0xDCL},{0L,0xC0L,0x84L}},{{0x91L,0x74L,9L},{0x43L,0x4DL,0xF0L},{0x4DL,0x4DL,0L},{0x55L,0x74L,0x21L},{0xDCL,0xC0L,9L},{0xF0L,0x47L,(-1L)},{0x74L,0xDCL,9L}},{{9L,0x84L,0x21L},{0x9FL,9L,0L},{0x50L,0xF0L,0xF0L},{0x50L,0L,9L},{0x9FL,0x21L,0x84L},{9L,9L,0xDCL},{0x74L,(-1L),0x47L}}};
            int16_t *l_1253 = &g_262[0];
            int16_t *l_1254 = &l_837;
            uint32_t ***l_1272 = (void*)0;
            int32_t l_1278 = 7L;
            int64_t l_1297[1];
            int32_t *l_1359 = (void*)0;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1297[i] = (-10L);
            if ((!((((((safe_mod_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(g_59, (safe_lshift_func_int16_t_s_u(((*l_1254) = (g_63[2][3] & ((*l_1253) ^= ((p_36 , ((safe_sub_func_int32_t_s_s(1L, (safe_mod_func_int16_t_s_s((safe_sub_func_int64_t_s_s(((*l_707) = (((+(~(safe_div_func_int64_t_s_s((safe_lshift_func_int8_t_s_u((((-2L) && (safe_div_func_uint64_t_u_u((safe_div_func_int64_t_s_s(((safe_div_func_int8_t_s_s((((l_1251[0][6][0] && l_1251[3][3][1]) , 0xD1L) , ((*l_559) &= (*l_658))), g_220[0])) <= 0L), p_37)), l_1251[3][5][0]))) > p_37), 1)), 1UL)))) , 1L) == p_36)), p_37)), g_572)))) , l_1252)) < 0xDEL)))), p_35)))), (*l_656))) , &g_385) == (void*)0) & p_37) && 2UL) < p_37)))
            { /* block id: 638 */
                uint8_t l_1255 = 255UL;
                ++l_1255;
                return l_1255;
            }
            else
            { /* block id: 641 */
                const uint64_t l_1270 = 1UL;
                uint32_t *****l_1276 = &g_1273;
                int32_t l_1277[6];
                uint8_t *l_1372 = &l_659[0];
                int i;
                for (i = 0; i < 6; i++)
                    l_1277[i] = 0x7EAE7384L;
                l_1278 = ((safe_sub_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u((l_1277[2] = (((0xC5FC95B7L >= (safe_div_func_uint32_t_u_u(((safe_mul_func_int16_t_s_s((0x61L | (safe_add_func_int8_t_s_s((-1L), ((safe_rshift_func_uint8_t_u_s(255UL, 3)) ^ (l_1270 || (((l_1271 == (g_445[1][0][5] , l_1272)) | ((((*l_1276) = g_1273) == (void*)0) , g_610[0][0])) > g_557)))))), l_1270)) && 0x785DL), p_36))) != p_37) , l_1270)), 1L)) || p_35), p_35)) & p_35);
                for (l_1278 = 0; (l_1278 <= 3); l_1278 += 1)
                { /* block id: 647 */
                    int16_t l_1282 = 0x8CD4L;
                    int32_t l_1296 = 0x0D9BD42DL;
                    int32_t l_1351 = 0x05AD5683L;
                    int8_t ** const *l_1354 = &g_1166;
                    int8_t ** const * const *l_1353 = &l_1354;
                    int8_t ** const * const **l_1352 = &l_1353;
                    for (l_489 = 0; (l_489 <= 3); l_489 += 1)
                    { /* block id: 650 */
                        int i;
                        (*l_658) |= l_1277[2];
                        (***l_1177) = (***l_1177);
                    }
                    if (((safe_lshift_func_uint16_t_u_s((((p_36 < ((((!(l_1282 >= (((*l_559) ^= (((p_35 != ((safe_rshift_func_uint16_t_u_s((safe_add_func_int64_t_s_s((((safe_div_func_uint32_t_u_u(((l_1277[2] = (((-4L) != 0xE61CC24CL) == p_37)) && (l_1296 = (safe_sub_func_int8_t_s_s((((*l_1253) = (safe_lshift_func_int16_t_s_s((safe_sub_func_uint64_t_u_u(0xF7211E7546A93E7CLL, 18446744073709551610UL)), 10))) || ((p_36 , p_37) == (-3L))), l_1295)))), l_1251[0][6][0])) , &g_1089[2][8]) == &l_1177), (*l_658))), 4)) & p_37)) , l_1277[2]) & l_1282)) != l_1282))) , 0x13L) ^ p_35) | p_36)) , l_1297[0]) || p_36), (*l_658))) || 0x698AL))
                    { /* block id: 658 */
                        uint8_t l_1298 = 247UL;
                        (*g_1060) = (*g_1060);
                        --l_1298;
                    }
                    else
                    { /* block id: 661 */
                        uint16_t l_1302[9][7][2] = {{{65535UL,0x3283L},{0UL,0xF051L},{65533UL,65535UL},{0x3E05L,0x32FDL},{0x9095L,0xF051L},{0x109DL,1UL},{65535UL,65535UL}},{{0x65EFL,0x3E05L},{0x08AAL,1UL},{65529UL,65526UL},{0xD433L,65529UL},{0x6CD8L,0x9095L},{0x6CD8L,65529UL},{0xD433L,65526UL}},{{65529UL,1UL},{0x08AAL,0x3E05L},{0x65EFL,65535UL},{65535UL,1UL},{0x109DL,0xF051L},{0x9095L,0x32FDL},{0x3E05L,65535UL}},{{65533UL,0xF051L},{0UL,0x3283L},{65535UL,0x1435L},{1UL,0UL},{0UL,65535UL},{0x9DF0L,0x7DDAL},{65535UL,0x9DF0L}},{{0x301DL,0UL},{0x3F6CL,1UL},{65535UL,0x1435L},{1UL,65535UL},{0UL,65531UL},{1UL,0UL},{0x60A1L,65533UL}},{{0xB041L,0x9096L},{0UL,0x9095L},{65531UL,0x9095L},{0UL,0x9096L},{0xB041L,65533UL},{0x60A1L,0UL},{1UL,65531UL}},{{0UL,65535UL},{1UL,0x1435L},{65535UL,1UL},{0x3F6CL,0UL},{0x301DL,0x9DF0L},{65535UL,0x7DDAL},{0x9DF0L,65535UL}},{{0UL,0UL},{1UL,0x60A1L},{0UL,65533UL},{0x61C7L,1UL},{0UL,7UL},{0UL,0x9095L},{0x619BL,1UL}},{{0xB041L,0xA4D5L},{0UL,0UL},{0x9096L,0UL},{0UL,3UL},{0x9DF0L,0x1435L},{1UL,0x9DF0L},{0x3F6CL,0x619BL}}};
                        float *l_1321 = (void*)0;
                        float *l_1322[6];
                        int32_t l_1350[3][7] = {{0xCEAFB18AL,0xCEAFB18AL,0x338A8CBBL,0xCEAFB18AL,0xCEAFB18AL,0x338A8CBBL,0xCEAFB18AL},{0xCEAFB18AL,(-1L),(-1L),0xCEAFB18AL,(-1L),(-1L),0xCEAFB18AL},{(-1L),0xCEAFB18AL,(-1L),(-1L),0xCEAFB18AL,(-1L),(-1L)}};
                        int i, j, k;
                        for (i = 0; i < 6; i++)
                            l_1322[i] = (void*)0;
                        (*g_384) = ((void*)0 != &l_620[2][3][2]);
                        l_1296 = (g_961 != ((-8L) <= ((p_36 , ((((((0xF279L < ((-(l_1302[8][6][0] == (safe_sub_func_float_f_f(((safe_add_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f(((g_1311 > (safe_add_func_float_f_f((((p_38 = (+((p_37 >= (safe_add_func_float_f_f((-0x5.7p-1), ((safe_div_func_float_f_f((safe_div_func_float_f_f(0x1.0B04C1p-18, (-0x1.3p-1))), g_129[2])) <= (-0x1.Ap-1))))) >= 0xF.51451Dp-42))) <= g_297) < p_36), 0x7.BCEED0p-69))) != 0x4.9B92A5p+21), 0x7.AC60F2p+72)), l_1251[1][1][0])), 0x0.Bp+1)) >= 0x6.522050p+40), g_267)))) , p_36)) != 0xBAB84D1E8C20228CLL) <= 9L) , &g_1089[2][5]) != (void*)0) | 0L)) | g_220[0])));
                        l_1351 ^= (safe_div_func_int32_t_s_s((((l_1350[0][1] = (((((*l_585) = l_1296) , (((void*)0 == l_1325[7][5]) < (safe_add_func_float_f_f(((safe_sub_func_float_f_f((safe_div_func_float_f_f((l_1277[2] = (((safe_mod_func_int16_t_s_s((*l_656), ((safe_mod_func_int8_t_s_s((safe_sub_func_int16_t_s_s(((~(**l_558)) < ((safe_mod_func_uint32_t_u_u((0xC7D9L & g_1311), (((((((safe_div_func_float_f_f((+(safe_div_func_float_f_f((l_1296 = (p_38 = ((*g_290) = l_1282))), (safe_mul_func_float_f_f((safe_div_func_float_f_f(g_205[6], l_1282)), 0xD.05B822p-0))))), g_572)) >= g_610[2][0]) < g_557) <= p_37) , l_1296) && 0x0AL) , l_1296))) >= 0xB397L)), g_129[2])), l_1277[5])) & g_433))) , l_1296) , p_38)), (-0x1.7p-1))), p_35)) >= l_1270), p_37)))) , 0xDC88846FL) && 0x1B922D0BL)) > p_35) != p_37), l_1270));
                    }
                    g_1355 = l_1352;
                    if (l_1215)
                        goto lbl_1358;
                }
                l_1359 = ((**g_537) = (void*)0);
                (*l_658) &= (((+p_35) , g_63[2][2]) == (safe_rshift_func_uint8_t_u_s((safe_div_func_int64_t_s_s((!((*l_1276) == &l_1271)), (safe_sub_func_int64_t_s_s((l_1251[0][2][0] >= (safe_rshift_func_int16_t_s_u((safe_sub_func_int64_t_s_s(((246UL | ((*l_1372) = ((p_36 != 0x91L) , (g_121 < p_35)))) & 0xC97DD44039C37B75LL), p_36)), 9))), l_1277[5])))), 6)));
            }
            return l_1297[0];
        }
    }
    else
    { /* block id: 683 */
        return p_37;
    }
    return (*l_559);
}


/* ------------------------------------------ */
/* 
 * reads : g_262 g_92 g_436 g_63 g_96
 * writes: g_92 g_172 g_433 g_63
 */
static int64_t  func_41(uint8_t  p_42)
{ /* block id: 184 */
    int32_t l_422 = 0xF886A3D9L;
    int32_t l_423 = (-1L);
    int32_t *** const *l_424 = &g_379;
    int8_t *l_429 = &g_92;
    uint32_t *l_430 = &g_172;
    int16_t *l_431 = (void*)0;
    int16_t *l_432 = &g_433;
    (*g_436) ^= (safe_mul_func_uint16_t_u_u(((((p_42 >= (((safe_add_func_int32_t_s_s((safe_div_func_uint32_t_u_u((safe_add_func_int16_t_s_s(((l_423 = l_422) ^ (6L >= ((g_262[1] < (((*l_432) = ((((*l_430) = (((((void*)0 != l_424) || (safe_mul_func_int8_t_s_s(1L, ((*l_429) ^= (safe_rshift_func_int16_t_s_s((8L >= p_42), g_262[0])))))) == l_422) != p_42)) , l_430) == l_430)) == p_42)) ^ (-10L)))), l_422)), p_42)), 4294967295UL)) , &g_409) != (void*)0)) > 0x45L) <= 0L) < l_422), p_42));
    return g_96[3];
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_59 g_63 g_69 g_96 g_93 g_95 g_117 g_121 g_71 g_122 g_92 g_129 g_123 g_172 g_205 g_180 g_220 g_290 g_262 g_319 g_267 g_274 g_379 g_384 g_385 g_387 g_392 g_320 g_380
 * writes: g_63 g_59 g_92 g_95 g_96 g_117 g_121 g_123 g_129 g_172 g_205 g_180 g_220 g_262 g_267 g_274 g_93 g_297 g_122 g_320 g_340 g_379 g_385
 */
static int16_t  func_47(int8_t  p_48)
{ /* block id: 3 */
    int32_t l_49[3];
    int32_t **l_72 = (void*)0;
    uint32_t l_90[2];
    int32_t **l_97 = (void*)0;
    int32_t l_98[8][8][4] = {{{0xC3015DB4L,(-1L),1L,0L},{(-1L),(-1L),0x58B99485L,1L},{4L,0x7D872568L,0xC61C7217L,(-2L)},{0xC61C7217L,(-2L),0xCECC5629L,0x1F8E388CL},{(-1L),0xCECC5629L,0x452CFD91L,0x58B99485L},{(-2L),0x901CC30FL,(-1L),(-1L)},{(-1L),(-1L),(-7L),3L},{0x1F8E388CL,(-1L),0L,(-2L)}},{{0xBCC83174L,0x499C9045L,0x58B99485L,0L},{(-1L),0x499C9045L,0x452CFD91L,(-2L)},{0x499C9045L,(-1L),1L,3L},{(-1L),(-1L),0x499C9045L,(-1L)},{4L,0x901CC30FL,5L,0x58B99485L},{0xBCC83174L,0xCECC5629L,(-7L),0x1F8E388CL},{0x3C83373EL,(-2L),1L,(-2L)},{0xCECC5629L,0x7D872568L,(-1L),1L}},{{(-1L),(-1L),0x499C9045L,0L},{0xC61C7217L,(-1L),0xBCC83174L,0xC3015DB4L},{0xC61C7217L,0xCECC5629L,0x499C9045L,5L},{(-1L),0xC3015DB4L,(-1L),0x58B99485L},{0xCECC5629L,1L,1L,3L},{0x3C83373EL,(-1L),(-7L),0L},{0xBCC83174L,0x7D872568L,5L,0xBCC83174L},{0x452CFD91L,0x1F8E388CL,0x1F8E388CL,0x452CFD91L}},{{0xC3015DB4L,0xC61C7217L,0x8980DF3AL,5L},{0x1F8E388CL,(-1L),0x901CC30FL,0xCE904CDEL},{0x499C9045L,0x58B99485L,0L,0xCE904CDEL},{1L,(-1L),1L,5L},{(-1L),0xC61C7217L,0xBCC83174L,0x452CFD91L},{0x14709020L,0x1F8E388CL,0xCE904CDEL,0xBCC83174L},{0xC61C7217L,0x3C83373EL,0x901CC30FL,0x7D872568L},{0xC3015DB4L,0x499C9045L,0x4DF11966L,0x20146E06L}},{{3L,(-1L),3L,0L},{0x452CFD91L,5L,0L,1L},{0x14709020L,0x4DF11966L,1L,5L},{5L,(-1L),1L,0x7D872568L},{0x14709020L,0xC3015DB4L,0L,1L},{0x452CFD91L,0x3C83373EL,3L,0xC61C7217L},{3L,0xC61C7217L,0x4DF11966L,(-1L)},{0xC3015DB4L,0x4DF11966L,0x901CC30FL,0L}},{{0xC61C7217L,0xC8398D9BL,0xCE904CDEL,0xCE904CDEL},{0x14709020L,0x14709020L,0xBCC83174L,0x20146E06L},{(-1L),(-1L),1L,0xC61C7217L},{1L,0x1F8E388CL,0L,1L},{0x499C9045L,0x1F8E388CL,0x901CC30FL,0xC61C7217L},{0x1F8E388CL,(-1L),0x8980DF3AL,0x20146E06L},{0xC3015DB4L,0x14709020L,0x1F8E388CL,0xCE904CDEL},{0x452CFD91L,0xC8398D9BL,1L,0L}},{{1L,0x4DF11966L,0xBCC83174L,(-1L)},{0x58B99485L,0xC61C7217L,1L,0xC61C7217L},{0x4DF11966L,0x3C83373EL,0xCE904CDEL,1L},{0x499C9045L,0xC3015DB4L,0x1F8E388CL,0x7D872568L},{3L,(-1L),1L,5L},{3L,0x4DF11966L,0x1F8E388CL,1L},{0x499C9045L,5L,0xCE904CDEL,0L},{0x4DF11966L,(-1L),1L,0x20146E06L}},{{0x58B99485L,0x499C9045L,0xBCC83174L,0x7D872568L},{1L,0x3C83373EL,1L,0xBCC83174L},{0x452CFD91L,0x1F8E388CL,0x1F8E388CL,0x452CFD91L},{0xC3015DB4L,0xC61C7217L,0x8980DF3AL,5L},{0x1F8E388CL,(-1L),0x901CC30FL,0xCE904CDEL},{0x499C9045L,0x58B99485L,0L,0xCE904CDEL},{1L,(-1L),1L,5L},{(-1L),0xC61C7217L,0xBCC83174L,0x452CFD91L}}};
    uint32_t *l_138 = (void*)0;
    uint32_t l_247 = 0x5735543DL;
    int8_t *l_279 = &g_92;
    uint32_t *l_282 = &l_247;
    int32_t l_283 = 9L;
    int32_t *l_303 = &g_96[6];
    uint64_t l_312[4] = {6UL,6UL,6UL,6UL};
    uint64_t l_321[7][5][1] = {{{18446744073709551615UL},{0x6D5FB6498450BF2CLL},{0x6D5FB6498450BF2CLL},{18446744073709551615UL},{0x6D5FB6498450BF2CLL}},{{0x6D5FB6498450BF2CLL},{18446744073709551615UL},{0x6D5FB6498450BF2CLL},{0x6D5FB6498450BF2CLL},{18446744073709551615UL}},{{0x6D5FB6498450BF2CLL},{0x6D5FB6498450BF2CLL},{18446744073709551615UL},{0x6D5FB6498450BF2CLL},{0x6D5FB6498450BF2CLL}},{{18446744073709551615UL},{0x6D5FB6498450BF2CLL},{0x6D5FB6498450BF2CLL},{18446744073709551615UL},{0x6D5FB6498450BF2CLL}},{{0x6D5FB6498450BF2CLL},{18446744073709551615UL},{18446744073709551615UL},{18446744073709551615UL},{18446744073709551613UL}},{{18446744073709551615UL},{18446744073709551615UL},{18446744073709551613UL},{18446744073709551615UL},{18446744073709551615UL}},{{18446744073709551613UL},{18446744073709551615UL},{18446744073709551615UL},{18446744073709551613UL},{18446744073709551615UL}}};
    float l_383 = 0x8.96ADE0p-79;
    uint16_t *l_388 = &g_59;
    uint16_t *l_391 = &g_121;
    int32_t **l_393 = &l_303;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_49[i] = 0x718BC698L;
    for (i = 0; i < 2; i++)
        l_90[i] = 1UL;
lbl_288:
    for (p_48 = 2; (p_48 >= 0); p_48 -= 1)
    { /* block id: 6 */
        int32_t *l_62 = &g_63[2][2];
        uint16_t l_132 = 65535UL;
        int8_t l_236 = 0xC7L;
        uint64_t l_249[1][7][4] = {{{0x17DEDC2EDBB1F5C7LL,0x478FC7E913C9C0DALL,0x478FC7E913C9C0DALL,0x17DEDC2EDBB1F5C7LL},{0x478FC7E913C9C0DALL,0x17DEDC2EDBB1F5C7LL,0x478FC7E913C9C0DALL,0x478FC7E913C9C0DALL},{0x17DEDC2EDBB1F5C7LL,0x17DEDC2EDBB1F5C7LL,0x810A8C1EE95B8BEFLL,0x17DEDC2EDBB1F5C7LL},{0x17DEDC2EDBB1F5C7LL,0x478FC7E913C9C0DALL,0x478FC7E913C9C0DALL,0x17DEDC2EDBB1F5C7LL},{0x478FC7E913C9C0DALL,0x17DEDC2EDBB1F5C7LL,0x478FC7E913C9C0DALL,0x478FC7E913C9C0DALL},{0x17DEDC2EDBB1F5C7LL,0x17DEDC2EDBB1F5C7LL,0x810A8C1EE95B8BEFLL,0x17DEDC2EDBB1F5C7LL},{0x17DEDC2EDBB1F5C7LL,0x478FC7E913C9C0DALL,0x478FC7E913C9C0DALL,0x17DEDC2EDBB1F5C7LL}}};
        const int32_t *l_256 = &g_96[0];
        int16_t *l_260 = (void*)0;
        int16_t *l_261 = &g_262[0];
        uint64_t *l_265 = (void*)0;
        uint64_t *l_266[1];
        int32_t *l_275 = &l_98[1][1][2];
        int32_t l_276[7][8] = {{0x7EF6DE33L,0L,0x7EF6DE33L,0x61A5A5C4L,0x514EF959L,0x4F2B1BE9L,0x0A34BDB7L,7L},{0x6E1C351EL,0xDEC4028CL,0x61A5A5C4L,0L,0x98BD247DL,0x514EF959L,0x514EF959L,0x98BD247DL},{0x6E1C351EL,1L,1L,0x6E1C351EL,0x514EF959L,0L,1L,0x4F2B1BE9L},{0x7EF6DE33L,0x8E6C1768L,0L,0x0A34BDB7L,1L,0L,0x4F2B1BE9L,0L},{0xF92BEB9DL,0x8E6C1768L,1L,0x8E6C1768L,0xF92BEB9DL,0L,0x6E1C351EL,0x0C033148L},{0L,1L,0xF92BEB9DL,7L,(-1L),0x514EF959L,0x8E6C1768L,0x8E6C1768L},{0x4F2B1BE9L,0xDEC4028CL,0xF92BEB9DL,0xF92BEB9DL,0xDEC4028CL,0x4F2B1BE9L,0x6E1C351EL,(-1L)}};
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_266[i] = &l_249[0][6][2];
        (*l_62) &= (safe_mul_func_uint8_t_u_u((l_49[p_48] && (safe_div_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((((!((safe_add_func_uint32_t_u_u(l_49[p_48], p_48)) < (((g_4 != ((0L & (l_49[p_48] != g_59)) > l_49[0])) <= ((safe_div_func_uint8_t_u_u(0xFEL, g_59)) , l_49[0])) > p_48))) , g_59) >= l_49[2]), g_59)), l_49[p_48]))), p_48));
        for (g_59 = 0; (g_59 <= 2); g_59 += 1)
        { /* block id: 10 */
            int32_t **l_64 = &l_62;
            int32_t *l_66 = &g_63[2][0];
            int32_t **l_65 = &l_66;
            int16_t l_89 = 0x7269L;
            uint16_t *l_199 = &g_59;
            uint8_t l_206 = 0x7FL;
            uint8_t l_240 = 248UL;
            int i, j;
            if (g_63[p_48][p_48])
                break;
            g_63[p_48][(g_59 + 1)] = (((*l_64) = &g_63[2][2]) != ((*l_65) = &g_63[0][0]));
            (*l_64) = &g_63[p_48][p_48];
            if (((safe_lshift_func_int16_t_s_u(0xCA44L, (g_69[4][0] != l_72))) && g_4))
            { /* block id: 16 */
                int32_t ***l_77 = &l_72;
                int8_t *l_91 = &g_92;
                int8_t *l_94 = &g_95;
                int32_t l_105 = (-1L);
                uint32_t *l_114 = (void*)0;
                uint32_t *l_115 = (void*)0;
                uint32_t *l_116[7][1][6] = {{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}},{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}},{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}},{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}},{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}},{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}},{{&g_117,&g_117,&g_117,&g_117,&g_117,&g_117}}};
                uint16_t *l_120 = &g_121;
                int i, j, k;
                (**l_64) ^= 0xFB6C7178L;
                l_98[2][3][1] ^= (g_63[2][1] = (safe_mod_func_uint32_t_u_u((*l_66), (((safe_sub_func_float_f_f((((((*l_77) = l_72) != &g_70[3]) != (safe_add_func_float_f_f((*l_66), g_4))) < ((0x3.36FDE0p-71 < (((g_96[5] ^= (safe_sub_func_int8_t_s_s(((*l_94) = ((*l_91) = (~((0x74ABL ^ ((safe_mod_func_uint64_t_u_u((safe_div_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(p_48, l_89)), l_49[1])), g_59)) ^ l_90[1])) <= p_48)))), p_48))) , (void*)0) != l_97)) >= g_93)), g_63[2][1])) != p_48) , 9UL))));
                (*g_122) = (safe_sub_func_int16_t_s_s(((safe_add_func_int8_t_s_s(g_95, (*l_62))) , ((g_96[5] |= (g_63[2][2] = (**l_64))) || (safe_mul_func_uint8_t_u_u(l_105, ((safe_sub_func_uint8_t_u_u((p_48 < (safe_lshift_func_uint8_t_u_s((((*l_94) = (((*l_66) ^= ((safe_mod_func_int16_t_s_s((1UL || 246UL), (safe_lshift_func_int16_t_s_s((((g_117--) , (((*l_120) ^= ((((void*)0 != l_62) , 253UL) >= p_48)) | 65535UL)) | p_48), 11)))) , p_48)) && 0x5086CD17L)) & p_48), g_71))), p_48)) , p_48))))), p_48));
                for (g_92 = 2; (g_92 >= 0); g_92 -= 1)
                { /* block id: 33 */
                    int64_t l_124 = 0x296547669497261CLL;
                    uint8_t *l_128 = &g_129[1];
                    int32_t l_134 = (-1L);
                    uint32_t *l_139 = (void*)0;
                    if ((p_48 || (l_124 | (safe_unary_minus_func_uint32_t_u((safe_mul_func_int8_t_s_s(p_48, (((*l_128)++) <= g_121))))))))
                    { /* block id: 35 */
                        float *l_133[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_133[i] = &g_93;
                        (*l_65) = (*l_65);
                        (*l_62) |= (l_132 = (*g_122));
                        if ((*g_122))
                            break;
                        l_134 = ((*l_62) < g_63[2][2]);
                    }
                    else
                    { /* block id: 41 */
                        int32_t *l_135 = &g_63[p_48][p_48];
                        l_135 = ((*l_65) = &g_96[5]);
                    }
                    (*l_64) = l_62;
                    (*l_62) = (safe_add_func_uint64_t_u_u(18446744073709551610UL, (l_138 == l_139)));
                }
            }
            else
            { /* block id: 48 */
                int32_t l_177 = 1L;
                int32_t l_182 = 0xC24AECF7L;
                uint32_t *l_188 = &g_172;
                int64_t *l_204 = &g_205[6];
                int32_t l_237 = 0L;
                (*l_62) &= (&g_95 != (void*)0);
                for (g_95 = 0; (g_95 <= 2); g_95 += 1)
                { /* block id: 52 */
                    uint32_t *l_170 = (void*)0;
                    uint32_t *l_171 = &g_172;
                    uint8_t *l_178 = &g_129[0];
                    uint8_t *l_179[2];
                    int32_t l_181 = 0x65633C59L;
                    int8_t *l_183 = &g_92;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_179[i] = &g_180;
                    if ((safe_mod_func_int32_t_s_s(((*l_62) , ((g_96[5] , ((safe_sub_func_int8_t_s_s(p_48, (((p_48 == ((*l_183) &= (safe_mul_func_uint8_t_u_u((safe_div_func_uint32_t_u_u((safe_add_func_int16_t_s_s(((safe_div_func_int8_t_s_s((((l_177 = (safe_sub_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(p_48, (safe_mod_func_uint32_t_u_u((((*l_178) = ((((!1UL) < (8L || (safe_mul_func_int16_t_s_s((safe_add_func_int16_t_s_s(((((safe_mul_func_uint16_t_u_u(g_117, ((((~(safe_add_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((((*l_171)--) || (safe_mul_func_uint8_t_u_u((((l_177 & p_48) >= 9UL) <= p_48), g_129[0]))) | l_177), 0x16734CF679B517F4LL)), (-1L)))) || 8L) , (void*)0) == (void*)0))) , (void*)0) == &g_129[2]) < p_48), 65532UL)), g_117)))) < l_177) > (*l_62))) <= 255UL), 0x2C410037L)))), 0x0D51L))) == l_181) , g_96[6]), g_123)) , g_117), p_48)), (*g_122))), l_182)))) > g_121) , p_48))) , l_72)) == l_97)), 0x7285B31AL)))
                    { /* block id: 57 */
                        uint16_t l_184 = 1UL;
                        return l_184;
                    }
                    else
                    { /* block id: 59 */
                        uint32_t l_185 = 0x59136648L;
                        l_185--;
                        if (p_48)
                            break;
                    }
                }
                (*g_122) |= (0xBF09L | 65535UL);
                if ((((++(*l_188)) , ((safe_rshift_func_int8_t_s_u(((safe_lshift_func_int8_t_s_s(p_48, (safe_mul_func_uint8_t_u_u((1L || 0xF5L), (g_59 == (safe_sub_func_int64_t_s_s(((&g_121 != l_199) < (**l_65)), ((-1L) < (safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(((((*l_204) |= ((((((g_121 , g_92) > 0L) <= g_129[1]) || p_48) <= p_48) , 3L)) , g_172) != p_48), 8)), (*l_66))))))))))) , 0x26L), 1)) , p_48)) < l_206))
                { /* block id: 67 */
                    const float *l_219 = &g_93;
                    uint16_t *l_221 = &g_121;
                    int32_t l_235 = 0x14B8F762L;
                    for (g_180 = 0; (g_180 <= 2); g_180 += 1)
                    { /* block id: 70 */
                        uint16_t l_207 = 0x965CL;
                        ++l_207;
                    }
                    (**l_64) |= (safe_rshift_func_int16_t_s_s((safe_add_func_int16_t_s_s((((p_48 , ((safe_lshift_func_int16_t_s_u((+((((*l_221) = (p_48 < ((-4L) <= ((safe_lshift_func_int16_t_s_s((g_92 ^ g_117), l_177)) || (p_48 <= ((g_220[0] ^= (l_219 != l_138)) ^ p_48)))))) , (void*)0) != &l_72)), p_48)) == p_48)) , g_123) , p_48), g_180)), 3));
                    l_237 |= ((((9UL < (((g_172 , (18446744073709551615UL || (safe_div_func_uint16_t_u_u((((safe_mod_func_int16_t_s_s((safe_sub_func_int16_t_s_s(0x28B1L, ((*l_221) &= (safe_lshift_func_uint16_t_u_s(((safe_sub_func_int16_t_s_s((~((safe_div_func_uint32_t_u_u(((((g_63[2][3] , (((**l_64) = (((&g_69[5][0] != (((p_48 , ((l_235 , 0x6156L) , g_129[1])) , g_59) , (void*)0)) == 0x587CEBA9940DB3F8LL) > l_182)) && g_96[5])) > p_48) & 255UL) , p_48), g_172)) < 1L)), 5L)) , (*l_62)), 14))))), l_177)) > 0x68L) & 6L), (-1L))))) < 0x49L) == 0x3868L)) ^ l_236) && p_48) , 0L);
                    if ((*g_122))
                    { /* block id: 79 */
                        float l_238 = 0xA.BC4B9Dp+37;
                        int32_t *l_239[9] = {&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)],&g_63[p_48][(g_59 + 1)]};
                        int i;
                        --l_240;
                        (*l_64) = &l_237;
                        (*l_65) = l_62;
                        if (p_48)
                            break;
                    }
                    else
                    { /* block id: 84 */
                        uint64_t l_248[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_248[i] = 0x53AD90D2FA85E071LL;
                        (*l_62) |= (safe_div_func_uint64_t_u_u((safe_mod_func_uint32_t_u_u(0UL, l_247)), l_248[0]));
                    }
                }
                else
                { /* block id: 87 */
                    --l_249[0][5][3];
                }
            }
        }
        if (p_48)
            break;
        l_276[3][1] |= ((*l_275) = (((g_129[1] < (safe_mul_func_int8_t_s_s((safe_unary_minus_func_int8_t_s((((0L | ((!(((&l_98[7][0][3] != l_256) ^ p_48) , (-1L))) && (safe_rshift_func_uint16_t_u_u((!((*l_261) = (-1L))), (safe_add_func_uint8_t_u_u(((g_267 = g_96[0]) && ((safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((g_274 = ((safe_add_func_int16_t_s_s((*l_256), g_63[0][0])) < g_117)) > g_220[0]), 1UL)), 0UL)) | g_59)), g_172)))))) | 6UL) | p_48))), (-10L)))) != g_92) < g_4));
    }
    if ((p_48 != (safe_mod_func_uint32_t_u_u(((*l_282) ^= (((*l_279) = (4294967295UL <= 0x293EDD8FL)) , ((safe_lshift_func_int8_t_s_u(p_48, 4)) || ((void*)0 != &l_98[2][3][1])))), l_283))))
    { /* block id: 101 */
        uint64_t *l_295 = (void*)0;
        uint64_t *l_296 = &g_297;
        int32_t *l_301[7][3] = {{(void*)0,(void*)0,&g_63[1][2]},{&g_267,&g_96[0],(void*)0},{&g_267,(void*)0,&g_267},{&g_267,&g_267,(void*)0},{&g_267,&g_267,&g_267},{(void*)0,&g_267,(void*)0},{&g_96[0],&g_267,&g_63[1][2]}};
        uint16_t l_302 = 0x0C1DL;
        int8_t *l_332 = &g_95;
        int i, j;
        for (g_123 = (-6); (g_123 < (-12)); g_123 = safe_sub_func_int16_t_s_s(g_123, 7))
        { /* block id: 104 */
            for (g_95 = 0; (g_95 >= 21); g_95++)
            { /* block id: 107 */
                float *l_289 = (void*)0;
                if (g_95)
                    goto lbl_288;
                (*g_290) = (g_220[0] , p_48);
            }
        }
        if (((((safe_lshift_func_uint8_t_u_s(g_262[0], 3)) == ((p_48 > (1UL != ((*l_296) = 18446744073709551612UL))) >= (safe_unary_minus_func_int16_t_s(((-1L) != (safe_rshift_func_uint8_t_u_s((l_301[2][0] != l_301[4][0]), 2))))))) , l_302) & (l_282 != l_282)))
        { /* block id: 113 */
            int64_t *l_310 = (void*)0;
            int64_t **l_309 = &l_310;
            int32_t *l_318[8] = {(void*)0,&g_63[2][2],&g_63[2][2],(void*)0,&g_63[2][2],&g_63[2][2],(void*)0,&g_63[2][2]};
            int i;
            for (g_172 = 0; (g_172 <= 3); g_172 += 1)
            { /* block id: 116 */
                uint16_t l_315 = 0x6068L;
                for (g_123 = 0; (g_123 <= 2); g_123 += 1)
                { /* block id: 119 */
                    for (g_59 = 0; (g_59 <= 3); g_59 += 1)
                    { /* block id: 122 */
                        int32_t **l_304 = &l_301[5][1];
                        (*l_304) = l_303;
                    }
                    if ((safe_sub_func_uint32_t_u_u((((*l_279) &= 0x84L) == (*l_303)), (g_129[1] < p_48))))
                    { /* block id: 126 */
                        int32_t *l_307 = (void*)0;
                        int64_t ***l_311 = &l_309;
                        int i, j;
                        g_122 = l_307;
                        g_63[g_123][g_123] &= p_48;
                        (*l_311) = l_309;
                    }
                    else
                    { /* block id: 130 */
                        int i;
                        ++l_312[3];
                    }
                    for (g_59 = 0; (g_59 <= 3); g_59 += 1)
                    { /* block id: 135 */
                        int i, j, k;
                        --l_315;
                        (*g_319) = l_318[0];
                    }
                }
                for (l_315 = 0; (l_315 <= 2); l_315 += 1)
                { /* block id: 142 */
                    return p_48;
                }
            }
            --l_321[3][2][0];
        }
        else
        { /* block id: 147 */
            uint64_t *l_333 = &l_321[6][4][0];
            int32_t l_334 = 0xBE3D4A74L;
            float *l_339[4] = {&g_340,&g_340,&g_340,&g_340};
            int32_t l_341 = 0xBCD2330FL;
            int i;
            g_340 = (safe_mul_func_float_f_f((l_334 = (safe_sub_func_float_f_f(((((((*g_290) = (((safe_sub_func_int32_t_s_s(((safe_lshift_func_int8_t_s_s(0L, 2)) , 0xDCA21D1CL), p_48)) , l_332) != &g_92)) != ((void*)0 == l_333)) == (l_334 != ((safe_add_func_float_f_f(((l_341 = (safe_div_func_float_f_f((p_48 , g_95), l_334))) == p_48), (*l_303))) == g_262[0]))) > g_267) != p_48), g_274))), 0x0.1p+1));
        }
        for (g_121 = 25; (g_121 == 20); --g_121)
        { /* block id: 155 */
            int32_t l_344 = 7L;
            int32_t l_345 = 0xF6053C3BL;
            int32_t l_346 = 0L;
            int32_t l_347 = 7L;
            uint8_t l_348 = 0UL;
            l_348++;
            for (g_123 = 0; (g_123 > 26); ++g_123)
            { /* block id: 159 */
                int64_t *l_358 = &g_220[0];
                int32_t l_359 = 1L;
                int32_t ****l_381 = &g_379;
                uint8_t *l_382 = &g_129[5];
                if ((p_48 || l_345))
                { /* block id: 160 */
                    int32_t **l_353 = &g_320;
                    (*l_353) = (void*)0;
                }
                else
                { /* block id: 162 */
                    int32_t l_354 = 0x187EB05FL;
                    return l_354;
                }
                l_359 |= (safe_add_func_int64_t_s_s((-3L), ((*l_358) &= ((l_346 || (3UL ^ p_48)) >= (+g_123)))));
                (*g_384) = ((safe_div_func_float_f_f(((safe_mod_func_uint32_t_u_u(p_48, (safe_sub_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s((((((((safe_lshift_func_uint16_t_u_s((p_48 , (g_180 && (safe_sub_func_int32_t_s_s((safe_lshift_func_uint16_t_u_s((p_48 == (((+((safe_mod_func_uint8_t_u_u(((*l_382) = ((((((*l_381) = g_379) == (((void*)0 == &l_312[3]) , (void*)0)) , &l_348) != (void*)0) >= g_96[5])), p_48)) , 0x1151AA0EL)) | 252UL) , (-8L))), 2)), (-1L))))), 11)) , g_123) <= 0x93L) && 0L) , g_172) | g_117) | 0x733499AAL), 9L)), 0xA354L)), g_63[2][2])))) , (*g_290)), (-0x4.Bp+1))) > g_63[2][2]);
            }
        }
    }
    else
    { /* block id: 172 */
        (*g_387) = g_385;
    }
    (*g_392) |= (((*l_391) = ((*l_388)--)) , (*l_303));
    (*l_393) = ((**g_379) = (*g_319));
    return g_262[0];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_59, "g_59", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_63[i][j], "g_63[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_71, "g_71", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc_bytes (&g_93, sizeof(g_93), "g_93", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_96[i], "g_96[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    transparent_crc(g_123, "g_123", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_129[i], "g_129[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_172, "g_172", print_hash_value);
    transparent_crc(g_180, "g_180", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_205[i], "g_205[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_220[i], "g_220[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_262[i], "g_262[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_267, "g_267", print_hash_value);
    transparent_crc(g_274, "g_274", print_hash_value);
    transparent_crc(g_297, "g_297", print_hash_value);
    transparent_crc_bytes (&g_340, sizeof(g_340), "g_340", print_hash_value);
    transparent_crc(g_409, "g_409", print_hash_value);
    transparent_crc(g_433, "g_433", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_445[i][j][k], "g_445[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_503, "g_503", print_hash_value);
    transparent_crc_bytes (&g_511, sizeof(g_511), "g_511", print_hash_value);
    transparent_crc(g_521, "g_521", print_hash_value);
    transparent_crc(g_532, "g_532", print_hash_value);
    transparent_crc(g_557, "g_557", print_hash_value);
    transparent_crc(g_560, "g_560", print_hash_value);
    transparent_crc(g_572, "g_572", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_610[i][j], "g_610[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_731, "g_731", print_hash_value);
    transparent_crc(g_771, "g_771", print_hash_value);
    transparent_crc(g_773, "g_773", print_hash_value);
    transparent_crc(g_961, "g_961", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1140[i], "g_1140[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1311, "g_1311", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_1420[i][j], "g_1420[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1939, "g_1939", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1991[i][j], "g_1991[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2018, "g_2018", print_hash_value);
    transparent_crc(g_2019, "g_2019", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_2028[i], "g_2028[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2085[i], "g_2085[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2434[i][j], "g_2434[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2499, "g_2499", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2684[i], "g_2684[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2735, "g_2735", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2754[i][j], "g_2754[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 684
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 47
breakdown:
   depth: 1, occurrence: 201
   depth: 2, occurrence: 49
   depth: 3, occurrence: 8
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 2
   depth: 7, occurrence: 2
   depth: 8, occurrence: 2
   depth: 9, occurrence: 2
   depth: 10, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 20, occurrence: 2
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 3
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 32, occurrence: 1
   depth: 33, occurrence: 4
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 39, occurrence: 3
   depth: 41, occurrence: 1
   depth: 46, occurrence: 1
   depth: 47, occurrence: 1

XXX total number of pointers: 508

XXX times a variable address is taken: 1295
XXX times a pointer is dereferenced on RHS: 275
breakdown:
   depth: 1, occurrence: 177
   depth: 2, occurrence: 51
   depth: 3, occurrence: 24
   depth: 4, occurrence: 13
   depth: 5, occurrence: 10
XXX times a pointer is dereferenced on LHS: 348
breakdown:
   depth: 1, occurrence: 274
   depth: 2, occurrence: 45
   depth: 3, occurrence: 19
   depth: 4, occurrence: 10
XXX times a pointer is compared with null: 49
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 21
XXX times a pointer is qualified to be dereferenced: 11026

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 977
   level: 2, occurrence: 336
   level: 3, occurrence: 521
   level: 4, occurrence: 214
   level: 5, occurrence: 63
XXX number of pointers point to pointers: 228
XXX number of pointers point to scalars: 280
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.1
XXX average alias set size: 1.51

XXX times a non-volatile is read: 2265
XXX times a non-volatile is write: 1121
XXX times a volatile is read: 140
XXX    times read thru a pointer: 36
XXX times a volatile is write: 59
XXX    times written thru a pointer: 10
XXX times a volatile is available for access: 2.09e+03
XXX percentage of non-volatile access: 94.4

XXX forward jumps: 0
XXX backward jumps: 8

XXX stmts: 202
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 26
   depth: 2, occurrence: 30
   depth: 3, occurrence: 39
   depth: 4, occurrence: 33
   depth: 5, occurrence: 43

XXX percentage a fresh-made variable is used: 13.4
XXX percentage an existing variable is used: 86.6
********************* end of statistics **********************/


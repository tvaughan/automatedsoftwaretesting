/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3653374044
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   unsigned f0 : 28;
};

struct S1 {
   uint32_t  f0;
};

#pragma pack(push)
#pragma pack(1)
struct S2 {
   int32_t  f0;
};
#pragma pack(pop)

union U3 {
   int8_t  f0;
   uint32_t  f1;
   volatile struct S1  f2;
   int16_t  f3;
   uint32_t  f4;
};

union U4 {
   volatile struct S2  f0;
   const volatile int64_t  f1;
};

union U5 {
   int8_t * volatile  f0;
};

/* --- GLOBAL VARIABLES --- */
static uint32_t g_4 = 5UL;
static volatile int8_t g_11 = (-2L);/* VOLATILE GLOBAL g_11 */
static volatile int8_t *g_10 = &g_11;
static int8_t g_13 = 1L;
static uint16_t g_56 = 65535UL;
static uint32_t g_84[10] = {0UL,8UL,0UL,0UL,8UL,0UL,0UL,8UL,0UL,0UL};
static struct S2 g_85[9] = {{6L},{6L},{6L},{6L},{6L},{6L},{6L},{6L},{6L}};
static float g_87[9] = {0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0,0x4.0293E9p-0};
static volatile union U4 g_89 = {{-6L}};/* VOLATILE GLOBAL g_89 */
static uint32_t g_93[10][1] = {{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL},{4294967286UL}};
static volatile union U5 g_111[2] = {{0},{0}};
static int64_t g_113 = 0x0FABB65F0DE344E3LL;
static int8_t g_116 = 0x7AL;
static int8_t g_119[4] = {0xFCL,0xFCL,0xFCL,0xFCL};
static uint16_t g_121 = 2UL;
static uint16_t *g_120 = &g_121;
static uint32_t *g_124 = &g_84[1];
static uint32_t **g_123 = &g_124;
static uint32_t *g_130 = &g_93[4][0];
static struct S0 g_136 = {14945};
static struct S0 g_139 = {14620};
static union U5 g_153 = {0};/* VOLATILE GLOBAL g_153 */
static int32_t g_159[4] = {0x202198C4L,0x202198C4L,0x202198C4L,0x202198C4L};
static union U4 g_180 = {{2L}};/* VOLATILE GLOBAL g_180 */
static int16_t g_182 = (-10L);
static struct S1 g_252[6] = {{0x0D868492L},{0x0D868492L},{0x0D868492L},{0x0D868492L},{0x0D868492L},{0x0D868492L}};
static struct S1 *g_254 = (void*)0;
static struct S1 ** volatile g_253 = &g_254;/* VOLATILE GLOBAL g_253 */
static int16_t g_257[7] = {0x59F0L,(-1L),0x59F0L,0x59F0L,(-1L),0x59F0L,0x59F0L};
static float * volatile g_261 = (void*)0;/* VOLATILE GLOBAL g_261 */
static float * volatile g_262 = (void*)0;/* VOLATILE GLOBAL g_262 */
static float * volatile g_263 = (void*)0;/* VOLATILE GLOBAL g_263 */
static int16_t g_278 = 0xF60EL;
static volatile int16_t g_281 = 0x8B0FL;/* VOLATILE GLOBAL g_281 */
static union U5 *g_290 = &g_153;
static union U5 ** volatile g_289 = &g_290;/* VOLATILE GLOBAL g_289 */
static volatile uint8_t * volatile g_296 = (void*)0;/* VOLATILE GLOBAL g_296 */
static volatile uint8_t * volatile *g_295 = &g_296;
static volatile uint8_t * volatile ** volatile g_297 = &g_295;/* VOLATILE GLOBAL g_297 */
static volatile union U4 g_305 = {{0x1BE30D72L}};/* VOLATILE GLOBAL g_305 */
static struct S0 *g_309 = (void*)0;
static struct S0 ** volatile g_308 = &g_309;/* VOLATILE GLOBAL g_308 */
static volatile uint16_t g_313 = 0x876AL;/* VOLATILE GLOBAL g_313 */
static float g_330[8] = {0x8.1p-1,0x8.1p-1,0x1.Cp+1,0x8.1p-1,0x8.1p-1,0x1.Cp+1,0x8.1p-1,0x8.1p-1};
static float * volatile g_329 = &g_330[7];/* VOLATILE GLOBAL g_329 */
static const union U5 ** const  volatile g_332 = (void*)0;/* VOLATILE GLOBAL g_332 */
static const union U5 *g_334 = &g_153;
static const union U5 ** volatile g_333 = &g_334;/* VOLATILE GLOBAL g_333 */
static struct S1 * volatile g_346[9][2][7] = {{{&g_252[5],&g_252[3],&g_252[3],&g_252[5],&g_252[3],(void*)0,&g_252[5]},{(void*)0,&g_252[3],&g_252[5],&g_252[2],&g_252[0],&g_252[3],&g_252[1]}},{{&g_252[5],(void*)0,(void*)0,&g_252[5],(void*)0,(void*)0,&g_252[5]},{&g_252[5],&g_252[5],(void*)0,&g_252[1],(void*)0,&g_252[0],&g_252[3]}},{{&g_252[5],&g_252[0],&g_252[2],(void*)0,&g_252[0],&g_252[0],(void*)0},{(void*)0,(void*)0,(void*)0,&g_252[5],&g_252[3],&g_252[5],&g_252[0]}},{{(void*)0,&g_252[1],(void*)0,&g_252[0],&g_252[3],(void*)0,&g_252[5]},{&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5]}},{{&g_252[5],(void*)0,&g_252[3],&g_252[5],&g_252[2],&g_252[0],&g_252[3]},{&g_252[5],&g_252[5],(void*)0,&g_252[0],&g_252[1],&g_252[0],(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_252[5],&g_252[5],(void*)0,(void*)0},{&g_252[5],&g_252[5],&g_252[5],(void*)0,(void*)0,&g_252[3],&g_252[3]}},{{&g_252[5],&g_252[1],&g_252[5],&g_252[1],&g_252[5],(void*)0,&g_252[5]},{&g_252[0],(void*)0,&g_252[5],&g_252[5],&g_252[1],&g_252[2],&g_252[5]}},{{&g_252[5],&g_252[0],&g_252[5],&g_252[2],&g_252[2],&g_252[5],&g_252[0]},{&g_252[0],&g_252[5],(void*)0,&g_252[5],&g_252[5],&g_252[5],(void*)0}},{{&g_252[5],(void*)0,(void*)0,&g_252[5],&g_252[3],&g_252[2],&g_252[3]},{&g_252[5],&g_252[3],&g_252[3],&g_252[5],&g_252[3],(void*)0,&g_252[5]}}};
static int32_t * volatile g_366 = &g_159[0];/* VOLATILE GLOBAL g_366 */
static volatile int32_t g_432[1] = {(-1L)};
static volatile uint8_t g_450[6] = {0x00L,0x00L,0x00L,0x00L,0x00L,0x00L};
static uint32_t ** volatile * volatile g_474 = &g_123;/* VOLATILE GLOBAL g_474 */
static uint32_t ** volatile * volatile *g_473 = &g_474;
static struct S1 **g_478 = &g_254;
static struct S1 ***g_477[6] = {&g_478,&g_478,&g_478,&g_478,&g_478,&g_478};
static uint32_t **g_484 = &g_130;
static uint8_t g_508 = 0UL;
static int32_t ** volatile g_542 = (void*)0;/* VOLATILE GLOBAL g_542 */
static struct S1 * volatile g_588 = &g_252[0];/* VOLATILE GLOBAL g_588 */
static union U3 g_628 = {0x60L};/* VOLATILE GLOBAL g_628 */
static union U5 g_644 = {0};/* VOLATILE GLOBAL g_644 */
static volatile union U5 g_666 = {0};/* VOLATILE GLOBAL g_666 */
static int32_t g_685 = 0xA695EB72L;
static uint64_t g_687[1][3] = {{0UL,0UL,0UL}};
static volatile uint8_t * volatile * volatile *g_699[2] = {&g_295,&g_295};
static volatile uint8_t * volatile * volatile * volatile *g_698 = &g_699[1];
static volatile uint8_t * volatile * volatile * volatile * const *g_697 = &g_698;
static uint16_t **** volatile g_707 = (void*)0;/* VOLATILE GLOBAL g_707 */
static uint16_t **g_710 = (void*)0;
static uint16_t ***g_709[2] = {&g_710,&g_710};
static uint16_t **** volatile g_708 = &g_709[0];/* VOLATILE GLOBAL g_708 */
static union U4 g_714 = {{0x0206C952L}};/* VOLATILE GLOBAL g_714 */
static union U4 *g_713 = &g_714;
static uint16_t *g_715[1][5][5] = {{{&g_121,(void*)0,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,(void*)0,&g_121},{&g_121,&g_121,&g_121,&g_121,&g_121},{&g_121,(void*)0,&g_121,(void*)0,&g_121}}};
static union U4 g_737 = {{-1L}};/* VOLATILE GLOBAL g_737 */
static union U5 **g_740 = &g_290;
static float * volatile g_743 = &g_330[2];/* VOLATILE GLOBAL g_743 */
static union U4 ** volatile g_746 = (void*)0;/* VOLATILE GLOBAL g_746 */
static volatile union U3 g_758[8] = {{-1L},{0x36L},{-1L},{-1L},{0x36L},{-1L},{-1L},{0x36L}};
static union U3 g_762 = {-9L};/* VOLATILE GLOBAL g_762 */
static const union U5 g_769 = {0};/* VOLATILE GLOBAL g_769 */
static union U5 g_772 = {0};/* VOLATILE GLOBAL g_772 */
static union U5 g_773 = {0};/* VOLATILE GLOBAL g_773 */
static union U5 g_774 = {0};/* VOLATILE GLOBAL g_774 */
static union U5 g_775 = {0};/* VOLATILE GLOBAL g_775 */
static union U5 g_776 = {0};/* VOLATILE GLOBAL g_776 */
static int8_t g_818 = 1L;
static union U5 ***g_867 = &g_740;
static union U5 ****g_866[2] = {&g_867,&g_867};
static uint32_t ***g_892 = &g_123;
static uint32_t ****g_891[4][5] = {{&g_892,(void*)0,&g_892,&g_892,(void*)0},{&g_892,&g_892,&g_892,&g_892,&g_892},{&g_892,(void*)0,&g_892,&g_892,(void*)0},{&g_892,&g_892,&g_892,&g_892,&g_892}};
static uint8_t **g_911 = (void*)0;
static uint8_t ***g_910 = &g_911;
static float g_917 = 0x0.Fp-1;
static uint16_t * const *g_978 = &g_715[0][4][0];
static uint16_t * const **g_977[6] = {&g_978,&g_978,&g_978,&g_978,&g_978,&g_978};
static int16_t *g_984 = &g_762.f3;
static float * volatile g_986 = &g_330[4];/* VOLATILE GLOBAL g_986 */
static int32_t *g_1014 = &g_159[0];
static const union U4 g_1019 = {{0x8A3FE23FL}};/* VOLATILE GLOBAL g_1019 */
static struct S0 * volatile g_1039 = &g_139;/* VOLATILE GLOBAL g_1039 */
static int64_t g_1040[1] = {0x5883D788B4A7DA80LL};
static union U3 g_1046 = {0L};/* VOLATILE GLOBAL g_1046 */
static union U3 g_1079 = {0x8AL};/* VOLATILE GLOBAL g_1079 */
static union U3 g_1080 = {0x97L};/* VOLATILE GLOBAL g_1080 */
static union U3 g_1081[3][9] = {{{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL}},{{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL}},{{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL},{0x3DL}}};
static union U3 g_1082 = {4L};/* VOLATILE GLOBAL g_1082 */
static union U3 g_1083 = {-1L};/* VOLATILE GLOBAL g_1083 */
static union U3 g_1084[1][3][3] = {{{{1L},{1L},{1L}},{{0x4CL},{-8L},{0x4CL}},{{1L},{1L},{1L}}}};
static union U3 g_1086 = {1L};/* VOLATILE GLOBAL g_1086 */
static union U5 **** volatile g_1119 = &g_867;/* VOLATILE GLOBAL g_1119 */
static union U4 **g_1139 = &g_713;
static union U4 *** volatile g_1138 = &g_1139;/* VOLATILE GLOBAL g_1138 */
static uint16_t **** volatile g_1143 = &g_709[0];/* VOLATILE GLOBAL g_1143 */
static int32_t * volatile g_1204 = &g_159[0];/* VOLATILE GLOBAL g_1204 */
static int32_t * volatile g_1218 = (void*)0;/* VOLATILE GLOBAL g_1218 */
static const int32_t *g_1224 = &g_159[2];
static const int32_t ** volatile g_1223[8] = {&g_1224,(void*)0,&g_1224,(void*)0,&g_1224,(void*)0,&g_1224,(void*)0};
static const int32_t ** volatile g_1225 = &g_1224;/* VOLATILE GLOBAL g_1225 */
static int32_t ** volatile g_1227 = &g_1014;/* VOLATILE GLOBAL g_1227 */
static int32_t * volatile g_1233 = &g_159[0];/* VOLATILE GLOBAL g_1233 */
static int32_t g_1250 = 2L;
static int32_t * volatile g_1249 = &g_1250;/* VOLATILE GLOBAL g_1249 */
static struct S2 * const g_1276 = &g_85[1];
static struct S2 * const * volatile g_1275[1][6][7] = {{{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276},{&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276,&g_1276}}};
static struct S2 * const * volatile * volatile g_1274[8][7][1] = {{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}},{{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]},{&g_1275[0][0][2]}}};
static volatile struct S2 g_1280[1] = {{0x4722445EL}};
static volatile struct S2 * volatile g_1279[3] = {&g_1280[0],&g_1280[0],&g_1280[0]};
static volatile struct S2 * volatile *g_1278[7] = {&g_1279[1],&g_1279[1],&g_1279[1],&g_1279[1],&g_1279[1],&g_1279[1],&g_1279[1]};
static volatile struct S2 * volatile **g_1277 = &g_1278[2];
static uint32_t g_1332[8][2] = {{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L},{0xE5251104L,0xE5251104L}};
static struct S0 * volatile g_1336[3] = {&g_139,&g_139,&g_139};
static int32_t ** volatile g_1378[2][7][4] = {{{&g_1014,(void*)0,&g_1014,(void*)0},{&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,(void*)0},{&g_1014,(void*)0,&g_1014,&g_1014},{&g_1014,(void*)0,(void*)0,&g_1014},{&g_1014,(void*)0,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014}},{{&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,(void*)0,&g_1014,&g_1014},{(void*)0,(void*)0,&g_1014,&g_1014},{&g_1014,(void*)0,&g_1014,(void*)0},{&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,(void*)0}}};
static volatile union U5 g_1399 = {0};/* VOLATILE GLOBAL g_1399 */
static volatile union U3 g_1479 = {0xDDL};/* VOLATILE GLOBAL g_1479 */
static union U3 g_1480 = {0L};/* VOLATILE GLOBAL g_1480 */
static volatile struct S0 * volatile * volatile *g_1513 = (void*)0;
static volatile struct S0 * volatile * volatile ** const  volatile g_1512[3][4] = {{&g_1513,&g_1513,&g_1513,&g_1513},{&g_1513,&g_1513,&g_1513,&g_1513},{&g_1513,&g_1513,&g_1513,&g_1513}};
static volatile struct S0 * volatile * volatile ** volatile * volatile g_1514 = (void*)0;/* VOLATILE GLOBAL g_1514 */
static volatile struct S0 * volatile * volatile ** volatile g_1516 = &g_1513;/* VOLATILE GLOBAL g_1516 */
static volatile struct S0 * volatile * volatile ** volatile * volatile g_1515[5][6][2] = {{{(void*)0,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516},{(void*)0,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516}},{{(void*)0,(void*)0},{&g_1516,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516}},{{&g_1516,(void*)0},{(void*)0,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516},{(void*)0,&g_1516},{&g_1516,&g_1516}},{{&g_1516,&g_1516},{(void*)0,(void*)0},{&g_1516,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516}},{{&g_1516,&g_1516},{&g_1516,(void*)0},{(void*)0,&g_1516},{&g_1516,&g_1516},{&g_1516,&g_1516},{(void*)0,&g_1516}}};
static volatile struct S0 * volatile * volatile ** volatile * volatile g_1517 = &g_1516;/* VOLATILE GLOBAL g_1517 */
static struct S0 * volatile g_1518 = &g_139;/* VOLATILE GLOBAL g_1518 */
static union U3 *g_1521 = &g_1480;
static union U3 ** volatile g_1520 = &g_1521;/* VOLATILE GLOBAL g_1520 */
static volatile union U5 g_1545 = {0};/* VOLATILE GLOBAL g_1545 */
static uint32_t g_1585 = 0UL;
static struct S0 * volatile g_1611 = &g_139;/* VOLATILE GLOBAL g_1611 */
static volatile int64_t g_1624 = 0x890146E2CE5D832ELL;/* VOLATILE GLOBAL g_1624 */
static int16_t g_1644 = 5L;
static int64_t g_1649 = 0x32D91EFABE91FD72LL;
static volatile union U4 g_1653[1][5] = {{{{0xB2818D16L}},{{0xB2818D16L}},{{0xB2818D16L}},{{0xB2818D16L}},{{0xB2818D16L}}}};
static struct S0 * const  volatile g_1670 = &g_136;/* VOLATILE GLOBAL g_1670 */
static union U5 g_1685 = {0};/* VOLATILE GLOBAL g_1685 */
static uint8_t g_1704[6][10] = {{0x6AL,0UL,0xA2L,0x6AL,0x14L,0x6AL,0xA2L,0UL,0x6AL,0x76L},{0xEDL,0UL,0UL,0xEDL,0x14L,0x65L,0UL,0UL,0x65L,0x14L},{0xEDL,0xA2L,0xA2L,0xEDL,0x76L,0x6AL,0UL,0xA2L,0x6AL,0x14L},{0x6AL,0UL,0xA2L,0x6AL,0x14L,0x6AL,0x65L,0x6AL,255UL,0x88L},{0x6EL,0x6AL,0xEDL,0x6EL,0x67L,0x73L,0x6AL,0x6AL,0x73L,0x67L},{0x6EL,0x65L,0x65L,0x6EL,0x88L,255UL,0x6AL,0x65L,255UL,0x67L}};
static volatile union U5 g_1707 = {0};/* VOLATILE GLOBAL g_1707 */
static union U5 * volatile *g_1724 = &g_290;
static union U5 * volatile **g_1723 = &g_1724;
static union U5 * volatile *** const  volatile g_1722 = &g_1723;/* VOLATILE GLOBAL g_1722 */
static int32_t ** volatile g_1770 = (void*)0;/* VOLATILE GLOBAL g_1770 */
static int32_t ** volatile g_1771 = &g_1014;/* VOLATILE GLOBAL g_1771 */
static int32_t ** volatile g_1780[4][5][9] = {{{&g_1014,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,(void*)0},{&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,(void*)0,&g_1014,&g_1014}},{{(void*)0,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,(void*)0},{&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,(void*)0,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014},{(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014}},{{&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014}},{{&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014},{(void*)0,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,(void*)0},{&g_1014,&g_1014,(void*)0,&g_1014,&g_1014,&g_1014,(void*)0,&g_1014,&g_1014},{&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014}}};
static volatile union U4 g_1785 = {{0x3F6CC3B5L}};/* VOLATILE GLOBAL g_1785 */
static union U3 ** const  volatile g_1805 = &g_1521;/* VOLATILE GLOBAL g_1805 */
static volatile union U5 g_1808 = {0};/* VOLATILE GLOBAL g_1808 */
static const union U3 g_1817 = {0x55L};/* VOLATILE GLOBAL g_1817 */
static int32_t g_1834 = 0xEDDE20E3L;
static union U4 g_1844[6] = {{{1L}},{{0xD371DB38L}},{{1L}},{{1L}},{{0xD371DB38L}},{{1L}}};
static volatile union U5 g_1896[7][8][1] = {{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}},{{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}},{{0}}}};
static struct S1 * volatile g_1898[10][6] = {{&g_252[2],&g_252[1],&g_252[2],&g_252[5],&g_252[5],&g_252[1]},{(void*)0,&g_252[0],&g_252[5],&g_252[2],&g_252[2],&g_252[5]},{&g_252[0],&g_252[0],&g_252[5],&g_252[2],&g_252[0],&g_252[5]},{(void*)0,&g_252[5],&g_252[1],&g_252[5],&g_252[1],&g_252[5]},{&g_252[2],(void*)0,&g_252[1],&g_252[3],&g_252[0],&g_252[5]},{&g_252[5],&g_252[3],&g_252[5],&g_252[5],&g_252[3],&g_252[5]},{&g_252[5],&g_252[3],&g_252[5],&g_252[5],&g_252[0],&g_252[1]},{&g_252[1],(void*)0,&g_252[2],(void*)0,&g_252[1],&g_252[3]},{&g_252[1],&g_252[5],(void*)0,&g_252[5],&g_252[0],&g_252[0]},{&g_252[5],&g_252[0],&g_252[0],&g_252[5],&g_252[2],&g_252[0]}};
static int32_t ** volatile g_1901 = &g_1014;/* VOLATILE GLOBAL g_1901 */
static int8_t g_1949 = 0x79L;
static volatile union U4 g_1953 = {{0x1FF408FCL}};/* VOLATILE GLOBAL g_1953 */
static volatile uint16_t g_2006[10] = {0x37CDL,0x37CDL,0x37CDL,0x37CDL,0x37CDL,0x37CDL,0x37CDL,0x37CDL,0x37CDL,0x37CDL};


/* --- FORWARD DECLARATIONS --- */
static const int32_t  func_1(void);
static struct S2  func_16(int32_t  p_17, uint8_t  p_18, int8_t * const  p_19, int8_t * const  p_20);
static int64_t  func_21(int32_t  p_22, int8_t  p_23, int8_t * p_24, int8_t * p_25, uint16_t  p_26);
static uint32_t  func_27(int8_t * p_28, int8_t * p_29, const int8_t * p_30, int8_t * p_31);
static int8_t * func_32(int32_t  p_33, uint64_t  p_34, int8_t * p_35);
static uint8_t  func_43(int8_t * const  p_44, uint8_t  p_45, uint32_t  p_46);
static int32_t  func_47(int8_t * p_48, int32_t  p_49);
static const int8_t  func_63(int8_t * p_64, struct S0  p_65, uint16_t * p_66, struct S1  p_67, uint8_t  p_68);
static int8_t * func_69(uint16_t * p_70, int8_t * p_71);
static uint16_t * func_72(uint64_t  p_73);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_10 g_11 g_13 g_85 g_89 g_93 g_111 g_113 g_89.f0.f0 g_120 g_123 g_136 g_124 g_84 g_153 g_159 g_121 g_180 g_130 g_180.f0.f0 g_253 g_116 g_254 g_252 g_139 g_281 g_289 g_295 g_297 g_305 g_308 g_313 g_329 g_333 g_257 g_366 g_119 g_309 g_450 g_473 g_508 g_182 g_588 g_330 g_278 g_87 g_644 g_666 g_687 g_697 g_484 g_708 g_715 g_986 g_478 g_758 g_1019 g_818 g_1039 g_1040 g_1014 g_1204 g_1225 g_1227 g_1233 g_1249 g_1250 g_1084.f0 g_1332 g_1649 g_685 g_1653 g_1276 g_1611 g_1670 g_1046.f3 g_1685 g_867 g_740 g_698 g_699 g_1624 g_1139 g_713 g_714 g_1704 g_1707 g_1224 g_1644 g_1722 g_1771 g_1785 g_305.f0.f0 g_1805 g_1808 g_1817 g_1083.f0 g_1834 g_1516 g_1513 g_1844 g_1896 g_1901 g_1277 g_1278 g_1279 g_1723 g_1724 g_290 g_1518 g_2006
 * writes: g_56 g_84 g_87 g_93 g_113 g_116 g_119 g_123 g_130 g_136 g_139 g_159 g_182 g_254 g_278 g_290 g_124 g_295 g_309 g_313 g_330 g_334 g_450 g_120 g_477 g_484 g_508 g_252 g_121 g_628.f0 g_687 g_709 g_713 g_984 g_1014 g_1040 g_1224 g_1250 g_762.f2.f0 g_1649 g_628.f1 g_1046.f3 g_1083.f1 g_1480.f4 g_1086.f4 g_85 g_818 g_1480.f0 g_1704 g_1083.f4 g_917 g_1079.f4 g_1521 g_685 g_1082.f0 g_1079.f1 g_13 g_1080.f4
 */
static const int32_t  func_1(void)
{ /* block id: 0 */
    float l_7 = 0xE.F5D870p+71;
    int32_t l_8 = (-5L);
    int8_t *l_12 = &g_13;
    uint64_t l_1652[10] = {0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL,0xEFCF53BBF9BA973ELL};
    int32_t *l_1832 = &g_685;
    uint64_t l_1833 = 18446744073709551613UL;
    int32_t *l_1835 = (void*)0;
    int32_t l_1836 = (-7L);
    const struct S0 *l_1846 = &g_136;
    const struct S0 * const *l_1845[4][6] = {{&l_1846,&l_1846,&l_1846,&l_1846,&l_1846,&l_1846},{&l_1846,&l_1846,&l_1846,&l_1846,&l_1846,&l_1846},{&l_1846,&l_1846,&l_1846,&l_1846,&l_1846,&l_1846},{&l_1846,&l_1846,&l_1846,&l_1846,&l_1846,&l_1846}};
    int32_t l_1864 = 0x08FDCCC3L;
    int64_t l_1865 = 0xF4850BBAC65536DDLL;
    int32_t l_1866 = 0x022A7692L;
    int32_t l_1867 = 5L;
    int32_t l_1868 = 0x8BAFDAB8L;
    int32_t l_1869 = 0x8993DFDDL;
    int32_t l_1870 = 0xD4F8681CL;
    int32_t l_1871[8];
    struct S2 *l_1892[1];
    struct S1 *** const l_1893 = (void*)0;
    struct S1 l_1897[5] = {{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}};
    uint32_t l_1928[5];
    uint16_t l_1972 = 0UL;
    int32_t l_1979 = 7L;
    float l_1990 = 0x3.CFF803p-62;
    uint16_t l_2002 = 5UL;
    int32_t l_2007 = 4L;
    int i, j;
    for (i = 0; i < 8; i++)
        l_1871[i] = 0x147F0F19L;
    for (i = 0; i < 1; i++)
        l_1892[i] = &g_85[5];
    for (i = 0; i < 5; i++)
        l_1928[i] = 0xE18FCA17L;
lbl_1957:
    l_1836 ^= (safe_add_func_int16_t_s_s(g_4, ((safe_mul_func_int16_t_s_s(((((*l_1832) = ((l_8 || ((!(g_10 != l_12)) < (safe_add_func_uint16_t_u_u((((((func_16(g_4, (func_21((l_8 = ((g_628.f1 = func_27(func_32(l_8, g_11, l_12), &g_13, &g_13, l_12)) ^ l_8)), g_685, &g_13, &g_818, l_1652[3]) , l_1652[8]), &g_13, l_12) , l_12) != l_12) != l_1652[2]) < 255UL) >= l_1652[3]), 8L)))) , g_1083.f0)) , l_8) <= 0xB5L), l_1833)) > g_1834)));
    for (g_1082.f0 = 24; (g_1082.f0 <= (-14)); g_1082.f0 = safe_sub_func_uint64_t_u_u(g_1082.f0, 5))
    { /* block id: 849 */
        struct S0 * const *l_1842 = &g_309;
        struct S0 * const **l_1841[8] = {&l_1842,&l_1842,&l_1842,&l_1842,&l_1842,&l_1842,&l_1842,&l_1842};
        int32_t l_1843 = 0L;
        int32_t l_1860 = 0x821F3622L;
        int32_t l_1861 = 8L;
        int32_t l_1863[7][5][7] = {{{0x6DB34B1DL,0x38E4F421L,(-1L),0x707B50AAL,(-1L),(-7L),1L},{0x4DBB8DF5L,5L,9L,0x124BEAD8L,5L,9L,1L},{1L,5L,7L,2L,0x1588B75AL,0x45D43B88L,0x288F22FDL},{7L,0x38E4F421L,7L,0xAF7B1E05L,0L,0x2083D72EL,5L},{0x46DEC582L,0x78005BF6L,(-6L),0x253B3B4FL,0x707B50AAL,(-8L),(-8L)}},{{0x78005BF6L,5L,0x104E6AA8L,5L,0x78005BF6L,6L,0x2C387EA9L},{0x988E2B9DL,0xC6B67BA3L,9L,0x707B50AAL,(-6L),0x2083D72EL,0x1588B75AL},{0x38E4F421L,0x288F22FDL,(-1L),(-1L),(-9L),0x38E4F421L,0L},{0x988E2B9DL,0x707B50AAL,0x45D43B88L,0xAF7B1E05L,0xC1834AB2L,0x14998473L,1L},{0x78005BF6L,(-1L),0x38E4F421L,1L,7L,(-7L),0L}},{{0x46DEC582L,1L,(-1L),7L,0x78005BF6L,0x41CC8FD6L,0x246DE571L},{7L,(-9L),0x14998473L,1L,0x2C387EA9L,(-1L),(-9L)},{1L,0L,5L,(-2L),0L,0x38E4F421L,(-9L)},{0x4DBB8DF5L,0x2C387EA9L,(-6L),9L,(-9L),(-1L),0x246DE571L},{0x6DB34B1DL,0x78005BF6L,(-8L),4L,1L,0x5855127DL,0L}},{{0x58EA4A5FL,7L,0xCF95C0A6L,1L,(-1L),(-1L),1L},{9L,0xC1834AB2L,9L,0x38E4F421L,0x707B50AAL,0L,0L},{1L,(-9L),(-3L),4L,0x288F22FDL,0x45D43B88L,0x1588B75AL},{0xA185EA3DL,(-6L),0x246DE571L,0xAF7B1E05L,0xC6B67BA3L,0L,0x2C387EA9L},{(-8L),0x78005BF6L,0x124BEAD8L,5L,5L,(-1L),(-8L)}},{{0L,0x707B50AAL,0L,1L,0x78005BF6L,0x5855127DL,5L},{0xA185EA3DL,0L,9L,0x2C387EA9L,0x38E4F421L,(-1L),0x288F22FDL},{0x38E4F421L,0x1588B75AL,0x253B3B4FL,0xCF95C0A6L,5L,0x38E4F421L,1L},{0xAF7B1E05L,5L,0x9D3139B0L,0xAF7B1E05L,5L,(-1L),1L},{0x58EA4A5FL,(-1L),0x246DE571L,0L,0x38E4F421L,0x41CC8FD6L,0L}},{{0x41CC8FD6L,1L,(-1L),0x246DE571L,0x78005BF6L,(-7L),(-6L)},{0xB1F85600L,5L,0x14998473L,5L,5L,0x14998473L,5L},{0x2083D72EL,6L,0x68DB374EL,0L,0x8729B972L,0L,0x41CC8FD6L},{0x104E6AA8L,0x2083D72EL,0x517742FCL,0x37B6333CL,0x41CC8FD6L,7L,0x14998473L},{7L,0x38E4F421L,7L,0L,0x4DBB8DF5L,0x707B50AAL,0x246DE571L}},{{0x38E4F421L,0x14998473L,(-1L),9L,0x45D43B88L,0L,0L},{(-3L),(-7L),0x37B6333CL,(-1L),(-1L),7L,0x8729B972L},{0x2083D72EL,0x41CC8FD6L,5L,0xC6B67BA3L,0x46DEC582L,0xA185EA3DL,(-1L)},{(-1L),(-1L),0L,(-1L),0L,0x37B6333CL,0x2083D72EL},{0x45D43B88L,0x38E4F421L,0L,0x68DB374EL,0L,0x9D3139B0L,0L}}};
        float l_1927 = 0x7.4p-1;
        uint64_t l_1956 = 0xB74DACFD7BA39DABLL;
        int32_t *l_1958 = (void*)0;
        union U5 ***l_1980 = &g_740;
        int64_t l_1991 = 0x05989DDDAD65B7B0LL;
        const int16_t l_1995 = 1L;
        float l_2003[6];
        int32_t *l_2008[7] = {&l_1870,&l_1870,&l_1870,&l_1870,&l_1870,&l_1870,&l_1870};
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_2003[i] = 0x6.CFF492p+85;
        if ((safe_rshift_func_int8_t_s_s((l_1841[2] != (*g_1516)), 2)))
        { /* block id: 850 */
            int32_t *l_1847 = (void*)0;
            uint32_t l_1851 = 1UL;
            int32_t l_1862[10];
            int i;
            for (i = 0; i < 10; i++)
                l_1862[i] = 0xB1BA48CEL;
            g_1250 &= ((l_1843 , (*g_713)) , ((g_1844[4] , l_1845[0][3]) == (void*)0));
            for (l_1833 = 0; (l_1833 <= 2); l_1833 += 1)
            { /* block id: 854 */
                float l_1848 = (-0x1.3p-1);
                int32_t l_1850 = 6L;
                int32_t *l_1854 = &l_8;
                int32_t *l_1855 = &g_1250;
                int32_t *l_1856 = &l_1843;
                int32_t *l_1857 = &l_1850;
                int32_t *l_1858 = &g_1250;
                int32_t *l_1859[10] = {&g_159[0],&g_159[0],&g_159[0],&l_8,&l_8,&g_159[0],&g_159[0],&g_159[0],&l_8,&l_8};
                uint32_t l_1872[9][2][2] = {{{0x6489E3BEL,0UL},{0x65965A1CL,0UL}},{{0x6489E3BEL,0x65965A1CL},{4294967295UL,4294967295UL}},{{4294967295UL,0x65965A1CL},{0x6489E3BEL,0UL}},{{0x65965A1CL,0UL},{0x6489E3BEL,0x65965A1CL}},{{4294967295UL,4294967295UL},{4294967295UL,0x65965A1CL}},{{0x6489E3BEL,0UL},{0x65965A1CL,0UL}},{{0x6489E3BEL,0x65965A1CL},{4294967295UL,4294967295UL}},{{4294967295UL,0x65965A1CL},{0x6489E3BEL,0UL}},{{0x65965A1CL,0UL},{0x6489E3BEL,0x65965A1CL}}};
                int i, j, k;
                for (g_116 = 0; (g_116 >= 0); g_116 -= 1)
                { /* block id: 857 */
                    int32_t *l_1849[5][5] = {{&l_1836,&g_1250,(void*)0,(void*)0,&g_1250},{&g_159[2],(void*)0,&g_1250,&g_1250,&g_1250},{(void*)0,(void*)0,&g_1250,(void*)0,&g_1250},{&g_1250,&g_1250,(void*)0,&g_159[2],(void*)0},{(void*)0,(void*)0,&l_1836,&g_159[2],&g_159[2]}};
                    int i, j;
                    l_1851--;
                    return (**g_1225);
                }
                if (l_1843)
                    continue;
                l_1872[3][1][1]++;
                for (g_1079.f1 = 0; (g_1079.f1 <= 2); g_1079.f1 += 1)
                { /* block id: 865 */
                    uint32_t l_1875 = 4294967292UL;
                    int16_t l_1894[3][4][2] = {{{(-7L),(-7L)},{(-7L),(-7L)},{(-7L),(-7L)},{(-7L),(-7L)}},{{(-7L),(-7L)},{(-7L),(-7L)},{(-7L),(-7L)},{(-7L),(-7L)}},{{(-7L),(-7L)},{(-7L),(-7L)},{(-7L),(-7L)},{(-7L),(-7L)}}};
                    int16_t l_1895 = 0xD160L;
                    int i, j, k;
                    if ((l_1875 , (safe_mul_func_uint16_t_u_u(((*g_120) = ((g_508 = ((safe_rshift_func_int8_t_s_s((((safe_rshift_func_int8_t_s_u(l_1843, 0)) <= (l_1863[3][4][6] = (safe_lshift_func_int8_t_s_s(0x79L, (l_1894[1][3][1] = ((safe_mul_func_int8_t_s_s((l_1862[6] != (((safe_mul_func_uint8_t_u_u(l_1863[3][4][6], ((4294967291UL > ((safe_sub_func_int32_t_s_s(0x10052146L, ((safe_mul_func_uint8_t_u_u(((void*)0 != l_1892[0]), 0xA6L)) ^ 0x32F4FB14L))) ^ (**g_484))) < 0xCFL))) , (void*)0) != l_1893)), l_1860)) && 4294967290UL)))))) != 1UL), 7)) == l_1875)) < (-7L))), l_1895))))
                    { /* block id: 870 */
                        return (**g_1225);
                    }
                    else
                    { /* block id: 872 */
                        struct S1 *l_1899 = &l_1897[2];
                        const int32_t l_1900 = 0x79D8DE5FL;
                        (*l_1899) = (g_1896[0][0][0] , l_1897[0]);
                        return l_1900;
                    }
                }
            }
            if ((*g_366))
                continue;
        }
        else
        { /* block id: 879 */
            uint8_t ****l_1907 = &g_910;
            const int32_t l_1923 = 0x81AD4998L;
            const uint64_t l_1930 = 1UL;
            int16_t l_1938[1];
            uint32_t l_1955[9][1][4] = {{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}},{{0x0F8C1174L,0x0F8C1174L,0x0F8C1174L,0x0F8C1174L}}};
            int32_t l_1974 = 0xD00B0923L;
            union U5 ***l_1981 = (void*)0;
            int32_t l_1992 = 1L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1938[i] = (-1L);
            for (g_685 = 0; (g_685 <= 1); g_685 += 1)
            { /* block id: 882 */
                const int32_t *l_1902 = &l_1861;
                union U5 *l_1915 = &g_772;
                union U5 **l_1914 = &l_1915;
                union U5 **l_1916 = (void*)0;
                union U5 *l_1918[2];
                union U5 **l_1917 = &l_1918[1];
                uint8_t *l_1926 = &g_1704[0][5];
                struct S0 l_1929 = {8578};
                int32_t *l_1931 = &l_1864;
                float l_1975 = 0xC.FCE665p-93;
                uint64_t l_1976 = 0x7A5BB3292F5002ECLL;
                int i;
                for (i = 0; i < 2; i++)
                    l_1918[i] = (void*)0;
                (*g_1901) = &l_1863[3][3][2];
                l_1902 = l_1902;
                if ((**g_1225))
                    break;
                (*l_1931) ^= (safe_sub_func_int32_t_s_s((safe_mod_func_uint32_t_u_u(((((((void*)0 != (**g_1277)) , ((void*)0 != l_1907)) , ((safe_lshift_func_uint8_t_u_u((safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(((**g_1723) == ((*l_1917) = ((*l_1914) = (**g_867)))), ((*l_12) = ((l_1928[1] &= (~((+((safe_lshift_func_int8_t_s_s(l_1923, (safe_mul_func_uint8_t_u_u(((*l_1926) = (l_1863[3][4][6] >= (-1L))), 8UL)))) | 0x7915ED90L)) >= g_1083.f0))) != l_1863[3][4][3])))), g_84[4])), 0)) != 0x40BB7EA0L)) , l_1929) , l_1930), (**g_484))), 0x59828D3DL));
                for (g_1080.f4 = 0; (g_1080.f4 <= 1); g_1080.f4 += 1)
                { /* block id: 894 */
                    int16_t *l_1939[3][7] = {{&g_182,&g_182,&l_1938[0],&g_182,&g_182,&l_1938[0],&g_182},{&g_1644,&l_1938[0],&l_1938[0],&g_1644,&l_1938[0],&l_1938[0],&g_1644},{&g_1084[0][1][1].f3,&g_182,&g_1084[0][1][1].f3,&g_1084[0][1][1].f3,&g_182,&g_1084[0][1][1].f3,&g_1084[0][1][1].f3}};
                    int64_t l_1954 = 0xFA443CD631F745A4LL;
                    int32_t l_1973 = 5L;
                    int i, j;
                }
            }
            l_1992 &= ((((safe_sub_func_int32_t_s_s(((l_1955[2][0][1] ^ (0x9FA3L < l_1979)) || (l_1980 != l_1981)), (((safe_rshift_func_int8_t_s_u((!(safe_div_func_uint16_t_u_u(((((*g_1611) = (*g_1518)) , (!(safe_mod_func_int16_t_s_s(((((((g_305.f0.f0 | g_818) || 255UL) >= l_1930) , l_1923) <= (*g_1224)) < l_1955[5][0][1]), (-1L))))) < (*g_120)), l_1938[0]))), 4)) , l_1991) || 1L))) ^ g_1332[4][0]) <= (*g_1224)) , l_1974);
            return l_1938[0];
        }
        g_159[0] |= ((safe_mul_func_int8_t_s_s(l_1995, (safe_sub_func_int64_t_s_s(((+((safe_lshift_func_uint8_t_u_s((safe_unary_minus_func_uint16_t_u(((l_2002 >= (((*l_1846) , ((l_1861 && l_1861) ^ (l_1972 < ((((safe_mul_func_uint16_t_u_u(0x290EL, 65528UL)) , 8L) , l_1979) == g_2006[4])))) < 4UL)) ^ 0x02463BAFA2C2F5CDLL))), 6)) || (*g_130))) >= 1UL), l_2007)))) , 1L);
    }
    if (g_116)
        goto lbl_1957;
    return l_1928[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_1805 g_120 g_121 g_1808 g_1817 g_257 g_13 g_1224 g_159 g_1250
 * writes: g_1521 g_121 g_687 g_1250
 */
static struct S2  func_16(int32_t  p_17, uint8_t  p_18, int8_t * const  p_19, int8_t * const  p_20)
{ /* block id: 838 */
    union U3 *l_1804 = (void*)0;
    const struct S0 *l_1816 = &g_136;
    const struct S0 * const *l_1815 = &l_1816;
    const struct S0 * const **l_1814 = &l_1815;
    const struct S0 * const ***l_1813 = &l_1814;
    struct S0 **l_1820 = &g_309;
    struct S0 ***l_1819 = &l_1820;
    struct S0 ****l_1818 = &l_1819;
    int32_t l_1829 = 1L;
    uint64_t *l_1830 = &g_687[0][1];
    struct S2 l_1831 = {0x2E1B2446L};
    (*g_1805) = l_1804;
    g_1250 |= (((*l_1830) = (((*g_120)++) > (g_1808 , (((safe_mul_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((l_1813 != (l_1818 = ((p_17 , g_1817) , l_1818))), (safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_u(((g_257[6] <= 0x0.9p-1) , (((safe_div_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((*p_19), (((p_17 <= l_1829) & p_17) | (*p_19)))), 0x0E41L)) | p_18) >= (*g_1224))), p_18)), l_1829)))), 0xA98FL)) | l_1829) != (-4L))))) , l_1829);
    return l_1831;
}


/* ------------------------------------------ */
/* 
 * reads : g_1653 g_1250 g_1276 g_1611 g_139 g_1670 g_1046.f3 g_1685 g_867 g_740 g_697 g_698 g_699 g_13 g_366 g_159 g_484 g_130 g_93 g_1624 g_1139 g_713 g_180 g_714 g_818 g_1480.f0 g_1704 g_1707 g_1224 g_10 g_11 g_1644 g_1083.f4 g_1722 g_120 g_121 g_1771 g_1079.f4 g_1785 g_1249 g_305.f0.f0 g_508 g_1083.f1 g_1480.f4 g_1086.f4 g_182
 * writes: g_508 g_1046.f3 g_1083.f1 g_1480.f4 g_628.f0 g_1014 g_1086.f4 g_1250 g_85 g_136 g_182 g_290 g_818 g_87 g_1480.f0 g_1704 g_1083.f4 g_917 g_330 g_93 g_159 g_121 g_1079.f4
 */
static int64_t  func_21(int32_t  p_22, int8_t  p_23, int8_t * p_24, int8_t * p_25, uint16_t  p_26)
{ /* block id: 724 */
    int32_t *l_1654 = &g_159[1];
    int32_t l_1680 = 0x9AF8A309L;
    struct S2 l_1692 = {-1L};
    int32_t l_1746 = 0x29F27699L;
    int32_t l_1748[3];
    int i;
    for (i = 0; i < 3; i++)
        l_1748[i] = 0L;
lbl_1675:
    l_1654 = (g_1653[0][2] , (void*)0);
    for (g_508 = 0; (g_508 == 11); ++g_508)
    { /* block id: 728 */
        int32_t * const l_1665 = &g_1250;
        struct S2 l_1669 = {0xB4800054L};
        int32_t l_1679 = 0xD48AFEF4L;
        struct S2 *l_1712[3][4][2] = {{{(void*)0,&g_85[3]},{(void*)0,&g_85[3]},{(void*)0,&g_85[3]},{(void*)0,&g_85[3]}},{{(void*)0,&g_85[3]},{(void*)0,&g_85[3]},{(void*)0,&g_85[3]},{(void*)0,&g_85[3]}},{{(void*)0,&g_85[3]},{(void*)0,&g_85[3]},{(void*)0,&g_85[3]},{(void*)0,&g_85[3]}}};
        struct S2 **l_1711 = &l_1712[0][2][1];
        struct S2 ***l_1710 = &l_1711;
        uint32_t ** const *l_1738 = &g_123;
        uint32_t ** const **l_1737 = &l_1738;
        uint32_t ** const ***l_1736 = &l_1737;
        int32_t l_1745[6][5][2] = {{{0xF0E34AEAL,0x558172E8L},{0x45C6786AL,0xF0E34AEAL},{1L,0x532B97A7L},{1L,0xF0E34AEAL},{0x45C6786AL,0x558172E8L}},{{0xF0E34AEAL,0x424B0DA2L},{0x9A158D72L,1L},{0xB88263E1L,0x558172E8L},{0x558172E8L,0xB88263E1L},{1L,0x9A158D72L}},{{0x424B0DA2L,0xF0E34AEAL},{0x558172E8L,0x45C6786AL},{0xF0E34AEAL,1L},{0x532B97A7L,1L},{0xF0E34AEAL,0x45C6786AL}},{{0x558172E8L,0xF0E34AEAL},{0x424B0DA2L,0x9A158D72L},{1L,0xB88263E1L},{0x558172E8L,0x558172E8L},{0xB88263E1L,1L}},{{0x9A158D72L,0x424B0DA2L},{0xF0E34AEAL,0x558172E8L},{0x45C6786AL,0xF0E34AEAL},{1L,0x532B97A7L},{1L,0xF0E34AEAL}},{{0x45C6786AL,0x558172E8L},{0xF0E34AEAL,0x424B0DA2L},{0x9A158D72L,1L},{0xB88263E1L,0x558172E8L},{0x558172E8L,0xB88263E1L}}};
        int32_t *l_1760 = (void*)0;
        int16_t l_1788 = 0xA991L;
        int8_t l_1800 = 0x60L;
        int i, j, k;
        for (g_1046.f3 = 0; (g_1046.f3 <= (-18)); g_1046.f3 = safe_sub_func_int8_t_s_s(g_1046.f3, 6))
        { /* block id: 731 */
            uint64_t l_1699 = 0x1D3B332953B3F724LL;
            uint8_t *l_1703 = &g_1704[0][5];
            int32_t l_1749 = 1L;
            int32_t l_1754 = (-1L);
            int32_t l_1763 = 0xFE38DDBAL;
            int32_t l_1764[4][10] = {{3L,(-1L),2L,(-1L),(-1L),(-1L),2L,(-1L),3L,(-1L)},{(-1L),1L,1L,0x6E0A7EC3L,(-5L),3L,1L,(-1L),2L,2L},{1L,0L,(-5L),0x6E0A7EC3L,0x6E0A7EC3L,(-5L),0L,1L,3L,(-1L)},{0L,(-1L),0x1C80998CL,(-1L),1L,2L,0x7D50C023L,0x6E0A7EC3L,1L,0x6E0A7EC3L}};
            uint8_t l_1801 = 253UL;
            int i, j;
            for (g_1083.f1 = (-12); (g_1083.f1 != 24); g_1083.f1 = safe_add_func_int16_t_s_s(g_1083.f1, 6))
            { /* block id: 734 */
                struct S2 *l_1690 = &g_85[1];
                for (g_1480.f4 = 0; (g_1480.f4 <= 46); g_1480.f4++)
                { /* block id: 737 */
                    for (g_628.f0 = (-25); (g_628.f0 < 0); g_628.f0 = safe_add_func_uint32_t_u_u(g_628.f0, 4))
                    { /* block id: 740 */
                        int32_t **l_1666 = &g_1014;
                        (*l_1666) = l_1665;
                    }
                    for (g_1086.f4 = 0; (g_1086.f4 != 18); ++g_1086.f4)
                    { /* block id: 745 */
                        (*l_1665) &= 0xE9765468L;
                        return p_23;
                    }
                    (*g_1276) = l_1669;
                    (*g_1670) = (*g_1611);
                }
                for (g_182 = 28; (g_182 <= (-14)); g_182--)
                { /* block id: 754 */
                    float l_1681 = 0xB.CE6EC7p+87;
                    struct S2 l_1686 = {0L};
                    for (g_1480.f4 = 0; (g_1480.f4 == 56); g_1480.f4 = safe_add_func_int16_t_s_s(g_1480.f4, 1))
                    { /* block id: 757 */
                        int32_t *l_1676 = &g_159[1];
                        int32_t *l_1677 = &g_159[1];
                        int32_t *l_1678[1];
                        uint32_t l_1682 = 1UL;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1678[i] = &g_1250;
                        if (g_1046.f3)
                            goto lbl_1675;
                        --l_1682;
                        (**g_867) = (g_1685 , (l_1686 , (void*)0));
                    }
                }
                (*l_1665) ^= ((((**g_697) != (void*)0) && (0x22L >= (p_26 | (*p_24)))) <= p_26);
                if (p_22)
                { /* block id: 764 */
                    float *l_1696 = (void*)0;
                    float *l_1697 = (void*)0;
                    float *l_1698 = &g_87[4];
                    if (((safe_div_func_uint16_t_u_u((248UL || 0x8BL), (~(*g_366)))) > (**g_484)))
                    { /* block id: 765 */
                        struct S2 **l_1691 = &l_1690;
                        (*l_1691) = l_1690;
                        return g_1624;
                    }
                    else
                    { /* block id: 768 */
                        uint32_t l_1693 = 2UL;
                        (*l_1690) = ((**g_1139) , l_1692);
                        --l_1693;
                    }
                    for (g_818 = 8; (g_818 >= 1); g_818 -= 1)
                    { /* block id: 774 */
                        int i;
                        g_87[g_818] = 0x7.Ep+1;
                    }
                    (*l_1698) = 0x0.684289p-58;
                    for (g_1480.f0 = 0; (g_1480.f0 <= 1); g_1480.f0 += 1)
                    { /* block id: 780 */
                        uint16_t l_1700 = 65535UL;
                        if (l_1699)
                            break;
                        if (l_1700)
                            break;
                    }
                }
                else
                { /* block id: 784 */
                    return p_26;
                }
            }
            if ((((safe_lshift_func_uint8_t_u_s(((*l_1703) |= p_22), 3)) , (((p_23 ^ (safe_sub_func_uint8_t_u_u(((g_1707 , (!1UL)) == (!(254UL & ((0xA299L && (*l_1665)) != (p_23 >= ((void*)0 != l_1710)))))), 0x37L))) < (*g_1224)) , (*g_10))) & g_1644))
            { /* block id: 789 */
                uint32_t *****l_1739 = &g_891[2][0];
                int32_t l_1750 = 0x9BE0787FL;
                int32_t l_1751 = (-1L);
                int32_t l_1752 = 0x471DE31BL;
                int32_t l_1753 = 8L;
                int32_t l_1755 = 0xFCCD95B5L;
                uint8_t l_1765[5] = {1UL,1UL,1UL,1UL,1UL};
                int i;
                for (g_1083.f4 = (-21); (g_1083.f4 <= 54); g_1083.f4 = safe_add_func_uint64_t_u_u(g_1083.f4, 1))
                { /* block id: 792 */
                    int64_t *l_1731 = &g_1040[0];
                    int64_t **l_1730 = &l_1731;
                    int32_t l_1741 = 0x92B1654EL;
                    int32_t l_1742 = 0x0FBDF08FL;
                    int32_t l_1743 = 7L;
                    int32_t l_1744 = 0x1870A5CDL;
                    int32_t l_1747[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
                    int32_t *l_1761 = (void*)0;
                    int32_t *l_1762[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1762[i] = &l_1755;
                    if ((safe_unary_minus_func_uint8_t_u((*l_1665))))
                    { /* block id: 793 */
                        struct S2 **l_1725 = &l_1712[0][3][0];
                        float *l_1726 = &g_917;
                        float *l_1727 = &g_330[0];
                        int32_t *l_1740[5] = {&l_1680,&l_1680,&l_1680,&l_1680,&l_1680};
                        uint64_t l_1756[7][5] = {{0x4E48C5D44F013DCFLL,0x4E48C5D44F013DCFLL,0xECB21EE024201577LL,0x4E48C5D44F013DCFLL,0x4E48C5D44F013DCFLL},{0x41A6841E008CF09CLL,0xEFB99D8EC5BB06F1LL,0x41A6841E008CF09CLL,0x41A6841E008CF09CLL,0xEFB99D8EC5BB06F1LL},{0x4E48C5D44F013DCFLL,1UL,1UL,0x4E48C5D44F013DCFLL,1UL},{0xEFB99D8EC5BB06F1LL,0xEFB99D8EC5BB06F1LL,18446744073709551615UL,0xEFB99D8EC5BB06F1LL,0xEFB99D8EC5BB06F1LL},{1UL,0x4E48C5D44F013DCFLL,1UL,0xECB21EE024201577LL,1UL},{0x41A6841E008CF09CLL,18446744073709551615UL,18446744073709551615UL,0x41A6841E008CF09CLL,18446744073709551615UL},{1UL,1UL,0x4E48C5D44F013DCFLL,1UL,1UL}};
                        int32_t **l_1759 = &l_1654;
                        int i, j;
                        (*l_1665) |= ((safe_add_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_s((((safe_sub_func_float_f_f((((((void*)0 != g_1722) != ((*l_1727) = ((*l_1726) = (((*l_1710) = l_1725) == (void*)0)))) == (safe_sub_func_float_f_f((l_1730 != (void*)0), (safe_add_func_float_f_f(((void*)0 == l_1726), (safe_mul_func_float_f_f((0x7.C95512p+85 > 0x0.5p-1), p_22))))))) < p_23), 0xC.B8D0FAp-1)) , l_1736) == l_1739), 2)) | (*g_120)), g_121)) , p_23);
                        l_1756[0][0]++;
                        l_1760 = ((*l_1759) = &l_1752);
                    }
                    else
                    { /* block id: 801 */
                        return p_22;
                    }
                    --l_1765[0];
                    (*g_366) |= (safe_sub_func_int32_t_s_s(((((*l_1703) = p_22) ^ (l_1752 , (*l_1654))) > ((**g_484) &= 0x1B4105FFL)), l_1753));
                    (*g_1771) = &l_1763;
                }
                return l_1751;
            }
            else
            { /* block id: 811 */
                int8_t l_1776 = 0x16L;
                int32_t *l_1779 = &g_159[3];
                struct S0 **l_1786 = &g_309;
                int32_t l_1787 = 0x7FE28E9EL;
                int32_t l_1789 = (-1L);
                int32_t l_1791 = 0x7E429240L;
                int32_t l_1792[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_1792[i] = (-1L);
                if ((p_26 != ((safe_add_func_uint16_t_u_u(p_22, p_23)) ^ ((*g_120) = (p_24 != (void*)0)))))
                { /* block id: 813 */
                    return p_22;
                }
                else
                { /* block id: 815 */
                    int32_t *l_1782 = &l_1748[1];
                    int32_t *l_1796 = &l_1792[3];
                    int32_t *l_1797 = &l_1764[1][7];
                    int32_t *l_1798 = &l_1787;
                    int32_t *l_1799[6][2][1] = {{{&l_1754},{(void*)0}},{{&l_1754},{(void*)0}},{{&l_1754},{(void*)0}},{{&l_1754},{(void*)0}},{{&l_1754},{(void*)0}},{{&l_1754},{(void*)0}}};
                    int i, j, k;
                    for (g_1079.f4 = 0; (g_1079.f4 < 14); g_1079.f4 = safe_add_func_uint16_t_u_u(g_1079.f4, 3))
                    { /* block id: 818 */
                        return l_1776;
                    }
                    for (l_1680 = 0; (l_1680 > (-7)); --l_1680)
                    { /* block id: 823 */
                        int32_t **l_1781[9][5] = {{&l_1779,(void*)0,(void*)0,&l_1779,&g_1014},{(void*)0,&g_1014,(void*)0,&l_1654,(void*)0},{&l_1779,&l_1779,&l_1779,(void*)0,&g_1014},{&l_1779,&l_1654,&l_1654,&l_1654,&l_1779},{&g_1014,(void*)0,&l_1779,&l_1779,&l_1779},{(void*)0,&l_1654,(void*)0,&g_1014,(void*)0},{&g_1014,&l_1779,(void*)0,(void*)0,&l_1779},{&l_1779,&g_1014,&l_1654,&g_1014,&l_1779},{&l_1779,(void*)0,(void*)0,&l_1779,&g_1014}};
                        struct S0 **l_1784 = &g_309;
                        struct S0 ***l_1783 = &l_1784;
                        int64_t l_1790 = 0xB6D82B3D9D682D5BLL;
                        uint32_t l_1793 = 6UL;
                        int i, j;
                        l_1782 = l_1779;
                        (*l_1779) = (((*l_1783) = &g_309) != (g_1785 , (l_1786 = &g_309)));
                        if (p_23)
                            continue;
                        l_1793++;
                    }
                    --l_1801;
                }
                if ((*g_1249))
                    break;
            }
        }
    }
    return g_305.f0.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_13 g_4 g_11 g_85 g_89 g_93 g_111 g_113 g_89.f0.f0 g_120 g_123 g_10 g_136 g_124 g_84 g_153 g_159 g_121 g_180 g_130 g_180.f0.f0 g_253 g_116 g_254 g_252 g_139 g_281 g_289 g_295 g_297 g_305 g_308 g_313 g_329 g_333 g_257 g_366 g_119 g_309 g_450 g_473 g_508 g_182 g_588 g_330 g_278 g_87 g_644 g_666 g_687 g_697 g_484 g_708 g_715 g_986 g_478 g_758 g_1019 g_818 g_1039 g_1040 g_1014 g_1204 g_1225 g_1227 g_1233 g_1249 g_1250 g_1084.f0 g_1332 g_1649
 * writes: g_56 g_84 g_87 g_93 g_113 g_116 g_119 g_123 g_130 g_136 g_139 g_159 g_182 g_254 g_278 g_290 g_124 g_295 g_309 g_313 g_330 g_334 g_450 g_120 g_477 g_484 g_508 g_252 g_121 g_628.f0 g_687 g_709 g_713 g_984 g_1014 g_1040 g_1224 g_1250 g_762.f2.f0 g_1649
 */
static uint32_t  func_27(int8_t * p_28, int8_t * p_29, const int8_t * p_30, int8_t * p_31)
{ /* block id: 3 */
    const uint16_t l_38 = 65533UL;
    int8_t *l_50 = &g_13;
    int64_t *l_1648 = &g_1649;
    int32_t *l_1650 = &g_159[0];
    int32_t l_1651 = 0xC4ED0532L;
    l_1651 &= (g_13 >= (safe_add_func_int32_t_s_s(l_38, ((*l_1650) = (safe_sub_func_uint64_t_u_u(l_38, ((*l_1648) |= (((void*)0 == p_29) , (safe_rshift_func_int8_t_s_u(l_38, (func_43(func_32((((func_47(l_50, l_38) || (**g_484)) == 0xD4L) <= l_38), l_38, &g_818), l_38, g_1084[0][1][1].f0) >= g_1332[6][1])))))))))));
    return (*l_1650);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t * func_32(int32_t  p_33, uint64_t  p_34, int8_t * p_35)
{ /* block id: 1 */
    return &g_13;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_43(int8_t * const  p_44, uint8_t  p_45, uint32_t  p_46)
{ /* block id: 553 */
    uint16_t l_1251 = 65532UL;
    int32_t l_1265 = 0xCBC3F914L;
    uint8_t *l_1281 = &g_508;
    int32_t *l_1282 = (void*)0;
    int32_t *l_1283 = &g_159[2];
    int32_t *l_1284 = (void*)0;
    int32_t *l_1285 = &g_1250;
    union U4 *l_1286 = &g_180;
    union U5 **l_1287 = &g_290;
    float l_1390 = 0xF.CB811Ep-54;
    uint32_t *****l_1395 = &g_891[0][1];
    uint32_t *****l_1396 = (void*)0;
    int16_t l_1401 = 1L;
    float l_1413 = 0xA.8AD0F3p+56;
    uint32_t l_1475[1][6][2] = {{{0x9E069E3AL,0UL},{0x9E069E3AL,0UL},{0x9E069E3AL,0UL},{0x9E069E3AL,0UL},{0x9E069E3AL,0UL},{0x9E069E3AL,0UL}}};
    struct S0 l_1476 = {4649};
    int32_t l_1499 = 0L;
    int32_t l_1501 = 7L;
    int32_t l_1502 = 0xC0BDF25DL;
    int32_t l_1503 = (-4L);
    int32_t l_1504[9] = {(-1L),0L,(-1L),(-1L),0L,(-1L),(-1L),0L,(-1L)};
    struct S1 l_1524 = {4294967290UL};
    int32_t l_1556 = 0xEFE7E663L;
    uint32_t * const *l_1581 = &g_130;
    int16_t l_1632 = 0x4ADEL;
    uint32_t l_1633 = 0x3D6EEF2EL;
    int i, j, k;
    return p_45;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_11 g_13 g_85 g_89 g_93 g_111 g_113 g_89.f0.f0 g_120 g_123 g_10 g_136 g_124 g_84 g_153 g_159 g_121 g_180 g_130 g_180.f0.f0 g_253 g_116 g_254 g_252 g_139 g_281 g_289 g_295 g_297 g_305 g_308 g_313 g_329 g_333 g_257 g_366 g_119 g_309 g_450 g_473 g_508 g_182 g_588 g_330 g_278 g_87 g_644 g_666 g_687 g_697 g_484 g_708 g_715 g_986 g_478 g_758 g_1019 g_818 g_1039 g_1040 g_1014 g_1204 g_1225 g_1227 g_1233 g_1249 g_1250
 * writes: g_56 g_84 g_87 g_93 g_113 g_116 g_119 g_123 g_130 g_136 g_139 g_159 g_182 g_254 g_278 g_290 g_124 g_295 g_309 g_313 g_330 g_334 g_450 g_120 g_477 g_484 g_508 g_252 g_121 g_628.f0 g_687 g_709 g_713 g_984 g_1014 g_1040 g_1224 g_1250 g_762.f2.f0
 */
static int32_t  func_47(int8_t * p_48, int32_t  p_49)
{ /* block id: 4 */
    int32_t l_51[7];
    int8_t *l_88 = &g_13;
    struct S0 l_1201 = {4758};
    struct S1 l_1202 = {0xFF6FFCB0L};
    const int32_t *l_1221 = &g_159[0];
    union U5 **l_1230[9];
    struct S1 ****l_1239 = &g_477[1];
    struct S1 ****l_1240[9] = {&g_477[1],&g_477[1],&g_477[1],&g_477[1],&g_477[1],&g_477[1],&g_477[1],&g_477[1],&g_477[1]};
    struct S1 ***l_1241 = &g_478;
    uint32_t ** const l_1246 = &g_130;
    int i;
    for (i = 0; i < 7; i++)
        l_51[i] = (-1L);
    for (i = 0; i < 9; i++)
        l_1230[i] = &g_290;
    for (p_49 = 6; (p_49 >= 0); p_49 -= 1)
    { /* block id: 7 */
        uint16_t *l_55 = &g_56;
        int8_t *l_82 = &g_13;
        int8_t **l_81 = &l_82;
        uint32_t *l_83 = &g_84[1];
        float *l_86 = &g_87[5];
        int64_t *l_1220 = &g_1040[0];
        const int32_t *l_1222 = (void*)0;
        union U5 **l_1231 = &g_290;
        union U5 **l_1232 = &g_290;
        int i;
        if ((safe_unary_minus_func_int64_t_s(((*l_1220) = ((safe_mul_func_uint16_t_u_u(((*l_55) = l_51[p_49]), (safe_sub_func_int32_t_s_s(((((g_4 | (safe_mod_func_int16_t_s_s((safe_add_func_int8_t_s_s(func_63(func_69(func_72(((safe_sub_func_int32_t_s_s(((g_11 < 0xD2L) & (((((*l_86) = (((+(safe_div_func_float_f_f((g_13 != p_49), (((safe_add_func_float_f_f(((((((*l_83) = (((*l_81) = (void*)0) != (void*)0)) , (p_49 , g_85[1])) , l_51[5]) < g_4) < 0x7.9375E7p+99), 0x7.A53CC6p+82)) , 0xB.567DFBp-16) != p_49)))) > l_51[5]) > l_51[p_49])) , l_88) != (void*)0) != l_51[1])), l_51[1])) ^ 0x103ABED3L)), &g_818), l_1201, l_55, l_1202, p_49), 1UL)), l_51[p_49]))) != l_1202.f0) != p_49) , p_49), g_13)))) != l_51[p_49])))))
        { /* block id: 540 */
            (*g_1225) = (l_1222 = (((void*)0 != &g_159[0]) , l_1221));
        }
        else
        { /* block id: 543 */
            int32_t *l_1226 = &g_159[0];
            (*g_1227) = l_1226;
        }
        (*g_1233) = ((((l_1231 = l_1230[4]) != l_1232) && ((void*)0 == (*g_484))) & 253UL);
    }
    (*g_1249) ^= (((!7UL) >= (safe_add_func_uint8_t_u_u((((safe_rshift_func_int16_t_s_s(((l_1241 = &g_478) != (void*)0), 15)) > (safe_lshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_u((*l_1221), 4)), 9))) <= ((l_1246 != (void*)0) <= (3L > (*g_130)))), (safe_rshift_func_uint16_t_u_s(0xD9E9L, 3))))) & (*l_1221));
    for (g_762.f2.f0 = 0; g_762.f2.f0 < 2; g_762.f2.f0 += 1)
    {
        g_709[g_762.f2.f0] = &g_710;
    }
    return (*g_1249);
}


/* ------------------------------------------ */
/* 
 * reads : g_1204 g_159 g_84 g_687 g_818
 * writes: g_159 g_687
 */
static const int8_t  func_63(int8_t * p_64, struct S0  p_65, uint16_t * p_66, struct S1  p_67, uint8_t  p_68)
{ /* block id: 532 */
    struct S1 **l_1203 = &g_254;
    const struct S2 l_1205 = {0xDF6C6256L};
    struct S2 l_1206 = {-9L};
    union U3 *l_1208 = &g_1079;
    union U3 **l_1207 = &l_1208;
    struct S2 **l_1211 = (void*)0;
    uint64_t *l_1212 = (void*)0;
    uint64_t *l_1213 = &g_687[0][1];
    uint32_t **l_1217[10] = {(void*)0,&g_130,(void*)0,(void*)0,&g_130,(void*)0,(void*)0,&g_130,(void*)0,(void*)0};
    int32_t *l_1219 = &g_159[0];
    int i;
    (*g_1204) |= ((void*)0 != l_1203);
    l_1206 = (g_84[0] , l_1205);
    (*l_1219) |= ((&g_1081[2][8] == ((*l_1207) = (void*)0)) >= (safe_rshift_func_uint16_t_u_s((((*l_1213) |= ((void*)0 != l_1211)) <= (+l_1206.f0)), ((((safe_rshift_func_uint8_t_u_s(l_1205.f0, (&g_130 == l_1217[4]))) ^ (p_68 > 18446744073709551615UL)) | g_84[1]) | 0x3EDEL))));
    return (*p_64);
}


/* ------------------------------------------ */
/* 
 * reads : g_113 g_484 g_130 g_93 g_116 g_366 g_159 g_478 g_628.f0 g_758 g_1019 g_180.f0.f0 g_139.f0 g_818 g_1039 g_1040 g_1014
 * writes: g_113 g_93 g_159 g_254 g_1014 g_628.f0 g_139
 */
static int8_t * func_69(uint16_t * p_70, int8_t * p_71)
{ /* block id: 420 */
    const struct S0 l_987 = {15183};
    int16_t *l_995 = &g_257[4];
    uint16_t * const ***l_999 = &g_977[3];
    uint32_t l_1002 = 6UL;
    float **l_1006 = (void*)0;
    int32_t l_1009[4][3][10] = {{{0L,7L,(-1L),7L,0L,0L,0L,7L,(-1L),7L},{(-1L),0x20C2961FL,0xA7A41E68L,7L,0xA7A41E68L,0x20C2961FL,(-1L),0x20C2961FL,0xA7A41E68L,7L},{0L,7L,0L,0x20C2961FL,0L,0x20C2961FL,0L,7L,0L,0x20C2961FL}},{{(-1L),7L,0L,7L,(-1L),0L,(-1L),7L,0L,7L},{0L,0x20C2961FL,0L,7L,0L,0x20C2961FL,0L,0x20C2961FL,0L,7L},{0xA7A41E68L,7L,0xA7A41E68L,0x20C2961FL,(-1L),0x20C2961FL,0xA7A41E68L,7L,0xA7A41E68L,0x20C2961FL}},{{0L,7L,(-1L),7L,0L,0L,0L,7L,(-1L),7L},{(-1L),0x20C2961FL,0xA7A41E68L,7L,0xA7A41E68L,0x20C2961FL,(-1L),0x20C2961FL,0xA7A41E68L,7L},{0L,0x20C2961FL,(-1L),0L,0L,0L,(-1L),0x20C2961FL,(-1L),0L}},{{0xA7A41E68L,0x20C2961FL,(-1L),0x20C2961FL,0xA7A41E68L,7L,0xA7A41E68L,0x20C2961FL,(-1L),0x20C2961FL},{0L,0L,(-1L),0x20C2961FL,(-1L),0L,0L,0L,(-1L),0x20C2961FL},{0L,0x20C2961FL,0L,0L,0xA7A41E68L,0L,0L,0x20C2961FL,0L,0L}}};
    int32_t *l_1013 = &l_1009[0][2][1];
    int32_t **l_1012[1][3][3] = {{{&l_1013,&l_1013,&l_1013},{&l_1013,&l_1013,&l_1013},{&l_1013,&l_1013,&l_1013}}};
    union U5 *** const *l_1022 = &g_867;
    union U5 *** const **l_1021 = &l_1022;
    int32_t *l_1034 = &l_1009[1][1][1];
    union U3 *l_1085 = &g_1086;
    int8_t l_1090 = 0x2FL;
    struct S1 l_1093[6][4] = {{{0x49EDBF29L},{0x42B041BBL},{6UL},{0xDC443203L}},{{0x42B041BBL},{0x81EBC6F3L},{0xA15B121FL},{0x49EDBF29L}},{{4294967295UL},{0UL},{0UL},{4294967295UL}},{{4294967295UL},{0xDC443203L},{0xA15B121FL},{0UL}},{{0x42B041BBL},{4294967295UL},{6UL},{4294967295UL}},{{0x49EDBF29L},{4294967289UL},{0x49EDBF29L},{4294967295UL}}};
    struct S0 **l_1133 = &g_309;
    union U5 ****l_1180[9][4] = {{&g_867,&g_867,(void*)0,&g_867},{&g_867,&g_867,&g_867,&g_867},{&g_867,&g_867,&g_867,(void*)0},{&g_867,&g_867,(void*)0,&g_867},{&g_867,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_867,&g_867},{(void*)0,&g_867,&g_867,(void*)0},{&g_867,&g_867,&g_867,&g_867},{&g_867,&g_867,&g_867,&g_867}};
    int i, j, k;
    if ((0xF578L || (l_987 , l_987.f0)))
    { /* block id: 421 */
        int16_t *l_994 = &g_278;
        int64_t *l_996[9][2];
        int32_t l_1003[1];
        float *l_1008 = &g_330[2];
        float **l_1007 = &l_1008;
        int32_t *l_1010[5];
        struct S1 *l_1011[8][7] = {{&g_252[5],&g_252[5],(void*)0,&g_252[5],&g_252[5],(void*)0,&g_252[5]},{&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5]},{(void*)0,&g_252[5],(void*)0,(void*)0,&g_252[5],(void*)0,(void*)0},{&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5]},{&g_252[5],(void*)0,(void*)0,&g_252[5],(void*)0,(void*)0,&g_252[5]},{&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5]},{&g_252[5],&g_252[5],(void*)0,&g_252[5],&g_252[5],(void*)0,&g_252[5]},{&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5]}};
        int i, j;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 2; j++)
                l_996[i][j] = &g_113;
        }
        for (i = 0; i < 1; i++)
            l_1003[i] = 0xCE4BEB03L;
        for (i = 0; i < 5; i++)
            l_1010[i] = &g_159[3];
        (*g_366) |= ((safe_div_func_int64_t_s_s(((((((safe_rshift_func_uint8_t_u_u(((g_113 &= (l_994 == l_995)) , (safe_mod_func_uint8_t_u_u((l_999 != &g_977[2]), l_987.f0))), (safe_mod_func_uint8_t_u_u(l_1002, ((l_1003[0] != (l_1009[0][2][1] = (((**g_484) |= ((0xEE66L < (safe_sub_func_int64_t_s_s((l_1006 == l_1007), 0xCF88AF06822C0CDFLL))) <= l_1002)) | l_1003[0]))) ^ 1L))))) && g_93[9][0]) & l_1003[0]) , g_116) >= 0UL) , 0x279978E42F67FBB1LL), l_1003[0])) && l_1003[0]);
        (*g_478) = l_1011[2][1];
    }
    else
    { /* block id: 427 */
        return p_71;
    }
    g_1014 = &l_1009[0][2][1];
    for (g_628.f0 = 0; (g_628.f0 == (-5)); g_628.f0--)
    { /* block id: 433 */
        union U5 *****l_1023 = (void*)0;
        int32_t l_1032 = 0x5F2D163FL;
        int32_t *l_1033[8] = {&g_159[2],&g_159[2],&g_159[2],&g_159[2],&g_159[2],&g_159[2],&g_159[2],&g_159[2]};
        int32_t l_1036[8][5] = {{0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL},{0xF16B5B09L,(-8L),0xF16B5B09L,(-8L),0xF16B5B09L},{0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL},{0xF16B5B09L,(-8L),0xF16B5B09L,(-8L),0xF16B5B09L},{0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL},{0xF16B5B09L,(-8L),0xF16B5B09L,(-8L),0xF16B5B09L},{0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL,0x637910DFL},{0xF16B5B09L,(-8L),0xF16B5B09L,(-8L),0xF16B5B09L}};
        int i, j;
        if ((g_758[7] , ((((*l_1013) ^ ((safe_div_func_uint32_t_u_u((g_1019 , (((safe_unary_minus_func_int32_t_s(((l_1021 = l_1021) != (g_180.f0.f0 , l_1023)))) > ((0x7E47L >= (safe_div_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(((**g_484) ^= l_1032), (g_139.f0 , (-2L)))), 0x34L)), (*p_71))), 2UL))) , 0xE2FFL)) & l_1032)), l_1032)) , l_1032)) && l_1032) && l_1032)))
        { /* block id: 436 */
            int32_t *l_1035 = (void*)0;
            l_1033[4] = &g_159[0];
            l_1035 = l_1034;
        }
        else
        { /* block id: 439 */
            uint32_t l_1037 = 4294967289UL;
            struct S0 l_1038 = {8000};
            l_1037 &= l_1036[5][0];
            (*g_1039) = l_1038;
            (*g_1014) = g_1040[0];
            (*g_1014) ^= (*g_366);
        }
        for (l_1032 = 0; (l_1032 <= (-17)); l_1032 = safe_sub_func_uint32_t_u_u(l_1032, 7))
        { /* block id: 447 */
            int32_t *l_1043 = &l_1032;
            l_1033[4] = l_1043;
        }
    }
    for (g_628.f0 = 0; (g_628.f0 >= 6); g_628.f0++)
    { /* block id: 453 */
        float l_1058 = 0x0.3E933Bp+30;
        int32_t l_1059 = 0x31D874CBL;
        uint8_t *l_1060[6];
        int32_t l_1061 = 0xFF76E108L;
        int32_t l_1062 = 0xB1422DDCL;
        union U3 *l_1078[2][3][4] = {{{&g_1079,(void*)0,(void*)0,&g_1079},{(void*)0,&g_1079,&g_1081[2][7],&g_1084[0][1][1]},{(void*)0,&g_1081[2][7],(void*)0,&g_1083}},{{&g_1079,&g_1084[0][1][1],&g_1083,&g_1083},{&g_1081[2][7],&g_1081[2][7],&g_1082,&g_1084[0][1][1]},{&g_1084[0][1][1],&g_1079,&g_1082,&g_1079}}};
        int8_t l_1087 = 0x99L;
        uint16_t ** const *l_1089 = &g_710;
        uint16_t ** const **l_1088[8][1][7] = {{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}},{{&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089,&l_1089}}};
        int8_t l_1091 = 0xCDL;
        uint64_t *l_1092 = &g_687[0][1];
        int32_t *l_1120 = &l_1059;
        union U5 **l_1125 = &g_290;
        struct S0 **l_1134 = &g_309;
        int32_t l_1157 = (-2L);
        union U5 *****l_1164[4];
        uint8_t * const *l_1174 = &l_1060[3];
        uint8_t * const **l_1173 = &l_1174;
        uint32_t l_1177 = 0xE310C072L;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_1060[i] = &g_508;
        for (i = 0; i < 4; i++)
            l_1164[i] = (void*)0;
    }
    return &g_818;
}


/* ------------------------------------------ */
/* 
 * reads : g_89 g_85.f0 g_93 g_111 g_113 g_11 g_89.f0.f0 g_120 g_123 g_10 g_136 g_124 g_84 g_4 g_153 g_13 g_159 g_121 g_180 g_130 g_180.f0.f0 g_253 g_116 g_254 g_252 g_139 g_281 g_289 g_295 g_297 g_305 g_308 g_313 g_329 g_333 g_85 g_257 g_366 g_119 g_309 g_450 g_473 g_508 g_182 g_588 g_330 g_278 g_87 g_644 g_628.f0 g_666 g_687 g_697 g_484 g_708 g_715 g_986
 * writes: g_93 g_113 g_116 g_119 g_123 g_130 g_136 g_139 g_159 g_182 g_87 g_254 g_278 g_290 g_124 g_295 g_309 g_313 g_330 g_334 g_450 g_120 g_477 g_484 g_508 g_252 g_121 g_628.f0 g_687 g_709 g_713 g_984
 */
static uint16_t * func_72(uint64_t  p_73)
{ /* block id: 12 */
    uint32_t *l_92 = &g_93[9][0];
    int32_t l_96 = (-10L);
    uint32_t *l_102[7][4] = {{&g_84[2],&g_84[0],&g_84[9],&g_84[7]},{(void*)0,&g_84[0],&g_84[0],(void*)0},{&g_84[0],(void*)0,&g_84[2],&g_84[1]},{&g_84[0],&g_84[2],&g_84[0],&g_84[9]},{(void*)0,&g_84[1],&g_84[9],&g_84[9]},{&g_84[2],&g_84[2],&g_84[7],&g_84[1]},{&g_84[1],(void*)0,&g_84[7],(void*)0}};
    uint32_t **l_101 = &l_102[0][2];
    uint32_t *l_104[7] = {&g_84[1],&g_84[1],&g_84[1],&g_84[1],&g_84[1],&g_84[1],&g_84[1]};
    uint32_t **l_103 = &l_104[4];
    int64_t *l_112 = &g_113;
    float l_114 = 0x7.Bp-1;
    int8_t *l_115 = &g_116;
    int8_t *l_117 = (void*)0;
    int8_t *l_118 = &g_119[1];
    uint32_t l_131 = 0xD9658938L;
    int64_t l_141 = (-1L);
    int32_t l_195[6] = {4L,4L,4L,4L,4L,4L};
    int16_t l_197 = (-7L);
    int16_t l_203[5][10] = {{1L,(-10L),0xCF16L,(-10L),1L,0xA130L,0x722CL,6L,4L,4L},{0x418EL,6L,6L,0xF12DL,0xF12DL,6L,6L,0x418EL,0xCF16L,4L},{(-1L),0xF12DL,6L,0x78C8L,1L,0x722CL,1L,0x78C8L,6L,0xF12DL},{6L,0xA130L,6L,1L,0xC850L,0x78C8L,0x418EL,0x418EL,0x78C8L,0xC850L},{4L,6L,6L,4L,(-10L),0x78C8L,(-1L),6L,0xC850L,(-10L)}};
    uint16_t l_238 = 0x3F5FL;
    uint32_t l_239 = 0UL;
    int8_t l_266 = (-2L);
    uint32_t l_291 = 4294967294UL;
    int32_t l_311[7][3][5] = {{{0xB87F05BAL,0x4DF620D2L,0x1719A44CL,0x8ADAF65EL,0xE1FBBDABL},{0x4DF620D2L,8L,(-3L),6L,(-1L)},{(-1L),0xB0F44A99L,0xB6190532L,9L,1L}},{{0L,8L,8L,0L,0x51A2D859L},{1L,0xB87F05BAL,8L,0xE1FBBDABL,0xF05E6B33L},{0xE1FBBDABL,(-1L),0xB6190532L,(-1L),8L}},{{0x277F5422L,1L,(-3L),(-3L),1L},{0x1310D614L,(-4L),0x1719A44CL,(-1L),4L},{(-1L),1L,(-1L),9L,8L}},{{(-4L),9L,0x8ADAF65EL,0xF05E6B33L,6L},{(-1L),0x1719A44CL,(-5L),1L,0x4CA97DAEL},{0x1310D614L,0x877B575EL,0x7845E461L,0x068EC750L,1L}},{{0x277F5422L,9L,0xF05E6B33L,0xB87F05BAL,0x1310D614L},{0xE1FBBDABL,0x1DA2793BL,0x04C87680L,1L,(-3L)},{1L,0x068EC750L,0xD12CE1CFL,1L,0x4DF620D2L}},{{0L,1L,0x8ADAF65EL,0xB87F05BAL,(-6L)},{(-1L),(-5L),0xB87F05BAL,0x068EC750L,0xB87F05BAL},{0x4DF620D2L,0x4DF620D2L,(-6L),1L,0xE1FBBDABL}},{{0xB87F05BAL,8L,(-3L),0xF05E6B33L,0x1310D614L},{(-1L),0x277F5422L,0x344A1A61L,9L,0x1DA2793BL},{(-1L),8L,(-7L),(-1L),0x51A2D859L}}};
    uint16_t **l_347 = &g_120;
    uint64_t l_380[9][1] = {{18446744073709551615UL},{6UL},{6UL},{18446744073709551615UL},{6UL},{6UL},{18446744073709551615UL},{6UL},{6UL}};
    int32_t l_433 = (-1L);
    struct S0 *l_520 = &g_139;
    uint64_t l_551[4] = {18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL};
    uint8_t l_659 = 0x8DL;
    struct S2 *l_733 = &g_85[1];
    uint32_t ****l_803 = (void*)0;
    uint32_t *****l_802 = &l_803;
    struct S1 l_853 = {4UL};
    struct S1 ****l_857 = &g_477[4];
    uint32_t l_946[8][2] = {{0x8382F230L,0x8382F230L},{4294967295UL,0x8382F230L},{0x8382F230L,4294967295UL},{0x8382F230L,0x8382F230L},{4294967295UL,0x8382F230L},{0x8382F230L,4294967295UL},{0x8382F230L,0x8382F230L},{4294967295UL,0x8382F230L}};
    uint16_t * const **l_981 = &g_978;
    int16_t *l_983 = &g_257[6];
    int16_t **l_982[4][3] = {{&l_983,&l_983,&l_983},{(void*)0,(void*)0,(void*)0},{&l_983,&l_983,&l_983},{(void*)0,(void*)0,(void*)0}};
    int16_t *l_985 = &l_203[4][5];
    int i, j, k;
    if (((g_89 , (safe_add_func_uint32_t_u_u(g_85[1].f0, (((*l_92)++) , ((l_96 < ((safe_rshift_func_int16_t_s_s(((safe_add_func_int8_t_s_s((((*l_101) = l_92) != ((*l_103) = l_92)), ((((*l_118) = ((*l_115) = ((((safe_mul_func_int8_t_s_s((((safe_mod_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s((l_96 <= 0x7CA3B757L), 5)) || ((g_111[1] , (((*l_112) ^= 0x104DD42852A3B4A0LL) ^ g_11)) >= 0x93L)), (-1L))) | l_96) || g_89.f0.f0), l_96)) >= (-1L)) & (-10L)) || g_11))) < p_73) | 0x9D8E95A6L))) , 0x6D91L), l_96)) > g_93[4][0])) , 0L))))) ^ 2L))
    { /* block id: 19 */
        return g_120;
    }
    else
    { /* block id: 21 */
        struct S2 l_122[8][7] = {{{0xD7AD1849L},{6L},{1L},{1L},{0x145A4F90L},{1L},{1L}},{{2L},{2L},{0x04077E1FL},{-3L},{-8L},{-5L},{1L}},{{0x03AF45EFL},{8L},{-3L},{2L},{1L},{0xAC77275AL},{0xAC77275AL}},{{-8L},{0x145A4F90L},{0xD7AD1849L},{0x145A4F90L},{-8L},{2L},{0x03AF45EFL}},{{0xB513C549L},{0x04077E1FL},{0xD7AD1849L},{0x5CE90A86L},{0x145A4F90L},{0L},{6L}},{{0x5CE90A86L},{1L},{-3L},{0L},{0L},{-3L},{1L}},{{0xB513C549L},{0x03AF45EFL},{1L},{0xB513C549L},{-1L},{8L},{0xD7AD1849L}},{{-5L},{0xAC77275AL},{0L},{0x5CE90A86L},{1L},{-8L},{1L}}};
        uint32_t ***l_125 = (void*)0;
        uint32_t ***l_126 = &g_123;
        uint8_t l_127[5];
        uint8_t *l_128 = &l_127[1];
        uint32_t **l_129[6];
        int32_t l_189 = 0xCCE8995DL;
        int32_t l_193[1][8][2] = {{{0xEDEE6F90L,0xEDEE6F90L},{0xBC0A3B53L,(-4L)},{0L,(-4L)},{0xBC0A3B53L,0xEDEE6F90L},{0xEDEE6F90L,0xBC0A3B53L},{(-4L),0L},{(-4L),0xBC0A3B53L},{0xEDEE6F90L,0xEDEE6F90L}}};
        uint16_t *l_250 = &l_238;
        struct S1 **l_277 = &g_254;
        union U3 *l_283 = (void*)0;
        union U5 *l_288 = &g_153;
        uint16_t *l_352 = &g_121;
        int32_t l_353 = 0x5BE354D0L;
        uint64_t l_354 = 1UL;
        int8_t l_378 = 0xBDL;
        int16_t l_379[9] = {0x55F7L,0x0EBDL,0x55F7L,0x0EBDL,0x55F7L,0x0EBDL,0x55F7L,0x0EBDL,0x55F7L};
        struct S0 l_383[6][8] = {{{14442},{14442},{14442},{14442},{14442},{14442},{14442},{14442}},{{14442},{14442},{14442},{14442},{14442},{14442},{14442},{14442}},{{14442},{14442},{14442},{14442},{14442},{14442},{14442},{14442}},{{14442},{14442},{14442},{14442},{14442},{14442},{14442},{14442}},{{14442},{14442},{14442},{14442},{14442},{14442},{14442},{14442}},{{14442},{14442},{14442},{14442},{14442},{14442},{14442},{14442}}};
        uint8_t l_445 = 0x0DL;
        float *l_486 = (void*)0;
        uint16_t l_496[6] = {0x9BDDL,0xB2C4L,0xB2C4L,0x9BDDL,0xB2C4L,0xB2C4L};
        int8_t l_539 = (-8L);
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_127[i] = 1UL;
        for (i = 0; i < 6; i++)
            l_129[i] = &l_92;
        if (((l_122[3][6] , ((((*l_126) = g_123) != &g_124) && (((*l_128) = (l_96 || l_127[1])) > (((g_130 = &g_93[9][0]) != &g_93[9][0]) != 65529UL)))) , (l_131 != l_122[3][6].f0)))
        { /* block id: 25 */
            int32_t l_164 = 0xCAE24E5DL;
            int32_t l_187[6][2][2] = {{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}}};
            struct S1 l_265 = {1UL};
            uint8_t l_274 = 0xCFL;
            uint32_t ***l_279[10][10][2] = {{{&l_103,&l_101},{&g_123,&l_101},{&l_103,&l_101},{&g_123,&g_123},{&g_123,&l_103},{&g_123,&l_103},{&g_123,&g_123},{&g_123,&l_101},{&l_103,&l_101},{&g_123,&l_101}},{{&l_103,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&l_103,&l_101},{&g_123,&l_101},{&l_103,&l_101},{&g_123,&g_123},{&g_123,&l_103},{&g_123,&l_103},{&g_123,&g_123}},{{&g_123,&l_101},{&l_103,&l_101},{&g_123,&l_101},{&l_103,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&l_103,&l_101},{&g_123,&l_101},{&l_103,&l_101},{&g_123,&g_123}},{{&g_123,&l_103},{&g_123,&l_103},{&g_123,&g_123},{&g_123,&l_101},{&l_103,&l_101},{&g_123,&l_101},{&l_103,&g_123},{&g_123,&g_123},{&l_101,&g_123},{&g_123,&g_123}},{{&g_123,&l_103},{&g_123,&l_101},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&l_101},{&g_123,&l_103},{&g_123,&g_123},{&g_123,&g_123}},{{&l_101,&g_123},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&l_103},{&g_123,&l_101},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&l_101}},{{&g_123,&l_103},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&g_123},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&l_103},{&g_123,&l_101},{&l_101,&g_123},{&g_123,&g_123}},{{&g_123,&g_123},{&g_123,&g_123},{&l_101,&l_101},{&g_123,&l_103},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&g_123},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&l_103}},{{&g_123,&l_101},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&l_101},{&g_123,&l_103},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&g_123}},{{&l_101,&g_123},{&g_123,&g_123},{&g_123,&l_103},{&g_123,&l_101},{&l_101,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&g_123,&g_123},{&l_101,&l_101},{&g_123,&l_103}}};
            int64_t l_312 = (-1L);
            struct S0 **l_339 = &g_309;
            int16_t l_375 = (-1L);
            uint32_t **l_400 = &g_130;
            float *l_407 = &g_330[2];
            int8_t **l_471 = &l_117;
            int32_t *l_521 = (void*)0;
            int32_t l_522 = (-1L);
            int i, j, k;
            if (l_96)
            { /* block id: 26 */
                struct S0 *l_137 = &g_136;
                struct S0 *l_138 = &g_139;
                struct S0 l_140 = {7934};
                int64_t *l_152 = &g_113;
                int8_t *l_154[2][7] = {{&g_116,&g_116,&g_116,&g_116,&g_116,&g_116,&g_116},{&g_119[3],&g_116,&g_116,&g_119[3],&g_116,&g_119[3],&g_116}};
                int32_t l_155 = 0L;
                int32_t *l_156 = (void*)0;
                int32_t *l_157 = &l_96;
                int32_t *l_158 = &g_159[0];
                int8_t **l_162 = &l_117;
                int8_t **l_163 = &l_115;
                int32_t l_188 = 0xC6C1D744L;
                int32_t l_191 = 1L;
                int32_t l_192[5][4][10] = {{{0xDB3B990FL,(-8L),2L,1L,2L,(-8L),0xDB3B990FL,0xDB3B990FL,(-8L),2L},{(-8L),0xDB3B990FL,0xDB3B990FL,(-8L),2L,1L,2L,(-8L),0xDB3B990FL,0xDB3B990FL},{2L,0xDB3B990FL,0x49CD5C75L,(-3L),(-3L),0x49CD5C75L,0xDB3B990FL,2L,0xDB3B990FL,0x49CD5C75L},{1L,(-8L),(-3L),(-8L),1L,0x49CD5C75L,0x49CD5C75L,1L,(-8L),(-3L)}},{{2L,2L,(-3L),1L,1L,1L,(-3L),2L,2L,(-3L)},{(-8L),1L,(-3L),(-3L),0x49CD5C75L,0xDB3B990FL,2L,0xDB3B990FL,0x49CD5C75L,(-3L)},{1L,1L,1L,(-3L),2L,2L,(-3L),1L,1L,1L},{1L,0xDB3B990FL,1L,0x49CD5C75L,1L,0xDB3B990FL,1L,1L,0xDB3B990FL,1L}},{{0xDB3B990FL,1L,1L,0xDB3B990FL,1L,0x49CD5C75L,1L,0xDB3B990FL,1L,1L},{1L,1L,(-3L),2L,2L,(-3L),1L,1L,1L,(-3L)},{0x49CD5C75L,0xDB3B990FL,2L,0xDB3B990FL,0x49CD5C75L,(-3L),(-3L),0x49CD5C75L,0xDB3B990FL,2L},{1L,1L,2L,0x49CD5C75L,(-8L),0x49CD5C75L,2L,1L,1L,2L}},{{0xDB3B990FL,0x49CD5C75L,(-3L),(-3L),0x49CD5C75L,0xDB3B990FL,2L,0xDB3B990FL,0x49CD5C75L,(-3L)},{1L,1L,1L,(-3L),2L,2L,(-3L),1L,1L,1L},{1L,0xDB3B990FL,1L,0x49CD5C75L,1L,0xDB3B990FL,1L,1L,0xDB3B990FL,1L},{0xDB3B990FL,1L,1L,0xDB3B990FL,1L,0x49CD5C75L,1L,0xDB3B990FL,1L,1L}},{{1L,1L,(-3L),2L,2L,(-3L),1L,1L,1L,(-3L)},{0x49CD5C75L,0xDB3B990FL,2L,0xDB3B990FL,0x49CD5C75L,(-3L),(-3L),0x49CD5C75L,0xDB3B990FL,2L},{1L,1L,2L,0x49CD5C75L,(-8L),0x49CD5C75L,2L,1L,1L,2L},{0xDB3B990FL,0x49CD5C75L,(-3L),(-3L),0x49CD5C75L,0xDB3B990FL,2L,0xDB3B990FL,0x49CD5C75L,(-3L)}}};
                int64_t l_198 = (-4L);
                int16_t l_202[6] = {0x1D61L,0x1D61L,0x1D61L,0x1D61L,0x1D61L,0x1D61L};
                struct S1 *l_251[7] = {&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5],&g_252[5]};
                float *l_273 = (void*)0;
                int8_t l_299 = 0xC4L;
                int i, j, k;
                (*l_158) |= (p_73 && (safe_sub_func_int32_t_s_s(((((*g_10) , ((safe_sub_func_int16_t_s_s((((l_140 = ((*l_138) = ((*l_137) = g_136))) , l_141) && ((**g_123) , l_141)), (safe_lshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(((*l_157) = (safe_mod_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s((l_155 = ((((safe_rshift_func_uint8_t_u_s(g_4, 7)) , func_32(((l_152 = &g_113) == ((g_153 , g_85[1].f0) , &g_113)), g_93[9][0], l_154[0][2])) == (void*)0) && g_93[9][0])), 0UL)) | g_4), p_73))), g_93[9][0])), 3)))) != g_13)) | 0x0EL) > 0x3BFFL), p_73)));
                if ((9L <= (((safe_lshift_func_uint16_t_u_u((l_96 = (*g_120)), (&g_119[1] != l_154[1][4]))) >= (((*l_162) = l_154[0][5]) != ((*l_163) = (void*)0))) , l_164)))
                { /* block id: 37 */
                    int32_t l_172 = 0x5247B03FL;
                    int16_t *l_181 = &g_182;
                    int32_t l_194 = 0L;
                    int32_t l_196 = (-5L);
                    int32_t l_199 = (-3L);
                    int32_t l_200 = 0x4E8F14B0L;
                    int32_t l_201 = (-8L);
                    int32_t l_204 = 5L;
                    int32_t l_205 = 1L;
                    int32_t l_206[1];
                    uint32_t l_207[3];
                    int64_t *l_230 = &l_198;
                    struct S0 *l_243 = (void*)0;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_206[i] = (-10L);
                    for (i = 0; i < 3; i++)
                        l_207[i] = 1UL;
                    if (((((g_89 , (~((*l_181) = (safe_mod_func_int8_t_s_s(0L, (safe_mul_func_uint16_t_u_u(65528UL, ((0x2E0D36818C3B0D5BLL | (((safe_sub_func_int64_t_s_s(l_172, (((*l_112) |= (g_4 ^ p_73)) , (safe_mod_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((~p_73), (safe_lshift_func_int8_t_s_u(((**l_162) = (g_180 , (*g_10))), g_113)))), 0x89B7L))))) == l_172) <= 0xA8L)) == (*g_130))))))))) && l_96) > l_127[1]) , (-6L)))
                    { /* block id: 41 */
                        int32_t *l_183 = &l_96;
                        int32_t *l_184 = (void*)0;
                        int32_t *l_185 = (void*)0;
                        int32_t *l_186[1][3][1];
                        int32_t l_190 = 8L;
                        float *l_236 = (void*)0;
                        float *l_237 = &g_87[7];
                        uint16_t *l_240 = &g_121;
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 3; j++)
                            {
                                for (k = 0; k < 1; k++)
                                    l_186[i][j][k] = &g_159[0];
                            }
                        }
                        l_207[1]--;
                        g_159[0] &= ((safe_sub_func_uint64_t_u_u((safe_add_func_int64_t_s_s((((*g_10) & (safe_lshift_func_uint8_t_u_u((((0UL != (safe_mul_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(p_73, (safe_rshift_func_uint16_t_u_u(l_193[0][6][0], 8)))), 1L))) , (safe_mod_func_int16_t_s_s((0xD4L != (safe_div_func_uint16_t_u_u(((((safe_add_func_float_f_f((safe_div_func_float_f_f((&l_198 != l_230), ((((safe_add_func_float_f_f(((*l_237) = (!(safe_mul_func_float_f_f(p_73, l_187[4][0][1])))), 0xE.F4691Ap-47)) <= p_73) == g_93[7][0]) >= l_238))), g_93[9][0])) , g_84[5]) , (*g_130)) != p_73), 0x0A81L))), 0x1524L))) != 255UL), l_239))) >= l_203[4][7]), (-3L))), p_73)) > p_73);
                        return l_240;
                    }
                    else
                    { /* block id: 46 */
                        uint16_t *l_249 = &l_238;
                        (*l_158) = (((((*l_152) = (g_180.f0.f0 , l_194)) & ((safe_mod_func_uint8_t_u_u((l_243 != (void*)0), (*l_157))) | (safe_sub_func_int16_t_s_s(((safe_unary_minus_func_uint64_t_u(g_136.f0)) || (*g_120)), l_164)))) || (safe_mul_func_int16_t_s_s(0x89ACL, g_136.f0))) == p_73);
                        return &g_121;
                    }
                }
                else
                { /* block id: 51 */
                    uint32_t l_258 = 0x7CEB9D6AL;
                    uint32_t *** const l_282 = (void*)0;
                    (*g_253) = l_251[2];
                    if (g_116)
                    { /* block id: 53 */
                        int32_t *l_255 = &g_159[2];
                        int32_t *l_256[1][10][3];
                        float *l_264 = &g_87[6];
                        uint32_t ****l_280 = &l_279[2][8][1];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 10; j++)
                            {
                                for (k = 0; k < 3; k++)
                                    l_256[i][j][k] = (void*)0;
                            }
                        }
                        l_258++;
                        (*l_264) = g_85[1].f0;
                        l_265 = (**g_253);
                        (*l_264) = (((((((l_266 >= (safe_rshift_func_int8_t_s_u(((safe_div_func_uint16_t_u_u((safe_div_func_uint64_t_u_u(((g_139 , ((p_73 , (void*)0) == ((*l_280) = ((((-6L) <= (l_274 &= ((g_136 , (void*)0) == l_273))) | (safe_lshift_func_uint8_t_u_u((g_278 = (&g_254 != l_277)), 3))) , l_279[2][8][1])))) <= p_73), (*l_255))), g_281)) ^ 0x626FD71BL), p_73))) >= p_73) , l_282) != &g_123) != g_93[0][0]) > p_73) >= g_4);
                    }
                    else
                    { /* block id: 61 */
                        (*l_157) = (-9L);
                    }
                }
                l_283 = l_283;
                for (l_141 = 16; (l_141 == (-18)); l_141 = safe_sub_func_int32_t_s_s(l_141, 9))
                { /* block id: 68 */
                    uint32_t *l_292 = &g_84[5];
                    const int32_t l_298 = (-1L);
                    for (g_116 = (-6); (g_116 >= 8); g_116++)
                    { /* block id: 71 */
                        (*l_157) ^= 5L;
                        if (p_73)
                            break;
                        (*g_289) = (g_136 , l_288);
                        (*l_157) = (g_159[0] = (l_291 <= (((*g_123) = (*g_123)) != ((*g_254) , l_292))));
                    }
                    for (l_96 = 17; (l_96 < (-5)); l_96 = safe_sub_func_int64_t_s_s(l_96, 3))
                    { /* block id: 81 */
                        (*g_297) = g_295;
                        if (l_298)
                            continue;
                    }
                    (*l_157) = l_299;
                    (*l_158) ^= (safe_add_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((!(g_305 , l_96)), 14)), 1UL));
                }
            }
            else
            { /* block id: 88 */
                struct S0 *l_306 = (void*)0;
                struct S0 **l_307 = &l_306;
                int32_t *l_310[4][6] = {{&l_193[0][7][0],(void*)0,&l_193[0][7][0],(void*)0,&l_193[0][7][0],(void*)0},{&g_159[0],(void*)0,&g_159[0],(void*)0,&g_159[0],(void*)0},{&l_193[0][7][0],(void*)0,&l_193[0][7][0],(void*)0,&l_193[0][7][0],(void*)0},{&g_159[0],(void*)0,&g_159[0],(void*)0,&g_159[0],(void*)0}};
                const union U5 *l_331 = &g_153;
                struct S1 l_345 = {0x6295AF4FL};
                int i, j;
                (*g_308) = ((*l_307) = l_306);
                --g_313;
                for (l_266 = 0; (l_266 > (-17)); l_266 = safe_sub_func_uint16_t_u_u(l_266, 8))
                { /* block id: 94 */
                    float *l_326 = &g_87[5];
                    uint32_t **l_344 = (void*)0;
                    int32_t l_348[2][10][8] = {{{(-1L),0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL,0L,0x9589AFB4L},{0L,0x3E01A304L,0x9589AFB4L,0xF88D794CL,0x9589AFB4L,0x3E01A304L,0L,0L},{0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL,0x3E01A304L,(-1L),0x3E01A304L},{0xF88D794CL,0x3E01A304L,(-1L),0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL},{0x3E01A304L,0L,0L,0x3E01A304L,0x9589AFB4L,0xF88D794CL,0x9589AFB4L,0x3E01A304L},{0L,0x9589AFB4L,0L,5L,(-1L),(-1L),5L,0L},{0x9589AFB4L,0x9589AFB4L,(-1L),0xF88D794CL,1L,0xF88D794CL,(-1L),0x9589AFB4L},{0x9589AFB4L,0L,5L,(-1L),(-1L),5L,0L,0x9589AFB4L},{0L,0x3E01A304L,0x9589AFB4L,0xF88D794CL,0x9589AFB4L,0x3E01A304L,0L,0L},{0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL,0x3E01A304L,(-1L),0x3E01A304L}},{{0xF88D794CL,0x3E01A304L,(-1L),0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL},{0x3E01A304L,0L,0L,0x3E01A304L,0x9589AFB4L,0xF88D794CL,0x9589AFB4L,0x3E01A304L},{0L,0x9589AFB4L,0L,5L,(-1L),(-1L),5L,0L},{0x9589AFB4L,0x9589AFB4L,(-1L),0xF88D794CL,1L,0xF88D794CL,(-1L),0x9589AFB4L},{0x9589AFB4L,0L,5L,(-1L),(-1L),5L,0L,0x9589AFB4L},{0L,0x3E01A304L,0x9589AFB4L,0xF88D794CL,0x9589AFB4L,0x3E01A304L,0L,0L},{0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL,0x3E01A304L,(-1L),0x3E01A304L},{0xF88D794CL,0x3E01A304L,(-1L),0x3E01A304L,0xF88D794CL,5L,5L,0xF88D794CL},{0x3E01A304L,0L,0L,0x3E01A304L,0x9589AFB4L,0xF88D794CL,0x9589AFB4L,0x3E01A304L},{0L,0x9589AFB4L,0L,5L,(-1L),(-1L),5L,0L}}};
                    int i, j, k;
                    (*g_329) = (safe_div_func_float_f_f(((*l_326) = (safe_div_func_float_f_f(l_122[3][6].f0, (safe_sub_func_float_f_f((l_187[3][1][1] < 0x9.1p-1), (safe_add_func_float_f_f(0xD.8690E2p+7, 0x1.29CAFDp-3))))))), (safe_mul_func_float_f_f(0x7.4CB4B9p-60, (&g_313 != &g_313)))));
                    (*g_333) = l_331;
                    if (((safe_sub_func_int8_t_s_s((safe_rshift_func_int16_t_s_s(((*l_288) , (((void*)0 != l_339) > l_187[1][1][1])), (!((p_73 || (+((-2L) >= 6L))) || (p_73 != (safe_mod_func_int16_t_s_s(((g_85[5] , l_344) == (void*)0), l_311[5][1][0]))))))), g_257[6])) || p_73))
                    { /* block id: 98 */
                        l_265 = l_345;
                        if (l_189)
                            continue;
                        l_347 = &g_120;
                    }
                    else
                    { /* block id: 102 */
                        uint64_t l_349[10][9] = {{1UL,9UL,1UL,4UL,18446744073709551615UL,0x97394E5E61AE0644LL,0x6113AD017C25CDBELL,18446744073709551612UL,9UL},{0xA5D5DA8B97A130FDLL,1UL,18446744073709551612UL,1UL,1UL,0x82B63C7F41241078LL,0xC724EEBE0AD6E44ALL,0xC39AD116CF501CBCLL,0x3F4236D67A228A5FLL},{0x97394E5E61AE0644LL,18446744073709551614UL,0x6113AD017C25CDBELL,0x3F4236D67A228A5FLL,4UL,0x7A5CB412A043479ALL,0x7A5CB412A043479ALL,4UL,0x3F4236D67A228A5FLL},{3UL,1UL,3UL,18446744073709551611UL,0x6113AD017C25CDBELL,2UL,0x745B0FD36144FC5CLL,3UL,9UL},{0xECEEBD542BDA7D39LL,0x2672286EEC79FE2DLL,2UL,18446744073709551612UL,1UL,1UL,0x2672286EEC79FE2DLL,18446744073709551615UL,1UL},{0xA5D5DA8B97A130FDLL,8UL,0UL,2UL,18446744073709551611UL,0xA5D5DA8B97A130FDLL,18446744073709551611UL,0xC724EEBE0AD6E44ALL,8UL},{3UL,0x97394E5E61AE0644LL,4UL,0xC724EEBE0AD6E44ALL,18446744073709551615UL,8UL,18446744073709551615UL,0xC724EEBE0AD6E44ALL,4UL},{1UL,1UL,18446744073709551611UL,0x745B0FD36144FC5CLL,18446744073709551613UL,18446744073709551611UL,9UL,18446744073709551615UL,18446744073709551615UL},{1UL,0UL,0xECEEBD542BDA7D39LL,8UL,18446744073709551615UL,0xC39AD116CF501CBCLL,0x97394E5E61AE0644LL,1UL,3UL},{0x6C1C48D3F96D9D49LL,3UL,18446744073709551611UL,0xC39AD116CF501CBCLL,0x7A5CB412A043479ALL,1UL,2UL,8UL,0xC39AD116CF501CBCLL}};
                        int i, j;
                        l_349[1][8]--;
                        return l_352;
                    }
                }
                --l_354;
            }
            for (l_141 = 3; (l_141 >= 0); l_141 -= 1)
            { /* block id: 111 */
                uint64_t l_359 = 0x0CB7807FCD2D7AB4LL;
                int32_t l_373 = 3L;
                int32_t l_374[3][6] = {{0x93D4E918L,3L,0x93D4E918L,3L,0x93D4E918L,3L},{0x93D4E918L,3L,0x93D4E918L,3L,0x93D4E918L,3L},{0x93D4E918L,3L,0x93D4E918L,3L,0x93D4E918L,3L}};
                int8_t l_376 = 0xE5L;
                int16_t l_397[2][8][3] = {{{1L,1L,0L},{0x0E51L,(-1L),0L},{(-1L),0x0E51L,0L},{1L,1L,0L},{0x0E51L,(-1L),0L},{(-1L),0x0E51L,0L},{1L,1L,0L},{0x0E51L,(-1L),0L}},{{(-1L),0x0E51L,0L},{1L,1L,0L},{0x0E51L,(-1L),0L},{(-1L),0x0E51L,0L},{1L,1L,0L},{0x0E51L,(-1L),0L},{(-1L),0x0E51L,0L},{1L,1L,0L}}};
                float *l_426 = &l_114;
                uint32_t l_434 = 0x505959B2L;
                int16_t l_444 = 0xBE80L;
                uint64_t l_469 = 0x5387ECE5C807DF57LL;
                struct S1 ***l_475 = &l_277;
                uint32_t **l_482[8] = {&l_92,&l_92,&l_92,&l_92,&l_92,&l_92,&l_92,&l_92};
                float l_495[1];
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_495[i] = 0x0.5716B8p+4;
                if ((safe_sub_func_uint16_t_u_u(l_291, l_359)))
                { /* block id: 112 */
                    uint32_t l_367 = 0x8F09FC10L;
                    for (l_265.f0 = 0; (l_265.f0 <= 3); l_265.f0 += 1)
                    { /* block id: 115 */
                        struct S2 *l_371 = (void*)0;
                        struct S2 **l_370 = &l_371;
                        int i;
                        if (p_73)
                            break;
                        (*g_366) |= (((p_73 > 3UL) || p_73) || (safe_div_func_int8_t_s_s(p_73, (safe_rshift_func_uint8_t_u_u(1UL, (safe_mul_func_uint16_t_u_u(65531UL, 0x815BL)))))));
                        l_367++;
                        (*l_370) = &g_85[(l_141 + 3)];
                    }
                    return &g_121;
                }
                else
                { /* block id: 122 */
                    int32_t *l_372[4];
                    int32_t l_377 = 0xAC6206D4L;
                    struct S0 l_384 = {8016};
                    int i;
                    for (i = 0; i < 4; i++)
                        l_372[i] = &g_159[0];
                    --l_380[0][0];
                    l_384 = l_383[2][3];
                }
                for (l_131 = 0; (l_131 <= 8); l_131 += 1)
                { /* block id: 128 */
                    struct S1 l_385 = {0x60A51EEEL};
                    int32_t l_396 = 0L;
                    int32_t *l_398 = (void*)0;
                    int32_t *l_399 = &l_193[0][6][0];
                    float **l_408 = &l_407;
                    int i;
                    l_385 = l_265;
                    (*l_399) &= (((safe_mod_func_int64_t_s_s(((((safe_mod_func_int32_t_s_s(7L, (safe_lshift_func_int16_t_s_u(((l_396 = (g_119[l_141] && (g_113 &= (((void*)0 == &g_281) < (g_119[l_141] <= (safe_sub_func_uint8_t_u_u((g_119[l_141] < g_119[l_141]), ((safe_sub_func_int64_t_s_s(((4L & (*g_130)) < g_257[2]), l_374[0][0])) < (*g_130))))))))) , p_73), l_397[0][3][1])))) , l_265.f0) | g_159[0]) , g_136.f0), p_73)) > p_73) , (-3L));
                    l_311[5][1][0] &= (l_400 == ((g_119[l_141] = ((safe_mul_func_uint16_t_u_u(p_73, (safe_mul_func_uint16_t_u_u((l_380[4][0] >= (safe_add_func_uint32_t_u_u(((((*l_408) = l_407) != (void*)0) | 0x44DFFC98L), (safe_mod_func_int32_t_s_s((((*g_120) < (safe_sub_func_int8_t_s_s((safe_sub_func_int16_t_s_s((l_339 != (void*)0), (*g_120))), 248UL))) < p_73), l_122[3][6].f0))))), (*g_120))))) == p_73)) , &g_130));
                    for (g_182 = 8; (g_182 >= 0); g_182 -= 1)
                    { /* block id: 138 */
                        uint32_t *l_415 = &l_291;
                        (*l_399) ^= (l_415 != &g_93[4][0]);
                    }
                }
                if (((safe_div_func_int16_t_s_s(((*l_103) == ((*l_101) = ((**l_126) = (*l_103)))), ((*g_130) | (safe_rshift_func_int16_t_s_s(((safe_add_func_float_f_f(((safe_div_func_float_f_f(((safe_div_func_float_f_f((l_397[1][0][2] <= ((*g_10) , (l_193[0][0][0] = (((*l_426) = (((*l_407) = ((g_139 , (*g_308)) == (void*)0)) > (((l_189 ^= p_73) , g_84[1]) , p_73))) >= l_122[3][6].f0)))), 0x1.Bp-1)) < (-0x1.1p-1)), p_73)) != g_116), 0x0.2p-1)) , p_73), 3))))) >= (*g_130)))
                { /* block id: 148 */
                    return &g_121;
                }
                else
                { /* block id: 150 */
                    int32_t *l_427 = &l_373;
                    int32_t *l_428 = &l_311[5][1][0];
                    int32_t *l_429 = &l_193[0][4][0];
                    int32_t l_430 = 0xA343DF76L;
                    int32_t *l_431[9] = {&g_159[0],&l_193[0][6][0],&g_159[0],&g_159[0],&l_193[0][6][0],&g_159[0],&g_159[0],&l_193[0][6][0],&g_159[0]};
                    int i;
                    l_434--;
                    if ((*g_366))
                        continue;
                    for (l_274 = 0; (l_274 <= 8); l_274 += 1)
                    { /* block id: 155 */
                        int i;
                        l_431[(l_141 + 1)] = (void*)0;
                    }
                    for (l_378 = 1; (l_378 >= 0); l_378 -= 1)
                    { /* block id: 160 */
                        union U5 **l_437 = &l_288;
                        (*l_437) = (void*)0;
                        return &g_121;
                    }
                }
                for (l_353 = 0; (l_353 <= 8); l_353 += 1)
                { /* block id: 167 */
                    int32_t *l_440[9][2] = {{&l_187[1][1][1],&l_193[0][6][0]},{&l_193[0][6][0],&l_187[1][1][1]},{&l_193[0][6][0],&l_193[0][6][0]},{&l_187[1][1][1],&l_193[0][6][0]},{&l_193[0][6][0],&l_187[1][1][1]},{&l_193[0][6][0],&l_193[0][6][0]},{&l_187[1][1][1],&l_193[0][6][0]},{&l_193[0][6][0],&l_187[1][1][1]},{&l_193[0][6][0],&l_193[0][6][0]}};
                    int i, j;
                    l_193[0][6][1] = (safe_rshift_func_int8_t_s_s(l_127[1], (l_433 == 0x07761BF0L)));
                    for (l_189 = 0; (l_189 <= 1); l_189 += 1)
                    { /* block id: 171 */
                        int32_t **l_441 = &l_440[6][0];
                        (*l_426) = 0x7.F809D9p+66;
                        (*l_441) = (void*)0;
                    }
                }
                for (l_376 = 8; (l_376 >= 2); l_376 -= 1)
                { /* block id: 178 */
                    int16_t l_443 = (-1L);
                    int32_t l_448 = (-1L);
                    int32_t l_449 = 0x36AD07AAL;
                    int8_t **l_472[9][3][1] = {{{&l_118},{&l_115},{&l_117}},{{&l_118},{&l_118},{&l_118}},{{&l_118},{&l_117},{&l_115}},{{&l_118},{&l_115},{&l_117}},{{&l_118},{&l_118},{&l_118}},{{&l_118},{&l_117},{&l_115}},{{&l_118},{&l_115},{&l_117}},{{&l_118},{&l_118},{&l_118}},{{&l_118},{&l_117},{&l_115}}};
                    struct S1 ****l_476[2];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_476[i] = (void*)0;
                    for (l_359 = 0; (l_359 <= 3); l_359 += 1)
                    { /* block id: 181 */
                        int32_t *l_442[6][1] = {{&l_374[0][5]},{&l_195[3]},{&l_374[0][5]},{&l_195[3]},{&l_374[0][5]},{&l_195[3]}};
                        uint16_t **l_457 = (void*)0;
                        uint16_t **l_458 = (void*)0;
                        uint16_t **l_459 = &l_352;
                        uint64_t *l_470[10] = {&l_354,&l_380[0][0],(void*)0,&l_380[0][0],&l_354,&l_380[0][0],&l_354,&l_380[0][0],&l_380[0][0],&l_354};
                        int i, j;
                        --l_445;
                        g_450[0]--;
                        g_159[3] ^= (((safe_mul_func_int8_t_s_s((((((*l_459) = (g_120 = (*l_347))) == (void*)0) || (!(((safe_lshift_func_int8_t_s_s(l_195[2], (safe_div_func_uint8_t_u_u((l_193[0][6][0] , (safe_mod_func_int64_t_s_s(0x162B1B51D8E311F2LL, ((l_373 = (safe_mod_func_int64_t_s_s(l_469, l_448))) , 0xF20BB6DDCB2AD97BLL)))), ((*l_118) = (((l_471 = (void*)0) == l_472[7][2][0]) || (*g_10))))))) , 0xFEL) & l_449))) ^ g_257[0]), 0x86L)) , g_473) == g_473);
                    }
                    g_477[3] = l_475;
                    for (l_444 = 0; (l_444 <= 3); l_444 += 1)
                    { /* block id: 194 */
                        const struct S0 *l_481 = &l_383[4][4];
                        const struct S0 **l_480 = &l_481;
                        const struct S0 ***l_479 = &l_480;
                        uint32_t ***l_483[10][9][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
                        float *l_485 = (void*)0;
                        int32_t *l_487[1][9][1];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 9; j++)
                            {
                                for (k = 0; k < 1; k++)
                                    l_487[i][j][k] = &l_374[0][0];
                            }
                        }
                        (*l_479) = (void*)0;
                        l_448 = ((l_486 = ((l_482[4] == (g_484 = &g_130)) , l_485)) != (void*)0);
                        g_508 ^= (safe_mul_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((((safe_sub_func_int32_t_s_s((l_426 != l_486), ((+g_121) < (((*l_118) = l_496[2]) <= p_73)))) < ((safe_rshift_func_int8_t_s_u(p_73, 7)) >= (safe_sub_func_int8_t_s_s((safe_div_func_int16_t_s_s((g_305 , ((*g_120) > (((+(safe_mul_func_int8_t_s_s((l_449 = (safe_add_func_int64_t_s_s(((((*l_128) = ((4294967295UL & 0xDCD5E1B1L) && l_312)) , p_73) < (*g_120)), p_73))), l_433))) == p_73) <= (*g_130)))), l_239)), (-1L))))) && 255UL), 5)), g_159[0]));
                    }
                    for (l_444 = 1; (l_444 >= 0); l_444 -= 1)
                    { /* block id: 206 */
                        uint16_t *l_509 = &g_121;
                        return l_509;
                    }
                }
            }
            l_522 ^= (safe_mod_func_int8_t_s_s((l_197 | 3UL), ((*l_128) ^= (safe_sub_func_uint32_t_u_u((~(!p_73)), (safe_sub_func_int8_t_s_s(p_73, ((safe_add_func_uint64_t_u_u(9UL, (l_265.f0 || (((void*)0 == l_520) & (l_187[0][1][1] != 65535UL))))) & p_73))))))));
        }
        else
        { /* block id: 213 */
            float **l_527 = &l_486;
            uint32_t ***l_530 = (void*)0;
            int32_t l_540 = 0x09D58044L;
            int32_t *l_541 = &l_193[0][0][1];
            int32_t **l_543 = &l_541;
            int32_t *l_544 = &l_96;
            int32_t *l_545 = (void*)0;
            int32_t *l_546 = &l_189;
            int32_t *l_547 = &l_195[3];
            int32_t *l_548 = &l_433;
            int32_t *l_549 = &l_189;
            int32_t *l_550[3][2][10] = {{{&l_193[0][6][0],(void*)0,&l_193[0][6][0],&l_96,&l_195[3],&l_96,(void*)0,(void*)0,(void*)0,(void*)0},{&l_96,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_96,&l_195[3]}},{{&l_193[0][6][0],&l_96,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_96,&l_193[0][6][0]},{&l_96,(void*)0,&l_96,(void*)0,&l_311[2][2][3],&l_193[0][6][0],&l_311[2][2][3],(void*)0,&l_96,(void*)0}},{{&l_195[3],(void*)0,&l_193[0][6][0],(void*)0,&l_311[2][2][3],&l_193[0][6][0],&l_193[0][6][0],&l_311[2][2][3],(void*)0,&l_193[0][6][0]},{&l_311[2][2][3],&l_311[2][2][3],&l_96,&l_195[3],(void*)0,&l_193[0][6][0],&l_96,&l_193[0][6][0],(void*)0,&l_195[3]}}};
            struct S0 l_560 = {1761};
            struct S2 *l_607 = &g_85[1];
            int i, j, k;
            (*l_541) = ((*g_120) <= (safe_mul_func_uint16_t_u_u(((g_182 |= ((safe_rshift_func_uint16_t_u_u((((l_527 != (void*)0) & (safe_lshift_func_int8_t_s_s(((void*)0 == l_530), ((((safe_lshift_func_int8_t_s_u((safe_mul_func_int8_t_s_s((0x744BD52A7AB22855LL & (safe_rshift_func_uint16_t_u_s(p_73, ((p_73 ^ (l_96 = ((safe_div_func_int64_t_s_s(g_257[6], g_180.f0.f0)) < g_13))) ^ 0x08D53B86L)))), 255UL)), l_379[6])) & 0x7D64EE45L) <= 1L) ^ 0L)))) | l_539), 3)) , 1L)) || l_540), 0x8943L)));
            (*l_543) = &l_540;
            ++l_551[2];
            for (g_113 = 0; (g_113 > 9); g_113++)
            { /* block id: 221 */
                uint32_t **** const l_571 = &l_125;
                int64_t *l_581 = (void*)0;
                int64_t *l_582 = (void*)0;
                int64_t *l_583 = &l_141;
                int32_t l_584 = (-7L);
                struct S2 *l_606 = (void*)0;
                int32_t l_632 = (-1L);
                if ((+((((((safe_mod_func_uint16_t_u_u((~(l_560 , ((l_311[5][1][0] = ((((((*g_130)++) >= (l_584 |= (l_193[0][3][0] |= (safe_sub_func_int64_t_s_s(g_450[3], ((*l_583) = ((safe_div_func_uint32_t_u_u((~((safe_add_func_int64_t_s_s((~(((l_571 == l_571) ^ (~g_85[1].f0)) != (safe_lshift_func_int8_t_s_s((safe_div_func_int16_t_s_s((safe_div_func_uint32_t_u_u(((&l_354 != &p_73) < 0xA9F3DE4426D0516DLL), (safe_mul_func_int16_t_s_s(g_182, 0xE7D0L)))), p_73)), 4)))), g_119[2])) , 0UL)), p_73)) < 1L))))))) && l_383[2][3].f0) < 0x36L) && l_584)) || p_73))), (-7L))) & p_73) , 0xB2C9377FA699B9E6LL) | p_73) || p_73) , (*g_120))))
                { /* block id: 227 */
                    float l_591 = (-0x9.Dp-1);
                    struct S2 **l_608 = &l_607;
                    uint32_t ** const *l_609[10][6] = {{(void*)0,&g_123,&l_101,&l_101,&g_123,&l_101},{&l_103,(void*)0,&l_101,(void*)0,&l_103,&l_103},{&g_123,(void*)0,&l_101,&g_123,&g_123,&l_101},{&g_123,&g_123,&l_101,(void*)0,(void*)0,&l_101},{&l_103,&l_103,&l_101,&l_101,(void*)0,&l_103},{(void*)0,&g_123,&l_101,&l_101,&g_123,&l_101},{&l_103,(void*)0,&l_101,(void*)0,&l_103,&l_103},{&g_123,(void*)0,&l_101,&g_123,&g_123,&l_101},{&g_123,&g_123,&l_101,(void*)0,(void*)0,&l_101},{&l_103,&l_103,&l_101,&l_101,(void*)0,&l_103}};
                    uint32_t ** const **l_610 = (void*)0;
                    uint32_t ** const **l_611 = &l_609[9][3];
                    struct S0 **l_614 = &g_309;
                    struct S0 *l_616 = &g_139;
                    struct S0 **l_615 = &l_616;
                    float *l_619 = &l_591;
                    int i, j;
                    for (l_291 = 22; (l_291 == 23); ++l_291)
                    { /* block id: 230 */
                        struct S1 l_587[2][5][8] = {{{{0xBC3941B3L},{0xD5EF7EB0L},{0x8F8A88B6L},{0x54F3722CL},{4294967291UL},{0x54F3722CL},{0x8F8A88B6L},{0xD5EF7EB0L}},{{1UL},{0x8F8A88B6L},{4294967295UL},{0x54F3722CL},{0x641A9658L},{4294967287UL},{4294967287UL},{0x641A9658L}},{{0xFAE713A1L},{0x641A9658L},{0x641A9658L},{0xFAE713A1L},{1UL},{0xD5EF7EB0L},{4294967287UL},{0xBC3941B3L}},{{0x8F8A88B6L},{0xFAE713A1L},{4294967295UL},{4294967287UL},{4294967295UL},{0xFAE713A1L},{0x8F8A88B6L},{4294967291UL}},{{4294967295UL},{0xFAE713A1L},{0x8F8A88B6L},{4294967291UL},{0xD5EF7EB0L},{0xD5EF7EB0L},{4294967291UL},{0x8F8A88B6L}}},{{{0x641A9658L},{0x641A9658L},{0xFAE713A1L},{1UL},{0xD5EF7EB0L},{4294967287UL},{0xBC3941B3L},{4294967287UL}},{{4294967295UL},{0x8F8A88B6L},{1UL},{0x8F8A88B6L},{4294967295UL},{0x54F3722CL},{0x641A9658L},{4294967287UL}},{{0x8F8A88B6L},{0xD5EF7EB0L},{0xBC3941B3L},{1UL},{1UL},{0xBC3941B3L},{0xD5EF7EB0L},{0x8F8A88B6L}},{{0xFAE713A1L},{0x54F3722CL},{0xBC3941B3L},{4294967291UL},{0x641A9658L},{4294967295UL},{0x641A9658L},{4294967291UL}},{{1UL},{0x8D874D3DL},{0x54F3722CL},{0x8F8A88B6L},{0xD5EF7EB0L},{0xBC3941B3L},{1UL},{1UL}}}};
                        int i, j, k;
                        (*g_588) = l_587[0][4][3];
                    }
                    l_189 &= ((((safe_sub_func_uint16_t_u_u(((*l_352) = ((*g_366) != (safe_mod_func_uint32_t_u_u(((safe_sub_func_int64_t_s_s(((*l_541) = (safe_lshift_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_u(((*l_128) |= (((((((safe_mul_func_uint8_t_u_u(p_73, ((safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((((l_606 = (void*)0) == ((*l_608) = l_607)) ^ (((*l_611) = l_609[2][4]) == (*l_571))), (safe_mul_func_int8_t_s_s((((*g_124) , l_520) != ((*l_615) = ((*l_614) = (l_122[3][6] , (void*)0)))), (-1L))))), l_433)) > p_73))) <= g_13) & 1L) <= (*l_541)) && p_73) >= p_73) < g_13)), l_197)) & 0xE9A79DF1A724106FLL), 13))), p_73)) == 0UL), 0x19A85849L)))), (*l_548))) <= 0xA881A6BBL) >= (-1L)) & l_193[0][6][0]);
                    l_195[2] = ((*l_619) = (safe_mul_func_float_f_f(0xD.80435Dp+5, (*g_329))));
                }
                else
                { /* block id: 244 */
                    (*l_544) &= l_584;
                    return (*l_347);
                }
                for (l_539 = 0; (l_539 == (-13)); l_539--)
                { /* block id: 250 */
                    struct S2 l_624[5] = {{-7L},{-7L},{-7L},{-7L},{-7L}};
                    int i;
                    for (l_433 = 19; (l_433 == (-27)); --l_433)
                    { /* block id: 253 */
                        return &g_121;
                    }
                    l_624[4] = g_85[1];
                }
                for (g_508 = 0; (g_508 >= 44); g_508 = safe_add_func_int16_t_s_s(g_508, 5))
                { /* block id: 260 */
                    union U3 *l_627 = &g_628;
                    int32_t l_636 = 0L;
                    if (((*l_547) , p_73))
                    { /* block id: 261 */
                        union U3 **l_629 = &l_627;
                        int32_t l_635 = 0x6FB31F9AL;
                        float *l_637 = &l_114;
                        (*l_629) = l_627;
                        (*l_637) = (((safe_mul_func_float_f_f(l_584, (g_89 , ((l_632 = ((void*)0 != &l_584)) == ((l_195[5] = (l_189 = (-0x1.1p+1))) == (*g_329)))))) >= p_73) == (l_636 = (safe_add_func_float_f_f((g_278 != (l_635 > g_87[6])), p_73))));
                    }
                    else
                    { /* block id: 268 */
                        volatile uint8_t * volatile ** volatile *l_639 = &g_297;
                        volatile uint8_t * volatile ** volatile **l_638 = &l_639;
                        (*l_638) = &g_297;
                        (*l_543) = (*l_543);
                    }
                    (*l_547) = (safe_lshift_func_uint8_t_u_u(255UL, 0));
                    for (g_121 = 21; (g_121 > 12); g_121--)
                    { /* block id: 275 */
                        if (l_584)
                            break;
                        (*l_541) |= (g_644 , l_632);
                    }
                }
                (*l_544) &= p_73;
            }
        }
    }
    for (l_239 = 0; (l_239 <= 4); l_239 += 1)
    { /* block id: 286 */
        int32_t *l_645 = &l_311[5][1][0];
        int32_t *l_646 = &l_96;
        int32_t l_647[7][4] = {{1L,1L,1L,1L},{1L,1L,1L,1L},{1L,1L,1L,1L},{1L,1L,1L,1L},{1L,1L,1L,1L},{1L,1L,1L,1L},{1L,1L,1L,1L}};
        int32_t *l_648 = &l_647[5][3];
        int32_t *l_649 = &l_311[5][1][0];
        int32_t *l_650 = &l_647[4][1];
        int32_t *l_651 = (void*)0;
        int32_t *l_652 = (void*)0;
        int32_t *l_653 = (void*)0;
        int32_t *l_654 = &g_159[0];
        int32_t *l_655 = &l_647[1][2];
        int32_t *l_656 = &l_195[3];
        int32_t *l_657 = &l_647[2][2];
        int32_t *l_658[4];
        uint8_t *l_696 = &l_659;
        uint8_t * const *l_695 = &l_696;
        uint8_t * const **l_694 = &l_695;
        uint8_t * const ***l_693 = &l_694;
        uint8_t * const ****l_692[10] = {&l_693,&l_693,&l_693,&l_693,&l_693,&l_693,&l_693,&l_693,&l_693,&l_693};
        int64_t l_793 = 0x8D7E5BF3771991F3LL;
        uint32_t ***l_801 = (void*)0;
        uint32_t ****l_800 = &l_801;
        uint32_t *****l_799 = &l_800;
        uint32_t l_805 = 4294967295UL;
        int8_t l_832 = 0x71L;
        int8_t l_834 = 0L;
        uint16_t *l_838 = &l_238;
        float *l_850 = &g_87[3];
        struct S2 l_851 = {0x278F167BL};
        struct S1 *** const *l_856[6][2] = {{&g_477[2],&g_477[2]},{&g_477[2],&g_477[2]},{&g_477[2],&g_477[2]},{&g_477[2],&g_477[2]},{&g_477[2],&g_477[2]},{&g_477[2],&g_477[2]}};
        union U5 ***l_863 = &g_740;
        union U5 ****l_862 = &l_863;
        struct S0 l_928 = {15991};
        int32_t l_945[8] = {0x78AA80F8L,0x78AA80F8L,0xDB50E078L,0x78AA80F8L,0x78AA80F8L,0xDB50E078L,0x78AA80F8L,0x78AA80F8L};
        union U5 * const *l_963 = &g_290;
        int i, j;
        for (i = 0; i < 4; i++)
            l_658[i] = &l_195[3];
        l_659--;
        for (g_628.f0 = 0; (g_628.f0 <= 4); g_628.f0 += 1)
        { /* block id: 290 */
            int32_t l_662[8] = {0x3D11ABE3L,0x3D11ABE3L,0x3D11ABE3L,0x3D11ABE3L,0x3D11ABE3L,0x3D11ABE3L,0x3D11ABE3L,0x3D11ABE3L};
            int32_t l_684 = 0x32995EB3L;
            int64_t l_701 = 0x2EFB14B93A83ED6DLL;
            union U4 *l_711 = &g_180;
            const struct S1 l_722 = {4294967295UL};
            int16_t *l_729 = &l_197;
            struct S2 *l_735 = &g_85[1];
            union U3 *l_761 = &g_762;
            struct S2 **l_767 = (void*)0;
            int32_t l_820 = 0x0056E0B9L;
            int32_t l_822 = (-1L);
            int32_t l_823 = (-1L);
            int32_t l_824[7][6] = {{6L,0L,0xA71A3AB5L,0L,0xC00B8841L,(-2L)},{(-1L),0x36DE06A0L,0xC00B8841L,0L,(-2L),(-2L)},{9L,0xA71A3AB5L,0xA71A3AB5L,9L,1L,0L},{(-2L),(-1L),0xBE9BDA4FL,0x3C4AA3EFL,0x809CC349L,0xC00B8841L},{0xBE9BDA4FL,(-1L),0x191AA0D5L,0x36DE06A0L,0x809CC349L,0x36DE06A0L},{0L,(-1L),0L,0x6A149095L,1L,(-1L)},{0L,0xA71A3AB5L,0L,0xC00B8841L,(-2L),1L}};
            int16_t l_833 = 3L;
            uint16_t *l_839 = &g_121;
            float *l_852 = &g_87[1];
            uint8_t ** const l_908 = &l_696;
            uint8_t ** const *l_907 = &l_908;
            union U5 * const *l_927 = &g_290;
            union U5 * const **l_926 = &l_927;
            union U5 * const ***l_925 = &l_926;
            union U5 * const ****l_924 = &l_925;
            struct S1 *l_966 = &g_252[5];
            int i, j;
            for (p_73 = 1; (p_73 <= 4); p_73 += 1)
            { /* block id: 293 */
                uint16_t *l_663 = &g_121;
                int32_t l_686[7][5] = {{1L,2L,1L,0x8ED869E9L,0xD464E598L},{0x532CE596L,0x9E1508CAL,8L,0xA9609917L,0xD464E598L},{0x3AE3F42EL,0xA1A72128L,0xA1A72128L,0x3AE3F42EL,1L},{(-9L),0x3AE3F42EL,8L,0xD464E598L,0x9E1508CAL},{(-9L),8L,1L,8L,(-9L)},{0x3AE3F42EL,0xA9609917L,0x532CE596L,0xD464E598L,0xA1A72128L},{0x532CE596L,0xA9609917L,0x3AE3F42EL,0x3AE3F42EL,0xA9609917L}};
                uint16_t ***l_706 = &l_347;
                int i, j;
                if (l_662[2])
                { /* block id: 294 */
                    return l_663;
                }
                else
                { /* block id: 296 */
                    uint8_t *l_683[2][7] = {{&l_659,&l_659,&l_659,&l_659,&l_659,&l_659,&l_659},{&g_508,&l_659,&l_659,&g_508,&l_659,&l_659,&g_508}};
                    int32_t l_700 = (-1L);
                    int32_t l_702 = 0L;
                    int i, j;
                    if ((((*g_124) , g_508) == (safe_mod_func_int16_t_s_s((l_203[l_239][(p_73 + 3)] = (g_666 , ((+(safe_lshift_func_uint16_t_u_s(((safe_add_func_int32_t_s_s((0L || (+(safe_add_func_uint8_t_u_u(6UL, (((safe_mul_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u((((*l_118) ^= ((safe_mod_func_int64_t_s_s(((*l_112) = ((-1L) && (g_687[0][1]++))), (safe_mul_func_int16_t_s_s(p_73, l_239)))) == (((l_692[8] == g_697) , g_84[1]) == p_73))) == 255UL), l_700)), (**g_484))), l_701)) ^ p_73) | l_197))))), p_73)) < l_686[5][1]), 0))) || p_73))), (*g_120)))))
                    { /* block id: 301 */
                        return (*l_347);
                    }
                    else
                    { /* block id: 303 */
                        uint32_t l_703 = 4UL;
                        union U4 **l_712 = (void*)0;
                        l_703++;
                        (*g_708) = l_706;
                        g_713 = l_711;
                    }
                    return g_715[0][0][1];
                }
            }
        }
    }
    (*g_986) = ((g_984 = &l_203[4][0]) == (l_985 = &g_182));
    return (*l_347);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_56, "g_56", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_84[i], "g_84[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_85[i].f0, "g_85[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_87[i], sizeof(g_87[i]), "g_87[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_89.f0.f0, "g_89.f0.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_93[i][j], "g_93[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_113, "g_113", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_119[i], "g_119[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_121, "g_121", print_hash_value);
    transparent_crc(g_136.f0, "g_136.f0", print_hash_value);
    transparent_crc(g_139.f0, "g_139.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_159[i], "g_159[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_180.f0.f0, "g_180.f0.f0", print_hash_value);
    transparent_crc(g_182, "g_182", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_252[i].f0, "g_252[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_257[i], "g_257[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_278, "g_278", print_hash_value);
    transparent_crc(g_281, "g_281", print_hash_value);
    transparent_crc(g_305.f0.f0, "g_305.f0.f0", print_hash_value);
    transparent_crc(g_313, "g_313", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc_bytes(&g_330[i], sizeof(g_330[i]), "g_330[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_432[i], "g_432[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_450[i], "g_450[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_508, "g_508", print_hash_value);
    transparent_crc(g_628.f0, "g_628.f0", print_hash_value);
    transparent_crc(g_685, "g_685", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_687[i][j], "g_687[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_714.f0.f0, "g_714.f0.f0", print_hash_value);
    transparent_crc(g_737.f0.f0, "g_737.f0.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_758[i].f0, "g_758[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_762.f0, "g_762.f0", print_hash_value);
    transparent_crc(g_818, "g_818", print_hash_value);
    transparent_crc_bytes (&g_917, sizeof(g_917), "g_917", print_hash_value);
    transparent_crc(g_1019.f0.f0, "g_1019.f0.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1040[i], "g_1040[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1046.f0, "g_1046.f0", print_hash_value);
    transparent_crc(g_1079.f0, "g_1079.f0", print_hash_value);
    transparent_crc(g_1080.f0, "g_1080.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1081[i][j].f0, "g_1081[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1082.f0, "g_1082.f0", print_hash_value);
    transparent_crc(g_1083.f0, "g_1083.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1084[i][j][k].f0, "g_1084[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1086.f0, "g_1086.f0", print_hash_value);
    transparent_crc(g_1250, "g_1250", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1280[i].f0, "g_1280[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1332[i][j], "g_1332[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1479.f0, "g_1479.f0", print_hash_value);
    transparent_crc(g_1480.f0, "g_1480.f0", print_hash_value);
    transparent_crc(g_1585, "g_1585", print_hash_value);
    transparent_crc(g_1624, "g_1624", print_hash_value);
    transparent_crc(g_1644, "g_1644", print_hash_value);
    transparent_crc(g_1649, "g_1649", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1653[i][j].f0.f0, "g_1653[i][j].f0.f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1704[i][j], "g_1704[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1785.f0.f0, "g_1785.f0.f0", print_hash_value);
    transparent_crc(g_1817.f0, "g_1817.f0", print_hash_value);
    transparent_crc(g_1834, "g_1834", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1844[i].f0.f0, "g_1844[i].f0.f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1949, "g_1949", print_hash_value);
    transparent_crc(g_1953.f0.f0, "g_1953.f0.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2006[i], "g_2006[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 513
   depth: 1, occurrence: 44
XXX total union variables: 24

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 29
breakdown:
   indirect level: 0, occurrence: 15
   indirect level: 1, occurrence: 3
   indirect level: 2, occurrence: 6
   indirect level: 3, occurrence: 2
   indirect level: 4, occurrence: 3
XXX full-bitfields structs in the program: 15
breakdown:
   indirect level: 0, occurrence: 15
XXX times a bitfields struct's address is taken: 13
XXX times a bitfields struct on LHS: 3
XXX times a bitfields struct on RHS: 19
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 12

XXX max expression depth: 43
breakdown:
   depth: 1, occurrence: 250
   depth: 2, occurrence: 61
   depth: 3, occurrence: 5
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 5
   depth: 7, occurrence: 2
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 18, occurrence: 2
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 3
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 37, occurrence: 2
   depth: 43, occurrence: 1

XXX total number of pointers: 584

XXX times a variable address is taken: 1016
XXX times a pointer is dereferenced on RHS: 227
breakdown:
   depth: 1, occurrence: 197
   depth: 2, occurrence: 28
   depth: 3, occurrence: 2
XXX times a pointer is dereferenced on LHS: 256
breakdown:
   depth: 1, occurrence: 244
   depth: 2, occurrence: 9
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 37
XXX times a pointer is compared with address of another variable: 11
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 8803

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1245
   level: 2, occurrence: 150
   level: 3, occurrence: 34
   level: 4, occurrence: 12
   level: 5, occurrence: 2
XXX number of pointers point to pointers: 241
XXX number of pointers point to scalars: 284
XXX number of pointers point to structs: 36
XXX percent of pointers has null in alias set: 30.8
XXX average alias set size: 1.35

XXX times a non-volatile is read: 1405
XXX times a non-volatile is write: 731
XXX times a volatile is read: 101
XXX    times read thru a pointer: 12
XXX times a volatile is write: 39
XXX    times written thru a pointer: 3
XXX times a volatile is available for access: 7.82e+03
XXX percentage of non-volatile access: 93.8

XXX forward jumps: 0
XXX backward jumps: 4

XXX stmts: 243
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 14
   depth: 2, occurrence: 25
   depth: 3, occurrence: 35
   depth: 4, occurrence: 63
   depth: 5, occurrence: 75

XXX percentage a fresh-made variable is used: 16.6
XXX percentage an existing variable is used: 83.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


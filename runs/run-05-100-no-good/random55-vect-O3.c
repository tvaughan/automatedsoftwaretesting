/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2670873597
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   unsigned f0 : 29;
   signed : 0;
   unsigned f1 : 3;
   signed f2 : 2;
   signed f3 : 11;
   volatile signed f4 : 24;
};

#pragma pack(push)
#pragma pack(1)
struct S1 {
   signed f0 : 22;
   volatile unsigned f1 : 13;
   signed f2 : 31;
   volatile uint8_t  f3;
   const unsigned f4 : 12;
   signed f5 : 17;
   const unsigned : 0;
   signed f6 : 16;
};
#pragma pack(pop)

union U2 {
   int16_t  f0;
   const uint8_t  f1;
   volatile int64_t  f2;
};

union U3 {
   const volatile uint32_t  f0;
   uint32_t  f1;
   signed f2 : 19;
   volatile int16_t  f3;
};

union U4 {
   uint16_t  f0;
   int32_t  f1;
   volatile int16_t  f2;
   int64_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2 = 0x2BFBD3AEL;/* VOLATILE GLOBAL g_2 */
static int32_t g_3[3] = {0L,0L,0L};
static int32_t *g_14 = &g_3[0];
static int32_t ** volatile g_13 = &g_14;/* VOLATILE GLOBAL g_13 */
static volatile int32_t g_15[5] = {0x47AA2967L,0x47AA2967L,0x47AA2967L,0x47AA2967L,0x47AA2967L};
static volatile int32_t g_16[4][9][5] = {{{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,0xA644184EL,0xA644184EL},{1L,0x8158FB5CL,1L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L}},{{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L}},{{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L}},{{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L},{0x0E1C2A91L,0L,0x0E1C2A91L,1L,1L}}};
static int32_t g_17 = 0L;
static int8_t g_23 = 0L;
static uint16_t g_32[10] = {0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL,0x5F8AL};
static uint64_t g_67[6] = {18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL};
static union U4 g_69 = {1UL};/* VOLATILE GLOBAL g_69 */
static const union U4 *g_95 = (void*)0;
static const union U4 **g_94 = &g_95;
static uint8_t g_100 = 1UL;
static const uint8_t *g_105 = (void*)0;
static uint8_t g_108[7] = {253UL,253UL,253UL,253UL,253UL,253UL,253UL};
static uint8_t *g_107 = &g_108[5];
static int8_t g_127 = (-5L);
static union U4 g_131 = {1UL};/* VOLATILE GLOBAL g_131 */
static union U4 *g_130 = &g_131;
static union U4 **g_129[5][7] = {{(void*)0,(void*)0,&g_130,(void*)0,(void*)0,(void*)0,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{(void*)0,&g_130,&g_130,&g_130,&g_130,(void*)0,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{(void*)0,(void*)0,&g_130,&g_130,&g_130,(void*)0,(void*)0}};
static int32_t g_138[8][10][3] = {{{0xE1A5778DL,0x29FF7D88L,0L},{1L,0x24283DBFL,0xB3ACBD10L},{0x6DA497F6L,0x226C0DD8L,0x8287FB00L},{(-1L),0x26BEBBADL,1L},{0x6DA497F6L,0L,1L},{1L,0x075EA316L,0xC89A7884L},{0xE1A5778DL,0xBC530126L,0x8287FB00L},{7L,0x075EA316L,(-1L)},{0xEE195F76L,0L,0L},{1L,0x26BEBBADL,1L}},{{0xE1A5778DL,0x226C0DD8L,0L},{(-5L),0x24283DBFL,(-1L)},{0x6DA497F6L,0x29FF7D88L,0x8287FB00L},{0xB3ACBD10L,0x26BEBBADL,0xC89A7884L},{0x6DA497F6L,0x8A4C82FAL,1L},{(-5L),0x075EA316L,1L},{0xE1A5778DL,(-1L),0x8287FB00L},{1L,0x075EA316L,0xB3ACBD10L},{0xEE195F76L,0x8A4C82FAL,0L},{7L,0x26BEBBADL,7L}},{{0xE1A5778DL,0x29FF7D88L,0L},{1L,0x24283DBFL,0xB3ACBD10L},{0x6DA497F6L,0x226C0DD8L,0x8287FB00L},{(-1L),0x26BEBBADL,1L},{0x6DA497F6L,0L,1L},{1L,0x075EA316L,0xC89A7884L},{0xE1A5778DL,0xBC530126L,0x8287FB00L},{7L,0x075EA316L,(-1L)},{0xEE195F76L,0L,0L},{1L,0x26BEBBADL,1L}},{{0xE1A5778DL,0x226C0DD8L,0L},{(-5L),0x24283DBFL,(-1L)},{0x6DA497F6L,0x29FF7D88L,0x8287FB00L},{0xB3ACBD10L,0x26BEBBADL,0xC89A7884L},{0x6DA497F6L,0x8A4C82FAL,1L},{(-5L),0x075EA316L,1L},{0xE1A5778DL,(-1L),0x8287FB00L},{1L,0x075EA316L,0xB3ACBD10L},{0xEE195F76L,0x8A4C82FAL,0L},{7L,0x26BEBBADL,7L}},{{0xE1A5778DL,0x29FF7D88L,0L},{1L,0x24283DBFL,0xB3ACBD10L},{0x6DA497F6L,0x226C0DD8L,0x8287FB00L},{(-1L),0x26BEBBADL,1L},{0x6DA497F6L,0L,1L},{1L,0x075EA316L,0xC89A7884L},{0xE1A5778DL,0xBC530126L,0x8287FB00L},{7L,0x075EA316L,(-1L)},{0xEE195F76L,0L,0L},{1L,0x26BEBBADL,1L}},{{0xE1A5778DL,0x226C0DD8L,0L},{(-5L),0x24283DBFL,(-1L)},{0x6DA497F6L,0x29FF7D88L,0x8287FB00L},{0xB3ACBD10L,0x26BEBBADL,0xC89A7884L},{0x6DA497F6L,0x8A4C82FAL,1L},{(-5L),0x075EA316L,1L},{0xE1A5778DL,(-1L),0x8287FB00L},{1L,0x075EA316L,0xB3ACBD10L},{0xEE195F76L,0x8A4C82FAL,0L},{7L,0x26BEBBADL,7L}},{{0xE1A5778DL,0x6DA497F6L,0xB690FF0AL},{(-1L),9L,0x635FBB50L},{8L,1L,1L},{0x309C40C0L,1L,0x2C582E53L},{8L,0xE1A5778DL,0xB797A8B7L},{(-1L),0x527B815EL,1L},{0L,0x8287FB00L,1L},{0xA0356080L,0x527B815EL,0x309C40C0L},{1L,0xE1A5778DL,0xB690FF0AL},{0xEFB49F98L,1L,0xEFB49F98L}},{{0L,1L,0xB690FF0AL},{0x02624CEEL,9L,0x309C40C0L},{8L,0x6DA497F6L,1L},{0x635FBB50L,1L,1L},{8L,0L,0xB797A8B7L},{0x02624CEEL,0x527B815EL,0x2C582E53L},{0L,0xEE195F76L,1L},{0xEFB49F98L,0x527B815EL,0x635FBB50L},{1L,0L,0xB690FF0AL},{0xA0356080L,1L,0xA0356080L}}};
static uint32_t g_139 = 1UL;
static union U4 g_176 = {65526UL};/* VOLATILE GLOBAL g_176 */
static uint8_t g_196[5] = {0xB2L,0xB2L,0xB2L,0xB2L,0xB2L};
static int64_t g_201[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint64_t g_202 = 0x9BCD8D3F1B2D74E9LL;
static int16_t g_206 = 3L;
static int32_t g_208 = (-1L);
static uint16_t g_210 = 65535UL;
static float g_227 = 0xB.E6E705p+81;
static float g_229 = 0x1.Ep-1;
static float g_231[3] = {0xB.58B70Ap+16,0xB.58B70Ap+16,0xB.58B70Ap+16};
static int32_t *g_258 = &g_69.f1;
static uint8_t g_263[7] = {3UL,0xA0L,3UL,3UL,0xA0L,3UL,3UL};
static uint64_t g_276 = 0x00561AD35146581DLL;
static union U2 g_307[1][8][9] = {{{{0x47B9L},{0x5B1AL},{3L},{0x90B4L},{3L},{0x5B1AL},{0x47B9L},{0xC31AL},{1L}},{{0xC31AL},{0x1A4BL},{0x47B9L},{0x90B4L},{1L},{2L},{0L},{3L},{0L}},{{0L},{0x3426L},{3L},{3L},{0x3426L},{0L},{1L},{0xC31AL},{0x47B9L}},{{3L},{0L},{3L},{2L},{0xC31AL},{0L},{1L},{1L},{0L}},{{3L},{1L},{0x47B9L},{1L},{3L},{3L},{1L},{0x90B4L},{0L}},{{0x5B1AL},{1L},{3L},{0L},{0L},{0x1A4BL},{0L},{0x1A4BL},{0L}},{{1L},{0L},{0L},{1L},{0L},{3L},{0x47B9L},{0L},{-1L}},{{1L},{0x3426L},{0xFF6BL},{3L},{-1L},{0L},{0L},{-1L},{3L}}}};
static union U2 g_308[9][6] = {{{1L},{0L},{0x415DL},{0L},{1L},{0xB2BDL}},{{0L},{1L},{0xB2BDL},{0xB2BDL},{1L},{0L}},{{0xF1B1L},{0L},{-1L},{1L},{-1L},{0L}},{{-1L},{0xF1B1L},{0xB2BDL},{0x415DL},{0x415DL},{0xB2BDL}},{{-1L},{-1L},{0x415DL},{1L},{1L},{1L}},{{0xF1B1L},{-1L},{0xF1B1L},{0xB2BDL},{0x415DL},{0x415DL}},{{0L},{0xF1B1L},{1L},{0xF1B1L},{1L},{0xB2BDL}},{{0xB2BDL},{0xF1B1L},{-1L},{0xF1B1L},{0xB2BDL},{0x415DL}},{{0xF1B1L},{0xB2BDL},{0x415DL},{0x415DL},{0xB2BDL},{0xF1B1L}}};
static union U2 g_309[1] = {{0xD161L}};
static union U2 g_310[7][1] = {{{0x5074L}},{{0x5F63L}},{{0x5074L}},{{0x5F63L}},{{0x5074L}},{{0x5F63L}},{{0x5074L}}};
static union U2 g_311 = {0x9A67L};/* VOLATILE GLOBAL g_311 */
static union U2 g_312[1] = {{0xFA75L}};
static union U2 g_313 = {1L};/* VOLATILE GLOBAL g_313 */
static union U2 g_315 = {0x082EL};/* VOLATILE GLOBAL g_315 */
static uint32_t g_343[8][8] = {{0xB409539DL,0xA629C30AL,0xB409539DL,0UL,0UL,0xB409539DL,0xA629C30AL,0xB409539DL},{1UL,0UL,0UL,0UL,1UL,1UL,0UL,0UL},{1UL,1UL,0UL,0UL,0UL,1UL,1UL,0UL},{0xB409539DL,0UL,0UL,0xB409539DL,0xA629C30AL,0xB409539DL,0UL,0UL},{0UL,0xA629C30AL,0UL,0UL,0xA629C30AL,0UL,0xA629C30AL,0UL},{0xB409539DL,0xA629C30AL,0xB409539DL,0UL,0UL,0xB409539DL,0xA629C30AL,0xB409539DL},{1UL,0UL,0UL,0UL,1UL,1UL,0UL,0UL},{1UL,1UL,0UL,0UL,0UL,1UL,1UL,0UL}};
static uint32_t g_351[6][8] = {{0x3EA57738L,0x5C299DAFL,7UL,0x3EA57738L,7UL,0x5C299DAFL,0x3EA57738L,1UL},{0xFA61CDEDL,0x1F8846DAL,0x788A09A1L,0x3EA57738L,0x3EA57738L,0x788A09A1L,0x1F8846DAL,0xFA61CDEDL},{1UL,0x3EA57738L,0x5C299DAFL,7UL,0x3EA57738L,7UL,0x5C299DAFL,0x3EA57738L},{0xFA61CDEDL,0x5C299DAFL,1UL,0xFA61CDEDL,7UL,7UL,0xFA61CDEDL,1UL},{0x3EA57738L,0x3EA57738L,0x788A09A1L,0x1F8846DAL,0xFA61CDEDL,0x788A09A1L,0xFA61CDEDL,0x1F8846DAL},{1UL,0x1F8846DAL,1UL,7UL,0x1F8846DAL,0x5C299DAFL,0x5C299DAFL,0x1F8846DAL}};
static union U4 g_369[5] = {{0x36A7L},{0x36A7L},{0x36A7L},{0x36A7L},{0x36A7L}};
static const union U4 *****g_370 = (void*)0;
static uint16_t g_383 = 1UL;
static volatile uint16_t g_387 = 0xCBA5L;/* VOLATILE GLOBAL g_387 */
static volatile uint16_t *g_386[1] = {&g_387};
static volatile uint16_t * volatile * const g_385[8][4][7] = {{{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]}},{{&g_386[0],(void*)0,&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0]},{(void*)0,&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{(void*)0,&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0],&g_386[0]}},{{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0,&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0,&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]}},{{&g_386[0],&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],(void*)0,(void*)0,&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],(void*)0,(void*)0,&g_386[0]}},{{&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0,&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0,&g_386[0]}},{{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],(void*)0,(void*)0,&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]}},{{&g_386[0],&g_386[0],&g_386[0],&g_386[0],(void*)0,(void*)0,&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0,&g_386[0],&g_386[0]}},{{&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],(void*)0,&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],&g_386[0],&g_386[0],(void*)0,&g_386[0],&g_386[0]},{&g_386[0],&g_386[0],(void*)0,(void*)0,&g_386[0],&g_386[0],&g_386[0]}}};
static uint64_t g_418 = 0x23049C1B42A29871LL;
static const float g_449 = 0x1.FA7974p-28;
static float *g_472 = &g_231[1];
static union U4 **g_508[6] = {&g_130,&g_130,&g_130,&g_130,&g_130,&g_130};
static int64_t g_539 = 1L;
static uint32_t g_541 = 0xBDC370ABL;
static int32_t g_599 = 0xB94A87A8L;
static union U4 g_603 = {0UL};/* VOLATILE GLOBAL g_603 */
static struct S0 g_659 = {17897,0,0,-4,1431};/* VOLATILE GLOBAL g_659 */
static struct S0 g_662 = {17533,1,1,-30,3568};/* VOLATILE GLOBAL g_662 */
static struct S0 *g_661 = &g_662;
static struct S1 g_859 = {-350,7,21405,254UL,31,-339,168};/* VOLATILE GLOBAL g_859 */
static struct S1 *g_858 = &g_859;
static union U4 g_1018 = {65527UL};/* VOLATILE GLOBAL g_1018 */
static int32_t **g_1087 = (void*)0;
static int32_t ***g_1086 = &g_1087;
static int8_t g_1118 = (-1L);
static int64_t g_1119 = (-9L);
static int16_t g_1120 = 0xAAE3L;
static uint32_t g_1121 = 0x5A21F91CL;
static int64_t ** const g_1135 = (void*)0;
static int64_t ** const *g_1134 = &g_1135;
static volatile union U4 ** volatile **g_1164 = (void*)0;
static const uint32_t g_1167 = 0x1221C618L;
static volatile union U4 *g_1205 = (void*)0;
static volatile union U4 * volatile *g_1204 = &g_1205;
static const int64_t g_1232 = (-3L);
static volatile union U3 g_1333 = {1UL};/* VOLATILE GLOBAL g_1333 */
static const volatile union U3 *g_1332 = &g_1333;
static int8_t g_1354 = (-1L);
static uint16_t g_1365[5] = {0xC3CBL,0xC3CBL,0xC3CBL,0xC3CBL,0xC3CBL};
static int64_t g_1400 = 0xF13D396FCAE371C7LL;
static int8_t g_1401 = 0L;
static uint16_t g_1402 = 0x5B6EL;
static uint32_t g_1406[9][2] = {{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L},{0x2913DE69L,0x2913DE69L}};
static struct S1 g_1411 = {-1615,81,-7019,0xBDL,31,-31,100};/* VOLATILE GLOBAL g_1411 */
static int64_t *g_1422 = &g_201[0];
static int64_t **g_1421 = &g_1422;
static int64_t ***g_1420 = &g_1421;
static int64_t ****g_1419[3][6][9] = {{{&g_1420,&g_1420,(void*)0,&g_1420,&g_1420,&g_1420,(void*)0,&g_1420,(void*)0},{(void*)0,(void*)0,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420},{&g_1420,(void*)0,(void*)0,&g_1420,&g_1420,(void*)0,(void*)0,&g_1420,&g_1420},{&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420},{(void*)0,&g_1420,&g_1420,(void*)0,(void*)0,&g_1420,&g_1420,(void*)0,&g_1420},{&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,(void*)0,(void*)0,&g_1420}},{{&g_1420,(void*)0,(void*)0,(void*)0,&g_1420,&g_1420,(void*)0,&g_1420,&g_1420},{&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420},{&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,(void*)0,(void*)0},{&g_1420,&g_1420,&g_1420,(void*)0,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420},{(void*)0,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,(void*)0,&g_1420},{&g_1420,(void*)0,&g_1420,(void*)0,(void*)0,&g_1420,&g_1420,&g_1420,(void*)0}},{{&g_1420,(void*)0,&g_1420,&g_1420,&g_1420,(void*)0,&g_1420,(void*)0,&g_1420},{(void*)0,(void*)0,&g_1420,&g_1420,(void*)0,&g_1420,(void*)0,&g_1420,&g_1420},{(void*)0,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420},{&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,(void*)0,&g_1420},{(void*)0,(void*)0,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,(void*)0,&g_1420},{&g_1420,(void*)0,&g_1420,&g_1420,&g_1420,(void*)0,&g_1420,&g_1420,&g_1420}}};
static union U3 g_1445 = {0x1F75A328L};/* VOLATILE GLOBAL g_1445 */
static union U3 g_1446 = {1UL};/* VOLATILE GLOBAL g_1446 */
static union U3 g_1447 = {18446744073709551606UL};/* VOLATILE GLOBAL g_1447 */
static union U3 *g_1444[8] = {&g_1445,&g_1445,&g_1445,&g_1445,&g_1445,&g_1445,&g_1445,&g_1445};
static struct S0 g_1524[5][1][2] = {{{{251,0,1,20,3868},{251,0,1,20,3868}}},{{{251,0,1,20,3868},{251,0,1,20,3868}}},{{{251,0,1,20,3868},{251,0,1,20,3868}}},{{{251,0,1,20,3868},{251,0,1,20,3868}}},{{{251,0,1,20,3868},{251,0,1,20,3868}}}};
static float g_1550 = 0x3.8A3ADAp-73;
static float *g_1549 = &g_1550;
static union U4 g_1566 = {0xFF47L};/* VOLATILE GLOBAL g_1566 */
static union U4 g_1568 = {0UL};/* VOLATILE GLOBAL g_1568 */
static union U4 g_1570 = {9UL};/* VOLATILE GLOBAL g_1570 */
static struct S1 * volatile *g_1574 = &g_858;
static struct S1 * volatile **g_1573 = &g_1574;
static union U4 g_1578 = {65535UL};/* VOLATILE GLOBAL g_1578 */
static union U3 g_1586[1] = {{1UL}};
static uint32_t g_1603 = 6UL;
static struct S0 **g_1633 = (void*)0;
static struct S0 *** const g_1632 = &g_1633;
static uint32_t g_1671 = 0x8740A393L;
static union U4 g_1726 = {0xF213L};/* VOLATILE GLOBAL g_1726 */
static union U4 g_1728[1][4] = {{{9UL},{9UL},{9UL},{9UL}}};
static const int16_t g_1732 = (-6L);
static const int16_t *g_1731 = &g_1732;
static volatile int32_t g_1768 = 0x5BFECDBAL;/* VOLATILE GLOBAL g_1768 */
static volatile int32_t g_1769[3][5] = {{0xEFDB5738L,1L,1L,0xEFDB5738L,1L},{0xEFDB5738L,0xEFDB5738L,(-3L),0xEFDB5738L,0xEFDB5738L},{1L,0xEFDB5738L,1L,1L,0xEFDB5738L}};
static volatile int32_t * volatile g_1767[1][5] = {{&g_1768,&g_1768,&g_1768,&g_1768,&g_1768}};
static volatile int32_t * volatile * volatile g_1766 = &g_1767[0][1];/* VOLATILE GLOBAL g_1766 */
static volatile int32_t g_1772 = (-1L);/* VOLATILE GLOBAL g_1772 */
static volatile int32_t * volatile g_1771 = &g_1772;/* VOLATILE GLOBAL g_1771 */
static volatile int32_t g_1774 = 0x34F92F79L;/* VOLATILE GLOBAL g_1774 */
static volatile int32_t * volatile g_1773 = &g_1774;/* VOLATILE GLOBAL g_1773 */
static volatile int32_t g_1776 = 0x8D65B1D1L;/* VOLATILE GLOBAL g_1776 */
static volatile int32_t g_1777 = 0x1CE47EA5L;/* VOLATILE GLOBAL g_1777 */
static volatile int32_t g_1778[7][6] = {{0x225F4C80L,0x225F4C80L,0x1B8F0406L,0xF54776CAL,0x1B8F0406L,0x225F4C80L},{0x1B8F0406L,0L,0xF54776CAL,0xF54776CAL,0L,0x1B8F0406L},{0x225F4C80L,0x1B8F0406L,0xF54776CAL,0x1B8F0406L,0x225F4C80L,0x225F4C80L},{0L,0x1B8F0406L,0x1B8F0406L,0L,0L,0L},{0L,0L,0L,0x1B8F0406L,0x1B8F0406L,0L},{0x225F4C80L,0x225F4C80L,0x1B8F0406L,0xF54776CAL,0x1B8F0406L,0x225F4C80L},{0x1B8F0406L,0L,0xF54776CAL,0xF54776CAL,0L,0x1B8F0406L}};
static volatile int32_t * volatile g_1775[6][2] = {{&g_1778[2][0],&g_1778[2][0]},{&g_1777,&g_1778[2][0]},{&g_1778[2][0],&g_1777},{&g_1778[2][0],&g_1778[2][0]},{&g_1777,&g_1778[2][0]},{&g_1778[2][0],&g_1777}};
static volatile int32_t g_1780 = 0x3A400FCEL;/* VOLATILE GLOBAL g_1780 */
static volatile int32_t *g_1779[10] = {&g_1780,&g_1780,&g_1780,&g_1780,&g_1780,&g_1780,&g_1780,&g_1780,&g_1780,&g_1780};
static volatile int32_t * volatile *g_1770[10][4][4] = {{{&g_1779[8],&g_1779[6],&g_1779[9],&g_1779[9]},{(void*)0,(void*)0,&g_1779[9],&g_1779[9]},{&g_1779[9],&g_1779[9],&g_1779[9],&g_1773},{&g_1773,&g_1779[1],(void*)0,&g_1779[9]}},{{&g_1779[9],&g_1779[1],&g_1779[9],&g_1773},{&g_1779[1],&g_1779[9],&g_1779[9],&g_1779[9]},{&g_1779[9],(void*)0,(void*)0,&g_1779[9]},{&g_1779[9],&g_1779[6],&g_1779[9],&g_1779[9]}},{{&g_1779[9],&g_1779[9],&g_1779[1],(void*)0},{&g_1779[9],&g_1779[9],&g_1779[9],&g_1779[7]},{&g_1779[9],(void*)0,(void*)0,&g_1779[9]},{&g_1779[9],&g_1773,&g_1779[9],&g_1779[2]}},{{&g_1779[1],&g_1779[7],&g_1779[9],&g_1779[8]},{&g_1779[9],&g_1779[9],(void*)0,&g_1779[8]},{&g_1773,&g_1779[7],&g_1779[9],&g_1779[2]},{&g_1779[9],&g_1773,&g_1779[9],&g_1779[9]}},{{(void*)0,(void*)0,&g_1779[9],&g_1779[7]},{&g_1779[8],&g_1779[9],&g_1779[2],(void*)0},{&g_1779[9],&g_1779[9],&g_1779[2],&g_1779[9]},{&g_1779[8],&g_1779[6],&g_1779[9],&g_1779[9]}},{{(void*)0,(void*)0,&g_1779[9],&g_1779[9]},{&g_1779[9],&g_1779[9],&g_1779[9],&g_1773},{&g_1773,&g_1779[1],(void*)0,&g_1779[9]},{&g_1779[9],&g_1779[1],&g_1779[9],&g_1773}},{{&g_1779[1],&g_1779[9],&g_1779[9],(void*)0},{&g_1779[7],&g_1779[9],(void*)0,&g_1779[9]},{&g_1779[9],&g_1779[9],&g_1779[8],&g_1779[9]},{&g_1779[9],&g_1779[8],&g_1779[9],(void*)0}},{{&g_1779[9],&g_1779[9],&g_1779[8],&g_1779[9]},{&g_1779[9],(void*)0,(void*)0,&g_1779[9]},{&g_1779[7],&g_1779[9],&g_1779[9],&g_1779[9]},{&g_1779[9],&g_1779[9],&g_1773,&g_1779[2]}},{{&g_1779[3],&g_1779[9],&g_1779[9],&g_1779[2]},{&g_1779[9],&g_1779[9],&g_1779[1],&g_1779[9]},{&g_1779[9],&g_1779[9],&g_1779[9],&g_1779[9]},{&g_1779[9],(void*)0,&g_1779[9],&g_1779[9]}},{{&g_1779[2],&g_1779[9],&g_1779[9],(void*)0},{(void*)0,&g_1779[8],&g_1779[9],&g_1779[9]},{&g_1779[2],&g_1779[9],&g_1779[9],&g_1779[9]},{&g_1779[9],&g_1779[9],&g_1779[9],(void*)0}}};
static volatile int32_t * volatile * volatile *g_1765[2][7][4] = {{{(void*)0,(void*)0,&g_1770[1][1][2],(void*)0},{&g_1770[1][1][2],&g_1770[1][1][2],&g_1770[7][0][2],&g_1770[1][1][2]},{&g_1770[1][1][2],&g_1770[1][1][2],(void*)0,&g_1770[1][1][2]},{(void*)0,&g_1770[1][1][2],(void*)0,(void*)0},{(void*)0,(void*)0,&g_1770[7][0][2],&g_1770[1][1][2]},{(void*)0,&g_1770[1][1][2],(void*)0,(void*)0},{(void*)0,&g_1770[1][1][2],(void*)0,(void*)0}},{{&g_1770[1][1][2],&g_1770[1][1][2],&g_1770[7][0][2],(void*)0},{&g_1770[1][1][2],&g_1770[1][1][2],&g_1770[1][1][2],&g_1770[1][1][2]},{(void*)0,(void*)0,&g_1770[1][1][2],(void*)0},{&g_1770[1][1][2],&g_1770[1][1][2],&g_1770[7][0][2],&g_1770[1][1][2]},{&g_1770[1][1][2],&g_1770[1][1][2],(void*)0,&g_1770[1][1][2]},{(void*)0,&g_1770[1][1][2],(void*)0,(void*)0},{(void*)0,(void*)0,&g_1770[7][0][2],&g_1770[1][1][2]}}};
static volatile int32_t * volatile * volatile * volatile * const g_1764 = &g_1765[1][6][3];
static volatile int32_t * volatile * volatile * volatile * const  volatile *g_1763 = &g_1764;
static int32_t ** const **g_1835 = (void*)0;
static int32_t * volatile g_1853 = &g_1018.f1;/* VOLATILE GLOBAL g_1853 */
static const struct S0 g_1856 = {17648,1,-0,0,2210};/* VOLATILE GLOBAL g_1856 */
static volatile union U3 g_1859 = {18446744073709551611UL};/* VOLATILE GLOBAL g_1859 */
static uint32_t *g_1919 = &g_1406[3][1];
static volatile union U3 g_1923 = {0x80ACD4E4L};/* VOLATILE GLOBAL g_1923 */
static volatile union U3 g_1947 = {0xDB1852DCL};/* VOLATILE GLOBAL g_1947 */
static union U4 g_1967[7] = {{0UL},{0UL},{0UL},{0UL},{0UL},{0UL},{0UL}};
static union U3 g_1975[8] = {{0xA0DCC263L},{0xA0DCC263L},{0xA0DCC263L},{0xA0DCC263L},{0xA0DCC263L},{0xA0DCC263L},{0xA0DCC263L},{0xA0DCC263L}};
static volatile union U4 g_2040 = {0xA19DL};/* VOLATILE GLOBAL g_2040 */
static struct S0 g_2086 = {16976,1,1,-14,-1259};/* VOLATILE GLOBAL g_2086 */
static struct S0 g_2094 = {3547,0,-1,-33,553};/* VOLATILE GLOBAL g_2094 */
static union U4 g_2100 = {0x1185L};/* VOLATILE GLOBAL g_2100 */
static volatile union U2 g_2106 = {1L};/* VOLATILE GLOBAL g_2106 */
static uint16_t g_2125[9] = {0x2500L,0x2500L,0x2500L,0x2500L,0x2500L,0x2500L,0x2500L,0x2500L,0x2500L};
static int32_t ** volatile g_2129[1][2] = {{&g_258,&g_258}};
static int32_t ** volatile g_2130 = &g_14;/* VOLATILE GLOBAL g_2130 */
static volatile uint8_t g_2139 = 0x0CL;/* VOLATILE GLOBAL g_2139 */
static union U3 ** volatile g_2151 = &g_1444[2];/* VOLATILE GLOBAL g_2151 */
static struct S0 g_2191 = {16454,0,-0,-11,-1215};/* VOLATILE GLOBAL g_2191 */
static union U4 g_2209 = {0x35FDL};/* VOLATILE GLOBAL g_2209 */
static volatile int16_t g_2239 = 7L;/* VOLATILE GLOBAL g_2239 */
static const volatile union U3 g_2244 = {0x8472007FL};/* VOLATILE GLOBAL g_2244 */
static int32_t ** const  volatile g_2258 = &g_258;/* VOLATILE GLOBAL g_2258 */
static volatile union U2 g_2283[2] = {{3L},{3L}};
static volatile union U2 g_2290 = {0x6248L};/* VOLATILE GLOBAL g_2290 */
static int8_t g_2291[4] = {1L,1L,1L,1L};
static volatile struct S1 g_2296[4][8] = {{{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165}},{{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165}},{{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165}},{{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165},{-952,57,-2865,0x0BL,42,-274,165}}};
static volatile union U2 g_2303 = {-10L};/* VOLATILE GLOBAL g_2303 */
static union U4 g_2309 = {0xD04CL};/* VOLATILE GLOBAL g_2309 */
static union U4 ** const *g_2353 = &g_508[1];
static union U4 ** const ** volatile g_2352 = &g_2353;/* VOLATILE GLOBAL g_2352 */
static volatile struct S1 g_2396 = {-206,48,-1344,255UL,12,-202,222};/* VOLATILE GLOBAL g_2396 */
static volatile struct S0 g_2399 = {2927,1,0,29,555};/* VOLATILE GLOBAL g_2399 */
static volatile struct S0 g_2404 = {10527,1,-0,28,3786};/* VOLATILE GLOBAL g_2404 */
static volatile struct S0 g_2413 = {15017,1,-1,-10,-440};/* VOLATILE GLOBAL g_2413 */
static struct S0 g_2440 = {978,1,-1,-6,4004};/* VOLATILE GLOBAL g_2440 */


/* --- FORWARD DECLARATIONS --- */
static const int8_t  func_1(void);
static struct S0  func_20(uint64_t  p_21);
static uint64_t  func_26(uint32_t  p_27, const float  p_28);
static uint32_t  func_36(int32_t ** p_37);
static uint16_t  func_38(uint16_t * p_39);
static float  func_47(int32_t ** p_48, const int32_t ** p_49, uint8_t  p_50);
static int32_t ** func_51(int16_t  p_52, int32_t ** p_53, int32_t * p_54);
static int32_t ** func_56(uint16_t  p_57, uint32_t  p_58, int64_t  p_59, int32_t * p_60);
static uint16_t  func_62(int32_t ** const  p_63, uint64_t  p_64);
static union U4 ** func_70(union U4 * const  p_71, union U4 * const * p_72);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_13 g_17 g_127
 * writes: g_3 g_14 g_17
 */
static const int8_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_7 = 0x06L;
    for (g_3[0] = 0; (g_3[0] >= 18); g_3[0]++)
    { /* block id: 3 */
        int32_t *l_6[2][10][6] = {{{&g_3[0],(void*)0,&g_3[2],&g_3[1],&g_3[1],&g_3[0]},{&g_3[0],&g_3[0],&g_3[0],&g_3[1],&g_3[0],&g_3[1]},{&g_3[0],&g_3[0],&g_3[0],&g_3[1],&g_3[0],&g_3[1]},{&g_3[1],&g_3[2],&g_3[1],&g_3[0],&g_3[1],&g_3[1]},{&g_3[0],&g_3[0],&g_3[1],&g_3[0],&g_3[0],&g_3[0]},{&g_3[0],&g_3[0],&g_3[0],&g_3[0],&g_3[1],&g_3[1]},{&g_3[1],&g_3[1],&g_3[0],&g_3[1],&g_3[2],&g_3[1]},{&g_3[0],&g_3[0],&g_3[1],&g_3[1],&g_3[0],&g_3[0]},{&g_3[1],&g_3[1],&g_3[0],&g_3[0],&g_3[0],&g_3[0]},{&g_3[0],&g_3[2],&g_3[0],&g_3[0],&g_3[0],&g_3[0]}},{{&g_3[0],&g_3[0],&g_3[1],&g_3[0],&g_3[0],&g_3[0]},{&g_3[1],&g_3[0],&g_3[0],&g_3[1],&g_3[1],&g_3[0]},{&g_3[0],&g_3[0],&g_3[1],&g_3[1],&g_3[0],&g_3[0]},{&g_3[1],&g_3[0],&g_3[1],&g_3[0],&g_3[0],&g_3[1]},{&g_3[0],&g_3[1],&g_3[0],&g_3[0],&g_3[0],&g_3[1]},{&g_3[0],&g_3[0],&g_3[0],&g_3[0],&g_3[2],&g_3[0]},{&g_3[1],&g_3[0],&g_3[1],&g_3[1],&g_3[0],&g_3[1]},{&g_3[0],&g_3[0],&g_3[0],&g_3[1],&g_3[0],&g_3[1]},{&g_3[1],&g_3[2],&g_3[1],&g_3[0],&g_3[1],&g_3[1]},{&g_3[0],&g_3[0],&g_3[1],&g_3[0],&g_3[0],&g_3[0]}}};
        int i, j, k;
        l_7--;
        for (l_7 = 0; (l_7 == 20); l_7 = safe_add_func_int64_t_s_s(l_7, 3))
        { /* block id: 7 */
            int32_t *l_12[1][7][6] = {{{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]},{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]},{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]},{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]},{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]},{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]},{(void*)0,&g_3[0],(void*)0,&g_3[0],&g_3[0],&g_3[0]}}};
            int i, j, k;
            (*g_13) = l_12[0][4][0];
            for (g_17 = (-22); (g_17 > (-8)); g_17 = safe_add_func_int64_t_s_s(g_17, 8))
            { /* block id: 11 */
                int8_t *l_22 = &g_23;
                struct S0 *l_2441 = &g_1524[1][0][0];
            }
        }
    }
    return g_127;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_2 g_23 g_343 g_351 g_1411.f2 g_1406 g_659.f0 g_3 g_310.f1 g_1447.f2 g_1420 g_472 g_308.f0 g_138 g_201 g_107 g_108 g_100 g_206 g_94 g_95 g_315.f0 g_1120 g_1573 g_1568.f3 g_14 g_1118 g_1402 g_1332 g_17 g_1603 g_309.f0 g_1422 g_418 g_1568.f1 g_1632 g_1524.f2 g_258 g_1421 g_1365 g_1671 g_659.f1 g_313.f0 g_210 g_858 g_1578.f0 g_1086 g_1087 g_67 g_231 g_1763 g_311.f0 g_1835 g_1731 g_1732 g_69.f1 g_176.f0 g_1728.f0 g_307.f0 g_1121 g_1856 g_661 g_1859 g_662.f2 g_369.f2 g_309 g_1445.f2 g_1167 g_1919 g_1923 g_603.f0 g_1570.f0 g_1947 g_662.f3 g_208 g_2151 g_1566.f1 g_1568.f2 g_1018.f0 g_1354 g_2191 g_1446.f0 g_1401 g_2209 g_1578.f1 g_196 g_2125 g_2244 g_1728.f2 g_1766 g_1767 g_1769 g_1768 g_2258 g_130 g_131 g_1446.f1 g_2283 g_2290 g_2291 g_2296 g_2303 g_1400 g_1574 g_599 g_2094.f1 g_176.f1 g_2309.f3 g_2352 g_662 g_2396 g_1445.f1 g_2399 g_2404 g_2413 g_2440
 * writes: g_32 g_23 g_343 g_1406 g_1447.f2 g_1421 g_231 g_14 g_1401 g_472 g_227 g_229 g_1549 g_1550 g_206 g_95 g_1120 g_1568.f3 g_1118 g_1402 g_202 g_108 g_69.f1 g_1365 g_210 g_131.f3 g_312.f0 g_1671 g_1731 g_1763 g_311.f0 g_131.f1 g_1835 g_662 g_1566.f1 g_603.f1 g_67 g_1919 g_1121 g_603.f0 g_1570.f0 g_201 g_208 g_1444 g_1633 g_1354 g_1578.f1 g_1419 g_131.f0 g_196 g_258 g_1726.f3 g_661 g_130 g_1767 g_69.f3 g_418 g_858 g_127 g_599 g_176.f1 g_2309.f3 g_2353 g_2100.f1
 */
static struct S0  func_20(uint64_t  p_21)
{ /* block id: 13 */
    uint16_t *l_31 = &g_32[2];
    int32_t l_35 = 0L;
    int8_t *l_2337 = &g_127;
    int32_t *l_2341 = &g_2309.f1;
    int32_t *l_2342 = &g_2100.f1;
    int32_t *l_2343 = &g_599;
    uint32_t l_2344 = 3UL;
    union U4 ** const *l_2351 = &g_508[2];
    int32_t l_2379[6][8] = {{0xF13A4B1FL,0x197B2C3CL,0xBAE9140DL,0xBAE9140DL,0x197B2C3CL,0xF13A4B1FL,2L,1L},{(-2L),2L,0L,0x1A7160C6L,0xF13A4B1FL,0L,0x197B2C3CL,0L},{1L,0x1A7160C6L,0x05DE082AL,0x1A7160C6L,1L,(-1L),0x1A7160C6L,1L},{(-6L),1L,0xF13A4B1FL,0xBAE9140DL,0x1A7160C6L,(-6L),(-6L),0x1A7160C6L},{(-2L),0xF13A4B1FL,0xF13A4B1FL,(-2L),2L,0x05DE082AL,0L,1L},{0L,(-6L),0x197B2C3CL,2L,(-6L),(-2L),(-6L),2L}};
    int32_t l_2380 = 0L;
    uint8_t l_2381 = 0x94L;
    int64_t ****l_2402 = &g_1420;
    struct S0 *l_2406 = (void*)0;
    union U4 ***l_2416 = (void*)0;
    union U4 ****l_2415[5][4] = {{&l_2416,&l_2416,&l_2416,&l_2416},{&l_2416,&l_2416,&l_2416,&l_2416},{&l_2416,&l_2416,&l_2416,&l_2416},{&l_2416,&l_2416,&l_2416,&l_2416},{&l_2416,&l_2416,&l_2416,&l_2416}};
    union U4 *****l_2414[1];
    int32_t *l_2425 = &g_599;
    int32_t *l_2426 = &g_1726.f1;
    int32_t *l_2427 = &g_2309.f1;
    int32_t *l_2428 = (void*)0;
    int32_t *l_2429 = &g_1570.f1;
    int32_t *l_2430 = (void*)0;
    int32_t *l_2431 = &g_369[1].f1;
    int32_t *l_2432 = &l_2380;
    int32_t *l_2433[7][1] = {{(void*)0},{&l_2379[2][5]},{(void*)0},{(void*)0},{&l_2379[2][5]},{(void*)0},{(void*)0}};
    int32_t l_2434 = 5L;
    int8_t l_2435 = (-1L);
    int16_t l_2436[2][8][10] = {{{0xF2FCL,1L,0x45A3L,(-1L),(-6L),0L,(-6L),(-1L),0x45A3L,1L},{1L,0xE74AL,0L,1L,(-6L),0xDD5BL,0xDD5BL,(-6L),1L,0L},{(-6L),(-6L),0xF2FCL,1L,0x761CL,0xDD5BL,0x45A3L,0xDD5BL,0x761CL,1L},{1L,0xFFA2L,1L,0xDD5BL,(-1L),0L,0x45A3L,0x45A3L,0L,(-1L)},{0xF2FCL,(-6L),(-6L),0xF2FCL,1L,0x761CL,0xDD5BL,0x45A3L,0xDD5BL,0x761CL},{0L,0xE74AL,1L,0xE74AL,0L,1L,(-6L),0xDD5BL,0xDD5BL,(-6L)},{0x45A3L,1L,0xF2FCL,0xF2FCL,1L,0x45A3L,(-1L),(-6L),0L,(-6L)},{0xE74AL,0xF2FCL,0L,0xDD5BL,0L,0xF2FCL,0xE74AL,(-1L),0x761CL,0x761CL}},{{0xE74AL,0x761CL,0x45A3L,1L,1L,0x45A3L,0x761CL,0xE74AL,1L,(-1L)},{0x45A3L,0x761CL,0xE74AL,1L,(-1L),1L,0xE74AL,0x761CL,0x45A3L,1L},{0L,0xF2FCL,0xE74AL,(-1L),0x761CL,0x761CL,(-1L),0xE74AL,0xF2FCL,0L},{0xF2FCL,1L,0x45A3L,(-1L),(-6L),0L,(-6L),(-1L),0x45A3L,1L},{1L,0xE74AL,0L,1L,(-6L),0xDD5BL,0xDD5BL,(-6L),1L,0L},{(-6L),(-6L),0xF2FCL,1L,0x761CL,0xDD5BL,0x45A3L,0xDD5BL,0x761CL,1L},{1L,0xFFA2L,1L,0xDD5BL,(-1L),0L,0x45A3L,0x45A3L,0L,(-1L)},{0xF2FCL,(-6L),(-6L),(-6L),1L,0L,0xE74AL,1L,0xE74AL,0L}}};
    uint64_t l_2437 = 7UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_2414[i] = &l_2415[3][1];
    if ((safe_div_func_uint64_t_u_u(func_26((safe_mod_func_uint16_t_u_u(((*l_31)--), l_35)), p_21), (((((safe_sub_func_int8_t_s_s(((*l_2337) = 1L), (safe_sub_func_uint64_t_u_u(((((((((l_35 > (+0x20L)) ^ p_21) < (((p_21 != ((*l_2343) ^= 1L)) >= l_2344) >= g_2094.f1)) ^ 0xB9D2L) , 0x5D40L) , p_21) , &l_2343) != &l_2342), 18446744073709551606UL)))) ^ p_21) >= 1L) < 0UL) && (*l_2343)))))
    { /* block id: 1040 */
        union U4 *l_2345[2][8] = {{&g_69,&g_1967[0],&g_1018,&g_1018,&g_1967[0],&g_69,&g_1967[0],&g_1018},{(void*)0,&g_1967[0],(void*)0,&g_69,&g_69,(void*)0,&g_1967[0],(void*)0}};
        int i, j;
        (*g_94) = l_2345[0][2];
    }
    else
    { /* block id: 1042 */
        int32_t *l_2346 = (void*)0;
        (*g_2258) = l_2346;
    }
    for (g_176.f1 = 0; (g_176.f1 < 2); g_176.f1 = safe_add_func_uint16_t_u_u(g_176.f1, 7))
    { /* block id: 1047 */
        int32_t *l_2366 = (void*)0;
        int32_t **l_2367 = &l_2366;
        int32_t l_2368 = 0xB8642E5FL;
        int32_t l_2369[7];
        int32_t *l_2370 = (void*)0;
        int32_t *l_2371 = &g_603.f1;
        int32_t *l_2372 = &g_1570.f1;
        int32_t *l_2373 = &g_1967[1].f1;
        int32_t *l_2374 = (void*)0;
        int32_t *l_2375 = &g_1578.f1;
        int32_t *l_2376 = &g_1967[1].f1;
        int32_t *l_2377 = &g_369[1].f1;
        int32_t *l_2378[7] = {&l_2368,(void*)0,(void*)0,&l_2368,(void*)0,(void*)0,&l_2368};
        int i;
        for (i = 0; i < 7; i++)
            l_2369[i] = 0L;
        for (g_2309.f3 = 0; (g_2309.f3 == (-25)); g_2309.f3 = safe_sub_func_int64_t_s_s(g_2309.f3, 3))
        { /* block id: 1050 */
            uint16_t *l_2354 = &g_176.f0;
            uint16_t *l_2355 = &g_2100.f0;
            uint16_t l_2361 = 65526UL;
            int32_t l_2363 = (-5L);
            uint32_t l_2364 = 0xFF5ECCE3L;
            int32_t l_2365 = 0x91920B24L;
            (*g_2352) = l_2351;
            (*l_2343) |= (l_2365 ^= (((((l_2354 = l_2354) != l_2355) || ((safe_rshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u(p_21, 6)), (((~0x20L) || (l_2361 , (*g_1919))) , ((((*l_2342) = 0x5F88E10EL) < (((*g_1422) = (safe_unary_minus_func_int32_t_s(((&g_67[2] != ((*g_661) , (void*)0)) <= l_2363)))) && g_1400)) <= l_2361)))) > 0xC4F32B7BL)) < l_2364) < (*g_107)));
        }
        (*l_2367) = l_2366;
        --l_2381;
    }
    for (l_35 = 0; (l_35 == (-12)); l_35 = safe_sub_func_uint16_t_u_u(l_35, 1))
    { /* block id: 1063 */
        const uint8_t **l_2390 = &g_105;
        int32_t l_2395 = 0L;
        float *l_2397 = &g_231[0];
        int8_t l_2398 = 1L;
        (*l_2343) &= (safe_rshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u((((void*)0 == l_2390) && (p_21 <= ((((safe_rshift_func_uint8_t_u_s((((safe_lshift_func_uint8_t_u_u(l_2395, 2)) | ((((0xE059069C78C3D28BLL | ((g_2396 , p_21) > (l_2397 == l_2342))) == l_2395) < (*g_1919)) & g_1445.f1)) != 5L), 2)) & (*g_1919)) ^ l_2398) < 1UL))), 15)), 15));
        for (g_131.f1 = 0; (g_131.f1 <= 2); g_131.f1 += 1)
        { /* block id: 1067 */
            struct S0 *l_2405 = &g_659;
            (*g_661) = g_2399;
            (*l_2343) ^= ((safe_lshift_func_int16_t_s_s((&g_1134 != l_2402), 6)) , (!l_2395));
            (*g_661) = g_2404;
            for (g_1568.f3 = 2; (g_1568.f3 >= 0); g_1568.f3 -= 1)
            { /* block id: 1073 */
                if (p_21)
                    break;
                l_2406 = l_2405;
            }
        }
        (*l_2343) &= (safe_sub_func_int16_t_s_s(p_21, ((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_u(6L, p_21)) >= (((g_2413 , l_2414[0]) == &g_1164) | ((safe_rshift_func_uint8_t_u_s((safe_mod_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_s(((g_1524[1][0][0].f2 <= (safe_add_func_int64_t_s_s((0xB652L > (l_2395 | 0UL)), 0L))) , g_1856.f1), 8)), 0xCDA42CEDL)), g_1445.f1)) , (*g_107)))), 6)) > l_2398)));
    }
    ++l_2437;
    return g_2440;
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_23 g_32 g_343 g_351 g_1411.f2 g_1406 g_659.f0 g_3 g_310.f1 g_1447.f2 g_1420 g_472 g_308.f0 g_138 g_201 g_107 g_108 g_100 g_206 g_94 g_95 g_315.f0 g_1120 g_1573 g_1568.f3 g_14 g_1118 g_1402 g_1332 g_17 g_1603 g_309.f0 g_1422 g_418 g_1568.f1 g_1632 g_1524.f2 g_258 g_1421 g_1365 g_1671 g_659.f1 g_313.f0 g_210 g_858 g_1578.f0 g_1086 g_1087 g_67 g_231 g_1763 g_311.f0 g_1835 g_1731 g_1732 g_69.f1 g_176.f0 g_1728.f0 g_307.f0 g_1121 g_1856 g_661 g_1859 g_662.f2 g_369.f2 g_309 g_1445.f2 g_1167 g_1919 g_1923 g_603.f0 g_1570.f0 g_1947 g_662.f3 g_208 g_2151 g_1566.f1 g_1568.f2 g_1018.f0 g_1354 g_2191 g_1446.f0 g_1401 g_2209 g_1578.f1 g_196 g_2125 g_2244 g_1728.f2 g_1766 g_1767 g_1769 g_1768 g_2258 g_130 g_131 g_1446.f1 g_2283 g_2290 g_2291 g_2296 g_2303 g_1400 g_1574
 * writes: g_23 g_343 g_1406 g_1447.f2 g_1421 g_231 g_14 g_1401 g_472 g_227 g_229 g_1549 g_1550 g_206 g_95 g_1120 g_1568.f3 g_1118 g_1402 g_202 g_108 g_69.f1 g_1365 g_210 g_131.f3 g_312.f0 g_1671 g_1731 g_1763 g_311.f0 g_131.f1 g_1835 g_662 g_1566.f1 g_603.f1 g_67 g_1919 g_1121 g_603.f0 g_1570.f0 g_201 g_208 g_1444 g_1633 g_1354 g_1578.f1 g_1419 g_131.f0 g_196 g_258 g_1726.f3 g_661 g_130 g_1767 g_69.f3 g_418 g_858
 */
static uint64_t  func_26(uint32_t  p_27, const float  p_28)
{ /* block id: 15 */
    int16_t l_1843 = (-8L);
    int32_t l_1844[4][9][2] = {{{0x2862CB6AL,0xB4664668L},{8L,0x4765F6FAL},{0x56F987B1L,0x92B0322DL},{(-1L),(-1L)},{(-1L),8L},{(-9L),0x1A5E41B0L},{0x92B0322DL,(-5L)},{0x2862CB6AL,(-9L)},{(-1L),0xF9C54AF5L}},{{0x289AD4C1L,0x92B0322DL},{(-5L),0xB4664668L},{(-9L),(-5L)},{(-9L),0x58903422L},{0xF9C54AF5L,(-1L)},{(-1L),(-9L)},{(-5L),0x4765F6FAL},{0x58903422L,0x4765F6FAL},{(-5L),(-9L)}},{{(-1L),(-1L)},{0xF9C54AF5L,0x58903422L},{(-9L),(-5L)},{(-9L),0xB4664668L},{(-5L),0x92B0322DL},{0x289AD4C1L,0xF9C54AF5L},{(-1L),(-9L)},{0x2862CB6AL,(-5L)},{0x92B0322DL,0x1A5E41B0L}},{{(-9L),8L},{(-1L),(-1L)},{(-1L),0x92B0322DL},{0x56F987B1L,0x4765F6FAL},{8L,0xB4664668L},{0x2862CB6AL,8L},{0xF9C54AF5L,0x289AD4C1L},{0xF9C54AF5L,8L},{0x2862CB6AL,0xB4664668L}}};
    uint32_t l_1954 = 0x2E166537L;
    struct S0 ***l_1955[6][5];
    struct S1 **l_1971 = &g_858;
    struct S1 ***l_1970 = &l_1971;
    int8_t l_2002 = 0x75L;
    int8_t l_2076 = 1L;
    int64_t l_2077[2][5];
    uint16_t l_2080[9][2] = {{0x64C1L,0x7E61L},{65531UL,0x7E61L},{0x64C1L,0xFF29L},{0xFF29L,0x64C1L},{0x7E61L,65531UL},{0x7E61L,0x64C1L},{0xFF29L,0xFF29L},{0x64C1L,0x7E61L},{65531UL,0x7E61L}};
    float l_2111 = 0xF.7C8865p-81;
    int32_t l_2187[10][9][1] = {{{0x82A90EA5L},{0x9CC92883L},{0x1B08589BL},{0L},{0x35FCD1CFL},{0xBE360201L},{0xD7E4EE07L},{0xE9EF2921L},{7L}},{{0x502FCADCL},{1L},{0x8E0BB735L},{1L},{0x502FCADCL},{7L},{0xE9EF2921L},{0xD7E4EE07L},{0xBE360201L}},{{0x35FCD1CFL},{0L},{0x1B08589BL},{0x9CC92883L},{0x82A90EA5L},{0x9CC92883L},{0x1B08589BL},{0L},{0x35FCD1CFL}},{{0xBE360201L},{0xD7E4EE07L},{0xE9EF2921L},{7L},{0x502FCADCL},{1L},{0x8E0BB735L},{1L},{0x502FCADCL}},{{7L},{0xE9EF2921L},{0xD7E4EE07L},{0xBE360201L},{0x35FCD1CFL},{0L},{0x1B08589BL},{0x9CC92883L},{0x82A90EA5L}},{{0x9CC92883L},{0x1B08589BL},{0L},{0x35FCD1CFL},{0xBE360201L},{0xD7E4EE07L},{0xE9EF2921L},{7L},{0x502FCADCL}},{{1L},{0x8E0BB735L},{1L},{0x502FCADCL},{7L},{0xE9EF2921L},{0xD7E4EE07L},{0xBE360201L},{0x35FCD1CFL}},{{0L},{0x1B08589BL},{0x9CC92883L},{0x82A90EA5L},{0x9CC92883L},{0x1B08589BL},{0L},{0x35FCD1CFL},{0x03ADAEB8L}},{{1L},{0x9CC92883L},{0x35FCD1CFL},{0x8E0BB735L},{0x8C7EFF1CL},{0x96F2B95CL},{0x8C7EFF1CL},{0x8E0BB735L},{0x35FCD1CFL}},{{0x9CC92883L},{1L},{0x03ADAEB8L},{1L},{0xE9EF2921L},{0x82A90EA5L},{0xCCD800B7L},{0x6389461AL},{0xCCD800B7L}}};
    int64_t ****l_2214 = &g_1420;
    int16_t *l_2271 = (void*)0;
    int16_t **l_2270[3][10][4] = {{{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271}},{{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271}},{{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271},{&l_2271,&l_2271,&l_2271,&l_2271}}};
    int16_t ***l_2269 = &l_2270[2][2][3];
    uint32_t l_2317 = 0xF5646FBCL;
    int i, j, k;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
            l_1955[i][j] = &g_1633;
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
            l_2077[i][j] = 8L;
    }
    if ((g_2 >= (func_36(&g_14) , 0x9DC9A9B7L)))
    { /* block id: 789 */
        uint64_t l_1845 = 18446744073709551612UL;
        int32_t l_1848 = 0L;
        int32_t l_1915 = (-1L);
        uint32_t l_1948 = 4294967295UL;
        if (l_1843)
        { /* block id: 790 */
            ++l_1845;
        }
        else
        { /* block id: 792 */
            int32_t *l_1849 = &g_603.f1;
            int32_t *l_1850 = (void*)0;
            int32_t *l_1851 = &g_1566.f1;
            int32_t *l_1852 = (void*)0;
            int32_t *l_1854 = (void*)0;
            int32_t *l_1855[10] = {&g_1568.f1,(void*)0,&g_1568.f1,(void*)0,&g_1568.f1,(void*)0,&g_1568.f1,(void*)0,&g_1568.f1,(void*)0};
            uint32_t l_1891 = 1UL;
            int i;
            l_1848 &= 0xB9A1F0F7L;
            l_1848 |= p_27;
lbl_1957:
            (*g_661) = g_1856;
            for (g_23 = 0; (g_23 < 2); g_23 = safe_add_func_uint64_t_u_u(g_23, 6))
            { /* block id: 798 */
                uint16_t l_1860 = 65532UL;
                int64_t l_1888 = (-1L);
                uint64_t *l_1889 = (void*)0;
                uint64_t *l_1890 = &g_67[1];
                int8_t *l_1892[6] = {&g_1118,&g_1118,&g_1118,&g_1118,&g_1118,&g_1118};
                int32_t l_1893[7][6][6] = {{{1L,1L,0x34BC37E3L,0x3FF8F651L,0x9D225B36L,(-1L)},{0x9D225B36L,(-1L),0x40B0A10EL,0x65A9C2CCL,(-1L),0x34BC37E3L},{1L,1L,0x40B0A10EL,1L,1L,(-1L)},{6L,0L,0x34BC37E3L,0x1581F227L,(-1L),(-8L)},{0x1581F227L,(-1L),(-8L),0L,1L,(-8L)},{1L,1L,0x34BC37E3L,0xE9D9F4B5L,0L,0x9D225B36L}},{{3L,0x5B8C55B0L,(-1L),0xD6EE84BAL,0x8D3315ECL,0x1581F227L},{(-1L),0x5FA17C3AL,(-1L),(-1L),7L,0x9D225B36L},{0x0D61B06AL,3L,0x1581F227L,8L,0x5B8C55B0L,0x3FF8F651L},{8L,0x5B8C55B0L,0x3FF8F651L,0x5FA17C3AL,0x5FA17C3AL,0x3FF8F651L},{(-1L),(-1L),0x1581F227L,(-8L),3L,0x9D225B36L},{(-1L),0x266DC0B5L,(-1L),0xB3F26CDBL,0x5B8C55B0L,0x1581F227L}},{{7L,(-1L),(-1L),0x5FA17C3AL,(-1L),0x9D225B36L},{1L,0x5FA17C3AL,0x1581F227L,0x0D61B06AL,0x266DC0B5L,0x3FF8F651L},{0x0D61B06AL,0x266DC0B5L,0x3FF8F651L,3L,(-1L),0x3FF8F651L},{7L,0xD4C88900L,0x1581F227L,0xD6EE84BAL,0x5FA17C3AL,0x9D225B36L},{0x5FA17C3AL,0x8D3315ECL,(-1L),(-8L),0x266DC0B5L,0x1581F227L},{0xD4C88900L,3L,(-1L),3L,0xD4C88900L,0x9D225B36L}},{{8L,(-1L),0x1581F227L,1L,0x8D3315ECL,0x3FF8F651L},{1L,0x8D3315ECL,0x3FF8F651L,(-1L),3L,0x3FF8F651L},{0xD4C88900L,7L,0x1581F227L,0xB3F26CDBL,(-1L),0x9D225B36L},{3L,0x5B8C55B0L,(-1L),0xD6EE84BAL,0x8D3315ECL,0x1581F227L},{(-1L),0x5FA17C3AL,(-1L),(-1L),7L,0x9D225B36L},{0x0D61B06AL,3L,0x1581F227L,8L,0x5B8C55B0L,0x3FF8F651L}},{{8L,0x5B8C55B0L,0x3FF8F651L,0x5FA17C3AL,0x5FA17C3AL,0x3FF8F651L},{(-1L),(-1L),0x1581F227L,(-8L),3L,0x9D225B36L},{(-1L),0x266DC0B5L,(-1L),0xB3F26CDBL,0x5B8C55B0L,0x1581F227L},{7L,(-1L),(-1L),0x5FA17C3AL,(-1L),0x9D225B36L},{1L,0x5FA17C3AL,0x1581F227L,0x0D61B06AL,0x266DC0B5L,0x3FF8F651L},{0x0D61B06AL,0x266DC0B5L,0x3FF8F651L,3L,(-1L),0x3FF8F651L}},{{7L,0xD4C88900L,0x1581F227L,0xD6EE84BAL,0x5FA17C3AL,0x9D225B36L},{0x5FA17C3AL,0x8D3315ECL,(-1L),(-8L),0x266DC0B5L,0x1581F227L},{0xD4C88900L,3L,(-1L),3L,0xD4C88900L,0x9D225B36L},{8L,(-1L),0x1581F227L,1L,0x8D3315ECL,0x3FF8F651L},{1L,0x8D3315ECL,0x3FF8F651L,(-1L),3L,0x3FF8F651L},{0xD4C88900L,7L,0x1581F227L,0xB3F26CDBL,(-1L),0x9D225B36L}},{{3L,0x5B8C55B0L,(-1L),0xD6EE84BAL,0x8D3315ECL,0x1581F227L},{(-1L),0x5FA17C3AL,(-1L),(-1L),0x882762F8L,0x5FA17C3AL},{0x5BBD477DL,0x2DED9CB3L,1L,0x544584C6L,0x34BC37E3L,0xD6EE84BAL},{0x544584C6L,0x34BC37E3L,0xD6EE84BAL,0xFC9A5E3CL,0xFC9A5E3CL,0xD6EE84BAL},{0xFFB59BEDL,0xFFB59BEDL,1L,(-1L),0x2DED9CB3L,0x5FA17C3AL},{0x40B0A10EL,0xB5DBBC59L,0x8D3315ECL,7L,0x34BC37E3L,1L}}};
                int i, j, k;
                g_662.f2 &= (g_1859 , l_1844[2][3][1]);
                (*g_258) = l_1860;
                if (l_1844[3][8][0])
                    break;
                if (((l_1845 && ((safe_mod_func_int8_t_s_s((l_1893[5][1][1] = ((safe_mul_func_int16_t_s_s((p_27 >= (safe_div_func_int64_t_s_s((safe_mod_func_uint8_t_u_u((safe_mod_func_int16_t_s_s(p_27, 1L)), ((*g_107) = (((l_1891 = (((*l_1890) = ((safe_add_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u((((0xAAD8L >= (safe_sub_func_int32_t_s_s(((*l_1849) = ((*l_1851) = (safe_unary_minus_func_int32_t_s((safe_rshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s(((safe_mod_func_int32_t_s_s(0x489C2990L, l_1848)) || l_1860), (((((*g_472) = p_28) < p_28) > l_1860) , p_27))), 9)))))), p_27))) && l_1844[2][3][1]) && g_1728[0][3].f0), p_27)) ^ l_1888), (-5L))) < l_1888)) | p_27)) && l_1888) , (*g_107))))), 18446744073709551615UL))), (-9L))) < g_369[1].f2)), p_27)) <= 0UL)) , (-7L)))
                { /* block id: 809 */
                    int8_t l_1902[7][4][3] = {{{0x48L,(-3L),(-1L)},{0x48L,0x48L,(-3L)},{0xB2L,(-3L),(-3L)},{(-3L),0x3AL,(-1L)}},{{0xB2L,0x3AL,0xB2L},{0x48L,(-3L),(-1L)},{0x48L,0x48L,(-3L)},{0xB2L,(-3L),(-3L)}},{{(-3L),0x3AL,(-1L)},{0xB2L,0x3AL,0xB2L},{0x48L,(-3L),(-1L)},{0x48L,0x48L,(-3L)}},{{0xB2L,(-3L),(-3L)},{(-3L),0x3AL,(-1L)},{0xB2L,0x3AL,0xB2L},{0x48L,(-3L),(-1L)}},{{0x48L,0x48L,(-3L)},{0xB2L,(-3L),(-3L)},{(-3L),0x3AL,(-1L)},{0xB2L,0x3AL,0xB2L}},{{0x48L,(-3L),(-1L)},{0x48L,0x48L,(-3L)},{0xB2L,(-3L),(-3L)},{(-3L),0x3AL,(-1L)}},{{0xB2L,0x3AL,0xB2L},{0x48L,(-3L),(-1L)},{0x48L,0x48L,(-3L)},{0xB2L,(-3L),(-3L)}}};
                    uint64_t *l_1911 = &l_1845;
                    int i, j, k;
                    l_1893[1][2][2] ^= ((safe_rshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s((g_309[0] , (safe_rshift_func_int8_t_s_s(p_27, l_1902[3][2][0]))), (safe_mod_func_int16_t_s_s((safe_add_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(((*l_1911) = (++(*l_1890))), p_27)), (safe_add_func_uint8_t_u_u(((~(l_1848 ^= g_1445.f2)) & p_27), l_1915)))), (safe_lshift_func_uint8_t_u_u((p_27 >= (((p_27 == p_27) <= g_1365[4]) ^ g_1365[4])), l_1844[3][5][0])))))), g_1167)) != 0UL);
                }
                else
                { /* block id: 814 */
                    uint32_t **l_1920 = (void*)0;
                    uint32_t *l_1922 = &g_1406[5][1];
                    uint32_t **l_1921 = &l_1922;
                    int32_t l_1928 = 7L;
                    const uint32_t l_1929 = 0x788EEE2FL;
                    uint16_t *l_1938 = &g_603.f0;
                    uint16_t *l_1941 = &g_1570.f0;
                    int32_t ****l_1951 = (void*)0;
                    (*g_258) ^= (((+((g_1919 = g_1919) == ((*l_1921) = &g_1121))) , (((((g_1923 , (safe_lshift_func_int16_t_s_s((safe_add_func_uint32_t_u_u(((*l_1922) = (l_1928 != (p_27 & (0x03L == l_1929)))), ((((safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((safe_div_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((++(*l_1938)), ((*l_1941)--))) < (safe_sub_func_uint8_t_u_u(p_27, ((((+p_27) <= l_1848) >= l_1844[2][3][1]) < 0x3DL)))), l_1844[2][3][1])), l_1845)), p_27)) < p_27) != 0x59E3L) >= (-1L)))), g_210))) , g_1947) , p_27) < 0x11611328L) , g_662.f3)) , l_1915);
                    if (l_1888)
                        continue;
                    for (l_1891 = 1; (l_1891 <= 4); l_1891 += 1)
                    { /* block id: 824 */
                        int32_t l_1956[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
                        int i;
                        l_1928 ^= (l_1948 ^ ((((safe_sub_func_float_f_f(p_27, p_28)) , (void*)0) == (l_1951 = &g_1086)) >= ((safe_sub_func_int64_t_s_s(((***g_1420) = ((***g_1420) , (((l_1956[0] = ((&g_1633 == (((p_27 <= 0x75L) && l_1954) , l_1955[4][4])) == 8L)) > (*g_1919)) <= p_27))), l_1844[2][3][1])) <= 0x4242L)));
                        if (g_659.f1)
                            goto lbl_1957;
                    }
                    if (l_1843)
                        continue;
                }
            }
        }
    }
    else
    { /* block id: 835 */
        struct S1 **l_1969 = &g_858;
        struct S1 *** const l_1968 = &l_1969;
        const uint32_t *l_1978[5][1][8] = {{{&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0]}},{{&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0]}},{{&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0]}},{{&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0]}},{{&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0],&g_343[0][0]}}};
        const uint32_t **l_1977 = &l_1978[0][0][4];
        int32_t l_1982[9];
        int8_t l_2000 = 3L;
        int64_t *****l_2019[2][3] = {{&g_1419[1][2][5],&g_1419[1][2][5],&g_1419[1][2][5]},{&g_1419[1][3][4],&g_1419[1][3][4],&g_1419[1][3][4]}};
        uint8_t l_2083 = 0xD3L;
        uint64_t l_2145 = 0x436CA416C3C70823LL;
        int32_t l_2220[9][3][9] = {{{0x63954D5FL,0xB198E379L,0xE93A04AEL,(-5L),(-1L),(-1L),0xBFAC8515L,(-10L),0L},{1L,1L,(-2L),0x63954D5FL,1L,0x91D2F607L,0xBFAC8515L,3L,(-6L)},{1L,1L,1L,0xDC94C7FFL,0x30E077A6L,0xCF2A5514L,0x2337D49DL,0xC5FDCEA7L,(-1L)}},{{(-1L),(-1L),(-4L),0L,(-1L),0xC6D58229L,(-1L),5L,1L},{1L,(-4L),0xB62B08E8L,0x191E469AL,2L,0xC6D58229L,1L,0x1E7DC7FBL,0x8478F998L},{0L,0xB62B08E8L,1L,0x40D7904BL,1L,3L,1L,5L,1L}},{{0L,0xA3C07A19L,(-4L),0xE93A04AEL,0x560965C9L,(-1L),0x40D7904BL,0x7BADCB35L,0x343EA5B9L},{1L,0x92AA5DB0L,1L,0xB198E379L,0x560965C9L,1L,0xB198E379L,(-4L),7L},{(-1L),(-1L),0xB62B08E8L,1L,1L,0x1E7DC7FBL,0x40D7904BL,7L,7L}},{{1L,1L,(-4L),1L,2L,6L,1L,(-1L),0x343EA5B9L},{1L,(-1L),(-1L),1L,(-1L),(-4L),1L,6L,1L},{0x40D7904BL,0x92AA5DB0L,9L,1L,0x8478F998L,6L,(-1L),6L,0x8478F998L}},{{0xB198E379L,0xA3C07A19L,0xA3C07A19L,0xB198E379L,0xF062BB8DL,0x1E7DC7FBL,1L,(-1L),1L},{0x40D7904BL,0xB62B08E8L,0xA3C07A19L,0xE93A04AEL,0x6A249700L,1L,0L,7L,(-1L)},{1L,(-4L),9L,0x40D7904BL,0xF062BB8DL,(-1L),0L,(-4L),0x90AD4CC4L}},{{1L,(-1L),(-1L),0x191E469AL,0x8478F998L,3L,1L,0x7BADCB35L,(-1L)},{(-1L),(-1L),(-4L),0L,(-1L),0xC6D58229L,(-1L),5L,1L},{1L,(-4L),0xB62B08E8L,0x191E469AL,2L,0xC6D58229L,1L,0x1E7DC7FBL,0x8478F998L}},{{0L,0xB62B08E8L,1L,0x40D7904BL,1L,3L,1L,5L,1L},{0L,0xA3C07A19L,(-4L),0xE93A04AEL,0x560965C9L,(-1L),0x40D7904BL,0x7BADCB35L,0x343EA5B9L},{1L,0x92AA5DB0L,1L,0xB198E379L,0x560965C9L,1L,0xB198E379L,(-4L),7L}},{{(-1L),(-1L),0xB62B08E8L,1L,1L,0x1E7DC7FBL,0x40D7904BL,7L,7L},{1L,1L,(-4L),1L,2L,6L,1L,(-1L),0x343EA5B9L},{1L,(-1L),(-1L),1L,(-1L),(-4L),1L,6L,1L}},{{0x40D7904BL,0L,(-8L),(-4L),1L,0L,0xD921BE43L,0L,1L},{0xB62B08E8L,0x3A2D34A3L,0x3A2D34A3L,0xB62B08E8L,0x63954D5FL,(-3L),(-1L),(-10L),0x2337D49DL},{0x92AA5DB0L,0x81C4A1D3L,0x3A2D34A3L,0xA3C07A19L,0xB82BFC0DL,(-1L),1L,(-8L),1L}}};
        uint32_t l_2241 = 18446744073709551615UL;
        struct S0 **l_2252 = &g_661;
        uint16_t l_2321 = 0UL;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1982[i] = (-1L);
        for (l_1954 = (-9); (l_1954 == 27); l_1954++)
        { /* block id: 838 */
            union U3 *l_1972 = (void*)0;
            union U3 **l_1973 = &g_1444[7];
            const union U3 *l_1974 = &g_1975[0];
            const union U3 **l_1976 = &l_1974;
            const int64_t * const l_1980 = &g_1119;
            const int64_t * const *l_1979 = &l_1980;
            int32_t l_1981 = 0x72F0A6BDL;
            int32_t l_1990 = 0L;
            int32_t l_1991 = 0x07EF9BFFL;
            int32_t l_1993 = 1L;
            int32_t l_1994 = 0x2562EBC3L;
            int32_t l_1998[8][9][3] = {{{0L,0x1A341E69L,0x1F69C7B1L},{(-1L),0x126F6DA5L,0x8D62462AL},{(-1L),3L,0xA694A964L},{0x74B0AE1FL,1L,(-1L)},{0x69C61CA7L,0x1C180882L,0L},{0x92BE8FCCL,1L,1L},{0xE6455A98L,1L,0xF0280DC2L},{6L,0L,4L},{6L,3L,0x92BE8FCCL}},{{0xE6455A98L,0x1CA1C370L,1L},{0x92BE8FCCL,0xEC611406L,(-9L)},{0x69C61CA7L,0x92BE8FCCL,0x921B5A47L},{0x74B0AE1FL,(-9L),1L},{(-1L),0x771185E4L,0x1A341E69L},{(-1L),1L,0L},{0L,0x65C1D85CL,0x7C0F98ECL},{1L,4L,1L},{0x771185E4L,0xA0186F7CL,1L}},{{0xBA2A1264L,0x02D8ABD4L,6L},{1L,0xE6455A98L,0xA0186F7CL},{0xCB0EAAD9L,0xCB0EAAD9L,(-1L)},{0x65C1D85CL,0L,0x94F4939DL},{(-7L),0x371D9EAAL,0x58676942L},{0xDEAD9690L,0L,1L},{0xC1C45537L,(-7L),0x58676942L},{0x74F89117L,0x7B7CE49FL,0x94F4939DL},{0x7C3A12B8L,1L,(-1L)}},{{3L,0L,0xA0186F7CL},{0xF15BF7FFL,(-1L),6L},{1L,(-1L),1L},{0L,1L,1L},{0xE3A20DCCL,(-1L),0x7C0F98ECL},{3L,0x88EFAEC1L,0L},{(-1L),0x69C61CA7L,0x1A341E69L},{9L,6L,1L},{1L,0xA632A3A8L,0x921B5A47L}},{{1L,0xA0110B83L,(-9L)},{0x126F6DA5L,0x58676942L,1L},{3L,0x18F6F1F8L,0x92BE8FCCL},{0x921B5A47L,0xE3A20DCCL,4L},{0x16F85D7CL,0xE3A20DCCL,0xF0280DC2L},{1L,0x18F6F1F8L,1L},{0x88EFAEC1L,0x58676942L,0L},{1L,0xA0110B83L,(-1L)},{0x8D62462AL,0xA632A3A8L,0xA694A964L}},{{0L,6L,0x8D62462AL},{0x1CA1C370L,0x69C61CA7L,0x1F69C7B1L},{1L,0x88EFAEC1L,0x1C180882L},{0xA0186F7CL,(-1L),3L},{0x58676942L,1L,0x18F6F1F8L},{1L,(-1L),1L},{0L,(-1L),(-1L)},{0xFB2221A7L,0L,0xFB2221A7L},{2L,1L,0xE6455A98L}},{{0xA694A964L,0x7B7CE49FL,(-7L)},{0x1A341E69L,0x1C180882L,0xA0110B83L},{1L,0xFB33568CL,(-9L)},{0x1A341E69L,1L,(-1L)},{0xC1C45537L,0x8D62462AL,0x16F85D7CL},{0x7B7CE49FL,0x69C61CA7L,1L},{0x771185E4L,0xAC5247A1L,1L},{0xE6455A98L,0x126F6DA5L,(-1L)},{0xBA2A1264L,1L,0xC84E1D97L}},{{0x88EFAEC1L,0xA632A3A8L,0x58676942L},{1L,1L,3L},{(-10L),0xA0186F7CL,1L},{1L,1L,(-10L)},{0xFB33568CL,6L,0L},{1L,0L,0xBA2A1264L},{0L,(-1L),(-10L)},{0x94F4939DL,1L,0x2FD9E1D5L},{1L,(-1L),0xFB33568CL}}};
            int32_t l_1999 = 0x4FA5274BL;
            int32_t l_2004 = 0xC37897E1L;
            uint32_t l_2005[8] = {0x57A6648CL,0x57A6648CL,0x57A6648CL,0x57A6648CL,0x57A6648CL,0x57A6648CL,0x57A6648CL,0x57A6648CL};
            int32_t l_2010 = (-5L);
            int64_t ***l_2018 = &g_1421;
            int64_t **** const l_2017 = &l_2018;
            int64_t **** const *l_2016 = &l_2017;
            struct S0 *l_2056 = (void*)0;
            int64_t l_2110 = (-3L);
            struct S1 *l_2115 = &g_1411;
            float l_2123[2][6][10] = {{{0x1.Ap+1,0xF.F86175p-91,(-0x1.Fp-1),0xB.571DCCp-33,0x6.DF2BF7p-22,0xB.571DCCp-33,(-0x1.Fp-1),0xF.F86175p-91,0x1.Ap+1,0xE.E3181Dp+22},{0x1.Ap+1,0x0.2p+1,0x6.5p+1,0x0.6F69CDp-33,0xB.571DCCp-33,0xE.E3181Dp+22,0xE.E3181Dp+22,0xB.571DCCp-33,0x0.6F69CDp-33,0x6.5p+1},{0xE.E3181Dp+22,0xE.E3181Dp+22,0xB.571DCCp-33,0x0.6F69CDp-33,0x6.5p+1,0x0.2p+1,0x1.Ap+1,0x1.Bp-1,0x1.Ap+1,0x0.2p+1},{(-0x1.Fp-1),0xB.571DCCp-33,0x6.DF2BF7p-22,0xB.571DCCp-33,(-0x1.Fp-1),0xF.F86175p-91,0x1.Ap+1,0xE.E3181Dp+22,0x3.FE09CAp+11,0x3.FE09CAp+11},{0x0.6F69CDp-33,0xE.E3181Dp+22,(-0x1.8p+1),0x0.2p+1,0x0.2p+1,(-0x1.8p+1),0xE.E3181Dp+22,0x0.6F69CDp-33,0x6.DF2BF7p-22,0x3.FE09CAp+11},{0x1.Bp-1,0x0.2p+1,0xE.E3181Dp+22,0x1.9p-1,(-0x1.Fp-1),0x1.Ap+1,(-0x1.Fp-1),0x1.9p-1,0xE.E3181Dp+22,0x0.2p+1}},{{(-0x1.8p+1),0xF.F86175p-91,0xE.E3181Dp+22,(-0x1.Fp-1),0x6.5p+1,0x1.9p-1,0x0.6F69CDp-33,0x0.6F69CDp-33,0x1.9p-1,0x6.5p+1},{0x3.FE09CAp+11,(-0x1.8p+1),(-0x1.8p+1),0x3.FE09CAp+11,0xB.571DCCp-33,0x1.9p-1,0x1.Bp-1,0xE.E3181Dp+22,0x6.5p+1,0xE.E3181Dp+22},{(-0x1.8p+1),0x1.Ap+1,0x6.DF2BF7p-22,0xE.E3181Dp+22,0x6.DF2BF7p-22,0x1.Ap+1,(-0x1.8p+1),0x1.Bp-1,0x6.5p+1,0xF.F86175p-91},{0x1.Bp-1,0x1.9p-1,0xB.571DCCp-33,0x3.FE09CAp+11,(-0x1.8p+1),(-0x1.8p+1),0x3.FE09CAp+11,0xB.571DCCp-33,0x1.9p-1,0x1.Bp-1},{0x0.6F69CDp-33,0x1.9p-1,0x6.5p+1,(-0x1.Fp-1),0xE.E3181Dp+22,0xF.F86175p-91,(-0x1.8p+1),0xF.F86175p-91,0xE.E3181Dp+22,(-0x1.Fp-1)},{(-0x1.Fp-1),0x1.Ap+1,(-0x1.Fp-1),0x1.9p-1,0xE.E3181Dp+22,0x0.2p+1,0x1.Bp-1,0x6.DF2BF7p-22,0x6.DF2BF7p-22,0x1.Bp-1}}};
            int32_t *l_2133 = &g_603.f1;
            int8_t l_2138 = 0x5DL;
            int i, j, k;
        }
        for (g_208 = 1; (g_208 >= 0); g_208 -= 1)
        { /* block id: 923 */
            int16_t l_2161 = 0x545FL;
            int32_t l_2162[5][5][4] = {{{0x3CBC93F3L,6L,2L,0x8655F593L},{(-1L),6L,9L,6L},{2L,5L,9L,5L},{2L,6L,0x3CBC93F3L,5L},{9L,2L,9L,0x8655F593L}},{{9L,0x8655F593L,0x3CBC93F3L,0L},{2L,0x8655F593L,0L,0x8655F593L},{0x3CBC93F3L,2L,0L,5L},{2L,6L,0x3CBC93F3L,5L},{9L,2L,9L,0x8655F593L}},{{9L,0x8655F593L,0x3CBC93F3L,0L},{2L,0x8655F593L,0L,0x8655F593L},{0x3CBC93F3L,2L,0L,5L},{2L,6L,0x3CBC93F3L,5L},{9L,2L,9L,0x8655F593L}},{{9L,0x8655F593L,0x3CBC93F3L,0L},{2L,0x8655F593L,0L,0x8655F593L},{0x3CBC93F3L,2L,0L,5L},{2L,6L,0x3CBC93F3L,5L},{9L,2L,9L,0x8655F593L}},{{9L,0x8655F593L,0x3CBC93F3L,0L},{2L,0x8655F593L,0L,0x8655F593L},{0x3CBC93F3L,2L,0L,5L},{2L,6L,0x3CBC93F3L,5L},{9L,2L,9L,0x8655F593L}}};
            uint64_t l_2170[9][8];
            int i, j, k;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 8; j++)
                    l_2170[i][j] = 18446744073709551615UL;
            }
            (*g_2151) = &g_1975[0];
            for (g_1566.f1 = 0; (g_1566.f1 <= 1); g_1566.f1 += 1)
            { /* block id: 927 */
                struct S0 **l_2160 = &g_661;
                int32_t l_2167 = (-1L);
                int32_t l_2168 = 3L;
                int32_t l_2169[5][7][7] = {{{1L,(-7L),(-2L),9L,0x990CDDB7L,7L,(-5L)},{0x6D4BC24DL,(-5L),7L,7L,0x4E59E514L,0xF34E38FFL,0xC86A928FL},{0x25855054L,(-5L),0x36776AD8L,(-1L),1L,0xE574564DL,0L},{0xAB5033B4L,(-7L),0xF34E38FFL,0x337B1FEBL,0xE85CC9F4L,0xE542DFAFL,0xC86A928FL},{2L,(-1L),0xE542DFAFL,0x21817AF2L,0xE85CC9F4L,0xC521B721L,(-5L)},{0xEB042FC0L,0x53DB8861L,7L,0xE85CC9F4L,1L,0x9A234690L,0x4068A454L},{(-3L),0xCF9F316CL,0xE542DFAFL,(-8L),0x4E59E514L,0xCB0F92CBL,0x53DB8861L}},{{(-3L),0L,0xF34E38FFL,0x6643C028L,0x990CDDB7L,0x5C63E55DL,0xFE51F680L},{0xEB042FC0L,4L,0x36776AD8L,0xB4AC5691L,(-1L),0xCB0F92CBL,0x1A544BD6L},{2L,0x4068A454L,7L,0xB4AC5691L,(-1L),0x9A234690L,1L},{0xAB5033B4L,0xFE51F680L,(-2L),0x6643C028L,0x21817AF2L,0xC521B721L,2L},{0x25855054L,0x4068A454L,0x5C63E55DL,(-8L),0xB4AC5691L,0xE542DFAFL,2L},{0x6D4BC24DL,4L,(-1L),0xE85CC9F4L,0x780B25C1L,0xE574564DL,1L},{1L,0L,1L,0x21817AF2L,0xB4AC5691L,0xF34E38FFL,0x1A544BD6L}},{{0xC2685EE6L,0xCF9F316CL,1L,0x337B1FEBL,0x25855054L,0xB4AC5691L,0xC63199ADL},{0xE8335386L,(-9L),(-1L),1L,1L,(-1L),(-9L)},{0x9950B958L,0xBF14CF43L,0x6643C028L,0xC2685EE6L,1L,(-1L),(-3L)},{0xF2BDD2F3L,3L,(-1L),0x2F166AACL,0xBA2C0B40L,0xB4AC5691L,1L},{0xA290A376L,1L,0x780B25C1L,0xC2685EE6L,1L,9L,(-1L)},{6L,1L,0x3BE7BC2DL,1L,0xF4A2C32CL,0L,0x83EB546BL},{(-1L),3L,9L,0x6D4BC24DL,0xAB5033B4L,0x4E59E514L,(-1L)}},{{0x0EC34582L,0xBF14CF43L,0x4E59E514L,0x25855054L,0xAB5033B4L,1L,1L},{0x7E54A7ABL,(-9L),0xB4AC5691L,0xAB5033B4L,0xF4A2C32CL,(-8L),(-3L)},{1L,0xCEB5C9D2L,0x4E59E514L,2L,1L,0x21817AF2L,(-9L)},{1L,0x83EB546BL,9L,0xEB042FC0L,0xBA2C0B40L,0x6643C028L,0xC63199ADL},{0x7E54A7ABL,0L,0x3BE7BC2DL,(-3L),1L,0x21817AF2L,0L},{0x0EC34582L,(-3L),0x780B25C1L,(-3L),1L,(-8L),(-2L)},{(-1L),0xC63199ADL,(-1L),0xEB042FC0L,0x25855054L,1L,(-1L)}},{{6L,(-3L),0x6643C028L,2L,(-3L),0x4E59E514L,(-1L)},{0xA290A376L,0L,(-1L),0xAB5033B4L,0xC7049582L,0L,(-2L)},{0xF2BDD2F3L,0x83EB546BL,0x337B1FEBL,0x25855054L,(-3L),9L,0L},{0x9950B958L,0xCEB5C9D2L,0x337B1FEBL,0x6D4BC24DL,0x25855054L,0xB4AC5691L,0xC63199ADL},{0xE8335386L,(-9L),(-1L),1L,1L,(-1L),(-9L)},{0x9950B958L,0xBF14CF43L,0x6643C028L,0xC2685EE6L,1L,(-1L),(-3L)},{0xF2BDD2F3L,3L,(-1L),0x2F166AACL,0xBA2C0B40L,0xB4AC5691L,1L}}};
                int i, j, k;
                for (l_2145 = 0; (l_2145 <= 1); l_2145 += 1)
                { /* block id: 930 */
                    int32_t ****l_2155 = (void*)0;
                    int32_t *****l_2154 = &l_2155;
                    int32_t *l_2163 = &g_1578.f1;
                    int32_t *l_2164 = (void*)0;
                    int32_t *l_2165 = &g_131.f1;
                    int32_t *l_2166[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j;
                    for (g_23 = 1; (g_23 >= 0); g_23 -= 1)
                    { /* block id: 933 */
                        int i, j;
                        return l_2080[(l_2145 + 6)][l_2145];
                    }
                    l_2161 = ((l_2080[(l_2145 + 3)][g_1566.f1] != 0xA53A0B9DL) & (safe_div_func_int32_t_s_s(((1L >= ((((*l_2154) = &g_1086) == (void*)0) != (safe_mul_func_int16_t_s_s(((g_1568.f2 & ((safe_div_func_int32_t_s_s((((*g_1632) = l_2160) == (void*)0), p_27)) , g_176.f0)) | g_1018.f0), p_27)))) != 0xB592L), 0x9D00EB1AL)));
                    l_2170[6][4]++;
                }
                l_2162[3][1][0] &= l_2080[(g_208 + 1)][g_208];
                for (g_1354 = (-7); (g_1354 >= (-20)); g_1354--)
                { /* block id: 944 */
                    return p_27;
                }
                for (l_1954 = (-12); (l_1954 >= 56); l_1954 = safe_add_func_int8_t_s_s(l_1954, 4))
                { /* block id: 949 */
                    uint16_t *l_2188 = &g_1570.f0;
                    int8_t *l_2189 = &g_1118;
                    int32_t l_2190[4] = {7L,7L,7L,7L};
                    int32_t l_2204 = (-4L);
                    int i;
                    if ((safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((*l_2189) = (safe_div_func_uint32_t_u_u((*g_1919), ((((&g_1444[2] == &g_1444[2]) <= ((*l_2188) = (l_2187[2][6][0] = (safe_mul_func_int8_t_s_s(l_2170[6][4], ((safe_lshift_func_uint16_t_u_s((p_27 | (g_108[2] > (l_1844[3][4][0] &= p_27))), 1)) == (l_2168 < l_2145))))))) <= l_2169[2][2][0]) | g_1566.f1)))), l_2169[2][2][0])), p_27)))
                    { /* block id: 954 */
                        if (l_2190[2])
                            break;
                        if (p_27)
                            continue;
                    }
                    else
                    { /* block id: 957 */
                        (*g_661) = g_2191;
                        if (p_27)
                            break;
                    }
                    (*g_258) = ((l_2190[2] ^= p_27) <= (safe_sub_func_uint8_t_u_u(((safe_div_func_uint64_t_u_u((safe_div_func_int32_t_s_s(l_2167, (p_27 & (safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(1L, p_27)), 2)), l_2002))))), g_1446.f0)) ^ (l_2204 < (9L && 0x35D05B6DEBEF6734LL))), (*g_107))));
                }
            }
        }
        for (g_1401 = 0; (g_1401 < (-23)); --g_1401)
        { /* block id: 968 */
            int32_t **** const *l_2207 = (void*)0;
            int32_t l_2208 = 0x6B47CFF7L;
            union U4 **l_2216[3][1];
            int32_t l_2227 = 1L;
            int32_t l_2232 = 0xA286738EL;
            int32_t l_2234 = 8L;
            int32_t l_2235[1];
            int64_t *** const *l_2247 = &g_1420;
            int16_t *l_2268 = (void*)0;
            int16_t **l_2267 = &l_2268;
            int16_t ***l_2266 = &l_2267;
            float *l_2275 = &g_1550;
            float *l_2276[2];
            uint16_t ** const l_2282 = (void*)0;
            uint32_t l_2314 = 0x572BC143L;
            uint16_t l_2332 = 0xD8E4L;
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_2216[i][j] = (void*)0;
            }
            for (i = 0; i < 1; i++)
                l_2235[i] = (-1L);
            for (i = 0; i < 2; i++)
                l_2276[i] = &g_231[2];
            l_2208 &= ((l_2207 = l_2207) == &g_1835);
            for (g_1578.f1 = 0; (g_1578.f1 <= 4); g_1578.f1 += 1)
            { /* block id: 973 */
                union U4 **l_2215 = &g_130;
                int32_t l_2228 = 0x09DC3582L;
                int32_t l_2229 = 7L;
                int32_t l_2230 = 0x2E92095FL;
                int32_t l_2231 = 5L;
                int32_t l_2233 = 0L;
                int32_t l_2236 = 0x8495391DL;
                int32_t l_2238 = (-1L);
                int32_t l_2240 = 0x0B6B2795L;
                uint32_t l_2256 = 0xAE46744EL;
                int16_t **l_2265[1];
                int16_t ***l_2264[3];
                struct S0 *l_2306 = &g_1524[1][0][0];
                union U4 *l_2307 = &g_2209;
                int i;
                for (i = 0; i < 1; i++)
                    l_2265[i] = (void*)0;
                for (i = 0; i < 3; i++)
                    l_2264[i] = &l_2265[0];
                if ((l_2187[2][6][0] = (g_2209 , (safe_sub_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((((g_1419[0][2][2] = l_2214) == l_2214) != (-1L)), (l_2215 == l_2216[2][0]))) < g_196[g_1578.f1]), (g_2125[g_1578.f1] >= ((void*)0 != (*l_2214))))))))
                { /* block id: 976 */
                    uint32_t l_2219 = 8UL;
                    int32_t l_2221 = 0x7A5F13FBL;
                    int32_t l_2222 = 0L;
                    int32_t l_2226[3][5] = {{0L,0x5D4BA045L,1L,1L,0x5D4BA045L},{0L,0x5D4BA045L,1L,1L,0x5D4BA045L},{0L,0x5D4BA045L,1L,1L,0x5D4BA045L}};
                    int16_t l_2237 = 0x2D5BL;
                    float **l_2274[7][10] = {{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549},{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549},{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549},{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549},{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549},{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549},{&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549,&g_472,&g_1549}};
                    int32_t *l_2277 = &l_2226[0][3];
                    int i, j;
                    if (p_27)
                        break;
                    l_2219 = (p_28 <= (*g_472));
                    for (g_131.f0 = 0; (g_131.f0 <= 8); g_131.f0 += 1)
                    { /* block id: 981 */
                        int32_t *l_2223 = &g_1967[1].f1;
                        int32_t *l_2224 = (void*)0;
                        int32_t *l_2225[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        float *l_2255 = &g_227;
                        int8_t *l_2257[8] = {&l_2076,&l_2076,&l_2076,&l_2076,&l_2076,&l_2076,&l_2076,&l_2076};
                        int i;
                        ++l_2241;
                        l_2228 ^= (l_2187[2][6][0] , (((g_2244 , (g_1354 ^= (((*g_1919)--) >= ((((((l_2247 == (void*)0) , ((safe_mod_func_int64_t_s_s((((*g_472) >= (safe_mul_func_float_f_f((((void*)0 != l_2252) , (((*l_2255) = (p_28 < ((safe_add_func_uint8_t_u_u((g_196[g_1578.f1] = 254UL), g_1728[0][3].f2)) , (*g_472)))) != (*g_472))), (*g_472)))) , 0x407A2987C993096BLL), (**g_1421))) == l_2240)) != p_27) == l_2256) & 0x92F3424CL) , (**g_1766))))) , (**g_1766)) ^ (-1L)));
                        l_2226[0][3] &= (&l_2225[5] != (void*)0);
                        (*g_2258) = &l_2226[1][3];
                    }
                    (*l_2277) |= (((**l_2215) , (safe_add_func_int64_t_s_s(((****l_2214) = (l_2236 | (safe_mod_func_uint32_t_u_u((&g_1574 == (void*)0), (~((((l_2264[1] != (l_2269 = l_2266)) || (g_196[g_1578.f1] = (((safe_div_func_float_f_f((((p_28 <= ((l_2275 = (g_1549 = (void*)0)) != (g_472 = l_2276[1]))) < (-0x6.3p-1)) >= 0xD.5D03BCp+79), (-0x1.Fp+1))) , 0xE33655234363C1CBLL) < g_1769[2][1]))) < 0UL) >= 65535UL)))))), 0x0434122DFB22F637LL))) != l_2077[1][2]);
                }
                else
                { /* block id: 998 */
                    uint64_t l_2305 = 0UL;
                    union U4 *l_2308 = &g_2309;
                    int32_t l_2310 = 0xDC6F85FDL;
                    int32_t *l_2312 = &l_2310;
                    int32_t *l_2313 = &l_2232;
                    for (g_1726.f3 = 8; (g_1726.f3 >= 0); g_1726.f3 -= 1)
                    { /* block id: 1001 */
                        int32_t *l_2292 = &l_2208;
                        int16_t ***l_2304 = &l_2265[0];
                        volatile int32_t * volatile *l_2311 = &g_1767[0][1];
                        (*l_2292) &= (g_1446.f1 > (safe_sub_func_uint32_t_u_u((((safe_sub_func_uint32_t_u_u((&g_1419[1][3][4] != (void*)0), ((void*)0 != l_2282))) || (g_2283[1] , ((l_2220[2][1][1] = (safe_mod_func_uint64_t_u_u(l_2145, (((safe_mod_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u(p_27, (g_2290 , g_2291[0]))), l_2220[4][1][0])) >= l_2229) , (*g_1422))))) <= g_1446.f1))) || l_2083), 0x88F96CC5L)));
                        l_2310 = (((safe_div_func_int32_t_s_s(((safe_unary_minus_func_int64_t_s(p_27)) & (g_2296[3][1] , ((safe_lshift_func_uint16_t_u_u(((((**g_1421) = (**g_1421)) >= 0x82C4A548EDDCB2C3LL) == (((((*l_2215) = (((safe_div_func_uint64_t_u_u(((&g_208 != (void*)0) & (safe_add_func_int16_t_s_s((((*l_2252) = (*l_2252)) != (((((g_2303 , (p_27 , l_2304)) != &l_2267) == l_2305) && p_27) , l_2306)), 0x0C54L))), 0xB0B31AE613CC48BBLL)) , p_27) , l_2307)) != l_2308) & 0L) == 0L)), l_2305)) <= g_1400))), 0x336EFE4BL)) && p_27) == l_2236);
                        (*l_2311) = (*g_1766);
                        l_2292 = &l_1844[3][6][1];
                    }
                    l_2314++;
                    (*l_2313) = l_2317;
                }
                for (g_69.f3 = 8; (g_69.f3 >= 1); g_69.f3 -= 1)
                { /* block id: 1016 */
                    int32_t *l_2320 = &g_369[1].f1;
                    l_2240 ^= (safe_mod_func_uint8_t_u_u(p_27, 7L));
                    l_2320 = &l_2231;
                    for (g_418 = 0; (g_418 <= 8); g_418 += 1)
                    { /* block id: 1021 */
                        (**g_1573) = (**g_1573);
                    }
                }
                ++l_2321;
                if ((safe_unary_minus_func_uint32_t_u(p_27)))
                { /* block id: 1026 */
                    if (l_2236)
                        break;
                }
                else
                { /* block id: 1028 */
                    int32_t l_2325 = 0x47C4A7A2L;
                    int32_t *l_2326 = &g_1018.f1;
                    int32_t *l_2327 = &g_1728[0][3].f1;
                    int32_t *l_2328 = (void*)0;
                    int32_t *l_2329 = &g_2309.f1;
                    int32_t *l_2330 = &l_2235[0];
                    int32_t *l_2331[6] = {&l_2220[0][1][3],&l_2220[0][1][3],&l_2220[0][1][3],&l_2220[0][1][3],&l_2220[0][1][3],&l_2220[0][1][3]};
                    int i;
                    l_2220[2][1][1] = (-1L);
                    l_2332--;
                    if (p_27)
                        break;
                }
            }
            if (l_2220[3][1][1])
                continue;
        }
    }
    return g_2125[8];
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_343 g_351 g_1411.f2 g_1406 g_659.f0 g_3 g_310.f1 g_1447.f2 g_1420 g_472 g_308.f0 g_138 g_201 g_107 g_108 g_100 g_94 g_95 g_315.f0 g_1120 g_1573 g_14 g_1332 g_17 g_1603 g_309.f0 g_1422 g_418 g_1568.f1 g_1632 g_1524.f2 g_258 g_1118 g_1421 g_1402 g_1365 g_1671 g_659.f1 g_313.f0 g_858 g_1578.f0 g_1086 g_1087 g_67 g_1568.f3 g_231 g_1763 g_311.f0 g_1835 g_1731 g_1732 g_69.f1 g_176.f0 g_1728.f0 g_307.f0 g_1121 g_23 g_206 g_210
 * writes: g_23 g_343 g_1406 g_1447.f2 g_1421 g_231 g_14 g_1401 g_472 g_227 g_229 g_1549 g_1550 g_206 g_95 g_1120 g_1568.f3 g_1118 g_1402 g_202 g_108 g_69.f1 g_1365 g_210 g_131.f3 g_312.f0 g_1671 g_1731 g_1763 g_311.f0 g_131.f1 g_1835
 */
static uint32_t  func_36(int32_t ** p_37)
{ /* block id: 16 */
    uint16_t *l_40 = &g_32[2];
    uint32_t *l_1472 = &g_343[4][4];
    int32_t l_1475 = 0L;
    uint16_t **l_1477 = &l_40;
    uint16_t ***l_1476 = &l_1477;
    int32_t l_1478[2][7][3] = {{{0xB791535CL,0xA6C1706DL,0xC3AEE469L},{0x044F6E9BL,0xA6C1706DL,0L},{0xB765DA9AL,0xA6C1706DL,0xA6C1706DL},{0xB791535CL,0xA6C1706DL,0xC3AEE469L},{0x044F6E9BL,0xA6C1706DL,0L},{0xB765DA9AL,0xA6C1706DL,0xA6C1706DL},{0xB791535CL,0xA6C1706DL,0xC3AEE469L}},{{0x044F6E9BL,0xA6C1706DL,0L},{0xB765DA9AL,0xA6C1706DL,0xA6C1706DL},{0xB791535CL,0xA6C1706DL,0xC3AEE469L},{0x044F6E9BL,0xA6C1706DL,0L},{0xB765DA9AL,0xA6C1706DL,0xA6C1706DL},{0xB791535CL,0xA6C1706DL,0xC3AEE469L},{0x044F6E9BL,0xA6C1706DL,0L}}};
    union U4 ***l_1485 = &g_129[1][5];
    union U4 ****l_1484[7] = {&l_1485,&l_1485,&l_1485,&l_1485,&l_1485,&l_1485,&l_1485};
    union U4 *****l_1483 = &l_1484[0];
    uint32_t *l_1490 = &g_1406[3][1];
    uint8_t *l_1491[10][3] = {{&g_108[5],&g_196[0],&g_196[0]},{&g_108[5],&g_263[5],&g_108[5]},{&g_108[5],(void*)0,&g_196[0]},{&g_108[5],&g_196[0],&g_196[0]},{&g_108[5],&g_263[5],&g_108[5]},{&g_108[5],(void*)0,&g_196[0]},{&g_108[5],&g_196[0],&g_196[0]},{&g_108[5],&g_263[5],&g_108[5]},{&g_108[5],(void*)0,&g_196[0]},{&g_108[5],&g_196[0],&g_196[0]}};
    int16_t l_1492 = 1L;
    int64_t **l_1495 = &g_1422;
    uint16_t l_1520 = 65532UL;
    struct S0 *l_1523 = &g_1524[1][0][0];
    struct S1 **l_1576 = &g_858;
    struct S1 *** const l_1575 = &l_1576;
    float l_1612 = (-0x2.Dp-1);
    int32_t ****l_1635 = &g_1086;
    int32_t ** const *l_1670 = (void*)0;
    uint32_t l_1712 = 0xB49CE591L;
    int16_t **l_1752 = (void*)0;
    int i, j, k;
lbl_1581:
    if ((l_1492 = (((func_38(l_40) , ((safe_sub_func_uint16_t_u_u(((((++(*l_1472)) || l_1475) > (l_1478[0][4][0] = (l_1476 != &g_385[4][2][3]))) != ((g_1447.f2 ^= (((((((((safe_rshift_func_int8_t_s_u((safe_mod_func_uint64_t_u_u(((void*)0 != l_1483), (safe_div_func_uint32_t_u_u(((*l_1490) &= ((((safe_add_func_int64_t_s_s(l_1475, ((l_40 == &g_206) <= l_1475))) ^ g_351[1][3]) != g_1411.f2) == l_1475)), g_659.f0)))), 6)) != 0x30BC432FL) | 1L) , l_1491[1][1]) == l_1491[1][1]) < (-10L)) && g_3[2]) | l_1475) > g_310[3][0].f1)) ^ 0xAD68C12DL)), g_32[3])) ^ l_1475)) || (-5L)) | l_1475)))
    { /* block id: 643 */
        int64_t **l_1502 = &g_1422;
        int32_t l_1507 = 1L;
        int32_t *l_1508 = &l_1478[1][5][1];
        int8_t *l_1527 = (void*)0;
        int8_t *l_1528 = &g_1401;
        const int32_t l_1529 = 0xED11041FL;
        int32_t ****l_1530[10] = {&g_1086,&g_1086,&g_1086,&g_1086,&g_1086,&g_1086,&g_1086,&g_1086,&g_1086,&g_1086};
        int16_t *l_1545[3];
        uint8_t l_1556 = 255UL;
        int i;
        for (i = 0; i < 3; i++)
            l_1545[i] = (void*)0;
        (*g_472) = (safe_div_func_float_f_f(((l_1475 <= (l_1495 != ((*g_1420) = l_1495))) != ((-((safe_unary_minus_func_uint8_t_u(254UL)) , (0x0.1p-1 > (safe_div_func_float_f_f(((safe_mul_func_float_f_f(((l_1495 == l_1502) <= (safe_div_func_float_f_f(((safe_add_func_float_f_f(((void*)0 == &g_385[0][0][2]), l_1507)) != l_1478[0][5][1]), 0xB.39FF37p-15))), 0xD.7574D4p-94)) <= l_1475), l_1507))))) <= 0x4.9p+1)), (-0x9.8p-1)));
lbl_1510:
        if (l_1475)
            goto lbl_1509;
lbl_1509:
        (*p_37) = l_1508;
        if (g_1447.f2)
            goto lbl_1510;
        if (((g_308[3][5].f0 && (safe_rshift_func_uint16_t_u_s((((safe_lshift_func_int8_t_s_u(((*l_1528) = (safe_div_func_int16_t_s_s(((safe_sub_func_int64_t_s_s((!(l_1520 <= ((safe_rshift_func_uint8_t_u_s(((void*)0 == l_1523), (*l_1508))) >= g_138[0][4][0]))), 0x15D282D621A013D9LL)) , (*l_1508)), (safe_sub_func_int64_t_s_s(l_1478[0][4][0], l_1492))))), l_1475)) == (*l_1508)) <= g_201[2]), g_1406[3][1]))) | l_1529))
        { /* block id: 650 */
            float **l_1531 = &g_472;
            float *l_1533 = &g_231[0];
            int32_t l_1544[6] = {0xE48B2D1AL,0xE48B2D1AL,0xE48B2D1AL,0xE48B2D1AL,0xE48B2D1AL,0xE48B2D1AL};
            float *l_1546 = &g_227;
            float *l_1547[3];
            float **l_1548[1][1][4];
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1547[i] = (void*)0;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 4; k++)
                        l_1548[i][j][k] = &l_1546;
                }
            }
lbl_1551:
            l_1530[2] = &g_1086;
            g_1550 = (((((*l_1531) = (((*g_107) , l_1492) , l_1472)) == (g_1549 = ((g_229 = ((-(((*l_1533) = 0x4.Bp+1) > (safe_div_func_float_f_f((safe_div_func_float_f_f((g_100 > (safe_mul_func_float_f_f((!(!g_308[3][5].f0)), ((*l_1546) = (((safe_mul_func_uint16_t_u_u((l_1475 > l_1544[4]), ((void*)0 == p_37))) , l_1545[0]) != (void*)0))))), 0xA.E5B61Dp+39)), 0x0.Fp-1)))) , 0x3.4C8588p+24)) , l_1547[1]))) && 18446744073709551615UL) , (*g_472));
            if (g_100)
                goto lbl_1551;
        }
        else
        { /* block id: 659 */
            float l_1554 = 0x0.Bp-1;
            int32_t l_1555[8][2][4] = {{{0xA60BEB07L,0xA60BEB07L,0L,0xA60BEB07L},{0xA60BEB07L,(-3L),(-3L),0xA60BEB07L}},{{(-3L),0xA60BEB07L,(-3L),(-3L)},{0xA60BEB07L,0xA60BEB07L,0L,0xA60BEB07L}},{{0xA60BEB07L,(-3L),(-3L),0xA60BEB07L},{(-3L),0xA60BEB07L,(-3L),(-3L)}},{{0xA60BEB07L,0xA60BEB07L,0L,0xA60BEB07L},{(-3L),0L,0L,(-3L)}},{{0L,(-3L),0L,0L},{(-3L),(-3L),0xA60BEB07L,(-3L)}},{{(-3L),0L,0L,(-3L)},{0L,(-3L),0L,0L}},{{(-3L),(-3L),0xA60BEB07L,(-3L)},{(-3L),0L,0L,(-3L)}},{{0L,(-3L),0L,0L},{(-3L),(-3L),0xA60BEB07L,(-3L)}}};
            int i, j, k;
            for (g_206 = (-30); (g_206 >= 14); ++g_206)
            { /* block id: 662 */
                (*p_37) = (void*)0;
            }
            l_1556--;
            (*g_94) = (*g_94);
        }
    }
    else
    { /* block id: 668 */
        uint64_t l_1563 = 0x711B0048DB51498ELL;
        int16_t *l_1564 = &g_1120;
        union U4 *l_1565 = &g_1566;
        union U4 *l_1567 = &g_1568;
        union U4 *l_1569 = &g_1570;
        const union U4 *l_1577 = &g_1578;
        l_1478[0][4][0] = ((((safe_sub_func_uint16_t_u_u(0xB8A2L, ((safe_sub_func_int32_t_s_s(l_1563, ((((*l_1564) |= g_315.f0) || l_1478[1][0][2]) , (((l_1569 = (l_1567 = (l_1565 = (l_1563 , l_1565)))) == ((safe_add_func_uint16_t_u_u(l_1563, (g_1573 != l_1575))) , l_1577)) , l_1563)))) == 0x55EA4791L))) < (-1L)) || l_1563) , 0x3.4B0466p-7);
    }
    for (g_1568.f3 = 2; (g_1568.f3 != (-16)); g_1568.f3 = safe_sub_func_uint16_t_u_u(g_1568.f3, 6))
    { /* block id: 677 */
        int32_t *l_1582 = &g_17;
        int32_t ****l_1637 = &g_1086;
        uint64_t l_1669 = 18446744073709551611UL;
        float l_1672 = 0xE.13EB5Bp+38;
        uint16_t ***l_1686 = &l_1477;
        int32_t *l_1687 = &g_69.f1;
        int32_t *l_1688 = &g_1018.f1;
        int32_t l_1702 = 0L;
        int32_t l_1705 = 0x39A3CAD7L;
        int32_t l_1706 = 0x9F418485L;
        int32_t l_1709 = 0xAB4D46B1L;
        int32_t l_1711 = 1L;
        int64_t *** const *l_1757 = &g_1420;
        int16_t l_1813 = 0xAB14L;
        int32_t l_1818 = 0x9C7A27D1L;
        uint32_t l_1826 = 0x143FFAC3L;
        int16_t l_1841[3][1];
        const union U4 ***l_1842 = &g_94;
        int i, j;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_1841[i][j] = 0xEE42L;
        }
        for (l_1475 = 1; (l_1475 >= 0); l_1475 -= 1)
        { /* block id: 680 */
            (*p_37) = (*p_37);
            for (g_1118 = 1; (g_1118 >= 0); g_1118 -= 1)
            { /* block id: 684 */
                if (g_1411.f2)
                    goto lbl_1581;
            }
        }
        if (((void*)0 == l_1495))
        { /* block id: 688 */
            (*p_37) = l_1582;
        }
        else
        { /* block id: 690 */
            union U3 *l_1585 = &g_1586[0];
            int32_t l_1595 = 0L;
            union U4 ****l_1634[7] = {&l_1485,&l_1485,&l_1485,&l_1485,&l_1485,&l_1485,&l_1485};
            int32_t ****l_1648 = &g_1086;
            uint64_t *l_1656 = &g_67[3];
            uint64_t **l_1655 = &l_1656;
            int i;
            for (g_1402 = 1; (g_1402 <= 4); g_1402 += 1)
            { /* block id: 693 */
                float *l_1601 = &g_1550;
                int32_t l_1604 = 0x164FF95EL;
                float *l_1605[5];
                int32_t ****l_1649 = &g_1086;
                uint64_t **l_1657 = &l_1656;
                int i;
                for (i = 0; i < 5; i++)
                    l_1605[i] = (void*)0;
                l_1595 = (safe_sub_func_float_f_f((((((*g_472) = (g_1332 == l_1585)) == ((safe_sub_func_float_f_f((safe_add_func_float_f_f((-0x1.3p-1), ((safe_add_func_float_f_f((safe_sub_func_float_f_f(l_1595, (!(safe_add_func_float_f_f((safe_sub_func_float_f_f((*l_1582), (((*l_1601) = (*l_1582)) <= ((!((g_1603 == 6UL) , (((((l_1604 < g_309[0].f0) > l_1604) != l_1604) , l_1604) , l_1595))) >= l_1604)))), 0x6.4F0D91p+88))))), l_1604)) < 0xF.6688ACp+24))), l_1604)) < l_1520)) == (*l_1582)) != l_1595), l_1604));
                if ((safe_lshift_func_uint16_t_u_s((*l_1582), 8)))
                { /* block id: 697 */
                    int8_t l_1610 = 7L;
                    int32_t l_1613 = 9L;
                    int32_t *****l_1636 = (void*)0;
                    for (g_202 = 1; (g_202 <= 4); g_202 += 1)
                    { /* block id: 700 */
                        int32_t **l_1611 = &l_1582;
                        l_1613 &= (safe_rshift_func_uint8_t_u_s(((*g_107) ^= l_1610), (l_1611 != p_37)));
                        (*p_37) = ((((safe_div_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s(((l_1595 == (safe_rshift_func_uint16_t_u_u(0xCD71L, ((((*g_258) = (((*l_1472) = (safe_mod_func_int64_t_s_s((*g_1422), (**l_1611)))) , ((((safe_lshift_func_int8_t_s_u(g_418, 6)) , (safe_mod_func_uint32_t_u_u(0xD9D94DE1L, (safe_div_func_uint32_t_u_u(((safe_add_func_uint16_t_u_u(((safe_mod_func_uint32_t_u_u(g_1568.f1, (((g_1632 != &g_1633) && (**l_1611)) || 65527UL))) ^ l_1595), g_1120)) != l_1604), g_1524[1][0][0].f2))))) , 0x4A2DE093B234C4BDLL) <= l_1604))) < l_1610) || 0x90B2L)))) <= g_1118), l_1595)), 0x2DDE4A814BC03345LL)) , l_1595) == 0x79L) , (*l_1611));
                        (*l_1483) = l_1634[6];
                    }
                    if (l_1595)
                        break;
                    if ((l_1635 == (l_1637 = (void*)0)))
                    { /* block id: 710 */
                        int32_t *l_1638 = &l_1478[0][4][0];
                        (*p_37) = l_1638;
                    }
                    else
                    { /* block id: 712 */
                        int16_t l_1639 = 0xD28DL;
                        int32_t *****l_1650 = (void*)0;
                        int32_t *****l_1651 = (void*)0;
                        int32_t *****l_1652 = &l_1635;
                        uint32_t l_1673 = 4294967295UL;
                        int i;
                        (*g_258) = l_1639;
                        l_1673 &= (safe_lshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u((((g_1568.f1 != ((18446744073709551615UL == ((safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_u(((l_1648 != ((*l_1652) = l_1649)) | ((safe_add_func_int32_t_s_s(((5L | (((***g_1420) < (l_1655 == l_1657)) , (!(--g_1365[g_1402])))) , (((safe_add_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((*l_1582), g_351[3][5])), 1)), l_1669)), (-1L))) , &p_37) == l_1670)), (*l_1582))) , l_1475)), g_1671)), g_138[7][4][2])) , (*l_1582))) == (*l_1582))) | 0xD995D2CAL) , g_659.f1), g_308[3][5].f0)), 7));
                    }
                }
                else
                { /* block id: 718 */
                    return g_313.f0;
                }
            }
        }
        for (g_210 = 0; (g_210 > 41); ++g_210)
        { /* block id: 725 */
            uint16_t ***l_1685 = (void*)0;
            int32_t l_1704 = 0x82DE8C9DL;
            int32_t l_1708[3];
            uint32_t l_1715 = 0x511B7DAFL;
            float *l_1721 = &g_227;
            union U4 *l_1727[5];
            int16_t *l_1751[10][9] = {{&l_1492,&g_311.f0,&l_1492,&g_1120,(void*)0,&l_1492,&g_313.f0,&l_1492,(void*)0},{(void*)0,&g_206,&g_206,(void*)0,&g_1120,(void*)0,&g_206,&g_315.f0,(void*)0},{(void*)0,&g_1120,&g_315.f0,&g_1120,(void*)0,&g_1120,(void*)0,(void*)0,(void*)0},{&g_315.f0,&g_1120,&g_315.f0,&g_315.f0,&g_1120,&g_315.f0,&g_312[0].f0,&g_315.f0,&g_311.f0},{&g_315.f0,&g_1120,(void*)0,(void*)0,(void*)0,&g_1120,&g_315.f0,&g_1120,&g_315.f0},{(void*)0,&g_312[0].f0,(void*)0,&g_315.f0,&g_315.f0,(void*)0,&g_312[0].f0,(void*)0,&g_315.f0},{&g_311.f0,&g_1120,(void*)0,&l_1492,(void*)0,&l_1492,(void*)0,&g_1120,&g_311.f0},{&g_206,&g_315.f0,&g_315.f0,&g_206,&g_206,&g_206,&g_206,&g_315.f0,&g_315.f0},{(void*)0,&g_311.f0,&g_315.f0,(void*)0,&g_313.f0,&g_1120,&g_313.f0,(void*)0,&g_315.f0},{&g_206,&g_315.f0,&g_206,&g_311.f0,&g_315.f0,&g_315.f0,&g_315.f0,&g_315.f0,&g_311.f0}};
            int16_t **l_1750[10][9][2] = {{{&l_1751[9][1],&l_1751[3][4]},{&l_1751[9][1],&l_1751[9][1]},{(void*)0,&l_1751[9][1]},{&l_1751[4][5],&l_1751[4][5]},{&l_1751[3][4],&l_1751[0][0]},{(void*)0,&l_1751[9][1]},{&l_1751[4][1],&l_1751[5][2]},{&l_1751[9][1],&l_1751[4][1]},{&l_1751[0][0],&l_1751[9][1]}},{{&l_1751[0][0],&l_1751[4][1]},{&l_1751[9][1],&l_1751[5][2]},{&l_1751[4][1],&l_1751[9][1]},{(void*)0,&l_1751[0][0]},{&l_1751[3][4],&l_1751[4][5]},{&l_1751[4][5],&l_1751[9][1]},{(void*)0,&l_1751[9][1]},{&l_1751[9][1],&l_1751[3][4]},{&l_1751[9][1],(void*)0}},{{&l_1751[9][1],&l_1751[9][1]},{(void*)0,&l_1751[2][3]},{&l_1751[6][2],&l_1751[9][1]},{&l_1751[5][2],(void*)0},{&l_1751[6][2],&l_1751[9][1]},{&l_1751[9][1],&l_1751[6][2]},{(void*)0,(void*)0},{(void*)0,&l_1751[0][0]},{(void*)0,&l_1751[6][2]}},{{(void*)0,&l_1751[9][1]},{(void*)0,&l_1751[3][6]},{&l_1751[4][1],&l_1751[0][6]},{(void*)0,&l_1751[2][8]},{&l_1751[0][7],&l_1751[5][5]},{&l_1751[8][1],&l_1751[3][4]},{(void*)0,&l_1751[3][4]},{&l_1751[8][1],&l_1751[5][5]},{&l_1751[0][7],&l_1751[2][8]}},{{(void*)0,&l_1751[0][6]},{&l_1751[4][1],&l_1751[8][1]},{&l_1751[8][3],&l_1751[1][2]},{&l_1751[2][8],&l_1751[3][6]},{(void*)0,(void*)0},{&l_1751[9][1],&l_1751[0][7]},{&l_1751[5][5],(void*)0},{&l_1751[9][1],&l_1751[0][6]},{&l_1751[3][6],&l_1751[9][1]}},{{&l_1751[9][1],&l_1751[1][8]},{(void*)0,&l_1751[4][1]},{(void*)0,&l_1751[6][2]},{&l_1751[9][1],&l_1751[8][3]},{&l_1751[1][2],&l_1751[9][1]},{&l_1751[6][2],&l_1751[0][6]},{&l_1751[3][4],&l_1751[9][1]},{&l_1751[9][1],&l_1751[9][1]},{&l_1751[9][1],(void*)0}},{{(void*)0,&l_1751[9][1]},{&l_1751[9][1],&l_1751[9][1]},{&l_1751[1][8],&l_1751[9][1]},{(void*)0,&l_1751[0][6]},{(void*)0,&l_1751[9][1]},{&l_1751[1][8],&l_1751[9][1]},{&l_1751[9][1],&l_1751[9][1]},{(void*)0,(void*)0},{&l_1751[9][1],&l_1751[9][1]}},{{&l_1751[9][1],&l_1751[9][1]},{&l_1751[3][4],&l_1751[0][6]},{&l_1751[6][2],&l_1751[9][1]},{&l_1751[1][2],&l_1751[8][3]},{&l_1751[9][1],&l_1751[6][2]},{(void*)0,&l_1751[4][1]},{(void*)0,&l_1751[1][8]},{&l_1751[9][1],&l_1751[9][1]},{&l_1751[3][6],&l_1751[0][6]}},{{&l_1751[9][1],(void*)0},{&l_1751[5][5],&l_1751[0][7]},{&l_1751[9][1],(void*)0},{(void*)0,&l_1751[3][6]},{&l_1751[2][8],&l_1751[1][2]},{&l_1751[8][3],&l_1751[8][1]},{&l_1751[4][1],&l_1751[0][6]},{(void*)0,&l_1751[2][8]},{&l_1751[0][7],&l_1751[5][5]}},{{&l_1751[8][1],&l_1751[3][4]},{(void*)0,&l_1751[3][4]},{&l_1751[8][1],&l_1751[5][5]},{&l_1751[0][7],&l_1751[2][8]},{(void*)0,&l_1751[0][6]},{&l_1751[4][1],&l_1751[8][1]},{&l_1751[8][3],&l_1751[1][2]},{&l_1751[2][8],&l_1751[3][6]},{(void*)0,(void*)0}}};
            uint16_t l_1788 = 0x3B7CL;
            int8_t l_1814 = 0xF9L;
            int32_t ** const ***l_1836 = &g_1835;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1708[i] = 0L;
            for (i = 0; i < 5; i++)
                l_1727[i] = &g_1728[0][3];
            for (g_131.f3 = 6; (g_131.f3 != 4); --g_131.f3)
            { /* block id: 728 */
                int64_t l_1682 = 0L;
                int32_t l_1691 = 0x15202DE6L;
                int32_t l_1707 = 0L;
                int32_t *l_1724 = &g_138[7][6][2];
                (*g_258) = ((**l_1575) == (void*)0);
                if ((safe_lshift_func_uint16_t_u_s(g_310[3][0].f1, (((((safe_mod_func_uint64_t_u_u(l_1682, (safe_div_func_uint16_t_u_u(g_32[1], g_309[0].f0)))) , (g_1578.f0 , l_1685)) == l_1686) , (*g_1086)) != (p_37 = p_37)))))
                { /* block id: 731 */
                    l_1688 = l_1687;
                }
                else
                { /* block id: 733 */
                    float l_1703[8][6] = {{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62},{0x6.131085p-63,0xE.B6AD8Dp-14,0x6.131085p-63,0xA.D839AAp+62,0xE.BBAEA8p-1,0xA.D839AAp+62}};
                    int32_t l_1710 = 1L;
                    int i, j;
                    for (g_69.f1 = 0; (g_69.f1 != (-14)); g_69.f1 = safe_sub_func_int16_t_s_s(g_69.f1, 1))
                    { /* block id: 736 */
                        int32_t *l_1692 = &g_1570.f1;
                        int32_t *l_1693 = (void*)0;
                        int32_t *l_1694 = &l_1478[0][6][0];
                        int32_t *l_1695 = &g_1570.f1;
                        int32_t *l_1696 = &g_176.f1;
                        int32_t *l_1697 = &l_1478[0][4][0];
                        int32_t *l_1698 = &l_1475;
                        int32_t *l_1699 = &g_369[1].f1;
                        int32_t *l_1700 = &l_1691;
                        int32_t *l_1701[3][2][4] = {{{&g_3[0],&g_1568.f1,&g_3[0],&g_3[0]},{&g_1568.f1,&g_1568.f1,&g_131.f1,&g_1568.f1}},{{&g_1568.f1,&g_3[0],&g_3[0],&g_1568.f1},{&g_3[0],&g_1568.f1,&g_3[0],&g_3[0]}},{{&g_1568.f1,&g_1568.f1,&g_131.f1,&g_1568.f1},{&g_3[0],&g_131.f1,&g_131.f1,&g_3[0]}}};
                        int16_t *l_1720 = &g_312[0].f0;
                        union U3 **l_1723 = (void*)0;
                        union U3 ***l_1722 = &l_1723;
                        union U4 *l_1725 = &g_1726;
                        int i, j, k;
                        --l_1712;
                        (*l_1694) = (l_1715 , ((safe_mul_func_uint16_t_u_u((*l_1582), (((((l_1710 == ((safe_mul_func_int16_t_s_s(((*l_1720) = g_17), (l_1721 != (void*)0))) & ((((-3L) != (((*l_1722) = &g_1444[2]) != (void*)0)) || g_67[0]) == 0x4ACA26EFL))) , (void*)0) == l_1724) & 3L) >= (*l_1582)))) ^ (-9L)));
                        l_1727[0] = l_1725;
                        if (g_1568.f3)
                            goto lbl_1581;
                    }
                }
            }
            for (g_1671 = 0; (g_1671 <= 6); g_1671 += 1)
            { /* block id: 748 */
                const int16_t *l_1733 = &g_1120;
                int32_t l_1755 = 0xD49FDCE6L;
                int32_t l_1787 = 1L;
                (*p_37) = (*p_37);
                (*p_37) = (*p_37);
                l_1708[0] |= l_1704;
                for (l_1492 = 1; (l_1492 <= 6); l_1492 += 1)
                { /* block id: 754 */
                    const int16_t **l_1734 = (void*)0;
                    const int16_t **l_1735 = (void*)0;
                    const int16_t **l_1736 = &l_1733;
                    int32_t l_1747 = 0xD0E236CFL;
                    int16_t ***l_1753 = &l_1752;
                    int8_t l_1754 = 0L;
                    int64_t ****l_1756[10] = {&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420};
                    int i;
                    if ((((safe_sub_func_int16_t_s_s(((g_1731 = &l_1492) != ((*l_1736) = l_1733)), (safe_rshift_func_uint8_t_u_s(((*g_107) && (safe_add_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((((safe_div_func_float_f_f((((((safe_add_func_float_f_f(((l_1747 > (l_1747 < l_1478[0][4][0])) , (*g_472)), ((safe_add_func_float_f_f((l_1750[8][4][0] != ((*l_1753) = l_1752)), 0x1.6p+1)) >= (*g_472)))) == 0xD.60C92Bp+96) , l_1754) < l_1755) >= (*g_472)), l_1715)) , l_1756[9]) != l_1757), (***g_1420))), 0x9582D36EB2325F06LL))), l_1708[0])))) <= l_1715) > (*l_1582)))
                    { /* block id: 758 */
                        uint32_t l_1758 = 0x98174D0AL;
                        int32_t *l_1759[2];
                        uint64_t l_1760 = 0xF485BAFA7B2956EELL;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1759[i] = (void*)0;
                        g_1447.f2 |= l_1758;
                        (*p_37) = (*p_37);
                        ++l_1760;
                    }
                    else
                    { /* block id: 762 */
                        int32_t *l_1781 = &l_1747;
                        int32_t *l_1782 = &l_1706;
                        int32_t *l_1783 = (void*)0;
                        int32_t *l_1784 = &g_1726.f1;
                        int32_t *l_1785 = &g_369[1].f1;
                        int32_t *l_1786[10] = {&l_1705,&l_1705,&g_3[1],&l_1705,&l_1705,&g_3[1],&l_1705,&l_1705,&g_3[1],&l_1705};
                        int i;
                        g_1763 = g_1763;
                        l_1788++;
                    }
                    if (l_1755)
                        break;
                    (*g_472) = 0x6.BEF129p+73;
                }
            }
            for (g_311.f0 = 6; (g_311.f0 >= 5); --g_311.f0)
            { /* block id: 772 */
                int16_t *l_1795 = &g_312[0].f0;
                int32_t l_1810 = 0x9477A07CL;
                int32_t l_1811 = 0L;
                int32_t l_1812 = 0xFE37301DL;
                int32_t l_1815 = (-2L);
                int32_t l_1816 = 0x17506458L;
                int32_t l_1817 = 0x4CAB37C0L;
                uint32_t l_1819 = 1UL;
                for (g_131.f1 = 18; (g_131.f1 < 16); g_131.f1 = safe_sub_func_int32_t_s_s(g_131.f1, 1))
                { /* block id: 775 */
                    uint16_t l_1796 = 0x1594L;
                    int32_t *l_1797 = &l_1478[1][2][0];
                    int32_t *l_1798 = &g_369[1].f1;
                    int32_t *l_1799 = (void*)0;
                    int32_t *l_1800 = &l_1711;
                    int32_t *l_1801 = &g_69.f1;
                    int32_t *l_1802 = &g_176.f1;
                    int32_t *l_1803 = (void*)0;
                    int32_t *l_1804 = &g_1570.f1;
                    int32_t *l_1805 = &g_69.f1;
                    int32_t *l_1806 = &l_1702;
                    int32_t *l_1807 = &l_1708[0];
                    int32_t *l_1808 = &g_176.f1;
                    int32_t *l_1809[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1809[i] = &l_1705;
                    l_1708[1] ^= (&g_1732 != l_1795);
                    if (l_1796)
                        break;
                    l_1819--;
                }
            }
            l_1478[0][4][0] |= (safe_rshift_func_int16_t_s_s(((((*l_1687) = (-5L)) , ((((safe_sub_func_int8_t_s_s((((((l_1826 | (safe_add_func_int8_t_s_s((safe_add_func_int64_t_s_s((safe_add_func_int32_t_s_s((safe_add_func_int8_t_s_s((((*l_1836) = g_1835) == (((*g_1731) && ((void*)0 != (*g_1086))) , l_1637)), (safe_add_func_uint64_t_u_u((*l_1582), (((((((((0xD5090714L & l_1841[2][0]) || (*l_1687)) , l_1814) > 0L) , 0x4C349F20L) , (*g_1731)) , l_1842) != (void*)0) , g_176.f0))))), (*l_1687))), 18446744073709551615UL)), 0xD6L))) , (***g_1420)) & g_1728[0][3].f0) , 0xD135EDD3L) >= g_307[0][4][5].f0), (*l_1582))) , (*g_1731)) < (*g_1731)) ^ l_1704)) == g_307[0][4][5].f0), 14));
        }
        return g_1121;
    }
    (*p_37) = (void*)0;
    return l_1520;
}


/* ------------------------------------------ */
/* 
 * reads : g_23 g_32
 * writes: g_23
 */
static uint16_t  func_38(uint16_t * p_39)
{ /* block id: 17 */
    int16_t l_55[1][2];
    int32_t *l_421 = &g_3[0];
    const int32_t *l_852 = &g_599;
    const int32_t **l_851 = &l_852;
    uint16_t *l_897 = (void*)0;
    int32_t l_905 = 8L;
    int32_t l_906 = (-2L);
    int32_t l_911 = 0x534D6AE0L;
    int32_t l_912 = 3L;
    int8_t l_950 = (-1L);
    union U4 *l_1017 = &g_1018;
    union U4 ***l_1054 = &g_129[1][5];
    union U4 ****l_1053 = &l_1054;
    union U4 *****l_1052 = &l_1053;
    int32_t l_1117 = 3L;
    int64_t * const l_1152 = &g_201[6];
    int64_t * const *l_1151 = &l_1152;
    uint8_t l_1165 = 0UL;
    const int64_t *l_1233 = (void*)0;
    uint16_t l_1297[4][6] = {{1UL,0xD39FL,0xD39FL,1UL,0xD39FL,0xD39FL},{1UL,0xD39FL,0xD39FL,1UL,0xD39FL,0xD39FL},{1UL,0xD39FL,0xD39FL,1UL,0xD39FL,0xD39FL},{1UL,0xD39FL,0xD39FL,1UL,0xD39FL,0xD39FL}};
    int32_t l_1356 = 0L;
    int32_t l_1359 = 3L;
    int32_t l_1360 = 0x4A7D1F19L;
    int32_t l_1362 = 0xA19B0421L;
    int32_t l_1363 = 0x48294B99L;
    int32_t l_1364[3];
    int32_t l_1391 = 0xD4ECA194L;
    int8_t l_1398 = 0L;
    float l_1468 = 0x1.4p+1;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_55[i][j] = 3L;
    }
    for (i = 0; i < 3; i++)
        l_1364[i] = 0x38098943L;
    for (g_23 = 0; (g_23 <= 21); g_23 = safe_add_func_uint8_t_u_u(g_23, 1))
    { /* block id: 20 */
        uint8_t l_61 = 1UL;
        int8_t l_854 = 6L;
        int32_t l_890 = 0x6A7E2232L;
        float l_896 = 0x0.44787Dp-62;
        int32_t l_908 = 0x3B72A735L;
        int32_t l_909 = 0xCF8FAD70L;
        uint8_t l_914 = 2UL;
        union U4 ***l_970[5];
        union U4 ****l_969 = &l_970[4];
        union U4 **** const *l_968 = &l_969;
        int32_t l_1110 = 0L;
        int32_t l_1111 = 0x3D651084L;
        int32_t l_1112 = 3L;
        int32_t l_1113 = 0xA36EFD51L;
        int32_t l_1114[7][3] = {{(-1L),1L,(-6L)},{0x4692A9C6L,0x8C8004BAL,0x4692A9C6L},{0x4692A9C6L,(-1L),0x8C8004BAL},{(-1L),0x4692A9C6L,0x4692A9C6L},{0x8C8004BAL,0x4692A9C6L,(-6L)},{1L,(-1L),0L},{0x8C8004BAL,0x8C8004BAL,0L}};
        int64_t *l_1133 = &g_201[6];
        int64_t ** const l_1132 = &l_1133;
        int64_t ** const *l_1131 = &l_1132;
        int64_t **l_1150 = (void*)0;
        float **l_1274[4] = {&g_472,&g_472,&g_472,&g_472};
        const struct S0 *l_1275 = (void*)0;
        int16_t l_1296 = 0xA645L;
        const uint32_t l_1322[5] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
        uint32_t l_1338 = 6UL;
        int32_t *l_1388 = &l_905;
        uint8_t l_1395 = 0xF3L;
        const struct S1 *l_1410 = &g_1411;
        const struct S1 **l_1409[7] = {&l_1410,&l_1410,&l_1410,&l_1410,&l_1410,&l_1410,&l_1410};
        int64_t *****l_1423 = &g_1419[1][3][4];
        uint32_t *l_1441 = &g_343[3][5];
        int16_t *l_1448 = (void*)0;
        int16_t *l_1449 = &g_206;
        int32_t *l_1469 = &l_890;
        int i, j;
        for (i = 0; i < 5; i++)
            l_970[i] = &g_508[4];
    }
    return (*p_39);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static float  func_47(int32_t ** p_48, const int32_t ** p_49, uint8_t  p_50)
{ /* block id: 363 */
    uint8_t l_853 = 0xA2L;
    (*p_49) = (void*)0;
    return l_853;
}


/* ------------------------------------------ */
/* 
 * reads : g_201 g_176.f0 g_312.f0 g_351 g_107 g_108 g_206 g_313.f0 g_3 g_310.f1 g_210 g_176.f1 g_127 g_343 g_258 g_603.f3 g_309.f1 g_138 g_310.f0 g_472 g_311.f1 g_383 g_69.f1 g_662.f1 g_418
 * writes: g_201 g_127 g_313.f0 g_210 g_196 g_176.f1 g_69.f1 g_603.f3 g_231 g_227 g_176.f0 g_603.f0 g_383 g_418
 */
static int32_t ** func_51(int16_t  p_52, int32_t ** p_53, int32_t * p_54)
{ /* block id: 191 */
    int64_t *l_428 = &g_369[1].f3;
    int64_t *l_429 = &g_201[6];
    int64_t *l_430 = &g_176.f3;
    int64_t *l_431 = &g_176.f3;
    int64_t *l_432 = &g_176.f3;
    int32_t l_433 = 0x016AD1CEL;
    int8_t *l_434 = &g_127;
    const int32_t l_435 = 0xD2713625L;
    int32_t l_444 = (-10L);
    const float *l_448[3];
    const float **l_447 = &l_448[2];
    int32_t l_450 = 0xB69C4481L;
    int32_t **l_522 = &g_258;
    int64_t l_629 = 0x93CF7336B561A30DLL;
    uint16_t l_664 = 0x6867L;
    uint8_t l_720 = 0xF5L;
    uint64_t l_772[9] = {0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL,0x9898DC6747B6EC19LL};
    union U4 * const *l_779[5][6][6] = {{{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130}},{{&g_130,&g_130,&g_130,&g_130,&g_130,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,(void*)0}},{{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130}},{{&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130}},{{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130}}};
    union U4 * const **l_778[10] = {&l_779[4][4][5],&l_779[2][5][1],&l_779[4][4][5],&l_779[2][5][1],&l_779[4][4][5],&l_779[2][5][1],&l_779[4][4][5],&l_779[2][5][1],&l_779[4][4][5],&l_779[2][5][1]};
    int32_t l_784 = 0x8820FE77L;
    int8_t l_785 = 3L;
    int32_t *l_786 = &l_433;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_448[i] = &g_449;
    if (((safe_div_func_uint8_t_u_u(((l_450 ^= (((safe_rshift_func_uint16_t_u_s(((safe_div_func_int8_t_s_s(((*l_434) = (((*l_429) |= p_52) & (l_433 ^= 1L))), ((g_176.f0 , l_435) , ((safe_lshift_func_int8_t_s_s((((safe_add_func_uint16_t_u_u(l_435, (safe_lshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u(65526UL, (g_312[0].f0 , (l_444 = p_52)))) | (safe_add_func_int32_t_s_s((((*l_447) = p_54) != (void*)0), 9L))), p_52)))) != p_52) , g_351[0][6]), l_435)) , (*g_107))))) > 0xA9L), 2)) || g_206) , p_52)) != 0xAC8FD3134440B08DLL), 0xCFL)) | g_108[0]))
    { /* block id: 198 */
        uint16_t l_453 = 65535UL;
        uint8_t l_487 = 255UL;
        int32_t l_491 = 5L;
        float *l_503[5];
        int64_t *l_507 = (void*)0;
        int32_t l_568 = 1L;
        union U4 **l_592 = &g_130;
        uint64_t l_604[4] = {0x0FB91D7C27A7F2FDLL,0x0FB91D7C27A7F2FDLL,0x0FB91D7C27A7F2FDLL,0x0FB91D7C27A7F2FDLL};
        int i;
        for (i = 0; i < 5; i++)
            l_503[i] = &g_229;
        for (g_313.f0 = 3; (g_313.f0 >= (-2)); g_313.f0 = safe_sub_func_uint32_t_u_u(g_313.f0, 3))
        { /* block id: 201 */
            int32_t *l_454 = &l_444;
            (*l_454) &= l_453;
            if ((*p_54))
                continue;
        }
        for (l_433 = (-27); (l_433 >= (-15)); l_433 = safe_add_func_uint64_t_u_u(l_433, 7))
        { /* block id: 207 */
            uint32_t l_466 = 0xFAA936E0L;
            int32_t l_494 = 0x01A921C5L;
            int32_t l_495 = 0x6ADCDE66L;
            union U4 * const l_496 = (void*)0;
            int8_t *l_518 = &g_23;
            int32_t **l_525 = (void*)0;
            uint8_t l_571 = 0UL;
            uint16_t *l_581[3];
            int32_t *l_584 = &g_369[1].f1;
            int32_t *l_585 = &l_494;
            int i;
            for (i = 0; i < 3; i++)
                l_581[i] = &g_131.f0;
        }
    }
    else
    { /* block id: 274 */
        float l_611[9][6] = {{0x7.Bp+1,0x7.Bp+1,(-0x4.4p-1),(-0x1.5p-1),(-0x4.4p-1),0x7.Bp+1},{(-0x4.4p-1),0x7.B82848p+20,(-0x1.5p-1),(-0x1.5p-1),0x7.B82848p+20,(-0x4.4p-1)},{0x7.Bp+1,(-0x4.4p-1),(-0x1.5p-1),(-0x4.4p-1),0x7.Bp+1,0x7.Bp+1},{0xD.13D743p+88,(-0x4.4p-1),(-0x4.4p-1),0xD.13D743p+88,0x7.B82848p+20,0xD.13D743p+88},{0xD.13D743p+88,0x7.B82848p+20,0xD.13D743p+88,(-0x4.4p-1),(-0x4.4p-1),0xD.13D743p+88},{0x7.Bp+1,0x7.Bp+1,(-0x4.4p-1),(-0x1.5p-1),(-0x4.4p-1),0x7.Bp+1},{(-0x4.4p-1),0x7.B82848p+20,(-0x1.5p-1),(-0x1.5p-1),0x7.B82848p+20,(-0x4.4p-1)},{0x7.Bp+1,(-0x4.4p-1),(-0x1.5p-1),(-0x4.4p-1),0x7.Bp+1,0x7.Bp+1},{0xD.13D743p+88,(-0x4.4p-1),(-0x4.4p-1),0xD.13D743p+88,0x7.B82848p+20,0xD.13D743p+88}};
        int32_t l_612 = 0L;
        int32_t l_613 = (-1L);
        int32_t l_614 = 0L;
        int32_t l_615 = (-8L);
        uint16_t l_616 = 65526UL;
        int64_t **l_626 = &l_430;
        int32_t **l_702 = (void*)0;
        const int8_t *l_761 = (void*)0;
        int i, j;
lbl_619:
        if ((safe_div_func_uint16_t_u_u(g_310[3][0].f1, g_201[1])))
        { /* block id: 275 */
            int32_t *l_607 = &g_603.f1;
            int32_t *l_608[4];
            int i;
            for (i = 0; i < 4; i++)
                l_608[i] = &g_131.f1;
            for (g_210 = 0; g_210 < 5; g_210 += 1)
            {
                g_196[g_210] = 4UL;
            }
            g_176.f1 |= (g_210 ^ 0xAADBL);
            if (g_176.f0)
                goto lbl_619;
        }
        else
        { /* block id: 278 */
            int32_t *l_609 = (void*)0;
            int32_t *l_610[4][7] = {{&l_433,(void*)0,&l_433,(void*)0,&l_433,(void*)0,&l_433},{&g_599,&g_599,(void*)0,(void*)0,&g_599,&g_599,(void*)0},{&l_450,(void*)0,&l_450,(void*)0,&l_450,(void*)0,&l_450},{&g_599,(void*)0,(void*)0,&g_599,&g_599,(void*)0,(void*)0}};
            int i, j;
            l_616++;
        }
        (**l_522) = (safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(p_52, 14)), ((g_127 > ((safe_lshift_func_int8_t_s_u((((*l_626) = &g_539) == &g_201[5]), (safe_sub_func_int8_t_s_s(p_52, (((((0x6.Ap-1 == (((g_201[5] , &g_472) == (p_52 , (void*)0)) == p_52)) != 0x1.1p+1) == p_52) <= l_616) , l_613))))) || g_343[7][2])) != l_629)));
        for (g_603.f3 = 0; (g_603.f3 > (-28)); g_603.f3--)
        { /* block id: 286 */
            float *l_651 = &l_611[3][0];
            struct S0 *l_658 = &g_659;
            uint32_t l_667 = 0x84F53389L;
            int32_t **l_701 = &g_14;
            int32_t l_719 = 0xAC7A9FB2L;
            int64_t *l_734 = &g_131.f3;
            if (g_176.f0)
                goto lbl_619;
        }
    }
    (*l_786) = (((safe_sub_func_int8_t_s_s(p_52, ((((safe_rshift_func_int16_t_s_s(((safe_unary_minus_func_uint64_t_u(p_52)) && ((((((void*)0 == l_778[0]) , p_52) || g_309[0].f1) <= ((**l_522) = (((void*)0 == &g_130) ^ (safe_lshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(0x26L, l_784)), g_138[7][4][2]))))) >= l_772[6])), 14)) , l_785) & p_52) < p_52))) && p_52) & g_310[3][0].f0);
    for (g_127 = 4; (g_127 <= 5); g_127 = safe_add_func_int64_t_s_s(g_127, 7))
    { /* block id: 350 */
        int64_t l_792[7] = {0x9C5AB0D63EB7E668LL,0x9C5AB0D63EB7E668LL,0x9C5AB0D63EB7E668LL,0x9C5AB0D63EB7E668LL,0x9C5AB0D63EB7E668LL,0x9C5AB0D63EB7E668LL,0x9C5AB0D63EB7E668LL};
        float *l_793 = (void*)0;
        float *l_794 = &g_227;
        uint16_t *l_813 = &g_176.f0;
        uint16_t l_821[10][3][8] = {{{0UL,5UL,0xD500L,0xE987L,0x730BL,0UL,0xAF1BL,3UL},{0UL,0x65E3L,0UL,65531UL,0x4C99L,65531UL,65535UL,65530UL},{4UL,0x6C67L,65531UL,0x539DL,0xC764L,65535UL,0x4C99L,0xC764L}},{{4UL,0xAF1BL,1UL,4UL,0x4C99L,0xE987L,3UL,0x3BDAL},{0UL,0UL,1UL,65535UL,0xAF1BL,0x3284L,65531UL,0x65E3L},{0x2F97L,65529UL,0x65DDL,0UL,0x033DL,1UL,4UL,0xDDA2L}},{{0x539DL,0xAF1BL,5UL,65530UL,1UL,0UL,0UL,0UL},{65531UL,0x3DABL,0x033DL,0x033DL,0x3DABL,65531UL,0UL,1UL},{1UL,0x3284L,0x65DDL,4UL,65529UL,0xE75CL,0x3BDAL,1UL}},{{3UL,65535UL,0xE987L,4UL,0x539DL,65535UL,3UL,1UL},{0x6C67L,0x539DL,0xD500L,0x033DL,65530UL,0UL,65535UL,0UL},{4UL,65535UL,65531UL,65530UL,0xE75CL,65531UL,0UL,0xDDA2L}},{{0x3DABL,4UL,65535UL,0UL,0x539DL,0xDB00L,0xAF1BL,0x65E3L},{0x65DDL,0x3BDAL,0xC764L,65535UL,0UL,65535UL,0xC764L,0x3BDAL},{1UL,0xDDA2L,0x3284L,4UL,0x6C67L,65531UL,0x539DL,0xC764L}},{{0x3BDAL,0UL,0UL,0x539DL,1UL,0UL,0x539DL,65530UL},{1UL,0x539DL,0x3284L,65531UL,0x2F97L,0UL,0xC764L,3UL},{0x2F97L,0UL,0xC764L,3UL,0xD500L,0xE75CL,0xAF1BL,0x65DDL}},{{65535UL,0x65E3L,65535UL,1UL,0x4C99L,0xA356L,0UL,65530UL},{0UL,0x3DABL,65531UL,1UL,7UL,65535UL,65535UL,7UL},{4UL,0xD500L,0xD500L,4UL,65535UL,1UL,3UL,65531UL}},{{65535UL,0UL,0xE987L,0UL,0xAF1BL,0UL,0x3BDAL,0x65E3L},{0x033DL,0UL,0x65DDL,65529UL,0x2F97L,1UL,0UL,4UL},{0x539DL,0xD500L,0x033DL,65530UL,0UL,65535UL,0UL,65529UL}},{{0x3BDAL,0x3DABL,5UL,0UL,0xD500L,0UL,0UL,65531UL},{65532UL,65535UL,8UL,0x2414L,1UL,5UL,7UL,0x65DDL},{0xDDA2L,0UL,0x128AL,0UL,0x128AL,0UL,0xDDA2L,65532UL}},{{0xD500L,0x65DDL,7UL,0UL,0x033DL,1UL,0x3284L,1UL},{0x2414L,65531UL,0x730BL,5UL,0x033DL,0x730BL,0xA356L,0UL},{0xD500L,0x2414L,0UL,1UL,0x128AL,0xA356L,0xC764L,0xDB00L}}};
        uint16_t *l_822 = &g_603.f0;
        uint16_t *l_823 = &g_383;
        uint64_t *l_824 = &g_418;
        int32_t *l_825 = &l_450;
        int32_t l_826 = (-8L);
        int32_t *l_827 = (void*)0;
        int32_t *l_828 = &l_433;
        int32_t l_829[8] = {1L,1L,0x99DD9DE4L,1L,1L,0x99DD9DE4L,1L,1L};
        int32_t *l_830 = &g_599;
        int32_t *l_831 = &l_433;
        int32_t *l_832 = &l_444;
        int32_t *l_833 = (void*)0;
        int32_t *l_834 = &g_176.f1;
        int32_t *l_835 = &l_829[7];
        int32_t *l_836 = &g_131.f1;
        int32_t *l_837 = &l_450;
        int32_t *l_838 = &g_69.f1;
        int32_t *l_839 = (void*)0;
        int32_t *l_840 = &g_69.f1;
        int32_t *l_841 = (void*)0;
        int32_t *l_842 = &l_829[3];
        int32_t *l_843 = &l_829[6];
        int32_t *l_844 = (void*)0;
        int32_t *l_845 = (void*)0;
        int32_t *l_846 = &l_826;
        int32_t *l_847[10][7] = {{&g_3[0],(void*)0,&g_3[0],&l_450,&l_826,&l_444,&l_829[6]},{(void*)0,&g_69.f1,&g_3[0],&l_444,&l_829[6],&l_444,&g_3[0]},{&l_826,&l_826,&l_444,(void*)0,&g_369[1].f1,&l_444,(void*)0},{(void*)0,&l_826,&g_3[0],&l_444,&l_444,&g_3[0],&l_826},{&g_3[0],&g_69.f1,&l_826,&g_3[0],&g_369[1].f1,&l_450,(void*)0},{&g_3[0],(void*)0,&l_829[6],&l_826,&l_829[6],(void*)0,&g_3[0]},{(void*)0,&l_450,&g_369[1].f1,&g_3[0],&l_826,&g_69.f1,&g_3[0]},{&l_826,&g_3[0],&l_444,&l_444,&g_3[0],&l_826,(void*)0},{(void*)0,&l_444,&g_369[1].f1,(void*)0,&l_444,&l_826,&l_826},{&g_3[0],&l_444,&l_829[6],&l_444,&g_3[0],&g_69.f1,(void*)0}};
        uint64_t l_848 = 0x8D58F1EA9390B91FLL;
        int i, j, k;
        (*l_794) = (-(safe_add_func_float_f_f((-0x6.Ep+1), (((**l_522) = (p_52 >= ((*g_472) = l_792[2]))) > p_52))));
        (*l_825) |= ((safe_mod_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((*l_824) |= (((safe_div_func_int16_t_s_s((safe_add_func_uint64_t_u_u((safe_add_func_int32_t_s_s((((*l_823) = (safe_lshift_func_uint16_t_u_s(((((0xE687L && g_127) || (safe_lshift_func_uint16_t_u_u((safe_div_func_uint64_t_u_u(g_311.f1, g_309[0].f1)), ((*l_822) = (0x95DA4834667D3430LL > ((safe_mul_func_uint16_t_u_u(((*l_813)++), (0x9859BACDL & (safe_div_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(1UL, (((+g_206) != (((g_383 == l_821[1][0][6]) != p_52) > l_792[3])) & (-1L)))), (-3L)))))) & 0x6DL)))))) & (**l_522)) & p_52), 1))) , (*l_786)), l_792[2])), g_310[3][0].f0)), 0xC3F3L)) && g_662.f1) | 0x83L)), p_52)), p_52)) & 65535UL);
        (**l_522) = 0x34445434L;
        l_848--;
    }
    return &g_14;
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_105 g_107 g_3 g_108 g_100 g_17 g_23 g_129 g_139 g_127 g_69.f3 g_138 g_32 g_131.f0 g_176.f0 g_67 g_131.f1 g_69.f1 g_196 g_202 g_210 g_69.f0 g_176.f1 g_201 g_351 g_309.f1 g_315.f1 g_263 g_418 g_131.f3
 * writes: g_67 g_100 g_105 g_127 g_139 g_69.f3 g_32 g_131.f1 g_176.f3 g_69.f1 g_196 g_202 g_210 g_108 g_227 g_229 g_231 g_69.f0 g_129 g_176.f1 g_418 g_131.f3
 */
static int32_t ** func_56(uint16_t  p_57, uint32_t  p_58, int64_t  p_59, int32_t * p_60)
{ /* block id: 21 */
    int32_t ** const l_65 = &g_14;
    uint64_t *l_66[4][6] = {{&g_67[3],&g_67[3],&g_67[3],&g_67[3],&g_67[3],&g_67[3]},{&g_67[3],&g_67[3],&g_67[3],&g_67[3],(void*)0,&g_67[3]},{&g_67[3],(void*)0,&g_67[3],&g_67[3],&g_67[3],&g_67[3]},{&g_67[3],&g_67[3],&g_67[3],&g_67[3],&g_67[3],&g_67[3]}};
    int32_t l_395 = 0x2BD38370L;
    int16_t l_409 = 0x7C74L;
    int8_t *l_410[10][9][1] = {{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_23}},{{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23}},{{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23}},{{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23}},{{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23}},{{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23},{&g_127},{&g_127},{&g_23}}};
    int32_t l_411 = 0x7B77027AL;
    const int32_t l_412[3][10] = {{8L,0x3CBE16C7L,0xCEB195B1L,0xCEB195B1L,0x3CBE16C7L,8L,0x3CBE16C7L,0xCEB195B1L,0xCEB195B1L,0x3CBE16C7L},{8L,0x3CBE16C7L,0xCEB195B1L,0xCEB195B1L,0x3CBE16C7L,8L,0x3CBE16C7L,0xCEB195B1L,0xCEB195B1L,0x3CBE16C7L},{8L,0x3CBE16C7L,0xCEB195B1L,0xCEB195B1L,0x3CBE16C7L,8L,0x3CBE16C7L,0xCEB195B1L,0xCEB195B1L,0x3CBE16C7L}};
    int32_t *l_413 = &g_369[1].f1;
    int32_t *l_414 = &g_369[1].f1;
    int32_t *l_415 = (void*)0;
    int32_t *l_416 = &g_369[1].f1;
    int32_t *l_417[1];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_417[i] = (void*)0;
    g_418 &= (0x6D0CL | ((func_62(l_65, (g_67[3] = p_58)) && ((safe_div_func_uint64_t_u_u((safe_div_func_int8_t_s_s((((((l_395 || ((*g_107)--)) > (safe_lshift_func_uint8_t_u_s(p_58, (safe_sub_func_int8_t_s_s(g_309[0].f1, (safe_mul_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_s((~l_409), 1)), 6)) | ((((l_411 ^= l_409) < p_57) , 4294967288UL) < g_23)), g_315.f1))))))) & p_57) == l_395) == g_263[3]), 252UL)), l_409)) > l_412[2][1])) < 5UL));
    for (g_131.f3 = (-26); (g_131.f3 > (-14)); g_131.f3++)
    { /* block id: 187 */
        return &g_258;
    }
    return &g_258;
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_105 g_107 g_3 g_108 g_100 g_17 g_23 g_129 g_139 g_127 g_69.f3 g_138 g_32 g_131.f0 g_176.f0 g_67 g_131.f1 g_69.f1 g_196 g_202 g_210 g_69.f0 g_176.f1 g_201 g_351
 * writes: g_100 g_105 g_127 g_139 g_69.f3 g_32 g_131.f1 g_176.f3 g_69.f1 g_196 g_202 g_210 g_108 g_227 g_229 g_231 g_69.f0 g_129 g_176.f1
 */
static uint16_t  func_62(int32_t ** const  p_63, uint64_t  p_64)
{ /* block id: 23 */
    union U4 *l_68 = &g_69;
    union U4 ***l_241 = &g_129[0][5];
    union U4 **l_243 = &g_130;
    union U4 ***l_242 = &l_243;
    int32_t l_245 = (-7L);
    int32_t l_249 = 1L;
    int32_t l_250[8][8][1] = {{{1L},{0L},{0xE061FFB3L},{1L},{1L},{(-1L)},{1L},{1L}},{{0xE061FFB3L},{0L},{1L},{(-1L)},{1L},{0L},{0xE061FFB3L},{1L}},{{1L},{(-1L)},{1L},{1L},{0xE061FFB3L},{0L},{1L},{(-1L)}},{{1L},{0L},{0xE061FFB3L},{1L},{1L},{(-1L)},{1L},{1L}},{{0xE061FFB3L},{0L},{1L},{(-1L)},{1L},{0L},{0xE061FFB3L},{1L}},{{1L},{(-1L)},{1L},{1L},{0xE061FFB3L},{0L},{1L},{(-1L)}},{{1L},{0L},{0xE061FFB3L},{1L},{1L},{(-1L)},{1L},{1L}},{{0xE061FFB3L},{0L},{1L},{(-1L)},{1L},{0L},{0xE061FFB3L},{1L}}};
    union U4 ** const ***l_363 = (void*)0;
    union U4 *l_368 = &g_369[1];
    union U4 ** const l_367 = &l_368;
    union U4 ** const *l_366 = &l_367;
    union U4 ** const **l_365 = &l_366;
    union U4 ** const ***l_364 = &l_365;
    uint64_t l_384 = 6UL;
    int i, j, k;
    l_68 = l_68;
    (*l_242) = ((*l_241) = func_70(l_68, &l_68));
    for (g_176.f1 = 5; (g_176.f1 >= 1); g_176.f1 -= 1)
    { /* block id: 101 */
        float *l_244 = &g_227;
        int32_t *l_246 = &g_69.f1;
        int32_t *l_247 = &l_245;
        int32_t *l_248[1];
        uint32_t l_251 = 0x9203A046L;
        int i;
        for (i = 0; i < 1; i++)
            l_248[i] = &l_245;
        (*l_244) = g_201[(g_176.f1 + 1)];
        --l_251;
    }
    for (g_127 = 0; (g_127 == (-29)); g_127--)
    { /* block id: 107 */
        int32_t l_260 = 0xE2FE3639L;
        union U4 ***l_304[6][2] = {{&l_243,&l_243},{&l_243,&l_243},{&l_243,&l_243},{&l_243,&l_243},{&l_243,&l_243},{&l_243,&l_243}};
        int32_t l_333 = 0x321D68B1L;
        int32_t l_338[3][6] = {{0x66DDD4FAL,0x09A7240CL,0x09A7240CL,0x66DDD4FAL,0x09A7240CL,0x09A7240CL},{0x66DDD4FAL,0x09A7240CL,0x09A7240CL,0x66DDD4FAL,0x09A7240CL,0x09A7240CL},{0x66DDD4FAL,0x09A7240CL,0x09A7240CL,0x66DDD4FAL,0x09A7240CL,0x09A7240CL}};
        int32_t l_340 = 0xECE6004FL;
        const union U4 ***l_373 = &g_94;
        const union U4 ****l_372 = &l_373;
        const union U4 *****l_371 = &l_372;
        int i, j;
        for (g_176.f1 = (-18); (g_176.f1 > 15); g_176.f1 = safe_add_func_uint64_t_u_u(g_176.f1, 6))
        { /* block id: 110 */
            uint64_t l_303 = 0UL;
            float *l_336[7] = {(void*)0,&g_227,&g_227,(void*)0,&g_227,&g_227,(void*)0};
            int32_t l_337 = 0L;
            const uint64_t l_339 = 0xECD709BD73319C66LL;
            int i;
        }
    }
    return g_351[3][5];
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_105 g_107 g_3 g_108 g_100 g_17 g_23 g_129 g_139 g_127 g_69.f3 g_138 g_32 g_131.f0 g_176.f0 g_67 g_131.f1 g_69.f1 g_196 g_202 g_210 g_69.f0 g_130
 * writes: g_100 g_105 g_127 g_139 g_69.f3 g_32 g_131.f1 g_176.f3 g_69.f1 g_196 g_202 g_210 g_108 g_227 g_229 g_231 g_69.f0
 */
static union U4 ** func_70(union U4 * const  p_71, union U4 * const * p_72)
{ /* block id: 25 */
    uint32_t l_75 = 4294967295UL;
    uint64_t *l_78[2][1];
    uint64_t **l_79 = &l_78[0][0];
    uint64_t *l_80[10];
    uint64_t **l_81 = (void*)0;
    uint64_t **l_82 = &l_80[4];
    const union U4 **l_96 = &g_95;
    union U4 *l_98 = (void*)0;
    union U4 **l_97 = &l_98;
    uint8_t *l_99 = &g_100;
    const uint8_t **l_106 = &g_105;
    uint8_t **l_109[10] = {&g_107,&g_107,&g_107,&g_107,&g_107,&g_107,&g_107,&g_107,&g_107,&g_107};
    uint8_t *l_110 = &g_108[2];
    int64_t l_111[8] = {0xEB3E32E7287D282ELL,0xEB3E32E7287D282ELL,0xFF1AB120DA2E755ELL,(-1L),(-1L),0xEB3E32E7287D282ELL,(-1L),(-1L)};
    int32_t l_112[9][8][1] = {{{0x5D99DD6BL},{(-2L)},{(-2L)},{0x5D99DD6BL},{0x49E4C9CDL},{5L},{(-1L)},{0L}},{{(-1L)},{0x5D99DD6BL},{(-1L)},{(-1L)},{0x5D99DD6BL},{0xCA57EF93L},{0xF6E8DA2FL},{(-1L)}},{{0xF6E8DA2FL},{0xCA57EF93L},{0x5D99DD6BL},{(-1L)},{(-1L)},{0x5D99DD6BL},{(-1L)},{0L}},{{(-1L)},{5L},{0x49E4C9CDL},{0x5D99DD6BL},{(-2L)},{(-2L)},{0x5D99DD6BL},{0x49E4C9CDL}},{{5L},{(-1L)},{0L},{(-1L)},{0x5D99DD6BL},{(-1L)},{(-1L)},{0x5D99DD6BL}},{{0xCA57EF93L},{0xF6E8DA2FL},{(-1L)},{0xF6E8DA2FL},{0xCA57EF93L},{0x5D99DD6BL},{(-1L)},{(-1L)}},{{0x5D99DD6BL},{(-1L)},{0L},{(-1L)},{5L},{0x49E4C9CDL},{0x5D99DD6BL},{(-2L)}},{{(-2L)},{0x5D99DD6BL},{0x49E4C9CDL},{5L},{(-1L)},{0L},{(-1L)},{0x5D99DD6BL}},{{(-1L)},{(-1L)},{0x5D99DD6BL},{0xCA57EF93L},{0xF6E8DA2FL},{(-1L)},{0xF6E8DA2FL},{0xCA57EF93L}}};
    int32_t *l_123 = &g_69.f1;
    int32_t l_128 = (-7L);
    int32_t *l_132 = (void*)0;
    int32_t *l_133 = &g_131.f1;
    int32_t *l_134 = &g_131.f1;
    int32_t *l_135 = &l_112[6][0][0];
    int32_t *l_136[6][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_112[3][7][0],(void*)0,&l_112[8][6][0]},{(void*)0,&l_128,&g_17,(void*)0,&l_112[8][6][0],(void*)0,(void*)0,&l_112[8][6][0],(void*)0},{&g_17,&l_112[1][6][0],&g_17,&g_17,(void*)0,(void*)0,(void*)0,&l_128,(void*)0},{&l_112[1][6][0],&g_17,&l_112[8][6][0],&l_112[3][7][0],(void*)0,&l_128,(void*)0,&l_112[3][7][0],&l_112[8][6][0]},{(void*)0,(void*)0,&l_112[1][6][0],&g_17,&l_112[1][6][0],&g_17,&l_112[8][6][0],&l_112[3][7][0],(void*)0},{(void*)0,(void*)0,&l_128,(void*)0,&l_112[1][6][0],&l_112[1][6][0],(void*)0,&l_128,(void*)0}};
    int64_t l_137 = 0x464C473E6E224C1BLL;
    int32_t **l_240 = &l_136[0][7];
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_78[i][j] = &g_67[3];
    }
    for (i = 0; i < 10; i++)
        l_80[i] = &g_67[1];
    if (((safe_add_func_int16_t_s_s(l_75, (safe_rshift_func_int16_t_s_s((((*l_79) = l_78[1][0]) != ((*l_82) = l_80[0])), (safe_lshift_func_int8_t_s_s(((((safe_rshift_func_uint8_t_u_u((l_112[1][6][0] = (safe_mul_func_int8_t_s_s((safe_add_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((!(((*l_99) = ((l_96 = g_94) != (l_97 = l_97))) > (safe_sub_func_int16_t_s_s((((safe_sub_func_int32_t_s_s((((*l_106) = g_105) != (l_110 = g_107)), l_75)) , (0xF4L >= l_75)) , l_75), 0x6AD3L)))) <= 0x1114L), l_75)), l_75)), l_111[3]))), 5)) , &g_23) == (void*)0) == l_111[7]), l_111[3])))))) | 9UL))
    { /* block id: 34 */
        int32_t *l_120 = &g_3[0];
        int32_t **l_121 = (void*)0;
        int32_t **l_122[4];
        int8_t *l_126[8][9][1] = {{{&g_23},{(void*)0},{&g_127},{&g_23},{&g_127},{&g_23},{&g_127},{(void*)0},{&g_23}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_23},{(void*)0},{&g_127},{&g_23},{&g_127}},{{&g_23},{&g_127},{(void*)0},{&g_23},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_23}},{{(void*)0},{&g_127},{&g_23},{&g_127},{&g_23},{&g_127},{(void*)0},{&g_23},{&g_127}},{{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_127},{&g_23},{&g_127},{&g_23}},{{&g_23},{&g_23},{&g_127},{&g_127},{&g_127},{&g_127},{(void*)0},{&g_127},{(void*)0}}};
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_122[i] = &l_120;
        l_112[3][4][0] = (0x5BF18921B67715EELL <= (((safe_add_func_int32_t_s_s((l_112[1][6][0] >= (+((safe_mul_func_int8_t_s_s(0x59L, (g_127 = (g_3[1] & ((((safe_rshift_func_int8_t_s_u(((l_123 = l_120) != (l_111[6] , (((l_128 ^= ((0x50F4L == l_112[1][6][0]) != (((safe_add_func_int16_t_s_s(g_108[0], g_100)) , l_112[1][6][0]) | g_17))) < (*l_120)) , &l_112[1][6][0]))), 2)) , g_100) || (*l_123)) | g_108[5]))))) & 6UL))), 7UL)) == 0x6BL) >= g_23));
    }
    else
    { /* block id: 39 */
        return g_129[1][5];
    }
    ++g_139;
    for (g_127 = 0; (g_127 < 26); ++g_127)
    { /* block id: 45 */
        int32_t l_150 = 1L;
        const uint64_t l_163 = 9UL;
        union U4 *l_175 = &g_176;
        int32_t l_199 = 1L;
        int8_t l_205 = 1L;
        int32_t l_207 = 0L;
        int32_t l_209[4];
        float *l_226 = &g_227;
        float *l_228 = &g_229;
        float *l_230 = &g_231[0];
        int i;
        for (i = 0; i < 4; i++)
            l_209[i] = (-1L);
        for (l_137 = 0; (l_137 == (-25)); --l_137)
        { /* block id: 48 */
            uint64_t ** const l_151 = (void*)0;
            int32_t l_173 = 0x1478FA29L;
            uint16_t *l_174 = &g_32[2];
            int32_t l_177 = 0x019ACE6FL;
            for (g_69.f3 = (-30); (g_69.f3 > 5); g_69.f3 = safe_add_func_int64_t_s_s(g_69.f3, 3))
            { /* block id: 51 */
                return &g_130;
            }
            l_177 |= (((safe_mul_func_int8_t_s_s(0x9AL, (((l_150 , &l_80[0]) == l_151) > (g_100 && (((safe_sub_func_int64_t_s_s(((+((0xE5F79893L >= (safe_div_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(((((safe_sub_func_int16_t_s_s((g_138[0][7][0] , l_163), (safe_add_func_uint8_t_u_u(((+((safe_lshift_func_uint8_t_u_u(((((*l_135) = (((safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u(((((*l_174) ^= l_173) && 4UL) || (*g_107)), (*l_123))), l_163)) , l_175) == (*p_72))) & l_163) ^ g_131.f0), l_150)) >= 7UL)) & 0x73D5L), l_163)))) & 0xFA1E05A8L) & l_150) & 0L), g_176.f0)), g_138[6][2][2]))) , l_173)) , l_173), g_67[3])) >= l_173) ^ g_17))))) && g_127) != l_173);
            for (g_131.f1 = 0; (g_131.f1 <= 6); g_131.f1 += 1)
            { /* block id: 59 */
                int16_t l_185 = (-10L);
                int32_t l_193 = 0xD564FFCEL;
                for (l_173 = 0; (l_173 <= 6); l_173 += 1)
                { /* block id: 62 */
                    int32_t l_200 = 0L;
                    int i;
                    if (g_108[l_173])
                        break;
                    for (g_176.f3 = 7; (g_176.f3 >= 0); g_176.f3 -= 1)
                    { /* block id: 66 */
                        int32_t l_194 = 0xE84944C1L;
                        int16_t l_195 = 0xE5F5L;
                        g_69.f1 = (safe_mod_func_uint16_t_u_u((+(0xFCEB201E952823A5LL && (5L <= (safe_mul_func_uint8_t_u_u((((safe_add_func_uint8_t_u_u(0xECL, l_185)) || (((safe_mul_func_uint8_t_u_u(((*l_134) ^ ((*l_135) = g_108[l_173])), (safe_lshift_func_int16_t_s_u(0x7AC8L, 14)))) || ((~(l_193 = g_108[l_173])) , ((l_194 = (-9L)) != l_195))) | g_108[l_173])) && 1L), 0xE5L))))), g_69.f1));
                        g_196[0]--;
                        g_202++;
                    }
                }
                (*l_135) = l_173;
            }
        }
        g_210--;
        (*l_230) = ((*l_228) = (g_196[3] , ((*l_226) = ((safe_mul_func_float_f_f(0x6.4p+1, (safe_mul_func_float_f_f((safe_div_func_float_f_f((*l_123), l_199)), ((safe_sub_func_float_f_f(((safe_mul_func_float_f_f(((g_176.f0 < ((((*g_107) = (+g_131.f0)) > ((((((void*)0 != &g_14) || ((0x0815C885L == 0xCACB046FL) ^ 0UL)) | 1L) == (*l_135)) == (*l_123))) , g_69.f0)) == l_163), l_209[1])) >= 0x6.BCCA87p-56), 0xC.8718FDp+51)) < (-0x1.8p-1)))))) <= 0x7.ADE7F3p-89))));
        for (g_69.f0 = (-10); (g_69.f0 != 40); g_69.f0 = safe_add_func_uint16_t_u_u(g_69.f0, 5))
        { /* block id: 85 */
            int64_t l_236 = 0x5F32683604912DCDLL;
            for (g_139 = 0; (g_139 <= 44); ++g_139)
            { /* block id: 88 */
                uint32_t l_237 = 0xCAB300E6L;
                l_237--;
                (*l_135) |= ((*l_134) = 0x7221A5A3L);
            }
        }
    }
    (*l_240) = &l_112[1][6][0];
    return &g_130;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_3[i], "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_15[i], "g_15[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_16[i][j][k], "g_16[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_17, "g_17", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_32[i], "g_32[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_67[i], "g_67[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_69.f0, "g_69.f0", print_hash_value);
    transparent_crc(g_69.f2, "g_69.f2", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_108[i], "g_108[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_131.f0, "g_131.f0", print_hash_value);
    transparent_crc(g_131.f2, "g_131.f2", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_138[i][j][k], "g_138[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_139, "g_139", print_hash_value);
    transparent_crc(g_176.f0, "g_176.f0", print_hash_value);
    transparent_crc(g_176.f2, "g_176.f2", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_196[i], "g_196[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_201[i], "g_201[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_202, "g_202", print_hash_value);
    transparent_crc(g_206, "g_206", print_hash_value);
    transparent_crc(g_208, "g_208", print_hash_value);
    transparent_crc(g_210, "g_210", print_hash_value);
    transparent_crc_bytes (&g_227, sizeof(g_227), "g_227", print_hash_value);
    transparent_crc_bytes (&g_229, sizeof(g_229), "g_229", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_231[i], sizeof(g_231[i]), "g_231[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_263[i], "g_263[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_276, "g_276", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_307[i][j][k].f0, "g_307[i][j][k].f0", print_hash_value);
                transparent_crc(g_307[i][j][k].f1, "g_307[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_308[i][j].f0, "g_308[i][j].f0", print_hash_value);
            transparent_crc(g_308[i][j].f1, "g_308[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_309[i].f0, "g_309[i].f0", print_hash_value);
        transparent_crc(g_309[i].f1, "g_309[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_310[i][j].f0, "g_310[i][j].f0", print_hash_value);
            transparent_crc(g_310[i][j].f1, "g_310[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_311.f0, "g_311.f0", print_hash_value);
    transparent_crc(g_311.f1, "g_311.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_312[i].f0, "g_312[i].f0", print_hash_value);
        transparent_crc(g_312[i].f1, "g_312[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_313.f0, "g_313.f0", print_hash_value);
    transparent_crc(g_313.f1, "g_313.f1", print_hash_value);
    transparent_crc(g_315.f0, "g_315.f0", print_hash_value);
    transparent_crc(g_315.f1, "g_315.f1", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_343[i][j], "g_343[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_351[i][j], "g_351[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_369[i].f0, "g_369[i].f0", print_hash_value);
        transparent_crc(g_369[i].f2, "g_369[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_383, "g_383", print_hash_value);
    transparent_crc(g_387, "g_387", print_hash_value);
    transparent_crc(g_418, "g_418", print_hash_value);
    transparent_crc_bytes (&g_449, sizeof(g_449), "g_449", print_hash_value);
    transparent_crc(g_539, "g_539", print_hash_value);
    transparent_crc(g_541, "g_541", print_hash_value);
    transparent_crc(g_599, "g_599", print_hash_value);
    transparent_crc(g_603.f0, "g_603.f0", print_hash_value);
    transparent_crc(g_603.f2, "g_603.f2", print_hash_value);
    transparent_crc(g_659.f0, "g_659.f0", print_hash_value);
    transparent_crc(g_659.f1, "g_659.f1", print_hash_value);
    transparent_crc(g_659.f2, "g_659.f2", print_hash_value);
    transparent_crc(g_659.f3, "g_659.f3", print_hash_value);
    transparent_crc(g_659.f4, "g_659.f4", print_hash_value);
    transparent_crc(g_662.f0, "g_662.f0", print_hash_value);
    transparent_crc(g_662.f1, "g_662.f1", print_hash_value);
    transparent_crc(g_662.f2, "g_662.f2", print_hash_value);
    transparent_crc(g_662.f3, "g_662.f3", print_hash_value);
    transparent_crc(g_662.f4, "g_662.f4", print_hash_value);
    transparent_crc(g_859.f0, "g_859.f0", print_hash_value);
    transparent_crc(g_859.f1, "g_859.f1", print_hash_value);
    transparent_crc(g_859.f2, "g_859.f2", print_hash_value);
    transparent_crc(g_859.f3, "g_859.f3", print_hash_value);
    transparent_crc(g_859.f4, "g_859.f4", print_hash_value);
    transparent_crc(g_859.f5, "g_859.f5", print_hash_value);
    transparent_crc(g_859.f6, "g_859.f6", print_hash_value);
    transparent_crc(g_1018.f0, "g_1018.f0", print_hash_value);
    transparent_crc(g_1018.f2, "g_1018.f2", print_hash_value);
    transparent_crc(g_1118, "g_1118", print_hash_value);
    transparent_crc(g_1119, "g_1119", print_hash_value);
    transparent_crc(g_1120, "g_1120", print_hash_value);
    transparent_crc(g_1121, "g_1121", print_hash_value);
    transparent_crc(g_1167, "g_1167", print_hash_value);
    transparent_crc(g_1232, "g_1232", print_hash_value);
    transparent_crc(g_1333.f0, "g_1333.f0", print_hash_value);
    transparent_crc(g_1333.f1, "g_1333.f1", print_hash_value);
    transparent_crc(g_1333.f2, "g_1333.f2", print_hash_value);
    transparent_crc(g_1333.f3, "g_1333.f3", print_hash_value);
    transparent_crc(g_1354, "g_1354", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1365[i], "g_1365[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1400, "g_1400", print_hash_value);
    transparent_crc(g_1401, "g_1401", print_hash_value);
    transparent_crc(g_1402, "g_1402", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1406[i][j], "g_1406[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1411.f0, "g_1411.f0", print_hash_value);
    transparent_crc(g_1411.f1, "g_1411.f1", print_hash_value);
    transparent_crc(g_1411.f2, "g_1411.f2", print_hash_value);
    transparent_crc(g_1411.f3, "g_1411.f3", print_hash_value);
    transparent_crc(g_1411.f4, "g_1411.f4", print_hash_value);
    transparent_crc(g_1411.f5, "g_1411.f5", print_hash_value);
    transparent_crc(g_1411.f6, "g_1411.f6", print_hash_value);
    transparent_crc(g_1445.f0, "g_1445.f0", print_hash_value);
    transparent_crc(g_1445.f1, "g_1445.f1", print_hash_value);
    transparent_crc(g_1445.f2, "g_1445.f2", print_hash_value);
    transparent_crc(g_1445.f3, "g_1445.f3", print_hash_value);
    transparent_crc(g_1446.f0, "g_1446.f0", print_hash_value);
    transparent_crc(g_1446.f1, "g_1446.f1", print_hash_value);
    transparent_crc(g_1446.f2, "g_1446.f2", print_hash_value);
    transparent_crc(g_1446.f3, "g_1446.f3", print_hash_value);
    transparent_crc(g_1447.f0, "g_1447.f0", print_hash_value);
    transparent_crc(g_1447.f1, "g_1447.f1", print_hash_value);
    transparent_crc(g_1447.f2, "g_1447.f2", print_hash_value);
    transparent_crc(g_1447.f3, "g_1447.f3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1524[i][j][k].f0, "g_1524[i][j][k].f0", print_hash_value);
                transparent_crc(g_1524[i][j][k].f1, "g_1524[i][j][k].f1", print_hash_value);
                transparent_crc(g_1524[i][j][k].f2, "g_1524[i][j][k].f2", print_hash_value);
                transparent_crc(g_1524[i][j][k].f3, "g_1524[i][j][k].f3", print_hash_value);
                transparent_crc(g_1524[i][j][k].f4, "g_1524[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_1550, sizeof(g_1550), "g_1550", print_hash_value);
    transparent_crc(g_1566.f0, "g_1566.f0", print_hash_value);
    transparent_crc(g_1566.f2, "g_1566.f2", print_hash_value);
    transparent_crc(g_1568.f0, "g_1568.f0", print_hash_value);
    transparent_crc(g_1568.f2, "g_1568.f2", print_hash_value);
    transparent_crc(g_1570.f0, "g_1570.f0", print_hash_value);
    transparent_crc(g_1570.f2, "g_1570.f2", print_hash_value);
    transparent_crc(g_1578.f0, "g_1578.f0", print_hash_value);
    transparent_crc(g_1578.f2, "g_1578.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1586[i].f0, "g_1586[i].f0", print_hash_value);
        transparent_crc(g_1586[i].f1, "g_1586[i].f1", print_hash_value);
        transparent_crc(g_1586[i].f2, "g_1586[i].f2", print_hash_value);
        transparent_crc(g_1586[i].f3, "g_1586[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1603, "g_1603", print_hash_value);
    transparent_crc(g_1671, "g_1671", print_hash_value);
    transparent_crc(g_1726.f0, "g_1726.f0", print_hash_value);
    transparent_crc(g_1726.f2, "g_1726.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1728[i][j].f0, "g_1728[i][j].f0", print_hash_value);
            transparent_crc(g_1728[i][j].f2, "g_1728[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1732, "g_1732", print_hash_value);
    transparent_crc(g_1768, "g_1768", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1769[i][j], "g_1769[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1772, "g_1772", print_hash_value);
    transparent_crc(g_1774, "g_1774", print_hash_value);
    transparent_crc(g_1776, "g_1776", print_hash_value);
    transparent_crc(g_1777, "g_1777", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_1778[i][j], "g_1778[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1780, "g_1780", print_hash_value);
    transparent_crc(g_1856.f0, "g_1856.f0", print_hash_value);
    transparent_crc(g_1856.f1, "g_1856.f1", print_hash_value);
    transparent_crc(g_1856.f2, "g_1856.f2", print_hash_value);
    transparent_crc(g_1856.f3, "g_1856.f3", print_hash_value);
    transparent_crc(g_1856.f4, "g_1856.f4", print_hash_value);
    transparent_crc(g_1859.f0, "g_1859.f0", print_hash_value);
    transparent_crc(g_1859.f1, "g_1859.f1", print_hash_value);
    transparent_crc(g_1859.f2, "g_1859.f2", print_hash_value);
    transparent_crc(g_1859.f3, "g_1859.f3", print_hash_value);
    transparent_crc(g_1923.f0, "g_1923.f0", print_hash_value);
    transparent_crc(g_1923.f1, "g_1923.f1", print_hash_value);
    transparent_crc(g_1923.f2, "g_1923.f2", print_hash_value);
    transparent_crc(g_1923.f3, "g_1923.f3", print_hash_value);
    transparent_crc(g_1947.f0, "g_1947.f0", print_hash_value);
    transparent_crc(g_1947.f1, "g_1947.f1", print_hash_value);
    transparent_crc(g_1947.f2, "g_1947.f2", print_hash_value);
    transparent_crc(g_1947.f3, "g_1947.f3", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1967[i].f0, "g_1967[i].f0", print_hash_value);
        transparent_crc(g_1967[i].f2, "g_1967[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1975[i].f0, "g_1975[i].f0", print_hash_value);
        transparent_crc(g_1975[i].f1, "g_1975[i].f1", print_hash_value);
        transparent_crc(g_1975[i].f2, "g_1975[i].f2", print_hash_value);
        transparent_crc(g_1975[i].f3, "g_1975[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2040.f0, "g_2040.f0", print_hash_value);
    transparent_crc(g_2040.f2, "g_2040.f2", print_hash_value);
    transparent_crc(g_2086.f0, "g_2086.f0", print_hash_value);
    transparent_crc(g_2086.f1, "g_2086.f1", print_hash_value);
    transparent_crc(g_2086.f2, "g_2086.f2", print_hash_value);
    transparent_crc(g_2086.f3, "g_2086.f3", print_hash_value);
    transparent_crc(g_2086.f4, "g_2086.f4", print_hash_value);
    transparent_crc(g_2094.f0, "g_2094.f0", print_hash_value);
    transparent_crc(g_2094.f1, "g_2094.f1", print_hash_value);
    transparent_crc(g_2094.f2, "g_2094.f2", print_hash_value);
    transparent_crc(g_2094.f3, "g_2094.f3", print_hash_value);
    transparent_crc(g_2094.f4, "g_2094.f4", print_hash_value);
    transparent_crc(g_2100.f0, "g_2100.f0", print_hash_value);
    transparent_crc(g_2100.f2, "g_2100.f2", print_hash_value);
    transparent_crc(g_2106.f0, "g_2106.f0", print_hash_value);
    transparent_crc(g_2106.f1, "g_2106.f1", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2125[i], "g_2125[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2139, "g_2139", print_hash_value);
    transparent_crc(g_2191.f0, "g_2191.f0", print_hash_value);
    transparent_crc(g_2191.f1, "g_2191.f1", print_hash_value);
    transparent_crc(g_2191.f2, "g_2191.f2", print_hash_value);
    transparent_crc(g_2191.f3, "g_2191.f3", print_hash_value);
    transparent_crc(g_2191.f4, "g_2191.f4", print_hash_value);
    transparent_crc(g_2209.f0, "g_2209.f0", print_hash_value);
    transparent_crc(g_2209.f2, "g_2209.f2", print_hash_value);
    transparent_crc(g_2239, "g_2239", print_hash_value);
    transparent_crc(g_2244.f0, "g_2244.f0", print_hash_value);
    transparent_crc(g_2244.f1, "g_2244.f1", print_hash_value);
    transparent_crc(g_2244.f2, "g_2244.f2", print_hash_value);
    transparent_crc(g_2244.f3, "g_2244.f3", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_2283[i].f0, "g_2283[i].f0", print_hash_value);
        transparent_crc(g_2283[i].f1, "g_2283[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2290.f0, "g_2290.f0", print_hash_value);
    transparent_crc(g_2290.f1, "g_2290.f1", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2291[i], "g_2291[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2296[i][j].f0, "g_2296[i][j].f0", print_hash_value);
            transparent_crc(g_2296[i][j].f1, "g_2296[i][j].f1", print_hash_value);
            transparent_crc(g_2296[i][j].f2, "g_2296[i][j].f2", print_hash_value);
            transparent_crc(g_2296[i][j].f3, "g_2296[i][j].f3", print_hash_value);
            transparent_crc(g_2296[i][j].f4, "g_2296[i][j].f4", print_hash_value);
            transparent_crc(g_2296[i][j].f5, "g_2296[i][j].f5", print_hash_value);
            transparent_crc(g_2296[i][j].f6, "g_2296[i][j].f6", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2303.f0, "g_2303.f0", print_hash_value);
    transparent_crc(g_2303.f1, "g_2303.f1", print_hash_value);
    transparent_crc(g_2309.f0, "g_2309.f0", print_hash_value);
    transparent_crc(g_2309.f2, "g_2309.f2", print_hash_value);
    transparent_crc(g_2396.f0, "g_2396.f0", print_hash_value);
    transparent_crc(g_2396.f1, "g_2396.f1", print_hash_value);
    transparent_crc(g_2396.f2, "g_2396.f2", print_hash_value);
    transparent_crc(g_2396.f3, "g_2396.f3", print_hash_value);
    transparent_crc(g_2396.f4, "g_2396.f4", print_hash_value);
    transparent_crc(g_2396.f5, "g_2396.f5", print_hash_value);
    transparent_crc(g_2396.f6, "g_2396.f6", print_hash_value);
    transparent_crc(g_2399.f0, "g_2399.f0", print_hash_value);
    transparent_crc(g_2399.f1, "g_2399.f1", print_hash_value);
    transparent_crc(g_2399.f2, "g_2399.f2", print_hash_value);
    transparent_crc(g_2399.f3, "g_2399.f3", print_hash_value);
    transparent_crc(g_2399.f4, "g_2399.f4", print_hash_value);
    transparent_crc(g_2404.f0, "g_2404.f0", print_hash_value);
    transparent_crc(g_2404.f1, "g_2404.f1", print_hash_value);
    transparent_crc(g_2404.f2, "g_2404.f2", print_hash_value);
    transparent_crc(g_2404.f3, "g_2404.f3", print_hash_value);
    transparent_crc(g_2404.f4, "g_2404.f4", print_hash_value);
    transparent_crc(g_2413.f0, "g_2413.f0", print_hash_value);
    transparent_crc(g_2413.f1, "g_2413.f1", print_hash_value);
    transparent_crc(g_2413.f2, "g_2413.f2", print_hash_value);
    transparent_crc(g_2413.f3, "g_2413.f3", print_hash_value);
    transparent_crc(g_2413.f4, "g_2413.f4", print_hash_value);
    transparent_crc(g_2440.f0, "g_2440.f0", print_hash_value);
    transparent_crc(g_2440.f1, "g_2440.f1", print_hash_value);
    transparent_crc(g_2440.f2, "g_2440.f2", print_hash_value);
    transparent_crc(g_2440.f3, "g_2440.f3", print_hash_value);
    transparent_crc(g_2440.f4, "g_2440.f4", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 583
   depth: 1, occurrence: 10
XXX total union variables: 12

XXX non-zero bitfields defined in structs: 14
XXX zero bitfields defined in structs: 2
XXX const bitfields defined in structs: 2
XXX volatile bitfields defined in structs: 2
XXX structs with bitfields in the program: 41
breakdown:
   indirect level: 0, occurrence: 14
   indirect level: 1, occurrence: 17
   indirect level: 2, occurrence: 4
   indirect level: 3, occurrence: 6
XXX full-bitfields structs in the program: 8
breakdown:
   indirect level: 0, occurrence: 8
XXX times a bitfields struct's address is taken: 17
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 15
XXX times a single bitfield on LHS: 4
XXX times a single bitfield on RHS: 25

XXX max expression depth: 36
breakdown:
   depth: 1, occurrence: 225
   depth: 2, occurrence: 64
   depth: 3, occurrence: 2
   depth: 4, occurrence: 4
   depth: 6, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 1
   depth: 18, occurrence: 5
   depth: 19, occurrence: 5
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 3
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1

XXX total number of pointers: 609

XXX times a variable address is taken: 1229
XXX times a pointer is dereferenced on RHS: 210
breakdown:
   depth: 1, occurrence: 183
   depth: 2, occurrence: 22
   depth: 3, occurrence: 5
XXX times a pointer is dereferenced on LHS: 253
breakdown:
   depth: 1, occurrence: 240
   depth: 2, occurrence: 9
   depth: 3, occurrence: 2
   depth: 4, occurrence: 2
XXX times a pointer is compared with null: 38
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 8365

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1191
   level: 2, occurrence: 183
   level: 3, occurrence: 60
   level: 4, occurrence: 31
   level: 5, occurrence: 13
XXX number of pointers point to pointers: 216
XXX number of pointers point to scalars: 352
XXX number of pointers point to structs: 14
XXX percent of pointers has null in alias set: 28.2
XXX average alias set size: 1.47

XXX times a non-volatile is read: 1636
XXX times a non-volatile is write: 857
XXX times a volatile is read: 36
XXX    times read thru a pointer: 9
XXX times a volatile is write: 10
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 3.42e+03
XXX percentage of non-volatile access: 98.2

XXX forward jumps: 2
XXX backward jumps: 7

XXX stmts: 223
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 43
   depth: 2, occurrence: 43
   depth: 3, occurrence: 30
   depth: 4, occurrence: 39
   depth: 5, occurrence: 34

XXX percentage a fresh-made variable is used: 18
XXX percentage an existing variable is used: 82
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


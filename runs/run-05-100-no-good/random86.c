/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      110308037
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   signed f0 : 1;
   signed f1 : 20;
};

union U1 {
   const volatile int32_t  f0;
};

union U2 {
   int32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 0L;
static int8_t g_7[2][2][5] = {{{1L,1L,1L,1L,1L},{0xBFL,0xBFL,0xBFL,0xBFL,0xBFL}},{{1L,1L,1L,1L,1L},{0xBFL,0xBFL,0xBFL,0xBFL,0xBFL}}};
static int8_t g_8[8] = {0x3DL,0x3DL,0x3DL,0x3DL,0x3DL,0x3DL,0x3DL,0x3DL};
static int32_t g_11 = 0x0452A26FL;
static volatile uint8_t g_20 = 0x2FL;/* VOLATILE GLOBAL g_20 */
static volatile uint32_t g_43[3][4][10] = {{{0xD1A68408L,0xF3F85024L,0x69002FCEL,0xB5B06A83L,7UL,0UL,0UL,7UL,0xB5B06A83L,0x69002FCEL},{0xD1A68408L,0xD1A68408L,0UL,0xF3F85024L,18446744073709551615UL,7UL,0x69002FCEL,1UL,0xB5B06A83L,0xB5B06A83L},{7UL,0x69002FCEL,1UL,0xB5B06A83L,0xB5B06A83L,1UL,0x69002FCEL,7UL,18446744073709551615UL,0xF3F85024L},{18446744073709551615UL,0xD1A68408L,0xF3F85024L,0x69002FCEL,0xB5B06A83L,7UL,0UL,0UL,7UL,0xB5B06A83L}},{{0xB5B06A83L,0xF3F85024L,0xF3F85024L,0xB5B06A83L,18446744073709551615UL,0UL,1UL,7UL,0xD1A68408L,0x69002FCEL},{0xB5B06A83L,0xD1A68408L,1UL,0xF3F85024L,7UL,7UL,0xF3F85024L,1UL,0xD1A68408L,0xB5B06A83L},{18446744073709551615UL,0x69002FCEL,0UL,0xB5B06A83L,0xD1A68408L,1UL,0xF3F85024L,7UL,7UL,0xF3F85024L},{7UL,0xD1A68408L,0x69002FCEL,0x69002FCEL,0xD1A68408L,7UL,1UL,0UL,18446744073709551615UL,0xB5B06A83L}},{{0xD1A68408L,0xF3F85024L,0x69002FCEL,0xB5B06A83L,7UL,0UL,0UL,7UL,0xB5B06A83L,0x69002FCEL},{0xD1A68408L,0xD1A68408L,0UL,0xF3F85024L,18446744073709551615UL,7UL,0x69002FCEL,1UL,0xB5B06A83L,0xB5B06A83L},{7UL,0x69002FCEL,1UL,0xB5B06A83L,0xB5B06A83L,1UL,0x69002FCEL,7UL,18446744073709551615UL,0xF3F85024L},{18446744073709551615UL,0xD1A68408L,0xF3F85024L,0x69002FCEL,0xB5B06A83L,7UL,0UL,0UL,7UL,0xB5B06A83L}}};
static union U1 g_46 = {1L};/* VOLATILE GLOBAL g_46 */
static const volatile struct S0 g_52 = {-0,-868};/* VOLATILE GLOBAL g_52 */
static int16_t g_54 = 9L;
static int32_t *g_58 = (void*)0;
static int32_t ** volatile g_57 = &g_58;/* VOLATILE GLOBAL g_57 */
static uint32_t g_64 = 0x9E61979BL;
static int32_t ** const  volatile g_66[4] = {&g_58,&g_58,&g_58,&g_58};
static int8_t g_91 = 0xEBL;
static int32_t *g_125[4] = {&g_3,&g_3,&g_3,&g_3};
static int8_t g_143 = 0xE4L;
static uint8_t g_145[5] = {0UL,0UL,0UL,0UL,0UL};
static int16_t g_168 = 0x2862L;
static float g_193[1] = {0xE.7CC681p-31};
static volatile uint8_t ** const *g_200 = (void*)0;
static volatile uint8_t ** const **g_199 = &g_200;
static volatile uint8_t ** const ** volatile *g_198[4][6][9] = {{{(void*)0,&g_199,&g_199,(void*)0,(void*)0,&g_199,&g_199,&g_199,&g_199},{&g_199,&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{&g_199,&g_199,&g_199,(void*)0,(void*)0,&g_199,(void*)0,&g_199,&g_199},{&g_199,&g_199,(void*)0,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199}},{{(void*)0,&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0,&g_199,&g_199},{&g_199,&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0,&g_199,&g_199},{&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{&g_199,&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{(void*)0,&g_199,&g_199,&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199},{&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0,(void*)0}},{{&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,(void*)0},{(void*)0,&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199,(void*)0,&g_199},{&g_199,&g_199,&g_199,&g_199,&g_199,(void*)0,(void*)0,&g_199,&g_199},{&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199,&g_199,&g_199,(void*)0},{&g_199,(void*)0,(void*)0,&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199},{&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199}},{{&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199},{(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{(void*)0,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{(void*)0,&g_199,&g_199,(void*)0,&g_199,(void*)0,&g_199,&g_199,&g_199},{&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199},{&g_199,&g_199,&g_199,&g_199,(void*)0,&g_199,&g_199,&g_199,&g_199}}};
static uint16_t g_221 = 65535UL;
static int64_t g_232 = (-7L);
static uint64_t g_238 = 18446744073709551610UL;
static int32_t g_239 = 6L;
static struct S0 g_241 = {-0,765};
static uint16_t g_270 = 0xE0F4L;
static uint32_t g_276 = 4294967295UL;
static union U2 g_314[9] = {{1L},{1L},{1L},{1L},{1L},{1L},{1L},{1L},{1L}};
static volatile uint64_t **g_316 = (void*)0;
static uint8_t *g_378 = &g_145[1];
static uint8_t **g_377 = &g_378;
static uint8_t ***g_376 = &g_377;
static uint8_t ****g_375 = &g_376;
static uint8_t ***g_405 = &g_377;
static uint8_t **** const g_404 = &g_405;
static uint8_t **** const *g_403[8][1][8] = {{{&g_404,&g_404,&g_404,&g_404,&g_404,&g_404,&g_404,&g_404}},{{&g_404,&g_404,(void*)0,(void*)0,&g_404,&g_404,&g_404,(void*)0}},{{&g_404,&g_404,&g_404,&g_404,&g_404,&g_404,&g_404,&g_404}},{{&g_404,&g_404,(void*)0,&g_404,&g_404,&g_404,&g_404,(void*)0}},{{&g_404,&g_404,&g_404,(void*)0,&g_404,&g_404,&g_404,&g_404}},{{(void*)0,&g_404,&g_404,(void*)0,&g_404,(void*)0,&g_404,&g_404}},{{&g_404,&g_404,&g_404,&g_404,&g_404,&g_404,&g_404,&g_404}},{{(void*)0,&g_404,(void*)0,&g_404,&g_404,(void*)0,&g_404,(void*)0}}};
static union U2 g_445 = {0x09A43D97L};
static union U2 g_508 = {0xE98D5D70L};
static int32_t **g_583 = (void*)0;
static int32_t ***g_582 = &g_583;
static uint64_t * volatile g_664 = (void*)0;/* VOLATILE GLOBAL g_664 */
static uint64_t * volatile * volatile g_663 = &g_664;/* VOLATILE GLOBAL g_663 */
static uint64_t * volatile * volatile *g_662 = &g_663;
static union U1 g_818 = {0x62279A99L};/* VOLATILE GLOBAL g_818 */
static union U1 g_820[9][2][1] = {{{{0x53A2CABBL}},{{5L}}},{{{-1L}},{{5L}}},{{{0x53A2CABBL}},{{5L}}},{{{-1L}},{{5L}}},{{{0x53A2CABBL}},{{5L}}},{{{-1L}},{{5L}}},{{{0x53A2CABBL}},{{5L}}},{{{-1L}},{{5L}}},{{{0x53A2CABBL}},{{5L}}}};
static union U1 g_838 = {-4L};/* VOLATILE GLOBAL g_838 */
static uint32_t g_973 = 1UL;
static uint64_t g_976 = 0x150076B197E8D6A6LL;
static int8_t g_1026 = 9L;
static uint64_t g_1027 = 0xA7CE3737A9D48F40LL;
static int64_t * const **g_1090 = (void*)0;
static int64_t g_1094[10] = {0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL,0xE7D04C1C23241E98LL};
static const int32_t g_1097 = (-9L);
static const int32_t *g_1098 = &g_314[7].f0;
static uint32_t g_1107 = 18446744073709551606UL;
static uint64_t * volatile * volatile **g_1267 = &g_662;
static int64_t g_1292 = 0xFEA4B09BE87F2781LL;
static volatile struct S0 g_1299[9] = {{0,782},{0,782},{0,782},{0,782},{0,782},{0,782},{0,782},{0,782},{0,782}};
static volatile struct S0 * volatile g_1298 = &g_1299[1];/* VOLATILE GLOBAL g_1298 */
static volatile struct S0 * volatile *g_1297 = &g_1298;
static volatile union U2 g_1397 = {-1L};/* VOLATILE GLOBAL g_1397 */
static volatile union U2 * volatile g_1396 = &g_1397;/* VOLATILE GLOBAL g_1396 */
static volatile union U2 g_1399 = {0x08280476L};/* VOLATILE GLOBAL g_1399 */
static volatile union U2 *g_1398 = &g_1399;
static volatile union U2 * volatile *g_1395[6][8][5] = {{{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,(void*)0,&g_1398,&g_1398,&g_1398},{(void*)0,&g_1398,&g_1398,(void*)0,(void*)0},{&g_1396,&g_1398,&g_1398,(void*)0,&g_1398},{(void*)0,(void*)0,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,(void*)0,(void*)0,&g_1398,&g_1398}},{{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,(void*)0,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1396,&g_1398},{(void*)0,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,(void*)0,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,(void*)0,&g_1398,&g_1398},{(void*)0,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398}},{{&g_1398,&g_1398,&g_1396,(void*)0,&g_1398},{(void*)0,&g_1398,&g_1398,(void*)0,&g_1398},{&g_1398,&g_1398,&g_1396,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,(void*)0},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,(void*)0,&g_1398,(void*)0},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{(void*)0,&g_1398,&g_1398,&g_1398,&g_1398}},{{&g_1396,&g_1398,&g_1398,&g_1398,&g_1398},{(void*)0,&g_1398,&g_1398,(void*)0,&g_1398},{&g_1398,&g_1396,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,(void*)0},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,(void*)0,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1396,&g_1398,&g_1398},{(void*)0,(void*)0,&g_1398,&g_1398,&g_1398}},{{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,(void*)0,(void*)0},{&g_1398,&g_1398,&g_1398,&g_1396,&g_1398},{&g_1398,(void*)0,&g_1398,(void*)0,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1396},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{(void*)0,&g_1398,&g_1396,&g_1398,&g_1396},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398}},{{&g_1396,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,(void*)0,(void*)0},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,(void*)0,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,(void*)0,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,&g_1398,&g_1398,&g_1398,&g_1398},{&g_1398,(void*)0,&g_1398,(void*)0,(void*)0}}};
static union U2 **g_1401 = (void*)0;
static const uint8_t *g_1410 = &g_145[2];
static const uint8_t **g_1409 = &g_1410;
static int32_t g_1496 = 0x6171AA9AL;
static int32_t g_1550 = (-1L);
static int32_t g_1605 = 0xFCDAF712L;
static volatile union U2 * volatile **g_1623 = &g_1395[2][3][2];
static volatile union U2 * volatile ** volatile *g_1622 = &g_1623;
static uint32_t g_1678 = 0x61DDF034L;
static union U1 g_1686 = {0x82D4A03CL};/* VOLATILE GLOBAL g_1686 */
static struct S0 g_1727 = {-0,-464};
static int16_t g_1771 = 5L;
static const uint16_t g_1795 = 65533UL;
static union U1 g_1823[9] = {{0x67C8361DL},{0x67C8361DL},{0x67C8361DL},{0x67C8361DL},{0x67C8361DL},{0x67C8361DL},{0x67C8361DL},{0x67C8361DL},{0x67C8361DL}};
static uint16_t g_1859 = 65531UL;
static union U2 ** volatile g_1904 = (void*)0;/* VOLATILE GLOBAL g_1904 */
static int32_t ** volatile g_1908 = &g_58;/* VOLATILE GLOBAL g_1908 */
static volatile union U1 g_1915 = {7L};/* VOLATILE GLOBAL g_1915 */
static volatile union U1 g_1962 = {0xA6EDC707L};/* VOLATILE GLOBAL g_1962 */
static volatile union U1 *g_1961 = &g_1962;
static volatile union U1 * const * const g_1960 = &g_1961;
static union U1 g_1969 = {0x9091D1D7L};/* VOLATILE GLOBAL g_1969 */
static volatile union U1 g_1974 = {5L};/* VOLATILE GLOBAL g_1974 */
static float * volatile g_2054 = (void*)0;/* VOLATILE GLOBAL g_2054 */
static int32_t * volatile *g_2059 = &g_58;
static int32_t * volatile **g_2058 = &g_2059;
static int32_t * volatile ***g_2057[9] = {(void*)0,&g_2058,(void*)0,&g_2058,(void*)0,&g_2058,(void*)0,&g_2058,(void*)0};
static int32_t * volatile *** volatile * volatile g_2056 = &g_2057[3];/* VOLATILE GLOBAL g_2056 */
static struct S0 **g_2075 = (void*)0;
static struct S0 ***g_2074[9] = {&g_2075,&g_2075,&g_2075,&g_2075,&g_2075,&g_2075,&g_2075,&g_2075,&g_2075};
static const struct S0 g_2082 = {-0,878};
static union U2 g_2090 = {0x773BEB28L};
static union U1 *g_2227[1] = {&g_838};
static union U1 ** volatile g_2226[5][6] = {{&g_2227[0],(void*)0,(void*)0,&g_2227[0],&g_2227[0],&g_2227[0]},{(void*)0,&g_2227[0],&g_2227[0],(void*)0,&g_2227[0],&g_2227[0]},{&g_2227[0],&g_2227[0],(void*)0,&g_2227[0],&g_2227[0],&g_2227[0]},{&g_2227[0],(void*)0,&g_2227[0],(void*)0,&g_2227[0],&g_2227[0]},{(void*)0,&g_2227[0],&g_2227[0],&g_2227[0],&g_2227[0],&g_2227[0]}};
static uint32_t *g_2262 = &g_1678;
static uint32_t **g_2261 = &g_2262;
static volatile union U1 ** volatile g_2298 = &g_1961;/* VOLATILE GLOBAL g_2298 */
static uint8_t g_2497 = 0xDBL;
static int64_t *g_2575[10][6] = {{&g_232,&g_232,(void*)0,&g_1094[5],&g_232,&g_1094[5]},{&g_232,&g_1094[5],&g_232,&g_1094[5],&g_1094[5],(void*)0},{&g_232,&g_1292,&g_1094[5],&g_1094[5],&g_1292,&g_232},{&g_232,&g_232,(void*)0,&g_1094[5],&g_232,&g_1094[4]},{&g_232,(void*)0,&g_232,&g_1094[4],(void*)0,&g_1292},{&g_232,&g_1094[5],&g_1094[4],&g_1094[4],&g_1094[5],&g_232},{&g_232,&g_232,&g_1292,&g_1094[4],&g_232,&g_1094[4]},{&g_232,(void*)0,&g_232,&g_1094[4],(void*)0,&g_1292},{&g_232,&g_1094[5],&g_1094[4],&g_1094[4],&g_1094[5],&g_232},{&g_232,&g_232,&g_1292,&g_1094[4],&g_232,&g_1094[4]}};
static int64_t **g_2574 = &g_2575[1][2];
static int64_t ***g_2573 = &g_2574;
static int64_t ****g_2572 = &g_2573;
static union U1 g_2640[4] = {{2L},{2L},{2L},{2L}};
static uint32_t g_2657[8] = {0xDF3C2550L,0xDF3C2550L,0xDF3C2550L,0xDF3C2550L,0xDF3C2550L,0xDF3C2550L,0xDF3C2550L,0xDF3C2550L};
static int32_t g_2676 = 1L;
static uint32_t * const *g_2679 = &g_2262;
static uint32_t * const **g_2678 = &g_2679;
static int32_t *g_2740 = &g_1605;
static const uint16_t g_2808 = 65535UL;
static union U1 g_2830 = {1L};/* VOLATILE GLOBAL g_2830 */
static int32_t g_2860 = 0L;
static const uint64_t *g_2881 = &g_238;
static const uint64_t **g_2880[3][5][4] = {{{&g_2881,&g_2881,&g_2881,&g_2881},{&g_2881,&g_2881,&g_2881,&g_2881},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_2881,&g_2881,&g_2881,(void*)0},{(void*)0,&g_2881,&g_2881,(void*)0}},{{&g_2881,(void*)0,(void*)0,&g_2881},{(void*)0,&g_2881,(void*)0,&g_2881},{&g_2881,&g_2881,(void*)0,(void*)0},{(void*)0,(void*)0,&g_2881,&g_2881},{(void*)0,&g_2881,&g_2881,(void*)0}},{{(void*)0,&g_2881,&g_2881,&g_2881},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_2881,(void*)0,(void*)0,&g_2881},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_2881,&g_2881,&g_2881,(void*)0}}};
static volatile int16_t g_2911 = 0xF7ECL;/* VOLATILE GLOBAL g_2911 */
static int16_t *g_2972 = (void*)0;
static int16_t **g_2971 = &g_2972;
static union U2 *g_3057 = &g_314[6];
static union U2 ** const  volatile g_3056 = &g_3057;/* VOLATILE GLOBAL g_3056 */
static float * volatile *g_3076 = (void*)0;
static volatile int32_t g_3148 = (-1L);/* VOLATILE GLOBAL g_3148 */
static uint8_t g_3206[7][8][4] = {{{0x65L,1UL,255UL,0x5DL},{0xD0L,250UL,0x15L,8UL},{1UL,0UL,0UL,0x19L},{0x5DL,8UL,0UL,0x15L},{0xF0L,0x6FL,0xD0L,0xD0L},{0x0CL,0x0CL,0x19L,252UL},{250UL,0x37L,8UL,0UL},{250UL,255UL,0x6FL,8UL}},{{8UL,255UL,0xF0L,0UL},{255UL,0x37L,1UL,252UL},{1UL,0x0CL,0x5DL,0xD0L},{0xD8L,0x6FL,255UL,0x15L},{0x56L,8UL,1UL,0x19L},{255UL,0UL,0x37L,8UL},{0UL,250UL,0UL,0x5DL},{0x15L,1UL,1UL,1UL}},{{0x37L,1UL,0xD1L,0UL},{0x5DL,0xF5L,0x5DL,0x0CL},{252UL,0xD1L,8UL,0xDAL},{252UL,0x95L,0x5DL,0UL},{0x5DL,0xDAL,250UL,4UL},{0xD0L,0xD8L,8UL,1UL},{0UL,0x65L,8UL,0xD0L},{1UL,0xD1L,0xD0L,0x11L}},{{1UL,1UL,0x6FL,0x10L},{0x65L,0xF5L,1UL,0xA0L},{0x15L,255UL,255UL,0x15L},{0xD1L,8UL,0xA0L,0xF5L},{250UL,0x11L,0UL,0x37L},{0x12L,4UL,0xD2L,0x37L},{0x5DL,0x11L,0xF0L,0xF5L},{0x10L,8UL,0UL,0x15L}},{{0x8EL,255UL,255UL,0xA0L},{0UL,0xF5L,1UL,0x10L},{8UL,1UL,0x19L,0x11L},{0xD8L,0xD1L,0UL,0xD0L},{255UL,0x65L,250UL,1UL},{0UL,0xD8L,0UL,4UL},{4UL,255UL,0x11L,0x0CL},{8UL,0x37L,0UL,255UL}},{{0xDAL,250UL,0UL,0x8EL},{8UL,0x95L,0x11L,1UL},{4UL,8UL,0UL,0x6FL},{0UL,0x6FL,250UL,255UL},{255UL,0x5DL,0UL,0xDAL},{0xD8L,1UL,0x19L,0UL},{8UL,0x12L,1UL,0UL},{0UL,0xD2L,255UL,255UL}},{{0x8EL,0x8EL,0UL,0x56L},{0x10L,0xD0L,0xF0L,8UL},{0x5DL,250UL,0xD2L,0xF0L},{0x12L,250UL,0UL,8UL},{250UL,0xD0L,0xA0L,0x56L},{0xD1L,0x8EL,255UL,255UL},{0x15L,0xD2L,1UL,0UL},{0x65L,0x12L,0x6FL,0UL}}};
static const volatile union U1 g_3216 = {0xA090A631L};/* VOLATILE GLOBAL g_3216 */
static uint32_t g_3249[3] = {18446744073709551612UL,18446744073709551612UL,18446744073709551612UL};
static volatile float g_3262[9] = {0x1.53195Ep+26,0xB.4AD4E7p-60,0xB.4AD4E7p-60,0x1.53195Ep+26,0xB.4AD4E7p-60,0xB.4AD4E7p-60,0x1.53195Ep+26,0xB.4AD4E7p-60,0xB.4AD4E7p-60};
static union U1 g_3279 = {-6L};/* VOLATILE GLOBAL g_3279 */
static uint64_t *g_3287[4][7] = {{&g_1027,&g_1027,&g_1027,&g_1027,&g_976,&g_976,&g_1027},{&g_1027,&g_976,&g_1027,&g_976,&g_976,&g_1027,&g_1027},{&g_976,&g_1027,&g_238,&g_1027,&g_976,&g_238,&g_976},{&g_976,&g_1027,&g_976,&g_976,&g_976,&g_1027,&g_976}};
static uint64_t **g_3286[8] = {&g_3287[3][5],&g_3287[3][0],&g_3287[3][5],&g_3287[3][5],&g_3287[3][0],&g_3287[3][5],&g_3287[3][5],&g_3287[3][0]};
static uint64_t ***g_3285 = &g_3286[1];
static uint64_t ****g_3284 = &g_3285;
static volatile union U1 g_3315 = {0xC89FFDACL};/* VOLATILE GLOBAL g_3315 */
static union U1 g_3420 = {0x1142D50CL};/* VOLATILE GLOBAL g_3420 */
static int8_t *g_3429 = &g_8[1];
static int8_t **g_3428 = &g_3429;
static volatile union U1 g_3435 = {0xE8FE4039L};/* VOLATILE GLOBAL g_3435 */
static float g_3465 = 0xC.87C95Ap+15;
static float * const  volatile g_3464[2][5][2] = {{{&g_3465,(void*)0},{&g_3465,&g_3465},{&g_3465,&g_3465},{&g_3465,(void*)0},{&g_3465,&g_3465}},{{&g_3465,&g_3465},{&g_3465,(void*)0},{&g_3465,&g_3465},{&g_3465,&g_3465},{&g_3465,(void*)0}}};
static uint32_t **g_3496 = &g_2262;
static uint32_t **g_3498 = &g_2262;
static volatile union U1 g_3501 = {0x0AE72B78L};/* VOLATILE GLOBAL g_3501 */
static uint16_t *g_3509 = &g_221;
static uint16_t * volatile *g_3508[7] = {&g_3509,&g_3509,&g_3509,&g_3509,&g_3509,&g_3509,&g_3509};
static int32_t g_3586 = 0L;
static int32_t g_3613 = 0xF84F62D8L;
static volatile int16_t g_3642 = 4L;/* VOLATILE GLOBAL g_3642 */
static uint32_t * const ***g_3660 = &g_2678;
static uint32_t * const **** volatile g_3659[10] = {&g_3660,&g_3660,&g_3660,&g_3660,&g_3660,&g_3660,&g_3660,&g_3660,&g_3660,&g_3660};


/* --- FORWARD DECLARATIONS --- */
static struct S0  func_1(void);
static uint16_t  func_26(uint64_t  p_27);
static int32_t  func_35(int16_t  p_36, uint8_t  p_37);
static union U1  func_38(int32_t  p_39);
static int32_t * func_59(struct S0  p_60, uint32_t  p_61);
static int8_t  func_78(struct S0  p_79, uint32_t  p_80, const int8_t  p_81, uint32_t * p_82, int16_t * p_83);
static int32_t  func_95(int8_t * p_96, uint64_t  p_97, int32_t  p_98, uint64_t  p_99, int16_t  p_100);
static uint16_t  func_102(int32_t * p_103, float  p_104, const int64_t  p_105, int8_t  p_106);
static float  func_108(uint32_t * p_109, uint32_t  p_110, int32_t * p_111, union U2  p_112);
static uint32_t * func_113(int8_t  p_114, struct S0  p_115, int32_t * p_116);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_20 g_43 g_46 g_52 g_8 g_7 g_54 g_57 g_1107 g_1026 g_1771 g_3 g_125 g_64 g_58 g_145 g_91 g_168 g_198 g_221 g_11 g_238 g_239 g_241 g_143 g_232 g_270 g_276 g_314 g_316 g_375 g_404 g_405 g_377 g_378 g_376 g_2657 g_508.f0 g_1908 g_2059 g_1795 g_1298 g_1299 g_2860 g_2678 g_2679 g_2262 g_2261 g_1678 g_1550 g_1409 g_1410 g_2880 g_1915.f0 g_2911 g_1027 g_2740 g_1605 g_2497 g_1727.f1 g_1097 g_2058 g_1292 g_1094 g_1297 g_2082.f0 g_3056 g_1098 g_314.f0 g_3076 g_2881 g_445.f0 g_976 g_2808 g_3206 g_3216 g_3249 g_3279 g_3284 g_3642 g_2572 g_2573 g_2574 g_3428 g_3429 g_3509
 * writes: g_20 g_43 g_54 g_58 g_64 g_11 g_1107 g_125 g_91 g_143 g_145 g_168 g_221 g_232 g_238 g_239 g_241 g_193 g_276 g_316 g_375 g_403 g_314 g_445 g_2657 g_270 g_1678 g_3 g_2880 g_1026 g_1605 g_1094 g_2971 g_3057 g_2497 g_1299 g_508.f0 g_976 g_3206 g_973 g_1027 g_3249 g_3284 g_1771 g_3465 g_2574 g_3660
 */
static struct S0  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = &g_3;
    int32_t *l_4 = &g_3;
    int32_t l_5 = 0xB274BF90L;
    int32_t *l_6 = &g_3;
    int32_t *l_9 = &g_3;
    int32_t *l_10 = (void*)0;
    int32_t *l_12 = &l_5;
    int32_t *l_13 = &l_5;
    int32_t *l_14 = &g_3;
    int32_t *l_15 = &l_5;
    int32_t *l_16 = &l_5;
    int32_t *l_17 = &l_5;
    int32_t *l_18 = (void*)0;
    int32_t *l_19[7] = {&l_5,&g_3,&l_5,&l_5,&g_3,&l_5,&l_5};
    int32_t * const l_25 = (void*)0;
    const int8_t l_30 = 0x14L;
    int16_t *l_53 = &g_54;
    uint16_t *l_2843 = &g_270;
    struct S0 l_2846[6] = {{0,-738},{0,-738},{0,-738},{0,-738},{0,-738},{0,-738}};
    union U2 l_2868 = {1L};
    int32_t l_2902 = 0x9C94D0DEL;
    int8_t l_2935 = (-1L);
    uint16_t l_2945[4];
    int16_t l_2951[8];
    int32_t l_2962 = 0x09BF32B6L;
    int16_t **l_2970 = &l_53;
    struct S0 *l_2976[6][4][8] = {{{&l_2846[1],&l_2846[1],&l_2846[2],&l_2846[1],(void*)0,&g_1727,&l_2846[4],&l_2846[1]},{&l_2846[1],&l_2846[2],(void*)0,(void*)0,&l_2846[1],&g_1727,&l_2846[4],&l_2846[1]},{&l_2846[5],(void*)0,&l_2846[2],&g_1727,(void*)0,&l_2846[0],&l_2846[1],&l_2846[1]},{(void*)0,&l_2846[0],&l_2846[1],&l_2846[1],&l_2846[2],(void*)0,(void*)0,&g_241}},{{(void*)0,&l_2846[1],(void*)0,&g_1727,&g_1727,&g_1727,&g_1727,&g_1727},{(void*)0,&l_2846[5],&l_2846[5],(void*)0,&g_1727,&g_1727,&l_2846[2],&l_2846[2]},{&l_2846[2],&g_1727,&l_2846[2],&g_1727,(void*)0,&g_241,&g_241,&g_1727},{&l_2846[1],&g_1727,&l_2846[1],&l_2846[1],&l_2846[1],&g_1727,&l_2846[2],&l_2846[0]}},{{&l_2846[0],&l_2846[5],&g_1727,&g_1727,&g_1727,&g_1727,&g_1727,(void*)0},{&g_1727,&l_2846[1],(void*)0,&g_1727,(void*)0,(void*)0,&g_1727,&g_1727},{&g_1727,&l_2846[0],&l_2846[2],(void*)0,&l_2846[2],&l_2846[0],&g_1727,(void*)0},{&g_1727,(void*)0,&g_1727,&l_2846[1],&l_2846[1],&g_1727,&l_2846[0],(void*)0}},{{&l_2846[4],&l_2846[2],&g_1727,&l_2846[0],&l_2846[1],&g_1727,&g_1727,(void*)0},{&g_1727,&l_2846[1],&g_1727,(void*)0,&l_2846[2],&l_2846[5],&l_2846[1],(void*)0},{&g_1727,&l_2846[1],(void*)0,&l_2846[4],(void*)0,&g_1727,(void*)0,&g_1727},{&g_1727,(void*)0,&l_2846[1],(void*)0,&g_1727,&g_1727,(void*)0,&l_2846[1]}},{{&l_2846[0],&l_2846[0],&g_241,&l_2846[1],&l_2846[1],&l_2846[1],&l_2846[1],&l_2846[2]},{&l_2846[1],&l_2846[2],(void*)0,&l_2846[1],(void*)0,(void*)0,&g_1727,&g_1727},{&l_2846[1],&g_1727,&l_2846[1],&l_2846[0],&l_2846[1],&g_1727,(void*)0,&l_2846[1]},{(void*)0,(void*)0,(void*)0,&l_2846[1],&l_2846[2],(void*)0,&g_241,&l_2846[1]}},{{(void*)0,&l_2846[1],&g_1727,(void*)0,&g_1727,(void*)0,&l_2846[1],(void*)0},{&l_2846[1],(void*)0,&l_2846[4],(void*)0,&l_2846[1],(void*)0,&l_2846[1],&g_1727},{&g_1727,&g_1727,(void*)0,&l_2846[1],&g_1727,(void*)0,(void*)0,(void*)0},{&l_2846[0],&l_2846[1],(void*)0,&l_2846[0],&g_1727,(void*)0,&l_2846[1],&g_241}}};
    struct S0 **l_2975 = &l_2976[0][0][5];
    int16_t l_2992 = 0x640DL;
    struct S0 * const *l_3005 = (void*)0;
    uint16_t l_3053[1];
    const union U2 *l_3060 = &g_314[7];
    int64_t l_3127[6] = {1L,1L,1L,1L,1L,1L};
    const struct S0 l_3130 = {-0,-839};
    int32_t l_3143[2];
    union U2 ***l_3191[2];
    union U2 ****l_3190 = &l_3191[1];
    int16_t l_3217 = 0xB586L;
    struct S0 ****l_3245 = &g_2074[3];
    uint64_t *l_3283[10][6][4] = {{{&g_976,&g_1027,&g_976,(void*)0},{&g_976,(void*)0,(void*)0,&g_976},{&g_238,(void*)0,&g_1027,(void*)0},{(void*)0,&g_1027,&g_1027,&g_1027},{&g_238,&g_238,(void*)0,&g_1027},{&g_976,&g_1027,&g_976,(void*)0}},{{&g_976,(void*)0,(void*)0,&g_976},{&g_238,(void*)0,&g_1027,(void*)0},{(void*)0,&g_1027,&g_1027,&g_1027},{&g_238,&g_238,(void*)0,&g_1027},{&g_976,&g_1027,&g_976,(void*)0},{&g_976,(void*)0,(void*)0,&g_976}},{{&g_238,(void*)0,&g_1027,(void*)0},{(void*)0,&g_1027,&g_1027,&g_1027},{&g_238,&g_238,(void*)0,&g_1027},{&g_976,&g_1027,&g_976,(void*)0},{&g_976,(void*)0,(void*)0,&g_976},{&g_238,(void*)0,&g_1027,(void*)0}},{{(void*)0,&g_1027,&g_1027,&g_1027},{&g_238,&g_238,(void*)0,&g_1027},{&g_976,&g_1027,&g_976,(void*)0},{&g_976,(void*)0,(void*)0,&g_976},{&g_238,(void*)0,&g_1027,(void*)0},{(void*)0,&g_1027,&g_1027,&g_1027}},{{&g_238,&g_238,(void*)0,&g_1027},{&g_976,&g_1027,&g_976,(void*)0},{&g_976,(void*)0,(void*)0,&g_976},{&g_238,(void*)0,&g_1027,(void*)0},{(void*)0,&g_1027,&g_1027,&g_1027},{&g_238,&g_238,(void*)0,&g_1027}},{{&g_976,&g_1027,&g_976,(void*)0},{&g_976,(void*)0,(void*)0,&g_976},{&g_238,(void*)0,&g_1027,(void*)0},{(void*)0,&g_1027,&g_1027,&g_1027},{(void*)0,(void*)0,&g_976,&g_1027},{&g_1027,&g_238,&g_1027,&g_976}},{{&g_1027,&g_976,&g_976,&g_1027},{(void*)0,&g_976,&g_1027,&g_976},{&g_976,&g_238,&g_1027,&g_1027},{(void*)0,(void*)0,&g_976,&g_1027},{&g_1027,&g_238,&g_1027,&g_976},{&g_1027,&g_976,&g_976,&g_1027}},{{(void*)0,&g_976,&g_1027,&g_976},{&g_976,&g_238,&g_1027,&g_1027},{(void*)0,(void*)0,&g_976,&g_1027},{&g_1027,&g_238,&g_1027,&g_976},{&g_1027,&g_976,&g_976,&g_1027},{(void*)0,&g_976,&g_1027,&g_976}},{{&g_976,&g_238,&g_1027,&g_1027},{(void*)0,(void*)0,&g_976,&g_1027},{&g_1027,&g_238,&g_1027,&g_976},{&g_1027,&g_976,&g_976,&g_1027},{(void*)0,&g_976,&g_1027,&g_976},{&g_976,&g_238,&g_1027,&g_1027}},{{(void*)0,(void*)0,&g_976,&g_1027},{&g_1027,&g_238,&g_1027,&g_976},{&g_1027,&g_976,&g_976,&g_1027},{(void*)0,&g_976,&g_1027,&g_976},{&g_976,&g_238,&g_1027,&g_1027},{(void*)0,(void*)0,&g_976,&g_1027}}};
    uint64_t **l_3282 = &l_3283[1][5][1];
    uint64_t ***l_3281[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint64_t ****l_3280[4] = {&l_3281[6],&l_3281[6],&l_3281[6],&l_3281[6]};
    uint8_t l_3355 = 255UL;
    float l_3360 = 0x1.Fp+1;
    uint8_t l_3367 = 0x39L;
    struct S0 l_3395 = {-0,-153};
    int64_t *****l_3397[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const struct S0 l_3454 = {0,794};
    int64_t l_3524 = (-5L);
    uint64_t l_3542 = 1UL;
    int64_t l_3543[9] = {0xF733CA35D41C97C9LL,(-2L),(-2L),0xF733CA35D41C97C9LL,(-2L),(-2L),0xF733CA35D41C97C9LL,(-2L),(-2L)};
    int32_t l_3581 = 0x7DF05606L;
    int64_t l_3610 = 0x53B38065B375F617LL;
    int32_t l_3611 = 0L;
    int32_t l_3612[6][2] = {{0x10AE03EAL,0x6D1C22E4L},{0x10AE03EAL,0x10AE03EAL},{0x6D1C22E4L,0x10AE03EAL},{0x10AE03EAL,0x6D1C22E4L},{0x10AE03EAL,0x10AE03EAL},{0x6D1C22E4L,0x10AE03EAL}};
    int64_t **l_3647 = &g_2575[9][3];
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_2945[i] = 0xCF84L;
    for (i = 0; i < 8; i++)
        l_2951[i] = 3L;
    for (i = 0; i < 1; i++)
        l_3053[i] = 0UL;
    for (i = 0; i < 2; i++)
        l_3143[i] = 0xA5FBB68AL;
    for (i = 0; i < 2; i++)
        l_3191[i] = &g_1401;
lbl_3351:
    --g_20;
    if (((safe_mul_func_uint16_t_u_u((((void*)0 == l_25) >= ((*l_2843) = func_26((safe_lshift_func_int8_t_s_u((l_30 ^ (safe_sub_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_u(((func_35((*l_12), (func_38(((void*)0 != &g_3)) , (!(safe_mul_func_int16_t_s_s(((*l_53) &= (safe_div_func_uint16_t_u_u((g_52 , g_8[1]), g_7[0][0][3]))), g_8[7]))))) > 4294967295UL) , 0x75L), 3)) < g_1795), (*l_12)))), 3))))), (*l_4))) & (*l_2)))
    { /* block id: 1154 */
        int16_t l_2847 = 0x9241L;
        uint8_t *****l_2858 = &g_375;
        int32_t l_2859 = (-1L);
        int64_t *l_2861[5][4][6] = {{{(void*)0,&g_1094[5],&g_232,&g_232,&g_1094[0],&g_232},{&g_1094[2],&g_1094[5],&g_232,&g_232,&g_1292,(void*)0},{&g_1094[6],&g_232,&g_232,&g_1094[5],&g_1094[5],&g_232},{&g_1094[0],&g_1094[0],&g_232,&g_1292,&g_232,&g_1292}},{{&g_232,&g_1094[5],&g_1292,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_1292,&g_232,&g_1094[0],&g_1292},{(void*)0,&g_232,&g_232,&g_232,(void*)0,&g_232},{&g_232,&g_1094[0],&g_232,&g_1094[2],&g_1094[6],&g_1292}},{{&g_1094[5],&g_232,&g_1094[6],&g_1292,&g_1094[2],&g_1094[5]},{&g_1094[2],&g_232,&g_232,&g_1292,(void*)0,&g_1094[2]},{&g_1094[5],(void*)0,&g_1094[5],&g_1094[2],&g_232,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_1292,&g_1094[2]}},{{&g_1292,&g_1292,&g_232,&g_1094[6],&g_232,&g_1292},{&g_1094[5],&g_232,(void*)0,&g_232,&g_232,&g_232},{&g_1094[2],&g_1292,&g_232,&g_1094[5],&g_1292,&g_1094[2]},{&g_232,&g_232,(void*)0,&g_232,&g_232,&g_232}},{{&g_1094[2],(void*)0,&g_232,&g_232,(void*)0,&g_1292},{&g_1292,&g_232,&g_232,(void*)0,&g_1094[2],&g_1292},{&g_232,&g_232,&g_232,&g_232,&g_1094[6],&g_232},{&g_1094[2],&g_1094[0],(void*)0,(void*)0,&g_1094[0],&g_1094[2]}}};
        int8_t l_2942[1];
        int32_t l_2954 = 0x5FEB372EL;
        uint32_t l_2965[4][5][5] = {{{0x35EE99EEL,18446744073709551615UL,0UL,18446744073709551615UL,1UL},{18446744073709551612UL,18446744073709551606UL,0xD9FD54D9L,1UL,0xD9FD54D9L},{0xAE3F8D8CL,0xAE3F8D8CL,0xF245A0E3L,18446744073709551613UL,18446744073709551615UL},{0UL,0x7DF6F6F8L,1UL,1UL,1UL},{0x3FAC9F87L,0x170E5A49L,0xF9ABFD4BL,0x35EE99EEL,0x8453BC60L}},{{18446744073709551606UL,0x7DF6F6F8L,18446744073709551614UL,18446744073709551612UL,0x0BB4699BL},{0xF1045662L,0xAE3F8D8CL,0x35EE99EEL,0xAE3F8D8CL,0xF1045662L},{18446744073709551609UL,18446744073709551606UL,0x793118B4L,0UL,0xDCD58A32L},{18446744073709551606UL,18446744073709551615UL,18446744073709551615UL,0x3FAC9F87L,0x35EE99EEL},{0x793118B4L,18446744073709551606UL,0xEBF54051L,18446744073709551606UL,0xDCD58A32L}},{{0x8453BC60L,0x3FAC9F87L,18446744073709551606UL,0xF1045662L,0xF1045662L},{0xDCD58A32L,0UL,0xDCD58A32L,18446744073709551609UL,0x0BB4699BL},{0x7316EE01L,1UL,0xBD74CB85L,18446744073709551606UL,0x8453BC60L},{0UL,0x0BB4699BL,0x7CF275E0L,0x793118B4L,1UL},{0xEC1F7C34L,0UL,0xBD74CB85L,0x8453BC60L,18446744073709551615UL}},{{0x52F982A2L,0xD9FD54D9L,0xDCD58A32L,0xDCD58A32L,0xD9FD54D9L},{7UL,0xF245A0E3L,18446744073709551606UL,0x7316EE01L,1UL},{18446744073709551606UL,1UL,0xEBF54051L,0UL,0x03933016L},{0xBD74CB85L,0xF9ABFD4BL,18446744073709551615UL,0xEC1F7C34L,7UL},{18446744073709551606UL,18446744073709551614UL,0x793118B4L,0x52F982A2L,18446744073709551606UL}}};
        const uint8_t l_3021 = 0x8EL;
        int32_t ***l_3083 = &g_583;
        const uint16_t l_3109 = 4UL;
        int32_t l_3138 = 0x78C820B7L;
        float l_3139 = 0x3.2p-1;
        int32_t l_3141 = 0xE7991FDFL;
        int32_t l_3150 = 0xDC99F0D4L;
        int32_t l_3157 = 8L;
        int32_t l_3158 = 0xC2014C16L;
        int32_t l_3159 = 0L;
        uint16_t l_3160 = 0x5600L;
        uint32_t l_3168 = 0x08246995L;
        const uint8_t l_3178 = 0UL;
        int32_t l_3198[9] = {0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL,0xFE7CF39DL};
        struct S0 l_3229 = {0,967};
        uint16_t l_3348 = 0x2F63L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2942[i] = 0xD9L;
        if ((((*l_6) &= (safe_rshift_func_int8_t_s_u(((l_2846[1] , (l_2847 , 0x9BL)) > (~((safe_mul_func_int16_t_s_s((-1L), (safe_add_func_int16_t_s_s(((*l_53) = ((((!(safe_add_func_uint32_t_u_u(((***g_2678) = (((l_2847 & ((0x81L ^ (safe_rshift_func_uint16_t_u_s(l_2847, ((l_2859 = ((&g_404 == (((*g_1298) , (*l_15)) , l_2858)) <= g_239)) || g_2860)))) | (*l_12))) >= g_7[0][0][3]) && 0x27BFL)), l_2847))) <= l_2847) && (**g_2261)) <= (*l_15))), l_2847)))) == g_1550))), 4))) >= 0x56527CBB4FBD1FF7LL))
        { /* block id: 1159 */
            uint16_t *l_2888 = &g_221;
            int32_t l_2892 = 0x1C1A3028L;
            int32_t l_2953 = 0L;
            for (g_221 = 1; (g_221 <= 5); g_221 += 1)
            { /* block id: 1162 */
                const uint64_t ***l_2882 = &g_2880[1][2][2];
                int8_t *l_2883 = &g_1026;
                uint16_t *l_2890 = (void*)0;
                int64_t l_2913 = 0x4C2CACCBDF060BF4LL;
                int8_t l_2914 = 1L;
                int16_t l_2943 = 0x6F94L;
                uint64_t l_2948[10][3][8] = {{{0UL,0x16AE5DA8AB3E17EDLL,0UL,0UL,0x90FD4B5C1FBF67EDLL,5UL,0UL,0UL},{2UL,0xFFF7E78C30666BFBLL,0x1EC7EC1F5672462ELL,4UL,18446744073709551615UL,5UL,0x6FBA831CA4117544LL,0xEF2521CC9D5F5B51LL},{0UL,2UL,18446744073709551615UL,4UL,0x90FD4B5C1FBF67EDLL,18446744073709551615UL,4UL,0UL}},{{0xFFF7E78C30666BFBLL,2UL,0x1EC7EC1F5672462ELL,0UL,3UL,5UL,0xE4A8E23CEC5DB8EFLL,0UL},{0UL,0xFFF7E78C30666BFBLL,0xEF2521CC9D5F5B51LL,4UL,5UL,5UL,4UL,0xEF2521CC9D5F5B51LL},{2UL,2UL,18446744073709551615UL,4UL,3UL,18446744073709551615UL,0x6FBA831CA4117544LL,0UL}},{{0x5E487D133F4F7A6FLL,2UL,0xEF2521CC9D5F5B51LL,0UL,0x90FD4B5C1FBF67EDLL,5UL,0UL,0UL},{2UL,0xFFF7E78C30666BFBLL,0x1EC7EC1F5672462ELL,4UL,18446744073709551615UL,5UL,0x6FBA831CA4117544LL,0xEF2521CC9D5F5B51LL},{0UL,2UL,18446744073709551615UL,4UL,0x90FD4B5C1FBF67EDLL,18446744073709551615UL,4UL,0UL}},{{0xFFF7E78C30666BFBLL,2UL,0x1EC7EC1F5672462ELL,0UL,3UL,5UL,0xE4A8E23CEC5DB8EFLL,0UL},{0UL,0xFFF7E78C30666BFBLL,0xEF2521CC9D5F5B51LL,4UL,5UL,5UL,4UL,0xEF2521CC9D5F5B51LL},{2UL,2UL,18446744073709551615UL,4UL,3UL,18446744073709551615UL,0x6FBA831CA4117544LL,0UL}},{{0x5E487D133F4F7A6FLL,2UL,0xEF2521CC9D5F5B51LL,0UL,0x90FD4B5C1FBF67EDLL,5UL,0UL,0UL},{2UL,0xFFF7E78C30666BFBLL,0x1EC7EC1F5672462ELL,4UL,18446744073709551615UL,5UL,0x6FBA831CA4117544LL,0xEF2521CC9D5F5B51LL},{0UL,2UL,18446744073709551615UL,4UL,0x90FD4B5C1FBF67EDLL,18446744073709551615UL,4UL,0UL}},{{0xFFF7E78C30666BFBLL,2UL,0x1EC7EC1F5672462ELL,0UL,3UL,5UL,0xE4A8E23CEC5DB8EFLL,0UL},{0UL,0xFFF7E78C30666BFBLL,0xEF2521CC9D5F5B51LL,4UL,5UL,5UL,4UL,0xEF2521CC9D5F5B51LL},{2UL,2UL,18446744073709551615UL,4UL,3UL,18446744073709551615UL,0x6FBA831CA4117544LL,0UL}},{{0x5E487D133F4F7A6FLL,2UL,0xEF2521CC9D5F5B51LL,0UL,0x90FD4B5C1FBF67EDLL,5UL,0UL,0UL},{2UL,0xFFF7E78C30666BFBLL,0x1EC7EC1F5672462ELL,4UL,18446744073709551615UL,5UL,0x6FBA831CA4117544LL,0xEF2521CC9D5F5B51LL},{0UL,2UL,18446744073709551615UL,4UL,0x90FD4B5C1FBF67EDLL,18446744073709551615UL,4UL,0UL}},{{0xFFF7E78C30666BFBLL,2UL,0x1EC7EC1F5672462ELL,0UL,3UL,5UL,0xE4A8E23CEC5DB8EFLL,0UL},{0UL,0xFFF7E78C30666BFBLL,0xEF2521CC9D5F5B51LL,4UL,5UL,5UL,4UL,0xEF2521CC9D5F5B51LL},{2UL,2UL,18446744073709551615UL,4UL,3UL,18446744073709551615UL,0x6FBA831CA4117544LL,0UL}},{{0x5E487D133F4F7A6FLL,2UL,18446744073709551615UL,0xCE30361C5A109E45LL,5UL,18446744073709551615UL,0xCE30361C5A109E45LL,0xEF2521CC9D5F5B51LL},{0xFFF7E78C30666BFBLL,0x16AE5DA8AB3E17EDLL,18446744073709551615UL,0UL,18446744073709551607UL,18446744073709551615UL,0xE4A8E23CEC5DB8EFLL,18446744073709551615UL},{0x5E487D133F4F7A6FLL,0xFFF7E78C30666BFBLL,0UL,0UL,5UL,0x90FD4B5C1FBF67EDLL,0UL,0xEF2521CC9D5F5B51LL}},{{0x16AE5DA8AB3E17EDLL,0xFFF7E78C30666BFBLL,18446744073709551615UL,0xCE30361C5A109E45LL,18446744073709551615UL,18446744073709551615UL,18446744073709551614UL,0xEF2521CC9D5F5B51LL},{0x5E487D133F4F7A6FLL,0x16AE5DA8AB3E17EDLL,18446744073709551615UL,0UL,18446744073709551615UL,18446744073709551615UL,0UL,18446744073709551615UL},{0xFFF7E78C30666BFBLL,0xFFF7E78C30666BFBLL,18446744073709551607UL,0UL,18446744073709551615UL,0x90FD4B5C1FBF67EDLL,0xE4A8E23CEC5DB8EFLL,0xEF2521CC9D5F5B51LL}}};
                int32_t l_2964 = 0xDD4E5412L;
                int16_t **l_2968 = &l_53;
                int i, j, k;
                if ((safe_sub_func_uint16_t_u_u(0x3EE1L, (safe_sub_func_uint32_t_u_u(0xC78D77DBL, (18446744073709551614UL == (l_2868 , ((safe_div_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((safe_lshift_func_int8_t_s_s((((*l_2883) = (safe_sub_func_uint16_t_u_u((((~(**g_1409)) , (l_2847 >= (l_2846[g_221] , ((safe_rshift_func_uint8_t_u_u(((l_2846[g_221].f0 >= (((*l_2882) = g_2880[1][2][2]) == (void*)0)) >= (*l_14)), (*l_15))) ^ (**g_2679))))) >= l_2847), 0UL))) , g_270), g_7[0][0][3])) != l_2847), g_1915.f0)), l_2847)) < l_2846[g_221].f0))))))))
                { /* block id: 1165 */
                    uint16_t **l_2889 = &l_2888;
                    float *l_2891 = &g_193[0];
                    int32_t l_2912[5][3] = {{0x0D50B8F0L,0x0D50B8F0L,0x0D50B8F0L},{0x88374986L,0x88374986L,0x88374986L},{0x0D50B8F0L,0x0D50B8F0L,0x0D50B8F0L},{0x88374986L,0x88374986L,0x88374986L},{0x0D50B8F0L,0x0D50B8F0L,0x0D50B8F0L}};
                    int i, j;
                    (*l_16) = ((safe_sub_func_uint16_t_u_u(0x82BCL, 0xF3DDL)) , ((safe_div_func_uint64_t_u_u(((((*l_2891) = (((*l_2889) = l_2888) == l_2890)) != l_2892) , (safe_sub_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((+l_2846[g_221].f0), ((-7L) ^ (safe_rshift_func_int16_t_s_u((((safe_sub_func_int32_t_s_s(((*g_2740) ^= ((l_2902 != (safe_mul_func_int16_t_s_s((safe_div_func_int16_t_s_s(((safe_div_func_int16_t_s_s(((safe_mod_func_uint32_t_u_u((***g_2678), 4294967295UL)) >= g_2911), l_2912[1][0])) || (-10L)), g_1027)), l_2846[g_221].f0))) & l_2847)), 2UL)) | (*l_12)) > g_2497), 3))))), l_2912[1][0]))), l_2913)) , l_2912[2][1]));
                    (*l_13) ^= l_2912[2][2];
                    for (g_270 = 0; (g_270 <= 0); g_270 += 1)
                    { /* block id: 1173 */
                        int i;
                        if (g_145[(g_270 + 2)])
                            break;
                        if ((*g_2740))
                            break;
                        if (l_2914)
                            continue;
                    }
                    l_2846[g_221].f0 &= (l_2912[0][0] && (4294967287UL != (((safe_sub_func_uint32_t_u_u((((g_1727.f1 , ((g_1094[(g_221 + 2)] = ((**g_2679) == 0x83164EB2L)) , (safe_add_func_uint8_t_u_u(((safe_add_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((((****g_375) = (((*l_53) &= (((safe_div_func_int32_t_s_s((((*l_2883) = ((safe_add_func_int32_t_s_s(((***g_2678) == (l_2892 = (safe_sub_func_uint32_t_u_u((((l_2892 || (safe_rshift_func_uint8_t_u_s(l_2935, 4))) && (safe_mod_func_uint16_t_u_u((safe_div_func_uint32_t_u_u((safe_sub_func_int8_t_s_s((1UL | 0x45BBEE6BL), (***g_376))), 1L)), g_1097))) ^ (*l_17)), (***g_2678))))), l_2913)) && l_2892)) ^ 0xAEL), 4294967295UL)) != l_2914) & 0x84L)) | 7L)) < 255UL), g_8[7])), 1L)), g_221)) | l_2942[0]), 0x1AL)))) >= l_2846[g_221].f1) , 3UL), 0x2D307DB1L)) > l_2943) < l_2912[1][0])));
                }
                else
                { /* block id: 1184 */
                    int32_t l_2944[8];
                    int32_t l_2952 = (-1L);
                    uint16_t l_2955 = 0xACC2L;
                    float l_2963 = 0xB.456E26p+80;
                    int i;
                    for (i = 0; i < 8; i++)
                        l_2944[i] = (-7L);
                    l_2945[2]--;
                    ++l_2948[6][0][5];
                    l_2955--;
                    if ((((safe_add_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((****g_404), (-5L))), l_2942[0])) , 0x30F1B9B9F177F5BFLL) , 0xFF36F151L))
                    { /* block id: 1188 */
                        int16_t ***l_2969[4][8] = {{&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968},{&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968},{&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968},{&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968,&l_2968}};
                        int32_t l_2983 = 0x8EBC48F7L;
                        int8_t l_2993 = (-1L);
                        int64_t l_2994[5][5] = {{1L,0xE31E498B67A2F5B9LL,0x14734BF0F288429FLL,3L,0x14734BF0F288429FLL},{0x14734BF0F288429FLL,0x14734BF0F288429FLL,0x8BAA9093F14F401FLL,3L,0x64F48F6BD4FA6F8CLL},{0xE31E498B67A2F5B9LL,1L,1L,0xE31E498B67A2F5B9LL,0x14734BF0F288429FLL},{0xE31E498B67A2F5B9LL,3L,0xCF1433A52CF37527LL,0xCF1433A52CF37527LL,3L},{0x14734BF0F288429FLL,1L,0xCF1433A52CF37527LL,0x8BAA9093F14F401FLL,0x8BAA9093F14F401FLL}};
                        int i, j;
                        ++l_2965[3][1][1];
                        g_2971 = (l_2970 = l_2968);
                        (**g_2058) = &l_2944[5];
                        (*l_9) &= ((0x6217L < ((safe_sub_func_int8_t_s_s(((void*)0 != l_2975), ((safe_mod_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_u(((safe_add_func_int16_t_s_s(l_2983, l_2983)) , l_2983), 11)) , (safe_sub_func_int32_t_s_s(((safe_mul_func_int8_t_s_s((l_2859 , (safe_rshift_func_uint16_t_u_s(((safe_lshift_func_uint8_t_u_u((*g_378), 7)) != ((l_2983 , l_2992) <= (*g_2262))), l_2952))), l_2965[3][1][1])) > 0x13BB0D2FDB4A46FDLL), l_2993))), 4L)) & l_2965[2][3][1]))) | l_2994[4][2])) > (**g_377));
                    }
                    else
                    { /* block id: 1194 */
                        struct S0 * const **l_3006 = (void*)0;
                        struct S0 * const **l_3007 = (void*)0;
                        struct S0 * const **l_3008 = (void*)0;
                        struct S0 * const **l_3009 = (void*)0;
                        struct S0 * const **l_3010 = &l_3005;
                        int32_t l_3020 = 0x49B7129CL;
                        float *l_3022 = &g_193[0];
                        (*l_3022) = (safe_mul_func_float_f_f(0xA.1D22D2p-35, (((safe_div_func_int16_t_s_s((((((safe_rshift_func_int8_t_s_s((((*g_1298) , (safe_rshift_func_int16_t_s_s((safe_mod_func_int64_t_s_s((((((*l_3010) = l_3005) == &l_2976[0][0][5]) , l_2953) >= (~l_2846[g_221].f1)), 18446744073709551615UL)), (safe_sub_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(((((safe_lshift_func_int16_t_s_u(((*l_53) ^= g_1727.f1), ((safe_rshift_func_uint8_t_u_u((***g_376), l_3020)) , g_1795))) , 0xADL) , l_3021) && l_2953), l_2954)), 65535UL))))) > 0x5D02AE74L), 6)) & l_2952) && g_8[1]) > g_1292) & 9UL), l_2847)) , &l_2868) == &g_314[3])));
                        (*l_3022) = (safe_sub_func_float_f_f((safe_div_func_float_f_f(g_1292, (safe_add_func_float_f_f((safe_sub_func_float_f_f(0x9.31ED3Cp+34, ((void*)0 != (*g_2059)))), (safe_div_func_float_f_f(((-((+(safe_mul_func_float_f_f(0x0.Fp+1, (-0x6.Cp+1)))) == (safe_div_func_float_f_f(0x0.9p-1, (safe_div_func_float_f_f(0x9.A8DA9Bp-18, ((((((1UL >= 0xE9DFD861B78E7F3FLL) , g_1094[6]) < (*l_2)) , 0x88EDL) , 0xA.23AA28p+1) <= l_2954))))))) > g_52.f1), 0x3.62AF8Cp-4)))))), l_3020));
                    }
                }
                return (*g_1298);
            }
        }
        else
        { /* block id: 1203 */
            float l_3050[6][5] = {{(-0x9.Ep+1),0x0.8CE9DFp-66,0xF.ED155Dp+58,0xD.F665C5p+64,0x0.CE7422p-26},{0xF.ED155Dp+58,0x0.8CE9DFp-66,(-0x9.Ep+1),(-0x9.Ep+1),0x0.8CE9DFp-66},{0x7.4p+1,0x6.594D1Cp+66,0xF.56C808p+6,0x0.8CE9DFp-66,0x0.CE7422p-26},{0x6.594D1Cp+66,(-0x9.Ep+1),0xF.56C808p+6,0xF.8F6BE0p-10,0xF.56C808p+6},{0x0.CE7422p-26,0x0.CE7422p-26,(-0x9.Ep+1),0x7.4p+1,0x0.D927F4p-48},{0x6.594D1Cp+66,0x0.D927F4p-48,0xF.ED155Dp+58,0x7.4p+1,0x7.4p+1}};
            int32_t l_3051[3][2] = {{(-1L),2L},{2L,(-1L)},{2L,2L}};
            int32_t l_3088 = 1L;
            int32_t l_3090 = 1L;
            int32_t l_3094 = 8L;
            struct S0 *l_3121[6][8] = {{&g_1727,&l_2846[1],&g_241,&l_2846[1],&l_2846[1],&l_2846[1],&g_241,&g_241},{&g_241,&l_2846[1],&l_2846[1],&l_2846[1],&l_2846[1],&l_2846[1],&l_2846[1],&l_2846[1]},{&g_241,&l_2846[1],&l_2846[1],&g_241,&g_241,&l_2846[3],&g_241,&l_2846[1]},{&g_241,&g_241,&l_2846[1],&l_2846[1],&g_241,&g_241,&g_241,&g_241},{&l_2846[3],&l_2846[1],&l_2846[1],&l_2846[3],&l_2846[1],&g_241,&l_2846[1],&l_2846[1]},{&l_2846[1],&g_241,&l_2846[1],&l_2846[1],&l_2846[1],&g_241,&g_241,&l_2846[2]}};
            int32_t l_3144[2];
            union U2 ****l_3192 = &l_3191[1];
            int64_t l_3197 = (-6L);
            int32_t l_3199 = 0x5A029356L;
            int16_t l_3204 = 1L;
            int64_t * const ***l_3224 = &g_1090;
            uint16_t l_3321 = 65533UL;
            int32_t **l_3336 = &g_125[1];
            int i, j;
            for (i = 0; i < 2; i++)
                l_3144[i] = 0xE39E056DL;
            for (g_1678 = (-9); (g_1678 > 22); g_1678 = safe_add_func_int16_t_s_s(g_1678, 5))
            { /* block id: 1206 */
                uint32_t l_3043 = 4294967294UL;
                const uint32_t *l_3049 = (void*)0;
                const uint32_t **l_3048 = &l_3049;
                uint32_t l_3052 = 0xCB6185D6L;
                l_3043--;
                (*l_6) |= (((&g_404 == (void*)0) < ((((*l_15) != (&g_2972 != ((**g_1297) , (void*)0))) == (((((safe_mul_func_int8_t_s_s((l_18 == ((*l_3048) = (void*)0)), g_2082.f0)) > 0UL) != (-3L)) & l_3051[2][0]) ^ l_3052)) < l_3052)) , l_3043);
            }
            --l_3053[0];
            if (((void*)0 != &g_2227[0]))
            { /* block id: 1212 */
                int32_t *l_3058 = &l_3051[2][0];
                union U2 *l_3059 = (void*)0;
                (*g_3056) = &l_2868;
                l_3058 = &l_2954;
                l_3051[1][0] ^= ((*l_14) = (l_3059 == l_3060));
                for (g_276 = 0; (g_276 < 60); g_276++)
                { /* block id: 1219 */
                    struct S0 ***l_3063 = &l_2975;
                    int32_t l_3064 = 7L;
                    (*l_3063) = &l_2976[4][0][2];
                    for (g_64 = 0; (g_64 <= 3); g_64 += 1)
                    { /* block id: 1223 */
                        (*l_4) &= (0x0E6D8828L ^ l_3064);
                        return (**g_1297);
                    }
                    if (l_3051[2][0])
                        break;
                }
            }
            else
            { /* block id: 1229 */
                int32_t l_3096 = 0xE787124FL;
                float l_3103[10] = {0x1.6p+1,0x0.3p-1,0x1.6p+1,0x1.6p+1,0x0.3p-1,0x1.6p+1,0x1.6p+1,0x0.3p-1,0x1.6p+1,0x1.6p+1};
                struct S0 l_3128 = {-0,65};
                int32_t l_3133 = 0x7141924DL;
                int32_t l_3135 = 0x000A5FDAL;
                int32_t l_3136 = 0xD7864AE9L;
                int32_t l_3145 = 0xDC67F761L;
                int32_t l_3147 = 4L;
                int32_t l_3152 = 0xC9FA5C07L;
                int32_t l_3155 = 0xE4DFCDC9L;
                uint64_t l_3263 = 0xB8D619E5D1101CA3LL;
                int16_t l_3300 = 0xB693L;
                int32_t **l_3310 = &l_15;
                uint8_t *l_3333[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_3333[i] = &g_145[2];
                if ((*g_1098))
                { /* block id: 1230 */
                    int16_t l_3089 = 0x93F5L;
                    const uint64_t l_3093[8] = {0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL,0xDD5776D7B29A59B9LL};
                    int i;
                    for (l_2992 = 3; (l_2992 >= 0); l_2992 -= 1)
                    { /* block id: 1233 */
                        float l_3086 = 0x0.Ep+1;
                        uint16_t l_3087 = 65527UL;
                        uint64_t *l_3091[3][2][3] = {{{&g_1027,&g_1027,&g_1027},{&g_976,&g_976,&g_976}},{{&g_1027,&g_1027,&g_1027},{&g_976,&g_976,&g_976}},{{&g_1027,&g_1027,&g_1027},{&g_976,&g_976,&g_976}}};
                        int32_t l_3092 = (-9L);
                        struct S0 l_3095[1][6] = {{{0,-465},{0,-465},{0,-465},{0,-465},{0,-465},{0,-465}}};
                        int i, j, k;
                        (*g_2740) |= (safe_rshift_func_uint8_t_u_s((((*g_2262) = (0x684CAB97L <= (safe_mod_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s(((safe_sub_func_int16_t_s_s((safe_lshift_func_int16_t_s_u((((!((void*)0 != g_3076)) , (l_3090 = (((l_3092 = (safe_div_func_int64_t_s_s(((((((l_3088 = ((safe_mul_func_int8_t_s_s(((safe_div_func_uint32_t_u_u((((g_508.f0 , l_3083) != (void*)0) & ((4294967291UL && ((safe_mul_func_uint16_t_u_u(((((*l_53) = ((l_3051[2][0] = l_3087) > l_3087)) >= g_1299[1].f0) == l_3088), l_3089)) >= 0L)) , (*l_15))), l_3087)) >= 0x350BL), (***g_376))) < g_11)) < (**g_377)) <= l_3090) == 65535UL) > l_3090) == 0UL), (*g_2881)))) <= (*l_15)) || l_3093[1]))) != l_3094), 9)), (*l_17))) , 0x9EL), 4)), (*g_2881))))) , 0xEEL), 5));
                        return l_3095[0][3];
                    }
                    for (g_445.f0 = 0; (g_445.f0 <= 3); g_445.f0 += 1)
                    { /* block id: 1245 */
                        int16_t l_3097 = 1L;
                        (*g_2740) &= (l_3096 , l_3097);
                    }
                }
                else
                { /* block id: 1248 */
                    uint8_t l_3104 = 0UL;
                    uint8_t *l_3108 = &g_2497;
                    int32_t l_3131 = (-1L);
                    int32_t l_3132 = 0x1C186511L;
                    int32_t l_3134 = 8L;
                    int32_t l_3137 = 1L;
                    int32_t l_3140 = 0x2421CD44L;
                    int32_t l_3142 = 0x40B2514BL;
                    int32_t l_3146 = 0xE200B3F0L;
                    int32_t l_3149 = 9L;
                    int32_t l_3151 = (-1L);
                    int32_t l_3153 = 0x924E3AAAL;
                    int32_t l_3154 = 0x2961B254L;
                    int32_t l_3156 = 0L;
                    const struct S0 l_3165 = {-0,366};
                    if (((safe_mul_func_uint16_t_u_u((+(((((safe_sub_func_uint32_t_u_u(((*l_2) & ((l_3096 & (l_3090 &= (*l_14))) , (l_3104 < (((+((*l_3108) ^= ((****g_375) = ((safe_div_func_int8_t_s_s(l_3090, (((0xCCC8E2E2CC651270LL == 9L) , func_38((4294967295UL > l_3104))) , l_3088))) , l_3088)))) , 0xC5L) ^ 1UL)))), l_3109)) & 0UL) , l_3096) & l_3104) || 0x0616137B664BCC37LL)), l_3094)) && (*l_15)))
                    { /* block id: 1252 */
                        (*l_17) ^= (l_3096 >= l_3104);
                    }
                    else
                    { /* block id: 1254 */
                        int64_t l_3125 = 0x6EB6BE5B3200CA51LL;
                        int32_t l_3126 = (-9L);
                        struct S0 l_3129[9] = {{-0,-683},{-0,-683},{-0,-683},{-0,-683},{-0,-683},{-0,-683},{-0,-683},{-0,-683},{-0,-683}};
                        int i;
                        (*l_6) ^= (g_1094[5] || ((safe_div_func_uint8_t_u_u(((safe_unary_minus_func_int32_t_s((((safe_div_func_uint32_t_u_u(((safe_sub_func_uint64_t_u_u(((safe_div_func_int32_t_s_s(((safe_mod_func_int16_t_s_s(((((**l_2970) = ((void*)0 == l_3121[0][1])) , ((****g_404) = (0x70ABF41F9528C8C2LL == (l_3126 = (((((**g_2261) = (***g_2678)) != 0xD71C2B89L) > ((0x6CDDL != ((l_3104 != (~((((safe_mul_func_int16_t_s_s((((l_3125 & l_3021) < l_3104) >= g_64), l_3125)) == g_1299[1].f0) >= (*l_12)) >= 0x8C9FL))) ^ g_1771)) | l_3125)) >= (-8L)))))) ^ (-1L)), l_3090)) > 0x5BCEL), 1L)) & l_3096), l_3127[0])) >= 0x97A4L), 0x16C6898FL)) , 0x52BDD86FFD6C648ELL) <= (-1L)))) , (***g_376)), l_3125)) >= l_3104));
                        (*l_12) |= l_3125;
                        (**g_1297) = (l_3129[5] = l_3128);
                        l_3128 = l_3130;
                    }
                    ++l_3160;
                    for (g_508.f0 = 5; (g_508.f0 >= 0); g_508.f0 -= 1)
                    { /* block id: 1268 */
                        uint64_t *l_3169 = &g_976;
                        l_3168 = (safe_rshift_func_int8_t_s_s((l_3165 , (safe_add_func_int64_t_s_s(l_3051[2][0], 0x59A4C0B60C9FDAEBLL))), 5));
                        (*l_14) &= (((-1L) != ((((*l_3169)--) ^ l_3145) && (safe_lshift_func_int16_t_s_u((((*g_378) != (safe_rshift_func_int8_t_s_s(g_2808, (safe_sub_func_uint64_t_u_u(l_3051[2][0], (l_3178 > (((g_1299[1].f0 , 0x118B1071ED7FCA86LL) < (l_3142 |= 2L)) , l_3152))))))) == l_3088), l_3144[1])))) >= l_3165.f1);
                    }
                }
                if (((0x6EA2E09FL >= 0xC9D3A8D8L) , ((safe_mod_func_int16_t_s_s((!(safe_add_func_uint16_t_u_u(((l_3158 < (safe_sub_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(l_3144[1], (safe_add_func_int32_t_s_s(((l_3051[2][0] != (l_3190 != l_3192)) < (*l_4)), ((safe_lshift_func_uint16_t_u_u((((safe_sub_func_int32_t_s_s((0x3C538C17L != 0xE6257C80L), l_3197)) == (*l_12)) && (-10L)), g_232)) < (-4L)))))), 7L))) || 0x2597L), g_7[1][0][2]))), 1UL)) & l_3155)))
                { /* block id: 1275 */
                    float l_3200 = 0x9.918DC5p+73;
                    int32_t l_3201 = 0x6ECCF24BL;
                    int32_t l_3202 = 0xBC889F2EL;
                    int32_t l_3203 = (-4L);
                    int32_t l_3205 = 2L;
                    uint32_t *l_3223 = &g_973;
                    uint64_t *l_3232 = &g_238;
                    struct S0 *****l_3242 = (void*)0;
                    struct S0 ****l_3244 = &g_2074[4];
                    struct S0 *****l_3243 = &l_3244;
                    g_3206[3][6][0]--;
                    if ((safe_lshift_func_int16_t_s_s((safe_unary_minus_func_int16_t_s(((***g_405) < (safe_add_func_uint32_t_u_u((((*l_2843) = (safe_add_func_uint32_t_u_u((l_3202 , 0x267C2477L), ((g_3216 , l_3202) != l_3217)))) , ((*l_3223) = ((*g_2262) = (safe_mod_func_int16_t_s_s((safe_sub_func_int8_t_s_s(0x20L, ((safe_unary_minus_func_uint64_t_u((l_3128 , (*g_2881)))) >= 0x4C18L))), g_445.f0))))), l_3155))))), l_3152)))
                    { /* block id: 1280 */
                        int64_t * const ****l_3225 = &l_3224;
                        int64_t * const ****l_3226 = (void*)0;
                        int64_t * const ***l_3228 = &g_1090;
                        int64_t * const ****l_3227 = &l_3228;
                        (*l_2) = l_3147;
                        (*l_3227) = ((*l_3225) = l_3224);
                    }
                    else
                    { /* block id: 1284 */
                        return l_3229;
                    }
                    l_3201 &= (l_3229 , (l_3229 , (safe_mod_func_uint32_t_u_u(((g_1027 &= ((l_3203 || ((*l_3232)++)) , ((*l_3232) &= ((safe_unary_minus_func_uint8_t_u((0xC5L < ((safe_sub_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((l_3135 || ((((*l_3243) = &g_2074[6]) != l_3245) <= (*g_2740))) < ((func_38(l_3205) , l_3135) != g_145[3])), g_1795)), 0x0B6EL)) >= (*l_4))))) , l_3133)))) | l_3051[2][0]), (*g_1098)))));
                    for (g_238 = 0; (g_238 >= 46); ++g_238)
                    { /* block id: 1294 */
                        int32_t l_3248 = (-1L);
                        ++g_3249[1];
                        (**g_2058) = &l_5;
                        return (**g_1297);
                    }
                }
                else
                { /* block id: 1299 */
                    float l_3256 = 0x5.5p-1;
                    int32_t l_3257[9][7][4] = {{{1L,(-8L),(-1L),0x4A5A759AL},{(-1L),0L,1L,0x6CD23AD7L},{0x232BAFFBL,1L,0xBD6E580CL,2L},{0xB099A7C2L,0xDC1B7AD5L,(-1L),0x1C435D6FL},{2L,3L,0xD21B3538L,(-8L)},{0xA3A3185FL,(-1L),0xD8FEBE54L,0xD8FEBE54L},{1L,1L,0L,0x95DDD69AL}},{{0xD21B3538L,0xE2DFA36FL,0L,0xBCD996E5L},{1L,(-4L),(-3L),0L},{0x6CD23AD7L,(-4L),(-5L),0xBCD996E5L},{(-4L),0xE2DFA36FL,0x5B80AD66L,0x95DDD69AL},{0xCB98B006L,1L,0xC26177BCL,0xD8FEBE54L},{(-1L),(-1L),0x7D6B68C7L,(-8L)},{(-1L),3L,1L,0x1C435D6FL}},{{0xFF1BBB58L,0xDC1B7AD5L,(-1L),2L},{0xE2DFA36FL,1L,0x90889EDDL,0x6CD23AD7L},{0xAC44C455L,0L,0x99869123L,0x4A5A759AL},{(-4L),(-8L),0x95DDD69AL,0x68F994FDL},{0xF4C5AC0CL,(-3L),0xC7406411L,0xA828698FL},{(-1L),0xCB98B006L,4L,0xF955DFE4L},{(-5L),1L,0x9906EC04L,0xBD6E580CL}},{{1L,0xA828698FL,1L,(-1L)},{0x58AC76FEL,0L,0x68F994FDL,0xCB98B006L},{0L,(-1L),4L,(-1L)},{0xB81CAF00L,1L,(-5L),4L},{0xA828698FL,4L,0xAC44C455L,1L},{(-1L),0x5B80AD66L,0x7D2B85F1L,0xEE855AC4L},{(-1L),0xBD6E580CL,0xAC44C455L,0xC26177BCL}},{{0xA828698FL,0xEE855AC4L,(-5L),0xDC1B7AD5L},{0xB81CAF00L,0xD8FEBE54L,4L,(-4L)},{0L,1L,0x68F994FDL,0xC7406411L},{0x58AC76FEL,(-1L),1L,(-7L)},{0xD21B3538L,0x7D2B85F1L,0x1C435D6FL,4L},{0x4A5A759AL,1L,(-1L),0xD21B3538L},{0xF42A4591L,1L,0L,(-7L)}},{{0x99869123L,0xBD6E580CL,(-5L),1L},{0xFF1BBB58L,0x14340045L,0L,(-5L)},{0x7F15AB6EL,0xFF1BBB58L,0L,0L},{(-1L),0xF4C5AC0CL,0L,4L},{2L,0xE2DFA36FL,(-8L),0xB81CAF00L},{0L,1L,4L,0x1C435D6FL},{1L,0xBCD996E5L,0xBCD996E5L,1L}},{{0x6CD23AD7L,(-1L),(-7L),(-4L)},{0x68F994FDL,0x4A5A759AL,0x7D2B85F1L,(-1L)},{0x90889EDDL,0xA3A3185FL,0xA828698FL,(-1L)},{0xD8FEBE54L,0x4A5A759AL,0xAC44C455L,(-4L)},{0x58AC76FEL,(-1L),(-3L),1L},{(-8L),0xBCD996E5L,0x95DDD69AL,0x1C435D6FL},{0xC7406411L,1L,0x58AC76FEL,0xB81CAF00L}},{{(-1L),0xE2DFA36FL,1L,4L},{0xC26177BCL,0xF4C5AC0CL,0x12DDC880L,0L},{0xCB98B006L,0xFF1BBB58L,(-4L),(-5L)},{0xB81CAF00L,0x14340045L,(-1L),1L},{1L,0xBD6E580CL,1L,(-7L)},{0x12DDC880L,1L,0xA3A3185FL,0xD21B3538L},{0L,1L,0x6CD23AD7L,4L}},{{0xEE855AC4L,0x7D2B85F1L,0x5B80AD66L,(-1L)},{0xA828698FL,1L,(-1L),0L},{0xB23242DDL,(-4L),1L,0x68F994FDL},{(-1L),0x95DDD69AL,(-1L),0xEE855AC4L},{1L,3L,0xD8FEBE54L,0xBCD996E5L},{1L,0x12DDC880L,0xF42A4591L,3L},{0x95DDD69AL,(-7L),0xF42A4591L,0L}}};
                    int i, j, k;
                    (*l_14) ^= ((*l_15) = 9L);
                    for (l_2935 = 0; (l_2935 >= 27); l_2935 = safe_add_func_int32_t_s_s(l_2935, 1))
                    { /* block id: 1304 */
                        (**g_2058) = &l_5;
                        (*l_4) &= (safe_sub_func_int32_t_s_s(l_3257[1][0][1], (safe_mod_func_int16_t_s_s(l_3257[2][0][2], (safe_sub_func_int16_t_s_s(l_3199, (l_3263 , (+0xA1L))))))));
                    }
                    (*l_13) |= l_3257[1][0][1];
                }
                for (l_3150 = (-13); (l_3150 < (-6)); l_3150++)
                { /* block id: 1312 */
                    uint64_t *****l_3288 = &g_3284;
                    int8_t *l_3289 = &g_143;
                    float l_3297 = 0x1.E3043Fp+14;
                    int32_t l_3298 = (-5L);
                    int16_t ***l_3299[4][5][3] = {{{(void*)0,&l_2970,(void*)0},{&l_2970,&l_2970,&g_2971},{&g_2971,&g_2971,&l_2970},{&l_2970,&g_2971,(void*)0},{&g_2971,&l_2970,&g_2971}},{{&l_2970,&l_2970,&g_2971},{&g_2971,&g_2971,&g_2971},{&l_2970,(void*)0,(void*)0},{&l_2970,(void*)0,&l_2970},{&l_2970,&l_2970,&g_2971}},{{&l_2970,&l_2970,(void*)0},{&g_2971,&l_2970,&g_2971},{&l_2970,&l_2970,&l_2970},{&g_2971,&l_2970,&g_2971},{&l_2970,(void*)0,&g_2971}},{{&g_2971,(void*)0,&l_2970},{&l_2970,&g_2971,&g_2971},{(void*)0,&l_2970,(void*)0},{&l_2970,&l_2970,&g_2971},{&g_2971,&g_2971,&l_2970}}};
                    int32_t l_3316 = (-3L);
                    struct S0 ****l_3320 = &g_2074[3];
                    int32_t l_3337 = (-1L);
                    int32_t l_3342 = (-8L);
                    int32_t l_3343 = 0x2BFAF434L;
                    int32_t l_3344 = (-1L);
                    int32_t l_3345 = 0x08C54BC7L;
                    int32_t l_3347[3];
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_3347[i] = 1L;
                    (*l_16) ^= ((*l_4) , (safe_div_func_int8_t_s_s(g_2657[7], (safe_rshift_func_int16_t_s_s(l_3094, 1)))));
                    (*l_13) = (safe_sub_func_uint64_t_u_u((*l_12), ((18446744073709551615UL & (safe_lshift_func_int16_t_s_s((g_1771 = (safe_mod_func_int8_t_s_s((((safe_rshift_func_int16_t_s_u(((((g_3279 , l_3280[0]) == ((*l_3288) = g_3284)) != ((((**l_2970) = (((*l_3289) &= (-1L)) > (safe_mul_func_int8_t_s_s(0L, (((safe_lshift_func_uint16_t_u_s((safe_add_func_int16_t_s_s(l_3088, ((((safe_unary_minus_func_uint8_t_u(((l_3298 >= (**g_2679)) != l_3094))) && 65532UL) & l_3144[1]) >= l_3136))), l_3199)) , (void*)0) == l_3299[0][1][1]))))) && 8UL) | l_3051[2][0])) < l_3300), 15)) ^ l_3152) | 0x051F0DFD3F55DA88LL), l_3204))), 14))) ^ l_3090)));
                }
            }
        }
    }
    else
    { /* block id: 1346 */
        int16_t l_3354 = (-9L);
        union U2 l_3361 = {0x003254DEL};
        int64_t ****l_3393 = &g_2573;
        struct S0 l_3394 = {-0,771};
        uint32_t l_3405 = 0x4CF974D6L;
        int64_t l_3430 = 0x696959F18FA26C1ELL;
        int32_t l_3436 = 0x11E907CAL;
        uint32_t l_3467[2];
        float l_3475[1][5] = {{0x1.5p+1,0x1.5p+1,0x1.5p+1,0x1.5p+1,0x1.5p+1}};
        uint16_t **l_3502 = &l_2843;
        int32_t l_3577 = 6L;
        int32_t l_3582[7][10][2] = {{{7L,1L},{0x72003EE8L,1L},{0x8DB871AAL,(-1L)},{1L,0x429B809AL},{(-1L),3L},{0x3D8DBC9EL,(-1L)},{0x8CC884B0L,(-1L)},{2L,8L},{0x5FA54C09L,0x8CC884B0L},{1L,0x60DA4EFDL}},{{(-1L),0x516A4ABEL},{0xEB1A43F7L,0x8CC884B0L},{(-1L),0x20FA068DL},{2L,2L},{0x8487CC84L,(-1L)},{(-1L),0xBD785B97L},{(-1L),0x54DB8F8DL},{8L,(-1L)},{0x72003EE8L,0xEB1A43F7L},{0x72003EE8L,(-1L)}},{{8L,0x54DB8F8DL},{(-1L),0xBD785B97L},{(-1L),(-1L)},{0x8487CC84L,2L},{2L,0x20FA068DL},{(-1L),0x8CC884B0L},{0xEB1A43F7L,0x516A4ABEL},{(-1L),0x60DA4EFDL},{1L,0x8CC884B0L},{0x5FA54C09L,8L}},{{2L,(-1L)},{0x8CC884B0L,(-1L)},{0x3D8DBC9EL,3L},{(-1L),0x429B809AL},{1L,(-1L)},{0x8DB871AAL,1L},{0x72003EE8L,(-3L)},{1L,0x54DB8F8DL},{(-3L),3L},{(-1L),1L}},{{0x8CC884B0L,2L},{(-1L),8L},{(-1L),0x8487CC84L},{1L,0x516A4ABEL},{1L,0x516A4ABEL},{1L,0x8487CC84L},{(-1L),8L},{(-1L),2L},{0x8CC884B0L,1L},{(-1L),3L}},{{(-3L),0x54DB8F8DL},{1L,(-3L)},{0x72003EE8L,1L},{0x8DB871AAL,(-1L)},{1L,0x429B809AL},{(-1L),3L},{0x3D8DBC9EL,(-1L)},{0x8CC884B0L,(-1L)},{2L,8L},{0x5FA54C09L,0x8CC884B0L}},{{1L,0x60DA4EFDL},{(-1L),0x516A4ABEL},{0xEB1A43F7L,0x8CC884B0L},{(-1L),0x20FA068DL},{2L,2L},{0x8487CC84L,(-1L)},{(-1L),0xBD785B97L},{(-1L),0x54DB8F8DL},{8L,(-1L)},{0x72003EE8L,0xEB1A43F7L}}};
        int16_t l_3585 = (-1L);
        uint8_t l_3604 = 7UL;
        int32_t l_3608 = 0L;
        int32_t *l_3649 = &g_3;
        uint32_t * const ****l_3661 = &g_3660;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_3467[i] = 1UL;
        if (g_1027)
            goto lbl_3351;
        for (g_276 = 28; (g_276 != 7); g_276 = safe_sub_func_uint32_t_u_u(g_276, 7))
        { /* block id: 1350 */
            uint16_t l_3365 = 0xDB65L;
            int64_t l_3366[4] = {(-5L),(-5L),(-5L),(-5L)};
            uint32_t l_3368 = 0xB49DFDB4L;
            int64_t ****l_3392 = (void*)0;
            int32_t l_3396 = (-2L);
            uint32_t l_3455 = 0x9792225EL;
            struct S0 *l_3458 = &l_3394;
            int32_t l_3469[5];
            uint32_t l_3481[7][5][3] = {{{4294967295UL,7UL,4294967288UL},{0x9C7B8466L,4294967294UL,0xEC41E699L},{0x9C7B8466L,0x9C7B8466L,0x3D8FD1EAL},{4294967295UL,0x554B303AL,0x61B6CA78L},{0x3D8FD1EAL,0x205C1F8DL,0xA2F49F47L}},{{4294967293UL,7UL,4294967288UL},{4294967293UL,0x3D8FD1EAL,0xA2F49F47L},{0x6C73FD14L,0x9C7B8466L,0x61B6CA78L},{0x25888E18L,0UL,0x3D8FD1EAL},{0x3D8FD1EAL,0xCC69D2A8L,0xEC41E699L}},{{4294967288UL,0xCC69D2A8L,4294967288UL},{4294967295UL,0UL,0UL},{0x369348A3L,0x9C7B8466L,0UL},{1UL,0x3D8FD1EAL,0x554B303AL},{0x3D8FD1EAL,7UL,0x61EA508EL}},{{1UL,0x205C1F8DL,4294967288UL},{0x369348A3L,0x25888E18L,0x205C1F8DL},{0UL,0x3D8FD1EAL,4294967288UL},{0x61EA508EL,4294967288UL,4294967288UL},{4294967293UL,4294967295UL,0x205C1F8DL}},{{0xA2F49F47L,0x369348A3L,0x61EA508EL},{0x554B303AL,1UL,7UL},{0x61B6CA78L,0x3D8FD1EAL,0x25888E18L},{0x224A2026L,1UL,4294967295UL},{4294967293UL,0x369348A3L,7UL}},{{0UL,4294967295UL,0x61EA508EL},{0x3D8FD1EAL,4294967288UL,0xCC69D2A8L},{0x3D8FD1EAL,0x3D8FD1EAL,4294967293UL},{0UL,0x25888E18L,1UL},{4294967293UL,0x6C73FD14L,0x1C9AB984L}},{{0x224A2026L,4294967293UL,0x61EA508EL},{0x61B6CA78L,4294967293UL,0x1C9AB984L},{0x554B303AL,0x3D8FD1EAL,1UL},{0xA2F49F47L,4294967295UL,4294967293UL},{4294967293UL,0x9C7B8466L,0xCC69D2A8L}}};
            uint8_t l_3485 = 255UL;
            uint32_t **l_3497 = &g_2262;
            const uint32_t l_3510 = 18446744073709551615UL;
            int16_t l_3550 = 0x8EFAL;
            uint8_t l_3596 = 255UL;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_3469[i] = (-1L);
            if (l_3354)
                break;
        }
        for (g_445.f0 = 0; (g_445.f0 <= 1); g_445.f0 += 1)
        { /* block id: 1465 */
            float l_3607 = 0x1.0p-1;
            int32_t l_3609[9] = {(-1L),0x555DC9C0L,(-1L),0x555DC9C0L,(-1L),0x555DC9C0L,(-1L),0x555DC9C0L,(-1L)};
            int8_t l_3614 = (-2L);
            int16_t l_3615 = 0x47B3L;
            int64_t l_3616 = (-6L);
            uint16_t l_3617 = 0xE5B8L;
            struct S0 l_3650 = {-0,268};
            int i;
            --l_3617;
            for (l_3611 = 0; (l_3611 <= 1); l_3611 += 1)
            { /* block id: 1469 */
                uint8_t *****l_3620 = &g_375;
                int32_t *l_3648 = (void*)0;
                const uint8_t l_3654 = 0x3FL;
                int i;
                if (l_3143[g_445.f0])
                    break;
                (*l_12) ^= (l_3620 == (void*)0);
                for (g_238 = (-3); (g_238 == 42); g_238++)
                { /* block id: 1474 */
                    int32_t l_3629[10] = {0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L,0xD28438C1L};
                    int32_t l_3643 = 0xB9D33614L;
                    int i;
                    for (g_1107 = 0; (g_1107 <= 3); g_1107 += 1)
                    { /* block id: 1477 */
                        float *l_3644 = (void*)0;
                        float *l_3645 = &g_3465;
                        float *l_3646 = &g_193[0];
                        int i, j;
                        (*l_14) &= (safe_mod_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((safe_add_func_uint64_t_u_u(g_2657[(l_3611 + 2)], (l_3629[0] && ((((l_3354 ^ (*g_2740)) | ((safe_sub_func_int64_t_s_s((~(safe_unary_minus_func_int16_t_s(g_145[4]))), (safe_lshift_func_int8_t_s_s((g_2657[(l_3611 + 2)] >= (((((l_3629[0] = (*l_12)) < (safe_lshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(0x1842DAA7L, 0x4B9A217EL)), 1)), g_2657[(l_3611 + 2)]))) & 0x3FL) > 0x46L) != 0x7AL)), 7)))) < 0xDF6028F5CB00582DLL)) != 0x94CA2D2ACFAE050CLL) >= g_3642)))), 3)), l_3643));
                        (*l_3646) = ((*l_3645) = 0x7.959B9Ap-70);
                    }
                    l_3629[0] = ((l_3647 = ((**g_2572) = (**g_2572))) == (void*)0);
                    l_3649 = l_3648;
                    (*l_12) ^= ((l_3608 , (l_3650 , (*l_9))) != (0xF8071EEFL < (safe_rshift_func_uint16_t_u_s((l_3616 | ((((~l_3654) | ((safe_mul_func_uint16_t_u_u(((((safe_lshift_func_uint8_t_u_s((****g_375), (**g_3428))) < 0x41L) != l_3629[0]) >= l_3643), 0x08C7L)) , l_3643)) & (*g_3429)) | (*g_3509))), 8))));
                }
            }
        }
        (*l_3661) = &g_2678;
    }
    return (*g_1298);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t  func_26(uint64_t  p_27)
{ /* block id: 1150 */
    int32_t *l_2841 = &g_1605;
    int32_t l_2842 = 0L;
    l_2841 = l_2841;
    return l_2842;
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_52 g_64 g_1107 g_8 g_1026 g_1771 g_3 g_54 g_125 g_58 g_145 g_91 g_168 g_198 g_221 g_11 g_238 g_239 g_241 g_143 g_232 g_270 g_276 g_314 g_316 g_375 g_404 g_405 g_377 g_378 g_376 g_7 g_2657 g_508.f0 g_1908 g_2059
 * writes: g_58 g_64 g_11 g_1107 g_54 g_125 g_91 g_143 g_145 g_168 g_221 g_232 g_238 g_239 g_241 g_193 g_276 g_316 g_375 g_403 g_314 g_445 g_2657
 */
static int32_t  func_35(int16_t  p_36, uint8_t  p_37)
{ /* block id: 6 */
    int32_t *l_55 = (void*)0;
    int32_t **l_56 = &l_55;
    int64_t l_62 = 0x01FF3C8D209D0817LL;
    uint32_t *l_63 = &g_64;
    (*g_57) = ((*l_56) = l_55);
    (*g_2059) = func_59(g_52, ((*l_63) = (l_62 < p_37)));
    return p_37;
}


/* ------------------------------------------ */
/* 
 * reads : g_43 g_46
 * writes: g_43
 */
static union U1  func_38(int32_t  p_39)
{ /* block id: 2 */
    int32_t *l_40 = &g_3;
    int32_t *l_41[7][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_3,(void*)0,&g_3,(void*)0,&g_3},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_3,(void*)0,&g_3,(void*)0,&g_3},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_3,(void*)0,&g_3,(void*)0,&g_3},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    float l_42 = 0xA.DBEFC7p-23;
    int i, j;
    --g_43[2][2][8];
    return g_46;
}


/* ------------------------------------------ */
/* 
 * reads : g_64 g_1107 g_8 g_1026 g_1771 g_3 g_54 g_125 g_58 g_145 g_91 g_168 g_198 g_221 g_11 g_238 g_239 g_241 g_143 g_232 g_270 g_276 g_314 g_316 g_375 g_404 g_405 g_377 g_378 g_376 g_7 g_2657 g_508.f0 g_1908 g_57
 * writes: g_64 g_11 g_1107 g_54 g_125 g_58 g_91 g_143 g_145 g_168 g_221 g_232 g_238 g_239 g_241 g_193 g_276 g_316 g_375 g_403 g_314 g_445 g_2657
 */
static int32_t * func_59(struct S0  p_60, uint32_t  p_61)
{ /* block id: 10 */
    int32_t *l_65 = &g_3;
    int32_t **l_67 = &l_65;
    uint8_t ***l_2296 = (void*)0;
    uint8_t l_2304 = 0xB8L;
    int32_t l_2309[1][10] = {{5L,0xF0C4E888L,5L,0xF0C4E888L,5L,0xF0C4E888L,5L,0xF0C4E888L,5L,0xF0C4E888L}};
    uint64_t * const ****l_2369 = (void*)0;
    int32_t l_2378[1][6] = {{(-1L),0x9C77487FL,(-1L),(-1L),0x9C77487FL,(-1L)}};
    struct S0 l_2391 = {0,-721};
    int64_t *l_2429[4] = {&g_232,&g_232,&g_232,&g_232};
    uint16_t *l_2441 = &g_1859;
    uint8_t l_2464[2][9] = {{255UL,0x70L,3UL,0x70L,255UL,0x25L,0x25L,255UL,0x70L},{0x6EL,249UL,0x6EL,0x25L,3UL,3UL,0x25L,0x6EL,249UL}};
    uint32_t l_2526 = 0UL;
    uint32_t ***l_2567 = (void*)0;
    uint32_t l_2581 = 4294967290UL;
    union U2 *l_2697 = &g_314[1];
    uint16_t l_2807 = 0x8417L;
    int i, j;
    (*l_67) = l_65;
    for (g_64 = 0; (g_64 == 31); g_64++)
    { /* block id: 14 */
        int8_t *l_101 = &g_8[1];
        int32_t l_124 = 0xC1F9A208L;
        int32_t l_2277 = 0x4E121556L;
        int32_t l_2319 = 9L;
        int32_t l_2325 = 0xE6D70E51L;
        int32_t l_2330 = 0xD1AC5971L;
        int32_t l_2332 = (-7L);
        int32_t l_2334 = 0x9185341AL;
        int32_t l_2338 = 0L;
        int32_t l_2341 = 0x387112B9L;
        struct S0 *l_2435 = &g_1727;
        struct S0 **l_2434[3][3] = {{&l_2435,&l_2435,&l_2435},{&l_2435,&l_2435,&l_2435},{&l_2435,&l_2435,&l_2435}};
        uint16_t *l_2440 = &g_221;
        uint8_t ****l_2472 = &g_405;
        uint8_t * const l_2496 = &g_2497;
        uint8_t * const *l_2495[3][2][7] = {{{&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496},{&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496}},{{&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496},{&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496}},{{&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496},{&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496,&l_2496}}};
        int64_t ****l_2576 = &g_2573;
        uint32_t l_2605 = 0x259C2AECL;
        int32_t l_2651[7] = {5L,5L,5L,5L,5L,5L,5L};
        int16_t *l_2675 = &g_168;
        uint8_t l_2677 = 0x02L;
        int32_t l_2696 = 0x11B2FD69L;
        uint16_t l_2737 = 1UL;
        uint64_t l_2770 = 0x26710FFB7FCD5A7DLL;
        uint8_t l_2771[7] = {1UL,254UL,254UL,1UL,254UL,254UL,1UL};
        uint8_t l_2801 = 4UL;
        uint32_t l_2824[5][1][7] = {{{4294967292UL,0UL,4294967293UL,0xC4A651F4L,4294967293UL,0UL,4294967292UL}},{{0x5DEF99C1L,0UL,0x6717778BL,0x781C230AL,0xC4A651F4L,0UL,4294967292UL}},{{0xC4A651F4L,4294967292UL,0x6B65CEACL,0x6B65CEACL,4294967292UL,0xC4A651F4L,0x5DEF99C1L}},{{0UL,0x6B65CEACL,0x6717778BL,0x5DEF99C1L,0xE2CCA3CCL,0xC4A651F4L,0xC4A651F4L}},{{0x781C230AL,0xE2CCA3CCL,4294967293UL,0xE2CCA3CCL,0x781C230AL,0UL,0UL}}};
        int i, j, k;
        for (g_11 = 0; (g_11 >= 16); g_11++)
        { /* block id: 17 */
            int8_t l_89 = 0xA5L;
            uint32_t l_92 = 0x5DC1C47FL;
            float *l_2289 = &g_193[0];
            const uint16_t *l_2301 = (void*)0;
            int32_t l_2305 = 0L;
            int32_t l_2306 = 0xA23D7F4DL;
            int32_t l_2311 = 0x254B497DL;
            float l_2314 = 0x4.82281Fp+25;
            int32_t l_2316[10];
            struct S0 l_2405 = {-0,18};
            int8_t l_2428 = 0x71L;
            union U2 l_2467 = {1L};
            int64_t *l_2521 = &g_232;
            uint32_t l_2586 = 18446744073709551615UL;
            int32_t *l_2596[9];
            int32_t l_2628 = (-1L);
            int i;
            for (i = 0; i < 10; i++)
                l_2316[i] = 3L;
            for (i = 0; i < 9; i++)
                l_2596[i] = &l_2305;
        }
        p_60.f1 ^= p_61;
        if ((g_1107 > p_60.f0))
        { /* block id: 1069 */
            int64_t l_2645 = 0xA1B0D18BC3AC4F9ELL;
            int32_t *l_2646 = &l_2325;
            int32_t *l_2647 = &g_3;
            int32_t *l_2648 = &g_3;
            int32_t *l_2649 = &g_1605;
            int32_t *l_2650[7][9][4] = {{{&g_314[7].f0,&l_2338,&l_2338,&g_508.f0},{&g_2090.f0,&l_2325,&l_2309[0][0],(void*)0},{&l_2277,&l_124,&g_2090.f0,&g_445.f0},{&g_2090.f0,&g_3,&g_3,(void*)0},{&l_2341,&l_2338,(void*)0,&l_2309[0][4]},{(void*)0,&g_508.f0,(void*)0,(void*)0},{&l_124,&l_2309[0][4],&l_2319,&l_2325},{&l_2330,&l_2277,&g_239,&l_2341},{&l_2277,&l_124,&l_2341,&g_3}},{{&g_3,&l_2325,&g_3,&l_2338},{&g_1605,&g_314[7].f0,(void*)0,&l_2277},{&g_314[7].f0,&l_2330,&l_2309[0][4],&l_2330},{&l_124,&g_239,&l_2277,&g_239},{&l_2325,&l_2309[0][4],&l_2341,&l_124},{&l_2338,(void*)0,&g_314[7].f0,&g_508.f0},{&l_2338,&g_3,&l_2341,(void*)0},{&l_2325,&g_508.f0,&l_2277,&l_124},{&l_124,&l_2332,&l_2309[0][4],(void*)0}},{{&g_314[7].f0,&l_2319,(void*)0,&l_2319},{&g_1605,&g_314[7].f0,&g_3,&l_2325},{&g_3,&g_239,&l_2341,&l_124},{&l_2277,&g_1605,&g_239,&g_3},{&l_2330,(void*)0,&l_2319,(void*)0},{&l_124,&l_2330,(void*)0,&g_3},{(void*)0,&g_314[7].f0,(void*)0,&l_2309[0][4]},{&l_2341,(void*)0,&g_3,(void*)0},{&g_2090.f0,&l_124,&g_2090.f0,&l_2338}},{{&l_2277,&l_2330,&l_2309[0][0],&g_3},{&g_2090.f0,&l_2341,&l_2338,&l_2341},{&g_314[7].f0,(void*)0,(void*)0,&g_314[7].f0},{&l_2277,&g_508.f0,(void*)0,&l_2309[0][2]},{(void*)0,&l_2277,&g_239,(void*)0},{(void*)0,&l_2334,&l_2309[0][4],(void*)0},{(void*)0,&l_2309[0][4],&l_2341,(void*)0},{&g_1605,&g_2090.f0,&l_2309[0][4],&g_3},{&g_508.f0,&g_508.f0,(void*)0,(void*)0}},{{&l_2319,&l_124,&g_1605,&l_124},{(void*)0,&g_2090.f0,&l_2309[0][0],&g_239},{(void*)0,&l_124,&l_2334,&g_314[7].f0},{&l_2277,&l_2277,(void*)0,&l_2277},{&l_124,&g_508.f0,(void*)0,(void*)0},{(void*)0,&g_3,&l_124,&l_2325},{&l_2334,&l_2309[0][0],&l_2341,&l_124},{&l_2319,&g_239,(void*)0,&l_2309[0][4]},{&g_508.f0,&g_314[7].f0,(void*)0,&l_2338}},{{&l_2325,&g_3,&l_2338,&l_2319},{&l_2309[0][2],(void*)0,&g_3,(void*)0},{&l_2341,(void*)0,&l_2341,&g_508.f0},{&l_124,&l_2325,&l_2332,&g_508.f0},{&l_124,&l_2334,&l_2330,&l_2325},{(void*)0,&l_2325,&l_2330,&l_2338},{&l_124,&l_2277,&l_2332,&g_3},{&l_124,(void*)0,&l_2341,&l_2330},{&l_2341,&l_2330,&g_3,(void*)0}},{{&l_2309[0][2],&l_2341,&l_2338,&l_2330},{&l_2325,&g_314[7].f0,(void*)0,&l_2334},{&g_508.f0,&l_2338,(void*)0,&l_124},{&l_2319,(void*)0,&l_2341,&g_314[7].f0},{&l_2334,&l_2325,&l_124,&l_124},{(void*)0,&l_2277,(void*)0,&g_314[7].f0},{&l_124,&l_2341,(void*)0,&g_3},{&l_2277,&l_124,&l_2334,(void*)0},{(void*)0,&g_508.f0,&l_2309[0][0],&l_2309[0][2]}}};
            uint64_t l_2652 = 0x3462E66A7F702173LL;
            int8_t *l_2655[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i, j, k;
            for (g_1107 = 0; (g_1107 <= 1); g_1107 += 1)
            { /* block id: 1072 */
                int i, j;
                if (l_2464[g_1107][(g_1107 + 3)])
                    break;
            }
            l_2391 = p_60;
            l_2652++;
            l_2649 = func_113((l_2330 ^= (0L < ((g_8[7] <= (g_1026 > g_1771)) && (*l_65)))), p_60, (*l_67));
        }
        else
        { /* block id: 1079 */
            int32_t *l_2656[4];
            union U2 ****l_2660[5];
            union U2 *****l_2661 = &l_2660[0];
            struct S0 l_2662 = {0,-855};
            int i;
            for (i = 0; i < 4; i++)
                l_2656[i] = &g_508.f0;
            for (i = 0; i < 5; i++)
                l_2660[i] = (void*)0;
            --g_2657[7];
            (*g_1908) = func_113(((p_61 , &g_1623) != ((*l_2661) = l_2660[3])), l_2662, l_2656[3]);
        }
    }
    return (*g_57);
}


/* ------------------------------------------ */
/* 
 * reads : g_1678 g_1605 g_64 g_1396 g_1397 g_7 g_1795 g_1771 g_1298 g_1299 g_1297 g_54 g_378 g_1823 g_143 g_11 g_239 g_43 g_46 g_1496 g_52.f0 g_91 g_276 g_57 g_58 g_1908 g_8 g_1915 g_1409 g_1410 g_145 g_973 g_1027 g_1960 g_1915.f0 g_1969 g_1098 g_314.f0 g_1026 g_1974 g_976 g_193 g_168 g_270 g_1823.f0 g_376 g_377 g_2056 g_1550 g_1622 g_1623 g_2074 g_2059 g_2090 g_1395 g_1292 g_1107 g_241.f1 g_404 g_405 g_838.f0 g_820.f0 g_445.f0 g_2058 g_314 g_232 g_221 g_1859 g_2262 g_46.f0 g_238
 * writes: g_1678 g_238 g_1299 g_145 g_239 g_973 g_43 g_193 g_1859 g_125 g_232 g_168 g_58 g_1771 g_54 g_1292 g_221 g_2056 g_1550 g_1026 g_976 g_314 g_445 g_143 g_1107 g_2261 g_270
 */
static int8_t  func_78(struct S0  p_79, uint32_t  p_80, const int8_t  p_81, uint32_t * p_82, int16_t * p_83)
{ /* block id: 703 */
    uint8_t *l_1772 = &g_145[4];
    int32_t l_1796[10][4][2] = {{{0L,0x7FE5A6A0L},{0x2149FAFCL,0L},{0x0D79EA4EL,0x0D79EA4EL},{0x0D79EA4EL,0L}},{{0x2149FAFCL,0x7FE5A6A0L},{0L,0x7FE5A6A0L},{0x2149FAFCL,0L},{0x0D79EA4EL,0x0D79EA4EL}},{{0x0D79EA4EL,0L},{0x2149FAFCL,0x7FE5A6A0L},{0L,0x7FE5A6A0L},{0x2149FAFCL,0L}},{{0x0D79EA4EL,0x0D79EA4EL},{0x0D79EA4EL,0L},{0x2149FAFCL,0x7FE5A6A0L},{0L,0x7FE5A6A0L}},{{0x2149FAFCL,0L},{0x0D79EA4EL,0x0D79EA4EL},{0x0D79EA4EL,0L},{0x2149FAFCL,0x7FE5A6A0L}},{{0L,0x7FE5A6A0L},{0x2149FAFCL,0L},{0x0D79EA4EL,0x0D79EA4EL},{0x0D79EA4EL,0L}},{{0x2149FAFCL,0x7FE5A6A0L},{0L,0x7FE5A6A0L},{0x2149FAFCL,0L},{0x0D79EA4EL,0x0D79EA4EL}},{{0x0D79EA4EL,0L},{0x2149FAFCL,0x7FE5A6A0L},{0L,0x7FE5A6A0L},{0x2149FAFCL,0L}},{{0L,0L},{0L,0x2149FAFCL},{6L,0x0D79EA4EL},{0x2149FAFCL,0x0D79EA4EL}},{{6L,0x2149FAFCL},{0L,0L},{0L,0x2149FAFCL},{6L,0x0D79EA4EL}}};
    uint64_t l_1804[9][4] = {{18446744073709551615UL,18446744073709551615UL,0x9FD6A4A224E154CFLL,0x47D3762D3A36AE3ALL},{0x47D3762D3A36AE3ALL,0x7E062FF41949FA55LL,0x9FD6A4A224E154CFLL,0x7E062FF41949FA55LL},{18446744073709551615UL,0x522E553152F60FDDLL,18446744073709551615UL,0x9FD6A4A224E154CFLL},{0x7E062FF41949FA55LL,0x522E553152F60FDDLL,0x522E553152F60FDDLL,0x7E062FF41949FA55LL},{0x522E553152F60FDDLL,0x7E062FF41949FA55LL,18446744073709551615UL,0x47D3762D3A36AE3ALL},{0x522E553152F60FDDLL,18446744073709551615UL,0x522E553152F60FDDLL,18446744073709551615UL},{0x7E062FF41949FA55LL,0x47D3762D3A36AE3ALL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0x9FD6A4A224E154CFLL,0x47D3762D3A36AE3ALL},{0x47D3762D3A36AE3ALL,0x7E062FF41949FA55LL,0x9FD6A4A224E154CFLL,0x7E062FF41949FA55LL}};
    union U2 l_1817 = {0x2E6A688AL};
    union U2 *l_1941 = &g_314[7];
    union U2 **l_1940 = &l_1941;
    const uint64_t *l_1952 = (void*)0;
    uint64_t ****l_1989 = (void*)0;
    uint64_t *****l_1988 = &l_1989;
    int32_t l_1990 = 1L;
    const uint8_t ***l_2001 = &g_1409;
    const uint8_t ****l_2000 = &l_2001;
    int32_t *l_2007 = &l_1796[4][2][0];
    int32_t ***l_2037 = &g_583;
    uint16_t l_2196 = 5UL;
    uint8_t l_2207 = 0x72L;
    uint32_t *l_2258 = &g_276;
    uint32_t **l_2257 = &l_2258;
    uint32_t **l_2260 = &l_2258;
    uint32_t ***l_2259[3][7][4] = {{{&l_2260,&l_2260,&l_2260,&l_2257},{&l_2260,&l_2257,&l_2257,&l_2257},{&l_2257,&l_2257,&l_2257,&l_2260},{&l_2257,&l_2257,&l_2257,&l_2260},{&l_2257,&l_2260,&l_2257,&l_2257},{&l_2257,&l_2260,&l_2257,&l_2260},{&l_2260,&l_2257,&l_2260,&l_2260}},{{&l_2257,&l_2257,(void*)0,&l_2257},{&l_2260,&l_2257,&l_2260,&l_2257},{&l_2260,&l_2260,&l_2260,&l_2260},{&l_2260,(void*)0,(void*)0,&l_2260},{&l_2257,&l_2260,&l_2260,&l_2260},{&l_2260,&l_2260,&l_2257,&l_2260},{&l_2257,&l_2260,&l_2257,&l_2260}},{{&l_2257,&l_2260,&l_2257,&l_2260},{&l_2257,&l_2260,&l_2257,&l_2260},{&l_2257,(void*)0,&l_2257,&l_2260},{&l_2260,&l_2260,&l_2260,&l_2257},{&l_2260,&l_2257,&l_2257,&l_2257},{&l_2257,&l_2257,&l_2257,&l_2260},{&l_2257,&l_2257,&l_2257,&l_2260}}};
    uint16_t *l_2267 = (void*)0;
    uint16_t *l_2268 = &l_2196;
    uint16_t *l_2275 = &g_270;
    uint64_t l_2276 = 0x0CD1DBDECF972611LL;
    int i, j, k;
    if ((l_1772 != l_1772))
    { /* block id: 704 */
        union U2 ** const *l_1786 = (void*)0;
        union U2 ** const **l_1785[4];
        uint32_t *l_1789 = &g_1678;
        int32_t l_1794[5];
        int32_t l_1797 = (-8L);
        int32_t *l_1798 = &l_1797;
        int i;
        for (i = 0; i < 4; i++)
            l_1785[i] = &l_1786;
        for (i = 0; i < 5; i++)
            l_1794[i] = 0x14B6C138L;
        (*l_1798) = (((safe_mul_func_uint8_t_u_u(((((safe_mod_func_int8_t_s_s(((safe_add_func_uint32_t_u_u(((safe_lshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u(((void*)0 != l_1785[2]), ((safe_add_func_uint32_t_u_u(0x5F90E3E9L, 8L)) > ((p_79.f0 < ((*l_1789)--)) <= g_1605)))), (safe_rshift_func_int16_t_s_u(l_1794[3], (((g_64 || ((*g_1396) , 0x076BL)) != 0x60DB102D2ECFD423LL) >= p_79.f0))))), 13)) ^ p_80), p_79.f1)) <= 8UL), 0x26L)) < g_7[0][0][3]) > g_1795) == l_1796[5][0][0]), l_1797)) , (*p_83)) <= 0x20C7L);
        return l_1796[4][3][1];
    }
    else
    { /* block id: 708 */
        int32_t *l_1799 = &l_1796[2][3][1];
        int32_t *l_1800 = &g_239;
        int32_t *l_1801 = (void*)0;
        int32_t *l_1802 = (void*)0;
        int32_t *l_1803[5][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        union U2 l_1815 = {0x7F8172C1L};
        struct S0 l_1906 = {-0,741};
        const uint8_t l_1912 = 0x38L;
        int32_t l_1938 = 0x1B49F1C3L;
        int32_t ***l_2014 = (void*)0;
        union U2 *l_2091 = &g_445;
        int8_t *l_2092 = &g_143;
        uint16_t *l_2101 = &g_1859;
        uint16_t *l_2102 = &g_221;
        uint16_t l_2111 = 1UL;
        uint64_t *l_2133[8] = {&g_238,&g_238,&g_238,&g_238,&g_238,&g_238,&g_238,&g_238};
        uint32_t l_2179 = 0x559D2EE4L;
        uint64_t l_2200[9][2] = {{3UL,18446744073709551615UL},{0x37069E5F9C6A002BLL,0x37069E5F9C6A002BLL},{18446744073709551615UL,0x37069E5F9C6A002BLL},{0x37069E5F9C6A002BLL,18446744073709551615UL},{0x37069E5F9C6A002BLL,0x37069E5F9C6A002BLL},{18446744073709551615UL,0x37069E5F9C6A002BLL},{0x37069E5F9C6A002BLL,18446744073709551615UL},{0x37069E5F9C6A002BLL,0x37069E5F9C6A002BLL},{18446744073709551615UL,0x37069E5F9C6A002BLL}};
        int64_t * const ** const * const l_2221 = &g_1090;
        int i, j;
lbl_1839:
        l_1804[3][3]++;
        for (g_238 = 13; (g_238 > 45); g_238 = safe_add_func_int16_t_s_s(g_238, 1))
        { /* block id: 712 */
            union U2 *l_1816[5][6] = {{&g_508,&g_508,&g_508,&g_508,&g_508,&g_508},{&g_508,&g_508,&g_508,&g_508,&g_508,&g_508},{&g_508,&g_508,&g_508,&g_508,&g_508,&g_508},{&g_508,&g_508,&g_508,&g_508,&g_508,&g_508},{&g_508,&g_508,&g_508,&g_508,&g_508,&g_508}};
            uint8_t ****l_1818 = &g_376;
            uint8_t *** const *l_1819 = &g_405;
            int32_t l_1820 = 0x2F54D150L;
            int16_t l_1880 = (-4L);
            int32_t l_1888[10][4] = {{0xB24B6489L,0x953AEA99L,(-10L),0x953AEA99L},{0xCE1D5A74L,(-10L),0xA2296E65L,(-10L)},{0x953AEA99L,(-10L),(-10L),0x953AEA99L},{(-10L),0x953AEA99L,0xCE1D5A74L,0xB24B6489L},{(-10L),0xCE1D5A74L,(-10L),0xA2296E65L},{0x953AEA99L,0xB24B6489L,0xA2296E65L,0xA2296E65L},{0xCE1D5A74L,0xCE1D5A74L,(-10L),0xB24B6489L},{0xB24B6489L,0x953AEA99L,(-10L),0x953AEA99L},{0xCE1D5A74L,(-10L),0xA2296E65L,(-10L)},{0x953AEA99L,(-10L),(-10L),0x953AEA99L}};
            int32_t *l_1907[4][1] = {{&l_1815.f0},{&g_3},{&l_1815.f0},{&g_3}};
            struct S0 *l_1937[8] = {&g_1727,&g_1727,&g_1727,&g_1727,&g_1727,&g_1727,&g_1727,&g_1727};
            struct S0 **l_1936[7];
            struct S0 ***l_1935 = &l_1936[6];
            uint64_t *l_1959 = &g_976;
            int32_t **l_1975[2][6][1] = {{{&l_1801},{(void*)0},{(void*)0},{(void*)0},{&l_1801},{&l_1801}},{{(void*)0},{(void*)0},{(void*)0},{&l_1801},{&l_1801},{(void*)0}}};
            const union U2 *l_1982[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            const union U2 * const *l_1981 = &l_1982[0];
            const union U2 * const **l_1980 = &l_1981;
            const uint64_t l_1987 = 0x76F455262DE29D91LL;
            int32_t *l_1991[6][4];
            uint16_t l_2003 = 65530UL;
            union U2 *l_2060 = &g_314[7];
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_1936[i] = &l_1937[7];
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 4; j++)
                    l_1991[i][j] = &l_1888[7][1];
            }
            (**g_1297) = (*g_1298);
            if (((((l_1820 = (((*l_1800) = (safe_mul_func_uint64_t_u_u(((g_54 != ((*g_378) = 7UL)) < (safe_sub_func_uint64_t_u_u(1UL, ((safe_add_func_uint16_t_u_u(l_1796[6][2][1], ((((l_1817 = l_1815) , (l_1804[1][3] , l_1818)) == l_1819) & p_79.f1))) < l_1796[5][0][0])))), 0L))) & p_79.f0)) , (void*)0) == (void*)0) & 0x9FA1E372L))
            { /* block id: 718 */
                uint32_t *l_1833 = &g_973;
                int32_t l_1838 = 0x0BA10C7AL;
                for (g_973 = 14; (g_973 == 22); g_973++)
                { /* block id: 721 */
                    uint32_t *l_1832 = (void*)0;
                    uint32_t **l_1834 = &l_1833;
                    int32_t l_1837 = (-1L);
                    l_1838 |= ((func_38((g_1823[2] , (0x87C42F8DL & (safe_add_func_int64_t_s_s(g_143, (safe_lshift_func_uint16_t_u_s(65535UL, ((((safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s(p_81, ((((p_81 ^ ((l_1832 != ((*l_1834) = l_1833)) == ((safe_div_func_int8_t_s_s(((void*)0 != &g_1297), 0x13L)) == p_79.f0))) && (-4L)) & g_11) == g_239))), 1L)) , l_1837) || p_79.f1) == p_80)))))))) , &g_1298) == (void*)0);
                }
                if (g_239)
                    goto lbl_1839;
            }
            else
            { /* block id: 726 */
                uint32_t l_1842 = 0UL;
                uint8_t l_1850 = 0xB7L;
                float *l_1854[9] = {&g_193[0],(void*)0,&g_193[0],(void*)0,&g_193[0],(void*)0,&g_193[0],(void*)0,&g_193[0]};
                uint32_t l_1860[9] = {1UL,8UL,1UL,1UL,8UL,1UL,1UL,8UL,1UL};
                uint8_t l_1872 = 255UL;
                int32_t *l_1877 = &l_1817.f0;
                int32_t l_1881 = (-8L);
                int32_t l_1885 = 0xB40EE34AL;
                int32_t l_1892 = 0x30B89347L;
                int32_t l_1893 = 9L;
                uint32_t l_1894[3];
                struct S0 *l_1934[8];
                struct S0 **l_1933 = &l_1934[0];
                struct S0 ***l_1932 = &l_1933;
                union U2 * const *l_1979[3];
                union U2 * const **l_1978 = &l_1979[1];
                int i;
                for (i = 0; i < 3; i++)
                    l_1894[i] = 0x2E3628CAL;
                for (i = 0; i < 8; i++)
                    l_1934[i] = &g_1727;
                for (i = 0; i < 3; i++)
                    l_1979[i] = (void*)0;
                if (((((safe_lshift_func_int16_t_s_s(((l_1842 != (safe_div_func_float_f_f((g_1859 = ((safe_mul_func_float_f_f((safe_sub_func_float_f_f((-0x5.Ap+1), ((safe_unary_minus_func_uint64_t_u((p_79.f1 , p_79.f0))) , ((l_1850 <= (0xB.E91C31p-30 >= (((safe_div_func_float_f_f(0x3.FF8544p+16, (+(g_193[0] = 0x8.0p-1)))) <= ((safe_sub_func_float_f_f((safe_div_func_float_f_f((l_1796[5][0][0] = p_81), p_79.f1)), 0x4.Ep-1)) >= g_7[0][0][3])) < g_1496))) != g_1299[1].f0)))), p_79.f0)) == 0xB.8989B0p+10)), p_80))) , g_52.f0), 6)) , l_1860[5]) , 2L) ^ l_1860[5]))
                { /* block id: 730 */
                    uint32_t l_1863 = 0xE72C8411L;
                    int32_t l_1868 = 1L;
                    uint8_t l_1869 = 0x47L;
                    int32_t **l_1876[7] = {&l_1803[3][3],&l_1803[3][3],&l_1803[3][3],&l_1803[3][3],&l_1803[3][3],&l_1803[3][3],&l_1803[3][3]};
                    union U2 *l_1903 = &g_314[7];
                    int i;
                    if ((safe_lshift_func_uint8_t_u_s(((*g_378) = ((l_1863 <= ((safe_unary_minus_func_uint64_t_u((((l_1860[5] && (((*l_1799) = p_80) | p_81)) && ((safe_mul_func_uint16_t_u_u((g_91 & 7UL), 4UL)) & (+l_1842))) , 1UL))) > 1UL)) & 4294967295UL)), 2)))
                    { /* block id: 733 */
                        l_1869++;
                        return g_276;
                    }
                    else
                    { /* block id: 736 */
                        int32_t **l_1875 = &g_125[2];
                        l_1872--;
                        (*l_1875) = l_1854[6];
                        (**g_1297) = (p_79.f1 , (**g_1297));
                        if (l_1869)
                            break;
                    }
                    l_1877 = &l_1820;
                    for (g_232 = 0; (g_232 >= (-28)); g_232 = safe_sub_func_uint64_t_u_u(g_232, 4))
                    { /* block id: 745 */
                        int32_t l_1882 = 0xE4091D8AL;
                        int32_t l_1883 = 0x47DD2169L;
                        int32_t l_1884 = 0x361982DBL;
                        int16_t l_1886 = 0x57A6L;
                        int32_t l_1887 = 0xEF8AB52AL;
                        int32_t l_1889 = 1L;
                        int32_t l_1890 = (-1L);
                        int32_t l_1891 = 1L;
                        int32_t *l_1897 = &l_1893;
                        l_1894[0]--;
                        l_1897 = (*g_57);
                    }
                    for (g_239 = 18; (g_239 < (-29)); g_239 = safe_sub_func_uint64_t_u_u(g_239, 7))
                    { /* block id: 751 */
                        uint16_t l_1900 = 1UL;
                        union U2 **l_1905 = &l_1816[3][0];
                        ++l_1900;
                        (*l_1905) = l_1903;
                    }
                }
                else
                { /* block id: 755 */
                    int32_t l_1911[7] = {4L,4L,4L,4L,4L,4L,4L};
                    uint64_t *l_1957 = &l_1804[6][1];
                    int i;
                    for (g_168 = 0; (g_168 <= 2); g_168 += 1)
                    { /* block id: 758 */
                        (*g_1908) = (l_1906 , l_1907[0][0]);
                        (*g_57) = (*g_57);
                    }
                    (*l_1799) |= (safe_mod_func_uint32_t_u_u(l_1911[2], l_1912));
                    for (g_168 = 6; (g_168 == 6); g_168 = safe_add_func_uint16_t_u_u(g_168, 6))
                    { /* block id: 765 */
                        return g_8[1];
                    }
                    if ((((*g_1298) , g_1915) , ((safe_mul_func_uint8_t_u_u(l_1911[1], (safe_sub_func_uint64_t_u_u(p_81, (safe_div_func_int16_t_s_s(((0x0557L >= g_43[2][2][8]) , (*p_83)), (safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((l_1796[5][0][0] = ((safe_div_func_uint16_t_u_u((0x242A3F505EFECB9ELL > (safe_lshift_func_int16_t_s_s(((safe_add_func_uint32_t_u_u((l_1932 != l_1935), 1L)) > l_1804[3][3]), (*p_83)))), 0xBF8EL)) != (**g_1409))) , 65535UL), l_1938)), g_973)))))))) > 4L)))
                    { /* block id: 769 */
                        union U2 **l_1942 = &l_1816[3][0];
                        int32_t l_1951 = 0xD21CEBE2L;
                        const uint64_t **l_1953 = &l_1952;
                        const uint64_t **l_1954 = (void*)0;
                        const uint64_t *l_1956 = (void*)0;
                        const uint64_t **l_1955 = &l_1956;
                        uint64_t **l_1958[4][1];
                        int i, j;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1958[i][j] = &l_1957;
                        }
                        if (p_79.f0)
                            break;
                        l_1911[4] = (((((*p_83) = (!p_79.f1)) , ((l_1940 == l_1942) , ((safe_sub_func_int64_t_s_s(4L, (safe_div_func_int64_t_s_s(((0xD0E91DF9B5100A3DLL & (((((safe_sub_func_float_f_f((safe_sub_func_float_f_f(l_1951, ((((((((*l_1955) = ((*l_1953) = l_1952)) == (l_1959 = (l_1957 = l_1957))) >= g_1027) , (void*)0) == &l_1888[0][0]) <= g_1496) != (-0x9.7p+1)))), l_1951)) >= g_143) , (**g_1297)) , (void*)0) != g_1960)) ^ 0UL), 0x0BC3A90667DFAA9ELL)))) == 0x7F59L))) || p_81) , 0x5.6D1CECp-54);
                    }
                    else
                    { /* block id: 777 */
                        g_193[0] = (safe_add_func_float_f_f(p_79.f0, g_1915.f0));
                    }
                }
                p_79.f1 |= ((*l_1877) < ((((&g_1094[3] != ((((safe_mul_func_uint8_t_u_u(((((*l_1799) <= ((void*)0 == &p_79)) || (safe_sub_func_int32_t_s_s((g_1969 , ((safe_mod_func_int8_t_s_s(g_43[2][2][8], ((*l_1800) && 9L))) && p_81)), (*g_1098)))) == g_1026), (*l_1877))) | p_79.f0) && p_80) , &g_1094[3])) >= 0xA6L) != l_1804[5][3]) , 1UL));
                l_1796[5][0][0] = p_80;
                l_1990 &= (safe_lshift_func_int16_t_s_u(((g_1974 , l_1975[1][3][0]) == (void*)0), ((((*l_1877) = (safe_add_func_int32_t_s_s((l_1978 != l_1980), (((((*l_1877) >= (((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u(((l_1987 , &g_1267) == (((0xB3L | 0xDDL) <= l_1796[5][0][0]) , l_1988)), 0x34L)) != g_1771), g_1771)) == (*l_1800)) & g_145[3])) < p_79.f1) ^ p_81) || 0x5836DB70L)))) > g_276) , g_8[1])));
            }
            for (g_54 = 1; (g_54 >= 0); g_54 -= 1)
            { /* block id: 788 */
                l_1801 = l_1991[0][0];
                l_1801 = p_82;
                for (l_1820 = 8; (l_1820 >= 0); l_1820 -= 1)
                { /* block id: 793 */
                    int i, j, k;
                    return l_1796[(g_54 + 5)][(g_54 + 2)][g_54];
                }
            }
            if (((p_79.f1 & (1UL == (safe_sub_func_uint64_t_u_u((g_1299[1].f0 && l_1804[3][3]), (p_80 | (((safe_div_func_float_f_f((safe_sub_func_float_f_f(0x9.BC2EE7p-66, (safe_sub_func_float_f_f(((((((void*)0 == l_2000) < ((g_976 , p_81) == p_80)) <= p_79.f1) >= 0xC.B2FD9Ap+22) < (-0x6.6p-1)), g_145[2])))), (*l_1799))) >= (-0x1.Bp-1)) , p_79.f1)))))) <= 18446744073709551612UL))
            { /* block id: 797 */
                int32_t *l_2002 = &l_1888[4][0];
                float *l_2011 = &g_193[0];
                l_2002 = &l_1796[5][0][0];
                (*l_2002) = (l_2003 < (safe_add_func_float_f_f((-((*g_1396) , (((*g_57) == (l_2007 = &g_239)) == p_79.f0))), ((*l_2002) < (safe_add_func_float_f_f(((+(g_193[0] , ((*l_1800) < (((*l_2011) = (-0x1.9p-1)) >= 0x1.83B6CAp+36)))) == g_276), 0x1.45CF19p+68))))));
            }
            else
            { /* block id: 802 */
                int32_t ****l_2030[1][10][4] = {{{&l_2014,(void*)0,&g_582,&l_2014},{&g_582,&g_582,&g_582,&g_582},{(void*)0,&g_582,&l_2014,&l_2014},{&g_582,(void*)0,(void*)0,&g_582},{&g_582,&l_2014,(void*)0,&g_582},{&g_582,(void*)0,&l_2014,(void*)0},{(void*)0,(void*)0,&g_582,(void*)0},{&g_582,(void*)0,&g_582,&g_582},{&l_2014,&l_2014,&l_2014,&g_582},{&l_2014,(void*)0,&g_582,&g_582}}};
                int32_t ****l_2032 = &g_582;
                int32_t *****l_2031 = &l_2032;
                int64_t *l_2033 = &g_1292;
                uint16_t l_2052[10];
                float *l_2066 = &g_193[0];
                float **l_2065 = &l_2066;
                int32_t l_2085 = 0L;
                int i, j, k;
                for (i = 0; i < 10; i++)
                    l_2052[i] = 0UL;
                if ((safe_add_func_int64_t_s_s(g_168, (l_2014 != (((safe_mod_func_int8_t_s_s(((safe_unary_minus_func_uint8_t_u((((safe_lshift_func_uint16_t_u_s(((*p_83) != ((((*l_2033) = (safe_rshift_func_uint16_t_u_s(0UL, ((safe_rshift_func_int8_t_s_u(((*p_83) , p_79.f1), 4)) < (safe_lshift_func_uint16_t_u_s((safe_div_func_int8_t_s_s((((((safe_lshift_func_int16_t_s_s((((*l_2031) = (p_79 , (l_2030[0][3][3] = &g_582))) == &g_582), g_64)) > (*l_2007)) ^ g_270) , 4294967290UL) == 0xC2E3E34DL), 0x9FL)), (*p_83))))))) , (*l_2007)) & p_79.f1)), 12)) ^ 0UL) | g_1823[2].f0))) , (-10L)), 0xB3L)) ^ g_973) , l_2014)))))
                { /* block id: 806 */
                    uint32_t l_2044 = 0xDE97350BL;
                    int32_t l_2055 = (-1L);
                    for (g_221 = 0; (g_221 <= 4); g_221 += 1)
                    { /* block id: 809 */
                        uint32_t l_2034[5][7][5] = {{{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL}},{{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L}},{{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL}},{{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L}},{{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL},{0xB96F4579L,0x9C994987L,0x298EBEF6L,0x9C994987L,0xB96F4579L},{0x9A98A9CAL,4294967295UL,0UL,4294967295UL,0x9A98A9CAL}}};
                        uint32_t *l_2045 = &l_2034[2][1][0];
                        float *l_2050 = (void*)0;
                        float *l_2051 = &g_193[0];
                        float *l_2053 = (void*)0;
                        int i, j, k;
                        l_2034[2][1][0]++;
                        l_2055 = (0x6.815971p+12 == (0x0.2F90D1p+19 != (((void*)0 != l_2037) > (safe_sub_func_float_f_f(((((*l_2051) = (((safe_mod_func_uint16_t_u_u((safe_div_func_int8_t_s_s((0x35803872L ^ ((*l_2045) = l_2044)), (*g_1410))), (*p_83))) && ((safe_add_func_uint32_t_u_u(((*l_2045) = (safe_mod_func_uint8_t_u_u((9UL == l_2044), (***g_376)))), l_2044)) , 0x3E397F0D57CFE2C7LL)) , 0x5.EF9477p-14)) > p_81) >= l_2052[5]), (-0x8.Ap+1))))));
                        p_79.f1 &= p_81;
                    }
                    g_2056 = g_2056;
                }
                else
                { /* block id: 818 */
                    uint8_t l_2083 = 9UL;
                    float **l_2084[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_2084[i] = (void*)0;
                    for (g_1550 = 0; (g_1550 <= 1); g_1550 += 1)
                    { /* block id: 821 */
                        union U2 *l_2061 = &l_1817;
                        int8_t *l_2071 = &g_1026;
                        struct S0 ****l_2076 = &l_1935;
                        const struct S0 ****l_2077 = (void*)0;
                        const struct S0 *l_2081 = &g_2082;
                        const struct S0 **l_2080 = &l_2081;
                        const struct S0 ***l_2079[3][4][8] = {{{&l_2080,&l_2080,&l_2080,&l_2080,(void*)0,&l_2080,&l_2080,&l_2080},{&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080},{&l_2080,&l_2080,(void*)0,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080},{&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080}},{{&l_2080,&l_2080,&l_2080,&l_2080,(void*)0,&l_2080,(void*)0,&l_2080},{&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080},{&l_2080,(void*)0,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080},{&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,(void*)0,&l_2080,&l_2080}},{{&l_2080,&l_2080,&l_2080,&l_2080,(void*)0,&l_2080,&l_2080,(void*)0},{&l_2080,&l_2080,&l_2080,(void*)0,&l_2080,&l_2080,&l_2080,&l_2080},{&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080},{&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080,&l_2080}}};
                        const struct S0 ****l_2078 = &l_2079[2][1][7];
                        int i, j, k;
                        l_2061 = l_2060;
                        if (l_1804[g_1550][(g_1550 + 1)])
                            continue;
                        l_2085 ^= (safe_mod_func_uint8_t_u_u((safe_unary_minus_func_uint32_t_u(((0x9B6C3CE7L && (l_2065 != ((safe_div_func_uint64_t_u_u(((safe_mul_func_int8_t_s_s(((*l_2071) = ((void*)0 != (*g_1622))), ((g_276 ^ (p_79.f0 , ((((*l_1959) = (safe_mod_func_uint8_t_u_u(p_79.f0, ((((*l_2076) = g_2074[3]) != ((*l_2078) = (void*)0)) , 0x09L)))) | l_2083) >= p_81))) >= 0xF0C784CB1CCD8C0FLL))) >= p_79.f1), p_80)) , l_2084[0]))) < (-3L)))), p_81));
                        (*g_2059) = (*g_2059);
                    }
                }
                l_1906 = p_79;
            }
        }
        (*l_1799) ^= (safe_div_func_uint32_t_u_u((safe_div_func_int64_t_s_s(((((*p_83) = (*p_83)) && (((*l_2091) = ((*l_1941) = g_2090)) , ((void*)0 == (*g_1623)))) < (((*l_2092) = p_81) > (safe_lshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(((*l_2102) = (0xCB27L == ((*l_2101) = (safe_mod_func_uint16_t_u_u(p_81, (safe_add_func_uint8_t_u_u((1UL & (p_79.f0 | 1UL)), (***g_376)))))))), p_81)), 1)))), p_80)), g_1292));
        for (g_1107 = 0; (g_1107 <= 53); g_1107 = safe_add_func_uint32_t_u_u(g_1107, 6))
        { /* block id: 844 */
            int32_t l_2108 = 1L;
            int32_t l_2109 = 0x3E76FF3AL;
            int32_t l_2110 = 8L;
            uint64_t *l_2134 = &l_1804[1][1];
            float l_2141[9][6][1] = {{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}},{{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5},{0x3.841949p-57},{0x3.841949p-57},{0x9.19A7D9p-5}}};
            int8_t l_2142[2];
            float l_2185 = 0xF.9EC754p+86;
            int32_t l_2189[5][9][1] = {{{7L},{0L},{3L},{0L},{7L},{0x79821241L},{0xED193826L},{1L},{3L}},{{4L},{1L},{0x79821241L},{1L},{4L},{3L},{1L},{0xED193826L},{0x79821241L}},{{7L},{0L},{3L},{0L},{7L},{0x79821241L},{0xED193826L},{1L},{3L}},{{4L},{1L},{0x79821241L},{1L},{4L},{3L},{1L},{0xED193826L},{0x79821241L}},{{7L},{0L},{3L},{0L},{7L},{0x79821241L},{0xED193826L},{1L},{3L}}};
            const int32_t l_2241 = 0xF1EF1A95L;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_2142[i] = 7L;
            for (g_143 = 0; (g_143 != (-18)); --g_143)
            { /* block id: 847 */
                float l_2107 = 0x1.Fp+1;
                l_2111--;
                (*l_2007) |= (-1L);
            }
            for (l_1938 = 0; (l_1938 >= 3); l_1938 = safe_add_func_int64_t_s_s(l_1938, 9))
            { /* block id: 853 */
                uint64_t *****l_2132[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int16_t l_2160 = 0L;
                int32_t l_2177[1];
                float l_2190 = 0x8.216423p+3;
                int16_t l_2199[8][9] = {{(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L)},{0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL},{(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L)},{0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL},{(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L)},{0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL},{(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L),0xB6BCL,(-1L)},{0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL,0x226CL}};
                int64_t * const ***l_2220 = &g_1090;
                int i, j;
                for (i = 0; i < 1; i++)
                    l_2177[i] = 0L;
                for (l_1817.f0 = 0; (l_1817.f0 == (-1)); l_1817.f0--)
                { /* block id: 856 */
                    union U2 l_2118 = {0L};
                    uint64_t ***l_2119 = (void*)0;
                    uint32_t l_2139 = 4UL;
                    int32_t l_2181 = (-8L);
                    int32_t l_2182 = 0xA3198CDFL;
                    int32_t l_2183[8] = {0x20513BEDL,(-1L),0x20513BEDL,0x20513BEDL,(-1L),0x20513BEDL,0x20513BEDL,(-1L)};
                    int i;
                    p_79.f1 &= (l_2118 , ((*l_2007) &= ((void*)0 == l_2119)));
                    if (((-1L) <= ((safe_rshift_func_uint8_t_u_s(((****g_404) |= (((!(((~(safe_mul_func_int8_t_s_s(((safe_lshift_func_uint8_t_u_u(((((safe_mod_func_int64_t_s_s((-1L), (((safe_rshift_func_int8_t_s_s(((g_54 ^= ((void*)0 == l_2132[0])) , (((l_2133[5] != (g_1605 , l_2134)) < p_80) , ((safe_mod_func_int64_t_s_s((safe_mul_func_uint8_t_u_u(((p_79.f0 > p_79.f1) | 1UL), 255UL)), l_2139)) , 0x33L))), l_2139)) ^ p_79.f0) && 0x16L))) > g_241.f1) < 0xBEL) ^ 0x900567AB11B221FCLL), 1)) ^ l_2109), p_80))) != g_168) > l_2108)) || l_2118.f0) | l_2108)), g_838.f0)) | (*l_2007))))
                    { /* block id: 861 */
                        uint32_t l_2140 = 0xC0C85787L;
                        return l_2140;
                    }
                    else
                    { /* block id: 863 */
                        float *l_2143 = (void*)0;
                        float *l_2144[4] = {&g_193[0],&g_193[0],&g_193[0],&g_193[0]};
                        int32_t l_2145 = 0x078F9F60L;
                        int i;
                        l_2145 = (g_193[0] = (l_2142[1] , (-0x1.Cp+1)));
                        if (p_79.f0)
                            continue;
                        p_79 = (*g_1298);
                    }
                    for (l_2111 = 0; (l_2111 < 51); l_2111 = safe_add_func_int64_t_s_s(l_2111, 7))
                    { /* block id: 871 */
                        int32_t l_2178[7];
                        int32_t l_2180 = 0x39FD84ADL;
                        int32_t l_2184 = 0x06D25F80L;
                        int32_t l_2186 = 0xB5CFDC7EL;
                        int32_t l_2187 = 0x966EA761L;
                        int32_t l_2188 = (-1L);
                        int32_t l_2191 = 7L;
                        int32_t l_2192 = 7L;
                        int32_t l_2193 = 0x966813A5L;
                        int32_t l_2194 = 1L;
                        int32_t l_2195[6];
                        int i;
                        for (i = 0; i < 7; i++)
                            l_2178[i] = 0L;
                        for (i = 0; i < 6; i++)
                            l_2195[i] = (-8L);
                        p_79.f1 |= (((safe_div_func_uint64_t_u_u((safe_mod_func_int32_t_s_s((l_2109 = ((l_2118.f0 != (safe_sub_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((safe_div_func_int16_t_s_s(l_2142[1], (safe_lshift_func_int8_t_s_u((p_80 | (l_2160 = 0xEBEC86CE7F5DC27BLL)), (safe_lshift_func_int8_t_s_s(((*l_2007) ^= (safe_rshift_func_int8_t_s_u(g_1605, 4))), 5)))))), (l_2178[0] = (safe_mod_func_uint64_t_u_u(0UL, (safe_div_func_int8_t_s_s((safe_mod_func_int64_t_s_s((safe_add_func_int16_t_s_s(((l_2118.f0 == (--(***g_405))) < (safe_add_func_uint8_t_u_u((((&g_1961 == (void*)0) || l_2177[0]) || 0x94608630L), l_2139))), g_820[5][1][0].f0)), p_81)), 0x0AL))))))), l_2179))) != l_2118.f0)), (*g_1098))), l_2177[0])) == g_445.f0) == p_81);
                        l_2196++;
                        (*g_2059) = p_82;
                        (**g_2058) = (void*)0;
                    }
                }
                if (p_79.f1)
                    break;
                ++l_2200[7][0];
                for (g_1550 = (-27); (g_1550 > (-15)); g_1550 = safe_add_func_int64_t_s_s(g_1550, 3))
                { /* block id: 887 */
                    int64_t l_2205 = 0xFF937B1BDA2DD443LL;
                    int32_t l_2206[9] = {0x569FAA33L,0x6B7DE7E8L,0x569FAA33L,0x569FAA33L,0x6B7DE7E8L,0x569FAA33L,0x569FAA33L,0x6B7DE7E8L,0x569FAA33L};
                    uint64_t ***** const l_2222 = &l_1989;
                    int64_t *l_2223 = (void*)0;
                    int64_t *l_2224 = &g_232;
                    int i;
                    l_2207--;
                    (*g_2059) = &l_2206[7];
                    p_79.f1 ^= (safe_mul_func_int8_t_s_s(((**l_1940) , (((*l_2224) |= ((((*l_2134) = p_80) & ((((*l_2092) = ((void*)0 == (*g_404))) , 1L) , (((safe_rshift_func_int16_t_s_u(((*p_83) = l_2142[1]), (safe_sub_func_uint8_t_u_u((l_2132[0] == ((safe_sub_func_float_f_f((safe_div_func_float_f_f((l_2220 == ((l_2206[8] = l_2206[5]) , l_2221)), p_81)), g_11)) , l_2222)), 7UL)))) && (-1L)) != 65535UL))) | g_168)) & 0x4E22DD2E1BD6F38CLL)), g_820[5][1][0].f0));
                }
            }
            if (p_79.f1)
                break;
            for (g_1026 = 0; (g_1026 >= 0); g_1026 -= 1)
            { /* block id: 901 */
                union U1 *l_2225 = &g_838;
                int32_t l_2240 = 1L;
                (**g_2058) = p_82;
                for (l_1815.f0 = 0; (l_1815.f0 >= 0); l_1815.f0 -= 1)
                { /* block id: 905 */
                    int32_t l_2230 = (-5L);
                    l_2225 = l_2225;
                    for (g_232 = 4; (g_232 >= 0); g_232 -= 1)
                    { /* block id: 909 */
                        uint32_t *l_2237 = (void*)0;
                        uint32_t *l_2238 = (void*)0;
                        uint32_t *l_2239 = &l_2179;
                        int i, j, k;
                        (**g_2058) = &l_2189[g_232][(g_232 + 2)][l_1815.f0];
                        p_79.f1 ^= (safe_div_func_int16_t_s_s(l_2189[(g_1026 + 2)][(l_1815.f0 + 8)][g_1026], ((func_38(l_2230) , (safe_lshift_func_uint16_t_u_s((p_79.f0 <= p_79.f0), 13))) , (((safe_div_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u((((l_2240 = ((*l_2239) = (((*l_1940) = (*l_1940)) != (void*)0))) <= (func_38(p_79.f0) , (**g_57))) , 0xBCL), p_80)), 8UL)) != l_2241) , l_2108))));
                    }
                }
            }
        }
    }
    for (g_54 = 0; (g_54 <= (-25)); g_54 = safe_sub_func_uint64_t_u_u(g_54, 1))
    { /* block id: 922 */
        int32_t l_2246 = 0xB2277CB2L;
        for (g_221 = 0; (g_221 != 6); ++g_221)
        { /* block id: 925 */
            if (l_2246)
                break;
            return l_2246;
        }
        if (p_81)
            break;
        for (g_1859 = 0; (g_1859 < 52); g_1859++)
        { /* block id: 932 */
            return p_80;
        }
    }
    p_79.f0 |= ((((safe_mul_func_uint16_t_u_u(65535UL, (g_54 = ((safe_rshift_func_int16_t_s_s((safe_unary_minus_func_int16_t_s((safe_sub_func_uint8_t_u_u(p_81, ((*l_2007) = ((!((l_2257 != (g_2261 = &l_2258)) & (safe_lshift_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((*l_2268)++), ((((**g_377) = 0UL) > (safe_rshift_func_uint16_t_u_u(65535UL, ((*l_2275) = ((safe_mul_func_int16_t_s_s((*l_2007), 0x07D3L)) == ((((*l_2007) < (-2L)) || p_79.f1) > (*g_2262))))))) > 0xB8653E95L))), (*p_83))))) == g_1795)))))), g_276)) , g_221)))) > g_46.f0) , (*l_2007)) < p_79.f1);
    return l_2276;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_95(int8_t * p_96, uint64_t  p_97, int32_t  p_98, uint64_t  p_99, int16_t  p_100)
{ /* block id: 701 */
    int32_t l_1767 = 0x7269952FL;
    return l_1767;
}


/* ------------------------------------------ */
/* 
 * reads : g_143 g_1027 g_3 g_145
 * writes: g_143 g_1027 g_1292 g_976
 */
static uint16_t  func_102(int32_t * p_103, float  p_104, const int64_t  p_105, int8_t  p_106)
{ /* block id: 583 */
    uint32_t l_1460 = 0xE874F5DBL;
    struct S0 l_1464 = {0,-31};
    int32_t l_1486 = 0x84C2B847L;
    int32_t l_1487 = (-1L);
    int32_t l_1489 = 0x635C43F8L;
    int32_t l_1490[4] = {0x6E77FF6AL,0x6E77FF6AL,0x6E77FF6AL,0x6E77FF6AL};
    float l_1566 = 0x0.Dp-1;
    int32_t l_1597 = 1L;
    union U2 ****l_1612 = (void*)0;
    union U1 * const l_1683 = (void*)0;
    int32_t l_1721 = 0xD9F01A30L;
    float l_1730 = (-0x9.Cp-1);
    int64_t l_1744[1][4][2] = {{{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)}}};
    int64_t *l_1749 = &g_1292;
    uint16_t *l_1760[6] = {&g_221,&g_221,&g_221,&g_221,&g_221,&g_221};
    uint64_t *l_1761 = &g_976;
    int32_t *l_1762[8] = {&l_1489,&l_1489,&l_1489,&l_1489,&l_1489,&l_1489,&l_1489,&l_1489};
    uint64_t l_1763 = 1UL;
    union U2 *l_1764 = &g_508;
    union U2 **l_1765 = &l_1764;
    int i, j, k;
    for (g_143 = 0; (g_143 > 10); g_143 = safe_add_func_uint8_t_u_u(g_143, 3))
    { /* block id: 586 */
        return l_1460;
    }
    for (g_1027 = 0; (g_1027 == 11); g_1027 = safe_add_func_int32_t_s_s(g_1027, 1))
    { /* block id: 591 */
        struct S0 l_1463 = {0,-275};
        int32_t l_1485 = 0xB42E9F7AL;
        int32_t l_1488 = 0x220081A7L;
        int32_t l_1491 = 0L;
        int32_t l_1493[10] = {0x2D4979D4L,0x84E48C74L,0x84E48C74L,0x2D4979D4L,0x84E48C74L,0x84E48C74L,0x2D4979D4L,0x84E48C74L,0x84E48C74L,0x2D4979D4L};
        int8_t l_1567 = 1L;
        const union U2 *l_1575[5] = {&g_314[7],&g_314[7],&g_314[7],&g_314[7],&g_314[7]};
        const union U2 **l_1574[1][6][9] = {{{&l_1575[4],&l_1575[4],(void*)0,&l_1575[0],&l_1575[2],&l_1575[0],(void*)0,&l_1575[1],(void*)0},{&l_1575[1],&l_1575[2],&l_1575[4],&l_1575[4],&l_1575[2],&l_1575[1],&l_1575[4],&l_1575[0],&l_1575[1]},{&l_1575[1],(void*)0,(void*)0,&l_1575[3],&l_1575[3],&l_1575[3],&l_1575[3],(void*)0,(void*)0},{&l_1575[2],(void*)0,&l_1575[1],&l_1575[0],&l_1575[3],&l_1575[0],&l_1575[4],&l_1575[4],&l_1575[0]},{(void*)0,(void*)0,&l_1575[0],(void*)0,(void*)0,&l_1575[0],(void*)0,&l_1575[4],&l_1575[3]},{&l_1575[4],(void*)0,&l_1575[0],&l_1575[0],&l_1575[0],&l_1575[0],&l_1575[0],(void*)0,&l_1575[4]}}};
        const union U2 ***l_1573 = &l_1574[0][4][4];
        const union U2 ****l_1572[8][2][10] = {{{&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573},{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573}},{{&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0},{&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,(void*)0,&l_1573}},{{&l_1573,(void*)0,&l_1573,(void*)0,&l_1573,(void*)0,&l_1573,(void*)0,&l_1573,&l_1573},{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573}},{{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0},{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,(void*)0,&l_1573,(void*)0,&l_1573}},{{&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573},{&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573}},{{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573},{&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0}},{{&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573},{&l_1573,(void*)0,(void*)0,(void*)0,&l_1573,(void*)0,(void*)0,(void*)0,&l_1573,&l_1573}},{{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573},{&l_1573,&l_1573,&l_1573,(void*)0,&l_1573,&l_1573,&l_1573,&l_1573,&l_1573,(void*)0}}};
        int32_t l_1583 = 0xF0418904L;
        int64_t *l_1602 = &g_1094[3];
        int64_t **l_1601 = &l_1602;
        int64_t ***l_1600 = &l_1601;
        uint8_t l_1631 = 0xA5L;
        uint8_t *****l_1640 = (void*)0;
        uint64_t *l_1661 = &g_238;
        uint64_t **l_1660 = &l_1661;
        uint64_t ***l_1659 = &l_1660;
        uint64_t ****l_1658 = &l_1659;
        uint64_t *****l_1657 = &l_1658;
        uint16_t l_1679 = 2UL;
        int32_t *l_1705 = &l_1485;
        int32_t *l_1706 = &l_1493[9];
        int i, j, k;
        l_1464 = l_1463;
    }
    l_1763 &= (((*l_1761) = ((((*l_1749) = (~(p_106 != l_1486))) & (safe_sub_func_uint8_t_u_u((safe_div_func_uint8_t_u_u(p_106, (safe_sub_func_int8_t_s_s((safe_sub_func_int64_t_s_s(((safe_unary_minus_func_uint32_t_u(0xBA63D62AL)) <= (l_1490[3] = (*p_103))), (l_1597 <= ((safe_unary_minus_func_int32_t_s(((void*)0 != l_1760[0]))) != g_145[1])))), l_1460)))), p_106))) != 248UL)) ^ l_1464.f0);
    (*l_1765) = l_1764;
    return p_105;
}


/* ------------------------------------------ */
/* 
 * reads : g_54
 * writes:
 */
static float  func_108(uint32_t * p_109, uint32_t  p_110, int32_t * p_111, union U2  p_112)
{ /* block id: 186 */
    int64_t l_525[9][3] = {{0x4629F6EBFAE2AE1BLL,(-1L),(-1L)},{0x4629F6EBFAE2AE1BLL,0xDA6D89BC37C76D20LL,0x4629F6EBFAE2AE1BLL},{0x4629F6EBFAE2AE1BLL,1L,1L},{0x4629F6EBFAE2AE1BLL,(-1L),(-1L)},{0x4629F6EBFAE2AE1BLL,0xDA6D89BC37C76D20LL,0x4629F6EBFAE2AE1BLL},{0x4629F6EBFAE2AE1BLL,1L,1L},{0x4629F6EBFAE2AE1BLL,(-1L),(-1L)},{0x4629F6EBFAE2AE1BLL,0xDA6D89BC37C76D20LL,0x4629F6EBFAE2AE1BLL},{0x4629F6EBFAE2AE1BLL,1L,1L}};
    uint8_t *l_529[7][2][9] = {{{(void*)0,(void*)0,&g_145[2],&g_145[2],&g_145[3],(void*)0,&g_145[1],&g_145[2],&g_145[2]},{&g_145[0],&g_145[2],&g_145[0],&g_145[4],(void*)0,&g_145[0],(void*)0,&g_145[0],(void*)0}},{{&g_145[2],&g_145[2],&g_145[2],&g_145[2],(void*)0,&g_145[2],&g_145[2],&g_145[4],&g_145[2]},{&g_145[0],&g_145[4],&g_145[0],&g_145[4],&g_145[0],&g_145[2],(void*)0,&g_145[2],(void*)0}},{{&g_145[4],(void*)0,(void*)0,(void*)0,(void*)0,&g_145[4],&g_145[1],(void*)0,&g_145[1]},{&g_145[2],&g_145[2],(void*)0,&g_145[2],(void*)0,&g_145[2],&g_145[2],&g_145[4],&g_145[2]}},{{&g_145[2],&g_145[1],&g_145[2],&g_145[4],&g_145[4],&g_145[2],&g_145[1],&g_145[2],&g_145[4]},{&g_145[2],&g_145[4],(void*)0,&g_145[0],&g_145[1],&g_145[0],(void*)0,&g_145[4],&g_145[2]}},{{&g_145[3],&g_145[4],(void*)0,&g_145[2],&g_145[3],&g_145[3],&g_145[2],(void*)0,&g_145[4]},{(void*)0,&g_145[2],&g_145[2],&g_145[2],(void*)0,&g_145[4],(void*)0,&g_145[2],&g_145[2]}},{{&g_145[3],&g_145[2],&g_145[2],&g_145[1],&g_145[4],&g_145[2],&g_145[2],&g_145[4],&g_145[1]},{&g_145[2],&g_145[2],&g_145[2],&g_145[4],(void*)0,&g_145[0],&g_145[1],&g_145[0],(void*)0}},{{&g_145[2],&g_145[4],&g_145[2],&g_145[2],(void*)0,&g_145[2],&g_145[2],&g_145[2],&g_145[2]},{&g_145[2],&g_145[4],&g_145[2],&g_145[4],&g_145[2],&g_145[2],(void*)0,&g_145[2],(void*)0}}};
    uint16_t *l_534[1][8][8] = {{{(void*)0,&g_221,(void*)0,(void*)0,&g_270,&g_270,(void*)0,(void*)0},{&g_221,&g_221,&g_270,(void*)0,&g_221,(void*)0,&g_270,&g_221},{&g_221,(void*)0,(void*)0,&g_270,&g_270,(void*)0,(void*)0,&g_221},{(void*)0,&g_270,&g_221,(void*)0,&g_221,&g_270,(void*)0,(void*)0},{&g_270,(void*)0,(void*)0,(void*)0,(void*)0,&g_270,&g_270,&g_270},{(void*)0,&g_270,&g_270,&g_270,(void*)0,(void*)0,(void*)0,(void*)0},{&g_270,(void*)0,(void*)0,&g_270,&g_221,(void*)0,&g_221,&g_270},{(void*)0,&g_221,(void*)0,(void*)0,&g_270,&g_270,(void*)0,(void*)0}}};
    int32_t l_535 = (-5L);
    int8_t *l_536[4][3][3] = {{{&g_91,&g_91,(void*)0},{&g_8[1],&g_8[1],&g_8[2]},{&g_91,&g_91,(void*)0}},{{&g_7[0][1][1],&g_7[0][1][1],&g_8[1]},{(void*)0,(void*)0,&g_91},{&g_7[0][1][1],&g_7[0][1][1],&g_8[1]}},{{(void*)0,(void*)0,&g_91},{&g_7[0][1][1],&g_7[0][1][1],&g_8[1]},{(void*)0,(void*)0,&g_91}},{{&g_7[0][1][1],&g_7[0][1][1],&g_8[1]},{(void*)0,(void*)0,&g_91},{&g_7[0][1][1],&g_7[0][1][1],&g_8[1]}}};
    int32_t l_537 = 0x72E0E86EL;
    uint32_t l_538 = 0x0D8E23CCL;
    float l_539[6][9] = {{0x2.A0AFBBp-11,0x2.07B60Fp+18,0xD.EB6DD3p+81,0x2.07B60Fp+18,0x2.A0AFBBp-11,0xD.EB6DD3p+81,0x1.Cp+1,0x1.Cp+1,0xD.EB6DD3p+81},{0x2.A0AFBBp-11,0x2.07B60Fp+18,0xD.EB6DD3p+81,0x2.07B60Fp+18,0x2.A0AFBBp-11,0xD.EB6DD3p+81,0x1.Cp+1,0x1.Cp+1,0xD.EB6DD3p+81},{0x2.A0AFBBp-11,0x2.07B60Fp+18,0xD.EB6DD3p+81,0x2.07B60Fp+18,0x2.A0AFBBp-11,0xD.EB6DD3p+81,0x1.Cp+1,0x1.Cp+1,0xD.EB6DD3p+81},{0x2.A0AFBBp-11,0x2.07B60Fp+18,0xD.EB6DD3p+81,0x2.07B60Fp+18,0x2.A0AFBBp-11,0xD.EB6DD3p+81,0x1.Cp+1,0x1.Cp+1,0xD.EB6DD3p+81},{0x2.A0AFBBp-11,0x2.07B60Fp+18,0xD.EB6DD3p+81,0x2.07B60Fp+18,0x2.A0AFBBp-11,0xD.EB6DD3p+81,0x1.Cp+1,0x1.Cp+1,0xD.EB6DD3p+81},{0x2.A0AFBBp-11,0x2.07B60Fp+18,0xD.EB6DD3p+81,0x2.07B60Fp+18,0x2.A0AFBBp-11,0xD.EB6DD3p+81,0x1.Cp+1,0x1.Cp+1,0xD.EB6DD3p+81}};
    int32_t l_606 = 7L;
    int16_t l_639[6] = {(-1L),(-1L),0L,(-1L),(-1L),0L};
    uint64_t *l_684 = (void*)0;
    uint64_t **l_683 = &l_684;
    int64_t l_786 = 7L;
    int32_t l_794[6] = {(-7L),(-7L),(-7L),(-7L),(-7L),(-7L)};
    union U1 * const l_817 = &g_818;
    uint32_t *l_877 = &g_276;
    int32_t l_1023 = 4L;
    const uint32_t l_1038 = 18446744073709551614UL;
    int64_t **l_1076 = (void*)0;
    uint16_t l_1079 = 0x80D0L;
    struct S0 l_1104 = {0,-96};
    struct S0 *l_1123 = &g_241;
    int32_t l_1138 = (-1L);
    union U2 *l_1155 = &g_445;
    union U2 **l_1154 = &l_1155;
    const uint8_t l_1229 = 2UL;
    uint8_t l_1341 = 0x0EL;
    uint8_t ***l_1363 = (void*)0;
    uint8_t *** const l_1364[5] = {&g_377,&g_377,&g_377,&g_377,&g_377};
    int8_t l_1369 = (-6L);
    uint32_t l_1391[3][8] = {{0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL}};
    float l_1439 = (-0x10.7p+1);
    float l_1440 = 0x1.1p-1;
    uint8_t l_1441 = 0x5CL;
    float l_1444 = 0x1.Ap+1;
    uint8_t l_1445[5][4][1] = {{{255UL},{7UL},{255UL},{7UL}},{{255UL},{7UL},{255UL},{7UL}},{{255UL},{7UL},{255UL},{7UL}},{{255UL},{7UL},{255UL},{7UL}},{{255UL},{7UL},{255UL},{7UL}}};
    int32_t *l_1448 = &l_794[4];
    int32_t *l_1449 = &l_794[4];
    int32_t *l_1450 = &g_314[7].f0;
    int32_t *l_1451[1];
    int32_t l_1452 = 0L;
    int16_t l_1453 = (-1L);
    int32_t l_1454[3][7][2] = {{{0x4C997633L,(-6L)},{7L,0L},{0x7D7F5EE1L,7L},{0L,(-6L)},{0L,7L},{0x7D7F5EE1L,0L},{7L,(-6L)}},{{0x4C997633L,0x4C997633L},{0x7D7F5EE1L,0x4C997633L},{0x4C997633L,(-6L)},{7L,0L},{0x7D7F5EE1L,7L},{0L,(-6L)},{0L,7L}},{{0x7D7F5EE1L,0L},{7L,(-6L)},{0x4C997633L,0x4C997633L},{0x7D7F5EE1L,0x4C997633L},{0x4C997633L,(-6L)},{7L,0L},{0x7D7F5EE1L,7L}}};
    uint32_t l_1455 = 1UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1451[i] = &g_239;
    return g_54;
}


/* ------------------------------------------ */
/* 
 * reads : g_54 g_125 g_8 g_64 g_3 g_58 g_145 g_239 g_91 g_168 g_198 g_221 g_11 g_238 g_241 g_143 g_232 g_270 g_276 g_314 g_316 g_375 g_404 g_405 g_377 g_378 g_376 g_7 g_508.f0 g_1605
 * writes: g_54 g_125 g_58 g_91 g_143 g_145 g_168 g_221 g_232 g_238 g_239 g_241 g_193 g_276 g_316 g_375 g_403 g_314 g_445
 */
static uint32_t * func_113(int8_t  p_114, struct S0  p_115, int32_t * p_116)
{ /* block id: 26 */
    const uint32_t l_131 = 4UL;
    int32_t l_169 = (-3L);
    int32_t l_177 = (-8L);
    int32_t l_178[9][5] = {{(-6L),1L,0L,0x37870156L,0L},{0x5343E278L,0x5343E278L,0x5343E278L,0L,0L},{0x97456AEAL,1L,0x97456AEAL,0xAA54225DL,0L},{0L,1L,1L,0L,1L},{0L,1L,(-6L),1L,0L},{1L,0L,1L,1L,0L},{0L,0xAA54225DL,0x97456AEAL,1L,0x97456AEAL},{0L,0L,0x5343E278L,0L,0L},{0x97456AEAL,1L,0x97456AEAL,0xAA54225DL,0L}};
    uint32_t l_185 = 1UL;
    uint8_t *l_226 = &g_145[2];
    const struct S0 l_350 = {0,1008};
    int32_t l_361 = 0x4BC8B719L;
    uint32_t l_382 = 0xB47C0D00L;
    int16_t l_389 = 0x39BEL;
    float *l_422 = &g_193[0];
    struct S0 *l_425[6][10] = {{(void*)0,&g_241,&g_241,&g_241,&g_241,(void*)0,&g_241,&g_241,&g_241,&g_241},{&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241},{&g_241,&g_241,&g_241,&g_241,&g_241,(void*)0,&g_241,&g_241,&g_241,&g_241},{(void*)0,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241},{&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241},{(void*)0,&g_241,&g_241,&g_241,&g_241,(void*)0,&g_241,&g_241,&g_241,&g_241}};
    struct S0 **l_424[2];
    uint32_t l_426 = 4UL;
    float *l_429[10][8][3] = {{{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{(void*)0,(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{(void*)0,&g_193[0],&g_193[0]}},{{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{&g_193[0],&g_193[0],&g_193[0]}},{{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{&g_193[0],&g_193[0],(void*)0},{&g_193[0],&g_193[0],&g_193[0]},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]}},{{(void*)0,(void*)0,&g_193[0]},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]}},{{(void*)0,(void*)0,(void*)0},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]}},{{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]}},{{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]}},{{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]}},{{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{(void*)0,&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],(void*)0}},{{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]},{&g_193[0],(void*)0,&g_193[0]},{&g_193[0],&g_193[0],&g_193[0]}}};
    union U2 *l_476 = &g_445;
    float l_490 = 0x1.8p-1;
    int32_t **l_501 = &g_58;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_424[i] = &l_425[3][0];
lbl_433:
    for (g_54 = 3; (g_54 >= 0); g_54 -= 1)
    { /* block id: 29 */
        int32_t **l_126[5][3][7] = {{{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,(void*)0,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,(void*)0,(void*)0,&g_58,&g_58,&g_58,&g_58}},{{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,(void*)0,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,(void*)0,(void*)0,&g_58,&g_58,&g_58,&g_58}},{{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,(void*)0,&g_58}},{{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,(void*)0,&g_58}},{{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,(void*)0,&g_58}}};
        uint8_t l_217 = 0x54L;
        union U2 l_258[7][6][6] = {{{{-1L},{0xFCF270A3L},{-1L},{1L},{0xEF6D6D97L},{-7L}},{{-1L},{0x50ACC4D4L},{0L},{2L},{-1L},{0x22844F21L}},{{0L},{0xEA1FB0C5L},{0x5B22501FL},{-1L},{0x5B22501FL},{0xEA1FB0C5L}},{{0L},{-5L},{0x91E915D1L},{0xF4D37C64L},{-5L},{0xFCF270A3L}},{{0x8D58CC53L},{0x188E24E2L},{0x50AE0783L},{-1L},{0xEA1FB0C5L},{0x480F0B60L}},{{-5L},{0x188E24E2L},{0x603C1C09L},{1L},{-5L},{4L}}},{{{-1L},{-5L},{0L},{0L},{0x5B22501FL},{0L}},{{0xF5AD00F8L},{0xEA1FB0C5L},{0xFCF270A3L},{0x01A939D3L},{-1L},{0xE060827DL}},{{0L},{0x50ACC4D4L},{0xB82DB4B4L},{1L},{0xEF6D6D97L},{0xFCF270A3L}},{{0x5BA41FA7L},{0xFCF270A3L},{0xB82DB4B4L},{-7L},{0xEA1FB0C5L},{0xE060827DL}},{{0x8CF48FB0L},{0x603C1C09L},{0xFCF270A3L},{1L},{1L},{0L}},{{1L},{1L},{0L},{0L},{0x603C1C09L},{4L}}},{{{0x678A6898L},{0xEA1FB0C5L},{0x603C1C09L},{0x0392AC9FL},{0xFCF270A3L},{0x480F0B60L}},{{0L},{0xEF6D6D97L},{0x50AE0783L},{0x0392AC9FL},{0x50ACC4D4L},{0xFCF270A3L}},{{0x678A6898L},{-1L},{0x91E915D1L},{0L},{0xEA1FB0C5L},{0xEA1FB0C5L}},{{1L},{0x5B22501FL},{0x5B22501FL},{1L},{-5L},{0x22844F21L}},{{0x8CF48FB0L},{-5L},{0L},{-7L},{0x188E24E2L},{-7L}},{{0x5BA41FA7L},{0xEA1FB0C5L},{-1L},{1L},{0x188E24E2L},{9L}}},{{{0L},{-5L},{0x8F597F90L},{0x01A939D3L},{-5L},{0xFCF270A3L}},{{0xF5AD00F8L},{0x5B22501FL},{0x58A86DAAL},{0L},{0xEA1FB0C5L},{1L}},{{-1L},{-1L},{0x188E24E2L},{1L},{0x50ACC4D4L},{0xE954CA77L}},{{-5L},{0xEF6D6D97L},{0L},{4L},{0L},{0L}},{{-1L},{3L},{-4L},{0xEF6D6D97L},{0xCB4935A8L},{0x0F4CF030L}},{{-7L},{-8L},{9L},{1L},{-8L},{0L}}},{{{0x603C1C09L},{0xCB4935A8L},{0x2FD4E0F7L},{0xE954CA77L},{3L},{1L}},{{0x58A86DAAL},{0L},{0x1C1735F6L},{0x50AE0783L},{0xE5F67253L},{-7L}},{{0x58A86DAAL},{0L},{0x95154DCBL},{0xE954CA77L},{0x1C1735F6L},{0x9D5A252DL}},{{0x603C1C09L},{3L},{0x0E17AFFBL},{1L},{0x0E17AFFBL},{3L}},{{-7L},{0xF457C9E6L},{0x9A2BFAC5L},{0xEF6D6D97L},{0xE2E9172EL},{0L}},{{-1L},{-4L},{0x96D44F74L},{4L},{3L},{0xDD7419F2L}}},{{{0x91E915D1L},{-4L},{0xCB4935A8L},{0x50AE0783L},{0xE2E9172EL},{-1L}},{{0x8F597F90L},{0xF457C9E6L},{0x95154DCBL},{0L},{0x0E17AFFBL},{0x95154DCBL}},{{0xFCF270A3L},{3L},{0L},{0x50ACC4D4L},{0x1C1735F6L},{-1L}},{{-7L},{0L},{-1L},{-5L},{0xE5F67253L},{0L}},{{0x188E24E2L},{0L},{-1L},{0x22844F21L},{3L},{-1L}},{{0xB82DB4B4L},{0xCB4935A8L},{0L},{0x50AE0783L},{-8L},{0x95154DCBL}}},{{{0x50AE0783L},{-8L},{0x95154DCBL},{-7L},{0xCB4935A8L},{-1L}},{{0x5B22501FL},{3L},{0xCB4935A8L},{-5L},{0L},{0xDD7419F2L}},{{-7L},{0xE5F67253L},{0x96D44F74L},{-5L},{0L},{0L}},{{0x5B22501FL},{0x1C1735F6L},{0x9A2BFAC5L},{-7L},{3L},{3L}},{{0x50AE0783L},{0x0E17AFFBL},{0x0E17AFFBL},{0x50AE0783L},{0xF457C9E6L},{0x9D5A252DL}},{{0xB82DB4B4L},{0xE2E9172EL},{0x95154DCBL},{0x22844F21L},{-4L},{-7L}}}};
        struct S0 **l_379 = (void*)0;
        struct S0 *l_381 = &g_241;
        struct S0 **l_380 = &l_381;
        int16_t *l_383 = &g_168;
        uint32_t *l_384 = (void*)0;
        uint32_t *l_385 = &g_276;
        uint32_t *l_386[8][9] = {{&l_382,(void*)0,(void*)0,(void*)0,(void*)0,&l_382,&l_382,&l_382,(void*)0},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,(void*)0},{(void*)0,(void*)0,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,(void*)0},{&l_382,&l_382,(void*)0,(void*)0,&l_382,&l_382,&l_382,&l_382,(void*)0},{&l_382,&l_382,&l_382,&l_382,&l_382,(void*)0,&l_382,&l_382,(void*)0},{(void*)0,&l_382,&l_382,&l_382,(void*)0,(void*)0,&l_382,&l_382,&l_382},{&l_382,(void*)0,(void*)0,&l_382,(void*)0,(void*)0,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,(void*)0,&l_382,(void*)0,&l_382,(void*)0,&l_382}};
        const uint64_t *l_388 = &g_238;
        const uint64_t **l_387 = &l_388;
        uint8_t *** const *l_401 = &g_376;
        uint8_t *** const **l_400 = &l_401;
        struct S0 ***l_423[4][1];
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
                l_423[i][j] = (void*)0;
        }
        g_58 = (g_125[g_54] = g_125[g_54]);
        for (p_114 = (-26); (p_114 != 1); p_114 = safe_add_func_int64_t_s_s(p_114, 9))
        { /* block id: 34 */
            union U2 l_166[4][1][5] = {{{{0x83AED21BL},{0xF5847AAFL},{0x83AED21BL},{0xF5847AAFL},{0x83AED21BL}}},{{{0xCAC88EEAL},{0xC1C087C3L},{0xC1C087C3L},{0xCAC88EEAL},{0xCAC88EEAL}}},{{{-7L},{0xF5847AAFL},{-7L},{0xF5847AAFL},{-7L}}},{{{0xCAC88EEAL},{0xCAC88EEAL},{0xC1C087C3L},{0xC1C087C3L},{0xCAC88EEAL}}}};
            uint32_t *l_170 = &g_64;
            uint8_t *l_174 = (void*)0;
            uint8_t **l_173 = &l_174;
            uint8_t ***l_172 = &l_173;
            uint8_t ****l_171 = &l_172;
            int32_t l_181 = 0L;
            int32_t l_182 = (-8L);
            int32_t l_183 = 0x7A493877L;
            int32_t l_184[1][3];
            uint64_t l_240 = 0x449B74578E65A7A0LL;
            int32_t l_302[7][9] = {{0xA4DF166BL,0xE48A2C21L,0x3F771F14L,1L,0xE3340633L,1L,0x3F771F14L,0xE48A2C21L,0xA4DF166BL},{0x9413205AL,1L,(-4L),1L,(-4L),1L,0x9413205AL,0x9413205AL,1L},{5L,0xE5D210BAL,0x3F771F14L,0xE5D210BAL,5L,0xE19047CAL,0x55292B81L,1L,0x3CCFAFCFL},{0x9413205AL,(-4L),0x9413205AL,5L,0L,0L,5L,0x9413205AL,(-4L)},{0xA4DF166BL,0x7AF79196L,0x55292B81L,0xCB86EDECL,0x3F771F14L,0xE19047CAL,0x6BA36E7AL,0xE48A2C21L,0x6BA36E7AL},{1L,1L,5L,5L,1L,1L,0L,1L,1L},{0x3CCFAFCFL,0x7AF79196L,0x6BA36E7AL,0xE5D210BAL,0xA4DF166BL,1L,0xA4DF166BL,0xE5D210BAL,0x6BA36E7AL}};
            uint64_t l_339 = 0x70F25A0BAEF3A465LL;
            uint16_t l_360[2];
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_184[i][j] = 1L;
            }
            for (i = 0; i < 2; i++)
                l_360[i] = 7UL;
            for (g_91 = 3; (g_91 >= 0); g_91 -= 1)
            { /* block id: 37 */
                int8_t *l_142 = &g_143;
                uint8_t *l_144 = &g_145[2];
                union U2 l_158 = {-1L};
                uint8_t ****l_176[10][10][1] = {{{(void*)0},{(void*)0},{&l_172},{&l_172},{&l_172},{(void*)0},{&l_172},{&l_172},{&l_172},{&l_172}},{{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172}},{{&l_172},{(void*)0},{&l_172},{&l_172},{&l_172},{(void*)0},{(void*)0},{&l_172},{&l_172},{&l_172}},{{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172}},{{&l_172},{(void*)0},{(void*)0},{&l_172},{&l_172},{&l_172},{(void*)0},{&l_172},{&l_172},{&l_172}},{{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172}},{{&l_172},{&l_172},{(void*)0},{&l_172},{&l_172},{&l_172},{(void*)0},{(void*)0},{&l_172},{&l_172}},{{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172}},{{&l_172},{&l_172},{(void*)0},{(void*)0},{&l_172},{&l_172},{&l_172},{(void*)0},{&l_172},{&l_172}},{{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172},{&l_172}}};
                int i, j, k;
                if ((safe_mul_func_uint16_t_u_u(l_131, (safe_mul_func_int8_t_s_s(6L, ((*l_144) = (p_115 , (safe_mul_func_uint16_t_u_u(p_115.f1, ((safe_rshift_func_int16_t_s_s(((g_8[0] , (safe_rshift_func_int8_t_s_s(g_64, 4))) && ((safe_mul_func_uint16_t_u_u((((*l_142) = g_8[2]) ^ (((((void*)0 != &g_7[1][0][3]) , l_131) , p_115.f1) ^ p_115.f1)), 65528UL)) == 0x70F7B9E3EC411819LL)), 3)) | g_3))))))))))
                { /* block id: 40 */
                    uint8_t **l_146 = &l_144;
                    uint8_t ***l_147 = &l_146;
                    int i;
                    (*l_147) = l_146;
                    if ((*g_58))
                        continue;
                }
                else
                { /* block id: 43 */
                    int16_t *l_167 = &g_168;
                    if ((safe_rshift_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u(p_114, 1L)), ((p_115.f0 && ((*l_144) = ((l_169 = (((safe_sub_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u(g_145[1], ((l_158 , (safe_div_func_int64_t_s_s(g_54, (~(safe_add_func_uint64_t_u_u(g_8[1], g_8[1])))))) , ((*l_167) = (safe_mod_func_int64_t_s_s((l_166[0][0][4] , p_115.f1), p_114)))))) ^ l_158.f0), l_131)) >= l_131) & p_114)) >= 0x014C235FF65E9E55LL))) , (-1L)))) , 0UL), 5)))
                    { /* block id: 47 */
                        if ((*p_116))
                            break;
                    }
                    else
                    { /* block id: 49 */
                        return l_170;
                    }
                    for (g_143 = 0; (g_143 <= 2); g_143 += 1)
                    { /* block id: 54 */
                        uint8_t *****l_175[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_175[i] = &l_171;
                        l_176[0][3][0] = l_171;
                    }
                    return &g_64;
                }
            }
            if ((g_125[g_54] == p_116))
            { /* block id: 60 */
                int32_t l_179 = 7L;
                int32_t l_180[9][6][4] = {{{0L,0L,6L,2L},{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L}},{{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L}},{{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L}},{{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L}},{{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L}},{{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L}},{{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L}},{{6L,0x62082439L,0x62082439L,6L},{6L,0x62082439L,2L,0x62082439L},{0x62082439L,0L,2L,2L},{6L,6L,0x62082439L,2L},{6L,0L,6L,0x62082439L},{6L,0x62082439L,0x62082439L,6L}},{{6L,0x62082439L,0L,6L},{6L,6L,0L,0L},{0x62082439L,0x62082439L,6L,0L},{2L,6L,2L,6L},{2L,6L,6L,2L},{0x62082439L,6L,0L,6L}}};
                uint16_t *l_220 = &g_221;
                int64_t *l_231 = &g_232;
                uint64_t *l_237 = &g_238;
                struct S0 *l_242 = &g_241;
                int i, j, k;
                ++l_185;
                for (g_91 = 0; (g_91 <= 0); g_91 += 1)
                { /* block id: 64 */
                    uint32_t l_194[9] = {0x4EC0A882L,0x4EC0A882L,0x4EC0A882L,0x4EC0A882L,0x4EC0A882L,0x4EC0A882L,0x4EC0A882L,0x4EC0A882L,0x4EC0A882L};
                    int32_t l_201 = 1L;
                    int i;
                    for (g_168 = 4; (g_168 >= 1); g_168 -= 1)
                    { /* block id: 67 */
                        float *l_192[10] = {&g_193[0],&g_193[0],&g_193[0],&g_193[0],&g_193[0],&g_193[0],&g_193[0],&g_193[0],&g_193[0],&g_193[0]};
                        int i;
                        l_201 = (safe_sub_func_float_f_f(g_145[(g_91 + 3)], ((l_194[4] = (safe_mul_func_float_f_f(g_145[g_168], (-0x1.2p+1)))) != (g_145[g_91] <= ((+(((-0x1.5p+1) == p_115.f0) < (((((safe_lshift_func_int16_t_s_u(9L, 11)) , ((l_180[5][0][1] ^ p_114) >= 0L)) || g_8[0]) , g_198[1][2][4]) == &g_199))) >= l_169)))));
                        l_180[5][0][1] = (+g_145[2]);
                    }
                    l_178[7][3] = (((l_174 == ((safe_sub_func_float_f_f(((safe_add_func_float_f_f((safe_mul_func_float_f_f(0xD.0DCD38p-49, ((l_131 > (&g_54 == (void*)0)) != (safe_sub_func_float_f_f((!((!((safe_mul_func_uint16_t_u_u(p_115.f0, 0UL)) | g_91)) , ((safe_add_func_float_f_f(0x1.2p-1, l_217)) >= p_115.f0))), g_64))))), 0x7.38AD2Cp-25)) != p_115.f1), 0x0.3p+1)) , &g_8[0])) , 0x57DBE0CDL) , (*p_116));
                    return p_116;
                }
                l_240 = (((safe_div_func_uint16_t_u_u((--(*l_220)), g_8[1])) , (l_184[0][1] = ((safe_mod_func_int8_t_s_s((((((***l_171) = &g_145[2]) == l_226) != 0UL) , ((safe_mod_func_int32_t_s_s(((-6L) == (g_54 && (((((g_239 |= ((((safe_mod_func_uint32_t_u_u((((((*l_231) = g_11) & (safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((((*l_237) ^= (p_115.f1 , ((g_145[2] <= 249UL) <= p_114))) < 1L), p_115.f0)), g_91))) ^ 0xF7EF872575A83E09LL) , p_114), l_179)) | g_145[0]) | l_166[0][0][4].f0) , l_182)) | g_168) , 0x70D45C56L) , p_114) >= (-3L)))), p_115.f0)) , (-9L))), g_168)) == g_8[1]))) < p_114);
                (*l_242) = g_241;
            }
            else
            { /* block id: 83 */
                struct S0 *l_259 = (void*)0;
                int32_t l_266 = 0x4190088FL;
                uint64_t *l_269 = &l_240;
                int32_t *l_271 = &l_178[7][3];
                uint8_t * const *l_281[8][7] = {{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226},{&l_226,&l_226,&l_226,&l_226,&l_226,&l_226,&l_226}};
                uint8_t * const **l_280 = &l_281[3][6];
                uint8_t * const ***l_279 = &l_280;
                uint64_t l_315[3][4][3] = {{{18446744073709551615UL,18446744073709551607UL,0UL},{5UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551607UL,0UL},{5UL,18446744073709551615UL,18446744073709551615UL}},{{18446744073709551615UL,18446744073709551607UL,0UL},{5UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551607UL,0UL},{5UL,18446744073709551615UL,18446744073709551615UL}},{{18446744073709551615UL,18446744073709551607UL,0UL},{5UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551607UL,0UL},{5UL,18446744073709551615UL,18446744073709551615UL}}};
                int32_t l_320 = 0x892C7EFDL;
                int32_t l_324 = 0x15E47498L;
                int32_t l_331 = 0L;
                int32_t l_332 = 0x61C8AC3AL;
                int32_t l_336 = (-1L);
                int32_t l_337 = 9L;
                union U2 l_351[7][10][3] = {{{{0xA2BED963L},{0xDBBC01CAL},{0xF4EB474EL}},{{0xE29CF1B3L},{-6L},{0x5C710D65L}},{{0x62245AE1L},{4L},{-1L}},{{0x571E0C31L},{0xFA7A597BL},{0x64853EBDL}},{{0x85479D56L},{0x54E1739EL},{4L}},{{1L},{0x28A2C5BBL},{0x64853EBDL}},{{0xF18C21F3L},{-1L},{-1L}},{{4L},{-6L},{0x5C710D65L}},{{7L},{0x33E4131CL},{0xF4EB474EL}},{{-4L},{0x624D6171L},{0x624D6171L}}},{{{7L},{1L},{0xB8FAD088L}},{{4L},{0xC8503016L},{-6L}},{{0xF18C21F3L},{0xB8FAD088L},{0x54E1739EL}},{{1L},{0x5C710D65L},{-1L}},{{0x85479D56L},{0xB8FAD088L},{0xDBBC01CAL}},{{0x571E0C31L},{0xC8503016L},{0x884F4C1FL}},{{0x62245AE1L},{1L},{1L}},{{0xE29CF1B3L},{0x624D6171L},{0xC8503016L}},{{0xA2BED963L},{0x33E4131CL},{1L}},{{-6L},{0x5237DCF1L},{1L}}},{{{1L},{9L},{0xF31EDEC0L}},{{-1L},{0x69455F23L},{0xEE8F932CL}},{{0xB8FAD088L},{1L},{1L}},{{-1L},{0x68C0A9DDL},{0x92609108L}},{{1L},{5L},{-7L}},{{-6L},{0x92609108L},{0L}},{{4L},{0xF31EDEC0L},{1L}},{{0x5C710D65L},{0x92609108L},{0x03EBD15CL}},{{-1L},{5L},{0x348B77A7L}},{{0x4E1DD453L},{0x68C0A9DDL},{0x6551FC91L}}},{{{0xDBBC01CAL},{1L},{5L}},{{0xC8503016L},{0x69455F23L},{0x6551FC91L}},{{-1L},{9L},{0x348B77A7L}},{{0x64853EBDL},{0x5237DCF1L},{0x03EBD15CL}},{{0L},{0xDB32BBC0L},{1L}},{{-6L},{0L},{0L}},{{0L},{0xEBC41AD2L},{-7L}},{{0x64853EBDL},{0x0B2C81C1L},{0x92609108L}},{{-1L},{-7L},{1L}},{{0xC8503016L},{0x03EBD15CL},{0xEE8F932CL}}},{{{0xDBBC01CAL},{-7L},{0xF31EDEC0L}},{{0x4E1DD453L},{0x0B2C81C1L},{1L}},{{-1L},{0xEBC41AD2L},{8L}},{{0x5C710D65L},{0L},{0x0B2C81C1L}},{{4L},{0xDB32BBC0L},{8L}},{{-6L},{0x5237DCF1L},{1L}},{{1L},{9L},{0xF31EDEC0L}},{{-1L},{0x69455F23L},{0xEE8F932CL}},{{0xB8FAD088L},{1L},{1L}},{{-1L},{0x68C0A9DDL},{0x92609108L}}},{{{1L},{5L},{-7L}},{{-6L},{0x92609108L},{0L}},{{4L},{0xF31EDEC0L},{1L}},{{0x5C710D65L},{0x92609108L},{0x03EBD15CL}},{{-1L},{5L},{0x348B77A7L}},{{0x4E1DD453L},{0x68C0A9DDL},{0x6551FC91L}},{{0xDBBC01CAL},{1L},{5L}},{{0xC8503016L},{0x69455F23L},{0x6551FC91L}},{{-1L},{9L},{0x348B77A7L}},{{0x64853EBDL},{0x5237DCF1L},{0x03EBD15CL}}},{{{0L},{0xDB32BBC0L},{1L}},{{-6L},{0L},{0L}},{{0L},{0xEBC41AD2L},{-7L}},{{0x64853EBDL},{0x0B2C81C1L},{0x92609108L}},{{-1L},{-7L},{1L}},{{0xC8503016L},{0x03EBD15CL},{0xEE8F932CL}},{{0xDBBC01CAL},{-7L},{0xF31EDEC0L}},{{0x4E1DD453L},{0x0B2C81C1L},{1L}},{{-1L},{0xEBC41AD2L},{8L}},{{0x5C710D65L},{0L},{0x0B2C81C1L}}}};
                int8_t l_358 = 1L;
                int i, j, k;
                for (l_181 = 0; (l_181 != 6); ++l_181)
                { /* block id: 86 */
                    int64_t *l_267 = &g_232;
                    int32_t l_268 = 1L;
                    uint8_t ****l_282 = (void*)0;
                    int32_t l_318 = 0x60C2995DL;
                    int32_t l_319[6][1] = {{(-6L)},{0x08BEAA29L},{(-6L)},{0x08BEAA29L},{(-6L)},{0x08BEAA29L}};
                    int64_t l_330 = 0x89F8EDF3B5059EE4LL;
                    int32_t l_333 = 0x0B22DD97L;
                    uint16_t *l_357 = &g_221;
                    int16_t *l_359 = &g_168;
                    int8_t *l_362 = &g_91;
                    int i, j;
                    if ((l_266 = (safe_div_func_int32_t_s_s(((+g_143) != (safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u(((safe_rshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s((((((((safe_sub_func_int64_t_s_s(p_115.f1, (l_258[0][3][4] , ((void*)0 != l_259)))) != ((safe_sub_func_int8_t_s_s(g_91, g_168)) < (((safe_rshift_func_uint16_t_u_s(((safe_mul_func_uint8_t_u_u((((*l_267) ^= l_266) | l_131), 0x31L)) | (*g_58)), 9)) || l_268) < (-9L)))) , l_269) == &g_238) , &p_114) != (void*)0) & g_3), g_8[1])), l_169)) , 0x3653L), g_270)), 10))), g_8[7]))))
                    { /* block id: 89 */
                        return p_116;
                    }
                    else
                    { /* block id: 91 */
                        float *l_272[4];
                        uint32_t *l_275 = &g_276;
                        uint8_t *****l_283 = (void*)0;
                        uint8_t *****l_284 = &l_171;
                        int32_t l_297 = (-1L);
                        int i;
                        for (i = 0; i < 4; i++)
                            l_272[i] = (void*)0;
                        l_271 = &g_3;
                        g_193[0] = l_178[7][3];
                        l_177 &= (l_178[7][3] &= (g_64 , (p_115.f0 = ((safe_mul_func_int8_t_s_s(((((--(*l_275)) , l_279) != ((*l_284) = l_282)) == (*p_116)), ((g_8[2] >= (p_115.f1 | (((safe_div_func_int8_t_s_s((((safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((safe_mod_func_int64_t_s_s(((safe_div_func_uint32_t_u_u(1UL, ((safe_rshift_func_uint8_t_u_s(p_115.f0, 0)) & ((g_168 , g_3) >= g_64)))) && 0x14L), l_297)), p_114)), 0xE0L)) == l_169) && g_241.f0), p_115.f0)) <= g_270) > g_3))) ^ l_297))) , l_268))));
                    }
                    for (g_276 = 0; (g_276 <= 6); g_276 += 1)
                    { /* block id: 102 */
                        int32_t l_305 = 1L;
                        volatile uint64_t ***l_317 = &g_316;
                        int32_t l_321 = 0x4933FC6AL;
                        int32_t l_322 = 0xA7F80732L;
                        int32_t l_323 = 0x9F2893A7L;
                        int32_t l_325 = 0xA6E41CB4L;
                        int32_t l_326 = 0xD0A1748FL;
                        uint16_t l_327 = 0x8D89L;
                        int32_t l_334 = 0x15692831L;
                        int32_t l_335 = (-5L);
                        int32_t l_338 = 1L;
                        int i;
                        (*l_317) = (((safe_mul_func_int16_t_s_s((g_145[g_54] & ((safe_add_func_int64_t_s_s(g_3, (g_238 = 1UL))) <= l_302[2][8])), ((safe_add_func_int32_t_s_s((l_305 |= (l_166[0][0][4].f0 &= 2L)), (p_115.f1 , (safe_sub_func_uint32_t_u_u(g_241.f1, (safe_add_func_uint8_t_u_u((((((safe_add_func_uint8_t_u_u((safe_div_func_int64_t_s_s((g_314[7] , ((*l_267) = l_178[1][1])), g_145[g_54])), l_183)) , l_268) && p_114) & g_145[2]) & p_114), 0xDBL))))))) >= l_315[2][1][1]))) , g_314[5]) , g_316);
                        p_115.f1 = (*p_116);
                        l_327++;
                        --l_339;
                    }
                    p_115.f0 = (safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(((*l_226) = ((((((*l_362) = ((((((((((&p_116 != &g_58) & ((safe_div_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((p_114 >= ((l_350 , l_351[1][1][2]) , ((~l_268) ^ (((safe_mul_func_uint16_t_u_u(((*l_357) |= 0x42F5L), ((*l_359) = l_358))) , (((((l_360[1] && p_114) | 0L) | l_131) , p_115.f1) > (-1L))) < l_318)))), l_169)), l_361)) >= 3UL)) != p_115.f0) , 1UL) , l_169) , (**l_172)) != l_226) && 0xA1BCL) == 0L) | p_115.f1)) < (*l_271)) <= 0xA115L) & l_178[3][1]) >= 1L)), 4)), g_241.f1));
                }
            }
        }
        if ((((safe_mod_func_int16_t_s_s(g_145[2], (safe_lshift_func_uint8_t_u_s(((((safe_add_func_uint32_t_u_u(1UL, (l_177 = ((*l_385) ^= (safe_sub_func_int16_t_s_s((safe_mod_func_int8_t_s_s(((p_115.f1 = (safe_mod_func_int64_t_s_s((0x994FL ^ ((((g_375 = g_375) == &g_376) , &g_241) != ((*l_380) = &p_115))), l_382))) , (((*l_383) |= p_115.f0) , g_241.f0)), l_382)), p_115.f0)))))) , &p_116) == &g_125[g_54]) ^ p_115.f0), p_114)))) , l_387) != (void*)0))
        { /* block id: 126 */
            l_389 |= (l_178[4][0] |= (l_169 = 0x5F841BD6L));
        }
        else
        { /* block id: 130 */
            int32_t *l_396 = &g_314[7].f0;
            int32_t *l_399 = &l_258[0][3][4].f0;
            uint8_t **** const *l_402 = &g_375;
            int32_t l_415 = 0x778426ACL;
            float **l_418 = (void*)0;
            float *l_420 = &g_193[0];
            float **l_419 = &l_420;
            float *l_421 = &g_193[0];
            (*l_399) = (safe_mul_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u(((l_396 = &g_239) != ((safe_rshift_func_uint8_t_u_s(p_114, 1)) , l_399)), (((l_400 != (g_403[4][0][3] = l_402)) , ((g_3 , g_54) >= (safe_sub_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u((+(safe_sub_func_uint32_t_u_u((l_415 > (safe_add_func_int8_t_s_s((((*l_385) = ((l_421 = ((*l_419) = p_116)) == l_422)) <= g_241.f1), 0L))), (-4L)))), 1)), g_270)), (*l_399))))) & (-10L)))) , (****g_404)), (****g_375))), 1UL));
        }
        l_424[0] = &l_381;
    }
    if (l_426)
    { /* block id: 140 */
        float *l_427 = &g_193[0];
        float **l_428 = &l_422;
        int32_t *l_430[3];
        int32_t *l_432 = &g_239;
        int32_t l_484 = 0x89FEDBDFL;
        int8_t l_485 = (-1L);
        int32_t * const l_502 = &l_361;
        int32_t **l_503 = &l_430[0];
        int i;
        for (i = 0; i < 3; i++)
            l_430[i] = &l_361;
        l_178[7][3] = (((*l_428) = l_427) == l_429[4][0][0]);
        if (l_178[8][4])
        { /* block id: 143 */
            int32_t **l_431 = &l_430[0];
            int16_t l_482[6][1][8] = {{{(-1L),0x11F2L,0xF202L,0x780EL,1L,4L,0L,0x1FCEL}},{{0L,4L,4L,0x780EL,1L,0x780EL,4L,4L}},{{0L,1L,(-6L),0xE10FL,0x1FCEL,0x1298L,0L,0xF62CL}},{{0xE10FL,0x0A9CL,4L,(-6L),0L,0L,0L,0L}},{{(-1L),1L,1L,(-1L),0x11F2L,0xF202L,0x780EL,1L}},{{0x11F2L,0xF202L,0x780EL,1L,4L,0L,0x1FCEL,0xC8A7L}}};
            uint32_t *l_483 = &l_185;
            int i, j, k;
            (*l_431) = &g_3;
            p_116 = l_432;
            if (g_232)
                goto lbl_433;
            for (l_169 = (-30); (l_169 == (-5)); l_169 = safe_add_func_int64_t_s_s(l_169, 3))
            { /* block id: 149 */
                uint32_t l_446 = 0xEB309497L;
                union U2 *l_469[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_469[i] = &g_445;
                for (l_185 = 0; (l_185 != 45); l_185++)
                { /* block id: 152 */
                    int16_t l_479 = 0x3E3DL;
                    for (g_221 = 29; (g_221 >= 27); g_221 = safe_sub_func_int16_t_s_s(g_221, 9))
                    { /* block id: 155 */
                        union U2 *l_444 = &g_314[7];
                        int8_t *l_458 = (void*)0;
                        int8_t *l_459 = (void*)0;
                        int8_t *l_460[8][2][4] = {{{&g_7[0][0][3],&g_91,&g_91,&g_7[0][0][3]},{&g_91,&g_7[0][0][3],&g_91,&g_91}},{{&g_7[0][0][3],&g_7[0][0][3],(void*)0,&g_7[0][0][3]},{&g_7[0][0][3],&g_91,&g_91,&g_7[0][0][3]}},{{&g_91,&g_7[0][0][3],&g_91,&g_91},{&g_7[0][0][3],&g_7[0][0][3],(void*)0,&g_7[0][0][3]}},{{&g_7[0][0][3],&g_91,&g_91,&g_7[0][0][3]},{&g_91,&g_7[0][0][3],&g_91,&g_91}},{{&g_7[0][0][3],&g_7[0][0][3],(void*)0,&g_7[0][0][3]},{&g_7[0][0][3],&g_91,&g_91,&g_7[0][0][3]}},{{&g_91,&g_7[0][0][3],&g_91,&g_91},{&g_7[0][0][3],&g_7[0][0][3],(void*)0,&g_7[0][0][3]}},{{&g_7[0][0][3],&g_91,&g_91,&g_7[0][0][3]},{&g_91,&g_7[0][0][3],&g_91,&g_91}},{{&g_7[0][0][3],&g_7[0][0][3],(void*)0,&g_7[0][0][3]},{&g_7[0][0][3],&g_91,&g_91,&g_7[0][0][3]}}};
                        union U2 **l_470 = &l_444;
                        int32_t l_471 = 0xBE523E81L;
                        int16_t *l_480 = (void*)0;
                        int16_t *l_481[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i, j, k;
                        g_241 = ((g_91 = (p_114 = (((**l_431) == ((safe_rshift_func_int16_t_s_s((*l_432), (safe_div_func_uint32_t_u_u(((g_445 = ((*l_444) = (g_168 , g_314[7]))) , (g_314[8] , ((l_446 < (safe_div_func_uint16_t_u_u(((safe_sub_func_uint32_t_u_u(((+(safe_mul_func_uint8_t_u_u(((safe_div_func_int16_t_s_s(g_64, ((safe_sub_func_uint64_t_u_u(0UL, l_169)) || (*l_432)))) > p_115.f0), g_145[0]))) > p_114), g_145[2])) >= g_7[1][0][2]), 0x40ACL))) | (*p_116)))), 4294967295UL)))) < 0x3C12B8C4EB826DE9LL)) , g_11))) , p_115);
                        if ((*p_116))
                            break;
                        (*p_116) &= (((void*)0 != p_116) | ((safe_div_func_int32_t_s_s(((safe_rshift_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(((**l_431) ^ ((safe_sub_func_uint64_t_u_u(((((*l_470) = l_469[0]) != &g_445) ^ l_471), (safe_mul_func_int8_t_s_s((((safe_sub_func_int32_t_s_s((((void*)0 != l_476) , (((safe_sub_func_int16_t_s_s(((l_178[5][0] = l_479) , l_350.f1), p_115.f1)) > 0x2983L) ^ g_64)), g_7[0][0][3])) , 0xE4L) | 8UL), p_115.f0)))) >= (**l_431))), l_350.f0)), g_145[1])) < l_482[4][0][5]), 0x3B3C3B25L)) | 4294967294UL));
                    }
                    if ((*p_116))
                        continue;
                    if ((**l_431))
                        continue;
                }
                p_116 = &g_3;
                p_115 = g_241;
                return p_116;
            }
        }
        else
        { /* block id: 173 */
            int32_t l_486[9][4] = {{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)},{(-1L),(-4L),(-1L),(-4L)}};
            int32_t l_487 = 0x6BECB0A1L;
            int32_t l_488[9];
            float l_489 = 0x2.Dp-1;
            uint32_t l_491 = 0x44D77566L;
            uint64_t *l_496 = &g_238;
            int32_t **l_499 = &g_125[2];
            int32_t ***l_500 = &l_499;
            int i, j;
            for (i = 0; i < 9; i++)
                l_488[i] = 1L;
            ++l_491;
            l_178[7][3] ^= (safe_rshift_func_int16_t_s_u(((((*l_496)++) , ((*l_500) = l_499)) != (l_501 = &p_116)), 8));
        }
        (*l_503) = l_502;
        (*l_501) = &g_239;
    }
    else
    { /* block id: 182 */
        uint8_t l_504[1][7][8] = {{{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L},{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L},{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L},{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L},{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L},{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L},{0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L,0x10L}}};
        int i, j, k;
        l_504[0][4][0]--;
    }
    return p_116;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_7[i][j][k], "g_7[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_8[i], "g_8[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_43[i][j][k], "g_43[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_46.f0, "g_46.f0", print_hash_value);
    transparent_crc(g_52.f0, "g_52.f0", print_hash_value);
    transparent_crc(g_52.f1, "g_52.f1", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    transparent_crc(g_64, "g_64", print_hash_value);
    transparent_crc(g_91, "g_91", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_145[i], "g_145[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_168, "g_168", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_193[i], sizeof(g_193[i]), "g_193[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_221, "g_221", print_hash_value);
    transparent_crc(g_232, "g_232", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    transparent_crc(g_239, "g_239", print_hash_value);
    transparent_crc(g_241.f0, "g_241.f0", print_hash_value);
    transparent_crc(g_241.f1, "g_241.f1", print_hash_value);
    transparent_crc(g_270, "g_270", print_hash_value);
    transparent_crc(g_276, "g_276", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_314[i].f0, "g_314[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_445.f0, "g_445.f0", print_hash_value);
    transparent_crc(g_508.f0, "g_508.f0", print_hash_value);
    transparent_crc(g_818.f0, "g_818.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_820[i][j][k].f0, "g_820[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_838.f0, "g_838.f0", print_hash_value);
    transparent_crc(g_973, "g_973", print_hash_value);
    transparent_crc(g_976, "g_976", print_hash_value);
    transparent_crc(g_1026, "g_1026", print_hash_value);
    transparent_crc(g_1027, "g_1027", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1094[i], "g_1094[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1097, "g_1097", print_hash_value);
    transparent_crc(g_1107, "g_1107", print_hash_value);
    transparent_crc(g_1292, "g_1292", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1299[i].f0, "g_1299[i].f0", print_hash_value);
        transparent_crc(g_1299[i].f1, "g_1299[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1397.f0, "g_1397.f0", print_hash_value);
    transparent_crc(g_1399.f0, "g_1399.f0", print_hash_value);
    transparent_crc(g_1496, "g_1496", print_hash_value);
    transparent_crc(g_1550, "g_1550", print_hash_value);
    transparent_crc(g_1605, "g_1605", print_hash_value);
    transparent_crc(g_1678, "g_1678", print_hash_value);
    transparent_crc(g_1686.f0, "g_1686.f0", print_hash_value);
    transparent_crc(g_1727.f0, "g_1727.f0", print_hash_value);
    transparent_crc(g_1727.f1, "g_1727.f1", print_hash_value);
    transparent_crc(g_1771, "g_1771", print_hash_value);
    transparent_crc(g_1795, "g_1795", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1823[i].f0, "g_1823[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1859, "g_1859", print_hash_value);
    transparent_crc(g_1915.f0, "g_1915.f0", print_hash_value);
    transparent_crc(g_1962.f0, "g_1962.f0", print_hash_value);
    transparent_crc(g_1969.f0, "g_1969.f0", print_hash_value);
    transparent_crc(g_1974.f0, "g_1974.f0", print_hash_value);
    transparent_crc(g_2082.f0, "g_2082.f0", print_hash_value);
    transparent_crc(g_2082.f1, "g_2082.f1", print_hash_value);
    transparent_crc(g_2090.f0, "g_2090.f0", print_hash_value);
    transparent_crc(g_2497, "g_2497", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2640[i].f0, "g_2640[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2657[i], "g_2657[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2676, "g_2676", print_hash_value);
    transparent_crc(g_2808, "g_2808", print_hash_value);
    transparent_crc(g_2830.f0, "g_2830.f0", print_hash_value);
    transparent_crc(g_2860, "g_2860", print_hash_value);
    transparent_crc(g_2911, "g_2911", print_hash_value);
    transparent_crc(g_3148, "g_3148", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_3206[i][j][k], "g_3206[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3216.f0, "g_3216.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_3249[i], "g_3249[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_3262[i], sizeof(g_3262[i]), "g_3262[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3279.f0, "g_3279.f0", print_hash_value);
    transparent_crc(g_3315.f0, "g_3315.f0", print_hash_value);
    transparent_crc(g_3420.f0, "g_3420.f0", print_hash_value);
    transparent_crc(g_3435.f0, "g_3435.f0", print_hash_value);
    transparent_crc_bytes (&g_3465, sizeof(g_3465), "g_3465", print_hash_value);
    transparent_crc(g_3501.f0, "g_3501.f0", print_hash_value);
    transparent_crc(g_3586, "g_3586", print_hash_value);
    transparent_crc(g_3613, "g_3613", print_hash_value);
    transparent_crc(g_3642, "g_3642", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 947
   depth: 1, occurrence: 38
XXX total union variables: 34

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 56
breakdown:
   indirect level: 0, occurrence: 38
   indirect level: 1, occurrence: 7
   indirect level: 2, occurrence: 6
   indirect level: 3, occurrence: 3
   indirect level: 4, occurrence: 2
XXX full-bitfields structs in the program: 38
breakdown:
   indirect level: 0, occurrence: 38
XXX times a bitfields struct's address is taken: 135
XXX times a bitfields struct on LHS: 18
XXX times a bitfields struct on RHS: 74
XXX times a single bitfield on LHS: 32
XXX times a single bitfield on RHS: 207

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 288
   depth: 2, occurrence: 73
   depth: 3, occurrence: 4
   depth: 4, occurrence: 4
   depth: 5, occurrence: 3
   depth: 7, occurrence: 2
   depth: 9, occurrence: 1
   depth: 14, occurrence: 2
   depth: 16, occurrence: 6
   depth: 18, occurrence: 2
   depth: 19, occurrence: 5
   depth: 20, occurrence: 3
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 6
   depth: 24, occurrence: 3
   depth: 25, occurrence: 4
   depth: 26, occurrence: 2
   depth: 27, occurrence: 3
   depth: 28, occurrence: 3
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 36, occurrence: 3
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1

XXX total number of pointers: 721

XXX times a variable address is taken: 1770
XXX times a pointer is dereferenced on RHS: 360
breakdown:
   depth: 1, occurrence: 243
   depth: 2, occurrence: 69
   depth: 3, occurrence: 35
   depth: 4, occurrence: 13
XXX times a pointer is dereferenced on LHS: 377
breakdown:
   depth: 1, occurrence: 322
   depth: 2, occurrence: 37
   depth: 3, occurrence: 8
   depth: 4, occurrence: 10
XXX times a pointer is compared with null: 70
XXX times a pointer is compared with address of another variable: 11
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 14035

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 4992
   level: 2, occurrence: 502
   level: 3, occurrence: 360
   level: 4, occurrence: 287
   level: 5, occurrence: 7
XXX number of pointers point to pointers: 310
XXX number of pointers point to scalars: 353
XXX number of pointers point to structs: 22
XXX percent of pointers has null in alias set: 29.7
XXX average alias set size: 1.48

XXX times a non-volatile is read: 2804
XXX times a non-volatile is write: 1271
XXX times a volatile is read: 107
XXX    times read thru a pointer: 41
XXX times a volatile is write: 45
XXX    times written thru a pointer: 35
XXX times a volatile is available for access: 1.32e+03
XXX percentage of non-volatile access: 96.4

XXX forward jumps: 2
XXX backward jumps: 12

XXX stmts: 279
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 28
   depth: 2, occurrence: 34
   depth: 3, occurrence: 46
   depth: 4, occurrence: 64
   depth: 5, occurrence: 80

XXX percentage a fresh-made variable is used: 17
XXX percentage an existing variable is used: 83
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


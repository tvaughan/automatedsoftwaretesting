/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3112266700
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile int16_t  f0;
   int8_t  f1;
   uint8_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_7[8][5][1] = {{{0xEC752F67L},{0L},{0xB1658084L},{2L},{(-8L)}},{{0xB5CABFC3L},{0xE0566A92L},{0xB5CABFC3L},{(-8L)},{2L}},{{0xB1658084L},{0L},{0xEC752F67L},{0x2A2B3B7BL},{0xEC752F67L}},{{0L},{0xB1658084L},{2L},{(-8L)},{0xB5CABFC3L}},{{0xE0566A92L},{0xB5CABFC3L},{(-8L)},{2L},{0xB1658084L}},{{0L},{0xEC752F67L},{0x2A2B3B7BL},{0xEC752F67L},{0L}},{{0xB1658084L},{2L},{(-8L)},{0xB5CABFC3L},{0xE0566A92L}},{{0xB5CABFC3L},{(-8L)},{2L},{0xB1658084L},{0L}}};
static int32_t g_8 = (-1L);
static int8_t g_61 = 0L;
static uint8_t g_78 = 252UL;
static volatile uint8_t * volatile g_85[4][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
static int8_t *g_114 = &g_61;
static int32_t g_116 = 6L;
static volatile int32_t g_118 = (-1L);/* VOLATILE GLOBAL g_118 */
static float g_145 = 0xF.98BF10p+19;
static int64_t g_146[8] = {0xB2B8B245C393FB47LL,(-6L),(-6L),0xB2B8B245C393FB47LL,(-6L),(-6L),0xB2B8B245C393FB47LL,(-6L)};
static int16_t g_148 = 0x7A2EL;
static int32_t *g_191[3][2][4] = {{{&g_7[7][3][0],&g_7[7][3][0],&g_7[7][3][0],&g_7[7][3][0]},{&g_7[7][3][0],&g_116,&g_116,&g_7[7][3][0]}},{{&g_116,&g_7[7][3][0],&g_116,&g_116},{&g_7[7][3][0],&g_7[7][3][0],&g_7[7][3][0],&g_7[7][3][0]}},{{&g_7[7][3][0],&g_116,&g_116,&g_7[7][3][0]},{&g_116,&g_7[7][3][0],&g_116,&g_116}}};
static union U0 g_192 = {0x389EL};/* VOLATILE GLOBAL g_192 */
static uint64_t g_206 = 0xD24EA24D4EC0B727LL;
static int8_t **g_249 = &g_114;
static int8_t ***g_248 = &g_249;
static int8_t ****g_247[8][1][7] = {{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248,&g_248,&g_248}}};
static int8_t ***** volatile g_250[9] = {&g_247[1][0][2],&g_247[2][0][1],&g_247[1][0][2],&g_247[1][0][2],&g_247[2][0][1],&g_247[1][0][2],&g_247[1][0][2],&g_247[2][0][1],&g_247[1][0][2]};
static int8_t ***** volatile g_251 = (void*)0;/* VOLATILE GLOBAL g_251 */
static int8_t ***** volatile g_252 = &g_247[1][0][2];/* VOLATILE GLOBAL g_252 */
static uint32_t g_284 = 9UL;
static uint16_t g_291[10][4] = {{65535UL,65527UL,65534UL,65527UL},{0x921BL,0x79CFL,0x9F60L,65534UL},{65527UL,0x79CFL,0x79CFL,65527UL},{0x79CFL,65527UL,0x921BL,65535UL},{0x79CFL,0x921BL,0x79CFL,0x9F60L},{65527UL,65535UL,0x9F60L,0x9F60L},{0x921BL,0x921BL,65534UL,65535UL},{65535UL,65527UL,65534UL,65527UL},{0x921BL,0x79CFL,0x9F60L,65534UL},{65527UL,0x79CFL,0x79CFL,65527UL}};
static uint16_t * volatile g_290 = &g_291[4][3];/* VOLATILE GLOBAL g_290 */
static uint16_t * volatile * volatile g_289 = &g_290;/* VOLATILE GLOBAL g_289 */
static uint64_t g_317 = 18446744073709551614UL;
static uint16_t * const g_371 = (void*)0;
static uint16_t * const *g_370 = &g_371;
static uint16_t g_375[4] = {0x599EL,0x599EL,0x599EL,0x599EL};
static uint16_t g_378 = 6UL;
static volatile union U0 g_419 = {-10L};/* VOLATILE GLOBAL g_419 */
static int8_t g_454 = (-5L);
static float g_456 = (-0x5.Dp-1);
static float * volatile g_455[6][7] = {{(void*)0,&g_456,(void*)0,&g_456,&g_456,(void*)0,&g_456},{&g_456,(void*)0,&g_456,&g_456,(void*)0,&g_456,&g_456},{&g_456,(void*)0,&g_456,(void*)0,&g_456,&g_456,(void*)0},{(void*)0,&g_456,(void*)0,&g_456,(void*)0,&g_456,(void*)0},{(void*)0,(void*)0,&g_456,&g_456,&g_456,&g_456,(void*)0},{(void*)0,&g_456,&g_456,&g_456,&g_456,&g_456,&g_456}};
static volatile union U0 g_464 = {1L};/* VOLATILE GLOBAL g_464 */
static float * volatile g_474 = &g_456;/* VOLATILE GLOBAL g_474 */
static union U0 g_475 = {2L};/* VOLATILE GLOBAL g_475 */
static int32_t ** volatile g_629 = (void*)0;/* VOLATILE GLOBAL g_629 */
static volatile int64_t * volatile *g_630 = (void*)0;
static uint16_t g_648 = 0xCB94L;
static uint32_t *g_655[7][9][3] = {{{&g_284,&g_284,&g_284},{&g_284,(void*)0,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,(void*)0},{&g_284,(void*)0,(void*)0},{&g_284,&g_284,&g_284}},{{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284}},{{(void*)0,&g_284,&g_284},{&g_284,&g_284,(void*)0},{&g_284,&g_284,(void*)0},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284}},{{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,(void*)0,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284}},{{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,(void*)0,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,(void*)0},{&g_284,(void*)0,(void*)0}},{{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284}},{{&g_284,&g_284,&g_284},{(void*)0,&g_284,&g_284},{&g_284,&g_284,(void*)0},{&g_284,&g_284,(void*)0},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284},{&g_284,&g_284,&g_284}}};
static uint32_t **g_654[3][3] = {{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0]},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0]},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0]}};
static uint32_t *** volatile g_653 = &g_654[2][0];/* VOLATILE GLOBAL g_653 */
static uint32_t g_696 = 0x11EE221BL;
static int32_t ** volatile g_698 = &g_191[1][0][1];/* VOLATILE GLOBAL g_698 */
static int32_t ** volatile g_708 = &g_191[1][0][1];/* VOLATILE GLOBAL g_708 */
static float * volatile g_733[5][9][1] = {{{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456}},{{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456}},{{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456}},{{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456}},{{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456},{&g_456}}};
static int32_t ** volatile g_739 = &g_191[0][1][0];/* VOLATILE GLOBAL g_739 */
static volatile uint32_t g_759 = 0x4D9F6AEFL;/* VOLATILE GLOBAL g_759 */
static int8_t *** const *g_816 = (void*)0;
static int8_t *** const **g_815 = &g_816;
static uint64_t g_890[10] = {18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL};
static int32_t ** volatile g_894[3] = {&g_191[1][0][1],&g_191[1][0][1],&g_191[1][0][1]};
static uint64_t g_974 = 1UL;
static volatile int16_t g_978 = 0xD824L;/* VOLATILE GLOBAL g_978 */
static volatile int16_t *g_977 = &g_978;
static volatile int16_t * const  volatile *g_976 = &g_977;
static float * volatile g_1008[5][10][5] = {{{&g_456,&g_145,&g_145,&g_456,&g_145},{(void*)0,(void*)0,&g_145,&g_145,&g_456},{&g_456,&g_145,&g_456,(void*)0,(void*)0},{&g_456,&g_145,&g_145,&g_456,&g_145},{&g_145,&g_145,&g_145,(void*)0,(void*)0},{&g_145,(void*)0,&g_145,&g_145,&g_456},{(void*)0,&g_145,&g_145,&g_456,&g_145},{(void*)0,&g_456,&g_145,&g_145,&g_456},{&g_145,&g_456,&g_456,&g_456,&g_456},{&g_145,&g_456,&g_145,&g_145,&g_456}},{{&g_456,(void*)0,&g_145,(void*)0,&g_456},{&g_456,&g_145,&g_456,&g_456,&g_456},{&g_145,&g_456,&g_145,(void*)0,&g_145},{&g_456,&g_456,&g_456,&g_145,&g_456},{&g_456,(void*)0,(void*)0,&g_456,(void*)0},{&g_145,&g_456,&g_145,&g_145,&g_145},{&g_145,&g_456,&g_456,&g_145,(void*)0},{(void*)0,&g_145,&g_145,(void*)0,&g_456},{(void*)0,(void*)0,(void*)0,&g_145,&g_145},{&g_145,&g_456,&g_456,(void*)0,(void*)0}},{{&g_145,&g_456,&g_145,&g_145,&g_456},{&g_456,&g_456,&g_456,&g_145,(void*)0},{&g_456,&g_145,&g_145,&g_456,&g_145},{(void*)0,(void*)0,&g_145,&g_145,&g_456},{&g_456,&g_145,&g_456,&g_456,&g_456},{&g_145,(void*)0,&g_456,&g_456,(void*)0},{&g_456,&g_145,(void*)0,&g_145,&g_456},{&g_456,&g_456,&g_456,(void*)0,(void*)0},{&g_145,(void*)0,(void*)0,&g_145,&g_145},{(void*)0,&g_145,&g_456,&g_145,&g_456}},{{&g_456,&g_145,(void*)0,&g_145,&g_145},{&g_145,&g_145,(void*)0,(void*)0,&g_145},{&g_456,&g_456,&g_145,&g_145,&g_145},{(void*)0,&g_456,&g_456,&g_456,&g_456},{(void*)0,&g_145,&g_456,&g_456,&g_145},{(void*)0,(void*)0,&g_456,&g_456,(void*)0},{&g_456,(void*)0,&g_456,(void*)0,&g_456},{&g_145,(void*)0,&g_145,&g_145,(void*)0},{&g_456,&g_145,&g_145,&g_456,&g_456},{(void*)0,&g_456,&g_145,&g_456,(void*)0}},{{&g_145,&g_456,&g_456,(void*)0,&g_145},{&g_456,&g_145,&g_456,&g_456,&g_456},{&g_456,&g_145,&g_456,&g_456,&g_145},{&g_145,&g_145,&g_456,&g_145,&g_456},{&g_456,(void*)0,&g_145,(void*)0,&g_145},{&g_456,&g_456,(void*)0,&g_456,(void*)0},{&g_456,&g_145,(void*)0,&g_456,&g_456},{&g_145,(void*)0,&g_456,&g_456,(void*)0},{&g_456,&g_145,(void*)0,&g_145,&g_456},{&g_456,&g_456,&g_456,(void*)0,(void*)0}}};
static uint16_t g_1043 = 0x9CD8L;
static float * volatile g_1048 = &g_145;/* VOLATILE GLOBAL g_1048 */
static int16_t g_1081 = 0L;
static uint8_t *g_1094[8][6] = {{(void*)0,(void*)0,(void*)0,&g_192.f2,&g_192.f2,&g_192.f2},{(void*)0,(void*)0,(void*)0,(void*)0,&g_78,&g_192.f2},{(void*)0,&g_192.f2,(void*)0,(void*)0,&g_192.f2,(void*)0},{&g_78,&g_475.f2,&g_78,&g_78,&g_192.f2,(void*)0},{(void*)0,&g_192.f2,(void*)0,&g_78,&g_78,(void*)0},{(void*)0,(void*)0,(void*)0,&g_78,&g_192.f2,&g_78},{(void*)0,(void*)0,(void*)0,&g_78,(void*)0,(void*)0},{&g_78,(void*)0,(void*)0,(void*)0,(void*)0,&g_78}};
static uint8_t **g_1093 = &g_1094[2][3];
static int64_t **g_1145 = (void*)0;
static int64_t ***g_1144 = &g_1145;
static int64_t g_1151[3] = {0xB7F90C5E4B351864LL,0xB7F90C5E4B351864LL,0xB7F90C5E4B351864LL};
static float * volatile g_1208 = &g_145;/* VOLATILE GLOBAL g_1208 */
static volatile uint16_t ** volatile ***g_1215 = (void*)0;
static volatile uint16_t *g_1220 = (void*)0;
static volatile uint16_t ** volatile g_1219 = &g_1220;/* VOLATILE GLOBAL g_1219 */
static volatile int32_t g_1295 = 0xC1F70106L;/* VOLATILE GLOBAL g_1295 */
static uint16_t **g_1313 = (void*)0;
static uint16_t ***g_1312 = &g_1313;
static uint16_t ****g_1311 = &g_1312;
static int8_t g_1330 = 0x1CL;
static uint32_t g_1332[7] = {0xA0C3FB4EL,0xA0C3FB4EL,0xA0C3FB4EL,0xA0C3FB4EL,0xA0C3FB4EL,0xA0C3FB4EL,0xA0C3FB4EL};
static uint32_t ***g_1487[8] = {&g_654[0][1],&g_654[0][1],&g_654[0][1],&g_654[0][1],&g_654[0][1],&g_654[0][1],&g_654[0][1],&g_654[0][1]};
static uint32_t **** const  volatile g_1486 = &g_1487[5];/* VOLATILE GLOBAL g_1486 */
static int32_t * volatile g_1595 = &g_7[7][3][0];/* VOLATILE GLOBAL g_1595 */
static volatile int32_t g_1663 = 1L;/* VOLATILE GLOBAL g_1663 */
static volatile int32_t g_1664[2] = {0xA9106481L,0xA9106481L};
static volatile int32_t *g_1662[8] = {&g_1663,&g_1664[0],&g_1663,&g_1663,&g_1664[0],&g_1663,&g_1663,&g_1664[0]};
static volatile int32_t **g_1661[8] = {&g_1662[1],(void*)0,&g_1662[1],(void*)0,&g_1662[1],(void*)0,&g_1662[1],(void*)0};
static volatile int32_t ** volatile *g_1660 = &g_1661[3];
static uint16_t *g_1691 = &g_375[0];
static uint16_t * volatile *g_1690[6] = {&g_1691,&g_1691,&g_1691,&g_1691,&g_1691,&g_1691};
static uint16_t * volatile * volatile *g_1689[9][6][4] = {{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,(void*)0,&g_1690[1],&g_1690[4]}},{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,(void*)0,&g_1690[1],&g_1690[4]}},{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,(void*)0,&g_1690[1],&g_1690[4]}},{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,(void*)0,&g_1690[1],&g_1690[4]}},{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,(void*)0,&g_1690[1],&g_1690[4]}},{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,(void*)0,&g_1690[1],&g_1690[4]}},{{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[4],&g_1690[1],(void*)0},{(void*)0,&g_1690[4],&g_1690[1],&g_1690[2]},{&g_1690[2],(void*)0,&g_1690[3],(void*)0},{&g_1690[2],&g_1690[2],&g_1690[1],&g_1690[4]},{(void*)0,&g_1690[4],&g_1690[3],&g_1690[5]}},{{(void*)0,&g_1690[2],&g_1690[3],&g_1690[2]},{(void*)0,&g_1690[5],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[2],&g_1690[3],&g_1690[1]},{(void*)0,&g_1690[4],&g_1690[3],&g_1690[4]},{(void*)0,&g_1690[1],&g_1690[3],&g_1690[2]},{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[5]}},{{(void*)0,&g_1690[2],&g_1690[3],&g_1690[2]},{(void*)0,&g_1690[5],&g_1690[3],&g_1690[4]},{&g_1690[2],&g_1690[2],&g_1690[3],&g_1690[1]},{(void*)0,&g_1690[4],&g_1690[3],&g_1690[4]},{(void*)0,&g_1690[1],&g_1690[3],&g_1690[2]},{&g_1690[2],&g_1690[4],&g_1690[3],&g_1690[5]}}};
static uint16_t * volatile * volatile * const *g_1688 = &g_1689[0][0][0];
static uint8_t g_1712 = 0xCEL;
static int32_t ** volatile g_1714 = &g_191[0][1][3];/* VOLATILE GLOBAL g_1714 */
static union U0 g_1717 = {-1L};/* VOLATILE GLOBAL g_1717 */
static volatile int64_t g_1723[6] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
static volatile int64_t *g_1722 = &g_1723[5];
static volatile int64_t **g_1721 = &g_1722;
static volatile int64_t ** volatile * volatile g_1720 = &g_1721;/* VOLATILE GLOBAL g_1720 */
static volatile int64_t ** volatile * volatile *g_1719 = &g_1720;
static volatile int64_t ** volatile * volatile * volatile * volatile g_1718 = &g_1719;/* VOLATILE GLOBAL g_1718 */
static int32_t ** volatile g_1824[1][3] = {{(void*)0,(void*)0,(void*)0}};
static int32_t ** volatile g_1825 = &g_191[2][1][1];/* VOLATILE GLOBAL g_1825 */
static int64_t g_1858 = 0xD40A957FF7B7D926LL;
static union U0 g_1865 = {0x7FACL};/* VOLATILE GLOBAL g_1865 */


/* --- FORWARD DECLARATIONS --- */
static union U0  func_1(void);
static int32_t * func_2(uint16_t  p_3);
static uint16_t  func_4(int32_t * const  p_5);
static int8_t  func_14(int32_t * p_15, int32_t * p_16, const int32_t * p_17, uint32_t  p_18, uint32_t  p_19);
static union U0  func_20(int32_t * p_21, int32_t * p_22, int32_t * p_23, float  p_24);
static int32_t * func_25(int32_t * p_26, int64_t  p_27, int32_t  p_28, uint16_t  p_29, const uint16_t  p_30);
static int32_t * func_31(uint64_t  p_32);
static uint32_t  func_37(int32_t * p_38);
static int32_t * func_39(float  p_40, int32_t  p_41);
static float  func_46(int32_t * const  p_47, uint16_t  p_48, int32_t * p_49);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_1865
 * writes:
 */
static union U0  func_1(void)
{ /* block id: 0 */
    int32_t * const l_6 = &g_7[7][3][0];
    uint32_t **l_1756 = (void*)0;
    uint64_t l_1757 = 0x634546261EA6A070LL;
    uint32_t l_1758 = 1UL;
    uint32_t l_1760[4][1][8] = {{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}}};
    int32_t l_1810 = 0L;
    float l_1828 = (-0x9.5p+1);
    float *l_1859[2][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0}};
    int32_t *l_1864 = &l_1810;
    int i, j, k;
    return g_1865;
}


/* ------------------------------------------ */
/* 
 * reads : g_192.f1 g_890 g_1208 g_1215 g_974 g_61 g_454 g_7 g_894 g_248 g_249 g_114 g_1295 g_192.f2 g_698 g_191 g_1311 g_290 g_291 g_1312 g_1313 g_976 g_977 g_978 g_1332 g_317 g_192 g_419 g_8 g_146 g_475.f1 g_1486 g_1043 g_1151 g_1048 g_1595 g_739 g_1081 g_475.f2 g_474 g_284 g_1660 g_696 g_148 g_456 g_78 g_289 g_1688 g_648 g_1712
 * writes: g_192.f1 g_456 g_145 g_61 g_974 g_454 g_475.f1 g_7 g_148 g_1081 g_475.f2 g_284 g_146 g_1043 g_291 g_1311 g_1313 g_696 g_1332 g_317 g_191 g_8 g_759 g_655 g_247 g_1330 g_1487 g_1151
 */
static int32_t * func_2(uint16_t  p_3)
{ /* block id: 425 */
    uint16_t *l_1206[6] = {&g_291[4][3],&g_291[4][3],&g_291[0][3],&g_291[4][3],&g_291[4][3],&g_291[0][3]};
    uint16_t **l_1205 = &l_1206[2];
    uint32_t **l_1212 = &g_655[3][8][0];
    int32_t l_1214 = 4L;
    uint32_t l_1249 = 8UL;
    int32_t l_1266 = 0x3E54AB8CL;
    int64_t l_1327 = 0x72683DB7DDAC55DDLL;
    float *l_1345[7] = {&g_145,&g_145,&g_145,&g_145,&g_145,&g_145,&g_145};
    int32_t l_1380 = 0xE5BD4BE5L;
    const int8_t ***l_1382 = (void*)0;
    uint64_t l_1384[4];
    int32_t * const *l_1387 = &g_191[1][0][1];
    int32_t l_1429 = 0x2498D54AL;
    int32_t l_1430 = 1L;
    int32_t l_1435 = 0x33F1E04BL;
    int32_t l_1436 = (-2L);
    int32_t l_1437 = 0x23E09BDAL;
    int32_t l_1438 = 1L;
    int32_t l_1439 = (-1L);
    int32_t l_1440 = 0xEBD153CCL;
    int8_t ***l_1445 = (void*)0;
    int32_t l_1469 = (-1L);
    int32_t l_1471 = 1L;
    int32_t l_1472 = 5L;
    uint64_t *l_1494 = (void*)0;
    uint64_t *l_1495 = &g_317;
    uint8_t **l_1498 = &g_1094[0][4];
    int64_t *l_1499 = &g_1151[2];
    const float l_1504 = 0x2.9322B0p-42;
    const uint8_t l_1505 = 0x25L;
    const int32_t l_1506 = 0x5138AB18L;
    int64_t *l_1507 = &l_1327;
    float l_1511 = 0x2.97449Dp+17;
    uint32_t l_1547[10] = {1UL,1UL,1UL,0x74E52E73L,1UL,1UL,1UL,1UL,0x74E52E73L,1UL};
    int32_t *l_1604 = &g_7[7][3][0];
    int i;
    for (i = 0; i < 4; i++)
        l_1384[i] = 18446744073709551615UL;
lbl_1480:
    for (g_192.f1 = 0; (g_192.f1 <= 9); g_192.f1 += 1)
    { /* block id: 428 */
        float *l_1207 = &g_456;
        int32_t *l_1213[2][4][5] = {{{&g_116,&g_116,&g_116,&g_116,&g_116},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_116,&g_116,&g_116,&g_116,&g_116},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_116,&g_116,&g_116,&g_116,&g_116},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_116,&g_116,&g_116,&g_116,&g_116},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
        int i, j, k;
        (*g_1208) = (safe_div_func_float_f_f((l_1205 != (void*)0), ((*l_1207) = g_890[g_192.f1])));
        l_1214 = ((!((safe_lshift_func_int16_t_s_s(p_3, 5)) >= ((void*)0 == &g_371))) <= ((void*)0 == l_1212));
    }
    for (g_61 = 0; (g_61 >= 0); g_61 -= 1)
    { /* block id: 435 */
        int64_t l_1250 = 0L;
        int32_t **l_1252[6];
        uint64_t l_1262[4][2] = {{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}};
        uint16_t ***l_1346 = &l_1205;
        uint8_t *l_1379 = (void*)0;
        int64_t ***l_1381 = &g_1145;
        int8_t ***l_1383 = &g_249;
        int16_t l_1455 = (-1L);
        int i, j;
        for (i = 0; i < 6; i++)
            l_1252[i] = &g_191[1][0][1];
        for (g_974 = 0; (g_974 <= 2); g_974 += 1)
        { /* block id: 438 */
            volatile uint16_t ** volatile *l_1218 = &g_1219;
            volatile uint16_t ** volatile **l_1217[10][3][7] = {{{(void*)0,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218},{(void*)0,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218}},{{&l_1218,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,(void*)0,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218}},{{&l_1218,(void*)0,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,(void*)0},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218}},{{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218}},{{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218},{&l_1218,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218}},{{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{(void*)0,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218}},{{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218},{&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218,(void*)0},{&l_1218,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218}},{{(void*)0,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218}},{{&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,&l_1218,&l_1218,(void*)0,&l_1218,&l_1218,&l_1218}},{{&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218,&l_1218},{&l_1218,(void*)0,&l_1218,&l_1218,&l_1218,(void*)0,&l_1218},{&l_1218,(void*)0,(void*)0,&l_1218,&l_1218,&l_1218,&l_1218}}};
            volatile uint16_t ** volatile ***l_1216 = &l_1217[7][2][2];
            int32_t l_1223 = 0xB07DACC3L;
            int i, j, k;
            l_1216 = g_1215;
            for (g_454 = 0; (g_454 <= 0); g_454 += 1)
            { /* block id: 442 */
                int32_t l_1253 = 0x412240A3L;
                for (g_475.f1 = 3; (g_475.f1 >= 0); g_475.f1 -= 1)
                { /* block id: 445 */
                    int32_t * const * const l_1251 = &g_191[1][0][1];
                    int i, j, k;
                    g_7[(g_974 + 2)][(g_61 + 2)][g_454] = 1L;
                    l_1253 = (((l_1214 = g_7[(g_974 + 4)][(g_454 + 1)][g_61]) && ((safe_lshift_func_uint16_t_u_u(l_1223, 1)) , (safe_mul_func_int8_t_s_s((+(((safe_sub_func_uint32_t_u_u(((safe_div_func_int8_t_s_s(((safe_lshift_func_int16_t_s_u((safe_mod_func_uint32_t_u_u(((p_3 , g_894[g_454]) == ((+(safe_sub_func_uint64_t_u_u(1UL, (safe_div_func_int32_t_s_s((safe_mod_func_int32_t_s_s(((safe_add_func_uint8_t_u_u(p_3, (safe_mod_func_uint16_t_u_u((18446744073709551610UL < (safe_add_func_int64_t_s_s((!4294967294UL), 0UL))), p_3)))) , l_1249), l_1250)), l_1223))))) , &g_191[1][0][1])), p_3)), g_7[(g_974 + 2)][(g_61 + 2)][g_454])) && p_3), 1L)) & (***g_248)), p_3)) , l_1251) == l_1252[4])), (**g_249))))) != (-3L));
                }
            }
        }
        l_1214 |= (safe_add_func_int64_t_s_s(((0UL || 9UL) | (safe_unary_minus_func_int64_t_s((safe_sub_func_int32_t_s_s((0x35A895DAL | 0xAB83C407L), (p_3 <= 0xD215B004L)))))), 0x9A5E9AD6ECAFDA1CLL));
        for (l_1214 = 0; (l_1214 <= 5); l_1214 += 1)
        { /* block id: 455 */
            const int32_t l_1263 = 1L;
            int16_t *l_1264 = &g_148;
            int16_t *l_1265 = &g_1081;
            int32_t l_1279 = 1L;
            float l_1291 = (-0x1.2p+1);
            const uint16_t *l_1321 = (void*)0;
            const uint16_t **l_1320 = &l_1321;
            const uint16_t ***l_1319 = &l_1320;
            const uint16_t ****l_1318[8];
            int32_t l_1328 = 0xF11FC6B5L;
            int8_t **l_1366 = &g_114;
            int i, j;
            for (i = 0; i < 8; i++)
                l_1318[i] = &l_1319;
            if ((safe_mul_func_uint16_t_u_u((p_3 & ((+(l_1262[3][1] >= 0x2AL)) != l_1263)), ((*l_1265) = ((*l_1264) = 0x7FAFL)))))
            { /* block id: 458 */
                uint16_t *l_1290 = &g_291[4][3];
                int32_t l_1292 = 0x8A32E6FAL;
                int64_t *l_1309 = (void*)0;
                int64_t *l_1310[7];
                uint16_t *****l_1314 = &g_1311;
                const uint16_t **l_1317 = (void*)0;
                const uint16_t ***l_1316 = &l_1317;
                const uint16_t ****l_1315 = &l_1316;
                int i;
                for (i = 0; i < 7; i++)
                    l_1310[i] = &g_146[7];
                l_1266 ^= p_3;
                for (g_1081 = 2; (g_1081 >= 0); g_1081 -= 1)
                { /* block id: 462 */
                    int32_t l_1283 = 0xB621B1EBL;
                    for (g_475.f2 = 0; (g_475.f2 <= 3); g_475.f2 += 1)
                    { /* block id: 465 */
                        uint32_t *l_1280 = &g_284;
                        int8_t *l_1287 = &g_454;
                        uint8_t l_1293 = 0x07L;
                        int64_t *l_1294 = &g_146[6];
                        int i, j, k;
                        l_1283 = (safe_mod_func_int16_t_s_s((p_3 , (safe_sub_func_uint64_t_u_u(0xB4C1C55A4356F194LL, (safe_lshift_func_uint16_t_u_s(((**l_1205) = (safe_div_func_int32_t_s_s((((*l_1294) = ((safe_rshift_func_int16_t_s_s((((safe_add_func_uint32_t_u_u(((*l_1280) = (l_1279 = l_1263)), (safe_add_func_int64_t_s_s(l_1283, (((((*l_1264) = (safe_unary_minus_func_int64_t_s(((safe_add_func_int8_t_s_s((-1L), ((*l_1287) = 0x40L))) < (safe_lshift_func_uint16_t_u_u(65535UL, 6)))))) , (l_1290 = l_1290)) == &p_3) , l_1214))))) || l_1292) >= 1L), l_1292)) >= l_1293)) > g_890[2]), g_1295))), g_192.f2))))), g_974));
                        return (*g_698);
                    }
                }
                l_1328 ^= (safe_div_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_u((((((+(safe_mod_func_int32_t_s_s((p_3 ^ l_1292), (safe_div_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((safe_add_func_int64_t_s_s((l_1279 = p_3), ((((((l_1292 | (((((*l_1314) = g_1311) == (l_1318[3] = l_1315)) || (safe_div_func_int32_t_s_s((((l_1214 , (safe_sub_func_int32_t_s_s(p_3, ((((+0x95L) , (*g_290)) < p_3) >= l_1292)))) || (-1L)) & p_3), p_3))) < p_3)) != p_3) >= 0xC9725FF60CDE1AB2LL) >= p_3) ^ 8UL) , 0x563496CA3A4AF4A9LL))), g_61)), l_1327))))) , (*l_1315)) != &l_1205) != 0x1003L) ^ l_1263), 2)) , p_3), p_3));
                (***l_1314) = (*g_1312);
            }
            else
            { /* block id: 482 */
                float l_1340 = 0x3.6EA5F8p+28;
                int32_t l_1341 = 0xACC9A2F3L;
                int32_t l_1348 = (-1L);
                for (g_475.f2 = 0; (g_475.f2 <= 3); g_475.f2 += 1)
                { /* block id: 485 */
                    float *l_1344 = &l_1340;
                    int32_t l_1347[6][6] = {{0xA9776B92L,0L,(-6L),(-6L),(-6L),0xEF9876C3L},{0L,0xEF9876C3L,0x800298C4L,(-6L),0x800298C4L,0xEF9876C3L},{0x800298C4L,0L,0xBAE48872L,0xD3643B62L,0xD3643B62L,0xBAE48872L},{0x800298C4L,0x800298C4L,0xD3643B62L,(-6L),0xA9776B92L,(-6L)},{0L,0x800298C4L,0L,0xBAE48872L,0xD3643B62L,0xD3643B62L},{0xEF9876C3L,0L,0L,0xEF9876C3L,0x800298C4L,(-6L)}};
                    int64_t ****l_1371[10] = {&g_1144,&g_1144,&g_1144,&g_1144,&g_1144,&g_1144,&g_1144,&g_1144,&g_1144,&g_1144};
                    int64_t *****l_1370 = &l_1371[9];
                    int32_t l_1372 = 0L;
                    int32_t l_1373 = (-1L);
                    int32_t **l_1374 = &g_191[1][0][2];
                    uint64_t *l_1375[1][8][3];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 8; j++)
                        {
                            for (k = 0; k < 3; k++)
                                l_1375[i][j][k] = &l_1262[3][1];
                        }
                    }
                    for (g_696 = 0; (g_696 <= 5); g_696 += 1)
                    { /* block id: 488 */
                        uint32_t *l_1329[2][4][5] = {{{&l_1249,&g_696,&g_696,&g_696,&l_1249},{&g_696,&g_696,&g_696,&g_696,&g_696},{(void*)0,&l_1249,&l_1249,(void*)0,&g_696},{&g_696,&g_696,&l_1249,&g_696,&g_696}},{{&l_1249,&g_696,&g_696,&g_696,&g_696},{&g_696,&g_696,&g_696,&g_696,&l_1249},{&g_696,&l_1249,&g_696,(void*)0,&l_1249},{&g_696,&g_696,&g_696,&g_696,&g_696}}};
                        int32_t l_1331 = 9L;
                        int32_t l_1349 = (-5L);
                        int i, j, k;
                        l_1347[0][0] = (l_1349 |= ((**g_976) , ((--g_1332[1]) , (safe_div_func_int64_t_s_s((safe_mul_func_int64_t_s_s((((0x6BL >= ((((!(l_1249 > (-1L))) && 0xD4F59A4FL) , p_3) < ((&l_1205 != ((l_1341 |= l_1327) , ((((safe_rshift_func_int8_t_s_s(((((((((l_1344 == l_1345[6]) , &p_3) == &p_3) || p_3) ^ p_3) , p_3) & l_1279) != 0xA9L), l_1331)) && p_3) && p_3) , l_1346))) ^ l_1347[0][0]))) > l_1331) <= l_1348), 0UL)), l_1347[0][0])))));
                    }
                    (*l_1374) = func_31((g_317 ^= (((((((safe_add_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((((safe_div_func_int32_t_s_s((l_1328 &= (((safe_mod_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u((l_1372 = (safe_add_func_int32_t_s_s((l_1348 && ((p_3 && (safe_sub_func_int32_t_s_s((l_1366 == l_1366), 1UL))) & ((l_1347[0][0] &= p_3) | (safe_rshift_func_uint8_t_u_s((+(l_1279 || (&g_1144 != ((*l_1370) = &g_1144)))), 1))))), l_1372))), 8)), p_3)) & p_3) < (**g_249))), (-9L))) & (-2L)) , l_1347[0][0]) | 0UL), l_1341)), p_3)) || p_3) == l_1373) ^ 0x3BA7639CL) , l_1374) == (void*)0) < 0xCFCA2CB3L)));
                }
                if (p_3)
                    continue;
            }
            if (l_1249)
                break;
            for (l_1327 = 5; (l_1327 >= 0); l_1327 -= 1)
            { /* block id: 506 */
                if (p_3)
                    break;
            }
        }
        l_1384[2] = (((safe_rshift_func_uint8_t_u_u((safe_unary_minus_func_uint32_t_u(0xDF663B28L)), 2)) , (l_1380 = l_1327)) < (((((((g_192 , l_1379) != l_1379) < p_3) , &g_630) != (p_3 , l_1381)) , l_1382) == l_1383));
        for (g_317 = 0; (g_317 <= 3); g_317 += 1)
        { /* block id: 514 */
            uint8_t **l_1393 = &l_1379;
            int32_t l_1394 = 0x4966042AL;
            int8_t * const ***l_1401 = (void*)0;
            int32_t l_1428[2][4][9] = {{{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L},{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L},{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L},{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L}},{{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L},{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L},{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L},{0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L,0x1BBFA35EL,1L,1L}}};
            int8_t ***l_1444 = (void*)0;
            float l_1448 = 0x0.Ep+1;
            int32_t l_1458[2][3][9] = {{{1L,1L,0L,0L,1L,1L,1L,0L,0L},{1L,1L,0L,1L,0L,1L,1L,0L,1L},{(-1L),1L,(-1L),1L,1L,(-1L),1L,(-1L),1L}},{{0xF0D5CDBEL,0L,0L,0xF0D5CDBEL,0L,0xF0D5CDBEL,0L,0L,0xF0D5CDBEL},{0x9523DEF2L,(-1L),1L,(-1L),1L,1L,(-1L),1L,(-1L)},{0xF0D5CDBEL,1L,0L,0L,1L,0xF0D5CDBEL,1L,0L,0L}}};
            int16_t l_1460 = 0x3F6EL;
            int64_t l_1462 = (-4L);
            int64_t l_1481 = 0x53807DD6ADCD0D23LL;
            int32_t l_1482 = (-3L);
            uint8_t l_1483 = 0xBEL;
            int i, j, k;
            for (g_8 = 0; (g_8 <= 3); g_8 += 1)
            { /* block id: 517 */
                int32_t * const **l_1388 = &l_1387;
                int64_t ****l_1398 = &g_1144;
                int64_t *****l_1397 = &l_1398;
                int32_t l_1425 = 0xC726A4AEL;
                int32_t l_1427 = 1L;
                int32_t l_1431 = 0L;
                int32_t l_1432 = 0x0627B580L;
                int32_t l_1434[10] = {1L,1L,4L,0x3273D5C5L,4L,1L,1L,4L,0x3273D5C5L,4L};
                uint16_t l_1441 = 0xDBE2L;
                int32_t l_1447 = 1L;
                uint64_t l_1477[8][10] = {{0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL},{18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL},{18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL},{0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL},{18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL},{18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL},{0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL,18446744073709551615UL,0xD26E03A69920E2F7LL,0xD26E03A69920E2F7LL},{18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL,18446744073709551615UL,0x6B46099592A08273LL,18446744073709551615UL}};
                int i, j;
                for (g_759 = 0; g_759 < 7; g_759 += 1)
                {
                    for (l_1250 = 0; l_1250 < 9; l_1250 += 1)
                    {
                        for (g_148 = 0; g_148 < 3; g_148 += 1)
                        {
                            g_655[g_759][l_1250][g_148] = &g_284;
                        }
                    }
                }
                l_1394 = (safe_div_func_uint32_t_u_u((((*l_1388) = l_1387) == (void*)0), (safe_mul_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((g_419 , ((**l_1205) = 65535UL)), p_3)), ((p_3 == (((((&l_1379 == (l_1393 = &l_1379)) == 0x127D2DE2L) ^ 7L) && l_1394) ^ 65535UL)) , g_61)))));
                for (l_1380 = 3; (l_1380 >= 0); l_1380 -= 1)
                { /* block id: 525 */
                    uint32_t l_1408 = 0xCC7F0FD5L;
                    int64_t ****l_1421[1][1][5] = {{{&g_1144,&g_1144,&g_1144,&g_1144,&g_1144}}};
                    uint64_t l_1422[6] = {0UL,0UL,0UL,0UL,0UL,0UL};
                    int32_t l_1423 = 0x68E2B964L;
                    int32_t l_1424[5][1];
                    int8_t ***l_1446 = &g_249;
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_1424[i][j] = (-1L);
                    }
                    for (g_284 = 0; (g_284 <= 2); g_284 += 1)
                    { /* block id: 528 */
                        uint64_t *l_1406 = (void*)0;
                        uint64_t *l_1407 = &l_1262[3][0];
                        int i, j, k;
                        l_1394 = (safe_sub_func_float_f_f((l_1397 == (void*)0), ((((((g_247[(g_317 + 2)][g_61][(g_317 + 1)] = &l_1383) != l_1401) <= 7L) != ((safe_mod_func_int64_t_s_s((safe_lshift_func_uint16_t_u_s((((*l_1407) = ((**g_248) != (void*)0)) | (p_3 | (0x34EE728EB53E533BLL || 0x9DAAA5FC11A3D6B3LL))), l_1408)), 8UL)) , 0UL)) && 1UL) , l_1408)));
                    }
                    l_1423 = (l_1422[2] |= (l_1394 || (safe_mod_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s(0xB4770B6FBBCA60F5LL, (safe_add_func_uint64_t_u_u(((p_3 | (**g_976)) , 4UL), ((p_3 & ((safe_sub_func_int64_t_s_s((g_146[(g_8 + 2)] ^= ((safe_lshift_func_uint8_t_u_u(0UL, (((safe_rshift_func_int8_t_s_s((p_3 , (((((void*)0 != l_1421[0][0][4]) , p_3) != l_1408) != l_1408)), 3)) ^ l_1394) & 255UL))) , p_3)), g_475.f1)) <= p_3)) , g_8))))) , p_3), p_3))));
                    for (g_148 = 3; (g_148 >= 0); g_148 -= 1)
                    { /* block id: 538 */
                        int16_t l_1426[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
                        int32_t l_1433[1][4][1] = {{{0x08901188L},{0x48D3CDEDL},{0x08901188L},{0x48D3CDEDL}}};
                        int i, j, k;
                        --l_1441;
                        l_1428[1][1][4] = (3L && (l_1444 == (l_1446 = l_1445)));
                        l_1428[1][1][3] = 0xF.734E81p+41;
                    }
                }
                for (g_1330 = 3; (g_1330 >= 0); g_1330 -= 1)
                { /* block id: 547 */
                    uint32_t l_1449[7] = {0x0A3F7A59L,0x0A3F7A59L,0x0A3F7A59L,0x0A3F7A59L,0x0A3F7A59L,0x0A3F7A59L,0x0A3F7A59L};
                    int32_t l_1452 = 0xFF7A3777L;
                    int32_t l_1453 = (-1L);
                    int32_t l_1454 = 0x7D4E1F70L;
                    int32_t l_1456 = 1L;
                    int32_t l_1457 = (-5L);
                    int32_t l_1459 = 0x99EA2C03L;
                    int32_t l_1461 = 0x1C176005L;
                    int32_t l_1463 = 0xEE8FB873L;
                    int32_t l_1464 = (-6L);
                    int32_t l_1465 = (-1L);
                    int32_t l_1466 = 2L;
                    int32_t l_1467 = 0xBAC9B3B4L;
                    int32_t l_1468 = 0xAA5511B6L;
                    int32_t l_1470 = 1L;
                    int32_t l_1473 = 0x3B6CAB06L;
                    int32_t l_1474 = 0L;
                    int32_t l_1475 = 0x34E09B8FL;
                    int32_t l_1476[5][2][6] = {{{0L,1L,(-10L),0x9EB94B71L,(-10L),1L},{0L,1L,(-10L),0x9EB94B71L,(-10L),1L}},{{0L,1L,(-10L),0x9EB94B71L,(-10L),1L},{0L,1L,(-10L),0x9EB94B71L,(-10L),1L}},{{0L,1L,(-10L),0x9EB94B71L,(-10L),1L},{0L,1L,(-10L),0x9EB94B71L,(-10L),1L}},{{0L,1L,(-10L),0x9EB94B71L,(-10L),1L},{0L,1L,(-10L),0x9EB94B71L,(-10L),1L}},{{0L,1L,(-10L),0x9EB94B71L,(-10L),0x2E68610AL},{(-10L),0x2E68610AL,0x7C07BCB0L,1L,0x7C07BCB0L,0x2E68610AL}}};
                    int i, j, k;
                    l_1449[5]--;
                    l_1477[6][4]--;
                    for (l_1457 = 0; (l_1457 <= 3); l_1457 += 1)
                    { /* block id: 552 */
                        int i;
                        if (g_454)
                            goto lbl_1480;
                        l_1471 |= p_3;
                    }
                }
            }
            l_1483++;
            (*g_1486) = &g_654[2][0];
            l_1482 = p_3;
        }
    }
    if ((safe_mul_func_int8_t_s_s((p_3 , p_3), (safe_mul_func_int16_t_s_s((((*l_1507) = ((((safe_rshift_func_uint16_t_u_u(0x966AL, (p_3 >= p_3))) || ((((((*l_1495)--) || (g_146[4] = ((*l_1499) = ((void*)0 != l_1498)))) , ((((safe_sub_func_int64_t_s_s((((-1L) <= (&l_1212 == &l_1212)) < (-1L)), p_3)) != p_3) & p_3) && l_1384[1])) & l_1505) , p_3)) && 0xD5A728A2L) > l_1506)) && l_1437), p_3)))))
    { /* block id: 567 */
        float l_1529 = (-0x2.6p+1);
        int32_t l_1531 = 0x8D9C6154L;
        int32_t l_1548 = 1L;
        uint32_t **l_1594[10][9] = {{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0},{&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],&g_655[0][1][0],(void*)0,(void*)0}};
        int32_t **l_1598[7][10] = {{&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0]},{&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0]},{&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0]},{&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0]},{&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0]},{&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0],&g_191[1][0][0],&g_191[0][0][0]},{&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0],&g_191[1][0][1],&g_191[0][0][0]}};
        int32_t ***l_1597 = &l_1598[4][5];
        int32_t ****l_1596 = &l_1597;
        int16_t l_1665[5][4] = {{0x6A7DL,0xB350L,0x6A7DL,0xB350L},{0x6A7DL,0xB350L,0x6A7DL,0xB350L},{0x6A7DL,0xB350L,0x6A7DL,0xB350L},{0x6A7DL,0xB350L,0x6A7DL,0xB350L},{0x6A7DL,0xB350L,0x6A7DL,0xB350L}};
        int i, j;
        for (g_1043 = 0; (g_1043 <= 5); g_1043 += 1)
        { /* block id: 570 */
            float l_1513 = 0x5.242917p+0;
            int32_t l_1528[5];
            int32_t l_1532 = (-1L);
            int32_t l_1564 = 0x4D28EE41L;
            uint8_t l_1645 = 253UL;
            int32_t *l_1666 = (void*)0;
            int i;
            for (i = 0; i < 5; i++)
                l_1528[i] = 0L;
            for (l_1437 = 0; (l_1437 >= 0); l_1437 -= 1)
            { /* block id: 573 */
                uint8_t l_1514[8][5] = {{0x71L,1UL,255UL,4UL,0x71L},{0x95L,0x15L,0x95L,0x6AL,0x3DL},{255UL,1UL,0x71L,1UL,255UL},{0x95L,0xC4L,0x15L,0x2CL,0x15L},{8UL,8UL,0x71L,255UL,0x78L},{0xC4L,0x95L,0x95L,0xC4L,0x15L},{1UL,255UL,4UL,4UL,255UL},{0x15L,0x95L,0x6AL,0x3DL,0x3DL}};
                int32_t l_1530[8];
                uint16_t ****l_1554 = &g_1312;
                int32_t ****l_1599 = &l_1597;
                float l_1624 = 0x9.CD3333p+87;
                uint32_t *l_1642 = &g_284;
                int8_t ****l_1646 = &g_248;
                uint32_t l_1647 = 4294967295UL;
                int i, j;
                for (i = 0; i < 8; i++)
                    l_1530[i] = 8L;
                (*g_1048) = ((!(((safe_add_func_float_f_f(l_1511, (p_3 <= p_3))) == (((p_3 > ((~l_1514[3][3]) != (safe_div_func_int64_t_s_s(((*l_1499) ^= (p_3 || (((~((safe_div_func_int16_t_s_s((((***g_248) = ((0xEFL || ((safe_rshift_func_int8_t_s_u(((0x3E0F4E05L >= (safe_add_func_uint16_t_u_u((l_1531 = (l_1530[5] = (safe_lshift_func_int8_t_s_s((safe_add_func_uint8_t_u_u(l_1528[4], (-1L))), p_3)))), 0x550CL))) > p_3), 5)) | l_1514[3][3])) ^ p_3)) | p_3), 0xE7B4L)) , (-5L))) , p_3) && p_3))), l_1514[7][0])))) ^ l_1532) , 0xE.A5663Ap-77)) >= p_3)) >= p_3);
                l_1528[1] = (l_1528[2] == (l_1531 = (p_3 & ((!(safe_add_func_int64_t_s_s(((safe_mod_func_int32_t_s_s((p_3 , 0xEEDF70F2L), p_3)) & 0x76E2L), (safe_div_func_uint32_t_u_u((safe_add_func_uint32_t_u_u(l_1531, (safe_lshift_func_int8_t_s_u(((***g_248) |= (~(l_1548 = (safe_mul_func_int8_t_s_s(l_1528[3], (l_1547[1] || (-8L))))))), p_3)))), 0xFA89D284L))))) <= 6UL))));
                if (p_3)
                { /* block id: 583 */
                    const int32_t l_1561 = 7L;
                    uint8_t *l_1562[10][5] = {{&g_192.f2,&g_192.f2,&g_78,&l_1514[3][3],&g_78},{(void*)0,(void*)0,&l_1514[3][3],&g_192.f2,&l_1514[3][3]},{&g_192.f2,&g_192.f2,&g_78,&l_1514[3][3],&g_78},{(void*)0,(void*)0,&l_1514[3][3],&g_192.f2,&l_1514[3][3]},{&g_192.f2,&g_192.f2,&g_78,&l_1514[3][3],&g_78},{(void*)0,(void*)0,&l_1514[3][3],&g_192.f2,&l_1514[3][3]},{&g_192.f2,&g_192.f2,&g_78,&l_1514[3][3],&g_78},{(void*)0,(void*)0,&l_1514[3][3],&g_192.f2,&l_1514[3][3]},{&g_192.f2,&g_192.f2,&g_78,&l_1514[3][3],&g_78},{(void*)0,(void*)0,&l_1514[3][3],&g_192.f2,&l_1514[3][3]}};
                    int32_t l_1563 = 0x9930EB70L;
                    int32_t l_1565 = (-5L);
                    uint32_t *l_1575 = &g_284;
                    int16_t *l_1592 = (void*)0;
                    int i, j;
                    l_1565 |= (((l_1563 &= (((!(-1L)) == ((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_s(1UL, ((&g_1312 != l_1554) ^ (((safe_rshift_func_uint16_t_u_s(((void*)0 != &g_1094[(g_1043 + 2)][g_1043]), (**g_976))) >= (safe_sub_func_int32_t_s_s((safe_rshift_func_int16_t_s_u(((p_3 || l_1530[7]) == 7UL), l_1530[1])), 0x3CCB94F7L))) , l_1530[5])))) > l_1561), 4)) & 0x6556E5B9L)) , p_3)) , 65533UL) , l_1564);
                    if ((safe_sub_func_int32_t_s_s(0x6399D261L, p_3)))
                    { /* block id: 586 */
                        float l_1586 = 0x1.0p-1;
                        int16_t *l_1591 = (void*)0;
                        int32_t l_1593[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1593[i] = 0x20434FEFL;
                        (*g_1595) = (((*l_1507) = (safe_unary_minus_func_int64_t_s(((*l_1499) &= p_3)))) & ((((*g_114) ^ ((((((((safe_lshift_func_uint16_t_u_u((((-9L) || (((l_1564 = (safe_mul_func_uint8_t_u_u((((void*)0 != l_1575) == ((safe_add_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_u(0x97F6L, (safe_lshift_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((*g_114), 0)), (0x878CEC30854B69B7LL <= ((safe_div_func_int8_t_s_s(((((safe_add_func_uint16_t_u_u(((l_1591 == l_1592) <= l_1528[4]), l_1561)) < 0L) , p_3) != p_3), p_3)) || 0x056B02C675333C19LL)))))), 3)) || 1UL), l_1563)) <= 0x65E565A2DD698C75LL)), l_1593[0]))) , p_3) | 0xC1BD8933CB15A9B8LL)) > 18446744073709551610UL), p_3)) , l_1594[8][6]) != (void*)0) != 0x78F1BB71L) >= 0x446FAE9FL) , l_1548) || 0x226F8FACL) == 0x67L)) > 0x86DF82A6BC5152D1LL) != 0xCF1AL));
                        l_1599 = l_1596;
                        (*g_739) = &l_1593[0];
                    }
                    else
                    { /* block id: 593 */
                        float l_1602 = 0x0.Cp-1;
                        l_1565 = (l_1532 <= (safe_lshift_func_int8_t_s_s(p_3, 7)));
                    }
                    for (p_3 = 0; (p_3 <= 0); p_3 += 1)
                    { /* block id: 598 */
                        int32_t *l_1603 = &l_1438;
                        return l_1604;
                    }
                }
                else
                { /* block id: 601 */
                    int8_t l_1607 = 0xC0L;
                    int32_t l_1635 = 0x67F38BFAL;
                    int16_t *l_1643 = &g_1081;
                    if ((safe_div_func_uint8_t_u_u(l_1607, (0xF45A9C0FL && (safe_add_func_uint16_t_u_u((((*l_1507) = (safe_mod_func_int16_t_s_s((l_1530[5] >= ((*l_1604) = ((safe_mul_func_uint16_t_u_u(((((safe_mod_func_uint8_t_u_u(5UL, (*l_1604))) ^ ((p_3 , 0xEFL) < 6L)) ^ (safe_lshift_func_int16_t_s_s((((*l_1205) = (*l_1205)) == (void*)0), 12))) > 0xEE227CCE76579B95LL), 1UL)) & p_3))), 0xDBEAL))) <= p_3), (**g_976)))))))
                    { /* block id: 605 */
                        uint8_t l_1620 = 0xFFL;
                        --l_1620;
                    }
                    else
                    { /* block id: 607 */
                        uint64_t l_1623 = 18446744073709551607UL;
                        if (l_1623)
                            break;
                        l_1624 = 0x1.EC39C7p+52;
                    }
                    (*g_474) = (-((safe_add_func_float_f_f(p_3, (((*g_1048) = ((l_1607 , l_1564) == p_3)) <= 0x1.7p-1))) <= ((((safe_lshift_func_int8_t_s_s(l_1528[4], 4)) , ((safe_add_func_float_f_f(p_3, (safe_mul_func_float_f_f(g_1081, (-g_1043))))) >= l_1635)) < g_475.f2) == l_1607)));
                    (*l_1604) = (((((safe_mod_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((**g_976), (g_148 = ((l_1642 != (void*)0) , ((0xF1L < (***g_248)) | ((*l_1643) |= g_192.f1)))))) <= p_3), (+p_3))) , l_1645), p_3)) >= 0UL) <= (*l_1604)) , l_1646) != l_1646);
                }
                for (l_1430 = 0; (l_1430 >= 0); l_1430 -= 1)
                { /* block id: 619 */
                    const int32_t **l_1659 = (void*)0;
                    const int32_t ***l_1658[2];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1658[i] = &l_1659;
                    g_7[(l_1430 + 4)][(l_1437 + 2)][l_1430] = l_1647;
                    l_1528[4] = ((((((safe_mul_func_uint8_t_u_u((((++(*l_1642)) , 0xB3E8B391L) && p_3), ((((safe_lshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s((-1L), (**g_976))), 0)) && (*g_290)) ^ (((safe_sub_func_uint32_t_u_u((((l_1530[4] &= ((*l_1604) = (((g_7[(l_1430 + 4)][(l_1437 + 2)][l_1430] & 0xB51D69EAA636F4ABLL) <= ((((*l_1495) = ((l_1658[0] == g_1660) & 2UL)) <= p_3) | l_1665[1][0])) , p_3))) , g_696) ^ g_148), 0xFB681E27L)) != 255UL) & p_3)) <= 0x024CB74EL))) , (*g_474)) < p_3) <= g_192.f2) != g_890[2]) == g_78);
                }
            }
            return l_1666;
        }
    }
    else
    { /* block id: 630 */
        uint8_t l_1669 = 8UL;
        const uint16_t l_1676 = 65533UL;
        int32_t *l_1678 = &l_1430;
        uint16_t ***l_1703 = &l_1205;
        if (((*l_1678) |= (safe_lshift_func_uint16_t_u_u(((l_1669 == (((safe_lshift_func_uint8_t_u_u(5UL, ((safe_sub_func_float_f_f(p_3, (((safe_sub_func_float_f_f(((0xD3CB0EE7E5C50533LL ^ (l_1676 , p_3)) , (*g_474)), p_3)) >= (((-g_146[6]) != g_1151[2]) > p_3)) <= p_3))) , l_1669))) && p_3) || p_3)) >= (*l_1604)), l_1676))))
        { /* block id: 632 */
            uint8_t l_1683 = 255UL;
            uint8_t *l_1692 = &l_1669;
            (*l_1678) ^= (safe_sub_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s((l_1683 = (**g_289)), ((*l_1604) = (0L | ((p_3 , p_3) , (p_3 ^ (((safe_lshift_func_uint8_t_u_u(((*l_1692) = (0xAAL < (g_284 , ((void*)0 != g_1688)))), p_3)) & 0UL) | p_3))))))), p_3));
            (*l_1604) |= (safe_mod_func_int16_t_s_s(p_3, (**g_976)));
        }
        else
        { /* block id: 638 */
            int8_t *l_1709 = &g_454;
            int32_t l_1710 = 0xF7001288L;
            int32_t l_1711 = 0x4C94E020L;
            int32_t *l_1713 = &g_8;
            (*l_1713) ^= (safe_sub_func_uint64_t_u_u(((((**g_976) ^ p_3) | (safe_mul_func_int8_t_s_s(((*l_1604) = ((*g_114) ^= 0x78L)), (((*l_1678) = ((*l_1678) || ((safe_div_func_int16_t_s_s((-1L), (safe_mod_func_int16_t_s_s((l_1703 == (((safe_add_func_float_f_f(((+(l_1711 |= (((safe_rshift_func_uint8_t_u_u((((((p_3 , ((((((*l_1709) ^= (((g_1151[1] >= ((((((*l_1678) , p_3) == 0xF.9F23B2p-76) == g_648) <= 0x2.9BD006p+7) <= (-0x6.7p-1))) < 0x0.6p+1) , 0x96L)) || (*l_1678)) , p_3) <= 0xF95B5D810162DDDDLL) > l_1710)) != 0UL) < p_3) , 8UL) & 0L), 4)) | 5L) , p_3))) , p_3), (-0x1.2p-1))) > p_3) , (void*)0)), g_1712)))) && 0xFA3E6596L))) & p_3)))) && p_3), p_3));
            (*l_1604) = 0x0.9p+1;
        }
    }
    return (*g_739);
}


/* ------------------------------------------ */
/* 
 * reads : g_8 g_475.f0 g_291 g_648 g_974 g_284 g_977 g_978 g_289 g_290 g_1151 g_7
 * writes: g_8 g_192.f2
 */
static uint16_t  func_4(int32_t * const  p_5)
{ /* block id: 1 */
    int8_t l_50 = (-1L);
    int32_t *l_63 = &g_7[7][3][0];
    int32_t *l_463 = (void*)0;
    int32_t *l_476 = &g_8;
    int32_t l_1082 = 0L;
    int32_t l_1104 = 0x1DDF128EL;
    int32_t l_1108 = (-6L);
    int32_t l_1110[7][9] = {{0x4289735CL,(-2L),0L,1L,0L,(-2L),0x4289735CL,0L,0x288127A7L},{(-6L),0L,(-1L),0x2B5BCE91L,3L,(-1L),0xF497D04FL,(-1L),3L},{0xA779B98BL,0L,0L,0xA779B98BL,0xBA5A57C5L,0xB2004069L,(-1L),0L,0xB2004069L},{0xD1019E8EL,3L,1L,0x2B5BCE91L,0x71BB9214L,0x71BB9214L,0x2B5BCE91L,1L,3L},{(-1L),0xBA5A57C5L,0x288127A7L,1L,0xBA5A57C5L,0L,0L,0x288127A7L,0x288127A7L},{0xD1019E8EL,0x71BB9214L,3L,0x66F1DB08L,3L,0x71BB9214L,0xD1019E8EL,3L,1L},{0xA779B98BL,0xBA5A57C5L,0xB2004069L,(-1L),0L,0xB2004069L,0L,0xB2004069L,0L}};
    uint64_t l_1133 = 0xBD2CA04570290572LL;
    int64_t ***l_1143 = (void*)0;
    int64_t l_1152 = 0xD393FE869FAD19D8LL;
    float l_1162 = (-0x1.1p-1);
    uint32_t l_1163 = 3UL;
    int32_t *l_1201[9] = {&l_1108,&l_1082,&l_1082,&l_1108,&l_1082,&l_1082,&l_1108,&l_1082,&l_1082};
    uint16_t l_1202 = 0x9342L;
    int i, j;
    for (g_8 = (-27); (g_8 > 16); g_8 = safe_add_func_uint8_t_u_u(g_8, 3))
    { /* block id: 4 */
        uint16_t l_13 = 0x657EL;
        int8_t l_44 = 1L;
        int32_t l_59[5][3][10] = {{{0xEB3E1984L,(-3L),0x169B2B29L,(-1L),0xE0EA626AL,0xE0EA626AL,(-1L),0x169B2B29L,(-3L),0xEB3E1984L},{0L,0xB2BD68E4L,0x0B0DEBF6L,9L,(-6L),(-5L),0xD3FD9053L,0x35309F33L,0xBB30732BL,0L},{0xB0C7B249L,0xE0EA626AL,0x48A24E22L,0xB2BD68E4L,(-6L),0x102D1F73L,0x41209775L,9L,(-6L),0xEB3E1984L}},{{(-6L),0xD3FD9053L,0xEB3E1984L,(-1L),(-3L),5L,9L,0xE0EA626AL,0x2F33BC6CL,0xE0EA626AL},{0x5CF455DBL,0xD3FD9053L,0xB2BD68E4L,(-6L),0xB2BD68E4L,0xD3FD9053L,0x5CF455DBL,5L,0L,0L},{(-1L),0L,0x48A24E22L,(-1L),0x0B0DEBF6L,0x35309F33L,0xB0C7B249L,(-1L),(-1L),5L}},{{(-6L),0L,(-1L),0x169B2B29L,(-4L),5L,0x5CF455DBL,0L,0xEB3E1984L,0x48A24E22L},{5L,0xD3FD9053L,(-1L),(-3L),1L,(-1L),9L,9L,(-1L),1L},{5L,(-6L),(-6L),5L,0L,(-1L),0x102D1F73L,0xCE01153AL,0x41209775L,0xB2BD68E4L}},{{0L,(-3L),0xB0C7B249L,0xE0EA626AL,0x48A24E22L,0xB2BD68E4L,(-6L),0x102D1F73L,0x41209775L,9L},{(-4L),5L,0L,5L,0xE0EA626AL,0xBB30732BL,0L,0x35309F33L,(-1L),(-1L)},{0x2F33BC6CL,(-4L),0xEB3E1984L,(-3L),(-3L),(-6L),(-3L),(-3L),0xEB3E1984L,(-4L)}},{{0x0B0DEBF6L,(-1L),0x5CF455DBL,0x169B2B29L,5L,4L,0xD3FD9053L,1L,(-1L),0xCE01153AL},{(-5L),1L,5L,(-1L),9L,4L,0x169B2B29L,(-6L),0L,0L},{0x0B0DEBF6L,0xC51F7EE0L,0xCE01153AL,(-6L),0xCF63547AL,(-6L),(-1L),0x5CF455DBL,0x2F33BC6CL,0x2F33BC6CL}}};
        int8_t *l_60[10][3][4] = {{{&l_50,&l_50,&l_50,&g_61},{&l_50,&l_50,&l_44,&g_61},{&l_50,(void*)0,&l_50,&g_61}},{{&l_50,&l_44,&l_44,&g_61},{&l_50,&g_61,&l_50,&g_61},{&l_50,&g_61,(void*)0,(void*)0}},{{&g_61,&l_44,&l_44,&g_61},{&l_50,&l_50,&g_61,&g_61},{&l_50,&g_61,(void*)0,&l_50}},{{&g_61,&l_50,&g_61,&l_50},{&l_50,&g_61,&g_61,&g_61},{(void*)0,&l_50,&l_50,&g_61}},{{&g_61,&l_44,&g_61,(void*)0},{&l_50,&g_61,&l_50,&g_61},{&g_61,&g_61,(void*)0,&g_61}},{{&g_61,&l_44,&l_50,&g_61},{&l_50,(void*)0,&l_50,&g_61},{&g_61,&l_50,(void*)0,&g_61}},{{&g_61,(void*)0,&g_61,&l_44},{&g_61,&l_44,&g_61,&g_61},{&g_61,&g_61,(void*)0,&l_44}},{{&l_50,&l_50,(void*)0,&l_50},{&g_61,&l_44,(void*)0,(void*)0},{&l_44,&l_44,&l_50,&l_50}},{{&l_44,&l_50,&l_50,&l_44},{&l_50,&g_61,&l_50,&g_61},{&g_61,&l_44,&l_50,&l_44}},{{&g_61,(void*)0,&l_50,&g_61},{&g_61,&g_61,(void*)0,&g_61},{&l_50,&l_50,(void*)0,&l_50}}};
        int32_t l_62 = 0xFABE4BCAL;
        const int32_t l_101 = 0x33A9B92FL;
        int32_t **l_462[8];
        uint64_t l_1090 = 0xCF12100AA7C0456ALL;
        uint8_t **l_1097 = &g_1094[3][3];
        int8_t l_1129 = 2L;
        int64_t ****l_1146 = &g_1144;
        uint8_t *l_1149 = (void*)0;
        uint8_t *l_1150 = &g_192.f2;
        int i, j, k;
        for (i = 0; i < 8; i++)
            l_462[i] = &g_191[1][0][1];
    }
    for (l_1104 = 3; (l_1104 >= 0); l_1104 -= 1)
    { /* block id: 412 */
        int32_t *l_1153 = &l_1110[2][5];
        int32_t *l_1154 = &l_1082;
        int32_t *l_1155 = &l_1082;
        int32_t *l_1156 = &g_116;
        int32_t *l_1157 = &g_7[2][2][0];
        int32_t *l_1158 = &l_1110[0][2];
        int32_t *l_1159 = &l_1108;
        int32_t *l_1160[10][4][1] = {{{&l_1108},{&g_8},{&l_1108},{&l_1108}},{{&l_1110[2][5]},{&l_1108},{&l_1108},{&g_8}},{{&l_1108},{&l_1108},{&l_1110[2][5]},{&l_1108}},{{&l_1108},{&g_8},{&l_1108},{&l_1108}},{{&l_1110[2][5]},{&l_1108},{&l_1108},{&g_8}},{{&l_1108},{&l_1108},{&l_1110[2][5]},{&l_1108}},{{&l_1108},{&g_8},{&l_1108},{&l_1108}},{{&l_1110[2][5]},{&l_1108},{&l_1108},{&g_8}},{{&l_1108},{&l_1108},{&l_1110[2][5]},{&l_1108}},{{&l_1108},{&g_8},{&l_1108},{&l_1108}}};
        int32_t l_1161[6][8][4] = {{{(-1L),0x4495C941L,0x47D85F03L,(-1L)},{0xF575E2F2L,0x4CB510F8L,0x47D85F03L,(-4L)},{(-1L),0x88BE0953L,0xAACFCF13L,0x68746ABFL},{0xD9136D32L,(-1L),0xD382AE1CL,0xAACFCF13L},{0xD382AE1CL,0xAACFCF13L,8L,0xE89183FBL},{0xCC055E7FL,0xEE914A57L,0L,0xFC040A39L},{(-1L),0x4CB510F8L,(-10L),0x47D85F03L},{(-3L),(-1L),(-2L),0x174CC20FL}},{{1L,0xB8EFEAFBL,0xD9136D32L,0x580BAE0DL},{0xD382AE1CL,0xA831B790L,0xFC040A39L,1L},{0x49B5BA96L,(-1L),0L,0x1860291DL},{(-2L),0xEC98388EL,0x47D85F03L,0x47D85F03L},{0x12C9CDC1L,0x12C9CDC1L,(-1L),(-4L)},{0xFC463D6AL,0x8E67A5F4L,1L,0x4495C941L},{0xD9136D32L,0xAACFCF13L,0xFC040A39L,1L},{1L,0xAACFCF13L,0L,0x4495C941L}},{{0xAACFCF13L,0x8E67A5F4L,0L,(-4L)},{(-3L),0x12C9CDC1L,0xF575E2F2L,0x47D85F03L},{8L,0xEC98388EL,0xFC463D6AL,0x1860291DL},{1L,(-1L),8L,1L},{1L,0xA831B790L,(-4L),0x580BAE0DL},{8L,0xB8EFEAFBL,0L,0x174CC20FL},{(-1L),(-1L),(-1L),0x47D85F03L},{0xE4787BD3L,0x4CB510F8L,8L,0xFC040A39L}},{{0xFC463D6AL,0xEE914A57L,0xAACFCF13L,0xE89183FBL},{0x49B5BA96L,0xAACFCF13L,(-4L),0xAACFCF13L},{0xE89183FBL,0xFE2F20F8L,0xFD5A2A99L,0xB8EFEAFBL},{0xA831B790L,8L,0xD382AE1CL,0xEC98388EL},{(-10L),0L,6L,0xE4787BD3L},{(-10L),8L,0xD382AE1CL,0x2886608DL},{0xA831B790L,0xE4787BD3L,0xFD5A2A99L,1L},{0xE89183FBL,0x1766FE96L,0xEC98388EL,(-1L)}},{{(-2L),0xF575E2F2L,0xC6358CB1L,0x8E67A5F4L},{(-4L),(-3L),(-10L),0xE4787BD3L},{6L,1L,0xE4787BD3L,0x68746ABFL},{0xFC040A39L,0xD9136D32L,0xDD572EFDL,8L},{0xFC463D6AL,0xFE2F20F8L,0xEC98388EL,0xDD572EFDL},{0x4495C941L,0xA831B790L,0xFC463D6AL,0xB8EFEAFBL},{(-6L),0x49B5BA96L,(-4L),0xE89183FBL},{(-10L),1L,1L,(-10L)}},{{0xF575E2F2L,8L,(-1L),1L},{0xC6358CB1L,0x12C9CDC1L,0xFD5A2A99L,(-1L)},{0x4495C941L,0x1860291DL,0xE89183FBL,(-1L)},{0L,0x12C9CDC1L,(-6L),1L},{(-4L),8L,0xE4787BD3L,(-10L)},{0x580BAE0DL,1L,0x4CB510F8L,0xE89183FBL},{0xD382AE1CL,0x49B5BA96L,0xDD572EFDL,0xB8EFEAFBL},{(-2L),0xA831B790L,0xE89183FBL,0xDD572EFDL}}};
        int i, j, k;
        ++l_1163;
        for (g_192.f2 = 0; (g_192.f2 <= 3); g_192.f2 += 1)
        { /* block id: 416 */
            const uint32_t l_1171[5][10] = {{4UL,0x1E4B04BDL,4UL,0UL,0UL,4UL,0x1E4B04BDL,4UL,0UL,0UL},{4UL,0x1E4B04BDL,4UL,0UL,0UL,4UL,0x1E4B04BDL,4UL,0UL,0UL},{4UL,0x1E4B04BDL,4UL,0UL,0UL,4UL,0x1E4B04BDL,4UL,0UL,0UL},{4UL,0x1E4B04BDL,4UL,0UL,0UL,4UL,0x1E4B04BDL,4UL,0UL,0UL},{4UL,0x1E4B04BDL,4UL,0UL,0UL,4UL,0x1E4B04BDL,4UL,0UL,0UL}};
            uint16_t *l_1187[10][3][1] = {{{&g_648},{&g_291[4][3]},{&g_648}},{{&g_1043},{&g_648},{&g_1043}},{{&g_648},{&g_291[4][3]},{&g_648}},{{&g_1043},{&g_648},{&g_1043}},{{&g_648},{&g_291[4][3]},{&g_648}},{{&g_1043},{&g_648},{&g_1043}},{{&g_648},{&g_291[4][3]},{&g_648}},{{&g_1043},{&g_648},{&g_1043}},{{&g_648},{&g_291[4][3]},{&g_648}},{{&g_1043},{&g_648},{&g_1043}}};
            int32_t l_1188 = 0xD8CDFF83L;
            uint16_t ****l_1189 = (void*)0;
            uint16_t **l_1192 = &l_1187[2][2][0];
            uint16_t ***l_1191[7];
            uint16_t ****l_1190 = &l_1191[2];
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_1191[i] = &l_1192;
            (*l_1154) |= ((((*l_1190) = ((((g_475.f0 > g_291[4][3]) , (~((g_648 != g_974) && g_284))) , (safe_mul_func_uint16_t_u_u((l_1188 ^= ((safe_mul_func_int16_t_s_s((l_1171[3][5] > (safe_mod_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_u((safe_unary_minus_func_uint8_t_u(5UL)), (safe_rshift_func_int16_t_s_u(l_1171[4][1], 11)))) && ((safe_add_func_int32_t_s_s((safe_lshift_func_uint16_t_u_s((safe_add_func_uint16_t_u_u(((*l_476) |= (safe_div_func_uint16_t_u_u(l_1171[3][5], 5UL))), (*g_977))), 13)), 1UL)) || l_1171[2][1])), (-6L)))), (*l_1153))) , (**g_289))), g_1151[1]))) , (void*)0)) == (void*)0) ^ (*l_63));
        }
    }
    l_1202 |= ((safe_div_func_int32_t_s_s(0x74B9BF3AL, ((*l_63) || (-1L)))) || (((safe_add_func_int8_t_s_s((*l_63), (*l_63))) ^ ((*l_63) < (safe_mul_func_uint16_t_u_u(65529UL, (-9L))))) < (((*l_476) , (*l_476)) == (*l_63))));
    return (*g_290);
}


/* ------------------------------------------ */
/* 
 * reads : g_375 g_291 g_192.f0 g_192.f2 g_475.f1 g_474 g_8 g_456 g_7 g_146 g_317 g_116 g_630 g_290 g_648 g_653 g_206 g_148 g_698 g_475.f2 g_464 g_475 g_708 g_78 g_454 g_739 g_284 g_1081
 * writes: g_146 g_291 g_7 g_192.f2 g_475.f1 g_148 g_456 g_116 g_191 g_648 g_654 g_206 g_696 g_475.f2 g_378 g_78 g_284 g_317
 */
static int8_t  func_14(int32_t * p_15, int32_t * p_16, const int32_t * p_17, uint32_t  p_18, uint32_t  p_19)
{ /* block id: 147 */
    uint8_t l_484 = 0x1EL;
    int64_t *l_485 = &g_146[6];
    uint64_t l_504 = 7UL;
    int32_t l_505 = 4L;
    uint16_t *l_509 = &g_291[4][3];
    uint16_t **l_512 = (void*)0;
    uint16_t *l_514 = &g_378;
    uint16_t **l_513 = &l_514;
    int32_t *l_515 = &g_7[7][0][0];
    int32_t l_554 = 0x7E2BCC1AL;
    int32_t l_556 = (-1L);
    int32_t l_559 = 0L;
    int32_t l_560 = 1L;
    int32_t l_561 = 0xEC7F6DAFL;
    int64_t l_604 = 0x63C334EAA2BFFFA3LL;
    int32_t l_624 = 0x7B7EF9E0L;
    int32_t *l_637[8] = {&l_559,&l_505,&l_559,&l_505,&l_559,&l_505,&l_559,&l_505};
    uint64_t l_668 = 18446744073709551613UL;
    uint32_t l_735 = 0x338A6B97L;
    uint16_t l_740 = 65531UL;
    int8_t *** const **l_817 = &g_816;
    uint8_t l_869 = 0x55L;
    const float l_983 = 0x0.0p-1;
    uint16_t l_1042[10] = {0x2B15L,0UL,0xEA91L,0UL,0x2B15L,0x2B15L,0UL,0xEA91L,0UL,0x2B15L};
    uint32_t ***l_1059 = (void*)0;
    int8_t **l_1075 = &g_114;
    uint8_t *l_1078 = (void*)0;
    uint8_t *l_1079 = &l_869;
    uint32_t *l_1080 = &g_696;
    int i;
    l_505 = ((safe_mod_func_int32_t_s_s((!((((safe_sub_func_int16_t_s_s((safe_mul_func_int16_t_s_s((((*l_485) = l_484) < (((safe_lshift_func_int8_t_s_u(p_18, 6)) > (4294967295UL >= (safe_unary_minus_func_int32_t_s(l_484)))) , l_484)), ((safe_lshift_func_uint16_t_u_u(l_484, 3)) , ((!(0xAA536D56L != (safe_rshift_func_uint8_t_u_s(((safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((safe_mod_func_int16_t_s_s((((safe_mul_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((l_484 < (g_375[0] || l_484)), l_484)), p_18)) , &g_206) != l_485), l_484)), 4)), 0x35L)) || 4294967295UL), 2)))) || 0x4CBDL)))), g_291[4][3])) < l_484) || l_484) | l_504)), p_19)) >= g_192.f0);
    if (((~(((*l_515) = (p_18 && (safe_mul_func_uint16_t_u_u((++(*l_509)), (l_504 > (l_509 != ((*l_513) = l_509))))))) > (l_509 != (p_18 , ((safe_lshift_func_uint16_t_u_u(p_18, 5)) , &g_148))))) <= p_18))
    { /* block id: 153 */
        uint32_t l_536 = 0xEE935D95L;
        uint16_t **l_537 = (void*)0;
        int32_t l_546 = 3L;
        float l_547 = 0x0.8p+1;
        int32_t l_550 = 0x3C2ED549L;
        float l_552[6][9] = {{0xE.C6066Bp+46,0x7.17D998p+19,0xC.B5D74Fp+71,0x7.17D998p+19,0xE.C6066Bp+46,(-0x8.0p-1),(-0x1.5p-1),0x8.0141AAp-38,0xD.2BE89Ep+98},{0x0.Bp+1,0x3.DA09D5p+51,(-0x1.8p+1),0x0.3A70FCp-75,0xF.CDE9FEp-2,0x0.Bp+1,0xA.23F84Ap-3,0x7.6p-1,0x1.CD5600p+33},{(-0x1.3p-1),(-0x1.Ap-1),(-0x1.Fp+1),0x7.6p-1,0xD.2BE89Ep+98,(-0x8.0p-1),(-0x8.0p-1),0xD.2BE89Ep+98,0x7.6p-1},{0x7.87FA0Cp+1,0xE.C6066Bp+46,0x7.87FA0Cp+1,0x0.Bp+1,0xD.2BE89Ep+98,0x2.5p+1,0xE.C6066Bp+46,0x8.7p-1,0x2.Ap-1},{0xC.B5D74Fp+71,0x0.Bp+1,(-0x1.5p-1),0x7.87FA0Cp+1,0xF.CDE9FEp-2,(-0x2.3p+1),0x2.Ap-1,(-0x1.Fp+1),0x2.5p+1},{0x7.17D998p+19,(-0x1.5p-1),0x1.CD5600p+33,0x0.Bp+1,(-0x1.Ap-1),(-0x1.Ap-1),0x0.Bp+1,0x1.CD5600p+33,(-0x1.5p-1)}};
        int32_t l_555 = 0L;
        int32_t l_557 = 0x292D667FL;
        int32_t l_558 = 0x3F6401ECL;
        int32_t l_564 = 0x866C8C42L;
        int32_t l_565 = 0x539CD804L;
        int8_t ****l_577[5];
        int32_t l_608 = 9L;
        int32_t l_609 = 1L;
        int32_t l_611 = 1L;
        int32_t l_658[7] = {0L,0x1C2D842CL,0x1C2D842CL,0L,0x1C2D842CL,0x1C2D842CL,0L};
        uint64_t l_706 = 0xE9E1D87678AF9F7ALL;
        int i, j;
        for (i = 0; i < 5; i++)
            l_577[i] = &g_248;
        for (g_192.f2 = 0; (g_192.f2 <= 3); g_192.f2 += 1)
        { /* block id: 156 */
            int64_t l_538 = 0x78D276F7462146B1LL;
            int32_t l_548 = (-8L);
            int32_t l_549 = 5L;
            int32_t l_551 = 0xAE86BC28L;
            int32_t l_553 = 0x0E8AA3B9L;
            int32_t l_562 = 0xFE3911DBL;
            int32_t l_563[1];
            uint32_t l_566[8];
            uint8_t l_613 = 0x97L;
            int8_t ***l_622 = &g_249;
            uint32_t *l_652 = &g_284;
            uint32_t **l_651 = &l_652;
            int64_t l_659 = (-1L);
            int32_t l_665 = 0x0B6FDCFEL;
            int8_t l_731 = 0xD9L;
            int i;
            for (i = 0; i < 1; i++)
                l_563[i] = (-1L);
            for (i = 0; i < 8; i++)
                l_566[i] = 9UL;
            for (g_475.f1 = 0; (g_475.f1 <= 3); g_475.f1 += 1)
            { /* block id: 159 */
                float *l_523[10] = {(void*)0,&g_145,&g_145,(void*)0,&g_456,(void*)0,&g_145,&g_145,(void*)0,&g_456};
                int32_t l_524[7];
                int64_t *l_527[6][1][2] = {{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}}};
                int64_t **l_528 = &l_485;
                int16_t *l_529 = &g_148;
                int32_t *l_539 = &g_7[4][1][0];
                int32_t *l_540 = &l_524[4];
                int32_t *l_541 = (void*)0;
                int32_t *l_542 = &l_524[5];
                int32_t *l_543 = &g_116;
                int32_t *l_544 = &g_7[5][2][0];
                int32_t *l_545[4][5][6] = {{{(void*)0,&g_116,&l_505,&g_7[7][3][0],&l_505,&g_116},{&g_116,(void*)0,&l_505,&g_8,&l_505,&g_7[7][3][0]},{&l_524[5],&g_8,&g_116,&g_116,&g_8,&l_524[5]},{&g_116,&g_8,&l_524[5],(void*)0,&l_505,&l_505},{&l_505,(void*)0,&g_116,(void*)0,&l_505,&g_8}},{{&l_505,&g_116,(void*)0,(void*)0,(void*)0,(void*)0},{&g_116,&l_505,&l_505,&g_116,&g_116,(void*)0},{&l_524[5],(void*)0,(void*)0,&g_8,(void*)0,&g_8},{&g_116,(void*)0,&g_116,&g_7[7][3][0],(void*)0,&l_505},{(void*)0,(void*)0,&l_524[5],&g_116,&g_116,&l_524[5]}},{{&l_505,&l_505,&g_116,&g_116,(void*)0,&g_7[7][3][0]},{(void*)0,&g_116,&l_505,&g_7[7][3][0],&l_505,&g_116},{&g_116,(void*)0,&l_505,&g_8,&l_505,&g_7[7][3][0]},{&l_524[5],&g_8,&g_116,&g_116,&g_8,&l_524[5]},{&g_116,&g_8,&l_524[5],(void*)0,&l_505,&l_505}},{{&l_505,(void*)0,&g_116,(void*)0,&l_505,&g_8},{&l_505,&g_116,(void*)0,(void*)0,(void*)0,(void*)0},{&g_116,&l_505,&l_505,&g_116,&g_116,(void*)0},{&l_524[5],(void*)0,(void*)0,&g_8,(void*)0,&g_116},{&g_8,(void*)0,&g_8,(void*)0,(void*)0,&l_524[5]}}};
                float l_586 = 0x1.9p+1;
                int8_t ***l_623[3];
                int i, j, k;
                for (i = 0; i < 7; i++)
                    l_524[i] = (-1L);
                for (i = 0; i < 3; i++)
                    l_623[i] = (void*)0;
                (*g_474) = ((safe_add_func_float_f_f(0x0.7B7CA4p-91, ((safe_sub_func_float_f_f(0xC.1C8228p+43, (!(l_524[5] = g_375[g_475.f1])))) < (((g_375[g_192.f2] > (safe_add_func_float_f_f((l_527[4][0][0] != ((*l_528) = (void*)0)), (((*l_529) = 2L) , (safe_add_func_float_f_f((safe_sub_func_float_f_f((l_536 = (safe_sub_func_float_f_f(p_19, 0x1.1p+1))), g_375[g_192.f2])), p_18)))))) > (-0x5.Ep-1)) < g_375[g_475.f1])))) <= p_19);
                l_538 = (l_537 == (void*)0);
                --l_566[0];
                for (p_19 = 0; (p_19 <= 1); p_19 += 1)
                { /* block id: 169 */
                    uint16_t l_599 = 65535UL;
                    int32_t l_600 = 0x0E42C6CCL;
                    int32_t l_601 = 0x83CA41CCL;
                    int32_t l_605 = (-9L);
                    int32_t l_606 = 0x3BAA523BL;
                    int32_t l_607 = (-1L);
                    int32_t l_610 = 0x03CC5CB5L;
                    int32_t l_612 = 0L;
                    uint32_t *l_635 = (void*)0;
                    int i, j, k;
                    if ((*p_16))
                    { /* block id: 170 */
                        (*g_474) = (safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_sub_func_float_f_f((*g_474), (((((safe_add_func_int16_t_s_s((&g_248 == l_577[4]), (safe_lshift_func_uint16_t_u_s(((0x5FAD88CBL & (((*p_17) <= 0x3B374265L) , ((safe_lshift_func_int16_t_s_u(((*l_539) = ((++(*l_509)) || ((((*l_515) >= (((*l_543) = 0L) < ((safe_lshift_func_int16_t_s_u(((safe_unary_minus_func_uint64_t_u((safe_div_func_uint16_t_u_u(((((safe_add_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(((g_146[6] = (~p_18)) == p_19), 0xB0719BB40CF64BA9LL)), (*p_17))), l_599)) , g_146[0]) , p_18) > g_8), p_18)))) < p_19), 12)) ^ 0x4AL))) && 1UL) , 1L))), p_18)) , (*p_17)))) & p_18), p_19)))) && l_563[0]) , g_291[8][0]) , g_456) >= g_317))), 0x0.Fp+1)), p_19));
                        return p_18;
                    }
                    else
                    { /* block id: 177 */
                        int32_t l_602 = 0x0A37B8E8L;
                        int32_t l_603[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_603[i] = 2L;
                        if ((*p_17))
                            break;
                        ++l_613;
                    }
                    (*l_539) |= (((safe_unary_minus_func_uint64_t_u((0x46L != ((g_8 , (((safe_unary_minus_func_uint16_t_u(((l_611 = ((0x99B0L != p_19) != p_18)) & ((((((*l_543) || (safe_rshift_func_int8_t_s_s((((*p_16) | (((((((*g_474) < ((l_622 != l_623[2]) >= (*l_543))) != p_19) == p_19) > p_19) , &g_370) != &g_370)) != p_18), 5))) != 0xA3BC4129E6B5083CLL) , &g_284) == &l_566[0]) && l_563[0])))) & 0UL) || (*l_542))) == p_18)))) != l_624) , l_549);
                    (*l_515) = p_18;
                    for (l_610 = 1; (l_610 >= 10); l_610 = safe_add_func_int16_t_s_s(l_610, 2))
                    { /* block id: 186 */
                        int32_t * const l_627 = &l_546;
                        int32_t **l_628[9] = {&l_540,&l_540,&l_540,&l_540,&l_540,&l_540,&l_540,&l_540,&l_540};
                        uint32_t *l_636 = &l_566[4];
                        int i;
                        g_191[1][0][1] = l_627;
                        l_637[3] = func_25((g_191[(p_19 + 1)][p_19][g_192.f2] = &l_554), l_563[0], g_375[0], ((g_630 != (void*)0) && (0x7DC9CD3AL > ((((*l_636) = (((((safe_add_func_uint8_t_u_u(0x3FL, ((((safe_lshift_func_int8_t_s_s(p_19, 1)) >= p_19) , g_146[2]) , 0UL))) , l_557) || p_18) , l_635) != (void*)0)) || (*l_515)) < p_18))), (*g_290));
                    }
                }
            }
            if (((*l_515) = g_375[g_192.f2]))
            { /* block id: 195 */
                if ((*p_17))
                    break;
            }
            else
            { /* block id: 197 */
                float l_656 = 0x2.75963Ep-46;
                int32_t l_657 = 0x99282E1BL;
                int32_t l_660 = 0x319EBB2CL;
                int32_t l_663 = 0x93A734AEL;
                float l_664 = 0x3.552D7Dp+31;
                int32_t l_666 = 0x2FF24191L;
                int16_t l_667 = 0x351DL;
                int32_t l_697 = 0xD4C53C1FL;
                uint32_t *l_723[4][6][6] = {{{(void*)0,&l_566[0],&l_566[5],&g_284,&g_284,&l_566[1]},{&l_566[0],&l_566[0],&l_566[5],&l_566[0],&l_566[0],&l_566[5]},{&l_566[0],&l_566[0],&l_566[1],&g_284,&g_284,&l_566[5]},{&g_284,&g_284,&l_566[5],&l_566[0],(void*)0,&l_566[1]},{(void*)0,&g_284,&l_566[5],(void*)0,&l_566[0],&l_566[5]},{&l_566[0],(void*)0,&l_566[1],&l_566[0],(void*)0,&l_566[5]}},{{&l_566[0],(void*)0,&l_566[5],(void*)0,&l_566[0],&l_566[1]},{&g_284,(void*)0,&l_566[5],&l_566[0],(void*)0,&l_566[5]},{(void*)0,&l_566[0],&l_566[1],(void*)0,&l_566[0],&l_566[5]},{(void*)0,&l_566[0],&l_566[5],&g_284,&g_284,&l_566[1]},{&l_566[0],&l_566[0],&l_566[5],&l_566[0],&l_566[0],&l_566[5]},{&l_566[0],&l_566[0],&l_566[1],&g_284,&g_284,&l_566[5]}},{{&g_284,&g_284,&l_566[5],&l_566[0],(void*)0,&l_566[1]},{(void*)0,&g_284,&l_566[5],(void*)0,&l_566[0],&l_566[5]},{&l_566[0],(void*)0,&l_566[1],&l_566[0],(void*)0,&l_566[5]},{&l_566[0],(void*)0,&l_566[5],(void*)0,&l_566[0],&l_566[1]},{&g_284,(void*)0,&l_566[5],&l_566[0],(void*)0,&l_566[5]},{(void*)0,&l_566[0],&l_566[1],(void*)0,&l_566[0],&l_566[5]}},{{(void*)0,&l_566[0],&l_566[5],&g_284,&g_284,&l_566[1]},{&l_566[0],&l_566[0],&l_566[5],&l_566[0],&l_566[0],&l_566[5]},{&l_566[0],&l_566[0],&l_566[1],&g_284,&g_284,&l_566[5]},{&g_284,&g_284,&l_566[5],&l_566[0],(void*)0,&l_566[1]},{(void*)0,&g_284,&l_566[5],(void*)0,&l_566[0],&l_566[5]},{&l_566[0],(void*)0,&l_566[1],&l_566[0],(void*)0,&l_566[5]}}};
                int i, j, k;
                for (l_564 = (-8); (l_564 != (-14)); l_564 = safe_sub_func_uint16_t_u_u(l_564, 3))
                { /* block id: 200 */
                    uint32_t l_642[7][6][4] = {{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}},{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}},{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}},{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}},{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}},{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}},{{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL},{0x6C96FF16L,3UL,8UL,3UL},{0x6C96FF16L,0x1F17BE4AL,8UL,0x1F17BE4AL}}};
                    int32_t l_645 = 1L;
                    int32_t l_661 = 8L;
                    int32_t l_662 = 1L;
                    int i, j, k;
                    for (l_546 = 7; (l_546 != (-14)); l_546--)
                    { /* block id: 203 */
                        int8_t l_646 = 0x3AL;
                        int32_t l_647[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_647[i] = (-1L);
                        ++l_642[6][4][2];
                        g_648++;
                        (*g_653) = l_651;
                        return p_18;
                    }
                    if ((*p_16))
                        continue;
                    ++l_668;
                }
                for (g_206 = 0; (g_206 <= 2); g_206 += 1)
                { /* block id: 214 */
                    uint8_t *l_692 = (void*)0;
                    uint8_t **l_691 = &l_692;
                    uint8_t l_693 = 6UL;
                    int16_t *l_694[4][8] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                    int32_t l_695 = 0L;
                    int i, j;
                    if ((l_666 ^ (safe_sub_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((l_563[0] <= ((safe_mod_func_int16_t_s_s(((+((*p_16) == 1UL)) && (-2L)), ((0L > (safe_div_func_uint8_t_u_u(((g_696 = (l_695 = (g_148 &= ((safe_lshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((*p_16) , (+((safe_lshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((l_691 == (l_666 , &l_692)), 6)), 6)) >= (*l_515)))), l_693)), l_566[6])) & 0x8B80L)))) | l_697), 255UL))) ^ 0x6F9084B0L))) || l_550)), g_206)), 1)), p_18))))
                    { /* block id: 218 */
                        uint8_t *l_701[10][2];
                        int32_t l_707 = 0L;
                        int i, j;
                        for (i = 0; i < 10; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_701[i][j] = &l_613;
                        }
                        (*g_698) = &l_666;
                        (*g_708) = func_25((func_20(p_16, (((((safe_mul_func_uint8_t_u_u(p_19, (g_475.f2--))) ^ l_550) != (safe_mul_func_uint16_t_u_u(((p_18 , ((l_706 & (g_148 = (((**l_513) = ((&g_146[2] == &g_146[5]) > ((void*)0 != &p_15))) >= l_707))) , p_19)) , g_375[g_192.f2]), l_555))) , l_548) , &l_695), &l_695, p_18) , (void*)0), l_693, p_18, p_19, l_551);
                        (*l_515) = 1L;
                        (*l_515) = 0xB1B385EBL;
                    }
                    else
                    { /* block id: 226 */
                        uint8_t *l_720 = &l_484;
                        uint8_t *l_721 = &l_693;
                        int32_t l_722[9][7] = {{0xCE9EB4E3L,0x6970686FL,0x72B817B6L,0x5D53BBEDL,0x813ABBE7L,(-5L),(-9L)},{0x6970686FL,0xFF72E113L,0x813ABBE7L,1L,0x50AF6CF5L,0x50AF6CF5L,1L},{0xCE9EB4E3L,0x5D53BBEDL,0xCE9EB4E3L,0x50AF6CF5L,0xB4A9B0DDL,0xFF72E113L,1L},{0x6970686FL,0x72B817B6L,0x5D53BBEDL,0x813ABBE7L,(-5L),(-9L),0x1EBE3A41L},{0x50AF6CF5L,(-9L),1L,0x72B817B6L,0x72B817B6L,1L,(-9L)},{0x1EBE3A41L,(-2L),(-1L),0x50AF6CF5L,0x72B817B6L,0xCE9EB4E3L,(-1L)},{0xFF72E113L,0x6970686FL,(-2L),0x0C7D2948L,(-5L),0x5D53BBEDL,(-1L)},{0x813ABBE7L,1L,0x50AF6CF5L,0x50AF6CF5L,1L,0x813ABBE7L,0xFF72E113L},{(-2L),(-1L),0x50AF6CF5L,0x72B817B6L,0xCE9EB4E3L,(-1L),1L}};
                        int32_t l_730 = 1L;
                        float *l_732 = &l_547;
                        float *l_734 = &l_552[1][7];
                        int i, j;
                        (*l_734) = ((*l_732) = (safe_sub_func_float_f_f(0x1.Dp+1, ((safe_add_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s((((((safe_mul_func_uint16_t_u_u((~(((g_78 ^= (l_722[6][1] = (p_18 , ((*l_721) = ((*l_720) &= (++g_475.f2)))))) , (l_637[1] != l_723[0][1][2])) > ((*l_515) , ((safe_lshift_func_int16_t_s_u((-4L), 14)) > ((safe_sub_func_uint16_t_u_u(p_18, (safe_mod_func_uint8_t_u_u(l_730, (((*p_17) == l_549) | l_695))))) || l_731))))), 0x8649L)) | l_551) || 0x636576FEL) != l_550) < g_375[0]), p_19)), g_454)) , 0xA.FC0FAAp-22))));
                        l_735--;
                    }
                    return l_657;
                }
                if ((*p_16))
                    continue;
                (*l_515) = l_538;
            }
        }
    }
    else
    { /* block id: 242 */
        int32_t *l_738[9] = {&l_561,(void*)0,&l_561,&l_561,(void*)0,&l_561,&l_561,(void*)0,&l_561};
        int32_t **l_741 = &l_738[7];
        int i;
        (*g_739) = l_738[7];
        (*g_708) = ((*l_741) = func_31(l_740));
        (*l_515) |= 0x51D4DBBDL;
    }
    for (g_284 = 0; (g_284 < 48); g_284 = safe_add_func_uint16_t_u_u(g_284, 9))
    { /* block id: 250 */
        int64_t l_752 = 0x2E63049120D0C17ELL;
        int32_t l_777 = 0x45322BE8L;
        int8_t ****l_819 = &g_248;
        int32_t l_857[6][8] = {{0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L)},{0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L)},{0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L)},{0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L)},{0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L)},{0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L),0x00E85766L,(-1L)}};
        int32_t l_870 = 0x8751DB7FL;
        const uint16_t *l_888 = &g_375[2];
        int32_t *l_895 = &l_624;
        int16_t **l_958 = (void*)0;
        int16_t *l_982 = &g_148;
        int16_t **l_981 = &l_982;
        uint64_t l_1014[8] = {6UL,0UL,6UL,6UL,0UL,6UL,6UL,0UL};
        uint32_t ***l_1058 = &g_654[2][0];
        int i, j;
        for (l_504 = 6; (l_504 != 11); ++l_504)
        { /* block id: 253 */
            uint64_t l_753 = 0xA2B620092E333B3DLL;
            const float *l_858 = &g_145;
            int32_t l_887 = 0x5320C2F8L;
            uint8_t l_922 = 0x2DL;
            int32_t l_927 = 0x8D32E4DDL;
            int8_t *****l_953[7];
            int16_t *l_960 = (void*)0;
            int16_t **l_959 = &l_960;
            uint32_t l_984 = 0x417A1EFCL;
            int32_t l_1013 = 0x1D745BBDL;
            uint16_t ***l_1044 = &l_512;
            int i;
            for (i = 0; i < 7; i++)
                l_953[i] = &g_247[1][0][2];
            for (g_317 = 15; (g_317 >= 48); g_317 = safe_add_func_int16_t_s_s(g_317, 6))
            { /* block id: 256 */
                int32_t *l_802 = &g_8;
                int8_t * const *l_826 = (void*)0;
                int8_t * const **l_825 = &l_826;
                int8_t * const ***l_824 = &l_825;
                int8_t * const **** const l_823[5][1] = {{&l_824},{&l_824},{&l_824},{&l_824},{&l_824}};
                int64_t **l_903 = &l_485;
                int32_t l_931[6];
                int32_t l_1005 = 0x32FEBD23L;
                uint32_t ***l_1057 = &g_654[2][0];
                int i, j;
                for (i = 0; i < 6; i++)
                    l_931[i] = (-9L);
            }
            return p_18;
        }
    }
    l_637[3] = ((safe_add_func_uint8_t_u_u(((p_18 & 0x5F5FL) , (*l_515)), (*l_515))) , func_25(func_25(&l_560, (((l_1075 == l_1075) , &g_654[2][0]) != ((safe_add_func_int8_t_s_s((((((*l_1080) = (((*l_515) |= ((*l_1079) = ((p_18 , l_817) == l_817))) && 0x4AL)) , &l_513) != &l_513) == g_206), p_19)) , l_1059)), g_1081, p_19, (*g_290)), p_18, g_148, p_19, p_18));
    return p_19;
}


/* ------------------------------------------ */
/* 
 * reads : g_464 g_317 g_7 g_291 g_474 g_475
 * writes: g_456
 */
static union U0  func_20(int32_t * p_21, int32_t * p_22, int32_t * p_23, float  p_24)
{ /* block id: 141 */
    int32_t *l_465 = &g_7[0][4][0];
    int32_t **l_466 = (void*)0;
    int32_t *l_467[2][2][5] = {{{&g_7[7][3][0],&g_8,(void*)0,(void*)0,&g_8},{&g_116,&g_7[7][3][0],(void*)0,(void*)0,&g_7[7][3][0]}},{{&g_7[7][3][0],&g_8,(void*)0,(void*)0,&g_8},{&g_116,&g_7[7][3][0],(void*)0,(void*)0,&g_7[7][3][0]}}};
    uint16_t *l_469 = &g_378;
    uint16_t **l_468 = &l_469;
    uint16_t **l_470[10][8][3] = {{{&l_469,(void*)0,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,(void*)0,&l_469}},{{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,(void*)0,(void*)0},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,(void*)0,&l_469},{&l_469,(void*)0,&l_469},{&l_469,&l_469,&l_469}},{{&l_469,&l_469,&l_469},{&l_469,(void*)0,(void*)0},{(void*)0,(void*)0,&l_469},{&l_469,&l_469,&l_469},{&l_469,(void*)0,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469}},{{(void*)0,&l_469,&l_469},{&l_469,(void*)0,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469}},{{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,(void*)0},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469}},{{&l_469,&l_469,&l_469},{(void*)0,(void*)0,(void*)0},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,(void*)0,&l_469},{&l_469,&l_469,&l_469}},{{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,(void*)0,&l_469}},{{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,(void*)0,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469}},{{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,&l_469,(void*)0}},{{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{(void*)0,(void*)0,(void*)0},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469},{&l_469,&l_469,&l_469}}};
    int16_t l_473 = 0xA932L;
    int i, j, k;
    (*g_474) = (g_464 , (((l_465 = l_465) != l_467[1][1][3]) <= ((g_317 , p_24) < (((((l_468 != (l_470[0][1][0] = l_470[0][1][0])) , ((safe_sub_func_int8_t_s_s(0xAFL, (g_7[7][3][0] || g_291[7][2]))) == 0xE2A82DE6746414B2LL)) , (-0x1.Bp-1)) != p_24) == l_473))));
    return g_475;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_25(int32_t * p_26, int64_t  p_27, int32_t  p_28, uint16_t  p_29, const uint16_t  p_30)
{ /* block id: 139 */
    return &g_8;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_31(uint64_t  p_32)
{ /* block id: 136 */
    int32_t *l_461 = &g_8;
    return l_461;
}


/* ------------------------------------------ */
/* 
 * reads : g_148 g_289 g_290 g_291 g_116 g_61 g_145 g_7 g_8
 * writes: g_148 g_145 g_454 g_456
 */
static uint32_t  func_37(int32_t * p_38)
{ /* block id: 125 */
    int8_t l_423 = 4L;
    int64_t *l_428 = (void*)0;
    int32_t l_432 = 0x46EB7A1BL;
    int16_t *l_435[3];
    uint64_t l_436[7][8] = {{0x68E0F3748E4E2396LL,18446744073709551608UL,0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL,18446744073709551608UL,0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL,18446744073709551608UL},{18446744073709551608UL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL},{0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL,18446744073709551608UL,0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL,18446744073709551608UL,0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL},{18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL},{0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL},{0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL,18446744073709551608UL,0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL,18446744073709551608UL,0x68E0F3748E4E2396LL,0x68E0F3748E4E2396LL},{18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL,18446744073709551609UL,18446744073709551609UL,0x68E0F3748E4E2396LL}};
    int i, j;
    for (i = 0; i < 3; i++)
        l_435[i] = &g_148;
    if (((l_423 || (safe_div_func_uint32_t_u_u((safe_mod_func_int16_t_s_s((((void*)0 == l_428) , (((g_148 |= ((+(((safe_add_func_int8_t_s_s((l_432 = l_423), l_423)) | (safe_mul_func_int8_t_s_s(l_423, ((void*)0 == &p_38)))) >= ((void*)0 == p_38))) , (-1L))) , &g_7[0][3][0]) != (void*)0)), (**g_289))), l_436[2][0]))) ^ 0L))
    { /* block id: 128 */
        uint64_t l_441 = 1UL;
        float *l_450 = (void*)0;
        float *l_451 = &g_145;
        float *l_457 = (void*)0;
        float *l_458 = &g_456;
        (*l_458) = (safe_sub_func_float_f_f((safe_sub_func_float_f_f(l_441, (safe_sub_func_float_f_f(g_116, ((safe_mul_func_float_f_f((0x6.5p+1 < ((((g_454 = (safe_add_func_float_f_f((safe_mul_func_float_f_f((0x0.1p+1 > ((*l_451) = g_291[4][1])), (l_441 != (((0x6.8p+1 != (safe_mul_func_float_f_f(((l_436[2][0] != 0x4.9FBD58p+62) <= l_436[1][1]), g_61))) != l_441) != l_441)))), l_441))) <= 0x1.9p+1) != (-0x1.Fp-1)) < l_432)), l_441)) < 0x6.6BDEAAp+42))))), (-0x4.Fp+1)));
    }
    else
    { /* block id: 132 */
        uint32_t l_460 = 1UL;
        l_460 = (!((*p_38) != (0x22D0028AL && l_423)));
    }
    return l_423;
}


/* ------------------------------------------ */
/* 
 * reads : g_78 g_8 g_116 g_7 g_61 g_146 g_118 g_192 g_192.f0 g_192.f2 g_247 g_252 g_148 g_284 g_289 g_290 g_291 g_317 g_206 g_192.f1 g_370 g_375 g_419
 * writes: g_7 g_114 g_116 g_61 g_146 g_148 g_78 g_145 g_191 g_206 g_247 g_289 g_284 g_317
 */
static int32_t * func_39(float  p_40, int32_t  p_41)
{ /* block id: 14 */
    int32_t *l_108 = &g_7[5][3][0];
    const uint8_t *l_111 = &g_78;
    int8_t *l_112[9] = {&g_61,&g_61,&g_61,&g_61,&g_61,&g_61,&g_61,&g_61,&g_61};
    int8_t **l_113[7] = {&l_112[3],&l_112[4],&l_112[4],&l_112[3],&l_112[4],&l_112[4],&l_112[3]};
    int32_t *l_115 = &g_116;
    int32_t l_120 = 0L;
    int32_t l_122 = 9L;
    int32_t l_123 = 0x11F35E5FL;
    int32_t l_124 = 0L;
    int32_t l_125 = 0x3A15A7CFL;
    int32_t l_126 = 0x8E4B99A0L;
    int32_t l_127 = (-10L);
    int32_t l_128[1][10] = {{0L,0L,0L,0L,0L,0L,0L,0L,0L,0L}};
    uint64_t l_219 = 1UL;
    uint16_t l_232 = 0xEBDFL;
    int32_t l_314 = (-4L);
    int8_t l_324 = 0L;
    int16_t l_408 = 9L;
    int32_t *l_420[4][1][5] = {{{&g_116,&g_116,&l_128[0][8],&l_128[0][8],&g_116}},{{&g_7[7][3][0],(void*)0,&g_7[7][3][0],(void*)0,&g_7[7][3][0]}},{{&g_116,&l_128[0][8],&l_128[0][8],&g_116,&g_116}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
    int i, j, k;
    if (((safe_sub_func_int16_t_s_s(((0UL != ((*l_115) |= (safe_sub_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u(((((*l_108) = 0x76920728L) , (((-2L) ^ g_78) ^ (l_111 == &g_78))) , ((p_41 , l_111) != (g_114 = l_112[3]))), g_8)), 0UL)))) != g_78), 0xA9E3L)) < 3UL))
    { /* block id: 18 */
        int32_t l_117 = 9L;
        int32_t l_119[5];
        int32_t *l_121[7][8] = {{&g_7[7][3][0],&l_119[3],&g_7[7][3][0],&l_120,&l_119[3],&g_8,&g_8,&l_119[3]},{&l_119[3],&g_8,&g_8,&l_119[3],&l_120,&g_7[7][3][0],&l_119[3],&g_7[7][3][0]},{&l_119[3],&g_116,&g_7[7][3][0],&g_116,&l_119[3],&g_7[7][3][0],&g_8,&g_8},{&g_7[7][3][0],&g_116,&l_120,&l_120,&g_116,&g_7[7][3][0],&g_8,&g_116},{&g_8,&g_8,&l_120,&g_8,&l_120,&g_8,&g_8,&g_7[7][3][0]},{&g_116,&l_119[3],&g_7[7][3][0],&g_8,&g_8,&g_7[7][3][0],&l_119[3],&g_116},{&g_7[7][3][0],&g_8,&g_8,&l_120,&g_8,&l_120,&g_8,&g_8}};
        float l_129 = 0x1.4p-1;
        uint16_t l_130 = 0x7EB2L;
        int8_t **l_143[6][8] = {{(void*)0,&l_112[3],&l_112[0],(void*)0,&g_114,(void*)0,(void*)0,&g_114},{&g_114,&g_114,(void*)0,(void*)0,&l_112[3],(void*)0,(void*)0,&g_114},{(void*)0,&l_112[3],&l_112[6],&l_112[3],&g_114,(void*)0,&g_114,&l_112[3]},{&l_112[3],&l_112[3],&g_114,&l_112[6],(void*)0,&g_114,&g_114,(void*)0},{(void*)0,&l_112[6],&l_112[6],(void*)0,&g_114,&g_114,(void*)0,(void*)0},{&g_114,&g_114,(void*)0,&g_114,(void*)0,(void*)0,&g_114,(void*)0}};
        int8_t ***l_144 = &l_143[4][0];
        int16_t *l_147 = &g_148;
        int64_t l_149 = 0x31D0C905CCA37329LL;
        int i, j;
        for (i = 0; i < 5; i++)
            l_119[i] = 0xFFEB3B2BL;
        l_117 = ((void*)0 != &g_114);
        l_130++;
        p_40 = (((*l_108) == (safe_mul_func_float_f_f(((((*l_147) = (((*l_108) & (safe_lshift_func_uint16_t_u_u(((*l_115) & (safe_mod_func_int16_t_s_s(((g_116 & (safe_sub_func_int8_t_s_s((g_146[6] &= (g_61 &= (0x75425EB3L | (((((safe_add_func_int32_t_s_s((((g_7[6][1][0] != (&l_112[3] == ((*l_144) = l_143[5][3]))) , ((void*)0 != &g_7[3][4][0])) && 0xDACF38B7L), 2L)) , (-10L)) >= p_41) == 9L) || g_7[0][4][0])))), p_41))) || 0xA49417E0L), (-1L)))), 2))) | 0UL)) || p_41) , g_118), 0x3.1p-1))) <= l_149);
        for (l_117 = 0; (l_117 < (-24)); l_117 = safe_sub_func_uint32_t_u_u(l_117, 8))
        { /* block id: 28 */
            l_121[4][3] = &g_8;
            (*l_108) &= g_78;
            return &g_8;
        }
    }
    else
    { /* block id: 33 */
        uint8_t l_164 = 0x46L;
        uint8_t l_196 = 8UL;
        int32_t l_197 = 0x55DDAB54L;
        int32_t l_213[5];
        uint8_t *l_231 = &l_196;
        uint8_t **l_230 = &l_231;
        uint8_t l_357 = 0xA0L;
        int32_t *l_361 = &l_123;
        uint16_t * const l_374 = &g_375[0];
        uint16_t * const *l_373[4][5] = {{&l_374,&l_374,&l_374,&l_374,&l_374},{(void*)0,&l_374,(void*)0,&l_374,(void*)0},{&l_374,&l_374,&l_374,&l_374,&l_374},{(void*)0,&l_374,(void*)0,&l_374,(void*)0}};
        uint64_t l_409 = 4UL;
        int8_t **** const l_412[1] = {&g_248};
        int i, j;
        for (i = 0; i < 5; i++)
            l_213[i] = 0x079C38A4L;
        for (g_78 = (-1); (g_78 < 48); g_78 = safe_add_func_int32_t_s_s(g_78, 8))
        { /* block id: 36 */
            uint32_t l_165 = 0UL;
            int32_t l_214 = (-1L);
            int32_t l_215 = 1L;
            int32_t l_216 = 0x7278546DL;
            int32_t l_218 = (-1L);
            int32_t l_262 = 0L;
            int32_t l_263 = 0xA2B8D20EL;
            int32_t l_264 = (-8L);
            int32_t l_265 = 5L;
            int32_t l_266[10] = {0x2169EFB6L,0x2C66DAE0L,0x2C66DAE0L,0x2169EFB6L,0L,0x2169EFB6L,0x2C66DAE0L,0x2C66DAE0L,0x2169EFB6L,0L};
            uint64_t l_286 = 1UL;
            float * const l_287 = &g_145;
            int32_t l_320 = 7L;
            uint8_t *l_339 = &l_196;
            uint16_t * const l_377 = &g_378;
            uint16_t * const *l_376 = &l_377;
            int i;
            if ((safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u(g_146[6], ((9L <= p_41) >= ((g_146[6] , (safe_mul_func_int8_t_s_s(((((safe_rshift_func_int8_t_s_u(((((4UL >= (l_164 = (0UL > (safe_mul_func_uint16_t_u_u((1UL < ((void*)0 == &g_148)), (-1L)))))) & p_41) ^ p_41) , (-8L)), 4)) & 0x23L) & l_165) > 1L), 0x70L))) < p_41)))), 0x5ADCE77D4040057CLL)))
            { /* block id: 38 */
                const int16_t *l_171 = (void*)0;
                const int16_t **l_170 = &l_171;
                float *l_177 = &g_145;
                int32_t l_207[1];
                uint8_t *l_229[10] = {&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2,&g_192.f2};
                uint8_t **l_228 = &l_229[3];
                const int8_t *l_243 = &g_61;
                const int8_t **l_242 = &l_243;
                int32_t l_261 = (-1L);
                uint64_t l_267 = 18446744073709551615UL;
                int i;
                for (i = 0; i < 1; i++)
                    l_207[i] = 1L;
                if (((safe_div_func_float_f_f((safe_add_func_float_f_f((p_40 = ((((*l_170) = &g_148) != &g_148) <= (+(safe_add_func_float_f_f(0x8.E054C2p+54, (safe_add_func_float_f_f((((((*l_177) = l_164) <= p_41) >= (safe_div_func_float_f_f(((((*l_115) = (safe_lshift_func_int16_t_s_s((g_148 = (~(((safe_lshift_func_uint16_t_u_u(p_41, 15)) >= ((((g_61 == (((*l_115) >= (safe_sub_func_int32_t_s_s(l_165, l_164))) <= g_8)) <= p_41) || 0L) , p_41)) > l_165))), g_146[4]))) < l_165) , l_164), l_164))) >= g_146[6]), 0xB.4F4348p-51))))))), g_78)), (-0x1.8p-1))) , (-1L)))
                { /* block id: 44 */
                    int32_t *l_193 = &g_8;
                    int32_t l_212 = 0x7BBF4A9AL;
                    int32_t l_217 = 4L;
                    uint32_t l_241[3][2][3] = {{{1UL,0x6B97F857L,1UL},{7UL,7UL,0x88D53BACL}},{{1UL,0x6B97F857L,1UL},{7UL,0x88D53BACL,0x88D53BACL}},{{1UL,0x6B97F857L,1UL},{7UL,7UL,0x88D53BACL}}};
                    int32_t l_258 = 0x81C1D833L;
                    int32_t l_259 = 0xDDF52832L;
                    int32_t l_260[10][2][2] = {{{0x0468DE32L,(-1L)},{0xFF09BE1AL,0xA1A9F441L}},{{(-1L),0xF3BF57D7L},{(-1L),0xA1A9F441L}},{{0xFF09BE1AL,(-1L)},{0x0468DE32L,(-1L)}},{{0xFF09BE1AL,0xA1A9F441L},{(-1L),0xF3BF57D7L}},{{(-1L),0xA1A9F441L},{0xFF09BE1AL,(-1L)}},{{0x0468DE32L,(-1L)},{0xFF09BE1AL,0xA1A9F441L}},{{(-1L),0xF3BF57D7L},{(-1L),0xA1A9F441L}},{{0xFF09BE1AL,(-1L)},{0x0468DE32L,(-1L)}},{{0xFF09BE1AL,0xA1A9F441L},{(-1L),0xF3BF57D7L}},{{(-1L),0xA1A9F441L},{0xFF09BE1AL,(-1L)}}};
                    int i, j, k;
                    for (l_122 = 0; (l_122 >= (-26)); --l_122)
                    { /* block id: 47 */
                        int32_t **l_190[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_190[i] = &l_108;
                        l_197 = (((((*l_177) = (-((((g_61 , (((g_191[1][0][1] = (l_108 = &l_127)) != (g_192 , l_193)) ^ g_7[6][2][0])) || (p_41 , ((*l_108) && ((safe_rshift_func_uint16_t_u_s(p_41, (65529UL < g_7[7][3][0]))) ^ (*l_193))))) && l_196) , g_192.f0))) != 0xC.A5C417p+98) > 0x8.1p-1) != p_40);
                        return &g_7[7][3][0];
                    }
                    for (l_127 = (-25); (l_127 <= (-1)); l_127 = safe_add_func_int16_t_s_s(l_127, 5))
                    { /* block id: 56 */
                        int32_t l_204 = 0xB55F120CL;
                        uint64_t *l_205[10] = {(void*)0,&g_206,(void*)0,&g_206,(void*)0,&g_206,(void*)0,&g_206,(void*)0,&g_206};
                        int32_t *l_208 = &g_7[7][3][0];
                        int32_t *l_209 = &l_197;
                        int32_t *l_210 = &l_122;
                        int32_t *l_211[9] = {&l_207[0],&l_204,&l_207[0],&l_207[0],&l_204,&l_207[0],&l_207[0],&l_204,&l_207[0]};
                        int i;
                        (*l_108) &= ((safe_div_func_uint32_t_u_u(p_41, (((safe_mod_func_uint64_t_u_u((((void*)0 != &l_193) & (0x8952L & (-1L))), l_204)) ^ ((p_41 && (-1L)) , ((g_206 = 18446744073709551615UL) >= (-1L)))) , p_41))) > g_192.f2);
                        if (l_207[0])
                            continue;
                        l_219--;
                    }
                    for (l_214 = (-22); (l_214 == 12); l_214++)
                    { /* block id: 64 */
                        int8_t ***l_244 = &l_113[0];
                        uint32_t *l_245[1][9][6] = {{{&l_165,&l_165,&l_241[0][1][2],&l_241[2][0][0],&l_241[0][1][2],&l_165},{&l_241[0][1][2],(void*)0,&l_241[2][0][0],&l_241[2][0][0],(void*)0,&l_241[0][1][2]},{&l_165,&l_241[0][1][2],&l_241[2][0][0],&l_241[0][1][2],&l_165,&l_165},{(void*)0,&l_241[0][1][2],&l_241[0][1][2],(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_241[0][1][2],&l_241[0][1][2],(void*)0},{&l_165,&l_165,&l_241[0][1][2],&l_241[2][0][0],&l_241[0][1][2],&l_165},{&l_241[0][1][2],(void*)0,&l_241[2][0][0],&l_241[2][0][0],(void*)0,&l_241[0][1][2]},{&l_165,&l_241[0][1][2],&l_241[2][0][0],&l_241[0][1][2],&l_165,&l_165},{(void*)0,&l_241[0][1][2],&l_241[0][1][2],(void*)0,(void*)0,(void*)0}}};
                        int32_t l_246 = (-5L);
                        int32_t *l_253 = &g_7[7][3][0];
                        int32_t *l_254 = (void*)0;
                        int32_t *l_255 = &l_120;
                        int32_t *l_256 = (void*)0;
                        int32_t *l_257[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_257[i] = &g_7[2][2][0];
                        (*l_108) |= (((safe_sub_func_uint32_t_u_u(g_146[0], 4294967295UL)) , (safe_div_func_uint16_t_u_u(((l_228 != l_230) ^ (-9L)), ((l_232 , (p_41 <= ((((((safe_lshift_func_int16_t_s_s(g_8, (((safe_lshift_func_int16_t_s_u((((((safe_div_func_int8_t_s_s((safe_lshift_func_int16_t_s_s(0x4DDBL, 15)), l_196)) >= 18446744073709551615UL) || 0xFFD9DDA32BF4E470LL) <= g_146[6]) == l_241[2][0][0]), l_165)) == l_207[0]) ^ g_116))) & g_146[0]) >= l_207[0]) <= p_41) < 0x70L) != g_192.f0))) , (-2L))))) && 4294967295UL);
                        (*l_108) |= (((*l_115) ^ (l_242 != ((*l_244) = &l_112[3]))) > ((l_218 = (0x58D98CD050AACBACLL || (l_246 |= (g_61 ^ (l_215 &= (*l_193)))))) || (p_41 , p_41)));
                        (*g_252) = g_247[1][0][2];
                        l_267--;
                    }
                }
                else
                { /* block id: 74 */
                    int32_t l_273 = 0xDBCD6006L;
                    int8_t ****l_277 = &g_248;
                    int32_t l_279[5][2][7] = {{{(-1L),(-2L),0x6C73E762L,(-2L),(-1L),0x0E587754L,0x0E587754L},{0xD3A3C413L,(-5L),(-1L),(-5L),0xD3A3C413L,(-6L),(-1L)}},{{0x0E587754L,9L,0x5E0CF097L,9L,0x0E587754L,0x6C73E762L,0x6C73E762L},{0xA375466DL,0x577BA112L,0xD604C6C6L,0x577BA112L,0xA375466DL,0xDDEAE75FL,(-1L)}},{{0x0E587754L,9L,0x5E0CF097L,9L,0x0E587754L,0x6C73E762L,0x6C73E762L},{0xA375466DL,0x577BA112L,0xD604C6C6L,0x577BA112L,0xA375466DL,0xDDEAE75FL,(-1L)}},{{0x0E587754L,9L,0x5E0CF097L,9L,0x0E587754L,0x6C73E762L,0x6C73E762L},{0xA375466DL,0x577BA112L,0xD604C6C6L,0x577BA112L,0xA375466DL,0xDDEAE75FL,(-1L)}},{{0x0E587754L,9L,0x5E0CF097L,9L,0x0E587754L,0x6C73E762L,0x6C73E762L},{0xA375466DL,0x577BA112L,0xD604C6C6L,0x577BA112L,0xA375466DL,0xDDEAE75FL,(-1L)}}};
                    int32_t l_288 = 0xF24BCD46L;
                    int i, j, k;
                    for (l_120 = 0; (l_120 <= 5); l_120 = safe_add_func_int16_t_s_s(l_120, 1))
                    { /* block id: 77 */
                        float *l_274 = &g_145;
                        uint16_t *l_285 = &l_232;
                        l_288 ^= ((!((p_41 || l_273) <= p_41)) ^ ((l_274 != (((safe_sub_func_uint64_t_u_u((18446744073709551615UL || ((void*)0 != l_277)), ((~((*l_285) = ((l_279[3][1][4] = g_148) > ((((safe_lshift_func_int8_t_s_s((safe_lshift_func_int16_t_s_s(((18446744073709551615UL ^ p_41) || g_7[4][1][0]), g_148)), (*l_115))) || p_41) <= g_284) , p_41)))) != l_286))) & (*l_115)) , l_287)) & 0xB0A4C5BDL));
                        return l_274;
                    }
                }
            }
            else
            { /* block id: 84 */
                int8_t **l_300 = (void*)0;
                int32_t l_309 = 0xD6F59A46L;
                uint32_t *l_315[9] = {&g_284,&g_284,&l_165,&g_284,&g_284,&l_165,&g_284,&g_284,&l_165};
                uint8_t *l_340[7][7] = {{&g_192.f2,&l_196,&l_196,&g_192.f2,(void*)0,&g_78,&g_192.f2},{&g_192.f2,&l_164,(void*)0,&l_164,&l_164,(void*)0,&l_164},{(void*)0,&g_192.f2,(void*)0,&l_164,&l_196,&g_192.f2,&g_192.f2},{&l_164,&g_192.f2,&l_164,&g_192.f2,&l_164,&g_78,&l_164},{&l_196,&g_78,&g_192.f2,&l_164,&g_192.f2,&l_164,&g_192.f2},{&l_164,&l_164,&g_78,&l_164,&l_164,&g_78,(void*)0},{&l_196,&l_164,&g_192.f2,&g_192.f2,&l_164,&l_196,&g_192.f2}};
                int32_t l_346 = (-9L);
                int32_t l_347 = 0xB75FA674L;
                int32_t l_348[4] = {0xFE5E05E8L,0xFE5E05E8L,0xFE5E05E8L,0xFE5E05E8L};
                int32_t *l_390 = &l_124;
                int32_t *l_391 = &l_266[8];
                float l_392 = 0x4.78E618p-35;
                int32_t *l_393 = &l_264;
                int32_t *l_394 = &l_348[2];
                int32_t *l_395 = &l_263;
                int32_t *l_396 = &l_128[0][3];
                int32_t *l_397 = &l_127;
                int32_t *l_398 = &l_214;
                int32_t *l_399 = (void*)0;
                int32_t *l_400 = &g_7[7][4][0];
                int32_t *l_401 = &l_128[0][7];
                int32_t *l_402 = &l_120;
                int32_t *l_403 = (void*)0;
                int32_t *l_404 = &l_346;
                int32_t *l_405 = &l_309;
                int32_t *l_406[6] = {&l_218,&l_123,&l_218,&l_218,&l_123,&l_218};
                int32_t l_407 = 0x4F8CA952L;
                int i, j;
                g_289 = g_289;
                if ((safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u((safe_sub_func_int32_t_s_s((safe_add_func_int16_t_s_s(((l_300 != (void*)0) ^ (g_206 = (0x63A6L && (*g_290)))), 0xC080L)), (safe_mod_func_int64_t_s_s(((((safe_lshift_func_uint16_t_u_s((((safe_div_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u(g_78, l_264)), (l_309 || (((g_284 = ((safe_lshift_func_int16_t_s_u((((l_314 = (((safe_mul_func_uint8_t_u_u(p_41, 255UL)) && p_41) != (*l_115))) , 4294967295UL) || (*l_108)), 15)) == 18446744073709551609UL)) , 0xE2A3B59A8C30F78DLL) , 65528UL)))) >= g_291[0][1]) > p_41), 14)) , &l_309) != (void*)0) && l_215), g_146[6])))), p_41)), g_148)))
                { /* block id: 89 */
                    int32_t *l_316[6] = {&l_262,&l_262,&l_262,&l_262,&l_262,&l_262};
                    int8_t l_349 = 0x1DL;
                    uint64_t l_350 = 0UL;
                    int i;
                    g_317++;
                    if (p_41)
                        continue;
                    if (l_216)
                    { /* block id: 92 */
                        uint32_t l_323 = 18446744073709551615UL;
                        (*l_108) = (l_323 = (l_320 , (g_206 & (safe_mod_func_uint8_t_u_u(((*l_231) = p_41), (p_41 & p_41))))));
                    }
                    else
                    { /* block id: 96 */
                        int64_t *l_337[1][10] = {{&g_146[3],&g_146[3],&g_146[3],&g_146[3],&g_146[3],&g_146[3],&g_146[3],&g_146[3],&g_146[3],&g_146[3]}};
                        int16_t *l_338[5][3] = {{&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148},{&g_148,&g_148,(void*)0},{&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148}};
                        int32_t **l_341 = &l_316[1];
                        int32_t l_342 = 0x8BB21BDCL;
                        int32_t l_343 = (-1L);
                        int32_t l_344 = 0x296DCEA9L;
                        int32_t l_345[6][3][7];
                        int32_t **l_353 = &l_115;
                        int i, j, k;
                        for (i = 0; i < 6; i++)
                        {
                            for (j = 0; j < 3; j++)
                            {
                                for (k = 0; k < 7; k++)
                                    l_345[i][j][k] = (-3L);
                            }
                        }
                        (*l_341) = (((((*g_290) , l_112[3]) == ((*l_230) = (l_340[4][3] = ((((*l_287) = (((l_324 <= ((safe_div_func_float_f_f((safe_div_func_float_f_f((((g_192.f2 & (safe_lshift_func_int16_t_s_u(g_61, (safe_mod_func_int16_t_s_s((l_266[8] &= (0xAFF6A0F1L | ((p_41 > (safe_mod_func_uint64_t_u_u(g_291[4][3], ((*l_115) = (safe_mod_func_int8_t_s_s(l_197, g_78)))))) ^ g_7[7][3][0]))), l_196))))) , p_41) , l_197), (-0x1.Ap+1))), g_192.f1)) >= l_265)) != p_41) >= p_41)) , 6UL) , l_339)))) , p_41) , (void*)0);
                        l_350--;
                        (*l_353) = ((*l_341) = &l_197);
                    }
                }
                else
                { /* block id: 107 */
                    int32_t *l_354 = (void*)0;
                    int32_t *l_355 = &l_128[0][3];
                    int32_t *l_356[5];
                    int32_t **l_360[3];
                    uint16_t * const **l_372[9] = {&g_370,&g_370,&g_370,&g_370,&g_370,&g_370,&g_370,&g_370,&g_370};
                    int8_t *l_388[6] = {(void*)0,(void*)0,&g_192.f1,(void*)0,(void*)0,&g_192.f1};
                    uint64_t *l_389 = &g_206;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_356[i] = &l_122;
                    for (i = 0; i < 3; i++)
                        l_360[i] = &g_191[1][0][1];
                    --l_357;
                    l_361 = l_355;
                    l_348[3] = ((safe_mul_func_int8_t_s_s((safe_div_func_int32_t_s_s((safe_div_func_uint64_t_u_u(((safe_lshift_func_int16_t_s_s(g_291[4][3], 6)) ^ ((*l_361) = (((((*l_389) = (((l_376 = (g_192 , (l_373[1][4] = g_370))) == ((((safe_div_func_uint32_t_u_u(((safe_mod_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_s((~(g_8 <= (p_41 , (((*l_108) |= ((**l_230) = 0x94L)) ^ (l_346 = ((safe_mul_func_int8_t_s_s(p_41, (((((void*)0 == l_388[0]) , g_375[3]) & 0L) & (-1L)))) & 0xE822L)))))), p_41)) >= 0xB35BL), 0xB9280CF39F880B69LL)) != p_41), p_41)) , (void*)0) != (void*)0) , &g_371)) <= l_286)) == p_41) | (-5L)) != 0x52L))), g_284)), g_116)), 1UL)) , p_41);
                }
                ++l_409;
            }
            (*l_108) = (((l_412[0] != l_412[0]) , &l_232) != ((safe_mod_func_uint32_t_u_u((*l_361), (safe_sub_func_int32_t_s_s((safe_rshift_func_int8_t_s_u((*l_361), 4)), (g_419 , (0x9CE0L != p_41)))))) , (*g_289)));
        }
    }
    return &g_116;
}


/* ------------------------------------------ */
/* 
 * reads : g_78 g_8 g_85 g_61 g_7
 * writes: g_78
 */
static float  func_46(int32_t * const  p_47, uint16_t  p_48, int32_t * p_49)
{ /* block id: 7 */
    int8_t l_76 = 0xEDL;
    uint8_t *l_77 = &g_78;
    int32_t *l_94 = &g_7[7][2][0];
    int32_t **l_93 = &l_94;
    const uint8_t **l_95 = (void*)0;
    const uint8_t *l_97 = (void*)0;
    const uint8_t **l_96 = &l_97;
    int8_t *l_98 = &l_76;
    int64_t l_99 = 0x783651A330C13F5ELL;
    int32_t l_100 = (-1L);
    l_100 ^= (p_48 < (&g_61 == ((safe_add_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((18446744073709551613UL > ((safe_mod_func_uint64_t_u_u((safe_mod_func_int64_t_s_s(((safe_div_func_int8_t_s_s(((*l_98) = (((((--(*l_77)) & ((safe_rshift_func_uint8_t_u_s((safe_add_func_uint8_t_u_u(g_8, 5L)), 3)) == (g_85[0][3] != ((*l_96) = ((safe_sub_func_int8_t_s_s((safe_mod_func_int64_t_s_s(g_61, (((safe_unary_minus_func_int16_t_s((safe_sub_func_uint32_t_u_u(0xADB298F9L, (((*l_93) = &g_7[7][3][0]) == (void*)0))))) && p_48) , 6UL))), 255UL)) , (void*)0))))) == (*p_49)) & 0x941AE2D2L) , 1L)), g_7[2][4][0])) , (-1L)), g_8)), p_48)) <= p_48)), l_99)), 0xD4L)) , (void*)0)));
    return (**l_93);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_7[i][j][k], "g_7[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_61, "g_61", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_118, "g_118", print_hash_value);
    transparent_crc_bytes (&g_145, sizeof(g_145), "g_145", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_146[i], "g_146[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_148, "g_148", print_hash_value);
    transparent_crc(g_192.f0, "g_192.f0", print_hash_value);
    transparent_crc(g_192.f1, "g_192.f1", print_hash_value);
    transparent_crc(g_192.f2, "g_192.f2", print_hash_value);
    transparent_crc(g_206, "g_206", print_hash_value);
    transparent_crc(g_284, "g_284", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_291[i][j], "g_291[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_317, "g_317", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_375[i], "g_375[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_378, "g_378", print_hash_value);
    transparent_crc(g_419.f0, "g_419.f0", print_hash_value);
    transparent_crc(g_419.f1, "g_419.f1", print_hash_value);
    transparent_crc(g_419.f2, "g_419.f2", print_hash_value);
    transparent_crc(g_454, "g_454", print_hash_value);
    transparent_crc_bytes (&g_456, sizeof(g_456), "g_456", print_hash_value);
    transparent_crc(g_464.f0, "g_464.f0", print_hash_value);
    transparent_crc(g_464.f1, "g_464.f1", print_hash_value);
    transparent_crc(g_464.f2, "g_464.f2", print_hash_value);
    transparent_crc(g_475.f0, "g_475.f0", print_hash_value);
    transparent_crc(g_475.f1, "g_475.f1", print_hash_value);
    transparent_crc(g_475.f2, "g_475.f2", print_hash_value);
    transparent_crc(g_648, "g_648", print_hash_value);
    transparent_crc(g_696, "g_696", print_hash_value);
    transparent_crc(g_759, "g_759", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_890[i], "g_890[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_974, "g_974", print_hash_value);
    transparent_crc(g_978, "g_978", print_hash_value);
    transparent_crc(g_1043, "g_1043", print_hash_value);
    transparent_crc(g_1081, "g_1081", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1151[i], "g_1151[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1295, "g_1295", print_hash_value);
    transparent_crc(g_1330, "g_1330", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1332[i], "g_1332[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1663, "g_1663", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1664[i], "g_1664[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1712, "g_1712", print_hash_value);
    transparent_crc(g_1717.f0, "g_1717.f0", print_hash_value);
    transparent_crc(g_1717.f1, "g_1717.f1", print_hash_value);
    transparent_crc(g_1717.f2, "g_1717.f2", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1723[i], "g_1723[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1858, "g_1858", print_hash_value);
    transparent_crc(g_1865.f0, "g_1865.f0", print_hash_value);
    transparent_crc(g_1865.f1, "g_1865.f1", print_hash_value);
    transparent_crc(g_1865.f2, "g_1865.f2", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 498
XXX total union variables: 6

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 179
   depth: 2, occurrence: 47
   depth: 3, occurrence: 3
   depth: 4, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 3
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 2
   depth: 16, occurrence: 5
   depth: 17, occurrence: 2
   depth: 18, occurrence: 3
   depth: 19, occurrence: 1
   depth: 20, occurrence: 2
   depth: 21, occurrence: 1
   depth: 22, occurrence: 3
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 4
   depth: 28, occurrence: 4
   depth: 29, occurrence: 3
   depth: 30, occurrence: 5
   depth: 31, occurrence: 1
   depth: 33, occurrence: 2
   depth: 37, occurrence: 2
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1

XXX total number of pointers: 407

XXX times a variable address is taken: 1054
XXX times a pointer is dereferenced on RHS: 163
breakdown:
   depth: 1, occurrence: 135
   depth: 2, occurrence: 24
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 187
breakdown:
   depth: 1, occurrence: 179
   depth: 2, occurrence: 5
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 32
XXX times a pointer is compared with address of another variable: 5
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 6546

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 722
   level: 2, occurrence: 202
   level: 3, occurrence: 70
   level: 4, occurrence: 28
   level: 5, occurrence: 15
XXX number of pointers point to pointers: 171
XXX number of pointers point to scalars: 236
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28
XXX average alias set size: 1.39

XXX times a non-volatile is read: 1272
XXX times a non-volatile is write: 591
XXX times a volatile is read: 80
XXX    times read thru a pointer: 34
XXX times a volatile is write: 21
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 4.39e+03
XXX percentage of non-volatile access: 94.9

XXX forward jumps: 0
XXX backward jumps: 2

XXX stmts: 187
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 24
   depth: 1, occurrence: 23
   depth: 2, occurrence: 25
   depth: 3, occurrence: 29
   depth: 4, occurrence: 38
   depth: 5, occurrence: 48

XXX percentage a fresh-made variable is used: 17.3
XXX percentage an existing variable is used: 82.7
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1546265134
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static const uint8_t g_6 = 0xF9L;
static int32_t g_18 = 0xE28FDEB7L;
static volatile int32_t g_20 = 1L;/* VOLATILE GLOBAL g_20 */
static volatile uint64_t g_21 = 1UL;/* VOLATILE GLOBAL g_21 */
static uint8_t g_56 = 0x6FL;
static int32_t g_58 = (-2L);
static int32_t * volatile g_57 = &g_58;/* VOLATILE GLOBAL g_57 */
static uint32_t g_89 = 0x13E67DCAL;
static float g_92 = 0x0.5p+1;
static uint16_t g_93 = 9UL;
static uint16_t g_95 = 1UL;
static int32_t g_96 = 0xA00AF408L;
static int16_t g_99[7][3][5] = {{{0xEAD7L,0x20F6L,0xEAD7L,0xEAD7L,0x20F6L},{(-5L),0x0CFAL,0x12D9L,0x2BF4L,0x12D9L},{0x20F6L,0x20F6L,0xA363L,0x20F6L,0x20F6L}},{{0x12D9L,0x2BF4L,0x12D9L,0x0CFAL,(-5L)},{0x20F6L,0xEAD7L,0xEAD7L,0x20F6L,0xEAD7L},{(-5L),0x2BF4L,7L,0x2BF4L,(-5L)}},{{0xEAD7L,0x20F6L,0xEAD7L,0xEAD7L,0x20F6L},{(-5L),0x0CFAL,0x12D9L,0x2BF4L,0x12D9L},{0x20F6L,0x20F6L,0xA363L,0x20F6L,0x20F6L}},{{0x12D9L,0x0CFAL,7L,0L,0x12D9L},{0xEAD7L,0xA363L,0xA363L,0xEAD7L,0xA363L},{0x12D9L,0x0CFAL,(-5L),0x0CFAL,0x12D9L}},{{0xA363L,0xEAD7L,0xA363L,0xA363L,0xEAD7L},{0x12D9L,0L,7L,0x0CFAL,7L},{0xEAD7L,0xEAD7L,0x20F6L,0xEAD7L,0xEAD7L}},{{7L,0x0CFAL,7L,0L,0x12D9L},{0xEAD7L,0xA363L,0xA363L,0xEAD7L,0xA363L},{0x12D9L,0x0CFAL,(-5L),0x0CFAL,0x12D9L}},{{0xA363L,0xEAD7L,0xA363L,0xA363L,0xEAD7L},{0x12D9L,0L,7L,0x0CFAL,7L},{0xEAD7L,0xEAD7L,0x20F6L,0xEAD7L,0xEAD7L}}};
static int64_t g_100[8][8] = {{4L,1L,0L,0x23B15C07A264E934LL,(-1L),1L,(-1L),0x23B15C07A264E934LL},{0x4349BA410498C5D2LL,1L,0x4349BA410498C5D2LL,1L,4L,0xC225C50F45F4D98BLL,(-1L),0xC225C50F45F4D98BLL},{0xA5FDE22B755D73B7LL,1L,0L,1L,0xA5FDE22B755D73B7LL,0x00BAF9C35C0EDEAELL,4L,0x23B15C07A264E934LL},{0xA5FDE22B755D73B7LL,0x00BAF9C35C0EDEAELL,4L,0x23B15C07A264E934LL,4L,0x00BAF9C35C0EDEAELL,0xA5FDE22B755D73B7LL,1L},{0x4349BA410498C5D2LL,1L,4L,0xC225C50F45F4D98BLL,(-1L),0xC225C50F45F4D98BLL,4L,1L},{4L,1L,0L,0x23B15C07A264E934LL,(-1L),1L,(-1L),0x23B15C07A264E934LL},{0x4349BA410498C5D2LL,1L,0x4349BA410498C5D2LL,1L,4L,0xC225C50F45F4D98BLL,(-1L),0xC225C50F45F4D98BLL},{0xA5FDE22B755D73B7LL,1L,0L,1L,0xA5FDE22B755D73B7LL,0x00BAF9C35C0EDEAELL,4L,0x23B15C07A264E934LL}};
static volatile uint32_t g_102 = 0xEE7DBBECL;/* VOLATILE GLOBAL g_102 */
static uint64_t g_122 = 18446744073709551615UL;
static uint64_t g_125 = 0xA46AFD11974AA164LL;
static volatile uint64_t g_140[9][6] = {{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,0UL,1UL}};
static volatile uint64_t *g_139 = &g_140[8][2];
static volatile uint64_t * volatile *g_138 = &g_139;
static int64_t g_141 = 0x8AC17F9EEEF0C4AFLL;
static uint64_t g_144 = 0xA64128449A3136B1LL;
static volatile int32_t g_162 = 0xDD512011L;/* VOLATILE GLOBAL g_162 */
static uint16_t g_163 = 0UL;
static float g_193 = (-0x1.Fp+1);
static float * volatile g_192 = &g_193;/* VOLATILE GLOBAL g_192 */
static volatile uint32_t g_206 = 1UL;/* VOLATILE GLOBAL g_206 */
static float g_245 = 0x1.33BA4Ap-95;
static float * volatile g_244 = &g_245;/* VOLATILE GLOBAL g_244 */
static float g_257 = 0x1.1p+1;
static int16_t g_278 = 0x91A4L;
static int32_t g_281 = 0xD0A5A646L;
static int32_t *g_282 = &g_281;
static int32_t *g_283[3][10] = {{(void*)0,&g_58,(void*)0,&g_58,(void*)0,&g_58,(void*)0,&g_58,(void*)0,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58},{(void*)0,&g_58,(void*)0,&g_58,(void*)0,&g_58,(void*)0,&g_58,(void*)0,&g_58}};
static uint32_t *g_309 = &g_89;
static uint32_t **g_308 = &g_309;
static uint8_t * volatile g_328 = &g_56;/* VOLATILE GLOBAL g_328 */
static uint8_t * volatile *g_327 = &g_328;
static uint8_t * volatile **g_326 = &g_327;
static uint8_t * volatile *** volatile g_325 = &g_326;/* VOLATILE GLOBAL g_325 */
static int8_t g_343[1] = {0x0AL};
static const int32_t g_349 = 1L;
static int32_t ** volatile g_519 = &g_282;/* VOLATILE GLOBAL g_519 */
static int32_t ** volatile *g_518 = &g_519;
static uint8_t g_535 = 253UL;
static volatile uint8_t g_539[4][10] = {{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL}};
static volatile uint8_t * volatile g_538 = &g_539[1][8];/* VOLATILE GLOBAL g_538 */
static volatile uint8_t * volatile *g_537 = &g_538;
static volatile uint8_t * volatile * volatile *g_536 = &g_537;
static uint32_t g_548 = 4294967293UL;
static int16_t g_631 = 0x9FBFL;
static uint16_t g_635 = 65535UL;
static int32_t **g_656 = &g_282;
static int32_t ***g_655 = &g_656;
static const int32_t **g_697 = (void*)0;
static int16_t g_703 = (-2L);
static int32_t g_802[7] = {0xB19DFED0L,0xB19DFED0L,0xB19DFED0L,0xB19DFED0L,0xB19DFED0L,0xB19DFED0L,0xB19DFED0L};
static float g_811 = 0xB.2B762Ep-23;
static int32_t g_861 = 1L;
static float *g_873 = &g_811;
static float **g_872 = &g_873;
static float ***g_871 = &g_872;
static uint8_t *****g_883 = (void*)0;
static const int8_t g_938 = 0L;
static const int32_t *g_965 = &g_861;
static int8_t * const * const  volatile g_980 = (void*)0;/* VOLATILE GLOBAL g_980 */
static uint16_t g_989 = 1UL;
static uint32_t g_1009[9] = {0xED039740L,0xED039740L,0xED039740L,0xED039740L,0xED039740L,0xED039740L,0xED039740L,0xED039740L,0xED039740L};
static uint8_t ** const g_1020 = (void*)0;
static uint8_t ** const *g_1019[5][7] = {{&g_1020,&g_1020,&g_1020,&g_1020,&g_1020,(void*)0,&g_1020},{&g_1020,(void*)0,&g_1020,(void*)0,&g_1020,(void*)0,&g_1020},{&g_1020,&g_1020,&g_1020,&g_1020,(void*)0,&g_1020,&g_1020},{&g_1020,(void*)0,&g_1020,&g_1020,(void*)0,&g_1020,&g_1020},{&g_1020,&g_1020,&g_1020,&g_1020,(void*)0,(void*)0,&g_1020}};
static uint8_t ** const **g_1018[6][4] = {{&g_1019[3][3],(void*)0,&g_1019[3][3],&g_1019[3][3]},{(void*)0,(void*)0,&g_1019[3][3],(void*)0},{(void*)0,&g_1019[3][3],&g_1019[3][3],(void*)0},{&g_1019[3][3],(void*)0,&g_1019[3][3],&g_1019[3][3]},{(void*)0,(void*)0,&g_1019[3][3],(void*)0},{(void*)0,&g_1019[3][3],&g_1019[3][3],(void*)0}};
static int32_t **g_1042[5][8] = {{&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9]},{&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9]},{&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9]},{&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9]},{&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9],&g_283[0][4],&g_283[1][9]}};
static int32_t ***g_1041 = &g_1042[1][4];
static int16_t *g_1052[7] = {&g_703,&g_703,(void*)0,&g_703,&g_703,(void*)0,&g_703};
static volatile uint16_t g_1058[3][5] = {{0x0DA1L,65535UL,65535UL,0x0DA1L,65535UL},{0x0DA1L,0x0DA1L,0x011AL,0x0DA1L,0x0DA1L},{65535UL,65535UL,0x011AL,0x011AL,65535UL}};
static volatile int64_t g_1152 = 0x70BF2DB391BAD86ALL;/* VOLATILE GLOBAL g_1152 */
static volatile int64_t *g_1151 = &g_1152;
static volatile int64_t **g_1150[3] = {&g_1151,&g_1151,&g_1151};
static int32_t *g_1310 = (void*)0;
static volatile float ** volatile **g_1312 = (void*)0;
static volatile float ** volatile ***g_1311 = &g_1312;
static int8_t g_1327[1] = {0xFDL};
static float g_1361 = 0x4.6p-1;
static const int16_t g_1462 = 0x1B64L;
static const int16_t *g_1461 = &g_1462;
static int32_t g_1540 = 0x678930E9L;
static uint32_t g_1551 = 18446744073709551615UL;
static const uint8_t *g_1559[3][4] = {{&g_535,&g_6,&g_535,&g_535},{&g_6,&g_6,&g_6,&g_6},{&g_6,&g_535,&g_535,&g_6}};
static const uint8_t **g_1558 = &g_1559[1][0];
static const uint8_t ***g_1557 = &g_1558;
static int64_t *g_1633 = &g_100[2][0];
static int64_t **g_1632 = &g_1633;
static int8_t g_1637 = 0x09L;
static const int32_t g_1672 = 0xBE26010DL;
static const int32_t g_1674 = 0L;
static uint32_t g_1686 = 18446744073709551610UL;
static volatile int32_t g_1700 = 0xFA533A19L;/* VOLATILE GLOBAL g_1700 */
static uint32_t g_1724[4] = {0x2AC277A6L,0x2AC277A6L,0x2AC277A6L,0x2AC277A6L};
static int32_t g_1733 = 1L;
static int64_t **g_1749 = &g_1633;
static float * volatile g_1755 = &g_92;/* VOLATILE GLOBAL g_1755 */
static int32_t ** volatile g_1757 = &g_283[0][4];/* VOLATILE GLOBAL g_1757 */
static const uint64_t * const g_1848 = (void*)0;
static const uint64_t * const *g_1847 = &g_1848;
static volatile uint64_t g_1887 = 0UL;/* VOLATILE GLOBAL g_1887 */
static uint32_t *** volatile g_1926 = (void*)0;/* VOLATILE GLOBAL g_1926 */
static uint32_t *g_1934 = &g_1686;
static uint32_t **g_2114 = (void*)0;
static int64_t ***g_2119 = &g_1632;
static int16_t **g_2135 = &g_1052[1];
static int16_t ***g_2134 = &g_2135;
static int32_t **g_2219[3][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
static int32_t *** volatile g_2218[5][4] = {{&g_2219[2][1],&g_2219[2][1],&g_2219[2][1],&g_2219[2][1]},{&g_2219[2][1],&g_2219[2][1],(void*)0,&g_2219[2][1]},{&g_2219[2][1],&g_2219[0][1],(void*)0,(void*)0},{&g_2219[2][1],&g_2219[2][1],&g_2219[2][1],(void*)0},{&g_2219[2][1],&g_2219[0][1],&g_2219[2][1],&g_2219[2][1]}};
static int32_t *** volatile g_2220[1] = {&g_2219[2][1]};
static int32_t *** volatile g_2221 = &g_2219[2][1];/* VOLATILE GLOBAL g_2221 */
static const uint8_t ****g_2247 = &g_1557;
static const uint8_t *****g_2246 = &g_2247;
static const int8_t g_2249[6] = {0xB5L,0xB5L,0xB5L,0xB5L,0xB5L,0xB5L};
static volatile uint64_t * const * const g_2266 = &g_139;
static volatile uint64_t * const * const  volatile *g_2265 = &g_2266;
static volatile uint64_t * const * const  volatile ** volatile g_2264[6][2] = {{&g_2265,&g_2265},{&g_2265,&g_2265},{&g_2265,&g_2265},{&g_2265,&g_2265},{&g_2265,&g_2265},{&g_2265,&g_2265}};
static volatile int64_t g_2273 = 0x357821E1CBF274DBLL;/* VOLATILE GLOBAL g_2273 */
static int8_t *g_2284[6][4] = {{&g_1327[0],&g_1327[0],&g_1327[0],&g_1327[0]},{&g_1327[0],&g_1327[0],&g_1327[0],&g_1327[0]},{&g_1327[0],&g_1327[0],&g_1327[0],&g_1327[0]},{&g_1327[0],&g_1327[0],&g_1327[0],&g_1327[0]},{&g_1327[0],&g_1327[0],&g_1327[0],&g_1327[0]},{&g_1327[0],&g_1327[0],&g_1327[0],&g_1327[0]}};
static int8_t **g_2283 = &g_2284[3][0];
static int8_t ***g_2282[2][6][9] = {{{&g_2283,&g_2283,&g_2283,(void*)0,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283},{&g_2283,(void*)0,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283},{&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283},{&g_2283,&g_2283,(void*)0,&g_2283,&g_2283,&g_2283,(void*)0,&g_2283,&g_2283},{(void*)0,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,(void*)0},{&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,&g_2283}},{{&g_2283,&g_2283,(void*)0,(void*)0,&g_2283,(void*)0,&g_2283,&g_2283,(void*)0},{&g_2283,&g_2283,&g_2283,&g_2283,&g_2283,(void*)0,&g_2283,(void*)0,&g_2283},{(void*)0,&g_2283,&g_2283,&g_2283,&g_2283,(void*)0,&g_2283,(void*)0,&g_2283},{&g_2283,&g_2283,&g_2283,&g_2283,(void*)0,(void*)0,&g_2283,&g_2283,&g_2283},{&g_2283,(void*)0,&g_2283,&g_2283,&g_2283,(void*)0,&g_2283,&g_2283,(void*)0},{&g_2283,&g_2283,&g_2283,(void*)0,&g_2283,&g_2283,(void*)0,&g_2283,(void*)0}}};
static uint64_t ***g_2333 = (void*)0;
static volatile uint8_t g_2364 = 0x30L;/* VOLATILE GLOBAL g_2364 */
static int32_t *g_2431[3] = {&g_18,&g_18,&g_18};
static int16_t g_2467 = 0L;
static volatile uint32_t * volatile g_2494 = &g_206;/* VOLATILE GLOBAL g_2494 */
static volatile uint32_t * volatile * volatile g_2493[6] = {&g_2494,&g_2494,&g_2494,&g_2494,&g_2494,&g_2494};
static uint32_t ***g_2540 = (void*)0;
static uint8_t *****g_2551 = (void*)0;
static int16_t g_2608 = 0x079FL;
static volatile uint16_t *g_2624[7] = {&g_1058[1][1],&g_1058[1][1],&g_1058[1][1],&g_1058[1][1],&g_1058[1][1],&g_1058[1][1],&g_1058[1][1]};
static volatile uint16_t **g_2623[3] = {&g_2624[6],&g_2624[6],&g_2624[6]};
static int16_t g_2642 = 0x51B4L;
static int32_t g_2740[5] = {0L,0L,0L,0L,0L};
static int16_t * const ** volatile g_2750 = (void*)0;/* VOLATILE GLOBAL g_2750 */
static volatile uint32_t * volatile * volatile * volatile * volatile g_2774 = (void*)0;/* VOLATILE GLOBAL g_2774 */
static volatile uint32_t * volatile * volatile * volatile g_2777[4][6][6] = {{{&g_2493[4],(void*)0,&g_2493[4],&g_2493[4],&g_2493[5],&g_2493[4]},{&g_2493[4],&g_2493[4],(void*)0,&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{(void*)0,&g_2493[4],&g_2493[4],&g_2493[5],&g_2493[5],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[2],(void*)0,&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[1],&g_2493[4],&g_2493[2]}},{{&g_2493[2],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[2],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[5],&g_2493[1],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[2],(void*)0,&g_2493[4],&g_2493[2],&g_2493[4]},{&g_2493[5],&g_2493[2],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]}},{{&g_2493[4],&g_2493[1],(void*)0,&g_2493[4],&g_2493[4],&g_2493[5]},{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[0],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[2],&g_2493[4],&g_2493[4],&g_2493[1],&g_2493[4],&g_2493[5]},{&g_2493[2],&g_2493[4],&g_2493[1],&g_2493[4],&g_2493[4],&g_2493[1]},{&g_2493[0],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[5],&g_2493[4]}},{{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[5],&g_2493[4],&g_2493[1],&g_2493[4],&g_2493[5],&g_2493[4]},{&g_2493[4],(void*)0,&g_2493[4],&g_2493[4],&g_2493[1],&g_2493[4]},{&g_2493[5],(void*)0,&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4]},{&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[4],&g_2493[1]}}};
static const int32_t ***g_2823 = (void*)0;
static const int32_t ****g_2822 = &g_2823;
static uint32_t g_2880 = 0x8716D23CL;
static int32_t * const **g_2900 = (void*)0;
static int32_t g_3013 = 0xB1DACC03L;
static int16_t g_3019[9] = {0x3607L,0x3607L,0x3607L,0x3607L,0x3607L,0x3607L,0x3607L,0x3607L,0x3607L};
static uint64_t *g_3052 = &g_125;
static uint16_t g_3054 = 0x7FB1L;
static const int32_t g_3067[3][1] = {{1L},{1L},{1L}};
static uint8_t g_3093 = 0x58L;
static uint32_t g_3103 = 0UL;
static int32_t g_3111 = 0L;
static int32_t * const **g_3119 = (void*)0;
static int32_t g_3160 = 0x49A7CCD1L;
static uint16_t **g_3209 = (void*)0;
static uint16_t ***g_3208 = &g_3209;


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static const int16_t  func_2(const uint32_t  p_3, float  p_4, uint64_t  p_5);
static int32_t * func_10(int32_t  p_11, uint8_t  p_12);
static int32_t * func_28(int32_t * p_29, int32_t * p_30, int32_t  p_31, uint32_t  p_32, const float  p_33);
static float  func_37(int8_t  p_38, int32_t * p_39, int32_t * p_40, uint64_t  p_41);
static int32_t * func_42(uint8_t  p_43, uint8_t  p_44, int32_t * const  p_45, int32_t * p_46, int32_t * p_47);
static int32_t * func_48(int16_t  p_49, uint32_t  p_50, uint8_t  p_51, uint32_t  p_52, int32_t * p_53);
static int16_t  func_54(uint32_t  p_55);
static int32_t * func_67(int32_t * p_68, int16_t  p_69, int32_t * p_70, uint16_t  p_71, float  p_72);
static int32_t * func_73(int32_t * p_74);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_21 g_18 g_56 g_57 g_58 g_326 g_327 g_328 g_1150 g_703 g_656 g_282 g_2740 g_281 g_655 g_871 g_872 g_139 g_140 g_343 g_980 g_125 g_308 g_309 g_89 g_938 g_163 g_1058 g_873 g_811 g_192 g_100 g_535 g_539 g_93 g_244 g_96 g_538 g_122 g_1311 g_1312 g_1327 g_325 g_1540 g_1551 g_1557 g_1461 g_1462 g_631 g_802 g_965 g_861 g_138 g_349 g_1637 g_1633 g_1686 g_1700 g_1674 g_20 g_1733 g_1672 g_144 g_193 g_1749 g_989 g_1755 g_1757 g_95 g_92 g_1558 g_1559 g_99 g_548 g_1009 g_1887 g_1041 g_1042 g_1847 g_141 g_536 g_537 g_1724 g_162 g_2551 g_2494 g_206 g_2608 g_2623 g_2642 g_518 g_519 g_2774 g_2283 g_2284 g_2880 g_2900 g_245 g_1151 g_1152 g_3013 g_2249 g_3019 g_3054 g_3067 g_3093 g_3103 g_3111 g_2822 g_2431 g_3160 g_3208 g_3052 g_3209 g_2265 g_2266
 * writes: g_21 g_18 g_58 g_56 g_2740 g_281 g_631 g_989 g_861 g_343 g_93 g_811 g_193 g_282 g_535 g_144 g_122 g_163 g_245 g_95 g_1551 g_1557 g_1540 g_883 g_1327 g_1632 g_100 g_635 g_1052 g_1686 g_89 g_125 g_802 g_1724 g_92 g_283 g_1733 g_703 g_278 g_1847 g_1887 g_308 g_1637 g_1934 g_1749 g_1633 g_548 g_2540 g_2551 g_2642 g_2467 g_96 g_1041 g_2822 g_2900 g_3052 g_3054 g_2608 g_3119 g_141 g_1009 g_3111 g_1310 g_99
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    const int32_t l_9 = 1L;
    int16_t *l_2801[4][5][7] = {{{(void*)0,(void*)0,(void*)0,&g_2467,&g_99[4][0][0],&g_703,&g_2642},{&g_278,&g_278,(void*)0,&g_99[1][0][2],(void*)0,&g_2608,(void*)0},{&g_99[5][2][0],&g_2642,(void*)0,&g_703,&g_2608,(void*)0,&g_2642},{&g_703,&g_278,&g_2467,(void*)0,(void*)0,(void*)0,(void*)0},{&g_99[6][1][1],&g_99[1][0][2],&g_99[6][1][1],&g_2642,&g_99[6][2][3],&g_703,&g_703}},{{(void*)0,&g_631,&g_278,(void*)0,&g_278,&g_631,&g_703},{&g_2642,&g_99[1][0][2],&g_2642,&g_2608,(void*)0,&g_703,&g_703},{&g_2467,&g_631,&g_2608,&g_703,(void*)0,(void*)0,&g_631},{(void*)0,(void*)0,&g_703,&g_703,(void*)0,(void*)0,&g_99[6][1][1]},{&g_703,&g_99[1][0][2],&g_2467,&g_631,&g_2467,&g_2608,&g_99[0][0][2]}},{{&g_2608,&g_99[4][0][2],&g_2608,&g_99[6][2][1],&g_2467,&g_703,&g_2642},{&g_99[0][0][2],&g_99[1][0][2],(void*)0,&g_703,&g_2608,(void*)0,&g_703},{&g_2608,(void*)0,&g_99[4][0][2],&g_99[3][2][0],&g_631,&g_99[3][2][0],&g_99[4][0][2]},{&g_631,&g_631,&g_631,&g_631,&g_703,&g_631,&g_703},{&g_703,&g_99[1][0][2],&g_703,(void*)0,&g_703,&g_2467,&g_99[6][2][3]}},{{&g_631,&g_631,(void*)0,(void*)0,&g_703,&g_99[2][1][2],&g_703},{(void*)0,&g_99[1][0][2],&g_2467,&g_99[4][0][2],&g_631,&g_703,&g_703},{&g_2608,&g_278,&g_703,&g_278,&g_2608,&g_703,&g_99[2][1][2]},{&g_99[1][0][2],&g_2642,(void*)0,(void*)0,&g_2467,&g_99[6][2][3],&g_2467},{&g_703,&g_278,(void*)0,(void*)0,&g_2467,&g_703,&g_631}}};
    int32_t l_2802[10][10][2] = {{{0x0D60FB1BL,1L},{0L,0x13F66194L},{0L,1L},{0x0D60FB1BL,0x0D60FB1BL},{1L,0L},{0x13F66194L,0L},{1L,0x0D60FB1BL},{0x0D60FB1BL,1L},{0L,0x13F66194L},{0L,1L}},{{0x0D60FB1BL,0x0D60FB1BL},{1L,0L},{0x13F66194L,0L},{1L,0x0D60FB1BL},{0x0D60FB1BL,1L},{0L,0x13F66194L},{0L,1L},{0x0D60FB1BL,0x0D60FB1BL},{1L,0L},{0x13F66194L,0L}},{{1L,0x0D60FB1BL},{0x0D60FB1BL,1L},{0L,0x13F66194L},{0L,1L},{0x0D60FB1BL,0x0D60FB1BL},{1L,0L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L}},{{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L}},{{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L}},{{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L}},{{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L}},{{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L}},{{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L}},{{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L},{1L,0x13F66194L},{0xC5179728L,0xC5179728L},{0x13F66194L,1L},{0x3F3A0CF2L,1L},{0x13F66194L,0xC5179728L},{0xC5179728L,0x13F66194L},{1L,0x3F3A0CF2L}}};
    uint32_t l_2806 = 4294967288UL;
    uint64_t l_2807 = 18446744073709551613UL;
    int32_t l_2808 = (-1L);
    int32_t l_2809[7];
    const int32_t ***l_2820[7] = {&g_697,(void*)0,(void*)0,&g_697,(void*)0,(void*)0,&g_697};
    const int32_t ****l_2819 = &l_2820[2];
    const int32_t *****l_2821[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_2824[3][8] = {{1L,(-1L),(-1L),1L,1L,(-1L),(-1L),1L},{1L,(-1L),(-1L),1L,1L,(-1L),(-1L),1L},{1L,(-1L),(-1L),1L,1L,(-1L),(-1L),1L}};
    float l_2825 = 0x1.7p+1;
    int8_t l_2826 = 0xA8L;
    int32_t *l_2827 = &g_2740[1];
    const uint8_t ***l_2998 = &g_1558;
    uint64_t l_3003[7] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
    uint8_t l_3021 = 0UL;
    int64_t l_3023 = 4L;
    uint8_t l_3100 = 0xC0L;
    uint8_t l_3115 = 0x78L;
    int32_t l_3145 = 1L;
    uint16_t l_3157 = 0x3F61L;
    int32_t l_3158[9][5] = {{5L,0x6A7B02BAL,5L,9L,0x3F6F0EA3L},{5L,(-5L),0x0C49EAF2L,0x6A7B02BAL,(-1L)},{0x9147D51CL,0x09C99138L,0xB4BEDE8CL,(-1L),0xB5D81272L},{0x6A7B02BAL,(-5L),0x0C49EAF2L,(-1L),0x0C49EAF2L},{0x488A540BL,0x488A540BL,5L,(-5L),0x0C49EAF2L},{9L,0x3F6F0EA3L,0x09C99138L,1L,0xB5D81272L},{0x92CE25FEL,1L,0x488A540BL,0x20551477L,(-1L)},{0x20551477L,0x3F6F0EA3L,0x9147D51CL,0x9147D51CL,0x3F6F0EA3L},{0xB5D81272L,0x488A540BL,(-1L),0x9147D51CL,(-5L)}};
    int32_t *l_3182 = &g_3160;
    int32_t **l_3181 = &l_3182;
    int16_t l_3198 = 0x9E4DL;
    int32_t l_3199 = 1L;
    int32_t l_3200 = 5L;
    int32_t l_3244[8][7][4] = {{{0xE8DA5139L,7L,0x053E01B2L,0x0DC00348L},{0x6B47DE9BL,9L,0x4FAFB62EL,1L},{1L,0x0DC00348L,1L,0x80631337L},{0xBC9088DAL,(-1L),0x2DDB734BL,0x0E0CE8E1L},{1L,(-1L),0xDBC3EDBEL,8L},{8L,(-3L),0x0DC00348L,2L},{(-3L),(-1L),(-3L),0x7470A0C3L}},{{9L,1L,0x62FB73D8L,1L},{1L,0xCB822C59L,0x7470A0C3L,1L},{0x0E0CE8E1L,7L,0x7470A0C3L,0x053E01B2L},{1L,0x12DFE08DL,0x62FB73D8L,1L},{9L,0x0E0CE8E1L,(-3L),0L},{(-3L),0L,0x0DC00348L,1L},{8L,0x6B47DE9BL,0xDBC3EDBEL,7L}},{{1L,0x215A640FL,0x2DDB734BL,0L},{0xBC9088DAL,2L,1L,1L},{1L,0x12DFE08DL,0x4FAFB62EL,(-1L)},{0x6B47DE9BL,1L,0x053E01B2L,1L},{0xE8DA5139L,9L,1L,1L},{1L,1L,2L,0x80631337L},{0xE16BF63BL,1L,0x2DDB734BL,2L}},{{1L,(-1L),1L,0x2DDB734BL},{8L,(-1L),1L,2L},{(-1L),1L,(-3L),0x80631337L},{0xCB822C59L,1L,1L,1L},{1L,9L,0x96ECBE93L,1L},{2L,1L,0x7470A0C3L,(-1L)},{1L,0x12DFE08DL,1L,1L}},{{9L,2L,(-7L),0L},{(-1L),0x215A640FL,0x0DC00348L,7L},{0x2DDB734BL,0x6B47DE9BL,1L,1L},{1L,0L,0L,0L},{0xE16BF63BL,0x0E0CE8E1L,1L,1L},{0x0DC00348L,0x12DFE08DL,1L,0x053E01B2L},{0x6B47DE9BL,7L,0x0993A5A1L,1L}},{{0x6B47DE9BL,0xCB822C59L,1L,1L},{0x0DC00348L,0x2DDB734BL,4L,0L},{0x7470A0C3L,1L,2L,1L},{(-3L),1L,0x0993A5A1L,0x0E0CE8E1L},{0x241F7845L,(-8L),0L,0xD915B969L},{(-8L),1L,0x60122349L,0xCB822C59L},{1L,0L,0xDBC3EDBEL,(-3L)}},{{(-3L),1L,0L,0L},{1L,0xE8DA5139L,9L,1L},{0xBC9088DAL,0xDBC3EDBEL,0xDBC3EDBEL,0xBC9088DAL},{0x62FB73D8L,1L,1L,1L},{(-8L),0x0DC00348L,8L,0xE8DA5139L},{0x0E0CE8E1L,0x2BD5C435L,0x0993A5A1L,0xE8DA5139L},{(-7L),0x0DC00348L,0x241F7845L,1L}},{{0x7470A0C3L,1L,7L,0xBC9088DAL},{0x2DDB734BL,0xDBC3EDBEL,(-7L),1L},{0x2BD5C435L,0xE8DA5139L,0x4FAFB62EL,0L},{0x215A640FL,1L,(-1L),(-3L)},{0x2DDB734BL,0L,4L,0xCB822C59L},{0x96ECBE93L,1L,0x241F7845L,0xD915B969L},{(-3L),(-8L),(-1L),0x0E0CE8E1L}}};
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_2809[i] = 0x794C3716L;
lbl_2881:
    l_2809[1] |= (func_2(g_6, (safe_div_func_float_f_f((l_9 , l_9), l_9)), l_9) , (safe_mod_func_uint64_t_u_u((l_2808 ^= ((+(l_2802[8][4][0] = l_9)) , (safe_mul_func_int16_t_s_s(((~(0xE9L >= (*g_328))) | l_2806), l_2807)))), l_2807)));
    if ((safe_lshift_func_uint8_t_u_u(l_2808, (safe_mul_func_int16_t_s_s((!(((--(**g_308)) <= ((*g_282) |= (l_2807 == (safe_rshift_func_uint16_t_u_s(l_2806, (((((g_2822 = (l_2809[1] , l_2819)) == &g_1041) && l_2824[1][0]) & 0x43L) <= ((*g_1633) = ((*g_1461) | l_2826)))))))) != (**g_138))), (*g_1461))))))
    { /* block id: 1287 */
        (**g_1041) = l_2827;
    }
    else
    { /* block id: 1289 */
        uint64_t l_2832 = 0x0E2D8984A21277A2LL;
        int8_t * const *l_2845 = &g_2284[2][0];
        uint16_t *l_2846 = (void*)0;
        uint16_t *l_2847 = &g_989;
        uint64_t *l_2856 = &l_2807;
        int64_t l_2857 = 0L;
        int32_t l_2910 = 0xC089432AL;
        float l_2911 = (-0x1.Dp+1);
        int32_t l_2916[9] = {(-7L),(-2L),(-2L),(-7L),(-2L),(-2L),(-7L),(-2L),(-2L)};
        float l_2918 = 0x0.5p+1;
        uint32_t l_2926 = 0x17A14EA9L;
        uint8_t *****l_2957 = (void*)0;
        uint32_t l_2977 = 8UL;
        uint64_t l_2978 = 9UL;
        int32_t *l_2979 = &l_2808;
        int64_t l_2982 = (-5L);
        int32_t ****l_3073 = &g_1041;
        int32_t *****l_3072 = &l_3073;
        uint8_t l_3097 = 255UL;
        uint32_t l_3110 = 1UL;
        uint32_t l_3161 = 0x02926E8DL;
        uint64_t l_3191 = 0x27752488F5C70387LL;
        int32_t l_3201 = 0x9C53A9D3L;
        uint16_t l_3202 = 65532UL;
        uint32_t l_3206 = 0UL;
        int8_t l_3213[4];
        int i;
        for (i = 0; i < 4; i++)
            l_3213[i] = 0xE2L;
        if ((safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(l_2832, (((safe_add_func_int8_t_s_s((((1UL ^ (**g_308)) & ((safe_mul_func_uint8_t_u_u(((**g_656) && (l_2832 , 0x87386C2EL)), (++(**g_327)))) > (safe_mod_func_int64_t_s_s(((*g_1633) = (safe_div_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(((*l_2847) = (l_2845 == l_2845)), ((safe_add_func_int32_t_s_s((((((safe_sub_func_int32_t_s_s((safe_div_func_uint16_t_u_u(((safe_div_func_int32_t_s_s(((((*l_2856) |= (g_144 &= (((**g_2283) = 0x95L) && l_2832))) < (**g_1749)) == (*g_1461)), (*g_965))) | 1UL), g_281)), l_2832)) , 0x0EL) , (void*)0) != &g_2219[1][0]) ^ 0xA69F8730527AF865LL), 0x67B1C4AEL)) == 65535UL))), 0xE34022CF8FBB145FLL))), l_2832)))) < 1UL), l_2857)) && 2UL) || l_2857))), 0xD075L)))
        { /* block id: 1296 */
            int8_t l_2862 = 0xD0L;
            int8_t l_2917 = (-1L);
            int32_t l_2921 = 1L;
            int32_t l_2922 = 0L;
            int32_t l_2923 = 1L;
            int32_t l_2925 = (-5L);
            int32_t * const ****l_2958 = (void*)0;
            int16_t l_2962 = (-1L);
            int32_t l_2981 = 0L;
            int32_t l_2984 = 0x47D1497BL;
            int32_t l_2986 = 0xC9C6DAECL;
            uint32_t l_3002[8][6][1] = {{{0xB83B145AL},{7UL},{0xB60D7FAFL},{0UL},{0xB60D7FAFL},{7UL}},{{0xB83B145AL},{18446744073709551615UL},{0x23DBFF99L},{0x6EC5C9D8L},{0x6EC5C9D8L},{0x23DBFF99L}},{{18446744073709551615UL},{0xB83B145AL},{7UL},{0xB60D7FAFL},{0UL},{0xB60D7FAFL}},{{7UL},{0xB83B145AL},{18446744073709551615UL},{0x23DBFF99L},{0x6EC5C9D8L},{0x6EC5C9D8L}},{{0x23DBFF99L},{18446744073709551615UL},{0xB83B145AL},{7UL},{0xB60D7FAFL},{0UL}},{{0xB60D7FAFL},{7UL},{0xB83B145AL},{18446744073709551615UL},{0x23DBFF99L},{0x6EC5C9D8L}},{{0x6EC5C9D8L},{0x23DBFF99L},{18446744073709551615UL},{0xB83B145AL},{7UL},{0xB60D7FAFL}},{{0UL},{0xB60D7FAFL},{7UL},{0xB83B145AL},{18446744073709551615UL},{0x23DBFF99L}}};
            float **l_3004 = &g_873;
            int32_t l_3030[5][10][5] = {{{0L,1L,(-1L),0x564FFD5FL,(-1L)},{0x6B95CFBFL,8L,0xCE3F3523L,3L,0x68738CFDL},{0x1F3D7741L,1L,0xE20720F7L,2L,(-1L)},{3L,0xCE3F3523L,8L,0x6B95CFBFL,0x64CAA5FDL},{0xD1AA78B3L,0xF7DB3A4AL,(-3L),1L,0xEB7E7C3CL},{0xCD7B84E9L,0L,8L,0x6B95CFBFL,0L},{0L,2L,6L,2L,6L},{0x01B72F7BL,0x01B72F7BL,0xBBCD2DB9L,3L,0L},{0xABA788A1L,1L,0xE0BB7141L,0x564FFD5FL,0x1F3D7741L},{0xCE3F3523L,0x87E9E93BL,0xFEB45B3AL,0xCD7B84E9L,0L}},{{(-1L),1L,(-1L),4L,(-3L)},{(-1L),0L,0x924AD8C4L,0L,(-1L)},{0L,1L,0x86BE0D78L,1L,0x279BA804L},{0xCD7B84E9L,3L,0L,(-2L),0x924AD8C4L},{0x86BE0D78L,0x526A1CB9L,0x501F928FL,1L,0x279BA804L},{0xCE3F3523L,(-2L),0xCD7B84E9L,(-1L),(-1L)},{0x279BA804L,0L,0x279BA804L,1L,6L},{8L,0xFEB45B3AL,8L,0xCD7B84E9L,0xCE3F3523L},{1L,(-1L),0xD1AA78B3L,(-8L),0xA21E32D9L},{0x64CAA5FDL,0x6B95CFBFL,8L,0xCE3F3523L,3L}},{{0x108BBC2EL,0L,0x279BA804L,0x348C7F73L,0x41381BAEL},{0x7DC86367L,0x87E9E93BL,0xCD7B84E9L,8L,(-3L)},{0xABA788A1L,0xF7DB3A4AL,0x501F928FL,0L,(-1L)},{8L,0xBBCD2DB9L,0L,0x64CAA5FDL,(-9L)},{0xABA788A1L,1L,0x86BE0D78L,0x70184DA5L,0xE20720F7L},{0x7DC86367L,0x924AD8C4L,0x924AD8C4L,0x7DC86367L,0xCD7B84E9L},{0x108BBC2EL,(-8L),(-3L),0x526A1CB9L,0x1F3D7741L},{0x64CAA5FDL,0L,0xBBCD2DB9L,8L,0x68738CFDL},{1L,0x6D7FCFD2L,(-1L),0x526A1CB9L,0xEB7E7C3CL},{8L,0xCD7B84E9L,0x87E9E93BL,0x7DC86367L,0x87E9E93BL}},{{0x279BA804L,0x348C7F73L,0x41381BAEL,0x70184DA5L,0x501F928FL},{0xCE3F3523L,8L,0x6B95CFBFL,0x64CAA5FDL,0x7DC86367L},{0x86BE0D78L,0x564FFD5FL,6L,0L,0x57B0B8B2L},{0xCD7B84E9L,8L,0xFEB45B3AL,8L,0xBBCD2DB9L},{0L,0x348C7F73L,1L,0x348C7F73L,0L},{(-1L),0xCD7B84E9L,(-2L),0xCE3F3523L,0L},{0xE20720F7L,0x6D7FCFD2L,0xABA788A1L,(-8L),1L},{(-2L),0L,3L,0xCD7B84E9L,0L},{0x57B0B8B2L,(-8L),0xE20720F7L,1L,0L},{0L,0x924AD8C4L,0L,(-1L),0xBBCD2DB9L}},{{0xE0BB7141L,1L,(-1L),1L,0x57B0B8B2L},{0x924AD8C4L,0xBBCD2DB9L,(-1L),(-2L),0x7DC86367L},{(-1L),0xF7DB3A4AL,(-1L),1L,0x501F928FL},{0x01B72F7BL,0x87E9E93BL,0L,0L,0x87E9E93BL},{0xA21E32D9L,0L,0xE20720F7L,0x0A92BFDFL,0xEB7E7C3CL},{0L,0x6B95CFBFL,3L,0x924AD8C4L,0x68738CFDL},{(-1L),(-1L),0xABA788A1L,(-3L),0x1F3D7741L},{0L,0xFEB45B3AL,(-2L),0x01B72F7BL,0xCD7B84E9L},{0xA21E32D9L,0L,1L,0x812937ADL,0xE20720F7L},{0L,(-1L),0xBBCD2DB9L,0xCE3F3523L,0x7DC86367L}}};
            int8_t l_3047 = 1L;
            int32_t *l_3069 = (void*)0;
            float l_3082 = (-0x7.2p-1);
            int16_t l_3130 = (-5L);
            uint16_t l_3152[8][2] = {{0UL,0xB277L},{0xB277L,0UL},{0xB277L,0xB277L},{0UL,0xB277L},{0xB277L,0UL},{0xB277L,0xB277L},{0UL,0xB277L},{0xB277L,0UL}};
            int32_t l_3159 = 0x8839AB29L;
            uint8_t *l_3190 = &l_3115;
            int32_t *l_3197[9][10] = {{&g_1733,(void*)0,&g_18,&l_3030[0][5][0],&g_2740[4],&l_2809[5],&l_2809[1],&l_2921,&l_2986,&l_2925},{&l_2921,(void*)0,&g_2740[4],&g_18,&l_2808,&l_3158[1][2],&l_2809[1],&l_3030[0][5][0],&l_3030[0][5][0],&l_2809[1]},{&g_2740[4],&l_2922,&l_2809[5],&l_2809[5],&l_2922,&g_2740[4],&l_2916[0],&l_2923,&g_1733,&g_1733},{&l_2986,&g_1733,&l_2809[1],&l_2808,&l_2809[5],&l_2925,&g_1733,&l_2809[2],&l_2984,&l_2921},{&l_2986,(void*)0,&l_3030[3][0][2],(void*)0,&l_3158[1][2],&g_2740[4],&l_2921,&l_2922,&l_2921,&g_2740[4]},{&g_2740[4],&l_2921,&l_2922,&l_2921,&g_2740[4],&l_3158[1][2],(void*)0,&l_3030[3][0][2],(void*)0,&l_2986},{&l_2921,&l_2984,&l_2809[2],&g_1733,&l_2925,&l_2809[5],&l_2808,&l_2809[1],&g_1733,&l_2986},{&g_1733,&g_1733,&l_2923,&l_2916[0],&g_2740[4],&l_2922,&l_2809[5],&l_2809[5],&l_2922,&g_2740[4]},{&l_2809[1],&l_3030[0][5][0],&l_3030[0][5][0],&l_2809[1],&l_3158[1][2],&l_2808,&g_18,&g_2740[4],(void*)0,&l_2921}};
            int i, j, k;
            if ((((safe_div_func_int64_t_s_s((safe_rshift_func_uint16_t_u_s(((l_2862 ^ ((safe_mul_func_int16_t_s_s(l_2857, ((*l_2827) ^= l_2862))) == ((safe_lshift_func_int16_t_s_u(0x20FBL, 12)) | ((safe_lshift_func_int8_t_s_u((((l_2857 & (((safe_rshift_func_uint8_t_u_s((**g_537), (safe_add_func_uint16_t_u_u(((**g_308) && ((*g_309) |= l_2857)), ((~((safe_mod_func_uint64_t_u_u((safe_div_func_int16_t_s_s(((l_2862 | (**g_1749)) < l_2857), l_2832)), g_2880)) , 0xB1L)) < (*g_1633)))))) , (*g_309)) ^ 0xA97CBB2AL)) < (-6L)) < 0UL), l_2832)) >= 0L)))) && l_2862), 7)), l_2862)) < 0x9317L) , (*g_57)))
            { /* block id: 1299 */
                int32_t * const ***l_2901 = &g_2900;
                float ***l_2902 = &g_872;
                int32_t l_2907 = 0xC2B777B5L;
                int32_t l_2920 = 0x7945669FL;
                int32_t l_2924 = 0x993E122DL;
                uint64_t l_2945 = 0x21640C5587549B18LL;
                int16_t *l_2950 = (void*)0;
                int8_t l_2983 = 0xDEL;
                int32_t l_2985[9];
                int64_t l_3020 = (-1L);
                uint64_t l_3027 = 3UL;
                uint64_t *l_3051 = &l_2832;
                int i;
                for (i = 0; i < 9; i++)
                    l_2985[i] = 0x7DAFE687L;
                if (g_1686)
                    goto lbl_2881;
                if (((((((safe_rshift_func_uint8_t_u_s((safe_mod_func_uint64_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_mod_func_int8_t_s_s(((**l_2845) |= (safe_rshift_func_int8_t_s_u((safe_div_func_uint8_t_u_u((((safe_add_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u(l_2832, 14)), (((*l_2847) ^= (safe_mod_func_uint64_t_u_u((((*l_2901) = g_2900) != (void*)0), (((0x8.2D3220p-98 >= (((*g_873) = ((void*)0 != l_2902)) >= (*l_2827))) < (safe_add_func_float_f_f((((*g_244) >= ((safe_div_func_uint32_t_u_u((l_2832 , 1UL), 0xD152758BL)) , l_2832)) == l_2832), l_2832))) , (**g_138))))) < g_2740[3]))) , (void*)0) == &g_1934), l_2862)), 1))), l_2832)), 15)) , l_2862), l_2832)), l_2907)) < g_163) , l_2862) > l_2862) , l_2862) ^ 0xEFED54DFL))
                { /* block id: 1305 */
                    int32_t l_2912[10][7] = {{0xAE9D04E7L,0x5DDE2F09L,1L,0x5DDE2F09L,0xAE9D04E7L,0x8A4A74D2L,0xAE9D04E7L},{(-1L),0xEF72A770L,0xEF72A770L,(-1L),0xABB212AFL,0xEF72A770L,(-3L)},{0x5AD6F991L,0x5DDE2F09L,0x5AD6F991L,0x1D940EC7L,1L,0x1D940EC7L,0x5AD6F991L},{(-1L),(-1L),(-3L),(-3L),(-1L),0xC36279C5L,(-3L)},{0xAE9D04E7L,0x1D940EC7L,0x1D6CB9DEL,0x5DDE2F09L,0x1D6CB9DEL,0x1D940EC7L,0xAE9D04E7L},{0xABB212AFL,(-3L),0xEF72A770L,0xABB212AFL,(-1L),0xEF72A770L,0xEF72A770L},{1L,0x5DDE2F09L,0x7117A16AL,0x5DDE2F09L,1L,0x8A4A74D2L,1L},{(-1L),0xABB212AFL,0xEF72A770L,(-3L),0xABB212AFL,0xABB212AFL,(-3L)},{0x1D6CB9DEL,0x5DDE2F09L,0x1D6CB9DEL,0x1D940EC7L,0xAE9D04E7L,0x1D940EC7L,0x1D6CB9DEL},{(-1L),(-3L),(-3L),(-1L),(-1L),(-3L),(-3L)}};
                    int32_t l_2919 = 0x715E9E92L;
                    int16_t l_2959 = 0x4717L;
                    float l_2960 = 0x7.2AD518p+95;
                    int32_t l_2961 = 0x5C74CEF8L;
                    int32_t *l_2963 = &g_58;
                    int32_t **** const l_2976 = &g_655;
                    int i, j;
                    for (l_2806 = 0; (l_2806 < 31); l_2806 = safe_add_func_int16_t_s_s(l_2806, 7))
                    { /* block id: 1308 */
                        int32_t *l_2913 = &g_861;
                        int32_t *l_2914 = &g_18;
                        int32_t *l_2915[10] = {(void*)0,&l_2912[5][3],(void*)0,&l_2912[5][3],(void*)0,&l_2912[5][3],(void*)0,&l_2912[5][3],(void*)0,&l_2912[5][3]};
                        int i;
                        ++l_2926;
                    }
                    (*l_2963) &= ((safe_mod_func_uint16_t_u_u(0xE37BL, (safe_div_func_int32_t_s_s((((l_2925 = (safe_sub_func_int8_t_s_s(((((((l_2919 = (l_2917 == (((*g_1633) = (((safe_sub_func_uint32_t_u_u((safe_add_func_int8_t_s_s((((safe_lshift_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((l_2945 || ((safe_add_func_uint8_t_u_u(1UL, ((((*l_2856) = l_2925) <= 0x6149E26BF7D91AC2LL) , ((safe_sub_func_int8_t_s_s(((((l_2912[4][0] ^= (l_2846 != l_2950)) == (((*l_2827) = (l_2959 = (((((safe_mul_func_int16_t_s_s(((safe_mod_func_int8_t_s_s((safe_mod_func_uint32_t_u_u((((l_2923 |= (((((*g_328) , l_2957) == (void*)0) , (*g_538)) , l_2921)) , (void*)0) == l_2958), (*g_309))), 9UL)) == 0x5C4BL), g_1672)) & l_2916[8]) ^ (*l_2827)) <= (*g_309)) , 0x7712L))) && (-1L))) , 0UL) == 0x0196L), 0x09L)) , l_2924)))) < l_2919)), g_535)), l_2919)) != 1UL) ^ l_2916[8]), 0L)), (-1L))) != (**g_2283)) || 0x48AC0ADAA16459CALL)) == g_1551))) ^ l_2961) , (**g_308)) != l_2962) , (-1L)) ^ l_2920), l_2857))) , (void*)0) == (void*)0), (*g_309))))) | 0x57L);
                    (***l_2976) = (*g_519);
                }
                else
                { /* block id: 1324 */
                    int32_t *l_2980[9] = {(void*)0,&g_58,(void*)0,(void*)0,&g_58,(void*)0,(void*)0,&g_58,(void*)0};
                    uint32_t l_2987 = 0x03AF26A3L;
                    uint8_t *l_3000[1][2][8] = {{{&g_56,&g_56,&g_56,&g_56,&g_56,&g_56,&g_56,&g_56},{&g_56,&g_56,&g_56,&g_56,&g_56,&g_56,&g_56,&g_56}}};
                    uint8_t **l_2999 = &l_3000[0][0][1];
                    uint8_t ***l_3001 = &l_2999;
                    int i, j, k;
                    --l_2987;
                    if (((*l_2979) ^ (((0xBDL & (**g_2283)) , (*g_871)) != ((safe_sub_func_uint8_t_u_u(0xE8L, ((safe_mod_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u((**g_2283), (safe_sub_func_int16_t_s_s((l_2998 == ((l_3003[0] &= (((((*l_3001) = l_2999) != (void*)0) & (*g_1151)) , l_3002[0][5][0])) , (*g_325))), (*l_2979))))) ^ 0UL), 0x17L)) != 0x2453L))) , l_3004))))
                    { /* block id: 1328 */
                        (***g_518) = (-1L);
                    }
                    else
                    { /* block id: 1330 */
                        int64_t *l_3018 = &l_2982;
                        int32_t l_3022 = 0x15EE125CL;
                        int32_t l_3024 = 0x411F0002L;
                        int32_t l_3025 = 0xFB603467L;
                        int32_t l_3026 = 0x9A3DA6AEL;
                        (**g_655) = func_10((((((safe_add_func_int64_t_s_s(0xD6210F0DC3CCE82ELL, (*l_2979))) > (**g_537)) , ((((**l_2999)--) > ((l_3020 |= (safe_sub_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u(((0xB7L != (*g_538)) || g_3013), ((safe_div_func_uint8_t_u_u(((-9L) <= ((safe_add_func_uint16_t_u_u((((void*)0 != l_3018) & (-1L)), g_95)) != g_2249[5])), 0x6EL)) , (*l_2979)))), g_3019[2]))) , (**g_2283))) ^ l_3021)) && 1UL) <= 0x3A1C35158E72A201LL), l_3022);
                        l_3027--;
                        (***g_871) = l_3025;
                    }
                    (***g_518) &= l_3030[0][5][0];
                }
                for (g_58 = 0; (g_58 < (-18)); --g_58)
                { /* block id: 1341 */
                    int8_t l_3039 = (-1L);
                    uint64_t *l_3050 = (void*)0;
                    int32_t *l_3053[8] = {&l_2923,&l_2923,&l_2923,&l_2923,&l_2923,&l_2923,&l_2923,&l_2923};
                    int i;
                    (*g_282) |= 9L;
                    for (g_18 = 0; (g_18 > (-20)); g_18 = safe_sub_func_uint32_t_u_u(g_18, 6))
                    { /* block id: 1345 */
                        const int16_t l_3046 = 0L;
                        (*g_282) &= ((*l_2979) > (l_3047 = (safe_mul_func_int16_t_s_s((*l_2979), ((safe_rshift_func_int16_t_s_s(((**g_2283) , (4294967295UL <= (2L ^ ((l_3039 != ((*g_309) |= (safe_sub_func_int64_t_s_s((255UL & ((**g_327) |= l_2923)), (safe_mul_func_uint16_t_u_u((((((safe_sub_func_float_f_f((**g_872), 0x1.Ap-1)) <= 0xD.9EFFAFp+49) < 0x5.358283p+59) , l_3039) >= l_3046), (*l_2979))))))) >= (*g_965))))), 15)) | l_3046)))));
                    }
                    for (l_2920 = 0; (l_2920 >= 6); ++l_2920)
                    { /* block id: 1353 */
                        (*l_2979) |= ((l_3051 = l_3050) == (g_3052 = &l_3027));
                    }
                    ++g_3054;
                }
            }
            else
            { /* block id: 1360 */
                uint32_t ***l_3060 = &g_2114;
                uint32_t ****l_3059 = &l_3060;
                uint32_t ***l_3061[10][3][8] = {{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,(void*)0,&g_2114}},{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,&g_2114,(void*)0}},{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,(void*)0},{&g_2114,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,(void*)0,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114}},{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,(void*)0,&g_2114}},{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,&g_2114,(void*)0}},{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,(void*)0},{&g_2114,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,(void*)0,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114}},{{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114}},{{(void*)0,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114}},{{(void*)0,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,(void*)0,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114}},{{(void*)0,&g_2114,(void*)0,(void*)0,&g_2114,(void*)0,&g_2114,&g_2114},{&g_2114,&g_2114,(void*)0,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114},{&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114,&g_2114}}};
                uint64_t l_3065 = 0UL;
                int i, j, k;
                for (g_2608 = 0; (g_2608 > 12); g_2608 = safe_add_func_uint8_t_u_u(g_2608, 9))
                { /* block id: 1363 */
                    (**l_3004) = ((*l_2827) > 0x6.1p+1);
                    if ((**g_656))
                        continue;
                }
                if ((((void*)0 != &l_3004) != (((*l_3059) = &g_2114) != ((*g_1461) , l_3061[3][2][4]))))
                { /* block id: 1368 */
                    for (l_2982 = 0; (l_2982 != 2); ++l_2982)
                    { /* block id: 1371 */
                        const int8_t l_3064[4][5] = {{0x4AL,0x42L,0x42L,0x4AL,0x2BL},{0x4AL,(-7L),0x92L,0x92L,(-7L)},{0x2BL,0x42L,0x92L,0x36L,0x36L},{0x42L,0x2BL,0x42L,0x92L,0x36L}};
                        int i, j;
                        if (g_122)
                            goto lbl_2881;
                        (**g_656) = l_3064[2][1];
                        if (l_3065)
                            break;
                    }
                    for (g_703 = 0; (g_703 <= 2); g_703 += 1)
                    { /* block id: 1378 */
                        int8_t l_3066 = (-3L);
                        return l_3066;
                    }
                }
                else
                { /* block id: 1381 */
                    (*g_873) = g_3067[0][0];
                }
            }
lbl_3195:
            for (g_548 = 0; (g_548 <= 7); g_548 += 1)
            { /* block id: 1387 */
                int32_t *l_3068[8] = {&l_2802[3][8][0],&l_2802[3][8][0],&l_2802[3][8][0],&l_2802[3][8][0],&l_2802[3][8][0],&l_2802[3][8][0],&l_2802[3][8][0],&l_2802[3][8][0]};
                uint16_t l_3114 = 0x6A33L;
                int32_t * const *l_3117 = &g_2431[1];
                int32_t * const **l_3116 = &l_3117;
                const uint64_t l_3124 = 0x507FAA535FB1A6DDLL;
                int64_t *l_3153 = &l_2982;
                uint32_t l_3154 = 4294967295UL;
                int i;
                l_3069 = l_3068[4];
                if ((*g_282))
                { /* block id: 1389 */
                    uint32_t l_3092 = 18446744073709551615UL;
                    int32_t l_3094 = 0x0757B1C6L;
                    const int32_t *l_3113 = &l_2925;
                    for (g_1551 = 1; (g_1551 <= 7); g_1551 += 1)
                    { /* block id: 1392 */
                        uint32_t l_3083 = 1UL;
                        const int32_t *l_3112 = &l_2984;
                        int i, j;
                        (*l_2979) = (((safe_div_func_int8_t_s_s((((((void*)0 != l_3072) || (safe_rshift_func_uint16_t_u_s((g_100[g_548][g_548] & (((*l_2856) = (safe_add_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s(((safe_mul_func_int8_t_s_s(l_3083, (0x40F9A44B000F179BLL > (safe_sub_func_uint8_t_u_u(((safe_mod_func_uint32_t_u_u(0x1E61C5D3L, ((safe_sub_func_int64_t_s_s((safe_div_func_uint8_t_u_u(((((void*)0 == &g_2822) <= l_3092) >= (*g_139)), 255UL)), (-10L))) & l_3083))) >= 1L), g_100[g_548][g_548]))))) && l_3092), (*g_1461))), (*g_309)))) && l_3083)), 10))) < 251UL) < g_100[g_548][g_548]), g_3093)) >= g_802[3]) , l_3092);
                        l_3094 = ((*l_2827) = (**g_656));
                        l_3113 = (((**g_2283) = ((***g_655) | ((g_100[g_548][g_548] , ((l_3094 ^ (((safe_rshift_func_int16_t_s_u(((*g_1151) < l_3097), 8)) != ((*g_309) = (safe_lshift_func_uint16_t_u_u(g_535, ((l_3100 && ((((((safe_rshift_func_int16_t_s_s((g_3103 == (safe_sub_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((*g_1633) <= 0L), l_3110)), 255UL))), 10)) <= l_3092) != g_3111) & 0xCDDB54139AE7DF7ALL) > l_3092) < 3UL)) , 1UL))))) , (*g_309))) || 0xC375F210L)) , 6UL))) , l_3112);
                    }
                }
                else
                { /* block id: 1401 */
                    int32_t * const ***l_3118[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_3118[i] = (void*)0;
                    l_3114 ^= 0x2689055BL;
                    (***g_518) = l_3115;
                    if (((*g_1461) | ((g_3119 = (g_2900 = l_3116)) != (*g_2822))))
                    { /* block id: 1406 */
                        int64_t *l_3121[3][6] = {{&g_141,&g_141,(void*)0,&g_141,(void*)0,&g_141},{(void*)0,&g_141,&g_141,&g_141,&g_141,(void*)0},{&g_141,(void*)0,&g_141,(void*)0,&g_141,&g_141}};
                        int16_t **l_3129 = &g_1052[0];
                        uint32_t l_3131[1];
                        int32_t l_3132[8][3] = {{7L,7L,(-5L)},{0x44B0B19FL,2L,0L},{7L,7L,(-5L)},{0x44B0B19FL,2L,0L},{7L,7L,(-5L)},{0x44B0B19FL,2L,0L},{7L,7L,(-5L)},{0x44B0B19FL,2L,0L}};
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_3131[i] = 18446744073709551607UL;
                        (*g_282) = ((**g_1749) | (**g_1749));
                        l_3132[3][2] |= (+(((((g_141 = ((**g_1749) ^= 4L)) , ((*l_2827) = (!(+((**l_3117) >= ((**g_308) = 0xD5E47B7BL)))))) > l_3124) ^ (**g_2283)) & (((***l_3116) || ((***l_3116) & (safe_div_func_int8_t_s_s(((((safe_sub_func_uint64_t_u_u((((((void*)0 != l_3129) >= (*g_1151)) , l_3130) & (*l_2979)), (*l_2979))) , 0x0.5p+1) , (*g_1461)) != g_3019[2]), l_3131[0])))) , (*g_328))));
                    }
                    else
                    { /* block id: 1413 */
                        uint16_t l_3138 = 0xD6B8L;
                        uint32_t *l_3140[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_3140[i] = &l_2926;
                        l_2802[8][4][0] &= ((*l_2979) , ((((**l_3117) , (((**l_3117) | (safe_div_func_uint64_t_u_u((*l_2979), (((safe_unary_minus_func_uint32_t_u((g_1009[3] |= (safe_mod_func_uint32_t_u_u((((**l_3117) , ((**g_2283) |= ((***g_2900) , ((l_3138 == 0x19D9L) != (~((*l_2847) ^= (**l_3117))))))) || (**l_3117)), (*g_309)))))) >= (*g_1151)) && l_3138)))) && (*g_538))) & (*l_2979)) , (*g_282)));
                        (**g_655) = (*g_519);
                    }
                }
                (***l_3116) = ((((**g_308) >= (safe_mul_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(((((((**g_1749) ^ l_3145) <= ((safe_mul_func_int8_t_s_s(((((l_2809[1] &= (safe_rshift_func_int16_t_s_u((*l_2979), (((safe_div_func_int64_t_s_s(((*l_3153) = (0x615AL == (l_3152[4][0] = (*l_2979)))), (l_3154 & ((**g_2283) &= (((&l_3073 == (((safe_mul_func_uint8_t_u_u(((((((0x58L || (-1L)) | l_3157) == (*g_57)) >= (*l_2827)) != (*l_2827)) < (*l_2827)), l_3158[3][3])) >= (**g_308)) , (void*)0)) , 0x23F5L) || (*g_1461)))))) & (**l_3117)) , l_3159)))) ^ (-1L)) & (*g_282)) < 0x7FA1L), g_3160)) && 0x55L)) <= (**g_656)) , (*g_309)) <= (-1L)), l_2984)), (-10L)))) , l_3161) && (**l_3117));
                for (l_3161 = 0; (l_3161 <= 7); l_3161 += 1)
                { /* block id: 1428 */
                    int i, j;
                    (*g_282) = (safe_mod_func_int16_t_s_s((-1L), g_100[g_548][l_3161]));
                    (*l_2979) &= (((*l_2856) ^= (((g_100[l_3161][l_3161] |= 1L) < (**g_138)) >= (**g_2283))) ^ (0xFCD2511BL < ((safe_rshift_func_int8_t_s_s((1L == ((**g_656) >= (safe_rshift_func_int8_t_s_u(((*g_309) & (safe_rshift_func_uint16_t_u_s(((0x38D8L == ((**g_2283) ^ 0xE4L)) , (***l_3116)), 12))), 0)))), (**g_2283))) , (***g_655))));
                    if (g_1674)
                        goto lbl_3196;
                }
            }
lbl_3196:
            for (g_95 = 0; (g_95 >= 20); g_95++)
            { /* block id: 1437 */
                const uint32_t l_3180 = 1UL;
                const int32_t **l_3183 = (void*)0;
                int32_t *l_3192 = &l_2910;
                (*l_3192) &= ((***g_655) |= ((safe_rshift_func_int8_t_s_s(((**g_138) | ((safe_mod_func_int32_t_s_s((l_3180 ^ (((**g_1749) = ((l_3181 != l_3183) || (safe_rshift_func_int16_t_s_s((safe_mod_func_uint8_t_u_u(((((safe_add_func_int64_t_s_s((l_3190 == (**l_2998)), (g_3160 , 7L))) , (0x9.902B83p+18 < 0x9.Ep+1)) <= (***g_871)) , 0xC6L), l_3180)), 6)))) != l_3180)), l_3191)) >= (*l_2979))), 3)) < 0L));
                for (l_2977 = 12; (l_2977 < 56); l_2977 = safe_add_func_uint8_t_u_u(l_2977, 8))
                { /* block id: 1443 */
                    (*g_282) &= (*g_57);
                }
                if (l_3021)
                    goto lbl_3195;
            }
            --l_3202;
        }
        else
        { /* block id: 1450 */
            int8_t l_3207 = 0x76L;
            uint16_t ***l_3210 = &g_3209;
            uint32_t l_3214 = 0x2578DC0EL;
            int32_t *l_3215 = &g_3111;
            uint32_t l_3230 = 18446744073709551608UL;
            uint32_t l_3231[7][7][3] = {{{18446744073709551607UL,18446744073709551615UL,1UL},{18446744073709551615UL,0x002C3A20L,0x1DFE4E4BL},{18446744073709551615UL,0xBC7F8D00L,1UL},{18446744073709551609UL,0xE24DF314L,9UL},{0xBA2D63EAL,1UL,18446744073709551609UL},{0x91A52685L,0x657A6986L,0xD365E54FL},{0x657A6986L,0x278F5893L,18446744073709551615UL}},{{0xBA138F9CL,0UL,0x16253B46L},{0xBC7F8D00L,0x9B74E68DL,0xF5CB0FA1L},{0UL,0x877960DEL,7UL},{18446744073709551609UL,0x877960DEL,0x5355B086L},{0x07E52E40L,0x9B74E68DL,4UL},{18446744073709551615UL,0UL,0xD0C6949EL},{5UL,1UL,18446744073709551613UL}},{{0x1DFE4E4BL,18446744073709551615UL,0x002C3A20L},{18446744073709551607UL,18446744073709551609UL,0x11624591L},{0x529EC3F3L,8UL,0xA7CDB468L},{18446744073709551609UL,18446744073709551611UL,1UL},{0x877960DEL,0x877960DEL,18446744073709551609UL},{8UL,1UL,0xFFC040BAL},{0x8CA567F0L,5UL,18446744073709551607UL}},{{18446744073709551614UL,0UL,18446744073709551615UL},{0xF6B24976L,0x8CA567F0L,18446744073709551607UL},{0x278F5893L,1UL,0xFFC040BAL},{0xBA2D63EAL,0xD365E54FL,18446744073709551609UL},{18446744073709551614UL,7UL,1UL},{18446744073709551609UL,18446744073709551614UL,0xA7CDB468L},{1UL,0x6D564DB7L,0x11624591L}},{{0x5355B086L,0x339C3D09L,0x002C3A20L},{9UL,18446744073709551607UL,18446744073709551613UL},{0xBC7F8D00L,9UL,0xD0C6949EL},{0x0AA3B327L,18446744073709551609UL,18446744073709551609UL},{0x710B3DA2L,0x99A56984L,0x9B74E68DL},{0xCD361B3DL,0x07E52E40L,8UL},{0xCD361B3DL,18446744073709551607UL,0x0AA3B327L}},{{0x710B3DA2L,18446744073709551615UL,0UL},{0x0AA3B327L,1UL,18446744073709551609UL},{0xBC7F8D00L,18446744073709551614UL,0x529EC3F3L},{9UL,1UL,0x34C8D03EL},{0x5355B086L,18446744073709551615UL,0xBA2D63EAL},{1UL,0UL,18446744073709551609UL},{18446744073709551609UL,1UL,1UL}},{{18446744073709551614UL,0x44350EB9L,5UL},{0xBA2D63EAL,0xBC7F8D00L,18446744073709551615UL},{0x278F5893L,4UL,18446744073709551611UL},{0xF6B24976L,18446744073709551609UL,18446744073709551607UL},{18446744073709551614UL,4UL,0x71286764L},{0x8CA567F0L,0xBC7F8D00L,0UL},{8UL,0x44350EB9L,0xE24DF314L}}};
            int i, j, k;
            (*l_3215) |= (~(((*l_2827) = ((((l_3206 , l_3207) | ((((l_3210 = g_3208) != &g_3209) & 0x054F8C6360D8C961LL) , (*l_2979))) ^ ((*l_2827) | (**g_308))) , (((safe_add_func_int16_t_s_s((0x6980B3A8CA0DE2D9LL < 0x75C277130D470621LL), l_3213[0])) >= (*g_1633)) , 4294967295UL))) && l_3214));
            for (g_861 = 0; (g_861 > 17); g_861++)
            { /* block id: 1456 */
                int32_t l_3222 = 0x3243101DL;
                int32_t l_3232 = 0x7D9DC23BL;
                uint64_t **l_3241 = (void*)0;
                uint64_t ***l_3240 = &l_3241;
                int32_t **l_3242 = &g_1310;
                l_3232 ^= ((safe_add_func_int16_t_s_s((safe_sub_func_int16_t_s_s(l_3222, ((((safe_sub_func_uint64_t_u_u((((+0UL) <= (safe_mod_func_uint64_t_u_u(((void*)0 == (*g_1311)), (((**l_2845) = (*l_3215)) || (safe_mod_func_int64_t_s_s(((*l_3215) ^ ((**g_2283) && (*l_3215))), (l_3230 && (***g_518)))))))) , l_3231[2][4][2]), (*l_3215))) , (*l_2979)) , (*l_2827)) || (*l_3215)))), l_3222)) && l_3222);
                (*g_1757) = func_67((**g_655), (*l_3215), ((*l_3242) = (((((((*l_3215) < ((*g_871) == ((safe_rshift_func_uint8_t_u_u(((!((((((*g_3052) = 3UL) & (&l_2847 == (*g_3208))) & (safe_add_func_int8_t_s_s(((safe_div_func_uint64_t_u_u((l_3240 == (void*)0), (***g_2265))) , 0xBAL), (*l_3215)))) , (*l_2979)) > 4294967295UL)) , l_3222), 2)) , (*g_871)))) >= l_3222) & (*l_2827)) > (*l_3215)) && l_3232) , &l_2809[1])), l_3222, (*g_873));
                (**g_872) = (-(***g_871));
                return (*l_3215);
            }
        }
    }
    return l_3244[6][6][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_21 g_18 g_56 g_57 g_58 g_326 g_327 g_328 g_1150 g_703 g_656 g_282 g_281 g_655 g_871 g_872 g_139 g_140 g_343 g_980 g_125 g_308 g_309 g_89 g_938 g_163 g_1058 g_873 g_811 g_192 g_100 g_535 g_539 g_93 g_244 g_96 g_538 g_122 g_1311 g_1312 g_1327 g_325 g_6 g_1540 g_1551 g_1557 g_1461 g_1462 g_631 g_802 g_965 g_861 g_138 g_349 g_1637 g_1633 g_1686 g_1700 g_1674 g_20 g_1733 g_1672 g_144 g_193 g_1749 g_989 g_1755 g_1757 g_95 g_92 g_1558 g_1559 g_99 g_548 g_1009 g_1887 g_1041 g_1042 g_1847 g_141 g_536 g_537 g_1724 g_162 g_2551 g_2494 g_206 g_2608 g_2623 g_2642 g_518 g_519 g_2774 g_2740
 * writes: g_21 g_18 g_58 g_56 g_281 g_631 g_989 g_861 g_343 g_93 g_811 g_193 g_282 g_535 g_144 g_122 g_163 g_245 g_95 g_1551 g_1557 g_1540 g_883 g_1327 g_1632 g_100 g_635 g_1052 g_1686 g_89 g_125 g_802 g_1724 g_92 g_283 g_1733 g_703 g_278 g_1847 g_1887 g_308 g_1637 g_1934 g_1749 g_1633 g_548 g_2540 g_2551 g_2642 g_2467 g_96 g_1041 g_2740
 */
static const int16_t  func_2(const uint32_t  p_3, float  p_4, uint64_t  p_5)
{ /* block id: 1 */
    uint32_t l_13 = 4294967295UL;
    uint64_t ****l_2787 = &g_2333;
    int32_t l_2796 = 0x9068205AL;
    (**g_655) = func_10(p_3, l_13);
    if (p_5)
    { /* block id: 1272 */
        uint16_t *l_2792 = &g_163;
        const int8_t *l_2794 = &g_2249[5];
        int32_t *l_2795[9][6] = {{(void*)0,&g_281,&g_1733,(void*)0,&g_281,(void*)0},{&g_2740[3],&g_1733,&g_2740[3],&g_58,&g_281,&g_281},{&g_2740[3],&g_281,(void*)0,(void*)0,&g_2740[3],&g_2740[3]},{&g_1733,&g_1733,(void*)0,&g_1733,&g_1733,&g_861},{&g_861,&g_2740[3],&g_281,&g_1733,&g_18,&g_2740[0]},{(void*)0,&g_1733,&g_1733,&g_2740[3],&g_2740[3],&g_2740[0]},{&g_281,&g_1733,&g_281,(void*)0,(void*)0,&g_861},{&g_2740[3],&g_2740[3],(void*)0,(void*)0,&g_2740[3],&g_2740[3]},{&g_861,(void*)0,(void*)0,&g_281,&g_1733,&g_281}};
        int i, j;
        l_2796 |= (safe_mod_func_int32_t_s_s((*g_282), ((((safe_add_func_int16_t_s_s((!(l_2787 == (void*)0)), ((safe_sub_func_uint8_t_u_u(((((*l_2792) ^= (safe_rshift_func_uint16_t_u_u(p_3, 5))) ^ ((void*)0 != &g_2265)) || (!(l_13 >= ((void*)0 != l_2794)))), p_3)) , l_13))) , p_3) != 0xED1402F11ADDE5E7LL) , l_13)));
        return p_5;
    }
    else
    { /* block id: 1276 */
        uint32_t l_2797 = 0x34C1E71FL;
        (*g_282) = l_2797;
    }
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_21 g_18 g_56 g_57 g_58 g_326 g_327 g_328 g_1150 g_703 g_656 g_282 g_281 g_655 g_871 g_872 g_139 g_140 g_343 g_980 g_125 g_308 g_309 g_89 g_938 g_163 g_1058 g_873 g_811 g_192 g_100 g_535 g_539 g_93 g_244 g_96 g_538 g_122 g_1311 g_1312 g_1327 g_325 g_6 g_1540 g_1551 g_1557 g_1461 g_1462 g_631 g_802 g_965 g_861 g_138 g_349 g_1637 g_1633 g_1686 g_1700 g_1674 g_20 g_1733 g_1672 g_144 g_193 g_1749 g_989 g_1755 g_1757 g_95 g_92 g_1558 g_1559 g_99 g_548 g_1009 g_1887 g_1041 g_1042 g_1847 g_141 g_536 g_537 g_1724 g_162 g_2551 g_2494 g_206 g_2608 g_2623 g_2642 g_518 g_519 g_2774 g_2740
 * writes: g_21 g_18 g_58 g_56 g_281 g_631 g_989 g_861 g_343 g_93 g_811 g_193 g_282 g_535 g_144 g_122 g_163 g_245 g_95 g_1551 g_1557 g_1540 g_883 g_1327 g_1632 g_100 g_635 g_1052 g_1686 g_89 g_125 g_802 g_1724 g_92 g_283 g_1733 g_703 g_278 g_1847 g_1887 g_308 g_1637 g_1934 g_1749 g_1633 g_548 g_2540 g_2551 g_2642 g_2467 g_96 g_1041 g_2740
 */
static int32_t * func_10(int32_t  p_11, uint8_t  p_12)
{ /* block id: 2 */
    int32_t l_1638 = 0L;
    int32_t l_2687 = 0xEA81DE37L;
    int32_t l_2688 = 0x3DA00463L;
    int32_t l_2690 = (-6L);
    int32_t l_2692 = 0L;
    int32_t l_2693 = 0x41F25F94L;
    int32_t l_2694 = (-6L);
    int32_t l_2695[4][9][7] = {{{0x17A82BEEL,1L,(-8L),0x052A28D1L,0x6ED009BBL,8L,0x750C33BBL},{0x052A28D1L,0xA51C8090L,3L,(-1L),0L,0x40987CAEL,0xCB64E66CL},{0xD1F7F9E9L,9L,0x12060DE5L,8L,1L,0L,(-5L)},{0x99BF5E6EL,9L,0x4FB47854L,(-5L),0xD1F7F9E9L,0x119BA20EL,0xB4A07AB3L},{3L,(-8L),0L,(-2L),0x12060DE5L,6L,0x12060DE5L},{0x026203C2L,(-5L),(-5L),0x026203C2L,0x48976BDEL,0x4FB47854L,(-8L)},{0xD912A2BDL,3L,7L,8L,0L,0xCEDC7080L,0x5322D3DBL},{7L,0x082CA0B2L,(-1L),0x4C26B29FL,(-4L),(-6L),(-8L)},{0xFF20A8E3L,(-6L),0x4C26B29FL,0x99BF5E6EL,(-2L),0xB4A07AB3L,0x12060DE5L}},{{1L,0L,0x5322D3DBL,1L,1L,(-1L),0xEFA0F8E9L},{0x4FB47854L,8L,(-1L),6L,0xFF20A8E3L,(-8L),6L},{2L,0xD912A2BDL,0x7127787EL,0x42EEA824L,0xFF20A8E3L,4L,0x750C33BBL},{0x12060DE5L,(-1L),0x99BF5E6EL,0x40987CAEL,1L,0xD912A2BDL,0x85DF3DE8L},{0xA51C8090L,0L,(-2L),0x7127787EL,(-2L),0L,0xA51C8090L},{0x334DF0F3L,0x4C26B29FL,(-1L),3L,(-4L),3L,(-5L)},{(-2L),0x334DF0F3L,1L,0x119BA20EL,0L,0xCB64E66CL,0x17A82BEEL},{0x082CA0B2L,0x12060DE5L,(-1L),2L,0x48976BDEL,1L,0L},{0x4C26B29FL,0x99BF5E6EL,(-2L),0xB4A07AB3L,0x12060DE5L,7L,0xCEDC7080L}},{{0xEFA0F8E9L,0xCEDC7080L,0x99BF5E6EL,0x334DF0F3L,(-9L),(-1L),0L},{0xD1F7F9E9L,4L,0x7127787EL,0x082CA0B2L,(-1L),0x750C33BBL,0x48976BDEL},{0xD1F7F9E9L,0x40987CAEL,(-1L),0xCEDC7080L,0xC04BC647L,(-9L),0x082CA0B2L},{0xEFA0F8E9L,0L,0x5322D3DBL,(-1L),0x4C26B29FL,0x4C26B29FL,(-1L)},{0x4C26B29FL,0x750C33BBL,0x4C26B29FL,0x85DF3DE8L,4L,0x40987CAEL,2L},{0x082CA0B2L,0x5EB0E038L,(-1L),0L,0x5322D3DBL,0xEFA0F8E9L,1L},{(-2L),(-1L),7L,(-9L),0L,0x40987CAEL,(-6L)},{0x334DF0F3L,(-4L),(-5L),0xD912A2BDL,(-3L),0x4C26B29FL,0xD1F7F9E9L},{0xA51C8090L,2L,0L,0L,0x750C33BBL,(-9L),0L}},{{0x12060DE5L,0xEFA0F8E9L,0xA51C8090L,0x5EB0E038L,0xB4A07AB3L,0x750C33BBL,(-1L)},{2L,0x5322D3DBL,4L,0x5EB0E038L,0x99BF5E6EL,(-1L),0xD912A2BDL},{0x4FB47854L,0xC04BC647L,0x42EEA824L,0L,0xEFA0F8E9L,7L,0xB4A07AB3L},{1L,9L,0xD912A2BDL,0xD912A2BDL,9L,1L,7L},{0xFF20A8E3L,0x119BA20EL,(-3L),(-6L),0x7127787EL,0x750C33BBL,0L},{0xE09080CFL,(-3L),(-1L),0x334DF0F3L,0x17A82BEEL,0x42EEA824L,0x4FB47854L},{1L,8L,0x12060DE5L,9L,0xD1F7F9E9L,0x6ED009BBL,0x99BF5E6EL},{0x48976BDEL,1L,0L,0x5322D3DBL,1L,1L,(-1L)},{0x17A82BEEL,0x12060DE5L,0xFF20A8E3L,0xCB64E66CL,0x42EEA824L,0L,0L}}};
    int64_t l_2696 = (-3L);
    float l_2697 = (-0x1.4p+1);
    int32_t l_2724 = 0x022B9CD2L;
    uint32_t ***l_2733 = &g_2114;
    int64_t l_2763 = 1L;
    volatile uint32_t * volatile * volatile * volatile * volatile *l_2775[9] = {&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774,&g_2774};
    volatile uint32_t * volatile * volatile * volatile * volatile l_2776[9][7][4] = {{{&g_2777[3][0][0],&g_2777[1][2][3],(void*)0,&g_2777[1][2][3]},{(void*)0,(void*)0,&g_2777[3][0][0],&g_2777[2][5][0]},{&g_2777[2][5][0],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]},{(void*)0,&g_2777[1][2][3],&g_2777[0][0][5],(void*)0},{&g_2777[0][5][2],&g_2777[3][0][3],&g_2777[1][2][3],&g_2777[2][5][1]},{&g_2777[2][0][3],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][0]},{&g_2777[1][2][3],&g_2777[1][2][3],(void*)0,(void*)0}},{{(void*)0,&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]},{(void*)0,&g_2777[2][2][0],&g_2777[1][2][3],&g_2777[2][0][3]},{&g_2777[1][2][3],(void*)0,(void*)0,&g_2777[1][2][3]},{&g_2777[0][5][5],&g_2777[1][2][3],&g_2777[0][1][4],&g_2777[1][2][3]},{&g_2777[1][4][1],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[1][1][4],&g_2777[1][2][3],&g_2777[1][0][3]},{&g_2777[1][2][3],&g_2777[0][1][4],&g_2777[1][1][4],&g_2777[1][2][3]}},{{&g_2777[1][2][3],(void*)0,&g_2777[1][2][3],&g_2777[1][4][1]},{&g_2777[3][1][2],&g_2777[0][1][5],&g_2777[0][5][4],&g_2777[0][1][5]},{&g_2777[1][2][3],&g_2777[1][4][0],(void*)0,&g_2777[1][3][2]},{&g_2777[1][2][3],&g_2777[3][4][3],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[3][0][0],&g_2777[1][2][0],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[1][2][3],(void*)0,&g_2777[1][2][3]}},{{&g_2777[1][2][3],&g_2777[2][0][3],&g_2777[0][5][4],&g_2777[1][2][3]},{&g_2777[3][1][2],&g_2777[1][2][0],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][1][4],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[0][5][5],&g_2777[1][2][3],&g_2777[2][0][3]},{&g_2777[1][2][3],&g_2777[0][5][0],&g_2777[1][2][3],&g_2777[1][4][0]},{&g_2777[1][4][1],&g_2777[1][2][3],&g_2777[0][1][4],(void*)0},{&g_2777[0][5][5],&g_2777[2][2][3],(void*)0,&g_2777[1][0][4]}},{{&g_2777[1][2][3],(void*)0,&g_2777[1][2][3],&g_2777[1][2][3]},{(void*)0,(void*)0,&g_2777[1][2][3],&g_2777[1][2][3]},{(void*)0,&g_2777[0][0][5],(void*)0,&g_2777[3][4][3]},{&g_2777[1][2][3],&g_2777[0][4][0],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[2][0][3],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[2][2][0]},{&g_2777[0][5][2],&g_2777[1][2][3],&g_2777[0][0][5],&g_2777[1][2][3]},{(void*)0,&g_2777[1][4][3],&g_2777[1][2][3],(void*)0}},{{&g_2777[2][5][0],&g_2777[2][0][2],&g_2777[3][0][0],&g_2777[0][5][1]},{(void*)0,&g_2777[1][2][3],(void*)0,&g_2777[0][5][5]},{&g_2777[3][0][0],&g_2777[1][2][3],(void*)0,&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[2][5][0],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[2][0][3],&g_2777[1][2][3],&g_2777[1][2][3]},{(void*)0,&g_2777[0][2][0],&g_2777[1][2][0],&g_2777[1][2][0]},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]}},{{&g_2777[1][2][3],(void*)0,&g_2777[1][4][0],&g_2777[1][3][2]},{(void*)0,&g_2777[0][1][4],&g_2777[1][2][3],&g_2777[1][4][0]},{&g_2777[2][2][3],&g_2777[0][1][4],&g_2777[2][2][0],&g_2777[1][3][2]},{&g_2777[0][1][4],(void*)0,(void*)0,&g_2777[1][2][3]},{&g_2777[2][5][0],&g_2777[1][2][3],&g_2777[0][4][0],&g_2777[1][2][0]},{&g_2777[3][1][2],&g_2777[0][2][0],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[2][0][3],&g_2777[2][2][3],&g_2777[1][2][3]}},{{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[3][1][2],&g_2777[1][2][3]},{(void*)0,&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]},{&g_2777[2][0][2],&g_2777[0][5][5],&g_2777[1][2][3],(void*)0},{&g_2777[1][2][3],&g_2777[1][2][3],(void*)0,&g_2777[1][2][3]},{&g_2777[0][1][5],(void*)0,&g_2777[0][5][1],&g_2777[2][2][0]},{&g_2777[1][2][3],&g_2777[3][1][2],(void*)0,&g_2777[1][2][3]},{&g_2777[3][0][3],&g_2777[1][2][3],&g_2777[2][5][1],&g_2777[0][5][1]}},{{&g_2777[1][2][3],(void*)0,(void*)0,&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[2][0][3],&g_2777[1][2][3],(void*)0},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[2][0][2]},{&g_2777[1][3][2],&g_2777[1][2][3],(void*)0,&g_2777[1][2][3]},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[1][2][3]},{(void*)0,&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[2][2][3]},{&g_2777[1][2][3],&g_2777[1][2][3],&g_2777[3][4][3],&g_2777[1][2][3]}}};
    int32_t *l_2778 = &l_2695[1][5][6];
    int32_t *l_2779 = &l_2695[1][5][6];
    int32_t *l_2780[2][8][10] = {{{&l_2688,&l_1638,(void*)0,&g_281,&g_281,(void*)0,&l_1638,&l_2688,&l_2690,&l_2688},{&g_281,&g_861,(void*)0,(void*)0,&l_1638,&l_2692,&g_281,(void*)0,&g_281,&g_281},{&g_281,&l_2694,(void*)0,&l_1638,&l_2694,(void*)0,&l_2692,&l_2688,&l_2692,(void*)0},{&l_1638,&l_2688,(void*)0,&l_2688,&l_1638,&g_1733,(void*)0,&g_281,&g_1733,&g_2740[3]},{&g_2740[3],&g_861,&l_2692,&g_2740[3],(void*)0,&g_2740[3],(void*)0,&g_281,&l_1638,&g_2740[3]},{&g_281,&g_2740[3],(void*)0,(void*)0,&l_1638,&g_861,&g_861,&l_1638,(void*)0,(void*)0},{&g_58,&g_58,&g_1733,(void*)0,&l_2694,(void*)0,&l_2688,&g_2740[3],&g_1733,&g_281},{&l_2688,&l_2692,&g_2740[3],&l_2688,&l_1638,&l_2690,&l_2688,&g_281,&g_861,&l_2688}},{{(void*)0,&g_58,&g_861,&l_1638,&g_281,&l_1638,&g_861,&g_2740[3],&g_2740[1],&l_2690},{(void*)0,&l_2692,(void*)0,&g_2740[3],&g_281,(void*)0,&g_861,&l_2690,&g_58,&l_1638},{&g_281,&l_2690,&l_2692,&g_2740[3],&g_2740[0],&g_1733,&g_281,&g_2740[1],&g_2740[1],&g_281},{&l_2687,&l_2690,&g_2740[0],&g_2740[0],&l_2690,&l_2687,(void*)0,(void*)0,&l_2690,&g_2740[1]},{&g_861,(void*)0,(void*)0,&l_1638,&l_2692,&g_281,(void*)0,&g_281,&g_281,(void*)0},{&g_861,&l_2690,&g_1733,&g_861,&g_2740[3],&l_2687,(void*)0,&l_2687,&g_2740[3],&g_861},{&l_2687,(void*)0,&l_2687,&g_2740[3],&g_861,&g_1733,&l_2690,&g_861,&g_2740[0],&g_2740[1]},{&g_281,(void*)0,&g_281,&l_2692,&l_1638,(void*)0,(void*)0,&g_861,&g_281,&g_2740[3]}}};
    int32_t *l_2781 = &g_2740[3];
    int i, j, k;
    for (p_11 = 0; (p_11 > 18); p_11++)
    { /* block id: 5 */
        int32_t *l_16 = (void*)0;
        int32_t *l_17 = &g_18;
        int32_t l_19 = 0L;
        ++g_21;
    }
    for (p_12 = 0; (p_12 >= 17); p_12++)
    { /* block id: 10 */
        int32_t *l_35 = &g_18;
        int32_t l_2684 = (-1L);
        int32_t l_2685 = 0x71E1BD0CL;
        int32_t l_2689 = 6L;
        int32_t l_2691[5] = {0x1E5E5D7BL,0x1E5E5D7BL,0x1E5E5D7BL,0x1E5E5D7BL,0x1E5E5D7BL};
        uint8_t l_2754 = 0UL;
        uint64_t *l_2771 = &g_144;
        int16_t *l_2772 = &g_2642;
        int16_t *l_2773[1];
        int i;
        for (i = 0; i < 1; i++)
            l_2773[i] = &g_631;
        for (g_18 = 0; (g_18 > (-12)); g_18--)
        { /* block id: 13 */
            int32_t *l_34 = &g_18;
            int32_t l_2686[4] = {3L,3L,3L,3L};
            int64_t l_2698 = 0x8049A0CA44AB6D3DLL;
            uint8_t l_2703 = 0xABL;
            uint64_t l_2711 = 0x6F2E6FCCC6F74F9ALL;
            uint32_t ***l_2732[10];
            uint32_t ****l_2731 = &l_2732[2];
            int32_t * const l_2739 = &g_2740[3];
            int32_t * const *l_2738 = &l_2739;
            int32_t * const **l_2737 = &l_2738;
            int32_t * const ***l_2736 = &l_2737;
            int32_t ***l_2741 = &g_656;
            int32_t ****l_2742 = &g_1041;
            int16_t * const *l_2749 = &g_1052[1];
            int16_t * const **l_2751 = &l_2749;
            int i;
            for (i = 0; i < 10; i++)
                l_2732[i] = &g_2114;
            (**g_1041) = func_28(l_34, l_35, ((p_12 == (((safe_unary_minus_func_int32_t_s(g_21)) , ((*g_873) = func_37(p_12, func_42((*l_35), p_11, l_34, func_48(func_54(p_11), g_18, (*l_35), g_56, &g_18), l_35), l_34, g_163))) == 0x1.Dp-1)) , l_1638), g_802[6], p_11);
            for (g_96 = 11; (g_96 <= (-23)); g_96 = safe_sub_func_int32_t_s_s(g_96, 7))
            { /* block id: 1245 */
                int32_t *l_2679 = &l_1638;
                int32_t *l_2680 = (void*)0;
                int32_t *l_2681 = &g_281;
                int32_t *l_2682 = &g_861;
                int32_t *l_2683[5][7] = {{&l_1638,&g_1733,&l_1638,&g_58,&g_1733,&g_1733,&g_58},{&g_861,&g_18,&g_861,&g_1733,&g_861,&g_861,&g_1733},{&l_1638,&g_1733,&l_1638,&g_58,&g_1733,&g_1733,&g_58},{&g_861,&g_18,&g_861,&g_1733,&g_861,&g_861,&g_1733},{&l_1638,&g_1733,&l_1638,&g_58,&g_1733,&g_1733,&g_58}};
                uint64_t l_2699[10] = {0xFEA70CC9B9262F50LL,0xFEA70CC9B9262F50LL,0x79DAC6CF246BF2FBLL,18446744073709551615UL,0x79DAC6CF246BF2FBLL,0xFEA70CC9B9262F50LL,0xFEA70CC9B9262F50LL,0x79DAC6CF246BF2FBLL,18446744073709551615UL,0x79DAC6CF246BF2FBLL};
                int64_t ***l_2728[10][8][3] = {{{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1749,&g_1749},{&g_1632,&g_1749,&g_1749},{&g_1749,&g_1749,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1749,&g_1749,&g_1632},{&g_1749,&g_1749,&g_1632}},{{&g_1632,&g_1749,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1749,&g_1749},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1749}},{{&g_1749,&g_1632,&g_1749},{&g_1749,&g_1749,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1749,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1749,&g_1749}},{{&g_1632,&g_1749,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1632,&g_1749,&g_1749},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1749,&g_1632}},{{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1749,&g_1632},{&g_1632,&g_1632,&g_1749},{&g_1632,&g_1749,&g_1632},{&g_1632,&g_1749,&g_1632}},{{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1749},{&g_1632,&g_1632,&g_1749},{&g_1749,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1749}},{{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1749,&g_1749},{&g_1632,&g_1749,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1632,&g_1749,&g_1749},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1632}},{{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1749,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1749,&g_1632},{&g_1632,&g_1632,&g_1749}},{{&g_1632,&g_1749,&g_1632},{&g_1632,&g_1749,&g_1632},{&g_1749,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1632},{&g_1632,&g_1632,&g_1749},{&g_1632,&g_1632,&g_1749}},{{&g_1749,&g_1632,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1632,&g_1632,&g_1632},{&g_1749,&g_1749,&g_1749},{&g_1632,&g_1749,&g_1632},{&g_1749,&g_1632,&g_1749},{&g_1632,&g_1749,&g_1749},{&g_1749,&g_1632,&g_1632}}};
                uint64_t *l_2729 = &g_125;
                int i, j, k;
            }
            (*g_656) = func_48((*l_35), (!(((*l_34) <= (((*l_2731) = (void*)0) != l_2733)) == (safe_mul_func_uint8_t_u_u((((*l_2736) = (void*)0) == ((*l_2742) = l_2741)), (((safe_mod_func_int64_t_s_s((65530UL <= (((((((**g_1749) = (safe_mod_func_uint32_t_u_u(0xC81029BDL, ((**g_308) = (safe_mod_func_uint64_t_u_u(((void*)0 == l_35), l_1638)))))) >= p_11) , 0x4F8FF3D412793F6DLL) > 0x93CEBE3FAF73F638LL) , 65527UL) || 8L)), p_12)) & p_12) >= 0UL))))), (****g_325), p_12, &l_2695[1][5][6]);
            (*l_2751) = l_2749;
        }
        (**g_656) = (((safe_sub_func_int16_t_s_s((((l_2754 &= p_12) , (**g_308)) && (((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_s(((l_2695[1][5][6] ^ l_2763) == (!(0xA83887ACL & (--(**g_308))))), 5)), (safe_div_func_int8_t_s_s((*l_35), (*l_35))))) >= ((((l_2690 = (safe_sub_func_int16_t_s_s(((*l_2772) &= ((((*l_2771) = 0x7478BA7789184E4CLL) ^ (*l_35)) > p_12)), l_2696))) <= p_12) == l_2763) <= (*g_965))) > 0xA4F98C89L)), p_11)) != 0xA40C8387062700CFLL) && 0x2052L);
    }
    l_2776[0][6][2] = g_2774;
    return l_2781;
}


/* ------------------------------------------ */
/* 
 * reads : g_535 g_1633 g_100 g_1551 g_309 g_89 g_861 g_343 g_1686 g_873 g_93 g_1461 g_1462 g_308 g_125 g_1540 g_656 g_1700 g_18 g_138 g_139 g_140 g_655 g_1674 g_872 g_20 g_1733 g_282 g_1672 g_144 g_57 g_192 g_193 g_1749 g_989 g_1755 g_1757 g_95 g_58 g_965 g_1311 g_1312 g_811 g_871 g_92 g_539 g_1557 g_1558 g_1559 g_99 g_548 g_1009 g_326 g_327 g_328 g_56 g_1887 g_1041 g_1042 g_1847 g_141 g_536 g_537 g_538 g_1724 g_162 g_2551 g_2494 g_206 g_2608 g_2623 g_2642 g_2467 g_122 g_518 g_519
 * writes: g_535 g_163 g_635 g_1052 g_989 g_1551 g_861 g_1686 g_811 g_93 g_89 g_125 g_282 g_802 g_1724 g_100 g_122 g_58 g_92 g_283 g_1733 g_703 g_278 g_1847 g_56 g_95 g_1887 g_631 g_308 g_1637 g_1934 g_1749 g_1633 g_548 g_144 g_2540 g_2551 g_2642 g_2467
 */
static int32_t * func_28(int32_t * p_29, int32_t * p_30, int32_t  p_31, uint32_t  p_32, const float  p_33)
{ /* block id: 745 */
    uint8_t l_1657 = 0x28L;
    uint64_t *l_1670 = (void*)0;
    uint64_t **l_1669 = &l_1670;
    uint64_t ***l_1668 = &l_1669;
    uint8_t l_1739 = 0x55L;
    int32_t l_1753 = (-9L);
    int8_t *l_1764[9][7][2] = {{{&g_343[0],&g_343[0]},{(void*)0,&g_1637},{(void*)0,&g_1327[0]},{&g_343[0],(void*)0},{&g_1327[0],&g_343[0]},{&g_1327[0],&g_343[0]},{&g_1327[0],&g_343[0]}},{{&g_1327[0],&g_1637},{&g_1327[0],&g_343[0]},{&g_1327[0],(void*)0},{&g_1637,&g_343[0]},{&g_343[0],&g_343[0]},{&g_343[0],&g_343[0]},{&g_1327[0],(void*)0}},{{&g_1637,(void*)0},{&g_1327[0],&g_343[0]},{(void*)0,&g_1327[0]},{&g_343[0],&g_1327[0]},{&g_1327[0],&g_1327[0]},{&g_343[0],&g_1327[0]},{(void*)0,&g_343[0]}},{{&g_1327[0],(void*)0},{&g_1637,(void*)0},{&g_1327[0],&g_343[0]},{&g_343[0],&g_343[0]},{&g_343[0],&g_343[0]},{&g_1637,(void*)0},{&g_1327[0],&g_343[0]}},{{&g_1327[0],&g_1637},{&g_1327[0],&g_1327[0]},{&g_343[0],&g_343[0]},{&g_343[0],&g_1327[0]},{&g_1327[0],&g_1637},{&g_1327[0],&g_343[0]},{&g_1327[0],(void*)0}},{{&g_1637,&g_343[0]},{&g_343[0],&g_343[0]},{&g_343[0],&g_343[0]},{&g_1327[0],(void*)0},{&g_1637,(void*)0},{&g_1327[0],&g_343[0]},{(void*)0,&g_1327[0]}},{{&g_343[0],&g_1327[0]},{&g_1327[0],&g_1327[0]},{&g_343[0],&g_1327[0]},{(void*)0,&g_343[0]},{&g_1327[0],(void*)0},{&g_1637,(void*)0},{&g_1327[0],&g_343[0]}},{{&g_343[0],&g_343[0]},{&g_343[0],&g_343[0]},{&g_1637,(void*)0},{&g_1327[0],&g_343[0]},{&g_1327[0],&g_1637},{&g_1327[0],&g_1327[0]},{&g_343[0],&g_343[0]}},{{&g_343[0],&g_1327[0]},{&g_1327[0],&g_1637},{&g_1327[0],&g_343[0]},{&g_1327[0],(void*)0},{&g_1637,&g_343[0]},{&g_343[0],&g_343[0]},{&g_343[0],&g_343[0]}}};
    const uint64_t l_1767 = 18446744073709551615UL;
    uint32_t l_1771 = 18446744073709551615UL;
    const uint64_t * const l_1845 = &g_144;
    const uint64_t * const *l_1844 = &l_1845;
    const float l_1849 = 0xB.E9CBA1p+11;
    int32_t ***l_1861 = &g_656;
    int32_t l_1886 = 1L;
    int32_t l_1915 = 0x71D5DB1EL;
    int32_t l_1917 = 0x5C83172EL;
    int32_t l_1918 = (-2L);
    int32_t l_1920 = (-3L);
    int32_t l_1921[10] = {6L,0xA6C20A66L,1L,1L,0xA6C20A66L,6L,0xA6C20A66L,1L,1L,0xA6C20A66L};
    uint32_t l_1956 = 0xAC986404L;
    uint8_t l_1980 = 0x0EL;
    uint64_t l_1991 = 18446744073709551615UL;
    uint16_t l_2045 = 65535UL;
    uint32_t ***l_2081[1][8] = {{&g_308,&g_308,&g_308,&g_308,&g_308,&g_308,&g_308,&g_308}};
    float l_2106 = (-0x6.1p+1);
    uint32_t **l_2112 = &g_1934;
    int16_t **l_2147 = &g_1052[0];
    int32_t *l_2158[2];
    int32_t l_2159 = 0x085DACF7L;
    int32_t l_2161 = (-1L);
    uint64_t l_2207 = 0xE391362AE0AB808DLL;
    int32_t *l_2217 = &g_1540;
    int32_t ** const l_2216 = &l_2217;
    const uint8_t *****l_2248 = &g_2247;
    uint32_t l_2339 = 6UL;
    float l_2388 = 0x8.8p+1;
    int8_t l_2392 = 0xB8L;
    uint64_t l_2405 = 1UL;
    uint32_t l_2425 = 18446744073709551612UL;
    float ***l_2440 = &g_872;
    uint8_t *l_2549 = &l_1739;
    uint8_t **l_2548 = &l_2549;
    uint8_t ***l_2547 = &l_2548;
    uint8_t ****l_2546 = &l_2547;
    uint8_t *****l_2545[5][8][5] = {{{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{(void*)0,&l_2546,&l_2546,&l_2546,(void*)0},{&l_2546,(void*)0,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,(void*)0,&l_2546},{(void*)0,&l_2546,&l_2546,(void*)0,&l_2546},{(void*)0,&l_2546,&l_2546,&l_2546,(void*)0},{&l_2546,(void*)0,&l_2546,&l_2546,&l_2546},{(void*)0,&l_2546,(void*)0,&l_2546,&l_2546}},{{&l_2546,&l_2546,(void*)0,(void*)0,&l_2546},{&l_2546,&l_2546,(void*)0,&l_2546,(void*)0},{&l_2546,&l_2546,(void*)0,(void*)0,&l_2546},{(void*)0,&l_2546,(void*)0,&l_2546,&l_2546},{(void*)0,&l_2546,(void*)0,(void*)0,(void*)0},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{(void*)0,&l_2546,(void*)0,(void*)0,&l_2546},{&l_2546,&l_2546,&l_2546,(void*)0,&l_2546}},{{(void*)0,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,(void*)0,&l_2546,(void*)0},{&l_2546,&l_2546,(void*)0,(void*)0,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,(void*)0},{(void*)0,&l_2546,(void*)0,&l_2546,&l_2546}},{{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,(void*)0,(void*)0,(void*)0,&l_2546},{&l_2546,&l_2546,(void*)0,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,(void*)0,(void*)0,(void*)0},{&l_2546,&l_2546,&l_2546,(void*)0,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546}},{{&l_2546,&l_2546,&l_2546,(void*)0,&l_2546},{&l_2546,(void*)0,&l_2546,(void*)0,(void*)0},{&l_2546,(void*)0,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{(void*)0,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,&l_2546,&l_2546},{&l_2546,&l_2546,&l_2546,(void*)0,(void*)0}}};
    const int8_t l_2640 = 0L;
    uint32_t l_2641[2];
    int16_t l_2671 = 0xDFD7L;
    int32_t *l_2672 = &l_1886;
    int32_t *l_2675 = &l_2161;
    int32_t *l_2676 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2158[i] = &g_58;
    for (i = 0; i < 2; i++)
        l_2641[i] = 0x38F0AB01L;
    for (g_535 = 0; (g_535 < 57); g_535 = safe_add_func_uint64_t_u_u(g_535, 2))
    { /* block id: 748 */
        int16_t *l_1654[6][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
        int32_t l_1659 = 0xC0E0F24CL;
        const int32_t *l_1673 = &g_1674;
        float l_1796[6][6][1] = {{{0x1.Cp+1},{0x5.Ap+1},{0x1.Cp+1},{0x6.90FB4Ap-84},{0x5.552FD9p+69},{0x5.552FD9p+69}},{{0x6.90FB4Ap-84},{0x1.Cp+1},{0x5.Ap+1},{0x1.Cp+1},{0x6.90FB4Ap-84},{0x5.552FD9p+69}},{{0x5.552FD9p+69},{0x6.90FB4Ap-84},{0x1.Cp+1},{0x5.Ap+1},{0x1.Cp+1},{0x6.90FB4Ap-84}},{{0x5.552FD9p+69},{0x5.552FD9p+69},{0x6.90FB4Ap-84},{0x1.Cp+1},{0x5.Ap+1},{0x1.Cp+1}},{{0x6.90FB4Ap-84},{0x5.552FD9p+69},{0x5.552FD9p+69},{0x6.90FB4Ap-84},{0x1.Cp+1},{0x5.Ap+1}},{{0x1.Cp+1},{0x6.90FB4Ap-84},{0x5.552FD9p+69},{0x5.552FD9p+69},{0x6.90FB4Ap-84},{0x1.Cp+1}}};
        uint32_t l_1797 = 0x6742DA0DL;
        int32_t l_1798 = 0x1552EE12L;
        float l_1816 = 0x9.F2FA7Dp-74;
        int32_t *l_1821[5][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        int32_t l_1822 = 0x0553E000L;
        uint8_t *l_1869 = &l_1657;
        uint64_t ***l_1908 = &l_1669;
        int64_t *l_1984 = &g_100[2][0];
        int64_t **l_1983 = &l_1984;
        float l_2044 = 0x0.Fp+1;
        int16_t l_2122 = 0x3B0AL;
        uint32_t l_2130[10][8][2] = {{{0x6B57048EL,0xF5DDD1E6L},{0x5CF18C2AL,0xBB35188AL},{0x97DFE4B3L,18446744073709551615UL},{0xF5DDD1E6L,0UL},{0x2AA01351L,0x41EBBDB5L},{1UL,0x41EBBDB5L},{0x2AA01351L,0UL},{0xF5DDD1E6L,18446744073709551615UL}},{{0x97DFE4B3L,0xBB35188AL},{0x5CF18C2AL,0xF5DDD1E6L},{0x6B57048EL,0x128F2DF7L},{18446744073709551615UL,0x792515EDL},{0xC664DA59L,1UL},{9UL,0xEEECAB0CL},{0UL,0xCE75F18EL},{0x128F2DF7L,0x9D15E5DEL}},{{0x74B960D8L,0x19A52CA8L},{1UL,0xE647C8C7L},{1UL,0x6C0B0106L},{0xA2C9E81EL,8UL},{2UL,0x97DFE4B3L},{1UL,1UL},{0xCE75F18EL,0x3AD1B982L},{0x8F547C47L,0x6B57048EL}},{{0x60C5C9CEL,0xE436B375L},{0xEEECAB0CL,0x60C5C9CEL},{0xE647C8C7L,0x5CF18C2AL},{0xE647C8C7L,0x60C5C9CEL},{0xEEECAB0CL,0xE436B375L},{0x60C5C9CEL,0x6B57048EL},{0x8F547C47L,0x3AD1B982L},{0xCE75F18EL,1UL}},{{1UL,0x97DFE4B3L},{2UL,8UL},{0xA2C9E81EL,0x6C0B0106L},{1UL,0xE647C8C7L},{1UL,0x19A52CA8L},{0x74B960D8L,0x9D15E5DEL},{0x128F2DF7L,0xCE75F18EL},{0UL,0xEEECAB0CL}},{{9UL,1UL},{0xC664DA59L,0x792515EDL},{18446744073709551615UL,0x128F2DF7L},{0x6B57048EL,0xF5DDD1E6L},{0x5CF18C2AL,0xBB35188AL},{0x97DFE4B3L,18446744073709551615UL},{0xF5DDD1E6L,0UL},{0x2AA01351L,0x41EBBDB5L}},{{1UL,0x41EBBDB5L},{0x2AA01351L,0UL},{0xF5DDD1E6L,18446744073709551615UL},{0x97DFE4B3L,0xBB35188AL},{0x5CF18C2AL,0xF5DDD1E6L},{0x6B57048EL,0x128F2DF7L},{18446744073709551615UL,0x792515EDL},{0xC664DA59L,1UL}},{{9UL,0xEEECAB0CL},{0UL,0xCE75F18EL},{0x128F2DF7L,0x9D15E5DEL},{0x74B960D8L,0x19A52CA8L},{1UL,0xE647C8C7L},{1UL,0x6C0B0106L},{0xA2C9E81EL,8UL},{2UL,0x97DFE4B3L}},{{1UL,1UL},{0xCE75F18EL,0x3AD1B982L},{0x8F547C47L,0x6B57048EL},{0x60C5C9CEL,0xE436B375L},{0xEEECAB0CL,0x60C5C9CEL},{0xE647C8C7L,0x5CF18C2AL},{0xE647C8C7L,0x60C5C9CEL},{0xEEECAB0CL,0xE436B375L}},{{0x60C5C9CEL,9UL},{0xDCC552E4L,18446744073709551615UL},{0x41EBBDB5L,0x6C0B0106L},{0x6C0B0106L,0x98850522L},{0xC664DA59L,18446744073709551613UL},{0xE436B375L,18446744073709551615UL},{18446744073709551615UL,0xF0D073A5L},{0xE647C8C7L,0x792515EDL}}};
        int16_t l_2160 = 0x2D9FL;
        uint16_t *l_2180[10] = {&g_163,&g_163,&g_163,&g_163,&g_163,&g_163,&g_163,&g_163,&g_163,&g_163};
        int32_t ***l_2313 = &g_1042[1][4];
        uint64_t * const **l_2336[3];
        const float l_2340[9] = {0x0.4p-1,0x0.4p-1,0x0.4p-1,0x0.4p-1,0x0.4p-1,0x0.4p-1,0x0.4p-1,0x0.4p-1,0x0.4p-1};
        int32_t * const *l_2366 = &l_2217;
        int32_t * const **l_2365 = &l_2366;
        uint32_t **l_2377 = &g_309;
        int32_t *l_2429 = &l_1920;
        uint32_t l_2439 = 0xD3588A00L;
        uint64_t l_2444 = 0x223CE429A1BE9D14LL;
        uint32_t l_2528 = 18446744073709551615UL;
        uint8_t *****l_2550 = &l_2546;
        int16_t ** const l_2566 = &g_1052[1];
        uint8_t l_2567 = 254UL;
        int32_t *l_2605 = (void*)0;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_2336[i] = (void*)0;
        for (g_163 = (-13); (g_163 == 43); ++g_163)
        { /* block id: 751 */
            uint8_t l_1666[4] = {6UL,6UL,6UL,6UL};
            int32_t *l_1699 = &g_18;
            int8_t *l_1717[8][1][6] = {{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}},{{&g_1327[0],&g_343[0],&g_1327[0],&g_1327[0],&g_343[0],&g_1327[0]}}};
            uint8_t *l_1723[7] = {&g_56,&g_56,&g_56,&g_56,&g_56,&g_56,&g_56};
            uint8_t **l_1722 = &l_1723[6];
            uint8_t *** const l_1721 = &l_1722;
            uint32_t l_1725 = 0x931BAA9FL;
            uint32_t l_1740 = 4294967295UL;
            uint8_t l_1779 = 253UL;
            int i, j, k;
            for (g_635 = (-20); (g_635 >= 13); g_635 = safe_add_func_uint8_t_u_u(g_635, 7))
            { /* block id: 754 */
                int16_t *l_1653 = &g_278;
                int32_t l_1658 = 2L;
                int16_t **l_1664 = &g_1052[1];
                uint16_t *l_1665 = &g_989;
                uint32_t *l_1667 = &g_1551;
                const int32_t *l_1671 = &g_1672;
                uint8_t * const l_1716[6][4] = {{(void*)0,&g_56,&g_56,(void*)0},{(void*)0,&g_56,&g_56,(void*)0},{(void*)0,&g_56,&g_56,(void*)0},{(void*)0,&g_56,&g_56,(void*)0},{(void*)0,&g_56,&g_56,(void*)0},{(void*)0,&g_56,&g_56,(void*)0}};
                uint8_t * const *l_1715 = &l_1716[1][0];
                uint8_t * const **l_1714[4][8] = {{&l_1715,&l_1715,&l_1715,&l_1715,&l_1715,&l_1715,&l_1715,&l_1715},{&l_1715,&l_1715,&l_1715,(void*)0,&l_1715,&l_1715,(void*)0,&l_1715},{&l_1715,&l_1715,(void*)0,&l_1715,(void*)0,&l_1715,&l_1715,(void*)0},{&l_1715,&l_1715,&l_1715,&l_1715,&l_1715,&l_1715,&l_1715,&l_1715}};
                int64_t **l_1718[5][8][6] = {{{&g_1633,(void*)0,(void*)0,&g_1633,&g_1633,&g_1633},{&g_1633,(void*)0,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,(void*)0,(void*)0,&g_1633,&g_1633,&g_1633},{(void*)0,(void*)0,&g_1633,(void*)0,&g_1633,&g_1633},{(void*)0,&g_1633,(void*)0,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,(void*)0,(void*)0},{&g_1633,(void*)0,(void*)0,&g_1633,&g_1633,&g_1633},{(void*)0,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633}},{{&g_1633,&g_1633,(void*)0,&g_1633,(void*)0,&g_1633},{&g_1633,(void*)0,(void*)0,(void*)0,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,(void*)0,(void*)0},{&g_1633,&g_1633,&g_1633,(void*)0,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,(void*)0,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,(void*)0}},{{&g_1633,&g_1633,(void*)0,(void*)0,&g_1633,&g_1633},{&g_1633,(void*)0,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,(void*)0,&g_1633,&g_1633,&g_1633,&g_1633},{(void*)0,&g_1633,&g_1633,&g_1633,&g_1633,(void*)0},{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,(void*)0},{(void*)0,&g_1633,(void*)0,&g_1633,(void*)0,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,(void*)0,&g_1633,&g_1633}},{{&g_1633,&g_1633,(void*)0,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,(void*)0,(void*)0,&g_1633,&g_1633},{&g_1633,(void*)0,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,&g_1633,&g_1633,(void*)0,&g_1633},{&g_1633,(void*)0,(void*)0,&g_1633,&g_1633,(void*)0},{&g_1633,&g_1633,&g_1633,&g_1633,(void*)0,(void*)0},{&g_1633,(void*)0,&g_1633,(void*)0,&g_1633,&g_1633},{(void*)0,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633}},{{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,&g_1633,(void*)0,&g_1633,&g_1633,(void*)0},{&g_1633,&g_1633,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,(void*)0,&g_1633,(void*)0,(void*)0,&g_1633},{&g_1633,(void*)0,&g_1633,&g_1633,&g_1633,&g_1633},{&g_1633,(void*)0,&g_1633,&g_1633,(void*)0,(void*)0},{&g_1633,(void*)0,&g_1633,&g_1633,(void*)0,&g_1633},{&g_1633,&g_1633,(void*)0,&g_1633,&g_1633,(void*)0}}};
                int32_t l_1726 = 0xB6FB975FL;
                int16_t *l_1776 = &g_278;
                int32_t l_1815 = (-2L);
                int i, j, k;
                if (((0xFB18L || (((safe_mul_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u((((((*l_1667) &= (safe_lshift_func_uint8_t_u_s(((l_1653 != (p_32 , l_1654[4][0])) < (((safe_mod_func_uint8_t_u_u((l_1658 ^= l_1657), (l_1659 & (*g_1633)))) , (safe_div_func_uint16_t_u_u(((*l_1665) = (safe_rshift_func_uint8_t_u_u(((((*l_1664) = &g_99[4][0][3]) == l_1654[4][0]) >= 65529UL), 5))), l_1657))) != l_1666[0])), 1))) , &g_138) != l_1668) && l_1659), 65535UL)) && p_32), 0UL)) , (*g_309)) | p_32)) | p_32))
                { /* block id: 759 */
                    const int32_t *l_1675 = &g_861;
                    int32_t *l_1719 = (void*)0;
                    int32_t *l_1720 = &g_802[4];
                    l_1675 = (l_1673 = (l_1671 = &l_1659));
                    for (g_861 = 0; (g_861 >= 0); g_861 -= 1)
                    { /* block id: 765 */
                        uint32_t *l_1685 = &g_1686;
                        uint64_t *l_1698 = &g_125;
                        int i;
                        (*g_656) = (((safe_mod_func_int8_t_s_s((((g_343[g_861] , ((((safe_rshift_func_int8_t_s_u(0x7FL, 5)) == (+(0xF3L & ((((*l_1698) ^= (((((-0x1.6p+1) > ((safe_div_func_float_f_f(((((void*)0 == (*l_1669)) > (((*l_1685) &= (++(*l_1667))) , (safe_mul_func_float_f_f((+((*g_873) = 0x4.62168Fp-23)), ((((**g_308) = (safe_mod_func_uint32_t_u_u((0xB9CD6F3CF4B91CDFLL < ((safe_mul_func_uint16_t_u_u((g_93--), (*g_1461))) == (*g_1633))), p_31))) , g_343[g_861]) != l_1666[0]))))) == 0xB.29F6B9p+31), 0x7.621A69p+36)) >= (*l_1675))) , (*g_1633)) , (-5L)) & (*l_1671))) < p_31) == p_32)))) <= 0xA4B48056L) , l_1666[1])) >= g_1540) && 0L), p_31)) && (*l_1675)) , l_1699);
                    }
                    l_1726 &= (g_1700 && (((*l_1675) != (+(safe_add_func_uint64_t_u_u(18446744073709551615UL, ((*g_1633) = (safe_add_func_uint64_t_u_u(((safe_add_func_int64_t_s_s((safe_rshift_func_int16_t_s_u(((g_1724[3] = (safe_rshift_func_uint16_t_u_u(((*l_1671) , ((safe_lshift_func_int16_t_s_u((((*l_1673) , l_1714[0][1]) == (((*l_1720) = ((l_1717[7][0][2] == (void*)0) || (&g_1151 != l_1718[1][1][3]))) , l_1721)), 1)) < (*p_30))), 2))) && p_31), p_32)), (**g_138))) || 0xB556BD60E09F8D6BLL), l_1725))))))) , g_535));
                    if ((*l_1699))
                        continue;
                }
                else
                { /* block id: 779 */
                    uint8_t ** const * const l_1727 = &g_1020;
                    int32_t * const l_1756 = &g_1733;
                    if ((&l_1722 != l_1727))
                    { /* block id: 780 */
                        int32_t l_1738 = 1L;
                        (**g_655) = &p_31;
                        (**g_656) = ((*l_1673) <= ((safe_rshift_func_uint8_t_u_s(((((*g_872) == (void*)0) & ((*g_1633) |= ((*l_1699) || (safe_rshift_func_int8_t_s_s((+0xE398E65DL), 1))))) ^ (g_20 | g_1733)), 6)) < ((safe_div_func_int64_t_s_s(((-9L) < 0L), 0x468A7BC06246A0A1LL)) < l_1657)));
                        (*g_656) = func_48((*g_1461), ((*g_309) = (0x79L == ((1UL & l_1657) != (g_122 = (~(~(((*l_1671) || (l_1738 && 1L)) | ((*g_139) ^ (((*l_1673) && 0L) || p_31))))))))), l_1739, g_144, &l_1659);
                    }
                    else
                    { /* block id: 787 */
                        int64_t ***l_1750 = &l_1718[2][0][3];
                        int64_t l_1754 = (-9L);
                        (*g_57) = l_1740;
                        (*g_1755) = (l_1754 = (safe_mul_func_float_f_f((*g_192), (safe_add_func_float_f_f((safe_add_func_float_f_f((((safe_add_func_float_f_f(((((*l_1750) = g_1749) == &g_1633) >= ((*g_873) = 0x0.3p+1)), 0x6.8BB26Cp-18)) == (((((*l_1665)++) , (*l_1671)) , l_1753) < p_31)) != (*l_1699)), 0x1.Cp+1)), 0xC.AB2426p+55)))));
                        (*g_1757) = l_1756;
                        if ((*l_1671))
                            continue;
                    }
                }
                for (g_1733 = 0; (g_1733 > 7); g_1733 = safe_add_func_uint32_t_u_u(g_1733, 8))
                { /* block id: 800 */
                    int16_t l_1770[9] = {8L,8L,8L,8L,8L,8L,8L,8L,8L};
                    int16_t **l_1777 = &l_1654[5][1];
                    int32_t l_1778[7] = {0x4D9B2780L,0x4D9B2780L,0x4D9B2780L,0x4D9B2780L,0x4D9B2780L,0x4D9B2780L,0x4D9B2780L};
                    int32_t *** const *l_1791[5][6] = {{&g_655,&g_655,&g_655,&g_655,&g_655,&g_655},{&g_655,&g_655,&g_655,&g_655,&g_655,&g_655},{&g_655,&g_655,&g_655,&g_655,&g_655,&g_655},{&g_655,&g_655,&g_655,&g_655,&g_655,&g_655},{&g_655,&g_655,&g_655,&g_655,&g_655,&g_655}};
                    float ****l_1808 = &g_871;
                    int i, j;
                    for (g_703 = 0; (g_703 <= (-30)); g_703 = safe_sub_func_int64_t_s_s(g_703, 9))
                    { /* block id: 803 */
                        uint64_t l_1768 = 0xFE233AF305CA9FD9LL;
                        int32_t l_1769[5][3][5] = {{{0x91FC3032L,1L,(-1L),(-1L),1L},{0x95877CD9L,(-1L),1L,(-10L),1L},{0L,3L,0xF0273C49L,0L,0x79D35769L}},{{0xF5845282L,0x95877CD9L,0x95877CD9L,0xF5845282L,0xCC68C052L},{0L,(-1L),0L,(-1L),0L},{0x95877CD9L,0x66EA9191L,0x2CC97939L,1L,0x2CC97939L}},{{0x91FC3032L,0x91FC3032L,0L,(-1L),0L},{0xFB2AD4E8L,0xD8691A5EL,0xCC68C052L,0xF5845282L,0x95877CD9L},{0xF0273C49L,0L,0x79D35769L,0L,0xF0273C49L}},{{(-1L),0xD8691A5EL,1L,(-10L),1L},{(-1L),0x91FC3032L,1L,(-1L),(-1L)},{(-10L),0x66EA9191L,(-10L),0xD8691A5EL,1L}},{{0xD0CBEABCL,(-1L),0x91FC3032L,0L,0xF0273C49L},{1L,0x95877CD9L,0x015977EBL,0x015977EBL,0x95877CD9L},{1L,3L,0x91FC3032L,0xF0273C49L,0L}}};
                        int i, j, k;
                        l_1768 = ((((void*)0 == &l_1721) , (safe_sub_func_uint16_t_u_u(((l_1764[3][0][1] == (void*)0) , (safe_mul_func_uint16_t_u_u(p_31, (((void*)0 == &g_518) || 18446744073709551608UL)))), l_1767))) || p_31);
                        ++l_1771;
                    }
                    if (((*p_29) > (safe_div_func_int32_t_s_s((p_31 | (l_1778[0] = ((*l_1673) <= (((*l_1664) = &l_1770[2]) == ((*l_1777) = l_1776))))), (l_1779--)))))
                    { /* block id: 811 */
                        int16_t l_1814 = 0xCA7BL;
                        p_31 = (l_1798 |= (safe_mul_func_uint16_t_u_u(((((safe_mod_func_int8_t_s_s(l_1739, (safe_add_func_uint32_t_u_u(((*g_309) = (safe_sub_func_uint16_t_u_u(((**g_138) , (safe_unary_minus_func_uint8_t_u(((((((p_32 , (((void*)0 != l_1791[2][1]) || (safe_add_func_uint16_t_u_u((--(*l_1665)), g_95)))) , ((void*)0 != &g_938)) , (((*l_1699) >= (*l_1671)) , 0x18C43FA9L)) == (*g_57)) >= p_32) ^ 0x02D8B4EB52D182D9LL)))), g_1674))), (*l_1671))))) && p_31) | 8UL) || (*g_965)), l_1797)));
                        if ((*l_1699))
                            continue;
                        (***g_871) = (((((((safe_mul_func_int8_t_s_s(((p_31 | (safe_div_func_int32_t_s_s((~((g_93 = g_144) , (l_1726 = (*p_29)))), ((0xBBL || (l_1815 = (safe_mul_func_int16_t_s_s((((safe_sub_func_uint64_t_u_u((((**g_1749) = (0x3B794BDC9515EED2LL == ((((*g_1311) == l_1808) , ((safe_sub_func_int32_t_s_s((((*l_1653) = (*l_1673)) | ((((+(safe_add_func_int16_t_s_s(p_31, l_1814))) || p_32) <= 0xDD78D82AL) == (**g_138))), (*g_309))) | 4294967286UL)) & (*p_29)))) , 18446744073709551615UL), 0L)) != (*l_1671)) | (*l_1699)), p_31)))) , (*p_29))))) > 0UL), l_1814)) > (*l_1673)) ^ p_31) , p_31) == l_1816) <= 0xC.DD7584p-87) > (**g_872));
                        (***g_871) = ((!p_31) , (safe_div_func_float_f_f(p_33, ((!p_32) == (*g_1755)))));
                    }
                    else
                    { /* block id: 824 */
                        p_29 = ((*g_656) = &p_31);
                    }
                }
            }
            return l_1821[4][7];
        }
        if ((*p_29))
            break;
        if (l_1822)
        { /* block id: 833 */
            uint64_t *l_1839 = &g_125;
            const uint64_t * const **l_1846 = &l_1844;
            int32_t l_1850[4][6];
            int32_t l_1853 = 0xD5C69274L;
            int8_t l_1854 = 0L;
            uint8_t *l_1862 = &g_56;
            uint32_t l_1909 = 0xE5F231EEL;
            int32_t l_1910 = 5L;
            int32_t l_1922 = 0L;
            uint64_t l_1923 = 0x766A94B1D8DAF82CLL;
            int32_t *l_1938 = &l_1921[0];
            int64_t *l_1982 = &g_100[0][5];
            int64_t **l_1981 = &l_1982;
            int16_t l_1985 = (-6L);
            int32_t *l_2052 = (void*)0;
            uint64_t l_2060 = 1UL;
            uint32_t ***l_2080 = &g_308;
            uint8_t l_2172[3][6] = {{1UL,0x98L,253UL,0x98L,1UL,1UL},{0xEDL,0x98L,0x98L,0xEDL,0UL,0xEDL},{0xEDL,0UL,0xEDL,0x98L,0x98L,0xEDL}};
            uint16_t *l_2177 = &g_989;
            int8_t l_2269 = 0xB6L;
            int i, j;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 6; j++)
                    l_1850[i][j] = 1L;
            }
            if (((((((safe_add_func_int64_t_s_s(((safe_add_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((~g_539[1][8]), ((p_32 ^ (safe_sub_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(0UL, 2)), p_31))) , (l_1753 = ((~(p_31 , ((((++(*l_1839)) | (l_1657 , ((safe_mul_func_uint16_t_u_u(((p_32 , &l_1839) != (g_1847 = ((*l_1846) = l_1844))), 65535UL)) <= (**g_308)))) && p_31) , p_32))) , p_32))))), p_31)) , (*l_1673)), 0x02L)), 1L)) , p_31), l_1767)) & 0x843C6A54L) > l_1850[2][5]) & p_31) >= (-1L)) , l_1771))
            { /* block id: 838 */
                if ((*p_29))
                    break;
                for (g_703 = 0; (g_703 <= 21); g_703++)
                { /* block id: 842 */
                    uint64_t l_1855 = 18446744073709551606UL;
                    --l_1855;
                }
            }
            else
            { /* block id: 845 */
                uint32_t l_1860 = 0xD63E074BL;
                uint16_t *l_1870 = &g_989;
                int32_t l_1871[1];
                int32_t *l_1881 = (void*)0;
                float l_1919[3];
                float *** const l_1933 = &g_872;
                uint32_t l_1942 = 7UL;
                int32_t l_1986 = (-1L);
                int8_t **l_2017 = (void*)0;
                uint32_t l_2025 = 6UL;
                int64_t l_2061[8] = {7L,0xDC34A7B57CE02F2DLL,7L,0xDC34A7B57CE02F2DLL,7L,0xDC34A7B57CE02F2DLL,7L,0xDC34A7B57CE02F2DLL};
                int i;
                for (i = 0; i < 1; i++)
                    l_1871[i] = 0xF0FC8CD2L;
                for (i = 0; i < 3; i++)
                    l_1919[i] = 0x0.A45A69p-97;
                l_1871[0] = ((safe_mul_func_uint16_t_u_u((l_1860 , ((*l_1870) &= (l_1861 == (((*g_139) ^ (l_1853 &= ((**g_1749) = ((l_1862 == (**g_1557)) == (safe_rshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s(((p_32 & (8UL >= (safe_rshift_func_int8_t_s_u(((l_1869 == (((*g_873) , p_31) , (void*)0)) < (*g_965)), 2)))) & l_1850[2][4]), l_1860)), g_99[1][0][2])))))) , (void*)0)))), (-1L))) & l_1850[1][1]);
                for (l_1739 = 11; (l_1739 >= 19); l_1739 = safe_add_func_int32_t_s_s(l_1739, 4))
                { /* block id: 852 */
                    uint8_t l_1877 = 0xEEL;
                    uint16_t *l_1880 = &g_95;
                    int32_t l_1883[1][5][6] = {{{1L,7L,0L,1L,0xFA07F896L,1L},{1L,0xFA07F896L,1L,0L,7L,1L},{(-7L),(-1L),0L,0xDC02E7EBL,7L,(-7L)},{0L,0xFA07F896L,0xDC02E7EBL,0xDC02E7EBL,0xFA07F896L,0L},{(-7L),7L,0xDC02E7EBL,0L,(-1L),(-7L)}}};
                    int32_t l_1901 = 8L;
                    float *l_1931 = (void*)0;
                    float *l_1932 = &g_92;
                    uint32_t *l_1936 = &l_1771;
                    uint32_t **l_1935 = &l_1936;
                    int i, j, k;
                    if (((((*g_1461) | ((*l_1870) = g_548)) <= (l_1871[0] = g_1009[7])) < ((l_1853 ^= (safe_lshift_func_int8_t_s_s(0x53L, 4))) , (safe_unary_minus_func_int8_t_s(((l_1877 , (0x2A6009EFB13C0EC1LL & ((**g_1749) &= (p_32 || ((*l_1880) = (safe_lshift_func_uint8_t_u_u(((***g_326) |= 0x12L), 7))))))) == p_32))))))
                    { /* block id: 859 */
                        return l_1881;
                    }
                    else
                    { /* block id: 861 */
                        int8_t l_1882[5][2] = {{(-8L),0xCFL},{(-8L),0xCFL},{(-8L),0xCFL},{(-8L),0xCFL},{(-8L),0xCFL}};
                        int32_t l_1884 = 0x15EFE0DCL;
                        int32_t l_1885 = 0xFA58E74FL;
                        int i, j;
                        g_1887++;
                        l_1884 = (*p_30);
                    }
                    for (g_631 = 0; (g_631 > 22); g_631 = safe_add_func_int32_t_s_s(g_631, 8))
                    { /* block id: 867 */
                        uint16_t l_1892 = 0x3A12L;
                        int32_t l_1911 = 2L;
                        int32_t l_1912 = (-10L);
                        int32_t l_1913 = 0L;
                        int32_t l_1914 = 0x224ECECDL;
                        int32_t l_1916[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_1916[i] = 0L;
                        --l_1892;
                        l_1910 |= (!(((l_1883[0][4][0] & (safe_mul_func_int8_t_s_s((p_32 , (*l_1673)), (safe_sub_func_uint16_t_u_u((l_1877 >= ((((((*g_57) = l_1877) ^ ((+l_1853) & (l_1901 &= (l_1850[2][5] = 0x662AL)))) & (safe_lshift_func_uint8_t_u_u((((safe_lshift_func_uint16_t_u_u(((*l_1870)--), (l_1853 ^ ((void*)0 != l_1908)))) , 0x339583F0810B812ALL) > p_32), l_1854))) , 0L) != p_31)), p_31))))) | l_1909) <= l_1892));
                        --l_1923;
                        g_308 = &g_309;
                    }
                    for (g_1637 = 1; (g_1637 <= 4); g_1637 += 1)
                    { /* block id: 879 */
                        int i, j;
                        (**l_1861) = &l_1853;
                    }
                    if (((&g_1686 == ((*l_1935) = (g_1934 = (((((**g_872) = p_31) >= ((((*g_1633) , (safe_add_func_float_f_f(p_32, (safe_mul_func_float_f_f((((*l_1932) = l_1877) > (&l_1883[0][2][3] == (void*)0)), p_32))))) > ((((void*)0 != l_1933) , 0xB.09701Ap+79) < p_32)) <= (-0x10.3p-1))) <= 0xB.541473p-34) , (void*)0)))) != g_20))
                    { /* block id: 886 */
                        uint32_t ***l_1937 = &g_308;
                        (*l_1937) = &g_309;
                        l_1938 = (void*)0;
                    }
                    else
                    { /* block id: 889 */
                        uint32_t l_1939 = 0xF645EF38L;
                        ++l_1939;
                        l_1942++;
                        (**g_1041) = &p_31;
                    }
                }
                for (g_861 = 0; (g_861 < 8); ++g_861)
                { /* block id: 897 */
                    uint32_t l_1947 = 0xFA4904A9L;
                    int32_t l_1951 = (-1L);
                    int32_t l_1954[2];
                    int32_t *l_1959 = (void*)0;
                    uint32_t **l_1989 = &g_1934;
                    uint8_t l_2005[4] = {0x35L,0x35L,0x35L,0x35L};
                    const int8_t *l_2020 = &g_343[0];
                    const int8_t **l_2019 = &l_2020;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1954[i] = 0xF6F62E9CL;
                    l_1947 &= (*p_30);
                    for (g_93 = 0; (g_93 <= 2); g_93 += 1)
                    { /* block id: 901 */
                        int32_t l_1948 = (-3L);
                        int32_t l_1949 = 0x86C9640DL;
                        int32_t l_1950 = 0x663C5CA6L;
                        int32_t l_1952 = 0xE3BD190CL;
                        int32_t l_1953 = 0x17D755A5L;
                        int32_t l_1955[6][5] = {{0x4282774DL,(-1L),0L,0x4282774DL,0L},{0x4282774DL,0x4282774DL,0x99A30E0EL,0xC55EE082L,0xFEE3D8A7L},{0x9E9DCDA9L,0xFEE3D8A7L,0L,0L,0xFEE3D8A7L},{0xFEE3D8A7L,(-1L),0x9E9DCDA9L,0xFEE3D8A7L,0L},{0xC55EE082L,0xFEE3D8A7L,0x99A30E0EL,0xFEE3D8A7L,0xC55EE082L},{0x9E9DCDA9L,0x4282774DL,(-1L),0L,0x4282774DL}};
                        int i, j;
                        ++l_1956;
                        g_1749 = &g_1633;
                        return l_1959;
                    }
                    if ((((safe_mod_func_int16_t_s_s(((safe_mod_func_uint32_t_u_u(p_32, p_31)) , (((safe_div_func_int16_t_s_s((((*g_1749) = (*g_1749)) == l_1839), p_31)) | (((**l_1983) = (safe_sub_func_int32_t_s_s(((safe_mul_func_float_f_f((safe_add_func_float_f_f(p_32, ((safe_sub_func_float_f_f(0xB.389EEEp+89, ((safe_mul_func_float_f_f((safe_mul_func_float_f_f((*g_192), ((safe_add_func_float_f_f(((l_1981 = (l_1980 , (void*)0)) != l_1983), (*g_873))) < (***g_871)))), (*g_873))) != p_31))) > p_32))), p_31)) , (*p_29)), 0xF64D202CL))) == l_1985)) == l_1986)), (*g_1461))) || (*p_30)) , (*p_30)))
                    { /* block id: 909 */
                        int16_t l_1990 = (-4L);
                        p_31 &= (l_1910 = (*p_30));
                        p_31 = (((((l_1991 = (l_1990 |= ((((((void*)0 != &l_1871[0]) , (p_31 & 0x69L)) | 0x9A1FL) != (safe_div_func_uint64_t_u_u(1UL, ((&g_1934 == l_1989) ^ (0x28F2ADF3L < p_31))))) && (**g_1847)))) != 2L) , 0x7.5p+1) , 0x755DL) ^ 1UL);
                    }
                    else
                    { /* block id: 915 */
                        int16_t l_2004 = 0x2EF5L;
                        if ((*p_29))
                            break;
                        if ((*p_29))
                            continue;
                        l_2005[1] &= (g_141 | ((((safe_add_func_uint64_t_u_u(p_31, p_31)) == p_32) < ((((((((((0x3726L & ((safe_rshift_func_int16_t_s_u((((((((safe_add_func_int64_t_s_s(0xD065F1DD1A80EF56LL, (((safe_lshift_func_uint16_t_u_u((((((0L == ((0x5AL != ((p_32 , 0x425BL) > p_32)) && p_32)) & g_100[2][0]) , &l_1881) == (void*)0) < (*g_309)), 13)) | (*l_1673)) ^ 0xCD9AL))) , 1UL) >= (*g_1633)) , p_31) || p_31) != (*g_309)) ^ l_2004), 1)) >= 18446744073709551609UL)) <= (*g_1461)) == 0UL) == 6L) < 0UL) , p_31) , &l_1981) != &l_1983) , p_32) == g_140[0][0])) == (*p_30)));
                    }
                    for (g_1551 = 0; (g_1551 <= 0); g_1551 += 1)
                    { /* block id: 922 */
                        uint32_t l_2008 = 4UL;
                        int8_t ***l_2018 = &l_2017;
                        int32_t l_2026 = (-1L);
                        int i;
                        l_1871[g_1551] = (((safe_div_func_int16_t_s_s((l_2008 < l_2008), ((safe_add_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(((***g_326) | ((((*l_2018) = l_2017) != l_2019) != (0x3FL || (p_32 > (l_2026 = (safe_mul_func_int8_t_s_s((0x48L <= ((safe_div_func_uint8_t_u_u(p_32, 0xEBL)) | l_2025)), p_31))))))), 8)), 1L)), l_2008)), (*p_30))) || p_31))) , l_2026) > 1UL);
                        if ((*p_29))
                            break;
                        l_1951 |= ((((**g_1847) && ((((safe_rshift_func_uint16_t_u_s((((safe_unary_minus_func_uint32_t_u((g_548 ^= (safe_div_func_uint64_t_u_u(p_31, ((((***g_536) ^ ((((safe_div_func_uint64_t_u_u(((*l_1839)++), (-9L))) , (((safe_sub_func_float_f_f(((**g_872) = (-0x10.Fp-1)), ((-0x6.5p+1) <= (p_32 , 0x8.2C914Ap+42)))) , (safe_sub_func_int8_t_s_s(((((safe_mul_func_uint16_t_u_u(((safe_add_func_float_f_f(((((((0x333E7DB2L >= (*p_29)) > p_32) , g_1462) , g_1009[7]) , 65535UL) , (-0x1.2p-1)), p_31)) , g_1674), 1UL)) == (*g_1633)) & l_2008) ^ g_343[0]), 0L))) & 18446744073709551608UL)) < p_31) >= (**g_308))) , p_33) , 0xAF3479C76EB92F22LL)))))) <= g_1724[3]) , p_31), 12)) , (*g_309)) <= p_31) && 0xB18B326EL)) & (*g_1633)) | g_95);
                        if (l_2045)
                            break;
                    }
                }
                for (l_1918 = 0; (l_1918 >= (-15)); l_1918--)
                { /* block id: 936 */
                    const int64_t l_2050[2][2] = {{0L,0L},{0L,0L}};
                    int32_t l_2051[5][10] = {{0xDFF7DC89L,0x7B00E53EL,0x336ACD22L,0x638BDEACL,0xFA1895D7L,0x638BDEACL,0x336ACD22L,0x7B00E53EL,0xDFF7DC89L,0xE5ADAEEBL},{0xDFF7DC89L,0L,0x05AF1E8CL,0xD97655BAL,0x638BDEACL,0xE5ADAEEBL,0xE5ADAEEBL,0x638BDEACL,0xD97655BAL,0x05AF1E8CL},{0xE5ADAEEBL,0xE5ADAEEBL,0x638BDEACL,0xD97655BAL,0x05AF1E8CL,0L,0xDFF7DC89L,0xD6B4A62AL,0xDFF7DC89L,0L},{0x336ACD22L,0x638BDEACL,0xFA1895D7L,0x638BDEACL,0x336ACD22L,0x7B00E53EL,0xDFF7DC89L,0xE5ADAEEBL,1L,1L},{0xD97655BAL,0xE5ADAEEBL,0x415DDE2EL,0L,0L,0x415DDE2EL,0xE5ADAEEBL,0xD97655BAL,0xFA1895D7L,1L}};
                    int32_t l_2079 = 0x89F42C60L;
                    int32_t l_2082 = 0xDFCC9F71L;
                    int i, j;
                    l_2051[1][0] &= (((safe_rshift_func_uint8_t_u_u(p_32, 7)) & (p_31 == 1L)) , l_2050[1][1]);
                    if (((*g_1311) == (*g_1311)))
                    { /* block id: 938 */
                        return l_2052;
                    }
                    else
                    { /* block id: 940 */
                        float * const ***l_2059 = (void*)0;
                        uint64_t *l_2062 = (void*)0;
                        uint64_t *l_2063[7][2][8] = {{{&g_122,&l_1923,&l_1991,(void*)0,(void*)0,&l_2060,&l_1991,&g_122},{&l_1991,&l_2060,&l_1991,(void*)0,(void*)0,&l_1991,&l_2060,&l_1991}},{{&g_122,&l_1991,&l_2060,&g_122,&l_1923,&l_2060,&l_1991,&l_2060},{&g_122,(void*)0,&g_122,&l_2060,&g_122,&l_2060,&g_122,(void*)0}},{{&l_2060,&l_1991,&l_2060,&l_1991,&g_122,&l_1991,&l_1923,&g_122},{(void*)0,&l_2060,(void*)0,&l_1923,&l_2060,&l_2060,&l_1923,(void*)0}},{{&l_1923,&l_1923,&l_2060,&l_1991,(void*)0,&l_2060,&g_122,&g_122},{(void*)0,&l_2060,&g_122,&g_122,&l_1991,(void*)0,&l_1991,&g_122}},{{&l_2060,&l_2060,&l_2060,&l_1991,&l_1991,(void*)0,&l_2060,(void*)0},{&l_1991,&g_122,&l_1991,&l_1923,&g_122,&l_1991,&l_1991,&g_122}},{{&l_1991,&l_1991,&l_1991,&l_1991,&l_1991,&l_1923,&g_122,(void*)0},{&l_2060,&g_122,&l_1923,&l_2060,&l_1991,&l_2060,(void*)0,&l_2060}},{{(void*)0,&g_122,&l_1991,&l_1923,&l_2060,&g_122,&l_2060,&l_1991},{&g_122,&g_122,&l_2060,(void*)0,&l_1991,&l_2060,&l_1923,&l_1923}}};
                        int16_t ***l_2072 = (void*)0;
                        int16_t ***l_2073 = (void*)0;
                        int16_t **l_2075 = &g_1052[2];
                        int16_t ***l_2074 = &l_2075;
                        uint32_t *l_2076 = (void*)0;
                        uint32_t *l_2077 = &l_1771;
                        int32_t l_2078[4];
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                            l_2078[i] = 7L;
                        l_2079 ^= (safe_mul_func_int16_t_s_s((safe_sub_func_int8_t_s_s(((l_2051[0][0] = (safe_mod_func_int16_t_s_s(((g_144 = ((*l_1839) &= (l_2059 != (*g_1311)))) == (l_2060 != 1L)), l_2061[1]))) >= ((((((*l_2077) = (safe_rshift_func_int8_t_s_s(l_2050[1][1], (safe_div_func_uint32_t_u_u(((**g_308) ^ ((safe_rshift_func_uint8_t_u_s((safe_mod_func_int32_t_s_s((&g_1052[1] == ((*l_2074) = &l_1654[4][0])), (*p_29))), l_2050[0][0])) >= g_162)), (*g_309)))))) , (**g_537)) || 0x72L) ^ l_2078[3]) && 0xBEDCC590L)), 0x23L)), g_141));
                        if ((*p_29))
                            break;
                        l_1871[0] = (p_31 ^= 0xE26C12A7L);
                    }
                    l_2082 &= (l_2080 == l_2081[0][6]);
                }
            }
            for (g_89 = 0; (g_89 <= 3); g_89 += 1)
            { /* block id: 956 */
                float *l_2105[9][8] = {{&l_1796[3][5][0],&l_1796[4][3][0],&g_193,&g_193,&l_1796[4][3][0],&l_1796[3][5][0],&g_193,&g_257},{&g_257,&l_1796[3][2][0],&g_245,(void*)0,(void*)0,&l_1816,&l_1796[3][5][0],&l_1796[5][2][0]},{&g_245,&g_92,&l_1796[5][2][0],(void*)0,&l_1796[5][2][0],&g_92,&g_245,&g_257},{&l_1796[4][3][0],&l_1796[5][2][0],&l_1796[1][0][0],&g_193,&l_1816,&g_257,&g_193,(void*)0},{(void*)0,&g_1361,&g_92,&l_1816,&l_1816,&g_92,&g_1361,(void*)0},{&l_1796[4][3][0],(void*)0,&g_92,(void*)0,&l_1796[5][2][0],&g_1361,&l_1796[1][0][0],&l_1796[3][2][0]},{&g_245,&l_1796[3][5][0],&g_193,&g_1361,(void*)0,&g_1361,&g_193,&l_1796[3][5][0]},{&g_257,(void*)0,&l_1796[3][2][0],&g_193,&l_1796[4][3][0],&g_92,&l_1796[5][2][0],&g_193},{&l_1796[3][5][0],&g_1361,&l_1816,&l_1796[5][2][0],&g_257,&g_257,&l_1796[5][2][0],&l_1816}};
                int32_t l_2123 = 0L;
                int32_t l_2124 = 0xDE8027EFL;
                int32_t l_2125 = 0x0E8CD9E3L;
                int32_t l_2127 = 0xF6D77719L;
                int32_t l_2128 = 0x20F6FD10L;
                int16_t ***l_2137[2][5] = {{&g_2135,&g_2135,&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135,&g_2135,&g_2135}};
                int32_t *l_2157 = &l_1917;
                int32_t l_2162 = 0xAFADE91FL;
                int32_t l_2163 = 4L;
                int32_t l_2164 = 2L;
                int32_t l_2165 = 0L;
                int32_t l_2166 = 1L;
                int32_t l_2167 = 0xBE601683L;
                int32_t l_2168 = 0x4BDECE06L;
                int32_t l_2169 = 9L;
                int32_t l_2170[4][7] = {{0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L},{3L,3L,3L,3L,3L,3L,3L},{0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L,0xE1D1D076L},{3L,3L,3L,3L,3L,3L,3L}};
                float l_2171 = (-0x6.Dp+1);
                uint16_t *l_2179 = &g_93;
                uint16_t **l_2178[8][3][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
                uint32_t l_2191 = 0xFCC0D0A2L;
                uint32_t l_2206 = 1UL;
                const int32_t ***l_2223 = &g_697;
                const int32_t ****l_2222[4][9][1] = {{{&l_2223},{(void*)0},{&l_2223},{&l_2223},{(void*)0},{&l_2223},{(void*)0},{&l_2223},{&l_2223}},{{(void*)0},{&l_2223},{(void*)0},{&l_2223},{&l_2223},{(void*)0},{&l_2223},{(void*)0},{&l_2223}},{{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223}},{{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223},{&l_2223}}};
                uint64_t ***l_2230[10][3] = {{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669},{&l_1669,&l_1669,&l_1669}};
                uint8_t *l_2261 = (void*)0;
                int32_t l_2294 = (-2L);
                int i, j, k;
                for (l_1991 = 0; (l_1991 <= 0); l_1991 += 1)
                { /* block id: 959 */
                    int i, j;
                    for (g_93 = 0; (g_93 <= 4); g_93 += 1)
                    { /* block id: 962 */
                        int i, j;
                        return l_1821[g_93][g_93];
                    }
                    if (l_1850[g_89][(g_89 + 2)])
                        continue;
                }
            }
        }
        else
        { /* block id: 1047 */
            uint32_t l_2310 = 4294967295UL;
            int16_t l_2312 = (-1L);
            int32_t l_2314 = 0xC72D4844L;
            int32_t l_2315 = 0xE430BB10L;
            int32_t l_2317 = (-8L);
            int32_t l_2318 = 4L;
            int32_t l_2320 = 0x3E15AEE1L;
            int32_t l_2322 = 0x7D3ECF2BL;
            int32_t l_2323 = 0x7757332DL;
            int32_t l_2324[9] = {(-1L),0xED079876L,0xED079876L,(-1L),0xED079876L,0xED079876L,(-1L),0xED079876L,0xED079876L};
            float l_2335 = 0xA.CA662Bp+16;
            int8_t l_2371 = 0x2DL;
            int32_t **l_2417 = &l_2217;
            int64_t ***l_2420 = &g_1749;
            uint32_t l_2445 = 2UL;
            int32_t *l_2453[4] = {&l_1918,&l_1918,&l_1918,&l_1918};
            int32_t l_2460 = 1L;
            int32_t l_2461 = (-1L);
            uint32_t l_2516 = 0x06BFA051L;
            int32_t *l_2523 = &l_1920;
            uint8_t ***l_2531 = (void*)0;
            uint32_t ***l_2536 = &l_2377;
            uint32_t ****l_2537 = &l_2536;
            uint32_t ****l_2538 = (void*)0;
            uint32_t ****l_2539[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t l_2620 = 4294967289UL;
            int i;
            for (g_1686 = 0; (g_1686 < 9); ++g_1686)
            { /* block id: 1050 */
                int16_t l_2306 = 0xDC1AL;
                int32_t l_2307[10][6] = {{1L,0xC3B13D28L,1L,1L,1L,0xC3B13D28L},{1L,0xC3B13D28L,(-1L),1L,1L,1L},{0x4BC294E5L,0xC3B13D28L,0xC3B13D28L,0x4BC294E5L,1L,(-1L)},{1L,0xC3B13D28L,1L,1L,1L,0xC3B13D28L},{1L,0xC3B13D28L,(-1L),1L,1L,1L},{0x4BC294E5L,0xC3B13D28L,0xC3B13D28L,0x4BC294E5L,1L,(-1L)},{1L,0xC3B13D28L,1L,1L,1L,0xC3B13D28L},{1L,0xC3B13D28L,(-1L),1L,1L,1L},{0x4BC294E5L,0xC3B13D28L,0xC3B13D28L,0x4BC294E5L,1L,(-1L)},{1L,0xC3B13D28L,1L,1L,1L,0xC3B13D28L}};
                const uint16_t l_2311 = 0x350DL;
                uint64_t ***l_2338[5][3];
                uint32_t l_2363 = 4UL;
                int32_t ***l_2379 = (void*)0;
                int32_t l_2400 = 0x31C75F7FL;
                float l_2426 = 0xC.B674F2p+30;
                float ***l_2441 = &g_872;
                float ****l_2443[6];
                float *****l_2442 = &l_2443[0];
                int32_t *l_2454[7][8] = {{&g_58,&g_58,&g_58,&g_58,&g_861,&g_58,&g_58,&g_58},{&g_58,&g_861,&l_1798,&l_1798,&g_861,&g_58,&g_861,&l_1798},{&g_58,&g_861,&g_58,&g_58,&g_58,&g_58,&g_861,&g_58},{&l_1915,&g_58,&l_1798,&g_58,&l_1915,&l_1915,&g_58,&l_1798},{&l_1915,&l_1915,&g_58,&l_1798,&g_58,&l_1915,&l_1915,&g_58},{&g_58,&g_58,&g_58,&g_58,&g_861,&g_58,&g_58,&g_58},{&g_58,&g_861,&l_1798,&l_1798,&g_861,&g_58,&g_861,&l_1798}};
                uint32_t *l_2521 = (void*)0;
                int16_t l_2522[2][1][2] = {{{0xA570L,0xA570L}},{{0xA570L,0xA570L}}};
                int i, j, k;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_2338[i][j] = &l_1669;
                }
                for (i = 0; i < 6; i++)
                    l_2443[i] = &l_2440;
                l_2312 = (safe_add_func_uint8_t_u_u(0xA4L, ((!(safe_lshift_func_uint16_t_u_s(g_125, (safe_sub_func_uint32_t_u_u(p_32, (l_2307[9][5] = l_2306)))))) , (safe_add_func_int32_t_s_s((l_2310 = l_2306), l_2311)))));
            }
            if ((safe_mod_func_uint64_t_u_u((safe_mod_func_int8_t_s_s(l_2528, (g_140[5][5] , (safe_rshift_func_int16_t_s_u((p_32 & (l_2531 != ((safe_mul_func_uint16_t_u_u(p_31, (((***l_2420) = 0x50A38D1D9C0AD585LL) , (p_31 ^ ((((((safe_mul_func_int16_t_s_s(((*l_2523) = ((p_33 == ((g_2540 = ((*l_2537) = l_2536)) != &g_308)) , p_32)), (*l_1673))) , p_32) <= p_33) <= (-0x1.3p+1)) > (*g_192)) , 0x9EL))))) , (void*)0))), g_989))))), 0x3E0F7A034CF753F3LL)))
            { /* block id: 1152 */
                (**l_1861) = &p_31;
            }
            else
            { /* block id: 1154 */
                int32_t l_2558 = 9L;
                l_2558 ^= (((((p_31 > (safe_rshift_func_int16_t_s_s(1L, 3))) != p_32) & (((l_2550 = l_2545[4][5][0]) != (g_2551 = g_2551)) > (((*g_2494) , (safe_add_func_float_f_f((safe_sub_func_float_f_f((p_32 != (p_32 , ((safe_sub_func_uint16_t_u_u((p_31 == 1L), g_100[4][2])) , p_32))), p_31)), p_32))) , p_31))) , g_1462) || (*l_2523));
            }
            if (((p_31 , ((p_30 == (*l_2216)) | (safe_rshift_func_int8_t_s_s(((safe_add_func_uint16_t_u_u(0xEC44L, p_32)) <= (((+((l_2377 == (void*)0) & (l_2147 == ((((safe_sub_func_uint16_t_u_u(p_31, g_1674)) , (void*)0) != (*l_2365)) , l_2566)))) & l_2567) , (*p_29))), p_31)))) & 0x7CL))
            { /* block id: 1159 */
                float *l_2568 = &l_2106;
                int32_t l_2576[4];
                int32_t l_2582 = 1L;
                int16_t l_2592 = 8L;
                int i;
                for (i = 0; i < 4; i++)
                    l_2576[i] = 0x55CBF585L;
                (*l_2568) = ((***g_871) = 0xB.19805Bp+62);
                (*g_57) = (((***g_536) != (((((((((***l_2547) ^= (p_31 & p_31)) <= (((safe_lshift_func_int8_t_s_u(((~((*l_1869) = (l_2576[0] &= (0x6AL || 0UL)))) >= ((((*g_309) = 4294967294UL) >= ((*l_2523) = (p_32 , (safe_unary_minus_func_uint64_t_u((safe_mul_func_uint8_t_u_u(248UL, ((safe_sub_func_int64_t_s_s((**g_1749), (**g_1749))) , p_32)))))))) < p_32)), l_2582)) == (*g_965)) , p_31)) , l_2582) != (*p_30)) > g_125) > p_32) , &g_1934) == (void*)0)) == p_32);
                l_2592 |= ((safe_rshift_func_int16_t_s_s((*g_1461), (((*p_29) >= (p_32 > p_31)) && 1L))) , ((*l_2429) = (safe_lshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_u(((!p_31) == (--(****l_2546))), 2)), 1))));
            }
            else
            { /* block id: 1171 */
                uint32_t l_2597 = 0x924F4725L;
                int32_t *l_2603 = &l_2460;
                int32_t *l_2604 = &l_1753;
                (**g_1041) = &p_31;
                if ((*p_29))
                    break;
                for (g_635 = 26; (g_635 == 29); g_635 = safe_add_func_uint8_t_u_u(g_635, 4))
                { /* block id: 1176 */
                    uint32_t l_2598 = 0xFD2900D8L;
                    int32_t *l_2601 = &l_2318;
                    uint16_t **l_2626 = &l_2180[2];
                    uint16_t ***l_2625 = &l_2626;
                    l_2598 |= (safe_sub_func_int16_t_s_s((*g_1461), l_2597));
                    for (l_2314 = 0; (l_2314 < 13); l_2314 = safe_add_func_uint8_t_u_u(l_2314, 9))
                    { /* block id: 1180 */
                        int32_t *l_2602[7] = {(void*)0,&l_1920,&g_58,&l_1920,&g_58,&g_58,&l_1920};
                        int i;
                        return l_2605;
                    }
                    for (l_1991 = (-22); (l_1991 == 48); l_1991++)
                    { /* block id: 1185 */
                        int32_t *l_2613 = &l_2315;
                        (**l_2313) = func_48(g_2608, ((****l_2537)++), ((*l_2604) = (++(****l_2546))), p_32, l_2613);
                    }
                    (*l_2523) = (safe_lshift_func_uint16_t_u_u(((5L | (*l_2601)) | (((*l_2601) && ((safe_rshift_func_int16_t_s_u(((*l_2601) && 0xC452L), 5)) > (++l_2620))) || (g_2623[1] != ((*l_2625) = (void*)0)))), p_32));
                }
            }
            p_31 ^= ((*l_2429) = 5L);
        }
    }
    g_2642 ^= ((safe_add_func_uint32_t_u_u((safe_sub_func_int32_t_s_s(0x7B8874CCL, p_31)), ((safe_div_func_uint8_t_u_u((((&l_2392 == (void*)0) & ((((1UL != 8UL) >= ((safe_unary_minus_func_uint64_t_u((g_144 ^= (((safe_div_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u(p_31, p_31)) || (0x2344L != 65529UL)), 0x635D657EL)) || p_31) ^ p_32)))) <= l_2640)) || p_31) != p_32)) <= 0xF5L), p_31)) | l_2641[1]))) != 0xA38F3E7F2439C395LL);
    for (g_2467 = 9; (g_2467 < 11); g_2467++)
    { /* block id: 1204 */
        const uint32_t l_2650 = 0x4C8843B9L;
        int32_t *l_2674 = &g_861;
        for (l_1739 = (-3); (l_1739 > 32); ++l_1739)
        { /* block id: 1207 */
            int32_t *l_2649 = &g_58;
            for (g_122 = 0; (g_122 == 21); g_122++)
            { /* block id: 1210 */
                (**g_518) = &p_31;
                return l_2649;
            }
            (***g_871) = l_2650;
        }
        for (l_1657 = 0; (l_1657 >= 32); l_1657 = safe_add_func_uint32_t_u_u(l_1657, 1))
        { /* block id: 1218 */
            uint8_t l_2657 = 251UL;
            float ****l_2662 = &g_871;
            for (l_1980 = 0; (l_1980 <= 0); l_1980 += 1)
            { /* block id: 1221 */
                uint16_t l_2654 = 0x676AL;
                float ****l_2660[7][1][10] = {{{&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871}},{{&g_871,&g_871,(void*)0,&g_871,&g_871,(void*)0,&g_871,&g_871,(void*)0,&g_871}},{{&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871}},{{&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871}},{{&g_871,&g_871,(void*)0,&g_871,&g_871,(void*)0,&g_871,&g_871,(void*)0,&g_871}},{{&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871}},{{&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871}}};
                float *****l_2661[6][4][9] = {{{&l_2660[6][0][4],&l_2660[1][0][0],&l_2660[6][0][0],(void*)0,&l_2660[0][0][1],&l_2660[6][0][0],(void*)0,&l_2660[6][0][0],&l_2660[3][0][0]},{&l_2660[3][0][5],&l_2660[6][0][0],&l_2660[0][0][1],&l_2660[5][0][3],(void*)0,(void*)0,(void*)0,&l_2660[6][0][0],&l_2660[3][0][0]},{&l_2660[6][0][0],(void*)0,(void*)0,&l_2660[6][0][0],&l_2660[0][0][5],&l_2660[6][0][0],&l_2660[0][0][4],(void*)0,(void*)0},{&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[5][0][8],&l_2660[6][0][0]}},{{&l_2660[0][0][5],&l_2660[6][0][0],&l_2660[6][0][0],(void*)0,&l_2660[6][0][0],&l_2660[0][0][6],&l_2660[6][0][0],&l_2660[6][0][0],(void*)0},{&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[0][0][5],&l_2660[6][0][9],(void*)0,&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[5][0][8],&l_2660[6][0][4]},{&l_2660[0][0][5],(void*)0,&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[1][0][0],&l_2660[6][0][0],&l_2660[1][0][4],(void*)0,&l_2660[6][0][4]},{(void*)0,&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[0][0][5],(void*)0,&l_2660[1][0][4],&l_2660[6][0][0],&l_2660[6][0][0],(void*)0}},{{(void*)0,&l_2660[6][0][0],&l_2660[0][0][4],&l_2660[0][0][9],&l_2660[5][0][8],(void*)0,&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[0][0][6]},{&l_2660[4][0][8],&l_2660[6][0][0],&l_2660[3][0][5],(void*)0,&l_2660[6][0][0],(void*)0,&l_2660[1][0][4],&l_2660[1][0][4],(void*)0},{&l_2660[6][0][0],&l_2660[0][0][6],&l_2660[6][0][0],&l_2660[0][0][6],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[1][0][0]},{(void*)0,&l_2660[6][0][0],&l_2660[0][0][8],&l_2660[6][0][0],&l_2660[6][0][4],&l_2660[6][0][0],&l_2660[6][0][0],(void*)0,&l_2660[6][0][0]}},{{&l_2660[1][0][0],&l_2660[3][0][0],&l_2660[6][0][4],&l_2660[6][0][0],&l_2660[5][0][2],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0]},{(void*)0,&l_2660[5][0][2],&l_2660[0][0][4],(void*)0,&l_2660[6][0][0],&l_2660[5][0][8],&l_2660[6][0][9],&l_2660[6][0][0],&l_2660[6][0][0]},{(void*)0,&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[4][0][5],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[0][0][4],&l_2660[3][0][5],&l_2660[6][0][0]},{&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][9],&l_2660[6][0][0],(void*)0,&l_2660[6][0][0],(void*)0,&l_2660[4][0][8]}},{{&l_2660[1][0][4],&l_2660[6][0][0],(void*)0,&l_2660[0][0][1],&l_2660[6][0][0],&l_2660[6][0][4],&l_2660[6][0][4],&l_2660[6][0][0],&l_2660[0][0][1]},{&l_2660[5][0][8],(void*)0,&l_2660[5][0][8],(void*)0,(void*)0,&l_2660[0][0][9],&l_2660[6][0][0],&l_2660[6][0][0],(void*)0},{&l_2660[6][0][0],&l_2660[0][0][4],&l_2660[0][0][5],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[3][0][5],&l_2660[6][0][0],&l_2660[5][0][8],&l_2660[6][0][9]},{&l_2660[6][0][0],&l_2660[6][0][0],(void*)0,(void*)0,&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[0][0][5],&l_2660[6][0][0],&l_2660[3][0][5]}},{{&l_2660[3][0][0],&l_2660[6][0][0],(void*)0,&l_2660[0][0][1],(void*)0,&l_2660[4][0][8],&l_2660[6][0][0],&l_2660[6][0][0],(void*)0},{&l_2660[0][0][1],&l_2660[6][0][0],&l_2660[0][0][8],&l_2660[6][0][9],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[6][0][0],&l_2660[0][0][9],(void*)0},{&l_2660[0][0][9],(void*)0,&l_2660[0][0][8],&l_2660[4][0][5],&l_2660[6][0][0],(void*)0,(void*)0,(void*)0,&l_2660[1][0][4]},{&l_2660[0][0][5],&l_2660[6][0][0],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_2660[6][0][0]}}};
                float **l_2666 = &g_873;
                float *** const l_2665 = &l_2666;
                float *** const *l_2664 = &l_2665;
                float *** const **l_2663[4][5][10] = {{{(void*)0,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,&l_2664},{&l_2664,(void*)0,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,(void*)0,(void*)0},{(void*)0,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664},{&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,&l_2664},{(void*)0,&l_2664,&l_2664,&l_2664,(void*)0,(void*)0,&l_2664,(void*)0,&l_2664,&l_2664}},{{&l_2664,(void*)0,(void*)0,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664},{(void*)0,&l_2664,&l_2664,(void*)0,(void*)0,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664},{&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664},{&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,(void*)0},{&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,(void*)0,&l_2664}},{{&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,(void*)0},{&l_2664,&l_2664,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_2664,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2664,(void*)0,&l_2664,&l_2664,(void*)0,&l_2664,(void*)0},{&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664},{&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0,(void*)0}},{{&l_2664,&l_2664,&l_2664,(void*)0,(void*)0,(void*)0,&l_2664,&l_2664,&l_2664,(void*)0},{&l_2664,&l_2664,&l_2664,(void*)0,(void*)0,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0},{&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,(void*)0,&l_2664,(void*)0,&l_2664,&l_2664},{&l_2664,&l_2664,&l_2664,(void*)0,&l_2664,&l_2664,&l_2664,&l_2664,&l_2664,(void*)0},{&l_2664,(void*)0,(void*)0,(void*)0,&l_2664,&l_2664,(void*)0,&l_2664,(void*)0,&l_2664}}};
                float *** const *l_2667 = (void*)0;
                int i, j, k;
                for (l_2161 = 0; (l_2161 <= 0); l_2161 += 1)
                { /* block id: 1224 */
                    int32_t l_2653 = (-1L);
                    int i;
                    l_2654--;
                    l_2657++;
                }
                (***l_2440) = (((l_2662 = l_2660[6][0][0]) != (l_2667 = &g_871)) == ((-p_32) != (-0x4.3p+1)));
            }
        }
        for (l_1886 = 4; (l_1886 <= (-11)); l_1886 = safe_sub_func_int32_t_s_s(l_1886, 2))
        { /* block id: 1235 */
            int32_t *l_2673 = &l_1920;
            l_2671 |= l_2650;
            (**g_1041) = &p_31;
            return l_2674;
        }
    }
    return l_2676;
}


/* ------------------------------------------ */
/* 
 * reads : g_281 g_18 g_1058 g_656 g_282 g_309 g_89 g_56 g_58 g_93 g_872 g_873 g_811 g_192 g_308 g_100 g_535 g_539 g_144 g_655 g_122 g_163 g_326 g_327 g_328 g_244 g_96 g_538 g_1311 g_1312 g_95 g_1327 g_325 g_139 g_140 g_6 g_1540 g_1551 g_1557 g_1461 g_1462 g_631 g_802 g_343 g_965 g_861 g_138 g_349 g_1637 g_2740
 * writes: g_281 g_56 g_343 g_58 g_93 g_811 g_193 g_282 g_535 g_144 g_122 g_163 g_245 g_95 g_631 g_989 g_1551 g_1557 g_1540 g_883 g_1327 g_1632 g_100 g_2740
 */
static float  func_37(int8_t  p_38, int32_t * p_39, int32_t * p_40, uint64_t  p_41)
{ /* block id: 635 */
    int64_t l_1402 = (-6L);
    int32_t l_1437 = 8L;
    int32_t l_1447[10] = {0x93B47CF1L,0xC0D261CDL,0x93B47CF1L,0xC0D261CDL,0x93B47CF1L,0xC0D261CDL,0x93B47CF1L,0xC0D261CDL,0x93B47CF1L,0xC0D261CDL};
    int32_t ****l_1475 = &g_1041;
    int16_t l_1492 = 0x5283L;
    uint8_t *l_1563 = &g_535;
    uint8_t **l_1562 = &l_1563;
    uint8_t *** const l_1561 = &l_1562;
    uint8_t ***l_1596 = &l_1562;
    uint8_t ****l_1595 = &l_1596;
    uint8_t *****l_1594 = &l_1595;
    int64_t **l_1634 = &g_1633;
    int i;
    if ((*p_39))
    { /* block id: 636 */
        uint8_t *l_1415 = &g_56;
        int32_t l_1418[10][10][2] = {{{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L}},{{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L}},{{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL}},{{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xAA54178EL},{0x8CD93205L,0x8CD93205L},{0xAA54178EL,0x8CD93205L},{0x8CD93205L,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL}},{{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL}},{{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL}},{{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL}},{{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL}},{{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL}},{{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL},{0xC233533EL,0xAA54178EL},{0xAA54178EL,0xC233533EL},{0xAA54178EL,0xAA54178EL}}};
        int8_t *l_1419 = &g_343[0];
        int32_t *l_1420 = &g_58;
        uint32_t **l_1432 = &g_309;
        uint64_t *l_1435[10][2] = {{(void*)0,&g_144},{&g_144,(void*)0},{&g_144,&g_144},{(void*)0,&g_144},{&g_144,(void*)0},{&g_144,&g_144},{(void*)0,&g_144},{&g_144,(void*)0},{&g_144,&g_144},{(void*)0,&g_144}};
        uint64_t **l_1434[3];
        float ***l_1458[3][5][5] = {{{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872}},{{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872}},{{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872},{&g_872,&g_872,&g_872,&g_872,&g_872}}};
        float l_1491 = (-0x3.2p-1);
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1434[i] = &l_1435[8][0];
        (*l_1420) &= (safe_mod_func_uint32_t_u_u((safe_div_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((g_1058[2][0] < l_1402), (safe_add_func_int64_t_s_s(p_38, (safe_rshift_func_int8_t_s_s((((**g_656) ^= (*p_40)) != (*g_309)), (l_1402 != (((safe_mul_func_int8_t_s_s(((*l_1419) = (safe_add_func_int32_t_s_s(((-3L) > 255UL), (safe_mod_func_uint64_t_u_u((safe_add_func_uint8_t_u_u((--(*l_1415)), l_1418[0][2][1])), l_1402))))), 255UL)) , 0xCAC2L) , p_38)))))))) && 0x65L), p_38)), 1L));
        for (g_93 = 0; (g_93 <= 6); g_93 += 1)
        { /* block id: 643 */
            float *l_1431 = &g_92;
            float **l_1430 = &l_1431;
            uint32_t ** const l_1433 = &g_309;
            int32_t l_1436 = 0L;
            uint32_t l_1438 = 0xA588AEBAL;
            float *l_1439[1];
            int32_t l_1441 = 0x65D1A2B0L;
            int32_t l_1442 = 0x9B8031E2L;
            int32_t l_1443 = 0xB30C9CC3L;
            int32_t l_1444 = 0xDE04C125L;
            int32_t l_1445 = 0xCA78E2A5L;
            int32_t l_1446 = 0x9A295B3FL;
            uint64_t l_1448 = 0xC871C9666746BCB4LL;
            int i;
            for (i = 0; i < 1; i++)
                l_1439[i] = &g_92;
            (*l_1420) = (safe_add_func_float_f_f((l_1437 = (((((((*g_192) = ((*g_873) = (-(**g_872)))) == ((((safe_add_func_float_f_f((safe_mul_func_float_f_f(((safe_div_func_float_f_f((((((p_40 == ((*l_1430) = p_40)) , ((**g_308) , l_1432)) == l_1433) <= (((g_100[2][0] , ((void*)0 == l_1434[0])) , (void*)0) != &g_138)) != 0xF.52134Cp-70), 0x1.Dp-1)) > l_1436), l_1436)), l_1402)) > (*l_1420)) <= p_41) < p_38)) != l_1436) == p_41) != (-0x1.Ep-1)) > l_1436)), l_1438));
            (*g_656) = &l_1418[8][5][0];
            for (g_535 = 0; (g_535 <= 3); g_535 += 1)
            { /* block id: 652 */
                int32_t *l_1440[3];
                int32_t l_1484 = 0x8A70D96DL;
                int i;
                for (i = 0; i < 3; i++)
                    l_1440[i] = &l_1418[9][0][0];
                l_1448++;
                for (l_1441 = 4; (l_1441 >= 2); l_1441 -= 1)
                { /* block id: 656 */
                    return l_1442;
                }
                for (l_1445 = 2; (l_1445 <= 8); l_1445 += 1)
                { /* block id: 661 */
                    uint16_t *l_1479 = &g_635;
                    uint16_t *l_1489 = &g_95;
                    int32_t l_1490 = 0x289400FFL;
                    int i, j;
                    if (g_539[g_535][l_1445])
                        break;
                }
                (*l_1420) ^= ((*g_282) &= l_1492);
            }
            for (g_144 = 1; (g_144 <= 6); g_144 += 1)
            { /* block id: 691 */
                (***g_655) = (*p_40);
            }
        }
        for (g_122 = 0; (g_122 != 35); g_122 = safe_add_func_uint32_t_u_u(g_122, 5))
        { /* block id: 697 */
            int32_t *l_1495 = &g_281;
            int32_t *l_1496 = &l_1447[4];
            int32_t *l_1497 = &l_1447[6];
            int32_t *l_1498 = &g_58;
            int32_t *l_1499[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t l_1500 = 18446744073709551615UL;
            uint16_t *l_1509 = &g_163;
            float ****l_1520 = &l_1458[1][4][0];
            int i;
            l_1500++;
            (*l_1498) = (safe_mul_func_uint8_t_u_u((((((safe_div_func_int32_t_s_s(((((((*l_1509) ^= (safe_sub_func_int32_t_s_s((*l_1420), 0x7FC3BDB5L))) && (safe_sub_func_uint16_t_u_u(((safe_mod_func_uint32_t_u_u((g_93 , (safe_mod_func_int64_t_s_s((safe_sub_func_uint64_t_u_u((*l_1420), ((***g_326) , ((((p_38 , ((((*g_244) = 0x0.6p-1) <= ((safe_rshift_func_uint8_t_u_u(p_41, ((3L ^ g_96) < (*l_1420)))) , (-0x1.3p+1))) < p_41)) < p_41) , (*g_538)) >= p_41)))), 0xE0E09EA32DC2AC26LL))), (**g_308))) >= (*l_1420)), g_122))) <= (*g_309)) , (*g_1311)) == l_1520), p_38)) <= 8L) || p_38) && p_41) , p_38), p_41));
        }
    }
    else
    { /* block id: 703 */
        int16_t l_1554[8];
        int32_t l_1573 = (-1L);
        int32_t l_1576[2];
        uint64_t l_1621[8][2] = {{0xA22E89B57B131B6DLL,18446744073709551615UL},{18446744073709551615UL,0xA22E89B57B131B6DLL},{18446744073709551615UL,18446744073709551615UL},{0xA22E89B57B131B6DLL,18446744073709551615UL},{18446744073709551615UL,0xA22E89B57B131B6DLL},{18446744073709551615UL,18446744073709551615UL},{0xA22E89B57B131B6DLL,18446744073709551615UL},{18446744073709551615UL,0xA22E89B57B131B6DLL}};
        int i, j;
        for (i = 0; i < 8; i++)
            l_1554[i] = 0xA8F2L;
        for (i = 0; i < 2; i++)
            l_1576[i] = (-10L);
        for (g_95 = 0; (g_95 <= 2); ++g_95)
        { /* block id: 706 */
            int8_t l_1527 = 0xF5L;
            uint64_t l_1569 = 0xA3E7E1FA0F0DBBBFLL;
            int32_t l_1577 = (-3L);
            int32_t l_1578 = 1L;
            int32_t l_1579 = 0L;
            int32_t l_1580 = 0xC442D249L;
            int32_t l_1581 = 1L;
            int64_t *l_1588 = &g_100[2][0];
            uint64_t *l_1593 = &g_122;
            uint64_t l_1599 = 0xF6B840DBF3D4D7E2LL;
            int32_t l_1636 = 0L;
            for (l_1402 = 0; (l_1402 <= 0); l_1402 += 1)
            { /* block id: 709 */
                const uint32_t *l_1529 = &g_548;
                const uint32_t **l_1528 = &l_1529;
                int16_t *l_1530 = &g_631;
                float ****l_1531 = &g_871;
                float ****l_1533 = &g_871;
                float *****l_1532 = &l_1533;
                uint16_t *l_1534 = (void*)0;
                uint16_t *l_1535 = &g_989;
                int32_t l_1552[8][5] = {{0x55B063ABL,0L,0xB177978DL,0L,0x55B063ABL},{1L,0x292B606AL,0xC7F42F7BL,3L,0x292B606AL},{0x55B063ABL,(-4L),(-4L),0x55B063ABL,0L},{(-1L),0L,0L,0x292B606AL,0x292B606AL},{1L,0x55B063ABL,1L,0L,0x55B063ABL},{0x292B606AL,0xC7F42F7BL,3L,0x292B606AL,3L},{0xA53BB2B3L,0xA53BB2B3L,0xB177978DL,0x55B063ABL,0L},{1L,(-1L),3L,3L,(-1L)}};
                uint16_t l_1582[3][4] = {{65528UL,65528UL,65528UL,65528UL},{65528UL,65528UL,65528UL,65528UL},{65528UL,65528UL,65528UL,65528UL}};
                uint64_t *l_1592 = (void*)0;
                uint64_t **l_1591[8] = {&l_1592,&l_1592,&l_1592,&l_1592,&l_1592,&l_1592,&l_1592,&l_1592};
                int64_t **l_1631 = (void*)0;
                int64_t ***l_1630[5][7] = {{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631},{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631},{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631},{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631},{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631}};
                uint32_t l_1635 = 0UL;
                int i, j;
                if ((((*g_282) = (safe_add_func_int16_t_s_s(g_1327[l_1402], p_41))) & (safe_sub_func_uint16_t_u_u((l_1527 | (l_1528 == (((*l_1535) = (((((((***g_325) == (((*l_1530) = 0xF03DL) , (void*)0)) , ((((l_1531 != ((*l_1532) = &g_871)) > (*g_139)) ^ g_58) || l_1527)) & 0xF71DL) || 0xA0L) | p_38) & p_41)) , (void*)0))), 0xF232L))))
                { /* block id: 714 */
                    int16_t l_1547 = 0L;
                    uint32_t *l_1548 = (void*)0;
                    int32_t l_1549 = (-3L);
                    uint32_t *l_1550[9] = {&g_1551,&g_1551,&g_1551,&g_1551,&g_1551,&g_1551,&g_1551,&g_1551,&g_1551};
                    int32_t l_1553 = 0xCD78EB02L;
                    const uint8_t ****l_1560 = &g_1557;
                    int i;
                    (***g_655) = ((**g_308) < (g_6 , ((((***g_326)++) && (safe_div_func_uint8_t_u_u(((***g_326) = g_1540), (safe_rshift_func_uint8_t_u_s(l_1527, (safe_sub_func_uint32_t_u_u(0x75E4FAE7L, ((safe_div_func_uint16_t_u_u(l_1547, p_38)) , (((((--g_1551) , ((*l_1560) = g_1557)) == (l_1552[0][0] , l_1561)) ^ 0x1BF9L) < 0x6C39DDC3L))))))))) != (*p_39))));
                }
                else
                { /* block id: 720 */
                    int32_t *l_1564 = &l_1552[0][0];
                    int32_t *l_1565 = (void*)0;
                    int32_t *l_1566 = (void*)0;
                    int32_t *l_1567 = &l_1447[8];
                    int32_t *l_1568[2][2];
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_1568[i][j] = &g_281;
                    }
                    l_1569++;
                    for (g_1540 = 0; (g_1540 >= 0); g_1540 -= 1)
                    { /* block id: 724 */
                        int64_t l_1572 = 0xFEA7D226281443B2LL;
                        int32_t l_1574 = 0x6F0BC9F8L;
                        int32_t l_1575[7][2] = {{0x30B7F398L,0x30B7F398L},{0x30B7F398L,0x25273691L},{3L,0L},{0x25273691L,0L},{3L,0x25273691L},{0x30B7F398L,0x30B7F398L},{0x30B7F398L,0x25273691L}};
                        int i, j;
                        ++l_1582[2][0];
                    }
                    if ((*p_39))
                        break;
                    if ((*p_39))
                        continue;
                }
                (**g_655) = &l_1576[1];
                (***g_655) = (((safe_sub_func_uint64_t_u_u(((*g_1461) , (~((void*)0 == l_1588))), ((((safe_mul_func_uint8_t_u_u((l_1573 = (*g_328)), ((l_1593 = &g_125) != (void*)0))) , ((l_1594 = (g_883 = l_1594)) != (void*)0)) == ((*g_1461) , p_41)) >= g_1540))) == g_631) > 0xA2D36B63497C5ACALL);
                l_1577 ^= (safe_lshift_func_uint8_t_u_s(l_1599, (((safe_mod_func_int64_t_s_s(((p_41 <= ((safe_sub_func_uint32_t_u_u((~((safe_div_func_uint32_t_u_u(((safe_sub_func_int32_t_s_s(0L, (((safe_add_func_uint16_t_u_u(((safe_sub_func_int32_t_s_s(0xD797916DL, l_1580)) & ((safe_mod_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s((safe_sub_func_int8_t_s_s((0xC7DCL && (safe_mul_func_uint8_t_u_u(((g_1327[l_1402] = l_1621[2][0]) , (((0xE1259451L || (safe_sub_func_int16_t_s_s((((((((*l_1588) = ((safe_div_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint16_t_u_u(((g_1632 = (void*)0) != l_1634), g_802[4])), l_1635)) > g_1058[0][3]), 254UL)) , p_38)) <= 0xC806CF11EB8DB1C3LL) && g_93) , l_1579) , (void*)0) == (void*)0), l_1621[2][0]))) || 8L) , l_1578)), l_1599))), g_343[0])), 7)), l_1576[1])) || p_41)), p_41)) >= g_343[0]) | 0xA031C444L))) & 0xE9DFL), l_1636)) < p_38)), (*g_965))) , (**g_138))) , l_1621[2][0]), g_349)) , g_1637) && 1UL)));
            }
        }
    }
    return p_38;
}


/* ------------------------------------------ */
/* 
 * reads : g_326 g_327 g_328 g_56 g_1150 g_703 g_656 g_282 g_281 g_18 g_655 g_871 g_872 g_631 g_139 g_140 g_343 g_980 g_125 g_308 g_309 g_89 g_989 g_938 g_2740
 * writes: g_56 g_281 g_631 g_989 g_861 g_2740
 */
static int32_t * func_42(uint8_t  p_43, uint8_t  p_44, int32_t * const  p_45, int32_t * p_46, int32_t * p_47)
{ /* block id: 524 */
    int32_t l_1128 = 0x43FDF7DCL;
    int64_t *l_1129 = (void*)0;
    int32_t l_1133 = (-1L);
    int32_t l_1138[8] = {0x3EDC3FD9L,0x3EDC3FD9L,0x3EDC3FD9L,0x3EDC3FD9L,0x3EDC3FD9L,0x3EDC3FD9L,0x3EDC3FD9L,0x3EDC3FD9L};
    uint8_t *l_1139[3];
    int8_t l_1140 = 5L;
    int32_t l_1141 = (-1L);
    int32_t l_1142 = 0L;
    int64_t l_1143 = 0x2139A9E31A9EBB2FLL;
    uint64_t l_1144 = 0x289B2CE3556EBB7DLL;
    int32_t ***l_1148 = &g_656;
    int32_t ****l_1149 = &l_1148;
    uint32_t l_1153 = 1UL;
    uint8_t **l_1157 = &l_1139[1];
    uint8_t ***l_1156 = &l_1157;
    uint8_t ****l_1155 = &l_1156;
    uint8_t *****l_1154 = &l_1155;
    const float l_1296 = 0x7.5p-1;
    int64_t l_1303 = 1L;
    int32_t *l_1313 = &g_861;
    int8_t *l_1393 = &g_343[0];
    int8_t **l_1392[1][2];
    int i, j;
    for (i = 0; i < 3; i++)
        l_1139[i] = (void*)0;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_1392[i][j] = &l_1393;
    }
lbl_1197:
    (***g_655) = ((safe_div_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u((((((p_44 < (l_1128 = (safe_lshift_func_uint16_t_u_s(l_1128, 13)))) ^ (0x261ECF03ECC8DDF3LL < ((+(safe_mod_func_uint32_t_u_u(((l_1133 && (l_1133 >= (safe_mod_func_uint32_t_u_u(((p_43 = ((**g_327) = (***g_326))) == ((safe_mul_func_uint16_t_u_u((((--l_1144) ^ ((~(((*l_1149) = l_1148) == &g_1042[1][0])) < (g_1150[2] == &g_1151))) < g_703), p_44)) || (***l_1148))), l_1153)))) && p_43), p_44))) ^ p_44))) ^ 0xF27A6CF7L) , p_44) >= l_1153), (*p_47))) || (***g_655)), p_44)), p_44)) == 0x1EED6E208E962BEALL);
    l_1154 = l_1154;
    for (l_1141 = 0; (l_1141 == (-5)); --l_1141)
    { /* block id: 534 */
        uint8_t l_1162 = 1UL;
        int32_t l_1239 = 2L;
        int32_t l_1250 = 0x50120AE6L;
        uint32_t l_1289[4][2] = {{0x799E0069L,0x799E0069L},{0x799E0069L,0x799E0069L},{0x799E0069L,0x799E0069L},{0x799E0069L,0x799E0069L}};
        const float *l_1293 = &g_811;
        const float **l_1292 = &l_1293;
        const float ***l_1291 = &l_1292;
        const float ****l_1290 = &l_1291;
        int32_t l_1339 = 0x63F74107L;
        int8_t *l_1390 = (void*)0;
        int8_t **l_1389[5][3][4] = {{{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390}},{{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390}},{{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390}},{{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390}},{{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390},{&l_1390,&l_1390,&l_1390,&l_1390}}};
        int8_t ***l_1391 = &l_1389[1][2][3];
        int i, j, k;
        if (((void*)0 == (*g_871)))
        { /* block id: 535 */
            (*g_282) &= (*p_45);
        }
        else
        { /* block id: 537 */
            int32_t ** const **l_1177 = (void*)0;
            int32_t l_1196 = (-7L);
            uint32_t **l_1237[1];
            int32_t l_1248 = 0x0CD74FC1L;
            int8_t l_1268[2][4] = {{0x29L,0L,0x29L,0x29L},{0L,0L,0x22L,0L}};
            float ****l_1295[7] = {&g_871,&g_871,&g_871,&g_871,&g_871,&g_871,&g_871};
            uint16_t *l_1348[8][6][5] = {{{(void*)0,(void*)0,&g_989,&g_95,(void*)0},{&g_635,&g_989,&g_95,&g_989,(void*)0},{&g_635,(void*)0,&g_635,&g_989,&g_635},{&g_95,&g_163,(void*)0,&g_989,&g_989},{&g_989,&g_989,&g_93,&g_93,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_989,&g_635,&g_635,(void*)0,(void*)0},{&g_989,&g_95,&g_95,&g_635,&g_635},{&g_989,(void*)0,&g_989,(void*)0,&g_989},{(void*)0,&g_989,&g_163,(void*)0,&g_95},{&g_635,&g_989,(void*)0,&g_93,&g_635},{&g_635,&g_95,&g_989,&g_989,&g_95}},{{(void*)0,&g_93,&g_93,&g_989,&g_989},{&g_95,(void*)0,&g_989,&g_989,&g_635},{&g_989,(void*)0,(void*)0,&g_95,(void*)0},{&g_95,&g_163,&g_163,&g_635,(void*)0},{(void*)0,&g_95,&g_989,(void*)0,(void*)0},{&g_635,&g_989,&g_93,&g_989,&g_989}},{{&g_635,&g_95,&g_635,(void*)0,&g_635},{(void*)0,&g_163,&g_635,&g_989,(void*)0},{&g_989,(void*)0,&g_93,&g_635,(void*)0},{&g_989,(void*)0,&g_635,(void*)0,&g_95},{&g_989,&g_93,&g_635,&g_989,(void*)0},{(void*)0,&g_95,&g_93,&g_635,&g_635}},{{&g_989,&g_989,&g_989,&g_989,&g_989},{&g_95,&g_989,&g_163,(void*)0,&g_163},{&g_635,(void*)0,(void*)0,&g_635,&g_635},{&g_635,&g_95,&g_989,&g_989,&g_163},{(void*)0,&g_635,&g_93,(void*)0,&g_989},{&g_163,(void*)0,&g_989,&g_989,&g_635}},{{&g_989,&g_989,(void*)0,(void*)0,(void*)0},{&g_163,&g_163,&g_163,&g_635,&g_95},{(void*)0,(void*)0,&g_989,&g_95,(void*)0},{&g_635,&g_989,&g_95,&g_989,(void*)0},{&g_635,(void*)0,&g_635,&g_989,&g_635},{&g_95,&g_163,(void*)0,&g_989,&g_989}},{{&g_989,&g_989,&g_93,&g_93,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_989,&g_635,&g_635,(void*)0,(void*)0},{&g_989,&g_95,&g_95,&g_635,&g_635},{&g_989,(void*)0,&g_989,(void*)0,&g_989},{(void*)0,&g_989,&g_163,(void*)0,&g_95}},{{&g_635,&g_989,(void*)0,&g_93,&g_635},{&g_635,&g_95,&g_989,&g_989,&g_95},{(void*)0,&g_93,&g_93,&g_989,&g_989},{&g_95,(void*)0,&g_989,&g_989,&g_635},{&g_989,(void*)0,(void*)0,&g_95,(void*)0},{&g_95,&g_163,&g_163,&g_635,(void*)0}}};
            uint16_t **l_1347 = &l_1348[6][3][0];
            int32_t *l_1382 = &l_1250;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1237[i] = &g_309;
            for (g_631 = (-21); (g_631 == (-15)); g_631++)
            { /* block id: 540 */
                int8_t *l_1193 = &l_1140;
                int8_t **l_1192[3][5][9] = {{{&l_1193,(void*)0,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193},{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193},{&l_1193,&l_1193,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,&l_1193,(void*)0},{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,(void*)0},{&l_1193,&l_1193,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,&l_1193,&l_1193}},{{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,(void*)0,&l_1193},{&l_1193,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193},{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193},{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193},{&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,&l_1193}},{{&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,(void*)0},{&l_1193,&l_1193,&l_1193,(void*)0,(void*)0,&l_1193,&l_1193,&l_1193,&l_1193},{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,(void*)0,&l_1193,&l_1193,&l_1193},{&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193},{&l_1193,(void*)0,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193,&l_1193}}};
                int32_t l_1194 = 0x282559C2L;
                float l_1195[5][8][6] = {{{0x8.Dp-1,0xE.81A7D6p+78,0x3.230148p+22,0x0.5p+1,0x0.7p-1,0xB.CC4CB9p+31},{0x0.Bp-1,0x6.7A527Ep-52,(-0x1.1p+1),0xC.8E5B90p+91,0x5.320DF2p-95,0x2.49743Dp+26},{0x4.9AFFBCp-37,0x1.0p-1,0x0.8974C1p+44,(-0x3.Ep-1),0xB.A987BDp-11,0x0.7p-1},{(-0x2.8p-1),0x3.2D254Ap+40,0x1.8p+1,0x1.0974B4p-31,0x0.14D11Cp+47,(-0x1.1p+1)},{(-0x7.8p+1),0x0.5p-1,0x4.F40438p+59,0x6.7A527Ep-52,0x3.E7623Cp-25,0xF.BEC3B3p+54},{0x2.5p-1,0x1.7p+1,0x0.Bp-1,0x0.5p+1,0x1.9p+1,(-0x1.7p+1)},{0xE.81A7D6p+78,0x3.524814p+78,0x1.8p-1,0xB.DCB1B6p+71,0x0.1p-1,0x3.2157E4p-63},{0x0.Ep+1,0xC.2C5730p-66,0x0.14D11Cp+47,0x8.4p+1,0x1.2p-1,0x1.2p-1}},{{0x3.524814p+78,(-0x1.Ep+1),(-0x1.Ep+1),0x3.524814p+78,(-0x1.7p+1),0x6.0p+1},{0xF.FAF928p+77,0x2.49743Dp+26,(-0x1.5p-1),(-0x7.8p+1),0x2.5p-1,0x5.320DF2p-95},{(-0x7.3p-1),0x0.Dp+1,(-0x3.Ep-1),0x3.2D254Ap+40,0x2.5p-1,0x8.0E574Cp+8},{0xC.5F3ECCp-8,0x2.49743Dp+26,(-0x1.6p-1),0x9.400711p-80,(-0x1.7p+1),0x0.5p-1},{(-0x3.7p+1),(-0x1.Ep+1),(-0x6.3p+1),0xB.CC4CB9p+31,0x1.2p-1,0x7.7DBA5Ap-72},{0x4.Fp-1,0xC.2C5730p-66,0x6.1AA99Cp-18,0x3.Dp-1,0x0.1p-1,0x2.5p-1},{0x3.230148p+22,0x3.524814p+78,0x5.Cp-1,0x4.0C4AC3p+4,0x1.9p+1,0x0.DF8114p-30},{0x4.F40438p+59,0x1.7p+1,(-0x1.1p-1),0x1.Fp-1,0x3.E7623Cp-25,0x0.5p+1}},{{0x6.7A527Ep-52,0x0.5p-1,0x5.320DF2p-95,0x7.3p-1,0x0.14D11Cp+47,0x1.8p+1},{0x1.9p+1,0x3.2D254Ap+40,0x2.5p-1,0xF.BEC3B3p+54,0xB.A987BDp-11,0x3.230148p+22},{(-0x1.5p-1),0x1.0p-1,0x0.3C56A0p+65,(-0x7.3p-1),0x5.320DF2p-95,0x4.9AFFBCp-37},{0x5.Cp-1,0x6.7A527Ep-52,0x3.2D254Ap+40,0x6.1B5321p-35,0x0.7p-1,0x1.7p+1},{0x2.49743Dp+26,0xE.81A7D6p+78,0x0.EFA7CCp-27,0x1.0p-1,(-0x6.3p+1),0x0.8FFEBAp+41},{0x1.Fp-1,0x2.8p-1,0x1.Dp+1,0x7.E51EC9p+7,0x1.8p+1,0x2.5p-1},{0xE.81A7D6p+78,0x0.Ep+1,0x3.230148p+22,0x2.4p+1,0x0.Dp+1,0x4.07CA89p-0},{0x7.3p-1,0x6.1AA99Cp-18,0x0.8p-1,0x0.EFA7CCp-27,0x3.230148p+22,0xF.BEC3B3p+54}},{{0x0.14D11Cp+47,(-0x1.6p-1),(-0x1.1p+1),0x0.DF8114p-30,0x4.959D3Dp+42,0x0.Bp-1},{0x1.5p-1,0xF.925F3Dp+65,0x2.49743Dp+26,0xB.CC4CB9p+31,0xF.FAF928p+77,0xB.CC4CB9p+31},{0x3.E7623Cp-25,0x0.8974C1p+44,0x3.E7623Cp-25,(-0x3.7p+1),0xC.2C5730p-66,0x1.8p-1},{0x3.92EEE8p+74,(-0x1.Bp-1),0x0.Bp-1,0xF.FAF928p+77,(-0x1.1p+1),0x3.2157E4p-63},{0x1.0p-1,(-0x1.4p+1),0x0.8FFEBAp+41,0xF.FAF928p+77,0x8.Dp-1,(-0x3.7p+1)},{0x3.92EEE8p+74,0x3.Dp-1,0xF.BEC3B3p+54,(-0x3.7p+1),0x7.7DBA5Ap-72,(-0x5.Fp-1)},{0x3.E7623Cp-25,0x5.1A15F0p-50,0x0.5p+1,0xB.CC4CB9p+31,0x0.1p-1,0x1.0974B4p-31},{0x1.5p-1,0xE.9808A7p+82,0x4.07CA89p-0,0x0.DF8114p-30,(-0x1.Ap-1),0xE.81A7D6p+78}},{{0x0.14D11Cp+47,0x6.0p+1,0xF.FAF928p+77,0x0.EFA7CCp-27,0x6.1B5321p-35,0x1.5p-1},{0x7.3p-1,0x3.2157E4p-63,0x1.9p+1,0x2.4p+1,0x7.E72231p-34,0x0.Fp+1},{0xE.81A7D6p+78,0xB.654DEBp+98,0x2.8p-1,0x7.E51EC9p+7,0x3.524814p+78,0x1.7p+1},{0x1.Fp-1,0x5.320DF2p-95,0xC.5F3ECCp-8,0xE.9808A7p+82,0x0.Cp-1,(-0x1.Ep+1)},{0x0.7p-1,0x4.959D3Dp+42,0x5.1A15F0p-50,0x3.2D254Ap+40,(-0x1.Bp-1),(-0x1.Ap-1)},{0x2.5p-1,0x0.Dp+1,0x6.7A527Ep-52,0xF.BEC3B3p+54,0x3.92EEE8p+74,0x7.E72231p-34},{0x0.Ep+1,0x4.Fp-1,0x3.92EEE8p+74,0x0.8p-1,0x3.2157E4p-63,0x3.Dp-1},{0x4.9AFFBCp-37,0x4.07CA89p-0,0x1.8p-1,(-0x1.6p-1),(-0x3.Ep-1),(-0x7.3p-1)}}};
                int i, j, k;
                l_1162++;
                (**g_656) &= (0x2975L || ((*g_139) != 0xB403B094D2E777B8LL));
                l_1196 &= (safe_lshift_func_uint8_t_u_s((safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u(((g_343[0] && (safe_add_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((void*)0 == l_1177), (safe_sub_func_uint64_t_u_u((safe_div_func_uint16_t_u_u((safe_mod_func_int32_t_s_s(((((safe_rshift_func_int8_t_s_s(p_44, (((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((((safe_sub_func_int16_t_s_s((0UL == ((g_980 == l_1192[1][1][0]) && l_1194)), (****l_1149))) >= 0xCFD6L) , l_1162), g_125)), l_1162)) <= (**g_308)) , 0xE1L))) , (***l_1148)) , l_1162) != l_1162), l_1194)), (***l_1148))), 0xA48D1A2DA2717AB3LL)))), (-1L))) | 9L), p_44))) & (****l_1149)), l_1194)), l_1162)), g_89));
                if (l_1162)
                    goto lbl_1197;
            }
            for (g_989 = 0; (g_989 <= 8); g_989 += 1)
            { /* block id: 548 */
                uint32_t l_1198 = 0x5B2AE68EL;
                uint8_t *l_1232[8] = {&l_1162,&l_1162,&l_1162,&l_1162,&l_1162,&l_1162,&l_1162,&l_1162};
                int32_t l_1246 = 0x4E31D905L;
                int32_t l_1249[8];
                uint32_t l_1251 = 0x55CA876AL;
                int64_t *l_1262 = &g_100[2][0];
                int64_t **l_1301 = &l_1129;
                int i;
                for (i = 0; i < 8; i++)
                    l_1249[i] = (-1L);
            }
            for (l_1250 = 28; (l_1250 == (-7)); l_1250 = safe_sub_func_uint32_t_u_u(l_1250, 3))
            { /* block id: 591 */
                uint64_t l_1340 = 0xD31217D174AA691DLL;
                int8_t *l_1341 = (void*)0;
                int8_t *l_1342 = &l_1268[0][0];
                const uint16_t *l_1346[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                const uint16_t **l_1345 = &l_1346[5];
                int32_t *l_1349 = &l_1141;
                uint32_t l_1368[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_1368[i] = 4294967295UL;
            }
        }
        (***l_1148) = (safe_mul_func_uint16_t_u_u((***l_1148), (safe_lshift_func_int8_t_s_s(((safe_sub_func_int32_t_s_s(((*l_1313) = ((&g_1041 == (((((*l_1391) = l_1389[1][2][3]) != ((*p_45) , l_1392[0][1])) < (*p_46)) , &l_1148)) && p_43)), (safe_mod_func_uint64_t_u_u((&p_47 == (void*)0), p_43)))) != p_43), g_938))));
        return (**l_1148);
    }
    return p_47;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_48(int16_t  p_49, uint32_t  p_50, uint8_t  p_51, uint32_t  p_52, int32_t * p_53)
{ /* block id: 17 */
    int32_t *l_64 = &g_18;
    int32_t *l_252 = &g_58;
    int32_t **l_251[9] = {&l_252,&l_252,&l_252,&l_252,&l_252,&l_252,&l_252,&l_252,&l_252};
    float *l_255 = (void*)0;
    float *l_256 = &g_257;
    uint32_t *l_307 = &g_89;
    uint32_t **l_306 = &l_307;
    uint16_t *l_339[8][6][5] = {{{(void*)0,&g_95,&g_163,&g_95,&g_93},{(void*)0,&g_93,&g_95,&g_95,&g_93},{(void*)0,(void*)0,&g_93,&g_163,&g_163},{(void*)0,&g_95,&g_93,&g_163,&g_93},{(void*)0,(void*)0,&g_93,&g_95,(void*)0},{&g_163,(void*)0,&g_93,(void*)0,&g_95}},{{(void*)0,&g_95,&g_95,(void*)0,&g_163},{&g_163,&g_163,&g_163,&g_95,&g_93},{&g_163,&g_95,&g_95,&g_163,&g_163},{&g_93,&g_93,(void*)0,&g_95,(void*)0},{&g_93,(void*)0,&g_163,&g_163,(void*)0},{(void*)0,&g_163,&g_95,&g_163,(void*)0}},{{(void*)0,&g_163,(void*)0,&g_93,&g_95},{&g_93,(void*)0,&g_93,&g_93,&g_95},{&g_163,(void*)0,&g_163,(void*)0,(void*)0},{&g_163,(void*)0,&g_93,&g_93,(void*)0},{(void*)0,(void*)0,&g_93,&g_163,(void*)0},{&g_93,&g_163,(void*)0,&g_163,(void*)0}},{{&g_95,&g_93,&g_163,&g_163,&g_163},{&g_93,&g_93,&g_93,&g_163,&g_93},{(void*)0,&g_163,&g_95,(void*)0,&g_93},{&g_163,&g_95,&g_163,&g_163,&g_163},{&g_163,&g_163,(void*)0,&g_163,&g_93},{&g_93,&g_163,&g_93,(void*)0,&g_93}},{{(void*)0,&g_95,&g_163,(void*)0,(void*)0},{(void*)0,&g_163,(void*)0,&g_93,&g_95},{&g_163,&g_93,&g_93,(void*)0,(void*)0},{&g_163,&g_93,&g_93,(void*)0,(void*)0},{&g_93,&g_163,&g_93,(void*)0,&g_163},{&g_163,(void*)0,(void*)0,&g_163,&g_95}},{{&g_163,(void*)0,&g_163,&g_163,&g_93},{&g_95,(void*)0,&g_93,&g_95,&g_163},{&g_163,(void*)0,(void*)0,&g_95,&g_163},{&g_163,&g_163,&g_163,&g_163,&g_163},{(void*)0,&g_163,&g_95,&g_163,(void*)0},{&g_163,(void*)0,&g_93,(void*)0,(void*)0}},{{&g_163,(void*)0,&g_163,(void*)0,&g_163},{&g_163,&g_93,(void*)0,(void*)0,(void*)0},{&g_93,(void*)0,&g_93,&g_93,(void*)0},{(void*)0,&g_163,&g_93,(void*)0,&g_163},{&g_93,&g_163,&g_163,(void*)0,&g_163},{&g_163,&g_163,&g_93,&g_163,&g_163}},{{&g_163,&g_163,(void*)0,&g_163,&g_93},{&g_93,(void*)0,&g_95,(void*)0,&g_95},{(void*)0,(void*)0,&g_163,&g_163,&g_163},{&g_93,&g_93,&g_93,&g_163,(void*)0},{&g_163,&g_93,&g_163,&g_163,(void*)0},{&g_163,&g_93,(void*)0,&g_163,&g_95}}};
    const int32_t *l_348 = &g_349;
    const int32_t **l_347 = &l_348;
    uint16_t l_381 = 0xCA48L;
    uint8_t ** const *l_423 = (void*)0;
    uint8_t ** const **l_422 = &l_423;
    uint8_t *l_534 = &g_535;
    uint8_t **l_533 = &l_534;
    uint8_t ***l_532 = &l_533;
    uint64_t *l_616 = &g_125;
    float l_642[6][1][9] = {{{0x0.001EDCp-12,0x9.6BB317p+80,(-0x1.4p+1),(-0x10.Ap-1),0x0.001EDCp-12,0x6.6FCE3Ep+13,0x4.9D494Fp+73,0x0.994434p+63,0x7.794581p+79}},{{(-0x2.3p-1),0x7.794581p+79,0x0.001EDCp-12,(-0x3.8p-1),(-0x9.8p+1),(-0x3.8p-1),0x0.001EDCp-12,0x7.794581p+79,(-0x2.3p-1)}},{{(-0x5.4p+1),0x7.794581p+79,0x9.6BB317p+80,0xC.4AD73Bp-19,(-0x1.5p-1),0xE.B8B534p-96,(-0x1.4p+1),0x9.6BB317p+80,0x0.001EDCp-12}},{{0x0.001EDCp-12,0x4.9D494Fp+73,0x1.4AEC8Dp+40,0x0.2p-1,0x6.607831p-51,0x0.001EDCp-12,(-0x9.8p+1),0xC.612C24p-2,0xC.612C24p-2}},{{(-0x5.4p+1),(-0x1.5p-1),(-0x1.4p+1),0xC.612C24p-2,(-0x1.4p+1),(-0x1.5p-1),(-0x5.4p+1),(-0x10.Ap-1),0xC.612C24p-2}},{{(-0x2.3p-1),0x2.DB1F40p+44,(-0x3.8p-1),0x7.794581p+79,0xC.4AD73Bp-19,0x4.9p-1,0x2.DB1F40p+44,0x0.994434p+63,0x0.001EDCp-12}}};
    int32_t ***l_658[1][2];
    float l_705 = 0x3.151570p-81;
    uint8_t l_706 = 5UL;
    uint16_t l_717 = 65532UL;
    int16_t l_818 = (-1L);
    int32_t l_828 = 0x8AEE5C0EL;
    uint8_t l_835 = 246UL;
    int16_t *l_1050 = (void*)0;
    uint8_t l_1071 = 249UL;
    uint64_t l_1082 = 2UL;
    int32_t l_1090 = 0xC945E98EL;
    int64_t *l_1110 = (void*)0;
    int64_t *l_1111 = &g_100[6][7];
    uint64_t l_1112 = 1UL;
    float l_1113 = 0xA.4C24C8p-23;
    int8_t *l_1116 = &g_343[0];
    uint8_t *l_1118 = &g_535;
    uint8_t **l_1117 = &l_1118;
    int32_t l_1119 = 1L;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_658[i][j] = &l_251[8];
    }
    return p_53;
}


/* ------------------------------------------ */
/* 
 * reads : g_56 g_57 g_58
 * writes: g_58
 */
static int16_t  func_54(uint32_t  p_55)
{ /* block id: 14 */
    (*g_57) &= g_56;
    return p_55;
}


/* ------------------------------------------ */
/* 
 * reads : g_100 g_163 g_57 g_18 g_58 g_162 g_6 g_122 g_96 g_281
 * writes: g_56 g_99 g_163 g_278 g_281
 */
static int32_t * func_67(int32_t * p_68, int16_t  p_69, int32_t * p_70, uint16_t  p_71, float  p_72)
{ /* block id: 120 */
    uint64_t l_262 = 18446744073709551615UL;
    int16_t *l_269 = &g_99[1][0][4];
    uint16_t *l_270 = (void*)0;
    uint16_t *l_271 = &g_163;
    int8_t l_276 = 1L;
    int16_t *l_277 = &g_278;
    int32_t *l_279 = (void*)0;
    int32_t *l_280 = &g_281;
    (*l_280) |= (safe_lshift_func_int16_t_s_s(5L, (((safe_add_func_int16_t_s_s((0xA7L <= (g_56 = l_262)), ((*l_277) = ((((p_71 = (safe_rshift_func_int8_t_s_s((((((safe_mul_func_int8_t_s_s((-9L), (safe_lshift_func_int8_t_s_s((((((*l_271) ^= (((*l_269) = p_71) | g_100[7][5])) <= (((*g_57) , (0x6254L != (((((safe_sub_func_uint64_t_u_u((safe_add_func_uint16_t_u_u((1UL < g_162), p_71)), (-10L))) >= g_6) >= 0UL) > g_100[2][0]) > 1UL))) , g_122)) , 0x54FCL) != 0UL), p_69)))) ^ g_96) < l_276) >= 0xB2800C3BL) > 0x62B8L), p_69))) , (void*)0) != (void*)0) > (-4L))))) & 65535UL) > (*p_70))));
    return p_70;
}


/* ------------------------------------------ */
/* 
 * reads : g_18 g_21 g_89 g_56 g_6 g_95 g_58 g_102 g_93 g_57 g_125 g_138 g_141 g_144 g_139 g_140 g_163 g_122 g_192 g_206 g_96 g_99 g_100 g_162 g_193 g_244
 * writes: g_89 g_93 g_95 g_58 g_102 g_57 g_122 g_125 g_141 g_56 g_144 g_163 g_99 g_92 g_193 g_206 g_245
 */
static int32_t * func_73(int32_t * p_74)
{ /* block id: 18 */
    int8_t l_77 = 0xC4L;
    int32_t *l_79 = &g_18;
    int32_t **l_78[2];
    int8_t l_126 = 0x0FL;
    uint64_t *l_135[8] = {&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125};
    uint8_t * const l_196 = (void*)0;
    uint16_t l_202[3][4] = {{6UL,0x6995L,0x6995L,6UL},{0x6995L,6UL,0x6995L,0x6995L},{6UL,6UL,0xEE2FL,6UL}};
    const uint8_t l_217 = 255UL;
    float l_250 = 0x8.1D7D2Fp-69;
    int i, j;
    for (i = 0; i < 2; i++)
        l_78[i] = &l_79;
    if ((l_77 || ((p_74 = &g_18) != (void*)0)))
    { /* block id: 20 */
        int16_t l_80[9];
        uint32_t *l_87 = (void*)0;
        uint32_t *l_88 = &g_89;
        int8_t *l_90 = (void*)0;
        int8_t *l_91[6] = {&l_77,&l_77,&l_77,&l_77,&l_77,&l_77};
        uint16_t *l_94 = &g_95;
        int32_t l_97[7];
        uint64_t l_159 = 18446744073709551615UL;
        int32_t l_205 = 0xD33C817CL;
        int i;
        for (i = 0; i < 9; i++)
            l_80[i] = 0xAA47L;
        for (i = 0; i < 7; i++)
            l_97[i] = 0x7464C660L;
        if ((g_58 &= (l_80[0] | (((safe_mul_func_uint8_t_u_u(((safe_unary_minus_func_int64_t_s(g_18)) , (+((((((*l_94) = ((safe_rshift_func_int8_t_s_s((g_93 = (((0x1FL & ((g_21 <= l_80[6]) >= l_80[0])) > g_18) >= ((*l_88) ^= 0xF88B3176L))), ((g_56 || g_56) == g_6))) && (*p_74))) , &g_18) == p_74) , g_95) & 0x37D7L))), 6UL)) != g_18) == (*l_79)))))
        { /* block id: 25 */
            int64_t l_98[10][7] = {{0xB7CB4D6242281945LL,0xB63E3B667D15778CLL,0x7316A37A54743E03LL,0xB2392579B7B7A4FDLL,0xC80F86AEFBDD559FLL,0x6691A67D55137C5FLL,0xB7CB4D6242281945LL},{0x53E45BD28040910BLL,0xB7CB4D6242281945LL,0x6691A67D55137C5FLL,0xC80F86AEFBDD559FLL,0xB2392579B7B7A4FDLL,0x7316A37A54743E03LL,0xB63E3B667D15778CLL},{0xB7CB4D6242281945LL,0xB2392579B7B7A4FDLL,(-5L),0x22B26CBFC796D631LL,0L,0x4EA093EA4B5445D8LL,0L},{0x22B26CBFC796D631LL,0xB63E3B667D15778CLL,0xB63E3B667D15778CLL,0x22B26CBFC796D631LL,0x7316A37A54743E03LL,0x53E45BD28040910BLL,0x5384E059069C78C3LL},{0xB236EDAE8E356D6BLL,0x4D3D82F88197C51CLL,0x7FD9B51ECC885CB6LL,0xC80F86AEFBDD559FLL,0x4D3D82F88197C51CLL,0x125B66FFBA242739LL,0x6691A67D55137C5FLL},{0L,(-9L),0x4EA093EA4B5445D8LL,0xB2392579B7B7A4FDLL,1L,0x4EA093EA4B5445D8LL,0x5384E059069C78C3LL},{(-10L),0x6691A67D55137C5FLL,0xB236EDAE8E356D6BLL,0L,0xC80F86AEFBDD559FLL,0xC80F86AEFBDD559FLL,0L},{0xB236EDAE8E356D6BLL,0x22B26CBFC796D631LL,0xB236EDAE8E356D6BLL,0x7316A37A54743E03LL,(-9L),(-7L),0xB63E3B667D15778CLL},{(-9L),(-10L),0x4EA093EA4B5445D8LL,0x4D3D82F88197C51CLL,0x5384E059069C78C3LL,(-5L),0xB7CB4D6242281945LL},{0xB2392579B7B7A4FDLL,0xB63E3B667D15778CLL,0x7FD9B51ECC885CB6LL,1L,0xC80F86AEFBDD559FLL,(-7L),0xB2392579B7B7A4FDLL}};
            int32_t l_101[1];
            int i, j;
            for (i = 0; i < 1; i++)
                l_101[i] = (-5L);
            --g_102;
            for (g_93 = 0; (g_93 <= 1); g_93 += 1)
            { /* block id: 29 */
                return &g_58;
            }
        }
        else
        { /* block id: 32 */
            int64_t l_106 = 0xCFDA208E2B08325BLL;
            int32_t l_107[5][2][9] = {{{0x02624D59L,0xF5A5DA86L,0x62F16C98L,0x609A124DL,0xCDAFBE71L,1L,0xF5A5DA86L,0xB6C92F55L,(-6L)},{0x3D8BF850L,(-6L),0xCDAFBE71L,0x62F16C98L,0x15673F55L,0x62F16C98L,0xCDAFBE71L,(-6L),0x3D8BF850L}},{{0x417F0C4AL,(-6L),0x450AD34FL,0x92873BCDL,1L,0x447AD0EDL,0x6D494579L,0x450AD34FL,0xCDAFBE71L},{0xCDAFBE71L,0xF5A5DA86L,1L,0x667010A8L,(-5L),0xCDAFBE71L,0x15673F55L,0xED7563F1L,0xED7563F1L}},{{0x417F0C4AL,1L,0x6D494579L,0xED7563F1L,0x6D494579L,1L,0x417F0C4AL,0x3D8BF850L,0x15673F55L},{0x667010A8L,4L,1L,0xB6C92F55L,1L,0x34E3A658L,4L,0x450AD34FL,0x02624D59L}},{{0x34E3A658L,1L,0xB6C92F55L,1L,4L,0x667010A8L,0x3D8BF850L,0x3D8BF850L,0x667010A8L},{0xCDAFBE71L,0x62F16C98L,0x15673F55L,0x62F16C98L,0xCDAFBE71L,(-6L),0x3D8BF850L,0x15673F55L,0xB6C92F55L}},{{0x02624D59L,0x447AD0EDL,0x5A6EF0A2L,1L,1L,0x02624D59L,4L,0xF5A5DA86L,(-5L)},{0x609A124DL,0xCDAFBE71L,1L,0xF5A5DA86L,0xB6C92F55L,(-6L),(-6L),0xB6C92F55L,0xF5A5DA86L}}};
            uint64_t *l_121 = &g_122;
            uint64_t *l_123 = (void*)0;
            uint64_t *l_124 = &g_125;
            float *l_249 = &g_92;
            int i, j, k;
lbl_246:
            for (g_89 = 0; (g_89 <= 1); g_89 += 1)
            { /* block id: 35 */
                int32_t l_105 = (-3L);
                int i;
                l_105 = (l_97[(g_89 + 4)] ^= 0x02B323ECL);
                if (l_97[(g_89 + 2)])
                    continue;
                g_57 = p_74;
                g_58 = ((l_107[0][0][1] &= l_106) & (*l_79));
            }
            for (g_89 = (-17); (g_89 < 57); g_89 = safe_add_func_int32_t_s_s(g_89, 7))
            { /* block id: 45 */
                int32_t *l_110[6][4][3] = {{{&l_107[0][0][1],(void*)0,&g_18},{&g_18,&g_18,(void*)0},{&l_107[1][0][0],&l_97[5],&l_107[0][0][1]},{&g_18,&l_97[0],&g_18}},{{&l_97[3],&l_97[0],&g_58},{&g_58,&l_97[5],(void*)0},{&l_97[0],&g_18,(void*)0},{&l_97[6],(void*)0,(void*)0}},{{&l_97[0],(void*)0,&g_58},{&g_58,&g_58,&g_58},{&l_97[3],(void*)0,&g_58},{&g_18,&l_97[6],&g_58}},{{&l_107[1][0][0],&g_18,(void*)0},{&g_18,&l_97[4],(void*)0},{&l_107[0][0][1],&g_18,(void*)0},{&g_18,&l_97[6],&g_58}},{{(void*)0,(void*)0,&g_18},{(void*)0,&g_58,&l_107[0][0][1]},{&g_18,(void*)0,(void*)0},{&l_107[0][0][1],(void*)0,&g_18}},{{&g_18,&g_18,(void*)0},{&l_107[1][0][0],&l_97[5],&l_107[0][0][1]},{&g_18,&l_97[0],&g_18},{&l_97[3],&l_97[0],&g_58}}};
                int i, j, k;
                return &g_58;
            }
            if ((safe_mul_func_int8_t_s_s((safe_sub_func_int8_t_s_s(((((safe_mul_func_int16_t_s_s(l_97[4], (&l_77 == (void*)0))) && (*p_74)) , l_80[1]) | ((*l_124) = (((((*l_121) = g_89) == ((void*)0 == &l_77)) != 0L) && 0UL))), l_80[0])), l_126)))
            { /* block id: 50 */
                uint64_t **l_136 = (void*)0;
                uint64_t **l_137 = &l_135[4];
                int32_t l_142 = 0x35208E98L;
                uint8_t *l_143 = &g_56;
                l_107[0][0][1] ^= ((g_144 &= ((*l_143) = (((safe_div_func_uint16_t_u_u((((safe_div_func_int8_t_s_s(0L, (g_141 &= (g_21 , (safe_div_func_int32_t_s_s((*g_57), (safe_rshift_func_uint16_t_u_s(g_95, ((((g_125 , ((*l_137) = l_135[5])) == &g_122) == 0x113F68E7L) ^ ((void*)0 != g_138)))))))))) | (*l_79)) < (*p_74)), l_142)) ^ l_80[3]) , l_97[4]))) | 0xADL);
            }
            else
            { /* block id: 56 */
                uint8_t *l_146[5] = {&g_56,&g_56,&g_56,&g_56,&g_56};
                uint8_t **l_145 = &l_146[4];
                int32_t l_172 = (-4L);
                const int32_t *l_187 = &l_97[4];
                const int32_t **l_186 = &l_187;
                int i;
                if ((0x17DE5E0314B3E7E0LL ^ (((((*l_145) = &g_56) == ((safe_div_func_uint8_t_u_u((((safe_mod_func_uint64_t_u_u(l_97[4], (safe_add_func_int32_t_s_s((safe_unary_minus_func_uint32_t_u(((safe_add_func_uint64_t_u_u((((1UL || 0x184A0F193884B002LL) != (((g_89 ^ g_95) && ((((safe_unary_minus_func_uint16_t_u(l_106)) , (void*)0) == l_90) || 9UL)) <= l_97[5])) >= g_125), 0UL)) > (**g_138)))), l_159)))) != 0L) >= 0x29E57E6BL), g_89)) , &g_56)) , 0x3962AE52L) >= l_97[4])))
                { /* block id: 58 */
                    int16_t *l_174 = &g_99[2][2][4];
                    uint16_t *l_183 = &g_163;
                    uint16_t *l_188[3];
                    int32_t l_189 = 0x6C1354E1L;
                    int32_t l_190 = 0x926A0632L;
                    float *l_191 = &g_92;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_188[i] = &g_93;
                    for (g_122 = (-22); (g_122 == 24); g_122 = safe_add_func_uint32_t_u_u(g_122, 7))
                    { /* block id: 61 */
                        ++g_163;
                    }
                    (*g_192) = ((*l_191) = (safe_add_func_float_f_f((-0x1.4p+1), ((((((((safe_add_func_uint8_t_u_u((0x7FL || (safe_rshift_func_int8_t_s_s(((((g_95 , (l_172 >= g_21)) <= ((~((*l_174) = l_172)) >= (l_189 = ((0x07L | (safe_sub_func_int8_t_s_s((safe_add_func_uint32_t_u_u((safe_mod_func_int64_t_s_s(((safe_add_func_int8_t_s_s((((g_6 < (--(*l_183))) , l_186) == (void*)0), 0xC2L)) || g_122), g_6)), 0L)), g_6))) ^ (*p_74))))) & l_190) , 9L), 6))), (*l_187))) || 0UL) , l_187) != &g_89) || (**l_186)) ^ g_93) , l_106) > l_190))));
                }
                else
                { /* block id: 69 */
                    uint32_t **l_194 = &l_88;
                    int32_t l_195 = 0L;
                    int32_t l_203[6] = {0xAE0601FEL,0x60537455L,0x60537455L,0xAE0601FEL,0x60537455L,0x60537455L};
                    float *l_204[4][2];
                    int i, j;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_204[i][j] = &g_92;
                    }
                    l_172 &= 0xEFA27BBEL;
                    l_203[2] |= (((((*l_194) = p_74) == p_74) && g_95) < ((0x3BC7L || ((l_195 | (l_196 == (void*)0)) , ((safe_add_func_int32_t_s_s(((+(safe_div_func_int64_t_s_s(((l_80[0] >= g_102) & l_195), 4UL))) >= (*l_187)), (*l_187))) || l_202[2][3]))) , 0x7BL));
                    g_58 |= (*p_74);
                    (*g_192) = (-0x1.Cp+1);
                }
                g_206--;
                for (g_144 = 0; (g_144 <= 5); g_144 += 1)
                { /* block id: 79 */
                    int8_t l_209 = 1L;
                    for (l_172 = 1; (l_172 <= 5); l_172 += 1)
                    { /* block id: 82 */
                        uint32_t l_210 = 0UL;
                        l_97[4] = 0xE34FB19BL;
                        l_210--;
                        return &g_58;
                    }
                    if (l_106)
                        continue;
                    for (l_205 = 1; (l_205 >= 0); l_205 -= 1)
                    { /* block id: 90 */
                        uint32_t l_213 = 0xEF565022L;
                        int32_t l_218 = 0x44C11F17L;
                        int32_t *l_219[2][3] = {{(void*)0,&l_97[4],&l_97[4]},{(void*)0,&l_97[4],&l_97[4]}};
                        int i, j;
                        ++l_213;
                        l_218 |= (~l_217);
                        l_219[0][2] = &g_58;
                    }
                    for (l_159 = 0; (l_159 <= 1); l_159 += 1)
                    { /* block id: 97 */
                        int8_t l_241 = 0L;
                        float *l_242 = (void*)0;
                        float *l_243 = &g_92;
                        (*g_244) = (l_80[0] , ((safe_add_func_float_f_f(((!0x3.5DB4DFp+95) != (((*l_243) = ((*g_192) = (((safe_mul_func_uint8_t_u_u(((((~(safe_rshift_func_uint8_t_u_s((((!(safe_sub_func_int8_t_s_s(((((safe_mul_func_int8_t_s_s(((0xC627A455B6F9C3CALL & (safe_add_func_uint64_t_u_u(((*l_79) >= (safe_rshift_func_uint8_t_u_u(l_107[0][0][1], (((**l_186) , 0x66DC162CL) , (safe_mod_func_int8_t_s_s(((safe_rshift_func_int8_t_s_s(l_107[4][1][2], g_96)) > 0x512604FD5BABCDBCLL), g_96)))))), 18446744073709551607UL))) , 0x70L), g_99[1][0][2])) && 0xBE56L) | (**l_186)) == l_241), g_100[3][0]))) && (*g_57)) , l_107[4][0][4]), 4))) , g_162) | l_209) || 0L), 0x2CL)) < g_163) , (*g_192)))) < g_100[2][0])), 0x1.BBB745p+96)) == l_107[0][0][5]));
                        if (g_89)
                            goto lbl_246;
                    }
                }
                for (g_141 = 0; (g_141 == (-17)); g_141 = safe_sub_func_uint8_t_u_u(g_141, 9))
                { /* block id: 106 */
                    (*l_186) = &g_58;
                }
            }
            (*l_249) = 0x2.512A26p+79;
        }
    }
    else
    { /* block id: 112 */
        (*g_57) |= 0xA9A1731CL;
        g_57 = p_74;
    }
    g_58 = ((void*)0 == &l_217);
    return &g_58;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_56, "g_56", print_hash_value);
    transparent_crc(g_58, "g_58", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc_bytes (&g_92, sizeof(g_92), "g_92", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_99[i][j][k], "g_99[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_100[i][j], "g_100[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_102, "g_102", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_140[i][j], "g_140[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_141, "g_141", print_hash_value);
    transparent_crc(g_144, "g_144", print_hash_value);
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc(g_163, "g_163", print_hash_value);
    transparent_crc_bytes (&g_193, sizeof(g_193), "g_193", print_hash_value);
    transparent_crc(g_206, "g_206", print_hash_value);
    transparent_crc_bytes (&g_245, sizeof(g_245), "g_245", print_hash_value);
    transparent_crc_bytes (&g_257, sizeof(g_257), "g_257", print_hash_value);
    transparent_crc(g_278, "g_278", print_hash_value);
    transparent_crc(g_281, "g_281", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_343[i], "g_343[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_349, "g_349", print_hash_value);
    transparent_crc(g_535, "g_535", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_539[i][j], "g_539[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc(g_631, "g_631", print_hash_value);
    transparent_crc(g_635, "g_635", print_hash_value);
    transparent_crc(g_703, "g_703", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_802[i], "g_802[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_811, sizeof(g_811), "g_811", print_hash_value);
    transparent_crc(g_861, "g_861", print_hash_value);
    transparent_crc(g_938, "g_938", print_hash_value);
    transparent_crc(g_989, "g_989", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1009[i], "g_1009[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1058[i][j], "g_1058[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1152, "g_1152", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1327[i], "g_1327[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1361, sizeof(g_1361), "g_1361", print_hash_value);
    transparent_crc(g_1462, "g_1462", print_hash_value);
    transparent_crc(g_1540, "g_1540", print_hash_value);
    transparent_crc(g_1551, "g_1551", print_hash_value);
    transparent_crc(g_1637, "g_1637", print_hash_value);
    transparent_crc(g_1672, "g_1672", print_hash_value);
    transparent_crc(g_1674, "g_1674", print_hash_value);
    transparent_crc(g_1686, "g_1686", print_hash_value);
    transparent_crc(g_1700, "g_1700", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1724[i], "g_1724[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1733, "g_1733", print_hash_value);
    transparent_crc(g_1887, "g_1887", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2249[i], "g_2249[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2273, "g_2273", print_hash_value);
    transparent_crc(g_2364, "g_2364", print_hash_value);
    transparent_crc(g_2467, "g_2467", print_hash_value);
    transparent_crc(g_2608, "g_2608", print_hash_value);
    transparent_crc(g_2642, "g_2642", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2740[i], "g_2740[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2880, "g_2880", print_hash_value);
    transparent_crc(g_3013, "g_3013", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_3019[i], "g_3019[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3054, "g_3054", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_3067[i][j], "g_3067[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3093, "g_3093", print_hash_value);
    transparent_crc(g_3103, "g_3103", print_hash_value);
    transparent_crc(g_3111, "g_3111", print_hash_value);
    transparent_crc(g_3160, "g_3160", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 958
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 56
breakdown:
   depth: 1, occurrence: 285
   depth: 2, occurrence: 84
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 6, occurrence: 2
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 10, occurrence: 2
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 14, occurrence: 3
   depth: 15, occurrence: 1
   depth: 16, occurrence: 4
   depth: 17, occurrence: 7
   depth: 18, occurrence: 1
   depth: 19, occurrence: 3
   depth: 20, occurrence: 4
   depth: 21, occurrence: 4
   depth: 22, occurrence: 3
   depth: 23, occurrence: 4
   depth: 24, occurrence: 4
   depth: 25, occurrence: 1
   depth: 26, occurrence: 3
   depth: 27, occurrence: 4
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 31, occurrence: 2
   depth: 32, occurrence: 3
   depth: 35, occurrence: 3
   depth: 36, occurrence: 1
   depth: 37, occurrence: 2
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 43, occurrence: 1
   depth: 56, occurrence: 1

XXX total number of pointers: 631

XXX times a variable address is taken: 1966
XXX times a pointer is dereferenced on RHS: 588
breakdown:
   depth: 1, occurrence: 425
   depth: 2, occurrence: 120
   depth: 3, occurrence: 36
   depth: 4, occurrence: 7
XXX times a pointer is dereferenced on LHS: 418
breakdown:
   depth: 1, occurrence: 286
   depth: 2, occurrence: 91
   depth: 3, occurrence: 35
   depth: 4, occurrence: 6
XXX times a pointer is compared with null: 69
XXX times a pointer is compared with address of another variable: 25
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 15078

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 4836
   level: 2, occurrence: 1087
   level: 3, occurrence: 786
   level: 4, occurrence: 223
   level: 5, occurrence: 33
XXX number of pointers point to pointers: 312
XXX number of pointers point to scalars: 319
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.4
XXX average alias set size: 1.61

XXX times a non-volatile is read: 2856
XXX times a non-volatile is write: 1423
XXX times a volatile is read: 203
XXX    times read thru a pointer: 100
XXX times a volatile is write: 52
XXX    times written thru a pointer: 22
XXX times a volatile is available for access: 3.81e+03
XXX percentage of non-volatile access: 94.4

XXX forward jumps: 1
XXX backward jumps: 9

XXX stmts: 298
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 24
   depth: 2, occurrence: 41
   depth: 3, occurrence: 62
   depth: 4, occurrence: 68
   depth: 5, occurrence: 75

XXX percentage a fresh-made variable is used: 14.9
XXX percentage an existing variable is used: 85.1
********************* end of statistics **********************/


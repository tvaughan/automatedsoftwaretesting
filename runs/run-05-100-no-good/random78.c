/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3872784073
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   volatile int8_t  f0;
   const volatile uint8_t  f1;
   uint32_t  f2;
   volatile int16_t  f3;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int16_t g_8[1] = {0x46BFL};
static int32_t g_10 = 0x07F0936AL;
static volatile int32_t g_13 = 0L;/* VOLATILE GLOBAL g_13 */
static int32_t *g_18 = &g_10;
static int32_t ** volatile g_17 = &g_18;/* VOLATILE GLOBAL g_17 */
static int32_t * volatile g_46 = (void*)0;/* VOLATILE GLOBAL g_46 */
static int32_t g_48[1] = {(-10L)};
static int32_t * const  volatile g_47 = &g_48[0];/* VOLATILE GLOBAL g_47 */
static int16_t *g_159 = &g_8[0];
static int16_t **g_158 = &g_159;
static int8_t g_165 = 0x56L;
static struct S0 g_197 = {-8L,0x62L,0xBC1734ECL,-1L};/* VOLATILE GLOBAL g_197 */
static int64_t g_207 = 3L;
static uint32_t g_210 = 0xAC821ADEL;
static uint64_t g_214 = 0xA404222332E96215LL;
static uint64_t *g_213 = &g_214;
static uint8_t g_245 = 0UL;
static float g_262 = 0x0.7B9890p-45;
static float * volatile g_261 = &g_262;/* VOLATILE GLOBAL g_261 */
static const int16_t g_304 = 0xD128L;
static uint64_t g_308[9] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static float * volatile g_309 = &g_262;/* VOLATILE GLOBAL g_309 */
static volatile uint8_t g_340 = 0xAAL;/* VOLATILE GLOBAL g_340 */
static uint32_t g_348 = 4UL;
static uint16_t ** volatile g_358 = (void*)0;/* VOLATILE GLOBAL g_358 */
static uint16_t ** volatile *g_357 = &g_358;
static uint16_t **g_360 = (void*)0;
static uint16_t ***g_359 = &g_360;
static struct S0 g_365 = {0x4EL,0xCEL,0x9D198411L,0x67B1L};/* VOLATILE GLOBAL g_365 */
static float * volatile g_432 = (void*)0;/* VOLATILE GLOBAL g_432 */
static float * volatile g_433 = &g_262;/* VOLATILE GLOBAL g_433 */
static uint64_t g_491 = 1UL;
static int32_t g_584 = 9L;
static int32_t *g_583 = &g_584;
static struct S0 g_588 = {1L,5UL,18446744073709551615UL,-1L};/* VOLATILE GLOBAL g_588 */
static struct S0 g_589 = {0L,0xEDL,18446744073709551607UL,-1L};/* VOLATILE GLOBAL g_589 */
static uint8_t g_592 = 255UL;
static int32_t *g_607 = &g_584;
static uint16_t g_702 = 65535UL;
static volatile struct S0 *g_736 = (void*)0;
static volatile struct S0 **g_735 = &g_736;
static int32_t g_753 = 0x490BF3BBL;
static int64_t * const *g_767 = (void*)0;
static int32_t **g_786 = &g_583;
static int32_t *** volatile g_785 = &g_786;/* VOLATILE GLOBAL g_785 */
static float g_791 = 0x4.0p-1;
static struct S0 g_800 = {0xDBL,0UL,0x2636FBEBL,-1L};/* VOLATILE GLOBAL g_800 */
static struct S0 g_815 = {0xF0L,251UL,18446744073709551611UL,0x49C4L};/* VOLATILE GLOBAL g_815 */
static float g_882 = 0x5.0p+1;
static volatile uint32_t g_890 = 0xF67F62E5L;/* VOLATILE GLOBAL g_890 */
static volatile int32_t g_930 = 0x943EF9E0L;/* VOLATILE GLOBAL g_930 */
static uint8_t *g_938 = &g_592;
static uint8_t ** volatile g_937[10] = {&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938};
static struct S0 g_939 = {-1L,1UL,0xF093FAEEL,1L};/* VOLATILE GLOBAL g_939 */
static float g_967 = (-0x10.Bp+1);
static int32_t g_968[10] = {8L,8L,8L,8L,8L,8L,8L,8L,8L,8L};
static uint32_t g_972 = 4UL;
static uint32_t g_981[7][8] = {{4294967293UL,0UL,0x674F807AL,4294967295UL,3UL,0x674F807AL,0x1224323AL,6UL},{1UL,3UL,0x5B8A5D98L,4294967293UL,0x18950FD6L,4294967293UL,0x5B8A5D98L,0xC38EB591L},{4294967293UL,0x674F807AL,0x36649BB5L,0x1526B6AAL,1UL,0x00A55564L,0x1526B6AAL,0UL},{1UL,1UL,0x18950FD6L,4294967292UL,4294967293UL,4294967295UL,0x1526B6AAL,0x5B8A5D98L},{0UL,4294967292UL,0x36649BB5L,0x00A55564L,9UL,0UL,0UL,9UL},{9UL,0UL,0UL,9UL,0x00A55564L,0x36649BB5L,4294967292UL,0UL},{0x5B8A5D98L,0x1526B6AAL,4294967295UL,4294967293UL,4294967292UL,0x18950FD6L,1UL,1UL}};
static uint16_t g_982 = 4UL;
static int32_t g_1002 = (-1L);
static int64_t g_1004 = 0xD43982265D549139LL;
static float * volatile g_1006[2] = {&g_967,&g_967};
static uint8_t g_1022 = 0x97L;
static volatile struct S0 g_1048 = {1L,4UL,1UL,0x8ED0L};/* VOLATILE GLOBAL g_1048 */
static int8_t *g_1082 = &g_165;
static int8_t **g_1081 = &g_1082;
static volatile struct S0 g_1094 = {9L,0xD1L,18446744073709551615UL,0xB183L};/* VOLATILE GLOBAL g_1094 */
static int32_t g_1104 = 0x5E6547E3L;
static int32_t * const g_1103 = &g_1104;
static int32_t * const *g_1102[8][10] = {{&g_1103,&g_1103,(void*)0,(void*)0,&g_1103,&g_1103,(void*)0,&g_1103,&g_1103,&g_1103},{&g_1103,(void*)0,&g_1103,&g_1103,&g_1103,(void*)0,&g_1103,(void*)0,&g_1103,&g_1103},{&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,(void*)0},{&g_1103,&g_1103,&g_1103,&g_1103,(void*)0,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103},{&g_1103,(void*)0,&g_1103,(void*)0,&g_1103,&g_1103,(void*)0,&g_1103,(void*)0,&g_1103},{(void*)0,&g_1103,&g_1103,(void*)0,&g_1103,&g_1103,&g_1103,(void*)0,&g_1103,&g_1103},{&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103},{&g_1103,&g_1103,(void*)0,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103,&g_1103}};
static int32_t * const *g_1106 = (void*)0;
static int32_t * const ** volatile g_1105[8][2] = {{(void*)0,(void*)0},{&g_1106,(void*)0},{(void*)0,&g_1106},{(void*)0,(void*)0},{&g_1106,(void*)0},{(void*)0,&g_1106},{(void*)0,(void*)0},{&g_1106,(void*)0}};
static volatile uint64_t *g_1143 = (void*)0;
static volatile uint64_t ** volatile g_1142[3] = {&g_1143,&g_1143,&g_1143};
static volatile uint64_t ** volatile * volatile g_1141 = &g_1142[1];/* VOLATILE GLOBAL g_1141 */
static uint64_t **g_1145[4][8][5] = {{{(void*)0,&g_213,&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,(void*)0},{&g_213,&g_213,(void*)0,&g_213,&g_213},{&g_213,(void*)0,(void*)0,&g_213,&g_213},{(void*)0,&g_213,&g_213,&g_213,(void*)0},{&g_213,&g_213,&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,(void*)0},{&g_213,&g_213,&g_213,&g_213,&g_213}},{{(void*)0,&g_213,(void*)0,&g_213,&g_213},{(void*)0,&g_213,(void*)0,(void*)0,(void*)0},{&g_213,(void*)0,&g_213,&g_213,&g_213},{(void*)0,&g_213,(void*)0,&g_213,&g_213},{(void*)0,&g_213,&g_213,(void*)0,&g_213},{&g_213,&g_213,(void*)0,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,&g_213}},{{&g_213,&g_213,&g_213,&g_213,&g_213},{&g_213,(void*)0,&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213,(void*)0,(void*)0},{&g_213,(void*)0,&g_213,&g_213,&g_213}},{{(void*)0,&g_213,(void*)0,&g_213,&g_213},{(void*)0,&g_213,&g_213,(void*)0,&g_213},{&g_213,&g_213,(void*)0,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213,&g_213,&g_213},{&g_213,(void*)0,&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213,&g_213,&g_213}}};
static uint64_t ***g_1144[9][10][2] = {{{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]}},{{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]}},{{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]}},{{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]}},{{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]}},{{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]}},{{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]}},{{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]}},{{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[0][3][1],(void*)0},{&g_1145[1][7][2],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[2][1][1],&g_1145[1][7][2]},{&g_1145[1][7][2],(void*)0},{&g_1145[0][3][1],&g_1145[1][7][2]}}};
static int64_t *g_1152 = &g_207;
static int64_t **g_1151 = &g_1152;
static const volatile int8_t g_1213 = 1L;/* VOLATILE GLOBAL g_1213 */
static uint32_t g_1217 = 0xE2595937L;
static struct S0 *g_1234 = &g_939;
static struct S0 **g_1233 = &g_1234;
static uint32_t g_1244[8] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
static int64_t ***g_1328 = &g_1151;
static int64_t **** volatile g_1327 = &g_1328;/* VOLATILE GLOBAL g_1327 */
static int8_t g_1337 = 0L;
static uint16_t g_1374 = 65527UL;
static struct S0 g_1379 = {0x43L,0xCDL,0x648A48B1L,0x61F8L};/* VOLATILE GLOBAL g_1379 */
static int16_t * const *g_1387 = &g_159;
static int16_t * const ** volatile g_1386 = &g_1387;/* VOLATILE GLOBAL g_1386 */
static int32_t *g_1394 = &g_1104;
static int32_t **g_1393 = &g_1394;
static int32_t *** volatile g_1392 = &g_1393;/* VOLATILE GLOBAL g_1392 */
static const volatile uint8_t g_1420 = 1UL;/* VOLATILE GLOBAL g_1420 */
static float * volatile g_1421[10][4][2] = {{{&g_262,&g_791},{&g_967,&g_967},{&g_262,&g_791},{(void*)0,&g_262}},{{&g_262,&g_967},{&g_791,&g_262},{(void*)0,&g_967},{(void*)0,&g_262}},{{&g_791,&g_967},{&g_262,&g_262},{(void*)0,&g_791},{&g_262,&g_967}},{{&g_967,&g_791},{&g_262,&g_791},{&g_967,&g_967},{&g_262,&g_791}},{{(void*)0,&g_262},{&g_262,&g_967},{&g_791,&g_262},{(void*)0,&g_967}},{{(void*)0,&g_262},{&g_791,&g_967},{&g_262,&g_262},{(void*)0,&g_791}},{{&g_262,&g_967},{&g_967,&g_791},{&g_262,&g_791},{&g_967,&g_967}},{{&g_262,&g_791},{(void*)0,&g_262},{&g_262,&g_967},{&g_791,&g_262}},{{(void*)0,&g_967},{(void*)0,&g_262},{&g_791,&g_967},{&g_262,&g_262}},{{(void*)0,&g_791},{&g_262,&g_967},{&g_967,&g_791},{&g_262,&g_791}}};
static volatile int32_t g_1487 = 1L;/* VOLATILE GLOBAL g_1487 */
static float * volatile g_1520 = &g_262;/* VOLATILE GLOBAL g_1520 */
static struct S0 g_1537 = {0x0EL,0x7EL,0x9ED61CBDL,0L};/* VOLATILE GLOBAL g_1537 */
static const volatile struct S0 * volatile * volatile **g_1591 = (void*)0;
static const volatile struct S0 g_1612 = {0xABL,0xFEL,0x2DF81B9BL,5L};/* VOLATILE GLOBAL g_1612 */
static const int64_t g_1698 = 0x12854C08CC6FE313LL;
static const int64_t *g_1697 = &g_1698;
static float * const  volatile g_1744[10] = {&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791,&g_791};
static float * volatile g_1753 = &g_262;/* VOLATILE GLOBAL g_1753 */
static int8_t *g_1761 = &g_165;
static float g_1763 = 0xE.61EC85p+1;
static uint16_t * const g_1796 = &g_1374;
static uint16_t * const *g_1795 = &g_1796;
static int16_t g_1822 = 0x1136L;
static volatile int32_t g_1849 = 0x30287675L;/* VOLATILE GLOBAL g_1849 */
static volatile uint32_t g_1853 = 0xEA89E8EAL;/* VOLATILE GLOBAL g_1853 */
static uint32_t *g_1886 = (void*)0;
static uint32_t ** volatile g_1885 = &g_1886;/* VOLATILE GLOBAL g_1885 */
static uint32_t ** volatile * volatile g_1884 = &g_1885;/* VOLATILE GLOBAL g_1884 */
static const int32_t *g_1915[8][6][5] = {{{&g_1002,&g_1104,&g_1002,(void*)0,&g_1104},{&g_1002,&g_1002,&g_1002,(void*)0,&g_1104},{&g_1002,&g_1104,&g_1104,&g_1002,&g_1002},{&g_1002,&g_1002,(void*)0,&g_1104,&g_1002},{&g_1002,&g_1002,&g_1002,&g_1002,&g_1002},{&g_1002,&g_1104,&g_1002,&g_1002,(void*)0}},{{&g_1104,(void*)0,&g_1104,&g_1104,&g_1002},{(void*)0,&g_1104,&g_1104,(void*)0,&g_1104},{&g_1104,(void*)0,(void*)0,&g_1002,&g_1002},{&g_1104,&g_1002,(void*)0,&g_1002,(void*)0},{(void*)0,(void*)0,&g_1104,&g_1002,&g_1104},{(void*)0,&g_1002,&g_1002,(void*)0,&g_1002}},{{&g_1002,&g_1104,(void*)0,&g_1104,&g_1104},{&g_1002,&g_1002,&g_1104,&g_1002,&g_1002},{&g_1002,(void*)0,&g_1104,&g_1002,(void*)0},{&g_1002,&g_1002,(void*)0,&g_1104,(void*)0},{&g_1002,(void*)0,(void*)0,&g_1104,&g_1002},{&g_1104,&g_1104,&g_1104,&g_1104,(void*)0}},{{&g_1002,(void*)0,&g_1104,&g_1104,&g_1002},{&g_1002,&g_1104,&g_1104,&g_1002,&g_1002},{(void*)0,&g_1002,&g_1002,&g_1104,&g_1002},{&g_1002,&g_1002,&g_1104,&g_1002,&g_1104},{&g_1002,&g_1104,&g_1002,&g_1104,(void*)0},{(void*)0,&g_1002,(void*)0,&g_1104,(void*)0}},{{&g_1104,&g_1104,&g_1002,&g_1002,&g_1104},{&g_1002,&g_1002,&g_1104,&g_1002,&g_1002},{&g_1104,&g_1104,&g_1002,(void*)0,&g_1002},{&g_1104,&g_1104,&g_1002,&g_1002,&g_1002},{&g_1104,&g_1104,&g_1002,&g_1002,(void*)0},{&g_1104,&g_1104,&g_1104,&g_1002,&g_1002}},{{&g_1104,(void*)0,&g_1002,&g_1002,(void*)0},{&g_1002,&g_1104,&g_1104,(void*)0,(void*)0},{(void*)0,&g_1104,(void*)0,&g_1002,&g_1002},{&g_1002,&g_1104,&g_1104,&g_1002,&g_1104},{&g_1002,&g_1002,(void*)0,&g_1002,&g_1002},{&g_1002,&g_1104,&g_1104,&g_1104,&g_1104}},{{&g_1002,&g_1002,(void*)0,(void*)0,&g_1002},{&g_1104,&g_1104,&g_1104,&g_1104,(void*)0},{&g_1002,&g_1002,&g_1104,&g_1104,(void*)0},{(void*)0,&g_1002,&g_1104,(void*)0,(void*)0},{&g_1002,(void*)0,&g_1002,&g_1002,&g_1104},{&g_1104,&g_1002,(void*)0,&g_1002,&g_1104}},{{&g_1104,&g_1002,&g_1104,(void*)0,&g_1104},{(void*)0,&g_1104,&g_1002,&g_1104,(void*)0},{&g_1104,&g_1002,&g_1002,&g_1104,(void*)0},{&g_1104,(void*)0,&g_1104,&g_1002,&g_1104},{&g_1104,&g_1104,&g_1002,&g_1002,&g_1002},{&g_1104,&g_1104,(void*)0,&g_1104,&g_1104}}};
static const int32_t **g_1914 = &g_1915[2][5][4];
static const int32_t ***g_1913 = &g_1914;
static const int32_t ****g_1912 = &g_1913;
static int8_t g_1920 = 0x18L;
static struct S0 g_1933 = {-5L,0x5AL,0x5E912F9FL,0x3E0BL};/* VOLATILE GLOBAL g_1933 */
static volatile struct S0 g_2013 = {0x93L,0xA7L,0x13A3CE9CL,-1L};/* VOLATILE GLOBAL g_2013 */
static volatile int32_t g_2061 = (-4L);/* VOLATILE GLOBAL g_2061 */
static volatile struct S0 g_2144 = {0x2AL,0x63L,0xE75BD324L,-4L};/* VOLATILE GLOBAL g_2144 */
static struct S0 g_2162 = {0xC5L,248UL,4UL,-9L};/* VOLATILE GLOBAL g_2162 */
static float *g_2227 = &g_791;
static float * volatile *g_2265 = &g_2227;
static uint64_t g_2268[2][4] = {{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL}};
static const uint32_t * const * const *g_2289 = (void*)0;
static uint32_t **g_2297[8][2][7] = {{{(void*)0,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886},{&g_1886,(void*)0,&g_1886,&g_1886,&g_1886,(void*)0,&g_1886}},{{(void*)0,&g_1886,&g_1886,&g_1886,&g_1886,(void*)0,&g_1886},{&g_1886,(void*)0,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886}},{{&g_1886,&g_1886,&g_1886,&g_1886,(void*)0,&g_1886,&g_1886},{&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886}},{{&g_1886,&g_1886,&g_1886,&g_1886,(void*)0,&g_1886,(void*)0},{&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886}},{{&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,(void*)0},{&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886}},{{&g_1886,&g_1886,(void*)0,&g_1886,&g_1886,(void*)0,&g_1886},{&g_1886,&g_1886,&g_1886,(void*)0,&g_1886,(void*)0,&g_1886}},{{&g_1886,&g_1886,(void*)0,&g_1886,&g_1886,&g_1886,&g_1886},{&g_1886,&g_1886,&g_1886,(void*)0,&g_1886,&g_1886,&g_1886}},{{&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886},{&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886,&g_1886}}};
static uint32_t ***g_2296 = &g_2297[7][0][4];
static uint32_t ***g_2299 = &g_2297[7][0][5];
static volatile uint16_t g_2314 = 0x6FCCL;/* VOLATILE GLOBAL g_2314 */
static int32_t ***g_2317 = &g_1393;
static int32_t **** volatile g_2316 = &g_2317;/* VOLATILE GLOBAL g_2316 */
static const int32_t *g_2332 = &g_10;
static const int32_t ** volatile g_2331 = &g_2332;/* VOLATILE GLOBAL g_2331 */
static int64_t g_2408 = 0xCAF2BF2F772618A8LL;
static const int16_t * volatile g_2495 = &g_8[0];/* VOLATILE GLOBAL g_2495 */
static const int16_t * volatile *g_2494[2][2][6] = {{{&g_2495,&g_2495,(void*)0,(void*)0,&g_2495,&g_2495},{&g_2495,(void*)0,(void*)0,&g_2495,&g_2495,(void*)0}},{{&g_2495,&g_2495,(void*)0,(void*)0,&g_2495,&g_2495},{&g_2495,(void*)0,(void*)0,&g_2495,&g_2495,(void*)0}}};
static const int16_t * volatile ** volatile g_2493 = &g_2494[0][0][4];/* VOLATILE GLOBAL g_2493 */
static const int16_t * volatile ** volatile * volatile g_2492 = &g_2493;/* VOLATILE GLOBAL g_2492 */
static const int16_t * volatile ** volatile * volatile *g_2491 = &g_2492;
static volatile int16_t * volatile g_2610 = &g_197.f3;/* VOLATILE GLOBAL g_2610 */
static volatile int16_t * volatile *g_2609[2] = {&g_2610,&g_2610};
static const uint16_t **g_2630 = (void*)0;
static const uint16_t g_2634 = 1UL;
static int32_t ****g_2709[7][10] = {{&g_2317,(void*)0,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,(void*)0,&g_2317,&g_2317},{&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317},{&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317},{&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,(void*)0,&g_2317,&g_2317,&g_2317,&g_2317},{&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317},{&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317},{&g_2317,(void*)0,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317,&g_2317}};
static int32_t *****g_2708[6] = {&g_2709[5][4],&g_2709[1][3],&g_2709[1][3],&g_2709[5][4],&g_2709[1][3],&g_2709[1][3]};
static int32_t g_2716 = 6L;
static uint16_t *****g_2760[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static volatile int16_t g_2821 = 0L;/* VOLATILE GLOBAL g_2821 */
static volatile int16_t *g_2820 = &g_2821;
static volatile int16_t ** volatile g_2819[3][4] = {{(void*)0,&g_2820,(void*)0,(void*)0},{&g_2820,&g_2820,&g_2820,&g_2820},{&g_2820,(void*)0,(void*)0,&g_2820}};
static volatile int16_t ** volatile * const g_2818 = &g_2819[1][2];
static volatile int16_t ** volatile * const *g_2817[2] = {&g_2818,&g_2818};
static volatile int16_t ** volatile * const **g_2816[7] = {&g_2817[0],&g_2817[0],&g_2817[0],&g_2817[0],&g_2817[0],&g_2817[0],&g_2817[0]};
static int64_t g_2839 = 0x9F2203017559879DLL;
static uint32_t g_2862[8] = {0UL,18446744073709551610UL,0UL,18446744073709551610UL,0UL,18446744073709551610UL,0UL,18446744073709551610UL};
static int16_t ***g_2880[9] = {&g_158,(void*)0,(void*)0,&g_158,(void*)0,(void*)0,&g_158,(void*)0,(void*)0};
static uint8_t **g_2891[7][1][7] = {{{&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,(void*)0}},{{&g_938,&g_938,&g_938,&g_938,(void*)0,&g_938,&g_938}},{{&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938}},{{&g_938,&g_938,&g_938,&g_938,(void*)0,(void*)0,&g_938}},{{&g_938,(void*)0,&g_938,&g_938,&g_938,&g_938,&g_938}},{{&g_938,(void*)0,&g_938,&g_938,(void*)0,&g_938,(void*)0}},{{&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938}}};
static uint8_t ***g_2890 = &g_2891[0][0][4];
static uint32_t g_2894 = 1UL;
static volatile uint32_t g_2918 = 0UL;/* VOLATILE GLOBAL g_2918 */
static uint16_t g_2925 = 3UL;
static const int32_t ** volatile g_2927 = &g_2332;/* VOLATILE GLOBAL g_2927 */
static volatile int32_t g_2950 = 0xD205D3FCL;/* VOLATILE GLOBAL g_2950 */
static uint8_t g_2960 = 0xBBL;
static int16_t ****g_2982[8] = {&g_2880[4],(void*)0,&g_2880[4],(void*)0,&g_2880[4],(void*)0,&g_2880[4],(void*)0};
static uint8_t g_2993 = 0xC0L;
static volatile float g_3034[2] = {(-0x1.2p+1),(-0x1.2p+1)};
static const int32_t ** volatile g_3036 = &g_2332;/* VOLATILE GLOBAL g_3036 */
static int32_t ***** volatile g_3039 = (void*)0;/* VOLATILE GLOBAL g_3039 */
static const int32_t ** const  volatile g_3078[1] = {&g_2332};
static const struct S0 **g_3084 = (void*)0;
static volatile struct S0 g_3115 = {0L,0xFEL,18446744073709551607UL,0L};/* VOLATILE GLOBAL g_3115 */
static float g_3146[7] = {0x6.8E77F1p-90,(-0x1.1p-1),(-0x1.1p-1),0x6.8E77F1p-90,(-0x1.1p-1),(-0x1.1p-1),0x6.8E77F1p-90};
static uint16_t *g_3196 = &g_702;
static uint16_t ** const g_3195 = &g_3196;
static uint16_t ** const *g_3194 = &g_3195;
static uint16_t ** const **g_3193 = &g_3194;
static volatile uint32_t g_3208 = 0xF804782BL;/* VOLATILE GLOBAL g_3208 */
static int32_t g_3225 = 0L;
static uint32_t * volatile * volatile *g_3245 = (void*)0;
static const int16_t *g_3259[9][4][4] = {{{&g_304,(void*)0,&g_304,&g_304},{&g_304,(void*)0,&g_304,&g_304},{(void*)0,(void*)0,&g_304,(void*)0},{&g_304,&g_304,&g_304,&g_304}},{{&g_304,&g_304,&g_304,(void*)0},{(void*)0,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304},{(void*)0,&g_304,&g_304,&g_304}},{{&g_304,&g_304,&g_304,&g_304},{(void*)0,(void*)0,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304}},{{(void*)0,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304},{&g_304,&g_304,(void*)0,&g_304}},{{&g_304,&g_304,&g_304,&g_304},{(void*)0,(void*)0,(void*)0,&g_304},{&g_304,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304}},{{&g_304,&g_304,&g_304,&g_304},{&g_304,&g_304,(void*)0,(void*)0},{(void*)0,&g_304,&g_304,&g_304},{&g_304,&g_304,(void*)0,(void*)0}},{{&g_304,(void*)0,&g_304,&g_304},{&g_304,(void*)0,&g_304,&g_304},{&g_304,(void*)0,&g_304,&g_304},{(void*)0,(void*)0,&g_304,(void*)0}},{{&g_304,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,(void*)0},{(void*)0,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304}},{{(void*)0,&g_304,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304},{(void*)0,(void*)0,&g_304,&g_304},{&g_304,&g_304,&g_304,&g_304}}};
static const int16_t **g_3258[3][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
static const int16_t ***g_3257 = &g_3258[1][1];
static const int16_t ****g_3256 = &g_3257;
static volatile int32_t g_3279 = 0xC873DD10L;/* VOLATILE GLOBAL g_3279 */
static int32_t ** const *g_3302[3] = {(void*)0,(void*)0,(void*)0};
static int32_t ** const **g_3301 = &g_3302[0];
static const uint16_t ****g_3322 = (void*)0;
static uint32_t *g_3331 = &g_800.f2;
static uint64_t g_3374[3][8] = {{18446744073709551615UL,1UL,18446744073709551615UL,0xD84DADB016807AC1LL,1UL,0x1DB8204AE1CE5001LL,0x1DB8204AE1CE5001LL,1UL},{1UL,0x1DB8204AE1CE5001LL,0x1DB8204AE1CE5001LL,1UL,0xD84DADB016807AC1LL,18446744073709551615UL,1UL,18446744073709551615UL},{1UL,0xC3B35C90251FC0BBLL,0UL,0xC3B35C90251FC0BBLL,1UL,0UL,6UL,6UL}};
static uint32_t **** volatile g_3394 = &g_2299;/* VOLATILE GLOBAL g_3394 */
static int32_t g_3416 = 6L;
static volatile uint32_t g_3512 = 4294967295UL;/* VOLATILE GLOBAL g_3512 */
static uint32_t ** const g_3551 = &g_3331;
static uint32_t ** const *g_3550 = &g_3551;
static int8_t g_3588 = 0xB4L;
static volatile uint64_t **g_3642 = &g_1143;
static volatile uint64_t *** volatile g_3641 = &g_3642;/* VOLATILE GLOBAL g_3641 */
static volatile uint64_t *** volatile * volatile g_3643 = &g_3641;/* VOLATILE GLOBAL g_3643 */


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int32_t * func_19(uint64_t  p_20);
static uint64_t  func_21(int16_t * p_22, int32_t  p_23, int32_t ** p_24);
static int32_t  func_26(int32_t  p_27, uint32_t  p_28, int32_t  p_29, int32_t * p_30, int64_t  p_31);
static uint16_t  func_36(const int32_t * p_37, int32_t * const  p_38, uint8_t  p_39);
static int32_t ** const  func_44(int16_t * p_45);
static int32_t  func_51(int8_t  p_52, int16_t *** p_53, int16_t *** p_54);
static int32_t  func_63(int16_t  p_64, int16_t * const ** p_65, int16_t *** p_66, const float  p_67, int32_t ** const  p_68);
static struct S0  func_75(int8_t  p_76, int16_t * p_77, float  p_78, uint8_t  p_79, int32_t * p_80);
static uint32_t  func_83(int16_t  p_84, int32_t ** p_85, int32_t  p_86, int32_t  p_87, int64_t  p_88);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3550 g_3551 g_3331 g_800.f2
 * writes:
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int64_t l_6[10][2][8] = {{{0L,0x1718BC698DC509CALL,1L,0x35F19716E289AA50LL,(-3L),6L,0x9F41AF53C8CBF698LL,(-1L)},{(-2L),0x9E13C9534CE5FEF4LL,0xCA44BAB92A1DD6B9LL,(-3L),0xA5166941FC9A57B5LL,0x9E864ADB5D2300B6LL,0x779A7B445ACEE617LL,0x74AA0D72276B87D0LL}},{{0x9E98C53483200138LL,0x4ADE726F2C0E8489LL,0x49B76EEA53061ECELL,(-1L),0xC629C4DDB84C6A37LL,0xFADACD7D393E75CBLL,1L,0L},{0x69B7D6B59229A275LL,1L,6L,(-8L),0x506946CB4F5FDFDALL,0L,0x74AA0D72276B87D0LL,0xCA44BAB92A1DD6B9LL}},{{0x22781501494D85D5LL,1L,(-1L),0x3F5047C4D1454BCDLL,(-6L),(-8L),0x4E2B783560277ED4LL,(-3L)},{0xC67A95534FD39EF5LL,0x3CC9D6C8400B8F3BLL,0x3F5047C4D1454BCDLL,(-9L),0xFE44FA3FDA37B187LL,0x3E5EC35211521FAFLL,0x9F41AF53C8CBF698LL,0x4ADE726F2C0E8489LL}},{{0x9F41AF53C8CBF698LL,0x506946CB4F5FDFDALL,0x0EEC299E885DC1B6LL,1L,0x9E13C9534CE5FEF4LL,7L,0x3F5047C4D1454BCDLL,0L},{0xFADACD7D393E75CBLL,0x49B76EEA53061ECELL,0xEB927EED050B7F45LL,0xE12FD89D6848298BLL,0L,0L,0xE12FD89D6848298BLL,0xEB927EED050B7F45LL}},{{(-5L),(-5L),0x779A7B445ACEE617LL,0x0EEC299E885DC1B6LL,(-3L),0x506946CB4F5FDFDALL,(-10L),0x42628217C7128ABFLL},{(-6L),0xD68C2AFE2499E68FLL,0xA5166941FC9A57B5LL,0x6CE4A6F44401D1B9LL,0L,0L,0x0F2D6273D5943E2FLL,0x42628217C7128ABFLL}},{{0xD68C2AFE2499E68FLL,0xE12FD89D6848298BLL,0xFADACD7D393E75CBLL,0x0EEC299E885DC1B6LL,1L,0x11140AE7762969FBLL,(-6L),0xEB927EED050B7F45LL},{0x4ADE726F2C0E8489LL,0x9E864ADB5D2300B6LL,0L,0xE12FD89D6848298BLL,(-1L),(-10L),0xCA44BAB92A1DD6B9LL,0L}},{{0xC629C4DDB84C6A37LL,0x1718BC698DC509CALL,0x74AA0D72276B87D0LL,1L,0x4E2B783560277ED4LL,0x9E98C53483200138LL,7L,0x4ADE726F2C0E8489LL},{0x74AA0D72276B87D0LL,(-1L),(-2L),(-9L),1L,(-1L),9L,(-3L)}},{{(-8L),0x779A7B445ACEE617LL,0x11140AE7762969FBLL,0x3F5047C4D1454BCDLL,0x4ADE726F2C0E8489LL,0xC18CC907F7A796FBLL,0L,0xCA44BAB92A1DD6B9LL},{0L,0x35F19716E289AA50LL,1L,(-8L),0x051EBF0DDD9CD443LL,0L,0x49B76EEA53061ECELL,0L}},{{0L,(-1L),(-5L),(-1L),0L,0x7EFBAE54FD57ECA5LL,0xC67A95534FD39EF5LL,0x74AA0D72276B87D0LL},{0x1718BC698DC509CALL,1L,0x35F19716E289AA50LL,(-3L),6L,0x9F41AF53C8CBF698LL,0x9E13C9534CE5FEF4LL,(-1L)}},{{0x42628217C7128ABFLL,0L,0x35F19716E289AA50LL,0x506946CB4F5FDFDALL,0xEB927EED050B7F45LL,0x22781501494D85D5LL,0xC67A95534FD39EF5LL,0x6CE4A6F44401D1B9LL},{6L,(-1L),(-5L),0x74AA0D72276B87D0LL,0x11140AE7762969FBLL,0L,0x49B76EEA53061ECELL,(-6L)}}};
    int16_t *l_7 = &g_8[0];
    int32_t *l_9[3][10] = {{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10}};
    uint32_t l_14 = 4UL;
    int32_t *****l_2707 = (void*)0;
    int8_t l_2777 = 0x7CL;
    uint16_t l_2822 = 0x5492L;
    int16_t ***l_2883 = (void*)0;
    uint8_t *l_2903 = &g_592;
    uint16_t *l_2909 = &g_982;
    uint16_t **l_2908[10][3][8] = {{{&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,(void*)0,&l_2909},{(void*)0,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909},{&l_2909,(void*)0,&l_2909,(void*)0,(void*)0,&l_2909,&l_2909,&l_2909}},{{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909},{(void*)0,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909},{(void*)0,&l_2909,&l_2909,(void*)0,&l_2909,(void*)0,&l_2909,&l_2909}},{{&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,(void*)0,(void*)0,&l_2909,&l_2909,&l_2909},{(void*)0,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909}},{{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,(void*)0}},{{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0},{&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909}},{{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,&l_2909},{(void*)0,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909},{&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,(void*)0,(void*)0}},{{&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909}},{{&l_2909,(void*)0,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909,(void*)0,(void*)0},{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0}},{{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,(void*)0,&l_2909,(void*)0,&l_2909,&l_2909,(void*)0,(void*)0},{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0,&l_2909}},{{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909},{&l_2909,(void*)0,(void*)0,&l_2909,&l_2909,&l_2909,&l_2909,(void*)0},{&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909,&l_2909}}};
    int32_t l_2919 = 0xD7F98781L;
    uint32_t l_2921 = 0UL;
    const int32_t ***l_2928 = &g_1914;
    uint64_t l_2972 = 18446744073709551615UL;
    int16_t l_2986 = (-6L);
    int64_t l_3031 = 0x0DDE0E265F0E641FLL;
    float l_3074 = 0xF.30F6E4p-64;
    int16_t l_3075[4];
    int16_t l_3178 = 1L;
    int8_t **l_3204[10] = {&g_1082,&g_1082,&g_1761,&g_1761,&g_1761,&g_1082,&g_1082,&g_1761,&g_1761,&g_1761};
    float l_3216 = (-0x7.1p+1);
    uint16_t ** const **l_3240 = &g_3194;
    uint32_t *l_3244 = &g_2862[1];
    uint32_t * const *l_3243 = &l_3244;
    uint32_t * const **l_3242 = &l_3243;
    uint32_t l_3265 = 0UL;
    int16_t *****l_3283 = &g_2982[4];
    const int32_t **l_3349[1];
    const int32_t ***l_3348 = &l_3349[0];
    const int32_t ****l_3347 = &l_3348;
    const uint64_t l_3414 = 0x31ECC04AF2BCF641LL;
    int16_t l_3580 = 0x0488L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_3075[i] = 0x0270L;
    for (i = 0; i < 1; i++)
        l_3349[i] = &g_2332;
    return (***g_3550);
}


/* ------------------------------------------ */
/* 
 * reads : g_158 g_159 g_8 g_17 g_18 g_786 g_359 g_360 g_584 g_1379 g_938 g_1386 g_1392 g_1233 g_10 g_1912 g_592 g_1795 g_1796 g_1374 g_1234 g_982 g_1082 g_165 g_1761 g_583 g_2013 g_1328 g_1151 g_1152 g_207 g_491 g_245 g_2061 g_213 g_214 g_47 g_48 g_2144 g_1520 g_1327 g_2162 g_309 g_753 g_785 g_197.f2 g_433 g_262 g_2227 g_261 g_1244 g_702 g_2265 g_2268 g_2289 g_2296 g_815.f2 g_1081 g_968 g_1337
 * writes: g_8 g_1337 g_583 g_592 g_1387 g_1393 g_214 g_1379.f2 g_1234 g_1002 g_702 g_800.f2 g_348 g_10 g_1912 g_584 g_491 g_939.f2 g_982 g_245 g_967 g_207 g_165 g_1004 g_1374 g_159 g_262 g_981 g_753 g_1537.f2 g_786 g_2227 g_1244 g_2289 g_2296 g_2299 g_1233 g_815.f2 g_2162.f2 g_1145 g_968
 */
static int32_t * func_19(uint64_t  p_20)
{ /* block id: 628 */
    int32_t *l_1365 = &g_584;
    int32_t l_1384[5][8][3] = {{{0xD5F8749EL,0xF04523D5L,0x42BBBE50L},{(-1L),0x4C21A478L,2L},{0x5534D787L,4L,4L},{0x75C9E88EL,1L,0xC3B7E9DCL},{0x75C9E88EL,0xC3B7E9DCL,(-1L)},{0x5534D787L,0xE281436DL,0L},{(-1L),0xD5F8749EL,0L},{0xD5F8749EL,0xE281436DL,0xF67E48DAL}},{{0x4C21A478L,0xC3B7E9DCL,0x4C21A478L},{0xE281436DL,1L,0x4C21A478L},{2L,4L,0xF67E48DAL},{0xF67E48DAL,0x4C21A478L,0L},{1L,0xF04523D5L,0L},{0xF67E48DAL,9L,(-1L)},{2L,2L,0xC3B7E9DCL},{0xE281436DL,2L,4L}},{{0x4C21A478L,9L,2L},{0xD5F8749EL,0xF04523D5L,0x42BBBE50L},{(-1L),0x4C21A478L,2L},{0x5534D787L,4L,4L},{0x75C9E88EL,1L,0xC3B7E9DCL},{0x75C9E88EL,0xC3B7E9DCL,(-1L)},{0x5534D787L,0xE281436DL,0L},{(-1L),0xD5F8749EL,0L}},{{0xD5F8749EL,0xE281436DL,0xF67E48DAL},{0x4C21A478L,0xC3B7E9DCL,0x4C21A478L},{0xE281436DL,1L,0x4C21A478L},{2L,4L,0xF67E48DAL},{0xF67E48DAL,0x4C21A478L,0L},{1L,0xF04523D5L,0L},{0xF67E48DAL,9L,(-1L)},{2L,2L,0xC3B7E9DCL}},{{0xE281436DL,2L,4L},{0x4C21A478L,9L,2L},{0xD5F8749EL,0xF04523D5L,0x42BBBE50L},{(-1L),0x4C21A478L,2L},{0x5534D787L,4L,4L},{0x75C9E88EL,1L,0xC3B7E9DCL},{0x75C9E88EL,0xC3B7E9DCL,(-1L)},{0x5534D787L,0xE281436DL,0L}}};
    int32_t l_1418[7] = {0xDF7E54C6L,0x62BEBC57L,0x62BEBC57L,0xDF7E54C6L,0x62BEBC57L,0x62BEBC57L,0xDF7E54C6L};
    uint32_t l_1469 = 0xC7D96CB4L;
    uint8_t **l_1590[5] = {&g_938,&g_938,&g_938,&g_938,&g_938};
    uint16_t ***l_1611 = (void*)0;
    int32_t ***l_1650 = (void*)0;
    int8_t l_1658[1];
    uint64_t ***l_1669 = &g_1145[3][1][2];
    float l_1685 = 0xD.69D9A3p-80;
    int32_t ***l_1743 = &g_786;
    int16_t *l_1821[8][4][4] = {{{&g_1822,(void*)0,(void*)0,&g_1822},{&g_1822,(void*)0,&g_1822,&g_1822},{&g_1822,(void*)0,(void*)0,(void*)0},{&g_1822,&g_1822,&g_1822,(void*)0}},{{&g_1822,&g_1822,&g_1822,&g_1822},{(void*)0,&g_1822,(void*)0,(void*)0},{&g_1822,(void*)0,(void*)0,(void*)0},{&g_1822,&g_1822,&g_1822,&g_1822}},{{(void*)0,&g_1822,&g_1822,&g_1822},{&g_1822,&g_1822,&g_1822,&g_1822},{&g_1822,&g_1822,&g_1822,&g_1822},{&g_1822,&g_1822,&g_1822,&g_1822}},{{&g_1822,&g_1822,(void*)0,(void*)0},{(void*)0,&g_1822,&g_1822,(void*)0},{&g_1822,&g_1822,&g_1822,(void*)0},{&g_1822,&g_1822,&g_1822,&g_1822}},{{(void*)0,&g_1822,&g_1822,&g_1822},{(void*)0,&g_1822,&g_1822,&g_1822},{&g_1822,&g_1822,(void*)0,&g_1822},{&g_1822,&g_1822,&g_1822,&g_1822}},{{(void*)0,&g_1822,&g_1822,(void*)0},{&g_1822,(void*)0,&g_1822,(void*)0},{(void*)0,&g_1822,&g_1822,&g_1822},{&g_1822,&g_1822,&g_1822,(void*)0}},{{(void*)0,&g_1822,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1822},{&g_1822,&g_1822,&g_1822,&g_1822},{(void*)0,(void*)0,&g_1822,&g_1822}},{{&g_1822,&g_1822,&g_1822,(void*)0},{(void*)0,&g_1822,&g_1822,&g_1822},{&g_1822,&g_1822,(void*)0,&g_1822},{(void*)0,&g_1822,(void*)0,(void*)0}}};
    int16_t l_1827 = 0xBC08L;
    int8_t ***l_1946 = &g_1081;
    float l_1973 = 0xD.9FFBD4p-26;
    struct S0 *l_1974[1][3][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,&g_815,(void*)0},{(void*)0,(void*)0,(void*)0}}};
    uint32_t l_2014[5][2] = {{4294967290UL,4294967290UL},{7UL,4294967290UL},{4294967290UL,7UL},{4294967290UL,4294967290UL},{7UL,4294967290UL}};
    int32_t l_2033 = (-2L);
    uint8_t l_2086 = 255UL;
    uint32_t l_2100 = 0x17CEC09FL;
    struct S0 * const *l_2161 = &l_1974[0][1][1];
    struct S0 * const **l_2160 = &l_2161;
    uint32_t ** const *l_2300 = &g_2297[3][0][4];
    uint16_t ****l_2321 = &l_1611;
    uint16_t *****l_2320 = &l_2321;
    uint8_t ** const *l_2334[7][8] = {{(void*)0,&l_1590[4],&l_1590[2],&l_1590[2],&l_1590[1],(void*)0,(void*)0,(void*)0},{&l_1590[0],&l_1590[1],&l_1590[0],&l_1590[1],&l_1590[0],&l_1590[2],&l_1590[2],(void*)0},{&l_1590[2],(void*)0,&l_1590[4],&l_1590[0],(void*)0,&l_1590[2],(void*)0,&l_1590[1]},{&l_1590[0],&l_1590[2],&l_1590[4],&l_1590[4],(void*)0,&l_1590[2],&l_1590[2],(void*)0},{(void*)0,&l_1590[0],&l_1590[0],(void*)0,&l_1590[2],&l_1590[2],(void*)0,(void*)0},{&l_1590[4],&l_1590[4],&l_1590[2],&l_1590[0],&l_1590[2],&l_1590[2],&l_1590[0],&l_1590[0]},{&l_1590[0],&l_1590[4],(void*)0,(void*)0,(void*)0,&l_1590[2],(void*)0,(void*)0}};
    uint8_t ** const **l_2333 = &l_2334[4][4];
    uint32_t l_2406 = 4294967287UL;
    uint32_t l_2502[8] = {0x20F3B2C5L,0xB5DD2DD8L,0x20F3B2C5L,0xB5DD2DD8L,0x20F3B2C5L,0xB5DD2DD8L,0x20F3B2C5L,0xB5DD2DD8L};
    int32_t l_2660 = (-4L);
    int64_t l_2662[1];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1658[i] = (-1L);
    for (i = 0; i < 1; i++)
        l_2662[i] = 0x1659351576C69105LL;
lbl_2010:
    if ((safe_rshift_func_uint16_t_u_s(5UL, ((*g_159) = (**g_158)))))
    { /* block id: 630 */
        int32_t l_1364 = 0xA19A51D5L;
        for (g_1337 = (-18); (g_1337 >= 23); g_1337 = safe_add_func_uint8_t_u_u(g_1337, 6))
        { /* block id: 633 */
            int32_t *l_1363 = &g_584;
            l_1363 = (*g_17);
            if (l_1364)
                continue;
            return l_1363;
        }
        (*g_786) = l_1365;
    }
    else
    { /* block id: 639 */
        uint16_t * const **l_1370 = (void*)0;
        uint16_t * const l_1373 = &g_1374;
        uint16_t * const *l_1372 = &l_1373;
        uint16_t * const **l_1371 = &l_1372;
        int16_t * const *l_1385 = (void*)0;
        int32_t l_1414[3];
        uint32_t *l_1459[10] = {&g_800.f2,(void*)0,(void*)0,&g_800.f2,&g_197.f2,&g_800.f2,(void*)0,(void*)0,&g_800.f2,&g_197.f2};
        int32_t l_1488 = (-1L);
        uint32_t *l_1527 = &g_981[1][0];
        uint32_t **l_1526 = &l_1527;
        int32_t ***l_1538[6][3] = {{&g_786,&g_786,&g_786},{&g_786,&g_786,(void*)0},{&g_786,&g_786,&g_786},{&g_786,(void*)0,(void*)0},{&g_786,&g_786,&g_786},{&g_786,&g_786,(void*)0}};
        int64_t l_1586 = 0L;
        uint32_t l_1605 = 5UL;
        uint64_t *l_1613 = &g_491;
        uint64_t **l_1633 = &l_1613;
        int32_t ***l_1649 = &g_1393;
        uint64_t ***l_1666 = (void*)0;
        int16_t **l_1674 = &g_159;
        const uint8_t l_1710 = 3UL;
        struct S0 *l_1747 = &g_588;
        int8_t *l_1762 = &l_1658[0];
        int32_t ***l_1767 = &g_1393;
        int32_t l_1838 = (-1L);
        uint32_t ***l_1878 = &l_1526;
        uint8_t l_1879 = 249UL;
        const uint16_t *l_1883 = (void*)0;
        const uint16_t * const *l_1882 = &l_1883;
        const uint16_t * const **l_1881 = &l_1882;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1414[i] = 0x0F39DC60L;
        (*g_1386) = ((safe_sub_func_int32_t_s_s(((((safe_sub_func_uint16_t_u_u((((*l_1371) = (*g_359)) == ((safe_lshift_func_int16_t_s_u(((*g_159) = (((*l_1365) && (safe_add_func_uint8_t_u_u((g_1379 , ((*g_938) = p_20)), p_20))) & ((safe_unary_minus_func_int32_t_s((+1L))) == (((safe_rshift_func_int8_t_s_u((*l_1365), 4)) == 0x00D0DDD4L) && p_20)))), 15)) , (*g_359))), p_20)) == p_20) >= 0x8819L) || p_20), l_1384[0][5][2])) , l_1385);
        for (g_592 = 0; (g_592 > 10); g_592 = safe_add_func_int8_t_s_s(g_592, 7))
        { /* block id: 646 */
            int32_t *l_1391[9][9] = {{&g_1104,&l_1384[0][5][2],(void*)0,&l_1384[0][5][2],&g_1104,(void*)0,&l_1384[0][5][2],&l_1384[0][5][2],(void*)0},{&l_1384[2][7][0],&l_1384[1][6][0],&l_1384[0][5][2],&l_1384[1][6][0],&l_1384[2][7][0],&l_1384[0][5][2],&g_1104,&g_1104,&l_1384[0][5][2]},{&g_1104,&l_1384[0][5][2],(void*)0,&l_1384[0][5][2],&g_1104,(void*)0,&l_1384[0][5][2],&l_1384[0][5][2],(void*)0},{&l_1384[2][7][0],&l_1384[1][6][0],&l_1384[0][5][2],&l_1384[1][6][0],&l_1384[2][7][0],&l_1384[0][5][2],&g_1104,&g_1104,&l_1384[0][5][2]},{&g_1104,&l_1384[0][5][2],(void*)0,&l_1384[0][5][2],&g_1104,(void*)0,&l_1384[0][5][2],&l_1384[0][5][2],(void*)0},{&l_1384[2][7][0],&l_1384[1][6][0],&l_1384[0][5][2],&l_1384[1][6][0],&l_1384[2][7][0],&l_1384[0][5][2],&g_1104,&g_1104,&l_1384[0][5][2]},{&g_1104,&l_1384[0][5][2],(void*)0,&l_1384[0][5][2],&g_1104,(void*)0,&l_1384[0][5][2],&l_1384[0][5][2],(void*)0},{&l_1384[2][7][0],&l_1384[1][6][0],&l_1384[0][5][2],&l_1384[1][6][0],&l_1384[2][7][0],&l_1384[0][5][2],&g_1104,&g_1104,&l_1384[0][5][2]},{&g_1104,&l_1384[0][5][2],(void*)0,&l_1384[0][5][2],&g_1104,(void*)0,&l_1384[0][5][2],&l_1384[0][5][2],(void*)0}};
            int32_t **l_1390[1][9] = {{&l_1391[2][3],&l_1391[2][3],&l_1391[2][3],&l_1391[2][3],&l_1391[2][3],&l_1391[2][3],&l_1391[2][3],&l_1391[2][3],&l_1391[2][3]}};
            int8_t *l_1403 = (void*)0;
            uint32_t l_1404 = 0xB7D4B348L;
            int8_t l_1407 = 0L;
            uint32_t *l_1415[4];
            int32_t l_1416 = 0xD141E2ADL;
            int32_t *l_1417[8] = {(void*)0,&l_1414[2],(void*)0,&l_1414[2],(void*)0,&l_1414[2],(void*)0,&l_1414[2]};
            float *l_1422 = &g_967;
            uint16_t * const ***l_1442 = (void*)0;
            uint16_t * const ****l_1441 = &l_1442;
            int i, j;
            for (i = 0; i < 4; i++)
                l_1415[i] = &g_981[5][5];
            (*g_1392) = l_1390[0][7];
        }
        for (g_214 = 27; (g_214 == 8); g_214 = safe_sub_func_uint32_t_u_u(g_214, 3))
        { /* block id: 694 */
            int32_t *l_1507 = &g_1002;
            int32_t l_1517 = 7L;
            float l_1616 = 0x8.484658p+84;
            uint64_t ***l_1631 = (void*)0;
            uint64_t ***l_1632 = &g_1145[1][7][2];
            int32_t ***l_1651 = &g_1393;
            uint16_t l_1659 = 0x8B4BL;
            int32_t l_1693 = 0x2429F411L;
            float l_1700 = 0x2.51F6DBp-70;
            uint8_t ***l_1720 = &l_1590[2];
            int32_t *l_1746[3];
            uint32_t l_1792 = 0x33510346L;
            uint32_t l_1880[7];
            int i;
            for (i = 0; i < 3; i++)
                l_1746[i] = &l_1418[2];
            for (i = 0; i < 7; i++)
                l_1880[i] = 1UL;
        }
    }
    for (g_1379.f2 = 0; (g_1379.f2 > 22); ++g_1379.f2)
    { /* block id: 860 */
        uint8_t l_1889[10] = {6UL,6UL,6UL,6UL,6UL,6UL,6UL,6UL,6UL,6UL};
        struct S0 *l_1892[7][4][3] = {{{&g_589,&g_589,&g_1379},{&g_815,&g_1379,(void*)0},{(void*)0,&g_1379,&g_815},{&g_1379,&g_589,&g_589}},{{&g_365,(void*)0,&g_815},{&g_589,&g_815,(void*)0},{&g_589,&g_589,&g_1379},{&g_365,&g_1537,&g_365}},{{&g_1379,&g_589,&g_589},{(void*)0,&g_815,&g_589},{&g_815,(void*)0,&g_365},{&g_589,&g_589,&g_1379}},{{&g_815,&g_1379,(void*)0},{(void*)0,&g_1379,&g_589},{&g_589,(void*)0,(void*)0},{&g_589,&g_1537,&g_589}},{{&g_365,&g_815,&g_1537},{&g_365,&g_1379,&g_589},{&g_589,&g_1379,&g_589},{&g_589,&g_1379,&g_365}},{{&g_1537,&g_815,&g_365},{&g_589,&g_1537,&g_589},{(void*)0,(void*)0,&g_589},{&g_589,&g_589,&g_1537}},{{&g_1537,&g_589,&g_589},{&g_589,(void*)0,(void*)0},{&g_589,&g_1537,&g_589},{&g_365,&g_815,&g_1537}}};
        uint32_t l_1898 = 0xF2339629L;
        int32_t l_1923 = 0L;
        int32_t **l_1925[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t *l_1979 = &g_968[4];
        int32_t l_1993 = 0xC32807C0L;
        int32_t l_1994 = 0x0FDE7D90L;
        int32_t l_1996 = (-1L);
        int32_t l_2000 = 0x7A2FEB05L;
        int32_t l_2003 = 0x34C86E4DL;
        int32_t l_2004 = 1L;
        int32_t *l_2031 = &l_1994;
        uint64_t l_2032[7];
        int8_t *l_2034 = &g_1337;
        int64_t *l_2035 = (void*)0;
        int64_t *l_2036 = (void*)0;
        int64_t *l_2037 = &g_1004;
        int32_t ****l_2060 = &l_1650;
        int32_t *****l_2059 = &l_2060;
        int32_t l_2074 = 1L;
        int32_t l_2075 = 0x24E64488L;
        int32_t l_2076 = 0x0F3A758FL;
        int32_t l_2078 = 6L;
        float l_2079[6];
        int32_t l_2080 = 0xE8DFB011L;
        int32_t l_2081 = 0x82C5E0DFL;
        int32_t l_2082 = (-10L);
        int32_t l_2083 = (-1L);
        int32_t l_2084[3];
        int32_t *l_2149 = &g_753;
        int32_t *l_2152 = &g_753;
        const struct S0 ***l_2156 = (void*)0;
        struct S0 *l_2184 = &g_588;
        int64_t l_2230 = (-1L);
        uint64_t ***l_2247 = &g_1145[1][7][2];
        float l_2269 = 0x0.Dp-1;
        int16_t l_2303 = (-1L);
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_2032[i] = 0xCB60EBADD0E4F14DLL;
        for (i = 0; i < 6; i++)
            l_2079[i] = (-0x1.6p-1);
        for (i = 0; i < 3; i++)
            l_2084[i] = 0x261812E4L;
        l_1889[2] &= 0x11052DC8L;
        for (p_20 = 0; (p_20 != 10); p_20 = safe_add_func_uint64_t_u_u(p_20, 2))
        { /* block id: 864 */
            int8_t l_1905 = 1L;
            int32_t **l_1926 = &g_1394;
            struct S0 *l_1932 = &g_1933;
            int32_t *l_1951 = &l_1418[2];
            int32_t l_1980 = (-5L);
            int32_t l_1981 = (-1L);
            int32_t l_1995 = 0L;
            int32_t l_1998[3][9] = {{0xB404CF7EL,(-9L),(-9L),0xB404CF7EL,0xCDD3D62AL,0xB404CF7EL,(-9L),(-9L),0xB404CF7EL},{0xAAA083C0L,(-9L),0xAA001735L,(-9L),0xAAA083C0L,0xAAA083C0L,(-9L),0xAA001735L,(-9L)},{(-9L),0xCDD3D62AL,0xAA001735L,0xAA001735L,0xCDD3D62AL,(-9L),0xCDD3D62AL,0xAA001735L,0xAA001735L}};
            int i, j;
            (*g_1233) = l_1892[2][2][1];
            if (((void*)0 != &l_1821[2][3][0]))
            { /* block id: 866 */
                int32_t *l_1901 = (void*)0;
                for (g_1002 = 0; (g_1002 <= 0); g_1002 += 1)
                { /* block id: 869 */
                    for (g_702 = 0; (g_702 <= 1); g_702 += 1)
                    { /* block id: 872 */
                        int32_t *l_1893 = (void*)0;
                        int32_t *l_1894 = &g_48[0];
                        int32_t l_1895[10][8][3] = {{{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,(-10L)},{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L}},{{(-10L),0x57EC1CDBL,(-10L)},{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,(-10L)},{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL}},{{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,(-10L)},{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,(-10L)}},{{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,(-10L)},{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL}},{{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,(-10L)},{0x133341DAL,0xB178FC2FL,1L},{0x133341DAL,0x133341DAL,0xB178FC2FL},{(-10L),0xB178FC2FL,0xB178FC2FL},{0xB178FC2FL,0x57EC1CDBL,1L},{(-10L),0x57EC1CDBL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL}},{{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL}},{{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)}},{{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L}},{{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)}},{{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL},{0xB178FC2FL,0xB178FC2FL,(-10L)},{1L,(-10L),(-10L)},{(-10L),0x133341DAL,0x57EC1CDBL},{1L,0x133341DAL,1L},{0xB178FC2FL,(-10L),0x57EC1CDBL}}};
                        int32_t *l_1896 = &g_753;
                        int32_t *l_1897[4];
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                            l_1897[i] = (void*)0;
                        l_1898--;
                        (**l_1743) = &l_1418[2];
                        return l_1901;
                    }
                    for (g_800.f2 = 0; (g_800.f2 <= 0); g_800.f2 += 1)
                    { /* block id: 879 */
                        int32_t *l_1902 = &g_968[4];
                        (**l_1743) = l_1902;
                    }
                }
                for (g_348 = (-1); (g_348 == 40); g_348 = safe_add_func_int64_t_s_s(g_348, 1))
                { /* block id: 885 */
                    const int32_t *****l_1916 = &g_1912;
                    int8_t * const l_1919 = &g_1920;
                    int8_t * const *l_1918 = &l_1919;
                    int8_t * const **l_1917 = &l_1918;
                    int8_t * const *l_1922 = &l_1919;
                    int8_t * const **l_1921 = &l_1922;
                    (*g_18) ^= l_1905;
                    l_1923 |= (safe_mul_func_int16_t_s_s(((safe_add_func_uint32_t_u_u((p_20 & 9UL), ((*g_18) = ((!(((1UL < (safe_unary_minus_func_uint64_t_u((p_20 , (((**g_158) <= ((((*l_1916) = g_1912) != &g_1392) , (((*g_938) |= (((*l_1917) = &g_1761) == ((*l_1921) = (((p_20 | 0xD9A3L) , 0x6746A4A7L) , &g_1761)))) >= (*l_1365)))) != p_20))))) >= 0x167BL) > p_20)) && 0x34A4L)))) ^ (**g_1795)), 0xAEC3L));
                }
                if ((*g_18))
                    continue;
                for (l_1469 = 0; (l_1469 <= 3); l_1469 += 1)
                { /* block id: 897 */
                    int32_t **l_1924 = (void*)0;
                    if (l_1923)
                        break;
                    (*l_1365) = ((l_1925[0] = l_1924) != l_1926);
                }
            }
            else
            { /* block id: 902 */
                const uint32_t l_1940[7][8][4] = {{{0xC5B96125L,9UL,6UL,18446744073709551608UL},{18446744073709551613UL,9UL,18446744073709551614UL,18446744073709551610UL},{18446744073709551613UL,0UL,6UL,0xFCDCA354L},{0xC5B96125L,18446744073709551610UL,0UL,3UL},{9UL,0xFBEB508EL,18446744073709551614UL,18446744073709551613UL},{0xB2D5BF9AL,1UL,0UL,9UL},{1UL,18446744073709551610UL,18446744073709551608UL,0xF257C8EFL},{0xFCDCA354L,18446744073709551613UL,0x959FD63AL,18446744073709551610UL}},{{0xFFD94CDDL,0xDDB136D2L,0xDDB136D2L,0xFFD94CDDL},{0xF257C8EFL,9UL,18446744073709551608UL,0UL},{0xFBEB508EL,18446744073709551615UL,6UL,18446744073709551615UL},{0xB2D5BF9AL,6UL,0x1CFD9052L,18446744073709551615UL},{0xDDB136D2L,18446744073709551615UL,0UL,0UL},{18446744073709551606UL,9UL,0x4F6B6D50L,0xFFD94CDDL},{18446744073709551613UL,0xDDB136D2L,3UL,18446744073709551610UL},{0UL,18446744073709551613UL,6UL,0xF257C8EFL}},{{18446744073709551606UL,18446744073709551610UL,0x0F725CF5L,1UL},{18446744073709551608UL,0UL,18446744073709551613UL,6UL},{18446744073709551614UL,0xF257C8EFL,18446744073709551614UL,1UL},{0xF257C8EFL,0xDDB136D2L,0x315D19B5L,3UL},{3UL,0x4F6B6D50L,1UL,0xDDB136D2L},{0x0F725CF5L,18446744073709551608UL,1UL,0x0F725CF5L},{3UL,1UL,0x315D19B5L,3UL},{0xF257C8EFL,0UL,18446744073709551614UL,1UL}},{{18446744073709551614UL,1UL,18446744073709551613UL,18446744073709551611UL},{18446744073709551608UL,18446744073709551608UL,18446744073709551615UL,3UL},{0UL,1UL,18446744073709551611UL,0UL},{0x4F6B6D50L,18446744073709551608UL,0x959FD63AL,0x959FD63AL},{6UL,6UL,0xFBEB508EL,3UL},{0UL,0x959FD63AL,0x4F6B6D50L,1UL},{1UL,0xF257C8EFL,18446744073709551613UL,0x4F6B6D50L},{3UL,0xF257C8EFL,18446744073709551610UL,1UL}},{{0xF257C8EFL,0x959FD63AL,18446744073709551614UL,3UL},{0xF4726F74L,6UL,1UL,0x959FD63AL},{0UL,18446744073709551608UL,0xFFD94CDDL,0UL},{3UL,1UL,18446744073709551614UL,3UL},{0UL,18446744073709551608UL,18446744073709551614UL,18446744073709551611UL},{3UL,1UL,18446744073709551606UL,1UL},{18446744073709551608UL,0UL,0x4F6B6D50L,3UL},{6UL,1UL,18446744073709551611UL,0x0F725CF5L}},{{6UL,18446744073709551608UL,9UL,0xDDB136D2L},{6UL,0x4F6B6D50L,18446744073709551611UL,3UL},{6UL,0xDDB136D2L,0x4F6B6D50L,1UL},{18446744073709551608UL,0xF257C8EFL,18446744073709551606UL,6UL},{3UL,0UL,18446744073709551614UL,1UL},{0UL,0xDDB136D2L,18446744073709551614UL,0xF4726F74L},{3UL,6UL,0xFFD94CDDL,0xDDB136D2L},{0UL,1UL,1UL,0UL}},{{0xF4726F74L,1UL,18446744073709551614UL,18446744073709551614UL},{0xF257C8EFL,18446744073709551608UL,18446744073709551610UL,1UL},{3UL,18446744073709551611UL,18446744073709551613UL,1UL},{1UL,18446744073709551608UL,0x4F6B6D50L,18446744073709551614UL},{0UL,1UL,0xFBEB508EL,0UL},{6UL,1UL,0x959FD63AL,0xDDB136D2L},{0x4F6B6D50L,6UL,18446744073709551611UL,0xF4726F74L},{0UL,0xDDB136D2L,18446744073709551615UL,1UL}}};
                int32_t *l_1949 = &l_1418[3];
                const int32_t l_1950 = 1L;
                int i, j, k;
                for (g_491 = 0; (g_491 <= 16); ++g_491)
                { /* block id: 905 */
                    int32_t *l_1929 = &g_968[4];
                    l_1929 = &l_1923;
                    for (g_939.f2 = 6; (g_939.f2 != 33); g_939.f2 = safe_add_func_uint64_t_u_u(g_939.f2, 9))
                    { /* block id: 909 */
                        uint16_t *l_1947 = &g_982;
                        int32_t l_1948 = 0x4FDBB63DL;
                        l_1932 = (*g_1233);
                        (*g_18) = ((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int8_t_s_s(p_20, ((((safe_mul_func_uint16_t_u_u((l_1940[0][7][0] , ((*l_1947) &= ((safe_mul_func_uint16_t_u_u((!((*l_1365) &= (*g_159))), ((safe_add_func_int8_t_s_s((p_20 , (((*g_159) , l_1946) == &g_1081)), 0x60L)) < l_1940[0][7][0]))) >= (**g_1795)))), l_1948)) , p_20) && l_1940[0][7][0]) != l_1940[0][7][0]))), (*g_1082))) & (*g_1761));
                        return (*g_17);
                    }
                }
                (**l_1743) = (*g_17);
                if (l_1950)
                    break;
                (*g_786) = l_1951;
            }
            for (g_245 = 0; (g_245 != 54); ++g_245)
            { /* block id: 923 */
                int32_t *l_1954 = &l_1418[2];
                uint16_t l_1963 = 0xC2C4L;
                int32_t l_1985 = 0L;
                int32_t l_1997 = 1L;
                int32_t l_1999 = 0x01896F7CL;
                int32_t l_2002 = (-1L);
                int32_t l_2005 = 0xB53E1103L;
                int32_t l_2006[1];
                uint16_t l_2007 = 65535UL;
                int i;
                for (i = 0; i < 1; i++)
                    l_2006[i] = 0x1A3BE546L;
                (*g_786) = l_1954;
                for (l_1898 = 0; (l_1898 < 27); l_1898 = safe_add_func_int16_t_s_s(l_1898, 3))
                { /* block id: 927 */
                    float l_1972 = 0x3.D4F2C7p+78;
                    int32_t *l_1982 = &g_584;
                    int32_t *l_1983 = &l_1981;
                    int32_t *l_1984 = &l_1923;
                    int32_t *l_1986 = &l_1418[2];
                    int32_t *l_1987 = &g_48[0];
                    int32_t *l_1988 = (void*)0;
                    int32_t *l_1989 = &l_1923;
                    int32_t *l_1990 = &g_968[4];
                    int32_t l_1991 = 0xCC359352L;
                    int32_t *l_1992[9];
                    int32_t l_2001 = 0x109AB76BL;
                    int i;
                    for (i = 0; i < 9; i++)
                        l_1992[i] = &g_48[0];
                    l_1951 = ((*g_786) = (**l_1743));
                    if ((**g_786))
                    { /* block id: 930 */
                        (**g_786) = (safe_add_func_uint32_t_u_u(((l_1954 != l_1954) && ((safe_div_func_uint64_t_u_u(p_20, (l_1963 & ((*l_1954) <= (safe_lshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((safe_mod_func_uint16_t_u_u((p_20 & ((safe_add_func_uint64_t_u_u((0x0FBEL & 1L), (((p_20 , (*g_583)) & l_1889[2]) > p_20))) | 0x895128C4879311ACLL)), 0xE63CL)), 1L)), p_20)))))) == 0x34B179E6L)), p_20));
                    }
                    else
                    { /* block id: 932 */
                        float *l_1975 = &g_967;
                        (*l_1975) = ((void*)0 != l_1974[0][1][1]);
                    }
                    for (l_1923 = (-14); (l_1923 != 22); ++l_1923)
                    { /* block id: 937 */
                        int32_t *l_1978 = (void*)0;
                        l_1979 = l_1978;
                    }
                    l_2007++;
                }
                if (g_982)
                    goto lbl_2010;
                (*g_786) = &l_2006[0];
            }
        }
        if (((safe_rshift_func_int16_t_s_u((**g_158), ((**g_1795) = ((g_2013 , (*l_1365)) < (l_2014[4][0] > ((*l_2037) = (safe_mul_func_int8_t_s_s((safe_mod_func_int8_t_s_s(((*l_2034) = ((g_245 &= ((safe_lshift_func_int8_t_s_u((safe_add_func_uint16_t_u_u(((*g_1796) < 0x5604L), ((safe_add_func_uint32_t_u_u((((*l_1365) > (safe_div_func_uint8_t_u_u(0x99L, (((*g_938) = (((*g_1761) = (safe_div_func_int64_t_s_s(((***g_1328) ^= ((safe_mod_func_int16_t_s_s((l_2031 == &l_1996), (**g_1795))) <= 1UL)), l_2032[1]))) , p_20)) & p_20)))) < g_491), (-3L))) , p_20))), 7)) , 1UL)) != l_2033)), (*l_2031))), 0x78L)))))))) > p_20))
        { /* block id: 953 */
            uint32_t l_2056 = 0xC6E98CE4L;
            uint16_t *l_2058[1];
            uint16_t **l_2057 = &l_2058[0];
            int32_t *l_2062 = &l_1418[2];
            int32_t *l_2063 = &g_48[0];
            int32_t *l_2064 = (void*)0;
            int32_t *l_2065 = &g_968[4];
            int32_t *l_2066 = &g_968[4];
            int32_t *l_2067 = &g_584;
            int32_t *l_2068 = &l_1994;
            int32_t *l_2069 = (void*)0;
            int32_t *l_2070 = &g_10;
            int32_t *l_2071 = &g_753;
            int32_t *l_2072 = &g_968[4];
            int32_t *l_2073[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            float l_2077 = 0x1.8p+1;
            int32_t l_2085[4][5] = {{2L,(-3L),(-3L),2L,0x193B0CA0L},{2L,(-3L),(-3L),2L,0x193B0CA0L},{2L,(-3L),(-3L),2L,0x193B0CA0L},{2L,(-3L),(-3L),2L,0x193B0CA0L}};
            int i, j;
            for (i = 0; i < 1; i++)
                l_2058[i] = &g_982;
            (*l_2031) = (((*g_213) &= (safe_div_func_uint32_t_u_u(((safe_mul_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(p_20, 0)), p_20)) < ((**g_1795) = (safe_add_func_int32_t_s_s(((((*g_158) = (*g_158)) == (((((safe_lshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_u((p_20 , ((safe_lshift_func_int16_t_s_s(p_20, 14)) | ((safe_lshift_func_uint16_t_u_s((&g_1912 != (((safe_add_func_int8_t_s_s(l_2056, ((void*)0 != l_2057))) <= 0x6B84L) , l_2059)), 10)) ^ (*g_1082)))), g_2061)), p_20)) < (*g_1152)) ^ 253UL) | 65533UL) , l_2058[0])) , 1L), (*l_1365))))), 0x202BEAA5L))) , (*g_47));
            if ((*g_18))
                break;
            l_2086++;
            (*l_2070) ^= 0x963C8DBAL;
        }
        else
        { /* block id: 961 */
            int16_t l_2089 = 0xDDF5L;
            int32_t *l_2090 = &l_1994;
            int32_t *l_2091 = &l_2082;
            int32_t *l_2092 = &l_2080;
            int32_t *l_2093 = &l_2033;
            int32_t *l_2094 = &l_2075;
            int32_t *l_2095 = (void*)0;
            int32_t l_2096 = (-8L);
            int32_t *l_2097 = &g_48[0];
            int32_t *l_2098[8][6] = {{&l_2003,&g_10,&l_2003,(void*)0,&l_2003,&g_10},{&l_2078,&g_10,&l_2004,&g_10,&l_2078,&g_10},{&l_2003,(void*)0,&l_2003,&g_10,&l_2003,(void*)0},{&l_2078,(void*)0,&l_2004,(void*)0,&l_2078,(void*)0},{&l_2003,&g_10,&l_2003,(void*)0,&l_2003,&g_10},{&l_2078,&g_10,&l_2004,&g_10,&l_2078,&g_10},{&l_2003,(void*)0,&l_2003,&g_10,&l_2003,(void*)0},{&l_2078,(void*)0,&l_2004,(void*)0,&l_2078,(void*)0}};
            float l_2099 = 0xE.D504A9p-92;
            int i, j;
            l_2100--;
            (*l_2094) &= (((safe_div_func_int64_t_s_s(((***g_1328) = (safe_lshift_func_uint8_t_u_s(((*l_1365) != ((safe_lshift_func_int8_t_s_u(((safe_sub_func_int16_t_s_s((safe_add_func_int8_t_s_s((p_20 || (((*g_159) |= (*l_2097)) , ((*g_1796) < (safe_mul_func_uint8_t_u_u(((*g_938) |= (safe_div_func_int16_t_s_s((*l_2031), p_20))), p_20))))), (safe_div_func_int32_t_s_s((p_20 < ((safe_rshift_func_int8_t_s_u((9L && p_20), 5)) , p_20)), p_20)))), (*g_1796))) ^ (*l_1365)), 5)) == p_20)), 5))), (*l_2097))) <= p_20) , 0L);
        }
        for (l_2003 = 0; (l_2003 != (-15)); l_2003 = safe_sub_func_uint32_t_u_u(l_2003, 5))
        { /* block id: 970 */
            uint32_t l_2133 = 1UL;
            float *l_2145 = (void*)0;
            float *l_2146[5];
            int32_t l_2147 = (-1L);
            int32_t *l_2148 = &l_2075;
            const struct S0 ****l_2157 = &l_2156;
            uint16_t *l_2159 = &g_702;
            uint16_t **l_2158 = &l_2159;
            uint32_t *l_2165[10] = {&l_2014[4][0],&l_1898,&g_1244[5],&g_1244[5],&l_1898,&l_2014[4][0],&l_1898,&g_1244[5],&g_1244[5],&l_1898};
            uint32_t **l_2288 = (void*)0;
            uint32_t ***l_2287 = &l_2288;
            const uint32_t * const * const **l_2290 = &g_2289;
            int32_t *l_2307 = &l_2078;
            int i;
            for (i = 0; i < 5; i++)
                l_2146[i] = &l_1685;
            if (((safe_add_func_int8_t_s_s((((safe_add_func_int32_t_s_s((((*g_1761) = (255UL >= ((safe_add_func_int8_t_s_s((((+(safe_sub_func_int32_t_s_s((((*g_213) = p_20) , (*l_2031)), (!p_20)))) , l_2133) | ((*l_1365) && p_20)), (safe_lshift_func_uint8_t_u_u((*l_2031), 4)))) , ((safe_lshift_func_int16_t_s_s((((safe_mul_func_float_f_f(((*g_1520) = (l_2147 = (safe_add_func_float_f_f((g_2144 , p_20), (*l_2031))))), 0x7.BFD698p+99)) , (-1L)) | 0UL), (*l_1365))) == 0xFFDDE1EBL)))) == 0xE2L), 4294967290UL)) >= 1UL) > l_2133), (*g_938))) , p_20))
            { /* block id: 975 */
                return l_2149;
            }
            else
            { /* block id: 977 */
                for (g_1374 = 0; (g_1374 >= 10); g_1374++)
                { /* block id: 980 */
                    (*g_786) = &l_2076;
                    if ((((*l_2037) = (****g_1327)) != (((*g_158) = &l_1827) != (void*)0)))
                    { /* block id: 984 */
                        return l_2152;
                    }
                    else
                    { /* block id: 986 */
                        int32_t *l_2153 = (void*)0;
                        return l_2153;
                    }
                }
            }
            (*g_309) = (safe_add_func_float_f_f(((*l_2149) = (((((*l_2157) = l_2156) != ((((void*)0 != l_2158) == (*g_18)) , l_2160)) == (g_2162 , p_20)) != ((*l_2148) = ((((*l_2148) | (((g_981[0][7] = (safe_sub_func_uint32_t_u_u((*l_2148), 4294967295UL))) <= (-8L)) , 0UL)) , (void*)0) != &g_1913)))), g_48[0]));
            if (p_20)
            { /* block id: 996 */
                int64_t l_2176 = 0L;
                for (l_2076 = 0; (l_2076 < (-5)); --l_2076)
                { /* block id: 999 */
                    int8_t l_2171 = 1L;
                    for (g_1537.f2 = 3; (g_1537.f2 <= 9); g_1537.f2 += 1)
                    { /* block id: 1002 */
                        (*g_18) |= (safe_mul_func_int8_t_s_s((((*g_213) = ((((!((l_2171 ^= ((*l_2148) && (p_20 || (-9L)))) > p_20)) , p_20) <= (0xDBL | ((l_2176 &= ((safe_div_func_uint32_t_u_u((0x897A8EB52A98A859LL > (safe_div_func_uint16_t_u_u(9UL, p_20))), p_20)) , (*g_1796))) | (*g_159)))) , (*g_213))) && p_20), (*l_1365)));
                    }
                    for (l_1996 = 0; (l_1996 >= 26); l_1996 = safe_add_func_int32_t_s_s(l_1996, 5))
                    { /* block id: 1010 */
                        int32_t *l_2179 = &l_2075;
                        (*l_1365) = 0L;
                        (*g_786) = l_2179;
                        (*g_309) = (safe_sub_func_float_f_f(p_20, p_20));
                    }
                    if (l_2176)
                        continue;
                }
            }
            else
            { /* block id: 1017 */
                int8_t l_2189 = 0x7BL;
                uint8_t *l_2194 = &l_1889[5];
                int32_t **l_2200[10] = {(void*)0,&g_18,(void*)0,&g_18,(void*)0,&g_18,(void*)0,&g_18,(void*)0,&g_18};
                int16_t *l_2248 = &g_1822;
                int64_t l_2267 = 0x084A6BD48DA0EF54LL;
                int i;
                if ((&l_1923 != (void*)0))
                { /* block id: 1018 */
                    const uint8_t *l_2193 = &g_592;
                    int32_t l_2208 = (-1L);
                    int32_t l_2215 = 0x41FBD045L;
                    float *l_2228 = &g_882;
                    int32_t l_2246 = 0xAAA4329CL;
                    for (l_2082 = 5; (l_2082 <= 16); l_2082 = safe_add_func_int8_t_s_s(l_2082, 2))
                    { /* block id: 1021 */
                        (**l_1743) = &l_2147;
                        (*g_583) &= (l_2184 == (void*)0);
                        return (*g_17);
                    }
                    if (((safe_mod_func_int32_t_s_s((((safe_mul_func_int8_t_s_s(((0x5FDAE82F0075E318LL < (((*l_2149) |= (l_2189 <= 0x46248A8114095503LL)) | (safe_add_func_int64_t_s_s((((p_20 , p_20) , ((*l_2031) |= (safe_unary_minus_func_uint64_t_u(0x423759B0ED3B2B83LL)))) != ((p_20 , l_2193) != l_2194)), p_20)))) >= (**g_1795)), (*l_1365))) , (*l_2148)) && 0xFEB88003AC2DEDFFLL), p_20)) , 0x50355926L))
                    { /* block id: 1028 */
                        uint64_t l_2203 = 0x982BA3F2A4696CBELL;
                        (*l_2152) &= (*g_18);
                        (*l_2149) |= (((safe_sub_func_int8_t_s_s(p_20, p_20)) | (1L > (-1L))) || (safe_sub_func_int8_t_s_s((+(((*l_1743) = (*g_785)) != l_2200[6])), (p_20 , (l_2203 ^= ((0UL & (((safe_div_func_float_f_f((p_20 == g_197.f2), 0x5.936678p-72)) , p_20) == (*g_938))) != 249UL))))));
                        (*l_2031) = p_20;
                    }
                    else
                    { /* block id: 1034 */
                        float **l_2226[5];
                        uint32_t *l_2229 = &g_939.f2;
                        int32_t l_2231 = 0xDDD89089L;
                        uint64_t *** const l_2232[10] = {&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3],&g_1145[3][3][3]};
                        uint32_t **l_2237 = &l_2165[7];
                        int16_t *l_2249 = (void*)0;
                        uint32_t l_2266 = 1UL;
                        int i;
                        for (i = 0; i < 5; i++)
                            l_2226[i] = &l_2145;
                        (*l_2152) &= ((safe_div_func_uint64_t_u_u(((safe_div_func_uint16_t_u_u((l_2208 = (*l_1365)), (l_2231 ^= (safe_mod_func_uint16_t_u_u(((((*g_1796) |= ((safe_lshift_func_uint16_t_u_s(l_2215, 10)) < (((safe_div_func_int16_t_s_s((p_20 ^ ((((*g_213) = ((((*l_2031) > (safe_add_func_float_f_f((safe_add_func_float_f_f(0x0.Fp+1, p_20)), p_20))) <= (*g_433)) , ((safe_lshift_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(((g_2227 = l_2148) == l_2228), 0L)), 2)) || p_20))) , p_20) | p_20)), l_2215)) , l_2229) == &g_210))) == p_20) && p_20), l_2230))))) , (*g_213)), p_20)) && l_2231);
                        (*l_2031) ^= (l_2232[3] != (p_20 , ((((safe_sub_func_float_f_f((safe_add_func_float_f_f(((((*l_2237) = &l_1898) == &l_2133) == ((*g_2227) = ((*l_2152) = (safe_mul_func_float_f_f(0x7.71430Bp+15, (safe_div_func_float_f_f(l_2208, (p_20 == (safe_div_func_float_f_f(((safe_mul_func_float_f_f(0xD.21670Fp-32, (l_2246 >= p_20))) >= 0xD.51A248p-34), (-0x1.9p+1))))))))))), (*g_261))), g_1244[7])) , p_20) , p_20) , l_2247)));
                        (*l_1365) = ((((((**g_158) , (p_20 <= ((l_2248 = (*g_158)) != l_2249))) == ((safe_add_func_float_f_f((((*l_2148) = ((*g_261) > (safe_mul_func_float_f_f((((--(*l_2159)) > ((safe_div_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((((safe_mul_func_int16_t_s_s(((p_20 & ((*l_2149) = ((*g_18) && l_2231))) == (((safe_rshift_func_uint16_t_u_u((((+(*l_2148)) , &g_2227) != g_2265), l_2208)) , l_2266) != l_2267)), (**g_158))) ^ (*g_213)) <= 0x67L), 2)), (*g_1152))) <= p_20)) , (-0x10.7p+1)), p_20)))) <= p_20), p_20)) != l_2231)) >= (*l_1365)) > p_20) >= g_2268[1][2]);
                        (*l_2148) = ((((l_2266 != 0xDF6CL) && ((((((safe_rshift_func_int16_t_s_u(l_2208, 3)) <= ((**l_2237) = ((safe_unary_minus_func_int8_t_s((!(l_2231 > (*l_2031))))) != ((*l_2248) = (safe_mul_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((*g_1796), l_2208)), (p_20 ^ (safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((l_2215 , p_20), 0x6763L)), l_2231)), (*l_2148)))))))))) | l_2266) > 0xADE4L) , (*g_159)) , (*l_2148))) >= l_2208) || 0x428DE23AF371E117LL);
                    }
                }
                else
                { /* block id: 1054 */
                    (*l_2149) ^= p_20;
                    for (l_2076 = 0; (l_2076 != 3); l_2076 = safe_add_func_uint64_t_u_u(l_2076, 1))
                    { /* block id: 1058 */
                        (**l_1743) = &l_2147;
                    }
                    for (l_2074 = 0; (l_2074 <= 9); l_2074 += 1)
                    { /* block id: 1063 */
                        int32_t *l_2286[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2286[i] = &l_2078;
                        (**l_1743) = (void*)0;
                        if (p_20)
                            break;
                        return (**g_785);
                    }
                    (*l_2031) ^= p_20;
                }
                if (p_20)
                    break;
            }
            if ((((l_2287 == ((*l_2290) = g_2289)) == (*l_1365)) , p_20))
            { /* block id: 1073 */
                uint32_t ****l_2298[6] = {&l_2287,&l_2287,&l_2287,&l_2287,&l_2287,&l_2287};
                int32_t l_2301[8] = {0xC2C529B9L,1L,1L,0xC2C529B9L,1L,1L,0xC2C529B9L,1L};
                struct S0 ***l_2304 = &g_1233;
                int i;
                (*l_1365) &= (((*l_2152) = 0x577BB7C4L) != ((((safe_mul_func_int16_t_s_s(p_20, ((safe_mul_func_uint16_t_u_u((+0xF3L), (l_2301[2] = ((**g_158) = ((g_2299 = (g_2296 = g_2296)) != l_2300))))) & (+l_2303)))) , p_20) < ((&g_736 != ((*l_2304) = &l_1974[0][2][2])) && 0x41306244L)) ^ 1UL));
            }
            else
            { /* block id: 1081 */
                int32_t *l_2306 = &l_1996;
                (**l_1743) = &l_2003;
                for (g_753 = 6; (g_753 >= 0); g_753 -= 1)
                { /* block id: 1085 */
                    int32_t *l_2305 = &l_2082;
                    int32_t *l_2309[9][2][6] = {{{&l_2084[2],&l_2075,&l_1923,(void*)0,(void*)0,&l_1923},{&l_1996,&l_1996,&l_2083,&l_2078,&g_48[0],&g_968[4]}},{{&l_2083,(void*)0,&l_2000,&l_2075,&l_1996,&l_2083},{&g_48[0],&l_2083,&l_2000,&l_2084[2],&l_1996,&g_968[4]}},{{(void*)0,&l_2084[2],&l_2083,&l_1994,&g_48[0],&l_1923},{&l_1994,&g_48[0],&l_1923,&l_1996,&l_1418[2],&g_10}},{{&l_2000,&g_48[0],&g_968[4],&g_48[0],&g_48[0],&g_753},{&g_48[0],&l_2084[2],&g_968[4],(void*)0,&l_1996,&l_1418[6]}},{{&g_48[0],&l_2083,&l_2084[1],&g_48[0],&l_1996,&l_2147},{&g_48[0],(void*)0,(void*)0,(void*)0,&g_48[0],&l_2000}},{{&g_48[0],&l_1996,&g_10,&g_48[0],(void*)0,&l_2083},{&l_2000,&l_2075,&l_2147,&l_1996,(void*)0,&l_2083}},{{&l_1994,&l_1996,&g_10,&l_1994,&l_2084[0],&l_2000},{(void*)0,&l_1996,(void*)0,&l_2084[2],(void*)0,&l_2147}},{{&g_48[0],&l_2000,&l_2084[1],&l_2075,(void*)0,&l_1418[6]},{&l_2083,&l_1996,&g_968[4],&l_2078,&l_2084[0],&g_753}},{{&l_1996,&l_1996,&g_968[4],(void*)0,(void*)0,&g_10},{&l_2084[2],&l_2075,&l_1923,(void*)0,(void*)0,&l_1923}}};
                    int i, j, k;
                    for (l_2082 = 0; (l_2082 <= 6); l_2082 += 1)
                    { /* block id: 1088 */
                        int32_t *l_2308[9][5][5] = {{{&l_2082,&l_2003,(void*)0,&l_1418[6],&g_584},{&l_2082,&l_2080,&l_2003,&l_2147,(void*)0},{(void*)0,&g_10,&g_10,(void*)0,&l_2003},{&l_2080,&g_584,&g_10,&l_2147,&l_2000},{&l_1418[2],&l_2075,(void*)0,&g_48[0],&l_2082}},{{(void*)0,&l_2000,&l_2147,&l_2147,&l_1418[2]},{&l_2147,(void*)0,&l_1994,(void*)0,&l_2147},{(void*)0,&g_48[0],(void*)0,&l_2147,&l_1418[2]},{&l_1994,&l_2147,&l_2081,&l_1418[6],(void*)0},{&g_584,&l_2147,(void*)0,&l_2076,(void*)0}},{{&l_2082,&g_48[0],&l_1418[2],&g_48[0],&l_2082},{(void*)0,(void*)0,(void*)0,&l_2080,&g_10},{(void*)0,&l_2000,&l_2147,&g_584,&l_2084[2]},{&g_10,&l_2075,&l_2147,(void*)0,&g_10},{&l_2081,&g_584,&g_48[0],&l_2075,&l_2082}},{{&g_10,&g_10,&l_2147,&l_2147,(void*)0},{(void*)0,&l_2080,(void*)0,(void*)0,(void*)0},{&l_2004,&l_2003,(void*)0,&l_2000,&l_1418[2]},{(void*)0,&l_2147,&l_2147,&l_2147,&l_2147},{&l_1418[6],(void*)0,&g_48[0],&l_2081,&l_1418[2]}},{{&l_2075,&l_2082,&l_2147,&l_2082,&l_2082},{(void*)0,&l_1923,&l_2147,&l_2082,&l_2000},{&l_2075,(void*)0,(void*)0,(void*)0,&l_2003},{&l_1418[6],&l_2147,&l_1418[2],&g_584,(void*)0},{(void*)0,&l_2004,(void*)0,&l_1418[2],&g_584}},{{&l_2004,(void*)0,&l_2081,&l_1418[2],(void*)0},{(void*)0,(void*)0,(void*)0,&g_584,(void*)0},{&g_10,&g_10,&l_1994,(void*)0,&l_1923},{&l_2081,(void*)0,&l_2147,&l_2082,&l_1418[6]},{&g_10,(void*)0,(void*)0,&l_2082,&l_2080}},{{(void*)0,(void*)0,&g_10,&l_2081,&l_2082},{(void*)0,&g_10,&g_10,&l_2147,&l_2147},{&l_2082,(void*)0,&l_2003,&l_2000,(void*)0},{&g_584,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1994,&l_2004,&l_2080,&l_2147,&l_2147}},{{(void*)0,&l_2147,(void*)0,&l_2075,&l_2082},{&l_2147,(void*)0,&g_48[0],(void*)0,&l_2080},{(void*)0,&l_1923,(void*)0,&g_584,&l_1418[6]},{&l_1418[2],(void*)0,&l_2082,&l_2075,&g_48[0]},{&l_2075,&l_1418[2],&l_2075,&l_2000,&g_10}},{{&l_1418[2],(void*)0,&l_2075,&g_10,(void*)0},{(void*)0,&l_2147,&l_2076,&l_1923,(void*)0},{(void*)0,&l_2075,&l_2147,(void*)0,&l_1418[2]},{&l_1418[2],(void*)0,(void*)0,&l_1418[2],&l_2147},{&l_2075,&g_584,&l_1994,(void*)0,(void*)0}}};
                        int i, j, k;
                        (*g_786) = l_2305;
                        return (*g_17);
                    }
                }
            }
        }
    }
    for (g_815.f2 = (-12); (g_815.f2 >= 11); g_815.f2 = safe_add_func_int16_t_s_s(g_815.f2, 4))
    { /* block id: 1098 */
        int64_t l_2336 = 1L;
        int32_t l_2344[9];
        int16_t *l_2365 = &g_1822;
        int32_t l_2370[1];
        int32_t l_2372 = 0x1B434191L;
        float l_2415 = 0x3.140AF3p-36;
        uint8_t l_2416[5];
        const uint64_t *l_2447 = (void*)0;
        uint64_t *l_2463 = &g_308[5];
        int i;
        for (i = 0; i < 9; i++)
            l_2344[i] = (-1L);
        for (i = 0; i < 1; i++)
            l_2370[i] = 0L;
        for (i = 0; i < 5; i++)
            l_2416[i] = 253UL;
    }
    for (g_2162.f2 = (-7); (g_2162.f2 <= 11); g_2162.f2 = safe_add_func_int8_t_s_s(g_2162.f2, 6))
    { /* block id: 1171 */
        int16_t * const **l_2481[10][6] = {{&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,(void*)0},{&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387},{&g_1387,(void*)0,&g_1387,&g_1387,&g_1387,&g_1387},{&g_1387,&g_1387,(void*)0,(void*)0,&g_1387,&g_1387},{&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387},{&g_1387,&g_1387,&g_1387,&g_1387,(void*)0,&g_1387},{&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,(void*)0},{&g_1387,&g_1387,&g_1387,(void*)0,&g_1387,&g_1387},{&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387},{&g_1387,&g_1387,&g_1387,&g_1387,&g_1387,&g_1387}};
        int16_t * const ***l_2480 = &l_2481[7][4];
        int32_t l_2496 = 0x1D2B3CBEL;
        int32_t l_2500 = 0xA9D1D7C4L;
        int32_t l_2501 = 0x3523A43FL;
        const uint64_t l_2516 = 0x23C1EDEDEB66DA5DLL;
        int64_t l_2532 = 0xAC019A0691BFD640LL;
        uint16_t l_2552 = 1UL;
        int32_t l_2584 = 0L;
        int32_t l_2589 = 0x865D62C6L;
        int32_t l_2590 = (-8L);
        int32_t l_2591 = 7L;
        int32_t l_2592 = 0xEE0BA4ECL;
        int32_t l_2594 = 0x29CAD52DL;
        int32_t l_2595 = 0x39D82F6DL;
        int32_t **l_2602 = &g_1394;
        const int16_t *l_2607 = (void*)0;
        const int16_t **l_2606[6][7][6] = {{{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{(void*)0,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{(void*)0,&l_2607,&l_2607,&l_2607,&l_2607,(void*)0},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,(void*)0},{&l_2607,(void*)0,&l_2607,&l_2607,(void*)0,&l_2607},{&l_2607,(void*)0,(void*)0,&l_2607,&l_2607,&l_2607},{(void*)0,&l_2607,&l_2607,&l_2607,(void*)0,&l_2607}},{{(void*)0,(void*)0,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,(void*)0,&l_2607,(void*)0,&l_2607},{&l_2607,(void*)0,&l_2607,&l_2607,&l_2607,&l_2607},{(void*)0,(void*)0,&l_2607,&l_2607,&l_2607,&l_2607},{(void*)0,(void*)0,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,(void*)0,(void*)0,(void*)0,(void*)0,&l_2607}},{{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,(void*)0,&l_2607,(void*)0},{&l_2607,&l_2607,&l_2607,&l_2607,(void*)0,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,(void*)0,&l_2607},{(void*)0,&l_2607,(void*)0,&l_2607,&l_2607,&l_2607}},{{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,(void*)0,&l_2607,&l_2607,(void*)0,&l_2607},{(void*)0,(void*)0,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,(void*)0,(void*)0,&l_2607,&l_2607,(void*)0},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607}},{{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,(void*)0},{&l_2607,(void*)0,(void*)0,&l_2607,&l_2607,&l_2607},{(void*)0,(void*)0,&l_2607,(void*)0,(void*)0,(void*)0},{&l_2607,(void*)0,&l_2607,&l_2607,&l_2607,(void*)0},{(void*)0,&l_2607,(void*)0,(void*)0,&l_2607,(void*)0},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,(void*)0},{&l_2607,&l_2607,&l_2607,&l_2607,(void*)0,&l_2607}},{{(void*)0,&l_2607,(void*)0,(void*)0,&l_2607,(void*)0},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,(void*)0,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,&l_2607,(void*)0},{&l_2607,&l_2607,(void*)0,(void*)0,&l_2607,&l_2607},{(void*)0,&l_2607,&l_2607,&l_2607,&l_2607,&l_2607},{&l_2607,&l_2607,&l_2607,&l_2607,(void*)0,&l_2607}}};
        uint8_t ***l_2677 = &l_1590[2];
        uint16_t *l_2678[4][4] = {{&l_2552,&l_2552,&l_2552,&l_2552},{&l_2552,&l_2552,&l_2552,&l_2552},{&l_2552,&l_2552,&l_2552,&l_2552},{&l_2552,&l_2552,&l_2552,&l_2552}};
        int32_t *l_2679 = &g_968[4];
        int32_t *l_2680 = &l_2591;
        int32_t *l_2681 = (void*)0;
        int32_t *l_2682 = (void*)0;
        int32_t *l_2683 = &g_48[0];
        int32_t *l_2684 = &g_10;
        int32_t *l_2685 = (void*)0;
        int32_t *l_2686 = &l_2592;
        int32_t *l_2687 = &l_2592;
        int32_t *l_2688 = &l_1418[3];
        int32_t *l_2689[8][9] = {{(void*)0,&l_2592,&g_753,(void*)0,&l_2592,&l_2592,(void*)0,&g_753,&l_2592},{(void*)0,&l_2501,&l_2590,(void*)0,&l_2590,&l_2501,(void*)0,&l_2501,&l_2590},{(void*)0,&l_2595,&l_2595,&l_2592,&l_2501,&l_2595,&g_753,&l_2501,&l_2501},{&g_10,&g_753,&l_2496,&l_2501,&l_2496,&g_753,&g_10,&g_753,&l_2496},{&l_2592,&l_2501,&l_2595,&g_753,&l_2501,&l_2501,&g_753,&l_2595,&l_2501},{&l_2590,&g_753,&l_2496,&l_2501,&l_2496,&g_753,&l_2590,&g_753,&l_2496},{&l_2592,&l_2595,&l_2595,&l_2592,&l_2501,&l_2595,&g_753,&l_2501,&l_2501},{&g_10,&g_753,&l_2496,&l_2501,&l_2496,&g_753,&g_10,&g_753,&l_2496}};
        uint64_t l_2690 = 18446744073709551609UL;
        int i, j, k;
        for (g_982 = 0; (g_982 == 54); g_982 = safe_add_func_uint8_t_u_u(g_982, 7))
        { /* block id: 1174 */
            uint64_t **l_2474 = (void*)0;
            const int32_t *l_2475 = &g_584;
            uint32_t *l_2497 = &g_972;
            int16_t ***l_2538 = &g_158;
            int32_t l_2582 = 0xE1C28266L;
            int32_t l_2583[10];
            uint16_t *l_2603 = &g_982;
            uint16_t *l_2605 = &l_2552;
            const int16_t **l_2608 = &l_2607;
            uint16_t l_2664 = 0xF6E8L;
            int i;
            for (i = 0; i < 10; i++)
                l_2583[i] = (-1L);
            (*g_18) = ((((*l_1669) = l_2474) == (void*)0) <= (((**g_1081) = 0x7BL) > (*g_938)));
            l_2475 = l_2475;
            for (l_2406 = 2; (l_2406 >= 37); l_2406 = safe_add_func_uint8_t_u_u(l_2406, 3))
            { /* block id: 1181 */
                int16_t ****l_2479 = (void*)0;
                int16_t *****l_2478[8] = {&l_2479,&l_2479,&l_2479,&l_2479,&l_2479,&l_2479,&l_2479,&l_2479};
                int i;
                l_2480 = (void*)0;
                if (p_20)
                    continue;
                (*g_18) |= (safe_mul_func_uint8_t_u_u(((void*)0 == &g_1884), (*g_1082)));
            }
        }
        (*l_2679) &= ((*g_18) = (0x6BL & ((***l_1946) = (safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u(((-7L) & (l_2500 = ((++(*g_213)) && ((safe_sub_func_uint8_t_u_u(l_2592, (p_20 ^ 0x37447B1F80CCA160LL))) ^ p_20)))), (safe_mul_func_int8_t_s_s(((((((*l_1365) = ((**g_1795) = (0x4C190B22L & (((*g_1152) = (p_20 <= ((l_2591 = ((***l_2677) = (((void*)0 == l_2677) < l_2584))) || (**g_1081)))) , 0xC550F166L)))) || p_20) ^ p_20) && l_2496) && p_20), (**g_1081))))), p_20)))));
        (*g_786) = (void*)0;
        l_2690++;
    }
    return (*g_17);
}


/* ------------------------------------------ */
/* 
 * reads : g_981 g_18
 * writes: g_981 g_10
 */
static uint64_t  func_21(int16_t * p_22, int32_t  p_23, int32_t ** p_24)
{ /* block id: 623 */
    uint32_t *l_1349 = &g_981[2][0];
    int32_t l_1356 = 0x1D0CB9CFL;
    uint32_t * const l_1357 = &g_1244[5];
    uint32_t **l_1358 = &l_1349;
    (*g_18) = ((safe_add_func_uint32_t_u_u((--(*l_1349)), 0x9FE9C88CL)) , (safe_lshift_func_int16_t_s_u((safe_mul_func_int16_t_s_s(l_1356, ((p_23 , l_1357) == ((*l_1358) = &g_1217)))), 6)));
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_588.f2 g_1152 g_982 g_158 g_159 g_8 g_213 g_214 g_702 g_1103 g_357 g_358 g_584 g_786 g_47 g_48 g_1002 g_18 g_10 g_939.f2 g_1082 g_165 g_1327 g_1328 g_1151 g_207 g_583
 * writes: g_588.f2 g_207 g_982 g_214 g_1104 g_939.f2 g_583 g_607 g_584 g_1002 g_1004 g_972 g_753 g_159 g_10 g_1337
 */
static int32_t  func_26(int32_t  p_27, uint32_t  p_28, int32_t  p_29, int32_t * p_30, int64_t  p_31)
{ /* block id: 534 */
    uint8_t *l_1271 = &g_592;
    int32_t l_1273[9] = {0x4A6BC744L,0x4A6BC744L,0x4A6BC744L,0x4A6BC744L,0x4A6BC744L,0x4A6BC744L,0x4A6BC744L,0x4A6BC744L,0x4A6BC744L};
    int8_t l_1279 = 0L;
    uint8_t l_1294 = 251UL;
    int16_t *l_1302 = &g_8[0];
    int32_t *l_1344 = &g_48[0];
    uint32_t l_1345 = 0x9FCB96ABL;
    int i;
    for (g_588.f2 = (-10); (g_588.f2 < 10); g_588.f2 = safe_add_func_int32_t_s_s(g_588.f2, 3))
    { /* block id: 537 */
        float l_1253 = 0xC.FCF45Bp-64;
        uint16_t *l_1258 = &g_982;
        int32_t l_1264[5][8] = {{0x7CFF0513L,0x7CFF0513L,0x92C4768FL,0x7CFF0513L,0x7CFF0513L,0x92C4768FL,0x7CFF0513L,0x7CFF0513L},{0xBFC9900EL,0x7CFF0513L,0xBFC9900EL,0xBFC9900EL,0x7CFF0513L,0xBFC9900EL,0xBFC9900EL,0x7CFF0513L},{0x7CFF0513L,0xBFC9900EL,0xBFC9900EL,0x7CFF0513L,0xBFC9900EL,0xBFC9900EL,0x7CFF0513L,0xBFC9900EL},{0x7CFF0513L,0x7CFF0513L,0x92C4768FL,0x7CFF0513L,0x7CFF0513L,0x92C4768FL,0x7CFF0513L,0x7CFF0513L},{0xBFC9900EL,0x7CFF0513L,0xBFC9900EL,0xBFC9900EL,0x7CFF0513L,0xBFC9900EL,0xBFC9900EL,0x7CFF0513L}};
        int32_t *l_1310[5][8] = {{&g_968[7],(void*)0,(void*)0,&g_968[7],&g_753,&g_968[4],&g_968[7],&g_968[4]},{&g_968[7],(void*)0,&l_1264[0][2],(void*)0,&g_968[7],&l_1264[0][2],(void*)0,(void*)0},{&g_968[4],(void*)0,&g_753,&g_753,(void*)0,&g_968[4],(void*)0,(void*)0},{(void*)0,(void*)0,&g_753,(void*)0,&g_753,(void*)0,(void*)0,&g_968[4]},{(void*)0,&g_968[7],&l_1264[0][2],(void*)0,(void*)0,&l_1264[0][2],&g_968[7],(void*)0}};
        int64_t ***l_1326 = &g_1151;
        int i, j;
        if ((p_27 != ((((*g_1103) = (safe_add_func_int32_t_s_s(((((*g_1152) = 0x5EA0C376A259E876LL) , 1UL) == ((safe_mul_func_uint16_t_u_u(p_29, 0x08ABL)) | (safe_add_func_uint64_t_u_u(p_31, ((*g_213) |= ((safe_mod_func_int64_t_s_s((((safe_div_func_uint16_t_u_u(((*l_1258)++), p_28)) , ((+(**g_158)) < (safe_div_func_int32_t_s_s((l_1264[0][2] | p_29), (-3L))))) && l_1264[0][2]), l_1264[2][6])) , p_31)))))), g_702))) , (*g_357)) == (void*)0)))
        { /* block id: 542 */
            uint32_t l_1276 = 0x343CB557L;
            uint32_t l_1283[3];
            int32_t *l_1284 = &l_1264[0][2];
            int32_t l_1285 = 0x59B1CBA5L;
            int32_t *l_1286 = &l_1264[4][7];
            int32_t *l_1287 = &g_48[0];
            int32_t *l_1288 = &g_48[0];
            int32_t *l_1289 = &l_1264[0][0];
            int32_t *l_1290 = &g_968[3];
            int32_t *l_1291 = &g_753;
            int32_t *l_1292 = (void*)0;
            int32_t *l_1293[8][5][6] = {{{(void*)0,&l_1285,&g_968[4],&l_1273[3],&g_48[0],&g_48[0]},{&l_1273[3],&g_48[0],&g_48[0],&l_1273[3],&g_968[4],&l_1285},{(void*)0,&l_1264[3][7],&l_1264[0][2],&l_1285,&g_968[4],(void*)0},{&g_968[4],&g_968[4],&l_1273[3],&g_968[4],&g_968[4],(void*)0},{(void*)0,&l_1264[3][7],&g_48[0],&g_968[4],&g_968[4],&l_1264[0][2]}},{{&l_1264[0][2],&g_48[0],&l_1264[3][7],&l_1264[3][7],&g_48[0],&l_1264[0][2]},{&g_968[4],&l_1285,&g_48[0],&g_968[4],&l_1264[0][2],&g_48[0]},{&l_1273[3],&g_968[4],(void*)0,&g_968[4],(void*)0,&g_968[4]},{&l_1273[3],&g_48[0],&g_968[4],&l_1264[3][7],&l_1273[3],(void*)0},{&l_1285,&g_968[4],&l_1273[3],&g_48[0],&g_48[0],&l_1273[3]}},{{&g_968[4],&g_968[4],(void*)0,&l_1285,&l_1273[3],&g_48[0]},{&g_968[4],&g_48[0],&l_1264[3][7],(void*)0,(void*)0,(void*)0},{&l_1264[3][7],&g_968[4],&l_1264[3][7],(void*)0,&g_968[4],&g_48[0]},{&g_968[4],(void*)0,(void*)0,&l_1264[0][2],&l_1273[3],&l_1273[3]},{&l_1264[0][2],&l_1273[3],&l_1273[3],&l_1264[0][2],(void*)0,(void*)0}},{{&g_968[4],&g_48[0],&g_968[4],(void*)0,&l_1264[3][7],&g_968[4]},{&l_1264[3][7],(void*)0,(void*)0,(void*)0,&l_1264[3][7],&g_48[0]},{&g_968[4],&g_48[0],&l_1273[3],&l_1285,(void*)0,&g_968[4]},{&g_968[4],&l_1273[3],&g_48[0],&g_48[0],&l_1273[3],&g_968[4]},{&l_1285,(void*)0,&l_1273[3],&l_1264[3][7],&g_968[4],&g_48[0]}},{{&l_1273[3],&g_968[4],(void*)0,&g_968[4],(void*)0,&g_968[4]},{&l_1273[3],&g_48[0],&g_968[4],&l_1264[3][7],&l_1273[3],(void*)0},{&l_1285,&g_968[4],&l_1273[3],&g_48[0],&g_48[0],&l_1273[3]},{&g_968[4],&g_968[4],(void*)0,&l_1285,&l_1273[3],&g_48[0]},{&g_968[4],&g_48[0],&l_1264[3][7],(void*)0,(void*)0,(void*)0}},{{&l_1264[3][7],&g_968[4],&l_1264[3][7],(void*)0,&g_968[4],&g_48[0]},{&g_968[4],(void*)0,(void*)0,&l_1264[0][2],&l_1273[3],&l_1273[3]},{&l_1264[0][2],&l_1273[3],&l_1273[3],&l_1264[0][2],(void*)0,(void*)0},{&g_968[4],&g_48[0],&g_968[4],(void*)0,&l_1264[3][7],&g_968[4]},{&l_1264[3][7],(void*)0,(void*)0,(void*)0,&l_1264[3][7],&g_48[0]}},{{&g_968[4],&g_48[0],&l_1273[3],&l_1285,(void*)0,&g_968[4]},{&g_968[4],&l_1273[3],&g_48[0],&g_48[0],&l_1273[3],&g_968[4]},{&l_1285,(void*)0,&l_1273[3],&l_1264[3][7],&g_968[4],&g_48[0]},{&l_1273[3],&g_968[4],(void*)0,&g_968[4],(void*)0,&g_968[4]},{&l_1273[3],&g_48[0],&g_968[4],&l_1264[3][7],&l_1273[3],(void*)0}},{{&l_1285,&g_968[4],&l_1273[3],&g_48[0],&g_48[0],&l_1273[3]},{&g_968[4],&g_968[4],(void*)0,&l_1285,&l_1273[3],&g_48[0]},{&g_968[4],&g_48[0],&l_1264[3][7],(void*)0,(void*)0,(void*)0},{&l_1264[3][7],&g_968[4],&l_1264[3][7],(void*)0,(void*)0,&l_1273[3]},{&l_1264[3][7],&g_48[0],&g_968[4],&g_968[4],&l_1264[0][2],&l_1264[0][2]}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1283[i] = 0x8DEA51D0L;
            for (g_939.f2 = 27; (g_939.f2 < 1); g_939.f2 = safe_sub_func_uint32_t_u_u(g_939.f2, 2))
            { /* block id: 545 */
                uint32_t l_1280 = 0x600B6807L;
                if ((*p_30))
                    break;
                for (g_214 = 0; (g_214 >= 40); g_214 = safe_add_func_uint8_t_u_u(g_214, 9))
                { /* block id: 549 */
                    int32_t **l_1269 = (void*)0;
                    int32_t **l_1270 = &g_607;
                    (*l_1270) = ((*g_786) = (void*)0);
                    if (((*p_30) = ((void*)0 == l_1271)))
                    { /* block id: 553 */
                        int32_t *l_1272 = &g_48[0];
                        int32_t *l_1274 = &l_1273[6];
                        int32_t *l_1275[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1275[i] = &l_1264[0][0];
                        l_1276--;
                        if (l_1279)
                            break;
                        return (*g_47);
                    }
                    else
                    { /* block id: 557 */
                        if ((*p_30))
                            break;
                    }
                    for (g_1002 = 2; (g_1002 >= 0); g_1002 -= 1)
                    { /* block id: 562 */
                        if ((*p_30))
                            break;
                        l_1280--;
                        if ((*p_30))
                            break;
                    }
                }
            }
            if (l_1283[0])
                continue;
            --l_1294;
            for (g_1004 = (-22); (g_1004 > 3); g_1004 = safe_add_func_int64_t_s_s(g_1004, 2))
            { /* block id: 573 */
                for (g_972 = 1; (g_972 <= 4); g_972 += 1)
                { /* block id: 576 */
                    (*g_786) = &p_29;
                }
                for (g_753 = 3; (g_753 >= 0); g_753 -= 1)
                { /* block id: 581 */
                    (*g_786) = &l_1273[5];
                }
            }
        }
        else
        { /* block id: 585 */
            uint16_t l_1299 = 0x77F4L;
            --l_1299;
            return (*p_30);
        }
        if ((l_1279 | (l_1302 == ((*g_158) = l_1302))))
        { /* block id: 590 */
            (*g_18) = (-7L);
        }
        else
        { /* block id: 592 */
            uint16_t **** const *l_1321 = (void*)0;
            uint64_t l_1343[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1343[i] = 0x85987EC6D146E96ALL;
            for (g_10 = 0; (g_10 <= 26); g_10 = safe_add_func_int32_t_s_s(g_10, 4))
            { /* block id: 595 */
                int32_t l_1307 = 1L;
                int32_t l_1325 = 0x91FC4949L;
                int8_t *l_1335 = &l_1279;
                int8_t *l_1336 = &g_1337;
                for (g_939.f2 = 0; (g_939.f2 >= 59); ++g_939.f2)
                { /* block id: 598 */
                    int64_t l_1308 = 3L;
                    int8_t *l_1313 = &l_1279;
                }
                l_1343[0] &= (safe_rshift_func_int8_t_s_s((((safe_mul_func_int16_t_s_s(((p_28 | (((*l_1335) ^= (*g_1082)) <= (4294967288UL > ((1UL & p_27) >= ((((*l_1336) = (-3L)) > (((((l_1325 <= (safe_add_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((safe_unary_minus_func_uint64_t_u(0x5EA41D979E7CBF66LL)), (p_28 != l_1273[3]))), p_29))) , (****g_1327)) > l_1307) , p_29) , l_1307)) , 0xBDE3ECC2ED63358ELL))))) > l_1307), (*g_159))) < p_28) <= 4294967292UL), 3));
            }
        }
    }
    l_1344 = (*g_786);
    return l_1345;
}


/* ------------------------------------------ */
/* 
 * reads : g_17 g_18 g_10 g_8 g_47 g_48 g_786 g_583 g_584
 * writes: g_10 g_17 g_48 g_584
 */
static uint16_t  func_36(const int32_t * p_37, int32_t * const  p_38, uint8_t  p_39)
{ /* block id: 14 */
    uint16_t l_94 = 0xC0BCL;
    int32_t l_590 = (-3L);
    int32_t l_593 = 0x57AEF369L;
    int32_t ** const l_597 = &g_583;
    int16_t ***l_987 = &g_158;
    uint64_t **l_1062 = &g_213;
    uint64_t ** const *l_1061 = &l_1062;
    uint8_t l_1080[7][4] = {{0xB9L,0xACL,0xB9L,0xACL},{0xB9L,0xACL,0xB9L,0xACL},{0xB9L,0xACL,0xB9L,0xACL},{0xB9L,0xACL,0xB9L,0xACL},{0xB9L,0xACL,0xB9L,0xACL},{0xB9L,0xACL,0xB9L,0xACL},{0xB9L,0xACL,0xB9L,0xACL}};
    uint32_t *l_1174 = &g_365.f2;
    int i, j;
    (*g_18) = (**g_17);
    for (p_39 = 0; (p_39 <= 0); p_39 += 1)
    { /* block id: 18 */
        int16_t *l_43[3];
        int16_t * const *l_42 = &l_43[2];
        int16_t * const **l_41 = &l_42;
        int i;
        for (i = 0; i < 3; i++)
            l_43[i] = &g_8[0];
        (*l_41) = (void*)0;
        g_17 = func_44(&g_8[0]);
        (*g_47) ^= ((*p_38) = g_8[p_39]);
    }
    for (p_39 = (-11); (p_39 != 40); p_39++)
    { /* block id: 28 */
        uint32_t l_62[5][8][6] = {{{0xCC562928L,0x8C93045DL,4294967292UL,0UL,0xF03BE979L,0xEBB8FA76L},{0x379706C8L,0x8C93045DL,0x9C904586L,0x7C424A88L,0x7C424A88L,0x9C904586L},{0x5446388DL,0x5446388DL,4294967290UL,0x47E9BEE6L,7UL,4294967295UL},{4294967295UL,0x7C424A88L,8UL,0xCC562928L,0UL,4294967290UL},{0xF03BE979L,4294967295UL,8UL,0x7CA69BD5L,0x5446388DL,4294967295UL},{0x8C93045DL,0x7CA69BD5L,4294967290UL,0x10C122BDL,0UL,0x9C904586L},{0x10C122BDL,0UL,0x9C904586L,0UL,4294967292UL,0xEBB8FA76L},{0x47E9BEE6L,0x794A777CL,4294967292UL,0UL,0x10C122BDL,4294967295UL}},{{0x10C122BDL,0UL,4294967286UL,0x10C122BDL,4294967295UL,0UL},{0x8C93045DL,0x47E9BEE6L,0xEBB8FA76L,0x7CA69BD5L,4UL,8UL},{0xF03BE979L,0UL,0x28406A83L,0xCC562928L,4UL,0UL},{4294967295UL,0x47E9BEE6L,4294967295UL,0x47E9BEE6L,4294967295UL,0x62AD52EBL},{0x5446388DL,0UL,0xAAFA3536L,0x7C424A88L,0x10C122BDL,0UL},{0x379706C8L,0x794A777CL,1UL,0UL,4294967292UL,0UL},{0xCC562928L,0UL,0xAAFA3536L,0x379706C8L,0UL,0x62AD52EBL},{4294967292UL,0x7CA69BD5L,4294967295UL,4UL,0x5446388DL,0UL}},{{0x7CA69BD5L,4294967295UL,0x28406A83L,7UL,0UL,8UL},{0x7CA69BD5L,0x7C424A88L,0xEBB8FA76L,4UL,7UL,0UL},{4294967292UL,0x5446388DL,4294967286UL,0x379706C8L,0x7C424A88L,4294967295UL},{0xCC562928L,0x8C93045DL,4294967292UL,0UL,0xF03BE979L,0xEBB8FA76L},{0x379706C8L,0x8C93045DL,0x9C904586L,0x7C424A88L,0x7C424A88L,0x9C904586L},{0x5446388DL,0x5446388DL,4294967290UL,0x47E9BEE6L,7UL,4294967295UL},{4294967295UL,0x7C424A88L,8UL,0xCC562928L,0UL,4294967290UL},{0xF03BE979L,4294967295UL,8UL,0x7CA69BD5L,0x5446388DL,4294967295UL}},{{0x8C93045DL,0x7CA69BD5L,4294967290UL,0x10C122BDL,0UL,0x9C904586L},{0x10C122BDL,0UL,0x9C904586L,0UL,0xE49B9DBFL,4294967295UL},{0x6CD17E59L,0xC5DFBC4EL,0x379706C8L,4294967295UL,3UL,0x8C93045DL},{3UL,4294967295UL,0x5446388DL,3UL,0UL,7UL},{1UL,0x6CD17E59L,4294967295UL,4294967286UL,0x96DE0D78L,0x7CA69BD5L},{0x33472970L,4294967295UL,0xF03BE979L,4294967293UL,0x96DE0D78L,0UL},{0UL,0x6CD17E59L,0x8C93045DL,0x6CD17E59L,0UL,4UL},{0x30343879L,4294967295UL,0x10C122BDL,0UL,3UL,0xBCF54F23L}},{{0x066B3233L,0xC5DFBC4EL,0x47E9BEE6L,4294967295UL,0xE49B9DBFL,0xBCF54F23L},{4294967293UL,0xF59CAFAAL,0x10C122BDL,0x066B3233L,0xF59CAFAAL,4UL},{0xE49B9DBFL,4294967286UL,0x8C93045DL,0x96DE0D78L,0x30343879L,0UL},{4294967286UL,0UL,0xF03BE979L,0xF5FBB4CDL,4294967295UL,0x7CA69BD5L},{4294967286UL,0UL,4294967295UL,0x96DE0D78L,0xF5FBB4CDL,7UL},{0xE49B9DBFL,0x30343879L,0x5446388DL,0x066B3233L,0UL,0x8C93045DL},{4294967293UL,1UL,0x379706C8L,4294967295UL,0x33472970L,4294967295UL},{0x066B3233L,1UL,0xCC562928L,0UL,0UL,0xCC562928L}}};
        int32_t **l_93 = &g_18;
        int64_t l_95[7][5] = {{(-1L),0x825B7CC70ECFAE2FLL,0xB9CBC2055579FD63LL,0x825B7CC70ECFAE2FLL,(-1L)},{0xBFC0376B33B0C632LL,0L,0xBFC0376B33B0C632LL,0xBFC0376B33B0C632LL,0L},{(-1L),0xEEE36A709CE989DCLL,0xCFEE4E9FC46A622BLL,0x825B7CC70ECFAE2FLL,0xCFEE4E9FC46A622BLL},{0L,0L,0x8D3F9E7EE7FC0861LL,0L,0L},{0xCFEE4E9FC46A622BLL,0x825B7CC70ECFAE2FLL,0xCFEE4E9FC46A622BLL,0xEEE36A709CE989DCLL,(-1L)},{0L,0xBFC0376B33B0C632LL,0xBFC0376B33B0C632LL,0L,0xBFC0376B33B0C632LL},{(-1L),0x825B7CC70ECFAE2FLL,0xB9CBC2055579FD63LL,0x825B7CC70ECFAE2FLL,(-1L)}};
        int32_t *l_97[3];
        int32_t **l_96 = &l_97[1];
        uint8_t *l_591[5][7] = {{&g_592,&g_592,&g_592,&g_592,&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592,&g_592,&g_592,&g_592,&g_592},{&g_592,&g_592,&g_592,&g_592,&g_592,&g_592,&g_592},{(void*)0,&g_592,(void*)0,(void*)0,&g_592,(void*)0,(void*)0},{&g_592,&g_592,&g_592,&g_592,&g_592,&g_592,&g_592}};
        int16_t * const *l_595 = &g_159;
        int16_t * const **l_594[7];
        int16_t ***l_596[1];
        int8_t l_1041 = (-8L);
        int32_t l_1056 = 1L;
        int64_t l_1123 = 5L;
        int32_t * const **l_1181 = &g_1106;
        int32_t l_1218 = 0x7D771052L;
        uint16_t ****l_1221 = &g_359;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_97[i] = &g_48[0];
        for (i = 0; i < 7; i++)
            l_594[i] = &l_595;
        for (i = 0; i < 1; i++)
            l_596[i] = &g_158;
    }
    (*p_38) &= ((**g_786) &= 0L);
    return (**l_597);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t ** const  func_44(int16_t * p_45)
{ /* block id: 20 */
    return &g_18;
}


/* ------------------------------------------ */
/* 
 * reads : g_158 g_159 g_8 g_365.f3 g_245 g_1002 g_1004 g_583 g_785 g_786 g_584 g_1022 g_938 g_348 g_589.f2 g_890
 * writes: g_207 g_165 g_1004 g_584 g_791 g_1022 g_592
 */
static int32_t  func_51(int8_t  p_52, int16_t *** p_53, int16_t *** p_54)
{ /* block id: 401 */
    uint32_t *l_994[3][7][1];
    int32_t l_997[7];
    int64_t *l_998[10][1][5] = {{{&g_207,&g_207,&g_207,(void*)0,&g_207}},{{&g_207,&g_207,&g_207,&g_207,&g_207}},{{&g_207,&g_207,&g_207,&g_207,&g_207}},{{&g_207,&g_207,&g_207,&g_207,(void*)0}},{{&g_207,&g_207,&g_207,&g_207,&g_207}},{{&g_207,&g_207,(void*)0,(void*)0,&g_207}},{{&g_207,&g_207,(void*)0,&g_207,&g_207}},{{&g_207,(void*)0,&g_207,&g_207,&g_207}},{{(void*)0,&g_207,&g_207,&g_207,(void*)0}},{{&g_207,&g_207,(void*)0,(void*)0,(void*)0}}};
    uint8_t **l_1000 = &g_938;
    uint8_t ***l_999 = &l_1000;
    const uint8_t ***l_1001 = (void*)0;
    int8_t *l_1003 = &g_165;
    int32_t *l_1005 = &g_584;
    float *l_1007 = &g_791;
    float l_1023[2][10][1] = {{{0xA.898E23p-37},{0xA.898E23p-37},{0x7.A1205Ap-68},{0x5.7p+1},{0x7.A1205Ap-68},{0xA.898E23p-37},{0xA.898E23p-37},{0x7.A1205Ap-68},{0x5.7p+1},{0x7.A1205Ap-68}},{{0xA.898E23p-37},{0xA.898E23p-37},{0x7.A1205Ap-68},{0x5.7p+1},{0x7.A1205Ap-68},{0xA.898E23p-37},{0xA.898E23p-37},{0x7.A1205Ap-68},{0x5.7p+1},{0x7.A1205Ap-68}}};
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 1; k++)
                l_994[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 7; i++)
        l_997[i] = 0x30E15FDBL;
    (*g_583) = ((safe_mod_func_int16_t_s_s((***p_54), ((safe_sub_func_uint32_t_u_u(g_365.f3, ((l_994[1][0][0] == l_994[2][0][0]) != (g_245 != (g_1004 ^= ((*l_1003) = ((((safe_div_func_uint16_t_u_u((p_52 , ((((((g_207 = l_997[6]) , 0x5.BD564Bp-60) , l_999) == l_1001) == 1L) , p_52)), p_52)) <= g_1002) ^ p_52) <= l_997[6]))))))) , l_997[3]))) && 0UL);
    l_1005 = (**g_785);
    (*l_1007) = (*l_1005);
    (*l_1005) = (((safe_sub_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u((0x4D4E38B2L ^ (((*l_1005) ^ ((((safe_add_func_int32_t_s_s((safe_add_func_uint64_t_u_u(0x7AE28C0DF946CACELL, (safe_sub_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((((**l_1000) = ((&g_159 != &g_159) ^ (safe_add_func_int32_t_s_s((((g_1022 &= (*g_159)) , p_52) ^ ((void*)0 != (*l_999))), (-1L))))) & 3L), (*g_159))) & (*l_1005)), p_52)))), (*l_1005))) < (**g_158)) & g_348) <= 0x71A1L)) < g_589.f2)), p_52)), 0x1571DACA6C548B35LL)) < 0x9EE8B84A309FED07LL) & g_890);
    return (***g_785);
}


/* ------------------------------------------ */
/* 
 * reads : g_365.f2 g_583 g_592 g_47 g_48 g_17 g_18 g_786 g_584
 * writes: g_18 g_607 g_584
 */
static int32_t  func_63(int16_t  p_64, int16_t * const ** p_65, int16_t *** p_66, const float  p_67, int32_t ** const  p_68)
{ /* block id: 260 */
    int64_t l_605[7][8] = {{1L,0x405303FCE295FA68LL,0x405303FCE295FA68LL,1L,0xCF31A1EA8823E39DLL,1L,0x405303FCE295FA68LL,0x405303FCE295FA68LL},{0x405303FCE295FA68LL,0xCF31A1EA8823E39DLL,5L,5L,0xCF31A1EA8823E39DLL,0x405303FCE295FA68LL,0xCF31A1EA8823E39DLL,5L},{1L,0xCF31A1EA8823E39DLL,1L,0x405303FCE295FA68LL,0x405303FCE295FA68LL,1L,0xCF31A1EA8823E39DLL,1L},{0x189A27764EEB6273LL,0x405303FCE295FA68LL,5L,0x405303FCE295FA68LL,0x189A27764EEB6273LL,0x189A27764EEB6273LL,0x405303FCE295FA68LL,5L},{0x189A27764EEB6273LL,0x189A27764EEB6273LL,0x405303FCE295FA68LL,5L,0x405303FCE295FA68LL,0x189A27764EEB6273LL,0x189A27764EEB6273LL,0x405303FCE295FA68LL},{1L,0x405303FCE295FA68LL,0x405303FCE295FA68LL,1L,0xCF31A1EA8823E39DLL,1L,0x405303FCE295FA68LL,0x405303FCE295FA68LL},{0x405303FCE295FA68LL,0xCF31A1EA8823E39DLL,5L,5L,0xCF31A1EA8823E39DLL,0x405303FCE295FA68LL,0xCF31A1EA8823E39DLL,5L}};
    int32_t **l_606 = &g_18;
    int32_t *l_609 = &g_10;
    int32_t **l_608 = &l_609;
    uint64_t *l_615[3];
    int32_t l_638 = 0x4351BA1AL;
    int32_t l_655 = (-5L);
    int32_t l_657[9] = {(-1L),0x5D2429BDL,(-1L),0x5D2429BDL,(-1L),0x5D2429BDL,(-1L),0x5D2429BDL,(-1L)};
    int16_t l_663 = (-7L);
    uint32_t l_664 = 1UL;
    uint16_t *l_701[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t **l_700 = &l_701[2];
    uint8_t l_721[9] = {1UL,0xB4L,1UL,0xB4L,1UL,0xB4L,1UL,0xB4L,1UL};
    int64_t *l_765[6] = {&l_605[2][5],&l_605[6][6],&l_605[2][5],&l_605[2][5],&l_605[6][6],&l_605[2][5]};
    int64_t **l_764 = &l_765[4];
    uint64_t l_778[10] = {0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL,0x14D795C7A3274913LL};
    struct S0 *l_814 = &g_815;
    struct S0 **l_813[4];
    uint8_t * const *l_935 = (void*)0;
    uint64_t **l_951[6];
    uint64_t l_983 = 0x89695C2033A1D00CLL;
    int i, j;
    for (i = 0; i < 3; i++)
        l_615[i] = (void*)0;
    for (i = 0; i < 4; i++)
        l_813[i] = &l_814;
    for (i = 0; i < 6; i++)
        l_951[i] = (void*)0;
    if ((+((safe_sub_func_int64_t_s_s((g_365.f2 == (p_64 | (((((safe_div_func_int32_t_s_s(((void*)0 == g_583), ((safe_sub_func_int16_t_s_s(l_605[2][5], (((((*l_606) = (void*)0) == (g_607 = (*p_68))) , (void*)0) != ((*l_608) = (*p_68))))) & p_64))) > (-4L)) ^ p_64) > g_592) <= 0xD128CC32L))), p_64)) >= p_64)))
    { /* block id: 264 */
        return (*g_47);
    }
    else
    { /* block id: 266 */
        uint64_t l_618 = 18446744073709551615UL;
        int32_t l_619 = 0x0ED97950L;
        int32_t **l_622 = &l_609;
        int32_t l_643[8] = {0xF463F96CL,0xF463F96CL,0x958A770AL,0xF463F96CL,0xF463F96CL,0x958A770AL,0xF463F96CL,0xF463F96CL};
        uint64_t l_795 = 1UL;
        int16_t l_878 = 0xA739L;
        const uint64_t *l_945 = &g_308[1];
        const uint64_t **l_944 = &l_945;
        uint32_t *l_966 = &g_348;
        uint32_t *l_970 = (void*)0;
        uint32_t *l_971 = &g_972;
        int i;
    }
    (**p_68) ^= ((*g_17) == (*g_786));
    (**p_68) ^= (safe_unary_minus_func_int16_t_s(0xE722L));
    return (*l_609);
}


/* ------------------------------------------ */
/* 
 * reads : g_588 g_589
 * writes: g_18
 */
static struct S0  func_75(int8_t  p_76, int16_t * p_77, float  p_78, uint8_t  p_79, int32_t * p_80)
{ /* block id: 251 */
    int32_t **l_585 = &g_18;
    (*l_585) = (void*)0;
    for (p_79 = 18; (p_79 > 60); p_79 = safe_add_func_uint32_t_u_u(p_79, 9))
    { /* block id: 255 */
        return g_588;
    }
    return g_589;
}


/* ------------------------------------------ */
/* 
 * reads : g_18 g_10 g_365.f2 g_245 g_207 g_213 g_214 g_308 g_491 g_433 g_262 g_48 g_17 g_359 g_365.f0 g_197.f2 g_197 g_158 g_159 g_8
 * writes: g_491 g_308 g_262 g_48 g_245 g_360 g_214 g_10 g_197.f2 g_210 g_165
 */
static uint32_t  func_83(int16_t  p_84, int32_t ** p_85, int32_t  p_86, int32_t  p_87, int64_t  p_88)
{ /* block id: 37 */
    float l_102 = (-0x6.Cp-1);
    int32_t l_129 = 1L;
    int16_t *l_133[8] = {&g_8[0],&g_8[0],&g_8[0],&g_8[0],&g_8[0],&g_8[0],&g_8[0],&g_8[0]};
    int16_t **l_132 = &l_133[5];
    int32_t l_161 = 1L;
    uint32_t l_190[8] = {0xB2804DBBL,0xB2804DBBL,0xB2804DBBL,0xB2804DBBL,0xB2804DBBL,0xB2804DBBL,0xB2804DBBL,0xB2804DBBL};
    int8_t *l_250 = &g_165;
    int8_t *l_253 = &g_165;
    uint32_t *l_366 = &g_348;
    int32_t l_380 = 0L;
    int32_t l_383 = 0L;
    int32_t l_384 = 0L;
    int32_t l_386 = 0L;
    int32_t l_387 = 0xEA18B767L;
    int32_t l_388 = 2L;
    int32_t l_389[5][4][9] = {{{(-1L),0x70DB3380L,0x5807F587L,0x7A17A590L,0x09DA0BEAL,8L,(-1L),(-1L),8L},{0L,(-1L),(-1L),4L,0L,1L,0x64A98009L,0x64A98009L,1L},{0xE818515DL,0xCFAC8844L,0x5807F587L,0xCFAC8844L,0xE818515DL,8L,(-1L),(-1L),8L},{0L,4L,(-1L),(-1L),0L,1L,0x957127D6L,0L,0x957127D6L}},{{0x99489F1CL,0x2063382BL,(-1L),0x4FCAD409L,2L,(-1L),0xAFA56BB3L,0xAFA56BB3L,(-1L)},{(-1L),(-2L),0L,(-2L),(-1L),0x957127D6L,0x4035D067L,0x0765A497L,0x957127D6L},{2L,0x4FCAD409L,(-1L),0x2063382BL,0x99489F1CL,(-1L),0xA5626CBFL,1L,(-1L)},{1L,6L,0L,0x4DE712E1L,1L,0x957127D6L,0x6C51C49BL,0x6C51C49BL,0x957127D6L}},{{(-1L),0L,(-1L),0L,(-1L),(-1L),1L,0xA5626CBFL,(-1L)},{1L,0x4DE712E1L,0L,6L,1L,0x957127D6L,0x0765A497L,0x4035D067L,0x957127D6L},{0x99489F1CL,0x2063382BL,(-1L),0x4FCAD409L,2L,(-1L),0xAFA56BB3L,0xAFA56BB3L,(-1L)},{(-1L),(-2L),0L,(-2L),(-1L),0x957127D6L,0x4035D067L,0x0765A497L,0x957127D6L}},{{2L,0x4FCAD409L,(-1L),0x2063382BL,0x99489F1CL,(-1L),0xA5626CBFL,1L,(-1L)},{1L,6L,0L,0x4DE712E1L,1L,0x957127D6L,0x6C51C49BL,0x6C51C49BL,0x957127D6L},{(-1L),0L,(-1L),0L,(-1L),(-1L),1L,0xA5626CBFL,(-1L)},{1L,0x4DE712E1L,0L,6L,1L,0x957127D6L,0x0765A497L,0x4035D067L,0x957127D6L}},{{0x99489F1CL,0x2063382BL,(-1L),0x4FCAD409L,2L,(-1L),0xAFA56BB3L,0xAFA56BB3L,(-1L)},{(-1L),(-2L),0L,(-2L),(-1L),0x957127D6L,0x4035D067L,0x0765A497L,0x957127D6L},{2L,0x4FCAD409L,(-1L),0x2063382BL,0x99489F1CL,(-1L),0xA5626CBFL,1L,(-1L)},{1L,6L,0L,0x4DE712E1L,1L,0x957127D6L,0x6C51C49BL,0x6C51C49BL,0x957127D6L}}};
    uint8_t l_390 = 0xB8L;
    uint16_t l_429 = 2UL;
    uint8_t l_484 = 0x77L;
    uint64_t **l_510 = &g_213;
    uint64_t l_566 = 0x2AD5169D51CA2F0FLL;
    int i, j, k;
lbl_555:
    for (p_87 = 29; (p_87 > (-19)); p_87--)
    { /* block id: 40 */
        uint64_t l_113[9] = {0UL,0UL,0x9AA0F7998293F6A8LL,0UL,0UL,0x9AA0F7998293F6A8LL,0UL,0UL,0x9AA0F7998293F6A8LL};
        int16_t *l_131 = &g_8[0];
        int16_t **l_130[9][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
        int32_t l_167 = 0x5FC087B6L;
        int32_t l_171 = 6L;
        uint16_t l_173 = 0x7F1AL;
        int32_t l_187 = 0xC25823E8L;
        int64_t *l_285[3];
        int64_t **l_284 = &l_285[1];
        int32_t l_327 = 1L;
        int32_t l_332[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
        uint16_t ***l_361[3][1][7] = {{{&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,(void*)0,(void*)0,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360}}};
        int16_t l_379 = (-1L);
        int8_t l_423 = 0x8AL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_285[i] = &g_207;
        for (p_88 = 0; (p_88 < 15); ++p_88)
        { /* block id: 43 */
            int16_t *l_103 = &g_8[0];
            uint32_t l_128 = 6UL;
            int32_t l_134 = (-1L);
            int16_t **l_160 = &l_103;
            int32_t l_186 = (-6L);
            int32_t l_188 = 0x556C1352L;
            int32_t l_189 = 6L;
            float l_260 = 0x4.Bp-1;
            uint8_t *l_279 = &g_245;
            int32_t l_328 = 0x443C5AEDL;
            int32_t l_334 = 0x51F8BB19L;
            int32_t l_335 = 0xD5261192L;
            int32_t l_336 = 0x9947CA15L;
            int32_t l_337 = 0xE689859AL;
        }
        (*p_85) = &l_171;
    }
    for (l_380 = (-26); (l_380 != 14); l_380 = safe_add_func_int8_t_s_s(l_380, 4))
    { /* block id: 177 */
        uint32_t l_464 = 0UL;
        int32_t *l_465 = &l_386;
        uint64_t l_485 = 0UL;
        int32_t l_488 = (-4L);
        int32_t l_489[1][6][3] = {{{0x9814ACB7L,0x9814ACB7L,(-3L)},{0x1316C65BL,1L,(-3L)},{1L,0x1316C65BL,(-3L)},{0x9814ACB7L,0x9814ACB7L,(-3L)},{0x1316C65BL,1L,(-3L)},{1L,0x1316C65BL,(-3L)}}};
        float l_490 = 0x0.Ep-1;
        uint8_t l_558 = 0x97L;
        int i, j, k;
        if ((((l_389[0][0][8] | 0x9D218CCEL) < (safe_add_func_uint8_t_u_u((((*l_465) = l_464) , (l_129 ^ 0UL)), (safe_mul_func_int16_t_s_s((safe_sub_func_int32_t_s_s((((0x81E3L && (((safe_rshift_func_int16_t_s_u(((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_s((safe_mod_func_int8_t_s_s(((l_464 < (safe_add_func_uint8_t_u_u((((safe_div_func_int16_t_s_s(l_464, (safe_rshift_func_int16_t_s_u(((((*g_18) && 0x0F38FB30L) , 0x02L) , p_86), p_88)))) >= l_380) | (-1L)), p_87))) != l_390), g_365.f2)), l_484)) <= l_387), l_485)) & g_245), 9)) , g_207) >= (*g_213))) && (-5L)) && g_308[1]), l_384)), 65534UL))))) >= p_87))
        { /* block id: 179 */
            int32_t *l_486 = &l_387;
            int32_t *l_487[9][2][2] = {{{&l_388,&g_10},{&g_10,&l_388}},{{&l_161,(void*)0},{&l_161,&l_388}},{{&g_10,&g_10},{&l_388,&l_161}},{{(void*)0,&l_161},{&l_388,&g_10}},{{&g_10,&l_388},{&l_161,(void*)0}},{{&l_161,&l_388},{&g_10,&g_10}},{{&l_388,&l_161},{(void*)0,&l_161}},{{&l_388,&g_10},{&g_10,&l_388}},{{&l_161,(void*)0},{&l_161,&l_388}}};
            float *l_505 = &g_262;
            uint8_t *l_515 = &g_245;
            int64_t l_518[4] = {(-1L),(-1L),(-1L),(-1L)};
            int i, j, k;
            ++g_491;
            g_48[0] ^= (7L >= (safe_unary_minus_func_int32_t_s(((((*g_433) , (safe_sub_func_int16_t_s_s(l_464, (safe_rshift_func_uint16_t_u_u((((*g_213) != ((g_308[2] = p_88) > (safe_rshift_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(0x14L, (g_10 , (((*l_505) = 0x7.Dp+1) , l_380)))), 2)), 13)))) & p_84), p_87))))) == p_87) ^ (*g_213)))));
            for (l_388 = 0; (l_388 <= 9); ++l_388)
            { /* block id: 186 */
                return p_86;
            }
            if (((*l_486) &= (safe_lshift_func_uint16_t_u_s(((&g_213 != l_510) || p_84), (&l_386 == ((safe_sub_func_uint8_t_u_u((((*l_515) |= (safe_lshift_func_int16_t_s_u(((l_161 != (*g_18)) || p_88), 13))) & (safe_lshift_func_int16_t_s_u(l_518[1], 9))), ((p_87 > (-7L)) , l_489[0][2][1]))) , (void*)0))))))
            { /* block id: 191 */
                uint64_t l_523 = 0x6F2690120C8CA76DLL;
                const uint32_t l_530 = 0xA685933AL;
                int64_t *l_532 = &g_207;
                if (((+((*l_515) = g_491)) ^ (-1L)))
                { /* block id: 193 */
                    uint16_t *l_529 = &l_429;
                    uint16_t **l_528 = &l_529;
                    for (l_384 = 0; (l_384 > (-7)); --l_384)
                    { /* block id: 196 */
                        int32_t **l_522 = &l_486;
                        uint16_t *l_527[5][5] = {{&l_429,&l_429,&l_429,&l_429,&l_429},{&l_429,(void*)0,&l_429,(void*)0,&l_429},{&l_429,&l_429,&l_429,&l_429,&l_429},{&l_429,&l_429,&l_429,(void*)0,&l_429},{&l_429,&l_429,&l_429,&l_429,&l_429}};
                        uint16_t **l_526 = &l_527[4][1];
                        int i, j;
                        l_489[0][2][1] &= (((l_522 != (void*)0) || l_523) <= (safe_mod_func_int32_t_s_s((**g_17), (p_86 || (((((*g_359) = l_526) == l_528) != ((((**l_510) = (((*g_213) && (*g_213)) >= p_87)) & p_88) == 0x8B590072L)) , l_530)))));
                        l_489[0][5][0] ^= ((~(**l_522)) && (&g_207 != l_532));
                    }
                    (*p_85) = (*g_17);
                    (*g_18) ^= 0L;
                    if (l_530)
                        continue;
                }
                else
                { /* block id: 205 */
                    for (g_197.f2 = 0; (g_197.f2 != 34); g_197.f2++)
                    { /* block id: 208 */
                        return g_365.f0;
                    }
                    for (g_210 = 0; g_210 < 9; g_210 += 1)
                    {
                        for (l_484 = 0; l_484 < 2; l_484 += 1)
                        {
                            for (g_245 = 0; g_245 < 2; g_245 += 1)
                            {
                                l_487[g_210][l_484][g_245] = &l_388;
                            }
                        }
                    }
                }
                for (l_383 = (-28); (l_383 > (-27)); l_383 = safe_add_func_uint8_t_u_u(l_383, 4))
                { /* block id: 215 */
                    return p_84;
                }
            }
            else
            { /* block id: 218 */
                int32_t l_554 = 1L;
                for (g_197.f2 = 1; (g_197.f2 <= 8); g_197.f2 += 1)
                { /* block id: 221 */
                    int32_t l_546 = (-6L);
                    int i;
                    if ((g_308[g_197.f2] & (safe_rshift_func_int16_t_s_u(g_308[g_197.f2], (safe_sub_func_int8_t_s_s((safe_sub_func_int16_t_s_s(g_308[g_197.f2], l_464)), (safe_add_func_uint16_t_u_u((((!0L) , (l_546 = ((*l_250) = (g_48[0] & g_207)))) > ((((safe_add_func_float_f_f((safe_sub_func_float_f_f((((-(g_197 , p_86)) <= 0x0.5p-1) < 0x8.D9C844p-68), g_365.f2)), g_245)) < p_87) , 65535UL) <= 0UL)), l_554))))))))
                    { /* block id: 224 */
                        return l_389[4][1][8];
                    }
                    else
                    { /* block id: 226 */
                        (*g_18) = (p_88 ^ 65529UL);
                        if (g_197.f2)
                            goto lbl_555;
                    }
                    if ((**g_17))
                        break;
                }
            }
        }
        else
        { /* block id: 233 */
            int32_t *l_556 = &l_161;
            int32_t *l_557[8][1] = {{(void*)0},{&l_489[0][2][1]},{&l_489[0][2][1]},{(void*)0},{&l_489[0][2][1]},{&l_489[0][2][1]},{(void*)0},{&l_489[0][2][1]}};
            int i, j;
            l_558++;
            for (l_464 = 0; (l_464 == 58); l_464 = safe_add_func_int32_t_s_s(l_464, 9))
            { /* block id: 237 */
                uint8_t *l_564 = &g_245;
                uint8_t **l_563 = &l_564;
                int32_t l_565[2][9][4] = {{{0x073B54D6L,(-1L),2L,0x42F49ADBL},{0x8062341AL,(-1L),(-1L),(-1L)},{0xA62BD7A4L,0xA62BD7A4L,(-1L),0L},{0x8062341AL,0L,2L,(-1L)},{0x073B54D6L,2L,0L,2L},{0L,2L,0x073B54D6L,(-1L)},{2L,0L,0x8062341AL,0L},{(-1L),0xA62BD7A4L,0xA62BD7A4L,(-1L)},{(-1L),(-1L),0x8062341AL,0x42F49ADBL}},{{2L,(-1L),0x073B54D6L,(-10L)},{0L,0xE29CDC6CL,0L,(-10L)},{0x073B54D6L,(-1L),2L,0x42F49ADBL},{0x8062341AL,(-1L),(-1L),(-1L)},{0xA62BD7A4L,0xA62BD7A4L,(-1L),0L},{0x8062341AL,0L,2L,(-1L)},{0x073B54D6L,2L,0L,2L},{0L,2L,0x073B54D6L,(-1L)},{2L,0L,0x8062341AL,0L}}};
                struct S0 *l_569 = &g_365;
                struct S0 **l_570 = &l_569;
                int i, j, k;
                (*l_556) &= (((*l_563) = &g_245) != (g_48[0] , &l_390));
                ++l_566;
                (*l_570) = (p_88 , l_569);
                if (l_129)
                    break;
            }
            (*p_85) = (*g_17);
            (*g_18) = (((((((0xBBE5E3F44A2FE8BBLL || (safe_mod_func_uint64_t_u_u((safe_add_func_uint16_t_u_u(((void*)0 == &l_133[5]), ((safe_rshift_func_int16_t_s_u(((((safe_sub_func_uint64_t_u_u(0x18E9C6364A263645LL, 1UL)) >= (safe_mul_func_int8_t_s_s(((p_86 >= (p_84 , ((**g_158) > (safe_div_func_uint8_t_u_u(p_84, (*l_556)))))) && l_388), 0x6EL))) || 0xCCL) > 0xC9D8DB36L), 4)) == 0xEFL))), p_87))) , p_86) || l_161) , p_87) || 0xCCA5L) != 0x6647DEB1E9862A53LL) ^ p_87);
        }
        (*p_85) = &l_129;
        return p_88;
    }
    return l_387;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_8[i], "g_8[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_10, "g_10", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_48[i], "g_48[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_165, "g_165", print_hash_value);
    transparent_crc(g_197.f0, "g_197.f0", print_hash_value);
    transparent_crc(g_197.f1, "g_197.f1", print_hash_value);
    transparent_crc(g_197.f2, "g_197.f2", print_hash_value);
    transparent_crc(g_197.f3, "g_197.f3", print_hash_value);
    transparent_crc(g_207, "g_207", print_hash_value);
    transparent_crc(g_210, "g_210", print_hash_value);
    transparent_crc(g_214, "g_214", print_hash_value);
    transparent_crc(g_245, "g_245", print_hash_value);
    transparent_crc_bytes (&g_262, sizeof(g_262), "g_262", print_hash_value);
    transparent_crc(g_304, "g_304", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_308[i], "g_308[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_340, "g_340", print_hash_value);
    transparent_crc(g_348, "g_348", print_hash_value);
    transparent_crc(g_365.f0, "g_365.f0", print_hash_value);
    transparent_crc(g_365.f1, "g_365.f1", print_hash_value);
    transparent_crc(g_365.f2, "g_365.f2", print_hash_value);
    transparent_crc(g_365.f3, "g_365.f3", print_hash_value);
    transparent_crc(g_491, "g_491", print_hash_value);
    transparent_crc(g_584, "g_584", print_hash_value);
    transparent_crc(g_588.f0, "g_588.f0", print_hash_value);
    transparent_crc(g_588.f1, "g_588.f1", print_hash_value);
    transparent_crc(g_588.f2, "g_588.f2", print_hash_value);
    transparent_crc(g_588.f3, "g_588.f3", print_hash_value);
    transparent_crc(g_589.f0, "g_589.f0", print_hash_value);
    transparent_crc(g_589.f1, "g_589.f1", print_hash_value);
    transparent_crc(g_589.f2, "g_589.f2", print_hash_value);
    transparent_crc(g_589.f3, "g_589.f3", print_hash_value);
    transparent_crc(g_592, "g_592", print_hash_value);
    transparent_crc(g_702, "g_702", print_hash_value);
    transparent_crc(g_753, "g_753", print_hash_value);
    transparent_crc_bytes (&g_791, sizeof(g_791), "g_791", print_hash_value);
    transparent_crc(g_800.f0, "g_800.f0", print_hash_value);
    transparent_crc(g_800.f1, "g_800.f1", print_hash_value);
    transparent_crc(g_800.f2, "g_800.f2", print_hash_value);
    transparent_crc(g_800.f3, "g_800.f3", print_hash_value);
    transparent_crc(g_815.f0, "g_815.f0", print_hash_value);
    transparent_crc(g_815.f1, "g_815.f1", print_hash_value);
    transparent_crc(g_815.f2, "g_815.f2", print_hash_value);
    transparent_crc(g_815.f3, "g_815.f3", print_hash_value);
    transparent_crc_bytes (&g_882, sizeof(g_882), "g_882", print_hash_value);
    transparent_crc(g_890, "g_890", print_hash_value);
    transparent_crc(g_930, "g_930", print_hash_value);
    transparent_crc(g_939.f0, "g_939.f0", print_hash_value);
    transparent_crc(g_939.f1, "g_939.f1", print_hash_value);
    transparent_crc(g_939.f2, "g_939.f2", print_hash_value);
    transparent_crc(g_939.f3, "g_939.f3", print_hash_value);
    transparent_crc_bytes (&g_967, sizeof(g_967), "g_967", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_968[i], "g_968[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_972, "g_972", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_981[i][j], "g_981[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_982, "g_982", print_hash_value);
    transparent_crc(g_1002, "g_1002", print_hash_value);
    transparent_crc(g_1004, "g_1004", print_hash_value);
    transparent_crc(g_1022, "g_1022", print_hash_value);
    transparent_crc(g_1048.f0, "g_1048.f0", print_hash_value);
    transparent_crc(g_1048.f1, "g_1048.f1", print_hash_value);
    transparent_crc(g_1048.f2, "g_1048.f2", print_hash_value);
    transparent_crc(g_1048.f3, "g_1048.f3", print_hash_value);
    transparent_crc(g_1094.f0, "g_1094.f0", print_hash_value);
    transparent_crc(g_1094.f1, "g_1094.f1", print_hash_value);
    transparent_crc(g_1094.f2, "g_1094.f2", print_hash_value);
    transparent_crc(g_1094.f3, "g_1094.f3", print_hash_value);
    transparent_crc(g_1104, "g_1104", print_hash_value);
    transparent_crc(g_1213, "g_1213", print_hash_value);
    transparent_crc(g_1217, "g_1217", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1244[i], "g_1244[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1337, "g_1337", print_hash_value);
    transparent_crc(g_1374, "g_1374", print_hash_value);
    transparent_crc(g_1379.f0, "g_1379.f0", print_hash_value);
    transparent_crc(g_1379.f1, "g_1379.f1", print_hash_value);
    transparent_crc(g_1379.f2, "g_1379.f2", print_hash_value);
    transparent_crc(g_1379.f3, "g_1379.f3", print_hash_value);
    transparent_crc(g_1420, "g_1420", print_hash_value);
    transparent_crc(g_1487, "g_1487", print_hash_value);
    transparent_crc(g_1537.f0, "g_1537.f0", print_hash_value);
    transparent_crc(g_1537.f1, "g_1537.f1", print_hash_value);
    transparent_crc(g_1537.f2, "g_1537.f2", print_hash_value);
    transparent_crc(g_1537.f3, "g_1537.f3", print_hash_value);
    transparent_crc(g_1612.f0, "g_1612.f0", print_hash_value);
    transparent_crc(g_1612.f1, "g_1612.f1", print_hash_value);
    transparent_crc(g_1612.f2, "g_1612.f2", print_hash_value);
    transparent_crc(g_1612.f3, "g_1612.f3", print_hash_value);
    transparent_crc(g_1698, "g_1698", print_hash_value);
    transparent_crc_bytes (&g_1763, sizeof(g_1763), "g_1763", print_hash_value);
    transparent_crc(g_1822, "g_1822", print_hash_value);
    transparent_crc(g_1849, "g_1849", print_hash_value);
    transparent_crc(g_1853, "g_1853", print_hash_value);
    transparent_crc(g_1920, "g_1920", print_hash_value);
    transparent_crc(g_1933.f0, "g_1933.f0", print_hash_value);
    transparent_crc(g_1933.f1, "g_1933.f1", print_hash_value);
    transparent_crc(g_1933.f2, "g_1933.f2", print_hash_value);
    transparent_crc(g_1933.f3, "g_1933.f3", print_hash_value);
    transparent_crc(g_2013.f0, "g_2013.f0", print_hash_value);
    transparent_crc(g_2013.f1, "g_2013.f1", print_hash_value);
    transparent_crc(g_2013.f2, "g_2013.f2", print_hash_value);
    transparent_crc(g_2013.f3, "g_2013.f3", print_hash_value);
    transparent_crc(g_2061, "g_2061", print_hash_value);
    transparent_crc(g_2144.f0, "g_2144.f0", print_hash_value);
    transparent_crc(g_2144.f1, "g_2144.f1", print_hash_value);
    transparent_crc(g_2144.f2, "g_2144.f2", print_hash_value);
    transparent_crc(g_2144.f3, "g_2144.f3", print_hash_value);
    transparent_crc(g_2162.f0, "g_2162.f0", print_hash_value);
    transparent_crc(g_2162.f1, "g_2162.f1", print_hash_value);
    transparent_crc(g_2162.f2, "g_2162.f2", print_hash_value);
    transparent_crc(g_2162.f3, "g_2162.f3", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2268[i][j], "g_2268[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2314, "g_2314", print_hash_value);
    transparent_crc(g_2408, "g_2408", print_hash_value);
    transparent_crc(g_2634, "g_2634", print_hash_value);
    transparent_crc(g_2716, "g_2716", print_hash_value);
    transparent_crc(g_2821, "g_2821", print_hash_value);
    transparent_crc(g_2839, "g_2839", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2862[i], "g_2862[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2894, "g_2894", print_hash_value);
    transparent_crc(g_2918, "g_2918", print_hash_value);
    transparent_crc(g_2925, "g_2925", print_hash_value);
    transparent_crc(g_2950, "g_2950", print_hash_value);
    transparent_crc(g_2960, "g_2960", print_hash_value);
    transparent_crc(g_2993, "g_2993", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_3034[i], sizeof(g_3034[i]), "g_3034[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3115.f0, "g_3115.f0", print_hash_value);
    transparent_crc(g_3115.f1, "g_3115.f1", print_hash_value);
    transparent_crc(g_3115.f2, "g_3115.f2", print_hash_value);
    transparent_crc(g_3115.f3, "g_3115.f3", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc_bytes(&g_3146[i], sizeof(g_3146[i]), "g_3146[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3208, "g_3208", print_hash_value);
    transparent_crc(g_3225, "g_3225", print_hash_value);
    transparent_crc(g_3279, "g_3279", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_3374[i][j], "g_3374[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3416, "g_3416", print_hash_value);
    transparent_crc(g_3512, "g_3512", print_hash_value);
    transparent_crc(g_3588, "g_3588", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 964
   depth: 1, occurrence: 14
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 33
breakdown:
   depth: 1, occurrence: 205
   depth: 2, occurrence: 63
   depth: 3, occurrence: 6
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 9, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 3
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 2
   depth: 33, occurrence: 1

XXX total number of pointers: 847

XXX times a variable address is taken: 2161
XXX times a pointer is dereferenced on RHS: 665
breakdown:
   depth: 1, occurrence: 447
   depth: 2, occurrence: 170
   depth: 3, occurrence: 32
   depth: 4, occurrence: 16
XXX times a pointer is dereferenced on LHS: 498
breakdown:
   depth: 1, occurrence: 403
   depth: 2, occurrence: 79
   depth: 3, occurrence: 16
XXX times a pointer is compared with null: 56
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 14639

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1905
   level: 2, occurrence: 784
   level: 3, occurrence: 305
   level: 4, occurrence: 56
   level: 5, occurrence: 29
XXX number of pointers point to pointers: 400
XXX number of pointers point to scalars: 428
XXX number of pointers point to structs: 19
XXX percent of pointers has null in alias set: 30.5
XXX average alias set size: 1.46

XXX times a non-volatile is read: 3197
XXX times a non-volatile is write: 1508
XXX times a volatile is read: 167
XXX    times read thru a pointer: 9
XXX times a volatile is write: 47
XXX    times written thru a pointer: 1
XXX times a volatile is available for access: 1.06e+04
XXX percentage of non-volatile access: 95.6

XXX forward jumps: 1
XXX backward jumps: 13

XXX stmts: 213
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 25
   depth: 2, occurrence: 36
   depth: 3, occurrence: 37
   depth: 4, occurrence: 38
   depth: 5, occurrence: 45

XXX percentage a fresh-made variable is used: 16
XXX percentage an existing variable is used: 84
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2484087328
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S3 {
   const int32_t  f0;
   uint32_t  f1;
   volatile uint32_t  f2;
   int32_t  f3;
   int64_t  f4;
   signed f5 : 18;
   uint8_t  f6;
   uint32_t  f7;
   const int64_t  f8;
   volatile int64_t  f9;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = (-1L);
static volatile uint32_t g_4 = 0xC3F30B91L;/* VOLATILE GLOBAL g_4 */
static volatile float g_7 = 0x0.Bp+1;/* VOLATILE GLOBAL g_7 */
static volatile int16_t g_8 = 0x8CBDL;/* VOLATILE GLOBAL g_8 */
static struct S3 g_14 = {-1L,0x1FFBE626L,0x9FF93F8DL,0x905F4D0CL,1L,381,246UL,18446744073709551608UL,0x5FBC01D4C3F121F2LL,0x59515ED761E7FF51LL};/* VOLATILE GLOBAL g_14 */


/* --- FORWARD DECLARATIONS --- */
static struct S3  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_14
 * writes: g_4
 */
static struct S3  func_1(void)
{ /* block id: 0 */
    int32_t *l_2[10][8][3] = {{{(void*)0,&g_3,&g_3},{&g_3,(void*)0,(void*)0},{(void*)0,(void*)0,&g_3},{(void*)0,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3}},{{&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3},{(void*)0,&g_3,&g_3},{(void*)0,&g_3,&g_3},{&g_3,(void*)0,(void*)0},{(void*)0,&g_3,&g_3}},{{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3}},{{&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0}},{{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0}},{{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3}},{{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3}},{{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3}},{{&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3}},{{&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3},{&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3}}};
    int16_t l_9 = 0x3846L;
    uint64_t l_10 = 0x39BF5BF81606DC06LL;
    int i, j, k;
lbl_13:
    g_4--;
    l_10--;
    if (l_10)
        goto lbl_13;
    return g_14;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc_bytes (&g_7, sizeof(g_7), "g_7", print_hash_value);
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_14.f0, "g_14.f0", print_hash_value);
    transparent_crc(g_14.f1, "g_14.f1", print_hash_value);
    transparent_crc(g_14.f2, "g_14.f2", print_hash_value);
    transparent_crc(g_14.f3, "g_14.f3", print_hash_value);
    transparent_crc(g_14.f4, "g_14.f4", print_hash_value);
    transparent_crc(g_14.f5, "g_14.f5", print_hash_value);
    transparent_crc(g_14.f6, "g_14.f6", print_hash_value);
    transparent_crc(g_14.f7, "g_14.f7", print_hash_value);
    transparent_crc(g_14.f8, "g_14.f8", print_hash_value);
    transparent_crc(g_14.f9, "g_14.f9", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 5
   depth: 1, occurrence: 1
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 1
breakdown:
   indirect level: 0, occurrence: 1
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 1
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 1
breakdown:
   depth: 1, occurrence: 6

XXX total number of pointers: 1

XXX times a variable address is taken: 10
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 0
breakdown:
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 0
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 1
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 100
XXX average alias set size: 2

XXX times a non-volatile is read: 1
XXX times a non-volatile is write: 1
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 3
XXX percentage of non-volatile access: 66.7

XXX forward jumps: 0
XXX backward jumps: 1

XXX stmts: 4
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 4

XXX percentage a fresh-made variable is used: 60
XXX percentage an existing variable is used: 40
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


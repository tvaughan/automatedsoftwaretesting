/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2005551402
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int32_t  f0;
   volatile int32_t  f1;
};

union U1 {
   volatile int16_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static float g_2[10] = {0xC.DF98A5p-39,(-0x1.Cp-1),0x5.5p-1,0x5.5p-1,(-0x1.Cp-1),0xC.DF98A5p-39,(-0x1.Cp-1),0x5.5p-1,0x5.5p-1,(-0x1.Cp-1)};
static volatile int32_t g_3 = 6L;/* VOLATILE GLOBAL g_3 */
static int32_t g_4 = 0xE63E3155L;
static volatile int32_t g_5 = 0L;/* VOLATILE GLOBAL g_5 */
static int32_t g_6 = 0xF90A2337L;
static uint16_t g_23[2] = {0xE004L,0xE004L};
static int8_t g_29 = 0x34L;
static uint32_t g_34 = 4294967295UL;
static int32_t g_47 = 0x3C34F970L;
static int32_t g_80 = 0x587A760FL;
static uint16_t g_88 = 0UL;
static uint16_t *g_94 = &g_23[1];
static uint16_t ** const g_93 = &g_94;
static uint8_t g_111[10] = {0xA5L,7UL,7UL,0xA5L,0xE0L,0xA5L,7UL,7UL,0xA5L,0xE0L};
static int32_t *g_140 = (void*)0;
static int32_t **g_139 = &g_140;
static int64_t g_151 = 0x028A4BD3C15F2FB4LL;
static int16_t g_153 = (-2L);
static uint64_t g_165 = 0xCF45A8FD6DDE4B23LL;
static int64_t g_167 = (-7L);
static union U0 g_169[2] = {{0x8E86F5F8L},{0x8E86F5F8L}};
static int8_t g_174[4] = {0x04L,0x04L,0x04L,0x04L};
static uint64_t g_175[2][3] = {{0x54ECD3A2B2265A2DLL,0x54ECD3A2B2265A2DLL,2UL},{0x54ECD3A2B2265A2DLL,0x54ECD3A2B2265A2DLL,2UL}};
static union U1 g_196 = {0xE0A3L};/* VOLATILE GLOBAL g_196 */
static float * volatile g_211[8] = {&g_2[3],&g_2[3],&g_2[3],&g_2[3],&g_2[3],&g_2[3],&g_2[3],&g_2[3]};
static float * volatile g_212[8][8] = {{&g_2[8],(void*)0,&g_2[1],&g_2[1],&g_2[8],&g_2[5],&g_2[8],&g_2[1]},{&g_2[8],&g_2[5],&g_2[8],&g_2[1],&g_2[5],&g_2[5],&g_2[1],&g_2[8]},{&g_2[8],&g_2[8],&g_2[1],&g_2[1],&g_2[8],(void*)0,&g_2[1],&g_2[1]},{&g_2[5],&g_2[8],&g_2[8],&g_2[8],&g_2[8],&g_2[5],&g_2[8],&g_2[1]},{&g_2[8],&g_2[5],&g_2[8],&g_2[1],&g_2[5],&g_2[5],&g_2[1],&g_2[8]},{&g_2[8],&g_2[8],&g_2[1],&g_2[1],&g_2[8],(void*)0,&g_2[1],&g_2[1]},{&g_2[5],&g_2[8],&g_2[8],&g_2[8],&g_2[8],&g_2[5],&g_2[8],&g_2[1]},{&g_2[8],&g_2[5],&g_2[8],&g_2[1],&g_2[5],&g_2[5],&g_2[1],&g_2[8]}};
static float * volatile g_213 = &g_2[4];/* VOLATILE GLOBAL g_213 */
static volatile int8_t g_217[7][8] = {{(-10L),0x7FL,0x87L,0x7FL,(-10L),6L,(-4L),0x20L},{0x87L,0xE5L,(-5L),0xA0L,(-6L),0xEEL,0x64L,0x7FL},{0x64L,(-4L),(-5L),1L,1L,(-5L),(-4L),0x64L},{(-6L),0xF3L,0x87L,0x20L,(-4L),0xE5L,2L,(-5L)},{(-3L),0xA0L,(-4L),6L,0x20L,0xE5L,0x20L,6L},{(-4L),0xF3L,(-4L),0x87L,0xE5L,(-5L),0xA0L,(-6L)},{6L,(-4L),0x20L,2L,(-3L),0xEEL,0xE5L,0xE5L}};
static uint64_t g_218 = 0x17551DDD8C588519LL;
static int32_t g_237 = 1L;
static const volatile union U0 g_271 = {1L};/* VOLATILE GLOBAL g_271 */
static volatile union U0 g_277[6] = {{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}};
static volatile uint32_t g_281 = 0x37000C01L;/* VOLATILE GLOBAL g_281 */
static volatile union U0 g_284 = {0L};/* VOLATILE GLOBAL g_284 */
static const int32_t * volatile g_327[2] = {(void*)0,(void*)0};
static const int32_t * volatile *g_326 = &g_327[1];
static const int32_t * volatile ** volatile g_325 = &g_326;/* VOLATILE GLOBAL g_325 */
static const int32_t * volatile ** volatile *g_324 = &g_325;
static float g_387 = (-0x1.3p-1);
static float g_395 = 0x9.6p-1;
static float g_397 = 0x0.DBF972p-95;
static union U0 g_411 = {7L};/* VOLATILE GLOBAL g_411 */
static int8_t g_420 = 1L;
static uint32_t g_421 = 0xC41BF3F6L;
static volatile int64_t g_425 = 0x9F9F9BACBC50E1AELL;/* VOLATILE GLOBAL g_425 */
static uint16_t g_431 = 0x01FAL;
static int8_t * const g_437 = (void*)0;
static int8_t * const *g_436 = &g_437;
static int8_t * const ** volatile g_435 = &g_436;/* VOLATILE GLOBAL g_435 */
static union U0 g_470 = {1L};/* VOLATILE GLOBAL g_470 */
static int64_t g_477 = (-7L);
static const uint32_t * const g_503 = &g_34;
static const uint32_t * const *g_502[8] = {&g_503,&g_503,&g_503,&g_503,&g_503,&g_503,&g_503,&g_503};
static union U1 g_513 = {0x8822L};/* VOLATILE GLOBAL g_513 */
static volatile union U1 g_542 = {0x4E6DL};/* VOLATILE GLOBAL g_542 */
static volatile int64_t *g_563[9][2][10] = {{{&g_425,(void*)0,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425,&g_425,(void*)0},{&g_425,(void*)0,&g_425,(void*)0,&g_425,&g_425,&g_425,&g_425,&g_425,(void*)0}},{{&g_425,&g_425,&g_425,&g_425,(void*)0,&g_425,(void*)0,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,(void*)0,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425}},{{&g_425,(void*)0,&g_425,&g_425,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425}},{{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425}},{{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425}},{{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425}},{{&g_425,(void*)0,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425}},{{&g_425,(void*)0,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425}},{{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,(void*)0,&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425,&g_425}}};
static volatile int64_t * volatile * const g_562 = &g_563[2][1][0];
static int8_t * const ** volatile *g_573 = &g_435;
static int8_t * const ** volatile ** volatile g_572 = &g_573;/* VOLATILE GLOBAL g_572 */
static volatile int16_t g_576 = 1L;/* VOLATILE GLOBAL g_576 */
static volatile uint8_t g_577 = 0xA1L;/* VOLATILE GLOBAL g_577 */
static volatile union U1 g_587 = {-3L};/* VOLATILE GLOBAL g_587 */
static union U1 g_595[2][10][6] = {{{{0xE0A8L},{1L},{0x499DL},{0L},{0xB931L},{0x8326L}},{{0xB931L},{-1L},{0x1057L},{0L},{0L},{4L}},{{-1L},{-1L},{0L},{0x2950L},{0xB931L},{0x3F2DL}},{{0xEF0EL},{1L},{0x2EE7L},{0xB931L},{0x1E8CL},{-1L}},{{-6L},{0xCB0DL},{4L},{4L},{0xCB0DL},{-6L}},{{5L},{0xA915L},{0xEF0EL},{0xBCC0L},{0x8326L},{0x94B4L}},{{0L},{0x94B4L},{0xB931L},{-2L},{4L},{0xE0A8L}},{{0L},{0x1E8CL},{-2L},{0xBCC0L},{0x3F2DL},{1L}},{{5L},{0x8326L},{0xEBB0L},{4L},{-1L},{0L}},{{-6L},{0xB931L},{0xCB0DL},{0xB931L},{-6L},{1L}}},{{{0xEF0EL},{0L},{-6L},{0x2950L},{0x94B4L},{0xBCC0L}},{{-1L},{-6L},{1L},{0x94B4L},{3L},{0L}},{{0x3F2DL},{-1L},{0xEF0EL},{1L},{0x2EE7L},{0xB931L}},{{3L},{4L},{0xA915L},{0x1057L},{1L},{1L}},{{0x1E8CL},{-1L},{-1L},{0x1E8CL},{0xB931L},{0x2EE7L}},{{-1L},{0L},{1L},{-2L},{0L},{3L}},{{0xCB0DL},{0x8326L},{0x3F2DL},{0x2EE7L},{0L},{0L}},{{0L},{0L},{0x1057L},{-1L},{0xB931L},{0xEF0EL}},{{0xBCC0L},{-1L},{5L},{0xC36DL},{1L},{0xC36DL}},{{0L},{4L},{0L},{0xBCC0L},{0x2EE7L},{0xE0A8L}}}};
static union U0 g_605 = {0x9DBB08F2L};/* VOLATILE GLOBAL g_605 */
static int16_t g_614 = 1L;
static volatile float g_616 = 0x6.041954p-22;/* VOLATILE GLOBAL g_616 */
static int8_t g_617[3] = {1L,1L,1L};
static volatile uint32_t g_618 = 1UL;/* VOLATILE GLOBAL g_618 */
static uint8_t g_677 = 247UL;
static const uint8_t *g_676 = &g_677;
static union U0 g_682 = {1L};/* VOLATILE GLOBAL g_682 */
static uint32_t g_697 = 3UL;
static int64_t *g_718[10][1] = {{&g_477},{&g_477},{&g_151},{&g_477},{&g_477},{&g_151},{&g_477},{&g_477},{&g_151},{&g_477}};
static int64_t **g_717 = &g_718[5][0];
static int64_t ***g_716 = &g_717;
static uint32_t g_729 = 9UL;
static union U0 g_732 = {0x39893BE5L};/* VOLATILE GLOBAL g_732 */
static union U1 *g_754 = &g_595[1][9][4];
static union U1 ** volatile g_753 = &g_754;/* VOLATILE GLOBAL g_753 */
static union U1 g_789 = {0L};/* VOLATILE GLOBAL g_789 */
static volatile uint64_t g_807 = 0UL;/* VOLATILE GLOBAL g_807 */
static union U0 *g_837 = &g_169[0];
static union U0 g_838 = {-1L};/* VOLATILE GLOBAL g_838 */
static union U0 g_876[10][8] = {{{0x7322B1B3L},{1L},{0x7322B1B3L},{5L},{-3L},{-3L},{5L},{0x7322B1B3L}},{{1L},{1L},{-3L},{0x18688B81L},{-6L},{0x18688B81L},{-3L},{1L}},{{1L},{0x7322B1B3L},{5L},{-3L},{-3L},{5L},{0x7322B1B3L},{1L}},{{0x7322B1B3L},{0x4AAEE773L},{1L},{0x18688B81L},{1L},{0x4AAEE773L},{0x7322B1B3L},{0x7322B1B3L}},{{0x4AAEE773L},{0x18688B81L},{5L},{5L},{0x18688B81L},{0x4AAEE773L},{-3L},{0x4AAEE773L}},{{0x18688B81L},{0x4AAEE773L},{-3L},{0x4AAEE773L},{0x18688B81L},{5L},{5L},{0x18688B81L}},{{0x4AAEE773L},{0x7322B1B3L},{0x7322B1B3L},{0x4AAEE773L},{1L},{0x18688B81L},{1L},{0x4AAEE773L}},{{0x7322B1B3L},{1L},{0x7322B1B3L},{5L},{-3L},{-3L},{5L},{0x7322B1B3L}},{{1L},{1L},{-3L},{0x18688B81L},{-6L},{0x18688B81L},{-3L},{1L}},{{1L},{0x7322B1B3L},{5L},{-3L},{-3L},{5L},{0x7322B1B3L},{1L}}};
static const uint32_t ** const g_952 = (void*)0;
static const uint32_t ** const *g_951 = &g_952;
static const uint32_t ** const **g_950 = &g_951;
static volatile union U0 g_975 = {1L};/* VOLATILE GLOBAL g_975 */
static union U0 ** volatile g_1019[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static union U0 ** volatile g_1020 = &g_837;/* VOLATILE GLOBAL g_1020 */
static const volatile union U0 g_1029 = {8L};/* VOLATILE GLOBAL g_1029 */
static volatile int16_t g_1057 = 0xE83BL;/* VOLATILE GLOBAL g_1057 */
static volatile int8_t g_1058 = (-1L);/* VOLATILE GLOBAL g_1058 */
static int32_t g_1059 = (-8L);
static volatile uint64_t g_1060 = 1UL;/* VOLATILE GLOBAL g_1060 */
static int8_t * volatile g_1095[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int8_t * volatile *g_1094[2][7][7] = {{{&g_1095[1],(void*)0,&g_1095[5],&g_1095[3],&g_1095[5],&g_1095[0],&g_1095[6]},{(void*)0,&g_1095[5],&g_1095[1],&g_1095[5],&g_1095[0],&g_1095[5],&g_1095[1]},{(void*)0,(void*)0,&g_1095[5],(void*)0,&g_1095[3],&g_1095[1],&g_1095[5]},{&g_1095[7],&g_1095[5],&g_1095[5],&g_1095[5],&g_1095[1],&g_1095[5],&g_1095[5]},{&g_1095[3],(void*)0,&g_1095[5],(void*)0,&g_1095[3],(void*)0,&g_1095[0]},{&g_1095[5],&g_1095[5],&g_1095[5],&g_1095[7],&g_1095[5],&g_1095[5],&g_1095[0]},{&g_1095[5],&g_1095[8],&g_1095[1],&g_1095[3],&g_1095[3],&g_1095[1],&g_1095[8]}},{{&g_1095[5],&g_1095[7],&g_1095[5],(void*)0,&g_1095[5],&g_1095[5],(void*)0},{&g_1095[1],&g_1095[6],&g_1095[5],&g_1095[6],&g_1095[5],&g_1095[3],&g_1095[5]},{(void*)0,&g_1095[1],&g_1095[1],(void*)0,&g_1095[4],(void*)0,&g_1095[5]},{&g_1095[5],&g_1095[5],&g_1095[1],&g_1095[3],(void*)0,&g_1095[5],(void*)0},{&g_1095[5],&g_1095[5],(void*)0,&g_1095[7],(void*)0,&g_1095[5],&g_1095[5]},{&g_1095[5],&g_1095[5],&g_1095[6],(void*)0,&g_1095[5],&g_1095[8],&g_1095[5]},{&g_1095[0],&g_1095[5],&g_1095[5],(void*)0,&g_1095[5],&g_1095[5],(void*)0}}};
static int8_t * volatile * volatile * volatile g_1093 = &g_1094[0][5][4];/* VOLATILE GLOBAL g_1093 */
static union U1 g_1106 = {0L};/* VOLATILE GLOBAL g_1106 */
static union U1 g_1107 = {0xE293L};/* VOLATILE GLOBAL g_1107 */
static int64_t g_1124 = 0L;
static volatile union U0 g_1126 = {0xE3B18B4AL};/* VOLATILE GLOBAL g_1126 */
static union U0 g_1156[2][7] = {{{0L},{0L},{0L},{0L},{0L},{0L},{0L}},{{0L},{0xAD307771L},{0xAD307771L},{0L},{0xAD307771L},{0xAD307771L},{0L}}};
static volatile float g_1162 = 0xD.3A4D4Ap+6;/* VOLATILE GLOBAL g_1162 */
static volatile float *g_1161[9] = {&g_1162,&g_1162,&g_1162,&g_1162,&g_1162,&g_1162,&g_1162,&g_1162,&g_1162};
static volatile float * volatile *g_1160 = &g_1161[4];
static union U1 g_1187 = {9L};/* VOLATILE GLOBAL g_1187 */
static union U1 *g_1186 = &g_1187;
static int32_t * volatile g_1243[4] = {&g_237,&g_237,&g_237,&g_237};
static int32_t * volatile g_1244[2][10] = {{&g_6,(void*)0,&g_80,(void*)0,&g_6,&g_80,&g_4,&g_4,&g_80,&g_6},{&g_6,&g_80,&g_80,&g_6,&g_237,&g_4,&g_6,&g_4,&g_237,&g_6}};
static volatile union U1 g_1268[10] = {{1L},{1L},{0xCE3DL},{0x759DL},{0xCE3DL},{1L},{1L},{0xCE3DL},{0x759DL},{0xCE3DL}};
static union U1 ** volatile g_1269 = &g_1186;/* VOLATILE GLOBAL g_1269 */
static int32_t * volatile g_1274 = &g_80;/* VOLATILE GLOBAL g_1274 */
static int64_t g_1317 = 0xE94EDCED255F9FE1LL;
static const volatile uint32_t g_1329 = 1UL;/* VOLATILE GLOBAL g_1329 */
static union U0 ** const  volatile g_1375 = (void*)0;/* VOLATILE GLOBAL g_1375 */
static union U0 *g_1377 = &g_732;
static union U0 ** volatile g_1376 = &g_1377;/* VOLATILE GLOBAL g_1376 */
static uint32_t *g_1389 = &g_34;
static uint32_t **g_1388 = &g_1389;
static uint32_t **g_1390 = &g_1389;
static uint8_t g_1407 = 0xACL;
static volatile uint32_t g_1416[7][4] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551610UL,18446744073709551607UL},{18446744073709551615UL,0x462086C6L,1UL,18446744073709551615UL},{0x7E0E14A9L,18446744073709551607UL,0x7E0E14A9L,1UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551610UL,18446744073709551615UL},{18446744073709551607UL,0x462086C6L,0x462086C6L,18446744073709551607UL},{0x7E0E14A9L,18446744073709551615UL,0x462086C6L,1UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551610UL,18446744073709551607UL}};
static int32_t g_1522 = 0L;
static volatile union U0 g_1543 = {0L};/* VOLATILE GLOBAL g_1543 */
static uint8_t *g_1551 = &g_1407;
static uint8_t **g_1550 = &g_1551;
static uint8_t ***g_1549 = &g_1550;
static int64_t ****g_1569 = (void*)0;
static int8_t g_1574 = 3L;
static int64_t g_1591 = 0x29434CE070572609LL;
static uint8_t ****g_1645 = (void*)0;
static volatile uint64_t g_1680[5] = {0x968EDA5D6E5593D7LL,0x968EDA5D6E5593D7LL,0x968EDA5D6E5593D7LL,0x968EDA5D6E5593D7LL,0x968EDA5D6E5593D7LL};
static volatile uint16_t g_1681 = 0UL;/* VOLATILE GLOBAL g_1681 */
static int8_t *g_1685 = (void*)0;
static volatile union U1 g_1777[5] = {{0x4B28L},{0x4B28L},{0x4B28L},{0x4B28L},{0x4B28L}};
static volatile uint16_t ***g_1790 = (void*)0;
static volatile uint16_t *** volatile * volatile g_1789 = &g_1790;/* VOLATILE GLOBAL g_1789 */
static union U0 g_1971[1][10][2] = {{{{0L},{1L}},{{1L},{0L}},{{1L},{1L}},{{0L},{1L}},{{1L},{0L}},{{1L},{1L}},{{0L},{1L}},{{1L},{0L}},{{1L},{1L}},{{0L},{1L}}}};
static uint64_t g_1973 = 1UL;
static union U0 ** volatile g_1976 = &g_1377;/* VOLATILE GLOBAL g_1976 */
static volatile uint32_t g_2029[5] = {7UL,7UL,7UL,7UL,7UL};
static volatile int32_t g_2033 = 1L;/* VOLATILE GLOBAL g_2033 */
static int8_t ***g_2105 = (void*)0;
static int8_t ****g_2104 = &g_2105;
static int32_t * volatile g_2118 = &g_4;/* VOLATILE GLOBAL g_2118 */
static union U1 g_2143[9][3][6] = {{{{0L},{0x3F3CL},{0x6758L},{9L},{0x182DL},{0x7A45L}},{{0xD2EBL},{0L},{3L},{0xD2EBL},{0xD2EBL},{3L}},{{0x2533L},{0x2533L},{0xD2EBL},{1L},{0x7A45L},{0xA7ADL}}},{{{9L},{0L},{-2L},{0x667EL},{-1L},{0xD2EBL}},{{-5L},{9L},{-2L},{0L},{0x2533L},{0xA7ADL}},{{0xD876L},{0L},{0xD2EBL},{8L},{0xEF3DL},{3L}}},{{{8L},{0xEF3DL},{3L},{0x45C9L},{0xC21FL},{8L}},{{0L},{-5L},{0L},{1L},{0L},{-5L}},{{0L},{0L},{0x667EL},{0x182DL},{-1L},{0x45C9L}}},{{{0x2533L},{0L},{0x182DL},{0xC21FL},{0x3F3CL},{0xA7ADL}},{{0xBC6BL},{0L},{9L},{3L},{-1L},{0xAB93L}},{{8L},{0L},{0xBC6BL},{0L},{0L},{0xD876L}}},{{{0xA7ADL},{-5L},{0xD2EBL},{5L},{0xC21FL},{1L}},{{0L},{0xEF3DL},{0x667EL},{0x667EL},{0xEF3DL},{0L}},{{0L},{0L},{0xA07CL},{0xC21FL},{0x2533L},{-5L}}},{{{0xFAB2L},{9L},{0x45C9L},{0xAB93L},{-1L},{3L}},{{0xFAB2L},{0L},{0xAB93L},{0xC21FL},{0x7A45L},{0xD876L}},{{0L},{0x2533L},{9L},{0x667EL},{0xD2EBL},{0x6758L}}},{{{0L},{0L},{-2L},{5L},{0L},{0xC21FL}},{{0xA7ADL},{0L},{0x182DL},{0L},{0xA7ADL},{1L}},{{8L},{0xC21FL},{0x45C9L},{3L},{0xEF3DL},{8L}}},{{{0xBC6BL},{0L},{0xFAB2L},{0xC21FL},{0L},{8L}},{{0x2533L},{0L},{0x45C9L},{0x182DL},{0xD2EBL},{1L}},{{0L},{0L},{0x182DL},{1L},{0x7B04L},{0xC21FL}}},{{{0L},{0L},{-2L},{0x45C9L},{0x6758L},{0x6758L}},{{8L},{9L},{9L},{8L},{0L},{0xD876L}},{{0xD876L},{0L},{0xAB93L},{0L},{0xC21FL},{3L}}}};
static const int16_t g_2248 = (-9L);
static uint16_t g_2350 = 65535UL;
static const volatile union U0 g_2352 = {0xBE56F420L};/* VOLATILE GLOBAL g_2352 */
static union U1 g_2371 = {1L};/* VOLATILE GLOBAL g_2371 */
static uint8_t * const g_2399 = (void*)0;
static uint8_t * const *g_2398 = &g_2399;
static uint8_t * const **g_2397 = &g_2398;
static uint8_t * const ***g_2396 = &g_2397;
static uint8_t * const ****g_2395 = &g_2396;
static volatile union U1 g_2409 = {0x864AL};/* VOLATILE GLOBAL g_2409 */
static const int64_t *g_2412 = &g_167;
static const int64_t **g_2411 = &g_2412;
static union U1 g_2427[6] = {{0x1D3EL},{0x1D3EL},{0x1D3EL},{0x1D3EL},{0x1D3EL},{0x1D3EL}};
static uint64_t g_2480[2] = {2UL,2UL};
static union U1 g_2493 = {0x2AADL};/* VOLATILE GLOBAL g_2493 */
static int16_t g_2529 = (-8L);
static union U1 **g_2541 = &g_1186;
static uint64_t *g_2561[5] = {&g_175[0][0],&g_175[0][0],&g_175[0][0],&g_175[0][0],&g_175[0][0]};
static volatile union U0 g_2592 = {-1L};/* VOLATILE GLOBAL g_2592 */
static int32_t * const ***g_2726 = (void*)0;
static int32_t * const ****g_2725 = &g_2726;
static uint16_t **g_2736 = (void*)0;
static uint16_t ***g_2735 = &g_2736;
static uint16_t ****g_2734 = &g_2735;
static volatile union U0 g_2747 = {5L};/* VOLATILE GLOBAL g_2747 */
static volatile union U1 g_2752[6][2] = {{{0xB411L},{0L}},{{-1L},{-1L}},{{-1L},{0L}},{{0xB411L},{-1L}},{{0L},{-1L}},{{0xB411L},{0L}}};
static int64_t ***g_2833 = (void*)0;
static int64_t ***g_2834 = (void*)0;
static int64_t ***g_2835[10] = {&g_717,&g_717,&g_717,&g_717,&g_717,&g_717,&g_717,&g_717,&g_717,&g_717};
static int64_t ***g_2836 = &g_717;
static int64_t **** const g_2832[3][9] = {{&g_2836,(void*)0,&g_2836,(void*)0,&g_2836,(void*)0,&g_2836,(void*)0,&g_2836},{&g_2834,&g_2833,&g_2833,&g_2834,&g_2834,&g_2833,&g_2833,&g_2834,&g_2834},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int64_t **** const *g_2831[9] = {&g_2832[1][2],&g_2832[1][2],&g_2832[1][2],&g_2832[1][2],&g_2832[1][2],&g_2832[1][2],&g_2832[1][2],&g_2832[1][2],&g_2832[1][2]};
static union U1 g_2842[6][4][4] = {{{{0x00FAL},{0xBBD9L},{0L},{0L}},{{0xEF0BL},{0xEF0BL},{0L},{0L}},{{-1L},{8L},{-1L},{0x573FL}},{{-1L},{0x573FL},{0x00FAL},{-1L}}},{{{0xEF0BL},{0x573FL},{4L},{0x573FL}},{{0x573FL},{8L},{0L},{0L}},{{0x2612L},{0xEF0BL},{0x00FAL},{0L}},{{-1L},{0xBBD9L},{1L},{0x573FL}}},{{{-1L},{0x00FAL},{0x00FAL},{-1L}},{{0x2612L},{0x573FL},{0L},{0x00FAL}},{{0x573FL},{0xBBD9L},{4L},{0L}},{{0xEF0BL},{0x2612L},{0x00FAL},{0L}}},{{{-1L},{0xBBD9L},{-1L},{0x00FAL}},{{-1L},{0x573FL},{0L},{-1L}},{{0xEF0BL},{0x00FAL},{0L},{0x573FL}},{{0x00FAL},{0xBBD9L},{0L},{0L}}},{{{0xEF0BL},{0xEF0BL},{0L},{0L}},{{-1L},{8L},{-1L},{0x573FL}},{{-1L},{0x573FL},{0x00FAL},{-1L}},{{0xEF0BL},{0x573FL},{4L},{0x573FL}}},{{{0x573FL},{8L},{0L},{0L}},{{0x2612L},{0xEF0BL},{0x00FAL},{0L}},{{-1L},{0xBBD9L},{1L},{0x573FL}},{{-1L},{0x00FAL},{0x00FAL},{-1L}}}};
static uint64_t g_2860 = 0x2BACB72E2B69CDFDLL;
static union U0 g_2932 = {0x8D7F0B3CL};/* VOLATILE GLOBAL g_2932 */
static int32_t ***g_2950 = &g_139;
static int32_t ****g_2949 = &g_2950;
static int32_t *****g_2948 = &g_2949;
static uint32_t g_2955 = 0UL;
static union U0 g_2969 = {0x0A87C6BDL};/* VOLATILE GLOBAL g_2969 */
static volatile union U0 g_2991 = {0x5C9DE92DL};/* VOLATILE GLOBAL g_2991 */
static const float g_3050 = 0x1.1p+1;
static const float g_3052[3][1][8] = {{{0xE.0D5388p-5,0xD.D615D8p-53,(-0x5.Ep-1),0x9.0F3113p-99,(-0x5.Ep-1),0xD.D615D8p-53,0xE.0D5388p-5,0xE.0D5388p-5}},{{0xD.D615D8p-53,0x9.0F3113p-99,0x2.A3056Dp+22,0x2.A3056Dp+22,0x9.0F3113p-99,0xD.D615D8p-53,0xA.810368p-62,0xD.D615D8p-53}},{{0x9.0F3113p-99,0xD.D615D8p-53,0xA.810368p-62,0xD.D615D8p-53,0x9.0F3113p-99,0x2.A3056Dp+22,0x2.A3056Dp+22,0x9.0F3113p-99}}};
static const float *g_3051 = &g_3052[0][0][6];
static int32_t * volatile g_3054 = &g_6;/* VOLATILE GLOBAL g_3054 */
static uint32_t *g_3072 = &g_729;
static uint32_t **g_3071 = &g_3072;
static const volatile uint8_t g_3115 = 0UL;/* VOLATILE GLOBAL g_3115 */
static uint32_t ***g_3128[10][4] = {{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388},{&g_1388,&g_1388,&g_1388,&g_1388}};
static uint32_t ****g_3127 = &g_3128[7][3];
static uint32_t *****g_3126 = &g_3127;
static union U1 g_3137[10] = {{0x6341L},{0x14F8L},{0L},{0L},{0x14F8L},{0x6341L},{0x14F8L},{0L},{0L},{0x14F8L}};


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static int32_t  func_37(const uint16_t * p_38, uint16_t * p_39, int32_t ** p_40, uint16_t * const  p_41, uint32_t  p_42);
static const uint16_t * func_43(uint32_t  p_44);
static uint16_t * func_50(int32_t ** p_51, uint16_t * p_52, const int16_t  p_53, int32_t * p_54, uint16_t  p_55);
static int32_t ** func_56(int16_t  p_57, const int32_t  p_58, const int16_t  p_59, float  p_60);
static int16_t  func_62(uint32_t * p_63);
static uint16_t ** const  func_66(uint16_t * p_67);
static float  func_95(int8_t  p_96, uint16_t ** const  p_97, uint32_t  p_98, float  p_99);
static float  func_107(const uint32_t  p_108, uint16_t * p_109, float  p_110);
static union U0  func_114(uint32_t * p_115, int16_t  p_116, int16_t  p_117, uint32_t  p_118, uint32_t * p_119);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_6 g_2 g_23 g_5 g_34 g_2949 g_2950 g_139 g_140 g_326
 * writes: g_4 g_6 g_23 g_34 g_327
 */
static float  func_1(void)
{ /* block id: 0 */
    int32_t * const l_9 = &g_6;
    int32_t l_28 = (-5L);
    uint16_t *l_48 = &g_23[1];
    const int8_t l_61 = 1L;
    const uint8_t l_1278 = 1UL;
    int32_t *l_3062[9] = {(void*)0,&g_4,(void*)0,&g_4,(void*)0,&g_4,(void*)0,&g_4,(void*)0};
    int32_t **l_3061[9][2] = {{&l_3062[0],&l_3062[2]},{&l_3062[0],&l_3062[0]},{&l_3062[2],&l_3062[0]},{&l_3062[0],&l_3062[2]},{&l_3062[0],&l_3062[0]},{&l_3062[2],&l_3062[0]},{&l_3062[0],&l_3062[2]},{&l_3062[0],&l_3062[0]},{&l_3062[2],&l_3062[0]}};
    int16_t l_3162[2];
    int i, j;
    for (i = 0; i < 2; i++)
        l_3162[i] = 1L;
    for (g_4 = 2; (g_4 <= 9); g_4 += 1)
    { /* block id: 3 */
        for (g_6 = 6; (g_6 >= 0); g_6 -= 1)
        { /* block id: 6 */
            int i;
            return g_2[g_4];
        }
    }
    for (g_4 = 2; (g_4 != (-22)); g_4 = safe_sub_func_int64_t_s_s(g_4, 8))
    { /* block id: 12 */
        uint16_t l_10 = 0x5927L;
        int32_t l_11[9] = {0x6BDE64F6L,0x8405CEFDL,0x8405CEFDL,0x6BDE64F6L,0x8405CEFDL,0x8405CEFDL,0x6BDE64F6L,0x8405CEFDL,0x8405CEFDL};
        int32_t *l_13 = &l_11[7];
        int32_t **l_12 = &l_13;
        int32_t l_3161 = 5L;
        int i;
        (*l_12) = (((l_10 &= (l_9 == &g_6)) < (l_11[3] || l_11[3])) , (void*)0);
        for (l_10 = 0; (l_10 > 25); l_10 = safe_add_func_uint8_t_u_u(l_10, 7))
        { /* block id: 17 */
            uint32_t l_18 = 0x255D5800L;
            int32_t l_21[1][9][4] = {{{0x095DBC2AL,0xEAC3EFC1L,0x6540F213L,0xEAC3EFC1L},{0xEAC3EFC1L,0xBDF78B15L,0x6540F213L,0x6540F213L},{0x095DBC2AL,0x095DBC2AL,0xEAC3EFC1L,0x6540F213L},{0x70004DDCL,0xBDF78B15L,0x70004DDCL,0xEAC3EFC1L},{0x70004DDCL,0xEAC3EFC1L,0xEAC3EFC1L,0x70004DDCL},{0x095DBC2AL,0xEAC3EFC1L,0x6540F213L,0xEAC3EFC1L},{0xEAC3EFC1L,0xBDF78B15L,0x6540F213L,0x6540F213L},{0x095DBC2AL,0x095DBC2AL,0xEAC3EFC1L,0x6540F213L},{0x70004DDCL,0xBDF78B15L,0x70004DDCL,0xEAC3EFC1L}}};
            uint16_t *l_22 = &g_23[0];
            uint32_t *l_45 = (void*)0;
            uint32_t *l_46 = &l_18;
            int i, j, k;
            (*l_9) = (((safe_mod_func_uint16_t_u_u(l_18, (safe_lshift_func_uint16_t_u_s(((*l_22)++), 4)))) & g_5) != 0x42L);
            for (l_18 = 29; (l_18 >= 9); l_18--)
            { /* block id: 22 */
                int32_t *l_30 = &l_21[0][1][0];
                int32_t *l_31 = &g_6;
                int32_t *l_32 = (void*)0;
                int32_t *l_33[4][3];
                int i, j;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_33[i][j] = &l_28;
                }
                g_34--;
                if (g_6)
                    break;
            }
        }
        (*g_326) = (***g_2949);
    }
    return l_3162[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_218 g_3071 g_1160 g_1161 g_1162 g_3051 g_421 g_3052 g_676 g_677 g_93 g_94 g_23 g_3072 g_876.f0 g_1550 g_1551 g_1407 g_153 g_1549 g_732.f0 g_1274 g_2955 g_3115 g_1269 g_1186
 * writes: g_218 g_1162 g_729 g_1407 g_732.f0 g_80 g_2955 g_237 g_470.f0 g_3126
 */
static int32_t  func_37(const uint16_t * p_38, uint16_t * p_39, int32_t ** p_40, uint16_t * const  p_41, uint32_t  p_42)
{ /* block id: 1439 */
    uint8_t l_3066 = 0x94L;
    int32_t l_3099 = 1L;
    int32_t l_3105 = 0L;
    int32_t l_3106 = 0xD3F364CDL;
    uint32_t ***l_3110 = &g_1390;
    int64_t l_3146[3];
    int32_t l_3150 = 0x970A4EBCL;
    int32_t l_3152 = 0xDFEC562FL;
    float l_3153 = (-0x1.2p+1);
    int32_t l_3155 = 0xAA9D6ACBL;
    int32_t l_3156 = (-9L);
    int i;
    for (i = 0; i < 3; i++)
        l_3146[i] = (-1L);
    for (g_218 = 0; (g_218 >= 13); g_218++)
    { /* block id: 1442 */
        uint32_t **l_3073 = (void*)0;
        int64_t *** const *l_3076 = (void*)0;
        int32_t l_3104 = (-5L);
        int32_t l_3107 = 1L;
        (**g_1160) = ((+l_3066) <= (safe_add_func_float_f_f((safe_add_func_float_f_f(((l_3073 = g_3071) == &g_3072), (**g_1160))), ((safe_div_func_float_f_f((((((void*)0 == l_3076) > l_3066) != (safe_div_func_float_f_f((p_42 == (((safe_mod_func_uint8_t_u_u((0x15L || p_42), l_3066)) , 0x8.4p-1) > l_3066)), p_42))) <= (-0x1.Fp-1)), (*g_3051))) > p_42))));
        l_3107 &= (safe_sub_func_uint8_t_u_u(((***g_1549) = ((((safe_lshift_func_uint8_t_u_u((*g_676), (((safe_mod_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(((((**g_93) & (((safe_rshift_func_int16_t_s_u(((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_sub_func_int16_t_s_s((((p_42 & ((l_3099 = 0x2C5C0FAFL) , ((safe_sub_func_int32_t_s_s((((**l_3073) = ((void*)0 != (*g_1160))) , 4L), (safe_mul_func_uint8_t_u_u((((l_3104 | 0xD3A026D9L) ^ g_876[5][6].f0) , 0xE4L), p_42)))) >= 1L))) , l_3099) > p_42), 0x130FL)), l_3066)), 65531UL)) != l_3104), 6)) > 0x50L) <= l_3105)) <= 5UL) >= (**g_1550)), 4)), 65535UL)) , l_3104) != 0x4DL))) | 0x4FDE2088L) < g_153) , l_3106)), 0x45L));
        for (g_732.f0 = 2; (g_732.f0 >= 0); g_732.f0 -= 1)
        { /* block id: 1451 */
            (*g_1274) = (safe_rshift_func_uint8_t_u_u(p_42, 2));
        }
    }
    (**g_1160) = ((void*)0 != l_3110);
    for (g_2955 = 0; (g_2955 >= 15); ++g_2955)
    { /* block id: 1458 */
        int32_t *l_3116 = &l_3099;
        int32_t *l_3117 = &g_237;
        uint32_t ****l_3125 = (void*)0;
        uint32_t *****l_3124 = &l_3125;
        union U1 *l_3136 = &g_3137[4];
        int32_t l_3145 = 0L;
        int32_t l_3147 = 0xBDC8DC7CL;
        int8_t l_3148[3][5][4];
        int32_t l_3149 = 3L;
        int32_t l_3151 = 8L;
        int32_t l_3154 = 0x57940F63L;
        int32_t l_3157[9][3] = {{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)},{0xEA57592FL,(-5L),(-5L)}};
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 5; j++)
            {
                for (k = 0; k < 4; k++)
                    l_3148[i][j][k] = 0xA3L;
            }
        }
        (*l_3117) = ((*l_3116) = ((g_3115 == p_42) > ((void*)0 != &g_1522)));
        for (g_470.f0 = 8; (g_470.f0 <= (-28)); g_470.f0 = safe_sub_func_uint64_t_u_u(g_470.f0, 3))
        { /* block id: 1463 */
            const int16_t l_3131[7][3][3] = {{{0x7D00L,(-3L),0x7D00L},{0xA9C7L,0xAA66L,0x7362L},{0xB5DCL,0xAA66L,7L}},{{0xCD67L,(-3L),0x0C2DL},{(-1L),0xCD67L,0xCD67L},{0xCD67L,0x5608L,9L}},{{0xB5DCL,0x8CCFL,9L},{0xA9C7L,9L,0xCD67L},{0x7D00L,7L,0x0C2DL}},{{9L,9L,7L},{(-3L),0x8CCFL,0x7362L},{(-3L),0x5608L,0x7D00L}},{{9L,0xCD67L,0x8CCFL},{0x7D00L,(-3L),0x7D00L},{0xA9C7L,0xAA66L,0x7362L}},{{0xB5DCL,0xAA66L,7L},{0xCD67L,(-3L),0x0C2DL},{(-1L),0xCD67L,0xCD67L}},{{0xCD67L,0x5608L,9L},{0xB5DCL,0x8CCFL,9L},{0xA9C7L,9L,0xCD67L}}};
            int32_t *l_3140 = &g_80;
            int32_t *l_3141 = &l_3106;
            int32_t *l_3142 = &g_6;
            int32_t *l_3143 = (void*)0;
            int32_t *l_3144[10] = {&g_80,&g_4,&g_80,&g_4,&g_80,&g_4,&g_80,&g_4,&g_80,&g_4};
            uint16_t l_3158 = 0UL;
            int i, j, k;
            (**g_1160) = (safe_mul_func_float_f_f((((safe_div_func_uint8_t_u_u(((g_3126 = l_3124) == (void*)0), (safe_mul_func_int16_t_s_s((l_3131[4][2][0] <= (-1L)), (safe_sub_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u(((*g_1269) == (l_3066 , l_3136)), 8L)), ((safe_sub_func_uint64_t_u_u((p_42 , p_42), p_42)) , l_3131[4][2][0]))))))) <= (*g_676)) , 0x1.Ep+1), p_42));
            --l_3158;
        }
    }
    return p_42;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const uint16_t * func_43(uint32_t  p_44)
{ /* block id: 27 */
    const uint16_t *l_49[10] = {&g_23[0],&g_23[0],&g_23[0],&g_23[0],&g_23[0],&g_23[0],&g_23[0],&g_23[0],&g_23[0],&g_23[0]};
    int i;
    return l_49[5];
}


/* ------------------------------------------ */
/* 
 * reads : g_153 g_1389 g_716 g_717 g_718 g_151 g_477 g_2411 g_2412 g_167 g_237 g_2842 g_2836 g_1551 g_1407 g_2860 g_2480 g_1569 g_421 g_1680 g_80 g_503 g_34 g_676 g_677 g_2932 g_94 g_23 g_951 g_952 g_1160 g_1161 g_2969 g_1549 g_1550 g_111 g_1274 g_2991 g_93 g_218 g_3054 g_6
 * writes: g_153 g_34 g_2831 g_237 g_614 g_151 g_2350 g_421 g_80 g_218 g_2948 g_1162 g_1407 g_111 g_140 g_395 g_2 g_29 g_175 g_3051 g_6
 */
static uint16_t * func_50(int32_t ** p_51, uint16_t * p_52, const int16_t  p_53, int32_t * p_54, uint16_t  p_55)
{ /* block id: 1335 */
    int16_t l_2815 = 8L;
    uint32_t *l_2816 = &g_421;
    uint32_t l_2821[9][4] = {{0x81F41533L,0xFDC4EA94L,0xFDC4EA94L,0x81F41533L},{18446744073709551610UL,0xFDC4EA94L,0UL,0xFDC4EA94L},{0xFDC4EA94L,18446744073709551609UL,0UL,0UL},{18446744073709551610UL,18446744073709551610UL,0xFDC4EA94L,0UL},{0x81F41533L,18446744073709551609UL,0x81F41533L,0xFDC4EA94L},{0x81F41533L,0xFDC4EA94L,0xFDC4EA94L,0x81F41533L},{18446744073709551610UL,0xFDC4EA94L,0UL,0xFDC4EA94L},{0xFDC4EA94L,18446744073709551609UL,0UL,0UL},{18446744073709551610UL,18446744073709551610UL,0xFDC4EA94L,0UL}};
    int32_t l_2824 = 0x64492580L;
    int32_t l_2859 = (-5L);
    int32_t l_2888 = 0x122C1B5EL;
    int32_t l_2891 = 1L;
    int32_t l_2894 = 3L;
    int32_t l_2895 = (-1L);
    int32_t l_2896 = 0x259E5548L;
    int32_t l_2897 = (-2L);
    int32_t l_2899 = 2L;
    int32_t l_2900 = 1L;
    int32_t l_2901 = (-8L);
    int32_t l_2902 = 0L;
    int32_t l_2903 = 0x0812E0A8L;
    int32_t l_2904[8][3][9] = {{{0xCCF18AE6L,1L,0L,(-1L),1L,0L,0x6D7BD940L,0x1733E261L,0x0EBDC27DL},{(-10L),0x19D5D83AL,0x20E2F345L,0x0A1AA622L,0L,(-1L),0L,0x1DACBD87L,0x094938B3L},{0x72F6E8A9L,5L,9L,0xA5898BDFL,(-1L),(-6L),(-6L),(-1L),0xA5898BDFL}},{{0L,0x20E2F345L,0L,0x3AB51CC3L,(-1L),0x66CCC56CL,0x9EC7A19FL,(-10L),0x20E2F345L},{0x0EBDC27DL,0xAACD8CF2L,0x9847A21DL,0x6D7BD940L,(-10L),0xDBE2F618L,0x2E8C3D03L,0x0EBDC27DL,0xBD3DDAF8L},{(-8L),0x3B06C237L,0x05ACAEB5L,0x3AB51CC3L,1L,0x17DCD5BDL,0x568D1BDDL,(-1L),1L}},{{5L,0xD298AB9AL,1L,0xA5898BDFL,1L,0x564856E2L,0x0EBDC27DL,9L,0x2E8C3D03L},{0L,9L,0x8808F982L,0x0A1AA622L,0x8D45E51EL,1L,0x66CCC56CL,0L,0L},{0x257EAE74L,0xA5898BDFL,1L,(-1L),1L,0xA5898BDFL,0x257EAE74L,0xAACD8CF2L,0x723A9E06L}},{{1L,0L,0L,(-8L),0x50FF5A99L,(-1L),0x05ACAEB5L,5L,0x19D5D83AL},{0xBD3DDAF8L,1L,0x257EAE74L,0x9847A21DL,0x26397DBFL,0L,1L,0xAACD8CF2L,1L},{0L,(-10L),(-1L),0L,0x66CCC56CL,(-1L),7L,0L,0L}},{{0x5E59321AL,0x23F44CB6L,(-6L),0xBB62017BL,0x564856E2L,0xE4E8912DL,(-10L),9L,0xA5BC82ECL},{0L,(-1L),(-10L),(-1L),9L,0x50FF5A99L,9L,(-1L),(-10L)},{0L,0L,0L,0x564856E2L,1L,0x8FA15308L,0x26397DBFL,0x0EBDC27DL,0x1733E261L}},{{7L,0x094938B3L,0x20E2F345L,0x3B06C237L,1L,(-8L),(-10L),(-10L),(-1L)},{0x72F6E8A9L,9L,0L,(-4L),0x23F44CB6L,0x26397DBFL,0L,0x2470FCBFL,0x2E8C3D03L},{0x3B06C237L,(-2L),1L,0xE497A635L,0x0907FBA8L,0L,(-1L),5L,(-2L)}},{{1L,0x23F44CB6L,(-10L),1L,1L,0xBFBB16F5L,0x72F6E8A9L,1L,0x72F6E8A9L},{(-1L),(-1L),0x66CCC56CL,0x66CCC56CL,(-1L),(-1L),1L,(-10L),(-1L)},{0xCBB5038DL,0L,0x23F44CB6L,1L,0xD200534FL,(-6L),(-9L),0xBD3DDAF8L,(-1L)}},{{0x58BE7497L,0x8D45E51EL,(-1L),(-1L),0L,0x3AB51CC3L,1L,0x4CDA7271L,1L},{0L,0xBD3DDAF8L,6L,0xBB62017BL,(-9L),0xD298AB9AL,0x72F6E8A9L,0x23F44CB6L,0xCBB5038DL},{0x05ACAEB5L,(-10L),0x20E2F345L,(-10L),0x9EC7A19FL,0x66CCC56CL,(-1L),0x3AB51CC3L,0L}}};
    int64_t ** const l_2944 = &g_718[9][0];
    int32_t *****l_2947 = (void*)0;
    uint32_t l_3005 = 0x6BC4B228L;
    uint32_t l_3037 = 3UL;
    int32_t l_3053 = 6L;
    float l_3055 = (-0x1.0p+1);
    int32_t *l_3056 = &l_2895;
    int32_t *l_3057[10] = {&g_6,&g_6,&g_6,&g_6,&g_6,&g_6,&g_6,&g_6,&g_6,&g_6};
    uint16_t l_3058 = 65531UL;
    int i, j, k;
    for (g_153 = 20; (g_153 != 17); --g_153)
    { /* block id: 1338 */
        uint32_t *l_2818[7];
        int32_t l_2823 = (-1L);
        int64_t **** const *l_2830 = &g_1569;
        const uint32_t *l_2850 = &g_34;
        const uint32_t **l_2849 = &l_2850;
        const uint32_t ***l_2848[1];
        const uint32_t ****l_2847[1][5][3] = {{{&l_2848[0],&l_2848[0],&l_2848[0]},{&l_2848[0],&l_2848[0],&l_2848[0]},{&l_2848[0],&l_2848[0],&l_2848[0]},{&l_2848[0],&l_2848[0],&l_2848[0]},{&l_2848[0],&l_2848[0],&l_2848[0]}}};
        const uint32_t *****l_2846 = &l_2847[0][0][0];
        uint16_t l_2858[1];
        int64_t ****l_2865 = &g_716;
        int16_t *l_2877 = &l_2815;
        int32_t l_2884[2][10][8] = {{{0x1992E7D4L,(-1L),0x4CBBFF88L,0x70646B43L,0x4CBBFF88L,(-1L),0x1992E7D4L,0x2C3ADEEEL},{(-1L),0x31548D36L,0xEF1B6059L,0x53444975L,1L,0x70646B43L,0x70646B43L,1L},{0xA04C0682L,0x1992E7D4L,0x1992E7D4L,0xA04C0682L,1L,0x3F0782A4L,0x2C3ADEEEL,0x70646B43L},{(-1L),9L,0L,1L,0x4CBBFF88L,1L,0L,9L},{0x1992E7D4L,9L,0x70646B43L,0L,0xEF1B6059L,0x3F0782A4L,0x53444975L,0x53444975L},{0x70646B43L,0x1992E7D4L,0x31548D36L,0x31548D36L,0x1992E7D4L,0x70646B43L,0x53444975L,0x4CBBFF88L},{0x2C3ADEEEL,0x31548D36L,0x70646B43L,0x3F0782A4L,0L,(-1L),0L,0x3F0782A4L},{0L,(-1L),0L,0x3F0782A4L,0x70646B43L,0x31548D36L,0x2C3ADEEEL,0x4CBBFF88L},{0x53444975L,0x70646B43L,0x1992E7D4L,0x31548D36L,0x31548D36L,0x1992E7D4L,0x70646B43L,0x53444975L},{0x53444975L,0x3F0782A4L,0xEF1B6059L,0L,0x70646B43L,9L,0x1992E7D4L,9L}},{{0L,1L,0x4CBBFF88L,1L,0L,9L,(-1L),0x70646B43L},{0x2C3ADEEEL,0x3F0782A4L,1L,0xA04C0682L,0x1992E7D4L,0x1992E7D4L,0xA04C0682L,1L},{0x70646B43L,0x70646B43L,(-1L),0x3F0782A4L,0x4CBBFF88L,0x1992E7D4L,0x53444975L,0L},{0xA04C0682L,0x53444975L,0x2C3ADEEEL,1L,0x2C3ADEEEL,0x53444975L,0xA04C0682L,0L},{0x53444975L,0x1992E7D4L,0x4CBBFF88L,0x3F0782A4L,(-1L),1L,1L,(-1L)},{0x70646B43L,0xA04C0682L,0xA04C0682L,0x70646B43L,(-1L),9L,0L,1L},{0x53444975L,0xEF1B6059L,0x31548D36L,(-1L),0x2C3ADEEEL,(-1L),0x31548D36L,0xEF1B6059L},{0xA04C0682L,0xEF1B6059L,1L,0x31548D36L,0x4CBBFF88L,9L,0x3F0782A4L,0x3F0782A4L},{1L,0xA04C0682L,0x1992E7D4L,0x1992E7D4L,0xA04C0682L,1L,0x3F0782A4L,0x2C3ADEEEL},{0L,0x1992E7D4L,1L,9L,0x31548D36L,0x53444975L,0x31548D36L,9L}}};
        int16_t l_2892 = 0xC9C0L;
        int64_t l_2898 = 0x02FCD9DAA3EE56D6LL;
        int8_t l_2930 = 0x5EL;
        uint32_t l_2987[5];
        uint64_t *l_3033 = &g_175[0][1];
        uint64_t *l_3034 = &g_218;
        float *l_3046[7];
        float **l_3047 = &l_3046[2];
        const float *l_3049 = &g_3050;
        const float **l_3048[5][6] = {{&l_3049,&l_3049,&l_3049,&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049,&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049,&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049,&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049,&l_3049,&l_3049,&l_3049}};
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_2818[i] = &g_421;
        for (i = 0; i < 1; i++)
            l_2848[i] = &l_2849;
        for (i = 0; i < 1; i++)
            l_2858[i] = 65535UL;
        for (i = 0; i < 5; i++)
            l_2987[i] = 0x168E7617L;
        for (i = 0; i < 7; i++)
            l_3046[i] = (void*)0;
        if ((0x3CFEL & 0xF7D0L))
        { /* block id: 1339 */
            uint16_t l_2802 = 1UL;
            uint32_t **l_2817 = &l_2816;
            int32_t l_2822 = 0L;
            (*p_54) = ((l_2802 < (((((l_2824 = (safe_div_func_uint32_t_u_u((((((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((safe_rshift_func_uint8_t_u_u((safe_add_func_int64_t_s_s((safe_add_func_uint32_t_u_u(((*g_1389) = l_2815), ((((((((*l_2817) = l_2816) == l_2818[2]) , ((l_2815 ^ l_2815) & 0xBC57L)) && 1UL) , ((((safe_mul_func_int8_t_s_s((l_2822 &= (((0x1DL && 0xAFL) , l_2821[4][3]) < (-8L))), l_2823)) && 65526UL) | 0x37943D17B0823648LL) && l_2823)) <= l_2821[4][3]) >= (***g_716)))), 0x3391936700696F14LL)), 2)) <= (**g_2411)) < 5UL), 8)), p_55)) <= 0xD7L) < 0x1C90L) & l_2823) && p_53), l_2802))) ^ (*p_54)) == 0x43AAL) == 4294967295UL) ^ l_2802)) , (*p_54));
        }
        else
        { /* block id: 1345 */
            int64_t *****l_2837 = &g_1569;
            int32_t l_2839 = 0x4B53EB1AL;
            int32_t l_2890[6] = {0xE26CC0E7L,0x4537965EL,0xE26CC0E7L,0xE26CC0E7L,0x4537965EL,0xE26CC0E7L};
            uint32_t * const l_2954 = &g_2955;
            uint32_t * const *l_2953 = &l_2954;
            int i;
            for (g_34 = 0; (g_34 == 53); ++g_34)
            { /* block id: 1348 */
                uint8_t l_2827 = 5UL;
                int32_t *l_2838 = &g_237;
                int16_t *l_2857 = &g_614;
                l_2827++;
                (*l_2838) |= ((*p_54) = ((g_2831[3] = l_2830) != l_2837));
                if (l_2839)
                    break;
                (*p_54) = (l_2839 = (safe_mul_func_uint16_t_u_u(((g_2842[1][0][3] , ((~3L) ^ (p_55 > (***g_2836)))) < ((*g_1551) < (((((((l_2859 |= (0UL ^ ((safe_add_func_int64_t_s_s(((l_2846 == (void*)0) | ((*l_2857) = (safe_mod_func_int16_t_s_s((safe_div_func_int16_t_s_s((safe_add_func_uint8_t_u_u(p_55, l_2824)), 0x1357L)), 0xE47FL)))), 18446744073709551611UL)) || l_2858[0]))) == l_2824) == 0xE9E5L) == l_2824) ^ 0xB91B4326L) < g_2860) < 0x886F1AE7D3A74AF5LL))), g_2480[0])));
            }
            for (g_151 = 7; (g_151 >= 0); g_151 -= 1)
            { /* block id: 1361 */
                int32_t l_2866 = 0L;
                int32_t l_2885 = 0x813302EEL;
                int32_t l_2886 = (-7L);
                int32_t l_2887 = 9L;
                int32_t l_2889 = (-4L);
                int32_t l_2893[8] = {0x06CF70D5L,0x7CC7522BL,0x06CF70D5L,0x06CF70D5L,0x7CC7522BL,0x06CF70D5L,0x06CF70D5L,0x7CC7522BL};
                uint16_t l_2905 = 65533UL;
                uint16_t l_2931[4] = {65528UL,65528UL,65528UL,65528UL};
                int64_t **l_2943 = (void*)0;
                int i;
                (*p_54) = (*p_54);
                l_2824 &= (~l_2858[0]);
                for (g_2350 = 0; (g_2350 <= 4); g_2350 += 1)
                { /* block id: 1366 */
                    uint32_t l_2862 = 0x8B27D835L;
                    float *l_2879 = &g_387;
                    int32_t l_2880 = 0L;
                    int32_t *l_2882 = &g_80;
                    int32_t *l_2883[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    for (g_421 = 0; (g_421 <= 2); g_421 += 1)
                    { /* block id: 1369 */
                        int16_t *l_2871 = (void*)0;
                        int16_t *l_2872 = &g_614;
                        int16_t **l_2878 = &l_2871;
                        int32_t l_2881 = 0L;
                        int i;
                        l_2862++;
                        l_2866 = ((l_2865 = (*l_2837)) != (*l_2837));
                        (*p_54) &= (((((safe_mod_func_int16_t_s_s(((*l_2872) = g_1680[(g_421 + 2)]), (safe_div_func_int16_t_s_s(((*l_2877) = (((p_53 || ((((l_2858[0] <= (safe_div_func_uint16_t_u_u(((p_53 , (void*)0) == ((*l_2878) = l_2877)), ((((l_2880 = ((-1L) && (l_2879 == (void*)0))) , p_55) >= p_53) ^ l_2881)))) , l_2862) && l_2880) && (-2L))) || l_2862) != p_53)), 4UL)))) < l_2862) || 0UL) , 0x28L) < l_2866);
                    }
                    l_2905--;
                    (*l_2882) &= 0xB2CBF082L;
                }
                for (l_2886 = 0; (l_2886 <= 4); l_2886 += 1)
                { /* block id: 1384 */
                    uint8_t l_2908[7] = {0x61L,0x61L,5UL,0x61L,0x61L,5UL,0x61L};
                    float l_2925 = 0x0.Ep-1;
                    int32_t *****l_2951 = &g_2949;
                    int32_t l_2952 = 0xEE1F8BC0L;
                    uint8_t l_2982 = 0xFEL;
                    int32_t l_2986[7] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
                    int i;
                    for (g_218 = 0; (g_218 <= 4); g_218 += 1)
                    { /* block id: 1387 */
                        ++l_2908[0];
                    }
                    if (((safe_div_func_uint64_t_u_u((safe_div_func_uint8_t_u_u((0UL | g_1680[l_2886]), (safe_rshift_func_int16_t_s_s((((safe_add_func_float_f_f(l_2908[0], (0x2.89DAAAp-99 >= (safe_add_func_float_f_f((safe_div_func_float_f_f(((safe_sub_func_uint32_t_u_u((*g_503), (l_2890[1] || (safe_div_func_int32_t_s_s(((safe_mod_func_int64_t_s_s((l_2930 &= l_2904[7][2][8]), ((1UL >= (p_53 || (*g_676))) || p_55))) || 0xAB60L), l_2895))))) , 0x0.5p-1), p_55)), p_53))))) <= l_2931[0]) , l_2839), p_53)))), 0xED8290607BCA0AF3LL)) , l_2898))
                    { /* block id: 1391 */
                        union U1 **l_2970[8][9][3] = {{{(void*)0,&g_754,&g_1186},{&g_1186,&g_754,&g_754},{(void*)0,&g_754,&g_754},{&g_1186,&g_1186,&g_754},{(void*)0,&g_754,&g_754},{&g_1186,&g_1186,&g_1186},{&g_1186,&g_754,&g_1186},{(void*)0,&g_754,&g_1186},{&g_1186,&g_754,&g_754}},{{(void*)0,&g_754,&g_754},{&g_1186,&g_1186,&g_754},{(void*)0,&g_754,&g_754},{&g_1186,&g_1186,&g_1186},{&g_1186,&g_754,&g_1186},{(void*)0,&g_754,&g_1186},{&g_1186,&g_754,&g_754},{(void*)0,&g_754,&g_754},{&g_1186,&g_1186,&g_754}},{{(void*)0,&g_754,&g_754},{&g_1186,&g_1186,&g_1186},{&g_1186,&g_754,&g_1186},{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754},{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0},{&g_754,(void*)0,&g_754}},{{&g_754,&g_1186,&g_1186},{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754},{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0},{&g_754,(void*)0,&g_754},{&g_754,&g_1186,&g_1186},{&g_754,(void*)0,&g_754}},{{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754},{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0},{&g_754,(void*)0,&g_754},{&g_754,&g_1186,&g_1186},{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754}},{{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0},{&g_754,(void*)0,&g_754},{&g_754,&g_1186,&g_1186},{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754},{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0}},{{&g_754,(void*)0,&g_754},{&g_754,&g_1186,&g_1186},{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754},{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0},{&g_754,(void*)0,&g_754},{&g_754,&g_1186,&g_1186}},{{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0},{&g_1186,&g_1186,&g_754},{&g_1186,(void*)0,&g_754},{&g_754,&g_754,(void*)0},{&g_754,(void*)0,&g_754},{&g_754,&g_1186,&g_1186},{&g_754,(void*)0,&g_754},{&g_1186,(void*)0,(void*)0}}};
                        uint8_t *l_2978 = &g_111[6];
                        int32_t l_2981 = 9L;
                        int32_t *l_2983 = (void*)0;
                        int32_t *l_2984 = &l_2901;
                        int32_t *l_2985[9] = {&l_2903,&l_2893[6],&l_2903,&l_2903,&l_2893[6],&l_2903,&l_2903,&l_2893[6],&l_2903};
                        int i, j, k;
                        (**g_1160) = ((((l_2953 = ((g_2932 , (safe_div_func_int64_t_s_s(((safe_mod_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((*g_94), 0xA256L)), ((*p_54) = l_2858[0]))), ((((safe_lshift_func_uint16_t_u_u(l_2901, (l_2943 == l_2944))) , (((safe_mul_func_int8_t_s_s((l_2952 = ((l_2951 = (g_2948 = l_2947)) != &g_2949)), 0xACL)) ^ p_53) , 65535UL)) || p_55) & l_2890[0]))) , 0xB5ED01C4A41C25ADLL), l_2890[1]))) , (void*)0)) == (*g_951)) ^ (*p_52)) , l_2931[2]);
                        (*p_54) = ((+(safe_mul_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s((safe_rshift_func_int16_t_s_s(((safe_sub_func_int32_t_s_s((((safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s(p_55, ((((g_2969 , (void*)0) != l_2970[4][6][2]) < ((((0UL != (((!l_2890[1]) , ((safe_lshift_func_int8_t_s_s(((++(***g_1549)) < (l_2885 = ((l_2893[0] = (((4294967287UL & (l_2892 < ((safe_add_func_uint8_t_u_u(((*l_2978)--), (p_55 , p_53))) , 0xDB11AD9EL))) || 18446744073709551607UL) >= 0x3892L)) != l_2981))), p_53)) | l_2890[0])) && 1UL)) == p_55) || p_53) && l_2930)) < 0L))), l_2981)) || 0x8296L) == 0xDEF9F9AFL), (*g_1389))) > 1L), 5)), l_2982)) , (*g_1551)), 0xA7L))) != p_55);
                        l_2987[1]--;
                        (*g_1274) |= (~l_2898);
                    }
                    else
                    { /* block id: 1405 */
                        uint32_t l_3004 = 0UL;
                        float *l_3006 = &g_395;
                        float *l_3007 = &l_2925;
                        float *l_3008 = (void*)0;
                        float *l_3009 = &g_2[3];
                        (*p_51) = p_54;
                        (**g_1160) = ((g_2991 , (safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f((((safe_div_func_float_f_f((0x0.Fp+1 <= ((***g_2836) , l_2888)), ((0x1.2p-1 != l_3004) < l_2890[1]))) > (((*l_3009) = ((*l_3007) = ((*l_3006) = (l_3004 != ((p_53 < l_3005) <= 0x0.026E26p-8))))) != p_53)) != p_55), 0x3.0p+1)), 0x5.9EB0D8p+46)), 0xF.A24D97p-7)), p_55)), p_53))) < l_2898);
                    }
                }
            }
            (*p_54) = 0L;
        }
        if ((((**g_1550) = ((&g_218 == &g_1060) , ((l_2818[0] == p_54) >= (*g_94)))) <= (l_2892 ^ p_55)))
        { /* block id: 1417 */
            uint16_t *l_3012 = &g_23[0];
            int32_t l_3024 = 0x11CF2727L;
            for (g_29 = 9; (g_29 >= (-20)); g_29--)
            { /* block id: 1420 */
                if ((*p_54))
                    break;
                if ((*p_54))
                    break;
                return l_3012;
            }
            (*p_54) = ((safe_mul_func_uint16_t_u_u((l_2884[0][6][0] != ((safe_mul_func_int8_t_s_s(((((safe_div_func_int64_t_s_s((p_53 || 5L), (safe_lshift_func_int8_t_s_u(1L, 2)))) , ((*g_676) <= (~l_2884[1][9][3]))) , (((safe_add_func_uint32_t_u_u(0xB0B4D5EDL, (p_55 , p_53))) || p_55) , l_3024)) <= p_55), p_53)) != 0x574EEE03C64B4A77LL)), l_2930)) == l_2884[1][4][3]);
        }
        else
        { /* block id: 1426 */
            return (*g_93);
        }
        (*g_3054) &= ((*p_54) | (safe_sub_func_int64_t_s_s((safe_div_func_int8_t_s_s((-4L), ((safe_mul_func_int16_t_s_s((0x650325E050C3495ALL >= (safe_lshift_func_uint16_t_u_u((l_3037 = (((*l_3033) = p_53) ^ ((*l_3034)++))), (((safe_mod_func_int32_t_s_s((safe_rshift_func_int8_t_s_u(p_55, (l_2884[1][5][6] &= (safe_rshift_func_int8_t_s_s((0x89L > ((safe_sub_func_int8_t_s_s(p_53, (((*l_3047) = l_3046[4]) != (g_3051 = l_2816)))) & (**g_1550))), l_2930))))), l_3053)) > 1L) == p_53)))), p_53)) , p_55))), 1L)));
    }
    ++l_3058;
    return (*g_93);
}


/* ------------------------------------------ */
/* 
 * reads : g_151 g_605.f0 g_1317 g_676 g_677 g_1329 g_6 g_617 g_237 g_1160 g_1161 g_1020 g_837 g_1376 g_753 g_754 g_595 g_717 g_718 g_477 g_2 g_94 g_23 g_1407 g_1156.f0 g_1416 g_139 g_140 g_324 g_325 g_326 g_838.f0 g_111 g_327 g_1269 g_1377 g_732 g_93 g_1389 g_34 g_587.f0 g_716 g_1522 g_174 g_218 g_175 g_217 g_1543 g_1549 g_1274 g_80 g_470.f1 g_503 g_2029 g_2033 g_153
 * writes: g_697 g_1058 g_431 g_595 g_151 g_617 g_395 g_1162 g_614 g_837 g_1377 g_139 g_1388 g_1390 g_153 g_729 g_1407 g_682.f0 g_327 g_1317 g_838.f0 g_111 g_23 g_2 g_421 g_29 g_88 g_1186 g_140 g_174 g_237 g_477 g_1244 g_218 g_677 g_411.f0 g_1549 g_80 g_1569 g_1574
 */
static int32_t ** func_56(int16_t  p_57, const int32_t  p_58, const int16_t  p_59, float  p_60)
{ /* block id: 578 */
    int32_t l_1279 = 0x0D3DB1E2L;
    int32_t *l_1280 = (void*)0;
    int32_t *l_1281[4][3] = {{&g_6,&g_47,&g_6},{&g_6,&g_47,&g_6},{&g_6,&g_47,&g_6},{&g_6,&g_47,&g_6}};
    float l_1282 = (-0x6.8p+1);
    int16_t l_1283 = 0xC058L;
    float l_1284 = (-0x1.Cp-1);
    float l_1285 = (-0x9.Ep+1);
    uint32_t l_1286 = 1UL;
    int32_t * const *l_1292[5][4][3] = {{{(void*)0,&l_1281[1][1],&l_1280},{&g_140,&g_140,&l_1280},{(void*)0,&l_1281[2][2],&l_1281[0][1]},{&l_1281[2][1],&g_140,&l_1281[2][1]}},{{&l_1281[2][0],&l_1281[1][1],&l_1280},{&l_1281[2][1],&l_1281[2][1],(void*)0},{&l_1280,&l_1281[2][2],&l_1280},{(void*)0,&l_1281[2][1],&g_140}},{{&l_1280,(void*)0,&l_1280},{&l_1281[2][1],(void*)0,&l_1280},{(void*)0,(void*)0,(void*)0},{(void*)0,&l_1281[2][1],&l_1280}},{{&l_1281[2][0],&l_1281[2][2],(void*)0},{&l_1280,(void*)0,&l_1280},{&l_1281[0][1],&l_1281[2][1],&l_1280},{&l_1280,&l_1280,&g_140}},{{&l_1281[2][0],&g_140,&l_1280},{(void*)0,&l_1280,(void*)0},{(void*)0,&l_1281[2][1],&l_1280},{&l_1281[2][1],(void*)0,(void*)0}}};
    int32_t * const **l_1291 = &l_1292[3][0][2];
    int32_t * const ***l_1290[2][6][5] = {{{&l_1291,&l_1291,&l_1291,&l_1291,&l_1291},{&l_1291,(void*)0,(void*)0,&l_1291,(void*)0},{&l_1291,&l_1291,&l_1291,&l_1291,&l_1291},{&l_1291,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1291,&l_1291,&l_1291,&l_1291,&l_1291},{(void*)0,(void*)0,&l_1291,(void*)0,(void*)0}},{{&l_1291,&l_1291,&l_1291,&l_1291,&l_1291},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1291,&l_1291,&l_1291,&l_1291,&l_1291},{&l_1291,(void*)0,&l_1291,(void*)0,(void*)0},{&l_1291,&l_1291,&l_1291,&l_1291,&l_1291},{&l_1291,&l_1291,&l_1291,(void*)0,(void*)0}}};
    int32_t * const ****l_1289 = &l_1290[1][2][1];
    uint32_t *l_1297[9];
    uint32_t **l_1296 = &l_1297[4];
    int32_t **l_1385 = &l_1280;
    float **l_1398 = (void*)0;
    float l_1406 = 0xC.0C2116p+8;
    int32_t l_1488[5];
    uint16_t * const *l_1502[7] = {&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94};
    uint16_t * const **l_1501 = &l_1502[1];
    int64_t *l_1510 = &g_1317;
    const int32_t * volatile l_1527[2][5];
    float *l_1544 = &l_1406;
    const int64_t *l_1566 = &g_1317;
    const int64_t **l_1565[9] = {&l_1566,&l_1566,(void*)0,&l_1566,&l_1566,(void*)0,&l_1566,&l_1566,(void*)0};
    const int64_t ***l_1564 = &l_1565[4];
    const int64_t ****l_1563 = &l_1564;
    int64_t ****l_1567[7][6][3] = {{{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{(void*)0,&g_716,&g_716},{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716}},{{&g_716,&g_716,&g_716},{(void*)0,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716}},{{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{(void*)0,&g_716,&g_716},{&g_716,&g_716,(void*)0}},{{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{(void*)0,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716}},{{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716}},{{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,(void*)0}},{{&g_716,&g_716,&g_716},{&g_716,&g_716,(void*)0},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,&g_716},{&g_716,&g_716,(void*)0}}};
    int64_t *****l_1568[1][2];
    const uint8_t **l_1623 = &g_676;
    uint8_t l_1679 = 1UL;
    uint32_t l_1751 = 0xA6343B3AL;
    uint32_t l_1764[2][7][3] = {{{1UL,1UL,0xD7430C9DL},{0xD8C326A1L,0x6EA387C1L,0x6EA387C1L},{0xD7430C9DL,1UL,1UL},{0xD8C326A1L,5UL,0xD8C326A1L},{1UL,0xD7430C9DL,1UL},{18446744073709551612UL,18446744073709551612UL,0x6EA387C1L},{0x672FF68FL,0xD7430C9DL,0xD7430C9DL}},{{0x6EA387C1L,5UL,0x0498804BL},{0x672FF68FL,1UL,0x672FF68FL},{18446744073709551612UL,0x6EA387C1L,0x0498804BL},{1UL,1UL,0xD7430C9DL},{0xD8C326A1L,0x6EA387C1L,0x6EA387C1L},{0xD7430C9DL,1UL,1UL},{0xD8C326A1L,5UL,0xD8C326A1L}}};
    uint16_t l_1780 = 0UL;
    uint16_t l_1859 = 8UL;
    float l_1941 = (-0x1.Bp-1);
    union U0 **l_1969 = &g_837;
    int16_t l_1970[3][4] = {{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)}};
    float l_2022[6][2];
    uint32_t l_2024 = 0x8B2F2FDBL;
    uint32_t *l_2028 = &g_697;
    uint32_t **l_2027[6][3][6] = {{{&l_2028,(void*)0,&l_2028,(void*)0,&l_2028,&l_2028},{&l_2028,&l_2028,&l_2028,&l_2028,&l_2028,(void*)0},{&l_2028,(void*)0,(void*)0,(void*)0,&l_2028,&l_2028}},{{&l_2028,&l_2028,(void*)0,&l_2028,&l_2028,&l_2028},{&l_2028,&l_2028,&l_2028,&l_2028,(void*)0,&l_2028},{&l_2028,(void*)0,&l_2028,&l_2028,&l_2028,&l_2028}},{{(void*)0,&l_2028,&l_2028,(void*)0,(void*)0,&l_2028},{&l_2028,(void*)0,(void*)0,(void*)0,(void*)0,&l_2028},{&l_2028,(void*)0,(void*)0,&l_2028,&l_2028,(void*)0}},{{&l_2028,&l_2028,&l_2028,&l_2028,(void*)0,&l_2028},{&l_2028,(void*)0,&l_2028,&l_2028,&l_2028,&l_2028},{&l_2028,&l_2028,&l_2028,(void*)0,&l_2028,&l_2028}},{{&l_2028,&l_2028,(void*)0,(void*)0,(void*)0,&l_2028},{(void*)0,&l_2028,&l_2028,&l_2028,&l_2028,&l_2028},{(void*)0,&l_2028,&l_2028,&l_2028,&l_2028,(void*)0}},{{(void*)0,&l_2028,&l_2028,&l_2028,&l_2028,&l_2028},{&l_2028,&l_2028,&l_2028,(void*)0,&l_2028,&l_2028},{&l_2028,(void*)0,(void*)0,&l_2028,&l_2028,&l_2028}}};
    uint32_t **l_2035 = &g_1389;
    int32_t *l_2044 = &g_80;
    int32_t ** const l_2043 = &l_2044;
    int32_t ** const *l_2042[9][7][4] = {{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}},{{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043},{&l_2043,&l_2043,&l_2043,&l_2043}}};
    uint16_t l_2093[3];
    float l_2098 = (-0x3.Ep-1);
    uint16_t l_2126 = 1UL;
    uint64_t l_2129 = 18446744073709551615UL;
    int8_t ****l_2164 = &g_2105;
    const int32_t *l_2228 = &g_4;
    const int32_t **l_2227 = &l_2228;
    const int32_t ***l_2226 = &l_2227;
    float l_2383 = 0x5.5p-1;
    int32_t l_2757 = 0L;
    int8_t l_2771 = (-10L);
    int32_t l_2796 = (-1L);
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_1297[i] = &g_421;
    for (i = 0; i < 5; i++)
        l_1488[i] = 0x91E2DEC9L;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
            l_1527[i][j] = &g_237;
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_1568[i][j] = (void*)0;
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
            l_2022[i][j] = 0xF.953BB5p+32;
    }
    for (i = 0; i < 3; i++)
        l_2093[i] = 65527UL;
    ++l_1286;
    if ((l_1289 == (void*)0))
    { /* block id: 580 */
        union U1 *l_1293 = &g_1107;
        int32_t l_1310 = 0x60B9BA2AL;
        int32_t l_1316 = 0x904528E1L;
        float *l_1346 = &g_395;
        int32_t l_1364[2];
        uint16_t **l_1378 = &g_94;
        int32_t ** const l_1383 = &l_1280;
        int32_t **l_1384 = &l_1281[2][1];
        uint64_t l_1426[10][5][5] = {{{0xB3EDF071FB410587LL,0xC744F2B8A8A23C81LL,18446744073709551611UL,18446744073709551609UL,18446744073709551609UL},{0x42D3F873A7A50EBALL,18446744073709551611UL,0x42D3F873A7A50EBALL,0UL,0x383731BA9CC8638CLL},{18446744073709551608UL,0x0054C7C8EAC984DALL,0xDEE1F3028D46F45ALL,0x195A2CEBF596DD43LL,0x5FBFF543DB328434LL},{0x42D3F873A7A50EBALL,0x7FA7C5CD095528CELL,0UL,0xE08ECB6B38BA96CBLL,18446744073709551615UL},{0xB3EDF071FB410587LL,0x360C6DCCB9C7199FLL,0xDEE1F3028D46F45ALL,0x5FBFF543DB328434LL,0x0FC332F221014819LL}},{{0x2C4976CB8F53A9BBLL,0xB72A1CE032CE7539LL,0x42D3F873A7A50EBALL,0xCFD998935D6F4D85LL,18446744073709551615UL},{0x344CD5EF6CF8F5E1LL,0x6FC344F1CBA958A2LL,18446744073709551611UL,0xED5BA8AA489FC0C0LL,0x5FBFF543DB328434LL},{0UL,0xB72A1CE032CE7539LL,18446744073709551615UL,0x383731BA9CC8638CLL,0x383731BA9CC8638CLL},{0x2174CC98736C9CD2LL,0x360C6DCCB9C7199FLL,0x2174CC98736C9CD2LL,0xED5BA8AA489FC0C0LL,18446744073709551609UL},{0x1F4AE327B2B77C1BLL,0x7FA7C5CD095528CELL,0x8E744A2CBF85338CLL,0xCFD998935D6F4D85LL,0xE08ECB6B38BA96CBLL}},{{0x2174CC98736C9CD2LL,0x0054C7C8EAC984DALL,0xB3EDF071FB410587LL,0x5FBFF543DB328434LL,18446744073709551615UL},{0UL,18446744073709551611UL,0x8E744A2CBF85338CLL,0xE08ECB6B38BA96CBLL,18446744073709551610UL},{0x344CD5EF6CF8F5E1LL,0xC744F2B8A8A23C81LL,0x2174CC98736C9CD2LL,0x195A2CEBF596DD43LL,18446744073709551615UL},{0x2C4976CB8F53A9BBLL,18446744073709551611UL,18446744073709551615UL,0UL,0xE08ECB6B38BA96CBLL},{0xB3EDF071FB410587LL,0xC744F2B8A8A23C81LL,18446744073709551611UL,18446744073709551609UL,18446744073709551609UL}},{{0x42D3F873A7A50EBALL,18446744073709551611UL,0x42D3F873A7A50EBALL,0UL,0x383731BA9CC8638CLL},{18446744073709551608UL,0x0054C7C8EAC984DALL,0xDEE1F3028D46F45ALL,0x195A2CEBF596DD43LL,0x5FBFF543DB328434LL},{0x42D3F873A7A50EBALL,0x7FA7C5CD095528CELL,0UL,0xE08ECB6B38BA96CBLL,18446744073709551615UL},{0xB3EDF071FB410587LL,0x360C6DCCB9C7199FLL,0xDEE1F3028D46F45ALL,0x5FBFF543DB328434LL,0x0FC332F221014819LL},{0x2C4976CB8F53A9BBLL,0xB72A1CE032CE7539LL,0x42D3F873A7A50EBALL,0xCFD998935D6F4D85LL,18446744073709551615UL}},{{0x344CD5EF6CF8F5E1LL,0x6FC344F1CBA958A2LL,18446744073709551611UL,0xED5BA8AA489FC0C0LL,18446744073709551611UL},{0x8DE3CD7961F92B58LL,1UL,9UL,0UL,0UL},{0xEEB9D77789404590LL,0xD64A7D0591EFB8CALL,0xEEB9D77789404590LL,0xDEE1F3028D46F45ALL,0xB3EDF071FB410587LL},{0UL,18446744073709551609UL,0xB8B8FE6E2F4EE3C5LL,0x42D3F873A7A50EBALL,18446744073709551615UL},{0xEEB9D77789404590LL,18446744073709551615UL,0xD99863949326CFA4LL,18446744073709551611UL,0xDCDB30D6EF3F38F0LL}},{{0x8DE3CD7961F92B58LL,0x72CA83D378D9A2EALL,0xB8B8FE6E2F4EE3C5LL,18446744073709551615UL,0x2C4976CB8F53A9BBLL},{0x037E761EB12D559ELL,1UL,0xEEB9D77789404590LL,0x2174CC98736C9CD2LL,0xDCDB30D6EF3F38F0LL},{1UL,18446744073709551613UL,9UL,0x8E744A2CBF85338CLL,18446744073709551615UL},{0xD99863949326CFA4LL,1UL,0x00D99FAE62BCE7C2LL,0xB3EDF071FB410587LL,0xB3EDF071FB410587LL},{18446744073709551612UL,0x72CA83D378D9A2EALL,18446744073709551612UL,0x8E744A2CBF85338CLL,0UL}},{{1UL,18446744073709551615UL,0x50A12F953A1D7560LL,0x2174CC98736C9CD2LL,18446744073709551611UL},{18446744073709551612UL,18446744073709551609UL,0x8DE3CD7961F92B58LL,18446744073709551615UL,0x5FFD705FB4953016LL},{0xD99863949326CFA4LL,0xD64A7D0591EFB8CALL,0x50A12F953A1D7560LL,18446744073709551611UL,0x344CD5EF6CF8F5E1LL},{1UL,1UL,18446744073709551612UL,0x42D3F873A7A50EBALL,0x5FFD705FB4953016LL},{0x037E761EB12D559ELL,0UL,0x00D99FAE62BCE7C2LL,0xDEE1F3028D46F45ALL,18446744073709551611UL}},{{0x8DE3CD7961F92B58LL,1UL,9UL,0UL,0UL},{0xEEB9D77789404590LL,0xD64A7D0591EFB8CALL,0xEEB9D77789404590LL,0xDEE1F3028D46F45ALL,0xB3EDF071FB410587LL},{0UL,18446744073709551609UL,0xB8B8FE6E2F4EE3C5LL,0x42D3F873A7A50EBALL,18446744073709551615UL},{0xEEB9D77789404590LL,18446744073709551615UL,0xD99863949326CFA4LL,18446744073709551611UL,0xDCDB30D6EF3F38F0LL},{0x8DE3CD7961F92B58LL,0x72CA83D378D9A2EALL,0xB8B8FE6E2F4EE3C5LL,18446744073709551615UL,0x2C4976CB8F53A9BBLL}},{{0x037E761EB12D559ELL,1UL,0xEEB9D77789404590LL,0x2174CC98736C9CD2LL,0xDCDB30D6EF3F38F0LL},{1UL,18446744073709551613UL,9UL,0x8E744A2CBF85338CLL,18446744073709551615UL},{0xD99863949326CFA4LL,1UL,0x00D99FAE62BCE7C2LL,0xB3EDF071FB410587LL,0xB3EDF071FB410587LL},{18446744073709551612UL,0x72CA83D378D9A2EALL,18446744073709551612UL,0x8E744A2CBF85338CLL,0UL},{1UL,18446744073709551615UL,0x50A12F953A1D7560LL,0x2174CC98736C9CD2LL,18446744073709551611UL}},{{18446744073709551612UL,18446744073709551609UL,0x8DE3CD7961F92B58LL,18446744073709551615UL,0x5FFD705FB4953016LL},{0xD99863949326CFA4LL,0xD64A7D0591EFB8CALL,0x50A12F953A1D7560LL,18446744073709551611UL,0x344CD5EF6CF8F5E1LL},{1UL,1UL,18446744073709551612UL,0x42D3F873A7A50EBALL,0x5FFD705FB4953016LL},{0x037E761EB12D559ELL,0UL,0x00D99FAE62BCE7C2LL,0xDEE1F3028D46F45ALL,18446744073709551611UL},{0x8DE3CD7961F92B58LL,1UL,9UL,0UL,0UL}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1364[i] = (-7L);
        for (g_697 = 0; g_697 < 2; g_697 += 1)
        {
            for (g_1058 = 0; g_1058 < 10; g_1058 += 1)
            {
                for (g_431 = 0; g_431 < 6; g_431 += 1)
                {
                    union U1 tmp = {0L};
                    g_595[g_697][g_1058][g_431] = tmp;
                }
            }
        }
        l_1293 = l_1293;
        for (g_151 = 24; (g_151 >= 7); --g_151)
        { /* block id: 585 */
            int8_t *l_1298 = &g_617[1];
            int8_t *l_1315[9][6][4] = {{{&g_174[2],(void*)0,&g_420,&g_29},{&g_420,&g_29,&g_29,&g_174[1]},{&g_420,&g_174[0],&g_174[3],&g_174[2]},{(void*)0,(void*)0,&g_174[1],&g_420},{&g_420,&g_174[2],&g_420,&g_29},{&g_29,&g_29,&g_420,(void*)0}},{{&g_29,(void*)0,(void*)0,&g_29},{&g_420,&g_420,(void*)0,&g_174[1]},{&g_29,&g_29,&g_420,&g_29},{&g_29,(void*)0,&g_420,&g_174[2]},{&g_420,&g_174[2],&g_174[1],&g_174[1]},{&g_420,&g_420,&g_420,(void*)0}},{{&g_420,&g_420,&g_174[3],&g_420},{&g_420,(void*)0,(void*)0,&g_174[3]},{&g_174[2],(void*)0,&g_174[0],&g_420},{(void*)0,&g_420,&g_174[3],(void*)0},{&g_174[1],&g_420,&g_420,&g_174[2]},{(void*)0,&g_174[2],&g_174[0],&g_174[2]}},{{&g_174[1],&g_420,&g_174[1],&g_29},{&g_420,&g_174[1],&g_174[2],&g_174[2]},{&g_420,&g_420,&g_420,(void*)0},{&g_420,&g_420,&g_174[2],&g_420},{&g_420,(void*)0,&g_174[1],&g_174[3]},{&g_174[1],(void*)0,&g_174[0],&g_420}},{{(void*)0,&g_420,&g_420,(void*)0},{&g_174[1],&g_420,&g_174[3],&g_174[2]},{(void*)0,&g_29,&g_174[0],&g_174[1]},{&g_174[2],&g_420,(void*)0,&g_174[1]},{&g_420,&g_29,&g_174[3],&g_174[2]},{&g_420,&g_420,&g_420,(void*)0}},{{&g_420,&g_420,&g_174[2],&g_420},{&g_420,(void*)0,&g_420,&g_174[3]},{&g_29,(void*)0,&g_174[0],&g_420},{(void*)0,&g_420,&g_420,(void*)0},{&g_174[1],&g_420,&g_420,&g_174[2]},{(void*)0,&g_174[1],&g_174[0],&g_29}},{{&g_29,&g_420,&g_420,&g_174[2]},{&g_420,&g_174[2],&g_174[2],&g_174[2]},{&g_420,&g_420,&g_420,(void*)0},{&g_420,&g_420,&g_174[3],&g_420},{&g_420,(void*)0,(void*)0,&g_174[3]},{&g_174[2],(void*)0,&g_174[0],&g_420}},{{(void*)0,&g_420,&g_174[3],(void*)0},{&g_174[1],&g_420,&g_420,&g_174[2]},{(void*)0,&g_174[2],&g_174[0],&g_174[2]},{&g_174[1],&g_420,&g_174[1],&g_29},{&g_420,&g_174[1],&g_174[2],&g_174[2]},{&g_420,&g_420,&g_420,(void*)0}},{{&g_420,&g_420,&g_174[2],&g_420},{&g_420,(void*)0,&g_174[1],&g_174[3]},{&g_174[1],(void*)0,&g_174[0],&g_420},{(void*)0,&g_420,&g_420,(void*)0},{&g_174[1],&g_420,&g_174[3],&g_174[2]},{(void*)0,&g_29,&g_174[0],&g_174[1]}}};
            uint32_t l_1318 = 0UL;
            int32_t l_1319[4] = {0L,0L,0L,0L};
            uint16_t l_1347[7][8] = {{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL},{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL},{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL},{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL},{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL},{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL},{65535UL,0x457DL,65535UL,65527UL,65535UL,0x457DL,65535UL,65527UL}};
            int32_t l_1348 = 0x010689A4L;
            int32_t l_1354[8][5] = {{0xCA22A564L,3L,0xA7ADA152L,8L,0xA7ADA152L},{0xCA22A564L,0xCA22A564L,(-1L),8L,(-1L)},{3L,0xCA22A564L,0xA7ADA152L,0x6A4F6701L,(-1L)},{0xCA22A564L,3L,0xA7ADA152L,8L,0xA7ADA152L},{0xCA22A564L,0xCA22A564L,(-1L),8L,(-1L)},{3L,0xCA22A564L,0xA7ADA152L,0x6A4F6701L,(-1L)},{0xCA22A564L,3L,0xA7ADA152L,8L,0xA7ADA152L},{0xCA22A564L,0xCA22A564L,(-1L),8L,(-1L)}};
            int32_t ***l_1357 = &g_139;
            int32_t ****l_1356 = &l_1357;
            int32_t *****l_1355 = &l_1356;
            union U1 *l_1358 = &g_595[1][4][2];
            int64_t **** const l_1431 = &g_716;
            int i, j, k;
            if ((((*l_1298) = (l_1296 != &l_1297[4])) <= ((g_605.f0 & 0x53E0L) > (safe_sub_func_uint8_t_u_u(p_59, (safe_lshift_func_uint16_t_u_s((((l_1319[1] &= (safe_lshift_func_uint8_t_u_s((safe_div_func_int32_t_s_s((+(safe_mul_func_uint16_t_u_u(l_1310, ((safe_sub_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s((((l_1316 = p_58) ^ (&g_677 == &g_111[7])) && g_1317), 1)) & l_1318), (*g_676))) , l_1318)))), l_1318)), p_57))) | l_1318) | l_1318), p_59)))))))
            { /* block id: 589 */
                int32_t l_1333[4] = {4L,4L,4L,4L};
                float *l_1345 = &g_387;
                float **l_1344 = &l_1345;
                int i;
                (**g_1160) = (p_57 , (((safe_add_func_float_f_f((p_57 <= (-l_1318)), (p_58 , ((safe_add_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f(g_1329, ((-(safe_div_func_float_f_f(((-0x5.Cp+1) >= l_1333[3]), ((((((safe_mul_func_float_f_f((l_1310 < (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((*l_1346) = (((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((((*l_1344) = (void*)0) == l_1346), g_6)) <= p_60), g_605.f0)) != p_57) > 0x5.A2EE4Bp+74)), 0x3.7p-1)), p_60))), 0x1.Dp+1)) >= l_1316) <= 0xF.43D38Fp+91) < l_1333[3]) <= g_617[0]) > (-0x4.5p+1))))) <= g_237))), p_60)), 0x8.9FDC77p-1)) <= 0x4.D34765p-35)))) <= l_1347[1][3]) < 0xD.539801p-9));
                l_1348 ^= ((1L & 0xE3387D3FL) & l_1319[1]);
            }
            else
            { /* block id: 594 */
                int16_t *l_1351 = &g_614;
                int32_t l_1362[7] = {0L,0L,0x3BA27908L,0L,0L,0x3BA27908L,0L};
                uint8_t l_1371 = 0xE3L;
                int i;
                l_1354[2][3] |= (l_1319[1] = (safe_mul_func_int16_t_s_s((((*l_1351) = p_57) >= l_1348), (safe_mul_func_int16_t_s_s((0x3D97FD39446FEC59LL | p_58), 0x5430L)))));
                if (((void*)0 != l_1355))
                { /* block id: 598 */
                    union U1 **l_1359 = &l_1358;
                    int32_t l_1360 = 0xA0EB2412L;
                    int8_t l_1361 = (-9L);
                    int32_t l_1363 = (-1L);
                    int32_t l_1365 = 1L;
                    int32_t l_1366 = 0x1D1DAA88L;
                    int32_t l_1367 = 0x4DCF81A4L;
                    int32_t l_1368 = 0xC621C247L;
                    int32_t l_1369 = 1L;
                    int32_t l_1370 = (-1L);
                    (*l_1359) = l_1358;
                    (**g_1160) = p_60;
                    l_1371++;
                    if (p_59)
                        break;
                }
                else
                { /* block id: 603 */
                    union U0 **l_1374 = &g_837;
                    uint16_t ***l_1379 = &l_1378;
                    int32_t l_1380 = 0x7597FF4AL;
                    (*g_1376) = ((*l_1374) = (*g_1020));
                    (*l_1379) = l_1378;
                    l_1380 &= 0xB524E11BL;
                    return &g_140;
                }
                if (p_59)
                    break;
            }
            if (((0x623B2840L || 0x60FF61C2L) < (l_1383 == (l_1385 = ((**l_1356) = l_1384)))))
            { /* block id: 614 */
                uint32_t **l_1386 = &l_1297[2];
                uint32_t ***l_1387[2][9] = {{(void*)0,&l_1296,&l_1296,(void*)0,(void*)0,&l_1296,&l_1296,(void*)0,(void*)0},{&l_1386,&l_1296,&l_1386,&l_1296,&l_1386,&l_1296,&l_1386,&l_1296,&l_1386}};
                int32_t l_1405 = 0xBCBCBC7CL;
                int16_t *l_1408 = &g_153;
                int32_t l_1415 = 0L;
                uint16_t l_1417[9][10][2] = {{{0xD5F5L,0x944EL},{7UL,0xADEAL},{8UL,8UL},{65529UL,0x9C00L},{0xADEAL,65535UL},{0xA141L,0x6437L},{1UL,0xA141L},{0x2515L,0x4BD1L},{0x2515L,0xA141L},{1UL,0x6437L}},{{0xA141L,65535UL},{0xADEAL,0x9C00L},{65529UL,8UL},{8UL,0xADEAL},{7UL,0x944EL},{0xD5F5L,65535UL},{65535UL,0x9A5AL},{65527UL,0x2281L},{0xFBE2L,0x41BAL},{0UL,0xD446L}},{{65535UL,0x2515L},{2UL,0xD5F5L},{65534UL,0xD5F5L},{2UL,0x2515L},{65535UL,0xD446L},{0UL,0x41BAL},{0xFBE2L,0x2281L},{65527UL,0x9A5AL},{65535UL,65535UL},{0xD5F5L,0x944EL}},{{7UL,0xADEAL},{8UL,8UL},{65529UL,0x9C00L},{0xADEAL,65535UL},{0xA141L,0x6437L},{1UL,0xA141L},{0x2515L,0x4BD1L},{0x2515L,0xA141L},{1UL,0x6437L},{0xA141L,65535UL}},{{0xADEAL,0xA876L},{0UL,8UL},{8UL,2UL},{0x6437L,0x9C00L},{0xFBE2L,65534UL},{0x41BAL,0x0E53L},{0x9A5AL,65535UL},{4UL,0xA141L},{0xADEAL,0x2515L},{65534UL,0x944EL}},{{0xD446L,0xFBE2L},{65527UL,0xFBE2L},{0xD446L,0x944EL},{65534UL,0x2515L},{0xADEAL,0xA141L},{4UL,65535UL},{0x9A5AL,0x0E53L},{0x41BAL,65534UL},{0xFBE2L,0x9C00L},{0x6437L,2UL}},{{8UL,8UL},{0UL,0xA876L},{2UL,0x41BAL},{1UL,0x4BD1L},{0xD5F5L,1UL},{0x944EL,8UL},{0x944EL,1UL},{0xD5F5L,0x4BD1L},{1UL,0x41BAL},{2UL,0xA876L}},{{0UL,8UL},{8UL,2UL},{0x6437L,0x9C00L},{0xFBE2L,65534UL},{0x41BAL,0x0E53L},{0x9A5AL,65535UL},{4UL,0xA141L},{0xADEAL,0x2515L},{65534UL,0x944EL},{0xD446L,0xFBE2L}},{{65527UL,0xFBE2L},{0xD446L,0x944EL},{65534UL,0x2515L},{0xADEAL,0xA141L},{4UL,65535UL},{0x9A5AL,0x0E53L},{0x41BAL,65534UL},{0xFBE2L,0x9C00L},{0x6437L,2UL},{8UL,8UL}}};
                int32_t l_1423[8];
                int i, j, k;
                for (i = 0; i < 8; i++)
                    l_1423[i] = (-1L);
                if (((&l_1297[2] == (g_1390 = (g_1388 = l_1386))) <= (((*l_1408) = (safe_mul_func_uint8_t_u_u((p_59 | ((+((**g_753) , (0xA245A4A2E2B917C6LL != (safe_div_func_uint64_t_u_u((safe_add_func_int64_t_s_s((**g_717), (((*l_1358) , l_1398) == ((((safe_mod_func_int8_t_s_s(((safe_add_func_int64_t_s_s((safe_div_func_uint64_t_u_u(((g_2[9] , p_59) < l_1405), (**g_717))), p_58)) != p_57), 0xD5L)) ^ (*g_94)) >= p_59) , (void*)0)))), 0x06A97C8322221BF1LL))))) < 0L)), g_1407))) && g_1156[0][2].f0)))
                { /* block id: 618 */
                    uint8_t l_1412 = 0xE0L;
                    int32_t l_1424[7];
                    int32_t l_1425[7][5][7] = {{{0xE3DEB70FL,0x8ABB4DD2L,0xD232FBB6L,0x748338A7L,(-3L),0x5B444F72L,(-5L)},{0L,(-1L),0xA62CAA4FL,(-1L),6L,0x91DBDACEL,(-5L)},{(-4L),0x292563F9L,0xEFCD2B26L,0x5F538FE1L,(-5L),(-1L),0x0BA0C5D4L},{(-1L),0x0315AD35L,0x91DBDACEL,0xDBF3FFA6L,0x292563F9L,0xA256C580L,0x0315AD35L},{0x818E6393L,0x37F02054L,0xD5C7228CL,0x3B21078CL,0xC26946D8L,0x3BD418ADL,0x07DD6FF8L}},{{1L,0x079F4988L,0x74AB04A0L,0x6A93920DL,0x748338A7L,0xD1C27162L,(-6L)},{4L,6L,(-1L),(-1L),6L,4L,(-1L)},{0x74AB04A0L,1L,0x3B21078CL,0x9EF0CA68L,0xEDB7AB7DL,7L,(-1L)},{(-1L),0xF42BAD09L,0x37F02054L,0L,0x4BBACA0FL,0xD6E303C3L,0L},{0x3BD418ADL,1L,0xD5C7228CL,(-3L),0x3775AD47L,0xC26946D8L,0xEFCD2B26L}},{{0x5B444F72L,6L,(-1L),0x748338A7L,0x0BA0C5D4L,0x78923D1DL,0xCFF14D32L},{0x07DD6FF8L,0x079F4988L,0x95792450L,0x89E08F24L,8L,(-1L),0L},{0x74AB04A0L,0x37F02054L,0L,0xE3DEB70FL,(-1L),1L,0x4BBACA0FL},{0x4BBACA0FL,0x0BA0C5D4L,3L,8L,0L,0x4DF4401DL,0x748338A7L},{0x91DBDACEL,0x78923D1DL,0x079F4988L,3L,0x4DF4401DL,0x6A93920DL,(-1L)}},{{0xDBF3FFA6L,0xA256C580L,0xA62CAA4FL,3L,7L,(-3L),0x80828EA0L},{0x5B444F72L,0L,0x7597E211L,8L,0xC26946D8L,0L,0xABD15A07L},{0x77B0077BL,0x973613BCL,(-1L),0xD5C7228CL,(-4L),0x8C773E95L,0x4850C525L},{0x0BA0C5D4L,1L,(-4L),0x8ABB4DD2L,3L,(-1L),0x292563F9L},{1L,0x5F538FE1L,0xCFF14D32L,0xD1C27162L,0L,0L,0xD1C27162L}},{{0x426F907FL,0xA256C580L,0x426F907FL,(-1L),(-1L),0L,(-10L)},{0xE3DEB70FL,(-2L),(-1L),0x557DBE4EL,(-10L),0x4850C525L,0xC26946D8L},{(-1L),0x4850C525L,0x5F538FE1L,1L,(-2L),0L,0x4850C525L},{0x292563F9L,0x07DD6FF8L,0x74AB04A0L,0xEDB7AB7DL,0x78923D1DL,0L,3L},{0x91DBDACEL,0xE3DEB70FL,0L,(-4L),0xEFCD2B26L,(-1L),0x74AB04A0L}},{{0x8C773E95L,0xF42BAD09L,(-1L),0x5B444F72L,(-1L),0x8C773E95L,0xA256C580L},{7L,0x80828EA0L,0xEDB7AB7DL,(-1L),0x0315AD35L,0L,0xF42BAD09L},{4L,0xD1C27162L,7L,0x8C773E95L,(-1L),(-3L),3L},{0L,0x37F02054L,0x74AB04A0L,0xEE6D9D07L,0xE3DEB70FL,0x6A93920DL,(-1L)},{0L,(-1L),0xBA65012DL,0xD1C27162L,0x292563F9L,0x4DF4401DL,0x5B444F72L}},{{4L,(-4L),0x89E08F24L,(-1L),7L,3L,(-1L)},{7L,(-1L),0xAC3B3365L,(-6L),0x3775AD47L,(-1L),(-1L)},{0x8C773E95L,0x4850C525L,3L,2L,0xABD15A07L,(-6L),(-1L)},{0x91DBDACEL,0x6A93920DL,(-4L),0xCFF14D32L,(-4L),0x6A93920DL,0x91DBDACEL},{0x292563F9L,(-1L),0xEE6D9D07L,3L,0x07DD6FF8L,0x748338A7L,0x74AB04A0L}}};
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_1424[i] = 0xD0C0C014L;
                    for (g_729 = 0; (g_729 <= 2); g_729 += 1)
                    { /* block id: 621 */
                        int64_t l_1409[5][1][5] = {{{0xD5E19A05DE23F863LL,0L,0xD5E19A05DE23F863LL,0L,0xD5E19A05DE23F863LL}},{{3L,3L,3L,3L,3L}},{{0xD5E19A05DE23F863LL,0L,0xD5E19A05DE23F863LL,0L,0xD5E19A05DE23F863LL}},{{3L,3L,3L,3L,3L}},{{0xD5E19A05DE23F863LL,0L,0xD5E19A05DE23F863LL,0L,0xD5E19A05DE23F863LL}}};
                        int32_t l_1410 = 1L;
                        int32_t l_1411[4] = {4L,4L,4L,4L};
                        int i, j, k;
                        l_1412++;
                        l_1405 = l_1415;
                        (*l_1346) = g_1416[2][2];
                        (**g_1160) = l_1417[7][4][0];
                    }
                    for (g_1407 = 0; (g_1407 > 25); g_1407++)
                    { /* block id: 629 */
                        if (p_57)
                            break;
                        if (p_57)
                            break;
                    }
                    for (g_682.f0 = 0; (g_682.f0 < 14); ++g_682.f0)
                    { /* block id: 635 */
                        int32_t *l_1422[9][9] = {{&l_1319[0],&g_6,&l_1319[0],&g_237,&l_1364[0],&l_1316,(void*)0,&l_1405,(void*)0},{(void*)0,&g_47,&l_1310,&l_1310,&l_1310,&g_47,(void*)0,&g_237,(void*)0},{&l_1319[0],&l_1364[1],&l_1319[3],&l_1364[0],&l_1316,&l_1316,&l_1316,&l_1364[0],&l_1319[3]},{&l_1310,&l_1310,&l_1354[2][3],&l_1316,(void*)0,&l_1354[2][3],&g_47,&g_237,&g_47},{&l_1319[3],(void*)0,&l_1364[1],&l_1364[1],(void*)0,&l_1319[3],&g_237,(void*)0,&l_1364[0]},{&g_237,&l_1316,&l_1354[2][3],&l_1415,(void*)0,(void*)0,&l_1415,&l_1354[2][3],&l_1316},{(void*)0,&l_1415,&l_1319[3],&l_1316,&l_1405,&l_1364[0],&g_237,&g_237,&l_1364[0]},{(void*)0,&l_1354[2][3],&l_1310,&l_1354[2][3],(void*)0,&l_1319[1],&g_47,&l_1310,&l_1415},{&l_1316,&l_1415,&l_1316,&g_6,(void*)0,&g_6,&l_1316,&l_1415,&l_1316}};
                        int i, j;
                        (*l_1385) = l_1422[2][8];
                    }
                    --l_1426[8][1][0];
                }
                else
                { /* block id: 639 */
                    (**g_1160) = (safe_div_func_float_f_f(p_57, ((*l_1346) = (&g_716 != l_1431))));
                }
                l_1415 ^= (((l_1423[4] = 7L) == p_58) < p_59);
            }
            else
            { /* block id: 645 */
                return &g_140;
            }
        }
    }
    else
    { /* block id: 649 */
        int64_t l_1435 = (-1L);
        union U0 **l_1440 = &g_837;
        int32_t l_1476 = 0xEB12F886L;
        int32_t l_1477 = 0x024957ABL;
        int32_t l_1479 = (-1L);
        int32_t l_1482 = 0xC9654CEAL;
        int32_t l_1483[4] = {(-1L),(-1L),(-1L),(-1L)};
        int64_t l_1484 = 0x35CC779038AFC1B3LL;
        int8_t l_1487 = 1L;
        int32_t *l_1493[1];
        uint32_t ***l_1495 = &g_1388;
        float *l_1545 = &g_395;
        int i;
        for (i = 0; i < 1; i++)
            l_1493[i] = (void*)0;
        if ((safe_rshift_func_uint16_t_u_u((!(1L & l_1435)), 2)))
        { /* block id: 650 */
            uint8_t l_1436 = 0xC7L;
            uint16_t l_1469[3][6][8] = {{{65526UL,0UL,8UL,0x928FL,0xCE77L,0x823DL,1UL,1UL},{0x928FL,1UL,8UL,0xDF99L,0xBA0CL,0xCE77L,0x0F34L,0UL},{0xCE77L,0xFD45L,0xC341L,65530UL,65535UL,0x47D5L,1UL,0xE02CL},{65535UL,0xAD30L,0x87B5L,65535UL,5UL,5UL,65535UL,0x87B5L},{3UL,3UL,0x6AC5L,65526UL,0xE02CL,0x0667L,65535UL,0xDF99L},{0UL,5UL,1UL,0UL,8UL,0x9E37L,0x47D5L,0xDF99L}},{{5UL,0xDF99L,1UL,65526UL,65535UL,0xC341L,0x9E37L,0x87B5L},{0xBA0CL,0UL,3UL,65535UL,0x823DL,1UL,65526UL,0xE02CL},{0xE02CL,0UL,0x9E37L,65530UL,0x9E37L,0UL,0xE02CL,0UL},{0x31E6L,65535UL,0xDEE4L,0xDF99L,0UL,8UL,0xAD30L,1UL},{0x87B5L,65526UL,0UL,0x928FL,0UL,0x6909L,65534UL,65530UL},{0x31E6L,65535UL,65535UL,1UL,0x9E37L,0x0F34L,1UL,65534UL}},{{0xE02CL,0x6AC5L,0x0F34L,8UL,0x823DL,0UL,0UL,0x823DL},{0xBA0CL,0x47D5L,0x47D5L,0xBA0CL,65535UL,0x928FL,0xDF99L,0xC341L},{5UL,0x87B5L,0UL,65535UL,65530UL,65534UL,0x6909L,0UL},{0x6909L,1UL,5UL,0UL,0x31E6L,0xE02CL,1UL,8UL},{0xC341L,3UL,0xBA0CL,65526UL,0x9E37L,0x928FL,0xFD45L,5UL},{0xBA0CL,65534UL,0xE02CL,65535UL,0xCE77L,65535UL,0xE02CL,65534UL}}};
            int32_t l_1471 = 0x06981E0BL;
            int32_t l_1478 = 0xDC910B2FL;
            int32_t l_1486 = 1L;
            uint32_t l_1490[5] = {0x1D0E697FL,0x1D0E697FL,0x1D0E697FL,0x1D0E697FL,0x1D0E697FL};
            int i, j, k;
            (***g_324) = (*g_139);
            for (g_1317 = 9; (g_1317 >= 0); g_1317 -= 1)
            { /* block id: 654 */
                float l_1454 = 0x3.A30A1Cp-53;
                int32_t l_1466 = (-1L);
                int32_t l_1475 = 0L;
                int32_t l_1480 = 0x5E9C2AEBL;
                int32_t l_1481 = (-1L);
                int32_t l_1485 = (-3L);
                int32_t l_1489[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_1489[i] = 0x70A8E139L;
                for (g_682.f0 = 9; (g_682.f0 >= 2); g_682.f0 -= 1)
                { /* block id: 657 */
                    float l_1465[3];
                    int32_t l_1467[5][5];
                    int32_t l_1472 = (-3L);
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1465[i] = (-0x1.4p-1);
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_1467[i][j] = (-1L);
                    }
                    for (g_838.f0 = 1; (g_838.f0 >= 0); g_838.f0 -= 1)
                    { /* block id: 660 */
                        uint8_t *l_1443 = &g_111[2];
                        int32_t l_1453 = 1L;
                        uint8_t *l_1468 = &l_1436;
                        int16_t *l_1470 = &g_153;
                        int i, j, k;
                        --l_1436;
                        l_1472 &= (l_1471 |= ((safe_unary_minus_func_uint16_t_u(((g_595[g_838.f0][(g_838.f0 + 4)][(g_838.f0 + 3)] , l_1440) != &g_1377))) ^ ((*l_1470) = ((((safe_rshift_func_uint8_t_u_s(((*l_1443) |= (*g_676)), 6)) <= (safe_mod_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((~(l_1453 <= (((*g_94) = (*g_94)) , l_1453))), (safe_mod_func_int8_t_s_s((&g_324 == (((safe_lshift_func_int16_t_s_u((safe_mul_func_int16_t_s_s((((safe_div_func_int64_t_s_s((safe_add_func_uint16_t_u_u((((((*l_1468) |= (((p_57 != l_1466) , l_1453) != l_1467[1][0])) , &g_951) == (void*)0) || p_59), l_1469[0][2][6])), 0x69A4B03D65DE5C50LL)) ^ (-5L)) ^ l_1467[4][0]), p_57)), 15)) > l_1466) , (void*)0)), 1L)))), p_58)), 3)), 65530UL))) > 0L) == p_57))));
                        g_2[g_1317] = 0x3.B5409Dp+53;
                    }
                    return &g_140;
                }
                for (g_421 = 0; (g_421 <= 1); g_421 += 1)
                { /* block id: 674 */
                    int8_t l_1473 = 0x4FL;
                    int32_t l_1474[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1474[i] = 0L;
                    ++l_1490[3];
                    for (g_1407 = 0; (g_1407 <= 3); g_1407 += 1)
                    { /* block id: 678 */
                        uint32_t l_1494 = 0xE01C3656L;
                        l_1494 = ((l_1493[0] = &l_1474[0]) == (**g_325));
                    }
                }
                for (g_29 = 0; (g_29 <= 9); g_29 += 1)
                { /* block id: 685 */
                    uint32_t ****l_1496 = &l_1495;
                    int32_t *l_1497 = &l_1479;
                    uint16_t * const **l_1504[9] = {&l_1502[1],&l_1502[1],&l_1502[1],&l_1502[1],&l_1502[1],&l_1502[1],&l_1502[1],&l_1502[1],&l_1502[1]};
                    int i;
                    for (g_88 = 0; (g_88 <= 9); g_88 += 1)
                    { /* block id: 688 */
                        if (p_58)
                            break;
                        (*g_1269) = (void*)0;
                    }
                    (*g_326) = ((((*l_1496) = l_1495) == (void*)0) , &l_1479);
                    for (l_1481 = 1; (l_1481 >= 0); l_1481 -= 1)
                    { /* block id: 696 */
                        float l_1500 = 0x4.0A533Cp+5;
                        uint16_t * const ***l_1503 = (void*)0;
                        int32_t l_1505 = 0x813D4895L;
                        (***g_324) = ((*g_139) = l_1497);
                        (*g_140) &= (l_1486 = (safe_mul_func_uint16_t_u_u(((*g_1377) , (((-10L) > p_58) == (((**g_93) = 2UL) >= (l_1481 < (p_58 ^ ((l_1504[8] = l_1501) == &l_1502[2])))))), l_1505)));
                    }
                }
            }
            l_1486 = 0x5D9AAEE4L;
        }
        else
        { /* block id: 707 */
            int64_t **l_1511 = &l_1510;
            uint64_t l_1523[8] = {0UL,18446744073709551613UL,0UL,18446744073709551613UL,0UL,18446744073709551613UL,0UL,18446744073709551613UL};
            int8_t *l_1524 = &g_174[3];
            int32_t **l_1525 = &l_1493[0];
            int i;
            g_237 &= (((*g_1389) >= ((((**l_1296) = (safe_lshift_func_uint16_t_u_s(p_57, 9))) || (((safe_mul_func_uint8_t_u_u((g_587.f0 < (((*g_94) &= ((*g_717) == ((*l_1511) = l_1510))) >= ((safe_mod_func_int8_t_s_s(((0x3DL | ((*l_1524) |= (safe_div_func_int64_t_s_s(((safe_div_func_uint16_t_u_u(((***g_716) , (safe_add_func_uint16_t_u_u((p_58 == ((safe_mod_func_uint16_t_u_u(p_58, g_1522)) >= p_57)), l_1523[6]))), p_58)) ^ l_1523[6]), p_58)))) < p_58), p_58)) >= 65528UL))), l_1523[6])) < 1UL) ^ p_58)) , l_1523[6])) ^ l_1523[6]);
            for (g_477 = 1; (g_477 >= 0); g_477 -= 1)
            { /* block id: 715 */
                if (l_1523[6])
                { /* block id: 716 */
                    (**g_1160) = 0x2.C635D7p+39;
                    for (l_1477 = 0; l_1477 < 2; l_1477 += 1)
                    {
                        for (l_1484 = 0; l_1484 < 10; l_1484 += 1)
                        {
                            g_1244[l_1477][l_1484] = (void*)0;
                        }
                    }
                    return &g_140;
                }
                else
                { /* block id: 720 */
                    int64_t l_1526 = 0x663E313F6B251833LL;
                    for (g_218 = 0; (g_218 <= 1); g_218 += 1)
                    { /* block id: 723 */
                        int i, j;
                        l_1526 |= 0xDAE71126L;
                        if (g_175[g_218][g_477])
                            continue;
                    }
                }
            }
        }
        l_1527[1][2] = (***g_324);
        for (g_677 = 0; (g_677 <= 1); g_677 += 1)
        { /* block id: 733 */
            float l_1530 = 0x9.C818F2p-51;
            int8_t l_1534[10] = {0x5DL,0xDDL,0xDDL,0x5DL,0xDDL,0xDDL,0x5DL,0xDDL,0xDDL,0x5DL};
            int32_t l_1535 = 0x05462275L;
            uint8_t ***l_1553 = &g_1550;
            int i;
            for (g_151 = 3; (g_151 >= 0); g_151 -= 1)
            { /* block id: 736 */
                uint8_t *l_1533[9][7][3] = {{{&g_1407,&g_111[7],&g_111[7]},{&g_111[7],(void*)0,(void*)0},{&g_111[7],&g_1407,&g_111[7]},{&g_1407,&g_111[7],(void*)0},{&g_111[5],&g_677,&g_111[7]},{&g_111[7],&g_677,(void*)0},{&g_111[5],(void*)0,&g_111[7]}},{{&g_111[7],&g_111[7],&g_111[7]},{&g_111[5],&g_677,(void*)0},{&g_1407,&g_677,&g_111[5]},{&g_111[7],&g_111[0],&g_677},{&g_111[7],&g_111[7],&g_111[8]},{&g_1407,&g_1407,&g_1407},{(void*)0,&g_111[8],&g_1407}},{{&g_111[7],&g_111[6],&g_111[8]},{(void*)0,&g_111[7],&g_677},{&g_1407,&g_111[5],&g_111[5]},{&g_1407,(void*)0,(void*)0},{&g_677,&g_1407,&g_111[7]},{(void*)0,&g_677,&g_111[7]},{&g_111[6],&g_111[7],(void*)0}},{{&g_677,&g_677,&g_111[7]},{&g_677,&g_1407,(void*)0},{&g_111[7],(void*)0,&g_111[7]},{&g_677,&g_111[5],(void*)0},{(void*)0,&g_111[7],&g_111[7]},{&g_111[7],&g_111[6],&g_111[0]},{&g_1407,&g_111[8],&g_677}},{{&g_1407,&g_1407,(void*)0},{&g_111[7],&g_111[7],(void*)0},{(void*)0,&g_111[0],(void*)0},{&g_677,&g_677,&g_111[2]},{&g_111[7],&g_677,&g_111[7]},{&g_677,&g_111[7],&g_111[7]},{&g_677,(void*)0,&g_111[6]}},{{&g_111[6],&g_677,&g_111[7]},{(void*)0,&g_677,&g_111[7]},{&g_677,&g_111[7],&g_111[2]},{&g_1407,&g_1407,(void*)0},{&g_1407,(void*)0,(void*)0},{(void*)0,&g_111[7],(void*)0},{&g_111[7],&g_1407,&g_677}},{{(void*)0,&g_1407,(void*)0},{(void*)0,&g_111[5],&g_111[7]},{&g_111[7],(void*)0,&g_677},{&g_111[7],&g_1407,&g_111[7]},{(void*)0,&g_111[7],&g_1407},{&g_111[7],&g_677,&g_111[6]},{&g_111[5],&g_111[7],(void*)0}},{{&g_677,&g_677,&g_111[8]},{&g_111[5],&g_111[7],&g_111[7]},{&g_111[7],(void*)0,&g_677},{(void*)0,&g_111[7],&g_111[7]},{&g_111[7],(void*)0,&g_111[7]},{&g_111[7],&g_677,(void*)0},{(void*)0,(void*)0,(void*)0}},{{&g_1407,(void*)0,(void*)0},{&g_111[6],&g_111[1],(void*)0},{&g_677,&g_111[7],&g_111[7]},{(void*)0,&g_111[7],&g_111[7]},{(void*)0,&g_111[5],&g_677},{&g_1407,(void*)0,&g_111[7]},{&g_111[5],&g_1407,&g_111[8]}}};
                int i, j, k;
                l_1483[g_151] |= ((g_23[g_677] ^ 0xCFB63C71L) ^ ((((*g_324) != (void*)0) , (((safe_sub_func_uint8_t_u_u((p_59 , ((((((*g_676) == 0xE0L) || (safe_mul_func_uint8_t_u_u(0x2DL, (((l_1534[2] = p_57) > p_59) & l_1535)))) , l_1534[5]) | l_1535) <= 0x1DF45FC7L)), l_1535)) >= p_58) || p_59)) , g_217[1][0]));
                for (g_411.f0 = 0; (g_411.f0 <= 3); g_411.f0 += 1)
                { /* block id: 741 */
                    uint8_t l_1537 = 3UL;
                    if ((~l_1534[0]))
                    { /* block id: 742 */
                        int i;
                        if (l_1537)
                            break;
                        l_1535 ^= 0xB23C19E8L;
                        (***g_324) = ((safe_lshift_func_uint8_t_u_u((++g_111[7]), 1)) , &l_1483[g_151]);
                        l_1483[g_151] |= l_1537;
                    }
                    else
                    { /* block id: 748 */
                        int32_t **l_1542 = &l_1281[2][1];
                        return &g_140;
                    }
                }
            }
            if ((l_1535 = 0x18B83A87L))
            { /* block id: 754 */
                float **l_1546 = &l_1544;
                float *l_1548 = &g_395;
                float **l_1547 = &l_1548;
                l_1535 ^= ((*g_1160) != ((*l_1547) = ((*l_1546) = (g_1543 , (l_1545 = l_1544)))));
            }
            else
            { /* block id: 759 */
                uint8_t ****l_1552[2][9] = {{&g_1549,(void*)0,&g_1549,&g_1549,&g_1549,(void*)0,&g_1549,&g_1549,(void*)0},{&g_1549,(void*)0,&g_1549,(void*)0,&g_1549,&g_1549,&g_1549,&g_1549,(void*)0}};
                int i, j;
                l_1553 = (g_1549 = g_1549);
            }
            for (l_1476 = 1; (l_1476 >= 0); l_1476 -= 1)
            { /* block id: 765 */
                return &g_140;
            }
            for (g_237 = 0; (g_237 <= 1); g_237 += 1)
            { /* block id: 770 */
                int32_t l_1556[8][2];
                int i, j;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_1556[i][j] = 7L;
                }
                l_1556[4][1] |= (safe_add_func_uint8_t_u_u(g_23[g_237], (-9L)));
                (*g_1274) &= g_23[g_237];
            }
        }
    }
    if ((g_1574 = (safe_mul_func_uint8_t_u_u((g_470.f1 ^ 65526UL), ((safe_sub_func_int32_t_s_s(((p_59 != ((**l_1296) = 4294967295UL)) != (safe_sub_func_uint64_t_u_u((l_1563 == (g_1569 = l_1567[1][0][1])), (safe_sub_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_s(0x6AL, 7)), p_59))))), (*g_503))) , 0xB1L)))))
    { /* block id: 779 */
        int16_t *l_1575 = &l_1283;
        int32_t l_1576 = (-5L);
        int64_t ****l_1610 = &g_716;
        int32_t l_1611[3];
        int8_t *l_1632 = &g_174[1];
        int8_t **l_1631 = &l_1632;
        int8_t ***l_1630[3][7] = {{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631},{&l_1631,&l_1631,&l_1631,&l_1631,(void*)0,&l_1631,&l_1631},{&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631,&l_1631}};
        int8_t l_1671[4];
        int16_t l_1711 = 2L;
        int16_t l_1752 = 0x8BE4L;
        uint32_t l_1795 = 0x4F902965L;
        uint8_t l_1889 = 0x22L;
        uint16_t l_1892 = 0xAB3BL;
        const uint8_t ** const **l_1894 = (void*)0;
        const uint8_t ** const ***l_1893 = &l_1894;
        uint32_t l_1895 = 0x71AB7330L;
        uint32_t l_2015 = 0x76822910L;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1611[i] = 0xCBB60CB5L;
        for (i = 0; i < 4; i++)
            l_1671[i] = 0x14L;
        (**g_1160) = ((void*)0 != l_1575);
        l_1576 &= 0x661D9227L;
        if (g_80)
            goto lbl_2018;
lbl_2018:
        for (g_838.f0 = 0; (g_838.f0 <= 1); g_838.f0 += 1)
        { /* block id: 784 */
            const int32_t *l_1577 = &l_1576;
            int8_t *l_1629 = &g_174[2];
            int8_t **l_1628 = &l_1629;
            int8_t ***l_1627[1][2];
            uint8_t ****l_1643 = (void*)0;
            int32_t l_1692[9][8][3] = {{{0x6AF44BCEL,0x43666BA2L,0L},{(-1L),1L,1L},{(-1L),(-7L),0xF2B9F8FDL},{0xC40957B0L,0xF6272FE0L,0L},{(-7L),0xD30796E9L,0x37279BA7L},{0x43666BA2L,0x8A421FE5L,1L},{(-1L),(-7L),0x1BE41689L},{0x6AF44BCEL,8L,0L}},{{(-1L),0xF6A86CA9L,(-1L)},{1L,0x7D778565L,1L},{(-9L),0L,0x92DEBE95L},{1L,0xD30796E9L,9L},{(-7L),0x20C5A6E2L,9L},{0x75A804FFL,0x959268F4L,0x92DEBE95L},{(-1L),(-1L),1L},{(-7L),(-1L),(-1L)}},{{0x2C373412L,0xF6272FE0L,0L},{0xC40957B0L,0x0B44594CL,0x1BE41689L},{0x959268F4L,1L,1L},{0x5B58E7A2L,0xC40957B0L,0x37279BA7L},{0x75A804FFL,0x744E9C82L,0L},{(-1L),8L,0xF2B9F8FDL},{0L,0x2C373412L,1L},{1L,0L,0L}},{{0x8A421FE5L,0x0B44594CL,0x6CF4C118L},{0x04382212L,0x9C652495L,9L},{0xD05BB4BBL,0x43666BA2L,0x3AB88473L},{(-1L),(-7L),(-8L)},{(-7L),0x43666BA2L,0xF2B9F8FDL},{0x7D778565L,0x9C652495L,0L},{0x2C373412L,0xF2AE0937L,(-8L)},{0x8EB742A5L,0x7ED3199AL,0x3AB88473L}},{{(-1L),0x6AF44BCEL,0x20C5A6E2L},{0L,0xB7069775L,1L},{(-1L),(-9L),(-1L)},{0x7ED3199AL,0x1567486CL,0x04382212L},{0x959268F4L,(-9L),0x5A844BB4L},{(-9L),0xF2AE0937L,(-7L)},{(-7L),1L,(-1L)},{0L,0xF2B9F8FDL,(-9L)}},{{0x0C9FB5A4L,0x038AB945L,(-7L)},{0xF2B9F8FDL,0xB1FE6125L,(-1L)},{0x9D11AA83L,(-1L),0x7F44CC9DL},{0x9D11AA83L,0L,0x0C9FB5A4L},{0xF2B9F8FDL,0x7ED3199AL,1L},{0x0C9FB5A4L,0x9D11AA83L,1L},{0L,0x0B44594CL,1L},{(-7L),0xB7069775L,0xC40957B0L}},{{(-9L),(-8L),0L},{0x959268F4L,0x5A844BB4L,0x7ED3199AL},{0x7ED3199AL,0L,0x8EB742A5L},{(-1L),1L,(-7L)},{0L,(-7L),(-5L)},{(-1L),0x7D778565L,(-7L)},{0x8EB742A5L,0x8EB742A5L,0xC40957B0L},{0x6AF44BCEL,0x92DEBE95L,0x06AEA568L}},{{0x9D11AA83L,0L,(-8L)},{(-7L),(-9L),(-1L)},{0xF6272FE0L,0x9D11AA83L,(-8L)},{0L,0x4FC5B75DL,0x06AEA568L},{0x1BE41689L,0x0B44594CL,0xC40957B0L},{0x5A844BB4L,0x1567486CL,(-7L)},{(-7L),8L,(-5L)},{0x7ED3199AL,0L,(-7L)}},{{1L,0L,0x8EB742A5L},{5L,(-7L),0x7ED3199AL},{0x0C9FB5A4L,(-7L),0L},{0xB1FE6125L,0xF2B9F8FDL,0xC40957B0L},{0x1567486CL,(-1L),1L},{0x6AF44BCEL,0L,1L},{(-7L),0x5A844BB4L,1L},{(-8L),0x6AF44BCEL,0x0C9FB5A4L}}};
            int32_t l_1841 = 0x7FE5574FL;
            float l_1855 = 0xE.6593B6p-25;
            int8_t * const l_1886 = &g_420;
            int32_t **l_1896[9] = {&g_140,&l_1281[2][1],&g_140,&g_140,&l_1281[2][1],&g_140,&g_140,&l_1281[2][1],&g_140};
            float l_1915 = (-0x1.9p-1);
            int32_t l_1940 = (-8L);
            const int32_t l_2010 = (-7L);
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_1627[i][j] = &l_1628;
            }
            (***g_324) = l_1577;
        }
        for (l_1283 = 0; (l_1283 < (-7)); l_1283 = safe_sub_func_int16_t_s_s(l_1283, 1))
        { /* block id: 957 */
            union U0 *l_2021[7][9] = {{&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_876[7][3],&g_876[5][6],&g_876[7][3]},{&g_169[0],&g_1156[1][4],&g_169[0],&g_411,&g_1971[0][8][0],&g_411,&g_169[0],&g_1156[1][4],&g_169[0]},{&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_876[7][3],&g_876[5][6],&g_876[7][3]},{&g_169[0],&g_1156[1][4],&g_169[0],&g_411,&g_1971[0][8][0],&g_411,&g_169[0],&g_1156[1][4],&g_169[0]},{&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_876[7][3],&g_876[5][6],&g_876[7][3]},{&g_169[0],&g_1156[1][4],&g_169[0],&g_411,&g_1971[0][8][0],&g_411,&g_169[0],&g_1156[1][4],&g_169[0]},{&g_876[7][3],&g_876[5][6],&g_876[7][3],&g_1971[0][0][0],&g_876[7][3],&g_1971[0][0][0],&g_1971[0][0][0],&g_876[7][3],&g_1971[0][0][0]}};
            int i, j;
            l_2021[0][4] = (*g_1376);
            if (p_59)
                continue;
        }
    }
    else
    { /* block id: 961 */
        int32_t l_2023[2];
        uint32_t ***l_2034 = &l_1296;
        int16_t *l_2036 = &g_153;
        int32_t l_2037 = 0x053101FFL;
        int8_t l_2052[6][1][7] = {{{0L,0x3CL,0L,0L,0x3CL,0L,0L}},{{0L,0L,(-2L),0L,0L,(-2L),0L}},{{0x3CL,0L,0L,0x3CL,0L,0L,0x3CL}},{{0L,0L,0L,0L,0L,0L,0L}},{{0x3CL,0x3CL,0x43L,0x3CL,0x3CL,0x43L,0x3CL}},{{0L,0L,0L,0L,0L,0L,0L}}};
        int32_t l_2078 = 0x80C8BBACL;
        int32_t l_2083 = 0L;
        int32_t l_2085 = 0x74F84B4FL;
        int32_t l_2086 = 0xA9CD71CBL;
        int32_t l_2087 = (-2L);
        int64_t l_2096 = 0xCC8305FF77A557BALL;
        union U0 **l_2158 = &g_1377;
        int64_t l_2173[1];
        int32_t l_2191[2];
        uint32_t l_2261 = 0UL;
        uint64_t l_2304[6] = {18446744073709551611UL,18446744073709551609UL,18446744073709551611UL,18446744073709551611UL,18446744073709551609UL,18446744073709551611UL};
        uint64_t l_2307[6] = {1UL,1UL,1UL,1UL,1UL,1UL};
        int8_t **l_2381 = &g_1685;
        int32_t *l_2402 = &l_2085;
        int64_t **l_2413 = &l_1510;
        int32_t l_2547[3];
        int32_t l_2551 = (-1L);
        uint64_t *l_2559 = &g_2480[0];
        uint8_t ** const *l_2572 = &g_1550;
        uint8_t ** const **l_2571 = &l_2572;
        uint32_t l_2687 = 18446744073709551615UL;
        int32_t ** const **l_2754[5][1][2] = {{{&l_2042[8][1][3],&l_2042[8][1][3]}},{{&l_2042[8][1][3],&l_2042[8][1][3]}},{{&l_2042[8][1][3],&l_2042[8][1][3]}},{{&l_2042[8][1][3],&l_2042[8][1][3]}},{{&l_2042[8][1][3],&l_2042[8][1][3]}}};
        int32_t ** const ***l_2753 = &l_2754[1][0][0];
        const float *l_2784[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        const float **l_2783 = &l_2784[1];
        uint8_t l_2788 = 0x4AL;
        int32_t l_2794[5];
        int32_t l_2795 = (-1L);
        uint64_t l_2797[9][4][7] = {{{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL}},{{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL}},{{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL}},{{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL},{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0UL,0UL,0UL}},{{18446744073709551615UL,0x501DC26C11D414A2LL,0x501DC26C11D414A2LL,18446744073709551615UL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL}},{{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL}},{{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL}},{{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL}},{{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL},{0x501DC26C11D414A2LL,0UL,0UL,0x501DC26C11D414A2LL,0x9312ED33093492E9LL,0x2D4C375197A4A0C0LL,0x9312ED33093492E9LL}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2023[i] = 8L;
        for (i = 0; i < 1; i++)
            l_2173[i] = 0L;
        for (i = 0; i < 2; i++)
            l_2191[i] = 0xA979703CL;
        for (i = 0; i < 3; i++)
            l_2547[i] = 0x37E3E801L;
        for (i = 0; i < 5; i++)
            l_2794[i] = 0xE65E15EFL;
        l_2024--;
        l_2037 = ((l_2027[2][1][1] != &l_2028) , (((*l_2036) ^= (((*l_2034) = (g_2029[3] , (((safe_lshift_func_uint8_t_u_s((*g_676), 7)) || (!g_2033)) , (void*)0))) != l_2035)) != p_59));
    }
    return &g_140;
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_34 g_29 g_88 g_93 g_237 g_1274 g_284.f0
 * writes: g_6 g_34 g_29 g_88 g_80 g_237
 */
static int16_t  func_62(uint32_t * p_63)
{ /* block id: 29 */
    uint16_t *l_68 = &g_23[0];
    uint16_t ***l_1275 = (void*)0;
    uint16_t **l_1277[5];
    uint16_t ***l_1276 = &l_1277[0];
    int i;
    for (i = 0; i < 5; i++)
        l_1277[i] = &g_94;
    (*l_1276) = func_66(l_68);
    return g_284.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_34 g_29 g_88 g_93 g_237 g_1274
 * writes: g_6 g_34 g_29 g_88 g_80 g_237
 */
static uint16_t ** const  func_66(uint16_t * p_67)
{ /* block id: 30 */
    int32_t l_75 = (-1L);
    uint32_t *l_123 = &g_34;
    uint16_t ** const l_1271 = &g_94;
    uint32_t *l_1273[4] = {&g_697,&g_697,&g_697,&g_697};
    uint32_t **l_1272 = &l_1273[2];
    int i;
    for (g_6 = 22; (g_6 > (-23)); --g_6)
    { /* block id: 33 */
        uint16_t ** const l_101 = &g_94;
        uint32_t l_121 = 0x6C3C0C88L;
        int32_t *l_1270 = &g_237;
        for (g_34 = 10; (g_34 < 46); g_34 = safe_add_func_uint64_t_u_u(g_34, 7))
        { /* block id: 36 */
            float l_78 = 0x9.8A7B98p-26;
            int32_t l_86 = 7L;
            int32_t l_87 = 2L;
            for (g_29 = (-29); (g_29 <= (-19)); g_29 = safe_add_func_int64_t_s_s(g_29, 8))
            { /* block id: 39 */
                int16_t l_100 = 4L;
                uint16_t ***l_102[2];
                uint16_t **l_104[3];
                uint16_t ***l_103 = &l_104[0];
                uint32_t *l_120[9][2] = {{&g_34,&g_34},{(void*)0,&g_34},{&g_34,&g_34},{&g_34,&g_34},{&g_34,(void*)0},{&g_34,&g_34},{&g_34,&g_34},{&g_34,&g_34},{(void*)0,&g_34}};
                int32_t l_122 = (-1L);
                int i, j;
                for (i = 0; i < 2; i++)
                    l_102[i] = (void*)0;
                for (i = 0; i < 3; i++)
                    l_104[i] = &g_94;
                l_75 ^= 0x82FB3486L;
                for (l_75 = 14; (l_75 >= (-17)); l_75 = safe_sub_func_uint32_t_u_u(l_75, 5))
                { /* block id: 43 */
                    int32_t *l_79 = &g_80;
                    int32_t *l_81 = &g_80;
                    int32_t *l_82 = &g_80;
                    int32_t *l_83 = &g_80;
                    int32_t *l_84 = (void*)0;
                    int32_t *l_85[5][9] = {{(void*)0,&g_80,(void*)0,&g_6,&g_6,&g_6,&g_6,(void*)0,&g_80},{&g_6,&g_80,&g_47,&g_4,&g_4,&g_47,&g_80,&g_6,&g_80},{&g_4,(void*)0,&g_6,&g_6,(void*)0,&g_4,&g_6,&g_4,(void*)0},{&g_80,&g_80,&g_80,&g_80,&g_6,&l_75,&g_80,&g_80,&l_75},{&g_4,&g_4,&g_80,&g_6,&g_4,&g_6,&g_80,&g_4,&g_4}};
                    int i, j;
                    if (l_75)
                        break;
                    g_88++;
                    for (g_80 = (-23); (g_80 > 23); g_80++)
                    { /* block id: 48 */
                        return g_93;
                    }
                }
            }
            return l_101;
        }
        (*l_1270) &= l_75;
        return l_1271;
    }
    (*g_1274) = (&g_697 != ((*l_1272) = &g_697));
    return l_1271;
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_617 g_237 g_676 g_677 g_94 g_23 g_1268 g_503 g_34 g_1160 g_1161 g_1162
 * writes: g_1124 g_617 g_614 g_237 g_111 g_153
 */
static float  func_95(int8_t  p_96, uint16_t ** const  p_97, uint32_t  p_98, float  p_99)
{ /* block id: 554 */
    float l_1230 = (-0x10.Fp+1);
    int32_t *l_1231 = &g_6;
    int32_t l_1236 = 0x7FAB19C1L;
    int64_t *l_1237 = &g_1124;
    int8_t *l_1240 = (void*)0;
    int8_t *l_1241 = &g_617[1];
    int16_t *l_1242[9][6][3] = {{{&g_153,&g_614,&g_153},{&g_614,&g_614,&g_153},{&g_614,&g_153,&g_614},{&g_153,(void*)0,&g_153},{&g_614,(void*)0,&g_614},{&g_614,&g_153,&g_153}},{{&g_153,(void*)0,&g_614},{&g_614,&g_153,&g_614},{&g_614,&g_614,&g_614},{&g_153,&g_614,&g_153},{&g_614,&g_614,&g_153},{&g_614,&g_153,&g_614}},{{&g_153,(void*)0,&g_153},{&g_614,(void*)0,&g_614},{&g_614,&g_153,&g_153},{&g_153,(void*)0,&g_614},{&g_614,&g_153,&g_614},{&g_614,&g_614,&g_614}},{{&g_153,&g_614,&g_153},{&g_614,&g_614,&g_153},{&g_614,&g_153,&g_614},{&g_153,(void*)0,&g_153},{&g_614,(void*)0,&g_614},{&g_614,&g_153,&g_153}},{{&g_153,(void*)0,&g_614},{&g_614,&g_153,&g_614},{&g_614,&g_614,&g_614},{&g_153,&g_614,&g_153},{&g_614,&g_614,&g_153},{&g_614,&g_153,&g_614}},{{&g_153,(void*)0,&g_153},{&g_614,(void*)0,&g_614},{&g_614,&g_153,&g_153},{&g_153,(void*)0,&g_614},{&g_614,&g_153,&g_614},{&g_614,&g_614,&g_614}},{{&g_153,&g_614,&g_153},{&g_614,&g_614,&g_153},{&g_614,&g_153,&g_614},{&g_153,(void*)0,&g_153},{&g_614,(void*)0,&g_614},{&g_614,&g_153,&g_153}},{{&g_153,(void*)0,&g_614},{&g_614,&g_153,&g_614},{&g_614,&g_614,&g_614},{&g_153,&g_614,&g_153},{&g_614,&g_614,&g_153},{&g_614,&g_153,&g_614}},{{&g_153,(void*)0,&g_153},{&g_614,(void*)0,&g_614},{&g_614,&g_153,&g_153},{&g_153,(void*)0,&g_614},{&g_614,&g_153,&g_614},{&g_614,&g_614,&g_614}}};
    int32_t *l_1245 = (void*)0;
    int32_t *l_1246 = (void*)0;
    int32_t *l_1247 = &g_237;
    int i, j, k;
    (*l_1247) |= (0UL >= (((safe_add_func_float_f_f(l_1230, 0x0.1p+1)) , l_1231) != ((safe_rshift_func_uint16_t_u_u((safe_div_func_int32_t_s_s((l_1236 = (*l_1231)), (((*l_1237) = p_96) ^ (0x5A16L & (g_614 = ((((safe_rshift_func_uint8_t_u_s((((((*l_1241) |= p_98) & (*l_1231)) ^ (*l_1231)) , p_96), 6)) >= p_96) < 0x66L) , p_96)))))), 7)) , &l_1236)));
    l_1236 |= (0x0E06L == (g_153 = (safe_lshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u(((((safe_add_func_int64_t_s_s(2L, ((safe_sub_func_int32_t_s_s((p_98 || p_98), 0x8D579ED4L)) & (safe_add_func_int8_t_s_s((p_96 = ((safe_add_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((*g_676), (g_111[7] = (((((safe_div_func_uint32_t_u_u(((safe_add_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((((((*l_1247) && ((((((*g_94) | ((g_1268[8] , 0L) , 0UL)) , p_96) || (*g_503)) || 0xDE4D2FDE8893FB42LL) , p_96)) > p_96) > 0x8FF7537DF590FB91LL) == p_98), 0xEF98L)), 250UL)) , p_98), (*l_1247))) && 0xEDL) | (*g_676)) ^ p_96) != 0x92F4L)))), p_96)) , p_96)), p_98))))) , 65529UL) && (-5L)) <= 0UL), (*l_1231))), 7))));
    return (**g_1160);
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_23 g_284.f1 g_80 g_93 g_789 g_111 g_139 g_324 g_325 g_326 g_1093 g_754 g_153 g_682.f1 g_237 g_1124 g_1126 g_676 g_677 g_421 g_1156 g_1156.f1 g_1160 g_151 g_34 g_838.f0 g_753 g_718 g_213 g_2 g_682.f0 g_327 g_1107.f0 g_1059 g_732.f0
 * writes: g_23 g_88 g_677 g_237 g_732.f0 g_80 g_140 g_327 g_1124 g_617 g_754 g_1186 g_421 g_151 g_477
 */
static float  func_107(const uint32_t  p_108, uint16_t * p_109, float  p_110)
{ /* block id: 361 */
    int32_t l_864 = 1L;
    union U0 *l_875 = &g_876[5][6];
    uint16_t *l_877 = &g_88;
    int8_t *l_878[3];
    int8_t l_879[3];
    uint8_t *l_880 = &g_677;
    int32_t l_881 = 7L;
    int32_t l_883 = 0xE772AA5AL;
    uint16_t l_884 = 0UL;
    int32_t l_892 = 0x2F229C3CL;
    int32_t l_893 = 0x1C32E042L;
    int32_t l_895 = 0L;
    uint64_t l_896 = 1UL;
    int32_t l_931 = 0x53979A24L;
    const uint32_t ** const **l_954[6][10][4] = {{{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0}},{{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0}},{{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0}},{{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0}},{{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0}},{{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0},{&g_951,&g_951,(void*)0,(void*)0}}};
    int64_t ****l_964 = &g_716;
    union U1 *l_1034 = &g_789;
    int16_t l_1045 = (-4L);
    uint64_t l_1078 = 18446744073709551614UL;
    const int32_t l_1083 = 0x76B2432AL;
    int32_t *l_1226[10];
    uint64_t l_1227 = 0x57538EF0024FBF65LL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_878[i] = &g_617[1];
    for (i = 0; i < 3; i++)
        l_879[i] = 0xFDL;
    for (i = 0; i < 10; i++)
        l_1226[i] = &g_237;
    if (((l_881 = (safe_lshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(((safe_sub_func_int16_t_s_s((+((*l_880) = (safe_add_func_uint32_t_u_u(((safe_rshift_func_int16_t_s_s((safe_sub_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(l_864, ((-2L) && (l_879[1] = ((safe_lshift_func_uint16_t_u_u(((*g_94) &= l_864), ((*l_877) = ((safe_div_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_u(((p_108 != (l_864 & (g_284.f1 <= (safe_mul_func_uint16_t_u_u((p_108 != l_864), (((void*)0 == l_875) <= p_108)))))) >= l_864), l_864)) ^ p_108), 0x28E6L)), 0x43L)) , p_108)))) && p_108))))), l_864)), l_864)), l_864)), p_108)), l_864)) == p_108), l_864)))), 1UL)) , l_879[1]), 14)), l_864))) , l_881))
    { /* block id: 367 */
        int32_t *l_882[5];
        int i;
        for (i = 0; i < 5; i++)
            l_882[i] = &l_881;
        ++l_884;
    }
    else
    { /* block id: 369 */
        int32_t *l_887 = &l_881;
        int32_t l_888 = (-4L);
        int32_t *l_889 = (void*)0;
        int32_t l_890 = 0xACD6301DL;
        int32_t *l_891[1];
        float l_894 = (-0x3.1p-1);
        int16_t l_1014[2][2] = {{(-1L),(-1L)},{(-1L),(-1L)}};
        int32_t l_1027 = 0L;
        union U1 *l_1105[5][2][4] = {{{&g_1107,&g_1107,(void*)0,&g_1107},{&g_1107,&g_1106,&g_1106,&g_1107}},{{&g_1106,&g_1107,&g_1106,&g_1106},{&g_1107,&g_1107,(void*)0,&g_1107}},{{&g_1107,&g_1106,&g_1106,&g_1107},{&g_1106,&g_1107,&g_1106,&g_1106}},{{&g_1107,&g_1107,(void*)0,&g_1107},{&g_1107,&g_1106,&g_1106,&g_1107}},{{&g_1106,&g_1107,&g_1106,&g_1106},{&g_1107,&g_1107,(void*)0,&g_1107}}};
        int64_t **l_1108 = &g_718[5][0];
        uint8_t l_1111 = 0x10L;
        uint16_t l_1151 = 65535UL;
        int64_t l_1168 = 0x257712FBF14490B7LL;
        uint32_t l_1181 = 0x1F54DA27L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_891[i] = &l_881;
lbl_1016:
        (*l_887) &= 0xB565B3F2L;
        l_896--;
        for (g_237 = 0; (g_237 >= 11); g_237 = safe_add_func_int8_t_s_s(g_237, 6))
        { /* block id: 374 */
            int32_t ***l_909 = &g_139;
            int32_t ****l_908 = &l_909;
            int16_t *l_910 = &g_153;
            union U0 *l_1005 = &g_732;
            float l_1032 = (-0x10.Bp+1);
            int32_t l_1055 = 0L;
            int32_t l_1056 = (-1L);
        }
        for (g_732.f0 = 0; (g_732.f0 <= (-7)); g_732.f0 = safe_sub_func_uint8_t_u_u(g_732.f0, 4))
        { /* block id: 483 */
            uint32_t l_1084 = 3UL;
            uint32_t l_1121 = 1UL;
            int32_t l_1123 = 0x6D682652L;
            int32_t l_1136 = 0xD4F57508L;
            for (g_80 = 0; (g_80 != 16); g_80 = safe_add_func_uint32_t_u_u(g_80, 2))
            { /* block id: 486 */
                uint16_t **l_1072 = &g_94;
                uint16_t ***l_1071 = &l_1072;
                uint16_t ****l_1073 = &l_1071;
                int32_t l_1079 = (-1L);
                if (g_80)
                    goto lbl_1016;
                if (((((safe_lshift_func_uint8_t_u_s((((safe_rshift_func_int8_t_s_s(((((*l_1073) = l_1071) == (void*)0) >= ((safe_div_func_uint16_t_u_u((**g_93), (l_892 = (safe_mul_func_int8_t_s_s(((((p_108 && (l_1078 < l_1079)) | (~p_108)) > ((((*l_1034) , ((safe_rshift_func_int8_t_s_u(p_108, l_892)) , g_111[3])) != p_108) <= 0UL)) , 0x1AL), p_108))))) , 0x309D405EL)), p_108)) , (*l_887)) && (-3L)), l_1083)) == p_108) && l_1084) >= p_108))
                { /* block id: 490 */
                    (***g_324) = (p_110 , ((*g_139) = (void*)0));
                }
                else
                { /* block id: 493 */
                    int8_t ** const l_1097 = &l_878[0];
                    int8_t ** const *l_1096 = &l_1097;
                    int64_t **l_1104 = (void*)0;
                    int32_t l_1110 = 0x574B8708L;
                    int32_t l_1112 = 5L;
                    uint16_t *l_1120 = (void*)0;
                    if (l_1084)
                    { /* block id: 494 */
                        int64_t l_1103 = 0L;
                        int32_t l_1109 = 0xF9488A68L;
                        int64_t l_1119 = (-1L);
                        uint8_t l_1122 = 0xFDL;
                        l_1112 ^= ((0xFD1CL == (safe_add_func_int32_t_s_s((safe_rshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s(((safe_div_func_int64_t_s_s((((p_108 <= (g_1093 == l_1096)) >= (((**g_93) = 1UL) <= (((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_int16_t_s(((6UL || ((l_1103 , (l_1109 = (l_1104 == ((g_754 != l_1105[3][0][0]) , l_1108)))) , g_153)) & (-5L)))), l_1110)) != 0xDB089B7030EE906ALL) == l_1111))) >= l_1110), g_111[7])) <= p_108), 0x48FAL)), 2)), l_864))) , p_108);
                        (*l_887) = l_1084;
                        g_1124 |= ((((l_1123 ^= ((safe_div_func_uint16_t_u_u((++(**g_93)), ((g_682.f1 > (safe_mul_func_int8_t_s_s((p_108 , ((((l_1119 |= l_1112) >= ((((*g_93) != l_1120) & (l_1121 <= 1UL)) == l_1122)) <= p_108) < 18446744073709551614UL)), 0xDCL))) & (-1L)))) == l_1110)) | 0x54B123E189CD97BALL) >= g_237) >= 1UL);
                    }
                    else
                    { /* block id: 503 */
                        float l_1125 = 0x7.6707F3p-91;
                        (*l_887) = p_108;
                    }
                    if (p_108)
                        continue;
                    l_1136 = (g_1126 , ((l_1123 &= ((((safe_lshift_func_int8_t_s_u(1L, (safe_sub_func_int64_t_s_s(((((void*)0 == l_880) <= (safe_rshift_func_int8_t_s_s((((*g_93) == (*g_93)) || (0x04412E35L || (l_1112 | (+(safe_div_func_int64_t_s_s(p_108, l_1079)))))), 3))) , p_108), p_108)))) > l_864) || l_1110) == l_879[1])) >= p_108));
                    l_892 = (safe_add_func_int32_t_s_s((l_895 ^= l_1123), (safe_sub_func_uint8_t_u_u(250UL, (safe_unary_minus_func_uint16_t_u((((+(*g_676)) | ((safe_sub_func_int8_t_s_s(((**l_1097) = l_1079), (0xA11AD3BEC42916F2LL != (safe_add_func_int16_t_s_s((safe_sub_func_int8_t_s_s((0x1ED91A72C9A9850ALL != ((safe_rshift_func_int16_t_s_u(((l_883 , 249UL) < 0x23L), l_1151)) >= g_421)), p_108)), l_896))))) || 6UL)) ^ p_108)))))));
                }
                return l_879[0];
            }
            for (l_895 = (-3); (l_895 <= 2); l_895 = safe_add_func_uint32_t_u_u(l_895, 3))
            { /* block id: 517 */
                int8_t l_1167 = 0xD6L;
                union U1 *l_1184 = (void*)0;
                uint32_t l_1203 = 0xC699304AL;
                int32_t l_1206 = 1L;
                if (((*l_887) = (l_1136 ^= ((safe_add_func_uint64_t_u_u((g_1156[0][2] , g_1156[0][2].f1), p_108)) == ((safe_unary_minus_func_uint8_t_u(0x3FL)) , ((safe_add_func_uint16_t_u_u(((void*)0 != g_1160), 4UL)) , (safe_mul_func_int8_t_s_s((safe_sub_func_int8_t_s_s((l_1168 = ((p_108 , (((**g_93) || p_108) > l_1167)) ^ g_151)), 1L)), l_895))))))))
                { /* block id: 521 */
                    for (l_1111 = 0; (l_1111 < 16); l_1111 = safe_add_func_uint64_t_u_u(l_1111, 8))
                    { /* block id: 524 */
                        if (l_1121)
                            goto lbl_1016;
                        return l_1121;
                    }
                    if (p_108)
                        break;
                    for (g_88 = 6; (g_88 != 54); ++g_88)
                    { /* block id: 531 */
                        (*l_887) &= (safe_div_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u(((1UL < (((safe_mod_func_uint16_t_u_u((l_1123 & (((1UL & g_1124) , &g_754) != &l_1034)), (safe_rshift_func_int16_t_s_u((((g_34 & (-1L)) > 0x43593FE1L) & 0x01C6545AL), (**g_93))))) && l_1181) & 0UL)) != 0L), p_108)), g_838.f0));
                    }
                    if (l_931)
                        continue;
                }
                else
                { /* block id: 535 */
                    union U1 **l_1185[10] = {&l_1105[3][1][3],&l_1034,&l_1034,&l_1105[3][1][3],(void*)0,&l_1105[3][1][3],&l_1034,&l_1034,&l_1105[3][1][3],(void*)0};
                    uint32_t *l_1199 = &g_421;
                    int32_t l_1202[8] = {(-1L),0xFAECD3C2L,(-1L),(-1L),0xFAECD3C2L,(-1L),(-1L),0xFAECD3C2L};
                    int i;
                    l_892 |= (((((l_884 ^ ((safe_sub_func_int16_t_s_s((((((*g_753) = l_1184) != (g_1186 = l_1105[3][0][2])) >= ((((*l_887) = ((safe_rshift_func_uint16_t_u_s((**g_93), 11)) , ((**l_1108) = ((**g_93) > ((safe_sub_func_int32_t_s_s((-1L), (((safe_mod_func_int16_t_s_s((((((255UL || 0x69L) , (((((safe_div_func_uint16_t_u_u(((((*l_880) = (safe_unary_minus_func_int16_t_s(((safe_add_func_uint32_t_u_u((((++(*l_1199)) || (l_1167 & p_108)) & p_108), (-9L))) != 0UL)))) > 1UL) != p_108), 65526UL)) ^ l_1202[2]) <= 2L) , (*g_676)) <= l_1121)) == 0xDEL) || 0L) != l_1136), g_111[6])) < l_1167) , p_108))) || 0xA7L))))) == g_153) , (*g_94))) , 0x4E46L), 0xF72DL)) && l_1167)) < l_1203) , (void*)0) == &l_954[5][3][1]) <= 0xB6A6B547L);
                }
                l_892 |= ((safe_div_func_int16_t_s_s(l_1206, ((*l_877) = l_1123))) || ((safe_lshift_func_int16_t_s_u((p_108 , l_883), ((safe_lshift_func_int16_t_s_u(l_1167, (((((p_108 , (((((0xD.B6A533p-88 > (safe_mul_func_float_f_f((safe_div_func_float_f_f(((*g_213) > (safe_sub_func_float_f_f(((0x1.0p-1 >= p_108) > p_108), (-0x8.Cp-1)))), g_682.f0)), g_1124))) < 0xC.E1B8FCp-30) , (-9L)) , (*g_676)) <= (*l_887))) != 0xAAAA78ABL) & 249UL) || p_108) >= p_108))) ^ p_108))) < 1L));
            }
            return l_1121;
        }
    }
    (***g_324) = (***g_324);
    g_80 |= ((~((!((65535UL && g_1107.f0) , (safe_rshift_func_int16_t_s_u(0x6ADBL, 3)))) < (l_881 = (~(safe_add_func_int16_t_s_s((l_880 != (l_881 , l_880)), (safe_mul_func_uint16_t_u_u((p_108 == ((4294967295UL >= ((p_108 == g_1059) >= 0x6EA5735D88ED56FBLL)) > 0UL)), 1L)))))))) > 0UL);
    return l_1227;
}


/* ------------------------------------------ */
/* 
 * reads : g_139 g_29 g_88 g_47 g_93 g_94 g_23 g_153 g_5 g_151 g_169 g_34 g_169.f0 g_174 g_175 g_140 g_196 g_213 g_218 g_237 g_196.f0 g_167 g_271 g_80 g_277 g_281 g_284 g_165 g_271.f0 g_324 g_2 g_411 g_421 g_325 g_326 g_327 g_431 g_435 g_217 g_436 g_277.f1 g_411.f0 g_470 g_411.f1 g_420 g_502 g_470.f0 g_513 g_437 g_503 g_542 g_562 g_572 g_577 g_587 g_595 g_605 g_618 g_617 g_284.f1 g_573 g_676 g_682 g_605.f0 g_677 g_697 g_716 g_169.f1 g_212 g_729 g_732 g_753 g_717 g_789 g_718 g_477 g_807 g_838 g_837
 * writes: g_2 g_151 g_153 g_5 g_165 g_167 g_174 g_175 g_140 g_80 g_23 g_218 g_237 g_281 g_88 g_387 g_395 g_397 g_421 g_431 g_436 g_477 g_327 g_420 g_502 g_573 g_577 g_618 g_676 g_697 g_716 g_729 g_717 g_754 g_807 g_677 g_837
 */
static union U0  func_114(uint32_t * p_115, int16_t  p_116, int16_t  p_117, uint32_t  p_118, uint32_t * p_119)
{ /* block id: 55 */
    int32_t *l_124[5] = {&g_80,&g_80,&g_80,&g_80,&g_80};
    uint16_t l_125 = 8UL;
    int32_t **l_143 = &l_124[0];
    int32_t **l_144 = &g_140;
    int32_t *l_147 = &g_47;
    int8_t l_148 = (-9L);
    uint64_t l_159 = 0x7380C216484500A6LL;
    uint64_t l_168[10][4] = {{18446744073709551606UL,0x5CE0FBC7DB13CF29LL,0xFD84452C2E73AF25LL,0x4E035C9A6D21B019LL},{18446744073709551606UL,0xFD84452C2E73AF25LL,18446744073709551606UL,18446744073709551615UL},{0x5CE0FBC7DB13CF29LL,0x4E035C9A6D21B019LL,18446744073709551615UL,18446744073709551615UL},{0xFD84452C2E73AF25LL,0xFD84452C2E73AF25LL,0xDDDAE5F053F68CE5LL,0x4E035C9A6D21B019LL},{0x4E035C9A6D21B019LL,0x5CE0FBC7DB13CF29LL,0xDDDAE5F053F68CE5LL,0x5CE0FBC7DB13CF29LL},{0xFD84452C2E73AF25LL,18446744073709551606UL,18446744073709551615UL,0xDDDAE5F053F68CE5LL},{0x5CE0FBC7DB13CF29LL,18446744073709551606UL,18446744073709551606UL,0x5CE0FBC7DB13CF29LL},{18446744073709551606UL,0x5CE0FBC7DB13CF29LL,0xFD84452C2E73AF25LL,0x4E035C9A6D21B019LL},{18446744073709551606UL,0xFD84452C2E73AF25LL,18446744073709551606UL,18446744073709551615UL},{0x5CE0FBC7DB13CF29LL,0x4E035C9A6D21B019LL,18446744073709551615UL,18446744073709551615UL}};
    int8_t *l_177 = &g_29;
    uint32_t *l_259 = &g_34;
    uint32_t **l_258 = &l_259;
    uint32_t l_371 = 1UL;
    int8_t l_429 = 0xF9L;
    uint32_t l_486 = 0x3A50FD07L;
    int64_t **l_733 = (void*)0;
    int i, j;
lbl_493:
    --l_125;
    if (p_118)
    { /* block id: 57 */
        int32_t l_130 = 1L;
        int32_t ***l_141 = (void*)0;
        int32_t ***l_142[9] = {&g_139,&g_139,&g_139,&g_139,&g_139,&g_139,&g_139,&g_139,&g_139};
        float *l_149 = &g_2[4];
        int64_t *l_150 = &g_151;
        int16_t *l_152 = &g_153;
        int32_t l_158 = 3L;
        uint64_t *l_164 = &g_165;
        int64_t *l_166 = &g_167;
        int i;
        g_5 |= ((((*l_152) ^= (((p_117 || ((((*l_150) = (((safe_div_func_float_f_f(l_130, ((*l_149) = (&g_94 == (((safe_sub_func_float_f_f(((-0x9.6p+1) < ((-0x1.1p-1) >= (safe_add_func_float_f_f((safe_div_func_float_f_f(((safe_mul_func_float_f_f(((l_144 = (l_143 = g_139)) != (void*)0), (safe_div_func_float_f_f(((l_147 == (void*)0) == l_148), g_29)))) > p_118), g_88)), (*l_147))))), p_118)) < 0xD.EC5ABBp-46) , (void*)0))))) , p_117) >= 0UL)) <= p_116) ^ p_117)) ^ p_116) != (**g_93))) || p_116) != 0x4CEF870AB71361DELL);
        l_168[4][1] = (safe_mod_func_uint8_t_u_u((((*l_166) = ((*l_150) = ((((((((safe_mod_func_uint32_t_u_u(g_153, l_158)) && g_23[0]) , (l_159 != (g_5 == (safe_sub_func_int64_t_s_s((safe_add_func_uint8_t_u_u(g_151, ((((*l_164) = ((18446744073709551615UL != (-1L)) < p_118)) , 8L) > g_151))), 5UL))))) == 1UL) && g_47) & 0xC6L) || g_5) , 1L))) , p_118), p_118));
        for (l_158 = 6; (l_158 >= 0); l_158 -= 1)
        { /* block id: 70 */
            return g_169[0];
        }
    }
    else
    { /* block id: 73 */
        int8_t *l_172 = &l_148;
        int8_t *l_173 = &g_174[2];
        int16_t l_176 = (-7L);
        int32_t l_178 = 0xC22C6A01L;
        int32_t ***l_190 = &l_144;
        uint8_t l_192 = 1UL;
        int32_t l_216 = 1L;
        uint32_t l_260 = 18446744073709551615UL;
        float * const l_278 = &g_2[4];
        uint32_t **l_505 = &l_259;
        int64_t **l_561 = (void*)0;
        int32_t l_696 = 4L;
        int16_t l_724[7] = {0L,9L,9L,0L,9L,9L,0L};
        int16_t l_788 = 0xF1F0L;
        int8_t l_839 = 0xCBL;
        int32_t *****l_840 = (void*)0;
        int i;
        if ((l_178 ^= (safe_rshift_func_int8_t_s_s((g_175[0][1] = ((*l_173) = ((*l_172) = (18446744073709551613UL == g_34)))), ((((&l_125 == &l_125) == 0x19L) & ((l_176 & (((p_118 | (l_177 != l_172)) < p_116) < g_169[0].f0)) | 5L)) & p_118)))))
        { /* block id: 78 */
            int64_t *l_185 = &g_151;
            int32_t ****l_191 = &l_190;
            int32_t l_201 = (-1L);
            uint8_t l_328 = 247UL;
            int32_t l_419[9][3];
            float *l_488 = &g_397;
            float **l_487 = &l_488;
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 3; j++)
                    l_419[i][j] = 1L;
            }
            (*g_139) = p_115;
            if ((((safe_sub_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((((p_118 , ((safe_add_func_int64_t_s_s(((*l_185) = 0L), (5UL < (safe_add_func_uint8_t_u_u(((p_118 && (((*l_191) = l_190) != (((l_192 >= (g_174[2] && p_117)) || ((*g_94) = (!(g_80 = (((p_116 != l_178) , (-7L)) >= g_47))))) , &g_139))) >= 0UL), p_117))))) , p_118)) && p_118) & 0x2DL), g_175[0][1])), (*l_147))) , (***l_191)) != (*g_139)))
            { /* block id: 84 */
                int32_t l_232 = 0x0557642EL;
                int32_t l_241[8] = {0xFEED6841L,0xFEED6841L,0xFEED6841L,0xFEED6841L,0xFEED6841L,0xFEED6841L,0xFEED6841L,0xFEED6841L};
                uint16_t *l_329 = &l_125;
                int8_t * const *l_434 = &l_172;
                int i;
                for (l_178 = 0; (l_178 > (-1)); l_178 = safe_sub_func_int64_t_s_s(l_178, 5))
                { /* block id: 87 */
                    uint32_t *l_205 = (void*)0;
                    uint32_t **l_204 = &l_205;
                    uint32_t **l_206 = (void*)0;
                    uint32_t *l_208[6];
                    uint32_t **l_207[6] = {&l_208[3],&l_208[3],&l_208[3],&l_208[3],&l_208[3],&l_208[3]};
                    uint64_t *l_209 = &l_159;
                    int32_t l_210 = 0x65A55B72L;
                    int32_t l_280 = (-10L);
                    int i;
                    for (i = 0; i < 6; i++)
                        l_208[i] = &g_34;
                    (*g_213) = (((g_196 , ((safe_add_func_int32_t_s_s(((((-1L) < (safe_mod_func_int64_t_s_s((l_201 , 0x630719000718B624LL), (0x2628L && ((**g_93) ^ (safe_rshift_func_int16_t_s_u(0L, 11))))))) , ((*l_209) = (l_124[2] != (p_115 = ((*l_204) = (void*)0))))) >= p_117), 0x8482C93BL)) , l_210)) , p_117) > 0x4.6322F5p+40);
                    for (p_116 = 0; (p_116 < (-30)); p_116 = safe_sub_func_int8_t_s_s(p_116, 5))
                    { /* block id: 94 */
                        --g_218;
                    }
                    for (l_192 = 0; (l_192 == 31); l_192 = safe_add_func_uint64_t_u_u(l_192, 2))
                    { /* block id: 99 */
                        int8_t l_225 = 1L;
                        float *l_242[2][1][8] = {{{&g_2[8],&g_2[0],&g_2[0],&g_2[8],&g_2[0],&g_2[0],&g_2[8],&g_2[0]}},{{&g_2[8],&g_2[8],&g_2[7],&g_2[8],&g_2[8],&g_2[7],&g_2[8],&g_2[8]}}};
                        int32_t l_243 = 0L;
                        int i, j, k;
                        l_201 = (safe_sub_func_float_f_f((l_225 > (safe_add_func_float_f_f(p_118, (safe_div_func_float_f_f((safe_add_func_float_f_f((((*p_119) , (l_232 < ((l_243 = ((safe_lshift_func_uint16_t_u_u((1L && (0xCDL < ((l_241[6] = (((((g_237 |= g_23[1]) <= ((safe_unary_minus_func_int8_t_s(5L)) && ((safe_mul_func_uint8_t_u_u(0x38L, (-6L))) & p_117))) != p_117) ^ 0xF5E34AC4052702D2LL) , (-1L))) <= l_225))), 12)) , l_225)) >= p_118))) <= 0xF.ECA5C7p+54), 0x4.E05431p-30)), p_117))))), 0x0.0p+1));
                        l_260 = (((safe_mul_func_int16_t_s_s((((safe_add_func_int32_t_s_s(l_210, (~((9L == ((l_232 | (safe_div_func_int64_t_s_s((safe_sub_func_int32_t_s_s(p_116, (~(l_241[6] = (((-4L) > (g_196.f0 <= g_167)) , g_175[1][0]))))), (safe_lshift_func_uint16_t_u_s((*g_94), g_174[2]))))) <= (-3L))) | 0x279D215AL)))) , l_258) == (void*)0), l_210)) , 0x15L) , (-0x1.4p-1));
                        l_241[3] = 0x3.17E4DAp-46;
                        if (g_5)
                            break;
                    }
                    if (((((((safe_div_func_int16_t_s_s(4L, (*g_94))) && ((safe_rshift_func_uint16_t_u_u(0xCC53L, 14)) , (((*l_190) = &g_140) != &g_140))) & (safe_mul_func_uint8_t_u_u(((safe_sub_func_int8_t_s_s((&g_167 == (((((&g_93 == (void*)0) , g_271) , p_118) , l_210) , (void*)0)), 9UL)) ^ 6L), p_117))) & 0UL) >= (**l_143)) && 65529UL))
                    { /* block id: 110 */
                        float *l_272 = (void*)0;
                        float **l_273 = &l_272;
                        int8_t **l_279 = &l_177;
                        int64_t *l_292 = &g_167;
                        int32_t l_306 = 0xEB692CA0L;
                        int32_t l_307 = 0L;
                        l_280 |= (l_210 |= ((((((*l_273) = l_272) == p_119) , ((0xB5E4L || (+((safe_mod_func_uint64_t_u_u((g_169[0].f0 >= (g_277[3] , 0xB3CD36BC43A748A0LL)), (((p_116 , l_278) == (((((*l_279) = &g_29) != &g_174[0]) >= p_118) , (void*)0)) , l_241[4]))) ^ p_117))) & g_175[0][1])) , p_117) || p_118));
                        g_281++;
                        l_307 ^= ((g_284 , (safe_mod_func_int8_t_s_s(1L, ((*g_94) || (safe_sub_func_uint64_t_u_u((((safe_unary_minus_func_int64_t_s(((*l_292) = ((*l_185) = (safe_rshift_func_uint16_t_u_u((*g_94), 6)))))) || 0x0BD12985L) >= (safe_sub_func_int8_t_s_s((l_210 = (safe_mod_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(((safe_unary_minus_func_int64_t_s(g_165)) < ((safe_rshift_func_int8_t_s_u(p_118, 5)) <= 0x1CB9L)), 5)), (safe_lshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(l_306, l_241[4])), 3))))), 5UL))), 1L)))))) , l_260);
                        (*l_144) = &l_210;
                    }
                    else
                    { /* block id: 121 */
                        (**l_190) = p_119;
                        if ((**g_139))
                            continue;
                    }
                }
                for (l_176 = (-24); (l_176 <= (-29)); l_176 = safe_sub_func_int8_t_s_s(l_176, 9))
                { /* block id: 128 */
                    for (g_218 = 0; (g_218 >= 37); ++g_218)
                    { /* block id: 131 */
                        (*l_143) = p_115;
                    }
                }
                if ((((g_271.f0 > (((*l_329) &= ((((*l_172) = ((p_116 , p_115) != p_115)) , ((safe_rshift_func_int16_t_s_u(((((p_117 &= p_116) < ((*p_119) || g_80)) != (safe_lshift_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s((((((safe_rshift_func_int16_t_s_s((((0UL <= ((safe_mod_func_int32_t_s_s((l_191 != g_324), p_118)) <= l_328)) <= p_116) >= 0UL), 10)) , p_116) != p_118) == (*g_94)) || 4294967295UL), p_116)), g_174[3])) == l_241[6]), 5))) <= p_116), 12)) == l_232)) > p_118)) | 4L)) | p_116) > g_29))
                { /* block id: 138 */
                    uint16_t l_369 = 0x7F54L;
                    float *l_386 = &g_387;
                    float *l_394 = &g_395;
                    float *l_396 = &g_397;
                    l_178 ^= (252UL || l_241[6]);
                    for (g_88 = (-16); (g_88 != 49); g_88++)
                    { /* block id: 142 */
                        const uint16_t * const l_368 = &g_23[0];
                        const uint16_t * const *l_367 = &l_368;
                        const uint16_t * const **l_366[4][7][4] = {{{(void*)0,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,(void*)0},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367}},{{&l_367,&l_367,&l_367,&l_367},{&l_367,(void*)0,&l_367,&l_367},{&l_367,(void*)0,&l_367,&l_367},{(void*)0,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{(void*)0,&l_367,&l_367,(void*)0},{&l_367,&l_367,&l_367,&l_367}},{{(void*)0,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,(void*)0},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367}},{{&l_367,(void*)0,&l_367,&l_367},{&l_367,(void*)0,&l_367,&l_367},{(void*)0,&l_367,&l_367,&l_367},{&l_367,&l_367,&l_367,&l_367},{(void*)0,&l_367,&l_367,(void*)0},{&l_367,&l_367,&l_367,&l_367},{(void*)0,&l_367,&l_367,&l_367}}};
                        int32_t l_370 = (-3L);
                        int i, j, k;
                        l_370 = (safe_mod_func_uint16_t_u_u(((safe_mod_func_uint8_t_u_u((((+(safe_mod_func_uint8_t_u_u(((-1L) || (((safe_add_func_int32_t_s_s((safe_mod_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((((safe_mod_func_uint32_t_u_u((safe_lshift_func_int8_t_s_s(p_118, (((((((safe_rshift_func_int16_t_s_u((g_169[0] , (+(p_118 <= p_118))), 12)) >= ((safe_add_func_int64_t_s_s(0x75F50E9BAB346613LL, (safe_sub_func_int16_t_s_s(((safe_mod_func_int64_t_s_s((g_151 = (safe_rshift_func_int8_t_s_u(((p_116 ^ (((l_366[1][1][1] != (void*)0) ^ g_34) & l_369)) == p_116), p_117))), 18446744073709551615UL)) >= 0xC6L), 0xA8EEL)))) , p_118)) != l_369) , &g_153) != (void*)0) == l_370) , p_116))), 0xC9798B30L)) , l_369) > p_116) < 0L), 2L)), g_29)), p_117)), 0x23C689B7L)) , (void*)0) != (void*)0)), l_371))) && g_34) <= 0xB6874CD0L), g_47)) | (*g_94)), 0xF4BBL));
                    }
                    l_201 = 1L;
                    (*l_396) = ((*l_394) = ((((-0x3.Dp+1) == ((l_369 != (safe_div_func_float_f_f(((*l_278) = (safe_div_func_float_f_f((safe_mul_func_float_f_f(((safe_div_func_float_f_f((p_118 , ((*g_213) >= (((safe_add_func_float_f_f((-(-(((*l_386) = ((0x7CL < (safe_mod_func_uint64_t_u_u(0UL, p_116))) , 0x0.Dp-1)) != ((safe_add_func_float_f_f(((safe_sub_func_float_f_f((safe_mul_func_float_f_f(0xE.480CF2p+79, p_118)), p_117)) == p_117), p_118)) , 0x1.3p-1)))), l_232)) < 0x1.482713p-27) == 0xF.9C1AF2p+10))), g_169[0].f0)) < (-0x2.2p-1)), g_47)), p_117))), l_232))) < p_116)) != g_174[2]) > 0xD.E95580p+93));
                }
                else
                { /* block id: 151 */
                    int16_t l_398[2];
                    const int32_t * volatile l_424 = &g_237;/* VOLATILE GLOBAL l_424 */
                    int32_t l_426[7][6][1] = {{{7L},{0x0FF412F7L},{(-5L)},{0x0FF412F7L},{7L},{0x114083A8L}},{{(-1L)},{6L},{(-1L)},{0x114083A8L},{7L},{0x0FF412F7L}},{{(-5L)},{0x0FF412F7L},{7L},{0x114083A8L},{(-1L)},{6L}},{{(-1L)},{0x114083A8L},{7L},{0x0FF412F7L},{(-5L)},{0x0FF412F7L}},{{7L},{0x114083A8L},{(-1L)},{6L},{(-1L)},{0x114083A8L}},{{7L},{0x0FF412F7L},{(-5L)},{0x0FF412F7L},{7L},{0x114083A8L}},{{(-1L)},{6L},{(-1L)},{0x114083A8L},{7L},{0x0FF412F7L}}};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_398[i] = 0x722AL;
                    g_5 ^= l_398[0];
                    for (l_201 = 0; (l_201 == 20); ++l_201)
                    { /* block id: 155 */
                        int8_t l_416[2];
                        int32_t l_417 = 0x839243F1L;
                        int32_t l_418 = 1L;
                        int32_t l_427 = 0x58A45DB5L;
                        int32_t l_428 = 1L;
                        int32_t l_430[10][7] = {{0x623F3E8BL,0x623F3E8BL,0x2C1FEB01L,0xDEA679EBL,(-1L),5L,0x7B847111L},{0x7B847111L,1L,1L,(-1L),1L,1L,(-7L)},{0x53EBCDE8L,(-1L),0x4B4920C3L,2L,(-1L),6L,2L},{0xDEA679EBL,0x4CCA799AL,0xF414B340L,(-8L),(-7L),0x02E60B7AL,0x7B847111L},{0L,1L,0x53EBCDE8L,(-8L),0x53EBCDE8L,1L,0L},{1L,0L,1L,2L,0x623F3E8BL,0x53EBCDE8L,5L},{0xFAE33703L,0L,(-8L),0xFAE33703L,1L,0x1308A2A7L,0x4B4920C3L},{0xBDB6203BL,0x02E60B7AL,0xF414B340L,1L,0xDACCB832L,0xF414B340L,0x53EBCDE8L},{0xDACCB832L,0xBDB6203BL,0x11B8DE1BL,0x2C1FEB01L,0L,0xF1E7EB24L,0xF1E7EB24L},{1L,0xBDB6203BL,0L,0xBDB6203BL,1L,(-1L),0L}};
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_416[i] = 1L;
                        l_416[1] ^= (safe_sub_func_int8_t_s_s(g_174[2], (0x9209L > (((safe_div_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(((safe_rshift_func_int8_t_s_u(0x5DL, 4)) , (safe_sub_func_int64_t_s_s((&l_143 == (g_411 , l_190)), (safe_lshift_func_uint16_t_u_u((p_116 || ((safe_mul_func_int16_t_s_s((((void*)0 != &g_151) & p_117), p_116)) >= (-1L))), 2))))), p_116)), l_398[0])) >= p_117) || p_117))));
                        ++g_421;
                        l_424 = (**g_325);
                        --g_431;
                    }
                    (*g_435) = l_434;
                    l_419[7][2] |= l_241[7];
                }
                (*l_278) = p_117;
            }
            else
            { /* block id: 165 */
                uint32_t l_464 = 9UL;
                int16_t l_485[6][4][3] = {{{8L,0L,8L},{1L,1L,1L},{8L,0L,8L},{1L,1L,1L}},{{8L,0L,8L},{1L,1L,1L},{8L,0L,8L},{1L,1L,1L}},{{8L,0L,8L},{1L,1L,1L},{8L,0L,8L},{1L,1L,1L}},{{8L,0L,8L},{1L,1L,1L},{8L,0L,8L},{1L,1L,1L}},{{8L,0L,8L},{1L,1L,1L},{8L,0L,8L},{1L,1L,1L}},{{8L,0L,8L},{1L,1L,1L},{8L,0L,8L},{1L,1L,1L}}};
                int i, j, k;
                if (((4294967291UL == ((((~(g_411 , ((l_216 = ((safe_add_func_int32_t_s_s(1L, ((safe_mul_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(0xB9B8L, g_47)), ((void*)0 != &g_175[0][1]))) , 0UL))) || (g_217[2][0] , g_80))) > p_116))) != 1L) > 4L) , (*l_147))) > (**l_143)))
                { /* block id: 167 */
                    uint8_t l_467 = 0xF9L;
                    for (g_88 = (-10); (g_88 == 36); g_88 = safe_add_func_int8_t_s_s(g_88, 4))
                    { /* block id: 170 */
                        int32_t l_451 = 0L;
                        int16_t *l_461 = &g_153;
                        int32_t l_465 = (-10L);
                        int32_t l_466 = 0L;
                        (*g_213) = p_117;
                        l_466 &= (((safe_div_func_uint32_t_u_u(0x5CBC03E0L, (safe_sub_func_uint32_t_u_u(l_451, ((void*)0 == (*g_435)))))) || ((g_421++) <= 0x49501CF5L)) , (g_237 ^= ((safe_lshift_func_int8_t_s_s(((*l_173) |= ((safe_lshift_func_uint16_t_u_s((safe_mul_func_int16_t_s_s(((-(((l_461 == &g_153) <= (l_451 |= (safe_sub_func_uint8_t_u_u((1UL ^ (((0xE4B5D248L > g_277[3].f1) , p_116) || p_118)), l_464)))) , 0x4.4D0107p-40)) , 0x6430L), l_465)), 1)) , (-1L))), 5)) == g_411.f0)));
                    }
                    l_467--;
                    for (l_371 = 0; (l_371 <= 3); l_371 += 1)
                    { /* block id: 181 */
                        return g_470;
                    }
                    for (g_80 = 0; (g_80 > (-4)); g_80 = safe_sub_func_uint8_t_u_u(g_80, 1))
                    { /* block id: 186 */
                        int16_t l_473 = 0x653EL;
                        uint8_t *l_484 = &l_192;
                        l_419[1][2] = (((**g_93) <= l_473) > ((safe_unary_minus_func_uint8_t_u(((((safe_rshift_func_uint16_t_u_u(((g_477 = 0x99L) || (safe_add_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((p_117 ^ ((safe_div_func_uint8_t_u_u((((6L & 255UL) <= 249UL) <= (*p_119)), ((*l_484) = 0x05L))) & l_485[4][2][1])), g_169[0].f0)), (-1L)))), (*g_94))) != (**g_93)) , 5L) == g_411.f1))) , 0UL));
                    }
                }
                else
                { /* block id: 191 */
                    float **l_489 = &l_488;
                    int32_t l_494 = 0xED3156D3L;
                    if (l_486)
                    { /* block id: 192 */
                        l_489 = l_487;
                        (*g_326) = (*g_326);
                    }
                    else
                    { /* block id: 195 */
                        uint16_t l_490 = 65535UL;
                        --l_490;
                        (***g_324) = (*g_326);
                        if (l_176)
                            goto lbl_493;
                        (**l_489) = (l_494 > (safe_div_func_float_f_f(l_490, p_117)));
                    }
                    for (g_420 = 17; (g_420 > (-14)); --g_420)
                    { /* block id: 203 */
                        return g_470;
                    }
                    l_419[5][2] ^= (-4L);
                }
            }
        }
        else
        { /* block id: 209 */
            int16_t l_566 = 0x54B5L;
            int8_t *l_593 = &g_29;
            uint64_t l_599 = 18446744073709551615UL;
            int32_t l_611 = (-8L);
            int32_t l_612[5];
            float *l_683[8] = {&g_397,(void*)0,&g_397,(void*)0,&g_397,(void*)0,&g_397,(void*)0};
            int64_t **l_790 = &g_718[5][0];
            int16_t l_791 = 0x0E95L;
            uint64_t l_810 = 0UL;
            uint16_t l_834 = 6UL;
            int32_t ****l_842 = &l_190;
            int32_t *****l_841 = &l_842;
            int i;
            for (i = 0; i < 5; i++)
                l_612[i] = 0xC8E9CAF6L;
            for (g_421 = 0; (g_421 == 5); g_421++)
            { /* block id: 212 */
                int8_t l_541 = 0xCEL;
                int64_t *l_545 = &g_167;
                uint32_t l_570[7];
                uint32_t l_571 = 3UL;
                uint16_t **l_588 = (void*)0;
                int32_t l_591 = (-1L);
                int i;
                for (i = 0; i < 7; i++)
                    l_570[i] = 18446744073709551615UL;
                for (l_176 = 6; (l_176 >= 0); l_176 -= 1)
                { /* block id: 215 */
                    uint32_t l_501 = 0xC5F60F43L;
                    uint8_t *l_533 = (void*)0;
                    uint8_t **l_532 = &l_533;
                    int32_t l_539 = 0x97084429L;
                    int16_t *l_540 = &g_153;
                    int64_t *l_547[7] = {&g_151,&g_151,&g_151,&g_151,&g_151,&g_151,&g_151};
                    int64_t **l_546 = &l_547[5];
                    int i;
                    for (l_371 = 1; (l_371 <= 9); l_371 += 1)
                    { /* block id: 218 */
                        const uint32_t * const **l_504 = &g_502[2];
                        g_5 |= ((l_501 = g_29) | (((*l_504) = g_502[0]) == l_505));
                        if (g_470.f0)
                            goto lbl_493;
                    }
                    l_541 &= ((((safe_unary_minus_func_uint16_t_u((((safe_sub_func_uint8_t_u_u((((safe_div_func_uint32_t_u_u((((safe_mod_func_int8_t_s_s((g_513 , ((*l_172) = (safe_mul_func_int16_t_s_s(g_175[0][1], (safe_add_func_int64_t_s_s(p_118, (safe_mul_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(((*l_173) &= (safe_mod_func_uint16_t_u_u((((*l_540) = (safe_div_func_uint16_t_u_u((0x3CL | ((safe_mul_func_int16_t_s_s((0xC100141ED1AC72E4LL > ((safe_mul_func_int8_t_s_s((((*l_532) = &l_192) == g_437), ((l_539 = (safe_mod_func_uint64_t_u_u(((safe_lshift_func_uint16_t_u_u(0xE035L, ((safe_unary_minus_func_uint64_t_u(g_217[1][0])) == l_501))) != 1UL), p_118))) & l_260))) , 0x8179770184F10F68LL)), (-9L))) , p_118)), 65527UL))) <= (*g_94)), p_117))), p_118)), p_118)), 8L)))))))), 248UL)) , 0x54FFL) | 0x7BDFL), (*g_503))) == p_117) != p_118), p_118)) & p_118) | 1L))) < p_117) > p_118) > g_23[1]);
                    if ((l_216 |= (g_542 , (safe_div_func_int64_t_s_s(((l_545 != ((*l_546) = &g_477)) != (((*l_173) &= (p_117 < 18446744073709551615UL)) != (((void*)0 == &l_258) == (safe_mul_func_int16_t_s_s(p_118, ((*l_540) = 0xCA21L)))))), 0x733357E4F9625D5ALL)))))
                    { /* block id: 234 */
                        float *l_567 = &g_395;
                        int32_t l_574 = 1L;
                        int32_t l_575 = 0x23419B1AL;
                        int i, j;
                        (*l_278) = (safe_add_func_float_f_f((!(l_539 = ((((safe_div_func_float_f_f((-0x10.Ap+1), (safe_add_func_float_f_f((safe_div_func_float_f_f(g_2[l_176], (safe_add_func_float_f_f(g_2[l_176], (((l_561 == g_562) , (safe_div_func_float_f_f(((*l_567) = l_566), p_117))) == (safe_mul_func_float_f_f(p_116, ((l_566 != l_566) != l_566)))))))), p_118)))) <= l_570[1]) != l_566) > l_541))), l_571));
                        (*g_572) = &g_435;
                        if (p_117)
                            break;
                        g_577--;
                    }
                    else
                    { /* block id: 241 */
                        uint8_t l_592 = 0x17L;
                        if (p_116)
                            goto lbl_493;
                        l_539 &= (l_592 = (safe_div_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((!((l_591 ^= (((safe_mul_func_int16_t_s_s((p_118 && p_118), (g_587 , (l_588 == (void*)0)))) , (p_116 != p_118)) , (safe_add_func_uint64_t_u_u(1UL, p_116)))) && 0x49837155A935A547LL)), p_116)), (*l_147))));
                    }
                }
            }
            if ((l_593 == l_593))
            { /* block id: 249 */
                int64_t l_610 = (-1L);
                int32_t l_628 = 0L;
                uint32_t ***l_675[2];
                uint16_t * const l_690 = &g_431;
                const uint8_t *l_695[3][7][10] = {{{&l_192,&l_192,&l_192,&l_192,&g_677,(void*)0,&l_192,&l_192,&g_677,&g_677},{(void*)0,(void*)0,&l_192,(void*)0,&l_192,(void*)0,&l_192,&l_192,(void*)0,&g_677},{(void*)0,&l_192,&g_677,&g_677,&g_677,&g_677,&l_192,&l_192,&l_192,&l_192},{&l_192,&l_192,&l_192,&g_677,(void*)0,&g_677,&l_192,&l_192,&l_192,(void*)0},{&g_677,&l_192,(void*)0,&l_192,&l_192,&l_192,&l_192,&l_192,&l_192,&g_677},{&g_677,(void*)0,&l_192,&l_192,(void*)0,&g_677,&l_192,&l_192,&l_192,&g_677},{&g_677,&l_192,&l_192,&g_677,&g_677,&l_192,&l_192,&g_677,&l_192,&l_192}},{{&g_677,&g_677,&g_677,&g_677,&g_677,&g_677,&g_677,&l_192,(void*)0,&l_192},{&g_677,&g_677,&l_192,(void*)0,&l_192,&l_192,&l_192,&l_192,(void*)0,&l_192},{(void*)0,(void*)0,(void*)0,&l_192,(void*)0,(void*)0,&l_192,&g_677,&g_677,&l_192},{&l_192,(void*)0,&l_192,&l_192,(void*)0,&l_192,(void*)0,&g_677,&l_192,&l_192},{(void*)0,&l_192,&g_677,(void*)0,&l_192,(void*)0,(void*)0,(void*)0,(void*)0,&l_192},{&l_192,&l_192,&l_192,&l_192,&l_192,(void*)0,(void*)0,&l_192,&g_677,&l_192},{&l_192,(void*)0,(void*)0,&l_192,&g_677,(void*)0,&l_192,(void*)0,&g_677,&g_677}},{{&l_192,(void*)0,&g_677,&l_192,&l_192,&l_192,&l_192,(void*)0,(void*)0,&l_192},{&l_192,&g_677,&g_677,(void*)0,&g_677,&l_192,&g_677,(void*)0,&l_192,(void*)0},{&l_192,&g_677,(void*)0,&l_192,&g_677,(void*)0,&l_192,&l_192,&g_677,&g_677},{&l_192,(void*)0,(void*)0,&l_192,&g_677,(void*)0,&l_192,(void*)0,(void*)0,&l_192},{&l_192,&l_192,&g_677,(void*)0,&l_192,(void*)0,&l_192,(void*)0,(void*)0,&l_192},{(void*)0,&l_192,&g_677,&l_192,&g_677,&l_192,(void*)0,(void*)0,&l_192,&g_677},{&l_192,&l_192,(void*)0,(void*)0,&g_677,(void*)0,(void*)0,&l_192,&l_192,(void*)0}}};
                int32_t l_723 = 0L;
                int32_t l_727 = 0x2678B5A4L;
                uint64_t l_750 = 2UL;
                int8_t **l_767 = &l_173;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_675[i] = &l_505;
                if (((safe_unary_minus_func_uint16_t_u(((g_595[1][9][4] , (safe_add_func_uint8_t_u_u((((!l_599) || (safe_sub_func_uint32_t_u_u((*l_147), (((l_190 != ((2L | ((-1L) != (safe_mul_func_uint8_t_u_u(0xE5L, ((-3L) <= (((!(&g_503 == &p_115)) , p_117) ^ 0x2D6BL)))))) , (*g_324))) ^ (-5L)) == 0x33L)))) > g_151), p_118))) <= 0x5BL))) >= (*p_119)))
                { /* block id: 250 */
                    (***g_324) = (*g_139);
                    return g_605;
                }
                else
                { /* block id: 253 */
                    const int64_t **l_607 = (void*)0;
                    int32_t l_609 = 0x29303491L;
                    int32_t l_613 = (-1L);
                    int32_t l_615 = 0x4FF1A4C7L;
                    for (g_151 = 0; (g_151 <= 1); g_151 += 1)
                    { /* block id: 256 */
                        int32_t *l_606 = &g_80;
                        const int64_t ***l_608 = &l_607;
                        (*g_139) = l_606;
                        (*l_608) = l_607;
                        (***l_190) = 0x4ADC3DF8L;
                        (*g_326) = p_119;
                    }
                    if ((l_609 < (**l_143)))
                    { /* block id: 262 */
                        --g_618;
                        (***g_324) = (***g_324);
                        l_613 = ((safe_rshift_func_int16_t_s_u((!g_175[0][1]), 9)) && p_118);
                    }
                    else
                    { /* block id: 266 */
                        (*l_278) = 0xA.594716p-54;
                    }
                }
                for (l_566 = 0; (l_566 != 22); l_566++)
                { /* block id: 272 */
                    uint8_t *l_627 = &l_192;
                    uint8_t **l_626 = &l_627;
                    int32_t l_629 = 0x9C5D2D45L;
                    int32_t l_637 = (-1L);
                    uint32_t l_644[4][8] = {{1UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL},{18446744073709551615UL,1UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{1UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL}};
                    int i, j;
                    l_628 |= (((void*)0 != l_626) && g_29);
                    if ((l_629 = (l_628 &= p_117)))
                    { /* block id: 276 */
                        const int16_t l_632 = 4L;
                        g_5 = (((safe_rshift_func_int8_t_s_u(l_632, (safe_sub_func_uint32_t_u_u((safe_add_func_uint8_t_u_u(g_617[1], l_629)), (((((l_637 |= p_116) || g_284.f1) , (safe_div_func_int32_t_s_s((safe_mul_func_int16_t_s_s(((**g_573) == (((&p_115 == (void*)0) <= ((safe_add_func_uint16_t_u_u((g_23[0] >= l_610), (*g_94))) ^ l_644[2][4])) , (void*)0)), l_610)), l_610))) >= l_632) , 0xB7142BA4L))))) , l_190) != (void*)0);
                    }
                    else
                    { /* block id: 279 */
                        uint64_t l_645 = 0xB553B662DB054510LL;
                        int64_t *l_646 = &g_151;
                        int32_t l_658 = 0x1CDDD9A3L;
                        const uint8_t **l_678 = &g_676;
                        uint64_t *l_679 = &l_599;
                        l_628 = (((*l_646) = l_645) ^ ((*l_679) ^= (253UL | (safe_mul_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((safe_mul_func_int8_t_s_s(((safe_unary_minus_func_uint8_t_u((((safe_div_func_int8_t_s_s((l_658 ^= l_645), (safe_mul_func_int16_t_s_s((safe_sub_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((safe_add_func_int32_t_s_s((0x493DL <= (safe_add_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s((18446744073709551606UL && ((safe_mod_func_uint8_t_u_u(((l_675[0] != l_675[0]) & (((*l_678) = g_676) != (void*)0)), p_116)) > l_566)), p_117)), p_117))), 0x0399FCCAL)), (*p_119))), 12)), 0x37760870L)), 0x4284L)))) == 255UL) <= l_645))) >= 0xB6L), (-3L))), g_23[1])), 0xB4L)), (**g_93))))));
                    }
                    for (g_167 = 0; (g_167 <= 2); g_167 += 1)
                    { /* block id: 288 */
                        int i, j;
                        if (g_217[(g_167 + 4)][(g_167 + 4)])
                            break;
                        if (g_617[g_167])
                            continue;
                    }
                }
                for (g_151 = 1; (g_151 <= 7); g_151 += 1)
                { /* block id: 295 */
                    int16_t l_687[5];
                    int64_t ***l_715 = (void*)0;
                    int32_t l_721 = 0x5363E0BCL;
                    union U1 *l_752 = &g_513;
                    int8_t l_768 = 0xB6L;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_687[i] = 1L;
                    if ((safe_lshift_func_int8_t_s_u((((g_682 , l_683[2]) == (void*)0) ^ ((!((g_397 = (safe_sub_func_float_f_f((l_687[3] > (safe_div_func_float_f_f((&g_94 != (void*)0), (l_690 != ((safe_rshift_func_int8_t_s_s((safe_mul_func_uint64_t_u_u(g_605.f0, (l_695[0][6][6] != (void*)0))), 4)) , (void*)0))))), p_116))) , p_117)) | (*p_119))), (*g_676))))
                    { /* block id: 297 */
                        (*g_139) = ((*l_143) = p_115);
                        if (p_116)
                            break;
                        if ((*l_147))
                            break;
                        g_697--;
                    }
                    else
                    { /* block id: 303 */
                        uint32_t l_703[1][9] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}};
                        int64_t ****l_719 = (void*)0;
                        int64_t ****l_720 = &g_716;
                        int i, j;
                        l_721 = (!(((safe_mul_func_int16_t_s_s(l_703[0][1], ((safe_sub_func_int8_t_s_s(0xEEL, (safe_lshift_func_uint16_t_u_s((((1L != ((safe_div_func_uint16_t_u_u((65528UL || (safe_unary_minus_func_uint32_t_u(((safe_add_func_uint32_t_u_u((safe_add_func_int32_t_s_s((l_216 = 0x4BC9DBFDL), (l_715 != ((*l_720) = g_716)))), (p_118 &= 0x4E88CF03L))) | ((0UL == 0xA3L) != 7UL))))), l_703[0][1])) > 0x0C03L)) & p_117) > 1UL), 11)))) || 0x1E29L))) , g_169[0].f1) , (-1L)));
                    }
                    if (p_118)
                        break;
                    for (l_566 = 0; (l_566 <= 7); l_566 += 1)
                    { /* block id: 312 */
                        int32_t l_722 = 0xA6F8D2A9L;
                        int32_t l_725 = (-10L);
                        int32_t l_726 = 0x29819D59L;
                        int32_t l_728 = 0x741FFE7EL;
                        uint8_t l_751 = 0x6BL;
                        int i, j;
                        (***g_324) = g_212[l_566][l_566];
                        --g_729;
                        (*g_716) = (g_732 , l_733);
                        l_628 = ((safe_div_func_int8_t_s_s((safe_lshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s(p_118, p_116)), (((safe_add_func_float_f_f((((safe_div_func_float_f_f(((l_722 = (-((-0x10.Fp-1) > ((g_284.f1 > (safe_unary_minus_func_uint64_t_u((((void*)0 != l_715) | 0xD0L)))) , (g_277[0] , ((((((safe_mod_func_int16_t_s_s((l_628 == p_116), 0x0170L)) || (-6L)) & (*g_503)) == l_687[1]) && 0x0F153890FDB21BE9LL) , g_2[(g_151 + 2)])))))) != 0x3.FA9B88p+16), l_750)) <= (-0x6.Ep+1)) <= l_751), g_174[2])) , (-1L)) <= p_118))), g_29)), 255UL)) ^ p_118);
                    }
                    for (l_192 = 0; (l_192 <= 4); l_192 += 1)
                    { /* block id: 321 */
                        union U1 **l_755 = &l_752;
                        uint32_t *l_762 = &l_371;
                        int32_t l_769 = 0xE5FA4010L;
                        int i, j;
                        (*g_753) = l_752;
                        (*l_278) = (l_755 != (void*)0);
                        l_612[4] = (safe_mul_func_float_f_f(((((((+((((void*)0 != (*g_716)) == (safe_add_func_float_f_f((l_612[l_192] <= (((l_612[l_192] < ((*l_278) = p_116)) < (-((g_697 = (++(*l_762))) , p_118))) == (l_768 = (safe_mul_func_float_f_f(((void*)0 == l_767), (l_612[l_192] != l_721)))))), 0x1.Ep+1))) >= 0x1.Ap+1)) < p_116) != p_118) >= l_769) > l_599) != l_610), p_117));
                    }
                }
            }
            else
            { /* block id: 331 */
                int8_t *l_773 = &g_617[1];
                uint32_t l_785 = 2UL;
                uint64_t l_792 = 18446744073709551607UL;
                int32_t l_793 = 0xAAA94631L;
                union U1 *l_798 = &g_595[1][1][4];
                int32_t l_800 = 1L;
                int32_t l_801 = 1L;
                int32_t l_802 = 0L;
                int32_t l_804 = (-7L);
                int32_t l_805 = 0x11B13862L;
                if ((safe_unary_minus_func_uint32_t_u((((l_793 = (((safe_rshift_func_int16_t_s_u((((void*)0 != l_773) <= ((0x1B50L == ((**g_93) = (l_612[3] = (safe_mod_func_uint32_t_u_u((p_116 && ((safe_unary_minus_func_uint64_t_u(1UL)) >= (safe_mul_func_int8_t_s_s((safe_mod_func_uint32_t_u_u((safe_div_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(0xA3L, (((((((l_785 < ((0x4EL | (safe_mul_func_int16_t_s_s(l_785, 3UL))) ^ 1L)) | 0xB93795F6L) == l_788) , g_789) , l_790) != (*g_716)) | (*g_676)))), p_116)), 0xEFB5878AL)), l_791)))), (*p_119)))))) , (***g_716))), l_792)) <= p_118) & 0x23L)) < p_118) | p_117))))
                { /* block id: 335 */
                    uint32_t l_794 = 4294967289UL;
                    int32_t l_797 = 1L;
                    --l_794;
                    l_797 = 0x2498A665L;
                    return g_277[3];
                }
                else
                { /* block id: 339 */
                    union U1 **l_799[1];
                    int32_t l_803 = 1L;
                    int32_t l_806[4][6][6] = {{{0x07A2E1A8L,(-1L),0xC13B6CC2L,(-1L),0x4D15A572L,0xACEF57FFL},{0x9BD98AFBL,(-1L),0xF124FF49L,(-1L),0L,0xACEF57FFL},{0xB89D6259L,5L,0xC13B6CC2L,0xC68B49CEL,0x4EF459C9L,(-1L)},{0L,0x3ACAC33BL,0x9BD98AFBL,(-9L),0x7D214BD5L,(-1L)},{5L,(-9L),0x4D15A572L,0x3ACAC33BL,9L,(-1L)},{(-1L),0xC68B49CEL,0x9BD98AFBL,0xC68B49CEL,(-1L),(-1L)}},{{9L,0x3ACAC33BL,0x4D15A572L,(-9L),5L,(-1L)},{0x7D214BD5L,(-9L),0x9BD98AFBL,0x3ACAC33BL,0L,(-1L)},{0x4EF459C9L,0xC68B49CEL,0x4D15A572L,0xC68B49CEL,0x4EF459C9L,(-1L)},{0L,0x3ACAC33BL,0x9BD98AFBL,(-9L),0x7D214BD5L,(-1L)},{5L,(-9L),0x4D15A572L,0x3ACAC33BL,9L,(-1L)},{(-1L),0xC68B49CEL,0x9BD98AFBL,0xC68B49CEL,(-1L),(-1L)}},{{9L,0x3ACAC33BL,0x4D15A572L,(-9L),5L,(-1L)},{0x7D214BD5L,(-9L),0x9BD98AFBL,0x3ACAC33BL,0L,(-1L)},{0x4EF459C9L,0xC68B49CEL,0x4D15A572L,0xC68B49CEL,0x4EF459C9L,(-1L)},{0L,0x3ACAC33BL,0x9BD98AFBL,(-9L),0x7D214BD5L,(-1L)},{5L,(-9L),0x4D15A572L,0x3ACAC33BL,9L,(-1L)},{(-1L),0xC68B49CEL,0x9BD98AFBL,0xC68B49CEL,(-1L),(-1L)}},{{9L,0x3ACAC33BL,0x4D15A572L,(-9L),5L,(-1L)},{0x7D214BD5L,(-9L),0x9BD98AFBL,0x3ACAC33BL,0L,(-1L)},{0x4EF459C9L,0xC68B49CEL,0x4D15A572L,0xC68B49CEL,0x4EF459C9L,(-1L)},{0L,0x3ACAC33BL,0x9BD98AFBL,(-9L),0x7D214BD5L,(-1L)},{5L,(-9L),0x4D15A572L,0x3ACAC33BL,9L,(-1L)},{(-1L),0xC68B49CEL,0x9BD98AFBL,0xC68B49CEL,(-1L),(-1L)}}};
                    uint8_t *l_835 = (void*)0;
                    uint8_t *l_836 = &g_677;
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_799[i] = &l_798;
                    l_798 = l_798;
                    g_807--;
                    ++l_810;
                    if ((((safe_mul_func_uint8_t_u_u(((*l_836) = (safe_sub_func_uint32_t_u_u(((safe_mod_func_uint8_t_u_u((*g_676), 0xD6L)) <= (((safe_mul_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((safe_add_func_int64_t_s_s(((l_834 = (~((safe_sub_func_uint32_t_u_u((l_599 , (((*g_213) = (((safe_sub_func_float_f_f((l_803 != (safe_sub_func_float_f_f((((*g_213) > (g_411.f0 <= (g_395 = ((safe_div_func_float_f_f((((void*)0 == &p_118) , 0x5.4p-1), p_117)) == p_118)))) > (-0x1.9p+1)), g_23[0]))), g_237)) != (-0x1.0p+1)) == g_2[1])) , 1UL)), 9UL)) || l_806[0][1][2]))) , p_117), p_118)), l_566)), p_116)) , (void*)0) == (void*)0)), l_566))), p_116)) || (-8L)) != l_810))
                    { /* block id: 347 */
                        g_837 = &g_605;
                    }
                    else
                    { /* block id: 349 */
                        (***g_324) = (*g_326);
                        return g_838;
                    }
                }
                l_839 ^= p_116;
                l_841 = l_840;
            }
        }
        (*g_572) = (void*)0;
    }
    return (*g_837);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_2[i], sizeof(g_2[i]), "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_23[i], "g_23[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_29, "g_29", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_80, "g_80", print_hash_value);
    transparent_crc(g_88, "g_88", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_111[i], "g_111[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_151, "g_151", print_hash_value);
    transparent_crc(g_153, "g_153", print_hash_value);
    transparent_crc(g_165, "g_165", print_hash_value);
    transparent_crc(g_167, "g_167", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_169[i].f0, "g_169[i].f0", print_hash_value);
        transparent_crc(g_169[i].f1, "g_169[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_174[i], "g_174[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_175[i][j], "g_175[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_196.f0, "g_196.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_217[i][j], "g_217[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_218, "g_218", print_hash_value);
    transparent_crc(g_237, "g_237", print_hash_value);
    transparent_crc(g_271.f0, "g_271.f0", print_hash_value);
    transparent_crc(g_271.f1, "g_271.f1", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_277[i].f0, "g_277[i].f0", print_hash_value);
        transparent_crc(g_277[i].f1, "g_277[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_281, "g_281", print_hash_value);
    transparent_crc(g_284.f0, "g_284.f0", print_hash_value);
    transparent_crc(g_284.f1, "g_284.f1", print_hash_value);
    transparent_crc_bytes (&g_387, sizeof(g_387), "g_387", print_hash_value);
    transparent_crc_bytes (&g_395, sizeof(g_395), "g_395", print_hash_value);
    transparent_crc_bytes (&g_397, sizeof(g_397), "g_397", print_hash_value);
    transparent_crc(g_411.f0, "g_411.f0", print_hash_value);
    transparent_crc(g_411.f1, "g_411.f1", print_hash_value);
    transparent_crc(g_420, "g_420", print_hash_value);
    transparent_crc(g_421, "g_421", print_hash_value);
    transparent_crc(g_425, "g_425", print_hash_value);
    transparent_crc(g_431, "g_431", print_hash_value);
    transparent_crc(g_470.f0, "g_470.f0", print_hash_value);
    transparent_crc(g_470.f1, "g_470.f1", print_hash_value);
    transparent_crc(g_477, "g_477", print_hash_value);
    transparent_crc(g_513.f0, "g_513.f0", print_hash_value);
    transparent_crc(g_542.f0, "g_542.f0", print_hash_value);
    transparent_crc(g_576, "g_576", print_hash_value);
    transparent_crc(g_577, "g_577", print_hash_value);
    transparent_crc(g_587.f0, "g_587.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_595[i][j][k].f0, "g_595[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_605.f0, "g_605.f0", print_hash_value);
    transparent_crc(g_605.f1, "g_605.f1", print_hash_value);
    transparent_crc(g_614, "g_614", print_hash_value);
    transparent_crc_bytes (&g_616, sizeof(g_616), "g_616", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_617[i], "g_617[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_618, "g_618", print_hash_value);
    transparent_crc(g_677, "g_677", print_hash_value);
    transparent_crc(g_682.f0, "g_682.f0", print_hash_value);
    transparent_crc(g_682.f1, "g_682.f1", print_hash_value);
    transparent_crc(g_697, "g_697", print_hash_value);
    transparent_crc(g_729, "g_729", print_hash_value);
    transparent_crc(g_732.f0, "g_732.f0", print_hash_value);
    transparent_crc(g_732.f1, "g_732.f1", print_hash_value);
    transparent_crc(g_789.f0, "g_789.f0", print_hash_value);
    transparent_crc(g_807, "g_807", print_hash_value);
    transparent_crc(g_838.f0, "g_838.f0", print_hash_value);
    transparent_crc(g_838.f1, "g_838.f1", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_876[i][j].f0, "g_876[i][j].f0", print_hash_value);
            transparent_crc(g_876[i][j].f1, "g_876[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_975.f0, "g_975.f0", print_hash_value);
    transparent_crc(g_975.f1, "g_975.f1", print_hash_value);
    transparent_crc(g_1029.f0, "g_1029.f0", print_hash_value);
    transparent_crc(g_1029.f1, "g_1029.f1", print_hash_value);
    transparent_crc(g_1057, "g_1057", print_hash_value);
    transparent_crc(g_1058, "g_1058", print_hash_value);
    transparent_crc(g_1059, "g_1059", print_hash_value);
    transparent_crc(g_1060, "g_1060", print_hash_value);
    transparent_crc(g_1106.f0, "g_1106.f0", print_hash_value);
    transparent_crc(g_1107.f0, "g_1107.f0", print_hash_value);
    transparent_crc(g_1124, "g_1124", print_hash_value);
    transparent_crc(g_1126.f0, "g_1126.f0", print_hash_value);
    transparent_crc(g_1126.f1, "g_1126.f1", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1156[i][j].f0, "g_1156[i][j].f0", print_hash_value);
            transparent_crc(g_1156[i][j].f1, "g_1156[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1162, sizeof(g_1162), "g_1162", print_hash_value);
    transparent_crc(g_1187.f0, "g_1187.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1268[i].f0, "g_1268[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1317, "g_1317", print_hash_value);
    transparent_crc(g_1329, "g_1329", print_hash_value);
    transparent_crc(g_1407, "g_1407", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1416[i][j], "g_1416[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1522, "g_1522", print_hash_value);
    transparent_crc(g_1543.f0, "g_1543.f0", print_hash_value);
    transparent_crc(g_1543.f1, "g_1543.f1", print_hash_value);
    transparent_crc(g_1574, "g_1574", print_hash_value);
    transparent_crc(g_1591, "g_1591", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1680[i], "g_1680[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1681, "g_1681", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1777[i].f0, "g_1777[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1971[i][j][k].f0, "g_1971[i][j][k].f0", print_hash_value);
                transparent_crc(g_1971[i][j][k].f1, "g_1971[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1973, "g_1973", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2029[i], "g_2029[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2033, "g_2033", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_2143[i][j][k].f0, "g_2143[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2248, "g_2248", print_hash_value);
    transparent_crc(g_2350, "g_2350", print_hash_value);
    transparent_crc(g_2352.f0, "g_2352.f0", print_hash_value);
    transparent_crc(g_2352.f1, "g_2352.f1", print_hash_value);
    transparent_crc(g_2371.f0, "g_2371.f0", print_hash_value);
    transparent_crc(g_2409.f0, "g_2409.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2427[i].f0, "g_2427[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_2480[i], "g_2480[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2493.f0, "g_2493.f0", print_hash_value);
    transparent_crc(g_2529, "g_2529", print_hash_value);
    transparent_crc(g_2592.f0, "g_2592.f0", print_hash_value);
    transparent_crc(g_2592.f1, "g_2592.f1", print_hash_value);
    transparent_crc(g_2747.f0, "g_2747.f0", print_hash_value);
    transparent_crc(g_2747.f1, "g_2747.f1", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2752[i][j].f0, "g_2752[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_2842[i][j][k].f0, "g_2842[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2860, "g_2860", print_hash_value);
    transparent_crc(g_2932.f0, "g_2932.f0", print_hash_value);
    transparent_crc(g_2932.f1, "g_2932.f1", print_hash_value);
    transparent_crc(g_2955, "g_2955", print_hash_value);
    transparent_crc(g_2969.f0, "g_2969.f0", print_hash_value);
    transparent_crc(g_2969.f1, "g_2969.f1", print_hash_value);
    transparent_crc(g_2991.f0, "g_2991.f0", print_hash_value);
    transparent_crc(g_2991.f1, "g_2991.f1", print_hash_value);
    transparent_crc_bytes (&g_3050, sizeof(g_3050), "g_3050", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc_bytes(&g_3052[i][j][k], sizeof(g_3052[i][j][k]), "g_3052[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3115, "g_3115", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_3137[i].f0, "g_3137[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 879
XXX total union variables: 37

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 44
breakdown:
   depth: 1, occurrence: 346
   depth: 2, occurrence: 88
   depth: 3, occurrence: 10
   depth: 4, occurrence: 5
   depth: 5, occurrence: 2
   depth: 6, occurrence: 4
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 14, occurrence: 3
   depth: 16, occurrence: 4
   depth: 17, occurrence: 5
   depth: 18, occurrence: 6
   depth: 19, occurrence: 3
   depth: 20, occurrence: 5
   depth: 21, occurrence: 1
   depth: 22, occurrence: 5
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 7
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 33, occurrence: 2
   depth: 35, occurrence: 3
   depth: 37, occurrence: 2
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 44, occurrence: 1

XXX total number of pointers: 560

XXX times a variable address is taken: 1419
XXX times a pointer is dereferenced on RHS: 301
breakdown:
   depth: 1, occurrence: 214
   depth: 2, occurrence: 64
   depth: 3, occurrence: 23
XXX times a pointer is dereferenced on LHS: 378
breakdown:
   depth: 1, occurrence: 281
   depth: 2, occurrence: 62
   depth: 3, occurrence: 34
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 47
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 24
XXX times a pointer is qualified to be dereferenced: 10569

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1229
   level: 2, occurrence: 556
   level: 3, occurrence: 271
   level: 4, occurrence: 131
   level: 5, occurrence: 80
XXX number of pointers point to pointers: 302
XXX number of pointers point to scalars: 238
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.8
XXX average alias set size: 1.52

XXX times a non-volatile is read: 2365
XXX times a non-volatile is write: 1226
XXX times a volatile is read: 127
XXX    times read thru a pointer: 33
XXX times a volatile is write: 117
XXX    times written thru a pointer: 82
XXX times a volatile is available for access: 7.86e+03
XXX percentage of non-volatile access: 93.6

XXX forward jumps: 1
XXX backward jumps: 10

XXX stmts: 343
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 38
   depth: 2, occurrence: 37
   depth: 3, occurrence: 51
   depth: 4, occurrence: 84
   depth: 5, occurrence: 103

XXX percentage a fresh-made variable is used: 18.5
XXX percentage an existing variable is used: 81.5
********************* end of statistics **********************/


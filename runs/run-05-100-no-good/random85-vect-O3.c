/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3903504408
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static float g_3[9] = {0x5.Ap-1,0x5.Ap-1,0x5.Ap-1,0x5.Ap-1,0x5.Ap-1,0x5.Ap-1,0x5.Ap-1,0x5.Ap-1,0x5.Ap-1};
static uint8_t g_15 = 0xB4L;
static uint64_t g_19 = 18446744073709551615UL;
static int16_t g_20 = (-1L);
static uint64_t g_22 = 1UL;
static int64_t g_32 = 0L;
static uint32_t g_51[8] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static int64_t g_63 = 0x74126315BFEB1EC6LL;
static int64_t * const g_62 = &g_63;
static int32_t g_68[6][10] = {{0x086237DDL,6L,(-9L),0x56014EFCL,0x56014EFCL,(-9L),6L,0x086237DDL,6L,(-9L)},{1L,0L,0x56014EFCL,0L,1L,(-9L),(-9L),1L,0L,0x56014EFCL},{0x086237DDL,0x086237DDL,0x56014EFCL,1L,6L,1L,0x56014EFCL,0x086237DDL,0x086237DDL,0x086237DDL},{6L,(-9L),0x56014EFCL,0x56014EFCL,(-9L),6L,0x086237DDL,6L,(-9L),0x56014EFCL},{1L,6L,1L,0x56014EFCL,0x086237DDL,0x086237DDL,0x56014EFCL,1L,6L,1L},{1L,6L,6L,(-9L),6L,6L,1L,1L,6L,6L}};
static int32_t g_100 = 1L;
static volatile int32_t g_105 = 5L;/* VOLATILE GLOBAL g_105 */
static uint8_t g_107 = 9UL;
static uint32_t g_117 = 0UL;
static int64_t g_122 = 0xF631B5FDDB278982LL;
static int32_t g_123[4][3] = {{0L,0L,0L},{(-3L),(-3L),(-3L)},{0L,0L,0L},{(-3L),(-3L),(-3L)}};
static int8_t g_124 = 0x30L;
static uint32_t g_125[2][5] = {{1UL,1UL,1UL,1UL,1UL},{4UL,4UL,4UL,4UL,4UL}};
static int16_t g_127[3] = {0xF2FDL,0xF2FDL,0xF2FDL};
static uint32_t g_156 = 0x12365208L;
static int32_t g_181 = 0x33F1638CL;
static int64_t g_219 = 5L;
static uint16_t g_226 = 0xCFDDL;
static int64_t *g_246 = &g_63;
static int16_t g_254 = 0L;
static int32_t *g_257 = &g_181;
static int32_t ** volatile g_256 = &g_257;/* VOLATILE GLOBAL g_256 */
static uint64_t g_282 = 3UL;
static const int32_t *g_288 = &g_181;
static float **g_296 = (void*)0;
static float *g_298 = &g_3[5];
static float **g_297[10][8] = {{&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,(void*)0},{&g_298,&g_298,&g_298,(void*)0,&g_298,&g_298,(void*)0,&g_298},{&g_298,&g_298,&g_298,(void*)0,&g_298,&g_298,(void*)0,(void*)0},{&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,(void*)0},{&g_298,&g_298,&g_298,(void*)0,&g_298,&g_298,(void*)0,&g_298},{&g_298,&g_298,&g_298,(void*)0,&g_298,&g_298,(void*)0,(void*)0},{&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,(void*)0},{&g_298,&g_298,&g_298,(void*)0,&g_298,&g_298,(void*)0,&g_298},{&g_298,&g_298,&g_298,(void*)0,&g_298,&g_298,(void*)0,(void*)0},{&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,&g_298,(void*)0}};
static const int8_t *g_305[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const int8_t **g_304[1] = {&g_305[0]};
static const int8_t *** volatile g_303[7][4][5] = {{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]}},{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]}},{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]}},{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]}},{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]}},{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]}},{{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]},{&g_304[0],&g_304[0],&g_304[0],&g_304[0],&g_304[0]},{(void*)0,&g_304[0],&g_304[0],(void*)0,&g_304[0]}}};
static uint16_t g_351[8][4][8] = {{{0xBCE6L,9UL,0UL,6UL,65532UL,0x22E5L,65532UL,6UL},{65528UL,0UL,65528UL,1UL,0x2623L,0x6DD3L,0UL,0xBCE6L},{2UL,0x3045L,6UL,65535UL,65532UL,65535UL,0x2623L,2UL},{2UL,65526UL,0UL,0xA5EFL,0x2623L,65528UL,65535UL,0x3946L}},{{65528UL,6UL,0x0BCBL,65526UL,65532UL,65532UL,65526UL,0x0BCBL},{0xBCE6L,0xBCE6L,65534UL,0x22E5L,0xC804L,0UL,1UL,0x6DD3L},{0x22E5L,0xA5EFL,0xEA95L,7UL,6UL,0x2623L,9UL,0x6DD3L},{0xA5EFL,1UL,0x3946L,0x22E5L,0xBB7BL,65535UL,65530UL,0x0BCBL}},{{0xEA95L,0x3946L,0xC804L,65526UL,0x5BDFL,65526UL,0xC804L,0x3946L},{7UL,0x22E5L,65526UL,0xA5EFL,65535UL,1UL,6UL,2UL},{6UL,0xBB7BL,0x5BDFL,65535UL,7UL,9UL,6UL,0xBCE6L},{0x2623L,65535UL,65526UL,1UL,9UL,65530UL,0xC804L,0x5BDFL}},{{0UL,0xA5EFL,6UL,0x5BDFL,0x5BDFL,6UL,0xA5EFL,0UL},{65526UL,0x6DD3L,0UL,65535UL,3UL,0x5BDFL,0UL,65532UL},{0xBCE6L,6UL,9UL,7UL,65535UL,0x5BDFL,0xBB7BL,6UL},{0xEA95L,0x6DD3L,0xBCE6L,9UL,0UL,6UL,65532UL,0x22E5L}},{{6UL,0xA5EFL,0x6DD3L,6UL,0x6DD3L,0xA5EFL,6UL,0UL},{65530UL,6UL,0xEA95L,0xBCE6L,65526UL,0UL,65534UL,0x3946L},{0UL,0UL,0x5BDFL,2UL,65526UL,0xBB7BL,65535UL,6UL},{65530UL,0x2623L,2UL,0x3946L,0x6DD3L,65532UL,7UL,7UL}},{{6UL,0UL,0x0BCBL,0x0BCBL,0UL,6UL,2UL,65534UL},{0xEA95L,0xBB7BL,65532UL,0x6DD3L,65535UL,65534UL,65530UL,0x3045L},{0xBCE6L,0xC804L,0UL,0x6DD3L,3UL,65535UL,65526UL,65534UL},{65526UL,3UL,0UL,0x0BCBL,0x5BDFL,7UL,0x2623L,7UL}},{{0UL,0x3946L,65530UL,0x3946L,0UL,2UL,0x3045L,6UL},{65534UL,65532UL,0xA5EFL,2UL,65532UL,65530UL,0xBCE6L,0x3946L},{0x3946L,1UL,0xA5EFL,0xBCE6L,6UL,65526UL,0x3045L,0UL},{65532UL,0xEA95L,65530UL,6UL,0x22E5L,0x2623L,0x2623L,0x22E5L}},{{9UL,0UL,0UL,9UL,0UL,0x3045L,65526UL,6UL},{0xC804L,65535UL,0UL,7UL,0x3946L,0xBCE6L,65530UL,65532UL},{0x2623L,65535UL,65532UL,65535UL,6UL,0x3045L,2UL,0UL},{3UL,0UL,0x0BCBL,0x5BDFL,7UL,0x2623L,7UL,0x5BDFL}}};
static float g_354 = 0x5.D3A11Ep+38;
static volatile int32_t g_355 = 0x02E03701L;/* VOLATILE GLOBAL g_355 */
static volatile int32_t g_356 = 0L;/* VOLATILE GLOBAL g_356 */
static volatile int64_t g_357 = 0x7090773055DAE048LL;/* VOLATILE GLOBAL g_357 */
static uint16_t g_358 = 0xA821L;
static int32_t ** volatile g_361 = (void*)0;/* VOLATILE GLOBAL g_361 */
static int32_t ** volatile g_368 = (void*)0;/* VOLATILE GLOBAL g_368 */
static int32_t ** const  volatile g_369 = &g_257;/* VOLATILE GLOBAL g_369 */
static int16_t * volatile *g_371 = (void*)0;
static int16_t * volatile * volatile *g_370 = &g_371;
static int16_t * volatile * volatile ** volatile g_372 = &g_370;/* VOLATILE GLOBAL g_372 */
static float ***g_374[2] = {(void*)0,(void*)0};
static float **** volatile g_373 = &g_374[0];/* VOLATILE GLOBAL g_373 */
static int8_t g_399[5] = {0x0EL,0x0EL,0x0EL,0x0EL,0x0EL};
static float *** volatile g_401 = &g_296;/* VOLATILE GLOBAL g_401 */
static uint64_t g_431 = 0x479BA9D7E2D96D40LL;
static int32_t ** volatile g_434 = (void*)0;/* VOLATILE GLOBAL g_434 */
static int32_t ** volatile g_435 = &g_257;/* VOLATILE GLOBAL g_435 */
static int32_t ** volatile g_436 = &g_257;/* VOLATILE GLOBAL g_436 */
static volatile uint32_t g_447 = 4294967292UL;/* VOLATILE GLOBAL g_447 */
static int8_t *g_466 = &g_124;
static int8_t **g_465 = &g_466;
static int8_t ***g_464 = &g_465;
static int8_t ****g_463[5][10][4] = {{{&g_464,&g_464,(void*)0,&g_464},{(void*)0,&g_464,&g_464,(void*)0},{&g_464,&g_464,&g_464,&g_464},{(void*)0,&g_464,&g_464,&g_464},{&g_464,(void*)0,&g_464,&g_464},{&g_464,&g_464,&g_464,&g_464},{&g_464,&g_464,&g_464,&g_464},{(void*)0,&g_464,(void*)0,&g_464},{(void*)0,(void*)0,&g_464,&g_464},{&g_464,&g_464,&g_464,(void*)0}},{{&g_464,(void*)0,(void*)0,&g_464},{(void*)0,(void*)0,&g_464,(void*)0},{(void*)0,&g_464,&g_464,&g_464},{(void*)0,&g_464,(void*)0,&g_464},{&g_464,(void*)0,&g_464,&g_464},{&g_464,&g_464,&g_464,&g_464},{(void*)0,&g_464,(void*)0,&g_464},{(void*)0,(void*)0,&g_464,&g_464},{&g_464,(void*)0,&g_464,&g_464},{&g_464,(void*)0,&g_464,&g_464}},{{(void*)0,&g_464,&g_464,(void*)0},{&g_464,&g_464,(void*)0,&g_464},{(void*)0,(void*)0,&g_464,(void*)0},{&g_464,&g_464,&g_464,&g_464},{&g_464,(void*)0,(void*)0,&g_464},{&g_464,&g_464,&g_464,&g_464},{&g_464,&g_464,&g_464,(void*)0},{&g_464,&g_464,&g_464,&g_464},{(void*)0,&g_464,&g_464,(void*)0},{&g_464,(void*)0,(void*)0,&g_464}},{{(void*)0,&g_464,&g_464,(void*)0},{&g_464,&g_464,&g_464,(void*)0},{&g_464,&g_464,(void*)0,&g_464},{&g_464,(void*)0,&g_464,(void*)0},{&g_464,&g_464,&g_464,&g_464},{&g_464,&g_464,(void*)0,(void*)0},{&g_464,&g_464,(void*)0,&g_464},{&g_464,&g_464,&g_464,&g_464},{&g_464,(void*)0,&g_464,&g_464},{&g_464,&g_464,&g_464,(void*)0}},{{(void*)0,(void*)0,&g_464,&g_464},{&g_464,&g_464,&g_464,(void*)0},{&g_464,&g_464,&g_464,&g_464},{&g_464,(void*)0,&g_464,&g_464},{(void*)0,(void*)0,&g_464,&g_464},{&g_464,(void*)0,&g_464,&g_464},{(void*)0,&g_464,(void*)0,&g_464},{&g_464,&g_464,&g_464,&g_464},{(void*)0,(void*)0,&g_464,&g_464},{&g_464,&g_464,(void*)0,&g_464}}};
static int16_t g_485 = 0x08B9L;
static volatile float ** const g_497 = (void*)0;
static volatile uint8_t g_520 = 0xFFL;/* VOLATILE GLOBAL g_520 */
static volatile uint8_t *g_519 = &g_520;
static volatile uint8_t **g_518 = &g_519;
static int32_t ** volatile g_530 = &g_257;/* VOLATILE GLOBAL g_530 */
static volatile uint32_t g_543 = 4294967295UL;/* VOLATILE GLOBAL g_543 */
static volatile uint32_t g_548 = 0x28328493L;/* VOLATILE GLOBAL g_548 */
static volatile int32_t * volatile g_610 = (void*)0;/* VOLATILE GLOBAL g_610 */
static volatile int32_t * volatile *g_609 = &g_610;
static float g_633 = 0x9.3DEBF3p+61;
static int32_t ** volatile g_690 = &g_257;/* VOLATILE GLOBAL g_690 */
static uint64_t *g_738 = (void*)0;
static uint64_t **g_737 = &g_738;
static uint16_t g_761 = 0x3F50L;
static int16_t g_762[6] = {0x474FL,0x6D4DL,0x474FL,0x474FL,0x6D4DL,0x474FL};
static const int16_t **g_789 = (void*)0;
static const int16_t ***g_788 = &g_789;
static int32_t * volatile g_790 = &g_100;/* VOLATILE GLOBAL g_790 */
static volatile float g_847 = 0x2.DAF65Fp-37;/* VOLATILE GLOBAL g_847 */
static volatile uint32_t g_848 = 0xF664E5F6L;/* VOLATILE GLOBAL g_848 */
static int32_t * volatile g_878 = &g_100;/* VOLATILE GLOBAL g_878 */
static int32_t * volatile g_958[4][5] = {{(void*)0,&g_100,(void*)0,&g_100,(void*)0},{&g_181,&g_181,&g_181,&g_181,&g_181},{(void*)0,&g_100,(void*)0,&g_100,(void*)0},{&g_181,&g_181,&g_181,&g_181,&g_181}};
static int32_t * volatile g_959 = &g_100;/* VOLATILE GLOBAL g_959 */
static uint16_t g_1055 = 0x7D94L;
static int32_t ** volatile g_1075 = &g_257;/* VOLATILE GLOBAL g_1075 */
static uint32_t *g_1119 = &g_51[0];
static uint32_t **g_1118 = &g_1119;
static uint32_t *** const  volatile g_1117[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint32_t *** volatile g_1120 = &g_1118;/* VOLATILE GLOBAL g_1120 */
static int32_t ** volatile g_1121 = (void*)0;/* VOLATILE GLOBAL g_1121 */
static int32_t ** volatile g_1276 = &g_257;/* VOLATILE GLOBAL g_1276 */
static int32_t *g_1317 = &g_123[2][0];
static uint32_t *g_1365 = &g_117;
static volatile uint64_t g_1464 = 0x049B55D0739A2D08LL;/* VOLATILE GLOBAL g_1464 */
static volatile uint64_t * volatile g_1463 = &g_1464;/* VOLATILE GLOBAL g_1463 */
static volatile uint64_t * volatile *g_1462 = &g_1463;
static volatile uint64_t * volatile **g_1461 = &g_1462;
static int32_t * volatile g_1486[1][9] = {{&g_100,&g_100,&g_100,&g_100,&g_100,&g_100,&g_100,&g_100,&g_100}};
static int32_t ** const  volatile g_1517 = &g_257;/* VOLATILE GLOBAL g_1517 */
static int32_t ** volatile g_1639 = &g_257;/* VOLATILE GLOBAL g_1639 */
static int32_t ** volatile g_1640 = &g_257;/* VOLATILE GLOBAL g_1640 */
static int32_t ** const  volatile g_1694 = &g_257;/* VOLATILE GLOBAL g_1694 */
static int32_t ** volatile g_1703 = &g_257;/* VOLATILE GLOBAL g_1703 */
static int32_t ** volatile g_1704[9][4][1] = {{{&g_257},{&g_257},{(void*)0},{&g_257}},{{&g_257},{(void*)0},{&g_257},{&g_257}},{{&g_257},{&g_257},{&g_257},{&g_257}},{{(void*)0},{&g_257},{&g_257},{&g_257}},{{&g_257},{&g_257},{&g_257},{(void*)0}},{{&g_257},{&g_257},{(void*)0},{&g_257}},{{&g_257},{&g_257},{&g_257},{&g_257}},{{&g_257},{(void*)0},{&g_257},{&g_257}},{{&g_257},{&g_257},{&g_257},{&g_257}}};
static int32_t ** volatile g_1705[5] = {&g_257,&g_257,&g_257,&g_257,&g_257};
static int32_t ** const  volatile g_1724 = &g_257;/* VOLATILE GLOBAL g_1724 */
static uint64_t g_1741 = 0x67954E4B4E2FAE69LL;
static int32_t g_1744 = 0xCDCC49EFL;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static uint8_t  func_10(float  p_11, uint8_t  p_12, float * p_13);
static int32_t * func_24(int64_t  p_25, float * p_26);
static int64_t  func_27(int64_t  p_28);
static int32_t  func_35(uint32_t  p_36, uint32_t  p_37, int64_t  p_38, int32_t * p_39);
static uint16_t  func_42(uint8_t  p_43, uint8_t  p_44);
static uint64_t  func_58(int64_t * const  p_59, int16_t  p_60, const int64_t * p_61);
static int64_t * func_69(uint8_t  p_70, uint32_t  p_71, int32_t  p_72, uint64_t  p_73);
static int64_t  func_90(uint64_t  p_91, int16_t  p_92, uint64_t  p_93, int32_t * const  p_94);
static int64_t  func_96(const int32_t * p_97);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_19 g_20 g_22 g_15 g_51 g_62 g_298 g_156 g_373 g_374 g_466 g_124 g_257 g_181 g_465 g_127 g_399 g_68 g_107 g_100 g_761 g_246 g_63 g_219 g_122 g_372 g_370 g_371 g_1055 g_518 g_519 g_520 g_351 g_690 g_1075 g_790 g_737 g_282 g_256 g_358 g_1120 g_435 g_436 g_254 g_3 g_530 g_762 g_485 g_288 g_1119 g_878 g_1276 g_117 g_32 g_609 g_610 g_1365 g_788 g_789 g_369 g_125 g_1517 g_1317 g_123 g_464 g_1639 g_1640 g_1486 g_1724 g_959 g_1741 g_1744
 * writes: g_3 g_15 g_19 g_22 g_32 g_20 g_51 g_68 g_633 g_399 g_282 g_124 g_107 g_100 g_358 g_761 g_181 g_219 g_1055 g_63 g_257 g_738 g_1118 g_254 g_127 g_122 g_762 g_485 g_464 g_246 g_1365 g_117 g_463 g_1461 g_1486 g_1741 g_1744
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    float *l_2 = &g_3[5];
    int32_t l_5 = 0L;
    uint8_t *l_14 = &g_15;
    uint64_t *l_18 = &g_19;
    uint64_t *l_21 = &g_22;
    float *l_23 = (void*)0;
    uint16_t l_1742[1][6] = {{0x5475L,0x5475L,0x5475L,0x5475L,0x5475L,0x5475L}};
    int32_t *l_1743 = &g_1744;
    int32_t *l_1745 = &g_1744;
    int32_t *l_1746 = &g_181;
    int32_t *l_1747 = (void*)0;
    int32_t l_1748 = 0x30AFD749L;
    int32_t *l_1749 = &g_100;
    int32_t *l_1750[9][4][6] = {{{&g_100,&l_5,&g_1744,(void*)0,&g_181,(void*)0},{&g_1744,&g_1744,&g_181,&l_5,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1744,(void*)0,&l_5,&l_1748},{(void*)0,&l_5,&g_100,&g_1744,(void*)0,&g_181}},{{&l_5,&g_181,&l_5,&g_100,&g_1744,&g_100},{(void*)0,&l_1748,&g_100,&l_5,(void*)0,&g_100},{&g_181,&l_1748,&l_1748,(void*)0,&g_1744,&g_181},{&g_1744,(void*)0,&l_1748,(void*)0,(void*)0,&g_1744}},{{&g_100,&g_181,&l_5,&g_1744,&g_100,&g_181},{&l_5,&g_100,&g_100,(void*)0,&g_1744,&g_100},{&g_100,&g_1744,(void*)0,&g_100,&l_1748,&g_181},{(void*)0,(void*)0,(void*)0,&l_5,&g_181,&g_1744}},{{&g_100,(void*)0,(void*)0,(void*)0,(void*)0,&g_100},{&l_5,(void*)0,&g_1744,(void*)0,(void*)0,&l_5},{&l_5,&g_181,&g_181,&g_181,&g_100,&g_1744},{&l_5,(void*)0,&g_181,(void*)0,&g_1744,(void*)0}},{{&l_5,(void*)0,(void*)0,(void*)0,&l_1748,(void*)0},{&g_100,&g_100,&g_181,&l_5,&l_5,&g_100},{(void*)0,(void*)0,&g_100,&g_100,(void*)0,&l_5},{&g_100,(void*)0,(void*)0,(void*)0,(void*)0,&l_1748}},{{&l_1748,(void*)0,&g_181,&g_100,&l_1748,&g_100},{&g_100,&l_5,(void*)0,&g_181,&g_100,&l_1748},{&g_1744,&g_181,&l_5,&l_1748,&l_5,(void*)0},{&l_5,&g_1744,(void*)0,&l_1748,&g_181,&g_100}},{{(void*)0,&g_181,&g_100,&g_100,&g_100,&l_1748},{&l_1748,(void*)0,&l_1748,&g_100,&l_1748,(void*)0},{&g_1744,&l_5,&g_1744,&g_1744,(void*)0,(void*)0},{&g_181,(void*)0,&l_5,&g_100,(void*)0,&l_1748}},{{&g_181,(void*)0,(void*)0,&g_181,(void*)0,&g_181},{&g_1744,&l_5,(void*)0,(void*)0,&l_1748,(void*)0},{&g_100,(void*)0,&g_100,(void*)0,&g_100,&l_5},{&g_181,&g_181,&g_1744,&g_1744,&g_181,&g_1744}},{{&g_181,&g_1744,&g_100,&l_5,&l_5,&l_5},{(void*)0,&g_181,&g_181,(void*)0,&g_100,&g_1744},{&g_100,&l_5,&g_1744,&l_5,&l_1748,&l_1748},{&g_181,(void*)0,&g_181,(void*)0,(void*)0,&l_1748}}};
    int64_t l_1751 = 0x288839EC4EEA0D6ALL;
    uint64_t l_1752 = 0UL;
    uint16_t l_1755[1];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1755[i] = 0xE19DL;
    (*l_2) = 0x8.62FD41p-35;
    (*l_1743) ^= (!((l_5 , (safe_sub_func_uint64_t_u_u((l_5 , (safe_add_func_uint8_t_u_u(func_10(l_5, ((*l_14) = (l_2 == l_2)), (((safe_lshift_func_uint16_t_u_s(l_5, 4)) && (((*l_18) &= 0x2DD78F4F823AAF5BLL) & (0xC9L & (((*l_21) = g_20) ^ l_5)))) , l_23)), l_1742[0][3]))), (-1L)))) , l_1742[0][3]));
    --l_1752;
    return l_1755[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_22 g_19 g_15 g_20 g_51 g_62 g_298 g_156 g_373 g_374 g_466 g_124 g_257 g_181 g_465 g_127 g_399 g_68 g_107 g_100 g_761 g_246 g_63 g_219 g_122 g_372 g_370 g_371 g_1055 g_518 g_519 g_520 g_351 g_690 g_1075 g_790 g_737 g_282 g_256 g_358 g_1120 g_435 g_436 g_254 g_3 g_530 g_762 g_485 g_288 g_1119 g_878 g_1276 g_117 g_32 g_609 g_610 g_1365 g_788 g_789 g_369 g_125 g_1517 g_1317 g_123 g_464 g_1639 g_1640 g_1486 g_1724 g_959 g_1741
 * writes: g_32 g_15 g_20 g_51 g_68 g_3 g_633 g_399 g_282 g_124 g_107 g_100 g_358 g_761 g_181 g_219 g_1055 g_63 g_257 g_738 g_1118 g_254 g_127 g_122 g_762 g_485 g_464 g_246 g_1365 g_117 g_22 g_463 g_1461 g_1486 g_19 g_1741
 */
static uint8_t  func_10(float  p_11, uint8_t  p_12, float * p_13)
{ /* block id: 5 */
    int64_t *l_31 = &g_32;
    int32_t l_40 = (-1L);
    uint8_t *l_45 = &g_15;
    int16_t l_1195 = 7L;
    float l_1196 = 0x1.8p+1;
    float *l_1488[8][6][4] = {{{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196}},{{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196}},{{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196}},{{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{(void*)0,&g_354,(void*)0,&l_1196},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1196},{&g_354,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0},{(void*)0,&g_354,(void*)0,(void*)0},{&g_354,&g_354,&l_1196,(void*)0},{&g_354,(void*)0,(void*)0,(void*)0}}};
    int32_t *l_1734[5][3] = {{(void*)0,(void*)0,(void*)0},{&g_181,&l_40,&g_181},{(void*)0,(void*)0,(void*)0},{&g_181,&l_40,&g_181},{(void*)0,(void*)0,(void*)0}};
    uint64_t *l_1740 = &g_19;
    int i, j, k;
    l_1734[2][2] = func_24(func_27((((((safe_sub_func_uint16_t_u_u((((-1L) <= g_22) & ((*l_31) = 0x205300CE3D97867ALL)), ((safe_add_func_int32_t_s_s(((func_35(l_40, g_19, (!func_42(((*l_45)++), g_20)), g_298) > 0xE0788D97L) , p_12), (-10L))) > l_1195))) & p_12) > p_12) && l_1195) , 0xBDECD318CADA8FE4LL)), l_1488[4][4][2]);
    g_1741 &= (!(0x9FD262A0E2F99120LL >= ((*l_1740) = (g_22 = (safe_sub_func_uint16_t_u_u(0x3778L, (safe_add_func_int64_t_s_s(p_12, 0xFABB8D0E885E4391LL))))))));
    return p_12;
}


/* ------------------------------------------ */
/* 
 * reads : g_369 g_257 g_246 g_63 g_107 g_125 g_62 g_1517 g_100 g_254 g_1317 g_123 g_466 g_124 g_298 g_32 g_762 g_15 g_351 g_282 g_181 g_3 g_465 g_436 g_435 g_464 g_1639 g_1276 g_1640 g_518 g_519 g_520 g_1486 g_1724 g_959
 * writes: g_63 g_257 g_100 g_254 g_3 g_32 g_762 g_282 g_124 g_15 g_181 g_1486
 */
static int32_t * func_24(int64_t  p_25, float * p_26)
{ /* block id: 723 */
    uint16_t l_1489 = 65534UL;
    int32_t *l_1516 = &g_100;
    uint8_t *l_1549 = &g_15;
    int64_t **l_1576 = &g_246;
    int8_t **l_1616[6][6] = {{&g_466,&g_466,&g_466,&g_466,&g_466,&g_466},{&g_466,&g_466,&g_466,&g_466,&g_466,&g_466},{&g_466,&g_466,&g_466,&g_466,&g_466,&g_466},{&g_466,&g_466,&g_466,&g_466,&g_466,&g_466},{&g_466,&g_466,&g_466,&g_466,&g_466,&g_466},{&g_466,&g_466,&g_466,&g_466,&g_466,&g_466}};
    int32_t l_1654 = 0x0FB43D1FL;
    int32_t l_1655 = 1L;
    int32_t l_1656[8][9] = {{0x584B7DD8L,1L,0L,(-1L),1L,0L,1L,0xA1544415L,(-8L)},{0L,(-1L),(-9L),1L,1L,0x02B2D95EL,(-1L),0x4B8C831EL,0x4B8C831EL},{(-1L),0x02B2D95EL,0x1AF515DAL,(-1L),0x1AF515DAL,0x02B2D95EL,(-1L),0L,0x584B7DD8L},{0x65B30D83L,1L,(-1L),0x1AF515DAL,(-1L),0L,0xF1E26D2EL,0L,(-1L)},{1L,(-1L),0x02B2D95EL,(-8L),1L,(-9L),(-1L),0x65B30D83L,1L},{1L,1L,0x1AF515DAL,8L,(-1L),0L,(-1L),8L,0x1AF515DAL},{1L,1L,0x71AD55FEL,0L,0x4B8C831EL,0xF1E26D2EL,0L,(-9L),(-1L)},{0x2A8192CDL,0x65B30D83L,0x584B7DD8L,1L,0L,1L,8L,1L,(-1L)}};
    float ****l_1712 = &g_374[0];
    uint64_t ** const l_1720 = (void*)0;
    int i, j;
    if (l_1489)
    { /* block id: 724 */
        int16_t l_1513[2];
        const uint32_t l_1514 = 0x1C02F195L;
        float l_1515 = 0x3.7p+1;
        int32_t l_1541 = 0x15A9DDA9L;
        int32_t l_1648 = (-1L);
        uint64_t l_1651 = 8UL;
        int32_t *l_1667 = &l_1541;
        int i;
        for (i = 0; i < 2; i++)
            l_1513[i] = 0x9D0CL;
        if ((0xE258L & (safe_rshift_func_int16_t_s_s(((safe_sub_func_int32_t_s_s((safe_mod_func_int64_t_s_s(0L, ((*g_62) = (((p_25 , (safe_sub_func_int16_t_s_s(l_1489, (~(((safe_lshift_func_int16_t_s_u(0xA9F5L, (p_25 , (safe_add_func_int16_t_s_s((((*g_369) != p_26) < (safe_mul_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((safe_mul_func_int16_t_s_s(((((safe_mod_func_uint64_t_u_u((safe_div_func_int64_t_s_s(l_1513[1], l_1514)), (*g_246))) , l_1489) == l_1489) , l_1513[0]), p_25)) == g_107), 0xFB70429450347080LL)), 0xDFL))), g_125[1][4]))))) | 0xCB87756AL) & 1L))))) <= p_25) , l_1489)))), l_1513[1])) != 0x7EAAB36CL), l_1513[1]))))
        { /* block id: 726 */
            (*g_1517) = l_1516;
        }
        else
        { /* block id: 728 */
            int8_t l_1518 = 0L;
            uint32_t *l_1524[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t l_1529 = 0xF76E289AL;
            uint8_t *l_1550 = &g_15;
            int16_t l_1592 = 0L;
            int32_t l_1638 = 0x6CC3D98FL;
            int32_t l_1699 = 0x988B8733L;
            int i;
            if (((*l_1516) ^= l_1518))
            { /* block id: 730 */
                uint32_t *l_1523 = &g_117;
                const int32_t l_1539 = 0x5B0F9400L;
                int16_t *l_1561 = &g_762[4];
                float **l_1591 = &g_298;
                int32_t l_1641 = 0L;
                int32_t l_1649[9] = {4L,0x3980EF8BL,4L,0x3980EF8BL,4L,0x3980EF8BL,4L,0x3980EF8BL,4L};
                uint32_t l_1657 = 0x774DED72L;
                int32_t **l_1666[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_1666[i] = &l_1516;
                for (g_254 = (-13); (g_254 > 13); g_254++)
                { /* block id: 733 */
                    int64_t l_1536 = (-3L);
                    int64_t *l_1540 = &g_32;
                    l_1541 ^= (((*g_62) = ((*l_1540) |= (((((safe_mul_func_float_f_f((l_1523 == l_1524[2]), (safe_add_func_float_f_f((safe_mul_func_float_f_f((l_1529 == ((*g_298) = ((((safe_div_func_float_f_f(((*g_1317) , (safe_add_func_float_f_f((*l_1516), ((safe_mul_func_uint8_t_u_u((l_1536 >= (p_25 , (safe_div_func_int64_t_s_s(((*g_466) != 7UL), (*g_62))))), 254UL)) , (-0x3.2p-1))))), (*l_1516))) <= l_1536) != p_25) == 0xA.2E3306p+58))), p_25)), l_1539)))) , p_25) & p_25) , (*g_246)) | (*g_62)))) | (-8L));
                }
                if ((*l_1516))
                { /* block id: 739 */
                    int32_t *l_1544 = (void*)0;
                    for (l_1518 = 0; (l_1518 != 6); ++l_1518)
                    { /* block id: 742 */
                        return l_1544;
                    }
                }
                else
                { /* block id: 745 */
                    uint8_t *l_1547 = &g_107;
                    uint8_t **l_1548 = &l_1547;
                    int16_t *l_1563 = (void*)0;
                    int16_t **l_1562 = &l_1563;
                    uint32_t ***l_1564[5] = {&g_1118,&g_1118,&g_1118,&g_1118,&g_1118};
                    const int32_t l_1573 = 6L;
                    uint64_t *l_1577[4][1] = {{&g_431},{&g_431},{&g_431},{&g_431}};
                    uint16_t *l_1590 = &l_1489;
                    int32_t l_1600 = 0L;
                    float l_1637 = 0x0.Cp-1;
                    int32_t l_1650 = (-3L);
                    int i, j;
                    (*l_1516) |= ((((safe_rshift_func_int16_t_s_u(((((((*l_1548) = l_1547) != (l_1550 = l_1549)) || (safe_rshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s(((((safe_rshift_func_int16_t_s_s(p_25, (safe_mul_func_int16_t_s_s(((*l_1561) |= (safe_sub_func_uint32_t_u_u((l_1561 != ((*l_1562) = &l_1513[1])), p_25))), (l_1564[0] != ((safe_mul_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(l_1513[1], (safe_rshift_func_int8_t_s_s((((l_1513[1] , l_1524[2]) != p_26) | (*g_62)), 3)))), l_1518)) , &g_1118)))))) & p_25) || l_1541) >= p_25), 1L)), g_15))) ^ 0UL) <= g_351[2][2][5]), 14)) & 1L) >= p_25) , 0x6F7CB394L);
                    if ((safe_rshift_func_uint8_t_u_u(l_1573, (((safe_sub_func_uint64_t_u_u(((*l_1516) = (((((*l_1549) |= (((l_1576 == l_1576) == (g_282--)) | ((safe_add_func_uint8_t_u_u((safe_div_func_int8_t_s_s(((safe_div_func_int8_t_s_s(((**g_465) ^= (((&p_26 == ((((safe_rshift_func_int16_t_s_u((((65532UL > ((*l_1590) = ((safe_lshift_func_int16_t_s_u(p_25, 1)) & ((**l_1576) = p_25)))) != ((0xA4DAAD0B48DED2B2LL != 0xCF1DAF4E99B0DC42LL) & 0x3D85F2B2L)) && 0x2FECL), 6)) , 0UL) <= 0x5790L) , l_1591)) < (**g_1517)) >= l_1539)), p_25)) <= p_25), 0x8FL)), (-1L))) != l_1529))) != (-1L)) <= p_25) != p_25)), l_1592)) || 0xAA9BC79A987B7DC6LL) & g_762[4]))))
                    { /* block id: 757 */
                        (*g_1517) = (*g_436);
                        return (*g_435);
                    }
                    else
                    { /* block id: 760 */
                        const int16_t l_1605 = 0xFB7DL;
                        int8_t **l_1615[6] = {&g_466,&g_466,&g_466,&g_466,&g_466,&g_466};
                        float *l_1631[10] = {(void*)0,&l_1515,(void*)0,&l_1515,(void*)0,&l_1515,(void*)0,&l_1515,(void*)0,&l_1515};
                        int i;
                        (*l_1516) = (((**g_436) = (**g_436)) & (((safe_unary_minus_func_int32_t_s(((safe_lshift_func_uint8_t_u_s(0xE4L, 2)) ^ ((l_1573 , l_1590) != &g_1055)))) , ((void*)0 == &g_246)) , (safe_mod_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((l_1600 = l_1573), ((safe_div_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(((((((***g_464) = p_25) == l_1592) , 1L) >= l_1605) & 252UL), p_25)), l_1513[1])) & 0UL))), l_1529))));
                        (*l_1516) ^= (safe_lshift_func_uint16_t_u_s((safe_unary_minus_func_uint8_t_u(l_1605)), 2));
                        (**l_1591) = ((safe_sub_func_float_f_f((*g_298), (l_1600 = (((safe_rshift_func_uint16_t_u_u(((((safe_mod_func_int32_t_s_s(p_25, l_1513[1])) , l_1615[2]) == l_1616[3][1]) >= (((safe_add_func_int64_t_s_s(0L, ((safe_rshift_func_uint16_t_u_s(((~p_25) == (!(safe_lshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(0x8AL, (((safe_div_func_int16_t_s_s(((void*)0 != &g_351[7][1][0]), p_25)) && p_25) <= 0x4869L))), (*g_466))), p_25)))), 15)) != l_1541))) | 0L) < l_1592)), 5)) > l_1605) , l_1518)))) > p_25);
                    }
                    if (((safe_add_func_int32_t_s_s(((void*)0 == &l_1513[0]), 0xD8F0E755L)) && (l_1638 = (safe_unary_minus_func_int8_t_s((safe_add_func_int32_t_s_s(p_25, (l_1600 ^ p_25))))))))
                    { /* block id: 770 */
                        (*g_1639) = p_26;
                        return (*g_1276);
                    }
                    else
                    { /* block id: 773 */
                        int32_t *l_1642 = &g_100;
                        int32_t *l_1643 = &l_1541;
                        int32_t *l_1644 = (void*)0;
                        int32_t *l_1645 = &l_1641;
                        int32_t *l_1646 = (void*)0;
                        int32_t *l_1647[8] = {&l_1600,&l_1600,&l_1600,&l_1600,&l_1600,&l_1600,&l_1600,&l_1600};
                        int i;
                        (*g_1640) = l_1516;
                        l_1651--;
                        l_1657--;
                        (*l_1516) = (0xD37CL & (((&l_1524[2] != (void*)0) | (safe_mod_func_int16_t_s_s((-5L), (p_25 , (18446744073709551615UL || ((0L != 3L) <= (~(safe_rshift_func_int8_t_s_u((~(p_25 >= l_1600)), (**g_518)))))))))) ^ (*g_257)));
                    }
                }
                l_1667 = (void*)0;
            }
            else
            { /* block id: 781 */
                uint64_t l_1683 = 18446744073709551610UL;
                int32_t l_1688 = 9L;
                int32_t l_1689[2];
                uint64_t *l_1698[1][5][10] = {{{&l_1683,&g_282,&l_1683,&l_1651,&l_1651,&l_1683,&g_282,&l_1683,(void*)0,&g_22},{&g_22,&g_19,&g_22,&l_1683,&l_1651,&l_1651,&g_282,&g_22,&g_282,&l_1651},{&g_282,&l_1651,&g_22,&l_1651,&g_282,&g_22,(void*)0,&l_1683,&g_282,&l_1683},{(void*)0,&g_282,&l_1683,&g_22,&g_22,&l_1683,&l_1683,&g_22,&g_22,&l_1683},{&g_22,&g_22,&g_19,&l_1683,&g_282,&l_1651,&g_22,&g_22,&l_1683,&l_1651}}};
                int8_t *l_1701 = &l_1518;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_1689[i] = 0xDB4399D8L;
            }
        }
        l_1516 = p_26;
    }
    else
    { /* block id: 820 */
        uint32_t l_1722 = 0x4ADEACD2L;
        int32_t l_1727 = 0L;
        for (g_282 = 0; (g_282 <= 0); g_282 += 1)
        { /* block id: 823 */
            int32_t *l_1706 = &g_100;
            (*l_1706) &= ((void*)0 == l_1706);
            if ((*l_1516))
                continue;
            for (l_1654 = 0; (l_1654 <= 0); l_1654 += 1)
            { /* block id: 828 */
                uint64_t * const *l_1721[4];
                int16_t *l_1731 = (void*)0;
                int16_t **l_1730 = &l_1731;
                int16_t ***l_1729[3][3] = {{(void*)0,&l_1730,(void*)0},{(void*)0,&l_1730,(void*)0},{(void*)0,&l_1730,(void*)0}};
                int16_t ****l_1728[9];
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1721[i] = &g_738;
                for (i = 0; i < 9; i++)
                    l_1728[i] = &l_1729[1][0];
                g_1486[l_1654][(l_1654 + 1)] = g_1486[g_282][(g_282 + 8)];
                for (g_181 = 1; (g_181 > (-3)); g_181 = safe_sub_func_uint16_t_u_u(g_181, 7))
                { /* block id: 832 */
                    uint8_t l_1713 = 0x1DL;
                    int32_t l_1723[10][10][2] = {{{(-1L),4L},{0x2597D535L,0L},{0x8B1BAB54L,0x6A6747A9L},{0x5F080542L,0xB8191352L},{0x5753EF49L,0xB8191352L},{0x5F080542L,0x6A6747A9L},{0x8B1BAB54L,0L},{0x2597D535L,4L},{(-1L),(-1L)},{(-1L),(-4L)}},{{0x6A6747A9L,0x5753EF49L},{0L,0xDD4A8709L},{(-4L),0L},{1L,0L},{1L,0L},{(-4L),0xDD4A8709L},{0L,0x5753EF49L},{0x6A6747A9L,(-4L)},{(-1L),(-1L)},{(-1L),4L}},{{0x2597D535L,0L},{0x8B1BAB54L,0x6A6747A9L},{0x5F080542L,0xB8191352L},{0x5753EF49L,0xB8191352L},{0x5F080542L,0x6A6747A9L},{0x8B1BAB54L,0L},{0x2597D535L,4L},{1L,1L},{0xDD4A8709L,0xB8191352L},{(-1L),4L}},{{0x5753EF49L,(-4L)},{0xB8191352L,0x5753EF49L},{0x7B509C41L,(-6L)},{0x7B509C41L,0x5753EF49L},{0xB8191352L,(-4L)},{0x5753EF49L,4L},{(-1L),0xB8191352L},{0xDD4A8709L,1L},{1L,(-1L)},{0x6A6747A9L,0L}},{{0x49D54BA8L,(-1L)},{0x2597D535L,0x8B1BAB54L},{4L,0x8B1BAB54L},{0x2597D535L,(-1L)},{0x49D54BA8L,0L},{0x6A6747A9L,(-1L)},{1L,1L},{0xDD4A8709L,0xB8191352L},{(-1L),4L},{0x5753EF49L,(-4L)}},{{0xB8191352L,0x5753EF49L},{0x7B509C41L,(-6L)},{0x7B509C41L,0x5753EF49L},{0xB8191352L,(-4L)},{0x5753EF49L,4L},{(-1L),0xB8191352L},{0xDD4A8709L,1L},{1L,(-1L)},{0x6A6747A9L,0L},{0x49D54BA8L,(-1L)}},{{0x2597D535L,0x8B1BAB54L},{4L,0x8B1BAB54L},{0x2597D535L,(-1L)},{0x49D54BA8L,0L},{0x6A6747A9L,(-1L)},{1L,1L},{0xDD4A8709L,0xB8191352L},{(-1L),4L},{0x5753EF49L,(-4L)},{0xB8191352L,0x5753EF49L}},{{0x7B509C41L,(-6L)},{0x7B509C41L,0x5753EF49L},{0xB8191352L,(-4L)},{0x5753EF49L,4L},{(-1L),0xB8191352L},{0xDD4A8709L,1L},{1L,(-1L)},{0x6A6747A9L,0L},{0x49D54BA8L,(-1L)},{0x2597D535L,0x8B1BAB54L}},{{4L,0x8B1BAB54L},{0x2597D535L,(-1L)},{0x49D54BA8L,0L},{0x6A6747A9L,(-1L)},{1L,1L},{0xDD4A8709L,0xB8191352L},{(-1L),4L},{0x5753EF49L,(-4L)},{0xB8191352L,0x5753EF49L},{0x7B509C41L,(-6L)}},{{0x7B509C41L,0x5753EF49L},{0xB8191352L,(-4L)},{0x5753EF49L,4L},{(-1L),0xB8191352L},{0xDD4A8709L,1L},{1L,(-1L)},{0x6A6747A9L,0L},{0x49D54BA8L,(-1L)},{0x2597D535L,0x8B1BAB54L},{4L,0x8B1BAB54L}}};
                    int16_t *****l_1732 = (void*)0;
                    int16_t *****l_1733 = &l_1728[2];
                    int i, j, k;
                    (*l_1706) = (((p_25 ^ (+(safe_lshift_func_int8_t_s_s((l_1723[0][5][0] = (((l_1712 == &g_374[0]) & ((l_1713 = (0x87737096088E129CLL ^ p_25)) != p_25)) , ((safe_lshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((p_25 <= (p_25 | ((l_1720 == l_1721[2]) ^ p_25))), 0x5DL)), l_1722)), l_1713)) < p_25))), 3)))) && p_25) > (*l_1706));
                    (*g_1724) = &l_1723[0][5][0];
                    (*g_257) &= ((*g_959) <= (((safe_lshift_func_int16_t_s_s(l_1727, 6)) < (((*l_1733) = l_1728[1]) != &l_1729[1][1])) , (*l_1516)));
                }
            }
        }
        return p_26;
    }
    return p_26;
}


/* ------------------------------------------ */
/* 
 * reads : g_288 g_181 g_246 g_63 g_1119 g_62 g_399 g_22 g_298 g_156 g_373 g_374 g_466 g_124 g_257 g_3 g_465 g_127 g_68 g_878 g_100 g_51 g_519 g_520 g_1276 g_117 g_32 g_1075 g_358 g_609 g_610 g_1365 g_788 g_789
 * writes: g_51 g_63 g_3 g_633 g_399 g_246 g_1365 g_124 g_117 g_257 g_32 g_100 g_22 g_181 g_358 g_463 g_1461
 */
static int64_t  func_27(int64_t  p_28)
{ /* block id: 604 */
    uint64_t **l_1199 = &g_738;
    int8_t ***l_1202 = &g_465;
    int32_t l_1203 = 0x05027CC6L;
    int32_t l_1204 = 1L;
    uint32_t l_1229 = 0x07CE082DL;
    int32_t *l_1316 = (void*)0;
    uint8_t l_1336[4][7][2] = {{{0x2CL,0UL},{0x2CL,255UL},{0UL,0x2CL},{0x2CL,9UL},{0x2CL,0x2CL},{0UL,255UL},{0x2CL,0UL}},{{0x2CL,255UL},{0UL,0x2CL},{0x2CL,9UL},{0x2CL,0x2CL},{0UL,255UL},{0x2CL,0UL},{0x2CL,255UL}},{{0UL,0x2CL},{0x2CL,9UL},{0x2CL,0x2CL},{0UL,255UL},{0x2CL,0UL},{0x2CL,255UL},{0UL,0x2CL}},{{0x2CL,9UL},{0UL,0UL},{0x76L,9UL},{0UL,0x76L},{0UL,9UL},{0x76L,0UL},{0UL,0UL}}};
    int8_t l_1342 = (-4L);
    uint8_t l_1343[9];
    uint32_t *l_1364 = &g_156;
    uint32_t l_1368 = 6UL;
    float l_1453 = 0x8.042649p+10;
    int32_t l_1454 = 0xE0DD12F9L;
    int16_t *l_1455[9] = {(void*)0,(void*)0,&g_762[4],(void*)0,(void*)0,&g_762[4],(void*)0,(void*)0,&g_762[4]};
    uint32_t l_1456 = 4UL;
    int16_t **l_1458 = &l_1455[2];
    int16_t ***l_1457[8] = {&l_1458,&l_1458,&l_1458,&l_1458,&l_1458,&l_1458,&l_1458,&l_1458};
    float l_1476 = 0x4.6p+1;
    uint32_t ** const *l_1485 = &g_1118;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_1343[i] = 255UL;
lbl_1373:
    l_1204 |= ((*g_288) && (safe_mod_func_int8_t_s_s((((void*)0 != l_1199) && (*g_246)), ((((((*g_1119) = 0x01BCD30CL) == (1L <= ((p_28 | (safe_rshift_func_uint8_t_u_s(p_28, ((void*)0 != l_1202)))) > l_1203))) <= l_1203) , l_1203) && l_1203))));
    if (p_28)
    { /* block id: 607 */
        uint32_t l_1272[7][1] = {{0xF8002B44L},{1UL},{1UL},{0xF8002B44L},{1UL},{1UL},{0xF8002B44L}};
        int16_t l_1273[1];
        uint32_t *l_1332 = &l_1229;
        int64_t l_1337 = 0xB87EC8100BD8EEC3LL;
        int32_t l_1349[10][4][6] = {{{0x3190B3ACL,(-1L),0xD9424721L,0xDA5DFD64L,1L,0xA8F36533L},{(-1L),(-4L),0x1142D81FL,0xB7A7C6E5L,1L,0xE234BA0EL},{(-8L),(-1L),0x117A47D6L,(-4L),0x816204EBL,1L},{0xB9B54E89L,0x279437E2L,0x9462D94BL,0xC3A85C8AL,0xC3A85C8AL,0x9462D94BL}},{{0x816204EBL,0x816204EBL,0xD9424721L,(-4L),0L,1L},{0xC161210EL,0x117A47D6L,0x279437E2L,0xB7A7C6E5L,1L,0xD9424721L},{1L,0xC161210EL,0x279437E2L,0xDA5DFD64L,0x816204EBL,1L},{1L,0xDA5DFD64L,0xD9424721L,(-1L),0x3190B3ACL,0x9462D94BL}},{{(-1L),0x3190B3ACL,0x9462D94BL,0x1142D81FL,1L,1L},{0xC161210EL,0xDA5DFD64L,0x117A47D6L,0x3190B3ACL,0xB9B54E89L,0xE234BA0EL},{0xB9B54E89L,0xC161210EL,0x1142D81FL,0xBB6C1134L,0xC3A85C8AL,0xA8F36533L},{0xB9B54E89L,0x117A47D6L,0xD9424721L,0x3190B3ACL,(-1L),0x24CCDF08L}},{{0xC161210EL,0x816204EBL,0x369C154AL,0x1142D81FL,1L,1L},{(-1L),0x279437E2L,0x279437E2L,(-1L),0xB9B54E89L,0x24CCDF08L},{1L,(-1L),0xBB6C1134L,0xDA5DFD64L,0x3190B3ACL,0xA8F36533L},{1L,(-4L),0x9462D94BL,0xB7A7C6E5L,0x3190B3ACL,0xE234BA0EL}},{{0xC161210EL,(-1L),0x9B19842BL,(-4L),0xB9B54E89L,1L},{0x816204EBL,0x279437E2L,0x1142D81FL,0xC3A85C8AL,1L,0x9462D94BL},{0xB9B54E89L,0x816204EBL,0xBB6C1134L,(-4L),(-1L),1L},{(-8L),0x117A47D6L,0x369C154AL,0xB7A7C6E5L,0xC3A85C8AL,0xD9424721L}},{{(-1L),0xC161210EL,0x369C154AL,0xDA5DFD64L,0xB9B54E89L,1L},{0x3190B3ACL,0xDA5DFD64L,0xBB6C1134L,(-1L),1L,0x9462D94BL},{1L,0x3190B3ACL,0x1142D81FL,0x1142D81FL,0x3190B3ACL,1L},{(-8L),0xDA5DFD64L,0x9B19842BL,0x3190B3ACL,0x816204EBL,0xE234BA0EL}},{{0x816204EBL,0xC161210EL,0x9462D94BL,0xBB6C1134L,1L,0xA8F36533L},{0x816204EBL,0x117A47D6L,0xBB6C1134L,0x3190B3ACL,0L,0x9462D94BL},{1L,(-1L),(-4L),0xBB6C1134L,0x816204EBL,0xB9B54E89L},{(-8L),(-4L),0x24CCDF08L,0xC161210EL,(-1L),0x9462D94BL}},{{0xB7A7C6E5L,0xC161210EL,0x9B19842BL,0x279437E2L,0xA8F36533L,1L},{0xC161210EL,0x1142D81FL,0xBB6C1134L,0xC3A85C8AL,0xA8F36533L,0x369C154AL},{1L,0xC161210EL,(-1L),0x1142D81FL,(-1L),(-8L)},{0L,(-4L),0xD9424721L,0x816204EBL,0x816204EBL,0xD9424721L}},{{(-1L),(-1L),0x9B19842BL,0x1142D81FL,1L,0xA8F36533L},{0x3190B3ACL,(-1L),(-4L),0xC3A85C8AL,0xB9B54E89L,0x9B19842BL},{(-8L),0x3190B3ACL,(-4L),0x279437E2L,(-1L),0xA8F36533L},{0xA8F36533L,0x279437E2L,0x9B19842BL,0xC161210EL,0xB7A7C6E5L,0xD9424721L}},{{0xC161210EL,0xB7A7C6E5L,0xD9424721L,0xBB6C1134L,0xA8F36533L,(-8L)},{0x3190B3ACL,0x279437E2L,(-1L),0xB7A7C6E5L,0L,0x369C154AL},{0L,0x3190B3ACL,0xBB6C1134L,0x117A47D6L,0x816204EBL,1L},{0L,(-1L),0x9B19842BL,0xB7A7C6E5L,(-1L),0x9462D94BL}}};
        int8_t ****l_1439 = &l_1202;
        int8_t ****l_1440[4];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1273[i] = 0x1F42L;
        for (i = 0; i < 4; i++)
            l_1440[i] = &g_464;
        if (p_28)
        { /* block id: 608 */
            uint32_t * const l_1219 = &g_125[1][0];
            int32_t l_1226 = 0xF48A3084L;
            int64_t **l_1227 = (void*)0;
            int64_t **l_1228 = &g_246;
            int32_t l_1274 = (-1L);
            const uint64_t *l_1295 = &g_431;
            const uint64_t **l_1294 = &l_1295;
            const uint64_t ***l_1293[5][10] = {{&l_1294,&l_1294,(void*)0,&l_1294,&l_1294,&l_1294,&l_1294,&l_1294,&l_1294,&l_1294},{&l_1294,&l_1294,&l_1294,&l_1294,(void*)0,&l_1294,&l_1294,(void*)0,(void*)0,&l_1294},{(void*)0,&l_1294,&l_1294,&l_1294,&l_1294,(void*)0,&l_1294,&l_1294,(void*)0,(void*)0},{&l_1294,&l_1294,(void*)0,&l_1294,&l_1294,(void*)0,&l_1294,&l_1294,&l_1294,&l_1294},{&l_1294,(void*)0,&l_1294,&l_1294,(void*)0,(void*)0,&l_1294,&l_1294,(void*)0,&l_1294}};
            int32_t l_1341 = 0L;
            int i, j;
            for (g_63 = 0; (g_63 <= (-26)); g_63--)
            { /* block id: 611 */
                return (*g_62);
            }
            if ((((safe_sub_func_int16_t_s_s(((((*l_1228) = func_69(((safe_lshift_func_uint16_t_u_s((0xD98FL != (safe_lshift_func_int8_t_s_u(((safe_rshift_func_int16_t_s_u((((*g_1119) = ((safe_mul_func_uint16_t_u_u(l_1203, ((safe_lshift_func_int16_t_s_s(((void*)0 != l_1219), p_28)) && (safe_mul_func_uint16_t_u_u(6UL, p_28))))) == ((safe_lshift_func_uint16_t_u_s(((l_1204 < (safe_add_func_int8_t_s_s(l_1226, l_1203))) != p_28), 6)) > p_28))) | (-1L)), g_399[0])) == 0x6BA2EB34551A2F32LL), 2))), 2)) < p_28), l_1226, p_28, p_28)) == (void*)0) , l_1203), p_28)) > l_1229) , (*g_878)))
            { /* block id: 616 */
                uint64_t *l_1253 = &g_431;
                int16_t *l_1270 = &g_127[2];
                int16_t *l_1271[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t *l_1275 = (void*)0;
                int32_t *l_1315 = &g_123[2][0];
                int32_t **l_1339 = &l_1275;
                int i;
            }
            else
            { /* block id: 650 */
                int32_t *l_1340[3][6] = {{&g_100,&g_100,&g_100,&g_100,&l_1203,&g_100},{&l_1226,&g_100,&l_1203,&l_1226,&l_1203,&g_100},{&l_1203,&g_100,&g_100,&l_1203,&l_1203,&l_1203}};
                int i, j;
                l_1343[6]++;
            }
        }
        else
        { /* block id: 653 */
            int32_t *l_1346 = &l_1203;
            int32_t *l_1347 = &g_100;
            int32_t *l_1348[1][6][8] = {{{&g_100,&g_181,&g_181,&g_100,&g_181,&g_181,&g_100,&g_181},{&g_100,&g_100,&l_1204,&g_100,&g_100,&l_1204,&g_100,&g_100},{&g_181,&g_100,&g_181,&g_181,&g_100,&g_181,&g_181,&g_100},{&g_100,&g_181,&g_181,&g_100,&g_181,&g_181,&g_181,&l_1204},{&g_181,&g_181,&g_100,&g_181,&g_181,&g_100,&g_181,&g_181},{&l_1204,&g_181,&l_1204,&l_1204,&g_181,&l_1204,&l_1204,&g_181}}};
            uint64_t l_1350 = 0xF0CDC6C7DEB9845CLL;
            uint32_t **l_1361 = (void*)0;
            uint32_t *l_1363 = (void*)0;
            uint32_t **l_1362[8][5][3] = {{{&l_1363,&l_1363,&l_1363},{&l_1363,(void*)0,&l_1363},{&l_1363,&l_1363,&l_1363},{(void*)0,&l_1363,(void*)0},{(void*)0,(void*)0,&l_1363}},{{&l_1363,(void*)0,&l_1363},{&l_1363,(void*)0,(void*)0},{&l_1363,&l_1363,&l_1363},{(void*)0,(void*)0,&l_1363},{(void*)0,&l_1363,&l_1363}},{{&l_1363,&l_1363,(void*)0},{(void*)0,&l_1363,&l_1363},{&l_1363,&l_1363,(void*)0},{&l_1363,&l_1363,&l_1363},{&l_1363,(void*)0,&l_1363}},{{(void*)0,&l_1363,&l_1363},{&l_1363,&l_1363,(void*)0},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,(void*)0}},{{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363}},{{(void*)0,(void*)0,&l_1363},{(void*)0,&l_1363,&l_1363},{&l_1363,&l_1363,(void*)0},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363}},{{(void*)0,&l_1363,(void*)0},{(void*)0,(void*)0,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,(void*)0,(void*)0},{&l_1363,(void*)0,&l_1363}},{{&l_1363,(void*)0,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,&l_1363,&l_1363},{&l_1363,(void*)0,(void*)0},{&l_1363,&l_1363,&l_1363}}};
            int16_t * const *l_1460 = &l_1455[2];
            int i, j, k;
            --l_1350;
            if ((((((*l_1346) = (((((1UL <= (safe_mul_func_uint8_t_u_u(((*g_257) >= 0x45E257ABL), (safe_mul_func_uint8_t_u_u((((((safe_lshift_func_int16_t_s_s(((((safe_mul_func_int16_t_s_s(p_28, (&l_1272[0][0] == (g_1365 = (l_1364 = &g_156))))) , (((((*g_1119) && (safe_div_func_uint32_t_u_u(p_28, (*l_1346)))) , p_28) > 0x053743FBL) , p_28)) & 0xDF21A6F69CD7A19CLL) || l_1343[7]), l_1368)) && p_28) > p_28) > p_28) && (*g_519)), l_1368))))) & p_28) ^ (*g_1119)) , 0x4.33B083p-39) >= (*l_1346))) > 0xE.E23B91p+36) , (-10L)) | p_28))
            { /* block id: 658 */
                int32_t l_1374 = (-1L);
                int32_t **l_1379 = &g_257;
                int32_t **l_1380 = &l_1346;
                uint32_t **l_1393 = &l_1332;
                uint32_t *l_1401 = &l_1272[6][0];
                uint32_t l_1402 = 4294967295UL;
                for (l_1204 = (-19); (l_1204 != 13); l_1204 = safe_add_func_uint16_t_u_u(l_1204, 4))
                { /* block id: 661 */
                    uint32_t l_1378[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1378[i] = 0xC96FCBCDL;
                    if ((safe_sub_func_int8_t_s_s(((***l_1202) = 0xE0L), p_28)))
                    { /* block id: 663 */
                        uint64_t l_1375 = 0x40D1541C97EF3CFBLL;
                        if (l_1350)
                            goto lbl_1373;
                        l_1375--;
                        if (l_1337)
                            continue;
                    }
                    else
                    { /* block id: 667 */
                        (*l_1346) ^= (**g_1276);
                    }
                    for (g_117 = 0; (g_117 <= 2); g_117 += 1)
                    { /* block id: 672 */
                        l_1378[4] ^= 0xBA584E0FL;
                        return p_28;
                    }
                }
                (*l_1380) = ((*l_1379) = (*g_1276));
                for (g_32 = 0; (g_32 >= 0); g_32 -= 1)
                { /* block id: 681 */
                    uint64_t *l_1396 = &g_22;
                    const int32_t l_1427[8][8][4] = {{{0x375A821DL,0xA3DC4998L,(-6L),1L},{0x14D673DAL,0x1C8ECF79L,0x29998770L,(-1L)},{0x5828E744L,0x26FC4F42L,1L,0x2033085FL},{0xA3DC4998L,0xA9433502L,(-9L),(-9L)},{0x0F7118D4L,0x0F7118D4L,0x5828E744L,0L},{0x168377F5L,0x14D673DAL,0x2329FFBDL,(-1L)},{1L,0x5D9468D5L,0xC937762DL,0x2329FFBDL},{0xBAE284C9L,0x5D9468D5L,0x5E32BE58L,(-1L)}},{{0x5D9468D5L,0x14D673DAL,0x69DA8ED9L,0L},{(-1L),0x0F7118D4L,0xB899CA39L,(-9L)},{(-5L),0xA9433502L,0x1C8ECF79L,0x2033085FL},{0x5E32BE58L,0x26FC4F42L,(-5L),(-1L)},{(-8L),0x1C8ECF79L,(-4L),1L},{0L,(-1L),(-1L),0L},{7L,0x72B20D0CL,0x2329FFBDL,(-1L)},{0L,0x9EB3780BL,1L,(-1L)}},{{(-6L),0x14D673DAL,(-1L),0x5D9468D5L},{0xBB227576L,7L,0x29998770L,0x5828E744L},{0x78E4B5AFL,0x375A821DL,0L,0x375A821DL},{0x168377F5L,(-5L),0x14D673DAL,0x32F2CCACL},{0x72B20D0CL,(-1L),1L,0x78E4B5AFL},{1L,0x2033085FL,(-6L),0L},{1L,1L,1L,0L},{0x72B20D0CL,0L,0x14D673DAL,0xA9433502L}},{{0x168377F5L,0x1050AC61L,0L,0x2329FFBDL},{0x78E4B5AFL,0xE5532258L,0x29998770L,1L},{0xBB227576L,(-9L),(-1L),1L},{(-6L),0x78E4B5AFL,1L,0x5E32BE58L},{0L,0x26FC4F42L,0x2329FFBDL,0x1C8ECF79L},{7L,1L,(-1L),0xC937762DL},{0L,0L,0x26FC4F42L,0x2033085FL},{(-9L),(-1L),0L,0xB899CA39L}},{{0xBAF8E4F2L,0x168377F5L,0xA7D5DEA4L,0xBB227576L},{0L,1L,1L,0L},{0x2033085FL,0x69DA8ED9L,(-4L),(-1L)},{0x1C8ECF79L,0xC937762DL,0xBAF8E4F2L,(-1L)},{1L,9L,0x5E32BE58L,(-1L)},{0x2FCC86A6L,0xC937762DL,0xE5532258L,(-1L)},{0x14D673DAL,0x69DA8ED9L,0L,0L},{1L,1L,0x72B20D0CL,0xBB227576L}},{{(-1L),0x168377F5L,1L,0xB899CA39L},{0L,(-1L),0x375A821DL,0x2033085FL},{(-3L),0L,(-8L),0xC937762DL},{9L,1L,0x32F2CCACL,0x1C8ECF79L},{0x5D9468D5L,0x26FC4F42L,(-6L),0x5E32BE58L},{0x5E32BE58L,0x78E4B5AFL,0x9EB3780BL,1L},{(-1L),(-9L),(-1L),1L},{(-8L),0xE5532258L,1L,0x2329FFBDL}},{{0x69DA8ED9L,0x1050AC61L,0x69DA8ED9L,0xA9433502L},{0x29998770L,0L,9L,0L},{(-4L),1L,(-1L),0L},{0x32F2CCACL,0x2033085FL,(-1L),0x78E4B5AFL},{(-4L),(-1L),9L,0x32F2CCACL},{0x29998770L,(-5L),0x69DA8ED9L,0x375A821DL},{0x69DA8ED9L,0x375A821DL,1L,0x5828E744L},{(-8L),7L,(-1L),0x5D9468D5L}},{{(-1L),0x14D673DAL,0x9EB3780BL,(-1L)},{0x5E32BE58L,0x9EB3780BL,(-6L),(-1L)},{0x5D9468D5L,0x72B20D0CL,0x32F2CCACL,0L},{9L,(-1L),(-8L),1L},{(-3L),0xA7D5DEA4L,0x375A821DL,0x29998770L},{7L,0x5828E744L,(-1L),1L},{0x9EB3780BL,(-1L),(-6L),(-6L)},{0L,0L,7L,0x3D88A5A0L}}};
                    int i, j, k;
                    if ((**g_1075))
                    { /* block id: 682 */
                        (*g_878) |= (p_28 & ((*g_62) = 0L));
                    }
                    else
                    { /* block id: 685 */
                        l_1203 ^= (safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((p_28 , g_399[0]), 1L)), (safe_lshift_func_uint8_t_u_u((((**l_1379) | p_28) != (safe_rshift_func_int8_t_s_u((0x28L ^ (**l_1379)), 3))), p_28))));
                    }
                    for (l_1374 = 8; (l_1374 >= 2); l_1374 -= 1)
                    { /* block id: 690 */
                        (*l_1379) = l_1346;
                    }
                    (**l_1379) &= (((safe_mul_func_int8_t_s_s((l_1349[0][1][2] && (safe_sub_func_uint32_t_u_u(((void*)0 != l_1393), ((*g_1119) = 0xD545D296L)))), ((((*l_1347) = ((safe_mod_func_uint64_t_u_u(((*l_1396)++), 0x754A35C3B68B5424LL)) ^ ((safe_mul_func_int16_t_s_s(p_28, (l_1273[0] >= ((g_1365 = (l_1401 = ((p_28 , (((&g_1317 != &g_1317) > (*l_1347)) ^ l_1337)) , (void*)0))) != &g_156)))) & 0x2EBFL))) && 0x5D0CA5A3L) , (*g_466)))) < l_1402) , (*l_1347));
                    for (g_358 = 0; (g_358 <= 8); g_358 += 1)
                    { /* block id: 701 */
                        int8_t *l_1428[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1428[i] = &g_399[0];
                        (*l_1347) = ((safe_div_func_int8_t_s_s((*g_466), ((**l_1380) = (safe_rshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(((safe_add_func_int8_t_s_s((((g_3[(g_32 + 5)] >= ((safe_sub_func_float_f_f((g_3[g_358] != (**l_1380)), (l_1348[0][0][6] != (((**l_1379) ^ p_28) , (*g_609))))) > (safe_mul_func_float_f_f((safe_div_func_float_f_f((((safe_add_func_float_f_f((safe_add_func_float_f_f((safe_div_func_float_f_f(((safe_sub_func_float_f_f(((((safe_mul_func_uint8_t_u_u(p_28, 0xEFL)) , p_28) != p_28) == 0x4.35BE64p+7), p_28)) < p_28), (**l_1380))), p_28)), 0x0.1p+1)) >= p_28) <= (-0x1.Ep-1)), (**l_1380))), l_1427[4][2][2])))) == p_28) , p_28), l_1427[7][6][0])) | p_28), l_1273[0])), p_28))))) <= 4UL);
                    }
                }
            }
            else
            { /* block id: 706 */
                int8_t *****l_1441 = (void*)0;
                int8_t *****l_1442 = &g_463[4][0][2];
                int32_t l_1452[2];
                int16_t ** const *l_1459 = (void*)0;
                int i;
                for (i = 0; i < 2; i++)
                    l_1452[i] = 0xFA3ED000L;
                (*g_298) = ((safe_sub_func_float_f_f((*l_1347), p_28)) > (safe_mul_func_float_f_f((safe_add_func_float_f_f((safe_mul_func_float_f_f(p_28, (safe_sub_func_float_f_f((l_1439 != ((*l_1442) = l_1440[2])), ((((-(((((((safe_unary_minus_func_uint8_t_u(((((l_1456 = ((safe_lshift_func_int16_t_s_s(p_28, (l_1349[6][1][4] = 0x5690L))) != ((~p_28) <= (safe_mul_func_int8_t_s_s((((((((((**g_465) ^= (l_1452[0] >= l_1454)) > l_1272[5][0]) >= l_1203) < 1L) , (void*)0) != l_1455[2]) , (*g_1365)) , l_1229), p_28))))) && 4294967290UL) , l_1457[0]) == l_1459))) , (*g_788)) == l_1460) == 0xF.6F5EF8p+4) == p_28) != p_28) != (-0x1.Ap-1))) <= p_28) == l_1452[0]) >= p_28))))), 0x3.C39B18p+26)), (-0x4.4p+1))));
            }
        }
    }
    else
    { /* block id: 714 */
        int32_t l_1467 = (-6L);
        uint8_t *l_1468 = &l_1336[1][4][0];
        int16_t *l_1473 = &g_762[5];
        float * const l_1477 = (void*)0;
        uint32_t ***l_1484 = &g_1118;
        int32_t l_1487 = 0xB30F4EC8L;
        g_1461 = (void*)0;
        l_1487 = ((*g_257) |= (0x75L < ((safe_mul_func_int8_t_s_s((l_1467 &= (-3L)), (++(*l_1468)))) , ((((safe_rshift_func_uint8_t_u_s((l_1473 == l_1473), 1)) >= ((l_1203 = (safe_lshift_func_int8_t_s_u(p_28, 4))) , (l_1477 != l_1477))) != (safe_mod_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((*g_519), (l_1484 == l_1485))), p_28)), l_1467))) > 9UL))));
    }
    return (*g_246);
}


/* ------------------------------------------ */
/* 
 * reads : g_435 g_257 g_1075 g_436 g_254 g_127 g_298 g_63 g_358 g_181 g_20 g_3 g_122 g_530 g_762 g_485 g_100 g_107
 * writes: g_257 g_254 g_181 g_3 g_63 g_358 g_20 g_127 g_122 g_762 g_485 g_100 g_464
 */
static int32_t  func_35(uint32_t  p_36, uint32_t  p_37, int64_t  p_38, int32_t * p_39)
{ /* block id: 532 */
    int32_t **l_1122[8][10] = {{(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0},{&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257},{&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257},{(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0},{&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257},{&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257},{(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0,&g_257,(void*)0,(void*)0},{&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257,&g_257}};
    int32_t *l_1124[7];
    uint64_t l_1131 = 0x6D9409438D441273LL;
    uint32_t l_1142 = 1UL;
    uint16_t l_1148 = 6UL;
    int8_t ***l_1193 = &g_465;
    int i, j;
    for (i = 0; i < 7; i++)
        l_1124[i] = &g_181;
    (*g_1075) = (*g_435);
    (*g_436) = p_39;
    for (g_254 = 0; (g_254 <= 2); g_254 += 1)
    { /* block id: 537 */
        int32_t l_1128 = 0xACE48CC8L;
        int32_t l_1129[4][5] = {{(-10L),0x656AD15BL,(-10L),0x3D07C8F1L,0x3D07C8F1L},{(-10L),0x656AD15BL,(-10L),0x3D07C8F1L,0x3D07C8F1L},{(-10L),0x656AD15BL,(-10L),0x3D07C8F1L,0x3D07C8F1L},{(-10L),0x656AD15BL,(-10L),0x3D07C8F1L,0x3D07C8F1L}};
        int i, j;
        (*p_39) = g_127[g_254];
        for (g_181 = 3; (g_181 >= 0); g_181 -= 1)
        { /* block id: 541 */
            uint16_t l_1123[7] = {2UL,2UL,0x4C0BL,2UL,2UL,0x4C0BL,2UL};
            int32_t l_1125 = 7L;
            int32_t l_1126 = 0L;
            int32_t l_1127 = 0x219BC2E3L;
            int32_t l_1130 = 1L;
            uint8_t l_1149[5][4] = {{0xEAL,0xEAL,0xEAL,0xEAL},{0xEAL,0xEAL,0xEAL,0xEAL},{0xEAL,0xEAL,0xEAL,0xEAL},{0xEAL,0xEAL,0xEAL,0xEAL},{0xEAL,0xEAL,0xEAL,0xEAL}};
            uint32_t l_1153 = 0xC31183A6L;
            int i, j;
            if (l_1123[2])
                break;
            p_39 = l_1124[1];
            l_1131++;
            for (l_1125 = 0; (l_1125 <= 7); l_1125 += 1)
            { /* block id: 547 */
                uint8_t *l_1138 = (void*)0;
                uint8_t *l_1140 = &g_107;
                uint8_t **l_1139 = &l_1140;
                int i;
            }
            for (p_38 = 3; (p_38 >= 0); p_38 -= 1)
            { /* block id: 561 */
                uint16_t l_1145 = 0xF6E6L;
                uint32_t ***l_1150 = &g_1118;
                float *l_1151[4];
                int32_t l_1152 = 0x4FAB6C0EL;
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1151[i] = &g_354;
                l_1127 = (p_36 , (l_1145 , (0x1.9p-1 < (l_1152 = (0x1.Ap+1 < ((((((*g_298) = 0x5.0CD4D8p-66) == g_127[g_254]) != (((safe_add_func_float_f_f((l_1148 == l_1125), ((((l_1149[0][1] != l_1145) < l_1149[1][3]) , &g_1118) != l_1150))) != p_36) > p_38)) != 0x6.FFCECFp+95) >= 0x2.1p+1))))));
                ++l_1153;
                for (g_63 = 3; (g_63 >= 0); g_63 -= 1)
                { /* block id: 568 */
                    int8_t * const *l_1184 = &g_466;
                    int8_t * const **l_1183 = &l_1184;
                    const int32_t l_1185 = 0x13D53582L;
                    for (g_358 = 0; (g_358 <= 2); g_358 += 1)
                    { /* block id: 571 */
                        if ((*p_39))
                            break;
                    }
                    for (g_20 = 0; (g_20 <= 3); g_20 += 1)
                    { /* block id: 576 */
                        int16_t *l_1177 = &g_127[g_254];
                        int32_t l_1180 = 5L;
                        int16_t *l_1186 = &g_762[0];
                        int16_t *l_1187 = &g_485;
                        int i, j, k;
                        l_1129[0][4] = l_1145;
                        if ((**g_435))
                            continue;
                        g_100 &= (safe_add_func_uint64_t_u_u(l_1152, ((((*l_1187) ^= ((*l_1186) ^= (safe_add_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(((((safe_mul_func_float_f_f((((safe_sub_func_float_f_f((((safe_mul_func_float_f_f((safe_sub_func_float_f_f((((p_36 >= (l_1180 = ((*g_298) = (l_1127 = ((safe_mul_func_float_f_f(((safe_add_func_uint32_t_u_u(((((((!(((l_1152 <= p_38) | ((safe_rshift_func_int16_t_s_s(p_37, 2)) , ((*l_1177) = 0x4065L))) < (g_122 |= ((safe_sub_func_float_f_f((p_37 == (l_1180 == (safe_mul_func_float_f_f(p_38, p_38)))), l_1130)) , 0xC0100C56B267AEEALL)))) , (void*)0) == l_1183) == l_1185) ^ p_37) , 0x9701B8A0L), (**g_530))) , l_1180), p_38)) >= p_38))))) < 0x8.Ep+1) < p_36), p_36)), l_1152)) >= 0xA.6E979Dp+33) > (-0x6.9p+1)), l_1185)) , l_1185) == p_38), l_1129[0][4])) , 0UL) >= p_37) ^ p_38), 0xCBBBL)), l_1153)))) > 65535UL) | l_1185)));
                    }
                    for (l_1130 = (-17); (l_1130 < (-2)); l_1130 = safe_add_func_uint64_t_u_u(l_1130, 9))
                    { /* block id: 590 */
                        int8_t ***l_1192 = &g_465;
                        int8_t ****l_1194 = &g_464;
                        if ((*p_39))
                            break;
                        if ((*p_39))
                            break;
                        (*g_257) = ((safe_lshift_func_uint8_t_u_u(1UL, ((g_107 != 1UL) > (l_1192 != ((*l_1194) = l_1193))))) && 0x8414A07E854E7480LL);
                        if ((**g_435))
                            continue;
                    }
                    return l_1145;
                }
            }
        }
    }
    (*g_436) = p_39;
    return (*p_39);
}


/* ------------------------------------------ */
/* 
 * reads : g_15 g_51 g_62 g_22 g_298 g_156 g_373 g_374 g_466 g_124 g_257 g_181 g_465 g_127 g_399 g_68 g_107 g_100 g_761 g_246 g_63 g_219 g_122 g_372 g_370 g_371 g_1055 g_518 g_519 g_520 g_351 g_690 g_1075 g_790 g_737 g_20 g_282 g_256 g_358 g_1120
 * writes: g_15 g_20 g_51 g_68 g_3 g_633 g_399 g_282 g_124 g_107 g_100 g_358 g_761 g_181 g_219 g_1055 g_63 g_257 g_738 g_1118
 */
static uint16_t  func_42(uint8_t  p_43, uint8_t  p_44)
{ /* block id: 8 */
    int32_t *l_77 = &g_68[3][3];
    int32_t l_1092[9][8] = {{0L,(-2L),9L,9L,(-2L),0L,1L,0L},{(-2L),0L,1L,0L,(-2L),9L,9L,(-2L)},{0L,0x8D2CDDE0L,0x8D2CDDE0L,0L,0x2FECE078L,(-2L),0x2FECE078L,0L},{0x8D2CDDE0L,0x2FECE078L,0x8D2CDDE0L,9L,1L,1L,9L,0x8D2CDDE0L},{0x2FECE078L,0x2FECE078L,1L,(-2L),1L,(-2L),1L,0x2FECE078L},{0x2FECE078L,0x8D2CDDE0L,9L,1L,1L,9L,0x8D2CDDE0L,0x2FECE078L},{0x8D2CDDE0L,0L,0x2FECE078L,9L,1L,0x8D2CDDE0L,(-2L),(-2L)},{0x8D2CDDE0L,9L,1L,1L,9L,0x8D2CDDE0L,0x2FECE078L,0x8D2CDDE0L},{9L,0x8D2CDDE0L,0x2FECE078L,0x8D2CDDE0L,9L,1L,1L,9L}};
    int i, j;
    for (g_15 = 0; (g_15 == 56); ++g_15)
    { /* block id: 11 */
        float *l_65 = &g_3[7];
        int32_t l_76 = (-7L);
        uint64_t *l_1113 = &g_22;
        for (g_20 = 0; (g_20 <= 8); g_20 += 1)
        { /* block id: 14 */
            uint32_t *l_50 = &g_51[1];
            float *l_64 = (void*)0;
            int32_t *l_66 = (void*)0;
            int32_t *l_67 = &g_68[3][3];
            int32_t l_75 = 0x351F4A1AL;
            int i;
            (*g_298) = (((*l_50)--) , (safe_sub_func_float_f_f(0xE.C53A21p-31, ((safe_add_func_int16_t_s_s((g_15 != (func_58(g_62, (((*g_298) = ((p_44 , ((((*l_67) = (l_64 != l_65)) , func_69(p_43, ((((safe_unary_minus_func_int8_t_s((l_75 , (g_51[1] < l_76)))) , l_77) == &g_68[3][3]) , 0x579AB673L), g_22, p_43)) != &g_63)) <= p_44)) , p_44), &g_63) || 4UL)), g_351[6][2][5])) , p_44))));
            for (g_100 = 29; (g_100 != 9); --g_100)
            { /* block id: 504 */
                uint32_t l_1087[7][3][1];
                int i, j, k;
                for (i = 0; i < 7; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_1087[i][j][k] = 1UL;
                    }
                }
                (*g_1075) = (*g_690);
                for (g_124 = 25; (g_124 == 24); --g_124)
                { /* block id: 508 */
                    int32_t *l_1078 = &l_76;
                    int32_t *l_1079 = &g_181;
                    int32_t *l_1080 = &l_76;
                    int32_t *l_1081 = &g_181;
                    int32_t *l_1082 = &l_75;
                    int32_t *l_1083 = &g_181;
                    int32_t *l_1084 = &l_76;
                    int32_t *l_1085 = &l_76;
                    int32_t *l_1086[2][7][3] = {{{&l_76,(void*)0,&l_76},{&l_76,&g_100,(void*)0},{&g_100,&g_181,&g_100},{(void*)0,&g_100,&l_76},{&l_76,(void*)0,&l_76},{(void*)0,&l_76,&g_100},{&g_100,&g_100,(void*)0}},{{(void*)0,(void*)0,&l_76},{&l_76,(void*)0,(void*)0},{(void*)0,&g_100,&g_100},{&g_100,&l_76,(void*)0},{&l_76,(void*)0,&l_76},{&l_76,&g_100,(void*)0},{&g_100,&g_181,&g_100}}};
                    int i, j, k;
                    l_1087[0][1][0]--;
                }
                for (g_107 = 0; (g_107 == 1); g_107++)
                { /* block id: 513 */
                    if ((*g_790))
                        break;
                }
                (*g_257) &= l_1092[4][6];
            }
        }
        for (g_20 = 0; (g_20 <= 5); g_20 += 1)
        { /* block id: 521 */
            uint64_t *l_1094 = (void*)0;
            uint64_t *l_1101 = &g_282;
            uint16_t *l_1110 = &g_1055;
            int32_t *l_1114 = &l_1092[7][2];
            uint32_t *l_1116[5][6][1] = {{{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]}},{{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]}},{{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]}},{{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]}},{{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]},{&g_125[1][0]},{&g_51[1]}}};
            uint32_t **l_1115 = &l_1116[2][5][0];
            int i, j, k;
            (*l_1114) = ((*g_257) = ((((+(((*g_737) = l_1094) != ((safe_mul_func_int8_t_s_s(((0x1AF1FFA5L || (((safe_div_func_uint8_t_u_u(g_68[g_20][(g_20 + 2)], l_76)) | (g_107 && (((safe_mod_func_uint32_t_u_u(((((*l_1101)++) , (((18446744073709551615UL <= (safe_mul_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_u(((--(*l_1110)) <= 0xB54EL), 1)) && ((65535UL > 0x45A6L) , 0x0A8D014DL)), 4)) || 1UL) >= l_76), 1UL))) > l_1092[8][3]) ^ p_44)) != p_44), g_122)) != (**g_256)) == p_44))) , p_43)) || l_1092[4][0]), 0xD0L)) , l_1113))) < g_358) ^ l_1092[3][0]) , p_43));
            if (p_44)
                break;
            (*g_1120) = l_1115;
        }
    }
    return p_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_282 g_107 g_100 g_358 g_761 g_399 g_257 g_246 g_63 g_219 g_122 g_372 g_370 g_371 g_465 g_466 g_124 g_1055 g_518 g_519 g_520 g_62 g_351
 * writes: g_282 g_124 g_107 g_100 g_358 g_761 g_51 g_181 g_219 g_1055 g_63
 */
static uint64_t  func_58(int64_t * const  p_59, int16_t  p_60, const int64_t * p_61)
{ /* block id: 457 */
    uint32_t l_1009 = 0xE07713DAL;
    int8_t *l_1025 = &g_399[3];
    int32_t l_1032 = (-9L);
    int32_t l_1035 = 0xE0D6914EL;
    int8_t ** const *l_1059[4];
    int16_t l_1070 = 0xFD9DL;
    uint32_t l_1071 = 0xB434E89FL;
    uint32_t l_1072 = 0xFBFB0194L;
    int i;
    for (i = 0; i < 4; i++)
        l_1059[i] = &g_465;
    for (g_282 = 0; (g_282 >= 17); g_282 = safe_add_func_uint8_t_u_u(g_282, 7))
    { /* block id: 460 */
        int32_t *l_1000 = &g_100;
        int32_t *l_1001 = &g_100;
        int32_t *l_1002 = &g_100;
        int32_t *l_1003 = (void*)0;
        int32_t *l_1004 = &g_100;
        int32_t *l_1005 = (void*)0;
        int32_t *l_1006 = (void*)0;
        int32_t *l_1007 = &g_100;
        int32_t *l_1008[2][2][9] = {{{&g_181,&g_181,&g_181,&g_181,(void*)0,&g_181,&g_181,&g_181,&g_181},{&g_181,(void*)0,&g_181,&g_181,&g_181,(void*)0,&g_181,&g_181,(void*)0}},{{&g_181,(void*)0,&g_181,(void*)0,&g_181,&g_181,&g_181,&g_181,(void*)0},{&g_181,&g_181,&g_181,&g_181,&g_181,&g_181,&g_181,&g_181,&g_181}}};
        uint8_t l_1056 = 0UL;
        int i, j, k;
        --l_1009;
        for (p_60 = (-26); (p_60 > (-6)); p_60 = safe_add_func_int64_t_s_s(p_60, 1))
        { /* block id: 464 */
            int8_t *l_1024 = &g_124;
            uint8_t *l_1029 = &g_107;
            (*l_1000) = (l_1032 = (safe_mul_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s((safe_mod_func_int64_t_s_s((0x12L ^ ((l_1025 = l_1024) == l_1024)), ((safe_mod_func_int8_t_s_s((((1L != ((!l_1009) <= p_60)) , ((*l_1024) = p_60)) & (++(*l_1029))), (((g_100 , (7L & p_60)) & p_60) | 0x2185L))) && p_60))), p_60)), (*l_1001))), 1L)));
        }
        for (g_358 = 0; (g_358 <= 6); g_358 = safe_add_func_int64_t_s_s(g_358, 2))
        { /* block id: 473 */
            uint8_t l_1038 = 0x80L;
            int32_t **l_1050[10] = {&l_1002,&l_1002,&l_1002,&l_1002,&l_1002,&l_1002,&l_1002,&l_1002,&l_1002,&l_1002};
            const float l_1052 = (-0x8.3p+1);
            int i;
            for (g_761 = 0; (g_761 <= 1); g_761 += 1)
            { /* block id: 476 */
                int64_t l_1036[5][4][6] = {{{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL},{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL},{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL},{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL}},{{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL},{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL},{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL},{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL}},{{0x8FF8D558973030AELL,0x6E36D399545DA170LL,0x6E36D399545DA170LL,0x8FF8D558973030AELL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L}},{{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L}},{{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L},{0x6E36D399545DA170LL,0L,0L,0x6E36D399545DA170LL,0L,0L}}};
                int32_t l_1037 = 0x735CEB33L;
                int16_t *l_1054 = &g_762[2];
                int16_t **l_1053[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int i, j, k;
                for (g_107 = 0; (g_107 <= 1); g_107 += 1)
                { /* block id: 479 */
                    for (l_1032 = 0; (l_1032 <= 4); l_1032 += 1)
                    { /* block id: 482 */
                        uint32_t *l_1041 = &g_51[1];
                        int64_t *l_1051[8][3][4] = {{{(void*)0,&l_1036[2][3][0],&l_1036[3][0][1],&l_1036[3][2][5]},{&g_219,&l_1036[3][2][5],&g_63,&g_219},{&g_63,&g_219,&g_63,(void*)0}},{{&g_219,&l_1036[3][2][5],&g_122,&g_63},{&g_219,&g_122,(void*)0,&l_1036[3][2][5]},{(void*)0,&g_63,(void*)0,&g_122}},{{&g_219,&l_1036[3][2][5],&g_122,&l_1036[3][0][1]},{&g_219,&l_1036[0][0][4],&g_63,&g_63},{&g_63,&g_63,&g_63,&g_63}},{{&g_219,&l_1036[3][3][3],&l_1036[3][0][1],&g_122},{(void*)0,(void*)0,&g_122,(void*)0},{&g_219,&l_1036[3][2][5],&l_1036[3][2][5],(void*)0}},{{&l_1036[2][3][0],(void*)0,&g_63,&g_122},{&g_63,&l_1036[3][3][3],(void*)0,&g_63},{&l_1036[3][2][5],&g_63,&g_219,&g_63}},{{(void*)0,&l_1036[0][0][4],&l_1036[3][2][5],&l_1036[3][0][1]},{&g_219,&l_1036[3][2][5],&l_1036[3][3][3],&g_122},{(void*)0,&g_63,&g_63,&l_1036[3][2][5]}},{{(void*)0,&g_122,&l_1036[3][3][3],&g_63},{&g_219,&l_1036[3][2][5],&l_1036[3][2][5],(void*)0},{(void*)0,&g_219,&g_219,&g_219}},{{&l_1036[3][2][5],&l_1036[3][2][5],(void*)0,&l_1036[3][2][5]},{&g_63,&l_1036[2][3][0],&g_63,&l_1036[3][3][3]},{&l_1036[2][3][0],(void*)0,&l_1036[3][2][5],&g_63}}};
                        int i, j, k;
                        l_1008[g_761][g_107][(g_107 + 5)] = l_1008[g_107][g_107][(g_107 + 4)];
                        if (g_399[l_1032])
                            continue;
                        l_1038++;
                        g_1055 ^= ((*l_1000) = (1L | ((((*l_1041) = 0UL) < ((safe_lshift_func_uint16_t_u_s((g_399[l_1032] && (safe_div_func_uint64_t_u_u((0L && (((((g_219 ^= (((safe_rshift_func_int16_t_s_s(((void*)0 != l_1050[1]), 8)) <= ((*g_257) = 0xDEF7611DL)) || (*g_246))) || (g_122 , p_60)) , p_60) , l_1053[4]) != (**g_372))), 0x9B40E7049B621931LL))), l_1036[0][2][2])) , 0x1FF41526L)) == (**g_465))));
                    }
                }
            }
        }
        l_1056 &= (*l_1000);
    }
    g_100 ^= ((*g_257) = (((safe_rshift_func_uint8_t_u_s(((l_1059[3] == (l_1032 , l_1059[2])) , (safe_sub_func_uint16_t_u_u((l_1009 != l_1009), (safe_lshift_func_uint8_t_u_u((l_1032 , (**g_518)), (l_1032 | (((safe_div_func_int64_t_s_s((((*g_62) = (safe_add_func_uint64_t_u_u((safe_mul_func_int8_t_s_s(l_1032, l_1070)), p_60))) > l_1032), l_1032)) <= g_351[7][1][0]) == l_1032))))))), 6)) != p_60) , l_1071));
    return l_1072;
}


/* ------------------------------------------ */
/* 
 * reads : g_22 g_298 g_156 g_373 g_374 g_466 g_124 g_257 g_181 g_465 g_127 g_399 g_68 g_3
 * writes: g_3 g_633 g_399
 */
static int64_t * func_69(uint8_t  p_70, uint32_t  p_71, int32_t  p_72, uint64_t  p_73)
{ /* block id: 17 */
    int32_t *l_82 = (void*)0;
    int64_t *l_83 = &g_63;
    int16_t l_95[9][3][9] = {{{0xF270L,(-1L),0L,0x80ABL,1L,0x094BL,0x2910L,0L,0L},{7L,0x5FFFL,(-5L),0xAF0CL,7L,0L,(-1L),0x1855L,0L},{2L,0x2915L,0L,0x76AFL,0x76AFL,0L,0xBD28L,0xB8AFL,0x38BBL}},{{(-1L),(-4L),0xA170L,(-5L),0xA339L,0L,0xEDF5L,(-5L),0xCA80L},{0x6125L,0L,1L,7L,0L,0x094BL,1L,(-1L),0x38BBL},{0xCA80L,0xA170L,(-1L),0L,0xAF0CL,0x00F6L,0x00F6L,0xAF0CL,0L}},{{0xF270L,0L,0xF270L,0x38BBL,(-1L),1L,0x094BL,0L,7L},{0x00F6L,0L,0x46A6L,0xCA80L,(-5L),0xEDF5L,0L,0xA339L,(-5L)},{(-1L),0L,(-1L),0x38BBL,0xB8AFL,0xBD28L,0L,0x76AFL,0x76AFL}},{{(-4L),7L,0x8818L,0L,0x8818L,7L,(-4L),0xABD1L,0x00F6L},{0L,0x38BBL,1L,7L,0x80ABL,(-1L),0x2915L,(-1L),0L},{(-5L),0x5FFFL,7L,(-5L),(-4L),0xA339L,0L,0xABD1L,0x46A6L}},{{0L,0xB8AFL,0L,0x76AFL,0xCCD2L,0x2910L,0xCCD2L,0x76AFL,0L},{0L,0L,(-5L),0x00F6L,0L,0x9FB3L,7L,0xA339L,0xD4DCL},{0x87D4L,1L,0x505AL,0L,1L,0xB8AFL,0x76AFL,0L,0x80ABL}},{{0xA170L,0x8818L,(-5L),0x46A6L,(-1L),0L,2L,0xAF0CL,2L},{0L,0xD6FFL,0L,0L,0xD6FFL,0L,0xF270L,(-1L),0L},{0xEDF5L,2L,7L,0xD4DCL,0xCA80L,(-1L),(-5L),(-5L),(-1L)}},{{0x2910L,0x094BL,1L,0x80ABL,0L,(-1L),0xF270L,0xB8AFL,0x87D4L},{0xAE56L,(-5L),0x8818L,2L,(-1L),(-1L),2L,0x8818L,(-5L)},{1L,0x2910L,(-1L),0L,0L,0x38BBL,0x76AFL,0x80ABL,0xCCD2L}},{{(-5L),0L,0x46A6L,(-1L),0L,(-3L),7L,(-4L),0L},{0x2915L,0x2910L,0xF270L,0x87D4L,7L,(-1L),0xCCD2L,0xCCD2L,(-1L)},{0L,(-5L),(-1L),(-5L),0L,0xD4DCL,0x5FFFL,(-3L),(-4L)}},{{0L,0x2915L,0L,1L,0xD6FFL,0L,0xBD28L,(-1L),0x80ABL},{0L,(-1L),0x46A6L,0L,(-5L),0xD4DCL,(-5L),0x1855L,0x8818L},{0xB8AFL,0L,0x76AFL,0xCCD2L,0x2910L,0xCCD2L,0x76AFL,0L,0xB8AFL}}};
    uint8_t l_681 = 1UL;
    int8_t *l_703 = &g_399[3];
    uint8_t **l_748 = (void*)0;
    uint16_t l_765 = 0x9C57L;
    int16_t *l_772 = &g_762[4];
    int16_t ** const l_771 = &l_772;
    int32_t l_838 = 0xF251CC7FL;
    int32_t l_839 = 2L;
    int32_t l_843 = (-6L);
    int32_t l_845 = 0x6E918F40L;
    int32_t l_846 = 0xEABD62C7L;
    float ***l_957 = &g_297[4][7];
    uint8_t l_974 = 255UL;
    float *l_975 = &g_633;
    const int16_t ****l_984 = &g_788;
    uint16_t *l_985 = &l_765;
    int16_t * const **l_989 = (void*)0;
    int16_t * const ***l_988[2];
    int8_t ** const *l_992 = &g_465;
    int8_t ** const **l_991 = &l_992;
    int8_t ** const ** const *l_990 = &l_991;
    uint8_t *l_995 = &l_681;
    float l_996 = 0x1.A842DAp+77;
    int32_t *l_997 = &l_839;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_988[i] = &l_989;
    for (p_72 = 7; (p_72 < 21); ++p_72)
    { /* block id: 20 */
        uint32_t l_84 = 0UL;
        const int32_t *l_98 = &g_68[3][3];
        float l_477 = 0xD.FA5970p+18;
        int16_t *l_478 = &g_127[2];
        int16_t *l_479 = (void*)0;
        int16_t *l_480 = (void*)0;
        int16_t *l_481 = &g_254;
        int16_t *l_484[9] = {&g_485,&g_485,&g_20,&g_485,&g_485,&g_20,&g_485,&g_485,&g_20};
        int32_t l_486 = 4L;
        int32_t l_487[10][3] = {{0x17F2254EL,0xA90DA042L,0L},{0x116B153CL,0x17F2254EL,0x17F2254EL},{0x17F2254EL,0x453FDD6BL,2L},{0L,1L,2L},{3L,2L,0x17F2254EL},{0xEB0BBA03L,4L,0L},{2L,2L,4L},{0xA90DA042L,1L,0xD4113B70L},{0xA90DA042L,0x453FDD6BL,0xEB0BBA03L},{2L,0x17F2254EL,1L}};
        int8_t *l_637 = &g_399[2];
        uint8_t l_639 = 4UL;
        uint64_t *l_686 = &g_282;
        int32_t l_688 = 7L;
        const float l_693 = 0xD.5FC748p-64;
        int16_t **l_721[10][10] = {{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0},{&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0,&l_484[4],&l_484[0],&l_484[4],(void*)0,(void*)0}};
        int16_t ***l_720 = &l_721[9][7];
        int16_t *** const *l_719 = &l_720;
        float ***l_750 = (void*)0;
        uint32_t l_759 = 0x73199422L;
        int8_t *** const *l_783 = &g_464;
        int8_t *** const **l_782 = &l_783;
        uint16_t *l_869[6][3][9] = {{{&g_351[0][3][4],&g_358,&g_351[7][1][0],&g_358,&g_351[0][3][4],&g_226,&g_226,&g_358,&l_765},{(void*)0,&g_358,&l_765,&g_351[7][1][0],&g_226,&g_358,&g_351[0][1][3],&g_761,(void*)0},{&g_351[7][1][0],&g_351[1][1][4],&g_351[2][0][5],&g_358,&g_358,&g_358,&g_358,&g_226,&g_351[2][0][5]}},{{&g_351[0][1][3],(void*)0,&g_351[7][1][0],&g_358,&g_226,&g_351[1][1][4],&g_226,(void*)0,&g_761},{&g_351[0][3][4],&g_761,&g_358,&g_358,&l_765,&g_358,&g_358,&g_358,&g_358},{&g_358,(void*)0,&g_761,&g_761,(void*)0,&g_358,&l_765,&g_351[2][0][5],&g_358}},{{&l_765,&g_351[2][0][5],(void*)0,&g_351[7][1][0],&g_351[0][1][3],&g_226,&g_358,&g_761,&g_761},{&g_351[7][1][0],(void*)0,&g_351[2][0][5],&l_765,&l_765,&g_351[7][1][0],&l_765,&l_765,&g_351[2][0][5]},{&g_761,&g_761,(void*)0,&g_358,&l_765,&g_351[2][0][5],&g_358,&g_761,&g_358}},{{&g_358,&g_358,&g_761,&g_351[0][3][4],&g_351[0][1][3],&l_765,&g_226,&l_765,&l_765},{&g_358,&g_351[7][1][0],(void*)0,&g_351[0][1][3],(void*)0,&g_351[7][1][0],&g_358,&g_226,&g_351[1][1][4]},{&g_358,&g_226,&g_351[2][0][5],&g_351[7][1][0],&l_765,(void*)0,&l_765,&g_351[7][1][0],&g_358}},{{&g_358,&l_765,(void*)0,&g_226,&g_226,&l_765,&l_765,&g_226,&g_226},{&g_761,&g_358,&g_761,&g_351[1][1][4],&g_358,&l_765,&g_351[0][1][3],&l_765,(void*)0},{&g_351[7][1][0],&g_226,&g_358,&g_351[2][0][5],&g_351[7][1][0],(void*)0,&g_358,&g_761,&l_765}},{{&l_765,&g_351[0][3][4],&g_351[7][1][0],&g_351[1][1][4],&g_351[1][1][4],&g_351[7][1][0],&g_351[0][3][4],&l_765,&g_761},{&g_358,&g_351[0][3][4],&g_226,&g_226,&g_358,&l_765,&g_358,&g_761,&g_226},{&g_351[0][3][4],&g_226,&l_765,&g_351[7][1][0],&g_761,&g_351[2][0][5],&g_761,&g_351[2][0][5],&g_761}}};
        int32_t *l_903 = &g_123[1][1];
        uint64_t ***l_954 = &g_737;
        int8_t l_962 = (-1L);
        int i, j, k;
        for (p_71 = 0; (p_71 <= 3); p_71 = safe_add_func_uint32_t_u_u(p_71, 4))
        { /* block id: 23 */
            l_82 = (void*)0;
            return l_83;
        }
        l_84 |= g_22;
    }
    (*l_975) = (p_72 == (((-(safe_div_func_float_f_f((safe_div_func_float_f_f(((safe_mul_func_float_f_f(p_73, (l_846 != ((p_73 < ((((((*g_298) = (p_70 >= p_73)) > ((p_71 <= (&g_464 == (((l_843 , (-8L)) , g_156) , (void*)0))) , 0x1.8p-1)) <= p_71) <= p_72) >= p_72)) == 0x0.Ap-1)))) != p_70), l_95[4][2][4])), p_72))) > p_72) == l_974));
    (*l_997) |= (safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((safe_sub_func_int8_t_s_s(((*l_703) &= ((((((0xB949B5C41FEDD787LL & ((l_984 == ((--(*l_985)) , l_988[0])) , ((((void*)0 != l_990) , ((void*)0 != (*g_373))) ^ ((((((*l_995) ^= (p_70 | (((((safe_div_func_uint8_t_u_u((l_974 & l_95[6][1][5]), p_72)) , 0x9BBECEF5L) , &g_374[0]) != &l_957) <= 0x09442BE171253031LL))) == (*g_466)) & p_73) != p_73) | (*g_257))))) <= 0xB537DAFBL) , p_72) , (**g_465)) == g_127[2]) && 5L)), g_68[4][1])), p_73)), p_71)), p_72));
    return l_83;
}


/* ------------------------------------------ */
/* 
 * reads : g_122 g_181 g_497 g_254 g_518 g_519 g_520 g_100 g_466 g_127 g_288 g_226 g_530 g_257 g_436 g_68 g_123 g_543 g_548 g_22 g_465 g_124 g_246 g_63 g_369 g_435 g_256 g_431 g_125 g_117 g_298 g_609 g_51 g_282 g_62
 * writes: g_122 g_485 g_181 g_226 g_124 g_257 g_543 g_127 g_68 g_123 g_282 g_117 g_431 g_254 g_51 g_125 g_3 g_107
 */
static int64_t  func_90(uint64_t  p_91, int16_t  p_92, uint64_t  p_93, int32_t * const  p_94)
{ /* block id: 231 */
    float l_490[5];
    int32_t *l_494 = &g_181;
    int32_t l_500 = 0xC4EDAE71L;
    int32_t l_501 = (-10L);
    int32_t l_542 = (-1L);
    uint32_t l_562 = 0x6D612AFBL;
    uint8_t l_571 = 255UL;
    int32_t *l_579 = &g_123[2][0];
    int64_t l_616 = (-1L);
    int32_t l_617 = 0x025CFCC5L;
    uint64_t l_618 = 18446744073709551615UL;
    int32_t l_634 = (-10L);
    uint64_t *l_635 = (void*)0;
    uint64_t *l_636 = &g_431;
    int i;
    for (i = 0; i < 5; i++)
        l_490[i] = 0x1.6p-1;
    for (g_122 = 12; (g_122 <= (-19)); g_122 = safe_sub_func_uint8_t_u_u(g_122, 1))
    { /* block id: 234 */
        const int64_t l_491 = 0x7FB32BE0F94A6298LL;
        int32_t *l_496[6][2][1];
        float **l_498 = (void*)0;
        uint8_t *l_522 = &g_107;
        uint8_t **l_521 = &l_522;
        uint64_t l_539 = 0x0E5571A8812CF314LL;
        float l_559 = (-0x9.0p-1);
        int32_t **l_608 = &l_579;
        int i, j, k;
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 1; k++)
                    l_496[i][j][k] = &g_181;
            }
        }
        if (l_491)
            break;
        for (g_485 = 0; (g_485 != 10); g_485 = safe_add_func_uint16_t_u_u(g_485, 5))
        { /* block id: 238 */
            int32_t **l_495[2];
            uint8_t **l_523 = &l_522;
            int16_t l_529 = (-8L);
            uint32_t l_589 = 4294967286UL;
            int i;
            for (i = 0; i < 2; i++)
                l_495[i] = &g_257;
            l_496[4][1][0] = (l_494 = l_494);
            (*l_494) ^= (-7L);
            if ((*p_94))
            { /* block id: 242 */
                int32_t l_499[6][7] = {{0x48A51667L,(-4L),0x4C16C75EL,0xC56878A8L,(-1L),(-1L),0xC56878A8L},{0x4C16C75EL,5L,0x4C16C75EL,1L,1L,(-1L),1L},{0x48A51667L,5L,0x3D9831F8L,0xC56878A8L,1L,2L,0xC56878A8L},{0x48A51667L,(-4L),0x4C16C75EL,0xC56878A8L,(-1L),(-1L),0xC56878A8L},{0x4C16C75EL,5L,0x4C16C75EL,1L,1L,(-1L),1L},{0x48A51667L,5L,0x3D9831F8L,0xC56878A8L,1L,2L,0xC56878A8L}};
                uint8_t **l_524 = (void*)0;
                int32_t *l_531[8][5][3] = {{{&g_100,&l_499[2][0],&g_181},{(void*)0,&g_181,&l_500},{&l_501,&g_181,&l_501},{&l_500,&l_499[1][6],&l_499[1][3]},{&g_181,&l_501,&g_181}},{{&g_100,&g_100,&l_499[2][0]},{(void*)0,&g_181,&g_181},{&l_500,&l_501,&l_499[1][3]},{&g_100,&l_500,&l_501},{(void*)0,&l_501,(void*)0}},{{&l_501,&l_500,&g_100},{&l_499[1][3],&l_501,&l_500},{&g_181,&g_181,(void*)0},{&l_499[2][0],&g_100,&g_100},{&g_181,&l_501,&g_181}},{{&l_499[1][3],&l_499[1][6],&l_500},{&l_501,&g_181,&g_181},{(void*)0,&g_100,&l_500},{&g_100,(void*)0,&g_181},{&l_500,&l_499[5][5],&g_100}},{{(void*)0,&l_501,(void*)0},{&g_100,&l_499[5][5],&l_500},{&g_181,(void*)0,&g_100},{&l_500,&g_100,(void*)0},{&g_181,&g_181,&l_501}},{{&l_500,&l_499[1][6],&l_499[1][3]},{&g_181,&l_501,&g_181},{&g_100,&g_100,&l_499[2][0]},{(void*)0,&g_181,&g_181},{&l_500,&l_501,&l_499[1][3]}},{{&g_100,&l_500,&l_501},{(void*)0,&l_501,(void*)0},{&l_501,&l_500,&g_100},{&l_499[1][3],&l_501,&l_500},{&g_181,&g_181,(void*)0}},{{&l_499[2][0],&g_100,&g_100},{&g_181,&l_501,&g_181},{&l_499[1][3],&l_499[1][6],&l_500},{&l_501,&g_181,&g_181},{(void*)0,&g_100,&l_500}}};
                int i, j, k;
                if ((g_497 == l_498))
                { /* block id: 243 */
                    uint64_t *l_515 = (void*)0;
                    uint64_t **l_514 = &l_515;
                    const int32_t l_528 = (-1L);
                    int32_t l_532 = 0x3BEA1271L;
                    int32_t l_533 = 0x6F67719EL;
                    uint32_t l_560 = 0x4826449BL;
                    for (g_226 = 0; (g_226 <= 5); g_226 += 1)
                    { /* block id: 246 */
                        int8_t l_502 = 0L;
                        uint32_t l_503[8][5] = {{0x1DB7756AL,0x1DB7756AL,0x5EEDD2B8L,0x21BC7396L,0xE3966BF4L},{0x1D2784A4L,0x3B0071A6L,0x3B0071A6L,0x1D2784A4L,0x1DB7756AL},{0x1D2784A4L,0x21BC7396L,0UL,0UL,0x21BC7396L},{0x1DB7756AL,0x3B0071A6L,0UL,0x5EEDD2B8L,0x5EEDD2B8L},{0x3B0071A6L,0x1DB7756AL,0x3B0071A6L,0UL,0x5EEDD2B8L},{0x21BC7396L,0x1D2784A4L,0x5EEDD2B8L,0x1D2784A4L,0x21BC7396L},{0x3B0071A6L,0x1D2784A4L,0x1DB7756AL,0x21BC7396L,0x1DB7756AL},{0x1DB7756AL,0x1DB7756AL,0x5EEDD2B8L,0x21BC7396L,0xE3966BF4L}};
                        int i, j;
                        (*l_494) ^= 0x316ED792L;
                        ++l_503[1][1];
                    }
                    if (((safe_sub_func_uint32_t_u_u(g_122, ((safe_add_func_uint16_t_u_u((((safe_mod_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u(((((*l_514) = &p_91) != &g_282) < ((safe_lshift_func_int8_t_s_u(((*g_466) = ((((g_254 & (g_518 != l_521)) , l_523) != l_524) | (((!((safe_rshift_func_uint8_t_u_u((**g_518), l_528)) , (*p_94))) < g_100) || l_529))), g_127[2])) > (*g_288))), p_92)), l_499[1][6])) > p_92) , g_226), 65534UL)) != p_91))) < 0xC17438A5L))
                    { /* block id: 252 */
                        uint32_t l_534 = 0xA103C6BBL;
                        (*g_530) = p_94;
                        l_531[6][2][1] = (*g_530);
                        --l_534;
                        if ((*l_494))
                            continue;
                    }
                    else
                    { /* block id: 257 */
                        float l_537 = 0x3.1p+1;
                        int32_t l_538[7][3][10] = {{{8L,(-10L),1L,0x790F9371L,0x74DED6C0L,(-1L),0x31F3A545L,0xF5B1743CL,8L,0xEA8A1E04L},{0x790F9371L,0xC5F8097BL,0x51829DB3L,(-1L),(-7L),0xA5391E9EL,0x31F3A545L,0x31F3A545L,0xA5391E9EL,(-7L)},{1L,(-10L),(-10L),1L,0xEA8A1E04L,8L,0xF5B1743CL,0x31F3A545L,(-1L),0x74DED6C0L}},{{0xA5391E9EL,1L,0x51829DB3L,1L,9L,0x6DFA885AL,(-10L),0xF5B1743CL,1L,0x9431AA63L},{1L,1L,8L,0xEDF92732L,0L,1L,8L,(-1L),(-1L),0x9431AA63L},{0L,8L,0x99E850ABL,1L,0x06411087L,0xEDF92732L,0x3E25D42CL,8L,0x0AFE179CL,(-2L)}},{{0L,0xA5391E9EL,0x790F9371L,0xDA4E3D26L,0x1801E379L,1L,0xA5391E9EL,0x3E25D42CL,(-8L),0x6F03CEE9L},{1L,0xA5391E9EL,0x3E25D42CL,(-8L),0x6F03CEE9L,(-8L),0x3E25D42CL,0xA5391E9EL,1L,0x1801E379L},{(-1L),8L,0x3E25D42CL,0x7DE33565L,(-2L),0x0AFE179CL,8L,0x3E25D42CL,0xEDF92732L,0x06411087L}},{{0xEDF92732L,1L,0x790F9371L,0x7DE33565L,0x9431AA63L,(-1L),(-1L),8L,1L,0L},{0xDA4E3D26L,0x3E25D42CL,0x99E850ABL,(-8L),0x9431AA63L,1L,1L,(-1L),(-8L),0x06411087L},{0xC3FC8887L,(-1L),8L,0xDA4E3D26L,(-2L),1L,0x790F9371L,1L,0x0AFE179CL,0x1801E379L}},{{0xDA4E3D26L,0xF3510A28L,0x6DFA885AL,1L,0x6F03CEE9L,(-1L),0x790F9371L,0x790F9371L,(-1L),0x6F03CEE9L},{0xEDF92732L,(-1L),(-1L),0xEDF92732L,0x1801E379L,0x0AFE179CL,1L,0x790F9371L,1L,(-2L)},{(-1L),0x3E25D42CL,0x6DFA885AL,0L,0x06411087L,(-8L),(-1L),1L,1L,0x9431AA63L}},{{1L,1L,8L,0xEDF92732L,0L,1L,8L,(-1L),(-1L),0x9431AA63L},{0L,8L,0x99E850ABL,1L,0x06411087L,0xEDF92732L,0x3E25D42CL,8L,0x0AFE179CL,(-2L)},{0L,0xA5391E9EL,0x790F9371L,0xDA4E3D26L,0x1801E379L,1L,0xA5391E9EL,0x3E25D42CL,(-8L),0x6F03CEE9L}},{{1L,0xA5391E9EL,0x3E25D42CL,(-8L),0x6F03CEE9L,(-8L),0x3E25D42CL,0xA5391E9EL,1L,0x1801E379L},{(-1L),8L,0x3E25D42CL,0x7DE33565L,(-2L),0x0AFE179CL,8L,0x3E25D42CL,0xEDF92732L,0x06411087L},{0xEDF92732L,1L,0x790F9371L,0x7DE33565L,0x9431AA63L,(-1L),(-1L),8L,1L,0L}}};
                        int16_t *l_561 = &g_127[2];
                        int i, j, k;
                        if ((**g_436))
                            break;
                        l_539++;
                        g_543--;
                        l_500 = (l_532 = ((*l_494) = (((((safe_add_func_int32_t_s_s((g_548 < p_91), ((((((g_22 , ((safe_add_func_float_f_f(((safe_mul_func_int8_t_s_s(p_92, ((**g_465) &= (safe_sub_func_uint16_t_u_u(65533UL, ((*l_561) = ((safe_add_func_int8_t_s_s(((((safe_mod_func_uint16_t_u_u((l_560 && 0xD8A5L), ((6UL < (*l_494)) , 65529UL))) == p_91) != 0x8AL) , p_91), 0x78L)) && p_92))))))) , l_560), p_92)) , p_92)) && 0L) == (*g_246)) & 0xBBBBL) , 0x7A452E01L) , p_91))) != (*l_494)) < l_562) , &l_528) == p_94)));
                    }
                    if ((*p_94))
                        continue;
                }
                else
                { /* block id: 268 */
                    uint32_t l_569[9][8] = {{0x8F882C8DL,0xC58AD98EL,0xC58AD98EL,0x8F882C8DL,0UL,0xC978B894L,0UL,0x8F882C8DL},{0xC58AD98EL,0UL,0xC58AD98EL,1UL,0xA86F650FL,0xA86F650FL,1UL,0xC58AD98EL},{0UL,0UL,0xA86F650FL,1UL,0x8F882C8DL,1UL,0UL,0x15BC5E60L},{0x15BC5E60L,0xC978B894L,0xA86F650FL,0UL,0UL,0xA86F650FL,0xC978B894L,0x15BC5E60L},{0xC978B894L,0xC58AD98EL,0x15BC5E60L,1UL,0x15BC5E60L,0xC58AD98EL,0xC978B894L,0xC978B894L},{0xC58AD98EL,1UL,0xA86F650FL,0xA86F650FL,1UL,0xC58AD98EL,0UL,0xC58AD98EL},{1UL,0xC58AD98EL,0UL,0xC58AD98EL,1UL,0xA86F650FL,0xA86F650FL,1UL},{0xC58AD98EL,0xC978B894L,0xC978B894L,0xC58AD98EL,0x15BC5E60L,1UL,0x15BC5E60L,0xC58AD98EL},{0xC978B894L,0x15BC5E60L,0xC978B894L,0xA86F650FL,0UL,0UL,0xA86F650FL,0xC978B894L}};
                    int32_t *l_580[2][3][5] = {{{&g_68[5][5],&g_68[4][0],&g_68[5][5],&g_68[5][5],&g_68[4][0]},{&g_68[4][0],&g_68[5][5],&g_68[5][5],&g_68[4][0],&g_68[5][5]},{&g_68[4][0],&g_68[4][0],&g_123[0][0],&g_68[4][0],&g_68[4][0]}},{{&g_68[5][5],&g_68[4][0],&g_68[5][5],&g_68[5][5],&g_68[4][0]},{&g_68[4][0],&g_68[5][5],&g_68[5][5],&g_68[4][0],&g_68[5][5]},{&g_68[4][0],&g_68[4][0],&g_123[0][0],&g_68[4][0],&g_68[4][0]}}};
                    int32_t l_587[5][10][5] = {{{0L,0xF8FBF686L,2L,(-1L),0x0C259331L},{0xBE50E91AL,(-1L),(-1L),1L,0x0DE4CAEAL},{0xA3CB1FA3L,7L,0xF90B6658L,0xE2B22685L,0L},{0x7F39D59DL,0x66B4AD9BL,6L,0L,6L},{(-1L),(-1L),(-1L),0xD119B0ACL,0xE2B22685L},{(-1L),0x7F6DDA1FL,1L,0xF619D96BL,0xDD4BC153L},{(-1L),0xE2B22685L,0x38248036L,0x2A5C62BFL,0xAD8EE91EL},{0x969838FFL,0x7F6DDA1FL,0L,0xD55D8CE4L,(-1L)},{0x0C259331L,(-1L),0xD540DF4EL,0x27EC538CL,2L},{(-1L),0x66B4AD9BL,(-1L),0xC55D6945L,5L}},{{(-1L),7L,(-1L),5L,1L},{0L,(-1L),0xDD4BC153L,0xEBAFB630L,0L},{1L,0xF8FBF686L,0L,6L,0xA3CB1FA3L},{(-1L),1L,0xDD4BC153L,0x6B27CA39L,(-1L)},{0xF90B6658L,0xD119B0ACL,(-1L),1L,9L},{0xF05D0C50L,0x8A54655FL,(-1L),0x7F6DDA1FL,0xC5490F31L},{0x38248036L,0xD540DF4EL,0xD540DF4EL,0x38248036L,0xF8FBF686L},{0x867992C3L,(-6L),0L,0L,0L},{0xD119B0ACL,0x2A683867L,0x38248036L,0xA3CB1FA3L,1L},{9L,(-7L),1L,0L,0x7F39D59DL}},{{7L,9L,(-1L),0x38248036L,0L},{0x3857577BL,(-1L),0L,0x46F4F246L,0xDD4BC153L},{2L,0L,2L,0L,2L},{0x43B2AB12L,0xE12CF42EL,0xD9867D08L,(-6L),(-7L)},{0xAD8EE91EL,5L,0L,1L,0L},{(-1L),0L,0L,0xE12CF42EL,(-7L)},{(-1L),1L,0x2A5C62BFL,0xF8FBF686L,2L},{(-7L),1L,6L,0x0365A202L,0xDD4BC153L},{1L,0x27EC538CL,9L,9L,0x27EC538CL},{0xF4B23933L,(-6L),0x6F4A1A66L,0L,9L}},{{0xE2B22685L,0xF90B6658L,7L,0xA3CB1FA3L,0L},{0xD9867D08L,0xBB1EF1FFL,0xC5490F31L,0x6B27CA39L,0x43B2AB12L},{0xE2B22685L,0xF8FBF686L,0x27EC538CL,1L,7L},{0xF4B23933L,(-9L),1L,0xC55D6945L,(-1L)},{1L,0x2A683867L,0x0C259331L,2L,0x0C259331L},{(-7L),0L,(-1L),(-6L),5L},{(-1L),0x2A5C62BFL,0x2A683867L,0xAD8EE91EL,1L},{(-1L),0xC55D6945L,(-1L),(-8L),0x7F39D59DL},{0xAD8EE91EL,0x2A5C62BFL,0x38248036L,0xE2B22685L,(-1L)},{0x43B2AB12L,0L,0x0903CBFBL,0xEBAFB630L,0xC5490F31L}},{{2L,0x2A683867L,2L,0L,0L},{0x969838FFL,(-9L),0x969838FFL,0x5836EA15L,0xFF27723CL},{0x38248036L,0xF8FBF686L,1L,0x0C259331L,6L},{(-1L),0xBB1EF1FFL,(-1L),(-1L),0xF4B23933L},{1L,0xF90B6658L,1L,6L,2L},{1L,(-6L),0x969838FFL,0x8A54655FL,0L},{0xD540DF4EL,0x27EC538CL,2L,0x2A5C62BFL,0xD119B0ACL},{(-1L),1L,0x0903CBFBL,(-1L),0L},{2L,1L,0x38248036L,(-1L),0xCE252D31L},{(-1L),0L,(-1L),0x283E6DF3L,(-1L)}}};
                    int i, j, k;
                    (**g_369) &= 0x33AEB0ECL;
                    for (g_282 = 0; (g_282 != 40); g_282 = safe_add_func_int8_t_s_s(g_282, 8))
                    { /* block id: 272 */
                        (*g_256) = (*g_435);
                    }
                    for (g_117 = (-23); (g_117 > 12); g_117 = safe_add_func_uint64_t_u_u(g_117, 2))
                    { /* block id: 277 */
                        if ((*p_94))
                            break;
                    }
                    for (g_226 = (-5); (g_226 != 6); ++g_226)
                    { /* block id: 282 */
                        int32_t *l_570[8] = {&g_123[3][2],&g_123[3][2],&g_123[3][2],&g_123[3][2],&g_123[3][2],&g_123[3][2],&g_123[3][2],&g_123[3][2]};
                        int32_t **l_578[9][10] = {{(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0},{&l_570[4],&l_570[4],&l_570[4],&l_570[4],&l_570[4],&l_570[4],&l_570[4],&l_570[4],&l_570[4],&l_570[4]},{&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4]},{(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0},{(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0},{(void*)0,&l_570[4],&l_570[4],(void*)0,&l_570[4],&l_570[4],(void*)0,&l_570[4],&l_570[4],(void*)0},{&l_570[4],(void*)0,&l_570[4],&l_570[4],(void*)0,&l_570[4],&l_570[4],(void*)0,&l_570[4],&l_570[4]},{(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0,(void*)0,&l_570[4],(void*)0},{(void*)0,&l_570[4],&l_570[4],(void*)0,&l_570[4],&l_570[4],(void*)0,&l_570[4],&l_570[4],(void*)0}};
                        int16_t *l_581 = &g_127[0];
                        uint64_t *l_582 = &g_431;
                        uint32_t *l_583 = &g_51[5];
                        uint32_t *l_584 = (void*)0;
                        uint32_t *l_585 = &g_125[1][0];
                        int32_t l_586 = 0x508FADB2L;
                        int32_t l_588[6][7][6] = {{{3L,6L,(-1L),0xFCB5C66CL,0xAA126F0AL,(-1L)},{(-10L),0x4842965AL,1L,(-1L),(-1L),0xA6FBE8A8L},{1L,(-1L),(-2L),3L,0x32AFA876L,6L},{(-1L),0xB66DDDD8L,0L,0x20121B0BL,0xECBFB99FL,1L},{1L,0xB744E8DEL,0x634B7104L,(-1L),0x1BF1F2BEL,0x0E9FD986L},{0x176A09FCL,0xFCB5C66CL,(-8L),0x7DE0C598L,0x0E9FD986L,7L},{7L,0x18F1B0F5L,0xD0373F30L,(-1L),0x4842965AL,(-1L)}},{{0xB9015A14L,(-1L),0xCCA552D5L,1L,0L,0x3CF86C1AL},{0x3CF86C1AL,0x7DE0C598L,0x952656E2L,0L,3L,0L},{0x44D14509L,0x6C904612L,0x41AD4F1FL,0xAA126F0AL,0x7DE0C598L,(-1L)},{0xFCB5C66CL,(-2L),0xB66DDDD8L,(-1L),1L,7L},{0xAA126F0AL,(-1L),0xFCB5C66CL,1L,0x952656E2L,0x18F1B0F5L},{0xECBFB99FL,0xF0D0CCD1L,0L,(-2L),0x9F639B9EL,0xB66DDDD8L},{0xA6FBE8A8L,1L,(-7L),(-2L),(-7L),1L}},{{(-1L),0x1B73440FL,(-1L),7L,4L,0x0A22F4F1L},{0x6C904612L,0x2896CA02L,0xECBFB99FL,0L,(-1L),9L},{2L,0x2896CA02L,0xFA965771L,7L,4L,0L},{0x08106D7BL,0x1B73440FL,(-9L),0x5154F31DL,(-7L),0x32AFA876L},{0xB66DDDD8L,1L,0x1418248DL,0x94D8F8F3L,0x9F639B9EL,0x947BA050L},{(-8L),0xF0D0CCD1L,0x1BF1F2BEL,0x445D952BL,0x952656E2L,(-1L)},{(-7L),(-1L),0L,0xA6FBE8A8L,1L,0x88C4BB8EL}},{{1L,(-2L),0x8C4BF6A3L,0xB744E8DEL,0x7DE0C598L,0x64BE390CL},{(-1L),0x6C904612L,7L,(-1L),3L,4L},{0x9D376D18L,0x7DE0C598L,(-1L),4L,5L,0x445D952BL},{(-1L),1L,0x20121B0BL,0xA6B406A1L,(-2L),4L},{7L,0x4798EECBL,0x176A09FCL,(-1L),(-5L),0L},{0x176A09FCL,(-1L),1L,0x64BE390CL,9L,0L},{0L,(-2L),(-8L),0xAA126F0AL,0x1418248DL,0xA6FBE8A8L}},{{0xFA965771L,4L,0x64BE390CL,0xCCA552D5L,0x8C4BF6A3L,0xA6B406A1L},{0xE01E0970L,0x32AFA876L,0xDFC049E6L,0x6C904612L,3L,0x5154F31DL},{0x952656E2L,(-2L),0x9D376D18L,(-1L),0x156BFFCFL,0x953FE260L},{0x88C4BB8EL,0x9F639B9EL,0x40804DAEL,0x5154F31DL,0x5154F31DL,0x40804DAEL},{(-3L),(-3L),0x947BA050L,(-1L),0L,0xD9ECEC2FL},{6L,0x41AD4F1FL,0xB66DDDD8L,0xBA8782FEL,(-7L),0x947BA050L},{(-9L),6L,0xB66DDDD8L,(-8L),(-3L),0xD9ECEC2FL}},{{0x9D376D18L,(-8L),0x947BA050L,7L,0x1F26FA47L,0x40804DAEL},{7L,0x1F26FA47L,0x40804DAEL,1L,0x1BF1F2BEL,0x953FE260L},{0xA2C40D9AL,0xD0373F30L,0x9D376D18L,(-1L),(-10L),0x5154F31DL},{(-10L),0x64BE390CL,0xDFC049E6L,1L,0x0E9FD986L,0xA6B406A1L},{0x645ADCE2L,0x7CFEF7E0L,0x64BE390CL,0xFA965771L,0x953FE260L,0xA6FBE8A8L},{0xB744E8DEL,0x1418248DL,(-8L),0xA2C40D9AL,(-1L),0L},{0x2896CA02L,0xEA1D85B1L,1L,0x7A2F0104L,0x947BA050L,0L}}};
                        int i, j, k;
                        l_569[1][4] |= (*g_257);
                        l_586 ^= ((*p_94) = ((l_571 = 1L) , (((*l_585) ^= ((*l_583) = ((g_254 |= (safe_rshift_func_uint16_t_u_s(((((void*)0 != p_94) , (p_92 > ((safe_add_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((((l_580[1][0][3] = (l_579 = &g_123[2][0])) == &g_123[3][2]) , ((*l_581) = p_93)), 2)) == (((**g_518) && (((*l_582) &= (&l_496[4][1][0] != &p_94)) ^ (-1L))) , (**g_436))), p_92)) || (*p_94)))) < 0x5B6891CEL), p_93))) > 0x2D04L))) ^ g_117)));
                        (*g_530) = l_580[1][0][3];
                        l_589--;
                    }
                }
                if ((*p_94))
                    break;
                l_500 &= (*p_94);
            }
            else
            { /* block id: 300 */
                uint64_t l_592 = 1UL;
                ++l_592;
                for (g_124 = 0; (g_124 < (-26)); --g_124)
                { /* block id: 304 */
                    (*g_298) = p_93;
                    return p_91;
                }
                for (l_592 = 6; (l_592 > 17); ++l_592)
                { /* block id: 310 */
                    int16_t l_603 = 4L;
                    int32_t l_615 = 0x702F769FL;
                    l_501 = ((0x720FC4D5F2B8EADALL >= ((((((((*p_94) = (safe_add_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u((l_603 | (safe_mul_func_uint16_t_u_u(0x47E1L, (safe_sub_func_int64_t_s_s((((**g_518) & (l_608 == g_609)) >= (safe_lshift_func_int8_t_s_u((safe_rshift_func_uint16_t_u_s(g_122, p_92)), (l_615 = ((**l_521) = (g_123[2][0] <= p_92)))))), g_125[1][0]))))), p_92)), p_93))) , l_603) , 0x38D9L) , p_93) >= p_91) , g_51[4]) | l_603)) ^ g_282);
                    return (*g_62);
                }
            }
        }
        --l_618;
    }
    l_494 = (((*g_62) > ((*l_636) &= (safe_lshift_func_int8_t_s_u(((safe_add_func_uint32_t_u_u(0xF98FB900L, ((1UL & ((safe_lshift_func_int8_t_s_s((((((safe_sub_func_uint32_t_u_u(g_543, (safe_sub_func_int64_t_s_s((((safe_add_func_uint32_t_u_u((&g_226 == (void*)0), 4294967295UL)) > p_93) , ((*l_494) < (*l_494))), (*l_494))))) != l_634) , (*l_494)) ^ 0UL) & (*l_494)), (*l_494))) && g_123[2][0])) && 65531UL))) || g_123[2][0]), p_93)))) , (void*)0);
    return (*g_246);
}


/* ------------------------------------------ */
/* 
 * reads : g_107 g_100
 * writes: g_107 g_100
 */
static int64_t  func_96(const int32_t * p_97)
{ /* block id: 28 */
    int32_t *l_99 = &g_100;
    int32_t *l_101 = &g_100;
    int32_t *l_102 = &g_100;
    int32_t *l_103 = &g_100;
    int32_t *l_104[2];
    int64_t l_106[6] = {0x33379BBDC21833D8LL,0x33379BBDC21833D8LL,0x33379BBDC21833D8LL,0x33379BBDC21833D8LL,0x33379BBDC21833D8LL,0x33379BBDC21833D8LL};
    int64_t l_128 = 0L;
    int64_t *l_245 = &g_122;
    int8_t *l_316 = &g_124;
    int8_t **l_315 = &l_316;
    uint32_t l_333 = 1UL;
    float l_428[3][10] = {{0x1.Ep-1,0x1.Fp-1,0x1.Ep-1,0x1.9p-1,0x7.5p+1,0x7.5p+1,0x1.9p-1,0x1.Ep-1,0x1.Fp-1,0x1.Ep-1},{0x1.Ep-1,0x5.DBEB43p+4,0x1.Fp-1,(-0x10.5p-1),0x1.Fp-1,0x5.DBEB43p+4,0x1.Ep-1,0x1.Ep-1,0x5.DBEB43p+4,0x1.Fp-1},{0x5.DBEB43p+4,0x1.Ep-1,0x1.Ep-1,0x5.DBEB43p+4,0x1.Fp-1,(-0x10.5p-1),0x1.Fp-1,0x5.DBEB43p+4,0x1.Ep-1,0x1.Ep-1}};
    int64_t l_462 = (-1L);
    int i, j;
    for (i = 0; i < 2; i++)
        l_104[i] = &g_100;
    --g_107;
    for (g_100 = 0; (g_100 <= 1); g_100 += 1)
    { /* block id: 32 */
        int32_t l_116 = 1L;
        uint8_t *l_150 = &g_15;
        uint8_t l_180 = 0x48L;
        float *l_216[5][5][5] = {{{(void*)0,&g_3[5],&g_3[5],&g_3[6],&g_3[5]},{&g_3[5],&g_3[5],&g_3[7],&g_3[5],&g_3[5]},{&g_3[7],&g_3[8],&g_3[6],&g_3[1],&g_3[6]},{&g_3[5],&g_3[8],&g_3[5],&g_3[5],&g_3[5]},{(void*)0,&g_3[5],&g_3[6],&g_3[5],&g_3[5]}},{{&g_3[5],&g_3[5],&g_3[7],&g_3[1],&g_3[5]},{&g_3[5],&g_3[6],&g_3[5],&g_3[5],&g_3[6]},{(void*)0,&g_3[5],&g_3[5],&g_3[6],&g_3[5]},{&g_3[5],&g_3[5],&g_3[7],&g_3[5],(void*)0},{(void*)0,&g_3[7],&g_3[5],&g_3[5],(void*)0}},{{&g_3[5],&g_3[7],&g_3[5],&g_3[5],&g_3[4]},{(void*)0,&g_3[4],&g_3[5],&g_3[5],&g_3[7]},{&g_3[5],&g_3[5],&g_3[6],&g_3[5],&g_3[4]},{&g_3[5],(void*)0,&g_3[5],&g_3[5],(void*)0},{(void*)0,&g_3[5],&g_3[5],&g_3[5],(void*)0}},{{&g_3[5],&g_3[4],&g_3[6],&g_3[5],(void*)0},{(void*)0,&g_3[7],&g_3[5],&g_3[5],(void*)0},{&g_3[5],&g_3[7],&g_3[5],&g_3[5],&g_3[4]},{(void*)0,&g_3[4],&g_3[5],&g_3[5],&g_3[7]},{&g_3[5],&g_3[5],&g_3[6],&g_3[5],&g_3[4]}},{{&g_3[5],(void*)0,&g_3[5],&g_3[5],(void*)0},{(void*)0,&g_3[5],&g_3[5],&g_3[5],(void*)0},{&g_3[5],&g_3[4],&g_3[6],&g_3[5],(void*)0},{(void*)0,&g_3[7],&g_3[5],&g_3[5],(void*)0},{&g_3[5],&g_3[7],&g_3[5],&g_3[5],&g_3[4]}}};
        int32_t l_218 = 0x3BB3813AL;
        float *l_261 = (void*)0;
        const int8_t *l_302 = &g_124;
        const int8_t **l_301[1];
        int8_t ** const *l_426 = &l_315;
        int32_t l_441 = 0L;
        int32_t l_444 = 8L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_301[i] = &l_302;
    }
    return (*l_103);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_3[i], sizeof(g_3[i]), "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    transparent_crc(g_22, "g_22", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_51[i], "g_51[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_63, "g_63", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_68[i][j], "g_68[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    transparent_crc(g_107, "g_107", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_123[i][j], "g_123[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_124, "g_124", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_125[i][j], "g_125[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_127[i], "g_127[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc(g_181, "g_181", print_hash_value);
    transparent_crc(g_219, "g_219", print_hash_value);
    transparent_crc(g_226, "g_226", print_hash_value);
    transparent_crc(g_254, "g_254", print_hash_value);
    transparent_crc(g_282, "g_282", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_351[i][j][k], "g_351[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_354, sizeof(g_354), "g_354", print_hash_value);
    transparent_crc(g_355, "g_355", print_hash_value);
    transparent_crc(g_356, "g_356", print_hash_value);
    transparent_crc(g_357, "g_357", print_hash_value);
    transparent_crc(g_358, "g_358", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_399[i], "g_399[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_431, "g_431", print_hash_value);
    transparent_crc(g_447, "g_447", print_hash_value);
    transparent_crc(g_485, "g_485", print_hash_value);
    transparent_crc(g_520, "g_520", print_hash_value);
    transparent_crc(g_543, "g_543", print_hash_value);
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc_bytes (&g_633, sizeof(g_633), "g_633", print_hash_value);
    transparent_crc(g_761, "g_761", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_762[i], "g_762[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_847, sizeof(g_847), "g_847", print_hash_value);
    transparent_crc(g_848, "g_848", print_hash_value);
    transparent_crc(g_1055, "g_1055", print_hash_value);
    transparent_crc(g_1464, "g_1464", print_hash_value);
    transparent_crc(g_1741, "g_1741", print_hash_value);
    transparent_crc(g_1744, "g_1744", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 412
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 46
breakdown:
   depth: 1, occurrence: 189
   depth: 2, occurrence: 48
   depth: 3, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 3
   depth: 9, occurrence: 1
   depth: 12, occurrence: 1
   depth: 15, occurrence: 1
   depth: 19, occurrence: 2
   depth: 20, occurrence: 3
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 30, occurrence: 4
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 2
   depth: 46, occurrence: 1

XXX total number of pointers: 416

XXX times a variable address is taken: 918
XXX times a pointer is dereferenced on RHS: 180
breakdown:
   depth: 1, occurrence: 134
   depth: 2, occurrence: 45
   depth: 3, occurrence: 1
XXX times a pointer is dereferenced on LHS: 236
breakdown:
   depth: 1, occurrence: 215
   depth: 2, occurrence: 16
   depth: 3, occurrence: 5
XXX times a pointer is compared with null: 32
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 3670

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 840
   level: 2, occurrence: 173
   level: 3, occurrence: 65
   level: 4, occurrence: 18
XXX number of pointers point to pointers: 169
XXX number of pointers point to scalars: 247
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.5
XXX average alias set size: 1.44

XXX times a non-volatile is read: 1369
XXX times a non-volatile is write: 710
XXX times a volatile is read: 66
XXX    times read thru a pointer: 19
XXX times a volatile is write: 38
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 905
XXX percentage of non-volatile access: 95.2

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 185
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 20
   depth: 2, occurrence: 26
   depth: 3, occurrence: 25
   depth: 4, occurrence: 32
   depth: 5, occurrence: 50

XXX percentage a fresh-made variable is used: 18.9
XXX percentage an existing variable is used: 81.1
********************* end of statistics **********************/


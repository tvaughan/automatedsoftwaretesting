/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1872059815
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile int64_t  f0;
   const uint32_t  f1;
   int8_t * f2;
   volatile uint32_t  f3;
};

union U1 {
   signed f0 : 28;
   int32_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = (-4L);
static int32_t g_6 = (-1L);
static int8_t g_23 = 0xF6L;
static int8_t *g_22[7][3][6] = {{{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,(void*)0,&g_23,&g_23,(void*)0,&g_23}},{{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,(void*)0,&g_23,&g_23,&g_23,&g_23}},{{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,(void*)0,&g_23,&g_23,&g_23}},{{&g_23,&g_23,(void*)0,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23}},{{&g_23,&g_23,&g_23,&g_23,&g_23,(void*)0},{&g_23,&g_23,&g_23,&g_23,&g_23,(void*)0},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23}},{{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,(void*)0,&g_23,&g_23,&g_23}},{{&g_23,&g_23,(void*)0,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23},{&g_23,&g_23,&g_23,&g_23,&g_23,&g_23}}};
static union U0 g_24 = {-8L};/* VOLATILE GLOBAL g_24 */
static volatile int32_t g_112 = (-3L);/* VOLATILE GLOBAL g_112 */
static volatile int32_t * const g_111 = &g_112;
static volatile int32_t * const *g_110[8][3][3] = {{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}},{{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111},{&g_111,&g_111,&g_111}}};
static const volatile union U1 g_113 = {0x17F0EF7CL};/* VOLATILE GLOBAL g_113 */
static uint64_t g_118 = 0x4C61C4CBDEF93A2FLL;
static float g_122 = (-0x8.Ep-1);
static int16_t g_123 = 0x44E5L;
static int32_t *** const *g_126 = (void*)0;
static int32_t *** const ** volatile g_127 = &g_126;/* VOLATILE GLOBAL g_127 */
static uint32_t g_150 = 2UL;
static uint32_t g_151 = 0xD108F377L;
static volatile union U0 g_185 = {0L};/* VOLATILE GLOBAL g_185 */
static int64_t g_192[6] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
static int64_t *g_191 = &g_192[0];
static uint16_t g_199 = 0x1CE9L;
static int64_t **g_205 = &g_191;
static int64_t **g_206[1][10][1] = {{{&g_191},{&g_191},{&g_191},{&g_191},{&g_191},{&g_191},{&g_191},{&g_191},{&g_191},{&g_191}}};
static volatile int64_t g_210 = 0x60BA7D1E3074757FLL;/* VOLATILE GLOBAL g_210 */
static uint32_t g_233 = 0xD4DF7525L;
static uint8_t g_282 = 0x51L;
static uint16_t g_286 = 0x7553L;
static uint8_t g_287 = 0x24L;
static uint32_t g_315 = 1UL;
static int8_t g_358 = 0x04L;
static int8_t g_360[10] = {0x45L,0x45L,0x49L,6L,0x49L,0x45L,0x45L,0x49L,6L,0x49L};
static int32_t *g_376 = &g_2;
static int32_t **g_375 = &g_376;
static int32_t ***g_374 = &g_375;
static int32_t ****g_373 = &g_374;
static int64_t g_380 = 0xAA5A37EBB39F1051LL;
static int64_t g_400 = 0x53217AC5580D7449LL;
static uint16_t *g_415[4][6] = {{&g_286,&g_199,&g_199,&g_286,&g_199,&g_286},{&g_199,&g_199,&g_199,&g_199,&g_199,&g_199},{&g_199,(void*)0,&g_286,&g_199,&g_286,&g_286},{&g_199,(void*)0,(void*)0,&g_199,&g_286,&g_286}};
static uint16_t **g_414 = &g_415[2][4];
static uint16_t ***g_413 = &g_414;
static volatile int32_t g_453 = 1L;/* VOLATILE GLOBAL g_453 */
static float * volatile g_463 = &g_122;/* VOLATILE GLOBAL g_463 */
static const volatile union U1 *g_497 = &g_113;
static const volatile union U1 ** volatile g_496 = &g_497;/* VOLATILE GLOBAL g_496 */
static volatile uint8_t * volatile * volatile g_540 = (void*)0;/* VOLATILE GLOBAL g_540 */
static const uint32_t * volatile g_548 = &g_24.f1;/* VOLATILE GLOBAL g_548 */
static const uint32_t * volatile *g_547 = &g_548;
static int8_t g_584 = 0xF9L;
static const int32_t g_606 = 2L;
static const int32_t *g_605 = &g_606;
static const int32_t ** volatile g_607[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const int32_t *g_610 = &g_6;
static const int32_t ** volatile g_609 = &g_610;/* VOLATILE GLOBAL g_609 */
static const int32_t ** volatile g_611 = &g_610;/* VOLATILE GLOBAL g_611 */
static union U1 *g_646 = (void*)0;
static union U1 **g_645 = &g_646;
static volatile union U0 *g_659 = (void*)0;
static volatile union U0 **g_658 = &g_659;
static union U0 g_662 = {0xA3B38BE298F7F4CFLL};/* VOLATILE GLOBAL g_662 */
static float g_703[9][6][4] = {{{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49},{0xD.64663Ap+26,0x4.DDEC11p-33,0x9.3349A7p+49,0x9.3349A7p+49},{(-0x1.Dp+1),(-0x1.Dp+1),0x2.31914Fp+99,0x4.DDEC11p-33},{0x4.DDEC11p-33,0xD.64663Ap+26,0x2.31914Fp+99,0xD.64663Ap+26},{(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99},{0xD.64663Ap+26,0xD.0C2409p+53,0xD.0C2409p+53,0xD.64663Ap+26}},{{0xD.0C2409p+53,0xD.64663Ap+26,(-0x1.Dp+1),0x4.DDEC11p-33},{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49},{0xD.64663Ap+26,0x4.DDEC11p-33,0x9.3349A7p+49,0x9.3349A7p+49},{(-0x1.Dp+1),(-0x1.Dp+1),0x2.31914Fp+99,0x4.DDEC11p-33},{0x4.DDEC11p-33,0xD.64663Ap+26,0x2.31914Fp+99,0xD.64663Ap+26},{(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99}},{{0xD.64663Ap+26,0xD.0C2409p+53,0xD.0C2409p+53,0xD.64663Ap+26},{0xD.0C2409p+53,0xD.64663Ap+26,(-0x1.Dp+1),0x4.DDEC11p-33},{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49},{0xD.64663Ap+26,0x4.DDEC11p-33,0x9.3349A7p+49,0x9.3349A7p+49},{(-0x1.Dp+1),(-0x1.Dp+1),0x2.31914Fp+99,0x4.DDEC11p-33},{0x4.DDEC11p-33,0xD.64663Ap+26,0x2.31914Fp+99,0xD.64663Ap+26}},{{(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99},{0xD.64663Ap+26,0xD.0C2409p+53,0xD.0C2409p+53,0xD.64663Ap+26},{0xD.0C2409p+53,0xD.64663Ap+26,(-0x1.Dp+1),0x4.DDEC11p-33},{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49},{0xD.64663Ap+26,0x4.DDEC11p-33,0x9.3349A7p+49,0x9.3349A7p+49},{(-0x1.Dp+1),(-0x1.Dp+1),0x2.31914Fp+99,0x4.DDEC11p-33}},{{0x4.DDEC11p-33,0xD.64663Ap+26,0x2.31914Fp+99,0xD.64663Ap+26},{(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99},{0xD.64663Ap+26,0xD.0C2409p+53,0xD.0C2409p+53,0xD.64663Ap+26},{0xD.0C2409p+53,0xD.64663Ap+26,(-0x1.Dp+1),0x4.DDEC11p-33},{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49},{0xD.64663Ap+26,0x4.DDEC11p-33,0x9.3349A7p+49,0x9.3349A7p+49}},{{(-0x1.Dp+1),(-0x1.Dp+1),0x2.31914Fp+99,0x4.DDEC11p-33},{0x4.DDEC11p-33,0xD.64663Ap+26,0x2.31914Fp+99,0xD.64663Ap+26},{(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99},{0xD.64663Ap+26,0xD.0C2409p+53,0xD.0C2409p+53,0xD.64663Ap+26},{0xD.0C2409p+53,0xD.64663Ap+26,(-0x1.Dp+1),0x4.DDEC11p-33},{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49}},{{0xD.64663Ap+26,0x4.DDEC11p-33,0x9.3349A7p+49,0x9.3349A7p+49},{(-0x1.Dp+1),(-0x1.Dp+1),0x2.31914Fp+99,0x4.DDEC11p-33},{0x4.DDEC11p-33,0xD.64663Ap+26,0x2.31914Fp+99,0xD.64663Ap+26},{(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99},{0xD.64663Ap+26,0xD.0C2409p+53,0xD.0C2409p+53,0xD.64663Ap+26},{0xD.0C2409p+53,0xD.64663Ap+26,(-0x1.Dp+1),0x4.DDEC11p-33}},{{0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53,0x9.3349A7p+49},{0xD.64663Ap+26,0x4.DDEC11p-33,0x2.31914Fp+99,0x2.31914Fp+99},{0x7.16C3C0p-38,0x7.16C3C0p-38,(-0x1.Dp+1),0x9.3349A7p+49},{0x9.3349A7p+49,0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53},{0x7.16C3C0p-38,0x4.DDEC11p-33,0x2.31914Fp+99,(-0x1.Dp+1)},{0xD.0C2409p+53,0x4.DDEC11p-33,0x4.DDEC11p-33,0xD.0C2409p+53}},{{0x4.DDEC11p-33,0xD.0C2409p+53,0x7.16C3C0p-38,0x9.3349A7p+49},{0x4.DDEC11p-33,0x7.16C3C0p-38,0x4.DDEC11p-33,0x2.31914Fp+99},{0xD.0C2409p+53,0x9.3349A7p+49,0x2.31914Fp+99,0x2.31914Fp+99},{0x7.16C3C0p-38,0x7.16C3C0p-38,(-0x1.Dp+1),0x9.3349A7p+49},{0x9.3349A7p+49,0xD.0C2409p+53,(-0x1.Dp+1),0xD.0C2409p+53},{0x7.16C3C0p-38,0x4.DDEC11p-33,0x2.31914Fp+99,(-0x1.Dp+1)}}};
static int16_t g_707 = 0x619FL;
static int16_t * const g_709[2] = {&g_123,&g_123};
static int16_t * const *g_708 = &g_709[1];
static union U0 **g_787 = (void*)0;
static union U0 ***g_786 = &g_787;
static int32_t g_822 = 0xD4C8A06AL;
static const union U1 g_940 = {0xFBE3979BL};
static const union U1 *g_942 = &g_940;
static const union U1 ** volatile g_941[10][9] = {{(void*)0,&g_942,&g_942,&g_942,(void*)0,&g_942,&g_942,&g_942,&g_942},{&g_942,&g_942,(void*)0,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942},{&g_942,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942},{&g_942,&g_942,(void*)0,&g_942,&g_942,&g_942,&g_942,(void*)0,&g_942},{(void*)0,&g_942,&g_942,(void*)0,&g_942,(void*)0,&g_942,&g_942,(void*)0},{&g_942,&g_942,(void*)0,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942},{&g_942,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942,&g_942},{&g_942,&g_942,&g_942,(void*)0,&g_942,&g_942,&g_942,&g_942,&g_942},{(void*)0,&g_942,(void*)0,&g_942,&g_942,(void*)0,&g_942,(void*)0,&g_942},{&g_942,&g_942,&g_942,&g_942,&g_942,(void*)0,&g_942,&g_942,&g_942}};
static const union U1 ** volatile g_943[6][5][3] = {{{&g_942,&g_942,&g_942},{(void*)0,&g_942,&g_942},{&g_942,&g_942,&g_942},{(void*)0,&g_942,(void*)0},{&g_942,&g_942,&g_942}},{{(void*)0,&g_942,(void*)0},{&g_942,&g_942,&g_942},{(void*)0,&g_942,&g_942},{&g_942,&g_942,&g_942},{(void*)0,&g_942,(void*)0}},{{&g_942,&g_942,&g_942},{(void*)0,&g_942,(void*)0},{&g_942,&g_942,&g_942},{(void*)0,&g_942,&g_942},{&g_942,&g_942,&g_942}},{{(void*)0,&g_942,(void*)0},{&g_942,&g_942,&g_942},{(void*)0,&g_942,(void*)0},{&g_942,&g_942,&g_942},{(void*)0,&g_942,(void*)0}},{{&g_942,&g_942,&g_942},{(void*)0,(void*)0,&g_942},{&g_942,&g_942,&g_942},{(void*)0,(void*)0,(void*)0},{&g_942,&g_942,&g_942}},{{(void*)0,&g_942,(void*)0},{&g_942,&g_942,&g_942},{(void*)0,(void*)0,&g_942},{&g_942,&g_942,&g_942},{(void*)0,(void*)0,(void*)0}}};
static union U1 ***g_974[5][6][8] = {{{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,&g_645,(void*)0},{&g_645,&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,(void*)0,&g_645},{&g_645,&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,&g_645}},{{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,(void*)0},{&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,(void*)0,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645}},{{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,&g_645,(void*)0},{&g_645,&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,(void*)0,&g_645},{&g_645,&g_645,&g_645,&g_645,(void*)0,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645}},{{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645}},{{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,(void*)0,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645},{&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645,&g_645}}};
static union U1 **** volatile g_973 = &g_974[2][3][1];/* VOLATILE GLOBAL g_973 */
static union U0 g_981[10][2] = {{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}},{{-6L},{-6L}}};
static uint32_t g_1012 = 0x5EEBCF79L;
static int32_t g_1013 = 0x0FB28E2CL;
static volatile union U0 g_1014 = {0xBE9C61E8556D953DLL};/* VOLATILE GLOBAL g_1014 */
static int64_t *g_1023 = (void*)0;
static int64_t ** const g_1022 = &g_1023;
static int64_t ** const *g_1021[6] = {&g_1022,&g_1022,&g_1022,&g_1022,&g_1022,&g_1022};
static int64_t ** const **g_1020 = &g_1021[3];
static volatile int64_t * const g_1027[2] = {&g_210,&g_210};
static volatile int64_t * const *g_1026[2][9] = {{&g_1027[0],&g_1027[0],&g_1027[0],(void*)0,&g_1027[0],&g_1027[0],&g_1027[0],&g_1027[0],(void*)0},{&g_1027[0],(void*)0,&g_1027[0],&g_1027[0],&g_1027[0],&g_1027[0],(void*)0,&g_1027[0],&g_1027[0]}};
static volatile int64_t * const **g_1025 = &g_1026[0][3];
static volatile int64_t * const ** volatile * volatile g_1024[8] = {(void*)0,(void*)0,&g_1025,(void*)0,(void*)0,&g_1025,(void*)0,(void*)0};
static volatile uint16_t g_1047 = 0xA736L;/* VOLATILE GLOBAL g_1047 */
static union U0 g_1093 = {0xC935148D89221EEFLL};/* VOLATILE GLOBAL g_1093 */
static union U0 g_1117 = {1L};/* VOLATILE GLOBAL g_1117 */
static union U0 g_1156 = {0x4DD6E8FCEB3A80F9LL};/* VOLATILE GLOBAL g_1156 */
static union U0 *g_1155 = &g_1156;
static union U1 g_1195 = {-5L};
static int8_t g_1199 = (-1L);
static volatile float * volatile * volatile *g_1205 = (void*)0;
static uint64_t g_1210 = 18446744073709551609UL;
static union U0 g_1234 = {0x159C9ACB5290AA4FLL};/* VOLATILE GLOBAL g_1234 */
static float g_1312[1] = {(-0x1.Ap+1)};
static uint8_t *g_1335 = (void*)0;
static uint8_t * volatile *g_1334 = &g_1335;
static uint8_t * volatile * volatile *g_1333 = &g_1334;
static volatile union U0 g_1368 = {-2L};/* VOLATILE GLOBAL g_1368 */
static int32_t g_1477[7][10] = {{0x18F45148L,0x18F45148L,(-10L),0x7E558760L,0x51A6CD7FL,(-10L),0x51A6CD7FL,0x7E558760L,(-10L),0x18F45148L},{0x51A6CD7FL,4L,0x758931BAL,0x51A6CD7FL,0x4F128E66L,0x4F128E66L,0x51A6CD7FL,0x758931BAL,4L,0x51A6CD7FL},{0x758931BAL,0x18F45148L,4L,0x4F128E66L,0x18F45148L,0x4F128E66L,4L,0x18F45148L,0x758931BAL,0x758931BAL},{0x51A6CD7FL,0x7E558760L,(-10L),0x18F45148L,0x18F45148L,(-10L),0x7E558760L,0x51A6CD7FL,(-10L),0x51A6CD7FL},{0x18F45148L,4L,0x4F128E66L,0x18F45148L,0x4F128E66L,4L,0x18F45148L,0x758931BAL,0x758931BAL,0x18F45148L},{0x758931BAL,0x51A6CD7FL,0x4F128E66L,0x4F128E66L,0x4F128E66L,7L,(-10L),0x4F128E66L,(-10L),7L},{4L,0x4F128E66L,0x18F45148L,0x4F128E66L,4L,0x18F45148L,0x758931BAL,0x758931BAL,0x18F45148L,4L}};
static int32_t g_1478 = 1L;
static int32_t g_1479 = 0xA10FFA5AL;
static int32_t g_1480[2][7][5] = {{{0x0CB4089EL,0x15338C57L,6L,0L,4L},{0xF827B160L,1L,1L,0xF827B160L,0x6B053B2BL},{(-4L),1L,0L,0L,0x0F54EA0FL},{1L,0L,(-6L),1L,1L},{0x15338C57L,(-4L),0L,0L,(-4L)},{0x7BE59457L,0x8A3BF186L,(-6L),0xF827B160L,0xDEABEAA7L},{1L,4L,1L,0L,0L}},{{0x8A3BF186L,0x58FB2951L,0x9194F4FFL,0x3F804A33L,0x3F804A33L},{1L,0x10178029L,1L,8L,0x2140A299L},{0x7BE59457L,(-6L),1L,0x8A3BF186L,0x6D632FA4L},{0x15338C57L,0L,0x0F54EA0FL,1L,0x4524E562L},{1L,0x6D632FA4L,1L,0x6D632FA4L,1L},{(-4L),6L,1L,4L,1L},{0xF827B160L,0xDEABEAA7L,0x9194F4FFL,0x6DE95BE1L,(-6L)}}};
static int32_t g_1481 = 0x18075FD8L;
static uint16_t * volatile *g_1498 = &g_415[2][2];
static uint16_t * volatile **g_1497 = &g_1498;
static uint16_t * volatile ** volatile *g_1496 = &g_1497;
static uint16_t * volatile ** volatile **g_1495 = &g_1496;
static volatile uint32_t g_1522 = 4UL;/* VOLATILE GLOBAL g_1522 */
static volatile uint64_t g_1538 = 1UL;/* VOLATILE GLOBAL g_1538 */
static volatile uint64_t * const g_1537 = &g_1538;
static volatile uint64_t * const * volatile g_1536 = &g_1537;/* VOLATILE GLOBAL g_1536 */
static uint32_t *g_1541 = &g_233;
static uint32_t **g_1540[6][10][4] = {{{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,(void*)0},{&g_1541,(void*)0,(void*)0,(void*)0},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,(void*)0,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541}},{{&g_1541,&g_1541,(void*)0,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,(void*)0,&g_1541,&g_1541},{&g_1541,(void*)0,&g_1541,&g_1541},{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541}},{{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,(void*)0,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,(void*)0},{&g_1541,&g_1541,&g_1541,&g_1541}},{{&g_1541,&g_1541,(void*)0,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,(void*)0},{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,(void*)0,&g_1541}},{{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,(void*)0,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{(void*)0,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,(void*)0},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{(void*)0,&g_1541,&g_1541,&g_1541}},{{&g_1541,&g_1541,(void*)0,(void*)0},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541},{&g_1541,&g_1541,&g_1541,&g_1541}}};
static uint32_t *** volatile g_1539 = &g_1540[1][3][2];/* VOLATILE GLOBAL g_1539 */
static const union U1 ** const  volatile g_1556 = &g_942;/* VOLATILE GLOBAL g_1556 */
static int32_t g_1638[9] = {(-10L),0x2057DE8AL,(-10L),(-10L),0x2057DE8AL,(-10L),(-10L),0x2057DE8AL,(-10L)};
static int64_t ***g_1661 = &g_206[0][2][0];
static int64_t ****g_1660[2][1] = {{&g_1661},{&g_1661}};
static int64_t *****g_1659 = &g_1660[1][0];
static volatile int32_t g_1733 = (-1L);/* VOLATILE GLOBAL g_1733 */
static int16_t g_1772[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static const int64_t *g_1789 = &g_192[0];
static const int64_t * const *g_1788 = &g_1789;
static const int64_t * const **g_1787 = &g_1788;
static float *g_1797 = &g_1312[0];
static float **g_1796[4][6] = {{&g_1797,&g_1797,&g_1797,&g_1797,&g_1797,&g_1797},{(void*)0,&g_1797,(void*)0,&g_1797,(void*)0,&g_1797},{&g_1797,&g_1797,&g_1797,&g_1797,&g_1797,&g_1797},{(void*)0,&g_1797,(void*)0,&g_1797,(void*)0,&g_1797}};
static float *** const  volatile g_1795 = &g_1796[2][0];/* VOLATILE GLOBAL g_1795 */
static int32_t *g_1906 = &g_1638[0];
static uint16_t * const *g_1970 = &g_415[2][4];
static uint16_t * const **g_1969 = &g_1970;
static uint8_t g_2090 = 0xDEL;
static volatile union U0 g_2160 = {-1L};/* VOLATILE GLOBAL g_2160 */
static uint32_t g_2198 = 0xCF72DB34L;
static volatile uint16_t g_2251 = 0x6517L;/* VOLATILE GLOBAL g_2251 */
static uint64_t **g_2273 = (void*)0;
static uint64_t *g_2275[4] = {&g_118,&g_118,&g_118,&g_118};
static uint64_t **g_2274 = &g_2275[2];
static uint64_t **g_2276 = &g_2275[2];
static union U0 g_2326[7][3] = {{{5L},{0x4D042BC16C8DF515LL},{5L}},{{8L},{8L},{8L}},{{5L},{0x4D042BC16C8DF515LL},{5L}},{{8L},{8L},{8L}},{{5L},{0x4D042BC16C8DF515LL},{5L}},{{8L},{8L},{8L}},{{5L},{0x4D042BC16C8DF515LL},{5L}}};
static int16_t g_2346 = 1L;


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static int8_t  func_13(int8_t * p_14, uint64_t  p_15, uint64_t  p_16, int8_t  p_17, int32_t  p_18);
static union U0  func_19(int8_t * p_20, const uint32_t  p_21);
static int32_t  func_27(uint32_t  p_28, uint32_t  p_29, uint8_t  p_30, const int8_t * p_31, uint16_t  p_32);
static uint64_t  func_40(const union U1  p_41, int16_t  p_42, uint32_t  p_43);
static uint32_t  func_47(int16_t  p_48, float  p_49, int8_t  p_50, int32_t * p_51);
static int32_t  func_63(uint64_t  p_64);
static int16_t  func_65(uint32_t  p_66, float  p_67, const uint64_t  p_68, int8_t * p_69, union U1  p_70);
static uint8_t  func_75(int32_t * p_76, int32_t * p_77, int32_t  p_78);
static int32_t * func_79(uint16_t  p_80);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_22 g_6 g_24 g_1155 g_1156 g_1541 g_233 g_1788 g_1789 g_192 g_1797 g_1312 g_584 g_1477 g_2346 g_282 g_1537 g_1538 g_2274 g_2275 g_118 g_610 g_111 g_112
 * writes: g_2 g_23 g_6 g_2346 g_282 g_708 g_112
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_8[3][5][6] = {{{0x6B0E1B1BL,0x25155467L,0xF33880E0L,6UL,6UL,0xF33880E0L},{0x6B0E1B1BL,0x6B0E1B1BL,6UL,0x82A10AA4L,0x923CCDE6L,0x82A10AA4L},{0x25155467L,0x6B0E1B1BL,0x25155467L,0xF33880E0L,6UL,6UL},{6UL,0x25155467L,0x25155467L,6UL,0x6B0E1B1BL,0x82A10AA4L},{0x82A10AA4L,6UL,6UL,6UL,0x82A10AA4L,0xF33880E0L}},{{6UL,0x82A10AA4L,0xF33880E0L,0xF33880E0L,0x82A10AA4L,6UL},{0x25155467L,6UL,0x6B0E1B1BL,0x82A10AA4L,0x6B0E1B1BL,6UL},{0x6B0E1B1BL,0x25155467L,0xF33880E0L,6UL,6UL,0xF33880E0L},{0x6B0E1B1BL,0x6B0E1B1BL,6UL,0x82A10AA4L,0x923CCDE6L,0x82A10AA4L},{0x25155467L,0x6B0E1B1BL,0x25155467L,0xF33880E0L,6UL,6UL}},{{6UL,0x25155467L,0x25155467L,6UL,0x6B0E1B1BL,0x82A10AA4L},{0x82A10AA4L,6UL,6UL,6UL,0x82A10AA4L,0xF33880E0L},{6UL,0x82A10AA4L,0xF33880E0L,0xF33880E0L,0x82A10AA4L,6UL},{0x25155467L,6UL,0x6B0E1B1BL,0x82A10AA4L,0x6B0E1B1BL,6UL},{0x6B0E1B1BL,0x25155467L,0xF33880E0L,6UL,6UL,0xF33880E0L}}};
    int32_t l_1963 = 0x678A5AF4L;
    uint8_t l_1964 = 0x84L;
    const int32_t **l_2353 = &g_610;
    const int32_t ***l_2352 = &l_2353;
    int i, j, k;
    for (g_2 = 0; (g_2 != (-24)); g_2 = safe_sub_func_int64_t_s_s(g_2, 6))
    { /* block id: 3 */
        int32_t *l_5 = &g_6;
        int32_t *l_7[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int64_t l_1965 = 0x18A21EC6C1B368D3LL;
        uint8_t *l_2347 = (void*)0;
        uint8_t *l_2348[4][2][7] = {{{(void*)0,&g_2090,&g_282,(void*)0,&g_287,(void*)0,&g_282},{&l_1964,&l_1964,&l_1964,&l_1964,&g_2090,&g_282,&g_287}},{{(void*)0,(void*)0,&l_1964,&l_1964,(void*)0,(void*)0,&g_287},{&g_287,&l_1964,&g_2090,&l_1964,&g_2090,&g_2090,&l_1964}},{{&g_2090,(void*)0,&g_2090,&g_287,&g_287,&g_282,(void*)0},{&g_2090,&l_1964,&g_287,&g_282,&g_287,&l_1964,&g_2090}},{{&l_1964,(void*)0,(void*)0,&g_287,&l_1964,&g_282,&l_1964},{&l_1964,&l_1964,&l_1964,&l_1964,&l_1964,&g_2090,&g_282}}};
        int32_t l_2349 = 0L;
        int16_t * const **l_2356[1][7] = {{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708}};
        int i, j, k;
        l_8[2][0][3]++;
        g_2346 ^= ((*l_5) = (safe_add_func_int8_t_s_s(func_13((func_19(g_22[4][2][5], g_6) , &g_360[5]), ((l_8[2][0][3] && 0x06L) == (((safe_mul_func_float_f_f((l_1963 = (((((((safe_sub_func_int64_t_s_s(0x84839B6CC735A513LL, ((l_8[2][0][3] <= (safe_div_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((((safe_sub_func_uint32_t_u_u(1UL, (*g_1541))) & 0L) , 1UL), (**g_1788))), 5)), l_8[2][0][3]))) ^ 6UL))) > l_8[0][3][5]) & l_8[2][0][3]) , 0xC.811C0Dp+29) <= (*g_1797)) != (*g_1797)) >= 0xA.DBA935p-97)), l_1964)) <= (*g_1797)) , 0x9622DCEF061EBB8FLL)), g_584, l_1964, l_1965), l_8[2][0][3])));
        g_708 = ((((++g_282) <= ((l_1963 = (((-2L) < ((l_1964 == (l_2352 == &l_2353)) < (((safe_add_func_uint64_t_u_u((*g_1537), (**g_2274))) < (((***l_2352) == 0x0DL) == (*l_5))) && (**l_2353)))) <= (***l_2352))) == (*l_5))) | (**g_2274)) , &g_709[0]);
    }
    (*g_111) ^= (-4L);
    return (**g_2274);
}


/* ------------------------------------------ */
/* 
 * reads : g_1477
 * writes:
 */
static int8_t  func_13(int8_t * p_14, uint64_t  p_15, uint64_t  p_16, int8_t  p_17, int32_t  p_18)
{ /* block id: 860 */
    uint16_t * const **l_1972 = &g_1970;
    int32_t l_1973 = 0x8C3CE804L;
    int8_t **l_2028 = &g_22[5][2][5];
    int8_t l_2052 = 0L;
    union U0 ** const *l_2055 = &g_787;
    union U0 ** const **l_2054[1][2][3] = {{{(void*)0,(void*)0,(void*)0},{&l_2055,&l_2055,&l_2055}}};
    union U0 ** const ***l_2053 = &l_2054[0][0][0];
    int32_t l_2060 = 0x3A083E5DL;
    int32_t l_2061 = 0xCC9911B0L;
    int32_t l_2063 = 0xFF8A22B9L;
    int32_t l_2069 = 1L;
    int32_t l_2071 = 0L;
    int32_t l_2074 = 0x18C485DBL;
    uint64_t *l_2088 = (void*)0;
    uint64_t *l_2089 = &g_118;
    union U1 l_2093 = {0x4602C31BL};
    uint32_t l_2094 = 6UL;
    const uint32_t l_2104[10] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    const int32_t l_2118 = (-7L);
    uint16_t l_2143 = 0UL;
    int32_t *l_2163 = &g_1477[0][7];
    int32_t l_2171 = 9L;
    int32_t l_2172 = 0xD603B305L;
    int32_t l_2173 = 5L;
    int32_t l_2174 = 0xC7718B64L;
    int8_t l_2197 = (-3L);
    int8_t ***l_2332 = &l_2028;
    uint16_t l_2345 = 0x8509L;
    int i, j, k;
    return (*l_2163);
}


/* ------------------------------------------ */
/* 
 * reads : g_24 g_6 g_1155 g_1156
 * writes: g_23 g_6
 */
static union U0  func_19(int8_t * p_20, const uint32_t  p_21)
{ /* block id: 5 */
    int32_t *l_26[10][4] = {{&g_6,(void*)0,&g_6,&g_6},{&g_6,&g_6,&g_6,(void*)0},{&g_6,(void*)0,&g_6,&g_6},{&g_6,&g_6,&g_6,(void*)0},{&g_6,(void*)0,&g_6,&g_6},{&g_6,&g_6,&g_6,(void*)0},{&g_6,(void*)0,&g_6,&g_6},{&g_6,&g_6,&g_6,(void*)0},{&g_6,(void*)0,&g_6,&g_6},{&g_6,&g_6,&g_6,(void*)0}};
    const union U1 l_44 = {1L};
    int8_t *l_56 = &g_23;
    int64_t l_57 = (-1L);
    const uint32_t l_1929 = 1UL;
    float l_1930[9][4] = {{(-0x4.4p-1),0xD.5457A2p+50,0x6.ED219Ep-29,(-0x7.Bp-1)},{0x6.ED219Ep-29,(-0x7.Bp-1),0x6.ED219Ep-29,0xD.5457A2p+50},{(-0x4.4p-1),(-0x7.Bp-1),0xB.852CF7p-74,(-0x7.Bp-1)},{(-0x4.4p-1),0xD.5457A2p+50,0x6.ED219Ep-29,(-0x7.Bp-1)},{0x6.ED219Ep-29,(-0x7.Bp-1),0x6.ED219Ep-29,0xD.5457A2p+50},{(-0x4.4p-1),(-0x7.Bp-1),0xB.852CF7p-74,(-0x7.Bp-1)},{(-0x4.4p-1),0xD.5457A2p+50,0x6.ED219Ep-29,(-0x7.Bp-1)},{0x6.ED219Ep-29,(-0x7.Bp-1),0x6.ED219Ep-29,0xD.5457A2p+50},{(-0x4.4p-1),(-0x7.Bp-1),0xB.852CF7p-74,(-0x7.Bp-1)}};
    uint64_t l_1932 = 0x46A8985A425B5E63LL;
    int i, j;
    for (g_23 = 0; (g_23 <= 2); g_23 += 1)
    { /* block id: 8 */
        return g_24;
    }
    g_6 |= (!(-2L));
    return (*g_1155);
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_111 g_112
 * writes: g_6
 */
static int32_t  func_27(uint32_t  p_28, uint32_t  p_29, uint8_t  p_30, const int8_t * p_31, uint16_t  p_32)
{ /* block id: 840 */
    uint64_t l_1931 = 0xEEC739D5C1E0805CLL;
    for (g_6 = 0; (g_6 <= 1); g_6 += 1)
    { /* block id: 843 */
        return (*g_111);
    }
    return l_1931;
}


/* ------------------------------------------ */
/* 
 * reads : g_233 g_113.f0 g_375 g_1210 g_380 g_111 g_1541 g_112 g_1659 g_1660 g_1661 g_206 g_191 g_1536 g_1537 g_1538
 * writes: g_233 g_199 g_282 g_376 g_1210 g_380 g_112 g_23 g_358 g_1906 g_192
 */
static uint64_t  func_40(const union U1  p_41, int16_t  p_42, uint32_t  p_43)
{ /* block id: 570 */
    int8_t *l_1266 = &g_358;
    int32_t l_1270[10] = {0xC61105DAL,5L,5L,0xC61105DAL,5L,5L,0xC61105DAL,5L,5L,0xC61105DAL};
    int32_t l_1273 = 0x086ED784L;
    uint8_t l_1274[2];
    uint32_t l_1281 = 4294967295UL;
    uint8_t l_1314[1];
    uint8_t *l_1332 = &l_1274[0];
    uint8_t **l_1331 = &l_1332;
    uint8_t ***l_1330 = &l_1331;
    union U1 ** const l_1354 = &g_646;
    uint16_t **l_1371 = &g_415[2][4];
    union U0 *l_1398 = &g_981[6][1];
    int32_t l_1443 = 1L;
    union U1 l_1506 = {0x80B0F461L};
    int64_t l_1520[5][6] = {{(-1L),0x5AEA87483AF66FD9LL,(-8L),1L,(-1L),(-1L)},{(-8L),0x0427AEE8BC6CB45FLL,1L,1L,0x0427AEE8BC6CB45FLL,(-8L)},{(-1L),(-1L),1L,(-8L),0x5AEA87483AF66FD9LL,(-1L)},{(-4L),(-1L),(-8L),(-4L),0x0427AEE8BC6CB45FLL,(-4L)},{(-4L),0x0427AEE8BC6CB45FLL,(-4L),(-8L),(-1L),(-4L)}};
    uint32_t l_1546 = 0UL;
    uint32_t * const l_1554 = (void*)0;
    uint64_t **l_1569 = (void*)0;
    int64_t *****l_1662 = &g_1660[0][0];
    uint32_t l_1689 = 4294967293UL;
    int16_t l_1732 = 0x4F81L;
    float **l_1885[6] = {&g_1797,&g_1797,&g_1797,&g_1797,&g_1797,&g_1797};
    uint8_t l_1892 = 0UL;
    int32_t *l_1907 = &g_1638[0];
    float l_1928 = (-0x9.Ap+1);
    int i, j;
    for (i = 0; i < 2; i++)
        l_1274[i] = 254UL;
    for (i = 0; i < 1; i++)
        l_1314[i] = 0x1EL;
    for (g_233 = 0; (g_233 <= 1); g_233 += 1)
    { /* block id: 573 */
        uint32_t l_1258[7] = {0x906BB574L,0x906BB574L,0x906BB574L,0x906BB574L,0x906BB574L,0x906BB574L,0x906BB574L};
        int64_t l_1261[3][9][9] = {{{0x64E2AAE23FE51328LL,0x64E2AAE23FE51328LL,0x20737FBB5FE61A72LL,0xB8526B72C5EA5D6ALL,1L,9L,0xB8526B72C5EA5D6ALL,0xC69754A7424A162BLL,0x91669BC104240395LL},{0L,0x35F4A1DC53C2D433LL,0x1DDFF874CDF5F795LL,0x35F4A1DC53C2D433LL,0L,0xA4A4E8CFAC6C852ELL,1L,0x35F4A1DC53C2D433LL,5L},{1L,0xB8526B72C5EA5D6ALL,0x20737FBB5FE61A72LL,0x64E2AAE23FE51328LL,0x64E2AAE23FE51328LL,0x20737FBB5FE61A72LL,0xB8526B72C5EA5D6ALL,1L,9L},{0L,0x35F4A1DC53C2D433LL,(-6L),(-1L),0L,0xA4A4E8CFAC6C852ELL,0L,(-1L),(-6L)},{1L,0x64E2AAE23FE51328LL,(-10L),0xB8526B72C5EA5D6ALL,0x64E2AAE23FE51328LL,9L,0xC69754A7424A162BLL,0xC69754A7424A162BLL,9L},{0L,(-1L),0x1DDFF874CDF5F795LL,(-1L),0L,0x8266396993B544A8LL,1L,(-1L),5L},{0x64E2AAE23FE51328LL,0xB8526B72C5EA5D6ALL,(-10L),0x64E2AAE23FE51328LL,1L,0x20737FBB5FE61A72LL,1L,0x8E6390F43E54B23ELL,0x64E2AAE23FE51328LL},{9L,0x1A1D9A5FFF77D071LL,0L,0xBE76875451F36208LL,9L,(-1L),9L,0xBE76875451F36208LL,0L},{0x6E06657F66792D57LL,0x6E06657F66792D57LL,0xC69754A7424A162BLL,0x362B70719402C167LL,0x8E6390F43E54B23ELL,1L,0x362B70719402C167LL,1L,0x64E2AAE23FE51328LL}},{{(-1L),0xBE76875451F36208LL,1L,0xBE76875451F36208LL,(-1L),0x35F4A1DC53C2D433LL,0x61C8B9643B89CC55LL,0xBE76875451F36208LL,0L},{0x8E6390F43E54B23ELL,0x362B70719402C167LL,0xC69754A7424A162BLL,0x6E06657F66792D57LL,0x6E06657F66792D57LL,0xC69754A7424A162BLL,0x362B70719402C167LL,0x8E6390F43E54B23ELL,1L},{9L,0xBE76875451F36208LL,0L,0x1A1D9A5FFF77D071LL,9L,0x35F4A1DC53C2D433LL,9L,0x1A1D9A5FFF77D071LL,0L},{0x8E6390F43E54B23ELL,0x6E06657F66792D57LL,0xB8526B72C5EA5D6ALL,0x362B70719402C167LL,0x6E06657F66792D57LL,1L,1L,1L,1L},{(-1L),0x1A1D9A5FFF77D071LL,1L,0x1A1D9A5FFF77D071LL,(-1L),(-1L),0x61C8B9643B89CC55LL,0x1A1D9A5FFF77D071LL,0L},{0x6E06657F66792D57LL,0x362B70719402C167LL,0xB8526B72C5EA5D6ALL,0x6E06657F66792D57LL,0x8E6390F43E54B23ELL,0xC69754A7424A162BLL,1L,0x8E6390F43E54B23ELL,0x64E2AAE23FE51328LL},{9L,0x1A1D9A5FFF77D071LL,0L,0xBE76875451F36208LL,9L,(-1L),9L,0xBE76875451F36208LL,0L},{0x6E06657F66792D57LL,0x6E06657F66792D57LL,0xC69754A7424A162BLL,0x362B70719402C167LL,0x8E6390F43E54B23ELL,1L,0x362B70719402C167LL,1L,0x64E2AAE23FE51328LL},{(-1L),0xBE76875451F36208LL,1L,0xBE76875451F36208LL,(-1L),0x35F4A1DC53C2D433LL,0x61C8B9643B89CC55LL,0xBE76875451F36208LL,0L}},{{0x8E6390F43E54B23ELL,0x362B70719402C167LL,0xC69754A7424A162BLL,0x6E06657F66792D57LL,0x6E06657F66792D57LL,0xC69754A7424A162BLL,0x362B70719402C167LL,0x8E6390F43E54B23ELL,1L},{9L,0xBE76875451F36208LL,0L,0x1A1D9A5FFF77D071LL,9L,0x35F4A1DC53C2D433LL,9L,0x1A1D9A5FFF77D071LL,0L},{0x8E6390F43E54B23ELL,0x6E06657F66792D57LL,0xB8526B72C5EA5D6ALL,0x362B70719402C167LL,0x6E06657F66792D57LL,1L,1L,1L,1L},{(-1L),0x1A1D9A5FFF77D071LL,1L,0x1A1D9A5FFF77D071LL,(-1L),(-1L),0x61C8B9643B89CC55LL,0x1A1D9A5FFF77D071LL,0L},{0x6E06657F66792D57LL,0x362B70719402C167LL,0xB8526B72C5EA5D6ALL,0x6E06657F66792D57LL,0x8E6390F43E54B23ELL,0xC69754A7424A162BLL,1L,0x8E6390F43E54B23ELL,0x64E2AAE23FE51328LL},{9L,0x1A1D9A5FFF77D071LL,0L,0xBE76875451F36208LL,9L,(-1L),9L,0xBE76875451F36208LL,0L},{0x6E06657F66792D57LL,0x6E06657F66792D57LL,0xC69754A7424A162BLL,0x362B70719402C167LL,0x8E6390F43E54B23ELL,1L,0x362B70719402C167LL,1L,0x64E2AAE23FE51328LL},{(-1L),0xBE76875451F36208LL,1L,0xBE76875451F36208LL,(-1L),0x35F4A1DC53C2D433LL,0x61C8B9643B89CC55LL,0xBE76875451F36208LL,0L},{0x8E6390F43E54B23ELL,0x362B70719402C167LL,0xC69754A7424A162BLL,0x6E06657F66792D57LL,0x6E06657F66792D57LL,0xC69754A7424A162BLL,0x362B70719402C167LL,0x8E6390F43E54B23ELL,1L}}};
        int32_t l_1267[5];
        uint32_t **l_1286 = (void*)0;
        union U1 l_1289 = {0x22AED62AL};
        int8_t l_1348 = 0x7FL;
        int32_t ***l_1369 = &g_375;
        float l_1466 = 0xC.0AA0ADp-60;
        int16_t l_1512 = (-6L);
        int32_t l_1523 = (-1L);
        uint64_t *l_1571 = &g_1210;
        uint64_t **l_1570 = &l_1571;
        float l_1576[1][5][3] = {{{0xC.E48854p-66,0x7.47E51Dp-11,0xC.E48854p-66},{0x4.1p-1,0x4.1p-1,0x4.1p-1},{0xC.E48854p-66,0x7.47E51Dp-11,0xC.E48854p-66},{0x4.1p-1,0x4.1p-1,0x4.1p-1},{0xC.E48854p-66,0x7.47E51Dp-11,0xC.E48854p-66}}};
        float l_1609 = 0xA.271145p-47;
        union U0 ***l_1643 = &g_787;
        int16_t l_1692 = (-10L);
        int32_t l_1722 = 5L;
        float *l_1794[2][5][6] = {{{&l_1576[0][3][1],&l_1576[0][1][0],&l_1576[0][0][2],&l_1576[0][0][2],&l_1576[0][1][0],&l_1576[0][3][1]},{&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][0][2],&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][3][1]},{&g_1312[0],&l_1576[0][3][1],&l_1576[0][3][1],&g_1312[0],&l_1576[0][1][0],&g_1312[0]},{&g_1312[0],&l_1576[0][1][0],&g_1312[0],&l_1576[0][3][1],&l_1576[0][3][1],&g_1312[0]},{&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][0][2],&l_1576[0][3][1],&l_1576[0][3][1]}},{{&l_1576[0][3][1],&l_1576[0][1][0],&l_1576[0][0][2],&l_1576[0][0][2],&l_1576[0][1][0],&l_1576[0][3][1]},{&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][0][2],&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][3][1]},{&g_1312[0],&l_1576[0][3][1],&l_1576[0][3][1],&g_1312[0],&l_1576[0][1][0],&g_1312[0]},{&g_1312[0],&l_1576[0][1][0],&g_1312[0],&l_1576[0][3][1],&l_1576[0][3][1],&g_1312[0]},{&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][3][1],&l_1576[0][0][2],&l_1576[0][3][1],&l_1576[0][3][1]}}};
        float **l_1793 = &l_1794[0][4][2];
        int32_t l_1808 = 0x069D85F6L;
        float l_1810 = 0x1.Cp+1;
        uint8_t l_1872[4] = {0xC1L,0xC1L,0xC1L,0xC1L};
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_1267[i] = 0x49E89799L;
        for (g_199 = 0; (g_199 <= 8); g_199 += 1)
        { /* block id: 576 */
            int32_t *l_1257[1];
            int8_t *l_1264[6][5] = {{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{(void*)0,&g_23,(void*)0,&g_23,(void*)0},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{(void*)0,&g_23,(void*)0,&g_23,(void*)0},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{(void*)0,&g_23,(void*)0,&g_23,(void*)0}};
            int8_t **l_1265[6] = {&g_22[4][2][1],&g_22[4][2][1],&g_22[4][2][1],&g_22[4][2][1],&g_22[4][2][1],&g_22[4][2][1]};
            int i, j;
            for (i = 0; i < 1; i++)
                l_1257[i] = &g_6;
            ++l_1258[1];
            if (l_1261[2][1][3])
                break;
            l_1273 |= (((safe_lshift_func_uint8_t_u_s(l_1261[2][1][3], (l_1267[3] ^= (l_1264[3][2] == (l_1266 = &g_23))))) < ((safe_mod_func_int16_t_s_s(l_1270[8], l_1270[3])) ^ l_1261[2][1][3])) != (safe_rshift_func_int8_t_s_s(l_1261[2][1][3], 1)));
            l_1274[0]--;
            for (g_282 = 0; (g_282 <= 1); g_282 += 1)
            { /* block id: 585 */
                return g_113.f0;
            }
        }
        (*g_375) = &l_1270[8];
        for (g_1210 = 0; (g_1210 <= 1); g_1210 += 1)
        { /* block id: 592 */
            int32_t l_1300 = 0L;
            int32_t l_1303 = (-2L);
            int32_t l_1305 = 3L;
            int32_t l_1307 = 0x24764163L;
            int32_t l_1309 = 8L;
            int32_t l_1311[1];
            uint8_t l_1320 = 0x6AL;
            union U0 **l_1363 = &g_1155;
            uint16_t l_1445 = 1UL;
            const int64_t *l_1533[10][8] = {{(void*)0,&g_380,&l_1261[2][6][1],&l_1520[1][3],&g_380,&l_1261[2][1][3],&g_192[4],&l_1261[2][6][1]},{&g_192[3],&g_192[4],&g_380,(void*)0,(void*)0,&g_380,&g_192[4],&g_192[3]},{&l_1261[1][4][1],(void*)0,&l_1261[2][6][1],&l_1261[2][1][3],(void*)0,&l_1261[1][4][1],&l_1261[2][1][3],&l_1520[1][3]},{(void*)0,&l_1261[1][4][1],&l_1261[2][1][3],&l_1520[1][3],&l_1261[2][1][3],&l_1261[1][4][1],(void*)0,&l_1261[2][1][3]},{&g_192[3],(void*)0,&l_1520[0][4],&g_192[3],&g_192[4],&g_380,(void*)0,(void*)0},{&l_1261[2][1][3],&g_192[4],&l_1261[2][6][1],&l_1261[2][6][1],&g_192[4],&l_1261[2][1][3],&g_380,&l_1520[1][3]},{&g_192[3],&g_380,&l_1261[2][1][3],(void*)0,&l_1261[2][1][3],&l_1261[2][1][3],&g_192[4],&l_1261[2][1][3]},{(void*)0,(void*)0,&g_380,(void*)0,(void*)0,(void*)0,(void*)0,&l_1520[1][3]},{&l_1261[1][4][1],(void*)0,&l_1261[2][1][3],&l_1261[2][6][1],(void*)0,&l_1261[1][4][1],&l_1261[1][4][1],(void*)0},{&g_192[3],&l_1261[2][1][3],&l_1261[2][1][3],&g_192[3],&g_380,&l_1261[2][1][3],(void*)0,&l_1261[2][1][3]}};
            const int64_t **l_1532 = &l_1533[4][2];
            const int64_t ***l_1531 = &l_1532;
            const int64_t ****l_1530 = &l_1531;
            const int64_t *****l_1529 = &l_1530;
            union U1 l_1621 = {1L};
            uint16_t ** const *l_1664 = &l_1371;
            uint16_t ** const **l_1663 = &l_1664;
            float l_1665 = 0x8.DDE65Bp-57;
            int8_t l_1723 = 1L;
            int8_t l_1729 = 0xE0L;
            int16_t l_1734 = 0x554AL;
            float *l_1750 = &l_1576[0][3][1];
            float **l_1749 = &l_1750;
            uint32_t ***l_1757[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int32_t l_1769 = 5L;
            uint16_t l_1773 = 0UL;
            int32_t *l_1792 = &g_1481;
            int i, j;
            for (i = 0; i < 1; i++)
                l_1311[i] = 0x7C2988B0L;
        }
    }
    for (g_1210 = 15; (g_1210 != 15); g_1210 = safe_add_func_uint8_t_u_u(g_1210, 8))
    { /* block id: 819 */
        union U1 l_1897[2] = {{0x6D3FFD88L},{0x6D3FFD88L}};
        int32_t l_1908 = 0xC49FD6B0L;
        uint8_t ***l_1925 = &l_1331;
        uint16_t l_1926 = 1UL;
        int8_t *l_1927 = (void*)0;
        int i;
        for (g_380 = (-29); (g_380 == 27); g_380 = safe_add_func_uint8_t_u_u(g_380, 3))
        { /* block id: 822 */
            float l_1894 = 0x1.Fp-1;
            union U1 *l_1898 = &l_1506;
            union U1 *l_1899 = &l_1897[1];
            const union U0 *l_1900 = &g_662;
            int32_t l_1905[9][3];
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1905[i][j] = 0L;
            }
            (*g_111) = ((safe_mod_func_uint8_t_u_u(((*l_1332) |= l_1892), (~p_43))) > 0x20D8B5B8L);
            (*g_111) ^= (0x6CCD6D71L <= (safe_lshift_func_uint8_t_u_u((((l_1908 = (((*l_1266) = (((*l_1899) = ((*l_1898) = l_1897[1])) , p_41.f0)) >= (l_1900 != ((safe_div_func_int32_t_s_s((safe_mul_func_int8_t_s_s((((l_1905[1][0] > (l_1273 &= ((g_1906 = &g_1638[0]) != l_1907))) & l_1908) == (l_1398 == l_1398)), l_1908)), (*g_1541))) , (void*)0)))) == p_41.f0) , p_42), p_43)));
        }
        l_1270[8] |= (((((safe_lshift_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(9UL, ((l_1897[1] , (safe_add_func_uint8_t_u_u(((((safe_div_func_int16_t_s_s((0x8CCC043B8F3440FCLL != ((*****g_1659) = 0x514C0A4866C57027LL)), (p_42 = ((l_1897[1].f0 = (safe_add_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(((((safe_lshift_func_uint16_t_u_u(((((((safe_sub_func_uint32_t_u_u(l_1897[1].f0, (0x4D477D6962B2ED70LL & 1L))) ^ ((((p_41.f0 , l_1925) == l_1925) && l_1314[0]) , l_1520[4][1])) & 0xB3L) && l_1926) , l_1927) == &g_360[4]), 3)) == 1L) , (void*)0) == &g_22[4][2][5]), l_1908)), p_41.f0))) | 0x44L)))) > 0L) == l_1506.f0) , 1UL), p_41.f0))) >= p_43))), l_1274[0])) || l_1908) == l_1443) & p_43) < (**g_1536));
    }
    l_1273 = p_43;
    return (**g_1536);
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_1024 g_282 g_547 g_548 g_24.f1 g_315 g_110 g_113 g_112 g_24.f0 g_23 g_123 g_126 g_127 g_118 g_150 g_111 g_185 g_191 g_151 g_192 g_199 g_210 g_374 g_375 g_400 g_2 g_708 g_709 g_206 g_1047 g_286 g_822 g_1020 g_185.f0 g_662.f1 g_1093 g_373 g_707 g_376 g_463 g_122 g_1117 g_233 g_287 g_605 g_606 g_609 g_610 g_973 g_974 g_1199 g_1205 g_942 g_940 g_1210 g_1093.f1 g_496 g_497 g_1234 g_1195.f0 g_360 g_1195.f1
 * writes: g_6 g_1020 g_282 g_315 g_118 g_122 g_123 g_126 g_150 g_151 g_112 g_191 g_199 g_205 g_206 g_192 g_376 g_400 g_822 g_23 g_360 g_707 g_703 g_233 g_287 g_358 g_1155 g_1199 g_1210 g_1195.f0 g_1195.f1
 */
static uint32_t  func_47(int16_t  p_48, float  p_49, int8_t  p_50, int32_t * p_51)
{ /* block id: 12 */
    int64_t l_61[1];
    int32_t *l_1015[8][5][6] = {{{&g_2,&g_6,&g_2,&g_822,(void*)0,&g_822},{&g_2,&g_822,&g_822,&g_6,&g_822,&g_822},{(void*)0,&g_2,(void*)0,&g_822,(void*)0,(void*)0},{&g_822,&g_2,&g_6,&g_822,&g_6,&g_2},{&g_822,&g_2,&g_822,&g_6,&g_822,(void*)0}},{{&g_6,&g_2,(void*)0,&g_822,&g_822,&g_822},{(void*)0,&g_2,&g_2,&g_2,&g_822,(void*)0},{&g_822,&g_2,&g_2,(void*)0,&g_6,&g_822},{(void*)0,&g_2,&g_822,(void*)0,(void*)0,&g_822},{&g_2,&g_2,&g_2,&g_2,&g_822,&g_822}},{{&g_6,&g_822,&g_2,(void*)0,(void*)0,&g_2},{&g_2,&g_6,&g_2,&g_6,&g_2,&g_822},{&g_822,&g_6,&g_2,&g_2,&g_2,&g_822},{&g_2,&g_2,&g_822,&g_822,&g_822,&g_822},{&g_2,&g_822,&g_2,(void*)0,&g_822,(void*)0}},{{&g_822,&g_822,&g_2,&g_6,&g_822,&g_822},{&g_2,&g_2,(void*)0,&g_6,(void*)0,(void*)0},{&g_822,&g_822,&g_822,(void*)0,(void*)0,&g_2},{&g_2,(void*)0,&g_6,&g_822,(void*)0,(void*)0},{&g_2,(void*)0,(void*)0,&g_2,&g_2,&g_822}},{{&g_822,(void*)0,&g_822,&g_6,&g_6,&g_822},{&g_2,&g_822,&g_2,(void*)0,&g_6,(void*)0},{&g_6,(void*)0,&g_2,&g_2,&g_2,&g_822},{&g_2,(void*)0,&g_822,(void*)0,(void*)0,&g_2},{(void*)0,(void*)0,&g_822,(void*)0,(void*)0,&g_2}},{{&g_822,&g_822,&g_822,&g_2,(void*)0,(void*)0},{(void*)0,&g_2,&g_822,&g_822,&g_822,(void*)0},{&g_6,&g_822,&g_822,&g_6,&g_822,&g_2},{&g_822,&g_822,&g_822,&g_822,&g_822,&g_2},{&g_822,&g_2,&g_822,&g_822,&g_2,&g_822}},{{(void*)0,&g_6,&g_2,&g_6,&g_2,(void*)0},{&g_2,&g_822,(void*)0,&g_822,&g_2,&g_2},{&g_822,&g_822,&g_822,&g_822,(void*)0,&g_2},{&g_822,&g_822,&g_822,&g_6,&g_822,&g_822},{(void*)0,&g_6,(void*)0,(void*)0,(void*)0,&g_6}},{{&g_822,&g_822,(void*)0,&g_822,(void*)0,&g_2},{&g_6,(void*)0,&g_822,(void*)0,&g_2,&g_2},{&g_2,(void*)0,&g_6,&g_2,(void*)0,&g_822},{&g_2,&g_822,&g_6,&g_2,(void*)0,(void*)0},{&g_822,&g_6,(void*)0,&g_822,&g_822,(void*)0}}};
    int64_t ** const *l_1018 = &g_206[0][2][0];
    int64_t ** const **l_1017 = &l_1018;
    uint64_t *l_1167 = &g_118;
    union U1 * const **l_1196[1][1];
    float *l_1208 = &g_122;
    float **l_1207 = &l_1208;
    float ***l_1206[2][2][9] = {{{(void*)0,&l_1207,&l_1207,(void*)0,&l_1207,&l_1207,&l_1207,(void*)0,&l_1207},{(void*)0,(void*)0,(void*)0,&l_1207,&l_1207,&l_1207,(void*)0,(void*)0,(void*)0}},{{&l_1207,(void*)0,&l_1207,&l_1207,&l_1207,(void*)0,&l_1207,&l_1207,(void*)0},{&l_1207,&l_1207,(void*)0,&l_1207,&l_1207,&l_1207,&l_1207,&l_1207,&l_1207}}};
    uint64_t l_1249 = 18446744073709551607UL;
    int32_t l_1254 = (-1L);
    uint16_t l_1256 = 65535UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_61[i] = (-5L);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_1196[i][j] = (void*)0;
    }
    for (g_6 = 0; (g_6 <= 20); g_6 = safe_add_func_int16_t_s_s(g_6, 1))
    { /* block id: 15 */
        uint32_t l_60 = 0xE3CEC334L;
        int32_t *l_62 = &g_2;
        const uint8_t *l_1031 = &g_282;
        const uint8_t **l_1030 = &l_1031;
        const uint8_t **l_1037 = (void*)0;
        int8_t l_1045 = 0x5EL;
        int32_t l_1061 = 7L;
        union U0 ****l_1139 = &g_786;
        union U1 ***l_1191 = &g_645;
        uint32_t l_1252[2][1];
        int16_t *l_1253 = &g_123;
        int i, j;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
                l_1252[i][j] = 0xC979EF2AL;
        }
        l_60 |= 7L;
        if (l_61[0])
        { /* block id: 17 */
            int32_t *l_88 = (void*)0;
            int64_t ** const ***l_1019[3];
            union U1 l_1040[10][8] = {{{0L},{1L},{0L},{0x1A11D8A1L},{3L},{0x2C5D8C18L},{0x7F6AC149L},{0x2C5D8C18L}},{{-3L},{0x1A11D8A1L},{0xA789E0CFL},{0x1A11D8A1L},{-3L},{0x014D0E08L},{3L},{-1L}},{{-3L},{0x014D0E08L},{3L},{-1L},{3L},{0x014D0E08L},{-3L},{0x1A11D8A1L}},{{0L},{0x1A11D8A1L},{3L},{0x2C5D8C18L},{0x7F6AC149L},{0x2C5D8C18L},{3L},{0x1A11D8A1L}},{{3L},{1L},{0xA789E0CFL},{-1L},{0x7F6AC149L},{0x1A11D8A1L},{0x7F6AC149L},{-1L}},{{0L},{1L},{0L},{0x1A11D8A1L},{3L},{0x2C5D8C18L},{0x7F6AC149L},{0x2C5D8C18L}},{{-3L},{0x1A11D8A1L},{0xA789E0CFL},{0x1A11D8A1L},{-3L},{0x014D0E08L},{3L},{-1L}},{{-3L},{0x014D0E08L},{3L},{-1L},{3L},{0x014D0E08L},{-3L},{0x1A11D8A1L}},{{0L},{0x1A11D8A1L},{3L},{0x2C5D8C18L},{0x7F6AC149L},{0x2C5D8C18L},{3L},{0x1A11D8A1L}},{{3L},{1L},{0xA789E0CFL},{-1L},{0x7F6AC149L},{0x1A11D8A1L},{0x7F6AC149L},{-1L}}};
            float *l_1098 = &g_122;
            float **l_1097 = &l_1098;
            int i, j;
            for (i = 0; i < 3; i++)
                l_1019[i] = &l_1017;
            for (p_50 = 0; (p_50 <= 0); p_50 += 1)
            { /* block id: 20 */
                int32_t *l_87 = &g_2;
                int32_t **l_89 = (void*)0;
                int32_t **l_90 = &l_87;
                union U1 l_791 = {7L};
                float *l_1016 = &g_122;
            }
            if ((((g_1020 = l_1017) == g_1024[4]) ^ p_50))
            { /* block id: 441 */
                const uint8_t ***l_1032 = &l_1030;
                const uint8_t ***l_1033 = (void*)0;
                const uint8_t **l_1035 = (void*)0;
                const uint8_t ***l_1034 = &l_1035;
                const uint8_t ***l_1036 = (void*)0;
                int32_t l_1038 = 0x02F7C7FCL;
                int64_t **l_1046[4][3][1];
                int i, j, k;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_1046[i][j][k] = &g_1023;
                    }
                }
                for (g_282 = (-26); (g_282 != 18); g_282 = safe_add_func_int32_t_s_s(g_282, 4))
                { /* block id: 444 */
                    return (**g_547);
                }
                for (g_315 = 0; (g_315 <= 0); g_315 += 1)
                { /* block id: 449 */
                    (**g_374) = func_79(p_48);
                    for (g_400 = 0; (g_400 <= 0); g_400 += 1)
                    { /* block id: 453 */
                        int i;
                        if ((*l_62))
                            break;
                    }
                }
                l_1037 = ((*l_1034) = ((*l_1032) = l_1030));
                g_822 &= (l_1038 || ((~(l_1038 = p_48)) && ((l_1040[2][6] , ((65529UL > p_50) <= (((safe_sub_func_uint8_t_u_u(((*l_62) >= ((p_50 >= ((((p_48 , ((safe_add_func_int64_t_s_s(p_48, l_1045)) < (**g_708))) , l_1046[2][0][0]) != (**l_1017)) , g_1047)) , 9L)), 5UL)) == p_48) & (*l_62)))) == g_286)));
            }
            else
            { /* block id: 462 */
                float l_1059 = (-0x1.Dp-1);
                int32_t l_1060[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_1060[i] = 4L;
                (*g_111) &= 0x1858CC4DL;
                (**g_374) = func_79(((l_1040[2][6].f0 & (((***l_1018) = (****g_1020)) | 18446744073709551607UL)) == (~(((l_1061 = ((*l_62) > (((safe_div_func_uint8_t_u_u((p_48 > (safe_mul_func_uint16_t_u_u((p_50 & (safe_mod_func_uint16_t_u_u((p_48 , p_50), (safe_add_func_int32_t_s_s(1L, (*l_62)))))), p_48))), p_48)) != 5L) > l_1060[1]))) > p_50) && l_1060[6]))));
            }
            for (g_23 = 19; (g_23 > (-15)); g_23 = safe_sub_func_uint64_t_u_u(g_23, 1))
            { /* block id: 470 */
                int32_t l_1078 = 0L;
                int32_t l_1104[7][2] = {{0x8A8A99C0L,0x8A8A99C0L},{0L,0x8A8A99C0L},{0x8A8A99C0L,0L},{0x8A8A99C0L,0x8A8A99C0L},{0L,0x8A8A99C0L},{0x8A8A99C0L,0L},{0x8A8A99C0L,0x8A8A99C0L}};
                int i, j;
                for (g_150 = 0; (g_150 <= 7); g_150 += 1)
                { /* block id: 473 */
                    int8_t l_1064[8][4][4] = {{{(-7L),6L,(-7L),0x93L},{1L,3L,0x2BL,6L},{(-7L),0x09L,0x2BL,0x93L},{0x2BL,0x93L,0x2BL,0x09L}},{{(-7L),(-1L),1L,0x93L},{(-7L),(-1L),0x2BL,(-1L)},{0x2BL,(-1L),0x2BL,(-1L)},{(-7L),0x93L,1L,(-1L)}},{{(-7L),0x09L,0x2BL,0x93L},{0x2BL,0x93L,0x2BL,0x09L},{(-7L),(-1L),1L,0x93L},{(-7L),(-1L),0x2BL,(-1L)}},{{0x2BL,(-1L),0x2BL,(-1L)},{(-7L),0x93L,1L,(-1L)},{(-7L),0x09L,0x2BL,0x93L},{0x2BL,0x93L,0x2BL,0x09L}},{{(-7L),(-1L),1L,0x93L},{(-7L),(-1L),0x2BL,(-1L)},{0x2BL,(-1L),0x2BL,(-1L)},{(-7L),0x93L,1L,(-1L)}},{{(-7L),0x09L,0x2BL,0x93L},{0x2BL,0x93L,0x2BL,0x09L},{(-7L),(-1L),1L,0x93L},{(-7L),(-1L),0x2BL,(-1L)}},{{0x2BL,(-1L),0x2BL,(-1L)},{(-7L),0x93L,1L,(-1L)},{(-7L),0x09L,0x2BL,0x93L},{0x2BL,0x93L,0x2BL,0x09L}},{{(-7L),(-1L),1L,0x93L},{(-7L),(-1L),0x2BL,(-1L)},{0x2BL,(-1L),0x2BL,(-1L)},{(-7L),0x93L,1L,(-1L)}}};
                    int32_t ** const **l_1084 = (void*)0;
                    float *l_1095 = &g_703[5][0][3];
                    float **l_1094 = &l_1095;
                    const uint64_t l_1103[10][1] = {{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL},{0xCAB8D2534604F33ALL}};
                    int i, j, k;
                    for (g_151 = 0; (g_151 <= 7); g_151 += 1)
                    { /* block id: 476 */
                        uint32_t l_1065 = 0UL;
                        uint64_t *l_1085 = &g_118;
                        int32_t l_1086 = (-2L);
                        int8_t *l_1092 = &g_360[0];
                        float ***l_1096 = &l_1094;
                        --l_1065;
                        l_1086 &= (safe_add_func_uint64_t_u_u(((*l_1085) = (safe_rshift_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(p_48, (safe_add_func_int32_t_s_s(((safe_sub_func_int64_t_s_s(((l_1078 = p_48) <= p_50), (((safe_add_func_uint64_t_u_u(((safe_sub_func_uint16_t_u_u(((((!(l_1064[0][0][2] || (((p_48 , (-1L)) != (((((p_50 ^ ((**g_708) = (**g_708))) || (p_50 < p_48)) , p_48) && 4294967295UL) & 1L)) >= p_48))) != 0x7DECAC9DL) != g_185.f0) < p_50), (*l_62))) & p_50), p_48)) , (void*)0) != l_1084))) <= 0x76C79833L), g_662.f1)))), 7))), (****g_1020)));
                        l_1104[2][0] &= ((0xC49F62C6L ^ (((****g_1020) = (+0xF2L)) || (l_1078 || (p_50 > (safe_lshift_func_int8_t_s_s((((*l_1092) = p_50) > (((*l_1096) = (g_1093 , l_1094)) != l_1097)), 0)))))) , (safe_mod_func_uint16_t_u_u(((l_1103[0][0] , (*g_191)) < 0L), l_1086)));
                    }
                    if ((*g_111))
                    { /* block id: 487 */
                        return (*g_548);
                    }
                    else
                    { /* block id: 489 */
                        return (*g_548);
                    }
                }
                for (g_199 = 1; (g_199 <= 5); g_199 += 1)
                { /* block id: 495 */
                    (*g_111) = 0x565366A0L;
                }
            }
            for (l_1045 = (-24); (l_1045 > 4); l_1045 = safe_add_func_int32_t_s_s(l_1045, 8))
            { /* block id: 501 */
                (***g_373) = p_51;
                for (g_822 = 0; g_822 < 10; g_822 += 1)
                {
                    g_360[g_822] = 0L;
                }
            }
        }
        else
        { /* block id: 505 */
            uint8_t ***l_1118 = (void*)0;
            uint8_t *l_1121 = &g_287;
            uint8_t **l_1120 = &l_1121;
            uint8_t ***l_1119 = &l_1120;
            float *l_1122 = (void*)0;
            float *l_1123 = &g_703[8][4][0];
            int16_t *l_1124 = (void*)0;
            int16_t *l_1125 = (void*)0;
            int16_t *l_1126 = &g_707;
            int32_t l_1127[3];
            union U0 *l_1154 = &g_1117;
            uint64_t *l_1169 = &g_118;
            int32_t l_1183 = 0x84AFCE72L;
            int64_t ****l_1185 = (void*)0;
            union U1 *l_1194[3];
            union U1 ** const l_1193 = &l_1194[0];
            union U1 ** const *l_1192[6][2][1] = {{{&l_1193},{(void*)0}},{{(void*)0},{&l_1193}},{{(void*)0},{(void*)0}},{{&l_1193},{(void*)0}},{{(void*)0},{&l_1193}},{{(void*)0},{(void*)0}}};
            uint16_t l_1202 = 7UL;
            uint16_t *l_1239 = &l_1202;
            uint64_t **l_1255 = &l_1167;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1127[i] = 0x560EE1DAL;
            for (i = 0; i < 3; i++)
                l_1194[i] = &g_1195;
            for (g_707 = 21; (g_707 > 22); g_707++)
            { /* block id: 508 */
                return (**g_547);
            }
            if (((safe_rshift_func_int16_t_s_s(((*l_1126) = ((((*l_62) && ((*l_62) ^ p_50)) && ((((safe_add_func_int64_t_s_s(((***g_374) ^ (safe_rshift_func_uint8_t_u_u((((((*l_1123) = (((*g_463) , ((safe_lshift_func_uint16_t_u_s((((*l_62) , (((**g_708) = ((&l_1031 != (g_1117 , ((*l_1119) = (void*)0))) | p_48)) >= 0x05F7L)) ^ g_192[3]), 6)) , (void*)0)) != (void*)0)) > p_48) == 0x2.Fp+1) , p_50), 0))), 0xD3C212037C8C8BB5LL)) && 0x48540893A3986279LL) , (*g_111)) & (**g_375))) ^ p_50)), l_1127[2])) ^ p_50))
            { /* block id: 515 */
                uint8_t l_1130 = 0x94L;
                int8_t l_1158 = 1L;
                uint32_t *l_1165 = &l_60;
                uint32_t **l_1164 = &l_1165;
                uint64_t **l_1168[3][3] = {{&l_1167,&l_1167,&l_1167},{&l_1167,&l_1167,&l_1167},{&l_1167,&l_1167,&l_1167}};
                int16_t l_1172 = 0xA1A5L;
                int32_t l_1184 = 1L;
                int i, j;
                if ((safe_add_func_int64_t_s_s(l_1130, p_50)))
                { /* block id: 516 */
                    float l_1152 = 0x5.AF5D6Bp-18;
                    int32_t l_1153[10][10] = {{0xF443C01BL,0xF443C01BL,0x437030BAL,0x031A28DBL,0L,0x031A28DBL,0x437030BAL,0xF443C01BL,0xF443C01BL,0x437030BAL},{0x59EBD9A0L,0x031A28DBL,1L,1L,0x031A28DBL,0x59EBD9A0L,0x437030BAL,0x59EBD9A0L,0x031A28DBL,1L},{0L,0xF443C01BL,0L,1L,0x437030BAL,0x437030BAL,1L,0L,0xF443C01BL,0L},{0L,0x59EBD9A0L,0xF443C01BL,0x031A28DBL,0xF443C01BL,0x59EBD9A0L,0L,0L,0x59EBD9A0L,0xF443C01BL},{0L,0x031A28DBL,0x031A28DBL,0L,0L,1L,0L,0L,0x031A28DBL,0x031A28DBL},{0L,0x031A28DBL,0x437030BAL,0xF443C01BL,0xF443C01BL,0x437030BAL,0x031A28DBL,0L,0x031A28DBL,0x437030BAL},{1L,0L,0xF443C01BL,0L,1L,0x437030BAL,0x437030BAL,1L,0L,0xF443C01BL},{0L,0L,0xF443C01BL,1L,0x59EBD9A0L,1L,0xF443C01BL,0L,0L,0xF443C01BL},{0L,1L,0x437030BAL,0x437030BAL,1L,0L,0xF443C01BL,0L,1L,0x437030BAL},{0x031A28DBL,0L,0x031A28DBL,0x437030BAL,0xF443C01BL,0xF443C01BL,0x437030BAL,0x031A28DBL,0L,0x031A28DBL}};
                    int i, j;
                    for (g_233 = 0; (g_233 <= 2); g_233 += 1)
                    { /* block id: 519 */
                        int8_t *l_1140 = &g_358;
                        int32_t l_1151 = 0x42EE5BE6L;
                        if (l_1127[0])
                            break;
                        l_1061 ^= (255UL < (safe_mod_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((*l_1121)--) >= ((*l_1140) = (l_1139 != &g_786))), (safe_lshift_func_int16_t_s_s(p_48, (((251UL < (safe_div_func_int8_t_s_s((((****l_1017) = ((g_822 = (((safe_sub_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_s(p_50, ((safe_mul_func_uint8_t_u_u((((((((void*)0 != &g_709[1]) & (p_50 <= 9L)) > p_48) , (**g_547)) < p_50) && 0x1C619C3DL), l_1127[2])) != 3L))), l_1127[0])) != 0L) || l_1151)) , l_1153[6][8])) && 0xB654AB2824478DC3LL), 0x45L))) <= (*g_605)) ^ l_1151))))), (**g_708))));
                        if ((****g_373))
                            break;
                    }
                }
                else
                { /* block id: 528 */
                    uint16_t **l_1157 = &g_415[2][4];
                    g_1155 = l_1154;
                    (*g_111) &= (l_1127[2] = (((void*)0 != l_1157) > l_1158));
                }
                l_1061 &= (l_1184 |= (l_1127[2] = (!(safe_div_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u(l_1130, ((((**g_708) = ((*g_547) != ((*l_1164) = p_51))) , (safe_unary_minus_func_uint8_t_u(((**g_609) & ((l_1169 = l_1167) == ((safe_lshift_func_int8_t_s_s((&g_126 != (void*)0), (l_1172 , (safe_mod_func_int16_t_s_s((((((g_118 ^= (safe_mul_func_uint8_t_u_u((((safe_div_func_int8_t_s_s(((safe_rshift_func_int8_t_s_s((safe_mod_func_int32_t_s_s((*g_605), 4294967294UL)), 7)) & 0x2D58L), l_1127[2])) , l_1158) , 0x2BL), (-1L)))) ^ p_48) ^ p_50) ^ 4294967290UL) ^ p_48), 0x99A0L))))) , l_1167)))))) , l_1183))), 0x5F357C6DD8093778LL)))));
            }
            else
            { /* block id: 540 */
                int64_t *****l_1186 = &l_1185;
                uint64_t *l_1209[2][5] = {{&g_1210,&g_1210,&g_1210,&g_1210,&g_1210},{&g_1210,&g_1210,&g_1210,&g_1210,&g_1210}};
                int32_t l_1211 = 1L;
                float l_1218 = 0x6.518529p+11;
                int32_t l_1219[9][8][2] = {{{0xAA9318DAL,(-1L)},{0L,(-1L)},{1L,(-4L)},{0x6ED80C31L,0xAD74BA38L},{0x59304455L,(-1L)},{0x0E19EFA3L,1L},{(-9L),1L},{0x5DB5D60FL,0x5DB5D60FL}},{{0x27A2E27BL,0x6D62DA23L},{0L,0x15702C2DL},{0xDA9366A4L,(-5L)},{0x0D9DF495L,0xDA9366A4L},{0x30369919L,3L},{0x30369919L,0xDA9366A4L},{0x0D9DF495L,(-5L)},{0xDA9366A4L,0x15702C2DL}},{{0L,0x6D62DA23L},{0x27A2E27BL,0x5DB5D60FL},{0x5DB5D60FL,1L},{(-9L),1L},{0x0E19EFA3L,(-1L)},{0x59304455L,0xAD74BA38L},{0x6ED80C31L,(-4L)},{1L,(-1L)}},{{0L,(-1L)},{0xAA9318DAL,0x54B53EFDL},{(-8L),0x718DC4ECL},{(-1L),0x3F69683FL},{(-1L),0L},{(-1L),0x0C113C26L},{0x98C58543L,1L},{(-1L),0x30369919L}},{{0x15702C2DL,(-8L)},{1L,0L},{(-1L),0x98C58543L},{0x718DC4ECL,0x608546D6L},{0xBB1C2031L,0xC4D19C6EL},{0xDE1CE07DL,0x27A2E27BL},{1L,0L},{8L,0x0D9DF495L}},{{1L,(-1L)},{0L,0xF05411E8L},{1L,0xDE1CE07DL},{(-9L),0xDE1CE07DL},{1L,0xF05411E8L},{0L,(-1L)},{1L,0x0D9DF495L},{8L,0L}},{{1L,0x27A2E27BL},{0xDE1CE07DL,0xC4D19C6EL},{0xBB1C2031L,0x608546D6L},{0x718DC4ECL,0x98C58543L},{(-1L),0L},{1L,(-8L)},{0x15702C2DL,0x30369919L},{(-1L),1L}},{{0x98C58543L,0x0C113C26L},{(-1L),0L},{(-1L),0x3F69683FL},{(-1L),0x718DC4ECL},{(-8L),0xF05411E8L},{(-1L),(-1L)},{0x59304455L,3L},{0x0C113C26L,0L}},{{0x5DB5D60FL,0xBC3CFC36L},{0L,(-5L)},{(-1L),(-8L)},{0x3BFD3DD2L,(-9L)},{8L,8L},{0x6D62DA23L,0xCA8600D2L},{0x665CBF02L,(-9L)},{0L,0L}}};
                int i, j, k;
                (*l_1186) = l_1185;
                for (g_282 = 0; (g_282 > 36); g_282 = safe_add_func_int8_t_s_s(g_282, 9))
                { /* block id: 544 */
                    if ((*l_62))
                        break;
                }
                l_1219[6][6][0] &= (safe_mod_func_int32_t_s_s((l_1211 ^= ((l_1191 == (l_1192[4][1][0] = (*g_973))) & (g_1195.f0 = ((((void*)0 != l_1196[0][0]) & ((g_1199 &= ((*l_1167)--)) | (safe_div_func_int64_t_s_s((l_1202 == (safe_rshift_func_uint8_t_u_u(((l_1127[2] = ((g_1205 != ((*g_942) , l_1206[0][1][0])) , (g_1210++))) >= (safe_div_func_int32_t_s_s((safe_div_func_uint64_t_u_u(g_662.f1, 18446744073709551615UL)), (*l_62)))), 2))), l_1183)))) < l_1183)))), 4UL));
            }
            (*l_1123) = ((safe_add_func_float_f_f(((((((((*l_1255) = (((safe_rshift_func_uint16_t_u_u(65527UL, ((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_s(((safe_add_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((g_1093.f1 || (((((**g_496) , (safe_div_func_int16_t_s_s((g_1234 , (**g_708)), (safe_add_func_uint16_t_u_u(((l_1202 , (safe_mul_func_uint16_t_u_u(((*l_1239)++), ((safe_add_func_int64_t_s_s((safe_lshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((l_1252[1][0] = ((*l_62) > (((((~l_1249) || (safe_mod_func_int32_t_s_s((*g_610), 0x8BB287E3L))) != p_48) < 0x5CL) <= p_48))), 6)), 1)), g_1195.f0)) , p_50)))) > p_48), l_1127[2]))))) <= 0x6825L) , l_1253) != (void*)0)), 3)), l_1254)) == p_48), (*l_62))), p_48)) >= p_50))) <= g_606) , &l_1249)) != &l_1249) | p_50) , l_1256) >= g_360[7]) , (*g_463)) == 0xD.A09F2Fp+77), (-0x1.3p+1))) > p_48);
        }
        return (*l_62);
    }
    for (g_1195.f1 = 0; (g_1195.f1 <= 3); g_1195.f1 += 1)
    { /* block id: 565 */
        return (**g_547);
    }
    return (**g_547);
}


/* ------------------------------------------ */
/* 
 * reads : g_548 g_24.f1 g_981 g_400 g_605 g_606 g_708 g_709 g_547 g_287 g_282 g_286 g_233 g_658
 * writes: g_287 g_282 g_360 g_1012 g_659
 */
static int32_t  func_63(uint64_t  p_64)
{ /* block id: 428 */
    uint32_t l_975 = 0x3F466470L;
    union U1 l_978[7] = {{0x21C8DCCCL},{0L},{0L},{0x21C8DCCCL},{0L},{0L},{0x21C8DCCCL}};
    int32_t * const *l_979[5][8][6] = {{{(void*)0,(void*)0,&g_376,&g_376,&g_376,&g_376},{&g_376,(void*)0,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,(void*)0,&g_376,(void*)0,(void*)0},{(void*)0,&g_376,&g_376,(void*)0,&g_376,(void*)0},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{(void*)0,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,(void*)0,(void*)0},{&g_376,(void*)0,&g_376,&g_376,&g_376,&g_376}},{{&g_376,(void*)0,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{(void*)0,(void*)0,&g_376,(void*)0,&g_376,(void*)0},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,(void*)0,&g_376,&g_376,(void*)0,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,(void*)0,&g_376,&g_376}},{{&g_376,(void*)0,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,(void*)0,(void*)0,&g_376,&g_376},{&g_376,(void*)0,&g_376,&g_376,&g_376,(void*)0},{&g_376,&g_376,&g_376,(void*)0,&g_376,&g_376},{&g_376,(void*)0,(void*)0,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,(void*)0,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376}},{{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,(void*)0,&g_376},{&g_376,&g_376,&g_376,&g_376,(void*)0,&g_376},{(void*)0,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,(void*)0,&g_376,(void*)0,&g_376,&g_376},{&g_376,(void*)0,&g_376,&g_376,&g_376,&g_376},{(void*)0,&g_376,&g_376,&g_376,&g_376,&g_376},{(void*)0,&g_376,(void*)0,&g_376,&g_376,(void*)0}},{{(void*)0,(void*)0,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,(void*)0},{(void*)0,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,(void*)0,(void*)0,&g_376},{&g_376,&g_376,&g_376,&g_376,&g_376,&g_376},{&g_376,&g_376,&g_376,&g_376,(void*)0,(void*)0},{&g_376,&g_376,(void*)0,(void*)0,&g_376,(void*)0},{&g_376,&g_376,&g_376,&g_376,(void*)0,&g_376}}};
    int32_t * const **l_980 = &l_979[0][7][4];
    float l_990 = (-0x3.1p+1);
    int8_t l_998 = 0xEDL;
    uint8_t *l_999 = &g_287;
    uint8_t *l_1002 = &g_282;
    int64_t *l_1009 = &g_380;
    uint64_t l_1010[6] = {0x6F7EFD3D694ED6F3LL,18446744073709551615UL,0x6F7EFD3D694ED6F3LL,0x6F7EFD3D694ED6F3LL,18446744073709551615UL,0x6F7EFD3D694ED6F3LL};
    int8_t *l_1011 = &g_360[4];
    int i, j, k;
    l_978[3].f1 = (((((l_975 != (safe_lshift_func_int8_t_s_s((((l_978[3] , ((*l_980) = l_979[0][7][4])) == ((*g_548) , (g_981[6][1] , (void*)0))) <= (safe_lshift_func_int8_t_s_u(p_64, 6))), 2))) | (safe_mod_func_uint32_t_u_u(((((safe_mul_func_uint16_t_u_u(((p_64 > ((0UL != 0L) , p_64)) && g_400), p_64)) <= p_64) >= p_64) > l_978[3].f0), (*g_605)))) , p_64) , (*g_708)) != (*g_708));
    (*g_658) = ((+(safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u(((*l_999) |= (safe_add_func_uint64_t_u_u((l_998 | (**g_547)), 18446744073709551613UL))), ((((*l_1002) ^= p_64) | (((g_1012 = (((*l_1011) = (safe_add_func_uint16_t_u_u((safe_div_func_uint32_t_u_u((((safe_add_func_float_f_f(g_286, (p_64 < p_64))) , l_1009) != (void*)0), l_1010[3])), p_64))) == (-1L))) != p_64) == p_64)) < g_233))), p_64))) , (void*)0);
    return p_64;
}


/* ------------------------------------------ */
/* 
 * reads : g_287 g_151 g_360 g_708 g_709 g_822 g_199 g_662.f1 g_111 g_112 g_373 g_374 g_375 g_376 g_315 g_463 g_122 g_547 g_123 g_205 g_191 g_192 g_548 g_24.f1 g_2 g_118 g_606 g_609 g_610 g_6 g_973
 * writes: g_287 g_151 g_822 g_123 g_112 g_376 g_315 g_360 g_118 g_974
 */
static int16_t  func_65(uint32_t  p_66, float  p_67, const uint64_t  p_68, int8_t * p_69, union U1  p_70)
{ /* block id: 347 */
    int32_t l_817 = 0L;
    int32_t l_823[8][4][8] = {{{(-10L),(-1L),(-7L),0xCE7DCF3FL,0x98484C46L,1L,0L,0x08C92718L},{3L,(-1L),0x1340626BL,0x2A9A8F4CL,(-5L),0xD4AC649EL,0x49EA5F41L,6L},{0L,0x2E83A184L,(-10L),(-1L),(-1L),0x08C92718L,0x1340626BL,0xF2B127F7L},{2L,1L,(-1L),0x0D95CA39L,6L,0xF2B127F7L,2L,3L}},{{0x2E83A184L,(-7L),(-1L),2L,(-1L),0x334861A8L,(-1L),2L},{0x98484C46L,0xD4AC649EL,0x98484C46L,3L,3L,1L,(-5L),0xCE7DCF3FL},{0L,(-1L),0xD4AC649EL,(-1L),0x49EA5F41L,2L,3L,0xD0A76C36L},{0L,5L,1L,0x2E83A184L,3L,0xA5861883L,0x334861A8L,(-7L)}},{{0x98484C46L,0x49EA5F41L,0x08C92718L,0x4AC45276L,(-1L),1L,0xD0A76C36L,1L},{0x2E83A184L,(-1L),0x2A9A8F4CL,(-6L),6L,1L,1L,(-10L)},{2L,0L,(-5L),0xF2B127F7L,(-1L),(-1L),0xF2B127F7L,(-5L)},{0L,0L,1L,(-1L),(-5L),(-1L),1L,0L}},{{3L,(-10L),1L,0x1340626BL,0x98484C46L,0x5236C17FL,0x2E83A184L,0L},{(-10L),(-1L),(-5L),0xF3532B0CL,0L,0x689A1724L,0x68C43C91L,3L},{1L,(-6L),(-1L),0xD4AC649EL,6L,1L,0x08C92718L,(-1L)},{(-1L),1L,6L,(-1L),0x334861A8L,6L,0xF3532B0CL,1L}},{{0x68C43C91L,1L,(-1L),0L,(-1L),0L,(-1L),1L},{0x2A9A8F4CL,(-1L),3L,2L,0xF2B127F7L,6L,0x0D95CA39L,(-1L)},{3L,0L,0x49EA5F41L,(-7L),0x2A9A8F4CL,1L,0x0D95CA39L,1L},{0L,(-7L),3L,6L,(-1L),0L,(-1L),(-6L)}},{{(-1L),0L,(-1L),(-6L),0x49EA5F41L,0x334861A8L,0xF3532B0CL,0x2E83A184L},{3L,1L,6L,0L,(-10L),(-1L),0x08C92718L,0xD4AC649EL},{0x4AC45276L,0x68C43C91L,(-1L),0x5236C17FL,0x5236C17FL,(-1L),0x68C43C91L,0x4AC45276L},{(-1L),0xF2B127F7L,(-5L),0L,2L,0x08C92718L,2L,0xD0A76C36L}},{{(-7L),0xCE7DCF3FL,0x98484C46L,1L,0L,0x08C92718L,1L,0x689A1724L},{6L,0xF2B127F7L,0x0D95CA39L,1L,(-6L),(-1L),0xD4AC649EL,6L},{0x689A1724L,0x68C43C91L,3L,0x1340626BL,(-1L),(-1L),1L,(-1L)},{0L,1L,0L,1L,1L,0x334861A8L,(-1L),(-1L)}},{{0xCE7DCF3FL,0L,0xD0A76C36L,(-5L),0xD0A76C36L,0L,0xCE7DCF3FL,1L},{0x1340626BL,(-7L),1L,0x08C92718L,(-1L),1L,0x2E83A184L,6L},{0x08C92718L,0L,(-7L),0x0D95CA39L,(-1L),6L,3L,(-5L)},{0x1340626BL,(-1L),(-5L),6L,0xD0A76C36L,0L,(-7L),0xF2B127F7L}}};
    int32_t l_825[9];
    uint32_t *l_877 = &g_150;
    uint32_t **l_876 = &l_877;
    uint32_t l_894 = 1UL;
    float l_908 = 0x1.BEDDE1p+58;
    uint64_t l_911 = 0xE33ECF2B190BF63BLL;
    int32_t *l_934 = &l_825[6];
    uint32_t l_946 = 0xD0670113L;
    uint8_t *l_952 = &g_287;
    uint8_t **l_951 = &l_952;
    int32_t l_971 = 0x764864EFL;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_825[i] = (-1L);
    for (g_287 = 0; (g_287 <= 4); g_287 += 1)
    { /* block id: 350 */
        union U0 **l_824 = (void*)0;
        uint32_t l_840 = 4294967292UL;
        uint8_t *l_853 = &g_287;
        uint8_t * const *l_852[5][4][1];
        union U1 ***l_878[9][8][2] = {{{&g_645,(void*)0},{&g_645,&g_645},{&g_645,(void*)0},{&g_645,(void*)0},{&g_645,&g_645},{(void*)0,&g_645},{&g_645,(void*)0},{&g_645,&g_645}},{{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{(void*)0,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,(void*)0}},{{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645}},{{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,(void*)0},{&g_645,&g_645}},{{&g_645,&g_645},{&g_645,(void*)0},{&g_645,&g_645},{(void*)0,(void*)0},{(void*)0,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645}},{{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{(void*)0,&g_645},{(void*)0,(void*)0},{&g_645,&g_645},{&g_645,&g_645},{(void*)0,&g_645}},{{(void*)0,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645}},{{(void*)0,&g_645},{(void*)0,&g_645},{&g_645,&g_645},{&g_645,(void*)0},{(void*)0,&g_645},{(void*)0,&g_645},{&g_645,&g_645},{&g_645,&g_645}},{{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{&g_645,&g_645},{(void*)0,(void*)0},{(void*)0,&g_645},{&g_645,(void*)0},{&g_645,&g_645}}};
        int32_t l_881 = (-1L);
        int32_t l_882 = 0xEDF19996L;
        int32_t l_884 = 0x75FB5087L;
        int32_t l_886 = 0xEC4BCCA5L;
        int32_t l_889 = 0xCDDF4882L;
        int32_t l_891 = (-4L);
        int32_t l_892 = 1L;
        int32_t l_893 = 6L;
        int32_t ****l_905 = (void*)0;
        int64_t *l_906[2][10] = {{&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5]},{&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5],&g_192[5]}};
        float *l_930 = &l_908;
        union U1 ***l_972 = &g_645;
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 4; j++)
            {
                for (k = 0; k < 1; k++)
                    l_852[i][j][k] = &l_853;
            }
        }
        if (p_70.f0)
        { /* block id: 351 */
            uint8_t l_807 = 0UL;
            union U0 ***l_818 = &g_787;
            int32_t l_842 = 0L;
            int32_t l_883 = (-6L);
            int32_t l_885 = 0L;
            int32_t l_887 = (-7L);
            int32_t l_888 = 0xF092D07BL;
            int32_t l_890[2];
            int64_t *l_907[4][9] = {{&g_380,(void*)0,&g_380,&g_192[2],&g_192[0],&g_192[2],&g_380,(void*)0,&g_380},{&g_192[0],&g_400,&g_192[0],&g_192[0],&g_400,&g_192[0],&g_192[0],&g_400,&g_192[0]},{&g_380,(void*)0,&g_380,&g_192[2],&g_192[0],&g_192[2],&g_380,(void*)0,&g_380},{&g_192[0],&g_400,&g_192[0],&g_192[0],&g_400,&g_192[0],&g_192[0],&g_400,&g_192[0]}};
            int64_t l_909[6][10][4] = {{{0x5B0CA09141C490A8LL,0x26DC1486712F4505LL,0L,0x8B192625CD788AC8LL},{0x2E67334FAF37F4BELL,0xAD3B5CD8708EDF05LL,(-8L),0x5D9C1C13D25D4D34LL},{0x344C8B519B07498FLL,0xD4518A111E94574FLL,0xD4518A111E94574FLL,0x344C8B519B07498FLL},{0x16775F8C1898CAE2LL,(-6L),0x8D7B25C4239C5DD2LL,0xBF57353E8AD242C2LL},{(-5L),0xA04CC7D8E144D9D3LL,0xEE6FF3725ED62244LL,0x8CFC079937FC95C3LL},{0x26DC1486712F4505LL,0x5B0CA09141C490A8LL,0x2D6250EA5547E0A3LL,0x8CFC079937FC95C3LL},{(-1L),0xA04CC7D8E144D9D3LL,0x85F4537E6A31DF53LL,0xBF57353E8AD242C2LL},{0x9705C1ABD89AAD3CLL,(-6L),0x2E67334FAF37F4BELL,0L},{0xB385C4E39D119D59LL,0x7A58D26AF3B1D5E0LL,0x8CA2BA79291A46F2LL,(-1L)},{(-1L),0x5B0CA09141C490A8LL,(-1L),0x18FFB771294729F8LL}},{{0xBF57353E8AD242C2LL,0xD4518A111E94574FLL,0xCDD725661B70AC3ALL,(-2L)},{(-1L),0x974EC2D5F1B263ADLL,1L,0x85F4537E6A31DF53LL},{0xEE6FF3725ED62244LL,0xC7C1E3D6BBF0E30ALL,(-1L),0xC98DD7AF8929F406LL},{0xB385C4E39D119D59LL,0xCB9279FE656AC421LL,0xA04CC7D8E144D9D3LL,0xC7C1E3D6BBF0E30ALL},{0xC7C1E3D6BBF0E30ALL,0x59D620A27EF2C3DDLL,(-1L),0L},{0x6B02B857B58C32E0LL,0xBF57353E8AD242C2LL,(-1L),(-1L)},{0L,0x5B0CA09141C490A8LL,(-3L),0xC4370091EE27BF2CLL},{(-1L),(-1L),0x0316ED8FDAC67FCFLL,0x83E4A694EFEFB57FLL},{0L,0xCB9279FE656AC421LL,(-1L),(-1L)},{0xC4370091EE27BF2CLL,0xD4AB48D75013604BLL,(-7L),0x3AAB93F170C03673LL}},{{0xB385C4E39D119D59LL,0x6B02B857B58C32E0LL,0xE234C9A3C956915BLL,(-2L)},{(-10L),0x16775F8C1898CAE2LL,0x92795ECA8DF06BD0LL,(-1L)},{0xBCF37511C413BE45LL,0x2072513CEE86A8B4LL,0xECDF67DE2758AD3CLL,0x8D7B25C4239C5DD2LL},{0xD4518A111E94574FLL,0x7A58D26AF3B1D5E0LL,(-1L),0xC4370091EE27BF2CLL},{0x7A58D26AF3B1D5E0LL,0x16775F8C1898CAE2LL,0xE50CDB01FCF0345ALL,8L},{0xBF57353E8AD242C2LL,0xB385C4E39D119D59LL,0x9705C1ABD89AAD3CLL,0x3AAB93F170C03673LL},{0L,0x5D01AA58520A1221LL,0xC98DD7AF8929F406LL,0x85F4537E6A31DF53LL},{(-1L),0xCB9279FE656AC421LL,0xE234C9A3C956915BLL,(-10L)},{0x2D6250EA5547E0A3LL,0xC4370091EE27BF2CLL,0xA04CC7D8E144D9D3LL,0xC4370091EE27BF2CLL},{0xCB9279FE656AC421LL,5L,0x8CA2BA79291A46F2LL,0x4912A33E218FB0D0LL}},{{0x2A481733E9E9440ALL,0xBF57353E8AD242C2LL,0x7A58D26AF3B1D5E0LL,0x18FFB771294729F8LL},{(-1L),0x16775F8C1898CAE2LL,0xF5CD07C9DBC93E4FLL,0xC7C1E3D6BBF0E30ALL},{(-1L),0x0316ED8FDAC67FCFLL,0x7A58D26AF3B1D5E0LL,0xE82623400F1E4E8ELL},{0x2A481733E9E9440ALL,0xC7C1E3D6BBF0E30ALL,0x8CA2BA79291A46F2LL,(-1L)},{0xCB9279FE656AC421LL,0xC5EE986D1804C2D1LL,0xA04CC7D8E144D9D3LL,(-2L)},{0x2D6250EA5547E0A3LL,0x2A481733E9E9440ALL,0xE234C9A3C956915BLL,0L},{(-1L),0x5B0CA09141C490A8LL,0xC98DD7AF8929F406LL,0xE234C9A3C956915BLL},{0L,0x2072513CEE86A8B4LL,0x9705C1ABD89AAD3CLL,0L},{0xBF57353E8AD242C2LL,(-1L),0xE50CDB01FCF0345ALL,0xC7C1E3D6BBF0E30ALL},{0x7A58D26AF3B1D5E0LL,0xC5EE986D1804C2D1LL,(-1L),0x8CA2BA79291A46F2LL}},{{0xD4518A111E94574FLL,0x2D6250EA5547E0A3LL,0xECDF67DE2758AD3CLL,0x3AAB93F170C03673LL},{0xBCF37511C413BE45LL,0x974EC2D5F1B263ADLL,0x92795ECA8DF06BD0LL,0xC7C1E3D6BBF0E30ALL},{(-10L),0xC4370091EE27BF2CLL,0xE234C9A3C956915BLL,0xEE6FF3725ED62244LL},{0xB385C4E39D119D59LL,0x2072513CEE86A8B4LL,(-7L),(-1L)},{0xC4370091EE27BF2CLL,5L,(-1L),0L},{0L,0xD4518A111E94574FLL,0x0316ED8FDAC67FCFLL,8L},{(-1L),0xC5EE986D1804C2D1LL,(-3L),0x85F4537E6A31DF53LL},{0L,(-8L),(-1L),0xE82623400F1E4E8ELL},{0x6B02B857B58C32E0LL,0xCB9279FE656AC421LL,(-1L),(-8L)},{0xC7C1E3D6BBF0E30ALL,0x16775F8C1898CAE2LL,0xA04CC7D8E144D9D3LL,0L}},{{0xB385C4E39D119D59LL,0L,(-1L),0x4912A33E218FB0D0LL},{0xEE6FF3725ED62244LL,0x5B0CA09141C490A8LL,1L,(-1L)},{(-1L),0xC4370091EE27BF2CLL,0xCDD725661B70AC3ALL,0x83E4A694EFEFB57FLL},{0xBF57353E8AD242C2LL,0x0316ED8FDAC67FCFLL,(-1L),0x85F4537E6A31DF53LL},{(-1L),0xD4AB48D75013604BLL,0x8CA2BA79291A46F2LL,0x8CA2BA79291A46F2LL},{0xB385C4E39D119D59LL,5L,0xBCF37511C413BE45LL,0xEBBC9390ECFBB592LL},{0x8CA2BA79291A46F2LL,0x74268B01963A4061LL,0x20662C9A146816EBLL,1L},{1L,(-10L),5L,0x20662C9A146816EBLL},{0x7A58D26AF3B1D5E0LL,(-10L),0xD4AB48D75013604BLL,1L},{(-10L),0x74268B01963A4061LL,0x26DC1486712F4505LL,0xEBBC9390ECFBB592LL}}};
            float *l_933 = &g_122;
            const union U1 *l_939 = &g_940;
            uint8_t **l_953 = (void*)0;
            uint32_t l_970 = 0xF9E03D8AL;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_890[i] = 0xBC9BE682L;
            for (g_151 = 2; (g_151 <= 9); g_151 += 1)
            { /* block id: 354 */
                union U1 l_800 = {0L};
                const int16_t *l_812 = &g_123;
                const int16_t **l_813 = &l_812;
                int32_t *l_819 = &l_800.f1;
                int32_t *l_820 = (void*)0;
                int32_t *l_821[9][2] = {{&g_2,&g_822},{&g_822,&g_2},{&g_822,&g_822},{&g_2,&g_822},{&g_822,&g_2},{&g_822,&g_822},{&g_2,&g_822},{&g_822,&g_2},{&g_822,&g_822}};
                int32_t l_832 = 0x2C2D4BF0L;
                int8_t l_836[4];
                uint8_t *l_850 = (void*)0;
                uint8_t **l_849 = &l_850;
                uint8_t l_866[10][10][2] = {{{255UL,1UL},{255UL,1UL},{0xE6L,0xFDL},{255UL,0xF5L},{0x3BL,3UL},{0x15L,3UL},{0x3BL,0xF5L},{255UL,0xFDL},{0xE6L,1UL},{255UL,1UL}},{{255UL,0x4DL},{0x4CL,248UL},{3UL,255UL},{1UL,7UL},{1UL,255UL},{3UL,248UL},{0x4CL,0x4DL},{255UL,1UL},{255UL,1UL},{0xE6L,0xFDL}},{{255UL,0xF5L},{0x3BL,3UL},{0x15L,3UL},{0x3BL,0xF5L},{255UL,0xFDL},{0xE6L,1UL},{255UL,1UL},{255UL,0x4DL},{0x4CL,248UL},{3UL,255UL}},{{1UL,7UL},{1UL,255UL},{3UL,248UL},{0x4CL,0x4DL},{255UL,1UL},{255UL,1UL},{0xE6L,0xFDL},{255UL,0xF5L},{0x3BL,3UL},{0x15L,3UL}},{{0x3BL,0xF5L},{255UL,0xFDL},{0xE6L,1UL},{255UL,1UL},{255UL,0x4DL},{0x4CL,248UL},{3UL,255UL},{1UL,7UL},{1UL,255UL},{3UL,248UL}},{{0x4CL,0x4DL},{255UL,1UL},{255UL,1UL},{0xE6L,0xFDL},{255UL,0xF5L},{0x3BL,3UL},{0x15L,3UL},{0x3BL,0xF5L},{255UL,0xFDL},{0xE6L,1UL}},{{255UL,1UL},{255UL,0x4DL},{0x4CL,248UL},{3UL,255UL},{1UL,7UL},{1UL,255UL},{3UL,248UL},{0x4CL,0x4DL},{255UL,1UL},{255UL,1UL}},{{0xE6L,0xFDL},{255UL,0xF5L},{0x3BL,3UL},{0x15L,3UL},{0x3BL,0xF5L},{255UL,0xFDL},{0xE6L,1UL},{0xE6L,252UL},{0UL,3UL},{0x07L,1UL}},{{0x4CL,0x81L},{0x15L,248UL},{0x15L,0x81L},{0x4CL,1UL},{0x07L,3UL},{0UL,252UL},{0xE6L,0xAAL},{1UL,0x16L},{255UL,0xFDL},{0UL,7UL}},{{255UL,7UL},{0UL,0xFDL},{255UL,0x16L},{1UL,0xAAL},{0xE6L,252UL},{0UL,3UL},{0x07L,1UL},{0x4CL,0x81L},{0x15L,248UL},{0x15L,0x81L}}};
                uint32_t * const *l_899 = &l_877;
                int i, j, k;
                for (i = 0; i < 4; i++)
                    l_836[i] = 0xB6L;
                if ((((g_360[g_151] & (((p_70 , (safe_div_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u(p_70.f0, (((safe_rshift_func_int8_t_s_s((l_800 , ((((l_823[2][2][5] ^= ((*l_819) = (safe_sub_func_int8_t_s_s((l_817 = (safe_add_func_uint16_t_u_u((safe_div_func_int8_t_s_s(l_807, (safe_lshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u(246UL, ((*g_708) != ((*l_813) = l_812)))), (+(safe_add_func_uint8_t_u_u(((((l_817 == 1UL) , (void*)0) == l_818) , 7UL), l_817))))))), p_68))), (*p_69))))) == p_66) < p_68) & p_66)), 7)) < p_68) | p_66))) , l_824) != (void*)0), l_825[6])), 0xB800F82D5BDF3FFDLL))) != l_825[8]) && (*p_69))) || p_70.f0) >= p_66))
                { /* block id: 359 */
                    int32_t l_841[1];
                    int32_t l_879 = 0x4E29A6B3L;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_841[i] = 0xCCB5976CL;
                    for (g_822 = 4; (g_822 >= 0); g_822 -= 1)
                    { /* block id: 362 */
                        uint8_t *l_833 = &l_807;
                        int32_t l_839 = 0xAE91CDE6L;
                        uint8_t ***l_851 = &l_849;
                        uint8_t * const **l_854 = &l_852[0][3][0];
                        uint32_t *l_859[7] = {(void*)0,&g_315,&g_315,(void*)0,&g_315,&g_315,(void*)0};
                        float *l_880[6] = {(void*)0,&g_122,(void*)0,(void*)0,&g_122,(void*)0};
                        int i;
                        (*g_111) &= ((safe_sub_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_s((((*l_819) |= ((((safe_mul_func_int8_t_s_s(l_832, ((*l_833) = p_66))) , ((7L | (safe_sub_func_uint64_t_u_u(g_199, p_66))) & ((((((**g_708) = (l_836[2] < (g_662.f1 , 0x74L))) && (safe_lshift_func_uint8_t_u_u(l_839, l_817))) <= p_70.f0) ^ l_840) | 0xCA6D6F59L))) | (-1L)) , 0x2C65C684L)) , 0xFFL), (*p_69))), l_841[0])) > l_842);
                        (*l_819) = (safe_mod_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(0xDE223A96L, ((*p_69) == ((((*l_851) = l_849) != ((*l_854) = l_852[2][0][0])) <= l_807)))), 13)), (safe_add_func_int64_t_s_s(p_66, 18446744073709551615UL))));
                        (***g_373) = (***g_373);
                        (*l_819) |= (l_842 = ((safe_div_func_uint32_t_u_u(((((++g_315) , ((safe_add_func_int32_t_s_s(((safe_mul_func_float_f_f((*g_463), (p_67 = (((l_866[3][8][1] ^ 65528UL) && (safe_div_func_int32_t_s_s((safe_sub_func_int64_t_s_s((safe_rshift_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u(p_66, ((!(l_841[0] = ((*p_69) = (g_547 == l_876)))) & (((l_840 , l_878[7][4][0]) != &g_645) >= p_66)))), l_823[2][2][5])), l_879)), 0x80EFA26EL))) , p_66)))) , p_68), p_70.f0)) != (**g_708))) < 0x555527846603C3FFLL) | 0xFDL), 1L)) <= 9UL));
                    }
                }
                else
                { /* block id: 378 */
                    l_881 |= p_70.f0;
                }
                ++l_894;
                if ((safe_add_func_int32_t_s_s(((((*p_69) = ((l_899 != ((((((((((**g_205) || 0x47D710FD62865519LL) != ((**g_547) != ((l_894 ^ 0x2FL) ^ (!(l_823[6][0][5] <= (safe_div_func_int8_t_s_s(((void*)0 != l_905), 0xE6L))))))) & p_70.f0) , l_906[1][7]) == l_907[0][5]) >= p_70.f0) >= (*p_69)) < l_817) , (void*)0)) || 0UL)) || l_909[3][2][2]) >= g_822), 0x6F79EDBAL)))
                { /* block id: 383 */
                    for (l_807 = 0; (l_807 <= 3); l_807 += 1)
                    { /* block id: 386 */
                        (*g_111) |= ((p_68 && l_887) ^ (&g_709[0] == &g_709[1]));
                    }
                }
                else
                { /* block id: 389 */
                    int32_t l_910 = 0x390F7A29L;
                    union U0 *l_916 = (void*)0;
                    int32_t l_921 = 2L;
                    float l_925 = 0x6.E04501p-85;
                    int32_t *l_926 = (void*)0;
                    l_911++;
                    if ((0L & (((safe_rshift_func_uint16_t_u_s(0x3828L, 11)) , l_916) != (void*)0)))
                    { /* block id: 391 */
                        (*g_111) = (safe_add_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(l_921, 2)), (safe_unary_minus_func_uint64_t_u(p_68))));
                        (*l_819) = (safe_rshift_func_uint8_t_u_u(l_887, ((void*)0 != &l_849)));
                        l_926 = (void*)0;
                    }
                    else
                    { /* block id: 395 */
                        uint64_t l_927 = 18446744073709551610UL;
                        --l_927;
                        l_916 = l_916;
                        if ((***g_374))
                            break;
                    }
                }
                for (g_118 = 0; (g_118 <= 3); g_118 += 1)
                { /* block id: 403 */
                    float **l_931 = &l_930;
                    float **l_932[2];
                    int32_t l_935[3][4] = {{0xB6754D37L,0L,0xEC68C381L,0xEC68C381L},{0L,0L,0xB6754D37L,0xEC68C381L},{0L,0L,0L,0xB6754D37L}};
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_932[i] = (void*)0;
                    if ((((*l_931) = l_930) == (l_933 = l_821[5][1])))
                    { /* block id: 406 */
                        l_934 = (void*)0;
                        return (**g_708);
                    }
                    else
                    { /* block id: 409 */
                        uint8_t l_936 = 0x13L;
                        int32_t l_944 = (-3L);
                        int32_t l_945[4] = {0xCFFBDBAAL,0xCFFBDBAAL,0xCFFBDBAAL,0xCFFBDBAAL};
                        int i;
                        l_936++;
                        l_939 = l_939;
                        (***g_373) = &l_935[1][0];
                        l_946++;
                    }
                    return l_842;
                }
            }
            l_971 = (((l_951 == l_953) >= ((((*p_69) |= (((0xB01774937D3EB086LL | l_887) < ((safe_mod_func_int16_t_s_s(p_70.f0, (safe_add_func_uint64_t_u_u(((safe_mod_func_uint64_t_u_u((0x940EL < (((safe_mul_func_uint16_t_u_u(l_807, (safe_mul_func_uint8_t_u_u(p_66, (safe_mod_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((*l_934), p_68)), 0x8ABBL)))))) <= l_890[0]) >= 0x2CD8L)), g_606)) < 4294967295UL), 0x5856A071C912AFFCLL)))) > (**g_609))) != l_970)) , l_842) < l_881)) > p_70.f0);
        }
        else
        { /* block id: 420 */
            (**g_374) = &l_817;
            (*g_973) = l_972;
            return p_70.f0;
        }
        if ((*g_610))
            break;
    }
    return (**g_708);
}


/* ------------------------------------------ */
/* 
 * reads : g_150 g_123 g_111 g_112 g_24.f1 g_2 g_606
 * writes: g_150 g_233 g_112
 */
static uint8_t  func_75(int32_t * p_76, int32_t * p_77, int32_t  p_78)
{ /* block id: 61 */
    int32_t l_230[10] = {1L,1L,1L,1L,1L,1L,1L,1L,1L,1L};
    int32_t *l_234 = &g_6;
    uint32_t *l_237 = &g_233;
    uint32_t **l_236 = &l_237;
    float l_262[2][2][10] = {{{0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70,0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70},{0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70,0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70}},{{0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70,0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70},{0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70,0xF.16F3B5p+70,0xB.3F817Ep-44,0x1.Cp-1,0xB.3F817Ep-44,0xF.16F3B5p+70}}};
    int32_t ** const l_270[1][7][6] = {{{&l_234,&l_234,&l_234,&l_234,&l_234,&l_234},{&l_234,(void*)0,&l_234,(void*)0,&l_234,&l_234},{&l_234,&l_234,(void*)0,&l_234,&l_234,&l_234},{&l_234,&l_234,&l_234,&l_234,&l_234,&l_234},{&l_234,&l_234,(void*)0,&l_234,&l_234,&l_234},{&l_234,&l_234,&l_234,(void*)0,&l_234,&l_234},{(void*)0,&l_234,&l_234,&l_234,&l_234,&l_234}}};
    int32_t ** const *l_269 = &l_270[0][6][5];
    int32_t ** const **l_268 = &l_269;
    union U1 l_312 = {0xC57B5C6CL};
    int8_t l_362 = (-5L);
    int64_t l_390 = 0xA9C6B7E5617E04D5LL;
    int16_t l_402 = 0x336FL;
    uint64_t *l_419 = &g_118;
    uint16_t l_420[2];
    int32_t l_436[6][6][7] = {{{(-6L),8L,0x3CCF125EL,0xF476E670L,5L,1L,(-7L)},{0x51597FA7L,1L,0x9119DF86L,(-1L),0x9119DF86L,1L,0x51597FA7L},{(-7L),1L,5L,0xF476E670L,0x3CCF125EL,8L,(-6L)},{0xF97191F8L,0x6187F734L,0x51597FA7L,0x6187F734L,0xBCD65C0AL,9L,0x482ACB09L},{(-7L),(-6L),9L,1L,0x4D148385L,(-7L),(-7L)},{0xD003DA39L,1L,8L,1L,0xD003DA39L,4L,(-6L)}},{{0xF476E670L,(-6L),(-7L),8L,(-8L),(-7L),(-8L)},{0xF97191F8L,0L,0xBCD65C0AL,(-1L),0x871B90EEL,0x08E09314L,0x7D53FEE8L},{0xF476E670L,8L,0x4D148385L,0x3CCF125EL,0xFBAF2102L,0xFBAF2102L,0x3CCF125EL},{0xD003DA39L,0x4B7BAED7L,0xD003DA39L,0x08E09314L,0x0D826CF2L,0x6187F734L,0x7D53FEE8L},{(-7L),5L,(-8L),0x4D148385L,(-8L),0x4D148385L,(-8L)},{0x0D826CF2L,9L,0x871B90EEL,(-2L),0x9119DF86L,0x6187F734L,(-6L)}},{{1L,(-7L),0xFBAF2102L,(-6L),(-6L),0xFBAF2102L,(-7L)},{0xBCD65C0AL,4L,0x0D826CF2L,0L,0x9119DF86L,0x08E09314L,0x482ACB09L},{0xFBAF2102L,(-7L),(-8L),(-7L),(-8L),(-7L),0xFBAF2102L},{0x482ACB09L,0x08E09314L,0x9119DF86L,0L,0x0D826CF2L,4L,0xBCD65C0AL},{(-7L),0xFBAF2102L,(-6L),(-6L),0xFBAF2102L,(-7L),1L},{(-6L),0x6187F734L,0x9119DF86L,(-2L),0x871B90EEL,9L,0x0D826CF2L}},{{(-8L),0x4D148385L,(-8L),0x4D148385L,(-8L),5L,(-7L)},{0x7D53FEE8L,0x6187F734L,0x0D826CF2L,0x08E09314L,0xD003DA39L,0x4B7BAED7L,0xD003DA39L},{0x3CCF125EL,0xFBAF2102L,0xFBAF2102L,0x3CCF125EL,0x4D148385L,8L,0xF476E670L},{0x7D53FEE8L,0x08E09314L,0x871B90EEL,(-1L),0xBCD65C0AL,0L,0xF97191F8L},{(-8L),(-7L),(-8L),8L,(-7L),(-6L),0xF476E670L},{(-6L),4L,0xD003DA39L,1L,8L,1L,0xD003DA39L}},{{(-7L),(-7L),0x4D148385L,1L,9L,(-6L),(-7L)},{0x482ACB09L,9L,0xBCD65C0AL,0x6187F734L,0x51597FA7L,0L,0x0D826CF2L},{0xFBAF2102L,5L,(-7L),(-8L),9L,8L,1L},{0xBCD65C0AL,0x4B7BAED7L,8L,9L,8L,0x4B7BAED7L,0xBCD65C0AL},{1L,8L,9L,(-8L),(-7L),5L,0xFBAF2102L},{0x0D826CF2L,0L,0x51597FA7L,0x6187F734L,0xBCD65C0AL,9L,0x482ACB09L}},{{(-7L),(-6L),9L,1L,(-7L),0x4D148385L,0x4D148385L},{0x51597FA7L,0x4B7BAED7L,0x7D53FEE8L,0x4B7BAED7L,0x51597FA7L,0x49696558L,0x482ACB09L},{(-8L),0xFBAF2102L,0x4D148385L,5L,(-6L),1L,(-6L)},{0x0D826CF2L,(-1L),0xF97191F8L,9L,(-6L),4L,0xD003DA39L},{(-8L),5L,(-7L),(-7L),0x3CCF125EL,0x3CCF125EL,(-7L)},{0x51597FA7L,(-2L),0x51597FA7L,4L,0x871B90EEL,0L,0xD003DA39L}}};
    union U1 l_546 = {0xF0E4A2BBL};
    int32_t l_562 = (-8L);
    uint8_t *l_567 = &g_282;
    uint32_t l_570 = 0x56EB4B52L;
    uint16_t ****l_618 = &g_413;
    int64_t l_625[2];
    uint16_t *l_633 = &l_420[1];
    uint16_t ** const l_632 = &l_633;
    uint16_t ** const *l_631[2];
    int16_t * const l_706 = &g_707;
    int16_t * const *l_705 = &l_706;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_420[i] = 65535UL;
    for (i = 0; i < 2; i++)
        l_625[i] = (-6L);
    for (i = 0; i < 2; i++)
        l_631[i] = &l_632;
    for (g_150 = (-15); (g_150 > 36); g_150 = safe_add_func_uint16_t_u_u(g_150, 5))
    { /* block id: 64 */
        int16_t *l_222 = (void*)0;
        int16_t *l_223[6][4][3] = {{{&g_123,&g_123,&g_123},{(void*)0,&g_123,(void*)0},{&g_123,&g_123,&g_123},{&g_123,&g_123,(void*)0}},{{&g_123,&g_123,&g_123},{&g_123,&g_123,&g_123},{&g_123,(void*)0,&g_123},{&g_123,&g_123,&g_123}},{{&g_123,(void*)0,&g_123},{(void*)0,(void*)0,&g_123},{&g_123,&g_123,(void*)0},{&g_123,(void*)0,&g_123}},{{&g_123,&g_123,&g_123},{&g_123,&g_123,&g_123},{&g_123,&g_123,&g_123},{&g_123,&g_123,&g_123}},{{&g_123,&g_123,&g_123},{&g_123,&g_123,(void*)0},{&g_123,&g_123,&g_123},{&g_123,&g_123,(void*)0}},{{(void*)0,&g_123,&g_123},{(void*)0,(void*)0,&g_123},{&g_123,&g_123,&g_123},{(void*)0,(void*)0,&g_123}}};
        int16_t l_224 = 0xE892L;
        int64_t *l_231 = (void*)0;
        uint32_t *l_232 = &g_233;
        int32_t **l_235 = &l_234;
        int i, j, k;
        (*l_235) = (((*l_232) = (safe_mod_func_int8_t_s_s((((((((((l_224 = g_123) | ((*g_111) == 4294967295UL)) > p_78) , ((safe_sub_func_uint8_t_u_u(0x6DL, (p_78 ^ (+(safe_div_func_uint16_t_u_u(p_78, (l_230[4] , (((((-5L) > p_78) >= l_224) <= 0x75D6BB65L) , 65535UL)))))))) , (void*)0)) == l_231) | g_24.f1) < (*p_76)) || 0x9741CF09D96FA83DLL) || 0L), g_24.f1))) , l_234);
        l_236 = &l_232;
    }
    for (g_150 = 0; (g_150 <= 9); g_150 += 1)
    { /* block id: 72 */
        int64_t ***l_240 = &g_206[0][6][0];
        int64_t ****l_241 = &l_240;
        int16_t *l_258 = &g_123;
        uint64_t *l_259 = &g_118;
        int32_t l_272 = 0x7778F09FL;
        uint64_t l_283 = 18446744073709551612UL;
        int32_t l_316 = 0xF84B9FB9L;
        int32_t l_317 = 0L;
        int32_t *l_322 = &l_317;
        float l_325 = 0x3.53BA40p-4;
        uint8_t l_388 = 253UL;
        int32_t l_401[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        uint16_t l_403 = 0xB632L;
        uint32_t l_458 = 0xC2172027L;
        uint8_t *l_542 = &g_282;
        uint8_t **l_541 = &l_542;
        union U1 l_544 = {0x57E9432AL};
        uint32_t l_555 = 4294967292UL;
        int16_t l_557 = (-2L);
        uint16_t ****l_620 = &g_413;
        uint16_t ****l_622 = (void*)0;
        union U1 **l_648 = (void*)0;
        uint8_t *l_672[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_733 = 0x706469E8L;
        float l_751 = 0x9.B44458p-71;
        uint8_t l_766[1];
        int i;
        for (i = 0; i < 1; i++)
            l_766[i] = 250UL;
    }
    (*g_111) = (*p_76);
    (*g_111) = (*p_77);
    return g_606;
}


/* ------------------------------------------ */
/* 
 * reads : g_24.f1 g_110 g_113 g_6 g_112 g_24.f0 g_23 g_123 g_126 g_127 g_118 g_150 g_111 g_185 g_191 g_151 g_192 g_199 g_210
 * writes: g_118 g_122 g_123 g_126 g_150 g_151 g_112 g_191 g_199 g_205 g_206 g_192
 */
static int32_t * func_79(uint16_t  p_80)
{ /* block id: 22 */
    union U1 l_95 = {0x7F45EA77L};
    int32_t *l_97 = &g_6;
    int32_t *l_99[3];
    int32_t **l_98 = &l_99[1];
    int32_t **l_109 = &l_97;
    int32_t ***l_108 = &l_109;
    uint8_t l_116 = 0xF0L;
    uint64_t l_117[7][5][4] = {{{0x05481B40075A6259LL,0x012C46B23711835ELL,18446744073709551615UL,0x0E1577BE735B57BDLL},{0xF66FFE49794E5485LL,0x8BB49DA01FE02729LL,0UL,18446744073709551611UL},{2UL,0x625AE403142C01D2LL,0x05481B40075A6259LL,5UL},{0xCEB12FA99DB6AB31LL,2UL,0x625AE403142C01D2LL,0x625AE403142C01D2LL},{0x81C3794E43DE26F3LL,0x81C3794E43DE26F3LL,1UL,0xF66FFE49794E5485LL}},{{0x79906C8188D8CC67LL,18446744073709551608UL,0x05481B40075A6259LL,2UL},{18446744073709551615UL,1UL,0x3B645C325D181DD9LL,0x05481B40075A6259LL},{0xF66FFE49794E5485LL,1UL,0x0E1E3E4B84AAFB9ALL,2UL},{1UL,18446744073709551608UL,0xCEB12FA99DB6AB31LL,0xF66FFE49794E5485LL},{0x625AE403142C01D2LL,0x81C3794E43DE26F3LL,0x7DF68D3435D61FA6LL,0x625AE403142C01D2LL}},{{0x012C46B23711835ELL,2UL,18446744073709551614UL,5UL},{1UL,0x625AE403142C01D2LL,18446744073709551615UL,18446744073709551611UL},{0x3B645C325D181DD9LL,0x8BB49DA01FE02729LL,0x3B645C325D181DD9LL,0x0E1577BE735B57BDLL},{2UL,0x012C46B23711835ELL,0xA164AA8A31C558FELL,5UL},{0x79906C8188D8CC67LL,18446744073709551608UL,0x625AE403142C01D2LL,0x012C46B23711835ELL}},{{0x7DF68D3435D61FA6LL,0x81C3794E43DE26F3LL,0x625AE403142C01D2LL,0x3B645C325D181DD9LL},{0x79906C8188D8CC67LL,0x075E4EBAA9A2C06FLL,0xA164AA8A31C558FELL,2UL},{2UL,18446744073709551615UL,18446744073709551609UL,0x717136C1A4EABE0FLL},{18446744073709551609UL,0x717136C1A4EABE0FLL,18446744073709551614UL,1UL},{0x717136C1A4EABE0FLL,18446744073709551615UL,0xF66FFE49794E5485LL,18446744073709551608UL}},{{0x7DF68D3435D61FA6LL,0x3A54A15197363D2FLL,0x3A54A15197363D2FLL,0x7DF68D3435D61FA6LL},{9UL,0x625AE403142C01D2LL,0UL,0x608F1A9DB3BFA596LL},{0x717136C1A4EABE0FLL,0x7DF68D3435D61FA6LL,0x79906C8188D8CC67LL,0x05481B40075A6259LL},{18446744073709551608UL,0xC362A02F93315705LL,18446744073709551609UL,0x05481B40075A6259LL},{18446744073709551614UL,0x7DF68D3435D61FA6LL,18446744073709551615UL,0x608F1A9DB3BFA596LL}},{{0x3B645C325D181DD9LL,0x625AE403142C01D2LL,0x81C3794E43DE26F3LL,0x7DF68D3435D61FA6LL},{0x0E1577BE735B57BDLL,0x3A54A15197363D2FLL,9UL,18446744073709551608UL},{0UL,18446744073709551615UL,18446744073709551615UL,1UL},{0xCEB12FA99DB6AB31LL,0x717136C1A4EABE0FLL,2UL,0x717136C1A4EABE0FLL},{18446744073709551608UL,18446744073709551615UL,18446744073709551614UL,0x625AE403142C01D2LL}},{{18446744073709551615UL,18446744073709551615UL,0UL,18446744073709551609UL},{0x7DF68D3435D61FA6LL,0x0E1577BE735B57BDLL,18446744073709551611UL,0x7DF68D3435D61FA6LL},{0x7DF68D3435D61FA6LL,1UL,0UL,18446744073709551608UL},{18446744073709551615UL,0x7DF68D3435D61FA6LL,18446744073709551614UL,0xA164AA8A31C558FELL},{18446744073709551608UL,0UL,2UL,0x05481B40075A6259LL}}};
    int8_t *l_119 = &g_23;
    uint64_t l_163 = 18446744073709551609UL;
    const uint32_t l_197 = 1UL;
    int64_t **l_202[5][4] = {{&g_191,&g_191,&g_191,&g_191},{&g_191,&g_191,&g_191,&g_191},{&g_191,&g_191,&g_191,&g_191},{&g_191,&g_191,&g_191,&g_191},{&g_191,&g_191,&g_191,&g_191}};
    int64_t **l_207 = &g_191;
    int32_t *l_215 = &g_2;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_99[i] = &g_2;
    if ((safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((l_95 , (((!1L) , ((l_97 != ((*l_98) = l_97)) > (((((((((safe_mul_func_float_f_f((safe_add_func_float_f_f(((g_118 = (((safe_lshift_func_int16_t_s_u((((((*l_108) = &l_97) == (g_24.f1 , g_110[4][1][0])) == (g_113 , ((safe_sub_func_uint32_t_u_u((((((l_116 , 65530UL) , (*l_97)) > (*l_97)) | g_112) , g_24.f0), 4L)) != 0xA189L))) >= p_80), 10)) <= l_117[6][0][1]) , (**l_109))) , 0xB.5F8E08p+52), (-0x3.1p-1))), g_24.f1)) != 0x4.4102D6p+55) , l_119) != &g_23) < 0xDDL) & g_24.f1) ^ g_23) && 0xDFL) < p_80))) < p_80)), p_80)), p_80)))
    { /* block id: 26 */
        int8_t l_120[6][3][5] = {{{(-3L),(-3L),3L,3L,(-3L)},{(-4L),0x5CL,(-4L),0x5CL,(-4L)},{(-3L),3L,3L,(-3L),(-3L)}},{{2L,0x5CL,2L,0x5CL,2L},{(-3L),(-3L),3L,3L,(-3L)},{(-4L),0x5CL,(-4L),0x5CL,(-4L)}},{{(-3L),3L,3L,(-3L),(-3L)},{2L,0x5CL,2L,0x5CL,2L},{(-3L),(-3L),3L,3L,(-3L)}},{{(-4L),0x5CL,(-4L),0x5CL,(-4L)},{(-3L),3L,3L,(-3L),(-3L)},{2L,0x5CL,2L,0x5CL,2L}},{{(-3L),(-3L),3L,3L,(-3L)},{(-4L),0x5CL,(-4L),0x5CL,(-4L)},{(-3L),3L,3L,(-3L),(-3L)}},{{2L,0x5CL,2L,0x5CL,2L},{(-3L),(-3L),3L,3L,(-3L)},{(-4L),0x5CL,(-4L),0x5CL,(-4L)}}};
        float *l_121 = &g_122;
        int i, j, k;
        (*l_121) = l_120[5][2][2];
    }
    else
    { /* block id: 28 */
        int32_t ***l_128 = (void*)0;
        int8_t l_137 = 8L;
        int64_t l_144 = 0L;
        int32_t l_153 = 2L;
        int32_t l_157 = 0x684D056AL;
        int32_t l_160[8][7][4] = {{{0L,0L,(-1L),(-3L)},{(-3L),4L,0L,0x5A274832L},{0x90D78255L,(-7L),(-1L),0L},{(-10L),(-7L),(-3L),(-10L)},{(-7L),0L,(-1L),0x0AB2CF5BL},{(-1L),1L,(-1L),(-1L)},{(-10L),0x0AB2CF5BL,0L,0L}},{{1L,(-1L),0L,0x0AB2CF5BL},{0xC7574EA9L,(-10L),0L,0L},{1L,(-7L),0L,0x90D78255L},{(-10L),(-1L),(-1L),(-10L)},{(-1L),(-10L),(-1L),0xC7574EA9L},{(-7L),1L,(-3L),0L},{(-10L),0xC7574EA9L,(-1L),0L}},{{(-1L),1L,0L,0xC7574EA9L},{0x0AB2CF5BL,(-10L),0L,(-10L)},{1L,(-1L),(-1L),0x90D78255L},{0L,(-7L),(-1L),0L},{(-7L),(-10L),5L,0x0AB2CF5BL},{(-7L),(-1L),(-1L),0L},{0L,0x0AB2CF5BL,(-1L),(-1L)}},{{1L,1L,0L,0x0AB2CF5BL},{0x0AB2CF5BL,0L,0L,(-10L)},{(-1L),(-7L),(-1L),0L},{(-10L),(-7L),(-3L),(-10L)},{(-7L),0L,(-1L),0x0AB2CF5BL},{(-1L),1L,(-1L),(-1L)},{(-10L),0x0AB2CF5BL,0L,0L}},{{1L,(-1L),0L,0x0AB2CF5BL},{0xC7574EA9L,(-10L),0L,0L},{1L,(-7L),0L,0x90D78255L},{(-10L),(-1L),(-1L),(-10L)},{(-1L),(-10L),(-1L),0xC7574EA9L},{(-7L),1L,(-3L),0L},{(-10L),0xC7574EA9L,(-1L),0L}},{{(-1L),1L,0L,0xC7574EA9L},{0x0AB2CF5BL,(-10L),0L,(-10L)},{1L,(-1L),(-1L),0x90D78255L},{0L,(-7L),(-1L),0L},{(-7L),(-10L),5L,0x0AB2CF5BL},{(-7L),(-1L),(-1L),0L},{0L,0x0AB2CF5BL,(-1L),(-1L)}},{{1L,1L,0L,0x0AB2CF5BL},{0x0AB2CF5BL,0L,0L,(-10L)},{(-1L),(-7L),(-1L),0L},{(-10L),(-7L),(-3L),(-10L)},{(-7L),0L,(-1L),0x0AB2CF5BL},{(-1L),1L,(-1L),(-1L)},{(-10L),0x0AB2CF5BL,0L,0L}},{{1L,(-1L),0L,0x0AB2CF5BL},{0L,(-1L),5L,(-3L)},{(-4L),0xC7574EA9L,0x90D78255L,(-1L)},{(-1L),1L,1L,(-1L)},{1L,(-1L),0x5A274832L,0L},{0xC7574EA9L,(-4L),0x0AB2CF5BL,0L},{(-1L),0L,0L,0L}}};
        int32_t l_161[8][7] = {{(-7L),0xCB043313L,(-6L),(-6L),0xCB043313L,(-7L),0x1D603A4CL},{0xD4E83F10L,(-1L),0x6C1EB299L,0L,0L,0x6C1EB299L,(-1L)},{0xCB043313L,0x1D603A4CL,(-7L),0xCB043313L,(-6L),(-6L),0xCB043313L},{4L,(-1L),4L,0L,(-1L),1L,1L},{0xE4492BDCL,0xCB043313L,0xD0D45F27L,0xCB043313L,0xE4492BDCL,0xD0D45F27L,(-1L)},{0L,1L,0L,0L,0L,1L,0L},{(-7L),(-1L),0x1D603A4CL,(-6L),(-1L),(-6L),0x1D603A4CL},{0L,0L,0x6C1EB299L,(-1L),0xD4E83F10L,0x6C1EB299L,0xD4E83F10L}};
        int64_t ***l_203 = (void*)0;
        int64_t ***l_204[6] = {&l_202[4][2],(void*)0,(void*)0,&l_202[4][2],(void*)0,(void*)0};
        int i, j, k;
        if ((g_123 &= 0x4645870CL))
        { /* block id: 30 */
            return &g_2;
        }
        else
        { /* block id: 32 */
            uint64_t l_152[4][2] = {{0x5795A9035C06EB02LL,0x5795A9035C06EB02LL},{0x5795A9035C06EB02LL,0x5795A9035C06EB02LL},{0x5795A9035C06EB02LL,0x5795A9035C06EB02LL},{0x5795A9035C06EB02LL,0x5795A9035C06EB02LL}};
            int32_t l_155 = 0x3A16EC97L;
            int32_t l_158 = 1L;
            int32_t l_159 = 0L;
            int32_t l_162 = 8L;
            int i, j;
            for (p_80 = (-2); (p_80 < 36); ++p_80)
            { /* block id: 35 */
                const int32_t *l_131 = &g_6;
                const int32_t **l_130 = &l_131;
                const int32_t ***l_129 = &l_130;
                int16_t *l_145 = &g_123;
                int64_t *l_146 = &l_144;
                uint32_t *l_149 = &g_150;
                int32_t l_154 = 0x15860905L;
                int32_t l_156[5] = {0xC88D7AC8L,0xC88D7AC8L,0xC88D7AC8L,0xC88D7AC8L,0xC88D7AC8L};
                int i;
                (*g_127) = g_126;
                (*g_111) = ((l_128 != l_129) <= (safe_lshift_func_uint8_t_u_s((safe_div_func_uint64_t_u_u((((!l_137) , (safe_mod_func_uint64_t_u_u(((((safe_mul_func_int8_t_s_s((((g_151 = (((*l_146) = (&l_108 == ((safe_add_func_int16_t_s_s((l_144 || g_112), ((*l_145) = (-7L)))) , &l_129))) | (safe_add_func_uint32_t_u_u(((*l_149) &= g_118), p_80)))) || p_80) || l_152[0][1]), 0x04L)) ^ g_24.f1) >= 0x7B95B8FDL) | (-10L)), l_152[0][1]))) <= 0L), g_6)), p_80)));
                l_163--;
                (*g_111) &= (**l_130);
            }
            (*g_111) |= (safe_add_func_uint64_t_u_u(0x495B1B18042E1AEELL, p_80));
        }
        for (l_137 = 0; (l_137 > (-20)); l_137 = safe_sub_func_int8_t_s_s(l_137, 1))
        { /* block id: 49 */
            int64_t **l_193 = &g_191;
            uint32_t *l_196 = &g_150;
            int32_t l_198 = 0x9E369C8CL;
            g_199 &= ((safe_unary_minus_func_int64_t_s((safe_sub_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((safe_mod_func_int32_t_s_s(((safe_div_func_uint64_t_u_u((((safe_mul_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((((((safe_div_func_int64_t_s_s(((g_185 , (safe_mod_func_uint8_t_u_u((((~((safe_lshift_func_uint8_t_u_u((((*l_193) = g_191) == &g_192[0]), (((g_6 <= g_23) <= (0x1D1B2C16L > ((*l_196) ^= (safe_div_func_int16_t_s_s(((void*)0 == l_119), 0x7D90L))))) ^ p_80))) > p_80)) && l_197) <= (-9L)), g_23))) > l_198), 0x79F1E3D1283D66D5LL)) ^ 0xC23DL) , (-10L)) & 1L) && 1L), (-1L))) | 0xBBCEL), g_151)) >= g_24.f1) && g_192[0]), 0x9EA0974D9EF021E7LL)) == 8UL), p_80)), g_118)), p_80)))) , 0L);
        }
        (*g_111) = (safe_mod_func_int16_t_s_s(((g_205 = l_202[4][2]) != (l_207 = (g_206[0][2][0] = &g_191))), (safe_mul_func_int8_t_s_s(p_80, ((&l_197 != &g_150) != (((*g_191) = ((g_210 , 0xDCC67E5AL) < ((safe_mul_func_float_f_f((safe_mul_func_float_f_f(l_161[5][2], ((void*)0 == &l_137))), g_6)) , p_80))) <= g_6))))));
    }
    return l_215;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    transparent_crc(g_24.f0, "g_24.f0", print_hash_value);
    transparent_crc(g_24.f1, "g_24.f1", print_hash_value);
    transparent_crc(g_24.f3, "g_24.f3", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_113.f0, "g_113.f0", print_hash_value);
    transparent_crc(g_118, "g_118", print_hash_value);
    transparent_crc_bytes (&g_122, sizeof(g_122), "g_122", print_hash_value);
    transparent_crc(g_123, "g_123", print_hash_value);
    transparent_crc(g_150, "g_150", print_hash_value);
    transparent_crc(g_151, "g_151", print_hash_value);
    transparent_crc(g_185.f0, "g_185.f0", print_hash_value);
    transparent_crc(g_185.f1, "g_185.f1", print_hash_value);
    transparent_crc(g_185.f3, "g_185.f3", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_192[i], "g_192[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_199, "g_199", print_hash_value);
    transparent_crc(g_210, "g_210", print_hash_value);
    transparent_crc(g_233, "g_233", print_hash_value);
    transparent_crc(g_282, "g_282", print_hash_value);
    transparent_crc(g_286, "g_286", print_hash_value);
    transparent_crc(g_287, "g_287", print_hash_value);
    transparent_crc(g_315, "g_315", print_hash_value);
    transparent_crc(g_358, "g_358", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_360[i], "g_360[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_380, "g_380", print_hash_value);
    transparent_crc(g_400, "g_400", print_hash_value);
    transparent_crc(g_453, "g_453", print_hash_value);
    transparent_crc(g_584, "g_584", print_hash_value);
    transparent_crc(g_606, "g_606", print_hash_value);
    transparent_crc(g_662.f0, "g_662.f0", print_hash_value);
    transparent_crc(g_662.f1, "g_662.f1", print_hash_value);
    transparent_crc(g_662.f3, "g_662.f3", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc_bytes(&g_703[i][j][k], sizeof(g_703[i][j][k]), "g_703[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_707, "g_707", print_hash_value);
    transparent_crc(g_822, "g_822", print_hash_value);
    transparent_crc(g_940.f0, "g_940.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_981[i][j].f0, "g_981[i][j].f0", print_hash_value);
            transparent_crc(g_981[i][j].f1, "g_981[i][j].f1", print_hash_value);
            transparent_crc(g_981[i][j].f3, "g_981[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1012, "g_1012", print_hash_value);
    transparent_crc(g_1013, "g_1013", print_hash_value);
    transparent_crc(g_1014.f0, "g_1014.f0", print_hash_value);
    transparent_crc(g_1014.f1, "g_1014.f1", print_hash_value);
    transparent_crc(g_1014.f3, "g_1014.f3", print_hash_value);
    transparent_crc(g_1047, "g_1047", print_hash_value);
    transparent_crc(g_1093.f0, "g_1093.f0", print_hash_value);
    transparent_crc(g_1093.f1, "g_1093.f1", print_hash_value);
    transparent_crc(g_1093.f3, "g_1093.f3", print_hash_value);
    transparent_crc(g_1117.f0, "g_1117.f0", print_hash_value);
    transparent_crc(g_1117.f1, "g_1117.f1", print_hash_value);
    transparent_crc(g_1117.f3, "g_1117.f3", print_hash_value);
    transparent_crc(g_1156.f0, "g_1156.f0", print_hash_value);
    transparent_crc(g_1156.f1, "g_1156.f1", print_hash_value);
    transparent_crc(g_1156.f3, "g_1156.f3", print_hash_value);
    transparent_crc(g_1195.f0, "g_1195.f0", print_hash_value);
    transparent_crc(g_1199, "g_1199", print_hash_value);
    transparent_crc(g_1210, "g_1210", print_hash_value);
    transparent_crc(g_1234.f0, "g_1234.f0", print_hash_value);
    transparent_crc(g_1234.f1, "g_1234.f1", print_hash_value);
    transparent_crc(g_1234.f3, "g_1234.f3", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_1312[i], sizeof(g_1312[i]), "g_1312[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1368.f0, "g_1368.f0", print_hash_value);
    transparent_crc(g_1368.f1, "g_1368.f1", print_hash_value);
    transparent_crc(g_1368.f3, "g_1368.f3", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1477[i][j], "g_1477[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1478, "g_1478", print_hash_value);
    transparent_crc(g_1479, "g_1479", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1480[i][j][k], "g_1480[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1481, "g_1481", print_hash_value);
    transparent_crc(g_1522, "g_1522", print_hash_value);
    transparent_crc(g_1538, "g_1538", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1638[i], "g_1638[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1733, "g_1733", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1772[i], "g_1772[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2090, "g_2090", print_hash_value);
    transparent_crc(g_2160.f0, "g_2160.f0", print_hash_value);
    transparent_crc(g_2160.f1, "g_2160.f1", print_hash_value);
    transparent_crc(g_2160.f3, "g_2160.f3", print_hash_value);
    transparent_crc(g_2198, "g_2198", print_hash_value);
    transparent_crc(g_2251, "g_2251", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_2326[i][j].f0, "g_2326[i][j].f0", print_hash_value);
            transparent_crc(g_2326[i][j].f1, "g_2326[i][j].f1", print_hash_value);
            transparent_crc(g_2326[i][j].f3, "g_2326[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2346, "g_2346", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 628
XXX total union variables: 35

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 40
breakdown:
   indirect level: 0, occurrence: 25
   indirect level: 1, occurrence: 3
   indirect level: 2, occurrence: 4
   indirect level: 3, occurrence: 8
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 24
XXX times a bitfields struct on LHS: 4
XXX times a bitfields struct on RHS: 33
XXX times a single bitfield on LHS: 4
XXX times a single bitfield on RHS: 63

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 135
   depth: 2, occurrence: 36
   depth: 3, occurrence: 3
   depth: 4, occurrence: 5
   depth: 5, occurrence: 1
   depth: 10, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 4
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 27, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 3
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 508

XXX times a variable address is taken: 1203
XXX times a pointer is dereferenced on RHS: 351
breakdown:
   depth: 1, occurrence: 216
   depth: 2, occurrence: 82
   depth: 3, occurrence: 43
   depth: 4, occurrence: 10
XXX times a pointer is dereferenced on LHS: 301
breakdown:
   depth: 1, occurrence: 252
   depth: 2, occurrence: 27
   depth: 3, occurrence: 16
   depth: 4, occurrence: 3
   depth: 5, occurrence: 3
XXX times a pointer is compared with null: 43
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 10106

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1193
   level: 2, occurrence: 391
   level: 3, occurrence: 283
   level: 4, occurrence: 162
   level: 5, occurrence: 46
XXX number of pointers point to pointers: 250
XXX number of pointers point to scalars: 234
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.1
XXX average alias set size: 1.38

XXX times a non-volatile is read: 2076
XXX times a non-volatile is write: 927
XXX times a volatile is read: 100
XXX    times read thru a pointer: 37
XXX times a volatile is write: 58
XXX    times written thru a pointer: 44
XXX times a volatile is available for access: 7.28e+03
XXX percentage of non-volatile access: 95

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 141
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 22
   depth: 2, occurrence: 23
   depth: 3, occurrence: 25
   depth: 4, occurrence: 17
   depth: 5, occurrence: 26

XXX percentage a fresh-made variable is used: 14.9
XXX percentage an existing variable is used: 85.1
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


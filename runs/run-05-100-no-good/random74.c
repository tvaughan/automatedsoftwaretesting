/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3693382649
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   uint32_t  f0;
   const float  f1;
   signed f2 : 2;
   volatile float  f3;
   int16_t  f4;
   int32_t  f5;
   int32_t  f6;
   const unsigned f7 : 18;
};

/* --- GLOBAL VARIABLES --- */
static float g_23 = 0x3.0C4E2Bp-81;
static uint8_t g_36 = 7UL;
static int32_t g_43 = 0x9F010208L;
static int32_t * volatile g_42[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int64_t g_54 = 3L;
static const int64_t g_77 = 0xB5251BB39D90DC6CLL;
static int64_t g_85 = 0L;
static int64_t *g_84 = &g_85;
static uint64_t g_97 = 1UL;
static int64_t g_101 = 0x683D18CDA47519ADLL;
static uint8_t g_106 = 249UL;
static int32_t g_109[4][2][5] = {{{0xBCB0F1F8L,0L,0xBCB0F1F8L,0L,0xBCB0F1F8L},{0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L}},{{0xBCB0F1F8L,0L,0xBCB0F1F8L,0L,0xBCB0F1F8L},{0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L}},{{0xBCB0F1F8L,0L,0xBCB0F1F8L,0L,0xBCB0F1F8L},{0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L}},{{0xBCB0F1F8L,0L,0xBCB0F1F8L,0L,0xBCB0F1F8L},{0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L,0xBE1E68A7L}}};
static float g_110[7][2][5] = {{{0x7.BFCA48p+80,0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80},{0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39}},{{0x7.BFCA48p+80,0x7.BFCA48p+80,0xA.A352C7p-45,0x7.BFCA48p+80,0x7.BFCA48p+80},{0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80}},{{0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39},{0x7.BFCA48p+80,0x7.BFCA48p+80,0xA.A352C7p-45,0x7.BFCA48p+80,0x7.BFCA48p+80}},{{0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80},{0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39}},{{0x7.BFCA48p+80,0x7.BFCA48p+80,0xA.A352C7p-45,0x7.BFCA48p+80,0x7.BFCA48p+80},{0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80}},{{0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39},{0x7.BFCA48p+80,0x7.BFCA48p+80,0xA.A352C7p-45,0x7.BFCA48p+80,0x7.BFCA48p+80}},{{0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80},{0x7.BFCA48p+80,0xD.733C45p-39,0xD.733C45p-39,0x7.BFCA48p+80,0xD.733C45p-39}}};
static int32_t ** volatile g_116 = (void*)0;/* VOLATILE GLOBAL g_116 */
static const int32_t g_124[2][10][3] = {{{0xEE16EA25L,(-1L),0x537F146AL},{(-1L),2L,0x6DABD80EL},{(-2L),(-1L),6L},{0x676F3906L,0xB72F399BL,0xB72F399BL},{0x676F3906L,6L,(-1L)},{(-2L),0x6DABD80EL,2L},{(-1L),0x537F146AL,(-1L)},{0xEE16EA25L,(-1L),0x59F6D150L},{0xB72F399BL,0x537F146AL,2L},{(-1L),0x6DABD80EL,0xEE16EA25L}},{{(-6L),6L,(-1L)},{(-1L),0xB72F399BL,(-1L)},{(-8L),(-1L),0xEE16EA25L},{0x8C3DAD0BL,2L,2L},{(-7L),(-1L),0x59F6D150L},{2L,0x59F6D150L,(-1L)},{(-7L),2L,2L},{0x8C3DAD0BL,0xEE16EA25L,(-1L)},{(-8L),(-1L),0xB72F399BL},{(-1L),(-1L),6L}}};
static const struct S0 g_127 = {18446744073709551610UL,0xB.EAA1EDp-74,-1,0xC.1FAE94p+65,0L,0x9D30BDFCL,0x86C6A208L,28};/* VOLATILE GLOBAL g_127 */
static int32_t g_160 = 3L;
static int64_t *g_199[2][10][10] = {{{&g_85,(void*)0,&g_85,&g_101,(void*)0,&g_85,&g_101,(void*)0,(void*)0,&g_85},{&g_101,(void*)0,&g_85,&g_85,&g_85,(void*)0,&g_85,&g_85,&g_101,&g_85},{&g_101,&g_85,(void*)0,&g_85,&g_85,&g_101,&g_85,&g_85,(void*)0,&g_85},{(void*)0,&g_101,(void*)0,&g_85,&g_85,&g_85,&g_101,&g_101,&g_101,(void*)0},{&g_85,&g_101,&g_85,&g_85,&g_101,&g_101,&g_85,&g_85,&g_101,&g_85},{&g_101,&g_101,&g_101,&g_85,&g_101,&g_101,&g_85,(void*)0,&g_85,(void*)0},{&g_101,&g_85,&g_101,&g_101,&g_101,(void*)0,(void*)0,&g_101,(void*)0,&g_85},{&g_101,&g_101,&g_85,&g_85,&g_85,&g_101,&g_85,(void*)0,&g_85,(void*)0},{&g_85,&g_101,&g_85,&g_85,&g_85,&g_85,(void*)0,&g_101,&g_101,&g_101},{&g_101,&g_85,&g_85,&g_101,&g_101,&g_101,&g_85,&g_85,&g_85,&g_85}},{{&g_101,&g_85,&g_85,&g_101,&g_85,&g_85,&g_101,&g_85,&g_85,&g_101},{&g_85,&g_85,&g_85,&g_101,&g_85,&g_85,&g_85,&g_85,(void*)0,&g_85},{&g_85,&g_85,&g_85,&g_101,&g_85,&g_85,&g_101,&g_101,&g_101,(void*)0},{&g_101,&g_85,&g_85,&g_85,&g_101,(void*)0,&g_85,&g_85,&g_101,&g_101},{&g_101,&g_85,&g_85,(void*)0,&g_101,&g_85,(void*)0,&g_101,&g_101,&g_101},{&g_85,&g_101,&g_85,(void*)0,(void*)0,&g_85,&g_85,&g_85,&g_85,(void*)0},{&g_85,&g_101,&g_101,&g_85,&g_101,&g_85,(void*)0,&g_85,&g_101,&g_101},{(void*)0,&g_101,&g_85,&g_85,&g_85,&g_85,&g_101,(void*)0,&g_101,&g_85},{&g_101,&g_101,(void*)0,&g_85,&g_85,&g_85,&g_85,&g_85,&g_85,&g_101},{&g_101,&g_85,&g_101,(void*)0,&g_101,&g_101,(void*)0,&g_101,&g_101,&g_101}}};
static const struct S0 g_224 = {18446744073709551608UL,0x8.96BE9Dp+21,1,0x7.0EDA4Bp+3,-2L,0x2DBE1E36L,-1L,122};/* VOLATILE GLOBAL g_224 */
static int16_t g_226 = 1L;
static uint8_t g_233 = 0x76L;
static volatile uint16_t g_243[6][10] = {{7UL,0x02B7L,0x7033L,65535UL,0x02B7L,0UL,0xE040L,7UL,7UL,0x6C84L},{0x19E0L,0x02B7L,0x6C7CL,0xBD4BL,65529UL,65528UL,0UL,0UL,65528UL,65529UL},{6UL,65535UL,65535UL,6UL,0xEB29L,0x6C84L,0x19E0L,7UL,0x7033L,6UL},{7UL,65529UL,0UL,0xEB29L,0x19E0L,0xE040L,65535UL,0x19E0L,0x7033L,0x6C84L},{65535UL,65535UL,65528UL,6UL,7UL,0x6C7CL,7UL,6UL,65528UL,65535UL},{0x02B7L,0xE040L,0UL,0xEB29L,0UL,0UL,0xEB29L,0x5DAEL,65528UL,65535UL}};
static volatile uint16_t *g_242 = &g_243[3][3];
static int32_t g_266 = 0xDAA45463L;
static uint8_t g_267 = 2UL;
static int32_t *g_299 = &g_43;
static int32_t ** volatile g_298 = &g_299;/* VOLATILE GLOBAL g_298 */
static int32_t *g_318 = &g_160;
static uint16_t g_321 = 0x21ABL;
static const int32_t g_330 = 0x4C12483FL;
static const int32_t *g_332 = &g_124[1][6][0];
static const int32_t ** volatile g_331 = &g_332;/* VOLATILE GLOBAL g_331 */
static uint32_t g_350 = 0UL;
static uint32_t g_366 = 0x36B64D6CL;
static const int32_t ** volatile g_369[7][10][3] = {{{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332}},{{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332}},{{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332}},{{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332}},{{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332}},{{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332},{(void*)0,&g_332,&g_332},{&g_332,&g_332,&g_332},{(void*)0,&g_332,&g_332}},{{(void*)0,&g_332,&g_332},{(void*)0,(void*)0,(void*)0},{&g_332,(void*)0,&g_332},{(void*)0,&g_332,(void*)0},{&g_332,(void*)0,&g_332},{&g_332,(void*)0,(void*)0},{&g_332,&g_332,&g_332},{&g_332,&g_332,(void*)0},{&g_332,&g_332,&g_332},{(void*)0,(void*)0,(void*)0}}};
static const int32_t ** volatile g_370 = (void*)0;/* VOLATILE GLOBAL g_370 */
static int8_t g_397[1] = {0xC2L};
static const volatile struct S0 g_400[9] = {{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84},{0xF3945556L,0x6.E0084Cp+87,-0,0x7.485DB0p+47,0xDC09L,-1L,0L,84}};
static uint16_t *g_402[6] = {&g_321,&g_321,&g_321,&g_321,&g_321,&g_321};
static uint16_t **g_401[9][1] = {{&g_402[3]},{&g_402[3]},{&g_402[3]},{&g_402[3]},{&g_402[3]},{&g_402[3]},{&g_402[3]},{&g_402[3]},{&g_402[3]}};
static volatile uint8_t g_407 = 0xA2L;/* VOLATILE GLOBAL g_407 */
static volatile uint8_t * const g_406 = &g_407;
static volatile uint8_t * const  volatile *g_405 = &g_406;
static const struct S0 g_419 = {0xE7EF8DF2L,0xE.FF5E14p-49,0,-0x7.Ep-1,-10L,0xC88D81F1L,0L,10};/* VOLATILE GLOBAL g_419 */
static uint64_t g_427 = 0x6CD009B4BD5CA159LL;
static int8_t g_453[1] = {0x93L};
static struct S0 g_463 = {18446744073709551615UL,0x0.7p+1,0,0x5.A18B2Fp-10,-1L,-9L,1L,355};/* VOLATILE GLOBAL g_463 */
static struct S0 g_466 = {0x163F761DL,0x1.Dp-1,-1,-0x1.Cp+1,1L,7L,0xC7111C89L,328};/* VOLATILE GLOBAL g_466 */
static struct S0 *g_465[8][1][3] = {{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}},{{&g_463,&g_463,&g_463}}};
static const uint16_t g_485[8] = {0xC54FL,0x8023L,0x8023L,0xC54FL,0x8023L,0x8023L,0xC54FL,0x8023L};
static int16_t *g_501 = (void*)0;
static int32_t *g_502 = &g_109[1][1][2];
static int32_t g_556[1] = {0x0EC099B2L};
static float * volatile g_602 = &g_23;/* VOLATILE GLOBAL g_602 */
static volatile int64_t g_630 = (-1L);/* VOLATILE GLOBAL g_630 */
static int32_t *g_634[8] = {&g_109[2][1][2],&g_109[2][1][2],&g_109[2][1][2],&g_109[2][1][2],&g_109[2][1][2],&g_109[2][1][2],&g_109[2][1][2],&g_109[2][1][2]};
static struct S0 g_649 = {18446744073709551612UL,0x0.9325CCp-2,-1,0x1.6p+1,1L,6L,-1L,88};/* VOLATILE GLOBAL g_649 */
static int32_t ** volatile g_718 = &g_299;/* VOLATILE GLOBAL g_718 */
static volatile struct S0 g_740 = {18446744073709551612UL,0x1.4DA346p+89,0,0xE.000901p-72,-10L,0x0F44B469L,0xA730B497L,343};/* VOLATILE GLOBAL g_740 */
static float g_774[4][2] = {{0x1.3p+1,0x1.3p+1},{0x1.3p+1,0x1.3p+1},{0x1.3p+1,0x1.3p+1},{0x1.3p+1,0x1.3p+1}};
static uint8_t g_825 = 0UL;
static int64_t g_847 = (-9L);
static volatile uint32_t g_887 = 5UL;/* VOLATILE GLOBAL g_887 */
static uint64_t g_914[6] = {18446744073709551615UL,0x0E26259BF763E2FALL,18446744073709551615UL,18446744073709551615UL,0x0E26259BF763E2FALL,18446744073709551615UL};
static int32_t g_986 = (-10L);
static const struct S0 g_1006[1][1] = {{{0x02C09693L,0x7.E8E905p-50,0,0x7.5CF939p+65,0L,0xA3EF989CL,0x6DD56F50L,316}}};
static int32_t ** const  volatile g_1046 = &g_634[6];/* VOLATILE GLOBAL g_1046 */
static volatile int64_t * volatile g_1073[6] = {(void*)0,(void*)0,&g_630,(void*)0,(void*)0,&g_630};
static volatile int64_t * volatile *g_1072[10] = {(void*)0,(void*)0,&g_1073[1],(void*)0,(void*)0,&g_1073[1],(void*)0,(void*)0,&g_1073[1],(void*)0};
static volatile int64_t * volatile * volatile * const g_1071 = &g_1072[3];
static int32_t ** volatile g_1074 = (void*)0;/* VOLATILE GLOBAL g_1074 */
static int32_t ** volatile g_1075[2] = {(void*)0,(void*)0};
static volatile int64_t g_1103 = 0x1EC34C132A7FC130LL;/* VOLATILE GLOBAL g_1103 */
static struct S0 g_1111 = {1UL,0x3.91F62Cp+33,0,0x8.C17D3Cp-80,-1L,0x862E1699L,2L,463};/* VOLATILE GLOBAL g_1111 */
static int32_t *g_1113 = &g_556[0];
static uint32_t g_1134 = 0x1DFC8EB1L;
static uint64_t *g_1159 = &g_914[3];
static uint64_t **g_1158 = &g_1159;
static int64_t *g_1182 = &g_85;
static uint32_t g_1186[7][9] = {{0UL,4294967295UL,0xF02ABD00L,0x9E8B6687L,0UL,0UL,0x9E8B6687L,0xF02ABD00L,4294967295UL},{4294967295UL,0UL,0xE9C6EC05L,4294967295UL,0x623FDB3EL,0x0C06B88EL,0x0C06B88EL,0x623FDB3EL,4294967295UL},{1UL,6UL,1UL,0UL,0x9E8B6687L,0UL,0x0DAAA92FL,0x0DAAA92FL,0UL},{0xE9C6EC05L,0UL,4294967295UL,0UL,0xE9C6EC05L,4294967295UL,0x623FDB3EL,0x0C06B88EL,0x0C06B88EL},{0xF02ABD00L,4294967295UL,0UL,0UL,0UL,4294967295UL,0xF02ABD00L,0x9E8B6687L,0UL},{4294967289UL,0x304337D2L,0UL,4294967295UL,0xB73DD1EBL,4294967295UL,0UL,0x304337D2L,4294967289UL},{4294967295UL,0x8B78A7B8L,0x0DAAA92FL,0x9E8B6687L,0x9DD51497L,0UL,0x9DD51497L,0x9E8B6687L,0x0DAAA92FL}};
static int32_t g_1201 = (-1L);
static volatile struct S0 g_1218 = {1UL,0x0.9p+1,-0,0x4.Ep-1,-9L,0x69407953L,0xCF3D3373L,291};/* VOLATILE GLOBAL g_1218 */
static struct S0 * const  volatile *g_1222 = &g_465[3][0][1];
static struct S0 * const  volatile ** volatile g_1221 = &g_1222;/* VOLATILE GLOBAL g_1221 */
static struct S0 **g_1224 = &g_465[1][0][0];
static struct S0 ***g_1223[2] = {&g_1224,&g_1224};
static const int64_t g_1230[5][3][4] = {{{(-2L),(-1L),(-2L),0xB8FA8A22DFC0C43ALL},{0x26BC46F7308311C1LL,(-1L),0x4717AC03DBCC46A9LL,0x357F1BAAE6FE669DLL},{(-1L),0x39B5937285D3F48CLL,0x39B5937285D3F48CLL,(-1L)}},{{(-2L),0x357F1BAAE6FE669DLL,0x39B5937285D3F48CLL,0xB8FA8A22DFC0C43ALL},{(-1L),0x26BC46F7308311C1LL,0x4717AC03DBCC46A9LL,0x26BC46F7308311C1LL},{0x26BC46F7308311C1LL,0x39B5937285D3F48CLL,(-2L),0x26BC46F7308311C1LL}},{{(-2L),0x26BC46F7308311C1LL,0xB8FA8A22DFC0C43ALL,0xB8FA8A22DFC0C43ALL},{0x357F1BAAE6FE669DLL,0x357F1BAAE6FE669DLL,0x4717AC03DBCC46A9LL,(-1L)},{0x357F1BAAE6FE669DLL,0x39B5937285D3F48CLL,0xB8FA8A22DFC0C43ALL,0x357F1BAAE6FE669DLL}},{{(-2L),(-1L),(-2L),0xB8FA8A22DFC0C43ALL},{0x26BC46F7308311C1LL,(-1L),0x4717AC03DBCC46A9LL,0x357F1BAAE6FE669DLL},{(-1L),0x39B5937285D3F48CLL,0x39B5937285D3F48CLL,(-1L)}},{{(-2L),0x357F1BAAE6FE669DLL,0x39B5937285D3F48CLL,0xB8FA8A22DFC0C43ALL},{(-1L),0x26BC46F7308311C1LL,0x4717AC03DBCC46A9LL,0x26BC46F7308311C1LL},{0x26BC46F7308311C1LL,0x39B5937285D3F48CLL,(-2L),0x26BC46F7308311C1LL}}};
static int32_t ** volatile g_1358 = &g_1113;/* VOLATILE GLOBAL g_1358 */
static int32_t ** volatile g_1405 = &g_1113;/* VOLATILE GLOBAL g_1405 */
static uint16_t g_1425[1] = {1UL};
static float g_1451 = 0x1.Bp-1;
static int64_t ***g_1465[2] = {(void*)0,(void*)0};
static int32_t ** volatile g_1499[8][9][3] = {{{&g_634[6],&g_1113,(void*)0},{&g_634[7],&g_502,&g_1113},{&g_634[0],&g_634[6],(void*)0},{&g_634[6],&g_634[6],&g_502},{&g_1113,&g_634[0],&g_502},{&g_634[1],&g_502,&g_299},{&g_1113,(void*)0,&g_299},{(void*)0,&g_1113,&g_634[0]},{&g_634[1],(void*)0,&g_1113}},{{&g_634[6],&g_502,&g_634[6]},{(void*)0,&g_634[3],(void*)0},{&g_299,&g_634[3],&g_634[4]},{&g_502,&g_502,&g_634[2]},{&g_1113,(void*)0,&g_634[6]},{&g_1113,&g_1113,&g_502},{(void*)0,(void*)0,&g_502},{&g_634[6],&g_502,&g_634[6]},{&g_299,&g_634[0],(void*)0}},{{&g_299,&g_634[6],&g_502},{&g_634[6],&g_634[6],&g_299},{&g_634[0],&g_502,&g_634[6]},{&g_299,&g_1113,&g_634[6]},{&g_634[6],&g_299,&g_299},{&g_502,&g_299,&g_634[6]},{&g_299,&g_502,&g_634[6]},{&g_502,(void*)0,&g_299},{(void*)0,&g_299,&g_502}},{{&g_502,&g_634[6],(void*)0},{(void*)0,(void*)0,&g_634[6]},{&g_634[6],&g_1113,&g_502},{&g_634[7],&g_634[6],&g_502},{&g_1113,&g_634[7],&g_634[6]},{&g_634[6],&g_634[0],&g_634[2]},{&g_634[6],&g_634[6],&g_634[4]},{&g_1113,&g_634[5],(void*)0},{&g_1113,&g_634[6],&g_634[6]}},{{&g_634[6],(void*)0,&g_1113},{&g_634[6],&g_634[1],&g_634[0]},{&g_1113,(void*)0,&g_299},{&g_634[7],&g_634[7],&g_299},{&g_634[6],&g_634[1],&g_502},{(void*)0,&g_1113,&g_502},{&g_502,(void*)0,(void*)0},{&g_1113,&g_299,&g_1113},{&g_634[1],&g_634[6],&g_1113}},{{&g_1113,&g_634[2],&g_634[5]},{&g_634[6],&g_1113,&g_634[7]},{&g_1113,&g_634[2],&g_634[6]},{&g_299,&g_634[6],&g_634[6]},{&g_634[6],&g_299,&g_634[0]},{&g_634[3],(void*)0,(void*)0},{&g_634[6],(void*)0,&g_1113},{&g_1113,&g_502,&g_634[3]},{&g_634[7],&g_502,&g_299}},{{(void*)0,&g_1113,&g_634[2]},{&g_1113,(void*)0,&g_634[6]},{&g_634[6],&g_1113,&g_634[6]},{&g_634[2],&g_634[7],&g_634[6]},{&g_634[6],&g_502,&g_634[6]},{&g_1113,&g_502,&g_634[6]},{&g_634[5],&g_502,&g_634[6]},{&g_634[0],&g_502,&g_634[2]},{&g_502,&g_299,&g_299}},{{&g_634[6],&g_1113,&g_634[3]},{(void*)0,&g_502,&g_1113},{&g_634[6],&g_1113,(void*)0},{&g_634[0],(void*)0,&g_634[0]},{&g_502,&g_299,&g_634[6]},{&g_502,&g_1113,&g_634[6]},{&g_299,&g_299,&g_634[7]},{(void*)0,&g_634[6],&g_634[5]},{&g_299,&g_1113,&g_1113}}};
static int32_t ** const  volatile g_1500 = &g_502;/* VOLATILE GLOBAL g_1500 */
static struct S0 g_1519 = {0x0DE3AAF0L,0x2.1p-1,0,0xA.1591C0p+30,0x240EL,-4L,6L,469};/* VOLATILE GLOBAL g_1519 */
static volatile uint8_t * const  volatile **g_1549 = &g_405;
static volatile uint8_t * const  volatile *** volatile g_1548 = &g_1549;/* VOLATILE GLOBAL g_1548 */
static struct S0 * const *g_1602 = &g_465[7][0][0];
static struct S0 * const **g_1601 = &g_1602;
static struct S0 g_1711 = {0x3BBF43E8L,0x2.5586C7p+83,-0,0x9.896C71p+68,0x6FE9L,0x3745F1B5L,0xCD305D76L,288};/* VOLATILE GLOBAL g_1711 */
static volatile uint32_t g_1727 = 4UL;/* VOLATILE GLOBAL g_1727 */
static volatile uint32_t g_1728 = 0UL;/* VOLATILE GLOBAL g_1728 */
static volatile uint32_t *g_1726[7] = {&g_1728,&g_1728,&g_1728,&g_1728,&g_1728,&g_1728,&g_1728};
static volatile uint32_t **g_1725[1] = {&g_1726[5]};
static volatile uint8_t * const  volatile ***g_1768 = (void*)0;
static volatile uint8_t * const  volatile **** volatile g_1767 = &g_1768;/* VOLATILE GLOBAL g_1767 */
static struct S0 g_1793 = {0xE07A5992L,0x0.5p+1,-0,0x7.764030p+7,0x0325L,1L,0xED85DF18L,506};/* VOLATILE GLOBAL g_1793 */
static volatile struct S0 g_1796 = {3UL,-0x3.4p-1,0,0x0.0088DBp+89,4L,8L,0xDBDD7D85L,4};/* VOLATILE GLOBAL g_1796 */
static const volatile int32_t * const * volatile g_1820 = (void*)0;/* VOLATILE GLOBAL g_1820 */
static const volatile int32_t g_1823 = (-1L);/* VOLATILE GLOBAL g_1823 */
static const volatile int32_t * const g_1822 = &g_1823;
static const volatile int32_t * const * volatile g_1821 = &g_1822;/* VOLATILE GLOBAL g_1821 */
static const volatile int32_t * const * volatile *g_1819[3][10][7] = {{{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820}},{{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820}},{{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820},{&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821,&g_1821},{&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820,&g_1820}}};
static int32_t g_1852 = 0x48979E71L;
static uint64_t ***g_1869 = &g_1158;
static uint64_t ****g_1868[9] = {&g_1869,&g_1869,&g_1869,&g_1869,&g_1869,&g_1869,&g_1869,&g_1869,&g_1869};
static uint64_t ***** volatile g_1867 = &g_1868[1];/* VOLATILE GLOBAL g_1867 */
static const uint64_t g_1891 = 0x6D9D65ADD6AB31B0LL;
static const uint64_t *g_1890 = &g_1891;
static const uint64_t **g_1889 = &g_1890;
static const uint64_t ***g_1888 = &g_1889;
static int16_t **g_1911[6][8] = {{&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501},{&g_501,&g_501,&g_501,(void*)0,&g_501,&g_501,(void*)0,&g_501},{&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501},{&g_501,&g_501,(void*)0,&g_501,&g_501,(void*)0,&g_501,&g_501},{&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501,&g_501},{&g_501,&g_501,(void*)0,(void*)0,&g_501,&g_501,&g_501,&g_501}};
static const uint32_t * volatile g_1921 = &g_224.f0;/* VOLATILE GLOBAL g_1921 */
static const uint32_t * volatile *g_1920 = &g_1921;
static const uint32_t * volatile * const  volatile *g_1919 = &g_1920;
static const uint32_t * volatile * const  volatile **g_1918 = &g_1919;
static int32_t * volatile g_1948 = (void*)0;/* VOLATILE GLOBAL g_1948 */
static volatile uint8_t g_2033 = 0UL;/* VOLATILE GLOBAL g_2033 */
static int32_t ** volatile g_2038 = &g_299;/* VOLATILE GLOBAL g_2038 */
static int32_t ** const  volatile g_2066 = (void*)0;/* VOLATILE GLOBAL g_2066 */
static struct S0 g_2112[1] = {{18446744073709551615UL,0x2.0CFB03p-34,1,0x9.D3BBD1p-84,0x37C3L,0x802F2280L,1L,506}};
static float **g_2136 = (void*)0;
static int8_t *g_2172[1][8] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int8_t **g_2171 = &g_2172[0][6];
static int64_t g_2275 = 0L;
static volatile int32_t *g_2277 = &g_1796.f6;
static uint16_t g_2365 = 65535UL;
static uint16_t * const g_2364 = &g_2365;
static uint16_t * const *g_2363[5] = {&g_2364,&g_2364,&g_2364,&g_2364,&g_2364};
static int32_t **g_2376 = &g_318;
static int32_t ***g_2375 = &g_2376;
static volatile struct S0 g_2397 = {0xF658A32AL,-0x1.7p-1,-0,0x9.C0D19Ep+86,0L,1L,0L,16};/* VOLATILE GLOBAL g_2397 */
static volatile uint16_t * volatile * volatile * volatile g_2448 = (void*)0;/* VOLATILE GLOBAL g_2448 */
static float * volatile g_2512[6][5][5] = {{{&g_1451,&g_110[3][0][1],&g_1451,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451}},{{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451}},{{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451}},{{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451}},{{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451}},{{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451},{(void*)0,&g_110[5][1][0],(void*)0,&g_1451,&g_1451},{&g_1451,(void*)0,&g_1451,&g_1451,&g_1451}}};
static float * volatile g_2513 = &g_110[0][1][3];/* VOLATILE GLOBAL g_2513 */
static int32_t ** volatile g_2563 = &g_634[3];/* VOLATILE GLOBAL g_2563 */
static int32_t g_2600 = 0x994D7A9FL;
static volatile struct S0 g_2611 = {18446744073709551611UL,-0x1.9p-1,-0,0x1.1p-1,0L,0x4B0EF1E5L,-2L,433};/* VOLATILE GLOBAL g_2611 */
static int64_t *** const  volatile g_2710 = (void*)0;/* VOLATILE GLOBAL g_2710 */


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static int32_t  func_11(int32_t  p_12, int64_t  p_13, int32_t  p_14, const int64_t  p_15);
static uint8_t  func_29(int32_t  p_30, int64_t  p_31);
static int32_t * func_47(const uint32_t  p_48, uint8_t  p_49, int32_t * p_50, int32_t * p_51, int8_t  p_52);
static int64_t  func_55(int32_t  p_56, int64_t  p_57);
static uint32_t  func_66(int32_t * p_67, int32_t  p_68, uint16_t  p_69);
static int32_t * func_70(uint16_t  p_71, int32_t * p_72, uint32_t  p_73, int32_t  p_74);
static uint8_t  func_78(int64_t * p_79, int32_t * p_80, int32_t * p_81, int64_t * p_82, int32_t  p_83);
static int32_t * func_86(int32_t * p_87, int32_t  p_88);
static int32_t * func_89(int64_t  p_90, int32_t  p_91, int32_t * p_92);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_36 g_43 g_649.f4 g_1549 g_405 g_406 g_407 g_419.f5 g_1113 g_556 g_502 g_109 g_1046 g_634 g_463.f6 g_266 g_2038 g_299 g_397 g_2448 g_2364 g_2365 g_1006.f0 g_84 g_85 g_101 g_1500 g_602 g_23 g_2112.f7 g_1793.f2 g_2112.f2 g_1182 g_1890 g_1891 g_2275 g_1793.f1 g_2513 g_1230 g_97 g_1186 g_1425 g_2375 g_2376 g_318 g_1548 g_2563 g_740.f0 g_1869 g_1158 g_1358 g_233 g_2611 g_1159 g_914 g_332 g_124 g_419 g_267 g_224.f5 g_224.f0 g_453 g_466.f6 g_298 g_331 g_466.f7 g_77 g_1111.f6 g_1889 g_2112.f0 g_242 g_243 g_2171 g_2172 g_1918 g_1919 g_127.f0 g_160
 * writes: g_43 g_556 g_266 g_321 g_109 g_36 g_85 g_299 g_101 g_1186 g_2365 g_825 g_774 g_110 g_1711.f5 g_160 g_634 g_1201 g_54 g_402 g_1111.f6 g_97 g_427 g_226 g_350 g_453 g_465 g_466.f6 g_463.f5 g_106 g_501 g_23 g_1465 g_914
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    int16_t l_22 = (-1L);
    const uint8_t l_34[9][9][3] = {{{0x88L,0x4EL,0x37L},{0x15L,1UL,0x75L},{246UL,7UL,0xFAL},{0x83L,1UL,1UL},{0UL,1UL,0xC9L},{0x4EL,1UL,0x36L},{1UL,1UL,0x4FL},{0x3DL,7UL,0x6FL},{246UL,1UL,0xA1L}},{{0xA2L,0x4EL,0x6AL},{6UL,246UL,0xA1L},{1UL,253UL,0x6FL},{1UL,2UL,0x4FL},{0x7AL,0x20L,0x36L},{0xBCL,0xE1L,0x4FL},{248UL,255UL,0xA2L},{0x88L,255UL,1UL},{255UL,9UL,0xBCL}},{{0x58L,9UL,0xDEL},{9UL,0x52L,0x88L},{0x99L,9UL,1UL},{0xF0L,9UL,0x69L},{7UL,255UL,7UL},{0UL,255UL,0x83L},{0x5AL,1UL,0x83L},{9UL,0xE0L,7UL},{255UL,255UL,0x69L}},{{1UL,0UL,1UL},{0x9BL,0xF0L,0x88L},{0UL,0x5AL,0xDEL},{0x9BL,247UL,0xBCL},{1UL,246UL,1UL},{255UL,0UL,0xA2L},{9UL,0x58L,0x4FL},{0x5AL,0x58L,2UL},{0UL,0UL,0x4EL}},{{7UL,246UL,0x3DL},{0xF0L,247UL,1UL},{0x99L,0x5AL,0UL},{9UL,0xF0L,1UL},{0x58L,0UL,0x3DL},{255UL,255UL,0x4EL},{0x88L,0xE0L,2UL},{248UL,1UL,0x4FL},{248UL,255UL,0xA2L}},{{0x88L,255UL,1UL},{255UL,9UL,0xBCL},{0x58L,9UL,0xDEL},{9UL,0x52L,0x88L},{0x99L,9UL,1UL},{0xF0L,9UL,0x69L},{7UL,255UL,7UL},{0UL,255UL,0x83L},{0x5AL,1UL,0x83L}},{{9UL,0xE0L,7UL},{255UL,255UL,0x69L},{1UL,0UL,1UL},{0x9BL,0xF0L,0x88L},{0UL,0x5AL,0xDEL},{0x9BL,247UL,0xBCL},{1UL,246UL,1UL},{255UL,0UL,0xA2L},{9UL,0x58L,0x4FL}},{{0x5AL,0x58L,2UL},{0UL,0UL,0x4EL},{7UL,246UL,0x3DL},{0xF0L,247UL,1UL},{0x99L,0x5AL,0UL},{9UL,0xF0L,1UL},{0x58L,0UL,0x3DL},{255UL,255UL,0x4EL},{0x88L,0xE0L,2UL}},{{248UL,1UL,0x4FL},{248UL,255UL,0xA2L},{0x88L,255UL,1UL},{255UL,9UL,0xBCL},{0x58L,9UL,0xDEL},{9UL,0x52L,0x88L},{0UL,0UL,255UL},{0x6FL,0x9AL,0xE4L},{250UL,249UL,246UL}}};
    int32_t l_35 = 0xD89C1C50L;
    int32_t *l_2440[5];
    uint16_t l_2441 = 0UL;
    uint64_t l_2461 = 1UL;
    uint32_t l_2465 = 0xA06170D6L;
    int32_t l_2486 = 1L;
    uint8_t l_2510 = 0x25L;
    uint16_t l_2511 = 0x7574L;
    int32_t l_2520 = 1L;
    uint64_t * const **l_2552 = (void*)0;
    int64_t l_2598[3][7][10] = {{{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL}},{{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL}},{{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL},{0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL,0L,0x7A3DE2DDB6612338LL}}};
    uint16_t l_2634 = 0x2843L;
    float l_2687 = (-0x2.Bp-1);
    int64_t **l_2709 = &g_1182;
    int64_t ***l_2711 = &l_2709;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_2440[i] = &g_556[0];
    (*g_1113) = (safe_div_func_uint32_t_u_u((0x2E5664DC9ED9BA07LL & (((((safe_rshift_func_int16_t_s_u((((safe_lshift_func_int8_t_s_s(((l_35 = (safe_unary_minus_func_uint64_t_u((func_11((safe_add_func_int16_t_s_s((safe_div_func_uint64_t_u_u(18446744073709551615UL, ((((((safe_rshift_func_uint8_t_u_s(l_22, 7)) || (l_22 || (l_22 >= (!(safe_add_func_int32_t_s_s((safe_mul_func_int8_t_s_s((0x64BDA99DA972737ALL > 0L), ((func_29(((safe_mul_func_uint16_t_u_u((7UL >= l_34[2][1][0]), l_35)) , 0x31A2955EL), g_36) || 0xC1L) ^ l_34[2][1][0]))), g_649.f4)))))) , (***g_1549)) == l_34[4][0][1]) <= g_419.f5) ^ (-9L)))), 0x45C9L)), l_34[2][1][0], l_35, l_34[0][0][0]) >= (*g_1113))))) & l_34[7][4][0]), l_34[2][1][0])) && l_34[8][5][0]) | l_34[4][6][1]), l_34[2][1][0])) , l_22) == (*g_502)) != 18446744073709551615UL) < 0xB75AL)), l_22));
    if ((**g_1046))
    { /* block id: 1038 */
        return l_34[3][8][1];
    }
    else
    { /* block id: 1040 */
        uint64_t l_2450 = 0x02A46E92700A7447LL;
        int32_t l_2466 = 1L;
        uint64_t l_2545 = 18446744073709551611UL;
        uint8_t l_2548 = 0x01L;
        float *l_2561[5][1] = {{&g_774[0][1]},{&g_23},{&g_774[0][1]},{&g_23},{&g_774[0][1]}};
        int8_t ***l_2591 = &g_2171;
        uint8_t l_2614 = 0x15L;
        struct S0 ****l_2626[2];
        struct S0 *****l_2625 = &l_2626[0];
        uint16_t *l_2628 = &g_2365;
        int32_t l_2633 = 1L;
        int32_t l_2661 = 1L;
        uint64_t l_2669 = 0UL;
        int64_t **l_2679 = &g_199[1][6][4];
        int64_t ***l_2678 = &l_2679;
        int8_t l_2700 = (-1L);
        int8_t l_2707 = 0xEAL;
        int i, j;
        for (i = 0; i < 2; i++)
            l_2626[i] = &g_1223[1];
        for (g_266 = 0; (g_266 >= 0); g_266 -= 1)
        { /* block id: 1043 */
            uint32_t l_2459[10][1] = {{0xBC1A0054L},{0x7AD4AB23L},{0xBC1A0054L},{0x7AD4AB23L},{0xBC1A0054L},{0x7AD4AB23L},{0xBC1A0054L},{0x7AD4AB23L},{0xBC1A0054L},{0x7AD4AB23L}};
            const uint64_t l_2460 = 0UL;
            int i, j;
            l_2440[0] = (*g_2038);
            for (g_321 = 0; (g_321 <= 0); g_321 += 1)
            { /* block id: 1047 */
                uint8_t *l_2449 = &g_36;
                uint8_t l_2462[10];
                uint8_t l_2463 = 0UL;
                int32_t l_2464 = 1L;
                int i;
                for (i = 0; i < 10; i++)
                    l_2462[i] = 6UL;
                if (g_397[g_266])
                    break;
                (*g_502) |= l_2441;
                if (((l_2466 &= (safe_sub_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_u(((l_2464 = ((*g_84) ^= ((((*l_2449) = ((void*)0 != g_2448)) > 0UL) < ((((((((*g_2364) & ((l_2450 > ((safe_div_func_uint8_t_u_u((((safe_div_func_int64_t_s_s((safe_mul_func_int16_t_s_s(((safe_mul_func_int8_t_s_s(l_2459[0][0], g_397[g_266])) >= l_2460), (l_2459[3][0] , 0x96EEL))), 0x1C45FF28F2AB9857LL)) , l_2450) , l_2461), l_2450)) >= (*g_502))) >= l_2450)) | l_2459[8][0]) || l_2462[4]) & l_2463) & 0x73DFL) , g_1006[0][0].f0) <= l_2459[0][0])))) <= l_2459[0][0]), l_2465)), 7)), 0x44CBL))) != 4L))
                { /* block id: 1054 */
                    int32_t **l_2467 = &g_299;
                    (*l_2467) = (void*)0;
                }
                else
                { /* block id: 1056 */
                    return l_2466;
                }
            }
        }
        for (g_101 = (-22); (g_101 <= 17); ++g_101)
        { /* block id: 1063 */
            int32_t l_2470 = 0x01CDC061L;
            int32_t l_2506 = 0L;
            int32_t l_2507[8][10] = {{5L,0x39708442L,1L,0xF1F9397BL,0xC49183AAL,0x78AA49B4L,0L,(-1L),(-1L),0L},{1L,0xA8AA2FDAL,0x184F10F6L,0x184F10F6L,0xA8AA2FDAL,1L,(-1L),0xEB1825EAL,0x5993346FL,(-2L)},{0L,0x063AF954L,0L,0xC49183AAL,0x184F10F6L,0L,0x5993346FL,0x8FCF190EL,0x08C7CB94L,5L},{0L,0x7DA4C627L,0xF7E080BFL,0x39708442L,0x78AA49B4L,1L,5L,0xA8AA2FDAL,5L,1L},{1L,5L,0xA8AA2FDAL,5L,1L,0x78AA49B4L,0x39708442L,0xF7E080BFL,0x7DA4C627L,0L},{5L,0x08C7CB94L,0x8FCF190EL,0x5993346FL,0L,0x184F10F6L,0xC49183AAL,0L,0x063AF954L,0L},{(-2L),0x5993346FL,0xEB1825EAL,(-1L),1L,0xA8AA2FDAL,0x184F10F6L,0x184F10F6L,0xA8AA2FDAL,1L},{0L,(-1L),(-1L),0L,0x78AA49B4L,0xC49183AAL,0xF1F9397BL,1L,0x39708442L,5L}};
            int32_t l_2508 = 1L;
            int32_t *l_2599 = &g_466.f6;
            int64_t l_2683 = (-6L);
            int16_t ***l_2686 = &g_1911[5][4];
            uint32_t *l_2695 = &g_463.f0;
            uint32_t **l_2694 = &l_2695;
            uint32_t ***l_2693 = &l_2694;
            int i, j;
            if (l_2470)
            { /* block id: 1064 */
                if ((**g_1500))
                    break;
            }
            else
            { /* block id: 1066 */
                struct S0 **l_2487 = &g_465[3][0][2];
                uint32_t *l_2488 = &g_1186[5][2];
                uint8_t *l_2503 = (void*)0;
                uint8_t *l_2504 = &g_825;
                int32_t l_2505 = 0xEF1B7FFEL;
                float *l_2509 = &g_774[0][1];
                int32_t l_2521[8];
                int16_t l_2547 = (-1L);
                int64_t l_2556[1];
                int i;
                for (i = 0; i < 8; i++)
                    l_2521[i] = 0x156790AFL;
                for (i = 0; i < 1; i++)
                    l_2556[i] = 0x7E4E332FD7D70619LL;
                (*g_2513) = (safe_sub_func_float_f_f((safe_sub_func_float_f_f((((safe_sub_func_float_f_f((*g_602), g_2112[0].f7)) > (safe_add_func_float_f_f(0x2.6p+1, (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((*l_2509) = ((safe_unary_minus_func_int16_t_s((((safe_mod_func_uint64_t_u_u(((l_2470 = (l_2470 >= (((l_2486 >= ((((*l_2488) = ((void*)0 != l_2487)) , ((((safe_rshift_func_uint16_t_u_s(((*g_2364)--), 2)) > (-1L)) ^ ((*l_2488) = (safe_rshift_func_int8_t_s_s((((safe_sub_func_uint32_t_u_u((((*l_2504) = (safe_mul_func_uint16_t_u_u((((*g_1182) &= (safe_mod_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u(g_1793.f2, l_2470)) == 0xCBE06B8DL), g_2112[0].f2))) <= (*g_1890)), 0xC7A9L))) ^ 0x29L), l_2505)) > 0xC4334580EAD4ACA5LL) ^ l_2505), l_2470)))) , g_2275)) | l_2466)) && 0xECC7L) & l_2470))) | (-1L)), l_2506)) >= l_2507[3][5]) , l_2508))) , g_1793.f1)), l_2507[3][5])), (-0x1.Ep+1)))))) < l_2510), l_2511)), l_2507[4][4]));
                for (g_1711.f5 = 0; (g_1711.f5 <= (-6)); g_1711.f5 = safe_sub_func_int32_t_s_s(g_1711.f5, 4))
                { /* block id: 1077 */
                    int8_t l_2544 = 7L;
                    int64_t l_2546 = 0x2F1F8AE8F5BAD54ALL;
                    int8_t l_2551 = 9L;
                    int8_t *l_2553 = (void*)0;
                    int8_t *l_2554[4][4] = {{&l_2551,&g_453[0],&g_453[0],&l_2551},{&l_2551,&g_453[0],&g_453[0],&l_2551},{&l_2551,&g_453[0],&g_453[0],&l_2551},{&l_2551,&g_453[0],&g_453[0],&l_2551}};
                    int i, j;
                    l_2506 = ((l_2507[3][5] && (((*g_502) = (safe_div_func_uint16_t_u_u(l_2505, (l_2520 || 0UL)))) > (l_2521[2] = ((*g_1113) = (-1L))))) | ((safe_sub_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(0xB855L, (safe_div_func_uint64_t_u_u((((4294967291UL != ((*l_2488) ^= (((safe_sub_func_int64_t_s_s((safe_rshift_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s(((((*g_2364)--) ^ (safe_mul_func_uint16_t_u_u((65528UL | (((safe_mod_func_int32_t_s_s((l_2545 &= (((safe_mod_func_uint8_t_u_u(l_2466, l_2544)) & 4UL) | 0x36681EEBL)), 0xB9151525L)) , l_2546) , 0xF215L)), l_2507[2][6]))) | l_2547), g_1230[3][1][3])) && l_2506), 12)), l_2450)) , l_2547) , g_97))) | 0xAD99CC62L) | 65527UL), 0xD4FB64406B3DE0CBLL)))), l_2548)), 1L)) , l_2505));
                    if ((safe_add_func_uint32_t_u_u(g_1425[0], ((l_2551 > ((l_2466 = ((void*)0 != l_2552)) & (+(1L < l_2556[0])))) || (((safe_div_func_uint32_t_u_u(0UL, (safe_sub_func_uint64_t_u_u(((((l_2561[2][0] != (((***g_2375) = ((((((l_2450 <= l_2544) | (*g_2364)) , l_2548) ^ l_2470) < l_2556[0]) <= (**g_1046))) , l_2440[0])) <= 1UL) , (****g_1548)) < l_2506), l_2450)))) , l_2544) || l_2556[0])))))
                    { /* block id: 1087 */
                        int32_t **l_2562 = &l_2440[1];
                        (*g_2563) = ((*l_2562) = &l_35);
                    }
                    else
                    { /* block id: 1090 */
                        if (l_2466)
                            break;
                        return l_2544;
                    }
                    return l_2544;
                }
            }
            if (l_2507[3][5])
                continue;
            if (l_2450)
                break;
            for (g_1201 = 0; (g_1201 <= 16); g_1201 = safe_add_func_int32_t_s_s(g_1201, 7))
            { /* block id: 1101 */
                int16_t *l_2577 = (void*)0;
                int16_t *l_2578[10] = {&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4,&g_463.f4};
                int32_t l_2579 = (-7L);
                int32_t l_2580[7][5] = {{(-1L),0xA2936E82L,0xA2936E82L,(-1L),0xA2936E82L},{(-1L),(-1L),0xC8E9CAF6L,(-1L),(-1L)},{0xA2936E82L,(-1L),0xA2936E82L,0xA2936E82L,(-1L)},{(-1L),0xA2936E82L,0xA2936E82L,(-1L),0xA2936E82L},{(-1L),(-1L),0xC8E9CAF6L,(-1L),(-1L)},{0xA2936E82L,(-1L),0xA2936E82L,0xA2936E82L,(-1L)},{(-1L),0xA2936E82L,0xA2936E82L,(-1L),0xA2936E82L}};
                uint16_t *l_2629 = &g_1425[0];
                int32_t *l_2635 = &l_2508;
                int i, j;
                if ((safe_mul_func_int16_t_s_s(0x0319L, ((((0x5B43L >= (*g_2364)) , ((safe_rshift_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(l_2508, 6)), 4)) < g_740.f0)) <= 0xDAL) ^ ((safe_mul_func_int8_t_s_s((~0x8B04L), (safe_mod_func_int16_t_s_s((l_2579 ^= ((void*)0 != (*g_1869))), l_2506)))) >= l_2580[1][2])))))
                { /* block id: 1103 */
                    const uint16_t l_2597[6][5][8] = {{{65535UL,0x8803L,0xD03CL,0x10F8L,0x10F8L,0xD03CL,0x8803L,0x9254L},{65535UL,0UL,0xBBAEL,1UL,0x9674L,0xE570L,0xE3F7L,0x2998L},{65526UL,0x4AF9L,0x34ABL,65535UL,1UL,0xE570L,0x2FCEL,0x8803L},{65535UL,0UL,65535UL,65531UL,0xD03CL,65535UL,65529UL,65535UL},{0x8803L,65533UL,65533UL,65528UL,65535UL,0x1A48L,0xCCF6L,1UL}},{{65535UL,0xCCF6L,1UL,0x2FCEL,65535UL,0xB670L,0UL,0x1A48L},{0x4AF9L,1UL,0x2998L,0x7470L,0x2998L,1UL,0x4AF9L,0x364BL},{65528UL,0x10B5L,0xCCF6L,0xE570L,0xE988L,0x2FCEL,0UL,65535UL},{0xE570L,9UL,65526UL,65535UL,0xE988L,65535UL,65533UL,0x7470L},{65528UL,0x1A48L,0x7470L,65535UL,0x2998L,65535UL,0x10B5L,0UL}},{{0x4AF9L,65531UL,1UL,0x9254L,65535UL,65535UL,0xD03CL,0xF7B8L},{65535UL,0x364BL,1UL,0xBBAEL,65535UL,65535UL,9UL,9UL},{0x8803L,0xD03CL,0x10F8L,0x10F8L,0xD03CL,0x8803L,65535UL,0xE988L},{65535UL,0UL,9UL,0UL,1UL,0x364BL,0xB670L,65526UL},{65526UL,0x10F8L,7UL,0UL,0x9674L,1UL,65528UL,0xE988L}},{{65535UL,0x9674L,0xE3F7L,0x10F8L,0x2D45L,1UL,0x2998L,9UL},{0x9254L,0xB670L,0xD03CL,0xBBAEL,1UL,0x34ABL,0x9674L,0xF7B8L},{65535UL,1UL,0x4AF9L,0x9254L,0UL,0UL,7UL,0UL},{0x1A48L,65535UL,0xE570L,65535UL,0x1A48L,0xE988L,0x9F8BL,0x7470L},{7UL,0xBBAEL,0xF7B8L,65535UL,7UL,0x9F8BL,1UL,65535UL}},{{65533UL,0x2D45L,0xF7B8L,0xE570L,0UL,0xCCF6L,0x9F8BL,0x364BL},{7UL,65526UL,0xE570L,0x7470L,0UL,0x4AF9L,7UL,0x1A48L},{65533UL,7UL,0x4AF9L,0x2FCEL,0xB670L,65529UL,0x9674L,1UL},{0xE988L,0x34ABL,0xD03CL,65528UL,65535UL,0x2998L,0x2998L,65535UL},{65531UL,0xE3F7L,0xE3F7L,65531UL,7UL,7UL,9UL,65533UL}},{{0x2D45L,65533UL,1UL,65535UL,0x1A48L,7UL,0x4AF9L,0UL},{65535UL,65533UL,0x9674L,0xB670L,65535UL,7UL,0x9254L,65535UL},{0x9F8BL,0xD03CL,0x2D45L,0x364BL,0UL,0UL,0x9674L,65526UL},{0x8803L,0x7470L,1UL,0x6499L,0UL,65526UL,65535UL,0xE3F7L},{0xD03CL,1UL,0UL,65535UL,65531UL,65535UL,65531UL,65535UL}}};
                    int32_t *l_2601 = &g_1111.f6;
                    uint32_t l_2631 = 1UL;
                    int i, j, k;
                    for (g_54 = 17; (g_54 >= (-29)); g_54 = safe_sub_func_uint32_t_u_u(g_54, 4))
                    { /* block id: 1106 */
                        uint8_t l_2596 = 0xEDL;
                        int32_t **l_2602 = &l_2599;
                        uint32_t *l_2603 = &g_1186[5][2];
                        uint16_t **l_2627 = &g_402[3];
                        struct S0 *****l_2630[6][10] = {{&l_2626[0],&l_2626[1],&l_2626[0],(void*)0,&l_2626[0],&l_2626[1],&l_2626[0],&l_2626[0],&l_2626[1],&l_2626[0]},{&l_2626[1],&l_2626[0],&l_2626[0],&l_2626[1],&l_2626[0],(void*)0,&l_2626[0],&l_2626[1],&l_2626[0],&l_2626[0]},{&l_2626[0],&l_2626[0],&l_2626[0],&l_2626[1],&l_2626[1],&l_2626[0],&l_2626[0],&l_2626[0],&l_2626[0],&l_2626[0]},{(void*)0,&l_2626[1],&l_2626[1],&l_2626[1],(void*)0,&l_2626[0],&l_2626[0],(void*)0,&l_2626[1],&l_2626[1]},{&l_2626[0],&l_2626[0],&l_2626[1],(void*)0,(void*)0,(void*)0,&l_2626[1],&l_2626[0],&l_2626[0],&l_2626[1]},{&l_2626[1],(void*)0,&l_2626[0],&l_2626[0],(void*)0,&l_2626[1],&l_2626[1],&l_2626[1],(void*)0,&l_2626[0]}};
                        int16_t *l_2632[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i, j;
                        l_2580[0][4] ^= (safe_mod_func_int32_t_s_s((safe_lshift_func_int8_t_s_u((((void*)0 != &g_1911[1][7]) , l_2579), (((*l_2603) = (safe_rshift_func_uint16_t_u_s((0xB9FEL | (((*l_2602) = l_2601) != (void*)0)), 4))) | (**g_1358)))), g_233));
                        if (l_2579)
                            continue;
                        l_2635 = func_89(((safe_div_func_int16_t_s_s(((safe_div_func_uint64_t_u_u((((!(-10L)) , (l_2633 |= (l_2466 ^= (safe_unary_minus_func_int8_t_s((+(((*l_2599) = (g_2611 , ((l_2614 >= (((safe_lshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u((&g_2171 != (void*)0), (safe_mod_func_int32_t_s_s(((safe_sub_func_uint8_t_u_u((l_2625 == ((((*l_2627) = &l_2441) != (l_2629 = l_2628)) , l_2630[2][4])), 0x2FL)) ^ (*g_1890)), 0x887C43A5L)))) || l_2631), (*g_406))), 15)) , (void*)0) == l_2632[2])) & l_2579))) | (-9L)))))))) || l_2579), (**g_1158))) >= l_2634), l_2580[6][1])) > 1UL), (*g_332), &l_35);
                    }
                }
                else
                { /* block id: 1119 */
                    const int32_t l_2660[9][4][3] = {{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,2L}},{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,2L}},{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,2L}},{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,2L}},{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,2L}},{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,2L}},{{3L,0L,3L},{2L,3L,2L},{3L,0L,3L},{2L,3L,8L}},{{1L,3L,1L},{8L,1L,8L},{1L,3L,1L},{8L,1L,8L}},{{1L,3L,1L},{8L,1L,8L},{1L,3L,1L},{8L,1L,8L}}};
                    int i, j, k;
                    (*g_502) |= ((safe_add_func_int64_t_s_s(0xB47FFD1B1BAB4E66LL, ((*g_1182) = l_2545))) & ((((l_2661 ^= (((*l_2599) = (((void*)0 == &g_1868[1]) && ((+((+(safe_unary_minus_func_uint8_t_u((!(safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(((safe_rshift_func_uint8_t_u_u((~((safe_div_func_uint64_t_u_u((((*l_2635) = (safe_unary_minus_func_uint64_t_u((((((*l_2628)--) ^ 0L) < (*l_2635)) && ((safe_rshift_func_uint16_t_u_s((l_2633 | (safe_mul_func_int16_t_s_s(0L, (safe_add_func_uint8_t_u_u((0x6BL > 0x22L), 254UL))))), 12)) || (*l_2599)))))) <= l_2466), (**g_1889))) , g_2112[0].f0)), 0)) == 8L), l_2660[7][3][0])), l_2660[3][3][0])))))) < (*g_242))) || (*g_242)))) > l_2660[7][3][0])) <= g_453[0]) || l_2633) ^ (*g_332)));
                    return l_2660[7][3][0];
                }
                for (g_43 = 0; (g_43 >= (-17)); g_43 = safe_sub_func_uint16_t_u_u(g_43, 7))
                { /* block id: 1130 */
                    uint32_t l_2664 = 0xABA182A7L;
                    int64_t ****l_2680 = (void*)0;
                    int64_t ****l_2681 = &g_1465[0];
                    uint8_t *l_2682[2][5];
                    int8_t l_2688 = 0x50L;
                    uint32_t l_2689[10] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
                    uint32_t l_2690 = 0UL;
                    int32_t **l_2696 = &l_2440[0];
                    int32_t l_2708 = (-1L);
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_2682[i][j] = (void*)0;
                    }
                    l_2664++;
                    (*l_2599) = ((**g_405) ^ ((safe_div_func_uint32_t_u_u(l_2664, (((l_2669 != (((l_2664 , (g_23 = (g_110[0][1][3] = l_2669))) > ((safe_div_func_float_f_f((safe_mul_func_float_f_f(((((safe_mod_func_uint16_t_u_u((safe_add_func_int64_t_s_s(((((((((*l_2681) = l_2678) != (void*)0) , (((++g_36) , (void*)0) == ((((*g_2171) == (*g_2171)) && l_2633) , (void*)0))) == (*l_2635)) , &g_1911[5][4]) != l_2686) && l_2688), (*l_2599))), 0xC665L)) >= (*l_2599)) , g_97) == 0x4.CCFD7Cp+93), l_2545)), g_419.f5)) < l_2688)) > l_2689[6])) , 0x5132692CL) & 1UL))) , 246UL));
                    l_2661 = (l_2690 , (safe_div_func_float_f_f((l_2689[6] , (*l_2599)), (((*g_1918) == (l_2693 = (void*)0)) < (l_2708 = ((((*l_2696) = &l_2633) != ((*g_1046) = func_86(&l_2580[1][2], ((***g_2375) ^= ((safe_mul_func_int8_t_s_s((l_2700 = (safe_unary_minus_func_int64_t_s(l_2664))), (safe_mul_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_s((safe_add_func_uint64_t_u_u(((((((***g_1869) = (0x4EL > (****g_1548))) || (*l_2599)) && (*g_406)) <= g_127.f0) == l_2461), l_2461)), l_2707)) ^ 0UL), 65532UL)))) < (-7L)))))) > 0x0.8p+1))))));
                    return l_2614;
                }
                if ((*l_2599))
                    continue;
            }
        }
    }
    (*l_2711) = l_2709;
    return (*g_1159);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_11(int32_t  p_12, int64_t  p_13, int32_t  p_14, const int64_t  p_15)
{ /* block id: 1034 */
    return p_12;
}


/* ------------------------------------------ */
/* 
 * reads : g_43
 * writes: g_43
 */
static uint8_t  func_29(int32_t  p_30, int64_t  p_31)
{ /* block id: 1 */
    int32_t l_41 = 6L;
    int32_t l_1315[10][6][2] = {{{0L,0L},{0x39BD47A7L,0L},{0L,0xD3B0B48CL},{0x1E6681D8L,0x6B0D5208L},{0L,0x1E6681D8L},{0xE7390D31L,0x91B645ABL}},{{0xE7390D31L,0x1E6681D8L},{0L,0x6B0D5208L},{0x1E6681D8L,0xD3B0B48CL},{0L,0L},{0x39BD47A7L,0L},{0L,0x8325A5E0L}},{{0x83282A70L,(-1L)},{0xDD88ACB0L,0L},{3L,0x55E26DD0L},{0xD3B0B48CL,0x55E26DD0L},{3L,0L},{0xDD88ACB0L,(-1L)}},{{0x83282A70L,0x8325A5E0L},{0L,0L},{0x39BD47A7L,0L},{0L,0xD3B0B48CL},{0x1E6681D8L,0x6B0D5208L},{0L,0x1E6681D8L}},{{0xE7390D31L,0x91B645ABL},{0xE7390D31L,0x1E6681D8L},{0L,0x6B0D5208L},{0x1E6681D8L,0xD3B0B48CL},{0L,0L},{0x39BD47A7L,0L}},{{0L,0x8325A5E0L},{0x83282A70L,(-1L)},{0xDD88ACB0L,0L},{3L,0x55E26DD0L},{0xD3B0B48CL,0x55E26DD0L},{3L,0L}},{{0xDD88ACB0L,(-1L)},{0x83282A70L,0x8325A5E0L},{0L,0L},{0x39BD47A7L,0L},{0L,0xD3B0B48CL},{0x1E6681D8L,0x6B0D5208L}},{{0L,0x1E6681D8L},{0xE7390D31L,0x91B645ABL},{0xE7390D31L,0x1E6681D8L},{0L,0x6B0D5208L},{0x1E6681D8L,0xD3B0B48CL},{0L,0L}},{{0x39BD47A7L,0L},{0L,0x8325A5E0L},{0x83282A70L,(-1L)},{0xDD88ACB0L,0L},{3L,0x55E26DD0L},{0xD3B0B48CL,0x55E26DD0L}},{{3L,0L},{0xDD88ACB0L,(-1L)},{0x83282A70L,0x8325A5E0L},{0L,0L},{0x39BD47A7L,0L},{0L,0xD3B0B48CL}}};
    int32_t l_1327 = 0x1206A999L;
    int16_t l_1404 = 0L;
    int32_t *l_1440 = &g_986;
    uint32_t l_1486 = 4294967293UL;
    uint32_t **l_1523 = (void*)0;
    uint32_t **l_1524 = (void*)0;
    const int64_t *l_1631 = &g_54;
    const int64_t **l_1630 = &l_1631;
    const int64_t ***l_1629 = &l_1630;
    const int64_t ****l_1628 = &l_1629;
    int32_t l_1766 = (-1L);
    int32_t l_1777 = (-4L);
    int32_t **l_1825[7][10] = {{&g_318,&g_318,(void*)0,&l_1440,&l_1440,&g_318,&l_1440,&l_1440,(void*)0,&g_318},{(void*)0,&l_1440,(void*)0,&g_318,&l_1440,&l_1440,&g_318,&l_1440,&l_1440,&l_1440},{&l_1440,&l_1440,&l_1440,(void*)0,&g_318,&l_1440,&g_318,&g_318,&l_1440,(void*)0},{(void*)0,&g_318,&l_1440,&g_318,&l_1440,&g_318,(void*)0,(void*)0,&g_318,&l_1440},{&l_1440,&l_1440,&l_1440,&l_1440,&l_1440,&g_318,&l_1440,&l_1440,&l_1440,(void*)0},{(void*)0,&l_1440,&l_1440,&g_318,&g_318,&g_318,&l_1440,&l_1440,&l_1440,&l_1440},{(void*)0,&l_1440,&l_1440,&l_1440,&l_1440,(void*)0,&l_1440,&l_1440,&g_318,&l_1440}};
    int32_t ***l_1824 = &l_1825[3][2];
    int32_t l_1846 = (-9L);
    struct S0 **l_1879 = &g_465[6][0][1];
    uint16_t *l_1905 = &g_1425[0];
    uint64_t ** const *l_1925 = &g_1158;
    int32_t *l_1954[3];
    uint8_t l_1964 = 0x2AL;
    const float l_1980[2] = {0x5.Cp+1,0x5.Cp+1};
    uint16_t * const **l_1988[9];
    uint8_t **l_2009 = (void*)0;
    uint8_t ***l_2008 = &l_2009;
    uint64_t ***l_2028 = &g_1158;
    uint32_t ***l_2043 = &l_1523;
    uint32_t ****l_2042 = &l_2043;
    uint32_t *****l_2041[4] = {&l_2042,&l_2042,&l_2042,&l_2042};
    int16_t l_2062 = 0L;
    uint32_t l_2099 = 0x2F289490L;
    struct S0 * const ***l_2249 = &g_1601;
    float ***l_2254 = &g_2136;
    uint64_t * const ****l_2256 = (void*)0;
    uint8_t l_2383 = 0x04L;
    int16_t **l_2401 = (void*)0;
    int64_t ****l_2417 = &g_1465[1];
    uint8_t l_2437 = 255UL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1954[i] = &g_556[0];
    for (i = 0; i < 9; i++)
        l_1988[i] = (void*)0;
    for (p_30 = 0; (p_30 <= (-17)); p_30--)
    { /* block id: 4 */
        int32_t *l_44 = &g_43;
        const int64_t *l_76 = &g_77;
        (*l_44) &= (safe_mul_func_int16_t_s_s(l_41, l_41));
        for (g_43 = 9; (g_43 < (-20)); g_43 = safe_sub_func_uint64_t_u_u(g_43, 7))
        { /* block id: 8 */
            int32_t *l_1303 = &g_109[0][0][3];
            for (l_41 = 4; (l_41 >= 0); l_41 -= 1)
            { /* block id: 11 */
                int64_t *l_53[8][5] = {{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54}};
                int8_t l_58 = 0x57L;
                int32_t *l_75 = (void*)0;
                uint32_t *l_1133 = &g_1134;
                int32_t **l_1302[6][3] = {{&g_502,&g_502,&g_502},{&g_299,&g_502,&g_299},{&g_502,&g_502,&g_502},{&g_299,&g_502,&g_299},{&g_502,&g_502,&g_502},{&g_299,&g_502,&g_299}};
                int i, j;
            }
        }
    }
    return p_30;
}


/* ------------------------------------------ */
/* 
 * reads : g_602 g_23 g_419.f4 g_1111.f5 g_485 g_124 g_1182 g_405 g_406 g_407 g_350 g_1186 g_463.f5 g_1113 g_556 g_1159 g_914 g_501 g_226 g_1201 g_502 g_109 g_1218 g_1221 g_1223 g_466.f5 g_419.f6 g_1111.f4 g_1230 g_332 g_463.f4 g_85 g_649.f2 g_649.f5 g_110 g_97 g_127.f4 g_127.f2 g_847 g_42 g_242 g_243 g_649.f4
 * writes: g_847 g_1158 g_774 g_23 g_453 g_350 g_1186 g_97 g_85 g_266 g_109 g_1223 g_36 g_226 g_106 g_649.f5 g_649.f6 g_42 g_1134 g_556 g_110 g_914 g_649.f4 g_1111.f4
 */
static int32_t * func_47(const uint32_t  p_48, uint8_t  p_49, int32_t * p_50, int32_t * p_51, int8_t  p_52)
{ /* block id: 420 */
    float l_1135 = 0x3.8178D1p+31;
    int32_t *l_1136 = (void*)0;
    int32_t *l_1137[7][10] = {{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]},{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]},{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]},{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]},{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]},{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]},{&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0],&g_556[0]}};
    uint16_t l_1138 = 0x7547L;
    int64_t *l_1147 = &g_847;
    const int8_t l_1148 = 0xD1L;
    uint64_t *l_1156 = (void*)0;
    uint64_t **l_1155 = &l_1156;
    uint64_t ***l_1157[3][6] = {{&l_1155,&l_1155,&l_1155,&l_1155,&l_1155,&l_1155},{&l_1155,&l_1155,&l_1155,&l_1155,&l_1155,&l_1155},{&l_1155,&l_1155,&l_1155,&l_1155,&l_1155,&l_1155}};
    uint64_t **l_1160 = &g_1159;
    float *l_1161[2];
    uint8_t l_1167 = 1UL;
    const struct S0 *l_1187 = &g_1006[0][0];
    const int8_t l_1295 = 0x3AL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_1161[i] = &g_110[0][1][3];
    --l_1138;
lbl_1232:
    (*g_602) = ((0xD.865265p-53 <= ((*g_602) <= (safe_add_func_float_f_f((((((~(!(safe_mod_func_uint64_t_u_u(((p_49 ^ ((*l_1147) = p_52)) , l_1148), p_52)))) >= 4294967295UL) , ((safe_sub_func_float_f_f((g_774[0][1] = ((p_52 >= (safe_div_func_float_f_f(((safe_div_func_float_f_f(((g_1158 = l_1155) == l_1160), g_419.f4)) <= p_48), p_49))) <= g_1111.f5)), 0x6.9F6116p+28)) >= g_485[1])) >= p_49) == 0x7.CC33C8p+50), 0x9.Fp-1)))) >= p_48);
    for (p_52 = 1; (p_52 >= 0); p_52 -= 1)
    { /* block id: 428 */
        int32_t l_1162 = 1L;
        int32_t l_1163 = 0L;
        int32_t l_1164 = 1L;
        int32_t l_1165 = 0x30A41B5FL;
        int32_t l_1166[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
        int8_t *l_1183 = &g_453[0];
        uint32_t *l_1184 = (void*)0;
        uint32_t *l_1185 = &g_1186[5][2];
        const uint16_t l_1198 = 0xF6AFL;
        int32_t **l_1242 = &g_318;
        int8_t l_1257[2];
        const uint16_t *l_1261 = &g_485[1];
        const uint16_t **l_1260 = &l_1261;
        uint64_t **l_1290 = &g_1159;
        uint8_t l_1294 = 4UL;
        int i;
        for (i = 0; i < 2; i++)
            l_1257[i] = 0x8CL;
        if ((*p_51))
            break;
        ++l_1167;
        (*g_602) = ((safe_div_func_float_f_f((safe_sub_func_float_f_f(((void*)0 != &g_406), (safe_add_func_float_f_f((((safe_sub_func_int8_t_s_s(g_124[1][8][0], p_52)) | ((((safe_rshift_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((*l_1185) ^= (g_350 ^= (((*l_1183) = ((void*)0 == g_1182)) ^ (**g_405)))), ((((p_49 , p_52) | l_1166[1]) >= 0xC98CL) & p_49))), g_463.f5)) | l_1163) || (*p_51)) == p_48)) , (*g_602)), (-0x5.Dp-1))))), 0x6.5D2F62p-24)) == 0xB.1FF923p+38);
        if ((*p_50))
        { /* block id: 435 */
            uint32_t l_1197 = 0x2111AC69L;
            int32_t l_1231[5][9] = {{0xCADDB513L,(-1L),0xBAE69BB1L,9L,9L,0xBAE69BB1L,(-1L),0xCADDB513L,0xBAE69BB1L},{0xCADDB513L,(-1L),0xBAE69BB1L,9L,9L,0xBAE69BB1L,(-1L),0xCADDB513L,0xBAE69BB1L},{0xCADDB513L,(-1L),0xBAE69BB1L,9L,9L,0xBAE69BB1L,(-1L),0xCADDB513L,0xBAE69BB1L},{0xCADDB513L,(-1L),0xBAE69BB1L,9L,9L,0xBAE69BB1L,(-1L),0xCADDB513L,0xBAE69BB1L},{0xCADDB513L,(-1L),0xBAE69BB1L,9L,9L,0xBAE69BB1L,(-1L),0xCADDB513L,0xBAE69BB1L}};
            uint32_t l_1243 = 0UL;
            struct S0 *l_1251 = (void*)0;
            uint16_t **l_1262 = &g_402[4];
            int i, j;
            for (g_97 = 0; (g_97 <= 5); g_97 += 1)
            { /* block id: 438 */
                struct S0 *l_1189[5][1][5] = {{{&g_463,&g_463,&g_463,&g_463,&g_463}},{{&g_649,&g_463,&g_649,&g_463,&g_649}},{{&g_463,&g_463,&g_463,&g_463,&g_463}},{{&g_649,&g_463,&g_649,&g_463,&g_649}},{{&g_463,&g_463,&g_463,&g_463,&g_463}}};
                uint16_t ***l_1196 = (void*)0;
                int32_t l_1202 = 8L;
                int32_t **l_1244[4];
                int i, j, k;
                for (i = 0; i < 4; i++)
                    l_1244[i] = &g_318;
                for (g_85 = 0; (g_85 <= 1); g_85 += 1)
                { /* block id: 441 */
                    const struct S0 **l_1188 = &l_1187;
                    uint16_t l_1228 = 65535UL;
                    (*l_1188) = l_1187;
                    for (g_266 = 0; (g_266 <= 1); g_266 += 1)
                    { /* block id: 445 */
                        if ((*p_51))
                            break;
                    }
                    for (l_1167 = 0; (l_1167 <= 9); l_1167 += 1)
                    { /* block id: 450 */
                        if ((*p_50))
                            break;
                        (*l_1188) = l_1189[3][0][3];
                    }
                    if (((((*g_1113) == ((((safe_sub_func_int32_t_s_s((safe_mod_func_uint64_t_u_u(((0xC1L | 0x44L) > p_49), (safe_lshift_func_int8_t_s_s((0x983E12D499F87D6DLL <= 18446744073709551608UL), ((*p_50) , p_52))))), ((l_1197 = ((&g_401[4][0] != l_1196) , p_48)) || (*g_1159)))) || 249UL) <= l_1198) != (*g_501))) , p_52) >= p_48))
                    { /* block id: 455 */
                        int32_t l_1203 = 0x30DBFFB1L;
                        struct S0 ****l_1225 = &g_1223[0];
                        uint8_t *l_1229 = &g_36;
                        int i, j;
                        (*g_502) |= ((0x76412DD7L >= ((safe_mod_func_uint64_t_u_u(p_52, g_1201)) || (p_52 , l_1202))) , 0x95E256EEL);
                        if (l_1203)
                            break;
                        (*g_602) = (safe_mul_func_float_f_f((l_1231[2][4] = (g_774[(g_85 + 2)][g_85] = (((l_1202 = ((safe_div_func_uint32_t_u_u(((safe_add_func_int32_t_s_s(((p_48 || ((p_48 < 7UL) && ((((safe_sub_func_int64_t_s_s(((safe_add_func_int32_t_s_s(((safe_sub_func_float_f_f((((safe_div_func_float_f_f(((((*g_501) = ((g_1218 , ((*l_1229) = (safe_sub_func_int16_t_s_s((g_1221 != ((*l_1225) = g_1223[0])), (((((safe_div_func_uint8_t_u_u((&g_1186[5][2] == (void*)0), p_52)) > g_466.f5) >= 0xB1L) , l_1228) == p_48))))) || (*g_406))) < p_49) , (*g_602)), g_419.f6)) < g_1111.f4) >= g_124[1][6][0]), p_48)) , l_1197), g_914[5])) == l_1166[5]), 0xA99D842B1A710347LL)) < 0x0BL) != p_52) | p_48))) , l_1197), (*p_50))) > g_1230[3][1][1]), 0xCECF2CF3L)) < (*g_332))) || g_463.f4) , p_52))), 0x0.6p-1));
                        if (g_463.f4)
                            goto lbl_1232;
                    }
                    else
                    { /* block id: 466 */
                        if ((*p_51))
                            break;
                    }
                }
                for (g_106 = 0; (g_106 <= 5); g_106 += 1)
                { /* block id: 472 */
                    return l_1161[1];
                }
                for (g_649.f5 = 2; (g_649.f5 >= 0); g_649.f5 -= 1)
                { /* block id: 477 */
                    uint16_t *l_1254 = &l_1138;
                    int32_t l_1263 = 0xA9E4F0C9L;
                    int i, j, k;
                    for (g_649.f6 = 1; (g_649.f6 <= 4); g_649.f6 += 1)
                    { /* block id: 480 */
                        return p_50;
                    }
                    g_42[p_52] = p_51;
                    for (g_1134 = 0; (g_1134 <= 2); g_1134 += 1)
                    { /* block id: 486 */
                        const int64_t *l_1235 = &g_85;
                        const int64_t **l_1234 = &l_1235;
                        const int64_t ** const *l_1233 = &l_1234;
                        const int64_t ** const **l_1236 = &l_1233;
                        int32_t l_1241 = 0x1D533C87L;
                        (*g_502) = (((*l_1236) = l_1233) != &g_1072[8]);
                        (*g_1113) ^= (safe_mul_func_uint16_t_u_u((((*p_51) | (((1UL && ((*l_1185) = (safe_add_func_uint32_t_u_u(l_1241, (((l_1242 = (void*)0) == ((l_1243 || (*g_501)) , l_1244[1])) < g_649.f2))))) >= (p_52 , l_1231[1][0])) & l_1241)) <= l_1241), 1L));
                    }
                    l_1263 = (((safe_add_func_float_f_f(((-0x3.7p-1) <= (safe_add_func_float_f_f(((safe_sub_func_float_f_f(((void*)0 == l_1251), ((safe_sub_func_float_f_f(((void*)0 == l_1254), (safe_sub_func_float_f_f((g_110[g_97][p_52][(p_52 + 2)] = g_110[(p_52 + 2)][p_52][(g_649.f5 + 1)]), (l_1257[1] <= (safe_add_func_float_f_f((((l_1260 == l_1262) < 0x4.159A0Dp+55) != 0x9.9p-1), p_52))))))) == (*g_602)))) < p_52), p_52))), p_49)) != p_48) > p_52);
                }
            }
        }
        else
        { /* block id: 497 */
            float l_1270 = (-0x1.1p-1);
            int32_t l_1273[2][7][1] = {{{(-6L)},{0L},{(-6L)},{0x0011C35BL},{(-5L)},{0x0011C35BL},{(-6L)}},{{0L},{(-6L)},{0x0011C35BL},{(-5L)},{0x0011C35BL},{(-6L)},{0L}}};
            int32_t **l_1274 = (void*)0;
            int32_t **l_1275 = &l_1137[6][4];
            int i, j, k;
            if ((*p_51))
                break;
            (*g_1113) = (*p_50);
            (*l_1275) = ((((p_49 > ((void*)0 != &g_318)) , ((safe_mod_func_uint8_t_u_u((safe_mod_func_int8_t_s_s(p_52, (**g_405))), g_127.f4)) , (((safe_sub_func_float_f_f(p_52, (l_1270 == (+(-((*g_602) != l_1273[0][4][0])))))) == g_127.f2) , l_1273[0][4][0]))) , 0x75D97B0FC70B210BLL) , &l_1273[0][4][0]);
        }
        for (g_847 = 4; (g_847 >= 0); g_847 -= 1)
        { /* block id: 504 */
            int32_t ***l_1288 = &l_1242;
            uint8_t *l_1289 = &g_825;
            int32_t l_1291 = 0xD011BFD7L;
            uint32_t *l_1293 = (void*)0;
            uint32_t **l_1292 = &l_1293;
            int32_t *l_1296 = (void*)0;
            (*g_502) ^= (safe_sub_func_int32_t_s_s((p_52 ^ (((*g_1159) &= ((safe_sub_func_uint16_t_u_u((((((safe_sub_func_uint8_t_u_u(((((*l_1292) = p_50) == g_42[(p_52 + 2)]) == 0x3870L), p_52)) && l_1294) | p_48) >= (*p_50)) , (*g_242)), l_1295)) <= (*g_501))) < p_52)), l_1166[5]));
            return l_1296;
        }
    }
    for (g_649.f4 = 0; (g_649.f4 >= 3); g_649.f4++)
    { /* block id: 516 */
        for (g_1111.f4 = 23; (g_1111.f4 != (-14)); g_1111.f4--)
        { /* block id: 519 */
            int32_t *l_1301[4][8][2] = {{{&g_556[0],&g_556[0]},{&g_109[1][0][2],(void*)0},{&g_556[0],&g_649.f6},{&g_109[2][0][1],&g_649.f6},{(void*)0,&g_109[2][0][1]},{&g_649.f6,&g_649.f6},{&g_649.f6,&g_109[2][0][1]},{(void*)0,&g_649.f6}},{{&g_109[2][0][1],&g_649.f6},{&g_556[0],(void*)0},{&g_109[1][0][2],&g_556[0]},{&g_556[0],(void*)0},{(void*)0,(void*)0},{&g_556[0],&g_556[0]},{&g_109[1][0][2],(void*)0},{&g_556[0],&g_649.f6}},{{&g_109[2][0][1],&g_649.f6},{(void*)0,&g_109[2][0][1]},{&g_649.f6,&g_649.f6},{&g_649.f6,&g_109[2][0][1]},{(void*)0,&g_649.f6},{&g_109[2][0][1],&g_649.f6},{&g_556[0],(void*)0},{&g_109[1][0][2],&g_556[0]}},{{&g_556[0],(void*)0},{(void*)0,(void*)0},{&g_556[0],&g_556[0]},{&g_109[1][0][2],(void*)0},{&g_556[0],&g_649.f6},{&g_109[2][0][1],&g_649.f6},{(void*)0,&g_109[2][0][1]},{&g_649.f6,&g_649.f6}}};
            int i, j, k;
            return l_1301[3][6][1];
        }
        (*g_602) = p_49;
    }
    return l_1161[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_1113 g_649.f0
 * writes: g_649.f0
 */
static int64_t  func_55(int32_t  p_56, int64_t  p_57)
{ /* block id: 411 */
    int32_t *l_1114 = (void*)0;
    int32_t l_1119 = 0x6CB575B6L;
    int32_t l_1120 = 0x1E6C2C68L;
    int32_t l_1121 = 5L;
    int32_t l_1122 = 0L;
    int32_t l_1124 = 0x20D18729L;
    int32_t l_1125 = 0x548EBAEFL;
    int32_t l_1126[9] = {0x456CD180L,0xC9F2301EL,0x456CD180L,0xC9F2301EL,0x456CD180L,0xC9F2301EL,0x456CD180L,0xC9F2301EL,0x456CD180L};
    float l_1128 = (-0x10.Cp-1);
    float l_1129 = 0x8.4p-1;
    int i;
    l_1114 = g_1113;
    for (g_649.f0 = 13; (g_649.f0 < 36); g_649.f0 = safe_add_func_uint64_t_u_u(g_649.f0, 1))
    { /* block id: 415 */
        int32_t *l_1117 = (void*)0;
        int32_t *l_1118[3][3] = {{(void*)0,&g_1111.f6,&g_1111.f6},{(void*)0,&g_1111.f6,&g_1111.f6},{(void*)0,&g_1111.f6,&g_1111.f6}};
        int32_t l_1123 = (-5L);
        int64_t l_1127 = 8L;
        uint32_t l_1130 = 0x710AED58L;
        int i, j;
        l_1130--;
    }
    return p_57;
}


/* ------------------------------------------ */
/* 
 * reads : g_466.f5 g_602 g_23 g_466.f4 g_109 g_427 g_406 g_407 g_501 g_226 g_366 g_397 g_463.f6 g_85 g_405 g_825 g_160 g_847 g_321 g_101 g_463.f4 g_243 g_887 g_224.f2 g_502 g_332 g_124 g_914 g_466.f6 g_350 g_463.f5 g_465 g_463 g_466 g_774 g_1111 g_1006.f5
 * writes: g_466.f5 g_110 g_23 g_774 g_321 g_427 g_463.f0 g_402 g_463.f6 g_109 g_101 g_85 g_226 g_160 g_649.f4 g_463.f4 g_887 g_634 g_466.f6 g_825
 */
static uint32_t  func_66(int32_t * p_67, int32_t  p_68, uint16_t  p_69)
{ /* block id: 246 */
    int64_t *l_764[7] = {&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101};
    const float *l_768 = &g_224.f1;
    int32_t l_770 = (-3L);
    float *l_789 = &g_110[1][0][1];
    float **l_790 = &l_789;
    uint16_t *l_791 = &g_321;
    int8_t l_801 = 0x3EL;
    uint64_t *l_802 = &g_427;
    uint64_t l_807 = 0xA2637B0E1F3ED9ECLL;
    int32_t l_827 = 1L;
    struct S0 **l_835 = &g_465[1][0][2];
    uint8_t *l_848 = (void*)0;
    int32_t l_856 = 0x78D3A78CL;
    int32_t l_857 = (-1L);
    int32_t l_858[9] = {0x215050D8L,0x215050D8L,0x215050D8L,0x215050D8L,0x215050D8L,0x215050D8L,0x215050D8L,0x215050D8L,0x215050D8L};
    int16_t *l_1001 = (void*)0;
    int16_t *l_1004 = &g_463.f4;
    uint8_t l_1042[5][2] = {{3UL,0x7CL},{254UL,0x7CL},{255UL,3UL},{254UL,254UL},{254UL,3UL}};
    uint16_t l_1043 = 0xBED9L;
    int32_t l_1061[10][1][8] = {{{0xF3302960L,4L,0x06C81AE8L,(-1L),0x90C6A09CL,0xFCCCB90CL,0x90C6A09CL,(-1L)}},{{0x90C6A09CL,0xFCCCB90CL,0x90C6A09CL,(-1L),0x06C81AE8L,4L,0xF3302960L,0xFA78B82DL}},{{1L,0x06C81AE8L,0x17E48F54L,4L,4L,0x17E48F54L,0x06C81AE8L,1L}},{{1L,(-1L),0xCF0A254CL,0x90C6A09CL,0x06C81AE8L,0x3BAE9599L,0x17E48F54L,0x3BAE9599L}},{{0x90C6A09CL,0x0FD6C96BL,0xFA78B82DL,0x0FD6C96BL,0x90C6A09CL,0x3BAE9599L,0xFCCCB90CL,0x06C81AE8L}},{{0xF3302960L,(-1L),0x0FD6C96BL,6L,0x17E48F54L,0x17E48F54L,6L,0x0FD6C96BL}},{{0x06C81AE8L,0x06C81AE8L,0x0FD6C96BL,1L,0xCF0A254CL,4L,0xFCCCB90CL,0xF3302960L}},{{0x17E48F54L,0xFCCCB90CL,0xFA78B82DL,0x06C81AE8L,0xFA78B82DL,0xFCCCB90CL,0x17E48F54L,0xF3302960L}},{{0xFCCCB90CL,4L,0xCF0A254CL,1L,0x0FD6C96BL,0x06C81AE8L,0x06C81AE8L,0x0FD6C96BL}},{{6L,0x17E48F54L,0x17E48F54L,6L,0x0FD6C96BL,(-1L),0xF3302960L,0x06C81AE8L}}};
    int64_t **l_1063 = &l_764[3];
    int64_t ***l_1062 = &l_1063;
    int64_t l_1105 = (-2L);
    int i, j, k;
    for (g_466.f5 = 4; (g_466.f5 > 21); g_466.f5 = safe_add_func_int64_t_s_s(g_466.f5, 1))
    { /* block id: 249 */
        const float l_745 = 0x1.Dp+1;
        const int32_t l_746 = 0x99846610L;
        float *l_757 = &g_110[0][1][3];
        struct S0 **l_760 = &g_465[6][0][0];
        int64_t **l_765 = &l_764[4];
        int16_t l_766 = (-1L);
        int32_t l_767[4][2] = {{0x18753FAFL,0L},{0L,0x18753FAFL},{0L,0L},{0x18753FAFL,0L}};
        int32_t l_769 = (-2L);
        float *l_771 = (void*)0;
        float *l_772 = &g_23;
        float *l_773 = &g_774[0][1];
        int i, j;
        if (l_746)
            break;
        if (l_746)
            break;
        (*l_773) = (safe_mul_func_float_f_f(((*l_772) = (((safe_div_func_float_f_f(((safe_div_func_float_f_f(((l_769 = ((safe_mul_func_float_f_f((safe_sub_func_float_f_f((*g_602), 0x1.Bp-1)), (((*l_757) = l_746) >= ((((((l_760 != (((safe_mod_func_uint16_t_u_u(1UL, 0xBC18L)) , (!(((*l_765) = l_764[0]) == (void*)0))) , &g_465[7][0][0])) , l_766) > l_767[3][1]) < 0UL) , p_67) != l_768)))) == 0x9.3DD3B0p-47)) < l_770), g_466.f4)) < (-0x4.Dp-1)), 0x9.B87889p+29)) > (-0x1.1p+1)) == g_109[2][1][2])), g_427));
        if (g_427)
            goto lbl_803;
    }
lbl_803:
    l_770 = (safe_div_func_uint32_t_u_u(0x4A90632DL, (((safe_add_func_uint64_t_u_u(((*l_802) |= ((safe_mul_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(0x26L, (safe_div_func_int8_t_s_s((safe_div_func_int8_t_s_s((p_69 , (safe_mul_func_uint16_t_u_u(((*l_791) = (p_69 ^= (l_768 == ((*l_790) = l_789)))), ((safe_div_func_int16_t_s_s((safe_unary_minus_func_int16_t_s(p_68)), 7L)) >= ((safe_sub_func_uint16_t_u_u(l_770, ((safe_add_func_uint8_t_u_u((*g_406), 255UL)) != p_68))) <= l_770))))), l_801)), p_68)))), 0x0639L)) != (*g_501))), 0xF0124CF70A19C8C8LL)) < g_366) & 6UL)));
    for (g_463.f0 = 0; g_463.f0 < 6; g_463.f0 += 1)
    {
        g_402[g_463.f0] = &g_321;
    }
    if ((((l_801 || 0L) ^ ((~(((void*)0 == p_67) >= (p_69 , (safe_div_func_uint32_t_u_u((l_807 >= (g_397[0] , 1L)), 0x2A35A502L))))) == p_69)) ^ p_68))
    { /* block id: 265 */
        int32_t l_817[7][4] = {{0xA1E74BCAL,2L,2L,0xA1E74BCAL},{2L,0xA1E74BCAL,2L,2L},{0xA1E74BCAL,0xA1E74BCAL,(-1L),0xA1E74BCAL},{0xA1E74BCAL,2L,2L,0xA1E74BCAL},{2L,0xA1E74BCAL,2L,2L},{0xA1E74BCAL,0xA1E74BCAL,(-1L),0xA1E74BCAL},{0xA1E74BCAL,2L,2L,0xA1E74BCAL}};
        int64_t * const l_823 = &g_101;
        int i, j;
        (*p_67) |= 0L;
        for (l_770 = 0; l_770 < 4; l_770 += 1)
        {
            for (g_101 = 0; g_101 < 2; g_101 += 1)
            {
                g_774[l_770][g_101] = 0xC.0EC5F8p-55;
            }
        }
        for (g_85 = (-27); (g_85 != (-29)); g_85--)
        { /* block id: 270 */
            const int16_t l_824 = (-9L);
            int32_t *l_826[3];
            int i;
            for (i = 0; i < 3; i++)
                l_826[i] = (void*)0;
            l_827 &= ((((safe_mod_func_uint8_t_u_u(p_68, p_68)) , (safe_add_func_int32_t_s_s(((*p_67) = ((((safe_mul_func_int8_t_s_s((safe_unary_minus_func_int8_t_s((((*g_501) = (l_817[3][0] <= (((**g_405) > (safe_mod_func_uint16_t_u_u((0x958D1F86L ^ (4294967294UL >= (((safe_lshift_func_uint8_t_u_u(((((!((((void*)0 == l_823) , ((*g_501) ^ l_770)) || p_68)) <= p_69) && l_824) < 0xBD57L), g_466.f5)) , p_69) & l_807))), p_69))) , 0x77L))) , l_824))), 0UL)) != p_69) , p_68) == l_801)), g_825))) ^ l_807) > 5UL);
        }
    }
    else
    { /* block id: 275 */
        int32_t l_845[10][2][8] = {{{0xD150EDE9L,(-1L),(-9L),0xB9F10FDBL,0xF0D6E88DL,0x0C5C8639L,0xF0D6E88DL,0xB9F10FDBL},{4L,0x4DA3CB2CL,4L,0xC6BB3CE5L,0xB9F10FDBL,0x0C5C8639L,(-9L),(-9L)}},{{(-9L),(-1L),0xD150EDE9L,0xD150EDE9L,(-1L),(-9L),0xB9F10FDBL,0xF0D6E88DL},{(-9L),0xA0B3C195L,0x9C84C09EL,(-1L),0xB9F10FDBL,(-1L),0x9C84C09EL,0xA0B3C195L}},{{4L,0x9C84C09EL,0x0C5C8639L,(-1L),0xF0D6E88DL,0xC6BB3CE5L,0xC6BB3CE5L,0xF0D6E88DL},{0xD150EDE9L,0xF0D6E88DL,0xF0D6E88DL,0xD150EDE9L,4L,0xA0B3C195L,0xC6BB3CE5L,(-9L)}},{{0x9C84C09EL,0xD150EDE9L,0x0C5C8639L,0xC6BB3CE5L,0x0C5C8639L,0xD150EDE9L,0x9C84C09EL,0xB9F10FDBL},{0x0C5C8639L,0xD150EDE9L,0x9C84C09EL,0xB9F10FDBL,0xA0B3C195L,0xA0B3C195L,0xB9F10FDBL,0x9C84C09EL}},{{0xF0D6E88DL,0xF0D6E88DL,0xD150EDE9L,4L,0xA0B3C195L,0x9C84C09EL,4L,0x9C84C09EL},{(-9L),0x4DA3CB2CL,(-1L),0x4DA3CB2CL,(-9L),0xD150EDE9L,0xC6BB3CE5L,0x9C84C09EL}},{{0x4DA3CB2CL,0x0C5C8639L,4L,(-1L),(-1L),4L,0x0C5C8639L,0x4DA3CB2CL},{0xF0D6E88DL,0xD150EDE9L,4L,0xA0B3C195L,0xC6BB3CE5L,(-9L),0xC6BB3CE5L,0xA0B3C195L}},{{(-1L),0xB9F10FDBL,(-1L),0x9C84C09EL,0xA0B3C195L,(-9L),4L,4L},{4L,0xD150EDE9L,0xF0D6E88DL,0xF0D6E88DL,0xD150EDE9L,4L,0xA0B3C195L,0xC6BB3CE5L}},{{4L,0x0C5C8639L,0x4DA3CB2CL,0xD150EDE9L,0xA0B3C195L,0xD150EDE9L,0x4DA3CB2CL,0x0C5C8639L},{(-1L),0x4DA3CB2CL,(-9L),0xD150EDE9L,0xC6BB3CE5L,0x9C84C09EL,0x9C84C09EL,0xC6BB3CE5L}},{{0xF0D6E88DL,0xC6BB3CE5L,0xC6BB3CE5L,0xF0D6E88DL,(-1L),0x0C5C8639L,0x9C84C09EL,4L},{0x4DA3CB2CL,0xF0D6E88DL,(-9L),0x9C84C09EL,(-9L),0xF0D6E88DL,0x4DA3CB2CL,0xA0B3C195L}},{{(-9L),0xF0D6E88DL,0x4DA3CB2CL,0xA0B3C195L,0x0C5C8639L,0x0C5C8639L,0xA0B3C195L,0x4DA3CB2CL},{0xC6BB3CE5L,0xC6BB3CE5L,0xF0D6E88DL,(-1L),0x0C5C8639L,0x9C84C09EL,4L,0x9C84C09EL}}};
        const int32_t l_846 = 0L;
        int8_t l_860 = 2L;
        uint8_t l_861 = 0x40L;
        int16_t l_871 = (-1L);
        int32_t l_880 = 0x36523AD9L;
        int32_t l_882 = (-10L);
        int8_t *l_1048 = &l_860;
        int8_t **l_1047 = &l_1048;
        int16_t * const * const l_1097 = (void*)0;
        int16_t l_1098 = 0x5DF3L;
        int64_t l_1102 = 0xA0AAC8848D009537LL;
        const int16_t *l_1112 = &g_463.f4;
        int i, j, k;
        for (g_160 = 0; (g_160 < (-27)); g_160 = safe_sub_func_int64_t_s_s(g_160, 2))
        { /* block id: 278 */
            int8_t l_849 = 0xC6L;
            uint16_t *l_850[6];
            int32_t l_855[8] = {(-1L),0xF39C8BC7L,(-1L),0xF39C8BC7L,(-1L),0xF39C8BC7L,(-1L),0xF39C8BC7L};
            int i;
            for (i = 0; i < 6; i++)
                l_850[i] = (void*)0;
            (*p_67) &= (((0x9C65L == 0UL) , (p_69 = ((p_68 , (safe_unary_minus_func_uint16_t_u(((*l_791) |= ((((**g_405) , (l_807 == ((safe_sub_func_uint8_t_u_u((l_835 == &g_465[2][0][1]), (0xEDL | (((safe_mod_func_int16_t_s_s(((((((+(((l_845[4][0][3] = (safe_lshift_func_int16_t_s_u((safe_div_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(7L, (*g_501))), 0x2A920863L)), 2))) ^ l_846) > g_847)) , (void*)0) != l_848) >= l_846) && (**g_405)) & p_68), (-10L))) >= 0xE8582E2FL) > l_849)))) , 0x81CBL))) <= l_770) || p_69))))) > 1UL))) < l_849);
            for (g_101 = 0; (g_101 == 5); g_101 = safe_add_func_uint64_t_u_u(g_101, 2))
            { /* block id: 285 */
                int32_t *l_853 = &l_845[4][0][3];
                int32_t *l_854[3];
                int8_t l_859 = (-6L);
                int i;
                for (i = 0; i < 3; i++)
                    l_854[i] = &g_556[0];
                l_861--;
            }
        }
        for (l_801 = 4; (l_801 >= 0); l_801 -= 1)
        { /* block id: 291 */
            uint32_t l_868 = 0x029B679AL;
            int32_t l_872[8] = {(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)};
            int i;
            for (g_649.f4 = 1; (g_649.f4 >= 0); g_649.f4 -= 1)
            { /* block id: 294 */
                int32_t l_873 = (-1L);
                int32_t l_874 = 0x7218104EL;
                int32_t l_875 = 0L;
                int32_t l_877 = (-1L);
                int32_t l_878 = 0x92812B52L;
                int32_t l_879 = (-10L);
                int32_t l_881 = 0xA692033DL;
                int32_t l_883 = 0x6FBE7FB9L;
                uint32_t l_938 = 0x021C8671L;
                for (g_463.f4 = 1; (g_463.f4 >= 0); g_463.f4 -= 1)
                { /* block id: 297 */
                    int32_t l_867 = 0x2C71707BL;
                    int32_t l_876[4] = {(-1L),(-1L),(-1L),(-1L)};
                    int64_t l_885 = 0x615FC377B195D598LL;
                    int i;
                    for (g_463.f0 = 0; (g_463.f0 <= 6); g_463.f0 += 1)
                    { /* block id: 300 */
                        int32_t *l_864 = (void*)0;
                        int32_t *l_865 = &l_856;
                        int32_t *l_866[4][9][7] = {{{&g_463.f6,&g_556[0],&l_856,&l_856,&g_556[0],&g_463.f6,&g_556[0]},{&g_556[0],(void*)0,&l_845[6][0][3],&l_858[3],&l_858[3],&l_845[6][0][3],(void*)0},{&g_556[0],&g_556[0],&g_463.f6,&g_556[0],&l_856,&l_856,&g_556[0]},{&g_466.f6,(void*)0,&g_466.f6,&l_858[7],(void*)0,&l_770,&l_770},{(void*)0,&g_556[0],&g_43,&g_556[0],(void*)0,&g_43,&g_466.f6},{&l_858[3],&l_770,&l_858[7],&l_858[3],&l_858[7],&l_770,&l_858[3]},{&g_463.f6,&g_466.f6,&g_556[0],&l_856,&g_466.f6,&l_856,&g_556[0]},{&l_858[3],&l_858[3],&l_845[6][0][3],(void*)0,&g_556[0],&l_845[6][0][3],&g_556[0]},{(void*)0,&g_556[0],&g_556[0],(void*)0,&l_856,&g_463.f6,(void*)0}},{{&g_466.f6,&g_556[0],&l_858[7],&l_858[7],&g_556[0],&g_466.f6,&l_770},{&g_556[0],(void*)0,&g_43,&g_466.f6,&g_466.f6,&g_43,(void*)0},{&g_556[0],&l_770,&g_466.f6,&l_858[7],&g_109[2][1][2],&g_109[2][1][2],&l_858[7]},{(void*)0,&g_556[0],(void*)0,&g_466.f6,&g_556[0],&g_43,&g_43},{&l_770,&l_858[7],&l_858[3],&l_858[7],&l_770,&l_858[3],&g_466.f6},{&g_463.f6,&g_43,&g_466.f6,&g_463.f6,&g_466.f6,&g_43,&g_463.f6},{&g_43,&g_466.f6,&l_845[6][0][3],&g_109[2][1][2],&g_466.f6,&g_109[2][1][2],&l_845[6][0][3]},{&g_463.f6,&g_463.f6,&g_466.f6,&g_556[0],&l_856,&g_466.f6,&l_856},{&l_770,&l_845[6][0][3],&l_845[6][0][3],&l_770,&g_109[2][1][2],&g_43,&l_770}},{{(void*)0,&l_856,&g_466.f6,&g_466.f6,&l_856,(void*)0,&g_43},{&l_858[7],&l_770,&l_858[3],&g_466.f6,&g_466.f6,&l_858[3],&l_770},{&l_856,&g_43,(void*)0,&l_856,&g_466.f6,&g_466.f6,&l_856},{&g_43,&l_770,&g_43,&g_109[2][1][2],&l_770,&l_845[6][0][3],&l_845[6][0][3]},{&g_556[0],&l_856,&g_466.f6,&l_856,&g_556[0],&g_466.f6,&g_463.f6},{&g_466.f6,&l_845[6][0][3],&g_109[2][1][2],&g_466.f6,&g_109[2][1][2],&l_845[6][0][3],&g_466.f6},{(void*)0,&g_463.f6,&g_43,&g_466.f6,&g_463.f6,&g_466.f6,&g_43},{&g_466.f6,&g_466.f6,&l_858[3],&l_770,&l_858[7],&l_858[3],&l_858[7]},{&g_556[0],&g_43,&g_43,&g_556[0],&g_466.f6,(void*)0,&g_556[0]}},{{&g_43,&l_858[7],&g_109[2][1][2],&g_109[2][1][2],&l_858[7],&g_43,&l_845[6][0][3]},{&l_856,&g_556[0],&g_466.f6,&g_463.f6,&g_463.f6,&g_466.f6,&g_556[0]},{&l_858[7],&l_845[6][0][3],&g_43,&l_858[7],&g_109[2][1][2],&g_109[2][1][2],&l_858[7]},{(void*)0,&g_556[0],(void*)0,&g_466.f6,&g_556[0],&g_43,&g_43},{&l_770,&l_858[7],&l_858[3],&l_858[7],&l_770,&l_858[3],&g_466.f6},{&g_463.f6,&g_43,&g_466.f6,&g_463.f6,&g_466.f6,&g_43,&g_463.f6},{&g_43,&g_466.f6,&l_845[6][0][3],&g_109[2][1][2],&g_466.f6,&g_109[2][1][2],&l_845[6][0][3]},{&g_463.f6,&g_463.f6,&g_466.f6,&g_556[0],&l_856,&g_466.f6,&l_856},{&l_770,&l_845[6][0][3],&l_845[6][0][3],&l_770,&g_109[2][1][2],&g_43,&l_770}}};
                        float l_884[6] = {(-0x8.1p+1),(-0x8.1p+1),(-0x8.1p+1),(-0x8.1p+1),(-0x8.1p+1),(-0x8.1p+1)};
                        int32_t l_886 = (-1L);
                        int i, j, k;
                        if (g_243[(g_463.f4 + 2)][(g_463.f4 + 4)])
                            break;
                        l_868--;
                        g_887--;
                        (*p_67) = ((safe_div_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((safe_lshift_func_int8_t_s_s(g_224.f2, 1)), ((((((((void*)0 != &l_848) & (l_876[1] = (safe_add_func_int64_t_s_s(p_68, (((*l_802)--) && ((*g_502) | (safe_div_func_int8_t_s_s((-3L), (safe_lshift_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((5L >= p_68), (((*l_791)--) & (*g_501)))), 7)))))))))) ^ 0L) == 0xF73F7124E8F9D0D9LL) < g_243[3][3]) & (-3L)) == l_868))), (*g_332))) || 65535UL);
                    }
                    (*g_502) = (safe_sub_func_int8_t_s_s(p_69, (safe_sub_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((**g_405), 0xF1L)), ((*l_791) = (l_807 , g_914[1]))))));
                }
            }
            for (g_466.f5 = 4; (g_466.f5 >= 0); g_466.f5 -= 1)
            { /* block id: 326 */
                int32_t **l_942 = &g_634[1];
                (*l_942) = &p_68;
            }
        }
        for (g_466.f6 = 1; (g_466.f6 <= 5); g_466.f6 += 1)
        { /* block id: 332 */
            uint32_t l_946 = 0UL;
            int32_t l_968 = 0x76D30B80L;
            int32_t l_969[7][3][10] = {{{0x495AE733L,9L,0x495AE733L,(-1L),(-1L),0x6C424911L,0x120676ADL,0xA8563B1CL,0xA8563B1CL,0x120676ADL},{(-1L),(-1L),(-1L),(-1L),0xF3F23576L,0x72A687A9L,(-1L),0x120676ADL,(-1L),0xE6B5F9A8L},{1L,0xE6B5F9A8L,0x6C424911L,9L,0x120676ADL,9L,0x6C424911L,0xE6B5F9A8L,1L,0x72A687A9L}},{{1L,(-1L),0xA8563B1CL,(-1L),9L,0x72A687A9L,0x72A687A9L,9L,(-1L),0xA8563B1CL},{0x72A687A9L,0x72A687A9L,9L,(-1L),0xA8563B1CL,(-1L),1L,0x495AE733L,1L,(-1L)},{0x6C424911L,9L,0x120676ADL,9L,0x6C424911L,0xE6B5F9A8L,1L,0x72A687A9L,(-1L),(-1L)}},{{(-1L),0x72A687A9L,0xF3F23576L,(-1L),(-1L),0xF3F23576L,0x72A687A9L,(-1L),0x120676ADL,(-1L)},{0x495AE733L,(-1L),0x72A687A9L,(-1L),0x6C424911L,1L,0x6C424911L,(-1L),0x72A687A9L,(-1L)},{0xF3F23576L,0xE6B5F9A8L,0x72A687A9L,0x6C424911L,0xA8563B1CL,(-1L),(-1L),(-1L),(-1L),0xA8563B1CL}},{{(-1L),0xF3F23576L,0xF3F23576L,(-1L),9L,(-1L),0x495AE733L,0x72A687A9L,0xA8563B1CL,0x72A687A9L},{0xF3F23576L,1L,0x120676ADL,0x72A687A9L,0x120676ADL,1L,0xF3F23576L,0x495AE733L,0xA8563B1CL,0xE6B5F9A8L},{0x495AE733L,(-1L),9L,(-1L),0xF3F23576L,0xF3F23576L,(-1L),9L,(-1L),0x495AE733L}},{{(-1L),(-1L),0xA8563B1CL,0x6C424911L,0x72A687A9L,0xE6B5F9A8L,0xF3F23576L,0xE6B5F9A8L,0x72A687A9L,0x6C424911L},{0x6C424911L,1L,0x6C424911L,(-1L),0x72A687A9L,(-1L),0x495AE733L,0x120676ADL,0x120676ADL,0x495AE733L},{0x72A687A9L,0xF3F23576L,(-1L),(-1L),0xF3F23576L,0x72A687A9L,(-1L),0x120676ADL,(-1L),0xE6B5F9A8L}},{{1L,0xE6B5F9A8L,0x6C424911L,9L,0x120676ADL,9L,0x6C424911L,0xE6B5F9A8L,1L,0x72A687A9L},{1L,(-1L),0xA8563B1CL,(-1L),9L,0x72A687A9L,0x72A687A9L,9L,(-1L),0xA8563B1CL},{0x72A687A9L,0x72A687A9L,9L,(-1L),0xA8563B1CL,(-1L),1L,0x495AE733L,1L,(-1L)}},{{0x6C424911L,9L,0x120676ADL,9L,0x6C424911L,0xE6B5F9A8L,1L,0x72A687A9L,(-1L),(-1L)},{(-1L),0x72A687A9L,0xF3F23576L,(-1L),(-1L),0xF3F23576L,0x72A687A9L,(-1L),0x120676ADL,(-1L)},{0x495AE733L,(-1L),0x72A687A9L,0xE6B5F9A8L,(-1L),(-1L),(-1L),0xE6B5F9A8L,9L,0xF3F23576L}}};
            uint16_t **l_989 = &g_402[1];
            int16_t *l_1005 = &g_649.f4;
            int32_t l_1106 = 0x15389D45L;
            int i, j, k;
            for (g_825 = 0; (g_825 <= 5); g_825 += 1)
            { /* block id: 335 */
                int64_t **l_943 = &g_199[0][8][4];
                int64_t ***l_944 = &l_943;
                int32_t l_945[2][2][5] = {{{5L,8L,5L,5L,8L},{8L,5L,5L,8L,5L}},{{8L,8L,1L,8L,8L},{5L,8L,5L,5L,8L}}};
                uint8_t *l_964 = &g_106;
                uint64_t l_970[1][9] = {{18446744073709551614UL,18446744073709551614UL,18446744073709551614UL,18446744073709551614UL,18446744073709551614UL,18446744073709551614UL,18446744073709551614UL,18446744073709551614UL,18446744073709551614UL}};
                uint32_t l_987 = 0x663BBDAEL;
                uint16_t * const *l_990 = &g_402[2];
                int32_t *l_1015 = &l_882;
                uint16_t l_1041[1];
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_1041[i] = 7UL;
                p_67 = (void*)0;
                (*l_944) = l_943;
            }
            for (g_160 = 0; (g_160 <= 0); g_160 += 1)
            { /* block id: 391 */
                uint64_t l_1088 = 6UL;
                (**l_790) = (l_1098 = ((safe_div_func_float_f_f(((+(((safe_mod_func_int8_t_s_s((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u((safe_div_func_uint16_t_u_u(0x11D8L, (l_1088 | ((safe_mul_func_int8_t_s_s(l_946, g_350)) , (--(*l_802)))))), (safe_mod_func_int16_t_s_s((l_882 | (((safe_mod_func_int32_t_s_s((p_69 & ((0x9F1B4F2AB85A6EFCLL > 0x0D59D4E865BD433ALL) & l_1088)), l_1042[1][1])) == 0xE6CD18F01FEA5E4CLL) , p_68)), 0xFE6FL)))) >= l_1061[7][0][0]), 251UL)), l_1088)) , l_1097) == (void*)0)) < (*g_602)), g_463.f5)) > 0x6.3FFFDCp+86));
            }
            for (l_770 = 0; (l_770 >= 0); l_770 -= 1)
            { /* block id: 398 */
                int32_t *l_1100 = &l_880;
                int32_t *l_1101[1][1][3];
                int8_t l_1104[7][2][4] = {{{2L,0x3CL,0x1AL,5L},{1L,0x6EL,0x93L,0x1AL}},{{0x66L,0x6EL,0x66L,5L},{0x6EL,0x3CL,(-1L),(-1L)}},{{(-1L),0x1AL,1L,0x3CL},{0xC3L,1L,1L,0xC3L}},{{(-1L),5L,(-1L),0x66L},{0x6EL,0x05L,0x66L,0x06L}},{{0x66L,0x06L,0x93L,0x06L},{1L,0x05L,0x1AL,0x66L}},{{2L,5L,0x06L,0xC3L},{0x1AL,1L,0x3CL,0x3CL}},{{0x1AL,0x1AL,0x06L,(-1L)},{2L,0x3CL,0x1AL,5L}}};
                uint32_t l_1107 = 18446744073709551615UL;
                int16_t *l_1110 = (void*)0;
                int i, j, k;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 3; k++)
                            l_1101[i][j][k] = &l_858[3];
                    }
                }
                for (g_226 = 1; (g_226 >= 0); g_226 -= 1)
                { /* block id: 401 */
                    uint64_t l_1099 = 18446744073709551615UL;
                    int i, j;
                    l_1099 = ((**l_835) , (-3L));
                    g_774[(g_226 + 2)][g_226] = g_774[l_770][g_226];
                }
                l_1107--;
                (*l_1100) |= (l_1110 == (g_1111 , l_1112));
            }
        }
    }
    return g_1006[0][0].f5;
}


/* ------------------------------------------ */
/* 
 * reads : g_84 g_85 g_43 g_36 g_405 g_406 g_419 g_267 g_224.f5 g_224.f0 g_299 g_124 g_397 g_453 g_332 g_407 g_466.f6 g_298 g_463.f5 g_331 g_106 g_466.f7 g_77 g_109 g_502 g_160 g_463.f6 g_501 g_226 g_556 g_97 g_466.f0 g_243 g_233 g_463.f4 g_224.f4 g_463.f0 g_466.f4 g_602 g_630 g_242 g_402 g_321 g_634 g_366 g_427 g_649 g_127.f7 g_330 g_718 g_740
 * writes: g_36 g_85 g_97 g_427 g_299 g_226 g_350 g_453 g_465 g_466.f6 g_463.f5 g_106 g_501 g_160 g_463.f6 g_466.f0 g_401 g_109 g_110 g_23 g_321 g_402 g_366 g_634 g_233 g_397 g_199 g_84
 */
static int32_t * func_70(uint16_t  p_71, int32_t * p_72, uint32_t  p_73, int32_t  p_74)
{ /* block id: 13 */
    uint16_t l_508[2];
    int64_t *l_519 = &g_101;
    int32_t *l_521 = (void*)0;
    int32_t *l_522 = (void*)0;
    int32_t *l_523 = &g_463.f6;
    uint16_t **l_525 = &g_402[3];
    struct S0 *** const l_555 = (void*)0;
    int16_t l_601 = 0xD941L;
    uint8_t *l_614[2];
    uint8_t **l_613[2];
    int32_t l_673 = 0x6F7F3A75L;
    int32_t l_676 = 1L;
    int32_t l_677[1];
    int32_t l_685 = 1L;
    uint32_t l_700 = 0x63AD6CA6L;
    struct S0 *l_709 = &g_649;
    int32_t **l_724 = (void*)0;
    const int32_t l_727[3] = {0x0ACB2E23L,0x0ACB2E23L,0x0ACB2E23L};
    int64_t **l_728 = &g_199[1][5][4];
    int64_t **l_729 = &g_84;
    int i;
    for (i = 0; i < 2; i++)
        l_508[i] = 65535UL;
    for (i = 0; i < 2; i++)
        l_614[i] = &g_233;
    for (i = 0; i < 2; i++)
        l_613[i] = &l_614[1];
    for (i = 0; i < 1; i++)
        l_677[i] = 0x23E94A45L;
    if (((*l_523) &= (func_78(g_84, func_86(func_89((*g_84), p_73, &g_43), (safe_unary_minus_func_uint64_t_u((safe_lshift_func_uint16_t_u_u(((((safe_add_func_uint64_t_u_u(l_508[0], (((safe_mod_func_uint32_t_u_u((l_508[0] >= (((l_508[0] || l_508[1]) != l_508[0]) | 0x4FC3L)), (*g_332))) && p_74) ^ l_508[0]))) == 0x580C63F3L) < 0x6033L) == 0xA6C4B74FA4154E9CLL), p_73))))), &g_109[2][1][2], l_519, (*g_332)) ^ l_508[0])))
    { /* block id: 172 */
        uint64_t l_534 = 0xC54A32ABA41DFAA4LL;
        uint16_t *l_639 = &l_508[1];
        int32_t l_642 = (-6L);
        int8_t l_663[7][10][3] = {{{(-1L),1L,0xD9L},{(-1L),0xA7L,(-9L)},{0x0FL,4L,(-1L)},{(-4L),0x42L,(-1L)},{0L,4L,0x3CL},{(-10L),0xA7L,0x32L},{1L,1L,(-8L)},{0x9CL,(-1L),(-1L)},{0x12L,0L,(-10L)},{(-9L),0xF7L,0x68L}},{{0xF7L,0x14L,1L},{(-8L),1L,1L},{0x0FL,(-1L),1L},{0x0FL,3L,0xF7L},{(-8L),(-10L),(-1L)},{0xF7L,1L,0x32L},{(-9L),0xA7L,(-1L)},{0x12L,(-10L),8L},{0x9CL,0x9CL,0xF7L},{1L,0xF7L,(-10L)}},{{(-10L),0x68L,0xD9L},{0L,0x14L,(-4L)},{(-4L),(-10L),0xD9L},{0x0FL,(-1L),(-10L)},{(-1L),4L,0xF7L},{(-1L),0x56L,8L},{0L,1L,(-1L)},{0x72L,(-1L),0x32L},{0x12L,0x72L,(-1L)},{(-4L),(-1L),0xF7L}},{{0x68L,0x68L,1L},{(-9L),0x68L,1L},{(-1L),1L,0x47L},{4L,(-10L),(-1L)},{8L,1L,(-3L)},{(-8L),0L,0x14L},{1L,(-1L),1L},{(-1L),0L,0L},{(-10L),0x68L,(-8L)},{0x32L,(-3L),0xB9L}},{{1L,0xBBL,0x14L},{0x32L,0x9CL,0x38L},{(-10L),(-4L),0x9AL},{(-1L),0xF1L,1L},{1L,(-3L),(-1L)},{(-8L),0x12L,0x56L},{8L,0L,1L},{4L,0xB7L,1L},{(-1L),0xF7L,(-8L)},{0x38L,1L,(-8L)}},{{(-1L),0x42L,1L},{0x05L,0xBBL,1L},{0x3CL,(-4L),0x56L},{(-10L),(-1L),(-1L)},{1L,1L,1L},{0xB9L,0x42L,0x9AL},{8L,0x12L,0x38L},{(-1L),0L,0x14L},{4L,(-1L),0xB9L},{1L,0L,(-8L)}},{{(-3L),0x12L,0L},{0x32L,0x42L,1L},{0xF1L,1L,0x14L},{0x3CL,(-1L),(-3L)},{0x38L,(-4L),(-1L)},{(-4L),0xBBL,0x47L},{1L,0x42L,0x32L},{(-1L),1L,0x56L},{(-1L),0xF7L,(-4L)},{1L,0xB7L,0xB9L}}};
        int32_t l_668 = 0xA4B8EC7DL;
        int32_t l_678[2];
        int8_t l_693[7];
        uint16_t l_704[9][5][5] = {{{65529UL,0xE8CAL,0xB478L,0x0A0CL,0x8196L},{0UL,4UL,0xDBFAL,1UL,4UL},{65529UL,2UL,0x765BL,1UL,1UL},{0UL,0x5ED7L,65527UL,0UL,4UL},{0xEAEFL,0x8196L,0xCBB3L,0x5E38L,0x8196L}},{{65526UL,0xB4ABL,1UL,0UL,65526UL},{0x252BL,0xC200L,0xB478L,1UL,0xE8CAL},{0UL,0xC9A3L,1UL,1UL,0xC9A3L},{65532UL,2UL,0xCBB3L,0x0A0CL,1UL},{0xC07BL,0xC9A3L,65527UL,0x30AEL,0x5ED7L}},{{0xEAEFL,0xC200L,0x765BL,0x5E38L,0xC200L},{0xC07BL,0xB4ABL,0xDBFAL,65535UL,65526UL},{65532UL,0x8196L,0xB478L,65532UL,0xC200L},{0UL,0x5ED7L,0xC350L,1UL,0x5ED7L},{0x252BL,2UL,0x5E38L,65532UL,1UL}},{{65526UL,65529UL,4UL,0xB4ABL,0xFADDL},{4UL,65535UL,65529UL,65529UL,65535UL},{0xECF9L,65534UL,65535UL,65526UL,65535UL},{0UL,65535UL,0xC200L,1UL,0x7023L},{0xCFB9L,65529UL,0UL,0xA176L,65529UL}},{{0UL,0x9DB7L,65535UL,2UL,0x6196L},{0xECF9L,0xFF8CL,4UL,65530UL,65529UL},{4UL,0x7023L,65534UL,65529UL,0x7023L},{0x34CEL,65534UL,0xA176L,65530UL,65535UL},{65535UL,0x9639L,0xC200L,2UL,65535UL}},{{0xCFB9L,0xFADDL,0xA176L,0xA176L,0xFADDL},{65530UL,0x9DB7L,65534UL,1UL,0x6196L},{8UL,0xFADDL,4UL,65526UL,0xFF8CL},{4UL,0x9639L,65535UL,65529UL,0x9639L},{8UL,65534UL,0UL,0xB4ABL,65535UL}},{{65530UL,0x7023L,0xC200L,0xCBB9L,0x9639L},{0xCFB9L,0xFF8CL,65535UL,0xA176L,0xFF8CL},{65535UL,0x9DB7L,65529UL,0xCBB9L,0x6196L},{0x34CEL,65529UL,4UL,0xB4ABL,0xFADDL},{4UL,65535UL,65529UL,65529UL,65535UL}},{{0xECF9L,65534UL,65535UL,65526UL,65535UL},{0UL,65535UL,0xC200L,1UL,0x7023L},{0xCFB9L,65529UL,0UL,0xA176L,65529UL},{0UL,0x9DB7L,65535UL,2UL,0x6196L},{0xECF9L,0xFF8CL,4UL,65530UL,65529UL}},{{4UL,0x7023L,65534UL,65529UL,0x7023L},{0x34CEL,65534UL,0xA176L,65530UL,65535UL},{65535UL,0x9639L,0xC200L,2UL,65535UL},{0xCFB9L,0xFADDL,0xA176L,0xA176L,0xFADDL},{65530UL,0x9DB7L,65534UL,1UL,0x6196L}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_678[i] = 0x46C60BABL;
        for (i = 0; i < 7; i++)
            l_693[i] = 0x99L;
lbl_703:
        for (g_466.f0 = 0; (g_466.f0 <= 1); g_466.f0 += 1)
        { /* block id: 175 */
            uint64_t l_553 = 0xA94DDCBCCEF71879LL;
            int8_t *l_554 = &g_453[0];
            uint16_t ***l_557 = &g_401[2][0];
            int32_t l_666 = 0L;
            int32_t l_670 = 0L;
            int32_t l_680 = 0x5E709025L;
            int32_t l_684 = 0x496EC488L;
            int32_t l_686 = 3L;
            int32_t l_688 = 0x759C0FCDL;
            int32_t l_689 = 0x764D9A14L;
            int32_t l_692 = (-7L);
            int32_t l_694 = (-1L);
            int32_t l_698[3][5][4] = {{{0x9922B833L,0x22B261CBL,0x36F0772FL,0L},{1L,0x5F37DE0DL,(-1L),0L},{0x24103BD1L,0x22B261CBL,0x24103BD1L,(-1L)},{0xF71C0705L,0x9922B833L,0x7591CA88L,0xF71C0705L},{1L,(-1L),0xF6A21284L,0x9922B833L}},{{(-1L),0x22B261CBL,0xF6A21284L,0xF6A21284L},{1L,1L,0x7591CA88L,0L},{0xF71C0705L,0x6C25C211L,0x24103BD1L,0x9922B833L},{0x24103BD1L,0x9922B833L,(-1L),0x24103BD1L},{1L,0x9922B833L,0x36F0772FL,0x9922B833L}},{{0x9922B833L,0x6C25C211L,(-1L),0x6C25C211L},{0x7591CA88L,(-1L),0xFF77C1DCL,(-1L)},{0xF6A21284L,0x5F37DE0DL,0L,0x24103BD1L},{0xF6A21284L,0xFF77C1DCL,0xFF77C1DCL,0xF6A21284L},{0x7591CA88L,0x24103BD1L,(-1L),0xFF77C1DCL}}};
            int i, j, k;
            (*g_502) ^= (p_74 != ((!((l_525 == ((*l_557) = ((safe_mod_func_int64_t_s_s(((safe_mul_func_uint8_t_u_u(((void*)0 == &g_402[3]), (safe_add_func_uint32_t_u_u(0UL, ((safe_add_func_uint32_t_u_u((l_534 && (*l_523)), (safe_div_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s((((((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s(((*g_501) == (safe_div_func_uint8_t_u_u((((*l_554) = (safe_mod_func_int32_t_s_s(((l_553 == p_73) ^ p_74), l_534))) , p_74), l_534))), l_534)) >= l_553), l_534)) , l_534) , (*g_501)), 8)), 5)) , p_73) , (void*)0) != l_555) , g_556[0]), l_534)), p_71)), l_534)))) > l_553))))) > p_73), (*g_84))) , l_525))) | (*g_501))) <= 0x34A42975L));
            for (l_553 = 0; (l_553 <= 2); l_553 += 1)
            { /* block id: 181 */
                int8_t l_599 = 0L;
                int8_t l_632[4] = {(-1L),(-1L),(-1L),(-1L)};
                int32_t l_669 = 0xE706010CL;
                int32_t l_674 = 9L;
                int32_t l_675 = 3L;
                int32_t l_679 = (-1L);
                int32_t l_681 = 0x117FDFA5L;
                int32_t l_682 = 2L;
                int32_t l_687 = 1L;
                int32_t l_690 = 0x318C3DCAL;
                int32_t l_691 = 0xBD84A99DL;
                int32_t l_695 = (-7L);
                int32_t l_696 = 0xE86583D0L;
                int32_t l_697[5][9][5] = {{{(-1L),0x1487CD6EL,0x17364B8CL,(-1L),0x6A96F5A7L},{(-2L),0xC2C56BE1L,0xA92DF712L,1L,0x2671C041L},{1L,(-1L),7L,0x6A96F5A7L,0x9C0B7B45L},{0x2671C041L,(-6L),0xF20EFC95L,0xEFDB89C9L,0xF20EFC95L},{0x228C26E9L,0x228C26E9L,0x60CC79F0L,0L,0x0934FC7FL},{0L,(-5L),0L,0x6E9C83C5L,0L},{(-5L),0x17364B8CL,0x7D195272L,0x5688B370L,0xA321927BL},{8L,(-5L),0xA92DF712L,0x4449917CL,0L},{7L,0x228C26E9L,0x1487CD6EL,0xA321927BL,0x443E76E0L}},{{0xAB1B8998L,(-6L),0L,0L,0xC41B4B39L},{0x0934FC7FL,(-1L),0xB14DF7A7L,0x228C26E9L,1L},{0L,0xC2C56BE1L,(-10L),0xE8CFADC8L,8L},{1L,0x1487CD6EL,0xE1DBD29AL,0x228C26E9L,0xA321927BL},{1L,1L,0x3D3A31FAL,0L,(-2L)},{1L,(-5L),0xA321927BL,0xA321927BL,(-5L)},{0L,0xD97870BEL,9L,0x4449917CL,0xF20EFC95L},{0L,0x0934FC7FL,0xB14DF7A7L,0x5688B370L,(-1L)},{9L,0xEFDB89C9L,0x6AB649B6L,0x6E9C83C5L,1L}},{{0L,0x1487CD6EL,2L,0L,0x6A96F5A7L},{0L,0L,0xA92DF712L,0xEFDB89C9L,1L},{1L,0x9C0B7B45L,1L,0x6A96F5A7L,0L},{1L,(-6L),0xC41B4B39L,1L,0xF20EFC95L},{1L,0x443E76E0L,0x60CC79F0L,(-1L),0x5688B370L},{0L,0x4449917CL,0xC41B4B39L,0x6E9C83C5L,(-1L)},{0x0934FC7FL,0x17364B8CL,1L,(-5L),0xA321927BL},{0xAB1B8998L,4L,0xA92DF712L,4L,0xAB1B8998L},{7L,1L,2L,0xA321927BL,0x228C26E9L}},{{8L,(-6L),0x6AB649B6L,9L,0xC41B4B39L},{(-5L),0L,0xB14DF7A7L,1L,0x228C26E9L},{0L,9L,9L,0xE8CFADC8L,0xAB1B8998L},{0x228C26E9L,0x1487CD6EL,0xA321927BL,0x443E76E0L,0xA321927BL},{0x2671C041L,4L,0x3D3A31FAL,0xC2C56BE1L,(-1L)},{1L,0x5688B370L,0xE1DBD29AL,0xA321927BL,0x5688B370L},{(-2L),0xD97870BEL,(-10L),(-5L),(-3L)},{0x17364B8CL,0xA321927BL,1L,0x6A96F5A7L,0x1487CD6EL},{(-3L),0xDA0B4B65L,0x0261C91FL,6L,9L}},{{2L,0xA19EF0BDL,0xA19EF0BDL,2L,0L},{0xC41B4B39L,0xD97870BEL,1L,0xDA0B4B65L,0L},{0x2DC687C4L,0x1487CD6EL,(-1L),0L,0x17364B8CL},{0L,0x4D1E345FL,1L,0xDA0B4B65L,(-3L)},{7L,1L,0x443E76E0L,2L,0xE1DBD29AL},{0L,0L,(-3L),6L,0L},{0xA321927BL,0xB14DF7A7L,0xBE963D19L,0x6A96F5A7L,0x7C193594L},{0L,0xE8CFADC8L,1L,0x6E9C83C5L,0x6AB649B6L},{0xBE963D19L,7L,0xB14DF7A7L,0x7C193594L,1L}}};
                int i, j, k;
                for (g_97 = 0; (g_97 <= 0); g_97 += 1)
                { /* block id: 184 */
                    uint8_t *l_571[1][8][7] = {{{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_267,(void*)0,&g_233,&g_106,&g_106,&g_233,(void*)0},{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_267,(void*)0,&g_233,&g_106,&g_106,&g_233,(void*)0},{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_267,(void*)0,&g_233,&g_106,&g_106,&g_233,(void*)0},{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_267,(void*)0,&g_233,&g_106,&g_106,&g_233,(void*)0}}};
                    int32_t l_572[5] = {3L,3L,3L,3L,3L};
                    int16_t l_619 = 0L;
                    int i, j, k;
                    if (((safe_lshift_func_int16_t_s_u((safe_mul_func_int8_t_s_s(g_397[g_97], 0UL)), (~(safe_mod_func_int8_t_s_s((+((((-9L) <= (!0L)) , (safe_div_func_int8_t_s_s(g_243[(g_97 + 2)][g_466.f0], (g_453[g_97] = 0xEBL)))) == (safe_mul_func_uint8_t_u_u((++g_106), ((+(safe_mod_func_uint64_t_u_u((((safe_sub_func_int64_t_s_s(l_534, (safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(((safe_rshift_func_uint16_t_u_s(0x9308L, (safe_lshift_func_int8_t_s_u(0x48L, p_74)))) <= 1L), 0)), l_572[0])))) <= l_553) , g_233), g_397[0]))) < p_71))))), (-1L)))))) >= 0xA1B649CEFE801D9CLL))
                    { /* block id: 187 */
                        int16_t l_598[8][5] = {{(-1L),0x8D35L,0L,0x86BCL,0x86BCL},{0x86BCL,0x89D4L,0x86BCL,(-10L),0x697FL},{0L,0x8D35L,(-1L),0xED82L,0x697FL},{(-1L),1L,1L,(-1L),0x86BCL},{(-1L),(-1L),(-1L),0x697FL,0x8D35L},{(-1L),(-1L),0x86BCL,(-1L),(-1L)},{(-1L),0xED82L,0L,0x697FL,1L},{0L,0xED82L,(-1L),(-1L),0xED82L}};
                        float *l_600 = &g_110[2][1][1];
                        int i, j;
                        (*l_557) = (((*g_84) = l_534) , l_525);
                        (*g_602) = (((((l_553 , g_243[(g_97 + 2)][g_466.f0]) < (((*l_600) = (p_71 != ((safe_mul_func_float_f_f(g_77, ((safe_mul_func_float_f_f((g_463.f4 >= (safe_add_func_float_f_f((safe_div_func_float_f_f((safe_add_func_float_f_f(((((g_224.f4 < g_463.f0) < ((&g_402[3] == ((p_71 , l_598[6][0]) , (void*)0)) == g_556[0])) != g_419.f4) == p_74), g_466.f4)), 0xB.F912FFp+64)), l_599))), 0x1.9p+1)) <= p_74))) < p_71))) > p_71)) <= l_601) >= p_71) == 0x1.272CE4p-10);
                    }
                    else
                    { /* block id: 192 */
                        uint64_t *l_631[8] = {&g_97,&g_97,&g_427,&g_97,&g_97,&g_427,&g_97,&g_97};
                        int32_t **l_633 = &l_521;
                        int i, j, k;
                        g_109[g_466.f0][g_466.f0][(l_553 + 1)] ^= (safe_rshift_func_int8_t_s_s((65535UL | (*g_501)), (safe_lshift_func_int16_t_s_s((&g_397[0] != (void*)0), l_534))));
                        (*l_523) &= (safe_mul_func_int16_t_s_s(0L, ((safe_div_func_int16_t_s_s((((l_632[0] = (safe_lshift_func_uint16_t_u_u((l_613[1] != (p_73 , &g_406)), ((**l_525) ^= (l_599 , ((g_109[g_466.f0][g_466.f0][(l_553 + 1)] <= (safe_add_func_int8_t_s_s((((safe_lshift_func_int16_t_s_u((*g_501), ((((((l_619 > (safe_sub_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(((safe_add_func_int64_t_s_s(((safe_add_func_uint16_t_u_u((safe_sub_func_int16_t_s_s((*g_501), p_71)), 0xE5F2L)) <= (-9L)), 18446744073709551615UL)) | g_630), 254UL)) < g_36), 0xC6L))) , (*g_242)) != 0xF0AEL) < l_534) , 0x4BL) && p_74))) & g_466.f6) > 3UL), (-1L)))) & 0xD5AFL)))))) || 6L) & l_553), (*g_501))) , l_534)));
                        (*l_633) = &p_74;
                        if (g_419.f2)
                            goto lbl_703;
                    }
                    return g_634[6];
                }
                if (p_74)
                    continue;
                for (l_599 = 0; (l_599 >= 0); l_599 -= 1)
                { /* block id: 204 */
                    uint16_t *l_640 = (void*)0;
                    int32_t l_644 = 0x35D1A430L;
                    int32_t l_667 = 0x818F649CL;
                    int32_t l_671 = 0xDBA76FC7L;
                    int32_t l_672 = 0xD77EB1CFL;
                    int32_t l_683[4][2][5] = {{{(-1L),1L,(-1L),2L,3L},{2L,(-6L),3L,2L,(-1L)}},{{3L,0x7B7421BCL,(-6L),(-6L),0x7B7421BCL},{0x7B7421BCL,0xFA3C62D0L,3L,(-1L),0x325ABCFBL}},{{1L,0xFA3C62D0L,(-1L),0L,0x1767971CL},{3L,0xF8413FABL,0xF8413FABL,3L,1L}},{{0L,0x7B7421BCL,0x1767971CL,2L,1L},{0xF8413FABL,0L,2L,0L,2L}}};
                    int32_t l_699 = (-10L);
                    int i, j, k;
                    for (g_85 = 2; (g_85 >= 0); g_85 -= 1)
                    { /* block id: 207 */
                        uint32_t *l_641 = &g_366;
                        int32_t **l_643 = &g_634[0];
                        int i, j, k;
                        if (g_397[l_599])
                            break;
                        (*l_523) = (g_397[l_599] , (g_109[(g_466.f0 + 2)][l_599][(l_553 + 1)] = (((safe_div_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s((((*l_525) = l_639) != l_640), (((((((g_397[l_599] , func_86(((*l_643) = func_86(func_86(l_523, p_71), (&l_525 != ((l_642 |= ((*l_641) ^= (((&p_74 != &p_74) , g_630) ^ 0L))) , (void*)0)))), p_74)) != l_641) && 2L) != l_644) | (-6L)) >= (*l_523)) & (*g_501)))), l_534)) & 0UL) >= g_427)));
                        l_642 ^= p_73;
                    }
                    for (g_463.f5 = 1; (g_463.f5 >= 0); g_463.f5 -= 1)
                    { /* block id: 219 */
                        uint64_t *l_662[2];
                        int32_t *l_664 = &g_463.f6;
                        int32_t *l_665[10][9] = {{&g_556[0],&g_649.f6,&g_556[0],&g_556[0],&g_649.f6,&g_43,&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_556[0],&g_556[0]},{&g_556[0],&g_109[2][1][2],(void*)0,&g_556[0],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_556[0],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_556[0],(void*)0},{&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_556[0],&g_649.f6,&g_649.f6,&g_556[0],&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&l_642},{&g_109[2][1][2],&g_556[0],&l_642,&g_649.f6,&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_43,&g_109[2][1][2],&g_109[2][1][2],&g_43},{&g_649.f6,&g_556[0],&g_556[0],&g_556[0],&g_649.f6,&g_556[0],&g_556[0],&g_649.f6,&g_43},{&g_109[2][1][2],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],(void*)0,&g_556[0],&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_556[0],&g_556[0],&g_109[2][1][2],&l_642},{&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_109[2][1][2],&g_556[0],&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_556[0],&g_109[2][1][2],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],(void*)0},{&g_556[0],&g_649.f6,&g_556[0],&g_556[0],&g_649.f6,&g_43,&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_556[0],&g_556[0]},{&g_556[0],&g_109[2][1][2],(void*)0,&g_556[0],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_556[0],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_556[0],(void*)0},{&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&g_556[0],&g_649.f6,&g_649.f6,&g_556[0],&g_109[g_466.f0][l_599][(g_466.f0 + 2)],&g_109[(l_599 + 2)][g_463.f5][(l_553 + 2)],&l_642}};
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_662[i] = &l_534;
                        g_109[l_553][l_599][(g_466.f0 + 3)] |= 0L;
                        g_109[g_466.f0][l_599][(g_466.f0 + 2)] = (((safe_mul_func_int16_t_s_s((safe_mod_func_int64_t_s_s((((g_649 , l_521) == &p_74) < (g_397[l_599] = ((safe_mod_func_uint8_t_u_u((g_233 &= 0xFEL), ((safe_mod_func_int8_t_s_s(g_397[l_599], g_127.f7)) , (safe_mul_func_uint8_t_u_u((safe_add_func_int32_t_s_s(((l_642 ^= ((g_330 | ((safe_mul_func_uint16_t_u_u(0x5B69L, (safe_div_func_int32_t_s_s((l_632[2] == p_74), (*l_523))))) | 0x4CF5C13EL)) <= 0xA31116A0L)) < 0x633ED7C079769FF5LL), 0x079D2F2CL)), 0x79L))))) != l_663[6][5][2]))), 0x17DE0EAEF298AEC8LL)), (*g_501))) < (*g_84)) > 0x4A4DD5D4L);
                        l_700--;
                        if (l_689)
                            break;
                    }
                }
            }
        }
        (*l_523) = l_704[8][2][1];
    }
    else
    { /* block id: 233 */
        struct S0 *l_710 = &g_466;
        uint16_t **l_712 = &g_402[3];
        uint16_t ***l_713 = (void*)0;
        uint16_t ***l_714[4][1][10];
        int16_t **l_717 = (void*)0;
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 10; k++)
                    l_714[i][j][k] = &l_525;
            }
        }
        (*g_718) = func_89((safe_rshift_func_int16_t_s_s((safe_mod_func_int8_t_s_s((l_709 == l_710), p_73)), 13)), (!((((((((g_401[3][0] = l_712) == ((((safe_sub_func_int32_t_s_s((*l_523), (&g_397[0] == (void*)0))) && g_463.f4) ^ 0L) , &g_242)) , p_73) , g_124[1][6][0]) ^ g_243[3][3]) != 0x942CL) , l_717) != &g_501)), &g_109[0][1][0]);
    }
    (*l_523) |= (0UL || (safe_mod_func_uint8_t_u_u(0x1DL, (+(safe_div_func_int32_t_s_s((((l_724 == &g_318) ^ (-1L)) != (safe_lshift_func_int16_t_s_u(((l_727[0] , (l_519 != ((*l_729) = ((*l_728) = (void*)0)))) & ((0xD758L && 0x8416L) != (*g_501))), 12))), 0x97B2EA78L))))));
    for (l_673 = 0; (l_673 == (-12)); l_673 = safe_sub_func_int32_t_s_s(l_673, 1))
    { /* block id: 242 */
        (*g_502) = ((((safe_mod_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((*g_501), 1)), (safe_div_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(g_224.f5, 3)), ((g_740 , ((safe_lshift_func_uint16_t_u_s(((*l_523) || (*g_502)), 14)) , 0xFA7CL)) | 65532UL))))) ^ p_71) , (g_366 , 0x306CL)) > p_73);
    }
    return &g_109[0][0][4];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_78(int64_t * p_79, int32_t * p_80, int32_t * p_81, int64_t * p_82, int32_t  p_83)
{ /* block id: 169 */
    int64_t l_520[9] = {0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL,0xB1E7CAB860160B36LL};
    int i;
    return l_520[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_160 g_502 g_109
 * writes: g_160
 */
static int32_t * func_86(int32_t * p_87, int32_t  p_88)
{ /* block id: 159 */
    uint16_t l_511 = 0x72C9L;
    int32_t *l_514[2];
    uint16_t l_515[1][1];
    uint64_t l_516 = 0x54378250759897D7LL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_514[i] = &g_463.f6;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_515[i][j] = 0xDC75L;
    }
    for (g_160 = 0; (g_160 >= 0); g_160 -= 1)
    { /* block id: 162 */
        return p_87;
    }
    l_511++;
    l_515[0][0] &= (*g_502);
    l_516++;
    return p_87;
}


/* ------------------------------------------ */
/* 
 * reads : g_43 g_36 g_85 g_405 g_406 g_419 g_267 g_224.f5 g_224.f0 g_299 g_124 g_84 g_397 g_453 g_332 g_407 g_466.f6 g_298 g_463.f5 g_331 g_106 g_466.f7 g_77 g_109 g_502 g_463.f6 g_556
 * writes: g_36 g_85 g_97 g_427 g_299 g_226 g_350 g_453 g_465 g_466.f6 g_463.f5 g_106 g_501
 */
static int32_t * func_89(int64_t  p_90, int32_t  p_91, int32_t * p_92)
{ /* block id: 14 */
    float l_103[2][1];
    int32_t l_107[3][9] = {{0L,1L,1L,(-2L),(-2L),1L,1L,0L,8L},{(-1L),0xD83399F1L,0L,2L,1L,1L,2L,0L,0xD83399F1L},{(-2L),0L,(-1L),8L,1L,0xBBF73BC9L,0xBBF73BC9L,1L,8L}};
    int8_t l_189 = 9L;
    int32_t *l_282 = &g_266;
    uint8_t l_289[4] = {0x37L,0x37L,0x37L,0x37L};
    uint16_t *l_320[7][5][3] = {{{(void*)0,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{(void*)0,&g_321,&g_321},{&g_321,&g_321,&g_321}},{{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321}},{{&g_321,&g_321,&g_321},{&g_321,&g_321,(void*)0},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321}},{{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321}},{{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,(void*)0},{&g_321,&g_321,&g_321}},{{(void*)0,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{(void*)0,&g_321,&g_321},{&g_321,&g_321,&g_321}},{{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,&g_321,&g_321},{&g_321,(void*)0,&g_321},{&g_321,&g_321,&g_321}}};
    uint16_t **l_319 = &l_320[4][2][1];
    uint32_t *l_349 = &g_350;
    uint64_t *l_362 = &g_97;
    uint8_t l_365 = 0UL;
    int32_t l_424 = 0xF048799DL;
    uint32_t l_459 = 0x3DF656AFL;
    const uint16_t *l_484 = &g_485[1];
    uint64_t l_486 = 18446744073709551615UL;
    int16_t *l_499 = &g_466.f4;
    int16_t **l_500 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_103[i][j] = (-0x6.Fp-1);
    }
    if ((*p_92))
    { /* block id: 15 */
        const int32_t *l_123 = &g_124[1][6][0];
        int32_t l_130 = 0x78B87F18L;
        int32_t l_135 = (-9L);
        int32_t l_149 = 0xDCB1B098L;
        int32_t l_150 = (-1L);
        int32_t l_151 = 0x40F3676AL;
        int32_t l_152[6];
        const uint8_t *l_173 = &g_36;
        uint8_t *l_177 = (void*)0;
        const int32_t *l_329 = &g_330;
        int64_t l_376 = 0x5960D218D4FA7841LL;
        uint64_t *l_396 = &g_97;
        uint16_t **l_403[8];
        int i;
        for (i = 0; i < 6; i++)
            l_152[i] = 0x061763C0L;
        for (i = 0; i < 8; i++)
            l_403[i] = (void*)0;
        for (g_36 = 0; (g_36 <= 4); g_36 += 1)
        { /* block id: 18 */
            int8_t l_93 = 0x1EL;
            float l_102[3][10][2] = {{{0x4.6A2B7Fp+97,0xC.A61894p-55},{0x2.4E94FAp+78,(-0x10.8p-1)},{(-0x2.7p-1),0xE.5685F0p-13},{(-0x5.Fp+1),0x0.B10513p-68},{0x0.B10513p-68,0x4.6A2B7Fp+97},{(-0x6.Bp+1),0x4.6A2B7Fp+97},{0x0.B10513p-68,0x0.B10513p-68},{(-0x5.Fp+1),0xE.5685F0p-13},{(-0x2.7p-1),(-0x10.8p-1)},{0x2.4E94FAp+78,0xC.A61894p-55}},{{0x4.6A2B7Fp+97,0x2.4E94FAp+78},{0x0.Dp-1,0xF.168752p-49},{0x0.Dp-1,0x2.4E94FAp+78},{0x4.6A2B7Fp+97,0xC.A61894p-55},{0x2.4E94FAp+78,(-0x10.8p-1)},{(-0x2.7p-1),0xE.5685F0p-13},{(-0x5.Fp+1),0x0.B10513p-68},{0x0.B10513p-68,0x4.6A2B7Fp+97},{(-0x6.Bp+1),0x4.6A2B7Fp+97},{0x0.B10513p-68,0x0.B10513p-68}},{{(-0x5.Fp+1),0xE.5685F0p-13},{(-0x2.7p-1),(-0x10.8p-1)},{0x2.4E94FAp+78,0xC.A61894p-55},{0x4.6A2B7Fp+97,0x2.4E94FAp+78},{0x0.Dp-1,0xF.168752p-49},{0x0.Dp-1,0x2.4E94FAp+78},{0x4.6A2B7Fp+97,0xC.A61894p-55},{0x2.4E94FAp+78,(-0x10.8p-1)},{(-0x2.7p-1),0xE.5685F0p-13},{(-0x5.Fp+1),0x0.B10513p-68}}};
            float l_121[9] = {0x3.5p+1,(-0x7.Bp+1),0x3.5p+1,(-0x7.Bp+1),0x3.5p+1,(-0x7.Bp+1),0x3.5p+1,(-0x7.Bp+1),0x3.5p+1};
            uint32_t l_140 = 0xE2808CFBL;
            int32_t l_146 = 0L;
            int32_t l_147[7][4][8] = {{{0x34962E96L,(-4L),0x1C853B70L,1L,0xC2ACA09AL,0L,1L,0x5993BAFFL},{0x96E0AB15L,(-4L),(-7L),0x84F830CDL,0xB49003B3L,0x20809C30L,0L,1L},{0x96E0AB15L,0x1C853B70L,0L,0L,0xC2ACA09AL,1L,2L,1L},{0x34962E96L,0xF86CA3B9L,(-4L),0x84F830CDL,1L,0xE7CFBBA9L,2L,0x5993BAFFL}},{{0x3D22D4EFL,0L,0L,1L,0xC7BD3F7DL,0xE7CFBBA9L,0L,(-1L)},{0x84F830CDL,0xF86CA3B9L,(-7L),0x3D22D4EFL,0xC7BD3F7DL,1L,1L,0xC7BD3F7DL},{0x3D22D4EFL,0x1C853B70L,0x1C853B70L,0x3D22D4EFL,1L,0x20809C30L,(-1L),(-1L)},{0x34962E96L,(-4L),0x1C853B70L,1L,0xC2ACA09AL,0L,0L,0xC7BD3F7DL}},{{0x42BC22F1L,0x8DFD47EAL,(-4L),1L,(-1L),1L,(-1L),0xB49003B3L},{0x42BC22F1L,0L,(-7L),0x96E0AB15L,1L,0L,0x20809C30L,0xB49003B3L},{0x84F830CDL,0x1C853B70L,0x8DFD47EAL,1L,0xB49003B3L,2L,0x20809C30L,0xC7BD3F7DL},{0x34962E96L,0L,(-7L),0L,8L,2L,(-1L),0x5993BAFFL}},{{1L,0x1C853B70L,(-4L),0x34962E96L,8L,0L,0L,8L},{0x34962E96L,0L,0L,0x34962E96L,0xB49003B3L,1L,0xA5ED00F0L,0x5993BAFFL},{0x84F830CDL,0x8DFD47EAL,0L,0L,1L,(-1L),0L,0xC7BD3F7DL},{0x42BC22F1L,0x8DFD47EAL,(-4L),1L,(-1L),1L,(-1L),0xB49003B3L}},{{0x42BC22F1L,0L,(-7L),0x96E0AB15L,1L,0L,0x20809C30L,0xB49003B3L},{0x84F830CDL,0x1C853B70L,0x8DFD47EAL,1L,0xB49003B3L,2L,0x20809C30L,0xC7BD3F7DL},{0x34962E96L,0L,(-7L),0L,8L,2L,(-1L),0x5993BAFFL},{1L,0x1C853B70L,(-4L),0x34962E96L,8L,0L,0L,8L}},{{0x34962E96L,0L,0L,0x34962E96L,0xB49003B3L,1L,0xA5ED00F0L,0x5993BAFFL},{0x84F830CDL,0x8DFD47EAL,0L,0L,1L,(-1L),0L,0xC7BD3F7DL},{0x42BC22F1L,0x8DFD47EAL,(-4L),1L,(-1L),1L,(-1L),0xB49003B3L},{0x42BC22F1L,0L,(-7L),0x96E0AB15L,1L,0L,0x20809C30L,0xB49003B3L}},{{0x84F830CDL,0x1C853B70L,0x8DFD47EAL,1L,0xB49003B3L,2L,0x20809C30L,0xC7BD3F7DL},{0x34962E96L,0L,(-7L),0L,8L,2L,(-1L),0x5993BAFFL},{1L,0x1C853B70L,(-4L),0x34962E96L,8L,0L,0L,8L},{0x34962E96L,0L,0L,0x34962E96L,0xB49003B3L,1L,0xA5ED00F0L,0x5993BAFFL}}};
            uint16_t l_167[10];
            uint8_t **l_203 = (void*)0;
            int64_t l_263[7] = {0xEE659F9DB165CC77LL,0xEE659F9DB165CC77LL,2L,0xEE659F9DB165CC77LL,0xEE659F9DB165CC77LL,2L,0xEE659F9DB165CC77LL};
            int32_t l_290 = 1L;
            uint8_t l_295 = 0x13L;
            uint64_t l_341[4] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
            uint32_t l_378 = 4294967295UL;
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_167[i] = 65526UL;
            for (g_85 = 4; (g_85 >= 0); g_85 -= 1)
            { /* block id: 21 */
                uint64_t *l_96 = &g_97;
                int64_t *l_100 = &g_101;
                uint8_t l_104 = 6UL;
                uint8_t *l_105 = &g_106;
                int32_t *l_108 = &g_109[2][1][2];
                int32_t l_134 = 1L;
                int32_t l_137 = (-1L);
                int32_t l_138[5][4] = {{0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL},{0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL},{0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL},{0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL},{0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL,0xFDF6FDFCL}};
                int32_t *l_143 = &l_137;
                const uint8_t **l_174 = (void*)0;
                const uint8_t **l_175 = &l_173;
                uint8_t **l_176 = (void*)0;
                float l_229[6];
                const int32_t *l_371 = &l_147[6][0][7];
                uint32_t l_408 = 1UL;
                int i, j;
                for (i = 0; i < 6; i++)
                    l_229[i] = 0xE.4EE88Ep-93;
            }
        }
    }
    else
    { /* block id: 115 */
        uint8_t *l_416 = &l_289[0];
        uint8_t **l_415 = &l_416;
        int32_t l_425 = 0L;
        uint64_t *l_426 = &g_427;
        int32_t **l_428 = &g_299;
        float *l_433[6] = {&g_110[2][1][1],&g_110[3][1][3],&g_110[3][1][3],&g_110[2][1][1],&g_110[3][1][3],&g_110[3][1][3]};
        int8_t *l_444 = &l_189;
        int16_t *l_445[1][6];
        int32_t *l_451 = (void*)0;
        int32_t **l_450 = &l_451;
        int8_t *l_452 = &g_453[0];
        int32_t l_457 = 1L;
        int32_t l_458 = 0L;
        struct S0 *l_462 = &g_463;
        uint8_t l_481 = 5UL;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 6; j++)
                l_445[i][j] = &g_226;
        }
        (*l_428) = (((safe_div_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u((4294967293UL < ((safe_rshift_func_uint16_t_u_s((l_107[2][0] , 65535UL), 4)) > ((((*l_415) = &l_289[1]) != (*g_405)) > (p_91 | ((*l_426) = ((*l_362) = ((safe_sub_func_uint32_t_u_u((l_424 ^= (p_90 ^ (g_419 , (safe_mul_func_int8_t_s_s((safe_sub_func_int16_t_s_s(l_107[1][7], p_90)), 5L))))), g_267)) < l_425))))))), g_224.f5)), (*p_92))) > l_425) , &l_107[1][7]);
        if ((safe_rshift_func_uint16_t_u_s((safe_add_func_uint8_t_u_u(((void*)0 == l_433[5]), ((safe_div_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((safe_add_func_int8_t_s_s((((*l_452) ^= ((safe_sub_func_uint32_t_u_u(((*l_349) = (((((*l_444) = g_224.f0) && l_424) , (g_226 = (**l_428))) < (((*g_84) = ((safe_lshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_u(l_289[0], 4)), (((*l_450) = &p_91) != &p_91))) || (((p_91 , 0L) <= g_124[1][6][0]) , l_365))) <= l_424))), g_397[0])) <= l_107[1][7])) & l_107[1][7]), p_91)), 250UL)), l_107[1][8])), p_91)) && (-1L)))), p_91)))
        { /* block id: 127 */
            int32_t *l_454 = &g_109[2][1][2];
            int32_t *l_455[1];
            int64_t l_456 = 0x4AFDCC56AF2951ECLL;
            struct S0 **l_464 = &l_462;
            int i;
            for (i = 0; i < 1; i++)
                l_455[i] = &g_109[2][1][2];
            --l_459;
            g_465[7][0][0] = ((*l_464) = l_462);
            (**l_428) &= 0xB5ED9690L;
        }
        else
        { /* block id: 132 */
            struct S0 ***l_475 = (void*)0;
            struct S0 **l_477 = &l_462;
            struct S0 ***l_476 = &l_477;
            int32_t l_479 = (-7L);
            int32_t *l_487 = &g_466.f6;
            (*l_487) &= (safe_sub_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(0x0CFE1D11B0D20D9ELL, 0xE0D34FA556043100LL)), (safe_add_func_uint8_t_u_u((safe_div_func_int32_t_s_s((&l_462 == ((*l_476) = &g_465[6][0][1])), ((((~(l_479 <= ((**l_450) = (l_424 == (*g_332))))) ^ ((**l_428) &= ((~l_481) , ((l_486 = ((((*l_349) = (((l_484 = ((safe_add_func_uint8_t_u_u((g_419.f2 > (**g_405)), l_479)) , l_445[0][2])) == (void*)0) & l_479)) < (*g_332)) , 0x2D095F09BE7A60EFLL)) == p_90)))) == p_90) , 0xC91C23FBL))), 4UL))));
            (*l_450) = (*g_298);
            (*l_450) = &l_479;
        }
        for (g_463.f5 = 0; (g_463.f5 < (-12)); g_463.f5 = safe_sub_func_uint8_t_u_u(g_463.f5, 1))
        { /* block id: 145 */
            if ((**g_331))
                break;
            for (g_106 = 0; (g_106 <= 0); g_106 += 1)
            { /* block id: 149 */
                uint16_t l_490 = 65535UL;
                (*l_450) = &p_91;
                ++l_490;
            }
        }
    }
    l_424 = (safe_sub_func_uint32_t_u_u(((((safe_add_func_int16_t_s_s(g_466.f7, (safe_lshift_func_uint8_t_u_u(((((*p_92) & l_486) > p_91) == (((l_499 == (g_501 = &g_226)) , (1UL < 4294967295UL)) != ((l_107[2][5] = ((g_77 > p_91) | l_107[0][2])) | g_109[1][1][1]))), 6)))) ^ p_91) & 0x0C2D1741L) , p_91), 0x10622E41L));
    return g_502;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc_bytes (&g_23, sizeof(g_23), "g_23", print_hash_value);
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_43, "g_43", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    transparent_crc(g_77, "g_77", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_106, "g_106", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_109[i][j][k], "g_109[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc_bytes(&g_110[i][j][k], sizeof(g_110[i][j][k]), "g_110[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_124[i][j][k], "g_124[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_127.f0, "g_127.f0", print_hash_value);
    transparent_crc_bytes (&g_127.f1, sizeof(g_127.f1), "g_127.f1", print_hash_value);
    transparent_crc(g_127.f2, "g_127.f2", print_hash_value);
    transparent_crc_bytes (&g_127.f3, sizeof(g_127.f3), "g_127.f3", print_hash_value);
    transparent_crc(g_127.f4, "g_127.f4", print_hash_value);
    transparent_crc(g_127.f5, "g_127.f5", print_hash_value);
    transparent_crc(g_127.f6, "g_127.f6", print_hash_value);
    transparent_crc(g_127.f7, "g_127.f7", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    transparent_crc(g_224.f0, "g_224.f0", print_hash_value);
    transparent_crc_bytes (&g_224.f1, sizeof(g_224.f1), "g_224.f1", print_hash_value);
    transparent_crc(g_224.f2, "g_224.f2", print_hash_value);
    transparent_crc_bytes (&g_224.f3, sizeof(g_224.f3), "g_224.f3", print_hash_value);
    transparent_crc(g_224.f4, "g_224.f4", print_hash_value);
    transparent_crc(g_224.f5, "g_224.f5", print_hash_value);
    transparent_crc(g_224.f6, "g_224.f6", print_hash_value);
    transparent_crc(g_224.f7, "g_224.f7", print_hash_value);
    transparent_crc(g_226, "g_226", print_hash_value);
    transparent_crc(g_233, "g_233", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_243[i][j], "g_243[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_266, "g_266", print_hash_value);
    transparent_crc(g_267, "g_267", print_hash_value);
    transparent_crc(g_321, "g_321", print_hash_value);
    transparent_crc(g_330, "g_330", print_hash_value);
    transparent_crc(g_350, "g_350", print_hash_value);
    transparent_crc(g_366, "g_366", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_397[i], "g_397[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_400[i].f0, "g_400[i].f0", print_hash_value);
        transparent_crc_bytes(&g_400[i].f1, sizeof(g_400[i].f1), "g_400[i].f1", print_hash_value);
        transparent_crc(g_400[i].f2, "g_400[i].f2", print_hash_value);
        transparent_crc_bytes(&g_400[i].f3, sizeof(g_400[i].f3), "g_400[i].f3", print_hash_value);
        transparent_crc(g_400[i].f4, "g_400[i].f4", print_hash_value);
        transparent_crc(g_400[i].f5, "g_400[i].f5", print_hash_value);
        transparent_crc(g_400[i].f6, "g_400[i].f6", print_hash_value);
        transparent_crc(g_400[i].f7, "g_400[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_407, "g_407", print_hash_value);
    transparent_crc(g_419.f0, "g_419.f0", print_hash_value);
    transparent_crc_bytes (&g_419.f1, sizeof(g_419.f1), "g_419.f1", print_hash_value);
    transparent_crc(g_419.f2, "g_419.f2", print_hash_value);
    transparent_crc_bytes (&g_419.f3, sizeof(g_419.f3), "g_419.f3", print_hash_value);
    transparent_crc(g_419.f4, "g_419.f4", print_hash_value);
    transparent_crc(g_419.f5, "g_419.f5", print_hash_value);
    transparent_crc(g_419.f6, "g_419.f6", print_hash_value);
    transparent_crc(g_419.f7, "g_419.f7", print_hash_value);
    transparent_crc(g_427, "g_427", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_453[i], "g_453[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_463.f0, "g_463.f0", print_hash_value);
    transparent_crc_bytes (&g_463.f1, sizeof(g_463.f1), "g_463.f1", print_hash_value);
    transparent_crc(g_463.f2, "g_463.f2", print_hash_value);
    transparent_crc_bytes (&g_463.f3, sizeof(g_463.f3), "g_463.f3", print_hash_value);
    transparent_crc(g_463.f4, "g_463.f4", print_hash_value);
    transparent_crc(g_463.f5, "g_463.f5", print_hash_value);
    transparent_crc(g_463.f6, "g_463.f6", print_hash_value);
    transparent_crc(g_463.f7, "g_463.f7", print_hash_value);
    transparent_crc(g_466.f0, "g_466.f0", print_hash_value);
    transparent_crc_bytes (&g_466.f1, sizeof(g_466.f1), "g_466.f1", print_hash_value);
    transparent_crc(g_466.f2, "g_466.f2", print_hash_value);
    transparent_crc_bytes (&g_466.f3, sizeof(g_466.f3), "g_466.f3", print_hash_value);
    transparent_crc(g_466.f4, "g_466.f4", print_hash_value);
    transparent_crc(g_466.f5, "g_466.f5", print_hash_value);
    transparent_crc(g_466.f6, "g_466.f6", print_hash_value);
    transparent_crc(g_466.f7, "g_466.f7", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_485[i], "g_485[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_556[i], "g_556[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_630, "g_630", print_hash_value);
    transparent_crc(g_649.f0, "g_649.f0", print_hash_value);
    transparent_crc_bytes (&g_649.f1, sizeof(g_649.f1), "g_649.f1", print_hash_value);
    transparent_crc(g_649.f2, "g_649.f2", print_hash_value);
    transparent_crc_bytes (&g_649.f3, sizeof(g_649.f3), "g_649.f3", print_hash_value);
    transparent_crc(g_649.f4, "g_649.f4", print_hash_value);
    transparent_crc(g_649.f5, "g_649.f5", print_hash_value);
    transparent_crc(g_649.f6, "g_649.f6", print_hash_value);
    transparent_crc(g_649.f7, "g_649.f7", print_hash_value);
    transparent_crc(g_740.f0, "g_740.f0", print_hash_value);
    transparent_crc_bytes (&g_740.f1, sizeof(g_740.f1), "g_740.f1", print_hash_value);
    transparent_crc(g_740.f2, "g_740.f2", print_hash_value);
    transparent_crc_bytes (&g_740.f3, sizeof(g_740.f3), "g_740.f3", print_hash_value);
    transparent_crc(g_740.f4, "g_740.f4", print_hash_value);
    transparent_crc(g_740.f5, "g_740.f5", print_hash_value);
    transparent_crc(g_740.f6, "g_740.f6", print_hash_value);
    transparent_crc(g_740.f7, "g_740.f7", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc_bytes(&g_774[i][j], sizeof(g_774[i][j]), "g_774[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_825, "g_825", print_hash_value);
    transparent_crc(g_847, "g_847", print_hash_value);
    transparent_crc(g_887, "g_887", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_914[i], "g_914[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_986, "g_986", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_1006[i][j].f0, "g_1006[i][j].f0", print_hash_value);
            transparent_crc_bytes(&g_1006[i][j].f1, sizeof(g_1006[i][j].f1), "g_1006[i][j].f1", print_hash_value);
            transparent_crc(g_1006[i][j].f2, "g_1006[i][j].f2", print_hash_value);
            transparent_crc_bytes(&g_1006[i][j].f3, sizeof(g_1006[i][j].f3), "g_1006[i][j].f3", print_hash_value);
            transparent_crc(g_1006[i][j].f4, "g_1006[i][j].f4", print_hash_value);
            transparent_crc(g_1006[i][j].f5, "g_1006[i][j].f5", print_hash_value);
            transparent_crc(g_1006[i][j].f6, "g_1006[i][j].f6", print_hash_value);
            transparent_crc(g_1006[i][j].f7, "g_1006[i][j].f7", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1103, "g_1103", print_hash_value);
    transparent_crc(g_1111.f0, "g_1111.f0", print_hash_value);
    transparent_crc_bytes (&g_1111.f1, sizeof(g_1111.f1), "g_1111.f1", print_hash_value);
    transparent_crc(g_1111.f2, "g_1111.f2", print_hash_value);
    transparent_crc_bytes (&g_1111.f3, sizeof(g_1111.f3), "g_1111.f3", print_hash_value);
    transparent_crc(g_1111.f4, "g_1111.f4", print_hash_value);
    transparent_crc(g_1111.f5, "g_1111.f5", print_hash_value);
    transparent_crc(g_1111.f6, "g_1111.f6", print_hash_value);
    transparent_crc(g_1111.f7, "g_1111.f7", print_hash_value);
    transparent_crc(g_1134, "g_1134", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1186[i][j], "g_1186[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1201, "g_1201", print_hash_value);
    transparent_crc(g_1218.f0, "g_1218.f0", print_hash_value);
    transparent_crc_bytes (&g_1218.f1, sizeof(g_1218.f1), "g_1218.f1", print_hash_value);
    transparent_crc(g_1218.f2, "g_1218.f2", print_hash_value);
    transparent_crc_bytes (&g_1218.f3, sizeof(g_1218.f3), "g_1218.f3", print_hash_value);
    transparent_crc(g_1218.f4, "g_1218.f4", print_hash_value);
    transparent_crc(g_1218.f5, "g_1218.f5", print_hash_value);
    transparent_crc(g_1218.f6, "g_1218.f6", print_hash_value);
    transparent_crc(g_1218.f7, "g_1218.f7", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1230[i][j][k], "g_1230[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1425[i], "g_1425[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1451, sizeof(g_1451), "g_1451", print_hash_value);
    transparent_crc(g_1519.f0, "g_1519.f0", print_hash_value);
    transparent_crc_bytes (&g_1519.f1, sizeof(g_1519.f1), "g_1519.f1", print_hash_value);
    transparent_crc(g_1519.f2, "g_1519.f2", print_hash_value);
    transparent_crc_bytes (&g_1519.f3, sizeof(g_1519.f3), "g_1519.f3", print_hash_value);
    transparent_crc(g_1519.f4, "g_1519.f4", print_hash_value);
    transparent_crc(g_1519.f5, "g_1519.f5", print_hash_value);
    transparent_crc(g_1519.f6, "g_1519.f6", print_hash_value);
    transparent_crc(g_1519.f7, "g_1519.f7", print_hash_value);
    transparent_crc(g_1711.f0, "g_1711.f0", print_hash_value);
    transparent_crc_bytes (&g_1711.f1, sizeof(g_1711.f1), "g_1711.f1", print_hash_value);
    transparent_crc(g_1711.f2, "g_1711.f2", print_hash_value);
    transparent_crc_bytes (&g_1711.f3, sizeof(g_1711.f3), "g_1711.f3", print_hash_value);
    transparent_crc(g_1711.f4, "g_1711.f4", print_hash_value);
    transparent_crc(g_1711.f5, "g_1711.f5", print_hash_value);
    transparent_crc(g_1711.f6, "g_1711.f6", print_hash_value);
    transparent_crc(g_1711.f7, "g_1711.f7", print_hash_value);
    transparent_crc(g_1727, "g_1727", print_hash_value);
    transparent_crc(g_1728, "g_1728", print_hash_value);
    transparent_crc(g_1793.f0, "g_1793.f0", print_hash_value);
    transparent_crc_bytes (&g_1793.f1, sizeof(g_1793.f1), "g_1793.f1", print_hash_value);
    transparent_crc(g_1793.f2, "g_1793.f2", print_hash_value);
    transparent_crc_bytes (&g_1793.f3, sizeof(g_1793.f3), "g_1793.f3", print_hash_value);
    transparent_crc(g_1793.f4, "g_1793.f4", print_hash_value);
    transparent_crc(g_1793.f5, "g_1793.f5", print_hash_value);
    transparent_crc(g_1793.f6, "g_1793.f6", print_hash_value);
    transparent_crc(g_1793.f7, "g_1793.f7", print_hash_value);
    transparent_crc(g_1796.f0, "g_1796.f0", print_hash_value);
    transparent_crc_bytes (&g_1796.f1, sizeof(g_1796.f1), "g_1796.f1", print_hash_value);
    transparent_crc(g_1796.f2, "g_1796.f2", print_hash_value);
    transparent_crc_bytes (&g_1796.f3, sizeof(g_1796.f3), "g_1796.f3", print_hash_value);
    transparent_crc(g_1796.f4, "g_1796.f4", print_hash_value);
    transparent_crc(g_1796.f5, "g_1796.f5", print_hash_value);
    transparent_crc(g_1796.f6, "g_1796.f6", print_hash_value);
    transparent_crc(g_1796.f7, "g_1796.f7", print_hash_value);
    transparent_crc(g_1823, "g_1823", print_hash_value);
    transparent_crc(g_1852, "g_1852", print_hash_value);
    transparent_crc(g_1891, "g_1891", print_hash_value);
    transparent_crc(g_2033, "g_2033", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2112[i].f0, "g_2112[i].f0", print_hash_value);
        transparent_crc_bytes(&g_2112[i].f1, sizeof(g_2112[i].f1), "g_2112[i].f1", print_hash_value);
        transparent_crc(g_2112[i].f2, "g_2112[i].f2", print_hash_value);
        transparent_crc_bytes(&g_2112[i].f3, sizeof(g_2112[i].f3), "g_2112[i].f3", print_hash_value);
        transparent_crc(g_2112[i].f4, "g_2112[i].f4", print_hash_value);
        transparent_crc(g_2112[i].f5, "g_2112[i].f5", print_hash_value);
        transparent_crc(g_2112[i].f6, "g_2112[i].f6", print_hash_value);
        transparent_crc(g_2112[i].f7, "g_2112[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2275, "g_2275", print_hash_value);
    transparent_crc(g_2365, "g_2365", print_hash_value);
    transparent_crc(g_2397.f0, "g_2397.f0", print_hash_value);
    transparent_crc_bytes (&g_2397.f1, sizeof(g_2397.f1), "g_2397.f1", print_hash_value);
    transparent_crc(g_2397.f2, "g_2397.f2", print_hash_value);
    transparent_crc_bytes (&g_2397.f3, sizeof(g_2397.f3), "g_2397.f3", print_hash_value);
    transparent_crc(g_2397.f4, "g_2397.f4", print_hash_value);
    transparent_crc(g_2397.f5, "g_2397.f5", print_hash_value);
    transparent_crc(g_2397.f6, "g_2397.f6", print_hash_value);
    transparent_crc(g_2397.f7, "g_2397.f7", print_hash_value);
    transparent_crc(g_2600, "g_2600", print_hash_value);
    transparent_crc(g_2611.f0, "g_2611.f0", print_hash_value);
    transparent_crc_bytes (&g_2611.f1, sizeof(g_2611.f1), "g_2611.f1", print_hash_value);
    transparent_crc(g_2611.f2, "g_2611.f2", print_hash_value);
    transparent_crc_bytes (&g_2611.f3, sizeof(g_2611.f3), "g_2611.f3", print_hash_value);
    transparent_crc(g_2611.f4, "g_2611.f4", print_hash_value);
    transparent_crc(g_2611.f5, "g_2611.f5", print_hash_value);
    transparent_crc(g_2611.f6, "g_2611.f6", print_hash_value);
    transparent_crc(g_2611.f7, "g_2611.f7", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 637
   depth: 1, occurrence: 16
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 43
breakdown:
   indirect level: 0, occurrence: 16
   indirect level: 1, occurrence: 10
   indirect level: 2, occurrence: 6
   indirect level: 3, occurrence: 5
   indirect level: 4, occurrence: 4
   indirect level: 5, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 18
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 17
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 32

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 167
   depth: 2, occurrence: 53
   depth: 3, occurrence: 3
   depth: 5, occurrence: 1
   depth: 6, occurrence: 2
   depth: 11, occurrence: 2
   depth: 14, occurrence: 3
   depth: 15, occurrence: 3
   depth: 18, occurrence: 1
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 22, occurrence: 2
   depth: 23, occurrence: 4
   depth: 24, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 3
   depth: 33, occurrence: 1
   depth: 36, occurrence: 2
   depth: 38, occurrence: 1
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 541

XXX times a variable address is taken: 1162
XXX times a pointer is dereferenced on RHS: 270
breakdown:
   depth: 1, occurrence: 207
   depth: 2, occurrence: 50
   depth: 3, occurrence: 9
   depth: 4, occurrence: 4
XXX times a pointer is dereferenced on LHS: 335
breakdown:
   depth: 1, occurrence: 318
   depth: 2, occurrence: 14
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 44
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 8594

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1824
   level: 2, occurrence: 307
   level: 3, occurrence: 34
   level: 4, occurrence: 10
   level: 5, occurrence: 10
XXX number of pointers point to pointers: 256
XXX number of pointers point to scalars: 275
XXX number of pointers point to structs: 10
XXX percent of pointers has null in alias set: 30.9
XXX average alias set size: 1.57

XXX times a non-volatile is read: 2029
XXX times a non-volatile is write: 986
XXX times a volatile is read: 145
XXX    times read thru a pointer: 69
XXX times a volatile is write: 37
XXX    times written thru a pointer: 3
XXX times a volatile is available for access: 3.27e+03
XXX percentage of non-volatile access: 94.3

XXX forward jumps: 2
XXX backward jumps: 11

XXX stmts: 187
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 32
   depth: 2, occurrence: 33
   depth: 3, occurrence: 26
   depth: 4, occurrence: 29
   depth: 5, occurrence: 34

XXX percentage a fresh-made variable is used: 15.9
XXX percentage an existing variable is used: 84.1
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3595874404
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 2L;
static float g_5 = 0x1.CB4E76p-89;
static int32_t g_42 = 0xE89405DBL;
static int32_t * volatile g_41[2] = {&g_42,&g_42};
static int32_t * volatile g_43 = &g_42;/* VOLATILE GLOBAL g_43 */
static int32_t *g_66 = (void*)0;
static int32_t **g_65[1] = {&g_66};
static uint64_t g_74 = 0x50D1B5CEFCB9D1C2LL;
static int32_t g_86 = (-6L);
static int32_t g_96 = 0x9AC8F413L;
static float * volatile g_104 = (void*)0;/* VOLATILE GLOBAL g_104 */
static float * volatile g_105 = &g_5;/* VOLATILE GLOBAL g_105 */
static uint8_t g_111 = 0xA3L;
static int64_t g_129 = 9L;
static int32_t g_131[7][8][1] = {{{(-7L)},{0x08CCFF5DL},{(-10L)},{0x4D6C3E8AL},{1L},{1L},{0x4D6C3E8AL},{(-10L)}},{{0x08CCFF5DL},{(-7L)},{1L},{0x08CCFF5DL},{(-1L)},{0x08CCFF5DL},{1L},{(-7L)}},{{0x08CCFF5DL},{(-10L)},{0x4D6C3E8AL},{1L},{1L},{0x4D6C3E8AL},{(-10L)},{0x08CCFF5DL}},{{(-7L)},{1L},{0x08CCFF5DL},{(-1L)},{0x08CCFF5DL},{1L},{(-7L)},{0x08CCFF5DL}},{{(-10L)},{0x4D6C3E8AL},{1L},{1L},{0x4D6C3E8AL},{(-10L)},{0x08CCFF5DL},{(-7L)}},{{1L},{0x08CCFF5DL},{(-1L)},{(-1L)},{0xD7A0CA37L},{0x49016727L},{(-1L)},{(-7L)}},{{(-1L)},{0xD7A0CA37L},{0xD7A0CA37L},{(-1L)},{(-7L)},{(-1L)},{0x49016727L},{0xD7A0CA37L}}};
static int32_t * const  volatile g_130[6] = {&g_131[4][5][0],&g_131[4][5][0],&g_131[4][5][0],&g_131[4][5][0],&g_131[4][5][0],&g_131[4][5][0]};
static float *g_150[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static float **g_149 = &g_150[2];
static int64_t g_157 = 8L;
static uint16_t g_160 = 65535UL;
static uint64_t g_192 = 0x26958433A3D88CF5LL;
static int64_t g_244[1][8][8] = {{{0x4D647C19EDD3A627LL,0xD368092EE9E647D6LL,0xD368092EE9E647D6LL,0x4D647C19EDD3A627LL,0L,0x431DA364DE6C08DALL,0x09343FE88F4ACA79LL,1L},{0x5695930398711890LL,0x4EB943A5B38CE81FLL,0x431DA364DE6C08DALL,(-1L),0xD368092EE9E647D6LL,0xDF1352EBC1667867LL,1L,0xDF1352EBC1667867LL},{0L,0x4EB943A5B38CE81FLL,0x09343FE88F4ACA79LL,0x4EB943A5B38CE81FLL,0L,0x431DA364DE6C08DALL,0x4D647C19EDD3A627LL,0xF652C32F726B6DD6LL},{0x431DA364DE6C08DALL,0xD368092EE9E647D6LL,0L,(-3L),(-6L),0L,0x4EB943A5B38CE81FLL,0x4EB943A5B38CE81FLL},{1L,(-10L),0L,0L,(-10L),1L,0x4D647C19EDD3A627LL,(-6L)},{(-6L),0xDF1352EBC1667867LL,0x09343FE88F4ACA79LL,0xF652C32F726B6DD6LL,0x4EB943A5B38CE81FLL,(-3L),1L,0x431DA364DE6C08DALL},{0x09343FE88F4ACA79LL,0x5695930398711890LL,0x431DA364DE6C08DALL,0xF652C32F726B6DD6LL,0x431DA364DE6C08DALL,0x5695930398711890LL,0x09343FE88F4ACA79LL,(-6L)},{(-10L),0x431DA364DE6C08DALL,0xD368092EE9E647D6LL,0L,(-3L),(-6L),0L,0x4EB943A5B38CE81FLL}}};
static float g_268 = 0x7.31B12Dp+40;
static const int32_t *g_353 = &g_131[5][2][0];
static const int32_t **g_352 = &g_353;
static float g_356 = 0x3.4p-1;
static const volatile int64_t g_370 = (-4L);/* VOLATILE GLOBAL g_370 */
static const volatile int64_t *g_369 = &g_370;
static const volatile int64_t ** volatile g_368 = &g_369;/* VOLATILE GLOBAL g_368 */
static int64_t *g_379 = &g_157;
static uint32_t g_400 = 0xF5FBF287L;
static int16_t g_404 = 4L;
static float * volatile g_407 = &g_268;/* VOLATILE GLOBAL g_407 */
static int32_t *g_438 = &g_86;
static uint16_t g_472 = 65535UL;
static uint8_t g_514 = 0x1CL;
static int64_t **g_521 = &g_379;
static int64_t ***g_520 = &g_521;
static int64_t ****g_519 = &g_520;
static int64_t *****g_518 = &g_519;
static int32_t *g_578 = &g_96;
static uint32_t g_591 = 0x4F1D7D89L;
static uint8_t *g_639[8][1][2] = {{{(void*)0,&g_111}},{{&g_111,&g_111}},{{(void*)0,&g_111}},{{&g_111,&g_111}},{{(void*)0,&g_111}},{{&g_111,&g_111}},{{(void*)0,&g_111}},{{&g_111,&g_111}}};
static int8_t g_664[7][4][2] = {{{0x05L,0L},{0x05L,1L},{0x9DL,1L},{0x39L,1L}},{{0x9DL,1L},{0x05L,0L},{0x05L,1L},{0x9DL,1L}},{{0x39L,1L},{0x9DL,1L},{0x05L,0L},{0x05L,1L}},{{0x9DL,1L},{0x39L,1L},{0x9DL,1L},{0x05L,0L}},{{0x05L,1L},{0x9DL,1L},{0x39L,1L},{0x9DL,1L}},{{0x05L,0L},{0x05L,1L},{0x9DL,1L},{0x39L,1L}},{{0x9DL,1L},{0x05L,0L},{0x05L,1L},{0x9DL,1L}}};
static int32_t g_704 = 0x65341F31L;
static int64_t * const ***g_770[2][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
static uint8_t g_852 = 7UL;
static float * volatile g_857 = &g_356;/* VOLATILE GLOBAL g_857 */
static int32_t ***g_903 = &g_65[0];
static float * const  volatile g_992 = &g_268;/* VOLATILE GLOBAL g_992 */
static uint16_t g_993 = 0xEFEBL;
static uint64_t g_1002 = 18446744073709551615UL;
static uint64_t **** volatile g_1034 = (void*)0;/* VOLATILE GLOBAL g_1034 */
static int32_t ** volatile g_1055 = (void*)0;/* VOLATILE GLOBAL g_1055 */
static int32_t ** volatile * volatile g_1056 = &g_1055;/* VOLATILE GLOBAL g_1056 */
static int32_t * const *g_1180 = &g_66;
static int32_t * const **g_1179 = &g_1180;
static int32_t * const ***g_1178 = &g_1179;
static uint32_t g_1304 = 0x2D490216L;
static volatile uint8_t * volatile ** volatile g_1399 = (void*)0;/* VOLATILE GLOBAL g_1399 */
static uint64_t **** volatile * volatile g_1413 = (void*)0;/* VOLATILE GLOBAL g_1413 */
static uint64_t **** volatile * volatile g_1414 = &g_1034;/* VOLATILE GLOBAL g_1414 */
static int16_t *g_1453 = &g_404;
static int8_t g_1512 = 0x5EL;
static int64_t ****g_1521 = &g_520;
static int32_t *g_1556 = &g_704;
static int32_t **g_1555 = &g_1556;
static int32_t ***g_1554 = &g_1555;
static uint8_t g_1592 = 0UL;
static uint8_t g_1652 = 0x87L;
static uint16_t g_1702 = 0xE0B8L;
static int32_t g_1777 = 6L;
static uint32_t *g_1817 = (void*)0;
static uint32_t **g_1816 = &g_1817;
static uint32_t **g_1819 = &g_1817;
static uint32_t ***g_1916 = &g_1816;
static uint32_t **** volatile g_1915 = &g_1916;/* VOLATILE GLOBAL g_1915 */
static uint64_t *g_2003[5] = {&g_74,&g_74,&g_74,&g_74,&g_74};
static uint64_t *g_2004 = (void*)0;
static uint64_t ** const g_2002[2][6] = {{&g_2004,(void*)0,&g_2004,&g_2004,(void*)0,&g_2004},{&g_2004,(void*)0,&g_2004,&g_2004,(void*)0,&g_2004}};
static uint64_t ** const *g_2001[2][10] = {{&g_2002[0][3],&g_2002[1][1],&g_2002[0][3],(void*)0,(void*)0,&g_2002[0][3],&g_2002[1][1],&g_2002[0][3],(void*)0,&g_2002[0][3]},{&g_2002[1][1],&g_2002[1][1],&g_2002[1][1],&g_2002[0][3],&g_2002[0][3],&g_2002[1][1],&g_2002[1][1],&g_2002[1][1],&g_2002[0][3],&g_2002[0][3]}};
static uint64_t ** const **g_2000 = &g_2001[1][6];
static uint64_t ** const ***g_1999 = &g_2000;
static int32_t *g_2012 = &g_2;
static int8_t *g_2094 = (void*)0;
static int8_t **g_2093 = &g_2094;
static volatile uint64_t g_2109 = 0UL;/* VOLATILE GLOBAL g_2109 */
static float g_2133 = (-0x5.5p-1);
static int32_t *g_2140 = &g_2;
static int32_t *g_2187 = &g_96;
static int32_t g_2203 = 1L;
static const int32_t ** volatile g_2305[5][7] = {{&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353},{&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353},{&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353},{&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353},{&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353}};
static uint32_t g_2316[3] = {0x4B01E4F8L,0x4B01E4F8L,0x4B01E4F8L};
static float g_2354 = 0xC.278864p-92;
static int32_t *g_2357 = &g_2;
static int8_t *** volatile g_2359 = (void*)0;/* VOLATILE GLOBAL g_2359 */
static int8_t *** volatile g_2360 = &g_2093;/* VOLATILE GLOBAL g_2360 */
static uint16_t g_2384 = 0UL;
static int8_t ***g_2495 = &g_2093;
static int16_t g_2499 = 2L;
static float * volatile g_2542 = &g_356;/* VOLATILE GLOBAL g_2542 */
static int64_t g_2568 = 0x3E09AF12FD1227BALL;
static uint32_t g_2629 = 6UL;
static float * volatile g_2739 = (void*)0;/* VOLATILE GLOBAL g_2739 */
static volatile int32_t g_2810 = 0x3E55DE06L;/* VOLATILE GLOBAL g_2810 */
static int32_t ** volatile g_2861[8][2][3] = {{{&g_578,&g_578,&g_2357},{&g_2357,&g_66,&g_578}},{{&g_578,&g_66,&g_2357},{&g_2140,&g_66,&g_66}},{{&g_578,&g_578,&g_2357},{&g_2357,&g_66,&g_578}},{{&g_578,&g_66,&g_2357},{&g_2140,&g_66,&g_66}},{{&g_578,&g_578,&g_2357},{&g_2357,&g_66,&g_578}},{{&g_578,&g_66,&g_2357},{&g_2140,&g_66,&g_66}},{{&g_578,&g_578,&g_2357},{&g_2357,&g_66,&g_578}},{{&g_578,&g_66,&g_2357},{&g_2140,&g_66,&g_66}}};
static int32_t ** volatile g_2862 = &g_578;/* VOLATILE GLOBAL g_2862 */
static volatile uint32_t g_2875 = 1UL;/* VOLATILE GLOBAL g_2875 */
static volatile uint32_t * const g_2874 = &g_2875;
static volatile uint32_t * const *g_2873 = &g_2874;
static uint16_t g_2928[6] = {0x63C6L,0x63C6L,65530UL,0x63C6L,0x63C6L,65530UL};
static int32_t ** volatile g_2967 = (void*)0;/* VOLATILE GLOBAL g_2967 */
static int32_t ** volatile g_2968 = (void*)0;/* VOLATILE GLOBAL g_2968 */
static int32_t ** volatile g_2969 = &g_2187;/* VOLATILE GLOBAL g_2969 */


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static int32_t * func_9(const int32_t * const  p_10, int16_t  p_11, int32_t * const  p_12);
static int64_t  func_15(int32_t * p_16, const uint16_t  p_17, int32_t * p_18);
static int32_t * func_19(int32_t  p_20, int32_t * const  p_21);
static int32_t * func_22(int32_t  p_23, int32_t * p_24, int32_t * p_25, int32_t * const  p_26);
static int32_t * func_27(int32_t * p_28, int32_t * const  p_29, uint8_t  p_30, uint32_t  p_31);
static int32_t * func_32(int32_t * p_33);
static int32_t * func_34(int32_t * p_35, int32_t * p_36, const int16_t  p_37);
static int32_t  func_44(int16_t  p_45, const int16_t  p_46, int32_t * p_47);
static int64_t  func_50(int32_t * p_51, int32_t * p_52, int32_t * const  p_53);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_43 g_42 g_74 g_96 g_86 g_105 g_111 g_131 g_149 g_157 g_5 g_160 g_192 g_129 g_244 g_150 g_352 g_353 g_368 g_370 g_404 g_407 g_514 g_857 g_356 g_1304 g_704 g_578 g_1179 g_1180 g_66 g_993 g_1034 g_1414 g_518 g_519 g_520 g_521 g_379 g_852 g_1453 g_472 g_1702 g_1002 g_664 g_1652 g_1554 g_1399 g_1521 g_992 g_268 g_591 g_2384 g_2542 g_438 g_2568 g_2316 g_2187 g_369 g_903 g_65 g_1915 g_1916 g_2629 g_2357 g_1816 g_1817 g_2140 g_1555 g_1556 g_1592 g_2109 g_2203 g_400 g_2499 g_2874 g_2875
 * writes: g_42 g_65 g_74 g_86 g_96 g_5 g_111 g_129 g_131 g_149 g_157 g_160 g_244 g_268 g_192 g_150 g_352 g_379 g_400 g_404 g_664 g_514 g_704 g_438 g_993 g_1034 g_1453 g_852 g_472 g_1512 g_1702 g_353 g_1555 g_1777 g_1002 g_2495 g_2 g_2499 g_2354 g_356 g_66 g_521 g_1592 g_2316
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_6 = 4294967295UL;
    int32_t *l_38 = &g_2;
    int8_t ***l_2494 = &g_2093;
    int32_t l_2497 = 0xABCAF367L;
    uint16_t l_2500 = 0x1100L;
    int32_t l_2501 = 0x915BA3D9L;
    int32_t l_2502 = 0x2A12B616L;
    int32_t l_2504 = 3L;
    uint32_t l_2505 = 0x7A0BBB95L;
    int32_t l_2514[2][8][8] = {{{(-1L),1L,0xC637F871L,0x3A5E5EBDL,0x3A5E5EBDL,0xC637F871L,1L,(-1L)},{0L,(-6L),0xC637F871L,0xF255CAACL,1L,0x1C1FEE51L,1L,0xF255CAACL},{0x3A5E5EBDL,5L,0x3A5E5EBDL,(-1L),0xF255CAACL,0x1C1FEE51L,0xC637F871L,0xC637F871L},{0xC637F871L,(-6L),0L,0L,(-6L),0xC637F871L,0xF255CAACL,1L},{0xC637F871L,1L,(-1L),(-6L),0xF255CAACL,(-6L),(-1L),1L},{0x3A5E5EBDL,(-1L),0x1C1FEE51L,(-6L),1L,(-1L),(-1L),1L},{0L,1L,1L,0L,0x3A5E5EBDL,1L,(-1L),0xC637F871L},{(-1L),0L,0x1C1FEE51L,(-1L),0x1C1FEE51L,0L,(-1L),0xF255CAACL}},{{0x1C1FEE51L,0L,(-1L),0xF255CAACL,1L,1L,0xF255CAACL,(-1L)},{1L,1L,0L,0x3A5E5EBDL,1L,(-1L),0xC637F871L,(-1L)},{0x1C1FEE51L,(-1L),0x3A5E5EBDL,(-1L),0x1C1FEE51L,(-6L),1L,(-1L)},{5L,0x1C1FEE51L,0x3A5E5EBDL,(-6L),(-6L),0x3A5E5EBDL,0x1C1FEE51L,5L},{1L,0L,0x3A5E5EBDL,1L,(-1L),0xC637F871L,(-1L),1L},{(-6L),0xF255CAACL,(-6L),(-1L),1L,0xC637F871L,0x3A5E5EBDL,0x3A5E5EBDL},{0x3A5E5EBDL,0L,1L,1L,0L,0x3A5E5EBDL,1L,(-1L)},{0x3A5E5EBDL,0x1C1FEE51L,5L,0L,1L,0L,5L,0x1C1FEE51L}}};
    int64_t l_2627 = 0xFD59E2BAA1BECF4CLL;
    uint32_t *l_2692 = &g_2316[2];
    uint16_t l_2712 = 0xBE28L;
    int8_t l_2718 = 2L;
    int16_t l_2786 = 8L;
    uint64_t ***l_2787 = (void*)0;
    uint32_t ****l_2821[9][10] = {{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916},{&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916,&g_1916}};
    int64_t * const *l_2851 = &g_379;
    const uint32_t *l_2855 = &g_2629;
    float l_2900[8][3] = {{0x1.0p+1,0x0.8p-1,0x1.0p+1},{0x9.Ap+1,0x7.1BFFC2p-33,0x0.0p+1},{0x9.Ap+1,0x9.Ap+1,0x7.1BFFC2p-33},{0x1.0p+1,0x7.1BFFC2p-33,0x7.1BFFC2p-33},{0x7.1BFFC2p-33,0x0.8p-1,0x0.0p+1},{0x1.0p+1,0x0.8p-1,0x1.0p+1},{0x9.Ap+1,0x7.1BFFC2p-33,0x0.0p+1},{0x9.Ap+1,0x9.Ap+1,0x7.1BFFC2p-33}};
    int16_t l_2902 = 0x1E28L;
    float l_2906 = 0xE.9CCC78p-66;
    uint32_t l_2907 = 0x9DE9A4AAL;
    int8_t l_2962 = 0xFAL;
    int64_t l_2964[8];
    int32_t *l_2965 = &g_2203;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_2964[i] = (-8L);
    if (g_2)
    { /* block id: 1 */
        int32_t *l_3 = &g_2;
        int32_t *l_4[6][9] = {{&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2},{&g_2,&g_2,(void*)0,(void*)0,(void*)0,&g_2,&g_2,(void*)0,&g_2},{&g_2,&g_2,(void*)0,&g_2,&g_2,&g_2,&g_2,(void*)0,(void*)0},{&g_2,(void*)0,&g_2,(void*)0,&g_2,(void*)0,&g_2,(void*)0,(void*)0},{&g_2,&g_2,(void*)0,&g_2,&g_2,&g_2,&g_2,(void*)0,&g_2},{&g_2,&g_2,&g_2,(void*)0,&g_2,&g_2,&g_2,&g_2,&g_2}};
        int i, j;
        --l_6;
        l_3 = func_9(&g_2, ((*l_3) <= g_2), ((((((void*)0 != &g_2) || (safe_lshift_func_int16_t_s_s(l_6, (func_15(func_19(g_2, func_22((*l_3), func_27(func_32(func_34(&g_2, l_38, g_2)), l_4[3][0], (*l_3), (*l_38)), &g_2, l_38)), g_852, l_38) > g_1304)))) >= 0x0F6C695E5CE45111LL) >= 0L) , l_4[1][0]));
    }
    else
    { /* block id: 1143 */
        uint64_t *l_2478 = (void*)0;
        uint64_t *l_2479 = &g_1002;
        int32_t l_2484[5];
        int8_t ***l_2493[4] = {&g_2093,&g_2093,&g_2093,&g_2093};
        int8_t ****l_2492 = &l_2493[1];
        int8_t *l_2496 = &g_1512;
        int16_t *l_2498 = &g_2499;
        uint32_t l_2515 = 0UL;
        int8_t l_2543 = 1L;
        int64_t **l_2601 = &g_379;
        uint32_t l_2634 = 0xAB1FC7EAL;
        uint64_t l_2738 = 0x792A47A4A5436955LL;
        int8_t l_2857 = 7L;
        int8_t l_2858 = 1L;
        uint8_t l_2886[6] = {0xA6L,0xA6L,0x33L,0xA6L,0xA6L,0x33L};
        int64_t l_2912 = 0L;
        uint32_t l_2929 = 0x80D21131L;
        int64_t l_2960 = 0xB3A9638E3DD76492LL;
        uint32_t *l_2980[6][3][7] = {{{&g_591,&g_591,&l_2505,&l_2929,&l_2505,&l_2929,(void*)0},{&g_591,&l_2929,(void*)0,&l_2929,&l_2929,&g_400,(void*)0},{(void*)0,&g_591,&l_2929,&l_2505,&l_2505,&l_2505,&l_2929}},{{&g_591,&g_591,&l_2929,&l_2929,&l_2505,&l_2929,(void*)0},{&g_591,(void*)0,&l_2929,&l_2929,&l_2929,&l_2505,(void*)0},{&l_2929,&g_591,(void*)0,&g_400,&l_2505,&g_400,(void*)0}},{{&g_591,&g_591,&l_2505,&l_2929,&l_2505,&l_2929,(void*)0},{&g_591,&l_2929,(void*)0,&l_2929,&l_2929,&g_400,(void*)0},{(void*)0,&g_591,&l_2929,&l_2505,&l_2505,&l_2505,&l_2929}},{{&g_591,&g_591,&l_2929,&l_2929,&l_2505,&l_2929,(void*)0},{&g_591,(void*)0,&l_2929,&l_2929,&l_2929,&l_2505,(void*)0},{&l_2929,&g_591,(void*)0,&g_400,&l_2505,&g_400,(void*)0}},{{&g_591,&g_591,&l_2505,&l_2929,&l_2505,&l_2929,(void*)0},{&g_591,&l_2929,(void*)0,&l_2929,&l_2929,&g_400,(void*)0},{(void*)0,&g_591,&l_2929,&l_2505,&l_2505,&l_2505,&l_2929}},{{&g_591,&g_591,&l_2929,&l_2929,&l_2505,&l_2929,(void*)0},{&g_591,(void*)0,&l_2929,&l_2929,&l_2929,&l_2505,(void*)0},{&l_2929,&g_591,(void*)0,&g_400,&l_2505,&g_400,(void*)0}}};
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_2484[i] = 0x3ED8251AL;
        if ((((((safe_rshift_func_int8_t_s_u((((*g_379) = (((*l_2479) |= (+5UL)) ^ (((*l_2498) = ((g_2384 ^ (l_2484[2] && ((0xD7L && (((+(-2L)) != (((safe_lshift_func_uint8_t_u_s((((*l_38) = ((safe_lshift_func_int8_t_s_s(((*l_2496) = ((safe_mul_func_int8_t_s_s(0x6CL, (l_2484[2] | ((*g_1453) &= (((((*l_2492) = &g_2093) == (g_2495 = l_2494)) || l_2484[0]) || 0x7622CB64L))))) ^ (*l_38))), l_2484[0])) | (*l_38))) & (****g_519)), 7)) & l_2497) < l_2484[4])) < g_591)) && 0UL))) <= 0L)) & g_131[5][2][0]))) < l_2484[3]), l_2484[2])) , (*l_38)) ^ 0UL) > 0x89B21AE5L) < l_2500))
        { /* block id: 1152 */
            int32_t *l_2503[7];
            uint32_t l_2552 = 18446744073709551611UL;
            int8_t l_2572 = (-1L);
            int16_t l_2578 = 1L;
            uint32_t l_2645 = 0x75AF6A43L;
            int i;
            for (i = 0; i < 7; i++)
                l_2503[i] = &g_86;
            ++l_2505;
            if (((*g_1453) && (*g_1453)))
            { /* block id: 1154 */
                float l_2508 = 0x3.659447p+13;
                int32_t l_2509 = 0x90ABEF30L;
                int8_t l_2510[6];
                int32_t l_2511 = 0L;
                int32_t l_2512[1];
                int16_t l_2513 = 1L;
                uint64_t *l_2518[9];
                uint8_t *l_2538 = &g_852;
                uint8_t *l_2539 = &g_111;
                float *l_2540 = (void*)0;
                float *l_2541 = &g_2354;
                uint32_t l_2569 = 18446744073709551608UL;
                int i;
                for (i = 0; i < 6; i++)
                    l_2510[i] = 0L;
                for (i = 0; i < 1; i++)
                    l_2512[i] = 6L;
                for (i = 0; i < 9; i++)
                    l_2518[i] = &g_74;
                l_2515--;
                (*g_2542) = (((l_2518[3] == l_2518[3]) <= ((safe_add_func_float_f_f(((((l_2484[2] &= (*l_38)) , ((*l_2541) = ((((safe_sub_func_float_f_f((safe_div_func_float_f_f((((safe_sub_func_float_f_f(0xD.96967Ap-38, 0x2.DFB39Ap+34)) > (!(*g_105))) > ((safe_add_func_float_f_f(((safe_lshift_func_uint8_t_u_u(((*l_2539) = (l_2484[2] | ((*l_2479) = (safe_lshift_func_uint16_t_u_s(((safe_rshift_func_uint8_t_u_u(((*l_2538) ^= (safe_div_func_uint32_t_u_u(((*l_38) < ((*l_38) == 0xC0L)), g_96))), l_2509)) , l_2513), 4))))), 1)) , 0x0.Ap+1), 0x0.225978p+36)) < l_2484[3])), l_2511)), (*l_38))) >= l_2510[2]) >= 0x8.C80635p-52) < 0xE.1D7716p-46))) >= 0xF.3A8960p-68) == l_2509), l_2512[0])) == l_2510[3])) >= (*l_38));
                if (((*l_38) |= (*g_438)))
                { /* block id: 1163 */
                    (*l_2541) = l_2543;
                }
                else
                { /* block id: 1165 */
                    int64_t l_2555 = 0x853A63A8E26C5D3ELL;
                    for (l_2500 = 0; (l_2500 <= 44); l_2500 = safe_add_func_int16_t_s_s(l_2500, 3))
                    { /* block id: 1168 */
                        uint8_t l_2556 = 0x01L;
                        int32_t **l_2561 = &g_2187;
                        (*g_438) ^= (safe_sub_func_int64_t_s_s(0xA7BE17F7857E4662LL, ((safe_rshift_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u(((-1L) || ((l_2552 = 0xD.BB5357p-4) , (((safe_lshift_func_int8_t_s_u(((((l_2556 = l_2555) , (((0xC563D8ABC5DBC728LL & ((void*)0 == l_2561)) | ((-8L) == ((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((((*g_578) |= ((safe_mod_func_int8_t_s_s(((l_2541 != (void*)0) || 4294967286UL), l_2543)) == 0xDE81FBE6L)) , (*g_1453)), l_2510[2])), 0x6DL)) | l_2484[2]))) , l_2515)) <= 0x7BE3643829F9D970LL) || l_2512[0]), l_2515)) && 18446744073709551615UL) , 6UL))), g_2568)) == l_2569), g_2316[2])) != 0x1272E4FDL)));
                        if (l_2555)
                            continue;
                        (*g_2187) = 0x2510DF49L;
                        if (l_2555)
                            break;
                    }
                    (*g_352) = (void*)0;
                }
                for (g_192 = 17; (g_192 > 17); g_192++)
                { /* block id: 1181 */
                    int16_t l_2584 = 1L;
                    int32_t l_2587 = 0x1D17EFD4L;
                    int32_t **l_2590 = &l_2503[4];
                    (*g_438) &= (l_2572 , (+(safe_div_func_int64_t_s_s(((safe_lshift_func_int16_t_s_u((l_2578 >= ((((****g_518) = (*g_521)) != l_2518[3]) , (safe_unary_minus_func_int64_t_s((*g_369))))), (((((*l_2539) = ((l_2515 != (((safe_add_func_uint32_t_u_u((safe_add_func_uint8_t_u_u(((l_2584 = ((*g_149) == (*g_149))) <= ((((safe_sub_func_uint16_t_u_u((*l_38), (*g_1453))) >= 18446744073709551615UL) | (*l_38)) != 1L)), 254UL)), 4294967295UL)) >= (*l_38)) ^ (*g_1453))) && (*l_38))) >= 8UL) != l_2484[0]) != 1L))) || (*l_38)), l_2587))));
                    for (l_2505 = 0; (l_2505 != 24); l_2505 = safe_add_func_uint16_t_u_u(l_2505, 3))
                    { /* block id: 1188 */
                        return l_2512[0];
                    }
                    if (l_2515)
                        break;
                    (*l_2590) = (*g_1180);
                }
            }
            else
            { /* block id: 1194 */
                int8_t l_2599 = 0x1DL;
                uint8_t l_2603 = 0x8DL;
                int32_t l_2606 = 0L;
                int8_t * const *l_2636[6] = {&g_2094,&g_2094,&g_2094,&g_2094,&g_2094,&g_2094};
                int8_t * const **l_2635[7][10] = {{(void*)0,&l_2636[2],&l_2636[3],(void*)0,&l_2636[2],&l_2636[3],&l_2636[3],&l_2636[3],&l_2636[3],&l_2636[3]},{&l_2636[3],(void*)0,&l_2636[5],&l_2636[5],(void*)0,&l_2636[3],(void*)0,&l_2636[3],&l_2636[4],&l_2636[3]},{&l_2636[3],&l_2636[1],&l_2636[3],&l_2636[2],&l_2636[5],(void*)0,&l_2636[4],&l_2636[2],&l_2636[3],(void*)0},{&l_2636[3],&l_2636[4],&l_2636[2],&l_2636[3],(void*)0,&l_2636[3],&l_2636[3],&l_2636[2],&l_2636[3],&l_2636[3]},{&l_2636[3],&l_2636[3],&l_2636[2],&l_2636[3],&l_2636[3],(void*)0,&l_2636[3],&l_2636[2],&l_2636[4],(void*)0},{&l_2636[3],(void*)0,&l_2636[3],&l_2636[3],&l_2636[3],&l_2636[5],&l_2636[5],&l_2636[1],&l_2636[3],(void*)0},{&l_2636[3],&l_2636[3],(void*)0,&l_2636[3],&l_2636[3],&l_2636[2],&l_2636[5],&l_2636[5],&l_2636[2],&l_2636[3]}};
                int i, j;
                (**g_903) = (l_2484[2] , (**g_903));
                for (g_852 = 0; (g_852 <= 6); ++g_852)
                { /* block id: 1198 */
                    int64_t **l_2600[6];
                    uint16_t *l_2602 = &g_160;
                    uint8_t *l_2604 = &g_1592;
                    int32_t l_2605 = 0xFE03D9AAL;
                    uint32_t l_2607 = 0x9E0F5722L;
                    int32_t l_2644[7][4][6] = {{{8L,1L,(-6L),0x365B7E7DL,(-1L),0x0EBA664FL},{1L,(-9L),(-1L),0x293E3C79L,0x149C81B7L,0xB78E0922L},{(-3L),1L,0x38741D81L,(-4L),1L,(-1L)},{(-1L),0x149C81B7L,0x85723A6FL,(-1L),(-9L),(-1L)}},{{0x94BE50FAL,(-1L),0x82B0D999L,(-6L),1L,0xCE24F441L},{0x94BE50FAL,1L,(-6L),(-1L),0x82B0D999L,0xB3AA35BEL},{(-1L),(-9L),0x0CB9197CL,(-4L),0x0CB9197CL,(-9L)},{(-3L),0x32D835ACL,0xC1B25BB0L,0x293E3C79L,0x49BF5186L,(-1L)}},{{1L,(-1L),0xA4F7CE5AL,0x365B7E7DL,(-9L),8L},{8L,(-1L),0x149C81B7L,(-6L),0x49BF5186L,(-2L)},{1L,0x32D835ACL,(-6L),(-5L),0x0CB9197CL,(-6L)},{0xC65E792FL,(-9L),(-1L),(-8L),0x82B0D999L,3L}},{{(-3L),1L,0x29E804E0L,(-4L),1L,(-1L)},{(-9L),(-1L),0x29E804E0L,0x9B50C51CL,(-9L),3L},{0x4C710942L,0x149C81B7L,(-1L),(-6L),1L,(-6L)},{(-6L),1L,(-6L),(-3L),0x149C81B7L,(-2L)}},{{1L,(-9L),0x149C81B7L,0x607C530FL,(-1L),8L},{(-3L),1L,0xA4F7CE5AL,0x607C530FL,1L,(-1L)},{1L,0x82B0D999L,0xC1B25BB0L,(-3L),(-9L),(-9L)},{(-6L),0x0CB9197CL,0x0CB9197CL,(-6L),0x32D835ACL,0xB3AA35BEL}},{{0x4C710942L,0x49BF5186L,(-6L),0x9B50C51CL,(-1L),0xCE24F441L},{(-9L),(-9L),0x82B0D999L,(-4L),(-1L),(-1L)},{(-3L),0x49BF5186L,0x85723A6FL,(-8L),0x32D835ACL,(-1L)},{0xC65E792FL,0x0CB9197CL,0x38741D81L,(-5L),(-9L),0xB78E0922L}},{{1L,0x82B0D999L,(-1L),(-6L),1L,0x0EBA664FL},{8L,1L,0xE46FCE11L,(-2L),(-1L),(-9L)},{0x82B0D999L,1L,1L,1L,0x3CFBC01EL,8L},{0xCE24F441L,0xCF13DA9FL,0x3A23D7ACL,1L,0xCF13DA9FL,(-1L)}}};
                    int i, j, k;
                    for (i = 0; i < 6; i++)
                        l_2600[i] = &g_379;
                    if ((*g_438))
                        break;
                    if (((1UL | ((*g_1915) == &g_1819)) ^ (((((safe_div_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((0xF3L || ((*l_2604) = ((safe_rshift_func_uint16_t_u_u(0xF8B3L, ((l_2599 , (l_2600[1] != ((**g_1521) = l_2601))) , ((*l_2602) = g_74)))) < l_2603))) ^ (*g_1453)), l_2543)), (*l_38))) & 0x7934L) || (*g_1453)) , l_2603) , l_2605)))
                    { /* block id: 1203 */
                        const int8_t l_2628 = 0xC0L;
                        (*l_38) &= (*g_2187);
                        ++l_2607;
                        (*g_2187) &= (~((safe_div_func_uint32_t_u_u(((g_2316[1] < (safe_lshift_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u(((((18446744073709551608UL < ((*l_38) | (safe_sub_func_int8_t_s_s(((((safe_mul_func_uint16_t_u_u(g_404, g_244[0][1][7])) , ((*g_438) = ((safe_mul_func_int16_t_s_s(((((safe_div_func_int8_t_s_s((l_2484[2] ^ ((safe_sub_func_uint64_t_u_u(((*g_1453) , ((((void*)0 != &g_400) <= l_2627) >= (*g_353))), l_2628)) >= g_2629)), l_2605)) < l_2484[3]) | (*g_2357)) , l_2607), 2UL)) && (***g_520)))) , (-5L)) && g_1702), l_2628)))) < l_2599) | (-1L)) <= l_2605), (*g_1453))) != l_2484[0]), 7))) , l_2515), l_2603)) & 18446744073709551611UL));
                        return g_2316[2];
                    }
                    else
                    { /* block id: 1209 */
                        int32_t l_2639 = 5L;
                        float *l_2642 = &g_356;
                        int32_t l_2643 = 0x6472494AL;
                        float l_2661 = 0xC.223A89p-10;
                        (*g_438) &= ((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_u(((*l_2604) = l_2634), l_2606)), 7)) ^ (((((g_111 , l_2635[6][9]) != &g_2093) > (((safe_mod_func_int32_t_s_s(l_2639, (safe_sub_func_int32_t_s_s(0x5E585245L, l_2603)))) , ((*l_2479) = 0x4DFE6678F7608A3ALL)) , l_2634)) <= l_2603) == (*l_38)));
                        (*l_2642) = (*g_105);
                        ++l_2645;
                        (*g_578) &= ((*l_38) || (safe_mul_func_int16_t_s_s((((safe_rshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s((!(safe_sub_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(l_2606, ((((void*)0 == &g_150[0]) > ((--(*l_2602)) ^ (((void*)0 != (***g_1915)) , ((safe_lshift_func_uint8_t_u_s(((l_2543 ^ (*l_38)) < l_2643), (*l_38))) > (*g_1453))))) > l_2639))), 5)), g_993))), 1)), (*g_1453))) > l_2605) > (*g_1453)), (*g_1453))));
                    }
                    return l_2603;
                }
            }
        }
        else
        { /* block id: 1221 */
            uint16_t l_2674 = 0x1133L;
            uint8_t *l_2677 = &g_514;
            int32_t l_2689 = 9L;
            const uint64_t *l_2696 = &g_74;
            const uint64_t * const *l_2695 = &l_2696;
            const uint64_t * const **l_2694[4] = {&l_2695,&l_2695,&l_2695,&l_2695};
            int8_t ****l_2735 = (void*)0;
            int i;
            if ((safe_lshift_func_int8_t_s_s(((safe_sub_func_uint64_t_u_u((safe_rshift_func_int16_t_s_s((safe_div_func_uint16_t_u_u((g_160 = ((*l_38) | ((l_2674 , g_664[4][0][0]) , (0x38EF0B08L <= (l_2484[2] = (((*l_2677) = 0xA0L) >= ((0x1C9558422C0A490ELL ^ ((((safe_mul_func_uint8_t_u_u(l_2484[2], l_2674)) == g_591) , 0x9C48DB4EL) , (*l_38))) != (**g_352)))))))), 0x3022L)), (*g_1453))), (*l_38))) && l_2674), 2)))
            { /* block id: 1225 */
                uint32_t *l_2690 = &l_6;
                uint32_t l_2698 = 0x5D15E732L;
                for (g_852 = 0; (g_852 == 4); g_852++)
                { /* block id: 1228 */
                    float l_2697 = 0xA.56E3DBp-94;
                    int32_t l_2700 = (-7L);
                    for (g_993 = 0; (g_993 >= 58); ++g_993)
                    { /* block id: 1231 */
                        (*g_2187) |= ((*g_438) |= ((*g_2140) = l_2674));
                    }
                    for (g_2499 = 0; (g_2499 > (-20)); g_2499--)
                    { /* block id: 1238 */
                        uint32_t **l_2691[8] = {&l_2690,(void*)0,&l_2690,&l_2690,(void*)0,&l_2690,&l_2690,(void*)0};
                        int32_t l_2699[8][3] = {{0x05E978C6L,(-7L),0x05E978C6L},{(-10L),7L,(-1L)},{0x31B66EA6L,0x31B66EA6L,(-1L)},{1L,7L,7L},{(-1L),(-7L),0x3DD756FDL},{1L,0x4D85EA95L,1L},{0x31B66EA6L,(-1L),0x3DD756FDL},{(-10L),(-10L),7L}};
                        uint64_t l_2701 = 0x87577905DF63ABC9LL;
                        int i, j;
                        (*g_578) = (safe_mul_func_int8_t_s_s(((0x2DD368A8L == ((((((safe_unary_minus_func_uint64_t_u((l_2689 = ((*l_2479) = 0x44B8B2BA6F2E1F67LL)))) ^ ((l_2692 = l_2690) != &g_2316[0])) | ((*g_2140) = 1L)) & ((~((void*)0 != l_2694[3])) & ((0xCDL > l_2484[2]) || (*g_1453)))) , l_2698) > 1L)) | g_129), l_2699[7][1]));
                        l_2701 &= (l_2700 & (*****g_518));
                        l_2699[7][1] &= ((safe_mul_func_int8_t_s_s((safe_mul_func_int8_t_s_s((((**g_1915) != (void*)0) | (&g_2495 == &l_2493[1])), ((((safe_mod_func_int64_t_s_s((safe_div_func_int64_t_s_s(((****g_519) = (l_2700 , (safe_rshift_func_int8_t_s_s(l_2698, 5)))), l_2712)), ((+((safe_lshift_func_uint16_t_u_s(g_993, 10)) != (!((((~(((***g_1554) , g_2316[0]) >= 1L)) > (*g_1453)) & (*g_2357)) < g_160)))) && g_472))) ^ l_2701) && l_2718) < l_2700))), 1L)) && l_2674);
                        if (l_2698)
                            break;
                    }
                    (*g_2187) |= (*l_38);
                }
                (*g_2187) ^= (*g_2357);
                (*g_2187) ^= (safe_div_func_int8_t_s_s((0x67C9L & (safe_lshift_func_int8_t_s_s(((safe_mul_func_int16_t_s_s(((((-1L) || (safe_add_func_uint64_t_u_u(((safe_sub_func_uint16_t_u_u(((&l_2493[1] == ((((safe_mod_func_int32_t_s_s(7L, ((*g_2140) = (*g_353)))) , (safe_add_func_uint8_t_u_u(l_2698, 3L))) , (safe_div_func_uint32_t_u_u(0xF0FC042EL, g_2))) , l_2735)) , (*l_38)), 1UL)) && 0xBBC5L), (**g_368)))) , (*l_38)) != l_2689), l_2484[4])) > l_2674), 0))), (-1L)));
            }
            else
            { /* block id: 1254 */
                float *l_2740[4][2][1] = {{{&g_2133},{&g_5}},{{&g_2133},{&g_2133}},{{&g_5},{&g_2133}},{{&g_2133},{&g_5}}};
                int i, j, k;
                l_2484[0] = (l_2738 = (safe_div_func_float_f_f(0x1.Dp-1, l_2484[2])));
            }
        }
        for (g_1702 = 3; (g_1702 <= 41); g_1702++)
        { /* block id: 1261 */
            uint32_t l_2757 = 0x010F0C00L;
            int8_t l_2784 = 0x81L;
            const uint64_t **l_2789 = (void*)0;
            const uint64_t ***l_2788 = &l_2789;
            int32_t l_2834 = 0x3AFB2E02L;
            const uint32_t l_2860 = 18446744073709551609UL;
            int64_t l_2881 = 0L;
            int32_t l_2898 = 0x57CEB50CL;
            int32_t l_2901 = 0L;
            uint8_t l_2903 = 0x19L;
            int32_t l_2924 = 0x6DDADC51L;
            int32_t ** const *l_2942 = &g_65[0];
            for (g_74 = 0; (g_74 < 22); g_74++)
            { /* block id: 1264 */
                int64_t l_2760 = 0x7BA560C4948ECBC2LL;
                uint8_t *l_2761 = &g_1592;
                int32_t l_2785 = (-3L);
                for (l_2502 = 3; (l_2502 > (-2)); l_2502 = safe_sub_func_int8_t_s_s(l_2502, 1))
                { /* block id: 1267 */
                    return (*l_38);
                }
                (*g_2357) = (safe_lshift_func_int16_t_s_s(((safe_mod_func_int16_t_s_s(((((((safe_sub_func_uint64_t_u_u((((safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((l_2543 != (l_2757 | (safe_div_func_int8_t_s_s((l_2760 = (*l_38)), ((--(*l_2761)) ^ ((safe_add_func_int8_t_s_s((l_2786 = (safe_rshift_func_int16_t_s_u((*l_38), (safe_div_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((+l_2484[4]), (safe_lshift_func_uint16_t_u_s(g_244[0][2][2], 12)))), ((safe_rshift_func_int16_t_s_s((-1L), ((safe_lshift_func_int16_t_s_u((((safe_mod_func_uint32_t_u_u((!3UL), ((+(l_2784 ^= (!(((((0x6CL <= l_2757) ^ g_42) | 2L) & l_2757) , l_2757)))) || 0xE59EC6E3L))) ^ l_2757) != (-1L)), l_2785)) & l_2785))) , 0x0CE234B1851F3B8DLL)))))), (*l_38))) != g_157)))))), 15)), l_2785)) & l_2757) , 1UL), l_2484[2])) , l_2787) == l_2788) & l_2785) , (void*)0) != (void*)0), (*g_1453))) != l_2757), (*g_1453)));
            }
        }
        for (l_2500 = 0; (l_2500 >= 36); l_2500 = safe_add_func_uint64_t_u_u(l_2500, 5))
        { /* block id: 1369 */
            (*g_2187) ^= (*l_38);
        }
        (*g_2187) = (((safe_div_func_int16_t_s_s(((*l_2498) &= ((g_400 |= (safe_div_func_int8_t_s_s(((*g_438) == (((*l_2692) = ((safe_add_func_uint16_t_u_u(g_2109, ((&g_2495 != &l_2493[1]) > ((*l_38) ^= ((safe_add_func_uint64_t_u_u((((0x5FL > (((8UL <= ((((*l_2965) == ((*g_1453) >= (*g_1453))) , (*g_1453)) != g_74)) < 0xBA5E2076L) == 0x92555541E7D3BF67LL)) , 0xF61AB930L) , 0xA5177FA2B4DFB512LL), l_2960)) < 4294967287UL))))) || (*g_1453))) <= l_2634)), l_2886[3]))) | l_2738)), l_2484[2])) , (*g_2874)) | g_129);
    }
    return (*l_38);
}


/* ------------------------------------------ */
/* 
 * reads : g_157 g_578 g_129
 * writes: g_157 g_96 g_129
 */
static int32_t * func_9(const int32_t * const  p_10, int16_t  p_11, int32_t * const  p_12)
{ /* block id: 856 */
    uint8_t l_1868 = 255UL;
    int32_t l_1880 = (-8L);
    uint32_t l_1883 = 0xBCFD26EBL;
    int32_t l_1906 = 0x476F0547L;
    int32_t l_1908 = 9L;
    int32_t l_1976 = 0L;
    int32_t l_1977 = (-5L);
    int32_t l_1978 = 1L;
    int32_t l_1979 = 0xA1F9B7E7L;
    int32_t l_1980 = (-4L);
    int32_t l_1981 = 0x88AA784DL;
    int32_t l_1982 = 0x1219BDC1L;
    int32_t l_1986 = (-1L);
    int32_t l_1988[2];
    uint64_t l_1993[3][9] = {{0UL,18446744073709551609UL,0x93FB2D7832E94647LL,0x93FB2D7832E94647LL,18446744073709551609UL,0UL,18446744073709551609UL,0x93FB2D7832E94647LL,0x93FB2D7832E94647LL},{0xB10655C5E1F6EAD2LL,0xB10655C5E1F6EAD2LL,0x0FA52F7F7B0AD357LL,18446744073709551615UL,0x0FA52F7F7B0AD357LL,0xB10655C5E1F6EAD2LL,0xB10655C5E1F6EAD2LL,0x0FA52F7F7B0AD357LL,18446744073709551615UL},{1UL,18446744073709551609UL,1UL,0UL,0UL,1UL,18446744073709551609UL,1UL,0UL}};
    uint32_t l_2038 = 0xA6786BE7L;
    uint32_t l_2067 = 9UL;
    int32_t *l_2111[1][8] = {{&l_1980,&l_1908,&l_1980,&l_1908,&l_1980,&l_1908,&l_1980,&l_1908}};
    int32_t *l_2181[1][6][2] = {{{&g_131[5][2][0],&l_1986},{&l_1986,&g_131[5][2][0]},{&l_1986,&l_1986},{&g_131[5][2][0],&l_1986},{&l_1986,&g_131[5][2][0]},{&l_1986,&l_1986}}};
    int32_t *l_2182 = &l_1977;
    uint32_t l_2235 = 0xA2B617F2L;
    const uint8_t *l_2245[3][5][8] = {{{&g_1592,&g_111,&g_1592,&g_852,&g_111,&l_1868,&l_1868,&g_111},{&g_111,&l_1868,&l_1868,&g_111,&g_852,&g_1592,&g_111,&g_1592},{&g_111,(void*)0,(void*)0,(void*)0,&g_111,(void*)0,&g_111,&g_111},{&g_1592,(void*)0,&g_852,&g_852,(void*)0,&g_1592,&l_1868,(void*)0},{&g_111,&l_1868,&g_852,&g_111,&g_852,&l_1868,&g_111,&g_1592}},{{(void*)0,&g_111,(void*)0,&g_111,&g_111,(void*)0,&g_111,(void*)0},{&g_1592,&g_111,&l_1868,&g_852,&g_111,&g_852,&l_1868,&g_111},{(void*)0,&l_1868,&g_1592,(void*)0,&g_852,&g_852,(void*)0,&g_1592},{&g_111,&g_111,(void*)0,&g_111,(void*)0,(void*)0,(void*)0,&g_111},{&g_1592,&g_111,&g_1592,&g_852,&g_111,&l_1868,&l_1868,&g_111}},{{&g_111,&l_1868,&l_1868,&l_1868,&g_111,&g_852,&l_1868,&g_852},{&l_1868,&g_852,&g_111,&g_852,&l_1868,&g_111,&g_1592,&g_1592},{&g_852,&g_852,&g_111,&g_111,&g_852,&g_852,(void*)0,&g_852},{&g_1592,(void*)0,&g_111,&g_1592,&g_111,(void*)0,&g_1592,&g_852},{&g_852,&l_1868,&g_111,&g_1592,&g_1592,&g_111,&l_1868,&g_852}}};
    const uint8_t **l_2244 = &l_2245[0][0][1];
    int32_t *l_2249 = &g_131[5][2][0];
    uint32_t *l_2364 = &g_400;
    uint32_t **l_2363 = &l_2364;
    uint16_t l_2376[4] = {0x0E5EL,0x0E5EL,0x0E5EL,0x0E5EL};
    int32_t *l_2470 = &l_1986;
    int32_t *l_2472 = &l_1978;
    int32_t *l_2473[2];
    int32_t *l_2474 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1988[i] = (-1L);
    for (i = 0; i < 2; i++)
        l_2473[i] = &g_131[2][7][0];
    for (g_157 = 1; (g_157 >= 0); g_157 -= 1)
    { /* block id: 859 */
        float * const * const **l_1869 = (void*)0;
        float ***l_1871[4];
        float ****l_1870 = &l_1871[3];
        int32_t l_1902 = 0L;
        int32_t l_1903[9] = {0x8B75BDE3L,0x8B75BDE3L,0xA3756077L,0x8B75BDE3L,0x8B75BDE3L,0xA3756077L,0x8B75BDE3L,0x8B75BDE3L,0xA3756077L};
        int32_t ** const *l_2063 = &g_1555;
        int8_t l_2065 = (-8L);
        int64_t *****l_2073 = &g_519;
        float l_2156 = (-0x1.Bp+1);
        int32_t *l_2186 = &l_1903[6];
        uint64_t **l_2191 = &g_2004;
        uint64_t ***l_2190[2];
        uint64_t ****l_2189 = &l_2190[0];
        int32_t l_2297 = 0x4BA2F905L;
        int8_t *l_2302 = &g_664[1][1][1];
        uint32_t *l_2362[3];
        uint32_t **l_2361 = &l_2362[2];
        uint32_t l_2406[9] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
        int32_t l_2428[1][8][7] = {{{0x4DA65A2AL,5L,0x7D4D2D1FL,5L,0x4DA65A2AL,9L,0x4DA65A2AL},{0x2AC872CCL,(-4L),(-4L),0x2AC872CCL,0x35123A7DL,(-4L),6L},{0x39B8ADE0L,5L,0x39B8ADE0L,0xB353E940L,1L,0xB353E940L,0x39B8ADE0L},{0x2AC872CCL,0x2AC872CCL,0x03EFB8D7L,6L,0x2AC872CCL,0xE865AEAEL,6L},{0x4DA65A2AL,0xB353E940L,0x5B313F41L,5L,0x5B313F41L,0xB353E940L,0x4DA65A2AL},{0x35123A7DL,6L,(-4L),0x35123A7DL,0x2AC872CCL,(-4L),(-4L)},{1L,5L,6L,5L,1L,9L,1L},{0x2AC872CCL,0x35123A7DL,(-4L),6L,0x35123A7DL,0x35123A7DL,6L}}};
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1871[i] = &g_149;
        for (i = 0; i < 2; i++)
            l_2190[i] = &l_2191;
        for (i = 0; i < 3; i++)
            l_2362[i] = &g_2316[0];
        (*g_578) = l_1868;
        (*l_1870) = (void*)0;
    }
    for (g_129 = 26; (g_129 >= (-29)); g_129 = safe_sub_func_int16_t_s_s(g_129, 5))
    { /* block id: 1138 */
        int32_t *l_2468[3];
        int32_t *l_2469 = &l_1979;
        int32_t *l_2471 = &l_1980;
        int i;
        for (i = 0; i < 3; i++)
            l_2468[i] = &l_1979;
        return l_2473[0];
    }
    return l_2474;
}


/* ------------------------------------------ */
/* 
 * reads : g_993 g_96 g_131 g_111 g_42 g_2 g_1453 g_404 g_1702 g_352 g_379 g_157 g_520 g_521 g_1002 g_664 g_1652 g_1179 g_1180 g_66 g_472 g_1554 g_852 g_578 g_1399 g_370 g_244 g_1521 g_992 g_268 g_160 g_353 g_591
 * writes: g_1453 g_852 g_404 g_472 g_1512 g_1702 g_353 g_42 g_1555 g_131 g_96 g_160 g_1777
 */
static int64_t  func_15(int32_t * p_16, const uint16_t  p_17, int32_t * p_18)
{ /* block id: 669 */
    int32_t l_1428[9][3][6] = {{{0x5F2C3B86L,1L,1L,(-8L),0x8E4F76A9L,(-4L)},{0x467A859AL,0L,0xD2F436CAL,0xE34D4101L,(-7L),0x377535D4L},{(-5L),(-5L),0L,(-1L),0xE8536625L,0xD920A162L}},{{(-1L),0xF333A66CL,(-1L),1L,8L,0x4192A042L},{0xF333A66CL,1L,(-1L),0xC2342643L,3L,(-3L)},{0x1B19B4CDL,0xBC7CF36FL,1L,0x656F3F87L,0x377535D4L,5L}},{{(-4L),(-3L),0x8E4F76A9L,0x1B19B4CDL,0x467A859AL,(-9L)},{3L,0L,0x4192A042L,(-4L),1L,0L},{2L,(-1L),1L,(-1L),2L,0L}},{{0L,0L,7L,1L,1L,8L},{(-1L),(-1L),1L,0L,0xD809BBB9L,8L},{0xC2342643L,1L,7L,0x390ADB3CL,0x6597ACD2L,0L}},{{0xD809BBB9L,(-8L),1L,(-7L),0L,0xA8B75C07L},{(-6L),0xBED7170EL,(-1L),0L,0xC6FB3615L,(-2L)},{0xD2F436CAL,1L,0L,1L,(-6L),0L}},{{0xD6CD3B8FL,4L,0x467A859AL,0L,1L,(-9L)},{0xE8536625L,0x1B19B4CDL,(-6L),7L,0x19D7E1F0L,(-1L)},{0xD809BBB9L,(-2L),0xF333A66CL,8L,0xD4C5A1CBL,0xE8536625L}},{{(-1L),0xD920A162L,0x3F742A9BL,0x390ADB3CL,(-7L),(-1L)},{1L,0x8E4F76A9L,0x19D7E1F0L,1L,0x377535D4L,0x377535D4L},{0x6597ACD2L,(-2L),(-2L),0x6597ACD2L,0x1644D53DL,0xE34D4101L}},{{0x19D7E1F0L,(-1L),(-2L),0L,(-7L),0xD2F436CAL},{8L,0x467A859AL,0xA7678EEFL,0L,(-7L),0xC6FB3615L},{0xC2342643L,(-1L),0x4192A042L,(-6L),0x1644D53DL,(-4L)}},{{1L,(-2L),(-1L),0L,0x377535D4L,0xBED7170EL},{0x3F742A9BL,0x8E4F76A9L,1L,(-6L),(-7L),1L},{1L,0xD920A162L,1L,0x1644D53DL,0xD4C5A1CBL,(-1L)}}};
    int32_t *l_1429 = &g_42;
    int32_t l_1430 = 0x7727675BL;
    int32_t *l_1431 = &g_131[5][2][0];
    int32_t *l_1432 = &g_131[6][7][0];
    int32_t *l_1433 = &g_42;
    int32_t l_1434 = (-3L);
    int32_t l_1435 = 0L;
    int32_t *l_1436 = &l_1434;
    int32_t l_1437[9][9] = {{0x897A3360L,0x8657BC21L,0x8657BC21L,0x897A3360L,0L,0x897A3360L,0x8657BC21L,0x8657BC21L,0x897A3360L},{0xC98FC8CAL,(-1L),0xA5195B55L,(-1L),0xC98FC8CAL,0xC98FC8CAL,(-1L),0xA5195B55L,(-1L)},{0x8657BC21L,0L,0x1380860DL,0x1380860DL,0L,0x8657BC21L,0L,0x1380860DL,0x1380860DL},{0xC98FC8CAL,0xC98FC8CAL,(-1L),0xA5195B55L,(-1L),0xC98FC8CAL,0xC98FC8CAL,(-1L),0xA5195B55L},{0x897A3360L,0L,0x897A3360L,0x8657BC21L,0x8657BC21L,0x897A3360L,0L,0x897A3360L,0x8657BC21L},{(-1L),(-1L),(-1L),(-1L),0xCBF7E602L,(-1L),(-1L),(-1L),(-1L)},{0x0B161203L,0x8657BC21L,0x1380860DL,0x8657BC21L,0x0B161203L,0x0B161203L,0x8657BC21L,0x1380860DL,0x8657BC21L},{(-1L),0xCBF7E602L,0xA5195B55L,0xA5195B55L,0xCBF7E602L,(-1L),0xCBF7E602L,0xA5195B55L,0xA5195B55L},{0x0B161203L,0x0B161203L,0x8657BC21L,0x1380860DL,0x8657BC21L,0x0B161203L,0x0B161203L,0x8657BC21L,0x1380860DL}};
    int32_t *l_1438 = (void*)0;
    int32_t *l_1439 = &l_1434;
    int32_t *l_1440 = (void*)0;
    int32_t *l_1441 = &l_1434;
    int32_t *l_1442 = &g_96;
    int32_t *l_1443[5][10] = {{&l_1434,&g_96,&g_131[2][5][0],&g_2,&g_131[2][5][0],&g_96,&l_1434,&l_1434,&l_1434,&l_1435},{&g_96,&l_1434,&l_1434,&g_86,&l_1437[3][7],&g_131[2][5][0],&l_1434,&l_1434,&l_1434,&l_1434},{&l_1437[0][2],&l_1434,&l_1434,&l_1434,&g_86,&g_2,&l_1434,&l_1434,&g_86,&l_1435},{&l_1434,&g_131[2][5][0],&l_1434,&g_2,&l_1435,&l_1437[3][7],&g_96,&l_1437[0][2],&g_96,&l_1437[3][7]},{&l_1434,&g_96,&g_131[5][2][0],&g_96,&l_1434,&g_2,&l_1437[3][7],&l_1434,(void*)0,(void*)0}};
    uint8_t l_1444 = 255UL;
    int16_t *l_1451 = &g_404;
    int16_t **l_1452[3][6][7] = {{{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,(void*)0,(void*)0},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,(void*)0,(void*)0},{(void*)0,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,(void*)0,&l_1451},{&l_1451,(void*)0,(void*)0,&l_1451,&l_1451,&l_1451,&l_1451},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451}},{{(void*)0,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451},{&l_1451,(void*)0,(void*)0,&l_1451,(void*)0,&l_1451,&l_1451},{(void*)0,&l_1451,(void*)0,&l_1451,&l_1451,&l_1451,&l_1451},{(void*)0,&l_1451,(void*)0,&l_1451,&l_1451,&l_1451,&l_1451},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,(void*)0}},{{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,(void*)0},{&l_1451,&l_1451,&l_1451,&l_1451,(void*)0,&l_1451,&l_1451},{&l_1451,&l_1451,(void*)0,&l_1451,&l_1451,&l_1451,(void*)0},{&l_1451,&l_1451,&l_1451,(void*)0,&l_1451,&l_1451,&l_1451},{(void*)0,&l_1451,&l_1451,(void*)0,(void*)0,&l_1451,&l_1451},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451}}};
    uint64_t *l_1462 = &g_1002;
    uint64_t *l_1464 = &g_192;
    uint64_t **l_1463 = &l_1464;
    uint64_t *l_1465[7][5][6] = {{{&g_1002,&g_192,&g_74,&g_74,&g_192,&g_74},{&g_192,&g_1002,&g_192,&g_74,&g_192,&g_192},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_192,&g_74,&g_192,&g_74,&g_74,(void*)0},{&g_1002,&g_74,&g_1002,&g_1002,(void*)0,&g_1002}},{{&g_1002,&g_192,&g_1002,&g_74,&g_1002,&g_1002},{&g_192,&g_74,&g_192,&g_74,(void*)0,&g_74},{&g_192,&g_192,&g_1002,&g_1002,&g_192,(void*)0},{&g_74,&g_74,(void*)0,&g_192,&g_74,&g_74},{&g_192,&g_1002,&g_1002,&g_74,&g_192,&g_192}},{{&g_74,&g_1002,&g_1002,(void*)0,(void*)0,&g_74},{&g_192,&g_1002,(void*)0,&g_192,(void*)0,&g_1002},{&g_192,&g_74,&g_74,&g_1002,&g_1002,(void*)0},{(void*)0,(void*)0,&g_192,&g_1002,&g_1002,&g_74},{&g_1002,(void*)0,(void*)0,&g_192,(void*)0,(void*)0}},{{(void*)0,&g_74,&g_192,&g_74,&g_74,&g_1002},{(void*)0,(void*)0,&g_192,(void*)0,&g_74,&g_74},{&g_1002,(void*)0,&g_74,(void*)0,&g_74,&g_1002},{&g_74,&g_74,&g_74,&g_74,(void*)0,&g_1002},{&g_1002,(void*)0,&g_192,&g_192,&g_1002,&g_192}},{{&g_192,(void*)0,&g_1002,(void*)0,&g_1002,&g_74},{&g_192,&g_74,&g_1002,&g_74,(void*)0,&g_74},{&g_1002,&g_1002,&g_192,&g_74,(void*)0,&g_74},{&g_74,&g_1002,(void*)0,&g_74,&g_192,&g_74},{(void*)0,&g_1002,&g_74,&g_1002,&g_74,&g_74}},{{&g_1002,&g_74,(void*)0,&g_1002,&g_192,&g_1002},{(void*)0,&g_192,&g_1002,(void*)0,(void*)0,&g_1002},{&g_192,&g_192,&g_192,&g_74,&g_192,&g_1002},{&g_1002,&g_1002,&g_74,&g_192,(void*)0,&g_192},{&g_192,&g_1002,&g_74,&g_1002,&g_192,&g_1002}},{{&g_74,&g_1002,&g_192,&g_74,&g_74,&g_1002},{&g_74,&g_74,&g_1002,&g_192,&g_74,&g_1002},{&g_192,(void*)0,(void*)0,(void*)0,&g_1002,&g_74},{&g_1002,&g_192,&g_74,&g_1002,&g_192,&g_74},{&g_1002,&g_74,(void*)0,&g_74,&g_74,&g_74}}};
    int32_t l_1511 = 0L;
    uint32_t l_1598[7];
    uint32_t *l_1639[1][6] = {{&l_1598[4],&g_1304,&g_1304,&l_1598[4],&g_1304,&g_1304}};
    int32_t l_1694 = (-1L);
    int64_t **l_1713 = &g_379;
    int32_t l_1812 = 0xB4253378L;
    int64_t l_1834 = 0x8AD072FD3E564C20LL;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1598[i] = 18446744073709551609UL;
lbl_1732:
    l_1444--;
    if (((((((g_993 != (safe_add_func_int64_t_s_s((*l_1442), p_17))) , (safe_lshift_func_int8_t_s_s(((g_1453 = l_1451) == &g_404), 4))) && (safe_mod_func_int8_t_s_s(((safe_lshift_func_int16_t_s_u((((safe_sub_func_int32_t_s_s((safe_rshift_func_int16_t_s_u((((((*l_1463) = (l_1462 = (void*)0)) == l_1465[2][0][3]) < ((safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u(((~p_17) > p_17), p_17)), 2)) | (*l_1432))) >= (*l_1431)), 1)), p_17)) < p_17) , 0L), g_111)) , 0xCFL), (*l_1429)))) < (*l_1432)) , p_17) & 1UL))
    { /* block id: 674 */
        int8_t l_1486[1][8] = {{0x6CL,0x6CL,0x6CL,0x6CL,0x6CL,0x6CL,0x6CL,0x6CL}};
        int32_t l_1490 = 1L;
        int32_t l_1494 = (-1L);
        int32_t l_1495 = 0x7CF7038DL;
        int32_t l_1496 = 0xE47896A2L;
        int32_t l_1501 = (-8L);
        int32_t l_1502 = (-7L);
        int8_t l_1514 = 0x3FL;
        int64_t ****l_1520 = &g_520;
        int32_t l_1593 = (-10L);
        int32_t l_1594 = 0xF230BFE4L;
        int32_t l_1596[4];
        uint64_t *l_1620 = &g_1002;
        uint32_t *l_1637 = (void*)0;
        const uint8_t *l_1688 = &g_514;
        const uint8_t **l_1687 = &l_1688;
        const uint8_t ***l_1686 = &l_1687;
        float ***l_1723[4][10] = {{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149}};
        int32_t * const *l_1738 = &g_1556;
        uint64_t l_1797 = 7UL;
        int32_t l_1799 = 2L;
        int32_t *l_1827 = &g_131[5][2][0];
        uint8_t l_1839 = 0x1DL;
        int i, j;
        for (i = 0; i < 4; i++)
            l_1596[i] = 0L;
        for (g_852 = 1; (g_852 <= 4); g_852 += 1)
        { /* block id: 677 */
            uint16_t l_1473 = 0xF73DL;
            int32_t l_1477[2];
            uint32_t l_1653 = 4UL;
            int i;
            for (i = 0; i < 2; i++)
                l_1477[i] = (-4L);
            (*l_1439) ^= (*p_18);
            for (g_404 = 0; (g_404 <= 4); g_404 += 1)
            { /* block id: 681 */
                int8_t l_1471[4];
                int32_t l_1476 = 0x9E41B983L;
                float l_1482 = 0x4.21BDCDp-66;
                int32_t l_1484 = (-1L);
                int32_t l_1485 = 0x1106CB65L;
                int32_t l_1489 = 0L;
                int32_t l_1492 = (-6L);
                int32_t l_1498 = 0xA06CF44CL;
                int32_t l_1503 = 1L;
                int32_t l_1505 = (-10L);
                uint64_t l_1506 = 18446744073709551613UL;
                int8_t l_1510 = (-1L);
                int32_t l_1513[2][9][7] = {{{(-2L),0x072631A8L,(-8L),0xC70B9191L,(-3L),(-8L),2L},{(-8L),0xE35ECABDL,0x072631A8L,0x774AD315L,(-1L),(-1L),0xDA378EF4L},{8L,(-4L),0x5ACA3484L,2L,0L,0xA436D8D6L,8L},{(-2L),(-1L),0x09E4AA0DL,1L,0x072631A8L,0x94320133L,8L},{0x072631A8L,8L,(-3L),(-3L),8L,0x072631A8L,0xDA378EF4L},{(-4L),0xC70B9191L,0x1413CAC5L,(-2L),0x1A910347L,0x333717C5L,2L},{1L,0x4FFBE605L,0xB8DC2D83L,0x1A910347L,0x774AD315L,0x94320133L,(-4L)},{0xA2B1EDABL,0xC70B9191L,0x072631A8L,(-1L),1L,(-5L),0x774AD315L},{6L,8L,0x055FCA73L,6L,0x1A910347L,(-6L),0x8D9FC914L}},{{0x8D9FC914L,(-1L),0x4FFBE605L,6L,(-8L),(-8L),6L},{0x774AD315L,(-4L),0x774AD315L,(-1L),2L,(-3L),0xA2B1EDABL},{(-4L),0xE35ECABDL,0L,0x1A910347L,0L,0xDE8EFA3EL,1L},{2L,0x072631A8L,0x4FFBE605L,(-2L),0x09E4AA0DL,(-3L),(-4L)},{0xDA378EF4L,2L,(-8L),(-3L),0xC70B9191L,(-8L),0x072631A8L},{8L,0xE35ECABDL,0x333717C5L,1L,(-1L),(-6L),(-2L)},{8L,(-5L),0xB8DC2D83L,2L,0xB8DC2D83L,(-5L),8L},{0xDA378EF4L,0xCDEA3122L,0x09E4AA0DL,0x774AD315L,2L,0x94320133L,(-8L)},{2L,8L,0x7393CFA6L,0xC70B9191L,8L,0x333717C5L,(-2L)}}};
                uint32_t l_1515 = 0x3DF1504DL;
                int32_t *l_1536 = &g_131[5][7][0];
                int32_t *l_1553 = &l_1428[7][2][3];
                int32_t **l_1552 = &l_1553;
                int32_t ***l_1551 = &l_1552;
                uint64_t l_1563 = 0x6EF755CDA8F58340LL;
                int32_t l_1595[3];
                int32_t l_1597 = 0xFACF658CL;
                uint8_t *l_1621 = &l_1444;
                int64_t *l_1622 = &g_244[0][7][1];
                int i, j, k;
                for (i = 0; i < 4; i++)
                    l_1471[i] = 0x52L;
                for (i = 0; i < 3; i++)
                    l_1595[i] = 0xA174B403L;
            }
            if ((*p_18))
                break;
            return l_1596[3];
        }
        if (l_1496)
        { /* block id: 759 */
            int32_t l_1683 = 0x75548FC6L;
            uint8_t **l_1685 = &g_639[1][0][0];
            uint8_t ***l_1684[3];
            const uint8_t ****l_1689 = &l_1686;
            int32_t l_1695 = 0xC03BF3B1L;
            int32_t l_1696[7][7] = {{(-3L),(-3L),0xF0480B1AL,0x1A4E68C4L,0xF0480B1AL,(-3L),(-3L)},{1L,0x6879DB22L,0xD0735BB6L,0x6879DB22L,1L,0x629B7BABL,1L},{0xDDD8BD6AL,0x73971A22L,0xDDD8BD6AL,0xF0480B1AL,0xF0480B1AL,0xDDD8BD6AL,0x73971A22L},{1L,(-1L),0xD0735BB6L,0x779D1374L,(-4L),0x6879DB22L,(-4L)},{0xDDD8BD6AL,0xF0480B1AL,0xDDD8BD6AL,0x1A4E68C4L,(-3L),0x1A4E68C4L,0xDDD8BD6AL},{1L,0x6879DB22L,(-8L),(-1L),(-8L),0x6879DB22L,1L},{0xF0480B1AL,0xDDD8BD6AL,0x73971A22L,0xDDD8BD6AL,0xF0480B1AL,0xF0480B1AL,0xDDD8BD6AL}};
            int32_t l_1756 = 1L;
            float *l_1825 = &g_5;
            int32_t *l_1826[9] = {&l_1490,&l_1495,&l_1490,&l_1490,&l_1495,&l_1490,&l_1490,&l_1495,&l_1490};
            int32_t l_1829 = 0xF9D7B612L;
            uint64_t * const *l_1846[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint64_t * const **l_1845 = &l_1846[2];
            int64_t ****l_1853 = (void*)0;
            uint16_t l_1859 = 0x4696L;
            int i, j;
            for (i = 0; i < 3; i++)
                l_1684[i] = &l_1685;
            (*l_1439) = (((safe_sub_func_int16_t_s_s((*g_1453), l_1683)) , l_1684[1]) != ((*l_1689) = l_1686));
            for (g_472 = 0; (g_472 < 6); g_472 = safe_add_func_uint32_t_u_u(g_472, 5))
            { /* block id: 764 */
                int64_t l_1697 = (-1L);
                int32_t l_1698 = (-1L);
                int32_t l_1701[9];
                uint64_t ***l_1717 = &l_1463;
                uint64_t ****l_1716 = &l_1717;
                float ***l_1726 = &g_149;
                int32_t **l_1739[3][3][6] = {{{&g_1556,&g_1556,&g_1556,&g_1556,&g_1556,&g_1556},{&g_1556,&g_1556,(void*)0,&g_1556,&g_1556,(void*)0},{&g_1556,&g_1556,(void*)0,(void*)0,&g_1556,&g_1556}},{{&g_1556,&g_1556,&g_1556,&g_1556,&g_1556,&g_1556},{&g_1556,&g_1556,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1556,&g_1556,(void*)0,&g_1556,(void*)0,&g_1556}},{{&g_1556,&g_1556,&g_1556,(void*)0,(void*)0,(void*)0},{&g_1556,&g_1556,&g_1556,&g_1556,&g_1556,&g_1556},{&g_1556,&g_1556,(void*)0,&g_1556,&g_1556,(void*)0}}};
                uint8_t l_1788 = 249UL;
                float l_1794 = (-0x4.5p+1);
                uint16_t l_1806[6][5] = {{0xC50FL,65535UL,65535UL,0xC50FL,0x83DEL},{0x9668L,0xC50FL,0x5484L,65529UL,65529UL},{0x3A83L,0xC50FL,0x3A83L,0x83DEL,0xC50FL},{65529UL,65535UL,0x83DEL,65529UL,0x83DEL},{65529UL,65529UL,0x5484L,0xC50FL,0x9668L},{0x3A83L,0x9668L,0x83DEL,0x83DEL,0x9668L}};
                uint32_t **l_1818 = &g_1817;
                int i, j, k;
                for (i = 0; i < 9; i++)
                    l_1701[i] = 8L;
                for (g_1512 = 28; (g_1512 < (-16)); g_1512--)
                { /* block id: 767 */
                    uint8_t l_1708 = 1UL;
                    uint64_t *** const l_1715 = &l_1463;
                    uint64_t *** const *l_1714 = &l_1715;
                    int32_t l_1727 = 0x6CDDD5E4L;
                    float ** const l_1733 = &g_150[1];
                    if ((l_1694 = l_1490))
                    { /* block id: 769 */
                        int16_t l_1699[2][10] = {{0xBE42L,1L,1L,0xBE42L,(-7L),0xE4E3L,(-7L),0xBE42L,1L,1L},{(-7L),1L,(-1L),0x23C8L,0x23C8L,(-1L),1L,(-7L),1L,(-1L)}};
                        int32_t l_1700[6];
                        int i, j;
                        for (i = 0; i < 6; i++)
                            l_1700[i] = 0xEF2D531BL;
                        g_1702++;
                        if (l_1596[1])
                            continue;
                    }
                    else
                    { /* block id: 772 */
                        (*g_352) = &l_1696[4][5];
                        return (*g_379);
                    }
                    for (g_42 = 0; (g_42 < (-16)); g_42 = safe_sub_func_uint16_t_u_u(g_42, 3))
                    { /* block id: 778 */
                        int16_t l_1707[1][5];
                        int32_t l_1720 = (-5L);
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 5; j++)
                                l_1707[i][j] = (-1L);
                        }
                        l_1708++;
                        l_1727 = ((safe_mul_func_float_f_f(((((*g_520) != l_1713) , l_1714) == ((*p_18) , l_1716)), ((safe_mod_func_int8_t_s_s(g_1002, ((l_1720 ^= p_17) & ((((((safe_sub_func_int16_t_s_s((l_1723[0][8] == ((((safe_mod_func_uint16_t_u_u((l_1593 & p_17), g_664[3][0][0])) <= g_1652) , l_1707[0][3]) , &g_149)), g_96)) , (void*)0) == l_1726) != 0x3CL) | p_17) , (-7L))))) , p_17))) == (-0x4.8p+1));
                        return (*l_1429);
                    }
                    for (l_1444 = 13; (l_1444 >= 26); l_1444 = safe_add_func_int16_t_s_s(l_1444, 1))
                    { /* block id: 786 */
                        int32_t **l_1730[3];
                        int32_t **l_1731 = &l_1443[0][8];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1730[i] = &l_1440;
                        (*l_1731) = (**g_1179);
                        if (g_472)
                            goto lbl_1732;
                    }
                    (*g_578) |= ((*l_1715) == (((*l_1431) = ((l_1733 == l_1733) && (0x9AA36FF1L && ((safe_div_func_uint32_t_u_u((((safe_add_func_int16_t_s_s((((((l_1701[1] , l_1738) == ((*g_1554) = l_1739[2][2][3])) != ((((safe_rshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s((safe_add_func_uint32_t_u_u(0xCFA6980CL, ((safe_lshift_func_int8_t_s_u((((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint16_t_u_s((safe_lshift_func_uint16_t_u_u((((65535UL || l_1696[3][5]) > 0xE1B2A2A6L) , 0x4FB4L), 13)), 12)), p_17)) >= p_17) <= 4294967286UL), p_17)) ^ 0xFAA5928AFC67ECA2LL))), p_17)) , l_1727), p_17)), 1)) && 6L) != l_1756) != p_17)) , p_17) != l_1683), p_17)) & g_852) | p_17), 1L)) == p_17)))) , &l_1465[0][4][5]));
                }
                for (g_160 = 0; (g_160 > 55); g_160 = safe_add_func_int32_t_s_s(g_160, 2))
                { /* block id: 796 */
                    int16_t l_1774 = 1L;
                    int32_t l_1798[10] = {1L,1L,6L,1L,1L,6L,1L,1L,6L,1L};
                    int8_t l_1801 = (-5L);
                    int8_t l_1802 = (-1L);
                    const int32_t *l_1809[4][5][6] = {{{&l_1696[3][1],&l_1596[2],(void*)0,&g_131[4][5][0],&l_1502,(void*)0},{(void*)0,&l_1798[8],&l_1495,&l_1594,&l_1798[9],&l_1594},{&l_1798[9],&l_1701[0],&l_1798[9],&l_1698,(void*)0,(void*)0},{&l_1593,&g_86,(void*)0,&l_1495,(void*)0,&l_1502},{(void*)0,&l_1695,(void*)0,&l_1495,&l_1490,&l_1698}},{{&l_1593,&l_1798[9],&l_1799,&l_1698,(void*)0,&l_1596[1]},{&l_1798[9],(void*)0,&l_1798[4],&l_1594,&l_1594,&l_1437[8][1]},{(void*)0,(void*)0,&l_1494,&l_1695,&l_1798[4],&l_1798[4]},{&l_1798[8],(void*)0,(void*)0,&l_1798[8],&l_1695,&l_1799},{(void*)0,&l_1435,&l_1501,&l_1495,&l_1596[1],(void*)0}},{{&l_1701[0],&l_1798[8],&l_1490,&g_131[4][5][0],&l_1596[1],(void*)0},{&l_1798[4],&l_1435,(void*)0,&l_1502,&l_1695,&l_1798[9]},{&g_131[5][2][0],(void*)0,&l_1696[3][1],&l_1593,&l_1798[4],&l_1495},{(void*)0,(void*)0,(void*)0,&l_1798[4],(void*)0,(void*)0},{&l_1495,&l_1594,&l_1798[9],&l_1594,&l_1495,&l_1798[8]}},{{&l_1701[5],(void*)0,&l_1593,&l_1435,&l_1701[0],&l_1593},{&l_1799,&l_1502,(void*)0,(void*)0,&l_1594,&l_1593},{&g_86,&l_1698,&l_1593,(void*)0,(void*)0,&l_1798[8]},{&l_1594,&l_1596[1],&l_1798[9],&g_86,(void*)0,(void*)0},{&g_2,&l_1437[8][1],(void*)0,&l_1593,&l_1596[1],&l_1495}}};
                    int i, j, k;
                    if ((p_17 >= (safe_sub_func_int16_t_s_s(((*g_1453) , ((void*)0 == g_1399)), l_1701[5]))))
                    { /* block id: 797 */
                        int64_t l_1761 = 0x9873D7CF9A1C1B4BLL;
                        int8_t *l_1775 = (void*)0;
                        int8_t *l_1776 = &g_1512;
                        g_1777 = (0xD3L | ((l_1761 , ((safe_mod_func_uint64_t_u_u((safe_sub_func_int32_t_s_s(((((safe_rshift_func_int16_t_s_u(((*l_1436) > (*p_18)), 15)) && ((l_1494 = p_17) & (((*l_1776) = (p_17 > ((*g_1453) , (safe_sub_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((safe_sub_func_int8_t_s_s((p_17 , g_664[4][0][0]), l_1774)), (***g_520))), 0x0F4BL))))) ^ l_1701[5]))) , 0xC8L) ^ g_370), 0xA3EA22B4L)), g_404)) > g_244[0][2][7])) == 4L));
                    }
                    else
                    { /* block id: 801 */
                        int32_t l_1795 = 7L;
                        int8_t *l_1796 = &l_1486[0][5];
                        int32_t l_1800 = 1L;
                        int32_t l_1803 = 0xEC95F4BAL;
                        int32_t l_1804 = 1L;
                        int32_t l_1805[5] = {0x1847D10CL,0x1847D10CL,0x1847D10CL,0x1847D10CL,0x1847D10CL};
                        int i;
                        (*l_1429) ^= (safe_div_func_int8_t_s_s(((safe_add_func_uint16_t_u_u(((1L & (safe_rshift_func_uint16_t_u_s((0x62L | ((*l_1796) = (safe_div_func_int64_t_s_s(((((****g_1521) >= ((-3L) <= g_370)) , (safe_mul_func_float_f_f(((-0x1.9p-1) <= (l_1788 > (*g_992))), (l_1795 = ((!(safe_mul_func_float_f_f((safe_add_func_float_f_f(((((void*)0 != (*g_1521)) > l_1697) < 0x8.3p-1), l_1794)), p_17))) > 0x0.8p+1))))) , 8L), p_17)))), 14))) == g_160), l_1797)) , l_1683), l_1698));
                        l_1806[5][1]--;
                        l_1809[2][4][5] = (*g_352);
                    }
                }
            }
        }
        else
        { /* block id: 842 */
            uint8_t l_1864 = 0x19L;
            for (g_472 = 1; (g_472 == 6); g_472++)
            { /* block id: 845 */
                return (****g_1521);
            }
            l_1864--;
        }
    }
    else
    { /* block id: 850 */
        const uint8_t *l_1867[7];
        int i;
        for (i = 0; i < 7; i++)
            l_1867[i] = &g_514;
        (*l_1439) = ((*l_1433) |= (g_591 , (((l_1867[3] == &g_1592) , &g_1817) == (void*)0)));
        return p_17;
    }
    return p_17;
}


/* ------------------------------------------ */
/* 
 * reads : g_111 g_514 g_857 g_356 g_1304 g_157 g_2 g_704 g_578 g_96 g_1179 g_1180 g_66 g_993 g_1034 g_1414 g_518 g_519 g_520 g_521 g_379 g_149 g_150 g_192 g_353 g_131
 * writes: g_111 g_5 g_268 g_664 g_514 g_96 g_74 g_704 g_438 g_993 g_1034
 */
static int32_t * func_19(int32_t  p_20, int32_t * const  p_21)
{ /* block id: 619 */
    float l_1292 = 0x2.Dp-1;
    int32_t l_1323 = 5L;
    uint8_t **l_1326 = (void*)0;
    int16_t l_1342 = 1L;
    int32_t l_1346 = 0xA5B9C8D4L;
    int32_t l_1347 = (-1L);
    int32_t l_1348 = (-1L);
    int32_t l_1350[10][9] = {{0x88D0B068L,0x1D3B6928L,2L,0xF5057A30L,7L,0xB9A392A0L,0xAD90721CL,0x2F9B33EDL,0xF88B66FDL},{0x3C72B778L,0x1EEBBD3BL,0x20FE02A7L,0x1D3B6928L,0xB12D22B2L,0x8D297A47L,8L,0xD9124C57L,0x7DF303E5L},{0x8D297A47L,(-1L),0x3C72B778L,0x1D3B6928L,0x009A971FL,0xF88B66FDL,0x009A971FL,0x1D3B6928L,0x3C72B778L},{(-6L),(-6L),(-1L),0xF5057A30L,(-3L),0x2F9B33EDL,1L,0xB12D22B2L,0x4EAE6942L},{0xF5057A30L,7L,0x9BBD9C7BL,0x761D84BEL,0x1D3B6928L,7L,0x7024528FL,0x20FE02A7L,0xF4CE402FL},{0xF88B66FDL,0x20FE02A7L,(-1L),8L,0xF4CE402FL,0x2A46BBC9L,0x3C72B778L,2L,0xAD90721CL},{2L,1L,0x3C72B778L,0x7024528FL,0xED7226A8L,2L,(-1L),0x8F3C6E41L,0xE48C5CD9L},{0xED7226A8L,1L,0x20FE02A7L,0L,0x2475CF97L,0L,0x20FE02A7L,1L,0xED7226A8L},{(-1L),0x20FE02A7L,2L,0xAD90721CL,(-6L),0x225294A8L,0L,0xECF9BF8DL,0xB9A392A0L},{0x7024528FL,7L,7L,(-6L),0xE48C5CD9L,0xECF9BF8DL,0x7DF303E5L,0xED7226A8L,0x8F3C6E41L}};
    uint64_t *l_1380[8] = {&g_192,&g_192,&g_192,&g_192,&g_192,&g_192,&g_192,&g_192};
    uint64_t **l_1379 = &l_1380[3];
    uint64_t ***l_1378 = &l_1379;
    int32_t * const ***l_1412[9][7] = {{&g_1179,(void*)0,&g_1179,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1179,&g_1179,&g_1179,&g_1179,&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1179,(void*)0,&g_1179,&g_1179,&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1179,(void*)0,&g_1179,&g_1179,&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1179,(void*)0,&g_1179,&g_1179,&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179,(void*)0,(void*)0,(void*)0,(void*)0}};
    int16_t *l_1421[3][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_404,&l_1342,&l_1342,&g_404,&l_1342},{(void*)0,(void*)0,&l_1342,(void*)0,(void*)0}};
    int16_t l_1422 = 0x7DE0L;
    int32_t l_1423 = 0L;
    uint8_t *l_1424[10] = {&g_514,&g_514,&g_514,&g_514,&g_514,&g_514,&g_514,&g_514,&g_514,&g_514};
    int32_t l_1425 = 0x7333F9FAL;
    int16_t l_1426[5];
    int32_t *l_1427 = (void*)0;
    int i, j;
    for (i = 0; i < 5; i++)
        l_1426[i] = 0x2EBFL;
    for (g_111 = 18; (g_111 <= 5); g_111 = safe_sub_func_int32_t_s_s(g_111, 4))
    { /* block id: 622 */
        int32_t *****l_1289 = (void*)0;
        int32_t l_1306 = 0x19FD76A0L;
        uint64_t **l_1314 = (void*)0;
        uint64_t ***l_1313 = &l_1314;
        int32_t l_1334 = 0x546A0586L;
        int32_t l_1335 = 0x88CF1213L;
        int32_t l_1337 = 0x7BC901CBL;
        int32_t l_1338 = 0xAD642130L;
        int32_t l_1340 = (-1L);
        int32_t l_1341 = (-1L);
        int32_t l_1343 = 9L;
        int32_t l_1344[3][8] = {{0x68BB3290L,(-1L),0L,0L,(-1L),0x68BB3290L,(-1L),0x68BB3290L},{(-1L),0x68BB3290L,(-1L),0x68BB3290L,(-1L),0L,0L,(-1L)},{0x68BB3290L,(-2L),(-2L),0x68BB3290L,9L,(-1L),9L,0x68BB3290L}};
        int i, j;
        if ((g_514 && p_20))
        { /* block id: 623 */
            float *l_1293 = &g_5;
            int32_t l_1302 = (-6L);
            float *l_1303 = &g_268;
            uint32_t l_1305 = 0x0E48F629L;
            uint64_t ***l_1315 = &l_1314;
            int8_t *l_1320 = &g_664[2][1][0];
            int8_t **l_1319[9] = {&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320};
            uint8_t *l_1322 = &g_514;
            uint8_t **l_1327[2][4][5] = {{{&g_639[1][0][0],&g_639[1][0][0],&l_1322,&g_639[1][0][0],&g_639[1][0][0]},{&l_1322,&g_639[1][0][0],&l_1322,&l_1322,&g_639[1][0][0]},{&g_639[1][0][0],&g_639[7][0][1],&g_639[0][0][0],&g_639[1][0][0],&g_639[0][0][0]},{&g_639[1][0][0],&g_639[1][0][0],&g_639[1][0][0],&g_639[1][0][0],&g_639[1][0][0]}},{{&g_639[0][0][0],&g_639[1][0][0],&g_639[0][0][0],&g_639[7][0][1],&g_639[1][0][0]},{&g_639[1][0][0],&l_1322,&l_1322,&g_639[1][0][0],&l_1322},{&g_639[1][0][0],&g_639[1][0][0],&l_1322,&g_639[1][0][0],&g_639[1][0][0]},{&l_1322,&g_639[1][0][0],&l_1322,&l_1322,&g_639[1][0][0]}}};
            int32_t l_1333 = (-2L);
            int32_t l_1336 = 0x2689B22FL;
            int32_t l_1339 = 3L;
            int32_t l_1345 = 0L;
            int32_t l_1349 = 0x8A3A475EL;
            int32_t l_1351 = 0L;
            int i, j, k;
            l_1306 = (((safe_div_func_float_f_f((&g_1178 != l_1289), (safe_mul_func_float_f_f((((*l_1293) = ((*g_857) <= l_1292)) != ((safe_add_func_float_f_f(p_20, (safe_sub_func_float_f_f((((0x8AFDF7F4L | p_20) , p_20) < ((safe_sub_func_float_f_f(((*l_1303) = (safe_mul_func_float_f_f((0x0.9p-1 != l_1302), p_20))), (-0x5.Fp-1))) >= g_1304)), p_20)))) >= l_1302)), p_20)))) > l_1302) != l_1305);
            (*g_578) &= (safe_mod_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(5UL, (((safe_add_func_uint16_t_u_u((((l_1313 == l_1315) & ((~((safe_lshift_func_uint8_t_u_u(l_1305, ((((((g_157 <= ((void*)0 == l_1319[5])) <= 0xD86559CBL) <= (((*l_1322) = (((*l_1320) = (~4294967295UL)) && 0x63L)) > p_20)) <= g_2) <= 0x7F9D9369L) != p_20))) ^ g_704)) | l_1323)) , p_20), l_1323)) | 0x0E1F4B02L) | l_1323))), p_20));
            for (g_74 = 0; (g_74 > 13); g_74++)
            { /* block id: 632 */
                int32_t *l_1328 = &g_42;
                int32_t *l_1329 = &g_86;
                int32_t *l_1330 = &g_42;
                int32_t *l_1331 = &l_1323;
                int32_t *l_1332[7][6] = {{&l_1323,&g_131[1][5][0],&g_2,&g_131[1][5][0],&l_1323,&l_1323},{&l_1323,&g_131[4][6][0],&g_131[1][5][0],&g_42,&g_86,&g_86},{&g_131[4][6][0],&l_1306,&l_1306,&g_131[4][6][0],&g_2,&g_86},{&g_42,&g_86,&g_131[1][5][0],&l_1323,&g_42,&l_1323},{&g_2,&g_96,&g_2,(void*)0,&g_42,&l_1323},{&g_131[1][5][0],&g_86,&g_42,&g_2,&g_2,&g_42},{&l_1306,&l_1306,&g_131[4][6][0],&g_2,&g_86,&g_131[1][5][0]}};
                uint8_t l_1352 = 0UL;
                int i, j;
                l_1327[1][1][2] = l_1326;
                l_1352++;
            }
            for (l_1339 = 20; (l_1339 <= (-19)); l_1339 = safe_sub_func_uint16_t_u_u(l_1339, 3))
            { /* block id: 638 */
                int32_t *l_1357 = &l_1338;
                int32_t *l_1358 = &l_1323;
                int32_t *l_1359 = (void*)0;
                int32_t *l_1360 = &l_1344[0][6];
                int32_t *l_1361 = &l_1334;
                int32_t *l_1362 = &l_1345;
                int32_t *l_1363 = &l_1333;
                int32_t *l_1364 = &g_42;
                int32_t *l_1365 = &l_1302;
                int32_t *l_1366 = &l_1306;
                int32_t *l_1367 = &l_1345;
                int32_t *l_1368 = &l_1345;
                int32_t *l_1369[2];
                int32_t l_1370 = (-4L);
                uint64_t l_1371 = 9UL;
                int64_t *l_1400 = (void*)0;
                int64_t *l_1401[5][6] = {{&g_129,&g_129,&g_129,&g_129,&g_129,&g_129},{&g_129,&g_129,&g_129,&g_129,&g_129,&g_129},{&g_129,&g_129,&g_129,&g_129,&g_129,&g_129},{&g_129,&g_129,&g_129,&g_129,&g_129,&g_129},{&g_129,&g_129,&g_129,&g_129,&g_129,&g_129}};
                uint64_t l_1407 = 0x50BC6B5C3D63D6EBLL;
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1369[i] = &l_1302;
                ++l_1371;
                for (g_704 = 0; (g_704 != 0); ++g_704)
                { /* block id: 642 */
                    int16_t l_1389 = 0xD275L;
                    int32_t **l_1396 = &l_1357;
                    float **l_1404 = &l_1293;
                    uint32_t *l_1405 = (void*)0;
                    uint32_t *l_1406[6][4];
                    int i, j;
                    for (i = 0; i < 6; i++)
                    {
                        for (j = 0; j < 4; j++)
                            l_1406[i][j] = &g_591;
                    }
                }
            }
        }
        else
        { /* block id: 653 */
            int32_t **l_1408 = &g_438;
            (*l_1408) = (**g_1179);
        }
    }
    for (g_993 = 0; (g_993 >= 11); ++g_993)
    { /* block id: 659 */
        int32_t * const ***l_1411[8][5][3] = {{{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,(void*)0,&g_1179},{(void*)0,(void*)0,&g_1179},{&g_1179,&g_1179,&g_1179}},{{(void*)0,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,(void*)0},{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179}},{{&g_1179,&g_1179,&g_1179},{(void*)0,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,(void*)0,&g_1179},{&g_1179,(void*)0,&g_1179}},{{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,(void*)0,(void*)0},{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179}},{{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{(void*)0,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179}},{{&g_1179,(void*)0,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,(void*)0,&g_1179},{(void*)0,(void*)0,&g_1179}},{{&g_1179,&g_1179,&g_1179},{(void*)0,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,(void*)0},{&g_1179,&g_1179,&g_1179}},{{&g_1179,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{(void*)0,&g_1179,&g_1179},{&g_1179,&g_1179,&g_1179},{&g_1179,(void*)0,&g_1179}}};
        int i, j, k;
        l_1412[6][3] = l_1411[4][1][1];
        (*g_1414) = g_1034;
    }
    l_1426[3] |= ((*g_578) &= ((safe_sub_func_uint8_t_u_u((l_1425 = ((p_20 || ((((*****g_518) && ((safe_lshift_func_uint8_t_u_u(((safe_mod_func_uint8_t_u_u(((void*)0 != (*g_149)), (65535UL || (l_1422 |= p_20)))) , (l_1423 = 0xA8L)), (6L > (p_20 ^ 5L)))) >= p_20)) != 4294967287UL) < (-2L))) ^ 8UL)), g_192)) , (*g_353)));
    return l_1427;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_404
 */
static int32_t * func_22(int32_t  p_23, int32_t * p_24, int32_t * p_25, int32_t * const  p_26)
{ /* block id: 155 */
    int64_t *l_411[3][7][10] = {{{&g_244[0][7][1],&g_157,(void*)0,&g_129,&g_129,&g_157,&g_244[0][7][5],&g_244[0][2][2],(void*)0,&g_244[0][5][4]},{&g_157,&g_244[0][7][1],(void*)0,&g_157,&g_129,(void*)0,&g_157,(void*)0,&g_244[0][2][6],&g_244[0][4][1]},{&g_244[0][7][1],&g_157,&g_157,&g_157,(void*)0,&g_244[0][7][5],(void*)0,&g_157,&g_157,&g_157},{&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_157,&g_244[0][1][4],&g_244[0][3][3],&g_244[0][7][1],&g_244[0][7][1],&g_129},{&g_244[0][7][1],(void*)0,&g_129,&g_244[0][7][1],&g_244[0][7][1],&g_129,&g_244[0][7][1],&g_244[0][7][1],(void*)0,&g_244[0][1][4]},{(void*)0,&g_244[0][3][3],&g_244[0][6][0],&g_244[0][7][1],&g_244[0][7][1],(void*)0,&g_157,(void*)0,&g_129,&g_129},{&g_129,&g_244[0][7][1],&g_129,&g_244[0][4][1],&g_244[0][4][1],&g_129,&g_244[0][7][1],&g_129,&g_244[0][1][4],(void*)0}},{{&g_244[0][2][6],&g_244[0][7][1],&g_157,&g_129,&g_244[0][3][3],&g_244[0][7][1],&g_244[0][1][4],&g_244[0][7][1],&g_244[0][6][0],&g_244[0][4][1]},{&g_244[0][1][4],&g_244[0][7][1],&g_157,&g_244[0][3][3],(void*)0,&g_244[0][7][1],(void*)0,&g_129,&g_244[0][2][6],&g_244[0][7][1]},{(void*)0,&g_157,&g_129,(void*)0,(void*)0,&g_157,&g_157,(void*)0,&g_157,&g_157},{&g_129,&g_157,&g_244[0][6][0],&g_157,&g_129,(void*)0,&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][2][2]},{&g_244[0][7][1],&g_129,&g_129,&g_244[0][7][1],(void*)0,(void*)0,&g_244[0][2][6],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][2][2]},{&g_157,&g_244[0][7][1],(void*)0,&g_129,&g_129,&g_244[0][7][5],&g_244[0][7][1],&g_157,&g_244[0][7][1],&g_157},{(void*)0,&g_244[0][7][1],&g_244[0][7][1],(void*)0,(void*)0,&g_129,&g_129,(void*)0,(void*)0,&g_244[0][7][1]}},{{&g_129,&g_129,&g_244[0][7][1],(void*)0,(void*)0,&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_129,&g_244[0][4][1]},{&g_244[0][7][1],(void*)0,&g_244[0][5][4],&g_157,&g_244[0][3][3],&g_129,&g_244[0][7][1],&g_244[0][6][0],&g_244[0][7][1],(void*)0},{&g_244[0][7][1],&g_129,(void*)0,&g_157,&g_244[0][4][1],&g_157,&g_129,&g_244[0][7][1],&g_157,&g_129},{&g_157,&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_244[0][7][1],&g_157,&g_244[0][1][4]},{&g_244[0][2][2],&g_244[0][7][1],&g_244[0][4][1],&g_244[0][7][1],&g_244[0][7][1],(void*)0,&g_244[0][2][6],&g_157,&g_129,&g_244[0][6][0]},{&g_244[0][7][1],&g_129,(void*)0,&g_244[0][7][1],(void*)0,(void*)0,&g_244[0][7][1],&g_129,&g_157,&g_244[0][2][6]},{&g_244[0][7][1],&g_157,&g_244[0][7][1],&g_244[0][7][1],&g_157,&g_244[0][7][1],&g_157,&g_157,&g_157,&g_157}}};
    int16_t *l_412 = (void*)0;
    int32_t ***l_416 = &g_65[0];
    int32_t ****l_415[3];
    int16_t *l_430 = (void*)0;
    int16_t *l_431 = &g_404;
    int32_t *l_432 = &g_96;
    int16_t l_555 = 1L;
    int64_t * const ***l_768 = (void*)0;
    uint32_t l_777 = 0xDB89BA57L;
    int8_t l_936 = 0x7BL;
    uint64_t *l_942 = &g_74;
    uint64_t **l_941 = &l_942;
    uint64_t ***l_1032[7];
    float l_1071[2][8][8] = {{{(-0x10.0p+1),0x2.3p-1,0x1.AC3E9Cp+6,0x9.Fp+1,0x0.Bp-1,0xE.723EA6p-47,(-0x1.5p-1),0x1.5p+1},{0xE.723EA6p-47,0x9.Fp+1,0xA.1446E7p+68,0x5.D08FE9p+80,(-0x1.9p+1),0x1.0p-1,0x6.5B4F57p+93,(-0x1.4p+1)},{(-0x1.9p+1),0x1.0p-1,0x6.5B4F57p+93,(-0x1.4p+1),0x1.Fp-1,0x6.5B4F57p+93,0x3.7E5B61p-37,0x5.D08FE9p+80},{(-0x10.0p+1),0x9.Fp+1,(-0x10.Ap+1),0x1.5p+1,0x3.B42807p-69,0x0.6p+1,0x9.EC349Ep+57,0x3.E11E79p+76},{0x0.Cp-1,0x2.0D8DB6p-60,(-0x1.9p+1),0xD.24D8AFp+74,0x0.1p-1,0x0.1p-1,0xD.24D8AFp+74,(-0x1.9p+1)},{0x0.6p-1,0x0.6p-1,0x0.1p-1,0x7.2p-1,0x4.74E623p-15,0x4.C60F12p-23,0x1.AC3E9Cp+6,0x9.EC349Ep+57},{0x9.EC349Ep+57,0x0.Ep-1,0x6.5B4F57p+93,0x1.Dp+1,0xB.C19942p+83,0xD.24D8AFp+74,0x0.Cp-1,0x9.EC349Ep+57},{0x0.Ep-1,0x1.5p+1,0x1.Ap+1,0x7.2p-1,0x6.0p+1,0xA.0A4A82p+4,(-0x10.Ap+1),(-0x1.9p+1)}},{{0x2.B8C80Ep+42,0x0.6p+1,0x0.Cp+1,0xD.24D8AFp+74,0x5.D08FE9p+80,0x1.5p+1,0x0.1p-1,0x3.E11E79p+76},{0xA.1446E7p+68,(-0x1.5p-1),0x1.AC3E9Cp+6,0x1.5p+1,0x7.2p-1,0x3.7E5B61p-37,0x8.A430E9p+11,0x5.D08FE9p+80},{0x1.5p+1,(-0x1.9p+1),0x0.Ep-1,(-0x1.5p-1),0x9.EC349Ep+57,(-0x9.4p+1),0x9.EC349Ep+57,(-0x1.5p-1)},{0x7.7BADC9p-91,0x1.Ap+1,0x7.7BADC9p-91,0x8.A430E9p+11,0x1.D3E62Ep+72,0x0.1p-1,0x6.5B4F57p+93,0x0.Cp-1},{(-0x5.6p+1),0x6.0p+1,0xB.C19942p+83,0x0.6p+1,0x4.74E623p-15,0x1.Fp-1,0x1.D3E62Ep+72,0x2.0D8DB6p-60},{(-0x5.6p+1),0x7.7BADC9p-91,0xD.24D8AFp+74,0x1.Dp+1,0x1.D3E62Ep+72,0x9.Ap+1,0x9.Fp+1,(-0x5.6p+1)},{0x7.7BADC9p-91,(-0x10.0p+1),0x1.Ap+1,0xA.1446E7p+68,0x9.EC349Ep+57,0x2.B8C80Ep+42,(-0x1.9p+1),0x9.Fp+1},{0x1.5p+1,0x0.6p+1,0x1.7p-1,0x9.Ap+1,0x7.2p-1,(-0x1.5p-1),0xB.C19942p+83,0x4.4BA8CCp+88}}};
    int64_t l_1129 = 1L;
    float l_1165 = (-0x3.9p+1);
    int32_t * const ***l_1181 = &g_1179;
    uint64_t l_1191 = 0x202CBD3074222358LL;
    uint32_t l_1221 = 18446744073709551615UL;
    int32_t *l_1284 = (void*)0;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_415[i] = &l_416;
    for (i = 0; i < 7; i++)
        l_1032[i] = &l_941;
    for (g_404 = 0; (g_404 >= 0); g_404 -= 1)
    { /* block id: 158 */
        int32_t *l_408 = &g_131[1][0][0];
        return l_408;
    }
    return l_1284;
}


/* ------------------------------------------ */
/* 
 * reads : g_74 g_370 g_404 g_407
 * writes: g_74 g_379 g_400 g_404 g_268
 */
static int32_t * func_27(int32_t * p_28, int32_t * const  p_29, uint8_t  p_30, uint32_t  p_31)
{ /* block id: 140 */
    int64_t *l_373 = &g_244[0][5][1];
    int64_t *l_378[6][1][7];
    int32_t l_386 = 0x0CD143E8L;
    int32_t l_391 = (-5L);
    int32_t *l_406[4];
    int i, j, k;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 7; k++)
                l_378[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 4; i++)
        l_406[i] = (void*)0;
    for (g_74 = 10; (g_74 < 51); g_74 = safe_add_func_int16_t_s_s(g_74, 1))
    { /* block id: 143 */
        int64_t **l_374 = (void*)0;
        int64_t **l_375 = &l_373;
        int64_t *l_377 = &g_129;
        int64_t **l_376[4];
        int32_t l_398[6];
        uint32_t *l_399 = &g_400;
        uint64_t *l_402[7] = {&g_74,&g_74,&g_74,&g_74,&g_74,&g_74,&g_74};
        uint64_t **l_401 = &l_402[2];
        int16_t *l_403[6][1][8] = {{{(void*)0,&g_404,&g_404,(void*)0,&g_404,&g_404,(void*)0,&g_404}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_404,(void*)0,&g_404,&g_404,(void*)0,&g_404,&g_404,(void*)0}},{{(void*)0,&g_404,&g_404,(void*)0,&g_404,&g_404,(void*)0,&g_404}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_404,(void*)0,&g_404,&g_404,(void*)0,&g_404,&g_404,(void*)0}}};
        int16_t *l_405 = &g_404;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_376[i] = &l_377;
        for (i = 0; i < 6; i++)
            l_398[i] = 0xA3F20EE1L;
        l_406[1] = func_34(func_34(func_34(p_28, p_28, ((*l_405) ^= ((((l_378[4][0][6] = ((*l_375) = l_373)) != (g_379 = &g_157)) >= 1L) == (safe_mul_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s((safe_mod_func_int64_t_s_s((l_386 = p_31), (0xB044E901C2B71EDBLL | ((safe_div_func_int64_t_s_s((((*l_401) = ((l_391 >= ((*l_399) = ((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(l_398[1], 0xAFL)), p_30)), p_31)) <= 1L))) , (void*)0)) != (void*)0), (-7L))) || g_370)))), 0xB9L)) != l_391), l_391))))), &l_391, g_74), p_28, l_391);
        (*g_407) = p_30;
    }
    return p_28;
}


/* ------------------------------------------ */
/* 
 * reads : g_43 g_42 g_2 g_74 g_96 g_86 g_105 g_111 g_131 g_149 g_157 g_5 g_160 g_192 g_129 g_244 g_150 g_352 g_353 g_368
 * writes: g_42 g_65 g_74 g_192 g_86 g_96 g_5 g_111 g_129 g_131 g_149 g_157 g_160 g_244 g_268 g_150 g_352
 */
static int32_t * func_32(int32_t * p_33)
{ /* block id: 5 */
    int8_t l_40 = 0xD6L;
    int32_t *l_54 = &g_42;
    int32_t *l_56 = &g_2;
    int32_t **l_55 = &l_56;
    int32_t **l_57 = (void*)0;
    int32_t *l_59 = &g_2;
    int32_t **l_58 = &l_59;
    uint64_t *l_82 = &g_74;
    int32_t *l_85 = &g_86;
    uint32_t l_89 = 18446744073709551611UL;
    uint32_t l_94 = 0xA18F1415L;
    int32_t *l_95 = &g_96;
    float l_126 = (-0x1.6p-1);
    float *l_134 = &l_126;
    float **l_133 = &l_134;
    int32_t l_158 = 0L;
    uint8_t *l_172 = &g_111;
    int32_t l_216 = 0xA3D0280CL;
    int32_t l_269[9][3][8] = {{{0xF6679340L,(-9L),1L,0L,0x68BDD198L,0x2F15F6EFL,0xFB2C8B33L,(-9L)},{(-9L),0x3B8D31ECL,7L,1L,0x37A7E7F2L,6L,0xA23D2193L,0xFB2C8B33L},{(-9L),0L,0x99F19B79L,0L,(-9L),(-1L),0L,(-1L)}},{{0xA23D2193L,0L,7L,(-5L),1L,(-1L),0x99F19B79L,0xACBED25AL},{0x0FBA2EC1L,0L,(-1L),1L,1L,0xE3DAEFC0L,1L,1L},{(-1L),0x2F15F6EFL,(-1L),0x05F66D64L,7L,0xECA4B766L,0xA3EB68EFL,0x96571B35L}},{{(-1L),(-1L),0x05F66D64L,0xACB78F74L,0x43FC71EBL,7L,7L,0x43FC71EBL},{(-1L),1L,0xFB2C8B33L,(-1L),7L,0xF6679340L,1L,0x68BDD198L},{(-1L),0L,0x22DD8A9AL,0xA3EB68EFL,1L,0x5CE4E5EDL,0xECA4B766L,0x2F15F6EFL}},{{0x0FBA2EC1L,0x5F64FC0DL,0xACBED25AL,0L,1L,0x22DD8A9AL,(-1L),8L},{0xACB78F74L,1L,1L,0x96571B35L,7L,0x5688E5A8L,0L,0L},{0xECA4B766L,0x78B85743L,1L,1L,0x78B85743L,0xECA4B766L,0x2A023E64L,7L}},{{7L,0x5CE4E5EDL,0xACBED25AL,1L,0x5F64FC0DL,0xDE6FF466L,0x68BDD198L,0x22DD8A9AL},{1L,0L,0xF6679340L,1L,0xACB78F74L,0x395141DAL,1L,7L},{(-1L),0xACB78F74L,0xC3DFB1DFL,1L,0x96571B35L,6L,7L,0L}},{{1L,0xA3EB68EFL,0x05F66D64L,0x96571B35L,0xDE6FF466L,0x05F66D64L,(-9L),8L},{0x78B85743L,1L,0x395141DAL,0L,0xACB78F74L,(-1L),1L,0x2F15F6EFL},{0xACBED25AL,0x68BDD198L,0x43FC71EBL,0xA3EB68EFL,0L,0xA3EB68EFL,0x43FC71EBL,0x68BDD198L}},{{7L,8L,0x5CE4E5EDL,(-1L),(-1L),0x05F66D64L,0xACB78F74L,0x43FC71EBL},{0x68BDD198L,(-9L),0xE3DAEFC0L,0xACB78F74L,7L,(-5L),0xACB78F74L,0x96571B35L},{0x22DD8A9AL,0xACB78F74L,0x5CE4E5EDL,0x05F66D64L,0x0FBA2EC1L,(-1L),0x43FC71EBL,1L}},{{0x0FBA2EC1L,(-1L),0x43FC71EBL,1L,0xC3DFB1DFL,0xDE6FF466L,1L,0xACBED25AL},{0L,0x2F15F6EFL,0x395141DAL,(-1L),7L,0x3FE88EDFL,(-9L),0x96571B35L},{0x2A023E64L,0x78B85743L,0x05F66D64L,0x22DD8A9AL,0x99F19B79L,7L,7L,0x99F19B79L}},{{(-1L),0xC3DFB1DFL,0xC3DFB1DFL,(-1L),7L,0x22DD8A9AL,1L,0xECA4B766L},{0L,0L,0xF6679340L,(-9L),1L,0L,0x68BDD198L,0x2F15F6EFL},{1L,0L,0xACBED25AL,0x5F64FC0DL,0x0FBA2EC1L,0x22DD8A9AL,0x2A023E64L,1L}}};
    int32_t l_329 = 0xA63A098AL;
    int16_t l_337 = 8L;
    int i, j, k;
lbl_97:
    (*g_43) = l_40;
    (*l_54) = func_44(g_42, (safe_mod_func_uint64_t_u_u(g_42, func_50(l_54, ((*l_58) = ((*l_55) = p_33)), func_34(func_34(p_33, &g_2, g_42), &g_2, (*l_54))))), p_33);
    if (((*l_95) &= (0x59L ^ (safe_sub_func_uint16_t_u_u((((safe_add_func_int16_t_s_s(((((*l_82) = (*l_54)) >= (safe_mod_func_uint32_t_u_u((g_2 >= ((((((*l_85) = (*l_54)) > (((((((safe_sub_func_int16_t_s_s(l_89, g_2)) , g_42) > (((((safe_div_func_int64_t_s_s((safe_add_func_uint8_t_u_u(((l_82 == (g_2 , (void*)0)) < 1UL), 0x9EL)), g_2)) | g_2) > (*l_54)) , 1UL) > (-1L))) , g_42) > (-1L)) & g_2) ^ l_94)) ^ 0x05B8L) > 1L) ^ (-7L))), g_42))) ^ 0x6C5FL), (*l_54))) || g_2) <= g_42), g_2)))))
    { /* block id: 22 */
        int32_t *l_103 = &g_96;
        if (g_96)
            goto lbl_97;
        for (g_42 = (-25); (g_42 == (-2)); ++g_42)
        { /* block id: 26 */
            uint32_t l_100 = 18446744073709551613UL;
            l_100 &= ((*l_95) ^= ((void*)0 != &l_54));
        }
        (*g_105) = ((((safe_lshift_func_int16_t_s_u((g_42 <= (((*l_85) , func_34(((*l_55) = p_33), func_34(p_33, l_103, (*l_103)), g_2)) != p_33)), g_42)) != (*l_103)) , g_74) > (*l_103));
    }
    else
    { /* block id: 32 */
        uint8_t *l_110 = &g_111;
        int32_t l_112 = 0x14B5F725L;
        int8_t *l_125 = &l_40;
        int8_t l_127 = 1L;
        int8_t *l_128[3][6] = {{&l_127,&l_127,&l_127,&l_127,&l_127,&l_127},{&l_127,&l_127,&l_127,&l_127,&l_127,&l_127},{&l_127,&l_127,&l_127,&l_127,&l_127,&l_127}};
        int32_t *l_132 = &g_131[5][2][0];
        int64_t *l_153 = &g_129;
        int64_t *l_154 = (void*)0;
        int64_t *l_155 = (void*)0;
        int64_t *l_156 = &g_157;
        uint16_t *l_159 = &g_160;
        float *l_161 = &g_5;
        int32_t l_173 = 0L;
        int8_t l_174 = 0L;
        int32_t l_266 = 0x69C0D2D9L;
        int32_t l_273[9][3][5] = {{{0xDC2EABF8L,(-3L),0L,0xDC2EABF8L,1L},{2L,0xDC2EABF8L,7L,0xDC2EABF8L,2L},{0L,0x7074C7D2L,(-3L),1L,0x7074C7D2L}},{{2L,(-3L),(-3L),2L,1L},{0xDC2EABF8L,2L,7L,0x7074C7D2L,0x7074C7D2L},{0L,2L,0L,1L,2L}},{{0x7074C7D2L,(-3L),1L,0x7074C7D2L,1L},{0x7074C7D2L,0x7074C7D2L,7L,2L,0xDC2EABF8L},{0L,0xDC2EABF8L,1L,1L,0xDC2EABF8L}},{{0xDC2EABF8L,(-3L),0L,0xDC2EABF8L,1L},{2L,0xDC2EABF8L,7L,0xDC2EABF8L,2L},{0L,0x7074C7D2L,(-3L),1L,0x7074C7D2L}},{{2L,(-3L),(-3L),2L,1L},{0xDC2EABF8L,2L,7L,0x7074C7D2L,0x7074C7D2L},{0L,2L,0L,1L,2L}},{{0x7074C7D2L,(-3L),1L,0x7074C7D2L,1L},{0x7074C7D2L,0x7074C7D2L,7L,2L,0xDC2EABF8L},{0L,0xDC2EABF8L,1L,1L,0xDC2EABF8L}},{{0xDC2EABF8L,(-3L),0L,0xDC2EABF8L,1L},{2L,0xDC2EABF8L,7L,0xDC2EABF8L,2L},{0L,0x7074C7D2L,(-3L),1L,0x7074C7D2L}},{{2L,(-3L),(-3L),2L,1L},{0xDC2EABF8L,2L,7L,0x7074C7D2L,0x7074C7D2L},{0L,2L,0L,1L,2L}},{{0x7074C7D2L,(-3L),1L,0x7074C7D2L,1L},{0x7074C7D2L,0x7074C7D2L,7L,2L,0xDC2EABF8L},{0L,0xDC2EABF8L,1L,1L,0xDC2EABF8L}}};
        uint32_t l_274 = 0x9BF3D29AL;
        const int64_t l_305 = 0x39BA7F623BDBF143LL;
        int i, j, k;
        (*l_132) |= ((((*l_85) = (((safe_rshift_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u((g_2 , ((*l_110) &= (*l_85))), 3)), 0)) == l_112) <= g_2)) | (g_42 > (g_129 = (safe_mul_func_int8_t_s_s(((safe_add_func_uint16_t_u_u(((safe_div_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((*l_125) = (((void*)0 != l_85) || (((((l_112 != (safe_add_func_uint8_t_u_u((((-1L) == (*l_95)) == l_112), l_112))) != g_96) < 2UL) <= 0xBC1BBF8DAC292C6CLL) >= g_96))), l_112)), (*l_95))), g_74)) == l_112), l_127)) , g_74), g_96))))) < 0UL);
        l_133 = (void*)0;
lbl_247:
        (*l_161) = ((safe_sub_func_float_f_f((safe_add_func_float_f_f((((*l_134) = (((safe_mul_func_float_f_f(((safe_add_func_int32_t_s_s(((((*l_159) = (safe_mod_func_uint8_t_u_u((((((safe_add_func_uint32_t_u_u((((*l_156) ^= (safe_mod_func_int32_t_s_s((*l_132), (((*l_95) >= (((*l_54) = 0x75B0AE68L) , ((g_149 = g_149) != (void*)0))) || ((*l_153) = (safe_add_func_uint8_t_u_u(((((*l_110) = (((*l_132) >= (*l_95)) > (((l_82 = &g_74) != ((((*l_132) , (*l_85)) != (*l_132)) , (void*)0)) <= 0xF5B55EC714A41B78LL))) | (*l_132)) < (*l_132)), g_74))))))) || (*l_132)), l_158)) || g_74) >= g_131[0][0][0]) | (-7L)) , g_96), 6L))) & (*l_132)) & (*l_132)), (*l_132))) , (*g_105)), 0x2.D848D3p-52)) == 0x0.4p-1) == (*l_132))) <= (*l_132)), 0x0.Bp-1)), (*l_132))) <= (-0x5.Cp-1));
        if (((safe_sub_func_int16_t_s_s(((((safe_add_func_int32_t_s_s((((-1L) < (safe_mul_func_int16_t_s_s((((safe_add_func_uint32_t_u_u(((void*)0 != l_82), (((safe_sub_func_int8_t_s_s((((((l_172 = (((void*)0 != &l_134) , l_110)) == &g_111) == ((void*)0 == p_33)) <= (-1L)) == 0xDBC7L), 0x10L)) , l_155) != &g_74))) && l_173) > (*l_85)), 65535UL))) <= (*l_95)), l_174)) | (*l_132)) , &g_150[3]) != (void*)0), g_160)) && 0x8346BC9B9D56F053LL))
        { /* block id: 49 */
            float **l_187 = &g_150[2];
            int32_t l_189[1][1];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                    l_189[i][j] = 0L;
            }
            (*l_85) = (((*l_172)++) & ((safe_lshift_func_int8_t_s_s((safe_div_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_u(((void*)0 != &l_40), ((void*)0 == l_187))) > ((!0x2300B1250D9DD068LL) & (l_189[0][0] <= ((l_189[0][0] | (safe_rshift_func_int16_t_s_s(((((*l_58) = (*l_55)) == (void*)0) > 0xFCF5082C358F4601LL), l_189[0][0]))) != 18446744073709551610UL)))), g_192)), l_189[0][0])) & g_131[4][4][0]), g_131[5][2][0])), 4)) | g_42));
            (*l_55) = &l_189[0][0];
            if (g_192)
                goto lbl_97;
        }
        else
        { /* block id: 55 */
            uint8_t l_205[5][6] = {{0x5CL,3UL,0xC9L,0xBCL,0xC9L,3UL},{0x5CL,3UL,0xC9L,0xBCL,0xC9L,3UL},{0x5CL,3UL,0xC9L,0xBCL,0xC9L,3UL},{0x5CL,3UL,0xC9L,0xBCL,0xC9L,3UL},{0x5CL,3UL,0xC9L,0xBCL,0xC9L,3UL}};
            float *l_245 = &l_126;
            int32_t l_246[6];
            int32_t * const *l_326 = &l_95;
            int32_t * const **l_325 = &l_326;
            int16_t *l_365 = &l_337;
            int i, j;
            for (i = 0; i < 6; i++)
                l_246[i] = 0xEA7DD988L;
lbl_290:
            (*l_85) |= ((*l_95) = (0x58L >= (((safe_sub_func_uint32_t_u_u(((*l_132) , (4294967292UL < (safe_div_func_int32_t_s_s((safe_lshift_func_int16_t_s_s((((*l_132) | (g_111 = (g_129 == (((safe_mul_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((l_205[3][0] & ((safe_mul_func_int8_t_s_s(l_205[3][2], (safe_div_func_int64_t_s_s(g_131[0][1][0], l_205[3][0])))) , (*l_132))), 0)), (*l_132))) < 0xD8C64F5160438E2FLL), l_205[4][4])) , g_131[3][4][0]) && 1UL)))) & 0x8821C202L), 7)), 0x52147F1CL)))), (*l_132))) || 1UL) == g_160)));
lbl_313:
            for (g_157 = 0; g_157 < 1; g_157 += 1)
            {
                g_65[g_157] = &g_66;
            }
            if ((safe_lshift_func_int8_t_s_u(g_86, 6)))
            { /* block id: 60 */
                uint32_t l_217 = 0x79B65F2BL;
                int32_t l_270 = (-1L);
                int32_t l_271 = (-1L);
                int32_t l_272[9][4] = {{1L,0L,0x3864813AL,0x80EDB1E2L},{1L,9L,5L,1L},{0x3864813AL,1L,1L,1L},{5L,5L,1L,9L},{0x80EDB1E2L,1L,5L,0L},{1L,0L,1L,5L},{1L,0L,1L,0L},{0L,1L,0xC443F89AL,9L},{9L,5L,1L,1L}};
                int i, j;
                for (g_111 = 1; (g_111 != 34); g_111 = safe_add_func_int16_t_s_s(g_111, 1))
                { /* block id: 63 */
                    int32_t *l_214 = &g_131[0][2][0];
                    int32_t *l_215[8][3][7] = {{{&g_131[5][2][0],(void*)0,&l_112,&g_2,&g_2,&g_131[5][2][0],&g_131[5][2][0]},{&g_131[5][2][0],&g_131[5][2][0],&g_42,&g_131[6][0][0],&g_131[5][2][0],&g_131[5][2][0],(void*)0},{&g_131[5][2][0],&g_86,&g_42,(void*)0,&g_42,&g_86,&g_131[5][2][0]}},{{&g_131[5][2][0],&g_42,&g_131[5][2][0],&g_131[5][2][0],&g_86,&g_42,&g_42},{&g_131[5][2][0],&g_2,&g_131[6][0][0],(void*)0,&g_131[5][2][0],&g_2,&g_2},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_2,&g_86,&g_131[5][2][0],&g_86}},{{&g_131[6][0][0],&g_42,&g_42,&g_131[6][0][0],&g_86,&g_131[6][5][0],&g_131[5][2][0]},{&g_2,&g_86,&g_42,&l_112,&g_131[5][2][0],&l_112,&g_131[5][2][0]},{&l_112,&g_131[5][2][0],&l_112,&g_42,&g_86,&g_2,&g_131[5][2][0]}},{{&g_131[6][5][0],&g_86,&g_131[6][0][0],&g_42,&g_42,&g_131[6][0][0],&g_86},{&g_131[5][2][0],&g_86,&g_2,&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_2},{&g_2,&g_131[5][2][0],(void*)0,&g_131[6][0][0],&g_2,&g_131[5][2][0],&g_42}},{{&g_42,&g_86,&g_131[5][2][0],&g_131[5][2][0],&g_42,&g_131[5][2][0],&g_131[5][2][0]},{&g_86,&g_42,(void*)0,&g_42,&g_86,&g_131[5][2][0],(void*)0},{&g_131[5][2][0],&g_131[5][2][0],&g_131[6][0][0],&g_42,&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]}},{{&g_131[5][2][0],&g_2,&g_2,&l_112,(void*)0,&g_131[5][2][0],&g_96},{&g_131[5][2][0],&g_42,&g_131[5][2][0],&g_131[6][0][0],&g_96,&g_131[6][0][0],&g_131[5][2][0]},{&g_86,&g_86,&g_131[5][2][0],&g_2,(void*)0,&g_2,&g_131[5][2][0]}},{{&g_42,&g_131[5][2][0],&g_2,(void*)0,&g_86,&l_112,&g_42},{&g_2,(void*)0,&g_131[6][0][0],&g_131[5][2][0],(void*)0,&g_131[6][5][0],(void*)0},{&g_131[5][2][0],&g_96,(void*)0,(void*)0,&g_96,&g_131[5][2][0],(void*)0}},{{&g_131[6][5][0],(void*)0,&g_131[5][2][0],&g_131[6][0][0],(void*)0,&g_2,&g_42},{&l_112,&g_86,(void*)0,&g_2,&g_131[5][2][0],&g_42,&g_131[5][2][0]},{&g_2,(void*)0,&g_2,&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]}}};
                    int32_t l_242 = (-3L);
                    int i, j, k;
                    --l_217;
                    for (g_74 = 0; (g_74 <= 2); g_74 += 1)
                    { /* block id: 67 */
                        uint32_t l_231 = 0UL;
                        int64_t *l_243 = &g_244[0][7][1];
                        int32_t l_265[4];
                        float *l_267 = &g_268;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_265[i] = 0x2C787117L;
                        l_246[0] = (((l_245 = func_34(p_33, (((((*l_243) &= (g_157 = ((*l_153) &= (safe_rshift_func_uint16_t_u_u((((((safe_mul_func_int16_t_s_s(((safe_div_func_int64_t_s_s((safe_unary_minus_func_int16_t_s(0x7021L)), (0x9EE913CB2BF3D79FLL & ((0xBBCF4FF7L == (((safe_div_func_uint8_t_u_u(0x59L, l_231)) == (safe_div_func_uint16_t_u_u((safe_div_func_int64_t_s_s((0xFF738745L >= (((0x48L ^ (l_242 ^= (safe_add_func_int8_t_s_s((safe_sub_func_int64_t_s_s((safe_add_func_int16_t_s_s(g_74, g_131[5][2][0])), l_205[3][0])), g_192)))) >= 4294967288UL) , 4294967295UL)), l_217)), l_205[4][1]))) == g_131[5][6][0])) , 6L)))) & g_131[3][1][0]), g_131[5][2][0])) == l_231) || (*l_132)) || (*l_132)) & (*l_214)), g_131[0][3][0]))))) > (-1L)) <= l_231) , p_33), l_205[3][0])) == p_33) & l_217);
                        if (l_174)
                            goto lbl_247;
                        (*l_214) = ((*l_132) , (((*l_267) = (+(((*l_161) = (l_231 , ((*g_105) != ((*l_134) = ((3UL && (safe_mod_func_int16_t_s_s((safe_lshift_func_int16_t_s_s(((((safe_lshift_func_uint16_t_u_s(((safe_lshift_func_uint16_t_u_u(((((((safe_rshift_func_uint16_t_u_u(((((safe_mod_func_int8_t_s_s(l_217, g_131[5][2][0])) | ((((1UL | (safe_lshift_func_int16_t_s_s(l_265[0], 10))) , &g_244[0][7][1]) == &g_244[0][7][1]) >= g_42)) == g_74) | (-1L)), l_265[2])) || g_244[0][7][1]) == g_42) ^ l_246[0]) | l_265[3]) , l_231), 3)) <= 4294967294UL), 10)) < l_266) ^ 0x4E12CC2AL) < g_157), 13)), 4UL))) , (*l_132)))))) > l_265[0]))) , (-1L)));
                        ++l_274;
                    }
                }
            }
            else
            { /* block id: 82 */
                const int64_t *l_282 = &g_244[0][6][0];
                int64_t *l_284 = (void*)0;
                float *l_291 = &g_268;
                float *l_293 = &l_126;
                int32_t l_303[9] = {2L,0xC61A893BL,2L,0xC61A893BL,2L,0xC61A893BL,2L,0xC61A893BL,2L};
                uint64_t l_306[8][4][4] = {{{0x867A1ABE66CE93E1LL,0xCA220BD3261207DELL,0xA8E6D828BD182D7CLL,0x18D7B6EF05C6FA70LL},{4UL,0x7A95DDFB3247F7E0LL,0xCA220BD3261207DELL,4UL},{0x867A1ABE66CE93E1LL,0x7A95DDFB3247F7E0LL,0x7A445A750B496DEBLL,0x18D7B6EF05C6FA70LL},{0x18D7B6EF05C6FA70LL,0xCA220BD3261207DELL,0xCA220BD3261207DELL,0x18D7B6EF05C6FA70LL}},{{18446744073709551611UL,0x7A95DDFB3247F7E0LL,0xA8E6D828BD182D7CLL,4UL},{0x18D7B6EF05C6FA70LL,0x7A95DDFB3247F7E0LL,0x1A288EE34ADD1B52LL,0x18D7B6EF05C6FA70LL},{0x867A1ABE66CE93E1LL,0xCA220BD3261207DELL,0xA8E6D828BD182D7CLL,0x18D7B6EF05C6FA70LL},{4UL,0x7A95DDFB3247F7E0LL,0xCA220BD3261207DELL,4UL}},{{0x867A1ABE66CE93E1LL,0x7A95DDFB3247F7E0LL,0x7A445A750B496DEBLL,0x18D7B6EF05C6FA70LL},{0x18D7B6EF05C6FA70LL,0xCA220BD3261207DELL,0xCA220BD3261207DELL,0x18D7B6EF05C6FA70LL},{18446744073709551611UL,0x7A95DDFB3247F7E0LL,0xA8E6D828BD182D7CLL,4UL},{0x18D7B6EF05C6FA70LL,0x7A95DDFB3247F7E0LL,0x1A288EE34ADD1B52LL,0x18D7B6EF05C6FA70LL}},{{0x867A1ABE66CE93E1LL,0xCA220BD3261207DELL,0xA8E6D828BD182D7CLL,0x18D7B6EF05C6FA70LL},{4UL,0x7A95DDFB3247F7E0LL,0xCA220BD3261207DELL,4UL},{0x867A1ABE66CE93E1LL,0x7A95DDFB3247F7E0LL,0x7A445A750B496DEBLL,0x18D7B6EF05C6FA70LL},{0x18D7B6EF05C6FA70LL,0xCA220BD3261207DELL,0xCA220BD3261207DELL,0x18D7B6EF05C6FA70LL}},{{18446744073709551611UL,0x7A95DDFB3247F7E0LL,0xA8E6D828BD182D7CLL,4UL},{0x18D7B6EF05C6FA70LL,0x7A95DDFB3247F7E0LL,0x1A288EE34ADD1B52LL,0x18D7B6EF05C6FA70LL},{0x867A1ABE66CE93E1LL,0xCA220BD3261207DELL,0xA8E6D828BD182D7CLL,0x18D7B6EF05C6FA70LL},{4UL,0x7A95DDFB3247F7E0LL,0xCA220BD3261207DELL,4UL}},{{0x867A1ABE66CE93E1LL,0x7A95DDFB3247F7E0LL,0x7A445A750B496DEBLL,0x18D7B6EF05C6FA70LL},{0x18D7B6EF05C6FA70LL,0xCA220BD3261207DELL,0xCA220BD3261207DELL,0x18D7B6EF05C6FA70LL},{18446744073709551611UL,0x7A95DDFB3247F7E0LL,0xA8E6D828BD182D7CLL,4UL},{0x18D7B6EF05C6FA70LL,0x7A95DDFB3247F7E0LL,0x1A288EE34ADD1B52LL,0x18D7B6EF05C6FA70LL}},{{0x867A1ABE66CE93E1LL,0xCA220BD3261207DELL,0xA8E6D828BD182D7CLL,0x18D7B6EF05C6FA70LL},{4UL,0x7A95DDFB3247F7E0LL,0xCA220BD3261207DELL,4UL},{0x867A1ABE66CE93E1LL,0x7A95DDFB3247F7E0LL,0x7A445A750B496DEBLL,0x18D7B6EF05C6FA70LL},{0x18D7B6EF05C6FA70LL,0xCA220BD3261207DELL,0xCA220BD3261207DELL,0x18D7B6EF05C6FA70LL}},{{18446744073709551611UL,0x7A95DDFB3247F7E0LL,0xA8E6D828BD182D7CLL,4UL},{0x18D7B6EF05C6FA70LL,0x7A95DDFB3247F7E0LL,0x1A288EE34ADD1B52LL,0x18D7B6EF05C6FA70LL},{0x867A1ABE66CE93E1LL,0xCA220BD3261207DELL,0xA8E6D828BD182D7CLL,0x18D7B6EF05C6FA70LL},{4UL,0x7A95DDFB3247F7E0LL,0xCA220BD3261207DELL,4UL}}};
                int32_t l_358 = 0x09D3251BL;
                int i, j, k;
                for (l_40 = 0; (l_40 <= 2); l_40 += 1)
                { /* block id: 85 */
                    uint64_t **l_277 = &l_82;
                    uint64_t *l_279 = &g_192;
                    uint64_t **l_278 = &l_279;
                    const int64_t **l_283 = &l_282;
                    (*l_85) = (0xD798B91969292E41LL < (((*l_278) = ((*l_277) = &g_192)) != (void*)0));
                    (*l_85) = ((*l_54) = ((4294967289UL | ((((((&g_160 != &g_160) , (void*)0) == (void*)0) < (--(*l_159))) ^ (((*l_283) = l_282) == l_284)) & (*l_132))) != ((!4294967295UL) > 0xE2BB5F5BL)));
                    for (g_74 = 0; (g_74 <= 2); g_74 += 1)
                    { /* block id: 95 */
                        if (l_205[0][2])
                            break;
                    }
                }
                for (l_94 = 11; (l_94 < 37); ++l_94)
                { /* block id: 101 */
                    float **l_292[9][2][6] = {{{(void*)0,&l_161,&l_245,&l_291,(void*)0,&l_134},{(void*)0,&l_291,&l_245,(void*)0,&l_245,&l_291}},{{&l_161,&l_291,&l_291,&l_245,&l_245,&l_245},{&l_161,&l_291,&l_245,(void*)0,&l_134,&l_161}},{{&l_134,&l_291,&l_161,&l_245,&l_245,(void*)0},{&l_291,&l_291,&l_161,&l_291,&l_245,&l_245}},{{&l_245,&l_291,(void*)0,&l_291,(void*)0,&l_245},{&l_134,&l_161,&l_245,&l_245,&l_161,&l_245}},{{&l_291,&l_245,&l_291,&l_245,&l_161,&l_245},{&l_161,&l_245,&l_161,&l_245,&l_134,(void*)0}},{{(void*)0,(void*)0,&l_291,&l_245,&l_161,(void*)0},{(void*)0,&l_291,&l_245,&l_245,&l_291,(void*)0}},{{&l_161,&l_134,(void*)0,&l_245,&l_291,&l_245},{&l_245,&l_245,&l_245,&l_245,(void*)0,&l_134}},{{&l_245,&l_134,&l_161,&l_245,&l_245,&l_291},{&l_245,&l_291,&l_245,&l_291,&l_245,(void*)0}},{{&l_245,(void*)0,&l_134,&l_161,&l_245,&l_161},{&l_291,&l_291,&l_245,(void*)0,(void*)0,&l_161}}};
                    int32_t l_302 = (-2L);
                    int32_t ***l_328 = &l_55;
                    const int32_t ***l_354 = &g_352;
                    int16_t *l_355[7];
                    uint32_t *l_357 = &l_274;
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_355[i] = (void*)0;
                    for (g_192 = 0; (g_192 < 27); g_192++)
                    { /* block id: 104 */
                        if (g_192)
                            goto lbl_290;
                        return p_33;
                    }
                    (*l_95) &= (((*g_149) = l_291) == (l_293 = l_245));
                    if ((safe_add_func_int64_t_s_s((((-10L) != (*l_54)) >= ((((safe_mod_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((0UL <= (safe_sub_func_int8_t_s_s(((((*g_149) != ((l_302 ^ ((l_302 <= (l_303[7] >= ((+255UL) , g_2))) , l_303[1])) , (void*)0)) | 9UL) < 1L), l_305))), g_96)), 0x90L)) <= l_303[4]) | 0x4B9D9CDEL) > l_306[0][1][0])), g_42)))
                    { /* block id: 111 */
                        return p_33;
                    }
                    else
                    { /* block id: 113 */
                        int32_t *l_307 = &g_131[3][0][0];
                        int32_t *l_308[5][8][5] = {{{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]}},{{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]}},{{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]}},{{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]}},{{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]},{&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0],&g_131[5][2][0]},{&l_246[5],&l_246[5],&l_246[5],&l_246[5],&l_246[5]}}};
                        uint64_t l_309 = 0xD4E75E4C8BF5B44CLL;
                        uint64_t l_312 = 0x1093485B00E19483LL;
                        int32_t * const ***l_327 = &l_325;
                        int8_t l_330 = (-1L);
                        int i, j, k;
                        ++l_309;
                        if (l_312)
                            continue;
                        if (l_274)
                            goto lbl_313;
                        (*l_54) |= (safe_rshift_func_int16_t_s_s((l_205[3][2] < (((0UL ^ ((*l_159) = ((1UL > (((*l_125) |= (+(*l_132))) , (safe_sub_func_int8_t_s_s(4L, ((g_86 , ((*l_132) = (((safe_rshift_func_uint16_t_u_s((((0x2FBB3DB7L || (((safe_mod_func_int8_t_s_s(((*l_125) = (safe_mul_func_uint16_t_u_u((((((((*l_327) = l_325) != ((l_306[0][1][0] , l_306[0][1][0]) , l_328)) , (**l_326)) <= (**l_326)) , &p_33) == (void*)0), 0x03A5L))), (*l_132))) != g_2) | l_329)) | l_330) != 18446744073709551611UL), 3)) >= g_74) || 65534UL))) < l_303[1]))))) , (*l_132)))) & g_86) < l_303[7])), 8));
                    }
                    l_358 ^= (((*l_357) &= (((void*)0 == &l_245) , (safe_sub_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(((l_337 == (safe_sub_func_int64_t_s_s((((safe_rshift_func_int16_t_s_s(((safe_div_func_uint64_t_u_u((((safe_mul_func_int8_t_s_s(g_244[0][1][2], (***l_325))) && ((*l_156) = (((safe_div_func_int16_t_s_s(l_306[0][1][0], g_244[0][7][1])) | ((*l_54) |= ((((***l_325) , (((safe_mod_func_int16_t_s_s(((*l_132) = (((((*l_82)++) >= ((((**l_326) = (((((*l_354) = g_352) == (void*)0) , (***l_354)) < (*l_95))) , (void*)0) == (void*)0)) | (*l_132)) || l_306[5][1][3])), l_306[0][2][2])) , l_306[0][1][0]) != (-1L))) > l_303[7]) , 0xA7D5AD24L))) || (***l_354)))) , l_306[0][1][0]), l_266)) , g_96), 14)) < g_111) || (**l_326)), g_129))) , 0x7182D5CAL), 1L)), 6)), 65535UL)))) == 0xF418D5C4L);
                }
            }
            (*l_85) = (0UL >= (0xAFL & (((safe_div_func_int8_t_s_s(((safe_lshift_func_int16_t_s_s((((safe_div_func_int32_t_s_s((((*l_365) = (*l_132)) >= (*l_132)), (((*l_132) & ((safe_rshift_func_uint16_t_u_s(0x8511L, ((***l_325) = (*l_132)))) , ((g_368 == (void*)0) < 0L))) , (-8L)))) , 0x114C46B2L) >= (**g_352)), 10)) ^ 18446744073709551610UL), 0xA4L)) && (**l_326)) , g_96)));
        }
    }
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_34(int32_t * p_35, int32_t * p_36, const int16_t  p_37)
{ /* block id: 3 */
    int32_t *l_39 = (void*)0;
    return l_39;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_44(int16_t  p_45, const int16_t  p_46, int32_t * p_47)
{ /* block id: 16 */
    uint64_t l_77 = 0x6D4D43A62FBF9108LL;
    return l_77;
}


/* ------------------------------------------ */
/* 
 * reads : g_42 g_2 g_74
 * writes: g_65 g_74
 */
static int64_t  func_50(int32_t * p_51, int32_t * p_52, int32_t * const  p_53)
{ /* block id: 9 */
    int32_t *l_64 = &g_42;
    int32_t **l_63 = &l_64;
    int32_t ***l_62[1];
    int8_t l_72 = 0x01L;
    uint8_t l_73 = 0xF6L;
    int i;
    for (i = 0; i < 1; i++)
        l_62[i] = &l_63;
    l_73 |= (((g_65[0] = &p_52) != &p_53) < (((((&l_63 == (((*l_64) || (l_72 = ((!(((*p_51) || ((((void*)0 != &l_63) & (*p_51)) , (safe_rshift_func_int16_t_s_u((((safe_div_func_uint32_t_u_u(((((*l_63) = p_51) != (void*)0) >= g_42), g_2)) , (*l_64)) != g_42), g_42)))) & 1UL)) > (-1L)))) , &l_63)) < 0xAAD9L) , 0L) == 0xD72D30A9L) <= g_2));
    g_74++;
    return (*l_64);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc_bytes (&g_5, sizeof(g_5), "g_5", print_hash_value);
    transparent_crc(g_42, "g_42", print_hash_value);
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_86, "g_86", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_131[i][j][k], "g_131[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_157, "g_157", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    transparent_crc(g_192, "g_192", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_244[i][j][k], "g_244[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_268, sizeof(g_268), "g_268", print_hash_value);
    transparent_crc_bytes (&g_356, sizeof(g_356), "g_356", print_hash_value);
    transparent_crc(g_370, "g_370", print_hash_value);
    transparent_crc(g_400, "g_400", print_hash_value);
    transparent_crc(g_404, "g_404", print_hash_value);
    transparent_crc(g_472, "g_472", print_hash_value);
    transparent_crc(g_514, "g_514", print_hash_value);
    transparent_crc(g_591, "g_591", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_664[i][j][k], "g_664[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_704, "g_704", print_hash_value);
    transparent_crc(g_852, "g_852", print_hash_value);
    transparent_crc(g_993, "g_993", print_hash_value);
    transparent_crc(g_1002, "g_1002", print_hash_value);
    transparent_crc(g_1304, "g_1304", print_hash_value);
    transparent_crc(g_1512, "g_1512", print_hash_value);
    transparent_crc(g_1592, "g_1592", print_hash_value);
    transparent_crc(g_1652, "g_1652", print_hash_value);
    transparent_crc(g_1702, "g_1702", print_hash_value);
    transparent_crc(g_1777, "g_1777", print_hash_value);
    transparent_crc(g_2109, "g_2109", print_hash_value);
    transparent_crc_bytes (&g_2133, sizeof(g_2133), "g_2133", print_hash_value);
    transparent_crc(g_2203, "g_2203", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2316[i], "g_2316[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_2354, sizeof(g_2354), "g_2354", print_hash_value);
    transparent_crc(g_2384, "g_2384", print_hash_value);
    transparent_crc(g_2499, "g_2499", print_hash_value);
    transparent_crc(g_2568, "g_2568", print_hash_value);
    transparent_crc(g_2629, "g_2629", print_hash_value);
    transparent_crc(g_2810, "g_2810", print_hash_value);
    transparent_crc(g_2875, "g_2875", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2928[i], "g_2928[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 788
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 161
   depth: 2, occurrence: 42
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 18, occurrence: 1
   depth: 20, occurrence: 6
   depth: 21, occurrence: 3
   depth: 22, occurrence: 1
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 34, occurrence: 3
   depth: 36, occurrence: 2
   depth: 37, occurrence: 1
   depth: 41, occurrence: 1
   depth: 42, occurrence: 2

XXX total number of pointers: 627

XXX times a variable address is taken: 1408
XXX times a pointer is dereferenced on RHS: 459
breakdown:
   depth: 1, occurrence: 393
   depth: 2, occurrence: 30
   depth: 3, occurrence: 18
   depth: 4, occurrence: 13
   depth: 5, occurrence: 5
XXX times a pointer is dereferenced on LHS: 440
breakdown:
   depth: 1, occurrence: 419
   depth: 2, occurrence: 13
   depth: 3, occurrence: 5
   depth: 4, occurrence: 3
XXX times a pointer is compared with null: 68
XXX times a pointer is compared with address of another variable: 17
XXX times a pointer is compared with another pointer: 19
XXX times a pointer is qualified to be dereferenced: 11329

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2601
   level: 2, occurrence: 357
   level: 3, occurrence: 224
   level: 4, occurrence: 117
   level: 5, occurrence: 36
XXX number of pointers point to pointers: 253
XXX number of pointers point to scalars: 374
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.9
XXX average alias set size: 1.46

XXX times a non-volatile is read: 2618
XXX times a non-volatile is write: 1236
XXX times a volatile is read: 72
XXX    times read thru a pointer: 10
XXX times a volatile is write: 36
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 2.89e+03
XXX percentage of non-volatile access: 97.3

XXX forward jumps: 4
XXX backward jumps: 11

XXX stmts: 170
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 26
   depth: 2, occurrence: 26
   depth: 3, occurrence: 22
   depth: 4, occurrence: 28
   depth: 5, occurrence: 43

XXX percentage a fresh-made variable is used: 14.2
XXX percentage an existing variable is used: 85.8
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1354256918
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static float g_12 = (-0x7.Cp+1);
static uint16_t g_13 = 0x12E1L;
static int32_t g_15[6][1][1] = {{{1L}},{{1L}},{{1L}},{{1L}},{{1L}},{{1L}}};
static uint32_t g_33 = 0x4D3EC93CL;
static int8_t g_36 = (-1L);
static uint32_t g_47 = 18446744073709551607UL;
static uint32_t *g_46 = &g_47;
static int8_t g_60 = 0x2BL;
static const int8_t * const g_59 = &g_60;
static int32_t g_73 = 0x90031234L;
static uint32_t g_78[5] = {1UL,1UL,1UL,1UL,1UL};
static int32_t g_80 = (-1L);
static int32_t * volatile g_79 = &g_80;/* VOLATILE GLOBAL g_79 */
static const int8_t g_96 = 0x9EL;
static const int8_t *g_95 = &g_96;
static float * volatile g_127 = &g_12;/* VOLATILE GLOBAL g_127 */
static const volatile int32_t g_146 = 0L;/* VOLATILE GLOBAL g_146 */
static uint8_t g_148 = 248UL;
static float g_151 = 0x3.408E22p-47;
static volatile float g_161 = (-0x6.8p-1);/* VOLATILE GLOBAL g_161 */
static volatile uint8_t g_162 = 1UL;/* VOLATILE GLOBAL g_162 */
static int16_t g_172 = 1L;
static const volatile int32_t g_176 = (-9L);/* VOLATILE GLOBAL g_176 */
static const volatile int32_t *g_175 = &g_176;
static int32_t * volatile g_185 = (void*)0;/* VOLATILE GLOBAL g_185 */
static int32_t g_187[1] = {0x573B10F1L};
static int32_t * volatile g_186 = &g_187[0];/* VOLATILE GLOBAL g_186 */
static int32_t *g_189 = (void*)0;
static uint64_t g_207 = 0x3779968936E2BBFELL;
static int32_t * const  volatile g_208 = &g_187[0];/* VOLATILE GLOBAL g_208 */
static volatile uint32_t **g_210 = (void*)0;
static volatile uint32_t *** const g_209 = &g_210;
static volatile uint32_t ***g_214 = &g_210;
static volatile uint32_t **** volatile g_213 = &g_214;/* VOLATILE GLOBAL g_213 */
static volatile int32_t g_230 = 3L;/* VOLATILE GLOBAL g_230 */
static int32_t ** volatile g_235[2] = {(void*)0,(void*)0};
static const uint32_t * const **g_249 = (void*)0;
static const uint32_t * const ***g_248 = &g_249;
static const uint32_t * const **** volatile g_247[4] = {&g_248,&g_248,&g_248,&g_248};
static const uint32_t * const **** volatile g_251[8][5][5] = {{{&g_248,&g_248,&g_248,&g_248,&g_248},{(void*)0,&g_248,&g_248,(void*)0,&g_248},{(void*)0,(void*)0,&g_248,&g_248,&g_248},{&g_248,(void*)0,&g_248,&g_248,(void*)0},{(void*)0,&g_248,&g_248,&g_248,&g_248}},{{&g_248,&g_248,&g_248,&g_248,&g_248},{&g_248,&g_248,&g_248,(void*)0,&g_248},{&g_248,(void*)0,&g_248,&g_248,&g_248},{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,&g_248,&g_248,&g_248}},{{&g_248,(void*)0,(void*)0,(void*)0,(void*)0},{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,&g_248,(void*)0,&g_248},{&g_248,&g_248,&g_248,&g_248,(void*)0},{&g_248,&g_248,(void*)0,&g_248,&g_248}},{{&g_248,&g_248,(void*)0,(void*)0,(void*)0},{&g_248,(void*)0,(void*)0,&g_248,&g_248},{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,&g_248,&g_248,&g_248},{&g_248,(void*)0,&g_248,&g_248,(void*)0}},{{(void*)0,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,(void*)0,(void*)0,&g_248},{&g_248,&g_248,&g_248,&g_248,(void*)0},{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,(void*)0,&g_248,(void*)0,&g_248}},{{&g_248,(void*)0,&g_248,&g_248,&g_248},{&g_248,&g_248,(void*)0,(void*)0,(void*)0},{&g_248,&g_248,&g_248,&g_248,&g_248},{(void*)0,(void*)0,&g_248,&g_248,(void*)0},{&g_248,(void*)0,(void*)0,&g_248,&g_248}},{{&g_248,&g_248,&g_248,(void*)0,&g_248},{&g_248,&g_248,&g_248,&g_248,(void*)0},{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,&g_248,(void*)0,&g_248},{&g_248,(void*)0,&g_248,&g_248,&g_248}},{{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,&g_248,&g_248,&g_248},{&g_248,(void*)0,(void*)0,(void*)0,(void*)0},{&g_248,&g_248,(void*)0,&g_248,&g_248},{&g_248,&g_248,&g_248,(void*)0,&g_248}}};
static const uint32_t * const **** volatile g_252 = &g_248;/* VOLATILE GLOBAL g_252 */
static uint32_t g_265 = 0xB33B7C41L;
static int64_t g_269[6][7][6] = {{{0xB990DF19C7C3C239LL,0x601889A85860A872LL,(-10L),(-1L),1L,0x2E6605E10C31E50DLL},{0x799C365DECAD3550LL,(-10L),0x4C86FCE4B269A4FFLL,0x1A0743956B44700ELL,1L,0xCC4CEC06E7D003E0LL},{(-1L),0xA5FBA940064E38AALL,0x2623FB48C5FC14D1LL,0x49828811F8AF52E7LL,0L,0x54A15C8C22795154LL},{0L,(-10L),0xEB2AF3A1AAD2AB0DLL,(-1L),0x39D242D67FBCB9C9LL,0x02D36866BDBAEF62LL},{(-1L),0x02D36866BDBAEF62LL,0x101DCE6AB1F8B39ELL,0x7EADD43F1477426CLL,(-4L),0x601889A85860A872LL},{0L,0xBAB814BB1F648EDCLL,0x4C86FCE4B269A4FFLL,0L,0xD939C06118430E8CLL,(-6L)},{0L,0x54A15C8C22795154LL,0xE7AA22D6C9223B54LL,(-1L),0x9B3D8044210E2D0ELL,0x7D9DACEBCEC931C8LL}},{{1L,0xD939C06118430E8CLL,0xA94C6A02E83A3F03LL,1L,(-1L),0L},{0x4362D38288692B57LL,(-1L),0x6E5DAF8DDA87CF72LL,1L,(-1L),1L},{0xD939C06118430E8CLL,0x65C628F4081C8072LL,0xBF8C482F6ECCC236LL,0xD85741A342A92665LL,1L,0x2E6605E10C31E50DLL},{0xBF8C482F6ECCC236LL,0L,0x97829D7283E07E04LL,0L,0xEB2AF3A1AAD2AB0DLL,0x725F73CB7B07397FLL},{0xB5E43229EAA28D7FLL,0L,0L,0x02D36866BDBAEF62LL,0x2623FB48C5FC14D1LL,1L},{(-10L),4L,0L,7L,(-1L),1L},{0x8ECCA06ABC1FAA2ALL,0xCC4CEC06E7D003E0LL,0x70BC3FBBB62DD90ELL,(-1L),(-3L),0x1A0743956B44700ELL}},{{4L,(-1L),0x9EC811DE73CCA038LL,9L,0L,0xCC8E43733E590DB3LL},{0x62E858675BAE6187LL,0L,0x2F2C5A303774D245LL,0x1A0743956B44700ELL,0x1A0743956B44700ELL,0x2F2C5A303774D245LL},{0x7D9DACEBCEC931C8LL,0x7D9DACEBCEC931C8LL,0x54A15C8C22795154LL,0x3EB784779427902DLL,0x725F73CB7B07397FLL,0xCD711647F9239230LL},{0xBA347272B21C91E3LL,3L,(-1L),0xA813C421EE8AF59BLL,1L,0x54A15C8C22795154LL},{0x4C86FCE4B269A4FFLL,0xBA347272B21C91E3LL,(-1L),0x3FFC807B8FDB146ELL,0x7D9DACEBCEC931C8LL,0xCD711647F9239230LL},{0x9EC811DE73CCA038LL,0x3FFC807B8FDB146ELL,0x54A15C8C22795154LL,(-1L),0x75080D3EC3273424LL,0x2F2C5A303774D245LL},{(-1L),0x75080D3EC3273424LL,0x2F2C5A303774D245LL,0x97829D7283E07E04LL,0x72038A2CE407AA3ELL,0xCC8E43733E590DB3LL}},{{(-1L),0x2A3786C04805B41BLL,0x9EC811DE73CCA038LL,8L,0L,0x1A0743956B44700ELL},{0L,0L,0x70BC3FBBB62DD90ELL,0x74A82D6B21FF38C2LL,0x101DCE6AB1F8B39ELL,1L},{0xA94C6A02E83A3F03LL,0xE150EA9A78E21C28LL,0L,(-10L),0xCC8E43733E590DB3LL,1L},{1L,0x2623FB48C5FC14D1LL,0L,(-1L),9L,0x725F73CB7B07397FLL},{0x601889A85860A872LL,1L,0x97829D7283E07E04LL,8L,0x54A15C8C22795154LL,0x2E6605E10C31E50DLL},{7L,0x757571829236C784LL,0xBF8C482F6ECCC236LL,0x2623FB48C5FC14D1LL,0x62E858675BAE6187LL,1L},{0x20C15073BAD02135LL,0x601889A85860A872LL,0x6E5DAF8DDA87CF72LL,(-1L),0xA94C6A02E83A3F03LL,0L}},{{(-4L),0xC504124FB894513ELL,0xA94C6A02E83A3F03LL,0x6E5DAF8DDA87CF72LL,3L,0x7D9DACEBCEC931C8LL},{0xBAB814BB1F648EDCLL,(-3L),0xE7AA22D6C9223B54LL,0xE150EA9A78E21C28LL,0x757571829236C784LL,(-6L)},{1L,0x1A0743956B44700ELL,0x4C86FCE4B269A4FFLL,(-10L),0x799C365DECAD3550LL,0x601889A85860A872LL},{0xAB1568D0C91DE95DLL,1L,0x101DCE6AB1F8B39ELL,0x26A0A948151461E7LL,0xE7AA22D6C9223B54LL,0x02D36866BDBAEF62LL},{0x2E6605E10C31E50DLL,8L,0xEB2AF3A1AAD2AB0DLL,(-6L),(-1L),0xC504124FB894513ELL},{0x49828811F8AF52E7LL,0xEB2AF3A1AAD2AB0DLL,0x20C15073BAD02135LL,(-1L),(-6L),(-1L)},{0x26A0A948151461E7LL,9L,0x26A0A948151461E7LL,1L,0x2F2C5A303774D245LL,1L}},{{3L,0L,0x2A3786C04805B41BLL,7L,0xBA347272B21C91E3LL,0x2F2C5A303774D245LL},{(-1L),0x8F760DCFBD6B6C70LL,0x20C15073BAD02135LL,7L,3L,0xDB1492B466F61382LL},{3L,1L,9L,0xDB1492B466F61382LL,0x7D9DACEBCEC931C8LL,0xD85741A342A92665LL},{(-1L),(-1L),0x8F760DCFBD6B6C70LL,0x4C86FCE4B269A4FFLL,0L,0x7D9DACEBCEC931C8LL},{0x2E6605E10C31E50DLL,0xA94C6A02E83A3F03LL,0xCC4CEC06E7D003E0LL,0x75080D3EC3273424LL,(-1L),(-1L)},{0x101DCE6AB1F8B39ELL,(-10L),(-1L),(-1L),0xCC4CEC06E7D003E0LL,0L},{0xD85741A342A92665LL,0x26A0A948151461E7LL,0x7D9DACEBCEC931C8LL,0x4362D38288692B57LL,4L,(-1L)}}};
static int32_t * volatile g_272 = &g_187[0];/* VOLATILE GLOBAL g_272 */
static uint16_t g_307 = 9UL;
static uint16_t g_312 = 0x4CC7L;
static float * volatile g_340 = (void*)0;/* VOLATILE GLOBAL g_340 */
static int32_t g_355 = 3L;
static float * volatile g_360 = &g_12;/* VOLATILE GLOBAL g_360 */
static int16_t g_403[7] = {1L,1L,1L,1L,1L,1L,1L};
static int32_t * volatile g_411 = &g_187[0];/* VOLATILE GLOBAL g_411 */
static int32_t ** volatile g_413 = &g_189;/* VOLATILE GLOBAL g_413 */
static uint16_t g_434 = 65526UL;
static const int32_t *g_460 = &g_187[0];
static float * volatile g_469 = &g_12;/* VOLATILE GLOBAL g_469 */
static float * volatile g_475 = &g_151;/* VOLATILE GLOBAL g_475 */
static uint32_t g_502[2][8] = {{0xA1031FD7L,0xAA501E6DL,0xAA501E6DL,0xA1031FD7L,1UL,0xA1031FD7L,0xAA501E6DL,0xAA501E6DL},{0xAA501E6DL,1UL,4UL,4UL,1UL,0xAA501E6DL,1UL,4UL}};
static float * const  volatile g_555 = &g_151;/* VOLATILE GLOBAL g_555 */
static float g_564[2][7] = {{(-0x1.Fp+1),(-0x5.8p-1),(-0x1.Fp+1),0x1.E76D39p+27,0x1.E76D39p+27,(-0x1.Fp+1),(-0x5.8p-1)},{0x1.E76D39p+27,(-0x5.8p-1),0x1.AA9ABAp+86,0x1.AA9ABAp+86,(-0x5.8p-1),0x1.E76D39p+27,(-0x5.8p-1)}};
static int32_t * volatile g_583 = &g_73;/* VOLATILE GLOBAL g_583 */
static int32_t * volatile g_594 = &g_187[0];/* VOLATILE GLOBAL g_594 */
static int64_t g_603 = 0x5F1B1795F354E003LL;
static uint32_t g_605[5][6] = {{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}};
static float *g_625[10] = {&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12};
static float **g_624 = &g_625[7];
static const int32_t ** volatile g_646 = &g_460;/* VOLATILE GLOBAL g_646 */
static volatile uint16_t g_715 = 65535UL;/* VOLATILE GLOBAL g_715 */
static volatile uint16_t *g_714 = &g_715;
static volatile uint16_t * volatile * volatile g_713[2][7][4] = {{{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,(void*)0,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714}},{{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,(void*)0,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714},{&g_714,&g_714,&g_714,&g_714}}};
static volatile uint16_t * volatile * volatile * const  volatile g_712[7] = {&g_713[0][3][3],&g_713[0][3][3],&g_713[0][3][3],&g_713[0][3][3],&g_713[0][3][3],&g_713[0][3][3],&g_713[0][3][3]};
static volatile uint16_t * volatile * volatile * volatile g_717 = &g_713[0][3][3];/* VOLATILE GLOBAL g_717 */
static uint32_t * volatile *g_738 = &g_46;
static uint32_t * volatile **g_737 = &g_738;
static uint32_t * volatile ***g_736 = &g_737;
static uint32_t *g_773 = &g_502[0][6];
static uint16_t g_864 = 65532UL;
static uint64_t *g_900 = (void*)0;
static uint64_t *g_901 = (void*)0;
static int64_t g_905 = 0x2A3F97EB1955044BLL;
static int8_t **** volatile g_931 = (void*)0;/* VOLATILE GLOBAL g_931 */
static const int32_t ** volatile g_975 = &g_460;/* VOLATILE GLOBAL g_975 */
static int64_t *g_986 = (void*)0;
static int64_t * const * volatile g_985 = &g_986;/* VOLATILE GLOBAL g_985 */
static uint32_t g_988 = 0x06A90E00L;
static const int32_t ** volatile g_1000 = &g_460;/* VOLATILE GLOBAL g_1000 */
static int32_t g_1027 = (-1L);
static const int32_t ** volatile g_1043 = (void*)0;/* VOLATILE GLOBAL g_1043 */
static uint32_t g_1044 = 0x60211885L;
static volatile uint8_t g_1056 = 0xD0L;/* VOLATILE GLOBAL g_1056 */
static volatile float g_1072 = 0x1.8331ABp-1;/* VOLATILE GLOBAL g_1072 */
static int32_t ** volatile g_1094 = &g_189;/* VOLATILE GLOBAL g_1094 */
static int64_t g_1182 = 0x27F17402AB5DC079LL;
static volatile uint8_t g_1198 = 0x90L;/* VOLATILE GLOBAL g_1198 */
static int16_t g_1279 = 0x352CL;
static int16_t g_1280 = 9L;
static uint8_t g_1281 = 0xE6L;
static uint32_t **g_1297 = (void*)0;
static int32_t ** const  volatile g_1319 = &g_189;/* VOLATILE GLOBAL g_1319 */
static volatile uint32_t g_1329 = 0x883FA5F9L;/* VOLATILE GLOBAL g_1329 */
static const volatile uint16_t *g_1358 = &g_715;
static const volatile uint16_t **g_1357 = &g_1358;
static const volatile uint16_t ***g_1356 = &g_1357;
static const volatile uint16_t ****g_1355[10][2] = {{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356},{&g_1356,&g_1356}};
static const volatile uint16_t *****g_1354[1] = {&g_1355[5][0]};
static uint8_t *g_1402 = &g_148;
static int32_t *g_1417 = &g_355;
static int32_t ** volatile g_1416 = &g_1417;/* VOLATILE GLOBAL g_1416 */
static int32_t ** volatile * const g_1415 = &g_1416;
static int32_t ** volatile g_1460 = (void*)0;/* VOLATILE GLOBAL g_1460 */
static const uint32_t ****g_1545 = (void*)0;
static const uint32_t ***** volatile g_1544 = &g_1545;/* VOLATILE GLOBAL g_1544 */
static uint16_t * const *g_1598 = (void*)0;
static uint16_t * const **g_1597 = &g_1598;
static uint16_t * const ***g_1596 = &g_1597;
static uint16_t * const ****g_1595 = &g_1596;
static int32_t * volatile g_1616 = &g_73;/* VOLATILE GLOBAL g_1616 */
static const int32_t ** volatile g_1652 = (void*)0;/* VOLATILE GLOBAL g_1652 */
static const int32_t ** volatile g_1653[9][5] = {{&g_460,&g_460,&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460,(void*)0,&g_460},{&g_460,(void*)0,&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460,(void*)0,&g_460},{&g_460,(void*)0,&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460,(void*)0,&g_460},{&g_460,(void*)0,&g_460,&g_460,&g_460}};
static const int32_t *g_1671 = &g_355;
static const int32_t **g_1670 = &g_1671;
static const int32_t ** const *g_1669 = &g_1670;
static const int32_t g_1676 = 3L;
static int32_t * volatile g_1715 = &g_15[4][0][0];/* VOLATILE GLOBAL g_1715 */
static int8_t *g_1718 = &g_60;
static int8_t **g_1717 = &g_1718;
static uint16_t g_1763[2][9] = {{0x6CB1L,0UL,0UL,0x6CB1L,0x6CB1L,0UL,0UL,0x6CB1L,0x6CB1L},{0xB85FL,0xB809L,0xB85FL,0xB809L,0xB85FL,0xB809L,0xB85FL,0xB809L,0xB85FL}};
static int32_t ** volatile g_1769 = &g_189;/* VOLATILE GLOBAL g_1769 */
static int8_t ** const *g_1771 = (void*)0;
static int8_t ** const **g_1770 = &g_1771;
static float g_1809 = 0xE.1D41ECp-78;
static float * volatile g_1811 = &g_564[0][6];/* VOLATILE GLOBAL g_1811 */
static int32_t g_1845 = 9L;
static int8_t g_1861 = (-3L);
static float ***g_1909 = (void*)0;
static const uint8_t g_2004[6] = {4UL,4UL,4UL,4UL,4UL,4UL};
static float * const  volatile g_2007 = &g_1809;/* VOLATILE GLOBAL g_2007 */
static int32_t * const  volatile g_2081 = &g_187[0];/* VOLATILE GLOBAL g_2081 */
static int32_t ** volatile g_2133[8][6][5] = {{{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0}},{{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0}},{{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0}},{{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,(void*)0},{&g_189,(void*)0,&g_189,&g_189,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189}},{{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189}},{{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189}},{{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189}},{{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189},{&g_189,&g_189,(void*)0,(void*)0,&g_189}}};
static const uint16_t g_2158 = 0xF0B9L;
static const uint16_t *g_2157 = &g_2158;
static const uint16_t ** const g_2156 = &g_2157;
static const uint16_t ** const * const g_2155[1] = {&g_2156};
static const uint16_t ** const * const *g_2154 = &g_2155[0];
static const uint16_t *g_2162 = &g_2158;
static const uint16_t *g_2163 = &g_2158;
static const uint16_t *g_2164 = (void*)0;
static const uint16_t *g_2165 = &g_2158;
static const uint16_t *g_2166[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const uint16_t *g_2167 = &g_2158;
static const uint16_t *g_2168 = &g_2158;
static const uint16_t *g_2169 = &g_2158;
static const uint16_t *g_2170[3][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
static const uint16_t *g_2171 = &g_2158;
static const uint16_t *g_2172[1][3] = {{(void*)0,(void*)0,(void*)0}};
static const uint16_t *g_2173 = &g_2158;
static const uint16_t *g_2174 = &g_2158;
static const uint16_t *g_2175 = &g_2158;
static const uint16_t *g_2176 = &g_2158;
static const uint16_t ** const g_2161[10][4] = {{&g_2176,(void*)0,(void*)0,(void*)0},{&g_2170[1][0],&g_2172[0][2],&g_2170[1][0],(void*)0},{&g_2170[1][0],(void*)0,(void*)0,&g_2163},{&g_2176,(void*)0,&g_2168,(void*)0},{(void*)0,&g_2172[0][2],&g_2168,(void*)0},{&g_2176,(void*)0,(void*)0,(void*)0},{&g_2170[1][0],&g_2172[0][2],&g_2170[1][0],(void*)0},{&g_2170[1][0],(void*)0,(void*)0,&g_2163},{&g_2176,(void*)0,&g_2168,(void*)0},{(void*)0,&g_2172[0][2],&g_2168,(void*)0}};
static const uint16_t ** const * const g_2160 = &g_2161[6][1];
static const uint16_t ** const * const *g_2159 = &g_2160;
static const uint16_t g_2223[4] = {1UL,1UL,1UL,1UL};
static int32_t **g_2226 = (void*)0;
static const int32_t ** volatile g_2248 = &g_460;/* VOLATILE GLOBAL g_2248 */
static volatile int32_t g_2264 = 0xDE23EDE1L;/* VOLATILE GLOBAL g_2264 */
static int8_t g_2269[2] = {0xA9L,0xA9L};


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int32_t * func_2(uint32_t  p_3, uint32_t  p_4, float  p_5);
static int32_t  func_9(int32_t  p_10, int32_t * p_11);
static uint8_t  func_26(int8_t  p_27, const int32_t * p_28, uint16_t  p_29, const uint8_t  p_30, uint32_t  p_31);
static const int32_t * func_37(int32_t  p_38, uint32_t  p_39);
static float  func_43(uint32_t * p_44, uint8_t  p_45);
static int32_t * func_48(uint32_t  p_49, uint32_t * p_50, int8_t * p_51, uint32_t * p_52, int64_t  p_53);
static int8_t  func_56(const int8_t * const  p_57, uint32_t  p_58);
static int8_t * func_68(int32_t  p_69, int32_t * p_70, uint32_t  p_71);
static const int8_t * func_83(int8_t * p_84, int8_t * p_85, int16_t  p_86, int32_t * p_87, int32_t  p_88);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_13 g_15 g_46 g_59 g_60 g_47 g_78 g_79 g_96 g_73 g_80 g_127 g_146 g_162 g_175 g_176 g_186 g_189 g_187 g_148 g_207 g_208 g_209 g_213 g_252 g_272 g_230 g_95 g_269 g_265 g_12 g_360 g_307 g_403 g_411 g_413 g_355 g_434 g_172 g_555 g_151 g_624 g_625 g_1094 g_33 g_1027 g_214 g_210 g_1056 g_714 g_715 g_905 g_1319 g_1329 g_36 g_594 g_1354 g_502 g_1182 g_736 g_931 g_1402 g_460 g_1279 g_1717 g_1718 g_1763 g_1769 g_864 g_1770 g_1670 g_1671 g_1357 g_1358 g_564 g_1811 g_1595 g_1596 g_975 g_1415 g_1416 g_1417 g_605 g_1715 g_2007 g_1676 g_1281 g_1616 g_603 g_2081 g_1356 g_312 g_2158
 * writes: g_15 g_33 g_36 g_73 g_47 g_80 g_95 g_78 g_12 g_148 g_162 g_172 g_175 g_187 g_207 g_214 g_189 g_248 g_265 g_269 g_60 g_307 g_312 g_151 g_403 g_355 g_460 g_1027 g_1297 g_1281 g_13 g_1329 g_1182 g_737 g_864 g_1770 g_1280 g_1809 g_564 g_1763 g_1417 g_1597 g_605 g_905
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int16_t l_6 = 9L;
    int32_t *l_14 = &g_15[1][0][0];
    uint32_t *l_32 = &g_33;
    int64_t l_34 = 0x78D8EF068CD54673LL;
    int8_t *l_35 = &g_36;
    uint16_t l_40 = 0xFC2EL;
    float *l_414 = &g_12;
    int32_t *l_1095 = &g_1027;
    const int16_t l_2066[4] = {0xA859L,0xA859L,0xA859L,0xA859L};
    uint32_t l_2067 = 4294967288UL;
    int32_t **l_2308 = &g_189;
    int i;
    (*l_2308) = func_2(l_6, ((safe_add_func_uint32_t_u_u((((func_9(((*l_14) ^= g_13), ((safe_rshift_func_int8_t_s_s(((safe_mod_func_int16_t_s_s((safe_add_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u(g_13, 3)), (safe_div_func_uint8_t_u_u(func_26(((*l_35) = (g_13 , (((*l_32) = g_13) , l_34))), func_37(l_40, ((safe_sub_func_float_f_f((((*l_414) = func_43(((0x376FBFCCL > (g_13 || g_13)) , g_46), g_13)) > 0xA.BB3B7Ep+81), 0x0.3p-1)) , 4294967295UL)), g_96, g_355, g_96), 0xEEL)))), 65533UL)) && l_40), 2)) , l_1095)) <= 6UL) > g_603) == l_2066[1]), g_603)) , 0x22AE8D47L), l_2067);
    for (g_312 = 0; (g_312 >= 53); g_312 = safe_add_func_uint16_t_u_u(g_312, 6))
    { /* block id: 985 */
        if ((**l_2308))
            break;
        (*l_2308) = (*g_413);
    }
    return g_2158;
}


/* ------------------------------------------ */
/* 
 * reads : g_2081 g_187 g_605 g_594 g_1616 g_15 g_33 g_1356 g_1357 g_1358 g_715 g_96 g_1402 g_148 g_905 g_603 g_307
 * writes: g_307 g_187 g_605 g_73 g_15 g_172 g_905 g_80
 */
static int32_t * func_2(uint32_t  p_3, uint32_t  p_4, float  p_5)
{ /* block id: 889 */
    uint16_t l_2070 = 0x5C4DL;
    uint16_t *l_2072 = &l_2070;
    uint16_t **l_2071 = &l_2072;
    uint16_t ***l_2073 = &l_2071;
    uint16_t *l_2078 = (void*)0;
    uint16_t *l_2079 = &g_307;
    uint64_t * const l_2080 = &g_207;
    uint32_t *l_2094 = &g_605[2][4];
    uint8_t **l_2097[8] = {&g_1402,&g_1402,&g_1402,&g_1402,&g_1402,&g_1402,&g_1402,&g_1402};
    int32_t *l_2098 = &g_15[1][0][0];
    int16_t *l_2107 = &g_172;
    int64_t *l_2108 = &g_905;
    int32_t *l_2109 = &g_80;
    int8_t ***l_2123 = &g_1717;
    int8_t ****l_2122 = &l_2123;
    int32_t l_2200 = 1L;
    int32_t l_2201 = 0x65523DBFL;
    int64_t l_2202 = 0L;
    int32_t l_2204 = 0xF14E7D84L;
    int64_t ***l_2214 = (void*)0;
    int32_t **l_2227 = (void*)0;
    int32_t l_2265 = 0xB21CEB8AL;
    int32_t l_2266[1];
    int16_t l_2267 = 0xD626L;
    int64_t l_2271 = 0x2DC146C1EB29961DLL;
    int32_t *l_2307 = &g_73;
    int i;
    for (i = 0; i < 1; i++)
        l_2266[i] = (-3L);
    (*g_2081) &= (safe_mul_func_int16_t_s_s(((l_2070 > (((*l_2073) = l_2071) != (((*l_2079) = ((0x4276112A78A5F616LL >= (safe_sub_func_int16_t_s_s((safe_div_func_uint64_t_u_u(p_3, p_3)), 0x8ACEL))) , p_3)) , &l_2072))) == ((void*)0 != l_2080)), 0x30B5L));
    (*g_594) = (safe_mod_func_int16_t_s_s((((***l_2073) = (safe_add_func_int16_t_s_s((l_2070 < ((safe_add_func_int32_t_s_s(((*l_2109) = (((p_3 >= (safe_sub_func_int64_t_s_s(((*l_2108) |= ((((safe_div_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((((((*l_2094)++) , ((((void*)0 != l_2097[1]) , ((*l_2098) = ((*g_1616) = (*g_594)))) && (p_4 , (safe_div_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((((*l_2107) = (safe_mul_func_uint8_t_u_u((*l_2098), ((safe_sub_func_uint32_t_u_u(g_33, ((***g_1356) != 0x57FAL))) , 246UL)))) == p_3), p_4)) == 0xA15BE09EL), g_96))))) , p_3) ^ p_4), (*g_1402))), p_3)) < 5L) >= p_4) == 0x0766A9B3805AF5DFLL)), 0x4E9B304CCCE5B383LL))) <= l_2070) | (*g_1402))), p_3)) | p_3)), 0UL))) <= g_603), (-1L)));
    for (g_307 = 0; (g_307 != 6); g_307++)
    { /* block id: 903 */
        float l_2119 = 0x0.1AB27Dp+60;
        uint32_t *l_2130 = &g_502[0][0];
        int32_t l_2131[4][8][6] = {{{0xE7B419E5L,0x7A02A153L,0L,0x729787AEL,0xF1D41173L,0x9DCBE61AL},{(-9L),0x7A02A153L,(-1L),0x63302C27L,0x62A1F0C3L,0x4153A71BL},{1L,4L,0x89FF1A82L,0x94F1A4FEL,0x63302C27L,9L},{(-1L),0x5045B292L,0x904F9002L,8L,0x7F2814B9L,0L},{(-1L),0xE545F48EL,(-3L),0x2C7F869FL,0xEFB91521L,0xED4DE659L},{0xF24E10AFL,0xED4DE659L,0x33A85A9EL,9L,0xAE17F05BL,1L},{9L,0xEFB91521L,0x2C1F6191L,0x9DCBE61AL,0x671779F7L,9L},{0x0C713307L,1L,0x1DE433F5L,0x89FF1A82L,(-1L),0x32623984L}},{{0x1DE433F5L,9L,(-1L),0xEFB91521L,1L,0x94F1A4FEL},{0x3FDF7F3CL,0x8497927FL,0xAE17F05BL,1L,0xF24E10AFL,0L},{0x729787AEL,1L,(-1L),0x623935CAL,0x623935CAL,(-1L)},{0L,0L,0L,(-1L),(-1L),0xEFB91521L},{0x33A85A9EL,0x83CF7695L,0x9DCBE61AL,0xDA70443FL,0xE545F48EL,0L},{(-1L),0x33A85A9EL,0x9DCBE61AL,0x54B240F5L,0L,0xEFB91521L},{(-1L),0x54B240F5L,0L,0xF1D41173L,0x89FF1A82L,(-1L)},{0xF1D41173L,0x89FF1A82L,(-1L),0x702D0DD2L,0xED4DE659L,0L}},{{(-1L),(-1L),0xAE17F05BL,5L,(-1L),0x94F1A4FEL},{(-1L),0x7CEF9E02L,(-1L),(-1L),9L,0x32623984L},{0x54B240F5L,0xF1D41173L,0x1DE433F5L,0x0C713307L,0x5F28E166L,9L},{0x7F2814B9L,0x671779F7L,0x2C1F6191L,0L,0x33A85A9EL,1L},{0xE545F48EL,0x62A1F0C3L,0x33A85A9EL,(-10L),5L,0xED4DE659L},{0xF8DF23D5L,0x79F1587EL,(-3L),(-1L),1L,0L},{0x2C1F6191L,0x3FDF7F3CL,0x904F9002L,9L,0xDA70443FL,9L},{0x89FF1A82L,(-1L),0x89FF1A82L,(-9L),0x2C7F869FL,0x4153A71BL}},{{(-6L),0x623935CAL,(-1L),0x671779F7L,0x5045B292L,0x9DCBE61AL},{0xDA70443FL,(-1L),0L,0x671779F7L,0x79F1587EL,(-9L)},{(-6L),0x729787AEL,(-1L),(-9L),(-1L),0x5045B292L},{0x89FF1A82L,0x94F1A4FEL,0x63302C27L,9L,(-1L),(-1L)},{0x2C1F6191L,(-3L),1L,(-1L),(-1L),0x1DE433F5L},{0x702D0DD2L,0x7A02A153L,1L,(-6L),0xC8168777L,1L},{0xF8DF23D5L,(-1L),0x2C7F869FL,1L,0L,(-9L)},{0xF1D41173L,0x94F1A4FEL,(-1L),0x33A85A9EL,1L,(-1L)}}};
        int64_t l_2132 = 0L;
        const uint16_t *l_2152 = &g_307;
        const uint16_t ** const l_2151[8][8] = {{(void*)0,&l_2152,&l_2152,(void*)0,&l_2152,(void*)0,(void*)0,&l_2152},{&l_2152,&l_2152,&l_2152,&l_2152,(void*)0,(void*)0,&l_2152,&l_2152},{&l_2152,&l_2152,&l_2152,&l_2152,&l_2152,&l_2152,&l_2152,&l_2152},{(void*)0,&l_2152,(void*)0,&l_2152,&l_2152,(void*)0,(void*)0,&l_2152},{&l_2152,&l_2152,(void*)0,&l_2152,(void*)0,(void*)0,&l_2152,(void*)0},{&l_2152,&l_2152,&l_2152,&l_2152,&l_2152,&l_2152,&l_2152,&l_2152},{(void*)0,&l_2152,&l_2152,(void*)0,&l_2152,(void*)0,&l_2152,&l_2152},{&l_2152,(void*)0,(void*)0,&l_2152,(void*)0,&l_2152,&l_2152,(void*)0}};
        const uint16_t ** const * const l_2150 = &l_2151[3][4];
        const uint16_t ** const * const *l_2149 = &l_2150;
        int64_t **l_2213 = &g_986;
        int64_t ***l_2212 = &l_2213;
        int32_t **l_2224 = &g_1417;
        int32_t l_2253 = 0xE1DB4DBDL;
        uint32_t ***l_2278 = (void*)0;
        uint32_t ****l_2277 = &l_2278;
        int64_t l_2291 = 0x4BD5290F8A08BE86LL;
        int32_t *l_2301 = &g_15[3][0][0];
        int32_t *l_2302 = &g_187[0];
        int32_t *l_2303[8][7][4] = {{{&g_73,&l_2200,&l_2266[0],&g_73},{&l_2131[0][0][0],&g_1845,&l_2265,&l_2200},{&g_15[1][0][0],&g_73,&l_2265,&g_1027},{&l_2131[0][0][0],&g_73,&l_2266[0],&g_73},{&g_73,&g_1845,&g_15[1][0][0],&g_73},{&g_15[1][0][0],&g_73,&l_2266[0],&g_1027},{&g_15[1][0][0],&g_73,&l_2266[0],&l_2200}},{{&g_15[1][0][0],&g_1845,&l_2266[0],&g_73},{&g_15[1][0][0],&l_2200,&g_15[1][0][0],&g_1027},{&g_73,&l_2200,&l_2266[0],&g_73},{&l_2131[0][0][0],&g_1845,&l_2265,&l_2200},{&g_15[1][0][0],&g_73,&l_2265,&g_1027},{&l_2131[0][0][0],&g_73,&l_2266[0],&g_73},{&g_73,&g_1845,&g_15[1][0][0],&g_73}},{{&g_15[1][0][0],&g_73,&l_2266[0],&g_1027},{&g_15[1][0][0],&g_73,&l_2266[0],&l_2200},{&g_15[1][0][0],&g_1845,&l_2266[0],&g_73},{&g_15[1][0][0],&l_2200,&g_15[1][0][0],&g_1027},{&g_73,&l_2200,&l_2266[0],&g_73},{&l_2131[0][0][0],&g_1845,&l_2265,&l_2200},{&g_15[1][0][0],&g_73,&l_2265,&g_1027}},{{&l_2131[0][0][0],&g_73,&l_2266[0],&g_73},{&g_73,&g_1845,&g_15[1][0][0],&g_73},{&g_15[1][0][0],&g_73,&l_2266[0],&g_1027},{&g_15[1][0][0],&g_73,&l_2266[0],&l_2200},{&g_15[1][0][0],&g_1845,&l_2266[0],&g_73},{&g_15[1][0][0],&l_2200,&g_15[1][0][0],&g_1027},{&l_2266[0],&g_1845,&g_15[1][0][0],&g_73}},{{&l_2265,&g_15[1][0][0],&l_2266[0],&g_1845},{&l_2131[0][0][3],&g_73,&l_2266[0],&g_187[0]},{&l_2265,&g_1027,&g_15[1][0][0],&g_1027},{&l_2266[0],&g_15[1][0][0],&l_2131[0][0][3],&g_1027},{&l_2131[0][0][3],&g_1027,&l_2201,&g_187[0]},{&g_15[1][0][0],&g_73,&g_15[1][0][0],&g_1845},{&g_15[1][0][0],&g_15[1][0][0],&l_2201,&g_73}},{{&l_2131[0][0][3],&g_1845,&l_2131[0][0][3],&g_187[0]},{&l_2266[0],&g_1845,&g_15[1][0][0],&g_73},{&l_2265,&g_15[1][0][0],&l_2266[0],&g_1845},{&l_2131[0][0][3],&g_73,&l_2266[0],&g_187[0]},{&l_2265,&g_1027,&g_15[1][0][0],&g_1027},{&l_2266[0],&g_15[1][0][0],&l_2131[0][0][3],&g_1027},{&l_2131[0][0][3],&g_1027,&l_2201,&g_187[0]}},{{&g_15[1][0][0],&g_73,&g_15[1][0][0],&g_1845},{&g_15[1][0][0],&g_15[1][0][0],&l_2201,&g_73},{&l_2131[0][0][3],&g_1845,&l_2131[0][0][3],&g_187[0]},{&l_2266[0],&g_1845,&g_15[1][0][0],&g_73},{&l_2265,&g_15[1][0][0],&l_2266[0],&g_1845},{&l_2131[0][0][3],&g_73,&l_2266[0],&g_187[0]},{&l_2265,&g_1027,&g_15[1][0][0],&g_1027}},{{&l_2266[0],&g_15[1][0][0],&l_2131[0][0][3],&g_1027},{&l_2131[0][0][3],&g_1027,&l_2201,&g_187[0]},{&g_15[1][0][0],&g_73,&g_15[1][0][0],&g_1845},{&g_15[1][0][0],&g_15[1][0][0],&l_2201,&g_73},{&l_2131[0][0][3],&g_1845,&l_2131[0][0][3],&g_187[0]},{&l_2266[0],&g_1845,&g_15[1][0][0],&g_73},{&l_2265,&g_15[1][0][0],&l_2266[0],&g_1845}}};
        uint64_t l_2304 = 0x46BFD68B0343139DLL;
        int i, j, k;
    }
    return l_2307;
}


/* ------------------------------------------ */
/* 
 * reads : g_33 g_175 g_176 g_146 g_1027 g_214 g_210 g_59 g_60 g_1056 g_403 g_172 g_714 g_715 g_905 g_1319 g_1329 g_36 g_207 g_594 g_187 g_1354 g_502 g_269 g_1182 g_736 g_78 g_931 g_1402 g_148 g_460 g_1279 g_1717 g_1718 g_1763 g_1769 g_864 g_1770 g_162 g_1670 g_1671 g_1357 g_1358 g_13 g_564 g_127 g_12 g_624 g_625 g_1811 g_1595 g_1596 g_975 g_1415 g_1416 g_1417 g_208 g_189 g_355 g_605 g_1715 g_2007 g_1676 g_1281 g_15 g_1616 g_73
 * writes: g_33 g_172 g_1027 g_355 g_1297 g_1281 g_60 g_13 g_189 g_1329 g_36 g_207 g_1182 g_737 g_269 g_864 g_1770 g_1280 g_151 g_1809 g_12 g_564 g_15 g_265 g_460 g_80 g_1763 g_1417 g_312 g_1597 g_187
 */
static int32_t  func_9(int32_t  p_10, int32_t * p_11)
{ /* block id: 491 */
    uint32_t l_1099 = 4294967290UL;
    int32_t l_1100 = 0x9B09DF0EL;
    uint32_t **l_1126[1];
    uint32_t ***l_1125[7] = {(void*)0,&l_1126[0],(void*)0,(void*)0,&l_1126[0],(void*)0,(void*)0};
    uint32_t ****l_1124 = &l_1125[2];
    int32_t l_1183 = 0L;
    int32_t l_1185 = 0x4DD89E34L;
    int32_t l_1187 = 0xE0818E69L;
    int32_t l_1188 = (-2L);
    uint16_t l_1189[1];
    int8_t *l_1194 = &g_60;
    int32_t l_1241 = (-4L);
    int32_t l_1287 = 0L;
    int16_t l_1289 = 9L;
    int64_t l_1326[8][8] = {{(-1L),0xFC5855B89F733CA3LL,1L,0xBAC4043EB463937ALL,0x983DDE052FADF05FLL,0x10666815252E1D5FLL,0x10666815252E1D5FLL,0x983DDE052FADF05FLL},{(-5L),0xB07B4A23531BEB24LL,0xB07B4A23531BEB24LL,(-5L),(-1L),(-2L),1L,0L},{0L,(-4L),0xB47A64D060A246F1LL,(-4L),0x352F8EFA2F31B656LL,0L,0x9B48FE85CB1D6B29LL,0xBAC4043EB463937ALL},{0x37B2E6AD492423AELL,(-4L),(-5L),1L,0L,(-2L),(-4L),(-2L)},{0xFC5855B89F733CA3LL,0xB07B4A23531BEB24LL,(-9L),0xB07B4A23531BEB24LL,0xFC5855B89F733CA3LL,0x10666815252E1D5FLL,0L,(-9L)},{0xE5DAD7A9B344A9D5LL,0xFC5855B89F733CA3LL,0x983DDE052FADF05FLL,0xB47A64D060A246F1LL,0L,0L,0x10666815252E1D5FLL,0xB07B4A23531BEB24LL},{(-5L),1L,0x983DDE052FADF05FLL,(-5L),0x9B48FE85CB1D6B29LL,(-1L),0L,0L},{0L,0L,(-9L),(-9L),0L,0L,(-4L),0xB47A64D060A246F1LL}};
    uint64_t *l_1335 = (void*)0;
    uint16_t *l_1353[1];
    uint16_t **l_1352 = &l_1353[0];
    uint16_t ***l_1351[8][1][6] = {{{&l_1352,&l_1352,&l_1352,&l_1352,&l_1352,&l_1352}},{{&l_1352,&l_1352,(void*)0,&l_1352,&l_1352,&l_1352}},{{&l_1352,&l_1352,&l_1352,&l_1352,&l_1352,&l_1352}},{{&l_1352,&l_1352,&l_1352,&l_1352,&l_1352,&l_1352}},{{&l_1352,&l_1352,&l_1352,(void*)0,(void*)0,&l_1352}},{{&l_1352,&l_1352,(void*)0,&l_1352,&l_1352,&l_1352}},{{&l_1352,&l_1352,&l_1352,&l_1352,(void*)0,(void*)0}},{{&l_1352,&l_1352,&l_1352,&l_1352,&l_1352,&l_1352}}};
    uint16_t ****l_1350[1];
    int32_t **l_1420 = &g_1417;
    int32_t ***l_1419[8] = {(void*)0,&l_1420,&l_1420,(void*)0,&l_1420,&l_1420,(void*)0,&l_1420};
    float *l_1473 = &g_151;
    int32_t l_1499[5][8][5] = {{{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L}},{{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L}},{{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L}},{{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,9L,0x60F71C87L},{1L,1L,4L,0xD6B4B254L,4L},{1L,1L,0x60F71C87L,2L,(-1L)},{4L,4L,0L,(-7L),0L},{0x60F71C87L,0x60F71C87L,(-1L),2L,(-1L)},{4L,4L,0L,(-7L),0L},{0x60F71C87L,0x60F71C87L,(-1L),2L,(-1L)}},{{4L,4L,0L,(-7L),0L},{0x60F71C87L,0x60F71C87L,(-1L),2L,(-1L)},{4L,4L,0L,(-7L),0L},{0x60F71C87L,0x60F71C87L,(-1L),2L,(-1L)},{4L,4L,0L,(-7L),0L},{0x60F71C87L,0x60F71C87L,(-1L),2L,(-1L)},{4L,4L,0L,(-7L),0L},{0x60F71C87L,0x60F71C87L,(-1L),2L,(-1L)}}};
    int32_t l_1509 = (-6L);
    int64_t l_1510[1];
    int64_t l_1551[5];
    int8_t l_1629[4][5][6] = {{{0x58L,1L,(-1L),0x4CL,8L,0L},{(-1L),5L,0xD9L,1L,0x95L,0x98L},{(-1L),1L,1L,0x4CL,0x0EL,0L},{0x58L,8L,1L,(-1L),(-3L),0x09L},{0x4AL,(-1L),0x98L,0L,0x62L,0x09L}},{{(-10L),0x95L,1L,(-10L),3L,0L},{0x09L,0xFDL,1L,0x73L,1L,0x98L},{1L,0L,0xD9L,0L,1L,0L},{(-1L),0xFDL,(-1L),0x0EL,3L,0x4AL},{0L,0x95L,(-1L),0x98L,0x62L,0xA1L}},{{0x73L,(-1L),0x0EL,0x98L,(-3L),0x0EL},{0L,8L,0x30L,0x0EL,0x0EL,(-1L)},{(-1L),1L,0L,0L,0x95L,0x30L},{1L,5L,0L,0x73L,8L,(-1L)},{0x09L,1L,0x30L,(-10L),0x52L,0x0EL}},{{(-10L),0x52L,0x0EL,0L,0x99L,0xA1L},{0x4AL,0x52L,(-1L),(-1L),0x52L,0x4AL},{0x58L,1L,(-1L),0x4CL,8L,0L},{(-1L),5L,0xD9L,1L,0x95L,0x98L},{(-1L),1L,1L,0x4CL,0x0EL,0L}}};
    uint16_t l_1703 = 65532UL;
    int8_t l_1735[3];
    uint8_t l_1736 = 255UL;
    uint32_t l_1762 = 18446744073709551614UL;
    int16_t l_1765 = (-4L);
    int64_t * const *l_1822 = &g_986;
    uint8_t *l_1920[5][2] = {{&l_1736,&l_1736},{&l_1736,&g_1281},{&g_148,&g_1281},{&g_1281,&g_1281},{&g_148,&g_148}};
    int32_t l_1949 = 0x3BB2E43BL;
    const uint8_t *l_2003[3];
    float ****l_2029[8][6][3] = {{{&g_1909,(void*)0,&g_1909},{&g_1909,&g_1909,&g_1909},{&g_1909,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,(void*)0,&g_1909}},{{&g_1909,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,&g_1909,&g_1909},{&g_1909,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909}},{{(void*)0,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,&g_1909,&g_1909},{&g_1909,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909}},{{(void*)0,&g_1909,&g_1909},{(void*)0,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,&g_1909,&g_1909},{&g_1909,&g_1909,&g_1909}},{{(void*)0,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,&g_1909,&g_1909}},{{&g_1909,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909}},{{&g_1909,&g_1909,&g_1909},{&g_1909,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,(void*)0,&g_1909},{&g_1909,(void*)0,&g_1909}},{{&g_1909,(void*)0,&g_1909},{&g_1909,&g_1909,&g_1909},{&g_1909,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,&g_1909,&g_1909},{(void*)0,(void*)0,&g_1909}}};
    uint8_t l_2046 = 1UL;
    int16_t l_2065 = (-1L);
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1126[i] = &g_46;
    for (i = 0; i < 1; i++)
        l_1189[i] = 0x534FL;
    for (i = 0; i < 1; i++)
        l_1353[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1350[i] = &l_1351[2][0][2];
    for (i = 0; i < 1; i++)
        l_1510[i] = 0x523F0C9381F5538BLL;
    for (i = 0; i < 5; i++)
        l_1551[i] = 0L;
    for (i = 0; i < 3; i++)
        l_1735[i] = 0xC8L;
    for (i = 0; i < 3; i++)
        l_2003[i] = &g_2004[0];
    for (g_33 = 0; (g_33 <= 3); g_33 += 1)
    { /* block id: 494 */
        uint16_t l_1101 = 0x4C24L;
        uint32_t **l_1123[7] = {(void*)0,(void*)0,&g_46,(void*)0,(void*)0,&g_46,(void*)0};
        uint32_t ***l_1122 = &l_1123[1];
        uint32_t **** const l_1121[5] = {&l_1122,&l_1122,&l_1122,&l_1122,&l_1122};
        int32_t l_1131 = (-10L);
        int32_t l_1186[10][7] = {{(-1L),1L,3L,0x52A9FE85L,0x52A9FE85L,3L,1L},{(-1L),0x3A083CF2L,0xC2D2ADBDL,0x52A9FE85L,0xCF9B56CEL,0xC2D2ADBDL,1L},{0L,1L,0xC2D2ADBDL,0xCF9B56CEL,0x52A9FE85L,0xC2D2ADBDL,0x3A083CF2L},{(-1L),1L,3L,0x52A9FE85L,0x52A9FE85L,3L,1L},{(-1L),0x3A083CF2L,0xC2D2ADBDL,0x52A9FE85L,0xCF9B56CEL,0xC2D2ADBDL,(-3L)},{0x7DA10494L,(-3L),4L,(-10L),0L,4L,5L},{1L,(-3L),0x52A9FE85L,0L,0L,0x52A9FE85L,(-3L)},{1L,5L,4L,0L,(-10L),4L,(-3L)},{0x7DA10494L,(-3L),4L,(-10L),0L,4L,5L},{1L,(-3L),0x52A9FE85L,0L,0L,0x52A9FE85L,(-3L)}};
        uint64_t *l_1195 = &g_207;
        uint32_t l_1243 = 0xAF0EDC83L;
        const int32_t *l_1338[8];
        uint8_t *l_1393 = (void*)0;
        uint32_t ** const **l_1398 = (void*)0;
        float ***l_1449 = (void*)0;
        const int16_t l_1451 = 0L;
        float l_1554 = (-0x1.1p+1);
        int8_t l_1555 = 0x5DL;
        uint16_t * const ****l_1599 = (void*)0;
        int32_t l_1651[5][7] = {{0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L},{8L,0x73ED3D95L,8L,0x73ED3D95L,8L,0x73ED3D95L,8L},{0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L},{8L,0x73ED3D95L,8L,0x73ED3D95L,8L,0x73ED3D95L,8L},{0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L,0xAFDEC304L}};
        int32_t ***l_1679 = &l_1420;
        const int8_t l_1706 = 0xB9L;
        uint32_t l_1723 = 0UL;
        int i, j;
        for (i = 0; i < 8; i++)
            l_1338[i] = &l_1241;
        for (g_172 = 6; (g_172 >= 0); g_172 -= 1)
        { /* block id: 497 */
            (*p_11) &= (*g_175);
        }
        for (g_355 = 0; (g_355 <= 3); g_355 += 1)
        { /* block id: 502 */
            int64_t *l_1098[4][1];
            int64_t **l_1120 = &l_1098[0][0];
            int8_t *l_1129 = &g_36;
            const int32_t l_1130 = 0x83C89E6BL;
            uint64_t *l_1132 = &g_207;
            int32_t l_1139[8] = {0xE4285376L,0xE4285376L,0xE4285376L,0xE4285376L,0xE4285376L,0xE4285376L,0xE4285376L,0xE4285376L};
            uint8_t l_1172 = 255UL;
            uint16_t l_1231 = 0xB386L;
            int8_t l_1234 = (-1L);
            int i, j;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1098[i][j] = &g_905;
            }
        }
        (*p_11) ^= ((*g_214) != (((safe_div_func_uint8_t_u_u(l_1099, ((p_11 != p_11) ^ 0xF0L))) , 1UL) , (g_1297 = ((*l_1122) = &g_773))));
        for (l_1243 = 0; (l_1243 <= 3); l_1243 += 1)
        { /* block id: 575 */
            uint16_t *l_1314 = &g_434;
            uint16_t **l_1313 = &l_1314;
            uint16_t ***l_1312[7] = {&l_1313,&l_1313,&l_1313,&l_1313,&l_1313,&l_1313,&l_1313};
            uint16_t ****l_1311 = &l_1312[4];
            uint8_t *l_1315 = &g_1281;
            int16_t *l_1316 = (void*)0;
            int16_t *l_1317 = &g_172;
            int32_t l_1325 = 1L;
            int32_t l_1327[8][4] = {{(-1L),(-1L),0x3A4D0E16L,(-1L)},{(-1L),0L,0L,(-1L)},{0L,(-1L),0L,0L},{(-1L),(-1L),0x3A4D0E16L,(-1L)},{(-1L),0L,0L,(-1L)},{0L,(-1L),0L,0L},{(-1L),(-1L),0x3A4D0E16L,(-1L)},{(-1L),0L,0L,(-1L)}};
            const uint8_t l_1359 = 0xEEL;
            int i, j;
            if ((safe_mod_func_uint64_t_u_u(((((((((*l_1317) |= ((safe_lshift_func_uint16_t_u_u(0x25D7L, ((safe_div_func_uint16_t_u_u(l_1099, (~(safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((*g_59), ((*l_1315) = (l_1183 , (safe_sub_func_uint16_t_u_u((0x1CEB23F4L ^ (((l_1311 == &g_712[3]) & p_10) == l_1289)), g_1056)))))), g_403[3]))))) >= 0x05BFL))) == p_10)) <= p_10) || (*g_714)) || l_1188) || 1L) , 0x4020098F24336D46LL) , g_905), p_10)))
            { /* block id: 578 */
                int64_t l_1321 = 0x16AF615886D14D24LL;
                int32_t l_1322 = 0xC40DDFA5L;
                int32_t l_1324[9][10][2] = {{{(-1L),5L},{(-10L),(-5L)},{0L,5L},{0L,(-2L)},{0xA421E729L,9L},{(-1L),0x2F62A7B8L},{(-1L),0x7B6CC7DDL},{0L,0x83CC1191L},{0xF8D270ABL,0x64BE918FL},{5L,1L}},{{0xCC386B3FL,(-8L)},{0x7B6CC7DDL,9L},{(-10L),0x83CC1191L},{0x5A3378E9L,0xEC4D9B1DL},{0x3471BDCBL,(-2L)},{(-1L),0x4F979AD0L},{0x22A17002L,0L},{(-1L),5L},{0x5A3378E9L,(-1L)},{0x870582C2L,0x64BE918FL}},{{(-1L),(-8L)},{0x4DE5CE7AL,0x4DE5CE7AL},{0x7B6CC7DDL,5L},{0xF8D270ABL,(-1L)},{0x2F62A7B8L,0xEC4D9B1DL},{0L,0x2F62A7B8L},{0x22A17002L,0x6C35B6DBL},{0x22A17002L,0x2F62A7B8L},{0L,0xEC4D9B1DL},{0x2F62A7B8L,(-1L)}},{{0xF8D270ABL,5L},{0x7B6CC7DDL,0x4DE5CE7AL},{0x4DE5CE7AL,(-8L)},{(-1L),0x64BE918FL},{0x870582C2L,(-1L)},{0x5A3378E9L,5L},{0x870582C2L,0x4F979AD0L},{5L,0xDDB84DC5L},{0xC6CB7BF5L,0x6C35B6DBL},{0xF8D270ABL,0x4DE5CE7AL}},{{0x18C3F0FAL,0L},{0xA421E729L,5L},{(-8L),0L},{0L,0L},{0xCC386B3FL,0x7B6CC7DDL},{0x5657944BL,0L},{0x4F979AD0L,(-8L)},{0x870582C2L,1L},{0xC6CB7BF5L,0x99CD2C5AL},{(-6L),0x6C35B6DBL}},{{0x26EDDCECL,0xCC386B3FL},{0x4F979AD0L,0x3471BDCBL},{0xA421E729L,(-1L)},{4L,0L},{0L,0L},{4L,(-1L)},{0xA421E729L,0x3471BDCBL},{0x4F979AD0L,0xCC386B3FL},{0x26EDDCECL,0x6C35B6DBL},{(-6L),0x99CD2C5AL}},{{0xC6CB7BF5L,1L},{0x870582C2L,(-8L)},{0x4F979AD0L,0L},{0x5657944BL,0x7B6CC7DDL},{0xCC386B3FL,0L},{0L,0L},{(-8L),5L},{0xA421E729L,0L},{0x18C3F0FAL,0x4DE5CE7AL},{0xF8D270ABL,0x6C35B6DBL}},{{0xC6CB7BF5L,0xDDB84DC5L},{5L,0x4F979AD0L},{0x870582C2L,0xCC386B3FL},{0x18C3F0FAL,(-1L)},{(-1L),0x7B6CC7DDL},{4L,0L},{0x7F180269L,0x7F180269L},{(-8L),(-1L)},{0x5657944BL,(-1L)},{1L,0x4DE5CE7AL}},{{0x26EDDCECL,1L},{5L,0x85ACB39AL},{5L,1L},{0x26EDDCECL,0x4DE5CE7AL},{1L,(-1L)},{0x5657944BL,(-1L)},{(-8L),0x7F180269L},{0x7F180269L,0L},{4L,0x7B6CC7DDL},{(-1L),(-1L)}}};
                float l_1328 = 0x7.8AD522p+29;
                int i, j, k;
                for (g_60 = 0; (g_60 <= 3); g_60 += 1)
                { /* block id: 581 */
                    int32_t * const l_1318 = &g_15[4][0][0];
                    int32_t *l_1323[7][8] = {{(void*)0,&l_1322,&l_1287,&l_1322,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1322,&g_80,&g_80,&l_1322,&l_1185,(void*)0,&l_1185,&l_1322},{&g_80,&l_1185,&g_80,(void*)0,&l_1287,&l_1287,(void*)0,&g_80},{&l_1185,&l_1185,&l_1287,(void*)0,&l_1131,(void*)0,&l_1287,&l_1185},{&l_1185,&g_80,(void*)0,&l_1287,&l_1287,(void*)0,&g_80,&l_1185},{&g_80,&l_1322,&l_1185,(void*)0,&l_1185,&l_1322,&g_80,&g_80},{&l_1322,(void*)0,(void*)0,(void*)0,(void*)0,&l_1322,&l_1287,&l_1322}};
                    int i, j;
                    for (g_13 = 0; (g_13 <= 3); g_13 += 1)
                    { /* block id: 584 */
                        int32_t **l_1320 = &g_189;
                        (*g_1319) = l_1318;
                        (*l_1320) = &p_10;
                        return l_1321;
                    }
                    g_1329--;
                }
            }
            else
            { /* block id: 591 */
                uint8_t l_1332[9][6][4] = {{{1UL,8UL,0xAAL,0x3DL},{0x5EL,0xEDL,9UL,0xAAL},{3UL,0x3DL,7UL,0x08L},{0x08L,0x04L,0x94L,1UL},{252UL,0x0CL,255UL,255UL},{0UL,0xE8L,249UL,0xC2L}},{{8UL,255UL,0UL,5UL},{253UL,0x5EL,251UL,255UL},{0x5EL,0UL,1UL,0x5FL},{9UL,0x22L,249UL,0x04L},{0x20L,252UL,0x20L,0xB1L},{0x22L,1UL,8UL,5UL}},{{0UL,0x05L,0x17L,1UL},{0xB1L,1UL,0x17L,251UL},{0UL,255UL,8UL,8UL},{0x22L,255UL,0x20L,0xAAL},{0x20L,0xAAL,249UL,0x20L},{9UL,0xA9L,1UL,246UL}},{{0x5EL,1UL,251UL,0x4CL},{253UL,255UL,0xC8L,0x17L},{0x94L,0x33L,0xF4L,246UL},{0x9AL,255UL,0xB1L,5UL},{0x22L,0xAAL,0x5EL,249UL},{0x5EL,0x26L,0x7EL,8UL}},{{1UL,0x22L,255UL,0xB5L},{7UL,1UL,0x20L,0xC8L},{255UL,246UL,255UL,5UL},{1UL,0xC8L,1UL,0x5EL},{0xB1L,252UL,0x5EL,0x5EL},{255UL,255UL,1UL,0x5FL}},{{0x9AL,246UL,0x20L,0xA9L},{1UL,0x5EL,0xC8L,0x20L},{0x46L,0x5EL,0xE8L,0xA9L},{0x5EL,246UL,0xFCL,0x5FL},{0x76L,255UL,249UL,0x5EL},{5UL,252UL,0xF4L,0x5EL}},{{0x22L,0xC8L,0x05L,5UL},{0x64L,246UL,0x17L,0xC8L},{0x5EL,1UL,0x16L,0xB5L},{0UL,0x22L,0x05L,8UL},{0x1BL,0x26L,0x20L,249UL},{5UL,0xAAL,0x29L,5UL}},{{9UL,255UL,0xFCL,246UL},{0xB1L,0x33L,251UL,0x17L},{0x46L,255UL,0UL,0x4CL},{0x94L,1UL,0x20L,246UL},{0x7FL,0xA9L,0xB1L,0x20L},{255UL,0xAAL,251UL,0xAAL}},{{0x5EL,255UL,1UL,8UL},{255UL,255UL,255UL,251UL},{255UL,1UL,0xF4L,1UL},{255UL,0x05L,255UL,5UL},{255UL,1UL,1UL,0xB1L},{0x5EL,252UL,251UL,0x04L}}};
                uint64_t **l_1336 = &l_1335;
                uint32_t ***l_1337 = &g_1297;
                uint16_t *****l_1344[3][1];
                int i, j, k;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1344[i][j] = &l_1311;
                }
                l_1338[4] = func_37(((*p_11) , (l_1332[5][4][0] ^ (l_1289 <= (safe_lshift_func_int8_t_s_u((((*l_1124) = &g_1297) == ((((*l_1336) = l_1335) != &g_207) , l_1337)), p_10))))), l_1186[1][2]);
                for (g_60 = 0; (g_60 <= 3); g_60 += 1)
                { /* block id: 597 */
                    int8_t *l_1343 = &g_36;
                    uint16_t ****l_1349 = (void*)0;
                    (*p_11) = ((safe_rshift_func_int8_t_s_s(l_1289, ((l_1186[3][6] ^= (((1UL < (0UL == (((safe_lshift_func_int8_t_s_s(((*l_1343) &= (*g_59)), p_10)) , (l_1344[0][0] != ((safe_add_func_uint8_t_u_u((l_1332[8][4][3] < ((*l_1195)++)), ((((*g_594) , l_1349) != l_1350[0]) && g_36))) , g_1354[0]))) > l_1359))) || l_1332[8][0][0]) | g_502[0][0])) | 0L))) || g_269[2][4][0]);
                }
            }
        }
        for (l_1188 = 3; (l_1188 >= 0); l_1188 -= 1)
        { /* block id: 607 */
            uint16_t l_1372 = 1UL;
            float *l_1375 = (void*)0;
            float *l_1376 = &g_564[1][4];
            uint8_t **l_1394 = &l_1393;
            uint32_t ** const ***l_1399 = (void*)0;
            uint32_t ** const *l_1401 = (void*)0;
            uint32_t ** const **l_1400 = &l_1401;
            int8_t *l_1403 = &g_36;
            int8_t **l_1411[7];
            int8_t ***l_1410 = &l_1411[4];
            int8_t **** const l_1409[8] = {&l_1410,&l_1410,&l_1410,&l_1410,&l_1410,&l_1410,&l_1410,&l_1410};
            int32_t l_1485 = (-7L);
            int32_t l_1489 = 0x9746917FL;
            int32_t l_1490 = 0xC7B0880FL;
            int32_t l_1495 = 0xFF55C531L;
            int32_t l_1496 = (-1L);
            int32_t l_1501 = 1L;
            int32_t l_1502 = 0x69A2B7AAL;
            int32_t l_1504 = 0xD294C418L;
            int32_t l_1508 = 0x4D2E428DL;
            int32_t l_1552 = (-10L);
            int32_t l_1553[10][7] = {{(-4L),(-4L),0L,0x70D9E897L,(-4L),0x786E9C13L,0x70D9E897L},{0x3FBABB9AL,0x40FBB789L,0x6C0A62FEL,(-5L),0x6C0A62FEL,0x40FBB789L,0x3FBABB9AL},{0x82AE9419L,0x70D9E897L,(-5L),0x82AE9419L,(-4L),(-5L),(-5L)},{0xEAD2D392L,(-5L),0x39162331L,(-5L),0xEAD2D392L,1L,0xEAD2D392L},{(-4L),0x82AE9419L,(-5L),0x70D9E897L,0x82AE9419L,0x82AE9419L,0x70D9E897L},{0x6C0A62FEL,(-5L),0x6C0A62FEL,0x40FBB789L,0x3FBABB9AL,0x40FBB789L,0x6C0A62FEL},{(-4L),0x70D9E897L,0L,(-4L),(-4L),0L,0x70D9E897L},{0xEAD2D392L,0x40FBB789L,1L,(-5L),1L,0x40FBB789L,0xEAD2D392L},{0x82AE9419L,(-4L),(-5L),(-5L),(-4L),0x82AE9419L,(-5L)},{0x3FBABB9AL,(-5L),2L,(-5L),0x3FBABB9AL,1L,0x3FBABB9AL}};
            int32_t l_1589 = 0xE106E6BAL;
            uint32_t *l_1590 = (void*)0;
            uint16_t *****l_1600 = &l_1350[0];
            int32_t l_1627 = (-10L);
            int32_t l_1650[8] = {(-4L),0xC0B28C8CL,0xC0B28C8CL,(-4L),0xC0B28C8CL,0xC0B28C8CL,(-4L),0xC0B28C8CL};
            const int32_t *l_1654 = &l_1186[0][2];
            const int32_t *l_1675 = &g_1676;
            const int32_t *l_1677[2][3] = {{&g_1676,&g_1676,&g_1676},{&g_1676,&g_1676,&g_1676}};
            const int32_t *l_1678 = (void*)0;
            const int32_t ** const l_1674[9] = {&l_1675,&l_1675,&l_1675,&l_1675,&l_1675,&l_1675,&l_1675,&l_1675,&l_1675};
            const int32_t ** const *l_1673 = &l_1674[5];
            const uint64_t *l_1704[5];
            uint32_t l_1732 = 0xE617AFA7L;
            int i, j;
            for (i = 0; i < 7; i++)
                l_1411[i] = (void*)0;
            for (i = 0; i < 5; i++)
                l_1704[i] = (void*)0;
        }
    }
    for (g_1182 = 1; (g_1182 >= 0); g_1182 -= 1)
    { /* block id: 727 */
        int32_t *l_1734[9][6] = {{&g_73,(void*)0,&l_1499[0][7][4],&l_1499[0][7][4],(void*)0,&g_73},{&l_1100,&g_73,&l_1499[0][7][4],&g_73,&l_1100,&l_1100},{(void*)0,&g_73,&g_73,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_73,&g_73,(void*)0},{&l_1100,&l_1100,&g_73,&l_1499[0][7][4],&g_73,&l_1100},{&g_73,(void*)0,&l_1499[0][7][4],&l_1499[0][7][4],(void*)0,&g_73},{&l_1100,&g_73,&l_1499[0][7][4],&g_73,&l_1100,&l_1100},{(void*)0,&g_73,&g_73,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_73,&g_73,(void*)0}};
        int8_t ***l_1740 = (void*)0;
        int8_t ****l_1739 = &l_1740;
        int32_t l_1760 = 0L;
        int64_t l_1766 = 0x78508024AC7EB023LL;
        uint32_t l_1810[1][8][1] = {{{0x200F92A0L},{0x200F92A0L},{0xC02038D7L},{0x200F92A0L},{0x200F92A0L},{0xC02038D7L},{0x200F92A0L},{0x200F92A0L}}};
        uint32_t *** const **l_1887 = (void*)0;
        float ***l_1908 = &g_624;
        const uint16_t l_1913 = 8UL;
        float l_1938 = (-0x1.6p-1);
        uint64_t l_1942 = 1UL;
        int32_t *l_1970[5][7] = {{(void*)0,&g_355,(void*)0,&g_355,(void*)0,&g_355,(void*)0},{(void*)0,&g_355,&g_355,(void*)0,&g_355,&g_355,&g_355},{&g_355,&g_355,&g_355,&g_355,&g_355,&g_355,&g_355},{&g_355,(void*)0,&g_355,&g_355,(void*)0,&g_355,&g_355},{(void*)0,&g_355,(void*)0,&g_355,(void*)0,&g_355,(void*)0}};
        const int64_t *l_1975 = &g_1182;
        const int64_t **l_1974 = &l_1975;
        uint32_t **l_1985[1][8][8] = {{{(void*)0,(void*)0,(void*)0,(void*)0,&g_773,&g_46,&g_46,(void*)0},{(void*)0,(void*)0,&g_773,&g_773,&g_773,&g_773,&g_46,&g_46},{&g_773,(void*)0,&g_46,&g_773,&g_773,&g_46,&g_46,(void*)0},{&g_46,(void*)0,&g_773,&g_773,&g_773,&g_773,&g_773,&g_773},{&g_46,&g_46,&g_46,&g_773,&g_773,&g_46,(void*)0,&g_46},{&g_773,&g_46,&g_773,(void*)0,&g_46,&g_46,&g_773,&g_773},{&g_773,&g_773,&g_46,(void*)0,&g_773,&g_46,(void*)0,(void*)0},{&g_46,&g_773,(void*)0,&g_773,&g_773,&g_773,&g_773,(void*)0}}};
        int16_t l_1987 = 0x9160L;
        uint8_t *l_2000[4] = {&g_148,&g_148,&g_148,&g_148};
        uint32_t l_2051 = 18446744073709551606UL;
        int i, j, k;
        ++l_1736;
        (*l_1739) = &g_1717;
        for (l_1287 = 0; (l_1287 <= 0); l_1287 += 1)
        { /* block id: 732 */
            int64_t *l_1761 = &g_269[0][0][1];
            uint8_t l_1764 = 0x30L;
            int32_t l_1767 = 0xDBE4A988L;
            int64_t **l_1821 = &g_986;
            int32_t l_1835[1];
            int8_t * const *l_1844[10][5][5] = {{{&l_1194,&g_1718,&g_1718,&g_1718,&l_1194},{&g_1718,(void*)0,&g_1718,&l_1194,(void*)0},{&l_1194,&l_1194,(void*)0,(void*)0,&l_1194},{&g_1718,&l_1194,(void*)0,&g_1718,&l_1194},{(void*)0,&l_1194,&l_1194,&l_1194,&l_1194}},{{&l_1194,(void*)0,&g_1718,&l_1194,&l_1194},{&l_1194,(void*)0,(void*)0,&g_1718,(void*)0},{&l_1194,&g_1718,(void*)0,(void*)0,&g_1718},{&l_1194,&l_1194,&l_1194,&l_1194,&g_1718},{(void*)0,&l_1194,&l_1194,&g_1718,&g_1718}},{{&g_1718,&g_1718,&g_1718,&l_1194,&l_1194},{(void*)0,&l_1194,&l_1194,&g_1718,&g_1718},{&l_1194,&l_1194,&g_1718,(void*)0,&l_1194},{&l_1194,&l_1194,(void*)0,&l_1194,&g_1718},{&l_1194,&g_1718,&l_1194,&l_1194,&g_1718}},{{&l_1194,&g_1718,&l_1194,&l_1194,(void*)0},{(void*)0,&l_1194,(void*)0,&g_1718,(void*)0},{&g_1718,&l_1194,&g_1718,&l_1194,&g_1718},{&l_1194,&g_1718,&l_1194,&l_1194,&l_1194},{&g_1718,&g_1718,&g_1718,&g_1718,&l_1194}},{{&l_1194,(void*)0,&l_1194,&g_1718,&l_1194},{&g_1718,&g_1718,&l_1194,(void*)0,&g_1718},{&l_1194,(void*)0,(void*)0,&l_1194,(void*)0},{&g_1718,&g_1718,&g_1718,&g_1718,&g_1718},{&l_1194,(void*)0,&l_1194,(void*)0,&g_1718}},{{&g_1718,(void*)0,&g_1718,&l_1194,(void*)0},{&g_1718,(void*)0,&g_1718,&l_1194,&l_1194},{&l_1194,&l_1194,&l_1194,&l_1194,&g_1718},{(void*)0,&g_1718,&g_1718,(void*)0,&g_1718},{&l_1194,&g_1718,(void*)0,&g_1718,(void*)0}},{{&l_1194,&g_1718,&g_1718,(void*)0,&l_1194},{&l_1194,(void*)0,&l_1194,&l_1194,&g_1718},{&l_1194,&l_1194,&g_1718,&l_1194,&l_1194},{&g_1718,&l_1194,&l_1194,&l_1194,&l_1194},{&l_1194,&g_1718,(void*)0,(void*)0,&l_1194}},{{&l_1194,&g_1718,(void*)0,&g_1718,&l_1194},{&g_1718,(void*)0,&g_1718,&g_1718,&g_1718},{&l_1194,&g_1718,&l_1194,(void*)0,&l_1194},{&l_1194,(void*)0,&g_1718,(void*)0,(void*)0},{&l_1194,&l_1194,&g_1718,&g_1718,&l_1194}},{{&l_1194,&l_1194,&g_1718,(void*)0,&l_1194},{(void*)0,&l_1194,&l_1194,&l_1194,&g_1718},{&l_1194,&g_1718,&g_1718,&g_1718,(void*)0},{&g_1718,&g_1718,(void*)0,&g_1718,&l_1194},{&g_1718,&l_1194,(void*)0,&l_1194,&l_1194}},{{&l_1194,(void*)0,&l_1194,&g_1718,(void*)0},{&g_1718,&g_1718,&g_1718,(void*)0,&g_1718},{&l_1194,&l_1194,&l_1194,&g_1718,&l_1194},{(void*)0,&l_1194,&g_1718,(void*)0,&l_1194},{&g_1718,&g_1718,(void*)0,&g_1718,(void*)0}}};
            uint32_t *l_1856 = &l_1810[0][5][0];
            int8_t *l_1858 = (void*)0;
            uint32_t l_1921[7][8][4] = {{{2UL,0x06C37F12L,0xE0084A6DL,0xEE0C52CAL},{0UL,0x98385610L,0x06C37F12L,0UL},{0x98385610L,7UL,18446744073709551611UL,18446744073709551615UL},{0x98385610L,18446744073709551612UL,0x06C37F12L,1UL},{0UL,18446744073709551615UL,0xE0084A6DL,0xE0084A6DL},{2UL,2UL,1UL,18446744073709551615UL},{18446744073709551615UL,0UL,1UL,7UL},{18446744073709551612UL,0x98385610L,0xE0084A6DL,1UL}},{{7UL,0x98385610L,18446744073709551612UL,7UL},{0x98385610L,0UL,18446744073709551612UL,18446744073709551615UL},{0x06C37F12L,2UL,0x06C37F12L,0xE0084A6DL},{7UL,18446744073709551615UL,18446744073709551615UL,1UL},{2UL,18446744073709551612UL,1UL,18446744073709551615UL},{0xFD43F63DL,7UL,1UL,0UL},{2UL,0x98385610L,0xEE0C52CAL,18446744073709551612UL},{0x06C37F12L,0xF8ADAE92L,0xF8ADAE92L,0x06C37F12L}},{{0xF8ADAE92L,0x06C37F12L,18446744073709551615UL,18446744073709551615UL},{0xFD43F63DL,0x578407F8L,18446744073709551615UL,1UL},{0x06C37F12L,18446744073709551615UL,1UL,1UL},{18446744073709551615UL,0x578407F8L,18446744073709551611UL,18446744073709551615UL},{0xE0084A6DL,0x06C37F12L,2UL,0x06C37F12L},{0x578407F8L,0xF8ADAE92L,1UL,18446744073709551612UL},{18446744073709551612UL,0xFD43F63DL,0xF8ADAE92L,18446744073709551612UL},{0xFD43F63DL,0x06C37F12L,0UL,0xE0084A6DL}},{{0xFD43F63DL,18446744073709551615UL,0xF8ADAE92L,1UL},{18446744073709551612UL,0xE0084A6DL,1UL,1UL},{0x578407F8L,0x578407F8L,2UL,0xE0084A6DL},{0xE0084A6DL,18446744073709551612UL,18446744073709551611UL,0x06C37F12L},{18446744073709551615UL,0xFD43F63DL,1UL,18446744073709551611UL},{0x06C37F12L,0xFD43F63DL,18446744073709551615UL,0x06C37F12L},{0xFD43F63DL,18446744073709551612UL,18446744073709551615UL,0xE0084A6DL},{0xF8ADAE92L,0x578407F8L,0xF8ADAE92L,1UL}},{{0x06C37F12L,0xE0084A6DL,0xEE0C52CAL,1UL},{0x578407F8L,18446744073709551615UL,18446744073709551611UL,0xE0084A6DL},{18446744073709551615UL,0x06C37F12L,18446744073709551611UL,18446744073709551612UL},{0x578407F8L,0xFD43F63DL,0xEE0C52CAL,18446744073709551612UL},{0x06C37F12L,0xF8ADAE92L,0xF8ADAE92L,0x06C37F12L},{0xF8ADAE92L,0x06C37F12L,18446744073709551615UL,18446744073709551615UL},{0xFD43F63DL,0x578407F8L,18446744073709551615UL,1UL},{0x06C37F12L,18446744073709551615UL,1UL,1UL}},{{18446744073709551615UL,0x578407F8L,18446744073709551611UL,18446744073709551615UL},{0xE0084A6DL,0x06C37F12L,2UL,0x06C37F12L},{0x578407F8L,0xF8ADAE92L,1UL,18446744073709551612UL},{18446744073709551612UL,0xFD43F63DL,0xF8ADAE92L,18446744073709551612UL},{0xFD43F63DL,0x06C37F12L,0UL,0xE0084A6DL},{0xFD43F63DL,18446744073709551615UL,0xF8ADAE92L,1UL},{18446744073709551612UL,0xE0084A6DL,1UL,1UL},{0x578407F8L,0x578407F8L,2UL,0xE0084A6DL}},{{0xE0084A6DL,18446744073709551612UL,18446744073709551611UL,0x06C37F12L},{18446744073709551615UL,0xFD43F63DL,1UL,18446744073709551611UL},{0x06C37F12L,0xFD43F63DL,18446744073709551615UL,0x06C37F12L},{0xFD43F63DL,18446744073709551612UL,18446744073709551615UL,0xE0084A6DL},{0xF8ADAE92L,0x578407F8L,0xF8ADAE92L,1UL},{0x06C37F12L,0xE0084A6DL,0xEE0C52CAL,1UL},{0x578407F8L,18446744073709551615UL,18446744073709551611UL,0xE0084A6DL},{18446744073709551615UL,0x06C37F12L,18446744073709551611UL,18446744073709551612UL}}};
            int32_t * const l_1971 = &g_355;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1835[i] = 0x725C1BA4L;
            (*g_736) = (void*)0;
            l_1767 = (l_1766 = (safe_sub_func_int16_t_s_s((safe_div_func_int8_t_s_s((safe_sub_func_int32_t_s_s((0x5BL <= (safe_rshift_func_uint16_t_u_u(((((((((*p_11) = (((***l_1740) &= ((safe_mod_func_uint64_t_u_u(p_10, ((safe_lshift_func_int16_t_s_s(((l_1510[0] != (((safe_add_func_int32_t_s_s(0x0DD08CEFL, (safe_mul_func_uint16_t_u_u((g_78[1] , l_1551[4]), (safe_unary_minus_func_uint16_t_u(((safe_sub_func_int64_t_s_s(((*l_1761) = l_1760), ((((void*)0 != g_931) | (*g_1402)) != (*g_460)))) && g_146))))))) , (void*)0) != (void*)0)) || l_1510[0]), g_1279)) || l_1762))) , p_10)) >= p_10)) , p_10) ^ p_10) , l_1551[0]) != 1UL) < g_1763[1][5]) > (*g_460)), l_1764))), 0xD78A65ACL)), l_1765)), (-6L))));
            for (g_864 = 0; (g_864 <= 1); g_864 += 1)
            { /* block id: 741 */
                int32_t **l_1768 = (void*)0;
                const uint16_t *** const l_1807 = (void*)0;
                uint32_t *l_1857 = (void*)0;
                int32_t l_1872 = 0xC550AFA5L;
                int32_t l_1873 = (-7L);
                int32_t l_1874 = 0xFE1167DAL;
                int32_t l_1875 = 1L;
                int32_t l_1878 = 0xD1210D7BL;
                int32_t l_1881 = 0x965D5E21L;
                int32_t l_1882 = 0xAA43698CL;
                int32_t l_1883 = (-1L);
                float l_1917[7][7][5] = {{{0x7.764609p-46,0x3.713ADBp+76,0x4.1CF176p-56,0x2.2BC40Fp+69,0xB.DEAC55p-16},{0x6.E03329p-62,0x4.7p-1,0x0.6p-1,0x3.FF7B74p+34,0x0.5p+1},{0x9.FFF6ADp-13,0xD.7824ECp-44,0x1.Dp+1,0x2.2BC40Fp+69,0x7.C329ADp+46},{0x9.0C1EF0p-1,0xF.999E11p+49,(-0x7.4p-1),0xD.87CF33p+79,0x7.B70259p-63},{0x7.Ap+1,0x0.5p-1,0x1.Fp+1,0xF.CDC2EAp-9,0x5.3DA6E0p+63},{0x1.Dp+1,(-0x3.Ep-1),0x8.6p-1,0x1.Fp+1,0x1.4p-1},{0x7.Ap+1,0xA.3E7BF6p-28,0xD.87CF33p+79,0x7.C329ADp+46,0xA.3E7BF6p-28}},{{(-0x1.1p+1),0x7.C329ADp+46,(-0x1.5p-1),0xA.548EE8p+83,0x9.796EA6p-74},{0x1.Fp+1,0xA.D2067Ep+58,0x0.7p+1,0x0.3p-1,0x5.A3297Bp+35},{0xD.7824ECp-44,0xF.105A1Ap-24,0x9.FFF6ADp-13,0x1.115EB1p-96,(-0x1.5p+1)},{0x0.679856p+76,(-0x1.Dp-1),0x1.4p-1,0x0.583B1Ep-62,0x5.529CA4p-64},{0xB.42A9E3p+15,0x0.B7F159p+25,(-0x1.Ap-1),0x6.E03329p-62,(-0x1.8p+1)},{0x7.Ap+1,0xB.F8D966p-51,(-0x1.2p-1),(-0x5.3p+1),0x7.Bp-1},{0x3.16756Ep+47,0xB.F8D966p-51,0x2.1p-1,0xF.198F90p+60,(-0x10.Dp-1)}},{{0x6.673175p-45,0x0.B7F159p+25,(-0x1.8p+1),0x3.FF7B74p+34,(-0x3.Ep-1)},{(-0x8.Bp-1),(-0x1.Dp-1),0x4.7p-1,0xE.BD55D8p-25,0x9.028898p+67},{(-0x1.8p+1),0xF.105A1Ap-24,0xA.548EE8p+83,(-0x1.8p+1),0xB.42A9E3p+15},{(-0x7.9p+1),0xA.D2067Ep+58,0x7.16554Dp-59,0x9.028898p+67,(-0x1.5p-1)},{(-0x9.Dp+1),0x7.C329ADp+46,0x0.Ap-1,0xF.CDC2EAp-9,0xB.F8D966p-51},{(-0x7.4p-1),0xA.3E7BF6p-28,0x2.1p-1,0xD.39D931p-29,0x0.6p-1},{0x7.C329ADp+46,(-0x3.Ep-1),0xA.D2067Ep+58,0x5.529CA4p-64,0x0.679856p+76}},{{0xE.BD55D8p-25,0xF.CDC2EAp-9,0x3.713ADBp+76,0xF.CDC2EAp-9,0xE.BD55D8p-25},{0xA.548EE8p+83,0xE.BD55D8p-25,0x1.4p-1,0x0.B7F159p+25,0x0.9p+1},{0x6.E03329p-62,0x3.16756Ep+47,0x0.0p+1,0x6.673175p-45,0x0.583B1Ep-62},{(-0x1.5p+1),0x1.Fp+1,(-0x3.4p-1),0xE.BD55D8p-25,0x0.9p+1},{0x7.85CB2Cp-59,0x6.673175p-45,(-0x1.5p-1),0x1.B2B8D7p+71,0xE.BD55D8p-25},{0x0.9p+1,0x2.89BD87p-35,0xF.E25EECp+7,(-0x10.Dp-1),0x0.679856p+76},{0x9.0984C7p+33,0x4.8F320Ap-69,0x1.7p+1,(-0x5.3p+1),0x0.6p-1}},{{(-0x1.5p-1),0x1.3p+1,0x1.9p-1,0x0.7p+1,0xB.F8D966p-51},{0x0.9p+1,0x2.1p-1,0xD.56EDEFp+85,0xF.105A1Ap-24,(-0x1.5p-1)},{0x0.3p-1,(-0x7.9p+1),0x6.Ap+1,0x1.115EB1p-96,0xB.42A9E3p+15},{0x0.7p-1,0x9.796EA6p-74,0xB.0C4B72p+32,0x7.85CB2Cp-59,0x9.028898p+67},{0x6.E03329p-62,0x1.967706p+13,0xF.198F90p+60,0x0.4p+1,(-0x3.Ep-1)},{0x0.4p+1,0x0.9p+1,0x1.Fp+1,0x7.C329ADp+46,(-0x10.Dp-1)},{0x0.0p+1,0x1.3p+1,(-0x1.8p+1),0x0.679856p+76,0x7.Bp-1}},{{0x7.C329ADp+46,0xD.56EDEFp+85,(-0x1.8p+1),0xC.22146Fp-93,(-0x1.8p+1)},{0x9.796EA6p-74,0x4.5p-1,0x1.Fp+1,0x9.796EA6p-74,0x5.529CA4p-64},{0x5.286E35p+16,0x6.673175p-45,0xF.198F90p+60,0x1.9p+1,(-0x1.5p+1)},{(-0x7.9p+1),0x0.679856p+76,0xB.0C4B72p+32,0xA.3E7BF6p-28,0x5.A3297Bp+35},{0x1.115EB1p-96,0x1.Dp+1,0x6.Ap+1,(-0x1.Dp-1),0x9.796EA6p-74},{(-0x3.Ep-1),0xE.BD55D8p-25,0xD.56EDEFp+85,(-0x7.4p-1),0xA.3E7BF6p-28},{0x6.673175p-45,0x1.Cp-1,0x1.9p-1,0xC.22146Fp-93,0x1.4p-1}},{{0x1.Dp+1,(-0x8.Bp-1),0x1.7p+1,0x1.Fp+1,0x0.6p-1},{0x0.Ap-1,0x0.679856p+76,0x4.234C28p-18,0x4.234C28p-18,0x0.679856p+76},{0x1.4p-1,0x4.234C28p-18,0x2.2BC40Fp+69,0xA.D2067Ep+58,(-0x9.7p+1)},{0x1.9p-1,0x3.16756Ep+47,(-0x7.9p+1),0xD.39D931p-29,0x0.7p+1},{0x0.5p+1,0xE.011A92p-25,0x7.10462Bp-28,0x7.764609p-46,0x0.B7F159p+25},{0x1.9p-1,0x9.6p-1,0x0.5p-1,0xE.011A92p-25,0x1.9p-1},{0x1.4p-1,0xD.52384Ep+63,0x6.673175p-45,0x1.967706p+13,(-0x1.1p+1)}}};
                uint8_t *l_1919[5];
                float ****l_1982 = (void*)0;
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_1919[i] = &l_1736;
                (*g_1769) = &p_10;
                for (g_13 = 0; (g_13 <= 3); g_13 += 1)
                { /* block id: 745 */
                    int8_t ** const ***l_1772 = &g_1770;
                    uint64_t *l_1777 = (void*)0;
                    uint64_t *l_1778 = &g_207;
                    int16_t *l_1784[2];
                    int32_t l_1813 = 1L;
                    int32_t l_1868 = 0x4907B7D4L;
                    int32_t l_1879 = 0xE1B34043L;
                    int32_t l_1880[10];
                    uint32_t l_1884 = 1UL;
                    uint32_t **l_1891 = &g_46;
                    uint32_t *** const l_1890 = &l_1891;
                    uint32_t *** const *l_1889 = &l_1890;
                    uint32_t *** const **l_1888 = &l_1889;
                    const int32_t **l_1923 = &g_460;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1784[i] = &g_172;
                    for (i = 0; i < 10; i++)
                        l_1880[i] = (-5L);
                    if ((((g_172 = (((((l_1629[(l_1287 + 3)][(g_864 + 1)][(l_1287 + 1)] , g_403[(g_1182 + 3)]) , ((*l_1772) = g_1770)) == &g_1771) <= (safe_mul_func_uint16_t_u_u(((g_1280 = (((safe_div_func_uint16_t_u_u(((((*l_1778) = g_403[(l_1287 + 3)]) | (g_403[(l_1287 + 2)] , (safe_unary_minus_func_uint64_t_u(g_162)))) ^ (safe_mul_func_uint16_t_u_u(p_10, (safe_rshift_func_int16_t_s_s(g_403[(l_1287 + 2)], 13))))), p_10)) , (*g_1670)) == (void*)0)) && (**g_1357)), p_10))) , g_60)) || 0xD731L) >= l_1736))
                    { /* block id: 750 */
                        int32_t l_1801[2];
                        float *l_1808 = &g_1809;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1801[i] = 9L;
                        (*g_1811) = (safe_div_func_float_f_f((safe_div_func_float_f_f((g_564[g_1182][(g_13 + 3)] > (safe_mul_func_float_f_f((safe_sub_func_float_f_f(0xF.B9312Bp+6, g_564[g_1182][(g_1182 + 1)])), ((safe_lshift_func_uint8_t_u_u(0xC7L, (*g_1402))) , (safe_add_func_float_f_f((((**g_624) = ((safe_div_func_float_f_f(((*g_127) > (safe_mul_func_float_f_f((l_1801[0] <= (safe_sub_func_float_f_f((safe_div_func_float_f_f(((*l_1808) = (((g_403[(g_1182 + 3)] > (((((*l_1473) = (+((void*)0 != l_1807))) > (**g_624)) == l_1241) == 0xA.F75D6Fp+45)) < l_1801[1]) >= p_10)), 0xD.4B502Bp+9)), 0x4.2471CFp+35))), p_10))), 0xA.27AE97p-96)) > g_403[(l_1287 + 2)])) <= 0x9.9F18CDp+58), p_10)))))), l_1810[0][5][0])), p_10));
                        if ((*p_11))
                            continue;
                        if (l_1629[2][2][1])
                            continue;
                        l_1499[(g_864 + 1)][g_1182][(g_1182 + 1)] &= (g_15[g_13][l_1287][l_1287] = 0L);
                    }
                    else
                    { /* block id: 759 */
                        const int32_t **l_1812 = &g_460;
                        (*l_1812) = func_37(((void*)0 == &g_1771), (g_265 = 0x81B77309L));
                        l_1813 &= l_1326[7][0];
                    }
                }
                for (g_80 = 0; (g_80 <= 6); g_80 += 1)
                { /* block id: 804 */
                    int8_t l_1932 = (-1L);
                    int32_t l_1933 = 1L;
                    int32_t l_1941[2][5][2] = {{{0L,0x153DA1EAL},{0xD3F9CD7BL,0x601FA74DL},{0xD3F9CD7BL,0x153DA1EAL},{0L,0xD3F9CD7BL},{0x153DA1EAL,0x601FA74DL}},{{0x2A02D06EL,0x2A02D06EL},{0L,0x2A02D06EL},{0x2A02D06EL,0x601FA74DL},{0x153DA1EAL,0xD3F9CD7BL},{0L,0x153DA1EAL}}};
                    int64_t ***l_1961[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j, k;
                    if ((safe_mul_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u((((((1L || 0x66510823194BEC60LL) & (safe_sub_func_int64_t_s_s((l_1510[0] <= ((**g_1717) = p_10)), (((l_1100 = (safe_rshift_func_uint16_t_u_u(((void*)0 != (*g_1357)), 7))) , ((*g_1595) != ((((g_1763[1][7]--) , &l_1740) == ((safe_add_func_uint32_t_u_u(p_10, (*p_11))) , &g_1771)) , (void*)0))) , p_10)))) , &l_1932) != &l_1932) , 0UL), p_10)), 0xBA48L)))
                    { /* block id: 808 */
                        if ((*p_11))
                            break;
                    }
                    else
                    { /* block id: 810 */
                        int32_t l_1939 = 1L;
                        int32_t l_1940 = 0x2141267BL;
                        int16_t *l_1951 = &l_1765;
                        int64_t *l_1957 = &l_1766;
                        uint32_t l_1958[10][5][5] = {{{4294967287UL,4294967295UL,4294967295UL,0x86795E30L,1UL},{4294967295UL,0x567ED9ABL,1UL,0xF9B5D3ACL,1UL},{1UL,1UL,0UL,4294967287UL,3UL},{4294967295UL,1UL,0UL,0x2BA0FCF7L,0x29E14FBEL},{4294967287UL,2UL,4294967287UL,0x272D3408L,4294967293UL}},{{0UL,1UL,4294967295UL,0xFF2E433CL,7UL},{0UL,1UL,1UL,0UL,4294967287UL},{1UL,0x567ED9ABL,4294967295UL,0x78463CA8L,0x27192E04L},{4294967295UL,4294967295UL,4294967287UL,4294967295UL,4294967295UL},{0x6530B10AL,0xFF2E433CL,0UL,0x78463CA8L,0xA4843FB5L}},{{0x23A970DEL,0x86795E30L,0UL,0UL,0x86795E30L},{0x29E14FBEL,0x49C65572L,1UL,0xFF2E433CL,0xA4843FB5L},{4294967295UL,0UL,4294967295UL,0x272D3408L,4294967295UL},{0xA4843FB5L,0xCC7414E6L,0x6530B10AL,0x2BA0FCF7L,0x27192E04L},{4294967295UL,3UL,0x23A970DEL,4294967287UL,4294967287UL}},{{0x29E14FBEL,0xE36406F1L,0x29E14FBEL,0xF9B5D3ACL,7UL},{0x23A970DEL,3UL,4294967295UL,0x86795E30L,4294967293UL},{0x6530B10AL,0xCC7414E6L,0xA4843FB5L,0x567ED9ABL,0x29E14FBEL},{4294967295UL,0UL,4294967295UL,4294967293UL,3UL},{1UL,0x49C65572L,0x29E14FBEL,0x49C65572L,1UL}},{{0UL,0x86795E30L,0x23A970DEL,4294967293UL,1UL},{0UL,0xFF2E433CL,0x6530B10AL,0x567ED9ABL,9UL},{4294967287UL,4294967295UL,4294967295UL,0x86795E30L,1UL},{4294967295UL,0x567ED9ABL,1UL,0xF9B5D3ACL,1UL},{1UL,1UL,0UL,4294967287UL,3UL}},{{4294967295UL,1UL,0UL,0x2BA0FCF7L,0x29E14FBEL},{4294967287UL,2UL,4294967287UL,0x272D3408L,4294967293UL},{0UL,4UL,4294967294UL,0x567ED9ABL,0x27192E04L},{1UL,0x272D3408L,0x272D3408L,1UL,0x86795E30L},{0UL,0xCC7414E6L,4294967294UL,1UL,1UL}},{{0x23A970DEL,2UL,0x86795E30L,2UL,0x23A970DEL},{0xA4843FB5L,0x567ED9ABL,0x29E14FBEL,1UL,4294967295UL},{4294967287UL,0UL,1UL,1UL,0UL},{9UL,0xE36406F1L,0UL,0x567ED9ABL,4294967295UL},{2UL,1UL,0x23A970DEL,4294967295UL,0x23A970DEL}},{{4294967295UL,0xF9B5D3ACL,0xA4843FB5L,0xFF2E433CL,1UL},{2UL,4294967295UL,4294967287UL,0x86795E30L,0x86795E30L},{9UL,0x78463CA8L,9UL,0x49C65572L,0x27192E04L},{4294967287UL,4294967295UL,2UL,0UL,3UL},{0xA4843FB5L,0xF9B5D3ACL,4294967295UL,0xCC7414E6L,9UL}},{{0x23A970DEL,1UL,2UL,3UL,4294967295UL},{0UL,0xE36406F1L,9UL,0xE36406F1L,0UL},{1UL,0UL,4294967287UL,3UL,0x272D3408L},{0x29E14FBEL,0x567ED9ABL,0xA4843FB5L,0xCC7414E6L,0x6530B10AL},{0x86795E30L,2UL,0x23A970DEL,0UL,0x272D3408L}},{{4294967294UL,0xCC7414E6L,0UL,0x49C65572L,0UL},{0x272D3408L,0x272D3408L,1UL,0x86795E30L,4294967295UL},{4294967294UL,4UL,0x29E14FBEL,0xFF2E433CL,9UL},{0x86795E30L,4294967293UL,0x86795E30L,4294967295UL,3UL},{0x29E14FBEL,4UL,4294967294UL,0x567ED9ABL,0x27192E04L}}};
                        int i, j, k;
                        l_1942--;
                        (*g_975) = &l_1941[1][2][1];
                        (*p_11) &= (p_10 || ((*l_1957) = ((safe_rshift_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_s(l_1949, 7)) || ((*l_1951) = ((safe_unary_minus_func_uint32_t_u(4294967294UL)) || ((*g_1670) == ((*l_1420) = (**g_1415)))))), (((safe_sub_func_uint64_t_u_u((p_11 == p_11), ((*l_1761) = ((((+0xE.C2D86Bp+90) != ((safe_sub_func_float_f_f((l_1287 >= l_1100), (-0x1.4p+1))) < p_10)) == l_1287) , l_1551[4])))) , p_10) || l_1941[0][0][0]))) | (-1L))));
                        l_1958[8][4][3]--;
                    }
                    for (g_312 = 1; (g_312 <= 6); g_312 += 1)
                    { /* block id: 822 */
                        int i, j;
                        l_1961[0] = (void*)0;
                        g_564[g_864][(g_1182 + 2)] = p_10;
                        return (*p_11);
                    }
                    if ((safe_mod_func_int32_t_s_s((p_10 && ((l_1932 , ((((safe_lshift_func_int16_t_s_s(((safe_mod_func_int64_t_s_s(((safe_lshift_func_int16_t_s_s(p_10, (((l_1970[1][6] = p_11) == l_1971) ^ (((safe_sub_func_int16_t_s_s(((((void*)0 == l_1974) != 0x037A38CC5BC7B926LL) && ((p_10 < 65535UL) == p_10)), l_1509)) != 0xED9687E5F993C4C3LL) | l_1289)))) ^ 0x6D5276D3CCC69FD7LL), p_10)) , 0xF3DEL), g_905)) , 8L) >= p_10) , p_10)) == 0UL)), g_172)))
                    { /* block id: 828 */
                        return (*g_208);
                    }
                    else
                    { /* block id: 830 */
                        int64_t l_1976 = 1L;
                        uint16_t l_1983 = 0UL;
                        (*g_1715) = (((l_1976 && l_1873) & (safe_lshift_func_int8_t_s_u((*g_1718), 7))) < ((**g_1319) < ((((p_10 , ((*g_1417) |= (safe_unary_minus_func_int32_t_s((safe_rshift_func_uint8_t_u_u((l_1983 |= (&g_1909 != l_1982)), (0xADEAL > (l_1976 , 1L)))))))) , g_605[3][5]) || g_60) <= 1UL)));
                        return (*p_11);
                    }
                }
            }
            for (l_1100 = 0; (l_1100 <= 0); l_1100 += 1)
            { /* block id: 840 */
                int32_t **l_1984 = &l_1734[(g_1182 + 5)][(l_1287 + 4)];
                (*l_1984) = (void*)0;
            }
        }
        if (((void*)0 != l_1985[0][0][3]))
        { /* block id: 844 */
            int32_t l_1988 = (-1L);
            int32_t l_1990 = 1L;
            uint8_t **l_2001 = (void*)0;
            uint8_t *l_2002 = (void*)0;
            uint32_t *l_2006 = (void*)0;
            uint32_t **l_2005 = &l_2006;
            int32_t **l_2008 = (void*)0;
            int32_t **l_2009 = &g_189;
            for (l_1185 = 1; (l_1185 >= 0); l_1185 -= 1)
            { /* block id: 847 */
                int32_t l_1986 = 0x90EA8FE5L;
                int32_t l_1989 = 4L;
                int32_t l_1991 = 0L;
                uint32_t l_1992 = 0x5B430BB3L;
                l_1992++;
                return (*g_594);
            }
            (*g_2007) = (((p_10 > l_1241) > (((p_10 >= 0x3.BB8C9Bp-40) >= ((0x1.Dp+1 < ((*l_1473) = (safe_add_func_float_f_f((safe_sub_func_float_f_f(((***l_1908) = ((&l_1736 != &l_1736) , (+(((*l_2005) = (((l_2002 = l_2000[1]) == (l_2003[2] = l_2000[0])) , &l_1099)) == (void*)0)))), l_1510[0])), 0x8.0p-1)))) > l_1988)) > l_1988)) > l_1990);
            (*l_2009) = &p_10;
            if ((**l_2009))
                break;
        }
        else
        { /* block id: 859 */
            uint32_t l_2022 = 0x402F87B4L;
            int32_t l_2023[10] = {0L,0L,0xE0C0BE3CL,0x9C1D2B8EL,0xE0C0BE3CL,0L,0L,0xE0C0BE3CL,0x9C1D2B8EL,0xE0C0BE3CL};
            uint64_t l_2024 = 0UL;
            float ** const *l_2028 = (void*)0;
            float ** const **l_2027 = &l_2028;
            int i;
            for (g_864 = 0; (g_864 <= 0); g_864 += 1)
            { /* block id: 862 */
                return (*g_460);
            }
            (*p_11) |= (safe_add_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(((((9UL != ((&g_1596 != &l_1350[0]) , (safe_add_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(6L, (l_1949 = ((l_2023[7] = (l_2022 = (0x90L >= (safe_sub_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((*g_1718), (*g_1402))), l_1189[0]))))) || (-5L))))), p_10)))) < l_1765) , (*g_1402)) , 0xE8L), l_1703)), l_2024));
            (*p_11) |= ((((safe_div_func_int32_t_s_s((0x7BAA16C1L >= (l_2027 == l_2029[2][5][2])), (safe_add_func_uint64_t_u_u((((safe_mul_func_int16_t_s_s((safe_mod_func_int16_t_s_s(g_1676, ((safe_mul_func_uint8_t_u_u(p_10, (p_10 ^ (safe_mul_func_uint16_t_u_u(((l_1629[3][4][4] <= (safe_div_func_uint8_t_u_u((*g_1402), (safe_mod_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((*g_714), 0x0DA0L)), 0xE0FEDD25L))))) > 0L), p_10))))) , p_10))), l_2022)) != p_10) > 0xEEA6FE3E0889C205LL), g_1281)))) && (*g_1715)) >= p_10) != l_1189[0]);
            --l_2046;
        }
        for (l_1703 = 0; (l_1703 <= 1); l_1703 += 1)
        { /* block id: 874 */
            uint16_t ** const l_2052 = &l_1353[0];
            int8_t *l_2061 = &g_36;
            int32_t l_2062 = 0xFC9516B2L;
            (*g_1596) = (void*)0;
            for (l_1509 = 0; (l_1509 <= 0); l_1509 += 1)
            { /* block id: 878 */
                int32_t **l_2063 = &l_1734[5][5];
                int i;
                (*l_2063) = &l_1760;
                (*g_208) = (safe_unary_minus_func_int32_t_s((*p_11)));
                return (*g_1616);
            }
            if ((*p_11))
                continue;
        }
    }
    return l_2065;
}


/* ------------------------------------------ */
/* 
 * reads : g_207 g_434 g_95 g_60 g_96 g_59 g_148 g_208 g_172 g_80 g_555 g_151 g_624 g_625 g_1094
 * writes: g_307 g_403 g_172 g_269 g_312 g_60 g_355 g_187 g_460 g_80 g_12 g_189
 */
static uint8_t  func_26(int8_t  p_27, const int32_t * p_28, uint16_t  p_29, const uint8_t  p_30, uint32_t  p_31)
{ /* block id: 177 */
    uint8_t l_424[1][6];
    uint16_t *l_428 = &g_307;
    int16_t *l_429 = &g_403[2];
    int16_t *l_430 = &g_172;
    int64_t *l_431 = &g_269[3][5][3];
    uint16_t *l_437 = (void*)0;
    uint16_t *l_438 = &g_312;
    int32_t l_511[2][6] = {{0x6C300E29L,0x6C300E29L,4L,0x6771D121L,4L,0x6C300E29L},{4L,(-6L),0x6771D121L,0x6771D121L,(-6L),4L}};
    const int32_t *l_582 = &g_187[0];
    float *l_622 = &g_564[0][0];
    float **l_621 = &l_622;
    int32_t *l_647 = &g_80;
    const uint32_t *l_668[9];
    const uint32_t **l_667 = &l_668[8];
    const uint32_t ***l_666[5][3][5] = {{{&l_667,&l_667,&l_667,&l_667,&l_667},{(void*)0,&l_667,(void*)0,(void*)0,(void*)0},{&l_667,&l_667,&l_667,&l_667,&l_667}},{{&l_667,(void*)0,&l_667,(void*)0,&l_667},{&l_667,&l_667,&l_667,&l_667,&l_667},{(void*)0,(void*)0,(void*)0,&l_667,(void*)0}},{{&l_667,&l_667,&l_667,&l_667,&l_667},{&l_667,&l_667,&l_667,&l_667,&l_667},{&l_667,&l_667,&l_667,&l_667,&l_667}},{{(void*)0,&l_667,(void*)0,(void*)0,(void*)0},{&l_667,&l_667,&l_667,&l_667,&l_667},{&l_667,(void*)0,&l_667,(void*)0,&l_667}},{{&l_667,&l_667,&l_667,&l_667,&l_667},{(void*)0,(void*)0,(void*)0,&l_667,(void*)0},{&l_667,&l_667,&l_667,&l_667,&l_667}}};
    const uint32_t ****l_665[9] = {&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3],&l_666[2][2][3]};
    const uint32_t ***** const l_664[3] = {&l_665[5],&l_665[5],&l_665[5]};
    uint64_t l_685 = 0xA71C57240283973CLL;
    int16_t l_702 = 0xCA86L;
    int16_t l_733 = 2L;
    uint16_t * const *l_747 = &l_438;
    uint16_t * const **l_746[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t **l_759 = (void*)0;
    uint16_t ***l_758 = &l_759;
    uint16_t ****l_757 = &l_758;
    int8_t *l_770[10] = {&g_60,&g_36,&g_60,&g_36,&g_60,&g_36,&g_60,&g_36,&g_60,&g_36};
    int32_t *l_862 = (void*)0;
    int32_t **l_861[7][3][7] = {{{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0}},{{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862}},{{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0}},{{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862}},{{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0}},{{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862}},{{&l_862,&l_862,(void*)0,(void*)0,&l_862,(void*)0,(void*)0},{&l_862,&l_862,&l_862,&l_862,&l_862,&l_862,&l_862},{&l_862,&l_862,(void*)0,(void*)0,&l_862,&l_862,&l_862}}};
    uint8_t l_873 = 0UL;
    int32_t l_907 = 1L;
    int32_t l_920[6][5][7] = {{{(-1L),(-6L),0xBE83A3D9L,(-1L),0L,0L,0x0964BBB1L},{(-1L),0x8EA7BC5DL,(-4L),0xCFBCAD9EL,0x90424362L,0xFF8109E3L,0L},{0xE05933FAL,2L,1L,0x7B63C3ECL,0x7B63C3ECL,1L,2L},{(-4L),0xA0EC2AE6L,0xCCCC8E2EL,(-1L),(-1L),0x8EA7BC5DL,0xE2AB8E7DL},{0L,0L,0x08BAACA4L,1L,(-3L),(-2L),0x7B63C3ECL}},{{0xE9E2FEB6L,3L,0L,(-1L),0xD2FBA04CL,0x893D3768L,(-9L)},{(-6L),0x7B63C3ECL,0x567FEAF0L,0x7B63C3ECL,(-6L),0x0964BBB1L,0x08BAACA4L},{0x62910DFCL,0xCF2E2378L,(-2L),0xCFBCAD9EL,0L,7L,(-1L)},{0L,(-3L),0x7AF5E72AL,(-1L),0xCC651270L,0L,0x8684959AL},{0x62910DFCL,0xCFBCAD9EL,(-4L),(-9L),0L,(-9L),(-4L)}},{{(-6L),(-6L),0L,0x24C72EC6L,0x08BAACA4L,(-1L),0x0964BBB1L},{0xE9E2FEB6L,0x01F70508L,(-1L),0L,0x90424362L,(-7L),0xD2FBA04CL},{0L,0xCC651270L,1L,0x7BEACD4DL,0x08BAACA4L,0xE05933FAL,0xCC651270L},{(-4L),0xAED8E6DFL,0x6D617326L,0xCC69B7DAL,0L,0x8EA7BC5DL,7L},{0xE05933FAL,0x08BAACA4L,0x7BEACD4DL,1L,0xCC651270L,0L,0xBE83A3D9L}},{{(-1L),3L,0x5776D7B2L,0x595CA045L,0L,0x97F3BB56L,(-9L)},{(-1L),0x08BAACA4L,0x24C72EC6L,0L,(-6L),(-6L),0L},{(-2L),0xAED8E6DFL,(-2L),0x8EA7BC5DL,0xD2FBA04CL,0x82611CBDL,0L},{0L,0xCC651270L,(-1L),0x7AF5E72AL,(-3L),0L,0x318ED5D5L},{0L,0x01F70508L,0xD2FBA04CL,(-9L),(-1L),0x82611CBDL,0x5776D7B2L}},{{0x0964BBB1L,(-6L),0x7B63C3ECL,0x567FEAF0L,0x7B63C3ECL,(-6L),0x0964BBB1L},{(-9L),0xCFBCAD9EL,0x6700B4A8L,0x01F70508L,0x90424362L,0x97F3BB56L,0xC80C70BFL},{(-2L),(-3L),1L,0x08BAACA4L,0L,0L,(-3L)},{(-4L),0xCF2E2378L,0x6700B4A8L,0x1BAAFF77L,(-1L),0x8EA7BC5DL,(-1L)},{1L,0x7B63C3ECL,0x7B63C3ECL,1L,2L,0xE05933FAL,0x7BEACD4DL}},{{1L,3L,0xD2FBA04CL,1L,(-4L),(-7L),(-9L)},{0L,0L,(-1L),0xBE83A3D9L,(-6L),(-1L),0L},{0x6700B4A8L,0x893D3768L,(-1L),(-9L),(-2L),0x595CA045L,(-9L)},{(-2L),0x0964BBB1L,0xCC651270L,(-3L),(-1L),(-2L),0L},{0x6D617326L,7L,0x62910DFCL,0x595CA045L,(-1L),0x1BAAFF77L,(-2L)}}};
    uint32_t l_998 = 7UL;
    int16_t l_1052[9] = {0x1EC1L,0x1EC1L,0x1EC1L,0x1EC1L,0x1EC1L,0x1EC1L,0x1EC1L,0x1EC1L,0x1EC1L};
    int32_t l_1079[4];
    int32_t **l_1093 = (void*)0;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
            l_424[i][j] = 254UL;
    }
    for (i = 0; i < 9; i++)
        l_668[i] = &g_502[1][4];
    for (i = 0; i < 4; i++)
        l_1079[i] = (-10L);
    if ((safe_add_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s((safe_sub_func_uint64_t_u_u(l_424[0][4], ((*l_431) = (!(g_207 > (((*l_429) = (safe_lshift_func_uint16_t_u_s(((*l_428) = 0UL), 13))) < (l_424[0][1] , ((*l_430) = (-1L))))))))), (safe_sub_func_uint16_t_u_u(p_30, g_434)))) ^ (safe_add_func_int8_t_s_s(((((*l_438) = 0x46B0L) , l_431) != l_431), 0xBBL))), (*g_95))))
    { /* block id: 183 */
        uint64_t l_447[2][5][6] = {{{0x8EC922509B706371LL,1UL,0x8EC922509B706371LL,0xEA9184F1F9B53A6DLL,0x82C67A7FA0BECA20LL,0x26FF3D3A7B81F921LL},{0UL,18446744073709551613UL,0x82C67A7FA0BECA20LL,0x6100B94CEAFF3A25LL,0xEA9184F1F9B53A6DLL,18446744073709551612UL},{0xEA9184F1F9B53A6DLL,0xBC03F2DFCA7302AALL,0x574AFD102C898199LL,0x6100B94CEAFF3A25LL,0x32BE8C5343160C5FLL,0xEA9184F1F9B53A6DLL},{0UL,18446744073709551615UL,0x99B1CD3BD6F87233LL,0xEA9184F1F9B53A6DLL,18446744073709551613UL,18446744073709551613UL},{0x8EC922509B706371LL,1UL,1UL,0x8EC922509B706371LL,0xCF659855A01E3A7DLL,18446744073709551612UL}},{{0x26FF3D3A7B81F921LL,2UL,0x8EC922509B706371LL,0x82C67A7FA0BECA20LL,0x1AD4E4BC3734FB01LL,0xAF6DE81C3EEFF37ALL},{18446744073709551612UL,18446744073709551613UL,18446744073709551615UL,0x574AFD102C898199LL,0x1AD4E4BC3734FB01LL,0xF6F0B6A1A720BD1CLL},{0xEA9184F1F9B53A6DLL,2UL,0xAC8259984276618FLL,0x99B1CD3BD6F87233LL,0xCF659855A01E3A7DLL,0xEA9184F1F9B53A6DLL},{18446744073709551613UL,1UL,0UL,1UL,18446744073709551613UL,18446744073709551606UL},{18446744073709551612UL,18446744073709551615UL,0x1AD4E4BC3734FB01LL,0x8EC922509B706371LL,0x32BE8C5343160C5FLL,0x28AB1C09BBEDB2E7LL}}};
        uint32_t ***l_454 = (void*)0;
        uint32_t ****l_453 = &l_454;
        const int32_t *l_457 = &g_187[0];
        int i, j, k;
        for (g_60 = 0; (g_60 != (-11)); g_60--)
        { /* block id: 186 */
            uint16_t **l_448 = &l_437;
            int32_t l_449 = 0x80D32DFFL;
            uint32_t **l_452[6][3] = {{&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46}};
            uint32_t ***l_451 = &l_452[5][0];
            uint32_t ****l_450 = &l_451;
            int32_t l_455 = 0x2FFAAB9AL;
            int32_t *l_456[4][5][2] = {{{&g_187[0],&l_455},{&g_187[0],&g_73},{&g_187[0],&g_73},{&g_187[0],&l_455},{&g_187[0],&g_73}},{{&g_187[0],&g_73},{&g_187[0],&l_455},{&g_187[0],&g_73},{&g_187[0],&g_73},{&g_187[0],&l_455}},{{&g_187[0],&g_73},{&g_187[0],&g_73},{&g_187[0],&l_455},{&g_187[0],&g_73},{&g_187[0],&g_73}},{{&g_187[0],&l_455},{&g_187[0],&g_73},{&g_187[0],&g_73},{&g_187[0],&l_455},{&g_187[0],&g_73}}};
            uint16_t l_458 = 0x83A1L;
            const int32_t **l_459[10][5] = {{&l_457,&l_457,&l_457,(void*)0,&l_457},{&l_457,(void*)0,&l_457,&l_457,&l_457},{&l_457,&l_457,(void*)0,&l_457,&l_457},{&l_457,&l_457,&l_457,(void*)0,&l_457},{&l_457,(void*)0,&l_457,&l_457,&l_457},{&l_457,&l_457,(void*)0,&l_457,&l_457},{&l_457,&l_457,&l_457,(void*)0,&l_457},{&l_457,(void*)0,&l_457,&l_457,&l_457},{&l_457,&l_457,&l_457,&l_457,(void*)0},{&l_457,&l_457,(void*)0,(void*)0,(void*)0}};
            int i, j, k;
            (*g_208) = (((*l_429) = (safe_add_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u((l_424[0][4] ^ p_30), (safe_lshift_func_int16_t_s_u((((((g_355 = (l_449 ^= (l_447[1][2][5] < ((((*l_448) = (void*)0) != &p_29) >= (*g_59))))) , l_450) != l_453) | (0x3D109C56L >= l_455)) , l_447[1][2][5]), p_31)))), 0xCB5690E7DD9E9FC1LL))) && g_148);
            l_457 = p_28;
            g_460 = func_37(l_458, g_172);
        }
    }
    else
    { /* block id: 195 */
        uint64_t l_461 = 0UL;
        float l_503 = (-0x1.2p+1);
        int32_t l_518 = 0x03BD73B9L;
        int32_t l_521 = 0xC63BF75CL;
        int32_t l_522[4];
        uint32_t **l_543 = &g_46;
        uint32_t ***l_542 = &l_543;
        uint32_t ****l_541 = &l_542;
        int32_t *l_657 = (void*)0;
        uint64_t *l_709 = &l_461;
        int32_t *l_724 = &l_511[1][2];
        int8_t *l_739[8];
        uint16_t l_754[3][6] = {{0x9101L,0x1D64L,65527UL,65535UL,65527UL,0x1D64L},{65527UL,0x9101L,0x4708L,0x33AEL,0x33AEL,0x4708L},{65527UL,65527UL,0x33AEL,65535UL,0x4728L,65535UL}};
        int8_t l_769 = (-7L);
        uint8_t l_799 = 0x15L;
        int64_t l_1035[5] = {(-9L),(-9L),(-9L),(-9L),(-9L)};
        uint16_t l_1036 = 65527UL;
        int64_t l_1039[2];
        int8_t l_1054 = 1L;
        const int32_t *l_1090 = (void*)0;
        int i, j;
        for (i = 0; i < 4; i++)
            l_522[i] = 0x3506005DL;
        for (i = 0; i < 8; i++)
            l_739[i] = (void*)0;
        for (i = 0; i < 2; i++)
            l_1039[i] = (-1L);
    }
    (*l_647) |= 0xC8C7BA59L;
    (**g_624) = (safe_mul_func_float_f_f(0x0.1p-1, (*g_555)));
    (*g_1094) = &l_511[1][2];
    return (*l_647);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int32_t * func_37(int32_t  p_38, uint32_t  p_39)
{ /* block id: 174 */
    uint16_t l_415 = 0UL;
    int32_t l_416[5][4] = {{0L,0L,0x26409F8CL,0L},{1L,0xB8501D77L,1L,0x26409F8CL},{1L,0x26409F8CL,0x26409F8CL,1L},{0L,0x26409F8CL,0L,0x26409F8CL},{0x26409F8CL,0xB8501D77L,0L,0L}};
    const int32_t *l_417 = &g_187[0];
    int i, j;
    l_416[3][3] &= l_415;
    return l_417;
}


/* ------------------------------------------ */
/* 
 * reads : g_59 g_60 g_47 g_13 g_78 g_79 g_96 g_73 g_80 g_127 g_146 g_162 g_175 g_176 g_46 g_186 g_189 g_187 g_148 g_207 g_208 g_209 g_213 g_252 g_272 g_230 g_95 g_269 g_265 g_12 g_360 g_307 g_403 g_411 g_413
 * writes: g_73 g_47 g_80 g_95 g_78 g_12 g_148 g_162 g_172 g_175 g_187 g_207 g_214 g_189 g_248 g_265 g_269 g_60 g_307 g_312 g_151
 */
static float  func_43(uint32_t * p_44, uint8_t  p_45)
{ /* block id: 4 */
    uint32_t l_61 = 0x2CAEABA4L;
    int32_t *l_72 = &g_73;
    int32_t **l_412 = (void*)0;
    (*g_413) = func_48((safe_mul_func_uint16_t_u_u(p_45, (func_56(g_59, l_61) < ((((safe_rshift_func_int16_t_s_s((p_45 & (safe_add_func_uint64_t_u_u((p_45 , (l_61 > (safe_mul_func_int8_t_s_s(0xE5L, func_56(func_68(g_47, l_72, g_13), g_13))))), g_13))), g_13)) >= p_45) || (*l_72)) == g_13)))), &l_61, &g_60, g_46, g_60);
    return (*l_72);
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_78 g_186 g_189 g_187 g_148 g_60 g_59 g_47 g_207 g_208 g_209 g_213 g_162 g_252 g_96 g_272 g_230 g_95 g_269 g_13 g_265 g_127 g_12 g_360 g_307 g_403 g_411 g_80
 * writes: g_80 g_73 g_187 g_12 g_148 g_207 g_214 g_172 g_189 g_47 g_248 g_265 g_269 g_60 g_307 g_312 g_151
 */
static int32_t * func_48(uint32_t  p_49, uint32_t * p_50, int8_t * p_51, uint32_t * p_52, int64_t  p_53)
{ /* block id: 67 */
    const uint16_t l_184 = 0x2292L;
    int32_t l_219[3][6] = {{0x18D4A032L,0L,0x18D4A032L,0x18D4A032L,0L,0x18D4A032L},{0x18D4A032L,0L,0x18D4A032L,0x18D4A032L,0L,0x18D4A032L},{0x18D4A032L,0L,0x18D4A032L,0x18D4A032L,0L,0x18D4A032L}};
    uint8_t l_232 = 0x33L;
    int32_t l_271 = 0L;
    int32_t **l_323 = &g_189;
    uint32_t *l_332 = &g_265;
    uint32_t l_335 = 0x70C95A2DL;
    int16_t *l_359 = &g_172;
    int8_t *l_380 = &g_60;
    int8_t **l_379 = &l_380;
    int16_t *l_402[1];
    uint32_t * const *l_409 = &g_46;
    uint32_t * const **l_408[8] = {&l_409,&l_409,&l_409,&l_409,&l_409,&l_409,&l_409,&l_409};
    uint32_t * const ***l_407 = &l_408[7];
    uint32_t * const ****l_406 = &l_407;
    float l_410 = 0x5.8p-1;
    int i, j;
    for (i = 0; i < 1; i++)
        l_402[i] = &g_403[2];
lbl_343:
    for (g_80 = 3; (g_80 >= (-16)); --g_80)
    { /* block id: 70 */
        uint64_t l_205 = 0x70E512B8E932EB66LL;
        int32_t l_222 = (-8L);
        int32_t l_223 = 1L;
        int32_t l_224 = (-1L);
        int32_t l_225[10][6][2] = {{{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL},{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL},{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL}},{{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL},{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL},{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL}},{{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL},{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL},{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL}},{{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL},{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL},{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL}},{{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL},{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL},{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL}},{{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL},{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL},{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL}},{{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL},{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL},{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL}},{{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL},{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL},{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL}},{{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL},{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL},{0L,0xF839FAA1L},{0x61F2928FL,0x61F2928FL}},{{(-9L),0xFCA7E3DBL},{(-9L),0x61F2928FL},{0x61F2928FL,0xF839FAA1L},{0L,0x61F2928FL},{0xCA4CA271L,0xFCA7E3DBL},{0xCA4CA271L,0x61F2928FL}}};
        int32_t *l_237 = &l_223;
        uint32_t l_315[10];
        uint32_t *l_317[8][8][4] = {{{&l_315[5],&g_265,&g_265,(void*)0},{(void*)0,&l_315[5],&l_315[5],&g_265},{&l_315[5],&g_265,&g_265,&l_315[5]},{&l_315[5],&l_315[5],&l_315[5],&l_315[5]},{(void*)0,(void*)0,&g_265,&g_265},{&l_315[5],&g_265,(void*)0,(void*)0},{(void*)0,&l_315[5],&l_315[5],(void*)0},{&g_265,&g_265,&l_315[5],&g_265}},{{&l_315[5],(void*)0,&l_315[5],&l_315[5]},{(void*)0,&l_315[5],&g_265,&l_315[5]},{&g_265,&g_265,&g_265,&g_265},{(void*)0,&l_315[5],&l_315[5],(void*)0},{&l_315[5],&g_265,&l_315[5],&l_315[5]},{&g_265,(void*)0,&l_315[5],&g_265},{(void*)0,(void*)0,(void*)0,&l_315[5]},{&l_315[5],&g_265,&g_265,(void*)0}},{{(void*)0,&l_315[5],&l_315[5],&g_265},{&l_315[5],&g_265,&g_265,&l_315[5]},{&l_315[5],&l_315[5],&l_315[5],&l_315[5]},{(void*)0,(void*)0,&g_265,&g_265},{&l_315[5],&g_265,(void*)0,(void*)0},{(void*)0,&l_315[5],&l_315[5],(void*)0},{&g_265,&g_265,&l_315[5],&g_265},{&l_315[5],(void*)0,&l_315[5],&l_315[5]}},{{(void*)0,&l_315[5],&g_265,&l_315[5]},{&g_265,&g_265,&g_265,&g_265},{(void*)0,&l_315[5],&l_315[5],(void*)0},{&l_315[5],&g_265,&l_315[5],&l_315[5]},{&g_265,(void*)0,&l_315[5],&g_265},{(void*)0,(void*)0,(void*)0,&l_315[5]},{&l_315[5],&g_265,&g_265,(void*)0},{(void*)0,&l_315[5],&l_315[5],&g_265}},{{&l_315[5],&g_265,&g_265,&l_315[5]},{&l_315[5],&l_315[5],&l_315[5],&l_315[5]},{(void*)0,(void*)0,&g_265,&g_265},{&l_315[5],&g_265,(void*)0,(void*)0},{(void*)0,&l_315[5],&l_315[5],(void*)0},{&g_265,&g_265,&l_315[5],&g_265},{&l_315[5],(void*)0,&l_315[5],&l_315[5]},{(void*)0,&l_315[5],&g_265,&l_315[5]}},{{&g_265,&g_265,&g_265,&g_265},{(void*)0,&l_315[5],&l_315[5],(void*)0},{&l_315[5],&g_265,&l_315[5],&l_315[5]},{&g_265,(void*)0,&l_315[5],&g_265},{(void*)0,(void*)0,(void*)0,&l_315[5]},{&l_315[5],&g_265,&g_265,(void*)0},{&l_315[5],&l_315[5],&g_265,&g_265},{&g_265,&l_315[5],&l_315[5],&g_265}},{{&g_265,&g_265,&g_265,&g_265},{&l_315[5],&l_315[5],&g_265,&l_315[5]},{&g_265,&l_315[5],&g_265,(void*)0},{&g_265,&l_315[5],&g_265,(void*)0},{&l_315[1],&l_315[5],(void*)0,&l_315[5]},{&g_265,&l_315[5],&g_265,&g_265},{&g_265,&g_265,&g_265,&g_265},{&l_315[5],&l_315[5],&g_265,&g_265}},{{&g_265,&l_315[5],&g_265,(void*)0},{&g_265,(void*)0,(void*)0,&g_265},{&l_315[1],&l_315[5],&g_265,&l_315[1]},{&g_265,&l_315[5],&g_265,&g_265},{&g_265,(void*)0,&g_265,(void*)0},{&l_315[5],&l_315[5],&g_265,&g_265},{&g_265,&l_315[5],&l_315[5],&g_265},{&g_265,&g_265,&g_265,&g_265}}};
        int i, j, k;
        for (i = 0; i < 10; i++)
            l_315[i] = 0xF10EE17EL;
        if ((l_184 >= 1L))
        { /* block id: 71 */
            for (g_73 = 4; (g_73 >= 0); g_73 -= 1)
            { /* block id: 74 */
                float *l_188 = &g_12;
                int i;
                (*g_186) = g_78[g_73];
                for (p_53 = 1; (p_53 <= 4); p_53 += 1)
                { /* block id: 78 */
                    return p_52;
                }
                (*l_188) = g_78[g_73];
            }
            return g_189;
        }
        else
        { /* block id: 84 */
            uint8_t *l_200 = &g_148;
            uint64_t *l_206 = &g_207;
            volatile uint32_t ***l_212 = &g_210;
            volatile uint32_t ****l_211[4][6] = {{(void*)0,&l_212,&l_212,(void*)0,(void*)0,&l_212},{(void*)0,(void*)0,&l_212,&l_212,(void*)0,(void*)0},{(void*)0,&l_212,&l_212,(void*)0,(void*)0,&l_212},{(void*)0,(void*)0,&l_212,&l_212,(void*)0,(void*)0}};
            int32_t l_217 = 0x027B1E6FL;
            int32_t l_220 = 0xFC4EB74DL;
            int32_t l_221[6];
            int64_t l_228 = 0x89AEFA68B3BC1160LL;
            int32_t l_229[10][6] = {{(-9L),(-2L),(-9L),0x0B716481L,0x7C0F1330L,1L},{7L,0x0B716481L,(-2L),1L,1L,1L},{1L,1L,1L,1L,(-2L),0x0B716481L},{7L,1L,0x7C0F1330L,0x0B716481L,(-9L),(-2L)},{(-9L),(-2L),0x37C8973BL,(-2L),(-9L),0x7457197BL},{(-2L),1L,9L,(-1L),(-2L),0x7C0F1330L},{0x7C0F1330L,1L,1L,1L,1L,0x7C0F1330L},{(-1L),0x0B716481L,9L,(-9L),0x7C0F1330L,0x7457197BL},{1L,(-2L),0x37C8973BL,0x7C0F1330L,0x37C8973BL,(-2L)},{1L,0x7457197BL,0x7C0F1330L,(-9L),9L,0x0B716481L}};
            int16_t l_231 = (-1L);
            int32_t *l_238 = &g_187[0];
            int i, j;
            for (i = 0; i < 6; i++)
                l_221[i] = 0xB68F5B90L;
            (*g_208) = (safe_mul_func_uint16_t_u_u(65529UL, (g_187[0] ^ (((*l_206) &= (p_53 , (((safe_lshift_func_int8_t_s_u((-8L), (((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((p_50 != (((*l_200)--) , &g_47)), 1)), (*p_51))) & ((p_53 , ((((safe_sub_func_int32_t_s_s((l_184 | 0xA769E6F5A32C323CLL), l_184)) , 0x243EL) > (-1L)) || l_205)) & l_184)), (*g_59))) , 0UL) >= p_49))) || g_47) > 0x5930L))) <= p_49))));
            (*g_213) = g_209;
            if (p_49)
            { /* block id: 89 */
                int8_t l_218 = (-5L);
                int32_t l_226 = 0x19914BD5L;
                int32_t l_227[5][3][9] = {{{6L,0x945547A3L,0L,0L,1L,0xBE899FA3L,0x6829673AL,0L,0x07C21F15L},{0x6829673AL,1L,1L,1L,0xC5DCDB80L,0x319FEF01L,0x7112B941L,0x2A9956A4L,0L},{0xC1CF444AL,0x8DB606F9L,(-1L),0xCCD35490L,1L,6L,(-1L),1L,1L}},{{0x070E6CD8L,0xB0013EDCL,(-1L),0xC5DCDB80L,0x026D30E9L,0x026D30E9L,0xC5DCDB80L,(-9L),1L},{(-1L),0xBE899FA3L,0xCCD35490L,(-9L),0xFFCAFF29L,4L,1L,0xC1CF444AL,(-1L)},{1L,4L,0x8608FC4CL,0x2A9956A4L,(-2L),0x4B5F3555L,0x6829673AL,(-4L),0x7112B941L}},{{4L,0xBE899FA3L,0x7112B941L,0L,6L,0x026D30E9L,(-10L),1L,0x07C21F15L},{0x38940A22L,1L,0L,0xEC96D48FL,4L,0x07C21F15L,0xC5DCDB80L,0x2ECA9E05L,(-9L)},{0x38940A22L,(-2L),(-1L),0x07C21F15L,0x07C21F15L,(-1L),(-2L),0x38940A22L,0x319FEF01L}},{{4L,1L,1L,(-10L),0xC1CF444AL,(-1L),0x38940A22L,0L,0x6829673AL},{1L,6L,(-4L),0L,0x8DB606F9L,(-9L),0x070E6CD8L,1L,0x319FEF01L},{(-1L),(-1L),0xBE899FA3L,0x0EC3E438L,0x2ECA9E05L,0L,6L,(-1L),(-9L)}},{{0xB0013EDCL,0x2ECA9E05L,0x4B5F3555L,1L,0x0EC3E438L,(-1L),0x07C21F15L,(-1L),0x07C21F15L},{(-4L),0x8608FC4CL,0x945547A3L,0x945547A3L,0x8608FC4CL,(-4L),0xCCD35490L,1L,0x7112B941L},{0x026D30E9L,0xFFCAFF29L,0xB0013EDCL,0x8DB606F9L,(-1L),0xC5DCDB80L,1L,0L,(-1L)}}};
                int i, j, k;
                for (p_53 = 0; (p_53 <= 0); p_53 += 1)
                { /* block id: 92 */
                    int32_t *l_215 = &g_187[0];
                    int32_t *l_216[10][2] = {{(void*)0,&g_73},{&g_73,(void*)0},{&g_73,&g_73},{(void*)0,&g_73},{&g_73,(void*)0},{&g_73,&g_73},{(void*)0,&g_73},{&g_73,(void*)0},{&g_73,&g_73},{(void*)0,&g_73}};
                    int i, j;
                    for (g_172 = 0; (g_172 <= 0); g_172 += 1)
                    { /* block id: 95 */
                        return &g_80;
                    }
                    l_232--;
                    l_221[4] |= ((*l_215) = (0L ^ 0x18E70DCBL));
                }
            }
            else
            { /* block id: 102 */
                int32_t **l_236 = &g_189;
                (*l_236) = p_50;
                l_238 = l_237;
            }
        }
        for (p_49 = 8; (p_49 == 40); p_49 = safe_add_func_uint64_t_u_u(p_49, 5))
        { /* block id: 109 */
            const uint32_t * const l_246[10] = {&g_78[1],&g_78[1],&g_78[1],&g_78[1],&g_78[1],&g_78[1],&g_78[1],&g_78[1],&g_78[1],&g_78[1]};
            const uint32_t * const *l_245 = &l_246[0];
            const uint32_t * const **l_244[1];
            const uint32_t * const *** const l_243 = &l_244[0];
            int32_t l_266 = 0x53172575L;
            int32_t l_267 = 0x0B8DA7C8L;
            uint64_t * const l_308 = (void*)0;
            int i;
            for (i = 0; i < 1; i++)
                l_244[i] = &l_245;
            for (g_47 = 0; (g_47 < 45); g_47++)
            { /* block id: 112 */
                const uint32_t * const ****l_250[6][1];
                int32_t **l_263 = &l_237;
                uint32_t *l_264 = &g_265;
                int64_t *l_268 = &g_269[3][5][3];
                int32_t *l_270 = &l_219[1][0];
                int i, j;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_250[i][j] = &g_248;
                }
                (*g_252) = (g_162 , l_243);
                (*g_272) &= ((((0xB3CF3B66BF3DB485LL & (((*l_270) = (((*p_51) = (safe_div_func_int16_t_s_s((((*l_268) = (((safe_sub_func_uint8_t_u_u(((0x28338B1BL == (p_49 , (safe_lshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((p_51 == p_51), (safe_mul_func_int16_t_s_s(((((*l_264) = (((((((*l_237) ^= (*p_51)) == 1UL) >= (*g_59)) | ((((l_263 == &g_208) , 0x01269EAFC1F1D460LL) && 0UL) | g_60)) , p_53) ^ 0xAAB4L)) || 0xA3583BD0L) <= l_266), g_60)))), p_49)))) && 0xD94905E8L), 0x1BL)) & 7UL) | l_267)) < 0L), p_49))) && g_96)) || l_271)) >= 0xE1444969DDCA7EE9LL) & 0xC5541E83EE87846DLL) == g_148);
                for (g_60 = 6; (g_60 < (-2)); g_60--)
                { /* block id: 122 */
                    uint64_t l_305 = 0xFE6CDB4F64149D98LL;
                    int32_t l_313 = 0x92CEC704L;
                    int32_t l_316[3][10] = {{(-7L),0x1F6C3278L,0xDB97F7C6L,0xDB97F7C6L,0x1F6C3278L,(-7L),(-1L),1L,2L,1L},{0xFCBE30F3L,0xDB97F7C6L,2L,0xA6476524L,2L,0xDB97F7C6L,0xFCBE30F3L,(-1L),8L,8L},{0xFCBE30F3L,8L,(-7L),0x76960FFFL,0x76960FFFL,(-7L),8L,0xFCBE30F3L,0x1F6C3278L,(-1L)}};
                    int i, j;
                    for (l_232 = 0; (l_232 < 21); ++l_232)
                    { /* block id: 125 */
                        float *l_292[6][10] = {{&g_151,&g_12,(void*)0,(void*)0,&g_12,&g_151,&g_151,&g_12,(void*)0,&g_151},{(void*)0,&g_12,&g_151,&g_12,&g_12,(void*)0,&g_12,&g_12,&g_151,&g_12},{(void*)0,&g_151,(void*)0,&g_12,&g_151,&g_151,&g_12,(void*)0,(void*)0,&g_12},{&g_12,&g_12,&g_151,&g_151,&g_12,&g_12,&g_151,&g_151,&g_151,&g_151},{(void*)0,&g_12,&g_12,&g_151,&g_12,&g_12,&g_12,&g_151,&g_12,&g_12},{(void*)0,&g_151,&g_151,&g_151,&g_151,&g_12,&g_12,&g_151,&g_151,&g_12}};
                        uint16_t *l_306 = &g_307;
                        int32_t l_309 = 1L;
                        uint16_t *l_310 = (void*)0;
                        uint16_t *l_311 = &g_312;
                        const int32_t l_314[3][5][10] = {{{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L}},{{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L}},{{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L},{0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L,0x6BCE59C2L,1L}}};
                        int i, j, k;
                        (**l_263) = (safe_mul_func_uint16_t_u_u(((safe_add_func_int8_t_s_s(((g_230 != ((l_316[0][8] ^= (~(((safe_div_func_int16_t_s_s((safe_mul_func_int16_t_s_s((((0x65L | ((((safe_sub_func_float_f_f((safe_sub_func_float_f_f((l_271 = ((safe_sub_func_float_f_f((l_219[1][0] = 0x4.Bp+1), p_49)) == p_49)), (((((((safe_rshift_func_uint16_t_u_s((l_313 = ((safe_lshift_func_uint16_t_u_s(((*l_311) = (safe_lshift_func_int16_t_s_s(((((l_309 = (safe_rshift_func_uint8_t_u_s((((safe_sub_func_int16_t_s_s(1L, ((*l_306) = (safe_rshift_func_int8_t_s_s(l_305, 3))))) , ((l_308 == l_268) > (*g_95))) > 7UL), 5))) && 0xABL) || g_269[3][5][3]) != p_49), 0))), 13)) , 65534UL)), 15)) != g_96) ^ g_187[0]) > (**l_263)) <= p_53) , l_309) == 0xE.3811A5p-9))), 0xB.5B0CBCp-57)) , 0xFAL) | (-10L)) , l_219[1][1])) ^ 0x8089AC56L) > p_53), 0x7381L)), g_13)) <= l_314[1][2][3]) < l_315[5]))) || g_78[3])) < p_53), 0xFBL)) >= 0xBA66C2B61E4DACD8LL), (*l_237)));
                        return p_52;
                    }
                }
            }
        }
        if (l_232)
            continue;
        if ((7L == (((++g_265) || ((safe_add_func_int8_t_s_s(((!((((*l_237) , l_323) == (void*)0) , (*l_237))) <= ((((*l_323) = p_50) == &l_224) <= (l_225[3][3][1] |= 1L))), 253UL)) != 9L)) != p_49)))
        { /* block id: 143 */
            float l_324 = 0x4.B9505Cp-76;
            int32_t l_325 = (-8L);
            if (p_53)
                break;
            l_325 = p_49;
            return p_52;
        }
        else
        { /* block id: 147 */
            float *l_330 = &g_12;
            int32_t l_331 = 1L;
            float *l_338 = (void*)0;
            float *l_339 = (void*)0;
            l_331 = ((safe_sub_func_float_f_f((safe_div_func_float_f_f(((*l_330) = (*g_127)), l_331)), (l_332 == &g_265))) < (l_331 > (safe_mul_func_float_f_f((((l_335 , (-1L)) < (safe_mod_func_uint32_t_u_u(g_47, l_331))) , g_47), 0x1.0p+1))));
        }
    }
    if ((safe_lshift_func_int8_t_s_s((&g_269[2][1][0] != &p_53), 1)))
    { /* block id: 152 */
        if (g_80)
            goto lbl_343;
    }
    else
    { /* block id: 154 */
        uint32_t l_353 = 0x79D9A955L;
        int32_t *l_354[1];
        uint64_t * const l_358 = &g_207;
        int32_t l_384 = 5L;
        int32_t *l_385 = &g_187[0];
        int32_t l_386 = (-1L);
        int32_t *l_387[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint32_t l_388 = 0x845BA051L;
        int i;
        for (i = 0; i < 1; i++)
            l_354[i] = &g_355;
        (*g_360) = (p_53 , (((((((((safe_mul_func_int16_t_s_s(0x0F99L, (((((safe_unary_minus_func_uint16_t_u((safe_mod_func_int32_t_s_s((((g_60 = (((((safe_div_func_uint8_t_u_u(0xE8L, ((((l_219[1][0] = ((&p_51 == (void*)0) != (safe_div_func_int32_t_s_s(l_353, (*g_208))))) , g_265) || (((((((((safe_lshift_func_int16_t_s_u(p_49, g_207)) >= 0UL) && g_96) || 3L) > g_78[1]) , l_358) == &g_207) | 0L) ^ g_162)) & 1UL))) ^ l_353) , &g_269[3][2][0]) == &p_53) , (*g_59))) , l_359) != l_359), l_232)))) > p_53) , g_207) < l_232) | 18446744073709551615UL))) < p_49) <= p_53) >= (-1L)) == p_49) && g_162) , p_53) == g_12) <= 0x0.0p-1));
        for (g_73 = 0; (g_73 <= 9); g_73 = safe_add_func_uint16_t_u_u(g_73, 7))
        { /* block id: 160 */
            float *l_368 = &g_151;
            int32_t l_383 = 0x85B17A32L;
            l_384 = (-(safe_mul_func_float_f_f((((((safe_sub_func_float_f_f(0xB.DB1773p-36, ((*l_368) = (*g_127)))) == p_49) < (safe_sub_func_float_f_f(((0x0.CEDB90p+46 == (safe_mul_func_float_f_f((safe_mul_func_float_f_f(((safe_sub_func_float_f_f((-0x2.1p+1), 0x5.631744p-23)) == (safe_sub_func_float_f_f(p_49, ((l_379 != ((safe_rshift_func_uint16_t_u_u(1UL, l_383)) , (void*)0)) < g_47)))), g_187[0])), g_96))) == 0x0.731285p-8), 0x4.549141p-81))) == p_49) >= 0x7.853708p+66), l_353)));
        }
        ++l_388;
    }
    (*g_411) = (safe_div_func_uint64_t_u_u(((safe_sub_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s((g_307 &= (safe_add_func_int16_t_s_s((p_53 >= (+((l_219[1][4] = (((g_269[3][2][2] | (!(-3L))) < ((*l_359) = (~0xDEL))) != (*g_208))) , ((p_49 , l_359) != &g_403[2])))), (safe_mul_func_uint16_t_u_u(((void*)0 != l_406), l_184))))), p_49)), 0xBDF46268L)) && p_49), g_403[2]));
    return p_52;
}


/* ------------------------------------------ */
/* 
 * reads : g_59 g_60
 * writes:
 */
static int8_t  func_56(const int8_t * const  p_57, uint32_t  p_58)
{ /* block id: 5 */
    return (*g_59);
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_78 g_79 g_96 g_60 g_59 g_73 g_13 g_80 g_127 g_146 g_162 g_175 g_176
 * writes: g_73 g_47 g_80 g_95 g_78 g_12 g_148 g_162 g_172 g_175
 */
static int8_t * func_68(int32_t  p_69, int32_t * p_70, uint32_t  p_71)
{ /* block id: 7 */
    int32_t l_107[6] = {0xBBC08F52L,0xFA727FAAL,0xFA727FAAL,0xBBC08F52L,0xFA727FAAL,0xFA727FAAL};
    int32_t l_113 = 0x0CF10AA6L;
    uint32_t *l_120 = &g_47;
    int8_t *l_138[4][8] = {{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60}};
    int8_t *l_142[5][4][2] = {{{&g_60,&g_60},{&g_60,&g_60},{&g_60,&g_60},{&g_60,&g_60}},{{&g_60,&g_60},{&g_60,&g_60},{(void*)0,(void*)0},{(void*)0,&g_60}},{{&g_60,&g_60},{(void*)0,&g_60},{(void*)0,&g_60},{&g_60,&g_60}},{{(void*)0,&g_60},{(void*)0,&g_60},{&g_60,&g_60},{(void*)0,(void*)0}},{{(void*)0,&g_60},{&g_60,&g_60},{&g_60,&g_60},{&g_60,&g_60}}};
    int8_t **l_141 = &l_142[2][2][0];
    int8_t *l_181[4][3] = {{&g_60,&g_60,&g_60},{(void*)0,&g_60,(void*)0},{&g_60,&g_60,&g_60},{(void*)0,&g_60,(void*)0}};
    int i, j, k;
    for (p_69 = 20; (p_69 != 10); --p_69)
    { /* block id: 10 */
        int8_t *l_105 = &g_60;
        int32_t l_125 = (-9L);
        uint8_t l_130 = 1UL;
        int32_t l_152 = 0L;
        int32_t l_158 = 0xAE3155DBL;
        int32_t l_159 = 1L;
        int32_t l_160[3];
        uint64_t l_168 = 0UL;
        int i;
        for (i = 0; i < 3; i++)
            l_160[i] = 0xD41E4D89L;
        for (g_73 = 0; (g_73 == 0); g_73++)
        { /* block id: 13 */
            uint32_t l_104[4] = {0xB7DFF937L,0xB7DFF937L,0xB7DFF937L,0xB7DFF937L};
            int16_t l_106[10];
            int8_t *l_123 = &g_60;
            int8_t **l_137 = &l_105;
            int8_t **l_139 = (void*)0;
            int8_t **l_140 = &l_138[0][7];
            int8_t **l_143 = &l_123;
            uint8_t *l_147 = &g_148;
            float *l_149 = &g_12;
            float *l_150[6] = {&g_151,&g_151,&g_151,&g_151,&g_151,&g_151};
            int32_t l_153 = 2L;
            int i;
            for (i = 0; i < 10; i++)
                l_106[i] = 4L;
            for (g_47 = 1; (g_47 <= 4); g_47 += 1)
            { /* block id: 16 */
                int8_t *l_108 = &g_60;
                uint32_t **l_121 = (void*)0;
                const int8_t *l_122[9][3][4] = {{{&g_60,&g_96,&g_96,(void*)0},{(void*)0,&g_60,&g_96,&g_96},{&g_96,&g_60,(void*)0,&g_60}},{{&g_60,(void*)0,&g_96,&g_96},{&g_96,&g_96,&g_96,(void*)0},{&g_96,&g_96,&g_60,&g_96}},{{&g_96,&g_60,&g_96,&g_60},{&g_96,&g_60,&g_60,&g_96},{&g_60,&g_96,&g_60,(void*)0}},{{&g_96,&g_96,&g_96,&g_96},{&g_60,(void*)0,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_96}},{{&g_96,&g_60,&g_60,(void*)0},{&g_96,&g_96,(void*)0,&g_96},{(void*)0,&g_60,&g_60,&g_96}},{{&g_96,&g_96,(void*)0,(void*)0},{&g_96,(void*)0,(void*)0,&g_96},{&g_96,&g_60,(void*)0,(void*)0}},{{&g_96,&g_96,&g_60,&g_60},{(void*)0,&g_60,(void*)0,&g_96},{&g_96,&g_96,&g_60,&g_96}},{{&g_96,&g_96,&g_60,&g_96},{(void*)0,&g_96,&g_60,&g_96},{&g_60,&g_96,&g_96,&g_60}},{{(void*)0,&g_60,(void*)0,(void*)0},{&g_96,&g_60,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96}}};
                int32_t *l_124[9] = {&l_107[5],&l_107[4],&l_107[5],&l_107[5],&l_107[4],&l_107[5],&l_107[5],&l_107[4],&l_107[5]};
                float *l_126 = (void*)0;
                int i, j, k;
                (*g_79) = g_78[g_47];
                l_113 ^= ((safe_div_func_uint16_t_u_u((func_56(func_83(((g_78[g_47] = ((safe_mod_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s((((safe_rshift_func_uint16_t_u_u((((&g_60 == (g_95 = &g_60)) > g_78[g_47]) < ((((((safe_div_func_uint32_t_u_u((safe_add_func_int16_t_s_s(0xE821L, (!(0UL && 0UL)))), (safe_sub_func_uint32_t_u_u(l_104[1], 0x4351D5E8L)))) , l_105) != l_105) || 0x60280E44E384C83BLL) >= g_96) || l_106[1])), l_106[9])) <= g_60) > l_106[1]), 1L)) , g_96), l_107[1])) && g_78[g_47])) , l_108), l_105, l_107[1], &g_80, p_71), p_69) & (*p_70)), l_107[1])) <= g_73);
                l_125 &= (l_107[1] = (safe_add_func_uint64_t_u_u((((g_13 , ((l_105 == ((safe_div_func_uint32_t_u_u(((safe_add_func_uint32_t_u_u(((((&g_78[2] == l_120) < (g_78[g_47] > ((void*)0 == l_121))) | g_47) | (l_122[2][1][1] == &g_60)), (*g_79))) < g_13), p_69)) , l_123)) != g_96)) & l_113) , p_69), p_69)));
                (*g_127) = p_71;
            }
            l_153 = ((p_71 <= (((safe_mul_func_float_f_f((l_152 = (l_125 = (l_130 < (p_70 != (((safe_mul_func_int16_t_s_s(((((((safe_sub_func_float_f_f(((*l_149) = (safe_mul_func_float_f_f((((*l_137) = l_105) != ((*l_140) = l_138[0][7])), ((((((((l_141 != l_143) || (((*l_147) = (safe_sub_func_int16_t_s_s(p_71, ((((g_146 > p_71) <= l_107[1]) , p_71) != l_104[1])))) ^ (*g_59))) && p_69) || (-4L)) ^ g_47) , p_69) >= l_104[3]) <= g_96)))), p_69)) < g_13) , &g_47) != (void*)0) , g_73) && l_104[1]), g_78[0])) != 1L) , p_70))))), p_71)) == g_13) < p_69)) < p_71);
        }
        for (l_130 = 0; (l_130 <= 3); l_130 += 1)
        { /* block id: 39 */
            int32_t *l_154 = (void*)0;
            int32_t *l_155 = (void*)0;
            int32_t *l_156 = (void*)0;
            int32_t *l_157[1][3];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_157[i][j] = &l_107[1];
            }
            --g_162;
            for (l_159 = 3; (l_159 >= 0); l_159 -= 1)
            { /* block id: 43 */
                uint32_t **l_165 = &l_120;
                if ((((*l_165) = l_120) == p_70))
                { /* block id: 45 */
                    for (g_80 = 1; (g_80 >= 0); g_80 -= 1)
                    { /* block id: 48 */
                        int16_t *l_171 = &g_172;
                        int i, j, k;
                        l_107[(l_130 + 1)] = (((*l_171) = ((l_160[(g_80 + 1)] != (safe_rshift_func_int16_t_s_u(((((l_168 ^ (l_160[1] & (((((safe_add_func_int8_t_s_s(((p_69 , &l_142[(l_130 + 1)][(g_80 + 2)][g_80]) != (void*)0), (0x1DC8AE0DL ^ 0xF0B9C9A9L))) , l_142[(l_159 + 1)][l_159][g_80]) == l_138[l_130][(l_159 + 3)]) , p_69) <= l_160[(g_80 + 1)]))) <= l_107[1]) && g_78[4]) && l_159), g_78[4]))) , l_160[(g_80 + 1)])) < 0x94B4L);
                    }
                }
                else
                { /* block id: 52 */
                    if ((*g_79))
                    { /* block id: 53 */
                        const volatile int32_t *l_174 = &g_146;
                        const volatile int32_t **l_173[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_173[i] = &l_174;
                        g_175 = &g_146;
                    }
                    else
                    { /* block id: 55 */
                        uint64_t l_177 = 0x4C0330D54037B03CLL;
                        int32_t l_180 = 0xCA9903D9L;
                        --l_177;
                        l_160[2] ^= (*p_70);
                        l_180 |= (*g_175);
                        if ((*p_70))
                            break;
                    }
                }
                if ((*p_70))
                    break;
            }
        }
    }
    return l_181[0][2];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_80
 */
static const int8_t * func_83(int8_t * p_84, int8_t * p_85, int16_t  p_86, int32_t * p_87, int32_t  p_88)
{ /* block id: 20 */
    uint32_t *l_111 = &g_78[3];
    uint32_t **l_112 = &l_111;
    (*p_87) = (safe_add_func_int8_t_s_s((((*l_112) = l_111) != p_87), (&g_60 == p_85)));
    return &g_60;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc_bytes (&g_12, sizeof(g_12), "g_12", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_15[i][j][k], "g_15[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_78[i], "g_78[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_80, "g_80", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    transparent_crc(g_146, "g_146", print_hash_value);
    transparent_crc(g_148, "g_148", print_hash_value);
    transparent_crc_bytes (&g_151, sizeof(g_151), "g_151", print_hash_value);
    transparent_crc_bytes (&g_161, sizeof(g_161), "g_161", print_hash_value);
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc(g_172, "g_172", print_hash_value);
    transparent_crc(g_176, "g_176", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_187[i], "g_187[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_207, "g_207", print_hash_value);
    transparent_crc(g_230, "g_230", print_hash_value);
    transparent_crc(g_265, "g_265", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_269[i][j][k], "g_269[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_307, "g_307", print_hash_value);
    transparent_crc(g_312, "g_312", print_hash_value);
    transparent_crc(g_355, "g_355", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_403[i], "g_403[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_434, "g_434", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_502[i][j], "g_502[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc_bytes(&g_564[i][j], sizeof(g_564[i][j]), "g_564[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_603, "g_603", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_605[i][j], "g_605[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_715, "g_715", print_hash_value);
    transparent_crc(g_864, "g_864", print_hash_value);
    transparent_crc(g_905, "g_905", print_hash_value);
    transparent_crc(g_988, "g_988", print_hash_value);
    transparent_crc(g_1027, "g_1027", print_hash_value);
    transparent_crc(g_1044, "g_1044", print_hash_value);
    transparent_crc(g_1056, "g_1056", print_hash_value);
    transparent_crc_bytes (&g_1072, sizeof(g_1072), "g_1072", print_hash_value);
    transparent_crc(g_1182, "g_1182", print_hash_value);
    transparent_crc(g_1198, "g_1198", print_hash_value);
    transparent_crc(g_1279, "g_1279", print_hash_value);
    transparent_crc(g_1280, "g_1280", print_hash_value);
    transparent_crc(g_1281, "g_1281", print_hash_value);
    transparent_crc(g_1329, "g_1329", print_hash_value);
    transparent_crc(g_1676, "g_1676", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1763[i][j], "g_1763[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1809, sizeof(g_1809), "g_1809", print_hash_value);
    transparent_crc(g_1845, "g_1845", print_hash_value);
    transparent_crc(g_1861, "g_1861", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2004[i], "g_2004[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2158, "g_2158", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2223[i], "g_2223[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2264, "g_2264", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_2269[i], "g_2269[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 553
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 150
   depth: 2, occurrence: 43
   depth: 3, occurrence: 4
   depth: 5, occurrence: 2
   depth: 9, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 18, occurrence: 4
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 27, occurrence: 1
   depth: 33, occurrence: 2
   depth: 35, occurrence: 1
   depth: 38, occurrence: 1
   depth: 39, occurrence: 2
   depth: 40, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 559

XXX times a variable address is taken: 1334
XXX times a pointer is dereferenced on RHS: 253
breakdown:
   depth: 1, occurrence: 223
   depth: 2, occurrence: 27
   depth: 3, occurrence: 3
XXX times a pointer is dereferenced on LHS: 283
breakdown:
   depth: 1, occurrence: 261
   depth: 2, occurrence: 17
   depth: 3, occurrence: 4
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 35
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 21
XXX times a pointer is qualified to be dereferenced: 9480

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1012
   level: 2, occurrence: 144
   level: 3, occurrence: 36
   level: 4, occurrence: 11
   level: 5, occurrence: 12
XXX number of pointers point to pointers: 241
XXX number of pointers point to scalars: 318
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.7
XXX average alias set size: 1.51

XXX times a non-volatile is read: 1729
XXX times a non-volatile is write: 828
XXX times a volatile is read: 91
XXX    times read thru a pointer: 14
XXX times a volatile is write: 46
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 1.36e+03
XXX percentage of non-volatile access: 94.9

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 157
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 23
   depth: 2, occurrence: 35
   depth: 3, occurrence: 28
   depth: 4, occurrence: 14
   depth: 5, occurrence: 29

XXX percentage a fresh-made variable is used: 14.8
XXX percentage an existing variable is used: 85.2
********************* end of statistics **********************/


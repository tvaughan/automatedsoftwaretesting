/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3343216211
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int8_t g_13 = 0x94L;
static uint32_t g_16[7][2][5] = {{{1UL,0UL,4294967295UL,1UL,4294967295UL},{4294967289UL,1UL,0xC5EB2006L,1UL,1UL}},{{4294967295UL,0UL,0x82C32B96L,1UL,1UL},{4294967295UL,4294967295UL,4294967295UL,0xC5EB2006L,1UL}},{{1UL,1UL,0x1D91E66CL,4294967289UL,1UL},{0UL,4294967289UL,0xE4F1F13BL,1UL,4294967295UL}},{{0UL,4294967295UL,0x1D91E66CL,1UL,0x15AA9614L},{4294967289UL,4294967295UL,4294967295UL,4294967289UL,0UL}},{{4294967289UL,1UL,0x82C32B96L,1UL,1UL},{0UL,1UL,0x07E67329L,0xE4F1F13BL,0UL}},{{1UL,0x12A3CC92L,0x80BFB3A4L,0x1D91E66CL,0xE4F1F13BL},{0xC5EB2006L,4294967295UL,4294967295UL,4294967295UL,0xC5EB2006L}},{{0x80BFB3A4L,4294967295UL,0x12A3CC92L,0x82C32B96L,0xF1E0C863L},{0xF1E0C863L,0x12A3CC92L,0xE4F1F13BL,0xC5EB2006L,0xE1C2E937L}}};
static int64_t g_55[9][8][3] = {{{0xBC05D4008F4F80CFLL,0xBC05D4008F4F80CFLL,(-1L)},{0L,0xCF1140913DB3B8CDLL,(-6L)},{0x04A32A0C022AD617LL,5L,0xC169712D56B38323LL},{1L,0x4028B0229B02088CLL,0x166DA9A16D6AE4D7LL},{0x80D42F53403E1FE0LL,0x04A32A0C022AD617LL,0xC169712D56B38323LL},{0x8350085190AC52CFLL,1L,(-6L)},{0xB0C7F2BAC4117773LL,1L,(-1L)},{0x4028B0229B02088CLL,0L,6L}},{{0L,0x2006156DBBF4C003LL,0xFA6FBAAEEA3ADEB0LL},{0x4028B0229B02088CLL,0x996F54E6A7687144LL,1L},{0xB0C7F2BAC4117773LL,3L,0xB5077743AFF46A2FLL},{0x8350085190AC52CFLL,1L,(-1L)},{0x80D42F53403E1FE0LL,0x86ADB69C5226059BLL,0x05ACF7E1B1CD7F69LL},{1L,1L,1L},{0x04A32A0C022AD617LL,3L,0xD6C9D3DA5920033CLL},{0L,0x996F54E6A7687144LL,1L}},{{0xBC05D4008F4F80CFLL,0x2006156DBBF4C003LL,0L},{(-1L),0L,1L},{0x6AB0C02EE360852BLL,1L,0xD6C9D3DA5920033CLL},{0x8B70F7348F3BEA0ALL,1L,1L},{0x2006156DBBF4C003LL,0x04A32A0C022AD617LL,0x05ACF7E1B1CD7F69LL},{3L,0x4028B0229B02088CLL,(-1L)},{0x2006156DBBF4C003LL,5L,0xB5077743AFF46A2FLL},{0x8B70F7348F3BEA0ALL,0xCF1140913DB3B8CDLL,1L}},{{0x6AB0C02EE360852BLL,0xBC05D4008F4F80CFLL,0xFA6FBAAEEA3ADEB0LL},{(-1L),0x4E37A2E34EF9DEABLL,6L},{0xBC05D4008F4F80CFLL,0xBC05D4008F4F80CFLL,(-1L)},{0L,0xCF1140913DB3B8CDLL,(-6L)},{0x04A32A0C022AD617LL,5L,0xC169712D56B38323LL},{1L,0x4028B0229B02088CLL,0x166DA9A16D6AE4D7LL},{0x80D42F53403E1FE0LL,0x04A32A0C022AD617LL,0xC169712D56B38323LL},{0x8350085190AC52CFLL,1L,(-6L)}},{{0xB0C7F2BAC4117773LL,1L,(-1L)},{0x4028B0229B02088CLL,0L,6L},{0L,0x2006156DBBF4C003LL,0xFA6FBAAEEA3ADEB0LL},{0x4028B0229B02088CLL,0x996F54E6A7687144LL,1L},{0xB0C7F2BAC4117773LL,3L,0xB5077743AFF46A2FLL},{0x8350085190AC52CFLL,1L,(-1L)},{0x80D42F53403E1FE0LL,0x86ADB69C5226059BLL,0x05ACF7E1B1CD7F69LL},{1L,1L,1L}},{{0x04A32A0C022AD617LL,3L,0xD6C9D3DA5920033CLL},{0L,0x996F54E6A7687144LL,1L},{0xBC05D4008F4F80CFLL,0x2006156DBBF4C003LL,0L},{(-1L),0L,1L},{0x6AB0C02EE360852BLL,1L,0xD6C9D3DA5920033CLL},{0x8B70F7348F3BEA0ALL,1L,1L},{0x2006156DBBF4C003LL,0x04A32A0C022AD617LL,0x05ACF7E1B1CD7F69LL},{3L,0x4028B0229B02088CLL,(-1L)}},{{0x2006156DBBF4C003LL,5L,0xB5077743AFF46A2FLL},{0x8B70F7348F3BEA0ALL,0xCF1140913DB3B8CDLL,1L},{0x6AB0C02EE360852BLL,0xBC05D4008F4F80CFLL,0xFA6FBAAEEA3ADEB0LL},{(-1L),0x4E37A2E34EF9DEABLL,6L},{0xBC05D4008F4F80CFLL,0xBC05D4008F4F80CFLL,(-1L)},{0L,0xCF1140913DB3B8CDLL,(-6L)},{0x04A32A0C022AD617LL,5L,0xC169712D56B38323LL},{1L,0x4028B0229B02088CLL,0x4028B0229B02088CLL}},{{0x536CA9A47FA8275BLL,0L,3L},{0x7709C18003DBE6A9LL,0xC990B51ECDE474C7LL,0x07631C0487CC540ELL},{0xC631DBC551464E94LL,0x233419813DA773CELL,0x04A32A0C022AD617LL},{0x8BCD06DB25596398LL,(-1L),0x8B70F7348F3BEA0ALL},{(-1L),6L,0L},{0x8BCD06DB25596398LL,0xE8E23095C3C62ED5LL,0x4E37A2E34EF9DEABLL},{0xC631DBC551464E94LL,0xCB42B08F18E0DA53LL,0x86ADB69C5226059BLL},{0x7709C18003DBE6A9LL,0x2E49D2A59A03C9D2LL,3L}},{{0x536CA9A47FA8275BLL,0x56EC073B9F6D4B68LL,0xB0C7F2BAC4117773LL},{0x2E49D2A59A03C9D2LL,0x2E49D2A59A03C9D2LL,0L},{0L,0xCB42B08F18E0DA53LL,0xA5C6A8B5F6E56995LL},{(-1L),0xE8E23095C3C62ED5LL,0xCF1140913DB3B8CDLL},{9L,6L,0x2006156DBBF4C003LL},{(-1L),(-1L),0xCF1140913DB3B8CDLL},{1L,0x233419813DA773CELL,0xA5C6A8B5F6E56995LL},{0xD9C50E9BCC685A4BLL,0xC990B51ECDE474C7LL,0L}}};
static int32_t g_60 = 0x8D9B7878L;
static uint32_t g_70[5][8][6] = {{{4294967290UL,0xB54EB929L,4294967287UL,0xACF22293L,0x8C0B7B9EL,4294967295UL},{0UL,4294967290UL,4294967287UL,0UL,0UL,1UL},{0UL,0UL,4294967295UL,0x4474F446L,4294967287UL,4294967287UL},{0x4474F446L,4294967287UL,4294967287UL,1UL,4294967295UL,0UL},{4294967288UL,0x20FBFF59L,0xD4E85873L,1UL,0x4D657348L,4294967293UL},{0x4D657348L,4294967295UL,0xC6E4F414L,4294967288UL,0xC6E4F414L,4294967295UL},{0x321833FEL,1UL,0x5A27E8BCL,0x5D945931L,4294967288UL,1UL},{4294967295UL,0x0579173DL,0xB111D6B6L,0x11D03F4AL,0x5F2429B4L,0UL}},{{4294967293UL,0x0579173DL,1UL,4294967287UL,4294967288UL,0x4D657348L},{4294967287UL,1UL,0x571CA78AL,0x5A27E8BCL,0xC6E4F414L,0xB111D6B6L},{0xACF22293L,4294967295UL,4294967295UL,0x0579173DL,0x4D657348L,0xC6E4F414L},{1UL,0x20FBFF59L,0x5D945931L,1UL,4294967295UL,4294967287UL},{1UL,4294967287UL,0UL,0UL,4294967287UL,1UL},{4294967295UL,0UL,0x20FBFF59L,4294967287UL,0UL,0x321833FEL},{4294967288UL,4294967290UL,4294967293UL,1UL,0x8C0B7B9EL,4294967290UL},{4294967288UL,0xB54EB929L,1UL,4294967287UL,1UL,4294967290UL}},{{4294967295UL,0UL,0UL,0UL,1UL,0xD4E85873L},{1UL,1UL,0x4474F446L,1UL,4294967290UL,4294967288UL},{1UL,4294967295UL,4294967295UL,0x0579173DL,1UL,1UL},{0xACF22293L,0x5A27E8BCL,1UL,0x5A27E8BCL,0xACF22293L,0UL},{4294967287UL,0x11D03F4AL,0UL,4294967287UL,0x0579173DL,0x20FBFF59L},{4294967293UL,1UL,0UL,0x11D03F4AL,0x321833FEL,0x20FBFF59L},{4294967295UL,0x4474F446L,0UL,0x5D945931L,0xB54EB929L,0UL},{0x321833FEL,1UL,1UL,0xB54EB929L,4294967288UL,0x5A27E8BCL}},{{0x5F2429B4L,1UL,0UL,1UL,0UL,0xB54EB929L},{0xB54EB929L,0x571CA78AL,4294967288UL,0UL,1UL,1UL},{4294967288UL,0x4474F446L,0x4474F446L,4294967288UL,0xE9FA02E7L,0x11D03F4AL},{4294967287UL,1UL,0xACF22293L,0xB111D6B6L,1UL,0x4D657348L},{0x4474F446L,4294967290UL,0UL,0x5D945931L,1UL,1UL},{0x11D03F4AL,1UL,0x8C0B7B9EL,0x5F2429B4L,0xE9FA02E7L,1UL},{4294967295UL,0x4474F446L,4294967287UL,4294967287UL,1UL,4294967295UL},{4294967290UL,0x571CA78AL,4294967295UL,1UL,0UL,0UL}},{{4294967295UL,1UL,0xE9FA02E7L,0x20FBFF59L,4294967288UL,0x20FBFF59L},{4294967290UL,4294967287UL,4294967290UL,1UL,4294967295UL,0x5F2429B4L},{4294967295UL,4294967288UL,4294967287UL,4294967295UL,1UL,0xB111D6B6L},{4294967293UL,0x5A27E8BCL,0x20FBFF59L,4294967295UL,0xD4E85873L,1UL},{4294967295UL,4294967288UL,0x0579173DL,1UL,0x5D945931L,4294967293UL},{4294967290UL,0x0579173DL,0UL,0x20FBFF59L,1UL,0UL},{4294967295UL,0xE9FA02E7L,1UL,1UL,0x4D657348L,4294967295UL},{4294967290UL,0xC6E4F414L,1UL,4294967287UL,4294967287UL,1UL}}};
static int32_t ** volatile g_71 = (void*)0;/* VOLATILE GLOBAL g_71 */
static const int8_t g_79 = 0xA5L;
static uint32_t g_94 = 6UL;
static uint32_t g_110 = 0x70772F82L;
static int16_t g_125[4] = {0xC583L,0xC583L,0xC583L,0xC583L};
static int64_t g_127 = 0x8D6ADFE8D4ABBEE4LL;
static const int32_t g_145 = (-1L);
static uint32_t g_146 = 0x340708E7L;
static int8_t g_164[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static float g_206 = 0x9.0p-1;
static uint64_t g_214 = 18446744073709551608UL;
static uint64_t *g_213 = &g_214;
static volatile uint32_t g_222 = 0xE43ADCCCL;/* VOLATILE GLOBAL g_222 */
static uint8_t g_235[4][2] = {{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}};
static volatile int32_t g_256 = 2L;/* VOLATILE GLOBAL g_256 */
static int32_t *g_318[8] = {&g_60,&g_60,(void*)0,&g_60,&g_60,(void*)0,&g_60,&g_60};
static int16_t *g_337[3] = {&g_125[1],&g_125[1],&g_125[1]};
static float g_353 = 0x1.2p+1;
static float * const  volatile g_352 = &g_353;/* VOLATILE GLOBAL g_352 */
static int64_t *g_414[2][5] = {{&g_55[8][4][0],&g_55[8][4][0],&g_55[8][4][0],&g_55[8][4][0],&g_55[8][4][0]},{&g_55[8][4][0],&g_55[8][4][0],&g_55[8][4][0],&g_55[8][4][0],&g_55[8][4][0]}};
static float * volatile g_500 = &g_206;/* VOLATILE GLOBAL g_500 */
static int32_t ** volatile g_511[2] = {&g_318[3],&g_318[3]};
static int32_t ** const  volatile g_512 = &g_318[7];/* VOLATILE GLOBAL g_512 */
static int64_t g_584 = 0L;
static volatile uint16_t g_663 = 4UL;/* VOLATILE GLOBAL g_663 */
static volatile uint16_t * const g_662[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile uint16_t * const  volatile * volatile g_661 = &g_662[1];/* VOLATILE GLOBAL g_661 */
static uint16_t **g_664 = (void*)0;
static int32_t g_759 = (-10L);
static volatile int16_t g_760 = 0xBDD6L;/* VOLATILE GLOBAL g_760 */
static const volatile int8_t g_764 = 3L;/* VOLATILE GLOBAL g_764 */
static int32_t **g_790 = &g_318[3];
static int32_t ***g_789 = &g_790;
static volatile uint16_t ***g_825 = (void*)0;
static volatile uint16_t *** volatile * volatile g_824 = &g_825;/* VOLATILE GLOBAL g_824 */
static volatile uint16_t *** volatile * volatile * volatile g_826 = &g_824;/* VOLATILE GLOBAL g_826 */
static int16_t g_865[6] = {0xAF33L,0xAF33L,0xAF33L,0xAF33L,0xAF33L,0xAF33L};
static const int8_t g_898 = 0x06L;
static int8_t g_925[9][4][3] = {{{0x29L,0x34L,0L},{0xC1L,0L,0L},{0xA4L,0x3FL,0L},{0xB0L,9L,(-4L)}},{{0xA4L,0L,0L},{0xC1L,1L,0xE5L},{0x29L,0L,0x3FL},{0x05L,9L,(-7L)}},{{(-1L),0x3FL,0x3FL},{0xE2L,0L,0xE5L},{(-8L),0x34L,0L},{0xE2L,(-7L),(-4L)}},{{(-1L),1L,0L},{0x05L,1L,0xFFL},{1L,0x80L,7L},{0L,0xFFL,0xFFL}},{{0x70L,0xD5L,0x25L},{0xE5L,(-1L),1L},{0x70L,7L,0x9CL},{0L,0x52L,(-1L)}},{{1L,7L,0xD5L},{1L,(-1L),1L},{0x3FL,0xD5L,0xD5L},{0L,0xFFL,(-1L)}},{{0L,0x80L,0x9CL},{0L,1L,1L},{0x3FL,0xA8L,0x25L},{1L,1L,0xFFL}},{{1L,0x80L,7L},{0L,0xFFL,0xFFL},{0x70L,0xD5L,0x25L},{0xE5L,(-1L),1L}},{{0x70L,7L,0x9CL},{0L,0x52L,(-1L)},{1L,7L,0xD5L},{1L,(-1L),1L}}};
static uint16_t g_950 = 0x2C77L;
static int8_t *g_967 = &g_164[5];
static int32_t g_975 = 1L;
static int32_t *g_974 = &g_975;
static int32_t g_1028 = 0xBFB9C491L;
static uint32_t g_1053 = 0xE04BEDCDL;
static int8_t g_1064 = (-2L);
static uint64_t g_1104 = 0x61B8F31123880CD4LL;
static int64_t g_1107 = 0L;
static float * volatile g_1113 = &g_206;/* VOLATILE GLOBAL g_1113 */
static volatile float g_1232 = 0xD.FE75F6p-33;/* VOLATILE GLOBAL g_1232 */
static uint32_t g_1243 = 0x3B25D548L;
static int32_t ****g_1378 = &g_789;
static uint8_t g_1485 = 0xBAL;
static uint8_t g_1486 = 3UL;
static uint8_t g_1487 = 0xEDL;
static uint8_t g_1488[6] = {0x37L,0x37L,0x37L,0x37L,0x37L,0x37L};
static uint8_t g_1489 = 2UL;
static uint8_t g_1490 = 0x70L;
static uint8_t g_1491 = 1UL;
static uint8_t g_1492 = 0UL;
static uint8_t g_1493 = 9UL;
static uint8_t g_1494 = 1UL;
static uint8_t g_1495 = 255UL;
static uint8_t g_1496 = 255UL;
static uint8_t g_1497 = 0x84L;
static uint8_t g_1498 = 0x71L;
static uint8_t g_1499 = 0x1CL;
static uint8_t g_1500 = 0x6EL;
static uint8_t g_1501 = 255UL;
static uint8_t g_1502 = 1UL;
static uint8_t g_1503 = 255UL;
static uint8_t g_1504[9][9] = {{1UL,0x4CL,0xAAL,0x0CL,0x30L,0xC7L,0x82L,0x30L,0x4CL},{0xAFL,0x7FL,0UL,0x82L,0xAAL,0xAAL,0x82L,0UL,0x7FL},{0x2EL,0UL,0xC7L,0x64L,0xAAL,0xC7L,0xAFL,0x7FL,0UL},{0x0CL,0x30L,0xC7L,0x82L,0x30L,0x4CL,0x64L,0x4CL,0x30L},{0x0CL,0UL,0UL,0x0CL,0x7FL,0x30L,0x2EL,0x4CL,0UL},{0x2EL,0x7FL,0xAAL,1UL,0xCDL,0x30L,0x64L,0x7FL,0x7FL},{0xAFL,0x4CL,0x7FL,1UL,0x7FL,0x4CL,0xAFL,0UL,0x4CL},{1UL,0x4CL,0xAAL,0x0CL,0x30L,0xC7L,0x82L,0x30L,0x4CL},{0xAFL,0x7FL,0UL,0x82L,0xAAL,0xAAL,0x82L,0UL,0x7FL}};
static uint8_t g_1505 = 0xDBL;
static uint8_t g_1506 = 8UL;
static uint8_t g_1507 = 0xC6L;
static uint8_t g_1508 = 0UL;
static uint8_t g_1509[5] = {0x80L,0x80L,0x80L,0x80L,0x80L};
static uint8_t g_1510 = 0x3FL;
static uint8_t g_1511 = 1UL;
static uint8_t g_1512 = 7UL;
static uint8_t g_1513 = 0UL;
static uint8_t g_1514 = 1UL;
static uint8_t g_1515 = 248UL;
static uint8_t g_1516 = 0xB7L;
static uint8_t g_1517 = 0x6EL;
static uint8_t g_1518 = 9UL;
static uint8_t g_1519 = 0x4BL;
static uint8_t g_1520[2][8] = {{3UL,3UL,3UL,3UL,3UL,3UL,3UL,3UL},{3UL,3UL,3UL,3UL,3UL,3UL,3UL,3UL}};
static uint8_t g_1521 = 1UL;
static uint8_t g_1522 = 0x57L;
static uint8_t g_1523[10] = {0x4AL,0x4AL,0x4AL,0x4AL,0x4AL,0x4AL,0x4AL,0x4AL,0x4AL,0x4AL};
static uint8_t g_1524 = 4UL;
static uint8_t g_1525[8][3][3] = {{{1UL,3UL,3UL},{0x6AL,0xECL,0x6AL},{1UL,1UL,3UL}},{{0x30L,0xECL,0x30L},{1UL,3UL,3UL},{0x6AL,0xECL,0x6AL}},{{1UL,1UL,3UL},{0x30L,0xECL,0x30L},{1UL,3UL,3UL}},{{0x6AL,0xECL,0x6AL},{1UL,1UL,3UL},{0x30L,0xECL,0x30L}},{{1UL,3UL,3UL},{0x6AL,0xECL,0x6AL},{1UL,1UL,3UL}},{{0x30L,0xECL,0x30L},{1UL,3UL,3UL},{0x6AL,0xECL,0x6AL}},{{1UL,1UL,3UL},{0x30L,0xECL,0x30L},{1UL,3UL,3UL}},{{0x6AL,0xECL,0x6AL},{1UL,1UL,3UL},{0x30L,0xECL,0x30L}}};
static uint8_t g_1526 = 2UL;
static uint8_t g_1527 = 0UL;
static uint8_t g_1528[1][7][5] = {{{0xB9L,0xF1L,0xF1L,0xB9L,0xB9L},{255UL,255UL,255UL,255UL,255UL},{0xB9L,0xB9L,0xF1L,0xF1L,0xB9L},{0xDDL,255UL,0xDDL,255UL,0xDDL},{0xB9L,0xF1L,0xF1L,0xB9L,0xB9L},{255UL,255UL,255UL,255UL,255UL},{0xB9L,0xB9L,0xF1L,0xF1L,0xB9L}}};
static uint8_t g_1529 = 0x4FL;
static uint8_t g_1530 = 9UL;
static uint8_t g_1531 = 0UL;
static uint8_t g_1532 = 0x5DL;
static uint8_t g_1533 = 255UL;
static uint8_t g_1534 = 0x57L;
static uint8_t g_1535[2] = {0UL,0UL};
static uint8_t g_1536 = 0xC4L;
static uint8_t g_1537[8] = {0x02L,0x02L,0x02L,0x02L,0x02L,0x02L,0x02L,0x02L};
static uint8_t g_1538 = 254UL;
static uint8_t g_1539 = 0xFEL;
static uint8_t g_1540[9][4] = {{0xCCL,246UL,0x8FL,246UL},{246UL,0UL,0x8FL,0x8FL},{0xCCL,0xCCL,246UL,0x8FL},{0xE6L,0UL,0xE6L,246UL},{0xE6L,246UL,246UL,0xE6L},{0xCCL,246UL,0x8FL,246UL},{246UL,0UL,0x8FL,0x8FL},{0xCCL,0xCCL,246UL,0x8FL},{0xE6L,0UL,0xE6L,246UL}};
static uint8_t g_1541 = 255UL;
static uint8_t g_1542[9][6][4] = {{{0UL,249UL,1UL,7UL},{0xC7L,0x66L,0x21L,249UL},{1UL,0UL,0x21L,0UL},{0xC7L,255UL,1UL,250UL},{0UL,255UL,1UL,0UL},{1UL,0UL,0xC7L,249UL}},{{1UL,0x66L,1UL,7UL},{0UL,249UL,1UL,7UL},{0xC7L,0x66L,0x21L,249UL},{1UL,0UL,0x21L,0UL},{0xC7L,255UL,1UL,0x66L},{1UL,249UL,1UL,255UL}},{{1UL,255UL,255UL,7UL},{1UL,8UL,1UL,250UL},{1UL,7UL,0x21L,250UL},{255UL,8UL,0xC7L,7UL},{1UL,255UL,0xC7L,255UL},{255UL,249UL,0x21L,0x66L}},{{1UL,249UL,1UL,255UL},{1UL,255UL,255UL,7UL},{1UL,8UL,1UL,250UL},{1UL,7UL,0x21L,250UL},{255UL,8UL,0xC7L,7UL},{1UL,255UL,0xC7L,255UL}},{{255UL,249UL,0x21L,0x66L},{1UL,249UL,1UL,255UL},{1UL,255UL,255UL,7UL},{1UL,8UL,1UL,250UL},{1UL,7UL,0x21L,250UL},{255UL,8UL,0xC7L,7UL}},{{1UL,255UL,0xC7L,255UL},{255UL,249UL,0x21L,0x66L},{1UL,249UL,1UL,255UL},{1UL,255UL,255UL,7UL},{1UL,8UL,1UL,250UL},{1UL,7UL,0x21L,250UL}},{{255UL,8UL,0xC7L,7UL},{1UL,255UL,0xC7L,255UL},{255UL,249UL,0x21L,0x66L},{1UL,249UL,1UL,255UL},{1UL,255UL,255UL,7UL},{1UL,8UL,1UL,250UL}},{{1UL,7UL,0x21L,250UL},{255UL,8UL,0xC7L,7UL},{1UL,255UL,0xC7L,255UL},{255UL,249UL,0x21L,0x66L},{1UL,249UL,1UL,255UL},{1UL,255UL,255UL,7UL}},{{1UL,8UL,1UL,250UL},{1UL,7UL,0x21L,250UL},{255UL,8UL,0xC7L,7UL},{1UL,255UL,0xC7L,255UL},{255UL,249UL,0x21L,0x66L},{1UL,249UL,1UL,255UL}}};
static uint8_t g_1543 = 0x65L;
static uint8_t g_1544 = 0x19L;
static uint8_t g_1545 = 0x68L;
static uint8_t g_1546 = 250UL;
static uint8_t g_1547 = 0UL;
static uint8_t g_1548 = 0x4AL;
static uint8_t g_1549 = 1UL;
static uint8_t g_1550 = 255UL;
static uint8_t g_1551 = 0xACL;
static uint8_t g_1552 = 255UL;
static uint8_t g_1553 = 7UL;
static uint8_t g_1554 = 0x10L;
static uint8_t g_1555[6][4] = {{253UL,0xB9L,253UL,5UL},{0x0FL,0xC7L,0UL,0xC7L},{0xC7L,253UL,253UL,0xC7L},{253UL,0xC7L,5UL,5UL},{0xB9L,253UL,0xB9L,253UL},{253UL,0UL,1UL,253UL}};
static uint8_t g_1556 = 0x08L;
static uint8_t g_1557 = 255UL;
static uint8_t g_1558 = 8UL;
static uint8_t g_1559 = 0xEDL;
static uint8_t g_1560 = 251UL;
static uint8_t g_1561 = 3UL;
static int32_t g_1572[2][7][8] = {{{6L,0xD70AE3B4L,0xD70AE3B4L,6L,1L,0x46511489L,1L,0xC151E76BL},{0xD70AE3B4L,1L,0L,0x87C46924L,0x4C49DF5FL,0xB9F5F4F1L,0x27C09F7FL,0L},{2L,1L,0xC86E9853L,7L,0L,0x46511489L,0x4C49DF5FL,2L},{1L,0xD70AE3B4L,0x87C46924L,0xB9F5F4F1L,0x715DF860L,1L,0xD70AE3B4L,0xBA7FBB67L},{0L,0x0081EBC8L,1L,0x3BECF9BDL,1L,0x0081EBC8L,0L,0xD70AE3B4L},{6L,7L,0L,0xA37050F5L,0x3B153423L,6L,0xB9F5F4F1L,0xC151E76BL},{7L,1L,0x0B4434F5L,0L,0x3B153423L,0L,0x27C09F7FL,0x3BECF9BDL}},{{6L,0x1251E9B7L,0x2ADB76C3L,0xC151E76BL,1L,0x46511489L,1L,6L},{0L,1L,0L,0L,0x715DF860L,0x715DF860L,0L,0L},{1L,1L,0xB9F5F4F1L,0x3BECF9BDL,0L,0L,0x715DF860L,1L},{2L,0xD70AE3B4L,0L,0xC86E9853L,0x4C49DF5FL,0xA37050F5L,0xD70AE3B4L,1L},{0xD70AE3B4L,1L,0xBA7FBB67L,0x3BECF9BDL,1L,0xC151E76BL,0x1251E9B7L,0L},{6L,0x3B153423L,0xA37050F5L,0L,7L,6L,0x4C49DF5FL,6L},{0L,0xC151E76BL,0x0B4434F5L,0xC151E76BL,0L,1L,0L,0x3BECF9BDL}}};
static int32_t * const ***g_1598 = (void*)0;
static int32_t * const ****g_1597 = &g_1598;
static int64_t g_1642 = 0x64E2C095C0FF80A1LL;
static uint32_t *g_1673 = &g_1053;
static const int32_t ** volatile g_1724[4][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
static const int32_t *g_1726 = (void*)0;
static const int32_t ** volatile g_1725 = &g_1726;/* VOLATILE GLOBAL g_1725 */
static float *g_1739 = (void*)0;
static uint32_t *g_1767[2][1] = {{&g_70[2][6][0]},{&g_70[2][6][0]}};
static uint32_t **g_1766 = &g_1767[1][0];
static volatile uint8_t ***g_1798 = (void*)0;
static volatile uint32_t g_1885 = 18446744073709551606UL;/* VOLATILE GLOBAL g_1885 */
static int32_t *g_1922[3] = {&g_1028,&g_1028,&g_1028};
static int32_t ** volatile g_1921[1] = {&g_1922[0]};
static volatile uint16_t ** volatile g_1992 = (void*)0;/* VOLATILE GLOBAL g_1992 */
static volatile uint16_t ** volatile * const g_1991 = &g_1992;
static volatile uint16_t ** volatile * const  volatile *g_1990 = &g_1991;
static volatile uint16_t ** volatile * const  volatile **g_1989 = &g_1990;
static uint8_t g_1994 = 0UL;
static int32_t g_1998 = (-9L);
static uint64_t **g_2035[7][4][3] = {{{&g_213,&g_213,&g_213},{&g_213,&g_213,(void*)0},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213}},{{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213}},{{&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213},{&g_213,&g_213,(void*)0},{&g_213,&g_213,&g_213}},{{&g_213,&g_213,(void*)0},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213}},{{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{(void*)0,&g_213,&g_213},{&g_213,&g_213,&g_213}},{{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213}},{{&g_213,(void*)0,&g_213},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213},{&g_213,&g_213,&g_213}}};
static uint64_t ** const * volatile g_2034 = &g_2035[2][0][0];/* VOLATILE GLOBAL g_2034 */
static float * volatile g_2089[7][2][4] = {{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206}}};
static uint32_t ***g_2092 = &g_1766;
static uint32_t **** volatile g_2091 = &g_2092;/* VOLATILE GLOBAL g_2091 */
static float * volatile g_2097 = &g_206;/* VOLATILE GLOBAL g_2097 */
static uint16_t ***g_2160 = &g_664;
static uint32_t g_2178 = 0xB7A4BC77L;
static const int16_t * volatile * const  volatile g_2241 = (void*)0;/* VOLATILE GLOBAL g_2241 */
static const int16_t * volatile * const  volatile *g_2240 = &g_2241;
static uint8_t g_2264 = 0x91L;
static volatile float g_2275 = (-0x9.Ap+1);/* VOLATILE GLOBAL g_2275 */
static uint16_t *g_2289 = &g_950;
static float * volatile g_2298 = (void*)0;/* VOLATILE GLOBAL g_2298 */
static float * volatile g_2300 = &g_353;/* VOLATILE GLOBAL g_2300 */
static uint32_t g_2323 = 9UL;
static uint64_t g_2325[9] = {7UL,18446744073709551613UL,18446744073709551613UL,7UL,18446744073709551613UL,18446744073709551613UL,7UL,18446744073709551613UL,18446744073709551613UL};
static int16_t g_2348 = 0x417BL;
static float * volatile g_2352[2] = {&g_206,&g_206};
static float * volatile g_2353 = &g_206;/* VOLATILE GLOBAL g_2353 */
static float * volatile g_2359 = &g_353;/* VOLATILE GLOBAL g_2359 */
static int32_t *g_2385[3][9][2] = {{{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975}},{{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975}},{{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975},{&g_975,&g_975}}};
static float * volatile g_2438 = &g_353;/* VOLATILE GLOBAL g_2438 */
static uint32_t g_2527 = 0x80006C13L;
static const uint32_t *g_2540 = &g_110;
static const uint32_t **g_2539[10] = {&g_2540,&g_2540,&g_2540,&g_2540,&g_2540,&g_2540,&g_2540,&g_2540,&g_2540,&g_2540};
static const uint32_t ***g_2538[3] = {&g_2539[6],&g_2539[6],&g_2539[6]};
static const uint32_t ****g_2537 = &g_2538[0];
static float * volatile g_2571 = &g_353;/* VOLATILE GLOBAL g_2571 */
static int16_t g_2594 = 0x5F13L;
static uint16_t g_2596 = 2UL;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static const int32_t  func_2(uint32_t  p_3, uint16_t  p_4, int64_t  p_5, int32_t  p_6);
static uint16_t  func_17(int8_t * p_18, int8_t * p_19);
static int8_t * func_20(uint8_t  p_21, int8_t  p_22);
static uint8_t  func_23(int8_t * p_24, float  p_25, float  p_26, int64_t  p_27);
static int8_t * func_28(int8_t * p_29, float  p_30, float  p_31);
static int8_t * func_32(int8_t * p_33, int8_t * p_34);
static int8_t * func_35(int64_t  p_36, int32_t  p_37, const float  p_38);
static int16_t  func_40(int64_t  p_41, int64_t  p_42);
static uint64_t  func_44(uint64_t  p_45);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_16 g_70 g_71 g_55 g_213 g_214 g_865 g_789 g_790 g_584 g_60 g_318 g_110 g_975 g_1542 g_1546 g_1507 g_1535 g_1504 g_967 g_925 g_1642 g_950 g_1572 g_1549 g_1053 g_125 g_1518 g_500 g_206 g_1525 g_1492 g_1725 g_127 g_1378 g_1523 g_352 g_353 g_1534 g_1766 g_1064 g_164 g_146 g_759 g_2594 g_2596 g_1994 g_1540 g_2540 g_1555 g_2091 g_2092 g_1767 g_2178 g_2289 g_13 g_256 g_1554 g_1505 g_1922 g_1527 g_1538 g_235 g_1533 g_2537 g_2538 g_2539 g_1558
 * writes: g_13 g_55 g_70 g_60 g_950 g_235 g_925 g_967 g_759 g_974 g_318 g_975 g_1546 g_1507 g_1500 g_1518 g_1673 g_353 g_125 g_1572 g_1516 g_1726 g_1739 g_127 g_214 g_1561 g_1064 g_146 g_2594 g_2596 g_1994 g_1492 g_1540 g_1489 g_1545 g_1554 g_1538 g_1505 g_1527 g_1514 g_206 g_1558
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int8_t *l_11 = (void*)0;
    int8_t *l_12 = &g_13;
    int32_t l_39 = 0x800FF68EL;
    int32_t l_53 = (-9L);
    int64_t *l_54 = &g_55[8][4][0];
    int8_t **l_977 = &l_11;
    uint16_t *l_2595 = &g_2596;
    float l_2597 = 0x0.7p+1;
    uint64_t l_2627 = 0x43D90F6367C85283LL;
    int64_t l_2660 = 0x702D9FAC47A8D4F1LL;
    int32_t l_2706 = (-1L);
    int32_t l_2707 = 6L;
    int32_t l_2709[5][7] = {{0x39886FD0L,1L,0xBE5153D8L,0x39886FD0L,0xC6DD9DCCL,0xC6DD9DCCL,0x39886FD0L},{0xBE5153D8L,0xB7DC391FL,0xBE5153D8L,0xC6DD9DCCL,0xB7DC391FL,1L,1L},{0xB7DC391FL,0x39886FD0L,0xF024F15CL,0x39886FD0L,0xB7DC391FL,0xF024F15CL,0L},{0L,1L,0xC6DD9DCCL,0L,0xC6DD9DCCL,1L,0L},{0xBE5153D8L,0L,1L,0L,0xBE5153D8L,0L,0xF024F15CL}};
    uint64_t l_2750 = 0x7F18C2F98EC12E6FLL;
    int32_t l_2768[4][9][5] = {{{1L,0xC365C925L,5L,0xC365C925L,1L},{9L,0xBAF79D21L,(-1L),0x97CEF86FL,(-1L)},{4L,(-1L),0x6C653551L,3L,0xC365C925L},{0x6C653551L,1L,0L,0xBAF79D21L,(-1L)},{0xDC0C344EL,3L,1L,0x9827F73DL,1L},{(-1L),(-1L),1L,(-1L),0xD37A06B5L},{0xB619A331L,0x2DEDE17BL,0xD37A06B5L,0xDB5340FEL,(-5L)},{0x75C45D5FL,0xFBA2AA5DL,0x9827F73DL,9L,4L},{0x75C45D5FL,(-7L),0xA1BACA4CL,8L,0x9827F73DL}},{{0xB619A331L,0x97CEF86FL,0xE08134C2L,(-1L),0xE08134C2L},{(-1L),(-1L),(-6L),0xD37A06B5L,1L},{0xDC0C344EL,(-1L),0L,(-6L),0L},{0x6C653551L,0xB619A331L,(-7L),(-5L),0x97CEF86FL},{4L,(-1L),3L,(-1L),0L},{9L,(-1L),(-1L),1L,(-1L)},{1L,0x97CEF86FL,0x75C45D5FL,0xA1BACA4CL,1L},{0x8D0C9D60L,(-7L),2L,0xDC0C344EL,1L},{1L,0xFBA2AA5DL,2L,(-1L),9L}},{{2L,0x2DEDE17BL,0x75C45D5FL,1L,5L},{0x2DEDE17BL,(-1L),(-1L),0x2DEDE17BL,0xDC0C344EL},{8L,3L,3L,5L,0x75C45D5FL},{0x9827F73DL,1L,(-7L),0L,(-1L)},{1L,(-1L),0L,5L,0xBAF79D21L},{0xC365C925L,0xBAF79D21L,(-6L),0x2DEDE17BL,0xA1BACA4CL},{(-1L),0xC365C925L,0xE08134C2L,1L,3L},{0xDB5340FEL,4L,0xA1BACA4CL,(-1L),0x55EFF32AL},{1L,3L,0x9827F73DL,0xDC0C344EL,0x55EFF32AL}},{{(-1L),0L,0xD37A06B5L,0xA1BACA4CL,3L},{(-1L),0xA1BACA4CL,1L,1L,0xA1BACA4CL},{0L,1L,1L,(-1L),0xBAF79D21L},{1L,(-1L),0L,(-5L),(-1L)},{5L,0L,0x6C653551L,(-6L),0x75C45D5FL},{1L,5L,(-1L),0xD37A06B5L,0xDC0C344EL},{0L,0x6C653551L,5L,(-1L),5L},{(-1L),2L,1L,8L,9L},{(-1L),0xD37A06B5L,0x97CEF86FL,9L,1L}}};
    int i, j, k;
    if (func_2(((safe_lshift_func_int8_t_s_u(((*l_12) = 0xA4L), 4)) > (((*l_2595) |= (safe_add_func_uint8_t_u_u(g_16[2][0][4], (g_2594 &= (func_17(func_20(func_23(func_28(((*l_977) = func_32(l_12, (g_967 = func_35((l_39 , (g_16[2][0][4] & func_40(g_16[0][1][1], (+((func_44((safe_div_func_int16_t_s_s(((g_16[0][1][4] >= ((((*l_54) = ((safe_div_func_uint16_t_u_u((((0xE3L < (safe_rshift_func_int16_t_s_u((+((0L != g_16[2][0][4]) > l_39)), l_39))) , 0xA442F2AAL) , l_53), l_39)) == l_39)) & g_16[1][1][3]) ^ g_16[5][1][2])) == 8UL), g_16[2][0][4]))) , l_39) >= l_39))))), l_53, g_584)))), g_110, l_39), l_39, g_214, l_53), l_53), &g_1064) < 0x8F8DL))))) == l_53)), l_53, l_39, l_39))
    { /* block id: 1113 */
        int16_t l_2624 = 2L;
        int32_t *l_2642 = &g_975;
        uint32_t l_2667 = 0x1542C0F5L;
        int32_t l_2668 = 0x56C20D25L;
        int32_t l_2705[4] = {0x54100462L,0x54100462L,0x54100462L,0x54100462L};
        uint8_t l_2715 = 249UL;
        uint64_t ***l_2753[8][5] = {{(void*)0,&g_2035[2][0][0],&g_2035[2][0][0],(void*)0,&g_2035[2][0][0]},{(void*)0,(void*)0,(void*)0,&g_2035[2][0][0],(void*)0},{&g_2035[2][0][0],(void*)0,&g_2035[2][0][0],&g_2035[2][0][0],(void*)0},{(void*)0,&g_2035[2][0][0],&g_2035[2][0][0],(void*)0,&g_2035[2][0][0]},{&g_2035[2][0][0],(void*)0,(void*)0,(void*)0,&g_2035[2][0][0]},{&g_2035[2][0][0],(void*)0,&g_2035[2][0][0],&g_2035[2][0][0],(void*)0},{&g_2035[2][0][0],&g_2035[2][0][0],&g_2035[2][0][0],&g_2035[2][0][0],&g_2035[2][0][0]},{(void*)0,&g_2035[2][0][0],(void*)0,(void*)0,(void*)0}};
        uint64_t **** const l_2752 = &l_2753[1][4];
        int i, j;
        if ((0x38DC0A0A953E3AA3LL ^ (safe_div_func_int32_t_s_s((-1L), (safe_mul_func_int8_t_s_s(((*g_967) = (safe_unary_minus_func_uint8_t_u((&g_2241 == &g_2241)))), 0x50L))))))
        { /* block id: 1115 */
            int64_t l_2638 = 9L;
            int32_t l_2641 = (-8L);
            uint32_t **l_2669[1][5] = {{&g_1673,&g_1673,&g_1673,&g_1673,&g_1673}};
            int32_t l_2704[7][6] = {{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL},{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL},{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL},{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL},{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL},{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL},{8L,0x000F91DDL,0x000F91DDL,8L,0x000F91DDL,0x000F91DDL}};
            int64_t l_2708[7][2][2] = {{{0xFF2CD97F49ABFA23LL,0xCE4B953B5F3B14E4LL},{0xFF2CD97F49ABFA23LL,0xFF2CD97F49ABFA23LL}},{{0xCE4B953B5F3B14E4LL,0xFF2CD97F49ABFA23LL},{0xFF2CD97F49ABFA23LL,0xCE4B953B5F3B14E4LL}},{{0xFF2CD97F49ABFA23LL,0xFF2CD97F49ABFA23LL},{0xCE4B953B5F3B14E4LL,0xFF2CD97F49ABFA23LL}},{{0xFF2CD97F49ABFA23LL,0xCE4B953B5F3B14E4LL},{0xFF2CD97F49ABFA23LL,0xFF2CD97F49ABFA23LL}},{{0xCE4B953B5F3B14E4LL,0xFF2CD97F49ABFA23LL},{0xFF2CD97F49ABFA23LL,0xCE4B953B5F3B14E4LL}},{{0xFF2CD97F49ABFA23LL,0xFF2CD97F49ABFA23LL},{0xCE4B953B5F3B14E4LL,0xFF2CD97F49ABFA23LL}},{{0xFF2CD97F49ABFA23LL,0xCE4B953B5F3B14E4LL},{0xFF2CD97F49ABFA23LL,0xFF2CD97F49ABFA23LL}}};
            int8_t l_2725[9] = {0xA5L,(-3L),0xA5L,(-3L),0xA5L,(-3L),0xA5L,(-3L),0xA5L};
            uint32_t l_2735 = 0x45D469E6L;
            int8_t l_2737[8][5] = {{0x64L,0L,0L,0x64L,0x64L},{(-1L),(-3L),(-1L),(-3L),(-1L)},{0x64L,0x64L,0L,0L,0x64L},{0x68L,(-3L),0x68L,(-3L),0x68L},{0x64L,0L,0L,0x64L,0x64L},{(-1L),(-3L),(-1L),(-3L),(-1L)},{0x64L,0x64L,0L,0L,0x64L},{0x68L,(-3L),0x68L,(-3L),0x68L}};
            int i, j, k;
lbl_2702:
            for (g_1994 = 0; (g_1994 <= 3); g_1994 += 1)
            { /* block id: 1118 */
                int16_t l_2639 = (-2L);
                int32_t l_2699 = 0x11DB81B8L;
                int32_t *l_2700 = &g_1572[0][6][4];
                uint32_t l_2701 = 0UL;
                for (g_1492 = 0; (g_1492 <= 3); g_1492 += 1)
                { /* block id: 1121 */
                    uint32_t ****l_2685 = &g_2092;
                    int16_t * const l_2693 = (void*)0;
                    int32_t l_2698[3];
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_2698[i] = 0x71F2BB0EL;
                    if (g_1540[(g_1994 + 5)][g_1994])
                    { /* block id: 1122 */
                        int32_t l_2621 = 0L;
                        int32_t *l_2640[8] = {&g_975,&g_975,(void*)0,&g_975,&g_975,(void*)0,&g_975,&g_975};
                        int i;
                        l_2641 |= (safe_sub_func_int32_t_s_s((((safe_mod_func_int16_t_s_s(((safe_mod_func_int16_t_s_s(((((safe_lshift_func_uint16_t_u_s((0xE79714AEL >= (((safe_sub_func_int32_t_s_s((((((safe_sub_func_uint32_t_u_u((safe_add_func_int32_t_s_s(((-1L) < ((l_2621 & (g_1540[(g_1994 + 5)][g_1994] && (((safe_lshift_func_uint8_t_u_u(l_2624, 1)) != (((*g_213)++) , l_2627)) != (safe_sub_func_uint32_t_u_u((*g_2540), (safe_rshift_func_int16_t_s_s(g_1540[(g_1994 + 5)][g_1994], (safe_mod_func_int16_t_s_s((((((safe_lshift_func_int8_t_s_s(((safe_div_func_uint16_t_u_u((l_2624 , 0x9B57L), l_39)) == 0xAA62L), (*g_967))) && (*g_967)) , l_2638) > g_1540[(g_1994 + 5)][g_1994]) | g_1555[2][2]), l_2624))))))))) ^ l_39)), l_2624)), (*g_2540))) >= l_2624) & (-2L)) != 0UL) , 0x678F69DFL), 4294967287UL)) > l_2621) < (*g_967))), 3)) ^ l_2624) & l_2639) , 1L), l_2638)) || (*g_213)), l_2638)) < (****g_2091)) , 0L), (*g_2540)));
                        (*g_790) = (void*)0;
                        if (l_2639)
                            continue;
                    }
                    else
                    { /* block id: 1127 */
                        (***g_1378) = &l_2641;
                        (***g_1378) = l_2642;
                    }
                    if (((g_1540[(g_1492 + 5)][g_1492] = (safe_sub_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(l_2639, 13)), (safe_sub_func_uint8_t_u_u(((0x3336L >= (safe_sub_func_int8_t_s_s((((((((safe_rshift_func_uint8_t_u_u((!(((safe_rshift_func_uint8_t_u_s(g_16[4][0][0], ((l_2638 && (l_2660 && ((safe_add_func_int8_t_s_s((0L > (safe_lshift_func_int16_t_s_s(((((safe_mul_func_uint16_t_u_u((((*l_54) &= (*l_2642)) , (((*g_967) = 7L) != 0x9EL)), (*l_2642))) , (*g_213)) == (-1L)) & l_53), l_2660))), g_1540[(g_1994 + 5)][g_1994])) , l_2639))) != (*l_2642)))) < (-8L)) >= g_2178)), 0)) < 0x87C2L) == l_2639) <= (*g_2289)) , &g_1642) != (void*)0) && l_2667), 247UL))) != 0x43E0196DA62A7C33LL), (*l_2642))))), g_1540[(g_1994 + 5)][g_1994])), (*g_2540)))) , (*l_2642)))
                    { /* block id: 1134 */
                        uint32_t ***l_2670 = &l_2669[0][0];
                        float *l_2671 = &l_2597;
                        int32_t l_2672[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_2672[i] = 0x2E86ED72L;
                        if (l_2668)
                            break;
                        (*l_2670) = l_2669[0][0];
                        (*l_2671) = 0xD.F2F149p-33;
                        if (l_2672[2])
                            break;
                    }
                    else
                    { /* block id: 1139 */
                        uint32_t *****l_2686 = &l_2685;
                        int32_t l_2689[5][3][6] = {{{(-8L),(-1L),0xE600A4EAL,0x10C4AFB4L,7L,0x10C4AFB4L},{0x10C4AFB4L,7L,0x10C4AFB4L,0xE600A4EAL,(-1L),(-8L)},{7L,0x43FF64FEL,(-1L),0x2FB95E0CL,0xE600A4EAL,8L}},{{0xE600A4EAL,0x85319E86L,0x7597F1B5L,(-6L),0xEB4EF598L,(-7L)},{5L,(-8L),0xF12338E7L,(-7L),0xE600A4EAL,0xE600A4EAL},{0x7597F1B5L,0x10C4AFB4L,0x10C4AFB4L,0x7597F1B5L,1L,0xBE8B2225L}},{{0x2FB95E0CL,7L,0x7597F1B5L,0xE0C2A718L,0x8CC0224CL,0xCB83D582L},{0L,0xE600A4EAL,(-8L),0x19AE76FEL,0x8CC0224CL,0xCD945947L},{(-7L),7L,0x85319E86L,0xF12338E7L,1L,(-7L)}},{{0xE600A4EAL,0x10C4AFB4L,7L,0x10C4AFB4L,0xE600A4EAL,(-1L)},{0xBE8B2225L,(-8L),0x8CC0224CL,0x7597F1B5L,0xEB4EF598L,0x89670CD5L},{0xCB83D582L,8L,0x7597F1B5L,(-8L),(-7L),0x89670CD5L}},{{0xCD945947L,0xE600A4EAL,0x8CC0224CL,0x85319E86L,0xE0C2A718L,(-1L)},{(-7L),0x43FF64FEL,7L,7L,0x43FF64FEL,(-7L)},{(-1L),0xE0C2A718L,0x85319E86L,0x8CC0224CL,0xE600A4EAL,0xCD945947L}}};
                        int16_t *l_2692 = &g_2594;
                        int i, j, k;
                        (*g_790) = &l_39;
                        l_2699 = (((safe_div_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u((0xBD60L && ((safe_rshift_func_uint16_t_u_s((((safe_sub_func_uint8_t_u_u((g_1540[(g_1492 + 1)][g_1994] = l_2641), (safe_rshift_func_uint16_t_u_u(((((safe_div_func_int32_t_s_s(((((*l_2686) = l_2685) != &g_2092) > ((*l_12) &= (safe_add_func_uint16_t_u_u(((l_2689[4][0][0] != ((*l_2692) = (l_53 < (safe_rshift_func_uint16_t_u_u(0xC5E3L, 11))))) && ((*l_2642) = (((l_2693 == (((***g_2092) = (safe_mul_func_uint8_t_u_u((((safe_mod_func_uint16_t_u_u((*g_2289), l_2689[4][0][0])) < l_2689[4][0][0]) , l_2689[4][0][0]), l_2638))) , (void*)0)) >= (*g_967)) != 0x36L))), l_2627)))), 4294967288UL)) > 0x43L) <= 0UL) , l_39), l_2698[2])))) || 0xEEL) != l_2698[2]), l_2638)) , l_2660)), 7)), 7L)) ^ 0xF16BL) & g_256);
                        if ((*l_2642))
                            break;
                    }
                    l_2700 = &l_2641;
                }
                (**g_789) = &l_39;
                (*g_790) = &l_2641;
                for (g_1489 = 0; (g_1489 <= 3); g_1489 += 1)
                { /* block id: 1156 */
                    for (g_1545 = 0; (g_1545 <= 3); g_1545 += 1)
                    { /* block id: 1159 */
                        return l_2701;
                    }
                }
            }
            for (g_1554 = 0; (g_1554 <= 1); g_1554 += 1)
            { /* block id: 1166 */
                int32_t *l_2703[2][10][9] = {{{&g_759,&g_975,(void*)0,&g_1572[0][6][4],&g_759,&l_2641,&l_2641,&g_759,&g_1572[0][6][4]},{(void*)0,&g_975,(void*)0,(void*)0,&g_1998,&g_1998,(void*)0,&g_1572[0][0][3],&l_2641},{(void*)0,&g_759,&l_2641,(void*)0,(void*)0,&l_2641,&g_975,&l_39,&g_975},{&l_39,&g_759,(void*)0,(void*)0,&g_759,&l_39,&g_1998,(void*)0,&g_759},{&g_1572[0][6][4],&g_759,&g_975,&g_1572[0][6][4],&l_39,&g_759,(void*)0,&g_975,&g_975},{&l_2641,&g_975,&g_1998,&g_759,&g_975,&g_759,&g_1998,&g_975,&l_2641},{&g_975,&g_975,(void*)0,&g_759,&l_39,&g_1572[0][6][4],&g_975,&g_759,&g_1572[0][6][4]},{&g_759,(void*)0,&g_1998,&l_39,&g_759,(void*)0,(void*)0,&g_759,&l_39},{&g_975,&l_39,&g_975,&l_2641,(void*)0,(void*)0,&l_2641,&g_759,(void*)0},{&l_2641,&g_1572[0][0][3],(void*)0,&g_1998,&g_1998,(void*)0,(void*)0,&g_975,(void*)0}},{{&g_1572[0][6][4],&g_759,&l_2641,&l_2641,&g_759,&g_1572[0][6][4],(void*)0,&g_975,&g_759},{&l_39,&g_1572[0][0][3],(void*)0,&l_39,&g_975,&g_759,&l_2641,(void*)0,(void*)0},{(void*)0,&l_39,(void*)0,&g_759,&g_759,&g_759,(void*)0,&l_39,(void*)0},{(void*)0,(void*)0,&l_2641,&g_759,&g_975,&l_39,&g_975,&g_759,&l_39},{&g_1572[0][0][4],&g_975,(void*)0,&g_60,&l_2641,(void*)0,(void*)0,&l_2641,&g_60},{&g_975,&g_1998,&g_975,&g_1572[0][6][3],&l_2641,(void*)0,&g_1572[0][6][3],&g_759,&g_60},{&g_759,&g_759,(void*)0,(void*)0,(void*)0,(void*)0,&l_2641,&g_759,&l_2641},{&l_39,(void*)0,&g_1572[0][6][3],&g_1572[0][6][3],(void*)0,&l_39,(void*)0,(void*)0,(void*)0},{&g_60,&g_759,&l_2641,&g_60,&g_759,&g_1572[0][0][4],&g_759,&g_975,&l_2641},{&g_60,&g_1998,(void*)0,(void*)0,&l_39,(void*)0,(void*)0,&g_1998,&g_60}}};
                uint64_t l_2710[4];
                uint32_t l_2733[8][1] = {{0x56537D52L},{5UL},{0x56537D52L},{5UL},{0x56537D52L},{5UL},{0x56537D52L},{5UL}};
                int64_t l_2736 = 0x8F17AE14BEE6A9D7LL;
                uint32_t l_2738 = 4294967289UL;
                int i, j, k;
                for (i = 0; i < 4; i++)
                    l_2710[i] = 0xFB1351C25EF9CCC3LL;
                if (g_2178)
                    goto lbl_2702;
                l_2710[0]--;
                for (g_1538 = 0; (g_1538 <= 2); g_1538 += 1)
                { /* block id: 1171 */
                    float l_2713 = 0x1.Dp-1;
                    int32_t l_2714[10][7][1] = {{{0xBBE8E49CL},{0L},{4L},{0x965D0FE0L},{4L},{0L},{0xBBE8E49CL}},{{1L},{4L},{0x1F0DA2A8L},{(-3L)},{0xAFF6D79EL},{(-5L)},{0L}},{{0x2E345E2EL},{0xCB512BF8L},{(-1L)},{0xCB512BF8L},{0x2E345E2EL},{0L},{(-5L)}},{{0xAFF6D79EL},{(-3L)},{0x1F0DA2A8L},{4L},{1L},{0xBBE8E49CL},{0L}},{{4L},{0x965D0FE0L},{4L},{0L},{0xBBE8E49CL},{1L},{4L}},{{0x1F0DA2A8L},{(-3L)},{0xAFF6D79EL},{(-5L)},{0L},{0x2E345E2EL},{0xCB512BF8L}},{{(-1L)},{0xCB512BF8L},{0x2E345E2EL},{0L},{(-5L)},{0xAFF6D79EL},{(-3L)}},{{0x1F0DA2A8L},{4L},{1L},{0xBBE8E49CL},{0L},{4L},{0x965D0FE0L}},{{4L},{0L},{0xBBE8E49CL},{1L},{4L},{0x1F0DA2A8L},{(-3L)}},{{0xAFF6D79EL},{(-5L)},{0L},{0x2E345E2EL},{0xCB512BF8L},{(-1L)},{0xCB512BF8L}}};
                    uint64_t ****l_2751 = (void*)0;
                    uint16_t l_2767 = 0xBCD5L;
                    uint16_t l_2769 = 65534UL;
                    int i, j, k;
                    for (g_1505 = 0; (g_1505 <= 1); g_1505 += 1)
                    { /* block id: 1174 */
                        int32_t l_2726[8][6] = {{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L},{0x4DAF0A99L,0L,0x4DAF0A99L,0x4DAF0A99L,0L,0x4DAF0A99L}};
                        int16_t *l_2734[3][5][9] = {{{&g_865[5],&g_125[1],&g_125[2],&g_125[2],&g_125[1],&g_865[5],&g_125[3],(void*)0,&g_865[5]},{&g_125[3],&l_2624,&g_125[2],&g_125[1],&g_2348,&g_125[0],(void*)0,&g_865[g_1554],&g_2348},{&g_865[g_1554],&g_865[0],&g_2348,&g_125[2],&l_2624,&g_2348,&g_125[3],&g_125[3],&g_2348},{&g_865[2],&g_865[g_1554],&g_2348,&g_865[g_1554],&g_865[2],&g_865[g_1554],&g_125[2],&l_2624,(void*)0},{(void*)0,&g_865[0],&g_125[2],&g_125[1],(void*)0,&g_125[1],&g_125[2],&g_865[0],(void*)0}},{{&g_2594,&l_2624,&l_2624,(void*)0,&g_2348,&g_865[g_1554],&g_2348,&g_865[g_1554],&g_2348},{&g_865[5],&g_865[g_1554],&g_865[g_1554],&g_865[5],&g_125[1],&g_2348,&g_865[0],&g_2348,&l_2624},{&g_2594,&l_2624,&g_2348,&l_2624,&g_2594,&g_125[0],&g_2348,&g_2348,&g_2348},{(void*)0,&g_125[1],&l_2624,&g_125[3],&g_125[1],&g_2348,&g_2348,&g_125[1],&g_125[3]},{&g_865[2],(void*)0,&g_865[2],&l_2624,&g_2348,&l_2624,&g_125[3],&g_125[1],&g_2348}},{{&g_865[g_1554],(void*)0,(void*)0,&g_2348,(void*)0,(void*)0,&g_865[g_1554],&g_865[0],&l_2624},{&g_125[3],&l_2624,&g_2348,&l_2624,&g_865[2],(void*)0,&g_865[2],&l_2624,&g_2348},{&g_2348,&g_2348,&g_125[1],&g_125[3],&l_2624,&g_125[1],(void*)0,&g_865[0],(void*)0},{&g_2348,&g_125[0],&g_2594,&l_2624,&g_2348,&l_2624,&g_2594,&g_125[1],(void*)0},{&g_865[0],&g_2348,&g_125[1],&g_865[5],&g_865[g_1554],&g_865[g_1554],&g_865[5],&g_125[1],&g_2348}}};
                        int i, j, k;
                        l_2715--;
                        l_2736 |= ((safe_add_func_int8_t_s_s((~(safe_lshift_func_int8_t_s_s(g_865[g_1554], (((((safe_lshift_func_int8_t_s_u((((65535UL >= 0x32F2L) , g_1572[g_1505][g_1505][(g_1554 + 3)]) == (l_2726[6][0] = l_2725[4])), (l_2714[7][2][0] ^ (safe_add_func_int16_t_s_s((l_2707 ^= (safe_mod_func_int8_t_s_s(((*g_967) = (safe_sub_func_int64_t_s_s(0x2B3C4D1DA18158E5LL, ((*g_213) = (*g_213))))), (((l_2704[3][3] ^= ((0x9FL | l_2733[4][0]) && (*l_2642))) , g_865[g_1554]) , l_2725[7])))), (*l_2642)))))) || l_2714[3][3][0]) ^ l_2725[7]) < 0x7EA474BFL) & l_2735)))), (-2L))) != l_2706);
                        ++l_2738;
                        (**g_789) = g_1922[g_1554];
                    }
                    for (g_1527 = 0; (g_1527 <= 1); g_1527 += 1)
                    { /* block id: 1187 */
                        uint8_t l_2745 = 0xECL;
                        float *l_2749[2][9][4] = {{{&g_206,&l_2597,&l_2597,&l_2597},{&g_353,&g_353,&l_2597,&l_2713},{&l_2597,&g_206,&g_206,&l_2597},{&g_206,&l_2597,&g_206,&g_206},{&g_353,&g_353,&g_206,&l_2713},{&l_2597,&g_206,&g_206,&l_2713},{&g_206,&g_353,&l_2597,&g_206},{&l_2713,&l_2597,&g_206,&l_2597},{&g_353,&g_206,&g_206,&l_2713}},{{&l_2597,&g_353,&g_206,&l_2597},{&g_353,&l_2597,&l_2597,&l_2713},{&g_353,&g_206,&g_206,&l_2713},{&l_2597,&l_2713,&g_206,&g_206},{&g_353,&g_353,&g_206,&l_2713},{&l_2713,&l_2597,&l_2597,&l_2597},{&g_206,&g_353,&g_206,&l_2597},{&g_206,&g_206,&l_2713,&g_206},{&g_206,&g_206,&g_353,&g_206}}};
                        uint8_t *l_2762 = &g_1514;
                        int i, j, k;
                        l_2769 = ((*g_500) = ((((safe_add_func_float_f_f((((-g_865[(g_1554 + 4)]) >= ((*l_2642) = (!((l_2745 <= ((l_2641 , ((safe_add_func_float_f_f((((l_2745 < (+((l_2750 = l_2714[7][4][0]) >= (((l_2751 != l_2752) == ((((((safe_add_func_uint16_t_u_u(((((safe_rshift_func_uint8_t_u_s((--g_235[(g_1538 + 1)][g_1554]), 4)) & (safe_div_func_uint8_t_u_u(((*l_2762) = 0x56L), ((safe_add_func_uint8_t_u_u(((safe_add_func_uint8_t_u_u(l_2767, l_2714[7][4][0])) , g_1533), (*g_967))) && l_2709[4][1])))) , (*l_2642)) ^ l_2660), l_2745)) == l_2660) , 0UL) ^ l_2745) == l_2641) | l_53)) , l_2767)))) == l_39) <= l_2768[3][8][0]), (*g_352))) <= (*l_2642))) > 0xF.DA534Ep-90)) > 0x7.20BB50p-64)))) != l_2725[4]), 0x3.06044Cp-10)) >= g_1505) >= l_2737[0][0]) == 0x3.A18243p+37));
                    }
                    l_2714[0][4][0] = (*l_2642);
                }
            }
        }
        else
        { /* block id: 1198 */
            (**g_789) = &l_2707;
            (***g_1378) = &l_39;
        }
        return (****g_2537);
    }
    else
    { /* block id: 1203 */
        int32_t *l_2772[10];
        uint64_t l_2774 = 18446744073709551615UL;
        int i;
        for (i = 0; i < 10; i++)
            l_2772[i] = &l_2709[0][6];
        for (g_1558 = 0; (g_1558 < 49); g_1558 = safe_add_func_uint32_t_u_u(g_1558, 6))
        { /* block id: 1206 */
            uint16_t l_2773 = 65526UL;
            (**g_789) = l_2772[6];
            (*g_790) = l_2772[9];
            l_2774 &= l_2773;
        }
        l_2709[0][6] |= l_2768[1][1][0];
    }
    return l_2709[0][6];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int32_t  func_2(uint32_t  p_3, uint16_t  p_4, int64_t  p_5, int32_t  p_6)
{ /* block id: 1109 */
    int32_t l_2600 = 8L;
    int32_t l_2601[5][7][7] = {{{4L,0L,0x61BEF8F1L,(-8L),1L,0xF61FBEE4L,8L},{(-1L),0L,2L,2L,(-3L),(-2L),0L},{0xD1F4B11EL,1L,(-8L),(-5L),0xE6D2AF7DL,0xE6D2AF7DL,(-5L)},{0xCC8E6A1FL,0x07F6E1ABL,0xCC8E6A1FL,0x68AE5DE1L,0L,(-4L),8L},{0x4A0615B9L,0xD5C49A5CL,0xDAF02A73L,0L,0L,2L,6L},{2L,(-1L),(-1L),(-1L),1L,(-4L),1L},{0xF5035BC8L,1L,0L,0x61BEF8F1L,0xF4C952C1L,0xE6D2AF7DL,(-1L)}},{{6L,(-6L),(-2L),0x151F52D0L,1L,(-2L),1L},{0x151F52D0L,0L,(-4L),0L,1L,0xF61FBEE4L,0x9C014595L},{1L,0x2443A2CDL,(-8L),0x76A57F8FL,0xD1F4B11EL,3L,0L},{(-1L),(-1L),1L,(-3L),(-1L),(-7L),0x4A0615B9L},{0L,0L,0xF5035BC8L,1L,0xE6D2AF7DL,0x68AE5DE1L,(-1L)},{(-1L),0L,0xF5035BC8L,0x61BEF8F1L,0L,8L,0xDD7182D5L},{0L,0xD5C49A5CL,1L,(-5L),(-1L),0x109D3251L,(-1L)}},{{0xF4C952C1L,0x707FB6F4L,(-8L),(-1L),0x80DDAC59L,(-4L),1L},{(-8L),2L,(-4L),0x68AE5DE1L,0xC6CE49B2L,0x61BEF8F1L,0L},{2L,(-3L),(-2L),0L,0L,0xDAF02A73L,0L},{0x8B42C1FFL,0L,0L,0x8B42C1FFL,(-8L),0L,0x9C014595L},{(-8L),1L,(-1L),(-8L),1L,(-1L),0xB4B9699EL},{1L,0x4A0615B9L,0xDAF02A73L,(-3L),1L,4L,0x9C014595L},{0xD1F4B11EL,0L,0xCC8E6A1FL,0x2443A2CDL,0x0AEF52FCL,0xDA9EC0A7L,0L}},{{(-1L),(-1L),(-8L),0xDA9EC0A7L,0L,(-1L),0L},{0xA2DADA72L,0L,2L,1L,(-1L),4L,1L},{1L,(-1L),0x61BEF8F1L,2L,0xDA9EC0A7L,8L,(-1L)},{1L,2L,0L,0x80DDAC59L,(-6L),0xE6D2AF7DL,0xDD7182D5L},{0xF4C952C1L,7L,(-2L),0x9C014595L,(-5L),0xDAF02A73L,(-8L)},{0x707FB6F4L,(-6L),(-2L),0L,0x4B72D0E5L,0x2BA7E05BL,0x707FB6F4L},{0x2BA7E05BL,0x61BEF8F1L,0xAEFCE2A1L,0x05293A67L,0x68AE5DE1L,0xC1BCA0F0L,4L}},{{1L,(-1L),(-1L),4L,3L,0xC6CE49B2L,0L},{0x74AF8F41L,0xDAF02A73L,7L,0xDA9EC0A7L,(-1L),0xAEFCE2A1L,0x0AEF52FCL},{0xA928B4CBL,0x707FB6F4L,0xC1BCA0F0L,0x4ACBF304L,(-1L),1L,1L},{8L,0x74AF8F41L,(-3L),0x74AF8F41L,8L,2L,0xF61FBEE4L},{0xCC8E6A1FL,0xDAF02A73L,0x0FBC4B30L,0xF5035BC8L,0x05293A67L,2L,0x470C34D3L},{1L,0xF61FBEE4L,(-6L),(-1L),1L,(-4L),(-2L)},{0xCC8E6A1FL,0xF5035BC8L,1L,0L,0x61BEF8F1L,0xF4C952C1L,0xE6D2AF7DL}}};
    int i, j, k;
    l_2601[0][2][5] |= (safe_lshift_func_uint16_t_u_s((p_4 = l_2600), 15));
    return p_6;
}


/* ------------------------------------------ */
/* 
 * reads : g_127 g_1378 g_789 g_790 g_1523 g_352 g_353 g_213 g_1534 g_967 g_925 g_1766 g_1064 g_164 g_146 g_759 g_60
 * writes: g_127 g_318 g_214 g_1561 g_1064 g_60 g_146
 */
static uint16_t  func_17(int8_t * p_18, int8_t * p_19)
{ /* block id: 736 */
    uint64_t l_1750 = 0x93D672167B7200D9LL;
    uint32_t *l_1765 = &g_70[2][4][4];
    uint32_t **l_1764 = &l_1765;
    uint8_t *l_1768 = (void*)0;
    uint8_t *l_1769 = &g_1561;
    int32_t l_1770[9][8][2] = {{{0L,(-7L)},{(-4L),(-8L)},{(-10L),(-7L)},{0L,0L},{0L,(-10L)},{(-5L),3L},{(-1L),(-10L)},{0x7D45CE0BL,3L}},{{(-7L),3L},{0x7D45CE0BL,(-10L)},{(-1L),3L},{(-5L),(-10L)},{0L,0L},{0L,(-7L)},{(-10L),(-8L)},{(-4L),(-7L)}},{{0L,0x00523C6FL},{0L,0x8F620289L},{(-10L),0x563520B3L},{(-1L),1L},{1L,0xFE65F2AAL},{0x1699A855L,0x1699A855L},{0x585209B7L,(-4L)},{0x8F620289L,0x3E07B792L}},{{(-6L),(-7L)},{3L,(-6L)},{(-1L),1L},{(-1L),(-6L)},{3L,(-7L)},{(-6L),0x3E07B792L},{0x8F620289L,(-4L)},{0x585209B7L,0x1699A855L}},{{0x1699A855L,0xFE65F2AAL},{1L,1L},{(-1L),0x563520B3L},{(-10L),0x8F620289L},{0L,0x00523C6FL},{0L,(-7L)},{(-4L),(-8L)},{(-10L),(-7L)}},{{0L,0L},{0L,(-10L)},{(-5L),3L},{(-1L),(-10L)},{0x7D45CE0BL,3L},{(-7L),3L},{0x7D45CE0BL,(-10L)},{(-1L),3L}},{{(-5L),(-10L)},{0L,0L},{0L,(-7L)},{(-10L),(-8L)},{(-4L),(-7L)},{0L,0x00523C6FL},{0L,0x8F620289L},{(-10L),0x563520B3L}},{{(-1L),1L},{1L,0xFE65F2AAL},{0x1699A855L,0x1699A855L},{0x585209B7L,(-4L)},{0x8F620289L,0x3E07B792L},{(-6L),(-7L)},{3L,(-6L)},{(-1L),0x00523C6FL}},{{(-7L),0x563520B3L},{0L,(-10L)},{0x563520B3L,0x6DA47D93L},{(-8L),0x7D45CE0BL},{(-7L),(-1L)},{(-1L),0x1699A855L},{0x00523C6FL,(-6L)},{(-10L),(-4L)}}};
    int32_t l_1771 = 0xA863F3A7L;
    int32_t l_1772 = 1L;
    int32_t *l_1773 = &g_60;
    int32_t *l_1784 = &g_759;
    int64_t l_1823 = 0x683072D848EA9CA8LL;
    int16_t **l_1844 = &g_337[1];
    int32_t l_1881 = 0xAD609C7FL;
    int32_t l_1882[4][5] = {{0x26855164L,1L,1L,0x26855164L,0x26855164L},{8L,(-2L),8L,(-2L),8L},{0x26855164L,0x26855164L,1L,1L,0x26855164L},{(-3L),(-2L),(-3L),(-2L),(-3L)}};
    int64_t *l_1912 = &g_1642;
    uint16_t *l_1923 = &g_950;
    uint64_t **l_1946 = (void*)0;
    int64_t l_1958 = 8L;
    int32_t ***l_1961 = &g_790;
    int32_t l_1985[2][2][9] = {{{2L,(-1L),(-8L),0x958DEF61L,0x91C4409AL,7L,0x6B735905L,0x6B735905L,7L},{0x958DEF61L,0x07A96253L,0xDC03447FL,0x07A96253L,0x958DEF61L,(-1L),0xEDADA38CL,(-8L),(-1L)}},{{0xEDADA38CL,(-1L),0x958DEF61L,0x07A96253L,0xDC03447FL,0x07A96253L,0x958DEF61L,(-1L),0xEDADA38CL},{0x6B735905L,7L,0x91C4409AL,0x958DEF61L,(-8L),(-1L),2L,(-1L),(-8L)}}};
    uint64_t l_2135 = 0x433534B73541F4A4LL;
    int32_t *****l_2140 = &g_1378;
    uint16_t ***l_2189 = &g_664;
    float l_2216 = (-0x10.4p-1);
    int16_t l_2238 = 1L;
    const int32_t l_2347 = 0x214550C2L;
    uint32_t l_2474[3][6][8] = {{{1UL,0x8928A5A5L,0xCAFB7451L,0xF1FDF211L,4UL,1UL,1UL,0x76BB3986L},{0UL,0x9CBBF9B9L,0x5D151C89L,0x8928A5A5L,4294967292UL,1UL,4294967292UL,1UL},{0x76BB3986L,0x602F62C5L,0xD7F7FB4EL,0UL,0xA921E9CBL,4UL,8UL,4UL},{0x9CBBF9B9L,8UL,0x76BB3986L,8UL,0x9CBBF9B9L,0x8928A5A5L,0UL,0xF5524AC2L},{0xF1FDF211L,0UL,0UL,1UL,0xC6798C4CL,1UL,0xCAFB7451L,8UL},{1UL,1UL,0UL,5UL,0x8928A5A5L,0xC6798C4CL,0UL,0x9CBBF9B9L}},{{0xC6798C4CL,4UL,0x76BB3986L,0xF5524AC2L,1UL,0xE61C9D4DL,8UL,0x4C654F5CL},{0x97C7B057L,0UL,0xD7F7FB4EL,4294967292UL,4294967295UL,4294967295UL,4294967292UL,0xD7F7FB4EL},{1UL,1UL,0x5D151C89L,0xF8CDF2B2L,0x4C654F5CL,0xA921E9CBL,1UL,5UL},{0UL,4294967295UL,0xCAFB7451L,4294967292UL,0UL,0x602F62C5L,0xE61C9D4DL,5UL},{4294967295UL,5UL,4294967295UL,0xF8CDF2B2L,4294967292UL,0x76BB3986L,0x602F62C5L,0xD7F7FB4EL},{0x8928A5A5L,4294967292UL,1UL,4294967292UL,1UL,8UL,0xF1FDF211L,0x4C654F5CL}},{{0x4C654F5CL,0UL,0x602F62C5L,0xF5524AC2L,0x602F62C5L,0UL,0x4C654F5CL,0x9CBBF9B9L},{0x60250323L,1UL,0xF77F1E7FL,5UL,1UL,0UL,0UL,8UL},{0xD7F7FB4EL,0xF8CDF2B2L,0x9CBBF9B9L,1UL,1UL,4294967292UL,0xD7F7FB4EL,0UL},{5UL,0x8928A5A5L,0xC6798C4CL,0UL,0x9CBBF9B9L,0x97C7B057L,0UL,0xD7F7FB4EL},{0x60250323L,4UL,0x97C7B057L,0xF5524AC2L,0xCAFB7451L,1UL,1UL,0xCAFB7451L},{0x5D151C89L,1UL,1UL,0x5D151C89L,0xF8CDF2B2L,0x4C654F5CL,0xA921E9CBL,1UL}}};
    uint64_t ***l_2490 = &l_1946;
    uint64_t ****l_2489 = &l_2490;
    uint32_t * const **l_2536 = (void*)0;
    uint32_t * const *** const l_2535 = &l_2536;
    int i, j, k;
    for (g_127 = 0; (g_127 <= 9); g_127 += 1)
    { /* block id: 739 */
        int i;
        (***g_1378) = (void*)0;
        if (g_1523[g_127])
            continue;
    }
    l_1772 = (((safe_add_func_int8_t_s_s(((l_1771 &= ((*g_352) , (!((*p_19) = (safe_rshift_func_uint8_t_u_s((((safe_sub_func_int64_t_s_s(l_1750, 5UL)) < (((((*g_213) = l_1750) == l_1750) < g_1534) || (((safe_unary_minus_func_int64_t_s(((safe_mul_func_uint8_t_u_u(((*l_1769) = (safe_sub_func_int8_t_s_s((safe_mod_func_int64_t_s_s(((safe_add_func_uint32_t_u_u(l_1750, l_1750)) >= ((safe_div_func_int32_t_s_s((safe_mod_func_uint8_t_u_u((((*g_967) , l_1764) != g_1766), (*p_18))), l_1750)) != (*g_967))), l_1750)), 0x18L))), (*g_967))) | 0x99456AD087EC2471LL))) | l_1750) < l_1750))) & l_1770[8][0][1]), 1)))))) , 0L), (-7L))) >= l_1770[8][0][1]) | 255UL);
    (*l_1773) = 0x1EE3FDC1L;
    for (g_146 = 0; (g_146 >= 13); ++g_146)
    { /* block id: 751 */
        int32_t l_1776 = 0x4AA7478DL;
        uint8_t *l_1779 = &g_1527;
        uint8_t *l_1780 = &g_1489;
        uint8_t **l_1781 = &l_1780;
        int32_t **l_1785 = &l_1773;
        int32_t l_1786 = 0x0F65AB87L;
        int64_t l_1803 = 4L;
        int64_t l_1822 = 0xD5FE8B4F9A54EB0ELL;
        uint32_t l_1856 = 4294967295UL;
        int16_t l_1862 = 0xC46BL;
        int32_t l_1878 = (-1L);
        int16_t l_1894[3];
        int32_t l_1895 = (-1L);
        int32_t l_1896 = (-1L);
        int32_t l_1897 = 0x201D1AC4L;
        int32_t l_1898 = 0x67D2A7C2L;
        int32_t *l_1919 = (void*)0;
        int32_t **l_1918 = &l_1919;
        int16_t **l_1937 = &g_337[1];
        uint16_t *l_1995 = &g_950;
        float l_1999 = (-0x4.3p-1);
        uint64_t l_2019 = 0UL;
        const uint64_t **l_2037 = (void*)0;
        const uint64_t ***l_2036[1][2][1];
        uint8_t l_2062 = 0x86L;
        uint16_t l_2111 = 0x791AL;
        int32_t * const ****l_2141[5] = {&g_1598,&g_1598,&g_1598,&g_1598,&g_1598};
        uint16_t ***l_2158 = &g_664;
        float l_2214 = 0xB.CBA78Bp-14;
        uint32_t l_2314 = 4294967295UL;
        int16_t l_2426 = 0xD891L;
        uint64_t **l_2483 = &g_213;
        uint32_t l_2523 = 0x1745E15DL;
        uint64_t l_2553 = 0x1F26A2760B0B6822LL;
        uint16_t l_2563 = 65529UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1894[i] = 0xF5C1L;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 1; k++)
                    l_2036[i][j][k] = &l_2037;
            }
        }
        l_1786 = (l_1776 && ((safe_sub_func_uint32_t_u_u(4294967288UL, (-6L))) & ((l_1779 != ((*l_1781) = l_1780)) <= (safe_rshift_func_uint16_t_u_u((((l_1784 = ((*l_1785) = l_1784)) == (void*)0) , (*l_1784)), l_1770[3][5][0])))));
    }
    return (*l_1773);
}


/* ------------------------------------------ */
/* 
 * reads : g_1546 g_1507 g_1535 g_1504 g_60 g_967 g_925 g_1542 g_213 g_214 g_1642 g_950 g_1572 g_865 g_1549 g_1053 g_125 g_1518 g_500 g_206 g_1525 g_1492 g_1725 g_789 g_790
 * writes: g_1546 g_1507 g_950 g_60 g_1500 g_1518 g_1673 g_353 g_125 g_70 g_1572 g_318 g_1516 g_1726 g_1739
 */
static int8_t * func_20(uint8_t  p_21, int8_t  p_22)
{ /* block id: 674 */
    int64_t l_1641[5][6][1] = {{{(-1L)},{7L},{0x6A2C265F82AE018CLL},{0x841FD317DE79C2AALL},{1L},{0x0227848EC8934C4ALL}},{{0x0227848EC8934C4ALL},{1L},{0x841FD317DE79C2AALL},{0x6A2C265F82AE018CLL},{7L},{(-1L)}},{{7L},{0x6A2C265F82AE018CLL},{0x841FD317DE79C2AALL},{1L},{0x0227848EC8934C4ALL},{0x0227848EC8934C4ALL}},{{1L},{0x841FD317DE79C2AALL},{0x6A2C265F82AE018CLL},{7L},{(-1L)},{7L}},{{0x6A2C265F82AE018CLL},{0x841FD317DE79C2AALL},{1L},{0x0227848EC8934C4ALL},{0x0227848EC8934C4ALL},{1L}}};
    int32_t **l_1656 = &g_318[4];
    uint16_t ***l_1664 = &g_664;
    uint16_t ****l_1663 = &l_1664;
    int32_t l_1713[4][10] = {{0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L},{0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L},{0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L},{0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L,0x8698A4BBL,0x6B6E8538L}};
    uint16_t l_1721 = 65528UL;
    float *l_1737[3];
    uint64_t ***l_1741 = (void*)0;
    uint64_t **l_1742 = &g_213;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1737[i] = &g_206;
    for (g_1546 = 0; (g_1546 <= 1); g_1546 += 1)
    { /* block id: 677 */
        uint32_t *l_1633 = &g_70[4][2][4];
        int32_t l_1640 = 0x63E37A75L;
        int32_t l_1644[9];
        uint16_t *l_1653 = &g_950;
        uint16_t * const * const l_1652 = &l_1653;
        uint16_t * const * const *l_1651 = &l_1652;
        uint16_t * const * const **l_1650 = &l_1651;
        uint16_t * const * const ***l_1649[7] = {&l_1650,&l_1650,&l_1650,&l_1650,&l_1650,&l_1650,&l_1650};
        float **l_1738[7][10][2] = {{{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[0],&l_1737[2]},{(void*)0,&l_1737[2]},{&l_1737[0],&l_1737[0]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[2],&l_1737[1]}},{{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[2],&l_1737[0]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[0],&l_1737[2]},{(void*)0,&l_1737[2]},{&l_1737[0],&l_1737[0]},{&l_1737[1],&l_1737[1]}},{{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[2],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[2],&l_1737[0]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[0],&l_1737[2]}},{{(void*)0,&l_1737[2]},{&l_1737[0],&l_1737[0]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[2],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[2],&l_1737[0]},{&l_1737[1],&l_1737[1]}},{{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],(void*)0},{&l_1737[0],(void*)0},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[1],&l_1737[2]},{&l_1737[1],&l_1737[0]},{(void*)0,&l_1737[1]},{&l_1737[1],&l_1737[1]}},{{&l_1737[1],&l_1737[1]},{(void*)0,&l_1737[0]},{&l_1737[1],&l_1737[2]},{&l_1737[1],&l_1737[0]},{&l_1737[1],&l_1737[1]},{&l_1737[1],(void*)0},{&l_1737[0],(void*)0},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[0]},{&l_1737[1],&l_1737[2]}},{{&l_1737[1],&l_1737[0]},{(void*)0,&l_1737[1]},{&l_1737[1],&l_1737[1]},{&l_1737[1],&l_1737[1]},{(void*)0,&l_1737[0]},{&l_1737[1],&l_1737[2]},{&l_1737[1],&l_1737[0]},{&l_1737[1],&l_1737[1]},{&l_1737[1],(void*)0},{&l_1737[0],(void*)0}}};
        int32_t *l_1740[8] = {&g_1572[0][4][5],&g_1572[0][4][5],&g_1572[0][4][5],&g_1572[0][4][5],&g_1572[0][4][5],&g_1572[0][4][5],&g_1572[0][4][5],&g_1572[0][4][5]};
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1644[i] = 1L;
        for (g_1507 = 0; (g_1507 <= 1); g_1507 += 1)
        { /* block id: 680 */
            int32_t *l_1632 = &g_60;
            uint32_t **l_1634 = &l_1633;
            uint16_t *l_1643 = &g_950;
            uint16_t ****l_1662 = (void*)0;
            int32_t l_1710 = 0xED343CADL;
            int32_t l_1711 = 0L;
            int32_t l_1717 = 1L;
            int32_t l_1718 = 0x74115FCDL;
            int32_t l_1719 = 0L;
            int32_t l_1720 = 0xBEF3D4F1L;
            int i, j;
            (*l_1632) = (l_1644[5] &= (safe_mul_func_int16_t_s_s(g_1535[g_1507], ((g_1504[(g_1507 + 5)][(g_1507 + 4)] != g_1504[(g_1507 + 1)][(g_1507 + 1)]) ^ ((((safe_div_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u(((*l_1643) |= (!(((((safe_sub_func_int16_t_s_s((((safe_sub_func_uint32_t_u_u(((((safe_lshift_func_uint16_t_u_u((l_1632 != l_1632), 13)) || (((*l_1634) = l_1633) == (((safe_sub_func_uint32_t_u_u((((safe_lshift_func_uint16_t_u_u((*l_1632), (((+p_21) , p_21) >= p_21))) > 1UL) | (*g_967)), l_1640)) | 1UL) , &g_70[0][7][5]))) & p_22) ^ p_21), g_1542[4][3][2])) & 1L) && l_1641[0][0][0]), 0x0E16L)) , (*g_213)) & p_22) >= (*g_213)) < g_1642))), 0x99E4L)), (*l_1632))) > 1UL) <= 1L) < 0xCEF9L)))));
            for (g_1500 = 0; (g_1500 <= 1); g_1500 += 1)
            { /* block id: 687 */
                const int32_t *l_1658 = &g_1572[0][2][1];
                const int32_t **l_1657 = &l_1658;
                int16_t *l_1685[6] = {&g_125[1],&g_125[1],&g_125[1],&g_125[1],&g_125[1],&g_125[1]};
                int16_t l_1704[10][7] = {{1L,0xA283L,0xDACEL,0x73FDL,0xDBD8L,0x6434L,1L},{1L,0xBDD1L,(-1L),0xDACEL,(-1L),0xBDD1L,1L},{1L,0x6434L,0xDBD8L,0x73FDL,0xDACEL,0xA283L,1L},{0xDACEL,1L,(-5L),(-5L),1L,0xDACEL,1L},{0xBDD1L,(-5L),0xDBD8L,1L,6L,0xDACEL,0xDACEL},{0x73FDL,6L,(-1L),6L,0x73FDL,0xA283L,0xBDD1L},{0x1FE6L,(-5L),0xDACEL,0x6434L,0x73FDL,0xBDD1L,0x73FDL},{1L,1L,1L,1L,6L,0x6434L,0x1FE6L},{0x1FE6L,0x6434L,6L,1L,1L,1L,1L},{0x73FDL,0xBDD1L,0x73FDL,0x6434L,0xDACEL,(-5L),0x1FE6L}};
                int32_t l_1705[3];
                int32_t l_1712 = 6L;
                int32_t l_1715[10] = {0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L,0x1D871D34L};
                int i, j;
                for (i = 0; i < 3; i++)
                    l_1705[i] = 0x14A28A0FL;
                for (g_1518 = 0; (g_1518 <= 1); g_1518 += 1)
                { /* block id: 690 */
                    int64_t *l_1647[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int32_t l_1648 = 0x2F45DFFAL;
                    int32_t l_1661 = 0x29315C99L;
                    int16_t *l_1684 = &g_125[1];
                    int i;
                    if ((g_1504[8][3] , (((safe_mul_func_uint8_t_u_u(((l_1648 = (l_1644[5] = p_22)) > ((&g_824 == l_1649[0]) >= ((((safe_mul_func_uint16_t_u_u(((l_1656 == l_1657) == p_22), (safe_lshift_func_int16_t_s_s(l_1661, 5)))) ^ (l_1662 == l_1663)) < 0L) , l_1661))), (*g_967))) < (**l_1657)) ^ (-9L))))
                    { /* block id: 693 */
                        uint32_t *l_1671 = (void*)0;
                        int32_t l_1674[1][3];
                        float *l_1675 = (void*)0;
                        float *l_1676 = &g_353;
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 3; j++)
                                l_1674[i][j] = 0xA5750F7DL;
                        }
                        if (l_1640)
                            break;
                        (*l_1657) = (*l_1657);
                        (*l_1676) = ((safe_rshift_func_uint16_t_u_u(((~((~p_22) >= p_21)) != (safe_add_func_int8_t_s_s((*g_967), (((((void*)0 != l_1671) , (((~(**l_1657)) | ((l_1644[3] , &g_222) == (g_1673 = &g_1053))) && 0x01501AD4L)) , 1UL) ^ 0x5642L)))), 1)) , l_1674[0][1]);
                    }
                    else
                    { /* block id: 698 */
                        return &g_1064;
                    }
                    if ((safe_unary_minus_func_uint16_t_u((((&g_865[0] == ((safe_sub_func_int32_t_s_s(((*l_1634) == (void*)0), ((*l_1633) = (((*l_1684) |= (((void*)0 != &l_1650) | ((g_865[0] > ((safe_add_func_uint64_t_u_u(l_1661, (safe_mod_func_uint8_t_u_u((l_1640 , ((g_1549 < g_1053) , p_21)), 0xC8L)))) & p_22)) , p_22))) || p_21)))) , l_1685[1])) != 4294967295UL) < l_1644[5]))))
                    { /* block id: 703 */
                        int i, j, k;
                        (*l_1632) = (safe_rshift_func_uint16_t_u_s((safe_sub_func_int32_t_s_s((l_1648 &= (((g_1572[g_1518][(g_1546 + 5)][(g_1518 + 5)] ^= p_22) , p_21) , g_1572[g_1518][(g_1546 + 5)][(g_1518 + 5)])), p_22)), 2));
                    }
                    else
                    { /* block id: 707 */
                        int32_t *l_1690[10] = {&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3],&g_1572[0][3][3]};
                        int i;
                        g_318[(g_1518 + 2)] = l_1690[0];
                    }
                }
                for (g_1516 = 0; (g_1516 <= 1); g_1516 += 1)
                { /* block id: 713 */
                    uint8_t l_1700 = 253UL;
                    float *l_1703 = &g_353;
                    int32_t l_1714 = 0x821EAE94L;
                    int32_t l_1716[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1716[i] = (-2L);
                    if ((((safe_add_func_float_f_f((+(p_22 , ((((*l_1703) = (0x8.3p+1 >= (safe_sub_func_float_f_f((((safe_div_func_float_f_f(((void*)0 == &g_967), (*g_500))) == (((*g_967) , (safe_rshift_func_int8_t_s_s(l_1700, 1))) , ((l_1644[0] > ((safe_mul_func_int8_t_s_s((*l_1632), 0xF2L)) , l_1700)) >= g_1525[4][2][1]))) == l_1700), g_1492)))) >= (-0x9.5p-1)) == p_22))), 0xE.3C6512p+87)) >= l_1704[9][3]) , l_1705[0]))
                    { /* block id: 715 */
                        int i;
                        if ((*l_1632))
                            break;
                        g_318[(g_1546 + 1)] = &l_1640;
                    }
                    else
                    { /* block id: 718 */
                        int32_t *l_1706 = &g_1572[0][4][7];
                        int32_t *l_1707 = &l_1644[8];
                        int32_t *l_1708 = &g_1572[1][2][7];
                        int32_t *l_1709[7][4] = {{&l_1640,(void*)0,(void*)0,&l_1640},{(void*)0,&l_1640,(void*)0,(void*)0},{&l_1640,&l_1640,&l_1644[8],&l_1640},{&l_1640,(void*)0,(void*)0,&l_1640},{(void*)0,&l_1640,(void*)0,(void*)0},{&l_1640,(void*)0,&l_1640,(void*)0},{(void*)0,&l_1644[8],&l_1644[8],(void*)0}};
                        uint64_t l_1736 = 0UL;
                        int i, j, k;
                        l_1721++;
                        (*g_1725) = ((*l_1657) = &l_1717);
                        if (p_21)
                            break;
                        l_1736 ^= ((safe_add_func_int64_t_s_s((((l_1715[7] = ((**l_1657) | ((((safe_add_func_int8_t_s_s(((safe_add_func_float_f_f(((*l_1703) = (0x1.Cp-1 > ((p_21 != 0x0.6p+1) <= (safe_add_func_float_f_f((+p_21), l_1714))))), (*g_500))) , (255UL && 1UL)), 0UL)) , 6L) >= p_22) < l_1716[0]))) < 0UL) | p_21), 0xE1B0E2B6D1566201LL)) == p_22);
                    }
                }
            }
        }
        l_1713[3][3] |= (l_1633 != (g_1739 = l_1737[1]));
    }
    l_1742 = (void*)0;
    (**g_789) = &l_1713[3][6];
    return &g_164[5];
}


/* ------------------------------------------ */
/* 
 * reads : g_975 g_1542
 * writes: g_950 g_975 g_60
 */
static uint8_t  func_23(int8_t * p_24, float  p_25, float  p_26, int64_t  p_27)
{ /* block id: 383 */
    int32_t ****l_991 = (void*)0;
    int32_t ****l_992[7] = {&g_789,&g_789,&g_789,&g_789,&g_789,&g_789,&g_789};
    uint32_t *l_1096 = (void*)0;
    uint32_t l_1246 = 0x9F39FE7EL;
    uint16_t ****l_1270 = (void*)0;
    float l_1312 = 0x8.F4C521p+0;
    uint8_t l_1314 = 254UL;
    uint16_t l_1330[5] = {0xF124L,0xF124L,0xF124L,0xF124L,0xF124L};
    int16_t **l_1436 = &g_337[0];
    int16_t * const l_1569 = (void*)0;
    float l_1612 = 0x2.2E0C8Ep+1;
    uint64_t l_1617 = 18446744073709551615UL;
    int i;
    for (p_27 = 20; (p_27 < (-2)); p_27 = safe_sub_func_uint32_t_u_u(p_27, 2))
    { /* block id: 386 */
        int32_t *l_984 = &g_975;
        int32_t l_997[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
        uint32_t l_1048 = 0x662203F2L;
        int8_t **l_1070[9][5] = {{&g_967,&g_967,(void*)0,&g_967,&g_967},{&g_967,&g_967,&g_967,&g_967,&g_967},{(void*)0,&g_967,(void*)0,&g_967,&g_967},{&g_967,(void*)0,&g_967,(void*)0,&g_967},{&g_967,&g_967,&g_967,&g_967,(void*)0},{&g_967,&g_967,&g_967,&g_967,&g_967},{&g_967,(void*)0,(void*)0,&g_967,(void*)0},{(void*)0,&g_967,&g_967,&g_967,&g_967},{(void*)0,(void*)0,&g_967,&g_967,&g_967}};
        int32_t ****l_1101 = &g_789;
        int8_t l_1106[2];
        int16_t l_1168 = 0x8E7AL;
        int16_t **l_1238 = &g_337[1];
        int32_t l_1259[10][4][1] = {{{0xB6C389E1L},{0x1E723E4EL},{4L},{0x759B1B0DL}},{{(-9L)},{0x65540A72L},{0xB6C389E1L},{0x65540A72L}},{{(-9L)},{0x759B1B0DL},{4L},{0x1E723E4EL}},{{0xB6C389E1L},{6L},{0x8A27E4C1L},{0x759B1B0DL}},{{0x8A27E4C1L},{6L},{0xB6C389E1L},{0x1E723E4EL}},{{4L},{0x759B1B0DL},{(-9L)},{0x65540A72L}},{{0xB6C389E1L},{0x65540A72L},{(-9L)},{0x759B1B0DL}},{{4L},{0x1E723E4EL},{0xB6C389E1L},{6L}},{{0x8A27E4C1L},{0x759B1B0DL},{0x8A27E4C1L},{6L}},{{0xB6C389E1L},{0x1E723E4EL},{4L},{0x759B1B0DL}}};
        int64_t *l_1267[8];
        int64_t l_1290 = 1L;
        int32_t l_1320 = 0x0B8A87F4L;
        uint32_t l_1333 = 1UL;
        uint16_t * const l_1348 = &l_1330[4];
        uint16_t ****l_1366 = (void*)0;
        const float l_1367[6] = {0x9.D080F7p-73,0x9.D080F7p-73,0x9.D080F7p-73,0x9.D080F7p-73,0x9.D080F7p-73,0x9.D080F7p-73};
        float l_1368 = 0x0.Fp-1;
        int32_t l_1385 = 0xE47819E3L;
        uint8_t *l_1481 = (void*)0;
        uint8_t ** const l_1480 = &l_1481;
        uint8_t * const l_1563 = (void*)0;
        uint8_t * const *l_1562 = &l_1563;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1106[i] = 0xC5L;
        for (i = 0; i < 8; i++)
            l_1267[i] = (void*)0;
        (*l_984) |= (safe_sub_func_int16_t_s_s(6L, (g_950 = (0xD0C4866BCF754756LL && p_27))));
        for (g_60 = 0; (g_60 >= 18); g_60 = safe_add_func_int8_t_s_s(g_60, 2))
        { /* block id: 391 */
            int32_t *****l_989 = (void*)0;
            int32_t *****l_990[5];
            int8_t *l_995 = (void*)0;
            uint32_t l_1047 = 4294967295UL;
            int8_t l_1075 = 0x85L;
            int i;
            for (i = 0; i < 5; i++)
                l_990[i] = (void*)0;
        }
    }
    return g_1542[1][0][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_789 g_790
 * writes: g_318
 */
static int8_t * func_28(int8_t * p_29, float  p_30, float  p_31)
{ /* block id: 379 */
    int32_t *l_978 = &g_975;
    int8_t *l_979[4][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
    int i, j;
    (**g_789) = l_978;
    (*g_790) = l_978;
    return l_979[2][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_789 g_790 g_318
 * writes: g_60 g_759 g_974 g_318
 */
static int8_t * func_32(int8_t * p_33, int8_t * p_34)
{ /* block id: 361 */
    int8_t *l_971 = &g_925[5][1][0];
    int32_t *l_976 = &g_975;
    for (g_60 = 25; (g_60 <= (-14)); --g_60)
    { /* block id: 364 */
        int16_t l_970[6] = {0xC085L,(-1L),0xC085L,0xC085L,(-1L),0xC085L};
        int i;
        if (l_970[1])
        { /* block id: 365 */
            return l_971;
        }
        else
        { /* block id: 367 */
            for (g_759 = 13; (g_759 != 13); g_759 = safe_add_func_uint32_t_u_u(g_759, 8))
            { /* block id: 370 */
                g_974 = (**g_789);
            }
            (*g_790) = l_976;
        }
    }
    (**g_789) = l_976;
    return p_34;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t * func_35(int64_t  p_36, int32_t  p_37, const float  p_38)
{ /* block id: 358 */
    int8_t *l_966[1];
    int i;
    for (i = 0; i < 1; i++)
        l_966[i] = &g_925[6][2][0];
    return l_966[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_213 g_214 g_865 g_789 g_790 g_584 g_55
 * writes: g_950 g_235 g_925
 */
static int16_t  func_40(int64_t  p_41, int64_t  p_42)
{ /* block id: 14 */
    int32_t l_88 = 1L;
    uint32_t l_105 = 3UL;
    int16_t l_128 = 0xBC0FL;
    uint64_t l_179 = 0x49B2E1E59718C347LL;
    uint32_t l_204 = 0x94D8C579L;
    int16_t l_212[8] = {7L,0L,7L,7L,0L,7L,7L,0L};
    int32_t l_288 = 3L;
    int64_t l_351[9][4] = {{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)},{(-2L),(-2L),(-2L),(-2L)}};
    int32_t l_357 = 0L;
    int32_t l_359 = 1L;
    int8_t *l_401[8][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_164[1],&g_164[5],&g_164[4],(void*)0},{(void*)0,&g_164[5],(void*)0,&g_164[1],(void*)0,(void*)0,(void*)0,(void*)0,&g_164[5]},{(void*)0,&g_164[3],(void*)0,&g_164[5],(void*)0,&g_164[5],&g_164[4],(void*)0,&g_164[5]},{&g_164[2],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_164[5],&g_164[5],&g_164[3],&g_164[5],&g_164[2],(void*)0,(void*)0,(void*)0,(void*)0},{&g_164[5],&g_164[4],(void*)0,&g_164[5],(void*)0,(void*)0,&g_164[5],(void*)0,&g_164[4]},{(void*)0,(void*)0,&g_164[3],&g_164[0],(void*)0,&g_164[5],&g_164[5],(void*)0,(void*)0},{(void*)0,&g_164[5],(void*)0,&g_164[4],&g_164[1],&g_164[4],(void*)0,&g_164[5],(void*)0}};
    const float *l_415 = (void*)0;
    int32_t l_460 = 0L;
    int32_t l_462 = (-1L);
    int32_t l_463 = (-3L);
    int32_t l_466 = 0xDC8211D7L;
    int32_t l_467 = (-1L);
    int32_t l_469 = (-1L);
    int32_t l_470[4] = {0x88416C87L,0x88416C87L,0x88416C87L,0x88416C87L};
    int16_t l_473[1][4][2] = {{{6L,9L},{9L,6L},{9L,9L},{6L,9L}}};
    int64_t l_555 = 1L;
    int32_t l_582 = 9L;
    uint64_t l_616[8] = {18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL};
    int32_t *l_618 = &l_288;
    const int32_t **l_640 = (void*)0;
    const int32_t *** const l_639[8] = {&l_640,&l_640,&l_640,&l_640,&l_640,&l_640,&l_640,&l_640};
    int16_t l_726[4][8][6] = {{{0xE14CL,0x85C0L,0xE14CL,1L,0L,0x2FC2L},{1L,0L,0x2FC2L,0L,0xE797L,2L},{0x0556L,0x2FC2L,(-1L),0L,1L,1L},{1L,0x5147L,0x5147L,1L,0x610DL,0x2117L},{0xE14CL,0x0556L,2L,0x85C0L,(-1L),1L},{0x693AL,0L,0xFC67L,0xF25EL,(-1L),0L},{0x610DL,0x0556L,1L,0x0556L,0x610DL,0xF25EL},{0xFC67L,0x5147L,0x3107L,0x2117L,1L,0x693AL}},{{0x3107L,0x2FC2L,0x610DL,0x5147L,0xE797L,0x693AL},{0xF25EL,0L,0x3107L,0x3107L,0L,0xF25EL},{0xE797L,0x85C0L,1L,(-1L),0xFC67L,0L},{0x85C0L,0x610DL,0xFC67L,2L,0L,1L},{0x85C0L,0x2117L,2L,(-1L),2L,0x2117L},{0xE797L,0xFC67L,0x5147L,0x3107L,0x2117L,1L},{0xF25EL,0xE14CL,(-1L),0x5147L,0x693AL,2L},{0x3107L,0xE14CL,0x2FC2L,0x2117L,0x2117L,0x2FC2L}},{{0xFC67L,0xFC67L,0xE14CL,0x0556L,2L,0x85C0L},{0x610DL,0x2117L,0x85C0L,(-1L),1L,0xE797L},{0xDA10L,0x0556L,0x85C0L,0x2FC2L,0x693AL,0x2FC2L},{0xE797L,0x2FC2L,0xE797L,0xE14CL,0L,0xF25EL},{0xE14CL,0L,0xF25EL,1L,0x2117L,0x610DL},{0x5147L,0xF25EL,0x3107L,1L,0xE14CL,0xE14CL},{0xE14CL,0xFC67L,0xFC67L,0xE14CL,0x0556L,2L},{0xE797L,0x5147L,0x610DL,0x2FC2L,0x3107L,0x85C0L}},{{0xDA10L,1L,0x693AL,(-1L),0x3107L,0L},{0x0556L,0x5147L,0xE14CL,0x5147L,0x0556L,(-1L)},{0x693AL,0xFC67L,1L,2L,0xE14CL,0xDA10L},{1L,0xF25EL,0x0556L,0xFC67L,0x2117L,0xDA10L},{(-1L),0L,1L,1L,0L,(-1L)},{0x2117L,0x2FC2L,0xE14CL,0x3107L,0x693AL,0L},{0x2FC2L,0x0556L,0x693AL,0x610DL,1L,0x85C0L},{0x2FC2L,2L,0x610DL,0x3107L,0x610DL,2L}}};
    int32_t l_727 = 4L;
    uint8_t l_728 = 4UL;
    uint8_t l_734 = 2UL;
    const int64_t *l_757[8] = {&l_555,(void*)0,&l_555,&l_555,(void*)0,&l_555,&l_555,(void*)0};
    const int64_t **l_756 = &l_757[2];
    float l_761[4][4][2] = {{{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54}},{{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54}},{{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54}},{{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54},{0xE.54E431p-54,0xE.54E431p-54}}};
    uint32_t l_762[9][9] = {{1UL,3UL,3UL,1UL,1UL,3UL,3UL,1UL,1UL},{0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L},{1UL,1UL,3UL,3UL,1UL,1UL,3UL,3UL,1UL},{0UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL},{1UL,3UL,3UL,1UL,1UL,3UL,3UL,1UL,1UL},{0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L,18446744073709551615UL,0xD2B2C4C1L},{1UL,1UL,3UL,3UL,1UL,1UL,3UL,3UL,1UL},{0UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL},{1UL,3UL,3UL,1UL,1UL,3UL,3UL,1UL,1UL}};
    uint32_t l_869 = 0xB0D93F01L;
    const int8_t *l_897 = &g_898;
    int64_t l_920 = 0L;
    uint32_t l_947 = 5UL;
    int16_t *l_948 = &l_212[7];
    uint16_t *l_949 = &g_950;
    uint8_t *l_951 = &g_235[3][0];
    const int32_t l_963 = 0x77F7C48EL;
    uint8_t l_964 = 254UL;
    int32_t *l_965 = &l_470[2];
    int i, j, k;
    for (p_41 = 0; (p_41 <= 4); p_41 += 1)
    { /* block id: 17 */
        int16_t l_87 = 0x05A9L;
        uint16_t *l_93 = (void*)0;
        int16_t *l_106 = &l_87;
        int32_t l_109[10] = {0x8608D2D9L,0x8608D2D9L,(-7L),0x8608D2D9L,0x8608D2D9L,(-7L),0x8608D2D9L,0x8608D2D9L,(-7L),0x8608D2D9L};
        uint16_t l_137[3];
        int8_t *l_163 = &g_164[5];
        int32_t l_211[1][1][10] = {{{0x5EBBC8A8L,0xF615AE1FL,0xF615AE1FL,0x5EBBC8A8L,0xF615AE1FL,0xF615AE1FL,0x5EBBC8A8L,0xF615AE1FL,0xF615AE1FL,0x5EBBC8A8L}}};
        int32_t l_259 = 0x5130D131L;
        int64_t l_306[2];
        uint32_t l_360 = 18446744073709551614UL;
        float *l_364[3];
        int16_t l_397 = 0xE37DL;
        const int32_t * const l_581[7] = {&g_145,&g_145,&g_145,&g_145,&g_145,&g_145,&g_145};
        const int32_t * const *l_580 = &l_581[1];
        uint16_t *l_583 = &l_137[2];
        int32_t l_598 = (-1L);
        int64_t **l_755 = &g_414[0][1];
        uint64_t *l_758[9][6] = {{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179},{&g_214,&g_214,&l_179,&g_214,&g_214,&l_179}};
        int32_t *l_763 = &l_211[0][0][2];
        int32_t ***l_792 = (void*)0;
        uint8_t l_890 = 248UL;
        uint8_t *l_922 = &l_734;
        uint8_t **l_921 = &l_922;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_137[i] = 0x1EDEL;
        for (i = 0; i < 2; i++)
            l_306[i] = (-4L);
        for (i = 0; i < 3; i++)
            l_364[i] = &g_206;
    }
    (*l_618) = (safe_mod_func_uint64_t_u_u(((*l_618) & (l_469 = (safe_lshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((l_401[6][5] != l_401[0][1]), 4)), (safe_mod_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(((safe_sub_func_int32_t_s_s(p_41, ((((safe_mul_func_uint16_t_u_u(((safe_lshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(p_41, 6)), 12)) | (((((p_42 , ((((*l_951) = (((*l_949) = ((((*l_948) ^= (!(l_947 |= ((p_41 , ((void*)0 == &l_757[2])) >= 0x25378851L)))) != 0xBDC7L) >= 0UL)) || p_42)) <= p_42) , p_42)) & 0x47FAL) , &g_235[0][1]) != &l_728) , 0x4D00CCE2L)), 0x2D60L)) , 18446744073709551615UL) < (*g_213)) >= 0UL))) > 4294967288UL), p_41)), g_865[0])))))), (*l_618)));
    (*l_965) &= (((safe_lshift_func_uint16_t_u_u((0x05L & ((safe_div_func_int16_t_s_s(((safe_add_func_int8_t_s_s((safe_unary_minus_func_int64_t_s(((((((safe_sub_func_int64_t_s_s(1L, (safe_lshift_func_int16_t_s_s((-1L), 0)))) , (*g_789)) == (((p_42 , ((*l_756) != &g_127)) & (((*l_618) &= g_584) && ((g_925[5][1][0] = p_42) | p_41))) , (void*)0)) <= 0x6B1E342C378AE933LL) && 0xD1L) == p_41))), g_584)) <= p_41), l_963)) , 0xA3L)), p_41)) != g_55[8][4][0]) ^ l_964);
    return p_41;
}


/* ------------------------------------------ */
/* 
 * reads : g_70 g_71 g_55
 * writes: g_70 g_60
 */
static uint64_t  func_44(uint64_t  p_45)
{ /* block id: 3 */
    uint32_t l_61 = 1UL;
    uint16_t l_68 = 0UL;
    uint32_t *l_69 = &g_70[0][7][5];
    const int8_t *l_78[4];
    int32_t *l_80 = (void*)0;
    int32_t *l_81[10][3] = {{&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60},{(void*)0,&g_60,(void*)0},{(void*)0,&g_60,&g_60},{&g_60,(void*)0,(void*)0},{&g_60,(void*)0,&g_60},{&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60},{&g_60,&g_60,(void*)0}};
    float l_82 = 0x7.4p-1;
    int i, j;
    for (i = 0; i < 4; i++)
        l_78[i] = &g_79;
    for (p_45 = 0; (p_45 == 35); p_45 = safe_add_func_uint32_t_u_u(p_45, 1))
    { /* block id: 6 */
        int32_t *l_59 = &g_60;
        int32_t **l_58 = &l_59;
        (*l_58) = (void*)0;
        ++l_61;
        return p_45;
    }
    g_60 = (((p_45 || ((safe_add_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u(l_61, ((((*l_69) &= l_68) , g_71) != (void*)0))), (safe_div_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u((p_45 < (0x6E0732D58142351FLL > (((safe_div_func_int8_t_s_s((-7L), g_55[8][4][0])) || p_45) & (-1L)))), 252UL)), p_45)))) || g_55[8][4][0])) , l_78[1]) == &g_79);
    return p_45;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_13, "g_13", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_16[i][j][k], "g_16[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_55[i][j][k], "g_55[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_60, "g_60", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_70[i][j][k], "g_70[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc(g_94, "g_94", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_125[i], "g_125[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    transparent_crc(g_146, "g_146", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_164[i], "g_164[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_206, sizeof(g_206), "g_206", print_hash_value);
    transparent_crc(g_214, "g_214", print_hash_value);
    transparent_crc(g_222, "g_222", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_235[i][j], "g_235[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_256, "g_256", print_hash_value);
    transparent_crc_bytes (&g_353, sizeof(g_353), "g_353", print_hash_value);
    transparent_crc(g_584, "g_584", print_hash_value);
    transparent_crc(g_663, "g_663", print_hash_value);
    transparent_crc(g_759, "g_759", print_hash_value);
    transparent_crc(g_760, "g_760", print_hash_value);
    transparent_crc(g_764, "g_764", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_865[i], "g_865[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_898, "g_898", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_925[i][j][k], "g_925[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_950, "g_950", print_hash_value);
    transparent_crc(g_975, "g_975", print_hash_value);
    transparent_crc(g_1028, "g_1028", print_hash_value);
    transparent_crc(g_1053, "g_1053", print_hash_value);
    transparent_crc(g_1064, "g_1064", print_hash_value);
    transparent_crc(g_1104, "g_1104", print_hash_value);
    transparent_crc(g_1107, "g_1107", print_hash_value);
    transparent_crc_bytes (&g_1232, sizeof(g_1232), "g_1232", print_hash_value);
    transparent_crc(g_1243, "g_1243", print_hash_value);
    transparent_crc(g_1485, "g_1485", print_hash_value);
    transparent_crc(g_1486, "g_1486", print_hash_value);
    transparent_crc(g_1487, "g_1487", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1488[i], "g_1488[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1489, "g_1489", print_hash_value);
    transparent_crc(g_1490, "g_1490", print_hash_value);
    transparent_crc(g_1491, "g_1491", print_hash_value);
    transparent_crc(g_1492, "g_1492", print_hash_value);
    transparent_crc(g_1493, "g_1493", print_hash_value);
    transparent_crc(g_1494, "g_1494", print_hash_value);
    transparent_crc(g_1495, "g_1495", print_hash_value);
    transparent_crc(g_1496, "g_1496", print_hash_value);
    transparent_crc(g_1497, "g_1497", print_hash_value);
    transparent_crc(g_1498, "g_1498", print_hash_value);
    transparent_crc(g_1499, "g_1499", print_hash_value);
    transparent_crc(g_1500, "g_1500", print_hash_value);
    transparent_crc(g_1501, "g_1501", print_hash_value);
    transparent_crc(g_1502, "g_1502", print_hash_value);
    transparent_crc(g_1503, "g_1503", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1504[i][j], "g_1504[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1505, "g_1505", print_hash_value);
    transparent_crc(g_1506, "g_1506", print_hash_value);
    transparent_crc(g_1507, "g_1507", print_hash_value);
    transparent_crc(g_1508, "g_1508", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1509[i], "g_1509[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1510, "g_1510", print_hash_value);
    transparent_crc(g_1511, "g_1511", print_hash_value);
    transparent_crc(g_1512, "g_1512", print_hash_value);
    transparent_crc(g_1513, "g_1513", print_hash_value);
    transparent_crc(g_1514, "g_1514", print_hash_value);
    transparent_crc(g_1515, "g_1515", print_hash_value);
    transparent_crc(g_1516, "g_1516", print_hash_value);
    transparent_crc(g_1517, "g_1517", print_hash_value);
    transparent_crc(g_1518, "g_1518", print_hash_value);
    transparent_crc(g_1519, "g_1519", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1520[i][j], "g_1520[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1521, "g_1521", print_hash_value);
    transparent_crc(g_1522, "g_1522", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1523[i], "g_1523[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1524, "g_1524", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1525[i][j][k], "g_1525[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1526, "g_1526", print_hash_value);
    transparent_crc(g_1527, "g_1527", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1528[i][j][k], "g_1528[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1529, "g_1529", print_hash_value);
    transparent_crc(g_1530, "g_1530", print_hash_value);
    transparent_crc(g_1531, "g_1531", print_hash_value);
    transparent_crc(g_1532, "g_1532", print_hash_value);
    transparent_crc(g_1533, "g_1533", print_hash_value);
    transparent_crc(g_1534, "g_1534", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1535[i], "g_1535[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1536, "g_1536", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1537[i], "g_1537[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1538, "g_1538", print_hash_value);
    transparent_crc(g_1539, "g_1539", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1540[i][j], "g_1540[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1541, "g_1541", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1542[i][j][k], "g_1542[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1543, "g_1543", print_hash_value);
    transparent_crc(g_1544, "g_1544", print_hash_value);
    transparent_crc(g_1545, "g_1545", print_hash_value);
    transparent_crc(g_1546, "g_1546", print_hash_value);
    transparent_crc(g_1547, "g_1547", print_hash_value);
    transparent_crc(g_1548, "g_1548", print_hash_value);
    transparent_crc(g_1549, "g_1549", print_hash_value);
    transparent_crc(g_1550, "g_1550", print_hash_value);
    transparent_crc(g_1551, "g_1551", print_hash_value);
    transparent_crc(g_1552, "g_1552", print_hash_value);
    transparent_crc(g_1553, "g_1553", print_hash_value);
    transparent_crc(g_1554, "g_1554", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1555[i][j], "g_1555[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1556, "g_1556", print_hash_value);
    transparent_crc(g_1557, "g_1557", print_hash_value);
    transparent_crc(g_1558, "g_1558", print_hash_value);
    transparent_crc(g_1559, "g_1559", print_hash_value);
    transparent_crc(g_1560, "g_1560", print_hash_value);
    transparent_crc(g_1561, "g_1561", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_1572[i][j][k], "g_1572[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1642, "g_1642", print_hash_value);
    transparent_crc(g_1885, "g_1885", print_hash_value);
    transparent_crc(g_1994, "g_1994", print_hash_value);
    transparent_crc(g_1998, "g_1998", print_hash_value);
    transparent_crc(g_2178, "g_2178", print_hash_value);
    transparent_crc(g_2264, "g_2264", print_hash_value);
    transparent_crc_bytes (&g_2275, sizeof(g_2275), "g_2275", print_hash_value);
    transparent_crc(g_2323, "g_2323", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2325[i], "g_2325[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2348, "g_2348", print_hash_value);
    transparent_crc(g_2527, "g_2527", print_hash_value);
    transparent_crc(g_2594, "g_2594", print_hash_value);
    transparent_crc(g_2596, "g_2596", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 701
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 113
   depth: 2, occurrence: 23
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 12, occurrence: 1
   depth: 15, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 24, occurrence: 1
   depth: 25, occurrence: 1
   depth: 30, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 437

XXX times a variable address is taken: 1190
XXX times a pointer is dereferenced on RHS: 386
breakdown:
   depth: 1, occurrence: 290
   depth: 2, occurrence: 67
   depth: 3, occurrence: 5
   depth: 4, occurrence: 8
   depth: 5, occurrence: 16
XXX times a pointer is dereferenced on LHS: 338
breakdown:
   depth: 1, occurrence: 286
   depth: 2, occurrence: 39
   depth: 3, occurrence: 11
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 59
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 25
XXX times a pointer is qualified to be dereferenced: 10302

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1266
   level: 2, occurrence: 388
   level: 3, occurrence: 182
   level: 4, occurrence: 107
   level: 5, occurrence: 99
XXX number of pointers point to pointers: 192
XXX number of pointers point to scalars: 245
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.6
XXX average alias set size: 1.79

XXX times a non-volatile is read: 2424
XXX times a non-volatile is write: 1052
XXX times a volatile is read: 58
XXX    times read thru a pointer: 7
XXX times a volatile is write: 26
XXX    times written thru a pointer: 1
XXX times a volatile is available for access: 1.78e+03
XXX percentage of non-volatile access: 97.6

XXX forward jumps: 0
XXX backward jumps: 3

XXX stmts: 106
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 15
   depth: 2, occurrence: 12
   depth: 3, occurrence: 10
   depth: 4, occurrence: 10
   depth: 5, occurrence: 30

XXX percentage a fresh-made variable is used: 14.1
XXX percentage an existing variable is used: 85.9
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      795253493
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   const float  f0;
   volatile int64_t  f1;
   volatile uint16_t  f2;
   volatile uint64_t  f3;
};

struct S1 {
   volatile signed f0 : 7;
   const unsigned f1 : 4;
   unsigned f2 : 19;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_3[5] = {(-8L),(-8L),(-8L),(-8L),(-8L)};
static int8_t g_5[10] = {0xF6L,0xF6L,0xF6L,0xF6L,0xF6L,0xF6L,0xF6L,0xF6L,0xF6L,0xF6L};
static uint16_t g_34 = 0x4DB9L;
static uint16_t g_57[3][10][8] = {{{0x381CL,0UL,0x381CL,0xEE72L,65531UL,65531UL,0xEE72L,0x381CL},{0UL,0UL,65531UL,4UL,0UL,4UL,65531UL,0UL},{0UL,0x381CL,0xEE72L,65531UL,65531UL,0xEE72L,0x381CL,0UL},{0x381CL,0xD8D7L,0UL,4UL,0UL,0xD8D7L,0x381CL,4UL},{0x381CL,0xEE72L,65531UL,65531UL,0xEE72L,0x381CL,0UL,0x381CL},{0xEE72L,0x381CL,0UL,0x381CL,0xEE72L,65531UL,65531UL,0xEE72L},{0x381CL,4UL,4UL,0x381CL,0UL,0xEE72L,0UL,0x381CL},{4UL,0UL,4UL,65531UL,0UL,0UL,65531UL,4UL},{0UL,0UL,0UL,0xEE72L,0xD8D7L,0xEE72L,0UL,0UL},{0UL,4UL,65531UL,0UL,0UL,65531UL,4UL,0UL}},{{4UL,0x381CL,0UL,0xEE72L,0UL,0x381CL,4UL,4UL},{0x381CL,0xEE72L,65531UL,65531UL,0xEE72L,0x381CL,0UL,0x381CL},{0xEE72L,0x381CL,0UL,0x381CL,0xEE72L,65531UL,65531UL,0xEE72L},{0x381CL,4UL,4UL,0x381CL,0UL,0xEE72L,0UL,0x381CL},{4UL,0UL,4UL,65531UL,0UL,0UL,65531UL,4UL},{0UL,0UL,0UL,0xEE72L,0xD8D7L,0xEE72L,0UL,0UL},{0UL,4UL,65531UL,0UL,0UL,65531UL,4UL,0UL},{4UL,0x381CL,0UL,0xEE72L,0UL,0x381CL,4UL,4UL},{0x381CL,0xEE72L,65531UL,65531UL,0xEE72L,0x381CL,0UL,0x381CL},{0xEE72L,0x381CL,0UL,0x381CL,0xEE72L,65531UL,65531UL,0xEE72L}},{{0x381CL,4UL,4UL,0x381CL,0UL,0xEE72L,0UL,0x381CL},{4UL,0UL,4UL,65531UL,0UL,0UL,65531UL,4UL},{0UL,0UL,0UL,0xEE72L,0xD8D7L,0xEE72L,0UL,0UL},{0UL,4UL,65531UL,0UL,0UL,65531UL,4UL,0UL},{4UL,0x381CL,0UL,0xEE72L,0UL,0x381CL,4UL,4UL},{0x381CL,0xEE72L,65531UL,65531UL,0xEE72L,0x381CL,0UL,0x381CL},{0xEE72L,0x381CL,0UL,0x381CL,0xEE72L,65531UL,65531UL,0xEE72L},{0x381CL,4UL,4UL,0x381CL,0UL,0xEE72L,0xD8D7L,4UL},{0xEE72L,0xD8D7L,0xEE72L,0UL,0UL,0UL,0UL,0xEE72L},{0xD8D7L,0xD8D7L,0UL,65531UL,0x381CL,65531UL,0UL,0xD8D7L}}};
static uint16_t *g_56 = &g_57[0][1][2];
static int32_t g_59[3] = {0xF207E879L,0xF207E879L,0xF207E879L};
static uint32_t g_80[7][1][10] = {{{4294967295UL,0UL,5UL,5UL,0UL,4294967295UL,5UL,0x59332C32L,4294967295UL,4294967295UL}},{{0UL,0x59332C32L,0x52A1FB9DL,0UL,0UL,0x52A1FB9DL,0x59332C32L,0UL,0x841AE9ECL,0x59332C32L}},{{0UL,4294967295UL,5UL,0x59332C32L,4294967295UL,4294967295UL,0x59332C32L,5UL,4294967295UL,0UL}},{{4294967295UL,0x59332C32L,5UL,4294967295UL,0UL,5UL,5UL,0UL,4294967295UL,5UL}},{{0UL,0UL,0x52A1FB9DL,0x59332C32L,0UL,0x841AE9ECL,0x59332C32L,0x59332C32L,0x841AE9ECL,0UL}},{{0UL,5UL,5UL,0UL,4294967295UL,5UL,0x59332C32L,4294967295UL,4294967295UL,0x59332C32L}},{{4294967295UL,0UL,5UL,5UL,0UL,4294967295UL,5UL,0x59332C32L,4294967295UL,4294967295UL}}};
static uint64_t g_84[1] = {0UL};
static int64_t g_91 = 0x2697EBF99C48247ALL;
static int16_t g_116[10] = {0x6874L,0x6874L,0xFAD1L,0xE0D7L,0xFAD1L,0x6874L,0x6874L,0xFAD1L,0xE0D7L,0xFAD1L};
static float g_126 = 0xB.EBBDCBp-48;
static volatile int32_t g_133 = 1L;/* VOLATILE GLOBAL g_133 */
static uint32_t g_138 = 0xD2E95C99L;
static const int16_t g_145 = 1L;
static uint8_t g_161 = 254UL;
static float g_163 = 0xE.B65092p+36;
static int32_t g_164[3] = {(-1L),(-1L),(-1L)};
static uint32_t g_167 = 4294967288UL;
static int8_t g_168 = (-1L);
static struct S1 g_172[5] = {{-7,0,14},{-7,0,14},{-7,0,14},{-7,0,14},{-7,0,14}};
static uint64_t g_188 = 0xEC0103C393CDE948LL;
static volatile struct S1 g_189 = {1,0,610};/* VOLATILE GLOBAL g_189 */
static int64_t g_191 = 0xBB44E860B7F1185DLL;
static int32_t g_199 = 0x3AFEE91EL;
static int32_t *g_198 = &g_199;
static struct S0 g_205 = {0xE.F8C7F8p-8,0x534156CA0117A253LL,8UL,0UL};/* VOLATILE GLOBAL g_205 */
static volatile struct S0 g_231 = {0x1.1p-1,0x228030A8251E51D7LL,0xE6F0L,1UL};/* VOLATILE GLOBAL g_231 */
static int32_t *g_263 = (void*)0;
static int32_t ** volatile g_262 = &g_263;/* VOLATILE GLOBAL g_262 */
static volatile struct S1 g_268[5][7][7] = {{{{5,2,280},{-9,2,62},{10,3,143},{10,3,143},{-9,2,62},{5,2,280},{-1,2,303}},{{-0,3,343},{-6,1,192},{3,1,706},{1,2,352},{-1,0,63},{-3,1,669},{5,3,468}},{{-1,2,303},{-9,0,265},{3,0,149},{-8,2,647},{-2,3,719},{-1,0,311},{8,2,172}},{{-10,0,511},{-6,1,192},{1,1,47},{7,0,718},{-10,3,564},{-10,0,256},{-0,3,343}},{{10,3,402},{-9,2,62},{8,0,16},{-2,0,59},{6,1,562},{10,3,143},{3,0,149}},{{-6,3,232},{1,1,47},{-6,2,389},{-2,1,500},{-0,2,644},{5,3,468},{1,3,436}},{{-9,0,265},{4,2,301},{-4,0,67},{-5,3,538},{-0,1,297},{-10,3,504},{10,3,391}}},{{{1,2,352},{-6,0,401},{5,3,468},{7,0,283},{1,3,436},{-1,0,63},{3,2,63}},{{-9,2,629},{-5,0,300},{6,1,357},{-0,2,15},{-5,1,620},{-0,2,15},{6,1,357}},{{1,0,57},{1,0,57},{-6,0,401},{-7,3,578},{8,1,106},{10,0,86},{-3,2,422}},{{-1,1,87},{-2,0,59},{5,2,280},{-10,1,146},{10,2,128},{-6,3,599},{-7,3,89}},{{-6,0,690},{-10,3,130},{-10,0,256},{5,0,5},{1,2,352},{-10,2,565},{-6,0,690}},{{9,1,524},{0,3,539},{5,2,646},{2,1,512},{6,1,357},{8,2,172},{-2,2,47}},{{8,2,578},{-7,3,480},{7,1,637},{-0,3,343},{-8,3,312},{8,1,106},{-0,3,398}}},{{{8,0,228},{6,1,357},{10,2,624},{8,0,42},{-10,2,682},{-2,0,59},{-0,3,491}},{{6,2,487},{0,0,240},{-10,2,565},{-1,0,63},{-8,1,429},{4,3,339},{-7,3,480}},{{10,3,143},{-1,1,87},{-4,0,67},{-8,0,529},{8,0,42},{-4,0,531},{-4,0,531}},{{8,1,106},{3,1,706},{-10,3,564},{3,1,706},{8,1,106},{-10,3,130},{-3,3,447}},{{-0,1,687},{10,3,391},{4,1,421},{9,3,602},{-0,1,297},{5,2,646},{-8,0,529}},{{-2,1,500},{7,3,81},{-6,2,389},{8,1,106},{8,2,578},{-5,3,463},{-0,2,644}},{{-0,1,687},{9,3,602},{-5,0,300},{-4,0,67},{5,2,280},{-8,2,456},{4,2,301}}},{{{8,1,106},{0,3,456},{4,3,339},{8,1,508},{-7,3,578},{7,0,283},{-5,2,662}},{{10,3,143},{-8,2,456},{-0,3,90},{8,2,172},{-9,0,265},{-7,3,89},{-6,3,599}},{{6,2,487},{-0,3,398},{-2,1,500},{1,2,352},{-6,0,401},{5,3,468},{7,0,283}},{{8,0,228},{8,2,188},{6,1,357},{-2,3,719},{0,3,539},{-3,0,214},{-1,1,87}},{{8,2,578},{1,1,47},{5,2,22},{-10,3,130},{-10,3,130},{5,2,22},{1,1,47}},{{9,1,524},{10,3,402},{-0,1,687},{-6,3,599},{-3,0,214},{-9,2,62},{-10,2,682}},{{-5,0,378},{-5,2,662},{-1,0,63},{0,3,456},{-6,3,232},{-10,3,564},{-5,0,169}}},{{{3,2,88},{-0,1,297},{-9,2,629},{-6,3,599},{-0,3,90},{10,2,128},{-10,3,504}},{{-6,1,192},{-5,0,169},{-0,3,398},{-10,3,130},{-1,1,279},{1,2,352},{-5,3,463}},{{10,2,128},{10,3,143},{8,0,42},{-2,3,719},{-0,1,687},{-3,0,409},{0,3,539}},{{10,0,86},{-6,2,389},{-8,3,312},{1,2,352},{-2,3,184},{7,3,81},{-3,2,422}},{{-9,2,629},{-5,1,620},{-10,3,504},{8,2,172},{4,2,558},{8,2,188},{-5,0,300}},{{5,0,5},{-10,0,511},{7,0,283},{8,1,508},{1,3,436},{-3,2,422},{1,3,436}},{{-4,0,67},{5,0,640},{5,0,640},{-4,0,67},{-6,3,599},{10,2,624},{10,3,143}}}};
static uint16_t g_288 = 0x9B6EL;
static int32_t ** volatile g_291[9][1] = {{&g_198},{&g_198},{&g_198},{&g_198},{&g_198},{&g_198},{&g_198},{&g_198},{&g_198}};
static int32_t ** const  volatile g_292[9][10] = {{&g_198,(void*)0,&g_198,(void*)0,&g_198,&g_198,(void*)0,(void*)0,&g_198,&g_198},{&g_198,(void*)0,&g_198,(void*)0,&g_198,&g_198,(void*)0,&g_198,(void*)0,(void*)0},{&g_198,(void*)0,&g_198,(void*)0,(void*)0,&g_198,(void*)0,&g_198,(void*)0,&g_198},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_198,&g_198,(void*)0},{(void*)0,(void*)0,&g_198,(void*)0,&g_198,&g_198,(void*)0,&g_198,(void*)0,(void*)0},{&g_198,(void*)0,&g_198,(void*)0,(void*)0,&g_198,(void*)0,&g_198,(void*)0,&g_198},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_198,&g_198,(void*)0},{(void*)0,(void*)0,&g_198,(void*)0,&g_198,&g_198,(void*)0,&g_198,(void*)0,(void*)0},{&g_198,(void*)0,&g_198,(void*)0,(void*)0,&g_198,(void*)0,&g_198,(void*)0,&g_198}};
static float g_304 = 0x4.7p+1;
static struct S0 g_328[5] = {{-0x1.6p+1,0x985383A0FE8AC2DBLL,0UL,0x979C347295AD5B88LL},{-0x1.6p+1,0x985383A0FE8AC2DBLL,0UL,0x979C347295AD5B88LL},{-0x1.6p+1,0x985383A0FE8AC2DBLL,0UL,0x979C347295AD5B88LL},{-0x1.6p+1,0x985383A0FE8AC2DBLL,0UL,0x979C347295AD5B88LL},{-0x1.6p+1,0x985383A0FE8AC2DBLL,0UL,0x979C347295AD5B88LL}};
static const int32_t g_364 = 0x1C618410L;
static volatile struct S1 *g_437 = &g_268[3][4][0];
static volatile struct S1 ** const  volatile g_436[9] = {&g_437,&g_437,&g_437,&g_437,&g_437,&g_437,&g_437,&g_437,&g_437};
static struct S1 g_471 = {-3,1,312};/* VOLATILE GLOBAL g_471 */
static struct S1 g_489 = {-2,3,101};/* VOLATILE GLOBAL g_489 */
static volatile struct S0 g_515 = {-0x1.9p+1,4L,0xA922L,0UL};/* VOLATILE GLOBAL g_515 */
static int32_t **g_591 = &g_198;
static int32_t *** volatile g_590 = &g_591;/* VOLATILE GLOBAL g_590 */
static int32_t g_648 = (-8L);
static int32_t * volatile g_647 = &g_648;/* VOLATILE GLOBAL g_647 */
static struct S1 *g_653[10] = {&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471};
static struct S1 ** volatile g_652 = &g_653[5];/* VOLATILE GLOBAL g_652 */
static uint32_t g_660 = 0UL;
static struct S0 *g_663 = (void*)0;
static struct S0 g_665 = {0x5.5D3ED4p-2,0x681154A1BA752C84LL,65532UL,0x91091E0DE0ADCD3ELL};/* VOLATILE GLOBAL g_665 */
static struct S0 *g_664 = &g_665;
static struct S1 g_690 = {-6,3,78};/* VOLATILE GLOBAL g_690 */
static volatile struct S1 g_772 = {8,0,557};/* VOLATILE GLOBAL g_772 */
static int32_t ***g_826[2] = {&g_591,&g_591};
static int32_t ****g_825 = &g_826[1];
static volatile int32_t g_833 = 0xD6CAA50AL;/* VOLATILE GLOBAL g_833 */
static uint64_t *g_835[2] = {&g_84[0],&g_84[0]};
static uint64_t ** volatile g_834 = &g_835[0];/* VOLATILE GLOBAL g_834 */
static int32_t * volatile g_864 = (void*)0;/* VOLATILE GLOBAL g_864 */
static int32_t * volatile g_865 = &g_648;/* VOLATILE GLOBAL g_865 */
static struct S1 g_897 = {-1,0,434};/* VOLATILE GLOBAL g_897 */
static const int32_t g_900[4] = {(-2L),(-2L),(-2L),(-2L)};
static const int32_t g_902 = (-1L);
static int32_t * volatile g_912 = &g_199;/* VOLATILE GLOBAL g_912 */
static struct S1 *g_914 = &g_489;
static struct S1 ** volatile g_913[6] = {&g_914,&g_914,&g_914,&g_914,&g_914,&g_914};
static struct S1 ** volatile g_915[7] = {&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914};
static struct S1 ** volatile g_916[3] = {&g_914,&g_914,&g_914};
static struct S1 ** volatile g_917 = &g_914;/* VOLATILE GLOBAL g_917 */
static uint8_t **g_922 = (void*)0;
static uint8_t *** const  volatile g_921 = &g_922;/* VOLATILE GLOBAL g_921 */
static int32_t * volatile g_957 = &g_199;/* VOLATILE GLOBAL g_957 */
static int64_t * volatile g_986 = &g_191;/* VOLATILE GLOBAL g_986 */
static int64_t * volatile * volatile g_985 = &g_986;/* VOLATILE GLOBAL g_985 */
static const volatile struct S0 g_991 = {0x1.6p+1,0x47ADC9A56E113304LL,0x3B4AL,0x8A36A5115EEBD62BLL};/* VOLATILE GLOBAL g_991 */
static float g_1028 = 0xB.312427p+84;
static const uint64_t *g_1073 = (void*)0;
static const uint64_t **g_1072 = &g_1073;
static const uint64_t ***g_1071 = &g_1072;
static const uint64_t **** volatile g_1070 = &g_1071;/* VOLATILE GLOBAL g_1070 */
static const struct S0 g_1137 = {0x1.2p-1,0xBAFF16FFE6EB9373LL,6UL,0xCF3D54A71830843DLL};/* VOLATILE GLOBAL g_1137 */
static volatile struct S0 g_1277[1] = {{-0x6.Ep-1,0x2A46A507DC87B74BLL,0x39ABL,0x3EF3A6617B080DB2LL}};
static uint8_t g_1344 = 6UL;
static int64_t *g_1370 = &g_191;
static int64_t **g_1369 = &g_1370;
static int64_t ***g_1368 = &g_1369;
static volatile float g_1382 = 0x9.86FDC8p-84;/* VOLATILE GLOBAL g_1382 */
static volatile uint32_t g_1394 = 0x7BDBDD27L;/* VOLATILE GLOBAL g_1394 */
static const struct S0 g_1408[6] = {{0x0.2p+1,0x6B1D58187672281CLL,0xB3ECL,18446744073709551606UL},{0x0.2p+1,0x6B1D58187672281CLL,0xB3ECL,18446744073709551606UL},{0x0.2p+1,0x6B1D58187672281CLL,0xB3ECL,18446744073709551606UL},{0x0.2p+1,0x6B1D58187672281CLL,0xB3ECL,18446744073709551606UL},{0x0.2p+1,0x6B1D58187672281CLL,0xB3ECL,18446744073709551606UL},{0x0.2p+1,0x6B1D58187672281CLL,0xB3ECL,18446744073709551606UL}};
static struct S0 g_1449 = {0x7.A3601Dp-22,9L,0x147DL,18446744073709551615UL};/* VOLATILE GLOBAL g_1449 */
static int16_t g_1478 = 0xA322L;
static volatile float g_1487[4] = {0xC.58B793p+39,0xC.58B793p+39,0xC.58B793p+39,0xC.58B793p+39};
static volatile float g_1488 = 0xC.86A89Cp-21;/* VOLATILE GLOBAL g_1488 */
static volatile float g_1489[4] = {0x0.1p+1,0x0.1p+1,0x0.1p+1,0x0.1p+1};
static volatile float g_1490[5] = {(-0x9.7p-1),(-0x9.7p-1),(-0x9.7p-1),(-0x9.7p-1),(-0x9.7p-1)};
static volatile float g_1491[1][1][5] = {{{0x0.8p-1,0x0.8p-1,0x0.8p-1,0x0.8p-1,0x0.8p-1}}};
static volatile float g_1492 = 0x6.04E215p-35;/* VOLATILE GLOBAL g_1492 */
static volatile float g_1493 = (-0x9.8p+1);/* VOLATILE GLOBAL g_1493 */
static volatile float *g_1486[7][10] = {{&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2]},{&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3]},{&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2]},{&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3]},{&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2]},{&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3]},{&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2],&g_1487[2],&g_1491[0][0][3],&g_1491[0][0][3],&g_1487[2]}};
static volatile float **g_1485[8] = {&g_1486[2][5],&g_1486[2][5],&g_1486[2][5],&g_1486[2][5],&g_1486[2][5],&g_1486[2][5],&g_1486[2][5],&g_1486[2][5]};
static uint64_t **g_1499 = &g_835[0];
static uint64_t ***g_1498 = &g_1499;
static uint64_t ****g_1497 = &g_1498;
static float g_1501 = 0x1.28C05Ep-61;
static struct S0 g_1502 = {-0x1.2p+1,-9L,65535UL,18446744073709551607UL};/* VOLATILE GLOBAL g_1502 */
static const uint32_t g_1514 = 0x181EC1EEL;
static const int64_t g_1559 = (-8L);
static uint64_t g_1628 = 0x693F8C0FB775EA23LL;
static uint64_t ****g_1643 = &g_1498;
static volatile float g_1678[6][7][3] = {{{0x8.4F452Dp+88,0xF.2BCF1Cp-22,(-0x8.3p+1)},{0x7.AA3B4Fp+83,0x1.Cp-1,0x5.8DDA8Dp+9},{(-0x3.Cp+1),0x4.772043p+35,0x4.772043p+35},{0x7.066657p+71,0x6.CA18B3p+70,0x4.772043p+35},{0x0.Cp+1,(-0x6.0p-1),0x5.8DDA8Dp+9},{(-0x8.Dp-1),0xD.E9AB46p-26,(-0x8.3p+1)},{0x9.7B44E2p+95,0x5.89EEFBp-30,0x6.CA18B3p+70}},{{0x9.5BA6BDp+26,0xD.E9AB46p-26,0xF.2BCF1Cp-22},{0xD.31E262p+22,(-0x6.0p-1),0x4.D2D345p-74},{0xA.4BA225p+63,0x6.CA18B3p+70,0xD.E9AB46p-26},{0xA.4BA225p+63,0x4.772043p+35,0x6.003FF6p+99},{0xD.31E262p+22,0x1.Cp-1,0x1.DF884Fp-75},{0x9.5BA6BDp+26,0xF.2BCF1Cp-22,0x8.B6BA02p-41},{0x9.7B44E2p+95,0x8.B6BA02p-41,0x1.DF884Fp-75}},{{(-0x8.Dp-1),0x8.6EE4A7p-13,0x6.003FF6p+99},{0x0.Cp+1,0xC.2ECEFFp+30,0xD.E9AB46p-26},{0x7.066657p+71,0xC.2ECEFFp+30,0x4.D2D345p-74},{(-0x3.Cp+1),0x8.6EE4A7p-13,0xF.2BCF1Cp-22},{0x7.AA3B4Fp+83,0x8.B6BA02p-41,0x6.CA18B3p+70},{0x8.4F452Dp+88,0xF.2BCF1Cp-22,(-0x8.3p+1)},{0x7.AA3B4Fp+83,0x1.Cp-1,0x5.8DDA8Dp+9}},{{(-0x3.Cp+1),0x4.772043p+35,0x4.772043p+35},{0x7.066657p+71,0x6.CA18B3p+70,0x4.772043p+35},{0x0.Cp+1,(-0x6.0p-1),0x5.8DDA8Dp+9},{(-0x8.Dp-1),0xD.E9AB46p-26,(-0x8.3p+1)},{0x9.7B44E2p+95,0x5.89EEFBp-30,0x6.CA18B3p+70},{0x9.5BA6BDp+26,0xD.E9AB46p-26,0xF.2BCF1Cp-22},{0xD.31E262p+22,(-0x6.0p-1),0x4.D2D345p-74}},{{0xA.4BA225p+63,0x6.CA18B3p+70,0xD.E9AB46p-26},{0xA.4BA225p+63,0x4.772043p+35,0x6.003FF6p+99},{0xD.31E262p+22,0x1.Cp-1,0x1.DF884Fp-75},{0x9.5BA6BDp+26,0xF.2BCF1Cp-22,0x8.B6BA02p-41},{0x9.7B44E2p+95,0x8.B6BA02p-41,0x1.DF884Fp-75},{(-0x8.Dp-1),0x8.6EE4A7p-13,0x6.003FF6p+99},{0x0.Cp+1,0xC.2ECEFFp+30,0xD.E9AB46p-26}},{{0x7.066657p+71,0xC.2ECEFFp+30,0x4.D2D345p-74},{(-0x3.Cp+1),0x8.6EE4A7p-13,0xF.2BCF1Cp-22},{0x7.AA3B4Fp+83,0x8.B6BA02p-41,0x6.CA18B3p+70},{0x8.4F452Dp+88,0xF.2BCF1Cp-22,(-0x8.3p+1)},{0x7.AA3B4Fp+83,0x1.Cp-1,0x5.8DDA8Dp+9},{(-0x3.Cp+1),0x4.772043p+35,0x4.772043p+35},{0x7.066657p+71,0x6.CA18B3p+70,0x4.772043p+35}}};
static uint32_t g_1679 = 0x2FA5600FL;
static float * volatile g_1710 = &g_163;/* VOLATILE GLOBAL g_1710 */
static struct S1 **g_1722 = &g_914;
static struct S1 *** volatile g_1721 = &g_1722;/* VOLATILE GLOBAL g_1721 */
static volatile int16_t g_1728 = 1L;/* VOLATILE GLOBAL g_1728 */
static volatile struct S1 g_1729 = {7,1,22};/* VOLATILE GLOBAL g_1729 */
static struct S0 g_1753[3][8] = {{{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL}},{{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL}},{{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL},{0x8.365BCFp-37,0xF96EF9C80824C89FLL,65527UL,0xBC374BAE83BBF73ALL}}};
static struct S0 g_1755[9] = {{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL},{0xD.4D4141p+86,0x21EF42AC8D211EE3LL,0UL,0x0AAC378894CC7384LL}};


/* --- FORWARD DECLARATIONS --- */
static const uint64_t  func_1(void);
static int16_t  func_10(int32_t  p_11, int32_t * p_12, const int32_t * const  p_13);
static const uint8_t  func_16(uint64_t  p_17, float  p_18);
static float  func_23(int8_t  p_24, uint16_t  p_25);
static uint8_t  func_35(uint64_t  p_36, uint16_t * p_37, int32_t * p_38);
static int32_t  func_41(int16_t  p_42, uint64_t  p_43, float  p_44, int8_t  p_45, int8_t  p_46);
static uint32_t  func_52(uint64_t  p_53, uint16_t * p_54, int32_t  p_55);
static const uint16_t  func_63(uint16_t * p_64, int32_t * p_65);
static int32_t * func_66(int32_t * const  p_67, const uint16_t  p_68, int32_t * p_69, uint16_t * p_70, uint32_t  p_71);
static int32_t * func_72(int32_t  p_73);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_188 g_167 g_825 g_826 g_591 g_198 g_199 g_1721 g_1368 g_1369 g_1370 g_1728 g_56 g_1497 g_1498 g_1499 g_835 g_84 g_1729 g_1679 g_957 g_912 g_161
 * writes: g_3 g_188 g_1628 g_167 g_199 g_1722 g_57 g_1028 g_161
 */
static const uint64_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_2[1];
    uint64_t l_1647 = 0UL;
    int32_t l_1684 = 0x2E01E119L;
    int32_t l_1685[2][3][9] = {{{(-4L),0x3EF352AAL,0xCF87AAFAL,(-1L),8L,(-5L),(-8L),0x1161E90DL,0L},{1L,0L,0x53652E08L,(-3L),(-1L),(-1L),0xB99044C5L,0x765CAB17L,0xB99044C5L},{0x765CAB17L,0x3EF352AAL,(-1L),(-1L),0x3EF352AAL,0x765CAB17L,(-6L),0x69854B78L,0xB99044C5L}},{{(-1L),0x69854B78L,0x1161E90DL,(-1L),(-4L),(-8L),0x765CAB17L,(-1L),0L},{(-1L),0x1161E90DL,0x69854B78L,(-1L),(-6L),0xF3579ED5L,(-6L),(-1L),0x69854B78L},{(-1L),(-1L),0x3EF352AAL,0x765CAB17L,(-6L),0x69854B78L,0xB99044C5L,0L,(-1L)}}};
    int32_t l_1686 = 0x6F0627BBL;
    uint8_t l_1688 = 0x9CL;
    int32_t * const *l_1707 = &g_263;
    const float *l_1748 = (void*)0;
    struct S0 *l_1754 = &g_1755[8];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_2[i] = 0UL;
    for (g_3[1] = 0; (g_3[1] >= 0); g_3[1] -= 1)
    { /* block id: 3 */
        int32_t *l_4[2];
        uint64_t l_1646 = 0xE6ED43AC3EC013FCLL;
        uint32_t l_1708 = 4UL;
        int i;
        for (i = 0; i < 2; i++)
            l_4[i] = (void*)0;
    }
    for (g_188 = 0; (g_188 < 22); g_188 = safe_add_func_uint8_t_u_u(g_188, 6))
    { /* block id: 649 */
        int32_t ****l_1723 = (void*)0;
        for (g_1628 = 21; (g_1628 == 60); g_1628 = safe_add_func_int16_t_s_s(g_1628, 1))
        { /* block id: 652 */
            int64_t l_1715 = 0xBD61007927EC3A29LL;
            uint32_t *l_1718 = &g_167;
            float *l_1730 = &g_1028;
            int32_t l_1731 = 0L;
            (****g_825) &= ((l_1715 < l_1715) & (0xC83E79220A15A1D0LL < (safe_sub_func_uint32_t_u_u((--(*l_1718)), 4294967295UL))));
            (*g_1721) = &g_653[4];
            (*l_1730) = ((((((**g_1368) == ((((((l_1723 == (void*)0) && (safe_sub_func_uint64_t_u_u(((l_1686 >= (((l_1715 && (((void*)0 == &g_660) ^ (safe_mul_func_uint16_t_u_u(((0xB8A1F51C061FB623LL & ((((*g_56) = (g_1728 < 0xA4FFL)) , l_1684) , l_1647)) < 0x3ED0L), (-1L))))) | 0L) , 0xEE29L)) ^ 0xAA2FFF37DBA77BF0LL), 0x4BBA5DC8B5BFD28FLL))) < (****g_1497)) ^ 0UL) , g_1729) , (*g_1369))) >= l_1685[1][1][1]) > g_1679) , l_1715) , l_1685[0][0][0]);
            l_1731 &= (*g_957);
        }
        (**g_591) = (*g_912);
    }
    for (g_161 = 0; (g_161 <= 1); g_161 += 1)
    { /* block id: 664 */
        float *l_1750 = &g_304;
        int32_t l_1761 = 0L;
        int i;
    }
    return l_1647;
}


/* ------------------------------------------ */
/* 
 * reads : g_1679
 * writes: g_1679
 */
static int16_t  func_10(int32_t  p_11, int32_t * p_12, const int32_t * const  p_13)
{ /* block id: 634 */
    int32_t *l_1655 = &g_648;
    int32_t *l_1656 = &g_199;
    int32_t *l_1657 = &g_648;
    int32_t *l_1658 = (void*)0;
    int32_t *l_1659 = (void*)0;
    int32_t *l_1660 = &g_199;
    int32_t *l_1661 = &g_199;
    int32_t *l_1662 = &g_199;
    int32_t *l_1663 = &g_199;
    int32_t l_1664 = 0L;
    int32_t *l_1665 = &l_1664;
    int32_t l_1666 = 0x9EE601FEL;
    int32_t l_1667[1];
    int32_t *l_1668 = &l_1666;
    int32_t *l_1669 = &g_648;
    int32_t *l_1670 = &g_648;
    int32_t *l_1671 = &l_1667[0];
    int32_t *l_1672 = &l_1667[0];
    int32_t *l_1673 = &l_1664;
    int32_t *l_1674 = &g_648;
    int32_t *l_1675[10] = {(void*)0,&g_3[0],&g_3[0],(void*)0,&g_3[0],&g_3[0],(void*)0,&g_3[0],&g_3[0],(void*)0};
    int32_t l_1676 = 0x0827261CL;
    int64_t l_1677 = 0xE268D4655949BEECLL;
    int i;
    for (i = 0; i < 1; i++)
        l_1667[i] = (-6L);
    --g_1679;
    return p_11;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_34 g_56 g_57 g_84 g_3 g_59 g_91 g_138 g_80 g_145 g_161 g_116 g_164 g_172 g_133 g_189 g_188 g_231 g_198 g_199 g_205.f2 g_262 g_268.f2 g_205.f1 g_288 g_328.f2 g_167 g_590 g_191 g_471.f2 g_471.f1 g_647 g_648 g_652 g_328.f3 g_690.f2 g_772 g_489.f1 g_653 g_825 g_437 g_826 g_833 g_834 g_865 g_205.f3 g_168 g_897 g_591 g_912 g_917 g_921 g_471.f0 g_268 g_957 g_985 g_991 g_986 g_690.f1 g_1070 g_1137 g_914 g_835 g_489.f2 g_1277 g_328.f1 g_665.f0 g_471 g_489 g_263 g_1368 g_1394 g_1344 g_1071 g_1449 g_364 g_1369 g_1370 g_1485 g_1502 g_1514 g_1498 g_1499 g_1628
 * writes: g_34 g_59 g_80 g_84 g_91 g_138 g_161 g_164 g_163 g_126 g_167 g_168 g_191 g_188 g_198 g_199 g_189.f0 g_591 g_116 g_648 g_653 g_663 g_664 g_57 g_288 g_304 g_914 g_922 g_985 g_1071 g_56 g_897.f2 g_489.f2 g_1028 g_5 g_1344 g_1368 g_1394 g_1478 g_1497 g_1643
 */
static const uint8_t  func_16(uint64_t  p_17, float  p_18)
{ /* block id: 6 */
    int32_t l_20[2][6] = {{3L,0x8700D35DL,3L,3L,0x8700D35DL,3L},{3L,0x8700D35DL,3L,3L,0x8700D35DL,3L}};
    int32_t *l_26 = (void*)0;
    int8_t l_27[10][10][2] = {{{5L,0L},{(-10L),(-1L)},{(-10L),0L},{5L,7L},{0L,(-6L)},{0x86L,0x5BL},{(-1L),0x38L},{0x38L,0x77L},{3L,9L},{0x4BL,(-10L)}},{{0xDCL,1L},{0x5BL,(-8L)},{0x77L,0x40L},{1L,0x40L},{0x77L,(-8L)},{0x5BL,1L},{0xDCL,(-10L)},{0x4BL,9L},{3L,0x77L},{0x38L,0x38L}},{{(-1L),0x5BL},{0x86L,(-6L)},{0L,7L},{5L,0L},{(-10L),(-1L)},{(-10L),0L},{5L,7L},{0L,(-6L)},{0x86L,0x5BL},{(-1L),0x38L}},{{0x38L,0x77L},{3L,9L},{0x4BL,(-10L)},{0xDCL,1L},{0x5BL,(-8L)},{0x77L,0x40L},{1L,0x40L},{0x77L,(-8L)},{0x5BL,1L},{0xDCL,(-10L)}},{{0x4BL,9L},{3L,0x77L},{0x38L,0x38L},{(-1L),0x5BL},{0x86L,(-6L)},{0L,7L},{5L,0L},{(-10L),(-1L)},{(-10L),0L},{5L,7L}},{{0L,(-6L)},{0x86L,0x5BL},{(-1L),0x38L},{0x38L,0x77L},{3L,9L},{0x4BL,(-10L)},{0xDCL,(-6L)},{0x77L,0x4BL},{(-4L),9L},{(-6L),9L}},{{(-4L),0x4BL},{0x77L,(-6L)},{(-10L),(-1L)},{0x38L,(-1L)},{1L,(-4L)},{(-1L),(-1L)},{1L,0x77L},{7L,0x40L},{0xDCL,0L},{0L,0xDCL}},{{(-1L),5L},{(-1L),0xDCL},{0L,0L},{0xDCL,0x40L},{7L,0x77L},{1L,(-1L)},{(-1L),(-4L)},{1L,(-1L)},{0x38L,(-1L)},{(-10L),(-6L)}},{{0x77L,0x4BL},{(-4L),9L},{(-6L),9L},{(-4L),0x4BL},{0x77L,(-6L)},{(-10L),(-1L)},{0x38L,(-1L)},{1L,(-4L)},{(-1L),(-1L)},{1L,0x77L}},{{7L,0x40L},{0xDCL,0L},{0L,0xDCL},{(-1L),5L},{(-1L),0xDCL},{0L,0L},{0xDCL,0x40L},{7L,0x77L},{1L,(-1L)},{(-1L),(-4L)}}};
    float *l_1645[4][10] = {{&g_304,&g_1501,&g_163,&g_163,&g_1501,&g_304,&g_1501,&g_1501,&g_126,&g_1501},{(void*)0,&g_163,&g_126,(void*)0,&g_126,&g_163,(void*)0,&g_1501,&g_304,&g_304},{(void*)0,&g_304,&g_304,(void*)0,(void*)0,&g_304,&g_304,(void*)0,&g_1501,&g_1501},{&g_304,&g_304,(void*)0,&g_1501,&g_1501,&g_1501,(void*)0,&g_304,&g_304,(void*)0}};
    int i, j, k;
    l_20[1][2] = 2L;
    p_18 = (safe_sub_func_float_f_f(func_23((l_26 != &g_3[1]), l_27[4][9][0]), p_17));
    return p_17;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_34 g_56 g_57 g_84 g_3 g_59 g_91 g_138 g_80 g_145 g_161 g_116 g_164 g_172 g_133 g_189 g_188 g_231 g_198 g_199 g_205.f2 g_262 g_268.f2 g_205.f1 g_288 g_328.f2 g_167 g_590 g_191 g_471.f2 g_471.f1 g_647 g_648 g_652 g_328.f3 g_690.f2 g_772 g_489.f1 g_653 g_825 g_437 g_826 g_833 g_834 g_865 g_205.f3 g_168 g_897 g_591 g_912 g_917 g_921 g_471.f0 g_268 g_957 g_985 g_991 g_986 g_690.f1 g_1070 g_1137 g_914 g_835 g_489.f2 g_1277 g_328.f1 g_665.f0 g_471 g_489 g_263 g_1368 g_1394 g_1344 g_1071 g_1449 g_364 g_1369 g_1370 g_1485 g_1502 g_1514 g_1498 g_1499 g_1628
 * writes: g_34 g_59 g_80 g_84 g_91 g_138 g_161 g_164 g_163 g_126 g_167 g_168 g_191 g_188 g_198 g_199 g_189.f0 g_591 g_116 g_648 g_653 g_663 g_664 g_57 g_288 g_304 g_914 g_922 g_985 g_1071 g_56 g_897.f2 g_489.f2 g_1028 g_5 g_1344 g_1368 g_1394 g_1478 g_1497 g_1643
 */
static float  func_23(int8_t  p_24, uint16_t  p_25)
{ /* block id: 8 */
    const float l_32 = 0x5.F87B70p-25;
    uint16_t *l_33 = &g_34;
    uint32_t l_49[6];
    int32_t *l_58 = &g_59[1];
    int32_t l_998 = 0x0DBC3A92L;
    int32_t l_1013 = (-8L);
    int32_t l_1014 = 0xE7BD8FA1L;
    int32_t l_1015[9][10][2] = {{{0xAD7660E8L,(-8L)},{0xF92D86D0L,0xDE1A656AL},{0x8B35D58FL,0x148C4704L},{0x5C77E8E2L,0x148C4704L},{0x8B35D58FL,0xDE1A656AL},{0xF92D86D0L,(-8L)},{0xAD7660E8L,0xC5411879L},{0xF7EF728CL,0x00B67855L},{0xA9DE2786L,(-1L)},{0x91D65ECCL,0x8EC65228L}},{{0L,0x09587245L},{0x530BE3AAL,5L},{0x530BE3AAL,0x09587245L},{0L,0x8EC65228L},{0x91D65ECCL,(-1L)},{0xA9DE2786L,0x00B67855L},{0xF7EF728CL,0xC5411879L},{0xAD7660E8L,(-8L)},{0xF92D86D0L,0xDE1A656AL},{0x8B35D58FL,0x148C4704L}},{{0x5C77E8E2L,0x148C4704L},{0x8B35D58FL,0xDE1A656AL},{0xF92D86D0L,(-8L)},{0xAD7660E8L,0xC5411879L},{0xF7EF728CL,0x00B67855L},{0xA9DE2786L,(-1L)},{0x91D65ECCL,0x8EC65228L},{0L,0x09587245L},{0x530BE3AAL,5L},{0x530BE3AAL,0x09587245L}},{{0L,0x8EC65228L},{0x91D65ECCL,(-1L)},{(-9L),(-1L)},{0xB2B3CA2DL,0x17B48E08L},{0x530BE3AAL,0L},{0xA9DE2786L,(-8L)},{9L,5L},{0xF92D86D0L,5L},{9L,(-8L)},{0xA9DE2786L,0L}},{{0x530BE3AAL,0x17B48E08L},{0xB2B3CA2DL,(-1L)},{(-9L),0x148C4704L},{(-7L),0xC5411879L},{0x91D65ECCL,2L},{0x5C77E8E2L,0x8EC65228L},{0x5C77E8E2L,2L},{0x91D65ECCL,0xC5411879L},{(-7L),0x148C4704L},{(-9L),(-1L)}},{{0xB2B3CA2DL,0x17B48E08L},{0x530BE3AAL,0L},{0xA9DE2786L,(-8L)},{9L,5L},{0xF92D86D0L,5L},{9L,(-8L)},{0xA9DE2786L,0L},{0x530BE3AAL,0x17B48E08L},{0xB2B3CA2DL,(-1L)},{(-9L),0x148C4704L}},{{(-7L),0xC5411879L},{0x91D65ECCL,2L},{0x5C77E8E2L,0x8EC65228L},{0x5C77E8E2L,2L},{0x91D65ECCL,0xC5411879L},{(-7L),0x148C4704L},{(-9L),(-1L)},{0xB2B3CA2DL,0x17B48E08L},{0x530BE3AAL,0L},{0xA9DE2786L,(-8L)}},{{9L,5L},{0xF92D86D0L,5L},{9L,(-8L)},{0xA9DE2786L,0L},{0x530BE3AAL,0x17B48E08L},{0xB2B3CA2DL,(-1L)},{(-9L),0x148C4704L},{(-7L),0xC5411879L},{0x91D65ECCL,2L},{0x5C77E8E2L,0x8EC65228L}},{{0x5C77E8E2L,2L},{0x91D65ECCL,0xC5411879L},{(-7L),0x148C4704L},{(-9L),(-1L)},{0xB2B3CA2DL,0x17B48E08L},{0x530BE3AAL,0L},{0xA9DE2786L,(-8L)},{9L,5L},{0xF92D86D0L,5L},{9L,(-8L)}}};
    int32_t l_1027 = (-9L);
    int32_t l_1029 = 4L;
    uint16_t l_1032 = 0x67E9L;
    int32_t l_1086 = 0L;
    uint8_t *l_1136 = &g_161;
    uint8_t **l_1135 = &l_1136;
    int32_t ***l_1147 = &g_591;
    const uint16_t l_1167 = 0xCF8FL;
    int64_t *l_1200 = (void*)0;
    int64_t **l_1199[7] = {&l_1200,&l_1200,&l_1200,&l_1200,&l_1200,&l_1200,&l_1200};
    struct S1 *l_1237 = &g_489;
    uint32_t l_1282 = 0xA61DBF9CL;
    uint64_t l_1333 = 0xCA5870076CEC1A6BLL;
    int32_t *l_1413 = &l_1014;
    float l_1442 = (-0x1.5p-1);
    int32_t l_1469 = 0L;
    int32_t * const l_1576 = &l_1469;
    uint32_t *l_1618 = (void*)0;
    uint32_t *l_1619 = &g_138;
    int32_t l_1626[9] = {(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L)};
    uint32_t *l_1627 = &g_167;
    int16_t *l_1629 = &g_116[7];
    uint32_t *l_1630 = &l_1282;
    int32_t *l_1631 = &g_648;
    uint64_t *****l_1642[9][9] = {{&g_1497,(void*)0,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{(void*)0,&g_1497,&g_1497,&g_1497,(void*)0,&g_1497,(void*)0,&g_1497,&g_1497},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,(void*)0,&g_1497,&g_1497,(void*)0},{(void*)0,(void*)0,&g_1497,(void*)0,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497,&g_1497},{&g_1497,&g_1497,&g_1497,(void*)0,&g_1497,&g_1497,&g_1497,(void*)0,&g_1497}};
    int16_t *l_1644[6] = {&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,&g_1478};
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_49[i] = 2UL;
    if ((safe_mod_func_int8_t_s_s((safe_add_func_uint16_t_u_u(g_5[0], ((*l_33) &= p_25))), func_35((safe_mod_func_uint32_t_u_u(4294967294UL, func_41(((0xE188A746L == (safe_div_func_uint8_t_u_u(((l_49[1] != (safe_sub_func_int32_t_s_s(((p_24 , func_52(g_5[0], g_56, ((*l_58) = (g_57[0][1][2] && l_49[5])))) != g_471.f2), l_49[5]))) || l_49[1]), 1L))) , l_49[0]), g_57[0][1][2], g_471.f2, l_49[2], l_49[0]))), l_33, l_58))))
    { /* block id: 384 */
        int64_t * volatile * volatile *l_987 = &g_985;
        int32_t l_990 = 0x58DF8429L;
        int32_t **l_992[2];
        const int8_t *l_996 = &g_5[4];
        int8_t l_997 = 0xDFL;
        int i;
        for (i = 0; i < 2; i++)
            l_992[i] = (void*)0;
        (*l_987) = g_985;
        l_998 &= (safe_mul_func_int8_t_s_s((((l_990 , &l_58) == (g_991 , ((1UL != l_990) , l_992[0]))) == ((((safe_sub_func_uint64_t_u_u(18446744073709551615UL, ((((((!((((-1L) && 18446744073709551610UL) != p_25) , l_990)) , l_996) == (void*)0) > (-1L)) == l_990) ^ l_49[2]))) ^ (*g_56)) && (*g_56)) ^ l_997)), l_49[2]));
    }
    else
    { /* block id: 387 */
        int32_t l_999 = 0xCFFC7B5AL;
        int32_t l_1010 = 0x4AEC0A1CL;
        int32_t l_1011 = 0L;
        int32_t l_1012 = (-9L);
        uint16_t l_1016 = 0x3DB9L;
        int32_t l_1019 = 0xACAA0A77L;
        int32_t l_1020 = (-1L);
        int32_t l_1021 = 0L;
        int32_t l_1022 = 0xA440D3D0L;
        int32_t l_1023 = 3L;
        int32_t l_1024 = 0xC73EE331L;
        int32_t l_1025[2];
        int32_t l_1038 = 0x2670E60AL;
        struct S1 **l_1045 = &g_914;
        struct S1 ***l_1044 = &l_1045;
        int32_t **l_1065[6][1] = {{(void*)0},{&g_198},{(void*)0},{&g_198},{(void*)0},{&g_198}};
        const uint64_t *l_1069 = &g_84[0];
        const uint64_t **l_1068 = &l_1069;
        const uint64_t ***l_1067 = &l_1068;
        const uint32_t l_1095 = 0x78A5E00BL;
        int16_t l_1096 = (-5L);
        int16_t l_1134 = 0xFF0DL;
        int32_t l_1140 = 0L;
        int64_t l_1220 = 8L;
        const int16_t l_1242 = 0x73C3L;
        int32_t l_1332 = 0xD12C9E1BL;
        float *l_1340 = &g_304;
        int8_t l_1400 = 0xF9L;
        int32_t **l_1453 = &l_58;
        int32_t l_1596 = (-6L);
        int i, j;
        for (i = 0; i < 2; i++)
            l_1025[i] = (-2L);
        if ((((((p_25 <= g_5[4]) < l_999) | (6L | (p_25 , 2UL))) ^ (**g_985)) | ((safe_unary_minus_func_int8_t_s(p_25)) < p_24)))
        { /* block id: 388 */
            return l_999;
        }
        else
        { /* block id: 390 */
            int16_t l_1007 = 0x5EF0L;
            int32_t l_1008[8][2][5] = {{{0L,0x681A010DL,1L,2L,2L},{0L,0xB49811D4L,0L,1L,1L}},{{0x886AAA1FL,0x2713A874L,2L,0x2713A874L,0x886AAA1FL},{0L,(-9L),0xA113B1FDL,0x325168A5L,0xA113B1FDL}},{{0L,0L,2L,0x886AAA1FL,(-10L)},{(-1L),1L,0L,(-9L),0xA113B1FDL}},{{0x2713A874L,0x886AAA1FL,1L,1L,0x886AAA1FL},{0xA113B1FDL,1L,0x4F04B71BL,0x5B998D04L,1L}},{{0x681A010DL,0L,0x681A010DL,1L,2L},{(-9L),(-9L),1L,(-9L),(-9L)}},{{0x681A010DL,0x2713A874L,0L,0x886AAA1FL,0L},{0xA113B1FDL,0xB49811D4L,1L,0x325168A5L,0x087B0C47L}},{{0x2713A874L,0x681A010DL,0x681A010DL,0x2713A874L,0L},{(-1L),0x325168A5L,0x4F04B71BL,1L,(-9L)}},{{0L,0x681A010DL,1L,2L,2L},{0L,0xB49811D4L,0L,1L,1L}}};
            int8_t l_1030 = 0x03L;
            int32_t l_1031 = 0x225564CFL;
            int32_t *l_1035 = &l_1008[0][1][0];
            int32_t *l_1036 = &l_1012;
            int32_t *l_1037[4];
            uint16_t l_1039 = 7UL;
            int8_t l_1058 = 8L;
            float *l_1066[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint8_t *l_1121 = &g_161;
            int32_t l_1139 = 1L;
            int64_t *l_1148 = &g_91;
            int32_t **l_1153 = &g_198;
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_1037[i] = &l_1013;
            for (g_199 = 0; (g_199 == (-2)); g_199 = safe_sub_func_uint32_t_u_u(g_199, 9))
            { /* block id: 393 */
                int32_t *l_1003 = &l_998;
                int32_t *l_1004 = &l_998;
                int32_t *l_1005 = (void*)0;
                int32_t *l_1006[3];
                int64_t l_1009 = 6L;
                int16_t l_1026 = 1L;
                int i;
                for (i = 0; i < 3; i++)
                    l_1006[i] = &g_648;
                ++l_1016;
                l_1032--;
            }
            l_1039--;
            if ((safe_rshift_func_int8_t_s_s(((void*)0 == l_1044), ((safe_add_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((((safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(0x001B414AL, ((l_1025[1] == l_1058) == (p_24 <= ((!((g_304 = (((((g_205.f1 <= (p_24 != ((safe_lshift_func_uint16_t_u_u((safe_unary_minus_func_uint16_t_u(((safe_sub_func_uint8_t_u_u(((void*)0 == l_1065[0][0]), (*l_1036))) != 8L))), (*g_56))) , g_690.f1))) , 0xE.B357B6p+93) < 0x8.6D4938p-59) >= 0x1.4469B3p-44) != 0x1.5p-1)) > g_59[1])) , (*g_56)))))), 4)), (*g_56))) > g_57[0][1][2]) & 0UL), (*g_56))), l_1027)) , 0x57L))))
            { /* block id: 399 */
                int8_t l_1076 = (-1L);
                uint32_t *l_1089 = (void*)0;
                uint32_t *l_1090[6] = {&g_167,&g_167,&g_167,&g_167,&g_167,&g_167};
                int32_t l_1091 = 0x7833050EL;
                int32_t l_1094[10] = {0L,0L,0L,0L,(-7L),0L,0L,0L,0L,(-7L)};
                int32_t ****l_1099 = &g_826[1];
                int i;
                (*g_1070) = l_1067;
                g_1028 = (safe_sub_func_float_f_f(l_1076, (safe_sub_func_float_f_f((safe_add_func_float_f_f((!(safe_div_func_float_f_f((safe_sub_func_float_f_f((g_126 = (l_1086 >= l_1015[4][3][0])), ((safe_add_func_int64_t_s_s((((g_56 = &l_1016) != (void*)0) && (l_1091 = (g_897.f2 = p_25))), (safe_sub_func_uint32_t_u_u((g_489.f2 = l_1094[7]), l_998)))) , (-0x3.Fp-1)))), (-0x1.7p-1)))), l_1095)), g_91))));
                (***l_1099) = &l_1008[3][1][3];
            }
            else
            { /* block id: 408 */
                int64_t *l_1108 = &g_191;
                int32_t l_1115 = 0x5ADE37D8L;
                int8_t *l_1120 = &l_1058;
                int16_t *l_1122 = &g_116[3];
                uint8_t **l_1138[8][8] = {{&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121},{&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121},{&l_1121,&l_1121,(void*)0,&l_1121,&l_1121,(void*)0,&l_1121,&l_1121},{&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121},{&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121},{&l_1121,&l_1121,(void*)0,&l_1121,&l_1121,(void*)0,&l_1121,&l_1121},{&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121,&l_1121},{&l_1121,(void*)0,(void*)0,&l_1121,(void*)0,(void*)0,&l_1121,(void*)0}};
                int32_t l_1141 = 0x90A6649AL;
                int32_t *l_1142 = &l_1008[0][1][0];
                int i, j;
                (*l_1036) |= (safe_sub_func_int16_t_s_s(((*l_1122) = (((safe_rshift_func_int8_t_s_s((p_25 > ((safe_mod_func_int64_t_s_s(p_25, ((*l_1108) = p_24))) == g_328[1].f3)), 2)) || (p_25 <= (safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_s(((((((safe_mod_func_uint8_t_u_u((l_1115 > (l_1015[5][2][0] = l_1015[3][1][1])), (p_24 , ((safe_sub_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((*l_1120) = ((p_25 , p_25) && g_3[1])), l_1115)), (*g_56))) & (-7L))))) ^ 0L) , p_24) , &g_161) != l_1121) | g_3[1]), 5)), l_1027)))) > 0xBC81201ED637A15ELL)), p_24));
                l_1141 ^= (p_25 > (safe_mul_func_int16_t_s_s(((*l_1122) = (*l_1036)), (safe_mul_func_uint8_t_u_u((!(0x66L >= ((safe_div_func_uint64_t_u_u(((((((safe_div_func_int32_t_s_s(((safe_add_func_uint8_t_u_u(((((*l_1108) |= (((l_1134 || (l_1135 != (g_1137 , l_1138[7][3]))) != g_138) ^ (*g_56))) != p_25) <= (*g_56)), p_25)) > l_1139), g_59[1])) == 0x4705D1FC99D6D91DLL) || l_1115) && p_25) , p_25) < p_24), (*l_1035))) && l_1140))), 1UL)))));
                (*g_591) = (void*)0;
                (***g_825) = l_1142;
            }
            g_304 = (p_25 , (((safe_div_func_uint16_t_u_u((p_24 != ((*l_1148) = (255UL == (g_231.f3 > ((*g_825) == l_1147))))), (*l_1035))) && ((safe_lshift_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s((((*l_1147) = (void*)0) == (l_1153 = l_1065[0][0])), (*g_198))) <= p_24), p_25)) < 1L)) , 0x7.7A5AFFp-82));
        }
lbl_1202:
        for (l_1140 = 0; (l_1140 != 0); l_1140 = safe_add_func_uint16_t_u_u(l_1140, 6))
        { /* block id: 427 */
            for (l_1010 = (-3); (l_1010 != 3); l_1010++)
            { /* block id: 430 */
                float l_1158 = (-0x9.Dp-1);
                l_1015[3][6][0] = (*g_865);
            }
        }
lbl_1600:
        if ((*g_957))
        { /* block id: 434 */
            float * const l_1170 = &g_304;
            uint32_t *l_1173 = &g_80[4][0][8];
            int32_t l_1188 = 1L;
            struct S1 **l_1209 = &g_914;
            int32_t l_1243 = 0x63D600FAL;
            uint8_t **l_1245 = &l_1136;
            float l_1303[4][6][3] = {{{0x4.Cp-1,0xF.B0C0B2p-10,0x0.3ECFC6p-79},{0x4.D309C6p-33,0x1.227CFEp+27,0x1.227CFEp+27},{0x0.4p-1,0x4.Cp-1,0x0.3ECFC6p-79},{(-0x1.4p-1),0x2.E13E20p-96,0xF.05669Bp+28},{0x0.5p-1,0x0.1p-1,0xF.B0C0B2p-10},{0x4.5A9CF9p+36,(-0x9.6p-1),0x4.5A9CF9p+36}},{{0xF.B0C0B2p-10,0x0.1p-1,0x0.5p-1},{0xF.05669Bp+28,0x2.E13E20p-96,(-0x1.4p-1)},{0x0.3ECFC6p-79,0x4.Cp-1,0x0.4p-1},{0x1.227CFEp+27,0x1.227CFEp+27,0x4.D309C6p-33},{0x0.3ECFC6p-79,0xF.B0C0B2p-10,0x4.Cp-1},{0xF.05669Bp+28,0x4.D309C6p-33,(-0x8.6p-1)}},{{0xF.B0C0B2p-10,0xB.04176Bp-71,0xB.04176Bp-71},{0x4.5A9CF9p+36,0xF.05669Bp+28,(-0x8.6p-1)},{0x0.5p-1,(-0x3.2p-1),0x4.Cp-1},{(-0x1.4p-1),0x0.Bp+1,0x4.D309C6p-33},{0x0.4p-1,0x9.26F15Bp+94,0x0.4p-1},{0x4.D309C6p-33,0x0.Bp+1,(-0x1.4p-1)}},{{0x4.Cp-1,(-0x3.2p-1),0x0.5p-1},{(-0x8.6p-1),0xF.05669Bp+28,0x4.5A9CF9p+36},{0xB.04176Bp-71,0xB.04176Bp-71,0xF.B0C0B2p-10},{(-0x8.6p-1),0x4.D309C6p-33,0xF.05669Bp+28},{0x4.Cp-1,0xF.B0C0B2p-10,0x0.3ECFC6p-79},{0x4.D309C6p-33,0x1.227CFEp+27,0x1.227CFEp+27}}};
            int32_t ** const *l_1324 = &g_591;
            int32_t ** const **l_1323[7];
            const int64_t *l_1342 = (void*)0;
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_1323[i] = &l_1324;
            if (((!(safe_div_func_int32_t_s_s(((safe_mul_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((3L | (!0UL)), l_1167)), (((void*)0 == l_1170) < ((*l_1173) = ((safe_lshift_func_uint8_t_u_s(p_25, 4)) >= (p_24 ^ g_489.f1)))))) ^ (safe_mul_func_uint8_t_u_u(p_25, p_24))), 6UL))) < 7UL))
            { /* block id: 436 */
                uint64_t l_1176 = 0x3CB612D6374CF252LL;
                uint64_t **l_1195[6] = {(void*)0,(void*)0,&g_835[1],(void*)0,(void*)0,&g_835[1]};
                int64_t *l_1197 = &g_91;
                int64_t **l_1196[8][5] = {{&l_1197,&l_1197,&l_1197,&l_1197,&l_1197},{&l_1197,&l_1197,(void*)0,(void*)0,&l_1197},{&l_1197,&l_1197,&l_1197,&l_1197,&l_1197},{&l_1197,(void*)0,(void*)0,&l_1197,&l_1197},{&l_1197,&l_1197,&l_1197,&l_1197,&l_1197},{&l_1197,&l_1197,(void*)0,(void*)0,&l_1197},{&l_1197,&l_1197,&l_1197,&l_1197,&l_1197},{&l_1197,(void*)0,(void*)0,&l_1197,&l_1197}};
                int64_t ***l_1198 = &l_1196[6][1];
                int32_t l_1201[10][2][9] = {{{0xFF6A9302L,0xB9E324CFL,0x3241A75FL,0xB9E324CFL,0xFF6A9302L,0xA834B848L,0xA834B848L,0xFF6A9302L,0xB9E324CFL},{0x36504DDBL,0x9B1E7C43L,0x36504DDBL,0x798831AEL,0x4CDCDFF5L,0x4CDCDFF5L,0x798831AEL,0x36504DDBL,0x9B1E7C43L}},{{0L,0xC09811FCL,0xA834B848L,0x3241A75FL,0x3241A75FL,0xA834B848L,0xC09811FCL,0L,0xC09811FCL},{0L,0xC8EAD729L,0x798831AEL,0x798831AEL,0xC8EAD729L,0L,0x4CDCDFF5L,0L,0xC8EAD729L}},{{0xB9E324CFL,0xC09811FCL,0xC09811FCL,0xB9E324CFL,0L,0xFF6A9302L,0L,0xB9E324CFL,0xC09811FCL},{0x9B1E7C43L,0x9B1E7C43L,0x4CDCDFF5L,0xC8EAD729L,(-1L),0xC8EAD729L,0x4CDCDFF5L,0x9B1E7C43L,0x9B1E7C43L}},{{0xC09811FCL,0xB9E324CFL,0L,0xFF6A9302L,0L,0xB9E324CFL,0xC09811FCL,0xC09811FCL,0xB9E324CFL},{0xC8EAD729L,0x36504DDBL,0x9B1E7C43L,0x36504DDBL,0x798831AEL,0x4CDCDFF5L,0x4CDCDFF5L,0x798831AEL,0x36504DDBL}},{{0xFF6A9302L,0xEA019F24L,0xFF6A9302L,0x3241A75FL,0L,0L,0x3241A75FL,0xFF6A9302L,0xEA019F24L},{(-1L),0xC8EAD729L,0x4CDCDFF5L,0x9B1E7C43L,0x9B1E7C43L,0x4CDCDFF5L,0xC8EAD729L,(-1L),0xC8EAD729L}},{{0xC09811FCL,0xA834B848L,0x3241A75FL,0x3241A75FL,0xA834B848L,0xC09811FCL,0L,0xC09811FCL,0xA834B848L},{0x36504DDBL,0xC8EAD729L,0xC8EAD729L,0x36504DDBL,(-1L),0x798831AEL,(-1L),0x36504DDBL,0xC8EAD729L}},{{0xEA019F24L,0xEA019F24L,0L,0xA834B848L,0xB9E324CFL,0xA834B848L,0L,0xEA019F24L,0xEA019F24L},{0xC8EAD729L,0x36504DDBL,(-1L),0x798831AEL,(-1L),0x36504DDBL,0xC8EAD729L,0xC8EAD729L,0x36504DDBL}},{{0xA834B848L,0xC09811FCL,0L,0xC09811FCL,0xA834B848L,0x3241A75FL,0x3241A75FL,0xA834B848L,0xC09811FCL},{0xC8EAD729L,(-1L),0xC8EAD729L,0x4CDCDFF5L,0x9B1E7C43L,0x9B1E7C43L,0x4CDCDFF5L,0xC8EAD729L,(-1L)}},{{0xEA019F24L,0xFF6A9302L,0x3241A75FL,0L,0L,0x3241A75FL,0xFF6A9302L,0xEA019F24L,0xFF6A9302L},{0x36504DDBL,0x798831AEL,0x4CDCDFF5L,0x4CDCDFF5L,0x798831AEL,0x36504DDBL,0x9B1E7C43L,0x36504DDBL,0x798831AEL}},{{0xC09811FCL,0xFF6A9302L,0xFF6A9302L,0xC09811FCL,0xEA019F24L,0xA834B848L,0xEA019F24L,0xC09811FCL,0xFF6A9302L},{(-1L),(-1L),0x9B1E7C43L,0x798831AEL,0L,0x798831AEL,0x9B1E7C43L,(-1L),(-1L)}}};
                int32_t l_1222[4][7] = {{0L,0L,0L,0L,0L,0L,0L},{0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL},{0L,0L,0L,0L,0L,0L,0L},{0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL,0x917C704EL}};
                int i, j, k;
                l_1176 &= 0x43296271L;
                if ((l_1201[7][0][7] &= (&g_436[4] != ((safe_div_func_uint32_t_u_u((safe_div_func_int8_t_s_s((safe_add_func_uint32_t_u_u(((safe_mod_func_uint32_t_u_u((~(safe_mul_func_int16_t_s_s(1L, 0x20CCL))), (l_1188 | p_25))) && g_189.f0), (safe_div_func_int16_t_s_s(((((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((l_1195[5] != ((((*l_1198) = l_1196[0][2]) != l_1199[6]) , (void*)0)) <= g_489.f1) && l_1176), 0x7FL)), (-1L))) , p_24) , p_24) <= 65527UL), 65535UL)))), g_5[8])), p_24)) , &g_916[0]))))
                { /* block id: 440 */
                    if (l_1167)
                        goto lbl_1202;
                }
                else
                { /* block id: 442 */
                    int8_t *l_1218 = &g_168;
                    int64_t l_1219 = 1L;
                    int32_t l_1221[8][10] = {{1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L,1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L},{1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L,1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L},{1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L,1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L},{1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L,1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L},{1L,0x3AE6E2CDL,0x3AE6E2CDL,1L,0x9A485976L,1L,0x3AE6E2CDL,1L,0xDAB238E8L,0xBC04DFF4L},{0xDAB238E8L,1L,1L,0xDAB238E8L,0xBC04DFF4L,0xDAB238E8L,1L,1L,0xDAB238E8L,0xBC04DFF4L},{0xDAB238E8L,1L,1L,0xDAB238E8L,0xBC04DFF4L,0xDAB238E8L,1L,1L,0xDAB238E8L,0xBC04DFF4L},{0xDAB238E8L,1L,1L,0xDAB238E8L,0xBC04DFF4L,0xDAB238E8L,1L,1L,0xDAB238E8L,0xBC04DFF4L}};
                    int i, j;
                    (*l_1045) = (**l_1044);
                    (*g_865) = (l_1222[0][1] = (p_25 | ((l_1221[0][9] = (safe_rshift_func_int8_t_s_s((safe_add_func_uint16_t_u_u((*g_56), (((l_1209 != (((safe_lshift_func_uint16_t_u_s(((((((*l_1218) ^= ((safe_mul_func_int16_t_s_s((((safe_lshift_func_uint16_t_u_u(0xE5D3L, p_25)) <= 8UL) && p_25), (l_1201[6][1][8] = p_24))) & (18446744073709551615UL >= (**g_834)))) == 0x57L) , (-1L)) > l_1219) >= p_25), 10)) | 0xFDC411FEBBB28EECLL) , (void*)0)) && l_1188) & l_1220))), 4))) >= p_25)));
                    l_1243 &= (l_1221[0][9] = (safe_div_func_uint16_t_u_u((l_1029 & (((safe_sub_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((safe_mul_func_int16_t_s_s((l_1219 < (safe_sub_func_uint32_t_u_u(g_91, (((safe_sub_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(p_24, 8)), (g_5[6] &= p_24))) <= (l_1237 == (((safe_div_func_int64_t_s_s((*g_986), g_489.f2)) , ((safe_mod_func_uint64_t_u_u((l_1242 < l_1188), 1UL)) != p_24)) , (void*)0))) ^ 0L)))), 0xC547L)), p_24)), l_1221[6][7])) , g_3[2]) > l_1221[0][9])), 0xB4D3L)));
                }
            }
            else
            { /* block id: 453 */
                int32_t * const l_1244 = (void*)0;
                int32_t **l_1260 = &g_198;
                int8_t *l_1283[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_1283[i] = &g_5[7];
                (*l_1260) = l_1244;
                (*l_1170) = (((*l_58) = (safe_div_func_int64_t_s_s((safe_rshift_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((safe_add_func_int32_t_s_s((l_998 = ((safe_mod_func_uint8_t_u_u(((*l_1136) = (((*l_33) = p_24) != (0L == (g_5[0] |= (l_1243 , (p_25 & (safe_lshift_func_int8_t_s_s((safe_add_func_int64_t_s_s(p_24, (safe_lshift_func_int16_t_s_s(0x7ED9L, (g_1277[0] , (((safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((0x316AL ^ ((l_1282 ^ g_328[1].f1) == 0UL)), g_3[1])), (*g_56))) , p_24) > 0x24L)))))), 7)))))))), g_3[1])) , (*g_647))), p_24)), p_24)), g_116[0])), 9UL))) , g_665.f0);
                for (l_1023 = 0; (l_1023 >= 0); l_1023 -= 1)
                { /* block id: 463 */
                    uint16_t l_1304 = 0x7406L;
                    int32_t ** const ** const l_1325 = &l_1324;
                    int32_t l_1331 = 0xF4A48EAAL;
                    int i;
                    (*l_1170) = (safe_mul_func_float_f_f((((+((safe_sub_func_uint32_t_u_u(0xBF9DBBB1L, (safe_mul_func_int8_t_s_s(g_84[l_1023], (safe_mod_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(((((safe_add_func_int64_t_s_s(((g_288 , ((void*)0 != &g_116[9])) == 0xAD6ED727509E7CA7LL), (safe_div_func_int64_t_s_s((-1L), ((safe_lshift_func_uint16_t_u_s((p_25 = 65535UL), ((**l_1209) , g_991.f2))) ^ p_24))))) <= p_24) > g_288) , 0x0F62L), 2)), l_1304)), l_1188)))))) & (*g_56))) , g_268[3][0][4].f2) < 0x0.2BC88Cp-32), 0x1.7p-1));
                    for (g_91 = 2; (g_91 >= 0); g_91 -= 1)
                    { /* block id: 468 */
                        const uint64_t l_1313 = 18446744073709551614UL;
                        int16_t *l_1328 = &g_116[2];
                        uint16_t *l_1329 = (void*)0;
                        uint16_t *l_1330 = &l_1304;
                        (*l_1260) = func_72(((safe_mod_func_uint64_t_u_u((safe_div_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((*g_56), ((safe_div_func_uint8_t_u_u(p_25, g_772.f2)) ^ ((*l_1330) = (l_1313 == (safe_sub_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u(((*l_1328) |= (safe_sub_func_uint8_t_u_u(((*l_1136)--), (safe_unary_minus_func_int32_t_s(((((l_1323[6] != l_1325) <= ((p_24 ^ (((*l_33) = (safe_sub_func_uint64_t_u_u(0x25F3A2C02294E9A6LL, 0x80BCCF25D71F345FLL))) <= p_24)) , p_25)) && 0x3AL) > g_84[0])))))), (*g_56))), p_24))))))), l_1331)), p_24)) , p_24));
                    }
                    if (l_1029)
                        goto lbl_1600;
                    for (l_1012 = 2; (l_1012 >= 0); l_1012 -= 1)
                    { /* block id: 477 */
                        return p_25;
                    }
                }
            }
            l_1333--;
            for (g_167 = 0; (g_167 <= 6); g_167 += 1)
            { /* block id: 485 */
                const float l_1339 = (-0x1.8p+1);
                int32_t l_1341 = 0xA6BB5069L;
                l_998 = p_24;
                l_1341 = ((**g_985) == ((p_25 , (((**l_1245) = p_24) & ((*g_56) || (~(safe_mod_func_int16_t_s_s(p_25, ((p_24 & p_25) & (0x966E9CE054DCA7E0LL > (l_1340 != (void*)0))))))))) == p_24));
                for (l_1010 = 6; (l_1010 >= 0); l_1010 -= 1)
                { /* block id: 491 */
                    for (g_138 = 0; (g_138 <= 5); g_138 += 1)
                    { /* block id: 494 */
                        const int64_t *l_1343 = (void*)0;
                        g_1344 = (p_25 ^ ((((l_1343 = l_1342) != (*g_985)) && (*g_865)) <= 0x21L));
                    }
                    return p_25;
                }
            }
        }
        else
        { /* block id: 501 */
            int64_t l_1350 = 0x128ACCB4D31C2014LL;
            int32_t l_1378 = 0x94E45394L;
            int32_t l_1386 = 0x523CC9E4L;
            int32_t l_1387 = (-1L);
            int32_t l_1390[6][6][6] = {{{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L},{(-4L),0xBD03A80CL,0x28AEF7FFL,1L,0x28AEF7FFL,0xBD03A80CL},{0x361C8DFCL,0xBD03A80CL,1L,0x6D0D8BAEL,0x41AA4559L,0xBD03A80CL},{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L}},{{(-4L),0xBD03A80CL,0x28AEF7FFL,1L,0x28AEF7FFL,0xBD03A80CL},{0x361C8DFCL,0xBD03A80CL,1L,0x6D0D8BAEL,0x41AA4559L,0xBD03A80CL},{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L},{(-4L),0xBD03A80CL,0x28AEF7FFL,1L,0x28AEF7FFL,0xBD03A80CL},{0x361C8DFCL,0xBD03A80CL,1L,0x6D0D8BAEL,0x41AA4559L,0xBD03A80CL}},{{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L},{(-4L),0xBD03A80CL,0x28AEF7FFL,1L,0x28AEF7FFL,0xBD03A80CL},{0x361C8DFCL,0xBD03A80CL,1L,0x6D0D8BAEL,0x41AA4559L,0xBD03A80CL},{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L}},{{(-4L),0xBD03A80CL,0x28AEF7FFL,1L,0x28AEF7FFL,0xBD03A80CL},{0x361C8DFCL,0xBD03A80CL,1L,0x6D0D8BAEL,0x41AA4559L,0xBD03A80CL},{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L},{(-4L),0xBD03A80CL,0x28AEF7FFL,1L,0x28AEF7FFL,0xBD03A80CL},{0x361C8DFCL,0xBD03A80CL,1L,0x6D0D8BAEL,0x41AA4559L,0xBD03A80CL}},{{(-4L),2L,0x28AEF7FFL,0x6D0D8BAEL,0x28AEF7FFL,2L},{0x361C8DFCL,2L,1L,1L,0x41AA4559L,2L},{(-4L),0xD8E7207FL,0x9902EB10L,2L,0x9902EB10L,0xD8E7207FL},{0x41AA4559L,0xD8E7207FL,0xDB8FFD43L,0xBD03A80CL,0x3D7B5AFBL,0xD8E7207FL},{0x28AEF7FFL,1L,0x9902EB10L,0xBD03A80CL,0x9902EB10L,1L},{0x41AA4559L,1L,0xDB8FFD43L,2L,0x3D7B5AFBL,1L}},{{0x28AEF7FFL,0xD8E7207FL,0x9902EB10L,2L,0x9902EB10L,0xD8E7207FL},{0x41AA4559L,0xD8E7207FL,0xDB8FFD43L,0xBD03A80CL,0x3D7B5AFBL,0xD8E7207FL},{0x28AEF7FFL,1L,0x9902EB10L,0xBD03A80CL,0x9902EB10L,1L},{0x41AA4559L,1L,0xDB8FFD43L,2L,0x3D7B5AFBL,1L},{0x28AEF7FFL,0xD8E7207FL,0x9902EB10L,2L,0x9902EB10L,0xD8E7207FL},{0x41AA4559L,0xD8E7207FL,0xDB8FFD43L,0xBD03A80CL,0x3D7B5AFBL,0xD8E7207FL}}};
            int32_t *l_1422 = (void*)0;
            uint64_t l_1446 = 4UL;
            int32_t **l_1452 = &g_263;
            int32_t **l_1455 = &l_58;
            int64_t **l_1468 = &l_1200;
            uint32_t l_1500[4][4][9] = {{{0x910BF283L,4UL,4294967286UL,1UL,0x647A2002L,4294967295UL,3UL,1UL,0xE0F522C2L},{3UL,0xEA528746L,4294967295UL,0xBD44C20CL,4294967291UL,0xBD390A29L,0x68C46B5AL,4294967286UL,0xEA528746L},{0x14E5D236L,0x4950C73EL,4294967295UL,4294967291UL,0x38B86E36L,0xBD44C20CL,4294967295UL,0x14E5D236L,4294967295UL},{0xE0F522C2L,7UL,4294967295UL,0x68C46B5AL,0xDC5E7191L,4UL,4294967291UL,4UL,0xDC5E7191L}},{{4UL,4294967295UL,4294967295UL,4UL,0x14E5D236L,1UL,4294967293UL,1UL,4294967286UL},{0UL,0x14E5D236L,4294967286UL,7UL,0x308E68CDL,0xDC5E7191L,4294967295UL,0x73D869C3L,0x37C1E2FDL},{4294967287UL,5UL,0x37C1E2FDL,0x49882907L,0x14E5D236L,1UL,3UL,0x4950C73EL,0UL},{0x910BF283L,0UL,1UL,4294967295UL,0xDC5E7191L,3UL,0xBD44C20CL,1UL,4UL}},{{0x4950C73EL,4294967293UL,3UL,0xCC48C982L,0x38B86E36L,0xBD390A29L,0xBD44C20CL,0xC9ADEDBFL,4294967293UL},{0x37C1E2FDL,3UL,0UL,4294967291UL,4294967291UL,0UL,3UL,0x37C1E2FDL,0xC9ADEDBFL},{4294967295UL,7UL,0xCB06673DL,3UL,0x647A2002L,4294967294UL,4294967295UL,0xE0F522C2L,0xDC5E7191L},{0UL,4294967287UL,4294967286UL,4294967294UL,0xEA528746L,0x647A2002L,4294967293UL,5UL,0xC9ADEDBFL}},{{0x73D869C3L,0x37C1E2FDL,0x38B86E36L,1UL,0x308E68CDL,0x7A773810L,4294967291UL,4294967295UL,4294967293UL},{4294967295UL,1UL,0x37C1E2FDL,0x647A2002L,4294967295UL,4294967295UL,4294967295UL,3UL,4UL},{0x910BF283L,1UL,4294967295UL,0UL,1UL,0x68C46B5AL,4294967286UL,4294967295UL,0xC9ADEDBFL},{0x7A773810L,0x9379E5A0L,0x7A773810L,4294967295UL,0xEA528746L,4294967295UL,1UL,0xDC5E7191L,0x9379E5A0L}}};
            float l_1516 = (-0x1.9p+1);
            struct S1 *l_1546 = &g_172[3];
            uint64_t l_1597 = 0x2D93789806FA40C9LL;
            int i, j, k;
            for (l_1022 = 0; (l_1022 <= 17); l_1022 = safe_add_func_uint8_t_u_u(l_1022, 3))
            { /* block id: 504 */
                float *l_1349[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_1349[i] = &g_163;
                l_1350 = (safe_add_func_float_f_f(((*l_1340) = g_199), 0x9.AE21EEp+85));
            }
            if ((*g_912))
            { /* block id: 508 */
                uint32_t l_1373 = 0xD0D71F4FL;
                int32_t l_1375 = 0x221C6ED2L;
                int32_t l_1379 = 0xAE155B90L;
                int32_t l_1389[10] = {1L,1L,1L,1L,1L,1L,1L,1L,1L,1L};
                uint8_t l_1401 = 254UL;
                int32_t *l_1412 = &l_1015[3][1][1];
                int32_t l_1421 = 2L;
                int32_t **l_1431[8] = {&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263};
                int32_t ** const l_1433[10] = {&l_58,&g_263,&g_263,&l_58,&g_263,&g_263,&l_58,&g_263,&g_263,&l_58};
                int16_t l_1441 = 0x5A0AL;
                int i;
                for (l_1020 = 0; (l_1020 >= 0); l_1020 -= 1)
                { /* block id: 511 */
                    int64_t ****l_1371[10][5][5] = {{{&g_1368,&g_1368,&g_1368,(void*)0,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,(void*)0},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{(void*)0,(void*)0,&g_1368,&g_1368,&g_1368}},{{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,(void*)0,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{(void*)0,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368}},{{&g_1368,(void*)0,(void*)0,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368}},{{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,(void*)0,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368}},{{&g_1368,&g_1368,&g_1368,&g_1368,(void*)0},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{(void*)0,(void*)0,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,(void*)0,&g_1368}},{{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{(void*)0,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,(void*)0,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368}},{{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,(void*)0},{(void*)0,(void*)0,&g_1368,&g_1368,(void*)0},{&g_1368,&g_1368,&g_1368,(void*)0,&g_1368}},{{&g_1368,(void*)0,&g_1368,(void*)0,(void*)0},{&g_1368,(void*)0,&g_1368,&g_1368,(void*)0},{&g_1368,(void*)0,&g_1368,(void*)0,&g_1368},{&g_1368,&g_1368,&g_1368,(void*)0,&g_1368},{(void*)0,&g_1368,&g_1368,&g_1368,&g_1368}},{{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,(void*)0},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368}},{{(void*)0,(void*)0,&g_1368,&g_1368,(void*)0},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368},{&g_1368,&g_1368,&g_1368,&g_1368,&g_1368},{&g_1368,(void*)0,&g_1368,&g_1368,&g_1368}}};
                    int16_t *l_1372 = &g_116[2];
                    int32_t l_1376 = 0xA6025A62L;
                    int32_t l_1380 = 0x72A376C6L;
                    int32_t l_1383 = (-8L);
                    int32_t l_1384 = 0x7CF69C70L;
                    int32_t l_1388 = (-10L);
                    uint64_t l_1391 = 0xADE5051EA5750A61LL;
                    int i, j, k;
                    (*g_957) = (safe_mod_func_uint64_t_u_u(((g_84[l_1020] || ((safe_add_func_uint32_t_u_u(0xDC5FC4A7L, ((*g_262) != &g_164[0]))) , (g_772.f2 <= (l_1350 == (((!(safe_sub_func_float_f_f(((safe_div_func_float_f_f((((*l_58) = ((safe_sub_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u((((safe_mod_func_uint16_t_u_u(((*g_56) |= g_84[l_1020]), ((*l_1372) &= (((g_1368 = g_1368) != &l_1199[6]) > 0xC416EA41L)))) , p_25) ^ p_25), 0L)) < l_1350) ^ 0UL), 0x38L)) , l_1373)) , g_59[1]), (-0x8.Ap+1))) > 0x6.615E8Dp+91), g_84[l_1020]))) , p_25) != g_84[l_1020]))))) | (-2L)), p_25));
                    for (l_1220 = 0; (l_1220 <= 0); l_1220 += 1)
                    { /* block id: 519 */
                        int32_t l_1374 = 1L;
                        int32_t l_1377 = 0x9F387054L;
                        int32_t l_1381 = 0x9ED0285FL;
                        int32_t l_1385 = 0x549DDC79L;
                        int i, j;
                        --l_1391;
                        return p_24;
                    }
                }
                g_1394--;
                if ((g_1344 , (((void*)0 == (*g_1070)) & (-1L))))
                { /* block id: 525 */
                    int32_t l_1397[5] = {8L,8L,8L,8L,8L};
                    int32_t l_1398 = 3L;
                    int32_t l_1399[6][2];
                    uint64_t **l_1409 = &g_835[0];
                    int32_t *l_1410 = &l_1025[0];
                    int32_t *** const l_1420[7][5] = {{&l_1065[2][0],(void*)0,(void*)0,&l_1065[2][0],&l_1065[0][0]},{&l_1065[1][0],&l_1065[0][0],&l_1065[1][0],&l_1065[0][0],&l_1065[1][0]},{&l_1065[2][0],&l_1065[2][0],(void*)0,(void*)0,&l_1065[0][0]},{(void*)0,&l_1065[0][0],&l_1065[0][0],&l_1065[0][0],(void*)0},{&l_1065[0][0],(void*)0,(void*)0,&l_1065[2][0],&l_1065[2][0]},{&l_1065[1][0],&l_1065[0][0],&l_1065[1][0],&l_1065[0][0],&l_1065[1][0]},{&l_1065[0][0],&l_1065[2][0],(void*)0,(void*)0,&l_1065[2][0]}};
                    int i, j;
                    for (i = 0; i < 6; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_1399[i][j] = 4L;
                    }
                    for (l_1020 = 0; (l_1020 >= 0); l_1020 -= 1)
                    { /* block id: 528 */
                        int i;
                        return g_164[(l_1020 + 2)];
                    }
                    l_1401--;
                    for (l_1038 = 9; (l_1038 >= 1); l_1038 -= 1)
                    { /* block id: 534 */
                        int32_t *l_1411 = (void*)0;
                        int i;
                        l_1411 = (l_1410 = &l_1379);
                        if (p_24)
                            break;
                        l_1413 = (l_1412 = &l_1389[l_1038]);
                        (*l_1340) = ((safe_div_func_float_f_f(((safe_div_func_float_f_f(((safe_div_func_float_f_f(((((*g_825) == l_1420[5][4]) , ((0x5.414987p+44 < ((g_133 <= (l_1421 , (l_1386 = (((l_1422 = (p_25 , func_72(l_1350))) != &l_1378) , p_24)))) == g_57[0][1][2])) > p_24)) < (-0x10.4p-1)), (*l_1412))) < 0x1.C33DD4p-36), p_24)) < g_188), p_25)) <= g_897.f1);
                    }
                }
                else
                { /* block id: 544 */
                    int32_t ***l_1432 = &l_1431[4];
                    int32_t l_1435 = 0x0724EFE9L;
                    int8_t *l_1438 = (void*)0;
                    int8_t *l_1439 = (void*)0;
                    int8_t *l_1440 = &g_168;
                    l_1412 = &l_1379;
                    l_1390[3][4][3] |= (((((+1UL) ^ (safe_mod_func_int16_t_s_s(((!(((safe_lshift_func_uint16_t_u_u(((*l_33)++), ((*g_56) = (((*l_1432) = l_1431[5]) != l_1433[0])))) >= ((safe_unary_minus_func_uint64_t_u(((((**g_834) > ((*l_1412) , p_24)) <= l_1435) & g_59[0]))) | (safe_lshift_func_int8_t_s_u(((*l_1440) = 0xDCL), g_138)))) , l_1441)) , p_24), g_489.f2))) ^ p_24) >= l_1435) < (*l_1413));
                    for (g_188 = 0; (g_188 != 32); g_188 = safe_add_func_uint64_t_u_u(g_188, 4))
                    { /* block id: 553 */
                        uint16_t l_1445 = 65535UL;
                        return l_1445;
                    }
                }
                ++l_1446;
            }
            else
            { /* block id: 558 */
                int32_t ***l_1454[5];
                uint64_t *l_1456[3];
                int32_t l_1470 = 1L;
                float **l_1477 = &l_1340;
                uint8_t ** const *l_1496[2][4][10] = {{{&l_1135,&g_922,&g_922,&g_922,&l_1135,&g_922,&g_922,&l_1135,&g_922,&g_922},{&g_922,&g_922,&g_922,&l_1135,&g_922,&l_1135,&g_922,&g_922,&g_922,&g_922},{&g_922,&l_1135,&g_922,&g_922,&l_1135,&g_922,&g_922,&g_922,&l_1135,&g_922},{&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922}},{{&g_922,&g_922,&g_922,&l_1135,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922},{&g_922,&g_922,&l_1135,&g_922,&g_922,&g_922,&g_922,&g_922,&l_1135,&l_1135},{&g_922,&l_1135,&g_922,&g_922,&g_922,&g_922,&l_1135,&g_922,&l_1135,&g_922},{&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922,&g_922}}};
                int64_t l_1503 = (-4L);
                int32_t *l_1558[2];
                float l_1572 = 0xA.77C91Cp-18;
                uint16_t l_1581 = 0UL;
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_1454[i] = &l_1453;
                for (i = 0; i < 3; i++)
                    l_1456[i] = &g_84[0];
                for (i = 0; i < 2; i++)
                    l_1558[i] = &l_1014;
                l_1470 &= ((g_1449 , (safe_mul_func_int8_t_s_s(((l_1469 = (3UL == ((((**g_834) = (l_1452 != (l_1455 = l_1453))) ^ ((*l_1413) , ((!(((safe_mul_func_int8_t_s_s(((((safe_div_func_uint8_t_u_u(((((0x89FAL != (safe_rshift_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(g_164[2], 2)), 7))) != (((((l_1468 == &l_1200) , (*l_1413)) & 0x02FF9F74BFEA1803LL) <= p_24) < p_25)) >= p_25) <= 0L), g_364)) && (*g_56)) , p_24) <= 65535UL), p_25)) <= 0x0DEAL) , 4L)) > 0x460AL))) & p_25))) & p_25), g_3[1]))) <= 0UL);
                l_1390[0][0][5] = (~((p_25 >= (((g_1478 = ((7UL | ((p_24 | (((safe_rshift_func_uint16_t_u_u(((*g_865) < (p_24 , (l_1422 == ((*l_1477) = ((safe_div_func_int16_t_s_s(((p_24 != (!(((*l_1413) &= (((void*)0 == &l_1333) | (**g_1369))) < 0x176690ACL))) ^ 4UL), (*g_56))) , l_1340))))), 5)) , p_24) < g_471.f2)) <= p_25)) < p_25)) | (***g_1368)) < p_24)) , p_24));
                if (((safe_lshift_func_uint8_t_u_s((g_471.f1 | g_205.f3), 6)) | ((*g_1370) >= (safe_rshift_func_int16_t_s_s((((*l_1413) = ((safe_rshift_func_int8_t_s_u((((((g_91 |= (g_1485[1] == (void*)0)) , (((safe_mul_func_int8_t_s_s(g_897.f2, (l_1496[0][1][7] == (void*)0))) == (((g_1497 = (void*)0) != (void*)0) <= p_24)) && p_24)) , p_24) | g_897.f1) <= p_25), l_1500[0][3][2])) , 0x69C556451999043FLL)) && p_25), 4)))))
                { /* block id: 570 */
                    int64_t l_1504 = 0L;
                    int32_t l_1505 = 0xDA776594L;
                    int32_t *l_1509 = &g_164[1];
                    int32_t **l_1529 = &l_1413;
                    int16_t *l_1530 = &g_1478;
                    int8_t *l_1536 = (void*)0;
                    int8_t *l_1537[3][8] = {{&l_1400,(void*)0,&g_168,(void*)0,&l_1400,&g_5[6],&l_1400,(void*)0},{(void*)0,(void*)0,(void*)0,&g_5[6],&l_1400,&g_5[6],(void*)0,(void*)0},{&l_1400,&g_5[6],(void*)0,(void*)0,(void*)0,&g_5[6],&l_1400,&g_5[6]}};
                    int32_t l_1541 = 0x2696895FL;
                    int32_t l_1542 = 1L;
                    uint8_t l_1543 = 255UL;
                    int i, j;
lbl_1548:
                    if ((g_1502 , p_25))
                    { /* block id: 571 */
                        uint32_t l_1506 = 0xCE0BC44DL;
                        float *l_1515 = &l_1442;
                        uint32_t *l_1522 = &g_138;
                        int8_t *l_1531 = &g_5[4];
                        l_1506--;
                        l_1516 = (((*l_1340) = (l_1509 != &l_1027)) >= ((safe_div_func_float_f_f(p_25, (safe_mul_func_float_f_f(p_24, l_1503)))) == ((*l_1515) = (0x7.3E45A3p+66 <= (g_1514 < 0x9.Bp-1)))));
                        (*l_1529) = (l_1422 = func_72(((*l_1509) = (p_25 && (safe_mod_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((safe_unary_minus_func_uint32_t_u((++(*l_1522)))), (safe_rshift_func_int16_t_s_u((((**l_1135) = l_1504) & g_189.f0), 10)))), (safe_mul_func_uint8_t_u_u((0xFC5D97BBL || (((l_1529 == &l_1422) & (l_1530 != &g_116[0])) & ((*l_1531) = ((((*g_56) , p_24) | 0x2811BEF14E7290B3LL) | p_25)))), g_59[2]))))))));
                    }
                    else
                    { /* block id: 582 */
                        return p_24;
                    }
                    if (((*l_1422) = (g_1277[0].f3 & ((safe_mul_func_int8_t_s_s(p_25, (-2L))) != ((p_24 = 1L) | ((safe_rshift_func_uint8_t_u_s(252UL, 7)) >= g_5[2]))))))
                    { /* block id: 587 */
                        int64_t l_1540[7][3] = {{0L,(-10L),9L},{0x11AFC7394850851FLL,6L,0x11AFC7394850851FLL},{0x11AFC7394850851FLL,0L,6L},{0L,0x11AFC7394850851FLL,0x11AFC7394850851FLL},{6L,0x11AFC7394850851FLL,9L},{(-10L),0L,(-5L)},{6L,6L,(-5L)}};
                        int i, j;
                        l_1543++;
                    }
                    else
                    { /* block id: 589 */
                        struct S1 **l_1547 = &l_1237;
                        (*l_1547) = ((**l_1044) = l_1546);
                    }
                    if (l_998)
                        goto lbl_1548;
                }
                else
                { /* block id: 594 */
                    int32_t * const l_1561[3] = {&l_1020,&l_1020,&l_1020};
                    int i;
                    if ((0xFDL >= (&g_590 != (void*)0)))
                    { /* block id: 595 */
                        int64_t l_1557[10] = {0x798098FB039AFA6ELL,0x7C3F9FBD9A52A010LL,0x798098FB039AFA6ELL,9L,9L,0x798098FB039AFA6ELL,0x7C3F9FBD9A52A010LL,0x798098FB039AFA6ELL,9L,9L};
                        uint16_t **l_1560 = (void*)0;
                        int32_t **l_1562[4][4][4] = {{{&l_1558[0],(void*)0,&l_1413,&g_198},{&l_1413,&l_1422,&l_1558[0],&l_1413},{(void*)0,&l_1422,&l_1413,&g_198},{&l_1422,(void*)0,&g_198,&l_1422}},{{(void*)0,&g_198,(void*)0,(void*)0},{&l_1558[1],(void*)0,&l_1413,(void*)0},{&l_1558[0],&l_1558[1],(void*)0,&l_1558[0]},{&l_1413,&l_1558[0],&l_1422,(void*)0}},{{&g_198,&l_1558[0],(void*)0,&g_198},{&g_198,(void*)0,&l_1422,&g_198},{&l_1413,&g_198,(void*)0,&l_1413},{&l_1558[0],&l_1558[1],&l_1413,&l_1558[0]}},{{&l_1558[1],(void*)0,(void*)0,&l_1558[1]},{(void*)0,&g_198,&g_198,(void*)0},{&l_1422,&l_1558[0],&l_1413,&l_1558[0]},{(void*)0,&l_1558[1],&l_1558[0],&l_1558[0]}}};
                        int32_t **l_1563 = (void*)0;
                        int32_t **l_1564 = &l_1558[1];
                        uint32_t *l_1567 = &l_1500[0][3][2];
                        int8_t *l_1575 = &g_5[6];
                        int i, j, k;
                        l_1558[0] = func_72(((--(***g_1498)) || (0xA84FL ^ (safe_mul_func_int16_t_s_s((safe_mod_func_uint64_t_u_u(l_1557[7], p_24)), 65535UL)))));
                        (*l_1564) = l_1561[0];
                        (*g_957) |= (p_25 , ((((*l_33) = ((safe_sub_func_int64_t_s_s((-1L), p_25)) != ((*l_1567) = 4294967290UL))) >= (safe_add_func_uint64_t_u_u(18446744073709551612UL, p_24))) && ((safe_sub_func_float_f_f(l_1572, p_25)) , ((*l_1575) = (safe_add_func_uint8_t_u_u(p_24, (*l_1413)))))));
                    }
                    else
                    { /* block id: 604 */
                        int32_t **l_1577 = &l_1422;
                        int32_t l_1578 = 1L;
                        int32_t l_1579 = 0x0A8537BFL;
                        int8_t l_1580 = 0xEAL;
                        (*l_1577) = l_1576;
                        l_1581++;
                        (*l_1413) ^= (safe_sub_func_int8_t_s_s(((safe_add_func_int64_t_s_s(p_25, ((*l_1422) = (p_25 | (safe_sub_func_uint8_t_u_u((*l_1422), (1UL || ((safe_mod_func_int32_t_s_s(p_24, (safe_sub_func_int8_t_s_s((((p_25 , (0x792766E678953D5ALL && ((**g_1369) = ((((safe_lshift_func_uint16_t_u_u((((**l_1455) = 0L) , (*g_56)), 8)) && (**l_1577)) & 0xA83093BB6BD6AD18LL) , (**g_985))))) && p_25) , p_24), (*l_1422))))) >= p_25)))))))) , g_471.f1), 0xB2L));
                    }
                }
            }
            ++l_1597;
        }
        l_1413 = &l_1469;
    }
    (*l_1631) |= (0xCC880D54L | ((*l_1630) = (safe_rshift_func_int8_t_s_s((!((((safe_lshift_func_int8_t_s_u(((18446744073709551615UL != ((((safe_mul_func_int16_t_s_s((*l_1576), ((*l_1629) ^= (safe_mod_func_uint32_t_u_u((((((*l_1627) = (safe_add_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(g_690.f1, (safe_lshift_func_uint8_t_u_u((safe_add_func_int64_t_s_s((((((--(*l_1619)) < p_25) < ((***g_1368) = (safe_rshift_func_uint8_t_u_s((&l_1282 != &l_1282), 4)))) != (*l_1576)) < ((((safe_mul_func_int16_t_s_s(5L, (*l_1576))) > p_24) < 0x635DL) , p_25)), (*l_1413))), 4)))), l_1626[0]))) && p_24) && g_1502.f2) || g_1628), 0x1C7191AFL))))) > p_24) >= p_24) , 0x3CB7B286D542D875LL)) , p_25), (*l_1413))) >= (*l_1413)) , (*l_1413)) >= (*l_1413))), 1))));
    (*l_1631) = (safe_lshift_func_int16_t_s_u(((safe_mod_func_int16_t_s_s(((*l_1413) = ((*l_1576) && ((*l_1631) || (((safe_mul_func_int16_t_s_s(0x6194L, ((*l_1629) &= (safe_add_func_int32_t_s_s((((g_1497 = &g_1498) == (g_1643 = &g_1498)) < 0xC466BA10L), 0xBCE1205EL))))) == ((void*)0 != &g_914)) != p_24)))), 0x6E7DL)) , (*l_1413)), 2));
    return p_24;
}


/* ------------------------------------------ */
/* 
 * reads : g_59
 * writes: g_59
 */
static uint8_t  func_35(uint64_t  p_36, uint16_t * p_37, int32_t * p_38)
{ /* block id: 381 */
    int32_t l_984 = 0x16AFA735L;
    (*p_38) = (*p_38);
    return l_984;
}


/* ------------------------------------------ */
/* 
 * reads : g_91 g_471.f1 g_189.f2 g_161 g_647 g_648 g_652 g_84 g_328.f3 g_172.f1 g_199 g_56 g_57 g_188 g_80 g_205.f1 g_471.f2 g_231.f1 g_5 g_145 g_288 g_690.f2 g_772 g_489.f1 g_138 g_164 g_653 g_825 g_437 g_826 g_833 g_834 g_865 g_205.f3 g_116 g_168 g_3 g_133 g_897 g_591 g_198 g_59 g_912 g_917 g_921 g_471.f0 g_205.f2 g_268 g_957
 * writes: g_91 g_168 g_84 g_199 g_648 g_653 g_663 g_664 g_188 g_57 g_80 g_116 g_288 g_191 g_304 g_198 g_914 g_922 g_138 g_161
 */
static int32_t  func_41(int16_t  p_42, uint64_t  p_43, float  p_44, int8_t  p_45, int8_t  p_46)
{ /* block id: 226 */
    uint8_t l_629 = 0x01L;
    int32_t ***l_638 = &g_591;
    float *l_640 = &g_304;
    struct S1 *l_686[10][10][2] = {{{&g_172[4],&g_172[4]},{&g_489,&g_471},{(void*)0,&g_489},{&g_172[2],&g_172[1]},{(void*)0,&g_172[4]},{&g_172[4],&g_489},{&g_172[4],&g_172[4]},{&g_489,&g_172[4]},{&g_471,&g_489},{&g_172[3],&g_172[4]}},{{&g_172[0],(void*)0},{(void*)0,&g_471},{&g_172[1],&g_172[4]},{&g_172[1],&g_172[1]},{&g_489,&g_489},{&g_471,&g_172[0]},{&g_471,(void*)0},{&g_489,&g_471},{&g_172[4],&g_172[2]},{&g_172[4],&g_471}},{{&g_172[4],(void*)0},{(void*)0,&g_471},{&g_172[0],&g_172[0]},{&g_489,&g_172[3]},{&g_172[3],&g_471},{&g_172[4],&g_489},{(void*)0,&g_489},{&g_489,&g_172[2]},{(void*)0,&g_489},{&g_172[4],&g_489}},{{&g_489,&g_489},{&g_172[4],&g_172[1]},{&g_471,&g_489},{&g_489,&g_172[4]},{(void*)0,&g_172[4]},{&g_172[3],&g_172[0]},{(void*)0,&g_172[4]},{&g_489,&g_172[3]},{&g_489,&g_172[1]},{(void*)0,&g_471}},{{&g_172[2],&g_172[4]},{&g_489,&g_172[1]},{(void*)0,&g_172[1]},{&g_489,&g_172[4]},{&g_172[2],&g_471},{(void*)0,&g_172[1]},{&g_489,&g_172[3]},{&g_489,&g_172[4]},{(void*)0,&g_172[0]},{&g_172[3],&g_172[4]}},{{(void*)0,&g_172[4]},{&g_489,&g_489},{&g_471,&g_172[1]},{&g_172[4],&g_489},{&g_489,&g_489},{&g_172[4],&g_489},{(void*)0,&g_172[2]},{&g_489,&g_489},{(void*)0,&g_489},{&g_172[4],&g_471}},{{&g_172[3],&g_172[3]},{&g_489,&g_172[0]},{&g_172[0],&g_471},{(void*)0,(void*)0},{&g_172[4],(void*)0},{&g_172[4],(void*)0},{&g_172[4],(void*)0},{&g_172[4],(void*)0},{(void*)0,&g_471},{&g_172[0],&g_172[0]}},{{&g_489,&g_172[3]},{&g_172[3],&g_471},{&g_172[4],&g_489},{(void*)0,&g_489},{&g_489,&g_172[2]},{(void*)0,&g_489},{&g_172[4],&g_489},{&g_489,&g_489},{&g_172[4],&g_172[1]},{&g_471,&g_489}},{{&g_489,&g_172[4]},{(void*)0,&g_172[4]},{&g_172[3],&g_172[0]},{(void*)0,&g_172[4]},{&g_489,&g_172[3]},{&g_489,&g_172[1]},{(void*)0,&g_471},{&g_172[2],&g_172[4]},{&g_489,&g_172[1]},{(void*)0,&g_172[1]}},{{&g_489,&g_172[4]},{&g_172[2],&g_471},{(void*)0,&g_172[1]},{&g_489,&g_172[3]},{&g_489,&g_172[4]},{(void*)0,&g_172[0]},{&g_172[3],&g_172[4]},{(void*)0,&g_172[4]},{&g_489,&g_489},{&g_471,&g_172[1]}}};
    float l_717[1];
    int32_t l_724 = 1L;
    int32_t l_725 = 0x8F733FF3L;
    int32_t l_728[10];
    int32_t l_739 = 0xD6C3B4CAL;
    float l_829 = 0x1.4p-1;
    uint8_t l_931 = 1UL;
    const uint32_t l_944[8] = {4294967295UL,4294967290UL,4294967295UL,4294967290UL,4294967295UL,4294967290UL,4294967295UL,4294967290UL};
    uint32_t *l_949[9] = {(void*)0,&g_167,(void*)0,&g_167,(void*)0,&g_167,(void*)0,&g_167,(void*)0};
    int64_t l_950 = 1L;
    float l_951[7][5] = {{0xE.210F0Ep+90,0x7.1p+1,0x1.6p+1,0x1.6p+1,0x7.1p+1},{0x0.Cp+1,0x1.Bp-1,0x1.6p+1,0x3.F8C0A9p+2,0x3.F8C0A9p+2},{0x1.Bp-1,0x0.Cp+1,0x1.Bp-1,0x1.6p+1,0x0.Cp+1},{0x1.6p+1,0x1.Bp-1,0x0.Cp+1,0x1.Bp-1,0x1.6p+1},{0x7.1p+1,0x1.Bp-1,(-0x1.Fp-1),0x1.6p+1,(-0x1.Fp-1)},{(-0x1.Fp-1),(-0x1.Fp-1),0x0.Cp+1,0x1.6p+1,0xE.210F0Ep+90},{0x1.Bp-1,0x7.1p+1,0x7.1p+1,0x1.Bp-1,(-0x1.Fp-1)}};
    uint8_t *l_954 = &g_161;
    uint8_t *l_955 = (void*)0;
    uint8_t *l_956 = &l_629;
    int32_t *l_958 = &l_728[5];
    int32_t *l_959 = &l_728[2];
    int32_t *l_960 = &l_739;
    int32_t *l_961 = (void*)0;
    int32_t *l_962 = &l_739;
    int32_t *l_963 = &g_199;
    int32_t *l_964 = (void*)0;
    int32_t *l_965 = (void*)0;
    int32_t *l_966 = &l_724;
    int32_t *l_967 = (void*)0;
    int32_t *l_968 = &l_725;
    int32_t *l_969 = (void*)0;
    int32_t *l_970 = (void*)0;
    int32_t *l_971 = &l_739;
    int32_t *l_972 = &l_725;
    int32_t *l_973 = &l_725;
    int32_t *l_974 = &g_199;
    int32_t *l_975[10][9][2] = {{{&g_3[1],&g_199},{&l_724,&l_725},{&l_725,&g_3[1]},{&l_739,&g_3[1]},{&l_725,&l_725},{&l_724,&g_199},{&g_3[1],&l_728[3]},{&l_728[0],&l_728[6]},{&g_199,&g_3[0]}},{{&g_199,&g_199},{&l_724,(void*)0},{&g_199,&l_728[5]},{&g_199,&g_3[1]},{&l_728[5],&l_728[6]},{&l_724,&g_199},{&g_3[1],&l_724},{&g_199,&l_725},{&l_728[0],&g_3[0]}},{{&l_739,&g_648},{&l_724,&l_725},{&g_199,&g_199},{&g_3[1],&l_728[5]},{&l_725,&l_728[6]},{&l_728[3],&g_648},{&g_199,&l_728[3]},{&g_199,(void*)0},{&g_199,&l_728[3]}},{{&g_199,&g_648},{&l_728[3],&l_728[6]},{&l_725,&l_728[5]},{&g_3[1],&g_199},{&g_199,&l_725},{&l_724,&g_648},{&l_739,&g_3[0]},{&l_728[0],&l_725},{&g_199,&l_724}},{{&g_3[1],&g_199},{&l_724,&l_728[6]},{&l_728[5],&g_3[1]},{&g_199,&l_728[5]},{&g_199,(void*)0},{&l_724,&g_199},{&g_199,&g_3[0]},{&g_199,&l_728[6]},{&l_728[0],&l_728[3]}},{{&g_3[1],&g_199},{&l_724,&l_725},{&l_725,&g_3[1]},{&l_739,&g_3[1]},{&l_725,&l_725},{&l_724,&g_199},{&g_3[1],&l_728[3]},{&l_728[0],&l_728[6]},{&g_199,&g_3[0]}},{{&g_199,&g_199},{&l_724,(void*)0},{&g_199,&l_728[5]},{&g_199,&g_3[1]},{&l_728[5],&l_728[6]},{&l_724,&g_199},{&g_3[1],&l_724},{&g_199,&l_725},{&l_728[0],&g_3[0]}},{{&l_739,&g_648},{&l_724,&l_725},{&g_199,&g_199},{&g_3[1],&l_728[5]},{&l_725,&l_728[6]},{&l_728[3],&g_648},{&g_199,&l_728[3]},{&g_199,(void*)0},{&g_199,&l_728[3]}},{{&g_199,&g_648},{&l_728[3],&l_728[6]},{&l_725,&l_728[5]},{&g_3[1],&g_199},{&g_199,&l_725},{&l_724,&g_648},{&l_739,&g_3[0]},{&l_728[0],&l_725},{&g_199,&l_724}},{{&g_3[1],&g_199},{&l_724,&l_728[6]},{&l_728[5],&g_3[1]},{&g_199,&l_728[5]},{&l_739,&g_199},{&l_725,&g_199},{&g_648,(void*)0},{&g_199,&l_728[5]},{&g_3[2],(void*)0}}};
    uint32_t l_976 = 0x73A36A83L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_717[i] = 0x8.B210D3p-65;
    for (i = 0; i < 10; i++)
        l_728[i] = 0xB2307A26L;
    if ((safe_unary_minus_func_uint16_t_u(0UL)))
    { /* block id: 227 */
        uint32_t l_643 = 0x3CDA39CEL;
        uint32_t *l_659 = &g_660;
        int8_t l_666 = 0x9DL;
        int32_t l_704 = (-8L);
        int32_t l_719 = 0xC5B16FAFL;
        int32_t l_720 = (-10L);
        int32_t l_721 = (-10L);
        int32_t l_723 = 0L;
        int32_t l_729 = (-10L);
        int32_t l_730[10][10] = {{0x69213789L,0x91EE85A9L,5L,0x1434E6D2L,0xA9638E3DL,(-6L),1L,0x84D2856BL,1L,0x4D588411L},{0x92CBC7C7L,3L,0x4EC7A24CL,(-1L),0x4C9D127FL,(-6L),0x1434E6D2L,0x59CC6E2CL,1L,0xA8F85CE0L},{0x69213789L,0x9F537D60L,0x4D588411L,0x59CC6E2CL,(-1L),4L,0x1A26C63BL,5L,0xFF91F424L,0xFF91F424L},{0xFF91F424L,0xDA2DFF12L,0L,1L,1L,0L,0xDA2DFF12L,0xFF91F424L,0xA8F85CE0L,1L},{(-1L),(-4L),0xDDC4E4B5L,0x4EC7A24CL,0x9F537D60L,0xDA2DFF12L,0xA8F85CE0L,(-1L),0x4D588411L,1L},{0xA8F85CE0L,0x92CBC7C7L,0xDDC4E4B5L,0x9F537D60L,5L,(-1L),0x7505B5B1L,0xFF91F424L,(-1L),0x21D13FABL},{0xA9638E3DL,0xDDC4E4B5L,0L,1L,0x7505B5B1L,0x1A26C63BL,4L,5L,4L,0x1A26C63BL},{0x4EC7A24CL,0x1A26C63BL,0x4D588411L,0x1A26C63BL,0x4EC7A24CL,0xA9638E3DL,0x21D13FABL,0x59CC6E2CL,0x4C9D127FL,3L},{0x59CC6E2CL,0xFF91F424L,0x4EC7A24CL,(-1L),3L,0x7505B5B1L,(-1L),0x84D2856BL,0xDA2DFF12L,3L},{4L,(-1L),5L,(-6L),0x4EC7A24CL,1L,(-1L),0x917950DDL,0x84D2856BL,0x1A26C63BL}};
        uint32_t l_798 = 0xBAA666DEL;
        int8_t l_831 = 0xC5L;
        struct S1 *l_895 = &g_489;
        const int32_t *l_903 = &g_648;
        int32_t l_906 = (-10L);
        uint8_t **l_920 = (void*)0;
        int8_t *l_940 = (void*)0;
        int8_t *l_941 = &g_168;
        int32_t *l_942[3];
        int i, j;
        for (i = 0; i < 3; i++)
            l_942[i] = &l_730[2][4];
        if (l_629)
        { /* block id: 228 */
            uint16_t l_637 = 0UL;
            struct S1 *l_651 = &g_471;
            int32_t ***l_695 = &g_591;
            int32_t l_718 = 0x25B41298L;
            int32_t l_722 = 1L;
            int32_t l_726 = 0xB77A06EFL;
            int32_t l_727 = (-8L);
            int32_t l_731 = (-1L);
            int32_t l_732 = 0x81B63BC1L;
            int32_t l_733 = 0L;
            int32_t l_734 = 0x88FE2102L;
            int32_t l_735 = 0L;
            int32_t l_736 = 0x3B0B55FAL;
            int32_t l_737 = (-1L);
            int32_t l_738[10][9][2] = {{{0xA704BAFFL,1L},{0xC815414DL,0xB39A01DFL},{0xA448726CL,0x49F26E56L},{0L,(-5L)},{(-1L),0L},{0xA704BAFFL,0xB7CB4ECDL},{5L,0L},{(-1L),0xE44F7A02L},{0L,0x49F26E56L}},{{2L,0L},{0xC815414DL,0xA704BAFFL},{1L,0L},{0xA448726CL,(-5L)},{1L,0x653CF854L},{2L,0L},{5L,5L},{0xC815414DL,0L},{0L,0x49F26E56L}},{{0xB39A01DFL,0x653CF854L},{(-1L),0xB39A01DFL},{1L,0xB7CB4ECDL},{1L,0xB39A01DFL},{(-1L),0x653CF854L},{0xB39A01DFL,0x49F26E56L},{0L,0L},{0xC815414DL,5L},{5L,0L}},{{2L,0x653CF854L},{1L,(-5L)},{0xA448726CL,0L},{1L,0xA704BAFFL},{0xC815414DL,0L},{2L,0x49F26E56L},{0L,0xE44F7A02L},{(-1L),0L},{5L,0xB7CB4ECDL}},{{0xA704BAFFL,0L},{(-1L),(-5L)},{0L,0x49F26E56L},{0xA448726CL,0xB39A01DFL},{0xCF66B113L,0x8E620607L},{0L,1L},{4L,0xC815414DL},{1L,0xC815414DL},{4L,1L}},{{0L,0x8E620607L},{0xCF66B113L,0L},{0xC235EE10L,0x79B2AEA8L},{(-5L),0xB7CB4ECDL},{1L,(-5L)},{0L,0x18442079L},{1L,(-1L)},{1L,0xC815414DL},{(-1L),0x79B2AEA8L}},{{1L,(-5L)},{0xCF66B113L,0L},{0x8E620607L,1L},{0xC235EE10L,0xB7CB4ECDL},{1L,0x3729BB2FL},{1L,1L},{1L,1L},{0xCF66B113L,(-1L)},{4L,0x79B2AEA8L}},{{0L,0x3729BB2FL},{1L,0L},{0x8E620607L,0x18442079L},{0x8E620607L,0L},{1L,0x3729BB2FL},{0L,0x79B2AEA8L},{4L,(-1L)},{0xCF66B113L,1L},{1L,1L}},{{1L,0x3729BB2FL},{1L,0xB7CB4ECDL},{0xC235EE10L,1L},{0x8E620607L,0L},{0xCF66B113L,(-5L)},{1L,0x79B2AEA8L},{(-1L),0xC815414DL},{1L,(-1L)},{1L,0x18442079L}},{{0L,(-5L)},{1L,0xB7CB4ECDL},{(-5L),0x79B2AEA8L},{0xC235EE10L,0L},{0xCF66B113L,0x8E620607L},{0L,1L},{4L,0xC815414DL},{1L,0xC815414DL},{4L,1L}}};
            uint8_t l_740 = 0xBBL;
            int i, j, k;
            for (g_91 = 28; (g_91 >= 19); g_91 = safe_sub_func_int32_t_s_s(g_91, 8))
            { /* block id: 231 */
                int8_t *l_634 = &g_168;
                float *l_639[9][5][5] = {{{(void*)0,(void*)0,(void*)0,(void*)0,&g_304},{&g_126,(void*)0,(void*)0,&g_126,&g_126},{(void*)0,&g_126,&g_304,(void*)0,&g_304},{(void*)0,(void*)0,&g_163,&g_126,&g_126},{(void*)0,(void*)0,&g_304,&g_163,&g_304}},{{(void*)0,&g_126,(void*)0,&g_126,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_304},{&g_126,(void*)0,(void*)0,&g_126,&g_126},{(void*)0,&g_126,&g_304,(void*)0,&g_304},{(void*)0,(void*)0,&g_163,&g_126,&g_126}},{{(void*)0,(void*)0,&g_304,&g_163,&g_304},{(void*)0,&g_126,(void*)0,&g_126,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_304},{&g_126,(void*)0,(void*)0,&g_126,&g_126},{(void*)0,&g_126,&g_304,(void*)0,&g_304}},{{(void*)0,(void*)0,&g_163,&g_126,&g_126},{(void*)0,(void*)0,&g_304,&g_163,&g_304},{(void*)0,&g_126,(void*)0,&g_126,(void*)0},{(void*)0,(void*)0,&g_126,&g_163,&g_163},{&g_163,(void*)0,(void*)0,&g_126,(void*)0}},{{&g_304,(void*)0,&g_304,&g_163,&g_304},{(void*)0,(void*)0,&g_126,&g_163,(void*)0},{(void*)0,(void*)0,&g_304,&g_126,&g_163},{(void*)0,&g_163,(void*)0,&g_163,(void*)0},{&g_304,(void*)0,&g_126,&g_163,&g_163}},{{&g_163,(void*)0,(void*)0,&g_126,(void*)0},{&g_304,(void*)0,&g_304,&g_163,&g_304},{(void*)0,(void*)0,&g_126,&g_163,(void*)0},{(void*)0,(void*)0,&g_304,&g_126,&g_163},{(void*)0,&g_163,(void*)0,&g_163,(void*)0}},{{&g_304,(void*)0,&g_126,&g_163,&g_163},{&g_163,(void*)0,(void*)0,&g_126,(void*)0},{&g_304,(void*)0,&g_304,&g_163,&g_304},{(void*)0,(void*)0,&g_126,&g_163,(void*)0},{(void*)0,(void*)0,&g_304,&g_126,&g_163}},{{(void*)0,&g_163,(void*)0,&g_163,(void*)0},{&g_304,(void*)0,&g_126,&g_163,&g_163},{&g_163,(void*)0,(void*)0,&g_126,(void*)0},{&g_304,(void*)0,&g_304,&g_163,&g_304},{(void*)0,(void*)0,&g_126,&g_163,(void*)0}},{{(void*)0,(void*)0,&g_304,&g_126,&g_163},{(void*)0,&g_163,(void*)0,&g_163,(void*)0},{&g_304,(void*)0,&g_126,&g_163,&g_163},{&g_163,(void*)0,(void*)0,&g_126,(void*)0},{&g_304,(void*)0,&g_304,&g_163,&g_304}}};
                int32_t l_646 = 0xD993C284L;
                struct S1 *l_689 = &g_690;
                float l_701 = 0x1.Ep-1;
                int32_t *l_707 = &g_648;
                int32_t *l_708 = (void*)0;
                int32_t *l_709 = &g_199;
                int32_t *l_710 = &l_704;
                int32_t *l_711 = &l_646;
                int32_t *l_712 = &g_199;
                int32_t *l_713 = &l_704;
                int32_t *l_714 = &g_648;
                int32_t *l_715 = &g_648;
                int32_t *l_716[9] = {&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199};
                int i, j, k;
                if ((safe_div_func_uint8_t_u_u(0UL, ((*l_634) = g_471.f1))))
                { /* block id: 233 */
                    uint64_t *l_644 = &g_84[0];
                    int32_t *l_645 = &g_199;
                    struct S0 *l_661 = &g_205;
                    (*l_645) = (((*l_644) = (((safe_add_func_uint32_t_u_u((((l_637 , ((((void*)0 != l_638) > p_46) , (l_639[2][4][1] == l_640))) , &g_231) != (l_637 , ((safe_mul_func_int8_t_s_s((l_643 && g_189.f2), 0L)) , (void*)0))), p_45)) , g_161) != 0UL)) || 0xA23EA27E73A5EE64LL);
                    (*g_647) ^= ((*l_645) = l_646);
                    for (p_46 = (-1); (p_46 == 6); p_46 = safe_add_func_uint64_t_u_u(p_46, 9))
                    { /* block id: 240 */
                        int32_t l_658 = 0xD8C413AFL;
                        struct S0 **l_662[9];
                        int i;
                        for (i = 0; i < 9; i++)
                            l_662[i] = &l_661;
                        (*g_652) = l_651;
                        g_664 = (g_663 = (((p_46 , g_84[0]) < (safe_mul_func_int16_t_s_s((safe_mod_func_int8_t_s_s(g_328[1].f3, (((&g_84[0] != (void*)0) || ((l_658 ^ (l_659 == l_659)) && (g_172[4].f1 , p_46))) ^ 0x121CL))), p_42))) , l_661));
                    }
                    (*l_645) &= l_666;
                }
                else
                { /* block id: 246 */
                    const int16_t l_679 = 7L;
                    int32_t l_706 = 0x1131017DL;
                    for (g_188 = 9; (g_188 != 1); g_188 = safe_sub_func_uint8_t_u_u(g_188, 2))
                    { /* block id: 249 */
                        int32_t l_670[9] = {0L,0L,(-2L),0L,0L,(-2L),0L,0L,(-2L)};
                        struct S1 **l_687 = (void*)0;
                        struct S1 **l_688[3][3][7] = {{{&l_651,&l_651,&g_653[5],&l_651,&l_651,&g_653[5],&l_651},{&g_653[5],&g_653[7],&g_653[7],&g_653[5],&g_653[7],&g_653[7],&g_653[5]},{(void*)0,&l_651,(void*)0,(void*)0,&l_651,(void*)0,(void*)0}},{{&g_653[5],&g_653[5],(void*)0,&g_653[5],&g_653[5],(void*)0,&g_653[5]},{&l_651,(void*)0,(void*)0,&l_651,(void*)0,(void*)0,&l_651},{&g_653[7],&g_653[5],&g_653[7],&g_653[7],&g_653[7],(void*)0,(void*)0}},{{(void*)0,(void*)0,&l_651,(void*)0,(void*)0,&l_651,(void*)0},{&g_653[7],(void*)0,(void*)0,&g_653[7],(void*)0,(void*)0,&g_653[7]},{&g_653[5],(void*)0,&g_653[5],&g_653[5],(void*)0,&g_653[5],&g_653[5]}}};
                        int32_t ****l_694[2];
                        uint32_t *l_696 = &g_80[6][0][6];
                        int16_t *l_702 = (void*)0;
                        int16_t *l_703 = &g_116[1];
                        uint16_t *l_705 = &g_288;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_694[i] = (void*)0;
                        l_706 |= ((+l_670[4]) ^ (safe_lshift_func_uint16_t_u_s(((*l_705) |= ((safe_mod_func_int32_t_s_s((safe_rshift_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(((l_704 ^= ((((((*l_703) = ((l_679 & (((safe_add_func_uint16_t_u_u((--(*g_56)), (safe_div_func_int64_t_s_s((((l_689 = l_686[4][9][1]) != (void*)0) <= ((p_45 ^ ((safe_lshift_func_uint8_t_u_s(((!g_188) >= ((((((*l_696) &= ((l_695 = &g_591) != (l_638 = &g_591))) & ((safe_mul_func_float_f_f((safe_add_func_float_f_f(l_629, l_679)), 0xE.D36D77p+6)) , p_42)) , 0x52BCC1A8AB41C7C9LL) , 0xAAF1211FL) , p_45)), g_471.f1)) | g_205.f1)) != g_471.f2)), l_629)))) , 0x311018FF0D94ABAALL) | 0xE7E72BC185E062C1LL)) || g_231.f1)) > l_637) , (void*)0) != &g_198) < g_5[7])) >= (-8L)), g_145)), l_666)), p_42)) < g_145)), p_45)));
                    }
                    return p_46;
                }
                l_740--;
                (*l_715) &= p_42;
            }
        }
        else
        { /* block id: 265 */
            int8_t l_757[10] = {1L,0x94L,1L,0x94L,1L,0x94L,1L,0x94L,1L,0x94L};
            int32_t l_782 = 0x90484CC0L;
            int32_t l_784 = 1L;
            int32_t l_785 = 0x66F2D28FL;
            uint32_t l_786 = 4294967293UL;
            struct S1 **l_853 = &l_686[4][9][1];
            uint64_t **l_891 = &g_835[0];
            const int32_t *l_901 = &g_902;
            int i;
            for (l_739 = 0; (l_739 >= 0); l_739 -= 1)
            { /* block id: 268 */
                uint64_t l_747 = 3UL;
                int32_t **l_778 = (void*)0;
                int32_t l_780 = (-1L);
                int32_t l_781 = 5L;
                int32_t l_783 = 0x450EA0A0L;
                int32_t l_792 = 0xC91E2900L;
                int32_t l_793 = 0xC3D09D88L;
                int32_t l_796 = 1L;
                int32_t l_797[5];
                int16_t l_827 = 1L;
                uint64_t **l_892[9];
                int i;
                for (i = 0; i < 5; i++)
                    l_797[i] = 0xD392A122L;
                for (i = 0; i < 9; i++)
                    l_892[i] = &g_835[0];
                for (l_725 = 0; (l_725 >= 0); l_725 -= 1)
                { /* block id: 271 */
                    int32_t *l_743 = &g_199;
                    int32_t *l_744 = (void*)0;
                    int32_t *l_745 = (void*)0;
                    int32_t *l_746[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    l_747++;
                }
                if (g_84[l_739])
                { /* block id: 274 */
                    int32_t *l_750 = &l_721;
                    int64_t *l_777[10] = {&g_191,&g_191,&g_191,&g_191,&g_191,&g_191,&g_191,&g_191,&g_191,&g_191};
                    int32_t *l_779[4][2] = {{&l_719,&l_723},{&l_723,&l_719},{&l_723,&l_723},{&l_719,&l_723}};
                    int i, j;
                    (*l_750) ^= p_42;
                    for (l_724 = 0; (l_724 >= 0); l_724 -= 1)
                    { /* block id: 278 */
                        int i;
                        (*l_750) ^= (safe_rshift_func_uint16_t_u_u(((*g_56)--), 5));
                        if (g_84[l_724])
                            break;
                    }
                    if (((((0x9624L && (safe_lshift_func_int16_t_s_s((l_757[4] == g_690.f2), 10))) & (safe_add_func_int64_t_s_s((safe_rshift_func_int8_t_s_u((((safe_lshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(((safe_sub_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u(0x6FDEL, ((safe_mod_func_int64_t_s_s((g_91 = (((g_772 , ((p_42 == (((0L ^ p_43) || (safe_lshift_func_uint16_t_u_s((p_43 | 1L), 9))) && 0L)) >= g_80[6][0][6])) >= g_80[6][0][6]) & p_46)), g_489.f1)) && (*g_56)))) ^ p_43), p_46)) && 0xA9L), 6)), 5)) >= p_46) | g_138), g_164[1])), p_43))) >= (*g_56)) != p_46))
                    { /* block id: 284 */
                        l_778 = &g_263;
                    }
                    else
                    { /* block id: 286 */
                        return l_747;
                    }
                    --l_786;
                }
                else
                { /* block id: 290 */
                    int16_t l_790 = (-1L);
                    int32_t l_795[2][2];
                    uint8_t *l_863 = &l_629;
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_795[i][j] = 0xE7CBA2ADL;
                    }
                    if (l_786)
                    { /* block id: 291 */
                        int32_t *l_789 = (void*)0;
                        int32_t *l_791[8] = {&l_721,&l_784,&l_784,&l_721,&l_784,&l_784,&l_721,&l_784};
                        int16_t l_794 = 9L;
                        struct S1 **l_801 = (void*)0;
                        struct S1 **l_802 = &g_653[5];
                        struct S1 **l_803 = &l_686[4][9][1];
                        int32_t l_828 = 6L;
                        uint64_t *l_830[9][8] = {{&g_84[l_739],&l_747,&g_84[l_739],&g_84[l_739],&l_747,&g_188,&g_84[l_739],&g_84[0]},{&g_188,&g_84[0],&g_84[0],&g_84[0],&g_188,&g_188,&l_747,&g_188},{&g_84[0],&g_188,&l_747,&g_188,&g_84[0],&g_188,&g_84[0],&g_84[l_739]},{&g_84[l_739],&l_747,&g_84[l_739],&g_84[l_739],&g_84[0],&g_84[0],&l_747,&g_188},{&g_84[0],&g_84[0],&g_84[l_739],&g_84[0],&l_747,&g_84[0],&g_84[0],&g_84[l_739]},{&g_84[0],&l_747,&l_747,&g_84[l_739],&g_84[0],&g_188,&l_747,&g_84[l_739]},{&g_84[0],&g_84[l_739],&g_84[0],&g_84[l_739],&g_84[l_739],&g_84[0],&g_84[l_739],&g_84[0]},{&l_747,&g_188,&g_84[l_739],&g_188,&l_747,&g_84[l_739],&g_84[l_739],&g_84[0]},{&g_84[l_739],&g_84[0],&g_84[0],&g_84[0],&g_188,&g_84[l_739],&g_188,&g_188}};
                        int i, j;
                        ++l_798;
                        p_44 = ((((*l_803) = ((*l_802) = (*g_652))) == (((l_720 ^= ((safe_div_func_int64_t_s_s((l_781 , ((((safe_lshift_func_int8_t_s_u(((((0xF735L | ((safe_add_func_int32_t_s_s((safe_sub_func_uint16_t_u_u(l_782, (((safe_lshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((p_46 > 1L), (((-7L) | (!(g_471.f1 | (safe_sub_func_int64_t_s_s((safe_add_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((g_825 == &g_826[1]), l_795[0][1])), l_785)), p_42))))) , l_786))), 6)) , p_42) || l_827))), l_828)) <= 2L)) , l_723) && p_42) , p_46), 2)) , l_829) , 0xF0018695L) > p_43)), g_471.f2)) && g_57[0][1][2])) , l_831) , g_437)) > g_489.f1);
                        l_730[2][4] ^= ((((safe_unary_minus_func_uint64_t_u((&g_80[2][0][5] != &g_138))) > ((*g_825) != (void*)0)) <= g_188) , l_719);
                    }
                    else
                    { /* block id: 298 */
                        int32_t *l_836 = &l_724;
                        uint64_t l_843 = 0x50B3C5ADEED82D4ELL;
                        uint64_t *l_854[8][3][5] = {{{&l_747,&l_843,&g_84[0],&l_747,&l_747},{&l_747,(void*)0,&g_84[0],&g_84[0],&g_84[0]},{&l_747,&l_747,&g_188,&l_843,&l_747}},{{&g_188,&g_84[0],(void*)0,&l_747,&g_84[0]},{&g_84[0],&g_188,&l_747,&g_84[0],&l_843},{&l_843,&l_747,&g_84[0],(void*)0,&g_188}},{{&g_84[0],&g_84[0],&l_747,(void*)0,(void*)0},{&l_843,&l_747,&l_747,&l_843,&g_84[0]},{&g_84[0],&g_84[0],&g_84[0],&l_747,&g_84[0]}},{{&g_84[l_739],&g_84[0],&l_747,(void*)0,&g_188},{(void*)0,&g_84[l_739],&g_84[0],&g_84[l_739],(void*)0},{(void*)0,&g_188,&g_84[0],&l_747,(void*)0}},{{&g_84[0],&l_747,&g_84[0],&g_84[0],&g_84[0]},{&g_188,&l_747,&l_747,&g_188,(void*)0},{&l_843,&g_84[0],&l_747,&g_188,(void*)0}},{{(void*)0,&g_84[0],&l_843,&l_843,&g_188},{&g_84[0],&g_188,(void*)0,&l_747,&g_188},{(void*)0,&g_188,&g_84[0],&g_84[l_739],&g_84[l_739]}},{{&g_84[0],&g_84[0],&g_188,&l_747,&g_84[l_739]},{&l_747,&g_84[0],&g_84[l_739],&g_84[0],&l_843},{&l_747,&g_188,&g_84[l_739],&l_747,(void*)0}},{{&l_843,&g_188,&g_84[0],&g_84[l_739],&l_747},{&l_747,&g_84[0],&g_84[0],&l_747,(void*)0},{&g_84[0],&g_84[0],(void*)0,&g_84[l_739],&l_747}}};
                        int i, j, k;
                        if (g_833)
                            break;
                        (*l_836) = (g_834 != &g_835[0]);
                        l_784 = p_43;
                        l_797[4] = (l_795[1][0] = ((safe_div_func_int8_t_s_s(((safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((0x352FFE8FL == (g_172[4].f1 , (l_843 ^ l_795[0][1]))), (safe_add_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u(((p_43 = (safe_rshift_func_uint16_t_u_u((safe_sub_func_int16_t_s_s((+(&l_686[4][9][1] == l_853)), ((void*)0 != &g_116[2]))), (((*l_836) |= l_720) <= 1L)))) < 0x1480B57BE442A19CLL), 1L)) != 4294967295UL) != g_772.f2), (-1L))))), 5L)) & p_46), p_45)) , l_797[4]));
                    }
                    if ((*g_647))
                        continue;
                    (*g_865) &= ((((safe_rshift_func_uint16_t_u_u(((-10L) <= l_704), (0xEA60C3B5L || g_84[l_739]))) ^ g_80[6][0][6]) || ((((safe_rshift_func_uint8_t_u_u((0xB458L < ((1UL < ((*g_56) ^= (safe_mul_func_int8_t_s_s(0x1EL, ((*l_863) = (safe_div_func_uint32_t_u_u(l_785, l_795[0][0]))))))) && 1L)), 2)) , 0xFBL) ^ 0x43L) , p_42)) | l_795[0][1]);
                    for (g_191 = 0; (g_191 < 22); g_191 = safe_add_func_int16_t_s_s(g_191, 8))
                    { /* block id: 313 */
                        int32_t *l_868 = &l_728[2];
                        struct S1 ***l_882 = &l_853;
                        (*l_868) &= 0x4EBAB747L;
                        (*l_640) = (safe_sub_func_float_f_f(g_205.f3, ((((~((safe_mul_func_uint16_t_u_u(p_45, (safe_add_func_uint64_t_u_u(((((safe_rshift_func_uint8_t_u_u(((safe_unary_minus_func_int64_t_s(6L)) , l_643), 2)) >= (((g_653[5] == (void*)0) != (p_43 || l_784)) , (safe_sub_func_uint8_t_u_u((!g_5[6]), g_116[1])))) != p_42) , 8UL), (*l_868))))) , g_5[5])) , g_145) <= 0x1.82BEE7p-40) > 0x1.4p-1)));
                        (*l_882) = (g_168 , &g_653[3]);
                    }
                }
                if ((safe_lshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_u((l_780 = (((((&g_436[4] == (void*)0) ^ ((*g_56)--)) && (0xF3E206BDL ^ (l_640 == (void*)0))) , (18446744073709551612UL < ((((l_730[8][4] = (safe_mul_func_uint8_t_u_u(((l_891 != l_892[2]) , ((safe_sub_func_uint16_t_u_u((l_686[6][4][0] != l_895), g_3[2])) == (-7L))), p_45))) | 7L) > l_724) , g_133))) && 0xBF632CDBL)), 7)), p_46)))
                { /* block id: 322 */
                    const int32_t *l_899 = &g_900[2];
                    const int32_t **l_898[6][1] = {{&l_899},{&l_899},{&l_899},{&l_899},{&l_899},{&l_899}};
                    int32_t l_909 = 1L;
                    int i, j;
                    l_903 = ((!(g_897 , 0x87L)) , (l_901 = func_72(g_188)));
                    for (l_784 = 0; (l_784 >= 4); ++l_784)
                    { /* block id: 327 */
                        l_906 = p_43;
                        return (*g_865);
                    }
                    for (g_648 = 25; (g_648 != 5); --g_648)
                    { /* block id: 333 */
                        int32_t *l_910 = &l_785;
                        (**l_638) = (**l_638);
                        l_909 &= p_42;
                        (*l_910) |= 0x9603A223L;
                    }
                    for (l_792 = 9; (l_792 >= 1); l_792 -= 1)
                    { /* block id: 340 */
                        int32_t *l_911[2];
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_911[i] = &l_728[8];
                        l_785 ^= (l_783 &= (g_116[1] <= (l_730[(l_739 + 7)][l_792] &= 0x885F1788L)));
                        return p_42;
                    }
                }
                else
                { /* block id: 346 */
                    (*g_912) &= (l_723 ^= (*l_903));
                    if ((0x2A68L | (*l_903)))
                    { /* block id: 349 */
                        (*g_912) = p_46;
                        (*g_917) = ((*l_853) = (*g_652));
                    }
                    else
                    { /* block id: 353 */
                        return p_45;
                    }
                }
            }
        }
        for (p_43 = 0; (p_43 < 60); p_43 = safe_add_func_uint32_t_u_u(p_43, 4))
        { /* block id: 361 */
            float *l_932 = &l_717[0];
            (*g_921) = l_920;
            (*l_932) = (safe_mul_func_float_f_f((((p_46 < (p_45 >= ((0x4.4D6F5Fp+55 > 0xD.3BA295p-96) , 0x3.FE8DC9p+82))) <= g_116[2]) != g_80[6][0][6]), (safe_div_func_float_f_f((safe_mul_func_float_f_f((((safe_mul_func_float_f_f(((*l_640) = ((0xB.AF99CBp+94 <= g_897.f0) >= 0x1.A9166Ap-12)), 0x9.Ep+1)) > l_931) != 0x3.7F02BDp+50), (*l_903))), 0xA.06EE24p-12))));
        }
        l_739 |= ((((((void*)0 == &l_903) != (((*l_903) <= (l_640 == l_640)) | (((p_43 != (((((safe_div_func_int64_t_s_s(p_46, (+(safe_div_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u(0x6EL, 5)) , ((((p_45 = ((*l_941) ^= (((p_46 || 65531UL) > g_897.f2) , g_199))) < (-1L)) | (*g_56)) != 0x41L)), p_42))))) > (*l_903)) , g_471.f0) == g_59[1]) | 0x1A0B511E0683550DLL)) == 0UL) >= p_46))) && 0x81FD0B9FFBC778B7LL) || g_205.f2) != 0x6BA2L);
    }
    else
    { /* block id: 369 */
        uint8_t l_943 = 0xA3L;
        l_943 &= p_45;
    }
    (*g_957) = (l_944[1] > ((safe_div_func_int16_t_s_s(((((*l_956) = ((safe_add_func_uint8_t_u_u(((g_138++) || ((((*g_437) , (void*)0) == &l_686[4][9][1]) , ((((*l_954) = p_45) | (l_739 = p_45)) >= (0xEF22BD71117C5F20LL <= (p_45 || ((0xD2B6L > 0xB79DL) || 0L)))))), p_42)) == g_897.f1)) != 0x7EL) ^ l_725), p_45)) | p_43));
    --l_976;
    (*l_974) ^= (!((*g_437) , (safe_sub_func_int8_t_s_s((((*l_956) = (safe_sub_func_int64_t_s_s(0x5FBD0C67F47954D7LL, p_43))) , p_45), (*l_973)))));
    return p_42;
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_56 g_84 g_3 g_59 g_91 g_5 g_138 g_80 g_145 g_161 g_116 g_164 g_172 g_133 g_189 g_188 g_231 g_198 g_199 g_205.f2 g_262 g_268.f2 g_205.f1 g_288 g_328.f2 g_167 g_590 g_191
 * writes: g_80 g_84 g_91 g_138 g_161 g_164 g_163 g_126 g_167 g_168 g_191 g_188 g_198 g_199 g_189.f0 g_591 g_116
 */
static uint32_t  func_52(uint64_t  p_53, uint16_t * p_54, int32_t  p_55)
{ /* block id: 11 */
    uint32_t l_60 = 0xE60D2E65L;
    int32_t *l_76 = &g_59[1];
    int32_t **l_75 = &l_76;
    uint32_t *l_79 = &g_80[6][0][6];
    int32_t **l_264 = &g_198;
    float *l_305 = &g_163;
    int32_t l_308 = 5L;
    int32_t l_378 = 0xFD77E798L;
    int32_t l_387 = 0x36541A2CL;
    int32_t l_390 = 5L;
    int32_t l_393 = 0xDB3E03C6L;
    int32_t l_402 = 6L;
    int32_t l_403 = 0x69A33885L;
    int32_t l_405 = (-1L);
    int32_t l_406 = 0x2CCCB004L;
    const int8_t *l_434 = (void*)0;
    int8_t l_477 = 0x34L;
    uint64_t *l_492[7] = {&g_84[0],&g_84[0],&g_84[0],&g_84[0],&g_84[0],&g_84[0],&g_84[0]};
    uint64_t * const *l_491[6] = {&l_492[3],(void*)0,(void*)0,&l_492[3],(void*)0,(void*)0};
    struct S0 *l_508 = &g_205;
    int16_t l_594 = 0x6F8EL;
    int8_t l_600 = 0xA3L;
    uint32_t l_601 = 0x43E83B2EL;
    const int64_t *l_611 = (void*)0;
    int i;
    if (((g_57[1][2][4] , l_60) < (safe_sub_func_uint16_t_u_u(func_63(p_54, ((*l_264) = func_66(&g_3[1], (*g_56), ((*l_75) = func_72(p_53)), &g_57[0][1][2], ((*l_79) = (safe_div_func_int32_t_s_s((0x9EEBL ^ l_60), p_55)))))), g_57[0][1][2]))))
    { /* block id: 94 */
        int32_t *l_297 = (void*)0;
        const int32_t l_301 = 4L;
        const int32_t *l_303 = &g_59[1];
        const int32_t * const *l_302 = &l_303;
        int32_t l_306 = 0xB464E269L;
        uint8_t l_307[9][6][4];
        int i, j, k;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 6; j++)
            {
                for (k = 0; k < 4; k++)
                    l_307[i][j][k] = 0x94L;
            }
        }
        (*l_264) = func_66(func_72(p_55), (l_306 = (safe_div_func_int32_t_s_s(((safe_unary_minus_func_uint32_t_u(((*l_79) = (l_297 == (void*)0)))) ^ ((g_268[3][0][4].f2 , ((l_305 = (((safe_sub_func_int8_t_s_s((((((+l_301) | ((void*)0 == l_302)) , ((0xA1CFE7ECL & 0x680E6147L) , 0xDDL)) > g_205.f1) >= (**l_264)), 255UL)) == (**l_264)) , l_297)) != l_303)) != 0x1FL)), (*g_198)))), l_297, &g_288, l_307[3][0][0]);
    }
    else
    { /* block id: 99 */
        uint64_t l_309 = 6UL;
        int32_t l_366 = 0x108F6788L;
        int32_t l_386 = (-1L);
        int32_t l_391 = (-6L);
        int32_t l_394[4];
        uint32_t * const l_452 = (void*)0;
        int16_t l_457 = 0L;
        const struct S0 *l_507 = &g_328[1];
        float l_549 = 0x2.Cp+1;
        const struct S1 *l_576 = &g_471;
        int32_t **l_589 = &g_198;
        int32_t l_624[6] = {5L,4L,4L,5L,4L,4L};
        int i;
        for (i = 0; i < 4; i++)
            l_394[i] = (-8L);
        l_309++;
        if (g_188)
            goto lbl_556;
lbl_556:
        for (g_91 = (-12); (g_91 == 25); g_91 = safe_add_func_int32_t_s_s(g_91, 1))
        { /* block id: 103 */
            int32_t l_320 = 0x2A1A66ECL;
            uint64_t l_329 = 3UL;
            int8_t *l_337 = &g_5[5];
            float *l_369[4] = {&g_163,&g_163,&g_163,&g_163};
            int32_t l_376 = 0L;
            int32_t l_377 = 1L;
            int32_t l_380 = 0x1C30D01DL;
            int32_t l_381 = 0L;
            int32_t l_382 = 0xDC7873C4L;
            int32_t l_383 = 1L;
            int32_t l_384 = 0xAE2566B4L;
            int32_t l_388 = (-1L);
            int32_t l_389 = (-9L);
            int32_t l_392 = (-1L);
            int16_t l_396 = 0x3855L;
            int32_t l_397 = 0x136B5557L;
            int32_t l_398 = (-6L);
            int32_t l_407 = 0x259BF9DEL;
            uint32_t l_441 = 0xBA805199L;
            int32_t *l_459 = &l_366;
            int32_t *l_460[9][3][9] = {{{&l_398,&l_402,&g_3[2],&l_393,&l_394[1],&l_380,(void*)0,&l_394[1],(void*)0},{&l_382,&l_383,&l_308,(void*)0,&l_390,&l_382,&l_377,&l_398,&l_393},{&l_377,&l_394[1],&l_377,&l_397,&l_384,&l_393,&l_377,&l_403,&l_381}},{{(void*)0,&l_394[1],&l_388,&l_382,&l_381,&l_394[1],&l_308,(void*)0,&l_407},{&l_308,&l_393,(void*)0,&l_397,&g_3[1],&l_394[1],&g_3[1],&l_397,(void*)0},{&l_382,&l_382,&l_398,(void*)0,&l_407,&l_383,&l_394[1],&l_392,&l_382}},{{&l_384,&l_380,&g_199,&l_393,&l_382,&l_402,&l_380,&l_403,&l_394[1]},{(void*)0,&l_380,&l_398,&l_377,(void*)0,&l_308,&l_392,&l_390,&g_199},{&g_3[2],&l_383,(void*)0,&l_406,&g_3[1],&l_397,&l_398,&l_394[1],&l_308}},{{&l_394[1],&l_390,&l_388,&l_398,&l_394[1],&l_380,&l_394[1],&l_377,&l_377},{&l_382,&l_378,&g_199,&l_394[1],&l_377,&l_394[1],&g_199,&l_378,&l_380},{&g_199,&l_378,&l_388,&l_383,&l_390,&l_308,&l_392,&l_407,&l_390}},{{(void*)0,(void*)0,&l_389,&l_402,(void*)0,&l_383,&g_3[1],&l_398,(void*)0},{&g_199,&l_390,&l_392,&l_308,(void*)0,&l_377,&l_398,&l_380,(void*)0},{&l_380,&l_380,&l_381,&l_397,&l_382,&l_398,&l_394[1],&l_394[1],(void*)0}},{{&l_383,(void*)0,&l_381,&l_380,&l_390,&l_308,&l_394[1],(void*)0,(void*)0},{&l_389,&l_394[1],&l_308,&l_398,&l_308,&l_394[1],&l_389,&l_406,(void*)0},{&l_394[1],(void*)0,&l_398,&l_378,&l_383,&l_393,&l_381,&l_378,&l_390}},{{&l_394[1],&l_398,&l_382,&l_397,&l_381,&l_380,&l_380,&l_406,&l_380},{&l_390,&l_392,&l_308,(void*)0,&l_377,&l_398,&l_380,(void*)0,&l_382},{&g_3[1],&l_383,(void*)0,&l_402,&l_389,(void*)0,(void*)0,&l_394[1],&l_386}},{{&l_377,&l_407,&l_398,&l_381,&l_381,&l_380,&l_380,&l_380,&l_380},{&g_199,&l_394[1],&l_377,&l_394[1],&g_199,&l_378,&l_380,&l_398,&l_384},{&l_393,&l_308,&l_381,&l_382,(void*)0,&l_394[1],&l_381,&l_407,&l_393}},{{&l_308,&l_383,(void*)0,&l_393,&l_390,&l_378,&l_389,&l_378,&l_390},{&l_382,&l_377,&l_392,&l_377,&l_382,&l_380,&l_394[1],&l_398,&l_381},{(void*)0,&l_398,&l_394[1],&l_380,&l_382,(void*)0,&l_394[1],&l_397,&l_377}}};
            uint64_t l_479 = 0x63F59EA7C12BB6B1LL;
            int16_t l_484 = 0x240EL;
            uint32_t l_552 = 4294967295UL;
            int64_t l_555[2];
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_555[i] = 7L;
        }
        for (l_457 = 0; (l_457 <= 2); l_457 += 1)
        { /* block id: 187 */
            uint64_t l_567 = 4UL;
            int32_t l_587[3];
            int8_t l_597 = (-1L);
            int32_t l_599 = 0xB3C4DF7FL;
            int64_t *l_612 = &g_191;
            int i;
            for (i = 0; i < 3; i++)
                l_587[i] = 0x0714EDF5L;
            for (g_188 = 0; (g_188 <= 3); g_188 += 1)
            { /* block id: 190 */
                float l_565 = 0x0.Fp+1;
                int32_t l_566 = 0xBBEFABC2L;
                int8_t l_585 = (-1L);
                struct S1 *l_586 = (void*)0;
                int32_t *l_588 = &l_390;
                int32_t l_595[8][3] = {{0L,0L,0xF441D224L},{1L,0xF441D224L,0xF441D224L},{0xF441D224L,0x792FBA3CL,0xD5B1AC65L},{1L,0x792FBA3CL,1L},{0L,0xF441D224L,0xD5B1AC65L},{0L,0L,0xF441D224L},{1L,0xF441D224L,0xF441D224L},{0xF441D224L,0x792FBA3CL,0xD5B1AC65L}};
                int16_t l_598[2][9][7] = {{{0x113BL,(-1L),0x113BL,0xF372L,0x5A11L,0x113BL,9L},{0x5A11L,(-1L),(-9L),0x2478L,(-1L),0x8DFEL,(-1L)},{0L,0x1AABL,0x1AABL,0x9879L,0x4D46L,(-9L),0x113BL},{0x8DFEL,0xF372L,0x1AABL,0L,0x113BL,0x8DFEL,0x8DFEL},{0xF372L,0x113BL,(-1L),0x113BL,0xF372L,0x5A11L,0x113BL},{0x9879L,0x4D46L,(-9L),0x113BL,0x1AABL,(-9L),0x9173L},{0x1AABL,0x9173L,0L,0L,0x9173L,0x1AABL,0x4D46L},{0x9879L,0x113BL,0L,0x9879L,0x9173L,0x2478L,0x113BL},{0xF372L,0x8DFEL,0x1AABL,9L,0x1AABL,0x8DFEL,0xF372L}},{{0x8DFEL,0x113BL,0L,0x1AABL,0xF372L,0x8DFEL,0x1AABL},{0x9879L,0x9173L,0x2478L,0x113BL,0x113BL,0x2478L,0x9173L},{0x113BL,0x4D46L,0L,9L,0x4D46L,0x1AABL,0x9173L},{(-9L),0x113BL,0x1AABL,(-9L),0x9173L,(-9L),0x1AABL},{0xF372L,0xF372L,0L,9L,0x113BL,0x5A11L,0xF372L},{0xF372L,0x1AABL,0L,0x113BL,0x8DFEL,0x8DFEL,0x113BL},{(-9L),0x9173L,(-9L),0x1AABL,0x113BL,(-9L),0x4D46L},{0x113BL,0x9173L,(-1L),9L,0x9173L,0L,0x9173L},{0x9879L,0x1AABL,0x1AABL,0x9879L,0x4D46L,(-9L),0x113BL}}};
                int8_t *l_613 = &l_585;
                uint32_t l_625 = 0x04127BAAL;
                int i, j, k;
                for (g_91 = 0; (g_91 <= 2); g_91 += 1)
                { /* block id: 193 */
                    int32_t *l_557 = &l_394[3];
                    int32_t *l_558 = &l_366;
                    int32_t *l_559 = &l_390;
                    int32_t l_560 = 0x1975AB1FL;
                    int32_t *l_561 = &l_394[1];
                    int32_t *l_562 = &l_394[1];
                    int32_t *l_563 = &l_391;
                    int32_t *l_564[8] = {(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1]};
                    int i, j, k;
                    --l_567;
                    if (g_57[l_457][(g_188 + 5)][(l_457 + 1)])
                        break;
                    for (l_60 = 0; (l_60 <= 9); l_60 += 1)
                    { /* block id: 198 */
                        int i, j, k;
                        l_587[2] = ((safe_sub_func_int32_t_s_s((g_57[g_91][l_60][(l_457 + 4)] >= g_116[(g_91 + 6)]), (safe_mod_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((0x2167L || (((((l_576 == ((safe_add_func_int32_t_s_s(((-1L) && ((l_386 || (g_328[1].f2 , ((safe_sub_func_uint32_t_u_u(((safe_add_func_uint8_t_u_u(0x84L, ((safe_lshift_func_uint8_t_u_s((((1L <= p_53) , 0x451FL) >= l_585), p_53)) | 0x97L))) | 5UL), (**l_264))) < 0xFBEB11C4L))) < l_566)), 0L)) , l_586)) > g_167) > (*g_56)) == 0xAC16F4C2L) || (*g_56))), (*p_54))) == (*p_54)), p_53)))) <= p_53);
                    }
                }
                for (l_309 = 2; (l_309 <= 9); l_309 += 1)
                { /* block id: 204 */
                    int32_t *l_592 = &l_378;
                    int32_t *l_593[10][9] = {{(void*)0,(void*)0,&l_405,&l_405,(void*)0,(void*)0,&l_566,(void*)0,&l_378},{&l_394[2],&l_394[1],&l_386,&l_394[1],&g_199,(void*)0,&l_366,&l_394[1],&l_393},{&l_378,&l_405,&l_405,&l_566,&l_587[2],&l_587[2],&l_566,&l_405,&l_405},{&g_199,&l_394[1],&g_3[0],(void*)0,&l_394[1],&l_308,&l_387,&g_3[1],&l_393},{&l_406,&l_386,&l_406,(void*)0,&l_566,&l_378,(void*)0,(void*)0,&l_378},{&g_3[0],&l_394[1],&g_199,&l_394[1],&g_3[0],(void*)0,&l_394[1],&l_308,&l_387},{&l_405,&l_405,&l_378,(void*)0,&l_378,&l_405,&l_405,&l_566,&l_587[2]},{&l_386,&l_394[1],&l_394[2],(void*)0,&l_587[0],(void*)0,&l_394[2],&l_394[1],&g_199},{(void*)0,&l_405,&l_406,&l_587[2],(void*)0,(void*)0,(void*)0,&l_587[2],&l_406},{&l_387,&l_308,&l_394[1],(void*)0,&g_3[0],&l_394[1],&g_199,&l_394[1],&g_3[0]}};
                    int16_t l_596[6][1][5];
                    int i, j, k;
                    for (i = 0; i < 6; i++)
                    {
                        for (j = 0; j < 1; j++)
                        {
                            for (k = 0; k < 5; k++)
                                l_596[i][j][k] = 0xA480L;
                        }
                    }
                    l_588 = &l_587[2];
                    (*g_590) = l_589;
                    --l_601;
                }
                (**l_264) = (safe_rshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((safe_mod_func_int16_t_s_s((safe_unary_minus_func_int8_t_s(((*l_613) = (l_611 != l_612)))), (g_116[(g_188 + 5)] = (safe_lshift_func_uint16_t_u_s(0x89E7L, 5))))), 0xBC5A95D8L)), (safe_add_func_uint8_t_u_u(p_55, 0x27L))));
                for (l_386 = 0; (l_386 <= 19); l_386 = safe_add_func_int8_t_s_s(l_386, 2))
                { /* block id: 214 */
                    int32_t *l_620 = &l_366;
                    int32_t *l_621 = (void*)0;
                    int32_t *l_622[6][10] = {{&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1]},{(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0},{&l_394[1],&l_394[1],(void*)0,&l_394[1],&l_394[1],(void*)0,&l_394[1],&l_394[1],(void*)0,&l_394[1]},{&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1]},{(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0,&l_394[1],(void*)0,(void*)0},{&l_394[1],&l_394[1],(void*)0,&l_394[1],&l_394[1],(void*)0,&l_394[1],&l_394[1],(void*)0,&l_394[1]}};
                    int8_t l_623 = 0L;
                    int i, j;
                    l_625--;
                }
            }
            for (g_191 = 2; (g_191 >= 0); g_191 -= 1)
            { /* block id: 220 */
                return l_587[0];
            }
        }
    }
    return p_55;
}


/* ------------------------------------------ */
/* 
 * reads : g_56 g_57
 * writes: g_198
 */
static const uint16_t  func_63(uint16_t * p_64, int32_t * p_65)
{ /* block id: 89 */
    int32_t * const l_265 = &g_199;
    uint64_t *l_272 = &g_188;
    uint16_t *l_287 = &g_288;
    int32_t **l_293 = &g_198;
    (*l_293) = l_265;
    return (*g_56);
}


/* ------------------------------------------ */
/* 
 * reads : g_84 g_3 g_59 g_57 g_91 g_5 g_138 g_80 g_145 g_161 g_116 g_164 g_172 g_133 g_56 g_189 g_188 g_231 g_198 g_199 g_205.f2 g_262 g_288
 * writes: g_84 g_91 g_138 g_161 g_164 g_163 g_126 g_167 g_168 g_191 g_188 g_198 g_199 g_189.f0
 */
static int32_t * func_66(int32_t * const  p_67, const uint16_t  p_68, int32_t * p_69, uint16_t * p_70, uint32_t  p_71)
{ /* block id: 17 */
    uint64_t *l_83[3][2];
    int32_t l_85 = 0x49DA2B61L;
    int32_t l_86 = 0L;
    int32_t l_87 = 0xFF831C21L;
    int32_t *l_90 = (void*)0;
    struct S0 *l_204 = &g_205;
    int64_t l_232 = 0x42BA089590904DF5LL;
    uint8_t l_261 = 255UL;
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
            l_83[i][j] = &g_84[0];
    }
    g_91 = ((++g_84[0]) >= ((void*)0 == l_83[0][0]));
    if ((&g_80[0][0][0] != &p_71))
    { /* block id: 20 */
        int32_t *l_92 = &l_85;
        uint16_t *l_105 = &g_57[2][0][3];
        int32_t l_121 = 0x80B265A9L;
        uint32_t l_162 = 0xDAD61334L;
        int32_t *l_165 = &g_59[1];
        const uint64_t *l_187 = &g_188;
        const uint64_t **l_186[9] = {&l_187,(void*)0,(void*)0,&l_187,(void*)0,(void*)0,&l_187,(void*)0,(void*)0};
        int i;
        (*l_92) ^= g_3[1];
        if (((safe_mod_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u((((p_68 , ((safe_div_func_float_f_f(g_59[1], ((((*l_92) &= g_59[1]) ^ (0xEAL ^ (safe_rshift_func_int16_t_s_u((safe_add_func_uint16_t_u_u((*p_70), (((safe_div_func_int16_t_s_s(((((l_105 == &g_57[1][4][6]) , p_71) , ((safe_mul_func_int8_t_s_s(((g_3[1] || p_68) >= 0x92L), 0L)) , &g_84[0])) != (void*)0), (*p_70))) , 1UL) != p_71))), 2)))) , (-0x1.9p-1)))) , 0xA278L)) ^ p_68) ^ (*p_67)), 0)), 0x0DL)) && g_59[1]))
        { /* block id: 23 */
            uint16_t l_112 = 65527UL;
            const int32_t * const l_114 = &g_59[0];
            int32_t l_120 = 1L;
            int32_t l_137 = (-6L);
            float *l_190 = &g_163;
            for (g_91 = 0; (g_91 <= 1); g_91 += 1)
            { /* block id: 26 */
                uint64_t **l_108 = &l_83[0][1];
                uint64_t *l_113 = &g_84[0];
                int16_t *l_115[8] = {&g_116[1],&g_116[1],&g_116[1],&g_116[1],&g_116[1],&g_116[1],&g_116[1],&g_116[1]};
                int32_t l_117 = 0x440229A8L;
                int32_t l_118[6];
                uint64_t l_169 = 3UL;
                int i;
                for (i = 0; i < 6; i++)
                    l_118[i] = (-6L);
                l_118[0] = ((((*l_92) = (((*l_108) = l_83[0][1]) == ((g_3[1] & ((((+(((*l_113) |= ((safe_mul_func_uint8_t_u_u(g_3[4], 0x06L)) || l_112)) , ((void*)0 == l_114))) && ((l_117 = p_68) < (l_112 || g_3[4]))) & 1L) || g_5[5])) , &g_84[0]))) && g_3[1]) , (-1L));
                for (l_87 = 7; (l_87 >= 0); l_87 -= 1)
                { /* block id: 34 */
                    int64_t l_119[6][1][5] = {{{0x27C9044C330A9AF7LL,0x27C9044C330A9AF7LL,(-1L),0x27C9044C330A9AF7LL,0x27C9044C330A9AF7LL}},{{0xF039857A524CF131LL,4L,0xF039857A524CF131LL,0xF039857A524CF131LL,4L}},{{0x27C9044C330A9AF7LL,0x4C7A7D0CF22777C7LL,0x4C7A7D0CF22777C7LL,0x27C9044C330A9AF7LL,0x4C7A7D0CF22777C7LL}},{{4L,4L,0x07E14A349480F4B2LL,4L,4L}},{{0x4C7A7D0CF22777C7LL,0x27C9044C330A9AF7LL,0x4C7A7D0CF22777C7LL,0x4C7A7D0CF22777C7LL,0x27C9044C330A9AF7LL}},{{4L,0xF039857A524CF131LL,0xF039857A524CF131LL,4L,0xF039857A524CF131LL}}};
                    int32_t *l_122 = (void*)0;
                    int32_t *l_123 = &l_86;
                    int32_t *l_124 = &l_85;
                    int32_t *l_125 = &l_121;
                    int32_t *l_127 = &l_120;
                    int32_t *l_128 = &l_118[0];
                    int32_t *l_129 = &l_85;
                    int32_t *l_130 = (void*)0;
                    int32_t *l_131 = &l_118[0];
                    int32_t *l_132 = &l_118[0];
                    int32_t *l_134 = &l_117;
                    int32_t *l_135 = &l_85;
                    int32_t *l_136[4] = {&l_118[0],&l_118[0],&l_118[0],&l_118[0]};
                    int i, j, k;
                    ++g_138;
                    return &g_3[4];
                }
                if (g_80[0][0][4])
                    break;
                for (l_120 = 0; (l_120 <= 1); l_120 += 1)
                { /* block id: 41 */
                    uint8_t *l_160 = &g_161;
                    float *l_166[9] = {&g_126,&g_163,&g_126,&g_163,&g_126,&g_163,&g_126,&g_163,&g_126};
                    int i, j;
                    if (g_80[6][0][6])
                        break;
                    l_169 = (g_168 = ((*l_92) > (g_167 = (g_126 = (g_163 = (l_118[1] >= (safe_div_func_float_f_f((safe_div_func_float_f_f(g_145, (((((g_164[1] ^= ((safe_lshift_func_uint16_t_u_s((((safe_mod_func_uint8_t_u_u(0xA2L, (safe_mod_func_uint64_t_u_u((((g_3[1] > (((*l_160) &= (safe_mod_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s(l_118[0], ((safe_add_func_int8_t_s_s(((((l_83[(l_120 + 1)][g_91] = &g_84[0]) != (((g_145 >= (g_80[6][0][6] , g_145)) <= l_120) , (void*)0)) & (*p_70)) >= g_57[1][2][0]), l_120)) <= p_71))), 247UL)), g_59[0]))) | g_5[6])) | 0xFDL) < p_68), 0xB5EF69CA31E374D1LL)))) <= (*p_70)) | l_162), g_116[1])) || p_71)) , (void*)0) != l_165) <= l_112) < 0x0.E5BB39p+3))), g_59[1]))))))));
                }
            }
            g_191 = ((safe_mul_func_float_f_f(((*l_92) > (l_90 == (void*)0)), ((g_172[4] , (safe_sub_func_float_f_f(p_71, ((*l_190) = ((safe_sub_func_float_f_f(0x7.558B3Ep-42, (safe_add_func_float_f_f((g_126 = (((safe_unary_minus_func_uint32_t_u((safe_div_func_uint8_t_u_u(0x16L, ((safe_rshift_func_uint8_t_u_s((((((0x6F017B85L > ((((++g_84[0]) != (l_186[4] == (void*)0)) || g_133) , (*l_92))) < (*g_56)) & 1L) , (*p_70)) || (*l_92)), 2)) , (-10L)))))) , g_189) , 0x9.DD1BAEp-41)), g_164[1])))) != (-0x1.Ap+1)))))) < p_71))) > g_57[0][1][2]);
        }
        else
        { /* block id: 57 */
            int32_t **l_192 = &l_92;
            (*l_192) = &g_3[1];
            for (g_188 = (-4); (g_188 >= 34); ++g_188)
            { /* block id: 61 */
                for (l_87 = (-2); (l_87 >= (-6)); --l_87)
                { /* block id: 64 */
                    int32_t *l_197 = &l_85;
                    (*l_192) = l_197;
                    g_198 = (*l_192);
                }
                if ((*l_92))
                    break;
            }
        }
    }
    else
    { /* block id: 71 */
        float *l_200[9][5] = {{&g_126,&g_126,&g_126,&g_126,&g_126},{&g_126,&g_126,&g_163,&g_126,&g_126},{&g_126,&g_126,&g_126,&g_126,&g_126},{&g_126,&g_126,&g_126,&g_126,&g_126},{&g_126,&g_126,(void*)0,&g_126,&g_126},{&g_126,&g_126,&g_126,&g_126,&g_126},{&g_126,&g_126,&g_126,&g_126,&g_126},{&g_126,&g_126,&g_163,&g_126,&g_126},{&g_126,&g_126,&g_126,&g_126,&g_126}};
        struct S0 *l_203 = (void*)0;
        uint64_t *l_226[1][8];
        int32_t l_241 = 0xBFE3058EL;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 8; j++)
                l_226[i][j] = (void*)0;
        }
        g_126 = (g_188 >= p_71);
        for (g_138 = (-2); (g_138 < 49); ++g_138)
        { /* block id: 75 */
            uint64_t **l_224 = &l_83[0][0];
            uint64_t **l_225[1];
            int32_t l_240 = 0xB29BC1B9L;
            int32_t *l_242 = &l_241;
            int i;
            for (i = 0; i < 1; i++)
                l_225[i] = (void*)0;
            l_204 = l_203;
            g_126 = 0x4.715A48p-84;
            g_189.f0 = (((&g_80[6][0][6] == ((safe_sub_func_uint8_t_u_u((((safe_mod_func_int64_t_s_s(1L, (l_240 = (safe_rshift_func_uint16_t_u_s((safe_add_func_int32_t_s_s(0xDD480647L, (safe_div_func_int64_t_s_s((((safe_mod_func_int8_t_s_s(((((safe_sub_func_uint8_t_u_u(0x68L, (((safe_div_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(((((l_226[0][3] = ((*l_224) = l_83[1][0])) != (void*)0) & (safe_mod_func_uint32_t_u_u((safe_mod_func_int32_t_s_s(((*l_242) = (((g_231 , (l_232 && ((((*g_198) = (safe_mul_func_uint8_t_u_u(((((safe_mod_func_int32_t_s_s((~(safe_lshift_func_int8_t_s_s((((g_145 ^ g_84[0]) , (*g_198)) , 0L), l_240))), l_241)) > p_71) , g_231.f3) , g_84[0]), p_71))) > l_240) & p_71))) != (*p_67)) != 0UL)), 3UL)), (*p_67)))) , (*p_70)), 5UL)), g_59[0])) >= l_240) > p_71))) , p_68) , (*g_56)) > p_68), 0x56L)) & p_71) , 0x12EE7D574BCF8604LL), g_91)))), 0))))) , (-9L)) || g_80[6][0][6]), p_68)) , (void*)0)) && p_68) , (*p_67));
        }
    }
    (*g_198) ^= (safe_mod_func_uint64_t_u_u(((((safe_sub_func_uint16_t_u_u(((~((!((void*)0 == &g_3[1])) <= (&p_67 != &l_90))) && (safe_mod_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u(((0xDB27L ^ (safe_div_func_uint32_t_u_u((g_205.f2 || ((p_71 || l_261) <= 0x10L)), (*p_67)))) & g_91), 0xACL)), p_71)), 11)), p_71)), p_68))), 5L)) == 0xC7L) , g_262) != &p_69), 3UL));
    return p_69;
}


/* ------------------------------------------ */
/* 
 * reads : g_57
 * writes:
 */
static int32_t * func_72(int32_t  p_73)
{ /* block id: 12 */
    int16_t l_74 = (-5L);
    l_74 = g_57[2][3][1];
    return &g_59[1];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_3[i], "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_5[i], "g_5[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_34, "g_34", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_57[i][j][k], "g_57[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_59[i], "g_59[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_80[i][j][k], "g_80[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_84[i], "g_84[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_91, "g_91", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_116[i], "g_116[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_126, sizeof(g_126), "g_126", print_hash_value);
    transparent_crc(g_133, "g_133", print_hash_value);
    transparent_crc(g_138, "g_138", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    transparent_crc(g_161, "g_161", print_hash_value);
    transparent_crc_bytes (&g_163, sizeof(g_163), "g_163", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_164[i], "g_164[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_167, "g_167", print_hash_value);
    transparent_crc(g_168, "g_168", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_172[i].f0, "g_172[i].f0", print_hash_value);
        transparent_crc(g_172[i].f1, "g_172[i].f1", print_hash_value);
        transparent_crc(g_172[i].f2, "g_172[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_188, "g_188", print_hash_value);
    transparent_crc(g_189.f0, "g_189.f0", print_hash_value);
    transparent_crc(g_189.f1, "g_189.f1", print_hash_value);
    transparent_crc(g_189.f2, "g_189.f2", print_hash_value);
    transparent_crc(g_191, "g_191", print_hash_value);
    transparent_crc(g_199, "g_199", print_hash_value);
    transparent_crc_bytes (&g_205.f0, sizeof(g_205.f0), "g_205.f0", print_hash_value);
    transparent_crc(g_205.f1, "g_205.f1", print_hash_value);
    transparent_crc(g_205.f2, "g_205.f2", print_hash_value);
    transparent_crc(g_205.f3, "g_205.f3", print_hash_value);
    transparent_crc_bytes (&g_231.f0, sizeof(g_231.f0), "g_231.f0", print_hash_value);
    transparent_crc(g_231.f1, "g_231.f1", print_hash_value);
    transparent_crc(g_231.f2, "g_231.f2", print_hash_value);
    transparent_crc(g_231.f3, "g_231.f3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_268[i][j][k].f0, "g_268[i][j][k].f0", print_hash_value);
                transparent_crc(g_268[i][j][k].f1, "g_268[i][j][k].f1", print_hash_value);
                transparent_crc(g_268[i][j][k].f2, "g_268[i][j][k].f2", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_288, "g_288", print_hash_value);
    transparent_crc_bytes (&g_304, sizeof(g_304), "g_304", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc_bytes(&g_328[i].f0, sizeof(g_328[i].f0), "g_328[i].f0", print_hash_value);
        transparent_crc(g_328[i].f1, "g_328[i].f1", print_hash_value);
        transparent_crc(g_328[i].f2, "g_328[i].f2", print_hash_value);
        transparent_crc(g_328[i].f3, "g_328[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_364, "g_364", print_hash_value);
    transparent_crc(g_471.f0, "g_471.f0", print_hash_value);
    transparent_crc(g_471.f1, "g_471.f1", print_hash_value);
    transparent_crc(g_471.f2, "g_471.f2", print_hash_value);
    transparent_crc(g_489.f0, "g_489.f0", print_hash_value);
    transparent_crc(g_489.f1, "g_489.f1", print_hash_value);
    transparent_crc(g_489.f2, "g_489.f2", print_hash_value);
    transparent_crc_bytes (&g_515.f0, sizeof(g_515.f0), "g_515.f0", print_hash_value);
    transparent_crc(g_515.f1, "g_515.f1", print_hash_value);
    transparent_crc(g_515.f2, "g_515.f2", print_hash_value);
    transparent_crc(g_515.f3, "g_515.f3", print_hash_value);
    transparent_crc(g_648, "g_648", print_hash_value);
    transparent_crc(g_660, "g_660", print_hash_value);
    transparent_crc_bytes (&g_665.f0, sizeof(g_665.f0), "g_665.f0", print_hash_value);
    transparent_crc(g_665.f1, "g_665.f1", print_hash_value);
    transparent_crc(g_665.f2, "g_665.f2", print_hash_value);
    transparent_crc(g_665.f3, "g_665.f3", print_hash_value);
    transparent_crc(g_690.f0, "g_690.f0", print_hash_value);
    transparent_crc(g_690.f1, "g_690.f1", print_hash_value);
    transparent_crc(g_690.f2, "g_690.f2", print_hash_value);
    transparent_crc(g_772.f0, "g_772.f0", print_hash_value);
    transparent_crc(g_772.f1, "g_772.f1", print_hash_value);
    transparent_crc(g_772.f2, "g_772.f2", print_hash_value);
    transparent_crc(g_833, "g_833", print_hash_value);
    transparent_crc(g_897.f0, "g_897.f0", print_hash_value);
    transparent_crc(g_897.f1, "g_897.f1", print_hash_value);
    transparent_crc(g_897.f2, "g_897.f2", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_900[i], "g_900[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_902, "g_902", print_hash_value);
    transparent_crc_bytes (&g_991.f0, sizeof(g_991.f0), "g_991.f0", print_hash_value);
    transparent_crc(g_991.f1, "g_991.f1", print_hash_value);
    transparent_crc(g_991.f2, "g_991.f2", print_hash_value);
    transparent_crc(g_991.f3, "g_991.f3", print_hash_value);
    transparent_crc_bytes (&g_1028, sizeof(g_1028), "g_1028", print_hash_value);
    transparent_crc_bytes (&g_1137.f0, sizeof(g_1137.f0), "g_1137.f0", print_hash_value);
    transparent_crc(g_1137.f1, "g_1137.f1", print_hash_value);
    transparent_crc(g_1137.f2, "g_1137.f2", print_hash_value);
    transparent_crc(g_1137.f3, "g_1137.f3", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_1277[i].f0, sizeof(g_1277[i].f0), "g_1277[i].f0", print_hash_value);
        transparent_crc(g_1277[i].f1, "g_1277[i].f1", print_hash_value);
        transparent_crc(g_1277[i].f2, "g_1277[i].f2", print_hash_value);
        transparent_crc(g_1277[i].f3, "g_1277[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1344, "g_1344", print_hash_value);
    transparent_crc_bytes (&g_1382, sizeof(g_1382), "g_1382", print_hash_value);
    transparent_crc(g_1394, "g_1394", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_1408[i].f0, sizeof(g_1408[i].f0), "g_1408[i].f0", print_hash_value);
        transparent_crc(g_1408[i].f1, "g_1408[i].f1", print_hash_value);
        transparent_crc(g_1408[i].f2, "g_1408[i].f2", print_hash_value);
        transparent_crc(g_1408[i].f3, "g_1408[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1449.f0, sizeof(g_1449.f0), "g_1449.f0", print_hash_value);
    transparent_crc(g_1449.f1, "g_1449.f1", print_hash_value);
    transparent_crc(g_1449.f2, "g_1449.f2", print_hash_value);
    transparent_crc(g_1449.f3, "g_1449.f3", print_hash_value);
    transparent_crc(g_1478, "g_1478", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc_bytes(&g_1487[i], sizeof(g_1487[i]), "g_1487[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1488, sizeof(g_1488), "g_1488", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc_bytes(&g_1489[i], sizeof(g_1489[i]), "g_1489[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc_bytes(&g_1490[i], sizeof(g_1490[i]), "g_1490[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc_bytes(&g_1491[i][j][k], sizeof(g_1491[i][j][k]), "g_1491[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_1492, sizeof(g_1492), "g_1492", print_hash_value);
    transparent_crc_bytes (&g_1493, sizeof(g_1493), "g_1493", print_hash_value);
    transparent_crc_bytes (&g_1501, sizeof(g_1501), "g_1501", print_hash_value);
    transparent_crc_bytes (&g_1502.f0, sizeof(g_1502.f0), "g_1502.f0", print_hash_value);
    transparent_crc(g_1502.f1, "g_1502.f1", print_hash_value);
    transparent_crc(g_1502.f2, "g_1502.f2", print_hash_value);
    transparent_crc(g_1502.f3, "g_1502.f3", print_hash_value);
    transparent_crc(g_1514, "g_1514", print_hash_value);
    transparent_crc(g_1559, "g_1559", print_hash_value);
    transparent_crc(g_1628, "g_1628", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc_bytes(&g_1678[i][j][k], sizeof(g_1678[i][j][k]), "g_1678[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1679, "g_1679", print_hash_value);
    transparent_crc(g_1728, "g_1728", print_hash_value);
    transparent_crc(g_1729.f0, "g_1729.f0", print_hash_value);
    transparent_crc(g_1729.f1, "g_1729.f1", print_hash_value);
    transparent_crc(g_1729.f2, "g_1729.f2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc_bytes(&g_1753[i][j].f0, sizeof(g_1753[i][j].f0), "g_1753[i][j].f0", print_hash_value);
            transparent_crc(g_1753[i][j].f1, "g_1753[i][j].f1", print_hash_value);
            transparent_crc(g_1753[i][j].f2, "g_1753[i][j].f2", print_hash_value);
            transparent_crc(g_1753[i][j].f3, "g_1753[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_1755[i].f0, sizeof(g_1755[i].f0), "g_1755[i].f0", print_hash_value);
        transparent_crc(g_1755[i].f1, "g_1755[i].f1", print_hash_value);
        transparent_crc(g_1755[i].f2, "g_1755[i].f2", print_hash_value);
        transparent_crc(g_1755[i].f3, "g_1755[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 449
   depth: 1, occurrence: 17
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 3
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 19
breakdown:
   indirect level: 0, occurrence: 8
   indirect level: 1, occurrence: 8
   indirect level: 2, occurrence: 2
   indirect level: 3, occurrence: 1
XXX full-bitfields structs in the program: 8
breakdown:
   indirect level: 0, occurrence: 8
XXX times a bitfields struct's address is taken: 52
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 9
XXX times a single bitfield on LHS: 3
XXX times a single bitfield on RHS: 45

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 235
   depth: 2, occurrence: 56
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 6, occurrence: 4
   depth: 7, occurrence: 1
   depth: 9, occurrence: 3
   depth: 10, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 5
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 4
   depth: 22, occurrence: 4
   depth: 23, occurrence: 4
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 3
   depth: 27, occurrence: 1
   depth: 28, occurrence: 6
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 41, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 400

XXX times a variable address is taken: 1054
XXX times a pointer is dereferenced on RHS: 166
breakdown:
   depth: 1, occurrence: 141
   depth: 2, occurrence: 18
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
XXX times a pointer is dereferenced on LHS: 190
breakdown:
   depth: 1, occurrence: 174
   depth: 2, occurrence: 11
   depth: 3, occurrence: 4
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 42
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 8183

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 789
   level: 2, occurrence: 120
   level: 3, occurrence: 112
   level: 4, occurrence: 44
XXX number of pointers point to pointers: 111
XXX number of pointers point to scalars: 268
XXX number of pointers point to structs: 21
XXX percent of pointers has null in alias set: 27.2
XXX average alias set size: 1.46

XXX times a non-volatile is read: 1189
XXX times a non-volatile is write: 585
XXX times a volatile is read: 90
XXX    times read thru a pointer: 7
XXX times a volatile is write: 20
XXX    times written thru a pointer: 1
XXX times a volatile is available for access: 6.82e+03
XXX percentage of non-volatile access: 94.2

XXX forward jumps: 2
XXX backward jumps: 7

XXX stmts: 230
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 21
   depth: 2, occurrence: 29
   depth: 3, occurrence: 43
   depth: 4, occurrence: 56
   depth: 5, occurrence: 51

XXX percentage a fresh-made variable is used: 15.8
XXX percentage an existing variable is used: 84.2
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


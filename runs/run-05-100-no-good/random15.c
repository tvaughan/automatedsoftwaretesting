/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1094309398
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int8_t  f0;
   const uint64_t  f1;
};

union U1 {
   int32_t  f0;
   int32_t  f1;
   int32_t  f2;
   const volatile uint32_t  f3;
   int64_t  f4;
};

union U2 {
   const uint32_t  f0;
   uint64_t  f1;
};

union U3 {
   const signed f0 : 4;
   uint8_t  f1;
   uint32_t  f2;
   uint64_t  f3;
};

union U4 {
   uint8_t  f0;
   uint64_t  f1;
   float  f2;
   int8_t * f3;
};

union U5 {
   uint32_t  f0;
   volatile float  f1;
   volatile int16_t  f2;
};

union U6 {
   int32_t  f0;
   int8_t  f1;
   int8_t * f2;
};

union U7 {
   volatile float  f0;
   volatile int8_t  f1;
};

union U8 {
   volatile uint64_t  f0;
   uint32_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0x773672D7L;
static volatile int32_t g_5 = 0xE665969AL;/* VOLATILE GLOBAL g_5 */
static volatile int32_t g_6 = 0xB785B647L;/* VOLATILE GLOBAL g_6 */
static int32_t g_7 = 0x8EC32687L;
static int8_t g_22 = 1L;
static union U6 g_50[8] = {{-1L},{-1L},{-1L},{-1L},{-1L},{-1L},{-1L},{-1L}};
static uint16_t g_115[8] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
static int32_t g_124 = 0L;
static uint32_t g_126 = 0x8FA0D634L;
static const int8_t *g_128 = &g_50[6].f1;
static const int8_t **g_127 = &g_128;
static int8_t *g_133 = &g_22;
static int8_t **g_132 = &g_133;
static uint32_t g_144[2] = {0xE030C704L,0xE030C704L};
static uint16_t *g_152 = &g_115[1];
static uint8_t g_161 = 0xB5L;
static int64_t g_167 = 4L;
static uint8_t g_172 = 0UL;
static union U2 g_180 = {5UL};
static float g_194 = 0x3.408E39p+62;
static int32_t g_197 = 1L;
static int16_t g_198[7] = {0x146CL,0x146CL,0x146CL,0x146CL,0x146CL,0x146CL,0x146CL};
static int8_t g_227 = 0xD9L;
static uint16_t g_228 = 0xB351L;
static const uint32_t *g_241 = &g_144[1];
static const uint32_t ** volatile g_240[7] = {&g_241,&g_241,&g_241,&g_241,&g_241,&g_241,&g_241};
static const uint32_t ** volatile *g_239 = &g_240[0];
static union U4 g_283 = {0x18L};
static int64_t g_291 = 0x2993C0D730730238LL;
static int8_t g_293 = 0x9CL;
static uint64_t g_295 = 0x807B0C9AB25B9800LL;
static float g_298 = 0x1.8A2399p+91;
static int32_t g_300 = 1L;
static uint8_t g_301 = 0x8AL;
static union U0 g_328 = {0x69L};
static uint32_t g_339 = 1UL;
static uint16_t g_360 = 0x2E68L;
static uint64_t *g_364 = &g_295;
static uint64_t **g_363[2][3] = {{&g_364,&g_364,&g_364},{&g_364,&g_364,&g_364}};
static const int32_t g_384[9] = {0x63957BBCL,1L,1L,0x63957BBCL,1L,1L,0x63957BBCL,1L,(-1L)};
static const int32_t *g_383 = &g_384[4];
static const int32_t **g_382 = &g_383;
static int32_t ***g_399 = (void*)0;
static uint64_t g_402 = 8UL;
static float g_422[5][6] = {{(-0x6.Bp+1),0x1.1p-1,(-0x3.Ap+1),(-0x3.Ap+1),0x1.1p-1,(-0x6.Bp+1)},{0xA.BE0DFAp+92,(-0x6.Bp+1),0x8.Cp-1,0x1.1p-1,0x8.Cp-1,(-0x6.Bp+1)},{0x8.Cp-1,0xA.BE0DFAp+92,(-0x3.Ap+1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x3.Ap+1)},{0x8.Cp-1,0x8.Cp-1,(-0x1.Cp-1),0x1.1p-1,(-0x10.5p-1),0x1.1p-1},{0xA.BE0DFAp+92,0x8.Cp-1,0xA.BE0DFAp+92,(-0x3.Ap+1),(-0x1.Cp-1),(-0x1.Cp-1)}};
static int32_t g_424 = 9L;
static uint16_t g_425 = 0UL;
static union U2 g_440 = {0x4D5AF559L};
static union U8 g_489 = {18446744073709551612UL};/* VOLATILE GLOBAL g_489 */
static const uint64_t **g_495[1] = {(void*)0};
static const uint64_t g_498 = 0xCEC06E7D003E0EB5LL;
static union U3 g_528 = {0xE8BED672L};
static uint16_t g_543 = 0x9BF2L;
static int32_t g_568 = 0L;
static int32_t * const g_567 = &g_568;
static int32_t * const *g_566 = &g_567;
static int32_t * const **g_565 = &g_566;
static int32_t * const ***g_564 = &g_565;
static uint64_t g_666 = 18446744073709551613UL;
static uint16_t g_673 = 65533UL;
static uint16_t ***g_736[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static int64_t g_744 = (-9L);
static uint8_t g_746 = 252UL;
static uint32_t g_749 = 4294967293UL;
static float g_770 = (-0x2.Cp-1);
static uint16_t g_771[2][4][1] = {{{1UL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{1UL},{1UL}}};
static int64_t g_788 = 0xA78AD8A29F60FDFBLL;
static uint8_t g_838 = 251UL;
static uint32_t g_843 = 1UL;
static uint8_t g_871 = 0xA9L;
static union U6 g_891 = {-6L};
static uint64_t g_894 = 0UL;
static union U8 g_899 = {18446744073709551615UL};/* VOLATILE GLOBAL g_899 */
static union U8 g_900 = {0UL};/* VOLATILE GLOBAL g_900 */
static union U8 g_901 = {3UL};/* VOLATILE GLOBAL g_901 */
static union U8 g_902 = {18446744073709551615UL};/* VOLATILE GLOBAL g_902 */
static union U8 g_903 = {0x3F6FC0E97B1F233DLL};/* VOLATILE GLOBAL g_903 */
static union U8 g_904 = {0x8C529AEE8B2E4D22LL};/* VOLATILE GLOBAL g_904 */
static union U8 g_905 = {8UL};/* VOLATILE GLOBAL g_905 */
static union U8 g_906 = {9UL};/* VOLATILE GLOBAL g_906 */
static union U8 g_907 = {9UL};/* VOLATILE GLOBAL g_907 */
static union U8 g_908 = {0xDC9520BD6704BB14LL};/* VOLATILE GLOBAL g_908 */
static union U8 g_909 = {3UL};/* VOLATILE GLOBAL g_909 */
static union U8 g_910 = {0UL};/* VOLATILE GLOBAL g_910 */
static union U8 g_911 = {0UL};/* VOLATILE GLOBAL g_911 */
static union U8 g_912 = {0xA8AF7AFD2BC9FA6ALL};/* VOLATILE GLOBAL g_912 */
static union U8 g_913 = {0x7E8395BEB7BBD197LL};/* VOLATILE GLOBAL g_913 */
static union U8 g_914 = {0xB7A93A14A72167E2LL};/* VOLATILE GLOBAL g_914 */
static union U8 g_915 = {0xB58512DFE6042345LL};/* VOLATILE GLOBAL g_915 */
static union U8 g_916[1][1] = {{{0x059EE6F25AA9BEFBLL}}};
static union U8 g_917 = {18446744073709551615UL};/* VOLATILE GLOBAL g_917 */
static union U8 g_918 = {18446744073709551615UL};/* VOLATILE GLOBAL g_918 */
static union U8 g_919 = {0UL};/* VOLATILE GLOBAL g_919 */
static union U8 g_920[5] = {{4UL},{4UL},{4UL},{4UL},{4UL}};
static union U8 g_921 = {0xFA446A33E30D6455LL};/* VOLATILE GLOBAL g_921 */
static union U8 g_922 = {18446744073709551606UL};/* VOLATILE GLOBAL g_922 */
static union U8 g_923[7][1][7] = {{{{0x0AD52F539918ED02LL},{0xED88C78EB8E9C7C7LL},{0UL},{0UL},{0xED88C78EB8E9C7C7LL},{0x0AD52F539918ED02LL},{0xF38987C07A337841LL}}},{{{18446744073709551615UL},{0xED88C78EB8E9C7C7LL},{0xCEACA67FE2CCA3CCLL},{1UL},{0xED88C78EB8E9C7C7LL},{1UL},{0x86717778BD77C6B6LL}}},{{{18446744073709551615UL},{3UL},{0UL},{1UL},{3UL},{0x0AD52F539918ED02LL},{0x86717778BD77C6B6LL}}},{{{0x0AD52F539918ED02LL},{0xED88C78EB8E9C7C7LL},{0UL},{0UL},{0xED88C78EB8E9C7C7LL},{0x0AD52F539918ED02LL},{0xF38987C07A337841LL}}},{{{18446744073709551615UL},{0xED88C78EB8E9C7C7LL},{0xCEACA67FE2CCA3CCLL},{1UL},{0xED88C78EB8E9C7C7LL},{1UL},{0x86717778BD77C6B6LL}}},{{{18446744073709551615UL},{3UL},{0UL},{1UL},{3UL},{0x0AD52F539918ED02LL},{0x86717778BD77C6B6LL}}},{{{0x0AD52F539918ED02LL},{0xED88C78EB8E9C7C7LL},{0UL},{0UL},{0xED88C78EB8E9C7C7LL},{0x0AD52F539918ED02LL},{0xF38987C07A337841LL}}}};
static union U8 g_924 = {0x407CF0C4822256AFLL};/* VOLATILE GLOBAL g_924 */
static union U8 g_925 = {0UL};/* VOLATILE GLOBAL g_925 */
static union U8 g_926 = {0x30C9CEAE1448DFF5LL};/* VOLATILE GLOBAL g_926 */
static union U8 g_927 = {1UL};/* VOLATILE GLOBAL g_927 */
static union U8 g_928 = {0x329F5B34B88D0204LL};/* VOLATILE GLOBAL g_928 */
static union U8 g_929 = {0xECA7DCA204065E25LL};/* VOLATILE GLOBAL g_929 */
static union U8 g_930[1] = {{0x2280ABCBDDF6D676LL}};
static union U8 g_931[5][3] = {{{0x5996ADBABFD6E79ELL},{1UL},{1UL}},{{0x5996ADBABFD6E79ELL},{1UL},{1UL}},{{0x5996ADBABFD6E79ELL},{1UL},{1UL}},{{0x5996ADBABFD6E79ELL},{1UL},{1UL}},{{0x5996ADBABFD6E79ELL},{1UL},{1UL}}};
static union U8 g_932[1] = {{0x7F302B4AFAB96046LL}};
static union U8 g_933 = {1UL};/* VOLATILE GLOBAL g_933 */
static union U8 g_934 = {0xE017BDB3083931E0LL};/* VOLATILE GLOBAL g_934 */
static union U8 g_935 = {0xDCB151BA95AE0143LL};/* VOLATILE GLOBAL g_935 */
static union U8 g_936 = {0x92AE74400B5F16EELL};/* VOLATILE GLOBAL g_936 */
static union U8 g_937 = {0xB7622B5DC7144C31LL};/* VOLATILE GLOBAL g_937 */
static union U8 g_938 = {0x1424332143F33EE4LL};/* VOLATILE GLOBAL g_938 */
static union U8 g_939 = {18446744073709551615UL};/* VOLATILE GLOBAL g_939 */
static union U8 g_940 = {0xD8A9F2FE7F8A1569LL};/* VOLATILE GLOBAL g_940 */
static union U8 g_941[3] = {{0xC73810CAD9929F1BLL},{0xC73810CAD9929F1BLL},{0xC73810CAD9929F1BLL}};
static union U8 g_942 = {4UL};/* VOLATILE GLOBAL g_942 */
static union U8 g_943 = {0xA83A4996BF92A6EDLL};/* VOLATILE GLOBAL g_943 */
static union U8 g_944 = {18446744073709551608UL};/* VOLATILE GLOBAL g_944 */
static union U8 g_945 = {0x6619CC58F1DFF5C7LL};/* VOLATILE GLOBAL g_945 */
static union U8 g_946[6] = {{0xBA2F5CC6BCA18768LL},{0xBA2F5CC6BCA18768LL},{0xBA2F5CC6BCA18768LL},{0xBA2F5CC6BCA18768LL},{0xBA2F5CC6BCA18768LL},{0xBA2F5CC6BCA18768LL}};
static union U8 g_947 = {18446744073709551613UL};/* VOLATILE GLOBAL g_947 */
static union U8 g_948 = {0x1113B003181331C6LL};/* VOLATILE GLOBAL g_948 */
static union U8 g_949 = {1UL};/* VOLATILE GLOBAL g_949 */
static union U8 g_950 = {0x05E35A2037C66D92LL};/* VOLATILE GLOBAL g_950 */
static union U8 g_951 = {0UL};/* VOLATILE GLOBAL g_951 */
static union U8 g_952 = {0x3E776270DD3BF075LL};/* VOLATILE GLOBAL g_952 */
static union U8 g_953 = {0UL};/* VOLATILE GLOBAL g_953 */
static union U8 g_954[5] = {{0xE83E1C0CABE8DBB6LL},{0xE83E1C0CABE8DBB6LL},{0xE83E1C0CABE8DBB6LL},{0xE83E1C0CABE8DBB6LL},{0xE83E1C0CABE8DBB6LL}};
static union U8 g_955 = {0x9E1C2A8B66837648LL};/* VOLATILE GLOBAL g_955 */
static union U8 g_956 = {0xCB7A18DFFC4BE980LL};/* VOLATILE GLOBAL g_956 */
static int32_t g_964 = 9L;
static uint64_t g_966 = 3UL;
static uint8_t g_975 = 0xF0L;
static union U2 g_981[2][1][6] = {{{{0x67451120L},{0x67451120L},{0x67451120L},{0x67451120L},{0x67451120L},{0x67451120L}}},{{{0x67451120L},{0x67451120L},{0x67451120L},{0x67451120L},{0x67451120L},{0x67451120L}}}};
static const uint32_t g_985 = 1UL;
static int32_t ****g_1011[10] = {&g_399,&g_399,&g_399,&g_399,&g_399,&g_399,&g_399,&g_399,&g_399,&g_399};
static int64_t g_1031 = 2L;
static uint8_t g_1032 = 0x45L;
static uint16_t g_1042 = 65534UL;
static uint16_t g_1043 = 0xF782L;
static const int32_t g_1048[3] = {0x3DF415F3L,0x3DF415F3L,0x3DF415F3L};
static const int32_t g_1050 = 0xB0C16AE5L;
static const int32_t *g_1049 = &g_1050;
static const int32_t g_1052 = 0xE697CD85L;
static const int32_t *g_1053 = &g_964;
static union U7 g_1059[9] = {{0x9.BD6D27p+50},{0xB.E718E8p+69},{0x9.BD6D27p+50},{0x9.BD6D27p+50},{0xB.E718E8p+69},{0x9.BD6D27p+50},{0x9.BD6D27p+50},{0xB.E718E8p+69},{0x9.BD6D27p+50}};
static union U0 g_1104[3][9][9] = {{{{-1L},{0x36L},{0L},{1L},{3L},{0x84L},{-1L},{0x17L},{0xDBL}},{{0L},{-2L},{3L},{0x03L},{-1L},{0x9BL},{0x70L},{0x3FL},{3L}},{{3L},{0x77L},{0xC5L},{-1L},{-6L},{1L},{-1L},{0x72L},{0x2AL}},{{5L},{0x3FL},{0L},{0x17L},{0x1CL},{0x7EL},{2L},{1L},{-10L}},{{1L},{0xDBL},{-2L},{0x77L},{0x77L},{-2L},{0xDBL},{1L},{5L}},{{0x36L},{0L},{0L},{0x2AL},{0x22L},{-1L},{6L},{-1L},{0xDBL}},{{6L},{3L},{0x77L},{-1L},{0x17L},{-3L},{0x36L},{0x2AL},{5L}},{{0L},{0x7EL},{-4L},{2L},{0L},{-1L},{0x2AL},{6L},{-10L}},{{0x03L},{0x1CL},{0x72L},{-1L},{-1L},{0xDBL},{-6L},{0L},{0x70L}}},{{{-1L},{1L},{0x03L},{-1L},{1L},{3L},{5L},{0x7EL},{0x7EL}},{{0x21L},{0x03L},{-1L},{2L},{-1L},{0x03L},{0x21L},{0x29L},{-1L}},{{0x07L},{-2L},{0x45L},{-1L},{0x70L},{-1L},{0x2EL},{-4L},{0xF1L}},{{-2L},{-4L},{1L},{0x2AL},{2L},{0x17L},{0x72L},{0x29L},{0x22L}},{{0x17L},{0x22L},{6L},{0x77L},{9L},{0x3FL},{1L},{0x7EL},{-3L}},{{-4L},{0x47L},{0x2EL},{0x17L},{0x7EL},{0x72L},{0L},{0L},{6L}},{{0x36L},{1L},{0x2EL},{0x84L},{5L},{0xC5L},{9L},{6L},{9L}},{{0xF1L},{0x29L},{6L},{6L},{0x29L},{0xF1L},{3L},{0x2AL},{0x72L}},{{0x7EL},{0x36L},{1L},{3L},{0L},{1L},{0x1CL},{-1L},{0x45L}}},{{{0xC5L},{0L},{0x45L},{0x70L},{-6L},{-4L},{3L},{1L},{0L}},{{0x47L},{-1L},{-1L},{-4L},{0x03L},{0x03L},{9L},{1L},{0x2AL}},{{0L},{-1L},{0x03L},{0xC5L},{-1L},{0x22L},{0L},{5L},{0x29L}},{{0x70L},{-1L},{0x72L},{0x9BL},{0x84L},{3L},{1L},{1L},{3L}},{{0L},{-1L},{-4L},{-1L},{0L},{0L},{0x72L},{2L},{3L}},{{3L},{0L},{0x77L},{1L},{0x21L},{5L},{0x2EL},{0x03L},{1L}},{{0x45L},{-2L},{0x47L},{-1L},{0xDBL},{0x29L},{-3L},{9L},{0x72L}},{{1L},{0x45L},{0x17L},{-10L},{3L},{0x7EL},{0x2EL},{0L},{5L}},{{-10L},{-1L},{3L},{0x45L},{0x2AL},{0L},{0x1CL},{0x03L},{-1L}}}};
static union U4 *g_1128 = &g_283;
static union U8 g_1145 = {18446744073709551615UL};/* VOLATILE GLOBAL g_1145 */
static uint64_t * volatile *g_1169 = &g_364;
static uint64_t * volatile **g_1168 = &g_1169;
static uint64_t * volatile ***g_1167 = &g_1168;
static uint64_t * volatile ****g_1166 = &g_1167;
static union U7 g_1179 = {0x5.F9E93Fp-49};/* VOLATILE GLOBAL g_1179 */
static union U7 g_1182 = {0xE.4CFEC8p-21};/* VOLATILE GLOBAL g_1182 */
static union U8 g_1233 = {3UL};/* VOLATILE GLOBAL g_1233 */
static union U7 g_1270[2] = {{0x4.EB5CF2p+1},{0x4.EB5CF2p+1}};
static int8_t g_1286 = (-7L);
static uint32_t g_1288 = 0x094F1F66L;
static const volatile int32_t *** volatile g_1315[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const volatile int32_t g_1319 = (-1L);/* VOLATILE GLOBAL g_1319 */
static volatile int32_t g_1320 = 0L;/* VOLATILE GLOBAL g_1320 */
static const volatile int32_t *g_1318[9] = {&g_1319,(void*)0,(void*)0,&g_1319,(void*)0,(void*)0,&g_1319,(void*)0,(void*)0};
static const volatile int32_t **g_1317 = &g_1318[0];
static const volatile int32_t ***g_1316 = &g_1317;
static const volatile int32_t *** volatile *g_1314[8][6] = {{&g_1316,&g_1316,&g_1315[4],&g_1315[4],&g_1316,&g_1316},{&g_1316,&g_1316,&g_1315[4],&g_1316,&g_1316,&g_1316},{&g_1316,&g_1316,&g_1316,&g_1316,&g_1316,&g_1316},{&g_1316,&g_1316,&g_1316,&g_1316,&g_1316,&g_1316},{&g_1316,&g_1316,&g_1316,&g_1315[4],&g_1316,&g_1316},{&g_1316,&g_1316,&g_1315[4],&g_1315[4],&g_1316,&g_1316},{&g_1316,&g_1316,&g_1315[4],&g_1316,&g_1316,&g_1316},{&g_1316,&g_1316,&g_1316,&g_1316,&g_1316,&g_1316}};
static int32_t g_1325 = 0x99AF277CL;
static int64_t * volatile *g_1327 = (void*)0;
static int32_t *g_1339 = &g_7;
static int32_t **g_1338 = &g_1339;
static uint64_t g_1373 = 0x61286CC7735FF915LL;
static uint64_t g_1432 = 1UL;
static uint32_t ***g_1435 = (void*)0;
static uint32_t *** const *g_1434 = &g_1435;
static uint64_t ***g_1443 = &g_363[1][0];
static uint64_t ****g_1442 = &g_1443;
static uint64_t ** const *g_1456 = (void*)0;
static uint64_t ** const **g_1455 = &g_1456;
static int8_t g_1470 = (-7L);
static uint64_t g_1473 = 0xD94761881B090FB1LL;
static int16_t g_1520[9] = {0x3004L,0x3004L,0x3004L,0x3004L,0x3004L,0x3004L,0x3004L,0x3004L,0x3004L};
static int16_t g_1522 = 0L;
static uint64_t g_1523 = 0xC9E1D1154FE0922CLL;
static union U8 g_1526 = {18446744073709551612UL};/* VOLATILE GLOBAL g_1526 */
static union U8 *g_1528 = (void*)0;
static union U8 * volatile *g_1527 = &g_1528;
static int8_t g_1543 = (-1L);
static uint32_t g_1544[7][4][9] = {{{0x6B8C01A0L,18446744073709551609UL,1UL,18446744073709551606UL,18446744073709551607UL,18446744073709551609UL,0UL,18446744073709551609UL,18446744073709551607UL},{6UL,8UL,0xDDFCCF46L,0x35216FC8L,7UL,0xDDFCCF46L,0xD479447FL,1UL,1UL},{0x9BDB1B06L,18446744073709551609UL,18446744073709551615UL,0xA432060EL,18446744073709551615UL,18446744073709551609UL,0x9BDB1B06L,0UL,18446744073709551615UL},{6UL,7UL,8UL,0xD479447FL,7UL,1UL,0xD88B2880L,0xDDFCCF46L,1UL}},{{0x6B8C01A0L,0UL,1UL,0xA432060EL,18446744073709551607UL,0UL,0UL,0UL,18446744073709551607UL},{0x35216FC8L,8UL,8UL,0x35216FC8L,1UL,0xDDFCCF46L,0xD88B2880L,1UL,7UL},{0x9BDB1B06L,0UL,18446744073709551615UL,18446744073709551606UL,18446744073709551615UL,0UL,0x9BDB1B06L,18446744073709551609UL,18446744073709551615UL},{0x35216FC8L,7UL,0xDDFCCF46L,0xD479447FL,1UL,1UL,0xD479447FL,0xDDFCCF46L,7UL}},{{0x6B8C01A0L,18446744073709551609UL,1UL,18446744073709551606UL,18446744073709551607UL,18446744073709551609UL,0UL,18446744073709551609UL,18446744073709551607UL},{6UL,8UL,0xDDFCCF46L,0x35216FC8L,7UL,0xDDFCCF46L,0xD479447FL,1UL,1UL},{0x9BDB1B06L,0xC6D8A9C0L,1UL,0UL,1UL,0xC6D8A9C0L,18446744073709551615UL,6UL,1UL},{1UL,0xBA98F894L,18446744073709551613UL,8UL,0xBA98F894L,0x610CAE31L,0xDDFCCF46L,0x253B9F3AL,0x610CAE31L}},{{18446744073709551607UL,6UL,0x4BBB0596L,0UL,1UL,6UL,1UL,6UL,1UL},{7UL,18446744073709551613UL,18446744073709551613UL,7UL,0x610CAE31L,0x253B9F3AL,0xDDFCCF46L,0x610CAE31L,0xBA98F894L},{18446744073709551615UL,6UL,1UL,18446744073709551609UL,1UL,6UL,18446744073709551615UL,0xC6D8A9C0L,1UL},{7UL,0xBA98F894L,0x253B9F3AL,8UL,0x610CAE31L,0x610CAE31L,8UL,0x253B9F3AL,0xBA98F894L}},{{18446744073709551607UL,0xC6D8A9C0L,0x4BBB0596L,18446744073709551609UL,1UL,0xC6D8A9C0L,1UL,0xC6D8A9C0L,1UL},{1UL,18446744073709551613UL,0x253B9F3AL,7UL,0xBA98F894L,0x253B9F3AL,8UL,0x610CAE31L,0x610CAE31L},{18446744073709551615UL,0xC6D8A9C0L,1UL,0UL,1UL,0xC6D8A9C0L,18446744073709551615UL,6UL,1UL},{1UL,0xBA98F894L,18446744073709551613UL,8UL,0xBA98F894L,0x610CAE31L,0xDDFCCF46L,0x253B9F3AL,0x610CAE31L}},{{18446744073709551607UL,6UL,0x4BBB0596L,0UL,1UL,6UL,1UL,6UL,1UL},{7UL,18446744073709551613UL,18446744073709551613UL,7UL,0x610CAE31L,0x253B9F3AL,0xDDFCCF46L,0x610CAE31L,0xBA98F894L},{18446744073709551615UL,6UL,1UL,18446744073709551609UL,1UL,6UL,18446744073709551615UL,0xC6D8A9C0L,1UL},{7UL,0xBA98F894L,0x253B9F3AL,8UL,0x610CAE31L,0x610CAE31L,8UL,0x253B9F3AL,0xBA98F894L}},{{18446744073709551607UL,0xC6D8A9C0L,0x4BBB0596L,18446744073709551609UL,1UL,0xC6D8A9C0L,1UL,0xC6D8A9C0L,1UL},{1UL,18446744073709551613UL,0x253B9F3AL,7UL,0xBA98F894L,0x253B9F3AL,8UL,0x610CAE31L,0x610CAE31L},{18446744073709551615UL,0xC6D8A9C0L,1UL,0UL,1UL,0xC6D8A9C0L,18446744073709551615UL,6UL,1UL},{1UL,0xBA98F894L,18446744073709551613UL,8UL,0xBA98F894L,0x610CAE31L,0xDDFCCF46L,0x253B9F3AL,0x610CAE31L}}};
static uint32_t g_1613 = 4294967287UL;
static uint32_t * const g_1612 = &g_1613;
static uint32_t * const *g_1611 = &g_1612;
static volatile union U5 g_1623[2] = {{1UL},{1UL}};
static float g_1693 = 0xA.222AD9p-42;
static uint32_t g_1694 = 0x86520F0BL;
static union U7 g_1745 = {0xA.C2DFA2p+93};/* VOLATILE GLOBAL g_1745 */
static float * volatile g_1769 = &g_298;/* VOLATILE GLOBAL g_1769 */
static volatile int16_t * volatile g_1772 = (void*)0;/* VOLATILE GLOBAL g_1772 */
static volatile int16_t * volatile * volatile g_1771 = &g_1772;/* VOLATILE GLOBAL g_1771 */
static volatile int16_t * volatile * volatile * volatile g_1773 = &g_1771;/* VOLATILE GLOBAL g_1773 */
static const union U8 *g_1775 = &g_932[0];
static int8_t g_1782 = 0xDBL;
static volatile uint32_t * const * volatile g_1802 = (void*)0;/* VOLATILE GLOBAL g_1802 */
static volatile uint32_t * const * volatile * volatile g_1803[7][9] = {{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802},{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802},{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802},{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802},{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802},{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802},{&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802,&g_1802}};
static volatile uint32_t * const * volatile * volatile g_1804 = &g_1802;/* VOLATILE GLOBAL g_1804 */
static volatile union U5 g_1831 = {0UL};/* VOLATILE GLOBAL g_1831 */
static volatile union U1 g_1836 = {0x8A887F64L};/* VOLATILE GLOBAL g_1836 */
static uint16_t **g_1838 = &g_152;
static volatile uint32_t * volatile *g_1847[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static float * volatile g_1868 = &g_770;/* VOLATILE GLOBAL g_1868 */
static union U1 g_1883 = {0L};/* VOLATILE GLOBAL g_1883 */
static union U7 * volatile * volatile g_1931 = (void*)0;/* VOLATILE GLOBAL g_1931 */
static float * volatile g_1934 = &g_422[0][4];/* VOLATILE GLOBAL g_1934 */
static union U7 *g_1938[10][2][2] = {{{&g_1059[0],&g_1182},{(void*)0,&g_1059[0]}},{{&g_1182,&g_1270[1]},{&g_1059[3],&g_1059[3]}},{{(void*)0,&g_1059[3]},{&g_1059[3],&g_1270[1]}},{{&g_1182,&g_1059[0]},{(void*)0,&g_1182}},{{&g_1059[0],&g_1270[1]},{&g_1059[0],&g_1182}},{{(void*)0,&g_1059[0]},{&g_1182,&g_1270[1]}},{{&g_1059[3],&g_1059[3]},{(void*)0,&g_1059[3]}},{{&g_1059[3],&g_1270[1]},{&g_1182,&g_1059[0]}},{{(void*)0,&g_1182},{&g_1059[0],&g_1270[1]}},{{&g_1059[0],&g_1182},{(void*)0,&g_1059[0]}}};
static union U7 **g_1937 = &g_1938[2][0][1];
static float *g_1946 = &g_298;
static float **g_1945 = &g_1946;
static union U8 g_1955 = {0UL};/* VOLATILE GLOBAL g_1955 */
static union U7 g_1961 = {0x1.6p-1};/* VOLATILE GLOBAL g_1961 */
static const uint16_t * const *g_1976 = (void*)0;
static const uint16_t g_1980 = 0x3389L;
static int32_t *****g_2009 = &g_1011[1];
static uint16_t ****g_2027 = &g_736[0];
static volatile uint8_t g_2041 = 1UL;/* VOLATILE GLOBAL g_2041 */
static volatile uint8_t *g_2040[7][4] = {{&g_2041,&g_2041,(void*)0,&g_2041},{&g_2041,&g_2041,&g_2041,&g_2041},{&g_2041,&g_2041,(void*)0,&g_2041},{&g_2041,&g_2041,&g_2041,(void*)0},{&g_2041,(void*)0,&g_2041,(void*)0},{&g_2041,&g_2041,&g_2041,&g_2041},{(void*)0,&g_2041,&g_2041,&g_2041}};
static volatile uint8_t **g_2039 = &g_2040[3][2];
static volatile union U1 g_2071 = {0L};/* VOLATILE GLOBAL g_2071 */
static volatile union U6 g_2100[1] = {{0L}};
static const volatile union U6 *g_2099 = &g_2100[0];
static const volatile union U6 **g_2098[7] = {&g_2099,&g_2099,(void*)0,&g_2099,&g_2099,(void*)0,&g_2099};
static volatile union U0 g_2120 = {-4L};/* VOLATILE GLOBAL g_2120 */
static volatile union U0 * volatile g_2119[6] = {&g_2120,&g_2120,&g_2120,&g_2120,&g_2120,&g_2120};
static volatile union U0 * volatile *g_2118[2][2] = {{&g_2119[2],&g_2119[2]},{&g_2119[2],&g_2119[2]}};
static volatile union U0 * volatile **g_2117 = &g_2118[0][0];
static volatile union U0 * volatile *** volatile g_2121[3][7] = {{&g_2117,&g_2117,&g_2117,&g_2117,&g_2117,&g_2117,&g_2117},{(void*)0,&g_2117,(void*)0,&g_2117,(void*)0,&g_2117,(void*)0},{&g_2117,&g_2117,&g_2117,&g_2117,&g_2117,&g_2117,&g_2117}};
static volatile union U0 * volatile *** volatile g_2123[1][1][7] = {{{&g_2117,&g_2117,&g_2117,&g_2117,&g_2117,&g_2117,&g_2117}}};
static volatile union U0 * volatile *** volatile g_2124[2] = {&g_2117,&g_2117};
static volatile union U0 * volatile *** volatile g_2125 = &g_2117;/* VOLATILE GLOBAL g_2125 */
static volatile int8_t g_2137 = 0L;/* VOLATILE GLOBAL g_2137 */
static volatile int8_t g_2138 = (-2L);/* VOLATILE GLOBAL g_2138 */
static int8_t g_2139 = 0x49L;
static volatile int16_t g_2140 = 0x0410L;/* VOLATILE GLOBAL g_2140 */
static uint32_t g_2144 = 0x45989186L;
static const union U7 g_2168 = {-0x1.Dp-1};/* VOLATILE GLOBAL g_2168 */
static volatile uint64_t g_2180 = 0x52EC32311A072B77LL;/* VOLATILE GLOBAL g_2180 */
static volatile union U7 g_2190 = {0xF.0D7A40p+55};/* VOLATILE GLOBAL g_2190 */
static int32_t g_2219 = 0xA73CF882L;
static volatile union U7 g_2273 = {0x2.9p+1};/* VOLATILE GLOBAL g_2273 */
static int32_t g_2305[10] = {0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL,0xE6D9F73DL};
static int32_t *g_2312 = &g_2305[0];
static int32_t ** volatile g_2311 = &g_2312;/* VOLATILE GLOBAL g_2311 */
static uint64_t *****g_2337 = &g_1442;


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static const union U6  func_15(const int8_t  p_16, int8_t * p_17, int8_t * p_18, int8_t * p_19, const union U0  p_20);
static int8_t * func_24(int8_t * p_25, uint32_t  p_26, uint64_t  p_27, int8_t ** p_28, int8_t * const * p_29);
static int16_t  func_40(int8_t  p_41);
static uint32_t  func_42(int8_t ** const  p_43, int16_t  p_44, int8_t * p_45, const uint16_t  p_46, union U6  p_47);
static union U4  func_57(uint8_t  p_58, uint8_t  p_59, const uint32_t  p_60);
static uint8_t  func_61(uint8_t  p_62, int32_t  p_63, int64_t  p_64, union U0  p_65);
static int32_t  func_75(uint32_t  p_76, uint8_t  p_77, int8_t * p_78, union U6  p_79, int8_t * p_80);
static int8_t * func_83(union U0  p_84, int8_t ** p_85, int8_t  p_86, float  p_87, int8_t * p_88);
static int8_t ** func_90(uint16_t  p_91, int8_t * p_92);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_2219
 * writes: g_2 g_7
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    int64_t l_10 = 0xC93CC254292978D8LL;
    int8_t *l_21 = &g_22;
    int8_t **l_23 = &l_21;
    uint32_t l_39 = 0x0BA5FC02L;
    int8_t *l_1939 = &g_1104[1][0][5].f0;
    uint32_t l_2182 = 1UL;
    uint64_t l_2183 = 0x3072628910250364LL;
    int32_t l_2210 = 0xD313ECD8L;
    int32_t l_2220 = 0L;
    int32_t l_2227 = 0L;
    int32_t l_2241 = 1L;
    int32_t *l_2341 = &g_2219;
    const union U0 l_2372 = {1L};
    union U6 l_2373[9][3][9] = {{{{1L},{0x7E6621EBL},{0x2F24D05DL},{0x399CC01EL},{0x399CC01EL},{0x2F24D05DL},{0x7E6621EBL},{1L},{0x2F24D05DL}},{{1L},{0x7E6621EBL},{0x2F24D05DL},{0x399CC01EL},{0x399CC01EL},{0x2F24D05DL},{0x7E6621EBL},{1L},{0x2F24D05DL}},{{1L},{0x7E6621EBL},{0x2F24D05DL},{0x399CC01EL},{0x399CC01EL},{0x2F24D05DL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}}},{{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0xBD29E273L},{0x399CC01EL}},{{0xBD29E273L},{-8L},{0x399CC01EL},{0x0FB4D7B9L},{0x0FB4D7B9L},{0x399CC01EL},{-8L},{0x0B843090L},{0x0FB4D7B9L}}},{{{0x0B843090L},{0x2F24D05DL},{0x0FB4D7B9L},{-6L},{-6L},{0x0FB4D7B9L},{0x2F24D05DL},{0x0B843090L},{0x0FB4D7B9L}},{{0x0B843090L},{0x2F24D05DL},{0x0FB4D7B9L},{-6L},{-6L},{0x0FB4D7B9L},{0x2F24D05DL},{0x0B843090L},{0x0FB4D7B9L}},{{0x0B843090L},{0x2F24D05DL},{0x0FB4D7B9L},{-6L},{-6L},{0x0FB4D7B9L},{0x2F24D05DL},{0x0B843090L},{0x0FB4D7B9L}}}};
    float l_2374 = 0x0.1p+1;
    int i, j, k;
    for (g_2 = (-2); (g_2 != (-27)); --g_2)
    { /* block id: 3 */
        for (g_7 = 13; (g_7 != 18); g_7 = safe_add_func_int32_t_s_s(g_7, 5))
        { /* block id: 6 */
            return l_10;
        }
        return g_6;
    }
    return (*l_2341);
}


/* ------------------------------------------ */
/* 
 * reads : g_1945 g_1838 g_152 g_115 g_2 g_1955 g_1961 g_1166 g_1167 g_1168 g_1169 g_364 g_1946 g_298 g_1443 g_363 g_295 g_1612 g_1613 g_22 g_1976 g_1522 g_981 g_239 g_240 g_528 g_382 g_383 g_50 g_2039 g_2071 g_1270.f1 g_1316 g_1317 g_838 g_1623 g_241 g_144 g_50.f0 g_1373 g_2098 g_1934 g_422 g_161 g_167 g_2117 g_2125 g_2144 g_402 g_1325 g_899.f1
 * writes: g_2 g_22 g_1031 g_298 g_402 g_1325 g_1522 g_2009 g_115 g_228 g_2027 g_899.f1 g_770 g_167 g_2117 g_2144
 */
static const union U6  func_15(const int8_t  p_16, int8_t * p_17, int8_t * p_18, int8_t * p_19, const union U0  p_20)
{ /* block id: 929 */
    float **l_1947 = &g_1946;
    int32_t l_1953[3][3];
    int32_t *l_1954 = &g_2;
    const uint64_t *l_1960[5] = {&g_1432,&g_1432,&g_1432,&g_1432,&g_1432};
    int64_t *l_1962 = (void*)0;
    int64_t *l_1963 = &g_1031;
    uint16_t **l_1975 = &g_152;
    int32_t l_1988 = 2L;
    int32_t *****l_2008 = (void*)0;
    uint32_t *l_2014 = &g_909.f1;
    uint32_t **l_2013 = &l_2014;
    union U8 **l_2044 = &g_1528;
    union U4 *l_2078 = &g_283;
    union U2 l_2112 = {1UL};
    int32_t l_2135 = 0x610CC11DL;
    int32_t l_2141 = 0xB8CA459CL;
    const union U6 l_2147[7][1] = {{{7L}},{{0xF3D494B7L}},{{0xF3D494B7L}},{{7L}},{{0xF3D494B7L}},{{0xF3D494B7L}},{{7L}}};
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            l_1953[i][j] = (-10L);
    }
lbl_2080:
    (*g_1946) = (safe_sub_func_float_f_f((((((~((*l_1963) = (safe_mul_func_uint16_t_u_u(((((l_1947 = g_1945) != (void*)0) >= (((*p_18) = ((safe_add_func_uint16_t_u_u((**g_1838), (safe_sub_func_int64_t_s_s(((~(l_1953[2][1] , ((*l_1954) ^= p_16))) <= (((g_1955 , (safe_div_func_float_f_f((safe_add_func_float_f_f(p_16, ((((((l_1960[2] != (g_1961 , (****g_1166))) , (**g_1166)) == (**g_1166)) & 0UL) , p_20.f0) == (**g_1945)))), (**g_1945)))) != 0xE.16223Cp-72) , (***g_1443))), 1L)))) , (*l_1954))) && (*l_1954))) < (*g_1612)), 0x7002L)))) , (void*)0) == (void*)0) != p_20.f0) == p_16), (**g_1945)));
    for (g_402 = 0; (g_402 != 6); ++g_402)
    { /* block id: 937 */
        const uint16_t * const l_1979 = &g_1980;
        const uint16_t * const *l_1978 = &l_1979;
        int32_t l_1989 = 1L;
        const int32_t ***l_2012 = (void*)0;
        const int32_t **** const l_2011 = &l_2012;
        const int32_t **** const *l_2010[8][2];
        uint16_t ****l_2026 = &g_736[3];
        uint8_t *l_2064 = &g_283.f0;
        uint8_t **l_2063 = &l_2064;
        int8_t **l_2076 = &g_133;
        int32_t **l_2082 = (void*)0;
        uint32_t ****l_2089 = &g_1435;
        uint32_t *****l_2088[3][6];
        int32_t l_2126 = (-5L);
        int32_t l_2136 = 0x6EB71B78L;
        int32_t l_2142 = 0x24B116D9L;
        int32_t l_2143 = 0x13829AA7L;
        int i, j;
        for (i = 0; i < 8; i++)
        {
            for (j = 0; j < 2; j++)
                l_2010[i][j] = &l_2011;
        }
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 6; j++)
                l_2088[i][j] = &l_2089;
        }
        for (g_1325 = (-9); (g_1325 > 13); g_1325 = safe_add_func_uint32_t_u_u(g_1325, 6))
        { /* block id: 940 */
            uint16_t l_1974 = 0UL;
            const uint16_t * const **l_1977[9][2][6] = {{{&g_1976,&g_1976,&g_1976,&g_1976,&g_1976,&g_1976},{&g_1976,&g_1976,&g_1976,(void*)0,&g_1976,&g_1976}},{{&g_1976,(void*)0,&g_1976,&g_1976,&g_1976,&g_1976},{&g_1976,&g_1976,&g_1976,&g_1976,&g_1976,&g_1976}},{{&g_1976,&g_1976,&g_1976,(void*)0,&g_1976,&g_1976},{&g_1976,(void*)0,&g_1976,&g_1976,&g_1976,&g_1976}},{{&g_1976,&g_1976,&g_1976,&g_1976,&g_1976,&g_1976},{&g_1976,&g_1976,&g_1976,(void*)0,&g_1976,&g_1976}},{{&g_1976,(void*)0,&g_1976,&g_1976,&g_1976,&g_1976},{&g_1976,&g_1976,&g_1976,&g_1976,&g_1976,&g_1976}},{{&g_1976,&g_1976,&g_1976,(void*)0,&g_1976,&g_1976},{&g_1976,(void*)0,&g_1976,&g_1976,&g_1976,&g_1976}},{{&g_1976,&g_1976,&g_1976,&g_1976,&g_1976,&g_1976},{&g_1976,&g_1976,&g_1976,(void*)0,&g_1976,&g_1976}},{{&g_1976,(void*)0,&g_1976,&g_1976,&g_1976,&g_1976},{&g_1976,&g_1976,&g_1976,&g_1976,&g_1976,&g_1976}},{{&g_1976,&g_1976,&g_1976,(void*)0,&g_1976,&g_1976},{&g_1976,(void*)0,&g_1976,&g_1976,&g_1976,&g_1976}}};
            int32_t *l_1986 = &g_1883.f0;
            int16_t *l_1987 = &g_1522;
            union U2 l_2048 = {0xC441F790L};
            union U6 *l_2070[3][6][4] = {{{&g_891,&g_891,(void*)0,&g_50[2]},{&g_50[6],(void*)0,&g_891,(void*)0},{&g_891,&g_50[4],&g_50[3],&g_50[6]},{&g_50[2],&g_50[3],&g_50[6],&g_50[6]},{&g_891,&g_50[2],&g_50[6],&g_50[2]},{&g_891,&g_891,&g_50[6],&g_50[6]}},{{&g_50[2],&g_50[2],&g_50[3],&g_50[6]},{&g_891,&g_50[3],&g_891,&g_50[4]},{&g_50[6],&g_50[2],(void*)0,&g_891},{&g_891,&g_50[2],&g_50[2],&g_50[4]},{&g_50[2],&g_50[3],&g_50[6],&g_891},{&g_50[3],&g_891,(void*)0,&g_50[2]}},{{&g_891,&g_50[3],&g_891,&g_891},{&g_50[6],&g_891,&g_891,&g_50[6]},{&g_891,(void*)0,(void*)0,&g_891},{&g_50[3],&g_50[6],&g_50[6],&g_50[2]},{&g_50[6],&g_50[2],&g_891,&g_891},{&g_50[4],(void*)0,&g_50[6],&g_891}}};
            uint32_t l_2085 = 0xFCCC8782L;
            int32_t l_2114 = (-10L);
            int32_t l_2133 = 0xC27C185FL;
            int32_t l_2134[9] = {(-7L),(-7L),0x3D99B012L,(-7L),(-7L),0x3D99B012L,(-7L),(-7L),0x3D99B012L};
            int i, j, k;
            if ((((safe_add_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_s(((((*p_18) || (safe_mod_func_int32_t_s_s((l_1974 < ((l_1975 == (l_1978 = g_1976)) | p_16)), (-9L)))) ^ ((safe_mod_func_int64_t_s_s((((*l_1987) = (l_1974 && (((!l_1974) , l_1986) != (void*)0))) | p_20.f0), (*l_1954))) , l_1988)) != 0xB326L), 4)) > 0x5AAB8EB0L), 0xE45F6ECCA1024933LL)) , l_1989) && l_1989))
            { /* block id: 943 */
                uint8_t *l_2015[3];
                int32_t l_2025 = (-7L);
                uint64_t l_2069 = 1UL;
                uint32_t *l_2096 = &g_1544[2][0][8];
                union U6 l_2103 = {-10L};
                union U7 ***l_2128 = (void*)0;
                union U7 ****l_2127 = &l_2128;
                int i;
                for (i = 0; i < 3; i++)
                    l_2015[i] = &g_746;
                (*l_1954) ^= (255UL < (p_20.f0 & ((*l_1987) |= 0xB93DL)));
                if ((safe_mod_func_int64_t_s_s((~(l_1989 = ((g_981[0][0][2] , ((safe_div_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_u((safe_add_func_uint16_t_u_u(p_20.f0, (safe_div_func_int16_t_s_s((((l_1989 & ((safe_div_func_int8_t_s_s((safe_add_func_int16_t_s_s((!(safe_sub_func_int16_t_s_s(((g_2009 = l_2008) != l_2010[7][0]), ((*l_1954) & (l_2013 == ((*l_1954) , (*g_239))))))), 0x2E11L)), 1UL)) == l_1974)) , l_1989) == 0x20L), p_20.f0)))), 6)) ^ 0xD876L), 0x0695L)) | l_1974)) ^ 0x76L))), (***g_1443))))
                { /* block id: 948 */
                    uint16_t *l_2021 = &g_228;
                    int64_t l_2022 = 0L;
                    uint64_t *****l_2051[6][7] = {{(void*)0,&g_1442,&g_1442,&g_1442,&g_1442,(void*)0,&g_1442},{&g_1442,&g_1442,(void*)0,&g_1442,&g_1442,&g_1442,(void*)0},{&g_1442,&g_1442,(void*)0,&g_1442,(void*)0,&g_1442,&g_1442},{&g_1442,&g_1442,&g_1442,&g_1442,&g_1442,&g_1442,(void*)0},{&g_1442,&g_1442,&g_1442,(void*)0,(void*)0,&g_1442,&g_1442},{(void*)0,&g_1442,&g_1442,(void*)0,&g_1442,&g_1442,&g_1442}};
                    int i, j;
                    if ((((safe_mod_func_int16_t_s_s(0x6982L, ((+(safe_add_func_uint32_t_u_u((((*l_2021) = ((**l_1975) = 0xB667L)) && ((l_2025 = ((g_528 , l_2022) >= (safe_lshift_func_int16_t_s_s(((*l_1987) = l_2025), 15)))) >= 0x0645D986L)), ((g_2027 = l_2026) == (void*)0)))) | ((void*)0 != (*g_382))))) , p_20.f0) , p_20.f0))
                    { /* block id: 954 */
                        return g_50[6];
                    }
                    else
                    { /* block id: 956 */
                        int32_t l_2030[10][5] = {{0x75C1533EL,0x75C1533EL,0x7C0FBF46L,0x7C0FBF46L,0x75C1533EL},{0L,0L,0L,0L,0L},{0x75C1533EL,0x7C0FBF46L,0x7C0FBF46L,0x75C1533EL,0x75C1533EL},{(-1L),0L,(-1L),0L,(-1L)},{0x75C1533EL,0x75C1533EL,0x7C0FBF46L,0x7C0FBF46L,0x75C1533EL},{0L,0L,0L,0L,0L},{0x75C1533EL,0x7C0FBF46L,0x7C0FBF46L,0x75C1533EL,0x75C1533EL},{(-1L),0L,(-1L),0L,(-1L)},{0x75C1533EL,0x75C1533EL,0x7C0FBF46L,0x7C0FBF46L,0x75C1533EL},{0L,0L,0L,0L,0L}};
                        union U8 * const *l_2042 = &g_1528;
                        union U8 * const **l_2043 = &l_2042;
                        int i, j;
                        l_1953[1][2] |= ((safe_rshift_func_uint8_t_u_u((l_2030[0][4] <= (((*l_1954) || 8L) | (safe_sub_func_uint8_t_u_u((safe_mod_func_int32_t_s_s((safe_rshift_func_int16_t_s_u(p_20.f0, (((safe_rshift_func_int8_t_s_s(((void*)0 != g_2039), 1)) & (((*l_2043) = l_2042) != l_2044)) & (+(*p_17))))), l_2030[4][1])), p_20.f0)))), p_16)) == 0x43D1BF8FB0C398F2LL);
                        l_1989 = ((safe_rshift_func_uint16_t_u_u(5UL, 5)) < ((((**l_1975) = (l_2048 , (safe_sub_func_uint64_t_u_u(9UL, ((void*)0 != l_2051[3][2]))))) , 0xCEL) < (safe_mod_func_uint8_t_u_u((((+(((((!(safe_mul_func_int8_t_s_s(((+(safe_mod_func_uint32_t_u_u(p_20.f0, 4L))) < (-7L)), l_2030[6][3]))) >= p_20.f0) && p_20.f0) == 0xEBL) || 0UL)) & l_2048.f0) | (*l_1954)), 1L))));
                        (*l_1954) = (safe_mod_func_int16_t_s_s(((void*)0 == l_2063), 65535UL));
                        (*l_1954) = (safe_div_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_s(0L, 2)) < ((((l_2069 ^ l_2022) || (l_2070[2][0][1] != &g_50[6])) < (g_2071 , p_16)) > (safe_div_func_int8_t_s_s(0L, (l_2030[0][4] || (*p_17)))))), 0x81A581993393ECC9LL));
                    }
                }
                else
                { /* block id: 964 */
                    union U4 *l_2079 = &g_283;
                    for (g_899.f1 = 6; (g_899.f1 != 13); g_899.f1 = safe_add_func_uint8_t_u_u(g_899.f1, 7))
                    { /* block id: 967 */
                        int8_t ***l_2077 = &l_2076;
                        if (p_20.f0)
                            break;
                        (**l_1947) = ((((*l_2077) = l_2076) != &p_17) < p_16);
                        l_2079 = l_2078;
                        if (l_2025)
                            goto lbl_2080;
                    }
                }
                if (((*p_18) < (((l_1974 < (~0xF79BL)) ^ (((((((g_1270[0].f1 | (((*l_1954) && (p_16 || 0x18L)) , (l_2082 == (*g_1316)))) <= p_20.f0) | l_1974) <= 1UL) | g_838) & p_16) <= p_20.f0)) && 0x33L)))
                { /* block id: 975 */
                    uint32_t ****l_2087 = &g_1435;
                    uint32_t *****l_2086 = &l_2087;
                    uint32_t *l_2097[7];
                    int32_t *l_2101 = (void*)0;
                    int32_t *l_2102 = &l_2025;
                    float *l_2113 = &g_770;
                    int i;
                    for (i = 0; i < 7; i++)
                        l_2097[i] = &g_1288;
                    (*l_2102) ^= (((safe_lshift_func_int16_t_s_s((0x6F8FL != (((0x6AB8D0EDL ^ l_2085) != ((((l_2086 != l_2088[1][1]) , (((((*g_152) = (safe_lshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u(((g_1623[1] , (((p_16 , l_2096) != l_2097[6]) < p_20.f0)) < 1UL), (*l_1954))), (*g_1612))), p_20.f0))) >= (*l_1954)) | (*g_241)) , 65529UL)) != 0xB73AL) != g_50[6].f0)) || p_20.f0)), g_1373)) , g_2098[4]) != (void*)0);
                    l_2114 = (l_2103 , (((*l_2113) = (((safe_div_func_float_f_f(p_16, p_16)) == (safe_mul_func_float_f_f(l_2048.f0, (safe_div_func_float_f_f((*g_1934), ((safe_sub_func_float_f_f(((**l_1947) = p_16), (p_20.f0 > (((((l_2112 , g_981[0][0][1]) , p_16) <= 0xC.8E543Fp+17) <= 0x7.Bp+1) > 0x1.7p-1)))) > g_161)))))) == l_1974)) <= 0x1.FE0345p+50));
                    for (g_167 = 0; (g_167 < (-27)); g_167 = safe_sub_func_int16_t_s_s(g_167, 8))
                    { /* block id: 983 */
                        volatile union U0 * volatile ***l_2122 = (void*)0;
                        (*g_2125) = g_2117;
                        if (l_2126)
                            continue;
                    }
                }
                else
                { /* block id: 987 */
                    (*l_1954) = p_16;
                }
                (*l_1954) ^= (&g_1937 == ((*l_2127) = &g_1937));
            }
            else
            { /* block id: 992 */
                int32_t *l_2129 = &g_7;
                int32_t *l_2130 = &l_1989;
                int32_t *l_2131 = &g_7;
                int32_t *l_2132[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int i;
                --g_2144;
            }
        }
    }
    return l_2147[5][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_951.f1 g_1338 g_1339 g_384 g_22 g_1623 g_1612 g_1613 g_364 g_295 g_1128 g_1166 g_1167 g_1168 g_1169 g_981 g_127 g_128 g_50.f1 g_152 g_115 g_919.f1 g_133 g_564 g_565 g_566 g_567 g_946.f0 g_925.f1 g_7 g_1694 g_132 g_239 g_240 g_241 g_144 g_964 g_1443 g_363 g_1048 g_950.f1 g_932.f0 g_1745 g_424 g_934.f1 g_1520 g_908.f1 g_1769 g_1771 g_1773 g_1031 g_1782 g_1772 g_1049 g_1050 g_1442 g_1802 g_1804 g_1527 g_1104 g_1831 g_1836 g_1838 g_528 g_1847 g_941 g_298 g_930.f1 g_949.f1 g_981.f0 g_1868 g_1883 g_1052 g_912.f1 g_1316 g_1317 g_1318 g_1053 g_198 g_1286 g_180 g_1931 g_1042 g_291 g_1934 g_328.f0
 * writes: g_7 g_328.f0 g_1613 g_295 g_903.f1 g_283 g_167 g_1520 g_115 g_228 g_919.f1 g_22 g_568 g_1694 g_843 g_1325 g_1128 g_424 g_908.f1 g_936.f1 g_907.f1 g_934.f1 g_770 g_298 g_1771 g_1775 g_1031 g_1802 g_964 g_1528 g_1339 g_912.f1 g_1318 g_198 g_194 g_422 g_1937
 */
static int8_t * func_24(int8_t * p_25, uint32_t  p_26, uint64_t  p_27, int8_t ** p_28, int8_t * const * p_29)
{ /* block id: 753 */
    int16_t * const l_1616[3][8][10] = {{{&g_1520[2],&g_1522,&g_1522,&g_1522,(void*)0,(void*)0,&g_1522,&g_1522,&g_1522,&g_1520[2]},{(void*)0,&g_1520[4],&g_1520[2],&g_198[3],&g_1522,&g_1522,(void*)0,&g_198[5],(void*)0,&g_1520[7]},{&g_198[2],(void*)0,&g_198[5],&g_1520[4],&g_1522,&g_198[2],(void*)0,&g_198[3],(void*)0,&g_1520[2]},{&g_1522,(void*)0,&g_1520[2],(void*)0,(void*)0,&g_1520[4],&g_198[2],&g_198[4],&g_1522,&g_198[4]},{&g_198[0],&g_198[3],&g_198[3],&g_198[5],&g_198[3],&g_198[3],&g_198[0],&g_1520[4],&g_1522,&g_198[3]},{&g_1520[2],(void*)0,&g_198[3],&g_198[0],&g_1520[2],&g_198[5],&g_198[2],&g_198[3],&g_1520[2],&g_1522},{(void*)0,&g_1520[7],&g_1520[2],&g_1522,&g_198[5],&g_198[3],&g_198[1],(void*)0,&g_1520[2],&g_198[5]},{&g_1522,(void*)0,&g_198[0],&g_198[3],(void*)0,&g_198[3],&g_198[3],&g_198[3],&g_198[3],(void*)0}},{{&g_198[3],&g_1522,&g_1522,&g_198[3],&g_1520[8],&g_198[0],&g_198[2],&g_198[3],(void*)0,&g_1520[4]},{&g_1520[7],&g_198[3],&g_198[2],(void*)0,&g_198[5],&g_1520[4],&g_1522,&g_198[2],(void*)0,&g_198[3]},{&g_198[5],&g_1522,(void*)0,&g_198[3],(void*)0,(void*)0,&g_1520[8],&g_198[5],&g_198[3],&g_1522},{&g_198[3],&g_198[5],&g_1520[2],&g_198[3],&g_1522,&g_1522,&g_1522,&g_198[3],&g_1520[2],&g_198[5]},{&g_1520[2],(void*)0,&g_198[1],&g_1522,&g_198[3],&g_198[4],(void*)0,(void*)0,&g_1520[2],&g_198[3]},{&g_1522,&g_1522,&g_1522,&g_198[0],&g_198[3],&g_198[4],&g_1522,(void*)0,&g_1520[8],&g_1520[7]},{&g_1520[2],&g_198[3],&g_198[3],(void*)0,(void*)0,&g_1522,&g_198[3],&g_198[1],&g_198[3],&g_198[3]},{&g_198[3],&g_1520[4],(void*)0,&g_1520[2],&g_1520[2],(void*)0,&g_1520[4],&g_198[3],&g_1520[7],&g_1520[8]}},{{&g_198[5],&g_198[2],(void*)0,&g_1522,&g_198[3],&g_1520[4],&g_1520[7],&g_198[0],&g_198[3],&g_1520[2]},{&g_1520[7],&g_1522,(void*)0,&g_198[3],&g_198[1],&g_198[0],&g_198[5],&g_198[3],&g_198[5],&g_1520[2]},{&g_198[3],(void*)0,(void*)0,&g_1520[8],&g_198[5],&g_198[3],&g_1522,&g_198[1],&g_1522,&g_198[3]},{&g_1522,&g_198[3],&g_198[3],&g_198[3],&g_1522,&g_198[3],&g_1520[2],(void*)0,&g_198[3],(void*)0},{(void*)0,&g_198[3],&g_1522,(void*)0,&g_1522,&g_198[5],&g_198[5],(void*)0,&g_1520[4],(void*)0},{&g_1522,(void*)0,&g_198[1],&g_198[4],&g_1522,(void*)0,&g_198[0],&g_198[3],(void*)0,&g_198[3]},{&g_198[1],(void*)0,&g_1520[2],&g_198[5],&g_198[5],&g_1522,&g_1522,&g_198[5],&g_198[5],&g_1520[2]},{(void*)0,(void*)0,(void*)0,&g_1522,&g_198[1],(void*)0,(void*)0,&g_198[2],&g_1522,&g_1520[2]}}};
    uint16_t l_1617[5][6][8] = {{{65526UL,0xFACFL,65535UL,0UL,65535UL,0xC50BL,0xDC07L,0x3B77L},{65530UL,65535UL,1UL,0UL,0x06A5L,0xFACFL,0x2296L,0xFACFL},{0x3B77L,65530UL,0x06A5L,65530UL,0x3B77L,65535UL,65526UL,0x2296L},{0xDC07L,65526UL,0xBCD9L,65530UL,0x6AD6L,0xAE6BL,9UL,65530UL},{0UL,0xA24AL,0xBCD9L,0xDC07L,65530UL,65535UL,65526UL,1UL},{0x6AD6L,0UL,0x06A5L,0x2296L,0xBCD9L,0xBCD9L,0x2296L,0x06A5L}},{{0xB5FAL,0xB5FAL,1UL,65526UL,65535UL,65530UL,0xDC07L,0xBCD9L},{1UL,0xC50BL,65530UL,9UL,0xAE6BL,0x6AD6L,65530UL,0xBCD9L},{0xC50BL,0x3475L,0x2296L,65526UL,65535UL,0x3B77L,65530UL,0x06A5L},{0UL,0x06A5L,0xFACFL,0x2296L,0xFACFL,0x06A5L,0UL,1UL},{0x2296L,0x68FEL,0x3B77L,0xDC07L,0xC50BL,65535UL,0UL,65530UL},{65535UL,0x3B77L,0UL,65530UL,0xC50BL,0xB5FAL,65535UL,0x2296L}},{{0x2296L,0UL,0x3475L,65530UL,0xFACFL,0UL,0UL,0xFACFL},{0UL,65530UL,65530UL,0UL,65535UL,0UL,0xBCD9L,0x3B77L},{0xC50BL,0xFACFL,0UL,0UL,0xAE6BL,1UL,0x68FEL,0UL},{1UL,0xFACFL,9UL,65535UL,65535UL,0UL,0xB5FAL,0x3475L},{0xB5FAL,65530UL,0xAE6BL,0UL,0xBCD9L,0UL,0xAE6BL,65530UL},{0x6AD6L,0UL,0x9837L,0xBCD9L,65530UL,0xB5FAL,65535UL,0UL}},{{0UL,0x3B77L,0UL,0x68FEL,0x6AD6L,65535UL,65535UL,9UL},{0xDC07L,0x68FEL,0x9837L,0xB5FAL,0x3B77L,0x06A5L,0xAE6BL,0xAE6BL},{0x3B77L,0x06A5L,0xAE6BL,0xAE6BL,0x06A5L,0x3B77L,0xB5FAL,0x9837L},{65530UL,0x3475L,9UL,65535UL,65535UL,0x6AD6L,0x68FEL,0UL},{0xA24AL,0xC50BL,0UL,65535UL,0xB5FAL,65530UL,0xBCD9L,0x9837L},{0x043FL,0xB5FAL,65530UL,0xAE6BL,0UL,0xBCD9L,0UL,0xAE6BL}},{{0x3475L,0UL,0x68FEL,65535UL,0x3475L,0x6AD6L,0xBCD9L,0UL},{65535UL,0x06A5L,65535UL,0xC50BL,0x3B77L,0x043FL,0x3475L,0xFACFL},{65535UL,0xA24AL,0x2296L,0UL,0x3475L,65530UL,0xFACFL,0UL},{0x68FEL,65535UL,0x9837L,65526UL,65526UL,0x9837L,65535UL,0x68FEL},{0xDC07L,9UL,0UL,0xBCD9L,65535UL,1UL,0UL,65535UL},{0x06A5L,0xB5FAL,65535UL,0x3475L,0xBCD9L,1UL,65530UL,0x2296L}}};
    int8_t *l_1620 = (void*)0;
    union U4 l_1633 = {0x90L};
    union U4 l_1642 = {8UL};
    int32_t *l_1653 = (void*)0;
    int32_t **l_1652 = &l_1653;
    int32_t ***l_1651 = &l_1652;
    int32_t l_1691 = 0x6F85B003L;
    int32_t l_1692 = 0xA44365E1L;
    int32_t *l_1697 = &g_964;
    union U6 l_1701 = {1L};
    int8_t l_1791 = 0x9EL;
    uint16_t **l_1806 = &g_152;
    uint16_t **l_1809 = &g_152;
    union U2 *l_1884[3][1];
    union U0 l_1918 = {0x70L};
    int32_t *l_1921 = &g_964;
    union U7 *l_1936 = &g_1059[4];
    union U7 **l_1935[4];
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_1884[i][j] = &g_440;
    }
    for (i = 0; i < 4; i++)
        l_1935[i] = &l_1936;
    if ((safe_mod_func_uint8_t_u_u(((1L >= (l_1616[0][3][8] != ((((g_951.f1 || l_1617[1][2][5]) < (safe_sub_func_uint64_t_u_u(((-1L) <= 0x4B75L), (0x30E6B737L >= 0L)))) & (l_1620 != (void*)0)) , l_1616[0][3][8]))) , l_1617[3][2][6]), 8UL)))
    { /* block id: 754 */
        int64_t *l_1632[1];
        int32_t l_1636[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        uint8_t l_1640 = 247UL;
        int32_t l_1672[5] = {1L,1L,1L,1L,1L};
        uint64_t *** const *l_1708 = &g_1443;
        uint32_t l_1730 = 0x2EABEB00L;
        int i;
        for (i = 0; i < 1; i++)
            l_1632[i] = (void*)0;
lbl_1698:
        (**g_1338) = p_26;
        for (g_328.f0 = 23; (g_328.f0 == 1); g_328.f0--)
        { /* block id: 758 */
            uint8_t *l_1625 = (void*)0;
            int32_t l_1626 = 0xF50681F7L;
            int64_t *l_1631 = &g_167;
            int64_t **l_1630[2];
            uint32_t *l_1634 = &g_903.f1;
            int32_t l_1635 = 0x7323C51BL;
            int32_t l_1687 = 0xA5882367L;
            int i;
            for (i = 0; i < 2; i++)
                l_1630[i] = &l_1631;
            if ((1L | ((((g_384[4] <= ((*p_25) || (((((*l_1634) = (((g_1623[0] , ((*g_364) ^= (((*g_1612) |= ((!l_1617[1][2][5]) | (l_1626 ^= 255UL))) && (+(0xB00B8E529E07862CLL == ((l_1632[0] = &g_1031) == (l_1633 , (void*)0))))))) && l_1626) <= p_26)) >= p_27) < p_26) != l_1635))) < (-9L)) , 1UL) != l_1636[4])))
            { /* block id: 764 */
                union U4 *l_1641[4][5] = {{&l_1633,&l_1633,&l_1633,&l_1633,&l_1633},{(void*)0,&l_1633,(void*)0,&l_1633,(void*)0},{&l_1633,&l_1633,&l_1633,&l_1633,&l_1633},{&l_1633,&l_1633,&l_1633,&l_1633,&l_1633}};
                uint64_t l_1650 = 0x986407DAE1808079LL;
                int32_t ****l_1654 = &l_1651;
                int32_t ***l_1655[1][9][4] = {{{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652},{&l_1652,&l_1652,&l_1652,&l_1652}}};
                uint16_t *l_1658 = &g_228;
                uint64_t l_1669 = 18446744073709551610UL;
                int32_t l_1688[9][5][5] = {{{(-2L),7L,0x9E285F76L,(-1L),8L},{0x43932BFAL,9L,(-10L),9L,0x43932BFAL},{5L,9L,0x54E4FA8FL,0L,1L},{1L,7L,0L,0x7DC1B9B0L,(-7L)},{0x59771DEAL,1L,(-2L),9L,1L}},{{0xE827906DL,0x7DC1B9B0L,0xA294D1C8L,(-3L),0x43932BFAL},{1L,0L,0xA294D1C8L,0L,8L},{(-2L),0xBDC3A0C0L,(-2L),(-1L),0x0867D919L},{0xD52B13EBL,9L,0L,(-1L),0x43932BFAL},{(-2L),(-1L),0x54E4FA8FL,0x7DC1B9B0L,0x20AEB76FL}},{{1L,0xBDC3A0C0L,(-10L),0x7DC1B9B0L,0x4F31EAA3L},{0xE827906DL,1L,0x9E285F76L,(-1L),1L},{0x59771DEAL,0L,0xA294D1C8L,(-1L),0xD52B13EBL},{1L,1L,0xE6EA4610L,0L,0x0867D919L},{5L,0xBDC3A0C0L,0x9E285F76L,(-3L),0x0867D919L}},{{0x43932BFAL,(-1L),0L,(-1L),0x8D15DBACL},{0L,(-1L),0xA5538EBCL,0x8C61458CL,0xA4EB5837L},{0x3005DF64L,0x536182CCL,0xED950FD1L,(-1L),(-1L)},{0L,7L,0x919649CFL,(-1L),0x3005DF64L},{0L,0x8C61458CL,0x57007D6AL,0x09BEF778L,(-3L)}},{{0x3005DF64L,1L,0xF772C9F2L,0x07CBBA2CL,7L},{0L,(-6L),0x919649CFL,0x09BEF778L,0xBC7C9981L},{(-3L),(-1L),(-1L),(-1L),(-3L)},{3L,(-1L),0x6B346AFEL,(-1L),0xA4EB5837L},{0xA4EB5837L,(-6L),0xED950FD1L,0x8C61458CL,0x954F8BF1L}},{{0L,1L,(-1L),(-1L),0xA4EB5837L},{0x43441F6BL,0x8C61458CL,0xF772C9F2L,0xBFBE06F0L,(-3L)},{0xA4EB5837L,7L,0xF772C9F2L,1L,0xBC7C9981L},{0L,0x536182CCL,(-1L),0x09BEF778L,7L},{0x8D15DBACL,(-1L),0xED950FD1L,0xDF0E33D1L,(-3L)}},{{0L,0xDF0E33D1L,0x6B346AFEL,0x8C61458CL,0x3005DF64L},{0xA4EB5837L,0x536182CCL,(-1L),0x8C61458CL,(-1L)},{0x43441F6BL,1L,0x919649CFL,0xDF0E33D1L,0xA4EB5837L},{0L,(-1L),0xF772C9F2L,0x09BEF778L,0x8D15DBACL},{0xA4EB5837L,1L,0x57007D6AL,1L,7L}},{{3L,0x536182CCL,0x919649CFL,0xBFBE06F0L,7L},{(-3L),0xDF0E33D1L,0xED950FD1L,(-1L),0x8D15DBACL},{0L,(-1L),0xA5538EBCL,0x8C61458CL,0xA4EB5837L},{0x3005DF64L,0x536182CCL,0xED950FD1L,(-1L),(-1L)},{0L,7L,0x919649CFL,(-1L),0x3005DF64L}},{{0L,0x8C61458CL,0x57007D6AL,0x09BEF778L,(-3L)},{0x3005DF64L,1L,0xF772C9F2L,0x07CBBA2CL,7L},{0L,(-6L),0x919649CFL,0x09BEF778L,0xBC7C9981L},{(-3L),(-1L),(-1L),(-1L),(-3L)},{3L,(-1L),0x6B346AFEL,(-1L),0xA4EB5837L}}};
                uint64_t *** const **l_1709 = &l_1708;
                uint8_t *l_1710 = &l_1640;
                int i, j, k;
                if ((!(safe_rshift_func_uint16_t_u_u(((*l_1658) = ((*g_152) = ((l_1640 , ((l_1642 = ((*g_1128) = l_1633)) , (((g_1520[4] = (safe_div_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((safe_add_func_int64_t_s_s(((l_1650 = (+((*****g_1166) |= l_1633.f0))) , ((((*g_1612) = ((*l_1634) = p_27)) || (*g_1612)) == (((*l_1654) = l_1651) == (g_1623[0] , l_1655[0][4][3])))), ((*l_1631) = (safe_rshift_func_uint16_t_u_u((g_981[0][0][2] , 0x0FD2L), 4))))), (**g_127))), 1UL))) || p_27) < (*g_152)))) && l_1633.f0))), l_1626))))
                { /* block id: 776 */
                    int32_t l_1689 = 1L;
                    for (g_919.f1 = 0; (g_919.f1 <= 5); g_919.f1 += 1)
                    { /* block id: 779 */
                        int i, j;
                        (*g_1339) ^= ((safe_mod_func_uint8_t_u_u((l_1636[g_919.f1] ^ ((safe_mul_func_uint16_t_u_u(p_26, (safe_mul_func_int16_t_s_s((l_1626 = (((***g_1168) = 0x765414A5A5653727LL) != l_1636[g_919.f1])), (safe_rshift_func_uint8_t_u_s(((safe_add_func_int16_t_s_s(((((*g_133) = l_1669) >= ((l_1688[5][2][3] ^= (safe_sub_func_int64_t_s_s(l_1672[3], ((safe_add_func_uint16_t_u_u(0x93CCL, ((!l_1633.f0) && ((safe_lshift_func_int8_t_s_u((~(safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s((((((l_1635 = ((****g_564) = ((((safe_lshift_func_uint8_t_u_s((p_26 < 0x42F5E0A7L), 0)) < 0L) ^ p_27) == p_26))) , 7UL) , 0xFEL) <= p_27) > l_1687), p_27)), 0x9DL))), 5)) | p_26)))) != g_946[4].f0)))) & 0xE3B9L)) | 9L), p_27)) && g_925.f1), (**g_127))))))) ^ l_1689)), l_1689)) > 0xDC36C424B7D28D50LL);
                        (*g_1339) = p_26;
                    }
                }
                else
                { /* block id: 789 */
                    int32_t *l_1690[10] = {&g_2,&g_7,(void*)0,&g_7,&g_2,&g_2,&g_7,(void*)0,&g_7,&g_2};
                    int i;
                    g_1694--;
                    l_1697 = &l_1636[4];
                    if (p_27)
                        goto lbl_1698;
                    (*g_1339) &= (-1L);
                }
                (**g_1338) ^= (safe_mul_func_uint8_t_u_u((l_1701 , (l_1688[5][2][3] &= ((*l_1710) ^= (safe_mod_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(((safe_div_func_uint64_t_u_u((((p_26 && ((((((0x998F65EADFAD63FDLL & (l_1642 , 1L)) , (void*)0) != ((*l_1709) = l_1708)) >= p_26) && (**g_132)) <= p_27)) && l_1687) <= (***g_239)), 0xE7B8B818D19CFA87LL)) > p_26), p_26)), l_1626))))), (*l_1697)));
            }
            else
            { /* block id: 799 */
                int64_t l_1719 = 0xD0A4939515D04D60LL;
                uint16_t **l_1721 = &g_152;
                uint16_t ***l_1720 = &l_1721;
                union U4 *l_1749[10][5] = {{&g_283,&l_1633,&g_283,&g_283,(void*)0},{&l_1633,&g_283,&g_283,&l_1633,&g_283},{(void*)0,&l_1633,&l_1633,&l_1633,(void*)0},{&g_283,&l_1633,&g_283,&g_283,&l_1633},{(void*)0,&g_283,&g_283,&l_1633,&g_283},{&l_1633,&l_1633,(void*)0,&l_1633,&l_1633},{&g_283,&l_1633,&g_283,&g_283,&g_283},{&g_283,(void*)0,(void*)0,&g_283,(void*)0},{&g_283,&g_283,(void*)0,&g_283,&g_283},{(void*)0,&g_283,(void*)0,(void*)0,&g_283}};
                int i, j;
                for (g_843 = 0; (g_843 == 34); g_843 = safe_add_func_uint64_t_u_u(g_843, 7))
                { /* block id: 802 */
                    int64_t l_1742 = 0xD033A7A9FA578339LL;
                    (**g_1338) &= (((safe_lshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(p_27, l_1719)), ((l_1720 != (void*)0) > (((l_1636[4] ^= (+(((((*l_1634) = (safe_sub_func_uint32_t_u_u((*g_241), ((l_1708 != (*g_1166)) && (safe_add_func_uint64_t_u_u(((***g_1443) = ((((((safe_mod_func_int8_t_s_s(((**g_132) ^= ((!((p_26 = ((***g_1443) <= g_1048[2])) > 0x7E6696FEL)) && l_1640)), 0xC5L)) || 2L) , l_1626) == p_27) ^ p_27) , 18446744073709551607UL)), l_1730)))))) | 4294967295UL) , g_950.f1) ^ l_1626))) & l_1730) == (*g_152))))), g_932[0].f0)) ^ l_1719) <= (*g_241));
                    for (g_1325 = (-3); (g_1325 <= (-1)); g_1325 = safe_add_func_int16_t_s_s(g_1325, 1))
                    { /* block id: 811 */
                        union U4 **l_1748 = &g_1128;
                        int32_t *l_1750[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1750[i] = &l_1636[3];
                        g_424 ^= (safe_lshift_func_uint16_t_u_s((safe_div_func_uint64_t_u_u((!(0x2DL ^ l_1719)), p_26)), (((safe_add_func_uint32_t_u_u(4294967288UL, ((*g_1339) = (safe_lshift_func_int16_t_s_u(l_1742, (+(((safe_unary_minus_func_uint32_t_u((g_1745 , (safe_rshift_func_int8_t_s_s((**p_29), (*l_1697)))))) | (((*l_1748) = &g_283) == l_1749[6][1])) <= 0xCCL))))))) ^ p_27) >= p_27)));
                    }
                }
            }
        }
    }
    else
    { /* block id: 819 */
        uint8_t l_1752 = 0xDBL;
        int32_t l_1755 = 0xD769248EL;
        int32_t l_1799 = 0x57BA5BF7L;
        uint16_t * const *l_1837 = &g_152;
        uint16_t **l_1839 = &g_152;
        for (p_27 = 0; (p_27 <= 5); p_27 += 1)
        { /* block id: 822 */
            uint64_t l_1770 = 0xCC01A45200D85E8FLL;
            const union U8 *l_1774 = (void*)0;
            const int16_t *l_1787[6][4][1] = {{{&g_198[3]},{(void*)0},{&g_198[3]},{(void*)0}},{{&g_198[3]},{(void*)0},{&g_198[3]},{(void*)0}},{{&g_198[3]},{(void*)0},{&g_198[3]},{(void*)0}},{{&g_198[3]},{(void*)0},{&g_198[3]},{(void*)0}},{{&g_198[3]},{(void*)0},{&g_198[3]},{(void*)0}},{{&g_198[3]},{(void*)0},{&g_198[3]},{(void*)0}}};
            int32_t l_1795[4] = {(-9L),(-9L),(-9L),(-9L)};
            uint8_t *l_1801[6] = {(void*)0,&g_172,(void*)0,(void*)0,&g_172,(void*)0};
            const float l_1867 = 0x0.56461Ep+52;
            uint32_t l_1885 = 6UL;
            int i, j, k;
            for (g_908.f1 = 0; (g_908.f1 <= 7); g_908.f1 += 1)
            { /* block id: 825 */
                for (g_936.f1 = 0; (g_936.f1 <= 5); g_936.f1 += 1)
                { /* block id: 828 */
                    uint64_t l_1766[8] = {0x650B3F83154F25A2LL,3UL,0x650B3F83154F25A2LL,0x650B3F83154F25A2LL,3UL,0x650B3F83154F25A2LL,0x650B3F83154F25A2LL,3UL};
                    int i;
                    for (g_907.f1 = 0; (g_907.f1 <= 2); g_907.f1 += 1)
                    { /* block id: 831 */
                        int32_t *l_1751 = &g_964;
                        l_1752--;
                    }
                    for (g_934.f1 = 0; (g_934.f1 <= 5); g_934.f1 += 1)
                    { /* block id: 836 */
                        int32_t *l_1756 = &l_1755;
                        int32_t *l_1757 = &g_964;
                        int32_t *l_1758 = &g_964;
                        int32_t *l_1759 = (void*)0;
                        int32_t *l_1760 = &g_7;
                        int32_t *l_1761 = &l_1692;
                        int32_t *l_1762 = &g_7;
                        int32_t *l_1763 = (void*)0;
                        int32_t *l_1764 = (void*)0;
                        int32_t *l_1765[7] = {&g_424,(void*)0,(void*)0,&g_424,(void*)0,(void*)0,&g_424};
                        int i;
                        ++l_1766[6];
                        (*g_1769) = (g_770 = (g_1520[(g_934.f1 + 2)] <= g_1520[(g_908.f1 + 1)]));
                    }
                }
                if (l_1770)
                    break;
                (*g_1773) = g_1771;
                g_1775 = l_1774;
            }
            if (p_26)
                continue;
            for (g_1031 = 2; (g_1031 >= 0); g_1031 -= 1)
            { /* block id: 849 */
                const int16_t **l_1788 = &l_1787[1][1][0];
                uint64_t *l_1796 = &l_1642.f1;
                uint64_t *l_1797 = (void*)0;
                uint64_t *l_1798 = &g_283.f1;
                int32_t l_1800 = 5L;
                uint16_t **l_1805 = &g_152;
                uint16_t **l_1808 = &g_152;
                uint16_t ***l_1807[9] = {&l_1808,&l_1808,&l_1808,&l_1808,&l_1808,&l_1808,&l_1808,&l_1808,&l_1808};
                uint64_t * const ***l_1850 = (void*)0;
                int32_t l_1886 = 0x9F77BA1BL;
                const int32_t **l_1887 = &g_1053;
                union U3 l_1930 = {0xB17ABE24L};
                int i;
                if (((safe_rshift_func_int16_t_s_u((((l_1755 && (safe_div_func_int64_t_s_s(((*l_1697) | (safe_rshift_func_uint8_t_u_u(g_1782, (safe_add_func_uint32_t_u_u(7UL, ((l_1799 ^= (safe_rshift_func_uint8_t_u_s((((((*l_1788) = l_1787[1][1][0]) != (*g_1771)) , ((safe_lshift_func_uint16_t_u_u(((((*l_1798) = ((*l_1796) = ((0xC8L && ((l_1791 != ((****g_1442) = ((((~(l_1795[2] &= ((safe_sub_func_int8_t_s_s((p_26 >= (*g_1049)), (**p_28))) , 0x5B1F9716L))) , (-8L)) | 255UL) >= l_1752))) & l_1755)) , (***g_1443)))) < p_26) > l_1770), l_1770)) <= 0x88C03A66L)) & (*g_1049)), 1))) || l_1800)))))), 4L))) ^ p_27) & (*g_152)), 4)) > l_1800))
                { /* block id: 856 */
                    if ((((l_1801[1] = p_25) == p_25) ^ 65526UL))
                    { /* block id: 858 */
                        if (p_26)
                            break;
                        (**g_1338) ^= 0x7CF05819L;
                        return (*g_132);
                    }
                    else
                    { /* block id: 862 */
                        (**g_1338) |= p_26;
                        if ((*l_1697))
                            break;
                        (*g_1804) = g_1802;
                        (*l_1697) = (*g_1049);
                    }
                }
                else
                { /* block id: 868 */
                    int i;
                    (*g_1527) = &g_941[g_1031];
                }
                if (((*g_1339) = ((l_1806 = l_1805) == (l_1809 = l_1805))))
                { /* block id: 874 */
                    int32_t l_1823[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1823[i] = (-6L);
                    if ((safe_div_func_uint64_t_u_u(((safe_add_func_uint64_t_u_u(p_26, (safe_mul_func_uint16_t_u_u((g_115[p_27] = p_26), (safe_rshift_func_int8_t_s_u((**p_29), (((+l_1800) == ((l_1752 <= l_1799) == (safe_lshift_func_int16_t_s_u(((safe_mod_func_int16_t_s_s(5L, (0xB27815EA87CB7B68LL ^ (g_1104[1][0][5] , (*l_1697))))) <= (*g_364)), 15)))) == (*g_133)))))))) > l_1823[0]), g_1031)))
                    { /* block id: 876 */
                        (**g_1338) |= 0xC8FF1096L;
                    }
                    else
                    { /* block id: 878 */
                        int32_t l_1830 = 0x9550FCE2L;
                        int32_t ** const *l_1832 = &g_1338;
                        int32_t *l_1835 = &l_1795[2];
                        (*g_1338) = &l_1823[1];
                        (*l_1835) &= ((*g_1339) = (safe_mul_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u((safe_add_func_int8_t_s_s(0xE2L, ((((l_1830 && p_27) >= (l_1823[1] >= (g_1831 , (0xD7L <= ((*g_133) = ((void*)0 == l_1832)))))) & (safe_lshift_func_int8_t_s_u(l_1755, 2))) < p_26))), 65532UL)) & 4294967295UL), (*l_1697))));
                    }
                }
                else
                { /* block id: 884 */
                    uint16_t l_1848[6][7] = {{0x9B2EL,65535UL,0x9B2EL,0x9B2EL,65535UL,0x9B2EL,0x9B2EL},{0x0D99L,0x0D99L,0x28ECL,0x0D99L,0x0D99L,0x28ECL,0x0D99L},{65535UL,0x9B2EL,0x9B2EL,65535UL,0x9B2EL,0x9B2EL,65535UL},{0xC7EFL,0x0D99L,0xC7EFL,0xC7EFL,0x0D99L,0xC7EFL,0xC7EFL},{65535UL,65535UL,0UL,65535UL,65535UL,0UL,65535UL},{0x0D99L,0xC7EFL,0xC7EFL,0x0D99L,0xC7EFL,0xC7EFL,0x0D99L}};
                    uint16_t ****l_1863 = &g_736[2];
                    const uint16_t * const **l_1914 = (void*)0;
                    int i, j;
                    if (((g_1836 , ((l_1837 != (l_1839 = g_1838)) && p_26)) != ((((safe_rshift_func_uint8_t_u_u((g_528 , (safe_unary_minus_func_uint16_t_u((safe_lshift_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s((l_1848[0][1] = (p_27 || ((void*)0 == g_1847[3]))), 11)), ((safe_unary_minus_func_int8_t_s((((*l_1697) , l_1850) == (void*)0))) > 0xC3L)))))), 2)) >= 0x7791136869CBE287LL) && 8UL) ^ p_26)))
                    { /* block id: 887 */
                        union U3 l_1862[7][1] = {{{7L}},{{7L}},{{7L}},{{7L}},{{7L}},{{7L}},{{7L}}};
                        int32_t l_1866 = 1L;
                        int i, j;
                        (*g_1338) = ((0x0A1A6EEAE9D68E1ALL || (!p_27)) , (l_1800 , (*g_1338)));
                        (*g_1868) = (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f((g_941[g_1031] , ((safe_sub_func_float_f_f(((0x7.E80680p+22 <= (((safe_mul_func_float_f_f(((*g_1769) < ((*p_25) , (l_1862[5][0] , ((l_1863 != &g_736[1]) <= (safe_mul_func_float_f_f(((((l_1848[3][5] >= ((l_1866 = p_26) >= l_1867)) != 0x0.9p-1) == g_930[0].f1) > p_26), p_27)))))), p_27)) == 0xB.EB1D3Ap+17) > g_949.f1)) != g_1694), l_1799)) != g_50[6].f1)), g_981[0][0][2].f0)), 0x0.7p+1)), p_26));
                    }
                    else
                    { /* block id: 891 */
                        (*l_1697) = ((safe_div_func_int16_t_s_s(p_27, ((0xE105C7E1L <= p_27) | p_27))) > (((**g_1338) ^= (safe_mul_func_uint8_t_u_u(p_27, ((((safe_add_func_uint16_t_u_u(((safe_unary_minus_func_int64_t_s(p_26)) || 4L), (((safe_sub_func_int16_t_s_s((safe_mod_func_uint8_t_u_u(((safe_div_func_int64_t_s_s((l_1885 = (+(l_1800 = (((18446744073709551614UL >= ((g_1883 , &g_180) != l_1884[1][0])) != g_295) | 0x0731L)))), 0xC278C2368CBD8606LL)) & l_1770), l_1886)), p_27)) || 0x3583L) <= p_27))) > g_1052) , l_1887) != (void*)0)))) == 4294967289UL));
                    }
                    (*l_1697) = ((*g_1339) = (safe_add_func_int64_t_s_s((safe_lshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s((**p_28), (*g_128))), 6)), ((*g_364) = l_1752))));
                    for (g_912.f1 = 0; (g_912.f1 <= 5); g_912.f1 += 1)
                    { /* block id: 902 */
                        const uint16_t * const ***l_1915 = &l_1914;
                        int32_t l_1916 = (-1L);
                        float *l_1917 = &g_194;
                        (*l_1697) = l_1848[0][1];
                        l_1800 = (**g_1338);
                        (*g_1317) = (**g_1316);
                        (*l_1917) = ((((((((safe_mul_func_uint8_t_u_u((((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_u((g_198[3] ^= ((p_26 == ((safe_div_func_int8_t_s_s((safe_add_func_uint8_t_u_u((p_26 || ((+(((*l_1796) = (safe_add_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(0x6AF615886D14D24ELL, (safe_div_func_uint16_t_u_u((1L <= (*l_1697)), p_27)))), (**l_1887)))) >= ((((safe_add_func_uint16_t_u_u((((!(((((*l_1915) = l_1914) == (void*)0) , &g_1771) != (void*)0)) < p_26) & 255UL), l_1848[0][2])) ^ 0x18FFL) <= (-1L)) > p_26))) >= l_1795[1])), 1L)), l_1916)) && p_26)) | p_27)), 9)), 11)) != l_1848[0][1]) & l_1795[2]), 0UL)) , p_26) >= p_26) == (**l_1887)) == (*g_1769)) > g_1286) == g_1782) <= 0xA.23F8D2p+45);
                    }
                    for (l_1691 = 2; (l_1691 >= 0); l_1691 -= 1)
                    { /* block id: 913 */
                        (*g_1339) |= ((*l_1697) ^= (((*g_1612) = (g_180 , (l_1918 , (safe_rshift_func_uint8_t_u_u(p_27, 0))))) <= p_27));
                        if (p_27)
                            continue;
                        l_1921 = ((*g_1338) = (*g_1338));
                    }
                }
                (*g_1934) = (safe_mul_func_float_f_f((safe_sub_func_float_f_f(p_27, (safe_sub_func_float_f_f((safe_mul_func_float_f_f(((p_27 != (((l_1930 , (g_1931 != (void*)0)) <= ((**l_1887) == ((p_27 , (g_1042 > (safe_mul_func_float_f_f((l_1799 < g_295), l_1752)))) > (-0x1.Cp+1)))) < p_27)) >= g_291), 0x3.Dp+1)), p_27)))), (*l_1921)));
                return (*g_132);
            }
        }
    }
    g_1937 = l_1935[1];
    return (*p_28);
}


/* ------------------------------------------ */
/* 
 * reads : g_402 g_1338 g_1339 g_1053 g_964 g_364 g_7 g_871 g_424 g_1434 g_1442 g_1166 g_1167 g_132 g_133 g_22 g_489.f1 g_152 g_115 g_1455 g_1473 g_1049 g_1050 g_928.f1 g_1031 g_440.f0 g_921.f1 g_198 g_1523 g_1527 g_771 g_1544 g_926.f1 g_1520 g_906.f1
 * writes: g_402 g_7 g_1339 g_291 g_295 g_489.f1 g_1432 g_424 g_1434 g_1166 g_964 g_198 g_1473 g_1520 g_1522 g_298 g_1544 g_926.f1 g_906.f1 g_115 g_1611
 */
static int16_t  func_40(int8_t  p_41)
{ /* block id: 12 */
    uint8_t l_1411 = 4UL;
    uint64_t l_1426 = 18446744073709551607UL;
    uint64_t ****l_1444 = &g_1443;
    int32_t l_1471 = 0x5A6ECE22L;
    uint64_t l_1529[4][1][8] = {{{0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL}},{{0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL}},{{0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL}},{{0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL,0x6612E93AE8D0B52DLL}}};
    int32_t l_1536 = 1L;
    int32_t l_1538 = 0x310766D8L;
    int32_t l_1542[9] = {0x5C23FF18L,0x5C23FF18L,0x5C23FF18L,0x5C23FF18L,0x5C23FF18L,0x5C23FF18L,0x5C23FF18L,0x5C23FF18L,0x5C23FF18L};
    int64_t *l_1556[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int i, j, k;
    if ((&g_22 != &p_41))
    { /* block id: 13 */
        int8_t *l_49 = &g_22;
        int8_t ** const l_48 = &l_49;
        const int32_t l_1410[3][9] = {{1L,0x70C9F9E4L,0L,0xB3F0756AL,0x70C9F9E4L,0xB3F0756AL,0L,0x70C9F9E4L,1L},{1L,0x70C9F9E4L,0L,0xB3F0756AL,0x70C9F9E4L,0xB3F0756AL,0L,0x70C9F9E4L,1L},{1L,0x70C9F9E4L,0L,(-1L),1L,(-1L),0L,1L,0x008EF536L}};
        int i, j;
    }
    else
    { /* block id: 665 */
        const union U4 l_1423 = {0x91L};
        int32_t l_1459 = 0x1677722BL;
        union U8 *l_1525 = &g_1526;
        union U8 **l_1524[6] = {&l_1525,&l_1525,&l_1525,&l_1525,&l_1525,&l_1525};
        int32_t l_1530 = (-1L);
        int32_t *l_1531 = &g_424;
        int32_t *l_1532 = &g_424;
        int32_t *l_1533 = &g_964;
        int32_t *l_1534 = &l_1471;
        int32_t *l_1535 = &g_964;
        int32_t *l_1537 = &g_964;
        int32_t *l_1539 = &g_424;
        int32_t *l_1540 = (void*)0;
        int32_t *l_1541[9];
        union U2 *l_1597[10][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
        int16_t *l_1601 = &g_1520[4];
        uint32_t *l_1602 = (void*)0;
        uint32_t *l_1603 = &g_906.f1;
        union U0 l_1606 = {0xE5L};
        uint32_t * const **l_1607 = (void*)0;
        uint32_t * const l_1610 = (void*)0;
        uint32_t * const *l_1609 = &l_1610;
        uint32_t * const **l_1608[3];
        int i, j;
        for (i = 0; i < 9; i++)
            l_1541[i] = &l_1459;
        for (i = 0; i < 3; i++)
            l_1608[i] = &l_1609;
        for (g_402 = 29; (g_402 <= 43); g_402++)
        { /* block id: 668 */
            uint8_t l_1431 = 1UL;
            int32_t l_1469 = 0xCD55F9B7L;
            uint32_t l_1480 = 0x8FEA3864L;
            union U2 *l_1495 = &g_180;
            union U2 **l_1494 = &l_1495;
            if (((**g_1338) = 0x197B4396L))
            { /* block id: 670 */
                (**g_1338) = (*g_1053);
            }
            else
            { /* block id: 672 */
                int64_t *l_1419 = &g_291;
                union U2 l_1420 = {18446744073709551615UL};
                uint32_t *l_1429 = (void*)0;
                uint32_t *l_1430 = &g_489.f1;
                int32_t *l_1433 = &g_424;
                int32_t l_1460 = 0L;
                int32_t l_1462 = 5L;
                int32_t l_1472[7][2][2] = {{{0L,0L},{(-7L),0L}},{{0L,(-7L)},{0L,0L}},{{(-7L),0L},{0L,(-7L)}},{{0L,0L},{(-7L),0L}},{{0L,(-7L)},{0L,0L}},{{(-7L),0L},{0L,(-7L)}},{{0L,0L},{(-7L),0L}}};
                int i, j, k;
                (*g_1338) = (*g_1338);
                (*l_1433) ^= (safe_mod_func_int8_t_s_s(((((*l_1419) = p_41) & ((((*g_364) = p_41) | (g_1432 = (((((((p_41 , l_1420) , (safe_lshift_func_int16_t_s_u((((l_1423 , (safe_sub_func_uint16_t_u_u((l_1426 , ((((*l_1430) = ((safe_add_func_int8_t_s_s((l_1426 != (4294967295UL && p_41)), 0xD1L)) > p_41)) <= 0xC3F71E26L) | (**g_1338))), p_41))) || g_871) | 0x84694EA1L), 12))) < l_1431) < p_41) | (-10L)) | l_1411) , p_41))) | 0x2290L)) <= p_41), 6L));
                if (((g_1434 = g_1434) == (void*)0))
                { /* block id: 680 */
                    if (p_41)
                        break;
                    g_1166 = &g_1167;
                }
                else
                { /* block id: 683 */
                    int32_t *l_1451[5][6] = {{(void*)0,&g_2,&g_964,&g_2,(void*)0,&g_424},{(void*)0,(void*)0,&g_2,&g_964,&g_2,&g_2},{(void*)0,&g_2,&g_2,(void*)0,&g_964,&g_2},{&g_964,&g_2,&g_2,&g_424,&g_964,&g_424},{&g_964,&g_964,&g_964,(void*)0,&g_964,(void*)0}};
                    int16_t *l_1452 = &g_198[3];
                    uint64_t ****l_1454 = (void*)0;
                    int i, j;
                    if (((((safe_rshift_func_uint16_t_u_s((((*l_1452) = (((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u((l_1431 , ((((l_1444 = g_1442) == (*g_1166)) || ((!p_41) > (safe_mod_func_uint32_t_u_u((safe_mul_func_int8_t_s_s((**g_132), (((*l_1430) ^= (safe_unary_minus_func_uint32_t_u((((*g_364) = 1UL) || (0x1EE47FACL == (*l_1433)))))) > (g_964 = ((((*g_1339) = p_41) || l_1431) != 3UL))))), 4294967295UL)))) <= l_1411)), 0x64994A17L)), l_1423.f0)) < 0xC9A406A9L) & l_1426)) , 0xA47DL), (*l_1433))) <= (*g_152)) || 65535UL) | (*l_1433)))
                    { /* block id: 690 */
                        uint64_t *****l_1453[10][1][3] = {{{&g_1442,(void*)0,&g_1442}},{{&g_1442,&g_1442,(void*)0}},{{&g_1442,(void*)0,(void*)0}},{{(void*)0,&l_1444,&g_1442}},{{&g_1442,&l_1444,&g_1442}},{{&g_1442,(void*)0,&g_1442}},{{&g_1442,&g_1442,(void*)0}},{{&g_1442,(void*)0,(void*)0}},{{(void*)0,&l_1444,&g_1442}},{{&g_1442,&l_1444,&g_1442}}};
                        uint64_t ** const **l_1457 = &g_1456;
                        int i, j, k;
                        (*l_1433) = ((((l_1454 = l_1444) != (l_1457 = g_1455)) <= (-0x1.Ap+1)) , (l_1431 != 1L));
                    }
                    else
                    { /* block id: 694 */
                        if (p_41)
                            break;
                        return p_41;
                    }
                }
                if ((**g_1338))
                { /* block id: 699 */
                    int32_t *l_1461 = &l_1459;
                    int32_t *l_1463 = &g_964;
                    int32_t *l_1464 = &g_424;
                    int32_t *l_1465 = (void*)0;
                    int32_t *l_1466 = &g_7;
                    int32_t *l_1467 = &l_1459;
                    int32_t *l_1468[10];
                    union U6 **l_1484 = (void*)0;
                    union U6 ***l_1483 = &l_1484;
                    int i;
                    for (i = 0; i < 10; i++)
                        l_1468[i] = &g_424;
                    ++g_1473;
                    if ((*g_1049))
                    { /* block id: 701 */
                        (**g_1338) ^= p_41;
                        return g_928.f1;
                    }
                    else
                    { /* block id: 704 */
                        float l_1478[1][6] = {{0x8.A1AFE2p+32,0x1.9p+1,0x1.9p+1,0x8.A1AFE2p+32,0x1.9p+1,0x1.9p+1}};
                        int32_t l_1479[10][7] = {{0L,7L,1L,9L,2L,0L,0L},{0xD81B2E04L,0L,0L,2L,9L,1L,7L},{0L,9L,0x0004EAF2L,0x93567FF6L,0x25A8B9C4L,1L,0x25A8B9C4L},{0x93567FF6L,7L,7L,0x93567FF6L,1L,0xD81B2E04L,(-1L)},{0L,0xE0E3F044L,1L,2L,0xE0E3F044L,(-1L),0L},{0x25A8B9C4L,7L,1L,9L,(-1L),1L,(-1L)},{(-5L),0L,0L,0x25A8B9C4L,2L,2L,0x25A8B9C4L},{0L,0x93567FF6L,0L,1L,7L,0x546B35A0L,7L},{7L,(-5L),1L,0xE0E3F044L,(-1L),0x0004EAF2L,0L},{9L,7L,1L,(-1L),2L,0x546B35A0L,9L}};
                        union U6 ****l_1485 = &l_1483;
                        int i, j;
                        (*g_1339) |= (safe_lshift_func_uint16_t_u_u((*l_1433), p_41));
                        --l_1480;
                        (*l_1485) = l_1483;
                        return g_1031;
                    }
                }
                else
                { /* block id: 710 */
                    union U2 *l_1493 = &g_981[0][0][2];
                    union U2 ** const l_1492 = &l_1493;
                    union U2 ***l_1496 = &l_1494;
                    int32_t l_1517 = 0xAFDA44A9L;
                    int16_t *l_1518 = &g_198[3];
                    int16_t *l_1519 = &g_1520[4];
                    int16_t *l_1521[4][2][6] = {{{(void*)0,&g_1522,(void*)0,(void*)0,(void*)0,&g_1522},{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522}},{{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522},{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522}},{{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522},{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522}},{{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522},{&g_1522,(void*)0,&g_1522,&g_1522,(void*)0,&g_1522}}};
                    int i, j, k;
                    (*g_1339) &= 0xC30B3575L;
                    (*g_1339) = (0x7A07L == (safe_add_func_int8_t_s_s((safe_sub_func_int16_t_s_s((safe_mod_func_int32_t_s_s(((l_1492 != ((*l_1496) = l_1494)) >= (safe_rshift_func_uint16_t_u_s((safe_mod_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((((*g_152) > (+p_41)) > 0xAEF37872L), (((safe_unary_minus_func_uint32_t_u(((((safe_mul_func_uint16_t_u_u((p_41 & (safe_lshift_func_uint8_t_u_u(((p_41 != (safe_rshift_func_int16_t_s_u((g_1522 = ((*l_1519) = ((*l_1518) ^= ((safe_mod_func_uint8_t_u_u((~(~(safe_rshift_func_int8_t_s_u(((l_1459 == 0xD9L) >= p_41), g_440.f0)))), l_1517)) || g_921.f1)))), 8))) == 0xDBL), g_1523))), 0x4B53L)) , l_1524[5]) != g_1527) && 249UL))) | l_1517) && 0UL))), 0x2711L)), 3))), p_41)), g_928.f1)), g_771[1][2][0])));
                    if ((*g_1053))
                        continue;
                    g_298 = 0x0.19863Ep-8;
                }
            }
            (**g_1338) |= 0xEBBB1F66L;
            (*g_1339) ^= l_1529[1][0][3];
        }
        --g_1544[4][2][4];
        for (g_926.f1 = 0; (g_926.f1 == 13); g_926.f1 = safe_add_func_int64_t_s_s(g_926.f1, 1))
        { /* block id: 727 */
            uint8_t *l_1553 = &g_283.f0;
            uint8_t *l_1554 = (void*)0;
            uint8_t *l_1555 = &g_1032;
            int32_t l_1561 = 0x284E3ED2L;
            int32_t l_1568 = 7L;
            union U2 l_1594 = {0xD23AD70CL};
        }
        l_1538 ^= ((((0L >= ((((*l_1601) |= (*l_1533)) != (0x6661L == l_1542[2])) >= ((*l_1603)--))) > ((l_1536 = (l_1606 , ((*g_152) = (*l_1531)))) | p_41)) & ((g_1611 = &l_1603) != (void*)0)) || 0xF2L);
    }
    return p_41;
}


/* ------------------------------------------ */
/* 
 * reads : g_22 g_7 g_50.f1 g_115 g_124 g_127 g_132 g_2 g_133 g_144 g_161 g_126 g_180 g_197 g_228 g_239 g_180.f0 g_172 g_241 g_283 g_295 g_301 g_328 g_339 g_227 g_328.f0 g_198 g_399 g_291 g_152 g_402 g_167 g_425 g_364 g_422 g_293 g_128 g_283.f0 g_495 g_528 g_543 g_360 g_498 g_424 g_440.f0 g_50 g_300 g_384 g_666 g_673 g_440 g_568 g_746 g_749 g_528.f2 g_838 g_843 g_871 g_744 g_891 g_736 g_894 g_528.f3 g_966 g_938.f1 g_975 g_964 g_985
 * writes: g_115 g_50.f1 g_124 g_126 g_144 g_152 g_161 g_167 g_172 g_194 g_197 g_198 g_228 g_180.f1 g_295 g_301 g_283.f0 g_339 g_298 g_360 g_363 g_328.f0 g_382 g_399 g_402 g_227 g_283.f2 g_425 g_291 g_300 g_293 g_133 g_128 g_543 g_564 g_424 g_673 g_736 g_746 g_749 g_528.f2 g_838 g_843 g_871 g_744 g_894 g_528.f3 g_966 g_975 g_7 g_964
 */
static uint32_t  func_42(int8_t ** const  p_43, int16_t  p_44, int8_t * p_45, const uint16_t  p_46, union U6  p_47)
{ /* block id: 14 */
    float l_66 = 0x3.6C8609p-72;
    int32_t l_67 = 0x55375BB7L;
    float l_70 = 0x5.71E957p-57;
    int32_t l_74[2];
    union U0 l_89 = {0x4EL};
    int8_t *l_134 = &g_22;
    int32_t *l_889 = &g_424;
    uint64_t l_890 = 0x372CA7BB5CFBE9D9LL;
    uint64_t *l_893 = &g_894;
    int32_t l_986 = 1L;
    uint64_t ***l_1017 = (void*)0;
    uint64_t *** const *l_1016 = &l_1017;
    int32_t l_1030 = 0x839E784DL;
    union U6 *l_1095 = &g_891;
    union U6 **l_1094 = &l_1095;
    uint32_t *l_1113 = (void*)0;
    uint32_t **l_1112 = &l_1113;
    uint32_t ***l_1111 = &l_1112;
    uint32_t ****l_1110 = &l_1111;
    union U8 *l_1232 = &g_1233;
    uint8_t l_1242 = 255UL;
    const uint32_t **l_1251 = &g_241;
    const uint32_t ***l_1250 = &l_1251;
    const uint32_t *** const *l_1249 = &l_1250;
    int32_t **l_1340 = &l_889;
    int32_t *l_1392 = (void*)0;
    int32_t **l_1391[6] = {&l_1392,&l_1392,&l_1392,&l_1392,&l_1392,&l_1392};
    uint16_t ****l_1394 = &g_736[0];
    union U4 * const l_1402 = &g_283;
    const uint32_t l_1409 = 1UL;
    int i;
    for (i = 0; i < 2; i++)
        l_74[i] = 0x2B50D22FL;
    l_986 |= (((safe_rshift_func_int8_t_s_u(0xCAL, 6)) , (safe_mod_func_uint64_t_u_u(((safe_sub_func_uint32_t_u_u(((func_57(func_61(l_67, p_47.f0, ((((safe_lshift_func_uint16_t_u_u(((**p_43) < (((*l_893) ^= (~(safe_sub_func_int8_t_s_s((l_74[0] = g_7), (func_75((((*l_889) = (safe_add_func_uint64_t_u_u(((*p_43) != ((*g_132) = func_83(l_89, func_90((l_67 || l_89.f0), &g_22), l_89.f0, l_89.f0, l_134))), l_67))) != l_890), l_67, (*p_43), g_891, (*p_43)) != 8UL))))) <= p_47.f1)), 4)) || 0x9EA3C6F19C642F79LL) < p_44) , 0xD5E242D22643D3D8LL), l_89), p_47.f0, p_44) , g_985) || p_47.f0), 0xADA6212FL)) != (**p_43)), l_890))) | 18446744073709551611UL);
    (*l_889) = 0xAEDA377FL;
    for (g_172 = 0; (g_172 <= 1); g_172 += 1)
    { /* block id: 443 */
        int32_t l_997 = 0x0DE03B2EL;
        uint32_t *l_998 = (void*)0;
        uint32_t *l_999 = &g_843;
        int32_t l_1000 = (-7L);
        int32_t ****l_1009 = &g_399;
        const int32_t *l_1051 = &g_1052;
        union U6 **l_1098[5][5][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_1095,&l_1095},{&l_1095,(void*)0},{&l_1095,&l_1095}},{{&l_1095,&l_1095},{&l_1095,&l_1095},{&l_1095,(void*)0},{&l_1095,&l_1095},{&l_1095,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_1095,&l_1095},{&l_1095,(void*)0}},{{&l_1095,&l_1095},{&l_1095,&l_1095},{&l_1095,&l_1095},{&l_1095,(void*)0},{&l_1095,&l_1095}},{{&l_1095,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_1095,&l_1095}}};
        union U7 *l_1181 = &g_1182;
        union U7 **l_1180 = &l_1181;
        uint16_t *l_1240 = &g_1043;
        uint64_t *l_1244 = &g_295;
        uint32_t **** const l_1254 = &l_1111;
        const union U7 *l_1269 = &g_1270[0];
        const union U7 **l_1268 = &l_1269;
        int8_t l_1275[5] = {0x80L,0x80L,0x80L,0x80L,0x80L};
        int8_t l_1369 = 0L;
        int i, j, k;
    }
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_975 g_22 g_7 g_964
 * writes: g_975 g_7 g_964
 */
static union U4  func_57(uint8_t  p_58, uint8_t  p_59, const uint32_t  p_60)
{ /* block id: 431 */
    int32_t *l_969 = (void*)0;
    int32_t *l_970 = (void*)0;
    int32_t *l_971 = &g_964;
    int32_t l_972 = (-8L);
    int32_t *l_973 = (void*)0;
    int32_t *l_974[7] = {&g_424,&g_424,&g_424,&g_424,&g_424,&g_424,&g_424};
    union U2 *l_978 = &g_180;
    union U2 **l_979[2][8] = {{&l_978,&l_978,&l_978,&l_978,&l_978,&l_978,&l_978,&l_978},{&l_978,&l_978,&l_978,&l_978,&l_978,&l_978,&l_978,&l_978}};
    union U2 *l_980[7][6] = {{&g_180,&g_180,&g_981[0][0][2],&g_440,&g_981[0][0][2],&g_180},{&g_981[0][0][2],&g_981[0][0][4],&g_440,&g_440,&g_981[0][0][4],&g_981[0][0][2]},{&g_180,&g_981[0][0][2],&g_440,&g_981[0][0][2],&g_180,&g_180},{&g_440,&g_981[0][0][2],&g_981[0][0][2],&g_440,&g_981[0][0][4],&g_440},{&g_440,&g_981[0][0][4],&g_440,&g_981[0][0][2],&g_981[0][0][2],&g_440},{&g_180,&g_180,&g_981[0][0][2],&g_440,&g_981[0][0][2],&g_180},{&g_981[0][0][2],&g_981[0][0][4],&g_440,&g_440,&g_981[0][0][4],&g_981[0][0][2]}};
    int32_t **l_982 = &l_974[4];
    int32_t **l_983 = &l_969;
    union U4 l_984 = {0x1EL};
    int i, j;
    g_975--;
    g_7 &= (((l_980[0][1] = l_978) != &g_981[0][0][2]) != g_22);
    (*l_983) = ((*l_982) = &l_972);
    (*l_971) &= 0x49A7B920L;
    return l_984;
}


/* ------------------------------------------ */
/* 
 * reads : g_528.f3 g_144 g_966 g_115 g_938.f1
 * writes: g_528.f3 g_194 g_966
 */
static uint8_t  func_61(uint8_t  p_62, int32_t  p_63, int64_t  p_64, union U0  p_65)
{ /* block id: 414 */
    int32_t *l_896 = &g_2;
    int32_t **l_895 = &l_896;
    union U8 *l_898[7][5][7] = {{{&g_926,&g_914,&g_933,&g_923[0][0][5],(void*)0,&g_935,&g_899},{&g_932[0],&g_908,(void*)0,&g_953,&g_903,&g_941[0],&g_951},{&g_914,&g_938,&g_933,&g_901,&g_955,&g_923[0][0][5],&g_921},{(void*)0,(void*)0,&g_932[0],&g_946[4],&g_930[0],&g_946[4],&g_932[0]},{&g_931[1][1],&g_931[1][1],(void*)0,&g_904,&g_937,&g_940,&g_914}},{{&g_903,(void*)0,&g_908,&g_900,&g_918,&g_910,&g_932[0]},{&g_907,(void*)0,&g_950,(void*)0,&g_937,&g_915,&g_926},{(void*)0,(void*)0,&g_908,&g_934,&g_930[0],&g_900,&g_956},{&g_907,&g_919,(void*)0,&g_915,&g_902,&g_915,(void*)0},{&g_903,&g_903,&g_932[0],&g_934,&g_906,&g_910,&g_944}},{{&g_931[1][1],&g_919,&g_938,(void*)0,&g_949,&g_940,(void*)0},{(void*)0,(void*)0,&g_920[2],&g_900,&g_906,&g_946[4],&g_956},{&g_919,(void*)0,&g_938,&g_904,&g_902,(void*)0,&g_926},{(void*)0,(void*)0,&g_932[0],&g_946[4],&g_930[0],&g_946[4],&g_932[0]},{&g_931[1][1],&g_931[1][1],(void*)0,&g_904,&g_937,&g_940,&g_914}},{{&g_903,(void*)0,&g_908,&g_900,&g_918,&g_910,&g_932[0]},{&g_907,(void*)0,&g_950,(void*)0,&g_937,&g_915,&g_926},{(void*)0,(void*)0,&g_908,&g_934,&g_930[0],&g_900,&g_956},{&g_907,&g_919,(void*)0,&g_915,&g_902,&g_915,(void*)0},{&g_903,&g_903,&g_932[0],&g_934,&g_906,&g_910,&g_944}},{{&g_931[1][1],&g_919,&g_938,(void*)0,&g_949,&g_940,(void*)0},{(void*)0,(void*)0,&g_920[2],&g_900,&g_906,&g_946[4],&g_956},{&g_919,(void*)0,&g_938,&g_904,&g_902,(void*)0,&g_926},{(void*)0,(void*)0,&g_932[0],&g_946[4],&g_930[0],&g_946[4],&g_932[0]},{&g_931[1][1],&g_931[1][1],(void*)0,&g_904,&g_937,&g_940,&g_914}},{{&g_903,(void*)0,&g_908,&g_900,&g_918,&g_910,&g_932[0]},{&g_907,(void*)0,&g_950,(void*)0,&g_937,&g_915,&g_926},{(void*)0,(void*)0,&g_908,&g_934,&g_930[0],&g_900,&g_956},{&g_907,&g_919,(void*)0,&g_915,&g_902,&g_915,(void*)0},{&g_903,&g_903,&g_932[0],&g_934,&g_906,&g_910,&g_944}},{{&g_931[1][1],(void*)0,(void*)0,&g_933,&g_954[3],&g_945,&g_907},{&g_943,&g_918,&g_925,(void*)0,&g_912,&g_951,&g_903},{(void*)0,&g_949,(void*)0,&g_909,&g_905,&g_933,&g_931[1][1]},{&g_943,(void*)0,(void*)0,&g_951,&g_936,&g_951,(void*)0},{&g_937,&g_937,&g_907,&g_909,&g_942,&g_945,&g_919}}};
    int32_t l_960 = 0x55A4503DL;
    int32_t l_961 = 0L;
    int32_t l_962 = 0L;
    int i, j, k;
    (*l_895) = &p_63;
    for (g_528.f3 = 0; (g_528.f3 <= 7); g_528.f3 += 1)
    { /* block id: 418 */
        float *l_897 = &g_194;
        int32_t *l_959[9] = {&g_424,&g_2,&g_424,&g_2,&g_424,&g_2,&g_424,&g_2,&g_424};
        int i;
        (*l_897) = 0x2.Ap-1;
        for (p_63 = 0; (p_63 <= 1); p_63 += 1)
        { /* block id: 422 */
            union U4 l_957 = {246UL};
            int32_t l_958 = (-5L);
            int32_t l_963 = 0x38BBD9ECL;
            int32_t l_965[2];
            int i;
            for (i = 0; i < 2; i++)
                l_965[i] = 0L;
            l_958 |= (((void*)0 == l_898[0][0][6]) == (l_957 , g_144[p_63]));
            l_959[6] = &l_958;
            g_966++;
            if (g_115[(p_63 + 5)])
                continue;
        }
        if (g_938.f1)
            continue;
    }
    return p_63;
}


/* ------------------------------------------ */
/* 
 * reads : g_736
 * writes: g_736
 */
static int32_t  func_75(uint32_t  p_76, uint8_t  p_77, int8_t * p_78, union U6  p_79, int8_t * p_80)
{ /* block id: 410 */
    uint16_t ****l_892 = &g_736[2];
    (*l_892) = g_736[3];
    return p_79.f1;
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_132 g_133 g_144 g_161 g_22 g_115 g_7 g_126 g_124 g_180 g_197 g_50.f1 g_228 g_239 g_180.f0 g_172 g_241 g_283 g_295 g_301 g_328 g_339 g_227 g_328.f0 g_198 g_399 g_291 g_152 g_402 g_167 g_425 g_364 g_422 g_293 g_127 g_128 g_283.f0 g_495 g_528 g_543 g_360 g_498 g_424 g_440.f0 g_50 g_300 g_384 g_666 g_673 g_440 g_568 g_746 g_749 g_528.f2 g_838 g_843 g_871 g_744
 * writes: g_126 g_144 g_152 g_161 g_167 g_172 g_124 g_194 g_197 g_198 g_228 g_180.f1 g_295 g_301 g_283.f0 g_339 g_115 g_298 g_360 g_363 g_328.f0 g_382 g_399 g_402 g_227 g_283.f2 g_425 g_291 g_300 g_293 g_133 g_50.f1 g_128 g_543 g_564 g_424 g_673 g_736 g_746 g_749 g_528.f2 g_838 g_843 g_871 g_744
 */
static int8_t * func_83(union U0  p_84, int8_t ** p_85, int8_t  p_86, float  p_87, int8_t * p_88)
{ /* block id: 25 */
    int32_t l_136 = 1L;
    int16_t l_169 = 8L;
    int32_t *l_174 = &l_136;
    int32_t l_317 = 1L;
    uint32_t *l_357 = &g_144[0];
    uint32_t ** const l_356 = &l_357;
    uint8_t l_359[4][3] = {{0x99L,0x99L,0x99L},{8UL,8UL,8UL},{0x99L,0x99L,0x99L},{8UL,8UL,8UL}};
    float l_421 = 0x0.Ep-1;
    float l_467 = 0x5.Dp+1;
    union U2 *l_481 = (void*)0;
    union U2 l_485 = {0x8D95F454L};
    uint8_t l_506 = 0xEDL;
    union U0 l_546[10][1][5] = {{{{0x47L},{0x3AL},{0x3AL},{0x47L},{0x11L}}},{{{0x47L},{-7L},{0x97L},{0x47L},{9L}}},{{{0x7CL},{-7L},{0x3AL},{0x7CL},{9L}}},{{{0x47L},{0x3AL},{0x3AL},{0x47L},{0x11L}}},{{{0x47L},{-7L},{0x97L},{0x47L},{9L}}},{{{0x7CL},{-7L},{0x3AL},{0x7CL},{9L}}},{{{0x47L},{0x3AL},{0x3AL},{0x47L},{0x11L}}},{{{0x47L},{-7L},{0x97L},{0x47L},{9L}}},{{{0x7CL},{-7L},{0x3AL},{0x7CL},{9L}}},{{{0x47L},{0x3AL},{0x3AL},{0x47L},{0x11L}}}};
    int32_t *** const l_572 = (void*)0;
    int32_t *** const *l_571 = &l_572;
    uint32_t l_588 = 0x3A273F51L;
    int16_t l_643[1][7][3] = {{{1L,1L,0L},{0xD846L,4L,0xD846L},{1L,0L,0L},{(-10L),4L,(-10L)},{1L,1L,0L},{0xD846L,4L,0xD846L},{1L,0L,0L}}};
    union U6 l_655[7][3][3] = {{{{0x670FE9FFL},{0x670FE9FFL},{0x19F7B850L}},{{1L},{0xD41F1FB9L},{0xABB091EEL}},{{0x23F37C98L},{0x8CBE5736L},{0x19F7B850L}}},{{{0x2133D7C5L},{0x2133D7C5L},{0xABB091EEL}},{{0x8CBE5736L},{0x23F37C98L},{0x19F7B850L}},{{0xD41F1FB9L},{1L},{0xABB091EEL}}},{{{0x670FE9FFL},{0x670FE9FFL},{0x19F7B850L}},{{1L},{0xD41F1FB9L},{0xABB091EEL}},{{0x23F37C98L},{0x8CBE5736L},{0x19F7B850L}}},{{{0x2133D7C5L},{0x2133D7C5L},{0xABB091EEL}},{{0x8CBE5736L},{0x23F37C98L},{0x19F7B850L}},{{0xD41F1FB9L},{1L},{0xABB091EEL}}},{{{0x670FE9FFL},{0x670FE9FFL},{0x19F7B850L}},{{1L},{0xD41F1FB9L},{0xABB091EEL}},{{0x23F37C98L},{0x8CBE5736L},{0x19F7B850L}}},{{{0x2133D7C5L},{0x2133D7C5L},{0xABB091EEL}},{{0x8CBE5736L},{0x23F37C98L},{0x19F7B850L}},{{0xD41F1FB9L},{1L},{0xABB091EEL}}},{{{0x670FE9FFL},{0x670FE9FFL},{0x19F7B850L}},{{1L},{0xD41F1FB9L},{0xABB091EEL}},{{0x23F37C98L},{0x8CBE5736L},{0x19F7B850L}}}};
    const float l_709 = 0x1.1p-1;
    uint16_t ***l_735 = (void*)0;
    int32_t l_739 = 4L;
    const int8_t **l_754 = &g_128;
    int32_t *l_841 = &g_424;
    int32_t *l_842[1];
    int16_t l_869 = (-1L);
    int8_t *l_888 = &g_22;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_842[i] = (void*)0;
    if (p_86)
    { /* block id: 26 */
        int32_t l_135 = 0x381DCF01L;
        uint16_t *l_147[1];
        uint16_t *l_153 = &g_115[7];
        uint16_t l_179 = 0x796FL;
        int32_t *l_215 = (void*)0;
        int32_t l_288 = (-7L);
        int32_t l_294 = 1L;
        union U3 l_308 = {0xAEDA4F9DL};
        uint64_t *l_362 = &g_295;
        uint64_t **l_361 = &l_362;
        int32_t **l_418 = (void*)0;
        union U2 *l_439 = &g_440;
        uint64_t ***l_510 = &l_361;
        int8_t *l_521[3];
        float *l_548 = &g_298;
        const float *l_549 = (void*)0;
        uint32_t **l_603 = (void*)0;
        uint32_t ***l_602 = &l_603;
        union U6 *l_625[6] = {&g_50[3],&g_50[3],&g_50[3],&g_50[3],&g_50[3],&g_50[3]};
        uint8_t l_632 = 255UL;
        int32_t * const *l_650 = (void*)0;
        int i;
        for (i = 0; i < 1; i++)
            l_147[i] = &g_115[0];
        for (i = 0; i < 3; i++)
            l_521[i] = (void*)0;
        if ((((g_2 && (p_86 ^ (((void*)0 == (*g_132)) >= 0xEE50L))) < 18446744073709551614UL) <= (l_135 , l_136)))
        { /* block id: 27 */
            int32_t *l_173 = &l_136;
            uint16_t *l_188 = (void*)0;
            float l_266[4] = {0xF.DB3593p-47,0xF.DB3593p-47,0xF.DB3593p-47,0xF.DB3593p-47};
            int64_t *l_271 = &g_167;
            int32_t l_292 = 0x9DFEE80CL;
            int i;
lbl_336:
            for (g_126 = 0; (g_126 <= 7); g_126 += 1)
            { /* block id: 30 */
                uint32_t *l_143 = &g_144[0];
                uint16_t **l_148 = (void*)0;
                uint16_t *l_149 = &g_115[7];
                uint16_t *l_151[8][9][3] = {{{&g_115[4],&g_115[7],&g_115[7]},{&g_115[7],&g_115[4],(void*)0},{&g_115[6],(void*)0,&g_115[7]},{&g_115[5],&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]},{(void*)0,&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],(void*)0},{&g_115[7],(void*)0,&g_115[7]},{(void*)0,&g_115[4],&g_115[3]}},{{&g_115[7],&g_115[7],&g_115[7]},{(void*)0,(void*)0,&g_115[7]},{(void*)0,&g_115[5],&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]},{&g_115[3],&g_115[6],&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]},{&g_115[4],&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]}},{{&g_115[7],&g_115[7],&g_115[3]},{&g_115[7],&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],(void*)0},{&g_115[6],&g_115[7],&g_115[7]},{&g_115[7],(void*)0,&g_115[7]},{&g_115[7],(void*)0,&g_115[7]},{&g_115[6],&g_115[4],&g_115[7]},{&g_115[7],&g_115[7],(void*)0},{&g_115[7],&g_115[3],&g_115[7]}},{{&g_115[7],&g_115[6],&g_115[6]},{&g_115[7],&g_115[7],(void*)0},{&g_115[7],&g_115[3],&g_115[4]},{&g_115[4],(void*)0,&g_115[7]},{&g_115[7],&g_115[6],&g_115[7]},{&g_115[3],(void*)0,&g_115[6]},{&g_115[7],&g_115[3],(void*)0},{(void*)0,&g_115[7],&g_115[4]},{(void*)0,&g_115[6],&g_115[7]}},{{&g_115[7],&g_115[3],&g_115[3]},{(void*)0,&g_115[7],&g_115[3]},{&g_115[7],&g_115[4],&g_115[7]},{&g_115[7],(void*)0,&g_115[7]},{(void*)0,(void*)0,&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]},{&g_115[5],&g_115[7],&g_115[3]},{&g_115[6],&g_115[7],&g_115[3]},{&g_115[7],&g_115[7],&g_115[7]}},{{&g_115[4],&g_115[7],&g_115[4]},{&g_115[7],&g_115[7],(void*)0},{&g_115[7],&g_115[7],&g_115[6]},{&g_115[0],&g_115[7],&g_115[7]},{&g_115[7],&g_115[6],&g_115[7]},{&g_115[0],&g_115[7],&g_115[4]},{&g_115[7],&g_115[5],(void*)0},{&g_115[7],(void*)0,&g_115[6]},{&g_115[4],&g_115[7],&g_115[7]}},{{&g_115[7],&g_115[4],(void*)0},{&g_115[6],(void*)0,&g_115[7]},{&g_115[5],&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],&g_115[7]},{(void*)0,&g_115[7],&g_115[7]},{&g_115[7],&g_115[7],(void*)0},{&g_115[7],(void*)0,&g_115[7]},{&g_115[3],&g_115[7],(void*)0},{&g_115[7],&g_115[7],(void*)0}},{{&g_115[7],&g_115[7],(void*)0},{&g_115[6],&g_115[7],&g_115[4]},{&g_115[7],&g_115[7],(void*)0},{(void*)0,&g_115[7],(void*)0},{&g_115[4],&g_115[7],(void*)0},{&g_115[7],&g_115[3],&g_115[4]},{&g_115[6],&g_115[7],(void*)0},{&g_115[7],&g_115[7],(void*)0},{&g_115[6],&g_115[4],(void*)0}}};
                uint16_t **l_150[9][8] = {{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]},{&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2],&l_151[0][3][2]}};
                uint32_t *l_157[10] = {&g_126,&g_126,&g_126,&g_126,&g_126,&g_126,&g_126,&g_126,&g_126,&g_126};
                uint32_t **l_156 = &l_157[7];
                uint8_t *l_160[3];
                int64_t *l_166 = &g_167;
                int32_t l_168 = 0x21F7AE59L;
                union U3 l_170 = {0x3E959B8CL};
                int32_t *l_171 = (void*)0;
                union U2 l_185 = {0x079A214AL};
                union U4 l_202 = {0xCAL};
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_160[i] = &g_161;
                g_172 = (safe_add_func_uint64_t_u_u((l_135 >= ((safe_sub_func_int16_t_s_s((safe_add_func_uint8_t_u_u(((((*l_143)--) != (l_147[0] != (l_153 = (g_152 = (l_149 = &g_115[7]))))) >= (safe_mod_func_int32_t_s_s((&g_126 != ((*l_156) = &g_126)), (safe_lshift_func_uint8_t_u_s((g_161++), (*p_88)))))), (((((p_84.f0 & p_84.f0) != (safe_add_func_int16_t_s_s(((((((((l_169 &= (((((*l_166) = ((p_86 || g_22) != g_2)) >= g_115[5]) != g_7) && l_168)) <= 0x68C9L) > (**p_85)) <= g_126) , g_115[6]) , l_170) , (void*)0) != &g_115[7]), 0xE0D9L))) , p_86) , p_86) != 1UL))), p_84.f0)) , p_86)), p_86));
                l_174 = l_173;
                for (g_124 = 7; (g_124 >= 0); g_124 -= 1)
                { /* block id: 43 */
                    uint32_t **l_252[10] = {&l_157[1],&l_157[1],&l_157[1],&l_157[1],&l_157[1],&l_157[1],&l_157[1],&l_157[1],&l_157[1],&l_157[1]};
                    int32_t l_253 = 0x2810AE63L;
                    int i;
                    if (p_86)
                        break;
                    for (l_168 = 0; (l_168 <= 9); l_168 += 1)
                    { /* block id: 47 */
                        float *l_193 = &g_194;
                        int32_t l_195 = 0x5FF7BA7BL;
                        float *l_196[3];
                        uint32_t ****l_199 = (void*)0;
                        uint32_t ****l_200 = (void*)0;
                        uint32_t ***l_201 = &l_156;
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_196[i] = (void*)0;
                        (*l_174) = p_84.f0;
                        g_198[3] = (safe_sub_func_float_f_f((g_197 = (((safe_add_func_float_f_f(l_179, 0x6.C32C6Dp+25)) == (g_180 , (safe_div_func_float_f_f(((safe_div_func_float_f_f((l_185 , ((*p_88) , (((*l_193) = (safe_add_func_float_f_f((&g_115[7] == (g_152 = l_188)), (safe_div_func_float_f_f((((safe_div_func_float_f_f((&l_188 != &l_147[0]), 0x9.1p+1)) > 0x6.6p+1) <= p_87), (-0x4.0p+1)))))) <= 0x1.Dp+1))), l_195)) != g_126), 0x5.1p-1)))) > g_115[7])), p_86));
                        if (l_135)
                            continue;
                        l_201 = &l_156;
                    }
                    for (l_169 = 7; (l_169 >= 0); l_169 -= 1)
                    { /* block id: 58 */
                        int32_t *l_209 = (void*)0;
                        int32_t *l_210 = &g_197;
                        float *l_216 = &l_202.f2;
                        int32_t *l_217 = &l_136;
                        int32_t *l_218 = (void*)0;
                        int32_t *l_219 = &l_136;
                        int32_t *l_220 = (void*)0;
                        int32_t *l_221 = (void*)0;
                        int32_t *l_222 = &l_168;
                        int32_t *l_223 = &l_168;
                        int32_t *l_224 = &l_168;
                        int32_t *l_225 = (void*)0;
                        int32_t *l_226[4][8][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
                        int i, j, k;
                        (*l_216) = (l_202 , (g_194 = ((safe_mul_func_float_f_f(p_84.f0, (p_87 = (0x8.363A23p+32 > (*l_174))))) < (((p_86 == (safe_lshift_func_uint8_t_u_s(((safe_sub_func_int16_t_s_s((((*l_210) ^= g_115[7]) , (safe_add_func_int64_t_s_s(((safe_mul_func_int8_t_s_s((((*l_143) = (((0xAE0DL ^ ((*p_88) < (l_215 != (void*)0))) < 0UL) >= g_50[6].f1)) ^ (-8L)), 7L)) , p_86), p_86))), 0x9B2AL)) == p_86), 3))) , 0xF.0D97DAp-93) == p_86))));
                        --g_228;
                        l_253 ^= (((safe_add_func_float_f_f((safe_sub_func_float_f_f(p_86, (safe_div_func_float_f_f(p_84.f0, (safe_mul_func_float_f_f((g_239 == g_239), 0x3.6p-1)))))), (safe_add_func_float_f_f(g_180.f0, (safe_div_func_float_f_f(((safe_mul_func_float_f_f(p_86, (safe_mul_func_float_f_f((((*l_216) = (*l_223)) == (safe_sub_func_float_f_f(((-0x4.1p+1) == g_228), 0xB.A9805Cp-61))), p_86)))) <= (*l_174)), g_172)))))) , (void*)0) == l_252[9]);
                        l_171 = l_221;
                    }
                }
            }
            for (g_161 = 0; (g_161 != 54); g_161++)
            { /* block id: 73 */
                float l_275 = (-0x1.Ep-1);
                int32_t l_280 = (-7L);
                int8_t l_299 = 0x90L;
                uint64_t *l_310 = &g_180.f1;
                uint64_t *l_311 = &g_180.f1;
                uint64_t *l_312 = &g_283.f1;
                uint64_t *l_313 = (void*)0;
                uint64_t *l_314 = &g_180.f1;
                uint64_t *l_315 = &g_180.f1;
                uint64_t *l_316[9][7][2] = {{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}},{{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1},{&g_283.f1,&g_283.f1}}};
                int32_t l_318 = (-8L);
                float *l_325 = (void*)0;
                float *l_326 = &l_266[2];
                union U0 l_327[4][6] = {{{0x97L},{0L},{3L},{0xC5L},{0xC5L},{3L}},{{7L},{7L},{0L},{0xC5L},{0L},{0xB6L}},{{0x97L},{0L},{9L},{0xB6L},{9L},{0L}},{{0xC5L},{0x97L},{9L},{0L},{7L},{0xB6L}}};
                int32_t **l_366 = &l_215;
                int i, j, k;
                for (g_197 = 16; (g_197 < (-4)); g_197--)
                { /* block id: 76 */
                    int32_t *l_261 = &g_124;
                    int32_t **l_260 = &l_261;
                    int32_t *l_263 = &g_124;
                    int32_t **l_262 = &l_263;
                    int32_t *l_265 = (void*)0;
                    int32_t **l_264 = &l_265;
                    int32_t l_270 = 1L;
                    uint8_t *l_272 = &g_172;
                    uint64_t *l_276 = (void*)0;
                    uint64_t *l_277[5];
                    int16_t *l_278[3];
                    int32_t l_279 = 0x55C4BC6AL;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_277[i] = &g_180.f1;
                    for (i = 0; i < 3; i++)
                        l_278[i] = &g_198[1];
                    (*l_174) = p_86;
                    if ((((*l_174) |= (((*l_262) = ((*l_260) = &l_135)) == ((*l_264) = l_173))) & (((*g_241) >= (safe_lshift_func_uint8_t_u_s(((*l_272) = (((safe_unary_minus_func_int8_t_s(l_270)) , (void*)0) != l_271)), 2))) >= ((l_279 = (g_198[0] = (safe_sub_func_uint64_t_u_u(((&g_152 != (void*)0) , ((g_180.f1 = p_86) || g_115[7])), 0xF0290027B1E6F47CLL)))) != l_280))))
                    { /* block id: 86 */
                        int32_t **l_281 = &l_173;
                        int16_t *l_282 = &g_198[3];
                        float *l_284[4][7] = {{(void*)0,&l_266[3],(void*)0,(void*)0,(void*)0,(void*)0,&l_266[3]},{&l_275,&l_266[1],&g_194,&g_194,&l_266[1],&l_275,&l_266[1]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_266[3],(void*)0,(void*)0},{&l_275,&l_275,&l_275,&g_194,&l_275,&l_275,&l_275}};
                        int i, j;
                        (*l_281) = &l_279;
                        (**l_281) |= (l_282 == (g_283 , l_188));
                        (*l_173) = (-0x1.7p+1);
                    }
                    else
                    { /* block id: 90 */
                        int32_t *l_285 = &l_280;
                        int32_t *l_286 = &l_279;
                        int32_t *l_287 = &l_279;
                        int32_t *l_289 = (void*)0;
                        int32_t *l_290[9][10][2] = {{{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279}},{{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0}},{{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279}},{{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0}},{{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279}},{{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0}},{{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279}},{{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0}},{{(void*)0,(void*)0},{&l_288,&l_279},{&l_288,(void*)0},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,&l_279},{(void*)0,(void*)0},{&l_270,(void*)0},{(void*)0,(void*)0},{&l_288,&l_279}}};
                        int i, j, k;
                        if ((*l_173))
                            break;
                        if (p_84.f0)
                            break;
                        --g_295;
                        --g_301;
                    }
                    return l_272;
                }
                (*l_326) = ((*l_174) == ((safe_add_func_float_f_f((safe_mul_func_float_f_f(l_299, (l_308 , 0x1.4BD559p-49))), (+((((g_295--) <= (safe_add_func_uint64_t_u_u((g_144[1] , (p_86 && (4294967295UL <= p_86))), ((~(safe_unary_minus_func_uint8_t_u((p_86 >= 255UL)))) ^ 0L)))) & 0x1CL) , p_87)))) <= g_124));
                if (l_299)
                { /* block id: 100 */
                    uint32_t *l_331 = (void*)0;
                    uint32_t *l_332 = (void*)0;
                    uint32_t *l_333 = (void*)0;
                    uint32_t *l_334 = &g_126;
                    uint8_t *l_335 = &g_283.f0;
                    (*l_174) = (((l_327[1][4] , (g_328 , (safe_lshift_func_int8_t_s_s(8L, 5)))) < 0xBE0902586CC2E19CLL) < (l_327[1][4] , ((*l_335) = (((*l_334) = (*g_241)) == g_2))));
                    if (g_126)
                        goto lbl_336;
                }
                else
                { /* block id: 105 */
                    uint32_t **l_358 = (void*)0;
                    int32_t **l_365 = &l_215;
                    if ((g_339 &= (((g_295 > p_86) > p_84.f0) | ((safe_div_func_int64_t_s_s(0x4575F8629F82947ELL, (*l_174))) != (g_126 &= (p_86 || (1L > p_86)))))))
                    { /* block id: 108 */
                        float *l_349 = &g_298;
                        const int32_t l_350 = (-1L);
                        g_360 = (~((*p_88) > ((--(*l_153)) <= (((safe_mul_func_int16_t_s_s((safe_rshift_func_int16_t_s_s((((safe_rshift_func_int16_t_s_s((((*l_173) = 8L) && p_86), 2)) != 0x0211L) ^ ((void*)0 == l_349)), 3)), l_350)) & (safe_lshift_func_int8_t_s_s((+(((((safe_mul_func_float_f_f(g_161, ((*l_349) = ((p_84 , l_356) == l_358)))) != 0x3.Bp-1) , g_227) & l_359[0][0]) , l_318)), 4))) > l_280))));
                    }
                    else
                    { /* block id: 113 */
                        g_363[1][0] = l_361;
                        return (*g_132);
                    }
                    (*l_365) = &l_318;
                }
                (*l_366) = (void*)0;
            }
        }
        else
        { /* block id: 121 */
            int32_t **l_367 = &l_215;
            int32_t l_404 = 0xAF958F17L;
            uint32_t l_406 = 7UL;
            union U8 *l_488 = &g_489;
            (*l_367) = (void*)0;
            if ((((*l_174) = g_180.f0) >= p_84.f0))
            { /* block id: 124 */
                const int32_t *l_381 = (void*)0;
                const int32_t **l_380[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t l_407 = 2L;
                int i;
                (*l_174) ^= ((void*)0 == &g_239);
                for (g_328.f0 = 0; (g_328.f0 == (-1)); g_328.f0 = safe_sub_func_uint64_t_u_u(g_328.f0, 8))
                { /* block id: 128 */
                    const int32_t ***l_385 = &l_380[1];
                    int32_t ****l_400 = &g_399;
                    int16_t *l_401[8][5][6] = {{{&l_169,&g_198[0],&l_169,&g_198[3],(void*)0,&g_198[4]},{&g_198[3],&g_198[0],&g_198[3],(void*)0,&l_169,&g_198[6]},{&g_198[3],&g_198[0],&l_169,&l_169,(void*)0,&l_169},{&g_198[3],&g_198[0],&g_198[3],(void*)0,&g_198[3],&l_169},{(void*)0,&l_169,&g_198[1],&l_169,&g_198[3],&g_198[1]}},{{&l_169,&g_198[3],&g_198[3],&g_198[3],&g_198[1],&g_198[3]},{&g_198[3],&g_198[3],(void*)0,(void*)0,(void*)0,&g_198[3]},{(void*)0,&l_169,(void*)0,&l_169,&g_198[3],&g_198[3]},{&l_169,&g_198[3],&g_198[3],&l_169,&g_198[3],&g_198[1]},{&l_169,&g_198[3],&g_198[1],&l_169,&l_169,&l_169}},{{&g_198[5],&l_169,&g_198[3],&l_169,&g_198[0],&g_198[4]},{&g_198[0],&l_169,&l_169,&l_169,&l_169,&g_198[3]},{&g_198[0],(void*)0,&g_198[3],&l_169,&g_198[4],&g_198[0]},{&g_198[0],(void*)0,&l_169,&l_169,&l_169,&l_169},{&g_198[5],(void*)0,&g_198[3],&l_169,&l_169,&l_169}},{{&l_169,&l_169,&l_169,&l_169,(void*)0,&l_169},{&l_169,&g_198[0],&l_169,&l_169,(void*)0,&g_198[0]},{(void*)0,&g_198[1],&g_198[2],(void*)0,(void*)0,&g_198[3]},{&g_198[3],&g_198[0],&g_198[3],&g_198[3],(void*)0,(void*)0},{&l_169,&l_169,&g_198[5],&l_169,&l_169,&l_169}},{{(void*)0,(void*)0,&g_198[3],(void*)0,&l_169,&g_198[3]},{&l_169,(void*)0,&l_169,(void*)0,&g_198[4],&l_169},{(void*)0,(void*)0,&l_169,&g_198[3],&l_169,&l_169},{&l_169,&l_169,&l_169,&g_198[3],&g_198[0],&g_198[3]},{&l_169,&l_169,&g_198[3],&g_198[3],&l_169,&l_169}},{{&g_198[3],&g_198[3],&g_198[5],&g_198[3],&g_198[3],(void*)0},{&l_169,&g_198[3],&g_198[3],(void*)0,&g_198[3],&g_198[3]},{&g_198[3],&l_169,&g_198[2],&l_169,(void*)0,&g_198[0]},{&g_198[3],&g_198[3],&l_169,(void*)0,&g_198[1],&l_169},{&l_169,&g_198[3],&l_169,&g_198[3],&g_198[3],&l_169}},{{&g_198[3],&l_169,&g_198[3],&g_198[3],&g_198[3],&l_169},{&l_169,&l_169,&l_169,&g_198[3],(void*)0,&g_198[0]},{&l_169,&g_198[4],&g_198[3],&g_198[3],&l_169,&g_198[3]},{(void*)0,&g_198[4],&l_169,(void*)0,(void*)0,&g_198[4]},{&l_169,&l_169,&g_198[3],(void*)0,&g_198[3],&l_169}},{{&g_198[3],&l_169,&g_198[3],&l_169,&l_169,&g_198[3]},{&g_198[3],&g_198[3],&g_198[2],&g_198[0],&g_198[3],&g_198[3]},{&g_198[1],&g_198[3],&l_169,&l_169,&g_198[3],&g_198[2]},{&g_198[3],&g_198[6],&l_169,&g_198[4],&g_198[3],&g_198[3]},{&g_198[2],(void*)0,&g_198[2],&l_169,&g_198[5],&g_198[3]}}};
                    int8_t *l_403 = &g_227;
                    float *l_405 = &g_194;
                    float *l_408 = &g_283.f2;
                    union U2 l_409 = {0x6EA70D4BL};
                    int64_t *l_423 = &g_167;
                    int i, j, k;
                    (*l_408) = (safe_div_func_float_f_f(((((safe_mul_func_float_f_f(((*l_405) = (safe_mul_func_float_f_f(((((safe_mul_func_float_f_f((((**p_85) , (safe_sub_func_float_f_f(g_328.f0, (((*l_385) = (g_382 = l_380[4])) != &l_381)))) <= ((p_87 > ((((-1L) && (*g_241)) == ((safe_lshift_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((!((safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(((*g_152) = ((safe_add_func_int16_t_s_s(g_198[4], (((*l_403) = (safe_lshift_func_int16_t_s_u((g_402 = (((*l_400) = g_399) == (void*)0)), 12))) & g_291))) | 0xEB4FL)), 1)), p_86)) < p_86)), 0xF6L)), 1)) != p_86)) , 0x3.D9820Ap-2)) < l_404)), (-0x1.Cp+1))) >= g_180.f0) < 0x1.0p-1) == g_124), (*l_174)))), g_197)) > p_84.f0) < l_406) < l_407), p_86));
                    if (g_402)
                        break;
                    (*l_174) |= ((&g_382 != (void*)0) & ((((l_409 , (safe_lshift_func_uint16_t_u_u((0L && (((safe_mod_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((p_84.f0 , (safe_div_func_uint32_t_u_u((g_144[1] < ((l_418 == &l_381) && ((*l_423) |= (safe_mul_func_int16_t_s_s((p_86 & l_409.f0), g_2))))), g_198[3]))), l_409.f0)), 0x1331BCE8L)) , (void*)0) == l_423)), p_84.f0))) , &l_407) != &l_294) != (*g_152)));
                }
                g_425--;
            }
            else
            { /* block id: 142 */
                uint32_t l_432 = 0x19BAF287L;
                int32_t *l_436[10] = {&l_294,&g_2,&g_2,&g_2,&l_294,&l_294,&g_2,&g_2,&g_2,&l_294};
                int i;
                for (l_135 = 0; (l_135 == 16); l_135 = safe_add_func_uint16_t_u_u(l_135, 4))
                { /* block id: 145 */
                    for (l_317 = 0; (l_317 > (-2)); l_317--)
                    { /* block id: 148 */
                        int8_t *l_433 = &g_328.f0;
                        if (l_432)
                            break;
                        return l_433;
                    }
                }
                for (g_197 = (-2); (g_197 != 4); g_197++)
                { /* block id: 155 */
                    (*l_367) = l_436[1];
                    return (*p_85);
                }
            }
            for (l_135 = 1; (l_135 >= 0); l_135 -= 1)
            { /* block id: 162 */
                float *l_437[1];
                int8_t *l_441 = &g_328.f0;
                float l_452 = (-0x3.8p-1);
                int i;
                for (i = 0; i < 1; i++)
                    l_437[i] = &g_194;
                g_283.f2 = g_115[(l_135 + 2)];
                for (g_227 = 0; (g_227 <= 0); g_227 += 1)
                { /* block id: 166 */
                    union U2 *l_438 = &g_180;
                    l_439 = l_438;
                    for (l_308.f3 = 0; (l_308.f3 <= 1); l_308.f3 += 1)
                    { /* block id: 170 */
                        return l_441;
                    }
                }
                for (g_291 = 1; (g_291 >= 0); g_291 -= 1)
                { /* block id: 176 */
                    uint8_t *l_449 = &l_359[2][0];
                    uint8_t *l_453 = &g_283.f0;
                    int8_t *l_468 = &g_293;
                    int i, j;
                    (*l_174) &= (((*l_441) = ((((void*)0 == &g_172) | g_115[(l_135 + 6)]) != ((safe_div_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_u(((g_115[(l_135 + 6)] <= (safe_mod_func_uint16_t_u_u((((((g_144[g_291] & (((g_115[(l_135 + 6)] & (l_437[0] != ((!((*l_453) = ((*l_449)++))) , &g_197))) , ((*g_364) = p_84.f0)) == p_86)) || 1UL) < 0xB9C9L) != p_84.f0) | g_22), g_115[(l_135 + 6)]))) > (*g_241)), g_144[g_291])) , 0x0EDE039BL), 0x044210E2L)) != p_86))) != (*g_133));
                    for (g_402 = 0; (g_402 <= 2); g_402 += 1)
                    { /* block id: 184 */
                        int i, j;
                        if (l_359[(l_135 + 2)][(g_291 + 1)])
                            break;
                        return (*g_132);
                    }
                    for (g_300 = 1; (g_300 >= 0); g_300 -= 1)
                    { /* block id: 190 */
                        p_87 = ((g_144[g_291] <= (safe_mul_func_float_f_f((0x4.6E2101p-61 >= (safe_sub_func_float_f_f((safe_mul_func_float_f_f(g_115[(l_135 + 2)], ((safe_div_func_float_f_f(((safe_add_func_float_f_f(g_115[(l_135 + 2)], ((safe_add_func_float_f_f(((p_84.f0 <= ((-(g_194 = l_135)) < (0x5.Ap-1 != l_467))) >= (p_87 < 0x4.32666Ep-56)), g_144[1])) == g_115[(l_135 + 2)]))) < (-0x1.8p-1)), p_86)) >= g_115[(l_135 + 6)]))), g_422[2][0]))), 0xD.862C61p+42))) == g_227);
                        return l_468;
                    }
                }
                for (l_406 = 0; (l_406 <= 7); l_406 += 1)
                { /* block id: 198 */
                    int32_t l_474 = 8L;
                    union U2 *l_479 = &g_180;
                    int i;
                    l_474 |= (+((safe_lshift_func_uint8_t_u_s((safe_mod_func_uint64_t_u_u(g_115[(l_135 + 5)], 4L)), 4)) ^ g_115[(l_135 + 4)]));
                    for (g_293 = 0; (g_293 >= 0); g_293 -= 1)
                    { /* block id: 202 */
                        union U0 *l_476 = (void*)0;
                        union U0 **l_475 = &l_476;
                        union U2 *l_480 = &g_180;
                        int64_t *l_486 = (void*)0;
                        int64_t *l_487 = &g_291;
                        union U8 **l_490 = &l_488;
                        int i;
                        if (g_115[g_293])
                            break;
                        (*l_475) = &g_328;
                        (*l_174) = ((((0x0DF19C7C3C239BF8LL & ((**p_85) > (safe_lshift_func_int8_t_s_s((l_479 != (l_481 = l_480)), (**g_127))))) != ((+(safe_div_func_int64_t_s_s((l_485 , ((*l_487) = ((0x1CC3L != ((p_86 < 2L) < p_84.f0)) | 248UL))), g_197))) < g_283.f0)) <= 0xA948151461E7F163LL) , g_115[7]);
                        (*l_490) = l_488;
                    }
                    (*l_367) = (*l_367);
                }
            }
        }
        if (l_308.f0)
            goto lbl_627;
lbl_627:
        for (g_161 = 0; (g_161 <= 2); g_161 += 1)
        { /* block id: 216 */
            const uint64_t *l_497 = &g_498;
            const uint64_t **l_496 = &l_497;
            uint64_t **l_499[10][4][6] = {{{&g_364,&g_364,&g_364,&l_362,&g_364,&g_364},{&l_362,&g_364,&g_364,&l_362,&g_364,&l_362},{&g_364,(void*)0,&l_362,&g_364,&l_362,(void*)0},{&g_364,&g_364,&l_362,&l_362,&l_362,&g_364}},{{(void*)0,(void*)0,(void*)0,&g_364,&g_364,&l_362},{(void*)0,&g_364,&g_364,&g_364,&g_364,(void*)0},{&l_362,&g_364,&g_364,&g_364,(void*)0,&l_362},{&l_362,&g_364,&g_364,&l_362,(void*)0,(void*)0}},{{(void*)0,&g_364,&g_364,&g_364,&g_364,&g_364},{(void*)0,&l_362,&l_362,&g_364,&g_364,&g_364},{&l_362,&g_364,&l_362,&g_364,(void*)0,&l_362},{&l_362,(void*)0,&g_364,&l_362,&l_362,&l_362}},{{&g_364,&g_364,&g_364,&g_364,(void*)0,&l_362},{&g_364,&l_362,&g_364,&g_364,&g_364,&g_364},{&g_364,&g_364,&l_362,(void*)0,&g_364,(void*)0},{(void*)0,&g_364,(void*)0,(void*)0,&g_364,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_364,&l_362,&l_362,(void*)0,&g_364},{&l_362,&l_362,&l_362,&g_364,&l_362,&g_364},{&l_362,&g_364,&g_364,&g_364,&g_364,&l_362}},{{&g_364,(void*)0,&l_362,(void*)0,&g_364,&g_364},{(void*)0,&l_362,(void*)0,&g_364,&l_362,&g_364},{&l_362,&g_364,(void*)0,&l_362,&g_364,&g_364},{&g_364,&l_362,(void*)0,&g_364,&l_362,&g_364}},{{&g_364,&g_364,&l_362,&l_362,&g_364,&l_362},{&g_364,&g_364,(void*)0,&l_362,&l_362,(void*)0},{&g_364,&g_364,&g_364,&l_362,&g_364,(void*)0},{&g_364,&g_364,&g_364,(void*)0,&l_362,&g_364}},{{&g_364,&g_364,&l_362,&g_364,&g_364,&l_362},{&l_362,&g_364,&l_362,&l_362,&l_362,&g_364},{(void*)0,&g_364,&g_364,&l_362,&g_364,&g_364},{(void*)0,(void*)0,&l_362,&g_364,&l_362,&l_362}},{{&l_362,&g_364,&l_362,(void*)0,(void*)0,&g_364},{&l_362,(void*)0,(void*)0,&g_364,&l_362,&g_364},{(void*)0,(void*)0,&g_364,&g_364,&g_364,&l_362},{&g_364,&l_362,&l_362,&g_364,&g_364,&g_364}},{{&l_362,(void*)0,&l_362,(void*)0,(void*)0,&g_364},{&l_362,&g_364,&g_364,(void*)0,&g_364,(void*)0},{&g_364,&l_362,&g_364,&g_364,&l_362,&l_362},{(void*)0,&g_364,(void*)0,(void*)0,&g_364,(void*)0}}};
            int32_t l_501 = (-1L);
            const int8_t *l_525 = &g_227;
            uint32_t ****l_621 = &l_602;
            int i, j, k;
            for (l_135 = 2; (l_135 >= 0); l_135 -= 1)
            { /* block id: 219 */
                uint64_t ***l_500 = &l_361;
                union U4 l_505 = {0x44L};
                int32_t *l_531 = &l_317;
                int32_t l_589 = 2L;
                uint16_t **l_590 = &l_147[0];
                int32_t *l_601[10] = {&g_300,&g_300,&l_135,&g_300,&g_300,&g_300,&g_300,&l_135,&g_300,&g_300};
                int i, j;
                if ((((-0x1.5p-1) >= ((0x4.8p+1 < (safe_div_func_float_f_f(l_359[l_135][g_161], (safe_div_func_float_f_f(((l_496 = g_495[0]) != ((*l_500) = l_499[5][0][2])), (l_501 < (safe_mul_func_float_f_f(((*l_174) >= (-(l_505 , (&g_144[1] == (void*)0)))), (-0x5.Ep-1))))))))) >= l_506)) , p_86))
                { /* block id: 222 */
                    int64_t l_542 = 0x5822A9634C159B5BLL;
                    int32_t *** const *l_569[2][1];
                    int32_t l_610[8][6] = {{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL},{0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL,0x9554127FL}};
                    const int32_t *l_612 = &l_288;
                    const int32_t **l_611 = &l_612;
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_569[i][j] = &g_399;
                    }
                    if ((l_501 = p_86))
                    { /* block id: 224 */
                        uint16_t l_507[5] = {0x8E14L,0x8E14L,0x8E14L,0x8E14L,0x8E14L};
                        uint64_t ****l_511 = &l_500;
                        int i;
                        ++l_507[1];
                        (*l_511) = l_510;
                    }
                    else
                    { /* block id: 227 */
                        uint32_t l_519 = 1UL;
                        int8_t *l_524 = &g_50[6].f1;
                        int32_t **l_532 = &l_174;
                        int32_t *l_533 = &l_317;
                        int32_t l_534[4][6][4] = {{{0x0BA81096L,0L,0xC7AFDE39L,0x0BA81096L},{(-6L),0L,0L,(-6L)},{(-6L),0xC7AFDE39L,0xC7AFDE39L,(-6L)},{0x0BA81096L,0L,0xC7AFDE39L,0x0BA81096L},{(-6L),0L,0L,(-6L)},{(-6L),0xC7AFDE39L,0xC7AFDE39L,(-6L)}},{{0x0BA81096L,0L,0xC7AFDE39L,0x0BA81096L},{(-6L),0L,0L,(-6L)},{(-6L),0xC7AFDE39L,0xC7AFDE39L,(-6L)},{0x0BA81096L,0L,0xC7AFDE39L,1L},{0x0BA81096L,0xC7AFDE39L,0L,0x0BA81096L},{0x0BA81096L,0L,0L,0x0BA81096L}},{{1L,0xC7AFDE39L,0L,1L},{0x0BA81096L,0xC7AFDE39L,0L,0x0BA81096L},{0x0BA81096L,0L,0L,0x0BA81096L},{1L,0xC7AFDE39L,0L,1L},{0x0BA81096L,0xC7AFDE39L,0L,0x0BA81096L},{0x0BA81096L,0L,0L,0x0BA81096L}},{{1L,0xC7AFDE39L,0L,1L},{0x0BA81096L,0xC7AFDE39L,0L,0x0BA81096L},{0x0BA81096L,0L,0L,0x0BA81096L},{1L,0xC7AFDE39L,0L,1L},{0x0BA81096L,0xC7AFDE39L,0L,0x0BA81096L},{0x0BA81096L,0L,0L,0x0BA81096L}}};
                        int32_t *l_535 = &l_294;
                        int32_t *l_536 = &l_501;
                        int32_t *l_537 = &l_288;
                        int32_t *l_538 = &l_288;
                        int32_t *l_539 = &l_294;
                        int32_t *l_540 = &l_534[3][4][3];
                        int32_t *l_541[6][9][4] = {{{&l_294,&l_534[0][3][1],(void*)0,(void*)0},{&g_424,&g_424,&l_534[0][3][1],&l_534[3][0][1]},{(void*)0,&g_424,&g_2,&l_317},{&g_2,&l_317,&l_294,&g_2},{&g_424,&l_317,&g_7,&l_317},{&l_317,&g_424,(void*)0,&l_534[3][0][1]},{(void*)0,&g_424,&l_294,(void*)0},{(void*)0,&l_534[0][3][1],&l_501,&l_317},{(void*)0,&l_294,&l_294,(void*)0}},{{(void*)0,&l_317,(void*)0,&l_294},{&l_317,&l_534[0][3][1],&g_7,&l_534[3][0][1]},{&g_424,(void*)0,&l_294,&l_534[3][0][1]},{&g_2,&l_534[0][3][1],&g_2,&l_294},{(void*)0,&l_317,&l_534[0][3][1],(void*)0},{&g_424,&l_294,(void*)0,&l_317},{&l_294,&l_534[0][3][1],(void*)0,(void*)0},{&g_424,&g_424,&l_534[0][3][1],&l_534[3][0][1]},{(void*)0,&g_424,&g_2,&l_317}},{{&g_2,&l_317,&l_294,&g_2},{&g_424,&l_317,&g_7,&l_317},{&l_317,&g_424,(void*)0,&l_534[3][0][1]},{(void*)0,&g_424,&l_294,(void*)0},{(void*)0,&l_534[0][3][1],&l_501,&l_317},{(void*)0,&l_294,&l_294,(void*)0},{(void*)0,&l_317,(void*)0,&l_294},{&l_317,&l_534[0][3][1],&g_7,&l_534[3][0][1]},{&g_424,(void*)0,&l_294,&l_534[3][0][1]}},{{&g_2,&l_534[0][3][1],&g_2,&l_294},{(void*)0,&l_317,&l_534[0][3][1],(void*)0},{&g_424,&l_294,(void*)0,&l_317},{&l_294,&l_534[0][3][1],(void*)0,(void*)0},{&g_424,&g_424,&l_534[0][3][1],&l_534[3][0][1]},{(void*)0,&g_424,&g_2,&l_317},{&g_2,&g_2,&l_501,&g_7},{&l_294,&g_2,&l_534[0][3][1],&g_2},{&g_2,&l_294,&l_534[0][3][1],&g_424}},{{&l_534[0][3][1],&l_294,&l_501,&l_534[0][3][1]},{(void*)0,(void*)0,&l_534[3][0][1],&g_2},{(void*)0,&l_501,&l_501,(void*)0},{&l_534[0][3][1],&g_2,&l_534[0][3][1],&l_501},{&g_2,(void*)0,&l_534[0][3][1],&g_424},{&l_294,&l_534[0][3][1],&l_501,&g_424},{&g_7,(void*)0,&g_7,&l_501},{(void*)0,&g_2,(void*)0,(void*)0},{&l_294,&l_501,&l_534[0][3][1],&g_2}},{{&l_501,(void*)0,&l_534[0][3][1],&l_534[0][3][1]},{&l_294,&l_294,(void*)0,&g_424},{(void*)0,&l_294,&g_7,&g_2},{&g_7,&g_2,&l_501,&g_7},{&l_294,&g_2,&l_534[0][3][1],&g_2},{&g_2,&l_294,&l_534[0][3][1],&g_424},{&l_534[0][3][1],&l_294,&l_501,&l_534[0][3][1]},{(void*)0,(void*)0,&l_534[3][0][1],&g_2},{(void*)0,&l_501,&l_501,(void*)0}}};
                        int i, j, k;
                        (*l_532) = (((safe_sub_func_int16_t_s_s((0xF7L || (safe_sub_func_uint64_t_u_u((safe_mul_func_int8_t_s_s(((*l_524) ^= (~(l_519 , (!(((*g_132) = l_521[2]) == (((**l_356)++) , l_524)))))), (l_501 && (((*g_127) = l_525) == (void*)0)))), (+((~((g_528 , (safe_mul_func_uint8_t_u_u(g_115[7], l_519))) ^ p_84.f0)) , l_501))))), p_84.f0)) & 0x2648E4C6L) , l_531);
                        g_543++;
                    }
                    if (((p_86 >= (l_546[4][0][0] , (!((l_548 = l_215) != (p_84 , l_549))))) == (l_359[(g_161 + 1)][g_161]++)))
                    { /* block id: 237 */
                        return (*g_132);
                    }
                    else
                    { /* block id: 239 */
                        int32_t * const l_563 = &l_135;
                        int32_t * const *l_562 = &l_563;
                        int32_t * const **l_561 = &l_562;
                        int32_t * const ***l_560 = &l_561;
                        int32_t *** const **l_570[5];
                        uint8_t *l_573 = &l_506;
                        uint16_t ***l_591 = &l_590;
                        int32_t *l_600 = (void*)0;
                        int64_t *l_608 = &l_542;
                        int64_t *l_609[4] = {&g_167,&g_167,&g_167,&g_167};
                        int i;
                        for (i = 0; i < 5; i++)
                            l_570[i] = (void*)0;
                        l_589 &= (safe_mod_func_int32_t_s_s((safe_div_func_int64_t_s_s(p_86, (~((safe_add_func_int16_t_s_s(((~((*g_241) , ((g_564 = l_560) != (l_571 = l_569[1][0])))) & ((++(*l_573)) & (l_588 ^= (((safe_lshift_func_uint16_t_u_s((safe_mul_func_int8_t_s_s(((*g_128) <= (255UL ^ (safe_div_func_uint16_t_u_u((safe_sub_func_int16_t_s_s((safe_lshift_func_int16_t_s_s(((1L >= ((safe_rshift_func_int16_t_s_u(0x0B2AL, (g_360 && p_86))) && 0x3EL)) && (*l_531)), 1)), p_86)), g_328.f0)))), (*l_174))), 12)) && p_86) < 0x8B3160A0A59EF8E0LL)))), 0xF60AL)) || 0L)))), 4294967290UL));
                        (*l_591) = l_590;
                        g_424 = (p_84.f0 == ((((((safe_add_func_uint32_t_u_u(l_501, (((safe_lshift_func_int8_t_s_u(0x9AL, (((safe_mul_func_int16_t_s_s(g_339, (((l_610[0][1] = (((*l_608) ^= ((safe_mul_func_int32_t_s_s((l_600 != (l_601[3] = &g_197)), (l_602 != (void*)0))) || (((((*l_362)--) && (((**l_356) = ((safe_mul_func_uint8_t_u_u((*l_531), 0x51L)) | p_86)) < (*l_174))) && 0xE6E0L) | 0xFF1D3D2DL))) != p_86)) ^ l_501) , g_126))) , (**g_127)) | p_84.f0))) < g_2) & p_84.f0))) < 0x25L) , 0xBB8B5C4DL) & g_498) || 0x0EA9L) | p_84.f0));
                    }
                    (*l_611) = (void*)0;
                }
                else
                { /* block id: 254 */
                    return (*p_85);
                }
                for (l_169 = 1; (l_169 >= 0); l_169 -= 1)
                { /* block id: 259 */
                    for (g_293 = 0; (g_293 <= 1); g_293 += 1)
                    { /* block id: 262 */
                        return (*p_85);
                    }
                }
                if (g_424)
                    break;
                return (*p_85);
            }
            for (g_543 = 0; (g_543 <= 1); g_543 += 1)
            { /* block id: 271 */
                uint32_t l_622[1];
                const int32_t *l_624 = &l_136;
                const int32_t **l_623 = &l_624;
                union U6 **l_626 = &l_625[3];
                int i, j;
                for (i = 0; i < 1; i++)
                    l_622[i] = 0xEDB90BB2L;
                (*l_623) = (((safe_sub_func_uint16_t_u_u((((l_359[(g_543 + 2)][g_543] || 7L) >= 0x9C73L) < g_301), (((l_359[(g_543 + 2)][g_543] ^ (l_359[(g_543 + 2)][g_543] & 4UL)) < (((safe_mod_func_uint16_t_u_u(((safe_add_func_uint32_t_u_u((safe_mod_func_int8_t_s_s(((((void*)0 != l_621) < p_84.f0) , (*g_128)), 0x1DL)), (*g_241))) < g_440.f0), 1UL)) , (*l_174)) | (*p_88))) <= (-3L)))) & l_622[0]) , (void*)0);
                (*l_626) = l_625[0];
            }
        }
        l_288 &= (l_294 = ((*l_174) = (safe_mul_func_int8_t_s_s((p_86 = (*g_128)), ((*l_174) > ((((g_161 & ((g_293 = l_632) >= (-9L))) > (!((0xFEL >= (g_50[6] , ((p_84.f0 = (safe_add_func_uint64_t_u_u((safe_sub_func_uint16_t_u_u(g_161, (--(*l_153)))), (((safe_unary_minus_func_int16_t_s((((((((((safe_rshift_func_uint8_t_u_s(l_643[0][1][2], 4)) | 0L) | g_300) && 0x01DE70A93C67A398LL) >= p_84.f0) != p_84.f0) & p_84.f0) != p_84.f0) , (*l_174)))) && (*l_174)) ^ (*l_174))))) >= g_384[2]))) != 0xA2C34738L))) != (-1L)) >= (*l_174)))))));
        for (l_135 = 0; (l_135 > (-20)); l_135 = safe_sub_func_int64_t_s_s(l_135, 4))
        { /* block id: 286 */
            uint8_t l_665 = 0x21L;
            int32_t **l_667 = &l_215;
            (*l_174) = (safe_mod_func_int64_t_s_s((safe_div_func_int64_t_s_s(((l_650 = l_418) == l_418), (safe_sub_func_uint32_t_u_u((safe_div_func_int64_t_s_s(0L, (((l_655[5][1][0] , 0x9EL) ^ (0xB196L == ((safe_lshift_func_uint8_t_u_u((safe_div_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s((!(g_528 , g_198[0])), p_84.f0)), l_665)) != l_665), p_86)), p_84.f0)) ^ g_666))) , l_665))), (*l_174))))), (*l_174)));
            (*l_667) = &l_136;
        }
    }
    else
    { /* block id: 291 */
        int32_t **l_671 = &l_174;
        int32_t ***l_670 = &l_671;
        const int16_t l_710 = 0x99C4L;
        int16_t l_724 = (-1L);
        uint16_t **l_733 = &g_152;
        uint16_t ***l_732 = &l_733;
        int32_t l_745[7][9] = {{0L,0L,(-1L),0L,0L,(-1L),0L,0L,(-1L)},{0x9A55D3F1L,0x771D121BL,0x236999E5L,0x771D121BL,0x9A55D3F1L,8L,0x9A55D3F1L,0x771D121BL,0x236999E5L},{0L,0L,(-1L),0L,0L,(-1L),0L,0L,(-1L)},{0x9A55D3F1L,0x771D121BL,0x236999E5L,0x771D121BL,0x9A55D3F1L,8L,0x9A55D3F1L,0x771D121BL,0x236999E5L},{0L,0L,(-1L),0L,0L,(-1L),0L,0L,(-1L)},{0x9A55D3F1L,0x771D121BL,0x236999E5L,0x771D121BL,0x9A55D3F1L,8L,0x9A55D3F1L,0x771D121BL,0x236999E5L},{0L,0L,(-1L),0L,0L,(-1L),0L,0L,(-1L)}};
        int32_t *l_825 = (void*)0;
        int32_t *l_826 = &l_136;
        int32_t *l_827 = &g_424;
        int32_t *l_828 = &l_745[1][2];
        int32_t *l_829 = &l_136;
        int32_t *l_830 = &g_424;
        int32_t *l_831 = &l_745[3][3];
        int32_t *l_832 = &l_136;
        int32_t *l_833 = &l_317;
        int32_t *l_834 = &l_745[5][6];
        int32_t *l_835 = (void*)0;
        int32_t *l_836 = &l_745[5][6];
        int32_t *l_837[1][2][7] = {{{(void*)0,&l_745[3][4],&l_745[3][4],(void*)0,&g_7,&g_7,&g_7},{(void*)0,&l_745[3][4],&l_745[3][4],(void*)0,&g_7,&g_7,&g_7}}};
        int i, j, k;
        for (g_328.f0 = 0; (g_328.f0 <= 21); g_328.f0++)
        { /* block id: 294 */
            int32_t ****l_672[7][2][9] = {{{&l_670,&l_670,&l_670,&l_670,&l_670,(void*)0,(void*)0,(void*)0,(void*)0},{&l_670,&l_670,&l_670,(void*)0,(void*)0,&l_670,&l_670,&l_670,&l_670}},{{&l_670,&l_670,&l_670,(void*)0,&l_670,&l_670,&l_670,&l_670,(void*)0},{&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670}},{{(void*)0,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,(void*)0,&l_670},{&l_670,(void*)0,&l_670,(void*)0,&l_670,&l_670,(void*)0,&l_670,(void*)0}},{{(void*)0,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670},{&l_670,(void*)0,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670}},{{&l_670,&l_670,(void*)0,&l_670,&l_670,&l_670,&l_670,&l_670,(void*)0},{&l_670,(void*)0,&l_670,&l_670,(void*)0,&l_670,&l_670,(void*)0,&l_670}},{{(void*)0,&l_670,(void*)0,&l_670,&l_670,(void*)0,(void*)0,(void*)0,&l_670},{&l_670,&l_670,&l_670,&l_670,(void*)0,&l_670,&l_670,&l_670,(void*)0}},{{&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,(void*)0},{&l_670,(void*)0,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670,&l_670}}};
            int i, j, k;
            l_670 = l_670;
            if (g_424)
                continue;
            g_673--;
            (*l_671) = (*l_671);
        }
        for (l_169 = 0; (l_169 <= 7); l_169 += 1)
        { /* block id: 302 */
            int8_t l_682[7];
            uint32_t *l_699[1];
            int32_t l_700 = 0xF6D6F02EL;
            int32_t *l_743[6][1];
            int32_t l_768 = 0xF439672BL;
            int i, j;
            for (i = 0; i < 7; i++)
                l_682[i] = 5L;
            for (i = 0; i < 1; i++)
                l_699[i] = &g_489.f1;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 1; j++)
                    l_743[i][j] = &l_317;
            }
            g_424 &= (safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(g_144[0], l_682[3])), ((safe_mod_func_uint32_t_u_u((l_700 |= (safe_add_func_int8_t_s_s(((void*)0 != &g_7), ((((p_84.f0 > 0UL) , ((safe_rshift_func_int8_t_s_u((safe_mul_func_int8_t_s_s((safe_add_func_uint32_t_u_u((safe_add_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((g_440 , g_22), ((safe_add_func_uint8_t_u_u(g_172, 0UL)) , (**l_671)))), g_115[7])), (*g_241))), 1L)), (**l_671))) ^ l_682[3])) < 1UL) , 0x2DL)))), 0xD8C20DF3L)) ^ (-1L)))), p_86));
            for (g_402 = 1; (g_402 <= 7); g_402 += 1)
            { /* block id: 307 */
                int32_t *l_725 = &g_424;
                uint16_t ****l_734[2];
                int64_t *l_737[10] = {&g_291,&g_291,&g_291,&g_291,&g_291,&g_291,&g_291,&g_291,&g_291,&g_291};
                int16_t *l_738 = &l_643[0][1][2];
                uint8_t *l_741[6][3][7] = {{{&g_283.f0,(void*)0,&g_161,&l_359[1][2],(void*)0,&l_359[1][2],&g_161},{&g_301,&g_301,&g_301,&g_172,&g_283.f0,&g_301,&l_359[0][0]},{&g_301,(void*)0,&l_506,&l_506,&g_161,&l_506,&l_506}},{{&g_283.f0,&l_506,&g_172,&l_506,&g_283.f0,&g_301,&g_161},{&l_359[0][0],&g_283.f0,&g_283.f0,&g_172,(void*)0,&g_301,(void*)0},{(void*)0,&l_359[0][0],&g_172,&g_161,&g_161,&g_172,&l_359[0][0]}},{{&l_359[0][0],&g_172,&g_283.f0,&g_301,(void*)0,&l_506,&l_506},{&g_283.f0,&g_301,&l_359[0][0],&g_172,(void*)0,&g_161,(void*)0},{&g_301,&g_283.f0,&g_283.f0,&g_301,&l_506,&l_506,&g_283.f0}},{{&g_301,(void*)0,&g_283.f0,&g_161,(void*)0,&g_283.f0,&l_506},{&g_283.f0,&g_172,&l_506,&g_172,&l_506,&g_172,&g_283.f0},{&g_283.f0,(void*)0,&g_301,&l_506,&g_172,&l_359[3][2],&l_359[0][0]}},{{(void*)0,(void*)0,&g_161,&g_283.f0,&l_506,&l_506,&g_283.f0},{&g_283.f0,&l_359[0][0],&g_283.f0,&g_283.f0,&g_301,&l_506,&l_359[3][2]},{&l_506,&l_506,&g_283.f0,(void*)0,&g_161,&l_359[1][2],(void*)0}},{{&l_506,(void*)0,&g_301,&g_301,(void*)0,&l_506,(void*)0},{&g_283.f0,&g_283.f0,&g_161,&g_172,&g_161,&l_506,&g_283.f0},{(void*)0,&l_506,&g_161,&l_359[3][2],&g_301,&l_359[3][2],&g_161}}};
                uint8_t **l_740 = &l_741[4][1][6];
                uint16_t l_742 = 0x6DE9L;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_734[i] = &l_732;
                (*l_725) |= (((((**g_132) & (p_86 > (safe_mod_func_int64_t_s_s((safe_mod_func_uint64_t_u_u(((((((safe_mul_func_int16_t_s_s((safe_div_func_uint32_t_u_u((*g_241), ((l_710 , (safe_mod_func_uint64_t_u_u((((safe_mod_func_uint8_t_u_u(((~((*g_152) = (***l_670))) >= (**l_671)), (**l_671))) > ((safe_div_func_uint32_t_u_u(((safe_add_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((safe_mul_func_int16_t_s_s(0xDAE7L, (4L | p_86))), p_86)), p_84.f0)) != l_724), (*g_241))) != 0x7E7559BAL)) ^ 0x7D15L), (*l_174)))) ^ 0L))), g_568)) < 0xA9L) == p_84.f0) && g_228) && g_384[4]) == 0xCBA3L), 18446744073709551606UL)), 0x03B1BFA07D45F3F9LL)))) != (**l_671)) < 0x33675849BBD31F6FLL) == 0x5FL);
                (*l_725) &= (0x2A0862CB02E6CFCALL <= (safe_sub_func_int16_t_s_s(((p_88 != ((*l_740) = ((safe_add_func_int8_t_s_s((((safe_add_func_uint16_t_u_u(((**l_733) = (1UL != ((*l_174) <= ((l_735 = l_732) == (g_736[1] = (void*)0))))), ((*l_738) |= ((l_700 ^= 0xA305DAE09A094C8BLL) == 0x7BCA7401381705B8LL)))) != (1L | l_739)) & l_682[3]), (*g_133))) , &l_359[1][0]))) <= 0x0201BE423C22EFB7LL), l_742)));
                (*l_671) = (*l_671);
            }
            g_746++;
            g_749--;
            for (g_528.f2 = 1; (g_528.f2 <= 7); g_528.f2 += 1)
            { /* block id: 323 */
                int32_t *l_760 = &g_300;
                int8_t l_767 = 0L;
                int32_t l_769[1][4];
                uint16_t l_823 = 0x7A42L;
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_769[i][j] = 0x7347BFFFL;
                }
            }
        }
        g_838--;
    }
lbl_848:
    ++g_843;
    for (g_838 = 17; (g_838 <= 19); g_838 = safe_add_func_int64_t_s_s(g_838, 2))
    { /* block id: 368 */
        uint16_t **l_851 = &g_152;
        int32_t l_864 = 5L;
        int8_t *l_865 = &g_50[6].f1;
        int64_t *l_866 = &g_167;
        int32_t l_870 = 1L;
        float *l_876 = (void*)0;
        if (l_317)
            goto lbl_848;
        (*l_841) = (safe_div_func_uint64_t_u_u(((*g_364) ^= (l_851 == (void*)0)), (l_870 ^= (safe_add_func_int32_t_s_s(((safe_rshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s((7L >= p_84.f0), (safe_mod_func_uint8_t_u_u(((((((*l_865) = (safe_mod_func_int64_t_s_s(p_86, (safe_div_func_uint32_t_u_u(4294967293UL, l_864))))) && (((*l_866) |= g_161) > (safe_mul_func_uint8_t_u_u((p_86 || 4L), (*p_88))))) ^ 0x47F26FCBDB531D12LL) >= p_84.f0) && g_180.f0), l_869)))), g_197)) , g_115[5]), l_864)))));
        g_871++;
        for (g_339 = 0; (g_339 == 56); g_339 = safe_add_func_uint64_t_u_u(g_339, 8))
        { /* block id: 378 */
            uint32_t l_881 = 0x6A6972BDL;
            if ((l_876 == (void*)0))
            { /* block id: 379 */
                return (*g_132);
            }
            else
            { /* block id: 381 */
                int16_t l_879 = (-1L);
                int32_t l_880 = (-1L);
                for (g_744 = 0; (g_744 < (-5)); g_744 = safe_sub_func_uint32_t_u_u(g_744, 4))
                { /* block id: 384 */
                    uint64_t l_887 = 18446744073709551615UL;
                    --l_881;
                    for (g_543 = 0; (g_543 <= 0); g_543 += 1)
                    { /* block id: 388 */
                        uint32_t l_884[10] = {0x3AB80132L,0x3AB80132L,0UL,0x3AB80132L,0x3AB80132L,0UL,0x3AB80132L,0x3AB80132L,0UL,0x3AB80132L};
                        int i;
                        l_884[3] |= ((*l_174) = p_86);
                        (*l_841) = 0x0FE013C7L;
                        return (*g_132);
                    }
                    for (g_293 = 0; (g_293 > (-25)); --g_293)
                    { /* block id: 396 */
                        return (*p_85);
                    }
                    l_887 &= 0xC42DC16CL;
                }
                g_424 = ((*l_174) &= g_161);
                return (*p_85);
            }
        }
    }
    return l_888;
}


/* ------------------------------------------ */
/* 
 * reads : g_50.f1 g_115 g_7 g_124 g_22 g_127 g_132
 * writes: g_115 g_50.f1 g_124 g_126
 */
static int8_t ** func_90(uint16_t  p_91, int8_t * p_92)
{ /* block id: 16 */
    int32_t l_106[5][3] = {{(-8L),0L,0L},{(-3L),0L,0L},{(-8L),0L,0L},{(-3L),0L,0L},{(-8L),0L,0L}};
    int16_t l_111 = 0x9D32L;
    int32_t l_112 = (-1L);
    uint16_t *l_113 = (void*)0;
    uint16_t *l_114 = &g_115[7];
    int8_t *l_118 = &g_50[6].f1;
    int8_t *l_119[7];
    int8_t *l_122[5] = {&g_22,&g_22,&g_22,&g_22,&g_22};
    int32_t l_123[3];
    uint32_t *l_125 = &g_126;
    const int32_t l_129 = 0xE26E4AE4L;
    int32_t *l_130 = (void*)0;
    int32_t l_131 = 0L;
    int i, j;
    for (i = 0; i < 7; i++)
        l_119[i] = (void*)0;
    for (i = 0; i < 3; i++)
        l_123[i] = (-1L);
    l_131 ^= (safe_mod_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u(((safe_unary_minus_func_uint32_t_u((safe_unary_minus_func_uint16_t_u(((safe_mul_func_uint16_t_u_u(((((safe_lshift_func_int16_t_s_s((((*l_125) = ((~g_50[6].f1) , ((safe_lshift_func_int8_t_s_u((l_106[4][1] != ((g_124 |= (((l_123[0] |= ((safe_div_func_int64_t_s_s(((safe_mul_func_int8_t_s_s(l_111, (l_112 ^= ((*l_118) = ((p_91 >= g_50[6].f1) <= (--(*l_114))))))) <= 0xC8249D4AL), (safe_rshift_func_int16_t_s_s((p_91 >= g_7), (&g_22 != l_122[2]))))) || 0UL)) , 0x8BL) ^ 0xA7L)) >= g_22)), p_91)) ^ l_106[3][1]))) < 0UL), p_91)) , g_127) != (void*)0) & 0x3F68L), 0x68F5L)) || l_106[4][1]))))) , l_129), 1)), (-6L)));
    return g_132;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_22, "g_22", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_50[i].f0, "g_50[i].f0", print_hash_value);
        transparent_crc(g_50[i].f1, "g_50[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_115[i], "g_115[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_124, "g_124", print_hash_value);
    transparent_crc(g_126, "g_126", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_144[i], "g_144[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_161, "g_161", print_hash_value);
    transparent_crc(g_167, "g_167", print_hash_value);
    transparent_crc(g_172, "g_172", print_hash_value);
    transparent_crc(g_180.f0, "g_180.f0", print_hash_value);
    transparent_crc_bytes (&g_194, sizeof(g_194), "g_194", print_hash_value);
    transparent_crc(g_197, "g_197", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_198[i], "g_198[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_227, "g_227", print_hash_value);
    transparent_crc(g_228, "g_228", print_hash_value);
    transparent_crc(g_283.f0, "g_283.f0", print_hash_value);
    transparent_crc(g_291, "g_291", print_hash_value);
    transparent_crc(g_293, "g_293", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc_bytes (&g_298, sizeof(g_298), "g_298", print_hash_value);
    transparent_crc(g_300, "g_300", print_hash_value);
    transparent_crc(g_301, "g_301", print_hash_value);
    transparent_crc(g_328.f0, "g_328.f0", print_hash_value);
    transparent_crc(g_339, "g_339", print_hash_value);
    transparent_crc(g_360, "g_360", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_384[i], "g_384[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_402, "g_402", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc_bytes(&g_422[i][j], sizeof(g_422[i][j]), "g_422[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_424, "g_424", print_hash_value);
    transparent_crc(g_425, "g_425", print_hash_value);
    transparent_crc(g_440.f0, "g_440.f0", print_hash_value);
    transparent_crc(g_489.f0, "g_489.f0", print_hash_value);
    transparent_crc(g_489.f1, "g_489.f1", print_hash_value);
    transparent_crc(g_498, "g_498", print_hash_value);
    transparent_crc(g_528.f0, "g_528.f0", print_hash_value);
    transparent_crc(g_543, "g_543", print_hash_value);
    transparent_crc(g_568, "g_568", print_hash_value);
    transparent_crc(g_666, "g_666", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_744, "g_744", print_hash_value);
    transparent_crc(g_746, "g_746", print_hash_value);
    transparent_crc(g_749, "g_749", print_hash_value);
    transparent_crc_bytes (&g_770, sizeof(g_770), "g_770", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_771[i][j][k], "g_771[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_788, "g_788", print_hash_value);
    transparent_crc(g_838, "g_838", print_hash_value);
    transparent_crc(g_843, "g_843", print_hash_value);
    transparent_crc(g_871, "g_871", print_hash_value);
    transparent_crc(g_891.f0, "g_891.f0", print_hash_value);
    transparent_crc(g_891.f1, "g_891.f1", print_hash_value);
    transparent_crc(g_894, "g_894", print_hash_value);
    transparent_crc(g_899.f0, "g_899.f0", print_hash_value);
    transparent_crc(g_899.f1, "g_899.f1", print_hash_value);
    transparent_crc(g_900.f0, "g_900.f0", print_hash_value);
    transparent_crc(g_900.f1, "g_900.f1", print_hash_value);
    transparent_crc(g_901.f0, "g_901.f0", print_hash_value);
    transparent_crc(g_901.f1, "g_901.f1", print_hash_value);
    transparent_crc(g_902.f0, "g_902.f0", print_hash_value);
    transparent_crc(g_902.f1, "g_902.f1", print_hash_value);
    transparent_crc(g_903.f0, "g_903.f0", print_hash_value);
    transparent_crc(g_903.f1, "g_903.f1", print_hash_value);
    transparent_crc(g_904.f0, "g_904.f0", print_hash_value);
    transparent_crc(g_904.f1, "g_904.f1", print_hash_value);
    transparent_crc(g_905.f0, "g_905.f0", print_hash_value);
    transparent_crc(g_905.f1, "g_905.f1", print_hash_value);
    transparent_crc(g_906.f0, "g_906.f0", print_hash_value);
    transparent_crc(g_906.f1, "g_906.f1", print_hash_value);
    transparent_crc(g_907.f0, "g_907.f0", print_hash_value);
    transparent_crc(g_907.f1, "g_907.f1", print_hash_value);
    transparent_crc(g_908.f0, "g_908.f0", print_hash_value);
    transparent_crc(g_908.f1, "g_908.f1", print_hash_value);
    transparent_crc(g_909.f0, "g_909.f0", print_hash_value);
    transparent_crc(g_909.f1, "g_909.f1", print_hash_value);
    transparent_crc(g_910.f0, "g_910.f0", print_hash_value);
    transparent_crc(g_910.f1, "g_910.f1", print_hash_value);
    transparent_crc(g_911.f0, "g_911.f0", print_hash_value);
    transparent_crc(g_911.f1, "g_911.f1", print_hash_value);
    transparent_crc(g_912.f0, "g_912.f0", print_hash_value);
    transparent_crc(g_912.f1, "g_912.f1", print_hash_value);
    transparent_crc(g_913.f0, "g_913.f0", print_hash_value);
    transparent_crc(g_913.f1, "g_913.f1", print_hash_value);
    transparent_crc(g_914.f0, "g_914.f0", print_hash_value);
    transparent_crc(g_914.f1, "g_914.f1", print_hash_value);
    transparent_crc(g_915.f0, "g_915.f0", print_hash_value);
    transparent_crc(g_915.f1, "g_915.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_916[i][j].f0, "g_916[i][j].f0", print_hash_value);
            transparent_crc(g_916[i][j].f1, "g_916[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_917.f0, "g_917.f0", print_hash_value);
    transparent_crc(g_917.f1, "g_917.f1", print_hash_value);
    transparent_crc(g_918.f0, "g_918.f0", print_hash_value);
    transparent_crc(g_918.f1, "g_918.f1", print_hash_value);
    transparent_crc(g_919.f0, "g_919.f0", print_hash_value);
    transparent_crc(g_919.f1, "g_919.f1", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_920[i].f0, "g_920[i].f0", print_hash_value);
        transparent_crc(g_920[i].f1, "g_920[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_921.f0, "g_921.f0", print_hash_value);
    transparent_crc(g_921.f1, "g_921.f1", print_hash_value);
    transparent_crc(g_922.f0, "g_922.f0", print_hash_value);
    transparent_crc(g_922.f1, "g_922.f1", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_923[i][j][k].f0, "g_923[i][j][k].f0", print_hash_value);
                transparent_crc(g_923[i][j][k].f1, "g_923[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_924.f0, "g_924.f0", print_hash_value);
    transparent_crc(g_924.f1, "g_924.f1", print_hash_value);
    transparent_crc(g_925.f0, "g_925.f0", print_hash_value);
    transparent_crc(g_925.f1, "g_925.f1", print_hash_value);
    transparent_crc(g_926.f0, "g_926.f0", print_hash_value);
    transparent_crc(g_926.f1, "g_926.f1", print_hash_value);
    transparent_crc(g_927.f0, "g_927.f0", print_hash_value);
    transparent_crc(g_927.f1, "g_927.f1", print_hash_value);
    transparent_crc(g_928.f0, "g_928.f0", print_hash_value);
    transparent_crc(g_928.f1, "g_928.f1", print_hash_value);
    transparent_crc(g_929.f0, "g_929.f0", print_hash_value);
    transparent_crc(g_929.f1, "g_929.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_930[i].f0, "g_930[i].f0", print_hash_value);
        transparent_crc(g_930[i].f1, "g_930[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_931[i][j].f0, "g_931[i][j].f0", print_hash_value);
            transparent_crc(g_931[i][j].f1, "g_931[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_932[i].f0, "g_932[i].f0", print_hash_value);
        transparent_crc(g_932[i].f1, "g_932[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_933.f0, "g_933.f0", print_hash_value);
    transparent_crc(g_933.f1, "g_933.f1", print_hash_value);
    transparent_crc(g_934.f0, "g_934.f0", print_hash_value);
    transparent_crc(g_934.f1, "g_934.f1", print_hash_value);
    transparent_crc(g_935.f0, "g_935.f0", print_hash_value);
    transparent_crc(g_935.f1, "g_935.f1", print_hash_value);
    transparent_crc(g_936.f0, "g_936.f0", print_hash_value);
    transparent_crc(g_936.f1, "g_936.f1", print_hash_value);
    transparent_crc(g_937.f0, "g_937.f0", print_hash_value);
    transparent_crc(g_937.f1, "g_937.f1", print_hash_value);
    transparent_crc(g_938.f0, "g_938.f0", print_hash_value);
    transparent_crc(g_938.f1, "g_938.f1", print_hash_value);
    transparent_crc(g_939.f0, "g_939.f0", print_hash_value);
    transparent_crc(g_939.f1, "g_939.f1", print_hash_value);
    transparent_crc(g_940.f0, "g_940.f0", print_hash_value);
    transparent_crc(g_940.f1, "g_940.f1", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_941[i].f0, "g_941[i].f0", print_hash_value);
        transparent_crc(g_941[i].f1, "g_941[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_942.f0, "g_942.f0", print_hash_value);
    transparent_crc(g_942.f1, "g_942.f1", print_hash_value);
    transparent_crc(g_943.f0, "g_943.f0", print_hash_value);
    transparent_crc(g_943.f1, "g_943.f1", print_hash_value);
    transparent_crc(g_944.f0, "g_944.f0", print_hash_value);
    transparent_crc(g_944.f1, "g_944.f1", print_hash_value);
    transparent_crc(g_945.f0, "g_945.f0", print_hash_value);
    transparent_crc(g_945.f1, "g_945.f1", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_946[i].f0, "g_946[i].f0", print_hash_value);
        transparent_crc(g_946[i].f1, "g_946[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_947.f0, "g_947.f0", print_hash_value);
    transparent_crc(g_947.f1, "g_947.f1", print_hash_value);
    transparent_crc(g_948.f0, "g_948.f0", print_hash_value);
    transparent_crc(g_948.f1, "g_948.f1", print_hash_value);
    transparent_crc(g_949.f0, "g_949.f0", print_hash_value);
    transparent_crc(g_949.f1, "g_949.f1", print_hash_value);
    transparent_crc(g_950.f0, "g_950.f0", print_hash_value);
    transparent_crc(g_950.f1, "g_950.f1", print_hash_value);
    transparent_crc(g_951.f0, "g_951.f0", print_hash_value);
    transparent_crc(g_951.f1, "g_951.f1", print_hash_value);
    transparent_crc(g_952.f0, "g_952.f0", print_hash_value);
    transparent_crc(g_952.f1, "g_952.f1", print_hash_value);
    transparent_crc(g_953.f0, "g_953.f0", print_hash_value);
    transparent_crc(g_953.f1, "g_953.f1", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_954[i].f0, "g_954[i].f0", print_hash_value);
        transparent_crc(g_954[i].f1, "g_954[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_955.f0, "g_955.f0", print_hash_value);
    transparent_crc(g_955.f1, "g_955.f1", print_hash_value);
    transparent_crc(g_956.f0, "g_956.f0", print_hash_value);
    transparent_crc(g_956.f1, "g_956.f1", print_hash_value);
    transparent_crc(g_964, "g_964", print_hash_value);
    transparent_crc(g_966, "g_966", print_hash_value);
    transparent_crc(g_975, "g_975", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_981[i][j][k].f0, "g_981[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_985, "g_985", print_hash_value);
    transparent_crc(g_1031, "g_1031", print_hash_value);
    transparent_crc(g_1032, "g_1032", print_hash_value);
    transparent_crc(g_1042, "g_1042", print_hash_value);
    transparent_crc(g_1043, "g_1043", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1048[i], "g_1048[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1050, "g_1050", print_hash_value);
    transparent_crc(g_1052, "g_1052", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_1059[i].f0, sizeof(g_1059[i].f0), "g_1059[i].f0", print_hash_value);
        transparent_crc(g_1059[i].f1, "g_1059[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_1104[i][j][k].f0, "g_1104[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1145.f0, "g_1145.f0", print_hash_value);
    transparent_crc(g_1145.f1, "g_1145.f1", print_hash_value);
    transparent_crc_bytes (&g_1179.f0, sizeof(g_1179.f0), "g_1179.f0", print_hash_value);
    transparent_crc(g_1179.f1, "g_1179.f1", print_hash_value);
    transparent_crc_bytes (&g_1182.f0, sizeof(g_1182.f0), "g_1182.f0", print_hash_value);
    transparent_crc(g_1182.f1, "g_1182.f1", print_hash_value);
    transparent_crc(g_1233.f0, "g_1233.f0", print_hash_value);
    transparent_crc(g_1233.f1, "g_1233.f1", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_1270[i].f0, sizeof(g_1270[i].f0), "g_1270[i].f0", print_hash_value);
        transparent_crc(g_1270[i].f1, "g_1270[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1286, "g_1286", print_hash_value);
    transparent_crc(g_1288, "g_1288", print_hash_value);
    transparent_crc(g_1319, "g_1319", print_hash_value);
    transparent_crc(g_1320, "g_1320", print_hash_value);
    transparent_crc(g_1325, "g_1325", print_hash_value);
    transparent_crc(g_1373, "g_1373", print_hash_value);
    transparent_crc(g_1432, "g_1432", print_hash_value);
    transparent_crc(g_1470, "g_1470", print_hash_value);
    transparent_crc(g_1473, "g_1473", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1520[i], "g_1520[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1522, "g_1522", print_hash_value);
    transparent_crc(g_1523, "g_1523", print_hash_value);
    transparent_crc(g_1526.f0, "g_1526.f0", print_hash_value);
    transparent_crc(g_1526.f1, "g_1526.f1", print_hash_value);
    transparent_crc(g_1543, "g_1543", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_1544[i][j][k], "g_1544[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1613, "g_1613", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1623[i].f0, "g_1623[i].f0", print_hash_value);
        transparent_crc_bytes(&g_1623[i].f1, sizeof(g_1623[i].f1), "g_1623[i].f1", print_hash_value);
        transparent_crc(g_1623[i].f2, "g_1623[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1693, sizeof(g_1693), "g_1693", print_hash_value);
    transparent_crc(g_1694, "g_1694", print_hash_value);
    transparent_crc_bytes (&g_1745.f0, sizeof(g_1745.f0), "g_1745.f0", print_hash_value);
    transparent_crc(g_1745.f1, "g_1745.f1", print_hash_value);
    transparent_crc(g_1782, "g_1782", print_hash_value);
    transparent_crc(g_1831.f0, "g_1831.f0", print_hash_value);
    transparent_crc_bytes (&g_1831.f1, sizeof(g_1831.f1), "g_1831.f1", print_hash_value);
    transparent_crc(g_1831.f2, "g_1831.f2", print_hash_value);
    transparent_crc(g_1836.f0, "g_1836.f0", print_hash_value);
    transparent_crc(g_1836.f1, "g_1836.f1", print_hash_value);
    transparent_crc(g_1836.f2, "g_1836.f2", print_hash_value);
    transparent_crc(g_1836.f3, "g_1836.f3", print_hash_value);
    transparent_crc(g_1883.f0, "g_1883.f0", print_hash_value);
    transparent_crc(g_1883.f1, "g_1883.f1", print_hash_value);
    transparent_crc(g_1883.f2, "g_1883.f2", print_hash_value);
    transparent_crc(g_1883.f3, "g_1883.f3", print_hash_value);
    transparent_crc(g_1955.f0, "g_1955.f0", print_hash_value);
    transparent_crc(g_1955.f1, "g_1955.f1", print_hash_value);
    transparent_crc_bytes (&g_1961.f0, sizeof(g_1961.f0), "g_1961.f0", print_hash_value);
    transparent_crc(g_1961.f1, "g_1961.f1", print_hash_value);
    transparent_crc(g_1980, "g_1980", print_hash_value);
    transparent_crc(g_2041, "g_2041", print_hash_value);
    transparent_crc(g_2071.f0, "g_2071.f0", print_hash_value);
    transparent_crc(g_2071.f1, "g_2071.f1", print_hash_value);
    transparent_crc(g_2071.f2, "g_2071.f2", print_hash_value);
    transparent_crc(g_2071.f3, "g_2071.f3", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2100[i].f0, "g_2100[i].f0", print_hash_value);
        transparent_crc(g_2100[i].f1, "g_2100[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2120.f0, "g_2120.f0", print_hash_value);
    transparent_crc(g_2137, "g_2137", print_hash_value);
    transparent_crc(g_2138, "g_2138", print_hash_value);
    transparent_crc(g_2139, "g_2139", print_hash_value);
    transparent_crc(g_2140, "g_2140", print_hash_value);
    transparent_crc(g_2144, "g_2144", print_hash_value);
    transparent_crc_bytes (&g_2168.f0, sizeof(g_2168.f0), "g_2168.f0", print_hash_value);
    transparent_crc(g_2168.f1, "g_2168.f1", print_hash_value);
    transparent_crc(g_2180, "g_2180", print_hash_value);
    transparent_crc_bytes (&g_2190.f0, sizeof(g_2190.f0), "g_2190.f0", print_hash_value);
    transparent_crc(g_2190.f1, "g_2190.f1", print_hash_value);
    transparent_crc(g_2219, "g_2219", print_hash_value);
    transparent_crc_bytes (&g_2273.f0, sizeof(g_2273.f0), "g_2273.f0", print_hash_value);
    transparent_crc(g_2273.f1, "g_2273.f1", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2305[i], "g_2305[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 540
XXX total union variables: 51

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 5
breakdown:
   indirect level: 0, occurrence: 5
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 10
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 44
breakdown:
   depth: 1, occurrence: 293
   depth: 2, occurrence: 66
   depth: 3, occurrence: 6
   depth: 4, occurrence: 7
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 2
   depth: 8, occurrence: 2
   depth: 10, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 15, occurrence: 3
   depth: 16, occurrence: 5
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 3
   depth: 20, occurrence: 3
   depth: 21, occurrence: 4
   depth: 22, occurrence: 3
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 2
   depth: 32, occurrence: 3
   depth: 33, occurrence: 1
   depth: 35, occurrence: 1
   depth: 37, occurrence: 2
   depth: 38, occurrence: 2
   depth: 44, occurrence: 1

XXX total number of pointers: 653

XXX times a variable address is taken: 1366
XXX times a pointer is dereferenced on RHS: 298
breakdown:
   depth: 1, occurrence: 222
   depth: 2, occurrence: 58
   depth: 3, occurrence: 15
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 325
breakdown:
   depth: 1, occurrence: 285
   depth: 2, occurrence: 30
   depth: 3, occurrence: 6
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 38
XXX times a pointer is compared with address of another variable: 11
XXX times a pointer is compared with another pointer: 10
XXX times a pointer is qualified to be dereferenced: 3332

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1058
   level: 2, occurrence: 274
   level: 3, occurrence: 81
   level: 4, occurrence: 42
   level: 5, occurrence: 9
XXX number of pointers point to pointers: 245
XXX number of pointers point to scalars: 362
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 25.9
XXX average alias set size: 1.43

XXX times a non-volatile is read: 1919
XXX times a non-volatile is write: 995
XXX times a volatile is read: 36
XXX    times read thru a pointer: 12
XXX times a volatile is write: 16
XXX    times written thru a pointer: 7
XXX times a volatile is available for access: 631
XXX percentage of non-volatile access: 98.2

XXX forward jumps: 1
XXX backward jumps: 14

XXX stmts: 289
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 25
   depth: 2, occurrence: 32
   depth: 3, occurrence: 50
   depth: 4, occurrence: 64
   depth: 5, occurrence: 88

XXX percentage a fresh-made variable is used: 22.6
XXX percentage an existing variable is used: 77.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


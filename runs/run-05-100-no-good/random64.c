/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3554719382
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint64_t g_5 = 6UL;
static int64_t g_11 = 0L;
static volatile uint16_t g_14 = 0x012EL;/* VOLATILE GLOBAL g_14 */
static uint32_t g_17 = 1UL;
static uint64_t g_57[10][9] = {{0xA2CC37069E5F9C6ALL,0x996121589AFCA862LL,0xFDF115CD63CCF75BLL,4UL,0x7311EBC84814C7C7LL,0x50D22F7F1C74F348LL,18446744073709551615UL,18446744073709551607UL,0xBA5CD23E821D2A6ALL},{18446744073709551615UL,1UL,0x2349124C6E12CDFDLL,18446744073709551612UL,0x8663C10B42B8FFD0LL,0UL,0xB1E5DE2CAEABA4A2LL,0x0E101EAE883781CDLL,18446744073709551607UL},{8UL,18446744073709551611UL,18446744073709551615UL,3UL,1UL,0xA2CC37069E5F9C6ALL,0xDBF60700C0D357E8LL,18446744073709551615UL,0x2349124C6E12CDFDLL},{0x6F7E707ECB515FE4LL,0xAB10E3A113D7AFF7LL,18446744073709551615UL,0x8663C10B42B8FFD0LL,0x9E00C255375BB799LL,0x9E00C255375BB799LL,0x8663C10B42B8FFD0LL,18446744073709551615UL,0xAB10E3A113D7AFF7LL},{0x0E101EAE883781CDLL,0x6EB81F947F9F41ADLL,0x2349124C6E12CDFDLL,18446744073709551615UL,0xDB075ABA8B036C86LL,18446744073709551612UL,0x50D22F7F1C74F348LL,4UL,0xFFF9752EE8B7203FLL},{0xAB10E3A113D7AFF7LL,0x70C387C7A5019003LL,0x121ADF561F6604C3LL,0x0E101EAE883781CDLL,0xDB075ABA8B036C86LL,18446744073709551609UL,18446744073709551615UL,18446744073709551606UL,0xB1E5DE2CAEABA4A2LL},{0x70C387C7A5019003LL,0x50D22F7F1C74F348LL,0xB1E5DE2CAEABA4A2LL,0xFDF115CD63CCF75BLL,0xA2CC37069E5F9C6ALL,0x9E00C255375BB799LL,18446744073709551615UL,0x7311EBC84814C7C7LL,0xBA5CD23E821D2A6ALL},{8UL,18446744073709551615UL,0xFDF115CD63CCF75BLL,1UL,0x70C387C7A5019003LL,0xBA5CD23E821D2A6ALL,0x8663C10B42B8FFD0LL,0UL,0x6EB81F947F9F41ADLL},{8UL,0xDB075ABA8B036C86LL,0xDBF60700C0D357E8LL,0xBA5CD23E821D2A6ALL,0xBA5CD23E821D2A6ALL,0xDBF60700C0D357E8LL,0xDB075ABA8B036C86LL,8UL,0UL},{0x70C387C7A5019003LL,0x7311EBC84814C7C7LL,18446744073709551612UL,18446744073709551615UL,8UL,18446744073709551615UL,8UL,0xFDF115CD63CCF75BLL,18446744073709551615UL}};
static int64_t g_72 = 0x8F68172882C65315LL;
static uint16_t g_75 = 0UL;
static uint32_t g_87 = 9UL;
static int32_t g_104 = 0x1D34D3BEL;
static float g_106[6] = {0x4.7F7088p+5,0x4.7F7088p+5,0x4.7F7088p+5,0x4.7F7088p+5,0x4.7F7088p+5,0x4.7F7088p+5};
static uint64_t g_107 = 0UL;
static uint8_t g_128 = 0x4CL;
static int8_t g_129 = 1L;
static int32_t **g_186 = (void*)0;
static const int32_t g_194 = 8L;
static const int32_t *g_193[4][3] = {{&g_194,&g_194,&g_194},{&g_194,&g_194,&g_194},{&g_194,&g_194,&g_194},{&g_194,&g_194,&g_194}};
static const int32_t **g_192[1][3] = {{&g_193[1][2],&g_193[1][2],&g_193[1][2]}};
static int8_t g_197 = 0L;
static uint32_t g_198 = 0UL;
static int8_t g_218 = 0xC6L;
static int8_t *g_225 = &g_218;
static int8_t **g_224[5][2][10] = {{{(void*)0,&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225,(void*)0,&g_225},{(void*)0,&g_225,(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225}},{{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225}},{{(void*)0,(void*)0,&g_225,&g_225,&g_225,(void*)0,(void*)0,&g_225,&g_225,&g_225},{&g_225,(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225}},{{&g_225,(void*)0,(void*)0,&g_225,&g_225,&g_225,(void*)0,&g_225,(void*)0,&g_225},{&g_225,(void*)0,(void*)0,(void*)0,(void*)0,&g_225,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225}}};
static const float *g_231 = &g_106[1];
static const float **g_230 = &g_231;
static const float ***g_229 = &g_230;
static int64_t g_238 = (-1L);
static uint8_t g_242 = 254UL;
static const uint16_t g_249 = 0xCA94L;
static int8_t g_296 = 0xC1L;
static int32_t g_297 = (-1L);
static int32_t g_298 = (-5L);
static uint8_t g_299 = 0xE2L;
static float g_304 = 0x0.769A12p-46;
static uint64_t g_305 = 18446744073709551615UL;
static uint32_t g_312 = 1UL;
static uint32_t g_359 = 18446744073709551615UL;
static uint8_t g_360 = 0x6FL;
static int32_t g_378 = 1L;
static uint16_t g_380 = 0x93DEL;
static uint32_t g_383 = 0xE79B31CCL;
static uint8_t g_397 = 246UL;
static int64_t *g_400 = &g_238;
static int16_t g_412 = (-8L);
static int64_t g_450[3] = {0x8FB90210760A3B93LL,0x8FB90210760A3B93LL,0x8FB90210760A3B93LL};
static uint16_t g_452 = 0x505EL;
static int32_t g_469 = 7L;
static int16_t g_470 = 0x181DL;
static uint8_t g_471 = 248UL;
static uint32_t g_474[5] = {0x36A11E93L,0x36A11E93L,0x36A11E93L,0x36A11E93L,0x36A11E93L};
static const uint16_t * volatile *g_478 = (void*)0;
static const uint16_t * volatile **g_477 = &g_478;
static int32_t ***g_620 = &g_186;
static uint32_t *g_718 = &g_383;
static uint32_t **g_717 = &g_718;
static int32_t g_767 = 1L;
static int64_t g_768 = 0xFC70386367C7F76ALL;
static uint32_t g_769 = 0x6CE7D614L;
static uint8_t g_791 = 255UL;
static int8_t g_826 = 0x50L;
static int32_t g_827 = 0L;
static int64_t g_829 = (-8L);
static uint32_t g_830 = 0UL;
static uint32_t g_912 = 1UL;
static int8_t g_921 = 0xC6L;
static uint64_t g_923 = 0x20FE2AAFE0D8BE05LL;
static uint16_t g_929 = 7UL;
static uint32_t g_998 = 0x274AABC6L;
static int8_t g_1021 = 0L;
static int64_t g_1022 = 0x8E987050C32397E8LL;
static int8_t g_1023 = 0x5FL;
static uint32_t g_1024 = 4294967287UL;
static int8_t * const * const g_1033 = &g_225;
static int8_t * const * const *g_1032 = &g_1033;
static int8_t * const * const * const  volatile *g_1031 = &g_1032;
static uint64_t g_1062 = 0x9FEDD5FC81BCAE5ALL;
static int8_t g_1074 = (-1L);
static uint8_t g_1075 = 2UL;
static float g_1080 = 0xD.7816EFp-82;
static int32_t g_1081[2] = {1L,1L};
static uint32_t g_1082 = 18446744073709551615UL;
static float *g_1093 = &g_106[3];
static uint8_t g_1114[4] = {2UL,2UL,2UL,2UL};
static const float *****g_1186 = (void*)0;
static const int32_t ***g_1208 = &g_192[0][1];
static const int32_t ****g_1207 = &g_1208;
static uint64_t g_1219 = 0xB4AE1710245F1529LL;
static uint32_t g_1377 = 1UL;
static int8_t g_1399 = (-2L);
static uint32_t g_1460 = 1UL;
static float g_1472 = 0xC.DB1148p+77;
static uint32_t g_1475[4] = {0x5AC2DFA2L,0x5AC2DFA2L,0x5AC2DFA2L,0x5AC2DFA2L};
static int64_t g_1536 = 0xFA26EECB3A1BEFE0LL;
static float g_1540[10] = {0x0.8p-1,0xF.56F306p-24,0x0.8p-1,0xF.56F306p-24,0x0.8p-1,0xF.56F306p-24,0x0.8p-1,0xF.56F306p-24,0x0.8p-1,0xF.56F306p-24};
static int32_t g_1542 = 0L;
static uint16_t g_1543 = 0x2960L;
static int32_t *g_1581 = &g_297;
static int64_t g_1615 = 0x6638673E0C40DDFALL;
static uint16_t g_1642 = 0xBBE7L;
static int64_t g_1718 = 0x0F668DB871AA754DLL;
static uint8_t g_1770 = 0xFBL;
static float ****g_1803 = (void*)0;
static float *****g_1802 = &g_1803;
static int8_t g_1830 = 7L;
static int16_t g_1871 = 0xD983L;
static const uint16_t g_1900 = 65534UL;
static int32_t * const ** const **g_1913 = (void*)0;
static uint16_t g_1933 = 65529UL;
static float g_1945[5][5][7] = {{{0x0.6p+1,0x8.Ap-1,0xB.90A2F3p+86,(-0x2.4p-1),0xB.059E52p-16,0xB.059E52p-16,(-0x2.4p-1)},{0x5.1F4BB2p-89,0xD.7A8D37p-44,0x5.1F4BB2p-89,0x2.6447FDp-69,(-0x2.Dp+1),0xA.71B17Cp+70,(-0x1.Ep-1)},{0xB.90A2F3p+86,0x8.Ap-1,0x0.6p+1,0x1.4284C2p-28,0x0.6p+1,0x8.Ap-1,0xB.90A2F3p+86},{0xB.9EA2E3p-0,0x6.B4CED0p+46,(-0x1.Ep-1),(-0x2.Dp+1),0x6.B0B77Cp+90,0xA.71B17Cp+70,0x6.B0B77Cp+90},{0x8.Ap-1,(-0x1.8p+1),(-0x1.8p+1),0x8.Ap-1,0xD.701C77p+90,0xB.059E52p-16,0x1.4284C2p-28}},{{0x0.7p-1,0x3.CC4FB3p+60,(-0x1.Ep-1),0x5.1F4BB2p-89,0x5.1F4BB2p-89,(-0x1.Ep-1),0x3.CC4FB3p+60},{0xD.701C77p+90,0xB.90A2F3p+86,0x0.6p+1,0x9.5p-1,(-0x1.8p+1),0x1.4284C2p-28,0x1.4284C2p-28},{0xA.71B17Cp+70,0x0.7p-1,0x5.1F4BB2p-89,0x0.7p-1,0xA.71B17Cp+70,0x6.B4CED0p+46,0x6.B0B77Cp+90},{0x9.0A130Cp+83,0xB.059E52p-16,0xB.90A2F3p+86,0x9.5p-1,(-0x2.4p-1),0x9.5p-1,0xB.90A2F3p+86},{0x6.B0B77Cp+90,0x6.B0B77Cp+90,0xB.9EA2E3p-0,0x5.1F4BB2p-89,0x3.CC4FB3p+60,0x2.6447FDp-69,(-0x1.Ep-1)}},{{0x9.0A130Cp+83,0x9.5p-1,0x8.Ap-1,0x8.Ap-1,0x9.5p-1,0x9.0A130Cp+83,(-0x2.4p-1)},{0xA.71B17Cp+70,0xB.9EA2E3p-0,0x0.7p-1,(-0x2.Dp+1),0x3.CC4FB3p+60,0x3.CC4FB3p+60,(-0x2.Dp+1)},{0xD.701C77p+90,0x9.Fp-1,0xD.701C77p+90,0x1.4284C2p-28,(-0x2.4p-1),0x0.6p+1,0x9.0A130Cp+83},{0x0.7p-1,0xB.9EA2E3p-0,0xA.71B17Cp+70,0x2.6447FDp-69,0xA.71B17Cp+70,0xB.9EA2E3p-0,0x0.7p-1},{0x8.Ap-1,0x9.5p-1,0x9.0A130Cp+83,(-0x2.4p-1),(-0x1.8p+1),0x0.6p+1,(-0x1.8p+1)}},{{0xB.9EA2E3p-0,0x6.B0B77Cp+90,0x6.B0B77Cp+90,0xB.9EA2E3p-0,0x5.1F4BB2p-89,0x3.CC4FB3p+60,0x2.6447FDp-69},{0xB.90A2F3p+86,0xB.059E52p-16,0x9.0A130Cp+83,0xD.701C77p+90,0xD.701C77p+90,0x9.0A130Cp+83,0xB.059E52p-16},{0x5.1F4BB2p-89,0x0.7p-1,0xA.71B17Cp+70,0x6.B4CED0p+46,0x6.B0B77Cp+90,0x2.6447FDp-69,0x2.6447FDp-69},{0x0.6p+1,0xB.90A2F3p+86,0xD.701C77p+90,0xB.90A2F3p+86,0x0.6p+1,0x9.5p-1,(-0x1.8p+1)},{(-0x1.Ep-1),0x3.CC4FB3p+60,0x0.7p-1,0x6.B4CED0p+46,(-0x2.Dp+1),0x6.B4CED0p+46,0x0.7p-1}},{{(-0x1.8p+1),(-0x1.8p+1),0x8.Ap-1,0xD.701C77p+90,0xB.059E52p-16,0x1.4284C2p-28,0x9.0A130Cp+83},{(-0x1.Ep-1),0x6.B4CED0p+46,0xB.9EA2E3p-0,0xB.9EA2E3p-0,0x6.B4CED0p+46,(-0x1.Ep-1),(-0x2.Dp+1)},{0x0.6p+1,0x8.Ap-1,0xB.90A2F3p+86,(-0x2.4p-1),0xB.059E52p-16,0xB.059E52p-16,(-0x2.4p-1)},{0x5.1F4BB2p-89,0xD.7A8D37p-44,0x5.1F4BB2p-89,0x2.6447FDp-69,(-0x2.Dp+1),0xA.71B17Cp+70,(-0x1.Ep-1)},{0xB.90A2F3p+86,0x8.Ap-1,0x0.6p+1,0x1.4284C2p-28,0x0.6p+1,0x8.Ap-1,0xB.90A2F3p+86}}};
static uint64_t g_1953 = 0xED5BBE5CC9A93D49LL;
static uint8_t g_1956 = 2UL;
static volatile int32_t g_1972 = 1L;/* VOLATILE GLOBAL g_1972 */
static const volatile int32_t *g_1971 = &g_1972;
static int32_t g_2037 = 0L;
static int8_t g_2038 = 0x87L;
static int64_t g_2039 = (-3L);
static int32_t g_2040 = (-1L);
static int32_t g_2042 = (-1L);
static uint64_t g_2043 = 0UL;
static uint16_t *g_2063 = &g_1642;
static uint16_t ** const g_2062 = &g_2063;
static uint16_t ** const *g_2061[9] = {&g_2062,&g_2062,&g_2062,&g_2062,&g_2062,&g_2062,&g_2062,&g_2062,&g_2062};
static int16_t * const *g_2108 = (void*)0;
static float g_2194 = (-0x1.1p-1);
static uint32_t g_2196 = 0UL;
static uint8_t g_2201[1] = {0UL};
static const uint16_t g_2286 = 65527UL;
static int8_t g_2352[7] = {3L,3L,0xECL,(-1L),(-1L),3L,(-1L)};
static int32_t g_2355 = 0L;
static float g_2356 = 0x1.6p+1;
static float g_2357[4] = {0x7.08F4D3p-89,0x7.08F4D3p-89,0x7.08F4D3p-89,0x7.08F4D3p-89};
static uint32_t g_2361 = 0x488A312FL;
static int32_t g_2424 = 3L;
static int64_t **g_2434 = (void*)0;
static int32_t g_2457 = 0xCE9E88F1L;
static uint16_t g_2458 = 65530UL;
static volatile uint16_t ** const  volatile **g_2470 = (void*)0;
static volatile uint16_t ** const  volatile ***g_2469 = &g_2470;
static volatile int16_t * volatile *g_2598 = (void*)0;
static volatile int16_t * volatile **g_2597 = &g_2598;
static volatile int16_t * volatile ***g_2596 = &g_2597;
static uint16_t **g_2623 = &g_2063;
static uint16_t ***g_2622 = &g_2623;
static uint16_t ****g_2621 = &g_2622;
static uint32_t g_2630 = 1UL;
static int32_t *g_2640 = &g_827;
static int32_t **g_2639 = &g_2640;
static volatile uint32_t g_2708 = 4294967293UL;/* VOLATILE GLOBAL g_2708 */
static volatile uint32_t *g_2707 = &g_2708;
static volatile uint32_t * const  volatile *g_2706 = &g_2707;
static volatile uint32_t * const  volatile * volatile *g_2705 = &g_2706;
static int16_t ***g_2745 = (void*)0;
static int16_t ****g_2744 = &g_2745;
static uint32_t g_2767 = 9UL;
static int32_t g_2787[9][5] = {{0x73598171L,0x73598171L,0x73598171L,0x73598171L,0x73598171L},{(-1L),(-1L),(-1L),(-1L),(-1L)},{0x73598171L,0x73598171L,0x73598171L,0x73598171L,0x73598171L},{(-1L),(-1L),(-1L),(-1L),(-1L)},{0x73598171L,0x73598171L,0x73598171L,0x73598171L,0x73598171L},{(-1L),(-1L),(-1L),(-1L),(-1L)},{0x73598171L,0x73598171L,0x73598171L,0x73598171L,0x73598171L},{(-1L),(-1L),(-1L),(-1L),(-1L)},{0x73598171L,0x73598171L,0x73598171L,0x73598171L,0x73598171L}};
static uint32_t ***g_2983 = &g_717;
static uint32_t ****g_2982 = &g_2983;
static uint64_t g_3025 = 0xAEB33C0940A9D51ALL;
static int8_t g_3055 = (-1L);
static uint16_t g_3083 = 0xCC0AL;
static int32_t *g_3166 = &g_2787[7][2];
static volatile uint16_t g_3198 = 0x005DL;/* VOLATILE GLOBAL g_3198 */


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static int8_t  func_15(float  p_16);
static float  func_18(uint32_t  p_19, int64_t * p_20, const uint64_t * p_21, int64_t  p_22, uint32_t  p_23);
static int8_t  func_26(int64_t * p_27, int16_t  p_28, int64_t  p_29, float  p_30, uint16_t  p_31);
static int32_t  func_35(uint16_t  p_36, const int16_t  p_37, int64_t * p_38);
static int64_t  func_39(uint64_t * p_40, int8_t  p_41, int64_t * p_42, uint64_t * p_43);
static uint64_t * func_44(int64_t * p_45);
static int64_t * func_46(uint64_t * p_47, int8_t  p_48, int16_t  p_49);
static int32_t  func_51(const uint16_t  p_52, uint64_t * p_53, uint64_t * p_54, float  p_55);
static int16_t  func_60(uint64_t  p_61, uint16_t  p_62, uint64_t * p_63, const float  p_64);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_14 g_17 g_57 g_75 g_72 g_107 g_106 g_104 g_129 g_128 g_186 g_87 g_194 g_198 g_197 g_218 g_229 g_242 g_230 g_231 g_225 g_299 g_305 g_312 g_298 g_297 g_359 g_380 g_383 g_397 g_238 g_296 g_452 g_471 g_474 g_477 g_360 g_400 g_469 g_378 g_450 g_412 g_1031 g_1032 g_1033 g_1074 g_791 g_1075 g_717 g_718 g_767 g_193 g_1581 g_1081 g_1093 g_1543 g_1399 g_998 g_1933 g_768 g_1953 g_1956 g_921 g_1022 g_1114 g_2458 g_2042 g_2469 g_2063 g_1642 g_1024 g_2062 g_929 g_1770 g_826 g_2037 g_2038 g_2767 g_1718 g_2621 g_2622 g_2623 g_1219 g_3055 g_620 g_2983 g_470 g_829 g_2639 g_2640 g_3198
 * writes: g_5 g_11 g_75 g_87 g_104 g_107 g_128 g_106 g_129 g_57 g_72 g_192 g_198 g_197 g_218 g_224 g_242 g_299 g_305 g_312 g_298 g_360 g_304 g_380 g_383 g_400 g_297 g_186 g_412 g_238 g_397 g_230 g_452 g_471 g_477 g_231 g_469 g_378 g_470 g_767 g_193 g_791 g_1913 g_1075 g_1399 g_1933 g_768 g_1953 g_2458 g_2357 g_2356 g_2042 g_450 g_1642 g_1543 g_1081 g_1219 g_1581 g_2705 g_1770 g_826 g_2037 g_2744 g_2040 g_2767 g_1718 g_921 g_1460 g_3166 g_827 g_1024 g_3198
 */
static float  func_1(void)
{ /* block id: 0 */
    uint64_t *l_4[10] = {&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5};
    int64_t *l_10 = &g_11;
    int32_t l_34[7];
    int16_t l_3174[10][4] = {{(-1L),(-1L),0xBDA7L,(-1L)},{(-1L),(-4L),(-4L),(-1L)},{(-4L),(-1L),(-4L),(-4L)},{(-1L),(-1L),0xBDA7L,(-1L)},{(-1L),(-4L),(-4L),(-1L)},{(-4L),(-1L),(-4L),(-4L)},{(-1L),(-1L),0xBDA7L,(-1L)},{(-1L),(-4L),(-4L),(-1L)},{(-4L),(-1L),(-4L),(-4L)},{(-1L),(-1L),0xBDA7L,(-1L)}};
    float ***l_3175 = (void*)0;
    int64_t l_3197 = 0L;
    int i, j;
    for (i = 0; i < 7; i++)
        l_34[i] = 0x3876A47CL;
    if ((((**g_2639) = ((safe_sub_func_uint64_t_u_u((--g_5), (safe_div_func_int64_t_s_s(((*l_10) = 0x15912491E29EA414LL), (safe_mul_func_int8_t_s_s(g_14, func_15((g_17 != ((((*g_1093) = (func_18((251UL >= (g_17 < ((safe_mul_func_int8_t_s_s(func_26(l_4[3], (safe_mod_func_int32_t_s_s((g_17 <= l_34[1]), func_35(((func_39(func_44(func_46(l_4[3], g_17, l_34[1])), l_34[2], &g_450[0], l_4[1]) <= l_34[3]) < g_921), g_1022, l_4[3]))), l_34[0], l_34[1], (*g_2063)), 0x5BL)) || (-3L)))), l_10, l_4[3], l_34[1], l_34[1]) <= l_34[1])) != l_34[0]) > l_34[1]))))))))) != 0xD0L)) , l_34[4]))
    { /* block id: 1486 */
        int32_t * const *l_3169 = &g_1581;
        int32_t l_3180 = 0xEE1080DCL;
        float **l_3182 = (void*)0;
        float ***l_3181 = &l_3182;
        int32_t *l_3183 = &g_767;
        int32_t *l_3184 = &l_34[1];
        int32_t *l_3185 = &l_3180;
        int32_t *l_3186 = &g_2042;
        int32_t *l_3187 = &g_2457;
        int32_t *l_3188 = (void*)0;
        int32_t *l_3189 = &g_2787[4][3];
        int32_t *l_3190 = &g_767;
        int32_t *l_3191 = &g_2037;
        int32_t *l_3192 = &l_3180;
        int32_t *l_3193[3];
        int16_t l_3194 = 0xC9F0L;
        int8_t l_3195 = 0xB1L;
        int8_t l_3196 = 1L;
        int i;
        for (i = 0; i < 3; i++)
            l_3193[i] = &g_2457;
        g_104 ^= (safe_sub_func_int16_t_s_s(((void*)0 == l_3169), ((safe_mod_func_int32_t_s_s((g_1022 <= (safe_lshift_func_int16_t_s_u((l_3174[2][2] & 9UL), (**g_2623)))), (g_1024 |= (l_3175 != (((((*g_718) &= (safe_sub_func_uint32_t_u_u((safe_add_func_int64_t_s_s((*g_400), (l_3174[8][0] && 65534UL))), 0x1535BA64L))) < l_3180) || 0x7F4BA0A4L) , l_3181))))) != 0xFDDC16E30569FD74LL)));
        --g_3198;
        return l_3174[2][0];
    }
    else
    { /* block id: 1492 */
        return l_34[1];
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_297 g_2038 g_767 g_383 g_717 g_718 g_1024 g_1033 g_225 g_218 g_1031 g_1032 g_400 g_238 g_1770 g_2767 g_1718 g_2621 g_2622 g_2623 g_2063 g_1642 g_471 g_1219 g_107 g_2458 g_231 g_106 g_299 g_3055 g_620 g_186 g_2983 g_470 g_829 g_359
 * writes: g_767 g_383 g_2744 g_2040 g_299 g_1770 g_2767 g_1718 g_1642 g_2458 g_921 g_471 g_378 g_186 g_1460 g_3166
 */
static int8_t  func_15(float  p_16)
{ /* block id: 1314 */
    uint64_t l_2726 = 0xF491560F218D362ELL;
    uint32_t *l_2753 = &g_1460;
    int32_t l_2761 = 0xE60B5F8EL;
    int32_t l_2762[7] = {0x44102D64L,0x0A8C7178L,0x0A8C7178L,0x44102D64L,0x0A8C7178L,0x0A8C7178L,0x44102D64L};
    uint16_t ****l_2798 = &g_2622;
    uint32_t ***l_2843 = &g_717;
    int32_t * const *l_2872 = &g_2640;
    int32_t * const **l_2871 = &l_2872;
    int32_t ****l_2900 = &g_620;
    int32_t **** const *l_2899 = &l_2900;
    int64_t l_2969 = 0x819EDC0E54B62A4DLL;
    uint8_t l_3056[8][3] = {{9UL,1UL,9UL},{1UL,0xA4L,0xA4L},{0x34L,1UL,0x34L},{1UL,1UL,0xA4L},{9UL,1UL,9UL},{1UL,0xA4L,0xA4L},{0x34L,1UL,0x34L},{1UL,1UL,0xA4L}};
    int16_t l_3071 = 0x0836L;
    int64_t l_3120 = 0x8C84018C8FABA48DLL;
    int64_t l_3124 = 0x44BCE58A6256DB07LL;
    float **l_3142 = &g_1093;
    float ***l_3141[3][9] = {{&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142},{&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142},{&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142,&l_3142}};
    float ****l_3140[7] = {&l_3141[0][3],&l_3141[0][3],&l_3141[0][3],&l_3141[0][3],&l_3141[0][3],&l_3141[0][3],&l_3141[0][3]};
    float l_3162 = 0x3.57D956p-27;
    int i, j;
    g_767 ^= ((safe_mul_func_uint8_t_u_u((safe_mod_func_int8_t_s_s((safe_div_func_uint8_t_u_u((0x83L != (1L || 0x11CF5132FC9DABC5LL)), l_2726)), (((~((g_297 ^ (safe_mod_func_int32_t_s_s(((1L > (l_2726 || (((safe_unary_minus_func_uint32_t_u(l_2726)) || l_2726) != l_2726))) == g_2038), 0xF110B0F3L))) ^ 18446744073709551615UL)) && 0xE682L) || l_2726))), 1UL)) >= l_2726);
    for (g_383 = (-23); (g_383 > 54); g_383++)
    { /* block id: 1318 */
        int16_t ***l_2742 = (void*)0;
        int16_t ****l_2741[7][5] = {{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742},{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742},{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742},{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742},{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742},{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742},{&l_2742,&l_2742,&l_2742,&l_2742,&l_2742}};
        int16_t *****l_2743[7] = {&l_2741[1][4],&l_2741[1][4],&l_2741[1][4],&l_2741[1][4],&l_2741[1][4],&l_2741[1][4],&l_2741[1][4]};
        const int32_t l_2746 = 0xA39BFC11L;
        int32_t *l_2747 = (void*)0;
        int32_t *l_2748[3][9][5] = {{{(void*)0,&g_2042,&g_2042,&g_378,&g_378},{(void*)0,&g_2040,(void*)0,&g_767,&g_378},{&g_378,&g_2457,&g_378,&g_104,&g_2037},{&g_2042,&g_297,&g_297,&g_2042,&g_378},{&g_2457,&g_2042,&g_378,&g_2037,&g_2042},{&g_104,&g_378,(void*)0,(void*)0,&g_2457},{&g_2042,&g_104,&g_2042,&g_378,&g_297},{&g_297,&g_104,&g_767,&g_767,&g_104},{&g_378,&g_378,&g_2457,&g_104,&g_2037}},{{(void*)0,&g_2042,&g_378,&g_104,&g_104},{&g_2037,&g_297,&g_767,(void*)0,&g_2042},{(void*)0,&g_2457,&g_297,&g_378,(void*)0},{&g_378,&g_2040,&g_2457,&g_104,&g_378},{&g_297,&g_2042,&g_378,&g_104,&g_378},{&g_2042,&g_2037,&g_2037,&g_2042,(void*)0},{&g_104,&g_2042,&g_2037,&g_378,&g_2042},{&g_2457,(void*)0,(void*)0,&g_378,&g_104},{&g_2042,&g_104,&g_297,&g_378,&g_2037}},{{&g_378,&g_104,&g_2040,&g_2042,&g_104},{(void*)0,(void*)0,&g_2457,&g_104,&g_297},{(void*)0,&g_767,&g_2457,&g_104,&g_2457},{&g_297,&g_297,&g_2040,&g_378,&g_2042},{&g_378,&g_2042,&g_297,(void*)0,&g_378},{&g_378,&g_297,(void*)0,&g_104,&g_2037},{&g_378,&g_2042,&g_2037,&g_104,&g_378},{&g_767,&g_297,&g_2037,&g_767,&g_378},{&g_104,&g_767,&g_378,&g_378,&g_2457}}};
        uint8_t *l_2754[3][1];
        int32_t **l_2757 = &l_2748[1][1][3];
        int8_t l_2770 = (-3L);
        float l_2786 = 0x1.D1B2C1p-0;
        int16_t l_2788 = (-1L);
        uint64_t l_2789[9] = {6UL,6UL,6UL,6UL,6UL,6UL,6UL,6UL,6UL};
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_2754[i][j] = &g_128;
        }
        (*l_2757) = ((safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((((safe_rshift_func_int8_t_s_s(((g_2744 = l_2741[5][1]) != &g_2745), 7)) <= ((l_2746 >= (g_2040 = l_2726)) , ((((l_2726 || (~(+(safe_div_func_int8_t_s_s((((((g_299 = ((void*)0 != l_2753)) , (safe_mod_func_uint32_t_u_u((**g_717), l_2726))) || 0xE735B57BL) , g_1024) == 0x49794E54854C362ALL), (**g_1033)))))) != 0x570527DF68D3435DLL) & l_2726) | (****g_1031)))) != l_2726), l_2726)), (*g_400))), l_2726)) , l_2753);
        for (g_1770 = 0; (g_1770 < 42); g_1770 = safe_add_func_uint16_t_u_u(g_1770, 4))
        { /* block id: 1325 */
            int32_t l_2760 = (-2L);
            int32_t l_2763 = 0x9D0C5B63L;
            int32_t l_2764 = 7L;
            int32_t l_2765 = 0xDC673911L;
            int32_t l_2766[2][10][5] = {{{1L,0xEDB1328FL,0L,8L,0x5B8FDA41L},{8L,0x69592194L,0x4765F4E7L,0x69592194L,8L},{0L,0xB40BA7A8L,0x1CAA5DFAL,0x0479FFEDL,0x5B8FDA41L},{2L,1L,0x20EF4093L,0x98B08B8FL,8L},{0xD11AB7CBL,8L,0xFFF795CCL,0xB40BA7A8L,0x5B8FDA41L},{0x05F3852FL,0x98B08B8FL,0x7E93FE2AL,0x81E9DA67L,(-3L)},{8L,0L,0x8166B7E9L,0L,8L},{0xA9035C06L,0x49D7A6C0L,0x9A40ACC8L,(-1L),(-3L)},{0xEDB1328FL,0L,0x7B1C5A9DL,0xAE07AF9BL,8L},{1L,0x81E9DA67L,0x42A8B0C0L,0x49D7A6C0L,(-3L)}},{{9L,0xAE07AF9BL,9L,6L,8L},{(-3L),0xD108F377L,0x3287AF7BL,0xD108F377L,(-3L)},{0xB40BA7A8L,(-2L),0x5C2CC3DEL,0L,8L},{0x98B08B8FL,(-1L),0x7DAF6EE4L,0x7E60D2C5L,(-3L)},{0x0479FFEDL,6L,0x824128E3L,(-2L),8L},{0x69592194L,0x7E60D2C5L,0L,0x81E9DA67L,(-3L)},{8L,0L,0x8166B7E9L,0L,8L},{0xA9035C06L,0x49D7A6C0L,0x9A40ACC8L,(-1L),(-3L)},{0xEDB1328FL,0L,0x7B1C5A9DL,0xAE07AF9BL,8L},{1L,0x81E9DA67L,0x42A8B0C0L,0x49D7A6C0L,(-3L)}}};
            int i, j, k;
            ++g_2767;
            for (g_1718 = 8; (g_1718 >= 1); g_1718 -= 1)
            { /* block id: 1329 */
                g_767 = (((****g_2621) = l_2760) <= l_2770);
            }
            l_2766[0][1][1] |= (safe_sub_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u(((*g_2063) &= (safe_lshift_func_uint16_t_u_u((0L || (-1L)), l_2761))), ((((((((&l_2760 != (*l_2757)) | ((!(safe_lshift_func_uint8_t_u_u(1UL, 2))) && g_471)) <= ((safe_sub_func_uint64_t_u_u(((safe_div_func_int32_t_s_s(((safe_rshift_func_int16_t_s_s((((((((void*)0 != &g_1207) , 0x1AL) < 0x16L) != 0x7DL) && 0xF944FCB0L) > 1L), l_2760)) != l_2763), l_2763)) >= g_1219), l_2760)) <= l_2762[0])) & l_2761) < l_2761) != 0x4CL) <= 0x0BL) ^ g_107))), l_2764));
        }
        --l_2789[4];
        if (l_2762[0])
            continue;
    }
    for (g_2458 = (-17); (g_2458 < 7); g_2458 = safe_add_func_int32_t_s_s(g_2458, 1))
    { /* block id: 1341 */
        int64_t l_2802 = 0xEE5588E29F691A5FLL;
        int32_t *l_2821[8][3][8] = {{{(void*)0,&g_297,&l_2762[4],&g_104,(void*)0,&g_104,&g_2787[3][0],&g_297},{&g_378,&g_297,&g_378,&g_767,&g_2042,&g_297,(void*)0,&g_2787[4][3]},{(void*)0,&g_767,&g_297,&g_297,&g_378,(void*)0,(void*)0,&g_378}},{{&g_2457,&g_297,&g_297,&g_2457,&g_297,&g_2457,&g_2787[3][0],&g_2042},{&g_2457,&g_2042,&g_104,&g_104,&g_2787[3][2],(void*)0,&g_378,&g_767},{&g_104,&g_2042,&l_2762[0],&g_104,&g_378,&g_2457,&g_104,&g_767}},{{&g_297,&g_297,(void*)0,&g_2042,&g_378,(void*)0,&g_2787[4][3],&g_2040},{(void*)0,&g_767,&l_2762[0],&g_2787[3][2],&g_2457,&g_297,&g_2787[3][2],&g_2457},{(void*)0,&g_297,&g_2787[4][3],&g_2040,&g_378,&g_104,&g_104,&g_2787[3][0]}},{{&g_104,&g_297,(void*)0,&g_378,&g_2042,&l_2762[4],&g_297,&l_2762[4]},{&g_2457,(void*)0,&g_297,(void*)0,&g_2457,&l_2762[0],&g_2042,&g_2457},{&g_378,&g_2787[4][3],&g_2037,&g_2042,&g_378,&l_2762[0],(void*)0,(void*)0}},{{&g_767,&g_2787[4][3],&g_2037,&g_2042,&g_2457,&g_2037,&g_2042,(void*)0},{&g_378,&g_104,&g_297,&g_2457,&g_297,&g_2457,&g_297,&g_767},{(void*)0,&g_2787[3][2],(void*)0,&g_2042,&g_2457,&g_2457,&g_104,&g_104}},{{&g_297,&g_2457,&g_2457,&g_2787[4][3],&g_2040,&l_2762[0],&l_2762[4],(void*)0},{&g_104,&g_297,&g_767,(void*)0,&g_297,&g_2457,&g_2042,&g_2042},{&l_2762[0],&g_2042,&g_297,&g_297,&g_2042,&l_2762[0],&g_2457,&g_2040}},{{&g_297,&g_378,&g_378,&g_2037,&g_2787[1][4],&g_2787[3][4],&g_2457,&g_378},{&g_297,&g_2040,&g_104,&g_2037,(void*)0,&g_767,&g_104,&g_2040},{(void*)0,(void*)0,&g_2042,&g_297,&l_2762[0],(void*)0,&l_2762[0],&g_2042}},{{&g_378,&l_2762[0],(void*)0,(void*)0,(void*)0,&l_2762[0],&g_297,(void*)0},{&g_2787[4][3],(void*)0,&g_297,&g_2787[4][3],&g_2040,&g_378,&g_104,&g_104},{(void*)0,&l_2762[0],&g_2787[3][4],&l_2762[0],&l_2762[4],&g_2457,(void*)0,&g_2042}}};
        int8_t **l_2828[10] = {&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225};
        uint16_t l_2868 = 0x8BBCL;
        int16_t l_2875 = 2L;
        int8_t l_2896 = 0xB0L;
        uint32_t l_2976 = 1UL;
        int8_t l_3086 = 0x0BL;
        uint64_t l_3106 = 0UL;
        uint32_t **l_3116 = &g_718;
        const float l_3123 = 0xB.1E3114p+67;
        int16_t l_3163[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
        uint16_t ***l_3164 = &g_2623;
        int32_t l_3165 = 0xBF056A81L;
        int i, j, k;
        for (g_921 = 0; (g_921 == (-5)); g_921--)
        { /* block id: 1344 */
            int64_t l_2799 = 0x62EEA79CDB673B34LL;
            uint8_t *l_2805 = &g_1956;
            const int32_t ***l_2807 = &g_192[0][1];
            int32_t l_2815 = 0xC0A4ABB3L;
            uint32_t * const *l_2846 = (void*)0;
            uint32_t * const **l_2845 = &l_2846;
            int32_t ****l_2898 = &g_620;
            int32_t *****l_2897 = &l_2898;
            uint8_t l_2971[4][9][2] = {{{0xFCL,0x91L},{0xFCL,0xE8L},{1UL,0xFCL},{0xE8L,0x91L},{0x1EL,0x1EL},{1UL,0x1EL},{0x1EL,0x91L},{0xE8L,0xFCL},{1UL,0xE8L}},{{0xFCL,0x91L},{0xFCL,0xE8L},{1UL,0xFCL},{0xE8L,0x91L},{0x1EL,0x1EL},{1UL,0x1EL},{0x1EL,0x91L},{0xE8L,0xFCL},{1UL,0xE8L}},{{0xFCL,0x91L},{0xFCL,0xE8L},{1UL,0xFCL},{0xE8L,0x91L},{0x1EL,0x1EL},{1UL,0x1EL},{0x1EL,0x91L},{0xE8L,0xFCL},{1UL,0xE8L}},{{0xFCL,0x91L},{0xFCL,0xE8L},{1UL,0xFCL},{0xE8L,0x91L},{0x1EL,0x1EL},{1UL,0x1EL},{0x1EL,0x91L},{0xE8L,0xFCL},{1UL,0xE8L}}};
            int8_t ***l_3016 = (void*)0;
            uint16_t *l_3054 = &l_2868;
            uint64_t *l_3057 = &g_2043;
            int32_t l_3070[10] = {(-6L),0x6C2938F3L,0x6C2938F3L,(-6L),0x6C2938F3L,0x6C2938F3L,(-6L),0x6C2938F3L,0x6C2938F3L,(-6L)};
            uint8_t l_3078[2][9] = {{0x66L,0UL,0x0DL,1UL,0UL,1UL,0x0DL,0UL,0x66L},{0x66L,0UL,0x0DL,1UL,0UL,1UL,0x0DL,0UL,0UL}};
            int16_t l_3081 = 3L;
            uint64_t l_3102 = 0xFCB15012088B5780LL;
            uint32_t *l_3104 = &g_1475[3];
            uint16_t ****l_3113 = (void*)0;
            uint16_t ***l_3115 = &g_2623;
            uint16_t ****l_3114 = &l_3115;
            uint32_t **l_3117[9] = {&l_2753,&l_2753,&l_2753,&l_2753,&l_2753,&l_2753,&l_2753,&l_2753,&l_2753};
            uint32_t l_3118 = 0xEE0964C2L;
            uint32_t l_3119[7];
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_3119[i] = 18446744073709551614UL;
        }
        for (g_471 = 0; (g_471 < 20); ++g_471)
        { /* block id: 1471 */
            uint8_t *l_3135[8][10] = {{(void*)0,&g_2201[0],&g_2201[0],&g_1114[1],&g_1114[1],&g_2201[0],&g_2201[0],(void*)0,&g_2201[0],&g_2201[0]},{(void*)0,&g_1075,&g_1114[1],&g_1075,(void*)0,&g_2201[0],&g_2201[0],(void*)0,&g_1075,&g_1114[1]},{(void*)0,(void*)0,&g_1114[1],(void*)0,&g_791,(void*)0,&g_1114[1],(void*)0,(void*)0,&g_1114[1]},{&g_1075,(void*)0,&g_2201[0],&g_2201[0],(void*)0,&g_1075,&g_1114[1],&g_1075,(void*)0,&g_2201[0]},{&g_2201[0],(void*)0,&g_2201[0],&g_2201[0],&g_1114[1],&g_1114[1],&g_2201[0],&g_2201[0],(void*)0,&g_2201[0]},{&g_2201[0],&g_1075,(void*)0,(void*)0,(void*)0,&g_1075,&g_2201[0],&g_2201[0],&g_1075,(void*)0},{&g_1075,&g_2201[0],&g_2201[0],&g_1075,(void*)0,(void*)0,(void*)0,&g_1075,&g_2201[0],&g_2201[0]},{(void*)0,&g_2201[0],&g_2201[0],&g_1114[1],&g_1114[1],&g_2201[0],&g_2201[0],(void*)0,&g_2201[0],&g_2201[0]}};
            int32_t l_3136 = 0xE9C240C9L;
            int32_t l_3137 = (-1L);
            int16_t *l_3143[5];
            uint32_t ****l_3158 = &l_2843;
            uint32_t ***l_3160 = &l_3116;
            uint32_t ****l_3159 = &l_3160;
            int32_t *****l_3161[4][3] = {{(void*)0,&l_2900,&l_2900},{(void*)0,&l_2900,(void*)0},{&l_2900,(void*)0,&l_2900},{&l_2900,&l_2900,(void*)0}};
            int i, j;
            for (i = 0; i < 5; i++)
                l_3143[i] = &l_2875;
            g_378 = l_3124;
            (***l_2899) = (((safe_lshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_s((*g_225), 1)), 2)) <= ((safe_div_func_int16_t_s_s((l_3136 = (safe_mul_func_int8_t_s_s(0x09L, ((l_3137 = (safe_mod_func_uint32_t_u_u(((*g_231) , ((g_299++) , l_3136)), l_3136))) & ((((void*)0 != l_3140[6]) || (*g_718)) && ((l_3136 <= g_3055) > l_3136)))))), (*g_2063))) >= (-1L))) , (*g_620));
            g_3166 = ((safe_mul_func_int8_t_s_s((***g_1032), ((l_3137 |= 0x53L) && ((***g_2983) > ((*l_2753) = (safe_div_func_int32_t_s_s(0x2F43B4E6L, (safe_div_func_int8_t_s_s((((((((safe_add_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u(((g_470 > (((*l_3159) = ((*l_3158) = &l_3116)) != (void*)0)) || ((l_3161[0][1] != &l_2900) == g_829)), (**g_2623))), g_359)), (**g_1033))), l_3163[6])) == 0x7F5AFC48D69AB130LL) , 1UL) , l_3164) != (void*)0) || (-1L)) ^ l_3056[3][0]), l_3165))))))))) , &l_3165);
        }
    }
    return (*g_225);
}


/* ------------------------------------------ */
/* 
 * reads : g_1075 g_2062 g_2063 g_1642 g_1093 g_106 g_242 g_929 g_230 g_231 g_198 g_1770 g_826 g_2037
 * writes: g_1642 g_242 g_1075 g_1581 g_2705 g_198 g_1770 g_826 g_2037 g_1219
 */
static float  func_18(uint32_t  p_19, int64_t * p_20, const uint64_t * p_21, int64_t  p_22, uint32_t  p_23)
{ /* block id: 1210 */
    uint32_t l_2531 = 0xF513FB72L;
    int32_t l_2542 = 0xB3203A03L;
    int32_t l_2559 = 0L;
    int32_t l_2560 = 0x2D9A8E20L;
    uint16_t ****l_2624 = &g_2622;
    float *****l_2625 = &g_1803;
    float *l_2663 = &g_2356;
    uint32_t l_2697[1];
    int16_t ****l_2700 = (void*)0;
    int i;
    for (i = 0; i < 1; i++)
        l_2697[i] = 1UL;
    if ((p_22 ^ ((**g_2062) ^= (safe_add_func_uint64_t_u_u(g_1075, (8UL | 6L))))))
    { /* block id: 1212 */
        return (*g_1093);
    }
    else
    { /* block id: 1214 */
        uint8_t l_2546 = 254UL;
        int16_t l_2564 = 0L;
        int64_t l_2571 = (-5L);
        float * const *l_2582 = &g_1093;
        float * const **l_2581 = &l_2582;
        float * const ** const *l_2580 = &l_2581;
        int32_t l_2588 = 0x2F2C7F86L;
        uint16_t l_2607 = 0x8777L;
        uint32_t *l_2610[4] = {&g_312,&g_312,&g_312,&g_312};
        float *****l_2627 = &g_1803;
        int32_t l_2688[10];
        uint32_t l_2689 = 0x53271639L;
        int16_t * const **l_2699 = &g_2108;
        int16_t * const ***l_2698[1];
        uint8_t *l_2701[3];
        int32_t l_2702 = 0L;
        int i;
        for (i = 0; i < 10; i++)
            l_2688[i] = 0x5C22C920L;
        for (i = 0; i < 1; i++)
            l_2698[i] = &l_2699;
        for (i = 0; i < 3; i++)
            l_2701[i] = &g_242;
        for (g_242 = 0; (g_242 >= 51); ++g_242)
        { /* block id: 1217 */
            int16_t *l_2536[3];
            int16_t **l_2535 = &l_2536[1];
            int16_t ***l_2534 = &l_2535;
            int32_t l_2543[5][8][4] = {{{0x16B27D25L,(-1L),0L,(-4L)},{(-9L),1L,0xE8B47DDFL,(-4L)},{0xA2CB5C4DL,(-1L),0L,3L},{3L,0x18951C1FL,0L,(-9L)},{0xA2CB5C4DL,0L,0xE8B47DDFL,(-6L)},{(-9L),0L,0L,(-9L)},{0x16B27D25L,0x18951C1FL,(-1L),3L},{0x16B27D25L,(-1L),0L,(-4L)}},{{(-9L),1L,0xE8B47DDFL,(-4L)},{0xA2CB5C4DL,(-1L),0L,3L},{3L,0x18951C1FL,0L,(-9L)},{0xA2CB5C4DL,0L,0xE8B47DDFL,(-6L)},{(-9L),0L,0L,(-9L)},{0x16B27D25L,0x18951C1FL,(-1L),3L},{0x16B27D25L,(-1L),0L,(-4L)},{(-9L),1L,0xE8B47DDFL,(-4L)}},{{0xA2CB5C4DL,(-1L),0L,3L},{3L,0x18951C1FL,0L,(-9L)},{0xA2CB5C4DL,0L,0xE8B47DDFL,(-6L)},{(-9L),0L,0L,(-9L)},{0x16B27D25L,0x18951C1FL,(-1L),3L},{0x16B27D25L,(-8L),1L,(-6L)},{0x16B27D25L,0xE8B47DDFL,0L,(-6L)},{0x22AE8D47L,(-8L),(-1L),(-4L)}},{{(-4L),0L,(-1L),0x16B27D25L},{0x22AE8D47L,1L,0L,0xA2CB5C4DL},{0x16B27D25L,1L,1L,0x16B27D25L},{3L,0L,(-8L),(-4L)},{3L,(-8L),1L,(-6L)},{0x16B27D25L,0xE8B47DDFL,0L,(-6L)},{0x22AE8D47L,(-8L),(-1L),(-4L)},{(-4L),0L,(-1L),0x16B27D25L}},{{0x22AE8D47L,1L,0L,0xA2CB5C4DL},{0x16B27D25L,1L,1L,0x16B27D25L},{3L,0L,(-8L),(-4L)},{3L,(-8L),1L,(-6L)},{0x16B27D25L,0xE8B47DDFL,0L,(-6L)},{0x22AE8D47L,(-8L),(-1L),(-4L)},{(-4L),0L,(-1L),0x16B27D25L},{0x22AE8D47L,1L,0L,0xA2CB5C4DL}}};
            uint64_t l_2609 = 0xBD190E41282F43D9LL;
            float *****l_2628 = &g_1803;
            uint16_t **l_2644 = &g_2063;
            int32_t l_2672 = 0x6B161CE4L;
            int32_t *l_2684 = (void*)0;
            int32_t *l_2685 = &g_2037;
            int32_t *l_2686 = &l_2560;
            int32_t *l_2687[1];
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2536[i] = &g_412;
            for (i = 0; i < 1; i++)
                l_2687[i] = &g_297;
        }
        if ((safe_unary_minus_func_int64_t_s(((((((p_19 , p_23) || p_19) & g_929) == l_2688[9]) & (l_2588 ^ (l_2564 >= (l_2702 = ((safe_lshift_func_int8_t_s_u(7L, ((safe_mod_func_uint8_t_u_u((g_1075 |= (((l_2697[0] = (((((*g_230) == (***l_2580)) >= 0x1.Cp+1) >= 0xC.B5CB1Bp+74) < p_19)) , l_2698[0]) == l_2700)), 1L)) ^ (-6L)))) & p_19))))) & l_2559))))
        { /* block id: 1289 */
            int32_t l_2703 = 5L;
            int32_t **l_2704 = &g_1581;
            l_2688[0] = l_2703;
            (*l_2704) = &l_2542;
            g_2705 = (void*)0;
            for (g_198 = 17; (g_198 != 21); g_198++)
            { /* block id: 1295 */
                int32_t *l_2713 = &g_2037;
                for (g_1770 = 0; (g_1770 <= 26); g_1770 = safe_add_func_uint64_t_u_u(g_1770, 2))
                { /* block id: 1298 */
                    int32_t *l_2714 = (void*)0;
                    for (g_826 = 0; (g_826 <= 0); g_826 += 1)
                    { /* block id: 1301 */
                        int i, j;
                        l_2714 = l_2713;
                        (*l_2713) ^= (-7L);
                    }
                }
            }
        }
        else
        { /* block id: 1307 */
            int32_t *l_2715 = &g_2040;
            int32_t *l_2716[1];
            uint16_t l_2717 = 0xC705L;
            int i;
            for (i = 0; i < 1; i++)
                l_2716[i] = &l_2559;
            for (g_1219 = 0; g_1219 < 10; g_1219 += 1)
            {
                l_2688[g_1219] = (-1L);
            }
            l_2717++;
        }
    }
    return l_2542;
}


/* ------------------------------------------ */
/* 
 * reads : g_2063 g_1642 g_1933 g_1032 g_1033 g_225 g_218 g_412 g_1024 g_400 g_238 g_1581
 * writes: g_1642 g_1933 g_470 g_218 g_1543 g_1081 g_1219 g_297
 */
static int8_t  func_26(int64_t * p_27, int16_t  p_28, int64_t  p_29, float  p_30, uint16_t  p_31)
{ /* block id: 1197 */
    uint16_t *l_2487 = &g_1933;
    int32_t l_2491 = 0xA38642ABL;
    int16_t *l_2492[4][8] = {{&g_412,&g_412,&g_412,&g_470,(void*)0,&g_1871,&g_412,&g_412},{&g_412,&g_1871,&g_1871,&g_1871,&g_1871,&g_412,(void*)0,&g_1871},{&g_412,&g_470,(void*)0,&g_1871,(void*)0,&g_1871,(void*)0,&g_470},{&g_412,(void*)0,&g_1871,&g_1871,&g_1871,&g_470,&g_470,&g_1871}};
    uint16_t *l_2495 = &g_1543;
    uint32_t l_2496 = 4294967295UL;
    int32_t *l_2497 = &g_1081[1];
    int32_t l_2498 = 0L;
    uint16_t *l_2503 = (void*)0;
    const int8_t *l_2510 = &g_2352[2];
    const int8_t **l_2509 = &l_2510;
    const int8_t ***l_2508 = &l_2509;
    int8_t ***l_2512 = &g_224[0][0][0];
    int8_t ****l_2511 = &l_2512;
    uint64_t *l_2515 = &g_1219;
    int32_t ***l_2516 = (void*)0;
    int32_t ***l_2517 = (void*)0;
    int32_t **l_2519 = &l_2497;
    int32_t ***l_2518 = &l_2519;
    int i, j;
    l_2498 |= (safe_rshift_func_uint16_t_u_s((((~(0xB5E7L || (safe_add_func_uint16_t_u_u(((*l_2487) &= ((*g_2063)++)), ((((~0x25BDL) & (p_31 >= ((((((*l_2497) = (((((safe_mul_func_int16_t_s_s(p_28, (g_470 = l_2491))) ^ (safe_add_func_uint16_t_u_u(((-1L) == p_31), ((*l_2495) = ((((*g_225) = (***g_1032)) == (&g_718 == (void*)0)) & 0x3AL))))) | l_2496) & 0L) && 18446744073709551611UL)) , 0x9CFEC5C85661EEA6LL) != 6L) ^ l_2491) ^ p_31))) > p_29) , p_28))))) < g_412) < 1L), l_2496));
    (*g_1581) = (4UL & ((*l_2515) = (safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(p_31, 8)), (((l_2503 == ((l_2496 , ((safe_lshift_func_int8_t_s_u((((safe_rshift_func_uint8_t_u_u(((l_2508 != ((*l_2511) = &g_224[0][0][0])) > l_2496), (p_31 < (((safe_mod_func_uint8_t_u_u(p_31, l_2496)) && l_2491) & l_2498)))) < 0x8B3A454BL) , (-1L)), 2)) == p_31)) , l_2495)) , g_1024) && (*g_400))))));
    (*l_2518) = &l_2497;
    return p_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_1953 g_312 g_1114 g_1581 g_297 g_2458 g_378 g_1093 g_2042 g_2469 g_238 g_104
 * writes: g_1953 g_312 g_104 g_297 g_2458 g_378 g_2357 g_106 g_2356 g_2042 g_450
 */
static int32_t  func_35(uint16_t  p_36, const int16_t  p_37, int64_t * p_38)
{ /* block id: 886 */
    int32_t *l_1959 = &g_104;
    int8_t ****l_1997 = (void*)0;
    int8_t *l_2034 = (void*)0;
    int16_t **l_2060 = (void*)0;
    int32_t l_2114 = 1L;
    uint64_t l_2158 = 0x4C8F1C4EB8102C0CLL;
    int32_t *l_2159 = &g_104;
    float *****l_2160 = &g_1803;
    int64_t *l_2167[2][9][10] = {{{(void*)0,(void*)0,(void*)0,&g_1615,&g_768,&g_768,&g_1615,(void*)0,(void*)0,(void*)0},{&g_1718,&g_450[1],&g_768,(void*)0,(void*)0,&g_768,&g_238,(void*)0,&g_450[1],&g_2039},{&g_829,&g_829,&g_450[1],&g_450[1],(void*)0,&g_450[1],&g_450[1],&g_829,&g_829,(void*)0},{(void*)0,&g_238,(void*)0,&g_829,&g_768,&g_829,&g_768,(void*)0,&g_450[1],&g_450[1]},{(void*)0,&g_450[0],(void*)0,&g_829,&g_829,(void*)0,&g_450[0],(void*)0,&g_829,&g_1615},{&g_450[0],(void*)0,&g_1718,&g_450[1],(void*)0,&g_450[1],&g_1718,&g_829,&g_450[1],&g_829},{&g_450[1],&g_829,&g_1718,(void*)0,&g_1718,&g_829,&g_450[1],(void*)0,(void*)0,&g_829},{&g_768,&g_768,(void*)0,&g_1615,&g_1718,&g_1718,(void*)0,(void*)0,(void*)0,(void*)0},{&g_450[1],&g_768,(void*)0,(void*)0,&g_1615,(void*)0,(void*)0,(void*)0,&g_829,&g_829}},{{(void*)0,&g_1718,&g_450[1],(void*)0,&g_450[1],&g_1718,&g_829,&g_450[1],&g_829,&g_1718},{(void*)0,&g_238,&g_450[0],&g_238,(void*)0,(void*)0,&g_1718,(void*)0,(void*)0,&g_2039},{(void*)0,&g_1718,(void*)0,(void*)0,&g_2039,&g_829,&g_1615,&g_1615,&g_829,&g_2039},{&g_450[0],(void*)0,(void*)0,&g_450[0],(void*)0,&g_1718,&g_450[1],(void*)0,&g_450[1],&g_1718},{(void*)0,(void*)0,&g_829,&g_829,&g_450[1],&g_450[1],(void*)0,&g_450[1],&g_450[1],&g_829},{&g_1718,(void*)0,&g_1718,&g_450[0],&g_1615,&g_450[1],&g_450[1],&g_829,&g_829,&g_768},{&g_768,&g_450[1],(void*)0,(void*)0,&g_829,&g_2039,&g_2039,&g_829,(void*)0,(void*)0},{&g_829,&g_829,&g_1718,&g_238,&g_829,(void*)0,(void*)0,&g_450[1],&g_829,&g_1718},{&g_2039,&g_450[1],&g_829,(void*)0,(void*)0,&g_1615,(void*)0,(void*)0,&g_829,&g_450[1]}}};
    int32_t l_2200 = 0x7DABCE85L;
    uint32_t *l_2219[9];
    uint32_t **l_2218 = &l_2219[7];
    int32_t **l_2223 = (void*)0;
    const int16_t l_2238[6] = {0xC74EL,0xC74EL,0xC74EL,0xC74EL,0xC74EL,0xC74EL};
    uint32_t ** const *l_2253 = &g_717;
    int32_t l_2267 = 0xAF410467L;
    int32_t l_2303 = 0L;
    int32_t l_2304 = 1L;
    int32_t l_2305 = 0x5D5E21BEL;
    int32_t l_2306 = 1L;
    int32_t l_2309 = 5L;
    float l_2358 = 0x5.5p-1;
    float l_2387 = 0x8.5716D5p+34;
    uint32_t l_2449 = 18446744073709551614UL;
    const int32_t l_2450 = 1L;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_2219[i] = &g_2196;
    for (g_1953 = 0; (g_1953 <= 2); g_1953 += 1)
    { /* block id: 889 */
        int32_t *l_1957[8][9][3] = {{{&g_297,&g_104,&g_378},{&g_297,&g_767,&g_297},{&g_297,&g_104,&g_767},{&g_767,&g_767,&g_297},{(void*)0,&g_767,&g_767},{&g_297,&g_378,&g_297},{&g_104,&g_297,&g_297},{&g_767,&g_767,(void*)0},{&g_297,&g_378,&g_104}},{{&g_767,&g_104,&g_767},{&g_297,&g_767,&g_104},{&g_767,&g_104,&g_767},{&g_104,&g_767,&g_297},{&g_297,&g_104,&g_378},{(void*)0,&g_297,&g_104},{&g_767,&g_297,(void*)0},{&g_297,&g_104,&g_767},{&g_297,&g_378,(void*)0}},{{&g_297,&g_104,&g_104},{&g_378,&g_767,&g_378},{&g_378,&g_297,&g_297},{&g_378,&g_378,&g_767},{&g_297,&g_104,&g_104},{&g_767,&g_378,&g_767},{&g_297,&g_767,&g_104},{&g_378,&g_378,(void*)0},{&g_378,&g_104,&g_297}},{{&g_297,&g_378,&g_297},{&g_767,&g_297,&g_767},{&g_378,&g_378,&g_378},{&g_297,&g_104,&g_378},{&g_104,&g_767,&g_378},{&g_104,&g_767,&g_104},{&g_104,&g_378,&g_378},{&g_297,&g_104,&g_297},{&g_767,&g_378,&g_767}},{{&g_378,&g_378,&g_767},{&g_297,&g_767,&g_297},{&g_104,&g_378,(void*)0},{&g_767,&g_378,&g_378},{&g_104,&g_104,(void*)0},{&g_378,&g_767,&g_297},{&g_767,&g_297,&g_767},{&g_378,&g_104,&g_767},{(void*)0,&g_767,&g_297}},{{(void*)0,&g_104,&g_378},{&g_297,(void*)0,&g_104},{&g_378,&g_104,&g_378},{&g_767,(void*)0,&g_378},{&g_767,&g_104,&g_378},{&g_767,&g_767,&g_378},{&g_297,&g_104,&g_297},{&g_104,&g_297,&g_104},{&g_297,&g_767,&g_767}},{{(void*)0,&g_104,&g_767},{&g_378,&g_378,&g_767},{(void*)0,&g_378,&g_104},{&g_297,&g_767,&g_378},{&g_104,&g_378,&g_767},{&g_297,&g_378,(void*)0},{&g_767,&g_104,&g_104},{&g_767,&g_378,&g_297},{&g_767,&g_767,&g_297}},{{&g_378,&g_767,&g_297},{&g_297,&g_104,&g_104},{(void*)0,&g_378,(void*)0},{(void*)0,&g_767,&g_767},{&g_378,&g_767,&g_378},{&g_767,&g_104,&g_104},{&g_378,&g_378,&g_767},{&g_104,&g_297,&g_767},{&g_767,&g_378,&g_767}}};
        int32_t ***l_1984[10][10] = {{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186},{&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186,&g_186}};
        int8_t ***l_1995 = &g_224[0][0][0];
        int8_t ****l_1994 = &l_1995;
        float *****l_2016 = (void*)0;
        int8_t *l_2033 = (void*)0;
        float l_2047 = 0xD.B198C3p-48;
        uint16_t **l_2074 = (void*)0;
        uint16_t ***l_2073 = &l_2074;
        uint16_t l_2102 = 3UL;
        uint8_t *l_2105 = &g_1956;
        int16_t *l_2107 = &g_470;
        int16_t **l_2106 = &l_2107;
        uint16_t l_2111[2];
        uint8_t *l_2115[2];
        uint16_t l_2133[10] = {65535UL,65535UL,0xC44FL,65535UL,65535UL,0xC44FL,65535UL,65535UL,0xC44FL,65535UL};
        int32_t l_2190 = 0L;
        uint8_t l_2204 = 0x58L;
        uint64_t *l_2239 = &g_107;
        const int64_t *l_2241[10] = {(void*)0,&g_1022,&g_1022,(void*)0,&g_1022,&g_1022,(void*)0,&g_1022,&g_1022,(void*)0};
        const int64_t **l_2240 = &l_2241[3];
        int64_t l_2245 = (-1L);
        int32_t l_2254 = 0xC23FF39CL;
        const uint16_t *l_2285 = &g_2286;
        uint16_t l_2311[4][8];
        uint32_t l_2375 = 18446744073709551607UL;
        uint32_t l_2422 = 1UL;
        uint32_t l_2428 = 0x3E12FA88L;
        int64_t **l_2433 = &l_2167[1][4][9];
        int64_t ***l_2432[7] = {&l_2433,&l_2433,&l_2433,&l_2433,&l_2433,&l_2433,&l_2433};
        int16_t *l_2451 = &g_1871;
        int16_t l_2452 = 0x1292L;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2111[i] = 0x7DC1L;
        for (i = 0; i < 2; i++)
            l_2115[i] = &g_128;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 8; j++)
                l_2311[i][j] = 2UL;
        }
    }
    for (g_312 = 0; (g_312 <= 3); g_312 += 1)
    { /* block id: 1176 */
        int32_t l_2453 = 0L;
        int32_t *l_2454 = (void*)0;
        int32_t *l_2455 = &l_2303;
        int32_t *l_2456[10] = {&l_2306,&l_2306,&l_2114,&l_2306,&l_2306,&l_2114,&l_2306,&l_2306,&l_2114,&l_2306};
        int i;
        (*g_1581) ^= ((*l_2159) = g_1114[g_312]);
        g_2458--;
        for (g_378 = 3; (g_378 >= 0); g_378 -= 1)
        { /* block id: 1182 */
            float *l_2461 = &g_2356;
            uint16_t ** const **l_2472 = &g_2061[3];
            uint16_t ** const ***l_2471 = &l_2472;
            int32_t l_2479 = 0xDD80C4F8L;
            int i;
            (*l_2461) = ((*g_1093) = (g_2357[g_378] = 0x1.Bp-1));
            for (g_2042 = 6; (g_2042 >= 0); g_2042 -= 1)
            { /* block id: 1188 */
                uint64_t l_2478 = 0x83282815493A779ELL;
                (*l_2159) ^= (safe_rshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u((1L <= (safe_div_func_int64_t_s_s((((!((g_2469 == l_2471) | ((p_38 != (void*)0) < ((0xC9AAL == (((g_238 >= 0xE9A17C39DA706563LL) < (safe_div_func_uint32_t_u_u(((((~((*l_2455) = (g_450[0] = (safe_rshift_func_uint8_t_u_s(0UL, 7))))) , p_37) < 0x88DBL) | p_36), 0x043A8923L))) >= 0xACCFF6A6D04053A4LL)) || l_2478)))) ^ 3L) > p_36), l_2478))), l_2478)), 12));
            }
            l_2479 |= p_37;
        }
    }
    return (*l_1959);
}


/* ------------------------------------------ */
/* 
 * reads : g_450
 * writes:
 */
static int64_t  func_39(uint64_t * p_40, int8_t  p_41, int64_t * p_42, uint64_t * p_43)
{ /* block id: 884 */
    return (*p_42);
}


/* ------------------------------------------ */
/* 
 * reads : g_400 g_87 g_193 g_194 g_1581 g_297 g_791 g_717 g_718 g_383 g_238 g_1031 g_1032 g_1033 g_225 g_218 g_305 g_1081 g_360 g_1093 g_1543 g_75 g_1075 g_1399 g_998 g_1933 g_768 g_106 g_1953 g_1956
 * writes: g_238 g_87 g_297 g_193 g_791 g_378 g_412 g_218 g_106 g_1913 g_1075 g_1399 g_1933 g_768 g_1953
 */
static uint64_t * func_44(int64_t * p_45)
{ /* block id: 589 */
    int8_t l_1261 = 0x99L;
    uint16_t l_1282 = 3UL;
    int8_t *l_1284 = (void*)0;
    int32_t l_1318 = (-1L);
    uint32_t l_1327 = 0xD9FD8920L;
    int32_t *l_1328 = (void*)0;
    const uint16_t l_1329 = 0xB7CCL;
    int32_t l_1331 = (-9L);
    const int32_t **l_1346 = &g_193[3][1];
    int64_t **l_1347 = &g_400;
    const int32_t **** const *l_1348 = &g_1207;
    int32_t l_1374 = 1L;
    int32_t l_1375 = (-4L);
    int32_t l_1376 = 0L;
    int32_t l_1413 = 2L;
    int32_t l_1420[6] = {1L,1L,1L,1L,1L,1L};
    int16_t l_1465 = 0x96EDL;
    int8_t ***l_1529 = &g_224[0][0][0];
    int8_t ****l_1528 = &l_1529;
    uint8_t l_1614 = 0UL;
    uint64_t *l_1685 = &g_57[7][0];
    uint16_t * const **l_1738 = (void*)0;
    uint16_t * const ***l_1737 = &l_1738;
    uint16_t * const ****l_1736 = &l_1737;
    float ** const l_1763 = &g_1093;
    float ** const *l_1762 = &l_1763;
    int8_t **l_1773 = &g_225;
    uint32_t **l_1839 = &g_718;
    int32_t *l_1936 = &g_297;
    int32_t *l_1937 = &g_297;
    int32_t *l_1938 = &g_767;
    int32_t *l_1939 = (void*)0;
    int32_t *l_1940 = &l_1318;
    int32_t *l_1941 = &l_1420[0];
    int32_t *l_1942 = &g_767;
    int32_t *l_1943 = &l_1318;
    int32_t *l_1944[2];
    uint8_t l_1946 = 2UL;
    int i;
    for (i = 0; i < 2; i++)
        l_1944[i] = &l_1375;
    if ((safe_unary_minus_func_int64_t_s(((*g_400) = (l_1261 ^ (255UL >= 0x78L))))))
    { /* block id: 591 */
        int16_t *l_1265 = (void*)0;
        int32_t l_1266 = 0x2801839BL;
        uint8_t *l_1273[4];
        int32_t l_1283 = 1L;
        int8_t **l_1285 = &g_225;
        int8_t *l_1286 = &l_1261;
        int32_t *l_1345 = (void*)0;
        int32_t **l_1344 = &l_1345;
        int i;
        for (i = 0; i < 4; i++)
            l_1273[i] = &g_1114[2];
    }
    else
    { /* block id: 629 */
        uint32_t l_1391 = 0x9D1529FEL;
        int32_t l_1421 = 1L;
        int8_t ** const *l_1438 = (void*)0;
        int32_t l_1467 = 0xF974D6A0L;
        int32_t l_1470 = 0xB5AB777AL;
        int32_t l_1471 = 0x727F1740L;
        int32_t l_1474 = (-1L);
        int32_t *l_1582 = &l_1420[0];
        int8_t **l_1645 = &g_225;
        float ** const *l_1665 = (void*)0;
        float ** const **l_1664 = &l_1665;
        int32_t ****l_1854 = &g_620;
        int32_t *****l_1853 = &l_1854;
        int16_t * const l_1873 = &g_470;
        int16_t * const *l_1872 = &l_1873;
        int64_t l_1929[4][6][2] = {{{0L,0xFF3DE191B4E72EBFLL},{(-8L),0xB462233A06B6A541LL},{1L,(-8L)},{(-1L),6L},{(-1L),(-8L)},{1L,0xB462233A06B6A541LL}},{{(-8L),0xFF3DE191B4E72EBFLL},{0L,0x78409A57E5C41BF5LL},{0xB462233A06B6A541LL,0xB8583767D490AE26LL},{0xB8583767D490AE26LL,0xB8583767D490AE26LL},{0xB462233A06B6A541LL,0x78409A57E5C41BF5LL},{0L,0xFF3DE191B4E72EBFLL}},{{(-8L),0xB462233A06B6A541LL},{1L,(-8L)},{(-1L),6L},{(-1L),(-8L)},{1L,0xB462233A06B6A541LL},{(-8L),0xFF3DE191B4E72EBFLL}},{{0L,0x78409A57E5C41BF5LL},{0xB462233A06B6A541LL,0xB8583767D490AE26LL},{0xB8583767D490AE26LL,0xB8583767D490AE26LL},{0xB462233A06B6A541LL,0x78409A57E5C41BF5LL},{0L,0xFF3DE191B4E72EBFLL},{(-8L),0xB462233A06B6A541LL}}};
        uint32_t l_1931[4] = {18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL};
        int32_t *l_1932[7][8];
        int i, j, k;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 8; j++)
                l_1932[i][j] = &l_1376;
        }
        for (g_87 = 6; (g_87 == 60); g_87 = safe_add_func_int8_t_s_s(g_87, 2))
        { /* block id: 632 */
            int32_t l_1390 = 0x0D0B09B3L;
            uint8_t *l_1392 = &g_397;
            int32_t *l_1400 = &l_1390;
            uint16_t l_1428 = 0xF31EL;
            int32_t l_1466 = 0xC8F1F06BL;
            int32_t l_1473 = 4L;
            int32_t **l_1500 = (void*)0;
            int32_t ****l_1502[10][3] = {{(void*)0,&g_620,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_620,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_620,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_620,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_620,(void*)0},{(void*)0,(void*)0,(void*)0}};
            int8_t ****l_1530 = &l_1529;
            int32_t l_1533 = 1L;
            int64_t l_1586 = 0xDD7BC5B64F5821BBLL;
            float l_1597 = 0xC.5627C2p-90;
            float **l_1657 = &g_1093;
            int i, j;
        }
        for (l_1413 = 0; (l_1413 > (-25)); l_1413--)
        { /* block id: 829 */
            int32_t ** const *l_1852 = (void*)0;
            int32_t ** const **l_1851[10] = {&l_1852,&l_1852,&l_1852,&l_1852,&l_1852,&l_1852,&l_1852,&l_1852,&l_1852,&l_1852};
            int32_t ** const ** const *l_1850 = &l_1851[0];
            float **l_1865 = &g_1093;
            float ***l_1864 = &l_1865;
            uint32_t ** const l_1901 = &g_718;
            const int16_t l_1918 = 0xF195L;
            uint64_t l_1923[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1923[i] = 18446744073709551615UL;
            (*g_1581) &= ((*l_1582) = (**l_1346));
            (*l_1346) = &l_1470;
            for (g_297 = 0; (g_297 > 5); g_297++)
            { /* block id: 835 */
                const uint32_t *l_1841 = &g_17;
                const uint32_t **l_1840 = &l_1841;
                int32_t l_1863[6] = {0x3495CF97L,0x3495CF97L,0x3495CF97L,0x3495CF97L,0x3495CF97L,0x3495CF97L};
                float ***l_1866[2];
                int16_t *l_1867 = &g_412;
                int16_t * const l_1870 = &g_1871;
                int16_t * const *l_1869 = &l_1870;
                int16_t * const **l_1868 = &l_1869;
                const uint16_t * const l_1899[1] = {&g_1900};
                const uint16_t * const *l_1898 = &l_1899[0];
                const uint16_t * const **l_1897 = &l_1898;
                int32_t l_1904[5][3] = {{0x69469A2BL,0x69469A2BL,0x69469A2BL},{0xB00582D1L,0xD33DF602L,0xB00582D1L},{0x69469A2BL,0x69469A2BL,0x69469A2BL},{0xB00582D1L,0xD33DF602L,0xB00582D1L},{0x69469A2BL,0x69469A2BL,0x69469A2BL}};
                int32_t *****l_1911[8][6] = {{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,(void*)0,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854},{&l_1854,&l_1854,&l_1854,&l_1854,&l_1854,&l_1854}};
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1866[i] = &l_1865;
                for (g_791 = 1; (g_791 <= 5); g_791 += 1)
                { /* block id: 838 */
                    return p_45;
                }
                if ((((+(l_1839 != l_1840)) == ((**g_717) , ((((*l_1868) = (((((*l_1867) = (((0x84L == (safe_sub_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(((void*)0 != &l_1665), (safe_div_func_int64_t_s_s((((((safe_rshift_func_int16_t_s_s((l_1850 != l_1853), (safe_mod_func_int32_t_s_s((+(safe_unary_minus_func_int32_t_s((g_378 = ((safe_add_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s(l_1863[5], (**l_1346))), (*g_400))) , l_1863[5]))))), l_1863[0])))) ^ (****g_1031)) & l_1863[5]) , l_1864) == l_1866[1]), g_305)))), 0UL))) , l_1863[5]) >= g_1081[0])) != 0x443BL) , g_383) , (void*)0)) == l_1872) < l_1863[3]))) & (*l_1582)))
                { /* block id: 844 */
                    const uint32_t l_1880 = 0x41E0B86AL;
                    l_1375 &= (safe_mod_func_uint64_t_u_u(((-6L) | (safe_rshift_func_int8_t_s_s(((safe_lshift_func_int8_t_s_s((((l_1880 && l_1863[5]) , ((*l_1582) &= ((***g_1032) = (***g_1032)))) , (safe_add_func_int64_t_s_s(((**l_1346) || (((((((safe_mod_func_int64_t_s_s((p_45 == l_1685), ((**l_1347) = (((**l_1346) < (safe_mod_func_uint8_t_u_u(((safe_div_func_uint32_t_u_u(((l_1880 >= (**l_1346)) ^ l_1863[5]), l_1880)) , 7UL), l_1863[2]))) , (*g_400))))) || (*l_1582)) || (*g_400)) , l_1863[5]) , 0UL) && l_1880) >= (**l_1346))), 0xD768E5DBFF55C531LL))), l_1863[3])) != l_1863[4]), l_1880))), g_360));
                }
                else
                { /* block id: 849 */
                    uint32_t l_1893 = 0x4F5D3E8DL;
                    const uint16_t * const *l_1895 = (void*)0;
                    const uint16_t * const **l_1894 = &l_1895;
                    const uint16_t * const ***l_1896[5] = {&l_1894,&l_1894,&l_1894,&l_1894,&l_1894};
                    int32_t *****l_1912 = (void*)0;
                    uint8_t *l_1919 = (void*)0;
                    uint8_t *l_1920[6][5] = {{&g_299,&g_299,&l_1614,&g_299,&g_299},{&g_242,&g_397,&g_242,&g_242,&g_397},{&g_299,&l_1614,&l_1614,&g_299,&l_1614},{&g_397,&g_397,&g_1770,&g_397,&g_397},{&l_1614,&g_299,&l_1614,&l_1614,&g_299},{&g_397,&g_242,&g_242,&g_397,&g_242}};
                    int32_t l_1930 = 0L;
                    int i, j;
                    (**l_1865) = (l_1863[5] != (safe_sub_func_float_f_f(0xF.18CA73p-97, (((((l_1893 <= ((l_1897 = l_1894) == ((((*g_400) < (0L && ((void*)0 != l_1901))) , ((((safe_add_func_int32_t_s_s(((*l_1582) = (((l_1904[1][2] = 0xE2304402L) , &g_717) != &g_717)), l_1893)) != (*g_1581)) & 65535UL) | l_1863[1])) , &g_478))) , (-1L)) , (void*)0) != p_45) >= l_1863[5]))));
                    (*l_1582) &= (safe_div_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u((safe_mod_func_uint64_t_u_u((((l_1912 = l_1911[7][2]) != (g_1913 = (void*)0)) , g_1543), (safe_mul_func_uint8_t_u_u((g_1075 |= ((((void*)0 != &g_469) & g_75) | (safe_lshift_func_uint8_t_u_u(((l_1893 , ((-1L) | l_1893)) & l_1893), l_1918)))), (-1L))))), 7)), 0x96CF8DF883183E15LL));
                    for (g_1399 = 5; (g_1399 == (-14)); g_1399 = safe_sub_func_uint64_t_u_u(g_1399, 1))
                    { /* block id: 860 */
                        int8_t l_1926 = (-1L);
                        if ((*l_1582))
                            break;
                        l_1923[1]++;
                        l_1930 |= (((g_998 > (((l_1926 , &l_1893) != &g_1082) > ((safe_add_func_uint64_t_u_u(l_1926, l_1929[0][0][1])) <= ((*l_1582) |= (l_1348 != (void*)0))))) , &l_1926) == (*l_1645));
                        (*l_1582) ^= (l_1931[2] = (*g_1581));
                    }
                }
            }
            (*l_1582) &= (**l_1346);
        }
        g_1933--;
    }
    --l_1946;
    for (g_768 = 1; (g_768 <= 5); g_768 += 1)
    { /* block id: 877 */
        int32_t l_1949 = (-8L);
        int32_t l_1950 = 0x328C551AL;
        int32_t l_1951 = 0x3F9D02CEL;
        int32_t l_1952 = (-1L);
        int i;
        g_106[g_768] = g_106[g_768];
        if (l_1949)
            break;
        --g_1953;
        l_1949 = (g_1956 >= 2UL);
    }
    return l_1685;
}


/* ------------------------------------------ */
/* 
 * reads : g_17 g_57 g_75 g_72 g_107 g_106 g_104 g_129 g_128 g_186 g_87 g_194 g_198 g_197 g_218 g_229 g_242 g_230 g_231 g_225 g_299 g_305 g_312 g_298 g_297 g_359 g_380 g_383 g_397 g_238 g_296 g_452 g_471 g_474 g_477 g_360 g_400 g_469 g_378 g_450 g_412 g_1031 g_1032 g_1033 g_1074 g_791 g_1075 g_717 g_718 g_767
 * writes: g_75 g_87 g_104 g_107 g_128 g_106 g_129 g_57 g_72 g_192 g_198 g_197 g_218 g_224 g_242 g_299 g_305 g_312 g_298 g_360 g_304 g_380 g_383 g_400 g_297 g_186 g_412 g_238 g_397 g_230 g_452 g_471 g_477 g_231 g_469 g_378 g_470 g_767
 */
static int64_t * func_46(uint64_t * p_47, int8_t  p_48, int16_t  p_49)
{ /* block id: 3 */
    int8_t l_50 = 0x4BL;
    uint64_t *l_56 = &g_57[7][0];
    int32_t l_69 = 0L;
    int64_t *l_70 = (void*)0;
    int64_t *l_71 = &g_72;
    uint16_t *l_73 = (void*)0;
    uint16_t *l_74 = &g_75;
    int16_t *l_411 = &g_412;
    uint64_t *l_413 = &g_305;
    float l_490 = 0x1.Bp+1;
    int32_t l_517 = (-3L);
    int32_t *l_541[8];
    int8_t * const *l_544[6] = {&g_225,&g_225,&g_225,&g_225,&g_225,&g_225};
    int32_t * const *l_619[6];
    int32_t * const **l_618 = &l_619[4];
    const uint16_t l_621[2][4] = {{65533UL,65533UL,0x6CA2L,65533UL},{65533UL,0UL,0UL,65533UL}};
    uint32_t l_622 = 0x3F00FE59L;
    float l_757 = 0x0.9p-1;
    uint8_t l_813 = 1UL;
    int8_t l_861[2][2] = {{(-1L),(-1L)},{(-1L),(-1L)}};
    float l_886 = 0x9.5p+1;
    uint8_t l_987 = 0x93L;
    int8_t l_1005 = 0L;
    int8_t l_1036 = (-1L);
    uint16_t **l_1137 = &l_74;
    uint16_t ***l_1136 = &l_1137;
    uint16_t **** const l_1135 = &l_1136;
    uint16_t **** const *l_1134 = &l_1135;
    int32_t l_1175 = (-10L);
    uint16_t l_1177 = 65535UL;
    uint8_t l_1237 = 255UL;
    int32_t ****l_1257 = &g_620;
    int i, j;
    for (i = 0; i < 8; i++)
        l_541[i] = (void*)0;
    for (i = 0; i < 6; i++)
        l_619[i] = &l_541[5];
lbl_476:
    l_50 = p_48;
    if (func_51(g_17, l_56, (l_413 = ((safe_sub_func_int16_t_s_s(((*l_411) = (p_48 | func_60(l_50, ((safe_rshift_func_uint16_t_u_u(((*l_74) = ((safe_mod_func_int8_t_s_s(((l_50 || (((l_69 = (l_50 , l_50)) > (l_70 == l_71)) , 0xFAL)) , p_49), 0x08L)) , g_57[7][0])), 8)) == 255UL), &g_57[7][0], p_49))), 0x31BCL)) , p_47)), p_49))
    { /* block id: 200 */
        float *l_475 = &g_106[2];
        const uint16_t * volatile ***l_479 = &g_477;
        int64_t l_480 = 0x701B48D667C5F155LL;
        uint32_t l_487 = 0x8CAC2AD3L;
        int64_t l_491 = 0x7F192CDCDDC66695LL;
        float ***l_512 = (void*)0;
        float *l_524 = &l_490;
        int32_t *l_561 = &g_104;
        int64_t l_586 = (-1L);
        int8_t ***l_610[7] = {&g_224[0][0][0],&g_224[0][0][0],&g_224[1][1][1],&g_224[0][0][0],&g_224[0][0][0],&g_224[1][1][1],&g_224[0][0][0]};
        float l_740 = (-0x7.Dp+1);
        int32_t l_786 = 0L;
        int i;
        (*l_475) = 0xA.1C6416p-11;
        if (g_297)
            goto lbl_476;
        (*l_479) = g_477;
        if ((((*l_71) = (l_480 , (((g_360 , ((safe_add_func_int32_t_s_s(p_48, 0x3F51C39FL)) || 0x60L)) < (!(~(safe_div_func_int64_t_s_s(((l_487 && (safe_lshift_func_uint8_t_u_u((0x2EL >= 0xCEL), 4))) && ((g_297 , 65531UL) || l_69)), g_87))))) < l_491))) , 0xFD9B3567L))
        { /* block id: 205 */
            int32_t *l_492 = (void*)0;
            int32_t l_515 = 0x5DA8ABE5L;
            uint32_t *l_534 = &g_87;
            int8_t **l_542[2];
            int16_t *l_559 = &g_412;
            int64_t **l_568 = &l_71;
            int i;
            for (i = 0; i < 2; i++)
                l_542[i] = &g_225;
            if (p_48)
            { /* block id: 206 */
                float **l_511 = &l_475;
                float ***l_510 = &l_511;
                int32_t l_522 = (-5L);
                int32_t *l_523 = &g_469;
                l_492 = l_492;
                for (g_312 = (-21); (g_312 == 16); ++g_312)
                { /* block id: 210 */
                    int64_t l_499 = 0x5D7DF1F51B59365ALL;
                    int16_t *l_506 = &g_412;
                    int32_t **l_516 = &l_492;
                    l_492 = (((safe_mul_func_int16_t_s_s(((((((0x04L || p_49) , (((*g_225) == (safe_sub_func_int32_t_s_s(l_499, ((safe_add_func_uint32_t_u_u(p_48, 1UL)) < (*g_225))))) , ((safe_mod_func_uint64_t_u_u(((safe_rshift_func_int16_t_s_s(((*l_506) = (((void*)0 != l_506) == (*g_400))), g_471)) && p_48), 3L)) > l_499))) & l_50) , 0x27D4993E6F23459CLL) ^ 0x79D9A9554127F8BDLL) , l_487), (-2L))) || l_491) , l_492);
                    if (p_49)
                        break;
                    l_517 ^= ((((safe_mod_func_int64_t_s_s(((((-(((***l_510) = ((l_510 == l_512) , 0x0.Fp-1)) != p_48)) , p_49) ^ p_48) || (safe_lshift_func_uint8_t_u_s(((g_469 > ((*l_56) = g_299)) < (((((l_515 <= l_69) , 18446744073709551614UL) , l_516) != (void*)0) > 0x01CC029363A9B49CLL)), 2))), p_49)) | (-1L)) , &l_515) == (void*)0);
                    for (g_87 = 0; (g_87 <= 30); ++g_87)
                    { /* block id: 219 */
                        (*l_516) = (*l_516);
                    }
                }
                g_304 = ((***l_510) = (safe_sub_func_float_f_f(0x7.ECBDB1p-97, (p_48 != ((((*g_230) = (**l_510)) != (((*l_523) = l_522) , l_524)) < p_48)))));
            }
            else
            { /* block id: 227 */
                int32_t l_529 = 0L;
                uint32_t *l_533 = &g_17;
                uint32_t **l_532[8][8][4] = {{{(void*)0,(void*)0,&l_533,(void*)0},{(void*)0,(void*)0,&l_533,&l_533},{&l_533,(void*)0,(void*)0,(void*)0},{&l_533,&l_533,&l_533,&l_533},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_533,(void*)0,&l_533,(void*)0},{&l_533,(void*)0,&l_533,(void*)0},{(void*)0,(void*)0,&l_533,&l_533}},{{&l_533,&l_533,(void*)0,(void*)0},{&l_533,(void*)0,&l_533,&l_533},{&l_533,(void*)0,&l_533,(void*)0},{&l_533,(void*)0,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,&l_533,(void*)0},{&l_533,&l_533,(void*)0,&l_533},{&l_533,&l_533,(void*)0,&l_533}},{{&l_533,&l_533,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,(void*)0},{&l_533,&l_533,&l_533,&l_533}},{{(void*)0,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,(void*)0},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,&l_533,(void*)0},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533}},{{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,(void*)0},{&l_533,(void*)0,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533}},{{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,&l_533},{&l_533,&l_533,&l_533,(void*)0},{(void*)0,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,(void*)0,&l_533}},{{&l_533,(void*)0,(void*)0,&l_533},{&l_533,(void*)0,&l_533,(void*)0},{(void*)0,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{(void*)0,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,(void*)0}},{{&l_533,&l_533,(void*)0,&l_533},{&l_533,(void*)0,&l_533,&l_533},{&l_533,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,(void*)0},{&l_533,&l_533,&l_533,&l_533},{(void*)0,&l_533,&l_533,&l_533},{&l_533,&l_533,(void*)0,(void*)0},{&l_533,&l_533,&l_533,&l_533}}};
                int i, j, k;
                if ((safe_div_func_int8_t_s_s((g_380 , p_48), (0x8749CAFEL & ((p_48 ^ (safe_lshift_func_uint8_t_u_s((l_529 != ((((l_534 = &l_487) != (void*)0) ^ p_49) || (l_529 >= p_48))), (*g_225)))) != (*g_400))))))
                { /* block id: 229 */
                    const int32_t l_543 = 8L;
                    int32_t l_560[6][10][4] = {{{1L,0xE8C53431L,0x7B63AC82L,1L},{0xD68B4F45L,(-6L),(-10L),(-1L)},{0L,1L,0xD92BC317L,(-8L)},{0x7A7FA0BEL,0x8F9BC03FL,1L,0x231AD4E4L},{0x0C5FCF6FL,0xE8C53431L,0L,0x89E09741L},{(-2L),0L,0xC09BBEDBL,0x7B63AC82L},{(-2L),(-1L),0xE8C53431L,1L},{0L,0x59855A01L,0xCF4EA918L,0x981995CFL},{0x70637105L,0x5D78AAD2L,(-6L),5L},{(-4L),0x8F9BC03FL,0xE7EE68A2L,0L}},{{0x5D78AAD2L,(-1L),0xD6F87233L,(-6L)},{0x409C1107L,(-2L),(-2L),0x409C1107L},{0xE8C53431L,0L,1L,(-6L)},{0L,0xC09BBEDBL,0x99842766L,0x5E28D0C8L},{0L,0L,1L,0x5E28D0C8L},{0x70637105L,0xC09BBEDBL,0x4AFD102CL,(-6L)},{0L,0L,(-4L),0x409C1107L},{(-1L),(-2L),0x3D3A7B81L,(-6L)},{0L,(-1L),0L,0L},{0x99842766L,0x8F9BC03FL,1L,5L}},{{1L,0x5D78AAD2L,0x99842766L,0x981995CFL},{0x8F9BC03FL,0x59855A01L,0xD6F87233L,1L},{0xC09BBEDBL,(-1L),0x099B1CD3L,0x7B63AC82L},{0x7B63AC82L,0L,0x7A7FA0BEL,0x89E09741L},{0x5D78AAD2L,0xE8C53431L,(-10L),0x231AD4E4L},{0L,(-1L),1L,0x3A6DAAF6L},{0x99842766L,0x409C1107L,0x5D78AAD2L,(-6L)},{(-2L),(-5L),0xE8C53431L,0x3D3A7B81L},{7L,(-1L),0x3D3A7B81L,1L},{1L,0xA7302AAAL,0x4AFD102CL,1L}},{{(-1L),0x8F9BC03FL,(-6L),0x8F9BC03FL},{1L,0L,(-10L),0xD68B4F45L},{0x8F9BC03FL,(-1L),(-2L),0L},{0xE8C53431L,7L,(-1L),0x7B63AC82L},{0xE8C53431L,(-5L),(-2L),(-5L)},{0x8F9BC03FL,0x7B63AC82L,(-10L),0x5E28D0C8L},{1L,0x48AB64A8L,(-6L),3L},{(-1L),0x409C1107L,0x4AFD102CL,0x89E09741L},{1L,0L,0x3D3A7B81L,(-4L)},{7L,(-2L),0xE8C53431L,0L}},{{(-2L),5L,0x5D78AAD2L,1L},{0x99842766L,0x5D78AAD2L,1L,0xCF4EA918L},{0L,5L,(-10L),0x981995CFL},{0x5D78AAD2L,0xA7302AAAL,0x7A7FA0BEL,(-4L)},{0x7B63AC82L,7L,0x099B1CD3L,0x409C1107L},{0xC09BBEDBL,0L,0xD6F87233L,1L},{0x8F9BC03FL,0xE8C53431L,0x99842766L,0x3A6DAAF6L},{1L,0L,1L,3L},{0x99842766L,0xC09BBEDBL,0L,(-5L)},{0L,1L,0x3D3A7B81L,0x3D3A7B81L}},{{(-1L),(-1L),(-4L),(-4L)},{0L,0x99842766L,0x48AB64A8L,0xE8C53431L},{4L,0L,0xD68B4F45L,0x48AB64A8L},{0L,0L,0L,0xE8C53431L},{0L,0x99842766L,(-10L),0x981995CFL},{(-1L),0xE4863396L,3L,0x099B1CD3L},{(-2L),0x3D3A7B81L,(-6L),0x213781D6L},{5L,(-8L),1L,0xCF4EA918L},{0x981995CFL,0xC7C5AB88L,(-5L),0L},{4L,(-1L),(-7L),0xA7302AAAL}}};
                    int i, j, k;
                    g_104 = (l_69 == p_49);
                    if (((((safe_lshift_func_int16_t_s_s((((*g_225) ^= p_48) | (((g_378 ^= ((0x76E4D17AL ^ g_128) == (safe_div_func_int32_t_s_s(0x9E5D797AL, 0x23F1CE54L)))) & ((((safe_mul_func_uint8_t_u_u(g_469, ((p_49 < 4UL) != 0xE06FL))) > (*g_400)) == l_529) == (*g_400))) <= p_49)), 1)) , l_541[3]) == (void*)0) != 246UL))
                    { /* block id: 233 */
                        return p_47;
                    }
                    else
                    { /* block id: 235 */
                        l_529 = l_529;
                        l_542[0] = &g_225;
                    }
                    l_515 |= (g_297 ^= 0xC296BDF4L);
                    if (l_491)
                    { /* block id: 241 */
                        l_515 |= (&g_225 != ((l_543 >= (0xE963L == 0x452DL)) , l_544[1]));
                        l_541[1] = &l_529;
                    }
                    else
                    { /* block id: 244 */
                        int64_t l_547 = 0L;
                        const float ****l_563 = &g_229;
                        const float *****l_562 = &l_563;
                        int32_t l_579 = 0xE4F518D2L;
                        l_560[4][0][3] = (safe_sub_func_int16_t_s_s(l_547, (((safe_lshift_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s(((safe_div_func_int16_t_s_s((((*l_56) = (safe_sub_func_int16_t_s_s((((((*g_400) < l_547) >= (~g_450[0])) , (p_48 < (safe_rshift_func_int16_t_s_u(0L, 15)))) <= l_487), (l_480 | (l_559 != l_73))))) <= p_48), g_412)) | l_547), p_49)) > g_397), 0)) < p_48) | p_48)));
                        l_561 = &l_529;
                        (*l_562) = &g_229;
                        l_529 = ((0x5A56L == ((safe_sub_func_uint8_t_u_u(l_547, (1UL == (safe_mod_func_int32_t_s_s((&g_400 != l_568), (safe_sub_func_int32_t_s_s((safe_sub_func_int64_t_s_s(((safe_lshift_func_uint8_t_u_s(p_49, 6)) || (((safe_mod_func_int16_t_s_s(((l_579 = ((*l_534) = ((safe_rshift_func_int16_t_s_u((-1L), 5)) , p_49))) >= (p_49 > 0x51F9B399L)), 0x8B1EL)) , 0UL) && l_560[4][0][3])), (*l_561))), 0L))))))) && p_49)) | l_543);
                    }
                }
                else
                { /* block id: 253 */
                    for (g_305 = 0; (g_305 < 50); ++g_305)
                    { /* block id: 256 */
                        float l_587 = 0x0.6p+1;
                        int32_t l_588[6][7] = {{(-6L),0x3F9B621EL,1L,1L,0x3F9B621EL,(-6L),0xB286A2AFL},{0xC898E97FL,2L,0x1D5CEE56L,5L,5L,0x1D5CEE56L,2L},{0x3F9B621EL,0xB286A2AFL,(-6L),0x3F9B621EL,1L,1L,0x3F9B621EL},{8L,2L,8L,0xBFA07D45L,2L,(-3L),(-3L)},{(-5L),0x3F9B621EL,(-1L),0x3F9B621EL,(-5L),(-1L),0xCFCAF57FL},{5L,(-3L),0xBFA07D45L,5L,0xBFA07D45L,(-3L),5L}};
                        int i, j;
                        l_588[0][3] &= (safe_mul_func_int16_t_s_s(g_380, (((*l_71) = ((4294967293UL >= ((safe_mul_func_uint8_t_u_u((p_48 | (1UL >= 0UL)), ((l_586 ^ g_75) | (p_49 < (&g_224[1][0][8] != (void*)0))))) , 4294967286UL)) != p_48)) >= g_107)));
                        (*l_475) = (**g_230);
                    }
                    for (p_49 = (-7); (p_49 == 12); p_49++)
                    { /* block id: 263 */
                        int32_t **l_591 = &l_492;
                        (*l_591) = &l_529;
                        return &g_72;
                    }
                }
                return &g_72;
            }
        }
        else
        { /* block id: 270 */
            uint8_t *l_603 = &g_360;
            uint32_t *l_606 = &g_87;
            int32_t l_607[7][6] = {{(-1L),0x031FD708L,(-1L),0x527552E1L,0x527552E1L,(-1L)},{0xA77C7C7AL,0xA77C7C7AL,0x527552E1L,0x91578055L,0x527552E1L,0xA77C7C7AL},{0x527552E1L,0x031FD708L,0x91578055L,0x91578055L,0x031FD708L,0x527552E1L},{0xA77C7C7AL,0x527552E1L,0x91578055L,0x527552E1L,0xA77C7C7AL,0xA77C7C7AL},{(-1L),0x527552E1L,0x527552E1L,(-1L),0x031FD708L,(-1L)},{(-1L),0x031FD708L,(-1L),0x527552E1L,0x527552E1L,(-1L)},{0xA77C7C7AL,0xA77C7C7AL,0x527552E1L,0x91578055L,0x527552E1L,0xA77C7C7AL}};
            int8_t ****l_611 = &l_610[1];
            int16_t *l_623 = &g_470;
            int64_t l_631 = 0x14EF60A16BF027EFLL;
            uint16_t **l_706[2];
            uint16_t ***l_705 = &l_706[0];
            uint16_t ****l_704 = &l_705;
            uint16_t *****l_703 = &l_704;
            int32_t l_714 = 6L;
            uint16_t * const l_758 = (void*)0;
            uint32_t **l_802[8][8][4] = {{{&g_718,&l_606,&g_718,(void*)0},{(void*)0,&g_718,&g_718,&g_718},{&g_718,(void*)0,&g_718,&l_606},{&l_606,&g_718,&g_718,&l_606},{&g_718,&l_606,&g_718,&g_718},{(void*)0,&l_606,(void*)0,(void*)0},{&g_718,&g_718,&g_718,&g_718},{(void*)0,&g_718,&l_606,&l_606}},{{(void*)0,(void*)0,&g_718,(void*)0},{&g_718,(void*)0,(void*)0,(void*)0},{&g_718,(void*)0,&g_718,&l_606},{&g_718,&g_718,&l_606,&g_718},{&g_718,&g_718,&l_606,(void*)0},{&g_718,&l_606,&l_606,&g_718},{&g_718,&l_606,&l_606,&l_606},{&g_718,&g_718,&g_718,&l_606}},{{&l_606,(void*)0,&g_718,&g_718},{(void*)0,&g_718,(void*)0,(void*)0},{&g_718,&g_718,&g_718,&g_718},{(void*)0,&g_718,(void*)0,&g_718},{&g_718,&g_718,&g_718,&g_718},{(void*)0,&l_606,&g_718,&l_606},{&g_718,(void*)0,&l_606,(void*)0},{&g_718,&g_718,&l_606,(void*)0}},{{&g_718,&g_718,&g_718,&l_606},{&g_718,(void*)0,&l_606,(void*)0},{&g_718,&l_606,&l_606,&l_606},{&g_718,&g_718,&g_718,&g_718},{(void*)0,&g_718,&g_718,&g_718},{&g_718,(void*)0,(void*)0,&g_718},{(void*)0,(void*)0,&g_718,&g_718},{&g_718,&l_606,&g_718,&g_718}},{{&g_718,&g_718,(void*)0,&l_606},{&g_718,&l_606,&g_718,&g_718},{(void*)0,(void*)0,&g_718,(void*)0},{&g_718,&l_606,&l_606,&g_718},{(void*)0,&l_606,(void*)0,(void*)0},{&l_606,&l_606,&g_718,&l_606},{&g_718,(void*)0,(void*)0,&l_606},{&l_606,&g_718,&g_718,(void*)0}},{{(void*)0,&g_718,&g_718,&l_606},{&g_718,(void*)0,&l_606,&l_606},{&g_718,&l_606,&g_718,(void*)0},{&l_606,&l_606,&g_718,&g_718},{&g_718,&l_606,&g_718,(void*)0},{(void*)0,(void*)0,&l_606,&g_718},{(void*)0,&l_606,(void*)0,&l_606},{(void*)0,&g_718,&l_606,&g_718}},{{&g_718,&l_606,&g_718,&g_718},{&g_718,(void*)0,&l_606,&g_718},{&l_606,(void*)0,&g_718,&g_718},{&g_718,&g_718,(void*)0,&g_718},{&g_718,&g_718,&g_718,&l_606},{&g_718,&l_606,&g_718,(void*)0},{(void*)0,(void*)0,(void*)0,&l_606},{&l_606,&g_718,(void*)0,(void*)0}},{{(void*)0,&g_718,&g_718,(void*)0},{&g_718,(void*)0,&g_718,&l_606},{&g_718,&l_606,(void*)0,&g_718},{&g_718,&g_718,&g_718,&g_718},{&l_606,&g_718,&l_606,&g_718},{&g_718,&g_718,&g_718,&g_718},{&g_718,(void*)0,&l_606,&l_606},{(void*)0,&g_718,(void*)0,(void*)0}}};
            int16_t l_817 = 1L;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_706[i] = (void*)0;
        }
    }
    else
    { /* block id: 388 */
        float l_836 = 0x9.EF40D4p+10;
        uint8_t *l_837 = &g_397;
        int16_t l_862 = (-1L);
        int64_t *l_878[6][1];
        int32_t l_920 = (-1L);
        int32_t l_922 = (-5L);
        int32_t l_928[4][9][7] = {{{(-4L),0x4B5C1FBFL,1L,0xDB16CA4AL,0xA9BD6D27L,0xA9BD6D27L,0xB70C0268L},{0xA43E9428L,1L,0xA43E9428L,0xA109E450L,(-8L),0xDDAE0E48L,0xD4C45BEEL},{0xCE30361CL,(-7L),0x7C23F507L,0xE718E84CL,(-4L),0xE718E84CL,0x7C23F507L},{(-8L),(-8L),(-1L),0xEBB5E487L,0x2462E8CAL,0xDDAE0E48L,1L},{0x4B5C1FBFL,1L,0xDB16CA4AL,0xA9BD6D27L,0xA9BD6D27L,0xDB16CA4AL,1L},{(-1L),0L,(-8L),0xA43E9428L,0x2462E8CAL,0xA109E450L,0xEBB5E487L},{0xDB16CA4AL,0xCE30361CL,(-4L),1L,(-4L),0xCE30361CL,0xDB16CA4AL},{0xEBB5E487L,0xA109E450L,0x2462E8CAL,0xA43E9428L,(-8L),0L,(-1L)},{1L,0xDB16CA4AL,0xA9BD6D27L,0xA9BD6D27L,0xDB16CA4AL,1L,0x4B5C1FBFL}},{{1L,0xDDAE0E48L,0x2462E8CAL,0xEBB5E487L,(-1L),(-8L),(-8L)},{0x7C23F507L,0xE718E84CL,(-4L),0xE718E84CL,0x7C23F507L,(-7L),0xCE30361CL},{0xD4C45BEEL,0xDDAE0E48L,(-8L),0xA109E450L,0xA43E9428L,1L,0xA43E9428L},{0xB70C0268L,0xDB16CA4AL,0xDB16CA4AL,0xB70C0268L,0xE718E84CL,(-4L),(-1L)},{0xD4C45BEEL,0xA109E450L,(-1L),(-1L),(-1L),(-1L),(-1L)},{0x7C23F507L,0xCE30361CL,0x7C23F507L,(-4L),1L,0xA9BD6D27L,(-1L)},{1L,0L,0xA43E9428L,(-1L),0xF4F7A6FBL,(-1L),0xA43E9428L},{1L,1L,0xE718E84CL,0x4B5C1FBFL,(-10L),0xA9BD6D27L,0xCE30361CL},{0xEBB5E487L,(-8L),(-1L),0xDDAE0E48L,0xDDAE0E48L,(-1L),(-8L)}},{{0xDB16CA4AL,(-7L),1L,0x7C23F507L,(-10L),(-4L),0x4B5C1FBFL},{(-1L),1L,0xF4F7A6FBL,(-8L),0xF4F7A6FBL,1L,(-1L)},{0x4B5C1FBFL,(-4L),(-10L),0x7C23F507L,1L,(-7L),0xDB16CA4AL},{(-8L),(-1L),0xDDAE0E48L,0xDDAE0E48L,(-1L),(-8L),0xEBB5E487L},{0xCE30361CL,0xA9BD6D27L,(-10L),0x4B5C1FBFL,0xE718E84CL,1L,1L},{0xA43E9428L,(-1L),0xF4F7A6FBL,(-1L),0xA43E9428L,0L,1L},{(-1L),0xA9BD6D27L,1L,(-4L),0x7C23F507L,0xCE30361CL,0x7C23F507L},{(-1L),(-1L),(-1L),(-1L),(-1L),0xA109E450L,0xD4C45BEEL},{(-1L),(-4L),0xE718E84CL,0xB70C0268L,0xDB16CA4AL,0xDB16CA4AL,0xB70C0268L}},{{0xA43E9428L,1L,0xA43E9428L,0xA109E450L,(-8L),0xDDAE0E48L,0xD4C45BEEL},{0xCE30361CL,(-10L),0xA9BD6D27L,0xCE30361CL,(-1L),0xCE30361CL,0xA9BD6D27L},{(-1L),(-1L),1L,0xA109E450L,0xF4F7A6FBL,(-1L),0xEBB5E487L},{(-4L),0xE718E84CL,0xB70C0268L,0xDB16CA4AL,0xDB16CA4AL,0xB70C0268L,0xE718E84CL},{(-1L),0x2462E8CAL,(-1L),0xDDAE0E48L,0xF4F7A6FBL,0L,0xA109E450L},{0xB70C0268L,0x4B5C1FBFL,(-1L),0xE718E84CL,(-1L),0x4B5C1FBFL,0xB70C0268L},{0xA109E450L,0L,0xF4F7A6FBL,0xDDAE0E48L,(-1L),0x2462E8CAL,(-1L)},{0xE718E84CL,0xB70C0268L,0xDB16CA4AL,0xDB16CA4AL,0xB70C0268L,0xE718E84CL,(-4L)},{0xEBB5E487L,(-1L),0xF4F7A6FBL,0xA109E450L,1L,(-1L),(-1L)}}};
        uint8_t l_949[3][3][2] = {{{0UL,0x53L},{0xD1L,0x53L},{0UL,0xD1L}},{{0x10L,0x10L},{0x10L,0xD1L},{0UL,0x53L}},{{0xD1L,0x53L},{0UL,0xD1L},{0x10L,0x10L}}};
        const int32_t ***l_986 = (void*)0;
        const int32_t ****l_985 = &l_986;
        int16_t l_1019[1][1][3];
        int32_t l_1063 = 0xB2E83D57L;
        int32_t *l_1086[4] = {&l_922,&l_922,&l_922,&l_922};
        float **l_1185 = &g_1093;
        float ***l_1184 = &l_1185;
        float ****l_1183 = &l_1184;
        float *****l_1182 = &l_1183;
        int32_t ****l_1206 = &g_620;
        int32_t l_1215 = 0x35B13D35L;
        int i, j, k;
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 1; j++)
                l_878[i][j] = &g_450[2];
        }
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 3; k++)
                    l_1019[i][j][k] = 0x7AB5L;
            }
        }
        g_297 = (((*g_225) , ((*l_837) = (safe_div_func_int64_t_s_s((*g_400), 5UL)))) <= p_49);
        for (g_452 = 0; (g_452 < 40); g_452 = safe_add_func_uint8_t_u_u(g_452, 7))
        { /* block id: 393 */
            float *l_855 = &g_304;
            float **l_854 = &l_855;
            float ***l_853 = &l_854;
            float ***l_857 = &l_854;
            float ****l_856 = &l_857;
            int32_t l_863 = (-1L);
            uint32_t *** const l_875 = (void*)0;
            int32_t l_896 = 0x828773A4L;
            int32_t l_913[8][7][1];
            int64_t *l_950 = &g_768;
            float l_1007 = 0x4.8F4FDBp+23;
            int32_t l_1079 = 6L;
            float *l_1092 = &l_836;
            uint32_t l_1112[5][3] = {{4294967295UL,0xCDE4106AL,0xCDE4106AL},{4294967295UL,0xCDE4106AL,0xCDE4106AL},{4294967295UL,0xCDE4106AL,0xCDE4106AL},{4294967295UL,0xCDE4106AL,0xCDE4106AL},{4294967295UL,0xCDE4106AL,0xCDE4106AL}};
            int32_t *l_1119[4];
            uint64_t l_1128 = 0xA5EAD606B4BEC153LL;
            int16_t *l_1143 = &l_1019[0][0][1];
            const float ****l_1188 = &g_229;
            const float *****l_1187 = &l_1188;
            int i, j, k;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 7; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_913[i][j][k] = 1L;
                }
            }
            for (i = 0; i < 4; i++)
                l_1119[i] = (void*)0;
        }
    }
    if ((+((safe_div_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u((p_48 , (safe_lshift_func_uint16_t_u_u((((0x37L ^ ((((((*g_225) = (safe_rshift_func_int8_t_s_s(((safe_lshift_func_int8_t_s_s((*g_225), (****g_1031))) != (l_1237 >= (p_48 && ((safe_add_func_int32_t_s_s((((safe_mod_func_int16_t_s_s((safe_rshift_func_int8_t_s_s((p_48 |= (safe_add_func_int16_t_s_s(6L, (((g_104 = ((*g_400) != (g_1074 , g_791))) == 4294967289UL) || (-3L))))), 0)), g_1075)) & (**g_717)) && 1UL), (*g_718))) != p_49)))), 4))) , 0x56F42BE025481CCBLL) && g_72) ^ p_49) != 0x9AL)) != 0x157D4794L) | 1UL), 5))), p_49)), 0xB43431BBL)), p_49)), 0xDFCCL)) , 0x6BA432060EB82025LL)))
    { /* block id: 578 */
        return l_71;
    }
    else
    { /* block id: 580 */
        int32_t *l_1249 = &l_1175;
        int32_t **l_1248 = &l_1249;
        int32_t l_1252 = 0x610CAE31L;
        int32_t ****l_1255 = &g_620;
        int32_t *****l_1256 = &l_1255;
        float **l_1258 = &g_1093;
        int32_t l_1259 = (-5L);
        l_1259 &= (safe_mul_func_int8_t_s_s((((*l_1248) = &g_1081[0]) != &g_1081[0]), (((-7L) < ((safe_mod_func_int16_t_s_s(l_1252, (((g_470 = 0L) < (((((*l_1256) = l_1255) != (l_1257 = ((p_48 == (**g_230)) , l_1257))) , l_1258) == (void*)0)) || p_49))) ^ (**g_1033))) | 0x6115L)));
        g_767 &= p_49;
    }
    return p_47;
}


/* ------------------------------------------ */
/* 
 * reads : g_107 g_238 g_230 g_231 g_106 g_397 g_312 g_229 g_452 g_471 g_474
 * writes: g_107 g_238 g_106 g_397 g_312 g_230 g_452 g_304 g_471
 */
static int32_t  func_51(const uint16_t  p_52, uint64_t * p_53, uint64_t * p_54, float  p_55)
{ /* block id: 168 */
    int64_t **l_417[6];
    int32_t l_432 = 0x008151DFL;
    int32_t l_440 = 1L;
    int32_t l_443 = 0xE135BE1AL;
    int32_t l_446 = 0xD23BE1CAL;
    int32_t l_447 = (-1L);
    int32_t l_448 = 0x18EE1040L;
    int32_t l_449 = 0xC7548CFDL;
    int32_t l_451 = 0xBF9BF29FL;
    int32_t ***l_456 = (void*)0;
    float *l_457 = &g_106[2];
    float *l_458 = &g_304;
    int32_t *l_459 = &l_440;
    int32_t *l_460 = &g_104;
    int32_t *l_461 = &g_104;
    int32_t *l_462 = &l_446;
    int32_t *l_463 = &l_449;
    int32_t *l_464 = &l_440;
    int32_t *l_465 = (void*)0;
    int32_t *l_466 = &g_104;
    int32_t *l_467 = &l_447;
    int32_t *l_468[8][9] = {{&l_446,&g_297,&g_297,&l_446,&l_446,&g_297,&g_297,&l_446,&l_446},{&g_297,&g_297,&g_297,&g_297,&g_297,&g_297,&g_297,&g_297,&g_297},{&l_446,&l_446,&g_297,&g_297,&l_446,&l_446,&g_297,&g_297,&l_446},{&l_449,&g_297,&l_449,&g_297,&l_449,&g_297,&l_449,&g_297,&l_449},{&l_446,&g_297,&g_297,&l_446,&l_446,&g_297,&g_297,&l_446,&l_446},{&g_297,&g_297,&g_297,&g_297,&g_297,&g_297,&g_297,&g_297,&g_297},{&l_446,&l_446,&g_297,&g_297,&l_446,&l_446,&g_297,&g_297,&l_446},{&l_449,&g_297,&l_449,&g_297,&l_449,&g_297,&l_449,&g_297,&l_449}};
    int i, j;
    for (i = 0; i < 6; i++)
        l_417[i] = (void*)0;
    for (g_107 = (-11); (g_107 <= 28); g_107 = safe_add_func_int64_t_s_s(g_107, 2))
    { /* block id: 171 */
        float l_416 = 0x1.32CEBEp+95;
        int32_t l_420 = (-5L);
        int32_t *l_441 = &l_432;
        int32_t *l_442 = &g_104;
        int32_t *l_444 = (void*)0;
        int32_t *l_445[5] = {&l_432,&l_432,&l_432,&l_432,&l_432};
        int i;
        l_416 = 0x3.0C7783p+23;
        for (g_238 = 0; (g_238 <= 1); g_238 += 1)
        { /* block id: 175 */
            float *l_429 = (void*)0;
            float *l_430 = (void*)0;
            float *l_431[8] = {&g_106[3],&g_106[3],&g_106[3],&g_106[3],&g_106[3],&g_106[3],&g_106[3],&g_106[3]};
            int32_t l_433 = 0xCA79F1E1L;
            int32_t *l_435 = (void*)0;
            int32_t **l_434 = &l_435;
            uint8_t l_438[9] = {0UL,1UL,0UL,0UL,1UL,0UL,0UL,1UL,0UL};
            int i;
            l_433 = ((&g_400 == l_417[4]) > (g_106[0] = (safe_mul_func_float_f_f((**g_230), (l_420 > (l_420 <= (safe_sub_func_float_f_f(((l_432 = (safe_sub_func_float_f_f((0x0.Ep+1 <= (&g_412 != &g_412)), (safe_div_func_float_f_f((safe_mul_func_float_f_f(p_52, (**g_230))), p_52))))) >= l_420), l_433))))))));
            for (g_397 = 0; (g_397 <= 1); g_397 += 1)
            { /* block id: 181 */
                int64_t l_439 = (-9L);
                for (g_312 = 0; (g_312 <= 8); g_312 += 1)
                { /* block id: 184 */
                    int32_t ***l_436 = (void*)0;
                    int32_t ***l_437 = &l_434;
                    (*l_437) = l_434;
                    if (l_420)
                        break;
                    return l_438[2];
                }
                (*g_229) = (*g_229);
                if (l_439)
                    continue;
            }
        }
        if (l_432)
            break;
        ++g_452;
    }
    (*l_458) = (-((*l_457) = (l_449 > (l_456 == (void*)0))));
    --g_471;
    return g_474[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_75 g_72 g_17 g_107 g_106 g_104 g_129 g_128 g_186 g_87 g_194 g_198 g_197 g_218 g_229 g_242 g_230 g_231 g_225 g_299 g_305 g_312 g_298 g_297 g_359 g_380 g_383 g_397 g_238 g_296
 * writes: g_87 g_75 g_104 g_107 g_128 g_106 g_129 g_57 g_72 g_192 g_198 g_197 g_218 g_224 g_242 g_299 g_305 g_312 g_298 g_360 g_304 g_380 g_383 g_400 g_297 g_186
 */
static int16_t  func_60(uint64_t  p_61, uint16_t  p_62, uint64_t * p_63, const float  p_64)
{ /* block id: 7 */
    uint64_t l_82 = 0UL;
    uint32_t *l_86 = &g_87;
    float l_95 = 0x9.0FA27Cp+60;
    uint16_t *l_96[8] = {&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,&g_75};
    int64_t *l_105 = &g_72;
    uint32_t l_108 = 4294967295UL;
    int8_t l_130 = 9L;
    int32_t l_133 = 0L;
    float **l_158[2];
    int32_t *l_179[8][6] = {{(void*)0,(void*)0,&l_133,&l_133,(void*)0,(void*)0},{(void*)0,&l_133,&l_133,(void*)0,(void*)0,&l_133},{(void*)0,(void*)0,&l_133,&l_133,(void*)0,(void*)0},{(void*)0,&l_133,&l_133,(void*)0,(void*)0,&l_133},{(void*)0,(void*)0,&l_133,&l_133,(void*)0,(void*)0},{(void*)0,&l_133,&l_133,(void*)0,(void*)0,&l_133},{(void*)0,(void*)0,&l_133,&l_133,(void*)0,(void*)0},{(void*)0,&l_133,&l_133,(void*)0,(void*)0,&l_133}};
    int32_t **l_205 = &l_179[2][5];
    int32_t l_309 = 4L;
    int32_t *l_349 = &g_297;
    uint64_t l_375 = 0xFF54A15C8C227951LL;
    const uint16_t l_409[6][3][6] = {{{0x1D88L,0xEE9AL,0xB54CL,65535UL,65534UL,0x1D88L},{65535UL,0x41E8L,1UL,0x41E8L,65535UL,0UL},{1UL,65535UL,0UL,0xECCCL,0x0AC5L,65530UL}},{{8UL,0x6C5FL,0xECCCL,65535UL,0x1D88L,65530UL},{0x766AL,65535UL,0UL,0xB54CL,0xB540L,0UL},{0x1D88L,0x27AFL,1UL,1UL,0x27AFL,0x1D88L}},{{0UL,0xB540L,0xB54CL,0UL,65535UL,0x766AL},{65530UL,0x1D88L,65535UL,0xECCCL,0x6C5FL,8UL},{65530UL,0x0AC5L,0xECCCL,0UL,65535UL,1UL}},{{0UL,65535UL,0x41E8L,1UL,0x41E8L,65535UL},{0x1D88L,65534UL,65535UL,0xB54CL,0xEE9AL,0x1D88L},{0x766AL,0UL,0xAA22L,65535UL,65535UL,65534UL}},{{8UL,0UL,0xB540L,0xECCCL,0xEE9AL,0x4496L},{1UL,65534UL,0xECCCL,0x41E8L,0x41E8L,0xECCCL},{65535UL,65535UL,0x1D88L,65535UL,65535UL,0xB1C5L}},{{0x1D88L,0x0AC5L,0xDDCAL,0xAA22L,0x6C5FL,0x1D88L},{65534UL,0x1D88L,0xDDCAL,0xB540L,65535UL,0xB1C5L},{0x4496L,0xB540L,0x1D88L,0xECCCL,0x27AFL,0xECCCL}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_158[i] = (void*)0;
    if ((((((safe_mod_func_int32_t_s_s((((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u(((l_82 ^ (safe_rshift_func_int16_t_s_u(0x950CL, 1))) < (!(((g_107 |= ((((*l_86) = p_62) >= (g_57[1][4] , (+((safe_div_func_int16_t_s_s(((((safe_sub_func_uint16_t_u_u((g_75 = ((safe_mul_func_int16_t_s_s(p_62, l_82)) , g_75)), (~((safe_rshift_func_uint16_t_u_s((safe_mod_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(((g_104 = (1L ^ (p_61 >= l_82))) , g_57[7][0]), l_82)), g_72)), 6)) , 1L)))) | g_17) , &g_72) != l_105), l_82)) && 0UL)))) , g_75)) && p_61) != l_82))), g_17)), p_61)) == 0x9381DCF0L) | p_61), 4294967295UL)) | l_108) < p_62) || g_107) > 0x37234FC06BE8E649LL))
    { /* block id: 12 */
        uint8_t l_122 = 7UL;
        int32_t *l_127[9][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
        float *l_131 = (void*)0;
        float *l_132 = &g_106[4];
        int i, j;
        (*l_132) = ((safe_mul_func_float_f_f((safe_div_func_float_f_f(g_107, (safe_add_func_float_f_f((safe_add_func_float_f_f(g_17, 0x1.Bp+1)), g_106[4])))), ((((~l_108) , (safe_sub_func_float_f_f((((g_104 | (safe_mul_func_int16_t_s_s((((*l_86) = l_122) || (safe_add_func_int16_t_s_s(0xFF24L, ((((safe_div_func_int32_t_s_s((g_128 = (0xCB2DEB47L < 4294967295UL)), g_104)) , 4294967295UL) ^ 0xA3785E6FL) || g_129)))), 0UL))) , g_72) != g_57[9][0]), l_130))) > p_64) != p_62))) >= g_129);
        for (g_107 = 3; (g_107 <= 8); g_107 += 1)
        { /* block id: 18 */
            (*l_132) = 0x1.2p+1;
            for (p_61 = 0; (p_61 <= 5); p_61 += 1)
            { /* block id: 22 */
                int i, j;
                return g_57[p_61][g_107];
            }
        }
        l_133 = (-2L);
    }
    else
    { /* block id: 27 */
        uint32_t l_147 = 0xD986BD2DL;
        float *l_175 = &g_106[0];
        int32_t *l_178 = &l_133;
        int32_t l_196[7][1][8] = {{{0xCD855B04L,0x2BCF4DB2L,0x2BCF4DB2L,0xCD855B04L,1L,0xB315883FL,0xA4908DBAL,(-1L)}},{{1L,0x90F2ED5BL,0L,1L,0x4FA8936DL,1L,(-1L),0xC16A4623L}},{{0xCD855B04L,0xC16A4623L,1L,(-1L),0x4FA8936DL,0x90F2ED5BL,0xB315883FL,0xB315883FL}},{{1L,0xCD855B04L,0x2BCF4DB2L,0x2BCF4DB2L,0xCD855B04L,1L,0xB315883FL,0xA4908DBAL}},{{0L,0x2BCF4DB2L,1L,0x90F2ED5BL,(-1L),1L,(-1L),0x90F2ED5BL}},{{(-1L),1L,(-1L),0x90F2ED5BL,1L,0x2BCF4DB2L,0L,0xA4908DBAL}},{{0xB315883FL,1L,0xCD855B04L,0x2BCF4DB2L,0x2BCF4DB2L,0xCD855B04L,1L,0xB315883FL}}};
        int64_t * const l_219 = &g_72;
        int16_t l_233 = 1L;
        float ** const *l_291 = (void*)0;
        uint16_t ** const l_329 = &l_96[3];
        uint16_t ** const *l_328 = &l_329;
        int32_t l_402 = (-9L);
        uint16_t l_410[9][5][2] = {{{0x8B28L,0x8B28L},{0x3A21L,0x6307L},{65533UL,0x6307L},{0x3A21L,0x8B28L},{0x8B28L,0x3A21L}},{{0x6307L,65533UL},{0x6307L,0x3A21L},{0x8B28L,0x8B28L},{0x3A21L,0x6307L},{65533UL,0x6307L}},{{0x3A21L,0x8B28L},{0x8B28L,0x3A21L},{0x6307L,65533UL},{0x6307L,0x3A21L},{0x8B28L,0x8B28L}},{{0x3A21L,0x6307L},{65533UL,0x6307L},{0x3A21L,0x8B28L},{0x8B28L,0x3A21L},{0x6307L,65533UL}},{{0x6307L,0x3A21L},{0x8B28L,0x8B28L},{0x3A21L,0x6307L},{65533UL,0x6307L},{0x3A21L,0x8B28L}},{{0x8B28L,0x3A21L},{0x6307L,65533UL},{0x6307L,0x3A21L},{0x8B28L,0x8B28L},{0x3A21L,0x6307L}},{{65533UL,0x6307L},{0x3A21L,0x8B28L},{0x8B28L,0x3A21L},{0x6307L,65533UL},{0x6307L,0x3A21L}},{{0x8B28L,0x8B28L},{0x3A21L,0x6307L},{65533UL,0x6307L},{0x3A21L,0x8B28L},{0x8B28L,0x3A21L}},{{0x6307L,65533UL},{0x6307L,0x3A21L},{0x8B28L,0x8B28L},{0x3A21L,0x6307L},{65533UL,0x6307L}}};
        int i, j, k;
        if (p_62)
        { /* block id: 28 */
lbl_220:
            for (g_107 = (-14); (g_107 >= 50); g_107 = safe_add_func_uint8_t_u_u(g_107, 8))
            { /* block id: 31 */
                int32_t *l_136 = &l_133;
                int32_t *l_137 = (void*)0;
                int32_t l_138 = 0xDBF9D0A1L;
                l_138 &= ((*l_136) = p_61);
            }
            g_104 ^= p_62;
        }
        else
        { /* block id: 36 */
            uint8_t l_144 = 0UL;
            int32_t l_157 = 0L;
            float *l_161 = &l_95;
            float **l_160 = &l_161;
            int8_t *l_222 = &g_197;
            int8_t **l_221 = &l_222;
            int32_t l_236 = 0x719FA182L;
            int32_t l_239 = 4L;
            int32_t l_241 = 6L;
            int32_t **l_245 = &l_178;
            uint16_t l_270 = 65535UL;
            for (g_129 = 0; (g_129 <= 8); g_129 += 1)
            { /* block id: 39 */
                int32_t l_156 = 0x5DA6A529L;
                const int32_t *l_191 = &l_157;
                const int32_t **l_190 = &l_191;
                int32_t l_234 = (-7L);
                int32_t l_235 = 6L;
                int32_t l_237 = 0x491D3E57L;
                const uint16_t *l_248[6][1] = {{&g_249},{&g_249},{&g_249},{&g_249},{&g_249},{&g_249}};
                const uint16_t **l_247 = &l_248[3][0];
                int i, j;
                l_157 = (g_129 <= (0x1.F8F6A0p-65 <= ((-(safe_add_func_float_f_f((-0x3.8p-1), (safe_add_func_float_f_f(l_144, (safe_sub_func_float_f_f(l_147, ((0x3.8p+1 > (safe_add_func_float_f_f(((safe_add_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f((p_62 , (l_156 <= (g_106[3] = (p_61 != l_147)))), 0x6.1C656Cp+89)), p_64)), p_62)) != l_144), p_62))) != l_133)))))))) == g_128)));
                if (g_128)
                { /* block id: 42 */
                    float ***l_159[6][4] = {{(void*)0,&l_158[1],(void*)0,&l_158[1]},{(void*)0,&l_158[1],&l_158[1],(void*)0},{(void*)0,&l_158[1],(void*)0,&l_158[1]},{&l_158[1],&l_158[1],(void*)0,(void*)0},{(void*)0,(void*)0,&l_158[1],(void*)0},{(void*)0,&l_158[1],(void*)0,&l_158[1]}};
                    int32_t l_170 = 6L;
                    int32_t **l_187 = &l_178;
                    int i, j;
                    l_160 = l_158[1];
                    for (l_130 = 0; (l_130 <= 8); l_130 += 1)
                    { /* block id: 46 */
                        int32_t *l_177[1][6][3] = {{{&g_104,&l_170,&l_156},{&l_133,(void*)0,&l_157},{&g_104,&g_104,&l_157},{(void*)0,&l_133,&l_156},{&l_170,&g_104,&l_170},{&l_170,(void*)0,&g_104}}};
                        int32_t **l_176[2][2][6] = {{{&l_177[0][4][0],&l_177[0][1][0],&l_177[0][1][0],&l_177[0][4][0],&l_177[0][3][1],(void*)0},{(void*)0,&l_177[0][4][0],&l_177[0][3][1],&l_177[0][4][0],&l_177[0][3][1],&l_177[0][3][1]}},{{&l_177[0][1][0],&l_177[0][3][1],&l_177[0][3][1],&l_177[0][3][1],&l_177[0][3][1],&l_177[0][1][0]},{(void*)0,&l_177[0][1][0],(void*)0,&l_177[0][3][1],(void*)0,&l_177[0][1][0]}}};
                        uint8_t *l_195 = &g_128;
                        int32_t ***l_203 = (void*)0;
                        int32_t ***l_204 = &l_176[0][1][4];
                        int8_t *l_216 = &g_197;
                        int8_t *l_217 = &g_218;
                        int i, j, k;
                        (*l_175) = (safe_mul_func_float_f_f(g_128, ((g_57[(l_130 + 1)][l_130] = g_72) , ((safe_add_func_float_f_f((safe_add_func_float_f_f(((safe_mul_func_float_f_f(0xE.FA8D4Cp-89, (l_170 > g_17))) >= (safe_sub_func_float_f_f((safe_add_func_float_f_f((&g_104 == &g_104), ((*l_161) = p_62))), ((l_179[2][5] = (l_178 = (((((l_175 != l_175) , g_129) & (-1L)) <= 0x92F91EA8L) , &g_104))) == &l_170)))), p_64)), 0xE.95C8E4p-47)) <= g_57[7][0]))));
                        l_196[2][0][0] = ((((((((p_61 > 65532UL) , (l_178 = ((safe_sub_func_int8_t_s_s((g_57[8][4] & ((safe_lshift_func_int8_t_s_u((((*l_195) = ((l_187 = g_186) == (g_192[0][2] = ((p_62 && (g_57[7][6] == ((*l_105) = (7L < (((safe_mul_func_int8_t_s_s(((-1L) & (0x8ABA538469119564LL == 0xEF2F43CA9903D9C6LL)), g_87)) | g_72) && g_57[7][0]))))) , l_190)))) >= g_17), (*l_191))) <= g_104)), (**l_190))) , &g_104))) == (*l_190)) | g_194) , (**l_190)) , (*p_63)) == g_75) , p_62);
                        ++g_198;
                        (*l_178) = (p_62 || (((safe_div_func_int8_t_s_s(((*l_217) &= ((((g_128 , p_63) == &g_72) , ((((*l_216) = (((l_205 = ((*l_204) = g_186)) == (void*)0) <= ((((*p_63)--) == (safe_add_func_int8_t_s_s(g_197, (safe_sub_func_uint64_t_u_u((((safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_s(0xECC7L, (g_197 , g_197))), 0)) < l_157) || g_107), p_61))))) ^ p_61))) | g_75) ^ 0UL)) <= g_104)), p_61)) , l_219) == &g_72));
                    }
                    if (l_133)
                        goto lbl_220;
                    if (p_61)
                        break;
                }
                else
                { /* block id: 68 */
                    int16_t l_232 = (-10L);
                    int32_t l_240 = (-9L);
                    int8_t ***l_246 = &l_221;
                    for (l_156 = 8; (l_156 >= 0); l_156 -= 1)
                    { /* block id: 71 */
                        int8_t ***l_223[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_223[i] = (void*)0;
                        g_224[0][0][0] = l_221;
                        (*l_178) = (safe_unary_minus_func_int64_t_s((safe_sub_func_uint8_t_u_u(((void*)0 == g_229), 7L))));
                        if (l_82)
                            goto lbl_220;
                        if (p_62)
                            continue;
                    }
                    --g_242;
                    (**l_245) = ((l_245 != (void*)0) > (l_246 == &l_221));
                }
                (*l_161) = ((0x0.5B609Bp+62 != (((g_106[3] = (((((*l_247) = &g_75) == (void*)0) , (safe_lshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_u(p_61, ((**l_245) , (safe_div_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_s(255UL, (((safe_div_func_int32_t_s_s((safe_add_func_uint16_t_u_u((((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_int8_t_s_s(((safe_rshift_func_int16_t_s_s(p_61, ((0x60L & (0x73AEC1CFL & g_72)) == (**l_245)))) , 0x6AL), l_270)) != 2UL), 6)), p_61)) >= g_75) | 0xCAC7ECDF4084801CLL), 0xA6F3L)), (*l_178))) == p_61) > p_61))) && (-4L)), 0x8AL))))), 11))) , (**g_230))) == 0x8.2698C1p-71) <= 0xB.F97C4Fp+83)) != p_61);
                for (l_157 = 1; (l_157 <= 8); l_157 += 1)
                { /* block id: 85 */
                    int32_t l_292 = 0x0211C2E6L;
                    int32_t *l_308 = (void*)0;
                    int8_t **l_334 = &g_225;
                    uint8_t l_340[6];
                    int i, j;
                    for (i = 0; i < 6; i++)
                        l_340[i] = 0UL;
                    if (p_61)
                        break;
                    if ((safe_mul_func_int16_t_s_s((g_198 ^ (g_57[g_129][l_157] = (safe_mod_func_int64_t_s_s((safe_add_func_int16_t_s_s((((*g_225) = (18446744073709551613UL == (p_62 | (((*l_86) = g_128) , (safe_lshift_func_uint8_t_u_u(p_62, 0)))))) & (safe_add_func_int64_t_s_s(((void*)0 == (*g_230)), 0xC4F514575F8629F8LL))), ((safe_div_func_int32_t_s_s(((safe_add_func_uint8_t_u_u(((safe_mod_func_uint32_t_u_u((safe_sub_func_int32_t_s_s((safe_add_func_uint16_t_u_u((l_235 = (&g_230 == l_291)), 0x8BEBL)), 0L)), (*l_178))) , g_57[0][0]), 0xCEL)) , 1L), p_61)) > (**l_245)))), p_61)))), l_292)))
                    { /* block id: 91 */
                        (*l_245) = (*l_245);
                    }
                    else
                    { /* block id: 93 */
                        const int32_t *l_293 = &l_234;
                        l_293 = (*l_190);
                        (**l_245) = (safe_mul_func_int64_t_s_s((*l_191), 0L));
                        if (p_61)
                            break;
                        ++g_299;
                    }
                    if ((safe_rshift_func_int8_t_s_s((**l_245), 0)))
                    { /* block id: 99 */
                        g_305++;
                        l_308 = (*l_245);
                    }
                    else
                    { /* block id: 102 */
                        if (p_61)
                            break;
                    }
                    if (p_62)
                    { /* block id: 105 */
                        uint32_t *l_310 = (void*)0;
                        uint32_t *l_311 = &g_312;
                        int32_t l_322 = 0x910637C8L;
                        int32_t l_327 = (-1L);
                        l_309 = p_62;
                        l_327 &= ((++(*l_311)) , (((safe_sub_func_uint8_t_u_u(p_62, g_129)) <= (safe_sub_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((!(((g_17 && (((((l_322 ^ ((((&l_292 == &l_322) >= 0x2FFF6BB7E7533F88LL) & ((safe_div_func_float_f_f((((***g_229) != ((((safe_sub_func_uint32_t_u_u(g_312, (-6L))) >= 0x58L) , (-0x2.Ep+1)) != 0x9.17E6EEp+98)) != p_62), (*l_191))) , 0x4AL)) && g_298)) | 0x62L) <= 0x6B7EL) & 0xFA74C0A3D9820A3ELL) , (**l_190))) <= 0xB7AF958FL) && g_297)) == p_62), g_297)), p_62))) <= 0x1AL));
                        if (l_292)
                            continue;
                    }
                    else
                    { /* block id: 110 */
                        uint16_t ** const **l_330 = &l_328;
                        (*l_330) = l_328;
                        l_196[2][0][0] |= ((*l_178) &= (((safe_mod_func_uint8_t_u_u(((g_298 == ((&g_225 != ((~p_62) , l_334)) | (~(safe_sub_func_int64_t_s_s(((*l_191) ^ (safe_rshift_func_int8_t_s_u((*g_225), 1))), (0xCEAEE16CL ^ ((((((0x80391040L < (l_340[0] , g_87)) , (*g_229)) == (void*)0) | (*g_225)) ^ g_72) , 0x8AC25AD5L))))))) >= g_305), 0xA1L)) , 0x1BCE832EL) > 0x7E3807DBL));
                        (*l_190) = (*l_190);
                    }
                }
            }
            (*l_245) = &l_196[2][0][0];
            (*l_245) = &l_196[1][0][2];
        }
        for (l_130 = 0; (l_130 <= 5); l_130 = safe_add_func_uint32_t_u_u(l_130, 1))
        { /* block id: 123 */
            const uint16_t l_354 = 65528UL;
            int32_t *l_372 = &g_297;
            int32_t l_377 = (-5L);
            uint16_t l_401 = 0x301DL;
            if (g_197)
                goto lbl_220;
            for (l_82 = (-21); (l_82 == 60); l_82 = safe_add_func_uint16_t_u_u(l_82, 2))
            { /* block id: 127 */
                uint16_t l_347[9];
                int32_t **l_352 = (void*)0;
                int i;
                for (i = 0; i < 9; i++)
                    l_347[i] = 0x3974L;
                for (g_298 = (-17); (g_298 == (-8)); g_298++)
                { /* block id: 130 */
                    int32_t l_348 = 0x828E4155L;
                    int32_t l_357 = 0L;
                    int32_t l_379 = 0xBBB62DD9L;
                    int64_t **l_398 = (void*)0;
                    int64_t **l_399[8][1] = {{&l_105},{&l_105},{&l_105},{&l_105},{&l_105},{&l_105},{&l_105},{&l_105}};
                    int32_t ***l_403 = &l_205;
                    int32_t ***l_404 = (void*)0;
                    int32_t ***l_405 = (void*)0;
                    int32_t ***l_406 = &g_186;
                    int i, j;
                    if (l_347[3])
                        break;
                    if (((*g_229) != (void*)0))
                    { /* block id: 132 */
                        int32_t **l_350 = &l_179[7][3];
                        uint32_t l_351 = 3UL;
                        (*l_350) = (l_348 , l_349);
                        (*l_350) = (((7L & (p_61 | ((***l_328) = l_351))) == (((void*)0 != l_352) > (~(l_354 >= ((l_348 = p_62) >= ((g_360 = (((--(*l_86)) & (l_357 || ((~g_359) , 0xD94905E8L))) ^ 9UL)) != p_61)))))) , (void*)0);
                    }
                    else
                    { /* block id: 139 */
                        int32_t **l_373 = (void*)0;
                        int32_t **l_374 = &l_372;
                        int32_t **l_376[9][10][1] = {{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}},{{&l_179[3][0]},{(void*)0},{&l_178},{&l_178},{&l_178},{(void*)0},{&l_179[3][0]},{(void*)0},{&l_179[2][5]},{(void*)0}}};
                        int i, j, k;
                        l_349 = ((safe_add_func_float_f_f((g_304 = (0x0.4p-1 >= ((safe_add_func_float_f_f((0x3.6p+1 == ((safe_div_func_float_f_f(l_354, (((*l_178) = (g_106[3] = (safe_add_func_float_f_f((safe_add_func_float_f_f((-(((*l_374) = l_372) != &l_196[2][0][0])), (***g_229))), (*g_231))))) != ((l_375 > p_62) < l_357)))) == p_64)), p_62)) >= l_354))), 0x0.6p+1)) , (void*)0);
                        if (p_61)
                            continue;
                        g_380++;
                        --g_383;
                    }
                    l_402 |= ((*l_178) = (((safe_mul_func_int16_t_s_s((((*l_372) = (safe_div_func_float_f_f(((safe_div_func_float_f_f(((((***g_229) <= (-p_62)) , p_62) , (l_379 == ((-(safe_div_func_float_f_f((+(((**g_230) < (((g_397 != (l_357 = (-0x10.1p+1))) < ((g_400 = &g_238) != p_63)) <= (-0x3.2p+1))) <= 0x2.1357D8p-40)), 0xA.92665Dp-90))) != p_64))), (**g_230))) < (-0x1.Bp+1)), (*l_178)))) , p_61), g_238)) | g_296) || l_401));
                    (*l_406) = ((*l_403) = g_186);
                }
            }
            for (g_299 = 20; (g_299 > 57); g_299++)
            { /* block id: 160 */
                l_410[1][2][0] = l_409[1][2][4];
            }
        }
    }
    return g_17;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_17, "g_17", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_57[i][j], "g_57[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_72, "g_72", print_hash_value);
    transparent_crc(g_75, "g_75", print_hash_value);
    transparent_crc(g_87, "g_87", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_106[i], sizeof(g_106[i]), "g_106[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_107, "g_107", print_hash_value);
    transparent_crc(g_128, "g_128", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    transparent_crc(g_194, "g_194", print_hash_value);
    transparent_crc(g_197, "g_197", print_hash_value);
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_218, "g_218", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    transparent_crc(g_242, "g_242", print_hash_value);
    transparent_crc(g_249, "g_249", print_hash_value);
    transparent_crc(g_296, "g_296", print_hash_value);
    transparent_crc(g_297, "g_297", print_hash_value);
    transparent_crc(g_298, "g_298", print_hash_value);
    transparent_crc(g_299, "g_299", print_hash_value);
    transparent_crc_bytes (&g_304, sizeof(g_304), "g_304", print_hash_value);
    transparent_crc(g_305, "g_305", print_hash_value);
    transparent_crc(g_312, "g_312", print_hash_value);
    transparent_crc(g_359, "g_359", print_hash_value);
    transparent_crc(g_360, "g_360", print_hash_value);
    transparent_crc(g_378, "g_378", print_hash_value);
    transparent_crc(g_380, "g_380", print_hash_value);
    transparent_crc(g_383, "g_383", print_hash_value);
    transparent_crc(g_397, "g_397", print_hash_value);
    transparent_crc(g_412, "g_412", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_450[i], "g_450[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_452, "g_452", print_hash_value);
    transparent_crc(g_469, "g_469", print_hash_value);
    transparent_crc(g_470, "g_470", print_hash_value);
    transparent_crc(g_471, "g_471", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_474[i], "g_474[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_767, "g_767", print_hash_value);
    transparent_crc(g_768, "g_768", print_hash_value);
    transparent_crc(g_769, "g_769", print_hash_value);
    transparent_crc(g_791, "g_791", print_hash_value);
    transparent_crc(g_826, "g_826", print_hash_value);
    transparent_crc(g_827, "g_827", print_hash_value);
    transparent_crc(g_829, "g_829", print_hash_value);
    transparent_crc(g_830, "g_830", print_hash_value);
    transparent_crc(g_912, "g_912", print_hash_value);
    transparent_crc(g_921, "g_921", print_hash_value);
    transparent_crc(g_923, "g_923", print_hash_value);
    transparent_crc(g_929, "g_929", print_hash_value);
    transparent_crc(g_998, "g_998", print_hash_value);
    transparent_crc(g_1021, "g_1021", print_hash_value);
    transparent_crc(g_1022, "g_1022", print_hash_value);
    transparent_crc(g_1023, "g_1023", print_hash_value);
    transparent_crc(g_1024, "g_1024", print_hash_value);
    transparent_crc(g_1062, "g_1062", print_hash_value);
    transparent_crc(g_1074, "g_1074", print_hash_value);
    transparent_crc(g_1075, "g_1075", print_hash_value);
    transparent_crc_bytes (&g_1080, sizeof(g_1080), "g_1080", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1081[i], "g_1081[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1082, "g_1082", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1114[i], "g_1114[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1219, "g_1219", print_hash_value);
    transparent_crc(g_1377, "g_1377", print_hash_value);
    transparent_crc(g_1399, "g_1399", print_hash_value);
    transparent_crc(g_1460, "g_1460", print_hash_value);
    transparent_crc_bytes (&g_1472, sizeof(g_1472), "g_1472", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1475[i], "g_1475[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1536, "g_1536", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_1540[i], sizeof(g_1540[i]), "g_1540[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1542, "g_1542", print_hash_value);
    transparent_crc(g_1543, "g_1543", print_hash_value);
    transparent_crc(g_1615, "g_1615", print_hash_value);
    transparent_crc(g_1642, "g_1642", print_hash_value);
    transparent_crc(g_1718, "g_1718", print_hash_value);
    transparent_crc(g_1770, "g_1770", print_hash_value);
    transparent_crc(g_1830, "g_1830", print_hash_value);
    transparent_crc(g_1871, "g_1871", print_hash_value);
    transparent_crc(g_1900, "g_1900", print_hash_value);
    transparent_crc(g_1933, "g_1933", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc_bytes(&g_1945[i][j][k], sizeof(g_1945[i][j][k]), "g_1945[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1953, "g_1953", print_hash_value);
    transparent_crc(g_1956, "g_1956", print_hash_value);
    transparent_crc(g_1972, "g_1972", print_hash_value);
    transparent_crc(g_2037, "g_2037", print_hash_value);
    transparent_crc(g_2038, "g_2038", print_hash_value);
    transparent_crc(g_2039, "g_2039", print_hash_value);
    transparent_crc(g_2040, "g_2040", print_hash_value);
    transparent_crc(g_2042, "g_2042", print_hash_value);
    transparent_crc(g_2043, "g_2043", print_hash_value);
    transparent_crc_bytes (&g_2194, sizeof(g_2194), "g_2194", print_hash_value);
    transparent_crc(g_2196, "g_2196", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2201[i], "g_2201[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2286, "g_2286", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2352[i], "g_2352[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2355, "g_2355", print_hash_value);
    transparent_crc_bytes (&g_2356, sizeof(g_2356), "g_2356", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc_bytes(&g_2357[i], sizeof(g_2357[i]), "g_2357[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2361, "g_2361", print_hash_value);
    transparent_crc(g_2424, "g_2424", print_hash_value);
    transparent_crc(g_2457, "g_2457", print_hash_value);
    transparent_crc(g_2458, "g_2458", print_hash_value);
    transparent_crc(g_2630, "g_2630", print_hash_value);
    transparent_crc(g_2708, "g_2708", print_hash_value);
    transparent_crc(g_2767, "g_2767", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_2787[i][j], "g_2787[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3025, "g_3025", print_hash_value);
    transparent_crc(g_3055, "g_3055", print_hash_value);
    transparent_crc(g_3083, "g_3083", print_hash_value);
    transparent_crc(g_3198, "g_3198", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 846
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 44
breakdown:
   depth: 1, occurrence: 209
   depth: 2, occurrence: 52
   depth: 3, occurrence: 3
   depth: 4, occurrence: 3
   depth: 5, occurrence: 3
   depth: 9, occurrence: 1
   depth: 11, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 3
   depth: 22, occurrence: 4
   depth: 23, occurrence: 3
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 3
   depth: 31, occurrence: 1
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1
   depth: 44, occurrence: 1

XXX total number of pointers: 667

XXX times a variable address is taken: 1360
XXX times a pointer is dereferenced on RHS: 419
breakdown:
   depth: 1, occurrence: 252
   depth: 2, occurrence: 127
   depth: 3, occurrence: 26
   depth: 4, occurrence: 14
XXX times a pointer is dereferenced on LHS: 388
breakdown:
   depth: 1, occurrence: 337
   depth: 2, occurrence: 31
   depth: 3, occurrence: 13
   depth: 4, occurrence: 7
XXX times a pointer is compared with null: 70
XXX times a pointer is compared with address of another variable: 20
XXX times a pointer is compared with another pointer: 23
XXX times a pointer is qualified to be dereferenced: 8985

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1306
   level: 2, occurrence: 429
   level: 3, occurrence: 339
   level: 4, occurrence: 152
   level: 5, occurrence: 140
XXX number of pointers point to pointers: 359
XXX number of pointers point to scalars: 308
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.1
XXX average alias set size: 1.42

XXX times a non-volatile is read: 2843
XXX times a non-volatile is write: 1330
XXX times a volatile is read: 17
XXX    times read thru a pointer: 16
XXX times a volatile is write: 3
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 51
XXX percentage of non-volatile access: 99.5

XXX forward jumps: 0
XXX backward jumps: 9

XXX stmts: 205
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 41
   depth: 2, occurrence: 32
   depth: 3, occurrence: 21
   depth: 4, occurrence: 34
   depth: 5, occurrence: 48

XXX percentage a fresh-made variable is used: 17.8
XXX percentage an existing variable is used: 82.2
********************* end of statistics **********************/


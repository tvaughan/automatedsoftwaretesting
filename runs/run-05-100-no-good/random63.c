/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1607911584
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   uint32_t  f0;
   const uint64_t  f1;
   volatile uint32_t  f2;
};
#pragma pack(pop)

union U1 {
   int32_t  f0;
   int8_t  f1;
   int8_t * f2;
   int8_t * const  volatile  f3;
   const unsigned f4 : 19;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_4[10] = {0x80444555L,0x80444555L,0x80444555L,0x80444555L,0x80444555L,0x80444555L,0x80444555L,0x80444555L,0x80444555L,0x80444555L};
static float g_11 = 0x9.E071A0p+79;
static float g_13[8][2] = {{0x1.0p-1,0x1.0p-1},{0x7.904128p+79,0x1.0p-1},{0x1.0p-1,0x7.904128p+79},{0x1.0p-1,0x1.0p-1},{0x7.904128p+79,0x1.0p-1},{0x1.0p-1,0x7.904128p+79},{0x1.0p-1,0x1.0p-1},{0x7.904128p+79,0x1.0p-1}};
static volatile int32_t g_14 = 0L;/* VOLATILE GLOBAL g_14 */
static volatile int32_t g_15 = 0x4B11F381L;/* VOLATILE GLOBAL g_15 */
static int32_t g_16 = 1L;
static int8_t g_20 = 0x6AL;
static int8_t * volatile g_19 = &g_20;/* VOLATILE GLOBAL g_19 */
static int16_t g_21 = (-1L);
static uint16_t g_37 = 0xDC06L;
static uint16_t g_84 = 0xE320L;
static int64_t g_93[2] = {0x8641A018BFD1E455LL,0x8641A018BFD1E455LL};
static int32_t g_113 = 0x30FE8BACL;
static uint32_t g_143 = 4294967294UL;
static uint8_t g_148[7][9] = {{0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L},{1UL,0x04L,1UL,0x04L,1UL,0x04L,1UL,0x04L,1UL},{0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L},{1UL,0x04L,1UL,0x04L,1UL,0x04L,1UL,0x04L,1UL},{0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L},{1UL,0x04L,1UL,0x04L,1UL,0x04L,1UL,0x04L,1UL},{0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L,0x42L}};
static uint32_t g_149 = 8UL;
static int16_t g_165 = 0x0042L;
static uint64_t g_190[10][3] = {{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL},{0x8D3978347ABF410FLL,0x8D3978347ABF410FLL,1UL}};
static int64_t *g_196 = &g_93[0];
static int64_t ** volatile g_195 = &g_196;/* VOLATILE GLOBAL g_195 */
static int64_t **g_197 = (void*)0;
static int64_t ** volatile *g_194[2][10][7] = {{{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,(void*)0,&g_197,&g_197,&g_197,(void*)0,&g_197},{(void*)0,&g_197,&g_197,&g_197,&g_197,&g_197,(void*)0},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{(void*)0,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_195,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,(void*)0,&g_197,&g_197,&g_197,(void*)0,&g_197},{&g_197,&g_197,&g_197,(void*)0,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,(void*)0},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_195,&g_197,&g_197,&g_195,&g_197,&g_197,&g_197},{(void*)0,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,(void*)0,&g_197,&g_197,&g_197,(void*)0,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_195,&g_197,(void*)0,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,(void*)0,&g_197}}};
static int32_t g_227 = 1L;
static uint32_t g_228[7] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static uint16_t g_276[5] = {65526UL,65526UL,65526UL,65526UL,65526UL};
static uint16_t g_323 = 0x646FL;
static uint8_t g_324[10] = {0xFEL,0xFEL,0xFEL,0xFEL,0xFEL,0xFEL,0xFEL,0xFEL,0xFEL,0xFEL};
static volatile struct S0 g_326 = {18446744073709551615UL,18446744073709551615UL,0x1D43B4D4L};/* VOLATILE GLOBAL g_326 */
static volatile struct S0 *g_325 = &g_326;
static struct S0 g_361 = {18446744073709551613UL,9UL,0xBF128C11L};/* VOLATILE GLOBAL g_361 */
static int8_t g_370[2][7][6] = {{{0xC9L,1L,0x0FL,1L,2L,0xB5L},{0xC9L,0x74L,1L,0x74L,0xC9L,0xCDL},{0x73L,0xCDL,2L,0xC9L,0xB5L,(-10L)},{(-2L),(-4L),0xB5L,0xCDL,(-10L),(-10L)},{5L,2L,2L,5L,(-1L),0xCDL},{(-10L),0xAFL,1L,0L,0x74L,0xB5L},{1L,(-2L),0x0FL,(-4L),0x74L,(-4L)}},{{1L,0xAFL,1L,2L,(-1L),(-2L)},{0xCDL,2L,0xC9L,0xB5L,(-10L),(-1L)},{0L,(-4L),2L,0xB5L,0xB5L,2L},{0xCDL,0xCDL,0xAFL,2L,0xC9L,1L},{1L,0x74L,0xCDL,(-4L),2L,0xAFL},{1L,1L,0xCDL,0L,0xCDL,1L},{(-10L),0L,0xAFL,5L,0x73L,2L}}};
static uint64_t g_383 = 18446744073709551615UL;
static int32_t *g_391 = &g_4[2];
static uint32_t g_408 = 0x24F352C1L;
static struct S0 g_427 = {0x4D8781CCL,0x2CF14F0DF438B3DCLL,0x2B964FE7L};/* VOLATILE GLOBAL g_427 */
static struct S0 g_428[1][3] = {{{3UL,18446744073709551610UL,18446744073709551614UL},{3UL,18446744073709551610UL,18446744073709551614UL},{3UL,18446744073709551610UL,18446744073709551614UL}}};
static struct S0 g_429 = {18446744073709551615UL,1UL,0x1CB5A17DL};/* VOLATILE GLOBAL g_429 */
static struct S0 g_430 = {0x392CCF0BL,18446744073709551607UL,0x09C42880L};/* VOLATILE GLOBAL g_430 */
static struct S0 g_431 = {0x63662FD1L,5UL,0x7635144DL};/* VOLATILE GLOBAL g_431 */
static struct S0 g_432[1][3] = {{{0x1FE5527AL,18446744073709551615UL,0xBBCC50A4L},{0x1FE5527AL,18446744073709551615UL,0xBBCC50A4L},{0x1FE5527AL,18446744073709551615UL,0xBBCC50A4L}}};
static struct S0 g_433 = {0x86E89833L,18446744073709551615UL,0xA066C90CL};/* VOLATILE GLOBAL g_433 */
static struct S0 *g_426[1][9][8] = {{{&g_430,(void*)0,(void*)0,(void*)0,&g_430,&g_428[0][2],&g_430,(void*)0},{(void*)0,(void*)0,(void*)0,&g_432[0][1],&g_430,&g_432[0][1],(void*)0,(void*)0},{&g_430,&g_432[0][1],(void*)0,(void*)0,(void*)0,&g_432[0][1],&g_430,&g_432[0][1]},{&g_430,(void*)0,(void*)0,(void*)0,&g_430,&g_428[0][2],&g_430,(void*)0},{(void*)0,(void*)0,(void*)0,&g_432[0][1],&g_430,&g_432[0][1],(void*)0,(void*)0},{&g_430,&g_432[0][1],(void*)0,(void*)0,(void*)0,&g_432[0][1],&g_430,&g_432[0][1]},{&g_430,(void*)0,(void*)0,(void*)0,&g_430,&g_428[0][2],(void*)0,&g_432[0][1]},{(void*)0,&g_432[0][1],(void*)0,&g_428[0][2],(void*)0,&g_428[0][2],(void*)0,&g_432[0][1]},{(void*)0,&g_428[0][2],(void*)0,&g_432[0][1],(void*)0,&g_428[0][2],(void*)0,&g_428[0][2]}}};
static struct S0 **g_425 = &g_426[0][1][6];
static struct S0 ***g_424[7][10][3] = {{{&g_425,&g_425,&g_425},{&g_425,(void*)0,(void*)0},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425}},{{(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425},{(void*)0,&g_425,(void*)0},{&g_425,&g_425,&g_425},{&g_425,(void*)0,(void*)0},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425}},{{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425},{(void*)0,&g_425,(void*)0},{&g_425,&g_425,&g_425},{&g_425,(void*)0,(void*)0}},{{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425}},{{&g_425,&g_425,&g_425},{(void*)0,&g_425,(void*)0},{&g_425,&g_425,&g_425},{&g_425,(void*)0,(void*)0},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425}},{{&g_425,(void*)0,&g_425},{&g_425,(void*)0,&g_425},{(void*)0,&g_425,&g_425},{&g_425,&g_425,&g_425},{&g_425,&g_425,&g_425},{(void*)0,&g_425,(void*)0},{&g_425,&g_425,&g_425},{&g_425,(void*)0,(void*)0},{&g_425,&g_425,&g_425},{(void*)0,&g_425,&g_425}},{{&g_425,&g_425,&g_425},{&g_425,(void*)0,&g_425},{&g_425,&g_425,(void*)0},{&g_425,&g_425,&g_425},{&g_425,&g_425,(void*)0},{(void*)0,&g_425,&g_425},{(void*)0,&g_425,&g_425},{(void*)0,(void*)0,&g_425},{&g_425,&g_425,&g_425},{&g_425,(void*)0,(void*)0}}};
static uint32_t g_455 = 18446744073709551615UL;
static const uint32_t g_470[7] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static uint16_t g_499 = 65535UL;
static uint64_t g_515 = 0xFFFF64BE8962453DLL;
static uint8_t g_548 = 0x3EL;
static uint16_t *g_552[9][3] = {{&g_84,(void*)0,(void*)0},{(void*)0,&g_84,&g_84},{&g_84,&g_84,&g_84},{&g_84,(void*)0,&g_84},{&g_84,&g_84,(void*)0},{&g_84,(void*)0,(void*)0},{(void*)0,&g_84,&g_84},{&g_84,&g_84,&g_84},{&g_84,(void*)0,&g_84}};
static uint8_t g_573 = 0x5CL;
static float **g_590 = (void*)0;
static float ***g_589 = &g_590;
static int16_t g_601 = 0xF083L;
static uint8_t *g_657 = &g_148[3][1];
static uint8_t * volatile *g_656 = &g_657;
static union U1 g_681 = {-1L};/* VOLATILE GLOBAL g_681 */
static union U1 g_682 = {0x3ADC26CFL};/* VOLATILE GLOBAL g_682 */
static union U1 g_684[1][4] = {{{0L},{0L},{0L},{0L}}};
static volatile int64_t g_910 = 0x6FBDC0758A74DB0ELL;/* VOLATILE GLOBAL g_910 */
static volatile int64_t * const  volatile g_909 = &g_910;/* VOLATILE GLOBAL g_909 */
static volatile int64_t * const  volatile *g_908 = &g_909;
static volatile int64_t * const  volatile * const *g_907 = &g_908;
static volatile int64_t * const  volatile * const ** volatile g_906 = &g_907;/* VOLATILE GLOBAL g_906 */
static volatile int64_t * const  volatile * const ** volatile *g_905[2][5] = {{&g_906,&g_906,&g_906,&g_906,&g_906},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static volatile int64_t * const  volatile * const ** volatile g_912 = (void*)0;/* VOLATILE GLOBAL g_912 */
static uint32_t g_995 = 0x71AA62EAL;
static int32_t g_1050 = (-1L);
static int64_t ****g_1100 = (void*)0;
static int8_t g_1160 = 0L;
static int8_t *g_1190 = &g_682.f1;
static uint8_t g_1230 = 0UL;
static uint8_t * const g_1229 = &g_1230;
static uint8_t * const * const g_1228[2] = {&g_1229,&g_1229};
static uint8_t * const * const *g_1227 = &g_1228[1];
static uint32_t g_1257[1][2][8] = {{{0xCCED6FE7L,4294967293UL,0xCCED6FE7L,4294967293UL,0xCCED6FE7L,4294967293UL,0xCCED6FE7L,4294967293UL},{0xCCED6FE7L,4294967293UL,0xCCED6FE7L,4294967293UL,0xCCED6FE7L,4294967293UL,0xCCED6FE7L,4294967293UL}}};
static int32_t * volatile *g_1305 = &g_391;
static int32_t * volatile **g_1304[1] = {&g_1305};
static uint8_t * const *g_1379 = &g_1229;
static uint8_t * const **g_1378 = &g_1379;
static const int32_t g_1387 = 0x2CADD708L;
static int32_t g_1497 = 6L;
static struct S0 g_1515 = {0x4BF60929L,0UL,7UL};/* VOLATILE GLOBAL g_1515 */
static float g_1522 = (-0x2.Dp-1);
static int32_t g_1523 = (-4L);
static uint16_t g_1536 = 0xD909L;
static struct S0 g_1599 = {0UL,1UL,1UL};/* VOLATILE GLOBAL g_1599 */
static union U1 g_1609 = {0x68AC0B9EL};/* VOLATILE GLOBAL g_1609 */
static union U1 g_1732 = {-6L};/* VOLATILE GLOBAL g_1732 */
static union U1 g_1735 = {0x1D40D888L};/* VOLATILE GLOBAL g_1735 */
static uint32_t g_1745[7][2] = {{4294967295UL,4UL},{4294967295UL,4294967295UL},{4UL,4294967295UL},{4294967295UL,4UL},{4294967295UL,4294967295UL},{4UL,4294967295UL},{4294967295UL,4UL}};
static int32_t * volatile g_1777 = &g_1732.f0;/* VOLATILE GLOBAL g_1777 */
static int32_t * volatile *g_1776 = &g_1777;
static union U1 * const g_1796 = (void*)0;
static int16_t *g_1818 = (void*)0;
static float g_1828 = 0xC.541567p+37;
static union U1 g_1880 = {0x3964AAF5L};/* VOLATILE GLOBAL g_1880 */
static union U1 g_1882[7][8] = {{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}},{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}},{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}},{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}},{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}},{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}},{{0x5B4C15E4L},{0xECF5EF55L},{-3L},{1L},{1L},{-3L},{0xECF5EF55L},{0x5B4C15E4L}}};
static uint8_t **g_1903 = &g_657;
static uint8_t ***g_1902 = &g_1903;
static uint32_t *g_1932 = &g_1745[4][0];
static uint32_t **g_1931 = &g_1932;
static uint64_t g_2015 = 0x4E19C7819AFCA6F7LL;
static volatile int32_t *** volatile g_2023 = (void*)0;/* VOLATILE GLOBAL g_2023 */
static volatile int32_t *** volatile *g_2022 = &g_2023;
static volatile int32_t *** volatile **g_2021[9] = {&g_2022,&g_2022,&g_2022,&g_2022,&g_2022,&g_2022,&g_2022,&g_2022,&g_2022};
static struct S0 g_2031[3] = {{0UL,1UL,0x1F806EB7L},{0UL,1UL,0x1F806EB7L},{0UL,1UL,0x1F806EB7L}};
static uint32_t *g_2128 = &g_455;
static uint8_t *****g_2182[3] = {(void*)0,(void*)0,(void*)0};
static volatile uint32_t g_2277 = 4294967295UL;/* VOLATILE GLOBAL g_2277 */
static union U1 *g_2288[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t g_2320 = 8UL;
static volatile struct S0 g_2324 = {0x5F1F261FL,18446744073709551610UL,1UL};/* VOLATILE GLOBAL g_2324 */
static const uint32_t * volatile g_2340[2] = {(void*)0,(void*)0};
static const uint32_t * volatile * volatile g_2339 = &g_2340[1];/* VOLATILE GLOBAL g_2339 */
static const uint32_t * volatile * volatile *g_2338[5] = {&g_2339,&g_2339,&g_2339,&g_2339,&g_2339};
static int32_t * volatile g_2346[5][6][4] = {{{&g_16,&g_16,&g_113,&g_113},{(void*)0,(void*)0,&g_113,&g_16},{&g_113,&g_16,&g_16,&g_113},{&g_16,&g_16,&g_16,&g_16},{(void*)0,&g_16,&g_16,&g_113},{&g_16,&g_16,&g_113,&g_16}},{{&g_16,(void*)0,&g_16,&g_113},{(void*)0,&g_16,(void*)0,&g_113},{&g_16,&g_113,&g_16,&g_16},{&g_113,&g_16,&g_113,&g_16},{&g_113,&g_16,&g_113,&g_16},{&g_16,&g_113,&g_16,&g_16}},{{&g_16,&g_16,&g_113,&g_113},{(void*)0,&g_16,&g_113,&g_16},{&g_16,&g_16,&g_16,&g_16},{&g_16,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_16},{&g_113,&g_113,&g_16,&g_113}},{{&g_16,&g_113,(void*)0,&g_16},{(void*)0,&g_16,&g_16,(void*)0},{&g_16,&g_113,&g_113,&g_16},{&g_16,(void*)0,&g_16,&g_113},{(void*)0,(void*)0,&g_16,&g_113},{&g_16,(void*)0,&g_16,&g_16}},{{&g_113,&g_113,&g_113,(void*)0},{(void*)0,&g_16,&g_113,&g_16},{&g_16,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_16,&g_16},{&g_113,&g_113,&g_16,&g_113},{&g_16,&g_113,&g_113,&g_16}}};
static int16_t g_2376 = 0L;
static volatile int8_t g_2384 = 0x26L;/* VOLATILE GLOBAL g_2384 */
static volatile int8_t *g_2383 = &g_2384;
static volatile int8_t ** volatile g_2382 = &g_2383;/* VOLATILE GLOBAL g_2382 */
static volatile int8_t ** volatile *g_2381 = &g_2382;
static int32_t g_2426 = 0L;
static volatile union U1 g_2443 = {0x24FB6C1CL};/* VOLATILE GLOBAL g_2443 */
static union U1 g_2457 = {-7L};/* VOLATILE GLOBAL g_2457 */
static volatile int8_t g_2488[10] = {0x8DL,0x8DL,0x8DL,0x8DL,0x8DL,0x8DL,0x8DL,0x8DL,0x8DL,0x8DL};
static union U1 g_2493 = {-3L};/* VOLATILE GLOBAL g_2493 */
static volatile union U1 g_2528 = {0x3A014E16L};/* VOLATILE GLOBAL g_2528 */
static struct S0 g_2567 = {18446744073709551611UL,0x2EEA0DF2E85950A9LL,7UL};/* VOLATILE GLOBAL g_2567 */
static struct S0 g_2571 = {0x12F52DCAL,1UL,0UL};/* VOLATILE GLOBAL g_2571 */
static const int32_t *g_2584 = (void*)0;
static const int32_t ** volatile g_2583 = &g_2584;/* VOLATILE GLOBAL g_2583 */
static volatile uint16_t g_2599 = 0x60E5L;/* VOLATILE GLOBAL g_2599 */
static uint32_t g_2610[1][4][10] = {{{4294967295UL,4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL},{4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL,0x94B87FFFL},{4294967295UL,4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL},{4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL,0x94B87FFFL,0x94B87FFFL,4294967295UL,4294967295UL,0x94B87FFFL}}};
static union U1 g_2617 = {0xCDAA7ED9L};/* VOLATILE GLOBAL g_2617 */
static struct S0 g_2623 = {0x9025E256L,0x8CEF07CA94EC49F1LL,0xCC042787L};/* VOLATILE GLOBAL g_2623 */
static int16_t g_2646[9][9][3] = {{{0xCED1L,(-9L),7L},{0x03CEL,0xBDE9L,0x50CDL},{1L,(-3L),(-9L)},{0L,0L,0x4001L},{0x0F40L,0xCED1L,0x7C15L},{0x50CDL,0xBDE9L,0x03CEL},{0x3FF2L,1L,(-3L)},{0L,0x709DL,0xC690L},{0x3FF2L,(-1L),1L}},{{0x50CDL,0xC50DL,4L},{0x0F40L,0x9AF2L,0xAB29L},{0x4001L,0x8CB4L,0x821EL},{(-9L),0x7C15L,0x3F5CL},{0xC690L,9L,(-1L)},{0xAB29L,(-3L),0x5FE1L},{0x78CBL,0x4CA2L,(-1L)},{0xCED1L,1L,0x3F5CL},{0xEB9FL,0L,0x821EL}},{{1L,0xAB29L,0xAB29L},{0L,7L,4L},{7L,0x3F5CL,1L},{4L,0xC7B3L,0xC690L},{0x7C15L,0x5FE1L,(-3L)},{0xBF21L,0xC7B3L,0x03CEL},{1L,0x3F5CL,0x7C15L},{0L,7L,0x4001L},{0x9AF2L,0xAB29L,0x9AF2L}},{{0x97C3L,0L,0x78CBL},{(-1L),1L,(-1L)},{0xF5A0L,0x4CA2L,0xA08CL},{1L,(-3L),1L},{0xF5A0L,9L,0L},{(-1L),0x7C15L,0xCED1L},{0x97C3L,0x8CB4L,0xBF21L},{0x9AF2L,0x9AF2L,0L},{0L,0xC50DL,0x7EECL}},{{1L,(-1L),1L},{0xBF21L,0x709DL,1L},{0x7C15L,1L,1L},{4L,0xBDE9L,0x7EECL},{7L,0xCED1L,0L},{0L,0L,0xBF21L},{1L,0L,0xCED1L},{0xEB9FL,0xB7F3L,0L},{0xCED1L,1L,1L}},{{0x78CBL,0x8614L,0xA08CL},{0xAB29L,1L,(-1L)},{0xC690L,0xB7F3L,0x78CBL},{(-9L),0L,0x9AF2L},{0x4001L,0L,0x4001L},{0x0F40L,0xCED1L,0x7C15L},{0x50CDL,0xBDE9L,0x03CEL},{0x3FF2L,1L,(-3L)},{0L,0x709DL,0xC690L}},{{0x3FF2L,(-1L),1L},{0x50CDL,0xC50DL,4L},{0x0F40L,0x9AF2L,0xAB29L},{0x4001L,0x8CB4L,0x821EL},{(-9L),0x7C15L,0x3F5CL},{0xC690L,9L,(-1L)},{0xAB29L,(-3L),0x5FE1L},{0x78CBL,0x4CA2L,(-1L)},{0xCED1L,1L,0x3F5CL}},{{0xEB9FL,0L,0x821EL},{1L,0xAB29L,0xAB29L},{0L,7L,4L},{7L,0x3F5CL,0x3F5CL},{0xBF21L,0L,0x50CDL},{0x0F40L,0x3FF2L,0L},{2L,0L,0xC690L},{(-1L),(-3L),0x0F40L},{0x4001L,0x8614L,0xDA29L}},{{0L,(-1L),0L},{0L,0L,0x97C3L},{0x7C15L,0x3F5CL,1L},{0x044BL,(-2L),0x821EL},{0x3F5CL,0L,(-1L)},{0x044BL,0x4CA2L,0x7EECL},{0x7C15L,0x0F40L,0xAB29L},{0L,(-7L),2L},{0L,0L,0x5FE1L}}};
static uint32_t ***g_2661[4] = {&g_1931,&g_1931,&g_1931,&g_1931};
static uint32_t ****g_2660 = &g_2661[3];
static union U1 ** volatile g_2691 = &g_2288[4];/* VOLATILE GLOBAL g_2691 */
static int32_t * volatile g_2755 = &g_4[9];/* VOLATILE GLOBAL g_2755 */


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static uint16_t  func_2(int8_t  p_3);
static uint32_t  func_22(int8_t * p_23, uint16_t  p_24, uint8_t  p_25, const int64_t  p_26, float * p_27);
static int8_t * func_28(int8_t * p_29, uint16_t  p_30, int32_t  p_31, int8_t * p_32);
static uint16_t  func_38(int64_t  p_39, uint32_t  p_40, uint64_t  p_41);
static int8_t  func_71(uint16_t * p_72, int8_t  p_73, uint32_t  p_74, uint8_t  p_75, float * const  p_76);
static int16_t  func_77(int16_t  p_78, int8_t  p_79, uint16_t * p_80, uint32_t  p_81);
static uint32_t  func_87(uint32_t  p_88, int8_t  p_89);
static uint32_t  func_97(int8_t * p_98, int8_t  p_99, const int64_t  p_100, const int64_t  p_101);
static int8_t * func_102(int32_t * p_103, uint32_t  p_104, uint16_t  p_105, int64_t  p_106);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_19 g_21 g_84 g_20 g_16 g_93 g_143 g_113 g_37 g_149 g_165 g_148 g_194 g_228 g_190 g_276 g_227 g_325 g_324 g_13 g_383 g_361.f1 g_391 g_408 g_370 g_425 g_455 g_431.f1 g_470 g_499 g_432.f1 g_432.f0 g_515 g_548 g_427.f1 g_552 g_573 g_428.f0 g_430.f0 g_1304 g_1190 g_361.f0 g_684.f4 g_433.f0 g_1227 g_1228 g_1229 g_1230 g_657 g_681.f0 g_682.f1 g_1497 g_656 g_427.f0 g_1523 g_1536 g_428.f1 g_601 g_1257 g_1378 g_1379 g_1522 g_1732.f1 g_1745 g_1609.f0 g_1305 g_1776 g_1796 g_1902 g_1818 g_1903 g_2015 g_1932 g_2128 g_909 g_910 g_2277 g_908 g_2182 g_2320 g_2324 g_2338 g_2376 g_1515.f1 g_1882.f0 g_2381 g_907 g_1732.f0 g_2443 g_2457 g_1735.f4 g_2488 g_2493 g_2528 g_995 g_1609.f4 g_2457.f4 g_1828 g_1599.f0 g_326.f1 g_2583 g_589 g_590 g_1735.f1 g_433.f2 g_1777 g_2599 g_2617 g_2623 g_1387 g_2567.f2 g_2457.f1 g_2646 g_2660 g_2691 g_2382 g_2383 g_2384 g_2755
 * writes: g_11 g_13 g_16 g_21 g_37 g_20 g_143 g_148 g_149 g_93 g_165 g_4 g_190 g_113 g_227 g_228 g_276 g_196 g_323 g_324 g_325 g_370 g_383 g_391 g_408 g_424 g_426 g_455 g_499 g_515 g_548 g_601 g_430.f0 g_1257 g_682.f1 g_361.f0 g_433.f0 g_681.f0 g_1230 g_1522 g_1160 g_1536 g_1818 g_1931 g_2015 g_1050 g_2021 g_1828 g_2128 g_84 g_2277 g_2288 g_427.f0 g_1497 g_1745 g_2320 g_2376 g_2324.f2 g_2426 g_1599.f0 g_2584 g_1732.f0 g_2610 g_2646 g_431.f0 g_2660 g_590
 */
static float  func_1(void)
{ /* block id: 0 */
    uint32_t l_2366 = 0x523D4D1EL;
    uint16_t **l_2369 = &g_552[0][0];
    int32_t *l_2374 = (void*)0;
    int32_t *l_2375[2];
    uint8_t l_2377 = 1UL;
    int32_t l_2378 = 0L;
    float *l_2417[10] = {(void*)0,(void*)0,&g_13[2][1],(void*)0,(void*)0,&g_13[2][1],(void*)0,(void*)0,&g_13[2][1],(void*)0};
    const int64_t * const l_2419 = &g_93[0];
    const int64_t * const *l_2418 = &l_2419;
    int16_t l_2456[8][8] = {{0xD00EL,(-1L),0L,0x340DL,0L,(-1L),0xD00EL,(-1L)},{0xD00EL,0x340DL,0x9ED4L,0x340DL,0xD00EL,1L,0xD00EL,0x340DL},{0L,0x340DL,0L,(-1L),0xD00EL,(-1L),0L,0x340DL},{0xD00EL,(-1L),0L,0x340DL,0L,(-1L),0xD00EL,(-1L)},{0xD00EL,0x340DL,0x9ED4L,0x340DL,0xD00EL,1L,0xD00EL,0x340DL},{0L,0x340DL,0L,(-1L),0xD00EL,(-1L),0L,0x340DL},{0xD00EL,(-1L),0L,0x340DL,0L,(-1L),0xD00EL,(-1L)},{0xD00EL,0x340DL,0x9ED4L,0x340DL,0xD00EL,1L,0xD00EL,0x340DL}};
    int16_t l_2496 = 0xEF9DL;
    uint32_t l_2542 = 0UL;
    uint32_t l_2544 = 0UL;
    struct S0 *l_2565[1][1][4] = {{{&g_429,&g_429,&g_429,&g_429}}};
    const int32_t *l_2587 = &g_113;
    uint16_t l_2588 = 0x7445L;
    int8_t l_2609 = 0x0AL;
    uint8_t l_2636 = 0x9BL;
    uint64_t l_2645 = 0xCBF17C0EBA2A5DFBLL;
    const uint32_t *l_2681[7] = {&l_2366,&l_2366,&l_2366,&l_2366,&l_2366,&l_2366,&l_2366};
    const uint32_t ** const l_2680 = &l_2681[4];
    const uint32_t ** const *l_2679[4][3][9] = {{{&l_2680,&l_2680,(void*)0,(void*)0,&l_2680,&l_2680,(void*)0,(void*)0,&l_2680},{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680},{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,(void*)0,&l_2680,&l_2680,&l_2680}},{{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680},{&l_2680,&l_2680,(void*)0,(void*)0,&l_2680,&l_2680,&l_2680,(void*)0,&l_2680},{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680}},{{&l_2680,(void*)0,&l_2680,&l_2680,&l_2680,(void*)0,(void*)0,&l_2680,&l_2680},{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680},{&l_2680,&l_2680,(void*)0,&l_2680,&l_2680,&l_2680,(void*)0,(void*)0,&l_2680}},{{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680},{&l_2680,(void*)0,(void*)0,&l_2680,&l_2680,&l_2680,(void*)0,&l_2680,&l_2680},{&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680,&l_2680}}};
    const uint32_t ** const **l_2678 = &l_2679[3][2][5];
    int32_t l_2701 = 1L;
    union U1 **l_2726 = &g_2288[4];
    uint8_t ****l_2749 = &g_1902;
    uint8_t *****l_2748 = &l_2749;
    uint32_t l_2756 = 3UL;
    uint32_t l_2759 = 0UL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2375[i] = &g_113;
    if (((func_2(g_4[9]) , (safe_mul_func_int8_t_s_s(((*g_1190) = (safe_add_func_int32_t_s_s(l_2366, ((safe_rshift_func_int16_t_s_s((l_2369 == &g_552[0][2]), (safe_mod_func_int64_t_s_s((l_2366 == ((safe_add_func_int16_t_s_s((-9L), (((((g_2376 |= l_2366) < ((((*g_1932) | 0xAE2C2784L) <= g_1515.f1) >= (*g_19))) && l_2377) && 0x65091427L) != (-1L)))) & 1L)), g_431.f1)))) < l_2378)))), (***g_1902)))) & g_1882[6][2].f0))
    { /* block id: 1069 */
        int8_t **l_2380[2][1][5] = {{{(void*)0,&g_1190,&g_1190,&g_1190,&g_1190}},{{(void*)0,&g_1190,&g_1190,&g_1190,&g_1190}}};
        int8_t ***l_2379[3][3][4] = {{{&l_2380[1][0][0],&l_2380[1][0][0],&l_2380[1][0][0],&l_2380[1][0][0]},{(void*)0,&l_2380[1][0][0],&l_2380[0][0][0],&l_2380[1][0][0]},{&l_2380[1][0][0],&l_2380[1][0][2],&l_2380[1][0][2],&l_2380[1][0][0]}},{{&l_2380[1][0][0],&l_2380[1][0][0],&l_2380[1][0][2],&l_2380[1][0][0]},{&l_2380[1][0][0],(void*)0,&l_2380[0][0][0],(void*)0},{(void*)0,&l_2380[1][0][2],&l_2380[1][0][0],(void*)0}},{{&l_2380[1][0][0],(void*)0,&l_2380[1][0][0],&l_2380[1][0][0]},{&l_2380[1][0][0],&l_2380[1][0][0],&l_2380[0][0][0],&l_2380[1][0][0]},{&l_2380[1][0][0],&l_2380[1][0][2],&l_2380[1][0][0],&l_2380[1][0][0]}}};
        int32_t *l_2385[3][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_113,&g_113,&g_113,&g_113,&g_113,&g_113,&g_113,&g_113,&g_113},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        int i, j, k;
        g_13[7][0] = (l_2379[2][2][1] != g_2381);
        for (g_2324.f2 = 0; g_2324.f2 < 5; g_2324.f2 += 1)
        {
            g_276[g_2324.f2] = 65535UL;
        }
        (*g_1305) = l_2385[0][5];
    }
    else
    { /* block id: 1073 */
        uint8_t l_2389 = 255UL;
        int32_t **l_2412 = &l_2374;
        int64_t **l_2420[3][9][4] = {{{&g_196,&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,(void*)0},{&g_196,&g_196,&g_196,(void*)0},{&g_196,(void*)0,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196},{&g_196,(void*)0,&g_196,(void*)0},{(void*)0,&g_196,&g_196,(void*)0},{&g_196,&g_196,(void*)0,&g_196}},{{&g_196,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,&g_196},{(void*)0,&g_196,(void*)0,&g_196},{&g_196,&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196},{&g_196,(void*)0,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196}},{{&g_196,&g_196,&g_196,(void*)0},{&g_196,&g_196,&g_196,(void*)0},{(void*)0,&g_196,(void*)0,&g_196},{&g_196,&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196}}};
        struct S0 * const l_2445 = &g_427;
        int32_t l_2497[9][7] = {{0xC2825FBDL,0x86D0D46CL,1L,(-1L),0x8E0FB35FL,0xC2825FBDL,(-1L)},{0xA9F654C8L,(-5L),0L,(-6L),(-6L),0L,(-5L)},{0x86D0D46CL,0x62664931L,1L,0xE1C04C94L,0x62664931L,(-1L),7L},{1L,(-6L),9L,1L,(-5L),1L,9L},{0x8E0FB35FL,0x8E0FB35FL,(-1L),0xE1C04C94L,0x86D0D46CL,0xF2908580L,0x8E0FB35FL},{(-2L),9L,0x0CED98E7L,(-6L),7L,7L,(-6L)},{1L,7L,1L,(-1L),0x86D0D46CL,1L,0x62664931L},{(-6L),(-5L),0x17CFAA94L,1L,(-5L),0L,(-5L)},{(-2L),(-1L),(-1L),(-2L),0x62664931L,1L,0x86D0D46CL}};
        uint32_t l_2505 = 1UL;
        uint32_t **l_2525 = &g_2128;
        struct S0 *l_2566 = &g_2567;
        struct S0 *l_2570 = &g_2571;
        int32_t **l_2591 = (void*)0;
        int64_t ***l_2597 = &g_197;
        int64_t ****l_2596 = &l_2597;
        uint32_t *l_2598 = &g_361.f0;
        int32_t l_2600 = 0x4E8E13B7L;
        uint64_t l_2684 = 0x473D2995660E0B3DLL;
        union U1 *l_2690[8] = {&g_1609,&g_1880,&g_1609,&g_1609,&g_1880,&g_1609,&g_1609,&g_1880};
        int64_t l_2697 = 0L;
        int32_t l_2728[5][5];
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 5; j++)
                l_2728[i][j] = 0x55718724L;
        }
        for (g_16 = 4; (g_16 >= 20); g_16++)
        { /* block id: 1076 */
            int32_t l_2388[9][8][3] = {{{(-7L),(-7L),1L},{(-6L),0x968F0B6BL,0x676EC767L},{0xD0EE9BEEL,(-7L),0xD0EE9BEEL},{0xD0EE9BEEL,(-6L),(-7L)},{(-6L),0xD0EE9BEEL,0xD0EE9BEEL},{(-7L),0xD0EE9BEEL,0x676EC767L},{0x968F0B6BL,(-6L),1L},{(-7L),(-7L),1L}},{{(-6L),0x968F0B6BL,0x676EC767L},{0xD0EE9BEEL,(-7L),0xD0EE9BEEL},{0xD0EE9BEEL,(-6L),(-7L)},{(-6L),0xD0EE9BEEL,0xD0EE9BEEL},{(-7L),0xD0EE9BEEL,0x676EC767L},{0x968F0B6BL,(-6L),1L},{(-7L),(-7L),1L},{(-6L),0x968F0B6BL,0x676EC767L}},{{0xD0EE9BEEL,(-7L),0xD0EE9BEEL},{0xD0EE9BEEL,(-6L),(-7L)},{(-6L),0xD0EE9BEEL,0xD0EE9BEEL},{(-7L),0xD0EE9BEEL,0x676EC767L},{0x968F0B6BL,(-6L),1L},{(-7L),(-7L),1L},{(-6L),0x968F0B6BL,0x676EC767L},{0xD0EE9BEEL,(-7L),0xD0EE9BEEL}},{{0xD0EE9BEEL,(-6L),(-7L)},{(-6L),0xD0EE9BEEL,0xD0EE9BEEL},{(-7L),0xD0EE9BEEL,1L},{0x676EC767L,0xD0EE9BEEL,(-7L)},{(-1L),(-1L),(-7L)},{0xD0EE9BEEL,0x676EC767L,1L},{0x968F0B6BL,(-1L),0x968F0B6BL},{0x968F0B6BL,0xD0EE9BEEL,(-1L)}},{{0xD0EE9BEEL,0x968F0B6BL,0x968F0B6BL},{(-1L),0x968F0B6BL,1L},{0x676EC767L,0xD0EE9BEEL,(-7L)},{(-1L),(-1L),(-7L)},{0xD0EE9BEEL,0x676EC767L,1L},{0x968F0B6BL,(-1L),0x968F0B6BL},{0x968F0B6BL,0xD0EE9BEEL,(-1L)},{0xD0EE9BEEL,0x968F0B6BL,0x968F0B6BL}},{{(-1L),0x968F0B6BL,1L},{0x676EC767L,0xD0EE9BEEL,(-7L)},{(-1L),(-1L),(-7L)},{0xD0EE9BEEL,0x676EC767L,1L},{0x968F0B6BL,(-1L),0x968F0B6BL},{0x968F0B6BL,0xD0EE9BEEL,(-1L)},{0xD0EE9BEEL,0x968F0B6BL,0x968F0B6BL},{(-1L),0x968F0B6BL,1L}},{{0x676EC767L,0xD0EE9BEEL,(-7L)},{(-1L),(-1L),(-7L)},{0xD0EE9BEEL,0x676EC767L,1L},{0x968F0B6BL,(-1L),0x968F0B6BL},{0x968F0B6BL,0xD0EE9BEEL,(-1L)},{0xD0EE9BEEL,0x968F0B6BL,0x968F0B6BL},{(-1L),0x968F0B6BL,1L},{0x676EC767L,0xD0EE9BEEL,(-7L)}},{{(-1L),(-1L),(-7L)},{0xD0EE9BEEL,0x676EC767L,1L},{0x968F0B6BL,(-1L),0x968F0B6BL},{0x968F0B6BL,0xD0EE9BEEL,(-1L)},{0xD0EE9BEEL,0x968F0B6BL,0x968F0B6BL},{(-1L),0x968F0B6BL,1L},{0x676EC767L,0xD0EE9BEEL,(-7L)},{(-1L),(-1L),(-7L)}},{{0xD0EE9BEEL,0x676EC767L,1L},{0x968F0B6BL,(-1L),0x968F0B6BL},{0x968F0B6BL,0xD0EE9BEEL,(-1L)},{0xD0EE9BEEL,0x968F0B6BL,0x968F0B6BL},{(-1L),0x968F0B6BL,1L},{0x676EC767L,0xD0EE9BEEL,(-7L)},{(-1L),(-1L),(-7L)},{0xD0EE9BEEL,0x676EC767L,1L}}};
            int i, j, k;
            ++l_2389;
        }
lbl_2589:
        for (g_433.f0 = (-29); (g_433.f0 <= 20); g_433.f0++)
        { /* block id: 1081 */
            int32_t l_2397 = 0xA592DFB3L;
            int32_t l_2475 = 0x1D977C2EL;
            uint64_t l_2508[3];
            int32_t l_2512[6] = {(-1L),0xB6AAE44DL,0xB6AAE44DL,(-1L),0xB6AAE44DL,0xB6AAE44DL};
            int32_t * const *l_2541[3][5][7] = {{{(void*)0,&l_2375[0],&g_391,&l_2375[1],(void*)0,&l_2375[0],&l_2375[0]},{(void*)0,&l_2374,&l_2375[1],&l_2374,(void*)0,&l_2375[0],&l_2375[1]},{(void*)0,&l_2375[1],&l_2375[0],&g_391,(void*)0,(void*)0,&l_2375[1]},{&g_391,&l_2375[1],&l_2375[0],&l_2375[1],(void*)0,&l_2374,(void*)0},{(void*)0,&g_391,&g_391,(void*)0,&l_2375[1],&l_2375[0],&g_391}},{{(void*)0,&l_2375[0],&l_2375[1],&l_2375[1],&l_2375[1],&l_2375[0],(void*)0},{(void*)0,(void*)0,&l_2375[0],&g_391,(void*)0,(void*)0,&g_391},{&l_2375[0],&l_2375[0],&l_2375[0],&l_2375[1],&g_391,&l_2375[1],(void*)0},{(void*)0,&l_2375[0],&l_2375[1],&l_2375[1],(void*)0,&l_2375[0],&l_2375[0]},{&l_2375[1],&l_2375[1],&l_2374,&l_2375[1],(void*)0,&l_2375[1],&l_2374}},{{(void*)0,(void*)0,&l_2375[0],&l_2375[1],(void*)0,(void*)0,&g_391},{&g_391,&l_2375[1],(void*)0,&l_2375[1],&g_391,&l_2374,&g_391},{&l_2375[1],&l_2375[0],&l_2375[0],(void*)0,(void*)0,&l_2375[0],&l_2375[0]},{&l_2374,&l_2375[0],&g_391,&l_2375[1],(void*)0,&l_2375[1],(void*)0},{&l_2375[1],(void*)0,&l_2375[0],&l_2375[0],(void*)0,&l_2375[1],&l_2375[0]}}};
            uint32_t l_2578 = 0UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2508[i] = 18446744073709551612UL;
            for (g_427.f0 = (-23); (g_427.f0 >= 5); g_427.f0 = safe_add_func_uint64_t_u_u(g_427.f0, 8))
            { /* block id: 1084 */
                int32_t l_2396 = 0x92E6F447L;
                uint32_t *l_2421 = &g_408;
                uint32_t l_2422 = 0x1569356AL;
                uint64_t *l_2423 = (void*)0;
                uint64_t *l_2424 = &g_2015;
                uint64_t *l_2425 = &g_515;
                l_2397 ^= l_2396;
                g_2426 = (safe_add_func_uint64_t_u_u(0x1A200B15A4E9F594LL, ((*l_2425) &= (safe_sub_func_uint64_t_u_u(((*l_2424) ^= (l_2389 < (safe_rshift_func_int8_t_s_s((((*l_2421) = ((*g_1932) = ((((safe_rshift_func_uint8_t_u_u((g_93[1] > (safe_rshift_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s(l_2396, ((l_2412 != (void*)0) , (safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(l_2397, ((void*)0 == l_2417[3]))), 0xB2A0L))))), 12)) , l_2396), g_228[4]))), (***g_1227))) , l_2418) == l_2420[2][1][0]) != 0x59C51AEB2CC83618LL))) && l_2422), 1)))), (***g_907))))));
            }
            for (g_361.f0 = 0; (g_361.f0 <= 4); g_361.f0 += 1)
            { /* block id: 1094 */
                int32_t l_2432 = (-1L);
                int32_t l_2444 = 1L;
                int32_t l_2502 = 6L;
                int32_t l_2504[9][2][7] = {{{0x1237E278L,(-9L),0xEC949949L,(-1L),0xEC949949L,(-9L),0x1237E278L},{(-9L),(-1L),0x737DE936L,(-8L),(-9L),0x91300BDFL,(-1L)}},{{0x41C29687L,(-9L),0x41C29687L,0x7079186DL,0xE1461110L,1L,0x32C5AF18L},{(-8L),0xF3DDB511L,0x737DE936L,0x737DE936L,0xF3DDB511L,(-8L),0x9324F858L}},{{(-5L),0x7079186DL,0xEC949949L,1L,1L,1L,0xEC949949L},{(-9L),0x91300BDFL,(-1L),8L,(-8L),0x91300BDFL,0x91300BDFL}},{{1L,0x7079186DL,0L,0x7079186DL,1L,(-9L),0xE1461110L},{0x2AA58A46L,0xF3DDB511L,1L,(-1L),(-8L),0x2AA58A46L,0x9324F858L}},{{0xEC949949L,(-9L),1L,(-1L),1L,0x7079186DL,1L},{0x2AA58A46L,(-1L),(-1L),0x2AA58A46L,0xF3DDB511L,1L,(-1L)}},{{1L,(-9L),0xE1461110L,(-1L),0xE1461110L,(-9L),1L},{(-9L),(-8L),0x737DE936L,(-1L),(-9L),(-9L),(-1L)}},{{(-5L),(-9L),(-5L),0x7079186DL,0xEC949949L,1L,1L},{(-8L),0x9324F858L,0x737DE936L,8L,0xF3DDB511L,(-1L),0x9324F858L}},{{0x41C29687L,0x7079186DL,0xE1461110L,1L,0x32C5AF18L,1L,0xE1461110L},{(-9L),(-9L),(-1L),0x737DE936L,(-8L),(-9L),0x91300BDFL}},{{0x1237E278L,0x7079186DL,1L,0x7079186DL,0x1237E278L,(-9L),0xEC949949L},{0x2AA58A46L,0x9324F858L,1L,(-8L),(-8L),1L,0x9324F858L}}};
                uint32_t l_2514 = 9UL;
                uint64_t l_2535 = 0UL;
                const int32_t *l_2585[9][3][9] = {{{&l_2497[6][5],&l_2512[5],&g_16,(void*)0,&l_2497[1][5],&g_4[9],(void*)0,(void*)0,(void*)0},{&l_2444,&l_2502,&l_2512[3],&g_4[9],(void*)0,(void*)0,&l_2502,(void*)0,&g_4[9]},{(void*)0,(void*)0,(void*)0,&g_4[9],&l_2378,(void*)0,&l_2497[6][6],&l_2444,&l_2378}},{{(void*)0,(void*)0,&g_113,&l_2512[1],&l_2497[8][4],(void*)0,&l_2512[3],&l_2512[3],(void*)0},{&l_2497[6][5],&g_4[9],(void*)0,&g_4[9],&l_2497[6][5],&g_4[9],&l_2378,&l_2378,(void*)0},{(void*)0,&l_2512[4],(void*)0,&g_4[9],&l_2378,&l_2512[3],&l_2475,&g_4[9],&g_4[9]}},{{&l_2444,&g_4[9],&l_2378,&l_2512[5],&l_2378,&g_4[9],&l_2444,&l_2502,&l_2502},{&l_2502,&l_2497[8][4],&g_16,&g_113,(void*)0,(void*)0,&l_2512[4],&g_16,(void*)0},{(void*)0,&l_2502,&l_2378,(void*)0,&g_4[6],(void*)0,&l_2378,&l_2502,(void*)0}},{{&l_2444,&g_113,(void*)0,(void*)0,&l_2475,(void*)0,&g_113,&g_4[9],(void*)0},{&l_2378,(void*)0,&l_2502,&l_2378,&l_2497[6][6],&l_2502,&l_2497[6][6],&l_2378,&l_2502},{&l_2444,&l_2475,&g_113,(void*)0,(void*)0,&l_2444,(void*)0,&l_2512[3],&g_4[9]}},{{(void*)0,&l_2444,&g_16,&g_4[9],(void*)0,&l_2444,(void*)0,&l_2444,(void*)0},{&l_2502,&g_113,&g_113,&l_2502,&l_2378,&g_16,&l_2512[3],(void*)0,&l_2502},{&l_2444,&g_4[9],&l_2502,&l_2502,&l_2444,&g_4[9],&l_2378,&l_2512[5],&l_2378}},{{(void*)0,&l_2497[8][4],(void*)0,(void*)0,&l_2378,(void*)0,&l_2512[4],(void*)0,&g_4[9]},{&l_2497[6][5],&g_4[9],&l_2378,&l_2512[5],(void*)0,&g_4[9],&g_4[9],&l_2502,&l_2497[1][5]},{(void*)0,&l_2512[4],&g_16,(void*)0,(void*)0,&g_16,&l_2512[4],(void*)0,(void*)0}},{{(void*)0,&l_2502,&l_2378,&l_2444,&l_2497[6][6],(void*)0,&l_2378,&g_4[9],(void*)0},{&l_2444,(void*)0,(void*)0,&g_113,&l_2475,&l_2444,&l_2512[3],(void*)0,(void*)0},{(void*)0,&l_2444,(void*)0,&l_2378,&g_4[6],&g_4[9],(void*)0,&l_2378,&l_2497[1][5]}},{{&l_2475,&l_2502,&g_113,&g_4[9],(void*)0,(void*)0,(void*)0,(void*)0,&g_4[9]},{(void*)0,&g_4[9],&l_2502,(void*)0,(void*)0,&l_2378,&l_2444,&l_2512[5],(void*)0},{&g_4[9],&l_2502,&g_16,(void*)0,&l_2444,&g_4[9],&l_2512[1],&g_16,&g_4[9]}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_2502,&g_16,(void*)0,(void*)0},{(void*)0,&l_2502,&l_2475,&l_2497[8][4],&l_2475,&g_16,&l_2502,&l_2378,&l_2378},{&l_2378,&l_2502,(void*)0,&l_2444,(void*)0,&l_2502,&l_2378,&l_2444,&l_2497[6][6]}}};
                const int32_t **l_2586 = (void*)0;
                int i, j, k;
                for (g_20 = 1; (g_20 >= 0); g_20 -= 1)
                { /* block id: 1097 */
                    int32_t l_2431 = 1L;
                    int64_t ***l_2447 = &l_2420[2][1][0];
                    int32_t *l_2448 = &g_4[3];
                    uint8_t ****l_2472 = &g_1902;
                    uint64_t l_2474 = 18446744073709551615UL;
                    const uint16_t *l_2490 = &g_323;
                    const uint16_t **l_2489 = &l_2490;
                    int i, j;
                    if (((((**g_656) |= ((safe_mul_func_int16_t_s_s(g_93[0], (safe_mod_func_uint16_t_u_u((((*g_1229) = 249UL) >= ((g_1732.f0 , ((0xC0889641L == ((((l_2432 |= l_2431) | (safe_sub_func_int32_t_s_s((l_2431 > ((safe_lshift_func_int16_t_s_s(l_2431, 12)) <= (safe_mod_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(((g_2443 , (-2L)) < l_2389), 3L)), 0xB25CL)))), l_2444))) >= l_2444) >= l_2397)) < 4294967293UL)) , l_2389)), l_2397)))) | l_2444)) >= l_2444) != l_2444))
                    { /* block id: 1101 */
                        struct S0 **l_2446 = &g_426[0][1][6];
                        (*l_2446) = l_2445;
                        l_2447 = (void*)0;
                        (*g_1305) = l_2448;
                    }
                    else
                    { /* block id: 1105 */
                        float **l_2449 = &l_2417[4];
                        uint16_t ***l_2453 = &l_2369;
                        uint16_t ****l_2452 = &l_2453;
                        int32_t l_2462 = 0x3F2F67C0L;
                        uint64_t *l_2466 = (void*)0;
                        uint64_t *l_2467 = (void*)0;
                        uint64_t *l_2468 = (void*)0;
                        uint64_t *l_2469 = &g_190[4][2];
                        int32_t l_2473[6][8][5] = {{{(-4L),0xE31D447FL,0xFCEBD946L,(-4L),(-6L)},{9L,(-4L),0x9E777681L,(-4L),9L},{0xFCEBD946L,0x649A4F33L,0xE31D447FL,(-6L),0x649A4F33L},{9L,0xE31D447FL,0xE31D447FL,9L,(-6L)},{(-4L),9L,0x9E777681L,0x649A4F33L,0x649A4F33L},{0xFCEBD946L,9L,0xFCEBD946L,(-6L),9L},{0x649A4F33L,0xE31D447FL,(-6L),0x649A4F33L,(-6L)},{0x649A4F33L,0x649A4F33L,0x9E777681L,9L,(-4L)}},{{0xFCEBD946L,(-4L),(-6L),(-6L),(-4L)},{(-4L),0xE31D447FL,0xFCEBD946L,(-4L),(-6L)},{9L,(-4L),0x9E777681L,(-4L),9L},{0xFCEBD946L,0x649A4F33L,0xE31D447FL,(-6L),0x649A4F33L},{9L,0xE31D447FL,0xE31D447FL,9L,(-6L)},{(-4L),9L,0x9E777681L,0x649A4F33L,0x649A4F33L},{0xFCEBD946L,9L,0xFCEBD946L,(-6L),9L},{0x649A4F33L,0xE31D447FL,(-6L),0x649A4F33L,(-6L)}},{{0x649A4F33L,0x649A4F33L,0x9E777681L,9L,(-4L)},{0xFCEBD946L,(-4L),(-6L),(-6L),(-4L)},{(-4L),0xE31D447FL,0xFCEBD946L,(-4L),(-6L)},{9L,(-4L),0x9E777681L,(-4L),9L},{0xFCEBD946L,0x649A4F33L,0xE31D447FL,(-6L),0x649A4F33L},{9L,0xE31D447FL,0xE31D447FL,9L,(-6L)},{(-4L),9L,0x9E777681L,0x649A4F33L,0x649A4F33L},{0xFCEBD946L,9L,0xFCEBD946L,(-6L),9L}},{{0x649A4F33L,0xE31D447FL,(-6L),0x649A4F33L,(-6L)},{0x649A4F33L,0x649A4F33L,0x9E777681L,9L,(-4L)},{0xFCEBD946L,(-4L),(-6L),(-6L),(-4L)},{(-4L),0xE31D447FL,0xFCEBD946L,(-4L),(-6L)},{9L,(-4L),0x9E777681L,(-4L),9L},{0xFCEBD946L,0x649A4F33L,0x9E777681L,1L,0xFCEBD946L},{0xE31D447FL,0x9E777681L,0x9E777681L,0xE31D447FL,1L},{(-6L),0xE31D447FL,0x649A4F33L,0xFCEBD946L,0xFCEBD946L}},{{0L,0xE31D447FL,0L,1L,0xE31D447FL},{0xFCEBD946L,0x9E777681L,1L,0xFCEBD946L,1L},{0xFCEBD946L,0xFCEBD946L,0x649A4F33L,0xE31D447FL,(-6L)},{0L,(-6L),1L,1L,(-6L)},{(-6L),0x9E777681L,0L,(-6L),1L},{0xE31D447FL,(-6L),0x649A4F33L,(-6L),0xE31D447FL},{0L,0xFCEBD946L,0x9E777681L,1L,0xFCEBD946L},{0xE31D447FL,0x9E777681L,0x9E777681L,0xE31D447FL,1L}},{{(-6L),0xE31D447FL,0x649A4F33L,0xFCEBD946L,0xFCEBD946L},{0L,0xE31D447FL,0L,1L,0xE31D447FL},{0xFCEBD946L,0x9E777681L,1L,0xFCEBD946L,1L},{0xFCEBD946L,0xFCEBD946L,0x649A4F33L,0xE31D447FL,(-6L)},{0L,(-6L),1L,1L,(-6L)},{(-6L),0x9E777681L,0L,(-6L),1L},{0xE31D447FL,(-6L),0x649A4F33L,(-6L),0xE31D447FL},{0L,0xFCEBD946L,0x9E777681L,1L,0xFCEBD946L}}};
                        int i, j, k;
                        (*l_2448) |= ((void*)0 == l_2449);
                        (*l_2448) = (safe_rshift_func_uint16_t_u_u(0UL, 13));
                        l_2475 |= ((((((*l_2452) = (void*)0) == &l_2369) != ((safe_rshift_func_uint16_t_u_u((l_2456[1][2] , (g_2457 , (safe_div_func_int32_t_s_s((safe_sub_func_int32_t_s_s((l_2462 >= ((*l_2448) && l_2432)), (((safe_unary_minus_func_uint64_t_u(l_2397)) < (safe_mod_func_uint32_t_u_u(((--(*l_2469)) < (((l_2473[2][3][3] &= ((((l_2472 == (void*)0) > 0x8DL) >= (*g_1932)) != l_2444)) ^ l_2462) > l_2474)), l_2432))) | 0x658BL))), l_2444)))), 2)) , (-0x8.6p+1))) , 0UL) || g_1735.f4);
                        l_2462 = ((safe_add_func_float_f_f((safe_div_func_float_f_f((*l_2448), (safe_div_func_float_f_f((0xA.1951D7p+83 >= (((safe_div_func_float_f_f(0x1.Cp+1, (safe_sub_func_float_f_f((0x6.8p+1 != ((safe_div_func_float_f_f(l_2473[2][3][3], ((g_2488[1] ^ ((void*)0 != l_2489)) , ((safe_div_func_float_f_f(((((g_2493 , l_2473[2][3][3]) >= (-8L)) , l_2473[2][3][3]) != l_2397), 0x8.BF52C6p-14)) <= 0x4.EE1D85p+73)))) <= l_2475)), l_2389)))) == l_2432) != l_2475)), (-0x1.0p-1))))), 0x0.Ep+1)) <= (*l_2448));
                    }
                    if (g_601)
                        goto lbl_2589;
                }
                for (g_143 = 0; (g_143 <= 4); g_143 += 1)
                { /* block id: 1117 */
                    return l_2475;
                }
                if (l_2432)
                { /* block id: 1120 */
                    uint32_t *l_2494 = &g_1745[4][0];
                    int32_t l_2498 = 0xC0255060L;
                    int32_t l_2499 = (-1L);
                    int32_t l_2500[9][6] = {{1L,0x776579A3L,0xDFA471C3L,0L,0xE0552F0BL,0xF8BF46B1L},{0x91393D2EL,0xC8C2DC67L,0x9213B34CL,0x4494DBD7L,0xE0552F0BL,0x423314E7L},{0xE35911A9L,0x776579A3L,(-8L),0L,8L,0xE35911A9L},{0xC8C2DC67L,0xB1AD0E68L,(-1L),0xB1AD0E68L,0xC8C2DC67L,0x1369024CL},{0xB45A5A54L,0x9213B34CL,0xE0552F0BL,0xDFA471C3L,(-2L),0x13FFA74BL},{0xF8BF46B1L,0x926C500EL,0xDFA471C3L,0x9213B34CL,0xE35911A9L,0x13FFA74BL},{0x423314E7L,0xC8C2DC67L,0xE0552F0BL,(-8L),0L,0x1369024CL},{0xE35911A9L,0xBA438DEEL,(-1L),(-1L),0xBA438DEEL,0xE35911A9L},{0x1369024CL,0L,(-8L),0xE0552F0BL,0xC8C2DC67L,0x423314E7L}};
                    int i, j;
                    if ((&g_408 == l_2494))
                    { /* block id: 1121 */
                        return l_2389;
                    }
                    else
                    { /* block id: 1123 */
                        int64_t l_2495 = 0x12032ACC40884247LL;
                        int32_t l_2501 = 0x5BEC294AL;
                        int32_t l_2503 = 0x71DDA1A2L;
                        l_2505++;
                    }
                    ++l_2508[1];
                }
                else
                { /* block id: 1127 */
                    int32_t * const l_2511 = (void*)0;
                    uint32_t l_2543 = 0x00DB706CL;
                    int32_t l_2545[7] = {0L,0L,0L,0L,0L,0L,0L};
                    uint32_t l_2546 = 4294967295UL;
                    const int32_t * const l_2582 = &l_2512[3];
                    int i;
                    l_2374 = l_2511;
                    for (g_165 = 3; (g_165 >= 0); g_165 -= 1)
                    { /* block id: 1131 */
                        return l_2508[1];
                    }
                    for (g_1599.f0 = 0; (g_1599.f0 <= 4); g_1599.f0 += 1)
                    { /* block id: 1136 */
                        int8_t l_2513 = 0x4DL;
                        int32_t ***l_2531[3][1];
                        int32_t ****l_2530 = &l_2531[0][0];
                        int32_t *****l_2529 = &l_2530;
                        struct S0 *l_2569 = (void*)0;
                        struct S0 **l_2568[8][5][6] = {{{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,&l_2565[0][0][0],&l_2565[0][0][1]},{&l_2569,&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,&l_2565[0][0][3],&l_2565[0][0][0]},{&l_2565[0][0][0],&l_2569,(void*)0,&l_2565[0][0][0],&l_2569,(void*)0},{&l_2565[0][0][2],&l_2565[0][0][2],&l_2565[0][0][1],&l_2565[0][0][2],&l_2565[0][0][3],&l_2565[0][0][0]},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2569,&l_2565[0][0][0],&l_2565[0][0][3],&l_2565[0][0][0]}},{{(void*)0,&l_2565[0][0][2],&l_2565[0][0][0],&l_2569,&l_2569,&l_2565[0][0][0]},{&l_2569,&l_2569,&l_2565[0][0][0],&l_2569,&l_2565[0][0][2],&l_2565[0][0][1]},{&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,&l_2565[0][0][2],(void*)0,&l_2565[0][0][1]},{&l_2569,(void*)0,&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,&l_2569},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][3],&l_2565[0][0][1],&l_2565[0][0][0],&l_2565[0][0][3]}},{{(void*)0,&l_2569,(void*)0,&l_2565[0][0][0],&l_2565[0][0][0],(void*)0},{(void*)0,&l_2565[0][0][1],&l_2565[0][0][0],&l_2565[0][0][1],(void*)0,&l_2569},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][2],&l_2565[0][0][0],&l_2565[0][0][1],&l_2565[0][0][3]},{&l_2569,&l_2569,&l_2565[0][0][1],&l_2565[0][0][2],&l_2565[0][0][2],&l_2565[0][0][0]},{&l_2565[0][0][0],&l_2569,&l_2565[0][0][2],&l_2569,&l_2565[0][0][1],&l_2565[0][0][2]}},{{(void*)0,&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,(void*)0},{(void*)0,&l_2565[0][0][1],(void*)0,&l_2569,&l_2565[0][0][0],&l_2565[0][0][0]},{&l_2565[0][0][1],&l_2569,(void*)0,(void*)0,&l_2565[0][0][0],(void*)0},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][3],(void*)0,&l_2565[0][0][2]},{&l_2565[0][0][3],(void*)0,&l_2565[0][0][2],&l_2565[0][0][0],(void*)0,&l_2565[0][0][0]}},{{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][1],&l_2565[0][0][0],&l_2565[0][0][2],&l_2565[0][0][3]},{&l_2565[0][0][3],&l_2569,&l_2565[0][0][2],&l_2565[0][0][3],&l_2569,&l_2569},{&l_2565[0][0][0],(void*)0,&l_2565[0][0][0],(void*)0,&l_2565[0][0][0],(void*)0},{&l_2565[0][0][1],&l_2565[0][0][0],(void*)0,&l_2569,&l_2565[0][0][0],&l_2565[0][0][3]},{(void*)0,(void*)0,&l_2565[0][0][3],&l_2565[0][0][0],&l_2569,&l_2569}},{{(void*)0,&l_2569,&l_2565[0][0][0],&l_2569,&l_2565[0][0][2],&l_2565[0][0][1]},{&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,&l_2565[0][0][2],(void*)0,&l_2565[0][0][1]},{&l_2569,(void*)0,&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,&l_2569},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][3],&l_2565[0][0][1],&l_2565[0][0][0],&l_2565[0][0][3]},{(void*)0,&l_2569,(void*)0,&l_2565[0][0][0],&l_2565[0][0][0],(void*)0}},{{(void*)0,&l_2565[0][0][1],&l_2565[0][0][0],&l_2565[0][0][1],(void*)0,&l_2569},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][2],&l_2565[0][0][0],&l_2565[0][0][1],&l_2565[0][0][3]},{&l_2569,&l_2569,&l_2565[0][0][1],&l_2565[0][0][2],&l_2565[0][0][2],&l_2565[0][0][0]},{&l_2565[0][0][0],&l_2569,&l_2565[0][0][2],&l_2569,&l_2565[0][0][1],&l_2565[0][0][2]},{(void*)0,&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][0],(void*)0,(void*)0}},{{(void*)0,&l_2565[0][0][1],(void*)0,&l_2569,&l_2565[0][0][0],&l_2565[0][0][0]},{&l_2565[0][0][1],&l_2569,(void*)0,(void*)0,&l_2565[0][0][0],(void*)0},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][3],(void*)0,&l_2565[0][0][2]},{&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][3],&l_2565[0][0][0],&l_2569,&l_2565[0][0][2]},{&l_2569,&l_2569,&l_2565[0][0][3],&l_2565[0][0][0],&l_2565[0][0][0],&l_2565[0][0][0]}}};
                        uint32_t l_2577 = 18446744073709551609UL;
                        int16_t *l_2579 = &l_2456[4][2];
                        uint64_t *l_2580[4][1][5] = {{{(void*)0,&g_190[4][2],(void*)0,&g_190[4][2],(void*)0}},{{(void*)0,&l_2508[0],&l_2508[0],(void*)0,(void*)0}},{{&l_2508[1],&g_190[4][2],&l_2508[1],&g_190[4][2],&l_2508[1]}},{{(void*)0,(void*)0,&l_2508[0],&l_2508[0],(void*)0}}};
                        int32_t l_2581 = (-7L);
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_2531[i][j] = &l_2412;
                        }
                        l_2514++;
                        l_2502 = (safe_add_func_float_f_f(((safe_div_func_float_f_f((((safe_add_func_float_f_f(((((safe_mul_func_uint8_t_u_u(((void*)0 != l_2525), ((((safe_sub_func_uint32_t_u_u((g_2528 , ((((g_995 , &g_2022) != l_2529) == (safe_sub_func_uint64_t_u_u(((safe_unary_minus_func_int64_t_s(l_2535)) || (!(l_2508[0] , (safe_lshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u((&l_2511 == l_2541[0][0][4]), l_2542)), 2))))), g_1609.f4))) <= 1L)), 0x6DB580DCL)) != (*g_657)) >= l_2505) ^ 0xF055L))) && l_2543) , l_2544) , g_228[4]), g_2457.f4)) < l_2504[1][1][0]) != g_276[0]), g_1828)) >= l_2545[2]), g_601));
                        if (l_2546)
                            continue;
                        l_2581 |= (safe_rshift_func_uint8_t_u_s(0x4EL, ((safe_mod_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((safe_sub_func_uint64_t_u_u((g_383 &= ((safe_mul_func_int16_t_s_s(((g_165 |= ((0x0651L & ((*l_2579) = (safe_lshift_func_uint16_t_u_s(((safe_mul_func_uint8_t_u_u(((**g_1903) = l_2504[1][1][0]), l_2504[1][1][0])) > (((safe_div_func_uint16_t_u_u((g_276[g_1599.f0] = (safe_sub_func_uint64_t_u_u(((void*)0 == &g_190[4][2]), ((l_2566 = l_2565[0][0][0]) == (l_2570 = ((*g_425) = (void*)0)))))), (safe_div_func_uint8_t_u_u((+(safe_mul_func_int16_t_s_s((((l_2577 , 251UL) >= (*g_1190)) >= g_431.f1), l_2578))), (***g_1378))))) || l_2514) > (*g_1932))), 0)))) != 4L)) < g_428[0][2].f0), 0L)) != 0x05E8L)), g_326.f1)), (***g_1227))), l_2504[1][1][0])) | l_2545[2])));
                    }
                    (*g_2583) = l_2582;
                }
                l_2587 = l_2585[0][1][3];
            }
            if (l_2588)
                break;
        }
        if ((l_2600 = (l_2497[6][0] = (+((((void*)0 == l_2591) && (safe_mod_func_int8_t_s_s(0L, (*g_657)))) , (safe_div_func_int64_t_s_s((((((**g_1776) = (((((((void*)0 == (*g_589)) , &l_2420[2][1][0]) == ((*l_2596) = &l_2420[2][1][0])) , (((*l_2525) = &l_2542) != l_2598)) | g_1735.f1) < g_433.f2)) , 0xF9154FC3B148F6C9LL) <= 0x4D69664490FDEA12LL) , g_2599), 0xF27B3E2802E02FE4LL)))))))
        { /* block id: 1162 */
            uint16_t l_2616 = 0x3A3AL;
            int32_t l_2620 = 0x04981CA9L;
            float l_2641[8][9][2] = {{{0x3.9p+1,(-0x8.Bp+1)},{0x1.0082C6p+85,(-0x1.0p+1)},{(-0x1.0p+1),0x0.CE782Ep-76},{0x7.Dp-1,0x6.6215C9p-80},{0x7.E2329Ep-53,0x3.9p+1},{0x2.0p-1,0x1.607425p-16},{(-0x9.8p+1),0x1.607425p-16},{0x2.0p-1,0x3.9p+1},{0x7.E2329Ep-53,0x6.6215C9p-80}},{{0x7.Dp-1,0x0.CE782Ep-76},{(-0x1.0p+1),(-0x1.0p+1)},{0x1.0082C6p+85,(-0x8.Bp+1)},{0x3.9p+1,(-0x9.8p+1)},{0x0.3p+1,0x4.4p+1},{(-0x8.Bp+1),0x0.3p+1},{0x0.1p-1,0x1.Cp+1},{0x0.1p-1,0x0.3p+1},{(-0x8.Bp+1),0x4.4p+1}},{{0x0.3p+1,(-0x9.8p+1)},{0x3.9p+1,(-0x8.Bp+1)},{0x1.0082C6p+85,(-0x1.0p+1)},{(-0x1.0p+1),0x0.CE782Ep-76},{0x7.Dp-1,0x6.6215C9p-80},{0x7.E2329Ep-53,0x3.9p+1},{0x2.0p-1,0x1.607425p-16},{(-0x9.8p+1),0x1.607425p-16},{0x2.0p-1,0x3.9p+1}},{{0x7.E2329Ep-53,0x6.6215C9p-80},{0x7.Dp-1,0x0.CE782Ep-76},{(-0x1.0p+1),(-0x1.0p+1)},{0x1.0082C6p+85,(-0x8.Bp+1)},{0x3.9p+1,(-0x9.8p+1)},{0x0.3p+1,0x4.4p+1},{(-0x8.Bp+1),0x0.3p+1},{0x0.1p-1,0x1.Cp+1},{0x0.1p-1,0x0.3p+1}},{{(-0x8.Bp+1),0x4.4p+1},{0x0.3p+1,(-0x9.8p+1)},{0x3.9p+1,(-0x8.Bp+1)},{0x1.0082C6p+85,(-0x1.0p+1)},{(-0x1.0p+1),0x0.CE782Ep-76},{0x7.Dp-1,0x6.6215C9p-80},{0x7.E2329Ep-53,0x3.9p+1},{0x2.0p-1,0x1.607425p-16},{(-0x9.8p+1),0x1.607425p-16}},{{0x2.0p-1,0x3.9p+1},{0x7.E2329Ep-53,0x6.6215C9p-80},{0x7.Dp-1,0x0.CE782Ep-76},{(-0x1.0p+1),(-0x1.0p+1)},{0x1.0082C6p+85,(-0x8.Bp+1)},{0x3.9p+1,(-0x9.8p+1)},{0x0.3p+1,0x4.4p+1},{(-0x8.Bp+1),0x0.3p+1},{0x0.1p-1,0x1.Cp+1}},{{0x0.1p-1,0x0.3p+1},{(-0x8.Bp+1),0x4.4p+1},{0x0.3p+1,(-0x9.8p+1)},{0x3.9p+1,(-0x8.Bp+1)},{0x1.0082C6p+85,(-0x1.0p+1)},{(-0x1.0p+1),0x0.CE782Ep-76},{0x7.Dp-1,0x6.6215C9p-80},{0x7.E2329Ep-53,0x3.9p+1},{0x2.0p-1,0x1.607425p-16}},{{(-0x9.8p+1),0x1.607425p-16},{0x2.0p-1,0x3.9p+1},{0x7.E2329Ep-53,0x6.6215C9p-80},{0x7.Dp-1,0x0.CE782Ep-76},{(-0x1.0p+1),(-0x1.0p+1)},{0x1.0082C6p+85,(-0x8.Bp+1)},{0x3.9p+1,(-0x9.8p+1)},{0x0.3p+1,0x4.4p+1},{(-0x8.Bp+1),0x0.3p+1}}};
            uint32_t l_2643 = 0xE745C3A9L;
            uint16_t l_2686 = 0UL;
            uint32_t l_2687 = 0x15A760E5L;
            uint32_t l_2688 = 0x0CADEEE0L;
            int16_t *l_2693 = (void*)0;
            int32_t l_2696 = 7L;
            int32_t l_2698 = 1L;
            int32_t l_2699 = 9L;
            int32_t l_2700 = (-1L);
            uint16_t l_2702[2];
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_2702[i] = 0xB3EDL;
            if (((safe_lshift_func_uint16_t_u_u((g_93[0] & (((safe_mul_func_uint8_t_u_u(0UL, (safe_lshift_func_int8_t_s_u((g_2610[0][1][3] = l_2609), 1)))) <= (((safe_rshift_func_int8_t_s_u(2L, 4)) != (safe_mul_func_uint16_t_u_u((+l_2616), (g_2617 , (safe_lshift_func_int8_t_s_s((l_2620 = l_2616), (safe_div_func_int64_t_s_s(((((((**g_656) ^= (g_2623 , 1UL)) >= l_2616) < 0xDBL) || (*g_1932)) ^ g_1387), l_2616)))))))) <= l_2389)) || (*g_1932))), l_2616)) ^ 0x8A06992AL))
            { /* block id: 1166 */
                uint64_t l_2624 = 1UL;
                ++l_2624;
            }
            else
            { /* block id: 1168 */
                int16_t *l_2631 = &g_165;
                uint64_t *l_2642 = &g_190[4][2];
                int32_t l_2644 = 0xB7750F21L;
                int64_t l_2685 = 0L;
                struct S0 * const *l_2692[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_2692[i] = (void*)0;
                g_2646[2][1][1] ^= ((((safe_mod_func_int8_t_s_s(((safe_add_func_uint32_t_u_u((((*l_2631) = (&l_2419 != (void*)0)) | (safe_mul_func_int16_t_s_s(g_2567.f2, l_2620))), (g_2457.f1 <= (((((safe_div_func_uint64_t_u_u(l_2636, g_515)) <= (safe_lshift_func_int16_t_s_u((0L == (safe_add_func_uint64_t_u_u(((*l_2642) = l_2389), (-1L)))), 2))) || (*g_1190)) <= l_2620) == l_2643)))) || g_1230), l_2497[6][4])) > l_2644) >= l_2620) | l_2645);
                for (g_431.f0 = 0; (g_431.f0 <= 7); g_431.f0 += 1)
                { /* block id: 1174 */
                    float **l_2669[4][3][6] = {{{&l_2417[4],(void*)0,&l_2417[4],(void*)0,&l_2417[4],(void*)0},{&l_2417[3],(void*)0,&l_2417[3],(void*)0,&l_2417[3],(void*)0},{&l_2417[4],(void*)0,&l_2417[4],(void*)0,&l_2417[4],(void*)0}},{{&l_2417[3],(void*)0,&l_2417[3],(void*)0,&l_2417[3],(void*)0},{&l_2417[4],(void*)0,&l_2417[4],(void*)0,&l_2417[4],(void*)0},{&l_2417[3],(void*)0,&l_2417[3],(void*)0,&l_2417[3],(void*)0}},{{&l_2417[4],(void*)0,&l_2417[4],(void*)0,&l_2417[4],(void*)0},{&l_2417[3],(void*)0,&l_2417[3],(void*)0,&l_2417[3],(void*)0},{&l_2417[4],(void*)0,&l_2417[4],(void*)0,&l_2417[4],(void*)0}},{{&l_2417[3],(void*)0,&l_2417[3],(void*)0,&l_2417[3],(void*)0},{&l_2417[4],(void*)0,&l_2417[4],(void*)0,&l_2417[4],(void*)0},{&l_2417[3],(void*)0,&l_2417[3],(void*)0,&l_2417[3],(void*)0}}};
                    int32_t l_2683 = (-1L);
                    int i, j, k;
                    for (g_383 = 0; (g_383 <= 1); g_383 += 1)
                    { /* block id: 1177 */
                        uint32_t *****l_2662 = &g_2660;
                        float **l_2670 = &l_2417[3];
                        float ***l_2671 = &l_2670;
                        const uint32_t ** const ***l_2682 = &l_2678;
                        int32_t l_2689 = 0x3135D210L;
                        int i, j;
                        l_2689 = ((1UL >= ((safe_div_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(l_2456[(g_383 + 3)][(g_383 + 6)], 4)), (safe_add_func_uint16_t_u_u((safe_add_func_int8_t_s_s((+(((safe_rshift_func_uint16_t_u_s((l_2687 = ((safe_mod_func_int16_t_s_s(((((*l_2662) = g_2660) != ((0xF0CA299CL > ((l_2456[(g_383 + 3)][(g_383 + 6)] , (((safe_rshift_func_uint8_t_u_s((safe_sub_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((((*g_589) = l_2669[1][1][3]) == ((*l_2671) = l_2670)), (safe_div_func_int16_t_s_s(((safe_add_func_uint64_t_u_u((((*l_2631) = (safe_div_func_uint64_t_u_u((((*l_2682) = l_2678) != (void*)0), l_2683))) ^ l_2684), (*g_909))) || g_361.f1), 8UL)))), l_2685)), 2)) != l_2686) > 0xABA9CD60863E6060LL)) > 0x9050A612L)) , (void*)0)) , l_2456[(g_383 + 3)][(g_383 + 6)]), 0x0EFCL)) | l_2643)), l_2683)) | 0x8F12AC7278D54F97LL) & l_2456[(g_383 + 3)][(g_383 + 6)])), 2UL)), l_2643)))) >= (***g_1902))) , l_2688);
                        (*g_2691) = l_2690[2];
                    }
                    for (l_2505 = 0; (l_2505 <= 1); l_2505 += 1)
                    { /* block id: 1189 */
                        int16_t *l_2694 = &g_165;
                        int16_t **l_2695 = &g_1818;
                        int i;
                        l_2644 |= (((0x9CF85240L ^ (l_2688 , (l_2692[0] == &l_2565[0][0][0]))) , l_2693) != ((*l_2695) = l_2694));
                        l_2375[l_2505] = &l_2620;
                    }
                }
            }
            --l_2702[0];
            (*l_2412) = &l_2378;
        }
        else
        { /* block id: 1198 */
            const int64_t l_2705 = 0L;
            uint32_t *l_2725[9][6][4] = {{{&g_995,(void*)0,&g_1257[0][0][0],&g_1745[5][1]},{&g_1745[5][1],&l_2505,&g_1257[0][1][0],(void*)0},{&g_995,&g_1257[0][1][5],&g_995,&g_1257[0][1][5]},{(void*)0,&g_1745[6][1],&g_1257[0][0][0],&g_408},{&l_2505,&g_1745[5][1],(void*)0,&g_1257[0][0][4]},{&g_2610[0][1][3],&g_1257[0][1][5],&g_2610[0][1][3],&g_995}},{{&g_2610[0][1][3],(void*)0,(void*)0,&g_1745[5][1]},{&l_2505,&g_995,&g_1257[0][0][0],&g_143},{(void*)0,&l_2505,&g_995,&g_995},{&g_995,&g_1257[0][0][4],&g_1257[0][1][0],&g_1257[0][1][5]},{&g_1745[5][1],&g_1745[5][1],&g_1257[0][0][0],&g_1745[5][1]},{&g_995,&g_1745[5][1],&g_1745[5][1],&g_1257[0][1][5]}},{{&g_2610[0][1][3],&g_1257[0][0][4],&g_1745[5][1],&g_995},{(void*)0,&l_2505,(void*)0,&g_143},{&g_995,&g_995,&g_2610[0][2][5],&g_1745[5][1]},{(void*)0,(void*)0,&g_1257[0][1][0],&g_995},{(void*)0,&g_1257[0][1][5],&g_1257[0][1][0],&g_1257[0][0][4]},{(void*)0,&g_1745[5][1],&g_2610[0][2][5],&g_408}},{{&g_995,&g_1745[6][1],(void*)0,&g_1257[0][1][5]},{(void*)0,&g_1257[0][1][5],&g_1745[5][1],(void*)0},{&g_2610[0][1][3],&l_2505,&g_1745[5][1],&g_1745[5][1]},{&g_995,(void*)0,&g_1257[0][0][0],&g_1745[5][1]},{&g_1745[5][1],&l_2505,&g_1257[0][1][0],(void*)0},{&g_995,&g_1257[0][1][5],&g_995,&g_1257[0][1][5]}},{{(void*)0,&g_1745[6][1],&g_1257[0][0][0],&g_408},{&l_2505,&g_1745[5][1],(void*)0,&g_1257[0][0][4]},{&g_2610[0][1][3],&g_1257[0][1][5],&g_2610[0][1][3],&g_995},{&g_2610[0][1][3],(void*)0,(void*)0,&g_1745[5][1]},{&l_2505,&g_995,&g_1257[0][0][0],&g_143},{(void*)0,&l_2505,&g_995,&g_995}},{{&g_995,&g_1257[0][0][4],&g_1257[0][1][0],&g_1257[0][1][5]},{&g_1745[5][1],&g_1745[5][1],&g_1257[0][0][0],&g_1745[5][1]},{&g_995,&g_1745[5][1],&g_1745[5][1],&g_1257[0][1][5]},{&g_2610[0][1][3],&g_143,&g_1745[5][1],&g_143},{(void*)0,&g_2610[0][1][3],(void*)0,&g_1745[6][0]},{(void*)0,&g_143,&g_2610[0][1][3],&g_1745[5][1]}},{{&g_143,&g_408,&g_2610[0][1][3],&g_143},{&g_2610[0][2][5],&g_1745[6][1],&g_2610[0][1][3],&g_143},{&g_143,(void*)0,&g_2610[0][1][3],(void*)0},{(void*)0,&g_2610[0][3][3],(void*)0,&g_1745[6][1]},{(void*)0,&g_1745[6][1],&g_1745[5][1],&g_1745[5][1]},{(void*)0,&g_2610[0][1][3],&g_995,&g_1745[5][1]}},{{(void*)0,&g_1745[5][1],&g_995,&g_1745[5][1]},{&g_1745[5][1],&g_2610[0][1][3],&g_2610[0][1][3],&g_1745[5][1]},{&g_1257[0][0][0],&g_1745[6][1],(void*)0,&g_1745[6][1]},{&g_143,&g_2610[0][3][3],&g_995,(void*)0},{&g_1745[5][1],(void*)0,(void*)0,&g_143},{(void*)0,&g_1745[6][1],&l_2366,&g_143}},{{(void*)0,&g_408,(void*)0,&g_1745[5][1]},{&g_1745[5][1],&g_143,&g_995,&g_1745[6][0]},{&g_143,&g_2610[0][1][3],(void*)0,&g_143},{&g_1257[0][0][0],&g_143,&g_2610[0][1][3],&g_1745[6][1]},{&g_1745[5][1],(void*)0,&g_995,&g_143},{(void*)0,(void*)0,&g_995,&g_1745[6][1]}}};
            uint64_t *l_2727 = &g_515;
            uint8_t l_2744 = 0UL;
            int i, j, k;
lbl_2731:
            l_2728[0][2] |= (((((**g_1776) , (l_2705 != (safe_mod_func_int32_t_s_s(((safe_sub_func_uint32_t_u_u((g_2488[1] , (*g_1932)), (l_2600 = (l_2497[6][5] = (safe_rshift_func_int8_t_s_u((!((l_2705 <= ((*l_2727) &= ((*g_1932) || (l_2705 >= ((safe_add_func_uint16_t_u_u(((((safe_rshift_func_uint8_t_u_s((((safe_rshift_func_int8_t_s_u(((*g_2128) , (safe_mod_func_uint64_t_u_u((safe_add_func_uint16_t_u_u(((void*)0 == l_2725[1][2][0]), l_2697)), 18446744073709551615UL))), 7)) , &g_2288[5]) != l_2726), (*g_1190))) || 0x59B95AF5L) >= 4UL) > l_2705), l_2705)) == 18446744073709551609UL))))) == (*g_1932))), (***g_1902))))))) & 6L), 2L)))) && 0UL) != 0x1F66L) > l_2705);
            for (l_2377 = 7; (l_2377 <= 36); l_2377++)
            { /* block id: 1205 */
                int16_t l_2738 = 0xB8EDL;
                uint8_t ***l_2739 = &g_1903;
                if (g_995)
                    goto lbl_2731;
                for (g_1230 = 0; (g_1230 <= 17); g_1230 = safe_add_func_int64_t_s_s(g_1230, 6))
                { /* block id: 1209 */
                    uint64_t *l_2740 = (void*)0;
                    int8_t l_2741 = (-5L);
                    struct S0 *l_2747 = &g_430;
                    uint8_t *****l_2750[1][3];
                    uint8_t **** const *l_2751 = &l_2749;
                    int8_t l_2752 = 0L;
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_2750[i][j] = (void*)0;
                    }
                    l_2744 &= ((((safe_lshift_func_int8_t_s_u((**g_2382), 4)) < l_2705) && (((*l_2727) = l_2738) == (l_2741 = (l_2739 == l_2739)))) ^ ((*g_1190) <= (safe_lshift_func_int16_t_s_u(l_2705, 0))));
                    for (g_1599.f0 = 11; (g_1599.f0 >= 19); g_1599.f0++)
                    { /* block id: 1215 */
                        if (g_515)
                            goto lbl_2589;
                        (*g_425) = l_2747;
                        (*l_2412) = l_2725[4][2][3];
                    }
                    l_2752 = ((l_2750[0][0] = l_2748) != l_2751);
                }
                for (l_2496 = 0; (l_2496 > (-11)); l_2496 = safe_sub_func_int8_t_s_s(l_2496, 8))
                { /* block id: 1225 */
                    (*g_1305) = g_2755;
                }
            }
        }
    }
    l_2756++;
    for (g_2376 = 2; (g_2376 >= 0); g_2376 -= 1)
    { /* block id: 1234 */
        return g_326.f1;
    }
    return l_2759;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_19 g_21 g_84 g_20 g_16 g_93 g_143 g_113 g_37 g_149 g_165 g_148 g_194 g_228 g_190 g_276 g_227 g_325 g_324 g_13 g_383 g_361.f1 g_391 g_408 g_370 g_425 g_455 g_431.f1 g_470 g_499 g_432.f1 g_432.f0 g_515 g_548 g_427.f1 g_552 g_573 g_428.f0 g_430.f0 g_1304 g_1190 g_361.f0 g_684.f4 g_433.f0 g_1227 g_1228 g_1229 g_1230 g_657 g_681.f0 g_682.f1 g_1497 g_656 g_427.f0 g_1523 g_1536 g_428.f1 g_601 g_1257 g_1378 g_1379 g_1522 g_1732.f1 g_1745 g_1609.f0 g_1305 g_1776 g_1796 g_1902 g_1818 g_1903 g_2015 g_1932 g_2128 g_909 g_910 g_2277 g_908 g_2182 g_2320 g_2324 g_2338
 * writes: g_11 g_13 g_16 g_21 g_37 g_20 g_143 g_148 g_149 g_93 g_165 g_4 g_190 g_113 g_227 g_228 g_276 g_196 g_323 g_324 g_325 g_370 g_383 g_391 g_408 g_424 g_426 g_455 g_499 g_515 g_548 g_601 g_430.f0 g_1257 g_682.f1 g_361.f0 g_433.f0 g_681.f0 g_1230 g_1522 g_1160 g_1536 g_1818 g_1931 g_2015 g_1050 g_2021 g_1828 g_2128 g_84 g_2277 g_2288 g_427.f0 g_1497 g_1745 g_2320
 */
static uint16_t  func_2(int8_t  p_3)
{ /* block id: 1 */
    uint32_t l_6[7][5] = {{1UL,0x1BC383C6L,0xC4307E4AL,0x1BC383C6L,1UL},{18446744073709551615UL,0x2733582FL,1UL,18446744073709551608UL,18446744073709551608UL},{0x9F3FAA32L,0x1BC383C6L,0x9F3FAA32L,6UL,0x9F3FAA32L},{18446744073709551615UL,18446744073709551608UL,0x2733582FL,0x2733582FL,18446744073709551608UL},{1UL,6UL,0xC4307E4AL,6UL,1UL},{18446744073709551608UL,0x2733582FL,0x2733582FL,18446744073709551608UL,18446744073709551615UL},{0x9F3FAA32L,6UL,0x9F3FAA32L,0x1BC383C6L,0x9F3FAA32L}};
    int8_t *l_33 = &g_20;
    uint32_t ***l_2213[2][2][3] = {{{&g_1931,&g_1931,&g_1931},{&g_1931,&g_1931,&g_1931}},{{&g_1931,&g_1931,&g_1931},{&g_1931,&g_1931,&g_1931}}};
    int32_t l_2218 = 0x25079523L;
    uint32_t l_2233 = 0x33BC691BL;
    int32_t l_2256 = (-1L);
    int32_t l_2266 = 1L;
    int32_t l_2269[5] = {0xA6002078L,0xA6002078L,0xA6002078L,0xA6002078L,0xA6002078L};
    uint8_t ****l_2302 = (void*)0;
    uint8_t *****l_2301 = &l_2302;
    uint32_t **l_2316 = &g_2128;
    uint32_t ***l_2315 = &l_2316;
    uint16_t *l_2319[6] = {&g_323,&g_323,&g_323,&g_323,&g_323,&g_323};
    uint32_t l_2325 = 0x729B3D4DL;
    int8_t **l_2342[7][6] = {{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33},{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33},{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33},{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33},{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33},{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33},{&l_33,&g_1190,&l_33,&l_33,&g_1190,&l_33}};
    int8_t ***l_2341 = &l_2342[6][4];
    int16_t *l_2343[4][1][8] = {{{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601}},{{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601}},{{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601}},{{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601}}};
    uint64_t l_2344 = 8UL;
    int32_t *l_2345 = &g_4[0];
    int32_t *l_2347 = &g_16;
    int32_t *l_2349 = &g_16;
    int32_t *l_2350 = &l_2256;
    int32_t *l_2351 = &g_4[9];
    int32_t *l_2352 = &g_16;
    int32_t *l_2353 = &g_4[3];
    int32_t *l_2354 = (void*)0;
    int32_t *l_2355 = &l_2269[3];
    int32_t *l_2356 = &l_2269[3];
    int32_t *l_2357 = &g_113;
    int32_t *l_2358[8][1] = {{(void*)0},{&l_2256},{(void*)0},{(void*)0},{&l_2256},{(void*)0},{(void*)0},{&l_2256}};
    uint32_t l_2359 = 0xEC235C9CL;
    int i, j, k;
    for (p_3 = 9; (p_3 >= 0); p_3 -= 1)
    { /* block id: 4 */
        int32_t *l_5 = &g_4[9];
        float *l_9 = (void*)0;
        float *l_10 = &g_11;
        float *l_12 = &g_13[4][1];
        uint64_t l_69 = 0x8F1E0EF155D26181LL;
        const uint32_t ***l_2210 = (void*)0;
        int32_t **l_2237 = &g_391;
        int32_t ***l_2236[1][1];
        float l_2264[9] = {(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1),(-0x2.Bp-1)};
        int16_t l_2268[1][1][7] = {{{0x90F0L,0x90F0L,0x90F0L,0x90F0L,0x90F0L,0x90F0L,0x90F0L}}};
        int32_t l_2274 = 0x6FD3F7A2L;
        uint64_t l_2284 = 0xC6337962B1F8A0E3LL;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_2236[i][j] = &l_2237;
        }
        l_6[5][3]++;
        (*l_12) = ((*l_10) = g_4[p_3]);
        for (g_16 = (-24); (g_16 < 24); g_16++)
        { /* block id: 10 */
            uint32_t l_34[4] = {1UL,1UL,1UL,1UL};
            uint16_t *l_35 = (void*)0;
            uint16_t *l_36 = &g_37;
            float *l_70 = &g_13[6][0];
            int32_t l_2203[9] = {0xC72A8FA4L,(-1L),0xC72A8FA4L,0xC72A8FA4L,(-1L),0xC72A8FA4L,0xC72A8FA4L,(-1L),0xC72A8FA4L};
            int32_t l_2235[8];
            int32_t **l_2306[6] = {&g_391,&g_391,&g_391,&g_391,&g_391,&g_391};
            int i;
            for (i = 0; i < 8; i++)
                l_2235[i] = 0x3E58A12FL;
            if ((p_3 , ((((((g_21 ^= ((void*)0 == g_19)) > func_22(func_28(l_33, (((((*l_36) = l_34[3]) <= func_38(((g_4[9] ^ (safe_div_func_uint8_t_u_u(((*l_5) == (safe_sub_func_int16_t_s_s(p_3, (p_3 < 4294967295UL)))), 0x1EL))) , l_34[3]), g_4[9], l_6[5][3])) , 0x035D6DF81F9D3F58LL) , 0x9ADBL), l_6[3][0], &g_20), g_4[5], l_69, g_4[9], l_70)) > p_3) , p_3) & l_6[5][3]) , p_3)))
            { /* block id: 993 */
                uint32_t ***l_2211 = &g_1931;
                uint32_t ****l_2212[10][2] = {{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211},{&l_2211,&l_2211}};
                int32_t l_2232 = (-6L);
                int32_t l_2234 = 1L;
                int i, j;
                l_2203[0] = p_3;
                l_2235[6] ^= (safe_sub_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u((l_2210 != ((*g_2128) , (l_2213[0][1][0] = l_2211))), p_3)), (safe_sub_func_uint64_t_u_u((safe_sub_func_int8_t_s_s((((l_2218 = (*g_1932)) | (((safe_mod_func_uint16_t_u_u(((*l_36)++), ((safe_mul_func_int16_t_s_s(l_34[1], (((safe_div_func_uint32_t_u_u((*l_5), (((safe_lshift_func_uint16_t_u_u((+p_3), (safe_mul_func_uint8_t_u_u((0x0C65E12BL | p_3), (*g_1229))))) & 0xF202E73300B0314DLL) , l_2232))) < 0x58D63BD277505CF3LL) <= (*l_5)))) || l_2233))) <= p_3) || l_6[5][3])) , p_3), l_2203[7])), (*g_909))))) || (-3L)), l_2234));
            }
            else
            { /* block id: 999 */
                (*l_10) = ((void*)0 == l_2236[0][0]);
            }
            for (l_69 = 0; (l_69 <= 56); l_69 = safe_add_func_int16_t_s_s(l_69, 3))
            { /* block id: 1004 */
                int64_t *l_2248 = &g_93[1];
                int32_t l_2255 = (-6L);
                int32_t l_2267 = 1L;
                int32_t l_2270 = 0x8F255842L;
                int32_t l_2273 = 0xFF6CA641L;
                int32_t l_2275 = 0x14162AAEL;
                int32_t l_2276[4][7] = {{5L,0xCDAE124FL,0L,0xCDAE124FL,5L,5L,0xCDAE124FL},{0x985595EEL,0x97E79E6FL,0x985595EEL,1L,1L,0x985595EEL,0x97E79E6FL},{0xCDAE124FL,0x2566770FL,0L,0L,0x2566770FL,0xCDAE124FL,0x2566770FL},{0x985595EEL,1L,1L,0x985595EEL,0x97E79E6FL,0x985595EEL,1L}};
                int i, j;
                if (((*l_5) & ((((*l_2248) = (~((+((!(safe_sub_func_float_f_f((l_2218 = ((*l_10) = ((*l_12) = p_3))), (!p_3)))) != p_3)) , p_3))) | ((void*)0 == &g_383)) < ((safe_rshift_func_uint16_t_u_s((l_2256 = ((3UL != (((safe_add_func_float_f_f((safe_sub_func_float_f_f(0x1.16E16Ep+93, l_2255)), (-0x1.8p-1))) , g_1732.f1) || p_3)) && 0L)), 9)) == l_2255))))
                { /* block id: 1010 */
                    uint8_t l_2257 = 255UL;
                    int32_t l_2262 = 0xCFA5AEBEL;
                    int32_t l_2263 = (-4L);
                    int32_t l_2265 = 3L;
                    int32_t l_2271 = 1L;
                    int32_t l_2272[6] = {(-1L),0x093644EEL,(-1L),(-1L),0x093644EEL,(-1L)};
                    int i;
                    for (g_84 = 0; (g_84 <= 1); g_84 += 1)
                    { /* block id: 1013 */
                        float l_2260 = 0x9.182EEBp+7;
                        int32_t l_2261[1][10] = {{2L,2L,2L,2L,2L,2L,2L,2L,2L,2L}};
                        int i, j;
                        (*g_1305) = &l_2255;
                        ++l_2257;
                        g_2277++;
                    }
                    for (g_227 = 5; (g_227 > (-30)); --g_227)
                    { /* block id: 1020 */
                        int8_t l_2282 = 0x42L;
                        int32_t l_2283 = 1L;
                        (*l_2237) = &l_2271;
                        ++l_2284;
                    }
                    return l_2272[5];
                }
                else
                { /* block id: 1025 */
                    union U1 *l_2287 = &g_684[0][1];
                    g_2288[4] = l_2287;
                }
                for (g_427.f0 = (-20); (g_427.f0 > 48); ++g_427.f0)
                { /* block id: 1030 */
                    uint64_t l_2291 = 0xAF29AE81E2D41F60LL;
                    if (l_2291)
                        break;
                    for (l_2266 = 0; (l_2266 >= 18); ++l_2266)
                    { /* block id: 1034 */
                        if (p_3)
                            break;
                        return l_2291;
                    }
                    if (l_2291)
                        continue;
                    for (g_1497 = 6; (g_1497 != 9); g_1497 = safe_add_func_int32_t_s_s(g_1497, 1))
                    { /* block id: 1041 */
                        (*l_10) = l_2291;
                    }
                    if (g_1732.f1)
                        goto lbl_2348;
                }
            }
            (*l_5) = (!(l_2266 , (**g_908)));
            (*l_2237) = ((((safe_mul_func_int16_t_s_s(((l_2301 == g_2182[1]) | (!(safe_rshift_func_uint16_t_u_s((l_2306[3] == l_2306[3]), 14)))), (-5L))) > (safe_lshift_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_u(l_2266, 5)) > (((((safe_mod_func_uint64_t_u_u(((((*g_1932) = ((((p_3 | 0xE88A2E1EL) | (*l_5)) > p_3) ^ (*g_1932))) || (*g_1932)) > (**g_1903)), l_2218)) & l_6[6][4]) & p_3) | l_2233) && 0xD01DCC2C64FA1B56LL)), 10))) <= p_3) , &l_2218);
        }
        if ((**g_1305))
            break;
    }
    l_2269[3] ^= ((((safe_rshift_func_uint8_t_u_s((p_3 ^ (((*g_1229) <= ((l_2315 != (void*)0) <= ((((g_2320--) > (p_3 == (g_228[4] > (~g_428[0][2].f0)))) , ((g_2324 , (((((void*)0 == &l_2319[2]) | 248UL) || (-6L)) || 9UL)) >= p_3)) || p_3))) >= p_3)), l_2233)) & l_2325) , 0xE405L) , 0L);
lbl_2348:
    (*l_2347) = (((*l_2345) |= (safe_sub_func_uint64_t_u_u((((*g_1932) = (p_3 , (safe_div_func_int8_t_s_s(((*g_19) = l_2325), (safe_lshift_func_int8_t_s_u(((l_2269[3] = ((-1L) > ((safe_add_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u((p_3 >= ((safe_mul_func_int16_t_s_s((g_601 &= ((g_2338[0] != &l_2316) , (l_2344 = (l_2266 = ((((l_2256 &= (0xBD0BL != l_6[5][3])) , ((*l_2341) = (void*)0)) == (void*)0) == p_3))))), 0x809AL)) , p_3)), p_3)), 255UL)) <= l_2218))) != l_2325), 0)))))) < (-5L)), p_3))) && (*g_1932));
    ++l_2359;
    return (*l_2345);
}


/* ------------------------------------------ */
/* 
 * reads : g_84 g_4 g_20 g_16 g_93 g_143 g_113 g_37 g_149 g_165 g_148 g_194 g_228 g_190 g_276 g_227 g_325 g_324 g_13 g_383 g_361.f1 g_391 g_408 g_370 g_425 g_455 g_431.f1 g_470 g_499 g_432.f1 g_432.f0 g_515 g_548 g_427.f1 g_552 g_573 g_428.f0 g_601 g_430.f0 g_1304 g_1190 g_361.f0 g_684.f4 g_433.f0 g_1227 g_1228 g_1229 g_1230 g_657 g_681.f0 g_682.f1 g_1497 g_656 g_427.f0 g_1523 g_1536 g_428.f1 g_1257 g_1378 g_1379 g_1160 g_1522 g_1732.f1 g_1745 g_1609.f0 g_1305 g_1776 g_323 g_1796 g_1902 g_1818 g_1903 g_2015 g_1050 g_1932
 * writes: g_20 g_143 g_37 g_148 g_149 g_93 g_165 g_4 g_190 g_113 g_227 g_228 g_276 g_196 g_13 g_11 g_323 g_324 g_325 g_370 g_383 g_391 g_408 g_424 g_426 g_455 g_499 g_515 g_548 g_601 g_430.f0 g_1257 g_682.f1 g_361.f0 g_433.f0 g_681.f0 g_1230 g_1522 g_1160 g_1536 g_1818 g_1931 g_2015 g_1050 g_2021 g_1828 g_2128
 */
static uint32_t  func_22(int8_t * p_23, uint16_t  p_24, uint8_t  p_25, const int64_t  p_26, float * p_27)
{ /* block id: 23 */
    uint8_t *l_546 = (void*)0;
    uint8_t *l_547[3][3][5] = {{{&g_548,&g_548,&g_548,&g_548,(void*)0},{&g_548,&g_548,&g_548,&g_548,&g_548},{&g_548,&g_548,&g_548,&g_548,&g_548}},{{(void*)0,&g_548,&g_548,&g_548,&g_548},{&g_548,&g_548,&g_548,&g_548,&g_548},{&g_548,&g_548,&g_548,&g_548,&g_548}},{{&g_548,&g_548,&g_548,&g_548,&g_548},{(void*)0,(void*)0,&g_548,&g_548,&g_548},{&g_548,&g_548,&g_548,&g_548,&g_548}}};
    int32_t l_549 = 0x59AF3303L;
    int32_t l_1520 = 0xA89125C9L;
    float * const l_1521 = &g_1522;
    struct S0 *l_2030 = &g_2031[2];
    int64_t *** const ** const l_2069 = (void*)0;
    uint64_t l_2070 = 0x0A93996EF0398367LL;
    float l_2078 = 0x1.68F855p+85;
    int64_t l_2079 = 6L;
    int32_t l_2080 = (-2L);
    int32_t l_2081 = 0xA389647EL;
    int32_t l_2082 = 0xDF0D848CL;
    int32_t l_2083 = 0x829B9F5AL;
    uint32_t l_2084 = 0x8AEA66F3L;
    uint32_t **l_2200 = &g_2128;
    int32_t l_2201[7] = {0xC7AF0A2BL,0xC7AF0A2BL,0x388DE0F6L,0xC7AF0A2BL,0xC7AF0A2BL,0x388DE0F6L,0xC7AF0A2BL};
    int32_t *l_2202 = &l_1520;
    int i, j, k;
    if (((((func_71(((l_549 = (func_77(((safe_sub_func_int64_t_s_s((g_84 , (p_26 , (-8L))), (safe_div_func_uint64_t_u_u((((func_87(g_4[8], (*p_23)) , 0xFDD34F56F5195AA8LL) || p_25) ^ ((safe_mul_func_uint8_t_u_u((++g_548), ((g_427.f1 <= 1L) || p_25))) < l_549)), p_24)))) ^ 18446744073709551606UL), l_549, g_552[4][2], p_24) , p_25)) , &g_276[0]), l_1520, p_26, (***g_1227), l_1521) , 0xDA5CL) && p_25) && 0xAF71L) | l_1520))
    { /* block id: 902 */
        int32_t *l_2026 = &l_549;
        float *l_2057 = &g_13[4][1];
        float **l_2056 = &l_2057;
lbl_2029:
        (*l_2026) ^= 0x28A5DA0DL;
        for (g_515 = 27; (g_515 <= 32); g_515++)
        { /* block id: 906 */
            uint64_t l_2033 = 0x57094D04141EDA73LL;
            for (g_2015 = 0; (g_2015 <= 1); g_2015 += 1)
            { /* block id: 909 */
                uint32_t l_2032 = 0x03358B4DL;
                int32_t l_2061 = 0x7AA3393FL;
                if (g_1497)
                    goto lbl_2029;
                l_2030 = l_2030;
                if (l_2032)
                    continue;
                if ((p_24 != l_549))
                { /* block id: 913 */
                    uint8_t l_2060 = 255UL;
                    for (l_549 = 1; (l_549 >= 0); l_549 -= 1)
                    { /* block id: 916 */
                        uint32_t l_2058[10];
                        float *l_2059 = &g_1828;
                        int8_t *l_2064 = &g_1160;
                        int32_t l_2071[10] = {0x3C73EBF5L,1L,1L,0x3C73EBF5L,1L,1L,0x3C73EBF5L,0xD2ED0B75L,0xD2ED0B75L,1L};
                        int32_t **l_2072 = (void*)0;
                        int32_t **l_2073 = &l_2026;
                        int i;
                        for (i = 0; i < 10; i++)
                            l_2058[i] = 0xD0D489CAL;
                        if (l_2033)
                            break;
                        l_2061 = (((g_93[l_549] = ((((safe_sub_func_float_f_f(((safe_sub_func_float_f_f((safe_sub_func_float_f_f((*p_27), (safe_sub_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f(((*p_27) > (p_26 , ((safe_sub_func_float_f_f(((*l_1521) = (*p_27)), (!(safe_mul_func_float_f_f((((*l_2059) = (safe_sub_func_float_f_f((((safe_sub_func_float_f_f((((*p_23) |= (*g_1190)) , (p_26 , (+(l_2056 == &p_27)))), 0x0.7p-1)) >= l_2058[2]) > l_2058[4]), 0x5.3C27CCp+72))) == (*p_27)), (*l_2026)))))) > 0x0.9p+1))), (*p_27))), (*p_27))), (*p_27))))), 0x0.2p-1)) < (*p_27)), (*p_27))) , l_2060) && p_25) == l_2060)) , 0x8CL) < p_24);
                        l_2071[1] ^= ((((safe_add_func_float_f_f((*p_27), ((l_2064 = &g_20) != (void*)0))) , (*g_1190)) <= (((p_24 , (((safe_div_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((((p_27 != l_1521) & (l_2069 != (void*)0)) >= p_26), 0x85L)), (*p_23))) < p_26) && p_26)) <= l_2058[3]) >= l_2060)) != l_2070);
                        (*l_2073) = p_27;
                    }
                }
                else
                { /* block id: 927 */
                    int32_t **l_2074 = &g_391;
                    (*l_2074) = &l_1520;
                }
            }
        }
    }
    else
    { /* block id: 932 */
        int32_t *l_2075 = &g_4[9];
        int32_t *l_2076 = &g_4[9];
        int32_t *l_2077[6][1] = {{&l_549},{&g_4[9]},{&l_549},{&g_4[9]},{&l_549},{&g_4[9]}};
        int64_t ****l_2104 = (void*)0;
        uint8_t l_2105 = 0xC0L;
        float l_2114 = 0x7.5344A4p-86;
        float l_2130 = 0x6.06E3EFp-27;
        uint16_t **l_2194 = &g_552[4][0];
        int i, j;
        l_2084--;
        (*l_1521) = 0x0.Cp-1;
        for (p_25 = 0; (p_25 < 39); p_25++)
        { /* block id: 937 */
            uint8_t l_2096 = 0x2BL;
            uint32_t *l_2126 = &g_361.f0;
            uint32_t *l_2129[2][6][2] = {{{&g_430.f0,(void*)0},{&g_430.f0,(void*)0},{&g_430.f0,&g_430.f0},{&g_428[0][2].f0,&g_428[0][2].f0},{&g_428[0][2].f0,&g_430.f0},{&g_430.f0,(void*)0}},{{&g_430.f0,(void*)0},{&g_430.f0,&g_430.f0},{&g_428[0][2].f0,&g_428[0][2].f0},{&g_428[0][2].f0,&g_430.f0},{&g_430.f0,(void*)0},{&g_430.f0,(void*)0}}};
            uint64_t l_2131 = 0x7861EF600149B903LL;
            uint8_t ****l_2184 = (void*)0;
            uint8_t *****l_2183 = &l_2184;
            int i, j, k;
        }
    }
    (*l_2202) ^= (l_2201[0] |= (&g_228[6] == ((*l_2200) = &g_228[5])));
    return (*g_1932);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t * func_28(int8_t * p_29, uint16_t  p_30, int32_t  p_31, int8_t * p_32)
{ /* block id: 16 */
    int32_t *l_68 = &g_16;
    int32_t **l_67 = &l_68;
    for (p_31 = 23; (p_31 == 25); p_31 = safe_add_func_uint32_t_u_u(p_31, 1))
    { /* block id: 19 */
        uint64_t l_64 = 0xC66C3C348FB8AAA8LL;
        int32_t *l_66 = &g_16;
        int32_t **l_65 = &l_66;
        l_67 = (l_64 , l_65);
    }
    return p_29;
}


/* ------------------------------------------ */
/* 
 * reads : g_4
 * writes:
 */
static uint16_t  func_38(int64_t  p_39, uint32_t  p_40, uint64_t  p_41)
{ /* block id: 13 */
    int32_t *l_46 = &g_4[4];
    int32_t *l_47 = &g_4[9];
    int32_t *l_48 = &g_4[9];
    int32_t l_49 = (-1L);
    int32_t *l_50 = &l_49;
    int32_t l_51 = 0L;
    int32_t *l_52 = &l_51;
    int32_t *l_53 = &g_4[9];
    int32_t l_54 = 1L;
    int32_t *l_55 = &l_54;
    int32_t *l_56 = &l_54;
    int32_t *l_57 = &g_4[9];
    int32_t *l_58[8][5] = {{(void*)0,&l_51,&g_16,&l_49,&g_4[4]},{&l_51,&g_4[9],&g_16,&l_51,&g_16},{&g_4[4],&g_4[4],&g_4[9],(void*)0,&l_49},{&l_51,&l_49,&g_4[9],(void*)0,(void*)0},{(void*)0,&g_16,(void*)0,&l_51,&l_54},{&g_4[9],&l_49,&l_51,&l_49,&l_54},{&g_4[9],&g_4[4],&g_4[4],&g_4[9],(void*)0},{&g_16,&g_4[9],&l_51,&l_54,&l_49}};
    uint16_t l_59 = 0xD5C0L;
    int i, j;
    l_59--;
    return g_4[9];
}


/* ------------------------------------------ */
/* 
 * reads : g_1523 g_276 g_93 g_1227 g_1228 g_148 g_1536 g_499 g_428.f1 g_143 g_601 g_165 g_190 g_1257 g_1497 g_1190 g_1229 g_1230 g_1378 g_1379 g_455 g_682.f1 g_37 g_1160 g_1522 g_4 g_1732.f1 g_657 g_427.f0 g_1745 g_1609.f0 g_432.f1 g_430.f0 g_515 g_1305 g_391 g_1776 g_227 g_370 g_433.f0 g_1796 g_324 g_656 g_383 g_1902 g_113 g_1818 g_1903 g_2015 g_1050 g_408 g_323
 * writes: g_93 g_276 g_601 g_165 g_408 g_37 g_391 g_681.f0 g_682.f1 g_1230 g_143 g_1522 g_455 g_1257 g_499 g_1160 g_148 g_430.f0 g_4 g_515 g_1536 g_323 g_190 g_1818 g_113 g_1931 g_227 g_383 g_2015 g_1050 g_2021
 */
static int8_t  func_71(uint16_t * p_72, int8_t  p_73, uint32_t  p_74, uint8_t  p_75, float * const  p_76)
{ /* block id: 656 */
    int32_t **l_1528 = &g_391;
    int32_t l_1535 = 1L;
    float l_1541[4] = {(-0x1.2p+1),(-0x1.2p+1),(-0x1.2p+1),(-0x1.2p+1)};
    uint32_t l_1542 = 0x82BA8F8FL;
    int32_t l_1546 = 0x3A689CEFL;
    int32_t l_1551 = 0x8A828F2CL;
    int32_t l_1555 = 0x8834416EL;
    uint32_t l_1556 = 1UL;
    int32_t l_1584[6][10] = {{1L,0xB14BC012L,0L,0L,0xB14BC012L,1L,7L,1L,0x54AB198FL,0L},{4L,0x11AEC5D5L,0xB9272AC2L,(-1L),0L,0x81C443E9L,0x54AB198FL,(-1L),(-1L),0xFB1B82B5L},{4L,0x628627A2L,(-9L),0x9D92BEACL,1L,1L,0xFB1B82B5L,0xB14BC012L,0xFB1B82B5L,1L},{1L,0xFB1B82B5L,0xB14BC012L,0xFB1B82B5L,1L,1L,0x9D92BEACL,(-9L),0x628627A2L,4L},{0xFB1B82B5L,(-1L),(-1L),0x54AB198FL,0x81C443E9L,0L,(-1L),0xB9272AC2L,0x11AEC5D5L,4L},{0L,0x54AB198FL,1L,7L,1L,0xB14BC012L,0L,0L,0xB14BC012L,1L}};
    uint32_t l_1585 = 1UL;
    uint8_t l_1594 = 249UL;
    struct S0 *l_1598 = &g_1599;
    int64_t ***l_1623 = &g_197;
    int64_t ***l_1624 = &g_197;
    uint32_t *** const l_1640 = (void*)0;
    int64_t ** const *l_1681 = &g_197;
    int64_t ** const **l_1680 = &l_1681;
    uint64_t l_1685 = 0x4EB7AE81271522EALL;
    int32_t *l_1693 = &l_1535;
    int64_t * const **l_1695 = (void*)0;
    int64_t * const ***l_1694 = &l_1695;
    union U1 *l_1734 = &g_1735;
    union U1 **l_1733[9][7][4] = {{{&l_1734,&l_1734,&l_1734,&l_1734},{(void*)0,(void*)0,&l_1734,&l_1734},{&l_1734,(void*)0,&l_1734,(void*)0},{&l_1734,&l_1734,(void*)0,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{(void*)0,(void*)0,(void*)0,&l_1734},{&l_1734,&l_1734,(void*)0,&l_1734}},{{&l_1734,(void*)0,(void*)0,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{(void*)0,&l_1734,&l_1734,&l_1734},{&l_1734,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,(void*)0}},{{(void*)0,&l_1734,(void*)0,(void*)0},{&l_1734,(void*)0,(void*)0,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,(void*)0,(void*)0,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,(void*)0},{&l_1734,&l_1734,&l_1734,&l_1734}},{{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,(void*)0,&l_1734,(void*)0},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,(void*)0}},{{&l_1734,&l_1734,&l_1734,(void*)0},{&l_1734,&l_1734,(void*)0,&l_1734},{&l_1734,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,(void*)0,(void*)0},{&l_1734,&l_1734,(void*)0,&l_1734},{(void*)0,&l_1734,&l_1734,(void*)0},{&l_1734,&l_1734,&l_1734,&l_1734}},{{&l_1734,&l_1734,(void*)0,(void*)0},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,(void*)0,(void*)0},{(void*)0,&l_1734,(void*)0,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,(void*)0,&l_1734}},{{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,(void*)0,&l_1734},{(void*)0,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734}},{{(void*)0,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,(void*)0,&l_1734},{(void*)0,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,&l_1734}},{{&l_1734,&l_1734,&l_1734,&l_1734},{&l_1734,&l_1734,(void*)0,&l_1734},{&l_1734,(void*)0,&l_1734,&l_1734},{(void*)0,&l_1734,(void*)0,&l_1734},{(void*)0,(void*)0,&l_1734,&l_1734},{&l_1734,(void*)0,&l_1734,&l_1734},{&l_1734,&l_1734,&l_1734,(void*)0}}};
    int32_t *l_1741 = &g_1735.f0;
    uint64_t l_1757 = 0xA49D0168D2258865LL;
    int64_t *l_1789[6][9] = {{(void*)0,&g_93[0],(void*)0,&g_93[1],&g_93[1],(void*)0,&g_93[0],(void*)0,&g_93[1]},{&g_93[0],&g_93[1],&g_93[1],&g_93[0],&g_93[0],&g_93[0],&g_93[1],&g_93[1],&g_93[0]},{&g_93[0],&g_93[1],(void*)0,&g_93[1],&g_93[0],&g_93[0],&g_93[1],(void*)0,&g_93[1]},{&g_93[1],&g_93[0],&g_93[0],&g_93[0],&g_93[0],&g_93[1],&g_93[0],&g_93[0],&g_93[0]},{&g_93[0],&g_93[0],&g_93[1],(void*)0,&g_93[1],&g_93[0],&g_93[0],&g_93[1],(void*)0},{&g_93[0],&g_93[0],&g_93[0],&g_93[1],&g_93[1],&g_93[0],&g_93[0],&g_93[0],&g_93[1]}};
    struct S0 ***l_1829 = (void*)0;
    int16_t l_1852[9][8][1] = {{{0x6ED0L},{0x7BEAL},{1L},{0x545AL},{(-1L)},{1L},{0x1007L},{0x0F57L}},{{0x1723L},{0xF46BL},{0x4624L},{2L},{2L},{0x4624L},{0xF46BL},{0x1723L}},{{0x0F57L},{0x1007L},{1L},{(-1L)},{0x545AL},{1L},{0x7BEAL},{0x6ED0L}},{{0x0882L},{0x6428L},{0xCC98L},{0x6428L},{0x0882L},{0x6ED0L},{0x7BEAL},{1L}},{{0x545AL},{(-1L)},{1L},{0x1007L},{0x0F57L},{0x1723L},{0xF46BL},{0x4624L}},{{2L},{2L},{0x4624L},{0xF46BL},{0x1723L},{0x0F57L},{0x1007L},{1L}},{{(-1L)},{0x545AL},{1L},{0x7BEAL},{0x6ED0L},{0x0882L},{0x6428L},{0xCC98L}},{{0x6428L},{0x0882L},{0x6ED0L},{0x7BEAL},{1L},{0x545AL},{(-1L)},{1L}},{{0x1007L},{0x0F57L},{0x1723L},{0xF46BL},{0x4624L},{2L},{2L},{0x4624L}}};
    int32_t l_1854 = 0xB88645A8L;
    uint32_t l_1860 = 18446744073709551614UL;
    uint32_t *l_1929 = &g_995;
    uint32_t **l_1928[9] = {&l_1929,&l_1929,&l_1929,&l_1929,&l_1929,&l_1929,&l_1929,&l_1929,&l_1929};
    uint32_t **l_1933[3][1][6] = {{{&g_1932,&g_1932,&g_1932,&g_1932,&g_1932,&g_1932}},{{&l_1929,&g_1932,&l_1929,&g_1932,&l_1929,&g_1932}},{{&g_1932,&g_1932,&g_1932,&g_1932,&g_1932,&g_1932}}};
    int8_t *l_1946[1][3];
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_1946[i][j] = (void*)0;
    }
    if (p_75)
    { /* block id: 657 */
        int64_t *l_1529 = &g_93[0];
        int32_t * const *l_1534[9][8][1] = {{{(void*)0},{&g_391},{&g_391},{&g_391},{(void*)0},{(void*)0},{&g_391},{&g_391}},{{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{(void*)0},{(void*)0},{&g_391}},{{&g_391},{&g_391},{(void*)0},{(void*)0},{&g_391},{&g_391},{&g_391},{&g_391}},{{&g_391},{&g_391},{&g_391},{(void*)0},{(void*)0},{&g_391},{&g_391},{&g_391}},{{&g_391},{&g_391},{&g_391},{&g_391},{(void*)0},{&g_391},{(void*)0},{&g_391}},{{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391}},{{&g_391},{&g_391},{(void*)0},{&g_391},{(void*)0},{&g_391},{&g_391},{&g_391}},{{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391}},{{(void*)0},{&g_391},{(void*)0},{&g_391},{&g_391},{&g_391},{&g_391},{&g_391}}};
        int16_t *l_1537 = (void*)0;
        int16_t *l_1538 = &g_601;
        int16_t *l_1539 = (void*)0;
        int16_t *l_1540[5];
        float l_1603 = 0x8.Ap-1;
        union U1 *l_1608[2];
        uint32_t l_1625 = 0UL;
        int64_t l_1634 = 1L;
        uint32_t *l_1639 = &g_430.f0;
        uint32_t **l_1638 = &l_1639;
        uint32_t ***l_1637 = &l_1638;
        uint8_t **l_1647 = &g_657;
        uint8_t ***l_1646 = &l_1647;
        uint32_t l_1665 = 18446744073709551611UL;
        uint32_t l_1689 = 0x7728758DL;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_1540[i] = &g_165;
        for (i = 0; i < 2; i++)
            l_1608[i] = &g_1609;
        if ((((((65535UL != ((0x0.0p-1 <= g_1523) , (g_165 &= ((*l_1538) ^= (((safe_div_func_uint32_t_u_u(((*p_72) && (((((((safe_mod_func_uint64_t_u_u(((l_1528 != (((*l_1529) ^= 0xED7E7D22DDA40D24LL) , (((safe_lshift_func_uint16_t_u_u(((*p_72) &= ((*g_1227) == (void*)0)), (safe_mod_func_uint16_t_u_u(p_74, 65535UL)))) , p_74) , l_1534[7][5][0]))) ^ l_1535), 5UL)) , 1L) == g_148[3][1]) , 0x963ED1B9141597DFLL) & g_1536) & g_499) , g_428[0][2].f1)), 0x44EAA689L)) && (-3L)) != g_143))))) > g_190[4][2]) == 2L) > g_1257[0][0][4]) == l_1542))
        { /* block id: 662 */
            int8_t l_1545 = 0x04L;
            int32_t l_1548 = 0xCF3ACBC3L;
            int32_t l_1549 = 1L;
            int32_t l_1552 = 0x6011A3D9L;
            int32_t l_1553[7][1] = {{(-8L)},{1L},{(-8L)},{(-8L)},{1L},{(-8L)},{(-8L)}};
            union U1 **l_1610 = &l_1608[0];
            int i, j;
lbl_1561:
            l_1535 = (-5L);
            for (g_408 = 19; (g_408 > 40); g_408 = safe_add_func_int16_t_s_s(g_408, 2))
            { /* block id: 666 */
                float l_1547 = 0x4.FC3A41p-19;
                int32_t l_1550 = 1L;
                int32_t l_1554 = (-2L);
                int32_t l_1571 = 0xDA570F2FL;
                int32_t l_1572 = 0x8A733D61L;
                int32_t l_1573 = 0x64BCDB8FL;
                int32_t l_1574 = 0xE1AD3D69L;
                int32_t l_1578 = 2L;
                int32_t l_1579 = 4L;
                int32_t l_1581 = 0x0E37848EL;
                int32_t l_1583 = 0L;
                ++l_1556;
                for (g_37 = (-18); (g_37 < 56); g_37 = safe_add_func_int8_t_s_s(g_37, 1))
                { /* block id: 670 */
                    uint64_t l_1565 = 2UL;
                    int32_t l_1568 = 0x933EEC5EL;
                    int32_t l_1569 = 1L;
                    int32_t l_1570 = 0L;
                    int32_t l_1575 = 0x60FA4516L;
                    int32_t l_1576[6][8];
                    const float l_1595 = 0x1.Dp-1;
                    int32_t *l_1604 = &g_681.f0;
                    int i, j;
                    for (i = 0; i < 6; i++)
                    {
                        for (j = 0; j < 8; j++)
                            l_1576[i][j] = 0xD42EF817L;
                    }
                    if (g_143)
                        goto lbl_1561;
                    for (l_1542 = 0; (l_1542 < 51); l_1542 = safe_add_func_int32_t_s_s(l_1542, 5))
                    { /* block id: 674 */
                        int32_t *l_1564[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_1564[i] = (void*)0;
                        (*l_1528) = l_1564[3];
                        if (l_1565)
                            break;
                    }
                    for (l_1546 = (-13); (l_1546 <= 20); l_1546++)
                    { /* block id: 680 */
                        int32_t l_1577 = (-5L);
                        int32_t l_1580 = 0xAF899202L;
                        int32_t l_1582 = (-6L);
                        l_1585++;
                        return p_75;
                    }
                    if ((((*p_72) , ((*l_1604) = (safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(((safe_div_func_uint8_t_u_u((l_1594 > g_1497), (safe_sub_func_int16_t_s_s((g_601 = (((void*)0 == l_1598) , (!l_1549))), 0xC75BL)))) & (safe_unary_minus_func_int64_t_s((~(p_75 != (p_73 , p_75)))))), 13)), p_74)))) , p_75))
                    { /* block id: 686 */
                        if (p_74)
                            break;
                        l_1569 |= l_1545;
                        if (p_74)
                            break;
                    }
                    else
                    { /* block id: 690 */
                        uint8_t l_1605 = 0x82L;
                        l_1605--;
                        return l_1550;
                    }
                }
            }
            (*l_1610) = (((*p_72) || (*p_72)) , l_1608[0]);
        }
        else
        { /* block id: 697 */
            int64_t ***l_1621[5][9];
            int64_t ****l_1622[4] = {&l_1621[4][2],&l_1621[4][2],&l_1621[4][2],&l_1621[4][2]};
            int32_t l_1626[2][7] = {{1L,1L,1L,1L,1L,1L,1L},{0x3B6AE2F4L,0x3B6AE2F4L,0x3B6AE2F4L,0x3B6AE2F4L,0x3B6AE2F4L,0x3B6AE2F4L,0x3B6AE2F4L}};
            int32_t l_1657 = 0x63D394D8L;
            uint16_t *l_1679 = &g_499;
            int64_t ** const ***l_1682 = &l_1680;
            int64_t l_1690 = (-9L);
            int64_t * const ****l_1696[6];
            int i, j;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 9; j++)
                    l_1621[i][j] = &g_197;
            }
            for (i = 0; i < 6; i++)
                l_1696[i] = &l_1694;
            if (((safe_div_func_int32_t_s_s(p_74, (p_73 , ((safe_sub_func_int32_t_s_s(p_75, ((((safe_mul_func_int8_t_s_s((safe_div_func_uint8_t_u_u(p_75, ((***g_1227) |= ((((*g_1190) = 0x0CL) <= 0xA4L) | (safe_div_func_int32_t_s_s(((l_1623 = l_1621[4][5]) == l_1624), (l_1626[1][2] = (((*p_72) <= 0x28A4L) & l_1625)))))))), 0x96L)) <= p_75) & p_75) != p_75))) | g_148[3][1])))) | g_190[4][2]))
            { /* block id: 702 */
                uint32_t l_1649 = 0x0110E434L;
                int32_t l_1650 = 0x74DDFB79L;
                int32_t l_1660 = 0x142D6E66L;
                int32_t l_1661 = 0xC504AC37L;
                int32_t l_1662 = 0x0C2F02E4L;
                int32_t l_1663 = 0x56D33B3DL;
                int32_t l_1664 = 0xC4514345L;
                for (g_1230 = 8; (g_1230 <= 7); g_1230 = safe_sub_func_uint32_t_u_u(g_1230, 5))
                { /* block id: 705 */
                    for (g_143 = (-26); (g_143 > 45); g_143 = safe_add_func_int64_t_s_s(g_143, 4))
                    { /* block id: 708 */
                        uint32_t l_1631 = 1UL;
                        l_1631++;
                        (*p_76) = 0x9.4p-1;
                        return l_1634;
                    }
                }
                if (((((safe_rshift_func_int16_t_s_s((l_1650 = (p_74 || ((((l_1637 != l_1640) || ((***g_1378) < 0xC5L)) | p_74) , (safe_lshift_func_int8_t_s_u(((((safe_add_func_uint16_t_u_u((safe_unary_minus_func_uint16_t_u(((&g_1379 == l_1646) || (safe_unary_minus_func_uint64_t_u(18446744073709551615UL))))), 65533UL)) , (-4L)) , (-1L)) , l_1649), 2))))), 10)) != (-7L)) <= (-1L)) == p_75))
                { /* block id: 715 */
                    int32_t *l_1653 = &l_1551;
                    uint32_t *l_1658 = &g_1257[0][0][5];
                    int32_t l_1659[7] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
                    int i;
                    for (g_455 = 1; (g_455 == 20); g_455 = safe_add_func_uint8_t_u_u(g_455, 2))
                    { /* block id: 718 */
                        int32_t **l_1654 = &l_1653;
                        if (l_1584[3][9])
                            break;
                        (*l_1654) = ((*l_1528) = l_1653);
                        if ((*l_1653))
                            continue;
                    }
                    l_1659[1] = ((*l_1653) = ((safe_add_func_int32_t_s_s(l_1657, ((*l_1658) &= (p_74 > (*l_1653))))) && p_75));
                }
                else
                { /* block id: 727 */
                    --l_1665;
                }
            }
            else
            { /* block id: 730 */
                uint32_t l_1668 = 18446744073709551613UL;
                return l_1668;
            }
            l_1546 |= (safe_rshift_func_uint8_t_u_s((safe_div_func_int64_t_s_s((safe_add_func_int32_t_s_s(((((((safe_rshift_func_uint16_t_u_s((safe_sub_func_int16_t_s_s(0xFB21L, ((*l_1679) |= (*p_72)))), (&l_1621[4][5] == ((*l_1682) = l_1680)))) , (l_1535 |= ((((safe_add_func_uint64_t_u_u(l_1685, (p_75 | ((+(safe_sub_func_int32_t_s_s(l_1689, ((((*g_1190) <= 0x33L) || p_73) , g_37)))) > p_75)))) , l_1690) , 0xED836567L) , 0L))) || 6UL) , p_73) >= p_73) , p_75), 0xA1884925L)), 0x7B2F37F9525BFB07LL)), p_75));
            for (g_1160 = 7; (g_1160 > (-5)); g_1160--)
            { /* block id: 739 */
                (*l_1528) = l_1693;
            }
            l_1694 = l_1694;
        }
    }
    else
    { /* block id: 744 */
        const uint64_t l_1711[5][8][4] = {{{6UL,18446744073709551615UL,1UL,0xA679642FC2AD2D59LL},{18446744073709551615UL,0x74583632FFC0D3A2LL,0x74583632FFC0D3A2LL,18446744073709551615UL},{0x9A30C71F5A9F649FLL,7UL,18446744073709551615UL,1UL},{0x019F1C9B07CCCFEFLL,4UL,0x66B4680436C07260LL,0x1AC28EAA6A30C130LL},{0x75BEA4E5F7671303LL,18446744073709551614UL,1UL,0x1AC28EAA6A30C130LL},{0x74583632FFC0D3A2LL,4UL,0x8C59ED3B40DAC06ALL,1UL},{0xB7C9D50083B37F51LL,7UL,1UL,18446744073709551615UL},{18446744073709551615UL,0x74583632FFC0D3A2LL,0UL,0xA679642FC2AD2D59LL}},{{0x8C59ED3B40DAC06ALL,18446744073709551615UL,18446744073709551614UL,0x9A30C71F5A9F649FLL},{0xB7C9D50083B37F51LL,0xA679642FC2AD2D59LL,0UL,7UL},{0x71081C41C5DDAB6BLL,1UL,1UL,18446744073709551615UL},{1UL,0x019F1C9B07CCCFEFLL,1UL,0x99B7D5A047F0997ELL},{0x019F1C9B07CCCFEFLL,0xA679642FC2AD2D59LL,0x4229574A1DD9E8A0LL,6UL},{6UL,0x8C59ED3B40DAC06ALL,0x74583632FFC0D3A2LL,0xA679642FC2AD2D59LL},{0xEC11B565C02B7393LL,0x71081C41C5DDAB6BLL,0x74583632FFC0D3A2LL,0xEC11B565C02B7393LL},{6UL,7UL,0x4229574A1DD9E8A0LL,0x75BEA4E5F7671303LL}},{{0x019F1C9B07CCCFEFLL,0xB96BBC4424074E52LL,1UL,0x1AC28EAA6A30C130LL},{1UL,0x1AC28EAA6A30C130LL,1UL,18446744073709551614UL},{0x71081C41C5DDAB6BLL,4UL,0UL,0x75BEA4E5F7671303LL},{0xB7C9D50083B37F51LL,0x99B7D5A047F0997ELL,18446744073709551614UL,18446744073709551615UL},{0x8C59ED3B40DAC06ALL,0x71081C41C5DDAB6BLL,0UL,0UL},{18446744073709551615UL,18446744073709551615UL,1UL,6UL},{0xB7C9D50083B37F51LL,0UL,0x8C59ED3B40DAC06ALL,7UL},{0x74583632FFC0D3A2LL,0x019F1C9B07CCCFEFLL,1UL,0x8C59ED3B40DAC06ALL}},{{0x75BEA4E5F7671303LL,0x019F1C9B07CCCFEFLL,0x66B4680436C07260LL,7UL},{0x019F1C9B07CCCFEFLL,0UL,18446744073709551615UL,6UL},{0x9A30C71F5A9F649FLL,18446744073709551615UL,0x74583632FFC0D3A2LL,0UL},{18446744073709551615UL,0x71081C41C5DDAB6BLL,1UL,18446744073709551615UL},{6UL,0x99B7D5A047F0997ELL,18446744073709551615UL,0x75BEA4E5F7671303LL},{1UL,18446744073709551615UL,0UL,18446744073709551609UL},{0UL,1UL,0xB7C9D50083B37F51LL,1UL},{0xEC11B565C02B7393LL,0x4229574A1DD9E8A0LL,1UL,0UL}},{{0x66B4680436C07260LL,0xB96BBC4424074E52LL,18446744073709551609UL,0UL},{18446744073709551614UL,0xEC11B565C02B7393LL,0x71081C41C5DDAB6BLL,0x74583632FFC0D3A2LL},{18446744073709551614UL,1UL,18446744073709551609UL,0x99B7D5A047F0997ELL},{0x66B4680436C07260LL,0x74583632FFC0D3A2LL,1UL,6UL},{0xEC11B565C02B7393LL,0x9A30C71F5A9F649FLL,0xB7C9D50083B37F51LL,18446744073709551614UL},{0UL,1UL,0UL,0xB96BBC4424074E52LL},{1UL,0x74583632FFC0D3A2LL,0x9D7D74157914EE02LL,0xCC2032C7929D515FLL},{0x99B7D5A047F0997ELL,18446744073709551614UL,18446744073709551615UL,0x74583632FFC0D3A2LL}}};
        const uint64_t l_1714 = 0xAB0E1F1DA507444CLL;
        union U1 ***l_1752 = &l_1733[3][6][3];
        int32_t l_1779 = 0x81274C66L;
        int32_t l_1780 = 0xB0915111L;
        uint32_t *l_1794 = (void*)0;
        int32_t l_1826[10][7][3] = {{{0x8225C46CL,(-6L),0xE63D0642L},{8L,0x6D6324B0L,0x6E575E55L},{0xB5514019L,(-1L),0x2AA08B6BL},{1L,0x6D6324B0L,0xF07CB3C6L},{0L,(-6L),0x159C2E97L},{4L,1L,0x5F53E6FDL},{0xE5BD498AL,0xF4551685L,0xF1F96E69L}},{{4L,0x6E575E55L,1L},{0L,0xF1F96E69L,0x68FCDBC8L},{1L,(-10L),(-10L)},{0xB5514019L,0xE63D0642L,0x68FCDBC8L},{8L,(-1L),1L},{0x8225C46CL,0xA1870807L,0xF1F96E69L},{0xB6F0CACDL,0xA3CCAF13L,0x5F53E6FDL}},{{0x0CFAA8C0L,0xA1870807L,0x159C2E97L},{(-10L),(-1L),0xF07CB3C6L},{0xB7A70F11L,0xE63D0642L,0x2AA08B6BL},{2L,(-10L),0x6E575E55L},{0xB7A70F11L,0xF1F96E69L,0xE63D0642L},{(-10L),0x6E575E55L,(-1L)},{0x0CFAA8C0L,0xF4551685L,0xF4551685L}},{{0xB6F0CACDL,1L,(-1L)},{0x8225C46CL,(-6L),0xE63D0642L},{8L,0x6D6324B0L,0x6E575E55L},{0xB5514019L,(-1L),0x2AA08B6BL},{1L,0x6D6324B0L,0xF07CB3C6L},{0L,(-6L),0x159C2E97L},{4L,1L,0x5F53E6FDL}},{{0xE5BD498AL,0xF4551685L,0xF1F96E69L},{4L,0x6E575E55L,1L},{0L,0xF1F96E69L,0x68FCDBC8L},{1L,(-10L),(-10L)},{0xB5514019L,0xE63D0642L,0x68FCDBC8L},{8L,(-1L),1L},{0x8225C46CL,0xA1870807L,0xF1F96E69L}},{{0xB6F0CACDL,0xA3CCAF13L,0x5F53E6FDL},{0x0CFAA8C0L,0xA1870807L,0x159C2E97L},{(-10L),(-1L),0xF07CB3C6L},{0xB7A70F11L,0xE63D0642L,0x2AA08B6BL},{2L,(-10L),0x6E575E55L},{0xB7A70F11L,0xF1F96E69L,0xE63D0642L},{(-10L),0x6E575E55L,(-1L)}},{{0x0CFAA8C0L,0xF4551685L,0xF4551685L},{0xB6F0CACDL,1L,(-1L)},{0x8225C46CL,(-6L),0xE63D0642L},{8L,0x6D6324B0L,0x6E575E55L},{0xB5514019L,(-1L),0x2AA08B6BL},{1L,0x6D6324B0L,0xF07CB3C6L},{0L,(-6L),0x159C2E97L}},{{4L,1L,(-1L)},{1L,0xD95FF8FAL,0x92C7B17DL},{0xF07CB3C6L,4L,1L},{(-1L),0x92C7B17DL,0xE5159C68L},{(-10L),1L,1L},{0xA1870807L,(-7L),0xE5159C68L},{0x5F53E6FDL,0x7C4A9FC3L,1L}},{{0x2AA08B6BL,(-9L),0x92C7B17DL},{0x6D6324B0L,(-6L),(-1L)},{0xF4551685L,(-9L),0x3A188EB2L},{0xA3CCAF13L,0x7C4A9FC3L,0x8F5F9574L},{0x159C2E97L,(-7L),0xD2D53D1CL},{(-7L),1L,4L},{0x159C2E97L,0x92C7B17DL,(-7L)}},{{0xA3CCAF13L,4L,0xE9332E87L},{0xF4551685L,0xD95FF8FAL,0xD95FF8FAL},{0x6D6324B0L,1L,0xE9332E87L},{0x2AA08B6BL,8L,(-7L)},{0x5F53E6FDL,0L,4L},{0xA1870807L,0x16E605A6L,0xD2D53D1CL},{(-10L),0L,0x8F5F9574L}}};
        int32_t l_1848 = (-1L);
        union U1 *l_1879 = &g_1880;
        int8_t l_1970 = 1L;
        int32_t *l_2025[2][9][2] = {{{&l_1848,&l_1848},{(void*)0,&l_1826[1][0][2]},{&g_4[7],&l_1584[4][1]},{&l_1826[7][6][2],(void*)0},{&l_1826[7][6][2],&l_1826[7][6][2]},{&g_4[9],(void*)0},{&g_4[9],&l_1826[7][6][2]},{&l_1826[7][6][2],(void*)0},{&l_1826[7][6][2],&l_1584[4][1]}},{{&g_4[7],&l_1826[1][0][2]},{(void*)0,&l_1848},{&l_1848,&l_1826[7][6][2]},{(void*)0,&l_1826[7][6][2]},{&l_1848,&l_1848},{(void*)0,&l_1826[1][0][2]},{&g_4[7],&l_1584[4][1]},{&l_1826[7][6][2],(void*)0},{&l_1826[7][6][2],&l_1826[7][6][2]}}};
        int i, j, k;
        if ((((safe_sub_func_uint8_t_u_u((((*g_1190) &= ((((((((((((safe_div_func_float_f_f(((*p_76) = (safe_add_func_float_f_f((safe_sub_func_float_f_f(((((safe_lshift_func_uint16_t_u_u(0UL, ((*p_72) = (1L | 0UL)))) , (safe_add_func_float_f_f((safe_mul_func_float_f_f((*p_76), 0x3.86436Ep+40)), (*p_76)))) > l_1711[1][2][0]) <= ((safe_div_func_float_f_f((((*p_76) == (-0x4.0p+1)) != 0x8.574813p+45), 0x0.Ep+1)) < (*l_1693))), 0xE.793786p+5)), (-0x1.Bp+1)))), 0x1.7p-1)) , p_74) != 0xCA567AF2L) , l_1598) == (void*)0) >= p_75) || 0UL) == p_73) , p_73) <= 0x19L) >= l_1711[4][4][1]) <= p_75)) | 0x74L), p_74)) | g_4[9]) <= l_1714))
        { /* block id: 748 */
            uint32_t l_1763 = 5UL;
            uint64_t *l_1772 = &g_190[4][2];
            int32_t l_1778 = 6L;
            uint8_t l_1805 = 0x13L;
            uint8_t **l_1807 = &g_657;
            uint8_t ***l_1806 = &l_1807;
            uint32_t * const l_1808 = &g_228[4];
            int32_t **l_1809 = &l_1693;
lbl_1810:
            for (g_1230 = 0; (g_1230 <= 5); g_1230 += 1)
            { /* block id: 751 */
                union U1 *l_1731 = &g_1732;
                union U1 **l_1730 = &l_1731;
                union U1 ***l_1729 = &l_1730;
                union U1 ***l_1736 = &l_1733[3][6][3];
                int32_t *l_1742[7][8][4] = {{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}},{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}},{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}},{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}},{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}},{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}},{{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227},{&g_227,&g_227,&g_227,&g_227}}};
                int16_t *l_1743 = &g_601;
                int16_t *l_1744 = &g_165;
                int32_t ***l_1774[5] = {&l_1528,&l_1528,&l_1528,&l_1528,&l_1528};
                int32_t ****l_1773 = &l_1774[4];
                int i, j, k;
                if ((g_4[(g_1230 + 2)] , (safe_lshift_func_uint16_t_u_s(((((*l_1744) = ((*l_1743) = (safe_sub_func_uint8_t_u_u((+((((((*g_657) &= ((safe_sub_func_int32_t_s_s(p_75, (p_75 <= 0x58L))) , (safe_div_func_uint64_t_u_u((!(safe_div_func_int32_t_s_s(((safe_rshift_func_int16_t_s_u((((((*l_1729) = (void*)0) != ((*l_1736) = l_1733[3][6][3])) < ((safe_lshift_func_int8_t_s_s((g_4[(g_1230 + 2)] ^ ((safe_div_func_int16_t_s_s(((l_1742[2][3][2] = l_1741) == (void*)0), g_1732.f1)) , p_74)), 2)) == 0xDDA965819270686ELL)) >= p_74), 4)) >= 0xE5A6DA14L), p_74))), 1L)))) , 8UL) & 0x9325BD5EL) && l_1714) | 1UL)), 0L)))) || (*p_72)) <= g_427.f0), p_75))))
                { /* block id: 758 */
                    int16_t l_1761 = 0L;
                    for (g_408 = 0; g_408 < 9; g_408 += 1)
                    {
                        for (p_73 = 0; p_73 < 7; p_73 += 1)
                        {
                            for (l_1535 = 0; l_1535 < 4; l_1535 += 1)
                            {
                                l_1733[g_408][p_73][l_1535] = &l_1734;
                            }
                        }
                    }
                    if (p_75)
                    { /* block id: 760 */
                        return g_1745[5][1];
                    }
                    else
                    { /* block id: 762 */
                        int8_t l_1760 = 0xCAL;
                        (*l_1528) = &g_4[9];
                        if (p_74)
                            continue;
                        l_1761 = (((safe_mul_func_float_f_f((safe_add_func_float_f_f((safe_mul_func_float_f_f((l_1752 == ((*l_1693) , (void*)0)), (((((18446744073709551615UL >= p_73) == (safe_div_func_int32_t_s_s((safe_mod_func_int64_t_s_s(l_1757, (safe_sub_func_int32_t_s_s((((g_1609.f0 , (p_73 != 0xE137L)) <= g_4[(g_1230 + 2)]) || 0x821CL), 0xC0672540L)))), g_432[0][1].f1))) , &l_1733[8][5][0]) != (void*)0) > (*p_76)))), l_1711[2][0][2])), 0x0.Cp-1)) < 0x2.Bp-1) == l_1760);
                    }
                    for (g_430.f0 = 0; (g_430.f0 <= 5); g_430.f0 += 1)
                    { /* block id: 769 */
                        int i, j;
                        l_1584[g_1230][(g_1230 + 2)] = (g_4[(g_430.f0 + 4)] = 0L);
                    }
                    for (g_143 = 0; (g_143 <= 0); g_143 += 1)
                    { /* block id: 775 */
                        int i, j;
                        l_1584[(g_143 + 1)][(g_1230 + 1)] ^= g_1745[(g_143 + 6)][(g_143 + 1)];
                        if (l_1584[(g_143 + 5)][(g_143 + 7)])
                            break;
                        l_1763 ^= (safe_unary_minus_func_uint16_t_u(g_4[(g_1230 + 2)]));
                    }
                }
                else
                { /* block id: 780 */
                    return p_73;
                }
                for (g_515 = 1; (g_515 <= 6); g_515 += 1)
                { /* block id: 785 */
                    const int32_t **l_1775 = (void*)0;
                    int i, j;
                    l_1584[g_1230][(g_515 + 3)] ^= l_1711[4][3][0];
                    for (g_1536 = 1; (g_1536 <= 6); g_1536 += 1)
                    { /* block id: 789 */
                        if ((**g_1305))
                            break;
                    }
                    l_1780 |= ((l_1779 = (((safe_lshift_func_int8_t_s_s((((g_148[(g_1230 + 1)][(g_515 + 2)]++) || ((safe_mul_func_uint16_t_u_u(((((g_4[(g_1230 + 2)] < p_74) == ((*g_1190) = ((((p_73 , ((l_1778 ^= ((l_1584[g_1230][g_515] = (((((l_1772 == &g_190[4][2]) != (*p_72)) , (void*)0) == (void*)0) < ((((void*)0 != l_1773) , l_1775) != g_1776))) ^ g_227)) == 1L)) >= (-1L)) == (*g_1190)) , 0x4BL))) & p_73) <= p_75), g_148[(g_1230 + 1)][(g_515 + 2)])) < 0x1A34CF07L)) < (**l_1528)), 5)) , p_73) == g_370[1][2][4])) && p_75);
                }
            }
            for (g_323 = 0; (g_323 > 20); ++g_323)
            { /* block id: 802 */
                const int32_t l_1795[7][4][2] = {{{0x7F1C004BL,1L},{1L,0x586F6A6DL},{1L,1L},{0x7F1C004BL,1L}},{{1L,0x7F1C004BL},{1L,1L},{0x7F1C004BL,1L},{1L,0x586F6A6DL}},{{1L,1L},{0x7F1C004BL,1L},{1L,0x7F1C004BL},{1L,1L}},{{0x7F1C004BL,1L},{1L,0x586F6A6DL},{1L,1L},{0x7F1C004BL,1L}},{{1L,0x7F1C004BL},{1L,1L},{0x7F1C004BL,1L},{1L,0x586F6A6DL}},{{1L,1L},{0x7F1C004BL,1L},{1L,0x7F1C004BL},{1L,1L}},{{0x7F1C004BL,1L},{1L,0x586F6A6DL},{1L,1L},{0x908FE69CL,0x586F6A6DL}}};
                int i, j, k;
                (*l_1693) = (((p_74 <= ((*g_1190) ^= (((4294967290UL <= (l_1778 = ((safe_add_func_uint32_t_u_u(1UL, (((safe_div_func_uint64_t_u_u(((safe_add_func_int32_t_s_s((0x7C50L & 4UL), (7UL && (l_1789[4][2] != ((((++(*l_1772)) , ((((safe_add_func_int32_t_s_s(((-5L) > 0x65C918FEL), 7L)) , l_1794) != p_76) , (*l_1693))) != p_75) , l_1772))))) < g_433.f0), p_74)) > 0xEF1BB985L) ^ p_74))) != g_143))) <= p_73) || p_74))) & p_73) > l_1795[1][0][1]);
                for (l_1757 = 1; (l_1757 <= 5); l_1757 += 1)
                { /* block id: 809 */
                    union U1 **l_1797 = &l_1734;
                    int32_t **l_1804 = &g_391;
                    g_391 = p_76;
                    (*l_1797) = g_1796;
                    for (l_1542 = 0; (l_1542 <= 5); l_1542 += 1)
                    { /* block id: 814 */
                        int16_t *l_1800 = (void*)0;
                        int16_t *l_1801 = &g_165;
                        int i, j;
                        (*l_1693) ^= ((g_324[(l_1542 + 2)] ^ ((safe_rshift_func_int16_t_s_s(((*l_1801) = g_324[(l_1757 + 2)]), 5)) == (safe_mul_func_uint16_t_u_u((l_1804 == ((l_1805 && (((l_1806 != (void*)0) == (&g_228[0] == l_1808)) || ((((-1L) || (**g_1379)) != p_73) < (**l_1804)))) , l_1809)), 0x61D3L)))) == g_324[7]);
                        if (g_427.f0)
                            goto lbl_1810;
                    }
                }
            }
        }
        else
        { /* block id: 821 */
            int8_t l_1813 = 0L;
            uint16_t *l_1827 = &g_1536;
            int32_t l_1831 = 0L;
            uint32_t l_1849 = 4294967290UL;
            int32_t l_1853 = (-8L);
            int32_t l_1855 = 0xFB71EB1AL;
            int32_t l_1856 = 0x3E6A39B1L;
            int32_t l_1857 = 0xA6138570L;
            int32_t l_1858 = (-1L);
            int32_t l_1859[1];
            float l_1877 = 0x0.6p-1;
            int16_t l_1878 = 0x18DEL;
            union U1 *l_1881 = &g_1882[6][2];
            int32_t l_1893 = 0xBC3EEB99L;
            uint32_t l_1895[8][5][6] = {{{18446744073709551615UL,0x02ECC6C3L,18446744073709551612UL,18446744073709551615UL,0x6DAEF704L,0x6DAEF704L},{1UL,1UL,1UL,1UL,0xE15A9B64L,1UL},{1UL,1UL,0xE15A9B64L,2UL,0x6DAEF704L,0x02ECC6C3L},{18446744073709551615UL,0x02ECC6C3L,0x5B24D094L,0x3E4F44C2L,0x6DAEF704L,0xC4A13448L},{18446744073709551612UL,1UL,0x1420A58AL,18446744073709551612UL,0xE15A9B64L,18446744073709551612UL}},{{7UL,1UL,0x6DAEF704L,1UL,0x6DAEF704L,1UL},{0x3E4F44C2L,0x02ECC6C3L,1UL,18446744073709551615UL,0x6DAEF704L,0xE15A9B64L},{18446744073709551609UL,1UL,0x02ECC6C3L,18446744073709551609UL,0xE15A9B64L,0x5B24D094L},{2UL,1UL,0xC4A13448L,7UL,0x6DAEF704L,0x1420A58AL},{18446744073709551615UL,0x02ECC6C3L,18446744073709551612UL,18446744073709551615UL,0x6DAEF704L,0x6DAEF704L}},{{1UL,1UL,1UL,1UL,0xE15A9B64L,1UL},{1UL,1UL,0xE15A9B64L,2UL,0x6DAEF704L,0x02ECC6C3L},{18446744073709551615UL,0x02ECC6C3L,0x5B24D094L,0x3E4F44C2L,0x6DAEF704L,0xC4A13448L},{18446744073709551612UL,1UL,0x1420A58AL,18446744073709551612UL,0xE15A9B64L,18446744073709551612UL},{7UL,1UL,0x6DAEF704L,1UL,0x6DAEF704L,1UL}},{{0x3E4F44C2L,0x02ECC6C3L,1UL,18446744073709551615UL,0x6DAEF704L,0xE15A9B64L},{18446744073709551609UL,1UL,0x02ECC6C3L,18446744073709551609UL,0xE15A9B64L,0x5B24D094L},{2UL,1UL,0xC4A13448L,7UL,0x6DAEF704L,0x1420A58AL},{18446744073709551615UL,0x02ECC6C3L,18446744073709551612UL,18446744073709551615UL,0x6DAEF704L,0x6DAEF704L},{1UL,1UL,1UL,1UL,0xE15A9B64L,1UL}},{{1UL,1UL,0xE15A9B64L,2UL,0x6DAEF704L,0x02ECC6C3L},{18446744073709551615UL,0x02ECC6C3L,0x5B24D094L,0x3E4F44C2L,0x6DAEF704L,0xC4A13448L},{18446744073709551612UL,1UL,0x1420A58AL,18446744073709551612UL,0xE15A9B64L,18446744073709551612UL},{7UL,1UL,0x6DAEF704L,1UL,0x6DAEF704L,1UL},{0x3E4F44C2L,0x02ECC6C3L,1UL,18446744073709551615UL,0x6DAEF704L,0xE15A9B64L}},{{18446744073709551609UL,1UL,0x02ECC6C3L,18446744073709551609UL,0xE15A9B64L,0x5B24D094L},{2UL,1UL,0xC4A13448L,7UL,0x6DAEF704L,0x1420A58AL},{18446744073709551615UL,0x02ECC6C3L,18446744073709551612UL,18446744073709551615UL,0x6DAEF704L,0x6DAEF704L},{1UL,1UL,1UL,1UL,0xE15A9B64L,1UL},{1UL,1UL,0xE15A9B64L,2UL,0x6DAEF704L,0x02ECC6C3L}},{{18446744073709551615UL,0x02ECC6C3L,0x5B24D094L,0x3E4F44C2L,0x6DAEF704L,0xC4A13448L},{18446744073709551612UL,1UL,7UL,1UL,0UL,18446744073709551607UL},{0x5B24D094L,0UL,1UL,1UL,1UL,0UL},{0x6DAEF704L,0x69AFBEE4L,0xEC370761L,0xE15A9B64L,1UL,0UL},{0x02ECC6C3L,0UL,0x69AFBEE4L,0x02ECC6C3L,0UL,0x70471671L}},{{18446744073709551612UL,0UL,0x628C41E2L,0x5B24D094L,1UL,7UL},{0xE15A9B64L,0x69AFBEE4L,18446744073709551607UL,0xC4A13448L,1UL,1UL},{0x1420A58AL,0UL,0UL,0x1420A58AL,0UL,0xEC370761L},{1UL,0UL,0UL,18446744073709551612UL,1UL,0x69AFBEE4L},{0xC4A13448L,0x69AFBEE4L,0x70471671L,0x6DAEF704L,1UL,0x628C41E2L}}};
            union U1 ***l_1900 = &l_1733[1][0][2];
            uint64_t l_2012 = 0xAB721576660B2563LL;
            union U1 **l_2024 = &l_1734;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1859[i] = 0x01466F8DL;
lbl_2020:
            if ((safe_sub_func_int32_t_s_s((l_1780 &= ((((l_1813 | (safe_div_func_int64_t_s_s(0x94DC00F5C1E153E7LL, (safe_mul_func_uint16_t_u_u(((*p_72) = ((g_1818 = &g_165) == (void*)0)), (((l_1714 == ((**g_656) = ((~0x51D3BC712DBF86D1LL) != ((*l_1827) |= (safe_add_func_uint16_t_u_u(l_1711[0][2][1], (((safe_mul_func_int8_t_s_s((*g_1190), (safe_add_func_uint32_t_u_u(l_1813, 1UL)))) > l_1826[7][6][2]) ^ p_73))))))) >= 0x8DL) , p_73)))))) <= 0x234FEB3A7EA38461LL) != l_1711[1][0][3]) < 0xBA24542BL)), l_1826[1][1][2])))
            { /* block id: 827 */
                struct S0 ***l_1830 = (void*)0;
                (*l_1693) ^= (l_1829 != l_1830);
            }
            else
            { /* block id: 829 */
                int32_t *l_1832 = &g_113;
                int32_t *l_1833 = (void*)0;
                int32_t *l_1834 = &l_1584[5][7];
                int32_t *l_1835 = &l_1546;
                int32_t *l_1836 = (void*)0;
                int32_t *l_1837 = (void*)0;
                int32_t *l_1838 = (void*)0;
                int32_t *l_1839 = &l_1826[7][6][2];
                int32_t *l_1840 = &l_1551;
                int32_t *l_1841 = &l_1826[7][6][2];
                int32_t *l_1842 = &l_1831;
                int32_t *l_1843 = &l_1551;
                int32_t *l_1844 = (void*)0;
                int32_t *l_1845 = &l_1584[1][7];
                int32_t *l_1846 = (void*)0;
                int32_t *l_1847[5] = {&l_1826[9][6][2],&l_1826[9][6][2],&l_1826[9][6][2],&l_1826[9][6][2],&l_1826[9][6][2]};
                int32_t l_1892[4][5] = {{(-10L),0L,(-10L),0L,(-10L)},{0L,0L,0L,0L,0L},{(-10L),0L,(-10L),0L,(-10L)},{0L,0L,0L,0L,0L}};
                uint8_t ***l_1904 = &g_1903;
                uint32_t l_1945 = 18446744073709551615UL;
                int i, j;
                --l_1849;
                l_1860++;
                if ((((*l_1834) ^= p_74) != ((*p_72) & (p_75 | (((safe_add_func_uint16_t_u_u((((safe_unary_minus_func_uint64_t_u((safe_rshift_func_int16_t_s_u((((p_74 , ((~(((void*)0 == &l_1528) , (safe_mod_func_uint32_t_u_u((safe_add_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u((((0x48B7FFCA15DB3C82LL || (((*l_1842) = ((l_1826[7][6][2] > (**g_656)) > p_74)) || p_74)) > l_1859[0]) & p_75), p_75)), l_1878)), g_383)))) > p_73)) , l_1879) != l_1881), l_1857)))) , l_1878) >= p_74), p_75)) & 1L) == p_75)))))
                { /* block id: 834 */
                    (*l_1693) |= ((*l_1835) |= (safe_sub_func_uint8_t_u_u(((*g_657) ^= (safe_lshift_func_uint8_t_u_s(0UL, l_1859[0]))), (**g_1379))));
                    (*l_1528) = &g_4[7];
                }
                else
                { /* block id: 839 */
                    int8_t l_1888 = 3L;
                    int32_t l_1889 = 0xE4EAB2B8L;
                    int32_t l_1890 = (-7L);
                    int32_t l_1891[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_1891[i] = 7L;
                    for (l_1780 = 4; (l_1780 >= 1); l_1780 -= 1)
                    { /* block id: 842 */
                        int32_t l_1887 = 0xF57E0AC8L;
                        int32_t l_1894 = 5L;
                        int32_t **l_1901 = &l_1693;
                        int i;
                        ++l_1895[3][0][1];
                        (*l_1840) = (safe_mod_func_uint16_t_u_u(g_276[l_1780], ((*l_1827) ^= (l_1900 == l_1752))));
                        (*l_1832) &= ((((void*)0 != l_1901) | (((((l_1904 = g_1902) == &g_1228[1]) , ((safe_add_func_int16_t_s_s((safe_sub_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s((l_1858 | ((safe_div_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_u((l_1826[7][6][2] || ((p_73 || ((((*l_1843) = ((safe_add_func_int64_t_s_s(p_75, ((safe_mul_func_int8_t_s_s(((((&l_1794 == (void*)0) != g_1745[0][1]) <= 1L) <= p_74), 0x69L)) && l_1889))) >= p_73)) | 0xA4CD25FBL) > 0x52F6D89906932A0ELL)) || p_75)), 1)), 3)), (**l_1901))) , 1L)), p_73)), 0x86L)), l_1831)) | 0xA18DL)) , l_1891[4]) , p_73)) ^ p_73);
                    }
                    for (g_1536 = (-25); (g_1536 == 21); ++g_1536)
                    { /* block id: 852 */
                        if (p_73)
                            break;
                    }
                    if (p_74)
                    { /* block id: 855 */
                        uint32_t ***l_1930 = (void*)0;
                        int32_t l_1944 = 0x97181E8CL;
                        (*l_1842) = 0x56DDACF2L;
                        (*l_1834) ^= (((safe_mul_func_int8_t_s_s((((+(safe_sub_func_int16_t_s_s(((((*l_1693) = (p_73 <= ((((g_1931 = l_1928[1]) == l_1933[0][0][0]) || (((((***g_1902) = (((*g_1818) = (safe_lshift_func_uint8_t_u_u((--(*g_1229)), 6))) && (((~((safe_lshift_func_uint16_t_u_s((safe_mod_func_int32_t_s_s((&p_75 != ((+l_1888) , &p_75)), (-3L))), 13)) || l_1889)) || 0UL) | 0x8BL))) <= p_74) , l_1856) < 0xE2BFL)) | p_73))) != p_75) && p_73), 0xDA87L))) | 0UL) != p_74), 247UL)) & l_1890) , p_74);
                    }
                    else
                    { /* block id: 864 */
                        uint8_t l_1947 = 0xF6L;
                        ++l_1947;
                        return p_74;
                    }
                }
                if (((-1L) | ((safe_div_func_int32_t_s_s(p_75, p_74)) | ((safe_sub_func_int8_t_s_s(0xF9L, (((**g_1903) = (safe_add_func_int32_t_s_s(1L, (safe_div_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u(((*p_72) , l_1711[4][2][3]), 1)), l_1826[7][6][2]))))) <= ((((((safe_mod_func_uint16_t_u_u(0UL, (*p_72))) , p_73) <= 0xDD81B75CL) != 0UL) > l_1826[8][2][2]) >= p_75)))) | (*l_1841)))))
                { /* block id: 870 */
                    uint32_t l_1971[3][8][8] = {{{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L}},{{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L}},{{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L},{18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L,18446744073709551615UL,0x4A305CB5L}}};
                    uint16_t l_1976[2][2] = {{0x7E83L,0x7E83L},{0x7E83L,0x7E83L}};
                    int i, j, k;
                    (*p_76) = (((l_1826[7][6][2] = l_1711[1][2][0]) , ((((!(*l_1693)) > 0xA.64E27Ap+94) < ((!(((0x8C78DCE9B58D12B5LL < ((safe_sub_func_int32_t_s_s(((*l_1840) & (*p_72)), 0xCA36173DL)) , p_73)) , (safe_add_func_float_f_f(((safe_add_func_float_f_f(l_1970, l_1971[0][0][4])) == l_1971[0][0][4]), (*p_76)))) > (*l_1693))) < (*p_76))) > 0x7.D46012p+16)) > l_1893);
                    for (g_227 = 5; (g_227 >= 21); g_227++)
                    { /* block id: 875 */
                        float l_1974 = 0xD.324FBFp+46;
                        int32_t l_1975 = 0x13E7186EL;
                        l_1976[1][1]--;
                    }
                }
                else
                { /* block id: 878 */
                    uint32_t *l_2001 = &g_455;
                    uint64_t *l_2006 = &g_383;
                    int32_t l_2008 = 1L;
                    int32_t l_2011[1][8][7] = {{{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)},{0L,9L,9L,0L,(-1L),0xC65B0B56L,(-1L)}}};
                    int i, j, k;
                    if ((safe_div_func_uint16_t_u_u(((safe_div_func_int8_t_s_s(((safe_add_func_int32_t_s_s(((safe_sub_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_s(0x9530L, 8)) & (p_75 < ((safe_mul_func_uint8_t_u_u(((safe_add_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s((p_73 ^ ((*l_1843) = 0x946F144FC3696E28LL)), 4)) != p_75), 0x3F79L)) || 0x62FFL), (safe_mul_func_int8_t_s_s((safe_add_func_uint16_t_u_u((*p_72), (((*l_2006) = (((*l_2001)++) , ((safe_mod_func_int64_t_s_s(((g_1523 , l_1970) || l_1714), 0xB31AD5CE0AB62B8ELL)) ^ l_1779))) <= p_74))), 0xCEL)))) > (*p_72)))), l_1831)) , p_75), 4L)) >= p_75), 2L)) == (*g_1818)), (*p_72))))
                    { /* block id: 882 */
                        int16_t l_2007 = 0x0ACCL;
                        int32_t l_2009[5] = {0x96F72042L,0x96F72042L,0x96F72042L,0x96F72042L,0x96F72042L};
                        int32_t l_2010 = 0x3948B081L;
                        int i;
                        l_2012++;
                        return p_74;
                    }
                    else
                    { /* block id: 885 */
                        (*l_1843) = l_2008;
                    }
                    g_2015++;
                }
            }
            for (g_1050 = 0; (g_1050 < 18); g_1050 = safe_add_func_int16_t_s_s(g_1050, 1))
            { /* block id: 893 */
                if (l_1858)
                    goto lbl_2020;
            }
            g_2021[8] = (void*)0;
            (*l_2024) = g_1796;
        }
        l_2025[1][2][0] = &l_1848;
    }
    return (*g_1190);
}


/* ------------------------------------------ */
/* 
 * reads : g_573 g_149 g_93 g_428.f0 g_431.f1 g_165 g_148 g_430.f0 g_4 g_425 g_276 g_432.f1 g_1304 g_1190 g_361.f0 g_684.f4 g_601 g_455 g_227 g_1227 g_1228 g_1229 g_1230 g_657 g_681.f0 g_228 g_190 g_361.f1 g_515 g_682.f1 g_324 g_1497 g_143 g_656 g_427.f0 g_433.f0
 * writes: g_20 g_455 g_165 g_391 g_601 g_430.f0 g_1257 g_426 g_113 g_682.f1 g_361.f0 g_143 g_13 g_433.f0 g_227 g_4 g_190 g_148
 */
static int16_t  func_77(int16_t  p_78, int8_t  p_79, uint16_t * p_80, uint32_t  p_81)
{ /* block id: 227 */
    const uint64_t l_557 = 0x09DA838D0D8C71C7LL;
    int8_t *l_570 = &g_20;
    int32_t * const *l_571[7] = {(void*)0,&g_391,&g_391,(void*)0,&g_391,&g_391,(void*)0};
    uint8_t l_572 = 0xB2L;
    int64_t l_574 = (-1L);
    int8_t l_610 = 0xC8L;
    int64_t ***l_650[8][2][4] = {{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}},{{&g_197,&g_197,&g_197,&g_197},{&g_197,&g_197,&g_197,&g_197}}};
    int64_t ****l_649 = &l_650[7][1][2];
    union U1 *l_680[6] = {&g_682,&g_682,&g_682,&g_682,&g_682,&g_682};
    int16_t *l_688 = (void*)0;
    uint32_t *l_713 = (void*)0;
    uint32_t l_716 = 18446744073709551615UL;
    uint32_t l_730 = 1UL;
    int32_t l_769 = (-1L);
    float l_773 = 0xE.6DAE43p-77;
    uint32_t l_840 = 1UL;
    float ****l_941[9];
    uint32_t l_945 = 1UL;
    int64_t l_958[5][6] = {{0xA1388514FC554015LL,(-6L),(-1L),(-1L),(-6L),0xA1388514FC554015LL},{7L,0xA1388514FC554015LL,(-1L),0xA1388514FC554015LL,7L,7L},{0xB20D142A2E5CF62DLL,0xA1388514FC554015LL,0xA1388514FC554015LL,0xB20D142A2E5CF62DLL,(-6L),0xB20D142A2E5CF62DLL},{0xB20D142A2E5CF62DLL,(-6L),0xB20D142A2E5CF62DLL,0xA1388514FC554015LL,0xA1388514FC554015LL,0xB20D142A2E5CF62DLL},{7L,7L,0xA1388514FC554015LL,(-1L),0xA1388514FC554015LL,7L}};
    uint16_t l_1151 = 0x62BFL;
    int32_t l_1158[3];
    uint64_t l_1187 = 0x3024002C91AF4377LL;
    uint32_t l_1192[8] = {0x9EA1B1A0L,0x9EA1B1A0L,0x9EA1B1A0L,0x9EA1B1A0L,0x9EA1B1A0L,0x9EA1B1A0L,0x9EA1B1A0L,0x9EA1B1A0L};
    uint32_t l_1218 = 0UL;
    uint8_t * const *l_1225[7][5] = {{&g_657,&g_657,&g_657,&g_657,&g_657},{&g_657,&g_657,&g_657,&g_657,&g_657},{&g_657,&g_657,&g_657,&g_657,&g_657},{&g_657,&g_657,&g_657,&g_657,&g_657},{&g_657,&g_657,&g_657,&g_657,&g_657},{&g_657,&g_657,&g_657,&g_657,&g_657},{&g_657,&g_657,&g_657,&g_657,&g_657}};
    uint8_t * const * const *l_1224 = &l_1225[6][2];
    int32_t l_1237 = 0x4949EDA5L;
    int32_t *l_1309 = &l_769;
    const uint8_t **l_1350 = (void*)0;
    const uint8_t ***l_1349 = &l_1350;
    const uint8_t ****l_1348[10][2][9] = {{{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349},{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,(void*)0}},{{(void*)0,(void*)0,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349},{&l_1349,(void*)0,&l_1349,(void*)0,&l_1349,&l_1349,(void*)0,&l_1349,&l_1349}},{{&l_1349,(void*)0,(void*)0,&l_1349,&l_1349,(void*)0,&l_1349,&l_1349,&l_1349},{&l_1349,&l_1349,&l_1349,&l_1349,(void*)0,(void*)0,(void*)0,(void*)0,&l_1349}},{{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349},{&l_1349,&l_1349,&l_1349,&l_1349,(void*)0,&l_1349,(void*)0,&l_1349,&l_1349}},{{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,(void*)0,(void*)0,&l_1349},{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349}},{{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349},{&l_1349,&l_1349,&l_1349,(void*)0,&l_1349,(void*)0,&l_1349,(void*)0,&l_1349}},{{&l_1349,&l_1349,(void*)0,(void*)0,&l_1349,(void*)0,&l_1349,&l_1349,&l_1349},{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349}},{{&l_1349,(void*)0,&l_1349,&l_1349,&l_1349,(void*)0,&l_1349,(void*)0,&l_1349},{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,(void*)0}},{{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,(void*)0,&l_1349,&l_1349,&l_1349},{&l_1349,&l_1349,(void*)0,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349}},{{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,(void*)0},{&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349}}};
    const uint8_t *****l_1347 = &l_1348[1][0][6];
    struct S0 **l_1456 = (void*)0;
    uint64_t l_1475[6][1][7] = {{{18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL}},{{0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL}},{{18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL}},{{0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL}},{{18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL}},{{0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL,0x951E8AE99CD00738LL}}};
    int8_t l_1513 = 1L;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_941[i] = &g_589;
    for (i = 0; i < 3; i++)
        l_1158[i] = 0x79E585CCL;
lbl_1310:
    if ((safe_div_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u(l_557, p_78)), ((safe_add_func_uint32_t_u_u((((safe_mul_func_int16_t_s_s(((((safe_div_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((((safe_rshift_func_int8_t_s_u(((safe_sub_func_uint8_t_u_u(p_79, ((*l_570) = 0x90L))) != (((1UL < 0x4CB2L) < (((((&g_391 == l_571[6]) || ((((p_81 != p_81) || 0L) > l_572) | g_573)) < p_81) > g_149) & p_81)) , p_78)), 5)) & 7L) & g_93[0]), 0xC0L)) > 0x2609L), 65535UL)) && p_81) & p_81) < p_81), p_79)) < g_428[0][2].f0) , g_431.f1), p_78)) | l_574))))
    { /* block id: 229 */
        uint8_t l_578[5][1] = {{2UL},{0xA9L},{2UL},{0xA9L},{2UL}};
        int32_t *l_579 = (void*)0;
        int32_t l_604 = 0L;
        int32_t l_609 = 0xFC1C25D0L;
        int32_t l_611 = (-6L);
        int32_t l_619 = 0x5FA9A253L;
        int32_t l_621 = (-1L);
        int32_t l_622 = (-5L);
        int32_t l_623 = 0xE754714EL;
        int32_t l_624 = 0L;
        int32_t l_627 = 0x07514770L;
        int32_t l_628 = 0x591D6D78L;
        int32_t l_629[10] = {0xB8C623BFL,1L,0x015BA3EFL,0x015BA3EFL,1L,0xB8C623BFL,1L,0x015BA3EFL,0x015BA3EFL,1L};
        int8_t l_676[7][10][3] = {{{(-1L),0L,(-1L)},{0x43L,1L,(-1L)},{1L,(-4L),(-1L)},{1L,1L,(-3L)},{0x73L,0L,0L},{(-1L),(-10L),(-8L)},{(-3L),(-1L),1L},{9L,8L,1L},{(-1L),0x99L,0x45L},{1L,1L,0x99L}},{{(-10L),(-1L),1L},{1L,5L,1L},{(-1L),(-1L),0xB5L},{(-1L),0x1FL,(-10L)},{0xD7L,0x99L,1L},{(-1L),(-4L),1L},{1L,0xBCL,(-1L)},{(-1L),(-3L),0xB5L},{0x82L,0L,0xD7L},{(-1L),(-1L),(-7L)}},{{(-7L),9L,0xD3L},{0xB5L,(-10L),0L},{(-1L),1L,(-1L)},{1L,0xB5L,0L},{0xBCL,(-1L),0xD3L},{0xC2L,1L,(-7L)},{(-1L),2L,0xD7L},{3L,1L,0xB5L},{0x43L,0xD7L,(-1L)},{(-3L),1L,1L}},{{0xA5L,1L,1L},{2L,1L,(-1L)},{1L,0x1FL,0x78L},{(-1L),0x1FL,0x45L},{1L,1L,8L},{0L,1L,0xE4L},{5L,1L,(-1L)},{0xD3L,0xD7L,(-3L)},{0x9DL,1L,1L},{1L,2L,1L}},{{2L,1L,(-10L)},{0x99L,(-1L),(-1L)},{9L,0xB5L,(-1L)},{1L,1L,0xE9L},{9L,(-10L),0x1FL},{0x99L,9L,3L},{2L,(-1L),3L},{1L,0L,0xA5L},{0x9DL,(-3L),0L},{0xD3L,0xBCL,(-4L)}},{{5L,(-4L),1L},{0L,0x99L,0xC2L},{1L,0x43L,1L},{(-1L),0x90L,1L},{1L,(-8L),0xC2L},{2L,0xE9L,1L},{0xA5L,0x45L,(-4L)},{(-3L),0xE4L,0L},{0x43L,(-1L),0xA5L},{3L,3L,3L}},{{(-1L),(-1L),3L},{0xC2L,0xA5L,0x1FL},{0xBCL,1L,0xE9L},{1L,0x73L,(-1L)},{(-1L),1L,(-1L)},{0xB5L,0xA5L,(-10L)},{(-7L),(-1L),1L},{(-1L),3L,1L},{0x82L,(-1L),(-3L)},{(-1L),0xE4L,(-1L)}}};
        union U1 *l_683[2];
        int16_t l_775 = 0L;
        const int8_t l_800[3] = {0x3CL,0x3CL,0x3CL};
        struct S0 * const *l_824 = &g_426[0][3][4];
        struct S0 * const **l_823 = &l_824;
        uint8_t l_891 = 255UL;
        volatile int64_t * const  volatile * const ** volatile *l_911 = &g_912;
        uint32_t l_932 = 0UL;
        uint8_t l_964 = 0x2EL;
        int16_t l_994 = (-7L);
        uint32_t **l_1009 = (void*)0;
        const uint16_t *l_1058 = (void*)0;
        const uint16_t **l_1057 = &l_1058;
        const uint16_t **l_1059 = &l_1058;
        int32_t **l_1123 = &l_579;
        int32_t ***l_1122 = &l_1123;
        int16_t l_1165 = 1L;
        uint8_t l_1172 = 255UL;
        int8_t *l_1182 = &l_676[2][7][2];
        int64_t ****l_1199 = &l_650[5][0][3];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_683[i] = &g_684[0][1];
        for (g_455 = 0; (g_455 <= 6); g_455 += 1)
        { /* block id: 232 */
            int32_t *l_575 = &g_4[8];
            int32_t **l_576 = &l_575;
            int32_t l_607 = 0L;
            int32_t l_608 = (-6L);
            int32_t l_613 = 0xAC756206L;
            int32_t l_618 = (-1L);
            int32_t l_620 = 0x7DE0AA22L;
            int32_t l_625 = 0L;
            int32_t l_626 = 0x15F52C56L;
            int32_t l_630[8][3][5] = {{{0L,5L,4L,(-6L),4L},{0xCED7B2CEL,2L,0x7F2D0050L,2L,0xCED7B2CEL},{5L,(-6L),5L,5L,(-6L)}},{{(-1L),0x52644046L,4L,2L,3L},{4L,5L,0x024B26DAL,4L,5L},{0x7F2D0050L,0x52644046L,0x7F2D0050L,0xD3B74D0CL,0x081F696FL}},{{4L,0x5B01CEB8L,2L,5L,0xD1E72B69L},{3L,0x9AA5D330L,0xACB2BE53L,0x9AA5D330L,3L},{0xD1E72B69L,5L,2L,0x5B01CEB8L,4L}},{{0x081F696FL,0xD3B74D0CL,0x7F2D0050L,0x52644046L,0x7F2D0050L},{5L,4L,0x024B26DAL,5L,4L},{0x8FE97404L,0x52644046L,0xD32A39A7L,0L,3L}},{{4L,0xE8AF9EE9L,0xE8AF9EE9L,4L,0xD1E72B69L},{0x081F696FL,0x52644046L,0xCED7B2CEL,0x9AA5D330L,0x081F696FL},{0x5B01CEB8L,4L,2L,0xE8AF9EE9L,5L}},{{3L,0xD3B74D0CL,0xD32A39A7L,0x9AA5D330L,0x8FE97404L},{5L,5L,7L,4L,4L},{0x7F2D0050L,0x9AA5D330L,0x7F2D0050L,0L,0x081F696FL}},{{5L,0x5B01CEB8L,0xE8AF9EE9L,5L,0x5B01CEB8L},{3L,0x52644046L,0xACB2BE53L,0x52644046L,3L},{0x5B01CEB8L,5L,0xE8AF9EE9L,0x5B01CEB8L,5L}},{{0x081F696FL,0L,0x7F2D0050L,0x9AA5D330L,0x7F2D0050L},{4L,4L,7L,5L,5L},{0x8FE97404L,0x9AA5D330L,0xD32A39A7L,0xD3B74D0CL,3L}}};
            int i, j, k;
            (*l_576) = l_575;
            for (g_165 = 2; (g_165 <= 6); g_165 += 1)
            { /* block id: 236 */
                const uint32_t l_602 = 5UL;
                int32_t l_606[2];
                int32_t l_632 = 0L;
                const int64_t ** const *l_648 = (void*)0;
                float ***l_659 = &g_590;
                int i;
                for (i = 0; i < 2; i++)
                    l_606[i] = 0x7C0CB202L;
                for (l_574 = 0; (l_574 <= 4); l_574 += 1)
                { /* block id: 239 */
                    int i, j;
                    for (g_20 = 4; (g_20 >= 0); g_20 -= 1)
                    { /* block id: 242 */
                        int32_t **l_577[9][9][3] = {{{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{(void*)0,(void*)0,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{(void*)0,&g_391,&g_391},{(void*)0,&g_391,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,&g_391}},{{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{(void*)0,&g_391,(void*)0},{&g_391,(void*)0,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{(void*)0,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391}},{{&g_391,&g_391,&g_391},{(void*)0,&g_391,&g_391},{(void*)0,&g_391,&g_391},{(void*)0,(void*)0,&g_391},{&g_391,&g_391,(void*)0},{&g_391,&g_391,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,(void*)0},{&g_391,&g_391,(void*)0}},{{&g_391,(void*)0,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{&g_391,(void*)0,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391}},{{&g_391,(void*)0,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{(void*)0,&g_391,(void*)0},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0}},{{&g_391,&g_391,&g_391},{(void*)0,(void*)0,(void*)0},{&g_391,&g_391,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{&g_391,(void*)0,&g_391},{&g_391,&g_391,&g_391},{(void*)0,&g_391,&g_391}},{{&g_391,(void*)0,(void*)0},{&g_391,(void*)0,&g_391},{&g_391,&g_391,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,(void*)0},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{&g_391,(void*)0,&g_391},{(void*)0,(void*)0,&g_391}},{{&g_391,&g_391,(void*)0},{&g_391,&g_391,(void*)0},{&g_391,(void*)0,&g_391},{(void*)0,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,(void*)0,&g_391},{(void*)0,&g_391,&g_391}},{{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{(void*)0,&g_391,(void*)0},{&g_391,&g_391,&g_391},{&g_391,&g_391,&g_391},{&g_391,&g_391,(void*)0},{&g_391,&g_391,&g_391},{(void*)0,(void*)0,(void*)0}}};
                        int i, j, k;
                        g_391 = ((*l_576) = &g_113);
                    }
                    if (g_148[l_574][(g_165 + 1)])
                        continue;
                    if (l_578[4][0])
                        break;
                }
                for (p_81 = 0; (p_81 <= 1); p_81 += 1)
                { /* block id: 251 */
                    int32_t l_605 = 1L;
                    int32_t l_612 = 0x1BD8BF0AL;
                    int32_t l_614 = (-2L);
                    int32_t l_615 = 0xA499E8B9L;
                    int32_t l_616 = 1L;
                    int32_t l_617[1][8][1] = {{{(-2L)},{0x857EF306L},{(-2L)},{0x857EF306L},{(-2L)},{0x857EF306L},{(-2L)},{0x857EF306L}}};
                    uint32_t l_633 = 0xA4AABEA3L;
                    uint8_t * volatile **l_658 = &g_656;
                    uint64_t *l_675 = &g_515;
                    int i, j, k;
                }
            }
        }
        for (g_601 = 0; (g_601 <= 0); g_601 += 1)
        { /* block id: 281 */
            uint16_t l_687 = 0UL;
            int16_t *l_689 = &g_165;
            const int32_t *l_714 = &g_682.f0;
            int64_t ****l_744 = (void*)0;
            float *l_767 = &g_13[3][0];
            float **l_766 = &l_767;
            int32_t l_771 = (-1L);
            int32_t l_772[4] = {0x9E71B0ADL,0x9E71B0ADL,0x9E71B0ADL,0x9E71B0ADL};
            struct S0 ***l_817 = &g_425;
            uint8_t l_862 = 0x79L;
            const float ***l_934 = (void*)0;
            const float ****l_933 = &l_934;
            uint8_t **l_950 = &g_657;
            uint8_t l_982 = 0UL;
            const uint16_t *l_1055[10][7] = {{&g_499,&g_37,&g_37,&g_499,&g_37,&g_37,&g_499},{&g_276[4],&g_37,&g_276[1],&l_687,&g_37,&l_687,&g_276[1]},{&g_499,&g_499,&g_37,&l_687,&g_37,&g_37,&g_37},{(void*)0,&g_276[1],&g_276[1],(void*)0,&l_687,&g_84,&g_276[1]},{&g_276[1],&g_37,(void*)0,(void*)0,&g_37,&g_276[1],&g_37},{&l_687,&g_276[1],&g_37,&g_276[4],&g_276[4],&g_37,&g_276[1]},{&g_37,&g_37,&g_276[1],&g_37,(void*)0,(void*)0,&g_37},{&g_84,&g_276[1],&g_84,&g_499,&g_276[1],&g_276[3],&g_276[3]},{&g_37,&g_37,&g_499,&g_37,&g_37,&g_499,&g_276[1]},{&g_276[4],&g_276[3],&g_499,&g_276[4],&g_499,&g_276[3],&g_276[4]}};
            const uint16_t **l_1054 = &l_1055[9][2];
            int8_t l_1066 = 0xF1L;
            float l_1117 = 0x4.1485CBp+97;
            float l_1128 = (-0x1.0p+1);
            uint16_t l_1131 = 0x8183L;
            int32_t l_1152 = 0x9E456A64L;
            int i, j;
            l_683[0] = l_680[3];
            if (g_431.f1)
                goto lbl_1310;
        }
        return g_573;
    }
    else
    { /* block id: 524 */
        int64_t *** const *l_1233[5][3][6] = {{{&l_650[6][0][3],&l_650[7][1][2],&l_650[6][0][3],&l_650[7][1][2],&l_650[3][0][0],&l_650[7][1][2]},{&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[2][1][2]},{&l_650[3][0][0],&l_650[7][1][2],&l_650[6][0][3],&l_650[7][1][2],&l_650[6][0][3],&l_650[7][1][2]}},{{&l_650[7][1][2],&l_650[2][0][3],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2]},{&l_650[6][0][3],&l_650[2][1][2],&l_650[6][0][3],&l_650[2][0][3],&l_650[3][0][0],&l_650[2][1][2]},{&l_650[7][1][2],&l_650[2][1][2],&l_650[7][1][2],&l_650[2][1][2],&l_650[7][1][2],&l_650[7][1][2]}},{{&l_650[3][0][0],&l_650[2][0][3],&l_650[6][0][3],&l_650[2][1][2],&l_650[6][0][3],&l_650[2][0][3]},{&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[2][0][3],&l_650[7][1][2],&l_650[2][0][3]},{&l_650[6][0][3],&l_650[7][1][2],&l_650[6][0][3],&l_650[7][1][2],&l_650[3][0][0],&l_650[7][1][2]}},{{&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[2][1][2]},{&l_650[3][0][0],&l_650[7][1][2],&l_650[6][0][3],&l_650[7][1][2],&l_650[6][0][3],&l_650[7][1][2]},{&l_650[7][1][2],&l_650[2][0][3],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2],&l_650[7][1][2]}},{{&l_650[6][0][3],&l_650[2][1][2],&l_650[6][0][3],&l_650[2][0][3],&l_650[3][0][0],&l_650[2][1][2]},{&l_650[7][1][2],&l_650[2][1][2],&l_650[7][1][2],&l_650[2][1][2],&l_650[7][1][2],&l_650[7][1][2]},{&l_650[3][0][0],&l_650[2][0][3],&l_650[6][0][3],&l_650[2][1][2],&l_650[6][0][3],&l_650[2][0][3]}}};
        int32_t l_1247 = (-1L);
        int32_t l_1249 = 0x495044FDL;
        int32_t l_1250 = 1L;
        int32_t l_1251 = 0x2B17DC42L;
        uint32_t l_1252 = 2UL;
        int i, j, k;
        if ((&l_650[7][1][2] == l_1233[2][0][5]))
        { /* block id: 525 */
            union U1 **l_1234 = &l_680[3];
            int32_t l_1239[6][5];
            int16_t l_1248 = (-1L);
            int i, j;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 5; j++)
                    l_1239[i][j] = 0xB0F7D8A3L;
            }
            (*l_1234) = l_680[3];
            for (l_1218 = 20; (l_1218 > 27); l_1218 = safe_add_func_int16_t_s_s(l_1218, 5))
            { /* block id: 529 */
                uint32_t l_1238 = 0x2BACD50FL;
                int32_t l_1240 = 0x248DC48CL;
                int32_t l_1243 = 0xA4E4A554L;
                int32_t l_1246[6];
                int64_t *****l_1281 = &l_649;
                int i;
                for (i = 0; i < 6; i++)
                    l_1246[i] = (-3L);
                if (l_1237)
                    break;
                if (l_1238)
                    break;
                for (g_430.f0 = 0; (g_430.f0 <= 4); g_430.f0 += 1)
                { /* block id: 534 */
                    int32_t l_1241 = 0x1E47B51BL;
                    int32_t l_1242 = 0xC86162ABL;
                    int32_t l_1244 = (-10L);
                    int32_t l_1245[4][4][4] = {{{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL},{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL}},{{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL},{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL}},{{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL},{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL}},{{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL},{(-9L),(-1L),0x6F665632L,(-1L)},{(-9L),0xF09BD47CL,0x6F665632L,0xF09BD47CL}}};
                    uint8_t ***l_1269[5];
                    uint8_t ****l_1268 = &l_1269[4];
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_1269[i] = (void*)0;
                    l_1252++;
                    l_1241 &= (g_1257[0][0][4] = (safe_mod_func_uint64_t_u_u(g_4[g_430.f0], p_78)));
                    for (l_1240 = 0; (l_1240 <= 2); l_1240 += 1)
                    { /* block id: 540 */
                        uint64_t *l_1270 = &l_1187;
                        int64_t *****l_1282 = &g_1100;
                        int32_t l_1283 = 1L;
                        int16_t *l_1284 = (void*)0;
                        uint16_t l_1285 = 0UL;
                        uint32_t *l_1286 = &l_1192[1];
                        int i, j, k;
                        if (l_1245[3][1][1])
                            break;
                        (*g_425) = (void*)0;
                        g_113 = (safe_div_func_uint32_t_u_u((0x59L < (safe_lshift_func_uint16_t_u_s(((0x46F6L < (safe_mod_func_uint8_t_u_u((((safe_div_func_uint64_t_u_u(((l_1245[2][2][0] &= ((*l_1286) = (((*l_1270) = (safe_mul_func_uint16_t_u_u((&g_1227 == l_1268), l_1239[2][1]))) != ((((l_1243 <= (((((g_276[0] , (safe_lshift_func_int16_t_s_u(((l_1285 = (l_1283 = (safe_lshift_func_int16_t_s_u((safe_lshift_func_int16_t_s_s(((((safe_div_func_int32_t_s_s((&g_1100 == (l_1282 = l_1281)), l_1239[2][0])) || 0x83EBL) ^ (-2L)) > 0xD488906CL), g_432[0][1].f1)), 15)))) | 6L), 7))) && p_79) , l_1247) | l_1247) == g_4[(g_430.f0 + 1)])) >= l_1243) ^ 0UL) || 1UL)))) | 1UL), l_1239[3][0])) != (-1L)) >= l_1248), p_78))) && 0xBAD8L), 5))), 5L));
                    }
                }
            }
            l_1239[2][4] |= p_78;
        }
        else
        { /* block id: 554 */
            uint16_t l_1295[3];
            int32_t ****l_1300 = (void*)0;
            int32_t **l_1303 = &g_391;
            int32_t ***l_1302 = &l_1303;
            int32_t ****l_1301 = &l_1302;
            const int64_t l_1306 = 1L;
            int8_t l_1307 = 0xCCL;
            uint32_t *l_1308[1][2];
            int i, j;
            for (i = 0; i < 3; i++)
                l_1295[i] = 1UL;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_1308[i][j] = (void*)0;
            }
            l_1249 = ((((p_81 = (0xCC63L < (safe_add_func_int64_t_s_s((safe_add_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((l_1295[0] != ((*l_570) = ((((*g_1190) = (((l_1252 || (p_79 < l_1295[1])) >= ((safe_rshift_func_uint16_t_u_u(65535UL, 13)) <= 0L)) | ((safe_div_func_int64_t_s_s((((*l_1301) = (void*)0) != g_1304[0]), 1L)) <= l_1306))) | l_1307) == 0xB089F718FCAF17E1LL))), 0xB4L)), 255UL)), p_81)), 0xE2D7CCA7093C0B3ELL)))) || 0xC785DAAAL) ^ p_79) , 0x0.D163ECp+32);
            l_1309 = (void*)0;
        }
    }
    for (g_361.f0 = 3; (g_361.f0 <= 9); g_361.f0 += 1)
    { /* block id: 566 */
        uint32_t *l_1333 = &g_143;
        int32_t l_1334 = (-10L);
        int32_t l_1335 = 0x992D6595L;
        uint8_t l_1336 = 0xF2L;
        int32_t *l_1339 = &g_4[0];
        uint64_t l_1366 = 5UL;
        int64_t *****l_1392 = (void*)0;
        int32_t l_1396 = 7L;
        int32_t l_1397 = 1L;
        int32_t l_1398[3];
        int16_t l_1400 = 0xF513L;
        int8_t l_1401 = 0x59L;
        int32_t l_1453[6] = {0x256376BFL,0x256376BFL,0x256376BFL,0x256376BFL,0x256376BFL,0x256376BFL};
        int32_t l_1476[2][1];
        int64_t l_1516 = (-4L);
        uint32_t l_1517 = 0x617A2E0BL;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1398[i] = 0L;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
                l_1476[i][j] = (-8L);
        }
        g_13[4][1] = ((safe_mul_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f(((((l_1336 = (((safe_mul_func_uint8_t_u_u((g_4[g_361.f0] < (safe_mod_func_uint32_t_u_u(((safe_rshift_func_uint16_t_u_s(g_4[g_361.f0], (safe_sub_func_uint16_t_u_u((255UL != (p_78 ^ 255UL)), (0x2DL || (safe_rshift_func_uint8_t_u_u(0xC3L, (safe_mod_func_uint64_t_u_u(g_684[0][1].f4, (l_1335 = (l_1334 &= (safe_mod_func_uint32_t_u_u(((((((((*l_1333) = (p_81 = (safe_lshift_func_int8_t_s_u(p_79, 1)))) > 1L) , p_79) >= 4UL) >= 0xD1L) >= p_79) <= 0xFF24CABFL), p_78))))))))))))) , p_81), g_4[g_361.f0]))), g_4[g_361.f0])) > p_79) , p_79)) > p_79) < g_4[g_361.f0]) < 0x7.0CF6ECp+10), 0x0.Cp+1)), 0x0.8437CAp+97)), p_79)) > p_78);
        if ((safe_lshift_func_int16_t_s_s(0x6C15L, 15)))
        { /* block id: 573 */
            uint32_t l_1351[1][2];
            int32_t **l_1394[3];
            int32_t *l_1395[1][8] = {{&g_113,&l_1158[0],&g_113,&l_1158[0],&g_113,&l_1158[0],&g_113,&l_1158[0]}};
            int16_t l_1399 = 1L;
            int64_t l_1402 = 1L;
            uint16_t l_1403 = 5UL;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_1351[i][j] = 6UL;
            }
            for (i = 0; i < 3; i++)
                l_1394[i] = &g_391;
            l_1309 = l_1339;
            for (g_601 = 0; (g_601 > (-10)); g_601 = safe_sub_func_uint8_t_u_u(g_601, 6))
            { /* block id: 577 */
                uint32_t l_1343 = 0x57900E3DL;
                const uint8_t *****l_1346 = (void*)0;
                int32_t l_1354 = 0x501934C4L;
                int32_t l_1355 = (-1L);
                int32_t l_1356 = (-3L);
                int32_t l_1357 = 0x05708BBFL;
                uint8_t * const **l_1381 = &g_1379;
                l_1343 = ((!p_81) , ((void*)0 != &l_571[6]));
                for (l_574 = (-30); (l_574 != 20); ++l_574)
                { /* block id: 581 */
                    l_1347 = l_1346;
                    return l_1351[0][1];
                }
                for (g_433.f0 = (-15); (g_433.f0 >= 24); ++g_433.f0)
                { /* block id: 587 */
                    uint32_t l_1358 = 0x796D3B6FL;
                    int32_t **l_1361 = &l_1339;
                    --l_1358;
                    (*l_1361) = (void*)0;
                    for (g_455 = (-22); (g_455 < 17); g_455++)
                    { /* block id: 592 */
                        if (g_433.f0)
                            goto lbl_1310;
                    }
                }
                for (g_227 = (-22); (g_227 > (-20)); g_227++)
                { /* block id: 598 */
                    uint16_t l_1367 = 0UL;
                    const int32_t *l_1386 = &g_1387;
                    int32_t *l_1389 = (void*)0;
                }
            }
            l_1395[0][0] = (p_79 , &g_113);
            l_1403++;
        }
        else
        { /* block id: 622 */
            int32_t l_1406 = (-9L);
            uint32_t l_1459[8];
            uint64_t *l_1462 = &g_190[8][0];
            float ***l_1479 = &g_590;
            int32_t l_1512 = 0x54685131L;
            struct S0 *l_1514 = &g_1515;
            int i;
            for (i = 0; i < 8; i++)
                l_1459[i] = 0xBC4FB183L;
            if ((l_1406 = 0L))
            { /* block id: 624 */
                uint16_t l_1434 = 0xDFBAL;
                int32_t l_1441 = (-10L);
                const uint32_t l_1452 = 0UL;
                int32_t **l_1455[9] = {&l_1339,&l_1339,&l_1339,&l_1339,&l_1339,&l_1339,&l_1339,&l_1339,&l_1339};
                int32_t ***l_1454 = &l_1455[1];
                int i;
                if (g_430.f0)
                    goto lbl_1310;
                (*l_1339) = ((safe_div_func_float_f_f((g_13[4][1] = ((safe_lshift_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((safe_add_func_uint8_t_u_u((safe_unary_minus_func_int64_t_s((*l_1339))), (safe_mul_func_uint16_t_u_u((((safe_add_func_int16_t_s_s((safe_sub_func_int64_t_s_s((l_1406 <= (safe_div_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((p_79 || ((((safe_lshift_func_uint8_t_u_u(((***g_1227) ^ (safe_mul_func_uint8_t_u_u((*g_657), (l_1434--)))), 6)) <= (g_681.f0 & (-5L))) , (safe_add_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((((l_1441 = p_81) >= (safe_div_func_int16_t_s_s((safe_mod_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s((safe_add_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_s((3UL & l_1452), 4)) & g_148[5][2]), 1L)), p_81)) , 0UL), (*l_1339))), p_81))) > p_81) | 0xFB05L), 0x9C525207L)), p_78))) != 0xD423CCE9L)), p_81)), 0x6EL)), 0xC857L)), 0xD726L))), p_81)), 65533UL)) != g_228[4]) & (*l_1339)), (*l_1339))))), (-2L))), 0)) , g_361.f0)), l_1453[1])) != (-0x6.Dp-1));
                (*l_1454) = &l_1339;
            }
            else
            { /* block id: 631 */
                struct S0 ***l_1457 = &l_1456;
                (*l_1457) = l_1456;
            }
            if ((*l_1339))
                break;
            if (((safe_unary_minus_func_uint32_t_u(((l_1459[3] = 0x834063EC4A618C8DLL) | (safe_add_func_int64_t_s_s(p_78, (((((*l_1462)--) >= (((((((((safe_sub_func_int64_t_s_s((l_1406 = ((safe_mul_func_uint16_t_u_u(l_1406, (5L != (((0xF5C6L & (safe_sub_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u((((65532UL && (safe_lshift_func_uint16_t_u_s(p_79, 10))) , (g_361.f1 != (p_78 > p_81))) , p_79), l_1406)), (*l_1339)))) , p_81) & 0x8CF7F7171CDA7B26LL)))) & (*l_1339))), g_276[0])) ^ (*l_1339)) && g_515) == (*l_1339)) || p_78) ^ p_78) , p_79) != (*l_1339)) == (*g_1190))) , g_324[3]) || l_1475[4][0][6])))))) == l_1476[0][0]))
            { /* block id: 638 */
                int32_t **l_1477 = (void*)0;
                int32_t **l_1478 = &g_391;
                (*l_1478) = &g_4[3];
                for (g_143 = 0; g_143 < 10; g_143 += 1)
                {
                    for (p_79 = 0; p_79 < 3; p_79 += 1)
                    {
                        g_190[g_143][p_79] = 1UL;
                    }
                }
            }
            else
            { /* block id: 641 */
                int64_t l_1511 = 0x12E661486E3104E3LL;
                l_1335 |= (((l_1479 != (void*)0) && ((*l_1462) = ((safe_sub_func_int64_t_s_s((safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((safe_sub_func_uint8_t_u_u((((((***l_1224) = (safe_div_func_uint8_t_u_u((0L == (safe_mod_func_int64_t_s_s(((safe_mod_func_int8_t_s_s((+(safe_sub_func_int8_t_s_s((p_79 = g_1497), ((*g_1190) = ((*l_1339) > (safe_sub_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s(((safe_add_func_int32_t_s_s((((safe_mod_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u((((safe_sub_func_int16_t_s_s(((g_143 && (+(l_1511 , ((0x0C830EB5L != ((&l_1366 != &g_515) ^ g_324[3])) && (*l_1339))))) || p_81), p_78)) , 0UL) & p_81), p_78)), (-1L))) < g_228[4]) , 1L), l_1511)) , p_78), 5)), g_324[7]))))))), 255UL)) , p_81), 0x74E5C8667176A522LL))), (**g_656)))) != p_78) == l_1512) | p_78), l_1511)), (*l_1339))), l_1513)), p_81)) < (*l_1339)))) || (*g_1190));
            }
            l_1398[1] = ((*l_1339) = ((void*)0 == l_1514));
        }
        l_1517--;
        if (p_79)
            break;
    }
    return g_427.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_20 g_16 g_84 g_93 g_143 g_113 g_4 g_37 g_149 g_165 g_148 g_194 g_228 g_190 g_276 g_227 g_325 g_324 g_13 g_383 g_361.f1 g_391 g_408 g_370 g_425 g_455 g_431.f1 g_470 g_499 g_432.f1 g_432.f0 g_515 l_2218
 * writes: g_20 g_143 g_37 g_148 g_149 g_93 g_165 g_4 g_190 g_113 g_227 g_228 g_276 g_196 g_13 g_11 g_323 g_324 g_325 g_370 g_383 g_391 g_408 g_424 g_426 g_455 g_499 g_515
 */
static uint32_t  func_87(uint32_t  p_88, int8_t  p_89)
{ /* block id: 24 */
    int8_t *l_109 = &g_20;
    int32_t l_110 = 0xEDB7DE59L;
    int32_t l_525 = (-1L);
    int32_t l_526 = 1L;
    int32_t l_527 = 0L;
    int32_t l_528 = 5L;
    int32_t l_529[1][1][5] = {{{1L,1L,1L,1L,1L}}};
    int32_t *l_534 = &g_4[2];
    int32_t *l_535 = &l_529[0][0][4];
    int32_t *l_536 = (void*)0;
    int32_t *l_537 = &g_4[9];
    int32_t *l_538 = &l_527;
    int32_t *l_539 = &l_529[0][0][4];
    int32_t *l_540[7][1];
    uint16_t l_541 = 65531UL;
    int i, j, k;
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
            l_540[i][j] = &l_527;
    }
    for (g_20 = 0; (g_20 == (-13)); g_20 = safe_sub_func_uint8_t_u_u(g_20, 3))
    { /* block id: 27 */
        int64_t *l_92[3];
        int32_t l_94 = 0x5DC3D349L;
        uint64_t l_111 = 0xE501E45A2DC96F09LL;
        uint64_t *l_514 = &g_515;
        uint64_t *l_516 = &g_383;
        int32_t **l_517 = &g_391;
        int i;
        for (i = 0; i < 3; i++)
            l_92[i] = &g_93[0];
        (*l_517) = ((l_110 = ((l_94 |= p_89) ^ ((*l_516) = ((*l_514) |= func_38(g_16, ((p_88 != ((safe_unary_minus_func_int32_t_s((g_84 & ((+(((func_97(func_102(&g_16, g_84, (((safe_lshift_func_int8_t_s_s(((((void*)0 == l_109) == g_93[1]) != 0x633FL), l_110)) && p_89) && l_111), l_110), l_111, p_88, g_113) , g_432[0][1].f0) < 1UL) , p_88)) , l_110)))) ^ 0x347AL)) , l_110), l_110))))) , &l_94);
        return g_149;
    }
    for (g_143 = (-19); (g_143 == 40); g_143 = safe_add_func_uint32_t_u_u(g_143, 6))
    { /* block id: 221 */
        int32_t *l_520 = &g_4[4];
        int32_t *l_521 = (void*)0;
        int32_t l_522 = 0xBD94162BL;
        int32_t *l_523 = &l_522;
        int32_t *l_524[9][6][4] = {{{&l_522,&l_522,&g_4[1],&g_113},{&g_113,&g_113,&g_4[9],&g_113},{(void*)0,(void*)0,(void*)0,&g_4[8]},{&l_110,(void*)0,&g_4[8],(void*)0},{&l_522,&g_4[9],&g_113,(void*)0},{(void*)0,&g_113,&g_113,&g_113}},{{&l_522,&g_113,&g_4[8],&g_113},{&l_110,&l_522,(void*)0,&l_522},{(void*)0,&l_522,&g_4[9],(void*)0},{&g_113,&g_4[9],&g_4[1],&g_4[8]},{&l_522,&g_16,(void*)0,(void*)0},{&l_522,&l_522,(void*)0,&g_113}},{{(void*)0,&g_113,&g_4[0],&l_522},{&l_522,&l_110,&g_4[8],&g_4[0]},{&g_113,&l_110,&g_4[1],&l_522},{&l_110,&g_113,&g_4[6],&g_113},{&g_113,&l_522,&l_110,(void*)0},{&l_110,&g_16,(void*)0,&g_4[8]}},{{&l_522,&g_4[9],(void*)0,(void*)0},{(void*)0,&l_522,&g_113,&l_522},{&g_4[9],&l_522,(void*)0,&g_113},{&g_113,&g_113,&l_110,&g_113},{(void*)0,&g_113,&l_522,(void*)0},{(void*)0,&g_4[9],&l_110,(void*)0}},{{&g_113,(void*)0,(void*)0,&g_4[8]},{&g_4[9],(void*)0,&g_113,&g_113},{(void*)0,&g_113,(void*)0,&g_113},{&l_522,&l_522,(void*)0,(void*)0},{&l_110,&l_110,&l_110,&l_110},{&g_113,&l_522,&g_4[6],(void*)0}},{{&l_110,(void*)0,&g_4[1],(void*)0},{&g_113,(void*)0,&g_4[8],(void*)0},{&l_522,(void*)0,&g_4[0],(void*)0},{(void*)0,&l_522,(void*)0,&l_110},{&l_522,&l_110,(void*)0,(void*)0},{&l_522,&l_522,&g_4[1],&g_113}},{{&g_113,&g_113,&g_4[9],&g_113},{(void*)0,(void*)0,(void*)0,&g_4[8]},{&l_110,(void*)0,&g_4[8],&g_113},{&l_110,(void*)0,(void*)0,(void*)0},{(void*)0,&g_113,(void*)0,&l_522},{&l_110,(void*)0,&g_4[0],(void*)0}},{{&g_4[1],(void*)0,&g_113,(void*)0},{&g_113,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_16,&g_4[0]},{(void*)0,&g_113,(void*)0,(void*)0},{&l_110,&l_110,&l_522,&g_4[8]},{(void*)0,&g_113,&g_4[6],(void*)0}},{{(void*)0,(void*)0,&g_4[0],&g_4[6]},{&l_110,(void*)0,&g_16,(void*)0},{(void*)0,&g_113,(void*)0,&g_4[8]},{(void*)0,&l_110,(void*)0,(void*)0},{&g_4[1],&g_113,&g_113,&g_4[0]},{(void*)0,(void*)0,&l_522,(void*)0}}};
        int32_t l_530 = (-1L);
        uint32_t l_531 = 0UL;
        int i, j, k;
        l_531--;
    }
    --l_541;
    return p_88;
}


/* ------------------------------------------ */
/* 
 * reads : g_84 g_93 g_4 g_20 g_113 g_194 g_228 g_148 g_190 g_165 g_143 g_276 g_16 g_37 g_227 g_149 g_325 g_324 g_13 g_383 g_361.f1 g_391 g_408 g_370 g_425 g_455 g_431.f1 g_470 g_499 g_432.f1 l_2218
 * writes: g_190 g_37 g_113 g_4 g_227 g_228 g_276 g_165 g_196 g_149 g_13 g_11 g_323 g_324 g_143 g_325 g_148 g_370 g_383 g_391 g_408 g_424 g_426 g_455 g_499
 */
static uint32_t  func_97(int8_t * p_98, int8_t  p_99, const int64_t  p_100, const int64_t  p_101)
{ /* block id: 42 */
    int32_t *l_166 = &g_4[9];
    int32_t *l_167 = &g_113;
    int32_t *l_168 = &g_4[9];
    int32_t l_169 = 0x70889BAEL;
    int32_t *l_170 = (void*)0;
    int32_t *l_171 = (void*)0;
    int32_t l_172 = 0x24557DD3L;
    int32_t *l_173 = &g_4[7];
    int32_t *l_174 = (void*)0;
    int32_t *l_175 = &g_4[9];
    int32_t *l_176 = &g_4[9];
    int32_t *l_177 = &g_4[4];
    int32_t *l_178 = &l_169;
    int32_t *l_179 = &g_4[2];
    int32_t *l_180 = &l_172;
    int32_t *l_181[9] = {&g_16,&g_16,&g_16,&g_16,&g_16,&g_16,&g_16,&g_16,&g_16};
    int64_t l_182 = 1L;
    uint32_t l_183 = 0x5407581BL;
    uint64_t *l_189 = &g_190[4][2];
    float *l_272[6] = {&g_11,&g_11,&g_11,&g_11,&g_11,&g_11};
    int32_t *l_295 = &g_4[2];
    uint32_t l_351[4][1];
    int64_t ***l_403[5] = {&g_197,&g_197,&g_197,&g_197,&g_197};
    int64_t ****l_402 = &l_403[0];
    struct S0 ***l_434 = &g_425;
    int32_t l_442[2][1];
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
            l_351[i][j] = 0x4A65A11AL;
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_442[i][j] = 8L;
    }
lbl_291:
    l_183--;
    if ((((1UL > g_84) >= p_101) | ((((safe_lshift_func_uint8_t_u_u(255UL, (safe_unary_minus_func_uint64_t_u((((((((*l_189) = g_93[1]) || (((((*l_177) & (safe_rshift_func_uint8_t_u_u((0x1D18F8EAD33D034BLL & (((-9L) ^ (!(*p_98))) & 4294967295UL)), 7))) < g_4[9]) , 0x9.Ap-1) , 1UL)) , &g_37) == &g_84) >= 65535UL) & (-1L)))))) <= (*l_167)) <= p_100) , p_101)))
    { /* block id: 45 */
        int64_t l_210 = 3L;
        int32_t l_224 = 0x076EBF33L;
        int16_t l_233 = (-4L);
        int32_t l_235 = 2L;
        for (g_37 = 0; (g_37 <= 1); g_37 += 1)
        { /* block id: 48 */
            int64_t ** volatile *l_198 = &g_197;
            int32_t l_211 = 0L;
            uint8_t l_234 = 255UL;
            const int8_t l_236 = 0xC3L;
            int32_t *l_283 = &l_235;
            for (g_113 = 0; (g_113 <= 1); g_113 += 1)
            { /* block id: 51 */
                uint8_t *l_223[1];
                int32_t *l_225 = (void*)0;
                int32_t *l_226 = &g_227;
                uint32_t *l_231 = (void*)0;
                int32_t l_232 = 0x050347C1L;
                int32_t **l_237 = &l_171;
                int32_t **l_238 = (void*)0;
                int32_t **l_239 = (void*)0;
                int32_t **l_240 = &l_178;
                int64_t ***l_279 = &g_197;
                int i;
                for (i = 0; i < 1; i++)
                    l_223[i] = &g_148[3][1];
                l_198 = g_194[0][8][6];
                (*l_240) = ((*l_237) = ((((safe_sub_func_int32_t_s_s((((3UL != (!(safe_rshift_func_int16_t_s_u((((g_93[g_113] < (safe_mod_func_int16_t_s_s((safe_sub_func_int64_t_s_s(((safe_add_func_uint16_t_u_u((l_210 && l_211), (safe_add_func_int32_t_s_s((safe_div_func_int32_t_s_s(((l_235 = ((safe_sub_func_uint16_t_u_u((((((l_232 = (!((safe_sub_func_uint8_t_u_u((((g_228[4] &= (((*l_168) = (safe_rshift_func_uint8_t_u_s((l_224 = 0xDDL), l_211))) , ((*l_226) = 1L))) , (0L <= (safe_lshift_func_uint16_t_u_u(p_101, 4)))) , g_20), g_93[1])) == g_148[5][7]))) == l_233) == 0xCE9EL) || g_4[7]) == 3L), 0L)) , l_234)) == 0x90DBD7C481DA6610LL), l_236)), 0UL)))) >= g_93[0]), g_93[g_113])), g_190[2][0]))) | p_99) || g_165), 9)))) | p_99) == g_190[4][2]), 0xE2DC38A9L)) <= g_93[0]) < g_143) , &l_211));
                (*l_179) |= ((void*)0 == p_98);
                if (((void*)0 != &g_93[g_113]))
                { /* block id: 62 */
                    uint64_t l_281[7][2] = {{0x703896E17EB04718LL,0x703896E17EB04718LL},{0x7FBBC7000DFB2D17LL,0x703896E17EB04718LL},{0x703896E17EB04718LL,0x7FBBC7000DFB2D17LL},{0x703896E17EB04718LL,0x703896E17EB04718LL},{0x7FBBC7000DFB2D17LL,0x703896E17EB04718LL},{0x703896E17EB04718LL,0x7FBBC7000DFB2D17LL},{0x703896E17EB04718LL,0x703896E17EB04718LL}};
                    int32_t l_282 = 3L;
                    int64_t **l_288 = &g_196;
                    int i, j;
                    for (l_211 = 1; (l_211 >= 0); l_211 -= 1)
                    { /* block id: 65 */
                        float *l_267[6][6] = {{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_13[4][1],&g_11,&g_11,&g_11,&g_13[4][1],&g_13[4][1]},{&g_13[1][1],&g_11,&g_11,&g_13[1][1],&g_11,&g_13[1][1]},{&g_13[1][1],&g_11,&g_13[1][1],&g_11,&g_11,&g_13[1][1]},{&g_13[4][1],&g_13[4][1],&g_11,&g_11,&g_11,&g_13[4][1]},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11}};
                        float **l_266 = &l_267[5][3];
                        float *l_269[3][3] = {{&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11}};
                        float **l_268 = &l_269[1][1];
                        float *l_271 = &g_11;
                        float **l_270[1][10] = {{&l_271,&l_271,&l_271,&l_271,&l_271,&l_271,&l_271,&l_271,&l_271,&l_271}};
                        uint16_t *l_275 = &g_276[0];
                        int16_t *l_280 = &g_165;
                        int i, j;
                        (*l_177) = (((((safe_lshift_func_uint16_t_u_u((safe_add_func_int64_t_s_s(p_101, (safe_sub_func_uint64_t_u_u((safe_div_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((*p_98), (+(safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((((l_282 = (safe_div_func_int32_t_s_s(((p_101 != (safe_add_func_uint64_t_u_u((((safe_rshift_func_int8_t_s_s((safe_mod_func_int16_t_s_s((((*l_266) = l_226) == (l_272[3] = ((*l_268) = l_166))), (((safe_mul_func_uint16_t_u_u(((*l_275) ^= 0xB0DCL), ((((**l_240) & (((*l_280) &= (safe_mod_func_uint64_t_u_u(((l_236 , (void*)0) == l_279), p_101))) , l_233)) > p_101) >= l_211))) <= (*p_98)) && g_113))), g_16)) , (**l_237)) , p_100), 0x6E52BDFDB656643BLL))) ^ 4294967286UL), l_281[5][1]))) && (**l_240)) && 0L) | l_210), l_236)), 2)), 0x69DAL))))), l_235)), g_20)))), p_100)) < g_37) | 18446744073709551615UL) || g_4[7]) & g_113);
                    }
                    for (l_234 = 0; (l_234 <= 1); l_234 += 1)
                    { /* block id: 76 */
                        if (p_100)
                            break;
                        l_283 = &g_113;
                    }
                    (*l_175) = (safe_add_func_uint64_t_u_u(((safe_lshift_func_int16_t_s_u((((*l_288) = &l_210) != &g_93[g_113]), 4)) && p_100), p_100));
                }
                else
                { /* block id: 82 */
                    uint8_t l_289 = 0x67L;
                    (*l_175) ^= g_227;
                    (*l_175) = 0x5ED5C099L;
                    if (p_100)
                    { /* block id: 85 */
                        (*l_171) = (l_289 = ((*l_177) |= g_148[0][2]));
                    }
                    else
                    { /* block id: 89 */
                        return g_276[4];
                    }
                    for (g_149 = 0; (g_149 <= 1); g_149 += 1)
                    { /* block id: 94 */
                        return g_228[4];
                    }
                }
            }
            for (l_234 = 0; (l_234 <= 1); l_234 += 1)
            { /* block id: 101 */
                int32_t *l_290 = &g_16;
                l_290 = l_290;
                if ((*l_283))
                    break;
            }
        }
    }
    else
    { /* block id: 106 */
        for (g_165 = 0; g_165 < 9; g_165 += 1)
        {
            l_181[g_165] = &g_16;
        }
        if (g_113)
            goto lbl_291;
    }
    for (p_99 = 0; (p_99 == 24); p_99++)
    { /* block id: 112 */
        int32_t **l_294[1][10][10] = {{{(void*)0,&l_167,&l_179,(void*)0,&l_181[1],(void*)0,&l_173,&l_173,(void*)0,&l_181[1]},{&l_173,&l_170,&l_170,&l_173,&l_181[4],&l_180,&l_173,&l_179,&l_170,&l_176},{(void*)0,&l_179,&l_167,(void*)0,&l_180,&l_174,&l_170,&l_173,&l_170,&l_173},{(void*)0,&l_181[4],(void*)0,&l_173,(void*)0,&l_181[4],(void*)0,&l_180,(void*)0,&l_176},{&l_180,&l_176,&l_179,&l_181[1],&l_170,&l_167,&l_179,&l_180,(void*)0,&l_180},{&l_170,&l_176,&l_167,&l_174,(void*)0,(void*)0,&l_174,&l_167,&l_176,&l_170},{(void*)0,&l_170,(void*)0,&l_176,&l_180,&l_174,&l_174,&l_181[1],&l_179,&l_170},{(void*)0,&l_180,&l_179,&l_170,&l_180,&l_176,(void*)0,&l_176,&l_180,&l_170},{&l_180,&l_174,&l_180,(void*)0,(void*)0,&l_176,&l_170,&l_179,&l_173,&l_180},{&l_174,(void*)0,(void*)0,(void*)0,&l_170,&l_168,&l_167,&l_179,&l_179,&l_167}}};
        int64_t **l_300[6][8] = {{&g_196,(void*)0,&g_196,&g_196,(void*)0,(void*)0,&g_196,&g_196},{&g_196,&g_196,&g_196,(void*)0,&g_196,&g_196,&g_196,&g_196},{(void*)0,&g_196,&g_196,&g_196,&g_196,&g_196,(void*)0,&g_196},{&g_196,&g_196,&g_196,(void*)0,&g_196,&g_196,&g_196,&g_196},{&g_196,(void*)0,(void*)0,&g_196,&g_196,(void*)0,&g_196,&g_196},{&g_196,&g_196,&g_196,&g_196,&g_196,&g_196,&g_196,&g_196}};
        int16_t l_322 = 0x4130L;
        uint8_t l_368[2][2] = {{0UL,0UL},{0UL,0UL}};
        int16_t l_381 = 0xF688L;
        int64_t ****l_405 = &l_403[0];
        float **l_496 = (void*)0;
        int32_t l_508 = 0x743AF3F6L;
        int i, j, k;
        l_295 = (void*)0;
        (*l_167) = ((*l_179) ^= (&g_148[3][3] != p_98));
        g_324[7] = (safe_div_func_float_f_f(g_149, ((safe_mul_func_float_f_f(p_101, (((*l_175) = p_100) >= (g_113 > ((void*)0 != l_300[0][0]))))) < (((g_323 = (safe_sub_func_float_f_f((g_11 = ((safe_add_func_float_f_f((((safe_add_func_float_f_f((!((((((safe_div_func_uint64_t_u_u(g_148[3][1], ((safe_lshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_u((((safe_div_func_float_f_f((g_13[4][1] = ((safe_sub_func_float_f_f(((safe_sub_func_float_f_f((safe_add_func_float_f_f(0x1.A18ECCp-92, ((0x9.9p+1 > p_101) , p_100))), (-0x7.9p+1))) != p_101), 0x8.BCD146p+71)) > p_101)), g_16)) , g_276[0]) , p_101), 11)), 13)) , p_101))) , 0x6.FFBF40p+6) > (-0x4.1p+1)) >= 0x3.D99382p+51) < g_276[3]) <= 0x0.4p+1)), g_16)) != 0x8.Bp+1) >= g_143), l_322)) < 0x2.C50F9Ep+16)), 0x3.7p-1))) == p_101) < p_101))));
        for (g_143 = 0; (g_143 <= 1); g_143 += 1)
        { /* block id: 123 */
            volatile struct S0 **l_327 = &g_325;
            int64_t ***l_337 = &l_300[2][1];
            int32_t l_352 = (-1L);
            uint8_t l_393 = 255UL;
            int64_t ****l_404 = &l_403[0];
            int32_t l_447 = 0xDE831855L;
            int32_t l_451 = 1L;
            int32_t l_454[8][9] = {{0L,0x0D7F1011L,1L,0xB2A08CD0L,0x530F2C12L,0L,(-1L),0xA90821E2L,1L},{(-1L),0xF5971CE6L,0xA90821E2L,1L,0xE4BD2645L,1L,0xA90821E2L,0xF5971CE6L,(-1L)},{0x530F2C12L,0x0D7F1011L,0x8715677BL,0xE4BD2645L,(-1L),1L,8L,1L,0xF5971CE6L},{0x0D7F1011L,1L,0L,0x8E60A467L,0x8E60A467L,0L,1L,0x0D7F1011L,0xB2A08CD0L},{0x530F2C12L,(-1L),8L,0xCD75C92EL,0x8E60A467L,0L,1L,0xE4BD2645L,0xE4BD2645L},{(-1L),0x8715677BL,(-1L),0x0D7F1011L,(-1L),0x8715677BL,(-1L),0L,0xB2A08CD0L},{0L,1L,(-1L),0x0D7F1011L,0xE4BD2645L,0x8E60A467L,0xF5971CE6L,(-1L),0xF5971CE6L},{0xF5971CE6L,0x530F2C12L,0xCD75C92EL,0xCD75C92EL,0x530F2C12L,0xF5971CE6L,0xB2A08CD0L,0L,(-1L)}};
            int32_t l_510 = 0x8ADA008EL;
            uint64_t l_511 = 0x746CD10336108B81LL;
            int i, j;
            (*l_327) = g_325;
            for (g_149 = 0; (g_149 <= 1); g_149 += 1)
            { /* block id: 127 */
                uint16_t l_330 = 0x578FL;
                uint32_t *l_350 = &g_143;
                int64_t ***l_354 = &g_197;
                int32_t l_423 = 0x64436861L;
                int32_t l_453[1][6][6] = {{{3L,0L,3L,3L,0L,3L},{3L,3L,0xB5ED8E83L,0xB5ED8E83L,3L,0xB5ED8E83L},{0xB5ED8E83L,3L,0xB5ED8E83L,0xB5ED8E83L,3L,0xB5ED8E83L},{0xB5ED8E83L,3L,0xB5ED8E83L,0xB5ED8E83L,3L,0xB5ED8E83L},{0xB5ED8E83L,3L,0xB5ED8E83L,0xB5ED8E83L,3L,0xB5ED8E83L},{0xB5ED8E83L,3L,0xB5ED8E83L,0xB5ED8E83L,3L,0xB5ED8E83L}}};
                uint16_t l_482 = 0UL;
                int i, j, k;
                for (l_322 = 1; (l_322 >= 0); l_322 -= 1)
                { /* block id: 130 */
                    int i, j;
                    l_181[(g_143 + 7)] = &g_113;
                    for (g_323 = 0; (g_323 <= 0); g_323 += 1)
                    { /* block id: 134 */
                        int64_t ****l_338 = &l_337;
                        uint8_t *l_343 = &g_324[2];
                        uint16_t *l_344 = (void*)0;
                        uint16_t *l_345 = &g_276[2];
                        int i, j, k;
                        l_167 = ((safe_rshift_func_int8_t_s_s((l_330 ^ ((safe_lshift_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u(((((p_99 != (1UL > (((p_100 < (&l_300[0][0] != ((*l_338) = l_337))) ^ (safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_s(((*l_343) = (&g_143 == (void*)0)), (((*l_345) &= 0xED7DL) >= p_101))), l_330))) > 9UL))) == g_227) , 0UL) ^ (*l_173)), p_101)) || 0L) < p_101), g_165)) >= p_99)), (*l_166))) , &g_113);
                        (*l_176) &= ((safe_mul_func_uint8_t_u_u(((-5L) == (safe_sub_func_int8_t_s_s((*p_98), (((0x1F4020B1L <= p_99) , l_350) == &g_143)))), ((l_351[1][0] , 0x64B3CA6587263D76LL) >= p_101))) , g_324[4]);
                    }
                    if ((p_101 & ((l_352 = g_13[g_143][g_143]) , p_100)))
                    { /* block id: 142 */
                        int64_t ****l_353 = &l_337;
                        struct S0 *l_360 = &g_361;
                        struct S0 **l_359 = &l_360;
                        struct S0 ***l_362 = &l_359;
                        uint32_t *l_363 = &l_351[3][0];
                        int16_t *l_364 = &g_165;
                        uint8_t *l_365 = &g_148[3][1];
                        int8_t *l_369[10][4][5] = {{{&g_20,&g_370[0][1][1],&g_370[1][2][5],&g_370[1][2][5],&g_370[0][1][1]},{&g_370[1][4][4],&g_370[1][1][5],&g_370[0][3][1],&g_20,(void*)0},{&g_370[0][3][1],&g_370[0][3][1],&g_20,&g_370[0][3][1],&g_20},{&g_20,&g_370[0][2][0],(void*)0,&g_20,&g_20}},{{&g_370[0][3][1],&g_20,&g_370[0][3][1],&g_370[0][1][1],&g_370[0][3][1]},{&g_370[1][4][4],&g_370[1][4][4],&g_370[1][1][3],&g_20,&g_20},{&g_20,&g_370[0][3][1],&g_370[0][3][1],&g_20,&g_370[0][3][1]},{&g_370[0][2][0],&g_20,&g_370[0][3][1],&g_370[0][3][1],&g_20}},{{&g_370[0][3][1],&g_370[0][3][1],(void*)0,&g_20,&g_20},{&g_370[1][1][5],&g_370[1][4][4],&g_370[1][1][5],&g_370[0][3][1],&g_20},{&g_370[0][1][1],&g_20,(void*)0,&g_370[0][1][1],&g_370[0][0][2]},{&g_20,&g_370[0][2][0],&g_370[0][3][1],&g_20,&g_370[1][4][4]}},{{&g_370[0][3][1],&g_370[0][3][1],(void*)0,&g_370[0][0][2],&g_370[1][1][5]},{&g_370[0][2][0],&g_370[1][1][5],&g_370[1][1][5],&g_370[0][2][0],&g_370[0][3][1]},{&g_370[0][1][1],&g_370[0][1][1],(void*)0,&g_370[0][3][1],&g_370[0][1][1]},{&g_370[0][3][1],&g_20,&g_370[0][3][1],(void*)0,&g_20}},{{&g_370[0][3][1],&g_370[0][3][1],&g_370[0][3][1],&g_370[0][3][1],(void*)0},{&g_20,&g_370[0][2][0],&g_370[1][1][3],&g_370[0][2][0],&g_20},{&g_370[0][3][1],&g_370[0][1][1],&g_370[0][3][1],&g_370[0][0][2],&g_370[0][3][1]},{&g_370[1][4][4],&g_370[0][3][1],(void*)0,&g_20,(void*)0}},{{&g_370[0][1][1],&g_370[0][3][1],&g_20,&g_370[0][1][1],&g_370[0][3][1]},{&g_20,&g_20,&g_370[0][3][1],&g_370[0][3][1],&g_20},{&g_370[0][3][1],&g_370[0][3][1],&g_370[1][2][5],&g_20,(void*)0},{&g_20,&g_370[1][4][4],(void*)0,&g_370[0][3][1],&g_20}},{{&g_370[0][0][2],&g_370[0][1][1],(void*)0,&g_20,&g_370[0][1][1]},{&g_20,&g_20,&g_370[0][3][1],&g_20,&g_370[0][3][1]},{&g_370[0][3][1],&g_370[0][3][1],&g_370[0][1][4],&g_370[0][1][1],&g_370[1][1][5]},{&g_20,&g_20,&g_370[1][1][5],&g_20,&g_370[1][4][4]}},{{&g_370[0][1][1],&g_370[0][0][2],&g_370[1][2][5],&g_370[0][3][1],&g_370[0][0][2]},{&g_370[1][4][4],&g_20,&g_20,&g_20,&g_20},{&g_370[0][3][1],&g_370[0][3][1],&g_370[0][3][1],&g_370[1][2][5],&g_20},{&g_20,&g_20,(void*)0,&g_370[0][2][0],&g_20}},{{&g_370[0][3][1],&g_370[0][1][1],&g_370[1][1][5],&g_370[1][2][5],(void*)0},{(void*)0,(void*)0,&g_370[0][3][1],&g_20,&g_20},{&g_370[0][3][1],&g_370[1][3][3],&g_370[1][3][3],&g_370[0][3][1],&g_370[0][3][1]},{&g_370[1][1][5],&g_370[0][3][1],&g_20,(void*)0,&g_370[0][3][1]}},{{&g_370[0][3][1],&g_370[0][0][2],&g_370[0][1][4],&g_370[0][3][1],&g_370[0][3][1]},{&g_20,(void*)0,&g_370[0][3][3],(void*)0,&g_370[0][3][1]},{&g_370[1][2][5],&g_370[0][3][1],&g_370[0][3][1],&g_370[0][3][1],&g_370[1][2][5]},{&g_370[0][3][3],&g_370[1][1][5],(void*)0,&g_20,(void*)0}}};
                        int32_t l_371 = 1L;
                        int i, j, k;
                        (*l_179) = ((((*l_353) = &g_197) != l_354) >= (safe_div_func_uint32_t_u_u((((((*l_363) = (&g_325 != ((*l_362) = l_359))) , &g_165) == l_364) || (((*l_365) = l_330) , (g_370[0][3][1] = (p_99 == ((safe_rshift_func_int16_t_s_s(((l_368[1][0] | (-1L)) != g_148[1][0]), g_148[4][6])) , g_93[0]))))), l_371)));
                        if (p_101)
                            break;
                    }
                    else
                    { /* block id: 150 */
                        uint64_t l_377 = 0xEBC46D2E92B18FB1LL;
                        uint64_t *l_382 = &g_383;
                        int16_t *l_390 = &g_165;
                        int32_t l_392 = (-2L);
                        g_391 = (((+p_100) > (safe_sub_func_int32_t_s_s((safe_div_func_uint64_t_u_u((l_377 == (l_381 |= (((p_100 > p_99) != ((*l_189) ^= p_101)) >= (safe_unary_minus_func_uint8_t_u((safe_mul_func_uint16_t_u_u(65535UL, 0xB696L))))))), ((*l_382)++))), (safe_mod_func_uint32_t_u_u((safe_div_func_uint16_t_u_u(0x8C91L, (((((*l_390) ^= p_101) == g_361.f1) < g_4[9]) , g_361.f1))), 0xFFCAD721L))))) , &g_16);
                        ++l_393;
                    }
                }
                for (l_381 = 0; (l_381 <= 0); l_381 += 1)
                { /* block id: 161 */
                    int i, j, k;
                    if ((*g_391))
                        break;
                }
                for (l_352 = 0; (l_352 <= 1); l_352 += 1)
                { /* block id: 166 */
                    uint32_t *l_406 = (void*)0;
                    uint32_t *l_407 = &g_408;
                    uint64_t *l_435 = &g_190[4][2];
                    int32_t l_449 = 0L;
                    int32_t l_452[2];
                    int16_t l_479 = 8L;
                    uint16_t *l_485[8][9] = {{(void*)0,(void*)0,&g_37,&g_276[4],&l_330,&g_84,&l_482,&l_330,&l_482},{&g_276[0],&l_330,(void*)0,(void*)0,&l_330,&g_276[0],&l_330,&g_276[0],&g_276[0]},{&g_276[0],&l_482,&g_323,&g_323,&g_276[2],&g_276[4],&g_37,(void*)0,&g_37},{&g_276[2],&g_276[0],&g_276[2],&g_276[0],&g_84,&g_84,&l_330,&g_84,(void*)0},{&l_330,(void*)0,(void*)0,&l_482,&g_37,&g_37,&l_482,(void*)0,(void*)0},{&g_276[2],&g_276[2],&l_482,&g_276[0],&g_276[0],(void*)0,(void*)0,&g_276[0],&g_37},{&g_37,&g_84,&g_276[4],&g_37,&g_276[0],(void*)0,(void*)0,&g_276[0],&g_276[0]},{&g_276[4],&g_276[2],&l_330,(void*)0,(void*)0,&l_482,&g_37,&g_37,&l_482}};
                    uint16_t *l_487 = (void*)0;
                    uint16_t **l_486 = &l_487;
                    uint16_t l_495 = 65532UL;
                    float ***l_497 = &l_496;
                    const int32_t l_498[10][10] = {{(-4L),1L,0L,0xB74016A3L,0L,1L,(-4L),(-4L),1L,0L},{1L,(-4L),(-4L),1L,0L,0xB74016A3L,0L,1L,(-4L),(-4L)},{0L,(-4L),0xA284B800L,1L,1L,0xA284B800L,(-4L),0L,(-4L),0xA284B800L},{0xB74016A3L,1L,1L,1L,0xB74016A3L,0xA284B800L,0xA284B800L,0xB74016A3L,1L,1L},{0L,0L,1L,0xB74016A3L,(-4L),0xB74016A3L,1L,0L,0L,1L},{1L,0xB74016A3L,0xA284B800L,0xA284B800L,0xB74016A3L,1L,1L,1L,0xB74016A3L,0xA284B800L},{(-4L),0L,(-4L),0xA284B800L,1L,1L,0xA284B800L,(-4L),0L,(-4L)},{(-4L),1L,0L,0xB74016A3L,0L,1L,(-4L),(-4L),1L,0L},{1L,0xB74016A3L,0xB74016A3L,(-4L),(-4L),0xA284B800L,(-4L),(-4L),0xB74016A3L,0xB74016A3L},{(-4L),0xB74016A3L,1L,0L,0L,1L,0xB74016A3L,(-4L),0xB74016A3L,1L}};
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_452[i] = 1L;
                    if (((((*l_189) = ((((((safe_mul_func_int8_t_s_s(l_368[l_352][l_352], (safe_mul_func_int16_t_s_s((safe_div_func_int32_t_s_s(((*l_175) = ((l_404 = ((l_350 != &g_143) , l_402)) != l_405)), ((*l_407) &= g_148[3][1]))), (safe_mul_func_uint16_t_u_u((((safe_add_func_uint16_t_u_u((((!g_190[4][1]) > (safe_rshift_func_uint8_t_u_u(((*l_180) >= (g_383 & 4294967294UL)), p_101))) ^ (-7L)), g_113)) , p_101) & p_99), p_101)))))) & l_393) ^ l_352) & l_368[l_352][l_352]) & l_330) < 0x1182DDDC65759DA7LL)) , g_113) , 0x0728016DL))
                    { /* block id: 171 */
                        uint16_t *l_418 = &g_276[1];
                        const float * const ***l_443 = (void*)0;
                        float **l_446 = &l_272[3];
                        float ***l_445[8][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_446,(void*)0,&l_446,&l_446,&l_446},{&l_446,&l_446,(void*)0,&l_446,&l_446},{(void*)0,(void*)0,&l_446,&l_446,(void*)0},{&l_446,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_446,&l_446,(void*)0,(void*)0},{&l_446,(void*)0,&l_446,&l_446,&l_446},{(void*)0,(void*)0,(void*)0,(void*)0,&l_446}};
                        float ****l_444 = &l_445[3][2];
                        int i, j;
                        (*l_173) = ((safe_mul_func_uint16_t_u_u(((*l_418)--), (l_423 >= (((g_424[2][7][0] = (void*)0) != l_434) != ((l_368[l_352][l_352] != ((&g_190[1][1] != l_435) != (safe_mod_func_uint32_t_u_u((safe_mod_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(1L, (((((void*)0 == &g_426[0][6][1]) | g_190[1][2]) ^ g_370[1][6][3]) >= 0xE8996AB2L))), l_368[l_352][l_352])), 0x817E4CFDL)))) == l_352))))) ^ l_442[1][0]);
                        (*l_444) = (void*)0;
                        (**l_434) = (void*)0;
                    }
                    else
                    { /* block id: 177 */
                        int64_t l_448 = 7L;
                        int32_t l_450 = (-8L);
                        const uint32_t *l_469 = &g_470[6];
                        const uint32_t **l_468 = &l_469;
                        uint16_t *l_480 = (void*)0;
                        uint16_t *l_481 = &g_276[0];
                        g_455--;
                        (*l_166) = ((safe_mod_func_int16_t_s_s((0xD1L == ((((*l_481) = (+(((safe_lshift_func_uint16_t_u_u((safe_unary_minus_func_int32_t_s(l_447)), 6)) || ((p_99 <= ((safe_rshift_func_int16_t_s_u((((safe_sub_func_int64_t_s_s(((l_454[3][5] ^= (&g_227 == ((0x7F44L != ((((*l_468) = &g_143) != ((safe_mod_func_int8_t_s_s(((~p_101) | (safe_rshift_func_int8_t_s_s((~(((safe_sub_func_int32_t_s_s(((void*)0 != l_294[0][6][4]), 0xA638798CL)) <= (-7L)) != g_431.f1)), l_451))), 0xA3L)) , &g_143)) || (-1L))) , &g_227))) | l_448), l_330)) , p_99) >= g_470[6]), 1)) ^ l_479)) < l_451)) != 1L))) || p_101) < 0x032A804D0E4DCF67LL)), 0x9EE0L)) != 0x8B6D5382L);
                        l_482++;
                    }
                    g_11 = ((0xA.05B87Cp-47 == ((((*p_98) , l_485[1][6]) != ((*l_486) = &l_482)) != (((((safe_mul_func_int8_t_s_s((safe_sub_func_uint16_t_u_u((+((safe_sub_func_uint8_t_u_u((g_324[7] , (((0xC1L <= p_99) , (l_495 , ((*l_497) = l_496))) != (void*)0)), g_228[4])) >= 1UL)), g_431.f1)), g_4[5])) != 0x6FL) != p_100) , p_101) > g_370[0][3][1]))) <= l_498[9][8]);
                }
                g_499--;
            }
            for (l_352 = 0; (l_352 <= 1); l_352 += 1)
            { /* block id: 193 */
                uint16_t l_505 = 0x90A1L;
                int32_t l_509[7][2] = {{(-2L),(-2L)},{8L,(-2L)},{(-2L),8L},{(-2L),(-2L)},{8L,(-2L)},{(-2L),8L},{(-2L),(-2L)}};
                int i, j;
                for (l_451 = 1; (l_451 >= 0); l_451 -= 1)
                { /* block id: 196 */
                    for (l_169 = 1; (l_169 >= 0); l_169 -= 1)
                    { /* block id: 199 */
                        int32_t l_504[6][1][4];
                        int i, j, k;
                        for (i = 0; i < 6; i++)
                        {
                            for (j = 0; j < 1; j++)
                            {
                                for (k = 0; k < 4; k++)
                                    l_504[i][j][k] = 0L;
                            }
                        }
                        (*l_173) = (((void*)0 == &l_327) , ((*l_180) = (safe_lshift_func_int16_t_s_s(g_432[0][1].f1, 11))));
                        --l_505;
                        if (p_99)
                            goto lbl_291;
                    }
                }
                if ((*g_391))
                    break;
                l_511--;
            }
        }
    }
    l_180 = (void*)0;
    return p_99;
}


/* ------------------------------------------ */
/* 
 * reads : g_16 g_143 g_20 g_93 g_113 g_4 g_37 g_84 g_149 g_165 g_148
 * writes: g_143 g_37 g_148 g_149 g_93 g_165 g_4
 */
static int8_t * func_102(int32_t * p_103, uint32_t  p_104, uint16_t  p_105, int64_t  p_106)
{ /* block id: 29 */
    int32_t *l_112[9][7] = {{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]},{&g_4[9],&g_4[9],(void*)0,&g_4[9],(void*)0,&g_4[9],&g_4[9]}};
    uint64_t l_114 = 9UL;
    int64_t *l_135 = &g_93[0];
    int64_t **l_134 = &l_135;
    uint32_t *l_142 = &g_143;
    uint16_t *l_144 = &g_37;
    uint8_t *l_145 = (void*)0;
    uint8_t *l_146 = (void*)0;
    uint8_t *l_147 = &g_148[3][1];
    int16_t *l_162 = (void*)0;
    int16_t *l_163 = (void*)0;
    int16_t *l_164 = &g_165;
    int i, j;
    --l_114;
    g_149 ^= ((p_105 ^ (safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((*l_147) = ((safe_mul_func_int16_t_s_s(p_105, ((safe_rshift_func_uint16_t_u_s(((*l_144) = (safe_add_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s(((!(((((safe_rshift_func_int8_t_s_u((&p_106 != ((*l_134) = &g_93[1])), 4)) & ((safe_div_func_int32_t_s_s((*p_103), g_16)) >= ((safe_mul_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_s((((p_106 && (((0x310A9C07L < ((*l_142) |= 0xA5CC0B3DL)) , g_20) , (-1L))) <= p_105) < g_93[0]), g_113)) | g_4[0]), 0x016FL)) != p_104))) && g_37) >= p_105) | p_106)) > p_106), g_113)), g_16)), 0x7CL))), g_93[0])) <= 1L))) != 0x92L)), g_84)), 0xD39CL))) ^ p_104);
    g_4[9] |= (((safe_add_func_int16_t_s_s(p_105, (p_104 || 0xA5DCL))) ^ (safe_add_func_int64_t_s_s((safe_sub_func_int32_t_s_s(0L, ((*l_142) = ((-1L) >= (safe_div_func_uint64_t_u_u(((((((*l_135) = 0x6EEDD9E1A7EA209BLL) != (p_106 = (g_37 || (safe_div_func_uint32_t_u_u(((safe_add_func_int16_t_s_s(((*l_164) &= 0xE8ABL), g_20)) & g_148[3][2]), g_148[3][1]))))) , &l_135) != &l_135) > g_113), p_104)))))), p_105))) , 4L);
    return &g_20;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_4[i], "g_4[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_11, sizeof(g_11), "g_11", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc_bytes(&g_13[i][j], sizeof(g_13[i][j]), "g_13[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_37, "g_37", print_hash_value);
    transparent_crc(g_84, "g_84", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_93[i], "g_93[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_113, "g_113", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_148[i][j], "g_148[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_149, "g_149", print_hash_value);
    transparent_crc(g_165, "g_165", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_190[i][j], "g_190[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_227, "g_227", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_228[i], "g_228[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_276[i], "g_276[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_323, "g_323", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_324[i], "g_324[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_326.f0, "g_326.f0", print_hash_value);
    transparent_crc(g_326.f1, "g_326.f1", print_hash_value);
    transparent_crc(g_326.f2, "g_326.f2", print_hash_value);
    transparent_crc(g_361.f0, "g_361.f0", print_hash_value);
    transparent_crc(g_361.f1, "g_361.f1", print_hash_value);
    transparent_crc(g_361.f2, "g_361.f2", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_370[i][j][k], "g_370[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_383, "g_383", print_hash_value);
    transparent_crc(g_408, "g_408", print_hash_value);
    transparent_crc(g_427.f0, "g_427.f0", print_hash_value);
    transparent_crc(g_427.f1, "g_427.f1", print_hash_value);
    transparent_crc(g_427.f2, "g_427.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_428[i][j].f0, "g_428[i][j].f0", print_hash_value);
            transparent_crc(g_428[i][j].f1, "g_428[i][j].f1", print_hash_value);
            transparent_crc(g_428[i][j].f2, "g_428[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_429.f0, "g_429.f0", print_hash_value);
    transparent_crc(g_429.f1, "g_429.f1", print_hash_value);
    transparent_crc(g_429.f2, "g_429.f2", print_hash_value);
    transparent_crc(g_430.f0, "g_430.f0", print_hash_value);
    transparent_crc(g_430.f1, "g_430.f1", print_hash_value);
    transparent_crc(g_430.f2, "g_430.f2", print_hash_value);
    transparent_crc(g_431.f0, "g_431.f0", print_hash_value);
    transparent_crc(g_431.f1, "g_431.f1", print_hash_value);
    transparent_crc(g_431.f2, "g_431.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_432[i][j].f0, "g_432[i][j].f0", print_hash_value);
            transparent_crc(g_432[i][j].f1, "g_432[i][j].f1", print_hash_value);
            transparent_crc(g_432[i][j].f2, "g_432[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_433.f0, "g_433.f0", print_hash_value);
    transparent_crc(g_433.f1, "g_433.f1", print_hash_value);
    transparent_crc(g_433.f2, "g_433.f2", print_hash_value);
    transparent_crc(g_455, "g_455", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_470[i], "g_470[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_499, "g_499", print_hash_value);
    transparent_crc(g_515, "g_515", print_hash_value);
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc(g_573, "g_573", print_hash_value);
    transparent_crc(g_601, "g_601", print_hash_value);
    transparent_crc(g_681.f0, "g_681.f0", print_hash_value);
    transparent_crc(g_681.f1, "g_681.f1", print_hash_value);
    transparent_crc(g_681.f4, "g_681.f4", print_hash_value);
    transparent_crc(g_682.f1, "g_682.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_684[i][j].f0, "g_684[i][j].f0", print_hash_value);
            transparent_crc(g_684[i][j].f1, "g_684[i][j].f1", print_hash_value);
            transparent_crc(g_684[i][j].f4, "g_684[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_910, "g_910", print_hash_value);
    transparent_crc(g_995, "g_995", print_hash_value);
    transparent_crc(g_1050, "g_1050", print_hash_value);
    transparent_crc(g_1160, "g_1160", print_hash_value);
    transparent_crc(g_1230, "g_1230", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_1257[i][j][k], "g_1257[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1387, "g_1387", print_hash_value);
    transparent_crc(g_1497, "g_1497", print_hash_value);
    transparent_crc(g_1515.f0, "g_1515.f0", print_hash_value);
    transparent_crc(g_1515.f1, "g_1515.f1", print_hash_value);
    transparent_crc(g_1515.f2, "g_1515.f2", print_hash_value);
    transparent_crc_bytes (&g_1522, sizeof(g_1522), "g_1522", print_hash_value);
    transparent_crc(g_1523, "g_1523", print_hash_value);
    transparent_crc(g_1536, "g_1536", print_hash_value);
    transparent_crc(g_1599.f0, "g_1599.f0", print_hash_value);
    transparent_crc(g_1599.f1, "g_1599.f1", print_hash_value);
    transparent_crc(g_1599.f2, "g_1599.f2", print_hash_value);
    transparent_crc(g_1609.f0, "g_1609.f0", print_hash_value);
    transparent_crc(g_1609.f1, "g_1609.f1", print_hash_value);
    transparent_crc(g_1609.f4, "g_1609.f4", print_hash_value);
    transparent_crc(g_1732.f0, "g_1732.f0", print_hash_value);
    transparent_crc(g_1732.f1, "g_1732.f1", print_hash_value);
    transparent_crc(g_1732.f4, "g_1732.f4", print_hash_value);
    transparent_crc(g_1735.f0, "g_1735.f0", print_hash_value);
    transparent_crc(g_1735.f1, "g_1735.f1", print_hash_value);
    transparent_crc(g_1735.f4, "g_1735.f4", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1745[i][j], "g_1745[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1828, sizeof(g_1828), "g_1828", print_hash_value);
    transparent_crc(g_1880.f0, "g_1880.f0", print_hash_value);
    transparent_crc(g_1880.f1, "g_1880.f1", print_hash_value);
    transparent_crc(g_1880.f4, "g_1880.f4", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1882[i][j].f0, "g_1882[i][j].f0", print_hash_value);
            transparent_crc(g_1882[i][j].f1, "g_1882[i][j].f1", print_hash_value);
            transparent_crc(g_1882[i][j].f4, "g_1882[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2015, "g_2015", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2031[i].f0, "g_2031[i].f0", print_hash_value);
        transparent_crc(g_2031[i].f1, "g_2031[i].f1", print_hash_value);
        transparent_crc(g_2031[i].f2, "g_2031[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2277, "g_2277", print_hash_value);
    transparent_crc(g_2320, "g_2320", print_hash_value);
    transparent_crc(g_2324.f0, "g_2324.f0", print_hash_value);
    transparent_crc(g_2324.f1, "g_2324.f1", print_hash_value);
    transparent_crc(g_2324.f2, "g_2324.f2", print_hash_value);
    transparent_crc(g_2376, "g_2376", print_hash_value);
    transparent_crc(g_2384, "g_2384", print_hash_value);
    transparent_crc(g_2426, "g_2426", print_hash_value);
    transparent_crc(g_2443.f0, "g_2443.f0", print_hash_value);
    transparent_crc(g_2443.f1, "g_2443.f1", print_hash_value);
    transparent_crc(g_2443.f4, "g_2443.f4", print_hash_value);
    transparent_crc(g_2457.f0, "g_2457.f0", print_hash_value);
    transparent_crc(g_2457.f1, "g_2457.f1", print_hash_value);
    transparent_crc(g_2457.f4, "g_2457.f4", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2488[i], "g_2488[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2493.f0, "g_2493.f0", print_hash_value);
    transparent_crc(g_2493.f1, "g_2493.f1", print_hash_value);
    transparent_crc(g_2493.f4, "g_2493.f4", print_hash_value);
    transparent_crc(g_2528.f0, "g_2528.f0", print_hash_value);
    transparent_crc(g_2528.f1, "g_2528.f1", print_hash_value);
    transparent_crc(g_2528.f4, "g_2528.f4", print_hash_value);
    transparent_crc(g_2567.f0, "g_2567.f0", print_hash_value);
    transparent_crc(g_2567.f1, "g_2567.f1", print_hash_value);
    transparent_crc(g_2567.f2, "g_2567.f2", print_hash_value);
    transparent_crc(g_2571.f0, "g_2571.f0", print_hash_value);
    transparent_crc(g_2571.f1, "g_2571.f1", print_hash_value);
    transparent_crc(g_2571.f2, "g_2571.f2", print_hash_value);
    transparent_crc(g_2599, "g_2599", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_2610[i][j][k], "g_2610[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2617.f0, "g_2617.f0", print_hash_value);
    transparent_crc(g_2617.f1, "g_2617.f1", print_hash_value);
    transparent_crc(g_2617.f4, "g_2617.f4", print_hash_value);
    transparent_crc(g_2623.f0, "g_2623.f0", print_hash_value);
    transparent_crc(g_2623.f1, "g_2623.f1", print_hash_value);
    transparent_crc(g_2623.f2, "g_2623.f2", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2646[i][j][k], "g_2646[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 754
   depth: 1, occurrence: 2
XXX total union variables: 5

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 18
breakdown:
   indirect level: 0, occurrence: 5
   indirect level: 1, occurrence: 9
   indirect level: 2, occurrence: 2
   indirect level: 3, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 12
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 5
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 5

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 358
   depth: 2, occurrence: 101
   depth: 3, occurrence: 7
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 15, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 4
   depth: 20, occurrence: 1
   depth: 21, occurrence: 6
   depth: 22, occurrence: 4
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 4
   depth: 26, occurrence: 1
   depth: 27, occurrence: 4
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 6
   depth: 33, occurrence: 4
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 2
   depth: 41, occurrence: 4

XXX total number of pointers: 604

XXX times a variable address is taken: 1770
XXX times a pointer is dereferenced on RHS: 183
breakdown:
   depth: 1, occurrence: 147
   depth: 2, occurrence: 24
   depth: 3, occurrence: 12
XXX times a pointer is dereferenced on LHS: 260
breakdown:
   depth: 1, occurrence: 241
   depth: 2, occurrence: 12
   depth: 3, occurrence: 7
XXX times a pointer is compared with null: 55
XXX times a pointer is compared with address of another variable: 19
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 10014

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1574
   level: 2, occurrence: 284
   level: 3, occurrence: 119
   level: 4, occurrence: 23
   level: 5, occurrence: 3
XXX number of pointers point to pointers: 273
XXX number of pointers point to scalars: 308
XXX number of pointers point to structs: 12
XXX percent of pointers has null in alias set: 30.5
XXX average alias set size: 1.47

XXX times a non-volatile is read: 1926
XXX times a non-volatile is write: 946
XXX times a volatile is read: 34
XXX    times read thru a pointer: 18
XXX times a volatile is write: 15
XXX    times written thru a pointer: 11
XXX times a volatile is available for access: 1.36e+03
XXX percentage of non-volatile access: 98.3

XXX forward jumps: 3
XXX backward jumps: 12

XXX stmts: 363
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 38
   depth: 2, occurrence: 49
   depth: 3, occurrence: 66
   depth: 4, occurrence: 79
   depth: 5, occurrence: 97

XXX percentage a fresh-made variable is used: 16.2
XXX percentage an existing variable is used: 83.8
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


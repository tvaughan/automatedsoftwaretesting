/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3294056687
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_6[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint32_t g_55 = 0x50041AC5L;
static uint8_t g_57[10] = {1UL,248UL,1UL,1UL,248UL,1UL,1UL,248UL,1UL,1UL};
static uint32_t g_59[3] = {1UL,1UL,1UL};
static int64_t g_109[10][7][3] = {{{0xF243A45608AF67B5LL,0L,0x77049B5B57A05E82LL},{0xBC345E11D1409FA7LL,0xCED03213856DD1CDLL,8L},{0x7A7163A7FA890769LL,0xCED03213856DD1CDLL,0x4F93C24CE628F5DBLL},{0xD9FC0A4C35ED1F7FLL,0L,1L},{2L,(-9L),0xF243A45608AF67B5LL},{0xC79244252728FC8ELL,0xDD4A0C5CC1C00ABCLL,0xC79244252728FC8ELL},{0x5A8EECE75099C322LL,1L,(-1L)}},{{0L,0x82FEEDE604A327E9LL,0xBC345E11D1409FA7LL},{0L,9L,(-9L)},{5L,0xE7498C5F844B9EA6LL,(-3L)},{0L,0L,2L},{1L,0x7A7163A7FA890769LL,0xE7498C5F844B9EA6LL},{4L,0x401126FE5016CB79LL,1L},{1L,(-9L),0L}},{{(-1L),0x82FEEDE604A327E9LL,(-4L)},{(-1L),4L,0xF243A45608AF67B5LL},{0x5A8EECE75099C322LL,(-4L),0xF243A45608AF67B5LL},{0x23D7E63684A2D252LL,6L,(-4L)},{9L,0xC79244252728FC8ELL,0L},{0x77049B5B57A05E82LL,1L,1L},{0xF243A45608AF67B5LL,9L,0xE7498C5F844B9EA6LL}},{{(-1L),0xF243A45608AF67B5LL,2L},{9L,0x9BF173AA3328DBD8LL,(-3L)},{6L,0x9807A6E463D7759BLL,(-1L)},{(-2L),0x9BF173AA3328DBD8LL,0x23D7E63684A2D252LL},{0L,0xF243A45608AF67B5LL,0xCED03213856DD1CDLL},{0x9807A6E463D7759BLL,9L,1L},{0x401126FE5016CB79LL,1L,9L}},{{0L,0xC79244252728FC8ELL,0x85461A87E759864ELL},{0xC79244252728FC8ELL,6L,(-9L)},{0xE9DA84259BBF2B79LL,(-4L),0x13AC178791A53EA2LL},{0xE9DA84259BBF2B79LL,4L,0L},{0xC79244252728FC8ELL,0x82FEEDE604A327E9LL,0x5A8EECE75099C322LL},{0L,(-9L),9L},{0x401126FE5016CB79LL,0x401126FE5016CB79LL,0xD9FC0A4C35ED1F7FLL}},{{0x9807A6E463D7759BLL,0x7A7163A7FA890769LL,0L},{0L,0L,(-1L)},{(-2L),0xE7498C5F844B9EA6LL,6L},{6L,0L,(-1L)},{9L,(-1L),0L},{(-1L),0x85461A87E759864ELL,0xD9FC0A4C35ED1F7FLL},{0xF243A45608AF67B5LL,0xE9DA84259BBF2B79LL,9L}},{{0x77049B5B57A05E82LL,(-1L),0x5A8EECE75099C322LL},{9L,0L,0L},{0x23D7E63684A2D252LL,0x4F93C24CE628F5DBLL,0x13AC178791A53EA2LL},{0x5A8EECE75099C322LL,0x4F93C24CE628F5DBLL,(-9L)},{(-1L),0L,0x85461A87E759864ELL},{(-1L),(-1L),9L},{1L,0xE9DA84259BBF2B79LL,1L}},{{4L,0x85461A87E759864ELL,0xCED03213856DD1CDLL},{1L,(-1L),0x23D7E63684A2D252LL},{0L,0L,(-1L)},{5L,0xE7498C5F844B9EA6LL,(-3L)},{0L,0L,2L},{1L,0x7A7163A7FA890769LL,0xE7498C5F844B9EA6LL},{4L,0x401126FE5016CB79LL,1L}},{{1L,(-9L),0L},{(-1L),0x82FEEDE604A327E9LL,(-4L)},{(-1L),4L,0xF243A45608AF67B5LL},{0x5A8EECE75099C322LL,(-4L),0xF243A45608AF67B5LL},{0x23D7E63684A2D252LL,6L,(-4L)},{9L,0xC79244252728FC8ELL,0L},{0x77049B5B57A05E82LL,1L,1L}},{{0xF243A45608AF67B5LL,9L,0xE7498C5F844B9EA6LL},{(-1L),0xF243A45608AF67B5LL,2L},{9L,0x9BF173AA3328DBD8LL,(-3L)},{6L,0x9807A6E463D7759BLL,(-1L)},{(-2L),0x9BF173AA3328DBD8LL,0x23D7E63684A2D252LL},{0L,0xF243A45608AF67B5LL,0xCED03213856DD1CDLL},{0x9807A6E463D7759BLL,9L,1L}}};
static int32_t g_116 = 0x6E4BE99DL;
static int32_t * const  volatile g_115 = &g_116;/* VOLATILE GLOBAL g_115 */
static int8_t g_125 = 0xB2L;
static uint64_t g_129 = 18446744073709551615UL;
static int16_t g_144 = 0L;
static uint32_t g_151 = 1UL;
static int32_t g_152 = (-7L);
static volatile float g_153 = 0x8.830F48p-93;/* VOLATILE GLOBAL g_153 */
static volatile int16_t g_155[9][3][8] = {{{0xA22EL,1L,9L,1L,(-10L),0x154FL,(-7L),0x16F8L},{0x3233L,0xB0C8L,0x3F73L,0L,5L,(-6L),0x3233L,0xD4A8L},{0L,0L,2L,0xB0C8L,0x154FL,(-3L),0xE996L,0L}},{{0xF6A3L,0L,1L,0x2FE4L,(-1L),0x2CD2L,0x2FE4L,1L},{0xF5C4L,0L,0L,0L,5L,0x3233L,0L,0xD2DBL},{7L,0xF872L,0xF5C4L,5L,9L,0x2478L,0xF477L,0x2478L}},{{0L,0xF6A3L,0x2CD2L,0xF6A3L,0L,0x2E49L,0x625EL,1L},{0x154FL,0xE996L,0xD6EFL,9L,4L,0x3F73L,2L,0xF6A3L},{0xD4A8L,0L,0xD6EFL,0xB0C8L,(-6L),0xD6EFL,0x625EL,2L}},{{4L,7L,0x2CD2L,1L,0L,(-10L),0xF477L,0L},{(-3L),0x2FE4L,0xF5C4L,(-1L),1L,9L,0L,0x8926L},{0x625EL,0x3233L,0L,(-1L),9L,0xD6EFL,0x2FE4L,(-1L)}},{{0x8926L,0x625EL,1L,0xF477L,0x998BL,0xA22EL,0xE996L,0xE996L},{(-1L),0xE996L,2L,2L,0xE996L,(-1L),0x3233L,0L},{0x625EL,0L,0x3F73L,7L,0L,0x2478L,4L,(-6L)}},{{0x998BL,0L,0x2557L,7L,0L,0x16F8L,0xD2DBL,0L},{(-1L),0L,0L,2L,0xB0C8L,0x154FL,(-3L),0xE996L},{0xD4A8L,0xD6EFL,0xF872L,0xF477L,(-1L),(-3L),0x998BL,(-1L)}},{{(-1L),0xF477L,0x04BAL,(-1L),0L,0x4F3CL,0xD2DBL,0x8926L},{0xF872L,0xB0C8L,0x2478L,(-1L),0x2FE4L,0L,(-1L),0L},{7L,(-6L),0x3F73L,1L,0x3F73L,(-6L),7L,2L}},{{0L,0xF477L,0x26EDL,0xB0C8L,(-1L),0x6645L,0xE996L,0xF6A3L},{0L,0L,0L,9L,(-1L),0x154FL,9L,1L},{0L,0xF5C4L,0L,0xF6A3L,0x3F73L,0x3233L,1L,0x2478L}},{{7L,0L,(-1L),5L,0x2FE4L,0x2557L,0xF5C4L,0x3233L},{(-1L),0L,(-7L),0x04BAL,0L,1L,1L,2L},{0L,0x325BL,0x2E49L,0x2478L,0x4F3CL,(-1L),0x154FL,0x04BAL}}};
static int64_t g_158 = (-9L);
static int32_t g_160[1][2][1] = {{{0L},{0L}}};
static int64_t g_162 = (-1L);
static int32_t g_164[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static volatile float g_165[5][6] = {{0x0.6p+1,0x0.9p-1,0x0.9p-1,0x0.6p+1,0x5.8AE8A4p-45,0xF.196D43p-88},{0xF.196D43p-88,0x0.6p+1,0x0.9p+1,0x0.6p+1,0xF.196D43p-88,0xB.8B9B3Cp+16},{0x0.6p+1,0xF.196D43p-88,0xB.8B9B3Cp+16,0xB.8B9B3Cp+16,0xF.196D43p-88,0x0.6p+1},{0x0.9p-1,0x0.6p+1,0x5.8AE8A4p-45,0xF.196D43p-88,0x5.8AE8A4p-45,0x0.6p+1},{0x5.8AE8A4p-45,0x0.9p-1,0xB.8B9B3Cp+16,0x0.9p+1,0x0.9p+1,0xB.8B9B3Cp+16}};
static uint32_t g_166 = 0x6CAFF2A1L;
static const uint32_t g_201 = 0UL;
static uint8_t *g_219[1][1][2] = {{{&g_57[5],&g_57[5]}}};
static int32_t ** volatile * volatile g_240 = (void*)0;/* VOLATILE GLOBAL g_240 */
static uint16_t g_243 = 65529UL;
static uint16_t g_244 = 0UL;
static float g_247 = 0x0.FDA571p+79;
static int16_t g_249 = 0x0444L;
static volatile float g_259 = (-0x4.0p-1);/* VOLATILE GLOBAL g_259 */
static volatile uint64_t g_260 = 6UL;/* VOLATILE GLOBAL g_260 */
static volatile int16_t g_265[5][4] = {{0x5AEAL,2L,0x5AEAL,2L},{0x5AEAL,2L,0x5AEAL,2L},{0x5AEAL,2L,0x5AEAL,2L},{0x5AEAL,2L,0x5AEAL,2L},{0x5AEAL,2L,0x5AEAL,2L}};
static volatile uint32_t g_266[1] = {0xAE4827AFL};
static int16_t g_271[9] = {2L,2L,2L,2L,2L,2L,2L,2L,2L};
static const uint32_t g_311 = 1UL;
static int32_t **g_333[2] = {(void*)0,(void*)0};
static uint32_t g_341 = 18446744073709551609UL;
static int8_t *g_469[1][10] = {{&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125}};
static int32_t *g_479 = &g_152;
static volatile int16_t g_512 = 0xF95DL;/* VOLATILE GLOBAL g_512 */
static volatile int16_t *g_511 = &g_512;
static volatile int16_t **g_510 = &g_511;
static int32_t *g_516 = &g_116;
static int32_t ** volatile g_515[10][6] = {{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,(void*)0,&g_516,&g_516,&g_516,&g_516},{&g_516,(void*)0,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,(void*)0,&g_516,&g_516,&g_516,&g_516}};
static int32_t ** volatile g_517[3][8] = {{&g_516,&g_516,(void*)0,&g_516,&g_516,(void*)0,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516},{&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516}};
static int32_t ** volatile g_518 = &g_516;/* VOLATILE GLOBAL g_518 */
static float g_521 = (-0x10.9p+1);
static int16_t g_547[2] = {0x72DDL,0x72DDL};
static float g_550 = 0x1.Ep-1;
static float * volatile g_549 = &g_550;/* VOLATILE GLOBAL g_549 */
static int32_t g_562[9] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
static const int32_t g_582 = 5L;
static uint64_t g_629 = 1UL;
static int16_t *g_792 = &g_271[4];
static int16_t **g_791[3][5] = {{&g_792,&g_792,&g_792,&g_792,&g_792},{&g_792,&g_792,&g_792,&g_792,&g_792},{&g_792,&g_792,&g_792,&g_792,&g_792}};
static int16_t ***g_790 = &g_791[2][1];
static volatile uint32_t g_798 = 0UL;/* VOLATILE GLOBAL g_798 */
static volatile uint16_t g_806 = 1UL;/* VOLATILE GLOBAL g_806 */
static int32_t g_810[9][2][7] = {{{(-8L),0x43B63CEDL,0xB6D6D8D9L,0xBDBAF770L,0xD1EFE268L,0xBDBAF770L,0xB6D6D8D9L},{0x00CB310AL,0x00CB310AL,0xA4249E4AL,(-2L),0xDD347EA0L,(-6L),(-2L)}},{{2L,0x216C86E6L,0x6B46FEC6L,0x43B63CEDL,0x6B46FEC6L,0x216C86E6L,2L},{7L,(-2L),0xDDA5289AL,7L,0xDD347EA0L,(-7L),(-7L)}},{{0xD1EFE268L,6L,0L,6L,0xD1EFE268L,(-1L),0L},{0xDD347EA0L,7L,0xDDA5289AL,(-2L),7L,3L,0xC2D47142L}},{{0x6B46FEC6L,0x43B63CEDL,0x6B46FEC6L,0x216C86E6L,2L,0xBDBAF770L,0x6B46FEC6L},{0xDD347EA0L,(-2L),0xA4249E4AL,0x00CB310AL,0x00CB310AL,0xA4249E4AL,(-2L)}},{{0xD1EFE268L,0xBDBAF770L,0xB6D6D8D9L,0x43B63CEDL,(-8L),0xBDBAF770L,0L},{7L,0x00CB310AL,(-7L),0xDDA5289AL,0xDD347EA0L,3L,0xDDA5289AL}},{{2L,6L,1L,0x43B63CEDL,2L,(-1L),2L},{0x00CB310AL,0xDDA5289AL,0xDDA5289AL,0x00CB310AL,3L,(-7L),0xC2D47142L}},{{(-8L),6L,0xB6D6D8D9L,0x216C86E6L,0xD1EFE268L,0x216C86E6L,0xB6D6D8D9L},{0xDD347EA0L,0x00CB310AL,0x15A44F02L,(-2L),0x00CB310AL,(-6L),0xC2D47142L}},{{2L,0xBDBAF770L,0x6B46FEC6L,6L,0x6B46FEC6L,0xBDBAF770L,2L},{3L,(-2L),(-7L),7L,0x00CB310AL,(-7L),0xDDA5289AL}},{{0xD1EFE268L,0x43B63CEDL,0L,0x43B63CEDL,0xD1EFE268L,0x2411D1E9L,0L},{0x00CB310AL,7L,(-7L),(-2L),3L,3L,(-2L)}}};
static int64_t g_898[5][3] = {{0x0074C8BB121E7583LL,0x536E1ADCBC37BFA8LL,(-1L)},{0x34768FD08DBF327ALL,0x536E1ADCBC37BFA8LL,0x34768FD08DBF327ALL},{0x04C7CEB3B7946B6ELL,0x0074C8BB121E7583LL,(-1L)},{0x04C7CEB3B7946B6ELL,0x04C7CEB3B7946B6ELL,0x0074C8BB121E7583LL},{0x34768FD08DBF327ALL,0x0074C8BB121E7583LL,0x0074C8BB121E7583LL}};
static float * volatile g_912 = &g_521;/* VOLATILE GLOBAL g_912 */
static int32_t ** volatile g_954 = (void*)0;/* VOLATILE GLOBAL g_954 */
static int32_t ** volatile g_955 = &g_516;/* VOLATILE GLOBAL g_955 */
static int16_t * volatile g_956 = &g_271[6];/* VOLATILE GLOBAL g_956 */
static const int16_t g_960[3][5] = {{0x0EBFL,0xA43AL,0x423BL,0x423BL,0xA43AL},{0x0EBFL,0xA43AL,0x423BL,0x423BL,0xA43AL},{0x0EBFL,0xA43AL,0x423BL,0x423BL,0xA43AL}};
static const int16_t *g_959 = &g_960[0][4];
static volatile uint32_t g_1059 = 1UL;/* VOLATILE GLOBAL g_1059 */
static int64_t g_1125 = 0xE2FE4FB1B14335A8LL;
static const volatile uint32_t g_1126 = 1UL;/* VOLATILE GLOBAL g_1126 */
static int32_t g_1141[1][7][7] = {{{8L,0xA6784435L,(-4L),0x216BC297L,(-3L),0x3FBB56D8L,(-3L)},{0x6060E457L,3L,3L,0x6060E457L,0xF4A72B88L,0x6060E457L,3L},{8L,0x3FBB56D8L,0x0D9AD59FL,0xA6784435L,0x0D9AD59FL,0x3FBB56D8L,8L},{3L,0x6060E457L,0xF4A72B88L,0x6060E457L,3L,3L,0x6060E457L},{(-4L),0x29D1D919L,(-4L),0x71AFF342L,0x0D9AD59FL,0x216BC297L,2L},{0x6060E457L,1L,0xF4A72B88L,0xF4A72B88L,1L,0x6060E457L,1L},{(-4L),0x71AFF342L,0x0D9AD59FL,0x216BC297L,2L,0x216BC297L,0x0D9AD59FL}}};
static uint32_t *g_1157 = &g_55;
static int32_t ** volatile g_1178 = &g_516;/* VOLATILE GLOBAL g_1178 */
static uint8_t g_1210[6] = {255UL,255UL,255UL,255UL,255UL,255UL};
static uint8_t g_1211 = 0xD8L;
static uint8_t g_1212 = 255UL;
static int32_t ** const  volatile g_1269 = (void*)0;/* VOLATILE GLOBAL g_1269 */
static int32_t ** volatile g_1270[5] = {&g_516,&g_516,&g_516,&g_516,&g_516};
static int32_t ** volatile g_1271[9] = {&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516,&g_516};
static int32_t ** volatile g_1273 = &g_516;/* VOLATILE GLOBAL g_1273 */
static uint16_t *g_1311 = &g_243;
static int8_t g_1581 = (-1L);
static int16_t g_1582[5] = {0x1830L,0x1830L,0x1830L,0x1830L,0x1830L};
static int32_t ** volatile g_1649 = &g_516;/* VOLATILE GLOBAL g_1649 */
static const volatile int16_t g_1788 = (-1L);/* VOLATILE GLOBAL g_1788 */
static volatile int16_t g_1837 = 0xFBECL;/* VOLATILE GLOBAL g_1837 */
static int32_t ** volatile g_1839 = &g_516;/* VOLATILE GLOBAL g_1839 */
static volatile uint32_t *g_1848 = &g_266[0];
static volatile uint32_t ** const  volatile g_1847 = &g_1848;/* VOLATILE GLOBAL g_1847 */
static const uint64_t g_1887 = 0x8C4F438A5379BCA0LL;
static int32_t ***g_1900 = &g_333[1];
static int32_t ****g_1899 = &g_1900;
static int32_t ***** volatile g_1898[4][5] = {{&g_1899,(void*)0,(void*)0,&g_1899,(void*)0},{&g_1899,&g_1899,&g_1899,&g_1899,&g_1899},{(void*)0,&g_1899,(void*)0,(void*)0,&g_1899},{&g_1899,(void*)0,(void*)0,&g_1899,(void*)0}};
static int32_t ***** volatile g_1901 = (void*)0;/* VOLATILE GLOBAL g_1901 */
static int32_t ***** volatile g_1902 = &g_1899;/* VOLATILE GLOBAL g_1902 */
static const int32_t *g_1944 = (void*)0;
static const int32_t ** volatile g_1943 = &g_1944;/* VOLATILE GLOBAL g_1943 */
static const int32_t ** volatile * const  volatile g_1942[6] = {&g_1943,&g_1943,&g_1943,&g_1943,&g_1943,&g_1943};
static volatile uint64_t g_1953[1] = {0UL};
static float g_1979 = (-0x1.Ep-1);
static volatile float g_2065 = 0x8.3p+1;/* VOLATILE GLOBAL g_2065 */
static volatile float g_2066 = 0x2.5p-1;/* VOLATILE GLOBAL g_2066 */
static volatile float *g_2064[4] = {&g_2065,&g_2065,&g_2065,&g_2065};
static volatile float * volatile *g_2063 = &g_2064[1];
static volatile int64_t g_2095 = 0L;/* VOLATILE GLOBAL g_2095 */
static const uint32_t g_2159 = 6UL;
static uint32_t g_2169 = 9UL;
static volatile int64_t g_2180 = 0xEDE998CD1C0E9874LL;/* VOLATILE GLOBAL g_2180 */
static volatile int16_t g_2188 = (-1L);/* VOLATILE GLOBAL g_2188 */
static volatile int32_t g_2190 = 0x58B66466L;/* VOLATILE GLOBAL g_2190 */
static uint16_t g_2212 = 1UL;
static const uint8_t g_2292 = 252UL;
static uint8_t **g_2346 = &g_219[0][0][0];
static uint8_t ***g_2345 = &g_2346;
static uint8_t *** const *g_2344 = &g_2345;
static const int32_t ** const  volatile g_2354 = (void*)0;/* VOLATILE GLOBAL g_2354 */
static const int32_t *g_2355 = &g_6[5];
static uint64_t *g_2376 = &g_129;
static uint64_t **g_2375 = &g_2376;
static uint64_t ***g_2374 = &g_2375;
static volatile int32_t g_2387 = 0xDB37E611L;/* VOLATILE GLOBAL g_2387 */
static float g_2411 = (-0x1.2p+1);
static int32_t ** const *g_2477 = &g_333[1];
static int32_t ** const **g_2476 = &g_2477;
static int32_t ** const ***g_2475 = &g_2476;
static uint8_t ****g_2593[5][7] = {{(void*)0,&g_2345,(void*)0,(void*)0,&g_2345,(void*)0,(void*)0},{&g_2345,&g_2345,&g_2345,&g_2345,&g_2345,&g_2345,&g_2345},{&g_2345,(void*)0,(void*)0,&g_2345,(void*)0,(void*)0,&g_2345},{&g_2345,&g_2345,&g_2345,&g_2345,&g_2345,&g_2345,&g_2345},{&g_2345,&g_2345,&g_2345,&g_2345,&g_2345,&g_2345,&g_2345}};
static uint8_t ***** volatile g_2592 = &g_2593[2][4];/* VOLATILE GLOBAL g_2592 */
static const int8_t ***g_2602 = (void*)0;
static const int8_t g_2608 = 1L;
static uint8_t *****g_2613 = &g_2593[3][0];
static uint8_t g_2615 = 0UL;
static uint64_t g_2634 = 18446744073709551609UL;
static volatile int16_t g_2657[4][3] = {{7L,7L,7L},{7L,7L,7L},{7L,7L,7L},{7L,7L,7L}};
static float *g_2792 = &g_2411;
static float **g_2791[6] = {&g_2792,&g_2792,&g_2792,&g_2792,&g_2792,&g_2792};
static float ***g_2790 = &g_2791[3];
static int32_t *****g_2865[8] = {&g_1899,&g_1899,&g_1899,&g_1899,&g_1899,&g_1899,&g_1899,&g_1899};
static uint64_t g_2867 = 0UL;
static int8_t **g_2945 = &g_469[0][0];
static int8_t ***g_2944 = &g_2945;
static int8_t **** const g_2943 = &g_2944;
static int8_t **** const *g_2942 = &g_2943;
static uint16_t g_2950 = 0xD0D9L;
static uint64_t g_2989 = 1UL;
static int16_t * const *g_3006 = (void*)0;
static int16_t * const **g_3005[7][2][8] = {{{&g_3006,&g_3006,(void*)0,(void*)0,&g_3006,&g_3006,&g_3006,(void*)0},{(void*)0,(void*)0,&g_3006,(void*)0,&g_3006,&g_3006,&g_3006,&g_3006}},{{&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006},{&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,(void*)0}},{{(void*)0,(void*)0,&g_3006,&g_3006,&g_3006,(void*)0,&g_3006,&g_3006},{&g_3006,&g_3006,&g_3006,(void*)0,(void*)0,&g_3006,&g_3006,&g_3006}},{{&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,(void*)0,&g_3006,&g_3006},{&g_3006,(void*)0,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,(void*)0}},{{&g_3006,&g_3006,&g_3006,(void*)0,(void*)0,&g_3006,&g_3006,&g_3006},{&g_3006,&g_3006,&g_3006,(void*)0,&g_3006,(void*)0,&g_3006,&g_3006}},{{&g_3006,(void*)0,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006,&g_3006},{&g_3006,&g_3006,&g_3006,&g_3006,(void*)0,&g_3006,&g_3006,&g_3006}},{{&g_3006,&g_3006,&g_3006,&g_3006,(void*)0,&g_3006,&g_3006,(void*)0},{&g_3006,&g_3006,&g_3006,&g_3006,(void*)0,(void*)0,(void*)0,&g_3006}}};
static int16_t * const **g_3007 = &g_3006;
static const int32_t ** volatile g_3039 = &g_2355;/* VOLATILE GLOBAL g_3039 */
static uint32_t *g_3044 = &g_59[1];
static uint32_t **g_3043 = &g_3044;
static volatile uint32_t g_3057 = 2UL;/* VOLATILE GLOBAL g_3057 */
static int16_t g_3090 = 0x19DFL;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static const uint64_t  func_9(const int32_t * p_10, int32_t * p_11);
static const int32_t * func_12(int32_t  p_13, uint16_t  p_14, uint32_t  p_15, uint64_t  p_16);
static float  func_20(float  p_21);
static float  func_31(int8_t  p_32, int32_t * p_33, uint32_t  p_34);
static int8_t  func_35(uint16_t  p_36, float  p_37, int32_t * p_38);
static float  func_41(int32_t * p_42, int32_t * p_43, uint8_t  p_44);
static uint8_t  func_48(uint32_t  p_49, float  p_50, int32_t * p_51, uint64_t  p_52, int32_t  p_53);
static int32_t ** func_69(uint8_t  p_70, uint16_t  p_71);
static uint32_t * func_77(int16_t  p_78, const int16_t  p_79, uint8_t  p_80);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_55 g_57 g_59 g_115 g_116 g_109 g_129 g_151 g_166 g_152 g_160 g_164 g_240 g_144 g_249 g_158 g_260 g_266 g_271 g_125 g_247 g_162 g_155 g_243 g_333 g_341 g_201 g_265 g_469 g_510 g_518 g_244 g_547 g_549 g_550 g_629 g_521 g_511 g_512 g_516 g_810 g_912 g_311 g_2063 g_2064 g_2066 g_2065 g_1581 g_1210 g_2212 g_1141 g_2159 g_1311 g_1848 g_1649 g_1157 g_2376 g_790 g_791 g_792 g_2355 g_2374 g_2375 g_2592 g_2602 g_2292 g_2615 g_1212 g_562 g_2634 g_2657 g_959 g_960 g_1839 g_1847 g_1902 g_1899 g_1900 g_1887 g_2790 g_2792 g_2411 g_1211 g_2345 g_2188 g_2791 g_2867 g_2346 g_219 g_479 g_2942 g_2344 g_2944 g_2945 g_2950 g_1582 g_2943 g_3090 g_3039
 * writes: g_6 g_55 g_57 g_59 g_109 g_116 g_125 g_129 g_144 g_151 g_166 g_219 g_243 g_244 g_249 g_260 g_266 g_247 g_162 g_164 g_333 g_341 g_155 g_152 g_158 g_165 g_479 g_516 g_521 g_550 g_259 g_2212 g_1581 g_1141 g_1210 g_271 g_2593 g_792 g_2602 g_2613 g_2066 g_2065 g_1212 g_2634 g_898 g_2615 g_2346 g_2865 g_2790 g_2942 g_1582 g_2950 g_2355
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_2 = 1UL;
    int32_t *l_45 = &g_6[3];
    uint32_t l_2203 = 5UL;
    uint8_t ****l_2590[3];
    int16_t * const l_2595 = (void*)0;
    int32_t l_2631 = 0x425F3536L;
    int32_t l_2632 = 5L;
    int32_t l_2633[2];
    int32_t l_2639 = 0xC3F42D8EL;
    int32_t ****l_2703[2];
    uint32_t l_2779 = 1UL;
    float * const l_2789 = &g_1979;
    float * const *l_2788 = &l_2789;
    float * const **l_2787[10][10][2] = {{{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0},{&l_2788,&l_2788}},{{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788}},{{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,(void*)0},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788}},{{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788}},{{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788}},{{(void*)0,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788}},{{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788}},{{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0}},{{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788}},{{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{(void*)0,&l_2788},{&l_2788,&l_2788},{&l_2788,&l_2788},{&l_2788,(void*)0}}};
    int8_t **l_2933 = &g_469[0][0];
    int8_t *****l_2941 = (void*)0;
    uint64_t ***l_2947 = (void*)0;
    uint8_t l_3074[8] = {0x2EL,0x2EL,0x2EL,0x2EL,0x2EL,0x2EL,0x2EL,0x2EL};
    int16_t l_3089 = 5L;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_2590[i] = (void*)0;
    for (i = 0; i < 2; i++)
        l_2633[i] = 7L;
    for (i = 0; i < 2; i++)
        l_2703[i] = &g_1900;
    if (l_2)
    { /* block id: 1 */
        int8_t l_19[8][4][7] = {{{0xDCL,0L,0x20L,0x38L,0x65L,0x1BL,6L},{0xB7L,0xA8L,(-6L),0xC9L,0L,6L,0L},{0L,0x21L,0x6CL,0xD7L,(-1L),0xD5L,0L},{0x4AL,8L,7L,0xA2L,0xBEL,(-9L),6L}},{{0xDDL,0x21L,9L,0x1BL,9L,0x21L,0xDDL},{0xC9L,0xA8L,6L,0xBEL,0xB1L,(-1L),0xA4L},{6L,0L,(-5L),0x21L,(-8L),0xD5L,0L},{(-3L),(-10L),6L,7L,0x4AL,0xB7L,3L}},{{0xCBL,0x14L,9L,0xA9L,0xDCL,0x1BL,0x3AL},{(-9L),0x03L,7L,(-3L),(-9L),0xA8L,0xA4L},{0xEEL,0xD5L,0x6CL,0xA9L,0L,0xA9L,0x6CL},{0xB1L,0xB1L,(-6L),7L,(-10L),1L,6L}},{{6L,0xA9L,0x20L,0x21L,1L,(-1L),(-2L)},{(-9L),(-6L),0x03L,0xBEL,(-10L),3L,0L},{(-8L),5L,0x6FL,0x14L,(-1L),(-1L),(-8L)},{8L,0xB1L,6L,0x03L,0xB7L,0xB1L,0x80L}},{{0xDCL,0x21L,0xCBL,0xD5L,0x3AL,(-1L),0x3AL},{0xB1L,0xA2L,0xA2L,0xB1L,0L,(-1L),0x03L},{(-5L),0x01L,(-8L),0xA9L,0x6FL,0x38L,0L},{0L,8L,6L,(-6L),1L,(-3L),0x03L}},{{6L,5L,1L,5L,6L,0x21L,0x3AL},{0x4AL,(-1L),0x80L,0xC9L,(-10L),(-6L),0x80L},{0L,0x1BL,(-5L),0xA9L,0L,0x14L,(-8L)},{0x4AL,0xC9L,0xA4L,0xA4L,0xC9L,0x4AL,3L}},{{6L,0L,0xD6L,0xD5L,9L,0x01L,0x65L},{0L,0L,0xA8L,(-3L),(-10L),6L,7L},{(-5L),0L,0xEEL,0x14L,0xEEL,0L,(-5L)},{0xB1L,0xC9L,7L,3L,1L,0xB7L,0xA2L}},{{0xDCL,0x1BL,0x3AL,0L,1L,0x01L,0xDDL},{8L,(-1L),7L,0L,0L,7L,(-1L)},{0x6FL,5L,0xEEL,0x21L,0L,0x14L,0x6CL},{0xB7L,8L,0xA8L,0x80L,0xB7L,0xC9L,0xA2L}}};
        int32_t *l_46 = &g_6[5];
        int32_t **l_47 = &l_46;
        uint32_t *l_54 = &g_55;
        uint32_t *l_58 = &g_59[0];
        uint8_t *l_60 = (void*)0;
        uint8_t *l_61 = (void*)0;
        uint8_t *l_62 = (void*)0;
        uint8_t *l_63 = &g_57[3];
        float *l_913 = &g_550;
        int32_t l_2616 = (-4L);
        int32_t l_2627 = 0x61065C35L;
        int32_t l_2630[9] = {0L,0xE33FEA3FL,0xE33FEA3FL,0L,0xE33FEA3FL,0xE33FEA3FL,0L,0xE33FEA3FL,0xE33FEA3FL};
        int8_t l_2655 = 0x9DL;
        uint32_t l_2656 = 0xBEBB7DCFL;
        int32_t l_2682 = 0L;
        int32_t l_2686 = 0x40A174A5L;
        int16_t l_2717 = 0x3ABBL;
        float ***l_2795[9][1][7] = {{{(void*)0,&g_2791[3],&g_2791[3],(void*)0,&g_2791[4],&g_2791[3],&g_2791[5]}},{{&g_2791[3],(void*)0,&g_2791[3],&g_2791[3],&g_2791[3],&g_2791[3],&g_2791[3]}},{{(void*)0,(void*)0,&g_2791[3],(void*)0,&g_2791[3],&g_2791[3],&g_2791[5]}},{{(void*)0,&g_2791[3],&g_2791[3],(void*)0,&g_2791[4],&g_2791[3],&g_2791[5]}},{{&g_2791[3],(void*)0,&g_2791[3],&g_2791[3],&g_2791[3],&g_2791[3],&g_2791[3]}},{{(void*)0,(void*)0,&g_2791[3],(void*)0,&g_2791[3],&g_2791[3],&g_2791[5]}},{{(void*)0,&g_2791[3],&g_2791[3],(void*)0,&g_2791[4],&g_2791[3],&g_2791[5]}},{{&g_2791[3],(void*)0,&g_2791[3],&g_2791[3],&g_2791[3],&g_2791[3],&g_2791[3]}},{{(void*)0,(void*)0,(void*)0,&g_2791[3],&g_2791[5],&g_2791[1],&g_2791[3]}}};
        int64_t *l_2807 = &g_898[3][1];
        int i, j, k;
lbl_2589:
        for (l_2 = (-7); (l_2 > 58); ++l_2)
        { /* block id: 4 */
            int32_t *l_5 = &g_6[1];
            (*l_5) = 0x7B1C2A2CL;
        }
        if ((safe_mul_func_int8_t_s_s(((func_9(func_12((((safe_sub_func_float_f_f(((l_19[7][3][6] != func_20((g_6[5] , (((((+l_19[7][3][6]) < ((g_6[3] <= (safe_add_func_float_f_f((safe_mul_func_float_f_f((((l_2 && (((safe_div_func_float_f_f(func_31(func_35(l_2, ((safe_sub_func_float_f_f(((*l_913) = (func_41(l_45, ((*l_47) = l_46), ((*l_63) = (((*l_58) = (func_48(((*l_54) = (&g_6[5] != (void*)0)), g_6[1], l_54, (*l_45), (*l_45)) <= g_6[1])) , 0x77L))) == (*l_45))), g_311)) > 0x1.5p-1), l_45), l_45, (*l_45)), g_810[8][0][6])) , l_45) != l_54)) != 0UL) , (*l_46)), g_1581)), g_1210[2]))) != l_2203)) , 0x2.FAA963p-87) < 0x4.6F2633p+4) >= 0x8.1606DCp-71)))) < g_201), l_19[4][0][6])) < (*l_45)) , (*l_45)), g_2159, (*l_45), (*l_45)), l_45) == (-5L)) || (**l_47)), l_2203)))
        { /* block id: 1016 */
            if (g_55)
                goto lbl_2589;
        }
        else
        { /* block id: 1018 */
            int8_t ***l_2601 = (void*)0;
            for (l_2203 = 1; (l_2203 <= 8); l_2203 += 1)
            { /* block id: 1021 */
                int32_t l_2594[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
                int i;
                for (g_151 = 1; (g_151 <= 8); g_151 += 1)
                { /* block id: 1024 */
                    int64_t *l_2600 = &g_109[9][1][2];
                    const int8_t ****l_2603 = &g_2602;
                    const int8_t *l_2607 = &g_2608;
                    const int8_t **l_2606 = &l_2607;
                    const int8_t ***l_2605 = &l_2606;
                    const int8_t ****l_2604 = &l_2605;
                    int32_t l_2614[5] = {(-10L),(-10L),(-10L),(-10L),(-10L)};
                    int i;
                    if ((*g_115))
                    { /* block id: 1025 */
                        uint8_t *****l_2591 = &l_2590[2];
                        int i;
                        (*l_46) ^= 0x069987C9L;
                        (*g_2592) = ((*l_2591) = l_2590[1]);
                    }
                    else
                    { /* block id: 1029 */
                        return l_2594[5];
                    }
                    (*l_913) = ((((**g_790) = (**g_790)) == l_2595) >= l_2594[5]);
                    (**g_2063) = ((safe_div_func_float_f_f((*g_912), (((((((*l_913) = (safe_div_func_float_f_f(l_2594[5], (((((((*l_2600) |= 0xA586DC91FEF6223DLL) <= (l_2601 != ((*l_2604) = ((*l_2603) = g_2602)))) , ((safe_sub_func_uint16_t_u_u((safe_div_func_int8_t_s_s(((((g_2613 = &g_2593[2][4]) != (l_2614[0] , &g_2593[2][4])) || (**g_2375)) <= 0x25CAL), l_2594[5])), 0xF6E8L)) , g_166)) , (void*)0) != &g_1311) >= 0x8.83879Ap-19)))) != 0x3.701DE3p-9) < 0x2.FA6EACp+52) >= 0x9.36F5E3p-97) != g_2292) != g_1141[0][3][6]))) <= g_2615);
                    (*l_47) = (*l_47);
                }
            }
        }
        for (g_1212 = 0; (g_1212 <= 2); g_1212 += 1)
        { /* block id: 1046 */
            int32_t *l_2617 = &g_1141[0][6][3];
            int32_t l_2618 = 0x85C68C13L;
            int32_t *l_2619 = &g_6[1];
            int32_t *l_2620 = (void*)0;
            int32_t *l_2621 = (void*)0;
            int32_t *l_2622 = &g_6[1];
            int32_t *l_2623 = &g_116;
            int32_t *l_2624 = &g_6[1];
            int32_t *l_2625 = &l_2616;
            int32_t *l_2626 = &g_6[1];
            int32_t *l_2628 = &g_6[4];
            int32_t *l_2629[2][10] = {{&g_1141[0][0][3],&g_1141[0][0][3],&l_2627,&g_116,&l_2627,&g_1141[0][0][3],&g_1141[0][0][3],&l_2627,&g_116,&l_2627},{&g_1141[0][0][3],&g_1141[0][0][3],&l_2627,&g_116,&l_2627,&g_1141[0][0][3],&g_1141[0][0][3],&l_2627,&g_116,&l_2627}};
            int32_t ****l_2653 = (void*)0;
            uint16_t l_2658 = 65533UL;
            int64_t *l_2659[6] = {&g_898[0][2],&g_898[0][2],&g_898[0][2],&g_898[0][2],&g_898[0][2],&g_898[0][2]};
            int32_t l_2685 = 0xD9D0AFB7L;
            uint32_t l_2691 = 0xC61D6EBAL;
            int8_t *l_2726 = (void*)0;
            const uint16_t l_2742 = 1UL;
            int32_t **l_2759 = &l_2629[1][8];
            int32_t **l_2765 = &g_479;
            int8_t * const *l_2786 = &l_2726;
            int8_t * const **l_2785 = &l_2786;
            int8_t * const ***l_2784 = &l_2785;
            uint32_t l_2832 = 18446744073709551615UL;
            uint8_t **l_2837 = &l_60;
            int i, j;
            (**g_2063) = ((*l_913) = g_562[(g_1212 + 2)]);
            ++g_2634;
            if ((g_562[(g_1212 + 5)] , (0x166DECCDC1A76C9ELL >= (g_59[g_1212] == (*l_46)))))
            { /* block id: 1050 */
                if (l_2639)
                    break;
            }
            else
            { /* block id: 1052 */
                const float l_2640 = 0x1.E605D8p+58;
                (*l_2617) &= (**l_47);
            }
            if (((safe_add_func_int64_t_s_s((g_898[0][0] = (((safe_div_func_float_f_f((safe_sub_func_float_f_f(((*l_913) = (safe_div_func_float_f_f((((*l_45) , &g_1157) != (((safe_div_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(((l_2653 == (void*)0) , (**l_47)), (!((0x35B6A63FL | ((*l_58) &= ((l_2655 , 0x609C3E4026AA75E0LL) <= ((*l_2623) ^ (*l_2619))))) != l_2656)))), (-1L))) , g_2657[2][1]) , &g_1157)), g_271[4]))), g_1210[1])), l_2658)) <= g_547[0]) , (-2L))), (*l_46))) > (*g_959)))
            { /* block id: 1058 */
                uint32_t l_2666 = 1UL;
                int8_t *l_2669 = (void*)0;
                int8_t *l_2670 = &g_125;
                int32_t l_2671 = 0L;
                (*l_2617) &= (safe_div_func_uint8_t_u_u((*l_2626), ((((safe_lshift_func_int8_t_s_u((l_2671 = ((*l_2670) = (safe_sub_func_int8_t_s_s(l_2666, (++(*l_63)))))), (((((-1L) >= ((safe_sub_func_uint8_t_u_u(0x50L, (safe_mul_func_uint16_t_u_u(7UL, (safe_rshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_s(((*g_792) = ((247UL | 0x97L) == ((((safe_mul_func_int16_t_s_s(((((0x90E24B46L || ((void*)0 == (*g_1839))) <= l_2666) && (*l_45)) == 1L), (***g_790))) , 0xD7FCL) , (*l_45)) , 0x2E2FL))), l_2682)), 7)))))) < 0UL)) | 0x231953091A97FFEDLL) ^ (*l_45)) , 0xBEL))) | (*l_45)) ^ (-1L)) ^ l_2666)));
            }
            else
            { /* block id: 1064 */
                int64_t l_2683 = 1L;
                int32_t l_2684 = 0L;
                int32_t l_2687 = 1L;
                int32_t l_2688 = (-8L);
                int32_t l_2689 = 0x47E5D712L;
                int32_t l_2690 = 0xAD66BCDCL;
                int32_t *****l_2701 = (void*)0;
                int32_t l_2746 = 2L;
                uint16_t l_2747 = 5UL;
                --l_2691;
                if ((((*l_2628) , (++(*g_2376))) , (safe_lshift_func_uint16_t_u_u(((*l_46) , l_2689), 2))))
                { /* block id: 1067 */
                    uint32_t l_2698 = 1UL;
                    for (l_2687 = 0; (l_2687 <= 2); l_2687 += 1)
                    { /* block id: 1070 */
                        l_2698++;
                        return (**l_47);
                    }
                    if ((*l_45))
                        break;
                    for (l_2618 = 2; (l_2618 >= 0); l_2618 -= 1)
                    { /* block id: 1077 */
                        int32_t ****l_2702 = &g_1900;
                        int16_t *** const l_2706 = &g_791[2][1];
                        (*l_2617) ^= ((*l_2625) = ((*l_2628) = ((((*l_45) || l_2698) , (l_2701 = &g_1899)) == (((l_2702 != l_2703[1]) != ((safe_add_func_uint32_t_u_u(0x112CF292L, ((l_2706 != ((((safe_mul_func_uint16_t_u_u(((((((safe_rshift_func_int8_t_s_s(((safe_sub_func_uint32_t_u_u(((g_6[1] , ((safe_lshift_func_int16_t_s_u(((safe_mod_func_uint16_t_u_u(((((**l_47) && 0xC9L) <= l_2717) , (**l_47)), (*g_1311))) && 0xA3202DDA089AD293LL), 14)) , (*g_1311))) , (**g_1847)), (**l_47))) , 0x03L), 7)) <= (**l_47)) < 1L) && l_2698) < l_2698) == l_2698), 0x81C7L)) || 0L) >= 0x89L) , &g_510)) & 0x3A88789CL))) && 0xC4B219D7L)) , &g_2476))));
                    }
                    for (g_341 = 0; (g_341 <= 2); g_341 += 1)
                    { /* block id: 1085 */
                        int32_t l_2741 = 0x5EBF6670L;
                        int8_t l_2743 = 0x16L;
                        float *l_2744[4];
                        int8_t l_2745 = 0x3DL;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_2744[i] = &g_247;
                        (*g_912) = ((safe_div_func_float_f_f(((*l_913) = (*g_912)), (safe_div_func_float_f_f((safe_sub_func_float_f_f(l_2698, (l_2745 = (((safe_add_func_float_f_f(l_2698, g_125)) == (((((void*)0 == l_2726) , ((((safe_div_func_float_f_f((((safe_sub_func_float_f_f(((safe_sub_func_float_f_f(((safe_div_func_float_f_f(((safe_add_func_float_f_f(((safe_add_func_float_f_f(g_55, (0xE.195D10p+96 < (safe_div_func_float_f_f((((g_1141[0][6][1] , l_2698) > l_2741) == l_2742), 0x2.1p-1))))) < (-0x1.9p+1)), g_1141[0][0][3])) < g_166), l_2741)) > g_160[0][1][0]), g_129)) != g_2292), g_151)) < (-0x9.Ep+1)) == l_2743), g_164[3])) <= (*l_46)) < g_59[0]) < g_201)) != l_2741) , g_1581)) == g_2159)))), l_2741)))) < g_1210[3]);
                    }
                }
                else
                { /* block id: 1090 */
                    float l_2751[9][4][6] = {{{0x0.5p-1,0xD.3D2DEFp+89,(-0x6.Bp-1),0x7.C2916Cp-39,(-0x2.1p+1),0xC.0417E4p+47},{0x0.4p+1,0x1.50BD0Fp-17,0x1.6p+1,0xD.3D2DEFp+89,0x0.Bp-1,0xC.0417E4p+47},{0x0.Bp-1,0x3.9B9665p-40,(-0x6.Bp-1),(-0x9.7p-1),0x5.D0A00Ep+99,0x6.E1EACDp-31},{0x0.Bp-1,0x2.71598Fp-74,0x0.6p+1,0xD.3D2DEFp+89,0x3.C37044p+0,0x1.Dp-1}},{{0x0.4p+1,0x2.71598Fp-74,0x4.E5F5EAp+97,0x7.C2916Cp-39,0x5.D0A00Ep+99,0x9.9p+1},{0x0.5p-1,0x3.9B9665p-40,0x4.E5F5EAp+97,0x1.50BD0Fp-17,0x0.Bp-1,0x1.Dp-1},{(-0x2.1p+1),0x1.50BD0Fp-17,0x0.6p+1,0x1.50BD0Fp-17,(-0x2.1p+1),0x6.E1EACDp-31},{0x0.5p-1,0xD.3D2DEFp+89,(-0x6.Bp-1),0x7.C2916Cp-39,(-0x2.1p+1),0xC.0417E4p+47}},{{0x0.4p+1,0x1.50BD0Fp-17,0x1.6p+1,0xD.3D2DEFp+89,0x0.Bp-1,0xC.0417E4p+47},{0x0.Bp-1,0x3.9B9665p-40,(-0x6.Bp-1),(-0x9.7p-1),0x5.D0A00Ep+99,0x6.E1EACDp-31},{0x0.Bp-1,0x2.71598Fp-74,0x0.6p+1,0xD.3D2DEFp+89,0x3.C37044p+0,0x1.Dp-1},{0x0.4p+1,0x2.71598Fp-74,0x4.E5F5EAp+97,0x7.C2916Cp-39,0x5.D0A00Ep+99,0x9.9p+1}},{{0x0.5p-1,0x3.9B9665p-40,0x4.E5F5EAp+97,0x1.50BD0Fp-17,0x0.Bp-1,0x1.Dp-1},{(-0x2.1p+1),0x1.50BD0Fp-17,0x0.6p+1,0x1.50BD0Fp-17,(-0x2.1p+1),0x6.E1EACDp-31},{0x0.5p-1,0xD.3D2DEFp+89,(-0x6.Bp-1),0x7.C2916Cp-39,(-0x2.1p+1),0xC.0417E4p+47},{0x0.4p+1,0x1.50BD0Fp-17,0x1.6p+1,0xD.3D2DEFp+89,0x0.Bp-1,0xC.0417E4p+47}},{{0x0.Bp-1,0x3.9B9665p-40,(-0x6.Bp-1),(-0x9.7p-1),0x5.D0A00Ep+99,0x6.E1EACDp-31},{0x0.Bp-1,0x2.71598Fp-74,0x0.6p+1,0xD.3D2DEFp+89,0x3.C37044p+0,0x1.Dp-1},{0x0.4p+1,0x2.71598Fp-74,0x4.E5F5EAp+97,0x7.C2916Cp-39,0x5.D0A00Ep+99,0x9.9p+1},{0x0.5p-1,0x3.9B9665p-40,0x4.E5F5EAp+97,0x1.50BD0Fp-17,0x0.Bp-1,0x1.Dp-1}},{{(-0x2.1p+1),0x1.50BD0Fp-17,0x0.6p+1,0x1.50BD0Fp-17,(-0x2.1p+1),0x6.E1EACDp-31},{0x0.5p-1,0xD.3D2DEFp+89,(-0x6.Bp-1),0x7.C2916Cp-39,(-0x2.1p+1),0xC.0417E4p+47},{0x0.4p+1,0x1.50BD0Fp-17,0x1.6p+1,0xD.3D2DEFp+89,0x0.Bp-1,0xC.0417E4p+47},{0x0.Bp-1,0x3.9B9665p-40,(-0x6.Bp-1),(-0x9.7p-1),0x5.D0A00Ep+99,0x6.E1EACDp-31}},{{0x0.Bp-1,0x2.71598Fp-74,0x0.6p+1,0xD.E8CEC6p-67,(-0x1.1p+1),0x7.C2916Cp-39},{0x1.2FEEEDp-22,0x5.Bp-1,(-0x2.1p+1),(-0x1.1p+1),0x1.Ep+1,0x2.71598Fp-74},{0x0.Ap-1,(-0x1.7p-1),(-0x2.1p+1),(-0x1.4p-1),0x9.Dp-1,0x7.C2916Cp-39},{0x3.6p+1,(-0x1.4p-1),0x0.Bp-1,(-0x1.4p-1),0x3.6p+1,(-0x9.7p-1)}},{{0x0.Ap-1,0xD.E8CEC6p-67,0x5.D0A00Ep+99,(-0x1.1p+1),0x3.6p+1,0x3.9B9665p-40},{0x1.2FEEEDp-22,(-0x1.4p-1),0x3.C37044p+0,0xD.E8CEC6p-67,0x9.Dp-1,0x3.9B9665p-40},{0x9.Dp-1,(-0x1.7p-1),0x5.D0A00Ep+99,(-0x4.9p+1),0x1.Ep+1,(-0x9.7p-1)},{0x9.Dp-1,0x5.Bp-1,0x0.Bp-1,0xD.E8CEC6p-67,(-0x1.1p+1),0x7.C2916Cp-39}},{{0x1.2FEEEDp-22,0x5.Bp-1,(-0x2.1p+1),(-0x1.1p+1),0x1.Ep+1,0x2.71598Fp-74},{0x0.Ap-1,(-0x1.7p-1),(-0x2.1p+1),(-0x1.4p-1),0x9.Dp-1,0x7.C2916Cp-39},{0x3.6p+1,(-0x1.4p-1),0x0.Bp-1,(-0x1.4p-1),0x3.6p+1,(-0x9.7p-1)},{0x0.Ap-1,0xD.E8CEC6p-67,0x5.D0A00Ep+99,(-0x1.1p+1),0x3.6p+1,0x3.9B9665p-40}}};
                    uint32_t l_2752 = 4294967295UL;
                    int i, j, k;
                    l_2747--;
                    for (l_2658 = 0; (l_2658 <= 2); l_2658 += 1)
                    { /* block id: 1094 */
                        int32_t l_2750[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2750[i] = 0x0BF76463L;
                        if ((*l_45))
                            break;
                        --l_2752;
                    }
                }
            }
            for (l_2717 = 2; (l_2717 >= 0); l_2717 -= 1)
            { /* block id: 1102 */
                uint64_t l_2774 = 18446744073709551606UL;
                float ****l_2793 = (void*)0;
                float ****l_2794[1];
                int32_t l_2796 = 0L;
                uint8_t **l_2838 = &l_61;
                int32_t ***l_2855 = &l_47;
                int i;
                for (i = 0; i < 1; i++)
                    l_2794[i] = &g_2790;
                if ((*l_2622))
                { /* block id: 1103 */
                    int16_t l_2755[10][2] = {{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL},{0x1E1CL,0x1E1CL}};
                    int32_t *l_2760 = (void*)0;
                    int32_t l_2775 = 0x1CF6F3E4L;
                    int i, j;
                    if (l_2755[8][1])
                        break;
                    for (g_125 = 0; (g_125 <= 2); g_125 += 1)
                    { /* block id: 1107 */
                        uint64_t l_2756[7] = {0xC76CB069B19D470CLL,0x7994664089043B08LL,0xC76CB069B19D470CLL,0xC76CB069B19D470CLL,0x7994664089043B08LL,0xC76CB069B19D470CLL,0xC76CB069B19D470CLL};
                        int i, j, k;
                        --l_2756[3];
                        return g_155[(g_1212 + 1)][g_1212][(g_1212 + 2)];
                    }
                    (**g_2063) = (((l_2759 == (***g_1902)) != ((((l_2760 == (void*)0) >= ((((safe_div_func_int64_t_s_s(((((*g_1311) ^ (safe_div_func_uint64_t_u_u(((&g_479 == l_2765) , 0x39C9717F21701322LL), (safe_lshift_func_uint8_t_u_s((++(*l_63)), 6))))) & (safe_div_func_uint32_t_u_u(((safe_mul_func_int8_t_s_s(l_2774, (*l_45))) , 9UL), g_1887))) , g_2634), (*g_2376))) , 0x0.E37394p-29) > 0xD.AFCAD7p+2) > g_2292)) < 0x0.Ap-1) < g_341)) <= g_6[1]);
                    for (l_2658 = 0; (l_2658 <= 2); l_2658 += 1)
                    { /* block id: 1115 */
                        uint32_t l_2776 = 0x17A7B131L;
                        l_2776++;
                    }
                }
                else
                { /* block id: 1118 */
                    --l_2779;
                    return l_2774;
                }
                l_2796 = ((((***g_790) = (((*l_45) != (g_898[2][1] = ((*l_46) != ((void*)0 != l_2784)))) , (*l_45))) , l_2787[0][4][1]) != (l_2795[1][0][4] = g_2790));
                for (g_2615 = 0; (g_2615 <= 2); g_2615 += 1)
                { /* block id: 1128 */
                    uint32_t l_2830 = 0x6F3CAE96L;
                    int32_t l_2834 = 0x310C8171L;
                    for (g_341 = 0; (g_341 <= 2); g_341 += 1)
                    { /* block id: 1131 */
                        int16_t **l_2814 = &g_792;
                        int16_t ***l_2813 = &l_2814;
                        const int32_t l_2831 = 0xE3100133L;
                        int32_t *l_2833 = &l_2618;
                        uint32_t l_2841 = 4294967295UL;
                        int64_t l_2842 = 0x9AAC09144F355A76LL;
                        uint32_t l_2843 = 1UL;
                        int i, j, k;
                        l_2833 = func_77((safe_unary_minus_func_int16_t_s(g_155[(l_2717 + 4)][l_2717][(l_2717 + 5)])), ((!((safe_mod_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s((safe_sub_func_int64_t_s_s(5L, (((safe_add_func_uint32_t_u_u(0UL, (l_2807 == (((safe_mod_func_int16_t_s_s((+((*g_790) == ((*l_2813) = ((safe_sub_func_float_f_f((*g_2792), 0x1.0p+1)) , (*g_790))))), (safe_mod_func_int8_t_s_s(((safe_div_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((((((+(safe_lshift_func_uint16_t_u_s(((((((safe_add_func_int16_t_s_s(((((safe_rshift_func_uint8_t_u_u(((*l_63) = ((*l_45) = l_2830)), l_2831)) == 0x8CL) < 0L) & 0x4C99A131L), 3UL)) > (*g_1311)) < (*g_1311)) >= 0x3C97L) && g_562[3]) && l_2831), (*g_959)))) >= g_1211) & 0x33L) != (-1L)) , 0L), l_2830)) , (*g_1311)), (-2L))) & g_160[0][1][0]), l_2832)))) <= (*g_2376)) , &g_162)))) , l_2831) , (-1L)))), l_2830)), 0x98837B24E157E18CLL)) <= g_243)) > l_2830), l_2830);
                        (*l_2624) |= ((l_2834 |= (*g_792)) == (((safe_sub_func_uint32_t_u_u(((*g_1311) != (((l_2830 ^ (((*g_2345) = &l_63) == (l_2838 = l_2837))) & ((*g_792) && ((g_1141[0][3][1] != (((*g_792) & (l_2841 = (((safe_mul_func_float_f_f((-0x1.4p-1), (*l_2625))) < 0x7.027813p-92) , 0L))) && (*g_1311))) & l_2842))) | 0xD02A0204L)), l_2843)) > (*l_2617)) , 65527UL));
                        (**l_2759) &= (+((***g_2374) < g_2188));
                    }
                    for (l_2682 = 7; (l_2682 >= 3); l_2682 -= 1)
                    { /* block id: 1145 */
                        int32_t l_2852 = 0L;
                        const int32_t l_2858[10][1][8] = {{{0x7C4D9C07L,0x16F52D66L,0L,0L,(-9L),(-9L),7L,0xADF67184L}},{{(-10L),0x208EBED7L,(-1L),(-9L),0L,(-9L),(-1L),0x208EBED7L}},{{0L,0x16F52D66L,0xADF67184L,0x2BFD5E07L,0x7C4D9C07L,0x0FC2DB7FL,(-9L),(-1L)}},{{0x208EBED7L,(-9L),6L,(-9L),0L,0L,(-9L),6L}},{{(-9L),(-9L),0xADF67184L,0x0FC2DB7FL,0L,0L,(-1L),(-10L)}},{{0L,0L,(-1L),(-10L),7L,6L,7L,(-10L)}},{{0L,0xADF67184L,0L,0x0FC2DB7FL,1L,0x208EBED7L,(-9L),6L}},{{0x2BFD5E07L,0x7C4D9C07L,0x0FC2DB7FL,(-9L),(-1L),1L,1L,(-1L)}},{{0x2BFD5E07L,7L,7L,0x2BFD5E07L,1L,(-9L),(-10L),0x208EBED7L}},{{0L,0L,(-9L),(-9L),7L,0xADF67184L,0x208EBED7L,0xADF67184L}}};
                        int i, j, k;
                        (*l_2623) &= (((safe_unary_minus_func_int64_t_s((((safe_sub_func_uint16_t_u_u(65535UL, (safe_mul_func_uint16_t_u_u((safe_add_func_uint8_t_u_u((g_57[g_2615] , ((*l_63) = g_57[g_2615])), (l_2852 = ((**l_47) < (**g_1847))))), (safe_rshift_func_int16_t_s_s(l_2830, ((((void*)0 != l_2855) , (safe_mod_func_uint8_t_u_u(l_2858[3][0][0], 0xAEL))) ^ 0L))))))) | 4294967287UL) ^ 0xCEL))) >= (***g_790)) ^ (*l_46));
                        (*l_913) = ((***g_2790) <= (safe_div_func_float_f_f(((+0x2.1p-1) != (*g_2792)), l_2834)));
                    }
                    for (l_2685 = 0; (l_2685 <= 2); l_2685 += 1)
                    { /* block id: 1153 */
                        if ((*l_2619))
                            break;
                    }
                }
            }
        }
    }
    else
    { /* block id: 1159 */
        int32_t l_2864[10] = {(-5L),0x179DA892L,(-5L),(-1L),(-1L),(-5L),0x179DA892L,(-5L),(-1L),(-1L)};
        int32_t l_2866 = 0xCFB48D39L;
        int8_t **l_2890 = &g_469[0][3];
        int8_t *** const l_2889[10][6] = {{(void*)0,(void*)0,&l_2890,&l_2890,&l_2890,(void*)0},{&l_2890,&l_2890,&l_2890,&l_2890,(void*)0,(void*)0},{&l_2890,&l_2890,&l_2890,&l_2890,&l_2890,(void*)0},{(void*)0,&l_2890,(void*)0,&l_2890,(void*)0,&l_2890},{&l_2890,(void*)0,&l_2890,&l_2890,(void*)0,&l_2890},{&l_2890,&l_2890,&l_2890,(void*)0,&l_2890,&l_2890},{&l_2890,&l_2890,&l_2890,(void*)0,(void*)0,&l_2890},{&l_2890,&l_2890,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2890,&l_2890,&l_2890,&l_2890,(void*)0,(void*)0},{&l_2890,&l_2890,&l_2890,&l_2890,&l_2890,(void*)0}};
        int8_t *** const *l_2888[1][4];
        int32_t l_2909 = 0x60383FB8L;
        int32_t *****l_2922 = &g_1899;
        uint64_t ***l_2948 = &g_2375;
        uint32_t l_2973 = 0x4608B9E9L;
        int32_t *l_3040 = &g_1141[0][0][3];
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 4; j++)
                l_2888[i][j] = &l_2889[7][2];
        }
        if ((l_2864[2] = (safe_div_func_int32_t_s_s(((l_2864[2] <= l_2864[2]) <= ((((void*)0 != &g_2374) ^ (g_810[4][0][0] , (l_2866 = ((((l_2864[0] , 253UL) , &l_2703[1]) == (g_2865[3] = &g_1899)) & 1L)))) , (*g_115))), g_2867))))
        { /* block id: 1163 */
            int64_t *l_2872 = (void*)0;
            uint32_t *l_2879[7][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
            int8_t *** const **l_2891 = &l_2888[0][2];
            float ****l_2892 = &g_2790;
            int32_t l_2903 = (-1L);
            int32_t l_2910 = (-1L);
            uint32_t l_2911 = 0UL;
            uint32_t l_2929 = 0x0B78484CL;
            int i, j;
            if ((safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((((((((g_158 = (l_2866 = (*l_45))) < (safe_lshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_u((safe_div_func_int64_t_s_s((((((g_151 = l_2864[2]) , (safe_add_func_int32_t_s_s((safe_rshift_func_int16_t_s_u(((((*g_2376) = (safe_mul_func_uint8_t_u_u((safe_div_func_int16_t_s_s(((((*l_2891) = l_2888[0][0]) != &g_2602) >= (((*l_2892) = &g_2791[1]) != ((safe_rshift_func_uint8_t_u_s(l_2864[3], 1)) , &l_2788))), (safe_sub_func_uint32_t_u_u(l_2864[6], (safe_mul_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((safe_div_func_int8_t_s_s(l_2903, (*l_45))), 0x5B68L)), l_2903)))))), l_2864[2]))) , (**g_510)) | (*g_792)), 10)), 7L))) && l_2864[0]) | l_2903) == (*g_1311)), l_2903)), 1)), (**g_2346)))) && l_2903) < 0x64L) < 0x1E3146B5L) <= 9L) != l_2864[9]) , l_2864[2]), l_2903)), (*l_45))))
            { /* block id: 1170 */
                int32_t *l_2904 = (void*)0;
                l_2904 = &l_2903;
                (*g_516) = l_2903;
            }
            else
            { /* block id: 1173 */
                int64_t l_2905 = 0x721ABED07AE28B91LL;
                int32_t l_2906 = 0x16A91E6CL;
                int32_t l_2907 = 4L;
                int32_t l_2908 = (-1L);
                --l_2911;
                (*l_45) ^= l_2864[3];
            }
            (*g_1839) = &l_2864[8];
            if (((safe_rshift_func_int8_t_s_s(((safe_rshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_u(((***g_790) >= ((l_2864[2] & ((&g_2476 != l_2922) >= (l_2911 || ((((safe_div_func_int8_t_s_s(((**l_2890) = ((***g_2374) && g_266[0])), 0xEEL)) | (0x9B14L & 0L)) > 0xB551ADFED57C63DELL) < l_2911)))) >= (***g_2374))), 2)), 6)) & (**g_2375)), 4)) && g_266[0]))
            { /* block id: 1179 */
                return (*g_479);
            }
            else
            { /* block id: 1181 */
                for (g_244 = 12; (g_244 != 5); g_244--)
                { /* block id: 1184 */
                    return l_2903;
                }
                for (l_2911 = 0; (l_2911 <= 59); l_2911 = safe_add_func_int32_t_s_s(l_2911, 9))
                { /* block id: 1189 */
                    return l_2929;
                }
            }
            l_2903 = (safe_sub_func_float_f_f((-l_2910), 0x9.FF6294p+51));
        }
        else
        { /* block id: 1194 */
            int8_t ***l_2940 = &l_2933;
            int8_t ****l_2939 = &l_2940;
            int8_t *****l_2938 = &l_2939;
            int16_t *l_2946 = &g_1582[2];
            int32_t l_2949[10] = {0L,0L,2L,0L,0L,2L,0L,0L,2L,0L};
            int64_t l_2957 = (-1L);
            int32_t *l_2996 = &g_116;
            int16_t * const *l_3004 = &l_2595;
            int16_t * const **l_3003 = &l_3004;
            uint32_t l_3024[7][5] = {{0xAFE85F97L,0xF7EFE41BL,0xF7EFE41BL,0xAFE85F97L,0xF7EFE41BL},{0x77EFF9FFL,0x77EFF9FFL,8UL,0x77EFF9FFL,0x77EFF9FFL},{0xF7EFE41BL,0xAFE85F97L,0xF7EFE41BL,0xF7EFE41BL,0xAFE85F97L},{0x77EFF9FFL,0x9EB559C9L,0x9EB559C9L,0x77EFF9FFL,0x9EB559C9L},{0xAFE85F97L,0xAFE85F97L,0xB4760137L,0xAFE85F97L,0xAFE85F97L},{0x9EB559C9L,0x77EFF9FFL,0x9EB559C9L,0x9EB559C9L,0x77EFF9FFL},{0xAFE85F97L,0xF7EFE41BL,0xF7EFE41BL,0xAFE85F97L,0xF7EFE41BL}};
            int32_t *l_3035 = &l_2633[0];
            int32_t l_3056 = 1L;
            int i, j;
            (*l_45) = ((((l_2864[5] >= ((l_2933 != (void*)0) <= (safe_mul_func_uint8_t_u_u((((((((safe_add_func_int16_t_s_s(((l_2941 = l_2938) == (g_2942 = g_2942)), ((*l_2946) = ((***g_790) = (-1L))))) != (*l_45)) , l_2947) != l_2948) && (**g_2375)) > l_2949[3]) , (****g_2344)), (***g_2944))))) == 0UL) >= (**g_1847)) || l_2949[3]);
            for (l_2779 = 3; (l_2779 <= 9); l_2779 += 1)
            { /* block id: 1202 */
                uint16_t l_2960 = 1UL;
                int8_t l_2967 = 0x54L;
                uint16_t l_2987[9][6] = {{65528UL,0UL,0UL,0UL,0UL,65528UL},{65528UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,65528UL,65528UL},{0UL,0UL,0UL,0UL,65528UL,1UL},{65528UL,0UL,0UL,0UL,0UL,65528UL},{65528UL,0UL,1UL,0UL,0UL,1UL},{0UL,0UL,1UL,0UL,65528UL,65528UL},{0UL,0UL,0UL,0UL,65528UL,1UL},{65528UL,0UL,0UL,0UL,0UL,65528UL}};
                uint32_t l_2991 = 6UL;
                uint64_t ** const *l_3020 = &g_2375;
                int32_t *l_3036 = &l_2949[7];
                uint32_t ** const l_3042 = (void*)0;
                int i, j;
                g_2950--;
                if ((*g_516))
                    continue;
            }
            (*g_1839) = (void*)0;
        }
    }
    (*g_3039) = func_77((safe_sub_func_int64_t_s_s((((65535UL != ((safe_add_func_uint64_t_u_u((safe_unary_minus_func_uint32_t_u((65535UL < (*l_45)))), (0xD8291E2566DAB489LL | ((***g_2374) = ((((*g_959) | 0xA15BL) != g_155[5][1][7]) <= (((safe_sub_func_uint32_t_u_u(9UL, (safe_mod_func_int16_t_s_s((!(((((safe_add_func_uint64_t_u_u((~18446744073709551615UL), g_1582[4])) != (*g_1311)) , g_2950) ^ (*l_45)) != 0x5FL)), (*g_1311))))) , (*g_2355)) ^ 1L)))))) | (*l_45))) , 0xF9L) < (****g_2943)), l_3089)), (*g_792), g_3090);
    for (g_166 = (-28); (g_166 == 58); g_166 = safe_add_func_int64_t_s_s(g_166, 3))
    { /* block id: 1272 */
        return (*l_45);
    }
    return (*l_45);
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_115 g_116 g_1848 g_266 g_1157 g_55 g_2376 g_129 g_790 g_791 g_792 g_271 g_1210 g_166 g_2355 g_2212 g_2374 g_2375 g_1311 g_243
 * writes: g_116 g_1210 g_166 g_2212 g_1581 g_271 g_6
 */
static const uint64_t  func_9(const int32_t * p_10, int32_t * p_11)
{ /* block id: 988 */
    int32_t *l_2523[2][3] = {{&g_1141[0][0][6],&g_1141[0][0][6],&g_1141[0][0][6]},{(void*)0,(void*)0,(void*)0}};
    uint8_t *l_2524[3][6][4] = {{{&g_1211,(void*)0,&g_57[7],(void*)0},{&g_1211,&g_1210[5],(void*)0,(void*)0},{&g_1210[2],(void*)0,(void*)0,&g_57[7]},{&g_1210[1],&g_1210[3],(void*)0,&g_1210[2]},{&g_1211,(void*)0,&g_1211,&g_1210[2]},{(void*)0,&g_1210[5],&g_1210[2],&g_1211}},{{(void*)0,&g_57[3],&g_1211,&g_1211},{&g_1211,&g_1211,(void*)0,&g_1210[3]},{&g_1210[1],(void*)0,(void*)0,&g_1210[3]},{&g_1211,&g_57[8],&g_57[3],(void*)0},{(void*)0,&g_57[8],&g_1211,&g_1210[3]},{&g_57[8],(void*)0,&g_57[8],&g_1210[3]}},{{&g_57[3],&g_1211,(void*)0,&g_1211},{&g_1210[2],&g_57[3],&g_1210[3],&g_1211},{&g_1210[3],&g_1210[5],&g_1210[3],&g_1210[2]},{&g_1210[2],(void*)0,(void*)0,&g_1210[2]},{&g_57[3],&g_1210[3],&g_57[8],&g_57[7]},{&g_57[8],&g_57[7],&g_1211,&g_1210[5]}}};
    uint8_t **l_2525 = (void*)0;
    uint8_t **l_2526 = &g_219[0][0][0];
    uint8_t **l_2527 = &l_2524[2][4][3];
    int32_t **** const l_2534 = &g_1900;
    uint32_t l_2537 = 0x829BFE54L;
    int16_t l_2538[2];
    uint32_t l_2539 = 0xC247009EL;
    uint8_t *l_2540 = &g_1210[2];
    int64_t l_2541 = 8L;
    uint64_t l_2542 = 1UL;
    uint32_t *l_2543 = &g_166;
    uint8_t *l_2544 = &g_1210[2];
    uint16_t l_2545[9][2][1] = {{{0x0507L},{0x4B5CL}},{{0xC9E6L},{0x4B5CL}},{{0x0507L},{0xC9E6L}},{{0xD281L},{0xD281L}},{{0xC9E6L},{0x0507L}},{{0x4B5CL},{0xC9E6L}},{{0x4B5CL},{0x0507L}},{{0xC9E6L},{0xD281L}},{{0xD281L},{0xC9E6L}}};
    int8_t l_2550 = 1L;
    float *l_2561 = &g_550;
    float **l_2560 = &l_2561;
    uint16_t * const *l_2568 = (void*)0;
    uint8_t l_2584 = 252UL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2538[i] = 0L;
    (*g_115) |= (*p_11);
    l_2545[4][1][0] = (((*l_2527) = l_2524[2][4][3]) == ((((*l_2543) &= ((safe_lshift_func_int16_t_s_u(((0x0E07B141089C80D7LL <= 0L) <= (((((*l_2540) ^= ((safe_add_func_uint8_t_u_u(((*g_1848) && (0x0069FC85A52866C1LL <= (((*g_1157) , l_2534) != l_2534))), (((safe_mul_func_uint8_t_u_u(((l_2537 , (*g_2376)) != 0x402889D8E4C7AD05LL), l_2538[0])) , (***g_790)) ^ (*g_792)))) > l_2539)) > l_2541) & l_2542) >= 0UL)), 8)) & 0x7BDB8A11L)) != (*g_2355)) , l_2544));
    for (l_2542 = 0; (l_2542 != 46); ++l_2542)
    { /* block id: 996 */
        int8_t l_2548 = 0L;
        int32_t l_2549[7][7] = {{6L,0xF3F68DA2L,0x375822D8L,9L,9L,0x375822D8L,0xF3F68DA2L},{0x2D1547B9L,0xBC6C22D2L,0x9B5ECC56L,0xBC6C22D2L,0x2D1547B9L,0xBC6C22D2L,0x9B5ECC56L},{9L,9L,0x375822D8L,0xF3F68DA2L,6L,6L,0xF3F68DA2L},{0xA1866D73L,0xBAD7CEA2L,0xA1866D73L,0xBC6C22D2L,0xA1866D73L,0xBAD7CEA2L,0xA1866D73L},{9L,0xF3F68DA2L,0xF3F68DA2L,9L,6L,0x375822D8L,0x375822D8L},{0x2D1547B9L,0xBAD7CEA2L,0x9B5ECC56L,0xBAD7CEA2L,0x2D1547B9L,0xBAD7CEA2L,0x9B5ECC56L},{6L,9L,0xF3F68DA2L,0xF3F68DA2L,9L,6L,0x375822D8L}};
        uint32_t l_2551 = 0UL;
        uint64_t ****l_2567 = &g_2374;
        int16_t l_2587 = 0x0CD6L;
        int i, j;
        l_2551--;
        for (g_2212 = 0; (g_2212 != 24); g_2212 = safe_add_func_uint8_t_u_u(g_2212, 8))
        { /* block id: 1000 */
            uint64_t ****l_2566[3];
            int i;
            for (i = 0; i < 3; i++)
                l_2566[i] = &g_2374;
        }
        for (g_166 = 0; (g_166 >= 47); g_166 = safe_add_func_int64_t_s_s(g_166, 6))
        { /* block id: 1006 */
            int8_t l_2579 = 0x1FL;
            int8_t *l_2585 = (void*)0;
            int8_t *l_2586 = &g_1581;
            int32_t l_2588 = 0x97C09C2AL;
            (*p_11) = (l_2588 ^= (((***g_2374) & (safe_lshift_func_int16_t_s_u(((***g_790) |= ((1UL <= (safe_lshift_func_uint8_t_u_u((((0x7866816F8B90A3C6LL & ((safe_lshift_func_int16_t_s_u((safe_add_func_uint32_t_u_u((0L <= (((((((l_2579 > ((((*l_2586) = (((safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u(((*g_1848) ^ (l_2551 || (((((((l_2579 > l_2584) , (void*)0) == (**l_2567)) , l_2579) | (-7L)) | 8L) >= 0L))), (*g_1311))), (*g_1311))) | 2UL) | l_2551)) & 250UL) & l_2587)) , (-3L)) , (-10L)) && l_2579) >= 1L) , l_2579) | l_2548)), 0xACA550A6L)), 15)) == (*g_2355))) != 0xF552F81D00961185LL) <= l_2548), 3))) & l_2549[3][6])), l_2548))) | l_2549[1][6]));
            return l_2579;
        }
        if ((*g_2355))
            continue;
    }
    return (*g_2376);
}


/* ------------------------------------------ */
/* 
 * reads : g_1311 g_243 g_1848 g_266 g_1649 g_516
 * writes: g_1581 g_1141
 */
static const int32_t * func_12(int32_t  p_13, uint16_t  p_14, uint32_t  p_15, uint64_t  p_16)
{ /* block id: 982 */
    int16_t l_2509 = (-1L);
    uint16_t * const *l_2514 = &g_1311;
    int8_t *l_2515 = &g_1581;
    int32_t *l_2516 = &g_1141[0][0][3];
    int32_t *l_2517 = &g_6[1];
    int32_t *l_2518[1][9][10] = {{{&g_6[1],(void*)0,&g_6[3],&g_1141[0][0][3],&g_1141[0][2][2],&g_1141[0][3][0],&g_1141[0][0][3],&g_116,&g_6[5],&g_6[5]},{&g_1141[0][0][3],&g_1141[0][1][1],&g_1141[0][0][3],&g_6[1],&g_6[1],&g_1141[0][0][3],&g_1141[0][1][1],&g_1141[0][0][3],&g_1141[0][0][3],&g_116},{(void*)0,&g_6[1],&g_1141[0][0][3],(void*)0,&g_1141[0][0][3],(void*)0,&g_6[1],(void*)0,&g_116,&g_6[1]},{(void*)0,(void*)0,&g_1141[0][0][3],&g_1141[0][0][3],(void*)0,(void*)0,&g_6[1],&g_1141[0][0][3],&g_6[1],&g_6[1]},{&g_1141[0][0][3],(void*)0,&g_1141[0][0][3],&g_116,&g_6[1],&g_6[1],(void*)0,&g_116,(void*)0,&g_1141[0][2][0]},{(void*)0,&g_1141[0][0][3],&g_6[3],&g_1141[0][2][0],&g_1141[0][0][3],&g_1141[0][2][0],&g_6[3],&g_1141[0][0][3],(void*)0,&g_116},{&g_1141[0][0][3],&g_116,&g_1141[0][0][3],&g_6[1],&g_6[1],&g_116,(void*)0,&g_1141[0][0][3],&g_1141[0][0][3],&g_116},{(void*)0,&g_6[1],&g_1141[0][0][3],&g_6[1],&g_6[3],&g_116,&g_6[1],&g_1141[0][0][3],(void*)0,&g_6[1]},{(void*)0,&g_1141[0][0][3],&g_6[1],&g_6[5],&g_1141[0][0][3],(void*)0,&g_116,(void*)0,&g_6[3],(void*)0}}};
    int64_t l_2519[1][4][5] = {{{0xCD75C92E5A90821ELL,0x15E4BD264594DEE0LL,0x15E4BD264594DEE0LL,0xCD75C92E5A90821ELL,0x15E4BD264594DEE0LL},{0xCD75C92E5A90821ELL,0xCD75C92E5A90821ELL,0x0A4678CE95AAB2A0LL,0xCD75C92E5A90821ELL,0xCD75C92E5A90821ELL},{0x15E4BD264594DEE0LL,0xCD75C92E5A90821ELL,0x15E4BD264594DEE0LL,0x15E4BD264594DEE0LL,0xCD75C92E5A90821ELL},{0xCD75C92E5A90821ELL,0x15E4BD264594DEE0LL,0x15E4BD264594DEE0LL,0xCD75C92E5A90821ELL,0x15E4BD264594DEE0LL}}};
    uint8_t l_2520 = 0UL;
    int i, j, k;
    (*l_2516) = (~((p_13 = 0x57ABA26CL) > ((safe_lshift_func_uint8_t_u_u((p_14 || l_2509), 7)) & ((safe_mod_func_int8_t_s_s(((*l_2515) = (((1UL || ((safe_sub_func_uint8_t_u_u((((0x3123L >= ((p_14 , l_2514) != l_2514)) && (((*g_1311) ^ p_15) & l_2509)) && 0xE6L), l_2509)) < p_16)) , (*g_1848)) <= 0x530DF48DL)), p_15)) == l_2509))));
    l_2520--;
    return (*g_1649);
}


/* ------------------------------------------ */
/* 
 * reads : g_2212 g_1141
 * writes: g_2212
 */
static float  func_20(float  p_21)
{ /* block id: 831 */
    int32_t *l_2204 = &g_6[5];
    int32_t l_2205 = 0x8DE905F4L;
    int32_t *l_2206 = &g_1141[0][2][4];
    int32_t *l_2207 = &l_2205;
    int32_t *l_2208 = &g_1141[0][0][3];
    int32_t *l_2209 = &g_1141[0][0][3];
    int32_t *l_2210 = &g_6[3];
    int32_t *l_2211[4];
    int8_t *l_2231 = &g_125;
    uint8_t *l_2234 = &g_57[2];
    int64_t *l_2235[3][9];
    uint8_t **l_2243 = &g_219[0][0][0];
    uint8_t ***l_2242 = &l_2243;
    uint8_t ****l_2241 = &l_2242;
    uint64_t l_2244 = 0UL;
    uint8_t l_2245 = 0x99L;
    uint64_t *l_2258[3][10] = {{&g_629,&g_629,&g_629,&g_629,&g_629,&g_629,&g_629,&g_629,&g_629,&g_629},{&g_629,&l_2244,&l_2244,&g_629,&l_2244,&l_2244,&g_629,&l_2244,&l_2244,&g_629},{&l_2244,&g_629,&l_2244,&l_2244,&g_629,&l_2244,&l_2244,&g_629,&l_2244,&l_2244}};
    const int32_t l_2259 = 0x33B1D787L;
    uint16_t *l_2293[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t l_2332 = 4294967295UL;
    uint32_t l_2350 = 18446744073709551615UL;
    uint32_t l_2371[2][1];
    int8_t l_2373 = (-1L);
    int16_t l_2386 = (-4L);
    float ** const l_2423 = (void*)0;
    int32_t ** const ***l_2478 = &g_2476;
    int32_t ***** const l_2490 = &g_1899;
    int i, j;
    for (i = 0; i < 4; i++)
        l_2211[i] = &g_6[1];
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
            l_2235[i][j] = (void*)0;
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_2371[i][j] = 0xE987CD8EL;
    }
    g_2212--;
    return (*l_2208);
}


/* ------------------------------------------ */
/* 
 * reads : g_2063 g_2064 g_2066 g_2065
 * writes:
 */
static float  func_31(int8_t  p_32, int32_t * p_33, uint32_t  p_34)
{ /* block id: 828 */
    int32_t *l_2196 = &g_1141[0][0][5];
    int32_t *l_2197 = &g_1141[0][0][3];
    int32_t *l_2198 = (void*)0;
    int32_t *l_2199[10] = {&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0],&g_1141[0][2][0]};
    uint16_t l_2200 = 0x26AFL;
    int i;
    ++l_2200;
    return (**g_2063);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_35(uint16_t  p_36, float  p_37, int32_t * p_38)
{ /* block id: 355 */
    uint8_t l_914 = 1UL;
    int64_t l_942 = 0x0FE3438F587A74A3LL;
    uint16_t *l_945 = (void*)0;
    uint16_t *l_946[6];
    int32_t l_947 = 0xDF13C594L;
    int32_t l_948 = 0x7627E720L;
    uint8_t *l_949 = &l_914;
    uint32_t l_950 = 0x9EEA1C55L;
    int32_t l_975 = 0x01927A18L;
    int32_t l_996 = 8L;
    int32_t l_998 = (-1L);
    int32_t l_1003 = 6L;
    int32_t l_1007 = 0xE75F58B5L;
    int32_t l_1008 = (-10L);
    int16_t **l_1016 = &g_792;
    uint32_t l_1048 = 0xC32249F9L;
    int8_t l_1057 = 0x64L;
    float *l_1080 = &g_247;
    int32_t *l_1124[8] = {&g_562[3],&g_562[3],&g_562[3],&g_562[3],&g_562[3],&g_562[3],&g_562[3],&g_562[3]};
    uint16_t l_1146 = 0x863CL;
    uint32_t l_1214 = 0x53EA97F0L;
    float l_1259 = (-0x6.Bp-1);
    int32_t l_1268[10][1][9] = {{{0L,2L,0x589CE9A6L,0x45661AE8L,3L,6L,6L,3L,0x45661AE8L}},{{0L,3L,0L,0x45661AE8L,0xC50E78D0L,(-8L),6L,0xC50E78D0L,(-9L)}},{{0L,0xC50E78D0L,0x4804AB05L,0x45661AE8L,2L,(-2L),6L,2L,0x9C5CF021L}},{{0L,2L,0x589CE9A6L,0x45661AE8L,3L,6L,6L,3L,0x45661AE8L}},{{0L,3L,0L,0x45661AE8L,0xC50E78D0L,(-8L),6L,0xC50E78D0L,(-9L)}},{{0L,0xC50E78D0L,0x4804AB05L,0x45661AE8L,2L,(-2L),6L,2L,0x9C5CF021L}},{{0L,2L,0x589CE9A6L,0x45661AE8L,3L,6L,6L,3L,0x45661AE8L}},{{0L,3L,0L,0x45661AE8L,0xC50E78D0L,(-8L),6L,0xC50E78D0L,(-9L)}},{{0L,0xC50E78D0L,0x4804AB05L,0x45661AE8L,2L,(-2L),6L,2L,0x9C5CF021L}},{{0L,2L,0x589CE9A6L,0x45661AE8L,3L,0xAB9817B5L,0xAB9817B5L,(-8L),0x9620C79EL}}};
    int16_t ***l_1386 = &l_1016;
    const float l_1388 = 0x6.6E0656p-71;
    uint16_t l_1423 = 0x53C7L;
    int32_t *l_1451 = &l_1003;
    uint8_t **l_1468 = (void*)0;
    uint32_t l_1491[5] = {0x50529480L,0x50529480L,0x50529480L,0x50529480L,0x50529480L};
    int64_t l_1517 = 1L;
    int16_t l_1554 = 0xF8EAL;
    uint16_t l_1583 = 0xB733L;
    uint32_t l_1615 = 0UL;
    uint32_t l_1663 = 0x5D8C56B7L;
    uint8_t l_1703 = 255UL;
    uint32_t l_1732 = 1UL;
    uint32_t **l_1765 = &g_1157;
    int16_t l_1787 = 0xD0F1L;
    int32_t l_1893 = 9L;
    uint32_t l_1956 = 0UL;
    uint16_t l_1975 = 6UL;
    int16_t l_1980 = 0x9421L;
    const int32_t **l_1999 = &g_1944;
    const int32_t ***l_1998 = &l_1999;
    int32_t ****l_2041 = (void*)0;
    int32_t l_2096 = 1L;
    int32_t l_2097 = 0x54C71A9FL;
    int32_t l_2098 = (-3L);
    int32_t l_2101[7][9][4] = {{{0xA382CC11L,0L,0xA382CC11L,4L},{0xC6ABDC80L,0xB00B4B8CL,0L,5L},{0xE49E1590L,0x4E41A48EL,0xB00B4B8CL,0xB00B4B8CL},{(-9L),(-9L),0xB00B4B8CL,0xA382CC11L},{0xE49E1590L,(-1L),0L,0x4E41A48EL},{0xC6ABDC80L,0L,0xA382CC11L,0L},{0xA382CC11L,0L,0xC6ABDC80L,0x4E41A48EL},{0L,(-1L),0xE49E1590L,0xA382CC11L},{0xB00B4B8CL,(-9L),(-9L),0xB00B4B8CL}},{{0xB00B4B8CL,0x4E41A48EL,0xE49E1590L,5L},{0L,0xB00B4B8CL,0xC6ABDC80L,4L},{0xA382CC11L,0L,0xA382CC11L,4L},{0xC6ABDC80L,0xB00B4B8CL,0L,5L},{0xE49E1590L,0x4E41A48EL,0xB00B4B8CL,0xB00B4B8CL},{(-9L),(-9L),0xB00B4B8CL,0xA382CC11L},{0xE49E1590L,(-1L),0L,0x4E41A48EL},{0xC6ABDC80L,0L,0xA382CC11L,0L},{0xA382CC11L,0L,0xC6ABDC80L,0x4E41A48EL}},{{0L,(-1L),0xE49E1590L,0xA382CC11L},{0xB00B4B8CL,(-9L),(-9L),0xB00B4B8CL},{0xB00B4B8CL,0x4E41A48EL,0xE49E1590L,5L},{0L,0xB00B4B8CL,0xC6ABDC80L,4L},{0xA382CC11L,0L,0xA382CC11L,4L},{0xC6ABDC80L,0xB00B4B8CL,0L,5L},{0xE49E1590L,0x4E41A48EL,0xB00B4B8CL,0xB00B4B8CL},{(-9L),(-9L),0xB00B4B8CL,0xA382CC11L},{0xE49E1590L,(-1L),0L,0x4E41A48EL}},{{0xC6ABDC80L,0L,0xA382CC11L,0L},{0xA382CC11L,0L,0xC6ABDC80L,0x4E41A48EL},{0L,(-1L),0xE49E1590L,0xA382CC11L},{0xB00B4B8CL,(-9L),(-9L),0xB00B4B8CL},{0xB00B4B8CL,0x4E41A48EL,0xE49E1590L,5L},{0L,0xB00B4B8CL,0xC6ABDC80L,4L},{0xA382CC11L,0L,0xA382CC11L,4L},{0xC6ABDC80L,0xB00B4B8CL,0L,5L},{0xE49E1590L,0x4E41A48EL,0xB00B4B8CL,0xB00B4B8CL}},{{(-9L),(-9L),0xB00B4B8CL,0xA382CC11L},{0xE49E1590L,(-1L),0L,0x4E41A48EL},{0xC6ABDC80L,0L,0xA382CC11L,0L},{0xA382CC11L,0L,0xC6ABDC80L,0x4E41A48EL},{0L,(-1L),0xE49E1590L,0xA382CC11L},{0xB00B4B8CL,(-9L),(-9L),0xB00B4B8CL},{0xB00B4B8CL,0x4E41A48EL,0xE49E1590L,5L},{0L,0xB00B4B8CL,0xC6ABDC80L,4L},{0xA382CC11L,0L,0xA382CC11L,4L}},{{0xC6ABDC80L,0xB00B4B8CL,0L,(-1L)},{0xA382CC11L,0xB00B4B8CL,(-9L),(-9L)},{4L,4L,(-9L),0x4E41A48EL},{0xA382CC11L,0xC6ABDC80L,0L,0xB00B4B8CL},{0xE49E1590L,0L,0x4E41A48EL,0L},{0x4E41A48EL,0L,0xE49E1590L,0xB00B4B8CL},{0L,0xC6ABDC80L,0xA382CC11L,0x4E41A48EL},{(-9L),4L,4L,(-9L)},{(-9L),0xB00B4B8CL,0xA382CC11L,(-1L)}},{{0L,(-9L),0xE49E1590L,0L},{0x4E41A48EL,5L,0x4E41A48EL,0L},{0xE49E1590L,(-9L),0L,(-1L)},{0xA382CC11L,0xB00B4B8CL,(-9L),(-9L)},{4L,4L,(-9L),0x4E41A48EL},{0xA382CC11L,0xC6ABDC80L,0L,0xB00B4B8CL},{0xE49E1590L,0L,0x4E41A48EL,0L},{0x4E41A48EL,0L,0xE49E1590L,0xB00B4B8CL},{0L,0xC6ABDC80L,0xA382CC11L,0x4E41A48EL}}};
    uint64_t l_2103 = 0xAEB214B6097A46E5LL;
    int32_t l_2184[1][8] = {{0x54C09794L,5L,5L,0x54C09794L,5L,5L,0x54C09794L,5L}};
    int32_t l_2185 = 0x4BE4BF41L;
    int32_t l_2186[1];
    int8_t l_2187[3];
    uint32_t l_2195 = 0x9DADA439L;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_946[i] = &g_243;
    for (i = 0; i < 1; i++)
        l_2186[i] = 0x7188FE53L;
    for (i = 0; i < 3; i++)
        l_2187[i] = 0x82L;
    l_914++;
    return l_2195;
}


/* ------------------------------------------ */
/* 
 * reads : g_59 g_6 g_57 g_55 g_115 g_116 g_109 g_129 g_151 g_166 g_152 g_160 g_164 g_240 g_144 g_249 g_158 g_260 g_266 g_271 g_125 g_247 g_162 g_155 g_243 g_333 g_341 g_201 g_265 g_469 g_510 g_518 g_244 g_547 g_549 g_550 g_629 g_521 g_511 g_512 g_516 g_810 g_912
 * writes: g_57 g_109 g_59 g_116 g_125 g_129 g_144 g_151 g_166 g_219 g_243 g_244 g_249 g_260 g_266 g_247 g_162 g_164 g_333 g_341 g_155 g_152 g_55 g_158 g_165 g_479 g_516 g_521 g_550 g_259
 */
static float  func_41(int32_t * p_42, int32_t * p_43, uint8_t  p_44)
{ /* block id: 15 */
    int32_t l_68 = 4L;
    uint16_t l_76 = 0xB70CL;
    for (p_44 = 0; (p_44 < 13); p_44 = safe_add_func_uint64_t_u_u(p_44, 5))
    { /* block id: 18 */
        uint8_t *l_83 = &g_57[3];
        int32_t ***l_910 = &g_333[1];
        float *l_911 = &g_247;
        (*g_912) = ((*l_911) = ((func_48(((safe_mod_func_int32_t_s_s((l_68 || ((p_44 >= g_59[0]) , ((((*l_910) = func_69(((safe_rshift_func_int8_t_s_s(((((safe_lshift_func_uint8_t_u_s(l_76, 0)) , func_77((g_59[0] , l_76), g_6[1], ((*l_83) = ((((g_6[1] & (safe_rshift_func_uint8_t_u_u((&g_57[3] == &g_57[3]), 4))) & l_76) ^ l_76) < 0x39L)))) != (void*)0) != 0UL), 0)) && 0x19BCL), g_158)) != &p_43) , g_116))), (-2L))) > l_76), g_810[8][0][6], &l_68, l_76, p_44) , 0xF5317633L) , p_44));
    }
    return (*g_912);
}


/* ------------------------------------------ */
/* 
 * reads : g_55 g_57
 * writes: g_55 g_57
 */
static uint8_t  func_48(uint32_t  p_49, float  p_50, int32_t * p_51, uint64_t  p_52, int32_t  p_53)
{ /* block id: 9 */
    int64_t l_56[8] = {6L,6L,6L,6L,6L,6L,6L,6L};
    int i;
    (*p_51) ^= l_56[3];
    g_57[3] ^= g_55;
    return p_49;
}


/* ------------------------------------------ */
/* 
 * reads : g_249 g_155 g_271 g_59 g_243 g_116 g_333 g_151 g_160 g_115 g_341 g_266 g_109 g_129 g_144 g_57 g_6 g_158 g_201 g_164 g_265 g_469 g_510 g_162 g_55 g_166 g_152 g_240 g_260 g_125 g_247 g_518 g_244 g_547 g_549 g_550 g_629 g_521 g_511 g_512 g_516
 * writes: g_249 g_243 g_333 g_247 g_341 g_116 g_166 g_260 g_155 g_152 g_57 g_129 g_55 g_151 g_158 g_165 g_162 g_479 g_109 g_59 g_125 g_144 g_219 g_244 g_266 g_164 g_516 g_521 g_550 g_259
 */
static int32_t ** func_69(uint8_t  p_70, uint16_t  p_71)
{ /* block id: 113 */
    int16_t l_330 = (-4L);
    int32_t l_349 = 6L;
    int32_t *l_364 = &g_164[3];
    int32_t *l_372 = &l_349;
    int32_t l_383 = (-10L);
    int32_t l_384[8][7][2];
    uint16_t l_418 = 1UL;
    const int64_t l_419 = 0L;
    uint64_t l_420[5] = {0x0D2A02A47BCB2FD8LL,0x0D2A02A47BCB2FD8LL,0x0D2A02A47BCB2FD8LL,0x0D2A02A47BCB2FD8LL,0x0D2A02A47BCB2FD8LL};
    uint16_t l_448 = 0x7012L;
    int32_t ***l_486 = &g_333[1];
    uint32_t *l_489 = &g_151;
    uint32_t **l_488 = &l_489;
    int32_t *l_584 = &l_384[5][1][0];
    uint32_t l_585[3][3][6] = {{{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L},{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L},{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L}},{{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L},{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L},{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L}},{{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L},{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L},{18446744073709551615UL,0xB4852A48L,0xB4852A48L,18446744073709551615UL,0xB4852A48L,0xB4852A48L}}};
    int8_t l_591 = (-9L);
    int32_t l_690 = 0x9993B159L;
    uint32_t l_723 = 0x956064F2L;
    uint64_t l_746 = 0UL;
    int16_t **l_759 = (void*)0;
    int16_t ***l_758 = &l_759;
    uint8_t l_903 = 0x27L;
    int i, j, k;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 2; k++)
                l_384[i][j][k] = 0xAFBDBAFEL;
        }
    }
    for (g_249 = 0; (g_249 == 15); g_249 = safe_add_func_uint64_t_u_u(g_249, 2))
    { /* block id: 116 */
        const uint32_t *l_310 = &g_311;
        int32_t l_327 = 2L;
        int32_t *l_343 = (void*)0;
        int32_t **l_342 = &l_343;
        int32_t l_376 = 0xE694AC0AL;
        for (p_71 = 0; (p_71 <= 8); p_71 += 1)
        { /* block id: 119 */
            const uint32_t *l_309 = &g_201;
            const uint32_t **l_308[8] = {&l_309,&l_309,&l_309,&l_309,&l_309,&l_309,&l_309,&l_309};
            uint32_t l_328[9][5] = {{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0UL,0UL,0UL,0UL,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0UL,0UL,0UL,0UL,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0UL,0UL,0UL,0UL,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0UL,0UL,0UL,0UL,0UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}};
            uint16_t *l_329 = &g_243;
            int32_t l_346 = 5L;
            uint8_t l_378 = 0UL;
            int32_t l_382 = 0xFA29C4A5L;
            int i, j;
            if ((((((&g_271[p_71] != (void*)0) | (-1L)) >= (((((((l_310 = (void*)0) != ((safe_mul_func_float_f_f(((+(((safe_lshift_func_uint16_t_u_u(((*l_329) |= (g_155[7][2][0] | (safe_unary_minus_func_int32_t_s((safe_mod_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((((safe_rshift_func_int8_t_s_s(0x14L, 6)) , g_271[p_71]) , (((((safe_lshift_func_int8_t_s_s((((((((safe_unary_minus_func_int8_t_s(p_70)) , (65535UL || p_71)) , (void*)0) == (void*)0) != p_70) , 0x96EA2A7FL) , p_70), g_59[0])) != 0x4527131DL) >= 253UL) < p_71) || (-3L))) , l_327) , 255UL), l_328[4][4])), l_328[2][1])))))), p_71)) , &l_327) != (void*)0)) , (-0x10.Ep+1)), p_71)) , &g_59[0])) , l_330) > g_116) == 0x0.B69B36p+10) <= 0xD.52C08Ap-32) , l_328[7][0])) & 0x198C85A8L) || 0x41L))
            { /* block id: 122 */
                int32_t ***l_334 = &g_333[1];
                float *l_340[8][1];
                int32_t l_344 = (-1L);
                int32_t *l_345[4][3][6] = {{{(void*)0,(void*)0,&g_116,&l_344,&g_116,(void*)0},{&g_116,&g_6[1],&l_344,&l_344,&g_6[1],&g_116},{(void*)0,&g_116,&l_344,&g_116,(void*)0,(void*)0}},{{&l_344,&g_116,&g_116,&l_344,&g_6[1],&l_344},{&l_344,&g_6[1],&l_344,&g_116,&g_116,&l_344},{(void*)0,(void*)0,&g_116,&l_344,&g_116,(void*)0}},{{&g_116,&g_6[1],&l_344,&l_344,&g_6[1],&g_116},{(void*)0,&g_116,&l_344,&g_116,(void*)0,(void*)0},{&l_344,&g_116,&g_116,&l_344,&g_6[1],&l_344}},{{&l_344,&g_6[1],&l_344,&g_116,&g_116,&l_344},{(void*)0,(void*)0,&g_116,&l_344,&g_116,(void*)0},{&g_116,&g_6[1],&l_344,&l_344,&g_6[1],&g_116}}};
                int i, j, k;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_340[i][j] = &g_247;
                }
                (*g_115) = ((safe_div_func_uint8_t_u_u((((((*l_334) = g_333[1]) != (((!p_70) <= (safe_div_func_float_f_f((safe_div_func_float_f_f((g_247 = l_327), (0xB.F1A1C1p+67 >= p_71))), (0x0.4p-1 <= (g_341 = g_151))))) , l_342)) <= (((g_243 >= p_71) , &g_243) != (void*)0)) || g_160[0][0][0]), 0x94L)) ^ l_344);
                l_346 = 0x3E6B98A5L;
                l_327 |= (*g_115);
            }
            else
            { /* block id: 129 */
                uint16_t l_368 = 0xCE3BL;
                int32_t ***l_371[3][1][9] = {{{(void*)0,&g_333[1],&g_333[1],&l_342,&g_333[1],&g_333[1],(void*)0,(void*)0,&g_333[1]}},{{&l_342,&g_333[1],(void*)0,&g_333[1],&l_342,&l_342,&l_342,&l_342,&g_333[1]}},{{(void*)0,&g_333[1],(void*)0,&l_342,(void*)0,(void*)0,&l_342,(void*)0,&g_333[1]}}};
                int i, j, k;
                for (g_166 = 0; g_166 < 9; g_166 += 1)
                {
                    for (g_260 = 0; g_260 < 3; g_260 += 1)
                    {
                        for (p_70 = 0; p_70 < 8; p_70 += 1)
                        {
                            g_155[g_166][g_260][p_70] = 0x2E24L;
                        }
                    }
                }
                for (g_152 = 0; (g_152 <= 8); g_152 += 1)
                { /* block id: 133 */
                    uint8_t *l_369 = &g_57[3];
                    int32_t ***l_370 = (void*)0;
                    int32_t l_377[3][1];
                    uint64_t l_385[7][3][8] = {{{18446744073709551606UL,0x9DA8BD7C63EB3C3ELL,0x7E0374708CE895B4LL,0x44A103A164675EDDLL,18446744073709551612UL,0xD20FDDF2EE573C37LL,0x76DEA2D110966B84LL,3UL},{18446744073709551615UL,5UL,0xA7FE2643B97613D9LL,0x87168DBB40D21595LL,18446744073709551606UL,1UL,0x76DEA2D110966B84LL,0xE16BC9C2AA3F622CLL},{0x993070F6600F9E3BLL,0x87168DBB40D21595LL,0x7E0374708CE895B4LL,0xD20FDDF2EE573C37LL,8UL,0x003F100ACFC79905LL,0x6C7B5BE34E2D2513LL,3UL}},{{8UL,0x003F100ACFC79905LL,0x6C7B5BE34E2D2513LL,3UL,0UL,0xA36C9952BF6DFB7BLL,18446744073709551610UL,18446744073709551613UL},{18446744073709551615UL,0x9A4C5D2104298FA6LL,1UL,0UL,18446744073709551611UL,8UL,0xD20FDDF2EE573C37LL,18446744073709551610UL},{0x10662D8ED43A3B5ELL,0xD2ADA55E93804FF1LL,18446744073709551610UL,0x513267C153BD5B5ALL,1UL,1UL,0x31D7939AF177189ALL,18446744073709551615UL}},{{5UL,18446744073709551610UL,8UL,0xA36C9952BF6DFB7BLL,0x7A4DF89FAFED6D07LL,1UL,8UL,18446744073709551611UL},{0UL,0xB711A4B42C5639CBLL,18446744073709551615UL,18446744073709551606UL,0x866A7898F86669DALL,3UL,2UL,0UL},{0x69B2F04D0AE51A94LL,0xA7FE2643B97613D9LL,0xCE775CA79990798ELL,0x7BEC53CC7EE48934LL,0x6C7B5BE34E2D2513LL,18446744073709551607UL,0x7A4DF89FAFED6D07LL,0x44A103A164675EDDLL}},{{0x513267C153BD5B5ALL,3UL,1UL,0x9DF7BD2D68554C87LL,0x9DF7BD2D68554C87LL,1UL,3UL,0x513267C153BD5B5ALL},{18446744073709551611UL,8UL,0x76DEA2D110966B84LL,0UL,0xD20FDDF2EE573C37LL,0xA36C9952BF6DFB7BLL,0x993070F6600F9E3BLL,8UL},{0x31D7939AF177189ALL,0xA44B69D266918ABFLL,0x529FBBD50D57B6DCLL,0x9DA8BD7C63EB3C3ELL,1UL,0xA36C9952BF6DFB7BLL,0x513267C153BD5B5ALL,0x51164A319C6820D9LL}},{{1UL,18446744073709551615UL,0UL,3UL,0x003F100ACFC79905LL,18446744073709551611UL,8UL,7UL},{0x51164A319C6820D9LL,18446744073709551611UL,6UL,0x4C903916DEC09C32LL,0xA36C9952BF6DFB7BLL,0x6081FDC6087E6869LL,0UL,0x87168DBB40D21595LL},{0xC207859915F8ECFFLL,0UL,7UL,0x6C7B5BE34E2D2513LL,8UL,18446744073709551611UL,1UL,0xA36C9952BF6DFB7BLL}},{{3UL,0xB40A934032C85529LL,0xE16BC9C2AA3F622CLL,18446744073709551611UL,1UL,0x9DF7BD2D68554C87LL,18446744073709551612UL,18446744073709551610UL},{1UL,0x87168DBB40D21595LL,2UL,1UL,1UL,0x993070F6600F9E3BLL,18446744073709551613UL,0x7A4DF89FAFED6D07LL},{0UL,0x6C7B5BE34E2D2513LL,0UL,0x87168DBB40D21595LL,3UL,0x76DEA2D110966B84LL,0xD20FDDF2EE573C37LL,18446744073709551612UL}},{{0x513267C153BD5B5ALL,0UL,18446744073709551615UL,0xCEFC77A986C0544ELL,18446744073709551607UL,18446744073709551610UL,18446744073709551615UL,2UL},{0xC5235D57191971AFLL,8UL,1UL,0x31D7939AF177189ALL,1UL,8UL,0xC5235D57191971AFLL,0UL},{0x76DEA2D110966B84LL,0xCE775CA79990798ELL,0x31D7939AF177189ALL,0xA7FE2643B97613D9LL,0UL,0xC207859915F8ECFFLL,0x10662D8ED43A3B5ELL,18446744073709551611UL}}};
                    uint64_t *l_391[8][3] = {{&l_385[3][1][0],&l_385[3][1][0],&l_385[3][1][0]},{&l_385[2][0][7],&l_385[2][0][7],(void*)0},{&l_385[3][1][0],(void*)0,&l_385[3][1][0]},{&l_385[3][1][0],(void*)0,&l_385[3][1][0]},{(void*)0,&l_385[2][0][7],&l_385[2][0][7]},{&l_385[3][1][0],&l_385[3][1][0],&l_385[3][1][0]},{(void*)0,&g_129,&l_385[3][1][0]},{(void*)0,&g_129,(void*)0}};
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_377[i][j] = 0x7A4CBB4CL;
                    }
                    l_349 |= (safe_rshift_func_uint16_t_u_s(0UL, p_70));
                    l_372 = ((((*l_342) == (void*)0) == ((safe_mul_func_uint16_t_u_u(((((safe_mod_func_uint32_t_u_u((((*l_369) = (p_71 , ((safe_rshift_func_int8_t_s_u(0x16L, 0)) && ((safe_div_func_int64_t_s_s((safe_div_func_uint32_t_u_u(g_341, (safe_div_func_int8_t_s_s(((safe_sub_func_int64_t_s_s(((l_364 == l_309) > (g_266[0] | ((safe_sub_func_uint16_t_u_u(((*l_329) = (~(g_109[2][3][2] & g_129))), l_328[4][4])) & l_330))), 0x5408F6A8A630E985LL)) == l_368), l_330)))), g_144)) || 0x612B10208DEA3B1ELL)))) & p_71), 0x06007C0EL)) | 0xBDL) , l_370) != l_371[2][0][1]), p_70)) >= 65532UL)) , (*l_342));
                    for (l_346 = 3; (l_346 <= 8); l_346 += 1)
                    { /* block id: 140 */
                        uint16_t l_373 = 0xA4B2L;
                        int32_t l_381[8] = {0x7B4EAEF4L,0x7B4EAEF4L,(-1L),0x7B4EAEF4L,0x7B4EAEF4L,(-1L),0x7B4EAEF4L,0x7B4EAEF4L};
                        int i;
                        --l_373;
                        l_378--;
                        ++l_385[3][1][0];
                    }
                    l_346 |= ((-9L) < (safe_unary_minus_func_int16_t_s((g_109[9][4][0] | (safe_sub_func_int32_t_s_s(((((p_71 ^ (g_129 = g_266[0])) >= ((void*)0 != &l_372)) ^ (safe_div_func_int16_t_s_s(((safe_div_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(0xB0378CB08FCC912DLL, (safe_rshift_func_int16_t_s_s((-9L), p_71)))), 0xF6L)) > 0xC69D896AL), g_271[4]))) , p_71), p_70))))));
                }
                (*l_342) = (*l_342);
            }
        }
    }
    if (((&g_57[4] != ((((((safe_mul_func_uint16_t_u_u(((g_55 = g_57[3]) , ((safe_add_func_int64_t_s_s(((safe_sub_func_int8_t_s_s(((((l_384[1][5][0] &= ((((safe_sub_func_uint8_t_u_u(p_71, p_71)) | (safe_add_func_uint8_t_u_u(p_71, ((-5L) == (safe_sub_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_s((safe_sub_func_uint32_t_u_u((((((safe_rshift_func_int8_t_s_u(1L, l_349)) & 0L) == 0xF874L) > g_271[4]) != g_59[2]), 0x1598D41BL)), p_71)) & g_6[3]), p_70)))))) && g_158) , p_71)) > l_418) == l_349) | l_349), l_419)) ^ p_71), p_71)) , g_249)), p_71)) ^ (-7L)) | 0xD0482F9EL) , l_420[2]) ^ p_71) , &g_57[3])) & g_158))
    { /* block id: 154 */
        int16_t *l_437 = (void*)0;
        int16_t * const * const l_436[1][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        int64_t *l_439 = &g_109[6][2][1];
        int32_t l_441 = 1L;
        int32_t **l_455 = &l_372;
        int8_t * const l_470 = &g_125;
        int32_t *l_481[4] = {&g_160[0][1][0],&g_160[0][1][0],&g_160[0][1][0],&g_160[0][1][0]};
        int64_t l_490[7][2][8] = {{{0x63C297416A418F1DLL,0L,0xE0653A872BBF8746LL,0L,0xD6C4041452F6E7D6LL,0x536F8D1277F98B9ELL,0xD6C4041452F6E7D6LL,0L},{0L,0x0456BA58EE2123EALL,0L,0x3B4713335AE07524LL,(-4L),0x30F35E84A42099E1LL,8L,0x536F8D1277F98B9ELL}},{{(-2L),1L,0x3B4713335AE07524LL,0x81098B1568685641LL,0xE0653A872BBF8746LL,(-4L),(-4L),0xE0653A872BBF8746LL},{(-2L),0xD6C4041452F6E7D6LL,0xD6C4041452F6E7D6LL,(-2L),(-4L),0x81098B1568685641LL,0L,0x30F35E84A42099E1LL}},{{0L,0xB8524FA4201D22E2LL,0x81098B1568685641LL,8L,0xD6C4041452F6E7D6LL,0x0456BA58EE2123EALL,0x30F35E84A42099E1LL,0x0456BA58EE2123EALL},{0x63C297416A418F1DLL,0xB8524FA4201D22E2LL,0L,0xB8524FA4201D22E2LL,0x63C297416A418F1DLL,0x81098B1568685641LL,(-2L),0L}},{{0x81098B1568685641LL,0xD6C4041452F6E7D6LL,0x63C297416A418F1DLL,0x536F8D1277F98B9ELL,1L,(-4L),0xB8524FA4201D22E2LL,0xB8524FA4201D22E2LL},{0x30F35E84A42099E1LL,1L,0x63C297416A418F1DLL,0x63C297416A418F1DLL,1L,0x30F35E84A42099E1LL,(-2L),1L}},{{0x3B4713335AE07524LL,8L,0L,(-2L),0x81098B1568685641LL,0x63C297416A418F1DLL,0xB8524FA4201D22E2LL,0L},{0L,(-4L),0L,(-2L),0L,(-4L),0L,0x3B4713335AE07524LL}},{{0xD6C4041452F6E7D6LL,0L,0xE0653A872BBF8746LL,0L,0x63C297416A418F1DLL,0x3B4713335AE07524LL,0x0456BA58EE2123EALL,0x81098B1568685641LL},{(-2L),0x30F35E84A42099E1LL,1L,0x63C297416A418F1DLL,0x63C297416A418F1DLL,1L,0x30F35E84A42099E1LL,(-2L)}},{{0xD6C4041452F6E7D6LL,(-2L),(-4L),0x81098B1568685641LL,0L,0x30F35E84A42099E1LL,0xE0653A872BBF8746LL,8L},{0L,0xB8524FA4201D22E2LL,0x536F8D1277F98B9ELL,0x30F35E84A42099E1LL,0x81098B1568685641LL,0x30F35E84A42099E1LL,0x536F8D1277F98B9ELL,0xB8524FA4201D22E2LL}}};
        int i, j, k;
        for (g_151 = 0; (g_151 <= 4); g_151 += 1)
        { /* block id: 157 */
            int16_t l_440 = 0xD268L;
            int32_t **l_453[5][8] = {{&l_372,&l_372,&l_372,&l_372,&l_372,&l_372,&l_372,&l_372},{&l_372,&l_372,&l_372,&l_372,&l_372,&l_372,&l_372,&l_372},{&l_372,&l_372,&l_372,&l_372,(void*)0,&l_372,&l_372,&l_372},{&l_372,(void*)0,&l_372,&l_372,(void*)0,&l_372,(void*)0,&l_372},{&l_372,(void*)0,&l_372,&l_372,&l_372,&l_372,(void*)0,&l_372}};
            int32_t *l_475[3][5] = {{&g_160[0][1][0],&g_160[0][1][0],&g_160[0][1][0],&g_152,(void*)0},{&g_152,&g_160[0][0][0],&g_160[0][0][0],&g_152,&g_160[0][1][0]},{&g_152,&g_152,&g_164[0],&g_164[0],&g_152}};
            int32_t **l_476 = (void*)0;
            int32_t **l_477 = (void*)0;
            int32_t **l_478[1][10];
            int32_t *l_480 = &g_152;
            int16_t l_487[9] = {5L,5L,1L,5L,5L,1L,5L,5L,1L};
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 10; j++)
                    l_478[i][j] = &l_364;
            }
            for (g_158 = 4; (g_158 >= 1); g_158 -= 1)
            { /* block id: 160 */
                int16_t *l_433 = &g_144;
                int16_t **l_432[6][9] = {{&l_433,(void*)0,&l_433,(void*)0,&l_433,&l_433,&l_433,&l_433,&l_433},{&l_433,(void*)0,&l_433,(void*)0,&l_433,&l_433,&l_433,&l_433,&l_433},{&l_433,(void*)0,&l_433,(void*)0,&l_433,&l_433,&l_433,&l_433,&l_433},{&l_433,(void*)0,&l_433,(void*)0,&l_433,&l_433,&l_433,&l_433,&l_433},{&l_433,(void*)0,&l_433,(void*)0,&l_433,&l_433,&l_433,&l_433,&l_433},{&l_433,(void*)0,&l_433,(void*)0,&l_433,&l_433,&l_433,&l_433,&l_433}};
                int16_t ***l_434 = (void*)0;
                int16_t ***l_435 = &l_432[5][7];
                uint8_t *l_438 = &g_57[1];
                float *l_442 = &g_247;
                int32_t l_447[4];
                int i, j;
                for (i = 0; i < 4; i++)
                    l_447[i] = (-1L);
                g_165[g_151][g_151] = ((safe_div_func_float_f_f(0x7.6F8773p-52, 0xA.67A99Ap+46)) , ((((*l_442) = (safe_mul_func_float_f_f(((((((safe_add_func_int8_t_s_s((p_71 == (safe_mul_func_int16_t_s_s((l_440 ^= (safe_rshift_func_uint8_t_u_u(((*l_438) = (!(p_70 , (((*l_435) = l_432[5][7]) != l_436[0][5])))), (0xF0E821CDL > ((void*)0 == l_439))))), l_441))), 7UL)) , g_201) , 0xAD2EC623C0E6BEA8LL) != g_164[3]) , (void*)0) == &g_260), p_70))) >= p_70) == l_441));
                for (g_166 = 0; (g_166 <= 42); g_166 = safe_add_func_int32_t_s_s(g_166, 1))
                { /* block id: 168 */
                    int32_t *l_445 = &g_116;
                    int32_t *l_446[6] = {&g_116,&g_116,&g_116,&g_116,&g_116,&g_116};
                    int i;
                    l_448--;
                    for (g_162 = 0; (g_162 >= 21); g_162 = safe_add_func_uint8_t_u_u(g_162, 7))
                    { /* block id: 172 */
                        int32_t **l_454[6][9][4] = {{{&l_446[2],&l_446[0],&l_445,&l_446[5]},{&l_446[4],&l_446[2],&l_446[2],(void*)0},{&l_446[4],(void*)0,(void*)0,(void*)0},{&l_445,&l_445,(void*)0,&l_446[4]},{&l_446[0],(void*)0,&l_445,&l_446[2]},{&l_445,(void*)0,&l_446[2],&l_445},{&l_372,(void*)0,&l_372,&l_446[2]},{(void*)0,(void*)0,(void*)0,&l_372},{(void*)0,&l_446[2],&l_446[2],&l_445}},{{&l_446[5],(void*)0,&l_446[2],&l_445},{&l_446[2],&l_446[2],&l_445,&l_372},{&l_445,&l_445,&l_446[2],&l_446[3]},{&l_446[5],&l_372,&l_445,&l_446[5]},{&l_445,&l_445,(void*)0,(void*)0},{&l_445,&l_445,&l_446[4],&l_445},{&l_372,&l_445,&l_445,&l_446[2]},{&l_446[2],&l_446[3],&l_445,(void*)0},{(void*)0,&l_446[2],(void*)0,(void*)0}},{{&l_446[2],&l_372,&l_372,&l_445},{&l_446[2],(void*)0,&l_446[2],&l_372},{(void*)0,&l_446[4],&l_446[2],&l_372},{&l_446[2],(void*)0,&l_372,&l_446[2]},{&l_446[2],&l_446[2],(void*)0,&l_372},{(void*)0,&l_372,&l_445,&l_445},{&l_446[2],(void*)0,&l_445,&l_445},{&l_372,&l_445,&l_446[4],&l_446[2]},{&l_445,&l_372,(void*)0,(void*)0}},{{&l_445,&l_446[2],&l_445,&l_446[2]},{&l_446[5],(void*)0,&l_446[2],&l_445},{&l_445,&l_446[4],&l_445,&l_446[2]},{&l_446[2],&l_445,&l_446[2],&l_445},{&l_446[5],&l_446[2],&l_446[2],&l_446[5]},{(void*)0,&l_446[2],(void*)0,&l_446[2]},{(void*)0,&l_445,&l_372,&l_446[2]},{&l_372,&l_372,&l_446[2],&l_446[2]},{&l_445,&l_445,&l_445,&l_446[2]}},{{&l_445,&l_446[2],&l_446[2],&l_446[5]},{&l_446[2],&l_446[2],&l_445,&l_445},{&l_445,&l_445,&l_446[2],&l_446[2]},{&l_372,&l_446[4],&l_446[2],&l_445},{&l_446[2],(void*)0,&l_445,&l_446[2]},{&l_446[2],&l_446[2],(void*)0,(void*)0},{&l_445,&l_372,&l_446[2],&l_446[2]},{&l_446[2],&l_445,&l_446[2],&l_445},{&l_446[2],(void*)0,&l_446[4],&l_445}},{{(void*)0,&l_372,&l_446[0],&l_372},{&l_445,&l_446[2],&l_446[2],&l_446[2]},{(void*)0,(void*)0,&l_446[2],&l_372},{&l_446[2],&l_446[4],&l_446[4],&l_372},{&l_446[2],(void*)0,&l_446[2],&l_445},{(void*)0,&l_372,&l_446[2],(void*)0},{&l_445,&l_446[2],&l_446[0],(void*)0},{(void*)0,&l_446[2],&l_445,&l_446[2]},{(void*)0,&l_446[4],&l_446[1],&l_446[2]}}};
                        int i, j, k;
                        return g_333[0];
                    }
                    (*l_445) = (safe_rshift_func_int8_t_s_s(l_447[2], (g_265[2][2] > (((safe_mul_func_int16_t_s_s((*l_445), p_71)) , (p_70 < p_70)) && (safe_add_func_uint32_t_u_u((p_70 , (safe_sub_func_uint32_t_u_u(((((0L > ((*l_438) = 0x44L)) & 0x4522L) | p_70) | 0x1EL), (-1L)))), g_144))))));
                }
            }
            l_490[1][1][2] &= (safe_lshift_func_uint16_t_u_s((!((p_71 ^ g_164[3]) == (safe_mul_func_uint16_t_u_u((g_469[0][0] != l_470), (safe_rshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u(((((((g_155[3][0][7] , (g_479 = (g_116 , l_475[1][4]))) != (l_481[1] = l_480)) || (safe_div_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((l_486 != &g_333[1]), l_487[6])), p_70))) , l_488) == (void*)0) , p_71), p_71)), 3)))))), 7));
        }
    }
    else
    { /* block id: 183 */
        int32_t l_502 = (-10L);
        int32_t l_513 = 0xCB3EF157L;
        int32_t l_560 = 0L;
        int32_t l_561[6][9][2] = {{{0xE826677CL,0L},{0x6513F886L,0L},{0x6513F886L,0L},{0xE826677CL,(-3L)},{0L,0L},{0x94430956L,1L},{0x376B4BDEL,8L},{8L,0x0995D460L},{0x5340F9EEL,(-8L)}},{{2L,0x6513F886L},{0L,(-1L)},{1L,2L},{0x0995D460L,0xEAD57540L},{(-1L),0xEAD57540L},{0x0995D460L,2L},{1L,(-1L)},{0L,0x6513F886L},{2L,(-8L)}},{{0x5340F9EEL,0x0995D460L},{8L,8L},{0x376B4BDEL,1L},{0x94430956L,0L},{0L,(-3L)},{0xE826677CL,0L},{0x6513F886L,0L},{0x6513F886L,0L},{0xE826677CL,(-3L)}},{{0L,0L},{0x94430956L,1L},{0x376B4BDEL,8L},{8L,0x0995D460L},{0x5340F9EEL,(-8L)},{2L,0x6513F886L},{0L,(-1L)},{1L,2L},{0x0995D460L,0xEAD57540L}},{{(-1L),0xEAD57540L},{0x0995D460L,2L},{1L,(-1L)},{0L,0x6513F886L},{2L,(-8L)},{0x5340F9EEL,0x0995D460L},{8L,8L},{0x376B4BDEL,1L},{0x94430956L,0L}},{{0L,(-3L)},{0xE826677CL,0L},{0x6513F886L,0L},{0x6513F886L,0L},{0xE826677CL,(-3L)},{0L,0L},{0x94430956L,1L},{0x376B4BDEL,8L},{8L,0x0995D460L}}};
        uint32_t l_563 = 4294967295UL;
        int i, j, k;
        for (g_158 = 0; (g_158 != (-14)); g_158 = safe_sub_func_uint32_t_u_u(g_158, 7))
        { /* block id: 186 */
            uint16_t l_498 = 65532UL;
            int16_t *l_534 = (void*)0;
            int16_t *l_535 = &l_330;
            float *l_546 = &g_521;
            int32_t l_548 = 0xE26C104CL;
            int32_t *l_551 = &l_349;
            int32_t *l_552 = &l_548;
            int32_t *l_553 = &l_548;
            int32_t *l_554 = &l_384[7][2][0];
            int32_t *l_555 = &l_349;
            int32_t l_556 = 1L;
            int32_t *l_557 = &l_513;
            int32_t *l_558[10] = {&g_116,&g_116,&l_556,&l_513,&l_556,&g_116,&g_116,&l_556,&l_513,&l_556};
            int8_t l_559[3];
            int i;
            for (i = 0; i < 3; i++)
                l_559[i] = 0x03L;
            for (l_448 = 0; (l_448 == 13); ++l_448)
            { /* block id: 189 */
                int32_t l_497 = 0L;
                int16_t *l_508 = &l_330;
                int16_t **l_507[6][10] = {{&l_508,&l_508,&l_508,(void*)0,(void*)0,&l_508,&l_508,&l_508,(void*)0,&l_508},{&l_508,&l_508,&l_508,(void*)0,&l_508,&l_508,&l_508,&l_508,(void*)0,&l_508},{&l_508,&l_508,(void*)0,(void*)0,&l_508,&l_508,&l_508,&l_508,(void*)0,&l_508},{&l_508,&l_508,&l_508,&l_508,&l_508,&l_508,&l_508,(void*)0,&l_508,(void*)0},{&l_508,&l_508,&l_508,(void*)0,&l_508,&l_508,&l_508,&l_508,(void*)0,&l_508},{&l_508,&l_508,&l_508,(void*)0,(void*)0,&l_508,&l_508,&l_508,(void*)0,&l_508}};
                int16_t ***l_509 = &l_507[2][5];
                int64_t *l_514 = &g_162;
                float *l_519 = (void*)0;
                float *l_520 = &g_521;
                int i, j;
                l_498 = (safe_mod_func_uint64_t_u_u((l_497 &= 0xB94A494A3481F0E7LL), 0x00450D95D2465672LL));
                (*g_518) = func_77((safe_mod_func_int64_t_s_s(((*l_514) &= (g_266[0] & ((((p_70 , (l_513 = ((+l_502) == ((l_497 ^ (safe_rshift_func_int8_t_s_s((l_497 <= (5L != (safe_mul_func_int8_t_s_s(p_71, (((*l_509) = l_507[2][5]) != (l_498 , g_510)))))), p_70))) <= 0x46L)))) , l_372) != (void*)0) , 0x8130FB83F6ED6AB0LL))), g_144)), g_116, p_70);
                (*l_520) = (g_247 = g_152);
            }
            (*g_549) = (((l_548 = ((((safe_div_func_float_f_f(((safe_sub_func_float_f_f((p_71 == ((safe_mul_func_float_f_f(((safe_mul_func_int8_t_s_s(p_71, (((*l_535) |= ((~(safe_mod_func_uint16_t_u_u(0x20A3L, 3L))) , ((!0x71L) >= p_71))) | 1L))) , ((g_247 = ((*l_546) = (safe_div_func_float_f_f((l_502 <= ((((safe_add_func_uint32_t_u_u((safe_sub_func_uint16_t_u_u((((safe_sub_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(p_70, g_166)), 253UL)) , p_71) == (-8L)), 65535UL)), p_71)) , g_244) >= p_70) != l_498)), 0x5.FBFEE2p-72)))) == 0x0.Bp-1)), g_265[4][1])) != l_498)), p_70)) <= 0x3.6p-1), 0x5.7A9F02p-13)) == (-0x5.Ap+1)) > g_547[0]) < p_70)) > l_502) == 0x8.Bp+1);
            l_563++;
            if (l_502)
                continue;
        }
    }
    for (p_71 = 0; (p_71 == 48); p_71++)
    { /* block id: 210 */
        int32_t l_579 = 0xA1C922BFL;
        const int32_t *l_581 = &g_582;
        const int32_t **l_580 = &l_581;
        float l_588 = 0xA.49A454p-11;
        int64_t *l_589[10] = {&g_162,&g_162,&g_158,&g_162,&g_158,&g_162,&g_158,(void*)0,&g_158,(void*)0};
        int16_t *l_625 = &g_249;
        int32_t l_648[4] = {2L,2L,2L,2L};
        uint8_t *l_676 = &g_57[3];
        uint8_t *l_677[3];
        uint64_t l_703 = 18446744073709551608UL;
        int16_t **l_815 = &g_792;
        const uint32_t l_838 = 18446744073709551615UL;
        int8_t l_852 = 1L;
        uint8_t l_887[8][3] = {{0x86L,0x86L,255UL},{0x86L,0x86L,255UL},{0x86L,0x86L,255UL},{0x86L,0x86L,255UL},{0x86L,0x86L,255UL},{0x86L,0x86L,255UL},{0x86L,0x86L,255UL},{0x86L,0x86L,255UL}};
        int i, j;
        for (i = 0; i < 3; i++)
            l_677[i] = (void*)0;
        if (((0x34L <= (safe_mod_func_int64_t_s_s((255UL | ((((((safe_lshift_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(((safe_add_func_int32_t_s_s(((!(l_579 ^ (((*l_580) = l_489) != (l_364 = l_372)))) != (((safe_unary_minus_func_uint64_t_u(((l_585[0][1][4] = ((l_584 = (void*)0) != (void*)0)) == (safe_mod_func_uint64_t_u_u(p_71, 0xDCC9DCD7518E83F4LL))))) , p_71) <= 0L)), 0x57B1E161L)) > l_579), l_579)), 6)) && l_579) , p_71) == 7UL) , &g_109[1][6][0]) == l_589[8])), p_71))) != 5L))
        { /* block id: 215 */
            int32_t ** const l_590 = &l_372;
            uint16_t *l_592 = &g_243;
            int8_t l_631 = (-1L);
            uint8_t l_694[8];
            int32_t l_701[8];
            int i;
            for (i = 0; i < 8; i++)
                l_694[i] = 0x58L;
            for (i = 0; i < 8; i++)
                l_701[i] = 0x951E9419L;
            if (((l_590 == ((*l_486) = (*l_486))) & (++(*l_592))))
            { /* block id: 218 */
                int32_t l_632[8] = {0x9E2A726CL,0x9E2A726CL,0x9E2A726CL,0x9E2A726CL,0x9E2A726CL,0x9E2A726CL,0x9E2A726CL,0x9E2A726CL};
                const uint64_t l_655[4][1] = {{18446744073709551614UL},{18446744073709551614UL},{18446744073709551614UL},{18446744073709551614UL}};
                int32_t l_665 = 1L;
                uint16_t l_666[4][7][8] = {{{0x37F5L,2UL,0x6AAAL,0x5348L,0xF9AEL,0x6AAAL,5UL,0x0630L},{0UL,9UL,0x6C02L,65532UL,65535UL,65535UL,0x6212L,0UL},{0x3BD6L,0x4C7DL,0x3C3CL,65531UL,65532UL,0x2A31L,0x76ACL,65531UL},{6UL,65531UL,0xED63L,0x50A8L,65528UL,0x6AAAL,0x6405L,65535UL},{65531UL,5UL,0xA8B4L,0UL,1UL,0UL,0xA8B4L,5UL},{0x5348L,0xA8B4L,65535UL,9UL,65535UL,0x4AB3L,0xEBF0L,65527UL},{0x50A8L,0x8B1DL,0UL,65527UL,0x5348L,0xE9FCL,0xEBF0L,0xE855L}},{{7UL,65527UL,65535UL,0x4AB3L,0x0EDFL,0UL,0xA8B4L,0x6405L},{0x0EDFL,0UL,0xA8B4L,0x6405L,9UL,0xBD3FL,0x6405L,7UL},{0x80B3L,65535UL,0xED63L,0x5348L,0xADC3L,1UL,0x76ACL,65531UL},{65535UL,0xCF4AL,0x3C3CL,0x272BL,0x85CAL,0x6C63L,0x6212L,0xEBF0L},{0x0630L,0UL,0x6C02L,0x3BD6L,0x3C3CL,0x94A0L,5UL,7UL},{0xCDB0L,65531UL,0x6AAAL,1UL,0x8B1DL,65535UL,0x80B3L,0UL},{65529UL,0UL,1UL,0x4AB3L,1UL,0xBB52L,0x94A0L,0x76ACL}},{{6UL,0x188DL,0UL,0xCDB0L,0UL,0x6C02L,0UL,65527UL},{0x76ACL,5UL,65529UL,65531UL,0UL,65535UL,0xCF4AL,0xADC3L},{65529UL,0x37F5L,65535UL,0UL,0x80B3L,1UL,1UL,0x80B3L},{65527UL,0x6212L,0x6212L,65527UL,0x3C3CL,0xBD3FL,0x88D3L,65531UL},{0x37F5L,0xCDB0L,65535UL,0x272BL,0x94A0L,0x8B1DL,9UL,0xBD3FL},{0UL,65535UL,0xAE4EL,65535UL,0xBB52L,1UL,0xE9FCL,65534UL},{0UL,1UL,7UL,0x6C63L,0x50F7L,65529UL,65529UL,0UL}},{{0x6C02L,0x4C7DL,65528UL,9UL,65531UL,0x50A8L,1UL,0x272BL},{0x676AL,65535UL,0x237FL,65535UL,0x6C63L,0x86E3L,65535UL,9UL},{65535UL,0xED63L,0x0A89L,9UL,65531UL,65535UL,0x4AB3L,0xFB4EL},{0x6C63L,65528UL,65528UL,0xA8B4L,9UL,0x76ACL,0x3C3CL,1UL},{0x0A89L,0UL,65533UL,7UL,0UL,0x237FL,0xE9FCL,1UL},{1UL,65535UL,0x26B7L,0UL,65535UL,0x9304L,0x4C7DL,1UL},{0x188DL,65534UL,0x76ACL,0xCC31L,1UL,0x0630L,7UL,0UL}}};
                int i, j, k;
                for (g_125 = 4; (g_125 <= 16); g_125 = safe_add_func_int16_t_s_s(g_125, 1))
                { /* block id: 221 */
                    uint32_t l_628[4][9][7] = {{{0x2DFE6C2CL,0x4262AFF8L,0x4262AFF8L,0x2DFE6C2CL,7UL,0x8725E487L,0xDCA8E724L},{0xCB362264L,0x92A160DCL,4294967295UL,4UL,4UL,4294967295UL,0x92A160DCL},{7UL,0x02FB1278L,0x05CAF742L,4294967295UL,0x4262AFF8L,0xDCA8E724L,0xDCA8E724L},{2UL,0xCB362264L,4UL,0xCB362264L,2UL,0x4EF03394L,9UL},{0x7024F495L,0x8725E487L,0x02FB1278L,4294967295UL,8UL,4294967295UL,0x02FB1278L},{9UL,9UL,0xCCA99FBCL,4UL,0x92A160DCL,0x7E685011L,4294967295UL},{0x7024F495L,4294967295UL,0x2DFE6C2CL,0x2DFE6C2CL,4294967295UL,0x7024F495L,8UL},{2UL,0xCCA99FBCL,0xCB362264L,1UL,0x92A160DCL,0x92A160DCL,1UL},{7UL,1UL,7UL,0xDCA8E724L,8UL,0x05CAF742L,0x7024F495L}},{{0xCB362264L,0xCCA99FBCL,2UL,0x7E685011L,2UL,0xCCA99FBCL,0xCB362264L},{0x2DFE6C2CL,4294967295UL,0x7024F495L,8UL,0x4262AFF8L,0x05CAF742L,0x4262AFF8L},{0xCCA99FBCL,9UL,9UL,0xCCA99FBCL,4UL,0x92A160DCL,0x7E685011L},{0x02FB1278L,0x8725E487L,0x7024F495L,7UL,7UL,0x7024F495L,0x8725E487L},{4UL,0xCB362264L,2UL,0x4EF03394L,9UL,0x7E685011L,0x7E685011L},{0x05CAF742L,0x02FB1278L,7UL,0x02FB1278L,0x05CAF742L,4294967295UL,0x4262AFF8L},{4294967295UL,0x92A160DCL,0xCB362264L,0x4EF03394L,1UL,0x4EF03394L,0xCB362264L},{0x4262AFF8L,0xDCA8E724L,0x4262AFF8L,4294967295UL,0x05CAF742L,0x02FB1278L,7UL},{4UL,0xCCA99FBCL,9UL,9UL,0xCCA99FBCL,4UL,0x92A160DCL}},{{0x7024F495L,0x4262AFF8L,1UL,0x8725E487L,0x05CAF742L,0x05CAF742L,0x8725E487L},{0x4EF03394L,1UL,0x4EF03394L,0xCB362264L,0x92A160DCL,4294967295UL,4UL},{1UL,0x4262AFF8L,0x7024F495L,0x02FB1278L,0x7024F495L,0x4262AFF8L,1UL},{9UL,0xCCA99FBCL,4UL,0x92A160DCL,0x7E685011L,4294967295UL,0x7E685011L},{0x4262AFF8L,0xDCA8E724L,0xDCA8E724L,0x4262AFF8L,4294967295UL,0x05CAF742L,0x02FB1278L},{0x25759A99L,2UL,4UL,0x4EF03394L,0x4EF03394L,4UL,2UL},{4294967295UL,1UL,0x7024F495L,0x2DFE6C2CL,0xDCA8E724L,0x02FB1278L,0x02FB1278L},{4294967295UL,0x25759A99L,0x4EF03394L,0x25759A99L,4294967295UL,0xCCA99FBCL,0x7E685011L},{7UL,0x05CAF742L,1UL,0x2DFE6C2CL,0x8725E487L,0x2DFE6C2CL,1UL}},{{0x7E685011L,0x7E685011L,9UL,0x4EF03394L,2UL,0xCB362264L,4UL},{7UL,0x2DFE6C2CL,0x4262AFF8L,0x4262AFF8L,0x2DFE6C2CL,7UL,0x8725E487L},{4294967295UL,9UL,0x25759A99L,0x92A160DCL,2UL,2UL,0x92A160DCL},{4294967295UL,8UL,4294967295UL,0x02FB1278L,0x8725E487L,0x7024F495L,7UL},{0x25759A99L,9UL,4294967295UL,0xCB362264L,4294967295UL,9UL,0x25759A99L},{0x4262AFF8L,0x2DFE6C2CL,7UL,0x8725E487L,0xDCA8E724L,0x7024F495L,0xDCA8E724L},{9UL,0x7E685011L,0x7E685011L,9UL,0x4EF03394L,2UL,0xCB362264L},{1UL,0x05CAF742L,7UL,4294967295UL,4294967295UL,7UL,0x05CAF742L},{0x4EF03394L,0x25759A99L,4294967295UL,0xCCA99FBCL,0x7E685011L,0xCB362264L,0xCB362264L}}};
                    uint64_t l_633 = 0x6447816CA5C67265LL;
                    int8_t l_647 = 2L;
                    int32_t *l_649 = (void*)0;
                    int64_t *l_671 = &g_109[9][4][0];
                    int32_t l_678 = 0x34703A85L;
                    int8_t *l_695 = &l_647;
                    int32_t *l_696 = (void*)0;
                    int32_t *l_697 = &l_384[1][5][0];
                    int i, j, k;
                    if (p_71)
                    { /* block id: 222 */
                        uint64_t *l_618 = &l_420[2];
                        int8_t *l_623 = &l_591;
                        int16_t * const l_624 = &g_547[0];
                        int32_t l_630 = 0x65283462L;
                        float *l_634[6][1];
                        int i, j;
                        for (i = 0; i < 6; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_634[i][j] = &g_550;
                        }
                        g_259 = ((*g_549) = (safe_div_func_float_f_f(((safe_div_func_float_f_f((((safe_sub_func_float_f_f((*g_549), (safe_add_func_float_f_f(((safe_mul_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_u((+0x5FE3L), ((safe_div_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u((safe_div_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u(1UL, ((*l_618)++))), (((((g_125 >= (((*l_623) = p_70) >= (l_624 != l_625))) | ((safe_sub_func_int8_t_s_s(((void*)0 != &l_624), l_628[3][1][3])) < g_629)) || p_71) > p_70) ^ l_630))), l_631)) , l_632[1]), p_70)) || 0x10805F7B690733FBLL))) & l_633), g_244)) , g_160[0][1][0]), g_521)))) != p_71) , p_70), g_129)) <= 0x5.F64115p+51), 0x5.3C3AA2p+62)));
                        l_648[1] |= ((((*l_489)++) & 0x75E19EB9L) , (g_6[1] && ((l_632[1] = ((safe_rshift_func_uint8_t_u_u(((18446744073709551610UL == (1L == (l_579 <= (safe_sub_func_uint32_t_u_u(((**l_488) &= 4294967295UL), ((p_71 != (safe_div_func_int8_t_s_s((safe_add_func_int32_t_s_s((safe_add_func_int32_t_s_s(2L, 0x966C0AD4L)), g_244)), l_647))) != 0x5C2A613AL)))))) > l_632[1]), 3)) <= 253UL)) > l_647)));
                        (*l_590) = &l_632[1];
                        (*l_590) = l_649;
                    }
                    else
                    { /* block id: 233 */
                        float *l_672[1][1];
                        uint8_t **l_675[6][2][5] = {{{&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1]},{&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0]}},{{&g_219[0][0][1],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][1]},{&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1]}},{{&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0]},{&g_219[0][0][1],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][1]}},{{&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1]},{&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0]}},{{&g_219[0][0][1],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][1]},{&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1]}},{{&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][0]},{&g_219[0][0][0],&g_219[0][0][0],&g_219[0][0][1],&g_219[0][0][0],&g_219[0][0][0]}}};
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_672[i][j] = &g_550;
                        }
                        if (l_628[3][1][3])
                            break;
                        l_666[3][6][0] = (safe_add_func_uint16_t_u_u((safe_unary_minus_func_int32_t_s(((((safe_lshift_func_int16_t_s_u(l_655[3][0], (++(*l_592)))) && 2L) , (safe_rshift_func_int16_t_s_s(((safe_unary_minus_func_int16_t_s((*g_511))) & 0UL), ((8L ^ p_70) & (safe_mod_func_uint64_t_u_u((l_632[3] >= g_629), (safe_rshift_func_int8_t_s_s((g_109[9][4][0] , 0xC4L), 1)))))))) || l_665))), p_70));
                        (*l_590) = (((safe_div_func_int32_t_s_s((safe_rshift_func_int16_t_s_u(((*l_625) &= ((void*)0 != l_671)), (l_649 == l_672[0][0]))), p_70)) , (g_247 = (safe_sub_func_float_f_f((l_678 = ((l_676 = (void*)0) == (l_677[1] = &p_70))), (l_632[7] = ((*g_549) = (0x1.4DB3E9p-75 >= ((safe_mul_func_uint8_t_u_u(((void*)0 != (*g_510)), l_628[3][1][3])) , l_666[3][6][0])))))))) , (void*)0);
                    }
                    (*g_115) = p_71;
                    (*l_697) |= (((*l_695) = ((safe_mod_func_uint32_t_u_u((+(safe_mul_func_uint16_t_u_u(((((safe_div_func_uint8_t_u_u((l_690 = (((l_579 | 0x6CL) >= (0L == ((*l_592)++))) , ((void*)0 != &g_55))), ((&l_666[3][5][2] != (void*)0) | p_70))) >= (safe_rshift_func_uint8_t_u_u((safe_unary_minus_func_uint64_t_u((0L == 1UL))), l_648[1]))) <= l_694[1]) == (**g_510)), l_648[3]))), l_648[2])) <= p_70)) , (-1L));
                }
                for (p_70 = 0; (p_70 >= 39); ++p_70)
                { /* block id: 254 */
                    int32_t *l_700[7][9] = {{&l_384[1][5][0],&g_116,&l_349,(void*)0,&l_349,&l_648[1],&g_116,&l_648[1],&l_349},{(void*)0,(void*)0,(void*)0,(void*)0,&l_648[1],&l_383,&l_665,(void*)0,&l_648[3]},{&l_384[1][5][0],&l_349,&g_116,&l_579,&l_579,&g_116,&l_349,&l_384[1][5][0],&g_6[0]},{(void*)0,&l_632[1],&l_648[3],(void*)0,&l_648[1],(void*)0,(void*)0,&l_648[1],(void*)0},{(void*)0,&l_383,(void*)0,&g_116,&l_349,&l_579,&l_648[1],&g_6[0],&g_6[0]},{(void*)0,(void*)0,&l_384[5][0][0],(void*)0,&l_384[5][0][0],(void*)0,(void*)0,&l_665,&l_648[3]},{&l_648[1],&l_579,&l_349,&g_116,(void*)0,&l_383,(void*)0,&g_116,&l_349}};
                    float l_702 = 0xA.113D85p+17;
                    int i, j;
                    l_703--;
                    (*l_590) = (void*)0;
                    for (l_448 = 0; (l_448 < 14); ++l_448)
                    { /* block id: 259 */
                        return (*l_486);
                    }
                }
            }
            else
            { /* block id: 263 */
                int32_t *l_708 = &l_384[0][0][0];
                int32_t *l_709 = (void*)0;
                int32_t *l_710 = (void*)0;
                int32_t *l_711 = &l_648[1];
                int32_t *l_712 = &l_701[6];
                int32_t *l_713 = (void*)0;
                int32_t *l_714 = &l_383;
                int32_t *l_715 = &l_383;
                int32_t *l_716 = (void*)0;
                int32_t *l_717 = &l_384[1][5][0];
                int32_t *l_718 = &l_349;
                int32_t *l_719 = &l_384[7][6][1];
                int32_t l_720 = 1L;
                int32_t *l_721 = &l_384[7][0][1];
                int32_t *l_722[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_722[i] = &l_648[1];
                l_723++;
            }
            (*l_590) = (*g_518);
        }
        else
        { /* block id: 267 */
            float l_726 = 0x4.483325p-94;
            int32_t *l_727 = &l_648[1];
            int32_t *l_728 = (void*)0;
            int32_t *l_729 = &l_579;
            int32_t *l_730 = &l_383;
            int32_t *l_731 = &l_579;
            int32_t *l_732 = &l_349;
            int32_t *l_733 = (void*)0;
            int32_t *l_734 = &l_648[1];
            int32_t *l_735 = (void*)0;
            int32_t *l_736 = &l_384[1][5][0];
            int32_t *l_737 = &l_648[1];
            int32_t *l_738 = &l_648[1];
            int32_t *l_739[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t l_740 = 0x8D3FF268L;
            int16_t *l_795 = &g_271[4];
            uint64_t l_875 = 7UL;
            int i;
            l_740--;
            (*l_730) ^= (*l_729);
            for (g_129 = 24; (g_129 >= 45); g_129 = safe_add_func_uint8_t_u_u(g_129, 8))
            { /* block id: 272 */
                int32_t l_745[5][1] = {{(-3L)},{0x5D557DE2L},{(-3L)},{0x5D557DE2L},{(-3L)}};
                int32_t l_776 = (-5L);
                uint64_t *l_799 = &l_420[2];
                int16_t **l_813 = (void*)0;
                int32_t **l_818 = &l_734;
                int32_t l_841 = 2L;
                int32_t l_843 = 0x4D423089L;
                int32_t l_847 = (-9L);
                int32_t l_848 = (-7L);
                int32_t l_858 = (-10L);
                int32_t l_859 = 1L;
                uint32_t l_884 = 0xDF8889CAL;
                int i, j;
                --l_746;
            }
        }
    }
    return (*l_486);
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_6 g_59 g_55 g_115 g_116 g_109 g_129 g_151 g_166 g_152 g_160 g_164 g_240 g_144 g_249 g_158 g_260 g_266 g_271 g_125 g_247 g_162
 * writes: g_109 g_59 g_116 g_125 g_129 g_144 g_151 g_166 g_219 g_243 g_244 g_249 g_260 g_266 g_247 g_162 g_164
 */
static uint32_t * func_77(int16_t  p_78, const int16_t  p_79, uint8_t  p_80)
{ /* block id: 20 */
    float l_101 = (-0x1.Bp+1);
    int32_t l_102 = 0x2FCEF8C3L;
    uint8_t *l_107 = &g_57[3];
    int32_t l_118[5];
    int32_t *l_205 = (void*)0;
    int32_t **l_204 = &l_205;
    int i;
    for (i = 0; i < 5; i++)
        l_118[i] = (-1L);
    for (p_80 = 0; (p_80 <= 9); p_80 += 1)
    { /* block id: 23 */
        int64_t *l_108 = &g_109[9][4][0];
        uint32_t *l_110 = (void*)0;
        uint32_t *l_111 = &g_59[0];
        int32_t l_131 = (-9L);
        int32_t l_154 = (-5L);
        int32_t l_156 = 8L;
        int8_t l_157[3];
        int32_t l_159 = 1L;
        int32_t l_163 = 0x5BAE7EB1L;
        int32_t **l_206 = (void*)0;
        int32_t **l_239 = &l_205;
        int32_t ***l_238 = &l_239;
        uint32_t l_245 = 18446744073709551609UL;
        uint32_t l_279 = 0UL;
        const int16_t *l_283 = &g_249;
        int16_t *l_284 = &g_271[4];
        int i;
        for (i = 0; i < 3; i++)
            l_157[i] = 0xBAL;
        if ((((((((*l_111) = (((*l_108) = (((((((safe_add_func_uint16_t_u_u(((safe_add_func_float_f_f(g_57[p_80], 0x0.Cp-1)) , ((safe_mul_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((safe_div_func_int32_t_s_s((0x36CF92AC6B8A9B06LL || (g_6[1] | 0xE977L)), (((((((safe_sub_func_uint64_t_u_u((!g_6[1]), ((((safe_sub_func_int32_t_s_s((safe_div_func_uint32_t_u_u(l_102, ((g_57[4] ^ (safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(g_57[p_80], l_102)), p_78))) , 4294967287UL))), g_57[2])) == p_78) , l_107) == &p_80))) , l_102) , (void*)0) != &g_57[3]) <= p_80) && 1UL) & g_59[0]))) != p_78), g_59[0])), 0xEFL)) | (-3L))), g_6[1])) & 0xDD1FAEF758CFF8D1LL) , g_57[p_80]) || p_78) <= g_55) | g_55) && 0xE6L)) || p_79)) , l_102) || (-10L)) <= g_57[p_80]) | 65533UL) | p_79))
        { /* block id: 26 */
            int32_t *l_112 = (void*)0;
            int32_t l_113 = 0L;
            int32_t *l_117[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            (*g_115) &= ((l_113 &= (0x2FE657869F8A89CBLL | 0L)) && ((safe_unary_minus_func_int16_t_s(4L)) <= 4L));
            l_118[4] &= (-9L);
            return &g_55;
        }
        else
        { /* block id: 31 */
            float l_123 = 0x3.9p+1;
            int8_t *l_124[2][8] = {{&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125},{&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125}};
            uint64_t *l_128[4][5] = {{&g_129,&g_129,&g_129,&g_129,&g_129},{&g_129,(void*)0,&g_129,(void*)0,&g_129},{&g_129,&g_129,&g_129,&g_129,&g_129},{&g_129,(void*)0,&g_129,(void*)0,&g_129}};
            int32_t l_130 = (-1L);
            int32_t *l_137[5][2][8] = {{{&g_6[1],(void*)0,&l_131,&l_118[4],&g_6[0],&g_6[1],&g_6[1],&l_131},{&l_118[4],&l_118[3],&l_130,&g_6[5],&g_6[5],&l_130,&l_118[3],&l_118[4]}},{{&g_6[4],&l_118[4],(void*)0,&g_6[1],&g_6[1],&g_6[0],&l_118[0],(void*)0},{&g_6[5],&l_102,&l_131,&g_6[1],&g_6[1],&g_6[0],(void*)0,(void*)0}},{{(void*)0,&l_118[4],&g_116,&l_118[4],&l_118[4],&l_130,(void*)0,&g_6[1]},{(void*)0,&l_118[3],(void*)0,&g_6[1],(void*)0,&g_6[1],(void*)0,&l_118[3]}},{{&g_6[5],(void*)0,&g_6[0],&l_118[4],&l_118[0],&l_131,&g_6[1],&l_118[0]},{&l_118[4],&g_6[1],(void*)0,&l_118[4],&g_6[5],&l_130,&g_6[1],(void*)0}},{{(void*)0,&l_118[4],&g_6[0],&l_130,(void*)0,(void*)0,&l_118[4],&l_102},{(void*)0,(void*)0,&l_118[4],&l_102,&l_118[4],&l_118[4],&l_102,&l_118[4]}}};
            float l_161 = 0xF.02EEB5p-43;
            const uint32_t **l_197 = (void*)0;
            const uint32_t **l_198 = (void*)0;
            const uint32_t *l_200[1];
            const uint32_t **l_199 = &l_200[0];
            int16_t l_202 = 7L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_200[i] = &g_201;
            (*g_115) = (g_6[1] >= (safe_mul_func_int16_t_s_s((((safe_mod_func_uint8_t_u_u(g_109[6][1][1], ((-8L) & ((g_125 = g_109[6][1][1]) , (safe_div_func_int8_t_s_s((0xAB7D55596E0544DDLL || (--g_129)), (+g_6[1]))))))) >= (safe_mod_func_int32_t_s_s(0x1348BD33L, g_59[0]))) ^ (((((p_79 < 0x483898E2L) , (void*)0) != &l_131) & l_118[3]) | l_130)), g_57[8])));
            g_151 &= (p_79 ^ ((*g_115) = (1L <= ((((*l_111) = ((safe_lshift_func_int16_t_s_s(0xD8A5L, (safe_sub_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((g_144 = p_79), (safe_add_func_uint16_t_u_u(3UL, ((safe_div_func_int64_t_s_s(((*l_108) = (safe_div_func_int64_t_s_s((((g_57[p_80] , (l_118[4] > ((((-10L) != (1UL || p_80)) , 0x27B5L) ^ p_79))) , (void*)0) == (void*)0), p_78))), g_59[1])) == 0L))))), 0x7645L)))) , p_79)) > 0xC90BC308L) < 8UL))));
            g_166--;
            l_102 |= (safe_mul_func_uint8_t_u_u((safe_div_func_int64_t_s_s((p_79 || (((!(safe_unary_minus_func_uint16_t_u((p_79 && (((safe_mul_func_uint16_t_u_u(((void*)0 == &l_137[2][0][6]), g_152)) , ((safe_lshift_func_uint16_t_u_s((((safe_lshift_func_int16_t_s_s((-10L), 3)) & (safe_sub_func_int32_t_s_s(((safe_sub_func_int16_t_s_s(((safe_sub_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((+l_118[3]), 65530UL)), ((~(safe_rshift_func_uint16_t_u_u((safe_div_func_int32_t_s_s(((safe_mul_func_uint8_t_u_u((((((*l_199) = (void*)0) != (void*)0) && g_160[0][1][0]) , 9UL), g_57[7])) ^ g_109[5][3][2]), p_78)), p_78))) > l_202))) < l_118[4]), l_159)) >= 0x1591L), 1UL))) > l_118[4]), 0)) | g_55)) | p_78))))) >= g_160[0][1][0]) || g_164[3])), g_55)), p_79));
        }
        if (((0x07F9E03FE52CA10BLL && ((safe_unary_minus_func_int16_t_s(0x6A7CL)) , g_57[p_80])) ^ (251UL && (65535UL <= (l_204 != l_206)))))
        { /* block id: 44 */
            int32_t *l_207 = &l_131;
            int32_t *l_208 = &l_154;
            (*l_207) |= 0L;
            if ((*g_115))
                continue;
            (*l_208) &= ((*l_207) = (*g_115));
        }
        else
        { /* block id: 49 */
            int32_t ***l_227 = &l_206;
            int32_t ***l_228 = &l_204;
            uint64_t *l_235[7];
            uint16_t *l_241 = (void*)0;
            uint16_t *l_242 = &g_243;
            int16_t *l_246 = (void*)0;
            int16_t *l_248 = &g_249;
            int32_t l_258 = (-8L);
            const uint64_t l_272 = 0xCD77AE1F0B0F387ALL;
            uint64_t l_301 = 0xB4F6941FB2E3F941LL;
            int i;
            for (i = 0; i < 7; i++)
                l_235[i] = &g_129;
            if ((safe_lshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s(((*l_248) |= ((((safe_lshift_func_uint16_t_u_s((((g_219[0][0][0] = &p_80) == ((((safe_rshift_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((((safe_unary_minus_func_int8_t_s((safe_div_func_int8_t_s_s((((*l_227) = &l_205) != ((*l_228) = &l_205)), 1UL)))) | p_78) == (safe_sub_func_int32_t_s_s((((((safe_add_func_uint64_t_u_u((g_244 = (safe_lshift_func_int16_t_s_u(((g_129 ^= 18446744073709551615UL) >= 18446744073709551613UL), ((*l_242) = (p_78 && ((safe_sub_func_int64_t_s_s(((l_238 == g_240) , p_80), g_109[9][4][0])) >= g_57[9])))))), p_80)) , 0x45L) ^ g_164[3]) , p_79) , 0x225D925FL), g_144))), p_80)), p_78)) != l_245) <= p_80) , &p_80)) > p_79), p_80)) | p_80) , 0UL) ^ (-9L))), g_59[2])), 0x9E9BL)), g_158)), p_79)))
            { /* block id: 57 */
                int32_t *l_250 = &l_118[4];
                int32_t *l_251 = &l_131;
                int32_t *l_252 = &g_116;
                int32_t *l_253 = &l_131;
                int32_t *l_254 = &l_118[0];
                int32_t *l_255 = &l_102;
                int32_t *l_256 = &l_156;
                int32_t *l_257[6][3] = {{&l_131,&l_131,&l_131},{&g_116,&l_131,&l_131},{&l_131,&g_116,&l_131},{&l_131,&l_131,&l_131},{&g_116,&l_131,&l_131},{&l_131,&g_116,&l_131}};
                int i, j;
                ++g_260;
                for (g_129 = 0; (g_129 <= 2); g_129 += 1)
                { /* block id: 61 */
                    int32_t l_263 = (-9L);
                    int i;
                    (*l_251) ^= g_59[g_129];
                    l_263 |= g_59[g_129];
                    if (p_79)
                    { /* block id: 64 */
                        (*l_256) = (&p_78 != &p_79);
                        if (p_78)
                            continue;
                    }
                    else
                    { /* block id: 67 */
                        int32_t l_264[7][1] = {{0x36B3F3D9L},{0x36B3F3D9L},{0xD280DFA0L},{0x36B3F3D9L},{0x36B3F3D9L},{0xD280DFA0L},{0x36B3F3D9L}};
                        int i, j;
                        if (p_78)
                            break;
                        ++g_266[0];
                    }
                    (*l_250) ^= (((safe_lshift_func_int16_t_s_s((p_79 , (p_78 > ((g_271[4] , l_272) , (&g_57[3] == (void*)0)))), (((&g_109[9][4][0] != ((((p_79 , 9UL) , l_263) & (*g_115)) , (void*)0)) & g_271[4]) < g_164[3]))) , g_57[4]) , (*g_115));
                }
                (*l_239) = (*l_204);
            }
            else
            { /* block id: 74 */
                int32_t *l_282 = &g_116;
                int16_t **l_285 = (void*)0;
                int16_t **l_286 = &l_246;
                for (g_249 = 9; (g_249 >= 0); g_249 -= 1)
                { /* block id: 77 */
                    int32_t *l_273 = &g_116;
                    int32_t *l_274 = &l_102;
                    int32_t *l_275 = (void*)0;
                    int32_t *l_276 = &l_156;
                    int32_t *l_277 = &l_131;
                    int32_t *l_278[2][3][7] = {{{&l_163,(void*)0,&l_163,&l_156,&l_163,&l_163,&l_156},{&l_159,&g_6[1],&l_159,&l_163,(void*)0,(void*)0,&l_156},{&l_159,&l_102,&g_6[1],&l_163,&l_156,&l_159,&l_258}},{{&g_6[1],&l_159,(void*)0,&l_102,&l_102,(void*)0,&l_159},{&l_258,&l_131,&l_159,&g_6[1],&l_102,&l_163,&l_118[0]},{&g_6[1],&l_159,&l_131,&l_102,&l_156,&g_6[1],&l_159}}};
                    int i, j, k;
                    if (p_79)
                        break;
                    ++l_279;
                }
                (*l_282) = 0L;
                if ((l_283 != ((*l_286) = (l_284 = l_246))))
                { /* block id: 84 */
                    return l_282;
                }
                else
                { /* block id: 86 */
                    int8_t l_298 = (-8L);
                    (**l_238) = l_282;
                    for (l_131 = 9; (l_131 >= 0); l_131 -= 1)
                    { /* block id: 90 */
                        uint16_t l_302 = 0xF3FDL;
                        float *l_303 = &g_247;
                        float *l_304 = &l_101;
                        (*l_304) = (((*l_303) = (safe_mul_func_float_f_f((-0x1.8p-1), (((g_144 <= (safe_add_func_float_f_f((!((!((*l_242) = (safe_rshift_func_uint16_t_u_u(((safe_unary_minus_func_int32_t_s((safe_lshift_func_uint16_t_u_u((((l_298 , (l_301 = (safe_div_func_uint8_t_u_u(p_78, (((void*)0 != &l_298) || (0x49L && p_79)))))) , g_57[5]) , 65528UL), g_125)))) , p_78), (**l_239))))) , l_302)), l_302))) > l_298) < g_247)))) , (*l_282));
                        if (p_78)
                            continue;
                    }
                }
            }
        }
        for (g_162 = 2; (g_162 >= 0); g_162 -= 1)
        { /* block id: 102 */
            int i;
            if (g_59[g_162])
                break;
            for (l_245 = 0; (l_245 <= 9); l_245 += 1)
            { /* block id: 106 */
                int32_t *l_305 = &g_164[2];
                (*l_239) = (((*l_305) &= g_162) , l_305);
            }
        }
    }
    return l_205;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_6[i], "g_6[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_55, "g_55", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_57[i], "g_57[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_59[i], "g_59[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_109[i][j][k], "g_109[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    transparent_crc(g_144, "g_144", print_hash_value);
    transparent_crc(g_151, "g_151", print_hash_value);
    transparent_crc(g_152, "g_152", print_hash_value);
    transparent_crc_bytes (&g_153, sizeof(g_153), "g_153", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_155[i][j][k], "g_155[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_158, "g_158", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_160[i][j][k], "g_160[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_162, "g_162", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_164[i], "g_164[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc_bytes(&g_165[i][j], sizeof(g_165[i][j]), "g_165[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_166, "g_166", print_hash_value);
    transparent_crc(g_201, "g_201", print_hash_value);
    transparent_crc(g_243, "g_243", print_hash_value);
    transparent_crc(g_244, "g_244", print_hash_value);
    transparent_crc_bytes (&g_247, sizeof(g_247), "g_247", print_hash_value);
    transparent_crc(g_249, "g_249", print_hash_value);
    transparent_crc_bytes (&g_259, sizeof(g_259), "g_259", print_hash_value);
    transparent_crc(g_260, "g_260", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_265[i][j], "g_265[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_266[i], "g_266[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_271[i], "g_271[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_311, "g_311", print_hash_value);
    transparent_crc(g_341, "g_341", print_hash_value);
    transparent_crc(g_512, "g_512", print_hash_value);
    transparent_crc_bytes (&g_521, sizeof(g_521), "g_521", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_547[i], "g_547[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_550, sizeof(g_550), "g_550", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_562[i], "g_562[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_582, "g_582", print_hash_value);
    transparent_crc(g_629, "g_629", print_hash_value);
    transparent_crc(g_798, "g_798", print_hash_value);
    transparent_crc(g_806, "g_806", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_810[i][j][k], "g_810[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_898[i][j], "g_898[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_960[i][j], "g_960[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1059, "g_1059", print_hash_value);
    transparent_crc(g_1125, "g_1125", print_hash_value);
    transparent_crc(g_1126, "g_1126", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1141[i][j][k], "g_1141[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1210[i], "g_1210[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1211, "g_1211", print_hash_value);
    transparent_crc(g_1212, "g_1212", print_hash_value);
    transparent_crc(g_1581, "g_1581", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1582[i], "g_1582[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1788, "g_1788", print_hash_value);
    transparent_crc(g_1837, "g_1837", print_hash_value);
    transparent_crc(g_1887, "g_1887", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1953[i], "g_1953[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1979, sizeof(g_1979), "g_1979", print_hash_value);
    transparent_crc_bytes (&g_2065, sizeof(g_2065), "g_2065", print_hash_value);
    transparent_crc_bytes (&g_2066, sizeof(g_2066), "g_2066", print_hash_value);
    transparent_crc(g_2095, "g_2095", print_hash_value);
    transparent_crc(g_2159, "g_2159", print_hash_value);
    transparent_crc(g_2169, "g_2169", print_hash_value);
    transparent_crc(g_2180, "g_2180", print_hash_value);
    transparent_crc(g_2188, "g_2188", print_hash_value);
    transparent_crc(g_2190, "g_2190", print_hash_value);
    transparent_crc(g_2212, "g_2212", print_hash_value);
    transparent_crc(g_2292, "g_2292", print_hash_value);
    transparent_crc(g_2387, "g_2387", print_hash_value);
    transparent_crc_bytes (&g_2411, sizeof(g_2411), "g_2411", print_hash_value);
    transparent_crc(g_2608, "g_2608", print_hash_value);
    transparent_crc(g_2615, "g_2615", print_hash_value);
    transparent_crc(g_2634, "g_2634", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_2657[i][j], "g_2657[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2867, "g_2867", print_hash_value);
    transparent_crc(g_2950, "g_2950", print_hash_value);
    transparent_crc(g_2989, "g_2989", print_hash_value);
    transparent_crc(g_3057, "g_3057", print_hash_value);
    transparent_crc(g_3090, "g_3090", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 767
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 56
breakdown:
   depth: 1, occurrence: 203
   depth: 2, occurrence: 52
   depth: 3, occurrence: 2
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 10, occurrence: 1
   depth: 15, occurrence: 4
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 3
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 31, occurrence: 3
   depth: 34, occurrence: 3
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 2
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1
   depth: 56, occurrence: 1

XXX total number of pointers: 604

XXX times a variable address is taken: 1171
XXX times a pointer is dereferenced on RHS: 409
breakdown:
   depth: 1, occurrence: 334
   depth: 2, occurrence: 52
   depth: 3, occurrence: 19
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 393
breakdown:
   depth: 1, occurrence: 358
   depth: 2, occurrence: 23
   depth: 3, occurrence: 12
XXX times a pointer is compared with null: 59
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 31
XXX times a pointer is qualified to be dereferenced: 12090

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2512
   level: 2, occurrence: 303
   level: 3, occurrence: 194
   level: 4, occurrence: 75
   level: 5, occurrence: 42
XXX number of pointers point to pointers: 244
XXX number of pointers point to scalars: 360
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30.5
XXX average alias set size: 1.42

XXX times a non-volatile is read: 2379
XXX times a non-volatile is write: 1143
XXX times a volatile is read: 124
XXX    times read thru a pointer: 31
XXX times a volatile is write: 49
XXX    times written thru a pointer: 10
XXX times a volatile is available for access: 1.72e+03
XXX percentage of non-volatile access: 95.3

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 206
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 17
   depth: 2, occurrence: 42
   depth: 3, occurrence: 40
   depth: 4, occurrence: 43
   depth: 5, occurrence: 36

XXX percentage a fresh-made variable is used: 15.1
XXX percentage an existing variable is used: 84.9
********************* end of statistics **********************/


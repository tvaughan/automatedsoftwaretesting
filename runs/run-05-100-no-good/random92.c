/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      4276439574
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   signed f0 : 25;
   unsigned f1 : 10;
   volatile unsigned f2 : 4;
   signed : 0;
   unsigned f3 : 15;
   signed f4 : 10;
   signed : 0;
   int32_t  f5;
};
#pragma pack(pop)

union U1 {
   uint32_t  f0;
   int64_t  f1;
   volatile unsigned f2 : 23;
   int8_t * f3;
   int16_t  f4;
};

/* --- GLOBAL VARIABLES --- */
static uint8_t g_4 = 0x83L;
static int8_t g_13 = 6L;
static uint16_t g_23 = 0xF689L;
static int32_t g_38 = 0x3C10B42BL;
static int32_t * volatile g_37 = &g_38;/* VOLATILE GLOBAL g_37 */
static uint16_t g_43 = 0x8837L;
static struct S0 g_65 = {-4267,0,2,169,-26,3L};/* VOLATILE GLOBAL g_65 */
static uint64_t g_73 = 18446744073709551615UL;
static uint32_t g_78 = 1UL;
static uint32_t g_97 = 4294967295UL;
static int8_t g_99 = (-1L);
static int32_t g_102 = 1L;
static volatile uint32_t g_110 = 0x947F7088L;/* VOLATILE GLOBAL g_110 */
static uint64_t **g_121 = (void*)0;
static float *** volatile g_127 = (void*)0;/* VOLATILE GLOBAL g_127 */
static uint8_t g_160 = 252UL;
static volatile int32_t g_171[5] = {1L,1L,1L,1L,1L};
static volatile int32_t g_180 = (-4L);/* VOLATILE GLOBAL g_180 */
static volatile union U1 g_185[8][5][6] = {{{{0UL},{6UL},{0UL},{0x87C8608FL},{0x6D48F99CL},{0x4C534E02L}},{{1UL},{0UL},{4294967295UL},{0x7B5E051AL},{0x4B5F3555L},{4294967295UL}},{{1UL},{0x87C8608FL},{0x7B5E051AL},{0x87C8608FL},{1UL},{0x6D48F99CL}},{{0UL},{0x6D48F99CL},{0x4B5F3555L},{1UL},{4294967295UL},{4294967295UL}},{{0x2A9956A4L},{0x4C534E02L},{4294967295UL},{0x6D48F99CL},{4294967295UL},{4294967295UL}}},{{{0x29AA79E0L},{0x4B5F3555L},{0x4B5F3555L},{0x29AA79E0L},{0xD30E9876L},{0x6D48F99CL}},{{4294967295UL},{0x319FEF01L},{0x7B5E051AL},{6UL},{0x87C8608FL},{1UL}},{{6UL},{0x87C8608FL},{4294967295UL},{0x2A9956A4L},{0x6D48F99CL},{0x2A9956A4L}},{{0xD30E9876L},{0x4C534E02L},{0xD30E9876L},{0UL},{0x319FEF01L},{0x87C8608FL}},{{0x7B5E051AL},{4294967295UL},{0UL},{1UL},{0xB47FFCAFL},{0x319FEF01L}}},{{{0x29AA79E0L},{0x2A9956A4L},{0UL},{1UL},{1UL},{0UL}},{{0x7B5E051AL},{0x7B5E051AL},{0x4C534E02L},{0UL},{0UL},{0xD30E9876L}},{{0xD30E9876L},{0x6D48F99CL},{0x7B5E051AL},{0x2A9956A4L},{4294967295UL},{0x4C534E02L}},{{6UL},{0xD30E9876L},{0x7B5E051AL},{0x29AA79E0L},{0x7B5E051AL},{0xD30E9876L}},{{0xB47FFCAFL},{0x29AA79E0L},{0x4C534E02L},{4294967295UL},{0x4B5F3555L},{0UL}}},{{{4294967295UL},{0x4B5F3555L},{0UL},{0x7B5E051AL},{4294967295UL},{0x319FEF01L}},{{0x87C8608FL},{0x4B5F3555L},{0UL},{0UL},{0x4B5F3555L},{0x87C8608FL}},{{0x4B5F3555L},{0x29AA79E0L},{0xD30E9876L},{0x6D48F99CL},{0x7B5E051AL},{0x2A9956A4L}},{{0UL},{0xD30E9876L},{4294967295UL},{6UL},{4294967295UL},{1UL}},{{0UL},{0x6D48F99CL},{6UL},{0x6D48F99CL},{0UL},{0x7B5E051AL}}},{{{0x4B5F3555L},{0x7B5E051AL},{4294967295UL},{0UL},{1UL},{0xB47FFCAFL}},{{0x87C8608FL},{0x2A9956A4L},{1UL},{0x7B5E051AL},{0xB47FFCAFL},{0xB47FFCAFL}},{{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL},{0x319FEF01L},{0x7B5E051AL}},{{0xB47FFCAFL},{0x4C534E02L},{6UL},{0x29AA79E0L},{0x6D48F99CL},{1UL}},{{6UL},{0x87C8608FL},{4294967295UL},{0x2A9956A4L},{0x6D48F99CL},{0x2A9956A4L}}},{{{0xD30E9876L},{0x4C534E02L},{0xD30E9876L},{0UL},{0x319FEF01L},{0x87C8608FL}},{{0x7B5E051AL},{4294967295UL},{0UL},{1UL},{0xB47FFCAFL},{0x319FEF01L}},{{0x29AA79E0L},{0x2A9956A4L},{0UL},{1UL},{1UL},{0UL}},{{0x7B5E051AL},{0x7B5E051AL},{0x4C534E02L},{0UL},{0UL},{0xD30E9876L}},{{0xD30E9876L},{0x6D48F99CL},{0x7B5E051AL},{0x2A9956A4L},{4294967295UL},{0x4C534E02L}}},{{{6UL},{0xD30E9876L},{0x7B5E051AL},{0x29AA79E0L},{0x7B5E051AL},{0xD30E9876L}},{{0xB47FFCAFL},{0x29AA79E0L},{0x4C534E02L},{4294967295UL},{0x4B5F3555L},{0UL}},{{4294967295UL},{0x4B5F3555L},{0UL},{0x7B5E051AL},{4294967295UL},{0x319FEF01L}},{{0x87C8608FL},{0x4B5F3555L},{0UL},{0UL},{0x4B5F3555L},{0x87C8608FL}},{{0x4B5F3555L},{0x29AA79E0L},{0xD30E9876L},{0x6D48F99CL},{0x7B5E051AL},{0x2A9956A4L}}},{{{0UL},{0xD30E9876L},{4294967295UL},{6UL},{4294967295UL},{1UL}},{{0UL},{0x6D48F99CL},{6UL},{0x6D48F99CL},{0UL},{0x7B5E051AL}},{{0x4B5F3555L},{0x7B5E051AL},{4294967295UL},{0UL},{1UL},{0xB47FFCAFL}},{{0x87C8608FL},{0x2A9956A4L},{1UL},{0x7B5E051AL},{0UL},{0UL}},{{0xB47FFCAFL},{4294967295UL},{4294967295UL},{0xB47FFCAFL},{0x4C534E02L},{6UL}}}};
static int64_t g_191 = 2L;
static int32_t *g_229 = &g_102;
static int32_t ** volatile g_228 = &g_229;/* VOLATILE GLOBAL g_228 */
static union U1 g_238 = {4294967295UL};/* VOLATILE GLOBAL g_238 */
static int64_t g_242 = 1L;
static const uint64_t *g_259 = &g_73;
static const uint64_t **g_258 = &g_259;
static const uint64_t *** volatile g_257 = &g_258;/* VOLATILE GLOBAL g_257 */
static struct S0 g_270 = {4159,18,0,62,-10,0xF760DCFBL};/* VOLATILE GLOBAL g_270 */
static const uint32_t g_273[8][6][5] = {{{0x02E75757L,7UL,0x48A5FBA9L,2UL,7UL},{4294967295UL,0x0064E38AL,0UL,0x53AB2A37L,0x0064E38AL},{4294967293UL,1UL,0x48A5FBA9L,0x48A5FBA9L,1UL},{4294967295UL,0xEDD30E99L,0UL,0x53AB2A37L,0xEDD30E99L},{0x02E75757L,1UL,4294967288UL,2UL,1UL},{0x6BF04042L,0x0064E38AL,0UL,0UL,0x0064E38AL}},{{0x02E75757L,7UL,0x48A5FBA9L,2UL,7UL},{4294967295UL,0x0064E38AL,0UL,0x53AB2A37L,0x0064E38AL},{4294967293UL,1UL,0x48A5FBA9L,0x48A5FBA9L,1UL},{4294967295UL,0xEDD30E99L,0UL,0x53AB2A37L,0xEDD30E99L},{0x02E75757L,1UL,4294967288UL,2UL,1UL},{0x6BF04042L,0x0064E38AL,0UL,0UL,0x0064E38AL}},{{0x02E75757L,7UL,0x48A5FBA9L,2UL,7UL},{4294967295UL,0x0064E38AL,0UL,0x53AB2A37L,0x0064E38AL},{4294967293UL,1UL,0x48A5FBA9L,0x48A5FBA9L,1UL},{4294967295UL,0xEDD30E99L,0UL,0x53AB2A37L,0xEDD30E99L},{0x02E75757L,1UL,4294967288UL,2UL,1UL},{0x6BF04042L,0x0064E38AL,0UL,0UL,0x0064E38AL}},{{0x02E75757L,7UL,0x48A5FBA9L,2UL,7UL},{4294967295UL,0x0064E38AL,0UL,0x53AB2A37L,0x0064E38AL},{4294967293UL,1UL,0x48A5FBA9L,0x48A5FBA9L,1UL},{4294967295UL,0xEDD30E99L,0UL,0x53AB2A37L,0xEDD30E99L},{0x02E75757L,1UL,4294967288UL,2UL,1UL},{0x6BF04042L,0x0064E38AL,0UL,0UL,0x0064E38AL}},{{0x02E75757L,7UL,0x48A5FBA9L,2UL,7UL},{4294967295UL,0x0064E38AL,0UL,0x53AB2A37L,0x0064E38AL},{4294967293UL,1UL,0x48A5FBA9L,0x48A5FBA9L,1UL},{4294967295UL,0xEDD30E99L,0UL,0x53AB2A37L,0xEDD30E99L},{0x02E75757L,1UL,4294967288UL,2UL,1UL},{0x6BF04042L,0x0064E38AL,0UL,0UL,0x0064E38AL}},{{0x02E75757L,7UL,0x48A5FBA9L,2UL,7UL},{4294967295UL,0x0064E38AL,0UL,0x53AB2A37L,0x0064E38AL},{4294967293UL,1UL,0x48A5FBA9L,0x48A5FBA9L,1UL},{4294967295UL,0xEDD30E99L,0UL,0x53AB2A37L,0xEDD30E99L},{0x02E75757L,1UL,4294967288UL,2UL,1UL},{0x6BF04042L,0x0064E38AL,0UL,0UL,0x0064E38AL}},{{0x02E75757L,7UL,0x6C04805BL,7UL,0x7DBA10B8L},{0UL,0x1BC4C52AL,0x0064E38AL,0xEDD30E99L,0x1BC4C52AL},{0x9BE3B201L,8UL,0x6C04805BL,0x6C04805BL,8UL},{0UL,0xC61CD711L,0xB2AF3A1AL,0xEDD30E99L,0xC61CD711L},{0x2B21C91EL,8UL,1UL,7UL,8UL},{0x829236C7L,0x1BC4C52AL,0xB2AF3A1AL,0xB2AF3A1AL,0x1BC4C52AL}},{{0x2B21C91EL,0x7DBA10B8L,0x6C04805BL,7UL,0x7DBA10B8L},{0UL,0x1BC4C52AL,0x0064E38AL,0xEDD30E99L,0x1BC4C52AL},{0x9BE3B201L,8UL,0x6C04805BL,0x6C04805BL,8UL},{0UL,0xC61CD711L,0xB2AF3A1AL,0xEDD30E99L,0xC61CD711L},{0x2B21C91EL,8UL,1UL,7UL,8UL},{0x829236C7L,0x1BC4C52AL,0xB2AF3A1AL,0xB2AF3A1AL,0x1BC4C52AL}}};
static struct S0 g_276 = {4137,21,1,151,14,1L};/* VOLATILE GLOBAL g_276 */
static struct S0 g_312[6] = {{-3420,11,0,0,-18,0L},{-3420,11,0,0,-18,0L},{-3420,11,0,0,-18,0L},{-3420,11,0,0,-18,0L},{-3420,11,0,0,-18,0L},{-3420,11,0,0,-18,0L}};
static uint32_t *g_339[2][7][7] = {{{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97}},{{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97}}};
static uint32_t **g_338 = &g_339[0][5][1];
static uint32_t ***g_337 = &g_338;
static uint32_t **** volatile g_336 = &g_337;/* VOLATILE GLOBAL g_336 */
static struct S0 g_349[4] = {{-1179,23,0,172,31,5L},{-1179,23,0,172,31,5L},{-1179,23,0,172,31,5L},{-1179,23,0,172,31,5L}};
static struct S0 g_367 = {-2894,3,3,41,24,1L};/* VOLATILE GLOBAL g_367 */
static struct S0 g_368 = {-162,25,3,64,-14,0L};/* VOLATILE GLOBAL g_368 */
static const union U1 g_438 = {0x838C53B6L};/* VOLATILE GLOBAL g_438 */
static volatile struct S0 g_455 = {-4549,6,1,131,-19,-1L};/* VOLATILE GLOBAL g_455 */
static volatile struct S0 * volatile g_456[2] = {&g_455,&g_455};
static volatile struct S0 * volatile g_457 = &g_455;/* VOLATILE GLOBAL g_457 */
static float g_459 = 0x1.6p-1;
static int32_t * volatile g_497 = &g_38;/* VOLATILE GLOBAL g_497 */
static union U1 g_523 = {4294967289UL};/* VOLATILE GLOBAL g_523 */
static float *g_531 = (void*)0;
static float **g_530 = &g_531;
static volatile struct S0 g_533 = {5124,15,2,13,-13,1L};/* VOLATILE GLOBAL g_533 */
static int32_t ** volatile g_546[9] = {&g_229,&g_229,&g_229,&g_229,&g_229,&g_229,&g_229,&g_229,&g_229};
static int32_t ** const  volatile g_547 = &g_229;/* VOLATILE GLOBAL g_547 */
static volatile struct S0 g_560[7][2] = {{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}},{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}},{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}},{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}},{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}},{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}},{{1266,22,0,106,13,1L},{1266,22,0,106,13,1L}}};
static uint32_t g_571 = 4294967291UL;
static int8_t *g_630 = &g_13;
static uint16_t *g_675 = &g_23;
static uint16_t **g_674[3][4][4] = {{{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675}},{{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675}},{{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675},{&g_675,&g_675,&g_675,&g_675}}};
static uint16_t *** volatile g_673 = &g_674[0][0][0];/* VOLATILE GLOBAL g_673 */
static const int32_t *g_678 = &g_38;
static struct S0 **** const  volatile g_682[2][1][1] = {{{(void*)0}},{{(void*)0}}};
static struct S0 *g_686 = &g_65;
static struct S0 **g_685 = &g_686;
static struct S0 ***g_684 = &g_685;
static struct S0 **** volatile g_683 = &g_684;/* VOLATILE GLOBAL g_683 */
static struct S0 g_697[6] = {{4551,10,3,28,31,0xD1D156A4L},{4551,10,3,28,31,0xD1D156A4L},{4551,10,3,28,31,0xD1D156A4L},{4551,10,3,28,31,0xD1D156A4L},{4551,10,3,28,31,0xD1D156A4L},{4551,10,3,28,31,0xD1D156A4L}};
static int32_t ** volatile g_710 = &g_229;/* VOLATILE GLOBAL g_710 */
static struct S0 ****g_713 = &g_684;
static struct S0 ***** volatile g_712[6][4][2] = {{{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713}},{{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713}},{{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713}},{{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713}},{{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713}},{{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713},{&g_713,&g_713}}};
static int16_t g_728 = 0xE81EL;
static volatile union U1 *g_743 = &g_185[7][1][5];
static volatile union U1 ** volatile g_742[4] = {&g_743,&g_743,&g_743,&g_743};
static volatile union U1 ** volatile g_744 = &g_743;/* VOLATILE GLOBAL g_744 */
static int32_t ** volatile g_745 = &g_229;/* VOLATILE GLOBAL g_745 */
static volatile int8_t g_762[5][1][2] = {{{(-1L),(-1L)}},{{(-1L),(-1L)}},{{(-1L),(-1L)}},{{(-1L),(-1L)}},{{(-1L),(-1L)}}};
static volatile int8_t * volatile g_761 = &g_762[3][0][0];/* VOLATILE GLOBAL g_761 */
static volatile int8_t * volatile *g_760 = &g_761;
static volatile int8_t * volatile ** volatile g_759[3] = {&g_760,&g_760,&g_760};
static uint64_t g_787[9] = {0x316EE01B5681F045LL,0x316EE01B5681F045LL,0xD1043B3C8F35EE99LL,0x316EE01B5681F045LL,0x316EE01B5681F045LL,0xD1043B3C8F35EE99LL,0x316EE01B5681F045LL,0x316EE01B5681F045LL,0xD1043B3C8F35EE99LL};
static volatile struct S0 g_848[8][1][1] = {{{{2975,25,2,103,18,0L}}},{{{-2266,1,3,74,-19,0x77A31D38L}}},{{{2975,25,2,103,18,0L}}},{{{-2266,1,3,74,-19,0x77A31D38L}}},{{{2975,25,2,103,18,0L}}},{{{-2266,1,3,74,-19,0x77A31D38L}}},{{{2975,25,2,103,18,0L}}},{{{-2266,1,3,74,-19,0x77A31D38L}}}};
static const volatile union U1 g_854[3] = {{0x3885C919L},{0x3885C919L},{0x3885C919L}};
static union U1 g_858 = {0xF891B487L};/* VOLATILE GLOBAL g_858 */
static volatile union U1 g_869 = {0x02896D69L};/* VOLATILE GLOBAL g_869 */
static int8_t g_887[1][10] = {{0xD0L,0xD0L,6L,0x75L,6L,0xD0L,0xD0L,6L,0x75L,6L}};
static volatile struct S0 g_898 = {-3289,11,0,148,30,5L};/* VOLATILE GLOBAL g_898 */
static const uint32_t *g_962 = &g_273[5][2][1];
static const uint32_t **g_961[7] = {&g_962,&g_962,&g_962,&g_962,&g_962,&g_962,&g_962};
static const float ** volatile * volatile *g_986 = (void*)0;
static const float ** volatile * volatile **g_985[5] = {&g_986,&g_986,&g_986,&g_986,&g_986};
static volatile struct S0 g_991 = {4688,20,3,141,11,0xA51DCB27L};/* VOLATILE GLOBAL g_991 */
static struct S0 g_1018 = {-4109,27,1,76,-29,0xC91E5315L};/* VOLATILE GLOBAL g_1018 */
static struct S0 g_1051 = {427,0,2,107,-6,1L};/* VOLATILE GLOBAL g_1051 */
static union U1 g_1085[1][4] = {{{0xE207FC64L},{0xE207FC64L},{0xE207FC64L},{0xE207FC64L}}};
static uint16_t g_1110[5] = {0x4695L,0x4695L,0x4695L,0x4695L,0x4695L};
static int32_t ** const  volatile g_1132 = &g_229;/* VOLATILE GLOBAL g_1132 */
static const int32_t *g_1135 = &g_102;
static const int32_t ** volatile g_1134 = &g_1135;/* VOLATILE GLOBAL g_1134 */
static int32_t ** volatile g_1136 = &g_229;/* VOLATILE GLOBAL g_1136 */
static int8_t *g_1140[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static float g_1148 = (-0x7.Bp+1);
static struct S0 g_1151 = {1043,3,1,23,16,1L};/* VOLATILE GLOBAL g_1151 */
static struct S0 g_1166 = {4446,16,3,83,-1,0xAFCFF58AL};/* VOLATILE GLOBAL g_1166 */
static struct S0 g_1200 = {-1708,1,1,51,-10,-1L};/* VOLATILE GLOBAL g_1200 */
static volatile struct S0 g_1224 = {4087,1,3,166,4,0x97B7EF78L};/* VOLATILE GLOBAL g_1224 */
static uint8_t g_1235 = 0xF3L;
static int8_t ****g_1256 = (void*)0;
static int8_t ***** volatile g_1255 = &g_1256;/* VOLATILE GLOBAL g_1255 */
static int32_t ** volatile g_1257 = (void*)0;/* VOLATILE GLOBAL g_1257 */
static int32_t ** volatile g_1258 = (void*)0;/* VOLATILE GLOBAL g_1258 */
static struct S0 ** volatile g_1304 = &g_686;/* VOLATILE GLOBAL g_1304 */
static const int8_t *g_1314 = &g_13;
static const int8_t **g_1313 = &g_1314;
static struct S0 g_1317 = {1246,18,0,134,27,0xF69F6E2AL};/* VOLATILE GLOBAL g_1317 */
static struct S0 g_1318 = {3257,18,3,98,-23,0x41AE1E87L};/* VOLATILE GLOBAL g_1318 */
static struct S0 g_1319 = {-1268,30,1,68,9,0xCE965906L};/* VOLATILE GLOBAL g_1319 */
static struct S0 g_1320 = {-2589,28,3,106,24,0xAA487A79L};/* VOLATILE GLOBAL g_1320 */
static struct S0 g_1322 = {5361,26,0,78,-31,-8L};/* VOLATILE GLOBAL g_1322 */
static struct S0 g_1340 = {3420,3,0,139,28,0L};/* VOLATILE GLOBAL g_1340 */
static volatile struct S0 * volatile *g_1343[10][1] = {{&g_456[0]},{&g_457},{&g_456[0]},{&g_457},{&g_456[0]},{&g_457},{&g_456[0]},{&g_457},{&g_456[0]},{&g_457}};
static volatile struct S0 * volatile ** volatile g_1342 = &g_1343[5][0];/* VOLATILE GLOBAL g_1342 */
static volatile struct S0 * volatile ** volatile * volatile g_1341 = &g_1342;/* VOLATILE GLOBAL g_1341 */
static union U1 g_1368 = {0x7737D2D4L};/* VOLATILE GLOBAL g_1368 */
static volatile union U1 g_1376 = {9UL};/* VOLATILE GLOBAL g_1376 */
static uint32_t *g_1407[8][1][1] = {{{&g_78}},{{&g_78}},{{&g_78}},{{&g_78}},{{&g_78}},{{&g_78}},{{&g_78}},{{&g_78}}};
static uint32_t **g_1406 = &g_1407[5][0][0];
static uint32_t **g_1408 = (void*)0;
static volatile int64_t g_1427 = 0x9A12151B8413781BLL;/* VOLATILE GLOBAL g_1427 */
static volatile struct S0 g_1433 = {120,11,1,163,-17,0x9035CB52L};/* VOLATILE GLOBAL g_1433 */
static volatile struct S0 g_1434 = {2094,5,1,106,-5,0x70CCD029L};/* VOLATILE GLOBAL g_1434 */
static struct S0 g_1435[2] = {{2927,3,1,19,-16,0x3E35B95EL},{2927,3,1,19,-16,0x3E35B95EL}};


/* --- FORWARD DECLARATIONS --- */
static struct S0  func_1(void);
static int32_t  func_2(uint32_t  p_3);
static float  func_6(int8_t  p_7, int8_t * p_8, uint64_t  p_9, int8_t * p_10);
static int8_t * func_14(uint16_t  p_15, float  p_16);
static uint16_t  func_17(int8_t * p_18, uint16_t  p_19, const int8_t * p_20, int8_t * p_21);
static int8_t * func_26(uint16_t * p_27, int32_t  p_28, int8_t * p_29);
static uint16_t * func_30(int64_t  p_31, int64_t  p_32, int32_t  p_33, int8_t * p_34, float  p_35);
static uint16_t * func_50(int64_t  p_51, int32_t * p_52, int8_t * p_53, int64_t  p_54);
static int64_t  func_66(int64_t  p_67, int8_t * p_68, int32_t * p_69, int8_t  p_70, int16_t  p_71);
static int8_t  func_84(int32_t  p_85, int8_t  p_86, uint64_t  p_87, int32_t  p_88);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_23 g_13 g_37 g_38 g_228 g_229 g_547 g_270.f2 g_560 g_102 g_571 g_185 g_270.f5 g_270.f4 g_258 g_259 g_73 g_368.f4 g_171 g_276.f5 g_99 g_713 g_684 g_338 g_339 g_97 g_523.f0 g_759 g_710 g_455.f4 g_675 g_630 g_787 g_762 g_367.f0 g_745 g_697.f0 g_848 g_854 g_858 g_869 g_530 g_887 g_337 g_898 g_685 g_160 g_65.f4 g_276.f4 g_1051 g_686 g_1085 g_238.f4 g_1110 g_65.f3 g_312.f0 g_1132 g_1134 g_1136 g_1140 g_761 g_858.f4 g_1018.f0 g_1151 g_457 g_455 g_65.f2 g_336 g_760 g_1200 g_1224 g_43 g_1255 g_728 g_185.f4 g_242 g_697.f2 g_1304 g_1313 g_1317 g_1319 g_1322 g_673 g_674 g_1340 g_1341 g_533.f0 g_697.f3 g_1368 g_65.f0 g_1376 g_1320.f1 g_1235 g_1342 g_1343 g_456 g_368.f3 g_65.f1 g_1314 g_1433 g_1435
 * writes: g_23 g_13 g_38 g_229 g_102 g_270.f5 g_571 g_238.f4 g_191 g_99 g_685 g_97 g_523.f0 g_787 g_78 g_367.f0 g_65.f5 g_73 g_160 g_531 g_270 g_65 g_1110 g_858.f4 g_678 g_1135 g_1018.f0 g_728 g_887 g_1256 g_560 g_1235 g_686 g_1313 g_459 g_1318 g_1320 g_523.f4 g_1319.f5 g_1406 g_1408 g_339 g_1434
 */
static struct S0  func_1(void)
{ /* block id: 0 */
    uint64_t l_11[2][5] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0x54292978D8EF068CLL,18446744073709551614UL,0x54292978D8EF068CLL,18446744073709551614UL,0x54292978D8EF068CLL}};
    int8_t *l_12 = &g_13;
    uint16_t *l_22 = &g_23;
    float l_1189 = 0x8.A069B9p-77;
    int32_t l_1425 = 0L;
    int32_t l_1426 = (-8L);
    int32_t l_1428[8] = {0x298D115DL,0x298D115DL,0x298D115DL,0x298D115DL,0x298D115DL,0x298D115DL,0x298D115DL,0x298D115DL};
    uint32_t l_1429 = 0xEBBE282BL;
    int32_t *l_1432[8] = {&l_1428[1],&l_1428[1],&l_1428[1],&l_1428[1],&l_1428[1],&l_1428[1],&l_1428[1],&l_1428[1]};
    int i, j;
    if (func_2(((((((g_4 & (((+func_6((l_11[1][4] && 0UL), l_12, (((((g_4 > ((l_11[1][4] , func_14(((*l_22) = func_17(&g_13, (++(*l_22)), l_12, func_26(func_30(g_13, l_11[0][3], l_11[1][4], l_12, g_4), l_11[1][4], &g_99))), l_11[0][0])) == (void*)0)) , g_848[6][0][0].f0) < 0xC0L) , (*g_760)) != (void*)0), l_12)) , l_12) == l_12)) == l_11[1][4]) != l_11[1][0]) , l_11[1][0]) , (****g_1341)) , 4294967295UL)))
    { /* block id: 764 */
        int32_t *l_1422 = &g_38;
        int32_t *l_1423 = &g_102;
        int32_t *l_1424[7];
        int i;
        for (i = 0; i < 7; i++)
            l_1424[i] = &g_38;
        ++l_1429;
    }
    else
    { /* block id: 766 */
        return (***g_1342);
    }
    (*g_37) |= l_11[0][3];
    g_1434 = g_1433;
    return g_1435[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_1319.f5 g_37 g_38 g_368.f3 g_65.f1 g_1313 g_1314 g_13 g_337 g_338
 * writes: g_1319.f5 g_1406 g_1408 g_339
 */
static int32_t  func_2(uint32_t  p_3)
{ /* block id: 747 */
    int64_t l_1389 = 0x4EC7591068A9E70ALL;
    int32_t l_1390 = 0x034FDBD2L;
    int32_t l_1391 = 1L;
    int32_t l_1392[2];
    int16_t l_1393 = 0L;
    int32_t l_1394[6] = {4L,4L,4L,4L,4L,4L};
    int32_t l_1395 = 0xDAD6CE7CL;
    uint32_t l_1396[4][6] = {{0x2274CBE0L,8UL,0x2274CBE0L,0x2274CBE0L,8UL,0x2274CBE0L},{0x2274CBE0L,8UL,0x2274CBE0L,0x2274CBE0L,8UL,0x2274CBE0L},{0x2274CBE0L,8UL,0x2274CBE0L,0x2274CBE0L,8UL,0x2274CBE0L},{0x2274CBE0L,8UL,0x2274CBE0L,0x2274CBE0L,8UL,0x2274CBE0L}};
    int8_t **l_1414 = (void*)0;
    int8_t ***l_1413 = &l_1414;
    uint32_t l_1416[9][2][4] = {{{0x16A5014EL,1UL,4294967295UL,0x027ECC31L},{4294967295UL,0x65DAEA7BL,1UL,0x40D00772L}},{{0xA10B0EBCL,0x480569D2L,4294967288UL,1UL},{0xF157C48BL,0UL,0UL,0xF157C48BL}},{{8UL,0x4478B7ECL,4294967295UL,0x23DFC475L},{0UL,1UL,4294967293UL,4294967290UL}},{{0x65DAEA7BL,4294967295UL,4294967286UL,4294967290UL},{0xA10B0EBCL,1UL,4294967295UL,0x23DFC475L}},{{0xB714A1F8L,0x4478B7ECL,1UL,0xF157C48BL},{0UL,0UL,4294967290UL,1UL}},{{0x4478B7ECL,0x480569D2L,4294967293UL,0x40D00772L},{8UL,0x65DAEA7BL,0x16A5014EL,0x027ECC31L}},{{0x60CE3A7FL,1UL,0x60CE3A7FL,4294967286UL},{0xF157C48BL,0x23DFC475L,4294967286UL,4294967288UL}},{{0UL,0x16A5014EL,4294967295UL,0x23DFC475L},{0x23DFC475L,0x8E5B9286L,4294967295UL,0x40D00772L}},{{0UL,8UL,4294967286UL,4294967295UL},{0xF157C48BL,0x480569D2L,0x60CE3A7FL,0x4478B7ECL}}};
    uint32_t **l_1420 = &g_1407[5][0][0];
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1392[i] = 0x4EA9680BL;
    for (g_1319.f5 = 0; (g_1319.f5 <= 1); g_1319.f5 += 1)
    { /* block id: 750 */
        int32_t *l_1388[1][3];
        uint32_t *l_1404 = &g_78;
        uint32_t **l_1403 = &l_1404;
        int8_t ***l_1415 = &l_1414;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 3; j++)
                l_1388[i][j] = &g_38;
        }
        --l_1396[3][0];
        for (l_1391 = 0; (l_1391 <= 1); l_1391 += 1)
        { /* block id: 754 */
            uint32_t ***l_1405[6] = {&l_1403,&l_1403,&l_1403,&l_1403,&l_1403,&l_1403};
            int16_t *l_1417[3];
            int32_t l_1418[4] = {0x175E02B8L,0x175E02B8L,0x175E02B8L,0x175E02B8L};
            int32_t l_1419 = 4L;
            int32_t l_1421 = 0x183B6626L;
            int i;
            for (i = 0; i < 3; i++)
                l_1417[i] = &g_1085[0][0].f4;
            l_1421 |= (((**g_337) = ((safe_div_func_int64_t_s_s(((-4L) ^ l_1395), (safe_sub_func_int16_t_s_s((-8L), ((g_1408 = (g_1406 = l_1403)) != (((*g_37) < (safe_sub_func_uint16_t_u_u((((l_1418[3] = ((g_368.f3 , (((safe_add_func_int32_t_s_s((l_1413 == l_1415), ((1L && p_3) | g_65.f1))) , l_1416[6][1][0]) < p_3)) , p_3)) ^ p_3) || (**g_1313)), l_1419))) , l_1420)))))) , &p_3)) == &p_3);
            if (p_3)
                continue;
        }
    }
    return l_1416[6][1][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_571 g_1200 g_1224 g_43 g_259 g_73 g_338 g_339 g_97 g_675 g_160 g_38 g_1255 g_13 g_560 g_728 g_185 g_229 g_185.f4 g_242 g_697.f2 g_65.f4 g_1304 g_1313 g_1317 g_1319 g_1322 g_673 g_674 g_23 g_1340 g_1341 g_630 g_533.f0 g_37 g_697.f3 g_1368 g_1018.f0 g_65.f0 g_1376 g_1320.f1 g_1110 g_1151.f2 g_270.f4 g_1235
 * writes: g_571 g_728 g_160 g_13 g_887 g_23 g_38 g_1256 g_560 g_229 g_1235 g_65.f4 g_686 g_1313 g_459 g_65.f5 g_1318 g_1320 g_858.f4 g_523.f4 g_1110 g_97
 */
static float  func_6(int8_t  p_7, int8_t * p_8, uint64_t  p_9, int8_t * p_10)
{ /* block id: 631 */
    uint64_t l_1193 = 0x947FAC7B6CC6620ALL;
    int16_t *l_1229 = (void*)0;
    int32_t l_1241 = 0L;
    int32_t l_1242 = 1L;
    int32_t l_1243 = 3L;
    int32_t l_1246 = 0xC1005646L;
    int32_t l_1247[8][4][8] = {{{0L,7L,1L,0x48D27FFEL,(-5L),0x37DECF5CL,0xFD11BDFCL,0xFD11BDFCL},{0x97E90910L,0x999E1E65L,0xD761E85AL,0xD761E85AL,0x999E1E65L,0x97E90910L,(-1L),0xFF0E578FL},{(-1L),(-1L),(-5L),(-5L),0x10AF4B22L,0x81A8E3E0L,(-5L),(-4L)},{0xCD3B4162L,0xE6A52034L,7L,(-5L),0x5B963F86L,4L,7L,0xFF0E578FL}},{{0xD4E83DA5L,0x5B963F86L,(-1L),0xD761E85AL,0x7BC5B64FL,0xB1F16823L,0xD4E83DA5L,0xFD11BDFCL},{(-1L),4L,0x10AF4B22L,0x48D27FFEL,(-1L),0xEACB9873L,(-5L),(-1L)},{0x81A8E3E0L,(-1L),(-1L),(-1L),7L,0L,0xB1F16823L,0xD4AD4EECL},{0xB277E105L,0xD4E83DA5L,(-3L),0x821BB0ADL,(-5L),(-3L),0x5DE1A287L,0x1203C4BFL}},{{0x21C9C4AEL,1L,0x50AE72B7L,0xEACB9873L,0xFF6AD1BCL,0x40FA35BEL,(-5L),0x50AE72B7L},{0x10AF4B22L,0x97E90910L,(-1L),0x6C5627C2L,0xEACB9873L,0xFD11BDFCL,0x21C9C4AEL,0x81A8E3E0L},{7L,(-4L),0L,(-3L),0xB1F16823L,(-3L),0L,(-4L)},{0x81A8E3E0L,0x5DE1A287L,9L,0x7E1F7D05L,4L,0L,(-1L),0x57026CC4L}},{{0xC11371A7L,(-1L),(-7L),0x1D3A78ABL,0x81A8E3E0L,(-3L),(-1L),0x1203C4BFL},{0x594FB541L,0x1D3A78ABL,9L,0L,0x97E90910L,0xFF6AD1BCL,0L,(-1L)},{0x97E90910L,0xFF6AD1BCL,0L,(-1L),0x37DECF5CL,0x594FB541L,0x21C9C4AEL,0x77BA1BA2L},{0xD761E85AL,0x81A8E3E0L,(-1L),(-3L),(-1L),(-1L),(-5L),0xEACB9873L}},{{0L,0x3D812B7DL,0x50AE72B7L,9L,0xD4E83DA5L,0L,0x5DE1A287L,(-1L)},{(-3L),9L,(-3L),0x81A8E3E0L,1L,7L,0xB1F16823L,0xB277E105L},{4L,0xEACB9873L,(-1L),0x1D3A78ABL,0x97E90910L,7L,(-4L),0x6C5627C2L},{0x40FA35BEL,0x97E90910L,0x77BA1BA2L,0x50AE72B7L,(-4L),1L,1L,(-4L)}},{{(-3L),0x821BB0ADL,0x821BB0ADL,(-3L),0x5DE1A287L,0xB277E105L,0xEACB9873L,0L},{0x81A8E3E0L,7L,(-5L),0x6C5627C2L,(-1L),0L,0x3D812B7DL,0x7E1F7D05L},{(-7L),7L,(-3L),0x77BA1BA2L,0x1D3A78ABL,0xB277E105L,(-1L),0x1203C4BFL},{0x97E90910L,0x821BB0ADL,0x0411A414L,(-5L),0xFF6AD1BCL,1L,0x1D3A78ABL,0x0411A414L}},{{(-1L),0x97E90910L,(-1L),(-1L),0x81A8E3E0L,7L,0x21C9C4AEL,(-5L)},{1L,0xEACB9873L,1L,(-3L),0x3D812B7DL,7L,0x37DECF5CL,0x1D3A78ABL},{0x81A8E3E0L,9L,0x0411A414L,0xD4AD4EECL,9L,0L,7L,0x2E52FE82L},{0L,0x3D812B7DL,(-7L),(-4L),0xEACB9873L,(-1L),0x3D812B7DL,0x1203C4BFL}},{{7L,0x81A8E3E0L,0x2E52FE82L,1L,0x97E90910L,0x594FB541L,0x77BA1BA2L,(-1L)},{0x21C9C4AEL,0xFF6AD1BCL,0x821BB0ADL,0xD4AD4EECL,0x821BB0ADL,0xFF6AD1BCL,1L,0xFF0E578FL},{0x50AE72B7L,7L,0xD761E85AL,0x57026CC4L,0x40FA35BEL,0xD4AD4EECL,0xB277E105L,(-3L)},{(-1L),7L,0x8C2368CBL,0xF3141393L,0x40FA35BEL,(-1L),0xE6A52034L,9L}}};
    uint8_t l_1286 = 255UL;
    int8_t *****l_1349 = &g_1256;
    uint16_t *l_1383 = &g_1110[2];
    int16_t *l_1384[6][1][2] = {{{&g_728,&g_1368.f4}},{{&g_728,&g_728}},{{&g_1368.f4,&g_728}},{{&g_728,&g_1368.f4}},{{&g_728,&g_728}},{{&g_1368.f4,&g_728}}};
    uint8_t *l_1387 = &g_1235;
    int i, j, k;
    for (g_571 = 0; (g_571 >= 21); g_571 = safe_add_func_uint16_t_u_u(g_571, 5))
    { /* block id: 634 */
        int16_t l_1238 = (-1L);
        int32_t l_1244[1][5];
        struct S0 *l_1303[10][9][2] = {{{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]},{&g_349[3],&g_1051},{&g_65,&g_270},{&g_1051,&g_1200},{&g_65,&g_1166},{&g_349[3],&g_349[3]},{&g_349[3],&g_1166}},{{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]},{&g_349[3],&g_1051},{&g_65,&g_270},{&g_1051,&g_1200},{&g_65,&g_1166},{&g_349[3],&g_349[3]}},{{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]},{&g_349[3],&g_1051},{&g_65,&g_270},{&g_1051,&g_1200},{&g_65,&g_1166}},{{&g_349[3],&g_349[3]},{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]},{&g_349[3],&g_1051},{&g_65,&g_270},{&g_1051,&g_1200}},{{&g_65,&g_1166},{&g_349[3],&g_349[3]},{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]},{&g_349[3],&g_1051},{&g_65,&g_270}},{{&g_1051,&g_1200},{&g_65,&g_1166},{&g_349[3],&g_349[3]},{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]},{&g_349[3],&g_1051}},{{&g_65,&g_270},{&g_1051,&g_1200},{&g_65,&g_1166},{&g_349[3],&g_349[3]},{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051},{&g_349[3],&g_349[0]}},{{&g_349[3],&g_1051},{&g_65,&g_270},{&g_1051,&g_1200},{&g_65,&g_1166},{&g_349[3],&g_349[3]},{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_270},{&g_65,&g_1051}},{{&g_349[3],&g_349[0]},{&g_349[3],&g_1051},{&g_65,&g_270},{&g_1051,&g_1200},{&g_65,&g_1166},{&g_349[3],&g_349[3]},{&g_349[3],&g_1166},{&g_65,&g_1200},{&g_1051,&g_349[0]}},{{&g_1051,&g_65},{&g_1051,&g_1166},{&g_1051,&g_65},{&g_1051,&g_349[0]},{&g_65,&g_349[3]},{&g_1051,&g_65},{&g_1051,&g_1051},{&g_1051,&g_65},{&g_1051,&g_349[3]}}};
        int16_t l_1337 = 1L;
        uint8_t *l_1346 = &g_160;
        int32_t *l_1350 = &l_1246;
        int32_t *l_1353 = &l_1247[0][1][1];
        int16_t l_1369[1];
        int32_t *l_1375 = &g_38;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 5; j++)
                l_1244[i][j] = 0xE13EC2E6L;
        }
        for (i = 0; i < 1; i++)
            l_1369[i] = 0xED82L;
        for (g_728 = 4; (g_728 >= 0); g_728 -= 1)
        { /* block id: 637 */
            int16_t l_1197 = 0xAFAFL;
            int32_t l_1236[7] = {(-1L),0x562AFBAEL,0x562AFBAEL,(-1L),0x562AFBAEL,0x562AFBAEL,(-1L)};
            int32_t l_1274[1][7][7] = {{{(-8L),0xEE5000FDL,(-8L),0xFABEC4D9L,0x270ABC18L,0xFABEC4D9L,(-8L)},{(-9L),(-9L),9L,0x22A17002L,(-9L),0x3F426EDDL,0x22A17002L},{1L,0xFABEC4D9L,(-1L),0xEE5000FDL,(-1L),0xFABEC4D9L,1L},{0xB86B4323L,0x22A17002L,0x64B36974L,0xB86B4323L,(-9L),0x64B36974L,0x64B36974L},{0x270ABC18L,0xEE5000FDL,1L,0xEE5000FDL,0x270ABC18L,(-1L),0x270ABC18L},{(-9L),0xB86B4323L,0x64B36974L,0x22A17002L,0xB86B4323L,0xB86B4323L,0x22A17002L},{(-1L),0xEE5000FDL,(-1L),0xFABEC4D9L,1L,0xFABEC4D9L,(-1L)}}};
            int16_t l_1312[6][10][4] = {{{0L,0x1AA7L,0xBD78L,9L},{4L,0x0FA0L,0x398FL,0x58C4L},{9L,0xDB27L,0L,0x0FA0L},{0xEABCL,0x847FL,(-10L),0x56E4L},{0x56E4L,6L,0x398FL,1L},{4L,1L,0x029CL,1L},{0L,0x052BL,0x08D8L,5L},{0x516AL,0x6169L,3L,0x9F4CL},{0xF765L,0xE5C9L,(-7L),0x7688L},{0x1AA7L,9L,0xD52EL,0x6701L}},{{1L,0x487CL,(-1L),0x3131L},{(-7L),1L,(-1L),(-6L)},{1L,9L,0xB1A4L,1L},{1L,0xF765L,1L,0x9F4CL},{(-1L),0xEABCL,1L,6L},{0L,0x052BL,0x2EFAL,0x052BL},{0x3131L,0x7688L,6L,1L},{0xDF05L,5L,0xB742L,9L},{0x052BL,0x847FL,0xCE0CL,0x516AL},{0x052BL,0xC410L,0xB742L,0x58C4L}},{{0xDF05L,0x516AL,6L,0x972BL},{0x3131L,0x1AA7L,0x2EFAL,0x87DFL},{0L,0xB742L,1L,(-1L)},{(-1L),0L,1L,(-1L)},{1L,0x6701L,0xB1A4L,2L},{1L,0L,(-1L),(-1L)},{(-7L),0x87DFL,(-1L),(-8L)},{1L,1L,0xD52EL,2L},{0x1AA7L,(-6L),(-7L),0x6413L},{0xF668L,0L,0x08D8L,0xCE0CL}},{{1L,0x487CL,1L,0L},{9L,2L,(-10L),(-1L)},{0x1F79L,1L,2L,0x1783L},{(-3L),0x972BL,(-6L),1L},{3L,(-7L),0xCE0CL,(-3L)},{9L,(-8L),2L,6L},{0xB742L,0xDF05L,0x56E4L,0x6169L},{9L,0xB1A4L,4L,(-8L)},{0x443EL,3L,0x08D8L,0x08D8L},{0x968CL,0x968CL,0xE5C9L,0xDF05L}},{{(-1L),0L,0xCA68L,1L},{0xABE9L,1L,0x7D84L,0xCA68L},{0xF765L,1L,0x1AA7L,1L},{1L,0L,8L,0xDF05L},{(-1L),0x968CL,(-9L),0x08D8L},{1L,3L,(-1L),(-8L)},{0L,0xB1A4L,0x1F79L,0x6169L},{0xCA68L,0xDF05L,2L,6L},{(-7L),(-8L),1L,(-3L)},{0x6169L,(-7L),(-9L),1L}},{{0xB1A4L,0x972BL,0x2D36L,0x1783L},{(-7L),1L,(-1L),(-1L)},{0xEC84L,2L,0x1F79L,0L},{(-9L),0x487CL,(-10L),0xCE0CL},{1L,0L,1L,1L},{6L,4L,8L,0x0FA0L},{6L,4L,(-7L),0xD52EL},{0xF765L,0L,0L,0L},{0xDF05L,0x2EFAL,0xCA68L,0x0FA0L},{2L,1L,0x1783L,0x7D84L}}};
            int i, j, k;
            for (g_160 = 0; (g_160 <= 1); g_160 += 1)
            { /* block id: 640 */
                uint32_t l_1192 = 0UL;
                return l_1192;
            }
            for (g_13 = 4; (g_13 >= 0); g_13 -= 1)
            { /* block id: 645 */
                int32_t l_1201 = 0x12780292L;
                int16_t l_1211 = (-4L);
                uint16_t l_1230 = 0x217CL;
                int32_t l_1232 = (-1L);
                int64_t *l_1262[9] = {&g_191,&g_191,&g_242,&g_191,&g_191,&g_242,&g_191,&g_191,&g_242};
                int32_t l_1283[6][9] = {{1L,1L,1L,1L,1L,1L,1L,1L,1L},{8L,8L,0x3F99413BL,0x3F99413BL,8L,8L,0x3F99413BL,0x3F99413BL,8L},{(-10L),1L,(-10L),1L,(-10L),1L,(-10L),1L,(-10L)},{8L,0x3F99413BL,0x3F99413BL,8L,8L,0x3F99413BL,0x3F99413BL,8L,8L},{1L,1L,1L,1L,1L,1L,1L,1L,1L},{8L,8L,0x3F99413BL,0x3F99413BL,8L,8L,0x3F99413BL,0x3F99413BL,8L}};
                uint64_t l_1305 = 5UL;
                uint64_t l_1327 = 18446744073709551615UL;
                int i, j;
                if (l_1193)
                { /* block id: 646 */
                    float l_1196 = 0xD.35AE21p+56;
                    float ***l_1223[3][7] = {{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}};
                    int32_t l_1245 = 0x183A2F49L;
                    uint64_t l_1248 = 18446744073709551613UL;
                    int32_t **l_1259 = &g_229;
                    int32_t l_1273 = (-9L);
                    uint8_t *l_1275 = &g_1235;
                    int32_t l_1281 = 2L;
                    int32_t l_1282 = 0x38561293L;
                    int32_t l_1284 = (-8L);
                    int32_t l_1285 = 0x9D90A079L;
                    int i, j, k;
                    if ((safe_mul_func_int8_t_s_s(0x48L, l_1197)))
                    { /* block id: 647 */
                        uint64_t *l_1202[9];
                        float ***l_1225 = &g_530;
                        int32_t l_1226 = 0xFCE2F5D8L;
                        int8_t *l_1227 = &g_887[0][7];
                        int16_t *l_1228[3];
                        uint8_t *l_1231 = &g_160;
                        int8_t *l_1233 = (void*)0;
                        uint8_t *l_1234[1][5][7] = {{{(void*)0,&g_1235,(void*)0,&g_1235,&g_1235,&g_1235,&g_1235},{&g_4,&g_1235,&g_1235,&g_4,&g_4,&g_4,(void*)0},{&g_1235,&g_4,&g_1235,&g_4,(void*)0,&g_1235,&g_1235},{&g_4,(void*)0,(void*)0,(void*)0,&g_4,&g_4,(void*)0},{&g_4,&g_4,&g_4,(void*)0,&g_1235,&g_4,&g_1235}}};
                        int32_t *l_1237 = &g_38;
                        int i, j, k;
                        for (i = 0; i < 9; i++)
                            l_1202[i] = (void*)0;
                        for (i = 0; i < 3; i++)
                            l_1228[i] = &g_523.f4;
                        l_1238 |= ((*l_1237) = (safe_lshift_func_uint8_t_u_s((g_1200 , (l_1236[2] &= ((p_7 = (((l_1201 | (p_9--)) & (l_1232 = ((safe_mul_func_int16_t_s_s(((+((~((safe_lshift_func_uint8_t_u_s(((*l_1231) &= (l_1211 , ((safe_add_func_int8_t_s_s((p_7 , ((safe_add_func_int16_t_s_s(((((*g_675) = ((safe_mod_func_uint64_t_u_u((safe_unary_minus_func_int32_t_s(((safe_div_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_s(((((l_1223[0][2] == (g_1224 , l_1225)) && ((((*l_1227) = l_1226) , (l_1228[0] == l_1229)) <= g_43)) == l_1226) < (*g_259)), l_1197)), 0x06AC43C8L)) && (**g_338)))), p_7)) != l_1193)) , 0xC954A45D24E8A887LL) , 0xC5D9L), p_7)) , l_1197)), l_1230)) != 0xD921B4CAL))), 3)) != 18446744073709551609UL)) || (-3L))) , 0xEDC8L), l_1226)) == p_7))) > 1L)) <= l_1197))), l_1226)));
                        (*l_1237) ^= l_1211;
                    }
                    else
                    { /* block id: 658 */
                        int32_t *l_1239 = (void*)0;
                        int32_t *l_1240[1];
                        int8_t **l_1253 = &g_1140[0];
                        int8_t ***l_1252[5] = {&l_1253,&l_1253,&l_1253,&l_1253,&l_1253};
                        int8_t ****l_1251 = &l_1252[4];
                        int8_t *****l_1254 = (void*)0;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1240[i] = (void*)0;
                        ++l_1248;
                        (*g_1255) = l_1251;
                    }
                    for (l_1230 = 0; (l_1230 <= 1); l_1230 += 1)
                    { /* block id: 664 */
                        int i, j;
                        g_560[(g_728 + 1)][l_1230] = g_560[(g_13 + 1)][l_1230];
                    }
                    (*l_1259) = &l_1236[6];
                    if (((safe_mul_func_int16_t_s_s((g_185[(g_13 + 3)][g_13][g_13] , ((((((void*)0 == l_1262[2]) & (((safe_rshift_func_uint16_t_u_u(((*g_675) = 0xE729L), (safe_mul_func_int8_t_s_s((*p_10), ((*l_1275) = (0UL != ((safe_lshift_func_int8_t_s_u((*p_10), ((l_1244[0][0] = (safe_div_func_uint16_t_u_u((((safe_lshift_func_int8_t_s_u((**l_1259), (l_1193 || p_9))) != 0L) | l_1273), l_1274[0][3][0]))) > 255UL))) != (**l_1259)))))))) == g_185[7][1][5].f4) , 0xA7F1CB0FL)) <= l_1197) ^ p_7) && l_1244[0][0])), l_1243)) != p_9))
                    { /* block id: 671 */
                        int32_t *l_1276 = &l_1274[0][3][0];
                        int32_t *l_1277 = &l_1244[0][0];
                        int32_t *l_1278 = &l_1236[4];
                        int32_t *l_1279 = &l_1236[2];
                        int32_t *l_1280[3];
                        uint64_t l_1289 = 0x96A99A4E44A1B2E1LL;
                        int8_t l_1302[1];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1280[i] = (void*)0;
                        for (i = 0; i < 1; i++)
                            l_1302[i] = 0xB2L;
                        ++l_1286;
                        ++l_1289;
                        g_65.f4 &= (p_9 < ((safe_mul_func_int16_t_s_s(g_242, (((**l_1259) < g_1224.f4) < (*l_1279)))) || ((*l_1276) = (((safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((g_697[4].f2 < p_7), ((safe_mod_func_uint16_t_u_u(65535UL, l_1236[0])) > l_1302[0]))), l_1244[0][0])), p_9)) >= l_1244[0][0]) < p_7))));
                        (*g_1304) = l_1303[2][7][1];
                    }
                    else
                    { /* block id: 677 */
                        const int8_t ***l_1315 = (void*)0;
                        const int8_t ***l_1316 = &g_1313;
                        l_1305++;
                        g_459 = (safe_add_func_float_f_f((l_1274[0][3][0] = (safe_sub_func_float_f_f((l_1312[3][0][1] = (0x1.0p+1 != ((void*)0 == &p_7))), (0x8.93EBF1p+57 < ((**l_1259) = (l_1247[0][3][6] = (-0x1.0p-1))))))), (((*l_1316) = g_1313) != (void*)0)));
                        return p_9;
                    }
                }
                else
                { /* block id: 687 */
                    int16_t l_1321 = (-4L);
                    for (g_65.f5 = 3; (g_65.f5 >= 1); g_65.f5 -= 1)
                    { /* block id: 690 */
                        g_1318 = g_1317;
                    }
                    g_1320 = g_1319;
                    for (g_1320.f5 = 0; (g_1320.f5 <= 1); g_1320.f5 += 1)
                    { /* block id: 696 */
                        return l_1321;
                    }
                }
                for (l_1193 = 0; (l_1193 <= 1); l_1193 += 1)
                { /* block id: 702 */
                    uint16_t l_1329[2];
                    int32_t l_1336[7];
                    int32_t l_1338 = 0x5AAD4308L;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1329[i] = 1UL;
                    for (i = 0; i < 7; i++)
                        l_1336[i] = (-1L);
                    for (l_1211 = 4; (l_1211 >= 0); l_1211 -= 1)
                    { /* block id: 705 */
                        int i, j;
                        g_560[(g_13 + 2)][l_1193] = g_1322;
                        return p_7;
                    }
                    if (p_7)
                        break;
                    if (l_1197)
                        continue;
                    for (p_9 = 0; (p_9 <= 4); p_9 += 1)
                    { /* block id: 713 */
                        uint64_t *l_1328 = (void*)0;
                        int16_t *l_1334 = &g_858.f4;
                        int16_t *l_1335[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1335[i] = &l_1197;
                        l_1283[3][3] |= 0x50819BA9L;
                        l_1338 = (safe_div_func_int64_t_s_s(((l_1336[0] = (safe_lshift_func_uint8_t_u_u((&g_743 == &g_743), (((0L | l_1244[0][0]) > (l_1329[0] = (l_1327 || (*p_10)))) < (g_523.f4 = ((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(((*l_1334) = (p_9 , p_9)), (***g_673))), 0x88L)) != 0xC9L)))))) < p_9), l_1337));
                    }
                }
                for (l_1305 = 0; (l_1305 <= 1); l_1305 += 1)
                { /* block id: 724 */
                    return l_1246;
                }
            }
        }
        if (p_9)
            continue;
        (*l_1353) ^= ((~((g_1340 , (((((*g_630) = ((void*)0 == g_1341)) , ((safe_rshift_func_uint8_t_u_u(((*l_1346)--), l_1241)) || (l_1244[0][4] &= ((((*l_1350) = (0x67L < (l_1349 == l_1349))) == (+(!(g_533.f0 , ((l_1193 & (*g_37)) != 0x4021D79D653F631BLL))))) && 0x05A53B38065B375FLL)))) || g_697[4].f3) > p_7)) , p_7)) || p_7);
        (*l_1375) &= (safe_mod_func_uint64_t_u_u((((((0L == (p_9 == ((safe_div_func_int64_t_s_s((safe_sub_func_uint32_t_u_u(p_7, (safe_mod_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s((safe_add_func_int8_t_s_s(((*g_630) = (safe_add_func_int32_t_s_s((g_1368 , l_1369[0]), ((safe_lshift_func_int16_t_s_s(g_1018.f0, (*l_1353))) | (safe_rshift_func_uint16_t_u_u(65529UL, (+l_1241))))))), 0xE1L)), g_65.f0)), p_7)))), 7UL)) , (-1L)))) , (-9L)) && 0xE0L) & (*l_1350)) >= 0x66A6D1C2L), 4L));
    }
    (*g_37) = (l_1247[1][3][7] = (((g_1376 , ((l_1247[5][2][3] ^ ((*l_1387) |= (safe_mul_func_int8_t_s_s(((((safe_mul_func_uint16_t_u_u((safe_div_func_int16_t_s_s(g_1320.f1, ((*l_1383) ^= ((*g_675) = p_7)))), (l_1246 = (0xEBL ^ 0xAFL)))) && ((safe_mul_func_int16_t_s_s(((p_9 == 0x12L) || (((l_1242 = (((**g_338) = (0UL ^ p_7)) >= l_1247[7][1][5])) , 0xD32FL) >= 0xFE96L)), p_9)) , p_7)) < g_1151.f2) <= l_1286), g_270.f4)))) == 4294967287UL)) & 0x78L) != 0UL));
    return l_1241;
}


/* ------------------------------------------ */
/* 
 * reads : g_270.f4 g_258 g_259 g_73 g_368.f4 g_171 g_276.f5 g_229 g_102 g_99 g_228 g_547 g_713 g_684 g_338 g_339 g_97 g_523.f0 g_759 g_710 g_455.f4 g_675 g_23 g_630 g_787 g_762 g_367.f0 g_745 g_697.f0 g_38 g_848 g_854 g_13 g_858 g_869 g_530 g_887 g_337 g_898 g_685 g_571 g_160 g_65.f4 g_276.f4 g_1051 g_686 g_560.f5 g_1085 g_238.f4 g_1110 g_65.f3 g_312.f0 g_1132 g_1134 g_1136 g_1140 g_761 g_858.f4 g_1018.f0 g_1151 g_37 g_457 g_455 g_65.f2 g_336
 * writes: g_238.f4 g_191 g_102 g_99 g_229 g_685 g_97 g_523.f0 g_13 g_787 g_78 g_23 g_367.f0 g_65.f5 g_73 g_160 g_38 g_531 g_270 g_571 g_65 g_1110 g_858.f4 g_678 g_1135 g_1018.f0
 */
static int8_t * func_14(uint16_t  p_15, float  p_16)
{ /* block id: 324 */
    int64_t l_585 = 1L;
    float ***l_596 = &g_530;
    float ****l_595 = &l_596;
    struct S0 *l_602 = (void*)0;
    struct S0 *l_603 = &g_270;
    struct S0 **l_604 = &l_603;
    int16_t *l_605 = &g_238.f4;
    int32_t l_606 = (-1L);
    int64_t *l_607 = &g_191;
    float l_608 = 0xD.C6A83Ap-14;
    uint8_t *l_612 = &g_160;
    float l_621 = (-0x7.5p-1);
    int32_t l_637 = 0x73199F06L;
    int8_t **l_653 = &g_630;
    uint16_t *l_672 = &g_23;
    uint16_t **l_671[1];
    float l_725[2][9][9] = {{{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23}},{{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23},{(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0xE.0D8BE0p-23,0xE.0D8BE0p-23,(-0x9.3p-1),0x9.755797p-64,0x9.755797p-64},{0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64,0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64,0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64},{0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64,0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64,0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64},{0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64,0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64,0xE.0D8BE0p-23,0x9.755797p-64,0x9.755797p-64}}};
    struct S0 **l_753[3];
    int32_t l_794 = 0x5E9B0965L;
    int32_t l_795 = 1L;
    int32_t l_796 = 0L;
    int32_t l_797 = 0x56D795A3L;
    int32_t l_798 = 0xC6EBAACEL;
    int32_t l_799 = 1L;
    int32_t l_800 = 0x46FDAB87L;
    int32_t l_802 = 9L;
    int32_t l_803 = 6L;
    int32_t l_804[5];
    int8_t l_805 = 0L;
    int32_t l_806 = 8L;
    uint32_t *l_813 = &g_78;
    int64_t l_814 = 0x199E411FE9A8DA9BLL;
    int64_t l_837 = 0L;
    struct S0 **l_840 = &l_603;
    uint64_t l_845 = 1UL;
    int32_t *l_950[8][4][5] = {{{&l_794,&l_637,&l_637,&l_794,&g_102},{&l_804[1],&g_38,&l_798,&l_794,&l_799},{&l_806,&l_806,&l_637,&l_799,&l_803},{&l_804[1],&l_637,&l_637,&l_804[1],&g_102}},{{&l_794,&l_796,&l_798,&l_798,&l_803},{&l_806,&l_796,&l_637,&l_799,&l_799},{&l_798,&l_637,&l_802,&l_798,&g_102},{&l_798,&l_806,&l_798,&l_804[1],&l_794}},{{&l_806,&g_38,&l_802,&l_799,&l_794},{&l_794,&l_637,&l_637,&l_794,&g_102},{&l_804[1],&g_38,&l_798,&l_794,&l_799},{&l_806,&l_806,&l_637,&l_799,&l_803}},{{&l_804[1],&l_637,&l_637,&l_804[1],&g_102},{&l_794,&l_796,&l_798,&l_798,&l_803},{&l_806,&l_796,&l_637,&l_799,&l_799},{&l_798,&l_637,&l_802,&l_798,&g_102}},{{&l_798,&l_806,&l_798,&l_804[1],&l_794},{&l_806,&g_38,&l_802,&l_799,&l_794},{&l_794,&l_637,&l_637,&l_794,&g_102},{&l_804[1],&g_38,&l_798,&l_794,&l_799}},{{&l_806,&l_806,&l_637,&l_799,&l_803},{&l_804[1],&l_637,&l_637,&l_804[1],&g_102},{&l_794,&l_796,&l_798,&l_798,&l_803},{&l_806,&l_796,&l_637,&l_799,&l_799}},{{&l_798,&l_637,&l_802,&l_798,&g_102},{&l_798,&l_806,&l_798,&l_804[1],&l_794},{&l_806,&g_38,&l_802,&l_799,&l_794},{&l_794,&l_637,&l_637,&l_794,&g_102}},{{&l_804[1],&g_38,&l_798,&l_794,&l_799},{&l_806,&l_806,&l_637,&l_799,&l_803},{&l_804[1],&l_637,&l_637,&l_804[1],&g_102},{&l_794,&l_796,&l_798,&l_798,&l_803}}};
    uint32_t l_1062 = 0x304C8A64L;
    uint64_t l_1131 = 0x9C9DC62B5F36FD48LL;
    struct S0 ****l_1146[4] = {&g_684,&g_684,&g_684,&g_684};
    float l_1155 = 0x1.2p+1;
    uint32_t l_1156 = 1UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_671[i] = &l_672;
    for (i = 0; i < 3; i++)
        l_753[i] = &l_603;
    for (i = 0; i < 5; i++)
        l_804[i] = 0x37F57A6EL;
    if (((l_585 & ((safe_sub_func_int32_t_s_s(((((safe_div_func_int64_t_s_s(((*l_607) = (safe_div_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((p_15 , ((((!((void*)0 != l_595)) != p_15) < ((+((l_606 = (safe_div_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(((*l_605) = ((l_602 == ((*l_604) = l_603)) != ((p_15 | (-9L)) & p_15))), p_15)), (-5L)))) , g_270.f4)) < l_585)) > 4294967288UL)), p_15)), l_585))), (-1L))) || (**g_258)) != 0x5691F7C7L) != l_585), p_15)) <= l_585)) < l_585))
    { /* block id: 329 */
        uint8_t * const l_613 = &g_160;
        const int32_t l_614[1] = {0xBF075AD3L};
        int64_t l_625 = 0xA44E3127BF031C91LL;
        uint32_t *l_661 = &g_523.f0;
        int32_t l_708[3];
        struct S0 **l_750 = &g_686;
        struct S0 ***l_751 = (void*)0;
        struct S0 ***l_752 = &l_604;
        int8_t ** const *l_763 = &l_653;
        int32_t l_801[2];
        uint16_t l_807 = 3UL;
        int i;
        for (i = 0; i < 3; i++)
            l_708[i] = 1L;
        for (i = 0; i < 2; i++)
            l_801[i] = (-1L);
        (*g_229) |= (1UL ^ (safe_unary_minus_func_int64_t_s(((safe_add_func_uint16_t_u_u(((p_15 <= ((((l_612 != l_613) >= l_614[0]) > (safe_add_func_int16_t_s_s(g_368.f4, g_171[2]))) || ((safe_mul_func_int16_t_s_s(l_614[0], ((safe_add_func_int8_t_s_s((-7L), l_585)) || l_614[0]))) || (-1L)))) ^ g_276.f5), 0x2C56L)) & 0x6E57L))));
        for (g_99 = 19; (g_99 != 14); g_99 = safe_sub_func_uint32_t_u_u(g_99, 5))
        { /* block id: 333 */
            int8_t * const l_624 = (void*)0;
            int8_t *l_629 = &g_99;
            int8_t **l_628[1];
            int32_t l_631 = 0L;
            int32_t l_724 = 0xC4EDC94FL;
            int32_t l_726 = 0x5448D675L;
            int32_t l_727 = (-2L);
            int i;
            for (i = 0; i < 1; i++)
                l_628[i] = &l_629;
        }
        (*g_547) = (*g_228);
        if ((l_637 = ((safe_div_func_int8_t_s_s((0xD5L & (((**g_338) |= ((safe_rshift_func_uint8_t_u_s(((((*l_605) = (((**g_713) = l_750) == (l_753[1] = ((*l_752) = l_750)))) ^ p_15) & l_606), 6)) && ((void*)0 != &l_602))) >= ((*g_229) = (safe_lshift_func_int8_t_s_s(((safe_unary_minus_func_int64_t_s((p_15 < (--(*l_661))))) || (l_637 & l_708[2])), p_15))))), l_606)) , p_15)))
        { /* block id: 398 */
            uint32_t l_768 = 2UL;
            int32_t *l_771[3];
            int16_t l_784 = 0x46D6L;
            float *****l_788 = &l_595;
            int i;
            for (i = 0; i < 3; i++)
                l_771[i] = &l_637;
            if ((g_759[0] != l_763))
            { /* block id: 399 */
                int32_t *l_764 = &g_102;
                int32_t *l_765 = &l_708[2];
                int32_t *l_766 = &l_637;
                int32_t *l_767[8];
                int32_t **l_772[8][7][4] = {{{(void*)0,&l_767[3],&l_767[2],&l_764},{&l_771[0],&l_771[1],(void*)0,&l_766},{&l_771[0],&l_764,&l_771[1],&l_764},{(void*)0,&l_765,&l_764,&l_771[1]},{(void*)0,&l_771[1],&l_767[1],&l_771[0]},{(void*)0,&l_766,(void*)0,&l_771[1]},{&l_765,(void*)0,&l_765,&l_771[2]}},{{(void*)0,&l_771[1],(void*)0,(void*)0},{(void*)0,&l_766,&l_771[1],(void*)0},{(void*)0,(void*)0,(void*)0,&l_766},{&l_764,&l_765,(void*)0,&l_767[2]},{&l_771[0],&l_771[1],&l_767[2],(void*)0},{(void*)0,&g_229,&l_764,&l_767[3]},{&l_767[2],&l_765,&l_765,&l_764}},{{&l_764,&l_766,&l_771[2],(void*)0},{&l_771[0],&l_765,&l_771[1],&l_764},{&l_765,&l_764,&l_771[1],&l_766},{(void*)0,&l_771[1],&l_764,&l_767[3]},{&l_765,&l_766,&l_771[1],&l_764},{&l_765,(void*)0,&l_766,&l_765},{&l_767[2],(void*)0,&l_767[2],(void*)0}},{{(void*)0,&l_766,&l_765,(void*)0},{(void*)0,(void*)0,(void*)0,&l_766},{&l_764,&l_764,(void*)0,&l_764},{(void*)0,&l_771[1],&l_765,&l_771[2]},{(void*)0,&l_771[1],&l_767[2],&l_766},{&l_767[2],&l_766,&l_766,&l_767[3]},{&l_765,&l_771[1],&l_771[1],(void*)0}},{{&l_765,&l_771[1],&l_764,&g_229},{(void*)0,&l_771[0],&l_771[1],&l_766},{&l_765,(void*)0,&l_771[1],&l_771[0]},{&l_771[0],&l_766,&l_771[2],&l_764},{&l_764,(void*)0,&l_765,(void*)0},{&l_767[2],(void*)0,&l_764,&l_771[1]},{(void*)0,&l_766,&l_767[2],(void*)0}},{{&l_771[0],&l_765,(void*)0,&l_764},{&l_764,&l_771[0],(void*)0,&l_764},{(void*)0,&l_764,&l_771[1],(void*)0},{(void*)0,&l_771[1],(void*)0,&l_771[1]},{(void*)0,&l_767[1],&l_765,&l_766},{&l_765,&g_229,(void*)0,&l_766},{(void*)0,&l_765,&l_767[1],&g_229}},{{(void*)0,&l_767[1],&l_764,&l_764},{(void*)0,(void*)0,&l_771[1],(void*)0},{&l_771[0],&l_766,(void*)0,&l_764},{&l_771[0],(void*)0,&l_767[2],(void*)0},{(void*)0,(void*)0,&l_767[2],&l_771[1]},{(void*)0,&l_765,(void*)0,(void*)0},{&l_771[1],&l_771[1],&l_764,(void*)0}},{{&l_771[1],&g_229,&l_767[2],(void*)0},{&l_771[1],&l_771[1],&l_766,&l_764},{(void*)0,&l_767[2],&l_766,(void*)0},{&l_771[1],&l_771[1],(void*)0,(void*)0},{&l_771[1],&l_765,&l_767[3],&l_766},{&l_766,&l_771[1],&l_767[1],&l_771[2]},{&l_767[3],&l_771[1],&l_767[1],(void*)0}}};
                int i, j, k;
                for (i = 0; i < 8; i++)
                    l_767[i] = (void*)0;
                l_768++;
                (*g_228) = l_771[1];
            }
            else
            { /* block id: 402 */
                uint16_t l_786 = 65529UL;
                (*g_229) = (**g_710);
                l_771[1] = (*g_547);
                (*g_229) = ((0x38FDL | (safe_mul_func_int16_t_s_s((-1L), (((safe_add_func_uint8_t_u_u((g_787[6] |= (safe_add_func_int32_t_s_s((safe_add_func_int8_t_s_s((g_455.f4 || (65535UL > p_15)), ((safe_lshift_func_uint16_t_u_u(((&g_191 != &g_191) <= ((safe_unary_minus_func_uint8_t_u(l_784)) != ((((***l_763) = (!((*g_675) && (*g_675)))) , p_15) != l_786))), (*g_675))) | p_15))), 0x8C7612D6L))), (-2L))) , l_788) != l_788)))) == l_708[2]);
            }
        }
        else
        { /* block id: 409 */
            int32_t *l_789 = (void*)0;
            int32_t *l_790[4];
            uint32_t l_791 = 4294967287UL;
            int i;
            for (i = 0; i < 4; i++)
                l_790[i] = &l_708[2];
            l_791--;
            l_807++;
        }
    }
    else
    { /* block id: 413 */
        return &g_13;
    }
    for (l_802 = 0; (l_802 <= 1); l_802 += 1)
    { /* block id: 418 */
        int16_t l_834 = (-7L);
        int32_t l_838 = 0xA62F8C0DL;
        int8_t l_874[4][10];
        int32_t l_879 = 0L;
        int32_t l_880 = 0x40413A51L;
        int32_t l_881[3][6] = {{(-3L),(-10L),0x29042436L,0x29042436L,(-10L),(-3L)},{0x19D82C3EL,(-3L),0x6CC69B7DL,(-10L),0x6CC69B7DL,(-3L)},{0x6CC69B7DL,0x19D82C3EL,0x29042436L,0x330A8DCEL,0x330A8DCEL,0x29042436L}};
        int32_t l_882 = 0x84991FE8L;
        int32_t l_883 = 0L;
        int32_t l_922 = 3L;
        int32_t *l_924 = &l_794;
        float **l_945 = &g_531;
        float *** const l_944 = &l_945;
        float *** const *l_943 = &l_944;
        uint16_t *l_956 = &g_23;
        int16_t l_993[6][9][4] = {{{1L,(-5L),0x290AL,0xB356L},{1L,(-3L),0x3C20L,(-5L)},{0x3C20L,(-5L),0x3C20L,(-3L)},{1L,0xB356L,0x290AL,(-5L)},{1L,0x2485L,0x3C20L,0xB356L},{0x3C20L,0xB356L,0x3C20L,0x2485L},{1L,(-5L),0x290AL,0xB356L},{1L,(-3L),0x3C20L,(-5L)},{0x3C20L,(-5L),0x3C20L,(-3L)}},{{1L,0xB356L,0x290AL,(-5L)},{1L,0x2485L,0x3C20L,0xB356L},{0x3C20L,0xB356L,0x3C20L,0x2485L},{1L,(-5L),0x290AL,0xB356L},{1L,(-3L),0x3C20L,(-5L)},{0x3C20L,(-5L),0x3C20L,(-3L)},{1L,0xB356L,0x290AL,(-5L)},{1L,0x2485L,0x3C20L,0xB356L},{0x3C20L,0xB356L,0x3C20L,0x2485L}},{{1L,(-5L),0x290AL,0xB356L},{1L,(-3L),0x3C20L,(-5L)},{0x3C20L,(-5L),0x3C20L,(-3L)},{1L,0xB356L,0x290AL,(-5L)},{1L,0x2485L,0x3C20L,0xB356L},{0x3C20L,0xB356L,0x3C20L,0x2485L},{1L,(-5L),0x290AL,0xB356L},{1L,(-3L),0x3C20L,(-5L)},{0x3C20L,(-5L),0x3C20L,(-3L)}},{{1L,0xB356L,0x290AL,(-5L)},{1L,0x2485L,0x3C20L,0xB356L},{0x3C20L,0xB356L,0x3C20L,0x2485L},{1L,(-5L),0x290AL,0xB356L},{1L,(-3L),0x3C20L,(-5L)},{0x3C20L,(-5L),0x3C20L,(-3L)},{1L,0xB356L,0x290AL,(-5L)},{0x3C20L,0xD95BL,0x290AL,(-3L)},{0x290AL,(-3L),0x290AL,0xD95BL}},{{0x3C20L,0x2485L,1L,(-3L)},{0x3C20L,0x96D5L,0x290AL,0x2485L},{0x290AL,0x2485L,0x290AL,0x96D5L},{0x3C20L,(-3L),1L,0x2485L},{0x3C20L,0xD95BL,0x290AL,(-3L)},{0x290AL,(-3L),0x290AL,0xD95BL},{0x3C20L,0x2485L,1L,(-3L)},{0x3C20L,0x96D5L,0x290AL,0x2485L},{0x290AL,0x2485L,0x290AL,0x96D5L}},{{0x3C20L,(-3L),1L,0x2485L},{0x3C20L,0xD95BL,0x290AL,(-3L)},{0x290AL,(-3L),0x290AL,0xD95BL},{0x3C20L,0x2485L,1L,(-3L)},{0x3C20L,0x96D5L,0x290AL,0x2485L},{0x290AL,0x2485L,0x290AL,0x96D5L},{0x3C20L,(-3L),1L,0x2485L},{0x3C20L,0xD95BL,0x290AL,(-3L)},{0x290AL,(-3L),0x290AL,0xD95BL}}};
        uint32_t l_1002 = 0xE0922C06L;
        float *****l_1028[2];
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 10; j++)
                l_874[i][j] = 0xA5L;
        }
        for (i = 0; i < 2; i++)
            l_1028[i] = &l_595;
        for (l_637 = 0; (l_637 <= 1); l_637 += 1)
        { /* block id: 421 */
            uint32_t *l_810 = &g_78;
            int32_t l_815 = 0x7AD63B21L;
            int32_t *l_816 = (void*)0;
            int32_t *l_817[4][1];
            int8_t *l_818 = (void*)0;
            int i, j;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 1; j++)
                    l_817[i][j] = &g_102;
            }
            g_367.f0 |= (((((*l_810) = ((l_796 = p_15) < ((0xAB1355EA60662743LL ^ g_762[3][0][0]) && p_15))) , (p_15 = ((l_814 = ((*g_675) |= (l_797 && ((((*l_605) = (-2L)) > ((*g_259) != (safe_div_func_uint16_t_u_u((l_813 == (void*)0), p_15)))) ^ 4UL)))) >= 65530UL))) & 0UL) >= l_815);
            return l_818;
        }
        for (g_65.f5 = 0; (g_65.f5 <= 1); g_65.f5 += 1)
        { /* block id: 433 */
            const uint16_t l_820 = 0xAF8CL;
            uint64_t *l_823 = &g_787[0];
            int32_t *l_839 = &g_38;
            int32_t l_843 = 0xD44D7EB7L;
            int32_t l_844 = (-3L);
            uint8_t l_873 = 0x6DL;
            int32_t *l_876 = (void*)0;
            int32_t *l_877 = &l_794;
            int32_t *l_878[1];
            uint8_t l_884 = 0UL;
            int16_t *l_895 = &g_728;
            int i;
            for (i = 0; i < 1; i++)
                l_878[i] = &l_806;
            (*l_839) ^= (((~((0x0.Ep-1 != l_820) , ((safe_sub_func_uint64_t_u_u(((*l_823) ^= ((**g_547) && (**g_745))), (safe_mod_func_uint32_t_u_u((((*l_612) = ((g_73 = (l_838 ^= (safe_add_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u((((safe_div_func_float_f_f((-0x7.Ap+1), ((safe_mul_func_float_f_f(0x5.CA4F53p+59, l_834)) != ((((((safe_sub_func_uint16_t_u_u(0x359BL, p_15)) , p_15) , (-0x2.Ap+1)) != (-0x1.4p+1)) > l_820) != l_794)))) , l_837) , p_15), 8)), l_820)))) != l_834)) & p_15), 4UL)))) != p_15))) < g_697[4].f0) < 0x35E4L);
            if (((((**g_713) = l_840) == &l_602) & (*l_839)))
            { /* block id: 440 */
                float *l_841 = &l_621;
                (*l_841) = 0xC.456D17p+49;
            }
            else
            { /* block id: 442 */
                int32_t *l_842[6][3][6] = {{{&l_606,&l_806,&l_798,(void*)0,&l_798,&l_806},{(void*)0,&l_798,&l_806,&l_606,(void*)0,&l_802},{&l_806,&l_804[1],&l_806,&l_637,&g_102,&g_102}},{{&g_102,&l_804[1],&l_804[1],&l_806,(void*)0,&l_797},{(void*)0,&l_798,&l_803,&l_803,&l_798,(void*)0},{&l_802,&l_806,&l_637,&l_797,&l_803,&l_606}},{{&l_797,&l_806,&l_606,&l_802,&g_102,&l_804[1]},{&l_797,&l_804[1],&l_802,&l_797,&l_804[1],&g_102},{&l_802,&l_803,&g_102,&l_803,&l_802,&l_606}},{{(void*)0,&l_637,&g_102,&l_806,&l_798,&l_606},{&g_102,&l_606,&l_800,&l_637,(void*)0,&l_606},{&l_806,&l_802,&g_102,&l_606,&l_606,&l_606}},{{(void*)0,&g_102,&g_102,(void*)0,&l_806,&g_102},{&l_606,&g_102,&l_802,&l_806,&l_797,&l_804[1]},{&l_637,&l_800,&l_606,&g_102,&l_797,&l_606}},{{&l_806,&g_102,&l_637,(void*)0,&l_806,(void*)0},{&l_803,&g_102,&l_803,&l_802,&l_606,&l_797},{&l_797,&l_802,&l_804[1],&l_797,(void*)0,&g_102}}};
                int i, j, k;
                l_845++;
                for (l_806 = 0; (l_806 <= 1); l_806 += 1)
                { /* block id: 446 */
                    const int32_t l_857[9] = {0xEA912E9CL,0xEA912E9CL,0xEA912E9CL,0xEA912E9CL,0xEA912E9CL,0xEA912E9CL,0xEA912E9CL,0xEA912E9CL,0xEA912E9CL};
                    int32_t l_875 = 0xB63C3EC0L;
                    int i, j, k;
                    (*g_229) = (g_848[6][0][0] , ((g_73 = (safe_lshift_func_uint16_t_u_s(p_15, ((((((((((~(((safe_mod_func_uint16_t_u_u(p_15, (p_15 ^ p_15))) <= ((*l_839) = ((p_16 , g_854[1]) , (safe_sub_func_int8_t_s_s((((*l_823) |= (l_857[2] == 0xD4L)) , p_15), (*g_630)))))) , p_15)) , l_857[2]) , g_858) , p_15) <= l_834) != 0L) , 0xAE4A5147L) == p_15) | 0x34L) != p_15)))) <= p_15));
                    for (g_73 = 0; (g_73 <= 1); g_73 += 1)
                    { /* block id: 453 */
                        float *l_859 = &l_725[0][0][8];
                        (*l_859) = 0x1.3p+1;
                    }
                    (*g_229) |= (safe_rshift_func_uint16_t_u_s(p_15, ((*l_605) = (p_15 > (((safe_unary_minus_func_int64_t_s((safe_mod_func_uint8_t_u_u((safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((*g_630) = ((g_869 , (*l_839)) & ((*l_839) <= (((safe_mul_func_int16_t_s_s((((**l_596) = l_839) == (void*)0), (((*l_612) = 1UL) | ((l_874[2][8] = (~(l_873 < (*g_259)))) , (-8L))))) <= l_802) > (-10L))))), l_838)), l_857[2])), l_796)))) > p_15) >= p_15)))));
                    if (l_875)
                        continue;
                }
            }
            --l_884;
            for (g_38 = 1; (g_38 >= 0); g_38 -= 1)
            { /* block id: 468 */
                float *l_891 = &l_608;
                int32_t l_894[6][4] = {{(-1L),0x7CD071DEL,0x7CD071DEL,(-1L)},{0L,0x7CD071DEL,7L,0x7CD071DEL},{0x7CD071DEL,0xE5DE8A97L,7L,7L},{0L,0L,0x7CD071DEL,7L},{(-1L),0xE5DE8A97L,(-1L),0x7CD071DEL},{(-1L),0x7CD071DEL,0x7CD071DEL,(-1L)}};
                float *l_896 = &l_725[0][0][8];
                float *l_897[10][7] = {{&g_459,(void*)0,&l_621,&l_621,(void*)0,&l_621,&l_621},{&l_621,&g_459,&l_621,&g_459,&l_621,&g_459,&l_621},{&g_459,&l_621,&l_621,(void*)0,(void*)0,(void*)0,&l_621},{&g_459,&g_459,&l_621,(void*)0,&g_459,(void*)0,&l_621},{(void*)0,(void*)0,&l_621,&l_621,&g_459,&l_621,&l_621},{&l_621,(void*)0,&l_621,(void*)0,&l_621,(void*)0,&l_621},{(void*)0,&l_621,&l_621,(void*)0,&g_459,&l_621,(void*)0},{&g_459,&g_459,(void*)0,&g_459,&g_459,(void*)0,(void*)0},{&l_621,(void*)0,&l_621,&l_621,(void*)0,&l_621,(void*)0},{&l_621,(void*)0,&l_621,(void*)0,&l_621,(void*)0,&l_621}};
                uint16_t ***l_917 = &l_671[0];
                struct S0 *****l_920 = &g_713;
                int8_t *l_923 = &g_887[0][3];
                int i, j;
                (**g_685) = ((g_887[0][3] > (p_16 = (safe_div_func_float_f_f((g_23 , ((*l_891) = (+0x6.68A25Cp+15))), ((*l_896) = (safe_div_func_float_f_f(((p_15 < (((***g_337) >= p_15) & l_894[2][1])) , p_16), (l_895 != l_672)))))))) , g_898);
                if ((**g_745))
                    break;
                for (g_571 = 0; (g_571 <= 1); g_571 += 1)
                { /* block id: 476 */
                    uint16_t ***l_916 = &l_671[0];
                    struct S0 *****l_921 = &g_713;
                    l_879 &= ((((safe_add_func_int8_t_s_s((((*l_877) = 0xD14FL) & (safe_mul_func_uint8_t_u_u((--(*l_612)), ((**g_745) < (***g_337))))), (((*l_823)--) <= ((l_804[3] = ((safe_sub_func_float_f_f(((safe_div_func_float_f_f((safe_add_func_float_f_f((((*l_891) = (+l_585)) == (safe_sub_func_float_f_f((l_916 == l_917), (0x7.B0FB15p+37 <= (safe_sub_func_float_f_f((l_920 != l_921), 0x1.3p+1)))))), l_922)), g_65.f4)) != l_894[5][3]), l_894[0][3])) <= l_894[2][1])) , p_15)))) < 0xFDA4L) && (*g_675)) & g_276.f4);
                    return l_923;
                }
            }
        }
        l_924 = &l_798;
        for (l_794 = 0; (l_794 <= 1); l_794 += 1)
        { /* block id: 490 */
            int16_t l_935 = 0L;
            int32_t l_946 = 0xF24C0B5EL;
            int32_t *l_947 = (void*)0;
            int32_t **l_948 = &g_229;
            int32_t **l_949[5];
            const uint32_t *l_958[7][10] = {{&g_97,&g_571,(void*)0,&g_438.f0,(void*)0,&g_571,&g_97,(void*)0,&g_523.f0,&g_438.f0},{&g_438.f0,&g_523.f0,&g_571,&g_523.f0,&g_858.f0,(void*)0,&g_97,&g_97,&g_273[3][4][3],(void*)0},{&g_523.f0,&g_523.f0,&g_523.f0,&g_523.f0,&g_438.f0,&g_858.f0,&g_97,&g_438.f0,&g_273[3][4][3],&g_273[3][4][3]},{&g_438.f0,&g_571,&g_438.f0,&g_273[0][1][3],&g_273[0][1][3],&g_438.f0,&g_571,&g_438.f0,&g_438.f0,&g_523.f0},{&g_523.f0,&g_438.f0,&g_858.f0,(void*)0,&g_438.f0,&g_438.f0,(void*)0,&g_571,&g_97,&g_273[0][1][3]},{&g_523.f0,&g_858.f0,&g_858.f0,&g_438.f0,&g_438.f0,&g_571,&g_858.f0,&g_438.f0,&g_97,(void*)0},{&g_438.f0,&g_438.f0,&g_438.f0,&g_523.f0,&g_571,&g_523.f0,&g_438.f0,&g_438.f0,&g_438.f0,&g_97}};
            const uint32_t **l_957 = &l_958[0][4];
            uint16_t ** const l_972[7] = {&l_672,&l_672,&l_672,&l_672,&l_672,&l_672,&l_672};
            int i, j;
            for (i = 0; i < 5; i++)
                l_949[i] = &l_924;
        }
    }
    if (((*g_229) = (safe_add_func_uint32_t_u_u((**g_338), p_15))))
    { /* block id: 549 */
        uint32_t l_1038 = 0xEC898F03L;
        int8_t *l_1041 = &l_805;
        l_1038++;
        return &g_887[0][7];
    }
    else
    { /* block id: 552 */
        uint16_t l_1048 = 0x21B3L;
        int32_t l_1054[6][4] = {{1L,0x47958AC7L,1L,0L},{1L,0L,0L,1L},{0xFE595DDDL,0L,9L,0L},{0L,0x47958AC7L,0x47958AC7L,0x47958AC7L},{0L,0L,1L,0x47958AC7L},{9L,0xFE595DDDL,9L,1L}};
        uint64_t * const l_1077 = &l_845;
        int32_t l_1091 = 0x0D34E237L;
        struct S0 *** const *l_1145 = (void*)0;
        int i, j;
        if (p_15)
        { /* block id: 553 */
            int8_t l_1042 = 0x4DL;
            int32_t l_1043 = (-5L);
            int32_t l_1044[2][1];
            float l_1059 = 0x1.5p-1;
            int32_t l_1061[7];
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1044[i][j] = 0x5A9EC284L;
            }
            for (i = 0; i < 7; i++)
                l_1061[i] = (-4L);
            for (g_97 = 0; (g_97 <= 1); g_97 += 1)
            { /* block id: 556 */
                uint32_t l_1045 = 0x7ECD2E22L;
                int32_t l_1052 = 0xB8232BAFL;
                int32_t l_1053 = (-10L);
                int32_t l_1055[4][6][2] = {{{(-1L),(-1L)},{0x36FA6135L,(-1L)},{(-1L),0x0B6735F3L},{(-1L),0x754FCD4AL},{0x36FA6135L,(-1L)},{0x754FCD4AL,0x0B6735F3L}},{{0x754FCD4AL,(-1L)},{0x36FA6135L,0x754FCD4AL},{(-1L),0x0B6735F3L},{(-1L),(-1L)},{0x36FA6135L,(-1L)},{(-1L),0x0B6735F3L}},{{(-1L),0x754FCD4AL},{0x36FA6135L,(-1L)},{0x754FCD4AL,0x0B6735F3L},{0x754FCD4AL,(-1L)},{0x36FA6135L,0x754FCD4AL},{(-1L),0x0B6735F3L}},{{(-1L),(-1L)},{0x36FA6135L,(-1L)},{(-1L),0x0B6735F3L},{(-1L),0x754FCD4AL},{0x36FA6135L,(-1L)},{0x754FCD4AL,0x0B6735F3L}}};
                int i, j, k;
                l_1045++;
                l_1048++;
                (**l_604) = g_1051;
                for (l_1048 = 0; (l_1048 <= 1); l_1048 += 1)
                { /* block id: 562 */
                    uint64_t l_1056 = 1UL;
                    l_1056--;
                    if (l_797)
                        goto lbl_1060;
                }
            }
lbl_1060:
            (*g_229) = (*g_229);
            --l_1062;
        }
        else
        { /* block id: 569 */
            int16_t l_1065 = 1L;
            int32_t l_1068 = 0L;
            int32_t l_1069 = 0L;
            (*g_229) |= (p_15 != l_1065);
            (*g_229) = p_15;
            for (l_1048 = 0; (l_1048 > 23); l_1048 = safe_add_func_uint8_t_u_u(l_1048, 7))
            { /* block id: 574 */
                uint64_t l_1070 = 0x63921E9A4151170ELL;
                ++l_1070;
            }
        }
        for (l_805 = 2; (l_805 >= 0); l_805 -= 1)
        { /* block id: 580 */
            int8_t l_1096 = 0x25L;
            uint32_t l_1097 = 0UL;
            int32_t l_1100 = 0xCAA9533EL;
            int32_t l_1102 = 0x01BA440FL;
            int32_t l_1106 = 0xBDC3A0C0L;
            int32_t l_1108[9][1];
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1108[i][j] = 0x003A6F72L;
            }
            if (((safe_add_func_uint16_t_u_u(((l_1054[0][1] ^ ((g_560[0][0].f5 | ((void*)0 == l_1077)) || ((!((*l_605) |= (safe_sub_func_int16_t_s_s(((safe_sub_func_uint64_t_u_u((safe_sub_func_int8_t_s_s((g_1085[0][0] , 0xB3L), (safe_unary_minus_func_uint64_t_u((safe_div_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((l_1091 |= p_15), 0x280BB43DL)), (safe_mul_func_int8_t_s_s(((((((safe_div_func_int32_t_s_s((&l_672 != (void*)0), l_1096)) ^ l_1096) != 0x4E64L) ^ 0x4BA04AB11C476BF8LL) & p_15) > p_15), l_1096)))))))), 1UL)) < (-1L)), p_15)))) <= l_1097))) ^ 0x6541L), (*g_675))) == l_1096))
            { /* block id: 583 */
                float l_1098[1];
                int32_t l_1101 = (-1L);
                int32_t l_1104 = 0x62740E43L;
                int32_t l_1107 = (-10L);
                int32_t l_1109[7][8] = {{7L,7L,7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L,7L,7L},{7L,7L,7L,7L,7L,7L,7L,7L}};
                int i, j;
                for (i = 0; i < 1; i++)
                    l_1098[i] = (-0x1.5p+1);
                if ((**g_710))
                { /* block id: 584 */
                    int32_t l_1099 = 0xDA42B1ADL;
                    int32_t l_1103 = 1L;
                    int32_t l_1105 = (-1L);
                    int64_t l_1113 = (-1L);
                    int64_t *l_1127[4][1][1];
                    int16_t *l_1130 = &g_858.f4;
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 1; j++)
                        {
                            for (k = 0; k < 1; k++)
                                l_1127[i][j][k] = (void*)0;
                        }
                    }
                    g_1110[2]++;
                    if (l_1113)
                        continue;
                    l_1105 = (safe_add_func_int64_t_s_s((safe_sub_func_uint16_t_u_u(l_1048, l_1103)), (safe_mod_func_uint64_t_u_u(((((safe_lshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s(((safe_add_func_int64_t_s_s((safe_unary_minus_func_uint64_t_u(((l_1102 = ((*l_607) = (-2L))) , (safe_add_func_int16_t_s_s(l_1109[2][3], (0xEAA3DBC7C9981D09LL & ((*l_607) = p_15))))))), ((((**g_547) = ((p_15 < p_15) <= ((*l_1130) = ((*l_605) = (g_1110[2] , 6L))))) & 0xDC36C424L) < g_65.f3))) != l_1091), g_312[1].f0)), p_15)) == l_1131) & 9UL) && 1L), 0x3C89E6B8192519FALL))));
                }
                else
                { /* block id: 594 */
                    const int32_t **l_1133 = &g_678;
                    if (p_15)
                        break;
                    (*g_1132) = &l_799;
                    (*g_1134) = ((*l_1133) = &l_1054[0][1]);
                }
                (*g_1136) = (*g_1132);
            }
            else
            { /* block id: 601 */
                uint16_t l_1137 = 6UL;
                l_1137++;
                return g_1140[2];
            }
        }
        g_1018.f0 &= (safe_rshift_func_int16_t_s_s((((g_160 = (p_15 && ((safe_sub_func_uint8_t_u_u(p_15, ((l_1145 != l_1146[2]) && ((0x02L >= (0UL && (((*l_1077) = (*g_259)) || (+(((***g_337) != p_15) >= (-2L)))))) != (**g_547))))) < 0x1509DB5DL))) , (*g_761)) ^ g_858.f4), 14));
        for (l_1131 = 0; (l_1131 < 28); l_1131 = safe_add_func_int8_t_s_s(l_1131, 7))
        { /* block id: 611 */
            (**l_604) = g_1151;
        }
    }
    if ((*g_37))
    { /* block id: 615 */
        float l_1152 = 0xC.0E917Bp-90;
        int32_t l_1153 = 1L;
        int32_t l_1154[8] = {0x7E6D4B4CL,0x7E6D4B4CL,0x7E6D4B4CL,0x7E6D4B4CL,0x7E6D4B4CL,0x7E6D4B4CL,0x7E6D4B4CL,0x7E6D4B4CL};
        int i;
        (**l_840) = (*g_457);
        --l_1156;
        return &g_887[0][3];
    }
    else
    { /* block id: 619 */
        struct S0 *l_1165[5];
        struct S0 ***l_1167 = &l_604;
        int8_t **l_1173 = &g_1140[1];
        int8_t ***l_1172 = &l_1173;
        int32_t l_1176 = (-2L);
        uint32_t **l_1179 = &l_813;
        int32_t l_1187 = 0xF59AF63EL;
        int32_t l_1188 = 0x03434616L;
        int i;
        for (i = 0; i < 5; i++)
            l_1165[i] = &g_1166;
        (*g_229) &= (safe_add_func_uint16_t_u_u(p_15, ((*l_605) = (safe_lshift_func_uint16_t_u_s((((&l_672 != &l_672) < (p_15 | (((g_65.f2 & (((safe_add_func_uint64_t_u_u(((((((((***g_713) != (l_1165[3] = (***g_713))) >= 0x1.7p+1) , (void*)0) == (void*)0) , 1UL) , (void*)0) == &l_798), p_15)) , (void*)0) == l_1167)) < 0UL) | p_15))) < p_15), p_15)))));
        l_1188 &= (safe_add_func_int64_t_s_s((safe_add_func_uint8_t_u_u((l_653 != ((*l_1172) = &g_1140[2])), (safe_mod_func_int32_t_s_s((l_1176 ^= (-2L)), (safe_rshift_func_uint16_t_u_s(((*l_672) &= (((*l_1179) = l_950[4][2][4]) != ((safe_mul_func_int8_t_s_s(((+(1L || (((((safe_lshift_func_int16_t_s_s(((**g_336) == (*g_337)), 14)) <= ((((**g_713) = (*l_1167)) != (void*)0) <= 0UL)) <= 3L) | l_1187) , p_15))) & (*g_229)), p_15)) , l_950[6][2][0]))), 6)))))), l_1187));
    }
    return (*l_653);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t  func_17(int8_t * p_18, uint16_t  p_19, const int8_t * p_20, int8_t * p_21)
{ /* block id: 321 */
    uint16_t l_584 = 0xCC55L;
    return l_584;
}


/* ------------------------------------------ */
/* 
 * reads : g_270.f5 g_571 g_228 g_229 g_102 g_37 g_38 g_547
 * writes: g_270.f5 g_571 g_229
 */
static int8_t * func_26(uint16_t * p_27, int32_t  p_28, int8_t * p_29)
{ /* block id: 301 */
    int32_t **l_582 = (void*)0;
    int32_t **l_583 = &g_229;
    for (p_28 = 0; (p_28 > (-18)); --p_28)
    { /* block id: 304 */
        int8_t *l_581 = &g_99;
        for (g_270.f5 = 12; (g_270.f5 <= (-4)); g_270.f5--)
        { /* block id: 307 */
            uint32_t l_576[4];
            int i;
            for (i = 0; i < 4; i++)
                l_576[i] = 0x9FDFCC79L;
            l_576[3]--;
            for (g_571 = 28; (g_571 != 28); g_571 = safe_add_func_int8_t_s_s(g_571, 5))
            { /* block id: 311 */
                if ((**g_228))
                    break;
                if ((*g_37))
                    continue;
            }
            return l_581;
        }
        return p_29;
    }
    (*l_583) = (*g_547);
    return &g_99;
}


/* ------------------------------------------ */
/* 
 * reads : g_13 g_37 g_38 g_228 g_229 g_547 g_270.f2 g_560 g_102 g_571 g_185
 * writes: g_13 g_38 g_229 g_102
 */
static uint16_t * func_30(int64_t  p_31, int64_t  p_32, int32_t  p_33, int8_t * p_34, float  p_35)
{ /* block id: 2 */
    uint32_t l_36[1];
    uint16_t *l_42 = &g_43;
    float ***l_517 = (void*)0;
    int32_t l_535[10][10][2] = {{{0x06B20EEFL,(-6L)},{0x9808F2C1L,(-9L)},{0xA32364FDL,0x9808F2C1L},{0x51B8FC70L,(-5L)},{0x51B8FC70L,0x9808F2C1L},{0xA32364FDL,(-9L)},{0x9808F2C1L,(-6L)},{0x06B20EEFL,(-10L)},{(-1L),(-8L)},{(-8L),0x06B20EEFL}},{{(-4L),0x1F8D0966L},{0xBDC010C3L,(-2L)},{(-6L),0x2B278FF3L},{0xF6FB8B50L,(-1L)},{0L,0x972BDD62L},{0x64067F70L,(-1L)},{(-2L),0x51B8FC70L},{(-7L),0xBDC010C3L},{0x1DB2590AL,0xBDC010C3L},{(-7L),0x51B8FC70L}},{{(-2L),(-1L)},{0x64067F70L,0x972BDD62L},{0L,(-1L)},{0xF6FB8B50L,0x2B278FF3L},{(-6L),(-2L)},{0xBDC010C3L,0x1F8D0966L},{(-4L),0x06B20EEFL},{(-8L),(-8L)},{(-1L),(-10L)},{0x06B20EEFL,(-6L)}},{{0x9808F2C1L,(-9L)},{0xA32364FDL,0x9808F2C1L},{0x51B8FC70L,(-5L)},{0x51B8FC70L,0x9808F2C1L},{0xBDC010C3L,(-5L)},{0xA32364FDL,0x972BDD62L},{(-7L),0x59B6D60BL},{0x64067F70L,0x67C7F76AL},{0x67C7F76AL,(-7L)},{(-9L),(-10L)}},{{0L,0x1DB2590AL},{0x972BDD62L,(-10L)},{0x2B278FF3L,(-6L)},{(-1L),0x9808F2C1L},{0x06B20EEFL,0x51B8FC70L},{0x1DB2590AL,0x1F8D0966L},{(-1L),0L},{0xF6FB8B50L,0L},{(-1L),0x1F8D0966L},{0x1DB2590AL,0x51B8FC70L}},{{0x06B20EEFL,0x9808F2C1L},{(-1L),(-6L)},{0x2B278FF3L,(-10L)},{0x972BDD62L,0x1DB2590AL},{0L,(-10L)},{(-9L),(-7L)},{0x67C7F76AL,0x67C7F76AL},{0x64067F70L,0x59B6D60BL},{(-7L),0x972BDD62L},{0xA32364FDL,(-5L)}},{{0xBDC010C3L,0xA32364FDL},{0x1F8D0966L,(-8L)},{0x1F8D0966L,0xA32364FDL},{0xBDC010C3L,(-5L)},{0xA32364FDL,0x972BDD62L},{(-7L),0x59B6D60BL},{0x64067F70L,0x67C7F76AL},{0x67C7F76AL,(-7L)},{(-9L),(-10L)},{0L,0x1DB2590AL}},{{0x972BDD62L,(-10L)},{0x2B278FF3L,(-6L)},{(-1L),0x9808F2C1L},{0x06B20EEFL,0x51B8FC70L},{0x1DB2590AL,0x1F8D0966L},{(-1L),0L},{0xF6FB8B50L,0L},{(-1L),0x1F8D0966L},{0x1DB2590AL,0x51B8FC70L},{0x06B20EEFL,0x9808F2C1L}},{{(-1L),(-6L)},{0x2B278FF3L,(-10L)},{0x972BDD62L,0x1DB2590AL},{0L,(-10L)},{(-9L),(-7L)},{0x67C7F76AL,0x67C7F76AL},{0x64067F70L,0x59B6D60BL},{(-7L),0x972BDD62L},{0xA32364FDL,(-5L)},{0xBDC010C3L,0xA32364FDL}},{{0x1F8D0966L,(-8L)},{0x1F8D0966L,0xA32364FDL},{0xBDC010C3L,(-5L)},{0xA32364FDL,0x972BDD62L},{(-7L),0x59B6D60BL},{0x64067F70L,0x67C7F76AL},{0x67C7F76AL,(-7L)},{(-9L),(-10L)},{0L,0x1DB2590AL},{0x972BDD62L,(-10L)}}};
    float l_545 = (-0x1.3p+1);
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_36[i] = 0UL;
    for (p_32 = 0; (p_32 >= 0); p_32 -= 1)
    { /* block id: 5 */
        uint8_t l_39 = 0xD9L;
        int32_t *l_57 = &g_38;
        const float *l_516 = (void*)0;
        const float **l_515 = &l_516;
        const float ***l_514 = &l_515;
        int16_t *l_524[8][1] = {{&g_523.f4},{&g_238.f4},{&g_523.f4},{&g_238.f4},{&g_523.f4},{&g_238.f4},{&g_523.f4},{&g_238.f4}};
        int32_t l_539 = 1L;
        int32_t l_540 = 0L;
        int i, j;
        for (g_13 = 0; (g_13 >= 0); g_13 -= 1)
        { /* block id: 8 */
            uint16_t *l_44 = (void*)0;
            int32_t l_47 = 0x7F7FFFF9L;
            int32_t *l_544[6][1] = {{&l_540},{&l_540},{&l_540},{&l_540},{&l_540},{&l_540}};
            int i, j;
            (*g_37) |= l_36[p_32];
        }
    }
    if ((*g_37))
    { /* block id: 290 */
        int32_t **l_548 = &g_229;
        (*g_547) = (*g_228);
        (*l_548) = (*g_228);
    }
    else
    { /* block id: 293 */
        const uint32_t *l_564 = &g_97;
        const uint32_t **l_563 = &l_564;
        float *l_565 = (void*)0;
        float *l_566 = &l_545;
        uint32_t l_567 = 0x4BB1423CL;
        int32_t l_568 = 1L;
        l_568 = (((safe_add_func_float_f_f(((-0x1.0p-1) <= (-((((safe_add_func_float_f_f((safe_add_func_float_f_f(p_35, ((safe_mul_func_int16_t_s_s(g_270.f2, l_36[0])) , (((*l_566) = (safe_sub_func_float_f_f((p_32 > (l_535[4][6][0] = (g_560[0][0] , p_32))), (safe_add_func_float_f_f(((p_33 , l_563) == &l_564), (-0x8.6p+1)))))) <= 0xA.83CB45p-9)))), l_567)) < p_33) < 0x2.A8AF7Ap+32) >= 0xC.9FA6A9p-11))), p_32)) , 0xB7BBD197L) != 0xDB7A93A1L);
        (*g_229) |= l_567;
        (*g_229) ^= (((safe_sub_func_int32_t_s_s(l_36[0], 4UL)) > (((((p_32 == l_36[0]) == ((p_31 >= 0x3CL) > p_31)) <= g_571) != (g_185[6][0][1] , l_568)) <= 0xC78EB8E9L)) == p_33);
    }
    return l_42;
}


/* ------------------------------------------ */
/* 
 * reads : g_65 g_4 g_78 g_73 g_37 g_38 g_99 g_13 g_110 g_43 g_185 g_185.f2 g_97 g_191 g_185.f4 g_102 g_160 g_228 g_238 g_257 g_270 g_276 g_238.f4 g_273 g_229 g_312 g_336 g_258 g_349 g_367 g_438 g_242 g_455 g_457 g_339 g_337 g_338 g_368.f1 g_259 g_497
 * writes: g_78 g_99 g_38 g_43 g_73 g_110 g_121 g_160 g_65.f3 g_191 g_65.f0 g_65.f4 g_229 g_242 g_238.f4 g_102 g_258 g_97 g_238.f1 g_337 g_65.f5 g_368 g_270.f5 g_367.f5 g_455 g_459
 */
static uint16_t * func_50(int64_t  p_51, int32_t * p_52, int8_t * p_53, int64_t  p_54)
{ /* block id: 10 */
    int16_t l_61 = 2L;
    uint64_t *l_72[9] = {&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73};
    int32_t l_74 = 0x4B879C93L;
    int32_t l_75 = 0xF937B1BDL;
    uint32_t *l_77 = &g_78;
    uint32_t *l_95 = (void*)0;
    uint32_t *l_96[1][3][8] = {{{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97}}};
    int8_t *l_98 = &g_99;
    int16_t *l_252 = &g_238.f4;
    uint16_t l_305[4][9];
    int32_t l_314 = 0xBE678901L;
    int8_t l_315 = 0xEFL;
    uint32_t **l_335 = &l_96[0][0][2];
    uint32_t *** const l_334[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_424 = (-1L);
    int32_t l_425 = 5L;
    int32_t l_426 = 0xA7561D45L;
    int32_t l_427[7] = {0x4832B7E7L,0x4832B7E7L,0x4832B7E7L,0x4832B7E7L,0x4832B7E7L,0x4832B7E7L,0x4832B7E7L};
    float l_467 = 0x1.Ap+1;
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
            l_305[i][j] = 0x0C77L;
    }
    if (((safe_sub_func_uint8_t_u_u((safe_unary_minus_func_int64_t_s(l_61)), (((~(((*l_252) = (safe_mod_func_int16_t_s_s(((g_65 , func_66(((l_75 &= (l_74 = g_4)) , (p_51 = (+((++(*l_77)) , (safe_rshift_func_uint16_t_u_s((~(g_73 , (func_84((*g_37), (g_65.f4 != g_65.f3), (safe_mul_func_int8_t_s_s(g_4, ((safe_lshift_func_int8_t_s_s(((*l_98) &= (safe_mod_func_int32_t_s_s((((l_74 = l_74) ^ l_61) && 0xB0F81A6866C5D899LL), l_61))), (*p_53))) , 0xF2L))), g_73) == l_61))), g_4)))))), p_53, p_52, g_73, l_61)) > l_61), 9UL))) >= 65535UL)) < (*p_53)) , l_61))) , (*p_52)))
    { /* block id: 113 */
        const uint64_t *l_254 = &g_73;
        const uint64_t ** const l_253 = &l_254;
        const uint64_t **l_256 = &l_254;
        const uint64_t ***l_255[10];
        const uint32_t *l_272 = &g_273[3][4][3];
        const uint32_t **l_271 = &l_272;
        int32_t l_277[8] = {1L,(-1L),(-1L),1L,(-1L),(-1L),1L,(-1L)};
        int32_t *l_278 = (void*)0;
        int32_t *l_279 = &g_102;
        int32_t *l_280 = &l_75;
        int32_t **l_281 = (void*)0;
        int32_t **l_282 = (void*)0;
        uint16_t l_299 = 0xBA1AL;
        int i;
        for (i = 0; i < 10; i++)
            l_255[i] = &l_256;
        (*g_257) = l_253;
        (*l_280) &= ((safe_mod_func_int8_t_s_s((safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(((safe_sub_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u(((g_270 , (((*l_279) = ((*p_52) = (((((*l_271) = p_52) != p_52) > (*p_53)) , ((g_73 ^= (g_99 || ((void*)0 != &l_72[1]))) , (safe_add_func_uint16_t_u_u((((g_276 , ((*l_252) |= (l_277[7] && 6L))) != 0x3FB4L) ^ g_78), 0UL)))))) < p_51)) < p_51), l_74)), 0x8AL)) || 1L), 1UL)), p_54)), g_273[3][4][3])) < p_51);
        (*g_228) = (p_52 = &l_277[6]);
        for (g_191 = (-4); (g_191 >= (-15)); g_191--)
        { /* block id: 125 */
            uint32_t l_296 = 18446744073709551606UL;
            int32_t l_297[2];
            uint16_t *l_298[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            for (i = 0; i < 2; i++)
                l_297[i] = 0L;
            l_299 ^= ((*p_53) != (((!((~(((g_43 = (((((((p_51 >= g_38) || (l_297[0] = (safe_mul_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u((~((*g_229) = (((*l_280) = ((((safe_sub_func_int8_t_s_s((safe_rshift_func_int16_t_s_s(1L, (5L != (*p_53)))), ((*l_98) = (((((g_4 <= (p_52 != p_52)) ^ l_296) , g_73) != p_54) , 1L)))) & g_110) , (*l_280)) || (*p_52))) | (*l_279)))), g_65.f0)), g_78)))) >= (*p_53)) ^ g_65.f3) , (void*)0) != (void*)0) , 4UL)) , g_160) == l_61)) >= g_38)) && 0xB1C5541EL) == l_74));
            (*l_279) |= ((*p_53) != 0x94L);
        }
    }
    else
    { /* block id: 134 */
        int32_t *l_302 = (void*)0;
        uint16_t *l_313 = &g_43;
        uint64_t **l_326 = &l_72[2];
        int32_t l_332 = (-3L);
        uint32_t **l_348 = &l_96[0][0][2];
        uint32_t l_376 = 0xF51B5936L;
        uint8_t l_395 = 255UL;
        int32_t l_409[7] = {0xA41250FDL,0xE6F56317L,0xE6F56317L,0xA41250FDL,0xE6F56317L,0xE6F56317L,0xA41250FDL};
        const int16_t l_440 = 1L;
        int i;
        if ((safe_rshift_func_int8_t_s_s((((g_273[3][4][3] == (l_75 |= (((p_52 == l_302) != (safe_rshift_func_uint8_t_u_s((l_305[1][7] | (g_270.f5 , (p_54 = ((safe_add_func_int16_t_s_s((((*l_313) = (((g_97--) | (0L <= (safe_add_func_int32_t_s_s((g_312[1] , (*g_37)), p_51)))) || l_61)) < 65527UL), l_314)) , p_54)))), l_315))) > (*p_52)))) || l_314) >= 0x1CA86679L), g_65.f3)))
        { /* block id: 139 */
            int32_t **l_329 = &l_302;
            int64_t *l_331 = &g_238.f1;
            int32_t *l_333 = &l_74;
            (*l_333) ^= ((safe_sub_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(((safe_mod_func_uint8_t_u_u(((((safe_rshift_func_uint8_t_u_s(((*p_52) || ((void*)0 == l_326)), (safe_div_func_int8_t_s_s((*p_53), 0xAAL)))) <= (((*l_329) = p_52) != p_52)) & ((*l_331) = (+l_314))) & ((l_332 , p_52) != (void*)0)), g_312[1].f1)) >= 0x6476524576960FFFLL), p_54)), 1UL)) != l_61);
            (*l_329) = (*l_329);
        }
        else
        { /* block id: 144 */
            uint32_t ** const l_350 = (void*)0;
            int8_t *l_360 = &l_315;
            int32_t l_361 = 1L;
            (*g_336) = l_334[4];
            (*p_52) |= (((void*)0 == (*g_257)) && (safe_mod_func_int16_t_s_s((safe_sub_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(((0x4C71C0AFDA356D11LL >= 0UL) <= (safe_mod_func_uint8_t_u_u((l_348 == (g_349[0] , l_350)), g_99))), (safe_rshift_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u(((*l_360) = (((+(safe_lshift_func_int16_t_s_u(((*l_252) &= g_276.f1), p_54))) , 0xC0BA828B3160A0A5LL) > 1UL)), 3)) <= p_51), l_361)), 6)))), (*p_53))), 0x8E30L)));
            for (g_65.f5 = 0; (g_65.f5 >= (-16)); g_65.f5 = safe_sub_func_int64_t_s_s(g_65.f5, 1))
            { /* block id: 151 */
                for (g_99 = 0; (g_99 > (-18)); g_99 = safe_sub_func_uint64_t_u_u(g_99, 1))
                { /* block id: 154 */
                    int32_t *l_366 = &g_102;
                    (*l_366) |= (*p_52);
                    for (l_315 = 0; (l_315 <= 0); l_315 += 1)
                    { /* block id: 158 */
                        if ((*l_366))
                            break;
                        (*g_228) = (void*)0;
                    }
                }
            }
        }
        g_368 = g_367;
        for (g_270.f5 = (-9); (g_270.f5 >= (-18)); g_270.f5 = safe_sub_func_int8_t_s_s(g_270.f5, 9))
        { /* block id: 168 */
            int32_t *l_371 = &l_74;
            int32_t l_403 = 0xA8126CA8L;
            int32_t l_410 = (-5L);
            int32_t l_412 = 6L;
            int32_t l_416 = (-1L);
            int32_t l_419 = (-8L);
            int32_t l_423[1];
            const int64_t *l_437 = &g_191;
            int i;
            for (i = 0; i < 1; i++)
                l_423[i] = 0x723F37C9L;
            (*l_371) &= (*p_52);
            for (g_367.f5 = 2; (g_367.f5 == 19); g_367.f5 = safe_add_func_uint16_t_u_u(g_367.f5, 1))
            { /* block id: 172 */
                uint8_t l_389 = 0x45L;
                int32_t l_394 = 0x4DCB59B8L;
                float l_411[5];
                int32_t l_413 = 0x5DEBCD60L;
                int32_t l_415 = 0x079303A5L;
                int32_t l_417 = 7L;
                int32_t l_418[4][3] = {{0L,0L,0L},{0L,0L,0L},{0L,0L,0L},{0L,0L,0L}};
                int64_t *l_439 = &g_242;
                int i, j;
                for (i = 0; i < 5; i++)
                    l_411[i] = 0xC.945491p-65;
                if ((*l_371))
                    break;
                (*p_52) = (safe_rshift_func_uint16_t_u_s(l_376, (safe_div_func_uint8_t_u_u((g_160 = 0UL), (safe_lshift_func_int16_t_s_s((((safe_mod_func_uint16_t_u_u((((-6L) <= ((**l_326) = (((((safe_mod_func_int16_t_s_s(0x51A8L, (((safe_mod_func_uint32_t_u_u((((safe_sub_func_int8_t_s_s((l_389 <= g_312[1].f5), (safe_lshift_func_int16_t_s_u(0x4127L, 8)))) || (p_51 ^ ((safe_lshift_func_int8_t_s_s((((l_394 = p_51) == l_75) || p_54), (*l_371))) < p_54))) >= l_389), l_395)) | p_51) , g_367.f1))) != l_314) , p_54) , (*g_257)) == (void*)0))) != 0x27A805AEL), g_349[0].f5)) || p_54) | p_54), p_51))))));
                for (g_38 = 0; (g_38 != (-1)); --g_38)
                { /* block id: 180 */
                    uint32_t l_398 = 0xABE53C10L;
                    int32_t l_402 = (-6L);
                    int32_t l_414 = (-3L);
                    int32_t l_420 = 0x367BC021L;
                    int32_t l_421 = (-9L);
                    int32_t l_422[4];
                    int i;
                    for (i = 0; i < 4; i++)
                        l_422[i] = 1L;
                    l_398 &= 0xCAC78550L;
                    for (l_75 = (-4); (l_75 > 8); l_75 = safe_add_func_int32_t_s_s(l_75, 6))
                    { /* block id: 184 */
                        int32_t *l_401 = &g_102;
                        int32_t *l_404 = &g_102;
                        int32_t *l_405 = &l_332;
                        int32_t *l_406 = &l_74;
                        int32_t *l_407 = (void*)0;
                        int32_t *l_408[6][1] = {{&l_403},{&l_314},{&l_314},{&l_403},{&l_314},{&l_314}};
                        uint32_t l_428 = 0x21E4D7CBL;
                        int i, j;
                        ++l_428;
                        if ((*p_52))
                            continue;
                        if (l_417)
                            continue;
                    }
                }
                if ((((safe_mod_func_int64_t_s_s(((*l_439) ^= (((g_185[1][2][1] , (0x6A2AF635C4C8D017LL > ((l_315 & (safe_div_func_int16_t_s_s((safe_add_func_int32_t_s_s((((l_437 == &g_242) || (g_438 , (l_418[0][1] & ((g_73 , l_417) == (-1L))))) ^ (-1L)), p_54)), p_54))) > 0x05DAL))) < 0L) <= 1UL)), 7L)) | p_54) || l_440))
                { /* block id: 191 */
                    if ((*l_371))
                        break;
                    for (g_65.f5 = 24; (g_65.f5 != (-12)); g_65.f5--)
                    { /* block id: 195 */
                        return &g_43;
                    }
                }
                else
                { /* block id: 198 */
                    uint16_t *l_443[10] = {&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7],&l_305[1][7]};
                    int i;
                    if ((*p_52))
                        break;
                    return l_313;
                }
            }
        }
        return l_313;
    }
    if ((0x48L || ((0x5BF7L < ((*l_252) = (safe_sub_func_int64_t_s_s(((safe_mul_func_int8_t_s_s(((l_305[3][8] != p_54) , (*p_53)), ((((l_314 = l_427[4]) < (l_74 , (safe_sub_func_int8_t_s_s(((p_51 >= 1L) < 0UL), 4UL)))) < l_75) || 9UL))) , p_54), l_424)))) | 0x1D121B44F8032887LL)))
    { /* block id: 208 */
        uint16_t *l_450 = (void*)0;
        return l_450;
    }
    else
    { /* block id: 210 */
        int32_t *l_451[7] = {(void*)0,(void*)0,&l_427[1],(void*)0,(void*)0,&l_427[1],(void*)0};
        uint32_t l_452[1][2][4] = {{{0x939738C6L,0x939738C6L,0x939738C6L,0x939738C6L},{0x939738C6L,0x939738C6L,0x939738C6L,0x939738C6L}}};
        int i, j, k;
        l_452[0][1][3]++;
    }
    (*g_457) = g_455;
    for (l_75 = 1; (l_75 >= 0); l_75 -= 1)
    { /* block id: 216 */
        float *l_458[8][10] = {{&g_459,&g_459,&g_459,&g_459,&g_459,(void*)0,&g_459,&g_459,&g_459,(void*)0},{&g_459,(void*)0,&g_459,(void*)0,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459},{&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459},{(void*)0,&g_459,&g_459,&g_459,&g_459,&g_459,(void*)0,&g_459,(void*)0,(void*)0},{(void*)0,(void*)0,&g_459,&g_459,&g_459,&g_459,(void*)0,(void*)0,&g_459,&g_459},{&g_459,(void*)0,(void*)0,&g_459,&g_459,&g_459,(void*)0,(void*)0,&g_459,&g_459},{&g_459,&g_459,(void*)0,&g_459,(void*)0,(void*)0,&g_459,(void*)0,&g_459,&g_459},{&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459}};
        int32_t l_496 = 0xD4A85938L;
        int32_t l_503 = (-1L);
        int i, j;
        g_459 = 0x8.7p+1;
        for (g_367.f5 = 1; (g_367.f5 >= 0); g_367.f5 -= 1)
        { /* block id: 220 */
            int64_t l_472 = (-9L);
            float ***l_474 = (void*)0;
            int32_t l_483 = 0xBF90C78AL;
            for (g_43 = 0; (g_43 <= 1); g_43 += 1)
            { /* block id: 223 */
                int32_t l_473 = 0L;
                float **l_477 = &l_458[4][5];
                uint64_t l_492 = 9UL;
                int i, j, k;
                if (((p_51 , g_339[l_75][(g_43 + 4)][(g_367.f5 + 3)]) != (***g_336)))
                { /* block id: 224 */
                    int32_t **l_460 = &g_229;
                    (*l_460) = &g_38;
                }
                else
                { /* block id: 226 */
                    int64_t *l_468 = &g_191;
                    float **l_476 = (void*)0;
                    float ***l_475 = &l_476;
                    int32_t l_484 = 0x0C71EADBL;
                    int i;
                    if (((((((**g_338) = (safe_div_func_int16_t_s_s(((((safe_lshift_func_uint16_t_u_u(((safe_mod_func_int64_t_s_s(((*l_468) = p_54), (((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_int16_t_s((g_368.f1 > p_54))), ((*l_252) = (g_99 && (((l_424 , (l_472 , ((l_473 , l_474) != l_475))) , g_367.f0) || 3UL))))) & p_54) , p_51))) , 0xBD1DL), p_54)) > l_314) <= 0UL) | (*g_37)), 1L))) | 0xEAFC7EBEL) || l_472) , 0x866B1129L) || 0x2A722A5BL))
                    { /* block id: 230 */
                        int32_t l_482 = 6L;
                        uint32_t l_491 = 0x6C48DF39L;
                        (*l_475) = l_477;
                        (*g_497) = (p_54 && ((safe_mod_func_uint32_t_u_u((l_483 ^= l_482), l_484)) < (((((((safe_mul_func_int16_t_s_s(l_472, (safe_div_func_uint64_t_u_u((g_73 = (safe_sub_func_uint32_t_u_u((l_491 = ((***g_337) &= 0x8111F090L)), l_472))), (l_492 |= p_54))))) , (safe_lshift_func_uint16_t_u_s(p_54, ((((!(p_51 >= p_51)) == p_51) , 0xD87C65D4180406FDLL) , p_51)))) == l_484) == g_13) || (*g_259)) ^ p_54) , l_496)));
                    }
                    else
                    { /* block id: 238 */
                        return &g_43;
                    }
                }
            }
            if (l_61)
                continue;
            for (g_38 = 0; (g_38 <= 1); g_38 += 1)
            { /* block id: 246 */
                int32_t l_500 = 0x21D8E638L;
                int32_t l_502[10] = {0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL,0x9B27EF6EL};
                int i, j, k;
                l_503 ^= (g_349[0].f0 < (l_502[8] |= (safe_div_func_int32_t_s_s(0x4B63D4F1L, ((((-9L) == (p_54 > (l_496 < (l_500 > ((!p_54) || (18446744073709551607UL & ((-7L) > g_65.f5))))))) , p_51) ^ l_305[1][7])))));
                l_483 = (l_503 <= (0x2723120AL | p_51));
            }
        }
    }
    return &g_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_38 g_110 g_78 g_4 g_13 g_37 g_43 g_65.f3 g_185 g_185.f2 g_65.f1 g_97 g_191 g_185.f4 g_102 g_65.f0 g_160 g_73 g_228 g_238
 * writes: g_38 g_43 g_73 g_110 g_121 g_78 g_160 g_65.f3 g_191 g_65.f0 g_65.f4 g_229 g_242 g_238.f4 g_102
 */
static int64_t  func_66(int64_t  p_67, int8_t * p_68, int32_t * p_69, int8_t  p_70, int16_t  p_71)
{ /* block id: 19 */
    int32_t *l_101[7] = {&g_102,&g_102,(void*)0,&g_102,&g_102,(void*)0,&g_102};
    float l_103 = 0x4.AD0BC0p+9;
    uint32_t l_104 = 4294967292UL;
    int16_t l_105 = 0xD634L;
    uint64_t **l_120 = (void*)0;
    int32_t l_178 = 0xE4A769E6L;
    float *l_233 = (void*)0;
    float **l_232 = &l_233;
    int i;
    l_105 &= (l_104 &= (*p_69));
lbl_119:
    for (g_38 = 0; (g_38 >= 11); g_38 = safe_add_func_int16_t_s_s(g_38, 4))
    { /* block id: 24 */
        int32_t l_109 = 0x57E67F59L;
        int32_t **l_116 = &l_101[4];
        for (g_43 = 0; (g_43 <= 6); g_43 += 1)
        { /* block id: 27 */
            float *l_113 = &l_103;
            for (g_73 = 0; (g_73 <= 6); g_73 += 1)
            { /* block id: 30 */
                int32_t l_108 = 0xE8548DC0L;
                int i;
                g_110++;
            }
            (*l_113) = 0xB.991B7Fp-11;
            for (l_109 = 26; (l_109 > (-30)); --l_109)
            { /* block id: 36 */
                return p_67;
            }
            return p_71;
        }
        (*l_116) = p_69;
    }
    for (g_38 = 8; (g_38 > (-25)); --g_38)
    { /* block id: 45 */
        uint16_t l_122 = 0UL;
        float *l_126 = (void*)0;
        float **l_125 = &l_126;
        int32_t l_166 = 0x4F5D4AB6L;
        int32_t l_167 = 1L;
        int32_t l_175 = 0L;
        int32_t l_177[5];
        uint32_t l_181 = 0xD559E113L;
        int8_t l_239 = 3L;
        int i;
        for (i = 0; i < 5; i++)
            l_177[i] = (-5L);
        if (p_67)
            goto lbl_119;
        g_121 = l_120;
        --l_122;
        for (l_122 = 0; (l_122 <= 6); l_122 += 1)
        { /* block id: 51 */
            float ***l_128 = &l_125;
            uint32_t *l_129 = (void*)0;
            uint32_t *l_130 = &g_78;
            uint16_t *l_131 = &g_43;
            int32_t l_161 = 0x3CA981A8L;
            int32_t l_169 = 0x7128C3FFL;
            int32_t l_172 = 0xA856C88DL;
            int32_t l_174[9] = {0xD3AC039BL,0xD3AC039BL,0xD3AC039BL,0xD3AC039BL,0xD3AC039BL,0xD3AC039BL,0xD3AC039BL,0xD3AC039BL,0xD3AC039BL};
            float l_192 = 0x1.9p+1;
            float l_222 = 0x9.5C612Bp-88;
            int i;
            (*l_128) = l_125;
            if ((65530UL || ((l_122 , &g_43) != (((*l_130) ^= 0x5B336EB9L) , l_131))))
            { /* block id: 54 */
                int32_t l_153 = 0x59EA234EL;
                const float l_158 = 0xD.3DBF9Dp+59;
                int32_t l_162 = 1L;
                int32_t l_163[7][2];
                int16_t l_165[10] = {0xA50FL,0xA2B0L,0xA2B0L,0xA50FL,0xA2B0L,0xA2B0L,0xA50FL,0xA2B0L,0xA2B0L,0xA50FL};
                int32_t l_176 = 0x0DA6F0B9L;
                int i, j;
                for (i = 0; i < 7; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_163[i][j] = 0x478149C9L;
                }
                for (l_105 = 0; (l_105 <= 6); l_105 += 1)
                { /* block id: 57 */
                    uint8_t *l_159[10] = {&g_160,&g_160,&g_160,&g_160,&g_160,&g_160,&g_160,&g_160,&g_160,&g_160};
                    int32_t l_164 = 0x5561C656L;
                    int32_t l_168 = 0x162A55EFL;
                    int32_t l_170 = 0x2407D1D3L;
                    int32_t l_173[1][10][10] = {{{6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L},{6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L},{0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL},{6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L},{6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L},{0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL},{6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L},{6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L},{0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL,6L,0x08701CCAL,0x08701CCAL},{6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L,6L,0x7FEA8024L,6L}}};
                    int16_t l_179 = 1L;
                    uint32_t *l_184[3];
                    int64_t *l_190 = &g_191;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_184[i] = (void*)0;
                    l_161 |= ((((safe_rshift_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(p_71, (safe_add_func_int64_t_s_s((safe_div_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((g_160 = (((((0L && ((void*)0 == &l_101[l_122])) ^ (safe_rshift_func_int16_t_s_s((!(safe_sub_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u(((safe_mod_func_uint32_t_u_u(g_38, (*p_69))) != ((*p_69) , (safe_add_func_int64_t_s_s(l_153, (safe_mod_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(((&p_69 != &g_37) , g_4), g_110)), p_67)))))), l_153)), p_71))), 12))) & p_71) == g_13) >= (*p_68))), (*p_68))), l_153)), p_71)))), p_70)) , (*g_37)) <= g_43) >= 0x66L);
                    ++l_181;
                    g_65.f0 ^= ((((g_65.f3 = g_43) , ((g_65.f3 < (g_185[7][1][5] , (safe_add_func_int32_t_s_s((((((*l_190) ^= ((safe_rshift_func_uint8_t_u_u((((((-9L) & g_185[7][1][5].f2) ^ g_38) < (((((l_101[l_105] != p_69) <= g_65.f1) >= g_65.f3) > g_97) | l_177[2])) <= 0L), g_65.f1)) == (-1L))) & 8UL) | (*p_68)) && g_185[7][1][5].f4), l_165[4])))) && l_174[8])) > g_102) > 0xE1L);
                }
                l_177[1] = ((-3L) | 65532UL);
                l_101[l_122] = l_101[l_122];
                for (l_176 = 0; (l_176 >= (-16)); --l_176)
                { /* block id: 69 */
                    int64_t l_220 = 7L;
                    int32_t l_221 = 5L;
                    int32_t l_223[7][7][5] = {{{0x571168ABL,0x758EC46EL,3L,0L,0xE7533F88L},{(-1L),0xED9C82AFL,0x4C0A3D98L,0x79F272DAL,(-1L)},{0x51C4F3B5L,(-3L),1L,0x758EC46EL,0L},{0xF4DEF939L,(-1L),0x4AB6A065L,0xEB4F8A2EL,0xE16C9F1EL},{0L,0xAF958F17L,0x38F8FDFCL,0L,0x785BB1C7L},{0L,0xEB4F8A2EL,0L,0x4C34E1C5L,(-1L)},{0L,0x937974BFL,3L,0xAF958F17L,0L}},{{0xF4DEF939L,1L,(-1L),1L,0xF4DEF939L},{0x51C4F3B5L,1L,(-1L),0xE7533F88L,(-3L)},{(-1L),0x4C34E1C5L,0L,0xEB4F8A2EL,0L},{0x571168ABL,(-3L),(-7L),1L,(-3L)},{0xE16C9F1EL,0xEB4F8A2EL,0x4AB6A065L,(-1L),0xF4DEF939L},{(-3L),1L,0x758EC46EL,0L,0L},{(-1L),0x79F272DAL,0x4C0A3D98L,0xED9C82AFL,(-1L)}},{{0x937974BFL,(-3L),(-1L),1L,0x785BB1C7L},{0xF4DEF939L,(-6L),0xA8CEDDB2L,0xED9C82AFL,0xE16C9F1EL},{0x571168ABL,1L,(-4L),0L,0L},{0x4C0A3D98L,(-6L),0x4C0A3D98L,9L,0L},{0x51C4F3B5L,0xE2D881AEL,0x38F8FDFCL,0x758EC46EL,0x8FADC044L},{0xE16C9F1EL,(-1L),(-1L),0xF5521942L,0xE16C9F1EL},{0x8FADC044L,1L,0x38F8FDFCL,0x8FADC044L,0x51C4F3B5L}},{{0L,(-1L),0x4C0A3D98L,0x4C34E1C5L,0x4C0A3D98L},{0xE7533F88L,0x937974BFL,0xE4BBC024L,1L,0L},{0L,0x4C34E1C5L,(-1L),1L,0xE16C9F1EL},{0x51C4F3B5L,(-7L),0x5F611623L,0xE7533F88L,0xE2D881AEL},{0L,0x4C34E1C5L,(-1L),0xF5521942L,0L},{0L,0x937974BFL,(-7L),(-7L),0x937974BFL},{0xE16C9F1EL,(-1L),1L,(-1L),0L}},{{0x937974BFL,1L,1L,0xE7533F88L,0L},{0x8CB54408L,(-1L),0x4C0A3D98L,0x818E70DCL,0L},{0x937974BFL,0xE2D881AEL,0x5F611623L,1L,(-1L)},{0xE16C9F1EL,(-6L),0xB7A331CBL,(-6L),0xE16C9F1EL},{0L,0x758EC46EL,(-4L),0x8FADC044L,0xE7533F88L},{0L,0x818E70DCL,0x4C0A3D98L,(-1L),0x8CB54408L},{0x51C4F3B5L,0x937974BFL,0xAF958F17L,0x758EC46EL,0xE7533F88L}},{{0L,(-1L),1L,(-1L),0xE16C9F1EL},{0xE7533F88L,0x38F8FDFCL,0x38F8FDFCL,0xE7533F88L,(-1L)},{0L,0xF5521942L,(-1L),0x4C34E1C5L,0L},{0x8FADC044L,0x937974BFL,(-4L),0x38F8FDFCL,0L},{0xE16C9F1EL,1L,(-1L),0x4C34E1C5L,0L},{0x51C4F3B5L,0x758EC46EL,0x13E63DB5L,0xE7533F88L,0x937974BFL},{0x4C0A3D98L,0x4C34E1C5L,0x4C0A3D98L,(-1L),0L}},{{0L,0xE2D881AEL,(-7L),0x758EC46EL,0xE2D881AEL},{0xE16C9F1EL,0xF5521942L,(-1L),(-1L),0xE16C9F1EL},{0xE2D881AEL,1L,(-7L),0x8FADC044L,0L},{0L,9L,0x4C0A3D98L,(-6L),0x4C0A3D98L},{0x937974BFL,0x937974BFL,0x13E63DB5L,1L,0x51C4F3B5L},{0L,(-6L),(-1L),0x818E70DCL,0xE16C9F1EL},{0L,(-7L),(-4L),0xE7533F88L,0x8FADC044L}}};
                    int i, j, k;
                    if (l_167)
                        break;
                    l_174[2] |= (g_38 && g_65.f3);
                    if ((&g_97 != (void*)0))
                    { /* block id: 72 */
                        uint32_t l_210 = 4294967295UL;
                        g_65.f0 = (l_163[0][0] &= (safe_mod_func_int8_t_s_s((((*g_37) ^ (l_174[0] = 4294967292UL)) , (0L <= (g_65.f4 = ((safe_unary_minus_func_int16_t_s(((safe_lshift_func_int16_t_s_u(0xE7CBL, ((*g_37) == (safe_rshift_func_uint8_t_u_u((safe_add_func_int64_t_s_s(((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(l_174[3], 8)), ((((safe_lshift_func_int8_t_s_s(1L, 5)) ^ (0x8A9B1481L < (((void*)0 != l_101[l_122]) || 253UL))) , 0UL) , l_210))) > g_160), l_166)), 4))))) ^ (-5L)))) , 0x6850E3D9L)))), 0x2BL)));
                    }
                    else
                    { /* block id: 77 */
                        int32_t l_211 = 0xCCC60F6CL;
                        int32_t **l_212 = &l_101[6];
                        if (l_211)
                            break;
                        (*l_212) = p_69;
                        return p_67;
                    }
                    if ((((((*l_131) |= ((-3L) || (safe_mod_func_uint32_t_u_u(((safe_add_func_int32_t_s_s(((safe_sub_func_int8_t_s_s((!p_70), l_220)) == g_110), (g_65.f4 = (*p_69)))) == ((((void*)0 != &l_126) && (-7L)) == (g_65.f0 & g_73))), (*p_69))))) <= (-10L)) , 0x5A87L) == p_67))
                    { /* block id: 84 */
                        if ((*g_37))
                            break;
                    }
                    else
                    { /* block id: 86 */
                        uint32_t l_224 = 1UL;
                        l_224--;
                        return p_67;
                    }
                }
            }
            else
            { /* block id: 91 */
                int32_t *l_227 = &l_169;
                (*g_228) = l_227;
                for (g_73 = 0; (g_73 < 15); g_73 = safe_add_func_int16_t_s_s(g_73, 7))
                { /* block id: 95 */
                    int64_t l_245 = 7L;
                    uint8_t *l_246[7][2];
                    int16_t *l_247 = (void*)0;
                    int16_t *l_248 = &g_238.f4;
                    int i, j;
                    for (i = 0; i < 7; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_246[i][j] = (void*)0;
                    }
                    g_102 ^= (((void*)0 != l_232) < (safe_div_func_int16_t_s_s(((*l_248) = ((safe_sub_func_int8_t_s_s((g_65.f3 , (((*l_227) = (p_67 ^ p_70)) >= (g_238 , (l_239 == (l_175 = (((safe_rshift_func_uint8_t_u_s(((g_242 = (l_167 = (*g_37))) & (safe_div_func_uint8_t_u_u((1UL && 0UL), 3L))), l_245)) , l_166) && (*p_68))))))), l_174[3])) , 1L)), 0x1272L)));
                    for (g_102 = (-29); (g_102 != (-5)); g_102 = safe_add_func_uint8_t_u_u(g_102, 8))
                    { /* block id: 104 */
                        l_161 &= (safe_unary_minus_func_int64_t_s(p_70));
                    }
                }
            }
        }
    }
    return p_71;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_84(int32_t  p_85, int8_t  p_86, uint64_t  p_87, int32_t  p_88)
{ /* block id: 16 */
    int32_t l_100 = 0x3F1199D3L;
    return l_100;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    transparent_crc(g_38, "g_38", print_hash_value);
    transparent_crc(g_43, "g_43", print_hash_value);
    transparent_crc(g_65.f0, "g_65.f0", print_hash_value);
    transparent_crc(g_65.f1, "g_65.f1", print_hash_value);
    transparent_crc(g_65.f2, "g_65.f2", print_hash_value);
    transparent_crc(g_65.f3, "g_65.f3", print_hash_value);
    transparent_crc(g_65.f4, "g_65.f4", print_hash_value);
    transparent_crc(g_65.f5, "g_65.f5", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    transparent_crc(g_99, "g_99", print_hash_value);
    transparent_crc(g_102, "g_102", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_171[i], "g_171[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_180, "g_180", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_185[i][j][k].f0, "g_185[i][j][k].f0", print_hash_value);
                transparent_crc(g_185[i][j][k].f2, "g_185[i][j][k].f2", print_hash_value);
                transparent_crc(g_185[i][j][k].f4, "g_185[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_191, "g_191", print_hash_value);
    transparent_crc(g_238.f4, "g_238.f4", print_hash_value);
    transparent_crc(g_242, "g_242", print_hash_value);
    transparent_crc(g_270.f0, "g_270.f0", print_hash_value);
    transparent_crc(g_270.f1, "g_270.f1", print_hash_value);
    transparent_crc(g_270.f2, "g_270.f2", print_hash_value);
    transparent_crc(g_270.f3, "g_270.f3", print_hash_value);
    transparent_crc(g_270.f4, "g_270.f4", print_hash_value);
    transparent_crc(g_270.f5, "g_270.f5", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_273[i][j][k], "g_273[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_276.f0, "g_276.f0", print_hash_value);
    transparent_crc(g_276.f1, "g_276.f1", print_hash_value);
    transparent_crc(g_276.f2, "g_276.f2", print_hash_value);
    transparent_crc(g_276.f3, "g_276.f3", print_hash_value);
    transparent_crc(g_276.f4, "g_276.f4", print_hash_value);
    transparent_crc(g_276.f5, "g_276.f5", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_312[i].f0, "g_312[i].f0", print_hash_value);
        transparent_crc(g_312[i].f1, "g_312[i].f1", print_hash_value);
        transparent_crc(g_312[i].f2, "g_312[i].f2", print_hash_value);
        transparent_crc(g_312[i].f3, "g_312[i].f3", print_hash_value);
        transparent_crc(g_312[i].f4, "g_312[i].f4", print_hash_value);
        transparent_crc(g_312[i].f5, "g_312[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_349[i].f0, "g_349[i].f0", print_hash_value);
        transparent_crc(g_349[i].f1, "g_349[i].f1", print_hash_value);
        transparent_crc(g_349[i].f2, "g_349[i].f2", print_hash_value);
        transparent_crc(g_349[i].f3, "g_349[i].f3", print_hash_value);
        transparent_crc(g_349[i].f4, "g_349[i].f4", print_hash_value);
        transparent_crc(g_349[i].f5, "g_349[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_367.f0, "g_367.f0", print_hash_value);
    transparent_crc(g_367.f1, "g_367.f1", print_hash_value);
    transparent_crc(g_367.f2, "g_367.f2", print_hash_value);
    transparent_crc(g_367.f3, "g_367.f3", print_hash_value);
    transparent_crc(g_367.f4, "g_367.f4", print_hash_value);
    transparent_crc(g_367.f5, "g_367.f5", print_hash_value);
    transparent_crc(g_368.f0, "g_368.f0", print_hash_value);
    transparent_crc(g_368.f1, "g_368.f1", print_hash_value);
    transparent_crc(g_368.f2, "g_368.f2", print_hash_value);
    transparent_crc(g_368.f3, "g_368.f3", print_hash_value);
    transparent_crc(g_368.f4, "g_368.f4", print_hash_value);
    transparent_crc(g_368.f5, "g_368.f5", print_hash_value);
    transparent_crc(g_438.f0, "g_438.f0", print_hash_value);
    transparent_crc(g_438.f2, "g_438.f2", print_hash_value);
    transparent_crc(g_438.f4, "g_438.f4", print_hash_value);
    transparent_crc(g_455.f0, "g_455.f0", print_hash_value);
    transparent_crc(g_455.f1, "g_455.f1", print_hash_value);
    transparent_crc(g_455.f2, "g_455.f2", print_hash_value);
    transparent_crc(g_455.f3, "g_455.f3", print_hash_value);
    transparent_crc(g_455.f4, "g_455.f4", print_hash_value);
    transparent_crc(g_455.f5, "g_455.f5", print_hash_value);
    transparent_crc_bytes (&g_459, sizeof(g_459), "g_459", print_hash_value);
    transparent_crc(g_523.f4, "g_523.f4", print_hash_value);
    transparent_crc(g_533.f0, "g_533.f0", print_hash_value);
    transparent_crc(g_533.f1, "g_533.f1", print_hash_value);
    transparent_crc(g_533.f2, "g_533.f2", print_hash_value);
    transparent_crc(g_533.f3, "g_533.f3", print_hash_value);
    transparent_crc(g_533.f4, "g_533.f4", print_hash_value);
    transparent_crc(g_533.f5, "g_533.f5", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_560[i][j].f0, "g_560[i][j].f0", print_hash_value);
            transparent_crc(g_560[i][j].f1, "g_560[i][j].f1", print_hash_value);
            transparent_crc(g_560[i][j].f2, "g_560[i][j].f2", print_hash_value);
            transparent_crc(g_560[i][j].f3, "g_560[i][j].f3", print_hash_value);
            transparent_crc(g_560[i][j].f4, "g_560[i][j].f4", print_hash_value);
            transparent_crc(g_560[i][j].f5, "g_560[i][j].f5", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_571, "g_571", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_697[i].f0, "g_697[i].f0", print_hash_value);
        transparent_crc(g_697[i].f1, "g_697[i].f1", print_hash_value);
        transparent_crc(g_697[i].f2, "g_697[i].f2", print_hash_value);
        transparent_crc(g_697[i].f3, "g_697[i].f3", print_hash_value);
        transparent_crc(g_697[i].f4, "g_697[i].f4", print_hash_value);
        transparent_crc(g_697[i].f5, "g_697[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_728, "g_728", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_762[i][j][k], "g_762[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_787[i], "g_787[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_848[i][j][k].f0, "g_848[i][j][k].f0", print_hash_value);
                transparent_crc(g_848[i][j][k].f1, "g_848[i][j][k].f1", print_hash_value);
                transparent_crc(g_848[i][j][k].f2, "g_848[i][j][k].f2", print_hash_value);
                transparent_crc(g_848[i][j][k].f3, "g_848[i][j][k].f3", print_hash_value);
                transparent_crc(g_848[i][j][k].f4, "g_848[i][j][k].f4", print_hash_value);
                transparent_crc(g_848[i][j][k].f5, "g_848[i][j][k].f5", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_854[i].f0, "g_854[i].f0", print_hash_value);
        transparent_crc(g_854[i].f2, "g_854[i].f2", print_hash_value);
        transparent_crc(g_854[i].f4, "g_854[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_858.f4, "g_858.f4", print_hash_value);
    transparent_crc(g_869.f0, "g_869.f0", print_hash_value);
    transparent_crc(g_869.f2, "g_869.f2", print_hash_value);
    transparent_crc(g_869.f4, "g_869.f4", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_887[i][j], "g_887[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_898.f0, "g_898.f0", print_hash_value);
    transparent_crc(g_898.f1, "g_898.f1", print_hash_value);
    transparent_crc(g_898.f2, "g_898.f2", print_hash_value);
    transparent_crc(g_898.f3, "g_898.f3", print_hash_value);
    transparent_crc(g_898.f4, "g_898.f4", print_hash_value);
    transparent_crc(g_898.f5, "g_898.f5", print_hash_value);
    transparent_crc(g_991.f0, "g_991.f0", print_hash_value);
    transparent_crc(g_991.f1, "g_991.f1", print_hash_value);
    transparent_crc(g_991.f2, "g_991.f2", print_hash_value);
    transparent_crc(g_991.f3, "g_991.f3", print_hash_value);
    transparent_crc(g_991.f4, "g_991.f4", print_hash_value);
    transparent_crc(g_991.f5, "g_991.f5", print_hash_value);
    transparent_crc(g_1018.f0, "g_1018.f0", print_hash_value);
    transparent_crc(g_1018.f1, "g_1018.f1", print_hash_value);
    transparent_crc(g_1018.f2, "g_1018.f2", print_hash_value);
    transparent_crc(g_1018.f3, "g_1018.f3", print_hash_value);
    transparent_crc(g_1018.f4, "g_1018.f4", print_hash_value);
    transparent_crc(g_1018.f5, "g_1018.f5", print_hash_value);
    transparent_crc(g_1051.f0, "g_1051.f0", print_hash_value);
    transparent_crc(g_1051.f1, "g_1051.f1", print_hash_value);
    transparent_crc(g_1051.f2, "g_1051.f2", print_hash_value);
    transparent_crc(g_1051.f3, "g_1051.f3", print_hash_value);
    transparent_crc(g_1051.f4, "g_1051.f4", print_hash_value);
    transparent_crc(g_1051.f5, "g_1051.f5", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1085[i][j].f0, "g_1085[i][j].f0", print_hash_value);
            transparent_crc(g_1085[i][j].f2, "g_1085[i][j].f2", print_hash_value);
            transparent_crc(g_1085[i][j].f4, "g_1085[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1110[i], "g_1110[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1148, sizeof(g_1148), "g_1148", print_hash_value);
    transparent_crc(g_1151.f0, "g_1151.f0", print_hash_value);
    transparent_crc(g_1151.f1, "g_1151.f1", print_hash_value);
    transparent_crc(g_1151.f2, "g_1151.f2", print_hash_value);
    transparent_crc(g_1151.f3, "g_1151.f3", print_hash_value);
    transparent_crc(g_1151.f4, "g_1151.f4", print_hash_value);
    transparent_crc(g_1151.f5, "g_1151.f5", print_hash_value);
    transparent_crc(g_1166.f0, "g_1166.f0", print_hash_value);
    transparent_crc(g_1166.f1, "g_1166.f1", print_hash_value);
    transparent_crc(g_1166.f2, "g_1166.f2", print_hash_value);
    transparent_crc(g_1166.f3, "g_1166.f3", print_hash_value);
    transparent_crc(g_1166.f4, "g_1166.f4", print_hash_value);
    transparent_crc(g_1166.f5, "g_1166.f5", print_hash_value);
    transparent_crc(g_1200.f0, "g_1200.f0", print_hash_value);
    transparent_crc(g_1200.f1, "g_1200.f1", print_hash_value);
    transparent_crc(g_1200.f2, "g_1200.f2", print_hash_value);
    transparent_crc(g_1200.f3, "g_1200.f3", print_hash_value);
    transparent_crc(g_1200.f4, "g_1200.f4", print_hash_value);
    transparent_crc(g_1200.f5, "g_1200.f5", print_hash_value);
    transparent_crc(g_1224.f0, "g_1224.f0", print_hash_value);
    transparent_crc(g_1224.f1, "g_1224.f1", print_hash_value);
    transparent_crc(g_1224.f2, "g_1224.f2", print_hash_value);
    transparent_crc(g_1224.f3, "g_1224.f3", print_hash_value);
    transparent_crc(g_1224.f4, "g_1224.f4", print_hash_value);
    transparent_crc(g_1224.f5, "g_1224.f5", print_hash_value);
    transparent_crc(g_1235, "g_1235", print_hash_value);
    transparent_crc(g_1317.f0, "g_1317.f0", print_hash_value);
    transparent_crc(g_1317.f1, "g_1317.f1", print_hash_value);
    transparent_crc(g_1317.f2, "g_1317.f2", print_hash_value);
    transparent_crc(g_1317.f3, "g_1317.f3", print_hash_value);
    transparent_crc(g_1317.f4, "g_1317.f4", print_hash_value);
    transparent_crc(g_1317.f5, "g_1317.f5", print_hash_value);
    transparent_crc(g_1318.f0, "g_1318.f0", print_hash_value);
    transparent_crc(g_1318.f1, "g_1318.f1", print_hash_value);
    transparent_crc(g_1318.f2, "g_1318.f2", print_hash_value);
    transparent_crc(g_1318.f3, "g_1318.f3", print_hash_value);
    transparent_crc(g_1318.f4, "g_1318.f4", print_hash_value);
    transparent_crc(g_1318.f5, "g_1318.f5", print_hash_value);
    transparent_crc(g_1319.f0, "g_1319.f0", print_hash_value);
    transparent_crc(g_1319.f1, "g_1319.f1", print_hash_value);
    transparent_crc(g_1319.f2, "g_1319.f2", print_hash_value);
    transparent_crc(g_1319.f3, "g_1319.f3", print_hash_value);
    transparent_crc(g_1319.f4, "g_1319.f4", print_hash_value);
    transparent_crc(g_1319.f5, "g_1319.f5", print_hash_value);
    transparent_crc(g_1320.f0, "g_1320.f0", print_hash_value);
    transparent_crc(g_1320.f1, "g_1320.f1", print_hash_value);
    transparent_crc(g_1320.f2, "g_1320.f2", print_hash_value);
    transparent_crc(g_1320.f3, "g_1320.f3", print_hash_value);
    transparent_crc(g_1320.f4, "g_1320.f4", print_hash_value);
    transparent_crc(g_1320.f5, "g_1320.f5", print_hash_value);
    transparent_crc(g_1322.f0, "g_1322.f0", print_hash_value);
    transparent_crc(g_1322.f1, "g_1322.f1", print_hash_value);
    transparent_crc(g_1322.f2, "g_1322.f2", print_hash_value);
    transparent_crc(g_1322.f3, "g_1322.f3", print_hash_value);
    transparent_crc(g_1322.f4, "g_1322.f4", print_hash_value);
    transparent_crc(g_1322.f5, "g_1322.f5", print_hash_value);
    transparent_crc(g_1340.f0, "g_1340.f0", print_hash_value);
    transparent_crc(g_1340.f1, "g_1340.f1", print_hash_value);
    transparent_crc(g_1340.f2, "g_1340.f2", print_hash_value);
    transparent_crc(g_1340.f3, "g_1340.f3", print_hash_value);
    transparent_crc(g_1340.f4, "g_1340.f4", print_hash_value);
    transparent_crc(g_1340.f5, "g_1340.f5", print_hash_value);
    transparent_crc(g_1368.f0, "g_1368.f0", print_hash_value);
    transparent_crc(g_1368.f2, "g_1368.f2", print_hash_value);
    transparent_crc(g_1368.f4, "g_1368.f4", print_hash_value);
    transparent_crc(g_1376.f0, "g_1376.f0", print_hash_value);
    transparent_crc(g_1376.f2, "g_1376.f2", print_hash_value);
    transparent_crc(g_1376.f4, "g_1376.f4", print_hash_value);
    transparent_crc(g_1427, "g_1427", print_hash_value);
    transparent_crc(g_1433.f0, "g_1433.f0", print_hash_value);
    transparent_crc(g_1433.f1, "g_1433.f1", print_hash_value);
    transparent_crc(g_1433.f2, "g_1433.f2", print_hash_value);
    transparent_crc(g_1433.f3, "g_1433.f3", print_hash_value);
    transparent_crc(g_1433.f4, "g_1433.f4", print_hash_value);
    transparent_crc(g_1433.f5, "g_1433.f5", print_hash_value);
    transparent_crc(g_1434.f0, "g_1434.f0", print_hash_value);
    transparent_crc(g_1434.f1, "g_1434.f1", print_hash_value);
    transparent_crc(g_1434.f2, "g_1434.f2", print_hash_value);
    transparent_crc(g_1434.f3, "g_1434.f3", print_hash_value);
    transparent_crc(g_1434.f4, "g_1434.f4", print_hash_value);
    transparent_crc(g_1434.f5, "g_1434.f5", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1435[i].f0, "g_1435[i].f0", print_hash_value);
        transparent_crc(g_1435[i].f1, "g_1435[i].f1", print_hash_value);
        transparent_crc(g_1435[i].f2, "g_1435[i].f2", print_hash_value);
        transparent_crc(g_1435[i].f3, "g_1435[i].f3", print_hash_value);
        transparent_crc(g_1435[i].f4, "g_1435[i].f4", print_hash_value);
        transparent_crc(g_1435[i].f5, "g_1435[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 406
   depth: 1, occurrence: 28
XXX total union variables: 10

XXX non-zero bitfields defined in structs: 8
XXX zero bitfields defined in structs: 2
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 2
XXX structs with bitfields in the program: 56
breakdown:
   indirect level: 0, occurrence: 38
   indirect level: 1, occurrence: 6
   indirect level: 2, occurrence: 3
   indirect level: 3, occurrence: 3
   indirect level: 4, occurrence: 4
   indirect level: 5, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 45
XXX times a bitfields struct on LHS: 6
XXX times a bitfields struct on RHS: 40
XXX times a single bitfield on LHS: 9
XXX times a single bitfield on RHS: 53

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 255
   depth: 2, occurrence: 65
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
   depth: 6, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 20, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 4
   depth: 24, occurrence: 4
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 27, occurrence: 2
   depth: 28, occurrence: 3
   depth: 30, occurrence: 1
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 353

XXX times a variable address is taken: 741
XXX times a pointer is dereferenced on RHS: 146
breakdown:
   depth: 1, occurrence: 111
   depth: 2, occurrence: 22
   depth: 3, occurrence: 10
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 182
breakdown:
   depth: 1, occurrence: 160
   depth: 2, occurrence: 19
   depth: 3, occurrence: 2
   depth: 4, occurrence: 0
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 24
XXX times a pointer is compared with address of another variable: 4
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 6014

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 667
   level: 2, occurrence: 132
   level: 3, occurrence: 29
   level: 4, occurrence: 12
   level: 5, occurrence: 3
XXX number of pointers point to pointers: 151
XXX number of pointers point to scalars: 190
XXX number of pointers point to structs: 10
XXX percent of pointers has null in alias set: 26.1
XXX average alias set size: 1.5

XXX times a non-volatile is read: 1062
XXX times a non-volatile is write: 580
XXX times a volatile is read: 98
XXX    times read thru a pointer: 11
XXX times a volatile is write: 33
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 5.23e+03
XXX percentage of non-volatile access: 92.6

XXX forward jumps: 1
XXX backward jumps: 1

XXX stmts: 251
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 54
   depth: 2, occurrence: 43
   depth: 3, occurrence: 42
   depth: 4, occurrence: 45
   depth: 5, occurrence: 36

XXX percentage a fresh-made variable is used: 18.4
XXX percentage an existing variable is used: 81.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      4099338585
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const uint32_t  f0;
   volatile int8_t * const  f1;
};

union U1 {
   uint32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2[7] = {0xE1D925B0L,0xE1D925B0L,0xE1D925B0L,0xE1D925B0L,0xE1D925B0L,0xE1D925B0L,0xE1D925B0L};
static volatile int32_t g_3 = 0x6D4F2887L;/* VOLATILE GLOBAL g_3 */
static volatile int32_t g_4 = 0xE5664DC9L;/* VOLATILE GLOBAL g_4 */
static int32_t g_5 = 0L;
static int64_t g_13 = 0xC345FE46C2E7CCC8LL;
static int32_t g_18[3] = {0x5145CCB9L,0x5145CCB9L,0x5145CCB9L};
static int16_t g_21 = (-1L);
static uint64_t g_22[1][1] = {{0xA14C5530C4E2BF5FLL}};
static int32_t g_26 = 0x4F9A0237L;
static int8_t g_54 = 0x5DL;
static uint32_t g_55 = 1UL;
static int8_t g_58[5][6] = {{0x26L,(-7L),(-5L),(-7L),0x26L,0x26L},{1L,(-7L),(-7L),1L,0x67L,1L},{1L,0x67L,1L,(-7L),(-7L),1L},{0x26L,0x26L,(-7L),(-5L),(-7L),0x26L},{(-7L),0x67L,(-5L),(-5L),0x67L,(-7L)}};
static uint8_t g_80 = 0x85L;
static union U0 g_91[8][3][5] = {{{{5UL},{6UL},{0x3B926A7FL},{4294967295UL},{0UL}},{{4294967295UL},{0UL},{0xB42A304AL},{0xBC47EF13L},{4294967290UL}},{{0x96F3BD68L},{6UL},{0x842841C1L},{4294967295UL},{0x34AC2647L}}},{{{0xE490029EL},{0xDD1E6761L},{4294967295UL},{0xFAEDEB9EL},{0xC8D2EE02L}},{{6UL},{0x34AC2647L},{0xCAC60C0AL},{0x96F3BD68L},{0x96F3BD68L}},{{0xCD7C78B5L},{4294967293UL},{0xCD7C78B5L},{4294967287UL},{1UL}}},{{{4294967290UL},{6UL},{0UL},{4294967295UL},{0x2A1A5AB3L}},{{1UL},{0xB2B6A97FL},{1UL},{0x9EDEBB4DL},{1UL}},{{0x3B926A7FL},{0xCAC60C0AL},{0UL},{0x2A1A5AB3L},{0x50F0C8DFL}}},{{{2UL},{1UL},{0xCD7C78B5L},{0xB42A304AL},{4294967295UL}},{{9UL},{4294967295UL},{0xCAC60C0AL},{0x2FDF6FDFL},{0xCAC60C0AL}},{{4294967287UL},{4294967287UL},{4294967295UL},{1UL},{0x4D21CDEBL}}},{{{4294967295UL},{4294967295UL},{0x842841C1L},{4294967290UL},{4294967293UL}},{{0x110A1B7EL},{0x268D07ABL},{0xB42A304AL},{1UL},{0xEB19FD91L}},{{0x34AC2647L},{4294967295UL},{5UL},{0x842841C1L},{4294967295UL}}},{{{0x983E45B0L},{4294967287UL},{0xE490029EL},{4294967290UL},{1UL}},{{5UL},{4294967295UL},{0xC6DB221BL},{0xC6DB221BL},{4294967295UL}},{{1UL},{1UL},{0x268D07ABL},{0x4C988BF1L},{0xB42A304AL}}},{{{0xB4DCC518L},{0xCAC60C0AL},{9UL},{4294967295UL},{0xC6DB221BL}},{{0x4D21CDEBL},{0xB2B6A97FL},{4294967295UL},{0xE490029EL},{0UL}},{{0xB4DCC518L},{6UL},{0x2FDF6FDFL},{4294967295UL},{4294967293UL}}},{{{1UL},{4294967293UL},{0xC8D2EE02L},{2UL},{0x4C988BF1L}},{{5UL},{0x34AC2647L},{4294967295UL},{0x34AC2647L},{5UL}},{{0x983E45B0L},{0xDD1E6761L},{2UL},{0xEB19FD91L},{4294967287UL}}}};
static int64_t g_93 = (-1L);
static uint8_t g_111 = 0UL;
static uint8_t g_119[7][8][4] = {{{0x9AL,0x55L,0UL,252UL},{0UL,0xA4L,0xBCL,252UL},{0xBCL,252UL,0UL,0x35L},{255UL,251UL,2UL,250UL},{0x35L,0xBCL,5UL,255UL},{0x55L,0x0AL,252UL,248UL},{0xBAL,0xCAL,254UL,0x8EL},{0xFDL,2UL,1UL,0UL}},{{0xA7L,252UL,255UL,0xD5L},{0x0AL,250UL,250UL,0x0AL},{0UL,9UL,254UL,0x91L},{0UL,5UL,0xD5L,0UL},{0xDFL,1UL,251UL,0UL},{0x10L,5UL,0x9AL,0x91L},{0xA4L,9UL,0xF3L,0x0AL},{255UL,250UL,0xDFL,0xD5L}},{{0x0BL,252UL,255UL,0UL},{254UL,2UL,251UL,0x8EL},{0x91L,0xCAL,0xA6L,248UL},{5UL,1UL,0x16L,250UL},{0x3AL,0xA4L,252UL,0xFDL},{0xFDL,248UL,0x55L,254UL},{0x16L,0xBAL,254UL,0xBAL},{0xF9L,255UL,0x0BL,0x8EL}},{{4UL,255UL,0x3AL,1UL},{255UL,0xA6L,7UL,0UL},{255UL,0xC1L,0x3AL,0x91L},{4UL,0UL,0x0BL,7UL},{0xF9L,4UL,254UL,5UL},{0x16L,251UL,0x55L,5UL},{0xFDL,0UL,252UL,255UL},{0x3AL,252UL,0x16L,0x9AL}},{{5UL,0xF3L,255UL,0UL},{0x0AL,0xB6L,0xF3L,248UL},{0xDFL,0xBCL,250UL,0x3AL},{0xA7L,0x9AL,252UL,252UL},{250UL,250UL,0xE9L,0xBCL},{255UL,255UL,0UL,254UL},{0UL,1UL,248UL,0UL},{252UL,1UL,0UL,254UL}},{{1UL,255UL,5UL,0xBCL},{0xB6L,250UL,0xFDL,252UL},{1UL,0x9AL,255UL,0x3AL},{255UL,0xBCL,251UL,248UL},{0UL,0xB6L,0xDFL,0UL},{0xD5L,0xF3L,0xBAL,0x9AL},{255UL,252UL,0x10L,255UL},{254UL,0UL,251UL,5UL}},{{7UL,251UL,0xB6L,5UL},{0xA4L,4UL,0xA4L,7UL},{0UL,0UL,0xBCL,0x91L},{0UL,0xC1L,250UL,0UL},{0UL,0xA6L,250UL,1UL},{0UL,255UL,0xBCL,0x8EL},{0UL,255UL,0xA4L,0xBAL},{0xA4L,0xBAL,0xB6L,254UL}}};
static int32_t *g_128 = (void*)0;
static int32_t **g_127[4] = {&g_128,&g_128,&g_128,&g_128};
static int8_t *g_131 = &g_58[3][0];
static uint8_t *g_139[5][9][3] = {{{(void*)0,(void*)0,&g_80},{(void*)0,&g_80,&g_80},{&g_80,&g_80,&g_80},{(void*)0,&g_80,(void*)0},{(void*)0,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80}},{{&g_80,&g_80,&g_80},{(void*)0,(void*)0,&g_80},{(void*)0,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,(void*)0},{&g_80,&g_80,&g_80},{(void*)0,&g_80,&g_80}},{{&g_80,(void*)0,&g_80},{&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80},{&g_80,(void*)0,(void*)0},{&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,(void*)0},{&g_80,&g_80,&g_80}},{{(void*)0,&g_80,&g_80},{&g_80,(void*)0,&g_80},{&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80},{&g_80,(void*)0,(void*)0},{&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,(void*)0}},{{&g_80,&g_80,&g_80},{(void*)0,&g_80,&g_80},{&g_80,(void*)0,&g_80},{&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80},{&g_80,(void*)0,(void*)0},{&g_80,(void*)0,&g_80},{&g_80,&g_80,&g_80},{&g_80,&g_80,&g_80}}};
static uint8_t **g_138 = &g_139[1][6][2];
static int32_t g_142 = 0xBA9724F8L;
static int32_t g_189 = 3L;
static uint64_t g_192[3] = {1UL,1UL,1UL};
static uint64_t *g_191 = &g_192[1];
static int32_t ** volatile g_194 = &g_128;/* VOLATILE GLOBAL g_194 */
static int16_t g_224 = 0x7306L;
static int32_t * volatile g_228 = &g_189;/* VOLATILE GLOBAL g_228 */
static uint16_t g_263 = 0xEC10L;
static const int64_t g_312[8] = {0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL,0xD26CD37671AC5D8DLL};
static int32_t * volatile g_314[3] = {&g_189,&g_189,&g_189};
static int32_t * volatile g_357[1] = {&g_189};
static int32_t * volatile g_358 = &g_189;/* VOLATILE GLOBAL g_358 */
static volatile union U0 g_378 = {0UL};/* VOLATILE GLOBAL g_378 */
static int64_t *g_384 = (void*)0;
static int64_t **g_383[8] = {&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384,&g_384};
static int64_t *** volatile g_382 = &g_383[7];/* VOLATILE GLOBAL g_382 */
static float g_405 = (-0x1.1p+1);
static union U1 ** volatile g_443 = (void*)0;/* VOLATILE GLOBAL g_443 */
static union U1 g_445 = {0x68B7485DL};
static volatile union U0 *g_467 = (void*)0;
static volatile uint8_t g_471 = 0xDFL;/* VOLATILE GLOBAL g_471 */
static uint16_t *g_498 = (void*)0;
static uint8_t g_513[8][5][6] = {{{251UL,0xB0L,0UL,0xF7L,0x94L,255UL},{255UL,255UL,251UL,0x86L,4UL,0UL},{0x2AL,0x34L,0xB0L,0x90L,0xB0L,0x34L},{0x0FL,0xF6L,0xA4L,0x6CL,0xBDL,255UL},{5UL,0xC0L,1UL,0xB0L,0UL,0x2EL}},{{1UL,0xC0L,0x67L,0x18L,0xBDL,1UL},{8UL,0xF6L,255UL,255UL,0xB0L,0x42L},{0x44L,0x34L,0xA5L,0xB9L,4UL,0x1DL},{0x67L,255UL,0xB9L,1UL,0x94L,0xB0L},{0UL,0xB0L,0x23L,0x67L,0x1DL,0x68L}},{{1UL,0x86L,0x3DL,251UL,0x8DL,0x9BL},{254UL,0xA0L,253UL,0x81L,255UL,0xB9L},{255UL,0x86L,8UL,0xECL,251UL,0x67L},{8UL,0x63L,0xEAL,0xFBL,1UL,1UL},{255UL,0x42L,255UL,8UL,0UL,255UL}},{{0xB9L,0UL,255UL,0xB8L,0UL,0xA4L},{0x6CL,0xD5L,0UL,0x18L,0x18L,0UL},{253UL,253UL,0x42L,0xBDL,255UL,255UL},{255UL,1UL,0x62L,0xFBL,0UL,0x42L},{255UL,255UL,0x62L,254UL,253UL,255UL}},{{1UL,254UL,0x42L,0UL,0x5DL,0UL},{0UL,0x5DL,0UL,255UL,0x17L,0xA4L},{255UL,4UL,255UL,5UL,253UL,255UL},{253UL,5UL,255UL,0x7BL,0x2AL,1UL},{0UL,255UL,0xEAL,8UL,0xA5L,0x67L}},{{0x4FL,251UL,8UL,0x0CL,0x34L,0xB9L},{0x23L,0x2AL,0x9AL,255UL,0x18L,1UL},{255UL,0UL,0x18L,0x62L,0xF7L,255UL},{0x9BL,0UL,3UL,0x8DL,0xD3L,255UL},{255UL,0x42L,0xF6L,254UL,0xB0L,0x67L}},{{0x90L,4UL,0x17L,0UL,1UL,0UL},{0x4CL,1UL,0x4CL,8UL,0UL,1UL},{253UL,0xA5L,255UL,0x94L,0xF7L,0UL},{254UL,0xD5L,0xBDL,0x94L,0x44L,8UL},{253UL,255UL,0x43L,8UL,0UL,255UL}},{{0x4CL,251UL,255UL,0UL,0UL,8UL},{0x90L,0x2EL,0xB9L,254UL,255UL,0x4CL},{255UL,0x81L,0x42L,0x8DL,253UL,0xC0L},{0x9BL,0x5DL,0xBDL,0x62L,0x1DL,0UL},{255UL,253UL,5UL,255UL,4UL,255UL}}};
static int32_t *g_547 = (void*)0;
static int32_t ** volatile g_546 = &g_547;/* VOLATILE GLOBAL g_546 */
static volatile uint8_t * volatile g_613[8][8] = {{&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471},{&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,(void*)0,&g_471},{&g_471,(void*)0,&g_471,&g_471,&g_471,&g_471,&g_471,(void*)0},{&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,(void*)0},{&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471,&g_471},{&g_471,&g_471,&g_471,&g_471,&g_471,(void*)0,&g_471,&g_471},{&g_471,&g_471,&g_471,(void*)0,&g_471,&g_471,&g_471,(void*)0},{(void*)0,&g_471,(void*)0,&g_471,&g_471,(void*)0,&g_471,&g_471}};
static volatile uint8_t * volatile *g_612 = &g_613[2][3];
static volatile uint8_t * volatile * volatile *g_611[7] = {&g_612,&g_612,&g_612,&g_612,&g_612,&g_612,&g_612};
static volatile uint8_t * volatile * volatile ** volatile g_610 = &g_611[1];/* VOLATILE GLOBAL g_610 */
static int32_t * const  volatile *g_623[3] = {&g_547,&g_547,&g_547};
static int32_t * const  volatile * volatile *g_622 = &g_623[1];
static int32_t * const  volatile * volatile * volatile *g_621 = &g_622;
static union U0 g_641 = {0x68471B58L};
static union U0 *g_640 = &g_641;
static const int32_t g_659 = 7L;
static uint64_t g_676 = 1UL;
static union U1 *g_692 = &g_445;
static union U1 **g_691 = &g_692;
static union U1 ***g_690[6] = {&g_691,&g_691,&g_691,&g_691,&g_691,&g_691};
static const uint8_t g_716 = 0x4DL;
static const uint8_t *g_715 = &g_716;
static const uint8_t **g_714 = &g_715;
static const uint8_t ***g_713 = &g_714;
static const uint8_t ****g_712[1] = {&g_713};
static const uint8_t *****g_711 = &g_712[0];
static int64_t g_719 = 6L;
static volatile uint16_t g_727 = 0x8D67L;/* VOLATILE GLOBAL g_727 */
static int64_t *** volatile ** volatile g_788 = (void*)0;/* VOLATILE GLOBAL g_788 */
static int16_t g_863 = 0L;
static int16_t g_864 = 1L;
static int32_t *g_889 = &g_18[1];
static uint8_t ****g_930 = (void*)0;
static volatile uint8_t *g_947 = &g_471;
static const uint16_t g_971 = 1UL;
static uint16_t g_1005 = 7UL;
static int32_t * volatile g_1031 = (void*)0;/* VOLATILE GLOBAL g_1031 */
static int32_t * volatile g_1032 = &g_189;/* VOLATILE GLOBAL g_1032 */
static float g_1036 = (-0x1.9p+1);
static uint32_t g_1038 = 1UL;
static const union U1 g_1055 = {0x4FD46190L};
static uint32_t *g_1100 = &g_445.f0;
static uint32_t **g_1099 = &g_1100;
static float *g_1123 = (void*)0;
static float * const *g_1122 = &g_1123;
static float * const ** volatile g_1121 = &g_1122;/* VOLATILE GLOBAL g_1121 */
static int8_t g_1181 = 0x7BL;
static volatile int64_t g_1288 = (-3L);/* VOLATILE GLOBAL g_1288 */
static int64_t *g_1346 = &g_719;
static int64_t ** const g_1345 = &g_1346;
static int64_t ** const *g_1344 = &g_1345;
static int64_t ** const **g_1343 = &g_1344;
static int64_t ** const ***g_1342[2] = {&g_1343,&g_1343};
static int32_t g_1375 = 0L;
static volatile uint32_t g_1434 = 0xEA3634DFL;/* VOLATILE GLOBAL g_1434 */
static int8_t g_1442 = 9L;
static int16_t g_1530 = (-2L);
static int64_t g_1568 = (-9L);
static uint64_t **g_1603 = &g_191;
static uint64_t *** volatile g_1604 = &g_1603;/* VOLATILE GLOBAL g_1604 */
static float * volatile g_1606 = &g_405;/* VOLATILE GLOBAL g_1606 */
static int64_t g_1625 = 0x1644ED5AE3A331A1LL;
static uint8_t *****g_1654[6][8][1] = {{{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0}},{{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930}},{{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0}},{{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0}},{{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930}},{{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0},{&g_930},{(void*)0},{(void*)0}}};
static union U1 * volatile * volatile ** volatile g_1696 = (void*)0;/* VOLATILE GLOBAL g_1696 */
static union U1 * volatile * volatile ** volatile *g_1695 = &g_1696;
static const int32_t *g_1701 = &g_659;
static int64_t g_1721 = (-3L);
static int32_t g_1766 = (-1L);
static const union U1 *g_1788[4] = {&g_445,&g_445,&g_445,&g_445};
static const int32_t ** volatile g_1809 = (void*)0;/* VOLATILE GLOBAL g_1809 */
static float * const  volatile g_1842 = &g_405;/* VOLATILE GLOBAL g_1842 */
static int32_t ** volatile g_1927 = &g_547;/* VOLATILE GLOBAL g_1927 */
static int32_t ** volatile g_1928 = (void*)0;/* VOLATILE GLOBAL g_1928 */
static int32_t ** volatile g_1930 = &g_547;/* VOLATILE GLOBAL g_1930 */
static volatile int64_t g_1934 = 9L;/* VOLATILE GLOBAL g_1934 */
static volatile int16_t g_1941 = (-3L);/* VOLATILE GLOBAL g_1941 */
static int32_t ** volatile g_1950 = &g_128;/* VOLATILE GLOBAL g_1950 */
static int32_t ** volatile g_1955 = &g_547;/* VOLATILE GLOBAL g_1955 */
static uint16_t **g_1958 = &g_498;
static uint16_t *** volatile g_1957 = &g_1958;/* VOLATILE GLOBAL g_1957 */
static uint8_t * const *g_2010 = &g_139[1][6][2];
static uint8_t * const **g_2009[6][3] = {{&g_2010,&g_2010,&g_2010},{&g_2010,&g_2010,&g_2010},{&g_2010,&g_2010,&g_2010},{(void*)0,&g_2010,&g_2010},{(void*)0,(void*)0,&g_2010},{&g_2010,&g_2010,&g_2010}};
static int32_t * volatile *g_2014 = &g_889;
static int32_t * volatile **g_2013 = &g_2014;
static int32_t * volatile ** volatile *g_2012 = &g_2013;
static volatile uint8_t g_2054 = 2UL;/* VOLATILE GLOBAL g_2054 */
static float * volatile g_2100 = &g_405;/* VOLATILE GLOBAL g_2100 */
static float * volatile g_2105[9][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_405,&g_405,(void*)0,(void*)0,&g_405,&g_405,&g_405,(void*)0,(void*)0,&g_405},{&g_405,&g_405,(void*)0,(void*)0,&g_405,&g_405,&g_405,(void*)0,(void*)0,&g_405},{&g_405,&g_405,(void*)0,(void*)0,&g_405,&g_405,&g_405,(void*)0,(void*)0,&g_405}};
static int32_t * volatile g_2155 = &g_142;/* VOLATILE GLOBAL g_2155 */
static union U1 * const * const g_2183 = (void*)0;
static union U1 * const * const *g_2182 = &g_2183;
static union U1 * const * const **g_2181 = &g_2182;
static union U1 * const * const ***g_2180[7][2][10] = {{{&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0},{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181}},{{(void*)0,(void*)0,&g_2181,&g_2181,(void*)0,(void*)0,(void*)0,&g_2181,&g_2181,&g_2181},{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,&g_2181}},{{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,&g_2181},{(void*)0,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181}},{{&g_2181,&g_2181,&g_2181,&g_2181,(void*)0,(void*)0,(void*)0,&g_2181,&g_2181,&g_2181},{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0,(void*)0,&g_2181,&g_2181,(void*)0}},{{(void*)0,&g_2181,&g_2181,&g_2181,&g_2181,(void*)0,(void*)0,&g_2181,&g_2181,&g_2181},{&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0}},{{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181,&g_2181},{(void*)0,(void*)0,&g_2181,&g_2181,(void*)0,(void*)0,(void*)0,&g_2181,&g_2181,&g_2181}},{{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,&g_2181},{&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,(void*)0,&g_2181,&g_2181,&g_2181,&g_2181}}};
static volatile uint32_t g_2230 = 0xD6210110L;/* VOLATILE GLOBAL g_2230 */
static volatile int16_t g_2316 = 0xEEEBL;/* VOLATILE GLOBAL g_2316 */
static float g_2337 = 0x8.46322Fp+63;
static volatile uint64_t g_2369 = 0xBB778705FF088BA0LL;/* VOLATILE GLOBAL g_2369 */
static int32_t ***g_2446[2][4][2] = {{{&g_127[2],&g_127[2]},{&g_127[2],&g_127[2]},{&g_127[2],&g_127[2]},{&g_127[2],&g_127[2]}},{{&g_127[2],&g_127[2]},{&g_127[2],&g_127[2]},{&g_127[2],&g_127[2]},{&g_127[2],&g_127[2]}}};
static int32_t **** volatile g_2445 = &g_2446[1][3][1];/* VOLATILE GLOBAL g_2445 */
static int32_t *g_2481[6] = {(void*)0,(void*)0,&g_5,(void*)0,&g_26,(void*)0};
static float g_2543 = 0x2.1DE16Ap-57;
static int32_t * volatile g_2548 = &g_189;/* VOLATILE GLOBAL g_2548 */
static int32_t **g_2744 = &g_889;
static int32_t *g_2773 = &g_189;
static int16_t *g_2774[3][10][4] = {{{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864}},{{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864}},{{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864},{&g_863,&g_863,&g_863,&g_864},{&g_864,&g_863,&g_863,&g_864}}};
static const int32_t ** volatile g_2775 = &g_1701;/* VOLATILE GLOBAL g_2775 */
static volatile int32_t g_2816 = 0xDD88601FL;/* VOLATILE GLOBAL g_2816 */
static int32_t g_2851[6] = {2L,2L,2L,2L,2L,2L};
static volatile int32_t g_2863 = 0x343514C9L;/* VOLATILE GLOBAL g_2863 */
static float * const  volatile g_2907[7] = {&g_405,&g_405,&g_405,&g_405,&g_405,&g_405,&g_405};
static int32_t ** volatile g_2932 = &g_547;/* VOLATILE GLOBAL g_2932 */
static float * volatile g_2934 = &g_2337;/* VOLATILE GLOBAL g_2934 */
static uint64_t g_2935 = 1UL;
static int32_t g_2954 = 0xB9319975L;
static union U1 ****g_2982 = &g_690[3];
static int32_t ** volatile g_2988 = (void*)0;/* VOLATILE GLOBAL g_2988 */
static int32_t ** volatile g_2989 = &g_547;/* VOLATILE GLOBAL g_2989 */
static union U0 g_3019 = {4294967295UL};
static int32_t g_3071 = 0x5FC176C8L;
static int32_t ** const *g_3082 = (void*)0;
static int32_t ** const **g_3081 = &g_3082;
static float * volatile g_3160 = &g_1036;/* VOLATILE GLOBAL g_3160 */
static float * volatile g_3162 = &g_2543;/* VOLATILE GLOBAL g_3162 */
static uint64_t g_3177[2][4] = {{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL}};


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static int32_t * func_8(float  p_9, int64_t  p_10, int32_t  p_11, const uint8_t  p_12);
static int32_t  func_14(uint64_t  p_15);
static union U0  func_76(uint8_t  p_77, const uint64_t  p_78);
static union U1  func_120(int32_t ** const  p_121, int32_t ** p_122, int32_t * p_123, int64_t  p_124, int16_t  p_125);
static int32_t * func_129(int8_t * p_130);
static const int32_t * func_145(int32_t  p_146, int32_t * p_147, int16_t * p_148, int16_t * p_149, uint32_t  p_150);
static int64_t  func_151(const int16_t  p_152, int32_t  p_153, float  p_154, int8_t  p_155);
static int16_t  func_164(int16_t * p_165, uint8_t * p_166, uint64_t * p_167, uint8_t  p_168, int32_t  p_169);
static int64_t  func_172(int32_t  p_173);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_2 g_13 g_22 g_26 g_18 g_54 g_58 g_21 g_3 g_80 g_4 g_91 g_91.f0 g_55 g_142 g_2932 g_2934 g_546 g_547 g_2935 g_3162 g_863 g_1927 g_2773 g_189 g_3177 g_715 g_716 g_1343 g_1344 g_1345 g_1346 g_719 g_2337 g_1530 g_2954 g_714
 * writes: g_5 g_22 g_2 g_21 g_13 g_54 g_55 g_58 g_80 g_26 g_4 g_93 g_142 g_547 g_405 g_2337 g_2543 g_863 g_189 g_1530 g_2954
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    int8_t *l_2114 = (void*)0;
    int32_t l_2157[5][7] = {{3L,1L,3L,(-7L),3L,1L,3L},{7L,0x6B695940L,0x6B695940L,7L,8L,0xE647CDD3L,0xE647CDD3L},{(-1L),1L,8L,1L,(-1L),1L,8L},{8L,7L,0x6B695940L,0x6B695940L,7L,8L,0xE647CDD3L},{3L,(-7L),3L,1L,3L,(-7L),3L}};
    uint32_t l_2164 = 18446744073709551615UL;
    int32_t *l_2232 = &g_142;
    int64_t l_2251 = 0xBF95235BB1E24EB7LL;
    int8_t l_2267 = 0x09L;
    int32_t l_2338 = 7L;
    union U1 *l_2339 = (void*)0;
    uint32_t l_2416[4][1] = {{9UL},{0x894EB692L},{9UL},{0x894EB692L}};
    int32_t l_2423 = 9L;
    const uint64_t l_2442 = 0xEC6072FB50480B25LL;
    int32_t ***** const l_2471 = (void*)0;
    const uint8_t **l_2487 = &g_715;
    uint8_t l_2502 = 0x9FL;
    uint8_t ***l_2546 = (void*)0;
    union U0 l_2553 = {2UL};
    uint32_t l_2583 = 5UL;
    int64_t l_2611 = 7L;
    int32_t l_2630 = (-1L);
    uint8_t l_2636 = 1UL;
    uint32_t l_2655 = 4294967289UL;
    uint8_t * const **l_2662[5][1][4] = {{{(void*)0,&g_2010,&g_2010,(void*)0}},{{&g_2010,&g_2010,&g_2010,&g_2010}},{{&g_2010,&g_2010,&g_2010,&g_2010}},{{&g_2010,&g_2010,(void*)0,&g_2010}},{{&g_2010,&g_2010,&g_2010,(void*)0}}};
    int32_t *l_2712 = &g_142;
    uint64_t l_2739 = 0xFFD1B1BAB4E669B9LL;
    const int8_t l_2753 = 2L;
    uint32_t l_2756[2];
    uint16_t l_2802 = 0x746CL;
    int64_t l_2826 = 0x77D214BD5333F827LL;
    int64_t l_2917 = 0x087C31D47CF4BD1DLL;
    uint8_t l_2918 = 0x69L;
    union U0 l_2948 = {0xDD9C06D4L};
    const union U1 *l_3003 = (void*)0;
    uint32_t l_3022 = 1UL;
    uint32_t l_3028 = 0xFD233855L;
    uint16_t l_3031[10][1] = {{3UL},{3UL},{2UL},{7UL},{2UL},{3UL},{3UL},{2UL},{7UL},{2UL}};
    int64_t l_3046 = 1L;
    int16_t l_3064[7] = {0x31E8L,0x31E8L,0x31E8L,0x31E8L,0x31E8L,0x31E8L,0x31E8L};
    int32_t ** const l_3074 = &g_547;
    uint8_t ****l_3089 = &l_2546;
    const int32_t l_3113 = 1L;
    uint32_t l_3142 = 4294967295UL;
    int64_t l_3150[1];
    int8_t **l_3161 = &l_2114;
    int32_t l_3197 = 0x8C94D7E8L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2756[i] = 18446744073709551613UL;
    for (i = 0; i < 1; i++)
        l_3150[i] = (-9L);
    for (g_5 = 0; (g_5 < 21); g_5 = safe_add_func_int32_t_s_s(g_5, 9))
    { /* block id: 3 */
        int32_t *l_98 = (void*)0;
        int32_t **l_97 = &l_98;
        uint8_t l_2115 = 0UL;
        union U1 ** const *l_2179[10][4][1] = {{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}},{{&g_691},{&g_691},{&g_691},{&g_691}}};
        union U1 ** const ** const l_2178 = &l_2179[2][0][0];
        union U1 ** const ** const *l_2177 = &l_2178;
        int32_t l_2207[4][3] = {{(-5L),8L,(-5L)},{1L,0x23B110C9L,1L},{(-5L),8L,(-5L)},{1L,0x23B110C9L,1L}};
        uint8_t ***l_2263 = &g_138;
        uint8_t ****l_2262 = &l_2263;
        int64_t l_2266[1][10] = {{0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL,0xC50B10B9BD427A37LL}};
        int8_t l_2392 = 0x5EL;
        float **l_2415 = &g_1123;
        uint8_t l_2443 = 1UL;
        int16_t *l_2482 = &g_863;
        uint32_t l_2509 = 7UL;
        int8_t *l_2534 = &l_2392;
        uint32_t l_2562 = 1UL;
        int32_t l_2577 = 1L;
        union U0 l_2627 = {1UL};
        uint32_t l_2635 = 1UL;
        uint32_t l_2732 = 4294967293UL;
        uint8_t ****l_2737 = &l_2546;
        int32_t **l_2743 = (void*)0;
        uint8_t l_2754[1];
        int32_t l_2803[9] = {(-1L),1L,(-1L),1L,(-1L),1L,(-1L),1L,(-1L)};
        int64_t l_2840 = 0L;
        uint64_t **l_2852 = (void*)0;
        float l_2902[4] = {(-0x4.6p+1),(-0x4.6p+1),(-0x4.6p+1),(-0x4.6p+1)};
        uint64_t l_2916 = 0x3A74848F2B994A18LL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2754[i] = 0x41L;
        (*l_97) = func_8(g_2[2], g_13, func_14(g_5), g_5);
        for (g_55 = 14; (g_55 > 6); g_55 = safe_sub_func_uint64_t_u_u(g_55, 4))
        { /* block id: 55 */
            int64_t l_2117 = 5L;
            uint16_t l_2158 = 0xEE49L;
            int64_t l_2163 = 0L;
            const uint32_t *l_2171 = (void*)0;
            const uint32_t * const *l_2170 = &l_2171;
            const uint32_t * const **l_2169[6][2] = {{&l_2170,&l_2170},{&l_2170,&l_2170},{&l_2170,&l_2170},{&l_2170,&l_2170},{&l_2170,&l_2170},{&l_2170,&l_2170}};
            uint16_t * const *l_2172 = (void*)0;
            float **l_2202 = &g_1123;
            float ***l_2201[2][3];
            int32_t l_2206 = 0xAF1D62C5L;
            uint8_t l_2228 = 255UL;
            int32_t **l_2259 = &l_98;
            const float l_2282 = 0xB.50BCEFp-35;
            uint8_t *l_2315[3];
            float l_2418 = 0x9.21C0D9p-81;
            union U1 l_2421[7][5][2] = {{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}},{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}},{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}},{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}},{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}},{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}},{{{0x5D856827L},{1UL}},{{0x3C2A6CF9L},{1UL}},{{1UL},{1UL}},{{0x3C2A6CF9L},{1UL}},{{0x5D856827L},{0x5D856827L}}}};
            union U1 ****l_2429 = &g_690[1];
            union U1 *****l_2428 = &l_2429;
            uint16_t *l_2444 = &g_263;
            int64_t * const *l_2456 = &g_384;
            int64_t * const **l_2455[6] = {&l_2456,&l_2456,&l_2456,&l_2456,&l_2456,&l_2456};
            int64_t * const ***l_2454[9] = {&l_2455[0],&l_2455[0],&l_2455[0],&l_2455[0],&l_2455[0],&l_2455[0],&l_2455[0],&l_2455[0],&l_2455[0]};
            int64_t * const ****l_2453 = &l_2454[6];
            const uint8_t **l_2488 = (void*)0;
            int32_t *l_2549[3];
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 3; j++)
                    l_2201[i][j] = &l_2202;
            }
            for (i = 0; i < 3; i++)
                l_2315[i] = &l_2115;
            for (i = 0; i < 3; i++)
                l_2549[i] = &g_142;
        }
    }
    (*l_2712) = 0L;
    if ((*l_2712))
    { /* block id: 1289 */
        int8_t l_2924 = 0x59L;
        return l_2924;
    }
    else
    { /* block id: 1291 */
        int32_t *l_2931 = &g_5;
        uint64_t l_2966 = 0x549A08E412178B52LL;
        union U1 ****l_2983 = &g_690[3];
        int32_t ***l_3055[4];
        const uint16_t l_3091 = 0xD1BCL;
        uint32_t l_3097 = 4294967295UL;
        uint32_t l_3115 = 0x352CAD3CL;
        uint32_t l_3141[4][7] = {{0x3117553CL,0x7E3B9276L,18446744073709551612UL,0x7E3B9276L,0x3117553CL,0UL,0UL},{0xF55849AEL,0x8109FFB7L,5UL,0x8109FFB7L,0xF55849AEL,18446744073709551615UL,18446744073709551615UL},{0x3117553CL,0x7E3B9276L,18446744073709551612UL,0x7E3B9276L,0x3117553CL,0UL,0UL},{0xF55849AEL,0x8109FFB7L,5UL,0x8109FFB7L,0xF55849AEL,18446744073709551615UL,18446744073709551615UL}};
        int8_t *l_3165 = &g_58[3][4];
        int i, j;
        for (i = 0; i < 4; i++)
            l_3055[i] = &g_127[0];
        for (l_2655 = (-12); (l_2655 <= 60); l_2655 = safe_add_func_int32_t_s_s(l_2655, 9))
        { /* block id: 1294 */
            float l_2941[6][2][3] = {{{0xB.CC46D5p+73,0xB.9BF2DBp-62,0xB.CC46D5p+73},{(-0x10.0p+1),0x7.38712Ap-14,0x7.38712Ap-14}},{{0xB.E16B1Ap-60,0xB.9BF2DBp-62,0x5.2D99EEp+55},{0xF.176F09p-38,(-0x10.0p+1),0x7.38712Ap-14}},{{0xB.CC46D5p+73,0x1.0p+1,0xB.CC46D5p+73},{0xF.176F09p-38,0x7.38712Ap-14,0x8.CE87FFp-40}},{{0xB.E16B1Ap-60,0x1.0p+1,0x5.2D99EEp+55},{(-0x10.0p+1),(-0x10.0p+1),0x8.CE87FFp-40}},{{0xB.CC46D5p+73,0xB.9BF2DBp-62,0xB.CC46D5p+73},{(-0x10.0p+1),0x7.38712Ap-14,0x7.38712Ap-14}},{{0xB.E16B1Ap-60,0xB.9BF2DBp-62,0x5.2D99EEp+55},{0xF.176F09p-38,(-0x10.0p+1),0x7.38712Ap-14}}};
            const int32_t l_2944 = 2L;
            float l_2961 = 0x8.E095ABp+12;
            int32_t l_2962[9][4][5] = {{{0x73A6DDDEL,(-4L),(-1L),0x325DE83BL,7L},{0x101012B7L,0x493A6F80L,0xFFA45CE4L,5L,0L},{7L,(-5L),(-1L),0x73A6DDDEL,5L},{(-5L),0x25F166D2L,0x126F55E0L,(-1L),(-1L)}},{{0L,0x490EE047L,0L,0xE742855CL,0xEF38B9A0L},{(-1L),0x36AD5D72L,0x101012B7L,(-3L),0x5E18E46DL},{0xD7C13ACCL,7L,(-1L),0xD7C13ACCL,0x73A6DDDEL},{0xFFA45CE4L,0x493A6F80L,0x101012B7L,0x5E18E46DL,0x25F166D2L}},{{0xA6F8D14EL,(-1L),0L,(-4L),0xB60F0EACL},{0x493A6F80L,(-3L),0x126F55E0L,0x03CC710CL,0xED679D99L},{0x7A9DDDC0L,0x325DE83BL,(-1L),(-1L),0x325DE83BL},{(-1L),0x101012B7L,0xFFA45CE4L,(-3L),(-6L)}},{{(-4L),0xD7C13ACCL,(-1L),0x639BF657L,0xA6F8D14EL},{(-6L),0xED679D99L,0L,0xFED2F317L,0x25F166D2L},{(-4L),5L,0x7A9DDDC0L,0x73A6DDDEL,0L},{(-1L),0L,0xE9417820L,(-1L),0x03CC710CL}},{{0x7A9DDDC0L,1L,(-1L),1L,0xEF38B9A0L},{0x493A6F80L,(-6L),(-6L),0x493A6F80L,0xFED2F317L},{0xA6F8D14EL,0xD7C13ACCL,0xBB0839BCL,0xEF38B9A0L,1L},{0xFFA45CE4L,(-5L),0xFED2F317L,0x101012B7L,0L}},{{0xD7C13ACCL,1L,0x7A9DDDC0L,0xEF38B9A0L,(-5L)},{(-1L),(-3L),0x022C0018L,0x493A6F80L,(-3L)},{0L,(-4L),0xB60F0EACL,1L,7L},{(-5L),0L,0x126F55E0L,0x101012B7L,0x126F55E0L}},{{0xE742855CL,0xE742855CL,7L,1L,0xB60F0EACL},{0xA56F8840L,0xFFA45CE4L,0xE9417820L,3L,0xCD077BD4L},{1L,0xE00236CEL,0x91EC2D87L,(-1L),1L},{0xFED2F317L,0xFFA45CE4L,0L,0L,0x36AD5D72L}},{{(-1L),0xE742855CL,0x64229B4DL,(-1L),1L},{0xA9DB8CBAL,0xCEB85D5EL,0xA56F8840L,0x36AD5D72L,0x940D12F2L},{0x7A9DDDC0L,0x5935B32DL,0x325DE83BL,0x5935B32DL,0x7A9DDDC0L},{0xCEB85D5EL,0L,0xE9417820L,0L,5L}},{{0L,0xA7B005FFL,(-1L),(-5L),0xA7B005FFL},{0xCD077BD4L,0xA388F85BL,(-1L),0L,5L},{0xB880A357L,(-5L),7L,0xBB0839BCL,0x7A9DDDC0L},{5L,0x7E3CBEFCL,0xCEB85D5EL,0xCD077BD4L,0x940D12F2L}}};
            int64_t ** const *l_2986 = (void*)0;
            int32_t ***l_3037 = &g_2744;
            int32_t l_3087 = 0xE14311D3L;
            int8_t l_3090 = (-1L);
            const uint32_t *l_3117 = (void*)0;
            uint8_t l_3151 = 0UL;
            float *l_3159 = (void*)0;
            int i, j, k;
            for (g_54 = 0; (g_54 != (-24)); g_54 = safe_sub_func_int32_t_s_s(g_54, 6))
            { /* block id: 1297 */
                for (g_93 = (-3); (g_93 <= 9); ++g_93)
                { /* block id: 1300 */
                    float *l_2933 = &g_405;
                    (*g_2932) = l_2931;
                    (*g_2934) = ((*l_2933) = (*l_2931));
                    if ((**g_546))
                        continue;
                }
                return g_2935;
            }
        }
        (*g_3162) = ((l_3161 = &g_131) != (void*)0);
        for (g_863 = 0; (g_863 == 29); ++g_863)
        { /* block id: 1398 */
            uint16_t l_3176 = 0x4202L;
            int32_t l_3187 = 0x60C40DE5L;
            for (l_2802 = 0; (l_2802 <= 1); l_2802 += 1)
            { /* block id: 1401 */
                int32_t l_3188 = 0x3EE08D6BL;
                if ((0x91248AEEL >= 0xDA5F1EEFL))
                { /* block id: 1402 */
                    (*l_3074) = func_129(l_3165);
                    if ((*g_2773))
                        continue;
                }
                else
                { /* block id: 1405 */
                    uint64_t l_3166[7] = {18446744073709551610UL,18446744073709551610UL,0xAC700531D9F79980LL,18446744073709551610UL,18446744073709551610UL,0xAC700531D9F79980LL,18446744073709551610UL};
                    int32_t l_3171 = 1L;
                    union U1 l_3182 = {4294967290UL};
                    uint32_t l_3186 = 8UL;
                    int i;
                    for (g_55 = 0; (g_55 <= 1); g_55 += 1)
                    { /* block id: 1408 */
                        int8_t l_3185[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_3185[i] = (-4L);
                        (*g_2773) = l_3166[6];
                        (*l_2931) |= ((safe_div_func_float_f_f((safe_sub_func_float_f_f((l_3171 = (*l_2232)), (safe_div_func_float_f_f((safe_sub_func_float_f_f(l_3176, ((g_3177[1][1] | ((l_3187 = ((safe_lshift_func_int8_t_s_s((safe_add_func_uint32_t_u_u(((l_3182 , (safe_mod_func_uint8_t_u_u((*g_715), 0x1EL))) | (((((****g_1343) < 0x0919C454F0BC8249LL) | 9UL) >= 0x1BL) , (*l_2232))), l_3185[5])), l_3176)) > l_3186)) && 1UL)) , (*g_2934)))), l_3182.f0)))), l_3176)) , l_3188);
                        if (l_3176)
                            continue;
                    }
                }
            }
            for (g_1530 = 27; (g_1530 == (-27)); g_1530 = safe_sub_func_int32_t_s_s(g_1530, 3))
            { /* block id: 1419 */
                return l_3176;
            }
            for (g_2954 = 2; (g_2954 >= 0); g_2954 -= 1)
            { /* block id: 1424 */
                int64_t l_3191 = 1L;
                if (l_3191)
                    break;
                if (l_3191)
                    continue;
            }
            return (**g_714);
        }
        for (g_13 = 0; (g_13 == (-7)); g_13 = safe_sub_func_uint8_t_u_u(g_13, 8))
        { /* block id: 1432 */
            uint64_t l_3194 = 0x12E3C8CBD0E6397ALL;
            ++l_3194;
        }
    }
    return l_3197;
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_13 g_22 g_26 g_18 g_54 g_58 g_21 g_3 g_80 g_5 g_4 g_91 g_91.f0 g_55
 * writes: g_2 g_21 g_22 g_13 g_54 g_55 g_58 g_80 g_26 g_4 g_93
 */
static int32_t * func_8(float  p_9, int64_t  p_10, int32_t  p_11, const uint8_t  p_12)
{ /* block id: 7 */
    int32_t *l_25[10][7] = {{&g_26,&g_26,&g_26,&g_26,&g_26,&g_26,&g_26},{&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5},{&g_26,&g_26,&g_26,&g_26,&g_26,&g_26,&g_26},{&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5},{&g_26,&g_26,&g_26,&g_26,&g_26,&g_26,&g_26},{&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5},{&g_26,&g_26,&g_26,&g_26,&g_26,&g_26,&g_26},{&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5},{&g_26,&g_26,&g_26,&g_26,&g_26,&g_26,&g_26},{&g_5,&g_5,&g_5,&g_5,&g_5,&g_5,&g_5}};
    int16_t *l_28 = &g_21;
    uint32_t l_29 = 0x4E2095E0L;
    int32_t l_52 = 0xAF09D89CL;
    int i, j;
    g_2[3] |= p_12;
    l_29 |= (+(((*l_28) = p_12) & (g_13 , func_14(p_11))));
    for (g_13 = 25; (g_13 < 20); g_13 = safe_sub_func_int64_t_s_s(g_13, 5))
    { /* block id: 13 */
        int16_t *l_42 = (void*)0;
        uint32_t l_61 = 2UL;
        int32_t l_64 = (-1L);
        uint64_t *l_95 = &g_22[0][0];
        for (p_10 = 0; (p_10 > (-16)); p_10 = safe_sub_func_int16_t_s_s(p_10, 5))
        { /* block id: 16 */
            uint64_t *l_47 = &g_22[0][0];
            int8_t *l_53 = &g_54;
            int8_t *l_56 = (void*)0;
            int8_t *l_57 = &g_58[3][4];
            int32_t l_62 = 0x1B934971L;
            int32_t l_63 = 0xBC5DE30EL;
            l_64 |= ((safe_mul_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(func_14((safe_add_func_int64_t_s_s((safe_sub_func_uint16_t_u_u(((l_62 = ((((l_42 == &g_21) && ((safe_div_func_int64_t_s_s(((((*l_47) = (safe_rshift_func_int16_t_s_s(p_12, g_26))) || 0xAFF65CE17C927DB4LL) , (safe_mul_func_int8_t_s_s(((*l_57) |= (g_55 = (6L <= (((*l_53) |= (l_52 &= g_18[0])) == p_11)))), ((safe_rshift_func_int8_t_s_u(l_61, 1)) != l_61)))), g_2[3])) != 9L)) , g_26) && 0L)) >= 0x8F9EL), 3L)), 18446744073709551615UL))), l_63)), g_21)) || p_11);
            for (g_54 = 0; (g_54 != (-6)); g_54 = safe_sub_func_uint64_t_u_u(g_54, 3))
            { /* block id: 26 */
                uint64_t *l_69 = &g_22[0][0];
                uint8_t *l_79 = &g_80;
                int64_t *l_92 = &g_93;
                int32_t l_94 = 0xD19BEEC0L;
                int32_t l_96 = 0L;
                l_96 &= (((((((safe_lshift_func_uint16_t_u_u(g_3, 12)) , ((*l_79) = ((l_69 == (((safe_mod_func_uint64_t_u_u((safe_div_func_uint64_t_u_u(((void*)0 == &l_64), ((*l_92) = (safe_lshift_func_int16_t_s_u((func_76(((*l_79) ^= func_14((((*l_28) = 0x6CE9L) , 0xFEDEA8F4439D3FD5LL))), p_11) , l_61), g_91[4][0][3].f0))))), g_58[2][5])) > l_94) , l_95)) , 0x37L))) > l_94) | 4294967286UL) ^ g_55) , g_13) <= p_12);
                for (l_52 = 4; (l_52 >= 0); l_52 -= 1)
                { /* block id: 39 */
                    for (l_61 = 0; (l_61 <= 6); l_61 += 1)
                    { /* block id: 42 */
                        int i;
                        g_2[l_61] = g_2[l_52];
                        if (g_2[l_61])
                            continue;
                        g_26 |= g_2[(l_52 + 2)];
                    }
                }
            }
        }
    }
    return &g_5;
}


/* ------------------------------------------ */
/* 
 * reads : g_22
 * writes: g_22
 */
static int32_t  func_14(uint64_t  p_15)
{ /* block id: 4 */
    int32_t *l_16 = (void*)0;
    int32_t l_17 = (-4L);
    int32_t *l_19 = &l_17;
    int32_t *l_20[6] = {&g_5,&g_5,&g_5,&g_5,&g_5,&g_5};
    int i;
    --g_22[0][0];
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_4 g_91
 * writes: g_26 g_4
 */
static union U0  func_76(uint8_t  p_77, const uint64_t  p_78)
{ /* block id: 29 */
    int32_t *l_81[6];
    int16_t l_82 = 0x0045L;
    int64_t l_83 = 0x227AF4644DEA1A09LL;
    int32_t l_84 = 1L;
    int32_t l_85[1];
    int8_t l_86 = 0x86L;
    int64_t l_87 = 0x766C6174CE2EAA37LL;
    uint32_t l_88 = 0x5382869BL;
    int i;
    for (i = 0; i < 6; i++)
        l_81[i] = &g_26;
    for (i = 0; i < 1; i++)
        l_85[i] = 2L;
    l_88++;
    g_26 = g_5;
    g_4 |= 0x350F044EL;
    return g_91[4][0][3];
}


/* ------------------------------------------ */
/* 
 * reads : g_128 g_711 g_712 g_713 g_714 g_715 g_716 g_546 g_547 g_1930 g_131 g_58 g_1100 g_445.f0 g_1604 g_1603 g_191 g_192 g_1343 g_1344 g_1345 g_1346 g_719 g_1442 g_2155
 * writes: g_128 g_547 g_1005 g_58 g_1442 g_142
 */
static union U1  func_120(int32_t ** const  p_121, int32_t ** p_122, int32_t * p_123, int64_t  p_124, int16_t  p_125)
{ /* block id: 957 */
    union U0 *l_2118 = &g_641;
    int32_t l_2126 = 0x745333E9L;
    uint16_t *l_2138 = &g_1005;
    uint64_t l_2150 = 0UL;
    int32_t l_2151 = 1L;
    int32_t l_2152 = (-6L);
    int8_t *l_2153 = &g_1442;
    int32_t l_2154 = 0x64B3A102L;
    union U1 l_2156[2] = {{0x4C2514F2L},{0x4C2514F2L}};
    int i;
    (*p_122) = (*p_122);
    (*p_122) = (*p_122);
    if (((void*)0 != l_2118))
    { /* block id: 960 */
        uint16_t l_2119 = 0xA1E2L;
        l_2119 |= ((p_124 != (*****g_711)) <= p_124);
    }
    else
    { /* block id: 962 */
        int32_t *l_2120 = &g_189;
        int32_t *l_2121 = &g_189;
        int32_t *l_2122 = (void*)0;
        int32_t *l_2123 = &g_189;
        int32_t *l_2124 = &g_26;
        int32_t *l_2125[9][9][1] = {{{&g_142},{&g_142},{&g_5},{&g_142},{&g_142},{&g_142},{&g_5},{&g_142},{&g_142}},{{&g_189},{(void*)0},{(void*)0},{&g_189},{&g_142},{&g_142},{&g_5},{&g_142},{&g_142}},{{&g_142},{&g_5},{&g_142},{&g_142},{&g_189},{(void*)0},{(void*)0},{&g_189},{&g_142}},{{&g_142},{&g_5},{&g_142},{&g_142},{&g_142},{&g_5},{&g_142},{&g_142},{&g_189}},{{(void*)0},{(void*)0},{&g_189},{&g_142},{&g_142},{&g_5},{&g_142},{&g_5},{&g_26}},{{&g_5},{&g_142},{(void*)0},{&g_142},{&g_142},{&g_142},{&g_142},{(void*)0},{&g_142}},{{&g_5},{&g_26},{&g_5},{&g_26},{&g_5},{&g_142},{(void*)0},{&g_142},{&g_142}},{{&g_142},{&g_142},{(void*)0},{&g_142},{&g_5},{&g_26},{&g_5},{&g_26},{&g_5}},{{&g_142},{(void*)0},{&g_142},{&g_142},{&g_142},{&g_142},{(void*)0},{&g_142},{&g_5}}};
        uint64_t l_2127 = 0xA1C7A6565F7D7EA1LL;
        int32_t **l_2130[2];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2130[i] = &l_2120;
        l_2127++;
        (*g_1930) = ((*p_122) = (*g_546));
        (*p_122) = ((*g_131) , (void*)0);
    }
    (*g_2155) = ((safe_mod_func_uint64_t_u_u(l_2126, (l_2154 &= ((((safe_add_func_uint64_t_u_u(((safe_div_func_int8_t_s_s(((*l_2153) |= ((*g_131) = ((~((*l_2138) = l_2126)) <= ((!((0xAF6104CFL != (l_2152 = (l_2151 = (((p_124 , (l_2150 = (((safe_mul_func_int16_t_s_s((((safe_mod_func_uint8_t_u_u((((4294967289UL & ((*g_1100) > (l_2126 | (safe_rshift_func_int8_t_s_u(p_125, ((safe_sub_func_int64_t_s_s(6L, (***g_1604))) , 255UL)))))) , 1UL) & l_2126), 4L)) != l_2126) | 0x32A02CBDL), 65535UL)) >= l_2126) <= (*g_131)))) != 5L) <= (****g_1343))))) , (*g_1346))) ^ 0L)))), (*g_715))) >= (*g_191)), l_2126)) , l_2151) >= (***g_1344)) != 0x72L)))) & p_124);
    return l_2156[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_26 g_93 g_1927 g_547
 * writes: g_26 g_93
 */
static int32_t * func_129(int8_t * p_130)
{ /* block id: 63 */
    uint8_t l_136 = 249UL;
    uint8_t **l_140[9][10] = {{(void*)0,&g_139[4][1][1],(void*)0,&g_139[3][6][2],&g_139[1][6][2],&g_139[1][5][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[4][6][0],&g_139[1][6][2]},{(void*)0,&g_139[1][6][2],&g_139[3][6][2],&g_139[4][1][1],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][5][0],&g_139[1][6][2],&g_139[1][6][2],&g_139[4][1][1]},{&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2],(void*)0,&g_139[4][5][2],&g_139[3][6][2],&g_139[1][6][2],&g_139[3][6][2],&g_139[3][2][2],&g_139[1][6][2]},{&g_139[1][6][2],&g_139[4][5][2],&g_139[1][5][0],(void*)0,&g_139[1][6][2],&g_139[0][6][0],&g_139[4][2][1],&g_139[3][6][2],&g_139[3][6][2],&g_139[4][2][1]},{&g_139[4][6][0],&g_139[3][2][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[3][2][2],&g_139[4][6][0],&g_139[3][6][2],&g_139[1][6][2],&g_139[2][0][0],&g_139[1][5][0]},{&g_139[1][6][2],&g_139[1][6][2],&g_139[3][6][2],&g_139[1][6][2],&g_139[0][4][2],&g_139[3][2][2],&g_139[0][6][0],&g_139[1][6][2],&g_139[1][5][2],(void*)0},{&g_139[1][6][2],&g_139[4][2][1],(void*)0,&g_139[4][6][0],&g_139[4][1][1],&g_139[4][6][0],(void*)0,&g_139[4][2][1],&g_139[1][6][2],&g_139[2][0][0]},{&g_139[4][6][0],(void*)0,&g_139[4][2][1],&g_139[1][6][2],&g_139[2][0][0],&g_139[0][6][0],&g_139[4][5][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2]},{&g_139[1][6][2],&g_139[3][6][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[3][6][2],&g_139[3][6][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2]}};
    int32_t *l_141 = &g_142;
    int32_t l_221[10] = {(-1L),(-8L),0x3E332AA2L,(-8L),(-1L),(-1L),(-8L),0x3E332AA2L,(-8L),(-1L)};
    int16_t *l_962 = &g_21;
    int32_t l_1814 = 6L;
    int32_t l_1817 = 0x3236A0C2L;
    int32_t l_1821 = 0x366DA809L;
    int32_t l_1825 = 0x77EA24C2L;
    int32_t l_1833 = 1L;
    uint8_t ***l_1837[10][6] = {{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{&g_138,&g_138,&g_138,&g_138,&l_140[2][6],&g_138},{(void*)0,&g_138,(void*)0,&g_138,&g_138,&g_138}};
    int32_t l_1935 = 0L;
    int32_t l_1936 = 3L;
    int32_t l_1937 = 0L;
    int32_t l_1938 = 1L;
    int32_t l_1939 = 0xC96AE1E7L;
    int32_t l_1940 = 6L;
    int32_t l_1942 = 1L;
    int32_t l_1943 = 0xBCB20436L;
    int32_t l_1944 = 0L;
    int32_t l_1945 = (-1L);
    int32_t l_1946 = 0x7FB6CE9EL;
    int32_t *l_1951 = (void*)0;
    uint8_t l_2077[2];
    union U0 **l_2083 = &g_640;
    const int8_t *l_2096[10][2][10] = {{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54}}};
    uint32_t ***l_2103 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2077[i] = 248UL;
    for (g_26 = 10; (g_26 == 15); ++g_26)
    { /* block id: 66 */
        float l_143[4];
        union U1 l_171[1] = {{0x185A720DL}};
        int32_t l_190 = (-1L);
        int32_t *l_961 = &g_142;
        int16_t *l_963 = (void*)0;
        int32_t l_1815[10][2][6] = {{{(-1L),(-8L),0x2F0871F1L,0x921673CDL,(-5L),0x71CE14E8L},{0L,0x988D5E8FL,(-7L),0L,(-1L),(-1L)}},{{0L,1L,0xA8328F47L,4L,0x730E52E4L,4L},{0xABC0867CL,0x921673CDL,0xABC0867CL,(-10L),0L,1L}},{{(-7L),0x730E52E4L,0x921673CDL,0L,0L,0xA593D53AL},{0x774C0E18L,(-1L),4L,0L,1L,(-10L)}},{{(-7L),0xA88AE434L,1L,(-10L),(-4L),0x774C0E18L},{0xABC0867CL,1L,(-1L),4L,0x57BFA924L,(-1L)}},{{0L,0xA8328F47L,(-5L),0L,0xED9F3181L,(-1L)},{0L,2L,1L,0x921673CDL,0x921673CDL,1L}},{{(-1L),(-1L),0L,1L,(-1L),0xFA1C61DCL},{5L,0L,0x71CE14E8L,(-4L),(-1L),0L}},{{(-8L),5L,0x71CE14E8L,0xA593D53AL,(-1L),0xFA1C61DCL},{0x2F0871F1L,0xA593D53AL,0L,0x730E52E4L,0x71CE14E8L,1L}},{{0x730E52E4L,0x71CE14E8L,1L,0xFA1C61DCL,8L,(-1L)},{0x747B8318L,8L,(-5L),0x00DC9CD2L,1L,(-1L)}},{{1L,0x774C0E18L,(-1L),0x747B8318L,(-1L),0x774C0E18L},{0L,0x00DC9CD2L,1L,(-7L),0x754A6C07L,(-10L)}},{{8L,1L,4L,0xA88AE434L,0x176B2F3CL,0xA593D53AL},{(-1L),1L,0x921673CDL,0x71CE14E8L,0x754A6C07L,1L}}};
        int8_t l_1829[5][4] = {{0xBAL,1L,0xBAL,0x31L},{0L,9L,0x31L,0x31L},{1L,1L,0x01L,9L},{9L,0L,0x01L,0L},{1L,0xBAL,0x31L,0x01L}};
        int64_t l_1832 = (-1L);
        int32_t l_1846 = (-3L);
        int16_t l_1892[1];
        union U1 ****l_1910[6];
        uint64_t ***l_1914 = &g_1603;
        float l_2033 = 0xA.D51AB6p+73;
        uint32_t l_2040 = 18446744073709551606UL;
        int32_t *l_2042 = &l_1935;
        float l_2099[9] = {0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61,0x7.FFD19Bp+61};
        int32_t l_2101[10];
        uint32_t *** const l_2102 = &g_1099;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_143[i] = 0x0.6F47B0p-97;
        for (i = 0; i < 1; i++)
            l_1892[i] = 0L;
        for (i = 0; i < 6; i++)
            l_1910[i] = (void*)0;
        for (i = 0; i < 10; i++)
            l_2101[i] = 0xF49ECF96L;
        for (g_93 = 0; (g_93 > (-3)); g_93--)
        { /* block id: 69 */
            int64_t l_144 = 4L;
            int16_t *l_170 = &g_21;
            int32_t *l_1811[6][8][5] = {{{&l_190,(void*)0,(void*)0,(void*)0,(void*)0},{&l_190,(void*)0,&g_189,&g_189,&g_5},{&g_142,&l_190,&l_190,&g_5,&g_189},{&l_190,(void*)0,&l_190,(void*)0,&g_142},{(void*)0,&g_189,&g_189,(void*)0,&l_190},{&l_190,&g_26,&l_190,(void*)0,&g_26},{&g_5,&g_142,&g_189,&g_142,&g_5},{&g_5,&g_26,&g_142,&g_142,&g_5}},{{&g_5,(void*)0,&g_189,&g_189,(void*)0},{&l_190,&g_189,&g_26,&g_26,&g_5},{(void*)0,&g_189,&l_190,(void*)0,&g_5},{&g_5,(void*)0,(void*)0,(void*)0,&g_26},{&g_142,&g_5,(void*)0,&g_142,&l_190},{&g_189,(void*)0,(void*)0,(void*)0,&g_142},{&g_142,(void*)0,&g_5,&g_189,&g_142},{&g_142,(void*)0,&g_189,&l_190,&g_5}},{{&l_190,(void*)0,&g_142,(void*)0,&g_189},{(void*)0,&g_5,&g_142,&g_26,(void*)0},{&g_26,(void*)0,(void*)0,&g_26,&g_189},{&g_142,&g_189,(void*)0,&g_26,&g_26},{&l_190,&g_189,&l_190,&g_5,&l_190},{(void*)0,(void*)0,&l_190,&g_26,(void*)0},{&l_190,&g_26,&l_190,&g_26,&l_190},{&g_26,&g_142,(void*)0,&g_26,&l_190}},{{&g_26,&g_26,&g_26,(void*)0,&g_5},{&g_189,&g_5,(void*)0,&l_190,&g_142},{(void*)0,&l_190,&g_189,&g_189,(void*)0},{(void*)0,&g_189,&g_142,(void*)0,&g_26},{&g_189,&l_190,(void*)0,&g_142,&l_190},{&g_26,(void*)0,(void*)0,(void*)0,(void*)0},{&g_26,&l_190,&g_26,(void*)0,(void*)0},{&l_190,&l_190,&g_5,&g_26,&g_142}},{{(void*)0,&g_5,&g_189,&g_189,(void*)0},{&l_190,(void*)0,&g_5,&g_142,&g_142},{&g_142,&g_189,&g_26,&g_142,(void*)0},{&g_26,&l_190,(void*)0,(void*)0,(void*)0},{(void*)0,&g_189,(void*)0,&l_190,&l_190},{&l_190,&l_190,&g_142,(void*)0,&g_26},{&g_142,(void*)0,&g_189,&g_142,&g_26},{&g_142,(void*)0,(void*)0,&g_142,&l_190}},{{&g_189,&g_142,&g_26,&l_190,(void*)0},{&g_142,&g_5,(void*)0,&g_5,(void*)0},{&g_5,&g_5,&l_190,&g_5,&g_142},{&g_189,(void*)0,&l_190,&g_142,&l_190},{&l_190,(void*)0,&g_26,&g_5,(void*)0},{&l_190,(void*)0,(void*)0,&g_142,&g_26},{(void*)0,&g_189,(void*)0,&g_189,(void*)0},{&g_142,&g_26,&g_142,&g_189,&g_5}}};
            uint64_t l_1834 = 18446744073709551615UL;
            uint8_t ***l_1843[5][3] = {{&g_138,&l_140[2][6],&l_140[2][6]},{&l_140[4][4],&l_140[2][8],&l_140[2][8]},{&g_138,&l_140[2][6],&l_140[2][6]},{&l_140[4][4],&l_140[2][8],&l_140[2][8]},{&g_138,&l_140[2][6],&l_140[2][6]}};
            union U1 *l_1893 = &l_171[0];
            int32_t **l_1898 = &g_889;
            int32_t ***l_1897[2][10][6] = {{{&l_1898,(void*)0,&l_1898,&l_1898,&l_1898,(void*)0},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,(void*)0,&l_1898,(void*)0,&l_1898,&l_1898},{&l_1898,&l_1898,(void*)0,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,(void*)0,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,(void*)0,&l_1898,&l_1898},{(void*)0,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,(void*)0}},{{(void*)0,&l_1898,&l_1898,(void*)0,&l_1898,&l_1898},{&l_1898,(void*)0,&l_1898,&l_1898,&l_1898,(void*)0},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,(void*)0,&l_1898,(void*)0,&l_1898,&l_1898},{&l_1898,&l_1898,(void*)0,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,(void*)0,&l_1898,&l_1898,&l_1898},{&l_1898,&l_1898,&l_1898,(void*)0,&l_1898,&l_1898},{(void*)0,&l_1898,&l_1898,&l_1898,&l_1898,&l_1898}}};
            int32_t *** const *l_1896 = &l_1897[1][8][4];
            uint8_t l_1947 = 0xCAL;
            int32_t l_1980 = 0xBCB90D94L;
            uint16_t l_1982 = 65532UL;
            uint32_t l_2068 = 0UL;
            int64_t ***l_2073 = (void*)0;
            int64_t ****l_2072 = &l_2073;
            union U0 **l_2082 = &g_640;
            uint8_t *****l_2088 = &g_930;
            int32_t l_2097 = 0x18394BC9L;
            int i, j, k;
        }
    }
    return (*g_1927);
}


/* ------------------------------------------ */
/* 
 * reads : g_263 g_546 g_54 g_18 g_659 g_192 g_21 g_191 g_1099 g_1100 g_445.f0 g_715 g_716 g_714 g_131 g_58 g_947 g_471 g_142 g_1288 g_713 g_691 g_641.f0 g_1181 g_91 g_224 g_228 g_864 g_127 g_513 g_1375 g_1343 g_1344 g_1345 g_1346 g_128 g_692 g_445 g_80 g_1434 g_1032 g_189 g_194 g_91.f0 g_863 g_1005 g_1530 g_676 g_1604 g_357 g_1606 g_358 g_1603 g_719 g_1695 g_1701 g_1721 g_4 g_610 g_611
 * writes: g_263 g_547 g_54 g_445.f0 g_692 g_864 g_224 g_1342 g_189 g_58 g_128 g_405 g_719 g_192 g_863 g_1005 g_1036 g_1099 g_131 g_676 g_1603 g_13 g_357 g_640 g_1654 g_1530 g_498 g_1721 g_1442 g_1181 g_1100 g_1038 g_712 g_1788 g_445
 */
static const int32_t * func_145(int32_t  p_146, int32_t * p_147, int16_t * p_148, int16_t * p_149, uint32_t  p_150)
{ /* block id: 429 */
    uint16_t *l_967 = &g_263;
    const int8_t *l_976 = &g_58[3][4];
    int32_t l_1003 = 0x029B679AL;
    int32_t ***l_1043 = &g_127[2];
    int32_t ****l_1042[5][3] = {{&l_1043,(void*)0,&l_1043},{(void*)0,(void*)0,&l_1043},{(void*)0,&l_1043,&l_1043},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
    const union U1 *l_1054[10][7] = {{(void*)0,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055},{&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,(void*)0,(void*)0},{(void*)0,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055},{(void*)0,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055},{&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,(void*)0,&g_1055},{&g_1055,&g_1055,(void*)0,&g_1055,(void*)0,&g_1055,&g_1055},{&g_1055,&g_1055,&g_1055,&g_1055,(void*)0,(void*)0,(void*)0},{&g_1055,(void*)0,(void*)0,&g_1055,&g_1055,&g_1055,&g_1055},{&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055,&g_1055},{&g_1055,&g_1055,(void*)0,&g_1055,(void*)0,&g_1055,&g_1055}};
    int64_t ***l_1065 = &g_383[1];
    int64_t **** const l_1064[2][10] = {{&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065},{&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065,&l_1065}};
    union U0 *l_1067 = &g_91[0][1][2];
    uint32_t l_1092 = 0xE0F1A258L;
    const int32_t *l_1098 = &g_659;
    uint8_t ** const *l_1255 = &g_138;
    uint8_t ** const **l_1254 = &l_1255;
    uint8_t ** const ***l_1253 = &l_1254;
    uint64_t l_1267 = 0xCB43DDFE54D23786LL;
    union U1 *l_1310 = &g_445;
    uint8_t * const *l_1386[8][10] = {{&g_139[3][5][1],&g_139[1][6][2],&g_139[1][8][1],&g_139[2][6][1],&g_139[1][8][1],&g_139[1][6][2],&g_139[3][5][1],&g_139[1][6][2],&g_139[1][6][2],(void*)0},{(void*)0,(void*)0,&g_139[1][1][0],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][1][0],&g_139[1][6][2],&g_139[3][5][1],(void*)0,&g_139[1][6][2]},{&g_139[3][6][1],(void*)0,&g_139[1][6][2],&g_139[3][5][0],&g_139[0][3][0],&g_139[3][8][0],&g_139[3][5][1],(void*)0,(void*)0,&g_139[1][6][2]},{&g_139[1][0][1],&g_139[1][6][2],(void*)0,&g_139[0][3][0],&g_139[1][6][2],&g_139[1][6][2],&g_139[0][3][0],(void*)0,&g_139[1][6][2],&g_139[1][0][1]},{&g_139[1][6][2],&g_139[3][5][0],&g_139[1][1][0],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][8][1],&g_139[1][6][2],&g_139[0][3][0],&g_139[1][1][0],(void*)0},{&g_139[1][6][2],&g_139[3][5][1],&g_139[4][0][0],&g_139[3][5][0],&g_139[1][6][2],&g_139[0][6][2],&g_139[2][6][1],&g_139[1][6][2],&g_139[3][8][0],&g_139[1][0][1]},{&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2],(void*)0,&g_139[3][6][1],(void*)0,&g_139[1][6][2],&g_139[1][6][2]},{&g_139[1][6][2],&g_139[1][6][2],&g_139[1][6][2],(void*)0,&g_139[0][3][0],&g_139[1][8][1],&g_139[1][1][0],&g_139[3][6][1],&g_139[1][8][1],&g_139[1][6][2]}};
    uint64_t l_1423 = 0x2B9399D66305C9B5LL;
    uint32_t **l_1541 = &g_1100;
    int64_t l_1617 = 7L;
    int32_t l_1619 = 0x52028CA2L;
    const int32_t *l_1700[4][9][3] = {{{(void*)0,(void*)0,&g_142},{&g_659,(void*)0,(void*)0},{&g_142,(void*)0,(void*)0},{(void*)0,&l_1003,(void*)0},{&g_142,&g_5,(void*)0},{(void*)0,&g_189,&g_142},{(void*)0,(void*)0,(void*)0},{&g_659,(void*)0,&g_659},{&g_26,&g_5,&g_26}},{{&l_1003,&g_26,&g_659},{&g_659,(void*)0,&g_5},{&g_189,&l_1003,(void*)0},{&g_5,(void*)0,&g_142},{&g_189,&g_189,&g_189},{(void*)0,&g_142,(void*)0},{(void*)0,&g_659,(void*)0},{(void*)0,&g_189,&g_659},{&g_189,(void*)0,(void*)0}},{{&g_5,&g_659,&g_5},{&l_1003,&g_659,&g_5},{(void*)0,(void*)0,&g_659},{(void*)0,&g_189,(void*)0},{&g_659,&g_659,&l_1003},{&g_142,&g_142,&l_1003},{&l_1003,&g_189,&g_5},{&g_26,(void*)0,(void*)0},{&g_659,&g_142,&g_189}},{{&g_189,&g_26,(void*)0},{&g_26,&l_1003,&g_5},{&g_659,&g_189,&l_1003},{(void*)0,&g_189,&l_1003},{&l_1003,&g_26,(void*)0},{(void*)0,&l_1003,&g_659},{(void*)0,&g_659,&g_5},{&g_189,&g_5,&g_5},{&g_189,&g_659,(void*)0}}};
    uint16_t **l_1702 = (void*)0;
    uint32_t l_1707 = 4294967287UL;
    const float *l_1713 = (void*)0;
    const float **l_1712 = &l_1713;
    float **l_1714[3];
    float ***l_1715 = (void*)0;
    float ***l_1716 = &l_1714[2];
    int32_t l_1719 = 1L;
    uint64_t l_1720[5];
    int16_t l_1722[8] = {(-9L),0x1FCBL,(-9L),0x1FCBL,(-9L),0x1FCBL,(-9L),0x1FCBL};
    int32_t l_1794[2];
    const int32_t *l_1806[3][1][7] = {{{&g_659,&l_1003,&g_659,&l_1003,&g_659,&g_142,&g_142}},{{&g_659,&l_1003,&g_659,&l_1003,&g_659,&g_142,&g_142}},{{&g_659,&l_1003,&g_659,&l_1003,&g_659,&g_142,&g_142}}};
    const int32_t *l_1807 = &l_1003;
    const int32_t *l_1808 = (void*)0;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1714[i] = &g_1123;
    for (i = 0; i < 5; i++)
        l_1720[i] = 0x5BFC7BB2DA987016LL;
    for (i = 0; i < 2; i++)
        l_1794[i] = 8L;
    for (g_263 = 0; (g_263 <= 6); g_263 += 1)
    { /* block id: 432 */
        uint32_t l_966 = 0x4910D41CL;
        const uint16_t *l_970 = &g_971;
        int64_t l_974 = 0x1B946D5610B4F425LL;
        union U1 *l_993 = &g_445;
        int32_t l_1018 = 0x92A25F46L;
        int64_t *** const *l_1066 = &l_1065;
        union U0 l_1076 = {4294967295UL};
        int32_t l_1080[2][10] = {{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)},{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)}};
        int64_t l_1086 = 0xDBB56AA5930BC709LL;
        const uint16_t l_1176 = 0x36F1L;
        float ***l_1190 = (void*)0;
        int i, j;
        (*g_546) = &p_146;
        for (g_54 = 0; (g_54 <= 2); g_54 += 1)
        { /* block id: 436 */
            int16_t l_975[1][3];
            union U1 *l_992 = &g_445;
            int32_t l_1009 = (-1L);
            int32_t l_1027 = 0L;
            uint32_t *l_1034 = &g_445.f0;
            uint32_t *l_1035 = (void*)0;
            uint32_t *l_1037 = &g_1038;
            const union U1 **l_1056 = &l_1054[3][5];
            int64_t *l_1059 = &l_974;
            int64_t *l_1060[6] = {&g_719,&g_719,&g_719,&g_719,&g_719,&g_719};
            int16_t *l_1061 = &g_864;
            int32_t l_1079 = (-7L);
            int32_t l_1091 = 0L;
            float l_1179 = 0x0.Dp-1;
            int8_t l_1184 = 0x34L;
            uint8_t ** const *l_1186 = &g_138;
            uint8_t ** const **l_1185 = &l_1186;
            float **l_1192[2][4][2] = {{{&g_1123,(void*)0},{&g_1123,(void*)0},{&g_1123,&g_1123},{&g_1123,&g_1123}},{{(void*)0,&g_1123},{(void*)0,&g_1123},{&g_1123,&g_1123},{&g_1123,(void*)0}}};
            float ***l_1191 = &l_1192[0][0][0];
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_975[i][j] = 0xAAE8L;
            }
        }
    }
    if ((((g_18[1] , (!(safe_add_func_int64_t_s_s(0L, (-8L))))) , l_1098) != (void*)0))
    { /* block id: 558 */
        int8_t l_1236[1];
        union U1 l_1259 = {3UL};
        int32_t l_1273 = (-1L);
        int32_t l_1297 = (-3L);
        uint16_t l_1298 = 65528UL;
        uint32_t **l_1540 = &g_1100;
        int32_t l_1570 = (-7L);
        int32_t l_1575[1][6] = {{0x2301E49FL,0x2301E49FL,0x2301E49FL,0x2301E49FL,0x2301E49FL,0x2301E49FL}};
        uint64_t **l_1601 = (void*)0;
        const int32_t *l_1699[9] = {&g_189,&g_142,&g_189,&g_189,&g_142,&g_189,&g_189,&g_142,&g_189};
        int i, j;
        for (i = 0; i < 1; i++)
            l_1236[i] = (-1L);
        if (((((safe_add_func_int8_t_s_s(0xFDL, ((l_1236[0] != (((p_146 && ((((safe_mul_func_uint16_t_u_u(0xAED6L, ((*l_1098) == (safe_rshift_func_uint8_t_u_u((g_192[0] || ((void*)0 != &p_146)), (6UL <= (*p_148))))))) | (*p_148)) <= p_146) | 2UL)) == 0x2DL) || p_150)) > p_146))) && 4294967287UL) >= 255UL) >= (*g_191)))
        { /* block id: 559 */
            uint64_t l_1241 = 1UL;
            int32_t l_1258 = 0x5BD72459L;
            int32_t *l_1260 = &g_18[1];
            int32_t l_1326 = 0x3B02C096L;
            int32_t **l_1355 = &l_1260;
            int32_t ** const *l_1354 = &l_1355;
            uint8_t * const *l_1385 = &g_139[1][6][2];
            int64_t ***l_1401 = &g_383[4];
            int32_t l_1418 = (-5L);
            int32_t l_1420 = 0xA0748E1EL;
            uint8_t l_1446 = 0xDCL;
            int16_t *l_1451 = &g_864;
            int16_t **l_1450 = &l_1451;
            float *l_1464 = &g_1036;
lbl_1449:
            ++l_1241;
            if ((safe_add_func_int8_t_s_s((l_1236[0] > ((safe_sub_func_int8_t_s_s((+((**g_1099)++)), (l_1258 |= (safe_add_func_uint32_t_u_u((0x533A7777931AD917LL ^ ((void*)0 == l_1253)), (p_146 = (safe_lshift_func_int8_t_s_s(l_1236[0], 7)))))))) >= ((l_1259 , l_1260) != (void*)0))), (l_1236[0] && 0xBDF8D431L))))
            { /* block id: 564 */
                int32_t l_1272 = 0xC30D5758L;
                int32_t l_1324 = (-1L);
                int32_t l_1325 = 0x51CCEA7DL;
                float **l_1337[5][9][2] = {{{&g_1123,&g_1123},{&g_1123,&g_1123},{(void*)0,(void*)0},{&g_1123,&g_1123},{&g_1123,&g_1123},{(void*)0,&g_1123},{(void*)0,&g_1123},{&g_1123,&g_1123},{&g_1123,(void*)0}},{{(void*)0,(void*)0},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,(void*)0},{(void*)0,(void*)0},{&g_1123,&g_1123},{&g_1123,&g_1123}},{{&g_1123,&g_1123},{&g_1123,(void*)0},{&g_1123,&g_1123},{(void*)0,&g_1123},{(void*)0,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123}},{{&g_1123,&g_1123},{(void*)0,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{(void*)0,&g_1123}},{{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{&g_1123,&g_1123},{(void*)0,&g_1123},{(void*)0,&g_1123},{&g_1123,(void*)0},{&g_1123,&g_1123}}};
                union U1 l_1367[5][4] = {{{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}}};
                int i, j, k;
                l_1273 |= (safe_sub_func_uint32_t_u_u((safe_add_func_uint8_t_u_u((*g_715), (((l_1236[0] >= l_1267) ^ ((-8L) != (((((p_150 , (p_146 < p_146)) == ((safe_add_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(l_1272, (0L & p_146))), (**g_714))) , (*p_148))) & (*g_131)) | p_146) || 0xD2L))) < (*g_947)))), (*p_147)));
                for (l_1259.f0 = 0; (l_1259.f0 == 39); ++l_1259.f0)
                { /* block id: 568 */
                    union U1 *l_1309[4];
                    int32_t l_1311 = 0L;
                    int32_t l_1323[5];
                    int32_t l_1358 = (-1L);
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1309[i] = &g_445;
                    for (i = 0; i < 5; i++)
                        l_1323[i] = (-1L);
                    l_1297 &= (safe_mul_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(((*l_1098) <= ((*g_191) || (safe_mul_func_uint8_t_u_u(((safe_sub_func_int8_t_s_s((safe_div_func_int64_t_s_s(l_1236[0], (g_1288 , (safe_rshift_func_uint8_t_u_s(0x2EL, 2))))), ((safe_rshift_func_uint8_t_u_u((252UL & (***g_713)), 3)) >= ((safe_mod_func_int64_t_s_s((safe_rshift_func_int8_t_s_s((((void*)0 == &g_467) <= l_1273), l_1258)), 1UL)) | 0x6D5878BAL)))) && p_150), p_146)))), (*p_147))), 0UL)), l_1241));
                    if ((*p_147))
                        continue;
                    if ((*p_147))
                    { /* block id: 571 */
                        int16_t *l_1321 = &g_864;
                        int16_t *l_1322 = &g_224;
                        int32_t l_1336[2];
                        float **l_1338 = (void*)0;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1336[i] = 0x59D3517AL;
                        l_1298++;
                        l_1326 &= (safe_lshift_func_int8_t_s_s(((((safe_lshift_func_int8_t_s_s(0x91L, ((((*g_191) != (safe_mod_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((l_1309[0] == ((*g_691) = l_1310)), l_1311)), p_146))) , (l_1258 = ((l_1324 = ((((65533UL || ((l_1272 ^= (((((-4L) == (safe_lshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_s(((safe_lshift_func_int16_t_s_u((l_1311 |= ((*l_1322) = ((*l_1321) = ((~((**g_1099) = ((*g_191) , p_146))) != p_150)))), l_1323[3])) , 0x53L), (*g_131))), (*g_131))), l_1323[3]))) , l_1311) && p_150) <= g_641.f0)) <= 0x10L)) & l_1324) <= l_1323[4]) , (*p_147))) , (***g_713)))) & l_1325))) && 4294967295UL) || (*p_147)) | l_1298), l_1241));
                        (*g_228) = (g_1181 || ((*l_1322) |= (safe_lshift_func_int16_t_s_u(((g_18[1] ^ (safe_lshift_func_uint8_t_u_s((!((*l_967) |= ((safe_mul_func_int8_t_s_s((l_1273 <= (((safe_rshift_func_uint16_t_u_u((l_1323[3] = l_1336[0]), 5)) <= ((*l_1067) , ((p_146 , l_1337[2][3][1]) == l_1338))) || (((safe_add_func_uint16_t_u_u(((safe_unary_minus_func_int16_t_s((((g_1342[0] = (void*)0) != &g_1343) <= p_150))) , 0UL), l_1336[0])) || l_1311) >= 0x4F31CFD546FA9E10LL))), l_1325)) , l_1325))), 4))) , l_1336[0]), 13))));
                    }
                    else
                    { /* block id: 587 */
                        const int64_t l_1357 = 0x9321972A2595791CLL;
                        union U1 l_1370 = {8UL};
                        l_1358 |= (~(l_1323[3] = ((((((*g_131) = (safe_mul_func_int16_t_s_s(((((255UL == ((((safe_lshift_func_uint16_t_u_u(0UL, ((safe_mul_func_float_f_f(((l_1326 = (0x4.A8591Cp-72 > ((l_1324 = ((void*)0 != l_1354)) <= (+l_1357)))) == (l_1297 <= (l_1325 = l_1323[2]))), l_1323[2])) , g_192[1]))) , 8L) | p_150) ^ (***g_713))) >= l_1311) || 0x18L) == g_716), g_864))) | p_150) , &g_21) != l_967) && l_1325)));
                        if (l_1357)
                            break;
                        (**l_1043) = &p_146;
                        (*g_128) ^= (safe_mod_func_int16_t_s_s(g_513[7][1][1], (safe_div_func_uint8_t_u_u((((safe_mul_func_float_f_f((g_405 = l_1273), (safe_mul_func_float_f_f(p_150, 0x4.C0FCEFp+95)))) , l_1367[0][3]) , (**g_714)), (safe_add_func_uint16_t_u_u(p_150, (((((****g_1343) = (l_1370 , ((safe_rshift_func_int8_t_s_u(((safe_rshift_func_uint16_t_u_s(l_1324, 7)) == 0xA2AC7564L), 1)) < g_1375))) && (*g_191)) && 252UL) | 0xDCCC2F4063E99975LL)))))));
                    }
                }
                (**l_1043) = &p_146;
            }
            else
            { /* block id: 602 */
                uint32_t l_1382 = 0xF437EED5L;
                int16_t *l_1408 = &g_224;
                int16_t *l_1409 = (void*)0;
                int16_t *l_1410 = (void*)0;
                int16_t *l_1411 = &g_863;
                int32_t l_1415 = 2L;
                int32_t l_1416 = 0x962C42BBL;
                int32_t l_1417 = 0x7D62F2A2L;
                int32_t l_1422 = (-10L);
                for (l_1092 = 0; (l_1092 <= 55); l_1092++)
                { /* block id: 605 */
                    int64_t l_1380[8][9] = {{1L,8L,1L,0x55275E7785983BAALL,1L,8L,1L,0x55275E7785983BAALL,1L},{0x052ECE96ECC02F34LL,0x052ECE96ECC02F34LL,8L,(-1L),0L,0L,(-1L),8L,0x052ECE96ECC02F34LL},{1L,0x55275E7785983BAALL,1L,0x55275E7785983BAALL,1L,0x55275E7785983BAALL,1L,0x55275E7785983BAALL,1L},{0L,(-1L),8L,0x052ECE96ECC02F34LL,0x052ECE96ECC02F34LL,8L,(-1L),0L,0L},{1L,0x55275E7785983BAALL,1L,8L,1L,0x55275E7785983BAALL,1L,8L,1L},{0L,0x052ECE96ECC02F34LL,(-1L),(-1L),0x052ECE96ECC02F34LL,0L,8L,8L,0L},{1L,8L,1L,8L,1L,8L,1L,8L,1L},{0x052ECE96ECC02F34LL,(-1L),(-1L),0x052ECE96ECC02F34LL,0L,8L,8L,0L,0x052ECE96ECC02F34LL}};
                    int i, j;
                    for (g_864 = 0; (g_864 != 3); ++g_864)
                    { /* block id: 608 */
                        int16_t l_1381[9] = {0x0348L,0x1094L,0x1094L,0x0348L,0x1094L,0x1094L,0x0348L,0x1094L,0x1094L};
                        int i;
                        l_1382--;
                        p_146 = (((**g_691) , (-5L)) == p_150);
                        l_1386[4][3] = l_1385;
                    }
                }
                l_1326 &= (((((safe_lshift_func_uint8_t_u_s(0x7FL, 0)) , ((safe_mul_func_int16_t_s_s((l_1259.f0 & (-1L)), ((((safe_mul_func_int16_t_s_s(g_58[1][3], (safe_rshift_func_uint8_t_u_u(l_1241, (((*l_1067) , (p_150 , (((safe_div_func_int8_t_s_s(((l_1382 <= (safe_sub_func_uint64_t_u_u(((*g_191) |= ((1L | l_1382) >= p_150)), 0L))) > p_150), p_146)) < (*p_148)) | 0xD4L))) || l_1236[0]))))) >= p_150) ^ p_150) , 7UL))) , g_80)) == (-10L)) >= p_150) & p_150);
                if ((((safe_rshift_func_int8_t_s_u(0x60L, 3)) == ((((*l_1411) = (0xFA78B82DA00917E4LL & (((*g_191) |= (l_1401 == (void*)0)) , ((1L >= ((*l_1408) &= (((safe_div_func_uint32_t_u_u((**g_1099), ((**g_691) , (*p_147)))) & (safe_add_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((2L >= p_146), 4L)), p_146))) , (-1L)))) < p_150)))) < 0x41A7L) > p_150)) || (*l_1098)))
                { /* block id: 619 */
                    uint16_t l_1414 = 0xDD97L;
                    int32_t l_1419 = 0x854BB2E3L;
                    int32_t l_1421 = 0L;
                    for (g_863 = 2; (g_863 >= 0); g_863 -= 1)
                    { /* block id: 622 */
                        const int32_t l_1412 = 0xC6225DA0L;
                        float *l_1413[1][2][9] = {{{&g_405,&g_405,&g_405,&g_405,&g_405,&g_405,&g_405,&g_405,&g_405},{&g_1036,&g_1036,&g_1036,&g_1036,&g_1036,&g_1036,&g_1036,&g_1036,&g_1036}}};
                        int i, j, k;
                        if ((*p_147))
                            break;
                        l_1414 = l_1412;
                    }
                    ++l_1423;
                }
                else
                { /* block id: 627 */
                    int64_t l_1441 = 0x989A6452165FD1EFLL;
                    int32_t l_1444 = 2L;
                    for (g_1005 = 0; (g_1005 <= 2); g_1005 += 1)
                    { /* block id: 630 */
                        int32_t l_1428 = 1L;
                        int32_t l_1443 = 0x2C5B36BEL;
                        int32_t l_1445 = 0xA210CCD6L;
                        int i;
                        (**l_1043) = (((safe_mod_func_uint64_t_u_u((l_1428 > ((*p_147) > ((safe_mod_func_uint64_t_u_u(0x93677728398BEF45LL, 0xF263FFFDC10525FCLL)) || l_1236[0]))), (safe_unary_minus_func_uint32_t_u(((((0x7.6p-1 >= (p_150 <= (safe_mul_func_float_f_f(g_1434, (safe_add_func_float_f_f((safe_add_func_float_f_f((l_1420 = ((safe_add_func_float_f_f((((-0x1.Fp-1) <= p_146) >= p_146), 0xC.B0AAB0p+23)) >= p_146)), 0x1.Cp-1)), p_146)))))) , (*g_1032)) || (*p_147)) >= 0xBB5F7A03FA3717C7LL))))) ^ (*g_191)) , (void*)0);
                        --l_1446;
                        return (*g_194);
                    }
                }
            }
            if (l_1446)
                goto lbl_1449;
            l_1258 = (((l_1450 == (void*)0) > p_146) >= (safe_div_func_float_f_f(((safe_mul_func_float_f_f(((safe_sub_func_float_f_f((safe_mul_func_float_f_f(((*l_1464) = (safe_mul_func_float_f_f((p_146 >= l_1258), ((+(!((*g_191) = (*g_191)))) , 0x9.48B3CAp-27)))), 0xC.72F808p-78)), ((!(-0x1.0p-1)) == (-0x6.4p+1)))) > p_146), p_150)) == 0x9.211D16p-14), p_146)));
        }
        else
        { /* block id: 642 */
            int32_t *l_1468 = &l_1003;
            int16_t *l_1482 = &g_863;
            uint16_t **l_1564 = (void*)0;
            int8_t *l_1565 = &g_58[3][4];
            int32_t l_1573 = (-1L);
            int32_t l_1611 = 0x0E7DBE1BL;
            int32_t l_1616 = 0x4DE6B86FL;
            int32_t l_1618 = 0x609F1F0EL;
            int32_t l_1620 = 3L;
            int32_t l_1621 = 0x7427E20EL;
            int32_t l_1622 = (-1L);
            int32_t l_1623 = 0L;
            int32_t l_1626 = 3L;
            int32_t *l_1646 = (void*)0;
            uint8_t l_1683 = 0UL;
            uint32_t l_1684 = 6UL;
            union U1 *****l_1697[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1697[i] = (void*)0;
            for (l_1259.f0 = 0; (l_1259.f0 >= 31); l_1259.f0++)
            { /* block id: 645 */
                (**l_1043) = l_1468;
            }
            l_1273 ^= (((--(*g_1100)) && (((safe_lshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u((safe_div_func_int8_t_s_s(((safe_add_func_int16_t_s_s((*p_148), (safe_add_func_int64_t_s_s(p_150, 0x376FD881D189D181LL)))) , (*g_131)), (safe_unary_minus_func_uint16_t_u(p_146)))), (*l_1098))), 2)) != p_146) , ((((*l_1067) , ((*l_1482) ^= (p_150 , g_91[4][0][3].f0))) == (*l_1098)) == (*l_1468)))) >= (*l_1468));
            p_146 = (l_1259.f0 > ((*l_1482) = (safe_add_func_int32_t_s_s(((safe_mul_func_uint8_t_u_u((0x3E66C9476C0A7F37LL <= p_146), (0x9B2C845B64A0CBC2LL < p_150))) > (safe_lshift_func_uint16_t_u_u(((void*)0 != p_149), 7))), ((**g_1099)--)))));
            if ((*p_147))
            { /* block id: 654 */
                int8_t l_1495 = 0L;
                uint8_t ***l_1502 = &g_138;
                uint8_t ****l_1501[7][7][1];
                const int32_t *l_1533 = &l_1297;
                const int32_t *l_1535 = &g_189;
                int16_t l_1558 = 0x48B9L;
                int8_t **l_1566 = &g_131;
                int32_t l_1567 = 0x07AF91CFL;
                uint16_t **l_1582 = (void*)0;
                int32_t l_1610 = 0x02C748CFL;
                int32_t l_1612[2];
                union U1 l_1639 = {0x5B0BC4E8L};
                union U0 *l_1642 = (void*)0;
                int i, j, k;
                for (i = 0; i < 7; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_1501[i][j][k] = &l_1502;
                    }
                }
                for (i = 0; i < 2; i++)
                    l_1612[i] = (-2L);
lbl_1536:
                for (g_1005 = 0; (g_1005 >= 34); g_1005++)
                { /* block id: 657 */
                    int32_t l_1528 = (-1L);
                    int32_t *l_1532 = &l_1273;
                    const int32_t *l_1534 = &l_1273;
                    for (g_863 = 1; (g_863 < (-4)); g_863--)
                    { /* block id: 660 */
                        uint32_t l_1529 = 0x025F7249L;
                        int8_t l_1531 = 0xA9L;
                        l_1297 |= (l_1495 || (((****g_1343) = ((((((safe_add_func_int16_t_s_s((+(safe_lshift_func_uint8_t_u_s(((l_1501[2][2][0] == ((safe_add_func_uint8_t_u_u(((g_1288 > ((safe_sub_func_int64_t_s_s((safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s(0x3D22L, (((safe_lshift_func_int16_t_s_u(((safe_sub_func_uint16_t_u_u(p_150, ((safe_mul_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((((g_864 = (*p_148)) , (((safe_sub_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_uint32_t_u(((((-1L) && (p_146 ^ ((((safe_mul_func_uint8_t_u_u(l_1528, l_1528)) & 0xB8L) < (*g_191)) == p_146))) | l_1298) | p_146))), l_1529)) ^ (-2L)), (*l_1468))) == p_150), 1L)) && 0x1043ADBD88775921LL) >= p_150)) | l_1236[0]), p_146)), g_863)) & l_1528))) == 3UL), 9)) & p_150) , 0UL))), 0x2317L)), p_146)) == (*p_147))) >= l_1528), p_150)) , (void*)0)) , l_1273), 2))), p_146)) , (*g_191)) == g_1530) == 0x02F931AAL) & p_146) , l_1531)) > (*g_191)));
                        if ((*g_1032))
                            break;
                        if ((*l_1468))
                            break;
                        (**l_1043) = l_1532;
                    }
                    return l_1535;
                }
                for (p_146 = 0; (p_146 <= 0); p_146 += 1)
                { /* block id: 672 */
                    union U0 *l_1543 = &g_641;
                    const uint16_t l_1554 = 0UL;
                    int32_t l_1559 = 4L;
                    (*l_1468) |= (*p_147);
                    for (g_189 = 0; (g_189 <= 0); g_189 += 1)
                    { /* block id: 676 */
                        uint32_t ***l_1542 = &g_1099;
                        union U0 *l_1546 = &g_91[1][2][0];
                        union U1 l_1549 = {0xAF10088AL};
                        float *l_1555 = (void*)0;
                        float *l_1556 = (void*)0;
                        float *l_1557[8] = {&g_405,&g_405,&g_405,&g_405,&g_405,&g_405,&g_405,&g_405};
                        int i;
                        if (l_1003)
                            goto lbl_1536;
                        (*l_1468) ^= ((p_146 >= (!(safe_mul_func_float_f_f((l_1540 == ((*l_1542) = l_1541)), (l_1543 != (((*g_191)--) , l_1546)))))) , 4L);
                        l_1559 = (((safe_div_func_float_f_f((((((*g_1100) >= (255UL ^ p_150)) | (l_1549 , (l_1297 && (0x389D4567L & 0xF79DB0D5L)))) >= 0xFE8ADB90L) , (safe_div_func_float_f_f(((-((g_1036 = (-((0x0.Ep+1 != 0xB.1AA800p-52) >= l_1554))) > 0x7.92CD45p-3)) == (-0x2.Ap+1)), 0x7.67F527p-58))), l_1298)) > p_150) != l_1558);
                    }
                }
                if ((safe_rshift_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((&p_147 != (((((void*)0 != l_1564) , l_1565) != ((*l_1566) = &g_58[3][4])) , &p_147)), (**g_1099))), 14)))
                { /* block id: 686 */
                    int16_t l_1569[1][7][10] = {{{0L,0xA73EL,(-1L),0L,0x59E3L,0x7063L,0x59E3L,1L,(-1L),0xB290L},{0L,0x7063L,1L,9L,0x3036L,1L,0xB290L,6L,0x7063L,1L},{(-1L),1L,0x3036L,(-1L),4L,1L,1L,4L,(-1L),0x3036L},{0L,0L,0x59E3L,4L,1L,0L,9L,0L,(-4L),9L},{1L,(-1L),1L,1L,(-1L),(-1L),9L,(-1L),(-1L),1L},{0x7063L,0L,0x7063L,1L,9L,0x3036L,1L,0xB290L,6L,0x7063L},{0xB290L,1L,(-4L),4L,0L,0xA73EL,0xB290L,0xB290L,0xA73EL,0L}}};
                    int32_t l_1571 = 0x8B9BC4B4L;
                    int32_t l_1572[8][4][7] = {{{0x20D18729L,(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL,(-6L),0x20D18729L},{3L,0x20D18729L,(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL,(-6L)},{0x20D18729L,0x20D18729L,0x22E70676L,0x1B3F2437L,(-1L),0x22E70676L,(-1L)},{0x1B3F2437L,(-6L),(-6L),0x1B3F2437L,0xC5C54D9BL,3L,0x1B3F2437L}},{{3L,(-1L),0xC5C54D9BL,0xC5C54D9BL,(-1L),3L,(-6L)},{(-1L),0x1B3F2437L,0x22E70676L,0x20D18729L,0x20D18729L,0x22E70676L,0x1B3F2437L},{(-1L),(-6L),3L,(-1L),0xC5C54D9BL,0xC5C54D9BL,(-1L)},{3L,0x1B3F2437L,3L,0xC5C54D9BL,0x1B3F2437L,(-6L),(-6L)}},{{0x1B3F2437L,(-1L),0x22E70676L,(-1L),0x1B3F2437L,0x22E70676L,0x20D18729L},{0x20D18729L,(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL,(-6L),0x20D18729L},{3L,0x20D18729L,(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL,(-6L)},{0x20D18729L,0x20D18729L,0x22E70676L,0x1B3F2437L,(-1L),0x22E70676L,(-1L)}},{{0x1B3F2437L,(-6L),(-6L),0x1B3F2437L,0xC5C54D9BL,3L,0x1B3F2437L},{3L,(-1L),0xC5C54D9BL,0xC5C54D9BL,(-1L),3L,(-6L)},{(-1L),0x1B3F2437L,0x22E70676L,0x20D18729L,0x20D18729L,0x22E70676L,0x1B3F2437L},{(-1L),(-6L),3L,(-1L),0xC5C54D9BL,0xC5C54D9BL,(-1L)}},{{3L,0x1B3F2437L,3L,0xC5C54D9BL,0x1B3F2437L,(-6L),(-6L)},{0x1B3F2437L,(-1L),0x22E70676L,(-1L),0x1B3F2437L,0x22E70676L,0x20D18729L},{0x20D18729L,0x22E70676L,0x548EBAEFL,3L,0x548EBAEFL,0x22E70676L,3L},{0x5851DF04L,3L,0x22E70676L,0x548EBAEFL,3L,0x548EBAEFL,0x22E70676L}},{{3L,3L,0x20D18729L,(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL},{(-6L),0x22E70676L,0x22E70676L,(-6L),0x548EBAEFL,0x5851DF04L,(-6L)},{0x5851DF04L,0xC5C54D9BL,0x548EBAEFL,0x548EBAEFL,0xC5C54D9BL,0x5851DF04L,0x22E70676L},{0xC5C54D9BL,(-6L),0x20D18729L,3L,3L,0x20D18729L,(-6L)}},{{0xC5C54D9BL,0x22E70676L,0x5851DF04L,0xC5C54D9BL,0x548EBAEFL,0x548EBAEFL,0xC5C54D9BL},{0x5851DF04L,(-6L),0x5851DF04L,0x548EBAEFL,(-6L),0x22E70676L,0x22E70676L},{(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL,(-6L),0x20D18729L,3L},{3L,0x22E70676L,0x548EBAEFL,3L,0x548EBAEFL,0x22E70676L,3L}},{{0x5851DF04L,3L,0x22E70676L,0x548EBAEFL,3L,0x548EBAEFL,0x22E70676L},{3L,3L,0x20D18729L,(-6L),0xC5C54D9BL,0x20D18729L,0xC5C54D9BL},{(-6L),0x22E70676L,0x22E70676L,(-6L),0x548EBAEFL,0x5851DF04L,(-6L)},{0x5851DF04L,0xC5C54D9BL,0x548EBAEFL,0x548EBAEFL,0xC5C54D9BL,0x5851DF04L,0x22E70676L}}};
                    uint32_t l_1576 = 5UL;
                    int i, j, k;
                    for (l_1297 = 0; (l_1297 <= 2); l_1297 += 1)
                    { /* block id: 689 */
                        int32_t l_1574 = 0xCEEEA30EL;
                        const uint16_t *l_1581[9] = {&g_971,&g_971,&g_971,&g_971,&g_971,&g_971,&g_971,&g_971,&g_971};
                        const uint16_t **l_1580 = &l_1581[2];
                        const uint16_t ***l_1579 = &l_1580;
                        int32_t l_1592 = (-1L);
                        int i;
                        ++l_1576;
                        (*g_228) |= ((l_1572[7][1][1] = g_18[l_1297]) < (((((((*l_1579) = (void*)0) != &l_967) || ((*l_1468) , (((*l_1468) = (l_1582 != l_1582)) || (safe_div_func_int16_t_s_s((-1L), ((*l_1482) &= (-1L))))))) >= ((((safe_sub_func_int16_t_s_s((((safe_add_func_int8_t_s_s(((((l_1592 = (((!(((*g_191) &= ((safe_mul_func_int8_t_s_s(0x48L, (-1L))) , 0x980BDAAEE6F165EALL)) >= 0x11DB2D5CA0C89C3FLL)) ^ (-1L)) < 0x85D5L)) == 0x2A9842E39776C7C9LL) , 7UL) , 0x20L), p_146)) , g_192[0]) , g_58[3][4]), 0x8A33L)) && p_150) || 0x24L) >= 0x11L)) , &p_150) != (void*)0));
                        if ((*l_1468))
                            continue;
                        if (l_1574)
                            continue;
                    }
                }
                else
                { /* block id: 701 */
                    union U0 * const l_1598 = &g_641;
                    int32_t l_1600 = 0x12D44B4EL;
                    for (g_676 = (-12); (g_676 > 54); g_676 = safe_add_func_uint16_t_u_u(g_676, 7))
                    { /* block id: 704 */
                        int32_t l_1597 = 0x0F07E21AL;
                        int32_t l_1599[6][5][8] = {{{(-1L),9L,4L,1L,7L,0x961DFC8EL,9L,0L},{(-1L),7L,(-1L),0xE59E440DL,(-2L),(-1L),0x261ECEA8L,(-5L)},{0x0E597547L,(-1L),(-1L),(-1L),0L,0L,(-1L),(-1L)},{1L,1L,0x961DFC8EL,0x4D00443FL,(-1L),0xD8A83E53L,0x0E597547L,0L},{(-1L),0L,(-4L),(-1L),9L,(-1L),(-1L),0L}},{{0L,0x261ECEA8L,0L,0x4D00443FL,0x4DE4066AL,(-4L),9L,(-1L)},{0x3E989664L,0x1984D68FL,0xBD597B19L,(-1L),0L,0xBD597B19L,7L,(-5L)},{0L,(-1L),9L,0xE59E440DL,0x0E597547L,0L,(-1L),0L},{1L,0x530172F4L,0xE59E440DL,1L,0xE59E440DL,0x530172F4L,1L,(-4L)},{0x94F0DE03L,0L,0x961DFC8EL,(-5L),7L,0xBD597B19L,0L,(-1L)}},{{(-4L),7L,0L,9L,7L,1L,0xD8A83E53L,0x6E9710AEL},{9L,0xD8A83E53L,0xFE6472E2L,(-1L),1L,0x6A992038L,0L,0xFE6472E2L},{(-4L),(-1L),7L,(-1L),0xE59E440DL,(-2L),(-1L),0x261ECEA8L},{0x86057030L,(-1L),0x512EF073L,(-10L),(-1L),0x961DFC8EL,0x961DFC8EL,(-1L)},{0L,0xBD597B19L,0xBD597B19L,0L,0x4FD6CC02L,0xFE6472E2L,(-1L),0x512EF073L}},{{(-1L),0L,7L,0x4DE4066AL,0x530172F4L,(-5L),0L,0x6E9710AEL},{0xFE6472E2L,0L,0x6A992038L,1L,(-1L),0xFE6472E2L,0xD8A83E53L,9L},{(-4L),0xBD597B19L,(-10L),(-1L),0x13F1FCB2L,0x961DFC8EL,(-1L),0x06F71FB0L},{0xE59E440DL,(-1L),(-5L),0x6E9710AEL,(-1L),(-2L),(-1L),(-1L)},{0xFE6472E2L,(-1L),0xBD597B19L,0x6A992038L,0L,0x6A992038L,0xBD597B19L,(-1L)}},{{(-1L),0xD8A83E53L,0x06F71FB0L,0x4DE4066AL,0x4FD6CC02L,1L,0L,0x06F71FB0L},{(-1L),0L,1L,(-1L),(-1L),1L,0L,0L},{0x86057030L,(-1L),0x06F71FB0L,1L,(-1L),0x961DFC8EL,0xBD597B19L,0x261ECEA8L},{(-1L),0x961DFC8EL,0xBD597B19L,0x261ECEA8L,1L,0x86057030L,(-1L),0x512EF073L},{0L,(-1L),(-5L),9L,0x530172F4L,1L,(-1L),(-1L)}},{{0x512EF073L,0L,(-10L),(-10L),0L,0x512EF073L,0xD8A83E53L,0x4DE4066AL},{(-1L),0xD8A83E53L,0x6A992038L,(-1L),1L,4L,0L,0xFE6472E2L},{0xE59E440DL,(-1L),7L,(-1L),(-4L),(-2L),(-1L),0x4DE4066AL},{0x86057030L,(-4L),0xBD597B19L,(-10L),(-1L),0x13F1FCB2L,0x961DFC8EL,(-1L)},{9L,0xBD597B19L,0x512EF073L,9L,0x4FD6CC02L,0x6A992038L,(-1L),0x512EF073L}}};
                        uint64_t ***l_1602[4][10][6] = {{{(void*)0,(void*)0,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,(void*)0,(void*)0,&l_1601,(void*)0,&l_1601},{&l_1601,&l_1601,(void*)0,&l_1601,(void*)0,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,(void*)0,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{(void*)0,&l_1601,&l_1601,(void*)0,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,(void*)0,&l_1601,(void*)0,&l_1601,&l_1601},{(void*)0,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601}},{{(void*)0,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,(void*)0,&l_1601},{&l_1601,(void*)0,&l_1601,&l_1601,(void*)0,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,(void*)0},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,(void*)0,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601}},{{&l_1601,&l_1601,&l_1601,&l_1601,(void*)0,&l_1601},{&l_1601,(void*)0,&l_1601,&l_1601,&l_1601,&l_1601},{(void*)0,(void*)0,&l_1601,&l_1601,(void*)0,(void*)0},{(void*)0,&l_1601,&l_1601,(void*)0,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,(void*)0,&l_1601,(void*)0,&l_1601,&l_1601},{(void*)0,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{(void*)0,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,(void*)0,&l_1601}},{{&l_1601,(void*)0,&l_1601,&l_1601,(void*)0,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,(void*)0},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,(void*)0,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,&l_1601},{&l_1601,&l_1601,&l_1601,&l_1601,&l_1601,(void*)0},{&l_1601,&l_1601,(void*)0,&l_1601,&l_1601,&l_1601},{(void*)0,&l_1601,&l_1601,&l_1601,&l_1601,(void*)0}}};
                        int i, j, k;
                        l_1600 = (((safe_div_func_float_f_f(0x3.1p+1, (l_1597 = (-0x3.7p+1)))) , l_1598) == (l_1599[1][4][1] , (void*)0));
                        g_1603 = l_1601;
                        (*g_194) = &l_1599[4][2][0];
                    }
                }
                for (g_864 = 0; (g_864 >= 0); g_864 -= 1)
                { /* block id: 713 */
                    int32_t l_1607 = 0x3031487FL;
                    int32_t l_1608[9][6][4] = {{{0xDC5877D1L,1L,0x78A7B8B3L,1L},{0xE6B7ABF5L,0x97FEE97DL,(-1L),0x9952EF72L},{(-3L),1L,(-8L),8L},{1L,1L,0xF28C5273L,(-1L)},{1L,1L,(-8L),0x15D0A9FBL},{(-3L),(-1L),(-1L),0xC476FD4CL}},{{0xE6B7ABF5L,(-9L),0x78A7B8B3L,(-1L)},{0xDC5877D1L,8L,1L,1L},{(-1L),1L,(-1L),0x34950064L},{0x3A374EE9L,1L,(-5L),1L},{1L,8L,(-1L),(-1L)},{(-8L),(-9L),(-8L),0xC476FD4CL}},{{0x3A374EE9L,(-1L),0L,0x15D0A9FBL},{0xE6B7ABF5L,1L,1L,(-1L)},{0xC00AC2EFL,1L,1L,8L},{0xE6B7ABF5L,1L,0L,0x9952EF72L},{0x3A374EE9L,0x97FEE97DL,(-8L),1L},{(-8L),1L,(-1L),0xE8518556L}},{{1L,(-9L),(-5L),0x15D0A9FBL},{0x3A374EE9L,0xE8518556L,(-1L),0x15D0A9FBL},{(-1L),(-9L),1L,0xE8518556L},{0xDC5877D1L,1L,0x78A7B8B3L,1L},{0xE6B7ABF5L,0x97FEE97DL,(-1L),0x9952EF72L},{(-3L),1L,(-8L),8L}},{{1L,1L,0xF28C5273L,(-1L)},{1L,1L,(-8L),0x15D0A9FBL},{(-3L),(-1L),(-1L),0xC476FD4CL},{0xE6B7ABF5L,(-9L),0x78A7B8B3L,(-1L)},{0xDC5877D1L,8L,1L,1L},{(-1L),1L,(-1L),0x34950064L}},{{0x3A374EE9L,1L,(-5L),1L},{1L,8L,(-1L),(-1L)},{(-8L),(-9L),(-8L),0xC476FD4CL},{0x3A374EE9L,(-1L),0L,0x15D0A9FBL},{0xE6B7ABF5L,1L,1L,(-1L)},{0xC00AC2EFL,1L,1L,8L}},{{0xE6B7ABF5L,1L,0L,0x9952EF72L},{0x3A374EE9L,0x97FEE97DL,(-8L),1L},{(-8L),1L,(-1L),0xE8518556L},{1L,(-9L),(-5L),0x15D0A9FBL},{0x3A374EE9L,0xE8518556L,(-1L),0x15D0A9FBL},{(-1L),(-9L),1L,0xE8518556L}},{{0xDC5877D1L,1L,0x78A7B8B3L,1L},{0xE6B7ABF5L,0x97FEE97DL,(-1L),0x9952EF72L},{(-3L),0xE8518556L,0x92C48C31L,0x88CE32F6L},{0xC00AC2EFL,0x97FEE97DL,0x9886208EL,0xC476FD4CL},{0xC00AC2EFL,0x4DCBA6AEL,0x92C48C31L,0x34950064L},{(-5L),0xC476FD4CL,0x78A7B8B3L,(-1L)}},{{0xC293E443L,(-1L),0xE6B7ABF5L,0xC476FD4CL},{(-1L),0x88CE32F6L,0xF28C5273L,0x97FEE97DL},{3L,0xE8518556L,0x78A7B8B3L,0xE86452C1L},{(-8L),0xE8518556L,0xDC5877D1L,0x97FEE97DL},{0xC00AC2EFL,0x88CE32F6L,3L,0xC476FD4CL},{0x92C48C31L,(-1L),0x92C48C31L,(-1L)}}};
                    int i, j, k;
                    (*g_1604) = &g_191;
                    for (g_13 = 0; (g_13 <= 2); g_13 += 1)
                    { /* block id: 717 */
                        float *l_1605 = &g_1036;
                        int32_t l_1609 = 1L;
                        int32_t l_1613 = 0x9F6B1FF9L;
                        int32_t l_1614 = (-2L);
                        int32_t l_1615 = 0xAF1EC4BBL;
                        int32_t l_1624[7];
                        uint32_t l_1627 = 4294967291UL;
                        int i, j;
                        for (i = 0; i < 7; i++)
                            l_1624[i] = 1L;
                        g_357[g_864] = g_357[g_864];
                        (*g_1606) = ((*l_1605) = p_150);
                        l_1627++;
                    }
                    for (l_1607 = 0; (l_1607 <= 2); l_1607 += 1)
                    { /* block id: 725 */
                        uint8_t l_1632[3];
                        int64_t l_1640 = 0xC87A0DB20583C7A0LL;
                        int32_t l_1641 = 0x8844056FL;
                        union U0 **l_1643 = &g_640;
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_1632[i] = 255UL;
                        l_1641 ^= (((0xC0C43A69L & (((p_150 ^ (*l_1468)) && ((safe_mod_func_int32_t_s_s(((*g_1100) | (((((3UL != ((p_150 == l_1632[2]) & (((((safe_sub_func_float_f_f((((safe_sub_func_float_f_f(p_150, (safe_div_func_float_f_f(p_150, p_150)))) < p_146) <= p_150), l_1607)) , l_1639) , (*p_147)) <= (*g_358)) ^ 7UL))) || (***g_1604)) | l_1640) & 0x6AL) < l_1298)), 0x79BD156EL)) > (**g_1099))) , 0L)) == l_1607) & (*l_1533));
                        (*l_1643) = l_1642;
                    }
                }
            }
            else
            { /* block id: 730 */
                int8_t l_1664 = (-8L);
                int64_t l_1682 = 0x1C35BE1FDDA19564LL;
                uint16_t *l_1685[9][7] = {{&g_1005,(void*)0,&g_1005,(void*)0,&g_1005,&g_1005,&l_1298},{&l_1298,&g_1005,(void*)0,&l_1298,(void*)0,&g_1005,&l_1298},{(void*)0,&l_1298,&g_1005,(void*)0,&l_1298,(void*)0,&g_1005},{&l_1298,&l_1298,&g_1005,&g_1005,(void*)0,&g_1005,(void*)0},{&g_1005,&g_1005,&g_1005,&g_1005,(void*)0,(void*)0,&g_1005},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_1005},{(void*)0,&g_1005,&g_1005,&l_1298,&l_1298,&g_1005,&g_1005},{(void*)0,&g_1005,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1005,(void*)0,(void*)0,&g_1005,&g_1005,&g_1005}};
                int32_t l_1687 = (-2L);
                int32_t l_1690[2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1690[i] = (-8L);
                for (g_719 = 0; (g_719 == (-30)); g_719 = safe_sub_func_int64_t_s_s(g_719, 9))
                { /* block id: 733 */
                    uint8_t *****l_1653[4];
                    int32_t l_1671 = 0x8A1419B5L;
                    int32_t l_1691 = 0x311E35D5L;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1653[i] = (void*)0;
                    p_146 &= ((*l_1468) = (*p_147));
                    (**l_1043) = l_1646;
                    for (l_1616 = 0; (l_1616 >= 0); l_1616 -= 1)
                    { /* block id: 739 */
                        (*l_1468) = (*p_147);
                        if ((*p_147))
                            continue;
                    }
                    if ((safe_mul_func_uint8_t_u_u(((g_1530 = (safe_add_func_uint8_t_u_u((((safe_sub_func_int8_t_s_s((l_1575[0][2] = ((g_1654[4][4][0] = l_1653[2]) == (void*)0)), ((*g_131) = (safe_mod_func_int8_t_s_s((safe_rshift_func_int16_t_s_s(((((0xB3234470L || ((!(((safe_add_func_int8_t_s_s(((safe_mod_func_int8_t_s_s(((((*g_1100) , ((l_1664 != (safe_sub_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((((((l_1671 <= p_146) > ((((((safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f(0x0.Bp+1, (-0x3.Ep+1))), p_150)), l_1671)), 0x0.3p+1)), 0xC.2476F8p-74)) == (*l_1468)) != (-0x4.4p+1)) != (*l_1468)) >= p_150) < 0x1.Cp+1)) == (-0x1.Bp-1)) >= 0x1.1p-1) < l_1682), p_146)), l_1671)), (-0x8.Bp+1)))) >= 0x0.Cp+1)) > 0x1.9p-1) , p_150), l_1683)) ^ (*l_1098)), l_1664)) , 0x6E20L) ^ l_1664)) <= l_1682)) || l_1682) == (**g_1603)) , g_58[0][4]), 0)), p_146))))) < l_1684) , p_146), p_150))) , 0xF8L), l_1297)))
                    { /* block id: 747 */
                        uint16_t **l_1686[7][10] = {{&g_498,&g_498,&l_1685[1][6],(void*)0,&l_1685[8][1],&l_1685[8][1],(void*)0,&l_1685[1][6],&g_498,&g_498},{&l_1685[8][1],(void*)0,&l_1685[1][6],&g_498,&g_498,&l_1685[1][6],(void*)0,&l_1685[8][1],&l_1685[8][1],(void*)0},{&l_1685[8][1],&g_498,(void*)0,(void*)0,&g_498,&l_1685[8][1],&l_1685[1][6],&l_1685[1][6],&l_1685[8][1],&g_498},{&g_498,(void*)0,(void*)0,&g_498,&l_1685[8][1],&l_1685[1][6],&l_1685[1][6],&l_1685[8][1],&g_498,(void*)0},{&g_498,&g_498,&l_1685[1][6],(void*)0,&l_1685[8][1],&l_1685[8][1],(void*)0,&l_1685[1][6],&g_498,&g_498},{&l_1685[8][1],(void*)0,&l_1685[1][6],&g_498,&g_498,&l_1685[1][6],(void*)0,&l_1685[8][1],&l_1685[8][1],(void*)0},{&l_1685[8][1],&g_498,(void*)0,(void*)0,&g_498,&l_1685[8][1],&l_1685[1][6],&l_1685[1][6],&l_1685[8][1],&g_498}};
                        int i, j;
                        (*l_1468) = (&g_263 == (g_498 = l_1685[2][0]));
                    }
                    else
                    { /* block id: 750 */
                        int32_t l_1688[3];
                        int32_t l_1689[8] = {0x93BFC5F7L,0x93BFC5F7L,0x93BFC5F7L,0x93BFC5F7L,0x93BFC5F7L,0x93BFC5F7L,0x93BFC5F7L,0x93BFC5F7L};
                        uint16_t l_1692 = 65534UL;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1688[i] = 0xF0AE9F88L;
                        ++l_1692;
                        (*l_1468) = l_1689[4];
                    }
                }
                (*l_1468) = (g_1695 == ((*l_1468) , l_1697[2]));
                for (l_1620 = 0; (l_1620 <= 0); l_1620 += 1)
                { /* block id: 758 */
                    const int32_t *l_1698[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1698[i] = &l_1626;
                    return g_1701;
                }
            }
        }
    }
    else
    { /* block id: 763 */
        const int32_t *l_1703 = (void*)0;
        l_1702 = &g_498;
        return l_1703;
    }
    l_1722[2] ^= (!((1L & (p_146 > ((((safe_lshift_func_int16_t_s_u(((l_1707 = (*l_1098)) <= (0xDA42FD5EL != ((g_1721 |= (l_1720[2] |= ((safe_lshift_func_int8_t_s_u((((safe_rshift_func_uint16_t_u_u((((((p_146 != (****g_1343)) , l_1712) == ((*l_1716) = l_1714[2])) != (safe_lshift_func_int8_t_s_u((l_1719 && (**g_1099)), p_150))) > p_146), 10)) > p_146) , p_150), (*g_715))) || 0UL))) == 0x0E3AE62EC0CC0653LL))), 11)) , (*g_1099)) != (void*)0) < (-1L)))) , 1L));
    if ((((*g_1100) ^= p_146) , (((safe_lshift_func_int8_t_s_s((p_146 >= (((*l_1098) != p_150) <= 0x4614L)), 5)) ^ (safe_lshift_func_int8_t_s_s((safe_div_func_uint32_t_u_u((((*l_1098) ^ (safe_mod_func_int32_t_s_s(((safe_rshift_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(((safe_div_func_int8_t_s_s(((((safe_mod_func_int64_t_s_s((g_58[2][2] != g_4), (((((safe_mod_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u((**g_714), p_150)), (*g_191))) , (*p_148)) , p_150) != 5UL) || p_150))) < (*g_131)) >= p_146) , (-10L)), p_150)) && p_146), 0xDDL)), 6)) && 251UL), 1UL))) > (**g_1099)), (**g_1099))), 4))) == 0x22117270F8F98642LL)))
    { /* block id: 773 */
        uint64_t l_1754 = 18446744073709551615UL;
        int32_t *l_1757 = &l_1719;
        union U1 *l_1769 = &g_445;
        for (g_224 = (-8); (g_224 < 15); g_224 = safe_add_func_int32_t_s_s(g_224, 3))
        { /* block id: 776 */
            float l_1760 = 0x7.9p+1;
            int32_t l_1761 = 0x81C02D6CL;
            int32_t l_1762[3][1];
            uint16_t *** const l_1774 = &l_1702;
            uint8_t ** const *l_1783 = (void*)0;
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1762[i][j] = 0x042BCFF0L;
            }
            for (l_1092 = 0; (l_1092 >= 29); l_1092 = safe_add_func_uint16_t_u_u(l_1092, 7))
            { /* block id: 779 */
                int32_t l_1756 = 0xA7EDEA4BL;
                int32_t l_1758 = 0x7B73212EL;
                const uint8_t ***l_1784 = &g_714;
                for (g_1442 = 0; (g_1442 == 28); g_1442 = safe_add_func_int64_t_s_s(g_1442, 2))
                { /* block id: 782 */
                    uint8_t *****l_1753[6][10] = {{&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930},{&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930},{&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930},{&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930},{&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930},{&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930}};
                    int32_t l_1759[9] = {0x5B9197B7L,0x5B9197B7L,0x5B9197B7L,0x5B9197B7L,0x5B9197B7L,0x5B9197B7L,0x5B9197B7L,0x5B9197B7L,0x5B9197B7L};
                    int i, j;
                    for (l_1619 = 6; (l_1619 > 4); l_1619 = safe_sub_func_int16_t_s_s(l_1619, 6))
                    { /* block id: 785 */
                        int8_t *l_1755 = &g_1181;
                        l_1756 &= (((void*)0 != &g_622) | (p_146 < ((l_1753[2][8] == &g_930) >= ((*l_1755) &= ((*g_131) = l_1754)))));
                        (**l_1043) = l_1757;
                        l_1758 = (((*l_1541) = &p_150) == (p_150 , l_1757));
                    }
                }
                for (g_1038 = 0; g_1038 < 1; g_1038 += 1)
                {
                    g_712[g_1038] = (void*)0;
                }
                if ((*g_1701))
                    continue;
                for (g_1005 = (-13); (g_1005 < 53); ++g_1005)
                { /* block id: 806 */
                    int16_t *l_1777 = (void*)0;
                    int16_t *l_1778 = (void*)0;
                    int16_t *l_1786 = &g_864;
                    const union U1 *l_1787 = &g_1055;
                    l_1769 = (void*)0;
                    if (((((*l_1786) = (safe_sub_func_int32_t_s_s((safe_mul_func_int8_t_s_s((l_1774 != l_1774), ((*g_131) = (((safe_sub_func_uint64_t_u_u((((g_1530 = 2L) < (safe_mod_func_uint64_t_u_u((safe_sub_func_int16_t_s_s(g_676, ((0x59L != (l_1783 == l_1784)) != (+9UL)))), 18446744073709551615UL))) && p_146), 0x7C90B236D074C057LL)) == l_1758) != 0UL)))), (*p_147)))) | 0x98ABL) > 0x5BL))
                    { /* block id: 811 */
                        g_1788[1] = l_1787;
                        if ((*p_147))
                            break;
                        if ((*p_147))
                            break;
                        (*g_546) = &p_146;
                    }
                    else
                    { /* block id: 816 */
                        uint8_t ***l_1793 = &g_138;
                        (*l_1757) |= (safe_mod_func_uint64_t_u_u(0xF87A0FFC0ABF362BLL, (0x4F18AF3EL && (safe_mod_func_uint16_t_u_u((((*g_610) != l_1793) != (*g_131)), 0xD14BL)))));
                    }
                    if (l_1794[1])
                        break;
                }
            }
        }
        for (g_1005 = 13; (g_1005 <= 25); g_1005 = safe_add_func_int8_t_s_s(g_1005, 4))
        { /* block id: 825 */
            for (l_1619 = 0; (l_1619 <= (-3)); l_1619 = safe_sub_func_int64_t_s_s(l_1619, 6))
            { /* block id: 828 */
                (**l_1043) = (p_147 = (((*g_692) = (**g_691)) , &p_146));
            }
        }
    }
    else
    { /* block id: 834 */
        int64_t l_1801 = 6L;
        int32_t l_1804 = 0L;
        int32_t l_1805 = 1L;
        l_1805 |= ((safe_sub_func_uint16_t_u_u(((p_146 || (*g_191)) != l_1801), ((***g_1344) | ((((*g_131) && p_150) | l_1801) , ((safe_mod_func_int64_t_s_s((l_1804 = 0x9508C0F1FF7B6791LL), ((0UL | p_150) , p_150))) , (*g_1346)))))) != l_1801);
    }
    return l_1808;
}


/* ------------------------------------------ */
/* 
 * reads : g_228 g_189 g_5 g_80 g_224 g_192 g_131 g_58 g_55 g_91 g_191 g_194 g_128 g_2 g_4 g_263 g_13 g_18 g_358 g_312 g_3 g_378 g_382 g_142 g_471 g_378.f0 g_498 g_546 g_443 g_610 g_621 g_513 g_445 g_54 g_690 g_719 g_727 g_26 g_691 g_692 g_640 g_641 g_127 g_711 g_712 g_863 g_864 g_713 g_714 g_715 g_716 g_91.f0 g_641.f0 g_138 g_139 g_947
 * writes: g_189 g_58 g_128 g_263 g_138 g_13 g_314 g_383 g_405 g_192 g_467 g_471 g_498 g_513 g_547 g_224 g_445.f0 g_640 g_676 g_445 g_690 g_711 g_719 g_727 g_863 g_18 g_889 g_139
 */
static int64_t  func_151(const int16_t  p_152, int32_t  p_153, float  p_154, int8_t  p_155)
{ /* block id: 95 */
    int32_t **l_226 = &g_128;
    int32_t ***l_227 = &l_226;
    int16_t l_237 = 0x4E7EL;
    uint8_t ** const *l_238 = (void*)0;
    uint8_t ***l_239 = &g_138;
    uint8_t ****l_240 = &l_239;
    int32_t l_275 = 0x4374FFD2L;
    int32_t l_283 = 0xF1236BC3L;
    int32_t l_284 = 1L;
    int32_t l_287 = 0x3B178A9DL;
    int32_t l_290 = (-2L);
    uint32_t l_295[4][1];
    int64_t **l_380 = (void*)0;
    int16_t l_423 = 0xAB04L;
    union U1 *l_444 = &g_445;
    int32_t l_470[4] = {0L,0L,0L,0L};
    uint16_t l_487 = 0x7825L;
    float *l_488 = (void*)0;
    uint32_t l_526 = 4294967293UL;
    uint32_t *l_687 = &g_445.f0;
    union U1 ***l_694 = &g_691;
    int8_t l_698[4];
    union U1 ****l_700 = &g_690[3];
    union U0 l_749 = {7UL};
    union U1 *l_785 = &g_445;
    uint32_t l_877[7][3];
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
            l_295[i][j] = 0x24C9B862L;
    }
    for (i = 0; i < 4; i++)
        l_698[i] = (-2L);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
            l_877[i][j] = 2UL;
    }
lbl_835:
    (*g_228) &= (((*l_227) = l_226) != (void*)0);
lbl_819:
    for (g_189 = 0; (g_189 > (-22)); g_189--)
    { /* block id: 100 */
        return g_5;
    }
    if ((((((safe_sub_func_uint16_t_u_u((p_153 | (safe_add_func_uint32_t_u_u(p_152, (l_237 | p_152)))), (l_238 != ((*l_240) = l_239)))) <= ((g_80 , (safe_lshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((((safe_rshift_func_int16_t_s_s(p_152, 6)) | p_152) | 0L), p_153)), 5))) != (*g_228))) || p_153) <= 0UL) & p_155))
    { /* block id: 104 */
        uint64_t l_249 = 0x73A4F682585733F8LL;
        int32_t l_264 = 1L;
        uint8_t ****l_267 = &l_239;
        int32_t l_289 = (-1L);
        int32_t l_291 = 0x7D5DAE99L;
        int32_t l_294[5][2][9] = {{{(-8L),0xF3A4FACBL,0x29910F59L,0x2C166499L,0x4790C7EEL,0x751EC9E2L,0x71989D66L,(-4L),(-10L)},{0xEE659F9DL,0x40E4FFF8L,0L,8L,1L,1L,8L,0L,0x40E4FFF8L}},{{0x4790C7EEL,(-4L),0xEA6F82FFL,0L,0x2A3B7F02L,0xEE53BEDFL,0L,0xE0167837L,(-10L)},{0xAA76A31EL,0L,0xAA76A31EL,0x2F2B0F47L,8L,0xEE659F9DL,0xA4BC359EL,0xA4BC359EL,0xEE659F9DL}},{{0xEA6F82FFL,(-4L),0x4790C7EEL,(-4L),0xEA6F82FFL,0L,0x2A3B7F02L,0xEE53BEDFL,0L},{0L,0x40E4FFF8L,0xEE659F9DL,0x2F2B0F47L,0xEE659F9DL,0x40E4FFF8L,0L,8L,1L}},{{0x29910F59L,0xF3A4FACBL,(-8L),0L,0x907BC61FL,0L,(-8L),0xF3A4FACBL,0x29910F59L},{0x40E4FFF8L,0x87262FD1L,0xA4BC359EL,8L,1L,0xEE659F9DL,1L,8L,0xA4BC359EL}},{{0x2A3B7F02L,0xE0167837L,0x9C52775EL,0x2C166499L,0x71989D66L,0xEE53BEDFL,0x29910F59L,0xEE53BEDFL,0x71989D66L},{0x40E4FFF8L,1L,1L,0x40E4FFF8L,0xAA76A31EL,1L,0x2F2B0F47L,0xA4BC359EL,0x2F2B0F47L}}};
        int64_t *l_303 = &g_13;
        int64_t *l_313 = &g_13;
        union U1 l_327 = {0x036A4EDCL};
        uint32_t l_356 = 0xDB2FAF04L;
        uint8_t l_388 = 247UL;
        uint32_t l_425 = 0x1023CFA5L;
        union U0 l_430 = {0x4E0C9C67L};
        float *l_435[8][4] = {{&g_405,&g_405,&g_405,(void*)0},{&g_405,&g_405,(void*)0,(void*)0},{&g_405,&g_405,&g_405,&g_405},{&g_405,&g_405,&g_405,&g_405},{&g_405,&g_405,(void*)0,&g_405},{&g_405,&g_405,&g_405,&g_405},{&g_405,&g_405,&g_405,&g_405},{&g_405,&g_405,&g_405,(void*)0}};
        int64_t l_460[5][7] = {{0L,0x232499634F65733ELL,(-1L),0x7C122F0C0610ABE2LL,0x4C5A0F89E9E98AA7LL,0x4C5A0F89E9E98AA7LL,(-1L)},{(-8L),0x5EDA3CF38AF548DCLL,(-8L),0x0C8677582B9D657FLL,(-1L),0x4C5A0F89E9E98AA7LL,0L},{0x5EDA3CF38AF548DCLL,1L,(-8L),0x3D6A094427D57D71LL,0L,0x3D6A094427D57D71LL,(-8L)},{(-1L),(-1L),0x3D6A094427D57D71LL,0x232499634F65733ELL,0x0E804B1561F89F00LL,0x4C5A0F89E9E98AA7LL,0x5EDA3CF38AF548DCLL},{0x232499634F65733ELL,(-1L),0x7C122F0C0610ABE2LL,0x4C5A0F89E9E98AA7LL,0x4C5A0F89E9E98AA7LL,0x7C122F0C0610ABE2LL,(-1L)}};
        uint64_t l_464 = 0x81764BC26CEEF8D5LL;
        int32_t *l_468 = (void*)0;
        int32_t *l_469[9] = {&l_289,&l_289,&l_289,&l_289,&l_289,&l_289,&l_289,&l_289,&l_289};
        int i, j, k;
        if ((((safe_rshift_func_int16_t_s_s((&g_21 != (void*)0), ((l_249 ^ (safe_lshift_func_int8_t_s_u(0x79L, 6))) >= g_224))) , (p_153 <= ((((safe_mul_func_int8_t_s_s((l_249 && (((*l_227) != (void*)0) <= g_192[1])), p_155)) < g_5) , &g_93) != &g_13))) < p_152))
        { /* block id: 105 */
            uint16_t *l_262[6];
            int32_t l_276 = (-4L);
            int32_t l_277 = 9L;
            int32_t l_286 = 1L;
            int32_t l_292[8] = {0x9D25AE42L,0xD1B4FF7CL,0x9D25AE42L,0x9D25AE42L,0xD1B4FF7CL,0x9D25AE42L,0x9D25AE42L,0xD1B4FF7CL};
            const int64_t *l_311 = &g_312[2];
            int16_t l_342 = (-6L);
            uint32_t l_343 = 0xB5822137L;
            float l_354 = 0x0.Ep+1;
            float l_395 = (-0x10.9p+1);
            uint32_t l_397 = 0xBD9BE98BL;
            int i;
            for (i = 0; i < 6; i++)
                l_262[i] = &g_263;
            if ((((safe_div_func_int64_t_s_s((safe_mul_func_int8_t_s_s(((*g_131) ^= p_152), 1L)), 0x16795591AD4FCD9ELL)) | p_152) , ((safe_div_func_int32_t_s_s((safe_mul_func_int16_t_s_s(0x0794L, (l_264 = 0x06E1L))), (p_152 | (safe_mul_func_int16_t_s_s(((void*)0 != l_267), 0xABB3L))))) == (*g_131))))
            { /* block id: 108 */
                int8_t l_270 = (-1L);
                int32_t l_281 = 8L;
                int32_t l_282 = (-5L);
                int32_t l_288 = 0x9E1F5F1FL;
                int32_t l_293 = 0x4653DEABL;
                int64_t **l_304 = &l_303;
                float l_315 = 0x1.7p+1;
                for (l_249 = 0; (l_249 > 37); l_249 = safe_add_func_int16_t_s_s(l_249, 1))
                { /* block id: 111 */
                    int32_t *l_271 = &g_189;
                    int32_t l_272 = (-2L);
                    int32_t *l_273 = &g_189;
                    int32_t *l_274[1][6][4] = {{{&l_264,&l_264,&l_264,&l_272},{(void*)0,&g_26,(void*)0,&l_264},{(void*)0,&l_264,&l_264,(void*)0},{&l_264,&l_264,&l_272,&l_264},{&l_264,&g_26,&l_272,&l_272},{&l_264,&l_264,&l_264,&l_272}}};
                    uint32_t l_278 = 18446744073709551615UL;
                    int64_t l_285 = 0x40700B3AA981A23DLL;
                    int i, j, k;
                    ++l_278;
                    l_295[0][0]--;
                }
                l_288 ^= (safe_mul_func_int8_t_s_s((((+p_152) >= (safe_rshift_func_int16_t_s_u((18446744073709551615UL <= (&g_93 == ((*l_304) = l_303))), (safe_add_func_int64_t_s_s(g_55, ((g_91[4][0][3] , (((safe_div_func_uint64_t_u_u((0L ^ (*g_191)), ((safe_div_func_int64_t_s_s(((l_311 = &g_93) == l_313), (*g_191))) & l_291))) , l_292[7]) != p_152)) || 0x3202B1D1L)))))) , 8L), l_281));
                (**l_227) = &l_291;
                for (l_249 = 1; (l_249 <= 6); l_249 += 1)
                { /* block id: 121 */
                    int32_t *l_316[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_316[i] = &l_292[5];
                    if ((*g_228))
                        break;
                    p_153 &= ((*g_128) = (**g_194));
                    if (((safe_mod_func_int64_t_s_s((l_282 >= (((((safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(0x5573L, (((void*)0 == &g_138) == (g_5 == ((((safe_rshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((l_327 , (p_153 && (((g_263 = 0xA935L) & (g_2[3] || p_155)) | p_155))), 5)), p_152)) <= p_152) > 0UL) , (*g_191)))))), l_286)) , g_2[0]) , p_155) , l_293) < (-7L))), l_292[7])) & 0UL))
                    { /* block id: 126 */
                        int32_t ****l_328 = &l_227;
                        (*l_328) = (void*)0;
                        if (p_152)
                            break;
                    }
                    else
                    { /* block id: 129 */
                        (**l_267) = &g_139[3][6][1];
                        return g_4;
                    }
                }
            }
            else
            { /* block id: 134 */
                int32_t *l_344 = &l_284;
                uint32_t *l_345 = (void*)0;
                uint32_t *l_346 = &l_327.f0;
                uint32_t *l_353[1];
                uint32_t l_355 = 1UL;
                int i;
                for (i = 0; i < 1; i++)
                    l_353[i] = &l_343;
                (*g_358) &= ((((*g_131) = ((safe_div_func_int8_t_s_s((safe_add_func_uint16_t_u_u((!(safe_mod_func_int64_t_s_s((l_292[6] >= ((((*l_313) = ((safe_div_func_uint32_t_u_u((safe_unary_minus_func_uint32_t_u((l_294[3][0][5] = (safe_sub_func_int8_t_s_s((+((((*l_346) = (l_342 | ((*l_344) = l_343))) , ((safe_mod_func_uint32_t_u_u(g_263, ((*l_346) = (safe_div_func_int32_t_s_s(((*g_191) < (0xACL ^ (safe_sub_func_uint16_t_u_u((g_13 ^ l_292[5]), ((p_153 , l_294[0][1][3]) ^ 0x67FE72FF66831ED0LL))))), g_5))))) , 4L)) , p_152)), 0x30L))))), p_153)) >= g_18[1])) || (*g_191)) ^ l_355)), l_356))), p_152)), p_155)) , 0x70L)) || l_356) < p_155);
            }
            (*l_226) = &l_292[4];
            if ((safe_unary_minus_func_uint16_t_u((l_264 , ((safe_mod_func_uint8_t_u_u((safe_div_func_int16_t_s_s((safe_add_func_int8_t_s_s((((((safe_mod_func_int64_t_s_s(((safe_rshift_func_int8_t_s_s(((safe_mod_func_uint16_t_u_u((((*g_131) = 0x21L) , ((((l_294[0][0][0] <= (**l_226)) | (safe_rshift_func_int16_t_s_s((p_153 && (safe_add_func_int32_t_s_s((g_18[1] ^ g_80), p_153))), (safe_div_func_uint8_t_u_u(p_153, p_153))))) & 0UL) == l_342)), 0xD94CL)) , p_153), 0)) || (-1L)), g_312[2])) | (-3L)) , g_192[1]) < 0xEAE4L) != g_263), g_192[1])), p_155)), l_292[7])) , 0x72F1L)))))
            { /* block id: 145 */
                return g_3;
            }
            else
            { /* block id: 147 */
                int32_t *l_385 = &l_291;
                int32_t *l_386 = &l_294[0][1][7];
                int32_t *l_387[6][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275,&l_275}};
                int8_t l_394 = (-10L);
                float l_396 = (-0x9.Ep+1);
                int i, j;
                for (l_356 = 0; (l_356 <= 2); l_356 += 1)
                { /* block id: 150 */
                    int32_t *l_379 = (void*)0;
                    int64_t ***l_381 = &l_380;
                    int i;
                    g_314[l_356] = (g_378 , l_379);
                    (*g_382) = ((*l_381) = l_380);
                }
                l_388++;
                for (l_327.f0 = 2; (l_327.f0 <= 7); l_327.f0 += 1)
                { /* block id: 158 */
                    uint8_t l_391[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_391[i] = 7UL;
                    l_391[1]--;
                    for (l_342 = 2; (l_342 <= 7); l_342 += 1)
                    { /* block id: 162 */
                        int i;
                        (*l_226) = &l_292[l_327.f0];
                    }
                }
                --l_397;
            }
        }
        else
        { /* block id: 168 */
            int32_t *l_402 = &l_284;
            int32_t l_419 = (-3L);
            int32_t l_420[2][9] = {{0x9B70DB72L,0xC78956A6L,0x46D06F69L,0x9B70DB72L,0x46D06F69L,0xC78956A6L,0x9B70DB72L,0x5B2FF600L,0x5B2FF600L},{0x9B70DB72L,0xC78956A6L,0x46D06F69L,0x9B70DB72L,0x46D06F69L,0xC78956A6L,0x9B70DB72L,0x5B2FF600L,0x5B2FF600L}};
            float l_422 = 0x2.3p+1;
            int16_t l_424 = 0xBA15L;
            const uint8_t *l_433 = (void*)0;
            const uint8_t **l_432 = &l_433;
            const uint8_t ***l_431 = &l_432;
            int64_t * const *l_454 = (void*)0;
            int64_t * const **l_453 = &l_454;
            int i, j;
            for (g_189 = 0; (g_189 == 17); g_189++)
            { /* block id: 171 */
                int32_t *l_403[1][10][2] = {{{&l_283,&l_290},{&l_283,&l_283},{&l_290,&l_283},{&l_283,&l_290},{&l_283,&l_283},{&l_290,&l_283},{&l_283,&l_290},{&l_283,&l_283},{&l_290,&l_283},{&l_283,&l_290}}};
                float *l_404 = &g_405;
                int i, j, k;
                (*l_404) = (((l_402 = &p_153) == l_403[0][7][0]) < ((void*)0 != (*l_227)));
                if (p_152)
                    continue;
            }
            if (p_155)
            { /* block id: 176 */
                uint8_t l_408 = 1UL;
                uint64_t *l_413 = &l_249;
                int32_t *l_414 = &l_289;
                (*l_414) &= ((+((((*g_131) , ((safe_unary_minus_func_int32_t_s(9L)) ^ 0L)) != ((*g_191) = ((l_408 | ((safe_add_func_int8_t_s_s((safe_mod_func_uint8_t_u_u(((((*l_413) |= (*g_191)) || (l_249 ^ (((65535UL || p_155) ^ ((l_408 < 4294967292UL) || p_155)) , 0x96L))) < 1UL), p_153)), l_408)) | p_155)) || p_155))) || l_264)) & p_155);
                return l_294[4][1][7];
            }
            else
            { /* block id: 181 */
                int32_t *l_415 = (void*)0;
                int32_t *l_416 = &l_275;
                int32_t *l_417 = &l_284;
                int32_t *l_418[10][8] = {{&l_264,&l_283,(void*)0,&l_294[3][0][5],&g_142,&g_5,(void*)0,&l_294[3][0][5]},{&g_5,&l_284,&l_284,&l_283,&l_290,&l_290,&l_283,&l_284},{&g_5,&g_5,&l_291,&l_294[3][1][8],(void*)0,(void*)0,&l_294[2][0][2],&l_287},{&l_294[3][1][8],&l_294[3][0][5],&l_287,(void*)0,&l_284,&l_291,(void*)0,&l_283},{(void*)0,&g_26,&g_189,&g_142,(void*)0,&l_284,&l_294[3][0][5],&l_287},{(void*)0,&g_189,(void*)0,&l_290,&l_294[3][1][8],&l_290,(void*)0,&g_189},{(void*)0,&g_142,&l_290,(void*)0,&l_284,&g_26,&l_294[3][0][5],(void*)0},{&l_294[3][0][5],(void*)0,&l_294[3][1][8],&l_284,(void*)0,(void*)0,&l_294[3][0][5],&l_294[3][1][2]},{&l_291,&l_284,&l_290,&g_26,(void*)0,&l_294[3][0][5],(void*)0,&l_294[3][0][5]},{(void*)0,&l_294[3][0][5],(void*)0,&l_294[3][0][5],&l_294[3][0][5],(void*)0,&l_294[3][0][5],(void*)0}};
                int32_t l_421 = 0x9E232B54L;
                const uint8_t ****l_434 = &l_431;
                int16_t *l_438 = &l_423;
                uint32_t l_447 = 0x506C693FL;
                int i, j;
                l_425--;
                if ((((p_155 = (((*l_438) = ((safe_sub_func_uint8_t_u_u((l_430 , p_155), ((((*l_434) = l_431) != ((((l_435[2][2] == ((safe_mul_func_float_f_f(((void*)0 != &p_153), l_356)) , l_435[2][1])) != g_142) || (*l_416)) , (*l_240))) && p_152))) || 0x414942E24EE99C37LL)) <= (*l_402))) && 0L) <= (*g_131)))
                { /* block id: 186 */
                    uint64_t l_441 = 18446744073709551610UL;
                    int32_t l_446 = 0L;
                    for (g_13 = 11; (g_13 >= (-14)); g_13 = safe_sub_func_int8_t_s_s(g_13, 2))
                    { /* block id: 189 */
                        return p_152;
                    }
                    for (l_287 = 2; (l_287 >= 0); l_287 -= 1)
                    { /* block id: 194 */
                        return l_441;
                    }
                    if (l_249)
                    { /* block id: 197 */
                        union U1 *l_442 = &l_327;
                        (*l_226) = &p_153;
                        l_444 = l_442;
                        --l_447;
                    }
                    else
                    { /* block id: 201 */
                        return l_249;
                    }
                    (*g_128) = (*g_228);
                }
                else
                { /* block id: 205 */
                    int64_t ***l_452 = &g_383[7];
                    int32_t l_455 = (-1L);
                    int32_t l_461 = 0xB7B12D7AL;
                    int32_t l_462 = 0x232CE009L;
                    int32_t l_463 = 0x4FA28D64L;
                    g_405 = (safe_add_func_float_f_f(((l_455 = (l_452 != l_453)) > (safe_div_func_float_f_f((-0x8.8p+1), (safe_div_func_float_f_f(l_388, 0xF.BD7C55p+51))))), p_153));
                    ++l_464;
                    return p_155;
                }
                g_467 = &g_378;
            }
        }
        ++g_471;
    }
    else
    { /* block id: 215 */
        float *l_489 = &g_405;
        float *l_490 = (void*)0;
        int32_t l_491 = 1L;
        const union U1 l_508 = {0xE6B3A94DL};
        int32_t l_536[9][7] = {{0x948DC50EL,0x932D0982L,0x948DC50EL,0x948DC50EL,0x932D0982L,0x948DC50EL,0x948DC50EL},{0x7B833C42L,0x7B833C42L,3L,0x7B833C42L,0x7B833C42L,3L,0x7B833C42L},{0x932D0982L,0x948DC50EL,0x948DC50EL,0x932D0982L,0x948DC50EL,0x948DC50EL,0x932D0982L},{7L,0x7B833C42L,7L,7L,0x7B833C42L,7L,7L},{0x932D0982L,0x932D0982L,1L,0x932D0982L,0x932D0982L,1L,0x932D0982L},{0x7B833C42L,7L,7L,0x7B833C42L,7L,7L,0x7B833C42L},{0x948DC50EL,0x932D0982L,0x948DC50EL,0x948DC50EL,0x932D0982L,0x948DC50EL,0x948DC50EL},{0x7B833C42L,0x7B833C42L,3L,0x7B833C42L,0x7B833C42L,3L,0x7B833C42L},{0x932D0982L,0x948DC50EL,0x948DC50EL,0x932D0982L,0x948DC50EL,0x948DC50EL,0x932D0982L}};
        union U1 **l_560 = &l_444;
        union U1 ***l_559[3][7] = {{&l_560,&l_560,&l_560,&l_560,&l_560,&l_560,&l_560},{&l_560,&l_560,&l_560,&l_560,&l_560,&l_560,&l_560},{&l_560,&l_560,&l_560,&l_560,&l_560,&l_560,&l_560}};
        union U0 l_578 = {0x0D8334A9L};
        const int32_t l_585 = 0x288424DBL;
        int32_t * const * const l_620 = &g_547;
        int32_t * const * const *l_619 = &l_620;
        int32_t * const * const **l_618[10];
        uint8_t * const *l_628 = &g_139[1][6][2];
        uint8_t * const * const *l_627 = &l_628;
        uint8_t * const * const **l_626 = &l_627;
        int8_t *l_720 = (void*)0;
        uint16_t *l_783 = &l_487;
        uint16_t **l_897[10] = {&g_498,&g_498,&g_498,&g_498,&g_498,&g_498,&g_498,&g_498,&g_498,&g_498};
        uint16_t ***l_896 = &l_897[4];
        uint8_t ****l_932 = &l_239;
        float l_958 = 0xB.46BA03p+15;
        int i, j;
        for (i = 0; i < 10; i++)
            l_618[i] = &l_619;
lbl_823:
        if ((safe_lshift_func_uint16_t_u_u((0x4818L && (-4L)), (((safe_sub_func_uint8_t_u_u((((safe_div_func_float_f_f((safe_add_func_float_f_f(g_18[2], ((safe_sub_func_float_f_f((!(p_154 = (((g_312[4] > (((((*l_489) = ((safe_mul_func_float_f_f(p_155, p_155)) <= ((((l_487 , p_152) || 4294967295UL) , l_488) != l_488))) <= (-0x3.Cp-1)) != p_153) < p_153)) == g_13) == g_18[0]))), 0x5.856934p+99)) <= g_4))), g_189)) , l_491) <= p_155), 0x90L)) , p_152) && l_491))))
        { /* block id: 218 */
            int16_t l_504 = 3L;
            float *l_511 = &g_405;
            int32_t l_529 = 0x10AABA71L;
            int32_t l_538 = 0x234B0ED8L;
            int32_t l_540 = 0xB4DE9221L;
            uint16_t l_542 = 65535UL;
            const union U0 *l_595 = (void*)0;
            int64_t *l_653[9][4][7] = {{{&g_13,&g_13,(void*)0,&g_93,&g_13,&g_93,&g_93},{&g_93,&g_13,&g_13,&g_13,&g_13,&g_13,&g_13},{&g_13,&g_13,&g_93,&g_13,&g_13,&g_93,&g_13},{&g_13,&g_13,&g_13,&g_93,&g_13,&g_93,&g_13}},{{&g_13,&g_13,&g_13,&g_93,&g_13,&g_93,&g_93},{&g_13,&g_93,&g_13,&g_93,&g_93,&g_93,&g_93},{&g_13,&g_13,&g_13,&g_93,&g_13,&g_13,&g_13},{&g_13,&g_13,&g_13,&g_93,&g_13,&g_93,&g_13}},{{&g_13,&g_13,&g_13,&g_93,&g_93,&g_13,&g_93},{&g_93,&g_13,&g_93,&g_93,&g_93,&g_93,&g_13},{&g_13,&g_13,&g_13,&g_93,&g_93,&g_93,&g_93},{&g_93,&g_13,&g_13,&g_13,&g_13,&g_13,&g_93}},{{&g_93,&g_93,&g_93,&g_93,&g_13,&g_13,&g_13},{&g_13,&g_93,&g_93,&g_93,&g_93,&g_13,&g_93},{&g_93,&g_13,&g_93,&g_93,&g_13,&g_13,&g_13},{&g_13,&g_93,&g_13,&g_93,&g_13,&g_13,&g_13}},{{&g_13,&g_13,&g_13,&g_93,(void*)0,(void*)0,(void*)0},{&g_13,&g_93,&g_93,&g_13,&g_93,&g_13,&g_13},{&g_13,&g_93,&g_13,&g_93,&g_13,&g_93,&g_13},{&g_13,&g_13,&g_13,&g_13,&g_13,&g_93,&g_13}},{{&g_93,&g_13,(void*)0,&g_13,&g_93,&g_13,(void*)0},{&g_13,&g_13,&g_93,&g_93,&g_93,&g_93,&g_13},{&g_93,&g_13,&g_13,&g_13,&g_13,&g_93,&g_13},{&g_93,&g_13,&g_13,&g_13,&g_93,&g_13,&g_93}},{{&g_13,(void*)0,&g_93,&g_13,&g_93,(void*)0,&g_13},{&g_93,&g_13,&g_93,&g_13,&g_13,&g_13,&g_93},{&g_13,&g_93,&g_13,&g_13,&g_13,&g_13,&g_93},{&g_13,&g_93,&g_93,&g_93,&g_93,&g_13,&g_13}},{{(void*)0,&g_13,&g_93,&g_13,(void*)0,&g_13,&g_93},{&g_13,&g_93,&g_13,&g_13,&g_13,&g_13,&g_13},{&g_13,&g_93,&g_13,&g_93,&g_13,&g_93,&g_13},{&g_13,&g_13,&g_93,&g_13,&g_93,&g_93,&g_13}},{{(void*)0,(void*)0,(void*)0,&g_93,&g_13,&g_13,&g_13},{&g_13,&g_13,&g_13,&g_93,&g_13,&g_13,&g_93},{&g_13,&g_13,&g_93,&g_93,&g_93,&g_93,&g_93},{&g_13,&g_93,&g_13,&g_93,&g_93,&g_13,&g_93}}};
            uint64_t *l_673 = (void*)0;
            uint64_t *l_674 = (void*)0;
            uint64_t *l_675 = &g_676;
            int i, j, k;
            for (l_237 = 0; (l_237 >= (-9)); --l_237)
            { /* block id: 221 */
                uint16_t *l_497 = &l_487;
                uint16_t **l_496[3];
                int32_t l_501 = 8L;
                int32_t l_537 = 0x3C537C6FL;
                int32_t l_539 = 0L;
                int32_t l_541[4][3] = {{1L,0x511A2537L,0x4ADD6E07L},{1L,1L,0x511A2537L},{0x3A411DE1L,0x511A2537L,0x511A2537L},{0x511A2537L,0xB297F468L,0x4ADD6E07L}};
                union U0 l_594[1][4] = {{{0x19E80E69L},{0x19E80E69L},{0x19E80E69L},{0x19E80E69L}}};
                uint8_t * const * const ***l_629 = &l_626;
                int8_t *l_630 = (void*)0;
                int8_t *l_631[5][9] = {{&g_54,&g_54,(void*)0,&g_54,&g_54,&g_54,(void*)0,(void*)0,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,(void*)0,&g_54},{&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54,&g_54},{&g_54,(void*)0,(void*)0,(void*)0,(void*)0,&g_54,&g_54,&g_54,&g_54},{&g_54,(void*)0,&g_54,&g_54,&g_54,&g_54,(void*)0,&g_54,&g_54}};
                int i, j;
                for (i = 0; i < 3; i++)
                    l_496[i] = &l_497;
                if ((safe_lshift_func_int16_t_s_u((0xF1L == ((((((g_498 = &g_263) == &g_263) , (safe_mul_func_float_f_f((l_501 == ((-0x3.Fp-1) < (g_378.f0 >= (g_192[1] != (-0x1.Dp-1))))), (safe_mul_func_float_f_f(g_192[2], l_501))))) , p_155) , l_504) || 1L)), l_504)))
                { /* block id: 223 */
                    uint8_t *l_512 = &g_513[7][1][1];
                    int32_t l_520 = 0xC0CF565CL;
                    int64_t *l_523 = &g_13;
                    int32_t *l_527 = (void*)0;
                    int32_t *l_528 = &l_491;
                    int32_t *l_545 = &l_501;
                    uint8_t l_590 = 254UL;
                    int16_t *l_591 = (void*)0;
                    int16_t *l_592 = &g_224;
                    int16_t *l_596 = &l_504;
                    int8_t *l_599[5][8][3] = {{{&g_54,&g_54,&g_54},{&g_54,(void*)0,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{(void*)0,&g_54,&g_54},{&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,(void*)0},{&g_54,&g_54,(void*)0},{(void*)0,(void*)0,&g_54},{&g_54,&g_54,&g_54},{&g_54,(void*)0,&g_54},{&g_54,&g_54,&g_54}},{{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{(void*)0,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,(void*)0}},{{&g_54,&g_54,(void*)0},{(void*)0,(void*)0,&g_54},{&g_54,&g_54,&g_54},{&g_54,(void*)0,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54}},{{(void*)0,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,&g_54},{&g_54,&g_54,(void*)0},{&g_54,&g_54,(void*)0},{&g_54,(void*)0,&g_54},{&g_54,&g_54,&g_54}}};
                    int i, j, k;
                    if (((*l_528) = (l_491 ^ (((((safe_unary_minus_func_int32_t_s((((safe_sub_func_uint16_t_u_u((l_508 , ((*l_497) = (safe_sub_func_uint8_t_u_u(((*l_512) = (l_488 != l_511)), p_152)))), (safe_mul_func_int16_t_s_s((safe_mod_func_int64_t_s_s((safe_add_func_uint16_t_u_u(((*g_498)++), (((p_152 , ((((*l_523) &= 1L) > ((safe_rshift_func_uint8_t_u_s(l_526, (*g_131))) <= p_152)) | p_155)) != l_501) < l_504))), l_491)), p_155)))) == p_153) == p_155))) , (*g_498)) , p_152) || g_192[1]) | p_153))))
                    { /* block id: 229 */
                        int32_t *l_530 = (void*)0;
                        int32_t *l_531 = &l_501;
                        int32_t *l_532 = &l_529;
                        int32_t *l_533 = &g_189;
                        int32_t *l_534 = (void*)0;
                        int32_t *l_535[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_535[i] = (void*)0;
                        l_542++;
                        (*g_546) = ((*l_226) = l_545);
                    }
                    else
                    { /* block id: 233 */
                        if ((*g_358))
                            break;
                    }
                    for (l_542 = 0; (l_542 <= 2); l_542 += 1)
                    { /* block id: 238 */
                        int i;
                        if (p_152)
                            break;
                    }
                    for (l_501 = 12; (l_501 < 19); l_501 = safe_add_func_int16_t_s_s(l_501, 1))
                    { /* block id: 243 */
                        int32_t l_557 = 0L;
                        int32_t *l_558 = &l_536[5][5];
                        union U1 ****l_561 = (void*)0;
                        union U1 ****l_562 = (void*)0;
                        union U1 ****l_563 = &l_559[2][6];
                        (*l_558) ^= (safe_mul_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(l_538, (((safe_rshift_func_uint16_t_u_s(p_152, 15)) | ((void*)0 != &g_357[0])) , ((p_155 > (((g_443 != (void*)0) || ((((+g_2[0]) > (((l_540 , 0x90L) | l_504) == g_312[5])) > (*g_191)) || l_491)) | p_153)) && l_557)))), l_541[0][2]));
                        (*l_563) = l_559[0][5];
                        (**l_227) = &p_153;
                        l_284 |= ((safe_rshift_func_int16_t_s_u(g_2[3], 4)) == ((safe_add_func_int8_t_s_s((p_152 >= 0xE1L), (*l_558))) || (g_189 != ((safe_mul_func_int8_t_s_s((0x472D5C35L ^ (((safe_mul_func_int16_t_s_s((((*l_497)--) >= (((safe_sub_func_int32_t_s_s((safe_add_func_uint64_t_u_u(((-7L) | p_153), ((l_578 , p_152) , (*g_191)))), (*l_528))) , p_152) | (-8L))), l_538)) >= 0x50BF7C3418F8445BLL) && g_192[1])), (*g_131))) , p_152))));
                    }
                    (*l_528) = (((safe_mod_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_u((((l_540 = (safe_sub_func_int32_t_s_s(l_585, (l_539 <= (safe_mul_func_uint8_t_u_u(((safe_div_func_int16_t_s_s(((*l_592) = l_590), (+(*g_498)))) <= (((*l_596) = (((*g_131) = ((l_594[0][3] , l_595) == &g_91[3][0][4])) , g_189)) , ((((*g_191) = (safe_div_func_int16_t_s_s((-1L), l_508.f0))) || 1UL) | l_538))), p_155)))))) , p_153) || p_152), l_536[2][2])) >= p_155), l_578.f0)) && 0xFCC8L) > p_152);
                }
                else
                { /* block id: 256 */
                    if (p_152)
                        break;
                }
                if (((l_541[3][2] ^= (safe_mod_func_int32_t_s_s((((p_155 &= (((safe_sub_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((safe_add_func_int32_t_s_s(((safe_rshift_func_int8_t_s_s((((g_610 == &g_611[6]) , l_511) == ((safe_add_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((0xAC4AL && (l_618[7] != g_621)), (safe_sub_func_uint64_t_u_u((0UL || (((l_539 <= ((*g_131) = ((((*l_629) = l_626) == &g_611[2]) , p_152))) && p_153) <= p_152)), p_153)))), 0xDAE4L)) , (void*)0)), 7)) != l_501), g_513[7][1][1])), 0)), p_153)) != p_152) == 0xC8962224L)) , l_594[0][3].f0) >= 0xB2L), (-1L)))) & 0x8BL))
                { /* block id: 263 */
                    int16_t *l_639[2];
                    int32_t l_642 = 4L;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_639[i] = &l_504;
                    for (g_445.f0 = 0; (g_445.f0 <= 59); g_445.f0 = safe_add_func_int64_t_s_s(g_445.f0, 3))
                    { /* block id: 266 */
                        uint8_t l_634 = 0x25L;
                        if ((*g_228))
                            break;
                        return l_634;
                    }
                    l_642 &= (safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(((g_224 &= 4L) < (l_541[1][1] == ((void*)0 != &g_91[1][0][2]))), 0)), ((*l_497) = ((p_152 , l_540) > (&l_578 != (g_640 = &g_91[3][0][4]))))));
                    (*l_226) = &l_642;
                    for (l_501 = (-14); (l_501 <= 18); l_501 = safe_add_func_uint32_t_u_u(l_501, 5))
                    { /* block id: 277 */
                        const int32_t *l_658[5][2][8] = {{{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,(void*)0,&g_659},{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,&g_659,&g_659}},{{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,(void*)0,&g_659},{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,&g_659,&g_659}},{{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,(void*)0,&g_659},{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,&g_659,&g_659}},{{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,(void*)0,&g_659},{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,&g_659,&g_659}},{{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,(void*)0,&g_659},{&g_659,&g_659,&g_659,(void*)0,&g_659,&g_659,&g_659,&g_659}}};
                        const int32_t **l_657 = &l_658[2][1][7];
                        const int32_t ***l_656 = &l_657;
                        int i, j, k;
                        (***l_227) = (safe_lshift_func_uint16_t_u_u(((safe_div_func_int8_t_s_s(((((((***l_227) <= ((safe_add_func_float_f_f((safe_div_func_float_f_f(((((((void*)0 == l_653[4][0][0]) , ((l_529 = (safe_mul_func_float_f_f((((*l_656) = (void*)0) != (void*)0), l_539))) != l_504)) < ((void*)0 != (*l_629))) < g_142) >= l_642), 0x4.6p+1)), p_154)) , 254UL)) , p_153) || 0x7A92FF0EFF1DA59FLL) & 0x76380750L) || p_152), p_155)) < p_153), (*g_498)));
                    }
                }
                else
                { /* block id: 282 */
                    uint16_t l_662 = 65535UL;
                    for (l_539 = 0; (l_539 >= 12); l_539++)
                    { /* block id: 285 */
                        float **l_666 = &l_511;
                        float ***l_665 = &l_666;
                        ++l_662;
                        if (p_155)
                            continue;
                        (*l_665) = (void*)0;
                    }
                    (*l_511) = g_513[2][3][4];
                }
                p_153 = p_155;
            }
            p_153 ^= (safe_rshift_func_uint16_t_u_s(0x8BC8L, (safe_mul_func_uint8_t_u_u(p_152, ((((p_155 && (safe_mul_func_int16_t_s_s(((((*l_675) = ((*g_191) = (0UL >= 1L))) ^ (safe_add_func_int32_t_s_s((((safe_rshift_func_uint8_t_u_s(((safe_mul_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(((p_152 | (g_471 != (((*l_444) = g_445) , ((l_687 = l_489) == l_511)))) && (-1L)), 1UL)), g_54)) < g_513[2][2][4]), (*g_131))) >= l_540) , p_152), (-5L)))) | p_152), g_55))) > 0UL) & 0xBED7005C5C18FDFFLL) != p_152)))));
            (**l_227) = &p_153;
        }
        else
        { /* block id: 300 */
            union U1 ****l_693 = &g_690[5];
            union U1 ****l_695 = &l_559[0][5];
            int32_t l_726[8] = {0x2248B7DBL,0x2248B7DBL,1L,0x2248B7DBL,0x2248B7DBL,1L,0x2248B7DBL,0x2248B7DBL};
            int i;
            for (l_423 = 0; (l_423 == 10); l_423 = safe_add_func_int32_t_s_s(l_423, 9))
            { /* block id: 303 */
                l_470[2] = p_153;
            }
            (*g_228) ^= (p_152 > (((*l_693) = g_690[3]) != ((*l_695) = l_694)));
            if ((l_698[0] = (safe_sub_func_int64_t_s_s(((((void*)0 != (*l_619)) , (*l_560)) != (*l_560)), (0x5759C0FCL <= 0x3AB6A243L)))))
            { /* block id: 310 */
                union U1 *****l_699 = &l_693;
                const uint8_t *l_710 = &g_80;
                const uint8_t **l_709 = &l_710;
                const uint8_t ***l_708 = &l_709;
                const uint8_t ****l_707 = &l_708;
                const uint8_t *****l_706[5];
                int64_t *l_717 = &g_13;
                int64_t *l_718 = &g_719;
                float l_723 = 0x1.4p+1;
                int16_t l_724 = 0x96BFL;
                uint16_t l_725 = 65535UL;
                int i;
                for (i = 0; i < 5; i++)
                    l_706[i] = &l_707;
                l_287 ^= (0L > (((g_471 , ((*l_699) = &g_690[2])) == l_700) > (p_153 = (safe_mul_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(((*l_718) |= ((*l_717) &= (safe_unary_minus_func_uint16_t_u(((g_711 = l_706[1]) != (void*)0))))), ((((**l_560) , ((1UL > (&g_54 != l_720)) <= p_155)) , 7UL) >= (-1L)))), 0L)))));
                for (g_719 = (-10); (g_719 == 13); g_719 = safe_add_func_int32_t_s_s(g_719, 1))
                { /* block id: 319 */
                    return l_724;
                }
                return l_725;
            }
            else
            { /* block id: 323 */
                ++g_727;
                return p_155;
            }
        }
        for (l_491 = 0; (l_491 > 26); l_491 = safe_add_func_uint32_t_u_u(l_491, 1))
        { /* block id: 330 */
            uint64_t l_732[7][6] = {{1UL,0UL,18446744073709551615UL,18446744073709551615UL,0UL,1UL},{5UL,1UL,18446744073709551615UL,1UL,5UL,5UL},{0x0316FAC99894412FLL,1UL,1UL,0x0316FAC99894412FLL,0UL,0x0316FAC99894412FLL},{0x0316FAC99894412FLL,0UL,0x0316FAC99894412FLL,1UL,1UL,0x0316FAC99894412FLL},{5UL,5UL,1UL,18446744073709551615UL,1UL,5UL},{1UL,0UL,18446744073709551615UL,18446744073709551615UL,0UL,1UL},{5UL,1UL,18446744073709551615UL,1UL,5UL,5UL}};
            union U1 *l_780[7] = {(void*)0,&g_445,(void*)0,(void*)0,&g_445,(void*)0,(void*)0};
            int32_t *l_782 = &g_26;
            uint16_t *l_784 = &g_263;
            int64_t ***l_799 = &g_383[7];
            int64_t ****l_798 = &l_799;
            uint16_t **l_813 = (void*)0;
            uint16_t ***l_812 = &l_813;
            int32_t l_866 = (-3L);
            int32_t l_868 = 0xB6B953E9L;
            int32_t l_870[2][9] = {{0x8DCC146DL,0x8DCC146DL,0xB864951EL,0x8DCC146DL,0x8DCC146DL,0xB864951EL,0x8DCC146DL,0x8DCC146DL,0xB864951EL},{0x8DCC146DL,0x8DCC146DL,0xB864951EL,0x8DCC146DL,0x8DCC146DL,0xB864951EL,0x8DCC146DL,0x8DCC146DL,0xB864951EL}};
            union U0 l_905[3] = {{1UL},{1UL},{1UL}};
            int i, j;
            l_732[0][5]--;
            for (g_189 = 8; (g_189 > (-10)); g_189 = safe_sub_func_uint8_t_u_u(g_189, 4))
            { /* block id: 334 */
                uint16_t l_737 = 0x194CL;
                uint16_t *l_744[10];
                int32_t l_758 = 0x449917C3L;
                int8_t *l_759 = &g_54;
                int8_t *l_760 = &l_698[2];
                union U1 *l_761 = &g_445;
                int8_t l_779 = 0L;
                int i;
                for (i = 0; i < 10; i++)
                    l_744[i] = &l_737;
                ++l_737;
            }
            if (((0x379AL != p_153) & (((l_783 != l_784) , (((*l_782) & (((*l_560) = (*g_691)) == (l_785 = (***l_700)))) <= 0x22B261CBL)) | 0L)))
            { /* block id: 356 */
                union U1 *l_797 = (void*)0;
                int32_t l_802[6];
                uint16_t ***l_811 = (void*)0;
                int i;
                for (i = 0; i < 6; i++)
                    l_802[i] = 0x392EA0C9L;
                for (p_155 = 0; (p_155 != 18); p_155 = safe_add_func_int8_t_s_s(p_155, 9))
                { /* block id: 359 */
                    uint8_t *l_800 = (void*)0;
                    uint8_t *l_801[8][8] = {{(void*)0,&g_513[5][2][1],&g_80,&g_80,&g_513[5][2][1],(void*)0,&g_513[5][2][1],&g_80},{&g_513[7][1][1],&g_513[5][2][1],&g_513[7][1][1],(void*)0,(void*)0,&g_513[7][1][1],&g_513[5][2][1],&g_513[7][1][1]},{&g_513[7][1][1],(void*)0,&g_80,(void*)0,&g_513[7][1][1],&g_513[7][1][1],(void*)0,&g_80},{&g_513[7][1][1],&g_513[7][1][1],&g_513[7][1][1],&g_513[5][2][1],&g_513[7][1][1],(void*)0,(void*)0,&g_513[7][1][1]},{&g_80,&g_513[7][1][1],&g_513[7][1][1],&g_80,&g_513[7][1][1],&g_80,&g_513[7][1][1],&g_513[7][1][1]},{&g_513[7][1][1],&g_513[7][1][1],&g_513[5][2][1],&g_513[5][2][1],&g_513[7][1][1],&g_513[7][1][1],&g_513[7][1][1],&g_513[5][2][1]},{&g_80,&g_513[7][1][1],&g_80,&g_513[7][1][1],&g_513[7][1][1],&g_80,&g_513[7][1][1],&g_80},{(void*)0,&g_513[7][1][1],&g_513[5][2][1],&g_513[7][1][1],(void*)0,(void*)0,&g_513[7][1][1],&g_513[5][2][1]}};
                    int i, j;
                    for (g_224 = 0; (g_224 <= 6); g_224 += 1)
                    { /* block id: 362 */
                        int64_t *** volatile *l_790 = (void*)0;
                        int64_t *** volatile **l_789 = &l_790;
                        (*l_789) = &g_382;
                        if (p_155)
                            continue;
                    }
                    (***l_227) = (safe_div_func_int32_t_s_s(((((safe_add_func_uint64_t_u_u(((l_797 == ((*l_560) = (void*)0)) != (((((((l_802[0] = (((l_798 != &g_382) & p_153) >= (4UL != p_152))) | (safe_add_func_int32_t_s_s(p_155, ((safe_add_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((((safe_sub_func_uint32_t_u_u(((0x65B65ED7L > 0x1969935DL) & p_155), 0x2CDE89AFL)) == g_18[1]) < (*l_782)), 6)), (*l_782))) <= (-6L))))) || g_312[2]) != p_153) <= g_13) >= (***l_227)) >= (*g_191))), p_155)) , p_155) < (*g_131)) | (**l_226)), (**g_194)));
                }
                l_812 = l_811;
            }
            else
            { /* block id: 371 */
                float l_817 = 0x2.5CF491p-5;
                int32_t l_818[10] = {7L,0xACFB99CBL,0xACFB99CBL,7L,0xACFB99CBL,0xACFB99CBL,7L,0xACFB99CBL,0xACFB99CBL,7L};
                int32_t l_867 = 0x935938A6L;
                int32_t *l_885 = &g_18[1];
                int32_t l_890 = 0L;
                int i;
                if ((~((*g_640) , (0x6FL < (l_818[5] = 0UL)))))
                { /* block id: 373 */
                    if (l_578.f0)
                        goto lbl_819;
                    for (g_263 = 0; (g_263 == 39); g_263++)
                    { /* block id: 377 */
                        const union U0 l_822 = {5UL};
                        (*l_226) = (l_822 , &l_818[4]);
                        if (l_487)
                            goto lbl_819;
                        return p_155;
                    }
                    if (l_487)
                        goto lbl_823;
                }
                else
                { /* block id: 383 */
                    int32_t ***l_834 = &g_127[1];
                    union U0 l_849[4][9][6] = {{{{1UL},{0x76A306C2L},{0xEE483976L},{4294967295UL},{0x2888F81FL},{0xA2998466L}},{{0xB0DCB7F1L},{0xDEB8809BL},{0UL},{4294967295UL},{4294967295UL},{0x1E24F394L}},{{0xB0DCB7F1L},{0x28809CBBL},{4294967295UL},{4294967295UL},{0UL},{8UL}},{{1UL},{0x2888F81FL},{0xDA4E9AB5L},{4294967295UL},{4294967295UL},{0x22910943L}},{{1UL},{1UL},{0x6664E41CL},{4294967289UL},{1UL},{0x279C7684L}},{{0xEC650C26L},{4294967295UL},{4294967288UL},{0x038989D5L},{0x279C7684L},{4294967295UL}},{{0xA2998466L},{4294967295UL},{4294967288UL},{0UL},{0xE238D409L},{0UL}},{{8UL},{4294967289UL},{0xEE483976L},{8UL},{0xEE483976L},{4294967289UL}},{{0UL},{0xA9CA9BF1L},{0xC24095F3L},{4294967295UL},{4294967288UL},{4294967288UL}}},{{{1UL},{0UL},{4294967295UL},{4294967295UL},{0UL},{0xE238D409L}},{{0xA9CA9BF1L},{0UL},{8UL},{0UL},{4294967288UL},{0x22910943L}},{{4294967295UL},{0xA9CA9BF1L},{0x01EAE450L},{0xA2998466L},{0xEE483976L},{0xEC650C26L}},{{0xEC650C26L},{4294967289UL},{4294967290UL},{4294967295UL},{0xE238D409L},{0UL}},{{0x038989D5L},{4294967295UL},{0UL},{0xC24095F3L},{0x279C7684L},{4294967295UL}},{{0x6664E41CL},{4294967295UL},{0xEE483976L},{0xE238D409L},{1UL},{0x038989D5L}},{{0xEE483976L},{1UL},{0x5BA156F6L},{4294967295UL},{4294967295UL},{0x5BA156F6L}},{{0x2888F81FL},{0x2888F81FL},{0x22910943L},{0x279C7684L},{0UL},{0x01DF6620L}},{{0xDEB8809BL},{0x28809CBBL},{0xA9CA9BF1L},{0x5BA156F6L},{4294967295UL},{0x22910943L}}},{{{4294967289UL},{0xDEB8809BL},{0xA9CA9BF1L},{0x76A306C2L},{0x2888F81FL},{0x01DF6620L}},{{0xEC650C26L},{0x76A306C2L},{0x22910943L},{0UL},{4294967295UL},{0x5BA156F6L}},{{0UL},{4294967295UL},{0x5BA156F6L},{0x1E24F394L},{8UL},{0x038989D5L}},{{0xDA4E9AB5L},{1UL},{0xEE483976L},{0x01DF6620L},{0x28809CBBL},{4294967295UL}},{{0x28809CBBL},{8UL},{0UL},{4294967295UL},{4294967290UL},{0UL}},{{0UL},{1UL},{4294967290UL},{0xEC650C26L},{0UL},{0xC24095F3L}},{{4294967295UL},{4294967288UL},{4294967295UL},{0x30B49776L},{0x2888F81FL},{0x28809CBBL}},{{0x01EAE450L},{4294967295UL},{4294967295UL},{0x6664E41CL},{4294967295UL},{0UL}},{{0xC24095F3L},{1UL},{0xB0DCB7F1L},{0x6664E41CL},{0x5BA156F6L},{0x30B49776L}}},{{{0x01EAE450L},{0x1E24F394L},{4294967295UL},{0x30B49776L},{0xC24095F3L},{8UL}},{{4294967295UL},{0xA9CA9BF1L},{4294967288UL},{0xC24095F3L},{0xB91C4F03L},{1UL}},{{0x2C8A48FBL},{0xEC650C26L},{0x30B49776L},{0UL},{0UL},{0x22910943L}},{{0UL},{0xB91C4F03L},{0xEE483976L},{0x5BA156F6L},{0x038989D5L},{0UL}},{{0xE238D409L},{4294967295UL},{0xEC650C26L},{1UL},{0x28809CBBL},{0x28809CBBL}},{{1UL},{0xE238D409L},{0xE238D409L},{1UL},{0x2C8A48FBL},{4294967288UL}},{{0xC24095F3L},{0xDA4E9AB5L},{0UL},{0x01EAE450L},{4294967295UL},{1UL}},{{8UL},{0x1E24F394L},{4294967290UL},{4294967288UL},{4294967295UL},{0xA9CA9BF1L}},{{0x279C7684L},{0xDA4E9AB5L},{4294967288UL},{0UL},{0x2C8A48FBL},{0x6664E41CL}}}};
                    int32_t l_869 = 0x9710716CL;
                    int32_t l_871 = 0L;
                    int32_t l_872 = 0x4DD7F097L;
                    int32_t l_873 = (-9L);
                    int32_t l_874 = 0x168E021DL;
                    int32_t l_875 = 0x2EDD8B18L;
                    int32_t l_876 = (-1L);
                    int32_t *l_886[7] = {&g_18[1],&g_18[1],&g_18[1],&g_18[1],&g_18[1],&g_18[1],&g_18[1]};
                    int32_t **l_887 = (void*)0;
                    int32_t **l_888[3];
                    int16_t l_891 = 1L;
                    uint16_t * const *l_899[4] = {&g_498,&g_498,&g_498,&g_498};
                    uint16_t * const * const *l_898 = &l_899[0];
                    int64_t *l_900 = &g_13;
                    uint8_t *l_912[2][10][3] = {{{&g_513[7][1][2],&g_513[7][1][1],(void*)0},{(void*)0,&g_513[0][3][4],&g_513[3][0][2]},{&g_513[7][1][1],&g_513[7][1][1],&g_513[7][1][1]},{(void*)0,(void*)0,&g_80},{&g_513[7][1][2],&g_513[3][4][3],&g_513[7][1][3]},{&g_513[0][3][4],(void*)0,&g_513[0][3][4]},{(void*)0,&g_513[7][1][1],(void*)0},{&g_513[7][1][1],&g_513[0][3][4],&g_513[0][3][4]},{(void*)0,&g_513[7][1][1],&g_513[7][1][3]},{&g_80,&g_513[7][1][1],&g_80}},{{(void*)0,&g_513[5][0][5],&g_513[7][1][1]},{&g_513[7][1][1],&g_80,&g_513[3][0][2]},{(void*)0,&g_513[5][0][5],(void*)0},{&g_513[0][3][4],&g_513[7][1][1],(void*)0},{&g_513[7][1][2],&g_513[7][1][1],(void*)0},{(void*)0,&g_513[0][3][4],&g_513[3][0][2]},{&g_513[7][1][1],&g_513[7][1][1],&g_513[7][1][1]},{(void*)0,(void*)0,&g_80},{&g_513[7][1][2],&g_513[3][4][3],&g_513[7][1][3]},{&g_513[0][3][4],(void*)0,&g_513[0][3][4]}}};
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_888[i] = (void*)0;
                    for (l_283 = 1; (l_283 == 27); ++l_283)
                    { /* block id: 386 */
                        int32_t ***l_832 = &g_127[2];
                        int32_t ****l_833 = &l_832;
                        uint8_t *l_861 = &g_513[7][1][1];
                        int64_t *l_862 = &g_13;
                        int32_t *l_865 = &g_18[1];
                        (*g_128) = (0x98L ^ (p_152 == (safe_div_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u(0x34D3L, 5UL)) , (p_155 > ((safe_div_func_int16_t_s_s(((((*l_833) = l_832) != l_834) & (***l_834)), ((*l_782) && 0x6EL))) ^ 0x689646610853B5A2LL))) | 0x31L), p_153))));
                        if (l_526)
                            goto lbl_835;
                        (**l_832) = &p_153;
                        l_275 ^= (((safe_mul_func_int8_t_s_s(9L, (((void*)0 != (*g_711)) , (*g_131)))) >= (((*l_865) = (safe_sub_func_int64_t_s_s(((p_153 >= ((((*l_784) = (!(*g_228))) & (l_818[9] = (safe_div_func_uint64_t_u_u((safe_add_func_int8_t_s_s((g_863 |= (safe_add_func_int32_t_s_s((l_849[3][8][1] , (((safe_mul_func_int16_t_s_s((safe_div_func_uint64_t_u_u(p_153, (safe_add_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((((~((((*l_862) = (((*l_861) = (safe_div_func_uint32_t_u_u((((g_80 , 0L) , (****l_833)) , l_818[5]), 4294967295UL))) > (**l_226))) , (**l_226)) , g_18[0])) == p_155) <= p_155), 4294967289UL)), (-7L))))), 0xA7C6L)) >= (*g_191)) != 0x68BDL)), l_818[5]))), p_155)), g_864)))) < (*****g_711))) > p_153), (*g_191)))) , 1L)) >= (*g_131));
                    }
                    --l_877[1][2];
                    l_890 |= (((safe_mod_func_uint8_t_u_u((safe_unary_minus_func_uint64_t_u(l_818[5])), p_152)) | g_18[1]) , ((safe_div_func_uint16_t_u_u(l_867, (*l_782))) , ((l_885 == (g_889 = l_886[6])) | ((g_91[4][0][3].f0 , 4294967286UL) == p_153))));
                    l_870[1][2] &= (l_891 > ((safe_add_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((((*l_900) = ((l_896 = &l_813) == l_898)) || (safe_div_func_uint8_t_u_u((((--(*l_784)) > (((p_152 , l_905[0]) , (safe_rshift_func_uint16_t_u_s(((safe_add_func_uint16_t_u_u(((((*l_783) = ((p_155 | (0x87F381D9L || g_58[3][0])) ^ ((**l_226) = (safe_rshift_func_uint16_t_u_s(0x7567L, p_153))))) ^ g_312[2]) > g_2[6]), 0xDD8BL)) == 2L), p_155))) == g_641.f0)) | 0xA9L), (-1L)))), p_155)), 0x44L)) == l_890));
                }
            }
            for (g_13 = 0; (g_13 < (-22)); g_13 = safe_sub_func_uint16_t_u_u(g_13, 1))
            { /* block id: 412 */
                uint64_t l_927 = 0x86E4E3E913A05D6ELL;
                uint8_t *****l_931[6] = {&l_240,&l_240,&l_240,&l_240,&l_240,&l_240};
                int32_t l_933[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_933[i] = (-5L);
            }
        }
        for (g_863 = 12; (g_863 < (-24)); --g_863)
        { /* block id: 420 */
            float l_954 = 0xF.5DE4F0p-11;
            int32_t l_955 = 9L;
            uint8_t l_956 = 0xB2L;
            int64_t *l_957 = &g_13;
            int64_t *l_959 = &g_719;
            int32_t l_960 = 2L;
            l_960 &= (!((safe_mod_func_int32_t_s_s(0L, ((((**l_226) && (safe_mul_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(0L, p_153)), (safe_div_func_uint16_t_u_u((((***l_932) = (**l_239)) == g_947), (p_152 || ((*l_959) = ((*l_957) = (((safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u(((*g_191) = 0x901D875EF126CC0CLL), (((((safe_mul_func_uint16_t_u_u((((p_155 & (***g_713)) >= l_955) >= p_153), 1L)) , (*l_785)) , l_956) , (**l_226)) != 1L))), p_153)) && 0x9CB79FFDF28BCA7FLL) | p_155)))))))), 0x71L))) != p_153) | 0x57982775L))) | 1L));
        }
    }
    return p_153;
}


/* ------------------------------------------ */
/* 
 * reads : g_194 g_142 g_54 g_192 g_21 g_131 g_58 g_128
 * writes: g_128
 */
static int16_t  func_164(int16_t * p_165, uint8_t * p_166, uint64_t * p_167, uint8_t  p_168, int32_t  p_169)
{ /* block id: 89 */
    int32_t l_203 = (-1L);
    int16_t *l_204 = &g_21;
    (*g_194) = &p_169;
    (**g_194) = (((p_168 <= ((safe_add_func_int8_t_s_s((-1L), (safe_rshift_func_int16_t_s_s(((((((((safe_sub_func_uint8_t_u_u(250UL, (safe_mul_func_int8_t_s_s(l_203, (&g_21 != (p_165 = l_204)))))) , (safe_mod_func_int64_t_s_s((safe_mod_func_int32_t_s_s(l_203, (safe_rshift_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s((safe_div_func_int32_t_s_s((p_169 , (safe_add_func_uint16_t_u_u(((0L >= 0UL) > l_203), g_142))), 0xBA61EA9DL)), g_54)) != 0L), 2)))), (*p_167)))) | p_168) || 0x29CE5635L) , g_21) ^ p_169) , g_142) <= (-2L)), p_168)))) < 0xC2DAF40BCFF1FC72LL)) , l_203) >= (*g_131));
    return l_203;
}


/* ------------------------------------------ */
/* 
 * reads : g_91.f0
 * writes: g_128
 */
static int64_t  func_172(int32_t  p_173)
{ /* block id: 83 */
    uint64_t l_184 = 7UL;
    int32_t *l_185 = &g_142;
    int32_t **l_186 = &g_128;
    int32_t **l_187[5][5][5] = {{{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,&l_185}},{{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,&l_185}},{{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,(void*)0}},{{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,(void*)0}},{{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,&l_185},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,(void*)0},{&l_185,&l_185,&l_185,&l_185,&l_185}}};
    int32_t *l_188 = &g_189;
    int i, j, k;
    l_188 = (l_184 , ((*l_186) = l_185));
    return g_91[4][0][3].f0;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_18[i], "g_18[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_21, "g_21", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_22[i][j], "g_22[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_26, "g_26", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    transparent_crc(g_55, "g_55", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_58[i][j], "g_58[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_80, "g_80", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_91[i][j][k].f0, "g_91[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_119[i][j][k], "g_119[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_142, "g_142", print_hash_value);
    transparent_crc(g_189, "g_189", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_192[i], "g_192[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_224, "g_224", print_hash_value);
    transparent_crc(g_263, "g_263", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_312[i], "g_312[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_378.f0, "g_378.f0", print_hash_value);
    transparent_crc_bytes (&g_405, sizeof(g_405), "g_405", print_hash_value);
    transparent_crc(g_445.f0, "g_445.f0", print_hash_value);
    transparent_crc(g_471, "g_471", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_513[i][j][k], "g_513[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_641.f0, "g_641.f0", print_hash_value);
    transparent_crc(g_659, "g_659", print_hash_value);
    transparent_crc(g_676, "g_676", print_hash_value);
    transparent_crc(g_716, "g_716", print_hash_value);
    transparent_crc(g_719, "g_719", print_hash_value);
    transparent_crc(g_727, "g_727", print_hash_value);
    transparent_crc(g_863, "g_863", print_hash_value);
    transparent_crc(g_864, "g_864", print_hash_value);
    transparent_crc(g_971, "g_971", print_hash_value);
    transparent_crc(g_1005, "g_1005", print_hash_value);
    transparent_crc_bytes (&g_1036, sizeof(g_1036), "g_1036", print_hash_value);
    transparent_crc(g_1038, "g_1038", print_hash_value);
    transparent_crc(g_1055.f0, "g_1055.f0", print_hash_value);
    transparent_crc(g_1181, "g_1181", print_hash_value);
    transparent_crc(g_1288, "g_1288", print_hash_value);
    transparent_crc(g_1375, "g_1375", print_hash_value);
    transparent_crc(g_1434, "g_1434", print_hash_value);
    transparent_crc(g_1442, "g_1442", print_hash_value);
    transparent_crc(g_1530, "g_1530", print_hash_value);
    transparent_crc(g_1568, "g_1568", print_hash_value);
    transparent_crc(g_1625, "g_1625", print_hash_value);
    transparent_crc(g_1721, "g_1721", print_hash_value);
    transparent_crc(g_1766, "g_1766", print_hash_value);
    transparent_crc(g_1934, "g_1934", print_hash_value);
    transparent_crc(g_1941, "g_1941", print_hash_value);
    transparent_crc(g_2054, "g_2054", print_hash_value);
    transparent_crc(g_2230, "g_2230", print_hash_value);
    transparent_crc(g_2316, "g_2316", print_hash_value);
    transparent_crc_bytes (&g_2337, sizeof(g_2337), "g_2337", print_hash_value);
    transparent_crc(g_2369, "g_2369", print_hash_value);
    transparent_crc_bytes (&g_2543, sizeof(g_2543), "g_2543", print_hash_value);
    transparent_crc(g_2816, "g_2816", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2851[i], "g_2851[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2863, "g_2863", print_hash_value);
    transparent_crc(g_2935, "g_2935", print_hash_value);
    transparent_crc(g_2954, "g_2954", print_hash_value);
    transparent_crc(g_3019.f0, "g_3019.f0", print_hash_value);
    transparent_crc(g_3071, "g_3071", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_3177[i][j], "g_3177[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 872
XXX total union variables: 35

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 47
breakdown:
   depth: 1, occurrence: 295
   depth: 2, occurrence: 83
   depth: 3, occurrence: 7
   depth: 4, occurrence: 3
   depth: 5, occurrence: 4
   depth: 6, occurrence: 3
   depth: 7, occurrence: 2
   depth: 8, occurrence: 1
   depth: 9, occurrence: 2
   depth: 11, occurrence: 3
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 2
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 5
   depth: 20, occurrence: 1
   depth: 21, occurrence: 4
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 1
   depth: 25, occurrence: 3
   depth: 26, occurrence: 5
   depth: 27, occurrence: 4
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 2
   depth: 32, occurrence: 1
   depth: 37, occurrence: 3
   depth: 47, occurrence: 2

XXX total number of pointers: 679

XXX times a variable address is taken: 1575
XXX times a pointer is dereferenced on RHS: 671
breakdown:
   depth: 1, occurrence: 474
   depth: 2, occurrence: 139
   depth: 3, occurrence: 38
   depth: 4, occurrence: 12
   depth: 5, occurrence: 8
XXX times a pointer is dereferenced on LHS: 417
breakdown:
   depth: 1, occurrence: 364
   depth: 2, occurrence: 34
   depth: 3, occurrence: 12
   depth: 4, occurrence: 7
XXX times a pointer is compared with null: 58
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 10888

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2057
   level: 2, occurrence: 743
   level: 3, occurrence: 371
   level: 4, occurrence: 167
   level: 5, occurrence: 28
XXX number of pointers point to pointers: 293
XXX number of pointers point to scalars: 349
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.5
XXX average alias set size: 1.4

XXX times a non-volatile is read: 3088
XXX times a non-volatile is write: 1223
XXX times a volatile is read: 119
XXX    times read thru a pointer: 23
XXX times a volatile is write: 75
XXX    times written thru a pointer: 17
XXX times a volatile is available for access: 2.61e+03
XXX percentage of non-volatile access: 95.7

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 309
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 27
   depth: 2, occurrence: 34
   depth: 3, occurrence: 57
   depth: 4, occurrence: 69
   depth: 5, occurrence: 87

XXX percentage a fresh-made variable is used: 15.8
XXX percentage an existing variable is used: 84.2
********************* end of statistics **********************/


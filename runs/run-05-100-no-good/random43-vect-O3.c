/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1232294591
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_4[1][1] = {{0x90C4D8E5L}};
static int32_t * const g_69 = &g_4[0][0];
static int32_t * const *g_68[4][7][6] = {{{&g_69,(void*)0,&g_69,&g_69,(void*)0,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{(void*)0,&g_69,&g_69,(void*)0,&g_69,&g_69},{&g_69,(void*)0,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,(void*)0,&g_69,&g_69},{&g_69,(void*)0,&g_69,&g_69,(void*)0,&g_69}},{{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,(void*)0,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{(void*)0,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0}},{{&g_69,&g_69,&g_69,&g_69,(void*)0,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{(void*)0,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,(void*)0,(void*)0}},{{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{(void*)0,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0},{&g_69,&g_69,&g_69,&g_69,(void*)0,(void*)0},{&g_69,&g_69,&g_69,&g_69,&g_69,(void*)0}}};
static int32_t g_72 = 0xD172E30EL;
static uint32_t g_79[3] = {0xEA342190L,0xEA342190L,0xEA342190L};
static int64_t g_99 = 0x31764C7F453ACC8DLL;
static uint8_t g_101[1][5] = {{0xFAL,0xFAL,0xFAL,0xFAL,0xFAL}};
static float g_103 = 0x9.ADBF82p+2;
static float * volatile g_102 = &g_103;/* VOLATILE GLOBAL g_102 */
static uint8_t g_127 = 0UL;
static float g_159 = 0x9.202534p+46;
static int16_t g_160 = 1L;
static const float g_193 = 0x1.4p+1;
static uint32_t g_200[4][5][6] = {{{2UL,4UL,0xFF8E3032L,0x60C6884EL,0xA853B33CL,0xA853B33CL},{0xA853B33CL,5UL,5UL,0xA853B33CL,0xBB3153F0L,0x344D2273L},{0x515ED506L,2UL,3UL,0x170729E3L,0xFF8E3032L,18446744073709551607UL},{18446744073709551607UL,0x60C6884EL,0x86FE3A84L,9UL,0xFF8E3032L,1UL},{0xBB3153F0L,2UL,0xA853B33CL,2UL,0xBB3153F0L,9UL}},{{0x86FE3A84L,5UL,18446744073709551615UL,0x344D2273L,0xA853B33CL,18446744073709551607UL},{18446744073709551615UL,4UL,0xBB3153F0L,5UL,0x63640059L,18446744073709551607UL},{9UL,1UL,18446744073709551615UL,18446744073709551615UL,1UL,9UL},{0x63640059L,0x170729E3L,0xA853B33CL,0xFF8E3032L,0x86FE3A84L,1UL},{0x170729E3L,0xBB3153F0L,0x86FE3A84L,3UL,0x60C6884EL,18446744073709551607UL}},{{4UL,3UL,0xBB3153F0L,18446744073709551615UL,0xBB3153F0L,3UL},{0x344D2273L,18446744073709551607UL,0x86FE3A84L,0xA853B33CL,3UL,0x515ED506L},{0xFF8E3032L,0x63640059L,18446744073709551615UL,0x86FE3A84L,0xCEEB8BFCL,0xBB3153F0L},{0xA853B33CL,0x63640059L,9UL,3UL,3UL,9UL},{18446744073709551607UL,18446744073709551607UL,0x63640059L,5UL,0xBB3153F0L,4UL}},{{2UL,3UL,0x170729E3L,0xFF8E3032L,18446744073709551607UL,0x63640059L},{0xCEEB8BFCL,2UL,0x170729E3L,4UL,18446744073709551607UL,4UL},{0x63640059L,4UL,0x63640059L,0x515ED506L,0x60C6884EL,9UL},{0x515ED506L,0x60C6884EL,9UL,18446744073709551607UL,0x344D2273L,0xBB3153F0L},{5UL,9UL,18446744073709551615UL,18446744073709551607UL,0x515ED506L,0x515ED506L}}};
static int32_t **g_208 = (void*)0;
static uint32_t g_224 = 0UL;
static int8_t g_226 = 1L;
static int16_t *g_252[7][6][4] = {{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}},{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}},{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}},{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}},{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}},{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}},{{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160},{&g_160,&g_160,&g_160,&g_160}}};
static float g_290 = 0x2.4p-1;
static float g_293[3][10] = {{0x0.9086BEp-24,0xB.0FE6DBp-27,0xB.0FE6DBp-27,0x0.9086BEp-24,0xB.7B5189p+83,0x0.8p-1,(-0x1.7p+1),(-0x9.Fp+1),(-0x1.7p+1),0x0.8p-1},{(-0x1.3p+1),0x8.Ap+1,0xB.7B5189p+83,0x8.Ap+1,(-0x1.3p+1),(-0x1.3p+1),0xB.0FE6DBp-27,(-0x1.7p+1),(-0x1.7p+1),0xB.0FE6DBp-27},{(-0x9.Fp+1),(-0x1.3p+1),0x0.9086BEp-24,0x0.9086BEp-24,(-0x1.3p+1),(-0x9.Fp+1),0x0.Dp+1,0xB.0FE6DBp-27,(-0x1.3p+1),0xB.0FE6DBp-27}};
static int8_t g_297 = (-1L);
static uint8_t *g_318 = (void*)0;
static uint8_t **g_317 = &g_318;
static uint8_t ***g_316 = &g_317;
static const int32_t *g_349[4][8] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static const int32_t ** volatile g_348 = &g_349[3][7];/* VOLATILE GLOBAL g_348 */
static volatile uint64_t g_373 = 1UL;/* VOLATILE GLOBAL g_373 */
static volatile uint64_t *g_372 = &g_373;
static volatile uint64_t **g_371 = &g_372;
static volatile uint64_t g_395 = 18446744073709551612UL;/* VOLATILE GLOBAL g_395 */
static uint64_t g_448 = 0x91F9277D26157F35LL;
static uint32_t g_493 = 6UL;
static uint64_t *** volatile g_527 = (void*)0;/* VOLATILE GLOBAL g_527 */
static uint64_t *g_530 = &g_448;
static uint64_t **g_529[7][2][9] = {{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{(void*)0,&g_530,&g_530,&g_530,(void*)0,(void*)0,&g_530,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{(void*)0,(void*)0,&g_530,&g_530,&g_530,(void*)0,(void*)0,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}},{{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530},{&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530,&g_530}}};
static uint64_t *** volatile g_528[1] = {&g_529[3][1][4]};
static uint64_t *** volatile g_531 = &g_529[3][1][4];/* VOLATILE GLOBAL g_531 */
static int32_t *g_533 = &g_4[0][0];
static int32_t ** volatile g_532 = &g_533;/* VOLATILE GLOBAL g_532 */
static volatile float * volatile * volatile g_593 = (void*)0;/* VOLATILE GLOBAL g_593 */
static volatile float * volatile * volatile * volatile g_592 = &g_593;/* VOLATILE GLOBAL g_592 */
static float **g_609 = (void*)0;
static int64_t g_634 = (-6L);
static int8_t g_635 = (-10L);
static int16_t * volatile *g_640[5][7] = {{&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2]},{&g_252[2][5][2],&g_252[2][5][2],&g_252[6][1][1],&g_252[2][5][2],&g_252[2][5][2],&g_252[6][1][1],&g_252[2][5][2]},{&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2]},{&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2],&g_252[2][5][2]},{&g_252[2][5][2],&g_252[2][5][2],&g_252[6][1][1],&g_252[2][5][2],&g_252[2][5][2],&g_252[6][1][1],&g_252[2][5][2]}};
static int16_t * volatile **g_639 = &g_640[2][3];
static int64_t g_659 = 0xE5EF1B0B4582E505LL;
static float * const **** volatile g_669 = (void*)0;/* VOLATILE GLOBAL g_669 */
static float * const *g_673 = (void*)0;
static float * const **g_672 = &g_673;
static float * const ***g_671 = &g_672;
static int32_t g_696 = 0x79F754FEL;
static volatile uint16_t g_754 = 0x0F17L;/* VOLATILE GLOBAL g_754 */
static volatile uint16_t * volatile g_753 = &g_754;/* VOLATILE GLOBAL g_753 */
static volatile uint16_t * volatile *g_752 = &g_753;
static uint64_t g_766 = 18446744073709551615UL;
static volatile uint32_t g_795[10] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static uint32_t g_897 = 8UL;
static uint32_t *g_896 = &g_897;
static uint16_t g_907 = 0UL;
static uint8_t *** const *g_928 = (void*)0;
static uint32_t * volatile *g_944 = &g_896;
static uint32_t * volatile **g_943[7][6][6] = {{{&g_944,&g_944,&g_944,&g_944,(void*)0,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,(void*)0},{&g_944,(void*)0,(void*)0,&g_944,(void*)0,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,(void*)0,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,(void*)0,&g_944}},{{&g_944,(void*)0,&g_944,&g_944,&g_944,&g_944},{(void*)0,&g_944,(void*)0,&g_944,(void*)0,&g_944},{(void*)0,&g_944,&g_944,(void*)0,&g_944,(void*)0},{&g_944,&g_944,(void*)0,&g_944,&g_944,(void*)0},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,(void*)0,&g_944,(void*)0,&g_944}},{{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,(void*)0,&g_944,(void*)0,(void*)0},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944}},{{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,(void*)0,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{(void*)0,&g_944,&g_944,&g_944,&g_944,(void*)0},{(void*)0,&g_944,(void*)0,&g_944,(void*)0,&g_944},{&g_944,&g_944,&g_944,&g_944,(void*)0,(void*)0}},{{&g_944,(void*)0,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,(void*)0},{&g_944,&g_944,&g_944,(void*)0,&g_944,&g_944},{(void*)0,&g_944,(void*)0,(void*)0,&g_944,(void*)0},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,(void*)0,&g_944,&g_944,&g_944,&g_944}},{{&g_944,&g_944,(void*)0,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,(void*)0,&g_944},{&g_944,&g_944,&g_944,(void*)0,&g_944,&g_944},{(void*)0,&g_944,(void*)0,(void*)0,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,(void*)0},{&g_944,&g_944,(void*)0,&g_944,&g_944,&g_944}},{{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{(void*)0,&g_944,(void*)0,&g_944,(void*)0,&g_944},{(void*)0,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,&g_944,&g_944,&g_944,&g_944,&g_944},{&g_944,(void*)0,&g_944,&g_944,&g_944,&g_944}}};
static uint32_t * volatile *** volatile g_945 = &g_943[0][3][4];/* VOLATILE GLOBAL g_945 */
static uint32_t **g_1111 = &g_896;
static uint32_t ***g_1110 = &g_1111;
static uint32_t ****g_1109 = &g_1110;
static volatile float g_1140 = 0xB.E89D2Dp-79;/* VOLATILE GLOBAL g_1140 */
static uint64_t *****g_1222 = (void*)0;
static uint64_t **g_1232[1][6][6] = {{{&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530},{&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530},{&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530},{&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530},{&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530},{&g_530,(void*)0,&g_530,&g_530,(void*)0,&g_530}}};
static uint64_t *** const g_1231 = &g_1232[0][4][0];
static uint64_t *** const *g_1230 = &g_1231;
static uint64_t *** const **g_1229 = &g_1230;
static uint8_t ***g_1270[4] = {&g_317,&g_317,&g_317,&g_317};
static volatile uint8_t g_1298 = 0xA4L;/* VOLATILE GLOBAL g_1298 */
static const int16_t *g_1329 = &g_160;
static const int16_t **g_1328 = &g_1329;
static const int16_t *** volatile g_1327 = &g_1328;/* VOLATILE GLOBAL g_1327 */
static int16_t * const g_1344[7] = {&g_160,&g_160,&g_160,&g_160,&g_160,&g_160,&g_160};
static uint32_t g_1345 = 0UL;
static int8_t g_1378 = (-3L);
static uint16_t g_1380 = 0x59B4L;
static float g_1385 = 0x9.84306Dp+70;
static int32_t g_1403 = 0xF925C46FL;
static uint64_t g_1408 = 18446744073709551615UL;
static int32_t * const  volatile g_1443 = &g_72;/* VOLATILE GLOBAL g_1443 */
static uint64_t g_1508[9][10] = {{0x187BCEE9E4D13CE8LL,0x3EEE790A5481410FLL,0x1162120E0F7B9F99LL,0xBDB9505AC6DF965FLL,0x187BCEE9E4D13CE8LL,0x187BCEE9E4D13CE8LL,0xBDB9505AC6DF965FLL,0x1162120E0F7B9F99LL,18446744073709551615UL,18446744073709551607UL},{18446744073709551615UL,0x3EEE790A5481410FLL,18446744073709551607UL,0xBDB9505AC6DF965FLL,0xF1D67E07F2E8D1F3LL,18446744073709551607UL,0xF1D67E07F2E8D1F3LL,0xBDB9505AC6DF965FLL,18446744073709551607UL,0x3EEE790A5481410FLL},{18446744073709551608UL,0x1162120E0F7B9F99LL,18446744073709551607UL,0x3EEE790A5481410FLL,0xF1D67E07F2E8D1F3LL,0xE7B6A7772B77D8ECLL,0xE7B6A7772B77D8ECLL,0xF1D67E07F2E8D1F3LL,0x3EEE790A5481410FLL,18446744073709551607UL},{0xF1D67E07F2E8D1F3LL,0xF1D67E07F2E8D1F3LL,18446744073709551615UL,18446744073709551608UL,0x187BCEE9E4D13CE8LL,0xE7B6A7772B77D8ECLL,18446744073709551607UL,0xE7B6A7772B77D8ECLL,0x187BCEE9E4D13CE8LL,18446744073709551608UL},{18446744073709551608UL,18446744073709551615UL,18446744073709551608UL,0xE7B6A7772B77D8ECLL,0xBDB9505AC6DF965FLL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,0xBDB9505AC6DF965FLL},{18446744073709551615UL,0xF1D67E07F2E8D1F3LL,0xF1D67E07F2E8D1F3LL,18446744073709551615UL,18446744073709551608UL,0x187BCEE9E4D13CE8LL,0xE7B6A7772B77D8ECLL,18446744073709551607UL,0xE7B6A7772B77D8ECLL,0x187BCEE9E4D13CE8LL},{18446744073709551607UL,0x1162120E0F7B9F99LL,18446744073709551608UL,0x1162120E0F7B9F99LL,18446744073709551607UL,0x3EEE790A5481410FLL,0xF1D67E07F2E8D1F3LL,0xE7B6A7772B77D8ECLL,0xE7B6A7772B77D8ECLL,0xF1D67E07F2E8D1F3LL},{18446744073709551607UL,0x3EEE790A5481410FLL,18446744073709551615UL,18446744073709551615UL,0x3EEE790A5481410FLL,18446744073709551607UL,0xBDB9505AC6DF965FLL,0xF1D67E07F2E8D1F3LL,18446744073709551607UL,0xF1D67E07F2E8D1F3LL},{0x1162120E0F7B9F99LL,18446744073709551615UL,18446744073709551607UL,0xE7B6A7772B77D8ECLL,18446744073709551607UL,18446744073709551615UL,0x1162120E0F7B9F99LL,0xBDB9505AC6DF965FLL,0x187BCEE9E4D13CE8LL,0x187BCEE9E4D13CE8LL}};
static uint64_t g_1509 = 1UL;
static uint64_t g_1510[9][4] = {{0xF9421BF21693076FLL,0xD1071FAB978642CCLL,18446744073709551608UL,0xD1071FAB978642CCLL},{0x8F2A44D9E18293B8LL,0x2262A94C8FD0AA71LL,0xC8DDF8E23E36EAE3LL,18446744073709551608UL},{18446744073709551607UL,18446744073709551609UL,0xF9421BF21693076FLL,0UL},{0x61D4CE3D4980A8A5LL,1UL,0UL,18446744073709551607UL},{0x61D4CE3D4980A8A5LL,0UL,0xF9421BF21693076FLL,0xF9421BF21693076FLL},{18446744073709551607UL,18446744073709551607UL,0xC8DDF8E23E36EAE3LL,0UL},{0x8F2A44D9E18293B8LL,0xC8DDF8E23E36EAE3LL,18446744073709551608UL,0x2262A94C8FD0AA71LL},{0xF9421BF21693076FLL,18446744073709551615UL,0x4CAD5AB6F278F8B8LL,18446744073709551608UL},{18446744073709551608UL,18446744073709551615UL,18446744073709551607UL,0x2262A94C8FD0AA71LL}};
static uint64_t g_1511[7][10] = {{0x5B64DF4223C59237LL,0x5E12D55C935A4AFCLL,0xC9CDB41FB3B6852FLL,0x5E12D55C935A4AFCLL,0x5B64DF4223C59237LL,0x35573D12926B4437LL,0xFCF255D57A4778DDLL,18446744073709551609UL,18446744073709551609UL,0xFCF255D57A4778DDLL},{0x0BC058A72BE4ED47LL,0x35573D12926B4437LL,8UL,8UL,0x35573D12926B4437LL,0x0BC058A72BE4ED47LL,0x17FB017A826D258ELL,0xFCF255D57A4778DDLL,0x5B64DF4223C59237LL,0xFCF255D57A4778DDLL},{0x5E12D55C935A4AFCLL,8UL,0x5B64DF4223C59237LL,18446744073709551609UL,0x5B64DF4223C59237LL,8UL,0x5E12D55C935A4AFCLL,0x17FB017A826D258ELL,0UL,0UL},{0x5E12D55C935A4AFCLL,0UL,0x0BC058A72BE4ED47LL,0xC9CDB41FB3B6852FLL,0xC9CDB41FB3B6852FLL,0x0BC058A72BE4ED47LL,0UL,0x5E12D55C935A4AFCLL,0x35573D12926B4437LL,0x17FB017A826D258ELL},{0x0BC058A72BE4ED47LL,0UL,0x5E12D55C935A4AFCLL,0x35573D12926B4437LL,0x17FB017A826D258ELL,0x35573D12926B4437LL,0x5E12D55C935A4AFCLL,0UL,0x0BC058A72BE4ED47LL,0xC9CDB41FB3B6852FLL},{0x5B64DF4223C59237LL,8UL,0x5E12D55C935A4AFCLL,0x17FB017A826D258ELL,0UL,0UL,0x17FB017A826D258ELL,0x5E12D55C935A4AFCLL,8UL,0x5B64DF4223C59237LL},{8UL,0x35573D12926B4437LL,0x0BC058A72BE4ED47LL,0x17FB017A826D258ELL,0xFCF255D57A4778DDLL,0x5B64DF4223C59237LL,18446744073709551609UL,0UL,0xC9CDB41FB3B6852FLL,8UL}};
static uint64_t g_1539 = 0xC3D099657A698610LL;
static int64_t g_1610 = 1L;
static int32_t ** volatile g_1615 = (void*)0;/* VOLATILE GLOBAL g_1615 */
static int16_t ***g_1636 = (void*)0;
static int16_t ****g_1635 = &g_1636;
static int32_t * volatile *g_1665 = &g_533;
static int32_t * volatile * volatile *g_1664 = &g_1665;
static int32_t * volatile * volatile **g_1663 = &g_1664;
static volatile int32_t g_1706 = (-1L);/* VOLATILE GLOBAL g_1706 */
static const int32_t ** const  volatile g_1951[2][1] = {{&g_349[3][4]},{&g_349[3][4]}};
static const int32_t ** volatile g_1952 = &g_349[1][0];/* VOLATILE GLOBAL g_1952 */
static const int32_t ** volatile g_1959 = &g_349[3][7];/* VOLATILE GLOBAL g_1959 */
static int8_t g_1980[3] = {0xFBL,0xFBL,0xFBL};
static int16_t g_1981[10][2] = {{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL},{(-1L),0xFCCAL}};
static uint8_t *****g_1986 = (void*)0;
static uint8_t ****g_1988 = &g_1270[0];
static uint8_t *****g_1987 = &g_1988;
static uint64_t g_2030 = 0x3D17C634C75103B1LL;
static volatile uint32_t g_2129 = 0x80811388L;/* VOLATILE GLOBAL g_2129 */
static int16_t g_2141 = (-1L);
static int16_t g_2142 = (-1L);
static int16_t * const g_2140[8] = {&g_2142,&g_2142,&g_2142,&g_2142,&g_2142,&g_2142,&g_2142,&g_2142};
static int16_t * const *g_2139 = &g_2140[3];
static uint8_t *g_2189 = &g_101[0][3];
static uint8_t ** const g_2188[2] = {&g_2189,&g_2189};
static uint8_t ** const *g_2187[3] = {&g_2188[0],&g_2188[0],&g_2188[0]};
static volatile float * volatile * volatile * volatile * volatile g_2210 = &g_592;/* VOLATILE GLOBAL g_2210 */
static float ***g_2212 = &g_609;
static float ****g_2211[8][7][3] = {{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,&g_2212}},{{&g_2212,&g_2212,(void*)0},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,(void*)0},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,(void*)0},{&g_2212,&g_2212,&g_2212},{&g_2212,&g_2212,(void*)0}}};
static int32_t *g_2362 = &g_1403;
static uint32_t *g_2366 = &g_200[0][0][3];
static const int16_t g_2423 = 0x6F3CL;
static int16_t *****g_2432 = &g_1635;
static const uint32_t *****g_2483 = (void*)0;
static volatile float * volatile g_2603 = &g_1140;/* VOLATILE GLOBAL g_2603 */
static volatile float * volatile *g_2602 = &g_2603;
static volatile float * volatile ** volatile g_2601 = &g_2602;/* VOLATILE GLOBAL g_2601 */
static int64_t g_2663 = 0xE814B6C8AE22FD68LL;
static int32_t g_2685 = 3L;
static int32_t g_2686 = 0xFF47284CL;
static int32_t * const g_2734 = &g_72;
static uint64_t g_2737 = 0xA9E28B9A5AA14BDFLL;
static uint32_t **g_2793 = (void*)0;
static uint32_t *** volatile g_2792 = &g_2793;/* VOLATILE GLOBAL g_2792 */
static uint32_t g_2810 = 0UL;
static float ***** volatile g_2824 = &g_2211[3][4][0];/* VOLATILE GLOBAL g_2824 */
static int32_t g_2933 = (-1L);
static float g_2954 = 0x1.Cp+1;
static int32_t g_3188 = 0x37075E49L;
static int8_t *g_3220 = (void*)0;
static int8_t **g_3219 = &g_3220;
static int8_t *** const  volatile g_3218[8] = {&g_3219,&g_3219,&g_3219,&g_3219,&g_3219,&g_3219,&g_3219,&g_3219};
static int8_t *** volatile g_3221 = &g_3219;/* VOLATILE GLOBAL g_3221 */
static const uint64_t *** const *g_3250 = (void*)0;
static uint16_t *g_3267[7][5] = {{&g_1380,&g_1380,&g_1380,&g_1380,&g_1380},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1380,&g_1380,&g_1380,&g_1380,&g_1380},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1380,&g_1380,&g_1380,&g_1380,&g_1380},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1380,&g_1380,&g_1380,&g_1380,&g_1380}};
static uint32_t g_3378 = 0xEDDEF02AL;
static int16_t g_3504 = (-9L);
static int32_t * const *g_3524 = &g_2734;
static uint32_t ****g_3548 = (void*)0;
static uint32_t *****g_3547 = &g_3548;
static const int32_t *g_3631 = (void*)0;
static const int32_t ** const  volatile g_3630 = &g_3631;/* VOLATILE GLOBAL g_3630 */
static uint32_t *g_3697 = &g_1345;
static volatile int64_t g_3753 = 0x4F639C4BC945732ELL;/* VOLATILE GLOBAL g_3753 */
static volatile int64_t *g_3752 = &g_3753;
static volatile int64_t **g_3751 = &g_3752;
static volatile int64_t ***g_3750 = &g_3751;
static uint32_t *** const g_3772 = &g_2793;
static uint32_t *** const *g_3771[8] = {&g_3772,&g_3772,&g_3772,&g_3772,&g_3772,&g_3772,&g_3772,&g_3772};
static uint32_t *** const **g_3770 = &g_3771[1];
static uint32_t g_3806 = 18446744073709551615UL;
static const float *g_3852 = &g_159;
static const float **g_3851[2] = {&g_3852,&g_3852};
static volatile uint32_t g_3926 = 0xF9235CB4L;/* VOLATILE GLOBAL g_3926 */
static int16_t * const **g_3979 = &g_2139;
static int16_t * const ***g_3978[3][8][7] = {{{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979}},{{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979}},{{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979},{&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979,&g_3979}}};
static int16_t * const ****g_3977 = &g_3978[0][1][5];
static int16_t g_4048 = (-9L);
static uint8_t g_4060[5][3] = {{1UL,3UL,1UL},{1UL,1UL,1UL},{1UL,3UL,1UL},{1UL,1UL,1UL},{1UL,3UL,1UL}};
static float ***g_4071 = &g_609;
static const int32_t *g_4106[10] = {&g_2686,&g_2686,&g_2686,&g_2686,&g_2686,&g_2686,&g_2686,&g_2686,&g_2686,&g_2686};
static const int32_t **g_4105 = &g_4106[2];
static float g_4177 = 0x1.687184p+46;
static float g_4231 = 0x0.Ap-1;
static uint8_t g_4273 = 255UL;
static int32_t g_4437 = 0x9188F1EDL;
static const int32_t ** volatile g_4442 = &g_349[3][7];/* VOLATILE GLOBAL g_4442 */
static uint16_t * const  volatile *** volatile * const  volatile g_4473 = (void*)0;/* VOLATILE GLOBAL g_4473 */
static uint16_t * const  volatile *g_4477 = &g_3267[5][4];
static uint16_t * const  volatile **g_4476 = &g_4477;
static uint16_t * const  volatile *** volatile g_4475 = &g_4476;/* VOLATILE GLOBAL g_4475 */
static uint32_t g_4521 = 1UL;
static uint16_t ***g_4537 = (void*)0;
static uint16_t **** const g_4536 = &g_4537;
static uint16_t **** const *g_4535 = &g_4536;
static const uint16_t g_4559[2][1] = {{0x3A87L},{0x3A87L}};
static const int64_t g_4564 = 0L;
static int32_t g_4631 = 7L;
static const uint16_t g_4633 = 65533UL;
static uint64_t g_4717 = 0UL;
static uint16_t g_4949 = 0UL;
static const float ***g_5314 = &g_3851[1];
static int64_t *g_5404[9] = {&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99};
static int64_t **g_5403 = &g_5404[5];
static int64_t ***g_5402 = &g_5403;
static int64_t **** volatile g_5401[2] = {&g_5402,&g_5402};
static int64_t **** volatile g_5405 = &g_5402;/* VOLATILE GLOBAL g_5405 */
static int32_t g_5419 = (-1L);
static uint32_t g_5588 = 0x23DB322DL;
static uint16_t **g_5719 = &g_3267[5][4];
static uint16_t ** const *g_5718 = &g_5719;
static uint16_t ** const **g_5717 = &g_5718;
static uint64_t ***g_5736 = (void*)0;
static uint64_t ****g_5735 = &g_5736;
static uint64_t ***** const g_5734[7][1][7] = {{{&g_5735,(void*)0,&g_5735,(void*)0,&g_5735,(void*)0,&g_5735}},{{&g_5735,&g_5735,&g_5735,&g_5735,&g_5735,(void*)0,(void*)0}},{{&g_5735,(void*)0,(void*)0,(void*)0,(void*)0,&g_5735,&g_5735}},{{&g_5735,&g_5735,&g_5735,&g_5735,&g_5735,&g_5735,&g_5735}},{{&g_5735,&g_5735,&g_5735,(void*)0,&g_5735,(void*)0,&g_5735}},{{&g_5735,&g_5735,&g_5735,(void*)0,&g_5735,&g_5735,&g_5735}},{{(void*)0,(void*)0,&g_5735,&g_5735,(void*)0,(void*)0,(void*)0}}};
static volatile uint8_t g_5794 = 255UL;/* VOLATILE GLOBAL g_5794 */
static int16_t **g_5879 = &g_252[4][2][1];
static int16_t ** const *g_5878 = &g_5879;
static int16_t ** const **g_5877 = &g_5878;


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static uint16_t  func_19(int32_t  p_20, int32_t * p_21, int16_t  p_22, int32_t * p_23);
static uint64_t  func_24(int64_t  p_25, int32_t * p_26, uint8_t  p_27);
static int32_t * func_32(int32_t * p_33, const int8_t  p_34, int32_t * p_35);
static int64_t  func_38(float  p_39, int32_t * p_40, uint32_t  p_41, int32_t * p_42, int32_t  p_43);
static int32_t * func_44(int32_t * p_45, int16_t  p_46, int64_t  p_47, float  p_48, uint16_t  p_49);
static int32_t * func_51(uint32_t  p_52, float  p_53, int16_t  p_54, const uint32_t  p_55, int64_t  p_56);
static int32_t ** func_57(const int32_t * p_58, int16_t  p_59, uint64_t  p_60);
static int32_t * func_61(int32_t * p_62, const int32_t ** p_63, const uint64_t  p_64);
static int32_t * func_65(int32_t * const * p_66, int32_t * p_67);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_68 g_72 g_99 g_102 g_103 g_101 g_127 g_79 g_200 g_208 g_69 g_160 g_297 g_348 g_226 g_371 g_372 g_373 g_395 g_224 g_448 g_317 g_318 g_493 g_531 g_532 g_533 g_252 g_592 g_609 g_639 g_593 g_634 g_696 g_530 g_635 g_640 g_752 g_753 g_754 g_795 g_907 g_659 g_943 g_945 g_766 g_897 g_1109 g_1380 g_1408 g_1327 g_1328 g_1329 g_1443 g_2734 g_3188 g_1663 g_1664 g_1665 g_2189 g_3751 g_3752 g_3753 g_3524 g_1510 g_2030 g_4536 g_4537 g_1229 g_1230 g_1231 g_1232 g_3979 g_2139 g_2140 g_4048 g_1111 g_896 g_4631 g_4633 g_3750 g_1222 g_3977 g_3978 g_2212 g_3697 g_2663 g_1539 g_4717 g_3852 g_159 g_2810 g_3504 g_1952 g_2142 g_2141 g_3806 g_1345 g_1403 g_1110 g_1980 g_4177 g_4071 g_1378 g_2432 g_1635 g_4535 g_1987 g_1988 g_3378 g_4521 g_2933 g_5405 g_2603 g_1140 g_5419 g_2366 g_2602 g_3221 g_3219 g_3630 g_3631 g_2685 g_5588 g_4476 g_4477 g_3267 g_5717 g_5734 g_4475 g_1610 g_4273 g_5718 g_5719 g_5794 g_4105 g_4106 g_5877 g_5879 g_2601 g_1981 g_2362
 * writes: g_72 g_79 g_99 g_101 g_103 g_127 g_159 g_160 g_68 g_224 g_226 g_252 g_297 g_316 g_349 g_448 g_293 g_493 g_529 g_533 g_609 g_634 g_659 g_671 g_766 g_795 g_896 g_907 g_928 g_943 g_696 g_1109 g_635 g_1378 g_897 g_1980 g_3188 g_2030 g_2142 g_2141 g_4048 g_1510 g_1385 g_1345 g_2663 g_1539 g_208 g_2810 g_3504 g_1380 g_3806 g_4177 g_1403 g_1111 g_1663 g_5314 g_3378 g_2933 g_5402 g_200 g_1988 g_4521 g_2685 g_5588 g_4717 g_4 g_1230 g_4631 g_1140 g_5717 g_1610 g_4273 g_3219 g_5877 g_1636 g_1329 g_3631
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = (void*)0;
    int32_t *l_3 = &g_4[0][0];
    int32_t *l_5 = (void*)0;
    int32_t *l_6 = &g_4[0][0];
    int32_t *l_7 = &g_4[0][0];
    int32_t *l_8 = &g_4[0][0];
    int32_t *l_9 = &g_4[0][0];
    int32_t *l_10[4];
    int32_t l_11 = (-1L);
    uint64_t l_12 = 18446744073709551615UL;
    int32_t l_5602 = 0x2D62B133L;
    int32_t **l_5616 = &g_2362;
    uint64_t ***l_5663[8][2][8] = {{{&g_529[3][1][4],&g_529[2][0][2],&g_529[3][1][4],&g_1232[0][2][5],&g_1232[0][2][5],&g_529[3][1][4],&g_529[2][0][2],&g_529[3][1][4]},{(void*)0,&g_529[3][1][4],&g_1232[0][4][3],&g_1232[0][2][5],(void*)0,&g_1232[0][4][4],&g_529[3][1][4],&g_1232[0][4][0]}},{{(void*)0,&g_1232[0][4][4],&g_529[3][1][4],&g_1232[0][4][0],&g_1232[0][2][5],&g_1232[0][4][4],&g_1232[0][4][4],&g_1232[0][2][5]},{&g_529[3][1][4],&g_529[3][1][4],&g_529[3][1][4],&g_529[3][1][4],(void*)0,&g_529[3][1][4],&g_529[3][1][4],&g_1232[0][2][5]}},{{&g_1232[0][2][5],&g_529[2][0][2],&g_1232[0][4][3],&g_1232[0][4][0],(void*)0,&g_529[3][1][4],&g_529[2][0][2],&g_1232[0][4][0]},{&g_529[3][1][4],&g_529[2][0][2],&g_529[3][1][4],&g_1232[0][2][5],&g_1232[0][2][5],&g_529[3][1][4],&g_529[2][0][2],&g_529[3][1][4]}},{{(void*)0,&g_529[3][1][4],&g_1232[0][4][3],&g_1232[0][2][5],(void*)0,&g_1232[0][4][4],&g_529[3][1][4],(void*)0},{&g_1232[0][2][5],&g_529[3][1][4],&g_529[3][1][4],(void*)0,&g_529[3][1][4],&g_529[3][1][4],&g_529[3][1][4],&g_529[3][1][4]}},{{&g_1232[0][4][0],&g_529[3][1][4],&g_529[3][1][4],&g_1232[0][4][0],(void*)0,&g_1232[0][4][3],&g_529[3][1][4],&g_529[3][1][4]},{&g_529[3][1][4],&g_1232[0][4][4],&g_529[2][0][2],(void*)0,(void*)0,&g_529[3][1][4],&g_1232[0][4][4],(void*)0}},{{&g_1232[0][4][0],&g_1232[0][4][4],&g_1232[0][4][3],&g_529[3][1][4],&g_529[3][1][4],&g_1232[0][4][3],&g_1232[0][4][4],&g_1232[0][4][0]},{&g_1232[0][2][5],&g_529[3][1][4],&g_529[2][0][2],&g_529[3][1][4],&g_1232[0][2][5],&g_529[3][1][4],&g_529[3][1][4],(void*)0}},{{&g_1232[0][2][5],&g_529[3][1][4],&g_529[3][1][4],(void*)0,&g_529[3][1][4],&g_529[3][1][4],&g_529[3][1][4],&g_529[3][1][4]},{&g_1232[0][4][0],&g_529[3][1][4],&g_529[3][1][4],&g_1232[0][4][0],(void*)0,&g_1232[0][4][3],&g_529[3][1][4],&g_529[3][1][4]}},{{&g_529[3][1][4],&g_1232[0][4][4],&g_529[2][0][2],(void*)0,(void*)0,&g_529[3][1][4],&g_1232[0][4][4],(void*)0},{&g_1232[0][4][0],&g_1232[0][4][4],&g_1232[0][4][3],&g_529[3][1][4],&g_529[3][1][4],&g_1232[0][4][3],&g_1232[0][4][4],&g_1232[0][4][0]}}};
    uint64_t ****l_5662[7];
    const float ***** const l_5687 = (void*)0;
    uint8_t ***l_5724[3][4] = {{&g_317,&g_317,&g_317,&g_317},{&g_317,&g_317,(void*)0,&g_317},{&g_317,&g_317,&g_317,&g_317}};
    uint64_t l_5725 = 0x5E0BAE8589AEE038LL;
    int8_t **l_5748 = &g_3220;
    int16_t l_5834[5];
    uint16_t l_5866 = 0xD93BL;
    int32_t l_5867[7][3][5] = {{{1L,0L,0L,1L,0xCC596C90L},{0x451D23E1L,0xA3777421L,0x6B9FADE7L,(-6L),(-2L)},{0xCC596C90L,0L,(-1L),8L,8L}},{{0xC2ACEA21L,1L,0xC2ACEA21L,(-6L),(-1L)},{0x5E3BA766L,1L,8L,1L,0x5E3BA766L},{0xC2ACEA21L,(-9L),0xFCE2A224L,0xA3777421L,0xFCE2A224L}},{{0xCC596C90L,0xCC596C90L,8L,0x5E3BA766L,(-7L)},{0x451D23E1L,(-1L),0xC2ACEA21L,(-9L),0xFCE2A224L},{1L,0x5E3BA766L,(-1L),(-1L),0x5E3BA766L}},{{0xFCE2A224L,(-1L),0x6B9FADE7L,0x59201B5AL,(-1L)},{0L,0xCC596C90L,0L,(-1L),8L},{(-2L),(-9L),(-1L),(-9L),(-2L)}},{{0L,1L,0xCC596C90L,0x5E3BA766L,0xCC596C90L},{0xFCE2A224L,1L,(-1L),0xA3777421L,0L},{1L,0L,0L,1L,0xCC596C90L}},{{0x451D23E1L,0xA3777421L,0x6B9FADE7L,(-6L),(-2L)},{0xCC596C90L,0L,(-1L),8L,8L},{0xC2ACEA21L,1L,0xC2ACEA21L,(-6L),(-1L)}},{{0x5E3BA766L,1L,8L,1L,0x5E3BA766L},{0xC2ACEA21L,(-9L),0xFCE2A224L,0xA3777421L,0xFCE2A224L},{0xCC596C90L,0xCC596C90L,8L,0x5E3BA766L,(-7L)}}};
    int16_t ***l_5895 = &g_5879;
    const uint32_t l_5943 = 4294967295UL;
    float l_5972 = 0x9.8FF56Ap+13;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_10[i] = &g_4[0][0];
    for (i = 0; i < 7; i++)
        l_5662[i] = &l_5663[7][1][6];
    for (i = 0; i < 5; i++)
        l_5834[i] = 1L;
    l_12++;
    if ((g_5588 ^= (g_2685 &= (safe_add_func_int16_t_s_s(g_4[0][0], ((safe_sub_func_uint32_t_u_u(0UL, (func_19(g_4[0][0], &l_11, ((func_24((safe_mul_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(((*g_530) = (&l_11 != ((*l_8) , func_32(&g_4[0][0], g_4[0][0], &l_11)))), g_4633)), (*l_9))), l_7, (*l_9)) >= 0L) , 4L), l_10[2]) & 0xA274L))) == (*l_8)))))))
    { /* block id: 2444 */
        uint32_t l_5598 = 18446744073709551615UL;
        uint32_t l_5607 = 1UL;
        uint8_t l_5611 = 248UL;
        int32_t l_5628 = 0x2D6C463FL;
        uint32_t l_5653[9][7] = {{0x102AF984L,0x323092BBL,0x102AF984L,0x3EB5A247L,18446744073709551612UL,0x102AF984L,0x75BD5E7FL},{18446744073709551612UL,0x323092BBL,1UL,0x8EBBA4D3L,0x323092BBL,0xBA85611BL,0x323092BBL},{18446744073709551615UL,0x3EB5A247L,0x3EB5A247L,18446744073709551615UL,0x75BD5E7FL,0x102AF984L,18446744073709551612UL},{0x6555E0ADL,0x0B5F68AEL,0x3EB5A247L,0x3583BE6EL,18446744073709551612UL,0x6555E0ADL,0x6555E0ADL},{0x0B5F68AEL,18446744073709551612UL,1UL,18446744073709551612UL,0x0B5F68AEL,0x4B16B3AEL,18446744073709551612UL},{18446744073709551615UL,0x75BD5E7FL,0x102AF984L,18446744073709551612UL,0x3EB5A247L,0x102AF984L,0x323092BBL},{0x3EB5A247L,0x323092BBL,0x3583BE6EL,0x3583BE6EL,0x323092BBL,0x3EB5A247L,0x75BD5E7FL},{18446744073709551615UL,18446744073709551612UL,0xBA85611BL,18446744073709551615UL,0x323092BBL,0x49B3EDC2L,18446744073709551612UL},{0x0B5F68AEL,0x6555E0ADL,0x3EB5A247L,0x8EBBA4D3L,0x3EB5A247L,0x6555E0ADL,0x0B5F68AEL}};
        uint32_t l_5654 = 1UL;
        const int16_t ***l_5657 = &g_1328;
        const int16_t ****l_5656 = &l_5657;
        const int16_t *****l_5655 = &l_5656;
        int32_t l_5689[3];
        int32_t l_5696 = 0x5F49E5CEL;
        uint8_t ** const * const l_5708[2] = {&g_317,&g_317};
        uint8_t l_5804 = 0xD8L;
        int i, j;
        for (i = 0; i < 3; i++)
            l_5689[i] = 0x2F3DE89AL;
        for (g_4717 = (-11); (g_4717 >= 12); g_4717 = safe_add_func_int32_t_s_s(g_4717, 2))
        { /* block id: 2447 */
            float l_5601 = 0x8.22D060p-39;
            int8_t *l_5610 = &g_635;
            int32_t l_5612 = 0L;
            int32_t l_5613[6][6] = {{0L,1L,1L,0L,1L,1L},{0L,1L,1L,0L,1L,1L},{0L,1L,1L,0L,1L,1L},{0L,1L,1L,0L,1L,1L},{0L,1L,1L,0L,1L,1L},{0L,1L,1L,0L,1L,1L}};
            int32_t ***l_5617 = &l_5616;
            int32_t **l_5619[2][8][10] = {{{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362}},{{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362},{&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362,&g_2362}}};
            int32_t ***l_5618 = &l_5619[1][2][4];
            int i, j, k;
            (*l_6) = ((safe_sub_func_uint16_t_u_u((safe_unary_minus_func_int64_t_s(((void*)0 == (**g_4476)))), (l_5612 = (((safe_lshift_func_int8_t_s_s((safe_div_func_uint32_t_u_u(l_5598, ((safe_sub_func_int32_t_s_s((0x77L >= ((*l_5610) = (l_5602 && ((((safe_sub_func_int64_t_s_s((*g_3752), (safe_rshift_func_uint16_t_u_u((l_5607 > ((*g_530) = (*l_8))), (safe_add_func_int64_t_s_s(0xE2C26B1CC810FEAELL, l_5598)))))) ^ l_5607) >= (*l_7)) & (*g_3697))))), l_5611)) ^ 0xCAB134DCCA64C22FLL))), 7)) | 1UL) , l_5611)))) & l_5613[5][2]);
            (*g_2734) &= (*l_9);
            (**g_3524) |= ((((0x2D0AL >= (***g_3979)) == (safe_rshift_func_int16_t_s_u((((*l_5618) = ((*l_5617) = l_5616)) != (void*)0), l_5613[5][2]))) > ((((((*g_530) & ((safe_add_func_int8_t_s_s(((safe_add_func_uint16_t_u_u((**g_752), (((((safe_add_func_int8_t_s_s(l_5613[2][3], ((((((safe_div_func_uint8_t_u_u(1UL, l_5613[5][2])) > 253UL) | l_5613[5][2]) , l_5598) , (-1L)) || l_5628))) < l_5612) || l_5612) , 0x88L) , (*g_1329)))) , (*l_7)), 0L)) <= l_5613[5][2])) & (*l_9)) , 6L) | (*l_6)) ^ 1L)) | l_5613[4][3]);
            (*l_3) ^= 0x7E45B26FL;
        }
        for (g_4717 = (-21); (g_4717 > 44); ++g_4717)
        { /* block id: 2460 */
            int16_t l_5641 = 7L;
            int8_t *l_5642 = &g_1980[0];
            int32_t l_5660 = 9L;
            int8_t *l_5661 = &g_1378;
            float *****l_5682[8] = {&g_2211[0][4][2],&g_2211[2][5][2],&g_2211[0][4][2],&g_2211[0][4][2],&g_2211[2][5][2],&g_2211[0][4][2],&g_2211[0][4][2],&g_2211[2][5][2]};
            int32_t l_5690 = 0xD1CC8849L;
            int32_t l_5691 = 1L;
            int32_t l_5693 = 0x4AB92C62L;
            int32_t l_5695 = 0xDE99990AL;
            int8_t l_5722 = 0x9BL;
            int i;
            if ((safe_lshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s(((*l_5661) = (((*l_5642) |= (safe_mul_func_int8_t_s_s(0x98L, l_5641))) == ((safe_rshift_func_uint16_t_u_s((l_5660 ^= ((((safe_mod_func_int8_t_s_s(((safe_sub_func_uint64_t_u_u(((safe_div_func_int32_t_s_s(l_5641, l_5653[2][6])) != (l_5654 <= (&g_3978[1][5][2] != l_5655))), ((*l_7) <= (safe_lshift_func_uint8_t_u_s((*l_8), 2))))) || 0L), l_5611)) & 0UL) && 0xA8CFB86BL) == (***g_3750))), 0)) == l_5641))), l_5611)), l_5641)), l_5641)), l_5607)))
            { /* block id: 2464 */
                (*g_1229) = l_5662[4];
            }
            else
            { /* block id: 2466 */
                uint16_t l_5664 = 0x39EDL;
                return l_5664;
            }
            for (g_766 = (-23); (g_766 < 38); g_766++)
            { /* block id: 2471 */
                uint64_t **l_5672 = &g_530;
                int32_t l_5675[2];
                uint16_t *l_5730 = (void*)0;
                int i;
                for (i = 0; i < 2; i++)
                    l_5675[i] = (-7L);
                for (g_4631 = (-4); (g_4631 > 10); g_4631 = safe_add_func_uint16_t_u_u(g_4631, 7))
                { /* block id: 2474 */
                    const uint64_t *l_5670 = (void*)0;
                    const uint64_t **l_5669 = &l_5670;
                    const uint64_t ***l_5671 = &l_5669;
                    (**g_2602) = (((*l_5671) = l_5669) != l_5672);
                }
                for (g_696 = 0; (g_696 == (-17)); g_696--)
                { /* block id: 2480 */
                    uint8_t l_5678 = 0xB6L;
                    int32_t l_5688[7][10] = {{0x3F069F4FL,0xF3B4AA75L,0xF3B4AA75L,0x3F069F4FL,0xFD01BB9DL,0xB436D16BL,0xBB6B37E6L,0xF3B4AA75L,0xB436D16BL,0x5D673176L},{0xB436D16BL,0xBB6B37E6L,0xF3B4AA75L,0xB436D16BL,0x5D673176L,0xB436D16BL,0xF3B4AA75L,0xBB6B37E6L,0xB436D16BL,0xFD01BB9DL},{0x3F069F4FL,0xBB6B37E6L,0x326BE585L,0x3F069F4FL,0x5D673176L,0xAEE05F14L,0xBB6B37E6L,0xBB6B37E6L,0xAEE05F14L,0x5D673176L},{0x3F069F4FL,0xF3B4AA75L,0xF3B4AA75L,0x3F069F4FL,0xFD01BB9DL,0xB436D16BL,0xBB6B37E6L,0xF3B4AA75L,0xB436D16BL,0x5D673176L},{0xB436D16BL,0xBB6B37E6L,0xF3B4AA75L,0xB436D16BL,0x5D673176L,0xB436D16BL,0xF3B4AA75L,0xBB6B37E6L,0xB436D16BL,0xFD01BB9DL},{0x3F069F4FL,0xBB6B37E6L,0x326BE585L,0x3F069F4FL,0x5D673176L,0xAEE05F14L,0xBB6B37E6L,0xBB6B37E6L,0xAEE05F14L,0x5D673176L},{0x3F069F4FL,0xF3B4AA75L,0xF3B4AA75L,0x3F069F4FL,0xFD01BB9DL,0xB436D16BL,0xBB6B37E6L,0xF3B4AA75L,0xB436D16BL,0x5D673176L}};
                    uint16_t l_5697 = 0UL;
                    int32_t l_5723[2][1][6] = {{{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)}},{{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)}}};
                    int i, j, k;
                    for (g_1345 = 0; (g_1345 <= 8); g_1345 += 1)
                    { /* block id: 2483 */
                        if (l_5675[0])
                            break;
                    }
                    if ((((safe_sub_func_int32_t_s_s((((0x18C121D7L < (&g_1663 == (l_5678 , &g_1663))) || l_5611) >= (!((safe_mul_func_int8_t_s_s(((((l_5682[3] = &g_2211[0][0][0]) != (((((l_5678 <= ((*l_5661) = ((*l_8) == (!(safe_unary_minus_func_uint16_t_u((*g_753))))))) >= l_5678) , &l_5672) != (*g_1230)) , l_5687)) == 4294967286UL) & l_5678), l_5628)) != 0x75D761979A3E6D10LL))), l_5678)) , (***g_3979)) != l_5678))
                    { /* block id: 2488 */
                        int8_t l_5692[6][5] = {{0x70L,(-1L),0x70L,(-1L),(-1L)},{0x0CL,(-1L),(-1L),0x0CL,(-1L)},{0x0CL,0x0CL,0x63L,(-1L),0x12L},{0x70L,0x12L,(-1L),(-1L),0x12L},{0x12L,(-1L),0x70L,0x12L,(-1L)},{(-1L),0x12L,0x63L,0x12L,(-1L)}};
                        int32_t l_5694[6][2][4] = {{{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L},{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L}},{{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L},{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L}},{{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L},{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L}},{{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L},{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L}},{{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L},{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L}},{{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L},{0xB7859DE4L,0xB7859DE4L,0x88B48028L,0x88B48028L}}};
                        int64_t l_5700 = 0x3BF9846DF827BAE9LL;
                        int64_t l_5707[5][7][7] = {{{3L,0x76DD5D60F9B7536ELL,0x79BDDAD7F3093BDDLL,4L,0x4AF8B2F099BB045ELL,4L,0x79BDDAD7F3093BDDLL},{0x7F92EEE487EFD8DBLL,0x7F92EEE487EFD8DBLL,0x61F13C2ACAE3F408LL,0xE954527CB4C8905ALL,3L,0x4D19BC6A2B62597CLL,8L},{0L,(-8L),(-5L),3L,0x1406DE13414D2406LL,(-3L),0x76DD5D60F9B7536ELL},{1L,4L,4L,(-8L),3L,6L,(-1L)},{0x605972310B5EF86FLL,0x4AF8B2F099BB045ELL,0x00A01310D5A8411ALL,(-2L),0x4AF8B2F099BB045ELL,0x61F13C2ACAE3F408LL,4L},{0xF6A446A5A969E7ECLL,1L,(-8L),0xB767ED8126A28344LL,(-1L),1L,0x76DD5D60F9B7536ELL},{(-3L),0x7F92EEE487EFD8DBLL,1L,(-5L),(-1L),0x2DDD244152E427E6LL,(-2L)}},{{6L,3L,4L,0x605972310B5EF86FLL,0x7F92EEE487EFD8DBLL,0x567CBBEA51B9392ALL,0x605972310B5EF86FLL},{0xB767ED8126A28344LL,(-3L),4L,0x605972310B5EF86FLL,0xB027863EBAA5804CLL,3L,0x4AF8B2F099BB045ELL},{0x79BDDAD7F3093BDDLL,4L,0x2625FC4155EF488FLL,(-5L),0x0E012878F5B0DA9CLL,0x79BDDAD7F3093BDDLL,(-1L)},{0xFFBEBFB84B9D1316LL,(-1L),1L,0xB767ED8126A28344LL,0xB2A1D23538B60046LL,0L,3L},{8L,0x1406DE13414D2406LL,(-1L),(-2L),(-1L),0x1406DE13414D2406LL,8L},{(-2L),0x76DD5D60F9B7536ELL,0x1017EDB7D11A1ADCLL,(-8L),0x7F92EEE487EFD8DBLL,0x2DDD244152E427E6LL,(-1L)},{0xFFBEBFB84B9D1316LL,0x0E012878F5B0DA9CLL,(-1L),3L,0x605972310B5EF86FLL,5L,0x0E012878F5B0DA9CLL}},{{0x605972310B5EF86FLL,(-1L),0x1017EDB7D11A1ADCLL,0xE954527CB4C8905ALL,1L,0x79BDDAD7F3093BDDLL,0x752505B464F00EFDLL},{4L,0x886027C8B7B7FA52LL,(-1L),4L,0x752505B464F00EFDLL,6L,(-8L)},{6L,0x7F92EEE487EFD8DBLL,1L,0xB00DD6276FEA69C8LL,0x605972310B5EF86FLL,(-1L),8L},{(-7L),0x79BDDAD7F3093BDDLL,0x2625FC4155EF488FLL,0x76DD5D60F9B7536ELL,0x1406DE13414D2406LL,0xADDA2D59EFDCF9F8LL,(-1L)},{6L,0x752505B464F00EFDLL,4L,(-1L),0x886027C8B7B7FA52LL,4L,1L},{0x605972310B5EF86FLL,0x752505B464F00EFDLL,4L,0xC1AC0D7F6D07026ELL,0xB2A1D23538B60046LL,0x61F13C2ACAE3F408LL,0x0E012878F5B0DA9CLL},{0xB5D1191455F31BBDLL,0x79BDDAD7F3093BDDLL,1L,(-5L),(-1L),4L,4L}},{{0x83EA79B2F8EEF51ALL,0x79BDDAD7F3093BDDLL,5L,0x8CCDD45805BD5A4DLL,6L,1L,1L},{0x79BDDAD7F3093BDDLL,4L,0x4AF8B2F099BB045ELL,4L,0x79BDDAD7F3093BDDLL,0x76DD5D60F9B7536ELL,3L},{0x2625FC4155EF488FLL,0x4D19BC6A2B62597CLL,0x936F45225DD064C8LL,6L,5L,4L,0xADDA2D59EFDCF9F8LL},{0xF0C146317118B5C2LL,(-3L),0xDACD309FAE4E658FLL,7L,0x2DDD244152E427E6LL,0xF0C146317118B5C2LL,0x567CBBEA51B9392ALL},{0x2625FC4155EF488FLL,6L,0xFFBEBFB84B9D1316LL,(-5L),0x1406DE13414D2406LL,0xE4ACBE9D3A8307C0LL,0xFC1D4ED1DB738689LL},{0x79BDDAD7F3093BDDLL,0x61F13C2ACAE3F408LL,0xF0C146317118B5C2LL,(-1L),0L,(-1L),0xB027863EBAA5804CLL},{0x83EA79B2F8EEF51ALL,1L,0xDACD309FAE4E658FLL,0L,0x79BDDAD7F3093BDDLL,(-10L),(-6L)}},{{0x34E2D3CC6A7B2E7CLL,0x2DDD244152E427E6LL,0xE4ACBE9D3A8307C0LL,1L,3L,0xC1AC0D7F6D07026ELL,0x2DDD244152E427E6LL},{0xFC1D4ED1DB738689LL,0x567CBBEA51B9392ALL,(-2L),(-3L),0x567CBBEA51B9392ALL,0xF0C146317118B5C2LL,0x2DDD244152E427E6LL},{0L,3L,5L,0x936F45225DD064C8LL,0x2DDD244152E427E6LL,0x00A01310D5A8411ALL,(-6L)},{(-8L),0x79BDDAD7F3093BDDLL,(-3L),1L,1L,3L,0xB027863EBAA5804CLL},{(-1L),0L,5L,0xFC1D4ED1DB738689LL,0x61F13C2ACAE3F408LL,0x61F13C2ACAE3F408LL,0xFC1D4ED1DB738689LL},{0x936F45225DD064C8LL,0x1406DE13414D2406LL,0x936F45225DD064C8LL,0xF0C146317118B5C2LL,6L,0L,0x567CBBEA51B9392ALL},{0xFC1D4ED1DB738689LL,0x2DDD244152E427E6LL,0xB2A1D23538B60046LL,1L,(-3L),0xFFBEBFB84B9D1316LL,0xADDA2D59EFDCF9F8LL}}};
                        uint16_t ** const ***l_5720 = (void*)0;
                        uint16_t ** const ***l_5721 = &g_5717;
                        int i, j, k;
                        l_5697++;
                        (**g_2602) = (&g_1109 != ((((((l_5692[1][2] ^ (l_5700 , 0xF6L)) >= l_5693) , (l_5694[0][1][3] , (safe_rshift_func_uint8_t_u_u((safe_mod_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(l_5707[4][0][6], (l_5708[0] == (((safe_div_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s((l_5723[1][0][5] = ((***g_3979) = (((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((((*l_5721) = g_5717) != (void*)0), 4)), l_5691)) < l_5675[0]) , l_5722))), 12)), l_5688[6][7])) , (*l_7)) , l_5724[1][1])))), (*l_9))), l_5675[1])))) && 0x98E99897L) != (*l_6)) , &g_1109));
                        l_5688[5][8] = (l_5688[2][3] != (l_5628 <= (l_5691 = ((l_5725 > (safe_rshift_func_uint8_t_u_s(1UL, 2))) , (l_5689[0] = (-0x1.0p+1))))));
                    }
                    else
                    { /* block id: 2497 */
                        (*l_9) &= ((*g_3752) | (l_5675[0] && (l_5641 == (safe_mod_func_int32_t_s_s((&g_4536 == (void*)0), 0xCBA09D4FL)))));
                    }
                    (**g_3524) = (((l_5628 < (l_5730 == ((safe_add_func_int8_t_s_s(((*l_5661) = (+((g_5734[3][0][6] != &l_5662[4]) | (safe_rshift_func_int16_t_s_s(((l_5675[0] = (*l_3)) || l_5675[0]), 15))))), l_5695)) , (***g_4475)))) , ((safe_div_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u(7UL, 0xA385L)) >= 7L), l_5697)) & 4294967295UL)) & l_5678);
                }
            }
        }
        for (g_1610 = 0; (g_1610 == (-15)); g_1610 = safe_sub_func_uint64_t_u_u(g_1610, 8))
        { /* block id: 2508 */
            int8_t **l_5747 = &g_3220;
            const int32_t l_5760 = 0x90BE4EB1L;
            const uint32_t *l_5772 = &g_3806;
            uint8_t l_5777 = 249UL;
            const uint16_t l_5796 = 0x5C47L;
            int64_t l_5803 = (-2L);
            for (g_4273 = 0; (g_4273 < 9); g_4273++)
            { /* block id: 2511 */
                int8_t ***l_5749 = (void*)0;
                int8_t ***l_5750 = &g_3219;
                int32_t l_5757 = (-6L);
                float l_5771 = 0x4.5A7006p-91;
                uint16_t **l_5792[9] = {&g_3267[2][3],&g_3267[5][4],&g_3267[5][4],&g_3267[2][3],&g_3267[5][4],&g_3267[5][4],&g_3267[2][3],&g_3267[5][4],&g_3267[5][4]};
                uint64_t *l_5800 = &g_1511[0][3];
                int i;
                (*g_69) |= (l_5747 == ((*l_5750) = l_5748));
                for (g_2142 = (-4); (g_2142 >= 14); g_2142 = safe_add_func_int64_t_s_s(g_2142, 1))
                { /* block id: 2516 */
                    int64_t *l_5764 = &g_99;
                    int32_t l_5769 = 0L;
                    (**g_1664) = (void*)0;
                    if ((((safe_lshift_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u((((l_5757 ^ ((*l_7) ^= l_5757)) , l_9) != l_5772), 65526UL)) > l_5757), l_5769)) || (***g_1327)) , 7L))
                    { /* block id: 2520 */
                        return l_5654;
                    }
                    else
                    { /* block id: 2522 */
                        return (*l_7);
                    }
                }
                for (g_1380 = 0; (g_1380 <= 0); g_1380 += 1)
                { /* block id: 2528 */
                    uint8_t l_5778 = 0x8CL;
                    int8_t l_5793 = 1L;
                    int8_t *l_5795 = &g_297;
                    int32_t l_5797 = 0x3DCA861CL;
                    int i;
                    l_5797 &= ((safe_div_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u(l_5777, 9)), ((l_5778 <= ((safe_div_func_int32_t_s_s((safe_add_func_int32_t_s_s((safe_mul_func_int8_t_s_s((*l_9), ((*l_5795) &= (safe_rshift_func_int16_t_s_s(((safe_div_func_int32_t_s_s((((~(((safe_mul_func_uint16_t_u_u(l_5757, (6L > (((((*g_2734) &= l_5777) & ((l_5792[4] == (**g_5717)) || (l_5778 || (**g_1328)))) , l_5757) | l_5793)))) > l_5777) != l_5760)) < 7L) == g_5794), l_5778)) | l_5793), l_5757))))), l_5796)), l_5793)) != 0x54L)) && (*g_2189)))) >= l_5793);
                    (*g_1665) = &l_5689[2];
                    return l_5598;
                }
                l_5689[2] = (l_5696 != (safe_mul_func_float_f_f(((*g_4105) != (((void*)0 != l_5800) , (*g_4105))), (safe_add_func_float_f_f(l_5654, (l_5607 >= (l_5803 >= 0x1.6p-1)))))));
            }
        }
        l_5804++;
    }
    else
    { /* block id: 2539 */
        uint64_t l_5835[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
        int32_t l_5838 = 0L;
        int16_t ****l_5876 = &g_1636;
        uint8_t *****l_5917 = &g_1988;
        uint64_t *****l_5942 = &l_5662[3];
        float l_5944 = 0x3.6CB95Dp+66;
        int16_t l_5945 = 0xB404L;
        int32_t l_5973 = 0xC73AA5EEL;
        int i;
        for (g_1403 = 0; (g_1403 < 4); ++g_1403)
        { /* block id: 2542 */
            int32_t l_5809[10] = {0x3376657DL,0x3376657DL,0x1039ECA3L,0x3376657DL,0x3376657DL,0x1039ECA3L,0x3376657DL,0x3376657DL,0x1039ECA3L,0x3376657DL};
            int32_t l_5883 = 0x46B51E57L;
            int16_t ***l_5896 = (void*)0;
            int16_t l_5939 = (-1L);
            uint16_t *****l_5958 = (void*)0;
            int i;
            if (l_5809[8])
            { /* block id: 2543 */
                int32_t l_5812 = 0x87B1B4C6L;
                uint32_t l_5833 = 4294967287UL;
                uint64_t l_5839[1][5][2] = {{{18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,0UL},{0xFA28D7A700D4204DLL,0x4C043533BFABD5ECLL},{0UL,0x4C043533BFABD5ECLL},{0xFA28D7A700D4204DLL,0UL}}};
                int i, j, k;
                for (g_4717 = (-20); (g_4717 < 31); g_4717++)
                { /* block id: 2546 */
                    int64_t l_5832 = 0L;
                    int32_t l_5836 = 0x0E2C2E6BL;
                    if (l_5812)
                    { /* block id: 2547 */
                        int8_t l_5813[3][1][9] = {{{(-1L),0xB3L,0xE1L,0xB3L,(-1L),0x51L,(-1L),6L,6L}},{{0xB3L,0xFDL,(-1L),6L,(-1L),0xFDL,0xB3L,(-1L),0x0CL}},{{(-1L),0x0CL,0xB3L,0x51L,(-1L),0x51L,0xB3L,0x0CL,(-1L)}}};
                        int i, j, k;
                        if (l_5813[0][0][6])
                            break;
                        return (**g_2139);
                    }
                    else
                    { /* block id: 2550 */
                        uint32_t l_5821 = 0x86B0A328L;
                        int8_t *l_5827 = &g_1980[1];
                        int32_t l_5837[2][4][10] = {{{9L,(-9L),0x004DCEF3L,0L,0xD99B59F7L,2L,0x09FB77B5L,(-1L),(-1L),(-1L)},{1L,0x8609C133L,(-9L),0x24362C75L,(-9L),0x8609C133L,1L,9L,(-10L),0xC0677F86L},{0x0D4F67A1L,(-6L),0xB9716547L,0xC0677F86L,0x2CF2AF4CL,0xB4937498L,1L,0xD99B59F7L,9L,0xC36E6171L},{0xD99B59F7L,2L,0x24362C75L,2L,(-1L),2L,(-9L),2L,2L,(-9L)}},{{0x24362C75L,(-10L),0xB4937498L,0xB4937498L,(-10L),0x24362C75L,0L,0x09FB77B5L,(-4L),0xC0677F86L},{9L,8L,(-9L),(-1L),0xB4937498L,0xC36E6171L,(-4L),0xE0909B9DL,0x8609C133L,0xD99B59F7L},{9L,0xF28A3F8DL,0xFC4F3928L,2L,2L,0x24362C75L,0xD99B59F7L,(-10L),0xD99B59F7L,0x24362C75L},{0x24362C75L,0xD99B59F7L,(-10L),0xD99B59F7L,0x24362C75L,2L,2L,0xFC4F3928L,0xF28A3F8DL,9L}}};
                        int i, j, k;
                        (**g_3524) ^= ((safe_add_func_uint64_t_u_u((((void*)0 == &g_2211[2][5][2]) != ((safe_sub_func_int8_t_s_s((+l_5809[8]), (((safe_div_func_uint32_t_u_u(l_5821, ((((l_5838 |= ((((l_5821 | (((+(safe_add_func_uint8_t_u_u(l_5821, (l_5837[0][1][1] = (safe_mod_func_uint8_t_u_u((((*l_5827) = 0x83L) < (safe_rshift_func_uint8_t_u_s((((**g_371) || (((((safe_add_func_int32_t_s_s((*l_3), l_5809[0])) , l_5832) && l_5833) && l_5834[3]) & l_5832)) , l_5835[1]), 4))), l_5836)))))) | 0x3304EF37A6FF1038LL) >= (*g_3697))) , (**g_3750)) != (void*)0) == 0x022DBCF2C743A86ELL)) | 4294967288UL) >= 0xB0L) ^ l_5809[1]))) , 0x19BE244D7C0AF784LL) , l_5833))) >= l_5832)), (*g_530))) != (-6L));
                    }
                    if (l_5835[1])
                    { /* block id: 2556 */
                        return l_5839[0][1][0];
                    }
                    else
                    { /* block id: 2558 */
                        return (***g_3979);
                    }
                }
            }
            else
            { /* block id: 2562 */
                uint64_t l_5852 = 18446744073709551608UL;
                uint64_t ***l_5865 = (void*)0;
                const int16_t *l_5902 = (void*)0;
                int32_t l_5907[1];
                uint32_t *l_5908 = &g_200[1][4][0];
                uint64_t l_5912 = 3UL;
                uint32_t * const **l_5937 = (void*)0;
                uint32_t **l_5950 = &g_3697;
                int64_t *l_5951[6][7] = {{&g_2663,&g_659,&g_659,&g_2663,&g_659,&g_659,&g_2663},{&g_659,&g_2663,&g_659,&g_659,&g_2663,&g_659,&g_659},{&g_2663,&g_2663,&g_99,&g_2663,&g_2663,&g_99,&g_2663},{&g_2663,&g_659,&g_659,&g_2663,&g_659,&g_659,&g_2663},{&g_659,&g_2663,&g_659,&g_659,&g_2663,&g_659,&g_659},{&g_2663,&g_2663,&g_99,&g_2663,&g_2663,&g_99,&g_2663}};
                int i, j;
                for (i = 0; i < 1; i++)
                    l_5907[i] = (-1L);
                if (((safe_mod_func_uint32_t_u_u((--(*g_3697)), 0x929BC152L)) == (((safe_add_func_int32_t_s_s((safe_sub_func_int16_t_s_s((safe_rshift_func_int16_t_s_s((~(~l_5852)), 5)), (l_5852 , 0xFCEAL))), (((safe_sub_func_int64_t_s_s(((safe_rshift_func_int16_t_s_u(((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s(l_5852, 13)), 15)), (4294967295UL & l_5809[9]))), (1L <= (l_5865 != l_5865)))) != (-1L)), 10)) >= l_5838), l_5866)) , l_5867[6][0][0]) | 0x6CFE0A70L))) & 0x7C8C598DE62DBE86LL) ^ l_5809[9])))
                { /* block id: 2564 */
                    int16_t ** const ***l_5880[1][7][7];
                    float *l_5881 = (void*)0;
                    float *l_5882[3];
                    int32_t l_5886 = 0xAB028F97L;
                    float l_5887 = 0x1.CCFA76p-5;
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 7; j++)
                        {
                            for (k = 0; k < 7; k++)
                                l_5880[i][j][k] = (void*)0;
                        }
                    }
                    for (i = 0; i < 3; i++)
                        l_5882[i] = &g_290;
                    (**g_2602) = (l_5838 = (safe_div_func_float_f_f(((safe_sub_func_float_f_f(0x1.8B1FAEp-51, (safe_sub_func_float_f_f(((l_5883 = ((*g_3697) , (safe_add_func_float_f_f((l_5876 != (g_5877 = g_5877)), 0xE.E4100Fp-25)))) > 0x1.92C3CBp-40), (((safe_sub_func_float_f_f(0xB.AE0C95p+22, (l_5886 , (((*g_102) = (l_5852 != l_5809[8])) != l_5887)))) > (*l_9)) != (*l_3)))))) == l_5886), 0x5.6A16ECp-88)));
                    if ((safe_sub_func_int8_t_s_s((((!(0xB.942310p+5 != (safe_mul_func_float_f_f(((((-0x3.2p+1) > (safe_mul_func_float_f_f(((l_5809[8] , l_5835[2]) , (l_5895 != ((*g_1635) = l_5896))), ((safe_sub_func_float_f_f((-(safe_div_func_float_f_f((((*g_102) = ((l_5907[0] = (((((*g_1328) = l_5902) != ((*g_5879) = (**l_5895))) != (l_5883 = ((!(safe_mul_func_float_f_f(((((-(***g_2601)) <= l_5852) < (*l_8)) <= l_5907[0]), 0xC.CBCED0p-36))) != l_5907[0]))) == l_5809[0])) <= l_5835[5])) == l_5886), l_5886))), 0x8.020216p+55)) >= l_5886)))) , 0x6.C721FBp-83) == 0x2.7E084Ap+70), l_5852)))) , (void*)0) != l_5908), l_5886)))
                    { /* block id: 2576 */
                        (*l_7) |= l_5886;
                        (*g_3630) = (void*)0;
                        (*l_9) |= l_5838;
                    }
                    else
                    { /* block id: 2580 */
                        float l_5909 = 0x4.94F8FFp-85;
                        return l_5907[0];
                    }
                }
                else
                { /* block id: 2583 */
                    uint32_t l_5910 = 0x7B37B226L;
                    int8_t *l_5911 = &g_297;
                    int8_t *l_5938 = &g_226;
                    int32_t l_5940 = 0x7EF9CB2BL;
                    int32_t l_5941 = 0L;
                    int8_t l_5946[10];
                    int32_t l_5947[6] = {(-1L),0L,0L,(-1L),0L,0L};
                    int i;
                    for (i = 0; i < 10; i++)
                        l_5946[i] = 0xFAL;
                    (*l_3) ^= (((*l_5911) = (l_5838 == l_5910)) , l_5912);
                    l_5947[5] |= ((safe_mul_func_int16_t_s_s((((((((0xBCBB2E68L > (safe_rshift_func_int16_t_s_s(((&g_928 == (l_5917 = &g_1988)) >= (safe_unary_minus_func_uint64_t_u((l_5809[6] || ((safe_mul_func_int8_t_s_s(((safe_sub_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u(((safe_div_func_int64_t_s_s((((*l_5911) = (safe_lshift_func_int16_t_s_s(0xCBFEL, 1))) == (l_5910 <= ((((((safe_rshift_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_u(65535UL, 6)) == ((((l_5941 = (l_5838 , ((((*g_2189) ^= (l_5940 = (l_5939 = ((((*l_5938) ^= ((safe_mul_func_uint16_t_u_u(((l_5937 != (*g_945)) > 0xE9A47702890D142BLL), l_5883)) != l_5907[0])) && 0xA3L) <= (*l_8))))) ^ (-1L)) == 0xB752E5289D668B40LL))) > g_1981[5][0]) > l_5907[0]) <= 0x98BAEBEBL)), 1)) , (void*)0) != l_5942) , 1L) , 0UL) && l_5852))), l_5943)) && (**g_752)), l_5809[3])) , l_5907[0]), 0x1AFEL)) , l_5939), l_5945)) == 0x5EECL))))), 4))) <= 0xC0L) , l_5907[0]) & 0x15A6L) | l_5809[1]) >= l_5945) || l_5946[7]), 0x3A62L)) | 1UL);
                    return l_5852;
                }
                l_5883 ^= ((safe_mod_func_int32_t_s_s((((l_5838 = (l_5809[7] >= ((l_5950 == (void*)0) | l_5939))) <= ((l_5835[2] | 0xC6L) , ((((*l_9) != ((***g_3979) = ((*l_5616) != (void*)0))) ^ l_5835[1]) < (*l_3)))) ^ 0x23869639046E35FCLL), l_5945)) ^ (*g_1443));
            }
            (*g_69) ^= (safe_mul_func_int8_t_s_s(l_5809[8], ((((((((l_5883 | l_5945) & (safe_mod_func_uint32_t_u_u(((*g_3697) = (safe_rshift_func_uint16_t_u_s(((((l_5835[2] , &g_4536) == l_5958) <= (safe_mod_func_int8_t_s_s((safe_unary_minus_func_uint8_t_u((safe_rshift_func_int16_t_s_s((safe_mod_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_u(((*g_2189) |= 1UL), (safe_add_func_uint8_t_u_u(0x9CL, l_5945)))), 7)) > 0x345B181BL), 0x46L)), 13)))), 0x87L))) || l_5973), 6))), 0xAB409033L))) , 1UL) ^ l_5835[0]) || 0xD05904C9L) | 0x5657L) , &l_5602) != &l_5883)));
            return l_5809[1];
        }
    }
    for (g_5588 = 0; (g_5588 == 48); g_5588 = safe_add_func_uint64_t_u_u(g_5588, 2))
    { /* block id: 2608 */
        int32_t l_5976 = 0x5197445BL;
        return l_5976;
    }
    return (*l_6);
}


/* ------------------------------------------ */
/* 
 * reads : g_3750 g_3751 g_4 g_530 g_448 g_3752 g_3753 g_1222 g_2030 g_3977 g_3978 g_1443 g_72 g_2212 g_3697 g_2734 g_1329 g_160 g_371 g_372 g_373 g_897 g_1510 g_696 g_4717 g_753 g_754 g_2663 g_659 g_752 g_1664 g_1665 g_3852 g_159 g_533 g_2810 g_1663 g_3524 g_2189 g_101 g_1952 g_2139 g_2140 g_1327 g_1328 g_2142 g_2141 g_3806 g_1345 g_1403 g_1110 g_99 g_102 g_103 g_635 g_297 g_1980 g_609 g_4177 g_69 g_127 g_317 g_318 g_532 g_3979 g_1109 g_1111 g_1229 g_1230 g_1231 g_4071 g_1378 g_795 g_2432 g_1635 g_4535 g_4536 g_1987 g_1988 g_907 g_4521 g_5405 g_2603 g_1140 g_5419 g_79 g_2366 g_200 g_2602 g_3221 g_3219 g_3630 g_3631 g_208 g_4048 g_1539 g_3504 g_3378 g_2933
 * writes: g_897 g_1385 g_2030 g_72 g_609 g_1345 g_2663 g_659 g_4048 g_349 g_1539 g_208 g_533 g_2810 g_3504 g_634 g_101 g_2142 g_2141 g_1380 g_448 g_3806 g_4177 g_1403 g_1111 g_99 g_635 g_297 g_1980 g_1378 g_696 g_103 g_1663 g_766 g_1109 g_5314 g_907 g_3378 g_2933 g_5402 g_160 g_127 g_159 g_200 g_1988 g_4521
 */
static uint16_t  func_19(int32_t  p_20, int32_t * p_21, int16_t  p_22, int32_t * p_23)
{ /* block id: 1958 */
    uint64_t * const l_4642 = &g_2030;
    int32_t l_4643[4][4] = {{0L,0x46150AF1L,0x1834B85FL,0x1834B85FL},{0xF3DB7550L,0xF3DB7550L,0x4A1D2426L,0x46150AF1L},{0x46150AF1L,0L,0x4A1D2426L,0L},{0xF3DB7550L,(-1L),0x1834B85FL,0x4A1D2426L}};
    uint64_t ***l_4651[4][7];
    uint64_t ****l_4650[4][1] = {{&l_4651[2][0]},{&l_4651[3][5]},{&l_4651[2][0]},{&l_4651[3][5]}};
    uint64_t *****l_4649 = &l_4650[3][0];
    float *l_4657 = &g_4177;
    float **l_4658[4][1];
    float *l_4659 = &g_293[2][2];
    float *l_4660 = &g_2954;
    int8_t *l_4661 = (void*)0;
    int8_t *l_4662[1][8] = {{&g_1980[2],&g_1980[2],&g_297,&g_1980[2],&g_1980[2],&g_297,&g_1980[2],&g_1980[2]}};
    int32_t l_4663 = 0xFC913FEEL;
    int32_t l_4664 = 0xE85982D0L;
    const int32_t l_4677 = 8L;
    int8_t l_4690 = (-9L);
    const uint32_t *l_4694 = &g_493;
    const uint32_t **l_4693 = &l_4694;
    const uint32_t *** const l_4692 = &l_4693;
    const int32_t *l_4698 = &g_696;
    uint32_t * volatile ***l_4701 = (void*)0;
    float ****l_4729 = &g_4071;
    float ** const *l_4731 = &l_4658[3][0];
    float ** const **l_4730 = &l_4731;
    uint32_t l_4770 = 0xD059880AL;
    uint32_t l_4909 = 0x381983C1L;
    int32_t *l_4943[2];
    uint64_t *l_5000 = &g_448;
    const uint8_t *l_5035 = &g_127;
    const uint8_t **l_5034[8][6] = {{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035},{&l_5035,&l_5035,&l_5035,&l_5035,&l_5035,&l_5035}};
    const uint8_t ***l_5033[3][5][5] = {{{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]},{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]},{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]},{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]},{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]}},{{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]},{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]},{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]},{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]},{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]}},{{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]},{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]},{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]},{&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1],&l_5034[1][1]},{&l_5034[5][3],&l_5034[1][1],&l_5034[5][3],&l_5034[1][1],&l_5034[5][3]}}};
    const uint8_t ****l_5032 = &l_5033[1][1][3];
    uint32_t l_5049 = 0UL;
    float *****l_5096 = &l_4729;
    uint16_t l_5118 = 5UL;
    uint32_t ***l_5203 = (void*)0;
    uint64_t l_5290 = 18446744073709551615UL;
    uint32_t l_5315 = 0x5407A355L;
    float l_5349 = (-0x10.6p+1);
    int32_t l_5352 = (-9L);
    uint32_t l_5397 = 0x75886E1FL;
    int64_t l_5422 = 0xDE179D24514BCB95LL;
    int8_t **l_5425 = &l_4662[0][5];
    const int16_t *****l_5430 = (void*)0;
    int16_t *l_5459 = &g_2141;
    uint32_t l_5461 = 0UL;
    int32_t l_5497 = 4L;
    uint32_t l_5565 = 18446744073709551615UL;
    int64_t **l_5584 = &g_5404[5];
    int64_t l_5585[3][5][4] = {{{0xFFD236463229D50DLL,0xDD647C033B429DB9LL,0xA9530A03E56ED42CLL,0xA9530A03E56ED42CLL},{(-1L),(-1L),0xFFD236463229D50DLL,0xA9530A03E56ED42CLL},{1L,0xDD647C033B429DB9LL,1L,0xFFD236463229D50DLL},{1L,0xFFD236463229D50DLL,0xFFD236463229D50DLL,1L},{(-1L),0xFFD236463229D50DLL,0xA9530A03E56ED42CLL,0xFFD236463229D50DLL}},{{0xFFD236463229D50DLL,0xDD647C033B429DB9LL,0xA9530A03E56ED42CLL,0xA9530A03E56ED42CLL},{(-1L),(-1L),0xFFD236463229D50DLL,0xA9530A03E56ED42CLL},{1L,0xDD647C033B429DB9LL,1L,0xFFD236463229D50DLL},{1L,0xFFD236463229D50DLL,0xFFD236463229D50DLL,1L},{(-1L),0xFFD236463229D50DLL,0xA9530A03E56ED42CLL,0xFFD236463229D50DLL}},{{0xFFD236463229D50DLL,0xDD647C033B429DB9LL,0xA9530A03E56ED42CLL,0xA9530A03E56ED42CLL},{(-1L),(-1L),0xFFD236463229D50DLL,0xA9530A03E56ED42CLL},{1L,0xDD647C033B429DB9LL,1L,0xFFD236463229D50DLL},{1L,0xFFD236463229D50DLL,0xFFD236463229D50DLL,1L},{(-1L),0xFFD236463229D50DLL,0xA9530A03E56ED42CLL,0xFFD236463229D50DLL}}};
    int8_t l_5586 = 0x30L;
    int64_t l_5587 = 0x412EF4CCFA4EB23ALL;
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
            l_4651[i][j] = &g_1232[0][1][2];
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
            l_4658[i][j] = &l_4657;
    }
    for (i = 0; i < 2; i++)
        l_4943[i] = &l_4643[1][3];
lbl_4991:
    for (g_897 = 0; (g_897 <= 3); g_897 += 1)
    { /* block id: 1961 */
        uint32_t l_4635[8] = {0x176D6983L,0UL,0x176D6983L,0x176D6983L,0UL,0x176D6983L,0x176D6983L,0UL};
        float *l_4644 = &g_1385;
        int i;
        (*p_21) ^= (((p_22 , (*g_3750)) != (void*)0) | ((l_4635[1] < (safe_add_func_int64_t_s_s((((safe_add_func_int64_t_s_s(((safe_mul_func_float_f_f(l_4635[6], (((void*)0 != l_4642) , ((*l_4644) = ((0x5.Dp-1 != ((l_4643[1][3] > (*p_23)) , p_22)) >= l_4643[1][2]))))) , l_4643[2][2]), (*g_530))) , p_22) > (-8L)), (***g_3750)))) ^ p_22));
        return p_22;
    }
    if ((safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(((l_4649 = g_1222) != (void*)0), p_20)), ((safe_mul_func_int8_t_s_s(l_4643[1][3], (((*l_4642) |= (0L != (((~(l_4664 &= ((((l_4643[3][3] < (l_4663 = (safe_sub_func_int16_t_s_s(0x26B7L, ((l_4660 = (l_4659 = l_4657)) == p_21))))) , 0xFA897F83074D5D4ALL) & p_22) ^ 0x37777A1AL))) | 7UL) ^ (*p_21)))) <= (**g_3751)))) , p_22))))
    { /* block id: 1972 */
lbl_4932:
        (*p_21) &= ((void*)0 != (*g_3977));
        (*g_1443) |= (safe_add_func_uint64_t_u_u(l_4643[3][3], (l_4664 , p_20)));
    }
    else
    { /* block id: 1975 */
        float **l_4678 = (void*)0;
        float **l_4679 = &l_4660;
        int32_t l_4689 = 0x9A2C8252L;
        int32_t l_4691[5][5] = {{1L,(-4L),1L,(-9L),(-9L)},{0x07530750L,0L,0x07530750L,0xCB3FF9E1L,0xCB3FF9E1L},{1L,(-4L),1L,(-9L),(-9L)},{0x07530750L,0L,0x07530750L,0xCB3FF9E1L,0xCB3FF9E1L},{1L,(-4L),1L,(-9L),(-9L)}};
        int32_t l_4695 = 0x3CB8AF84L;
        int16_t l_4747 = 0x448AL;
        int32_t l_4748 = 0xF33FBCD1L;
        uint32_t l_4876[3][3][9] = {{{4294967294UL,0xAC53225AL,0xE01E515AL,0x3538EB57L,4294967295UL,4294967291UL,1UL,4294967295UL,0x53ACD204L},{0xE01E515AL,0xAC53225AL,4294967294UL,0xE01E515AL,4294967295UL,0x53ACD204L,4294967291UL,0x088937FFL,4294967291UL},{1UL,5UL,0xE01E515AL,0xE01E515AL,5UL,1UL,4294967295UL,0xAC53225AL,8UL}},{{4294967287UL,4294967295UL,1UL,0x3538EB57L,4294967295UL,1UL,0x53ACD204L,0x39B930BEL,1UL},{0xAF4C86CAL,4294967294UL,0UL,0xDD40BA7CL,8UL,0UL,0xDD40BA7CL,0x39B930BEL,3UL},{4294967288UL,0x39B930BEL,0xAF4C86CAL,1UL,4294967291UL,0x9A9E56A8L,0x9A9E56A8L,4294967291UL,1UL}},{{4294967288UL,4294967295UL,4294967288UL,0x9A9E56A8L,0x53ACD204L,0xAF4C86CAL,4UL,0x3538EB57L,0xAF4C86CAL},{0xAF4C86CAL,0x39B930BEL,4294967288UL,0UL,1UL,1UL,4294967295UL,4294967287UL,0x9A9E56A8L},{0UL,4294967294UL,0xAF4C86CAL,0x9A9E56A8L,1UL,3UL,0UL,0x53ACD204L,0UL}}};
        uint8_t l_4882[1][3][2];
        uint16_t **l_4941[10][7][3] = {{{&g_3267[5][4],&g_3267[5][4],&g_3267[5][4]},{(void*)0,(void*)0,&g_3267[1][2]},{(void*)0,&g_3267[0][1],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][2],&g_3267[5][4]},{&g_3267[0][3],&g_3267[1][2],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][4],&g_3267[5][4]},{(void*)0,&g_3267[5][4],&g_3267[5][4]}},{{(void*)0,&g_3267[5][4],(void*)0},{&g_3267[5][4],&g_3267[1][2],&g_3267[3][4]},{&g_3267[0][4],&g_3267[5][2],(void*)0},{&g_3267[3][4],&g_3267[0][1],&g_3267[5][4]},{&g_3267[5][4],(void*)0,&g_3267[5][4]},{&g_3267[3][4],&g_3267[5][4],&g_3267[5][4]},{&g_3267[0][4],&g_3267[2][4],&g_3267[5][4]}},{{&g_3267[5][4],&g_3267[5][4],&g_3267[5][4]},{(void*)0,(void*)0,&g_3267[1][2]},{(void*)0,&g_3267[0][1],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][2],&g_3267[5][4]},{&g_3267[0][3],&g_3267[1][2],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][4],&g_3267[5][4]},{(void*)0,&g_3267[5][4],&g_3267[5][4]}},{{(void*)0,&g_3267[5][4],(void*)0},{&g_3267[5][4],&g_3267[1][2],&g_3267[3][4]},{&g_3267[0][4],&g_3267[5][2],(void*)0},{&g_3267[3][4],&g_3267[0][1],&g_3267[5][4]},{&g_3267[5][4],(void*)0,&g_3267[5][4]},{&g_3267[3][4],&g_3267[5][4],&g_3267[5][4]},{&g_3267[0][4],&g_3267[2][4],&g_3267[5][4]}},{{&g_3267[5][4],&g_3267[5][4],&g_3267[5][4]},{(void*)0,(void*)0,&g_3267[1][2]},{(void*)0,&g_3267[0][1],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][2],&g_3267[5][4]},{&g_3267[0][3],&g_3267[1][2],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][4],&g_3267[5][4]},{(void*)0,&g_3267[5][4],&g_3267[5][4]}},{{(void*)0,&g_3267[5][4],(void*)0},{&g_3267[5][4],&g_3267[1][2],&g_3267[3][4]},{&g_3267[0][4],&g_3267[5][2],(void*)0},{&g_3267[3][4],&g_3267[0][1],&g_3267[5][4]},{&g_3267[5][4],(void*)0,&g_3267[5][4]},{&g_3267[3][4],&g_3267[5][4],&g_3267[5][4]},{&g_3267[0][4],&g_3267[2][4],&g_3267[5][4]}},{{&g_3267[5][4],&g_3267[5][4],&g_3267[5][4]},{(void*)0,(void*)0,&g_3267[1][2]},{(void*)0,&g_3267[0][1],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][2],&g_3267[5][4]},{&g_3267[0][3],&g_3267[1][2],&g_3267[5][4]},{&g_3267[1][2],&g_3267[5][4],&g_3267[5][4]},{&g_3267[6][4],&g_3267[5][4],&g_3267[6][4]}},{{(void*)0,(void*)0,(void*)0},{(void*)0,&g_3267[5][4],(void*)0},{&g_3267[6][3],&g_3267[5][4],(void*)0},{(void*)0,&g_3267[0][3],&g_3267[6][4]},{&g_3267[4][3],(void*)0,&g_3267[3][1]},{(void*)0,&g_3267[5][4],&g_3267[5][4]},{&g_3267[6][3],&g_3267[5][4],&g_3267[5][4]}},{{(void*)0,&g_3267[5][4],(void*)0},{(void*)0,(void*)0,&g_3267[5][4]},{&g_3267[6][4],&g_3267[0][3],(void*)0},{&g_3267[5][4],&g_3267[5][4],&g_3267[5][4]},{&g_3267[6][4],&g_3267[5][4],&g_3267[5][4]},{&g_3267[5][4],(void*)0,&g_3267[3][1]},{&g_3267[6][4],&g_3267[5][4],&g_3267[6][4]}},{{(void*)0,(void*)0,(void*)0},{(void*)0,&g_3267[5][4],(void*)0},{&g_3267[6][3],&g_3267[5][4],(void*)0},{(void*)0,&g_3267[0][3],&g_3267[6][4]},{&g_3267[4][3],(void*)0,&g_3267[3][1]},{(void*)0,&g_3267[5][4],&g_3267[5][4]},{&g_3267[6][3],&g_3267[5][4],&g_3267[5][4]}}};
        uint32_t l_4980 = 0xE756DCBFL;
        uint64_t *l_4999[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint32_t l_5007 = 0x287A80A2L;
        const uint32_t *l_5015[5];
        const uint32_t **l_5014 = &l_5015[1];
        int16_t **l_5127 = &g_252[3][5][3];
        int16_t l_5140 = (-1L);
        uint64_t ***l_5173 = &g_529[4][0][7];
        int32_t *l_5197 = &g_2686;
        uint32_t ***l_5205 = &g_2793;
        const int16_t ***l_5227 = &g_1328;
        const int16_t ****l_5226 = &l_5227;
        uint64_t l_5230 = 0x96203ADBBF2409E5LL;
        uint8_t ****l_5309 = &g_1270[0];
        float l_5310 = (-0x1.Fp+1);
        const float ***l_5311 = (void*)0;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 3; j++)
            {
                for (k = 0; k < 2; k++)
                    l_4882[i][j][k] = 248UL;
            }
        }
        for (i = 0; i < 5; i++)
            l_5015[i] = &g_4521;
lbl_4696:
        l_4695 &= (((((safe_add_func_int64_t_s_s(((((((safe_mul_func_int8_t_s_s((l_4689 = (safe_lshift_func_uint8_t_u_u((safe_div_func_int64_t_s_s((((((((safe_add_func_int32_t_s_s((l_4643[3][1] != (((*g_2212) = &l_4659) == (l_4679 = (l_4677 , (l_4678 = l_4678))))), ((*g_3697) = p_22))) , (((safe_lshift_func_int16_t_s_s(((safe_mod_func_int8_t_s_s((+((((*g_2734) >= p_20) ^ ((safe_add_func_uint16_t_u_u((0UL > (((p_20 < 0x0A15EC67L) >= 0x17136ABFL) != (*g_1329))), p_22)) <= l_4663)) <= p_20)), 8L)) ^ 65526UL), 11)) >= (-1L)) || l_4689)) >= l_4689) , (-5L)) || l_4689) <= p_22) <= l_4689), p_22)), l_4690))), l_4691[1][2])) <= 8UL) != p_20) , (void*)0) == l_4692) , (*g_3752)), l_4690)) | (*p_23)) || 9L) && (**g_371)) != p_20);
        for (g_2663 = 3; (g_2663 >= 0); g_2663 -= 1)
        { /* block id: 1984 */
            int32_t l_4723 = 1L;
            int32_t l_4732 = 0xB419E0E5L;
            uint8_t l_4756 = 1UL;
            int32_t l_4875 = (-8L);
            int8_t **l_4926 = &l_4662[0][5];
            const uint8_t *l_4928 = &g_4060[2][0];
            const uint8_t ** const l_4927 = &l_4928;
            uint32_t **l_4936[9] = {&g_2366,&g_2366,&g_2366,&g_2366,&g_2366,&g_2366,&g_2366,&g_2366,&g_2366};
            uint32_t l_4981 = 1UL;
            int32_t l_4985 = 3L;
            uint64_t *l_4998 = (void*)0;
            uint64_t *l_5001 = (void*)0;
            int32_t *l_5037 = &l_4695;
            int32_t l_5050 = 0xF3AFF8EDL;
            int32_t l_5051[10][7][3] = {{{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x93F91A82L,1L},{0x6037E6D5L,0x93F91A82L,0x6037E6D5L},{0x9FA579AEL,0x0AC5349FL,1L},{0x9FA579AEL,0x1CD77260L,(-1L)},{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x93F91A82L,1L}},{{0x6037E6D5L,0x93F91A82L,0x6037E6D5L},{0x9FA579AEL,0x0AC5349FL,1L},{0x9FA579AEL,0x1CD77260L,(-1L)},{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x93F91A82L,1L},{0x6037E6D5L,0x93F91A82L,0x6037E6D5L},{0x9FA579AEL,0x0AC5349FL,1L}},{{0x9FA579AEL,0x1CD77260L,(-1L)},{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x93F91A82L,1L},{0x6037E6D5L,0x93F91A82L,0x6037E6D5L},{0x9FA579AEL,0x0AC5349FL,1L},{0x9FA579AEL,0x1CD77260L,(-1L)},{0x6037E6D5L,0x0AC5349FL,(-1L)}},{{(-1L),0x93F91A82L,1L},{0x6037E6D5L,0x93F91A82L,0x6037E6D5L},{0x9FA579AEL,0x0AC5349FL,1L},{0x9FA579AEL,0x1CD77260L,(-1L)},{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x93F91A82L,1L},{0x6037E6D5L,0x93F91A82L,0x6037E6D5L}},{{0x9FA579AEL,0x0AC5349FL,1L},{0x9FA579AEL,0x1CD77260L,(-1L)},{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x93F91A82L,1L},{0x6037E6D5L,0x93F91A82L,0x6037E6D5L},{0x9FA579AEL,0x0AC5349FL,1L},{0x9FA579AEL,0x1CD77260L,(-1L)}},{{0x6037E6D5L,0x0AC5349FL,(-1L)},{(-1L),0x9FA579AEL,0x4685B934L},{0x1FF71057L,0x9FA579AEL,0x1FF71057L},{(-8L),0x6037E6D5L,0x4685B934L},{(-8L),(-1L),(-1L)},{0x1FF71057L,0x6037E6D5L,(-1L)},{(-1L),0x9FA579AEL,0x4685B934L}},{{0x1FF71057L,0x9FA579AEL,0x1FF71057L},{(-8L),0x6037E6D5L,0x4685B934L},{(-8L),(-1L),(-1L)},{0x1FF71057L,0x6037E6D5L,(-1L)},{(-1L),0x9FA579AEL,0x4685B934L},{0x1FF71057L,0x9FA579AEL,0x1FF71057L},{(-8L),0x6037E6D5L,0x4685B934L}},{{(-8L),(-1L),(-1L)},{0x1FF71057L,0x6037E6D5L,(-1L)},{(-1L),0x9FA579AEL,0x4685B934L},{0x1FF71057L,0x9FA579AEL,0x1FF71057L},{(-8L),0x6037E6D5L,0x4685B934L},{(-8L),(-1L),(-1L)},{0x1FF71057L,0x6037E6D5L,(-1L)}},{{(-1L),0x9FA579AEL,0x4685B934L},{0x1FF71057L,0x9FA579AEL,0x1FF71057L},{(-8L),0x6037E6D5L,0x4685B934L},{(-8L),(-1L),(-1L)},{0x1FF71057L,0x6037E6D5L,(-1L)},{(-1L),0x9FA579AEL,0x4685B934L},{0x1FF71057L,0x9FA579AEL,0x1FF71057L}},{{(-8L),0x6037E6D5L,0x4685B934L},{(-8L),(-1L),(-1L)},{0x1FF71057L,0x6037E6D5L,(-1L)},{(-1L),0x9FA579AEL,0x4685B934L},{0x1FF71057L,0x9FA579AEL,0x1FF71057L},{(-8L),0x6037E6D5L,0x4685B934L},{(-8L),(-1L),(-1L)}}};
            uint64_t l_5056 = 18446744073709551615UL;
            int i, j, k;
            for (g_659 = 3; (g_659 >= 0); g_659 -= 1)
            { /* block id: 1987 */
                int32_t l_4716 = 0x6AE41583L;
                if (g_72)
                    goto lbl_4696;
                for (g_4048 = 3; (g_4048 >= 0); g_4048 -= 1)
                { /* block id: 1991 */
                    for (g_897 = 0; (g_897 <= 3); g_897 += 1)
                    { /* block id: 1994 */
                        const int32_t **l_4697 = &g_349[3][7];
                        uint32_t * volatile ***l_4700 = &g_943[0][3][4];
                        uint32_t * volatile ****l_4699[10][5][5] = {{{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,(void*)0,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,(void*)0,&l_4700,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,(void*)0,&l_4700}},{{&l_4700,&l_4700,&l_4700,(void*)0,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{(void*)0,(void*)0,&l_4700,&l_4700,&l_4700},{(void*)0,&l_4700,&l_4700,&l_4700,&l_4700}},{{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,(void*)0,&l_4700},{(void*)0,&l_4700,&l_4700,(void*)0,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,(void*)0,&l_4700,(void*)0,&l_4700}},{{(void*)0,&l_4700,&l_4700,&l_4700,&l_4700},{(void*)0,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{(void*)0,(void*)0,&l_4700,&l_4700,&l_4700},{(void*)0,&l_4700,&l_4700,(void*)0,&l_4700}},{{&l_4700,(void*)0,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,(void*)0,&l_4700},{&l_4700,(void*)0,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,(void*)0,&l_4700},{(void*)0,(void*)0,&l_4700,&l_4700,&l_4700}},{{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,(void*)0,(void*)0,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700}},{{(void*)0,&l_4700,&l_4700,(void*)0,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,(void*)0,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700}},{{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,(void*)0,&l_4700,(void*)0,(void*)0},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,(void*)0,&l_4700,(void*)0,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0}},{{&l_4700,(void*)0,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,(void*)0,(void*)0,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0},{&l_4700,(void*)0,&l_4700,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,(void*)0}},{{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700},{&l_4700,(void*)0,&l_4700,&l_4700,(void*)0},{(void*)0,&l_4700,(void*)0,(void*)0,(void*)0},{(void*)0,&l_4700,(void*)0,&l_4700,&l_4700},{&l_4700,&l_4700,&l_4700,&l_4700,&l_4700}}};
                        int i, j, k;
                        l_4698 = ((*l_4697) = p_21);
                        if (g_1510[(g_897 + 1)][g_897])
                            break;
                        l_4701 = &g_943[0][3][4];
                    }
                    return p_22;
                }
                for (g_1539 = 0; (g_1539 <= 3); g_1539 += 1)
                { /* block id: 2004 */
                    int32_t l_4710 = 1L;
                    int32_t ***l_4713 = &g_208;
                    int i, j;
                    l_4643[g_2663][g_659] ^= ((safe_sub_func_uint64_t_u_u(((((l_4695 ^ (*l_4698)) != (((safe_div_func_uint16_t_u_u((safe_add_func_int16_t_s_s((safe_mod_func_int32_t_s_s(((l_4710 <= p_22) , (((safe_mod_func_uint32_t_u_u((p_22 <= (((*l_4713) = &g_533) != &p_23)), ((p_22 >= (safe_sub_func_float_f_f(l_4691[1][2], l_4716))) , 0x2034E083L))) >= 0x91816080BCB88AB5LL) , (*p_21))), g_4717)), (*g_753))), p_22)) , l_4716) | (*l_4698))) & p_22) >= p_20), (-1L))) == p_20);
                }
                return (**g_752);
            }
            if ((safe_lshift_func_uint16_t_u_s((safe_unary_minus_func_uint32_t_u((safe_add_func_uint64_t_u_u(((p_22 != ((p_20 == (l_4723 ^= 0x46L)) >= (-3L))) || (*l_4698)), (safe_div_func_int64_t_s_s((4294967295UL != (+(safe_mul_func_uint16_t_u_u((((*l_4698) , l_4729) != (l_4730 = l_4730)), l_4691[0][4])))), (**g_3751))))))), l_4732)))
            { /* block id: 2012 */
                uint32_t l_4749 = 0UL;
                (**g_1664) = &l_4732;
                l_4748 = (safe_add_func_float_f_f(((safe_sub_func_float_f_f(0xA.C1F29Dp+84, (safe_mul_func_float_f_f((safe_div_func_float_f_f(((l_4691[1][4] <= (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((safe_add_func_float_f_f((*l_4698), 0x0.52512Dp+85)) <= p_20), ((void*)0 != &g_3250))), (*g_3852)))) <= (-0x1.7p+1)), l_4747)), (*l_4698))))) <= l_4691[2][1]), p_20));
                --l_4749;
                (*g_533) &= (safe_lshift_func_int8_t_s_u(p_22, (safe_lshift_func_int16_t_s_s(p_22, 12))));
            }
            else
            { /* block id: 2017 */
                float l_4769 = (-0x10.5p-1);
                for (g_2810 = 0; (g_2810 <= 3); g_2810 += 1)
                { /* block id: 2020 */
                    for (g_897 = 0; (g_897 <= 2); g_897 += 1)
                    { /* block id: 2023 */
                        int i, j;
                        l_4643[g_2810][g_897] = 7L;
                    }
                    if (g_897)
                        goto lbl_4696;
                }
                for (l_4695 = 3; (l_4695 >= 0); l_4695 -= 1)
                { /* block id: 2030 */
                    (***g_1663) = p_23;
                    (***g_1663) = ((l_4756 = p_22) , p_23);
                }
                (**g_3524) &= ((((safe_div_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((((0x33L >= ((((((safe_mod_func_int16_t_s_s(((safe_div_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(0x8126L, p_22)), p_22)) || p_22), (((l_4643[3][0] = 0x62L) < ((l_4756 > p_20) < ((((((*g_3752) ^ l_4695) , (**g_371)) ^ l_4723) , (*l_4698)) >= (*p_21)))) & (*p_23)))) , l_4695) <= p_20) , 0x074F5562L) , l_4756) == (*l_4698))) <= p_20) <= l_4770), l_4756)) , (**g_752)), p_20)) >= p_22) > (*l_4698)) && p_22);
            }
            (*g_1665) = &l_4664;
            for (g_3504 = 0; (g_3504 <= 3); g_3504 += 1)
            { /* block id: 2041 */
                int32_t ***l_4774 = (void*)0;
                int32_t ****l_4773 = &l_4774;
                int16_t ** const l_4880 = &g_252[6][2][3];
                uint32_t l_4892[2][10][7] = {{{18446744073709551612UL,0xBE0616CCL,18446744073709551612UL,1UL,2UL,1UL,18446744073709551615UL},{0UL,0xA1DABBB7L,0x6F9D9724L,0xAC059AFBL,4UL,0xD885821CL,18446744073709551611UL},{1UL,0xBE0616CCL,18446744073709551615UL,0UL,0UL,18446744073709551615UL,0xBE0616CCL},{18446744073709551615UL,0UL,3UL,0xD885821CL,0x45E67B40L,0xA578909DL,0x32F7B4C5L},{2UL,1UL,0x4AFAD168L,18446744073709551612UL,18446744073709551615UL,1UL,0x50F5207DL},{0xAC059AFBL,0x32F7B4C5L,0x6F9D9724L,18446744073709551615UL,0UL,0UL,18446744073709551615UL},{0UL,0x50F5207DL,0UL,1UL,0x4AFAD168L,0xBE0616CCL,1UL},{4UL,0xEC9CB016L,0UL,0UL,3UL,0xA1DABBB7L,3UL},{0x50F5207DL,18446744073709551612UL,1UL,1UL,18446744073709551615UL,0xBE0616CCL,0xBE0616CCL},{0xB01FA435L,0x45E67B40L,0UL,0x45E67B40L,0xB01FA435L,0UL,0UL}},{{0xAEB05AA3L,18446744073709551615UL,18446744073709551612UL,0xD1034CC2L,0xBE0616CCL,1UL,6UL},{0x32F7B4C5L,0UL,4UL,18446744073709551611UL,18446744073709551615UL,0xB01FA435L,0UL},{0xAEB05AA3L,0xD1034CC2L,0x67E5B233L,1UL,0x67E5B233L,0xD1034CC2L,0xAEB05AA3L},{0xB01FA435L,0xAC059AFBL,0xA578909DL,0UL,0x45E67B40L,18446744073709551615UL,0UL},{0x50F5207DL,0x4AFAD168L,0x9A794D9DL,0UL,1UL,6UL,18446744073709551615UL},{4UL,0x6F9D9724L,0xA578909DL,18446744073709551607UL,0xAC059AFBL,18446744073709551607UL,0xA578909DL},{0UL,0UL,0x67E5B233L,18446744073709551611UL,18446744073709551612UL,18446744073709551612UL,1UL},{0xAC059AFBL,0UL,4UL,0xA578909DL,0UL,0x6F9D9724L,0UL},{2UL,1UL,18446744073709551612UL,0xBE0616CCL,18446744073709551612UL,18446744073709551615UL,18446744073709551612UL},{0UL,0UL,0UL,0UL,0xAC059AFBL,3UL,0xE4B8A98DL}}};
                const int32_t **l_4924 = &g_3631;
                const int32_t ***l_4923[5];
                uint16_t **l_4939[10] = {&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3],&g_3267[6][3]};
                uint32_t *** const *l_4983 = &g_1110;
                uint32_t *** const **l_4982[7][6] = {{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983},{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983},{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983},{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983},{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983},{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983},{&l_4983,&l_4983,&l_4983,&l_4983,&l_4983,&l_4983}};
                int32_t l_4984 = 0L;
                uint32_t l_5036 = 4UL;
                int32_t l_5054 = 0xBEE60056L;
                int32_t l_5055 = 1L;
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_4923[i] = &l_4924;
                if ((p_20 > (p_20 || (((safe_add_func_uint8_t_u_u(((((((*g_1663) != ((*l_4773) = &g_208)) , p_20) >= (safe_mod_func_uint16_t_u_u(p_22, l_4723))) != p_22) < ((safe_lshift_func_uint16_t_u_u(0xB8F3L, 6)) , 4L)), 0L)) == (*l_4698)) & l_4747))))
                { /* block id: 2043 */
                    int8_t l_4781 = (-4L);
                    int64_t l_4782 = 2L;
                    int64_t *l_4810 = &g_634;
                    uint16_t l_4813 = 0x202AL;
                    int i, j;
                    if (((((safe_add_func_uint64_t_u_u(l_4781, l_4756)) , l_4782) , ((safe_mul_func_int8_t_s_s(((p_22 || (p_22 , (safe_add_func_uint16_t_u_u(((*l_4698) | ((safe_div_func_int8_t_s_s((safe_div_func_int8_t_s_s(((*g_753) ^ p_20), (p_20 && p_20))), p_20)) > (*g_2189))), p_22)))) ^ 0xCAL), 1UL)) < l_4756)) <= p_20))
                    { /* block id: 2044 */
                        p_21 = p_21;
                        (*g_1952) = (void*)0;
                    }
                    else
                    { /* block id: 2047 */
                        if (l_4695)
                            goto lbl_4696;
                    }
                    l_4643[g_2663][g_2663] = ((((((safe_add_func_uint8_t_u_u(0xA5L, (!(safe_lshift_func_uint8_t_u_u(((safe_sub_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((safe_sub_func_int16_t_s_s((*l_4698), 0xD365L)), (safe_mul_func_int8_t_s_s(l_4782, (safe_lshift_func_uint8_t_u_u((*g_2189), l_4756)))))), ((safe_mul_func_int16_t_s_s(((((*l_4810) = 5L) || ((((1L != p_20) < l_4782) , 0xB70E0B90L) >= (*l_4698))) != (*p_21)), (*g_753))) > l_4782))) , (*l_4698)), 5))))) < (*l_4698)) , p_20) , p_22) >= 0x11DFL) & (-6L));
                    for (g_2810 = 5; (g_2810 <= 5); g_2810++)
                    { /* block id: 2054 */
                        uint16_t l_4814 = 0xA30BL;
                        int8_t l_4836 = 2L;
                        uint16_t *l_4837 = &g_1380;
                        uint16_t *l_4838[3][10] = {{&g_907,&g_907,&g_907,&g_907,&g_907,&g_907,&g_907,&g_907,&g_907,&g_907},{&g_907,&l_4813,&l_4813,&g_907,&l_4813,&l_4813,&g_907,&l_4813,&l_4813,&g_907},{&l_4813,&g_907,&l_4813,&l_4813,&g_907,&l_4813,&l_4813,&l_4813,&g_907,&g_907}};
                        int32_t l_4839 = (-6L);
                        int i, j;
                        (**g_3524) = l_4756;
                        if ((*p_23))
                            break;
                    }
                }
                else
                { /* block id: 2064 */
                    (***g_1663) = (void*)0;
                }
                if ((*p_23))
                { /* block id: 2067 */
                    uint32_t l_4872 = 0x55BAED62L;
                    int16_t **l_4878 = &g_252[2][5][2];
                    int32_t l_4881[9][8] = {{(-7L),(-7L),8L,9L,8L,(-7L),(-7L),8L},{(-4L),8L,8L,(-4L),3L,(-4L),8L,8L},{8L,3L,9L,9L,3L,8L,3L,9L},{(-4L),3L,(-4L),8L,8L,(-4L),3L,(-4L)},{(-7L),8L,9L,8L,(-7L),(-7L),8L,9L},{(-7L),(-7L),8L,9L,8L,(-7L),(-7L),8L},{(-4L),8L,8L,(-4L),3L,(-4L),8L,8L},{8L,3L,9L,9L,3L,8L,3L,9L},{(-4L),3L,(-4L),8L,8L,(-4L),3L,(-4L)}};
                    uint32_t l_4897 = 0x90A22DCFL;
                    int i, j;
                    if ((safe_rshift_func_uint8_t_u_s(((*g_753) && 0UL), 0)))
                    { /* block id: 2068 */
                        uint16_t *l_4873 = (void*)0;
                        uint16_t *l_4874 = &g_1380;
                        const int32_t l_4877 = 0L;
                        int16_t ***l_4879 = &l_4878;
                        (*g_2734) = ((safe_lshift_func_int8_t_s_u((((*g_2189)++) , ((safe_mul_func_uint16_t_u_u((((*l_4879) = ((safe_div_func_uint16_t_u_u(((*p_23) & ((l_4732 = ((safe_sub_func_uint16_t_u_u(0x8E1DL, (safe_lshift_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(((((safe_sub_func_uint16_t_u_u((((safe_add_func_uint16_t_u_u(l_4756, (((*g_530) = (~(!(l_4875 ^= (((*l_4698) && (l_4691[3][0] , (((safe_mul_func_int8_t_s_s(l_4732, (3UL >= ((*l_4874) = ((safe_rshift_func_int16_t_s_s((l_4691[3][3] | (safe_add_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u((((**g_2139) = (((safe_add_func_uint64_t_u_u(p_20, 0x2A54AF4E59F3B85ALL)) , p_20) || (*l_4698))) && l_4732), p_22)), p_22))), 5)) < l_4872))))) <= (-7L)) && 3UL))) > 0x9C69B0D1D5089273LL))))) , l_4875))) < (-3L)) , l_4876[0][0][1]), l_4877)) && p_20) & 0xA4L) >= p_22), p_22)), 0)))) <= (***g_1327))) , 0xCC82C4E1L)), 65535UL)) , l_4878)) != l_4880), l_4872)) | 0x50L)), l_4877)) == 1UL);
                    }
                    else
                    { /* block id: 2077 */
                        int64_t l_4885 = (-10L);
                        float l_4886 = 0x1.Ep-1;
                        int32_t *l_4887 = &g_72;
                        int32_t *l_4888 = &l_4695;
                        int32_t *l_4889 = &l_4689;
                        int32_t *l_4890 = &l_4643[1][3];
                        int32_t *l_4891[9][9] = {{&l_4732,&g_2933,&l_4643[1][3],&g_3188,&g_3188,&l_4643[1][3],&g_2933,&l_4732,&g_2933},{(void*)0,&l_4691[1][2],&l_4643[1][3],&l_4643[1][3],&l_4691[1][2],(void*)0,&g_4[0][0],(void*)0,&l_4691[1][2]},{&l_4881[5][0],&g_2933,&g_2933,&l_4881[5][0],&l_4732,&g_4[0][0],&l_4732,&l_4881[5][0],&g_2933},{&l_4732,&l_4732,&g_4[0][0],&l_4691[1][2],&g_2933,&l_4691[1][2],&g_4[0][0],&l_4732,&l_4732},{&g_2933,&l_4881[5][0],&l_4732,&g_4[0][0],&l_4732,&l_4881[5][0],&g_2933,&g_2933,&l_4881[5][0]},{&l_4691[1][2],(void*)0,&g_4[0][0],(void*)0,&l_4691[1][2],&l_4643[1][3],&l_4643[1][3],&l_4691[1][2],(void*)0},{&g_2933,&l_4732,&g_2933,&l_4643[1][3],&g_3188,&g_3188,&l_4643[1][3],&g_2933,&l_4732},{&l_4732,&l_4881[5][0],&l_4643[1][3],&g_4[0][0],&g_4[0][0],&l_4643[1][3],&l_4881[5][0],&l_4732,&l_4881[5][0]},{&l_4881[5][0],&g_4[0][0],&l_4643[1][3],&l_4643[1][3],&g_4[0][0],&l_4881[5][0],&g_3188,&l_4881[5][0],&g_4[0][0]}};
                        int i, j;
                        if ((*p_23))
                            break;
                        if ((*p_23))
                            break;
                        ++l_4882[0][0][0];
                        l_4892[1][9][4]++;
                    }
                    for (g_3806 = 13; (g_3806 < 27); g_3806 = safe_add_func_int16_t_s_s(g_3806, 1))
                    { /* block id: 2085 */
                        int32_t l_4908[8] = {0x25E55A83L,0x25E55A83L,0x5DCA8E5BL,0x25E55A83L,0x25E55A83L,0x5DCA8E5BL,0x25E55A83L,0x25E55A83L};
                        int i;
                        --l_4897;
                        (*p_21) ^= (((*g_2734) |= (safe_unary_minus_func_uint32_t_u(((!(((((((*l_4698) != p_20) , (((safe_add_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(l_4723, l_4695)), ((safe_lshift_func_uint16_t_u_s(l_4908[5], (l_4909 == (((safe_lshift_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u(((safe_sub_func_int64_t_s_s((-1L), 0xDF852EAD88A929EBLL)) , 0xD2F4L), 1)) && p_22), p_20)) , l_4748) || (**g_2139))))) >= l_4723))) <= 2L) > 0xFD00L)) && 2UL) , (void*)0) == (void*)0) && l_4723)) <= p_20)))) <= p_20);
                        if (g_3806)
                            goto lbl_4696;
                    }
                }
                else
                { /* block id: 2091 */
                    float l_4922 = (-0x1.Bp+1);
                    int32_t l_4929 = 0x2D12F149L;
                    uint16_t l_4935 = 5UL;
                    uint16_t **l_4940 = &g_3267[5][4];
                    for (p_22 = (-5); (p_22 <= (-24)); --p_22)
                    { /* block id: 2094 */
                        const int32_t ****l_4925 = &l_4923[2];
                        if (l_4875)
                            goto lbl_4696;
                        (*g_2734) = (1UL < (safe_lshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((((*g_3697) , (((*g_3697) = (((*l_4925) = l_4923[2]) == ((*l_4773) = (*l_4773)))) , &l_4662[0][6])) != l_4926) | (l_4927 == (l_4747 , &l_4928))), 65535UL)), 10)));
                        if (l_4929)
                            break;
                    }
                    (*l_4657) = l_4929;
                    for (g_1403 = (-17); (g_1403 > (-7)); g_1403++)
                    { /* block id: 2105 */
                        int32_t **l_4942[8][6] = {{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533},{&g_533,&g_533,&g_533,&g_533,&g_533,&g_533}};
                        uint32_t * const l_4946 = (void*)0;
                        uint32_t * const *l_4945[8][7][1] = {{{&l_4946},{&l_4946},{&l_4946},{(void*)0},{&l_4946},{&l_4946},{&l_4946}},{{&l_4946},{&l_4946},{(void*)0},{&l_4946},{&l_4946},{&l_4946},{&l_4946}},{{&l_4946},{(void*)0},{&l_4946},{&l_4946},{&l_4946},{&l_4946},{&l_4946}},{{(void*)0},{&l_4946},{&l_4946},{&l_4946},{&l_4946},{&l_4946},{(void*)0}},{{&l_4946},{&l_4946},{&l_4946},{&l_4946},{&l_4946},{(void*)0},{&l_4946}},{{&l_4946},{&l_4946},{&l_4946},{&l_4946},{(void*)0},{&l_4946},{&l_4946}},{{&l_4946},{&l_4946},{&l_4946},{(void*)0},{&l_4946},{&l_4946},{&l_4946}},{{&l_4946},{&l_4946},{(void*)0},{&l_4946},{&l_4946},{&l_4946},{&l_4946}}};
                        uint32_t * const **l_4944 = &l_4945[2][4][0];
                        uint32_t * const *l_4948 = (void*)0;
                        uint32_t * const **l_4947 = &l_4948;
                        int i, j, k;
                        (*p_21) |= 1L;
                        if (g_659)
                            goto lbl_4932;
                        (***g_1663) = p_21;
                    }
                }
                if ((safe_div_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u((l_4732 = (((safe_lshift_func_uint8_t_u_s((((safe_sub_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(p_20, (-1L))), 9)), (safe_sub_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(p_20, (safe_unary_minus_func_int64_t_s((safe_rshift_func_int8_t_s_u(p_20, 6)))))), ((l_4691[3][2] = 0x5755F02DL) == (safe_add_func_int64_t_s_s((safe_div_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((!(!(l_4875 = ((*g_3697) |= ((safe_unary_minus_func_uint32_t_u(l_4695)) || (0UL > ((*g_753) != 0xCF7BL))))))), p_20)), l_4980)), g_1403))))))) & 255UL) , l_4882[0][0][0]), l_4981)) , &g_1109) == l_4982[1][3])), p_22)) != p_22), p_22)))
                { /* block id: 2121 */
                    uint32_t l_4986 = 4UL;
                    (*g_2734) = l_4876[0][0][1];
                    l_4986++;
                    for (g_2142 = 10; (g_2142 != 14); g_2142++)
                    { /* block id: 2126 */
                        if (g_2030)
                            goto lbl_4991;
                        (*g_1443) = (*p_23);
                    }
                }
                else
                { /* block id: 2130 */
                    int32_t l_5006 = 0x0262A36DL;
                    const uint32_t ***l_5016 = &l_5014;
                    int64_t *l_5017 = (void*)0;
                    int64_t *l_5018 = &g_99;
                    const uint8_t **l_5023 = (void*)0;
                    const uint32_t **l_5038 = &l_5015[1];
                    int32_t l_5052 = (-6L);
                    int32_t l_5053[6] = {(-8L),(-8L),(-8L),(-8L),(-8L),(-8L)};
                    int i;
                    if ((safe_div_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(((l_4999[8] = (l_4998 = (void*)0)) == (l_5001 = l_5000)), ((safe_mod_func_uint16_t_u_u(((((((*l_5018) ^= (safe_rshift_func_uint16_t_u_s((--l_5007), ((-1L) && ((safe_rshift_func_uint16_t_u_u((((((safe_sub_func_uint32_t_u_u(((*g_3697) |= l_4732), 0x7E163F8CL)) != p_22) | 0x53L) , ((*l_5016) = l_5014)) != ((*g_1110) = (void*)0)), l_5006)) == 0x0A42ADD86085F988LL))))) , (*g_102)) , p_22) , p_22) && p_20), p_22)) , p_20))), 4)), (*p_23))))
                    { /* block id: 2139 */
                        int64_t l_5028 = 0L;
                        l_5037 = func_44(p_23, ((safe_mul_func_uint16_t_u_u((0UL <= l_4691[4][2]), ((safe_div_func_int8_t_s_s(((**l_4926) ^= (l_5023 != &l_4928)), (safe_sub_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((l_5028 & (!(safe_mod_func_int64_t_s_s(p_20, ((((l_4875 | (l_5036 |= (l_5032 != (void*)0))) , 0x239872DCL) , l_5028) & (*p_23)))))), 0x5CL)) ^ p_20), p_22)))) <= l_4981))) >= 0x25L), (***g_3750), (**g_609), l_4985);
                        l_5038 = (*l_5016);
                        (**g_3524) = 0x6A644EE3L;
                    }
                    else
                    { /* block id: 2145 */
                        uint64_t l_5045 = 9UL;
                        int32_t l_5048 = 0xB9969556L;
                        (*l_4660) = ((((*g_3752) || (((((l_5048 = ((((safe_mul_func_uint16_t_u_u(l_4876[1][1][3], ((p_22 > p_20) , (safe_sub_func_uint64_t_u_u(p_20, 1L))))) && ((**l_4926) &= 0x8FL)) , l_5045) && ((((safe_sub_func_int8_t_s_s(((l_4747 , p_22) >= l_4876[0][0][1]), l_4695)) < 4UL) & (*p_21)) >= 0x6633L))) <= l_5006) > l_5007) , 0x7D7117F4L) || 0UL)) && p_20) , p_20);
                        if (l_5049)
                            continue;
                        return l_5048;
                    }
                    l_5056--;
                }
            }
        }
        if ((p_22 != (safe_mod_func_int8_t_s_s((!((((((safe_mul_func_int16_t_s_s(p_22, (p_20 >= (p_23 == (void*)0)))) ^ ((safe_rshift_func_uint8_t_u_s(p_22, (safe_mul_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(((safe_add_func_int64_t_s_s(((0xB2EA1C02E3F0A255LL ^ (safe_div_func_int8_t_s_s(((~(safe_lshift_func_int16_t_s_s(((((*p_21) = ((void*)0 == (*g_317))) <= l_4747) & l_4689), p_22))) && 0xE0E3A811B34DB317LL), p_20))) , 0x4372EEFF82E9B829LL), 0x3C45030EDCDCF941LL)) & l_5007), (*l_4698))), p_22)))) == p_22)) | l_4882[0][0][0]) ^ p_22) , (*l_4698)) , (**g_3751))), 7L))))
        { /* block id: 2157 */
            int64_t *l_5097 = &g_659;
            int32_t l_5106 = 0xA500CB1CL;
            int32_t l_5107 = 0L;
            uint8_t **l_5116 = (void*)0;
            (*g_1665) = ((safe_mod_func_int64_t_s_s((***g_3750), (safe_lshift_func_int8_t_s_s((-9L), 6)))) , &l_4689);
            (*g_533) = (0xD5F3A37D0CF0BC5FLL & ((p_20 | ((safe_sub_func_uint64_t_u_u(p_22, ((safe_sub_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(p_20, l_4747)), 1L)) ^ ((((safe_lshift_func_int16_t_s_u(((***g_3979) |= (0xC1L | (4294967288UL || ((l_5116 != (void*)0) ^ (**g_532))))), 4)) ^ p_20) >= p_20) <= 0xDB30B726L)))) & l_5106)) <= p_22));
        }
        else
        { /* block id: 2170 */
            uint8_t l_5117 = 4UL;
            int32_t l_5134[4];
            uint64_t ***l_5172[3];
            int32_t l_5183 = 0x11A5AA47L;
            uint32_t l_5206 = 0x1ADFD3DEL;
            const float ***l_5313 = &g_3851[1];
            const float ****l_5312[4][6][8] = {{{&l_5313,&l_5313,&l_5313,(void*)0,&l_5313,(void*)0,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,(void*)0,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,(void*)0,&l_5313,&l_5313}},{{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,(void*)0,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,(void*)0,(void*)0,(void*)0},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313},{&l_5313,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,(void*)0}},{{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,(void*)0},{(void*)0,&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313}},{{(void*)0,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,(void*)0,&l_5313},{&l_5313,&l_5313,&l_5313,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{(void*)0,(void*)0,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313},{&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313,&l_5313}}};
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_5134[i] = 1L;
            for (i = 0; i < 3; i++)
                l_5172[i] = (void*)0;
            if ((*g_1443))
            { /* block id: 2171 */
                const int32_t l_5125 = 0x637EA170L;
                for (g_297 = 0; (g_297 <= 0); g_297 += 1)
                { /* block id: 2174 */
                    (*g_2734) |= (((*g_1663) == (void*)0) | (l_5117 | ((l_5118 && ((((void*)0 != (**g_1109)) | (safe_mod_func_uint32_t_u_u(4294967292UL, (safe_rshift_func_int16_t_s_s(p_20, (safe_add_func_uint32_t_u_u(((void*)0 != &l_5033[1][1][3]), l_5125))))))) < 0L)) , (*p_23))));
                    (*g_1665) = &l_4691[4][1];
                    for (g_2142 = 0; (g_2142 <= 0); g_2142 += 1)
                    { /* block id: 2179 */
                        int8_t l_5128[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_5128[i] = (-5L);
                        if ((**g_532))
                            break;
                        l_5128[0] = ((l_5127 = ((l_5125 || (~(-1L))) , l_5127)) == (void*)0);
                    }
                }
            }
            else
            { /* block id: 2185 */
                uint64_t l_5139[4][9] = {{0UL,0xA2CC6111204D1E27LL,0UL,0xA2CC6111204D1E27LL,0UL,0xA2CC6111204D1E27LL,0UL,0xA2CC6111204D1E27LL,0UL},{1UL,1UL,0x715D6C184A017461LL,0x715D6C184A017461LL,1UL,1UL,0x715D6C184A017461LL,0x715D6C184A017461LL,1UL},{4UL,0xA2CC6111204D1E27LL,4UL,0xA2CC6111204D1E27LL,4UL,0xA2CC6111204D1E27LL,4UL,0xA2CC6111204D1E27LL,4UL},{1UL,0x715D6C184A017461LL,0x715D6C184A017461LL,1UL,1UL,0x715D6C184A017461LL,0x715D6C184A017461LL,1UL,1UL}};
                float **l_5150 = &l_4660;
                int32_t l_5153 = (-10L);
                uint64_t l_5207[8] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
                int32_t l_5212[1][4];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_5212[i][j] = 0xA3E6617AL;
                }
lbl_5186:
                if (((l_4689 |= (safe_rshift_func_uint16_t_u_s((((+((&g_3219 == (void*)0) < (l_5134[0] |= (((safe_lshift_func_int16_t_s_s(0L, 2)) != (l_4691[4][0] , ((*g_3697) = 0xA11AFA60L))) , (++(*g_3697)))))) <= p_22) < (safe_div_func_uint8_t_u_u(((l_5140 |= l_5139[2][5]) && (safe_mod_func_uint32_t_u_u((safe_mod_func_int8_t_s_s(((((safe_div_func_int16_t_s_s(((**g_2139) |= ((void*)0 == (**g_1229))), p_22)) != p_20) | 1L) != p_22), p_22)), p_22))), l_5139[2][5]))), 6))) , 0x9BF8AFE4L))
                { /* block id: 2192 */
                    float **l_5151 = (void*)0;
                    int16_t *****l_5154 = &g_1635;
                    uint64_t *** const l_5159[7] = {&g_1232[0][4][0],&g_1232[0][4][0],&g_1232[0][4][0],&g_1232[0][4][0],&g_1232[0][4][0],&g_1232[0][4][0],&g_1232[0][4][0]};
                    int32_t l_5161 = 0L;
                    int i;
                    if (((-2L) == 0x7289L))
                    { /* block id: 2193 */
                        uint32_t l_5147 = 0UL;
                        l_5147 ^= 9L;
                        (*****l_5096) = (((l_5134[3] &= (l_4691[1][2] = (l_5147 ^ ((safe_lshift_func_int16_t_s_s(((l_4695 = (l_5139[1][5] & (l_5150 == l_5151))) | ((p_22 || (*g_2189)) == ((*p_21) <= (safe_unary_minus_func_int32_t_s(0xF93D2A45L))))), 15)) , ((l_5153 = 0xC4L) , (*p_23)))))) , (void*)0) == l_5154);
                    }
                    else
                    { /* block id: 2200 */
                        (**g_1664) = p_21;
                        if (l_4690)
                            goto lbl_5186;
                        return l_5140;
                    }
                    for (g_1378 = 0; (g_1378 <= 6); g_1378++)
                    { /* block id: 2206 */
                        return p_22;
                    }
                    for (g_2810 = 0; (g_2810 <= 27); g_2810++)
                    { /* block id: 2211 */
                        uint64_t l_5160 = 0x8F2643A3C382CF40LL;
                        l_5160 = ((void*)0 != l_5159[6]);
                        return p_20;
                    }
                    for (l_5007 = 0; (l_5007 <= 3); l_5007 += 1)
                    { /* block id: 2217 */
                        int i;
                        if (g_99)
                            goto lbl_4991;
                        l_5161 &= g_795[l_5007];
                    }
                }
                else
                { /* block id: 2221 */
                    int32_t * volatile * volatile ***l_5162 = &g_1663;
                    const uint64_t * const l_5171 = &g_1510[4][1];
                    const uint64_t * const *l_5170 = &l_5171;
                    const uint64_t * const **l_5169 = &l_5170;
                    int16_t **l_5182 = &g_252[2][1][2];
                    (*l_5162) = &g_1664;
                    for (g_2142 = 0; (g_2142 > 29); g_2142++)
                    { /* block id: 2225 */
                        int16_t *l_5180[3];
                        int16_t ***l_5181 = &l_5127;
                        uint64_t l_5184 = 0xC3F6FF00E69D5357LL;
                        int32_t l_5185 = 0x0032031EL;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_5180[i] = &g_2141;
                        (*g_2734) = (((safe_mul_func_int8_t_s_s(0x6FL, (l_5185 = ((safe_div_func_int8_t_s_s(l_5153, (l_5184 = ((l_5169 != (l_5173 = l_5172[0])) , (l_5183 = (safe_rshift_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((((safe_rshift_func_uint16_t_u_s(p_20, (l_5134[2] = (&g_4536 == &g_4536)))) ^ p_20) ^ ((((*g_2189) = (((*l_5181) = l_5127) == l_5182)) , (**g_3751)) >= g_635)), l_4691[1][2])), p_20))))))) , (*l_4698))))) , 0x07566F00EE381841LL) ^ 18446744073709551615UL);
                        return (*l_4698);
                    }
                }
                for (g_766 = 0; g_766 < 3; g_766 += 1)
                {
                    g_1980[g_766] = 0L;
                }
                if ((p_22 , (*g_2734)))
                { /* block id: 2239 */
                    g_1109 = (void*)0;
                }
                else
                { /* block id: 2241 */
                    int32_t *l_5195 = (void*)0;
                    int32_t **l_5196[3][3][1] = {{{&l_5195},{&g_2362},{&l_5195}},{{&g_2362},{&l_5195},{&g_2362}},{{&l_5195},{&g_2362},{&l_5195}}};
                    int32_t l_5200 = (-9L);
                    uint32_t ****l_5204 = &l_5203;
                    int32_t l_5208 = 0x4BDED6B1L;
                    uint32_t l_5214 = 1UL;
                    uint16_t l_5261 = 1UL;
                    int i, j, k;
                    if ((l_5207[4] = ((*p_21) = ((*g_2734) = ((l_5206 &= (safe_mod_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((safe_div_func_uint32_t_u_u((l_4876[1][0][0] , (&p_20 != (l_5197 = l_5195))), ((*p_21) , (safe_sub_func_uint32_t_u_u(l_5200, (safe_lshift_func_int16_t_s_s(((***g_3979) &= (((*l_5204) = l_5203) == (p_22 , l_5205))), l_5007))))))), 4L)), l_5200)) < 0x19B334C0972C1121LL), 1UL))) != l_4748)))))
                    { /* block id: 2249 */
                        int64_t l_5209 = 3L;
                        int32_t l_5210 = 0x57E14419L;
                        int32_t l_5211 = 0x6B455029L;
                        int32_t l_5213 = 0x7DBC484DL;
                        ++l_5214;
                    }
                    else
                    { /* block id: 2251 */
                        int8_t *l_5229 = &g_1980[0];
                        int32_t *l_5231 = &l_4664;
                        (**g_3524) = ((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(p_20, 0)), 2)) >= (safe_mod_func_uint16_t_u_u(((safe_div_func_uint32_t_u_u((((*g_3697) = (safe_unary_minus_func_int32_t_s(l_5212[0][1]))) > (((0xCA5AB57DL || (l_5226 != (*g_2432))) ^ (-1L)) == (safe_unary_minus_func_uint16_t_u((((void*)0 == l_5229) & ((void*)0 != (**g_1327))))))), l_5207[4])) & l_5214), l_5230)));
                        (***g_1663) = l_5231;
                    }
                    (**g_1664) = p_23;
                    for (l_4748 = 0; (l_4748 >= 26); l_4748 = safe_add_func_int32_t_s_s(l_4748, 2))
                    { /* block id: 2259 */
                        int16_t l_5260[2];
                        uint32_t ** const *l_5281 = &g_1111;
                        uint32_t ** const **l_5280 = &l_5281;
                        uint32_t ** const ***l_5279[7] = {&l_5280,&l_5280,&l_5280,&l_5280,&l_5280,&l_5280,&l_5280};
                        int i;
                        for (i = 0; i < 2; i++)
                            l_5260[i] = 0L;
                        (*p_21) = (((safe_rshift_func_int8_t_s_u((safe_sub_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(((safe_add_func_int32_t_s_s((p_22 && ((safe_lshift_func_uint16_t_u_u(((((((0x87B0017BL ^ (4294967295UL || (~(++(*l_5000))))) >= (safe_sub_func_uint16_t_u_u((((safe_sub_func_uint32_t_u_u(((safe_div_func_int32_t_s_s(((((l_5200 | (0x8E8930E5L >= (safe_add_func_int32_t_s_s((safe_lshift_func_int16_t_s_u(((((((*g_4535) == (void*)0) != (((safe_add_func_uint32_t_u_u((~l_5200), (p_20 , l_5260[0]))) || p_20) & p_20)) ^ l_5140) , l_5117) & l_5208), 13)), (*p_21))))) > (*p_21)) & p_22) || l_5007), p_20)) | l_5207[4]), p_22)) ^ (-3L)) , p_22), p_22))) > 1UL) ^ p_22) || p_22) ^ p_20), l_5117)) , (*p_21))), 0x09A34349L)) && l_5153), l_5200)), l_5208)), 0)) ^ p_20) < p_22);
                        l_5261--;
                        if ((*p_21))
                            break;
                        (*p_21) ^= (safe_rshift_func_uint16_t_u_u((0xCB7BL != (l_4695 = 0x9D10L)), (safe_mod_func_int64_t_s_s((safe_rshift_func_uint16_t_u_u((l_5200 = (0x1EBDBC17F8F91F3ALL != ((!(safe_lshift_func_int16_t_s_u((p_22 = (safe_lshift_func_int8_t_s_u(((l_5279[5] = ((safe_div_func_int64_t_s_s((p_20 , 0x2A1C73A3ED7B2D4ALL), (-1L))) , l_5279[5])) != (void*)0), 5))), 12))) != ((safe_lshift_func_uint8_t_u_s((l_5214 < 252UL), 3)) == p_20)))), 12)), (*g_3752)))));
                    }
                    (*p_21) &= l_5208;
                }
            }
            (*p_21) = (l_5315 = ((safe_div_func_int32_t_s_s((&l_5173 == (((((((((l_5134[2] ^ (((**g_3524) ^= (safe_mul_func_int16_t_s_s(0L, (safe_mod_func_int8_t_s_s(l_5290, (l_4691[2][2] = 0x6AL)))))) == (l_4689 &= (safe_add_func_int8_t_s_s((((safe_div_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((safe_sub_func_int8_t_s_s((safe_mod_func_int64_t_s_s((((g_5314 = (l_5311 = ((~(safe_unary_minus_func_int64_t_s(((safe_sub_func_uint64_t_u_u((safe_add_func_int64_t_s_s(p_22, ((((((((safe_rshift_func_int8_t_s_s((l_5183 = (((**g_3751) , (*g_1987)) == (l_5140 , l_5309))), 1)) > (*l_4698)) & (*p_21)) , l_5140) > p_22) , l_5134[3]) > 0x26L) | (-1L)))), l_5134[3])) , l_5117)))) , l_5311))) == (void*)0) | (*l_4698)), 0x9C30CFA7BCE1DDF2LL)), 255UL)), l_4695)), p_22)) > 0xC20C2E0FL) , 0xFCL), 0x59L))))) , p_20) , p_20) ^ 249UL) <= 18446744073709551611UL) >= (-9L)) , p_22) != (*g_530)) , &l_5172[1])), 1UL)) || l_4882[0][0][0]));
            for (g_907 = (-15); (g_907 < 19); g_907 = safe_add_func_uint16_t_u_u(g_907, 6))
            { /* block id: 2283 */
                uint64_t l_5318 = 0UL;
                l_5318++;
            }
        }
        for (g_448 = 0; (g_448 >= 27); g_448 = safe_add_func_uint32_t_u_u(g_448, 1))
        { /* block id: 2289 */
            return (**g_752);
        }
    }
    if ((*p_23))
    { /* block id: 2293 */
lbl_5354:
        (***g_1663) = &l_4643[3][1];
    }
    else
    { /* block id: 2295 */
        float l_5323 = (-0x6.Dp-1);
        uint32_t l_5324 = 0UL;
        int32_t l_5350 = 0xAD1B8A9EL;
        int32_t l_5353 = 0x05E63266L;
        int32_t l_5391 = 0xB6C040BBL;
        int32_t l_5392 = 0x8908EEF6L;
        int32_t l_5393 = 1L;
        int32_t l_5394 = (-1L);
        int32_t l_5395 = 0x78ECFB50L;
        int32_t l_5396 = (-5L);
        int64_t ***l_5400 = (void*)0;
        int8_t **l_5423[1][6][9] = {{{&l_4662[0][6],(void*)0,&l_4662[0][6],&l_4662[0][6],(void*)0,&l_4662[0][6],&l_4662[0][6],(void*)0,&l_4662[0][6]},{(void*)0,&l_4661,(void*)0,&l_4662[0][6],&l_4662[0][0],&l_4662[0][6],(void*)0,&l_4661,(void*)0},{&l_4662[0][6],(void*)0,&l_4662[0][6],&l_4662[0][6],(void*)0,&l_4662[0][6],&l_4662[0][6],(void*)0,&l_4662[0][6]},{(void*)0,&l_4661,(void*)0,&l_4662[0][6],&l_4662[0][0],&l_4662[0][6],(void*)0,&l_4661,(void*)0},{&l_4662[0][6],(void*)0,&l_4662[0][6],(void*)0,&l_4662[0][6],(void*)0,(void*)0,&l_4662[0][6],(void*)0},{&l_4661,&l_4662[0][6],&l_4661,&l_4662[0][5],(void*)0,&l_4662[0][5],&l_4661,&l_4662[0][6],&l_4661}}};
        const uint32_t l_5479 = 0xE7AB4154L;
        int32_t l_5488 = 0x82B433A9L;
        uint64_t l_5489 = 0x35F836EFC9EA230CLL;
        int32_t l_5500 = (-1L);
        int32_t l_5505 = 0x8F160F1CL;
        int32_t l_5506 = (-3L);
        int32_t l_5509 = 0xC4A21678L;
        int32_t l_5510 = (-5L);
        int32_t l_5511[7][4][6] = {{{0x564E1D8AL,9L,(-9L),0x90D33844L,(-2L),1L},{0xFA196440L,0x53A9200FL,8L,9L,0x36194DF2L,1L},{1L,0x7D3934FCL,(-9L),8L,(-1L),1L},{0x36194DF2L,1L,(-2L),1L,9L,0x829B7752L}},{{(-1L),1L,4L,1L,0L,7L},{(-2L),0xF167A3FEL,0x5460057FL,0x5460057FL,0xF167A3FEL,(-2L)},{8L,7L,0x15D387E2L,1L,0L,0L},{0x7D3934FCL,(-8L),5L,0xF167A3FEL,1L,0xB4EF7555L}},{{0x7D3934FCL,(-5L),0xF167A3FEL,1L,(-1L),0xFA196440L},{8L,0L,0x829B7752L,0x5460057FL,(-9L),(-8L)},{(-2L),0xB4EF7555L,(-5L),1L,(-8L),8L},{(-1L),6L,0xB4EF7555L,1L,0xB4EF7555L,6L}},{{0x36194DF2L,0x5460057FL,(-1L),8L,1L,1L},{1L,0L,7L,9L,0x53A9200FL,0L},{0xFA196440L,0L,1L,0x90D33844L,1L,0x36194DF2L},{0x564E1D8AL,0x5460057FL,9L,0xC47D6CA2L,0xB4EF7555L,4L}},{{7L,6L,0xFA196440L,(-8L),(-8L),0xC47D6CA2L},{0L,0xB4EF7555L,0L,(-9L),(-9L),0L},{0L,0L,1L,0x53A9200FL,(-1L),(-8L)},{(-8L),(-5L),1L,0xEB0D28DDL,1L,1L}},{{(-2L),(-8L),1L,7L,0L,(-8L)},{(-1L),7L,1L,1L,0xF167A3FEL,0L},{1L,0xF167A3FEL,0L,0x2B745867L,0L,0xC47D6CA2L},{0x5460057FL,1L,0xFA196440L,4L,9L,4L}},{{9L,1L,9L,1L,(-1L),0x36194DF2L},{0xEB0D28DDL,0x7D3934FCL,1L,0xFA196440L,0x36194DF2L,0L},{(-5L),0x53A9200FL,7L,0xFA196440L,(-2L),1L},{0xEB0D28DDL,9L,(-1L),1L,7L,6L}}};
        uint32_t l_5514 = 0xCCD55DC8L;
        const int32_t ***l_5542 = &g_4105;
        uint32_t * const *l_5568 = &g_896;
        int16_t l_5569 = 0x8BC5L;
        int i, j, k;
        for (g_3378 = 0; (g_3378 <= 1); g_3378 += 1)
        { /* block id: 2298 */
            const int16_t l_5348 = (-8L);
            int32_t l_5351 = (-2L);
            (**g_1664) = (void*)0;
            l_5324--;
            (**g_3524) &= (safe_mod_func_uint32_t_u_u(4294967294UL, ((l_5353 = ((+0xBAD5L) < (safe_add_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((l_5324 , (safe_rshift_func_uint8_t_u_s(((p_20 == (safe_lshift_func_uint8_t_u_s(l_5324, 6))) || (l_5351 = (((*g_3697) ^= (safe_div_func_uint8_t_u_u((safe_mod_func_int16_t_s_s((7UL == ((safe_add_func_int64_t_s_s((g_2663 = (safe_sub_func_uint8_t_u_u((l_5350 &= ((((***g_3979) = (safe_rshift_func_uint16_t_u_s((p_20 || (((0xE5L ^ 1L) || (*g_3752)) == l_5348)), p_20))) ^ p_20) == l_5348)), l_5348))), g_4521)) >= p_22)), l_5348)), 0x39L))) < 0x1454C901L))), p_20))), l_5352)), p_20)))) ^ l_5324)));
            if ((*g_69))
                break;
            for (g_2933 = 0; (g_2933 <= 1); g_2933 += 1)
            { /* block id: 2311 */
                uint8_t l_5355 = 0xE3L;
                for (g_659 = 0; (g_659 <= 1); g_659 += 1)
                { /* block id: 2314 */
                    return p_20;
                }
                if (g_4521)
                    goto lbl_5354;
                l_5355++;
            }
        }
        for (g_2663 = (-14); (g_2663 >= (-24)); --g_2663)
        { /* block id: 2323 */
            int64_t *l_5375 = (void*)0;
            int64_t *l_5376[9] = {&g_2663,&g_2663,&g_2663,&g_2663,&g_2663,&g_2663,&g_2663,&g_2663,&g_2663};
            const int32_t l_5379 = 1L;
            int32_t l_5383[4];
            int8_t ***l_5424[2][10][3] = {{{(void*)0,(void*)0,&l_5423[0][0][0]},{&g_3219,(void*)0,&g_3219},{&l_5423[0][5][1],&l_5423[0][0][2],&l_5423[0][0][0]},{&l_5423[0][5][1],(void*)0,(void*)0},{&g_3219,&l_5423[0][0][2],(void*)0},{(void*)0,(void*)0,&l_5423[0][0][0]},{&g_3219,(void*)0,&g_3219},{&l_5423[0][5][1],&l_5423[0][0][2],&l_5423[0][0][0]},{&l_5423[0][5][1],(void*)0,(void*)0},{&g_3219,&l_5423[0][0][2],(void*)0}},{{(void*)0,(void*)0,&l_5423[0][0][0]},{&g_3219,(void*)0,&g_3219},{&l_5423[0][5][1],&l_5423[0][0][2],&l_5423[0][0][0]},{&l_5423[0][5][1],(void*)0,(void*)0},{&g_3219,&l_5423[0][0][2],(void*)0},{(void*)0,(void*)0,&l_5423[0][0][0]},{&g_3219,(void*)0,&g_3219},{&l_5423[0][5][1],&l_5423[0][0][2],&l_5423[0][0][0]},{&l_5423[0][5][1],(void*)0,(void*)0},{&g_3219,&l_5423[0][0][2],(void*)0}}};
            const int32_t **l_5432[2][4][2];
            const int32_t ***l_5431 = &l_5432[1][1][1];
            int32_t *l_5433[1][1][10] = {{{&l_5394,&g_2685,&l_5394,&l_5394,&g_2685,&l_5394,&l_5394,&g_2685,&l_5394,&l_5394}}};
            int32_t l_5487[4][5][7] = {{{0x961B5F57L,0x598413BDL,0x1A0DAFABL,(-4L),0L,0xE91B4305L,0x01EF9187L},{(-1L),(-1L),0L,8L,0L,0L,0xE91B4305L},{0xE2D9DD8BL,1L,0L,(-4L),0x961B5F57L,1L,1L},{0xE2D9DD8BL,(-1L),0x1A0DAFABL,(-1L),0xE2D9DD8BL,1L,0xE91B4305L},{(-1L),0x598413BDL,0xCFFF9C3DL,(-1L),0x961B5F57L,0L,0x01EF9187L}},{{0x961B5F57L,0x598413BDL,0x1A0DAFABL,(-4L),0L,0xE91B4305L,0x01EF9187L},{(-1L),(-1L),0L,8L,0L,0L,0xE91B4305L},{0xE2D9DD8BL,1L,0L,(-4L),0x961B5F57L,1L,1L},{0xE2D9DD8BL,(-1L),0x1A0DAFABL,(-1L),0xE2D9DD8BL,1L,0xE91B4305L},{(-1L),0x598413BDL,0xCFFF9C3DL,(-1L),0x961B5F57L,0L,0x01EF9187L}},{{0x961B5F57L,0x598413BDL,0x1A0DAFABL,(-4L),0L,0xE91B4305L,0x01EF9187L},{(-1L),(-1L),0L,8L,0L,0L,0xE91B4305L},{0xE2D9DD8BL,1L,0L,(-4L),0x961B5F57L,1L,1L},{0xE2D9DD8BL,(-1L),0x1A0DAFABL,(-1L),0xE2D9DD8BL,1L,0xE91B4305L},{(-1L),0x598413BDL,0xCFFF9C3DL,(-1L),0x961B5F57L,0L,0x01EF9187L}},{{0x961B5F57L,0x598413BDL,0x1A0DAFABL,(-4L),0L,0xE91B4305L,0x01EF9187L},{(-1L),(-1L),0L,8L,0L,0L,0xE91B4305L},{0xE91B4305L,0x3A04963DL,0x511F0934L,0x691E4372L,0L,0L,0L},{0xE91B4305L,(-8L),(-1L),(-8L),0xE91B4305L,0L,0xCFFF9C3DL},{0xCD6B9BA1L,5L,0xEBB0459AL,(-8L),0L,0x1A0DAFABL,0x44A0C5E2L}}};
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_5383[i] = 4L;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    for (k = 0; k < 2; k++)
                        l_5432[i][j][k] = &l_4698;
                }
            }
            for (g_297 = 18; (g_297 >= 10); g_297 = safe_sub_func_uint16_t_u_u(g_297, 5))
            { /* block id: 2326 */
                int8_t l_5371[4] = {0x88L,0x88L,0x88L,0x88L};
                int64_t **l_5377 = (void*)0;
                int64_t **l_5378 = &l_5376[1];
                int32_t l_5382 = 0L;
                int32_t l_5384 = (-1L);
                int32_t l_5385 = 0xA0E47703L;
                int32_t l_5386[10][6] = {{0L,0L,8L,0x526F8B19L,(-7L),(-1L)},{0x5ABF8155L,0x6B573B1AL,0L,0L,0x6B573B1AL,0x5ABF8155L},{(-1L),(-7L),0x526F8B19L,8L,0L,0L},{0xFF7696C9L,0x5ABF8155L,(-1L),(-8L),(-6L),0xF3AAB67FL},{0xFF7696C9L,0xEBF0F8A7L,(-8L),8L,(-1L),0xD074D775L},{(-1L),0L,(-1L),0L,(-1L),0L},{0x5ABF8155L,(-1L),0x06CBBE9AL,0x526F8B19L,0xA1A0CDD7L,0x5ABF8155L},{0L,8L,(-1L),(-1L),0L,(-1L)},{0xF3AAB67FL,8L,(-7L),(-8L),0xA1A0CDD7L,0xBCAC7B63L},{0xD074D775L,(-1L),(-8L),(-1L),(-1L),(-8L)}};
                int32_t l_5421 = 0x9CCC0FC8L;
                int i, j;
                (***l_4731) = (+((p_20 != (((safe_div_func_int32_t_s_s((*p_23), p_22)) >= ((safe_lshift_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u(l_5371[0], (!((safe_div_func_int8_t_s_s(((l_5375 = l_5375) != ((*l_5378) = l_5376[1])), (*l_4698))) == p_22)))) && p_20) | l_5379), p_20)), 0)) , l_5324)) , p_22)) != l_5353));
                for (g_766 = (-2); (g_766 == 35); g_766++)
                { /* block id: 2332 */
                    int32_t l_5387 = 0x5583B9BAL;
                    int32_t l_5388 = (-1L);
                    int32_t l_5389 = 8L;
                    int32_t l_5390[6][1][4] = {{{0L,(-1L),0x8DEF166FL,(-1L)}},{{0L,(-4L),0xAAB15750L,(-1L)}},{{0xAAB15750L,(-1L),0xAAB15750L,(-4L)}},{{0L,(-1L),0x8DEF166FL,(-1L)}},{{0L,(-4L),0xAAB15750L,(-1L)}},{{0xAAB15750L,(-1L),0xAAB15750L,(-4L)}}};
                    int i, j, k;
                    l_5397++;
                }
                (*g_5405) = l_5400;
                for (g_160 = 0; (g_160 < 8); g_160 = safe_add_func_int32_t_s_s(g_160, 3))
                { /* block id: 2338 */
                    uint64_t l_5410 = 0x25D443DC8138995CLL;
                    int8_t l_5417 = (-1L);
                    int32_t l_5418 = (-3L);
                    uint16_t *l_5420 = &l_5118;
                    for (g_3504 = (-20); (g_3504 <= (-19)); g_3504++)
                    { /* block id: 2341 */
                        (*g_1665) = p_21;
                    }
                    (*l_4657) = ((*g_2603) <= (l_5410 == (((safe_div_func_int8_t_s_s((((safe_mul_func_int8_t_s_s(l_5417, 0x78L)) >= (l_5421 = ((l_5418 &= 0x46C9L) == ((*l_5420) = g_5419)))) <= ((((*g_2189) = ((l_5422 < l_5410) > p_20)) < (-7L)) , p_20)), p_22)) , 0xF.4604DAp+11) == p_20)));
                }
            }
            if (((p_20 > 65530UL) <= (((l_5425 = l_5423[0][5][1]) == ((safe_sub_func_int8_t_s_s((l_5391 = (l_5394 = (((***g_3979) ^= l_5383[0]) == ((((safe_rshift_func_uint16_t_u_s(p_20, 7)) != ((l_5433[0][0][1] = func_61(p_21, (((void*)0 == l_5430) , ((*l_5431) = (void*)0)), l_5393)) == p_23)) , p_20) || l_5393)))), p_22)) , &l_4661)) == 8L)))
            { /* block id: 2357 */
                int32_t l_5460 = 9L;
                uint32_t l_5476 = 0xBCFE5B78L;
                if (((*p_21) = ((((((safe_add_func_uint64_t_u_u((safe_unary_minus_func_uint64_t_u((safe_lshift_func_int16_t_s_u(((((safe_lshift_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s(((safe_lshift_func_int8_t_s_u((((*g_3697) = (~((((safe_div_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s((*l_4698), ((p_22 & ((((*g_2366) ^= ((safe_lshift_func_uint8_t_u_s((*l_4698), (safe_unary_minus_func_uint8_t_u((&g_4105 == (void*)0))))) > ((safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((**g_752), p_20)), 7)) ^ (*p_21)))) , ((l_5459 = ((safe_mod_func_int32_t_s_s((-1L), (*p_21))) , &p_22)) == (void*)0)) >= l_5350)) ^ 0x31E8L))), p_20)) == l_5460) ^ p_20) >= 0x30L))) ^ p_22), 7)) && (-1L)), 2)), l_5396)) <= (-1L)) ^ l_5460) <= p_22), p_20)))), 8UL)) , p_20) ^ l_5460) != l_5461) == p_22) & (**g_2139))))
                { /* block id: 2362 */
                    uint32_t l_5477 = 4294967291UL;
                    const int32_t l_5478[9][1] = {{(-1L)},{0L},{(-1L)},{0L},{(-1L)},{0L},{(-1L)},{0L},{(-1L)}};
                    int i, j;
                    (***g_1663) = p_21;
                    (*g_533) ^= l_5479;
                }
                else
                { /* block id: 2369 */
                    for (g_2141 = 28; (g_2141 > (-27)); g_2141 = safe_sub_func_uint8_t_u_u(g_2141, 6))
                    { /* block id: 2372 */
                        uint64_t l_5482 = 18446744073709551606UL;
                        (***g_1663) = p_23;
                        return l_5482;
                    }
                }
                (*p_21) ^= 1L;
            }
            else
            { /* block id: 2378 */
                const int32_t l_5486 = 0L;
                for (g_4048 = (-1); (g_4048 > (-14)); --g_4048)
                { /* block id: 2381 */
                    int32_t l_5485 = 0x444D89ACL;
                    (***g_1663) = p_21;
                    if ((**g_3524))
                    { /* block id: 2383 */
                        return p_22;
                    }
                    else
                    { /* block id: 2385 */
                        (**g_1665) |= 0x796AD66AL;
                        (***g_1664) |= l_5485;
                        if (l_5486)
                            break;
                    }
                }
            }
            l_5489++;
            for (g_634 = 0; (g_634 > 19); g_634++)
            { /* block id: 2395 */
                int64_t l_5494 = (-8L);
                int32_t l_5495 = 0x35120692L;
                int32_t l_5496 = 8L;
                int32_t l_5498 = 4L;
                int32_t l_5499 = (-3L);
                int32_t l_5501 = 1L;
                int32_t l_5502 = 0xEB7CFB51L;
                int32_t l_5503 = 0x3745A62EL;
                int32_t l_5504 = (-3L);
                int32_t l_5507 = (-1L);
                int32_t l_5508 = 0L;
                int32_t l_5512 = 0x57A1AE35L;
                int32_t l_5513[9][5][5] = {{{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L}},{{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL}},{{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L}},{{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L}},{{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L}},{{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL}},{{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L}},{{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L}},{{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L},{0x7A5CF5EEL,0L,0x7A5CF5EEL,0L,0x7A5CF5EEL},{1L,0xBC49BCE6L,0xBC49BCE6L,1L,1L},{0L,0L,0L,0L,0L},{1L,1L,0xBC49BCE6L,0xBC49BCE6L,1L}}};
                float l_5547 = 0x5.C9C1DCp+75;
                uint32_t **l_5567 = &g_896;
                int i, j, k;
                ++l_5514;
                (*g_1987) = (*g_1987);
                for (g_4521 = 1; (g_4521 <= 4); g_4521 += 1)
                { /* block id: 2400 */
                    uint32_t *****l_5517 = &g_1109;
                    int32_t l_5518 = 1L;
                    int32_t l_5544 = (-10L);
                    int32_t l_5546 = (-7L);
                    (*l_5517) = (void*)0;
                    for (l_4664 = 1; (l_4664 <= 4); l_4664 += 1)
                    { /* block id: 2404 */
                        uint32_t l_5519 = 1UL;
                        int i, j, k;
                        --l_5519;
                        return l_5513[(l_4664 + 2)][l_4664][l_4664];
                    }
                    for (l_5510 = 3; (l_5510 >= 0); l_5510 -= 1)
                    { /* block id: 2410 */
                        uint32_t ****l_5543[4][6];
                        int32_t l_5545 = 3L;
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 6; j++)
                                l_5543[i][j] = &l_5203;
                        }
                        if (g_200[l_5510][g_4521][(l_5510 + 1)])
                            break;
                        (**g_1664) = (void*)0;
                        (***g_1663) = &l_4664;
                        (*p_21) = (((safe_lshift_func_uint16_t_u_s(((((safe_sub_func_float_f_f((l_5546 = (l_5545 = (safe_add_func_float_f_f(((safe_add_func_float_f_f((safe_sub_func_float_f_f(g_200[l_5510][g_4521][(l_5510 + 2)], (safe_div_func_float_f_f((**g_2602), (l_5544 = (safe_sub_func_float_f_f((l_5518 = ((*l_4660) = (l_5513[2][1][4] = p_20))), (safe_add_func_float_f_f(((((p_20 >= ((((safe_mod_func_uint64_t_u_u(((((l_5542 == (void*)0) | ((void*)0 == l_5543[0][0])) & l_5501) <= l_5505), g_200[l_5510][g_4521][(l_5510 + 2)])) ^ (-10L)) , (void*)0) == (void*)0)) > p_20) <= 0xC.FD88DCp+50) != p_20), l_5396))))))))), 0x2.4BFDE9p+35)) == 0x4.ED63C4p-22), l_5395)))), p_20)) , l_5547) , (*p_21)) , p_22), p_20)) <= p_22) < p_20);
                    }
                    if (((*g_2734) = ((safe_mul_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_s(l_5546, (p_22 = p_22))) <= (safe_rshift_func_int16_t_s_u((***g_3979), 14))) <= p_20), 0xFBL)) && 4294967295UL)))
                    { /* block id: 2424 */
                        if ((*p_23))
                            break;
                        if (g_72)
                            goto lbl_4932;
                    }
                    else
                    { /* block id: 2427 */
                        uint32_t l_5566 = 0UL;
                        (*l_4657) = (((safe_add_func_float_f_f(((&g_1663 == (void*)0) < 0xC.919DEFp-95), ((*g_3221) == (l_5423[0][5][1] = &g_3220)))) <= (!l_5353)) > ((((safe_unary_minus_func_int16_t_s(((safe_sub_func_int8_t_s_s(p_22, ((!0x748F2054L) ^ (safe_add_func_int64_t_s_s((l_5396 = (safe_add_func_int32_t_s_s((((g_1980[0] &= p_20) , (-9L)) || 0x0F19L), l_5565))), l_5566))))) <= (*g_530)))) , 0x7D5E134A0E772DCDLL) , l_5567) == l_5568));
                    }
                }
            }
        }
        p_23 = (l_5509 , func_65(func_57((*g_3630), l_5569, ((((safe_add_func_float_f_f((-0x2.8p+1), (safe_mul_func_float_f_f((((***l_4731) = (((l_5511[6][3][1] = (safe_mul_func_float_f_f((((safe_div_func_float_f_f((((safe_sub_func_int16_t_s_s(l_5394, (((safe_rshift_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s((((l_5584 != (void*)0) , p_20) != (253UL == p_20)), (**g_1328))), 7)) ^ l_5585[0][2][3]) && p_20))) , l_5586) <= (*g_3852)), p_20)) , 0xB.340A31p-32) , p_22), l_5587))) > 0xD.7F7C7Fp+38) > (-0x5.Ap-1))) <= 0x1.3p-1), (-0x9.6p+1))))) > (*l_4698)) , p_22) , p_20)), p_23));
        l_5395 |= (*p_23);
    }
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t  func_24(int64_t  p_25, int32_t * p_26, uint8_t  p_27)
{ /* block id: 1955 */
    int16_t l_4634 = 0x429AL;
    l_4634 = 0x3.6B6F48p+29;
    return l_4634;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_68 g_72 g_99 g_102 g_103 g_101 g_127 g_79 g_200 g_208 g_69 g_160 g_297 g_348 g_226 g_371 g_372 g_373 g_395 g_224 g_448 g_317 g_318 g_493 g_531 g_532 g_533 g_252 g_592 g_609 g_639 g_593 g_634 g_696 g_530 g_635 g_640 g_752 g_753 g_754 g_795 g_907 g_659 g_943 g_945 g_766 g_897 g_1109 g_1380 g_1408 g_1327 g_1328 g_1329 g_1443 g_2734 g_3188 g_1663 g_1664 g_1665 g_2189 g_3751 g_3752 g_3753 g_3524 g_1510 g_2030 g_4536 g_4537 g_1229 g_1230 g_1231 g_1232 g_3979 g_2139 g_2140 g_4048 g_1111 g_896 g_4631
 * writes: g_72 g_79 g_99 g_101 g_103 g_127 g_159 g_160 g_68 g_224 g_226 g_252 g_297 g_316 g_349 g_448 g_293 g_493 g_529 g_533 g_609 g_634 g_659 g_671 g_766 g_795 g_896 g_907 g_928 g_943 g_696 g_1109 g_635 g_1378 g_897 g_1980 g_3188 g_2030 g_2142 g_2141 g_4048 g_1510
 */
static int32_t * func_32(int32_t * p_33, const int8_t  p_34, int32_t * p_35)
{ /* block id: 2 */
    uint8_t l_50 = 0x3FL;
    int32_t **l_1406[3];
    uint32_t l_1407 = 0x1FF6D02DL;
    int64_t l_3950 = 0x53F6ACDA00ECBDBFLL;
    uint8_t * const *l_3954 = &g_2189;
    uint8_t * const **l_3953[9][1] = {{&l_3954},{&l_3954},{&l_3954},{&l_3954},{&l_3954},{&l_3954},{&l_3954},{&l_3954},{&l_3954}};
    uint8_t * const ** const *l_3952 = &l_3953[2][0];
    uint32_t *l_3968 = &g_200[3][1][0];
    uint32_t l_3981 = 6UL;
    uint32_t ***l_3999[6];
    uint64_t l_4003 = 18446744073709551611UL;
    int16_t l_4007 = 0L;
    uint8_t ** const **l_4036 = &g_2187[1];
    int64_t *l_4039 = &g_99;
    int32_t l_4058 = 0xAF4FDBCEL;
    int8_t l_4059 = 0xE0L;
    float ***l_4074 = &g_609;
    int16_t * const *l_4098 = (void*)0;
    uint64_t l_4135 = 18446744073709551610UL;
    int32_t l_4143 = (-7L);
    uint64_t l_4329 = 0x295D0C5FD78DB584LL;
    const int16_t l_4332 = 0xFD7FL;
    int32_t **l_4356 = &g_533;
    int8_t * const * const l_4362 = &g_3220;
    uint64_t l_4384[6][1][6] = {{{18446744073709551609UL,1UL,0UL,1UL,18446744073709551609UL,18446744073709551609UL}},{{0x001A3C8606661588LL,1UL,1UL,0x001A3C8606661588LL,0x775878EC11135B92LL,0x001A3C8606661588LL}},{{0x001A3C8606661588LL,0x775878EC11135B92LL,0x001A3C8606661588LL,1UL,1UL,0x001A3C8606661588LL}},{{18446744073709551609UL,18446744073709551609UL,1UL,0UL,1UL,18446744073709551609UL}},{{1UL,0x775878EC11135B92LL,0UL,0UL,0x775878EC11135B92LL,0x001A3C8606661588LL}},{{1UL,0x001A3C8606661588LL,0x775878EC11135B92LL,0x001A3C8606661588LL,1UL,1UL}}};
    uint64_t l_4416 = 1UL;
    uint32_t l_4421 = 0xFDF51455L;
    int32_t l_4471 = 9L;
    uint16_t l_4472 = 65534UL;
    uint16_t * const  volatile *** volatile * volatile l_4474 = &g_4475;/* VOLATILE GLOBAL l_4474 */
    float ** const *l_4488 = &g_609;
    float ** const **l_4487 = &l_4488;
    float ** const ***l_4486 = &l_4487;
    int16_t l_4576 = 0x372BL;
    int64_t l_4615 = 0xEB931A71AE4D886BLL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1406[i] = (void*)0;
    for (i = 0; i < 6; i++)
        l_3999[i] = &g_2793;
    if ((safe_div_func_uint64_t_u_u(p_34, func_38(g_4[0][0], func_44((l_50 , ((*g_532) = func_51(l_50, p_34, p_34, ((0L != 65532UL) , 0x4E8160A8L), l_50))), l_1407, g_1380, g_1408, g_200[1][4][0]), p_34, p_35, (*g_69)))))
    { /* block id: 1627 */
        int32_t l_3951 = 3L;
        uint8_t * const ** const **l_3955 = (void*)0;
        uint8_t * const ** const **l_3956 = &l_3952;
        int32_t l_3993 = 1L;
        int8_t ***l_4046 = &g_3219;
        uint32_t *l_4047 = &g_3806;
        int32_t l_4050 = 0x7D9AFAF6L;
        int32_t l_4053 = 0xE7BB9D97L;
        int32_t l_4055[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        float l_4079 = 0x0.Dp+1;
        int16_t *l_4104 = (void*)0;
        uint8_t *l_4301 = &g_4060[1][2];
        int32_t **l_4355 = (void*)0;
        int8_t l_4396 = 0xDDL;
        int32_t l_4412 = 0xAA052822L;
        int64_t l_4478 = 0x066377F5CB66BE14LL;
        uint16_t **l_4541 = (void*)0;
        uint16_t ***l_4540 = &l_4541;
        uint16_t **** const l_4539 = &l_4540;
        uint16_t **** const *l_4538 = &l_4539;
        const uint16_t *l_4558 = &g_4559[1][0];
        const uint16_t **l_4557 = &l_4558;
        const uint16_t ***l_4556[4] = {&l_4557,&l_4557,&l_4557,&l_4557};
        const uint16_t ****l_4555 = &l_4556[2];
        const uint16_t *****l_4554 = &l_4555;
        int i;
    }
    else
    { /* block id: 1893 */
        for (l_4472 = 0; l_4472 < 3; l_4472 += 1)
        {
            g_1980[l_4472] = 0xFAL;
        }
        (*p_35) ^= 0xD4773189L;
    }
    for (g_3188 = 0; (g_3188 <= 0); g_3188 += 1)
    { /* block id: 1899 */
        int16_t l_4567 = 0x232FL;
        int32_t ***l_4577 = &l_4356;
        uint32_t l_4578[1][9] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}};
        int8_t *l_4579 = &l_4059;
        int32_t l_4583 = 0xF4B7FC67L;
        int32_t l_4585 = (-1L);
        float l_4630 = 0x7.53EB8Fp+82;
        int i, j;
        (***g_1663) = p_35;
        (**g_3524) = ((((*l_4579) = (((+(l_4567 <= ((safe_mul_func_int8_t_s_s(((((void*)0 != (*g_1663)) & ((safe_mul_func_int16_t_s_s(((safe_div_func_int64_t_s_s((((safe_sub_func_int64_t_s_s(l_4576, (l_4577 != (void*)0))) <= ((**l_3954) = ((void*)0 == &g_3219))) || 0xCDAB74ACL), (**g_3751))) != 0x910B214C2422914BLL), p_34)) , 0x34B6E75292EB0524LL)) | 0L), (***l_4577))) ^ p_34))) || (***l_4577)) < l_4578[0][6])) < (***l_4577)) && (***l_4577));
        for (l_3981 = 0; (l_3981 <= 0); l_3981 += 1)
        { /* block id: 1906 */
            (***l_4577) = 0x3F238A44L;
            return (*l_4356);
        }
        for (g_72 = 0; (g_72 <= 0); g_72 += 1)
        { /* block id: 1912 */
            int32_t l_4580 = (-6L);
            int32_t l_4581 = (-1L);
            int32_t l_4582 = 0x0ABE85DFL;
            int32_t l_4584 = 0xB0BA435AL;
            int32_t l_4586[2];
            int i, j;
            for (i = 0; i < 2; i++)
                l_4586[i] = 0x81D03DF5L;
            l_4580 &= g_1510[(g_3188 + 4)][g_72];
            p_35 = (*l_4356);
            (**g_1665) = (*p_33);
            for (g_2030 = 0; (g_2030 <= 0); g_2030 += 1)
            { /* block id: 1918 */
                uint32_t l_4587 = 5UL;
                ++l_4587;
                for (g_635 = 0; (g_635 <= 0); g_635 += 1)
                { /* block id: 1922 */
                    uint32_t l_4590 = 1UL;
                    uint16_t ***l_4591 = (void*)0;
                    for (l_4576 = 0; (l_4576 <= 5); l_4576 += 1)
                    { /* block id: 1925 */
                        uint64_t **l_4592 = &g_530;
                        (**l_4356) |= 0x9A701622L;
                        if (l_4590)
                            continue;
                        l_4584 |= (((((l_4591 != (*g_4536)) , l_4592) != (***g_1229)) < (safe_mul_func_int16_t_s_s(0x0A1CL, ((safe_sub_func_int16_t_s_s((p_34 & ((*p_33) <= p_34)), (l_4586[1] = 0x8C91L))) , (*g_753))))) ^ (***l_4577));
                    }
                    for (l_4567 = 0; (l_4567 >= 0); l_4567 -= 1)
                    { /* block id: 1933 */
                        uint32_t l_4599 = 0UL;
                        (*g_102) = (***l_4577);
                        (***l_4577) = (l_4599 < ((255UL <= l_4599) != (safe_rshift_func_uint8_t_u_s((((l_4586[1] = ((***g_3979) = ((*g_1230) != (**g_1229)))) ^ ((!(***l_4577)) != (p_34 , (safe_sub_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u(0x7EL, ((*l_4579) |= (-9L)))), p_34)), 0x5CEBF87E6ECF76DCLL))))) & 1L), p_34))));
                    }
                    for (g_4048 = 0; (g_4048 <= 0); g_4048 += 1)
                    { /* block id: 1942 */
                        uint32_t l_4629 = 0xCEEF9BE1L;
                        float l_4632 = (-0x1.1p+1);
                        int i, j;
                        (****g_1663) &= (safe_sub_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(((*g_1111) != p_33), (*g_2189))), (safe_rshift_func_int8_t_s_u(l_4615, 1))));
                        l_4582 = (safe_div_func_int64_t_s_s((((safe_add_func_uint16_t_u_u((safe_add_func_int32_t_s_s(((**g_1665) = (((!((safe_div_func_uint32_t_u_u(0x1A9B3BC4L, (safe_lshift_func_int16_t_s_s(0x2AB7L, 15)))) & ((g_1510[(g_3188 + 7)][(g_4048 + 2)] |= ((((((*l_4039) = l_4629) == (p_34 >= l_4586[0])) & (0x3795797BL < 1UL)) == 1UL) <= 0x0A139A13L)) , p_34))) || (***l_4577)) >= g_4631)), 7UL)), l_4586[1])) == 0xF8F1L) , 0x9D4314F7B553D99FLL), p_34));
                    }
                }
            }
        }
    }
    return p_35;
}


/* ------------------------------------------ */
/* 
 * reads : g_2734 g_72
 * writes: g_72
 */
static int64_t  func_38(float  p_39, int32_t * p_40, uint32_t  p_41, int32_t * p_42, int32_t  p_43)
{ /* block id: 1624 */
    int32_t l_3934[1];
    int i;
    for (i = 0; i < 1; i++)
        l_3934[i] = 0x9137877EL;
    (*g_2734) ^= 1L;
    return l_3934[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_897 g_696 g_1327 g_1328 g_1329 g_160 g_69 g_4 g_127 g_1443 g_72
 * writes: g_1378 g_897 g_696 g_103 g_72
 */
static int32_t * func_44(int32_t * p_45, int16_t  p_46, int64_t  p_47, float  p_48, uint16_t  p_49)
{ /* block id: 538 */
    int64_t l_1411 = 1L;
    uint64_t ***l_1418 = &g_1232[0][4][0];
    const float *l_1425 = &g_293[2][5];
    const float **l_1424 = &l_1425;
    const float ***l_1423 = &l_1424;
    const float ****l_1422 = &l_1423;
    uint8_t ****l_1434 = &g_1270[2];
    uint8_t *****l_1433[1][8][1];
    uint16_t l_1442[6][4] = {{0xA792L,0xFD6EL,0xDEB7L,0x7DFDL},{0xFD6EL,0UL,0UL,0xFD6EL},{0xA249L,0x7DFDL,0UL,0x33DBL},{0xFD6EL,0xA792L,0xDEB7L,0xA792L},{0xA792L,0UL,0xA249L,0xA792L},{0xA249L,0xA792L,0x33DBL,0x33DBL}};
    int32_t l_1444 = 0x3F61E009L;
    int32_t l_1464 = 4L;
    int64_t l_1526 = (-1L);
    int32_t l_1534[9] = {(-1L),0x89A52BBEL,(-1L),0x89A52BBEL,(-1L),0x89A52BBEL,(-1L),0x89A52BBEL,(-1L)};
    uint32_t ** const l_1580 = (void*)0;
    int16_t * const *l_1640[7][4][6] = {{{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]}},{{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]}},{{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]}},{{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]}},{{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]}},{{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[5],&g_1344[0]},{&g_1344[5],&g_1344[5],&g_1344[0],&g_1344[5],&g_1344[1],&g_1344[5]},{&g_1344[1],&g_1344[1],&g_1344[5],&g_1344[1],&g_1344[1],&g_1344[5]},{&g_1344[1],&g_1344[1],&g_1344[5],&g_1344[1],&g_1344[1],&g_1344[5]}},{{&g_1344[1],&g_1344[1],&g_1344[5],&g_1344[1],&g_1344[1],&g_1344[5]},{&g_1344[1],&g_1344[1],&g_1344[5],&g_1344[1],&g_1344[1],&g_1344[5]},{&g_1344[1],&g_1344[1],&g_1344[5],&g_1344[1],&g_1344[1],&g_1344[5]},{&g_1344[1],&g_1344[1],&g_1344[5],&g_1344[1],&g_1344[1],&g_1344[5]}}};
    int16_t * const **l_1639[2][5][7] = {{{&l_1640[6][1][2],&l_1640[6][1][2],(void*)0,&l_1640[1][0][5],&l_1640[4][2][5],&l_1640[4][2][5],&l_1640[1][0][5]},{&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5]},{&l_1640[6][1][2],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[6][1][2],&l_1640[4][2][5],(void*)0,(void*)0},{(void*)0,&l_1640[1][0][5],(void*)0,&l_1640[1][0][5],(void*)0,&l_1640[1][0][5],(void*)0},{&l_1640[4][2][5],&l_1640[6][1][2],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[6][1][2],&l_1640[4][2][5],(void*)0}},{{&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5]},{&l_1640[4][2][5],&l_1640[1][0][5],(void*)0,&l_1640[6][1][2],&l_1640[6][1][2],(void*)0,&l_1640[1][0][5]},{(void*)0,&l_1640[1][0][5],(void*)0,&l_1640[1][0][5],(void*)0,&l_1640[1][0][5],(void*)0},{&l_1640[6][1][2],&l_1640[6][1][2],(void*)0,&l_1640[1][0][5],&l_1640[4][2][5],&l_1640[4][2][5],&l_1640[1][0][5]},{&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5],&l_1640[1][0][5]}}};
    int16_t * const ***l_1638[7];
    int32_t *l_1666[10] = {&g_72,&g_4[0][0],&g_4[0][0],&g_72,&g_4[0][0],&g_4[0][0],&g_72,&g_4[0][0],&g_4[0][0],&g_72};
    uint32_t l_1736[9][4][2] = {{{0xF08E4DC4L,0xBAF7A3D4L},{1UL,0x3DDBE4A4L},{0xE096ED8AL,0x191BD1E4L},{9UL,0xE096ED8AL}},{{5UL,6UL},{5UL,0xE096ED8AL},{9UL,0x191BD1E4L},{0xE096ED8AL,0x3DDBE4A4L}},{{1UL,0xBAF7A3D4L},{0xF08E4DC4L,0x756E5B07L},{0x756E5B07L,4294967295UL},{0x191BD1E4L,0xBAF7A3D4L}},{{0x3F9D1153L,0x6245DD15L},{0xE096ED8AL,0xF08E4DC4L},{4294967293UL,0xE096ED8AL},{4294967295UL,4294967295UL}},{{5UL,1UL},{4294967293UL,0x191BD1E4L},{1UL,0x6245DD15L},{1UL,0x0807594AL}},{{0x191BD1E4L,0x756E5B07L},{4294967295UL,0x756E5B07L},{0x191BD1E4L,0x0807594AL},{1UL,0x6245DD15L}},{{1UL,0x191BD1E4L},{4294967293UL,1UL},{5UL,4294967295UL},{4294967295UL,0xE096ED8AL}},{{4294967293UL,0xF08E4DC4L},{0xE096ED8AL,0x6245DD15L},{0x3F9D1153L,0xBAF7A3D4L},{0x191BD1E4L,4294967295UL}},{{0x756E5B07L,0x756E5B07L},{0xF08E4DC4L,0xBAF7A3D4L},{1UL,0x3DDBE4A4L},{0xE096ED8AL,0x191BD1E4L}}};
    int32_t l_1754 = (-1L);
    uint32_t l_1785 = 4294967292UL;
    int32_t l_1822 = 0x826A103CL;
    int64_t l_1883 = 0xFAC970DF71C22D4CLL;
    int16_t *l_1889[8][8][2] = {{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}},{{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160},{&g_160,&g_160}}};
    int16_t ****l_1894 = &g_1636;
    uint8_t l_1920 = 1UL;
    int8_t l_1934 = 0L;
    uint32_t l_1947 = 0x31BF4CE5L;
    float l_2008 = 0x9.6p+1;
    uint64_t **l_2073 = &g_530;
    uint32_t *****l_2094 = &g_1109;
    float ***l_2095 = &g_609;
    float ****l_2096 = (void*)0;
    float ****l_2097 = &l_2095;
    float ***l_2098[7];
    uint16_t l_2099 = 0x6152L;
    uint32_t l_2381 = 0x30379E35L;
    uint8_t l_2399 = 0UL;
    int16_t **l_2416 = &l_1889[4][4][1];
    int16_t ** const *l_2415 = &l_2416;
    int16_t ** const **l_2414 = &l_2415;
    uint16_t l_2462 = 65527UL;
    int32_t *** const l_2481 = &g_208;
    int32_t *** const *l_2480 = &l_2481;
    uint32_t l_2558 = 4294967288UL;
    float l_2589 = (-0x1.Ep-1);
    int64_t l_2678 = 1L;
    uint8_t l_2789 = 0xDDL;
    const int32_t l_2800 = 5L;
    uint32_t l_2820 = 18446744073709551607UL;
    float ****l_2823 = &l_2098[6];
    uint32_t **l_2849[5];
    uint64_t l_2866 = 1UL;
    float l_2871 = 0xC.F957A2p+62;
    uint32_t l_2873 = 18446744073709551608UL;
    int32_t *l_2902 = &g_2685;
    uint32_t l_2926 = 5UL;
    const int32_t **l_2975 = &g_349[1][6];
    int32_t l_3027 = 0x94A0B60CL;
    uint64_t l_3101 = 18446744073709551612UL;
    int8_t l_3147 = 0xBEL;
    uint8_t l_3186 = 0x5DL;
    uint64_t **l_3196[1];
    float ***l_3265 = (void*)0;
    float **** const l_3264 = &l_3265;
    float **** const *l_3263[9][9][3] = {{{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,(void*)0},{&l_3264,(void*)0,(void*)0},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,(void*)0,&l_3264},{&l_3264,&l_3264,&l_3264}},{{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,(void*)0,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,(void*)0,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,(void*)0},{&l_3264,&l_3264,(void*)0}},{{&l_3264,&l_3264,&l_3264},{&l_3264,(void*)0,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264}},{{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,(void*)0},{&l_3264,(void*)0,(void*)0},{(void*)0,&l_3264,(void*)0},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264}},{{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,(void*)0,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264}},{{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,(void*)0},{&l_3264,(void*)0,(void*)0}},{{(void*)0,&l_3264,(void*)0},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,(void*)0,&l_3264},{&l_3264,&l_3264,&l_3264}},{{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264}},{{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,(void*)0},{&l_3264,(void*)0,(void*)0},{(void*)0,&l_3264,(void*)0},{&l_3264,&l_3264,&l_3264},{(void*)0,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264},{&l_3264,&l_3264,&l_3264}}};
    int32_t l_3272 = (-6L);
    int16_t l_3339[7][4] = {{0x3416L,0xD8C1L,0xB336L,0xB336L},{0x4CC3L,0x4CC3L,0xAEB6L,0xD8C1L},{0xD8C1L,0x3416L,0xAEB6L,0xE6BCL},{1L,0xD8C1L,0xAEB6L,0x4CC3L},{0xE6BCL,0xD8C1L,0xD8C1L,0xE6BCL},{0xD8C1L,0xE6BCL,1L,0xB336L},{0xD8C1L,1L,0xD8C1L,0xAEB6L}};
    const int8_t l_3382 = (-1L);
    uint32_t **l_3393 = &g_2366;
    int32_t l_3394 = 0xD0DB54CAL;
    uint16_t l_3399[10][6] = {{0x6CF0L,1UL,0xEF5EL,0xEF5EL,1UL,0x6CF0L},{65531UL,65530UL,0x4125L,0x66F9L,65535UL,0x6CF0L},{0x4125L,1UL,0xEF5EL,0UL,0xEB00L,0xEF5EL},{0x4125L,65535UL,0UL,0x66F9L,65530UL,0x66F9L},{65531UL,65535UL,65531UL,0xEF5EL,0xEB00L,0UL},{0x6CF0L,1UL,65531UL,0x6CF0L,65535UL,0x66F9L},{0x66F9L,65530UL,0UL,0x6CF0L,1UL,0xEF5EL},{0x6CF0L,1UL,0xEF5EL,0xEF5EL,1UL,0x6CF0L},{65531UL,65530UL,0x4125L,0x66F9L,65535UL,0x6CF0L},{0x4125L,1UL,0xEF5EL,0UL,0xEB00L,0xEF5EL}};
    int32_t **l_3424 = &g_2362;
    int64_t l_3427 = (-1L);
    int8_t **l_3440 = &g_3220;
    const uint16_t l_3442[3] = {65531UL,65531UL,65531UL};
    uint16_t l_3469 = 65535UL;
    int32_t l_3503 = 0L;
    float l_3570 = 0x4.Bp+1;
    int64_t l_3614 = 1L;
    uint32_t l_3670 = 0UL;
    uint64_t ****l_3694 = &l_1418;
    uint32_t *****l_3769[2][7][3] = {{{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,(void*)0},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548}},{{&g_3548,&g_3548,(void*)0},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548},{&g_3548,&g_3548,&g_3548}}};
    int8_t l_3819 = 1L;
    float l_3879 = 0x2.434B41p+23;
    int16_t l_3898 = 0x3BF3L;
    const int32_t l_3903 = (-1L);
    int64_t l_3909[3][7][4] = {{{0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL},{1L,1L,(-1L),1L},{1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL,1L},{0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL},{1L,1L,(-1L),1L},{1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL,1L},{0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL}},{{1L,1L,(-1L),1L},{1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL,1L},{0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL},{1L,1L,(-1L),1L},{1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL,1L},{0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL},{1L,1L,(-1L),1L}},{{1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL,1L},{0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL},{1L,1L,(-1L),1L},{0xB8D21DCBB7C31749LL,(-1L),(-1L),0xB8D21DCBB7C31749LL},{(-1L),0xB8D21DCBB7C31749LL,(-1L),(-1L)},{0xB8D21DCBB7C31749LL,0xB8D21DCBB7C31749LL,1L,0xB8D21DCBB7C31749LL},{0xB8D21DCBB7C31749LL,(-1L),(-1L),0xB8D21DCBB7C31749LL}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 1; k++)
                l_1433[i][j][k] = &l_1434;
        }
    }
    for (i = 0; i < 7; i++)
        l_1638[i] = &l_1639[1][1][2];
    for (i = 0; i < 7; i++)
        l_2098[i] = &g_609;
    for (i = 0; i < 5; i++)
        l_2849[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_3196[i] = &g_530;
    for (g_1378 = (-5); (g_1378 > 28); g_1378 = safe_add_func_uint64_t_u_u(g_1378, 6))
    { /* block id: 541 */
        uint32_t *l_1412 = &g_897;
        int32_t *l_1415 = &g_696;
        int32_t l_1421 = (-1L);
        const float ****l_1426 = &l_1423;
        float *l_1427 = &g_103;
        int16_t **l_1428 = &g_252[3][3][1];
        int16_t l_1437 = 0x73D7L;
        int32_t *l_1445 = (void*)0;
        int32_t *l_1446 = &g_72;
        uint64_t *l_1471 = &g_1408;
        uint64_t ***l_1473 = &g_1232[0][1][1];
        float ***l_1484 = &g_609;
        int64_t l_1487 = 0xE069840330D3E51ELL;
        int32_t l_1533 = 8L;
        uint32_t l_1594 = 4UL;
        int16_t ****l_1637 = &g_1636;
        uint16_t l_1646 = 0xAEADL;
        int16_t l_1686 = (-1L);
        int32_t l_1687 = 0xAF306B3EL;
        uint32_t **l_1712 = &g_896;
        int32_t l_1737 = 0xE28B2BC9L;
        int32_t l_1929[3][9] = {{1L,0x4DD37800L,0x374A570FL,(-1L),0x3E8BCC84L,0x3E8BCC84L,(-1L),0x374A570FL,0x4DD37800L},{0x3E8BCC84L,(-1L),(-8L),0x972C6B67L,0x0BE0C58BL,0x4DD37800L,(-1L),(-1L),0x4DD37800L},{0x972C6B67L,0x374A570FL,2L,0x374A570FL,0x972C6B67L,(-1L),0x8AAF70A8L,(-8L),(-1L)}};
        uint64_t l_1930 = 0xF67724BEEADC5B1FLL;
        int i, j;
        (*l_1415) &= (l_1411 <= ((*l_1412)--));
        (*l_1427) = (safe_mul_func_float_f_f(0x0.Dp+1, ((l_1418 != (void*)0) >= ((((p_49 , (-0x2.8p-1)) != p_47) < (l_1421 = (safe_mul_func_float_f_f(p_46, 0xD.5F0CD9p+30)))) == (((***g_1327) , l_1422) == l_1426)))));
        (*g_1443) |= (((l_1428 != (void*)0) >= (l_1411 == ((safe_lshift_func_uint16_t_u_s((safe_mod_func_uint32_t_u_u((((&g_928 == l_1433[0][1][0]) && (((*g_69) , (((l_1437 |= (safe_mul_func_uint8_t_u_u(1UL, l_1411))) <= ((safe_mul_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(p_49, 4L)), 0xA7L)) , p_49)) || 0xAEFEL)) == l_1442[2][1])) != g_127), p_49)), p_46)) != p_46))) ^ (*l_1415));
    }
    return p_45;
}


/* ------------------------------------------ */
/* 
 * reads : g_68 g_4 g_99 g_72 g_102 g_103 g_101 g_127 g_79 g_200 g_208 g_69 g_160 g_297 g_348 g_226 g_371 g_372 g_373 g_395 g_224 g_448 g_317 g_318 g_493 g_531 g_532 g_533 g_252 g_592 g_609 g_639 g_593 g_634 g_696 g_530 g_635 g_640 g_752 g_753 g_754 g_795 g_907 g_659 g_943 g_945 g_766 g_897 g_1109
 * writes: g_72 g_79 g_99 g_101 g_103 g_127 g_159 g_160 g_68 g_224 g_226 g_252 g_297 g_316 g_349 g_448 g_293 g_493 g_529 g_533 g_609 g_634 g_659 g_671 g_766 g_795 g_896 g_907 g_928 g_943 g_696 g_1109 g_635
 */
static int32_t * func_51(uint32_t  p_52, float  p_53, int16_t  p_54, const uint32_t  p_55, int64_t  p_56)
{ /* block id: 3 */
    uint16_t l_70 = 0x5443L;
    int32_t *l_71 = &g_72;
    int32_t l_73 = (-5L);
    int32_t l_74[10];
    const int32_t **l_105 = (void*)0;
    const uint8_t l_106 = 247UL;
    uint32_t l_326[5] = {0x2A34898CL,0x2A34898CL,0x2A34898CL,0x2A34898CL,0x2A34898CL};
    int16_t **l_334 = &g_252[5][5][3];
    int16_t ***l_333[7][10] = {{&l_334,(void*)0,&l_334,&l_334,(void*)0,&l_334,&l_334,(void*)0,&l_334,(void*)0},{(void*)0,&l_334,&l_334,(void*)0,&l_334,(void*)0,(void*)0,&l_334,(void*)0,&l_334},{(void*)0,(void*)0,(void*)0,&l_334,&l_334,&l_334,&l_334,&l_334,(void*)0,(void*)0},{&l_334,&l_334,&l_334,(void*)0,(void*)0,&l_334,&l_334,&l_334,&l_334,&l_334},{&l_334,(void*)0,&l_334,&l_334,(void*)0,&l_334,(void*)0,(void*)0,&l_334,(void*)0},{(void*)0,&l_334,&l_334,(void*)0,&l_334,(void*)0,&l_334,&l_334,(void*)0,&l_334},{(void*)0,(void*)0,&l_334,&l_334,&l_334,&l_334,&l_334,&l_334,(void*)0,(void*)0}};
    uint8_t ***l_346 = (void*)0;
    int16_t l_381[1];
    uint8_t l_447 = 0UL;
    uint32_t l_452 = 0x6C5446F4L;
    int8_t *l_505 = &g_226;
    uint64_t *l_526 = &g_448;
    uint64_t **l_525 = &l_526;
    uint8_t l_559 = 0x0FL;
    float *l_611 = &g_293[2][4];
    float **l_610 = &l_611;
    uint32_t l_749 = 0x87D1CDECL;
    int8_t l_878 = 0xE5L;
    uint8_t *** const l_930 = (void*)0;
    uint8_t *** const *l_929 = &l_930;
    int32_t l_972 = 1L;
    int32_t *l_996 = &g_696;
    int16_t l_1011 = 0xC3BAL;
    int8_t l_1022 = (-1L);
    uint32_t l_1036 = 18446744073709551615UL;
    int32_t *l_1079 = &l_74[0];
    uint32_t ***l_1096 = (void*)0;
    uint32_t ****l_1095 = &l_1096;
    uint32_t l_1148 = 0UL;
    float l_1254 = 0x1.350FB0p-47;
    const float * const l_1275 = &g_293[0][0];
    const float * const *l_1274 = &l_1275;
    const float * const **l_1273 = &l_1274;
    const float * const ***l_1272 = &l_1273;
    const float * const ****l_1271 = &l_1272;
    int16_t l_1276 = 0x26A1L;
    float *l_1300[8][8] = {{(void*)0,&g_103,&l_1254,&g_159,&g_159,&l_1254,&g_103,(void*)0},{&g_159,&l_1254,&l_1254,&g_159,&g_159,&g_103,&g_159,&g_159},{&g_159,&g_290,&g_159,&g_290,&g_159,&g_103,&l_1254,&l_1254},{&l_1254,&l_1254,&g_159,&g_159,&l_1254,&l_1254,&g_159,&g_159},{&l_1254,&g_103,(void*)0,&l_1254,&g_159,&l_1254,(void*)0,&g_103},{&g_159,(void*)0,&g_103,&l_1254,&g_159,&g_290,&g_290,&g_159},{&g_159,&g_159,&g_159,&g_159,&g_159,&g_103,&g_290,&l_1254},{(void*)0,&g_159,&g_103,&g_290,&g_103,&g_159,(void*)0,&g_159}};
    float **l_1299 = &l_1300[6][6];
    uint32_t l_1364 = 0xA60E8234L;
    uint32_t l_1405 = 0x8484771EL;
    int i, j;
    for (i = 0; i < 10; i++)
        l_74[i] = 1L;
    for (i = 0; i < 1; i++)
        l_381[i] = 0x9075L;
lbl_376:
    g_68[2][5][5] = func_57(func_61(func_65(g_68[3][3][4], ((l_70 != ((l_70 < (l_74[0] |= (l_73 = ((*l_71) = 1L)))) == ((safe_lshift_func_int8_t_s_s(((safe_mul_func_int16_t_s_s(p_52, (g_4[0][0] && (g_79[2] = ((((&l_71 != ((g_4[0][0] != 0x88D5L) , &g_69)) || 0x7EL) , g_4[0][0]) <= g_4[0][0]))))) , p_56), p_52)) < g_4[0][0]))) , &l_74[0])), l_105, l_106), p_55, p_54);
lbl_501:
    for (g_127 = (-1); (g_127 == 46); g_127++)
    { /* block id: 48 */
        uint8_t l_221[10][10][2] = {{{0xB3L,0x5AL},{0x2FL,1UL},{0xE2L,0x2AL},{0xBCL,0xE2L},{255UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL},{255UL,0x2AL},{0x5AL,250UL}},{{250UL,0xA6L},{1UL,0xA6L},{250UL,250UL},{0x5AL,0x2AL},{255UL,0xBCL},{0xB3L,251UL},{0xA6L,0xB3L},{1UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL}},{{0xB3L,0xBCL},{255UL,0x2AL},{0x5AL,250UL},{250UL,0xA6L},{1UL,0xA6L},{250UL,250UL},{0x5AL,0x2AL},{255UL,0xBCL},{0xB3L,251UL},{0xA6L,0xB3L}},{{1UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL},{255UL,0x2AL},{0x5AL,250UL},{250UL,0xA6L},{1UL,0xA6L},{250UL,250UL},{0x5AL,0x2AL}},{{255UL,0xBCL},{0xB3L,251UL},{0xA6L,0xB3L},{1UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL},{255UL,0x2AL},{0x5AL,250UL},{250UL,0xA6L}},{{1UL,0xA6L},{250UL,250UL},{0x5AL,0x2AL},{255UL,0xBCL},{0xB3L,251UL},{0xA6L,0xB3L},{1UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL}},{{255UL,0x2AL},{0x5AL,250UL},{250UL,0xA6L},{1UL,0xA6L},{250UL,250UL},{0x5AL,0x2AL},{255UL,0xBCL},{0xB3L,251UL},{0xA6L,0xB3L},{1UL,0xE2L}},{{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL},{255UL,0x2AL},{0x5AL,250UL},{250UL,0xA6L},{1UL,0xA6L},{250UL,250UL},{0x5AL,0x2AL},{255UL,0xBCL}},{{0xB3L,251UL},{0xA6L,0xB3L},{1UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL},{255UL,0x2AL},{0x5AL,250UL},{250UL,0xA6L},{1UL,0xA6L}},{{250UL,250UL},{0x5AL,0x2AL},{255UL,0xBCL},{0xB3L,251UL},{0xA6L,0xB3L},{1UL,0xE2L},{1UL,0xB3L},{0xA6L,251UL},{0xB3L,0xBCL},{255UL,251UL}}};
        int32_t l_228 = 0xCF4D97D3L;
        float *l_257 = &g_103;
        int32_t l_261[9] = {0x77215B51L,(-5L),0x77215B51L,0x77215B51L,(-5L),0x77215B51L,0x77215B51L,(-5L),0x77215B51L};
        float * const l_292 = &g_293[2][4];
        float * const *l_291 = &l_292;
        uint8_t **l_315 = (void*)0;
        uint8_t ***l_314 = &l_315;
        const int32_t *l_329 = &g_4[0][0];
        int32_t *l_335[7][2][2] = {{{&l_228,(void*)0},{&l_74[0],&l_228}},{{&l_74[0],&l_73},{&l_73,&g_4[0][0]}},{{&l_261[0],&g_4[0][0]},{&l_73,&l_73}},{{&l_74[0],&l_228},{&l_74[0],(void*)0}},{{&l_228,&g_72},{&g_4[0][0],&l_228}},{{&l_74[0],&g_72},{&l_74[0],&l_228}},{{&g_4[0][0],&g_72},{&l_228,(void*)0}}};
        int64_t l_392 = 0xEA83097D7708A6B2LL;
        const int16_t *l_405 = &l_381[0];
        const int16_t **l_404 = &l_405;
        const int16_t l_444[4][9] = {{0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL},{0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL},{0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL},{0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL,0x9BDBL}};
        int32_t *l_496 = &l_74[0];
        int i, j, k;
        (*l_71) &= 0xCEF1D68DL;
        for (p_52 = 3; (p_52 != 12); p_52 = safe_add_func_int32_t_s_s(p_52, 1))
        { /* block id: 52 */
            uint32_t l_220 = 0x8C3209DCL;
            int16_t *l_222 = &g_160;
            uint32_t *l_223 = &g_224;
            int8_t *l_225 = &g_226;
            int32_t *l_227[8] = {&l_74[0],&l_74[0],(void*)0,&l_74[0],&l_74[0],(void*)0,&l_74[0],&l_74[0]};
            int i;
            l_73 |= (safe_div_func_uint8_t_u_u(((((void*)0 != &g_101[0][1]) || 0xA66CL) , (((((((*l_225) = (((*l_223) = (safe_mod_func_int16_t_s_s(((*l_222) = (((*l_71) ^ (safe_div_func_int64_t_s_s(((!l_220) != p_55), 0xCD9F9836C0E96C7BLL))) || l_221[1][1][0])), 1L))) > g_72)) , p_55) , p_54) | 0x5B517930L) == 0xC058660D6635F1EALL) > p_56)), (*l_71)));
            l_228 ^= 0x3F765756L;
            for (l_73 = (-5); (l_73 <= (-19)); l_73 = safe_sub_func_uint64_t_u_u(l_73, 2))
            { /* block id: 60 */
                int32_t *l_231 = &g_4[0][0];
                return l_231;
            }
        }
        l_228 = (safe_sub_func_int16_t_s_s((&l_221[7][5][1] != (void*)0), 0x692DL));
        for (g_226 = 14; (g_226 < 5); g_226 = safe_sub_func_int64_t_s_s(g_226, 5))
        { /* block id: 67 */
            const int32_t l_253 = (-1L);
            int32_t l_264 = 0x10983F19L;
            int32_t l_265[7] = {1L,1L,1L,1L,1L,1L,1L};
            uint64_t l_266[9][9][3] = {{{1UL,0x4B25155DE64BFC91LL,18446744073709551611UL},{0x7930CD3334BC5BCELL,0x2A7CD9CA31092662LL,0x2A7CD9CA31092662LL},{18446744073709551611UL,1UL,18446744073709551615UL},{0x538669AFFD46F371LL,0x7930CD3334BC5BCELL,18446744073709551615UL},{18446744073709551611UL,18446744073709551611UL,0x89FD16D84DE35C32LL},{0x7930CD3334BC5BCELL,0x538669AFFD46F371LL,0xD076DFE5F2ED8236LL},{1UL,18446744073709551611UL,1UL},{0x2A7CD9CA31092662LL,0x7930CD3334BC5BCELL,18446744073709551611UL},{0x4B25155DE64BFC91LL,1UL,1UL}},{{18446744073709551611UL,0x2A7CD9CA31092662LL,0xD076DFE5F2ED8236LL},{0x0FB16671C60A45B5LL,0x4B25155DE64BFC91LL,0x89FD16D84DE35C32LL},{18446744073709551611UL,18446744073709551611UL,18446744073709551615UL},{0x4B25155DE64BFC91LL,0x0FB16671C60A45B5LL,18446744073709551615UL},{0x2A7CD9CA31092662LL,18446744073709551611UL,0x2A7CD9CA31092662LL},{1UL,0x4B25155DE64BFC91LL,18446744073709551611UL},{0x7930CD3334BC5BCELL,0x2A7CD9CA31092662LL,0x2A7CD9CA31092662LL},{18446744073709551611UL,1UL,18446744073709551615UL},{0x538669AFFD46F371LL,0x7930CD3334BC5BCELL,18446744073709551615UL}},{{18446744073709551611UL,18446744073709551611UL,0x89FD16D84DE35C32LL},{0x7930CD3334BC5BCELL,0x538669AFFD46F371LL,0xD076DFE5F2ED8236LL},{1UL,18446744073709551611UL,1UL},{0x2A7CD9CA31092662LL,0x7930CD3334BC5BCELL,18446744073709551611UL},{0x4B25155DE64BFC91LL,1UL,1UL},{18446744073709551611UL,0x2A7CD9CA31092662LL,0xD076DFE5F2ED8236LL},{0x0FB16671C60A45B5LL,0x4B25155DE64BFC91LL,0x89FD16D84DE35C32LL},{18446744073709551611UL,18446744073709551611UL,18446744073709551615UL},{0x4B25155DE64BFC91LL,0x0FB16671C60A45B5LL,18446744073709551615UL}},{{0x2A7CD9CA31092662LL,18446744073709551611UL,0x2A7CD9CA31092662LL},{1UL,0x4B25155DE64BFC91LL,18446744073709551611UL},{0x7930CD3334BC5BCELL,0x2A7CD9CA31092662LL,0x2A7CD9CA31092662LL},{18446744073709551611UL,1UL,18446744073709551615UL},{0x538669AFFD46F371LL,0x7930CD3334BC5BCELL,18446744073709551615UL},{18446744073709551611UL,18446744073709551611UL,0x89FD16D84DE35C32LL},{0x7930CD3334BC5BCELL,0x538669AFFD46F371LL,0xD076DFE5F2ED8236LL},{0x0FB16671C60A45B5LL,0UL,0x0FB16671C60A45B5LL},{0x538669AFFD46F371LL,0x2A7CD9CA31092662LL,0xAAD51AD7872CC1CCLL}},{{1UL,0x0FB16671C60A45B5LL,0x0FB16671C60A45B5LL},{0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551611UL},{0xAAD51AD7872CC1CCLL,0xAAD51AD7872CC1CCLL,18446744073709551611UL},{1UL,18446744073709551615UL,0x89FD16D84DE35C32LL},{0x538669AFFD46F371LL,0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL},{0x0FB16671C60A45B5LL,1UL,0UL},{0x2A7CD9CA31092662LL,0x538669AFFD46F371LL,0x538669AFFD46F371LL},{0UL,0x0FB16671C60A45B5LL,0x89FD16D84DE35C32LL}},{{0xD076DFE5F2ED8236LL,0x2A7CD9CA31092662LL,18446744073709551611UL},{0UL,0UL,18446744073709551611UL},{0x2A7CD9CA31092662LL,0xD076DFE5F2ED8236LL,18446744073709551615UL},{0x0FB16671C60A45B5LL,0UL,0x0FB16671C60A45B5LL},{0x538669AFFD46F371LL,0x2A7CD9CA31092662LL,0xAAD51AD7872CC1CCLL},{1UL,0x0FB16671C60A45B5LL,0x0FB16671C60A45B5LL},{0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551611UL},{0xAAD51AD7872CC1CCLL,0xAAD51AD7872CC1CCLL,18446744073709551611UL}},{{1UL,18446744073709551615UL,0x89FD16D84DE35C32LL},{0x538669AFFD46F371LL,0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL},{0x0FB16671C60A45B5LL,1UL,0UL},{0x2A7CD9CA31092662LL,0x538669AFFD46F371LL,0x538669AFFD46F371LL},{0UL,0x0FB16671C60A45B5LL,0x89FD16D84DE35C32LL},{0xD076DFE5F2ED8236LL,0x2A7CD9CA31092662LL,18446744073709551611UL},{0UL,0UL,18446744073709551611UL},{0x2A7CD9CA31092662LL,0xD076DFE5F2ED8236LL,18446744073709551615UL},{0x0FB16671C60A45B5LL,0UL,0x0FB16671C60A45B5LL}},{{0x538669AFFD46F371LL,0x2A7CD9CA31092662LL,0xAAD51AD7872CC1CCLL},{1UL,0x0FB16671C60A45B5LL,0x0FB16671C60A45B5LL},{0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551611UL},{0xAAD51AD7872CC1CCLL,0xAAD51AD7872CC1CCLL,18446744073709551611UL},{1UL,18446744073709551615UL,0x89FD16D84DE35C32LL},{0x538669AFFD46F371LL,0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL},{0x0FB16671C60A45B5LL,1UL,0UL},{0x2A7CD9CA31092662LL,0x538669AFFD46F371LL,0x538669AFFD46F371LL}},{{0UL,0x0FB16671C60A45B5LL,0x89FD16D84DE35C32LL},{0xD076DFE5F2ED8236LL,0x2A7CD9CA31092662LL,18446744073709551611UL},{0UL,0UL,18446744073709551611UL},{0x2A7CD9CA31092662LL,0xD076DFE5F2ED8236LL,18446744073709551615UL},{0x0FB16671C60A45B5LL,0UL,0x0FB16671C60A45B5LL},{0x538669AFFD46F371LL,0x2A7CD9CA31092662LL,0xAAD51AD7872CC1CCLL},{1UL,0x0FB16671C60A45B5LL,0x0FB16671C60A45B5LL},{0xAAD51AD7872CC1CCLL,0x538669AFFD46F371LL,18446744073709551615UL},{18446744073709551615UL,1UL,18446744073709551611UL}}};
            const float *l_300[4];
            uint64_t l_323 = 0UL;
            uint8_t *l_325 = &g_101[0][1];
            volatile uint64_t **l_374 = &g_372;
            int32_t **l_375 = &l_335[2][0][1];
            int16_t **l_407[3][4][5] = {{{&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2]},{&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3]},{&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2]},{&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3]}},{{&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2]},{&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3]},{&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2]},{&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3]}},{{&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2]},{&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3]},{&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2],&g_252[0][3][0],&g_252[2][5][2]},{&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3],&g_252[2][5][3]}}};
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_300[i] = &g_290;
            for (l_228 = 0; (l_228 <= 3); l_228 += 1)
            { /* block id: 70 */
                uint8_t *l_237[7][4][4] = {{{(void*)0,(void*)0,&g_101[0][4],(void*)0},{&l_221[4][4][1],(void*)0,(void*)0,&g_101[0][1]},{(void*)0,&g_127,&g_101[0][1],&l_221[1][1][0]},{&l_221[0][0][1],(void*)0,(void*)0,&l_221[0][7][0]}},{{&l_221[0][0][1],&g_101[0][4],&g_101[0][1],(void*)0},{(void*)0,&l_221[0][7][0],(void*)0,&g_127},{&l_221[4][4][1],&g_101[0][4],&g_101[0][4],&g_101[0][1]},{(void*)0,&g_101[0][4],&g_127,&g_127}},{{&g_127,&g_127,&g_101[0][4],&g_127},{&l_221[1][1][0],(void*)0,&l_221[2][9][0],(void*)0},{&g_101[0][4],&g_101[0][4],(void*)0,&l_221[2][9][0]},{&g_101[0][4],&g_101[0][4],&g_101[0][3],(void*)0}},{{&g_101[0][4],(void*)0,&g_101[0][1],&g_127},{(void*)0,&g_127,&l_221[1][1][0],&g_127},{&g_101[0][3],&g_101[0][4],&l_221[3][0][1],&g_101[0][1]},{(void*)0,&g_101[0][4],(void*)0,&g_127}},{{&g_101[0][1],&l_221[0][7][0],(void*)0,(void*)0},{(void*)0,&g_101[0][4],(void*)0,&l_221[0][7][0]},{&g_101[0][4],(void*)0,(void*)0,&l_221[1][1][0]},{(void*)0,&g_127,(void*)0,&g_101[0][1]}},{{&g_101[0][1],(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_221[3][0][1],&l_221[0][0][1]},{&g_101[0][3],&l_221[1][1][0],&l_221[1][1][0],&g_101[0][3]},{(void*)0,(void*)0,&g_101[0][1],&l_221[1][1][0]}},{{&g_101[0][4],&l_221[3][0][1],&g_101[0][3],(void*)0},{&g_101[0][4],(void*)0,(void*)0,(void*)0},{&g_101[0][4],&l_221[3][0][1],&l_221[2][9][0],&l_221[1][1][0]},{&l_221[1][1][0],(void*)0,&g_101[0][4],&g_101[0][3]}}};
                uint8_t **l_236 = &l_237[4][0][2];
                int32_t l_260 = 0xF8A4DB21L;
                int32_t l_262[10][1];
                int32_t *l_299 = &l_260;
                uint64_t *l_341 = &l_323;
                uint32_t *l_344 = &l_326[3];
                uint64_t l_355 = 18446744073709551610UL;
                uint8_t ****l_362[8] = {&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316};
                int i, j, k;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_262[i][j] = (-5L);
                }
                if (p_56)
                { /* block id: 71 */
                    uint8_t ***l_238 = &l_236;
                    int16_t *l_245 = &g_160;
                    int16_t *l_247 = &g_160;
                    int16_t **l_246 = &l_247;
                    int32_t l_254 = 5L;
                    int32_t l_263[8] = {0xAA5A505EL,0xAA5A505EL,0L,0xAA5A505EL,0xAA5A505EL,0L,0xAA5A505EL,0xAA5A505EL};
                    int i;
                    (*l_238) = l_236;
                    if ((safe_sub_func_int32_t_s_s(((((((safe_add_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u(1L, (l_245 != ((*l_246) = &g_160)))), ((((safe_mul_func_int8_t_s_s(p_55, (249UL >= (((safe_mod_func_uint8_t_u_u((g_79[1] == 0UL), p_55)) != (((((g_252[2][5][2] = &g_160) == &g_160) , 18446744073709551615UL) | 2L) == 1UL)) != (-10L))))) | g_101[0][4]) < l_253) <= l_253))) != 0xD56D9DE8EE48BEC9LL) == 0xC7812134L) != l_254) || (*g_69)) != l_254), 0xA8E44E8DL)))
                    { /* block id: 75 */
                        uint8_t l_258[6][9][4] = {{{0x82L,0xC6L,255UL,0x98L},{0xDCL,0x75L,0x98L,1UL},{0x29L,1UL,0UL,0x78L},{3UL,247UL,0xEBL,247UL},{0x1BL,0xCFL,1UL,255UL},{247UL,255UL,0x82L,1UL},{255UL,0xDCL,0xFCL,0x7CL},{255UL,0xC6L,0x82L,0UL},{247UL,0x7CL,1UL,0xD2L}},{{0x1BL,0UL,0xEBL,1UL},{3UL,0x9BL,0UL,0xE4L},{0x29L,0x87L,0x98L,0UL},{0xDCL,0x06L,255UL,1UL},{4UL,248UL,0xD2L,0x98L},{247UL,0xD2L,0x78L,0x19L},{0x87L,255UL,0x2FL,0UL},{0xD2L,1UL,0x1BL,0x09L},{0xBDL,0x7DL,0UL,0x06L}},{{249UL,0x7CL,0xEBL,1UL},{1UL,0x75L,247UL,0xCFL},{0x78L,0x3BL,0xFDL,0x6DL},{247UL,249UL,0x10L,246UL},{0x18L,0x98L,0xD3L,0xD3L},{0x78L,0x78L,0xFCL,0x09L},{1UL,248UL,0UL,255UL},{0xB8L,0x10L,255UL,0UL},{0x87L,0x10L,247UL,255UL}},{{0x10L,248UL,0x0FL,0x09L},{0x75L,0x78L,1UL,0xD3L},{0x06L,0x98L,0xEBL,246UL},{0xD2L,249UL,0x29L,0x6DL},{0x09L,0x3BL,0xC6L,0xCFL},{0xB8L,0x75L,0x19L,1UL},{0x18L,0x7CL,0x75L,0x06L},{1UL,0x7DL,249UL,0x09L},{4UL,1UL,0x10L,0UL}},{{0x82L,255UL,255UL,0x19L},{0xFDL,0xD2L,0x29L,0x98L},{255UL,248UL,0x1BL,0x87L},{249UL,1UL,0x17L,0xFCL},{0x06L,0x7CL,4UL,255UL},{0x10L,0x06L,0xB8L,0x6DL},{0x78L,0xC2L,0x78L,0UL},{0x82L,0xBDL,0x19L,246UL},{0x0FL,255UL,0xFCL,0xBDL}},{{0xD2L,0x7DL,0xFCL,0x87L},{0x0FL,3UL,0x19L,0x6DL},{0x82L,0x10L,0x78L,1UL},{0x78L,1UL,0xB8L,0x98L},{0x10L,1UL,4UL,255UL},{0x06L,0xD2L,0x17L,0xD3L},{249UL,255UL,0x1BL,0x69L},{255UL,0x06L,0x29L,0xCFL},{0xFDL,4UL,255UL,255UL}}};
                        int i, j, k;
                        (*l_71) = (safe_mod_func_int32_t_s_s(((g_4[0][0] , ((void*)0 != l_257)) & p_56), l_258[4][4][0]));
                    }
                    else
                    { /* block id: 77 */
                        int32_t *l_259[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i, j, k;
                        l_266[3][1][1]++;
                        return &g_72;
                    }
                    for (g_160 = 0; (g_160 <= 3); g_160 += 1)
                    { /* block id: 83 */
                        int32_t *l_269[7] = {&l_262[8][0],&l_262[8][0],&l_262[8][0],&l_262[8][0],&l_262[8][0],&l_262[8][0],&l_262[8][0]};
                        int i;
                        return &g_4[0][0];
                    }
                }
                else
                { /* block id: 86 */
                    uint8_t ***l_285 = (void*)0;
                    float **l_294 = &l_257;
                    int32_t l_295[1][4][6] = {{{0xAE2BCC02L,0x0025CD26L,0x0025CD26L,0xAE2BCC02L,(-1L),0xB7509372L},{0x48755DABL,0xB7509372L,0L,1L,0xB7509372L,0xAE2BCC02L},{1L,0xCEE5EC71L,1L,0L,0xB7509372L,0x48755DABL},{0xF1D00878L,0xC0AF2784L,(-1L),1L,1L,(-1L)}}};
                    uint8_t * const l_324 = &g_101[0][1];
                    int i, j, k;
                    for (l_73 = 0; (l_73 <= 3); l_73 += 1)
                    { /* block id: 89 */
                        uint8_t * const * const *l_286 = (void*)0;
                        float * const l_289 = &g_290;
                        float * const *l_288 = &l_289;
                        float * const **l_287[5][9] = {{&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288},{&l_288,&l_288,&l_288,(void*)0,&l_288,&l_288,&l_288,&l_288,&l_288},{&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288},{&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288},{&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288,&l_288}};
                        int8_t *l_296 = &g_297;
                        int32_t l_298 = 0xBD839757L;
                        uint8_t ****l_319 = &l_285;
                        int i, j;
                        (**l_294) = ((g_99 , func_65(((safe_lshift_func_int16_t_s_u(((*l_71) < ((1L <= ((safe_mul_func_uint16_t_u_u(p_55, (safe_sub_func_int32_t_s_s((l_266[3][1][1] ^ ((~((safe_rshift_func_int8_t_s_u(((*l_296) = (g_4[0][0] && (safe_mul_func_int16_t_s_s((safe_add_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(((l_285 != l_286) ^ (((l_291 = &l_257) == l_294) == 0x954DL)), 253UL)), p_55)), l_295[0][3][1])))), 2)) , 65535UL)) < g_99)), l_298)))) < (-10L))) ^ p_56)), 5)) , (void*)0), l_299)) == l_300[0]);
                        (*l_299) &= (p_54 && (g_79[2] == ((!((safe_lshift_func_uint16_t_u_s(l_264, 2)) | (safe_add_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s(((((safe_mul_func_uint8_t_u_u((((safe_mul_func_int16_t_s_s(((((safe_add_func_uint64_t_u_u(((g_316 = l_314) == ((*l_319) = &g_317)), ((0L && g_160) ^ p_54))) < ((((((void*)0 != &p_54) && g_72) | 0x489BA7C49396E87CLL) == p_52) & 0x9FD082DB463EA9C3LL)) > l_265[5]) || 0xCF74D3B42406BDFELL), g_4[0][0])) & p_55) | p_54), g_297)) ^ g_160) , p_54) , p_52), p_56)) >= g_160), l_298)))) , l_298)));
                        (*l_299) = ((safe_lshift_func_int8_t_s_s((p_56 <= ((*l_324) = (((((+((&g_99 != &g_99) < (p_54 , (l_323 || (*l_299))))) || ((l_324 != ((*l_236) = ((((g_4[0][0] , func_65(&l_299, &g_72)) != &l_298) > p_56) , l_325))) == p_55)) < l_326[0]) ^ l_264) > (-1L)))), 2)) & 65533UL);
                    }
                }
                for (p_56 = 0; (p_56 <= 2); p_56 += 1)
                { /* block id: 103 */
                    int16_t **l_332[2];
                    int16_t ***l_331 = &l_332[1];
                    int16_t ****l_330[6][1][2] = {{{&l_331,&l_331}},{{&l_331,&l_331}},{{&l_331,&l_331}},{{&l_331,&l_331}},{{&l_331,&l_331}},{{&l_331,&l_331}}};
                    int32_t *l_336[5][8] = {{&g_4[0][0],&l_265[5],&l_74[0],&l_228,&l_228,&l_74[0],&l_74[0],&l_74[0]},{&l_262[3][0],&l_228,&l_74[2],&l_228,&l_262[3][0],(void*)0,&l_228,&l_265[5]},{&l_265[6],&l_262[3][0],&l_228,&l_265[4],&l_228,&l_265[6],&l_265[6],&l_228},{&g_4[0][0],&l_228,&l_228,&g_4[0][0],&l_265[5],&l_74[0],&l_228,&l_228},{&l_228,&l_74[0],&l_74[2],&l_265[5],&l_74[0],(void*)0,&l_74[0],&l_265[5]}};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_332[i] = &g_252[1][0][3];
                    l_333[6][9] = ((((safe_sub_func_int32_t_s_s(0xE0AB808DL, g_79[p_56])) , l_329) != &l_253) , (void*)0);
                    return &g_72;
                }
                (*l_299) = (*l_299);
                if (((*l_71) = (safe_sub_func_uint32_t_u_u(g_200[1][4][0], ((*l_344) = (safe_sub_func_uint64_t_u_u(18446744073709551615UL, (--(*l_341)))))))))
                { /* block id: 111 */
                    uint8_t ****l_345 = &l_314;
                    l_346 = ((*l_345) = &l_315);
                    for (g_72 = 0; (g_72 <= 2); g_72 += 1)
                    { /* block id: 116 */
                        const int32_t *l_347[3];
                        int32_t **l_350 = &l_335[2][0][1];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_347[i] = &l_265[5];
                        (*g_348) = l_347[0];
                        if (g_79[g_72])
                            break;
                        (*l_350) = &l_265[6];
                    }
                    (*l_71) ^= ((((safe_sub_func_uint16_t_u_u(l_355, ((safe_sub_func_int8_t_s_s((safe_sub_func_int8_t_s_s(p_54, (safe_mul_func_uint8_t_u_u(((((((&l_314 != l_362[7]) < (safe_rshift_func_uint8_t_u_u((l_265[5] ^= ((g_4[0][0] , p_55) ^ 0x37D0L)), 1))) | ((((safe_sub_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u(7L, 2)), ((safe_rshift_func_int16_t_s_u(p_54, p_54)) >= g_101[0][3]))) | p_55) || 1L) > p_55)) < l_266[8][3][2]) >= g_79[2]) != (-2L)), 0x1CL)))), g_226)) , 0x4629L))) == 0UL) <= p_54) ^ g_99);
                    l_374 = g_371;
                }
                else
                { /* block id: 124 */
                    if (p_52)
                        break;
                }
            }
            (*l_375) = &l_264;
            if (l_323)
                goto lbl_376;
            for (l_323 = 0; (l_323 != 27); l_323 = safe_add_func_int64_t_s_s(l_323, 9))
            { /* block id: 132 */
                int32_t l_382 = (-10L);
                int32_t l_383 = 0x9CD1F47DL;
                int32_t l_384[4][8] = {{0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL,0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL},{0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL,0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL},{0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL,0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL},{0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL,0x00371FDFL,1L,0x00371FDFL,0x56DDFBACL}};
                float l_451 = 0x9.BAD20Fp-45;
                uint32_t l_480 = 0x1501E2DEL;
                int i, j;
                for (p_56 = 0; (p_56 > 17); p_56 = safe_add_func_int64_t_s_s(p_56, 1))
                { /* block id: 135 */
                    uint8_t l_385 = 255UL;
                    int32_t l_399 = 0xD2C6600DL;
                    --l_385;
                    (*l_71) ^= (safe_lshift_func_uint16_t_u_u(l_382, 0));
                    if (p_54)
                        break;
                    (*l_71) &= (((((**g_371) , ((p_52 & (safe_mod_func_uint32_t_u_u(l_392, ((safe_sub_func_uint16_t_u_u(g_127, p_54)) , (-9L))))) ^ g_395)) | ((l_399 = (safe_mul_func_uint16_t_u_u((!g_200[3][0][5]), (((p_52 & 0xD0F8L) , g_297) , p_56)))) , 8UL)) ^ (-1L)) , 1L);
                }
                if ((p_54 == 0x55L))
                { /* block id: 142 */
                    uint32_t l_402 = 0x2E34F034L;
                    int16_t **l_406[7] = {&g_252[6][5][1],&g_252[1][0][3],&g_252[1][0][3],&g_252[6][5][1],&g_252[1][0][3],&g_252[1][0][3],&g_252[6][5][1]};
                    int8_t *l_408[7] = {&g_226,&g_297,&g_226,&g_226,&g_297,&g_226,&g_226};
                    int64_t l_411 = 0xF19D0FB312B20DABLL;
                    int32_t *l_455 = (void*)0;
                    int64_t l_465 = (-1L);
                    int i;
                    (*l_71) &= (safe_lshift_func_int16_t_s_u(p_54, (l_402 ^ 2L)));
                    if ((safe_unary_minus_func_int8_t_s((g_297 |= ((g_224 , ((p_55 , l_404) == (l_407[2][3][4] = l_406[5]))) || g_101[0][4])))))
                    { /* block id: 146 */
                        uint64_t *l_443 = (void*)0;
                        uint64_t **l_442 = &l_443;
                        int32_t l_445[7];
                        uint16_t *l_446[8][9][3];
                        uint32_t l_449 = 0x3CA4DEB1L;
                        uint32_t *l_450[9][5][5] = {{{&g_224,&g_224,&l_402,&l_449,&g_224},{&l_326[0],&l_449,&l_402,&l_449,&l_326[4]},{&l_449,&l_402,&l_326[0],&l_402,&l_402},{&g_224,&l_449,&l_402,&l_449,&l_449},{(void*)0,&g_224,&l_402,(void*)0,&l_449}},{{&g_224,&l_326[0],&l_326[3],&l_326[0],&l_326[0]},{&l_326[2],&l_326[0],&l_326[4],(void*)0,&l_326[4]},{&g_224,&l_402,&l_326[4],&l_326[0],(void*)0},{&l_449,(void*)0,&g_224,&l_402,&l_402},{(void*)0,(void*)0,&l_402,&l_402,&l_402}},{{&l_449,&g_224,&l_402,(void*)0,&g_224},{&l_326[0],&l_326[3],&g_224,&l_449,(void*)0},{(void*)0,&l_326[2],&l_326[4],(void*)0,&l_402},{&g_224,&l_402,&l_326[0],&l_402,&g_224},{&l_402,&l_326[2],&l_326[0],(void*)0,&l_449}},{{&l_402,&l_326[0],&l_326[0],&l_402,&l_326[0]},{&l_402,&l_326[1],&l_326[4],&l_326[1],&l_326[0]},{&l_326[4],&l_402,&l_449,&l_326[0],&g_224},{&g_224,&l_402,&l_449,&l_326[1],&l_449},{&l_326[0],(void*)0,&g_224,&l_402,(void*)0}},{{&g_224,(void*)0,&l_326[4],(void*)0,&l_326[3]},{&l_449,&g_224,&g_224,&l_402,&l_326[2]},{&l_326[0],&l_326[0],&g_224,(void*)0,&l_326[2]},{(void*)0,&l_326[0],&l_449,&l_449,&l_402},{&l_326[0],&l_402,&l_449,(void*)0,&l_402}},{{&l_449,&l_326[2],&l_402,&l_402,&l_402},{&l_326[1],&l_326[0],&l_449,&l_402,&l_402},{&l_326[0],&l_402,&l_449,&l_326[0],&l_326[2]},{&l_326[4],&l_449,&l_326[0],(void*)0,&l_326[2]},{&l_402,(void*)0,&l_449,&l_449,&l_326[3]}},{{&l_449,(void*)0,&l_326[0],&l_326[0],(void*)0},{&l_326[0],&l_402,&l_402,&g_224,&l_449},{&l_402,&g_224,&l_326[0],&l_326[3],&g_224},{&l_402,&l_326[0],&g_224,(void*)0,&l_326[0]},{&l_402,&l_326[2],&l_402,&l_402,&l_326[0]}},{{&l_326[0],(void*)0,&l_326[0],&l_402,&l_449},{&l_449,&g_224,&l_402,(void*)0,&g_224},{&l_402,&l_402,&l_402,&l_326[0],&l_402},{&l_326[4],&g_224,&l_326[4],&l_402,(void*)0},{&l_326[0],&l_449,&g_224,&l_326[4],&g_224}},{{&l_326[1],(void*)0,&l_326[4],&l_449,&l_402},{&l_449,(void*)0,&g_224,&l_326[1],&l_402},{&l_326[0],&l_449,&g_224,(void*)0,(void*)0},{(void*)0,&g_224,&l_326[0],&l_326[0],&l_326[4]},{&l_326[0],&l_402,&l_326[0],&l_449,&l_402}}};
                        int i, j, k;
                        for (i = 0; i < 7; i++)
                            l_445[i] = 0x62D18C72L;
                        for (i = 0; i < 8; i++)
                        {
                            for (j = 0; j < 9; j++)
                            {
                                for (k = 0; k < 3; k++)
                                    l_446[i][j][k] = &l_70;
                            }
                        }
                        (*l_71) |= (safe_sub_func_int64_t_s_s(((l_411 ^ ((((g_224 = (((((((g_448 |= (safe_sub_func_int8_t_s_s((p_56 || (((0x0BCCL && ((((!((+0UL) > ((safe_mul_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((l_384[2][0] = ((~(g_297 != ((safe_add_func_uint64_t_u_u(((safe_div_func_int16_t_s_s(p_56, (safe_sub_func_uint64_t_u_u((~(((safe_rshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(((safe_sub_func_uint32_t_u_u((((safe_div_func_float_f_f((p_54 <= l_411), ((safe_add_func_float_f_f((safe_div_func_float_f_f(((l_442 == (void*)0) , 0x1.4p-1), (-0x7.7p-1))), p_55)) < p_55))) , p_55) < (*l_329)), 0L)) != 0x01L), p_52)), l_411)), p_56)) & p_52) , l_444[0][1])), l_445[2])))) ^ 1UL), 0L)) ^ (-8L)))) , 0x4610L)), p_54)) < 0x8F09L), 0x9717L)) || p_55))) ^ 0L) != p_52) && p_54)) != l_447) <= 1L)), p_55))) >= l_449) , (*g_317)) == l_408[5]) , p_55) < 0x821BL) , 3UL)) >= 0x4778DA17L) < g_297) , 18446744073709551615UL)) || l_411), 0xD702494B7287B92FLL));
                        l_452--;
                        (*l_375) = (void*)0;
                    }
                    else
                    { /* block id: 153 */
                        return l_455;
                    }
                    l_265[5] &= (l_73 = ((((safe_lshift_func_int8_t_s_s(8L, 7)) && (p_56 , (((*l_71) = (!((~(0x8EL >= (5L < (safe_unary_minus_func_int8_t_s((safe_lshift_func_uint8_t_u_s((*l_71), 0))))))) , p_55))) && (0xD1DCL > l_383)))) & (safe_mul_func_int8_t_s_s(p_54, l_465))) && 7UL));
                }
                else
                { /* block id: 159 */
                    const int32_t l_490 = 1L;
                    int32_t l_491 = 1L;
                    uint32_t l_492 = 1UL;
                    int8_t *l_494 = &g_297;
                    if ((safe_rshift_func_uint16_t_u_s(g_448, (safe_rshift_func_uint16_t_u_u((((*l_494) &= (p_54 , ((0xDA7B7EE8495CEEA3LL >= (((((*g_317) == (void*)0) >= (safe_add_func_uint64_t_u_u((safe_div_func_int16_t_s_s((safe_mul_func_int16_t_s_s((p_54 = (safe_mod_func_uint8_t_u_u((((((l_480 = (safe_div_func_int16_t_s_s(9L, g_127))) | ((-(safe_div_func_float_f_f(p_53, (((safe_div_func_float_f_f((safe_div_func_float_f_f(((safe_sub_func_float_f_f(((((((*l_292) = (((*l_257) = 0xB.EB2116p+57) > p_54)) != 0xE.2184DBp+38) , p_56) > g_101[0][1]) < 0x0.0FB529p-22), l_490)) <= p_53), l_491)), p_54)) >= 0x0.6p+1) > g_226)))) , 1L)) && p_55) , 0xB9L) , l_492), g_493))), (*l_71))), p_56)), g_4[0][0]))) <= p_55) , (**g_371))) & 0x61261F334AA78AA9LL))) < g_160), g_127)))))
                    { /* block id: 165 */
                        if (p_56)
                            goto lbl_376;
                    }
                    else
                    { /* block id: 167 */
                        int32_t *l_495 = &l_382;
                        (*l_71) = p_55;
                        return &g_72;
                    }
                }
            }
        }
    }
    for (g_224 = 0; (g_224 != 3); g_224++)
    { /* block id: 177 */
        int8_t *l_504 = (void*)0;
        int32_t l_512 = 1L;
        int16_t *l_581[4] = {&l_381[0],&l_381[0],&l_381[0],&l_381[0]};
        float *l_596[3];
        float **l_595 = &l_596[0];
        float *** const l_594[7][7][5] = {{{&l_595,(void*)0,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{(void*)0,&l_595,&l_595,(void*)0,&l_595},{&l_595,&l_595,&l_595,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595}},{{&l_595,(void*)0,&l_595,&l_595,(void*)0},{&l_595,&l_595,(void*)0,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,(void*)0,&l_595,&l_595},{&l_595,&l_595,&l_595,(void*)0,&l_595},{(void*)0,&l_595,&l_595,&l_595,&l_595},{(void*)0,&l_595,&l_595,&l_595,&l_595}},{{&l_595,&l_595,&l_595,&l_595,(void*)0},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,(void*)0,(void*)0},{&l_595,&l_595,(void*)0,&l_595,(void*)0},{&l_595,&l_595,&l_595,&l_595,&l_595}},{{&l_595,&l_595,&l_595,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{(void*)0,&l_595,&l_595,&l_595,&l_595},{(void*)0,(void*)0,(void*)0,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595}},{{&l_595,&l_595,&l_595,(void*)0,&l_595},{&l_595,(void*)0,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,(void*)0},{&l_595,&l_595,(void*)0,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{(void*)0,&l_595,&l_595,(void*)0,&l_595}},{{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{(void*)0,&l_595,(void*)0,&l_595,&l_595},{&l_595,(void*)0,&l_595,&l_595,(void*)0},{&l_595,(void*)0,&l_595,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595}},{{&l_595,(void*)0,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{(void*)0,&l_595,&l_595,(void*)0,&l_595},{&l_595,&l_595,(void*)0,(void*)0,&l_595},{&l_595,&l_595,&l_595,&l_595,&l_595},{&l_595,&l_595,&l_595,&l_595,(void*)0},{&l_595,&l_595,&l_595,&l_595,&l_595}}};
        uint64_t **l_613 = &l_526;
        int16_t * const *l_642[7] = {&l_581[3],&l_581[3],&l_581[3],&l_581[3],&l_581[3],&l_581[3],&l_581[3]};
        int16_t * const **l_641 = &l_642[2];
        int32_t ***l_815[3];
        uint8_t l_908 = 255UL;
        uint16_t l_978 = 0x93F6L;
        int32_t l_1021 = (-8L);
        uint8_t l_1065 = 0UL;
        int16_t l_1080[5][1][5] = {{{0L,(-1L),(-5L),(-1L),0L}},{{0xEEAFL,2L,0x2998L,(-1L),0xF618L}},{{0x2998L,2L,0xEEAFL,0xEEAFL,2L}},{{(-5L),(-1L),0L,2L,0xF618L}},{{(-1L),0xEEAFL,0L,1L,0L}}};
        int64_t l_1081 = (-1L);
        int32_t l_1108 = 0xF7150013L;
        uint8_t l_1113 = 1UL;
        int32_t *l_1144 = &l_74[8];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_596[i] = &g_290;
        for (i = 0; i < 3; i++)
            l_815[i] = &g_208;
        if ((*g_69))
            break;
        for (g_72 = 0; (g_72 < (-13)); g_72 = safe_sub_func_int64_t_s_s(g_72, 9))
        { /* block id: 181 */
            uint64_t *l_507 = &g_448;
            uint64_t ** const l_506 = &l_507;
            int32_t *l_513 = (void*)0;
            float *l_517 = &g_293[1][2];
            float **l_516 = &l_517;
            int16_t l_560 = 6L;
            int32_t *l_569[3][2] = {{(void*)0,&g_4[0][0]},{&g_4[0][0],(void*)0},{&g_4[0][0],&g_4[0][0]}};
            uint8_t ****l_574 = &g_316;
            int32_t l_598 = 0x3533A8F9L;
            uint64_t l_663 = 1UL;
            int8_t *l_787 = &g_297;
            uint64_t l_798 = 0x7659ABDA6AB01BC5LL;
            uint32_t l_845 = 4UL;
            uint8_t **l_925 = &g_318;
            uint8_t **l_926 = (void*)0;
            uint8_t **l_927 = &g_318;
            uint8_t *** const l_924[5][1][2] = {{{&l_927,(void*)0}},{{&l_927,(void*)0}},{{&l_927,(void*)0}},{{&l_927,(void*)0}},{{&l_927,(void*)0}}};
            uint8_t *** const *l_923 = &l_924[3][0][1];
            uint8_t *** const **l_922[7][5] = {{(void*)0,&l_923,&l_923,&l_923,(void*)0},{&l_923,&l_923,(void*)0,&l_923,&l_923},{(void*)0,(void*)0,(void*)0,(void*)0,&l_923},{&l_923,(void*)0,&l_923,&l_923,&l_923},{&l_923,(void*)0,&l_923,&l_923,(void*)0},{&l_923,(void*)0,&l_923,&l_923,&l_923},{&l_923,&l_923,&l_923,(void*)0,&l_923}};
            uint32_t *l_935 = &l_749;
            int64_t *l_938 = &g_634;
            const int64_t l_981[10] = {0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL,0xE3FAF73F638F111ALL};
            int16_t **l_1035 = (void*)0;
            int32_t *l_1064 = &g_696;
            int i, j, k;
            if (p_55)
                goto lbl_501;
            if (((((l_512 ^= (((safe_lshift_func_int16_t_s_s((l_504 == l_505), p_55)) , (((p_52 && ((void*)0 == l_506)) == (safe_div_func_uint16_t_u_u(((g_448 != p_54) ^ (safe_rshift_func_uint8_t_u_u((0x6BL ^ 0xE0L), p_52))), p_52))) || g_373)) && 0x57E6AFCAE517FE08LL)) ^ p_52) && g_297) , 0x609AA719L))
            { /* block id: 184 */
                return l_513;
            }
            else
            { /* block id: 186 */
                uint32_t *l_522 = &l_326[0];
                int32_t l_524 = 0x4C6E1062L;
                int32_t ***l_549[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t ****l_550 = &l_549[1];
                uint8_t *l_561 = &g_127;
                const uint64_t *l_587 = &g_448;
                const uint64_t **l_586 = &l_587;
                uint32_t l_657[7];
                uint16_t l_660[2];
                int64_t l_688[3][10] = {{0x02A92FB390453676LL,(-1L),(-4L),(-1L),(-1L),(-9L),(-1L),(-1L),(-4L),(-1L)},{0x02A92FB390453676LL,(-5L),0xAAEA9C4ED2A309AALL,(-1L),(-5L),(-1L),(-1L),0xAAEA9C4ED2A309AALL,0xAAEA9C4ED2A309AALL,(-1L)},{(-1L),(-1L),0xAAEA9C4ED2A309AALL,0xAAEA9C4ED2A309AALL,(-1L),(-1L),(-5L),(-1L),0xAAEA9C4ED2A309AALL,(-1L)}};
                const float l_695 = 0x1.727CAFp-35;
                int i, j;
                for (i = 0; i < 7; i++)
                    l_657[i] = 18446744073709551610UL;
                for (i = 0; i < 2; i++)
                    l_660[i] = 1UL;
                for (g_493 = 0; (g_493 <= 3); g_493 += 1)
                { /* block id: 189 */
                    float **l_518 = &l_517;
                    int32_t l_519 = 0x43EE5101L;
                    for (l_447 = 0; (l_447 <= 2); l_447 += 1)
                    { /* block id: 192 */
                        int32_t *l_523[7] = {&l_74[8],&l_74[8],&l_74[8],&l_74[8],&l_74[8],&l_74[8],&l_74[8]};
                        int i;
                        if (p_56)
                            goto lbl_376;
                        l_519 = ((g_79[l_447] ^ (*l_71)) || (safe_lshift_func_uint16_t_u_s((l_516 == l_518), 10)));
                        l_74[0] ^= (l_524 = (safe_lshift_func_int8_t_s_s(((**g_371) < (g_160 < g_101[0][3])), ((void*)0 != l_522))));
                    }
                    (*g_531) = l_525;
                    for (g_226 = 3; (g_226 >= 0); g_226 -= 1)
                    { /* block id: 201 */
                        int32_t *l_534[9][6][4] = {{{&g_4[0][0],&l_524,&l_74[0],(void*)0},{&l_73,(void*)0,(void*)0,(void*)0},{&l_73,&l_73,&l_74[0],(void*)0},{&g_4[0][0],(void*)0,&l_73,&l_524},{(void*)0,&l_512,&l_73,&l_73},{&g_4[0][0],&l_512,&g_4[0][0],&l_524}},{{&l_512,(void*)0,(void*)0,(void*)0},{(void*)0,&l_73,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_524,(void*)0,&g_4[0][0]},{&l_512,(void*)0,&g_4[0][0],&l_74[0]},{&g_4[0][0],&l_74[0],&l_73,&l_74[0]}},{{(void*)0,(void*)0,&l_73,&g_4[0][0]},{&g_4[0][0],&l_524,&l_74[0],(void*)0},{&l_73,(void*)0,(void*)0,(void*)0},{&l_73,&l_73,&l_74[0],(void*)0},{&g_4[0][0],(void*)0,&l_73,&l_524},{(void*)0,&l_512,&l_73,&l_73}},{{&g_4[0][0],&l_512,&g_4[0][0],&l_524},{&l_512,(void*)0,(void*)0,(void*)0},{(void*)0,&l_73,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_524,(void*)0,&g_4[0][0]},{&l_512,(void*)0,&g_4[0][0],&l_74[0]}},{{&g_4[0][0],&l_74[0],&l_73,&l_74[0]},{(void*)0,(void*)0,&l_73,&g_4[0][0]},{&g_4[0][0],&l_524,&l_74[0],(void*)0},{&l_73,(void*)0,(void*)0,(void*)0},{&l_73,&l_73,&l_74[0],(void*)0},{&g_4[0][0],(void*)0,&l_73,&l_524}},{{(void*)0,&l_512,&l_73,&l_73},{&g_4[0][0],&l_512,&g_4[0][0],&l_524},{&l_512,(void*)0,(void*)0,(void*)0},{(void*)0,&l_73,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_524,(void*)0,&g_4[0][0]}},{{&l_512,(void*)0,&g_4[0][0],&l_74[0]},{&g_4[0][0],&l_74[0],&l_73,&l_74[0]},{(void*)0,(void*)0,&l_73,&g_4[0][0]},{&g_4[0][0],&l_524,&l_74[0],(void*)0},{&l_73,(void*)0,(void*)0,(void*)0},{&l_73,&l_73,&l_74[0],(void*)0}},{{&g_4[0][0],(void*)0,&l_73,&l_524},{(void*)0,&l_512,&l_73,&l_73},{&g_4[0][0],&l_512,&g_4[0][0],&l_524},{&l_512,(void*)0,(void*)0,&l_524},{&l_524,&l_74[0],(void*)0,(void*)0},{&l_73,(void*)0,(void*)0,&l_73}},{{&l_524,(void*)0,&l_73,(void*)0},{(void*)0,(void*)0,(void*)0,&l_512},{(void*)0,&l_512,&g_4[0][0],&l_512},{(void*)0,(void*)0,&l_74[0],(void*)0},{&g_4[0][0],(void*)0,&l_512,&l_73},{&l_74[0],(void*)0,(void*)0,(void*)0}}};
                        int i, j, k;
                        (*g_532) = l_517;
                        return l_513;
                    }
                }
                if (p_56)
                    continue;
                if ((((safe_add_func_int8_t_s_s(((p_56 >= p_52) > (safe_sub_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(((*l_561) |= ((safe_add_func_uint8_t_u_u((((safe_lshift_func_uint16_t_u_s((safe_mod_func_int8_t_s_s((((p_54 | (safe_lshift_func_int8_t_s_s((p_56 != (((*l_550) = l_549[4]) == (((safe_lshift_func_uint8_t_u_u(p_55, 6)) < (safe_div_func_int8_t_s_s(((*l_505) = (safe_lshift_func_uint8_t_u_s(((((safe_lshift_func_uint16_t_u_u(((void*)0 != &g_102), ((((l_524 |= ((l_559 || (*g_372)) || 6L)) != 0xCE0BC2E2L) != (*l_71)) ^ 0xE1D3733FL))) == g_493) < 1L) > g_101[0][4]), g_297))), 0x37L))) , &l_105))), l_512))) == g_4[0][0]) , (-9L)), p_55)), l_560)) , p_54) & 0xBEL), g_200[1][4][0])) , l_512)), g_101[0][1])), 0xBD47L))), (-1L))) < p_56) , p_56))
                { /* block id: 211 */
                    const float *l_565 = &g_293[2][4];
                    const float **l_564 = &l_565;
                    int32_t *l_568 = &l_74[5];
                    int16_t *l_582 = &l_560;
                    uint64_t **l_588 = &g_530;
                    int32_t *l_600 = &g_4[0][0];
                    int32_t l_619[8] = {0xE520D001L,0xBF02DB73L,0xBF02DB73L,0xE520D001L,0xBF02DB73L,0xBF02DB73L,0xE520D001L,0xBF02DB73L};
                    const int16_t l_625 = 0x3F03L;
                    int i;
                    for (l_73 = 0; (l_73 > 29); ++l_73)
                    { /* block id: 214 */
                        const float ***l_566 = &l_564;
                        int32_t *l_567 = &l_524;
                        (*l_566) = l_564;
                        l_569[2][1] = (l_568 = l_567);
                        l_71 = &l_512;
                        return (*g_532);
                    }
                    if ((safe_sub_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((((*l_561) = (*l_568)) , (((void*)0 != l_574) || (safe_lshift_func_int8_t_s_u((safe_sub_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(g_395, (((*l_522) = p_54) , ((l_581[3] = &l_560) == l_582)))), ((-1L) >= (g_252[3][5][2] != (void*)0)))), p_55)))), g_99)) < p_52), g_4[0][0])))
                    { /* block id: 224 */
                        uint32_t l_583 = 0xD4F5B720L;
                        int64_t *l_589 = (void*)0;
                        int64_t *l_590 = (void*)0;
                        int64_t *l_591[10] = {&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99};
                        int64_t l_597 = 1L;
                        int32_t l_599 = 1L;
                        int i;
                        l_599 &= ((*l_568) , ((l_598 = (((l_583 , (((p_55 < 0x2DC6DF94L) || ((safe_rshift_func_int16_t_s_u((((void*)0 == &g_529[1][0][5]) & (l_512 = (65527UL <= (((((((*l_568) ^ (((((p_56 = (l_586 == l_588)) , g_592) == l_594[1][3][2]) > 4294967290UL) , p_55)) && l_597) & p_54) , 0xD8C69418L) <= 0UL) == l_583)))), 7)) , l_583)) && p_56)) >= p_52) && g_101[0][4])) || 0x8590BAFABCA97DC4LL));
                        return l_600;
                    }
                    else
                    { /* block id: 230 */
                        float l_618 = 0x7.7CFFDCp-68;
                        uint16_t *l_636 = &l_70;
                        const int32_t l_643 = (-1L);
                        int32_t l_655[5] = {0x723473B5L,0x723473B5L,0x723473B5L,0x723473B5L,0x723473B5L};
                        uint64_t l_656 = 18446744073709551615UL;
                        int64_t *l_658 = &g_659;
                        int i;
                        l_619[1] &= ((safe_div_func_int16_t_s_s((p_54 = ((0xA82BL != p_52) && (4294967295UL > (((+(safe_unary_minus_func_uint16_t_u(((safe_div_func_int32_t_s_s(((safe_sub_func_int8_t_s_s((((g_609 = g_609) != l_610) , (((~0xD95A0C63L) , (((void*)0 == l_613) , ((*l_582) &= (safe_div_func_int64_t_s_s(((safe_mod_func_uint64_t_u_u((*l_568), p_54)) && g_226), p_56))))) > l_512)), 0x78L)) ^ g_448), 0xF32AFA55L)) != p_52)))) ^ p_55) > 0x4D6524E635E3819CLL)))), g_160)) & p_55);
                        l_73 = (((*l_505) = (((+((*l_658) = ((g_634 ^= (safe_div_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((l_74[9] = ((255UL >= l_625) > (safe_lshift_func_uint8_t_u_s(((((((g_99 = (safe_div_func_uint16_t_u_u(((safe_sub_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(((*l_636)++), ((g_639 != l_641) <= (((*l_610) = ((*l_595) = (((l_643 ^ (safe_mul_func_uint8_t_u_u((l_655[2] &= ((*l_561) = ((safe_sub_func_uint8_t_u_u(((((((safe_mod_func_uint32_t_u_u((p_55 != (-8L)), (safe_add_func_int16_t_s_s(p_56, ((safe_mul_func_int16_t_s_s(((+(((void*)0 == &p_56) >= 0xD4L)) != 4294967286UL), g_226)) != 0L))))) & p_56) < 6UL) >= l_643) >= p_52) | 0xFCL), p_54)) >= p_54))), 9L))) && l_512) , l_568))) == l_522)))), 0x07A68C89095398C1LL)) != 1UL), 0xAE82L))) , (*g_592)) != (void*)0) > l_656) < 0xE3DE31C7L) , 0x7FL), l_657[1])))), l_643)), l_656))) > 9L))) != 0xF2C4L) , g_200[3][0][5])) <= 0xCBL);
                        l_512 ^= ((-10L) | l_660[0]);
                        return (*g_532);
                    }
                }
                else
                { /* block id: 249 */
                    uint64_t l_731 = 0x4F58D46C5D3588A0LL;
                    int32_t l_733 = 0x6C856529L;
                    for (p_54 = 0; (p_54 != (-4)); p_54 = safe_sub_func_int64_t_s_s(p_54, 4))
                    { /* block id: 252 */
                        float * const *l_668 = &l_596[0];
                        float * const **l_667 = &l_668;
                        float * const ***l_666 = &l_667;
                        float * const ****l_670[2][4][1] = {{{&l_666},{&l_666},{&l_666},{&l_666}},{{&l_666},{&l_666},{&l_666},{&l_666}}};
                        int i, j, k;
                        l_663--;
                        g_671 = l_666;
                    }
                    for (p_52 = 0; (p_52 <= 2); p_52 += 1)
                    { /* block id: 258 */
                        int32_t l_697 = (-9L);
                        int64_t *l_698 = &g_634;
                        l_74[0] = (safe_sub_func_int16_t_s_s(((safe_add_func_uint16_t_u_u(((((((safe_mod_func_uint32_t_u_u(((safe_mod_func_int8_t_s_s((safe_add_func_int64_t_s_s((safe_add_func_uint32_t_u_u((safe_unary_minus_func_int64_t_s((safe_unary_minus_func_int8_t_s(l_688[0][5])))), (**g_532))), ((*l_698) = ((((p_52 > (*g_530)) || (g_160 && ((((*l_561) ^= 0x58L) < (safe_mul_func_uint16_t_u_u(p_54, (safe_div_func_uint32_t_u_u(((p_56 & ((safe_sub_func_uint16_t_u_u((0xDFL < g_101[0][4]), p_54)) , p_55)) | 0xCE86L), (-3L)))))) < g_696))) > g_4[0][0]) & l_697)))), p_56)) , 0x73BBC48BL), 1UL)) > 0x078DL) == 0L) , 7L) || p_54) >= p_54), 0xE854L)) | 0UL), g_635));
                        if (p_56)
                            continue;
                        return (*g_532);
                    }
                    for (g_448 = (-30); (g_448 < 1); ++g_448)
                    { /* block id: 267 */
                        const uint64_t l_711 = 18446744073709551611UL;
                        uint32_t **l_724 = (void*)0;
                        uint32_t **l_725 = &l_522;
                        int32_t l_730 = 9L;
                        uint64_t *l_732 = &l_663;
                        l_733 ^= ((+(safe_sub_func_uint64_t_u_u((*g_530), (((void*)0 == (*g_639)) , (!(((safe_mul_func_int8_t_s_s(0xEAL, (safe_sub_func_uint64_t_u_u(((*l_732) = (((g_297 ^= (safe_lshift_func_int8_t_s_u(l_711, (safe_mod_func_int8_t_s_s(l_512, (safe_sub_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u((safe_div_func_int64_t_s_s(((safe_mul_func_int16_t_s_s((l_730 = (safe_rshift_func_uint16_t_u_u(((l_596[0] == ((*l_725) = &p_52)) & (safe_mul_func_uint8_t_u_u((safe_add_func_int32_t_s_s(0x51A61430L, p_55)), p_54))), 10))), 65530UL)) > (-1L)), l_731)), l_731)), p_56))))))) != l_731) >= 4294967293UL)), l_512)))) != g_160) & g_4[0][0])))))) && 1UL);
                        return (*g_532);
                    }
                }
            }
            for (l_598 = 0; (l_598 == 19); l_598 = safe_add_func_uint64_t_u_u(l_598, 2))
            { /* block id: 279 */
                float *l_740[4];
                int32_t l_744 = 4L;
                int64_t *l_776 = (void*)0;
                int64_t *l_777 = &g_659;
                int32_t ***l_838 = &g_208;
                int32_t l_879 = (-1L);
                uint32_t **l_895 = (void*)0;
                uint8_t l_904 = 0x94L;
                uint16_t *l_905 = &l_70;
                uint16_t *l_906 = &g_907;
                int i;
                for (i = 0; i < 4; i++)
                    l_740[i] = &g_103;
                if ((safe_add_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_u((((*l_516) == ((*l_595) = (l_740[3] = (*l_610)))) >= (((*l_505) = ((!((((g_493 == (safe_mod_func_int64_t_s_s(g_79[2], 0x93FE12F74A39AA89LL))) , ((l_512 != p_52) == l_744)) , ((safe_lshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u((((p_52 && g_696) > p_55) & p_56), 0)), 7)) , g_200[0][4][4])) > l_749)) , 0x71L)) , p_56)), 5)) >= 0x35FE24CB67A6AE3FLL), g_200[1][4][0])))
                { /* block id: 283 */
                    for (l_452 = 0; (l_452 != 24); l_452 = safe_add_func_int64_t_s_s(l_452, 6))
                    { /* block id: 286 */
                        volatile uint16_t * volatile *l_755 = &g_753;
                        l_755 = g_752;
                    }
                }
                else
                { /* block id: 289 */
                    uint32_t *l_765[6][1] = {{&g_224},{(void*)0},{&g_224},{(void*)0},{&g_224},{(void*)0}};
                    int32_t l_773 = 0xA97D718CL;
                    int32_t l_774 = 0x5BAA7ECCL;
                    int i, j;
                    if (((((safe_mul_func_int16_t_s_s(7L, (l_773 = (+((0x71L > p_52) | (safe_lshift_func_uint8_t_u_u(((p_52 <= (l_512 == (p_54 = (safe_div_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((g_766 = 0x96BF7DB8L), (((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_s((safe_mul_func_int8_t_s_s(((void*)0 != &g_316), p_55)), p_54)), 14)) , p_52) < (-1L)))), p_54))))) , p_56), 0))))))) | l_774) && g_200[1][4][0]) <= p_55))
                    { /* block id: 293 */
                        uint8_t l_775 = 4UL;
                        if (l_775)
                            break;
                    }
                    else
                    { /* block id: 295 */
                        if (g_127)
                            goto lbl_376;
                    }
                }
                if ((((*l_777) = 0x574934A4371AE5C2LL) , p_54))
                { /* block id: 300 */
                    int64_t l_794 = 0x13F15580BC76099FLL;
                    int16_t l_808 = 0x1AF5L;
                    for (g_226 = 0; (g_226 <= 2); g_226 += 1)
                    { /* block id: 303 */
                        int i;
                        l_74[(g_226 + 7)] = (safe_div_func_uint16_t_u_u((!(l_74[(g_226 + 2)] , (safe_add_func_int16_t_s_s((safe_lshift_func_int8_t_s_u((0UL || ((safe_div_func_uint64_t_u_u((l_787 == &g_635), (l_512 && (safe_div_func_uint32_t_u_u((((safe_lshift_func_uint8_t_u_u(l_512, (((safe_mul_func_int8_t_s_s(p_56, g_79[0])) > 0x42FE1F3FL) & (-9L)))) > (-6L)) != l_794), 3UL))))) , g_635)), 2)), 0x9270L)))), (**g_752)));
                        --g_795[2];
                        l_74[(g_226 + 7)] = (((((((l_798 , (safe_mul_func_int8_t_s_s(l_512, (!((safe_mul_func_int16_t_s_s((safe_div_func_uint16_t_u_u((l_808 >= ((&g_529[3][1][4] != (void*)0) > (safe_mod_func_int64_t_s_s(1L, ((*l_777) = p_56))))), (safe_add_func_uint8_t_u_u(l_512, ((safe_rshift_func_uint16_t_u_s((9L >= g_72), 9)) > g_127))))), 0x6AF9L)) == p_56))))) != 1L) ^ p_55) > (*g_753)) , l_815[0]) != l_815[0]) > 0x6EL);
                        if (p_55)
                            continue;
                    }
                }
                else
                { /* block id: 310 */
                    uint16_t *l_822 = &l_70;
                    uint64_t **l_841 = &l_526;
                    int32_t l_842 = (-1L);
                    int32_t l_843[6][4] = {{0x08DF618FL,1L,0xB337E1A0L,0xB337E1A0L},{0xEB43BFF8L,0xEB43BFF8L,0x08DF618FL,0xB337E1A0L},{1L,1L,1L,0x08DF618FL},{1L,0x08DF618FL,0x08DF618FL,1L},{0xEB43BFF8L,0x08DF618FL,0xB337E1A0L,0x08DF618FL},{0x08DF618FL,1L,0xB337E1A0L,0xB337E1A0L}};
                    int32_t l_844[5][5][6] = {{{6L,0x2570167AL,6L,0xFB5B6C1CL,0xFB5B6C1CL,6L},{0x084CF366L,0x084CF366L,0x85C1AE11L,0L,0x9D3496D2L,0x2570167AL},{0x01D9AED0L,6L,0x3B076C60L,(-10L),0xB79F4FB2L,0x85C1AE11L},{0x0812977CL,0x01D9AED0L,0x3B076C60L,0x248C5D7CL,0x084CF366L,0x2570167AL},{(-1L),0x248C5D7CL,0x85C1AE11L,0x3B076C60L,0xE4AE285BL,6L}},{{0x3B076C60L,0xE4AE285BL,6L,(-8L),6L,0xE4AE285BL},{1L,(-10L),0x0A6A4F61L,0L,0x2570167AL,0x084CF366L},{0x2570167AL,0x0812977CL,0L,0x5A1CF5DFL,0x85C1AE11L,0xB79F4FB2L},{0xFB5B6C1CL,0x0812977CL,0L,0x8AADC4C9L,0x2570167AL,0x9D3496D2L},{(-8L),(-10L),6L,0x2570167AL,6L,0xFB5B6C1CL}},{{(-1L),0xE4AE285BL,0xB79F4FB2L,0xB79F4FB2L,0xE4AE285BL,(-1L)},{(-1L),0x248C5D7CL,(-8L),0x46148816L,0x084CF366L,0x01D9AED0L},{0x85C1AE11L,0x01D9AED0L,0x2570167AL,0x5569B796L,0xB79F4FB2L,1L},{0x85C1AE11L,6L,0x5569B796L,0x46148816L,0x9D3496D2L,(-10L)},{(-1L),0x084CF366L,(-1L),0xB79F4FB2L,0xFB5B6C1CL,0L}},{{(-1L),0x2570167AL,0xE4AE285BL,0x2570167AL,(-1L),0xC9CE1A2CL},{0L,0x01D9AED0L,(-8L),(-1L),0x85C1AE11L,0L},{0L,(-8L),6L,0x01D9AED0L,5L,0L},{0x9D3496D2L,0x0812977CL,(-8L),(-10L),6L,0x2570167AL},{5L,0xB79F4FB2L,0x248C5D7CL,0L,(-10L),(-10L)}},{{6L,0xFB5B6C1CL,0xFB5B6C1CL,6L,0x2570167AL,6L},{0xFB5B6C1CL,0x5A1CF5DFL,0x3B076C60L,0x5569B796L,0L,5L},{0xE4AE285BL,0x084CF366L,0x9D3496D2L,6L,0L,0x85C1AE11L},{0x85C1AE11L,0x5A1CF5DFL,0L,0x0812977CL,0x2570167AL,(-8L)},{0x46148816L,0xFB5B6C1CL,(-1L),0L,(-10L),0L}}};
                    uint64_t l_853 = 6UL;
                    int i, j, k;
                    if (((l_844[1][1][4] &= (l_843[4][0] &= ((safe_sub_func_int32_t_s_s((1UL | ((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(((*l_822)--), ((p_54 && (((+((safe_lshift_func_uint16_t_u_s((~(safe_rshift_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((0UL ^ (safe_mod_func_uint64_t_u_u((~((safe_rshift_func_uint8_t_u_s(((void*)0 == l_838), 7)) , (safe_add_func_int16_t_s_s(4L, (((p_55 && p_52) >= ((l_613 != l_841) && 0L)) > p_55))))), l_842))) >= p_54), (*l_71))), 2))), p_54)) ^ (*l_71))) | (*l_71)) | 0xDD083A75L)) != 0xEB28L))), 2)) <= 0UL)), 0x758CB12FL)) == g_200[1][4][0]))) || p_52))
                    { /* block id: 314 */
                        uint64_t l_850 = 0UL;
                        int32_t l_851 = (-3L);
                        int32_t l_852 = 1L;
                        ++l_845;
                        l_851 &= (l_850 = (safe_rshift_func_int16_t_s_s(p_56, 6)));
                        l_842 = 0xE738E01DL;
                        --l_853;
                    }
                    else
                    { /* block id: 320 */
                        return (*g_532);
                    }
                    for (l_512 = 0; (l_512 <= 11); l_512++)
                    { /* block id: 325 */
                        int64_t l_870[10][2] = {{0L,(-1L)},{(-1L),0L},{0x4B6F5595D0AD28A8LL,0x2F7B98FD6334AA9ELL},{0L,0x4B6F5595D0AD28A8LL},{0x2F7B98FD6334AA9ELL,0L},{0x2F7B98FD6334AA9ELL,0x4B6F5595D0AD28A8LL},{0L,0x2F7B98FD6334AA9ELL},{0x4B6F5595D0AD28A8LL,0L},{(-1L),(-1L)},{0L,(-1L)}};
                        int32_t l_873[4];
                        int32_t *l_880[7][6] = {{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]},{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]},{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]},{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]},{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]},{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]},{&l_843[4][0],&l_74[0],&l_74[0],&l_843[4][0],&l_74[0],&l_74[0]}};
                        int i, j;
                        for (i = 0; i < 4; i++)
                            l_873[i] = 4L;
                        l_873[3] = ((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mul_func_float_f_f(((*l_611) = p_55), (((((safe_mod_func_uint8_t_u_u((((safe_unary_minus_func_int64_t_s((safe_unary_minus_func_uint32_t_u((safe_add_func_int64_t_s_s(((l_870[7][1] = (p_54 = l_843[4][0])) | ((!0x998FL) , (!(p_56 ^ (l_873[3] != 0x83E06A55D8DC9EDCLL))))), (safe_mod_func_int32_t_s_s((l_744 = (safe_add_func_int8_t_s_s(((*g_69) != ((l_878 && (&g_317 == &g_317)) && p_55)), p_55))), p_52)))))))) , 0x3FF4A96AL) , l_873[2]), p_55)) , 1UL) >= g_696) >= (-6L)) , 0x0.Ep+1))) , p_54), l_879)), p_55)) , p_55);
                        l_880[1][2] = (*g_532);
                        (*l_611) = p_52;
                        l_569[2][1] = &l_873[2];
                    }
                }
                l_74[8] &= ((l_73 |= (*l_71)) || (((*l_906) |= (safe_lshift_func_uint8_t_u_u((0xEAL & ((p_56 |= (-5L)) & ((safe_div_func_int8_t_s_s((((*l_905) = (safe_div_func_int8_t_s_s(p_52, ((safe_mul_func_float_f_f((p_55 , 0xA.8390AEp+71), (p_52 >= (p_53 = (safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f(((g_896 = (void*)0) == (((((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((safe_rshift_func_uint8_t_u_s(l_904, 2)) | p_55) , (*g_753)), p_54)), 0L)) <= p_55) > (*l_71)) , p_55) , l_596[1])), 0x5.55DED1p+44)), p_53)), p_52)))))) , g_795[9])))) || p_55), g_635)) == 248UL))), 4))) & g_226));
                l_74[0] &= l_908;
            }
            if (((!(safe_div_func_uint64_t_u_u(p_54, (safe_sub_func_uint8_t_u_u((p_56 ^ ((safe_rshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s(g_99, (safe_sub_func_int32_t_s_s((l_74[8] = ((((((safe_mod_func_int64_t_s_s((((*l_938) = (((l_929 = (g_928 = l_574)) == (void*)0) ^ (safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_u(((p_55 > ((*g_753) < ((1UL && ((*l_935)--)) ^ 6UL))) == p_56), 1)), g_493)))) || 0x45DB00537ED95C96LL), (*g_530))) == p_52) , p_52) , &l_506) == &l_613) && g_127)), g_226)))), g_659)) , 0UL)), p_52))))) && p_54))
            { /* block id: 350 */
                return (*g_532);
            }
            else
            { /* block id: 352 */
                int8_t l_946 = 0x7EL;
                int32_t l_947 = 0x33829B7AL;
                int32_t l_950[8][5][1] = {{{7L},{0xCB552DA0L},{0x08722891L},{0x2ECB3EB4L},{0x2ECB3EB4L}},{{0x08722891L},{0xCB552DA0L},{7L},{0x843B9824L},{0x958D7747L}},{{0x843B9824L},{7L},{0xCB552DA0L},{0x08722891L},{0x2ECB3EB4L}},{{0x2ECB3EB4L},{0x08722891L},{0xCB552DA0L},{7L},{0x843B9824L}},{{0x958D7747L},{0x843B9824L},{7L},{0xCB552DA0L},{0x08722891L}},{{0x2ECB3EB4L},{0x2ECB3EB4L},{0x08722891L},{0xCB552DA0L},{7L}},{{0x843B9824L},{0x958D7747L},{0x843B9824L},{7L},{0xCB552DA0L}},{{0x08722891L},{0x2ECB3EB4L},{0x2ECB3EB4L},{0x08722891L},{0xCB552DA0L}}};
                int8_t l_973 = 0x5CL;
                int32_t l_975 = (-1L);
                int8_t l_977 = 0x1AL;
                uint64_t ***l_989 = &l_525;
                uint16_t l_1024[2][8][7] = {{{65526UL,0UL,0UL,65526UL,65531UL,0x4BB5L,0xEB48L},{65532UL,0x08ADL,65528UL,0xEAE2L,0xEAE2L,65528UL,0x08ADL},{65531UL,1UL,1UL,0x7DAEL,0UL,0xEB48L,0xEB48L},{5UL,65532UL,0xEAE2L,65532UL,5UL,1UL,0x9DE5L},{9UL,0x4BB5L,1UL,0x7DAEL,0xC38AL,0x7DAEL,1UL},{0UL,0UL,0x9DE5L,1UL,5UL,65532UL,0xEAE2L},{65531UL,65526UL,0UL,0UL,65526UL,65531UL,0x4BB5L},{65528UL,0x9DE5L,9UL,0x08ADL,5UL,5UL,0x08ADL}},{{0x7DAEL,0xC38AL,0x7DAEL,1UL,0x4BB5L,9UL,65531UL},{9UL,0x9DE5L,65528UL,65532UL,65528UL,0x9DE5L,9UL},{0UL,65526UL,65531UL,0x4BB5L,0xEB48L,9UL,0xEB48L},{0x9DE5L,0UL,0UL,0x9DE5L,1UL,5UL,65532UL},{0xC0D4L,1UL,65531UL,0x7DAEL,0x7DAEL,65531UL,1UL},{1UL,9UL,65528UL,0x7D39L,0UL,65532UL,65532UL},{9UL,0xC0D4L,0x7DAEL,0xC0D4L,9UL,65526UL,0xEB48L},{0xEAE2L,5UL,9UL,0x7D39L,0x08ADL,0x7D39L,9UL}}};
                uint64_t *l_1053 = &l_798;
                int i, j, k;
                for (p_52 = 11; (p_52 > 35); p_52++)
                { /* block id: 355 */
                    float l_948 = 0x0.2p+1;
                    int32_t l_951 = (-4L);
                    int32_t l_952 = 0xC9AF59DBL;
                    int32_t l_953 = 0xFBC263B7L;
                    int32_t l_956 = 0L;
                    int32_t l_958 = (-8L);
                    int32_t l_961 = 0x8D24DD53L;
                    int32_t l_962 = 4L;
                    int32_t l_963 = 9L;
                    int32_t l_964 = 0x8F38D208L;
                    int32_t l_966 = 0xB934325DL;
                    int32_t l_969 = (-2L);
                    int32_t l_971[9] = {0x4D72D0DBL,0x4D72D0DBL,0L,0x4D72D0DBL,0x4D72D0DBL,0L,0x4D72D0DBL,0x4D72D0DBL,0L};
                    int8_t l_974[4][5][4] = {{{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L}},{{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L}},{{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L}},{{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L},{0x83L,0x83L,0xB6L,0xB6L}}};
                    uint8_t ****l_1020 = &l_346;
                    int16_t l_1023 = 0xAAA9L;
                    int i, j, k;
                    for (l_749 = 0; (l_749 == 48); l_749 = safe_add_func_uint16_t_u_u(l_749, 1))
                    { /* block id: 358 */
                        int16_t l_949 = 0xF870L;
                        int32_t l_954 = 0xA3DF5EF9L;
                        int32_t l_955 = 0L;
                        int32_t l_957 = 0x08D508AAL;
                        int32_t l_959 = 1L;
                        int32_t l_960 = 1L;
                        int32_t l_965 = 0L;
                        int32_t l_967 = 0xBEDCF974L;
                        int32_t l_968[4][1];
                        int32_t l_970 = 0xE8368AA9L;
                        int64_t l_976 = (-7L);
                        uint64_t ****l_986 = (void*)0;
                        uint64_t ***l_988 = &l_613;
                        uint64_t ****l_987[6][6] = {{&l_988,&l_988,&l_988,&l_988,&l_988,&l_988},{&l_988,&l_988,&l_988,&l_988,&l_988,&l_988},{&l_988,&l_988,&l_988,&l_988,&l_988,&l_988},{&l_988,&l_988,&l_988,&l_988,&l_988,&l_988},{&l_988,&l_988,(void*)0,(void*)0,&l_988,(void*)0},{(void*)0,&l_988,(void*)0,(void*)0,&l_988,(void*)0}};
                        int i, j;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_968[i][j] = 0x8D21B1EDL;
                        }
                        (*g_945) = g_943[0][3][4];
                        l_978++;
                        l_951 = l_981[8];
                        l_950[4][4][0] &= ((safe_lshift_func_int16_t_s_s(((~(g_395 , ((*l_938) = g_297))) | ((*l_996) = (g_200[1][3][5] , (!((l_989 = &g_529[3][1][4]) != ((safe_lshift_func_uint8_t_u_s(((--(*g_530)) | (((l_952 != (((void*)0 == l_996) < ((safe_mul_func_int8_t_s_s(((*l_505) ^= ((p_55 < 4294967295UL) < l_949)), l_958)) , 0x37F20122L))) , p_52) < (-1L))), g_635)) , &g_371)))))), p_52)) != (-9L));
                    }
                    if (((((safe_lshift_func_uint16_t_u_u(((safe_mod_func_int16_t_s_s((safe_add_func_uint8_t_u_u((l_517 != ((safe_rshift_func_int16_t_s_s((((safe_lshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(l_1011, ((p_55 <= (safe_mul_func_uint8_t_u_u(250UL, (safe_mul_func_uint16_t_u_u((((*g_102) , &l_610) != &g_673), (safe_div_func_int16_t_s_s(((*l_996) = p_52), (safe_mul_func_int16_t_s_s((l_1020 == l_1020), 65535UL))))))))) != (*g_69)))), 1)) , 0xD7L) != 4UL), p_52)) , (void*)0)), l_950[3][3][0])), l_1021)) != (-1L)), p_52)) , p_52) <= l_1022) ^ 0UL))
                    { /* block id: 370 */
                        int32_t *l_1034 = &l_1021;
                        if (p_56)
                            break;
                        l_1024[1][4][1]++;
                        l_951 = p_56;
                        (**l_516) = (((*l_1034) = ((g_634 = (!0xEB6EBE649380CECDLL)) > (((void*)0 != &l_642[2]) <= (safe_div_func_int32_t_s_s(l_961, ((0UL || (safe_rshift_func_int16_t_s_u((p_54 &= l_1024[1][3][4]), 14))) && (safe_mul_func_int16_t_s_s(l_1024[1][4][1], p_52)))))))) , ((0UL >= 0xCA13B9B142B7C4C5LL) , p_53));
                    }
                    else
                    { /* block id: 378 */
                        (*l_641) = l_1035;
                    }
                    ++l_1036;
                    l_950[0][0][0] = (safe_mod_func_int64_t_s_s((-5L), p_52));
                }
                (*g_348) = ((safe_sub_func_int32_t_s_s((((*g_753) <= ((safe_rshift_func_int16_t_s_s(((safe_add_func_int64_t_s_s(((safe_mul_func_int8_t_s_s(g_766, 255UL)) & ((((void*)0 == &g_944) == ((safe_div_func_int64_t_s_s(((l_947 >= g_659) <= ((*l_1053) = ((**l_613)++))), p_55)) & (safe_rshift_func_uint16_t_u_s(((((safe_add_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_s((((*l_935) = ((((l_950[4][4][0] = (p_55 | 0x33L)) & 0x01L) ^ p_56) , p_52)) || 0x4CCB4E91L), g_897)), 11)) , 0x57D8L), 0x56B3L)) <= 0x5179728DL), 1L)) < l_947) || (*l_71)) & p_52), p_56)))) , l_946)), (-10L))) & p_54), p_55)) != 0xEE8E907BL)) && l_975), 0L)) , l_1064);
                --l_1065;
            }
        }
        for (l_1065 = 15; (l_1065 > 13); l_1065--)
        { /* block id: 394 */
            int64_t l_1076 = 1L;
            uint64_t * const ***l_1084[2];
            uint64_t * const ****l_1085 = &l_1084[1];
            uint8_t l_1088[7][6] = {{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L},{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L},{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L},{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L},{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L},{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L},{0xD8L,0x09L,0xD8L,0xD8L,0x09L,0xD8L}};
            uint32_t *****l_1112 = &g_1109;
            uint16_t *l_1114 = &l_70;
            int32_t *l_1115 = &l_73;
            int32_t l_1122 = 0xB68716D2L;
            int32_t l_1125 = (-5L);
            int32_t l_1126 = (-1L);
            int32_t l_1127 = (-1L);
            int32_t l_1128 = (-1L);
            int32_t l_1129 = (-3L);
            int32_t l_1130 = 1L;
            int32_t l_1131 = 0x12AF5CC0L;
            int32_t l_1132 = 0xC5ECF50CL;
            int32_t l_1133 = 0xDED64D28L;
            int32_t l_1134 = 0xCF46C77CL;
            int32_t l_1135 = (-2L);
            int32_t l_1136[8][1] = {{0x3AC6F153L},{0x7945669FL},{0x3AC6F153L},{0x7945669FL},{0x3AC6F153L},{0x7945669FL},{0x3AC6F153L},{0x7945669FL}};
            uint32_t l_1137 = 8UL;
            uint32_t l_1141 = 0xE5D602BEL;
            int i, j;
            for (i = 0; i < 2; i++)
                l_1084[i] = (void*)0;
            (**l_610) = (safe_add_func_float_f_f(((p_54 != 0x1.0p-1) < ((((safe_div_func_float_f_f((p_55 > (l_815[0] == (void*)0)), 0x6.1p+1)) <= (safe_sub_func_float_f_f(l_1076, (((safe_mul_func_float_f_f((((*g_532) = func_65(&g_533, l_1079)) == (void*)0), l_1080[3][0][3])) <= p_52) < l_1076)))) <= l_1081) > 0x3.5CBB7Ap+56)), (-0x9.Dp-1)));
            (*l_1079) = (safe_lshift_func_uint16_t_u_u(((((*l_1085) = l_1084[1]) == (void*)0) , ((*l_1114) = (((safe_lshift_func_int16_t_s_s(((l_1088[5][4] < ((((safe_mul_func_int8_t_s_s((safe_mod_func_int32_t_s_s((safe_add_func_int16_t_s_s(((((void*)0 != l_1095) <= (safe_rshift_func_int16_t_s_u((safe_add_func_int64_t_s_s(((safe_rshift_func_uint8_t_u_s((safe_add_func_int16_t_s_s(p_54, (p_54 , (safe_unary_minus_func_int64_t_s((safe_div_func_int64_t_s_s((((((l_1108 , ((*l_1112) = g_1109)) == (void*)0) , p_52) <= 4294967293UL) & p_56), p_56))))))), l_1088[5][4])) > l_1076), (*l_1079))), 1))) || l_1113), (**g_752))), l_1088[5][4])), 0xFCL)) < (*l_996)) , p_55) || g_297)) && (-3L)), 13)) > l_1088[1][2]) , p_52))), g_4[0][0]));
            (*g_348) = l_1115;
            for (g_160 = (-10); (g_160 < 6); g_160 = safe_add_func_uint16_t_u_u(g_160, 9))
            { /* block id: 404 */
                int32_t *l_1118 = &g_696;
                int32_t l_1119 = (-1L);
                int32_t l_1120 = 0x8456CE3CL;
                int32_t l_1121 = 0x279A4DEBL;
                int32_t l_1123 = (-1L);
                int32_t l_1124[9][6] = {{0x2884733CL,0xEFF14884L,0x2AF3D486L,(-10L),(-10L),0x2AF3D486L},{0x2884733CL,0x2884733CL,(-10L),0x6731B71CL,0x388F2D4FL,0x6731B71CL},{0xEFF14884L,0x2884733CL,0xEFF14884L,0x2AF3D486L,(-10L),(-10L)},{0xDC392C4BL,0xEFF14884L,0xEFF14884L,0xDC392C4BL,0x2884733CL,0x6731B71CL},{0x6731B71CL,0xDC392C4BL,(-10L),0xDC392C4BL,0x6731B71CL,0x2AF3D486L},{0xDC392C4BL,0x6731B71CL,0x2AF3D486L,0x2AF3D486L,0x6731B71CL,0xDC392C4BL},{0xEFF14884L,0xDC392C4BL,0x2884733CL,0x6731B71CL,0x2884733CL,0xDC392C4BL},{0x2884733CL,0xEFF14884L,0x2AF3D486L,(-10L),(-10L),0x2AF3D486L},{0x2884733CL,0x2884733CL,(-10L),0x6731B71CL,0x388F2D4FL,0x6731B71CL}};
                int i, j;
                (*g_532) = l_1118;
                --l_1137;
                ++l_1141;
            }
        }
        l_1144 = &l_74[0];
    }
    for (l_559 = 0; (l_559 <= 5); l_559 += 1)
    { /* block id: 414 */
        int32_t *l_1145 = &g_696;
        int32_t *l_1146 = &l_74[0];
        int32_t *l_1147[7][2][5] = {{{&g_696,&g_696,&g_72,&g_72,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}},{{&g_696,&g_72,&g_72,&g_696,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}},{{&g_696,&g_696,&g_72,&g_72,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}},{{&g_696,&g_72,&g_72,&g_696,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}},{{&g_696,&g_696,&g_72,&g_72,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}},{{&g_696,&g_72,&g_72,&g_696,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}},{{&g_696,&g_696,&g_72,&g_72,&g_696},{&l_73,(void*)0,&l_73,(void*)0,&l_73}}};
        float ** const *l_1173 = (void*)0;
        float ** const **l_1172[3];
        uint64_t ***l_1303 = &g_529[3][1][4];
        uint64_t **** const l_1302 = &l_1303;
        uint64_t **** const *l_1301 = &l_1302;
        uint8_t **l_1342 = &g_318;
        uint16_t l_1343 = 65534UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1172[i] = &l_1173;
        if (p_54)
            break;
        l_1148--;
        for (g_766 = 0; (g_766 <= 4); g_766 += 1)
        { /* block id: 419 */
            uint64_t l_1158 = 18446744073709551615UL;
            const uint32_t l_1205[2][8] = {{1UL,8UL,8UL,1UL,1UL,8UL,8UL,1UL},{1UL,8UL,8UL,1UL,1UL,8UL,8UL,1UL}};
            int32_t *l_1206 = &l_73;
            uint64_t ***l_1225 = &l_525;
            uint64_t ****l_1224 = &l_1225;
            uint64_t *****l_1223 = &l_1224;
            float ***l_1292[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            float ****l_1291 = &l_1292[0];
            float ***** const l_1290 = &l_1291;
            int32_t l_1361 = 0xC29DEDEEL;
            int i, j;
            for (l_1036 = 0; (l_1036 <= 5); l_1036 += 1)
            { /* block id: 422 */
                int16_t * const *l_1159 = &g_252[2][5][2];
                float ** const *l_1170 = &g_609;
                float ** const **l_1169 = &l_1170;
                int32_t *l_1183 = &l_74[0];
                for (l_73 = 0; (l_73 <= 4); l_73 += 1)
                { /* block id: 425 */
                    uint32_t l_1151 = 0x540716E3L;
                    --l_1151;
                }
                for (p_52 = 0; (p_52 <= 4); p_52 += 1)
                { /* block id: 430 */
                    float ***l_1180 = (void*)0;
                    float ****l_1179 = &l_1180;
                    int32_t l_1181 = 1L;
                    (*l_1145) = (*g_533);
                    for (g_448 = 1; (g_448 <= 5); g_448 += 1)
                    { /* block id: 434 */
                        int16_t l_1168 = 5L;
                        float ** const ***l_1171[8] = {&l_1169,&l_1169,&l_1169,&l_1169,&l_1169,&l_1169,&l_1169,&l_1169};
                        int16_t l_1182[1][4] = {{0x4CF5L,0x4CF5L,0x4CF5L,0x4CF5L}};
                        int i, j, k;
                        (*l_71) = ((safe_sub_func_uint64_t_u_u(l_1158, 0x4DAF1312E7E7859FLL)) <= ((void*)0 != l_1159));
                        (*l_1146) = (((safe_lshift_func_int16_t_s_u((((g_4[0][0] , 3L) | 0xF3DF5EA8248F4D86LL) <= ((safe_lshift_func_int8_t_s_s((0x8FL && (((((safe_div_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_s((((l_1172[2] = ((l_1168 == 0x8CL) , l_1169)) != ((safe_mod_func_uint8_t_u_u(((--l_326[p_52]) != p_54), (+(((((0x182FD7A575F2438FLL & p_55) | (-1L)) && p_56) | g_373) & p_56)))) , l_1179)) == p_56), 1)) , 18446744073709551612UL), l_1158)) > g_297) && l_1181) & 0x26B812D5L) == p_55)), 7)) <= (-1L))), l_1181)) != l_1182[0][3]) , 0x40E91D0FL);
                    }
                    if (l_1158)
                        continue;
                    for (g_635 = 5; (g_635 >= 0); g_635 -= 1)
                    { /* block id: 443 */
                        if (g_907)
                            goto lbl_501;
                        return (*g_532);
                    }
                }
                if (l_749)
                    goto lbl_376;
            }
            for (p_56 = 5; (p_56 >= 0); p_56 -= 1)
            { /* block id: 452 */
                int32_t *l_1211 = (void*)0;
                uint64_t *****l_1226 = &l_1224;
                uint64_t *** const *l_1228 = (void*)0;
                uint64_t *** const **l_1227 = &l_1228;
                int8_t *l_1233[5];
                uint8_t *l_1239 = (void*)0;
                uint8_t *l_1240 = &g_101[0][4];
                int16_t ***l_1248 = (void*)0;
                int16_t ****l_1249[9];
                int8_t l_1250[1];
                int32_t l_1251 = (-7L);
                float ***l_1255 = (void*)0;
                float * const ****l_1263[8][3] = {{&g_671,(void*)0,&g_671},{&g_671,&g_671,(void*)0},{&g_671,&g_671,&g_671},{&g_671,&g_671,&g_671},{&g_671,(void*)0,(void*)0},{&g_671,(void*)0,&g_671},{&g_671,&g_671,(void*)0},{&g_671,&g_671,&g_671}};
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_1233[i] = &l_1022;
                for (i = 0; i < 9; i++)
                    l_1249[i] = &l_1248;
                for (i = 0; i < 1; i++)
                    l_1250[i] = (-5L);
            }
        }
    }
    return (*g_532);
}


/* ------------------------------------------ */
/* 
 * reads : g_99 g_4 g_102 g_103 g_79 g_200 g_101 g_72 g_208
 * writes: g_160 g_103 g_72
 */
static int32_t ** func_57(const int32_t * p_58, int16_t  p_59, uint64_t  p_60)
{ /* block id: 36 */
    int32_t *l_165 = &g_4[0][0];
    int32_t **l_164 = &l_165;
    const int32_t *l_167 = &g_4[0][0];
    const int32_t **l_166 = &l_167;
    const int32_t ***l_168 = &l_166;
    const float *l_192 = &g_193;
    const float **l_191 = &l_192;
    float *l_194 = (void*)0;
    int16_t l_199 = 0x4B33L;
    uint8_t l_201 = 2UL;
    float *l_202[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t *l_203 = (void*)0;
    int32_t *l_204 = (void*)0;
    int32_t *l_205 = &g_72;
    int32_t l_206[7][1][9] = {{{0x27A39A48L,0x4D6E761FL,0x0B773852L,0x0B773852L,0x4D6E761FL,0x27A39A48L,1L,0x27A39A48L,0x4D6E761FL}},{{0x27A39A48L,(-1L),(-1L),0x27A39A48L,(-5L),0x4D6E761FL,(-5L),0x27A39A48L,(-1L)}},{{(-5L),(-5L),1L,0x4D6E761FL,0x500B87C0L,0x4D6E761FL,1L,(-5L),(-5L)}},{{(-1L),0x27A39A48L,(-5L),0x4D6E761FL,(-5L),0x27A39A48L,(-1L),(-1L),0x27A39A48L}},{{0x4D6E761FL,0x27A39A48L,1L,0x27A39A48L,0x4D6E761FL,0x0B773852L,0x0B773852L,0x4D6E761FL,0x27A39A48L}},{{(-1L),(-5L),(-1L),0x0B773852L,1L,1L,0x0B773852L,(-1L),(-5L)}},{{(-5L),(-1L),0x0B773852L,1L,1L,0x0B773852L,(-1L),(-5L),(-1L)}}};
    int32_t l_207[10] = {0x798639D1L,0x11E98D02L,0x11E98D02L,0x798639D1L,0L,0x798639D1L,0x11E98D02L,0x11E98D02L,0x798639D1L,0L};
    int i, j, k;
    l_207[8] ^= (((g_160 = (safe_lshift_func_uint8_t_u_u(g_99, 2))) < (p_59 = (safe_unary_minus_func_int8_t_s(((l_164 != ((*l_168) = l_166)) || ((safe_mul_func_uint8_t_u_u((*l_165), (~(safe_rshift_func_int16_t_s_u((+((safe_add_func_uint16_t_u_u((safe_sub_func_int32_t_s_s(((*l_205) &= (safe_div_func_int64_t_s_s((((safe_div_func_float_f_f((safe_mul_func_float_f_f((((*g_102) = (safe_div_func_float_f_f(((*g_102) == (safe_sub_func_float_f_f((safe_mul_func_float_f_f((((*l_191) = p_58) != l_194), (safe_sub_func_float_f_f(g_79[0], ((safe_mul_func_float_f_f((l_199 == g_200[1][4][0]), g_79[2])) < g_101[0][3]))))), 0x9.C12630p-92))), l_201))) >= (*l_165)), 0x0.4p-1)), p_59)) >= 0x7.F97FFCp-5) , p_60), g_101[0][4]))), 0x0A6548F5L)), 0xA423L)) , (*l_205))), (***l_168)))))) ^ g_4[0][0])))))) > l_206[0][0][5]);
    return g_208;
}


/* ------------------------------------------ */
/* 
 * reads : g_102 g_103 g_99 g_72 g_101 g_127 g_4 g_79
 * writes: g_72 g_127 g_99 g_101 g_103 g_159 g_160
 */
static int32_t * func_61(int32_t * p_62, const int32_t ** p_63, const uint64_t  p_64)
{ /* block id: 17 */
    int32_t *l_112 = &g_72;
    int32_t **l_111 = &l_112;
    const int32_t * const l_113 = (void*)0;
    const int32_t l_120[2] = {0x596A4226L,0x596A4226L};
    uint8_t *l_123 = (void*)0;
    uint8_t *l_126 = &g_127;
    int32_t l_128 = 0xCAD1337AL;
    float *l_156 = &g_103;
    float *l_157 = (void*)0;
    float *l_158 = &g_159;
    int i;
    (*l_112) = ((*g_102) <= (safe_mul_func_float_f_f(0x2.Ap+1, ((safe_add_func_float_f_f(0xA.BD3BCAp+36, ((l_111 = &p_62) == &p_62))) >= (0xD.85FC26p+57 >= 0x1.Cp-1)))));
    if ((l_128 = (g_99 , ((((*l_111) = (*l_111)) != l_113) && ((safe_mod_func_int8_t_s_s(((8L == ((*l_126) &= (((*l_112) = ((*l_112) , (safe_sub_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(l_120[0], ((safe_div_func_int8_t_s_s((l_123 != &g_101[0][4]), (safe_lshift_func_uint16_t_u_u((1L || g_101[0][4]), p_64)))) <= 0L))), 5UL)))) != 1L))) , l_128), g_4[0][0])) || g_72)))))
    { /* block id: 24 */
        int32_t *l_129 = &l_128;
        (*l_111) = l_129;
    }
    else
    { /* block id: 26 */
        int32_t l_152[1];
        int32_t l_153 = 0x5577DD43L;
        int i;
        for (i = 0; i < 1; i++)
            l_152[i] = (-1L);
        (*l_111) = func_65(&g_69, &g_72);
        l_153 &= (safe_sub_func_uint8_t_u_u(((*l_126) = (0xCFD0E8CDL < ((*l_112) = (~(safe_rshift_func_uint16_t_u_u((safe_unary_minus_func_int64_t_s((safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u((p_64 , 0xAF61L), 6)), (((safe_lshift_func_int8_t_s_u((((safe_rshift_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((((0L <= ((safe_mod_func_uint8_t_u_u((g_4[0][0] && (safe_add_func_int8_t_s_s(0x6CL, (0xF6FDL == (**l_111))))), (safe_lshift_func_int8_t_s_u(((((*l_112) & g_4[0][0]) > p_64) , l_152[0]), 6)))) && 0L)) < 0x1DFB358544ED18BBLL) <= g_101[0][3]), 11)), 0)) | g_4[0][0]) < 65535UL), g_79[1])) , l_152[0]) > l_152[0]))))), p_64)))))), 0x1AL));
    }
    g_160 = ((*l_158) = (0xD.0C04BEp+2 != (safe_sub_func_float_f_f(((*l_156) = 0x5.Fp+1), 0x1.3p+1))));
    return &g_4[0][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_72 g_99 g_4 g_102 g_696
 * writes: g_72 g_99 g_101 g_103
 */
static int32_t * func_65(int32_t * const * p_66, int32_t * p_67)
{ /* block id: 8 */
    int32_t l_89 = 1L;
    int32_t *l_104[7] = {&g_4[0][0],&g_4[0][0],&g_4[0][0],&g_4[0][0],&g_4[0][0],&g_4[0][0],&g_4[0][0]};
    int i;
    for (g_72 = 0; (g_72 <= 24); g_72 = safe_add_func_uint8_t_u_u(g_72, 5))
    { /* block id: 11 */
        int32_t l_86 = 0x10B1E149L;
        int32_t *l_90 = &g_72;
        int64_t *l_98 = &g_99;
        uint8_t *l_100 = &g_101[0][4];
        (*g_102) = (safe_add_func_float_f_f(((safe_mul_func_float_f_f(0x0.8p+1, (l_86 , (safe_sub_func_float_f_f(l_89, (p_67 != l_90)))))) <= ((safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f(((0x59678192L ^ (0x12L && ((*l_100) = (safe_unary_minus_func_int32_t_s(((&p_67 != (((*l_98) ^= (((&g_69 == &p_67) | (*p_67)) && 8UL)) , &p_67)) && g_4[0][0])))))) , (*l_90)), 0x5.22E61Fp+17)), l_89)), (*l_90))) > (*l_90))), (-0x1.Bp+1)));
    }
    return l_104[3];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_4[i][j], "g_4[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_72, "g_72", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_79[i], "g_79[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_99, "g_99", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_101[i][j], "g_101[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_103, sizeof(g_103), "g_103", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc_bytes (&g_159, sizeof(g_159), "g_159", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    transparent_crc_bytes (&g_193, sizeof(g_193), "g_193", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_200[i][j][k], "g_200[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_224, "g_224", print_hash_value);
    transparent_crc(g_226, "g_226", print_hash_value);
    transparent_crc_bytes (&g_290, sizeof(g_290), "g_290", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc_bytes(&g_293[i][j], sizeof(g_293[i][j]), "g_293[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_297, "g_297", print_hash_value);
    transparent_crc(g_373, "g_373", print_hash_value);
    transparent_crc(g_395, "g_395", print_hash_value);
    transparent_crc(g_448, "g_448", print_hash_value);
    transparent_crc(g_493, "g_493", print_hash_value);
    transparent_crc(g_634, "g_634", print_hash_value);
    transparent_crc(g_635, "g_635", print_hash_value);
    transparent_crc(g_659, "g_659", print_hash_value);
    transparent_crc(g_696, "g_696", print_hash_value);
    transparent_crc(g_754, "g_754", print_hash_value);
    transparent_crc(g_766, "g_766", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_795[i], "g_795[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_897, "g_897", print_hash_value);
    transparent_crc(g_907, "g_907", print_hash_value);
    transparent_crc_bytes (&g_1140, sizeof(g_1140), "g_1140", print_hash_value);
    transparent_crc(g_1298, "g_1298", print_hash_value);
    transparent_crc(g_1345, "g_1345", print_hash_value);
    transparent_crc(g_1378, "g_1378", print_hash_value);
    transparent_crc(g_1380, "g_1380", print_hash_value);
    transparent_crc_bytes (&g_1385, sizeof(g_1385), "g_1385", print_hash_value);
    transparent_crc(g_1403, "g_1403", print_hash_value);
    transparent_crc(g_1408, "g_1408", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1508[i][j], "g_1508[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1509, "g_1509", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1510[i][j], "g_1510[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1511[i][j], "g_1511[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1539, "g_1539", print_hash_value);
    transparent_crc(g_1610, "g_1610", print_hash_value);
    transparent_crc(g_1706, "g_1706", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1980[i], "g_1980[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1981[i][j], "g_1981[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2030, "g_2030", print_hash_value);
    transparent_crc(g_2129, "g_2129", print_hash_value);
    transparent_crc(g_2141, "g_2141", print_hash_value);
    transparent_crc(g_2142, "g_2142", print_hash_value);
    transparent_crc(g_2423, "g_2423", print_hash_value);
    transparent_crc(g_2663, "g_2663", print_hash_value);
    transparent_crc(g_2685, "g_2685", print_hash_value);
    transparent_crc(g_2686, "g_2686", print_hash_value);
    transparent_crc(g_2737, "g_2737", print_hash_value);
    transparent_crc(g_2810, "g_2810", print_hash_value);
    transparent_crc(g_2933, "g_2933", print_hash_value);
    transparent_crc_bytes (&g_2954, sizeof(g_2954), "g_2954", print_hash_value);
    transparent_crc(g_3188, "g_3188", print_hash_value);
    transparent_crc(g_3378, "g_3378", print_hash_value);
    transparent_crc(g_3504, "g_3504", print_hash_value);
    transparent_crc(g_3753, "g_3753", print_hash_value);
    transparent_crc(g_3806, "g_3806", print_hash_value);
    transparent_crc(g_3926, "g_3926", print_hash_value);
    transparent_crc(g_4048, "g_4048", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_4060[i][j], "g_4060[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_4177, sizeof(g_4177), "g_4177", print_hash_value);
    transparent_crc_bytes (&g_4231, sizeof(g_4231), "g_4231", print_hash_value);
    transparent_crc(g_4273, "g_4273", print_hash_value);
    transparent_crc(g_4437, "g_4437", print_hash_value);
    transparent_crc(g_4521, "g_4521", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_4559[i][j], "g_4559[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_4564, "g_4564", print_hash_value);
    transparent_crc(g_4631, "g_4631", print_hash_value);
    transparent_crc(g_4633, "g_4633", print_hash_value);
    transparent_crc(g_4717, "g_4717", print_hash_value);
    transparent_crc(g_4949, "g_4949", print_hash_value);
    transparent_crc(g_5419, "g_5419", print_hash_value);
    transparent_crc(g_5588, "g_5588", print_hash_value);
    transparent_crc(g_5794, "g_5794", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1591
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 451
   depth: 2, occurrence: 108
   depth: 3, occurrence: 12
   depth: 4, occurrence: 3
   depth: 5, occurrence: 6
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 8, occurrence: 2
   depth: 9, occurrence: 2
   depth: 10, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 2
   depth: 14, occurrence: 2
   depth: 15, occurrence: 3
   depth: 16, occurrence: 4
   depth: 17, occurrence: 6
   depth: 18, occurrence: 4
   depth: 19, occurrence: 3
   depth: 20, occurrence: 3
   depth: 21, occurrence: 5
   depth: 22, occurrence: 7
   depth: 23, occurrence: 4
   depth: 24, occurrence: 9
   depth: 25, occurrence: 3
   depth: 26, occurrence: 5
   depth: 27, occurrence: 3
   depth: 28, occurrence: 5
   depth: 29, occurrence: 2
   depth: 30, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 2
   depth: 33, occurrence: 4
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 2
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1
   depth: 43, occurrence: 1
   depth: 46, occurrence: 1
   depth: 47, occurrence: 1
   depth: 51, occurrence: 1
   depth: 52, occurrence: 2

XXX total number of pointers: 1067

XXX times a variable address is taken: 2613
XXX times a pointer is dereferenced on RHS: 703
breakdown:
   depth: 1, occurrence: 521
   depth: 2, occurrence: 141
   depth: 3, occurrence: 36
   depth: 4, occurrence: 5
XXX times a pointer is dereferenced on LHS: 719
breakdown:
   depth: 1, occurrence: 577
   depth: 2, occurrence: 93
   depth: 3, occurrence: 42
   depth: 4, occurrence: 6
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 135
XXX times a pointer is compared with address of another variable: 29
XXX times a pointer is compared with another pointer: 35
XXX times a pointer is qualified to be dereferenced: 22499

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 6581
   level: 2, occurrence: 1276
   level: 3, occurrence: 743
   level: 4, occurrence: 439
   level: 5, occurrence: 131
XXX number of pointers point to pointers: 659
XXX number of pointers point to scalars: 408
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30.5
XXX average alias set size: 1.48

XXX times a non-volatile is read: 4844
XXX times a non-volatile is write: 2276
XXX times a volatile is read: 302
XXX    times read thru a pointer: 178
XXX times a volatile is write: 168
XXX    times written thru a pointer: 123
XXX times a volatile is available for access: 3.29e+03
XXX percentage of non-volatile access: 93.8

XXX forward jumps: 3
XXX backward jumps: 31

XXX stmts: 462
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 43
   depth: 2, occurrence: 52
   depth: 3, occurrence: 70
   depth: 4, occurrence: 108
   depth: 5, occurrence: 159

XXX percentage a fresh-made variable is used: 15.2
XXX percentage an existing variable is used: 84.8
********************* end of statistics **********************/


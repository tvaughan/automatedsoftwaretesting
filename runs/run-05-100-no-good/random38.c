/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      4047789425
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   const unsigned f0 : 23;
   signed f1 : 18;
   unsigned f2 : 13;
   signed f3 : 4;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S1 {
   const int32_t  f0;
   signed f1 : 5;
   unsigned f2 : 10;
   signed f3 : 11;
   signed f4 : 1;
   signed f5 : 10;
   unsigned f6 : 30;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S2 {
   unsigned f0 : 2;
   volatile unsigned f1 : 5;
   signed f2 : 22;
   volatile signed f3 : 11;
   signed f4 : 17;
   unsigned f5 : 10;
   uint64_t  f6;
   const volatile signed f7 : 21;
   volatile signed f8 : 5;
   uint64_t  f9;
};
#pragma pack(pop)

struct S3 {
   volatile unsigned f0 : 7;
   const volatile unsigned f1 : 27;
};

/* --- GLOBAL VARIABLES --- */
static volatile struct S2 g_15 = {0,0,1762,44,-353,12,0x9105FA2EF703C576LL,988,2,0x6B719D87263B0A6DLL};/* VOLATILE GLOBAL g_15 */
static int32_t g_16 = (-1L);
static int32_t g_21 = 0L;
static int32_t g_33 = 0xFD208DD2L;
static int32_t g_39[1] = {0x9C7819AFL};
static struct S1 g_43 = {0xF5D9AEB8L,-2,11,30,-0,15,18529};
static int64_t g_47 = 0x59FB917DE484183ALL;
static int64_t g_50 = 7L;
static volatile float g_55[8] = {0xD.23A2E2p+23,0xD.23A2E2p+23,0xD.23A2E2p+23,0xD.23A2E2p+23,0xD.23A2E2p+23,0xD.23A2E2p+23,0xD.23A2E2p+23,0xD.23A2E2p+23};
static uint16_t g_56 = 1UL;
static int64_t **g_61 = (void*)0;
static int64_t *** volatile g_62 = &g_61;/* VOLATILE GLOBAL g_62 */
static const int32_t g_76[1] = {(-1L)};
static const int32_t *g_75[5] = {&g_76[0],&g_76[0],&g_76[0],&g_76[0],&g_76[0]};
static const int32_t g_78 = (-1L);
static int32_t *g_106 = &g_21;
static int32_t **g_105 = &g_106;
static int16_t g_112 = (-7L);
static int32_t g_116 = 0xA393D434L;
static uint32_t g_117 = 0xD653C032L;
static uint8_t g_133 = 1UL;
static uint64_t g_134 = 0x49BC54FA017C95ABLL;
static int16_t g_136 = 4L;
static float g_148 = 0x0.6665C6p+82;
static float * volatile g_147[6][8][5] = {{{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,(void*)0,&g_148,&g_148,&g_148},{(void*)0,&g_148,&g_148,(void*)0,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148}},{{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,(void*)0,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,(void*)0,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148}},{{&g_148,&g_148,(void*)0,&g_148,(void*)0},{&g_148,&g_148,&g_148,&g_148,(void*)0},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148}},{{(void*)0,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,(void*)0,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,(void*)0,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148}},{{&g_148,&g_148,(void*)0,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148}},{{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_148}}};
static float * volatile g_149 = &g_148;/* VOLATILE GLOBAL g_149 */
static uint32_t g_158 = 0x1CDD84ECL;
static const volatile struct S3 g_161 = {8,11434};/* VOLATILE GLOBAL g_161 */
static int8_t g_178 = 8L;
static struct S3 g_191 = {10,7635};/* VOLATILE GLOBAL g_191 */
static const int64_t *g_198[7][7] = {{(void*)0,&g_47,&g_50,&g_47,(void*)0,&g_47,&g_50},{&g_47,&g_47,&g_50,(void*)0,(void*)0,(void*)0,(void*)0},{&g_50,&g_47,&g_50,&g_47,&g_50,&g_47,&g_50},{&g_47,(void*)0,(void*)0,&g_47,(void*)0,&g_50,&g_50},{(void*)0,&g_47,&g_50,&g_47,(void*)0,&g_47,&g_50},{(void*)0,&g_47,(void*)0,(void*)0,&g_47,(void*)0,&g_50},{&g_50,&g_47,&g_50,&g_47,&g_50,&g_47,&g_50}};
static const int64_t * const *g_197 = &g_198[3][4];
static const int64_t * const ** volatile g_196 = &g_197;/* VOLATILE GLOBAL g_196 */
static volatile uint32_t g_214 = 4UL;/* VOLATILE GLOBAL g_214 */
static struct S0 g_227[8] = {{917,504,53,3},{917,504,53,3},{2646,-57,34,-3},{917,504,53,3},{917,504,53,3},{2646,-57,34,-3},{917,504,53,3},{917,504,53,3}};
static struct S2 g_236[3] = {{1,4,68,-36,310,30,0x6F4B5C2CF825CBD8LL,-1141,3,1UL},{1,4,68,-36,310,30,0x6F4B5C2CF825CBD8LL,-1141,3,1UL},{1,4,68,-36,310,30,0x6F4B5C2CF825CBD8LL,-1141,3,1UL}};
static struct S2 *g_239 = &g_236[0];
static struct S2 ** volatile g_238 = &g_239;/* VOLATILE GLOBAL g_238 */
static struct S2 ***g_285 = (void*)0;
static struct S3 g_323 = {5,6385};/* VOLATILE GLOBAL g_323 */
static uint32_t g_332 = 0x1F9A6713L;
static volatile uint16_t g_336 = 0x63CCL;/* VOLATILE GLOBAL g_336 */
static float g_400[3][5] = {{0x1.A35EF9p-99,0xE.9A6A0Fp+34,0x1.A35EF9p-99,0x1.A35EF9p-99,0xE.9A6A0Fp+34},{0x0.Ap-1,0x5.2B41F7p+82,0x5.2B41F7p+82,0x0.Ap-1,0x5.2B41F7p+82},{0xE.9A6A0Fp+34,0xE.9A6A0Fp+34,0x1.EC5A63p+53,0xE.9A6A0Fp+34,0xE.9A6A0Fp+34}};
static struct S0 *g_406[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile struct S2 g_407 = {1,1,842,-28,-348,0,18446744073709551608UL,569,1,0x001C37C15E10267ALL};/* VOLATILE GLOBAL g_407 */
static const struct S2 g_428 = {0,4,375,-20,-200,29,0x701F40AD70EA69CALL,-261,2,1UL};/* VOLATILE GLOBAL g_428 */
static uint16_t g_451 = 0xB623L;
static struct S3 g_454[1][5] = {{{3,1824},{3,1824},{3,1824},{3,1824},{3,1824}}};
static uint16_t g_462 = 65530UL;
static uint16_t g_488 = 0xCED9L;
static struct S2 g_502 = {1,4,970,1,-219,17,0x9B0DD2C938424177LL,-49,-1,18446744073709551615UL};/* VOLATILE GLOBAL g_502 */
static volatile int8_t g_506 = 0x09L;/* VOLATILE GLOBAL g_506 */
static volatile int8_t * volatile g_505[9][1] = {{&g_506},{&g_506},{&g_506},{&g_506},{&g_506},{&g_506},{&g_506},{&g_506},{&g_506}};
static volatile int8_t * volatile * const g_504 = &g_505[1][0];
static volatile int8_t * volatile *g_508 = &g_505[0][0];
static volatile int8_t * volatile ** volatile g_507 = &g_508;/* VOLATILE GLOBAL g_507 */
static struct S1 *g_511[5][8] = {{(void*)0,&g_43,&g_43,&g_43,(void*)0,&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43},{&g_43,(void*)0,&g_43,&g_43,&g_43,&g_43,(void*)0,&g_43},{&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43}};
static struct S1 ** volatile g_510 = &g_511[3][2];/* VOLATILE GLOBAL g_510 */
static int8_t g_570 = (-8L);
static struct S0 ** volatile g_592[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static struct S0 ** volatile g_593 = &g_406[2];/* VOLATILE GLOBAL g_593 */
static struct S0 g_599 = {2567,-195,75,2};
static uint16_t g_618[10] = {0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L};
static struct S1 g_633 = {0x1C68D558L,0,14,-28,0,-24,22091};
static int32_t * volatile g_664[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t * volatile g_665 = &g_116;/* VOLATILE GLOBAL g_665 */
static float * volatile g_716 = &g_148;/* VOLATILE GLOBAL g_716 */
static volatile struct S2 g_721 = {1,2,-1861,18,50,28,0UL,-599,-1,0xCF852403D7143BB4LL};/* VOLATILE GLOBAL g_721 */
static volatile uint16_t g_757 = 1UL;/* VOLATILE GLOBAL g_757 */
static uint64_t *g_764 = &g_502.f9;
static uint64_t **g_763[2][1] = {{&g_764},{&g_764}};
static uint64_t ***g_762 = &g_763[0][0];
static uint64_t **** volatile g_761 = &g_762;/* VOLATILE GLOBAL g_761 */
static volatile struct S2 g_773[9] = {{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL},{0,1,-1966,-29,-45,18,0UL,987,-4,0UL}};
static struct S1 *g_788 = &g_43;
static struct S1 ** volatile g_787 = &g_788;/* VOLATILE GLOBAL g_787 */
static volatile uint32_t g_796 = 0x3F69AE21L;/* VOLATILE GLOBAL g_796 */
static int16_t *g_843 = &g_112;
static int16_t **g_842[9][3][4] = {{{(void*)0,&g_843,&g_843,(void*)0},{&g_843,&g_843,(void*)0,&g_843},{&g_843,(void*)0,&g_843,&g_843}},{{(void*)0,&g_843,&g_843,&g_843},{(void*)0,&g_843,&g_843,&g_843},{&g_843,&g_843,&g_843,&g_843}},{{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,(void*)0,&g_843}},{{(void*)0,&g_843,&g_843,&g_843},{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,(void*)0,&g_843}},{{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,(void*)0,&g_843},{(void*)0,&g_843,&g_843,&g_843}},{{&g_843,&g_843,&g_843,&g_843},{&g_843,(void*)0,&g_843,&g_843},{&g_843,&g_843,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_843},{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,(void*)0,&g_843}},{{&g_843,&g_843,(void*)0,(void*)0},{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,(void*)0,&g_843}},{{(void*)0,&g_843,(void*)0,&g_843},{&g_843,&g_843,&g_843,&g_843},{&g_843,&g_843,&g_843,&g_843}}};
static int16_t ***g_841[7] = {(void*)0,&g_842[0][1][3],&g_842[0][1][3],(void*)0,&g_842[0][1][3],&g_842[0][1][3],(void*)0};
static int16_t **** volatile g_840 = &g_841[2];/* VOLATILE GLOBAL g_840 */
static float * volatile g_896 = &g_400[0][1];/* VOLATILE GLOBAL g_896 */
static int64_t ***g_915 = &g_61;
static int64_t ****g_914[8] = {&g_915,&g_915,&g_915,&g_915,&g_915,&g_915,&g_915,&g_915};
static struct S2 g_955[6] = {{1,0,23,-4,-335,2,0x5ED6E22E9BF3C811LL,875,-4,18446744073709551612UL},{1,2,634,9,216,14,18446744073709551615UL,-161,-3,1UL},{1,2,634,9,216,14,18446744073709551615UL,-161,-3,1UL},{1,0,23,-4,-335,2,0x5ED6E22E9BF3C811LL,875,-4,18446744073709551612UL},{1,2,634,9,216,14,18446744073709551615UL,-161,-3,1UL},{1,2,634,9,216,14,18446744073709551615UL,-161,-3,1UL}};
static int8_t *g_978[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int8_t **g_977 = &g_978[3];
static struct S3 g_1006[5] = {{8,877},{8,877},{8,877},{8,877},{8,877}};
static volatile int16_t g_1053 = 0L;/* VOLATILE GLOBAL g_1053 */
static uint8_t *g_1146 = &g_133;
static uint8_t ** volatile g_1145 = &g_1146;/* VOLATILE GLOBAL g_1145 */
static float * volatile g_1149 = &g_400[0][1];/* VOLATILE GLOBAL g_1149 */
static uint8_t ** volatile *g_1161[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t ** volatile ** volatile g_1160[6] = {&g_1161[1],&g_1161[1],&g_1161[1],&g_1161[1],&g_1161[1],&g_1161[1]};
static volatile struct S2 g_1194 = {0,4,-1770,-5,-44,7,8UL,770,-1,0UL};/* VOLATILE GLOBAL g_1194 */
static volatile int64_t g_1204[5][4][5] = {{{0x9F8287EC6CB94585LL,(-1L),0x97282D25CC5599B8LL,0x9F8287EC6CB94585LL,0x4863120FAD18FF4BLL},{0x1F63BCAC1280E359LL,0xE715FA55933F5FEDLL,0L,0xE715FA55933F5FEDLL,0x1F63BCAC1280E359LL},{0x97282D25CC5599B8LL,0x9E5F4121E2B3D5CALL,(-1L),0x4863120FAD18FF4BLL,0x9E5F4121E2B3D5CALL},{0x1F63BCAC1280E359LL,1L,(-10L),0L,0x1E90EACC8FFB3086LL}},{{0x9F8287EC6CB94585LL,1L,0x6415D8A66540860BLL,0x9E5F4121E2B3D5CALL,0x9E5F4121E2B3D5CALL},{0x269CFCD7D07BD5EFLL,0L,0x269CFCD7D07BD5EFLL,0x7E4DA8CC620B15D1LL,0x1F63BCAC1280E359LL},{0x9E5F4121E2B3D5CALL,(-1L),0x4863120FAD18FF4BLL,0x9E5F4121E2B3D5CALL,0x4863120FAD18FF4BLL},{0x5478777CB122DB28LL,0xA974DFE454403283LL,0L,0L,(-3L)}},{{0x32B83967713A0403LL,0x4863120FAD18FF4BLL,(-1L),(-1L),0x4863120FAD18FF4BLL},{0x1E90EACC8FFB3086LL,1L,0xF0BFD8B9102B65ADLL,0x7E4DA8CC620B15D1LL,0x6E1003DA64DE7415LL},{(-1L),0x4863120FAD18FF4BLL,0x9E5F4121E2B3D5CALL,0x4863120FAD18FF4BLL,(-1L)},{0xF0BFD8B9102B65ADLL,0x5B868E71119D7315LL,0L,9L,0x269CFCD7D07BD5EFLL}},{{(-1L),0x6415D8A66540860BLL,0x6415D8A66540860BLL,(-1L),(-1L)},{0x1E90EACC8FFB3086LL,1L,0x5478777CB122DB28LL,0x5B868E71119D7315LL,0x269CFCD7D07BD5EFLL},{0x32B83967713A0403LL,(-1L),0x32B83967713A0403LL,(-1L),(-1L)},{0x269CFCD7D07BD5EFLL,1L,0x6E1003DA64DE7415LL,0x5B868E71119D7315LL,0x6E1003DA64DE7415LL}},{{0x97282D25CC5599B8LL,0x97282D25CC5599B8LL,0x9E5F4121E2B3D5CALL,(-1L),0x4863120FAD18FF4BLL},{0xF0BFD8B9102B65ADLL,0x7E4DA8CC620B15D1LL,0x6E1003DA64DE7415LL,9L,0x1E90EACC8FFB3086LL},{0x4863120FAD18FF4BLL,0x6415D8A66540860BLL,0x32B83967713A0403LL,0x4863120FAD18FF4BLL,(-1L)},{(-10L),0x7E4DA8CC620B15D1LL,0x5478777CB122DB28LL,0x7E4DA8CC620B15D1LL,(-10L)}}};
static struct S0 g_1250 = {2481,-41,20,-1};
static struct S1 g_1254 = {0x5DA658DAL,-4,26,16,0,15,19971};
static float g_1256[8][10][3] = {{{0x1.9p-1,0xD.FAAA52p+83,(-0x3.1p-1)},{0x8.0791E9p+50,(-0x8.0p-1),0x2.9p-1},{0x1.9p-1,0x1.Dp+1,0xB.10BD57p+92},{0x1.Ap+1,0x1.7p-1,0xA.6D5DA3p-18},{0x1.9p-1,0x9.FB18F7p+5,0x0.Ep+1},{0x8.0791E9p+50,0xB.D21330p+1,0x8.0791E9p+50},{0x1.9p-1,0x6.Ep+1,(-0x3.1p-1)},{0x1.Ap+1,(-0x8.0p-1),0x4.6193CBp+38},{0x1.9p-1,0x2.Ep-1,0xB.10BD57p+92},{0x8.0791E9p+50,0x1.7p-1,0x0.8886FFp-33}},{{0x1.9p-1,0x6.07FB2Dp-22,0x0.Ep+1},{0x1.Ap+1,0xB.D21330p+1,0x1.Ap+1},{0x1.9p-1,0xD.FAAA52p+83,(-0x3.1p-1)},{0x8.0791E9p+50,(-0x8.0p-1),0x2.9p-1},{0x1.9p-1,0x1.Dp+1,0xB.10BD57p+92},{0x1.Ap+1,0x1.7p-1,0xA.6D5DA3p-18},{0x1.9p-1,0x9.FB18F7p+5,0x0.Ep+1},{0x8.0791E9p+50,0xB.D21330p+1,0x8.0791E9p+50},{0x1.9p-1,0x6.Ep+1,(-0x3.1p-1)},{0x1.Ap+1,(-0x8.0p-1),0x4.6193CBp+38}},{{0x1.9p-1,0x2.Ep-1,0xB.10BD57p+92},{0x8.0791E9p+50,0x1.7p-1,0x0.8886FFp-33},{0x1.9p-1,0x6.07FB2Dp-22,0x0.Ep+1},{0x1.Ap+1,0xB.D21330p+1,0x1.Ap+1},{0x1.9p-1,0xD.FAAA52p+83,(-0x3.1p-1)},{0x8.0791E9p+50,(-0x8.0p-1),0x2.9p-1},{0x1.9p-1,0x1.Dp+1,0xB.10BD57p+92},{0x1.Ap+1,0x1.7p-1,0xA.6D5DA3p-18},{0x1.9p-1,0x9.FB18F7p+5,0x0.Ep+1},{0x8.0791E9p+50,0xB.D21330p+1,0x8.0791E9p+50}},{{0x1.9p-1,0x6.Ep+1,(-0x3.1p-1)},{0x1.Ap+1,(-0x8.0p-1),0x4.6193CBp+38},{0x1.9p-1,0x2.Ep-1,0xB.10BD57p+92},{0x8.0791E9p+50,0x1.7p-1,0x0.8886FFp-33},{0x1.9p-1,0x6.07FB2Dp-22,0x0.Ep+1},{0x1.Ap+1,0xB.D21330p+1,0x1.Ap+1},{0x1.9p-1,0xD.FAAA52p+83,(-0x3.1p-1)},{0x8.0791E9p+50,(-0x8.0p-1),0x2.9p-1},{0x1.9p-1,0x1.Dp+1,0xB.10BD57p+92},{0x1.Ap+1,0x1.7p-1,0xA.6D5DA3p-18}},{{0x1.9p-1,0x9.FB18F7p+5,0x0.Ep+1},{0x8.0791E9p+50,0xB.D21330p+1,0x8.0791E9p+50},{0x1.9p-1,0x6.Ep+1,(-0x3.1p-1)},{0x1.Ap+1,(-0x8.0p-1),0x9.D907B7p+77},{(-0x1.8p+1),0x5.C826C5p+43,0x1.0p-1},{0x4.20B599p-59,0x1.5p+1,0x2.Cp-1},{(-0x1.8p+1),0x0.Ep+1,0xE.196CBFp-66},{0x3.3p-1,(-0x5.Bp-1),0x3.3p-1},{(-0x1.8p+1),0x1.9p-1,0x4.DCFD45p+9},{0x4.20B599p-59,0xF.6E38B8p-76,0x1.EF60A9p+84}},{{(-0x1.8p+1),(-0x3.1p-1),0x1.0p-1},{0x3.3p-1,0x1.5p+1,0xE.E3F854p+35},{(-0x1.8p+1),(-0x5.Cp-1),0xE.196CBFp-66},{0x4.20B599p-59,(-0x5.Bp-1),0x4.20B599p-59},{(-0x1.8p+1),0xB.10BD57p+92,0x4.DCFD45p+9},{0x3.3p-1,0xF.6E38B8p-76,0x9.D907B7p+77},{(-0x1.8p+1),0x5.C826C5p+43,0x1.0p-1},{0x4.20B599p-59,0x1.5p+1,0x2.Cp-1},{(-0x1.8p+1),0x0.Ep+1,0xE.196CBFp-66},{0x3.3p-1,(-0x5.Bp-1),0x3.3p-1}},{{(-0x1.8p+1),0x1.9p-1,0x4.DCFD45p+9},{0x4.20B599p-59,0xF.6E38B8p-76,0x1.EF60A9p+84},{(-0x1.8p+1),(-0x3.1p-1),0x1.0p-1},{0x3.3p-1,0x1.5p+1,0xE.E3F854p+35},{(-0x1.8p+1),(-0x5.Cp-1),0xE.196CBFp-66},{0x4.20B599p-59,(-0x5.Bp-1),0x4.20B599p-59},{(-0x1.8p+1),0xB.10BD57p+92,0x4.DCFD45p+9},{0x3.3p-1,0xF.6E38B8p-76,0x9.D907B7p+77},{(-0x1.8p+1),0x5.C826C5p+43,0x1.0p-1},{0x4.20B599p-59,0x1.5p+1,0x2.Cp-1}},{{(-0x1.8p+1),0x0.Ep+1,0xE.196CBFp-66},{0x3.3p-1,(-0x5.Bp-1),0x3.3p-1},{(-0x1.8p+1),0x1.9p-1,0x4.DCFD45p+9},{0x4.20B599p-59,0xF.6E38B8p-76,0x1.EF60A9p+84},{(-0x1.8p+1),(-0x3.1p-1),0x1.0p-1},{0x3.3p-1,0x1.5p+1,0xE.E3F854p+35},{(-0x1.8p+1),(-0x5.Cp-1),0xE.196CBFp-66},{0x4.20B599p-59,(-0x5.Bp-1),0x4.20B599p-59},{(-0x1.8p+1),0xB.10BD57p+92,0x4.DCFD45p+9},{0x3.3p-1,0xF.6E38B8p-76,0x9.D907B7p+77}}};
static volatile struct S3 g_1260[6] = {{6,9872},{6,9872},{6,9872},{6,9872},{6,9872},{6,9872}};
static volatile uint64_t * volatile g_1270 = &g_773[6].f9;/* VOLATILE GLOBAL g_1270 */
static float * volatile * volatile g_1304[8] = {&g_896,&g_896,&g_896,&g_896,&g_896,&g_896,&g_896,&g_896};
static float * volatile * volatile *g_1303[8] = {&g_1304[1],&g_1304[1],&g_1304[1],&g_1304[1],&g_1304[1],&g_1304[1],&g_1304[1],&g_1304[1]};
static int8_t g_1363 = 1L;
static float * volatile g_1364 = &g_148;/* VOLATILE GLOBAL g_1364 */
static float * volatile g_1377 = &g_148;/* VOLATILE GLOBAL g_1377 */


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static float  func_6(uint64_t  p_7, uint32_t  p_8, int32_t  p_9);
static int16_t  func_22(uint16_t  p_23, struct S1  p_24, struct S0  p_25, uint8_t  p_26, uint32_t  p_27);
static uint16_t  func_34(int64_t  p_35, uint32_t  p_36);
static int64_t ** func_51(int64_t * p_52);
static int32_t * func_79(int64_t ** p_80, uint8_t  p_81, uint32_t  p_82, struct S0  p_83, int8_t  p_84);
static uint32_t  func_86(uint64_t  p_87, uint8_t  p_88, int32_t  p_89, int64_t *** p_90, struct S0  p_91);
static struct S1  func_93(int8_t  p_94, struct S1  p_95, struct S1  p_96, int64_t *** p_97, int32_t  p_98);
static int64_t  func_101(struct S1  p_102);
static struct S0  func_103(int32_t ** p_104);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_15 g_16 g_21 g_1377 g_762 g_763 g_764 g_502.f9
 * writes: g_16 g_33 g_400 g_148
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    struct S1 l_28[5][2] = {{{0x0F839313L,-4,12,-32,0,11,15306},{0x0F839313L,-4,12,-32,0,11,15306}},{{-3L,2,13,11,0,-24,4484},{0x0F839313L,-4,12,-32,0,11,15306}},{{0x0F839313L,-4,12,-32,0,11,15306},{-3L,2,13,11,0,-24,4484}},{{0x0F839313L,-4,12,-32,0,11,15306},{0x0F839313L,-4,12,-32,0,11,15306}},{{-3L,2,13,11,0,-24,4484},{0x0F839313L,-4,12,-32,0,11,15306}}};
    struct S0 l_29 = {186,152,69,-3};
    int32_t *l_32 = &g_33;
    float *l_1376 = &g_400[1][0];
    int i, j;
    (*g_1377) = ((safe_add_func_float_f_f((((*l_1376) = (safe_mul_func_float_f_f(func_6((safe_unary_minus_func_uint16_t_u((safe_sub_func_uint32_t_u_u(((((safe_div_func_int32_t_s_s((g_16 ^= (g_15 , 1L)), (safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s(g_21, g_21)), 1UL)))) , g_15.f6) ^ (g_21 == g_21)) == ((*l_32) = (func_22(g_21, l_28[2][1], l_29, g_21, l_28[2][1].f0) != l_28[2][1].f4))), g_21)))), g_21, l_28[2][1].f6), 0x2.Bp+1))) > 0xD.EC6950p-10), l_28[2][1].f2)) < l_28[2][1].f5);
    return (***g_762);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static float  func_6(uint64_t  p_7, uint32_t  p_8, int32_t  p_9)
{ /* block id: 6 */
    int16_t l_38 = 0L;
    int32_t l_42 = 4L;
    int64_t *l_46 = &g_47;
    int64_t *l_48 = (void*)0;
    int64_t *l_49 = &g_50;
    struct S1 l_666 = {5L,-3,1,-39,-0,31,4959};
    int32_t l_667 = 0x43F73044L;
    uint8_t *l_682 = &g_133;
    int16_t l_684 = 0x5623L;
    uint8_t l_689 = 1UL;
    int8_t l_691 = 0x97L;
    int64_t ***l_722 = &g_61;
    uint64_t l_753[7] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL};
    int32_t l_756 = 0xCD559CC1L;
    struct S1 *l_785 = &g_43;
    int32_t l_822 = 1L;
    int32_t l_823 = (-10L);
    int32_t l_826 = 0xD8443C00L;
    int32_t l_827 = 9L;
    int32_t l_829 = 0x0B583222L;
    int32_t l_831 = 8L;
    int32_t l_833[9] = {0xEC266A7EL,(-4L),0xEC266A7EL,0xEC266A7EL,(-4L),0xEC266A7EL,0xEC266A7EL,(-4L),0xEC266A7EL};
    uint32_t l_860 = 3UL;
    const uint16_t l_890 = 65535UL;
    uint16_t l_910 = 65530UL;
    float ** const l_928 = (void*)0;
    float ** const *l_927 = &l_928;
    struct S2 *l_952 = &g_236[1];
    int8_t l_960 = 0x45L;
    uint64_t l_961 = 18446744073709551613UL;
    int32_t l_962 = (-6L);
    uint64_t l_1005 = 0x5601D7E5CEDC5888LL;
    int8_t l_1025 = 0xE5L;
    uint32_t l_1135 = 0xAC356F93L;
    int32_t *l_1148 = &g_16;
    int32_t l_1159 = 1L;
    const uint32_t l_1182[1] = {0x88370B7AL};
    float l_1186 = (-0x2.4p+1);
    uint8_t **l_1234 = &g_1146;
    uint8_t *** const l_1233[2][5][9] = {{{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,(void*)0,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,(void*)0,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,(void*)0},{&l_1234,&l_1234,(void*)0,&l_1234,&l_1234,(void*)0,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,(void*)0,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234}},{{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1234,(void*)0,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,(void*)0,(void*)0,&l_1234,&l_1234,&l_1234},{&l_1234,(void*)0,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,(void*)0,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,(void*)0,&l_1234,&l_1234}}};
    uint8_t *** const *l_1232 = &l_1233[0][2][2];
    uint64_t * const ***l_1238 = (void*)0;
    float l_1296 = (-0x10.0p-1);
    int32_t *l_1375 = &l_826;
    int i, j, k;
    return p_9;
}


/* ------------------------------------------ */
/* 
 * reads : g_21
 * writes:
 */
static int16_t  func_22(uint16_t  p_23, struct S1  p_24, struct S0  p_25, uint8_t  p_26, uint32_t  p_27)
{ /* block id: 2 */
    const int32_t *l_30 = &g_21;
    const int32_t **l_31 = &l_30;
    (*l_31) = l_30;
    return (**l_31);
}


/* ------------------------------------------ */
/* 
 * reads : g_56 g_61 g_62 g_33 g_105 g_106 g_21 g_178 g_507 g_508 g_505 g_506 g_47 g_76 g_43.f0 g_336 g_428.f4 g_134 g_133 g_593 g_502.f9 g_43.f4 g_15.f5 g_15.f9 g_238 g_239 g_236 g_43.f2 g_43.f1 g_599.f0 g_618 g_451 g_633 g_116 g_488 g_599.f2 g_570 g_504 g_117 g_158 g_462 g_502.f6 g_665
 * writes: g_56 g_61 g_33 g_106 g_21 g_178 g_47 g_488 g_406 g_117 g_15.f6 g_15.f9 g_50 g_147 g_116 g_43.f1 g_148 g_618 g_502.f6 g_400 g_16 g_112 g_570 g_633.f2
 */
static uint16_t  func_34(int64_t  p_35, uint32_t  p_36)
{ /* block id: 12 */
    int64_t *l_53 = (void*)0;
    struct S1 l_234 = {0xE0F63417L,-1,9,28,0,-27,18380};
    float *l_296 = &g_148;
    int32_t l_306 = (-1L);
    int16_t l_335 = 0L;
    int32_t *l_339 = &g_33;
    struct S2 ** const l_376 = &g_239;
    uint64_t l_402 = 0x5FEA8F517381DB2DLL;
    uint8_t l_459 = 0x25L;
    int64_t l_491 = 0xE3DCA4C07D12DB45LL;
    int64_t ***l_503[8][6] = {{&g_61,(void*)0,(void*)0,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{(void*)0,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,(void*)0,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{(void*)0,&g_61,(void*)0,&g_61,&g_61,&g_61},{&g_61,(void*)0,(void*)0,&g_61,&g_61,&g_61}};
    struct S2 **l_582[6][3] = {{&g_239,&g_239,&g_239},{&g_239,&g_239,&g_239},{&g_239,&g_239,&g_239},{&g_239,&g_239,&g_239},{&g_239,&g_239,&g_239},{&g_239,&g_239,&g_239}};
    struct S2 ***l_581[5] = {&l_582[1][2],&l_582[1][2],&l_582[1][2],&l_582[1][2],&l_582[1][2]};
    int32_t l_615[8];
    int16_t l_616 = 0x2D3CL;
    int64_t l_617 = (-2L);
    uint16_t l_624[7][1] = {{0x39D7L},{1UL},{1UL},{0x39D7L},{1UL},{1UL},{0x39D7L}};
    int64_t ***l_642 = &g_61;
    uint32_t *l_643 = (void*)0;
    uint32_t *l_644 = (void*)0;
    uint32_t *l_645 = (void*)0;
    uint32_t *l_646 = (void*)0;
    uint32_t *l_647[8] = {&g_158,(void*)0,&g_158,(void*)0,&g_158,(void*)0,&g_158,(void*)0};
    uint32_t *l_652 = &g_117;
    int32_t *l_663 = &g_21;
    int i, j;
    for (i = 0; i < 8; i++)
        l_615[i] = 0x7CA80F05L;
lbl_403:
    (*g_62) = func_51(l_53);
    for (g_33 = 0; (g_33 >= 0); g_33 -= 1)
    { /* block id: 19 */
        float l_63 = 0xC.655405p+96;
        int64_t ***l_70 = &g_61;
        int32_t l_232[5] = {0x80A1DA36L,0x80A1DA36L,0x80A1DA36L,0x80A1DA36L,0x80A1DA36L};
        uint16_t l_345[6][3] = {{0x506FL,0x40EAL,0x40EAL},{0x74EDL,0UL,0x74EDL},{0x506FL,0x506FL,0x40EAL},{0x968FL,0UL,0x968FL},{0x506FL,0x40EAL,0x40EAL},{0x74EDL,0UL,0x74EDL}};
        int32_t **l_365 = (void*)0;
        struct S2 *l_427 = &g_236[2];
        const float l_448 = 0x1.9p+1;
        const uint64_t *l_469 = &g_236[2].f9;
        struct S1 l_497 = {0x0498EC84L,1,19,6,0,5,32050};
        int i, j;
    }
    if ((**g_105))
    { /* block id: 230 */
        uint64_t l_525 = 0x52422B1043792A23LL;
        int32_t l_534 = 0x5F1A5C17L;
        int32_t l_537 = 1L;
        int32_t l_538[4][1][3] = {{{0x183AFC6DL,8L,8L}},{{0L,0x62CC3560L,0x62CC3560L}},{{0x183AFC6DL,8L,8L}},{{0L,0x62CC3560L,0x62CC3560L}}};
        int16_t *l_547 = &g_112;
        int16_t **l_546 = &l_547;
        struct S2 ** const *l_558 = &l_376;
        struct S2 ** const * const *l_557[3][5] = {{&l_558,&l_558,&l_558,&l_558,&l_558},{&l_558,&l_558,&l_558,&l_558,&l_558},{&l_558,&l_558,&l_558,&l_558,&l_558}};
        uint64_t l_594 = 1UL;
        const struct S0 *l_598[8][3] = {{&g_227[5],(void*)0,&g_227[5]},{(void*)0,&g_599,&g_599},{&g_227[0],(void*)0,&g_227[0]},{(void*)0,(void*)0,&g_599},{&g_227[5],(void*)0,&g_227[5]},{(void*)0,&g_599,&g_599},{&g_227[0],(void*)0,&g_227[0]},{(void*)0,(void*)0,&g_599}};
        int i, j, k;
        if (p_35)
        { /* block id: 231 */
            (*g_105) = (void*)0;
        }
        else
        { /* block id: 233 */
            int32_t *l_523 = &g_116;
            int32_t *l_524[7] = {&g_33,&g_33,&g_33,&g_33,&g_33,&g_33,&g_33};
            int16_t **l_548 = &l_547;
            int8_t *** const l_565[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            const struct S0 *l_597[10] = {(void*)0,&g_227[4],&g_227[4],(void*)0,&g_227[4],&g_227[4],(void*)0,&g_227[4],&g_227[4],(void*)0};
            int16_t l_614 = 0x5A17L;
            int i;
            --l_525;
            if (((*g_106) = 0x4746D6BFL))
            { /* block id: 236 */
                float l_528 = 0xD.44B6B7p+49;
                int32_t l_529 = 1L;
                int32_t l_530 = 0L;
                int32_t l_531 = 1L;
                int32_t l_532 = 1L;
                int32_t l_533 = (-1L);
                int32_t l_535 = (-1L);
                int32_t l_536 = (-1L);
                uint32_t l_539 = 18446744073709551609UL;
                l_539++;
                for (g_21 = 0; (g_21 != (-6)); g_21--)
                { /* block id: 240 */
                    if (g_33)
                        goto lbl_403;
                    if (p_35)
                        break;
                    if ((**g_105))
                        break;
                }
                for (g_178 = 0; (g_178 <= (-6)); g_178 = safe_sub_func_int8_t_s_s(g_178, 7))
                { /* block id: 247 */
                    int8_t * const l_569[3] = {&g_570,&g_570,&g_570};
                    int8_t * const *l_568 = &l_569[1];
                    int8_t * const **l_567[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int8_t * const ***l_566 = &l_567[2];
                    const struct S2 **l_584 = (void*)0;
                    const struct S2 ***l_583 = &l_584;
                    int32_t l_589 = 0x7EE76987L;
                    struct S0 *l_591 = (void*)0;
                    int i;
                    l_548 = l_546;
                    for (g_47 = (-10); (g_47 <= (-8)); g_47 = safe_add_func_uint32_t_u_u(g_47, 6))
                    { /* block id: 251 */
                        if (p_36)
                            break;
                        return p_35;
                    }
                    if (((p_36 && (safe_add_func_int64_t_s_s(((safe_mul_func_uint8_t_u_u((((void*)0 == l_557[2][1]) <= (safe_lshift_func_int8_t_s_u(((((safe_mod_func_int8_t_s_s((***g_507), (safe_mod_func_int64_t_s_s(0x4C6D35B93D66BEC8LL, l_525)))) != (((((l_565[1] != ((*l_566) = (void*)0)) || (l_538[3][0][2] != p_35)) <= (-1L)) ^ (*l_339)) >= g_47)) , 0x6DL) || l_525), g_76[0]))), 1L)) == 0x9D30L), g_43.f0))) & 0UL))
                    { /* block id: 256 */
                        uint16_t *l_590 = &g_488;
                        (*g_105) = (*g_105);
                        (*g_105) = (*g_105);
                        (*g_106) ^= ((((*l_590) = (safe_div_func_uint64_t_u_u(0x018DA9F7A12CA1CBLL, (safe_mod_func_uint8_t_u_u(((l_534 = (p_35 && ((safe_mul_func_uint16_t_u_u(g_336, (safe_mod_func_int8_t_s_s((safe_add_func_int8_t_s_s(((g_428.f4 , l_581[4]) == l_583), ((safe_mod_func_uint8_t_u_u(((((p_35 = p_35) , (safe_sub_func_uint8_t_u_u(251UL, (l_525 < p_35)))) <= g_134) ^ 0xA89642856B086EFCLL), 0x2CL)) > (-7L)))), g_76[0])))) , l_529))) && l_589), g_133))))) > g_43.f0) & p_36);
                    }
                    else
                    { /* block id: 263 */
                        (*g_105) = l_524[6];
                        (*g_593) = l_591;
                        l_594--;
                        l_598[2][1] = l_597[0];
                    }
                }
            }
            else
            { /* block id: 270 */
                int64_t **l_606 = &l_53;
                uint64_t *l_612 = &g_502.f6;
                struct S0 l_613 = {2427,-237,60,0};
                (*l_296) = (0xF.34E1C3p+76 < (((p_36 != (safe_add_func_float_f_f((((safe_div_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u(((((void*)0 == &g_238) , func_79(l_606, (~(safe_lshift_func_int16_t_s_s(0x0996L, (l_537 ^ (((safe_mod_func_uint8_t_u_u((&l_525 != l_612), (***g_507))) >= p_35) > p_35))))), g_502.f9, l_613, p_35)) == &l_534), p_35)), p_36)) , 0xA.B0A90Bp-18) > p_36), 0x0.2p+1))) != g_599.f0) > p_35));
            }
            ++g_618[6];
        }
        l_537 = 0xCC02C5E4L;
        if (p_35)
        { /* block id: 276 */
            return g_451;
        }
        else
        { /* block id: 278 */
            return l_594;
        }
    }
    else
    { /* block id: 281 */
        int32_t *l_621 = &g_116;
        int32_t l_622[6][1] = {{0x6347D624L},{5L},{0x6347D624L},{0x6347D624L},{5L},{0x6347D624L}};
        int32_t *l_623[8][8] = {{&l_622[4][0],(void*)0,&l_306,(void*)0,&l_622[4][0],&l_306,&l_615[0],&l_615[5]},{&g_116,&l_622[0][0],&l_306,(void*)0,&l_615[3],&l_615[5],&l_615[3],(void*)0},{&l_306,&g_116,&l_306,&l_306,&l_306,&l_306,&l_615[0],&l_622[0][0]},{&l_615[3],&g_116,&l_306,&l_615[5],&l_622[2][0],&l_615[5],&l_306,&g_116},{&l_615[3],&l_622[0][0],&l_615[0],&l_306,&l_306,&l_306,&l_306,&g_116},{&l_306,(void*)0,&l_615[3],&l_615[5],&l_615[3],(void*)0,&l_306,&l_622[0][0]},{&g_116,&l_615[5],&l_615[0],&l_306,&l_622[4][0],(void*)0,&l_306,(void*)0},{&l_622[4][0],(void*)0,&l_306,(void*)0,&l_622[4][0],&l_306,&l_615[0],&l_615[5]}};
        struct S1 l_632 = {0xBED688FAL,-2,26,42,-0,26,7748};
        int32_t *l_634 = &g_16;
        uint16_t *l_640 = &g_488;
        int8_t *l_641 = &g_570;
        int i, j;
        for (g_502.f6 = 0; g_502.f6 < 3; g_502.f6 += 1)
        {
            for (l_306 = 0; l_306 < 5; l_306 += 1)
            {
                g_400[g_502.f6][l_306] = 0x1.5p-1;
            }
        }
        ++l_624[0][0];
        (*l_339) |= ((*l_621) = (safe_rshift_func_int8_t_s_s((safe_unary_minus_func_int64_t_s((safe_lshift_func_uint8_t_u_u(((func_93(p_36, l_632, g_633, l_503[1][0], ((*l_634) = p_35)) , ((*l_641) &= (safe_unary_minus_func_uint8_t_u((((((*l_640) &= ((safe_mul_func_uint8_t_u_u((((*g_238) == ((safe_lshift_func_int8_t_s_s((*l_621), 0)) , g_239)) != 1UL), p_35)) == 0x85L)) == (*l_621)) != g_633.f5) == g_599.f2))))) != 3L), 0)))), p_35)));
        (*g_106) = (l_503[1][0] != l_642);
    }
    (*g_665) = ((g_633.f2 &= 0x1E4161D9L) , ((safe_div_func_int64_t_s_s((safe_add_func_int32_t_s_s((((**g_504) < ((((*l_652) &= ((&g_112 == &l_335) < 0x9660CAFCL)) , (safe_sub_func_int64_t_s_s((p_35 = 8L), (+(~(safe_mul_func_uint8_t_u_u(g_428.f4, 0x2CL))))))) && (((*l_663) ^= (safe_lshift_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(((*l_339) = (0x8F0D4B400176441BLL != g_158)), p_36)), g_462))) ^ 0L))) || (*l_663)), g_502.f6)), p_36)) || 0x737DA29976C1390ELL));
    return (*l_339);
}


/* ------------------------------------------ */
/* 
 * reads : g_56 g_61
 * writes: g_56
 */
static int64_t ** func_51(int64_t * p_52)
{ /* block id: 13 */
    int32_t *l_54[6];
    int64_t *l_60 = &g_47;
    int64_t **l_59 = &l_60;
    int i;
    for (i = 0; i < 6; i++)
        l_54[i] = &g_21;
    g_56--;
    return g_61;
}


/* ------------------------------------------ */
/* 
 * reads : g_178 g_105 g_21 g_43.f4 g_15.f5 g_15.f9 g_33 g_238 g_239 g_236 g_43.f2 g_43.f1 g_106
 * writes: g_178 g_117 g_106 g_15.f6 g_15.f9 g_50 g_147 g_116 g_43.f1
 */
static int32_t * func_79(int64_t ** p_80, uint8_t  p_81, uint32_t  p_82, struct S0  p_83, int8_t  p_84)
{ /* block id: 85 */
    uint64_t l_255 = 0x0337DF4CC50A3A8ELL;
    struct S1 l_263[5] = {{1L,-0,17,-16,-0,11,2665},{1L,-0,17,-16,-0,11,2665},{1L,-0,17,-16,-0,11,2665},{1L,-0,17,-16,-0,11,2665},{1L,-0,17,-16,-0,11,2665}};
    struct S2 **l_275 = &g_239;
    struct S2 ***l_274 = &l_275;
    int i;
    for (g_178 = 24; (g_178 <= 27); g_178++)
    { /* block id: 88 */
        int8_t l_254 = 0L;
        uint16_t l_258 = 0xF69AL;
        for (g_117 = 27; (g_117 < 35); g_117++)
        { /* block id: 91 */
            int32_t *l_253[5][3][9] = {{{&g_116,(void*)0,&g_116,(void*)0,(void*)0,(void*)0,&g_21,(void*)0,(void*)0},{&g_116,&g_33,&g_33,&g_116,&g_33,(void*)0,&g_21,(void*)0,&g_33},{&g_21,&g_33,&g_116,&g_21,&g_116,(void*)0,&g_21,&g_33,&g_33}},{{&g_21,(void*)0,&g_33,&g_21,&g_33,(void*)0,&g_21,&g_33,(void*)0},{&g_21,(void*)0,&g_116,&g_116,(void*)0,&g_116,(void*)0,(void*)0,(void*)0},{&g_21,&g_33,&g_33,(void*)0,&g_116,&g_116,(void*)0,&g_33,&g_116}},{{&g_116,&g_116,(void*)0,&g_33,(void*)0,(void*)0,(void*)0,&g_116,&g_116},{&g_33,&g_33,(void*)0,&g_116,&g_33,(void*)0,&g_33,(void*)0,&g_33},{&g_33,&g_116,&g_116,&g_33,&g_116,&g_33,&g_116,(void*)0,&g_116}},{{&g_116,&g_116,(void*)0,&g_116,&g_33,&g_33,&g_33,&g_116,&g_116},{(void*)0,(void*)0,&g_116,&g_116,&g_116,(void*)0,(void*)0,&g_116,(void*)0},{&g_116,(void*)0,(void*)0,&g_33,&g_33,(void*)0,&g_116,&g_33,(void*)0}},{{(void*)0,&g_116,&g_116,&g_116,(void*)0,(void*)0,&g_116,&g_116,&g_116},{&g_116,&g_116,(void*)0,&g_33,(void*)0,(void*)0,(void*)0,&g_116,&g_116},{&g_33,&g_33,(void*)0,&g_116,&g_33,(void*)0,&g_33,(void*)0,&g_33}}};
            int i, j, k;
            ++l_255;
            l_258 |= 0x11D92424L;
            (*g_105) = (void*)0;
            for (g_15.f6 = 0; g_15.f6 < 6; g_15.f6 += 1)
            {
                for (g_15.f9 = 0; g_15.f9 < 8; g_15.f9 += 1)
                {
                    for (g_50 = 0; g_50 < 5; g_50 += 1)
                    {
                        g_147[g_15.f6][g_15.f9][g_50] = &g_148;
                    }
                }
            }
        }
    }
    if ((safe_lshift_func_int8_t_s_u(g_21, (safe_sub_func_uint64_t_u_u(((l_263[3] , (safe_div_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(g_43.f4, 4)), ((g_15.f5 , ((18446744073709551613UL <= (safe_sub_func_int64_t_s_s(p_84, l_263[3].f2))) > g_15.f9)) & (safe_add_func_int8_t_s_s((((l_263[3].f1 & 0UL) == g_33) , p_83.f3), p_81)))))) >= l_263[3].f6), p_83.f2)))))
    { /* block id: 98 */
        struct S2 ****l_276 = &l_274;
        int32_t *l_277 = &g_116;
        (*l_276) = l_274;
        (*l_277) = ((**g_238) , l_263[3].f5);
    }
    else
    { /* block id: 101 */
        int32_t *l_278[2];
        int i;
        for (i = 0; i < 2; i++)
            l_278[i] = &g_33;
        (*g_105) = l_278[1];
        g_43.f1 ^= (~g_43.f2);
    }
    p_83.f1 = p_83.f3;
    return (*g_105);
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_39
 * writes: g_47
 */
static uint32_t  func_86(uint64_t  p_87, uint8_t  p_88, int32_t  p_89, int64_t *** p_90, struct S0  p_91)
{ /* block id: 78 */
    int16_t l_247 = 4L;
    for (g_47 = 0; (g_47 <= 0); g_47 += 1)
    { /* block id: 81 */
        int i;
        return g_39[g_47];
    }
    return l_247;
}


/* ------------------------------------------ */
/* 
 * reads : g_112 g_105
 * writes: g_112 g_106
 */
static struct S1  func_93(int8_t  p_94, struct S1  p_95, struct S1  p_96, int64_t *** p_97, int32_t  p_98)
{ /* block id: 71 */
    int32_t * const l_244 = &g_33;
    for (g_112 = 0; (g_112 >= (-7)); --g_112)
    { /* block id: 74 */
        (*g_105) = (p_96.f2 , l_244);
    }
    return p_95;
}


/* ------------------------------------------ */
/* 
 * reads : g_238
 * writes: g_239
 */
static int64_t  func_101(struct S1  p_102)
{ /* block id: 68 */
    struct S2 *l_235 = &g_236[2];
    struct S2 **l_237 = (void*)0;
    (*g_238) = l_235;
    return p_102.f5;
}


/* ------------------------------------------ */
/* 
 * reads : g_112 g_117 g_16 g_106 g_105 g_21 g_43.f3 g_78 g_134 g_47 g_149 g_158 g_161 g_43.f1 g_116 g_15.f0 g_191 g_178 g_39 g_43.f4 g_196 g_214 g_43 g_15.f2 g_133 g_227
 * writes: g_112 g_117 g_133 g_134 g_106 g_148 g_158 g_56 g_178 g_21 g_116 g_197 g_214
 */
static struct S0  func_103(int32_t ** p_104)
{ /* block id: 31 */
    int32_t l_110 = 0x9B9F5A9AL;
    int16_t *l_111 = &g_112;
    int64_t *l_126 = &g_47;
    struct S0 l_131 = {2056,441,25,-3};
    float *l_132 = (void*)0;
    int16_t *l_135[9][7] = {{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}};
    int64_t ** const * const l_182 = &g_61;
    struct S0 l_188 = {2543,407,88,0};
    int32_t l_209 = 1L;
    int32_t l_210 = (-1L);
    int32_t l_212 = 0x7E1D4163L;
    int i, j;
    if ((!((safe_lshift_func_int16_t_s_s(((*l_111) ^= (l_110 > l_110)), ((safe_mul_func_int16_t_s_s((+(++g_117)), (safe_add_func_uint32_t_u_u((((safe_rshift_func_int16_t_s_s(0x22B3L, 4)) < (l_131.f1 = (g_16 & ((safe_sub_func_uint8_t_u_u((((g_134 = ((g_133 = ((l_126 == (void*)0) < (((safe_mod_func_int32_t_s_s((safe_div_func_uint64_t_u_u(((0x20DDCAC83E95E56FLL && (l_110 >= (((l_131 , (*p_104)) != (void*)0) != l_110))) || (**g_105)), 0xB5FCD5A05CCE5AB6LL)), 0x40E40D8DL)) || g_43.f3) , 0x3.BDDC6Dp-76))) > (-0x1.8p+1))) > (-0x4.Bp+1)) , g_78), l_131.f1)) <= 0xD7L)))) >= (-7L)), (**p_104))))) || g_134))) >= l_110)))
    { /* block id: 37 */
        uint8_t l_144 = 0x8CL;
        uint32_t *l_157 = &g_158;
        uint32_t *l_162 = (void*)0;
        int32_t l_163 = (-9L);
        uint16_t *l_166 = &g_56;
        uint64_t *l_169 = &g_134;
        int8_t *l_177[2];
        int32_t l_181 = 0xA96804C7L;
        int i;
        for (i = 0; i < 2; i++)
            l_177[i] = &g_178;
        (*g_105) = (*g_105);
        if (g_16)
            goto lbl_150;
lbl_150:
        (*g_149) = ((g_47 < 0x9.11F2A1p+39) < (safe_add_func_float_f_f(((0x0.1p-1 > (-(safe_div_func_float_f_f((safe_sub_func_float_f_f(0xB.9B809Ep-70, ((l_144 != l_131.f1) > l_144))), (((safe_sub_func_float_f_f((((0xD.F1B6C9p-37 <= l_131.f0) <= (-0x1.1p-1)) <= l_110), g_47)) == l_144) > 0x1.9p-1))))) != l_131.f3), 0x0.Fp+1)));
        l_181 |= ((safe_rshift_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(((((*l_157)++) > ((g_161 , (l_163 = l_110)) <= ((!(((**p_104) & (~((*l_166) = 65535UL))) | ((safe_mod_func_uint64_t_u_u((++(*l_169)), l_144)) & (safe_add_func_int8_t_s_s((safe_add_func_int8_t_s_s((g_178 = (!(l_131.f3 >= ((void*)0 == l_162)))), (safe_rshift_func_int16_t_s_s(l_131.f1, 6)))), l_144))))) > 1UL))) != l_131.f3), l_110)), g_43.f1)) | 0x7620F8F3L);
    }
    else
    { /* block id: 47 */
        int8_t l_183[8] = {0xB8L,(-2L),(-2L),0xB8L,(-2L),(-2L),0xB8L,(-2L)};
        int32_t *l_184 = &g_116;
        int32_t l_207 = 0x76BEBF0BL;
        int32_t l_208 = 1L;
        int32_t l_211[10][1] = {{0x8E47B015L},{1L},{0x8E47B015L},{(-6L)},{(-6L)},{0x8E47B015L},{1L},{0x8E47B015L},{(-6L)},{(-6L)}};
        int i, j;
        (*l_184) &= ((**g_105) = (((void*)0 == l_182) != l_183[5]));
        for (g_21 = 4; (g_21 >= 0); g_21 -= 1)
        { /* block id: 52 */
            uint8_t l_185 = 0x5FL;
            float *l_189 = &g_148;
            float **l_190 = &l_189;
            const int64_t * const * const l_192 = (void*)0;
            const int64_t * const l_195 = &g_50;
            const int64_t * const *l_194 = &l_195;
            const int64_t * const **l_193[5][3] = {{&l_194,&l_194,&l_194},{&l_194,&l_194,&l_194},{&l_194,&l_194,&l_194},{&l_194,&l_194,&l_194},{&l_194,&l_194,&l_194}};
            int32_t *l_199 = &g_116;
            int32_t *l_200 = &g_116;
            int32_t *l_201 = &g_116;
            int32_t *l_202 = (void*)0;
            int32_t *l_203 = &g_116;
            int32_t *l_204 = (void*)0;
            int32_t *l_205 = (void*)0;
            int32_t *l_206[8] = {&g_21,&g_21,&g_21,&g_21,&g_21,&g_21,&g_21,&g_21};
            int32_t l_213 = (-1L);
            uint8_t *l_221 = (void*)0;
            uint8_t *l_222[9];
            int i, j;
            for (i = 0; i < 9; i++)
                l_222[i] = &l_185;
            (*g_196) = (((l_185 < (safe_add_func_int8_t_s_s(((((g_15.f0 , ((l_131.f3 , l_188) , l_132)) == ((*l_190) = l_189)) && ((g_191 , g_178) & g_39[0])) & 0x94L), g_43.f4))) , l_131) , l_192);
            ++g_214;
            if ((**g_105))
                break;
            (*l_200) = (g_43 , (safe_mul_func_int16_t_s_s((g_15.f2 > (*l_200)), (((safe_mod_func_uint32_t_u_u(l_131.f0, g_112)) ^ ((((*l_184) < (++g_133)) , &g_178) != ((((safe_mod_func_uint32_t_u_u(4294967289UL, (g_21 , (**p_104)))) , 3UL) > g_39[0]) , &l_183[6]))) > 8UL))));
            for (l_209 = 7; (l_209 >= 0); l_209 -= 1)
            { /* block id: 61 */
                return g_227[4];
            }
        }
    }
    return g_227[4];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_15.f0, "g_15.f0", print_hash_value);
    transparent_crc(g_15.f1, "g_15.f1", print_hash_value);
    transparent_crc(g_15.f2, "g_15.f2", print_hash_value);
    transparent_crc(g_15.f3, "g_15.f3", print_hash_value);
    transparent_crc(g_15.f4, "g_15.f4", print_hash_value);
    transparent_crc(g_15.f5, "g_15.f5", print_hash_value);
    transparent_crc(g_15.f6, "g_15.f6", print_hash_value);
    transparent_crc(g_15.f7, "g_15.f7", print_hash_value);
    transparent_crc(g_15.f8, "g_15.f8", print_hash_value);
    transparent_crc(g_15.f9, "g_15.f9", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_33, "g_33", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_39[i], "g_39[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_43.f0, "g_43.f0", print_hash_value);
    transparent_crc(g_43.f1, "g_43.f1", print_hash_value);
    transparent_crc(g_43.f2, "g_43.f2", print_hash_value);
    transparent_crc(g_43.f3, "g_43.f3", print_hash_value);
    transparent_crc(g_43.f4, "g_43.f4", print_hash_value);
    transparent_crc(g_43.f5, "g_43.f5", print_hash_value);
    transparent_crc(g_43.f6, "g_43.f6", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_50, "g_50", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc_bytes(&g_55[i], sizeof(g_55[i]), "g_55[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_56, "g_56", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_76[i], "g_76[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_133, "g_133", print_hash_value);
    transparent_crc(g_134, "g_134", print_hash_value);
    transparent_crc(g_136, "g_136", print_hash_value);
    transparent_crc_bytes (&g_148, sizeof(g_148), "g_148", print_hash_value);
    transparent_crc(g_158, "g_158", print_hash_value);
    transparent_crc(g_161.f0, "g_161.f0", print_hash_value);
    transparent_crc(g_161.f1, "g_161.f1", print_hash_value);
    transparent_crc(g_178, "g_178", print_hash_value);
    transparent_crc(g_191.f0, "g_191.f0", print_hash_value);
    transparent_crc(g_191.f1, "g_191.f1", print_hash_value);
    transparent_crc(g_214, "g_214", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_227[i].f0, "g_227[i].f0", print_hash_value);
        transparent_crc(g_227[i].f1, "g_227[i].f1", print_hash_value);
        transparent_crc(g_227[i].f2, "g_227[i].f2", print_hash_value);
        transparent_crc(g_227[i].f3, "g_227[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_236[i].f0, "g_236[i].f0", print_hash_value);
        transparent_crc(g_236[i].f1, "g_236[i].f1", print_hash_value);
        transparent_crc(g_236[i].f2, "g_236[i].f2", print_hash_value);
        transparent_crc(g_236[i].f3, "g_236[i].f3", print_hash_value);
        transparent_crc(g_236[i].f4, "g_236[i].f4", print_hash_value);
        transparent_crc(g_236[i].f5, "g_236[i].f5", print_hash_value);
        transparent_crc(g_236[i].f6, "g_236[i].f6", print_hash_value);
        transparent_crc(g_236[i].f7, "g_236[i].f7", print_hash_value);
        transparent_crc(g_236[i].f8, "g_236[i].f8", print_hash_value);
        transparent_crc(g_236[i].f9, "g_236[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_323.f0, "g_323.f0", print_hash_value);
    transparent_crc(g_323.f1, "g_323.f1", print_hash_value);
    transparent_crc(g_332, "g_332", print_hash_value);
    transparent_crc(g_336, "g_336", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc_bytes(&g_400[i][j], sizeof(g_400[i][j]), "g_400[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_407.f0, "g_407.f0", print_hash_value);
    transparent_crc(g_407.f1, "g_407.f1", print_hash_value);
    transparent_crc(g_407.f2, "g_407.f2", print_hash_value);
    transparent_crc(g_407.f3, "g_407.f3", print_hash_value);
    transparent_crc(g_407.f4, "g_407.f4", print_hash_value);
    transparent_crc(g_407.f5, "g_407.f5", print_hash_value);
    transparent_crc(g_407.f6, "g_407.f6", print_hash_value);
    transparent_crc(g_407.f7, "g_407.f7", print_hash_value);
    transparent_crc(g_407.f8, "g_407.f8", print_hash_value);
    transparent_crc(g_407.f9, "g_407.f9", print_hash_value);
    transparent_crc(g_428.f0, "g_428.f0", print_hash_value);
    transparent_crc(g_428.f1, "g_428.f1", print_hash_value);
    transparent_crc(g_428.f2, "g_428.f2", print_hash_value);
    transparent_crc(g_428.f3, "g_428.f3", print_hash_value);
    transparent_crc(g_428.f4, "g_428.f4", print_hash_value);
    transparent_crc(g_428.f5, "g_428.f5", print_hash_value);
    transparent_crc(g_428.f6, "g_428.f6", print_hash_value);
    transparent_crc(g_428.f7, "g_428.f7", print_hash_value);
    transparent_crc(g_428.f8, "g_428.f8", print_hash_value);
    transparent_crc(g_428.f9, "g_428.f9", print_hash_value);
    transparent_crc(g_451, "g_451", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_454[i][j].f0, "g_454[i][j].f0", print_hash_value);
            transparent_crc(g_454[i][j].f1, "g_454[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_462, "g_462", print_hash_value);
    transparent_crc(g_488, "g_488", print_hash_value);
    transparent_crc(g_502.f0, "g_502.f0", print_hash_value);
    transparent_crc(g_502.f1, "g_502.f1", print_hash_value);
    transparent_crc(g_502.f2, "g_502.f2", print_hash_value);
    transparent_crc(g_502.f3, "g_502.f3", print_hash_value);
    transparent_crc(g_502.f4, "g_502.f4", print_hash_value);
    transparent_crc(g_502.f5, "g_502.f5", print_hash_value);
    transparent_crc(g_502.f6, "g_502.f6", print_hash_value);
    transparent_crc(g_502.f7, "g_502.f7", print_hash_value);
    transparent_crc(g_502.f8, "g_502.f8", print_hash_value);
    transparent_crc(g_502.f9, "g_502.f9", print_hash_value);
    transparent_crc(g_506, "g_506", print_hash_value);
    transparent_crc(g_570, "g_570", print_hash_value);
    transparent_crc(g_599.f0, "g_599.f0", print_hash_value);
    transparent_crc(g_599.f1, "g_599.f1", print_hash_value);
    transparent_crc(g_599.f2, "g_599.f2", print_hash_value);
    transparent_crc(g_599.f3, "g_599.f3", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_618[i], "g_618[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_633.f0, "g_633.f0", print_hash_value);
    transparent_crc(g_633.f1, "g_633.f1", print_hash_value);
    transparent_crc(g_633.f2, "g_633.f2", print_hash_value);
    transparent_crc(g_633.f3, "g_633.f3", print_hash_value);
    transparent_crc(g_633.f4, "g_633.f4", print_hash_value);
    transparent_crc(g_633.f5, "g_633.f5", print_hash_value);
    transparent_crc(g_633.f6, "g_633.f6", print_hash_value);
    transparent_crc(g_721.f0, "g_721.f0", print_hash_value);
    transparent_crc(g_721.f1, "g_721.f1", print_hash_value);
    transparent_crc(g_721.f2, "g_721.f2", print_hash_value);
    transparent_crc(g_721.f3, "g_721.f3", print_hash_value);
    transparent_crc(g_721.f4, "g_721.f4", print_hash_value);
    transparent_crc(g_721.f5, "g_721.f5", print_hash_value);
    transparent_crc(g_721.f6, "g_721.f6", print_hash_value);
    transparent_crc(g_721.f7, "g_721.f7", print_hash_value);
    transparent_crc(g_721.f8, "g_721.f8", print_hash_value);
    transparent_crc(g_721.f9, "g_721.f9", print_hash_value);
    transparent_crc(g_757, "g_757", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_773[i].f0, "g_773[i].f0", print_hash_value);
        transparent_crc(g_773[i].f1, "g_773[i].f1", print_hash_value);
        transparent_crc(g_773[i].f2, "g_773[i].f2", print_hash_value);
        transparent_crc(g_773[i].f3, "g_773[i].f3", print_hash_value);
        transparent_crc(g_773[i].f4, "g_773[i].f4", print_hash_value);
        transparent_crc(g_773[i].f5, "g_773[i].f5", print_hash_value);
        transparent_crc(g_773[i].f6, "g_773[i].f6", print_hash_value);
        transparent_crc(g_773[i].f7, "g_773[i].f7", print_hash_value);
        transparent_crc(g_773[i].f8, "g_773[i].f8", print_hash_value);
        transparent_crc(g_773[i].f9, "g_773[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_796, "g_796", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_955[i].f0, "g_955[i].f0", print_hash_value);
        transparent_crc(g_955[i].f1, "g_955[i].f1", print_hash_value);
        transparent_crc(g_955[i].f2, "g_955[i].f2", print_hash_value);
        transparent_crc(g_955[i].f3, "g_955[i].f3", print_hash_value);
        transparent_crc(g_955[i].f4, "g_955[i].f4", print_hash_value);
        transparent_crc(g_955[i].f5, "g_955[i].f5", print_hash_value);
        transparent_crc(g_955[i].f6, "g_955[i].f6", print_hash_value);
        transparent_crc(g_955[i].f7, "g_955[i].f7", print_hash_value);
        transparent_crc(g_955[i].f8, "g_955[i].f8", print_hash_value);
        transparent_crc(g_955[i].f9, "g_955[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1006[i].f0, "g_1006[i].f0", print_hash_value);
        transparent_crc(g_1006[i].f1, "g_1006[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1053, "g_1053", print_hash_value);
    transparent_crc(g_1194.f0, "g_1194.f0", print_hash_value);
    transparent_crc(g_1194.f1, "g_1194.f1", print_hash_value);
    transparent_crc(g_1194.f2, "g_1194.f2", print_hash_value);
    transparent_crc(g_1194.f3, "g_1194.f3", print_hash_value);
    transparent_crc(g_1194.f4, "g_1194.f4", print_hash_value);
    transparent_crc(g_1194.f5, "g_1194.f5", print_hash_value);
    transparent_crc(g_1194.f6, "g_1194.f6", print_hash_value);
    transparent_crc(g_1194.f7, "g_1194.f7", print_hash_value);
    transparent_crc(g_1194.f8, "g_1194.f8", print_hash_value);
    transparent_crc(g_1194.f9, "g_1194.f9", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1204[i][j][k], "g_1204[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1250.f0, "g_1250.f0", print_hash_value);
    transparent_crc(g_1250.f1, "g_1250.f1", print_hash_value);
    transparent_crc(g_1250.f2, "g_1250.f2", print_hash_value);
    transparent_crc(g_1250.f3, "g_1250.f3", print_hash_value);
    transparent_crc(g_1254.f0, "g_1254.f0", print_hash_value);
    transparent_crc(g_1254.f1, "g_1254.f1", print_hash_value);
    transparent_crc(g_1254.f2, "g_1254.f2", print_hash_value);
    transparent_crc(g_1254.f3, "g_1254.f3", print_hash_value);
    transparent_crc(g_1254.f4, "g_1254.f4", print_hash_value);
    transparent_crc(g_1254.f5, "g_1254.f5", print_hash_value);
    transparent_crc(g_1254.f6, "g_1254.f6", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc_bytes(&g_1256[i][j][k], sizeof(g_1256[i][j][k]), "g_1256[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1260[i].f0, "g_1260[i].f0", print_hash_value);
        transparent_crc(g_1260[i].f1, "g_1260[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1363, "g_1363", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 304
   depth: 1, occurrence: 37
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 20
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 3
XXX volatile bitfields defined in structs: 6
XXX structs with bitfields in the program: 68
breakdown:
   indirect level: 0, occurrence: 37
   indirect level: 1, occurrence: 19
   indirect level: 2, occurrence: 4
   indirect level: 3, occurrence: 7
   indirect level: 4, occurrence: 1
XXX full-bitfields structs in the program: 19
breakdown:
   indirect level: 0, occurrence: 19
XXX times a bitfields struct's address is taken: 42
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 57
XXX times a single bitfield on LHS: 20
XXX times a single bitfield on RHS: 86

XXX max expression depth: 33
breakdown:
   depth: 1, occurrence: 90
   depth: 2, occurrence: 15
   depth: 4, occurrence: 1
   depth: 14, occurrence: 1
   depth: 16, occurrence: 3
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 3
   depth: 24, occurrence: 1
   depth: 26, occurrence: 1
   depth: 33, occurrence: 1

XXX total number of pointers: 353

XXX times a variable address is taken: 852
XXX times a pointer is dereferenced on RHS: 89
breakdown:
   depth: 1, occurrence: 66
   depth: 2, occurrence: 20
   depth: 3, occurrence: 3
XXX times a pointer is dereferenced on LHS: 184
breakdown:
   depth: 1, occurrence: 176
   depth: 2, occurrence: 8
XXX times a pointer is compared with null: 20
XXX times a pointer is compared with address of another variable: 8
XXX times a pointer is compared with another pointer: 7
XXX times a pointer is qualified to be dereferenced: 4055

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 582
   level: 2, occurrence: 124
   level: 3, occurrence: 29
   level: 4, occurrence: 1
XXX number of pointers point to pointers: 134
XXX number of pointers point to scalars: 196
XXX number of pointers point to structs: 23
XXX percent of pointers has null in alias set: 26.9
XXX average alias set size: 1.41

XXX times a non-volatile is read: 956
XXX times a non-volatile is write: 512
XXX times a volatile is read: 63
XXX    times read thru a pointer: 9
XXX times a volatile is write: 28
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 2.86e+03
XXX percentage of non-volatile access: 94.2

XXX forward jumps: 1
XXX backward jumps: 6

XXX stmts: 79
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 24
   depth: 1, occurrence: 20
   depth: 2, occurrence: 15
   depth: 3, occurrence: 5
   depth: 4, occurrence: 6
   depth: 5, occurrence: 9

XXX percentage a fresh-made variable is used: 19.1
XXX percentage an existing variable is used: 80.9
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2717605387
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   unsigned f0 : 4;
   volatile unsigned f1 : 11;
   const unsigned f2 : 2;
   const unsigned f3 : 5;
   unsigned f4 : 2;
   signed f5 : 19;
   volatile unsigned f6 : 6;
   unsigned f7 : 1;
   const unsigned : 0;
};
#pragma pack(pop)

struct S1 {
   uint32_t  f0;
};

union U2 {
   const volatile int16_t  f0;
   int32_t  f1;
   unsigned f2 : 18;
   int8_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0xF0BC5021L;
static int32_t g_5 = 0x443AC46DL;
static float g_9[3][2][6] = {{{0x1.2p+1,(-0x1.1p-1),0x0.Fp+1,(-0x1.1p-1),0x1.2p+1,0x0.6p-1},{0xD.3BED16p-68,0x0.6p-1,(-0x2.Dp+1),0x1.2p+1,0x2.4p-1,0x5.64C7B7p-57}},{{0xD.94E8FEp+6,0xA.07B304p+65,0x2.4p-1,0x0.6p-1,0x5.64C7B7p-57,0x5.64C7B7p-57},{(-0x1.9p+1),(-0x2.Dp+1),(-0x2.Dp+1),(-0x1.9p+1),0x2.0B50F6p+17,0x0.6p-1}},{{0x5.64C7B7p-57,0x8.ACE4B0p-82,0x0.Fp+1,0x3.4DE6F8p-13,(-0x1.1p-1),0x2.4p-1},{0x0.Fp+1,0xD.94E8FEp+6,0x0.Fp+1,0xA.07B304p+65,(-0x1.1p-1),0xA.07B304p+65}}};
static float * volatile g_8 = &g_9[2][1][1];/* VOLATILE GLOBAL g_8 */
static uint64_t g_23[9][3][1] = {{{18446744073709551606UL},{0x352FB29D945AD33DLL},{0x352FB29D945AD33DLL}},{{18446744073709551606UL},{18446744073709551606UL},{18446744073709551606UL}},{{0x352FB29D945AD33DLL},{0x352FB29D945AD33DLL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL},{18446744073709551615UL}},{{18446744073709551615UL},{18446744073709551606UL},{0x352FB29D945AD33DLL}},{{18446744073709551606UL},{18446744073709551615UL},{18446744073709551615UL}},{{18446744073709551606UL},{0x352FB29D945AD33DLL},{18446744073709551606UL}},{{18446744073709551615UL},{18446744073709551615UL},{18446744073709551606UL}},{{0x352FB29D945AD33DLL},{18446744073709551606UL},{18446744073709551615UL}}};
static uint64_t g_33 = 0UL;
static uint64_t * volatile g_32 = &g_33;/* VOLATILE GLOBAL g_32 */
static struct S0 g_61 = {3,3,1,2,0,720,5,0};/* VOLATILE GLOBAL g_61 */
static struct S0 *g_60 = &g_61;
static uint16_t g_69 = 3UL;
static uint8_t g_82 = 5UL;
static int64_t g_86 = 0xE7D42A80B09DE24ELL;
static uint32_t g_101[2][7][1] = {{{0xA1655ED6L},{0xA3C451EDL},{0xA3C451EDL},{0xA1655ED6L},{0xA3C451EDL},{0xA3C451EDL},{0xA1655ED6L}},{{0xA3C451EDL},{0xA3C451EDL},{0xA1655ED6L},{0xA3C451EDL},{0xA3C451EDL},{0xA1655ED6L},{0xA3C451EDL}}};
static int16_t g_103 = 0x7B08L;
static int32_t g_105 = 0x0DC3605AL;
static const struct S1 g_110 = {18446744073709551608UL};
static uint64_t g_116 = 0x753CE469C6BC5226LL;
static struct S1 g_119 = {0x3651BE0EL};
static volatile int32_t g_121[7] = {0x102B4660L,0x102B4660L,0x102B4660L,0x102B4660L,0x102B4660L,0x102B4660L,0x102B4660L};
static volatile int32_t *g_120 = &g_121[6];
static volatile int32_t ** volatile g_123 = (void*)0;/* VOLATILE GLOBAL g_123 */
static volatile int32_t ** volatile g_124 = &g_120;/* VOLATILE GLOBAL g_124 */
static volatile union U2 g_133 = {-1L};/* VOLATILE GLOBAL g_133 */
static int8_t g_143[9] = {0x94L,0x94L,0x94L,0x94L,0x94L,0x94L,0x94L,0x94L,0x94L};
static volatile union U2 g_158 = {0L};/* VOLATILE GLOBAL g_158 */
static float **g_177 = (void*)0;
static float *** volatile g_176 = &g_177;/* VOLATILE GLOBAL g_176 */
static int32_t g_209 = 1L;
static volatile int32_t g_220[8] = {0xB22C6849L,0xB22C6849L,0xB22C6849L,0xB22C6849L,0xB22C6849L,0xB22C6849L,0xB22C6849L,0xB22C6849L};
static float ***g_241 = &g_177;
static float ****g_240[1][3] = {{&g_241,&g_241,&g_241}};
static struct S1 * volatile * const  volatile g_296 = (void*)0;/* VOLATILE GLOBAL g_296 */
static volatile union U2 *g_298 = &g_158;
static volatile union U2 ** volatile g_297 = &g_298;/* VOLATILE GLOBAL g_297 */
static int32_t *g_310 = &g_105;
static int32_t ** volatile g_309 = &g_310;/* VOLATILE GLOBAL g_309 */
static const int64_t g_342 = 0xD580E14417E8B6F4LL;
static float g_355[5] = {0x7.65B70Bp-48,0x7.65B70Bp-48,0x7.65B70Bp-48,0x7.65B70Bp-48,0x7.65B70Bp-48};
static int32_t ** volatile g_360 = &g_310;/* VOLATILE GLOBAL g_360 */
static int8_t g_371 = 0x5DL;
static volatile uint16_t g_374 = 5UL;/* VOLATILE GLOBAL g_374 */
static int64_t g_404[6] = {0x64B9C2C9F82D43B1LL,0x64B9C2C9F82D43B1LL,0x5B4DA3F98A702B2DLL,0x64B9C2C9F82D43B1LL,0x64B9C2C9F82D43B1LL,0x5B4DA3F98A702B2DLL};
static volatile float g_409 = 0xE.75F007p+7;/* VOLATILE GLOBAL g_409 */
static uint16_t g_450 = 1UL;
static uint16_t * const *g_454 = (void*)0;
static float * volatile * volatile * volatile ** volatile g_465 = (void*)0;/* VOLATILE GLOBAL g_465 */
static int16_t g_476 = (-7L);
static volatile union U2 ** volatile g_479 = &g_298;/* VOLATILE GLOBAL g_479 */
static struct S1 g_482 = {0x6F56433BL};
static struct S1 * volatile g_481 = &g_482;/* VOLATILE GLOBAL g_481 */
static int32_t ** const  volatile g_484 = &g_310;/* VOLATILE GLOBAL g_484 */
static int16_t g_508 = (-1L);
static union U2 g_548 = {0x5055L};/* VOLATILE GLOBAL g_548 */
static int32_t ** volatile g_559[1] = {&g_310};
static int32_t ** volatile g_560 = &g_310;/* VOLATILE GLOBAL g_560 */
static volatile union U2 g_574 = {0x502EL};/* VOLATILE GLOBAL g_574 */
static volatile struct S1 g_596 = {0x77188E42L};/* VOLATILE GLOBAL g_596 */
static volatile struct S1 *g_595 = &g_596;
static volatile struct S1 **g_594 = &g_595;
static int32_t ** volatile g_642 = &g_310;/* VOLATILE GLOBAL g_642 */
static const struct S1 * const g_654[7][4] = {{&g_119,&g_110,(void*)0,&g_482},{&g_482,(void*)0,(void*)0,&g_482},{&g_119,&g_119,&g_119,&g_110},{&g_119,&g_110,&g_110,&g_110},{&g_110,&g_110,(void*)0,&g_110},{(void*)0,&g_110,&g_110,&g_110},{&g_119,&g_119,&g_110,&g_482}};
static const struct S1 * const *g_653 = &g_654[1][2];
static const struct S1 g_658 = {0xC0885419L};
static const volatile int16_t g_668 = 0x9666L;/* VOLATILE GLOBAL g_668 */
static uint64_t *g_678 = (void*)0;
static uint64_t **g_677 = &g_678;
static const float g_687 = 0x1.2D8D1Cp-80;
static const float g_689 = 0x0.AFCA77p+73;
static const float *g_688 = &g_689;
static struct S0 ** volatile g_737 = &g_60;/* VOLATILE GLOBAL g_737 */
static int64_t *g_891 = &g_86;
static int64_t * volatile * volatile g_890 = &g_891;/* VOLATILE GLOBAL g_890 */
static int64_t * const *g_954 = (void*)0;
static int64_t * const **g_953 = &g_954;
static const union U2 g_968[3] = {{0xDC72L},{0xDC72L},{0xDC72L}};
static volatile union U2 g_970 = {7L};/* VOLATILE GLOBAL g_970 */
static volatile uint32_t g_994[9][6] = {{4294967295UL,4294967288UL,4294967288UL,4294967295UL,1UL,4294967295UL},{4294967295UL,4294967295UL,4294967289UL,4294967295UL,4294967295UL,4294967295UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967295UL,1UL,4294967295UL,1UL,4294967295UL},{1UL,4294967288UL,4294967295UL,4294967289UL,4294967289UL,4294967295UL},{1UL,1UL,4294967289UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,1UL,4294967288UL,4294967295UL,4294967289UL,4294967289UL},{4294967295UL,4294967288UL,4294967288UL,4294967295UL,1UL,4294967295UL},{4294967295UL,4294967295UL,4294967289UL,4294967295UL,4294967295UL,4294967295UL}};
static int16_t g_999[5][5] = {{0xDB11L,0xFE46L,0x12E5L,(-6L),0xFE46L},{(-4L),0x12E5L,0x12E5L,(-4L),(-6L)},{0xD46CL,(-4L),0xCF57L,0xFE46L,0xFE46L},{0xDB11L,(-4L),0xDB11L,(-6L),(-4L)},{0xFE46L,0x12E5L,(-6L),0xFE46L,(-6L)}};
static float g_1094 = (-0x1.5p+1);
static uint32_t g_1201 = 0UL;
static struct S0 ** volatile g_1213[3][8] = {{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60,&g_60}};
static struct S0 ** volatile g_1214 = &g_60;/* VOLATILE GLOBAL g_1214 */
static const struct S0 g_1262 = {0,1,1,2,1,-689,6,0};/* VOLATILE GLOBAL g_1262 */
static volatile int8_t g_1339[9][8][2] = {{{(-10L),0x95L},{0x78L,(-1L)},{0xDAL,0xC3L},{0x95L,8L},{0L,8L},{0x95L,0xC3L},{0xDAL,(-1L)},{0x78L,0x95L}},{{(-10L),0x78L},{(-6L),0L},{3L,1L},{(-10L),0L},{1L,(-1L)},{0L,(-1L)},{0x95L,0L},{8L,8L}},{{0L,(-1L)},{0xDAL,0xB2L},{1L,0x95L},{0L,1L},{(-6L),0xCBL},{(-6L),1L},{0L,0x95L},{1L,0xB2L}},{{0xDAL,(-1L)},{0L,8L},{8L,0L},{0x95L,(-1L)},{0L,(-1L)},{1L,0L},{(-10L),1L},{3L,0L}},{{(-6L),0x78L},{(-10L),0x95L},{0x78L,(-1L)},{0xDAL,0xC3L},{0x95L,8L},{0L,8L},{0x95L,0xC3L},{0xDAL,(-1L)}},{{0x78L,0x95L},{(-10L),0x78L},{(-6L),0L},{3L,1L},{(-10L),0L},{1L,(-1L)},{0L,(-1L)},{0x95L,0L}},{{8L,8L},{0L,(-1L)},{0xDAL,0xB2L},{1L,0x95L},{0L,1L},{(-6L),0xCBL},{(-6L),1L},{0L,0x95L}},{{1L,0xB2L},{0xDAL,(-1L)},{0L,8L},{8L,0L},{0x95L,(-1L)},{0L,(-1L)},{1L,0L},{(-10L),1L}},{{3L,0L},{(-6L),0x78L},{(-10L),0x95L},{0x78L,(-1L)},{0xDAL,0xC3L},{0x95L,8L},{0L,8L},{0x95L,0xC3L}}};
static struct S0 g_1372[6][10][1] = {{{{2,14,1,0,1,-449,7,0}},{{1,29,1,4,1,-696,2,0}},{{2,3,1,0,0,-206,1,0}},{{1,29,1,4,1,-696,2,0}},{{2,14,1,0,1,-449,7,0}},{{0,35,0,3,0,586,3,0}},{{2,14,1,0,1,-449,7,0}},{{1,29,1,4,1,-696,2,0}},{{2,3,1,0,0,-206,1,0}},{{1,29,1,4,1,-696,2,0}}},{{{2,14,1,0,1,-449,7,0}},{{0,35,0,3,0,586,3,0}},{{2,14,1,0,1,-449,7,0}},{{1,29,1,4,1,-696,2,0}},{{2,3,1,0,0,-206,1,0}},{{1,29,1,4,1,-696,2,0}},{{2,14,1,0,1,-449,7,0}},{{0,35,0,3,0,586,3,0}},{{2,14,1,0,1,-449,7,0}},{{1,29,1,4,1,-696,2,0}}},{{{2,3,1,0,0,-206,1,0}},{{1,29,1,4,1,-696,2,0}},{{2,14,1,0,1,-449,7,0}},{{0,35,0,3,0,586,3,0}},{{2,14,1,0,1,-449,7,0}},{{1,29,1,4,1,-696,2,0}},{{2,3,1,0,0,-206,1,0}},{{1,29,1,4,1,-696,2,0}},{{2,14,1,0,1,-449,7,0}},{{0,35,0,3,0,586,3,0}}},{{{2,14,1,0,1,-449,7,0}},{{1,29,1,4,1,-696,2,0}},{{2,3,1,0,0,-206,1,0}},{{0,35,0,3,0,586,3,0}},{{2,3,1,0,0,-206,1,0}},{{0,16,1,0,1,486,0,0}},{{2,3,1,0,0,-206,1,0}},{{0,35,0,3,0,586,3,0}},{{1,27,0,4,0,214,6,0}},{{0,35,0,3,0,586,3,0}}},{{{2,3,1,0,0,-206,1,0}},{{0,16,1,0,1,486,0,0}},{{2,3,1,0,0,-206,1,0}},{{0,35,0,3,0,586,3,0}},{{1,27,0,4,0,214,6,0}},{{0,35,0,3,0,586,3,0}},{{2,3,1,0,0,-206,1,0}},{{0,16,1,0,1,486,0,0}},{{2,3,1,0,0,-206,1,0}},{{0,35,0,3,0,586,3,0}}},{{{1,27,0,4,0,214,6,0}},{{0,35,0,3,0,586,3,0}},{{2,3,1,0,0,-206,1,0}},{{0,16,1,0,1,486,0,0}},{{2,3,1,0,0,-206,1,0}},{{0,35,0,3,0,586,3,0}},{{1,27,0,4,0,214,6,0}},{{0,35,0,3,0,586,3,0}},{{2,3,1,0,0,-206,1,0}},{{0,16,1,0,1,486,0,0}}}};
static uint32_t g_1409 = 0x7B013DA6L;
static int32_t g_1426 = (-5L);
static union U2 g_1461 = {0xA048L};/* VOLATILE GLOBAL g_1461 */
static int32_t ** volatile g_1472 = &g_310;/* VOLATILE GLOBAL g_1472 */
static struct S1 *g_1496 = &g_482;
static struct S1 **g_1495 = &g_1496;
static uint32_t g_1501[8] = {0x5A80F139L,0x5A80F139L,0x5A80F139L,0x5A80F139L,0x5A80F139L,0x5A80F139L,0x5A80F139L,0x5A80F139L};
static uint8_t g_1515 = 0UL;
static struct S0 g_1534 = {0,30,1,3,1,-181,4,0};/* VOLATILE GLOBAL g_1534 */
static float g_1545 = 0xD.F1F3BFp-75;
static volatile int32_t g_1555 = 0x637F1B3DL;/* VOLATILE GLOBAL g_1555 */
static struct S1 ***g_1562 = (void*)0;
static struct S1 ****g_1561 = &g_1562;
static struct S1 ***** volatile g_1560 = &g_1561;/* VOLATILE GLOBAL g_1560 */
static uint8_t g_1612 = 252UL;
static volatile int8_t g_1626 = 0xD2L;/* VOLATILE GLOBAL g_1626 */
static union U2 g_1627 = {0xBBBCL};/* VOLATILE GLOBAL g_1627 */
static int32_t ** volatile g_1653 = &g_310;/* VOLATILE GLOBAL g_1653 */
static volatile union U2 g_1681 = {2L};/* VOLATILE GLOBAL g_1681 */
static uint32_t g_1682 = 4294967291UL;
static struct S0 g_1733 = {1,34,0,1,0,-122,1,0};/* VOLATILE GLOBAL g_1733 */
static int64_t g_1743 = 0x8B0960C8F6278910LL;
static int64_t g_1745 = 1L;
static volatile union U2 g_1748 = {-2L};/* VOLATILE GLOBAL g_1748 */
static int16_t *g_1763 = (void*)0;
static int16_t *g_1764 = (void*)0;
static int32_t g_1780 = 1L;
static volatile struct S0 g_1781 = {1,8,0,1,1,85,4,0};/* VOLATILE GLOBAL g_1781 */
static const int16_t g_1824[7][8] = {{6L,0x2558L,1L,(-4L),1L,0x2558L,6L,0L},{1L,0x2558L,6L,0L,0x1AD3L,0x1AD3L,0L,6L},{0x78B6L,0x78B6L,0x2558L,0x3F15L,0x1AD3L,(-4L),0xAE64L,(-4L)},{1L,6L,0x3F15L,6L,1L,0x9B31L,0x78B6L,(-4L)},{6L,0x1AD3L,0xAE64L,0x3F15L,0x3F15L,0xAE64L,0x1AD3L,6L},{0x2558L,0x9B31L,0xAE64L,0L,0x78B6L,1L,0x78B6L,0L},{0x3F15L,(-7L),0x3F15L,(-4L),0L,1L,0xAE64L,0xAE64L}};
static struct S0 g_1845 = {0,21,0,1,1,77,1,0};/* VOLATILE GLOBAL g_1845 */
static int16_t ***g_1847 = (void*)0;
static int32_t ** volatile g_1880 = &g_310;/* VOLATILE GLOBAL g_1880 */
static int32_t ** volatile g_1881 = &g_310;/* VOLATILE GLOBAL g_1881 */
static int32_t ** volatile g_1895 = &g_310;/* VOLATILE GLOBAL g_1895 */
static int32_t g_1978[6] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
static struct S0 g_2015 = {0,11,1,3,0,-231,2,0};/* VOLATILE GLOBAL g_2015 */
static volatile struct S0 g_2037 = {0,31,1,2,1,75,2,0};/* VOLATILE GLOBAL g_2037 */
static int32_t ** volatile g_2047 = (void*)0;/* VOLATILE GLOBAL g_2047 */
static int32_t ** volatile g_2048 = &g_310;/* VOLATILE GLOBAL g_2048 */
static volatile int32_t *g_2050 = &g_1555;
static volatile int32_t ** volatile g_2051 = (void*)0;/* VOLATILE GLOBAL g_2051 */
static volatile int32_t ** volatile g_2052 = &g_120;/* VOLATILE GLOBAL g_2052 */
static uint8_t g_2070 = 1UL;
static union U2 g_2073[4] = {{0L},{0L},{0L},{0L}};
static struct S0 g_2086 = {2,22,0,3,0,-522,2,0};/* VOLATILE GLOBAL g_2086 */
static union U2 g_2107 = {-2L};/* VOLATILE GLOBAL g_2107 */
static int32_t * const g_2111 = &g_5;
static uint32_t *g_2131 = &g_482.f0;
static struct S0 g_2133 = {1,2,1,4,0,198,0,0};/* VOLATILE GLOBAL g_2133 */
static struct S0 g_2134 = {1,26,1,0,1,-492,5,0};/* VOLATILE GLOBAL g_2134 */
static struct S0 g_2135[9] = {{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0},{2,25,1,2,1,-244,6,0}};
static struct S0 g_2137 = {2,14,1,0,0,432,2,0};/* VOLATILE GLOBAL g_2137 */
static volatile uint8_t * volatile * volatile g_2154 = (void*)0;/* VOLATILE GLOBAL g_2154 */
static uint8_t *g_2157 = &g_82;
static uint8_t **g_2156[9][7][4] = {{{&g_2157,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157}},{{(void*)0,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157}},{{(void*)0,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157},{(void*)0,&g_2157,&g_2157,&g_2157}},{{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157}},{{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157}},{{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157}},{{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157}},{{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157}},{{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157},{&g_2157,&g_2157,(void*)0,&g_2157},{&g_2157,&g_2157,&g_2157,&g_2157}}};
static int32_t ** volatile g_2241 = &g_310;/* VOLATILE GLOBAL g_2241 */
static uint32_t g_2270[6][6][6] = {{{7UL,4294967295UL,0xDAFF8DDAL,0xDAFF8DDAL,4294967295UL,7UL},{0x945DE88EL,7UL,1UL,4294967295UL,1UL,7UL},{1UL,0x945DE88EL,0xDAFF8DDAL,4294967292UL,4294967292UL,0xDAFF8DDAL},{1UL,1UL,1UL,0xDAFF8DDAL,7UL,0xDAFF8DDAL},{4294967295UL,1UL,4294967295UL,4294967292UL,1UL,1UL},{0x945DE88EL,4294967295UL,4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL}},{{0xDAFF8DDAL,0x945DE88EL,1UL,0x945DE88EL,0xDAFF8DDAL,4294967292UL},{0x945DE88EL,0xDAFF8DDAL,4294967292UL,4294967292UL,0xDAFF8DDAL,0x945DE88EL},{4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL,1UL,0x945DE88EL},{1UL,4294967295UL,4294967292UL,1UL,1UL,4294967292UL},{1UL,1UL,1UL,0xDAFF8DDAL,7UL,0xDAFF8DDAL},{4294967295UL,1UL,4294967295UL,4294967292UL,1UL,1UL}},{{0x945DE88EL,4294967295UL,4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL},{0xDAFF8DDAL,0x945DE88EL,1UL,0x945DE88EL,0xDAFF8DDAL,4294967292UL},{0x945DE88EL,0xDAFF8DDAL,4294967292UL,4294967292UL,0xDAFF8DDAL,0x945DE88EL},{4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL,1UL,0x945DE88EL},{1UL,4294967295UL,4294967292UL,1UL,1UL,4294967292UL},{1UL,1UL,1UL,0xDAFF8DDAL,7UL,0xDAFF8DDAL}},{{4294967295UL,1UL,4294967295UL,4294967292UL,1UL,1UL},{0x945DE88EL,4294967295UL,4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL},{0xDAFF8DDAL,0x945DE88EL,1UL,0x945DE88EL,0xDAFF8DDAL,4294967292UL},{0x945DE88EL,0xDAFF8DDAL,4294967292UL,4294967292UL,0xDAFF8DDAL,0x945DE88EL},{4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL,1UL,0x945DE88EL},{1UL,4294967295UL,4294967292UL,1UL,1UL,4294967292UL}},{{1UL,1UL,1UL,0xDAFF8DDAL,7UL,0xDAFF8DDAL},{4294967295UL,1UL,4294967295UL,4294967292UL,1UL,1UL},{0x945DE88EL,4294967295UL,4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL},{0xDAFF8DDAL,0x945DE88EL,1UL,0x945DE88EL,0xDAFF8DDAL,4294967292UL},{0x945DE88EL,0xDAFF8DDAL,4294967292UL,4294967292UL,0xDAFF8DDAL,0x945DE88EL},{4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL,1UL,0x945DE88EL}},{{1UL,4294967295UL,4294967292UL,1UL,1UL,4294967292UL},{1UL,1UL,1UL,0xDAFF8DDAL,7UL,0xDAFF8DDAL},{4294967295UL,1UL,4294967295UL,4294967292UL,1UL,1UL},{0x945DE88EL,4294967295UL,4294967295UL,0x945DE88EL,1UL,0xDAFF8DDAL},{0xDAFF8DDAL,0x945DE88EL,1UL,0x945DE88EL,0xDAFF8DDAL,4294967292UL},{0x945DE88EL,4294967292UL,1UL,1UL,4294967292UL,4294967295UL}}};
static int32_t ** volatile g_2287 = &g_310;/* VOLATILE GLOBAL g_2287 */
static int8_t *g_2289 = (void*)0;
static int8_t * volatile * volatile g_2288[1][4][3] = {{{(void*)0,&g_2289,(void*)0},{&g_2289,&g_2289,&g_2289},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}}};
static int8_t * volatile * volatile * volatile g_2290 = &g_2288[0][3][1];/* VOLATILE GLOBAL g_2290 */
static volatile struct S1 * volatile g_2310[9] = {&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596};
static uint32_t g_2334 = 0xA3140BC7L;
static union U2 g_2338 = {-1L};/* VOLATILE GLOBAL g_2338 */
static int32_t g_2341 = 0x77A4A058L;
static volatile int32_t *g_2342 = (void*)0;
static struct S0 g_2345 = {2,18,1,4,1,159,5,0};/* VOLATILE GLOBAL g_2345 */
static const union U2 *g_2360 = (void*)0;
static const union U2 ** volatile g_2359 = &g_2360;/* VOLATILE GLOBAL g_2359 */
static volatile int32_t ** volatile g_2416 = &g_2050;/* VOLATILE GLOBAL g_2416 */
static volatile int16_t g_2447 = 0x655CL;/* VOLATILE GLOBAL g_2447 */
static int32_t ** volatile g_2461 = &g_310;/* VOLATILE GLOBAL g_2461 */
static int8_t **g_2465 = &g_2289;
static int8_t ***g_2464 = &g_2465;
static int32_t ** volatile g_2498 = (void*)0;/* VOLATILE GLOBAL g_2498 */
static int32_t ** const  volatile g_2499 = &g_310;/* VOLATILE GLOBAL g_2499 */
static volatile union U2 *g_2504 = &g_1681;
static volatile struct S0 g_2524 = {1,22,0,1,0,-189,1,0};/* VOLATILE GLOBAL g_2524 */
static volatile struct S0 g_2525 = {0,1,1,0,1,-361,4,0};/* VOLATILE GLOBAL g_2525 */
static volatile uint64_t g_2566 = 0UL;/* VOLATILE GLOBAL g_2566 */
static volatile int32_t g_2610 = 0x2DAADDDEL;/* VOLATILE GLOBAL g_2610 */
static union U2 g_2648 = {1L};/* VOLATILE GLOBAL g_2648 */
static union U2 g_2664 = {0x13E2L};/* VOLATILE GLOBAL g_2664 */
static int64_t g_2775 = (-1L);
static int32_t ** volatile g_2788[1] = {&g_310};
static uint16_t g_2810 = 0xCAA1L;
static uint16_t g_2820 = 0x8BD5L;
static float g_2904 = 0x1.F0A1E8p-34;
static struct S0 **g_2957 = &g_60;
static struct S0 ***g_2956[1][5] = {{&g_2957,&g_2957,&g_2957,&g_2957,&g_2957}};
static const uint8_t g_2978 = 0xD3L;
static const uint8_t *g_2977 = &g_2978;
static const uint8_t **g_2976[2][1][8] = {{{&g_2977,&g_2977,&g_2977,&g_2977,&g_2977,&g_2977,&g_2977,&g_2977}},{{&g_2977,&g_2977,&g_2977,&g_2977,&g_2977,&g_2977,&g_2977,&g_2977}}};
static int32_t g_3046 = 0x0AAA8CBCL;
static const struct S0 g_3069 = {1,6,1,1,1,418,1,0};/* VOLATILE GLOBAL g_3069 */
static volatile uint64_t g_3246 = 0x0464B80DC3E13F96LL;/* VOLATILE GLOBAL g_3246 */
static volatile uint32_t g_3262 = 18446744073709551608UL;/* VOLATILE GLOBAL g_3262 */
static union U2 **g_3330 = (void*)0;
static union U2 *g_3335 = (void*)0;
static union U2 **g_3334 = &g_3335;
static union U2 **g_3336 = (void*)0;
static union U2 g_3338 = {0x197BL};/* VOLATILE GLOBAL g_3338 */
static union U2 g_3340[3] = {{0x7C23L},{0x7C23L},{0x7C23L}};
static struct S0 g_3342 = {2,16,0,1,1,4,7,0};/* VOLATILE GLOBAL g_3342 */
static const volatile uint16_t *g_3381 = &g_374;
static const volatile uint16_t **g_3380 = &g_3381;
static const volatile uint16_t ** volatile *g_3379 = &g_3380;
static const volatile uint16_t ** volatile **g_3378 = &g_3379;
static const volatile uint16_t ** volatile ***g_3377 = &g_3378;
static volatile float g_3403[4][9][3] = {{{0x0.Cp+1,(-0x7.7p-1),0x9.ECA2D9p-17},{0xC.77AA59p+97,0x3.DC6EB4p-98,(-0x2.3p-1)},{0x9.ECA2D9p-17,0xA.0284D8p+44,0xA.0284D8p+44},{0x0.C12215p+79,0x2.89991Ap-80,(-0x3.4p-1)},{0xF.77F3F5p+13,0xE.0BA454p+4,0x0.7191E9p-44},{0xE.0BA454p+4,0x4.2E21E2p+51,0xE.AB26B8p+26},{0xA.0284D8p+44,(-0x1.Cp+1),0x7.A9914Dp-0},{0x7.A9914Dp-0,0x4.2E21E2p+51,0x1.8p-1},{0x8.6F85C8p-37,0xE.0BA454p+4,0xA.59DF21p-84}},{{0x0.Ep-1,0x2.89991Ap-80,(-0x1.Cp+1)},{0xC.5EFE8Bp-0,0xA.0284D8p+44,0xF.77F3F5p+13},{(-0x10.8p-1),0x3.DC6EB4p-98,(-0x5.0p-1)},{0xC.9946F7p+36,(-0x7.7p-1),0x2.7BFAF1p-81},{(-0x8.Fp-1),0xA.59DF21p-84,0x2.7BFAF1p-81},{0x0.0p+1,0x0.Ep-1,(-0x5.0p-1)},{0x2.89991Ap-80,0xD.8A6C59p-54,0xF.77F3F5p+13},{(-0x1.Cp+1),0x1.8p-1,(-0x1.Cp+1)},{(-0x1.4p+1),0xE.1A2744p-70,0xA.59DF21p-84}},{{0x2.7BFAF1p-81,0x9.ECA2D9p-17,0x1.8p-1},{0x1.7871B5p+16,0x8.6F85C8p-37,0x7.A9914Dp-0},{0x3.DC6EB4p-98,0xD.E6FDEBp+99,0xE.AB26B8p+26},{0x1.7871B5p+16,0x0.0p+1,0x0.7191E9p-44},{0x2.7BFAF1p-81,0x7.C206E8p+1,(-0x3.4p-1)},{(-0x1.4p+1),0xC.5EFE8Bp-0,0xA.0284D8p+44},{(-0x1.Cp+1),(-0x3.4p-1),(-0x2.3p-1)},{0x2.89991Ap-80,(-0x8.Fp-1),0x9.ECA2D9p-17},{0x0.0p+1,(-0x5.0p-1),(-0x1.2p+1)}},{{(-0x8.Fp-1),(-0x5.0p-1),(-0x1.4p+1)},{0xC.9946F7p+36,(-0x8.Fp-1),0x0.Ep-1},{(-0x10.8p-1),(-0x3.4p-1),0x0.7p+1},{0xC.5EFE8Bp-0,0xC.5EFE8Bp-0,0xC.9946F7p+36},{0x0.Ep-1,0x7.C206E8p+1,0xC.5EFE8Bp-0},{0x8.6F85C8p-37,0x0.0p+1,0xD.8A6C59p-54},{0x7.A9914Dp-0,0xD.E6FDEBp+99,0xE.1A2744p-70},{0xA.0284D8p+44,0x8.6F85C8p-37,0xD.8A6C59p-54},{0xE.0BA454p+4,0x9.ECA2D9p-17,0xC.5EFE8Bp-0}}};
static volatile float *g_3402 = &g_3403[1][3][0];
static const float g_3405 = (-0x4.Dp+1);
static const union U2 ** volatile g_3410 = &g_2360;/* VOLATILE GLOBAL g_3410 */
static volatile uint64_t g_3438 = 18446744073709551615UL;/* VOLATILE GLOBAL g_3438 */
static volatile union U2 g_3458[7][8][4] = {{{{0xE9AFL},{0x781CL},{0xCD07L},{0x4B90L}},{{-3L},{0xE38DL},{0x4714L},{0xD750L}},{{0x4714L},{0xD750L},{0x430EL},{-4L}},{{2L},{0x5923L},{-1L},{1L}},{{0x5923L},{0xE38DL},{1L},{-2L}},{{0x0A8BL},{0xE9AFL},{0x5F55L},{1L}},{{-3L},{0x1FF6L},{0x31A5L},{0x5923L}},{{-7L},{1L},{1L},{6L}}},{{{-1L},{0x4714L},{-1L},{-1L}},{{0x7355L},{0xF099L},{0L},{0xDC82L}},{{0x430EL},{0xCD07L},{-4L},{0xDC82L}},{{6L},{0xF099L},{0x2C01L},{-1L}},{{0x611AL},{0x4714L},{1L},{6L}},{{0x5F55L},{0x31A5L},{0L},{0x4714L}},{{-4L},{8L},{0x619BL},{0L}},{{0x430EL},{1L},{1L},{1L}}},{{{6L},{0x805BL},{-1L},{0x7355L}},{{6L},{0x4714L},{0x31A5L},{0x2C01L}},{{1L},{-4L},{0L},{-4L}},{{-4L},{0x805BL},{0x619BL},{0xDC82L}},{{-1L},{0x430EL},{-1L},{0L}},{{0x611AL},{0x0A8BL},{-2L},{0x7355L}},{{0x611AL},{0x31A5L},{-1L},{0x611AL}},{{-1L},{0x7355L},{0x619BL},{-1L}}},{{{-4L},{8L},{0L},{1L}},{{1L},{0xCD07L},{0x31A5L},{0xE38DL}},{{6L},{0x0A8BL},{-1L},{-1L}},{{6L},{-4L},{1L},{1L}},{{0x430EL},{0x31A5L},{0x619BL},{-4L}},{{-4L},{0xF099L},{0L},{0L}},{{0x5F55L},{0x5F55L},{1L},{0xE38DL}},{{0x611AL},{0x805BL},{0x2C01L},{0x4714L}}},{{{6L},{0x7355L},{-4L},{0x2C01L}},{{0x430EL},{0x7355L},{0L},{0x4714L}},{{0x7355L},{0x805BL},{-1L},{0xE38DL}},{{-1L},{0x5F55L},{0x31A5L},{0L}},{{6L},{0xF099L},{-2L},{-4L}},{{1L},{0x31A5L},{-4L},{1L}},{{-1L},{-4L},{0L},{-1L}},{{0x4714L},{0x0A8BL},{0L},{0xE38DL}}},{{{0x5F55L},{0xCD07L},{-1L},{1L}},{{6L},{8L},{6L},{-1L}},{{1L},{0x7355L},{1L},{0x611AL}},{{1L},{0x31A5L},{-1L},{0x7355L}},{{-4L},{0x0A8BL},{-1L},{0L}},{{1L},{0x430EL},{1L},{0xDC82L}},{{1L},{0x805BL},{6L},{-4L}},{{6L},{-4L},{-1L},{0x2C01L}}},{{{0x5F55L},{0x4714L},{0L},{0x7355L}},{{0x4714L},{0x805BL},{0L},{1L}},{{-1L},{1L},{-4L},{0L}},{{1L},{8L},{-2L},{0x4714L}},{{6L},{0x31A5L},{0x31A5L},{6L}},{{-1L},{0x4714L},{-1L},{-1L}},{{0x7355L},{0xF099L},{0L},{0xDC82L}},{{0x430EL},{0xCD07L},{0x4B90L},{-1L}}}};
static union U2 g_3463 = {0x32A8L};/* VOLATILE GLOBAL g_3463 */
static int64_t ****g_3472 = (void*)0;
static int32_t **g_3478 = (void*)0;
static int32_t ***g_3477[6] = {(void*)0,&g_3478,(void*)0,(void*)0,&g_3478,(void*)0};
static struct S1 *****g_3509 = &g_1561;
static int8_t g_3540 = 5L;
static int64_t * const ***g_3544[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int64_t * const ****g_3543[7] = {&g_3544[4],&g_3544[4],&g_3544[4],&g_3544[4],&g_3544[4],&g_3544[4],&g_3544[4]};
static const volatile struct S0 g_3554 = {3,15,1,4,1,308,0,0};/* VOLATILE GLOBAL g_3554 */
static uint8_t g_3571[1][6][7] = {{{252UL,0x2AL,255UL,0xC6L,0x61L,252UL,0xC6L},{0x5FL,1UL,252UL,0x2AL,0x2AL,252UL,1UL},{0x2AL,0x39L,255UL,0xB0L,0x39L,0xC6L,1UL},{0x72L,0x2AL,0xC6L,0x72L,1UL,0x72L,0xC6L},{0x61L,0x61L,0xFEL,0xB0L,0x2AL,0x5AL,0x61L},{0x61L,0xC6L,255UL,0x2AL,252UL,252UL,0x2AL}}};
static volatile struct S0 g_3597 = {2,31,1,4,0,14,7,0};/* VOLATILE GLOBAL g_3597 */
static uint8_t g_3646[6][7] = {{254UL,0xEBL,254UL,247UL,1UL,1UL,247UL},{254UL,0xEBL,254UL,247UL,1UL,1UL,247UL},{254UL,0xEBL,254UL,247UL,1UL,1UL,247UL},{254UL,0xEBL,254UL,247UL,1UL,1UL,247UL},{254UL,0xEBL,254UL,247UL,1UL,1UL,247UL},{254UL,0xEBL,254UL,247UL,1UL,1UL,247UL}};
static volatile union U2 g_3665 = {-1L};/* VOLATILE GLOBAL g_3665 */
static volatile union U2 g_3746 = {0L};/* VOLATILE GLOBAL g_3746 */
static struct S0 ** volatile *g_3852[8][10][2] = {{{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{(void*)0,&g_2957},{(void*)0,&g_1213[1][5]},{(void*)0,&g_2957},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[1][5]},{(void*)0,&g_2957},{(void*)0,&g_2957}},{{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{(void*)0,&g_2957},{(void*)0,&g_1213[1][5]},{(void*)0,&g_2957},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[1][5]}},{{(void*)0,&g_2957},{(void*)0,&g_2957},{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{(void*)0,&g_2957},{(void*)0,&g_1213[1][5]},{(void*)0,&g_2957}},{{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[1][5]},{(void*)0,&g_2957},{(void*)0,&g_2957},{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{(void*)0,&g_2957},{(void*)0,&g_2957}},{{(void*)0,&g_1213[1][5]},{(void*)0,&g_2957},{&g_1213[1][5],&g_2957},{(void*)0,&g_2957},{&g_1213[1][5],&g_1213[1][5]},{&g_2957,&g_1213[0][7]},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[1][5]},{&g_2957,&g_1213[1][5]},{(void*)0,&g_2957}},{{&g_1213[1][5],&g_1213[0][7]},{&g_2957,&g_1213[1][5]},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{&g_2957,&g_1213[0][7]},{(void*)0,&g_2957},{&g_1213[1][5],&g_1213[1][5]},{&g_2957,&g_1213[0][7]},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[1][5]}},{{&g_2957,&g_1213[1][5]},{(void*)0,&g_2957},{&g_1213[1][5],&g_1213[0][7]},{&g_2957,&g_1213[1][5]},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{&g_2957,&g_1213[0][7]},{(void*)0,&g_2957},{&g_1213[1][5],&g_1213[1][5]},{&g_2957,&g_1213[0][7]}},{{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[1][5]},{&g_2957,&g_1213[1][5]},{(void*)0,&g_2957},{&g_1213[1][5],&g_1213[0][7]},{&g_2957,&g_1213[1][5]},{&g_1213[1][5],&g_2957},{(void*)0,&g_1213[0][7]},{&g_2957,&g_1213[0][7]},{(void*)0,&g_2957}}};
static struct S0 ** volatile * volatile * volatile g_3851 = &g_3852[2][1][0];/* VOLATILE GLOBAL g_3851 */
static struct S0 ** volatile * volatile * volatile * volatile g_3850 = &g_3851;/* VOLATILE GLOBAL g_3850 */
static volatile uint8_t g_3908 = 0x59L;/* VOLATILE GLOBAL g_3908 */
static volatile uint8_t * volatile g_3907 = &g_3908;/* VOLATILE GLOBAL g_3907 */
static volatile uint8_t * const  volatile * volatile g_3906 = &g_3907;/* VOLATILE GLOBAL g_3906 */
static volatile uint8_t * const  volatile * volatile *g_3905 = &g_3906;
static volatile uint8_t * const  volatile * volatile **g_3904[2][7][6] = {{{(void*)0,&g_3905,&g_3905,&g_3905,(void*)0,&g_3905},{&g_3905,&g_3905,(void*)0,&g_3905,&g_3905,&g_3905},{&g_3905,&g_3905,&g_3905,&g_3905,(void*)0,&g_3905},{&g_3905,&g_3905,(void*)0,&g_3905,&g_3905,&g_3905},{&g_3905,&g_3905,&g_3905,&g_3905,(void*)0,&g_3905},{&g_3905,(void*)0,(void*)0,&g_3905,(void*)0,&g_3905},{&g_3905,(void*)0,&g_3905,&g_3905,&g_3905,&g_3905}},{{(void*)0,&g_3905,(void*)0,&g_3905,&g_3905,&g_3905},{&g_3905,(void*)0,&g_3905,(void*)0,(void*)0,&g_3905},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_3905},{&g_3905,&g_3905,&g_3905,&g_3905,&g_3905,(void*)0},{(void*)0,&g_3905,&g_3905,&g_3905,(void*)0,&g_3905},{&g_3905,&g_3905,(void*)0,&g_3905,&g_3905,&g_3905},{&g_3905,&g_3905,&g_3905,&g_3905,(void*)0,&g_3905}}};
static int32_t ** volatile g_3932 = &g_310;/* VOLATILE GLOBAL g_3932 */
static int32_t g_3972 = (-1L);
static int32_t * const g_3971 = &g_3972;
static int32_t * const *g_3970 = &g_3971;
static volatile union U2 ** volatile * volatile g_3980 = &g_297;/* VOLATILE GLOBAL g_3980 */
static volatile union U2 ** volatile * volatile * volatile g_3979[1][8][6] = {{{&g_3980,&g_3980,&g_3980,&g_3980,&g_3980,&g_3980},{&g_3980,&g_3980,&g_3980,(void*)0,&g_3980,&g_3980},{&g_3980,(void*)0,(void*)0,&g_3980,&g_3980,&g_3980},{&g_3980,&g_3980,&g_3980,&g_3980,(void*)0,&g_3980},{&g_3980,&g_3980,&g_3980,&g_3980,(void*)0,&g_3980},{&g_3980,&g_3980,&g_3980,&g_3980,&g_3980,&g_3980},{&g_3980,&g_3980,(void*)0,&g_3980,&g_3980,&g_3980},{&g_3980,(void*)0,&g_3980,&g_3980,&g_3980,(void*)0}}};
static const struct S1 *g_3984 = &g_482;
static const struct S1 ** const  volatile g_3983 = &g_3984;/* VOLATILE GLOBAL g_3983 */


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static struct S1  func_15(int32_t * const  p_16, float * p_17, int32_t * const  p_18, int16_t  p_19);
static int32_t * const  func_20(int32_t * p_21);
static uint8_t  func_29(uint32_t  p_30, const uint8_t  p_31);
static struct S0 * func_34(uint32_t  p_35, struct S1  p_36, const uint64_t * p_37, const int32_t * p_38);
static struct S1  func_40(uint16_t  p_41, float * p_42, int16_t  p_43);
static uint16_t  func_44(float * p_45, int32_t  p_46);
static uint64_t  func_50(uint32_t  p_51);
static int32_t  func_54(struct S0 * p_55, int16_t  p_56, const struct S1  p_57, uint32_t  p_58, struct S1  p_59);
static struct S1  func_62(uint64_t  p_63, int16_t  p_64, int32_t  p_65);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_8 g_2 g_2111 g_2504 g_1681 g_688 g_689 g_209 g_994 g_1682 g_3746 g_3380 g_3381 g_374 g_2820 g_890 g_891 g_86 g_310 g_105 g_371 g_3402 g_1560 g_1561 g_1562 g_1534.f7 g_2157 g_120 g_1895 g_3342.f2 g_2086.f1 g_1372.f7 g_2052 g_121 g_3850 g_360 g_69 g_61.f3 g_61.f1 g_61.f5 g_61.f4 g_101 g_33 g_103 g_61.f2 g_61.f6 g_32 g_3904 g_2341 g_2270 g_1495 g_1496 g_482 g_595 g_737 g_60 g_61 g_484 g_3932 g_594 g_2461 g_3971 g_3979 g_3983 g_1472 g_2050 g_82 g_3377 g_3378 g_2137.f7 g_1555
 * writes: g_2 g_5 g_9 g_103 g_355 g_1545 g_209 g_1461.f3 g_1682 g_2820 g_105 g_1094 g_3403 g_86 g_82 g_121 g_2341 g_2664.f1 g_69 g_61.f5 g_101 g_116 g_2270 g_596 g_310 g_1627.f1 g_3970 g_3972 g_3984 g_1555
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    float l_2797 = 0x0.Fp+1;
    int32_t l_2811 = 0x29C497C6L;
    int32_t l_3256 = 0x72F07933L;
    int16_t l_3257 = 1L;
    int32_t l_3258 = (-7L);
    int32_t l_3260 = 0x4FD9A283L;
    struct S1 *l_3265 = &g_119;
    union U2 *l_3337 = &g_3338;
    struct S0 **l_3347 = &g_60;
    int32_t l_3354 = (-10L);
    int32_t l_3356[8][10][3] = {{{0xD3C5F8EEL,2L,0x94FF3C5AL},{0L,0x8E770BAFL,0x8E770BAFL},{2L,0x94FF3C5AL,2L},{(-1L),0L,0x3A7F0F61L},{2L,3L,0xD4FF4F36L},{0xAE007E45L,5L,1L},{0x1D5E7843L,3L,0x32782FEBL},{2L,0L,0xAE007E45L},{0xB6544ADEL,0x94FF3C5AL,(-7L)},{(-1L),0x8E770BAFL,(-1L)}},{{0L,2L,(-1L)},{0x04298D3BL,0x3A7F0F61L,0xCEE86CF6L},{0x83FECB1BL,0xD4FF4F36L,(-1L)},{0x3A7F0F61L,1L,2L},{0x83FECB1BL,0x32782FEBL,(-10L)},{0x04298D3BL,0xAE007E45L,0x80D58B7CL},{0L,(-7L),0x1D5E7843L},{(-1L),(-1L),(-7L)},{0xB6544ADEL,(-1L),0x55310A26L},{2L,0xCEE86CF6L,0x188CF9ABL}},{{0x1D5E7843L,(-1L),0x7A59F123L},{0xAE007E45L,2L,0x188CF9ABL},{2L,(-10L),0x55310A26L},{(-1L),0x80D58B7CL,(-7L)},{2L,0x1D5E7843L,0x1D5E7843L},{0L,(-7L),0x80D58B7CL},{0xD3C5F8EEL,0x55310A26L,(-10L)},{0x80D58B7CL,0x188CF9ABL,2L},{(-1L),0x7A59F123L,(-1L)},{0x8E770BAFL,0x188CF9ABL,0xCEE86CF6L}},{{0xD4FF4F36L,0x55310A26L,(-1L)},{(-6L),(-7L),(-1L)},{(-7L),0x1D5E7843L,(-7L)},{0x4A8324ACL,0x80D58B7CL,0xAE007E45L},{(-3L),(-10L),0x32782FEBL},{0xFD88C69AL,2L,1L},{(-10L),(-1L),0xD4FF4F36L},{0xFD88C69AL,0xCEE86CF6L,0x3A7F0F61L},{(-3L),(-1L),2L},{0x4A8324ACL,(-1L),0x8E770BAFL}},{{(-7L),(-7L),0x94FF3C5AL},{(-6L),0xAE007E45L,0L},{0xD4FF4F36L,0x32782FEBL,3L},{0x8E770BAFL,1L,5L},{(-1L),0xD4FF4F36L,3L},{0x80D58B7CL,0x3A7F0F61L,0L},{0xD3C5F8EEL,2L,0x94FF3C5AL},{0L,0x8E770BAFL,0x8E770BAFL},{2L,0x94FF3C5AL,2L},{(-1L),0L,0x3A7F0F61L}},{{2L,3L,0xD4FF4F36L},{0xAE007E45L,5L,1L},{0x1D5E7843L,3L,0x32782FEBL},{2L,0L,0xAE007E45L},{0xB6544ADEL,0x94FF3C5AL,(-7L)},{(-1L),0x8E770BAFL,(-1L)},{0L,2L,(-1L)},{0x04298D3BL,0x3A7F0F61L,0xCEE86CF6L},{0x83FECB1BL,0xD4FF4F36L,0x94FF3C5AL},{0L,(-7L),0x04298D3BL}},{{0x817B2556L,0xD4FF4F36L,0x55310A26L},{0xAE007E45L,0x4A8324ACL,0x8E770BAFL},{(-10L),8L,0x7EE58E40L},{(-9L),(-9L),5L},{(-7L),0L,(-1L)},{0x04298D3BL,2L,(-6L)},{0x7EE58E40L,0x94FF3C5AL,0x83FECB1BL},{0x4A8324ACL,0x04298D3BL,(-6L)},{0x1D5E7843L,0x55310A26L,(-1L)},{0xCEE86CF6L,0x8E770BAFL,5L}},{{0x5FC37A61L,0x7EE58E40L,0x7EE58E40L},{0xB4821710L,5L,0x8E770BAFL},{0x32782FEBL,(-1L),0x55310A26L},{0x8E770BAFL,(-6L),0x04298D3BL},{0L,0x83FECB1BL,0x94FF3C5AL},{(-1L),(-6L),2L},{(-3L),(-1L),0L},{(-1L),5L,(-9L)},{8L,0x7EE58E40L,8L},{0x3A7F0F61L,0x8E770BAFL,0x4A8324ACL}}};
    int32_t l_3413 = (-6L);
    struct S1 ***l_3434 = (void*)0;
    int64_t **l_3495 = &g_891;
    uint8_t l_3499 = 247UL;
    uint32_t *l_3511[1];
    int64_t * const ****l_3545 = &g_3544[4];
    uint16_t *l_3551[9][2] = {{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69},{(void*)0,&g_69}};
    uint16_t **l_3550 = &l_3551[0][0];
    uint16_t ***l_3549 = &l_3550;
    uint16_t ****l_3548 = &l_3549;
    const float l_3565 = (-0x1.Ap-1);
    float l_3603 = (-0x1.8p-1);
    uint32_t l_3618 = 0x838E4F42L;
    int16_t l_3645 = 0xC9ECL;
    int8_t ** const *l_3709 = &g_2465;
    uint64_t l_3798 = 0x69B2D7EA9D3975E4LL;
    int64_t *****l_3874 = &g_3472;
    uint32_t l_4033[5][1][8] = {{{0x4CA2D030L,7UL,9UL,9UL,7UL,0x4CA2D030L,4294967295UL,0x4CA2D030L}},{{7UL,0x4CA2D030L,4294967295UL,0x4CA2D030L,7UL,9UL,9UL,7UL}},{{0x4CA2D030L,9UL,9UL,0x4CA2D030L,0UL,7UL,0UL,0x4CA2D030L}},{{9UL,0UL,9UL,9UL,4294967295UL,4294967295UL,9UL,9UL}},{{0UL,0UL,4294967295UL,7UL,1UL,7UL,4294967295UL,0UL}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_3511[i] = (void*)0;
lbl_3310:
    for (g_2 = 0; (g_2 > (-11)); g_2--)
    { /* block id: 3 */
        int32_t *l_12 = &g_5;
        int32_t l_2812 = 0x426A8F82L;
        int32_t l_3259 = 1L;
        int32_t l_3261 = 3L;
        for (g_5 = 0; (g_5 != 20); g_5 = safe_add_func_uint32_t_u_u(g_5, 4))
        { /* block id: 6 */
            (*g_8) = g_5;
        }
        for (g_5 = 0; (g_5 > 18); ++g_5)
        { /* block id: 11 */
            return g_5;
        }
        (*l_12) &= g_2;
        for (g_5 = 0; (g_5 >= (-11)); g_5--)
        { /* block id: 17 */
            float *l_2791 = &g_1094;
            int32_t **l_2792 = &l_12;
            int32_t l_2798 = 0xBFB6ACF7L;
            float *l_2808 = (void*)0;
            uint16_t *l_2809 = &g_2810;
            int32_t *l_3253 = &l_2798;
            int32_t *l_3254 = (void*)0;
            int32_t *l_3255[9];
            struct S1 **l_3266 = &g_1496;
            int i;
            for (i = 0; i < 9; i++)
                l_3255[i] = &g_2341;
        }
    }
    if (((*g_2111) = l_2811))
    { /* block id: 1428 */
        const union U2 **l_3286 = (void*)0;
        int32_t l_3293 = 0x1F2C8116L;
        int32_t l_3308 = 0x459D2392L;
        union U2 *l_3339 = &g_3340[0];
        int8_t l_3341 = 1L;
        int32_t l_3359 = 3L;
        int16_t l_3366 = (-1L);
        int8_t l_3376[8][8][3] = {{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}},{{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L},{(-4L),0x55L,(-4L)},{5L,5L,1L},{0xD9L,0x55L,0xD9L},{5L,1L,1L}}};
        uint16_t l_3404 = 9UL;
        uint16_t l_3408 = 4UL;
        int32_t **l_3475 = &g_310;
        int32_t ***l_3474 = &l_3475;
        uint16_t l_3479 = 0x8C31L;
        uint8_t *l_3516 = (void*)0;
        int64_t * const l_3538 = (void*)0;
        struct S0 ***l_3593 = &g_2957;
        int32_t l_3595 = 0L;
        int32_t l_3634 = 0x1BC5F19FL;
        int32_t l_3636[1][10][3] = {{{(-5L),(-1L),(-1L)},{0xA6A55F2EL,0x8E474607L,1L},{(-5L),0xAFBD5CF4L,(-5L)},{0xD0AEFA9CL,0xA6A55F2EL,1L},{0x1C93909AL,0x1C93909AL,(-1L)},{5L,0xA6A55F2EL,0xA6A55F2EL},{(-1L),0xAFBD5CF4L,(-3L)},{5L,0x8E474607L,5L},{0x1C93909AL,(-1L),(-3L)},{0xD0AEFA9CL,0xD0AEFA9CL,0xA6A55F2EL}}};
        int i, j, k;
        for (g_103 = 0; (g_103 <= 0); g_103 += 1)
        { /* block id: 1431 */
            float *l_3276 = &g_9[1][0][3];
            float *l_3294 = &g_355[0];
            const int32_t l_3300[2] = {0x1A9591FEL,0x1A9591FEL};
            float *l_3301 = (void*)0;
            float *l_3302 = &g_1545;
            float *l_3303 = &l_2797;
            float *l_3304[4] = {&g_2904,&g_2904,&g_2904,&g_2904};
            int32_t l_3305 = 0L;
            int32_t l_3306 = 0xB34D5438L;
            const uint8_t l_3307 = 9UL;
            int32_t l_3358[8][3] = {{4L,0x21597A1AL,4L},{0x2865E37CL,0x2865E37CL,(-5L)},{7L,0x21597A1AL,7L},{0x2865E37CL,(-5L),(-5L)},{4L,0x21597A1AL,4L},{0x2865E37CL,0x2865E37CL,(-5L)},{7L,0x21597A1AL,7L},{0x2865E37CL,(-5L),(-5L)}};
            uint8_t l_3363 = 254UL;
            float *l_3367[1];
            struct S0 *l_3384 = (void*)0;
            uint32_t l_3401 = 18446744073709551607UL;
            uint16_t l_3406 = 0xFED6L;
            int32_t **l_3407 = &g_310;
            float l_3445 = 0x0.CE3745p-78;
            uint32_t l_3446 = 0UL;
            int64_t **l_3494[9] = {&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891};
            struct S1 *****l_3508 = &g_1561;
            uint16_t ****l_3556 = &l_3549;
            int16_t *l_3564[4][6][8] = {{{&g_508,&l_3257,&l_3366,&g_999[4][0],&g_508,&l_3366,&g_103,&g_508},{&g_476,(void*)0,&g_103,&g_999[4][0],&g_476,&g_476,&g_999[3][2],(void*)0},{&g_508,&g_476,&g_999[3][0],&l_3257,(void*)0,&g_508,&g_999[3][2],&l_3366},{&g_476,(void*)0,&g_508,(void*)0,&g_508,&g_999[3][2],&g_476,&g_508},{&g_999[1][2],(void*)0,&l_3366,&g_999[1][2],(void*)0,&g_999[1][2],&g_999[3][2],&l_3366},{&g_508,(void*)0,&l_3366,&l_3366,(void*)0,&g_999[3][2],&g_508,&g_508}},{{&g_999[4][0],&g_508,&l_3366,&g_476,&l_3366,&g_508,&g_999[4][0],&l_3257},{&g_476,(void*)0,&l_3257,(void*)0,&g_999[1][2],&g_999[3][2],&l_3366,&l_3366},{&l_3257,&g_508,&g_508,&g_999[3][2],&g_999[1][2],&g_508,&g_999[3][2],&g_476},{&g_476,&l_3366,&g_999[3][2],&l_3366,&l_3366,&g_999[3][2],&g_476,&l_3366},{&g_999[4][0],(void*)0,&g_476,&g_999[3][2],(void*)0,&g_103,(void*)0,&g_999[3][2]},{&g_508,&l_3366,&g_508,&l_3257,(void*)0,&l_3366,&g_999[3][2],&g_476}},{{&g_999[1][2],(void*)0,(void*)0,(void*)0,&g_508,&l_3366,&g_476,&l_3257},{&g_476,&g_508,(void*)0,(void*)0,(void*)0,(void*)0,&g_508,&g_476},{&g_508,&g_999[4][0],&g_508,&g_508,&g_476,(void*)0,&g_999[3][0],&g_508},{&g_476,&g_999[3][2],&l_3366,&l_3366,&g_508,(void*)0,&g_476,&l_3366},{&g_508,&g_999[4][0],&g_999[0][2],&g_999[1][2],&l_3366,(void*)0,(void*)0,&g_103},{&l_3366,&g_508,&g_508,&g_999[3][0],&g_476,&l_3366,(void*)0,&l_3366}},{{&l_3366,(void*)0,&l_3257,&g_476,(void*)0,&l_3366,&g_999[4][0],&g_508},{&g_508,&l_3366,&g_999[4][4],&g_999[4][0],&g_476,&g_103,&g_103,(void*)0},{&g_999[3][2],(void*)0,&l_3366,&g_999[3][2],&g_476,&g_999[3][2],&l_3366,(void*)0},{&g_103,&l_3366,&g_999[3][0],&g_476,&g_508,&g_508,&g_476,(void*)0},{&g_476,&g_508,&l_3366,(void*)0,&g_103,&g_999[3][2],&g_476,&g_103},{&g_508,(void*)0,&g_999[3][0],&g_508,(void*)0,&g_999[4][4],&g_476,&g_508}}};
            int16_t l_3569 = (-3L);
            const int32_t l_3596 = 0x7E84D674L;
            uint16_t l_3624 = 1UL;
            int32_t *l_3625 = (void*)0;
            int32_t l_3632 = 0xFFBF7F31L;
            int8_t l_3644 = 0L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_3367[i] = &g_9[2][1][1];
            l_3258 = ((((*g_2504) , (safe_div_func_float_f_f((safe_add_func_float_f_f(0x7.440E08p+99, (safe_sub_func_float_f_f((+(((safe_sub_func_float_f_f(((((*l_3276) = 0x0.A4A140p+36) > (safe_add_func_float_f_f(((-(l_3306 = (l_3293 = (safe_mul_func_float_f_f(((l_3305 = ((((safe_sub_func_float_f_f(((safe_add_func_float_f_f(((*g_688) >= l_2811), ((*l_3303) = ((*l_3302) = (((void*)0 != l_3286) , (safe_sub_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f((((((*l_3294) = l_3293) >= ((safe_sub_func_float_f_f((safe_div_func_float_f_f((+((0x1.3p-1 == (-0x1.Ap-1)) != l_3256)), l_3258)), l_3300[1])) > l_3257)) <= 0xF.C992BBp+46) < l_3293), l_3256)), l_3258)), l_3300[0]))))))) == (*g_688)), l_3300[1])) == l_3300[1]) <= 0x6.572AB3p+62) > 0x4.B14F0Cp+94)) < 0x1.Cp+1), (-0x1.Bp+1)))))) < (*g_688)), (-0x3.1p+1)))) == (-0x1.0p+1)), l_3257)) <= l_3307) < 0x0.Bp+1)), 0x1.4C03E7p+96)))), l_3308))) == l_3308) >= l_3300[0]);
        }
        for (g_209 = 2; (g_209 >= 0); g_209 -= 1)
        { /* block id: 1604 */
            uint16_t l_3677 = 0xF633L;
            for (l_3634 = 0; (l_3634 >= 0); l_3634 -= 1)
            { /* block id: 1607 */
                int i, j;
                return g_994[(l_3634 + 6)][g_209];
            }
            l_3677++;
            for (g_1461.f3 = 0; (g_1461.f3 >= 0); g_1461.f3 -= 1)
            { /* block id: 1613 */
                float l_3696 = 0x1.Cp-1;
                int8_t ***l_3703[5] = {&g_2465,&g_2465,&g_2465,&g_2465,&g_2465};
                int32_t l_3715 = 0L;
                int32_t l_3716[5] = {1L,1L,1L,1L,1L};
                int i;
            }
        }
    }
    else
    { /* block id: 1634 */
        int8_t l_3726 = 0L;
        int32_t l_3751[7] = {0x9C7668BAL,0x0121F83BL,0x9C7668BAL,0x9C7668BAL,0x0121F83BL,0x9C7668BAL,0x9C7668BAL};
        const uint32_t l_3771[8] = {18446744073709551606UL,18446744073709551606UL,0x541E88FAL,18446744073709551606UL,18446744073709551606UL,0x541E88FAL,18446744073709551606UL,18446744073709551606UL};
        int32_t l_3818 = 0xB000F8A2L;
        int32_t l_3827 = 0L;
        int16_t l_3829 = 0L;
        int32_t l_3835 = 1L;
        int16_t l_3839[2];
        float l_3860[10][8][3] = {{{(-0x4.9p-1),0x0.7p-1,(-0x1.Dp+1)},{(-0x8.4p+1),0xC.CD11DCp-64,(-0x1.3p-1)},{0x9.516CEAp+30,0x1.B5A5E5p-30,(-0x1.0p+1)},{0xD.999CB5p+76,(-0x8.4p+1),(-0x1.3p-1)},{0x9.1624B3p-2,(-0x1.9p-1),(-0x1.Dp+1)},{0x7.FCA77Bp-52,0x1.Bp-1,0x0.A17C05p+55},{(-0x1.0p+1),0x0.BA0D37p+20,0xA.6FA0F5p-67},{0xD.999CB5p+76,0x7.FCA77Bp-52,(-0x4.Bp-1)}},{{0x7.Ep+1,0x0.BA0D37p+20,0x7.Ep+1},{(-0x8.4p+1),0x1.Bp-1,0xC.CD11DCp-64},{0x6.Dp-1,(-0x1.9p-1),0x9.516CEAp+30},{0xA.731AC7p-90,(-0x8.4p+1),(-0x4.Bp-1)},{0x3.1B0F82p-80,0x1.B5A5E5p-30,0x1.0p+1},{0xA.731AC7p-90,0xC.CD11DCp-64,0x0.A17C05p+55},{0x6.Dp-1,0x0.7p-1,0xE.3342DFp-85},{(-0x8.4p+1),0xA.731AC7p-90,(-0x1.3p-1)}},{{0x7.Ep+1,0x1.B5A5E5p-30,0x9.1624B3p-2},{0xD.999CB5p+76,(-0x1.3p-1),(-0x1.3p-1)},{(-0x1.0p+1),(-0x1.9p-1),0xE.3342DFp-85},{0x7.FCA77Bp-52,0xD.999CB5p+76,0x0.A17C05p+55},{0x9.1624B3p-2,0x0.BA0D37p+20,0x1.0p+1},{0xD.999CB5p+76,0xB.3380F2p+61,(-0x4.Bp-1)},{0x9.516CEAp+30,0x0.BA0D37p+20,0x9.516CEAp+30},{(-0x8.4p+1),0xD.999CB5p+76,0xC.CD11DCp-64}},{{(-0x4.9p-1),(-0x1.9p-1),0x7.Ep+1},{0xA.731AC7p-90,(-0x1.3p-1),(-0x4.Bp-1)},{0x1.Bp+1,0x1.B5A5E5p-30,0xA.6FA0F5p-67},{0xA.731AC7p-90,0xA.731AC7p-90,0x0.A17C05p+55},{(-0x4.9p-1),0x0.7p-1,(-0x1.Dp+1)},{(-0x8.4p+1),0xC.CD11DCp-64,(-0x1.3p-1)},{0x9.516CEAp+30,0x1.B5A5E5p-30,(-0x1.0p+1)},{0xD.999CB5p+76,(-0x8.4p+1),(-0x1.3p-1)}},{{0x9.1624B3p-2,(-0x1.9p-1),(-0x1.Dp+1)},{0x7.FCA77Bp-52,0x1.Bp-1,0x0.A17C05p+55},{(-0x1.0p+1),0x0.BA0D37p+20,0xA.6FA0F5p-67},{0xD.999CB5p+76,0x7.FCA77Bp-52,(-0x4.Bp-1)},{0x7.Ep+1,0x0.BA0D37p+20,0x7.Ep+1},{(-0x8.4p+1),0x1.Bp-1,0xC.CD11DCp-64},{0x6.Dp-1,(-0x1.9p-1),0x9.516CEAp+30},{0xA.731AC7p-90,(-0x8.4p+1),(-0x4.Bp-1)}},{{0x3.1B0F82p-80,0x1.B5A5E5p-30,0x1.0p+1},{0xA.731AC7p-90,0xC.CD11DCp-64,0x0.A17C05p+55},{0x6.Dp-1,0x0.7p-1,0xE.3342DFp-85},{(-0x8.4p+1),0xA.731AC7p-90,(-0x1.3p-1)},{0x7.Ep+1,0x1.B5A5E5p-30,0x9.1624B3p-2},{0xD.999CB5p+76,(-0x1.3p-1),(-0x1.3p-1)},{(-0x1.0p+1),(-0x1.9p-1),0xE.3342DFp-85},{0x7.FCA77Bp-52,0xD.999CB5p+76,0x0.A17C05p+55}},{{0x9.1624B3p-2,0x0.BA0D37p+20,0x1.0p+1},{0xD.999CB5p+76,0xB.3380F2p+61,(-0x4.Bp-1)},{0x9.516CEAp+30,0x0.BA0D37p+20,0x9.516CEAp+30},{(-0x8.4p+1),0xD.999CB5p+76,0xC.CD11DCp-64},{(-0x4.9p-1),(-0x1.9p-1),0x7.Ep+1},{0xA.731AC7p-90,(-0x1.3p-1),(-0x4.Bp-1)},{0x1.Bp+1,(-0x1.9p-1),(-0x1.0p+1)},{(-0x1.Ep-1),(-0x1.Ep-1),0xC.CD11DCp-64}},{{0x7.Ep+1,0x1.6p-1,0x1.0p+1},{0x7.FCA77Bp-52,0x1.1p+1,0xB.3380F2p+61},{0x1.Bp+1,(-0x1.9p-1),0x1.6p-1},{(-0x8.4p+1),0x7.FCA77Bp-52,0xB.3380F2p+61},{0x0.4047E6p-92,0x0.7p-1,0x1.0p+1},{(-0x6.2p-1),(-0x1.3p-1),0xC.CD11DCp-64},{0x1.6p-1,0x1.6p-1,(-0x1.0p+1)},{(-0x8.4p+1),(-0x6.2p-1),0x0.A17C05p+55}},{{0x3.1B0F82p-80,0x1.6p-1,0x3.1B0F82p-80},{0x7.FCA77Bp-52,(-0x1.3p-1),0x1.1p+1},{0x9.516CEAp+30,0x0.7p-1,0x1.Bp+1},{(-0x1.Ep-1),0x7.FCA77Bp-52,0x0.A17C05p+55},{(-0x1.Dp+1),(-0x1.9p-1),0x9.1624B3p-2},{(-0x1.Ep-1),0x1.1p+1,0xC.CD11DCp-64},{0x9.516CEAp+30,0x1.6p-1,0xA.6FA0F5p-67},{0x7.FCA77Bp-52,(-0x1.Ep-1),0xB.3380F2p+61}},{{0x3.1B0F82p-80,(-0x1.9p-1),0x0.4047E6p-92},{(-0x8.4p+1),0xB.3380F2p+61,0xB.3380F2p+61},{0x1.6p-1,0x0.7p-1,0xA.6FA0F5p-67},{(-0x6.2p-1),(-0x8.4p+1),0xC.CD11DCp-64},{0x0.4047E6p-92,0x1.6p-1,0x9.1624B3p-2},{(-0x8.4p+1),(-0x4.Bp-1),0x0.A17C05p+55},{0x1.Bp+1,0x1.6p-1,0x1.Bp+1},{0x7.FCA77Bp-52,(-0x8.4p+1),0x1.1p+1}}};
        int64_t *****l_3875[7][4];
        const uint16_t l_3883 = 0x1162L;
        uint64_t l_3923[5];
        struct S1 l_3928 = {4UL};
        int32_t * const **l_3957[5];
        uint8_t *l_3975 = &g_3646[5][4];
        int64_t l_3985[9][7][3] = {{{0xEC8355240D6A9C4ELL,0xCD18250D6889F079LL,9L},{(-5L),(-8L),0x922F5302D07CDA0BLL},{9L,0xEC8355240D6A9C4ELL,9L},{6L,(-1L),0x8326687B01C497E3LL},{2L,(-1L),0x2D61E63C6DC1AFD6LL},{(-8L),0xEC8355240D6A9C4ELL,8L},{0xF88DFB89BC2A9EBELL,(-8L),(-8L)}},{{(-8L),0xCD18250D6889F079LL,(-5L)},{2L,0x922F5302D07CDA0BLL,(-5L)},{6L,(-5L),(-8L)},{9L,0x2D61E63C6DC1AFD6LL,8L},{(-5L),(-5L),0x2D61E63C6DC1AFD6LL},{0xEC8355240D6A9C4ELL,0x922F5302D07CDA0BLL,0x8326687B01C497E3LL},{0xEC8355240D6A9C4ELL,0xCD18250D6889F079LL,9L}},{{(-5L),(-8L),0x922F5302D07CDA0BLL},{9L,0xEC8355240D6A9C4ELL,9L},{6L,(-1L),0x8326687B01C497E3LL},{2L,(-1L),0x2D61E63C6DC1AFD6LL},{(-8L),0xEC8355240D6A9C4ELL,8L},{0xF88DFB89BC2A9EBELL,(-8L),(-8L)},{(-8L),0xCD18250D6889F079LL,(-5L)}},{{2L,0x922F5302D07CDA0BLL,(-5L)},{6L,(-5L),(-8L)},{9L,0x2D61E63C6DC1AFD6LL,8L},{(-5L),(-5L),0x2D61E63C6DC1AFD6LL},{0xEC8355240D6A9C4ELL,0x922F5302D07CDA0BLL,0x8326687B01C497E3LL},{0xEC8355240D6A9C4ELL,0xCD18250D6889F079LL,9L},{(-5L),(-8L),0x922F5302D07CDA0BLL}},{{9L,0xEC8355240D6A9C4ELL,9L},{6L,(-1L),0x8326687B01C497E3LL},{2L,(-1L),0x2D61E63C6DC1AFD6LL},{(-8L),0xEC8355240D6A9C4ELL,8L},{0xF88DFB89BC2A9EBELL,(-8L),(-8L)},{(-8L),0xCD18250D6889F079LL,(-5L)},{2L,0x922F5302D07CDA0BLL,(-5L)}},{{6L,(-5L),(-8L)},{9L,0x2D61E63C6DC1AFD6LL,8L},{(-5L),(-5L),0x2D61E63C6DC1AFD6LL},{6L,(-8L),8L},{6L,0x8326687B01C497E3LL,0x922F5302D07CDA0BLL},{0x2D61E63C6DC1AFD6LL,0xF88DFB89BC2A9EBELL,(-8L)},{0x922F5302D07CDA0BLL,6L,0x922F5302D07CDA0BLL}},{{(-1L),0xCD18250D6889F079LL,8L},{9L,0xCD18250D6889F079LL,0xEC8355240D6A9C4ELL},{0xF88DFB89BC2A9EBELL,6L,2L},{(-5L),0xF88DFB89BC2A9EBELL,0xF88DFB89BC2A9EBELL},{0xF88DFB89BC2A9EBELL,0x8326687B01C497E3LL,0x2D61E63C6DC1AFD6LL},{9L,(-8L),0x2D61E63C6DC1AFD6LL},{(-1L),0x2D61E63C6DC1AFD6LL,0xF88DFB89BC2A9EBELL}},{{0x922F5302D07CDA0BLL,0xEC8355240D6A9C4ELL,2L},{0x2D61E63C6DC1AFD6LL,0x2D61E63C6DC1AFD6LL,0xEC8355240D6A9C4ELL},{6L,(-8L),8L},{6L,0x8326687B01C497E3LL,0x922F5302D07CDA0BLL},{0x2D61E63C6DC1AFD6LL,0xF88DFB89BC2A9EBELL,(-8L)},{0x922F5302D07CDA0BLL,6L,0x922F5302D07CDA0BLL},{(-1L),0xCD18250D6889F079LL,8L}},{{9L,0xCD18250D6889F079LL,0xEC8355240D6A9C4ELL},{0xF88DFB89BC2A9EBELL,6L,2L},{(-5L),0xF88DFB89BC2A9EBELL,0xF88DFB89BC2A9EBELL},{0xF88DFB89BC2A9EBELL,0x8326687B01C497E3LL,0x2D61E63C6DC1AFD6LL},{9L,(-8L),0x2D61E63C6DC1AFD6LL},{(-1L),0x2D61E63C6DC1AFD6LL,0xF88DFB89BC2A9EBELL},{0x922F5302D07CDA0BLL,0xEC8355240D6A9C4ELL,2L}}};
        struct S0 **l_3997 = &g_60;
        int32_t l_4011 = 0L;
        const float l_4020 = 0xC.DC0B1Ep+58;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_3839[i] = 0xC5E4L;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 4; j++)
                l_3875[i][j] = &g_3472;
        }
        for (i = 0; i < 5; i++)
            l_3923[i] = 0xFFEE22BCD3741DADLL;
        for (i = 0; i < 5; i++)
            l_3957[i] = (void*)0;
        if ((l_3726 > l_3726))
        { /* block id: 1635 */
            uint32_t *l_3731 = &g_1682;
            int64_t l_3736 = 0xBB3292F5002ECB99LL;
            uint32_t *l_3741 = &l_3618;
            float *l_3754 = &g_1094;
            int32_t l_3770 = 0x905441F2L;
            int16_t *l_3772[10][6][4] = {{{&l_3645,&g_103,&g_508,(void*)0},{(void*)0,&g_999[3][2],(void*)0,&g_508},{&g_999[3][2],&g_999[3][2],&g_999[0][1],(void*)0},{&g_999[3][2],&g_103,(void*)0,&g_103},{&g_476,(void*)0,&l_3645,&l_3645},{(void*)0,(void*)0,&g_508,&g_103}},{{&l_3645,&g_508,&g_999[3][2],&l_3645},{&l_3645,&l_3645,&g_508,&g_103},{(void*)0,&l_3645,&l_3645,(void*)0},{&g_476,(void*)0,(void*)0,(void*)0},{&g_999[3][2],&g_103,&g_999[0][1],(void*)0},{&g_999[3][2],&g_103,(void*)0,(void*)0}},{{(void*)0,&g_103,&g_508,(void*)0},{&l_3645,(void*)0,&l_3645,(void*)0},{(void*)0,&l_3645,&l_3645,&g_103},{(void*)0,&l_3645,(void*)0,&l_3645},{&g_103,&g_508,(void*)0,&g_103},{(void*)0,(void*)0,&l_3645,&l_3645}},{{(void*)0,(void*)0,&l_3645,&g_103},{&l_3645,&g_103,&g_508,(void*)0},{(void*)0,&g_999[3][2],(void*)0,&g_508},{&g_999[3][2],&g_999[3][2],&g_999[0][1],(void*)0},{&g_999[3][2],&g_103,(void*)0,&g_103},{&g_476,(void*)0,&l_3645,&l_3645}},{{(void*)0,(void*)0,&g_508,&g_103},{&l_3645,&g_508,&g_999[3][2],&l_3645},{&l_3645,&l_3645,&g_508,&g_103},{(void*)0,&l_3645,&l_3645,(void*)0},{&g_476,(void*)0,(void*)0,&l_3645},{&g_103,&g_508,(void*)0,(void*)0}},{{&g_999[3][2],&g_999[0][1],&g_999[3][2],(void*)0},{&g_508,&g_508,&g_999[3][2],&l_3645},{&g_476,&g_508,&g_476,&l_3645},{&g_999[3][2],(void*)0,(void*)0,&g_508},{(void*)0,&g_476,&l_3645,(void*)0},{&l_3645,(void*)0,&l_3645,&g_999[0][1]}},{{(void*)0,&g_103,(void*)0,(void*)0},{&g_999[3][2],&g_999[3][2],&g_476,&l_3645},{&g_476,&l_3645,&g_999[3][2],&g_508},{&g_508,&g_103,&g_999[3][2],&g_999[3][2]},{&g_999[3][2],&g_103,(void*)0,&g_508},{&g_103,&l_3645,&l_3645,&l_3645}},{{&g_103,&g_999[3][2],(void*)0,(void*)0},{&l_3645,&g_103,(void*)0,&g_999[0][1]},{(void*)0,(void*)0,&g_103,(void*)0},{(void*)0,&g_476,(void*)0,&g_508},{&l_3645,(void*)0,(void*)0,&l_3645},{&g_103,&g_508,&l_3645,&l_3645}},{{&g_103,&g_508,(void*)0,(void*)0},{&g_999[3][2],&g_999[0][1],&g_999[3][2],(void*)0},{&g_508,&g_508,&g_999[3][2],&l_3645},{&g_476,&g_508,&g_476,&l_3645},{&g_999[3][2],(void*)0,(void*)0,&g_508},{(void*)0,&g_476,&l_3645,(void*)0}},{{&l_3645,(void*)0,&l_3645,&g_999[0][1]},{(void*)0,&g_103,(void*)0,(void*)0},{&g_999[3][2],&g_999[3][2],&g_476,&l_3645},{&g_476,&l_3645,&g_999[3][2],&g_508},{&g_508,&g_103,&g_999[3][2],&g_999[3][2]},{&g_999[3][2],&g_103,(void*)0,&g_508}}};
            int32_t l_3773 = 0xAEFC9049L;
            int i, j, k;
            (*g_3402) = (((*l_3754) = (safe_sub_func_float_f_f((safe_sub_func_float_f_f((((18446744073709551607UL && (((++(*l_3731)) == l_3726) == (((safe_sub_func_int8_t_s_s(l_3736, 255UL)) != (safe_lshift_func_int8_t_s_u((safe_mod_func_int16_t_s_s((((((--(*l_3741)) & (l_3736 >= (safe_div_func_uint32_t_u_u(2UL, ((*g_310) |= ((*g_2111) = ((g_3746 , ((safe_rshift_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((**g_3380), (g_2820--))), 2)) , (((**g_890) , l_3354) | 7UL))) , 0x5CE26484L))))))) ^ 1L) != l_3356[3][4][2]) || l_3736), l_3736)), 7))) <= 0xFA332097L))) && g_371) , l_3751[2]), l_3256)), l_3751[1]))) < l_3726);
lbl_3757:
            for (g_86 = 0; (g_86 < 6); g_86++)
            { /* block id: 1645 */
                (*g_2111) ^= 9L;
                if (g_105)
                    goto lbl_3757;
            }
            (*g_120) = (((safe_lshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s((l_3773 &= ((l_3258 , (safe_mul_func_int8_t_s_s(((safe_sub_func_uint32_t_u_u((((safe_add_func_uint8_t_u_u((l_2811 = 8UL), (((((l_3434 != (**g_1560)) > (safe_lshift_func_int16_t_s_s((l_3751[0] = ((l_3770 &= g_1534.f7) >= (((*g_2157) = (&l_3337 == (l_3771[0] , (void*)0))) ^ l_3736))), l_3736))) && l_3258) != l_3726) , 0L))) , (-9L)) | l_3726), l_3356[6][7][2])) & 0x8BL), l_3256))) ^ l_3726)), 0xF1L)), l_3771[0])) , &l_3645) != (void*)0);
        }
        else
        { /* block id: 1655 */
            uint32_t l_3780[5] = {0UL,0UL,0UL,0UL,0UL};
            int32_t l_3792 = (-8L);
            uint16_t **l_3797 = &l_3551[7][1];
            int64_t l_3832 = 0x944F88E8CFC6DFE2LL;
            int32_t l_3834 = 1L;
            int32_t l_3836 = 0x32263C26L;
            int32_t l_3837 = 0xC3DF7102L;
            int32_t l_3838 = 0xCA494D7DL;
            uint32_t l_3840[2][3][5] = {{{0xB161D667L,0x14A98576L,0x14A98576L,0xB161D667L,0x14A98576L},{0xB161D667L,0xB161D667L,0xB136F7DEL,0xB161D667L,0xB161D667L},{0x14A98576L,0xB161D667L,0x14A98576L,0x14A98576L,0xB161D667L}},{{0xB161D667L,0x14A98576L,0x14A98576L,0xB161D667L,0x14A98576L},{0xB161D667L,0xB161D667L,0xB136F7DEL,0xB161D667L,0xB161D667L},{0x14A98576L,0xB161D667L,0x14A98576L,0x14A98576L,0xB161D667L}}};
            struct S1 l_3872 = {18446744073709551615UL};
            uint8_t *** const l_3873 = &g_2156[3][6][0];
            int16_t *l_4032 = &l_3839[0];
            int i, j, k;
            if (((safe_mod_func_uint8_t_u_u((safe_sub_func_int16_t_s_s((((-8L) > (l_3771[0] >= (l_3780[4] < (safe_div_func_int32_t_s_s(0x7F244868L, (safe_sub_func_int32_t_s_s((**g_1895), (((((!(safe_sub_func_int64_t_s_s((safe_rshift_func_uint16_t_u_s((((safe_rshift_func_uint8_t_u_s(((l_3771[0] > (l_3792 = l_3499)) & ((safe_div_func_int8_t_s_s((safe_div_func_int32_t_s_s(((void*)0 != l_3797), 4294967286UL)), l_3751[1])) , l_3751[1])), 2)) || 0x8A94E2C0L) > (*g_891)), l_3780[4])), (*g_891)))) >= l_3780[4]) , l_3726) & l_3256) == g_3342.f2)))))))) <= l_3798), l_3780[1])), l_3780[1])) && g_2086.f1))
            { /* block id: 1657 */
                int64_t l_3828[2][7] = {{0L,0xEADE7DF21B026854LL,0xEADE7DF21B026854LL,0L,0xEADE7DF21B026854LL,0xEADE7DF21B026854LL,0L},{0xEADE7DF21B026854LL,0L,0xEADE7DF21B026854LL,0xEADE7DF21B026854LL,0L,0xEADE7DF21B026854LL,0xEADE7DF21B026854LL}};
                int64_t l_3830 = (-1L);
                int32_t l_3831[10] = {0xF758BE41L,0xF758BE41L,0xCB27527FL,0xF758BE41L,0xF758BE41L,0xCB27527FL,0xF758BE41L,0xF758BE41L,0xCB27527FL,0xF758BE41L};
                float l_3855 = 0x4.F4E797p-37;
                int64_t l_3859 = 0xAF68603386B47601LL;
                int i, j;
                if (((safe_div_func_int16_t_s_s(((l_3780[4] , 1UL) | (safe_unary_minus_func_int8_t_s((safe_div_func_int64_t_s_s(((**g_890) = 1L), l_3780[4]))))), 0xF97CL)) != 0xDBL))
                { /* block id: 1659 */
                    float l_3804 = 0x0.6p+1;
                    int32_t *l_3805 = (void*)0;
                    int32_t *l_3806 = &l_3356[3][4][2];
                    int32_t *l_3807 = &g_2341;
                    int32_t *l_3808 = &l_3751[1];
                    int32_t *l_3809 = &l_3356[3][4][2];
                    int32_t *l_3810 = &l_3751[1];
                    int32_t *l_3811 = &g_105;
                    int32_t *l_3812 = &g_2;
                    int32_t *l_3813 = &g_105;
                    int32_t *l_3814 = &l_3413;
                    int32_t *l_3815 = &g_5;
                    int32_t *l_3816 = &g_2341;
                    int32_t *l_3817 = &g_2341;
                    int32_t *l_3819 = &l_3256;
                    int32_t *l_3820 = &l_3354;
                    int32_t *l_3821 = &l_3413;
                    int32_t *l_3822 = &l_3356[3][4][2];
                    int32_t *l_3823 = &l_3256;
                    int32_t *l_3824 = &g_105;
                    int32_t *l_3825 = &g_2341;
                    int32_t *l_3826[6] = {&g_105,&l_3356[6][1][0],&l_3356[6][1][0],&g_105,&l_3356[6][1][0],&l_3356[6][1][0]};
                    int16_t l_3833 = 0L;
                    int i;
                    l_3840[1][1][4]++;
                    (**g_2052) |= (((~(safe_sub_func_uint64_t_u_u((((((safe_rshift_func_uint8_t_u_s(l_3645, l_3831[1])) <= ((*g_891) = 0xE8772A07982F5184LL)) , (safe_add_func_int64_t_s_s(l_3726, (g_1372[1][6][0].f7 > ((*l_3816) = l_3829))))) && l_3832) <= (l_3413 >= ((*g_2157) = l_3645))), l_3835))) <= l_3830) >= 0xB57C14B3L);
                    (*g_310) |= (-7L);
                }
                else
                { /* block id: 1666 */
                    struct S0 ** volatile * volatile * volatile * volatile l_3853 = &g_3851;/* VOLATILE GLOBAL l_3853 */
                    int32_t l_3857 = 0x7EF23E46L;
                    int32_t l_3858 = 0x51158E58L;
                    l_3853 = g_3850;
lbl_3864:
                    for (g_209 = 0; (g_209 <= 3); g_209 += 1)
                    { /* block id: 1670 */
                        int32_t l_3854 = (-1L);
                        int32_t *l_3856[5][1][5] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_2811,&l_2811,&l_2811,&l_2811,&l_2811}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_2811,&l_2811,&l_2811,&l_2811,&l_2811}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
                        uint32_t l_3861 = 1UL;
                        int i, j, k;
                        (*g_310) ^= 0x90E86489L;
                        --l_3861;
                        if (l_3258)
                            goto lbl_3864;
                        if ((**g_360))
                            continue;
                    }
                    for (g_2664.f1 = 0; (g_2664.f1 < (-20)); g_2664.f1 = safe_sub_func_uint32_t_u_u(g_2664.f1, 8))
                    { /* block id: 1678 */
                        return l_3859;
                    }
                }
            }
            else
            { /* block id: 1682 */
                int64_t l_3871[2];
                uint16_t l_3943 = 65535UL;
                float *l_3944 = &g_9[1][0][1];
                const uint16_t *l_4016 = &l_3943;
                const uint16_t **l_4015 = &l_4016;
                const uint16_t ***l_4014 = &l_4015;
                const uint16_t ****l_4013 = &l_4014;
                const uint16_t ***** const l_4012 = &l_4013;
                union U2 *l_4018 = (void*)0;
                uint16_t l_4021 = 1UL;
                int i;
                for (i = 0; i < 2; i++)
                    l_3871[i] = 0xC639B07A7798753ELL;
                for (l_3832 = 6; (l_3832 <= (-23)); --l_3832)
                { /* block id: 1685 */
                    uint32_t l_3876 = 0UL;
                    uint8_t ***l_3903[8];
                    uint8_t ****l_3902 = &l_3903[1];
                    uint16_t l_3912 = 3UL;
                    int32_t l_3927 = 7L;
                    struct S0 **l_3941[9][1] = {{&g_60},{&g_60},{&g_60},{&g_60},{&g_60},{&g_60},{&g_60},{&g_60},{&g_60}};
                    int i, j;
                    for (i = 0; i < 8; i++)
                        l_3903[i] = &g_2156[2][3][2];
                    (*g_120) ^= 0x08FFC656L;
                    if (l_3413)
                        break;
                    if (((((l_3354 &= (safe_mod_func_uint16_t_u_u((l_3751[1] == (func_62(l_3499, l_3356[2][4][2], (((l_3871[1] , l_3872) , (void*)0) == l_3873)) , ((l_3874 = l_3874) != l_3875[0][2]))), 1UL))) > l_3839[0]) ^ l_3876) < l_3876))
                    { /* block id: 1690 */
                        int16_t *l_3909 = &l_3829;
                        int32_t l_3910 = 3L;
                        uint32_t *l_3911 = &g_2270[3][0][2];
                        float *l_3917 = (void*)0;
                        float *l_3918 = &g_355[4];
                        float *l_3924 = (void*)0;
                        float *l_3925 = &l_3860[2][6][2];
                        float *l_3926 = &g_1094;
                        (*g_310) = (safe_lshift_func_int16_t_s_s(((safe_mod_func_int8_t_s_s(((safe_sub_func_int8_t_s_s(((((*g_32) & (l_3883 > ((safe_sub_func_uint32_t_u_u(((*l_3911) |= (safe_mod_func_int32_t_s_s((l_3910 = ((*g_2111) = (safe_mod_func_int64_t_s_s(((*g_891) = ((((safe_mul_func_uint8_t_u_u((~l_3792), (((*g_2111) && 0xE4F71DE7L) , (1UL & ((safe_rshift_func_int8_t_s_u(((safe_mod_func_int8_t_s_s((!((((*l_3909) = (((0x534DB89AL == (l_3871[1] , (l_3902 == g_3904[1][5][4]))) > l_3829) ^ g_2341)) , (*l_3495)) == (void*)0)), l_2811)) && (*g_891)), 2)) < l_3356[3][4][2]))))) != l_3798) < l_3871[0]) | 0xB72C1407L)), l_3818)))), l_3835))), l_3871[1])) != 0xDCL))) && l_3912) <= l_3836), 0x88L)) > l_3871[1]), l_3871[1])) , 0x0879L), 14));
                        l_3927 = (safe_sub_func_float_f_f((0x6.202E23p-84 > ((*l_3926) = ((safe_add_func_float_f_f(((*l_3918) = ((*g_8) = 0x1.283427p-92)), ((*l_3925) = (0x8.2FD218p+54 < (safe_div_func_float_f_f(((**g_1495) , (safe_sub_func_float_f_f((*g_688), 0x3.Bp+1))), l_3923[4])))))) <= 0xA.75C307p+99))), (l_3645 >= l_3876)));
                    }
                    else
                    { /* block id: 1702 */
                        (*g_595) = l_3928;
                    }
                    for (l_3835 = 0; (l_3835 == (-23)); l_3835 = safe_sub_func_int32_t_s_s(l_3835, 8))
                    { /* block id: 1707 */
                        int8_t l_3931 = 0x06L;
                        struct S0 ***l_3940 = &l_3347;
                        int16_t *l_3942 = &l_3257;
                        if (l_3931)
                            break;
                        (*g_3932) = ((**g_737) , (*g_484));
                    }
                }
                (**g_594) = func_40(l_3943, l_3944, (safe_add_func_int32_t_s_s((l_3871[1] , (safe_div_func_uint16_t_u_u((((((((*g_688) < (l_3798 , (l_3871[1] != (safe_mul_func_float_f_f((0x1.891900p-81 < (safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f((l_3957[4] != ((l_3413 == 4294967291UL) , (void*)0)), 0x1.Ap+1)), 0x1.E8D4B3p+18)), 0x5.9A8912p+69))), l_3871[1]))))) , g_101[0][4][0]) ^ l_2811) ^ l_3943) , l_3780[4]) , l_3872.f0), 0xB1B2L))), 0x6469023DL)));
                for (l_3943 = 0; (l_3943 != 55); ++l_3943)
                { /* block id: 1718 */
                    int32_t **l_3968 = &g_310;
                    const struct S1 *l_3981 = &l_3928;
                    struct S0 **l_3996 = &g_60;
                    uint32_t *l_4019 = &g_2270[1][2][5];
                    for (g_1627.f1 = (-21); (g_1627.f1 != 7); g_1627.f1 = safe_add_func_uint64_t_u_u(g_1627.f1, 7))
                    { /* block id: 1721 */
                        int32_t * const *l_3969[3][9][3] = {{{&g_2111,&g_2111,(void*)0},{&g_2111,&g_2111,&g_2111},{&g_310,&g_2111,&g_310},{(void*)0,&g_310,&g_2111},{(void*)0,&g_2111,&g_2111},{&g_2111,&g_2111,&g_310},{&g_2111,&g_2111,&g_2111},{&g_2111,(void*)0,(void*)0},{&g_2111,&g_310,(void*)0}},{{(void*)0,&g_2111,&g_2111},{(void*)0,&g_2111,(void*)0},{&g_310,&g_310,(void*)0},{&g_2111,&g_310,&g_2111},{&g_2111,&g_310,&g_310},{&g_310,&g_310,&g_2111},{&g_310,&g_2111,&g_2111},{&g_2111,&g_2111,&g_310},{&g_310,&g_310,&g_2111}},{{&g_310,(void*)0,(void*)0},{&g_2111,&g_2111,(void*)0},{&g_2111,&g_2111,&g_2111},{&g_310,&g_2111,&g_310},{(void*)0,&g_310,&g_2111},{(void*)0,&g_2111,&g_2111},{&g_2111,&g_2111,&g_310},{&g_2111,&g_2111,&g_2111},{&g_2111,(void*)0,(void*)0}}};
                        int32_t l_3978 = 0xFD720318L;
                        const struct S1 **l_3982 = (void*)0;
                        int i, j, k;
                        (**l_3968) = ((safe_add_func_float_f_f(0x8.Dp+1, ((safe_div_func_float_f_f((((((*l_3944) = (((safe_sub_func_float_f_f(l_3780[4], (l_3968 == (g_3970 = l_3969[2][2][1])))) <= (safe_div_func_float_f_f(0x9.6D7DC7p+45, ((l_3975 != ((((*g_3971) = (0x80375A50L ^ (**g_2461))) >= (((safe_mod_func_int64_t_s_s(l_3256, 0x5E295090CE47970ELL)) >= (**l_3968)) >= l_3978)) , &l_3499)) == (*g_688))))) != l_3256)) , g_3979[0][4][3]) != (void*)0) == l_3943), (*g_688))) >= (-0x1.3p+1)))) > (**l_3968));
                        (*g_3983) = l_3981;
                        l_3985[8][1][2] ^= (**g_1472);
                    }
                    (*g_2050) = 0L;
                    if (g_2)
                        goto lbl_3310;
                    (**g_3932) = (safe_add_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_u(0xE0L, ((((*g_2157) ^= (safe_sub_func_uint64_t_u_u((safe_add_func_int8_t_s_s(((safe_mod_func_int8_t_s_s((l_3996 != l_3997), l_3260)) , (safe_rshift_func_uint16_t_u_s((safe_add_func_int64_t_s_s((*g_891), ((safe_mul_func_float_f_f(((+(((safe_add_func_uint8_t_u_u(((((safe_rshift_func_uint8_t_u_s(((safe_add_func_uint8_t_u_u(l_4011, (((((void*)0 == l_4012) < ((((((*l_4019) = ((!((l_3337 != l_4018) , (*g_32))) >= l_3836)) , l_3871[1]) != l_3943) ^ 0x883BL) , l_3837)) , (void*)0) != l_4019))) < (**l_3968)), 4)) , &g_3851) != (void*)0) | (**l_3968)), l_3780[4])) ^ 2L) != l_3356[6][8][1])) , 0x1.939E14p-34), l_3258)) , (*g_32)))), l_4021))), (**l_3968))), (*g_891)))) <= l_3356[3][4][2]) != l_2811))) != l_3871[0]), (-4L)));
                }
            }
            (*g_3402) = 0x4.E081CBp-7;
            (*g_2050) &= (safe_mod_func_int32_t_s_s((safe_div_func_int16_t_s_s((l_3645 && (safe_rshift_func_uint16_t_u_s(((void*)0 == (*g_3377)), 13))), l_3838)), ((safe_mul_func_uint16_t_u_u((l_3837 && (l_3780[4] >= 0xC7442438CE79D492LL)), ((((*l_4032) = (safe_sub_func_uint32_t_u_u(g_2137.f7, ((*g_2111) <= 4294967295UL)))) & l_3257) , 0x501EL))) || l_2811)));
        }
        --l_4033[1][0][1];
        (*g_310) |= (safe_lshift_func_uint16_t_u_s(1UL, 6));
    }
    return l_3356[7][6][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_2070 g_2820 g_450 g_1339 g_5 g_595 g_32 g_33 g_2111 g_2050 g_1555 g_69 g_642 g_2052 g_120 g_121 g_2 g_61.f3 g_61.f1 g_61.f5 g_61.f4 g_101 g_86 g_103 g_61.f2 g_105 g_61.f6 g_1496 g_1780 g_23 g_1515 g_2137.f1 g_1612 g_2133.f7 g_1534.f7 g_1372.f0 g_1094 g_1426 g_891 g_2338.f3 g_994 g_116 g_8 g_9 g_2956 g_2977 g_2978 g_482 g_2810 g_124 g_2290 g_2288 g_3069 g_2648.f3 g_2015.f0 g_297 g_298 g_158 g_133 g_2133.f5 g_1501 g_3246 g_1262.f3 g_2086.f5 g_119
 * writes: g_2070 g_450 g_482.f0 g_103 g_2664.f1 g_596 g_2820 g_33 g_2334 g_1555 g_69 g_310 g_999 g_82 g_86 g_61.f5 g_101 g_105 g_116 g_482 g_1780 g_1612 g_1534.f7 g_1094 g_1682 g_2338.f3 g_2956 g_2664.f3 g_2976 g_2810 g_119.f0 g_3046 g_2107.f3 g_121 g_508 g_143 g_1461.f1 g_209 g_1501 g_371 g_454 g_119
 */
static struct S1  func_15(int32_t * const  p_16, float * p_17, int32_t * const  p_18, int16_t  p_19)
{ /* block id: 1214 */
    uint16_t *l_2815 = &g_450;
    int32_t l_2817[9][3][8] = {{{3L,0xFD9E0323L,3L,0x2C773AC3L,0x00393F57L,2L,(-1L),0xE68FEEE0L},{0x5BEB977BL,3L,0xDC5E4C4AL,(-5L),(-10L),0x3AD57F78L,0x1CC913AFL,0L},{5L,1L,0xE0FD8DCBL,0xEE4DA7E2L,0x9E98D27DL,0xFED2286EL,0x8048C0F6L,1L}},{{0x3DC8991EL,0x5BA6C7C2L,0xD311A49BL,0x296554A0L,(-1L),5L,(-5L),9L},{(-1L),9L,0x17E8580AL,1L,5L,0xC8B3E995L,0x0049D06DL,1L},{(-5L),3L,0x45EF7AA3L,0L,0x48414B2CL,(-2L),3L,(-8L)}},{{0L,(-5L),0x684F4845L,4L,0x8F075D0BL,0xF88AF068L,0x48C2F4FCL,0xFF21B1F9L},{0xC2376E95L,(-4L),0L,0xF448B63BL,0xFF21B1F9L,(-5L),0x48414B2CL,0x3BD90ECDL},{0x2C773AC3L,0xF88AF068L,2L,0x48C2F4FCL,0x6D3912F5L,(-1L),1L,1L}},{{(-6L),(-1L),0xFED2286EL,0xFED2286EL,(-1L),(-4L),0L,(-1L)},{0x35D51A8AL,0xBCE4BEB0L,0x5E7C6E7FL,5L,0x10ED398AL,5L,(-5L),1L},{(-1L),4L,(-1L),5L,0x3BD90ECDL,0x863A10D4L,1L,(-1L)}},{{(-2L),0x3BD90ECDL,0x5BEB977BL,0x48C2F4FCL,(-9L),0L,0x684F4845L,0xBCE4BEB0L},{0x8F075D0BL,0xFED2286EL,0xD311A49BL,(-2L),1L,0x3DC8991EL,1L,0xB6F5B3E5L},{0L,0x296554A0L,2L,0x3F7184D6L,0x863A10D4L,0xF88AF068L,8L,0x35D51A8AL}},{{(-6L),(-5L),0x10ED398AL,0x6D3912F5L,0x3AD57F78L,0xD311A49BL,0xE68FEEE0L,0L},{5L,1L,8L,(-1L),0xFF21B1F9L,0x3AD57F78L,0xE4A8C503L,0x5BEB977BL},{0L,(-1L),0xC2376E95L,0xBCE4BEB0L,1L,0xAA01BFC2L,0x3F7184D6L,0x3DC8991EL}},{{0xA92A7B16L,0xE0FD8DCBL,(-10L),0xFD9E0323L,0xAA01BFC2L,0L,0x8048C0F6L,0xD311A49BL},{0xFED2286EL,0L,(-9L),0xE4A8C503L,0x6D3912F5L,(-8L),(-3L),3L},{0xDC5E4C4AL,(-1L),0L,4L,1L,(-1L),1L,0x684F4845L}},{{9L,1L,0xFAAE0383L,1L,0xFAAE0383L,1L,9L,0x0E172591L},{0x863A10D4L,0x0049D06DL,0xFD9E0323L,2L,0xF448B63BL,0xE4A8C503L,0L,0x3E548BA2L},{0x55CF1A2EL,(-6L),0xA92A7B16L,(-8L),0xF448B63BL,(-1L),0x8F075D0BL,1L}},{{0x863A10D4L,0xE016226FL,0L,0x3E548BA2L,0xFAAE0383L,0L,0x9E98D27DL,0xC2376E95L},{9L,0x48C2F4FCL,0x3F7184D6L,4L,1L,(-2L),(-1L),1L},{0xDC5E4C4AL,0x863A10D4L,0xC8B3E995L,(-6L),0x6D3912F5L,0x3E548BA2L,0xB6F5B3E5L,(-4L)}}};
    int64_t * const ***l_2822 = (void*)0;
    int64_t * const ****l_2821 = &l_2822;
    struct S1 l_2859 = {0xB25E631CL};
    uint64_t l_2906 = 0UL;
    const int8_t l_2933[8] = {0x2FL,0x2FL,0x2FL,0x2FL,0x2FL,0x2FL,0x2FL,0x2FL};
    float *l_2949[3];
    int32_t l_3010 = 0xFBC6995DL;
    int16_t l_3031 = 0x1E2AL;
    uint64_t l_3102 = 0x7B77CC0F2B0C192ELL;
    const int8_t l_3131 = 1L;
    float l_3145 = 0x1.3p-1;
    uint8_t ***l_3194 = &g_2156[4][1][0];
    int16_t l_3215 = (-1L);
    struct S1 l_3252 = {0xEFDBBB49L};
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_2949[i] = &g_9[1][1][5];
    for (g_2070 = 4; (g_2070 < 39); ++g_2070)
    { /* block id: 1217 */
        uint32_t l_2816 = 0x24207E55L;
        int16_t l_2823 = 0xD291L;
        uint32_t l_2828 = 0x3C35CE77L;
        int32_t l_2829 = (-4L);
        uint16_t *l_2830 = (void*)0;
        int32_t l_2845 = 0x448DD6F7L;
        struct S1 **l_2852 = (void*)0;
        int32_t l_2894 = (-1L);
        uint64_t *l_2898 = &g_116;
        uint8_t l_2932 = 0UL;
        int32_t l_2940[9];
        uint16_t l_2942 = 0x6D83L;
        uint8_t **l_2972 = &g_2157;
        int64_t l_3182[9][7][4] = {{{0xE4DFE41218F67B49LL,0x7B0B5B0FF6015135LL,0xA7314D9B02390C62LL,0x9345BABD5D91A742LL},{(-1L),(-1L),0xA7314D9B02390C62LL,6L},{0xE4DFE41218F67B49LL,0xF2572C4CF017DFBBLL,0x84E6B565CB0F6FDFLL,0xEC29ABB21CAF883ALL},{0x061D531740442849LL,0x335D863A5765ABBBLL,(-1L),0x71D14FD2BE20195BLL},{(-1L),0x71D14FD2BE20195BLL,9L,(-4L)},{0xB29BB9ACE387ADAALL,0xDD37653C75138A03LL,8L,0x1145B7626FE45572LL},{0x9F581EB96EEBF823LL,0x0AC3A60DA16F5165LL,4L,6L}},{{1L,0xB29BB9ACE387ADAALL,0x9345BABD5D91A742LL,1L},{(-9L),0x72F75CEF69532A3FLL,(-4L),1L},{0x3AE37A66C28B0082LL,0x9345BABD5D91A742LL,0x7B0B5B0FF6015135LL,0x244D91516F8D2CF0LL},{0xB29BB9ACE387ADAALL,0x1AAA7E491508D5D6LL,0x238C82C0EFA911B0LL,(-4L)},{9L,0xE4DFE41218F67B49LL,0x2A53342BA10B34ABLL,0x335D863A5765ABBBLL},{1L,1L,0x526EB77ED4AB2C57LL,0x0AC3A60DA16F5165LL},{0L,0x8BA2F0B8DCAA439ELL,6L,0xAC0D721EE2A4955FLL}},{{8L,4L,4L,8L},{(-1L),1L,0L,9L},{6L,0x48161C3DD757680CLL,0x2A53342BA10B34ABLL,0xEEEA751440657435LL},{0x9345BABD5D91A742LL,0x1AAA7E491508D5D6LL,0x3AE37A66C28B0082LL,0xEEEA751440657435LL},{6L,0x48161C3DD757680CLL,4L,9L},{0xEC29ABB21CAF883ALL,1L,0x061D531740442849LL,8L},{0x71D14FD2BE20195BLL,4L,(-1L),0xAC0D721EE2A4955FLL}},{{(-4L),0x8BA2F0B8DCAA439ELL,0xEEEA751440657435LL,0x0AC3A60DA16F5165LL},{0x1145B7626FE45572LL,1L,(-4L),0x335D863A5765ABBBLL},{6L,0xE4DFE41218F67B49LL,1L,(-4L)},{1L,6L,9L,0x7104345455382B8CLL},{1L,0xAC0D721EE2A4955FLL,0L,0x244D91516F8D2CF0LL},{0x244D91516F8D2CF0LL,0x8BA2F0B8DCAA439ELL,(-4L),(-1L)},{0x7B0B5B0FF6015135LL,(-1L),4L,0x72F75CEF69532A3FLL}},{{0L,0x9E99C5D2FA02AF0ELL,0x238C82C0EFA911B0LL,9L},{0xA09863DEE91DF23ELL,0x341EAEE98163BB5DLL,9L,0x704E743B5CCADCA7LL},{0x9345BABD5D91A742LL,0x061D531740442849LL,1L,0x061D531740442849LL},{4L,0x48161C3DD757680CLL,0xFB962C2DBD4DF6BALL,0x2A53342BA10B34ABLL},{0x1145B7626FE45572LL,0xDD37653C75138A03LL,0x061D531740442849LL,0x72F75CEF69532A3FLL},{4L,(-5L),0xA7314D9B02390C62LL,0xAC0D721EE2A4955FLL},{4L,(-4L),0x061D531740442849LL,0L}},{{0x1145B7626FE45572LL,0xAC0D721EE2A4955FLL,0xFB962C2DBD4DF6BALL,1L},{4L,0xE4DFE41218F67B49LL,1L,0xFB962C2DBD4DF6BALL},{0x9345BABD5D91A742LL,1L,9L,0x335D863A5765ABBBLL},{0xA09863DEE91DF23ELL,0xB29BB9ACE387ADAALL,0x238C82C0EFA911B0LL,0x244D91516F8D2CF0LL},{0L,(-4L),4L,0xA7314D9B02390C62LL},{0x7B0B5B0FF6015135LL,4L,(-4L),(-9L)},{0x244D91516F8D2CF0LL,0x9E99C5D2FA02AF0ELL,0L,0x2A53342BA10B34ABLL}},{{1L,0x84E6B565CB0F6FDFLL,9L,0xEEEA751440657435LL},{1L,8L,1L,0x061D531740442849LL},{6L,0x341EAEE98163BB5DLL,(-4L),1L},{0x1145B7626FE45572LL,1L,0xEEEA751440657435LL,(-9L)},{(-4L),(-5L),(-1L),(-1L)},{0x71D14FD2BE20195BLL,0x71D14FD2BE20195BLL,0x061D531740442849LL,0x0AC3A60DA16F5165LL},{0xEC29ABB21CAF883ALL,0xB29BB9ACE387ADAALL,4L,1L}},{{6L,6L,0x3AE37A66C28B0082LL,4L},{0x9345BABD5D91A742LL,6L,0x2A53342BA10B34ABLL,1L},{6L,0xB29BB9ACE387ADAALL,0L,0x0AC3A60DA16F5165LL},{(-1L),0x71D14FD2BE20195BLL,4L,(-1L)},{8L,(-5L),6L,(-9L)},{0L,1L,0x526EB77ED4AB2C57LL,1L},{1L,0x341EAEE98163BB5DLL,0x2A53342BA10B34ABLL,0x061D531740442849LL}},{{9L,8L,1L,0xEEEA751440657435LL},{(-1L),0x84E6B565CB0F6FDFLL,(-4L),0x2A53342BA10B34ABLL},{0xEC29ABB21CAF883ALL,0x9E99C5D2FA02AF0ELL,0xBAA38A371AB332ADLL,4L},{0xEEEA751440657435LL,8L,6L,(-4L)},{0x061D531740442849LL,0x704E743B5CCADCA7LL,0xE4DFE41218F67B49LL,0x7104345455382B8CLL},{1L,(-1L),8L,0x48161C3DD757680CLL},{(-9L),0x244D91516F8D2CF0LL,0xEC29ABB21CAF883ALL,8L}}};
        float **l_3250 = &l_2949[1];
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_2940[i] = (-3L);
        if ((l_2815 == (((l_2816 && l_2817[7][2][4]) ^ (safe_lshift_func_uint16_t_u_u((((((g_2820 , l_2821) != (void*)0) == ((*l_2815) = l_2823)) || (l_2816 < ((safe_sub_func_uint64_t_u_u((((((((safe_add_func_uint16_t_u_u(l_2816, p_19)) & 0xECFAL) > l_2817[7][2][4]) || l_2828) > p_19) , g_450) <= l_2829), l_2817[7][2][4])) | l_2817[7][2][4]))) , 65528UL), g_1339[8][2][1]))) , l_2830)))
        { /* block id: 1219 */
            uint64_t l_2846[10] = {0xFEE761EC7DFD6A7ALL,4UL,4UL,0xFEE761EC7DFD6A7ALL,4UL,4UL,0xFEE761EC7DFD6A7ALL,4UL,4UL,0xFEE761EC7DFD6A7ALL};
            int32_t *l_2857 = &g_105;
            int16_t *l_2888 = &g_999[3][4];
            uint64_t *l_2893[1];
            int32_t l_2895 = 0x21CAC597L;
            int32_t *l_2899 = &g_1780;
            uint32_t *l_2900 = (void*)0;
            uint32_t *l_2901 = (void*)0;
            uint32_t *l_2902[7][6][6] = {{{&g_101[1][0][0],&g_2270[0][3][5],&l_2828,&g_1501[5],&g_101[0][4][0],&g_1501[0]},{&g_1501[0],&g_1501[5],&g_1501[0],(void*)0,&g_1501[0],&g_1501[5]},{&g_1501[5],(void*)0,&g_1501[5],&g_101[0][4][0],&g_2270[0][3][5],&g_1501[3]},{&l_2828,&g_1501[2],&g_1501[0],&g_1501[0],&g_2270[4][4][1],&l_2828},{&l_2828,&g_1501[2],&g_1501[0],&g_2270[4][4][1],&g_2270[0][3][5],&g_2270[4][4][1]},{&g_2270[4][4][1],(void*)0,&g_101[0][2][0],&g_2270[4][4][1],&g_1501[0],&g_2270[4][4][1]}},{{&g_1501[4],&g_1501[5],&g_2270[4][4][1],&g_1682,&g_101[0][4][0],(void*)0},{&g_1501[0],&g_2270[0][3][5],&g_2270[4][4][1],&g_1501[0],&g_101[0][4][0],&g_101[0][4][0]},{&l_2828,&g_101[0][4][0],&g_1501[2],&g_1501[2],&g_2270[3][3][1],&g_2270[4][4][1]},{&g_101[0][2][0],&g_101[0][4][0],&g_1501[5],&g_1501[0],&g_1501[2],&g_2270[4][4][1]},{(void*)0,&g_101[0][4][0],&g_1501[5],&g_1501[5],&g_1501[5],&g_1501[5]},{&g_2270[0][3][3],&g_2270[0][3][3],&g_1501[0],&g_101[0][4][0],&g_101[0][4][0],&g_1501[0]}},{{&g_2270[4][4][1],&g_101[0][4][0],&g_1501[5],(void*)0,&g_1501[0],&g_1501[0]},{&g_2270[3][0][1],&g_2270[4][4][1],&g_1501[5],&g_1501[2],&g_2270[0][3][3],&g_1501[0]},{&g_1501[5],&g_1501[2],&g_1501[0],&l_2828,&g_1682,&g_1501[5]},{&l_2828,&g_1682,&g_1501[5],&g_1501[0],&g_101[0][4][0],&g_2270[4][4][1]},{&g_1501[4],&g_1501[0],&g_1501[5],&g_1501[2],&g_1501[0],&g_2270[4][4][1]},{&g_1501[0],&g_101[0][4][0],&g_1501[2],&g_1682,&g_101[0][4][0],&g_101[0][4][0]}},{{&g_1501[5],&g_101[0][4][0],&g_2270[4][4][1],&g_1501[0],&g_2270[3][0][1],(void*)0},{&g_1501[5],&g_101[0][4][0],&g_2270[4][4][1],&g_2270[4][4][1],&g_1501[2],&g_2270[4][4][1]},{&g_101[0][2][0],&g_2270[2][2][3],&g_101[0][2][0],&g_1501[3],&g_1501[0],&g_2270[4][4][1]},{&g_1501[0],(void*)0,&g_1501[0],&g_101[0][4][0],&g_1501[5],&l_2828},{&g_101[0][4][0],&g_2270[0][3][5],&g_1501[0],&g_101[0][4][0],&g_101[0][4][0],&g_1501[3]},{&g_1501[0],&g_1501[0],&g_1501[5],&g_1501[3],&g_1501[4],&g_1501[5]}},{{&g_101[0][2][0],&g_2270[4][4][1],&g_1501[0],&g_2270[4][4][1],&g_2270[0][3][5],&g_1501[0]},{&g_1501[5],&g_1682,&l_2828,&g_1501[0],&g_1501[2],&g_1501[5]},{&g_1501[5],&g_2270[4][4][1],&g_1501[0],&g_1682,&g_101[0][4][0],&g_1501[4]},{&g_1501[0],&g_2270[0][1][4],&g_1682,&g_2270[2][2][3],&l_2828,&l_2828},{&g_1501[0],&g_101[1][0][0],&g_2270[2][2][3],&g_1501[6],&g_1501[0],&g_1501[0]},{&g_2270[4][4][1],&g_1501[0],&g_1501[0],&g_2270[4][4][1],&g_2270[4][4][1],&g_1501[5]}},{{&g_101[0][4][0],&g_1501[4],&g_1501[1],&l_2828,&g_1501[6],&g_1501[0]},{&g_1501[2],&g_1501[0],&g_2270[4][4][1],(void*)0,&g_1501[6],&g_1501[0]},{&l_2828,&g_1501[4],&g_101[0][4][0],&g_1682,&g_2270[4][4][1],&g_101[0][2][0]},{&g_1501[3],&g_1501[0],&g_2270[0][3][5],&g_2270[4][4][1],&g_1501[0],&g_2270[4][4][1]},{&g_1501[1],&g_101[1][0][0],&g_1501[5],&g_1501[2],&l_2828,&g_2270[4][4][1]},{&g_101[0][4][0],&g_2270[0][1][4],&g_101[0][4][0],&l_2828,&g_1501[0],&g_1501[2]}},{{&g_1501[5],&g_1501[0],&g_101[1][0][0],&g_2270[0][3][5],&l_2828,&g_1501[5]},{&g_101[1][0][0],&g_1501[5],&g_101[0][4][0],&g_101[0][4][0],&g_1501[0],&g_1501[5]},{&g_1501[0],&l_2828,&g_1682,&l_2828,&g_1501[0],&g_1501[0]},{&g_101[0][4][0],&g_101[1][0][0],&l_2828,&g_2270[4][4][1],&g_1501[4],&g_1501[5]},{&g_2270[4][4][1],&g_1501[0],(void*)0,&g_101[1][0][0],&g_2270[4][4][1],&g_1501[5]},{&g_1501[5],&g_1501[0],&l_2828,&g_101[0][4][0],&g_1501[6],&g_1501[0]}}};
            uint8_t *l_2903 = &g_1612;
            uint32_t l_2905 = 0xCF868E74L;
            struct S1 l_2934 = {0UL};
            int32_t l_2939 = 0xC31D1065L;
            int32_t l_2941[10][9][2] = {{{0xAF320983L,0xBC23E805L},{1L,0xB54A9507L},{0x6D8EA450L,0x0AD33255L},{0L,1L},{1L,0xAF320983L},{(-1L),0x7DC0E625L},{0x7D0E95D0L,1L},{0x730D8F49L,0xEC0AC84DL},{0x6D8EA450L,0xEAABF294L}},{{0L,0xBC23E805L},{0x7DC0E625L,0L},{0x32A40C09L,0x99F99C93L},{(-7L),1L},{0x7DC0E625L,(-1L)},{1L,0xEAABF294L},{0xC3D3D3E7L,0x0AD33255L},{0x730D8F49L,0x7D0E95D0L},{1L,0x7DC0E625L}},{{0xAD013815L,0x7DC0E625L},{1L,0x7D0E95D0L},{0x730D8F49L,0x0AD33255L},{0xC3D3D3E7L,0xEAABF294L},{1L,(-1L)},{0x7DC0E625L,1L},{(-7L),0x99F99C93L},{0x32A40C09L,0L},{0x7DC0E625L,0xBC23E805L}},{{0L,0xEAABF294L},{0x6D8EA450L,0xEC0AC84DL},{0x730D8F49L,1L},{0x7D0E95D0L,0x7DC0E625L},{(-1L),0xAF320983L},{1L,1L},{0L,0x0AD33255L},{0x6D8EA450L,0xB54A9507L},{1L,0xBC23E805L}},{{0xAF320983L,1L},{0x32A40C09L,0xD41E0FCFL},{0x32A40C09L,1L},{0xAF320983L,0xBC23E805L},{1L,0xB54A9507L},{0x6D8EA450L,0x0AD33255L},{0L,1L},{1L,0xAF320983L},{(-1L),0x7DC0E625L}},{{0x7D0E95D0L,1L},{0x730D8F49L,0xEC0AC84DL},{0x6D8EA450L,0xEAABF294L},{0L,0xBC23E805L},{0x7DC0E625L,0L},{0x32A40C09L,0L},{1L,0L},{0xAD013815L,0xE8F23450L},{0L,0xAF320983L}},{{0x54AFE2CEL,(-1L)},{0xEC0AC84DL,(-5L)},{0xD41E0FCFL,0xAD013815L},{(-9L),0xAD013815L},{0xD41E0FCFL,(-5L)},{0xEC0AC84DL,(-1L)},{0x54AFE2CEL,0xAF320983L},{0L,0xE8F23450L},{0xAD013815L,0L}},{{1L,0L},{0xB54A9507L,0xD9848F10L},{0xAD013815L,(-1L)},{0xD9848F10L,0xAF320983L},{(-7L),0xCAB8846DL},{0xEC0AC84DL,0xD41E0FCFL},{(-5L),0xAD013815L},{0x7D0E95D0L,0xCF3DBB26L},{0xD41E0FCFL,0xD41E0FCFL}},{{0x61B763A1L,(-1L)},{(-7L),0L},{0L,(-1L)},{0xCF3DBB26L,0L},{0xB54A9507L,0L},{0xB54A9507L,0L},{0xCF3DBB26L,(-1L)},{0L,0L},{(-7L),(-1L)}},{{0x61B763A1L,0xD41E0FCFL},{0xD41E0FCFL,0xCF3DBB26L},{0x7D0E95D0L,0xAD013815L},{(-5L),0xD41E0FCFL},{0xEC0AC84DL,0xCAB8846DL},{(-7L),0xAF320983L},{0xD9848F10L,(-1L)},{0xAD013815L,0xD9848F10L},{0xB54A9507L,0L}}};
            const uint8_t *l_2974 = &g_1515;
            const uint8_t **l_2973 = &l_2974;
            const int32_t *l_2982 = &g_2341;
            const int32_t **l_2981 = &l_2982;
            int32_t *l_3011[10][1] = {{&g_1780},{&l_2845},{&g_1780},{&l_2845},{&g_1780},{&l_2845},{&g_1780},{&l_2845},{&g_1780},{&l_2845}};
            uint32_t l_3012[2][4] = {{0x9786E082L,0x9786E082L,0x9786E082L,0x9786E082L},{0x9786E082L,0x9786E082L,0x9786E082L,0x9786E082L}};
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_2893[i] = (void*)0;
            for (g_482.f0 = 1; (g_482.f0 > 29); g_482.f0++)
            { /* block id: 1222 */
                uint32_t l_2837 = 6UL;
                for (g_103 = 0; (g_103 != 29); g_103++)
                { /* block id: 1225 */
                    struct S1 l_2840 = {18446744073709551614UL};
                    for (g_2664.f1 = 0; (g_2664.f1 != (-20)); --g_2664.f1)
                    { /* block id: 1228 */
                        uint16_t *l_2847 = &g_2820;
                        if ((*p_18))
                            break;
                        ++l_2837;
                        (*g_595) = l_2840;
                        l_2817[7][2][4] |= (((*l_2847) = ((*l_2815) = (safe_rshift_func_int16_t_s_u(((safe_add_func_uint8_t_u_u((l_2840.f0 | p_19), (l_2845 = (255UL > 0xBAL)))) , l_2837), l_2846[4])))) && (((p_19 < 1L) || ((*g_32) = (*g_32))) ^ (*g_2111)));
                    }
                    for (g_2334 = 0; (g_2334 >= 58); g_2334 = safe_add_func_int32_t_s_s(g_2334, 4))
                    { /* block id: 1240 */
                        int32_t l_2856 = 0x5A5B0A26L;
                        int32_t **l_2858[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_2858[i] = &g_310;
                        (*g_2050) |= (safe_mul_func_int16_t_s_s(l_2817[1][0][7], (l_2852 == (void*)0)));
                        l_2856 = (((&p_18 == (void*)0) <= (!l_2817[8][0][3])) <= (++g_69));
                        (*g_642) = l_2857;
                    }
                    if ((**g_2052))
                        break;
                    if (l_2816)
                        continue;
                }
                return l_2859;
            }
            if (((g_1534.f7 ^= (safe_div_func_uint32_t_u_u(((safe_rshift_func_uint16_t_u_s(((*l_2815)++), 9)) == (safe_add_func_int16_t_s_s((safe_unary_minus_func_int8_t_s((((((p_19 != (safe_add_func_uint64_t_u_u(((((((((safe_mod_func_int64_t_s_s((((+(safe_mul_func_uint8_t_u_u(((*l_2903) ^= (safe_sub_func_int32_t_s_s(l_2845, (l_2817[7][2][4] = ((((safe_add_func_int32_t_s_s(((safe_mod_func_uint8_t_u_u(((((*l_2899) &= (((safe_div_func_uint32_t_u_u(p_19, (safe_lshift_func_int16_t_s_s(((0x802AL ^ ((0xBAFBL && ((safe_rshift_func_int16_t_s_s(((*l_2888) = l_2817[7][2][4]), 5)) ^ (0x7910DF89L != (safe_div_func_int32_t_s_s(((18446744073709551612UL & (safe_mul_func_uint8_t_u_u((((*g_1496) = func_62((--(*g_32)), (l_2894 &= (l_2898 == (void*)0)), p_19)) , 0x50L), 2L))) == (-1L)), l_2828))))) , 1UL)) ^ l_2895), 6)))) >= l_2829) | 1UL)) ^ g_23[4][2][0]) > (*p_18)), l_2828)) ^ p_19), g_1515)) ^ 0UL) , g_2137.f1) == 1UL))))), 0x71L))) ^ l_2859.f0) < g_2133.f7), p_19)) & l_2859.f0) | p_19) || l_2905) != p_19) & l_2859.f0) == 0xC1046F9AF6D0E0ACLL) && 0x9F7D7564DA0FC46ALL), 0L))) || p_19) || 0x8EL) ^ l_2906) >= l_2816))), l_2859.f0))), 4UL))) != g_1372[1][6][0].f0))
            { /* block id: 1260 */
                uint32_t l_2915[2][3] = {{0x1D635859L,4294967295UL,0x1D635859L},{0x1D635859L,4294967295UL,0x1D635859L}};
                int8_t l_2931 = 1L;
                int i, j;
                (*l_2899) &= (safe_sub_func_uint32_t_u_u((l_2817[0][2][3] &= (safe_div_func_uint32_t_u_u((safe_add_func_uint64_t_u_u((((*p_17) = ((*p_17) != (((g_2 , p_19) , ((safe_mod_func_uint16_t_u_u(l_2915[1][0], (safe_sub_func_int32_t_s_s((safe_mod_func_int8_t_s_s((((0x52L & (((((*g_32)++) >= (safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s((p_19 > ((*g_891) = (((safe_mod_func_uint64_t_u_u((safe_div_func_int32_t_s_s(((g_1426 < 0x15L) ^ (!p_19)), l_2906)), l_2816)) == 4L) || l_2845))), p_19)), (*p_18)))) , p_19) , l_2931)) <= p_19) > p_19), l_2932)), p_19)))) , (*p_17))) != 0x1.0p+1))) , 18446744073709551614UL), l_2906)), l_2933[3]))), 0xD7ECE2AFL));
                return l_2934;
            }
            else
            { /* block id: 1267 */
                int32_t *l_2935 = &l_2895;
                int32_t *l_2936 = &l_2894;
                int32_t *l_2937 = (void*)0;
                int32_t *l_2938[2][10] = {{&g_2,&l_2817[7][2][4],&g_2341,(void*)0,&g_2341,&l_2817[7][2][4],&g_2,&g_2,&l_2817[7][2][4],&g_2341},{&l_2817[7][2][4],&g_2,&g_2,&l_2817[7][2][4],&g_2341,(void*)0,&g_2341,&l_2817[7][2][4],&g_2,&g_2}};
                struct S1 l_2948 = {18446744073709551612UL};
                uint32_t l_2967 = 0x36F6B4B8L;
                int32_t **l_3004[5][1];
                uint16_t *l_3007 = &g_2810;
                int i, j;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_3004[i][j] = &l_2938[0][0];
                }
                l_2942++;
                for (g_1682 = 10; (g_1682 >= 1); g_1682 = safe_sub_func_int8_t_s_s(g_1682, 4))
                { /* block id: 1271 */
                    uint8_t l_2947 = 1UL;
                    (*p_17) = l_2947;
                    l_2948 = l_2859;
                    (*g_2050) ^= ((*l_2936) = l_2817[5][1][7]);
                }
                for (g_2338.f3 = 5; (g_2338.f3 >= 0); g_2338.f3 -= 1)
                { /* block id: 1279 */
                    struct S1 l_2950 = {0xE3008BFBL};
                    int64_t l_2969 = (-1L);
                    int32_t l_2970[4][2][5] = {{{0L,1L,0L,0x0754779FL,0x7AC998BAL},{0L,0L,(-1L),0L,0L}},{{1L,0xBF546C9DL,0x7ECD7655L,0x0754779FL,6L},{0x7ECD7655L,0xBF546C9DL,1L,1L,0xBF546C9DL}},{{(-1L),0L,0L,0xBF546C9DL,6L},{0L,1L,0L,0x07095D56L,0L}},{{6L,6L,1L,(-1L),0x7AC998BAL},{0L,0x7AC998BAL,0x7ECD7655L,(-1L),(-1L)}}};
                    uint8_t ***l_2971[2][8][6] = {{{&g_2156[3][0][1],&g_2156[0][2][0],(void*)0,&g_2156[3][4][3],&g_2156[3][0][1],&g_2156[3][0][1]},{&g_2156[3][0][1],(void*)0,(void*)0,(void*)0,&g_2156[3][0][1],&g_2156[3][0][1]},{&g_2156[3][0][1],(void*)0,&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],(void*)0},{(void*)0,(void*)0,&g_2156[3][0][1],&g_2156[1][6][2],&g_2156[4][5][1],&g_2156[3][0][1]},{&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[8][4][1],(void*)0,&g_2156[3][0][1]},{&g_2156[7][0][1],(void*)0,(void*)0,&g_2156[3][0][1],&g_2156[4][2][3],&g_2156[3][0][1]},{&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],(void*)0},{&g_2156[1][6][2],&g_2156[3][0][1],(void*)0,(void*)0,&g_2156[3][0][1],&g_2156[3][0][1]}},{{&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[2][1][3],(void*)0,&g_2156[3][0][1]},{&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[0][2][0],&g_2156[0][2][0],&g_2156[3][0][1],&g_2156[3][0][1]},{(void*)0,&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[1][0][1],&g_2156[1][3][2]},{&g_2156[4][2][3],&g_2156[8][4][1],&g_2156[7][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1]},{&g_2156[4][2][3],(void*)0,&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[8][4][1]},{(void*)0,&g_2156[1][0][1],&g_2156[3][0][1],&g_2156[0][2][0],(void*)0,&g_2156[3][4][3]},{&g_2156[3][0][1],&g_2156[3][0][1],&g_2156[4][5][1],&g_2156[2][1][3],&g_2156[3][0][1],&g_2156[3][0][1]},{&g_2156[3][0][1],(void*)0,(void*)0,(void*)0,&g_2156[2][1][3],&g_2156[3][0][1]}}};
                    const uint8_t ***l_2975 = (void*)0;
                    const int32_t ***l_2983 = &l_2981;
                    int i, j, k;
                    (*g_595) = ((*g_1496) = func_40(g_994[(g_2338.f3 + 2)][g_2338.f3], l_2949[1], l_2932));
                    if (((*g_32) , (p_19 <= ((*l_2898) ^= (p_19 , 0UL)))))
                    { /* block id: 1283 */
                        return l_2950;
                    }
                    else
                    { /* block id: 1285 */
                        int8_t l_2953 = 4L;
                        struct S0 ****l_2958 = &g_2956[0][1];
                        int8_t *l_2968 = (void*)0;
                        (*p_17) = ((*p_17) >= (safe_add_func_float_f_f((l_2817[7][2][1] = ((*g_8) != ((l_2953 || ((*g_891) = (l_2953 , ((safe_lshift_func_uint16_t_u_s((((((*l_2958) = g_2956[0][1]) != &g_2957) , (safe_rshift_func_uint8_t_u_u(((*l_2903)--), 5))) && (safe_rshift_func_uint8_t_u_s(((*l_2936) <= (*l_2857)), (g_2664.f3 = (safe_add_func_int32_t_s_s(((((((l_2950.f0 , l_2967) <= l_2817[7][2][4]) & l_2953) & 0UL) , p_19) >= 0xCED23828L), (*p_18))))))), l_2969)) & p_19)))) , (*l_2857)))), (*p_17))));
                        if ((*p_18))
                            break;
                        (*g_2050) = ((*l_2899) = (*l_2935));
                    }
                    (*l_2899) |= (l_2970[1][0][0] > ((*l_2936) ^ (((l_2972 = (void*)0) != (g_2976[1][0][4] = l_2973)) , ((8L <= ((safe_add_func_uint8_t_u_u(((p_19 , (((((((*l_2983) = l_2981) == (void*)0) & (safe_mul_func_int8_t_s_s((+(~(0UL != p_19))), (*g_2977)))) ^ 0xE0C345B52A9A1371LL) , p_19) ^ 255UL)) , p_19), l_2817[3][1][2])) >= (*l_2935))) | p_19))));
                    for (g_82 = 0; (g_82 <= 5); g_82 += 1)
                    { /* block id: 1302 */
                        (**l_2983) = &l_2894;
                        return (*g_1496);
                    }
                }
                (*l_2935) = (((p_19 ^ (safe_mod_func_int32_t_s_s(((((((safe_mul_func_uint16_t_u_u((safe_add_func_int32_t_s_s((safe_add_func_uint16_t_u_u((*l_2857), p_19)), (safe_sub_func_int16_t_s_s(p_19, (safe_mul_func_int8_t_s_s(p_19, (l_3010 ^= (((safe_sub_func_uint16_t_u_u(((safe_sub_func_int8_t_s_s(1L, ((*l_2903) = ((l_2899 = &l_2940[8]) == &l_2817[4][2][3])))) , ((*l_3007) ^= ((*l_2815)++))), (((((l_2940[8] = ((safe_rshift_func_int16_t_s_u((l_2940[8] , (*l_2857)), 0)) == l_2932)) , (void*)0) == (void*)0) && l_2817[7][2][4]) > (**g_2052)))) , l_2932) , p_19)))))))), p_19)) == p_19) | p_19) || p_19) < p_19) >= p_19), l_2932))) , (**g_124)) || (*g_120));
            }
            l_3012[0][1]--;
        }
        else
        { /* block id: 1316 */
            uint32_t l_3032 = 4294967295UL;
            int32_t l_3034 = 1L;
            int32_t l_3061 = 0x2B7F2D17L;
            int8_t l_3073[4];
            struct S1 l_3160 = {0xFBC298A5L};
            float ** const *l_3170 = &g_177;
            float *** const l_3172 = &g_177;
            int32_t l_3180 = 0x948FD9E4L;
            int32_t l_3183 = 0xACA19F85L;
            int32_t *l_3197 = &g_1780;
            int32_t *l_3198 = &l_2817[7][2][4];
            int8_t *l_3207 = &g_143[4];
            uint8_t *l_3208[6] = {&l_2932,&l_2932,&l_2932,&l_2932,&l_2932,&l_2932};
            int i;
            for (i = 0; i < 4; i++)
                l_3073[i] = 0x79L;
            for (g_119.f0 = 0; (g_119.f0 >= 29); g_119.f0 = safe_add_func_int8_t_s_s(g_119.f0, 2))
            { /* block id: 1319 */
                int32_t l_3026 = 0L;
                int32_t l_3027 = (-1L);
                uint8_t *l_3030 = &g_1612;
                uint8_t *l_3033[7][8][4] = {{{&l_2932,&g_2070,&g_1515,(void*)0},{&g_2070,&g_2070,&g_1515,&g_1515},{&l_2932,&l_2932,(void*)0,&g_1515},{&g_2070,&g_2070,&g_82,(void*)0},{&g_2070,&g_2070,(void*)0,&g_82},{&l_2932,&g_2070,&g_1515,(void*)0},{&g_2070,&g_2070,&g_1515,&g_1515},{&l_2932,&l_2932,(void*)0,&g_1515}},{{&g_2070,&g_2070,&g_82,(void*)0},{&g_2070,&g_2070,(void*)0,&g_82},{&l_2932,&g_2070,&g_1515,(void*)0},{&g_2070,&g_2070,&g_1515,&g_1515},{&l_2932,&l_2932,(void*)0,&g_1515},{&g_2070,&g_2070,&g_82,(void*)0},{&g_2070,&g_2070,(void*)0,&g_82},{&l_2932,&g_2070,&g_1515,(void*)0}},{{&g_2070,&g_2070,&g_1515,&g_1515},{&l_2932,&l_2932,(void*)0,&g_1515},{&g_2070,&g_2070,&g_82,(void*)0},{&g_2070,&g_2070,(void*)0,&g_82},{&l_2932,&g_2070,&g_1515,(void*)0},{&g_2070,&g_2070,&g_1515,&g_1515},{&l_2932,&l_2932,(void*)0,&g_1515},{&g_2070,&g_2070,&g_82,(void*)0}},{{&g_2070,&g_2070,(void*)0,&g_82},{&l_2932,&g_2070,&g_1515,(void*)0},{&g_2070,&g_2070,&g_1515,&g_1515},{&l_2932,&l_2932,(void*)0,&g_1515},{&g_2070,&g_2070,&g_82,(void*)0},{&g_2070,&g_2070,&g_82,&g_1515},{&g_2070,&g_2070,&g_2070,&g_82},{&g_2070,&l_2932,&g_2070,&g_2070}},{{&g_2070,&g_2070,&g_82,&g_2070},{&g_82,&l_2932,&g_1515,&g_82},{&g_82,&g_2070,&g_82,&g_1515},{&g_2070,&g_2070,&g_2070,&g_82},{&g_2070,&l_2932,&g_2070,&g_2070},{&g_2070,&g_2070,&g_82,&g_2070},{&g_82,&l_2932,&g_1515,&g_82},{&g_82,&g_2070,&g_82,&g_1515}},{{&g_2070,&g_2070,&g_2070,&g_82},{&g_2070,&l_2932,&g_2070,&g_2070},{&g_2070,&g_2070,&g_82,&g_2070},{&g_82,&l_2932,&g_1515,&g_82},{&g_82,&g_2070,&g_82,&g_1515},{&g_2070,&g_2070,&g_2070,&g_82},{&g_2070,&l_2932,&g_2070,&g_2070},{&g_2070,&g_2070,&g_82,&g_2070}},{{&g_82,&l_2932,&g_1515,&g_82},{&g_82,&g_2070,&g_82,&g_1515},{&g_2070,&g_2070,&g_2070,&g_82},{&g_2070,&l_2932,&g_2070,&g_2070},{&g_2070,&g_2070,&g_82,&g_2070},{&g_82,&l_2932,&g_1515,&g_82},{&g_82,&g_2070,&g_82,&g_1515},{&g_2070,&g_2070,&g_2070,&g_82}}};
                int32_t **l_3035 = &g_310;
                uint32_t *l_3045[4][5][5] = {{{&g_1682,&l_3032,&g_1501[0],&g_2270[1][4][3],&g_2270[4][4][1]},{&g_2270[4][4][1],&g_1501[0],&l_3032,&g_2270[4][4][1],&g_2270[1][4][3]},{&l_3032,(void*)0,&g_2270[4][4][1],&g_101[0][4][0],&g_2270[1][4][3]},{&g_1501[0],&l_3032,&g_2270[4][4][1],&l_2828,&g_2270[4][4][1]},{&l_2828,&l_2828,&g_1682,&g_1682,&g_2270[4][4][1]}},{{(void*)0,&l_2828,&g_1501[0],&g_101[0][4][0],&g_1501[0]},{&l_3032,&g_1501[0],&g_2270[4][4][1],&g_2270[1][4][3],&g_1501[0]},{&g_1682,&l_2828,&g_1682,&l_3032,&l_2828},{&g_101[0][4][0],&l_2828,&g_101[0][4][0],(void*)0,(void*)0},{&l_3032,&l_3032,&g_1501[0],&l_2828,&g_1501[2]}},{{&l_2828,(void*)0,&l_3032,&l_2828,&l_3032},{&g_1501[2],&g_1501[0],(void*)0,(void*)0,&g_1501[0]},{&g_1501[0],&l_3032,&g_1501[2],&l_3032,&g_2270[4][4][1]},{&g_1682,&l_2828,&g_2270[3][3][1],&g_2270[1][4][3],(void*)0},{&g_101[0][4][0],&g_1501[0],&g_101[0][4][0],&g_101[0][4][0],&g_2270[1][4][3]}},{{&g_1682,(void*)0,&g_2270[4][4][1],&g_1682,&g_1501[0]},{&g_1501[0],&g_1682,&g_1682,&g_1501[0],(void*)0},{&g_1682,&g_1501[0],&g_1682,&g_1501[0],&g_2270[4][4][1]},{&g_1501[0],&l_2828,&g_1682,&g_1501[2],&l_3032},{(void*)0,&l_3032,&l_2828,&l_3032,(void*)0}}};
                uint8_t l_3163 = 0UL;
                int32_t l_3185 = 0xF069DCF9L;
                const uint8_t ***l_3193 = &g_2976[0][0][4];
                const uint8_t ****l_3192 = &l_3193;
                int i, j, k;
                (*l_3035) = ((safe_lshift_func_int16_t_s_s((safe_unary_minus_func_uint8_t_u(((safe_mul_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_u((l_3034 = (((l_2817[1][1][2] = (((l_3026 != ((l_3027 ^= p_19) > (((void*)0 != (*g_2290)) ^ (((p_19 | ((*l_3030) ^= (((*l_2815) &= (safe_sub_func_uint32_t_u_u(1UL, (*g_2111)))) && p_19))) && (p_19 ^ 8L)) != 0xA937604EL)))) < l_3031) | l_3032)) | 0L) | 18446744073709551609UL)), p_19)) && l_2845), 0x1130L)) , 251UL))), 0)) , (void*)0);
                (*g_2050) &= (*p_18);
                if ((safe_div_func_uint64_t_u_u(18446744073709551615UL, ((l_2942 >= (l_2845 = ((safe_mod_func_uint8_t_u_u(p_19, ((l_3031 > 0x3FL) , (((g_3046 = ((l_2906 & (safe_rshift_func_uint16_t_u_u((&g_120 != (void*)0), (safe_unary_minus_func_uint16_t_u(((((l_3010 = (safe_div_func_int64_t_s_s(p_19, 0xA3B2FDED3778ED51LL))) ^ l_3034) && l_2845) == l_2828)))))) < (*p_18))) >= (*p_18)) && 1UL)))) , 0x8115A79AL))) , l_2817[6][0][0]))))
                { /* block id: 1330 */
                    struct S1 l_3049 = {0xA546BE54L};
                    int32_t l_3053 = 0xE92C77EAL;
                    int32_t l_3057 = 0L;
                    int32_t l_3058 = 1L;
                    int32_t l_3059 = 0x0660F770L;
                    int32_t l_3060[8] = {(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)};
                    const struct S0 * const l_3077 = &g_1262;
                    const struct S0 * const *l_3076 = &l_3077;
                    const struct S0 * const **l_3075 = &l_3076;
                    const struct S0 * const ***l_3074 = &l_3075;
                    int32_t *l_3078 = (void*)0;
                    int32_t *l_3079 = &l_3060[0];
                    int i;
                    if ((safe_rshift_func_int8_t_s_s(l_2829, 6)))
                    { /* block id: 1331 */
                        if (l_3026)
                            break;
                    }
                    else
                    { /* block id: 1333 */
                        return l_3049;
                    }
                    for (g_86 = 0; (g_86 == (-23)); g_86--)
                    { /* block id: 1338 */
                        int16_t l_3052 = 0L;
                        int32_t *l_3054 = (void*)0;
                        int32_t *l_3055 = (void*)0;
                        int32_t *l_3056[9][4][4];
                        uint8_t l_3062 = 0xF6L;
                        int i, j, k;
                        for (i = 0; i < 9; i++)
                        {
                            for (j = 0; j < 4; j++)
                            {
                                for (k = 0; k < 4; k++)
                                    l_3056[i][j][k] = (void*)0;
                            }
                        }
                        if (l_3052)
                            break;
                        (*l_3035) = p_17;
                        --l_3062;
                    }
                    (*l_3079) &= (safe_rshift_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((((*l_2821) = &g_953) != &g_953), p_19)) < ((g_3069 , (safe_sub_func_float_f_f((((l_3027 = ((*p_17) = (+(*p_17)))) < 0x5.C697F8p-7) <= (l_3073[3] == l_3031)), ((void*)0 != l_3074)))) , p_19)), p_19));
                }
                else
                { /* block id: 1347 */
                    int32_t l_3115 = (-1L);
                    int32_t l_3118 = 1L;
                    int32_t l_3120 = 0x15EEDE86L;
                    int16_t **l_3179 = &g_1763;
                    if ((safe_mod_func_uint32_t_u_u((((((((safe_sub_func_uint8_t_u_u((safe_mod_func_int16_t_s_s((-1L), p_19)), p_19)) & l_3034) ^ (safe_mod_func_int8_t_s_s(p_19, (safe_add_func_uint8_t_u_u(p_19, ((((safe_div_func_int32_t_s_s((*p_18), (*g_2111))) ^ (!p_19)) > l_2845) < p_19)))))) >= p_19) == 0x4C7119DB0218B75ALL) <= 0UL) , 1UL), (*p_18))))
                    { /* block id: 1348 */
                        uint16_t l_3095 = 1UL;
                        int32_t *l_3103 = &l_2817[7][2][4];
                        int16_t *l_3116 = (void*)0;
                        int16_t *l_3117[1];
                        int8_t *l_3119 = &g_2107.f3;
                        int32_t *l_3146[2][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0}};
                        int64_t l_3147 = 0x10076E99F3C71A3CLL;
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_3117[i] = &g_508;
                        (*g_2050) = ((l_3120 ^= (p_19 , ((*l_3119) = (safe_lshift_func_int8_t_s_s(((--l_3095) , (((safe_rshift_func_int8_t_s_u((safe_add_func_uint16_t_u_u(((0x1AL == 255UL) || ((*l_3103) = l_3102)), p_19)), 4)) > ((safe_rshift_func_int8_t_s_u(p_19, 0)) <= (((safe_rshift_func_int16_t_s_s(((((l_3010 = ((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_s((l_3118 |= (safe_unary_minus_func_uint16_t_u((safe_div_func_int16_t_s_s((p_19 & 0xFF27L), l_3115))))), l_3073[3])), (-1L))) >= p_19)) < l_2816) | (*g_2050)) ^ (*g_2111)), p_19)) , l_2940[1]) , p_19))) == 65528UL)), p_19))))) && (*g_2977));
                        (*g_120) &= (l_3147 = ((*l_3103) |= (p_19 > ((safe_lshift_func_int8_t_s_u((p_19 , (safe_sub_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(((safe_add_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u(l_3131, 4)) , l_3120), 0UL)) & ((safe_add_func_uint32_t_u_u(((safe_div_func_uint8_t_u_u(((l_3131 & (((*g_891) | (safe_unary_minus_func_uint8_t_u((((safe_mul_func_int8_t_s_s((((safe_mul_func_uint16_t_u_u(65535UL, ((safe_sub_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s(p_19, l_3031)), 0x665FECB4ED589E3DLL)) | l_3118))) || g_2648.f3) <= 0x41E39EFBL), 0x33L)) == p_19) , p_19)))) == 0x8CL)) >= p_19), l_3032)) == g_2015.f0), (*p_18))) == 0xF9L)), p_19)), (*p_18)))), 2)) , p_19))));
                        l_3118 = (safe_sub_func_float_f_f((safe_sub_func_float_f_f(((l_2817[7][2][4] = (((**g_297) , (safe_sub_func_float_f_f((-0x9.5p-1), (safe_div_func_float_f_f(l_3118, ((*p_17) = (*p_17))))))) >= (((safe_rshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((1UL && (l_3160 , (1L | 0x4A5AE68B2E11A86ALL))), (safe_sub_func_uint32_t_u_u(((l_2816 , (*l_3103)) | (-5L)), g_2133.f5)))), l_3010)) >= (-2L)) , (-0x10.6p-1)))) != l_3163), p_19)), p_19));
                    }
                    else
                    { /* block id: 1362 */
                        float ** const **l_3171 = &l_3170;
                        int16_t *l_3181 = &g_508;
                        int32_t *l_3184 = &l_3027;
                        (*l_3184) &= (safe_rshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s(((0x5E95A930C5E92F0CLL == 0x8BC3F300ADE3F0CDLL) , ((l_3183 = (safe_add_func_uint16_t_u_u((((((*l_3171) = l_3170) == l_3172) <= g_2137.f1) != ((*g_891) = (((safe_mod_func_int16_t_s_s(((*l_3181) = ((((safe_sub_func_int16_t_s_s(l_3131, 0x6E47L)) >= 18446744073709551615UL) == (safe_lshift_func_int8_t_s_u(((void*)0 != l_3179), 6))) <= l_3180)), p_19)) < l_3182[0][4][3]) | (*g_2111)))), l_2859.f0))) | p_19)), p_19)), 9));
                        if (l_3185)
                            break;
                        if (l_3073[3])
                            break;
                    }
                }
                l_2894 = (safe_sub_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((*g_2977), ((p_19 > 18446744073709551615UL) == ((((*g_891) | (safe_mod_func_uint8_t_u_u((l_3160.f0 < (((*l_3192) = &g_2976[1][0][0]) == (l_2816 , l_3194))), (safe_add_func_uint64_t_u_u((p_19 | (*g_2977)), 0UL))))) , 0x3E95L) | p_19)))), 0xDABAL));
            }
            (*g_2050) ^= (-2L);
            l_2845 |= ((((((*l_3198) = ((*l_3197) ^= l_3032)) , ((((*p_18) <= l_2816) != ((0x95B2L || (~(((((p_16 != (void*)0) , ((safe_mod_func_uint16_t_u_u((((*l_3198) = ((+l_3182[3][1][0]) || (((safe_rshift_func_uint8_t_u_u((((*l_3207) = ((safe_sub_func_int16_t_s_s(p_19, ((l_2828 || 65529UL) == p_19))) & p_19)) | (*l_3198)), p_19)) < p_19) || (-1L)))) & 0x15L), 0xD62DL)) , p_19)) , g_994[5][0]) != 0xD7225D55L) , p_19))) && p_19)) & 0L)) == 4L) < l_2942) , 0x42A47FCDL);
            for (l_2828 = (-15); (l_2828 < 43); l_2828++)
            { /* block id: 1383 */
                uint16_t l_3211 = 0xBC99L;
                const uint8_t ***l_3223 = &g_2976[1][0][4];
                const uint8_t ****l_3222[1][6][5] = {{{&l_3223,&l_3223,&l_3223,(void*)0,&l_3223},{&l_3223,(void*)0,(void*)0,&l_3223,(void*)0},{&l_3223,&l_3223,&l_3223,&l_3223,&l_3223},{&l_3223,&l_3223,(void*)0,(void*)0,&l_3223},{&l_3223,(void*)0,&l_3223,&l_3223,(void*)0},{&l_3223,&l_3223,&l_3223,&l_3223,&l_3223}}};
                struct S1 l_3231 = {0UL};
                int i, j, k;
                for (g_1461.f1 = 3; (g_1461.f1 >= 0); g_1461.f1 -= 1)
                { /* block id: 1386 */
                    float ** const l_3249 = &l_2949[1];
                    for (g_209 = 3; (g_209 >= 0); g_209 -= 1)
                    { /* block id: 1389 */
                        (*g_120) |= (-1L);
                    }
                    if (l_3211)
                        continue;
                    if (((*l_3197) ^= l_3211))
                    { /* block id: 1394 */
                        if ((*l_3198))
                            break;
                        (*l_3198) |= (**g_124);
                    }
                    else
                    { /* block id: 1397 */
                        float l_3214 = 0x0.7EFEB4p-87;
                        uint32_t *l_3218 = &g_1501[0];
                        uint32_t *l_3221 = &g_101[0][4][0];
                        uint8_t ** const *l_3225 = (void*)0;
                        uint8_t ** const **l_3224 = &l_3225;
                        int32_t **l_3230 = &l_3197;
                        (*g_120) |= ((safe_mod_func_int8_t_s_s(((l_3010 != (l_3215 | (safe_sub_func_uint32_t_u_u(((*l_3221) ^= (++(*l_3218))), (l_3222[0][2][2] != l_3224))))) & (0xC1L < (safe_mul_func_uint8_t_u_u((0x82A4C0C5L & (safe_add_func_int16_t_s_s((((*l_2815) = l_3010) , p_19), 1L))), p_19)))), l_3182[0][4][3])) < l_3102);
                        (*g_1496) = l_2859;
                        (*l_3230) = &l_3010;
                    }
                    for (g_371 = 0; (g_371 <= 3); g_371 += 1)
                    { /* block id: 1407 */
                        struct S1 l_3232 = {0UL};
                        uint16_t **l_3251 = (void*)0;
                        l_3232 = (l_3231 = ((*g_1496) = l_2859));
                        g_454 = (((l_2845 = (safe_lshift_func_int8_t_s_s((safe_mod_func_uint8_t_u_u(((safe_unary_minus_func_int8_t_s(p_19)) == (safe_mod_func_uint8_t_u_u(0x78L, p_19))), (safe_sub_func_uint8_t_u_u(251UL, ((((p_19 > ((func_62((safe_mod_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u(g_3246, 4)) >= (safe_mul_func_uint16_t_u_u((*l_3197), (g_1262.f3 , g_2086.f5)))), (*l_3197))), l_2940[8], p_19) , &g_2157) == &g_2977)) == p_19) , l_3249) != l_3250))))), 3))) || l_3010) , l_3251);
                        return l_3252;
                    }
                }
            }
        }
    }
    return l_3252;
}


/* ------------------------------------------ */
/* 
 * reads : g_23 g_2 g_32 g_60 g_69 g_61.f3 g_5 g_61.f1 g_61.f5 g_61.f4 g_101 g_33 g_86 g_103 g_61.f2 g_105 g_61.f6 g_110 g_120 g_124 g_133 g_61.f0 g_119.f0 g_82 g_158 g_176 g_8 g_9 g_177 g_119 g_121 g_116 g_143 g_209 g_240 g_133.f3 g_61.f7 g_296 g_297 g_220 g_241 g_309 g_310 g_360 g_374 g_371 g_61 g_158.f3 g_450 g_454 g_404 g_465 g_476 g_298 g_479 g_481 g_484 g_560 g_158.f0 g_574 g_548.f0 g_594 g_642 g_595 g_653 g_668 g_677 g_342 g_737 g_596 g_133.f0 g_574.f0 g_890 g_891 g_968 g_970 g_994 g_548.f1 g_508 g_968.f3 g_999 g_355 g_1214 g_482.f0 g_1201 g_1262 g_1339 g_1372 g_1409 g_1426 g_1461 g_1472 g_548.f3 g_1501 g_1515 g_1461.f3 g_574.f3 g_1534 g_1560 g_1745 g_1748 g_2137 g_2464 g_2465 g_2107.f3 g_2499 g_2504 g_2050 g_1555 g_2345.f5 g_2524 g_2525 g_1496 g_2157 g_2566 g_2416 g_2338.f3 g_2648 g_2052 g_2664 g_2289 g_2341 g_1780 g_2648.f1 g_2664.f1
 * writes: g_23 g_69 g_82 g_86 g_61.f5 g_101 g_103 g_105 g_116 g_119 g_120 g_121 g_61.f4 g_177 g_9 g_33 g_143 g_298 g_310 g_240 g_374 g_450 g_371 g_482 g_476 g_355 g_508 g_596 g_677 g_688 g_60 g_404 g_953 g_999 g_548.f1 g_1201 g_209 g_1409 g_548.f3 g_1495 g_1515 g_1461.f3 g_891 g_1561 g_1743 g_1745 g_1763 g_1764 g_2107.f3 g_1555 g_2157 g_2107.f1 g_1780 g_1682 g_2334 g_2648.f1 g_2664.f1
 */
static int32_t * const  func_20(int32_t * p_21)
{ /* block id: 18 */
    uint64_t *l_22 = &g_23[2][1][0];
    int32_t l_28 = (-7L);
    int64_t *l_1742 = &g_1743;
    int64_t *l_1744 = &g_1745;
    int16_t *l_1761 = &g_103;
    int8_t l_1879 = 6L;
    int32_t l_1885 = 0xCED7C6C5L;
    int32_t l_1887[6] = {0x8B9DDA26L,0x8B9DDA26L,0x8B9DDA26L,0x8B9DDA26L,0x8B9DDA26L,0x8B9DDA26L};
    int8_t l_1940 = 1L;
    float ***l_1963[6];
    uint8_t l_2084 = 1UL;
    struct S0 *l_2136 = &g_2137;
    uint16_t l_2209[5][6] = {{65533UL,4UL,65533UL,65533UL,4UL,65533UL},{65533UL,4UL,65533UL,65533UL,4UL,65533UL},{65533UL,4UL,65533UL,65533UL,4UL,65533UL},{65533UL,4UL,65533UL,65533UL,4UL,65533UL},{65533UL,4UL,65533UL,65533UL,4UL,65533UL}};
    union U2 *l_2320 = &g_2073[0];
    union U2 **l_2319 = &l_2320;
    uint16_t *l_2325 = &g_69;
    uint16_t **l_2324 = &l_2325;
    int64_t *** const ** const l_2401 = (void*)0;
    uint64_t l_2426[3];
    int8_t **l_2463 = &g_2289;
    int8_t ***l_2462 = &l_2463;
    int32_t *l_2502 = &l_28;
    int8_t l_2515 = (-1L);
    const int32_t l_2528[7][2][2] = {{{0xB5727E86L,5L},{0x5849F6E1L,0x9824FB54L}},{{0xE1066D13L,0xC89E04C0L},{0x5D724F21L,0xE1066D13L}},{{(-1L),0x881A6BBBL},{(-1L),0xE1066D13L}},{{0x5D724F21L,0xC89E04C0L},{0xE1066D13L,0x9824FB54L}},{{0x5849F6E1L,5L},{0xB5727E86L,0xB20CA598L}},{{0xB20CA598L,0x5D724F21L},{0xEAAE2A3EL,0x5D724F21L}},{{0xB20CA598L,0xB20CA598L},{0xB5727E86L,5L}}};
    uint32_t l_2588 = 0xE7900332L;
    uint64_t l_2630 = 18446744073709551612UL;
    int32_t *l_2699 = &g_1780;
    uint8_t *l_2700[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const struct S1 l_2726 = {18446744073709551615UL};
    int64_t l_2781 = 0xC3D3FF11BC888189LL;
    uint32_t l_2784 = 0UL;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1963[i] = &g_177;
    for (i = 0; i < 3; i++)
        l_2426[i] = 0UL;
    if ((((++(*l_22)) || g_2) < (l_28 = ((((safe_mul_func_uint16_t_u_u(l_28, g_2)) || func_29(l_28, (g_32 != l_22))) > (safe_sub_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((((*l_1744) &= ((*l_1742) = ((((safe_rshift_func_uint8_t_u_s(0UL, ((safe_mul_func_int16_t_s_s((l_28 > 0xF26E375574F60EE6LL), (-9L))) , l_28))) < l_28) || 0x88L) != 65534UL))) && l_28), 15)), l_28))) ^ 0xD1E066AF290F72FALL))))
    { /* block id: 693 */
        int16_t **l_1762[4][5][2] = {{{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761}},{{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761}},{{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761}},{{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761},{&l_1761,&l_1761}}};
        uint16_t * const l_1767 = &g_69;
        int32_t l_1768 = 0L;
        struct S1 **l_1800 = &g_1496;
        float ****l_1866 = (void*)0;
        float ****l_1867 = &g_241;
        int32_t l_1884 = 0x66052DBCL;
        int32_t l_1888 = 0xAA229768L;
        int32_t l_1891 = 0x0F1E569BL;
        uint64_t l_1939[10] = {0x8E5C97493B576298LL,0x8E5C97493B576298LL,0xCF3C38EFF2801407LL,0x39393F5684B0DFABLL,0xCF3C38EFF2801407LL,0x8E5C97493B576298LL,0x8E5C97493B576298LL,0xCF3C38EFF2801407LL,0x39393F5684B0DFABLL,0xCF3C38EFF2801407LL};
        const uint64_t *l_1947 = (void*)0;
        const uint64_t **l_1946 = &l_1947;
        int32_t l_2061 = 0L;
        int32_t l_2064 = 0x044F218FL;
        int32_t l_2065[5];
        uint16_t l_2121 = 65535UL;
        uint32_t * const l_2130 = &g_1201;
        struct S0 *l_2132[1][9] = {{&g_2134,&g_2133,&g_2133,&g_2134,&g_2133,&g_2133,&g_2134,&g_2133,&g_2133}};
        uint32_t l_2210 = 5UL;
        int64_t l_2216 = (-1L);
        const uint64_t l_2265 = 0x7B9A6EAC713916A9LL;
        union U2 *l_2274[6][1] = {{&g_548},{&g_2073[2]},{&g_2073[3]},{&g_2073[3]},{&g_2073[2]},{&g_2073[3]}};
        union U2 **l_2273[6] = {&l_2274[1][0],&l_2274[1][0],&l_2274[1][0],&l_2274[1][0],&l_2274[1][0],&l_2274[1][0]};
        uint16_t **l_2339 = &l_2325;
        int32_t l_2374 = 0xC4A3C352L;
        uint32_t l_2433 = 0UL;
        struct S1 ***l_2477 = (void*)0;
        int32_t *l_2503[8] = {&l_28,&l_1887[3],&l_1887[3],&l_28,&l_1887[3],&l_1887[3],&l_28,&l_1887[3]};
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_2065[i] = 0xE071CAB4L;
        if (((safe_sub_func_uint8_t_u_u((g_1748 , ((1L ^ ((!(safe_mod_func_int8_t_s_s(g_101[1][4][0], 0xAEL))) || (safe_mul_func_int8_t_s_s((safe_div_func_int16_t_s_s(((safe_unary_minus_func_uint16_t_u(((safe_div_func_int32_t_s_s((safe_rshift_func_uint8_t_u_u(((l_1761 = (g_1763 = l_1761)) == (g_1764 = &g_999[0][4])), (safe_sub_func_int32_t_s_s(((((void*)0 != l_1767) , l_28) <= 0UL), l_1768)))), 0x18E8E619L)) , l_1768))) | l_28), 3UL)), 3L)))) && 0xE881E5ECL)), l_28)) ^ 65535UL))
        { /* block id: 697 */
            int32_t *l_1769 = &l_28;
            struct S0 **l_1793 = &g_60;
            struct S0 ***l_1792 = &l_1793;
            int32_t *l_1809 = (void*)0;
            struct S1 l_1810 = {1UL};
            float **l_1829 = (void*)0;
            int32_t l_1889 = (-1L);
            int32_t l_1890 = (-4L);
            struct S1 **l_1900 = &g_1496;
            struct S1 *****l_1931 = &g_1561;
            int32_t l_1954 = 1L;
            float ***l_2038[8][2][9] = {{{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829},{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829}},{{&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829,&l_1829,&l_1829},{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829}},{{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829},{&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829,&l_1829,&l_1829}},{{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829},{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829}},{{&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829,&l_1829,&l_1829},{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829}},{{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829},{&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829,&l_1829,&l_1829}},{{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829},{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829,&l_1829}},{{&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829,&l_1829,&l_1829},{&g_177,&g_177,&l_1829,&l_1829,&l_1829,&l_1829,(void*)0,&l_1829,&l_1829}}};
            uint32_t l_2041 = 0xFB72A334L;
            uint8_t *l_2053 = &g_82;
            int32_t l_2057 = 0xCE732B9EL;
            int32_t l_2058[5][2][10] = {{{7L,0L,0L,8L,0xF6092038L,0x906056CCL,0x49628F99L,0x49628F99L,0x906056CCL,0xF6092038L},{1L,7L,7L,1L,0x863550A7L,0x906056CCL,0xEB3E2729L,0L,0xF6092038L,0L}},{{7L,0L,(-4L),0L,(-4L),0L,7L,8L,(-4L),0xF6092038L},{8L,0L,0L,0L,1L,1L,0L,0L,0L,8L}},{{0x906056CCL,0L,(-4L),0xC097C9C5L,0x863550A7L,0xF6092038L,1L,0xF6092038L,0x863550A7L,0xC097C9C5L},{0xC097C9C5L,0x49628F99L,0xC097C9C5L,0L,0x863550A7L,7L,8L,0xEB3E2729L,0xEB3E2729L,8L}},{{0x863550A7L,1L,7L,7L,1L,0x863550A7L,0x906056CCL,0xEB3E2729L,0L,0xF6092038L},{0x49628F99L,0xF6092038L,0xC097C9C5L,0L,0xEB3E2729L,0L,0xC097C9C5L,0xF6092038L,0x49628F99L,0x863550A7L}},{{0x49628F99L,7L,(-4L),0x906056CCL,0L,0x863550A7L,0x863550A7L,0L,0x906056CCL,(-4L)},{0x863550A7L,0x863550A7L,0L,0x906056CCL,(-4L),7L,0x49628F99L,8L,0x49628F99L,7L}}};
            const uint32_t l_2085 = 0xC0A90451L;
            int16_t *l_2106 = &g_103;
            int32_t *l_2112 = &l_28;
            int32_t *l_2113 = &l_2065[1];
            int32_t *l_2114 = (void*)0;
            int32_t *l_2115 = &l_2057;
            int32_t *l_2116 = &l_2058[1][1][7];
            int32_t *l_2117 = &g_1780;
            int32_t *l_2118 = &g_1780;
            int32_t *l_2119[5][6][4] = {{{&l_2057,&l_1887[0],&g_105,&l_1889},{&l_1885,&l_28,(void*)0,&g_5},{&g_2,&l_1768,&l_2058[1][0][9],(void*)0},{&l_2064,&l_2064,&g_105,(void*)0},{&l_1887[0],&l_1887[0],&l_1890,&l_2064},{&l_2057,&g_105,&l_1887[3],&l_1888}},{{&l_1888,&g_1780,&l_1888,&l_28},{&g_5,&l_2061,&l_2064,&l_2065[1]},{&g_105,&l_1889,&g_1780,&l_2061},{&l_2065[1],&g_105,&g_1780,&l_2064},{&g_105,(void*)0,&l_2064,&l_1887[0]},{&g_5,(void*)0,&l_1888,&l_1887[2]}},{{&l_1888,&l_1887[2],&l_1887[3],&l_2064},{&l_2057,(void*)0,&l_1890,(void*)0},{&l_1887[0],&l_1890,&g_105,&l_1890},{&l_2064,&l_2065[0],&l_2058[1][0][9],(void*)0},{&g_2,&g_105,(void*)0,&l_1887[3]},{&l_1885,&l_2064,&g_105,&g_105}},{{&l_2057,&l_2057,&l_2064,&l_1890},{&l_1887[2],&g_1780,&g_105,&l_1891},{&l_28,&g_105,&l_1768,&g_105},{&g_1780,&g_105,&l_1887[0],&l_1891},{&g_105,&g_1780,(void*)0,&l_1890},{&l_1890,&l_2057,&l_1887[0],&g_105}},{{(void*)0,&l_2064,&l_1890,&l_1887[3]},{(void*)0,&g_105,&l_2064,(void*)0},{&l_1890,&l_2065[0],&l_28,&l_1890},{(void*)0,&l_1890,&l_2057,(void*)0},{&l_28,(void*)0,&l_1890,&l_2064},{(void*)0,&l_1887[2],&l_1887[3],(void*)0}}};
            int32_t l_2120 = 9L;
            uint32_t l_2176 = 0xA3808852L;
            int64_t ** const l_2184 = &l_1744;
            int64_t ** const *l_2183 = &l_2184;
            int64_t ** const **l_2182 = &l_2183;
            int64_t ** const *** const l_2181 = &l_2182;
            union U2 *l_2272 = &g_1461;
            union U2 **l_2271 = &l_2272;
            struct S1 *l_2286 = &g_482;
            int16_t l_2423 = 0x4725L;
            int i, j, k;
        }
        else
        { /* block id: 1017 */
            int32_t * const l_2466 = &l_2061;
            const struct S1 *l_2480 = &g_482;
            const struct S1 **l_2479 = &l_2480;
            const struct S1 ***l_2478 = &l_2479;
            struct S1 ***l_2482 = (void*)0;
            int16_t l_2494 = 0L;
            int32_t **l_2500 = (void*)0;
            int32_t **l_2501[6][6][1] = {{{(void*)0},{&g_310},{&g_310},{(void*)0},{&g_310},{(void*)0}},{{&g_310},{&g_310},{&g_310},{&g_310},{(void*)0},{&g_310}},{{(void*)0},{&g_310},{&g_310},{(void*)0},{&g_310},{(void*)0}},{{&g_310},{&g_310},{(void*)0},{&g_310},{(void*)0},{&g_310}},{{&g_310},{&g_310},{&g_310},{(void*)0},{&g_310},{(void*)0}},{{&g_310},{&g_310},{(void*)0},{&g_310},{(void*)0},{&g_310}}};
            int i, j, k;
            (*g_120) = (((*l_2136) , l_2462) == g_2464);
            for (l_1885 = 0; (l_1885 <= 5); l_1885 += 1)
            { /* block id: 1021 */
                uint32_t l_2467[7] = {9UL,0xBE4C34CDL,0xBE4C34CDL,9UL,0xBE4C34CDL,0xBE4C34CDL,9UL};
                int8_t l_2474 = (-1L);
                float l_2483 = 0x2.B6DD54p+90;
                int i;
                for (g_82 = 0; (g_82 <= 5); g_82 += 1)
                { /* block id: 1024 */
                    return p_21;
                }
                for (g_2107.f3 = 1; (g_2107.f3 >= 0); g_2107.f3 -= 1)
                { /* block id: 1029 */
                    uint16_t ***l_2496 = &l_2339;
                    uint16_t ****l_2495 = &l_2496;
                    int i;
                    if (l_1887[l_1885])
                        break;
                    for (g_1745 = 0; (g_1745 <= 1); g_1745 += 1)
                    { /* block id: 1033 */
                        uint16_t *****l_2497 = &l_2495;
                        int i, j, k;
                        l_1887[(g_1745 + 3)] = (*p_21);
                        (*g_8) = (((l_2467[2] > l_1887[l_1885]) , ((((safe_add_func_float_f_f(((void*)0 == (*g_2464)), ((safe_sub_func_float_f_f(((safe_mul_func_float_f_f(((**g_124) , 0x9.5997D8p+68), 0x1.9p-1)) > ((l_2474 = ((g_9[(g_2107.f3 + 1)][g_1745][l_1885] != l_1887[(g_1745 + 3)]) >= l_1887[l_1885])) == 0x0.2717F2p-27)), l_1887[(g_1745 + 3)])) == (*l_2466)))) >= (-0x8.2p+1)) > (*l_2466)) > (-0x4.7p+1))) != l_2374);
                        l_28 = (l_1888 < (safe_div_func_float_f_f(((l_2477 = (void*)0) != l_2478), (-((l_2482 != (void*)0) > ((l_2483 = l_2467[2]) == ((safe_mul_func_float_f_f((safe_add_func_float_f_f((*l_2466), (((safe_sub_func_float_f_f(((((*g_8) = ((-0x1.Dp+1) <= (((((*l_2497) = ((safe_mul_func_float_f_f((((safe_mul_func_float_f_f((l_1940 < 0x4.E964EAp-99), l_2084)) < l_2494) < (*g_8)), 0x0.5p-1)) , l_2495)) == &l_2496) >= (*l_2466)) <= l_2121))) < (*l_2466)) > l_2474), (*l_2466))) == l_1887[(g_1745 + 3)]) != 0x4.7p+1))), l_1888)) >= (*l_2466))))))));
                    }
                }
            }
            (*g_2499) = p_21;
            l_2503[5] = (l_2502 = &l_1885);
        }
        (*l_2502) ^= ((void*)0 == g_2504);
    }
    else
    { /* block id: 1050 */
        int32_t l_2505 = 9L;
        const uint16_t *l_2527[8][7] = {{&g_69,(void*)0,(void*)0,(void*)0,(void*)0,&l_2209[0][0],(void*)0},{&l_2209[0][2],&l_2209[0][0],&l_2209[0][0],&l_2209[0][2],(void*)0,(void*)0,&g_69},{&g_69,(void*)0,(void*)0,&l_2209[0][2],&l_2209[0][0],&l_2209[0][0],&l_2209[0][2]},{(void*)0,&l_2209[0][0],(void*)0,(void*)0,(void*)0,(void*)0,&g_69},{&l_2209[0][0],&g_69,(void*)0,(void*)0,&l_2209[0][4],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2209[0][1],&l_2209[0][0],(void*)0,&l_2209[0][0]},{&l_2209[0][1],(void*)0,&l_2209[0][0],(void*)0,(void*)0,&l_2209[0][0],(void*)0},{&l_2209[0][0],&g_69,(void*)0,(void*)0,&l_2209[0][0],(void*)0,&l_2209[0][1]}};
        const uint16_t **l_2526 = &l_2527[0][2];
        struct S1 l_2530 = {18446744073709551615UL};
        uint8_t l_2567[4];
        int32_t l_2611 = 0xE7C2D14FL;
        int32_t l_2612 = 0x9333E41BL;
        int32_t l_2613 = 0x135BD216L;
        int32_t l_2614 = (-7L);
        int32_t l_2615 = 0x952202DEL;
        int32_t l_2616 = 0x3E9B3C29L;
        int32_t l_2617 = 2L;
        int32_t l_2618[5][9] = {{0xB1A2B40DL,0xB1A2B40DL,0xD5165785L,2L,0x0BD94375L,0x0BD94375L,2L,0xD5165785L,0xB1A2B40DL},{0L,(-5L),(-1L),(-5L),0L,(-5L),(-1L),(-5L),0L},{0x0BD94375L,2L,0xD5165785L,0xB1A2B40DL,0xB1A2B40DL,0xD5165785L,2L,0x0BD94375L,0x0BD94375L},{0x15AFCDF1L,(-5L),0x15AFCDF1L,1L,0x15AFCDF1L,(-5L),0x15AFCDF1L,1L,0x15AFCDF1L},{0x0BD94375L,0xB1A2B40DL,2L,2L,0xB1A2B40DL,0x0BD94375L,0xD5165785L,0xD5165785L,0x0BD94375L}};
        uint32_t l_2620[5][8][3] = {{{1UL,1UL,0x8D6547C2L},{0x82B10541L,0x59ED2082L,0x59ED2082L},{0x39B34583L,1UL,4294967293UL},{1UL,0x59ED2082L,0UL},{4294967295UL,1UL,1UL},{0xF40EB360L,0x59ED2082L,0UL},{1UL,1UL,0x8D6547C2L},{0x82B10541L,0x59ED2082L,0x59ED2082L}},{{0x39B34583L,1UL,4294967293UL},{1UL,0x59ED2082L,0UL},{4294967295UL,1UL,1UL},{0xF40EB360L,0x59ED2082L,0UL},{1UL,1UL,0x8D6547C2L},{0x82B10541L,0x59ED2082L,0x59ED2082L},{0x39B34583L,1UL,4294967293UL},{0UL,0x5B4FA085L,4294967293UL}},{{1UL,0xAF29461EL,0xAF29461EL},{0UL,0x5B4FA085L,0x31AFE472L},{4294967293UL,0xAF29461EL,1UL},{0x59ED2082L,0x5B4FA085L,0x5B4FA085L},{0x8D6547C2L,0xAF29461EL,4294967294UL},{0UL,0x5B4FA085L,4294967293UL},{1UL,0xAF29461EL,0xAF29461EL},{0UL,0x5B4FA085L,0x31AFE472L}},{{4294967293UL,0xAF29461EL,1UL},{0x59ED2082L,0x5B4FA085L,0x5B4FA085L},{0x8D6547C2L,0xAF29461EL,4294967294UL},{0UL,0x5B4FA085L,4294967293UL},{1UL,0xAF29461EL,0xAF29461EL},{0UL,0x5B4FA085L,0x31AFE472L},{4294967293UL,0xAF29461EL,1UL},{0x59ED2082L,0x5B4FA085L,0x5B4FA085L}},{{0x8D6547C2L,0xAF29461EL,4294967294UL},{0UL,0x5B4FA085L,4294967293UL},{1UL,0xAF29461EL,0xAF29461EL},{0UL,0x5B4FA085L,0x31AFE472L},{4294967293UL,0xAF29461EL,1UL},{0x59ED2082L,0x5B4FA085L,0x5B4FA085L},{0x8D6547C2L,0xAF29461EL,4294967294UL},{0UL,0x5B4FA085L,4294967293UL}}};
        float **l_2655 = (void*)0;
        uint16_t l_2678 = 65532UL;
        int8_t l_2697 = 0xF7L;
        uint32_t l_2729 = 1UL;
        int16_t l_2782 = 5L;
        int32_t * const l_2790 = &g_1780;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_2567[i] = 0x81L;
lbl_2672:
        (*g_2050) |= l_2505;
        for (g_1201 = 0; (g_1201 <= 0); g_1201 += 1)
        { /* block id: 1054 */
            int32_t * const l_2512 = &g_105;
            uint64_t *l_2529 = &g_33;
            struct S0 **l_2543 = &l_2136;
            struct S0 ***l_2542 = &l_2543;
            int32_t l_2573 = 0x8D4EDF86L;
            int32_t l_2586 = 0L;
            int32_t l_2619 = 0x0932325CL;
            int16_t l_2677 = 0x6654L;
            uint8_t * const l_2701 = (void*)0;
            int32_t l_2739 = 1L;
            int32_t l_2740 = (-4L);
            int32_t l_2741 = 0x37C50A8DL;
            int32_t l_2742 = 0x6A4702C6L;
            int32_t l_2744 = 0x9A77FA39L;
            int32_t l_2746[10][9][2] = {{{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L},{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L}},{{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L},{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L}},{{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L},{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L}},{{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L},{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L}},{{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L},{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L}},{{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L},{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x84680EE4L},{0x9D9BB54EL,0xB282A262L},{0x56EB0A85L,0xB282A262L}},{{0x9D9BB54EL,0x84680EE4L},{0xFE8DD5C1L,0xB282A262L},{0x390FDD85L,0xB282A262L},{0xFE8DD5C1L,0x2BFC3B16L},{0x390FDD85L,0x84680EE4L},{0x35103EEAL,0x84680EE4L},{0x390FDD85L,0x2BFC3B16L},{0x56EB0A85L,0x84680EE4L},{0L,0x84680EE4L}},{{0x56EB0A85L,0x2BFC3B16L},{0x390FDD85L,0x84680EE4L},{0x35103EEAL,0x84680EE4L},{0x390FDD85L,0x2BFC3B16L},{0x56EB0A85L,0x84680EE4L},{0L,0x84680EE4L},{0x56EB0A85L,0x2BFC3B16L},{0x390FDD85L,0x84680EE4L},{0x35103EEAL,0x84680EE4L}},{{0x390FDD85L,0x2BFC3B16L},{0x56EB0A85L,0x84680EE4L},{0L,0x84680EE4L},{0x56EB0A85L,0x2BFC3B16L},{0x390FDD85L,0x84680EE4L},{0x35103EEAL,0x84680EE4L},{0x390FDD85L,0x2BFC3B16L},{0x56EB0A85L,0x84680EE4L},{0L,0x84680EE4L}},{{0x56EB0A85L,0x2BFC3B16L},{0x390FDD85L,0x84680EE4L},{0x35103EEAL,0x84680EE4L},{0x390FDD85L,0x2BFC3B16L},{0x56EB0A85L,0x84680EE4L},{0L,0x84680EE4L},{0x56EB0A85L,0x2BFC3B16L},{0x390FDD85L,0x84680EE4L},{0x35103EEAL,0x84680EE4L}}};
            int i, j, k;
            (*g_120) |= ((*l_2502) = (safe_mod_func_uint32_t_u_u(((~l_2505) && (safe_unary_minus_func_uint32_t_u((safe_add_func_uint64_t_u_u(((void*)0 != l_2512), (safe_sub_func_uint8_t_u_u((0x6AACL & l_2505), (l_2515 < 0L)))))))), ((*l_2512) &= (*l_2502)))));
            if ((((safe_add_func_uint64_t_u_u(((*l_2529) ^= ((safe_sub_func_uint8_t_u_u(l_2505, (safe_rshift_func_int8_t_s_s((g_2345.f5 , 0xE8L), ((*l_2512) <= (safe_div_func_int32_t_s_s((((((g_2524 , l_2512) == (void*)0) , ((*l_22) ^= ((((g_2525 , l_2526) == &l_2527[2][4]) < l_2528[4][0][1]) > l_2505))) , 1L) ^ (*l_2502)), (*l_2512)))))))) , (*l_2512))), (*l_2512))) , l_2505) , (*p_21)))
            { /* block id: 1060 */
                if ((*l_2502))
                    break;
                (*g_1496) = l_2530;
                (*l_2512) = (*p_21);
            }
            else
            { /* block id: 1064 */
                uint8_t *l_2533 = &g_1515;
                uint8_t **l_2534 = &g_2157;
                int32_t l_2541 = 4L;
                int32_t *l_2544[8] = {&g_1978[2],(void*)0,&g_1978[2],(void*)0,&g_1978[2],(void*)0,&g_1978[2],(void*)0};
                int i;
                if (((safe_rshift_func_uint8_t_u_u(((l_2533 == ((*l_2534) = &l_2084)) && (((*l_2502) = (safe_div_func_uint64_t_u_u((*l_2512), (safe_lshift_func_uint16_t_u_u(((l_2505 = ((*l_2502) != (l_2541 != (((-1L) | ((*l_22) = ((((void*)0 == l_2542) < (*l_2502)) >= l_2530.f0))) , l_2505)))) <= 3L), l_2541))))) , 0x80A6L)), 7)) != 0x3B8D4B64L))
                { /* block id: 1069 */
                    return l_2512;
                }
                else
                { /* block id: 1071 */
                    return p_21;
                }
            }
            if (l_2530.f0)
            { /* block id: 1075 */
                int64_t l_2548 = 0x069152576F72D6C7LL;
                int32_t l_2581 = (-4L);
                int32_t l_2587 = 0x582FD22EL;
                int32_t * const l_2591 = (void*)0;
                struct S1 l_2603 = {0UL};
                if ((*p_21))
                { /* block id: 1076 */
                    int32_t l_2547 = 0x633749DCL;
                    int32_t l_2565 = 0x208DC5FBL;
                    struct S1 l_2568 = {0x6F860A3FL};
                    if ((safe_mod_func_int32_t_s_s(l_2547, (((l_2547 , ((l_2548 > ((*l_2512) <= ((safe_lshift_func_int16_t_s_s(0L, 6)) <= ((*l_2512) == (safe_mul_func_uint16_t_u_u((safe_add_func_int32_t_s_s((safe_div_func_uint32_t_u_u(((l_2565 = (safe_rshift_func_int8_t_s_s(0L, (safe_mod_func_int8_t_s_s((safe_lshift_func_int16_t_s_s((*l_2502), 8)), ((*g_2157) = (safe_div_func_int8_t_s_s((&g_2288[0][3][1] == (void*)0), (*l_2512))))))))) && 0x77L), l_2547)), 0xF75EB6B6L)), g_2566)))))) != l_2548)) == 0x6558L) | l_2505))))
                    { /* block id: 1079 */
                        int32_t *l_2569 = (void*)0;
                        int32_t *l_2570 = &g_2341;
                        int32_t *l_2571 = &l_1885;
                        int32_t *l_2572 = (void*)0;
                        int32_t *l_2574 = &l_1885;
                        int32_t *l_2575 = &g_105;
                        int32_t *l_2576 = (void*)0;
                        int32_t *l_2577 = &l_2573;
                        int32_t *l_2578 = &g_2341;
                        int32_t *l_2579 = &l_1885;
                        int32_t *l_2580 = (void*)0;
                        int32_t *l_2582 = &l_1887[3];
                        int32_t *l_2583 = &l_1887[3];
                        int32_t *l_2584 = &l_2505;
                        int32_t *l_2585[9] = {&g_2,&g_2,&l_1887[3],&g_2,&g_2,&l_1887[3],&g_2,&g_2,&l_1887[3]};
                        int i;
                        l_2568 = func_62((&g_241 == (void*)0), l_2567[1], ((*p_21) >= 0x68C340FEL));
                        (*g_484) = l_2569;
                        l_2588++;
                        if ((*p_21))
                            continue;
                    }
                    else
                    { /* block id: 1084 */
                        return l_2591;
                    }
                    if ((**g_2416))
                    { /* block id: 1087 */
                        int32_t * const l_2592 = &l_2586;
                        return (*g_309);
                    }
                    else
                    { /* block id: 1089 */
                        uint16_t *l_2604 = &l_2209[0][0];
                        (*g_2050) &= (safe_mul_func_uint16_t_u_u(((*l_2512) = (1L == (safe_mod_func_uint16_t_u_u((safe_div_func_int16_t_s_s((*l_2502), ((*l_1761) &= g_2338.f3))), ((l_2567[1] >= ((*l_2604) = ((**l_2324) |= ((((*l_2502) & ((safe_rshift_func_uint16_t_u_u(((safe_sub_func_int8_t_s_s(0x40L, (l_2603 , 1UL))) <= l_2567[1]), 9)) <= 0x068A8860DFEEEAA5LL)) >= (*l_2502)) >= (*g_2157))))) ^ l_2547))))), 0xF42EL));
                        return p_21;
                    }
                }
                else
                { /* block id: 1097 */
                    int32_t *l_2605 = &l_2587;
                    int32_t *l_2606 = (void*)0;
                    int32_t *l_2607 = &l_1887[0];
                    int32_t *l_2608 = &l_2581;
                    int32_t *l_2609[2][7] = {{&l_1887[3],&g_1780,&l_1887[3],&l_1885,&l_1885,&l_1887[3],&g_1780},{&l_1885,&g_1780,&l_2587,&l_2587,&g_1780,&l_1885,&g_1780}};
                    int32_t **l_2623 = &l_2609[0][6];
                    int i, j;
                    l_2620[4][0][1]++;
                    (*l_2623) = p_21;
                }
            }
            else
            { /* block id: 1101 */
                int32_t *l_2624 = (void*)0;
                int32_t *l_2625 = &g_105;
                int32_t *l_2626 = &l_2613;
                int32_t *l_2627 = &l_2618[0][7];
                int32_t *l_2628[5][2] = {{&l_2619,&g_5},{&g_5,&l_2619},{&g_5,&g_5},{&l_2619,&g_5},{&g_5,&l_2619}};
                int8_t l_2629[6];
                int i, j;
                for (i = 0; i < 6; i++)
                    l_2629[i] = 0xF6L;
                l_2630--;
            }
            for (l_2530.f0 = 0; (l_2530.f0 <= 0); l_2530.f0 += 1)
            { /* block id: 1106 */
                int32_t l_2638 = 0x90A67F68L;
                int32_t l_2649 = 9L;
                float *l_2653[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                float **l_2652[8] = {&l_2653[1],&l_2653[1],&l_2653[1],&l_2653[1],&l_2653[1],&l_2653[1],&l_2653[1],&l_2653[1]};
                int32_t l_2673 = (-10L);
                int32_t l_2743 = 0L;
                int32_t l_2745[6];
                int16_t l_2748 = 8L;
                struct S1 *l_2755 = (void*)0;
                int i;
                for (i = 0; i < 6; i++)
                    l_2745[i] = 0x6BB14502L;
                for (g_548.f3 = 0; (g_548.f3 <= 0); g_548.f3 += 1)
                { /* block id: 1109 */
                    uint8_t l_2635 = 254UL;
                    l_2614 = (safe_sub_func_float_f_f((l_2635 > (safe_sub_func_float_f_f(l_2635, (l_2638 >= l_2567[1])))), (safe_div_func_float_f_f(l_2620[1][5][1], ((safe_add_func_float_f_f((-((((((((-1L) ^ ((safe_mod_func_int32_t_s_s((safe_sub_func_uint16_t_u_u((g_2648 , l_2611), 0x3B75L)), (*p_21))) , l_2635)) != l_2638) >= (*p_21)) | l_2638) , (*l_2512)) < l_2635) , l_2649)), (*l_2502))) > (*l_2502))))));
                    for (l_2638 = 0; (l_2638 >= 0); l_2638 -= 1)
                    { /* block id: 1113 */
                        (*l_2512) ^= (**g_2052);
                    }
                    if ((*l_2502))
                        continue;
                    for (g_2107.f1 = 5; (g_2107.f1 >= 1); g_2107.f1 -= 1)
                    { /* block id: 1119 */
                        float **l_2654 = &l_2653[1];
                        uint16_t ***l_2668[1][2];
                        uint16_t ****l_2667 = &l_2668[0][1];
                        float **l_2671[5];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_2668[i][j] = &l_2324;
                        }
                        for (i = 0; i < 5; i++)
                            l_2671[i] = (void*)0;
                        (*l_2512) = ((safe_add_func_uint8_t_u_u(((l_2654 = l_2652[6]) == l_2655), (safe_div_func_int8_t_s_s(g_101[(g_548.f3 + 1)][(g_548.f3 + 3)][l_2530.f0], ((safe_mul_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((g_2664 , (l_2530 , (safe_mul_func_int8_t_s_s((&l_2324 == ((*l_2667) = (void*)0)), (safe_mul_func_uint16_t_u_u(((void*)0 != l_2671[4]), g_994[(g_1201 + 2)][g_1201])))))) && g_101[(g_548.f3 + 1)][(g_548.f3 + 3)][l_2530.f0]), g_404[2])), 0x38L)) || 0L))))) , (*l_2502));
                    }
                }
                if (g_1745)
                    goto lbl_2672;
                for (g_2107.f3 = 0; (g_2107.f3 >= 0); g_2107.f3 -= 1)
                { /* block id: 1128 */
                    int32_t l_2674 = (-1L);
                    int32_t *l_2675 = &g_2341;
                    int32_t *l_2676 = &l_28;
                    int8_t *l_2693[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int8_t **l_2694 = &l_2693[4];
                    int i;
                    ++l_2678;
                    (*g_2050) = (safe_lshift_func_uint16_t_u_s((((safe_add_func_uint8_t_u_u((((safe_mod_func_uint16_t_u_u((safe_sub_func_int64_t_s_s(((**g_890) != (safe_mul_func_int16_t_s_s((((*l_2512) >= l_2530.f0) == 0x67420AC61F48F1C7LL), (((safe_rshift_func_int8_t_s_u((((((l_2673 = ((*l_2502) ^= (*l_2512))) ^ (*l_2512)) , (**l_2462)) == ((*l_2694) = (*g_2465))) & ((safe_sub_func_int8_t_s_s(l_2615, l_2649)) && g_23[2][1][0])), l_2613)) , (*l_2502)) > (*l_2675))))), 0L)), 0x2BB1L)) <= (*l_2512)) == (*l_2512)), l_2567[2])) , 255UL) != 252UL), l_2697));
                }
                if ((*p_21))
                { /* block id: 1135 */
                    struct S1 *l_2702 = &l_2530;
                    uint16_t * const l_2703 = &l_2209[1][0];
                    int8_t *l_2704 = (void*)0;
                    int8_t *l_2705 = &l_2515;
                    int32_t l_2716 = 1L;
                    for (g_1745 = 0; (g_1745 <= 0); g_1745 += 1)
                    { /* block id: 1138 */
                        int32_t **l_2698[4] = {&g_310,&g_310,&g_310,&g_310};
                        int i;
                        if ((*l_2512))
                            break;
                        l_2699 = (void*)0;
                        return p_21;
                    }
                    if ((*l_2699))
                        continue;
                    if (((0xD5L < ((((((*l_2705) ^= (((*l_2502) &= (((l_2700[0] = &l_2084) == l_2701) & ((*g_891) = ((l_2702 = l_2702) == (void*)0)))) & (((*l_2512) , l_2703) != (void*)0))) ^ (l_2618[3][2] ^ 1UL)) >= 249UL) >= l_2649) , (*l_2699))) ^ 0xE87DD72613F4DD70LL))
                    { /* block id: 1149 */
                        uint32_t l_2727 = 0xAFCC2260L;
                        uint32_t *l_2728 = &g_1682;
                        const int32_t *l_2731[3][9] = {{&l_2505,&l_2611,&l_2505,&l_2611,&l_2505,&l_2611,&l_2505,&l_2611,&l_2505},{&g_5,(void*)0,(void*)0,&g_5,&g_5,(void*)0,(void*)0,&g_5,&g_5},{(void*)0,&l_2611,(void*)0,&l_2611,(void*)0,&l_2611,(void*)0,&l_2611,(void*)0}};
                        const int32_t **l_2730 = &l_2731[0][4];
                        int i, j;
                        (*l_2699) |= (-10L);
                        (*l_2699) &= (safe_sub_func_int16_t_s_s((safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((*g_2157) = (safe_lshift_func_uint8_t_u_u(((*l_2512) <= ((-7L) | ((((safe_mod_func_uint8_t_u_u(((*g_32) < (((**l_2324) = l_2673) > l_2716)), (safe_rshift_func_int16_t_s_s(((*l_1761) &= (safe_add_func_int8_t_s_s(0x0CL, (safe_sub_func_uint16_t_u_u((((!(0UL != 0x897E0523B9C19EE9LL)) && ((*l_2728) = (((((safe_div_func_int8_t_s_s(((l_2567[1] , l_2726) , (-4L)), l_2649)) == 248UL) <= l_2673) ^ 0xEE5BL) || l_2727))) && l_2727), l_2727))))), 5)))) , (void*)0) != (void*)0) || l_2638))), (*l_2512)))), l_2729)), 65535UL)), 0x489EL));
                        (*l_2730) = &l_2528[2][1][0];
                    }
                    else
                    { /* block id: 1157 */
                        return p_21;
                    }
                }
                else
                { /* block id: 1160 */
                    int64_t l_2732[2];
                    int32_t l_2738 = 0x87FE0633L;
                    int32_t l_2747 = 0xD94B8FAAL;
                    int32_t l_2749 = (-1L);
                    int32_t l_2750 = 5L;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2732[i] = 0L;
                    for (g_2334 = 0; g_2334 < 9; g_2334 += 1)
                    {
                        g_143[g_2334] = (-10L);
                    }
                    if ((*p_21))
                        break;
                    for (g_105 = 0; (g_105 <= 0); g_105 += 1)
                    { /* block id: 1165 */
                        int32_t *l_2733 = &l_2617;
                        int32_t *l_2734 = &g_1780;
                        int32_t *l_2735 = (void*)0;
                        int32_t *l_2736 = &g_2341;
                        int32_t *l_2737[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        uint32_t l_2751 = 0x7B75C1DEL;
                        int i;
                        (**l_2542) = l_2136;
                        ++l_2751;
                        return p_21;
                    }
                }
                for (g_2648.f1 = 0; (g_2648.f1 >= 0); g_2648.f1 -= 1)
                { /* block id: 1173 */
                    struct S1 *l_2754 = &g_482;
                    for (l_2742 = 0; (l_2742 <= 0); l_2742 += 1)
                    { /* block id: 1176 */
                        l_2755 = l_2754;
                        return p_21;
                    }
                }
            }
        }
        if ((safe_mul_func_int8_t_s_s((((**g_890) = (*l_2502)) && l_2618[0][8]), l_2617)))
        { /* block id: 1184 */
            float l_2773 = (-0x7.7p+1);
            int32_t l_2774 = 0x757C8CA8L;
            int32_t l_2776 = 0x7F8219E3L;
            int32_t l_2777 = 0L;
            int32_t l_2778 = 0x24168C62L;
            int32_t l_2779[4][9][5] = {{{0x878450B1L,1L,0xEFA6215DL,0x86EDACD1L,0x2C294FD5L},{0x6CF5EA09L,0x98029F78L,0x3BF5E98CL,(-10L),0x84CAC2FAL},{0x878450B1L,(-1L),5L,0xFADDA357L,0x78D550EDL},{0L,(-6L),1L,1L,0L},{0L,1L,0L,0x37698585L,0x98029F78L},{0x7D7669E0L,1L,5L,0xECBDD8D1L,0x0B1315CFL},{0x0C7030A2L,2L,0xE28940D7L,0L,0L},{0x06C6064EL,(-10L),0x2C294FD5L,0xEFA6215DL,0xB367D4DFL},{0xEFA6215DL,(-10L),0x46B6D35EL,0x86EDACD1L,0x78D550EDL}},{{5L,2L,0L,1L,0x0C7030A2L},{(-1L),1L,1L,0L,0x44EA26A7L},{0x628E0BA9L,0x78D550EDL,0x6F014D27L,(-9L),(-9L)},{1L,(-5L),1L,0xC4BA55A3L,(-1L)},{0L,0L,0x0B1315CFL,0x44EA26A7L,0x4EC94972L},{4L,0x0C7030A2L,(-10L),1L,0x6CF5EA09L},{0x78D550EDL,6L,0x0B1315CFL,0x4EC94972L,0x84CAC2FAL},{(-9L),(-1L),1L,(-1L),0x86EDACD1L},{(-1L),0xC4BA55A3L,0x6F014D27L,0L,(-3L)}},{{(-1L),3L,1L,(-6L),(-10L)},{1L,0x9779E521L,0L,(-5L),0x628E0BA9L},{3L,0x878450B1L,0x46B6D35EL,0x837B311AL,6L},{0xCA55B390L,0x4EC94972L,0x2C294FD5L,0x837B311AL,0x878450B1L},{6L,0x6F014D27L,0xE28940D7L,(-5L),1L},{0x9906FD5AL,0x3BF5E98CL,5L,(-6L),2L},{(-1L),1L,0x7D7669E0L,0L,0xF70E4C3CL},{0xECBDD8D1L,(-1L),0x8FB082CBL,(-1L),0xECBDD8D1L},{0x3BF5E98CL,0x44EA26A7L,(-1L),0x4EC94972L,0x8FB082CBL}},{{0xC4BA55A3L,0L,6L,1L,0x06C6064EL},{0x6F014D27L,0x837B311AL,0xF70E4C3CL,0x44EA26A7L,0x8FB082CBL},{0xE28940D7L,1L,(-6L),0xC4BA55A3L,0xECBDD8D1L},{0x8FB082CBL,0L,0x219BF040L,(-9L),0xF70E4C3CL},{0x98029F78L,1L,0x6CF5EA09L,0L,2L},{0x86EDACD1L,0xE28940D7L,6L,1L,1L},{0x46B6D35EL,0x3A172DB6L,0x37698585L,0x86EDACD1L,0x878450B1L},{(-1L),0x9906FD5AL,0x9779E521L,0xEFA6215DL,6L},{(-1L),(-3L),0x3BF5E98CL,0L,0x628E0BA9L}}};
            int32_t l_2780 = (-6L);
            int32_t l_2783 = 0x71298BBDL;
            int32_t **l_2789[8][1] = {{&l_2502},{&l_2699},{&l_2502},{&l_2699},{&l_2502},{&l_2699},{&l_2502},{&l_2699}};
            int i, j, k;
            for (g_2664.f1 = 4; (g_2664.f1 >= 1); g_2664.f1 -= 1)
            { /* block id: 1187 */
                int16_t l_2759 = 0L;
                int32_t l_2760 = (-8L);
                int32_t *l_2761 = &l_2617;
                int32_t *l_2762 = &l_2617;
                int32_t l_2763 = (-10L);
                int32_t *l_2764 = &l_2614;
                int32_t *l_2765 = &l_1887[5];
                int32_t *l_2766 = &l_2614;
                int32_t *l_2767 = &l_1887[3];
                int32_t *l_2768 = &l_2614;
                int32_t *l_2769 = &g_105;
                int32_t *l_2770 = &l_1887[3];
                int32_t *l_2771 = &l_2612;
                int32_t *l_2772[6];
                int i;
                for (i = 0; i < 6; i++)
                    l_2772[i] = &l_2763;
                for (g_548.f3 = 4; (g_548.f3 >= 0); g_548.f3 -= 1)
                { /* block id: 1190 */
                    int32_t * const l_2758 = (void*)0;
                    return l_2758;
                }
                l_2784++;
                for (g_119.f0 = 0; (g_119.f0 <= 4); g_119.f0 += 1)
                { /* block id: 1196 */
                    for (g_103 = 0; (g_103 <= 0); g_103 += 1)
                    { /* block id: 1199 */
                        int32_t **l_2787 = &l_2772[3];
                        int i, j;
                        (*l_2787) = &l_2618[g_2664.f1][(g_2664.f1 + 3)];
                        return p_21;
                    }
                }
                return p_21;
            }
            p_21 = p_21;
        }
        else
        { /* block id: 1207 */
            return l_2790;
        }
    }
    return p_21;
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_2 g_69 g_61.f3 g_5 g_61.f1 g_61.f5 g_61.f4 g_101 g_33 g_86 g_103 g_61.f2 g_105 g_61.f6 g_110 g_120 g_124 g_133 g_32 g_61.f0 g_119.f0 g_82 g_158 g_176 g_8 g_9 g_177 g_119 g_121 g_116 g_143 g_209 g_240 g_133.f3 g_61.f7 g_296 g_297 g_220 g_241 g_309 g_310 g_360 g_374 g_371 g_61 g_158.f3 g_450 g_454 g_404 g_465 g_476 g_298 g_479 g_481 g_484 g_560 g_158.f0 g_574 g_548.f0 g_594 g_642 g_595 g_653 g_668 g_677 g_342 g_737 g_596 g_133.f0 g_574.f0 g_890 g_891 g_968 g_970 g_994 g_548.f1 g_508 g_968.f3 g_999 g_355 g_1214 g_482.f0 g_1201 g_1262 g_1339 g_1372 g_1409 g_1426 g_1461 g_1472 g_548.f3 g_1501 g_1515 g_1461.f3 g_574.f3 g_1534 g_1560
 * writes: g_69 g_82 g_86 g_61.f5 g_101 g_103 g_105 g_116 g_119 g_120 g_121 g_61.f4 g_177 g_9 g_33 g_143 g_298 g_310 g_240 g_374 g_450 g_371 g_482 g_476 g_355 g_508 g_596 g_677 g_688 g_60 g_404 g_953 g_999 g_548.f1 g_1201 g_209 g_1409 g_548.f3 g_1495 g_1515 g_1461.f3 g_891 g_1561
 */
static uint8_t  func_29(uint32_t  p_30, const uint8_t  p_31)
{ /* block id: 20 */
    uint16_t l_39 = 0xD7D5L;
    float *l_47 = (void*)0;
    uint8_t l_972 = 0x4DL;
    const uint64_t *l_975 = (void*)0;
    struct S0 *l_1732 = &g_1733;
    l_1732 = func_34(l_39, func_40((l_972 = func_44(l_47, (func_50((safe_mod_func_int32_t_s_s(func_54(g_60, (l_39 > l_39), func_62(((safe_mul_func_int16_t_s_s(p_30, 0x6F9CL)) , 18446744073709551607UL), g_2, p_30), l_39, g_110), g_61.f0))) && 0L))), l_47, g_61.f3), l_975, l_47);
    return p_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_891 g_86 g_890 g_994 g_110.f0 g_32 g_33 g_595 g_105 g_297 g_298 g_508 g_404 g_69 g_61.f1 g_120 g_968.f3 g_450 g_209 g_61.f3 g_668 g_116 g_737 g_60 g_61.f7 g_999 g_355 g_596.f0 g_8 g_9 g_596 g_61.f5 g_1214 g_482.f0 g_476 g_119.f0 g_1201 g_124 g_121 g_1262 g_158.f3 g_143 g_5 g_101 g_1339 g_1372 g_1409 g_103 g_1426 g_158 g_133 g_1461 g_82 g_371 g_1472 g_310 g_548.f3 g_220 g_1501 g_1515 g_1461.f3 g_574.f3 g_1534 g_2 g_1560 g_548.f1
 * writes: g_86 g_82 g_476 g_69 g_999 g_105 g_596 g_548.f1 g_9 g_450 g_143 g_482 g_1201 g_60 g_371 g_121 g_310 g_119.f0 g_209 g_116 g_1409 g_61.f4 g_548.f3 g_1495 g_1515 g_1461.f3 g_404 g_891 g_1561
 */
static struct S0 * func_34(uint32_t  p_35, struct S1  p_36, const uint64_t * p_37, const int32_t * p_38)
{ /* block id: 376 */
    uint8_t *l_976 = &g_82;
    uint32_t *l_981[9][1][2] = {{{&g_101[0][4][0],&g_101[1][6][0]}},{{&g_101[0][4][0],(void*)0}},{{&g_101[1][6][0],(void*)0}},{{&g_101[0][4][0],&g_101[1][6][0]}},{{&g_101[0][4][0],&g_101[0][4][0]}},{{&g_101[0][4][0],&g_101[1][6][0]}},{{&g_101[0][4][0],(void*)0}},{{&g_101[1][6][0],(void*)0}},{{&g_101[0][4][0],&g_101[1][6][0]}}};
    int16_t *l_989 = &g_476;
    int32_t l_992 = (-5L);
    uint16_t *l_993 = &g_69;
    int32_t l_1000[4] = {0x1E9750B8L,0x1E9750B8L,0x1E9750B8L,0x1E9750B8L};
    struct S1 l_1041 = {0xE094E774L};
    float l_1048 = 0xF.C551D2p+54;
    struct S1 *l_1068 = &g_482;
    struct S1 **l_1067 = &l_1068;
    const float l_1093 = 0x5.AEE899p+60;
    uint16_t l_1109 = 0UL;
    uint64_t l_1138 = 1UL;
    float l_1167[6][10][3] = {{{0x1.Dp+1,0x1.Ep+1,(-0x5.Ap+1)},{0x8.Ep-1,0xF.D7C183p-81,0xB.256249p+18},{0x6.93E703p-10,(-0x1.4p-1),0x5.9F5303p-3},{0x7.15ACDDp+87,0x2.Ep-1,0xB.256249p+18},{0x6.356B24p+77,0x1.4p+1,0xC.758F3Fp+30},{0x0.18DF49p+42,0x1.Fp+1,0x2.5p+1},{0xA.5720EDp-79,0x2.132C58p-39,0xE.D314D5p-5},{0x6.C6A537p+46,0x5.2AD750p-1,(-0x10.8p-1)},{0xE.D314D5p-5,0xE.AF47C2p-99,0xE.452523p+8},{0xF.D7C183p-81,0x5.2AD750p-1,0x8.Ep-1}},{{0x1.5p+1,0x2.132C58p-39,0x1.5p+1},{0x5.2AD750p-1,0x1.Fp+1,0x2.Ep-1},{0x8.F8B355p+44,0x1.4p+1,(-0x1.Fp-1)},{0x8.Ep-1,0x2.Ep-1,0x1.Ep+1},{0x7.6C6F61p-63,(-0x1.4p-1),0x1.Dp+1},{0x8.Ep-1,0x1.61463Fp+69,0x7.15ACDDp+87},{0x8.F8B355p+44,0x4.D27C85p+95,(-0x1.Fp-1)},{0x5.2AD750p-1,0xB.256249p+18,0x7.7CAB94p+96},{0x1.5p+1,0x1.Ep+1,0x8.F8B355p+44},{0xF.D7C183p-81,0x1.1p+1,(-0x5.Fp-1)}},{{0xE.D314D5p-5,0xD.890A17p-67,0x8.F8B355p+44},{0x6.C6A537p+46,0x7.7CAB94p+96,0x7.7CAB94p+96},{0xA.5720EDp-79,0x2.0p+1,(-0x1.Fp-1)},{0x0.18DF49p+42,0x2.5p+1,0x7.15ACDDp+87},{0x6.356B24p+77,0x1.173224p+57,0x1.Dp+1},{0x7.15ACDDp+87,0xF.D7C183p-81,0x1.Ep+1},{0x6.93E703p-10,0x1.173224p+57,(-0x1.Fp-1)},{0x2.Ep-1,0x2.5p+1,0x2.Ep-1},{(-0x5.Ap+1),0x2.0p+1,0x1.5p+1},{0xB.256249p+18,0x7.7CAB94p+96,0x8.Ep-1}},{{(-0x1.Fp-1),0xD.890A17p-67,0xE.452523p+8},{0x1.Fp+1,0x1.1p+1,(-0x10.8p-1)},{(-0x1.Fp-1),0x1.Ep+1,0xE.D314D5p-5},{0xB.256249p+18,0xB.256249p+18,0x2.5p+1},{(-0x5.Ap+1),0x4.D27C85p+95,0xC.758F3Fp+30},{0x2.Ep-1,0x1.61463Fp+69,0xB.256249p+18},{0x6.93E703p-10,(-0x1.4p-1),0x5.9F5303p-3},{0x7.15ACDDp+87,0x2.Ep-1,0xB.256249p+18},{0x6.356B24p+77,0x1.4p+1,0xC.758F3Fp+30},{0x0.18DF49p+42,0x1.Fp+1,0x2.5p+1}},{{0xA.5720EDp-79,0x2.132C58p-39,0xE.D314D5p-5},{0x6.C6A537p+46,0x5.2AD750p-1,(-0x10.8p-1)},{0xE.D314D5p-5,0xE.AF47C2p-99,0xE.452523p+8},{0xF.D7C183p-81,0x5.2AD750p-1,0x8.Ep-1},{0x1.5p+1,0x2.132C58p-39,0x1.5p+1},{0x5.2AD750p-1,0x1.Fp+1,0x2.Ep-1},{0x8.F8B355p+44,0x1.4p+1,(-0x1.Fp-1)},{0x8.Ep-1,0x2.Ep-1,0x1.Ep+1},{0x7.6C6F61p-63,(-0x1.4p-1),0x1.Dp+1},{0x8.Ep-1,0x1.61463Fp+69,0x7.15ACDDp+87}},{{0x8.F8B355p+44,0x4.D27C85p+95,(-0x1.Fp-1)},{0x5.2AD750p-1,0xB.256249p+18,0x7.7CAB94p+96},{0x1.5p+1,0x1.Ep+1,0x8.F8B355p+44},{0xF.D7C183p-81,0x1.1p+1,(-0x5.Fp-1)},{0xE.D314D5p-5,0xD.890A17p-67,0x8.F8B355p+44},{0x6.C6A537p+46,0x7.7CAB94p+96,0x7.7CAB94p+96},{0xA.5720EDp-79,0x2.0p+1,(-0x1.Fp-1)},{0x0.18DF49p+42,0x2.5p+1,0x7.15ACDDp+87},{0x6.356B24p+77,0x1.173224p+57,0x1.Dp+1},{0x7.15ACDDp+87,0xF.D7C183p-81,0x1.Ep+1}}};
    uint32_t l_1210 = 18446744073709551615UL;
    int16_t l_1242 = 0x0016L;
    struct S0 *l_1257 = (void*)0;
    struct S0 *l_1352 = &g_61;
    int16_t l_1354 = (-1L);
    float l_1425 = 0xE.6BB2A0p-41;
    uint32_t l_1427[5];
    uint8_t l_1469 = 0x7FL;
    int64_t *l_1535 = &g_86;
    int8_t l_1552[9][1][5] = {{{(-1L),0L,0L,0x67L,0x02L}},{{(-1L),(-1L),1L,0x67L,0x67L}},{{0L,(-1L),0L,0x02L,0x67L}},{{0x5BL,0L,0x63L,0L,1L}},{{0x5BL,0L,0x14L,0L,0L}},{{0x63L,0L,0x63L,1L,0L}},{{0x5BL,0L,0x63L,0L,1L}},{{0x5BL,0L,0x14L,0L,0L}},{{0x63L,0L,0x63L,1L,0L}}};
    int16_t l_1590 = 0x8017L;
    int64_t * const ***l_1599 = &g_953;
    int64_t * const ****l_1598 = &l_1599;
    float ** const **l_1712 = (void*)0;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1427[i] = 18446744073709551614UL;
lbl_1385:
    if ((((**g_890) = (*g_891)) || (((*l_976) = 0x22L) != (((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u(0xA6F2BF7653661856LL, ((p_35 = 1UL) || ((p_35 <= (safe_unary_minus_func_uint64_t_u(((-7L) > ((safe_mod_func_uint32_t_u_u((safe_mod_func_int16_t_s_s((safe_add_func_uint64_t_u_u((((*l_989) = p_36.f0) < ((*l_993) = ((l_992 | 0L) , l_992))), 0x683AAE073C0CA332LL)), 0x6894L)), g_994[5][5])) & l_992))))) != 1L)))), p_36.f0)) , l_992) && 1UL))))
    { /* block id: 382 */
        int64_t * const ***l_996 = &g_953;
        int64_t * const ****l_995 = &l_996;
        int32_t *l_997 = &g_105;
        int32_t *l_998[8][8][4] = {{{&g_5,&l_992,&g_5,&g_105},{&l_992,(void*)0,&g_105,&l_992},{(void*)0,&l_992,&g_2,(void*)0},{&g_5,&l_992,&g_2,&g_2},{(void*)0,&g_2,&g_105,&g_5},{&l_992,(void*)0,&g_5,&g_105},{&g_5,&g_105,(void*)0,&g_2},{&g_105,&g_2,&g_105,(void*)0}},{{&g_2,&g_2,&l_992,&g_105},{&g_5,&l_992,&l_992,(void*)0},{(void*)0,(void*)0,&g_5,(void*)0},{&g_105,&g_2,&g_105,&g_105},{&l_992,&g_5,&g_105,&l_992},{&g_2,&g_105,&g_2,&l_992},{&g_2,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_2,&g_2}},{{&g_2,&l_992,&g_5,&g_5},{&g_105,&g_105,&g_5,&g_5},{(void*)0,&g_105,&g_2,&g_5},{&g_105,&l_992,&l_992,&g_2},{&l_992,(void*)0,&l_992,(void*)0},{&g_2,(void*)0,&g_5,&l_992},{&g_2,&g_105,&g_2,&l_992},{&g_5,&g_5,&g_2,&g_105}},{{&l_992,&g_2,&l_992,(void*)0},{&g_2,(void*)0,(void*)0,(void*)0},{(void*)0,&l_992,&l_992,&g_105},{&l_992,&g_2,(void*)0,(void*)0},{&g_5,&g_2,&l_992,&g_2},{&g_2,&g_105,(void*)0,&g_105},{&g_2,(void*)0,&g_2,&g_5},{(void*)0,&g_2,(void*)0,&g_2}},{{(void*)0,&l_992,&g_105,(void*)0},{(void*)0,&l_992,(void*)0,&l_992},{(void*)0,(void*)0,&g_2,&g_105},{&g_2,&l_992,(void*)0,(void*)0},{&g_2,&g_5,&l_992,(void*)0},{&g_5,&g_5,(void*)0,&g_2},{&l_992,&g_2,&l_992,&g_5},{(void*)0,&g_2,(void*)0,&g_105}},{{&g_2,&g_2,&g_105,&g_5},{(void*)0,&g_2,&g_105,&l_992},{(void*)0,(void*)0,&g_5,&l_992},{(void*)0,&g_2,&g_2,(void*)0},{&g_5,&g_105,&g_105,&g_5},{&l_992,&l_992,&g_2,&g_2},{&g_5,&g_5,(void*)0,&l_992},{&g_105,&g_5,&g_2,&l_992}},{{(void*)0,&g_5,&l_992,&g_2},{&l_992,&l_992,&g_2,&g_5},{(void*)0,&g_105,&g_105,(void*)0},{&l_992,&g_2,&g_2,&l_992},{&g_2,(void*)0,&g_105,&l_992},{&l_992,&g_2,&g_5,&g_5},{&g_5,&g_2,&g_105,&g_5},{&g_105,&g_105,&g_105,&g_2}},{{&g_2,(void*)0,&l_992,&g_2},{&g_2,(void*)0,&l_992,&l_992},{&g_5,&l_992,&l_992,&g_5},{&l_992,&g_105,&l_992,(void*)0},{&g_105,&g_2,&g_5,(void*)0},{&l_992,&g_2,&l_992,&g_2},{&g_105,&l_992,&l_992,(void*)0},{&l_992,&g_2,&g_5,&g_105}}};
        uint16_t l_1001 = 65527UL;
        const int32_t l_1040 = (-1L);
        float *l_1045 = &g_9[2][1][1];
        struct S0 *l_1073 = &g_61;
        int i, j, k;
        (*l_995) = &g_953;
        --l_1001;
        if ((safe_mod_func_int16_t_s_s((g_999[3][2] = g_994[0][0]), ((*l_989) = (safe_add_func_uint64_t_u_u(((void*)0 == &g_143[3]), ((void*)0 != p_38)))))))
        { /* block id: 387 */
            float l_1008 = 0x4.0C5DFBp-65;
            const uint32_t l_1009 = 0xF2B2F827L;
            uint16_t **l_1014[2];
            int64_t *l_1032 = (void*)0;
            int32_t l_1033 = 0x55B2BE78L;
            float *l_1036[9][8] = {{&g_9[2][1][5],&g_355[1],(void*)0,&g_355[2],&l_1008,&l_1008,&g_9[2][1][1],&l_1008},{&g_355[1],&l_1008,&g_355[0],&l_1008,&g_355[1],(void*)0,&g_355[4],&g_355[1]},{&g_355[4],&l_1008,&g_9[2][1][1],(void*)0,&g_355[1],&g_9[2][1][1],(void*)0,&l_1008},{&g_355[0],(void*)0,&g_9[2][1][1],&g_355[1],(void*)0,&g_355[4],&g_355[4],(void*)0},{&g_355[1],&g_355[0],&g_355[0],&g_355[1],&g_355[2],(void*)0,&g_9[2][1][1],(void*)0},{&g_355[1],&g_9[2][1][1],(void*)0,&g_355[0],(void*)0,(void*)0,&g_9[1][0][5],(void*)0},{(void*)0,&g_9[2][1][1],&l_1008,&g_355[4],(void*)0,(void*)0,(void*)0,&g_355[4]},{&l_1008,&g_355[0],&l_1008,&g_355[1],(void*)0,&g_355[4],&g_355[1],(void*)0},{&g_355[2],(void*)0,&g_355[1],&g_9[2][1][5],&g_355[0],&g_9[2][1][1],(void*)0,&g_9[2][1][1]}};
            int32_t l_1037 = 0x58DAA0B5L;
            int64_t *****l_1038 = (void*)0;
            int32_t l_1039[10][9] = {{0xF38E7244L,1L,(-9L),(-9L),(-3L),5L,(-9L),(-8L),0xF14A3F32L},{0xF27C4F7DL,(-8L),0xF38E7244L,3L,9L,0xD1A67663L,0xD1A67663L,9L,3L},{0xF27C4F7DL,(-3L),0xF27C4F7DL,0xD1A67663L,(-3L),(-4L),0x2CA63206L,0xF27C4F7DL,(-4L)},{(-4L),0x7F5E6E86L,0xDE30DF7DL,0xC03BCEDCL,0xF7270CA9L,0xCC916856L,0L,(-9L),0x92C5D770L},{0xC03BCEDCL,(-2L),(-4L),0x92C5D770L,0xF7270CA9L,0xBF42F071L,0xC03BCEDCL,5L,0x8B726351L},{0x2CA63206L,0xF38E7244L,0xC03BCEDCL,0xCC916856L,5L,0xCC916856L,0xC03BCEDCL,0xF38E7244L,0x2CA63206L},{0xBF42F071L,0xF14A3F32L,0x2CA63206L,(-1L),0xD1A67663L,(-4L),0L,5L,0x2CA63206L},{0L,0xD1A67663L,0xBF42F071L,0xDE30DF7DL,0xF38E7244L,0x92C5D770L,0x2CA63206L,(-9L),0x8B726351L},{0xBF42F071L,0xD1A67663L,0L,0xBF42F071L,3L,0x8B726351L,0x92C5D770L,0xF27C4F7DL,0x92C5D770L},{0x2CA63206L,0xF14A3F32L,0xBF42F071L,0xBF42F071L,0xF14A3F32L,0x2CA63206L,(-1L),0xD1A67663L,(-4L)}};
            struct S1 *l_1042 = (void*)0;
            struct S1 *l_1043 = (void*)0;
            struct S1 *l_1044 = &l_1041;
            int i, j;
            for (i = 0; i < 2; i++)
                l_1014[i] = (void*)0;
            (*l_997) = (g_110.f0 , l_1009);
            (*g_595) = func_40((safe_add_func_int16_t_s_s((safe_add_func_int8_t_s_s((((void*)0 != l_1014[0]) | (safe_rshift_func_int8_t_s_u((((*l_1044) = (((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_s(((l_1000[1] && 0x9B45L) && (-1L)), 2)), 0)) , ((safe_rshift_func_int8_t_s_s((+((l_1039[7][7] = ((((*g_32) != (((safe_add_func_float_f_f((l_1037 = (safe_mul_func_float_f_f((safe_mul_func_float_f_f((l_1033 = (l_1032 != p_37)), (safe_sub_func_float_f_f(l_1009, p_35)))), 0x5.A3BF40p+10))), (-0x1.4p+1))) , (void*)0) != l_1038)) && l_992) , l_1009)) , p_35)), l_1040)) & 0L)) , l_1041)) , p_36.f0), l_1009))), 0x0BL)), p_36.f0)), l_1045, p_35);
        }
        else
        { /* block id: 394 */
            int8_t l_1052 = 0xC4L;
            struct S1 *l_1066[1];
            struct S1 **l_1065 = &l_1066[0];
            int i;
            for (i = 0; i < 1; i++)
                l_1066[i] = &g_119;
            for (g_548.f1 = (-15); (g_548.f1 > 6); g_548.f1 = safe_add_func_int64_t_s_s(g_548.f1, 8))
            { /* block id: 397 */
                int64_t l_1049 = 0L;
                (*l_997) |= l_1049;
                (*l_1045) = (((safe_mul_func_float_f_f(((l_1052 , (safe_lshift_func_uint8_t_u_u(((*l_976) = (((p_35 = ((*g_297) == (void*)0)) || (safe_mod_func_int32_t_s_s(((*g_32) > (safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((-3L), 6)), (g_508 >= 0x6C428EA8L)))), ((safe_mod_func_int64_t_s_s((safe_mod_func_uint64_t_u_u((((l_1067 = l_1065) != &l_1068) | l_1049), p_36.f0)), 18446744073709551615UL)) , 0xEDA4B553L)))) || p_36.f0)), p_36.f0))) , (-0x4.Cp+1)), 0x2.87A87Dp+62)) <= 0xF.3E96A9p-18) <= (-0x3.9p-1));
                for (g_450 = 0; (g_450 != 23); g_450++)
                { /* block id: 405 */
                    uint32_t l_1092 = 0x07C90378L;
                    uint16_t l_1095 = 0x9A31L;
                    int32_t l_1096 = 0x022111F6L;
                    for (g_69 = (-13); (g_69 > 19); g_69++)
                    { /* block id: 408 */
                        return l_1073;
                    }
                    for (p_36.f0 = 0; (p_36.f0 <= 58); ++p_36.f0)
                    { /* block id: 413 */
                        float *l_1078[7] = {&g_355[0],&g_355[0],&g_355[0],&g_355[0],&g_355[0],&g_355[0],&g_355[0]};
                        int8_t *l_1081[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1081[i] = &g_143[7];
                        l_1096 = (safe_div_func_int16_t_s_s((((*l_993) &= g_404[2]) | ((func_40((*l_997), l_1078[2], (safe_add_func_int8_t_s_s((g_143[7] = 0L), (safe_sub_func_uint16_t_u_u((l_1000[2] = (safe_add_func_uint16_t_u_u((g_61.f1 ^ ((safe_rshift_func_int8_t_s_u((((safe_mod_func_int8_t_s_s(0x52L, p_35)) >= (((((safe_rshift_func_int16_t_s_s((((l_1092 ^= ((void*)0 != g_120)) <= g_968[0].f3) , (-1L)), g_450)) , 1UL) | p_35) < (-1L)) != p_35)) ^ l_1095), g_209)) , l_1041.f0)), l_1000[2]))), g_61.f3))))) , 9L) & 0x4CE14855L)), 0xF29DL));
                    }
                }
                (*l_1045) = ((safe_lshift_func_uint8_t_u_s((((l_1052 >= (g_668 || (((((~(-5L)) <= ((p_35 = (0xDB8A7F5E09CF2286LL ^ p_36.f0)) > p_36.f0)) | (safe_rshift_func_uint16_t_u_u((((*g_891) , (((l_1000[2] != ((((g_116 >= 0x5EL) <= 1L) != 0L) & p_36.f0)) , l_1049) , 0x6FL)) & 0L), l_1052))) < p_36.f0) < l_1000[1]))) > (*g_891)) && (*g_32)), 5)) , p_35);
            }
            return (*g_737);
        }
    }
    else
    { /* block id: 426 */
        uint16_t *l_1104 = &g_69;
        int32_t l_1105[3];
        int8_t *l_1108 = &g_143[7];
        struct S1 ****l_1114[5];
        float l_1168[3];
        uint8_t l_1200 = 0x31L;
        struct S0 *l_1215 = &g_61;
        uint32_t l_1231 = 1UL;
        const uint64_t l_1243 = 18446744073709551615UL;
        int8_t l_1281 = 0L;
        int i;
        for (i = 0; i < 3; i++)
            l_1105[i] = 0L;
        for (i = 0; i < 5; i++)
            l_1114[i] = (void*)0;
        for (i = 0; i < 3; i++)
            l_1168[i] = 0x0.2p+1;
        if (((((void*)0 == l_1104) == (0xC2L && ((l_1105[0] = 0x6746L) , ((safe_sub_func_int8_t_s_s(((*l_1108) = p_36.f0), l_1109)) <= l_1000[1])))) && (p_35 >= l_992)))
        { /* block id: 429 */
            uint64_t l_1113[2][8][7] = {{{4UL,0x8649D6312F4DF832LL,0x56C26C1F79FB91D5LL,0x8649D6312F4DF832LL,4UL,0xAFB0B99DF8D37E51LL,0UL},{0UL,0xD43A08639E9ED4ACLL,0x9D29336043B11B1ALL,18446744073709551606UL,0UL,0x800FC18F92A8693ALL,18446744073709551610UL},{0x9D29336043B11B1ALL,0xAFB0B99DF8D37E51LL,0xFA5D96B5594E3AEDLL,18446744073709551609UL,0UL,4UL,0xEFA0AA30F76A46E3LL},{0UL,18446744073709551606UL,0x157D7C93D52C492BLL,0xEFA0AA30F76A46E3LL,0x157D7C93D52C492BLL,18446744073709551606UL,0UL},{4UL,18446744073709551606UL,0x5B9E9D8655B36BC3LL,0UL,0x8649D6312F4DF832LL,0UL,0x56C26C1F79FB91D5LL},{0x56C26C1F79FB91D5LL,0xAFB0B99DF8D37E51LL,8UL,0x5B9E9D8655B36BC3LL,0xEFA0AA30F76A46E3LL,18446744073709551610UL,0xD43A08639E9ED4ACLL},{0xFA5D96B5594E3AEDLL,0xD43A08639E9ED4ACLL,0x5B9E9D8655B36BC3LL,0xDCDCBFBA83CAE4C6LL,18446744073709551606UL,0xDCDCBFBA83CAE4C6LL,0x5B9E9D8655B36BC3LL},{0x8649D6312F4DF832LL,0x8649D6312F4DF832LL,0x157D7C93D52C492BLL,0xDCDCBFBA83CAE4C6LL,0x48FA6F4E97728F45LL,0x9D29336043B11B1ALL,0UL}},{{18446744073709551606UL,0x48FA6F4E97728F45LL,0xFA5D96B5594E3AEDLL,0x5B9E9D8655B36BC3LL,0x56C26C1F79FB91D5LL,0xD43A08639E9ED4ACLL,0xAFB0B99DF8D37E51LL},{18446744073709551609UL,0x800FC18F92A8693ALL,0x9D29336043B11B1ALL,0UL,0x48FA6F4E97728F45LL,0x43F7600D7BFD9D6FLL,0x48FA6F4E97728F45LL},{0xEFA0AA30F76A46E3LL,0x56C26C1F79FB91D5LL,0x56C26C1F79FB91D5LL,0xEFA0AA30F76A46E3LL,18446744073709551606UL,0x43F7600D7BFD9D6FLL,8UL},{0UL,0x9D29336043B11B1ALL,0x800FC18F92A8693ALL,18446744073709551609UL,0xEFA0AA30F76A46E3LL,0xD43A08639E9ED4ACLL,0xDCDCBFBA83CAE4C6LL},{0x5B9E9D8655B36BC3LL,0xFA5D96B5594E3AEDLL,0x48FA6F4E97728F45LL,18446744073709551606UL,0x8649D6312F4DF832LL,0x9D29336043B11B1ALL,8UL},{0xDCDCBFBA83CAE4C6LL,0x157D7C93D52C492BLL,0x8649D6312F4DF832LL,0x8649D6312F4DF832LL,0x157D7C93D52C492BLL,0xDCDCBFBA83CAE4C6LL,0x48FA6F4E97728F45LL},{0xDCDCBFBA83CAE4C6LL,0x5B9E9D8655B36BC3LL,0xD43A08639E9ED4ACLL,0xFA5D96B5594E3AEDLL,0UL,18446744073709551610UL,0xAFB0B99DF8D37E51LL},{0x5B9E9D8655B36BC3LL,8UL,0xAFB0B99DF8D37E51LL,0x56C26C1F79FB91D5LL,0UL,0UL,0UL}}};
            int32_t l_1131[1][10] = {{0xC5C8E05AL,8L,1L,1L,8L,0xA7138AE5L,0xBA5CDD12L,8L,8L,0xBA5CDD12L}};
            union U2 * const l_1150[6] = {&g_548,&g_548,&g_548,&g_548,&g_548,&g_548};
            uint16_t l_1163 = 65534UL;
            int8_t l_1166 = (-2L);
            uint64_t l_1177 = 0x43038B991AFEE638LL;
            int i, j, k;
            for (g_450 = 0; (g_450 <= 1); g_450 += 1)
            { /* block id: 432 */
                uint8_t l_1120 = 255UL;
                int16_t l_1130[6] = {0xF188L,0L,0xF188L,0xF188L,0L,0xF188L};
                int32_t l_1135 = 0x8064A6AEL;
                int32_t l_1136[9];
                int32_t l_1137 = 0x5DF4C505L;
                uint32_t l_1170 = 0x46D9229EL;
                uint16_t **l_1181 = (void*)0;
                int i;
                for (i = 0; i < 9; i++)
                    l_1136[i] = 0x2B39B271L;
                for (p_35 = 0; (p_35 <= 1); p_35 += 1)
                { /* block id: 435 */
                    struct S1 ****l_1115 = (void*)0;
                    int32_t *l_1121 = &g_105;
                    int32_t l_1129 = 0x6CEA39ADL;
                    int32_t l_1132 = 1L;
                    int32_t l_1133[10] = {1L,(-1L),1L,(-1L),1L,(-1L),1L,(-1L),1L,(-1L)};
                    int i;
                    (*l_1121) |= ((+((*l_989) = (safe_lshift_func_uint16_t_u_s((l_1113[1][2][4] <= (l_1114[2] == l_1115)), ((((void*)0 == (*g_297)) < 1UL) , (safe_mul_func_int16_t_s_s(((((*g_32) == (((((safe_div_func_uint64_t_u_u(((&p_37 == (void*)0) , 0x89D19B6924D52C7BLL), p_36.f0)) || (-4L)) <= g_61.f7) == l_1000[1]) , l_1105[0])) && 0UL) & g_999[3][2]), l_1120))))))) ^ l_992);
                    for (g_86 = 0; (g_86 <= 1); g_86 += 1)
                    { /* block id: 440 */
                        int32_t *l_1122 = &l_1000[1];
                        int32_t *l_1123 = &l_992;
                        int32_t *l_1124 = &l_1000[0];
                        int32_t *l_1125 = &l_1105[0];
                        int32_t *l_1126 = &l_992;
                        int32_t *l_1127 = &l_1000[1];
                        int32_t *l_1128[9][5][5] = {{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]}},{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]}},{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]}},{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]}},{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]}},{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]}},{{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],&l_1105[2]},{&l_1105[0],&g_105,&g_105,&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0}},{{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0}},{{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0},{&l_1105[0],&l_1105[0],&l_1105[0],&l_1105[0],(void*)0}}};
                        int16_t l_1134 = 1L;
                        int i, j, k;
                        l_1138--;
                        l_1105[0] = l_1131[0][4];
                    }
                }
                for (p_36.f0 = 1; (p_36.f0 <= 4); p_36.f0 += 1)
                { /* block id: 447 */
                    int8_t **l_1141 = &l_1108;
                    int32_t l_1157[8];
                    int32_t l_1158 = (-3L);
                    int32_t l_1159 = 1L;
                    int32_t l_1160 = 0x2C8B28C0L;
                    int32_t l_1161 = 0xBAE4E033L;
                    int64_t l_1169 = (-1L);
                    int i;
                    for (i = 0; i < 8; i++)
                        l_1157[i] = (-1L);
                    if (((l_976 != ((*l_1141) = l_976)) < (safe_mod_func_int32_t_s_s(((((safe_mod_func_int32_t_s_s(((0x3.Ep+1 <= ((*g_891) , (l_992 = (g_355[p_36.f0] > ((safe_mul_func_float_f_f(((-0x1.Ap-1) == (((l_1137 = (safe_add_func_float_f_f(((void*)0 == l_1150[4]), ((safe_div_func_float_f_f(((l_1136[1] = (0x1.Fp+1 == l_1113[1][3][1])) > (-0x9.Bp-1)), l_1109)) <= l_1113[1][6][2])))) >= (-0x1.0p+1)) < 0x0.Fp-1)), l_1105[2])) <= l_1130[3]))))) , 0x9A1D8981L), 1L)) && p_36.f0) , l_1105[0]) & g_61.f7), 0x2C96E562L))))
                    { /* block id: 452 */
                        int32_t *l_1153 = &l_992;
                        int32_t *l_1154 = &l_1135;
                        int32_t *l_1155 = (void*)0;
                        int32_t *l_1156[2];
                        int32_t l_1162 = 0xF14C45FEL;
                        float *l_1178[8][4][8];
                        uint16_t **l_1179 = (void*)0;
                        uint16_t ***l_1180[7];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1156[i] = &g_105;
                        for (i = 0; i < 8; i++)
                        {
                            for (j = 0; j < 4; j++)
                            {
                                for (k = 0; k < 8; k++)
                                    l_1178[i][j][k] = &g_355[0];
                            }
                        }
                        for (i = 0; i < 7; i++)
                            l_1180[i] = &l_1179;
                        --l_1163;
                        --l_1170;
                        l_1160 = (safe_add_func_float_f_f((-0x4.7p-1), (l_1105[0] != (l_1158 != (((void*)0 == &g_143[7]) > (((*l_1153) = ((*l_1154) = (safe_mul_func_float_f_f(((g_596.f0 , &g_101[1][4][0]) != l_981[1][0][1]), (l_1131[0][1] = l_1177))))) >= p_35))))));
                        (*l_1153) |= (((*g_8) , ((l_1181 = l_1179) == (void*)0)) , l_1105[2]);
                    }
                    else
                    { /* block id: 461 */
                        uint8_t l_1191 = 255UL;
                        int32_t *l_1198 = (void*)0;
                        int32_t *l_1199 = &g_105;
                        int32_t *l_1202 = &l_1131[0][4];
                        int32_t *l_1203 = &l_1136[1];
                        int32_t *l_1204 = &l_1000[1];
                        int32_t *l_1205 = &l_1157[0];
                        int32_t *l_1206 = &l_1136[1];
                        int32_t *l_1207 = &l_1137;
                        int32_t *l_1208 = (void*)0;
                        int32_t *l_1209[4] = {&l_1000[1],&l_1000[1],&l_1000[1],&l_1000[1]};
                        int i;
                        l_1161 &= (((*l_1068) = (*g_595)) , (safe_mod_func_int16_t_s_s((!(0x38BF97A9264A0D3ELL == (safe_lshift_func_int16_t_s_u(g_69, 4)))), (g_1201 = (safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((l_1191 && ((*l_1199) = (g_61.f5 | (safe_add_func_int8_t_s_s((((*g_891) = (((safe_lshift_func_int8_t_s_u(0x67L, p_36.f0)) < p_36.f0) & (safe_add_func_int16_t_s_s(0x955EL, 9L)))) & (*g_32)), 0UL))))) ^ p_36.f0), p_36.f0)), l_1200))))));
                        p_38 = &l_1157[7];
                        l_1210++;
                    }
                    (*g_1214) = (*g_737);
                    return l_1215;
                }
                for (g_1201 = 0; (g_1201 <= 1); g_1201 += 1)
                { /* block id: 475 */
                    int32_t *l_1216 = &l_992;
                    int32_t *l_1217 = (void*)0;
                    int32_t *l_1218 = &l_1136[5];
                    int32_t *l_1219 = &l_1131[0][2];
                    int32_t *l_1220 = &l_1137;
                    int32_t *l_1221 = (void*)0;
                    int32_t *l_1222 = &l_1135;
                    int32_t *l_1223 = &l_992;
                    int32_t *l_1224 = (void*)0;
                    int32_t *l_1225 = (void*)0;
                    int32_t *l_1226 = &l_1131[0][8];
                    int32_t *l_1227 = &g_105;
                    int32_t *l_1228 = &l_1105[1];
                    int32_t *l_1229 = &l_1131[0][2];
                    int32_t *l_1230[9] = {&l_1136[2],&l_1136[2],&l_1136[2],&l_1136[2],&l_1136[2],&l_1136[2],&l_1136[2],&l_1136[2],&l_1136[2]};
                    const uint16_t ** const *l_1234 = (void*)0;
                    uint64_t l_1256 = 18446744073709551613UL;
                    int i;
                    l_1231++;
                    for (g_482.f0 = 0; (g_482.f0 <= 0); g_482.f0 += 1)
                    { /* block id: 479 */
                        struct S0 **l_1258 = &g_60;
                        int i, j, k;
                        if (l_1136[(g_482.f0 + 2)])
                            break;
                        (*l_1220) = ((0xE7L <= ((void*)0 != l_1234)) != ((safe_rshift_func_uint8_t_u_s((((safe_add_func_int64_t_s_s(l_1136[(g_450 + 3)], (l_1136[1] < ((*l_1222) && 0x8ED1AC99967DC0CFLL)))) && ((safe_unary_minus_func_int64_t_s(((safe_add_func_int64_t_s_s(p_36.f0, ((((void*)0 != &g_890) , g_476) && l_1242))) < g_119.f0))) , 1UL)) != 0x87BFF864BE5CF862LL), 1)) >= p_36.f0));
                        (*l_1223) ^= (((*l_989) ^= l_1120) && (((*g_32) | l_1243) & (((p_36 , ((((safe_div_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((l_1131[0][2] = l_1109) || ((-8L) && l_1113[1][2][4])), 0)), (safe_lshift_func_uint8_t_u_s(((safe_sub_func_float_f_f((safe_mul_func_float_f_f(g_9[(g_482.f0 + 2)][g_1201][(g_482.f0 + 4)], (g_9[(g_482.f0 + 2)][g_1201][(g_482.f0 + 2)] == p_36.f0))), 0x7.EE3F45p+77)) , 0x23L), l_1136[(g_450 + 3)])))), 255UL)) , l_1136[(g_450 + 3)]) != l_1105[0]) , 0xA6D97501L)) && l_1131[0][8]) == l_1256)));
                        (*l_1258) = l_1257;
                    }
                    for (g_371 = 1; (g_371 >= 0); g_371 -= 1)
                    { /* block id: 489 */
                        p_36 = p_36;
                    }
                }
            }
            (*g_120) = (l_992 = (**g_124));
            (*g_120) = l_1163;
        }
        else
        { /* block id: 497 */
            float *l_1268[3];
            int32_t l_1285 = 1L;
            int32_t l_1314 = (-1L);
            int32_t l_1315 = 0x57755A0AL;
            int32_t l_1316 = 0x17888564L;
            uint8_t l_1317 = 0x8AL;
            int32_t l_1340 = (-9L);
            int32_t l_1341 = (-1L);
            int32_t l_1342 = 1L;
            int32_t l_1343 = 0x12684BE4L;
            int32_t l_1344 = 0L;
            int32_t l_1345[6] = {0xFB2DD11CL,(-1L),(-1L),0xFB2DD11CL,(-1L),(-1L)};
            int32_t l_1346 = 0x2A297E04L;
            int i;
            for (i = 0; i < 3; i++)
                l_1268[i] = &g_355[3];
            for (l_1242 = (-17); (l_1242 <= 19); l_1242++)
            { /* block id: 500 */
                int16_t l_1284[8][6] = {{0xE94EL,(-1L),0xE94EL,1L,0xE94EL,(-1L)},{3L,(-1L),0x6763L,(-1L),3L,(-1L)},{0xE94EL,1L,0xE94EL,(-1L),0xE94EL,1L},{3L,1L,0x6763L,1L,3L,1L},{0xE94EL,(-1L),0xE94EL,1L,0xE94EL,(-1L)},{3L,(-1L),0x6763L,(-1L),3L,(-1L)},{0xE94EL,1L,0xE94EL,(-1L),0xE94EL,1L},{3L,1L,0x6763L,1L,3L,1L}};
                int32_t *l_1286 = &l_1105[0];
                uint64_t l_1298 = 1UL;
                int32_t l_1299[5];
                struct S0 *l_1300[5][8][6] = {{{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,(void*)0,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{(void*)0,&g_61,(void*)0,&g_61,&g_61,(void*)0},{&g_61,&g_61,(void*)0,(void*)0,&g_61,&g_61}},{{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,(void*)0},{&g_61,&g_61,(void*)0,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61}},{{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,(void*)0,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,(void*)0,&g_61,&g_61,&g_61},{&g_61,(void*)0,&g_61,&g_61,&g_61,&g_61}},{{&g_61,&g_61,(void*)0,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,(void*)0,(void*)0,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61}},{{(void*)0,(void*)0,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,(void*)0,&g_61,&g_61},{(void*)0,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,&g_61,(void*)0,&g_61,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_61,&g_61,&g_61},{&g_61,(void*)0,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_61,&g_61,(void*)0,&g_61,&g_61}}};
                uint8_t l_1301 = 0UL;
                int32_t *l_1334 = &l_1299[4];
                int32_t *l_1335 = &l_1315;
                int32_t *l_1336 = &l_1285;
                int32_t *l_1337 = &l_1105[2];
                int32_t *l_1338[7][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
                uint8_t l_1347 = 3UL;
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_1299[i] = 0xE8045727L;
                if ((((*l_1286) = (safe_unary_minus_func_uint64_t_u((g_1262 , ((((+(safe_div_func_uint16_t_u_u(((*l_993) = (g_158.f3 >= (safe_add_func_int64_t_s_s((l_1268[2] != (void*)0), ((safe_lshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u((l_1000[2] = ((safe_div_func_int16_t_s_s(((safe_add_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(((l_1105[2] != (p_36 , (*g_891))) , ((l_1281 , ((safe_rshift_func_int16_t_s_s(0x72DAL, g_482.f0)) != 0x8AFC0F02D4BA2C51LL)) | l_1284[3][0])), 12)), g_105)), 0L)) | p_35), 0x081AL)) != 0x3240C79A3BED9BCDLL)), 0x71B2L)), l_992)) , l_1285))))), l_1284[1][4]))) != p_35) , g_476) == 0x47L))))) == 0xE3E1055CL))
                { /* block id: 504 */
                    const int8_t *l_1296 = (void*)0;
                    int32_t l_1297[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_1297[i] = 0x9855235FL;
                    if (((safe_unary_minus_func_uint32_t_u(g_209)) < (safe_lshift_func_uint16_t_u_s((((safe_div_func_uint8_t_u_u(((*l_976) = ((safe_mod_func_int64_t_s_s(((*g_891) = (safe_lshift_func_int16_t_s_u(((((l_1296 != (p_36 , ((((l_1200 < ((((g_86 >= p_36.f0) || ((l_1297[1] = ((((l_1285 &= p_36.f0) | 5UL) >= ((*l_1108) &= ((*g_32) , 0x16L))) >= g_1262.f0)) && l_1297[3])) == 0x5DDCDEB5772EA3D5LL) > p_35)) >= (*l_1286)) >= g_110.f0) , l_976))) , l_1138) , p_36.f0) | g_476), l_1231))), 0xC0C597EDE629FA9FLL)) , g_5)), p_36.f0)) || 0x67E8B3DCL) != l_1298), l_1299[1]))))
                    { /* block id: 510 */
                        return l_1300[4][1][0];
                    }
                    else
                    { /* block id: 512 */
                        int32_t *l_1302 = &g_105;
                        int32_t *l_1303 = &l_992;
                        int32_t *l_1304 = &l_1000[1];
                        int32_t *l_1305 = &l_1285;
                        int32_t *l_1306 = &l_1000[1];
                        int32_t *l_1307 = &g_105;
                        int32_t *l_1308 = &g_105;
                        int32_t *l_1309 = &l_1299[1];
                        int32_t *l_1310 = &l_1285;
                        int32_t *l_1311 = &l_1105[0];
                        int32_t *l_1312 = (void*)0;
                        int32_t *l_1313[5] = {&l_1297[1],&l_1297[1],&l_1297[1],&l_1297[1],&l_1297[1]};
                        int i;
                        (*l_1286) ^= l_1301;
                        l_1317--;
                        (*l_1307) ^= (safe_div_func_int64_t_s_s(l_1285, l_1297[1]));
                        (*l_1308) &= ((*l_1311) = (*l_1286));
                    }
                }
                else
                { /* block id: 519 */
                    int32_t l_1327 = 0xA49345B0L;
                    if ((**g_124))
                        break;
                    for (g_482.f0 = 6; (g_482.f0 == 10); ++g_482.f0)
                    { /* block id: 523 */
                        int32_t *l_1324 = &l_1314;
                        int32_t *l_1325 = &l_992;
                        int32_t *l_1326[7][1][4] = {{{&l_1299[0],&l_1299[3],&l_992,&l_992}},{{&l_1105[2],&l_1105[2],(void*)0,&l_1299[3]}},{{&l_1299[3],&l_1299[0],(void*)0,&l_1299[0]}},{{&l_1105[2],&l_1000[1],&l_992,(void*)0}},{{&l_1299[0],&l_1000[1],&l_1000[1],&l_1299[0]}},{{&l_1000[1],&l_1299[0],&l_1105[2],&l_1299[3]}},{{&l_1000[1],&l_1105[2],&l_1000[1],&l_992}}};
                        uint32_t l_1328[2];
                        int32_t **l_1331 = (void*)0;
                        int32_t **l_1332 = &g_310;
                        struct S0 *l_1333 = &g_61;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1328[i] = 0x02E618A4L;
                        --l_1328[0];
                        (*l_1332) = (void*)0;
                        return l_1333;
                    }
                    (*l_1286) = p_36.f0;
                }
                l_1347++;
            }
        }
    }
    for (g_119.f0 = 0; (g_119.f0 <= 5); g_119.f0 += 1)
    { /* block id: 536 */
        struct S0 *l_1350 = &g_61;
        struct S0 **l_1351[8] = {&l_1257,&l_1350,&l_1350,&l_1257,&l_1350,&l_1350,&l_1257,&l_1350};
        float *l_1353 = &g_9[0][0][3];
        int32_t l_1357[9][3] = {{(-9L),0x018AC7F4L,0xFAC3735EL},{(-9L),(-9L),0x018AC7F4L},{7L,0x018AC7F4L,0x018AC7F4L},{0x018AC7F4L,(-5L),0xFAC3735EL},{7L,(-5L),7L},{(-9L),0x018AC7F4L,0xFAC3735EL},{(-9L),(-9L),0x018AC7F4L},{7L,0x018AC7F4L,0x018AC7F4L},{0x018AC7F4L,(-5L),0xFAC3735EL}};
        int32_t l_1446 = 1L;
        uint16_t * const *l_1466 = &l_993;
        int64_t l_1467 = 0xB6A6E822FF15E3D6LL;
        int32_t l_1468 = 0x4A0CC3D8L;
        struct S1 ***l_1529 = &g_1495;
        struct S1 ****l_1528[3][9] = {{&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529},{&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529},{&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529,&l_1529}};
        int64_t ** const l_1595 = &g_891;
        int i, j;
        (*l_1353) = ((l_1257 = l_1350) != l_1352);
        if (l_1354)
            break;
        if ((safe_sub_func_int16_t_s_s(((*l_989) = 0x1BE6L), (l_1357[4][1] , ((safe_mul_func_int8_t_s_s((safe_sub_func_int8_t_s_s(((0xBF37092642FBF63ALL && p_36.f0) && 0x5C38L), (safe_div_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((0xFB98L ^ l_1357[5][0]), (safe_div_func_int8_t_s_s(g_1201, g_101[0][4][0])))), l_1357[0][1])))), g_1339[5][1][0])) || 5L)))))
        { /* block id: 541 */
            const int32_t *l_1369[10][3][4] = {{{&g_105,&l_1000[1],&g_105,&l_1357[4][1]},{&l_1000[1],&g_2,&l_1000[1],&g_105},{&g_2,&l_1357[4][1],&l_992,&g_2}},{{&l_1357[4][2],&l_1357[4][1],&g_5,(void*)0},{&l_1000[1],&g_2,&l_1000[1],&l_1357[4][1]},{&g_2,&l_992,&g_2,&g_2}},{{&l_1357[4][1],&l_1357[4][2],&g_5,(void*)0},{&l_1000[1],(void*)0,&g_2,&l_1357[4][2]},{&l_992,&l_1357[4][1],&g_2,&g_2}},{{&l_1000[1],&g_105,&g_5,&l_1357[4][1]},{&l_1357[4][1],&l_1000[2],&g_2,&l_1000[1]},{&g_2,&l_1000[1],&l_1000[1],(void*)0}},{{&l_1000[1],&g_105,&g_5,&g_105},{&l_1357[4][2],&l_992,&l_992,&l_1357[4][2]},{&g_2,&l_1357[4][1],&l_1000[1],&g_5}},{{&l_1000[1],&l_1357[4][2],&g_105,&l_1357[4][1]},{&g_105,&l_992,&g_2,&l_1357[4][1]},{&l_1000[1],&l_1357[4][2],&l_1000[2],&g_5}},{{&l_1000[1],&l_1357[4][1],&l_1357[4][1],&l_1357[4][2]},{&l_1357[4][1],&l_992,&g_2,&g_105},{&l_1000[1],&g_105,&l_1000[2],(void*)0}},{{&l_1357[4][1],&l_1000[1],&l_1357[7][2],&l_1000[1]},{(void*)0,&g_5,&g_5,&g_2},{&g_5,&l_1000[1],&l_1357[7][2],&l_1000[2]}},{{&l_992,&g_2,&l_1357[4][1],&l_992},{&l_992,&l_1357[4][1],&l_1357[7][2],&g_2},{&g_5,&l_992,&g_5,&g_5}},{{(void*)0,&g_5,&l_1357[4][1],&l_1000[2]},{&l_1000[1],&l_1000[1],&g_5,&g_2},{&l_1357[7][2],&g_2,&l_1000[1],&l_1000[1]}}};
            const int32_t **l_1368 = &l_1369[5][0][0];
            uint32_t *l_1389 = &g_482.f0;
            int32_t l_1408 = 1L;
            int i, j, k;
            (*l_1368) = p_38;
            for (g_482.f0 = 0; (g_482.f0 <= 1); g_482.f0 += 1)
            { /* block id: 545 */
                int32_t l_1384 = 0xDA694DD5L;
                float l_1391 = (-0x5.7p+1);
                int i;
                g_121[(g_482.f0 + 3)] ^= (0x4E51478837A61457LL && 0x4674350A26F4990BLL);
                for (g_209 = 4; (g_209 >= 1); g_209 -= 1)
                { /* block id: 549 */
                    int32_t l_1377 = (-1L);
                    uint64_t *l_1380[6];
                    uint32_t l_1383 = 18446744073709551613UL;
                    uint32_t *l_1390[2][9] = {{&l_1210,&l_1210,(void*)0,&l_1210,&l_1210,(void*)0,&l_1210,&l_1210,(void*)0},{&l_1041.f0,&l_1041.f0,&g_1201,&l_1041.f0,&l_1041.f0,&g_1201,&l_1041.f0,&l_1041.f0,&g_1201}};
                    uint16_t **l_1419 = &l_993;
                    uint16_t ***l_1418 = &l_1419;
                    int i, j, k;
                    for (i = 0; i < 6; i++)
                        l_1380[i] = &g_116;
                    if ((((safe_sub_func_int16_t_s_s(((((g_1372[1][6][0] , ((safe_div_func_uint8_t_u_u((g_1339[g_119.f0][(g_482.f0 + 2)][g_482.f0] == (((g_1262.f2 >= (safe_sub_func_uint32_t_u_u(l_1377, p_36.f0))) || (safe_mod_func_int8_t_s_s(((g_116--) >= (*g_891)), (p_36.f0 , p_36.f0)))) != (*g_891))), 1UL)) , l_1383)) > 7L) | g_5) != p_35), l_1384)) & 0x8803F739L) | p_35))
                    { /* block id: 551 */
                        if (g_476)
                            goto lbl_1385;
                        l_1352 = (*g_737);
                        l_1357[4][1] = l_1357[4][1];
                    }
                    else
                    { /* block id: 555 */
                        int32_t *l_1386 = &l_1000[0];
                        int8_t *l_1394 = &g_143[6];
                        int32_t *l_1403 = &l_1000[1];
                        int32_t *l_1404 = &l_992;
                        int32_t *l_1405 = &l_1384;
                        int32_t *l_1406 = (void*)0;
                        int32_t *l_1407[4][10] = {{&l_1377,&g_2,&l_1377,(void*)0,(void*)0,&l_1377,&g_2,&l_1377,(void*)0,(void*)0},{&l_1377,&g_2,&l_1377,(void*)0,(void*)0,&l_1377,&g_2,&l_1377,(void*)0,(void*)0},{&l_1377,&g_2,&l_1377,(void*)0,(void*)0,&l_1377,&g_2,&l_1377,(void*)0,(void*)0},{&l_1377,&g_2,&l_1377,(void*)0,(void*)0,&l_1377,&g_2,&l_1377,(void*)0,(void*)0}};
                        int i, j;
                        (*l_1386) |= (g_994[1][4] ^ 0xFE3A4C84L);
                        (*l_1386) = (((*l_1394) |= (safe_div_func_uint64_t_u_u(((l_1390[1][0] = l_1389) != (p_35 , &g_1201)), (0x557AL & (safe_mul_func_int8_t_s_s(g_1372[1][6][0].f5, g_1372[1][6][0].f6)))))) < ((safe_mod_func_int64_t_s_s((g_1262.f4 , (safe_add_func_uint32_t_u_u(g_476, ((safe_sub_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(p_35, (-1L))), p_36.f0)) >= g_1372[1][6][0].f5)))), g_1262.f2)) <= 0UL));
                        g_1409++;
                    }
                    (*g_120) = ((p_35 != (safe_add_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_u(((((((safe_sub_func_uint64_t_u_u((((void*)0 != l_1418) && p_35), (*g_32))) , 65529UL) < (-1L)) , ((g_61.f4 = (safe_lshift_func_int16_t_s_s((g_1372[1][6][0].f4 , ((((-2L) || p_36.f0) == g_101[0][4][0]) | (**g_890))), g_1372[1][6][0].f0))) & 0xCE211E66L)) , p_35) || 0xB3L), g_1262.f0)) >= l_1138), l_1242))) ^ g_1262.f7);
                }
            }
        }
        else
        { /* block id: 566 */
            int32_t *l_1422 = &l_1000[0];
            int32_t *l_1423[7][4];
            int64_t l_1424 = 1L;
            int i, j;
            for (i = 0; i < 7; i++)
            {
                for (j = 0; j < 4; j++)
                    l_1423[i][j] = &l_1000[0];
            }
            ++l_1427[1];
            (*l_1422) |= ((safe_mod_func_int64_t_s_s((((safe_rshift_func_int16_t_s_s(g_1262.f7, ((((safe_rshift_func_uint16_t_u_s((((safe_lshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u(p_36.f0, (safe_add_func_uint8_t_u_u(g_5, (safe_mod_func_int16_t_s_s(g_61.f7, (l_1357[2][0] | ((l_1041.f0 <= ((((((0xDEAF2F22A5F1C8BDLL && l_1357[5][1]) , (safe_div_func_uint16_t_u_u((--(*l_993)), ((safe_rshift_func_uint16_t_u_u(0UL, 9)) , p_35)))) == 1L) & 0xC9L) < l_1446) ^ p_36.f0)) , (**g_124))))))))), g_103)) && p_35) >= l_1357[4][1]), 14)) == l_1357[0][2]) < l_1357[4][1]) & 2UL))) >= g_1426) && l_1357[2][2]), 7L)) < 254UL);
            g_105 &= (((**g_297) , (((((safe_sub_func_uint32_t_u_u((safe_div_func_uint32_t_u_u(((safe_mod_func_uint8_t_u_u((0x0B2C09C5L == l_1210), (safe_mul_func_uint16_t_u_u(p_35, l_1210)))) > (safe_div_func_uint8_t_u_u((g_1461 , p_36.f0), (++(*l_976))))), p_35)), (l_1467 = (safe_mul_func_uint8_t_u_u(((void*)0 == l_1466), g_371))))) < p_36.f0) || (**g_124)) | 1UL) || (**g_890))) && (*l_1422));
            ++l_1469;
        }
        for (l_1041.f0 = 0; (l_1041.f0 <= 5); l_1041.f0 += 1)
        { /* block id: 577 */
            uint32_t l_1502 = 1UL;
            const int32_t l_1527 = 0xA427758FL;
            struct S0 *l_1542 = &g_1372[0][8][0];
            int32_t l_1546 = 0xBFEA03F8L;
            int32_t l_1554 = (-5L);
            int8_t l_1556 = 0L;
            int32_t *l_1563 = (void*)0;
            int32_t *l_1564 = &l_1000[1];
            int32_t *l_1565 = &l_1357[4][1];
            int32_t *l_1566 = (void*)0;
            int32_t *l_1567 = (void*)0;
            int32_t *l_1568[9][5][5] = {{{&l_1468,&l_992,&l_1357[4][1],(void*)0,&g_5},{(void*)0,&l_1546,&l_1000[2],&l_1357[2][0],&l_1357[4][1]},{(void*)0,&l_1446,&l_1000[1],&g_105,&l_992},{&g_5,(void*)0,&g_2,&l_1357[4][1],&l_992},{&l_1357[4][1],&l_1357[3][1],&g_105,&l_1554,&l_1446}},{{&l_1468,(void*)0,&l_1446,(void*)0,(void*)0},{(void*)0,&g_2,&l_1446,&g_5,&l_1000[1]},{&g_5,&l_1357[4][1],&g_105,&g_2,(void*)0},{&l_1546,&l_1357[6][1],&g_2,&l_1000[2],&l_1357[3][1]},{&l_992,(void*)0,&l_1000[1],&g_5,&l_1446}},{{&l_1546,&g_105,&l_1000[2],&l_1000[2],&g_105},{&l_1357[6][1],(void*)0,&l_1357[4][1],&g_105,&g_2},{&l_1357[4][1],&l_1357[4][1],&l_1446,&l_1446,&l_1446},{&l_1000[2],&l_1000[1],&g_5,&l_1446,&g_2},{&l_1357[4][1],&l_1546,&l_1000[1],(void*)0,&g_105}},{{&l_1357[6][1],&l_1000[2],(void*)0,&l_1357[4][1],(void*)0},{&l_1546,&l_1357[4][1],&l_1554,&l_1468,(void*)0},{&l_992,&l_1468,(void*)0,(void*)0,&l_1546},{&l_1546,&l_1554,&l_1000[1],&l_1468,&g_2},{&g_5,(void*)0,&g_5,&l_1000[1],&l_992}},{{(void*)0,(void*)0,&l_992,&l_1000[1],(void*)0},{&l_1357[6][1],&l_1554,&l_1000[1],&l_1000[1],&g_105},{&g_5,&l_1000[2],(void*)0,&l_1468,&g_5},{&l_1357[4][1],&l_1546,&g_2,&l_1357[6][1],&l_1468},{&l_1468,&l_1000[1],(void*)0,&l_992,(void*)0}},{{&g_2,&g_2,&l_992,&l_1446,&l_1000[2]},{(void*)0,&l_1357[4][1],(void*)0,&l_1000[2],&l_1468},{&l_1000[1],&l_1546,&l_1468,&l_1357[4][1],&l_1357[4][1]},{&l_1446,&l_1357[4][1],&l_1000[2],&l_1468,(void*)0},{&l_1468,&g_2,&g_2,(void*)0,(void*)0}},{{&l_992,&l_1000[1],&l_1446,&l_1000[2],&l_1000[2]},{&l_1446,&l_1546,(void*)0,&g_105,&g_5},{&l_992,&l_1000[2],&l_1468,(void*)0,&l_992},{(void*)0,&l_1554,&l_1446,&l_992,&l_1357[3][1]},{&l_992,(void*)0,&l_1446,(void*)0,&l_1357[3][1]}},{{&l_1468,(void*)0,&l_1546,&l_1554,&l_992},{&l_1357[4][1],&l_1554,&l_1468,&l_1446,&g_5},{&l_1554,(void*)0,(void*)0,(void*)0,&l_1000[2]},{&l_1357[4][1],&l_992,&g_105,(void*)0,(void*)0},{&l_1357[4][1],(void*)0,&l_1357[4][1],&l_992,(void*)0}},{{&l_1357[3][1],&g_5,&l_1554,&g_2,&l_1357[4][1]},{(void*)0,&l_1000[1],&g_105,&g_2,&l_1468},{&l_1000[2],&g_5,&l_1554,&l_1357[4][1],&l_1000[2]},{&l_992,&l_1000[1],&l_1357[4][1],&l_992,(void*)0},{(void*)0,(void*)0,&g_105,&l_992,&l_1468}}};
            uint8_t l_1569 = 0xBAL;
            union U2 *l_1573[6][3][6] = {{{(void*)0,&g_548,(void*)0,(void*)0,&g_1461,&g_1461},{&g_548,&g_1461,&g_548,(void*)0,(void*)0,&g_548},{&g_1461,&g_1461,&g_548,(void*)0,(void*)0,(void*)0}},{{&g_548,&g_1461,&g_1461,(void*)0,&g_548,&g_548},{(void*)0,&g_1461,(void*)0,&g_548,&g_548,(void*)0},{&g_548,&g_548,&g_1461,&g_1461,&g_1461,&g_1461}},{{&g_548,&g_548,&g_1461,(void*)0,&g_1461,&g_548},{&g_548,&g_548,(void*)0,&g_1461,(void*)0,&g_1461},{&g_548,&g_548,(void*)0,&g_1461,&g_548,&g_548}},{{&g_548,&g_1461,&g_1461,(void*)0,(void*)0,&g_1461},{(void*)0,(void*)0,&g_1461,(void*)0,&g_1461,(void*)0},{&g_1461,&g_548,&g_548,&g_1461,&g_548,&g_548}},{{&g_548,&g_548,(void*)0,&g_548,&g_548,&g_1461},{&g_1461,&g_548,&g_548,&g_1461,&g_1461,(void*)0},{(void*)0,&g_548,&g_548,&g_548,&g_548,&g_1461}},{{(void*)0,&g_548,(void*)0,(void*)0,&g_548,&g_1461},{(void*)0,&g_548,&g_548,&g_548,&g_1461,&g_1461},{(void*)0,&g_548,(void*)0,(void*)0,&g_548,(void*)0}}};
            union U2 * const *l_1572 = &l_1573[4][0][5];
            struct S1 **l_1629 = &g_1496;
            uint8_t *l_1673 = &l_1469;
            int8_t l_1715[7][7][4] = {{{0x6FL,(-3L),0x2EL,0xAAL},{0L,7L,0x9DL,0L},{(-6L),0x19L,3L,1L},{0x9DL,1L,0xD3L,(-1L)},{0L,(-1L),0xD7L,0x2EL},{0L,3L,1L,3L},{0xD3L,1L,0x76L,1L}},{{0x2EL,(-1L),0x73L,(-3L)},{0L,0x76L,0x19L,1L},{0L,0x3AL,0x73L,(-1L)},{0x2EL,1L,0x76L,0x50L},{0xD3L,0x8BL,1L,0L},{0L,(-6L),0xD7L,0xD3L},{0L,0x6FL,0xD3L,0x6AL}},{{0x9DL,(-2L),3L,(-7L)},{(-6L),(-1L),0x9DL,1L},{0L,0x2EL,0x2EL,0L},{0x6FL,0xD7L,1L,(-1L)},{(-7L),0x6AL,0xE5L,0x3AL},{0x19L,0x50L,7L,0x3AL},{(-1L),0x6AL,1L,(-1L)}},{{(-7L),0xD7L,1L,0L},{(-3L),0x2EL,0xAAL,1L},{0xE5L,(-1L),1L,(-7L)},{0x73L,(-2L),1L,0x6AL},{0x8BL,0x6FL,0L,0xD3L},{0xAAL,(-6L),(-7L),0L},{6L,0x8BL,6L,0x50L}},{{0x50L,1L,0x6AL,(-1L)},{1L,0x3AL,(-7L),1L},{(-1L),0x76L,(-7L),(-3L)},{1L,(-1L),0x6AL,1L},{0x50L,1L,6L,3L},{6L,3L,(-7L),0x2EL},{0xAAL,(-1L),0L,(-1L)}},{{0x8BL,1L,1L,1L},{0x73L,0x19L,1L,0L},{0x6AL,(-5L),(-7L),(-7L)},{(-1L),(-1L),0L,(-6L)},{8L,0xAAL,0xE5L,1L},{0x2EL,(-1L),(-5L),0xE5L},{0x06L,(-1L),0x6AL,1L}},{{(-1L),0xAAL,(-2L),(-6L)},{0x76L,(-1L),(-1L),(-7L)},{0L,(-5L),0x6FL,(-1L)},{6L,0x06L,7L,0L},{0x6FL,1L,0xAAL,1L},{1L,0x2EL,1L,(-1L)},{(-1L),7L,(-1L),7L}}};
            int64_t ***l_1718 = (void*)0;
            union U2 *l_1721 = &g_548;
            struct S0 ***l_1728 = (void*)0;
            struct S1 ****l_1730[4][1];
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1730[i][j] = &l_1529;
            }
            for (l_1469 = 0; (l_1469 <= 5); l_1469 += 1)
            { /* block id: 580 */
                int32_t l_1485 = 0xF41F45D3L;
                struct S1 **l_1499[3];
                int32_t l_1547 = 0x0824E1F2L;
                int32_t l_1548 = (-2L);
                int32_t l_1549 = 0x3E991116L;
                int32_t l_1551[7][1] = {{2L},{0xBD644844L},{0xBD644844L},{2L},{0xBD644844L},{0xBD644844L},{2L}};
                int i, j;
                for (i = 0; i < 3; i++)
                    l_1499[i] = &g_1496;
                for (g_209 = 0; (g_209 <= 1); g_209 += 1)
                { /* block id: 583 */
                    struct S1 ***l_1494[8][6][5] = {{{(void*)0,(void*)0,&l_1067,(void*)0,&l_1067},{(void*)0,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,(void*)0,(void*)0,&l_1067,(void*)0},{(void*)0,&l_1067,&l_1067,(void*)0,&l_1067},{&l_1067,&l_1067,(void*)0,&l_1067,&l_1067},{&l_1067,(void*)0,&l_1067,(void*)0,(void*)0}},{{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,(void*)0,(void*)0,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067}},{{&l_1067,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,(void*)0,&l_1067},{(void*)0,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067}},{{(void*)0,(void*)0,&l_1067,&l_1067,(void*)0},{(void*)0,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,&l_1067,(void*)0,(void*)0,&l_1067},{&l_1067,(void*)0,&l_1067,&l_1067,&l_1067},{(void*)0,(void*)0,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067}},{{(void*)0,&l_1067,(void*)0,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,(void*)0,(void*)0,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067},{(void*)0,(void*)0,(void*)0,&l_1067,&l_1067},{&l_1067,(void*)0,&l_1067,&l_1067,&l_1067}},{{(void*)0,&l_1067,&l_1067,&l_1067,(void*)0},{(void*)0,&l_1067,&l_1067,&l_1067,&l_1067},{(void*)0,&l_1067,&l_1067,(void*)0,&l_1067},{&l_1067,&l_1067,&l_1067,(void*)0,&l_1067},{&l_1067,&l_1067,&l_1067,(void*)0,&l_1067},{&l_1067,(void*)0,(void*)0,(void*)0,&l_1067}},{{(void*)0,(void*)0,&l_1067,&l_1067,&l_1067},{(void*)0,&l_1067,&l_1067,&l_1067,&l_1067},{(void*)0,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,(void*)0,&l_1067},{(void*)0,(void*)0,&l_1067,(void*)0,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067}},{{&l_1067,&l_1067,(void*)0,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,(void*)0},{&l_1067,&l_1067,&l_1067,(void*)0,&l_1067},{(void*)0,(void*)0,&l_1067,(void*)0,&l_1067},{&l_1067,&l_1067,&l_1067,&l_1067,&l_1067}}};
                    int32_t *l_1500 = &l_1446;
                    int64_t **l_1536 = &g_891;
                    int64_t **l_1537 = (void*)0;
                    int64_t *l_1539 = &g_404[2];
                    int64_t **l_1538 = &l_1539;
                    int64_t *l_1541 = &g_86;
                    int64_t **l_1540 = &l_1541;
                    int8_t l_1544[9][5][3] = {{{0L,5L,(-6L)},{(-9L),(-9L),(-5L)},{0xA4L,0x42L,0x25L},{(-9L),1L,0L},{0L,0x42L,(-6L)}},{{0xC7L,(-9L),0L},{0xA4L,5L,0x25L},{0xC7L,1L,(-5L)},{0L,5L,(-6L)},{(-9L),(-9L),(-5L)}},{{0xA4L,0x42L,0x25L},{(-9L),1L,0L},{0L,0x42L,(-6L)},{0xC7L,(-9L),0L},{0xA4L,5L,0x25L}},{{0xC7L,1L,(-5L)},{0L,5L,(-6L)},{(-9L),(-9L),(-5L)},{0xA4L,0x42L,0x25L},{(-9L),1L,0L}},{{0L,0x42L,(-6L)},{0xC7L,(-9L),0L},{0xA4L,5L,0x25L},{0xC7L,1L,(-5L)},{0L,5L,(-6L)}},{{(-9L),(-9L),(-5L)},{0xA4L,0x42L,0x25L},{(-9L),1L,0L},{0L,0x42L,(-6L)},{0x9BL,0xDBL,1L}},{{0xFDL,0L,0xA4L},{0x9BL,(-3L),0x0DL},{0xBEL,0L,(-6L)},{0xDBL,0xDBL,0x0DL},{0xFDL,6L,0xA4L}},{{0xDBL,(-3L),1L},{0xBEL,6L,(-6L)},{0x9BL,0xDBL,1L},{0xFDL,0L,0xA4L},{0x9BL,(-3L),0x0DL}},{{0xBEL,0L,(-6L)},{0xDBL,0xDBL,0x0DL},{0xFDL,6L,0xA4L},{0xDBL,(-3L),1L},{0xBEL,6L,(-6L)}}};
                    int32_t l_1550[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
                    int32_t l_1553 = 5L;
                    uint32_t l_1557 = 0x8BEAD20BL;
                    int i, j, k;
                    for (p_36.f0 = 0; (p_36.f0 <= 1); p_36.f0 += 1)
                    { /* block id: 586 */
                        int32_t l_1475 = 0x6F28F2C2L;
                        int32_t *l_1483 = (void*)0;
                        int32_t *l_1484 = &l_992;
                        int8_t *l_1486 = &g_548.f3;
                        int32_t *l_1487 = &l_1446;
                        int i, j, k;
                        (*g_1472) = &l_1357[5][0];
                        if (g_105)
                            goto lbl_1385;
                        (*l_1487) ^= ((safe_sub_func_uint8_t_u_u(g_1339[(g_209 + 6)][(p_36.f0 + 1)][p_36.f0], l_1475)) , (g_121[6] <= (l_1467 != (safe_sub_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u((+(l_1357[4][1] && ((g_476 = (((l_1000[1] = (safe_mul_func_int8_t_s_s(g_82, ((*l_1486) |= (l_1485 |= (((*l_1484) &= (*g_310)) <= ((1L || p_35) < p_35))))))) , (void*)0) != p_38)) && p_36.f0))), g_1372[1][6][0].f2)), p_35)))));
                    }
                    if (((safe_mod_func_int32_t_s_s(((*l_1500) = (0x06L && (((0x0064A88BFDA83DA5LL ^ (safe_sub_func_int32_t_s_s((safe_add_func_int16_t_s_s(((g_1495 = &l_1068) == (void*)0), (p_36.f0 & (++(**l_1466))))), (((g_143[7] = g_33) < 8L) > p_36.f0)))) , &g_1496) == l_1499[2]))), (-1L))) >= g_220[3]))
                    { /* block id: 600 */
                        (*g_120) &= (l_1446 ^= g_1501[0]);
                        if (l_1485)
                            continue;
                    }
                    else
                    { /* block id: 604 */
                        int8_t *l_1503 = &g_548.f3;
                        int32_t l_1513 = (-1L);
                        uint8_t *l_1514 = &g_1515;
                        int8_t *l_1516 = &g_1461.f3;
                        int64_t *l_1530 = &g_404[2];
                        int32_t **l_1531 = &g_310;
                        (*l_1531) = ((((((*l_1503) = (l_1502 &= 0xA7L)) <= g_968[0].f3) > ((safe_rshift_func_int8_t_s_u(((((*l_1530) = (safe_unary_minus_func_int64_t_s((safe_add_func_uint16_t_u_u(((((l_1485 = ((*g_891) = ((((((*l_1516) &= (!((*l_1514) &= (g_82 <= ((3UL <= (safe_unary_minus_func_int8_t_s((safe_mod_func_uint16_t_u_u(p_36.f0, l_1513))))) | g_1426))))) != ((safe_rshift_func_uint8_t_u_u((((safe_mod_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(0xDFL, (((safe_rshift_func_int16_t_s_u((((safe_add_func_int16_t_s_s((-7L), l_1527)) , 0x5940L) < p_35), l_1357[4][1])) && 0xFA086D478B66BC7DLL) != g_61.f7))), 0x2AL)) && l_1000[1]) & 0UL), 0)) <= g_574.f3)) , p_35) , l_1528[2][1]) == (void*)0))) || (*l_1500)) != 0xE1304832AFC9AB70LL) || 0x497968B7L), 0x8DADL))))) < 18446744073709551612UL) > (*l_1500)), l_1357[4][1])) & (-1L))) < p_36.f0) , (void*)0);
                        (*l_1500) |= 1L;
                        if (l_1485)
                            break;
                    }
                    if ((safe_div_func_int8_t_s_s((((*l_1536) = (g_1534 , l_1535)) != ((*l_1540) = ((*l_1538) = &g_404[5]))), (l_1467 | g_2))))
                    { /* block id: 619 */
                        return l_1542;
                    }
                    else
                    { /* block id: 621 */
                        int32_t *l_1543[6][3] = {{&l_1446,&l_1446,&l_1468},{&l_1446,&l_1446,&l_1468},{&l_1446,&l_1446,&l_1468},{&l_1446,&l_1446,&l_1468},{&l_1446,&l_1446,&l_1468},{&l_1446,&l_1446,&l_1468}};
                        int i, j;
                        ++l_1557;
                    }
                    for (l_1446 = 0; (l_1446 <= 1); l_1446 += 1)
                    { /* block id: 626 */
                        (*g_1560) = (p_36.f0 , &l_1494[2][4][1]);
                    }
                }
                return (*g_1214);
            }
            ++l_1569;
            for (l_1569 = 0; (l_1569 <= 5); l_1569 += 1)
            { /* block id: 635 */
                uint16_t l_1600 = 1UL;
                int32_t l_1609 = 0x181C4BF7L;
                float **l_1701 = &l_1353;
                struct S0 ****l_1729 = &l_1728;
                struct S1 *****l_1731[7] = {&l_1730[3][0],&l_1730[3][0],&l_1730[3][0],&l_1730[3][0],&l_1730[3][0],&l_1730[3][0],&l_1730[3][0]};
                int i;
            }
        }
    }
    return l_1352;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static struct S1  func_40(uint16_t  p_41, float * p_42, int16_t  p_43)
{ /* block id: 373 */
    int32_t l_973 = 0xBF67DD54L;
    struct S1 l_974 = {0x5F3A8BE0L};
    l_973 ^= p_43;
    return l_974;
}


/* ------------------------------------------ */
/* 
 * reads : g_158.f0 g_69 g_32 g_33 g_143 g_574 g_481 g_548.f0 g_594 g_404 g_476 g_82 g_61.f3 g_86 g_116 g_105 g_642 g_595 g_653 g_668 g_677 g_374 g_342 g_61.f5 g_737 g_120 g_124 g_121 g_101 g_596 g_371 g_60 g_61 g_133.f0 g_560 g_574.f0 g_103 g_158.f3 g_220 g_110.f0 g_890 g_891 g_8 g_298 g_158 g_133 g_968 g_970
 * writes: g_69 g_450 g_482 g_371 g_476 g_508 g_143 g_82 g_86 g_105 g_310 g_596 g_677 g_688 g_33 g_61.f5 g_355 g_60 g_404 g_121 g_101 g_103 g_9 g_953
 */
static uint16_t  func_44(float * p_45, int32_t  p_46)
{ /* block id: 234 */
    int32_t l_561[4];
    uint64_t *l_604 = &g_33;
    uint64_t **l_603 = &l_604;
    uint32_t *l_693[5] = {&g_119.f0,&g_119.f0,&g_119.f0,&g_119.f0,&g_119.f0};
    int32_t l_733 = 7L;
    int32_t l_766 = 0xAA7C94C0L;
    int16_t l_767 = 0x67A8L;
    int16_t l_811 = 0x98BEL;
    struct S0 **l_876 = &g_60;
    int64_t **l_931 = &g_891;
    int64_t ** const *l_948[4] = {&l_931,&l_931,&l_931,&l_931};
    const int64_t l_971 = (-5L);
    int i;
    for (i = 0; i < 4; i++)
        l_561[i] = 0xEE412549L;
    if (l_561[0])
    { /* block id: 235 */
        return g_158.f0;
    }
    else
    { /* block id: 237 */
        int8_t l_575 = 0x4EL;
        uint32_t l_585 = 0UL;
        float l_590 = 0x1.Fp-1;
        struct S1 l_591 = {0x2B3496C6L};
        const struct S1 * const l_657 = &g_658;
        const struct S1 * const *l_656 = &l_657;
        int32_t l_671[4][4] = {{0x75B2DA19L,0xD34B2647L,0x75B2DA19L,0xD34B2647L},{0x75B2DA19L,0xD34B2647L,0x75B2DA19L,0xD34B2647L},{0x75B2DA19L,0xD34B2647L,0x75B2DA19L,0xD34B2647L},{0x75B2DA19L,0xD34B2647L,0x75B2DA19L,0xD34B2647L}};
        int32_t l_672[9][1] = {{0x8B8A26C1L},{(-4L)},{(-4L)},{0x8B8A26C1L},{(-4L)},{(-4L)},{0x8B8A26C1L},{(-4L)},{(-4L)}};
        const float *l_690 = (void*)0;
        uint32_t l_755 = 0x02FFC566L;
        uint16_t l_816 = 4UL;
        uint16_t l_916 = 0x4EB2L;
        int i, j;
lbl_673:
        for (g_69 = 0; (g_69 <= 3); g_69 += 1)
        { /* block id: 240 */
            uint16_t *l_573 = &g_450;
            struct S1 l_576 = {18446744073709551615UL};
            int i;
            l_561[g_69] = ((((((safe_add_func_int8_t_s_s((safe_div_func_uint32_t_u_u(6UL, ((safe_mod_func_uint16_t_u_u(l_561[g_69], (7L && (safe_div_func_int16_t_s_s(6L, l_561[g_69]))))) & (((&g_481 == (((safe_div_func_uint8_t_u_u(((*g_32) == p_46), (((*l_573) = (+(0L && l_561[0]))) ^ g_143[0]))) , g_574) , (void*)0)) , 4294967295UL) & l_575)))), l_561[g_69])) > 0x6529F9C1L) != l_561[g_69]) < p_46) < 2UL) > p_46);
            (*g_481) = l_576;
        }
        for (g_371 = 16; (g_371 != (-28)); g_371--)
        { /* block id: 247 */
            struct S1 *l_593 = (void*)0;
            struct S1 ** const l_592 = &l_593;
            int32_t l_633[10] = {(-2L),0L,(-2L),(-2L),0L,(-2L),(-2L),0L,(-2L),(-2L)};
            uint64_t **l_634 = (void*)0;
            int32_t *l_641 = &l_633[8];
            int32_t *l_645 = &l_561[1];
            const struct S1 * const **l_655[8][10][3] = {{{&g_653,&g_653,(void*)0},{&g_653,&g_653,&g_653},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653}},{{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{(void*)0,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653}},{{(void*)0,&g_653,&g_653},{(void*)0,&g_653,(void*)0},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653}},{{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653}},{{&g_653,&g_653,(void*)0},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,(void*)0,(void*)0},{&g_653,&g_653,(void*)0},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653}},{{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,(void*)0},{&g_653,&g_653,(void*)0},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,(void*)0},{&g_653,&g_653,&g_653}},{{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653}},{{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,&g_653},{&g_653,&g_653,(void*)0},{&g_653,(void*)0,&g_653},{&g_653,&g_653,&g_653}}};
            uint8_t *l_669 = (void*)0;
            uint8_t *l_670[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            struct S0 *l_735 = &g_61;
            struct S1 ***l_745 = (void*)0;
            int64_t ** const **l_949 = &l_948[1];
            int64_t * const *l_951 = &g_891;
            int64_t * const **l_950 = &l_951;
            int64_t * const ***l_952[5][5][7] = {{{&l_950,&l_950,(void*)0,&l_950,&l_950,(void*)0,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950},{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950}},{{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950},{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950}},{{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950},{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950},{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950}},{{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950},{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950}},{{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950,&l_950},{&l_950,&l_950,&l_950,&l_950,(void*)0,&l_950,&l_950},{&l_950,&l_950,&l_950,(void*)0,(void*)0,&l_950,&l_950}}};
            int i, j, k;
            for (g_476 = (-23); (g_476 < 5); g_476 = safe_add_func_uint32_t_u_u(g_476, 4))
            { /* block id: 250 */
                int32_t l_629[1];
                int32_t l_631 = (-1L);
                struct S1 l_646 = {1UL};
                int i;
                for (i = 0; i < 1; i++)
                    l_629[i] = 1L;
                if ((((safe_sub_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(l_585, 6)), ((safe_sub_func_uint8_t_u_u(251UL, (p_46 > (safe_lshift_func_int16_t_s_u(((-1L) == (((g_548.f0 >= (((l_591 , l_592) == g_594) & 0x829C244CL)) , 0x8EL) && l_561[0])), l_561[0]))))) , p_46))) > p_46) && (-1L)))
                { /* block id: 251 */
                    int32_t l_615 = 0xA3478515L;
                    int16_t l_626 = 0x7EC5L;
                    int16_t *l_627 = &g_508;
                    int8_t *l_628 = &g_143[7];
                    int32_t *l_630 = (void*)0;
                    uint8_t *l_632 = &g_82;
                    int64_t *l_635[6][5][8] = {{{(void*)0,&g_404[2],(void*)0,&g_404[2],(void*)0,&g_86,&g_404[2],&g_404[5]},{(void*)0,&g_86,&g_404[3],(void*)0,&g_404[5],&g_404[5],(void*)0,&g_404[2]},{(void*)0,&g_404[2],&g_86,&g_404[2],&g_404[0],&g_404[2],(void*)0,&g_404[2]},{&g_404[2],&g_404[3],&g_404[5],&g_404[2],&g_404[5],&g_404[3],&g_86,(void*)0},{(void*)0,(void*)0,&g_404[2],&g_86,(void*)0,&g_404[2],(void*)0,(void*)0}},{{&g_404[2],&g_404[2],&g_404[3],&g_404[3],&g_86,&g_404[2],&g_404[4],&g_404[2]},{&g_86,(void*)0,&g_86,&g_86,&g_404[2],&g_404[0],(void*)0,&g_404[2]},{(void*)0,&g_404[3],&g_404[2],&g_404[3],&g_404[3],&g_404[2],&g_404[3],(void*)0},{(void*)0,&g_404[4],&g_404[3],&g_404[2],&g_86,&g_404[3],(void*)0,&g_404[2]},{&g_404[2],&g_86,&g_404[2],&g_404[2],&g_404[2],&g_404[3],&g_404[2],(void*)0}},{{&g_86,&g_404[4],(void*)0,&g_404[2],&g_404[2],&g_404[2],&g_86,&g_86},{(void*)0,&g_404[3],&g_404[3],&g_404[3],&g_404[3],&g_404[0],(void*)0,&g_404[2]},{(void*)0,(void*)0,&g_86,(void*)0,&g_404[4],&g_404[2],&g_404[2],&g_404[2]},{(void*)0,&g_404[2],(void*)0,(void*)0,(void*)0,&g_404[2],&g_404[5],&g_86},{&g_404[2],(void*)0,&g_404[2],(void*)0,(void*)0,&g_404[3],&g_404[2],&g_86}},{{&g_404[2],&g_404[3],&g_86,&g_86,&g_404[2],&g_404[4],&g_86,&g_404[2]},{(void*)0,(void*)0,(void*)0,&g_404[4],&g_404[5],&g_404[4],&g_404[0],&g_404[3]},{&g_404[2],(void*)0,&g_404[3],&g_86,&g_404[2],(void*)0,&g_86,&g_86},{&g_404[2],&g_404[2],&g_404[3],&g_404[4],&g_86,&g_86,&g_86,&g_404[4]},{&g_86,(void*)0,&g_86,&g_404[3],&g_404[5],&g_404[3],(void*)0,&g_86}},{{(void*)0,&g_86,&g_86,&g_404[0],&g_86,&g_404[2],&g_404[5],(void*)0},{(void*)0,&g_404[2],&g_404[2],&g_86,&g_404[5],&g_404[2],(void*)0,&g_404[2]},{&g_86,&g_404[4],&g_404[2],&g_86,&g_86,&g_404[3],&g_404[2],&g_404[3]},{&g_404[2],&g_86,&g_404[2],&g_86,&g_404[2],(void*)0,(void*)0,&g_404[4]},{&g_404[2],(void*)0,(void*)0,&g_404[3],&g_404[5],&g_404[2],&g_404[2],&g_86}},{{(void*)0,&g_404[2],&g_86,(void*)0,&g_404[2],&g_86,(void*)0,&g_404[2]},{&g_404[2],&g_404[2],&g_404[0],&g_86,(void*)0,&g_404[5],(void*)0,&g_86},{&g_404[2],&g_404[2],&g_404[2],(void*)0,(void*)0,&g_404[2],&g_86,(void*)0},{&g_404[5],&g_404[5],&g_404[3],(void*)0,&g_404[2],&g_404[2],&g_404[0],&g_86},{&g_86,&g_404[0],(void*)0,&g_404[2],&g_86,&g_404[2],&g_86,&g_86}}};
                    int32_t *l_636 = &g_105;
                    int i, j, k;
                    (*l_636) &= (safe_rshift_func_uint16_t_u_s((safe_add_func_int64_t_s_s((g_86 |= (safe_mod_func_int64_t_s_s((l_603 != (((((void*)0 == &g_476) < (safe_div_func_int8_t_s_s(((*g_32) || (((safe_add_func_int16_t_s_s((safe_rshift_func_int8_t_s_s((l_561[0] <= g_404[2]), 1)), (safe_mul_func_uint16_t_u_u((p_46 == ((safe_rshift_func_uint16_t_u_u(l_615, 11)) , ((((*l_632) ^= (safe_sub_func_int32_t_s_s((l_631 &= (((((safe_div_func_int32_t_s_s(((((((*l_628) ^= (safe_div_func_int32_t_s_s((((*l_627) = (safe_rshift_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(0x669532B9A683BA06LL, l_626)), 6))) > (-8L)), l_591.f0))) , 0x4486FECDBF1ED672LL) , (*g_32)) < p_46) == 0x264B237EL), l_629[0])) && g_476) != (-10L)) == (-9L)) == p_46)), (-9L)))) != l_633[6]) ^ g_61.f3))), 65531UL)))) <= 0xA13C7BD9L) && 0xB6298ADEF48B33E8LL)), l_585))) | p_46) , l_634)), l_561[0]))), l_629[0])), g_116));
                }
                else
                { /* block id: 258 */
                    struct S1 l_643 = {0UL};
                    for (g_450 = 0; (g_450 <= 60); g_450++)
                    { /* block id: 261 */
                        return l_633[6];
                    }
                    for (g_508 = 27; (g_508 <= (-4)); g_508--)
                    { /* block id: 266 */
                        struct S1 *l_644[10] = {&g_482,&g_482,&g_482,&l_643,&l_643,&g_482,&g_482,&g_482,&l_643,&l_643};
                        int i;
                        (*g_642) = l_641;
                        (*g_481) = (l_643 = l_643);
                    }
                    l_645 = (l_641 = p_45);
                }
                (*g_595) = l_646;
            }
            if ((safe_rshift_func_uint16_t_u_s((safe_sub_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_u(((&g_595 != (l_656 = g_653)) < ((((safe_add_func_int32_t_s_s(0x929653E5L, 1L)) < (((safe_add_func_uint32_t_u_u(((((l_672[5][0] = (l_671[2][1] = (((safe_lshift_func_uint8_t_u_s(p_46, ((((void*)0 == &g_82) != (((((safe_mod_func_int32_t_s_s(l_585, (~l_575))) >= 4L) && l_585) != 0UL) | 0xE3L)) == g_668))) || 0UL) != 0x39E37C79L))) , &g_101[0][4][0]) != (void*)0) | p_46), 0x822AEF60L)) == 0x3C0FL) > p_46)) , p_46) || p_46)), 0)) , l_672[5][0]), p_46)), 7)))
            { /* block id: 279 */
                return l_585;
            }
            else
            { /* block id: 281 */
                uint64_t ***l_676 = &l_603;
                const float *l_686 = &g_687;
                const float **l_685[4][10][6] = {{{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,(void*)0,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,(void*)0,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,&l_686,&l_686}},{{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,(void*)0,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,(void*)0,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,&l_686,&l_686}},{{&l_686,&l_686,&l_686,&l_686,(void*)0,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,(void*)0,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,(void*)0,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686}},{{(void*)0,&l_686,(void*)0,&l_686,(void*)0,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,&l_686,&l_686},{&l_686,&l_686,&l_686,&l_686,(void*)0,&l_686},{(void*)0,&l_686,&l_686,&l_686,&l_686,&l_686},{(void*)0,&l_686,(void*)0,&l_686,(void*)0,&l_686},{&l_686,&l_686,(void*)0,&l_686,&l_686,&l_686}}};
                int32_t l_692 = 0x402635C9L;
                int32_t l_694 = 1L;
                int64_t *l_707 = &g_86;
                struct S1 ***l_742 = (void*)0;
                int32_t l_804 = (-1L);
                int32_t l_805[9];
                int8_t l_814[3];
                uint64_t l_941 = 0x349AF5DCC4A81EDDLL;
                int i, j, k;
                for (i = 0; i < 9; i++)
                    l_805[i] = (-8L);
                for (i = 0; i < 3; i++)
                    l_814[i] = 0x60L;
                if (p_46)
                    goto lbl_673;
                g_61.f5 |= ((safe_div_func_int64_t_s_s(((l_694 ^= ((((*l_676) = l_634) == (g_677 = g_677)) && (safe_mul_func_uint16_t_u_u((((p_46 & g_374) || (safe_rshift_func_int16_t_s_u((p_45 == (l_690 = (g_688 = (void*)0))), 6))) & (!((*l_604) = ((((l_692 , p_45) != l_693[0]) & g_342) ^ p_46)))), p_46)))) | p_46), g_105)) & 18446744073709551615UL);
                if ((0xECBDC93CL >= ((18446744073709551611UL > (safe_div_func_int64_t_s_s(((*l_707) &= ((safe_div_func_int64_t_s_s(0x18F73206E5F351C8LL, l_561[0])) >= (safe_mul_func_int8_t_s_s((g_69 < ((safe_lshift_func_uint16_t_u_s((safe_sub_func_int8_t_s_s(0xBDL, (safe_sub_func_uint16_t_u_u(((l_561[0] && (l_561[0] <= p_46)) ^ l_692), p_46)))), 5)) || l_575)), l_672[5][0])))), p_46))) > l_671[2][1])))
                { /* block id: 291 */
                    uint64_t l_732[6][7][6] = {{{18446744073709551607UL,18446744073709551609UL,0xC2D410080710A764LL,0UL,0x9F87B02ADE5526D5LL,0x5D0BAB7CC43E970ELL},{18446744073709551607UL,1UL,0x5FBCD25D628BC22ALL,1UL,18446744073709551607UL,0x4D57C5AC7214CF40LL},{0x5FBCD25D628BC22ALL,0UL,1UL,0UL,18446744073709551615UL,18446744073709551609UL},{0x1E07C308634B7260LL,3UL,0x2D8074EAD944B4A9LL,0UL,0xA0152223FCC6EFB3LL,18446744073709551609UL},{18446744073709551615UL,0xE897E959EDAA3E93LL,1UL,1UL,0x3B55505A3987FA2FLL,0x4D57C5AC7214CF40LL},{0xA0152223FCC6EFB3LL,0x2E5523CD1F20742FLL,0x5FBCD25D628BC22ALL,18446744073709551609UL,0x2D8074EAD944B4A9LL,0x5D0BAB7CC43E970ELL},{18446744073709551615UL,1UL,0xC2D410080710A764LL,0UL,1UL,0UL}},{{18446744073709551615UL,0xF996DCC54B39DAF7LL,1UL,0UL,18446744073709551607UL,0UL},{0x32AD9215C52079D9LL,1UL,0xAB7EA70F02F078B2LL,0x8C3E16117035B262LL,0x1E07C308634B7260LL,18446744073709551611UL},{0x32AD9215C52079D9LL,0xDCBE45D2E612538ALL,0xA0152223FCC6EFB3LL,0UL,0x6901E6307982EE56LL,1UL},{18446744073709551615UL,0x7831E5966CEAEAF3LL,18446744073709551607UL,0UL,18446744073709551613UL,8UL},{18446744073709551615UL,0x3B434577D3A99F6CLL,1UL,18446744073709551609UL,1UL,0x3B434577D3A99F6CLL},{0xA0152223FCC6EFB3LL,0x5D0BAB7CC43E970ELL,18446744073709551615UL,1UL,18446744073709551615UL,0xE61B97C671A42977LL},{18446744073709551615UL,1UL,18446744073709551613UL,0UL,0xCE9FBB6AE9AECE57LL,0x37339E7D68D24263LL}},{{0x1E07C308634B7260LL,1UL,0UL,0UL,18446744073709551615UL,0x6E85146F568DC2F3LL},{0x5FBCD25D628BC22ALL,0x5D0BAB7CC43E970ELL,0xAE7CFBC14AB60256LL,1UL,1UL,0xE897E959EDAA3E93LL},{18446744073709551607UL,0x3B434577D3A99F6CLL,0x6901E6307982EE56LL,0UL,18446744073709551613UL,18446744073709551609UL},{18446744073709551607UL,0x7831E5966CEAEAF3LL,0xD25EC07A24E36A30LL,0x5D0BAB7CC43E970ELL,18446744073709551613UL,0x37339E7D68D24263LL},{0x6901E6307982EE56LL,0x37339E7D68D24263LL,1UL,0x8C3E16117035B262LL,1UL,0x7831E5966CEAEAF3LL},{18446744073709551607UL,18446744073709551609UL,1UL,0x6E85146F568DC2F3LL,0xFB3A888D5A6C03A4LL,0x37339E7D68D24263LL},{0xAB7EA70F02F078B2LL,0x6E85146F568DC2F3LL,18446744073709551607UL,18446744073709551609UL,0xC2D410080710A764LL,0UL}},{{18446744073709551607UL,0UL,18446744073709551613UL,8UL,0x32AD9215C52079D9LL,8UL},{18446744073709551615UL,18446744073709551613UL,18446744073709551615UL,3UL,0xD25EC07A24E36A30LL,0xE897E959EDAA3E93LL},{0xD25EC07A24E36A30LL,8UL,1UL,0UL,0xCE9FBB6AE9AECE57LL,0x3B434577D3A99F6CLL},{0xC2D410080710A764LL,0x2E5523CD1F20742FLL,0x2D8074EAD944B4A9LL,0UL,18446744073709551615UL,3UL},{0xD25EC07A24E36A30LL,1UL,0x9F87B02ADE5526D5LL,3UL,0x0E0236AB3481E1AALL,1UL},{18446744073709551615UL,1UL,18446744073709551615UL,8UL,0xA0152223FCC6EFB3LL,0x8C3E16117035B262LL},{18446744073709551607UL,0xDCBE45D2E612538ALL,0x0E0236AB3481E1AALL,18446744073709551609UL,1UL,1UL}},{{0xAB7EA70F02F078B2LL,3UL,0xCE9FBB6AE9AECE57LL,0x6E85146F568DC2F3LL,18446744073709551615UL,0UL},{18446744073709551607UL,0UL,18446744073709551607UL,0x8C3E16117035B262LL,18446744073709551615UL,7UL},{0x6901E6307982EE56LL,3UL,0xC2D410080710A764LL,18446744073709551611UL,1UL,1UL},{0xFB3A888D5A6C03A4LL,0xDCBE45D2E612538ALL,1UL,7UL,0xA0152223FCC6EFB3LL,18446744073709551611UL},{0x0E0236AB3481E1AALL,1UL,0UL,1UL,0x0E0236AB3481E1AALL,0x660B9C12E648401ALL},{0UL,1UL,0xAB7EA70F02F078B2LL,0x7831E5966CEAEAF3LL,18446744073709551615UL,0xDCBE45D2E612538ALL},{1UL,0x2E5523CD1F20742FLL,0x32AD9215C52079D9LL,1UL,0xCE9FBB6AE9AECE57LL,0xDCBE45D2E612538ALL}},{{0xAC7F31FED5E89686LL,8UL,0xAB7EA70F02F078B2LL,18446744073709551609UL,0xD25EC07A24E36A30LL,0x660B9C12E648401ALL},{0xCE9FBB6AE9AECE57LL,18446744073709551613UL,0UL,0UL,0x32AD9215C52079D9LL,18446744073709551611UL},{0x1E07C308634B7260LL,0UL,1UL,0xF996DCC54B39DAF7LL,0xC2D410080710A764LL,1UL},{18446744073709551615UL,0x6E85146F568DC2F3LL,0xC2D410080710A764LL,0UL,0xFB3A888D5A6C03A4LL,7UL},{0x3B55505A3987FA2FLL,18446744073709551609UL,18446744073709551607UL,1UL,1UL,0UL},{0x3B55505A3987FA2FLL,0x37339E7D68D24263LL,0xCE9FBB6AE9AECE57LL,0UL,18446744073709551613UL,1UL},{18446744073709551615UL,0xF96B63913D6A8C85LL,0x0E0236AB3481E1AALL,0xF996DCC54B39DAF7LL,0x2D8074EAD944B4A9LL,0x8C3E16117035B262LL}}};
                    float *l_734 = &g_355[0];
                    struct S0 **l_736 = &l_735;
                    struct S1 ****l_743 = (void*)0;
                    struct S1 ****l_744[4];
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_744[i] = (void*)0;
                    (*l_734) = (safe_sub_func_float_f_f((!(safe_mul_func_float_f_f(((safe_add_func_float_f_f(p_46, (-0x1.3p+1))) < (safe_mul_func_float_f_f((safe_mul_func_float_f_f((l_561[3] = p_46), (!((((*l_707) ^= p_46) < ((safe_add_func_uint16_t_u_u((((1UL | (safe_sub_func_uint64_t_u_u(((safe_mul_func_float_f_f(((safe_mul_func_float_f_f((-0x9.Bp-1), ((g_143[2] , (safe_mul_func_float_f_f((safe_mul_func_float_f_f((p_46 <= p_46), 0x0.Dp-1)), l_672[5][0]))) > l_732[0][5][0]))) > p_46), l_694)) , 1UL), l_733))) | l_672[0][0]) || l_733), 0xDEE8L)) || 0x5A7CL)) , (-0x8.0p+1))))), (-0x6.Fp-1)))), 0xE.F0DC46p+1))), l_692));
                    (*g_737) = ((*l_736) = l_735);
                    (*g_120) = (safe_mul_func_uint16_t_u_u(p_46, ((safe_sub_func_uint8_t_u_u(((g_404[2] = p_46) == ((l_745 = l_742) == l_742)), (g_143[4] == 0xCD8EL))) | (safe_add_func_uint16_t_u_u(p_46, (l_561[0] == (l_561[0] , p_46)))))));
                }
                else
                { /* block id: 300 */
                    uint32_t *l_750 = &g_101[0][4][0];
                    int32_t l_753[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_753[i] = 0x218F41F2L;
                    if ((0UL <= (((safe_sub_func_int64_t_s_s(1L, 0L)) ^ (**g_124)) || ((*l_750)--))))
                    { /* block id: 302 */
                        if (p_46)
                            break;
                        if (p_46)
                            continue;
                        return l_753[4];
                    }
                    else
                    { /* block id: 306 */
                        int32_t *l_754[10][7] = {{&l_692,(void*)0,&l_692,&l_692,(void*)0,&l_692,&l_692},{&l_672[0][0],&l_672[0][0],&l_671[2][1],&l_672[0][0],&l_672[0][0],&l_671[2][1],&l_672[0][0]},{(void*)0,&l_692,&l_692,(void*)0,&l_692,&l_692,(void*)0},{&g_105,&l_672[0][0],&g_105,&g_105,&l_672[0][0],&g_105,&g_105},{(void*)0,(void*)0,&l_694,(void*)0,(void*)0,&l_694,(void*)0},{&l_672[0][0],&g_105,&g_105,&l_672[0][0],&g_105,&g_105,&l_672[0][0]},{&l_692,(void*)0,&l_694,&l_694,&l_692,&l_694,&l_694},{&g_105,&g_105,&l_672[0][0],&g_105,&g_105,&l_672[0][0],&g_105},{&l_692,&l_694,&l_694,&l_692,&l_694,&l_694,&l_692},{&l_671[2][1],&g_105,&l_671[2][1],&l_671[2][1],&g_105,&l_671[2][1],&l_671[2][1]}};
                        int i, j;
                        if (l_753[2])
                            break;
                        ++l_755;
                    }
                    (**g_594) = (**g_594);
                }
                for (g_476 = 0; (g_476 <= 11); g_476 = safe_add_func_uint8_t_u_u(g_476, 3))
                { /* block id: 314 */
                    uint32_t l_776 = 0xA9E864B8L;
                    int32_t *l_777 = &l_672[5][0];
                    int32_t l_796 = 1L;
                    int32_t l_800 = (-1L);
                    int32_t l_801 = 2L;
                    int32_t l_802 = (-3L);
                    int32_t l_803 = 0x296FBB97L;
                    int32_t l_806 = (-3L);
                    int32_t l_807 = 0L;
                    int32_t l_808 = (-8L);
                    int32_t l_809 = 0xE24F95CDL;
                    int32_t l_810 = 0x0312D494L;
                    int32_t l_812[5];
                    float l_815 = 0x0.Dp-1;
                    struct S0 **l_835 = &l_735;
                    const float **l_862 = &g_688;
                    const float ***l_863 = &l_862;
                    int32_t *l_869 = &l_561[0];
                    uint16_t *l_872 = &g_69;
                    uint16_t *l_875 = &l_816;
                    struct S0 ***l_877 = &l_835;
                    const int32_t l_882 = 0x75E844CBL;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_812[i] = 0x55E9D503L;
                    (*l_777) ^= (safe_lshift_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(((safe_add_func_uint32_t_u_u(((l_767 = l_766) , 2UL), (4L || (safe_mul_func_int8_t_s_s(g_371, (0x19D8L <= ((safe_mod_func_uint64_t_u_u((safe_sub_func_uint32_t_u_u(((safe_add_func_float_f_f(((*g_60) , p_46), l_776)) , (g_133.f0 , p_46)), 0xD14F4DE1L)), l_633[8])) > 65535UL))))))) ^ p_46), l_692)), g_82));
                    for (l_766 = 0; (l_766 <= (-29)); l_766 = safe_sub_func_int32_t_s_s(l_766, 6))
                    { /* block id: 319 */
                        int32_t *l_780 = &l_633[8];
                        int32_t *l_781 = &l_694;
                        int32_t l_782[8] = {0xE729E06FL,0x2E017277L,0xE729E06FL,0x2E017277L,0xE729E06FL,0x2E017277L,0xE729E06FL,0x2E017277L};
                        int32_t *l_783 = &l_692;
                        int32_t *l_784 = &l_671[2][1];
                        int32_t *l_785 = &l_561[0];
                        int32_t *l_786 = &l_561[1];
                        int32_t *l_787 = &l_633[6];
                        int32_t *l_788 = &l_782[6];
                        int32_t *l_789 = &l_672[5][0];
                        int32_t *l_790 = &l_633[6];
                        int32_t *l_791 = &l_782[2];
                        int32_t *l_792 = &l_692;
                        int32_t *l_793 = &l_733;
                        int32_t *l_794 = &l_782[2];
                        int32_t *l_795 = (void*)0;
                        int32_t *l_797 = (void*)0;
                        int32_t *l_798 = (void*)0;
                        int32_t *l_799[4];
                        int32_t l_813[8] = {0x74883985L,1L,0x74883985L,0x74883985L,1L,0x74883985L,0x74883985L,1L};
                        int32_t **l_819[9][3][4] = {{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}},{{&l_785,&l_794,&l_794,&l_785},{&l_794,&l_785,&l_794,&l_794},{&l_785,&l_785,&l_788,&l_785}}};
                        uint16_t *l_830 = (void*)0;
                        uint16_t *l_831 = &l_816;
                        int32_t l_834 = (-1L);
                        int16_t *l_838 = &g_103;
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                            l_799[i] = &l_671[2][3];
                        l_816--;
                        (*g_560) = (void*)0;
                        (*l_789) = ((*l_791) = (safe_lshift_func_uint16_t_u_s((safe_div_func_uint64_t_u_u(((*l_604) = (((l_671[2][1] = (safe_lshift_func_uint16_t_u_s(l_671[2][1], ((((safe_mod_func_int16_t_s_s(p_46, (safe_lshift_func_uint8_t_u_u((((*l_783) &= (0xA194L ^ (--(*l_831)))) | l_834), 4)))) | p_46) , l_835) == (void*)0)))) >= (((*l_838) = ((safe_rshift_func_int16_t_s_u(g_476, ((void*)0 != &g_60))) == l_814[1])) ^ 1L)) && p_46)), g_574.f0)), (*l_777))));
                        (*l_790) = (&p_45 != ((safe_add_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s(p_46, 11)), ((*l_777) , ((g_61.f6 , (((*l_777) > (safe_sub_func_float_f_f((!(((safe_add_func_float_f_f(p_46, l_575)) , (safe_div_func_float_f_f((&g_481 != ((safe_rshift_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((*l_838) = p_46), g_342)), p_46)) , (void*)0)), p_46))) < p_46)), (*l_777)))) , g_103)) && g_158.f3)))) , (void*)0));
                    }
                    if ((0xB456L == (+((((+(safe_mod_func_uint16_t_u_u((*l_777), (safe_mod_func_int16_t_s_s((0x3B036F9EL & (safe_add_func_int32_t_s_s((((*l_863) = l_862) != ((((!l_672[5][0]) , (safe_div_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_u((((((*l_869) = (-1L)) <= (safe_mul_func_uint8_t_u_u((&g_60 != (((*l_875) &= ((*l_872)++)) , ((*l_877) = l_876))), (((safe_rshift_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(g_220[3], p_46)), l_766)) || 0xE1F0EE51L) || (-1L))))) & l_804) , p_46), 2)) <= (*l_777)), p_46))) != l_882) , (void*)0)), p_46))), g_103))))) , 1L) || 0xB548815D69C9511ELL) <= g_86))))
                    { /* block id: 337 */
                        return p_46;
                    }
                    else
                    { /* block id: 339 */
                        int8_t *l_887 = (void*)0;
                        int8_t *l_888 = (void*)0;
                        int8_t *l_889 = &g_143[7];
                        float *l_917 = &l_815;
                        int32_t l_920 = 0xBC84F54DL;
                        uint32_t *l_921 = &g_101[0][4][0];
                        int32_t **l_922 = &l_777;
                        (*l_869) ^= ((safe_lshift_func_int16_t_s_u(0L, 5)) != ((*l_777) && (((l_672[5][0] &= (((*l_889) |= g_110.f0) && (g_890 != &g_891))) , (((((3L || ((safe_rshift_func_int16_t_s_s((l_591.f0 <= (*g_891)), (safe_add_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((void*)0 == &l_811), 0L)), 0x45L)))) > p_46)) >= p_46) >= p_46) <= 0x4EE4BE35L) > l_575)) == (*g_891))));
                        l_809 |= (safe_rshift_func_int16_t_s_u(((((*l_921) |= (((safe_mod_func_int64_t_s_s((((l_733 & l_672[5][0]) , l_814[2]) , (safe_mod_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u((g_82 &= (safe_rshift_func_uint8_t_u_s((((safe_mul_func_float_f_f((safe_div_func_float_f_f(((*g_8) = 0x1.5p+1), ((safe_mul_func_float_f_f(((safe_mul_func_float_f_f((l_916 < ((*l_917) = p_46)), (((safe_mod_func_int64_t_s_s((-1L), (((*l_872) = (l_585 , (((g_110.f0 | 1L) <= (-1L)) , 8UL))) ^ l_805[3]))) , l_561[0]) > p_46))) != l_920), (*l_777))) >= (-0x1.Ap-1)))), l_920)) , 0x0435L) && p_46), l_585))), p_46)) ^ (*g_891)), p_46))), 18446744073709551614UL)) && (-1L)) == 0xC9AF5983L)) , p_46) > 0x86738CC9L), 15));
                        (*l_922) = &l_803;
                        if (l_766)
                            continue;
                    }
                    for (l_692 = 27; (l_692 == (-2)); --l_692)
                    { /* block id: 354 */
                        int64_t ***l_932 = &l_931;
                        (*l_777) = (safe_lshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_mod_func_int8_t_s_s((0xDB10L < ((((*l_932) = l_931) == (void*)0) , 0UL)), (safe_sub_func_uint16_t_u_u(65535UL, (l_805[1] ^= ((safe_lshift_func_uint8_t_u_s(0x14L, p_46)) <= ((safe_mul_func_uint16_t_u_u((l_941 && ((((safe_mod_func_int16_t_s_s((*l_777), g_82)) | g_82) | g_116) , l_671[2][1])), l_585)) < p_46))))))), 4294967291UL)), 15));
                    }
                }
            }
            g_61.f5 |= ((safe_add_func_int16_t_s_s((l_672[6][0] &= ((*g_298) , (safe_lshift_func_int16_t_s_s((((*l_949) = l_948[3]) != (g_953 = l_950)), 12)))), ((safe_sub_func_int64_t_s_s(((g_82 = (l_585 >= (safe_mul_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u(6UL, 9)) == (l_811 > (safe_div_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s(l_591.f0, l_755)), ((safe_add_func_float_f_f(((((l_585 | p_46) , p_45) == p_45) != 0xD.8CBB74p+62), p_46)) , p_46))))), 0x4CL)))) == 0xDCL), l_755)) , 0x0DFCL))) < l_591.f0);
        }
        (*g_120) |= (safe_unary_minus_func_int16_t_s(0xA1A3L));
    }
    l_561[0] ^= (l_766 |= (g_968[0] , (~(g_970 , l_971))));
    return p_46;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_33 g_103 g_119.f0 g_82 g_61.f2 g_158 g_86 g_110 g_2 g_101 g_176 g_8 g_9 g_177 g_119 g_69 g_121 g_61.f3 g_116 g_5 g_143 g_61.f5 g_209 g_240 g_120 g_133.f3 g_61.f4 g_61.f7 g_296 g_297 g_220 g_241 g_309 g_61.f1 g_61.f0 g_310 g_105 g_360 g_374 g_371 g_60 g_61 g_124 g_158.f3 g_450 g_454 g_404 g_465 g_476 g_298 g_479 g_481 g_484 g_133 g_560
 * writes: g_82 g_119.f0 g_86 g_101 g_61.f4 g_177 g_9 g_119 g_33 g_69 g_143 g_298 g_116 g_310 g_240 g_121 g_374 g_450 g_371 g_105 g_482 g_476 g_355
 */
static uint64_t  func_50(uint32_t  p_51)
{ /* block id: 53 */
    uint32_t l_141[4][5][10] = {{{5UL,0xC9291112L,0xA19B6E0FL,0xB43B7A3EL,18446744073709551606UL,1UL,0x916D73FBL,1UL,1UL,0xCA947FBAL},{0x6933D78BL,1UL,18446744073709551614UL,0xAF38EC91L,0x7E4AC2E1L,0x4D4209EDL,0x8BC82935L,0xFDE8E729L,0xFDE8E729L,0x8BC82935L},{18446744073709551615UL,18446744073709551615UL,0xE5926A32L,0xE5926A32L,18446744073709551615UL,18446744073709551615UL,0xC9291112L,0UL,1UL,0x122574F0L},{0xE5926A32L,18446744073709551615UL,0xE2EA94D1L,2UL,5UL,18446744073709551610UL,18446744073709551613UL,0xA19B6E0FL,0x0B1AE647L,0x83FE5607L},{0xE5926A32L,0x122574F0L,0xC3FE914DL,0xCA947FBAL,18446744073709551612UL,0x83FE5607L,0x8BC82935L,18446744073709551614UL,18446744073709551608UL,1UL}},{{0x83FE5607L,0x8BC82935L,18446744073709551614UL,18446744073709551608UL,1UL,1UL,1UL,0x7E4AC2E1L,0xE2EA94D1L,0xFDE8E729L},{5UL,0xCA947FBAL,2UL,0xB43B7A3EL,0xC9291112L,0xFDE8E729L,18446744073709551612UL,0xFDE8E729L,0xC9291112L,0xB43B7A3EL},{0x4D4209EDL,0xE2EA94D1L,0x4D4209EDL,0x6933D78BL,0xDCC5544FL,1UL,18446744073709551614UL,18446744073709551610UL,0x916D73FBL,0UL},{18446744073709551614UL,0xF43B5DB3L,0xC9291112L,0x0ADD41A2L,0xE917A824L,0x0B1AE647L,0xAF38EC91L,18446744073709551610UL,0x91E88D44L,1UL},{18446744073709551608UL,1UL,0x4D4209EDL,0x4045D3C7L,0x916D73FBL,0xDBD4BE37L,0x0ADD41A2L,0xFDE8E729L,0x122574F0L,1UL}},{{0x8BC82935L,0xE917A824L,2UL,0x0B1AE647L,0x3D11B6B2L,0x9E426141L,1UL,0x7E4AC2E1L,0xB43B7A3EL,0x4D4209EDL},{18446744073709551615UL,0x3D11B6B2L,18446744073709551614UL,0x9E426141L,0xCA947FBAL,0xCA947FBAL,0x9E426141L,18446744073709551614UL,0x3D11B6B2L,18446744073709551615UL},{1UL,0xFDE8E729L,0xC3FE914DL,18446744073709551613UL,1UL,0xA19B6E0FL,18446744073709551615UL,0x916D73FBL,0x83FE5607L,18446744073709551610UL},{0xE2EA94D1L,0x91E88D44L,0xE917A824L,0xFDE8E729L,1UL,0xE5926A32L,18446744073709551608UL,18446744073709551606UL,0x0ADD41A2L,18446744073709551615UL},{1UL,18446744073709551615UL,0x91E88D44L,18446744073709551615UL,0xCA947FBAL,0x916D73FBL,0xF43B5DB3L,0x9E426141L,2UL,0x4D4209EDL}},{{18446744073709551606UL,0UL,0x6933D78BL,1UL,0x3D11B6B2L,0x4045D3C7L,0xCA947FBAL,1UL,0xAF38EC91L,1UL},{2UL,18446744073709551613UL,0x916D73FBL,0x4D4209EDL,0x916D73FBL,18446744073709551613UL,2UL,0UL,0xE917A824L,1UL},{18446744073709551612UL,0xC3FE914DL,0UL,0xAF38EC91L,0xE917A824L,18446744073709551615UL,0x7E4AC2E1L,0x4045D3C7L,0x0B1AE647L,0UL},{0x3D11B6B2L,0xC3FE914DL,1UL,18446744073709551615UL,0xDCC5544FL,18446744073709551606UL,2UL,1UL,0xCA947FBAL,0xB43B7A3EL},{1UL,18446744073709551613UL,18446744073709551606UL,18446744073709551615UL,0xC9291112L,0x3D11B6B2L,0xCA947FBAL,0xDCC5544FL,0x4D4209EDL,0xFDE8E729L}}};
    int8_t *l_142[4];
    int32_t l_144 = 0L;
    uint8_t *l_151 = &g_82;
    uint32_t *l_152[8][10] = {{(void*)0,&l_141[2][2][2],&g_119.f0,&l_141[3][4][3],(void*)0,(void*)0,(void*)0,(void*)0,&l_141[3][4][3],&g_119.f0},{(void*)0,&l_141[2][4][8],&g_119.f0,&l_141[3][4][3],&l_141[3][4][3],(void*)0,&l_141[3][4][3],&g_119.f0,(void*)0,&g_119.f0},{(void*)0,(void*)0,&l_141[3][4][3],&l_141[3][4][3],&l_141[3][4][3],(void*)0,(void*)0,&g_119.f0,&l_141[3][4][3],&l_141[3][4][3]},{(void*)0,(void*)0,(void*)0,&l_141[3][4][3],&g_119.f0,&g_119.f0,&l_141[3][4][3],&l_141[0][1][1],(void*)0,(void*)0},{&l_141[3][4][3],(void*)0,&g_119.f0,&l_141[3][3][4],&g_119.f0,&l_141[3][4][3],(void*)0,&g_119.f0,(void*)0,&g_119.f0},{&g_119.f0,&g_119.f0,&g_119.f0,&l_141[3][4][3],&g_119.f0,&l_141[3][4][3],&l_141[3][4][3],&g_119.f0,&l_141[3][4][3],&g_119.f0},{&l_141[3][4][3],&l_141[3][4][3],&g_119.f0,&g_119.f0,(void*)0,&g_119.f0,&l_141[3][4][3],&g_119.f0,&l_141[3][4][3],&g_119.f0},{&g_119.f0,&g_119.f0,&g_119.f0,&l_141[3][2][1],&g_119.f0,&g_119.f0,&l_141[3][4][3],&l_141[0][1][1],&g_119.f0,&g_119.f0}};
    struct S1 *l_157 = &g_119;
    uint16_t *l_183 = &g_69;
    uint16_t **l_182 = &l_183;
    int32_t l_231 = 0xBA90784FL;
    int8_t l_232[3][2];
    int32_t l_235 = 0x03F65F44L;
    float ****l_243 = &g_241;
    uint32_t l_258[8][3] = {{0xD957DF1AL,0x4570CD12L,0x8F600EE0L},{0xD957DF1AL,0UL,0x7E2E718DL},{0xD957DF1AL,0UL,0xD957DF1AL},{0xD957DF1AL,0x4570CD12L,0x8F600EE0L},{0xD957DF1AL,0UL,0x7E2E718DL},{0xD957DF1AL,0UL,0xD957DF1AL},{0xD957DF1AL,0x4570CD12L,0x8F600EE0L},{0xD957DF1AL,0UL,0x7E2E718DL}};
    uint64_t *l_287 = &g_116;
    int32_t l_366 = 0xC5A7B4FAL;
    int32_t l_367 = 0xF936D604L;
    int32_t l_368 = 0x2291522DL;
    int32_t l_369 = 0x376D4718L;
    int32_t l_370 = 0x986A9B86L;
    int32_t l_373 = 0x9C2D89C0L;
    int64_t l_379 = (-8L);
    int32_t l_387 = 0L;
    int32_t l_388 = 8L;
    int32_t l_389 = 5L;
    int32_t l_390 = 1L;
    int32_t l_391 = 0xDC7F7299L;
    int32_t l_393 = 0xA867A1B1L;
    int32_t l_396 = 0x41EFFA9CL;
    int32_t l_397 = 0xA893D41EL;
    int32_t l_400 = (-3L);
    int32_t l_401 = 0xA29370D8L;
    int32_t l_402 = 0x071E8C18L;
    int32_t l_403[9][4][1] = {{{0x3A35BFECL},{7L},{7L},{0x3A35BFECL}},{{7L},{7L},{0x3A35BFECL},{7L}},{{7L},{0x3A35BFECL},{7L},{7L}},{{0x3A35BFECL},{7L},{7L},{0x3A35BFECL}},{{7L},{7L},{0x3A35BFECL},{7L}},{{7L},{0x3A35BFECL},{7L},{7L}},{{0x3A35BFECL},{7L},{7L},{0x3A35BFECL}},{{7L},{7L},{0x3A35BFECL},{7L}},{{7L},{0x3A35BFECL},{7L},{7L}}};
    float l_407 = 0x9.0p+1;
    int64_t l_455 = (-1L);
    int64_t *l_553 = &g_404[2];
    int64_t **l_552[10] = {&l_553,&l_553,&l_553,&l_553,&l_553,&l_553,&l_553,&l_553,&l_553,&l_553};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_142[i] = &g_143[7];
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
            l_232[i][j] = 0x73L;
    }
    if ((((g_119.f0 &= ((safe_rshift_func_int8_t_s_s(0L, ((!(*g_32)) < (-6L)))) ^ (((l_141[3][4][3] < (&g_120 != (void*)0)) != (l_144 &= p_51)) == (0UL < (safe_lshift_func_uint8_t_u_s((safe_div_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s(((*l_151) = l_141[3][1][3]), g_103)), 0x2ECAC6B4L)), 0)))))) , g_82) > g_61.f2))
    { /* block id: 57 */
        struct S1 *l_159 = &g_119;
        int64_t *l_164 = &g_86;
        uint32_t *l_165[8] = {&g_101[0][4][0],&g_101[0][4][0],&g_101[0][4][0],&g_101[0][4][0],&g_101[0][4][0],&g_101[0][4][0],&g_101[0][4][0],&g_101[0][4][0]};
        uint8_t l_168 = 0x2AL;
        int32_t *l_171 = &l_144;
        float *l_173[9];
        float ** const l_172 = &l_173[1];
        float **l_175 = &l_173[1];
        float ***l_174[8];
        int i;
        for (i = 0; i < 9; i++)
            l_173[i] = &g_9[2][1][1];
        for (i = 0; i < 8; i++)
            l_174[i] = &l_175;
        (*l_171) &= (safe_mul_func_uint16_t_u_u((((safe_sub_func_int16_t_s_s(((l_157 != (g_158 , l_159)) != ((g_61.f4 = (safe_add_func_int32_t_s_s((safe_sub_func_int64_t_s_s(((*l_164) &= p_51), ((g_110 , p_51) ^ g_2))), ((--g_101[0][4][0]) == (l_168 & (safe_rshift_func_uint8_t_u_s(((g_61.f2 , 0UL) < 0UL), 4))))))) != 0x963C60D9L)), 0x4432L)) || 0x5588B0772C01C9B9LL) > 0x5482L), 9UL));
        (*g_176) = l_172;
        (**g_177) = (*g_8);
        for (l_168 = (-10); (l_168 > 3); l_168++)
        { /* block id: 66 */
            if (l_141[3][4][3])
                break;
            (*l_159) = (*l_159);
        }
    }
    else
    { /* block id: 70 */
        uint16_t *l_181 = &g_69;
        uint16_t **l_180 = &l_181;
        int32_t l_208 = 0x1FE8F313L;
        int32_t l_214 = 1L;
        int32_t l_216 = 4L;
        int32_t l_218 = 7L;
        int32_t l_219 = 4L;
        int32_t l_221 = 0x0642ACEBL;
        int32_t l_222 = 8L;
        int32_t l_223 = 0xAC126E95L;
        int32_t l_224 = 9L;
        int32_t l_227 = 0xE361AB7BL;
        int64_t l_228[3][7] = {{0xCEB63D19799441F9LL,0L,0xCEB63D19799441F9LL,0xCEB63D19799441F9LL,0L,0xCEB63D19799441F9LL,0xCEB63D19799441F9LL},{(-1L),(-1L),0x74A22506B0F82286LL,(-1L),(-1L),0x74A22506B0F82286LL,(-1L)},{0L,0xCEB63D19799441F9LL,0xCEB63D19799441F9LL,0L,0xCEB63D19799441F9LL,0xCEB63D19799441F9LL,0L}};
        int32_t l_230 = (-1L);
        int32_t l_233[3][1];
        uint32_t *l_248 = &l_141[3][4][3];
        int64_t l_365 = 0L;
        int64_t l_372 = 0x15195B6B8E5B3D8CLL;
        int8_t l_377 = 0xB5L;
        int32_t l_386 = 0x90B1CABDL;
        int32_t l_398 = 0x011CB06BL;
        float *l_422 = &g_355[0];
        float **l_421 = &l_422;
        struct S1 l_480 = {18446744073709551615UL};
        uint16_t l_513 = 0xC0E3L;
        uint16_t l_556 = 65533UL;
        int i, j;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_233[i][j] = 0xD80B03E0L;
        }
lbl_299:
        l_182 = l_180;
        for (g_82 = 0; (g_82 <= 3); g_82 += 1)
        { /* block id: 74 */
            int32_t *l_184 = &g_5;
            float ***l_196[7] = {&g_177,(void*)0,&g_177,&g_177,(void*)0,&g_177,&g_177};
            int32_t l_213[1][8][7] = {{{1L,0x41F32220L,2L,2L,0x41F32220L,1L,0x41F32220L},{1L,1L,1L,1L,0x41F32220L,1L,1L},{(-2L),(-2L),1L,2L,1L,(-2L),(-2L)},{(-2L),1L,2L,1L,(-2L),(-2L),1L},{1L,0x41F32220L,1L,1L,1L,1L,0x41F32220L},{1L,(-2L),0x41F32220L,0x41F32220L,(-2L),1L,(-2L)},{2L,1L,1L,2L,(-2L),2L,1L},{1L,1L,1L,0x41F32220L,1L,1L,1L}}};
            float l_225[7][7] = {{0x0.9p-1,0x1.9p-1,0x0.Ap+1,0x1.9p-1,0x0.9p-1,0x0.72B01Bp-57,(-0x2.4p-1)},{0x3.6B931Dp-14,0x0.6p-1,0x7.EC9DE8p-70,(-0x1.3p+1),0x0.5p+1,(-0x1.3p+1),0x7.EC9DE8p-70},{(-0x2.4p-1),(-0x2.4p-1),0x6.6p-1,0x0.Ap+1,0xF.C1ADAEp+72,(-0x5.3p+1),0x0.4p+1},{0x3.6B931Dp-14,(-0x1.3p+1),0x5.8p-1,0x5.8p-1,(-0x1.3p+1),0x3.6B931Dp-14,0x0.5p+1},{0x0.9p-1,0x6.6p-1,0x1.9p-1,0xB.2D16ADp+34,0xF.C1ADAEp+72,0xF.C1ADAEp+72,0xB.2D16ADp+34},{0x0.6p+1,0xA.9385EAp+25,0x0.6p+1,(-0x10.1p+1),0x0.5p+1,0xB.A7F952p-47,0x3.6B931Dp-14},{0x1.9p-1,0x6.6p-1,0x0.9p-1,(-0x5.3p+1),0x0.9p-1,0x6.6p-1,0x1.9p-1}};
            struct S1 l_247 = {0UL};
            uint64_t *l_288 = (void*)0;
            struct S1 ** const l_314 = &l_157;
            int64_t l_382 = 0x236AC3FF344C95DALL;
            int8_t l_394 = (-1L);
            int64_t l_405[5];
            int8_t l_485 = 2L;
            uint32_t l_491[6][5][6] = {{{18446744073709551615UL,0x4FC5F617L,18446744073709551611UL,0UL,18446744073709551615UL,4UL},{0x045CFCCBL,3UL,0x4DB84B80L,18446744073709551606UL,5UL,0x1E019507L},{0x2113767CL,7UL,1UL,8UL,0xD53A441AL,0x065501E3L},{4UL,4UL,0x36779BF6L,18446744073709551615UL,18446744073709551613UL,18446744073709551613UL},{0xB8C5C897L,0x065501E3L,18446744073709551606UL,0x5977E5E2L,0UL,0x21905A08L}},{{0x4DB84B80L,0UL,0xDD6A7AEFL,4UL,0x6619711EL,0x2113767CL},{3UL,18446744073709551611UL,0x56A99088L,0xBD1046B4L,0xBD1046B4L,0x56A99088L},{0x28D04695L,0x28D04695L,18446744073709551609UL,18446744073709551613UL,18446744073709551611UL,4UL},{1UL,18446744073709551615UL,0x045CFCCBL,0xF672321CL,5UL,18446744073709551609UL},{18446744073709551614UL,1UL,0x045CFCCBL,0x21905A08L,0x28D04695L,4UL}},{{18446744073709551615UL,0x21905A08L,18446744073709551609UL,18446744073709551615UL,0xA7024455L,0x56A99088L},{18446744073709551615UL,0xA7024455L,0x56A99088L,0x8D0AFFC3L,0xEC365E38L,0x2113767CL},{0x045CFCCBL,7UL,0xDD6A7AEFL,0xB1E63954L,0x06F18D07L,0x21905A08L},{0x06F18D07L,7UL,18446744073709551606UL,0UL,1UL,18446744073709551613UL},{18446744073709551615UL,0x28D04695L,0x36779BF6L,4UL,0UL,0x065501E3L}},{{3UL,0xD53A441AL,1UL,0x5977E5E2L,0UL,0x1E019507L},{18446744073709551614UL,0x9D59BB6FL,0x4DB84B80L,9UL,0x6619711EL,4UL},{0xB1E63954L,2UL,18446744073709551611UL,18446744073709551614UL,18446744073709551613UL,0x56A99088L},{4UL,0xEC365E38L,8UL,18446744073709551615UL,0x065501E3L,9UL},{1UL,18446744073709551615UL,0x06F18D07L,4UL,0x06F18D07L,18446744073709551615UL}},{{0xDD6A7AEFL,1UL,18446744073709551611UL,18446744073709551615UL,18446744073709551613UL,18446744073709551615UL},{0UL,18446744073709551608UL,0x28D04695L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xEC365E38L,18446744073709551608UL,18446744073709551613UL,1UL,18446744073709551613UL,0x730281EAL},{3UL,18446744073709551607UL,0xA7024455L,0x9D59BB6FL,9UL,8UL},{0x56A99088L,1UL,0xB204A71EL,18446744073709551611UL,0x4DD830B2L,5UL}},{{1UL,0xF672321CL,0x393ED4A9L,0x36779BF6L,0x5977E5E2L,0x045CFCCBL},{18446744073709551613UL,0x045CFCCBL,1UL,8UL,18446744073709551611UL,7UL},{0xAC9CD9E6L,0UL,0x1E019507L,3UL,0xD53A441AL,18446744073709551611UL},{1UL,0x951DEAE0L,0xDD6A7AEFL,0x5977E5E2L,0x21905A08L,18446744073709551615UL},{4UL,0x22B03306L,0xEC365E38L,0x1E019507L,0x06F18D07L,3UL}}};
            union U2 *l_547 = &g_548;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_405[i] = (-2L);
            for (g_33 = 0; (g_33 <= 0); g_33 += 1)
            { /* block id: 77 */
                int32_t *l_186 = &l_144;
                int32_t l_226 = 0x54124C9FL;
                int32_t l_229[6][7] = {{0x74E1F021L,0xE32F4612L,0xE32F4612L,0x74E1F021L,(-9L),1L,0x010A9E94L},{0x010A9E94L,1L,(-9L),0x74E1F021L,0xE32F4612L,0xE32F4612L,0x74E1F021L},{0x2663875DL,5L,0x2663875DL,1L,0x06F0957DL,(-2L),0x010A9E94L},{5L,(-1L),0x2663875DL,(-9L),1L,(-9L),0x2663875DL},{0x06F0957DL,0x06F0957DL,(-9L),0xDB75E829L,0xB53B084DL,(-2L),5L},{0xDB75E829L,0x06F0957DL,0xE32F4612L,(-2L),(-2L),0xE32F4612L,0x06F0957DL}};
                float l_234 = (-0x3.Fp+1);
                int64_t l_263 = 0L;
                struct S1 **l_315 = &l_157;
                struct S1 ***l_316 = &l_315;
                uint16_t l_321 = 0UL;
                float l_399[3][3][9] = {{{0x2.471F33p-28,(-0x7.3p+1),0xD.35A801p+90,0xC.A0F561p+19,(-0x4.Fp+1),0x9.Bp+1,0x2.59429Bp-13,0x2.59429Bp-13,0x9.Bp+1},{0xC.A0F561p+19,0x8.ECDC71p-97,0xD.FC4C48p+30,0x8.ECDC71p-97,0xC.A0F561p+19,(-0x7.3p+1),0x2.Ap-1,0xD.35A801p+90,0x2.632B20p-10},{0x2.Ap-1,(-0x7.3p+1),0xC.A0F561p+19,0x8.ECDC71p-97,0xD.FC4C48p+30,0x8.ECDC71p-97,0xC.A0F561p+19,(-0x7.3p+1),0x2.Ap-1}},{{0x2.59429Bp-13,0x9.Bp+1,(-0x4.Fp+1),0xC.A0F561p+19,0xD.35A801p+90,(-0x7.3p+1),0x2.471F33p-28,(-0x7.3p+1),0xD.35A801p+90},{0x2.632B20p-10,0x2.471F33p-28,0x2.471F33p-28,0x2.632B20p-10,0x8.ECDC71p-97,0x9.Bp+1,0xA.46A385p-55,0xD.35A801p+90,(-0x4.Fp+1)},{0x2.59429Bp-13,0xD.35A801p+90,0x2.471F33p-28,0x1.1p-1,0x1.1p-1,0x2.471F33p-28,0xD.35A801p+90,0x2.59429Bp-13,0xD.FC4C48p+30}},{{0x2.Ap-1,0x1.1p-1,(-0x4.Fp+1),0x2.59429Bp-13,0x8.ECDC71p-97,0xD.35A801p+90,0xD.35A801p+90,0x8.ECDC71p-97,0x2.59429Bp-13},{0xC.A0F561p+19,0x2.Ap-1,0xC.A0F561p+19,0x9.Bp+1,0xD.35A801p+90,0x1.1p-1,0xA.46A385p-55,0xD.FC4C48p+30,0xD.FC4C48p+30},{0x2.471F33p-28,0x2.Ap-1,0xD.FC4C48p+30,0xD.35A801p+90,0xD.FC4C48p+30,0x2.Ap-1,0x2.471F33p-28,0xA.46A385p-55,(-0x4.Fp+1)}}};
                uint8_t l_410 = 255UL;
                int i, j, k;
                for (g_69 = 0; (g_69 <= 3); g_69 += 1)
                { /* block id: 80 */
                    int i, j, k;
                    l_144 = g_101[g_33][(g_69 + 2)][g_33];
                }
                if ((g_121[(g_33 + 2)] & p_51))
                { /* block id: 83 */
                    int32_t **l_185[8][4] = {{&l_184,(void*)0,&l_184,&l_184},{&l_184,&l_184,&l_184,&l_184},{(void*)0,&l_184,&l_184,&l_184},{&l_184,&l_184,&l_184,&l_184},{&l_184,&l_184,&l_184,&l_184},{&l_184,&l_184,&l_184,&l_184},{&l_184,&l_184,&l_184,&l_184},{&l_184,&l_184,&l_184,&l_184}};
                    float ****l_197 = &l_196[5];
                    float ***l_199 = &g_177;
                    float ****l_198 = &l_199;
                    int64_t *l_210 = (void*)0;
                    int64_t *l_211 = (void*)0;
                    int64_t *l_212 = &g_86;
                    int64_t l_257 = (-8L);
                    int i, j;
                    l_186 = l_184;
                    if (((~((*l_212) &= ((p_51 && (safe_add_func_int8_t_s_s(g_61.f3, (safe_add_func_uint16_t_u_u(((0xAD200183L != (((safe_sub_func_uint32_t_u_u((((*l_197) = l_196[5]) == ((*l_198) = &g_177)), ((safe_div_func_int64_t_s_s(((safe_div_func_uint32_t_u_u(g_116, p_51)) == (((safe_rshift_func_int8_t_s_u((g_143[3] |= (safe_sub_func_uint64_t_u_u((0L ^ (((*l_184) <= 0x4DL) & 0x92C1L)), 0x40130CC3C0569548LL))), (*l_184))) & (*l_184)) <= p_51)), g_110.f0)) , l_208))) || 9UL) != g_61.f5)) ^ g_209), p_51))))) || g_103))) >= l_144))
                    { /* block id: 89 */
                        int32_t l_215 = 9L;
                        int32_t l_217[7];
                        int32_t l_236[7][1];
                        uint16_t l_237 = 0x34ADL;
                        float *****l_242 = &l_198;
                        float *****l_244 = &l_197;
                        int i, j;
                        for (i = 0; i < 7; i++)
                            l_217[i] = 0x53BE8B13L;
                        for (i = 0; i < 7; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_236[i][j] = 0xE41CE896L;
                        }
                        --l_237;
                        (*l_244) = (l_243 = ((*l_242) = g_240[0][1]));
                        l_258[1][1] = (safe_mul_func_float_f_f(((l_247 , l_248) == (void*)0), (safe_add_func_float_f_f((safe_sub_func_float_f_f((((((safe_sub_func_float_f_f((l_215 = (l_217[2] = (l_236[0][0] = p_51))), (*g_8))) == (safe_div_func_float_f_f(p_51, p_51))) >= p_51) , (l_224 = ((*g_8) = (l_257 != (-0x2.3p-1))))) >= 0x0.EFE6FDp-25), 0x4.2p+1)), p_51))));
                    }
                    else
                    { /* block id: 100 */
                        l_216 |= ((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_int8_t_s_u(g_61.f5, p_51)) < 0UL), 2)) , (*g_120));
                    }
                    return l_263;
                }
                else
                { /* block id: 104 */
                    uint64_t **l_289 = &l_287;
                    uint64_t *l_290 = &g_116;
                    int32_t **l_302 = &l_184;
                    if ((!(((safe_sub_func_int8_t_s_s(g_86, (safe_add_func_int8_t_s_s((l_247 , (safe_mod_func_int64_t_s_s(g_133.f3, ((safe_sub_func_int16_t_s_s((-5L), l_144)) && g_101[0][4][0])))), (safe_mul_func_uint8_t_u_u(((((safe_mul_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((safe_rshift_func_uint8_t_u_s((!(((safe_mul_func_uint8_t_u_u((((0xF72A84310BBCF169LL && (!(((safe_mul_func_int16_t_s_s((((*l_289) = (l_288 = l_287)) == l_290), g_101[1][5][0])) == g_209) == 0x824C0F3E8564D6ADLL))) <= g_116) <= g_61.f4), g_61.f7)) > p_51) || g_101[1][0][0])), p_51)) && 0x16L), p_51)), p_51)) ^ 1L) ^ l_221) & g_103), 1L)))))) ^ (*l_184)) , p_51)))
                    { /* block id: 107 */
                        (*l_186) = (0x6.2464D5p-83 >= (safe_div_func_float_f_f((-0x6.4p+1), (safe_div_func_float_f_f(p_51, ((-(g_296 == &l_157)) == 0x8.3E85DAp-86))))));
                        (*g_297) = &g_133;
                        if (p_51)
                            break;
                    }
                    else
                    { /* block id: 111 */
                        if (l_247.f0)
                            goto lbl_299;
                        (*l_186) &= (((*l_287) = (p_51 || (safe_rshift_func_int16_t_s_u(p_51, 7)))) >= g_220[1]);
                    }
                    (*l_302) = &l_219;
                    (*l_302) = &l_227;
                    l_186 = &l_221;
                }
                for (g_119.f0 = 0; (g_119.f0 <= 0); g_119.f0 += 1)
                { /* block id: 122 */
                    int32_t **l_308 = &l_184;
                    int i, j, k;
                    (*l_186) = ((*l_184) &= (+(safe_div_func_uint32_t_u_u(l_213[g_119.f0][(g_33 + 1)][g_33], (safe_rshift_func_uint8_t_u_u(g_121[6], 7))))));
                    (**l_243) = (**l_243);
                    (*g_309) = ((*l_308) = &l_216);
                }
                if (((+(p_51 == ((safe_div_func_uint32_t_u_u(((l_314 != ((*l_316) = l_315)) == ((0xD0834960F4A7E143LL == ((safe_lshift_func_int16_t_s_u(g_61.f1, ((safe_sub_func_uint64_t_u_u(((void*)0 != &l_232[0][0]), (g_116 = (p_51 != (*l_186))))) > (*l_184)))) & 0x4D8FF2EE9B4C6B08LL)) && l_321)), l_233[0][0])) > 0xD633L))) && g_61.f4))
                { /* block id: 131 */
                    uint32_t l_331 = 4294967289UL;
                    int32_t l_347 = 4L;
                    if ((((*l_184) & (g_220[6] | (safe_rshift_func_uint16_t_u_s(((-1L) < 0x7866C184L), 9)))) > ((*l_184) , ((safe_unary_minus_func_int8_t_s(((g_61.f0 == (safe_lshift_func_uint8_t_u_s(((safe_mod_func_int8_t_s_s(((((*g_310) != 0xCE878399L) != (safe_div_func_int64_t_s_s((-3L), p_51))) && (-6L)), g_33)) , g_103), g_61.f7))) >= (*l_184)))) >= l_331))))
                    { /* block id: 132 */
                        const int64_t *l_341 = &g_342;
                        const int64_t **l_340 = &l_341;
                        float * const l_354 = &g_355[0];
                        float * const *l_353[3];
                        float * const **l_352 = &l_353[1];
                        float * const ***l_351 = &l_352;
                        float * const ****l_350 = &l_351;
                        uint32_t *l_356 = &g_101[0][4][0];
                        float *****l_357[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_353[i] = &l_354;
                        for (i = 0; i < 3; i++)
                            l_357[i] = (void*)0;
                        (*l_186) = (0x25L >= ((safe_rshift_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s((safe_sub_func_int8_t_s_s((l_288 != ((*l_340) = &l_263)), 1L)), g_5)) , (l_347 = (safe_div_func_uint64_t_u_u((--(*l_287)), (*l_186))))), 2)) == (safe_mod_func_int16_t_s_s((((0x37D7B04CL >= ((*l_356) = (((*l_350) = (void*)0) == &g_176))) & 1UL) && g_121[6]), 0x57CAL))));
                        g_240[0][1] = (p_51 , &g_241);
                        (*g_120) |= (g_116 >= (safe_lshift_func_int8_t_s_s(g_33, 3)));
                    }
                    else
                    { /* block id: 141 */
                        if ((*l_184))
                            break;
                        return l_347;
                    }
                    (*g_360) = &l_235;
                    if (l_208)
                        goto lbl_299;
                }
                else
                { /* block id: 147 */
                    int32_t *l_361 = &l_144;
                    int32_t *l_362 = &l_231;
                    int32_t *l_363 = &l_218;
                    int32_t *l_364[6][6][7] = {{{&l_230,&l_230,(void*)0,&l_231,&g_5,&l_229[5][1],&l_231},{&l_224,&g_5,(void*)0,&l_231,&l_208,&l_208,&l_208},{&l_231,&l_208,&l_208,&l_231,&g_2,&l_231,&l_231},{&l_231,&l_208,&l_224,&l_208,&l_230,&g_2,&l_229[4][6]},{&l_231,&g_5,&l_231,&l_230,&l_231,&g_5,&l_231},{&g_5,&l_230,&g_5,&l_229[4][6],&l_231,&l_233[1][0],&l_208}},{{&l_213[0][4][3],&l_233[1][0],&l_208,&l_231,&l_230,&l_230,&l_231},{&g_5,(void*)0,&g_5,&l_229[5][1],&g_2,&l_213[0][4][3],&l_233[1][0]},{&g_5,&l_229[4][6],&l_231,&l_233[1][0],&l_208,(void*)0,&l_213[0][4][3]},{&l_213[0][4][3],&g_5,&l_224,&l_224,&g_5,&l_213[0][4][3],&l_231},{&g_5,&l_231,&l_208,&l_224,&l_208,&l_230,&g_2},{&l_231,&l_213[0][4][3],(void*)0,&l_233[1][0],&l_229[4][6],&l_233[1][0],(void*)0}},{{&l_231,&l_231,(void*)0,&l_229[5][1],&l_224,&g_5,(void*)0},{&l_231,&g_5,&l_229[5][1],&l_231,(void*)0,&g_2,&g_2},{&l_224,&l_229[4][6],&l_231,&l_229[4][6],&l_224,&l_231,&l_231},{&l_230,(void*)0,&l_231,&l_230,&l_229[4][6],&l_208,&l_213[0][4][3]},{&l_230,&l_233[1][0],&l_229[5][1],&l_208,&l_208,&l_229[5][1],&l_233[1][0]},{&l_230,&l_230,(void*)0,&l_231,&g_5,&l_229[5][1],&l_231}},{{&l_224,&g_5,(void*)0,&l_231,&l_208,&l_208,&l_208},{&l_231,&l_208,&l_208,&l_231,&g_2,&l_231,&l_231},{&l_231,&l_208,&l_224,&l_208,&l_230,&g_2,&l_229[4][6]},{&l_231,&g_5,&l_231,&l_230,&l_231,&g_5,&l_231},{&g_5,&l_230,&g_5,&l_229[4][6],&l_231,&l_233[1][0],&l_208},{&l_213[0][4][3],&l_233[1][0],&l_208,&l_231,&l_230,&l_230,&l_231}},{{&g_2,&l_233[1][0],&g_2,&g_5,(void*)0,&l_229[4][6],&l_213[0][4][3]},{&g_2,&l_231,&l_231,&l_213[0][4][3],(void*)0,&l_233[1][0],&l_229[4][6]},{&l_229[4][6],&l_208,&g_5,&g_5,&l_208,&l_229[4][6],&l_230},{&l_208,&l_231,(void*)0,&g_5,&l_224,&l_229[5][1],(void*)0},{&l_230,&l_229[4][6],&l_208,&l_213[0][4][3],&l_231,&l_213[0][4][3],&l_208},{&l_231,&l_231,&l_233[1][0],&g_5,&g_5,&g_2,&l_208}},{{&l_230,&l_208,&g_5,&l_231,&l_208,(void*)0,(void*)0},{&g_5,&l_231,&l_230,&l_231,&g_5,&l_231,&l_230},{&l_229[5][1],&l_233[1][0],&l_230,&l_231,&l_231,&l_224,&l_229[4][6]},{&l_231,&l_213[0][4][3],&g_5,&l_224,&l_224,&g_5,&l_213[0][4][3]},{&l_229[5][1],&l_231,&l_233[1][0],&l_230,&l_208,&g_5,&l_231},{&g_5,&g_2,&l_208,&l_230,(void*)0,&l_224,(void*)0}}};
                    int64_t l_383 = (-9L);
                    float l_392[5][10] = {{0x3.8p-1,0x7.AFE907p+76,0x3.8p-1,0x4.6AE00Ep+29,0x5.D945B9p-86,0x4.6AE00Ep+29,0x3.8p-1,0x7.AFE907p+76,0x3.8p-1,0x4.6AE00Ep+29},{0x5.D945B9p-86,0x7.AFE907p+76,(-0x4.Fp-1),0x7.AFE907p+76,0x5.D945B9p-86,0x8.1E77B7p-8,0x5.D945B9p-86,0x7.AFE907p+76,(-0x4.Fp-1),0x7.AFE907p+76},{0x5.D945B9p-86,0x4.6AE00Ep+29,0x3.8p-1,0x7.AFE907p+76,0x3.8p-1,0x4.6AE00Ep+29,0x5.D945B9p-86,0x4.6AE00Ep+29,0x3.8p-1,0x7.AFE907p+76},{0x3.8p-1,0x7.AFE907p+76,0x3.8p-1,0x4.6AE00Ep+29,0x5.D945B9p-86,0x4.6AE00Ep+29,0x3.8p-1,0x7.AFE907p+76,0x3.8p-1,0x4.6AE00Ep+29},{0x5.D945B9p-86,0x7.AFE907p+76,(-0x4.Fp-1),0x7.AFE907p+76,0x5.D945B9p-86,0x8.1E77B7p-8,0x5.D945B9p-86,0x7.AFE907p+76,(-0x4.Fp-1),0x7.AFE907p+76}};
                    uint16_t * const l_413[1] = {&l_321};
                    int i, j, k;
                    if (l_263)
                        goto lbl_299;
                    g_374--;
                    for (l_247.f0 = 1; (l_247.f0 <= 6); l_247.f0 += 1)
                    { /* block id: 152 */
                        int64_t l_378 = 0x790C9ABCD47B4CE0LL;
                        int32_t l_380 = 3L;
                        int32_t l_381 = (-4L);
                        int32_t l_384 = 0x39C4FD50L;
                        int32_t l_385[4] = {0xFE268840L,0xFE268840L,0xFE268840L,0xFE268840L};
                        float l_395 = 0x7.1C4F48p+39;
                        int64_t l_406 = 0L;
                        float l_408 = (-0x1.1p-1);
                        int i;
                        l_410++;
                        (*l_186) ^= (**g_309);
                    }
                    if ((g_371 >= (&g_69 == l_413[0])))
                    { /* block id: 156 */
                        uint8_t l_420 = 0xDDL;
                        (*l_363) ^= (((**l_314) , ((*l_186) && (((safe_add_func_int16_t_s_s(((((--(*l_287)) < (safe_mul_func_int8_t_s_s((l_362 == (void*)0), (((*l_184) < ((*g_60) , ((((*l_186) &= (**g_124)) && (*g_310)) != ((*g_310) | p_51)))) >= p_51)))) , &g_86) != (void*)0), (*l_184))) & l_420) > 9UL))) || (*l_186));
                        (*g_120) = ((*l_184) >= p_51);
                    }
                    else
                    { /* block id: 161 */
                        float * const l_427[3][5] = {{&g_9[2][1][1],&g_9[2][1][1],&g_9[2][1][1],&g_9[2][1][1],&g_9[2][1][1]},{&l_225[2][3],(void*)0,&l_225[2][3],(void*)0,&l_225[2][3]},{&g_9[2][1][1],&g_9[2][1][1],&g_9[2][1][1],&g_9[2][1][1],&g_9[2][1][1]}};
                        int8_t l_435 = 0xB9L;
                        int i, j;
                        (*g_241) = l_421;
                        (*l_361) ^= (safe_lshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(((*l_184) = (((void*)0 == l_427[1][4]) < (l_435 = (g_69 = (!(((*l_186) ^ (*l_184)) <= ((*l_363) = (((safe_mul_func_uint8_t_u_u((0x5EL > ((p_51 && ((*l_287) = (l_258[2][0] <= ((safe_sub_func_int64_t_s_s((safe_mul_func_int16_t_s_s(1L, (g_158.f3 ^ p_51))), 0UL)) | 1L)))) < 0x4EE0C7701F99D9A9LL)), p_51)) >= g_86) ^ l_396)))))))), 8)), g_82));
                    }
                }
            }
            for (l_402 = 0; (l_402 >= 0); l_402 -= 1)
            { /* block id: 174 */
                uint16_t **l_453 = &l_181;
                int32_t l_459 = 0xD061B0DAL;
                uint8_t l_464 = 7UL;
                struct S1 l_478 = {0xAA8818C4L};
                uint8_t l_496 = 0x35L;
                int8_t l_506 = (-9L);
                int32_t l_510 = 0L;
                int32_t l_512 = (-1L);
                int64_t *l_551 = &l_228[2][4];
                int64_t **l_550 = &l_551;
                int i;
                if ((!g_121[(g_82 + 2)]))
                { /* block id: 175 */
                    uint8_t *l_449[2][9] = {{&g_82,&g_82,(void*)0,&g_82,&g_82,(void*)0,&g_82,&g_82,(void*)0},{&g_82,&g_82,&g_82,&g_82,&g_82,&g_82,&g_82,&g_82,&g_82}};
                    const int32_t l_456 = 8L;
                    int32_t l_457 = 0xCB13BE25L;
                    int32_t *l_458[8] = {&l_367,&l_367,&l_367,&l_367,&l_367,&l_367,&l_367,&l_367};
                    int16_t l_460 = 5L;
                    int i, j;
                    l_460 ^= (((safe_sub_func_uint64_t_u_u(((l_459 = ((g_121[(l_402 + 3)] && 0x094FC7FEL) || (((((*g_310) = (safe_lshift_func_uint16_t_u_u((4294967291UL && ((l_457 &= ((g_371 ^= ((safe_sub_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((((safe_add_func_uint32_t_u_u(((g_121[(l_402 + 1)] == ((safe_mod_func_uint64_t_u_u(((g_450 ^= 1UL) == p_51), (safe_mul_func_int16_t_s_s((((**l_314) , l_453) != g_454), l_455)))) | p_51)) || p_51), 0UL)) || g_220[1]) , (*l_184)), 4294967295UL)), l_456)) ^ g_143[8])) , (**g_360))) != g_404[4])), 12))) != l_456) < 0x1520L) != g_61.f4))) ^ 0x443620B6L), 18446744073709551615UL)) <= g_143[8]) >= p_51);
                    if ((*l_184))
                    { /* block id: 182 */
                        int32_t **l_461 = &l_458[6];
                        (*g_360) = ((*l_461) = (*g_309));
                    }
                    else
                    { /* block id: 185 */
                        uint32_t *l_466 = &g_101[0][4][0];
                        const int32_t l_477 = 0xCC8F087AL;
                        (*g_310) ^= ((safe_div_func_uint64_t_u_u(((l_464 <= ((((&g_240[0][1] != g_465) <= (((*l_466) = p_51) != 0xD6742A25L)) , (((l_366 ^= (safe_div_func_uint16_t_u_u(((~0UL) == (safe_mul_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u(0x1245L, 3)), ((safe_lshift_func_uint16_t_u_s(g_476, 4)) == 0xB8L)))), l_477))) ^ p_51) , p_51)) & g_371)) || g_101[0][3][0]), p_51)) & 0xB0FB7AAD8B1F7E3DLL);
                        (**l_314) = l_478;
                        (*g_479) = (*g_297);
                    }
                    (*g_481) = ((**l_314) = l_480);
                }
                else
                { /* block id: 194 */
                    for (l_379 = 0; (l_379 <= 0); l_379 += 1)
                    { /* block id: 197 */
                        int32_t **l_483 = &l_184;
                        (*g_484) = ((*l_483) = (*g_309));
                        (*l_483) = &l_387;
                    }
                    if (l_227)
                        goto lbl_299;
                }
                for (g_476 = 0; (g_476 >= 0); g_476 -= 1)
                { /* block id: 206 */
                    int32_t l_507 = 1L;
                    int32_t l_509 = 0xB66E0D91L;
                    int i, j, k;
                    if (g_101[l_402][(l_402 + 6)][l_402])
                    { /* block id: 207 */
                        int32_t *l_486 = &l_227;
                        int32_t *l_487 = &l_459;
                        int32_t *l_488 = &l_233[2][0];
                        int32_t *l_489 = &l_367;
                        int32_t *l_490[8][2] = {{&l_367,&l_233[1][0]},{&l_213[0][4][3],&l_388},{&l_388,&l_213[0][4][3]},{&l_233[1][0],&l_367},{&l_233[1][0],&l_213[0][4][3]},{&l_221,&l_221},{&l_367,&l_213[0][4][3]},{&l_391,&l_213[0][4][3]}};
                        uint32_t *l_497[10][4][3] = {{{&l_258[5][2],&g_101[0][4][0],&l_258[6][0]},{&l_258[5][2],(void*)0,&g_101[0][4][0]},{(void*)0,&l_258[5][2],&l_258[6][0]},{&g_101[0][4][0],&l_258[5][2],&g_101[0][4][0]}},{{(void*)0,(void*)0,(void*)0},{&g_101[0][4][0],&g_101[0][4][0],(void*)0},{(void*)0,(void*)0,&g_101[0][4][0]},{&l_258[5][2],&g_101[0][4][0],&l_258[6][0]}},{{&l_258[5][2],(void*)0,&g_101[0][4][0]},{(void*)0,&l_258[5][2],&l_258[6][0]},{&g_101[0][4][0],&l_258[5][2],&g_101[0][4][0]},{(void*)0,(void*)0,(void*)0}},{{&g_101[0][4][0],&g_101[0][4][0],(void*)0},{(void*)0,(void*)0,&g_101[0][4][0]},{&l_258[5][2],&g_101[0][4][0],&l_258[6][0]},{&l_258[5][2],(void*)0,&g_101[0][4][0]}},{{(void*)0,&l_258[5][2],&l_258[6][0]},{&g_101[0][4][0],&l_258[5][2],&g_101[0][4][0]},{(void*)0,(void*)0,(void*)0},{&g_101[0][4][0],&g_101[0][4][0],(void*)0}},{{(void*)0,(void*)0,&g_101[0][4][0]},{&l_258[5][2],&g_101[0][4][0],&l_258[6][0]},{&l_258[5][2],(void*)0,&g_101[0][4][0]},{(void*)0,&l_258[5][2],&l_258[6][0]}},{{&g_101[0][4][0],&l_258[5][2],&g_101[0][4][0]},{(void*)0,(void*)0,(void*)0},{&g_101[0][4][0],&g_101[0][4][0],(void*)0},{(void*)0,(void*)0,&g_101[0][4][0]}},{{&l_258[5][2],&g_101[0][4][0],&l_258[6][0]},{&l_258[5][2],(void*)0,&g_101[0][4][0]},{(void*)0,&l_258[5][2],&l_258[6][0]},{&g_101[0][4][0],&l_258[5][2],&g_101[0][4][0]}},{{(void*)0,(void*)0,(void*)0},{&g_101[0][4][0],&g_101[0][4][0],(void*)0},{(void*)0,(void*)0,&g_101[0][4][0]},{&l_258[5][2],&g_101[0][4][0],&l_258[6][0]}},{{&l_258[5][2],(void*)0,&g_101[0][4][0]},{(void*)0,&l_258[5][2],&l_258[6][0]},{&g_101[0][4][0],&l_258[5][2],&g_101[0][4][0]},{(void*)0,(void*)0,(void*)0}}};
                        float l_511[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_511[i] = 0x0.8EED28p-12;
                        --l_491[3][3][0];
                        if (g_101[l_402][(l_402 + 6)][l_402])
                            break;
                        l_506 ^= (safe_rshift_func_uint8_t_u_s(((((**g_297) , (p_51 = (l_496 < g_101[l_402][(l_402 + 6)][l_402]))) <= (((safe_sub_func_uint8_t_u_u(((safe_mod_func_uint8_t_u_u((((0x6E8505CB7F9E59CBLL || (g_61.f5 != g_101[l_402][(l_402 + 6)][l_402])) , ((safe_div_func_int64_t_s_s((safe_sub_func_uint32_t_u_u((g_61.f4 = g_101[l_402][(l_402 + 6)][l_402]), (*g_310))), g_61.f7)) , 0xFC60965EL)) , 1UL), l_464)) >= (*l_488)), l_496)) , g_116) ^ 0x49395CCF67F4E9E9LL)) , g_61.f1), 5));
                        ++l_513;
                    }
                    else
                    { /* block id: 214 */
                        float l_545 = 0x5.Fp+1;
                        const int32_t l_546 = 0x9418C047L;
                        int64_t l_549[5][8] = {{(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L),(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L)},{(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L),(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L)},{(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L),(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L)},{(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L),(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L)},{(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L),(-1L),(-6L),0xDBE5C1DB951696F7LL,(-6L)}};
                        int32_t *l_554 = &l_224;
                        int32_t *l_555[5][9][5] = {{{&l_400,&l_388,(void*)0,&l_231,&l_510},{&l_400,(void*)0,&l_370,(void*)0,&l_400},{&l_388,&g_2,&g_5,&l_231,&l_367},{&g_5,&g_2,&l_388,&l_388,&g_2},{&l_370,(void*)0,&l_400,&g_2,&l_367},{&l_403[7][3][0],&l_367,&g_5,(void*)0,&g_5},{&l_387,&l_387,&l_367,&g_2,&l_400},{&l_403[7][3][0],&l_400,&l_370,&g_2,&g_2},{&g_2,&l_231,&g_2,(void*)0,&l_510}},{{&l_370,&l_400,&l_403[7][3][0],&l_388,&l_510},{&l_367,&l_387,&l_387,&l_367,&g_2},{&g_5,&l_367,&l_403[7][3][0],&l_510,&l_400},{&g_5,&l_403[7][3][0],&g_2,&l_403[7][3][0],&g_5},{&l_367,&l_388,&l_370,&l_510,&l_387},{&l_370,&l_388,&l_367,&l_367,&l_388},{&g_2,&l_403[7][3][0],&g_5,&l_388,&l_387},{&l_403[7][3][0],&l_367,&g_5,(void*)0,&g_5},{&l_387,&l_387,&l_367,&g_2,&l_400}},{{&l_403[7][3][0],&l_400,&l_370,&g_2,&g_2},{&g_2,&l_231,&g_2,(void*)0,&l_510},{&l_370,&l_400,&l_403[7][3][0],&l_388,&l_510},{&l_367,&l_387,&l_387,&l_367,&g_2},{&g_5,&l_367,&l_403[7][3][0],&l_510,&l_400},{&g_5,&l_403[7][3][0],&g_2,&l_403[7][3][0],&g_5},{&l_367,&l_388,&l_370,&l_510,&l_387},{&l_370,&l_388,&l_367,&l_367,&l_388},{&g_2,&l_403[7][3][0],&g_5,&l_388,&l_387}},{{&l_403[7][3][0],&l_367,&g_5,(void*)0,&g_5},{&l_387,&l_387,&l_367,&g_2,&l_400},{&l_403[7][3][0],&l_400,&l_370,&g_2,&g_2},{&g_2,&l_231,&g_2,(void*)0,&l_510},{&l_370,&l_400,&l_403[7][3][0],&l_388,&l_510},{&l_367,&l_387,&l_387,&l_367,&g_2},{&g_5,&l_367,&l_403[7][3][0],&l_510,&l_400},{&g_5,&l_403[7][3][0],&g_2,&l_403[7][3][0],&g_5},{&l_367,&l_388,&l_370,&l_510,&l_387}},{{&l_370,&l_388,&l_367,&l_367,&l_388},{&g_2,&l_403[7][3][0],&g_5,&l_388,&l_387},{&l_403[7][3][0],&l_367,&g_5,(void*)0,&g_5},{&l_387,&l_387,&l_367,&g_2,&l_400},{&l_403[7][3][0],&l_400,&l_370,&g_2,&g_2},{&g_2,&l_231,&g_2,(void*)0,&l_510},{&l_370,&l_400,&l_403[7][3][0],&l_388,&l_510},{&l_387,(void*)0,(void*)0,&l_387,&l_388},{&l_370,&l_387,&l_231,&l_400,&g_5}}};
                        int i, j, k;
                        l_509 = (l_549[4][1] = ((safe_sub_func_float_f_f((((safe_div_func_float_f_f((((!(safe_mul_func_uint16_t_u_u(g_101[l_402][(l_402 + 6)][l_402], ((((safe_sub_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s(((p_51 >= p_51) , ((((-0x1.1p-1) > ((((0x2.70E433p-25 >= ((safe_div_func_float_f_f(((safe_add_func_float_f_f((safe_div_func_float_f_f((((*l_422) = ((safe_mul_func_float_f_f(((!((safe_rshift_func_uint8_t_u_s(((~((((safe_mul_func_uint16_t_u_u((safe_add_func_uint8_t_u_u((p_51 || (safe_mul_func_uint8_t_u_u(g_101[1][4][0], ((void*)0 != &l_258[1][2])))), l_400)), l_403[7][3][0])) ^ p_51) < g_450) <= 1L)) < g_61.f6), 5)) , l_546)) < l_141[3][4][3]), 0x0.EFC096p-78)) > (-0x1.1p+1))) != p_51), p_51)), l_510)) > 0xC.286DA5p-46), l_546)) , p_51)) == p_51) <= 0x1.7BDD03p-10) > (-0x4.Fp-1))) , 0x222BEB9B0E62C1B0LL) < 18446744073709551612UL)), 14)), g_33)) <= 65535UL) , l_546) ^ 0xB0L)))) , l_547) == g_298), 0x8.166F8Ap-90)) >= 0x0.1p-1) != l_398), 0x7.4CF074p+19)) > l_507));
                        l_552[2] = l_550;
                        if (l_546)
                            continue;
                        l_556++;
                    }
                    (*g_120) = 0x303945E9L;
                    for (l_510 = 0; (l_510 <= 5); l_510 += 1)
                    { /* block id: 225 */
                        int i, j;
                        if (l_233[(g_476 + 2)][g_476])
                            break;
                        (*g_560) = &l_233[(l_402 + 1)][l_402];
                    }
                }
            }
        }
    }
    return l_232[0][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_110 g_120 g_124 g_133 g_60 g_32 g_33
 * writes: g_119 g_120 g_69 g_121
 */
static int32_t  func_54(struct S0 * p_55, int16_t  p_56, const struct S1  p_57, uint32_t  p_58, struct S1  p_59)
{ /* block id: 46 */
    struct S1 *l_118 = &g_119;
    volatile int32_t **l_122 = (void*)0;
    int64_t *l_134 = (void*)0;
    int64_t **l_135 = &l_134;
    uint16_t *l_136[10][10] = {{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69,&g_69}};
    int32_t l_137 = 0xFFBABF7BL;
    int i, j;
    (*l_118) = g_110;
    (*g_124) = g_120;
    (*g_120) = (safe_lshift_func_uint16_t_u_s((g_69 = (((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f(((g_133 , ((*l_135) = l_134)) == &g_86), ((void*)0 != g_60))), (p_57.f0 > (p_59.f0 , (-0x1.Cp+1))))), p_59.f0)) , (*g_32)) || 1UL)), l_137));
    return p_58;
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_2 g_61.f3 g_5 g_61.f1 g_61.f5 g_61.f4 g_101 g_33 g_86 g_103 g_61.f2 g_105 g_61.f6
 * writes: g_69 g_82 g_86 g_61.f5 g_101 g_103 g_105 g_116
 */
static struct S1  func_62(uint64_t  p_63, int16_t  p_64, int32_t  p_65)
{ /* block id: 21 */
    uint16_t *l_68 = &g_69;
    int32_t *l_78 = &g_2;
    int32_t *l_80 = &g_5;
    int32_t **l_79 = &l_80;
    uint8_t *l_81 = &g_82;
    int64_t *l_85 = &g_86;
    uint32_t *l_100 = &g_101[0][4][0];
    int16_t *l_102[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t *l_104 = &g_105;
    struct S1 l_117 = {0xB7CA390AL};
    int i;
    (*l_104) |= (((--(*l_68)) , ((safe_mod_func_uint16_t_u_u(((g_103 &= (safe_add_func_int64_t_s_s((0xC3L || (((*l_85) = (safe_rshift_func_uint8_t_u_s(((*l_81) = (l_78 != ((*l_79) = &g_5))), (safe_sub_func_uint16_t_u_u(((*l_78) ^ g_2), 4L))))) && ((safe_sub_func_uint64_t_u_u((safe_rshift_func_int16_t_s_s(((+((!((*l_100) &= ((g_61.f3 >= (*l_80)) & ((safe_lshift_func_int16_t_s_s((((safe_mul_func_uint16_t_u_u(((safe_div_func_int16_t_s_s(((safe_unary_minus_func_int32_t_s((g_61.f5 ^= (((void*)0 != l_68) , g_61.f1)))) > p_64), 0x33F8L)) != 5UL), g_69)) , p_63) != g_61.f4), 1)) || 1L)))) && g_33)) , (*l_80)), (*l_78))), 0x007C168B3AD7D893LL)) || g_86))), 0x581CA57882416071LL))) || p_63), g_61.f2)) | 0x2B3C324F1CC41D5CLL)) <= p_64);
lbl_111:
    for (g_103 = (-28); (g_103 > (-25)); g_103 = safe_add_func_int64_t_s_s(g_103, 9))
    { /* block id: 32 */
        (*l_104) |= 1L;
        (*l_79) = &g_105;
    }
    for (p_63 = 0; (p_63 <= 3); p_63 += 1)
    { /* block id: 38 */
        const struct S1 *l_109 = &g_110;
        const struct S1 **l_108 = &l_109;
        int i;
        (*l_108) = (void*)0;
        if (g_105)
            goto lbl_111;
        g_61.f5 = (safe_mod_func_int32_t_s_s(g_103, ((g_116 = (safe_rshift_func_uint8_t_u_s((*l_80), 7))) | ((*l_80) , g_61.f6))));
        (*l_104) = 0L;
    }
    return l_117;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_9[i][j][k], sizeof(g_9[i][j][k]), "g_9[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_23[i][j][k], "g_23[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_61.f0, "g_61.f0", print_hash_value);
    transparent_crc(g_61.f1, "g_61.f1", print_hash_value);
    transparent_crc(g_61.f2, "g_61.f2", print_hash_value);
    transparent_crc(g_61.f3, "g_61.f3", print_hash_value);
    transparent_crc(g_61.f4, "g_61.f4", print_hash_value);
    transparent_crc(g_61.f5, "g_61.f5", print_hash_value);
    transparent_crc(g_61.f6, "g_61.f6", print_hash_value);
    transparent_crc(g_61.f7, "g_61.f7", print_hash_value);
    transparent_crc(g_69, "g_69", print_hash_value);
    transparent_crc(g_82, "g_82", print_hash_value);
    transparent_crc(g_86, "g_86", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_101[i][j][k], "g_101[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    transparent_crc(g_110.f0, "g_110.f0", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_119.f0, "g_119.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_121[i], "g_121[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_133.f0, "g_133.f0", print_hash_value);
    transparent_crc(g_133.f3, "g_133.f3", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_143[i], "g_143[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_158.f0, "g_158.f0", print_hash_value);
    transparent_crc(g_158.f3, "g_158.f3", print_hash_value);
    transparent_crc(g_209, "g_209", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_220[i], "g_220[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_342, "g_342", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc_bytes(&g_355[i], sizeof(g_355[i]), "g_355[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_371, "g_371", print_hash_value);
    transparent_crc(g_374, "g_374", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_404[i], "g_404[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_409, sizeof(g_409), "g_409", print_hash_value);
    transparent_crc(g_450, "g_450", print_hash_value);
    transparent_crc(g_476, "g_476", print_hash_value);
    transparent_crc(g_482.f0, "g_482.f0", print_hash_value);
    transparent_crc(g_508, "g_508", print_hash_value);
    transparent_crc(g_548.f0, "g_548.f0", print_hash_value);
    transparent_crc(g_548.f3, "g_548.f3", print_hash_value);
    transparent_crc(g_574.f0, "g_574.f0", print_hash_value);
    transparent_crc(g_574.f3, "g_574.f3", print_hash_value);
    transparent_crc(g_596.f0, "g_596.f0", print_hash_value);
    transparent_crc(g_658.f0, "g_658.f0", print_hash_value);
    transparent_crc(g_668, "g_668", print_hash_value);
    transparent_crc_bytes (&g_687, sizeof(g_687), "g_687", print_hash_value);
    transparent_crc_bytes (&g_689, sizeof(g_689), "g_689", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_968[i].f0, "g_968[i].f0", print_hash_value);
        transparent_crc(g_968[i].f3, "g_968[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_970.f0, "g_970.f0", print_hash_value);
    transparent_crc(g_970.f3, "g_970.f3", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_994[i][j], "g_994[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_999[i][j], "g_999[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1094, sizeof(g_1094), "g_1094", print_hash_value);
    transparent_crc(g_1201, "g_1201", print_hash_value);
    transparent_crc(g_1262.f0, "g_1262.f0", print_hash_value);
    transparent_crc(g_1262.f1, "g_1262.f1", print_hash_value);
    transparent_crc(g_1262.f2, "g_1262.f2", print_hash_value);
    transparent_crc(g_1262.f3, "g_1262.f3", print_hash_value);
    transparent_crc(g_1262.f4, "g_1262.f4", print_hash_value);
    transparent_crc(g_1262.f5, "g_1262.f5", print_hash_value);
    transparent_crc(g_1262.f6, "g_1262.f6", print_hash_value);
    transparent_crc(g_1262.f7, "g_1262.f7", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1339[i][j][k], "g_1339[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1372[i][j][k].f0, "g_1372[i][j][k].f0", print_hash_value);
                transparent_crc(g_1372[i][j][k].f1, "g_1372[i][j][k].f1", print_hash_value);
                transparent_crc(g_1372[i][j][k].f2, "g_1372[i][j][k].f2", print_hash_value);
                transparent_crc(g_1372[i][j][k].f3, "g_1372[i][j][k].f3", print_hash_value);
                transparent_crc(g_1372[i][j][k].f4, "g_1372[i][j][k].f4", print_hash_value);
                transparent_crc(g_1372[i][j][k].f5, "g_1372[i][j][k].f5", print_hash_value);
                transparent_crc(g_1372[i][j][k].f6, "g_1372[i][j][k].f6", print_hash_value);
                transparent_crc(g_1372[i][j][k].f7, "g_1372[i][j][k].f7", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1409, "g_1409", print_hash_value);
    transparent_crc(g_1426, "g_1426", print_hash_value);
    transparent_crc(g_1461.f3, "g_1461.f3", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1501[i], "g_1501[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1515, "g_1515", print_hash_value);
    transparent_crc(g_1534.f0, "g_1534.f0", print_hash_value);
    transparent_crc(g_1534.f1, "g_1534.f1", print_hash_value);
    transparent_crc(g_1534.f2, "g_1534.f2", print_hash_value);
    transparent_crc(g_1534.f3, "g_1534.f3", print_hash_value);
    transparent_crc(g_1534.f4, "g_1534.f4", print_hash_value);
    transparent_crc(g_1534.f5, "g_1534.f5", print_hash_value);
    transparent_crc(g_1534.f6, "g_1534.f6", print_hash_value);
    transparent_crc(g_1534.f7, "g_1534.f7", print_hash_value);
    transparent_crc_bytes (&g_1545, sizeof(g_1545), "g_1545", print_hash_value);
    transparent_crc(g_1555, "g_1555", print_hash_value);
    transparent_crc(g_1612, "g_1612", print_hash_value);
    transparent_crc(g_1626, "g_1626", print_hash_value);
    transparent_crc(g_1627.f0, "g_1627.f0", print_hash_value);
    transparent_crc(g_1627.f3, "g_1627.f3", print_hash_value);
    transparent_crc(g_1681.f0, "g_1681.f0", print_hash_value);
    transparent_crc(g_1681.f3, "g_1681.f3", print_hash_value);
    transparent_crc(g_1682, "g_1682", print_hash_value);
    transparent_crc(g_1733.f0, "g_1733.f0", print_hash_value);
    transparent_crc(g_1733.f1, "g_1733.f1", print_hash_value);
    transparent_crc(g_1733.f2, "g_1733.f2", print_hash_value);
    transparent_crc(g_1733.f3, "g_1733.f3", print_hash_value);
    transparent_crc(g_1733.f4, "g_1733.f4", print_hash_value);
    transparent_crc(g_1733.f5, "g_1733.f5", print_hash_value);
    transparent_crc(g_1733.f6, "g_1733.f6", print_hash_value);
    transparent_crc(g_1733.f7, "g_1733.f7", print_hash_value);
    transparent_crc(g_1743, "g_1743", print_hash_value);
    transparent_crc(g_1745, "g_1745", print_hash_value);
    transparent_crc(g_1748.f0, "g_1748.f0", print_hash_value);
    transparent_crc(g_1748.f3, "g_1748.f3", print_hash_value);
    transparent_crc(g_1780, "g_1780", print_hash_value);
    transparent_crc(g_1781.f0, "g_1781.f0", print_hash_value);
    transparent_crc(g_1781.f1, "g_1781.f1", print_hash_value);
    transparent_crc(g_1781.f2, "g_1781.f2", print_hash_value);
    transparent_crc(g_1781.f3, "g_1781.f3", print_hash_value);
    transparent_crc(g_1781.f4, "g_1781.f4", print_hash_value);
    transparent_crc(g_1781.f5, "g_1781.f5", print_hash_value);
    transparent_crc(g_1781.f6, "g_1781.f6", print_hash_value);
    transparent_crc(g_1781.f7, "g_1781.f7", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1824[i][j], "g_1824[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1845.f0, "g_1845.f0", print_hash_value);
    transparent_crc(g_1845.f1, "g_1845.f1", print_hash_value);
    transparent_crc(g_1845.f2, "g_1845.f2", print_hash_value);
    transparent_crc(g_1845.f3, "g_1845.f3", print_hash_value);
    transparent_crc(g_1845.f4, "g_1845.f4", print_hash_value);
    transparent_crc(g_1845.f5, "g_1845.f5", print_hash_value);
    transparent_crc(g_1845.f6, "g_1845.f6", print_hash_value);
    transparent_crc(g_1845.f7, "g_1845.f7", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1978[i], "g_1978[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2015.f0, "g_2015.f0", print_hash_value);
    transparent_crc(g_2015.f1, "g_2015.f1", print_hash_value);
    transparent_crc(g_2015.f2, "g_2015.f2", print_hash_value);
    transparent_crc(g_2015.f3, "g_2015.f3", print_hash_value);
    transparent_crc(g_2015.f4, "g_2015.f4", print_hash_value);
    transparent_crc(g_2015.f5, "g_2015.f5", print_hash_value);
    transparent_crc(g_2015.f6, "g_2015.f6", print_hash_value);
    transparent_crc(g_2015.f7, "g_2015.f7", print_hash_value);
    transparent_crc(g_2037.f0, "g_2037.f0", print_hash_value);
    transparent_crc(g_2037.f1, "g_2037.f1", print_hash_value);
    transparent_crc(g_2037.f2, "g_2037.f2", print_hash_value);
    transparent_crc(g_2037.f3, "g_2037.f3", print_hash_value);
    transparent_crc(g_2037.f4, "g_2037.f4", print_hash_value);
    transparent_crc(g_2037.f5, "g_2037.f5", print_hash_value);
    transparent_crc(g_2037.f6, "g_2037.f6", print_hash_value);
    transparent_crc(g_2037.f7, "g_2037.f7", print_hash_value);
    transparent_crc(g_2070, "g_2070", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2073[i].f0, "g_2073[i].f0", print_hash_value);
        transparent_crc(g_2073[i].f3, "g_2073[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2086.f0, "g_2086.f0", print_hash_value);
    transparent_crc(g_2086.f1, "g_2086.f1", print_hash_value);
    transparent_crc(g_2086.f2, "g_2086.f2", print_hash_value);
    transparent_crc(g_2086.f3, "g_2086.f3", print_hash_value);
    transparent_crc(g_2086.f4, "g_2086.f4", print_hash_value);
    transparent_crc(g_2086.f5, "g_2086.f5", print_hash_value);
    transparent_crc(g_2086.f6, "g_2086.f6", print_hash_value);
    transparent_crc(g_2086.f7, "g_2086.f7", print_hash_value);
    transparent_crc(g_2107.f0, "g_2107.f0", print_hash_value);
    transparent_crc(g_2107.f3, "g_2107.f3", print_hash_value);
    transparent_crc(g_2133.f0, "g_2133.f0", print_hash_value);
    transparent_crc(g_2133.f1, "g_2133.f1", print_hash_value);
    transparent_crc(g_2133.f2, "g_2133.f2", print_hash_value);
    transparent_crc(g_2133.f3, "g_2133.f3", print_hash_value);
    transparent_crc(g_2133.f4, "g_2133.f4", print_hash_value);
    transparent_crc(g_2133.f5, "g_2133.f5", print_hash_value);
    transparent_crc(g_2133.f6, "g_2133.f6", print_hash_value);
    transparent_crc(g_2133.f7, "g_2133.f7", print_hash_value);
    transparent_crc(g_2134.f0, "g_2134.f0", print_hash_value);
    transparent_crc(g_2134.f1, "g_2134.f1", print_hash_value);
    transparent_crc(g_2134.f2, "g_2134.f2", print_hash_value);
    transparent_crc(g_2134.f3, "g_2134.f3", print_hash_value);
    transparent_crc(g_2134.f4, "g_2134.f4", print_hash_value);
    transparent_crc(g_2134.f5, "g_2134.f5", print_hash_value);
    transparent_crc(g_2134.f6, "g_2134.f6", print_hash_value);
    transparent_crc(g_2134.f7, "g_2134.f7", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2135[i].f0, "g_2135[i].f0", print_hash_value);
        transparent_crc(g_2135[i].f1, "g_2135[i].f1", print_hash_value);
        transparent_crc(g_2135[i].f2, "g_2135[i].f2", print_hash_value);
        transparent_crc(g_2135[i].f3, "g_2135[i].f3", print_hash_value);
        transparent_crc(g_2135[i].f4, "g_2135[i].f4", print_hash_value);
        transparent_crc(g_2135[i].f5, "g_2135[i].f5", print_hash_value);
        transparent_crc(g_2135[i].f6, "g_2135[i].f6", print_hash_value);
        transparent_crc(g_2135[i].f7, "g_2135[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2137.f0, "g_2137.f0", print_hash_value);
    transparent_crc(g_2137.f1, "g_2137.f1", print_hash_value);
    transparent_crc(g_2137.f2, "g_2137.f2", print_hash_value);
    transparent_crc(g_2137.f3, "g_2137.f3", print_hash_value);
    transparent_crc(g_2137.f4, "g_2137.f4", print_hash_value);
    transparent_crc(g_2137.f5, "g_2137.f5", print_hash_value);
    transparent_crc(g_2137.f6, "g_2137.f6", print_hash_value);
    transparent_crc(g_2137.f7, "g_2137.f7", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_2270[i][j][k], "g_2270[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2334, "g_2334", print_hash_value);
    transparent_crc(g_2338.f0, "g_2338.f0", print_hash_value);
    transparent_crc(g_2338.f3, "g_2338.f3", print_hash_value);
    transparent_crc(g_2341, "g_2341", print_hash_value);
    transparent_crc(g_2345.f0, "g_2345.f0", print_hash_value);
    transparent_crc(g_2345.f1, "g_2345.f1", print_hash_value);
    transparent_crc(g_2345.f2, "g_2345.f2", print_hash_value);
    transparent_crc(g_2345.f3, "g_2345.f3", print_hash_value);
    transparent_crc(g_2345.f4, "g_2345.f4", print_hash_value);
    transparent_crc(g_2345.f5, "g_2345.f5", print_hash_value);
    transparent_crc(g_2345.f6, "g_2345.f6", print_hash_value);
    transparent_crc(g_2345.f7, "g_2345.f7", print_hash_value);
    transparent_crc(g_2447, "g_2447", print_hash_value);
    transparent_crc(g_2524.f0, "g_2524.f0", print_hash_value);
    transparent_crc(g_2524.f1, "g_2524.f1", print_hash_value);
    transparent_crc(g_2524.f2, "g_2524.f2", print_hash_value);
    transparent_crc(g_2524.f3, "g_2524.f3", print_hash_value);
    transparent_crc(g_2524.f4, "g_2524.f4", print_hash_value);
    transparent_crc(g_2524.f5, "g_2524.f5", print_hash_value);
    transparent_crc(g_2524.f6, "g_2524.f6", print_hash_value);
    transparent_crc(g_2524.f7, "g_2524.f7", print_hash_value);
    transparent_crc(g_2525.f0, "g_2525.f0", print_hash_value);
    transparent_crc(g_2525.f1, "g_2525.f1", print_hash_value);
    transparent_crc(g_2525.f2, "g_2525.f2", print_hash_value);
    transparent_crc(g_2525.f3, "g_2525.f3", print_hash_value);
    transparent_crc(g_2525.f4, "g_2525.f4", print_hash_value);
    transparent_crc(g_2525.f5, "g_2525.f5", print_hash_value);
    transparent_crc(g_2525.f6, "g_2525.f6", print_hash_value);
    transparent_crc(g_2525.f7, "g_2525.f7", print_hash_value);
    transparent_crc(g_2566, "g_2566", print_hash_value);
    transparent_crc(g_2610, "g_2610", print_hash_value);
    transparent_crc(g_2648.f0, "g_2648.f0", print_hash_value);
    transparent_crc(g_2648.f3, "g_2648.f3", print_hash_value);
    transparent_crc(g_2664.f0, "g_2664.f0", print_hash_value);
    transparent_crc(g_2664.f3, "g_2664.f3", print_hash_value);
    transparent_crc(g_2775, "g_2775", print_hash_value);
    transparent_crc(g_2810, "g_2810", print_hash_value);
    transparent_crc(g_2820, "g_2820", print_hash_value);
    transparent_crc_bytes (&g_2904, sizeof(g_2904), "g_2904", print_hash_value);
    transparent_crc(g_2978, "g_2978", print_hash_value);
    transparent_crc(g_3046, "g_3046", print_hash_value);
    transparent_crc(g_3069.f0, "g_3069.f0", print_hash_value);
    transparent_crc(g_3069.f1, "g_3069.f1", print_hash_value);
    transparent_crc(g_3069.f2, "g_3069.f2", print_hash_value);
    transparent_crc(g_3069.f3, "g_3069.f3", print_hash_value);
    transparent_crc(g_3069.f4, "g_3069.f4", print_hash_value);
    transparent_crc(g_3069.f5, "g_3069.f5", print_hash_value);
    transparent_crc(g_3069.f6, "g_3069.f6", print_hash_value);
    transparent_crc(g_3069.f7, "g_3069.f7", print_hash_value);
    transparent_crc(g_3246, "g_3246", print_hash_value);
    transparent_crc(g_3262, "g_3262", print_hash_value);
    transparent_crc(g_3338.f0, "g_3338.f0", print_hash_value);
    transparent_crc(g_3338.f3, "g_3338.f3", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_3340[i].f0, "g_3340[i].f0", print_hash_value);
        transparent_crc(g_3340[i].f3, "g_3340[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3342.f0, "g_3342.f0", print_hash_value);
    transparent_crc(g_3342.f1, "g_3342.f1", print_hash_value);
    transparent_crc(g_3342.f2, "g_3342.f2", print_hash_value);
    transparent_crc(g_3342.f3, "g_3342.f3", print_hash_value);
    transparent_crc(g_3342.f4, "g_3342.f4", print_hash_value);
    transparent_crc(g_3342.f5, "g_3342.f5", print_hash_value);
    transparent_crc(g_3342.f6, "g_3342.f6", print_hash_value);
    transparent_crc(g_3342.f7, "g_3342.f7", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc_bytes(&g_3403[i][j][k], sizeof(g_3403[i][j][k]), "g_3403[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_3405, sizeof(g_3405), "g_3405", print_hash_value);
    transparent_crc(g_3438, "g_3438", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_3458[i][j][k].f0, "g_3458[i][j][k].f0", print_hash_value);
                transparent_crc(g_3458[i][j][k].f3, "g_3458[i][j][k].f3", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3463.f0, "g_3463.f0", print_hash_value);
    transparent_crc(g_3463.f3, "g_3463.f3", print_hash_value);
    transparent_crc(g_3540, "g_3540", print_hash_value);
    transparent_crc(g_3554.f0, "g_3554.f0", print_hash_value);
    transparent_crc(g_3554.f1, "g_3554.f1", print_hash_value);
    transparent_crc(g_3554.f2, "g_3554.f2", print_hash_value);
    transparent_crc(g_3554.f3, "g_3554.f3", print_hash_value);
    transparent_crc(g_3554.f4, "g_3554.f4", print_hash_value);
    transparent_crc(g_3554.f5, "g_3554.f5", print_hash_value);
    transparent_crc(g_3554.f6, "g_3554.f6", print_hash_value);
    transparent_crc(g_3554.f7, "g_3554.f7", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_3571[i][j][k], "g_3571[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3597.f0, "g_3597.f0", print_hash_value);
    transparent_crc(g_3597.f1, "g_3597.f1", print_hash_value);
    transparent_crc(g_3597.f2, "g_3597.f2", print_hash_value);
    transparent_crc(g_3597.f3, "g_3597.f3", print_hash_value);
    transparent_crc(g_3597.f4, "g_3597.f4", print_hash_value);
    transparent_crc(g_3597.f5, "g_3597.f5", print_hash_value);
    transparent_crc(g_3597.f6, "g_3597.f6", print_hash_value);
    transparent_crc(g_3597.f7, "g_3597.f7", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_3646[i][j], "g_3646[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3665.f0, "g_3665.f0", print_hash_value);
    transparent_crc(g_3665.f3, "g_3665.f3", print_hash_value);
    transparent_crc(g_3746.f0, "g_3746.f0", print_hash_value);
    transparent_crc(g_3746.f3, "g_3746.f3", print_hash_value);
    transparent_crc(g_3908, "g_3908", print_hash_value);
    transparent_crc(g_3972, "g_3972", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 965
   depth: 1, occurrence: 48
XXX total union variables: 18

XXX non-zero bitfields defined in structs: 10
XXX zero bitfields defined in structs: 1
XXX const bitfields defined in structs: 3
XXX volatile bitfields defined in structs: 2
XXX structs with bitfields in the program: 86
breakdown:
   indirect level: 0, occurrence: 33
   indirect level: 1, occurrence: 26
   indirect level: 2, occurrence: 17
   indirect level: 3, occurrence: 6
   indirect level: 4, occurrence: 2
   indirect level: 5, occurrence: 2
XXX full-bitfields structs in the program: 15
breakdown:
   indirect level: 0, occurrence: 15
XXX times a bitfields struct's address is taken: 138
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 35
XXX times a single bitfield on LHS: 11
XXX times a single bitfield on RHS: 107

XXX max expression depth: 61
breakdown:
   depth: 1, occurrence: 464
   depth: 2, occurrence: 117
   depth: 3, occurrence: 7
   depth: 4, occurrence: 3
   depth: 5, occurrence: 6
   depth: 6, occurrence: 4
   depth: 7, occurrence: 2
   depth: 8, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 3
   depth: 15, occurrence: 4
   depth: 16, occurrence: 5
   depth: 17, occurrence: 8
   depth: 18, occurrence: 2
   depth: 19, occurrence: 7
   depth: 20, occurrence: 9
   depth: 21, occurrence: 6
   depth: 22, occurrence: 6
   depth: 23, occurrence: 7
   depth: 24, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 4
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 29, occurrence: 5
   depth: 30, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 2
   depth: 33, occurrence: 3
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 2
   depth: 38, occurrence: 1
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1
   depth: 43, occurrence: 1
   depth: 45, occurrence: 1
   depth: 61, occurrence: 1

XXX total number of pointers: 935

XXX times a variable address is taken: 2306
XXX times a pointer is dereferenced on RHS: 405
breakdown:
   depth: 1, occurrence: 337
   depth: 2, occurrence: 62
   depth: 3, occurrence: 4
   depth: 4, occurrence: 0
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 529
breakdown:
   depth: 1, occurrence: 504
   depth: 2, occurrence: 24
   depth: 3, occurrence: 0
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 74
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 17685

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 3076
   level: 2, occurrence: 424
   level: 3, occurrence: 201
   level: 4, occurrence: 30
   level: 5, occurrence: 17
XXX number of pointers point to pointers: 373
XXX number of pointers point to scalars: 499
XXX number of pointers point to structs: 45
XXX percent of pointers has null in alias set: 28.2
XXX average alias set size: 1.48

XXX times a non-volatile is read: 2832
XXX times a non-volatile is write: 1440
XXX times a volatile is read: 240
XXX    times read thru a pointer: 59
XXX times a volatile is write: 122
XXX    times written thru a pointer: 58
XXX times a volatile is available for access: 1.48e+04
XXX percentage of non-volatile access: 92.2

XXX forward jumps: 3
XXX backward jumps: 17

XXX stmts: 469
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 39
   depth: 2, occurrence: 56
   depth: 3, occurrence: 63
   depth: 4, occurrence: 116
   depth: 5, occurrence: 168

XXX percentage a fresh-made variable is used: 14.6
XXX percentage an existing variable is used: 85.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


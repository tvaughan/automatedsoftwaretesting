/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3729442531
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile int8_t * volatile  f0;
   const int32_t  f1;
   uint16_t  f2;
   volatile int32_t  f3;
   volatile signed f4 : 30;
};

union U1 {
   unsigned f0 : 14;
};

union U2 {
   const volatile int8_t * f0;
   int8_t * f1;
   const float  f2;
};

union U3 {
   uint32_t  f0;
   uint32_t  f1;
   int32_t  f2;
};

union U4 {
   uint32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static uint32_t g_2 = 4294967290UL;
static int32_t g_5 = 0x00F68029L;
static int32_t g_7 = (-10L);
static int32_t g_9 = (-1L);
static int32_t * volatile g_8 = &g_9;/* VOLATILE GLOBAL g_8 */
static uint16_t g_27 = 0UL;
static int8_t *g_46 = (void*)0;
static int64_t g_55 = (-8L);
static int16_t g_57 = 0x0230L;
static int32_t g_75 = 1L;
static uint32_t g_77 = 4294967292UL;
static union U2 g_84 = {0};
static uint64_t g_95 = 5UL;
static uint16_t g_105[7][10][3] = {{{0x87FBL,0xD137L,1UL},{65526UL,0xD137L,0xE246L},{8UL,0xADD1L,4UL},{65526UL,0x3EB3L,4UL},{0x87FBL,0x55D6L,0xE246L},{1UL,0x3EB3L,1UL},{1UL,0xADD1L,65526UL},{0x87FBL,0xD137L,1UL},{65526UL,0xD137L,0xE246L},{8UL,0xADD1L,4UL}},{{65526UL,0x3EB3L,4UL},{0x87FBL,0x55D6L,0xE246L},{1UL,0x3EB3L,1UL},{1UL,0xADD1L,65526UL},{0x87FBL,0xD137L,1UL},{65526UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL}},{{3UL,1UL,0UL},{65529UL,8UL,3UL},{0UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL},{3UL,1UL,0UL},{65529UL,8UL,3UL},{0UL,8UL,65526UL}},{{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL},{3UL,1UL,0UL},{65529UL,8UL,3UL},{0UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL}},{{3UL,0x7A53L,3UL},{3UL,1UL,0UL},{65529UL,8UL,3UL},{0UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL},{3UL,1UL,0UL},{65529UL,8UL,3UL}},{{0UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL},{3UL,1UL,0UL},{65529UL,8UL,3UL},{0UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL}},{{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL},{3UL,1UL,0UL},{65529UL,8UL,3UL},{0UL,8UL,65526UL},{1UL,1UL,8UL},{0UL,0x7A53L,8UL},{65529UL,0xE246L,65526UL},{3UL,0x7A53L,3UL},{3UL,1UL,0UL}}};
static uint16_t *g_104[7] = {&g_105[5][9][0],&g_105[5][9][0],&g_105[5][9][0],&g_105[5][9][0],&g_105[5][9][0],&g_105[5][9][0],&g_105[5][9][0]};
static int8_t g_107[6][7] = {{0x84L,0x1FL,0x84L,(-6L),0x84L,0x1FL,0x84L},{0x0FL,0xC8L,(-10L),0L,0L,(-10L),0xC8L},{1L,0x1FL,0x48L,0x1FL,1L,0x1FL,0x48L},{0L,0L,(-10L),0xC8L,0x0FL,0x0FL,0xC8L},{0x84L,(-6L),0x84L,0x1FL,0x84L,(-6L),0x84L},{0L,0xC8L,0xC8L,0L,0x0FL,(-10L),(-10L)}};
static uint8_t g_118 = 0x57L;
static int8_t g_148 = 8L;
static float g_149[10][7][3] = {{{0xB.A5D09Cp-47,0xA.B9C1A6p-82,(-0x1.Dp+1)},{(-0x8.Bp+1),0xA.B9C1A6p-82,0x7.886503p-72},{0x0.Fp+1,0x1.C6990Dp-95,0xA.85E5DDp-55},{0xB.5A0E2Dp+95,0x1.4p-1,0x1.4p-1},{0x1.C6990Dp-95,0x0.1p-1,(-0x7.Dp+1)},{0x5.D293FBp-90,(-0x1.9p+1),0x5.D293FBp-90},{(-0x1.Dp+1),0x0.Fp-1,(-0x1.9p+1)}},{{0x0.Fp-1,0x5.D293FBp-90,0xA.B9C1A6p-82},{0xB.F4A85Dp+58,0xC.2C7D6Ap-15,(-0x6.0p-1)},{0x0.Fp+1,(-0x1.6p-1),0x6.6p+1},{0xB.F4A85Dp+58,(-0x8.Bp+1),(-0x1.0p-1)},{0x0.Fp-1,0x0.1p-1,0x0.71A0B9p-12},{(-0x1.Dp+1),0xF.BA12C3p+76,0x0.368A9Bp+74},{0x5.D293FBp-90,0xB.A5D09Cp-47,(-0x1.9p+1)}},{{0x1.C6990Dp-95,(-0x1.Dp+1),0x9.DD727Ep+91},{0xB.5A0E2Dp+95,0x9.DD727Ep+91,0x3.A068EAp-59},{0x0.Fp+1,(-0x9.4p+1),0x5.0425FFp+92},{(-0x8.Bp+1),0xB.F4A85Dp+58,0x5.0425FFp+92},{0xB.A5D09Cp-47,0x0.1p-1,0x3.A068EAp-59},{0x0.368A9Bp+74,0x7.84E5F1p-0,0x9.DD727Ep+91},{0xA.B9C1A6p-82,0xA.3DCFAEp+93,(-0x1.9p+1)}},{{(-0x1.6p-1),0x0.368A9Bp+74,0x0.368A9Bp+74},{0x7.84E5F1p-0,0x0.Bp-1,0x0.71A0B9p-12},{0x0.Fp+1,0x7.6801D4p+53,(-0x1.0p-1)},{0x1.4p-1,0xB.5A0E2Dp+95,0x6.6p+1},{0xA.3DCFAEp+93,0x0.1p-1,(-0x6.0p-1)},{0x0.Bp-1,0xB.5A0E2Dp+95,0xA.B9C1A6p-82},{0xC.2C7D6Ap-15,0x7.6801D4p+53,(-0x1.9p+1)}},{{(-0x9.4p+1),0x0.Bp-1,0x5.D293FBp-90},{0xF.BA12C3p+76,0x0.368A9Bp+74,(-0x7.Dp+1)},{0x0.Fp+1,0xA.3DCFAEp+93,0x1.4p-1},{(-0x1.9p+1),0x7.84E5F1p-0,0xA.85E5DDp-55},{0x7.6801D4p+53,0x0.1p-1,0x7.886503p-72},{0x9.DD727Ep+91,0xB.F4A85Dp+58,(-0x1.Dp+1)},{0x9.DD727Ep+91,(-0x9.4p+1),(-0x1.9p+1)}},{{0x7.6801D4p+53,0x9.DD727Ep+91,0xC.2C7D6Ap-15},{(-0x1.9p+1),(-0x1.Dp+1),(-0x1.Ep-1)},{0x0.Fp+1,0xB.A5D09Cp-47,0x0.1p+1},{0xF.BA12C3p+76,0xF.BA12C3p+76,0x5.E26FEEp+25},{(-0x9.4p+1),0x0.1p-1,0x0.Cp-1},{0xC.2C7D6Ap-15,(-0x8.Bp+1),0x0.Bp-1},{0x0.Bp-1,(-0x1.6p-1),(-0x1.9p+1)}},{{0xA.3DCFAEp+93,0xC.2C7D6Ap-15,0x0.Bp-1},{0x1.4p-1,0x5.D293FBp-90,0x0.Cp-1},{0x0.Fp+1,0x0.Fp-1,0x5.E26FEEp+25},{0x7.84E5F1p-0,(-0x1.9p+1),0x0.1p+1},{(-0x1.6p-1),0x0.1p-1,(-0x1.Ep-1)},{0xA.B9C1A6p-82,0x1.4p-1,0xC.2C7D6Ap-15},{0x0.368A9Bp+74,0x1.C6990Dp-95,(-0x1.9p+1)}},{{0xB.A5D09Cp-47,0xA.B9C1A6p-82,(-0x1.Dp+1)},{(-0x8.Bp+1),0xA.B9C1A6p-82,0x7.886503p-72},{0x0.Fp+1,0x1.C6990Dp-95,0xA.85E5DDp-55},{0xB.5A0E2Dp+95,0x1.4p-1,0x1.4p-1},{0x1.C6990Dp-95,0x0.1p-1,(-0x7.Dp+1)},{0x5.D293FBp-90,(-0x1.9p+1),0x5.D293FBp-90},{(-0x1.Dp+1),0x0.Fp-1,(-0x1.9p+1)}},{{0x5.D293FBp-90,(-0x3.Bp-1),0x0.1p-1},{0x7.0F736Fp+90,0x1.5p+1,0x1.4p-1},{0x0.71A0B9p-12,(-0x1.Dp+1),0xB.F4A85Dp+58},{0x7.0F736Fp+90,0x9.7907AFp+37,0xB.5A0E2Dp+95},{0x5.D293FBp-90,0x0.Cp-1,(-0x1.0p-1)},{0xD.B0F719p-86,0xA.5D364Ep+25,0xE.8DDC73p+19},{(-0x3.Bp-1),0x0.Bp-1,(-0x2.9p-1)}},{{0x9.DD727Ep+91,0xD.B0F719p-86,0x0.Fp+1},{0xA.7E67F4p-24,0x0.Fp+1,0xA.85E5DDp-55},{0x0.71A0B9p-12,0xC.2C7D6Ap-15,(-0x1.9p+1)},{0x9.7907AFp+37,0x7.0F736Fp+90,(-0x1.9p+1)},{0x0.Bp-1,0x0.Cp-1,0xA.85E5DDp-55},{0xE.8DDC73p+19,0x0.3p-1,0x0.Fp+1},{0x0.1p-1,0xA.B9C1A6p-82,(-0x2.9p-1)}}};
static float g_153 = 0x1.1p+1;
static union U3 g_170 = {5UL};
static float g_189[2] = {0x1.0p+1,0x1.0p+1};
static uint64_t g_202[8][9] = {{0x5E8D5C61AD10CDFCLL,18446744073709551615UL,0x5E8D5C61AD10CDFCLL,0x4F71C144FEC2E3E7LL,18446744073709551606UL,0x4F71C144FEC2E3E7LL,0x5E8D5C61AD10CDFCLL,18446744073709551615UL,0x5E8D5C61AD10CDFCLL},{0xA65C23F69FC186F3LL,7UL,0x103B0344C086913BLL,0xA65C23F69FC186F3LL,1UL,0x103B0344C086913BLL,0x103B0344C086913BLL,1UL,0xA65C23F69FC186F3LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x4F71C144FEC2E3E7LL,0xF7CF346102277C61LL,0x4F71C144FEC2E3E7LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xA65C23F69FC186F3LL,1UL,0x103B0344C086913BLL,0x103B0344C086913BLL,1UL,0xA65C23F69FC186F3LL,0x103B0344C086913BLL,7UL,0xA65C23F69FC186F3LL},{0x5E8D5C61AD10CDFCLL,18446744073709551615UL,0x5E8D5C61AD10CDFCLL,0x4F71C144FEC2E3E7LL,18446744073709551606UL,0x4F71C144FEC2E3E7LL,0x5E8D5C61AD10CDFCLL,18446744073709551615UL,0x5E8D5C61AD10CDFCLL},{0xA65C23F69FC186F3LL,7UL,0x103B0344C086913BLL,0xA65C23F69FC186F3LL,1UL,0x103B0344C086913BLL,0x103B0344C086913BLL,1UL,0xA65C23F69FC186F3LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x4F71C144FEC2E3E7LL,0xF7CF346102277C61LL,0x4F71C144FEC2E3E7LL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xA65C23F69FC186F3LL,1UL,0x103B0344C086913BLL,0x103B0344C086913BLL,1UL,0xA65C23F69FC186F3LL,0x103B0344C086913BLL,7UL,0xA65C23F69FC186F3LL}};
static float g_233 = 0x3.74D607p-62;
static int32_t g_234[9] = {(-6L),(-1L),(-6L),(-1L),(-6L),(-1L),(-6L),(-1L),(-6L)};
static uint16_t g_236 = 0xF5C5L;
static union U1 g_241 = {0x7C7F10DEL};
static union U1 * const g_246[7][1] = {{&g_241},{&g_241},{&g_241},{&g_241},{&g_241},{&g_241},{&g_241}};
static union U1 * const *g_245 = &g_246[1][0];
static union U1 * const **g_244 = &g_245;
static const uint16_t g_253[3][4][7] = {{{2UL,0xF579L,0x5F9CL,0xA590L,0xA590L,0x5F9CL,0xF579L},{0x59B8L,0xE74EL,0x13D5L,65526UL,0xFA6DL,0x53A4L,1UL},{0UL,0x57A3L,0xE1C2L,0x98A7L,5UL,0xF579L,8UL},{1UL,0UL,0xA590L,65526UL,1UL,0xFA6DL,0x13D5L}},{{0x5F9CL,8UL,0xE74EL,0xA590L,0x57A3L,0x5355L,1UL},{0xE74EL,1UL,9UL,1UL,0x57A3L,0x57A3L,1UL},{0x6B23L,1UL,0x6B23L,0x53A4L,1UL,0x59B8L,65526UL},{0x57A3L,9UL,1UL,0xFDF3L,5UL,1UL,0UL}},{{65527UL,2UL,5UL,0x1EF2L,1UL,0x0DB5L,0xE74EL},{9UL,65535UL,0x5F9CL,0xFA6DL,0xFDF3L,0xF579L,65527UL},{65534UL,65526UL,0xE74EL,1UL,0xE1C2L,65527UL,65527UL},{65527UL,0x2113L,0x6B23L,0x2113L,65527UL,1UL,0xE74EL}}};
static union U4 g_275 = {18446744073709551615UL};
static int16_t g_284 = (-10L);
static int16_t g_286 = 0x12C6L;
static int8_t g_287[10][1] = {{0xB4L},{0x42L},{6L},{0x42L},{0xB4L},{0xB4L},{0x42L},{6L},{0x42L},{0xB4L}};
static int32_t g_294[4][5] = {{0x87EF0A5CL,(-6L),0x87EF0A5CL,(-6L),0x87EF0A5CL},{0L,0L,0L,0L,0L},{0x87EF0A5CL,(-6L),0x87EF0A5CL,(-6L),0x87EF0A5CL},{0L,0L,0L,0L,0L}};
static uint32_t g_313 = 0x5C183433L;
static const int32_t g_346 = (-2L);
static const int32_t *g_349[8] = {&g_170.f2,&g_170.f2,&g_170.f2,&g_170.f2,&g_170.f2,&g_170.f2,&g_170.f2,&g_170.f2};
static const uint16_t *g_352[9] = {&g_253[2][2][5],(void*)0,&g_253[2][2][5],(void*)0,&g_253[2][2][5],(void*)0,&g_253[2][2][5],(void*)0,&g_253[2][2][5]};
static const uint16_t **g_351 = &g_352[6];
static union U1 g_397[2][10][4] = {{{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}}},{{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}},{{0x03C5B6B4L},{0x8DDEFBBDL},{4294967295UL},{4294967295UL}},{{0x8DDEFBBDL},{0x8DDEFBBDL},{0UL},{4294967295UL}}}};
static union U1 *g_399 = &g_397[0][9][3];
static union U1 **g_398 = &g_399;
static int64_t *g_425 = &g_55;
static int64_t **g_424 = &g_425;
static union U1 ***g_491 = &g_398;
static union U1 ****g_490[1][9] = {{&g_491,&g_491,&g_491,&g_491,&g_491,&g_491,&g_491,&g_491,&g_491}};
static int16_t g_493[8] = {(-3L),7L,(-3L),7L,(-3L),7L,(-3L),7L};
static int32_t *g_509 = (void*)0;
static int32_t **g_508 = &g_509;
static uint64_t *g_579 = &g_202[1][7];
static uint64_t **g_578[2] = {&g_579,&g_579};
static union U3 g_625 = {1UL};
static uint32_t *g_651 = &g_77;
static int16_t *g_678[3] = {&g_493[7],&g_493[7],&g_493[7]};
static int16_t **g_677 = &g_678[1];
static int16_t ***g_676 = &g_677;
static volatile uint32_t g_723 = 0xA7CB8449L;/* VOLATILE GLOBAL g_723 */
static volatile uint32_t *g_722 = &g_723;
static volatile uint32_t * volatile *g_721 = &g_722;
static volatile uint32_t * volatile **g_720 = &g_721;
static volatile union U0 g_734 = {0};/* VOLATILE GLOBAL g_734 */
static volatile union U0 *g_733 = &g_734;
static uint8_t g_758 = 0xACL;
static uint32_t g_787 = 0UL;
static uint64_t g_795 = 0xB2F5E946457AD3E6LL;
static uint32_t g_829 = 1UL;
static union U0 g_879 = {0};/* VOLATILE GLOBAL g_879 */
static union U0 g_880 = {0};/* VOLATILE GLOBAL g_880 */
static union U0 g_881 = {0};/* VOLATILE GLOBAL g_881 */
static union U0 g_882[1][10] = {{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}}};
static union U0 g_883 = {0};/* VOLATILE GLOBAL g_883 */
static union U0 g_884[5] = {{0},{0},{0},{0},{0}};
static union U0 g_885[9][10] = {{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}}};
static union U0 g_886 = {0};/* VOLATILE GLOBAL g_886 */
static union U0 g_887 = {0};/* VOLATILE GLOBAL g_887 */
static union U0 g_888[10][1][7] = {{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0},{0},{0}}}};
static union U0 g_889[5] = {{0},{0},{0},{0},{0}};
static union U0 g_890 = {0};/* VOLATILE GLOBAL g_890 */
static union U0 g_891 = {0};/* VOLATILE GLOBAL g_891 */
static union U0 g_892 = {0};/* VOLATILE GLOBAL g_892 */
static union U0 g_893 = {0};/* VOLATILE GLOBAL g_893 */
static union U0 g_894 = {0};/* VOLATILE GLOBAL g_894 */
static union U0 g_895 = {0};/* VOLATILE GLOBAL g_895 */
static union U0 g_896 = {0};/* VOLATILE GLOBAL g_896 */
static union U0 g_897 = {0};/* VOLATILE GLOBAL g_897 */
static union U0 g_898 = {0};/* VOLATILE GLOBAL g_898 */
static union U0 g_899 = {0};/* VOLATILE GLOBAL g_899 */
static union U0 g_900 = {0};/* VOLATILE GLOBAL g_900 */
static union U0 g_901 = {0};/* VOLATILE GLOBAL g_901 */
static union U0 g_902 = {0};/* VOLATILE GLOBAL g_902 */
static union U0 g_903 = {0};/* VOLATILE GLOBAL g_903 */
static union U0 g_904[6] = {{0},{0},{0},{0},{0},{0}};
static union U0 g_905[2][7] = {{{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0}}};
static union U0 g_906 = {0};/* VOLATILE GLOBAL g_906 */
static union U0 g_907[7] = {{0},{0},{0},{0},{0},{0},{0}};
static union U0 g_908 = {0};/* VOLATILE GLOBAL g_908 */
static union U0 g_909[10][7][3] = {{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}},{{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}},{{0},{0},{0}}}};
static union U0 g_910 = {0};/* VOLATILE GLOBAL g_910 */
static union U0 g_911[10][8] = {{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}};
static union U0 g_912[5][6][5] = {{{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}}},{{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0}}}};
static union U0 g_913 = {0};/* VOLATILE GLOBAL g_913 */
static union U0 g_914[6][8] = {{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0}}};
static union U0 g_915 = {0};/* VOLATILE GLOBAL g_915 */
static union U0 g_916 = {0};/* VOLATILE GLOBAL g_916 */
static union U0 g_917 = {0};/* VOLATILE GLOBAL g_917 */
static union U0 g_918 = {0};/* VOLATILE GLOBAL g_918 */
static union U0 g_919 = {0};/* VOLATILE GLOBAL g_919 */
static union U0 g_920 = {0};/* VOLATILE GLOBAL g_920 */
static union U0 g_921 = {0};/* VOLATILE GLOBAL g_921 */
static union U0 g_922 = {0};/* VOLATILE GLOBAL g_922 */
static union U0 g_923 = {0};/* VOLATILE GLOBAL g_923 */
static union U0 g_924 = {0};/* VOLATILE GLOBAL g_924 */
static union U0 g_925 = {0};/* VOLATILE GLOBAL g_925 */
static union U0 g_926 = {0};/* VOLATILE GLOBAL g_926 */
static union U0 g_927 = {0};/* VOLATILE GLOBAL g_927 */
static union U0 g_928[9] = {{0},{0},{0},{0},{0},{0},{0},{0},{0}};
static union U0 g_929 = {0};/* VOLATILE GLOBAL g_929 */
static union U0 g_930 = {0};/* VOLATILE GLOBAL g_930 */
static union U0 g_931 = {0};/* VOLATILE GLOBAL g_931 */
static union U0 g_932 = {0};/* VOLATILE GLOBAL g_932 */
static union U0 g_933 = {0};/* VOLATILE GLOBAL g_933 */
static union U0 g_934 = {0};/* VOLATILE GLOBAL g_934 */
static union U0 g_935 = {0};/* VOLATILE GLOBAL g_935 */
static union U0 g_936 = {0};/* VOLATILE GLOBAL g_936 */
static union U0 g_937[6] = {{0},{0},{0},{0},{0},{0}};
static union U0 g_938 = {0};/* VOLATILE GLOBAL g_938 */
static union U0 g_939 = {0};/* VOLATILE GLOBAL g_939 */
static union U0 g_940 = {0};/* VOLATILE GLOBAL g_940 */
static union U0 g_941 = {0};/* VOLATILE GLOBAL g_941 */
static union U0 g_942 = {0};/* VOLATILE GLOBAL g_942 */
static union U0 g_943[3][10] = {{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}},{{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}}};
static union U0 g_944 = {0};/* VOLATILE GLOBAL g_944 */
static union U0 g_945 = {0};/* VOLATILE GLOBAL g_945 */
static union U0 g_946 = {0};/* VOLATILE GLOBAL g_946 */
static union U0 g_947 = {0};/* VOLATILE GLOBAL g_947 */
static union U0 g_948 = {0};/* VOLATILE GLOBAL g_948 */
static union U0 g_949 = {0};/* VOLATILE GLOBAL g_949 */
static union U0 g_950 = {0};/* VOLATILE GLOBAL g_950 */
static union U0 g_951 = {0};/* VOLATILE GLOBAL g_951 */
static union U0 g_952 = {0};/* VOLATILE GLOBAL g_952 */
static union U0 g_953 = {0};/* VOLATILE GLOBAL g_953 */
static union U0 g_954 = {0};/* VOLATILE GLOBAL g_954 */
static union U0 g_955 = {0};/* VOLATILE GLOBAL g_955 */
static union U0 g_956 = {0};/* VOLATILE GLOBAL g_956 */
static int32_t g_963[3][8][9] = {{{9L,0x69722272L,(-8L),(-9L),(-7L),1L,0L,0x638592B0L,(-9L)},{5L,0L,1L,1L,0x23B0A5A5L,0x23B0A5A5L,1L,1L,0L},{0x8643C30EL,0x7B627381L,1L,(-1L),0x23E519D0L,0L,0x7B627381L,(-2L),0L},{0x40A00F04L,0xCF42B48EL,1L,0x0B4C6502L,(-8L),0x40A00F04L,0x23B0A5A5L,0x5C6B3738L,0xA1572F0BL},{(-9L),0x7B627381L,(-4L),0x69722272L,0L,1L,0x90C37AF5L,0L,9L},{1L,0x746E1A44L,(-8L),0x0B4C6502L,0x40A00F04L,0x0B4C6502L,(-8L),0x746E1A44L,1L},{0x638592B0L,(-4L),2L,1L,(-4L),0x58DE8F19L,9L,(-7L),(-4L)},{0xFE9B6C03L,5L,0xAAEE2AE5L,0L,0L,0x888A75EEL,0x23B0A5A5L,(-1L),0L}},{{0x638592B0L,(-4L),9L,0x23E519D0L,0L,0x8643C30EL,(-8L),9L,2L},{1L,0xCF42B48EL,2L,(-8L),(-8L),2L,0xCF42B48EL,1L,0x959771FFL},{0L,1L,(-1L),(-1L),0x9A6F356AL,0L,0L,(-4L),1L},{(-5L),0x959771FFL,0xD35EF404L,0xF4140707L,(-4L),1L,0x746E1A44L,0L,0x959771FFL},{0L,0x90C37AF5L,0x605F5188L,0L,0L,1L,0xE890689FL,2L,2L},{(-8L),(-5L),0x746E1A44L,(-1L),0x746E1A44L,(-5L),(-8L),0x959771FFL,0L},{1L,0L,(-2L),(-4L),1L,(-1L),(-8L),1L,(-4L)},{0xA1572F0BL,0x746E1A44L,0x959771FFL,2L,0x958787A7L,(-1L),0x888A75EEL,0x959771FFL,1L}},{{(-4L),9L,0x23E519D0L,0L,0x8643C30EL,(-8L),9L,2L,9L},{0xF4140707L,0L,0xF5F5C991L,0xF5F5C991L,0L,0xF4140707L,0x958787A7L,0L,(-1L)},{0x58DE8F19L,0x638592B0L,9L,0x7B627381L,0L,0L,(-4L),(-4L),(-7L)},{1L,(-8L),(-1L),0xFE9B6C03L,(-1L),0x0B4C6502L,0x958787A7L,1L,0x746E1A44L},{0x72F19E9FL,(-4L),0L,1L,0x90C37AF5L,0L,9L,9L,0L},{1L,0xF4140707L,0xAAEE2AE5L,0xF4140707L,1L,(-8L),0x888A75EEL,(-1L),0xF4140707L},{9L,0x90C37AF5L,(-7L),0x23E519D0L,0xE890689FL,0L,(-8L),(-7L),2L},{(-4L),(-8L),0x888A75EEL,0xAAEE2AE5L,(-8L),(-8L),(-8L),0x746E1A44L,0xD35EF404L}}};
static union U0 g_1015[2][4][2] = {{{{0},{0}},{{0},{0}},{{0},{0}},{{0},{0}}},{{{0},{0}},{{0},{0}},{{0},{0}},{{0},{0}}}};
static uint16_t g_1116 = 0xE3D6L;
static union U2 *g_1129[3] = {&g_84,&g_84,&g_84};
static union U2 **g_1128[5] = {&g_1129[0],&g_1129[0],&g_1129[0],&g_1129[0],&g_1129[0]};
static int32_t *g_1133 = &g_963[1][0][5];
static const uint32_t g_1162 = 18446744073709551615UL;
static uint32_t **g_1177 = &g_651;
static uint32_t ***g_1176 = &g_1177;
static float g_1205 = 0x1.6p+1;
static float *g_1210 = &g_153;
static uint64_t g_1233[10][8][3] = {{{0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL},{0x7C9BC91D6C9E0031LL,0x5F42464EEDFEC0ADLL,18446744073709551609UL},{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0x7B4FFB9ECE0C0926LL},{18446744073709551609UL,0x7C9BC91D6C9E0031LL,1UL},{18446744073709551615UL,18446744073709551615UL,0x1D91632BFE87F7CFLL},{18446744073709551609UL,18446744073709551609UL,1UL},{18446744073709551615UL,18446744073709551615UL,0UL},{0x7C9BC91D6C9E0031LL,18446744073709551609UL,0x7C9BC91D6C9E0031LL}},{{0x7B4FFB9ECE0C0926LL,18446744073709551615UL,0x49CD2AE43FBF333ALL},{0x5F42464EEDFEC0ADLL,0x7C9BC91D6C9E0031LL,0x7C9BC91D6C9E0031LL},{0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL,0UL},{3UL,0x5F42464EEDFEC0ADLL,1UL},{0x49CD2AE43FBF333ALL,0x49CD2AE43FBF333ALL,0x1D91632BFE87F7CFLL},{0x5F42464EEDFEC0ADLL,3UL,1UL},{0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL},{0x7C9BC91D6C9E0031LL,0x5F42464EEDFEC0ADLL,18446744073709551609UL}},{{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0x7B4FFB9ECE0C0926LL},{18446744073709551609UL,0x7C9BC91D6C9E0031LL,1UL},{18446744073709551615UL,18446744073709551615UL,0x1D91632BFE87F7CFLL},{18446744073709551609UL,18446744073709551609UL,1UL},{18446744073709551615UL,18446744073709551615UL,0UL},{0x7C9BC91D6C9E0031LL,18446744073709551609UL,0x7C9BC91D6C9E0031LL},{0x7B4FFB9ECE0C0926LL,18446744073709551615UL,0x49CD2AE43FBF333ALL},{0x5F42464EEDFEC0ADLL,0x7C9BC91D6C9E0031LL,0x7C9BC91D6C9E0031LL}},{{0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL,0UL},{3UL,0x5F42464EEDFEC0ADLL,1UL},{0x49CD2AE43FBF333ALL,0x49CD2AE43FBF333ALL,0x1D91632BFE87F7CFLL},{0x5F42464EEDFEC0ADLL,3UL,1UL},{0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL},{0x7C9BC91D6C9E0031LL,0x5F42464EEDFEC0ADLL,18446744073709551609UL},{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0x7B4FFB9ECE0C0926LL},{18446744073709551609UL,0x7C9BC91D6C9E0031LL,1UL}},{{18446744073709551615UL,18446744073709551615UL,0x1D91632BFE87F7CFLL},{18446744073709551609UL,18446744073709551609UL,1UL},{18446744073709551615UL,18446744073709551615UL,0UL},{0x7C9BC91D6C9E0031LL,18446744073709551609UL,0x7C9BC91D6C9E0031LL},{0x7B4FFB9ECE0C0926LL,18446744073709551615UL,0x49CD2AE43FBF333ALL},{0x5F42464EEDFEC0ADLL,0x7C9BC91D6C9E0031LL,0x7C9BC91D6C9E0031LL},{0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL,0UL},{3UL,0x5F42464EEDFEC0ADLL,1UL}},{{0x49CD2AE43FBF333ALL,0x49CD2AE43FBF333ALL,0x1D91632BFE87F7CFLL},{0x5F42464EEDFEC0ADLL,3UL,1UL},{0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL},{0x7C9BC91D6C9E0031LL,0x5F42464EEDFEC0ADLL,18446744073709551609UL},{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0x7B4FFB9ECE0C0926LL},{18446744073709551609UL,0x7C9BC91D6C9E0031LL,1UL},{18446744073709551615UL,18446744073709551615UL,0x1D91632BFE87F7CFLL},{18446744073709551609UL,18446744073709551609UL,1UL}},{{18446744073709551615UL,18446744073709551615UL,0UL},{0x7C9BC91D6C9E0031LL,18446744073709551609UL,0x7C9BC91D6C9E0031LL},{0x7B4FFB9ECE0C0926LL,18446744073709551615UL,0x49CD2AE43FBF333ALL},{0x5F42464EEDFEC0ADLL,0x7C9BC91D6C9E0031LL,0x7C9BC91D6C9E0031LL},{0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL,0UL},{3UL,0x5F42464EEDFEC0ADLL,1UL},{0x49CD2AE43FBF333ALL,0x49CD2AE43FBF333ALL,0x1D91632BFE87F7CFLL},{0x5F42464EEDFEC0ADLL,3UL,1UL}},{{0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL,0x7B4FFB9ECE0C0926LL},{0x7C9BC91D6C9E0031LL,0x5F42464EEDFEC0ADLL,18446744073709551609UL},{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0x7B4FFB9ECE0C0926LL},{18446744073709551609UL,0x7C9BC91D6C9E0031LL,1UL},{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL},{0x59D570F2D0FDECA9LL,0x59D570F2D0FDECA9LL,18446744073709551609UL},{0x7B4FFB9ECE0C0926LL,0UL,0x1D91632BFE87F7CFLL},{3UL,0x59D570F2D0FDECA9LL,3UL}},{{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0UL},{0x7C9BC91D6C9E0031LL,3UL,3UL},{0UL,18446744073709551615UL,0x1D91632BFE87F7CFLL},{1UL,0x7C9BC91D6C9E0031LL,18446744073709551609UL},{0UL,0UL,0x49CD2AE43FBF333ALL},{0x7C9BC91D6C9E0031LL,1UL,1UL},{18446744073709551615UL,0UL,18446744073709551615UL},{3UL,0x7C9BC91D6C9E0031LL,0x59D570F2D0FDECA9LL}},{{0x7B4FFB9ECE0C0926LL,18446744073709551615UL,18446744073709551615UL},{0x59D570F2D0FDECA9LL,3UL,1UL},{0UL,0x7B4FFB9ECE0C0926LL,0x49CD2AE43FBF333ALL},{0x59D570F2D0FDECA9LL,0x59D570F2D0FDECA9LL,18446744073709551609UL},{0x7B4FFB9ECE0C0926LL,0UL,0x1D91632BFE87F7CFLL},{3UL,0x59D570F2D0FDECA9LL,3UL},{18446744073709551615UL,0x7B4FFB9ECE0C0926LL,0UL},{0x7C9BC91D6C9E0031LL,3UL,3UL}}};
static int32_t g_1254 = (-1L);
static union U4 *g_1262 = &g_275;
static const union U1 g_1313[6] = {{0UL},{0xC79901D2L},{0xC79901D2L},{0UL},{0xC79901D2L},{0xC79901D2L}};
static uint8_t g_1343 = 1UL;
static union U0 g_1458 = {0};/* VOLATILE GLOBAL g_1458 */
static union U0 * const g_1528 = (void*)0;
static union U0 * const *g_1527 = &g_1528;
static uint16_t g_1626[10] = {0x6107L,1UL,0x6107L,0x9A90L,0x9A90L,0x6107L,1UL,0x6107L,0x9A90L,0x9A90L};
static int32_t g_1645 = 0xC6B6F56BL;
static volatile int16_t * volatile g_1656 = (void*)0;/* VOLATILE GLOBAL g_1656 */
static volatile int16_t * volatile *g_1655 = &g_1656;
static volatile int16_t * volatile ** volatile g_1654[8] = {&g_1655,&g_1655,&g_1655,&g_1655,&g_1655,&g_1655,&g_1655,&g_1655};
static volatile int16_t * volatile ** volatile *g_1653 = &g_1654[5];
static volatile int16_t * volatile ** volatile * volatile *g_1652 = &g_1653;
static int16_t * const ****g_1714 = (void*)0;
static uint8_t g_1784 = 1UL;
static const uint32_t g_1832 = 0UL;
static uint8_t *g_1904 = &g_118;
static uint8_t **g_1903[5][10] = {{&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904},{&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904},{&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904},{&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904},{&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904,&g_1904}};
static union U0 g_1987 = {0};/* VOLATILE GLOBAL g_1987 */
static int32_t ** volatile g_1991[9] = {&g_509,&g_509,&g_509,&g_509,&g_509,&g_509,&g_509,&g_509,&g_509};
static int32_t g_2050 = 1L;
static const union U4 *g_2084[4] = {&g_275,&g_275,&g_275,&g_275};
static const union U4 **g_2083 = &g_2084[0];
static uint64_t ***g_2090[9][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
static uint64_t ****g_2089 = &g_2090[7][0];
static uint64_t *****g_2088[2] = {&g_2089,&g_2089};
static uint8_t *** const  volatile g_2091 = &g_1903[0][3];/* VOLATILE GLOBAL g_2091 */
static const uint64_t ***g_2124 = (void*)0;
static const uint64_t ****g_2123 = &g_2124;
static uint8_t *** const  volatile *g_2223 = &g_2091;
static uint8_t *** const  volatile ** volatile g_2222[9][6] = {{&g_2223,&g_2223,&g_2223,&g_2223,&g_2223,&g_2223},{&g_2223,&g_2223,&g_2223,(void*)0,(void*)0,(void*)0},{&g_2223,&g_2223,&g_2223,&g_2223,&g_2223,&g_2223},{(void*)0,&g_2223,&g_2223,(void*)0,&g_2223,(void*)0},{(void*)0,(void*)0,&g_2223,(void*)0,(void*)0,&g_2223},{(void*)0,(void*)0,&g_2223,&g_2223,&g_2223,&g_2223},{(void*)0,&g_2223,(void*)0,&g_2223,(void*)0,&g_2223},{(void*)0,(void*)0,&g_2223,&g_2223,&g_2223,&g_2223},{(void*)0,(void*)0,&g_2223,&g_2223,(void*)0,&g_2223}};
static uint8_t *** const  volatile ** volatile g_2225 = (void*)0;/* VOLATILE GLOBAL g_2225 */
static uint8_t *** const  volatile ** volatile g_2226[4] = {&g_2223,&g_2223,&g_2223,&g_2223};
static int32_t ** volatile g_2234[8] = {&g_1133,&g_1133,&g_1133,&g_1133,&g_1133,&g_1133,&g_1133,&g_1133};
static int32_t ** volatile g_2235 = (void*)0;/* VOLATILE GLOBAL g_2235 */


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static union U1  func_14(int32_t * p_15, uint32_t  p_16);
static uint16_t  func_21(uint64_t  p_22, int32_t * p_23, int8_t * p_24, uint16_t  p_25);
static int8_t  func_35(union U3  p_36, float  p_37);
static union U3  func_38(uint16_t * p_39, uint16_t * p_40, uint8_t  p_41);
static uint16_t * func_42(int32_t * p_43);
static int32_t * func_44(int8_t * p_45);
static const union U4  func_59(int32_t  p_60, int64_t  p_61, const uint16_t * p_62);
static uint16_t * func_64(uint64_t  p_65);
static uint64_t  func_66(int16_t * p_67, union U4  p_68, int32_t  p_69, int8_t * p_70);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_8 g_9 g_7
 * writes: g_5 g_7 g_9
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int8_t l_3 = 0xD6L;
    int32_t *l_4 = &g_5;
    int32_t *l_6 = &g_7;
    uint16_t *l_26 = &g_27;
    int32_t *l_962 = &g_963[2][4][5];
    float l_964[1][5][1];
    uint8_t *l_1783 = &g_1784;
    int8_t l_1785 = 0xE8L;
    uint32_t *l_1786 = &g_275.f0;
    uint32_t *l_1976 = &g_77;
    int32_t l_2023 = 1L;
    int32_t l_2026[3];
    int32_t l_2035[8];
    int16_t **l_2061[2];
    uint64_t ****l_2121[8];
    uint8_t *l_2139 = (void*)0;
    float l_2143 = 0x0.F2FCDAp-14;
    uint32_t l_2150 = 0UL;
    union U1 *l_2151 = &g_397[0][4][1];
    int8_t **l_2209[4] = {&g_46,&g_46,&g_46,&g_46};
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 1; k++)
                l_964[i][j][k] = 0x4.C19A3Ap-48;
        }
    }
    for (i = 0; i < 3; i++)
        l_2026[i] = 1L;
    for (i = 0; i < 8; i++)
        l_2035[i] = (-1L);
    for (i = 0; i < 2; i++)
        l_2061[i] = &g_678[1];
    for (i = 0; i < 8; i++)
        l_2121[i] = &g_2090[8][0];
    (*g_8) |= ((*l_6) = (g_2 , ((*l_4) = l_3)));
    return (*l_6);
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_55 g_170.f2 g_5 g_1177 g_651 g_77 g_7 g_625.f2 g_351 g_352 g_963 g_95 g_275 g_189 g_1626 g_75 g_202 g_275.f0 g_286 g_287 g_294 g_1262 g_1313.f0 g_1832 g_1176 g_579 g_1116 g_1210 g_625.f0 g_153 g_1343 g_1233 g_509 g_758 g_625
 * writes: g_2 g_1343 g_236 g_55 g_170.f2 g_202 g_95 g_104 g_153 g_149 g_189 g_57 g_118 g_284 g_286 g_287 g_170.f0 g_889.f2 g_294 g_1784 g_963 g_677 g_625.f0 g_27 g_7 g_1903 g_77 g_1233 g_509 g_1133 g_148 g_758
 */
static union U1  func_14(int32_t * p_15, uint32_t  p_16)
{ /* block id: 798 */
    const int8_t l_1787 = 4L;
    int32_t l_1788 = 0x9A4CAA7FL;
    union U4 *l_1797[2];
    union U4 *l_1798 = &g_275;
    int16_t *l_1817 = &g_57;
    float *l_1863 = &g_189[1];
    union U3 l_1864[1][10][5] = {{{{4294967294UL},{0x43B0CA54L},{0x1FEF2479L},{0x1FEF2479L},{0x43B0CA54L}},{{7UL},{4294967295UL},{0x263DCD46L},{4294967295UL},{0xC517A03FL}},{{4294967294UL},{0x43B0CA54L},{0x1FEF2479L},{0x1FEF2479L},{0x43B0CA54L}},{{7UL},{4294967295UL},{0x263DCD46L},{4294967295UL},{0xC517A03FL}},{{4294967294UL},{0x43B0CA54L},{0x1FEF2479L},{0x1FEF2479L},{0x43B0CA54L}},{{7UL},{4294967295UL},{0x263DCD46L},{4294967295UL},{0xC517A03FL}},{{4294967294UL},{0x43B0CA54L},{0x1FEF2479L},{0x1FEF2479L},{0x43B0CA54L}},{{7UL},{4294967295UL},{0x263DCD46L},{4294967295UL},{0xC517A03FL}},{{4294967294UL},{0x43B0CA54L},{0x1FEF2479L},{0x1FEF2479L},{0x43B0CA54L}},{{7UL},{4294967295UL},{0x263DCD46L},{4294967295UL},{0xC517A03FL}}}};
    int32_t l_1877 = 0xFA2550B7L;
    int32_t l_1878 = 0x0A96261CL;
    int32_t l_1879 = 0xDD7690ECL;
    int32_t l_1884 = 9L;
    int32_t l_1885[6][5] = {{0x02D41A0AL,0x02D41A0AL,0x02D41A0AL,0x02D41A0AL,0x02D41A0AL},{1L,1L,1L,1L,1L},{0x02D41A0AL,0x02D41A0AL,0x02D41A0AL,0x02D41A0AL,0x02D41A0AL},{1L,1L,1L,1L,1L},{0x02D41A0AL,0x02D41A0AL,0x02D41A0AL,0x02D41A0AL,0x02D41A0AL},{1L,1L,1L,1L,1L}};
    union U1 l_1964 = {7UL};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1797[i] = &g_275;
    l_1788 |= l_1787;
    for (g_2 = 0; (g_2 > 50); g_2++)
    { /* block id: 802 */
        const int64_t l_1791 = 0xC5411EF1B584C65BLL;
        int16_t *l_1816 = (void*)0;
        uint32_t *l_1845 = &g_625.f0;
        int32_t l_1853[5];
        uint8_t l_1869 = 8UL;
        union U4 l_1930 = {0x05DDA21DL};
        int i;
        for (i = 0; i < 5; i++)
            l_1853[i] = 0xE77FB746L;
        for (g_1343 = 0; (g_1343 <= 2); g_1343 += 1)
        { /* block id: 805 */
            union U4 **l_1799 = &l_1798;
            int32_t *l_1800[1];
            uint64_t **l_1801 = (void*)0;
            const uint16_t *l_1811 = &g_1626[0];
            int64_t l_1833 = 0L;
            union U2 l_1834 = {0};
            uint16_t *l_1848 = &g_894.f2;
            uint16_t *l_1849 = &g_895.f2;
            uint16_t *l_1850[3];
            int64_t l_1851 = 7L;
            float l_1852[2];
            union U1 *l_1953 = (void*)0;
            int i;
            for (i = 0; i < 1; i++)
                l_1800[i] = &g_75;
            for (i = 0; i < 3; i++)
                l_1850[i] = &g_930.f2;
            for (i = 0; i < 2; i++)
                l_1852[i] = 0x7.73D7DEp+68;
            for (g_236 = 0; (g_236 <= 2); g_236 += 1)
            { /* block id: 808 */
                uint8_t l_1793 = 0xE7L;
                int32_t *l_1794 = &g_170.f2;
                if (l_1791)
                    break;
                (*l_1794) = ((0xEA6FL > (((g_55 ^= (~0x9FE0DD14L)) > (l_1793 >= p_16)) | (-6L))) <= p_16);
            }
            if ((g_170.f2 &= (safe_mul_func_int8_t_s_s((l_1791 <= p_16), (l_1788 = (l_1797[0] == ((*l_1799) = l_1798)))))))
            { /* block id: 816 */
                uint16_t *l_1812 = &g_889[4].f2;
                if ((*p_15))
                    break;
                g_294[0][1] &= (((*l_1812) = ((((**g_1177) && (&g_579 != l_1801)) & (safe_lshift_func_int8_t_s_u(g_625.f2, (~(l_1791 | ((p_16 , (*g_351)) == (p_16 , (func_59((safe_sub_func_uint8_t_u_u((safe_div_func_int32_t_s_s(((l_1788 , g_963[2][4][5]) , 0L), l_1791)), 0xE5L)), p_16, l_1811) , (*g_351))))))))) == 0x32L)) < 0x3B54L);
            }
            else
            { /* block id: 820 */
                uint64_t l_1813 = 3UL;
                uint8_t *l_1818 = (void*)0;
                uint8_t *l_1819 = &g_1784;
                union U1 *****l_1831 = &g_490[0][3];
                int16_t ** const l_1835 = &l_1816;
                int16_t ***l_1836 = &g_677;
                g_963[1][4][2] = ((*g_1262) , (l_1813 == ((g_1313[5].f0 > ((((((p_16 < ((*l_1819) = (l_1788 && (l_1816 != l_1817)))) | ((safe_div_func_uint32_t_u_u(p_16, ((safe_div_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s((+(safe_mul_func_uint16_t_u_u((((safe_add_func_int32_t_s_s(((void*)0 != l_1831), l_1787)) & l_1813) >= g_1832), l_1833))), l_1787)) != 0x0A58D344L), 0x20CCL)) & 252UL))) != (***g_1176))) > p_16) > (*g_579)) != l_1813) ^ 0xE7L)) ^ l_1791)));
                (*g_1210) = (0x1.Fp-1 > ((g_1116 , (l_1834 , l_1835)) == ((*l_1836) = (void*)0)));
            }
            (*l_1863) = ((safe_add_func_float_f_f((safe_sub_func_float_f_f((l_1787 >= ((safe_add_func_float_f_f(((((*l_1845) |= (((*g_579)--) , ((((l_1845 != (**g_1176)) & (safe_mod_func_int8_t_s_s(p_16, 0x6CL))) , (l_1853[2] |= (l_1851 &= p_16))) > ((((((safe_rshift_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_uint32_t_u((((((safe_rshift_func_int16_t_s_u(l_1787, 14)) > ((0x3C669565L <= (safe_div_func_int64_t_s_s((((*l_1817) = (((l_1863 == (void*)0) > p_16) & 255UL)) == 0x12DFL), p_16))) >= p_16)) >= p_16) , 0x6210L) < p_16))), p_16)) || l_1788), l_1788)) != 8UL) != p_16) ^ l_1787) >= (-9L)) > 0x493CD7D2120CBA05LL)))) , l_1864[0][5][0]) , 0x8.Cp+1), 0x7.Dp+1)) < l_1864[0][5][0].f1)), 0x2.FC6965p-70)), (-0x5.9p-1))) , (*g_1210));
            for (g_27 = 0; (g_27 <= 2); g_27 += 1)
            { /* block id: 834 */
                uint32_t l_1868[8] = {0xC9EA0CA2L,0xC9EA0CA2L,0xC9EA0CA2L,0xC9EA0CA2L,0xC9EA0CA2L,0xC9EA0CA2L,0xC9EA0CA2L,0xC9EA0CA2L};
                uint32_t l_1870 = 0xB44E61B4L;
                int32_t l_1876 = 1L;
                int32_t l_1880 = 0xBCF88D28L;
                int32_t l_1881 = 0xE081BE7FL;
                int32_t l_1883 = 0xDC3AD712L;
                int32_t l_1886 = (-3L);
                int32_t l_1893 = 0x9042A0F1L;
                uint8_t **l_1902 = (void*)0;
                union U2 **l_1907 = &g_1129[1];
                uint16_t *l_1963[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_1963[i] = &g_236;
                l_1788 ^= (-1L);
                l_1864[0][5][0].f2 &= 0x11F1D158L;
                if (((l_1787 ^ (+(l_1869 = (safe_rshift_func_int16_t_s_s((l_1868[2] = p_16), l_1853[3]))))) && l_1870))
                { /* block id: 839 */
                    int16_t l_1872 = 0L;
                    int32_t l_1875[2];
                    uint64_t l_1887[5][2][3] = {{{18446744073709551613UL,5UL,5UL},{18446744073709551609UL,18446744073709551615UL,0x5D0305707391EAB5LL}},{{0x1869386A3E59DEEALL,18446744073709551613UL,1UL},{18446744073709551609UL,18446744073709551609UL,18446744073709551614UL}},{{18446744073709551613UL,0x1869386A3E59DEEALL,0x378CA3551DB35B6FLL},{18446744073709551615UL,18446744073709551609UL,18446744073709551615UL}},{{5UL,18446744073709551613UL,0xE2225C86807AD70ELL},{1UL,18446744073709551615UL,18446744073709551615UL}},{{0xE2225C86807AD70ELL,5UL,0x378CA3551DB35B6FLL},{18446744073709551607UL,1UL,18446744073709551614UL}}};
                    int32_t **l_1898 = &l_1800[0];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1875[i] = (-1L);
                    for (g_7 = 0; (g_7 <= 2); g_7 += 1)
                    { /* block id: 842 */
                        int16_t l_1871 = 0x7E6BL;
                        int32_t l_1873 = 0xC531FCA7L;
                        int32_t l_1874 = (-1L);
                        int32_t l_1882 = (-6L);
                        int32_t l_1890 = 0x27A8F609L;
                        int32_t l_1891 = 0L;
                        int32_t l_1892 = (-1L);
                        uint16_t l_1894 = 65535UL;
                        union U1 l_1897 = {3UL};
                        l_1887[2][1][0]--;
                        l_1894++;
                        return l_1897;
                    }
                    (*l_1898) = p_15;
                }
                else
                { /* block id: 848 */
                    const int8_t l_1908 = 0L;
                    int32_t l_1917 = 3L;
                    int64_t *l_1943 = (void*)0;
                    int64_t *l_1944 = &l_1833;
                    int32_t **l_1954[8];
                    int i, j, k;
                    for (i = 0; i < 8; i++)
                        l_1954[i] = &l_1800[0];
                    if (l_1787)
                    { /* block id: 849 */
                        uint32_t l_1899 = 0x843A7DC8L;
                        int64_t *l_1916 = (void*)0;
                        l_1899--;
                        g_1903[0][3] = l_1902;
                        l_1917 = (((safe_lshift_func_int8_t_s_u(l_1886, p_16)) == ((void*)0 == l_1907)) || (l_1908 < (((safe_div_func_uint32_t_u_u((((***g_1176) = ((((void*)0 == &g_721) ^ p_16) > ((safe_lshift_func_int8_t_s_u((l_1788 ^= ((safe_unary_minus_func_uint8_t_u((safe_add_func_uint8_t_u_u(((g_55 = (p_16 & l_1899)) && p_16), (-1L))))) != l_1868[2])), p_16)) ^ 0xA12BDD4C6885D267LL))) > p_16), p_16)) & l_1853[4]) == p_16)));
                    }
                    else
                    { /* block id: 856 */
                        union U1 l_1918 = {4294967295UL};
                        return l_1918;
                    }
                    l_1886 |= (safe_sub_func_uint16_t_u_u(l_1869, (safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((~0x0BL), (safe_div_func_uint16_t_u_u((l_1881 |= (safe_div_func_int32_t_s_s((l_1930 , (safe_lshift_func_int16_t_s_u((safe_mod_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((safe_mul_func_int16_t_s_s(l_1908, (((((safe_add_func_int8_t_s_s((safe_sub_func_int32_t_s_s((((*l_1944) = p_16) >= (g_1233[(g_1343 + 7)][(g_1343 + 1)][g_1343] ^= ((p_16 || (safe_div_func_int8_t_s_s((safe_add_func_int64_t_s_s((safe_sub_func_uint32_t_u_u((safe_add_func_uint64_t_u_u((l_1869 < ((void*)0 == l_1953)), (*g_579))), p_16)), l_1787)), p_16))) <= (*g_651)))), (*p_15))), 0xC8L)) < (*p_15)) , (void*)0) != (void*)0) != (*g_579)))) , (*p_15)) >= (**g_1177)), p_16)), 0xDDF6L)), p_16))), p_16))), 4L)))), l_1876))));
                    g_1133 = (g_509 = func_44(&g_287[6][0]));
                    l_1876 ^= (p_16 == ((safe_rshift_func_int8_t_s_u((((8L >= (l_1885[3][2] ^ ((~(safe_add_func_int8_t_s_s(((~(((0x27E777ADA3EEC66CLL ^ (((*g_509) = (safe_lshift_func_uint8_t_u_s(((-1L) ^ 1UL), 6))) , (func_38(l_1963[2], (((void*)0 == &g_1645) , (void*)0), p_16) , 0L))) > p_16) , (*g_509))) | l_1853[2]), p_16))) || l_1788))) != l_1869) & l_1885[1][3]), 1)) & l_1791));
                }
            }
        }
    }
    return l_1964;
}


/* ------------------------------------------ */
/* 
 * reads : g_244 g_245 g_246 g_795 g_7 g_9 g_425 g_55 g_625.f2 g_118 g_579 g_963 g_625.f0 g_107 g_677 g_678 g_284 g_294 g_493 g_1116 g_1128 g_424 g_148 g_1133 g_1176 g_1177 g_651 g_77 g_75 g_170.f2 g_1210 g_1262 g_1233 g_1343 g_153 g_234 g_758 g_170.f0 g_241.f0 g_625 g_398 g_399 g_733 g_286 g_253 g_202 g_287 g_241 g_346 g_84 g_397 g_1254 g_1626 g_95
 * writes: g_55 g_57 g_7 g_118 g_202 g_963 g_625.f0 g_493 g_284 g_1116 g_1262 g_148 g_170.f2 g_153 g_275 g_75 g_1343 g_189 g_294 g_758 g_170.f0 g_95 g_733 g_509 g_286 g_425 g_1133 g_1205 g_1527 g_77 g_241.f0 g_1176 g_1714 g_107
 */
static uint16_t  func_21(uint64_t  p_22, int32_t * p_23, int8_t * p_24, uint16_t  p_25)
{ /* block id: 431 */
    union U1 * const l_979[8] = {&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1]};
    int32_t *l_983 = (void*)0;
    int32_t **l_982 = &l_983;
    int32_t l_984[6] = {0xE71F1737L,0xE71F1737L,0xE71F1737L,0xE71F1737L,0xE71F1737L,0xE71F1737L};
    uint64_t **l_987 = &g_579;
    int32_t *l_988 = &g_7;
    uint16_t *l_989[2];
    int16_t l_990 = (-1L);
    union U0 *l_1014 = &g_1015[0][2][1];
    union U0 **l_1013 = &l_1014;
    int32_t l_1051 = 0xF05282DAL;
    float l_1054[6] = {0x1.Ap-1,0x0.Cp+1,0x0.Cp+1,0x1.Ap-1,0x0.Cp+1,0x0.Cp+1};
    uint16_t l_1060 = 65535UL;
    int32_t l_1087[9] = {0x410D5318L,0x410D5318L,0x410D5318L,0x410D5318L,0x410D5318L,0x410D5318L,0x410D5318L,0x410D5318L,0x410D5318L};
    uint64_t l_1106 = 18446744073709551611UL;
    const int32_t *l_1224 = (void*)0;
    int8_t l_1225 = 1L;
    const union U2 *l_1244 = &g_84;
    const union U2 * const *l_1243 = &l_1244;
    int16_t * const *l_1291 = (void*)0;
    int16_t * const **l_1290 = &l_1291;
    int16_t * const ***l_1289 = &l_1290;
    int16_t * const ****l_1288[2];
    union U1 ****l_1314 = &g_491;
    int32_t l_1456[4] = {9L,9L,9L,9L};
    uint32_t l_1469[1];
    uint32_t l_1585 = 0x0245113CL;
    uint32_t ***l_1591 = &g_1177;
    uint16_t l_1696 = 65535UL;
    float l_1733 = 0x8.2D4AFCp-51;
    uint8_t l_1736 = 6UL;
    float *l_1760 = &l_1054[5];
    uint64_t ***l_1777 = &g_578[1];
    int i;
    for (i = 0; i < 2; i++)
        l_989[i] = &g_943[0][7].f2;
    for (i = 0; i < 2; i++)
        l_1288[i] = &l_1289;
    for (i = 0; i < 1; i++)
        l_1469[i] = 0UL;
    if ((safe_rshift_func_int16_t_s_s((safe_div_func_int32_t_s_s((safe_rshift_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_u((l_990 &= (safe_mod_func_uint64_t_u_u(p_25, ((safe_sub_func_uint32_t_u_u((safe_mod_func_int16_t_s_s(((**g_244) == l_979[4]), (safe_add_func_int32_t_s_s(((*l_988) |= (((*l_982) = func_44(p_24)) != ((l_984[3] < ((l_984[0] && ((safe_div_func_uint8_t_u_u((l_987 != &g_579), g_795)) , (-1L))) > l_984[3])) , &g_234[7]))), (*p_23))))), (*p_23))) && (*g_425))))), p_22)) >= l_984[3]), g_625.f2)), 2UL)), 7)))
    { /* block id: 435 */
        uint64_t l_993 = 18446744073709551615UL;
        union U0 **l_1012 = (void*)0;
        int32_t l_1053 = 9L;
        int32_t l_1055 = 0xEE3094D6L;
        int32_t l_1056 = 0xD8834F6DL;
        int32_t l_1057 = 0x2ABC891CL;
        int32_t l_1059[1];
        union U4 l_1071 = {0x049AE6B5L};
        uint32_t l_1099 = 18446744073709551615UL;
        uint32_t **l_1175 = &g_651;
        uint32_t ***l_1174 = &l_1175;
        uint8_t l_1204[7][8] = {{0UL,0x24L,255UL,1UL,1UL,246UL,246UL,1UL},{1UL,246UL,246UL,1UL,1UL,0x82L,0xEFL,4UL},{1UL,0x51L,1UL,0x37L,0xEFL,0x59L,255UL,0UL},{4UL,0x51L,1UL,248UL,0x51L,0x82L,1UL,0x37L},{6UL,246UL,248UL,255UL,248UL,246UL,6UL,0x82L},{0x51L,0xEFL,1UL,6UL,0UL,1UL,1UL,0x1FL},{0x82L,255UL,1UL,248UL,0UL,1UL,246UL,255UL}};
        int i, j;
        for (i = 0; i < 1; i++)
            l_1059[i] = 5L;
        for (g_118 = 0; (g_118 != 42); g_118 = safe_add_func_int64_t_s_s(g_118, 7))
        { /* block id: 438 */
            uint8_t l_994 = 0xAAL;
            int32_t *l_995 = (void*)0;
            int32_t *l_996 = &g_963[2][4][5];
            int32_t l_1031 = 0x0F0FF1D9L;
            int32_t l_1050[6][5][8] = {{{0xA796FCC7L,0x8568AB98L,0xDAAEC1AAL,7L,0L,(-1L),7L,0L},{6L,0xDEFEFB40L,0x87BF8C31L,(-2L),0xA092C2F8L,0x05356498L,0L,1L},{0L,0x636B8A8DL,0x94AC20D0L,0x1170250AL,1L,0x73187438L,6L,1L},{1L,0x4FCF501AL,(-6L),0x636B8A8DL,(-6L),0xCBC1F1FDL,1L,0L},{1L,1L,0xAF5F891DL,0L,(-3L),0x87BF8C31L,0x8285E24BL,1L}},{{0xCBC1F1FDL,0L,0x1C79BE03L,0L,0L,0xD5399C4DL,0x73187438L,0L},{(-1L),0xA5A8017FL,(-7L),0xDEFEFB40L,(-1L),0x133FF8B9L,0L,2L},{0L,(-2L),0xAF5F891DL,(-6L),(-6L),0xAF5F891DL,(-2L),0L},{1L,0x8285E24BL,0x67474C24L,0L,1L,0x264D851EL,6L,0L},{1L,0x8568AB98L,(-1L),0L,0L,0x264D851EL,0x21E5B58DL,0xDAAEC1AAL}},{{1L,0x8285E24BL,0x87BF8C31L,(-3L),0L,0xAF5F891DL,1L,1L},{0xDAAEC1AAL,(-2L),0L,0x1170250AL,1L,0x133FF8B9L,0xA5A8017FL,(-2L)},{0L,0xA5A8017FL,0x8568AB98L,1L,(-6L),0xD5399C4DL,(-2L),0xCBC1F1FDL},{1L,0L,0x05356498L,0xA092C2F8L,(-2L),0x87BF8C31L,0xDEFEFB40L,6L},{0xDAAEC1AAL,1L,0L,(-1L),1L,0xCBC1F1FDL,0x73187438L,0xC3482E76L}},{{(-6L),0x4FCF501AL,(-6L),(-1L),0L,0x73187438L,1L,2L},{7L,0x636B8A8DL,0x0EBB45B9L,0L,6L,0x05356498L,1L,0xA796FCC7L},{1L,0xDEFEFB40L,0x8F338BF6L,4L,1L,4L,0x8F338BF6L,0xD5399C4DL},{(-1L),0x0EBB45B9L,7L,0x1170250AL,4L,0L,0x3AF58420L,3L},{0xCBC1F1FDL,(-1L),(-9L),0x8568AB98L,(-1L),0x8285E24BL,0x3AF58420L,6L}},{{3L,0x8568AB98L,7L,0L,(-6L),0x441AB20EL,0x8F338BF6L,(-6L)},{(-6L),0x441AB20EL,0x8F338BF6L,(-6L),(-10L),(-1L),0x8568AB98L,0xF5C0B545L},{0xA5A8017FL,(-6L),0x8285E24BL,0L,0x73187438L,(-9L),1L,0L},{0xB8028EACL,0x3AF58420L,(-10L),0xB9D2B345L,0x1170250AL,0xA7C5032CL,0xAF5F891DL,(-1L)},{0xD5399C4DL,0x441AB20EL,0xB9D2B345L,1L,5L,0x594D8EBBL,0xD5399C4DL,7L}},{{0x3AF58420L,0x73187438L,0L,0x94AC20D0L,0xCBC1F1FDL,0x8285E24BL,0x4FCF501AL,0x1C79BE03L},{(-1L),1L,0x0EBB45B9L,5L,0L,8L,0x594D8EBBL,(-1L)},{0x1170250AL,0x0EBB45B9L,0xB8028EACL,0x1C79BE03L,5L,5L,0x1C79BE03L,0xB8028EACL},{0x94AC20D0L,0x94AC20D0L,(-9L),6L,(-6L),0L,(-1L),0x73187438L},{0xB8028EACL,0x4FCF501AL,(-8L),0L,9L,0x05356498L,0x441AB20EL,0x73187438L}}};
            int32_t l_1105 = 1L;
            const union U2 *l_1127[6][4][7] = {{{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,(void*)0},{(void*)0,&g_84,&g_84,(void*)0,&g_84,&g_84,&g_84},{&g_84,&g_84,(void*)0,&g_84,&g_84,&g_84,(void*)0},{&g_84,&g_84,&g_84,&g_84,(void*)0,(void*)0,&g_84}},{{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,(void*)0},{&g_84,&g_84,&g_84,(void*)0,&g_84,&g_84,(void*)0},{&g_84,(void*)0,(void*)0,&g_84,&g_84,&g_84,&g_84},{(void*)0,&g_84,(void*)0,&g_84,(void*)0,&g_84,(void*)0}},{{&g_84,&g_84,&g_84,(void*)0,&g_84,&g_84,(void*)0},{(void*)0,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,&g_84,&g_84,(void*)0,&g_84,&g_84,(void*)0},{&g_84,(void*)0,(void*)0,&g_84,(void*)0,&g_84,&g_84}},{{(void*)0,&g_84,&g_84,&g_84,&g_84,(void*)0,&g_84},{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84},{(void*)0,&g_84,(void*)0,&g_84,(void*)0,&g_84,&g_84},{&g_84,&g_84,&g_84,(void*)0,&g_84,(void*)0,(void*)0}},{{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,&g_84,(void*)0,&g_84,(void*)0,&g_84,(void*)0},{(void*)0,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,(void*)0,&g_84,&g_84,&g_84,&g_84,&g_84}},{{(void*)0,&g_84,&g_84,(void*)0,(void*)0,(void*)0,&g_84},{&g_84,&g_84,&g_84,&g_84,(void*)0,&g_84,(void*)0},{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84}}};
            const union U2 **l_1126 = &l_1127[3][3][6];
            int64_t l_1171 = 0x5C06AB35A63DC396LL;
            uint32_t l_1180 = 18446744073709551615UL;
            const uint64_t *l_1200[10] = {&l_1106,&l_1106,&l_1106,&l_1106,&l_1106,&l_1106,&l_1106,&l_1106,&l_1106,&l_1106};
            const uint64_t **l_1199[4];
            uint64_t **l_1201 = &g_579;
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_1199[i] = &l_1200[0];
            if ((l_993 || ((p_25 ^= (0xD2199B0A44B016A3LL < ((*g_579) = (*l_988)))) < (l_993 && ((((*l_996) ^= l_994) < ((safe_lshift_func_int16_t_s_u((~(p_22 , (8UL & (safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_u((safe_add_func_uint8_t_u_u((((safe_add_func_int16_t_s_s((p_22 , (-2L)), p_22)) == 0x4AL) ^ l_993), 0xC9L)), 3)), 1))))), p_22)) | 0xE985ACAB8C529882LL)) || l_993)))))
            { /* block id: 442 */
                int8_t l_1026 = 0L;
                uint8_t l_1027 = 0xD2L;
                int32_t l_1043 = (-1L);
                int32_t l_1045 = 6L;
                int32_t l_1046 = 0xBF3132FCL;
                int32_t l_1047 = 1L;
                int32_t l_1048 = 0xFE0FB626L;
                int32_t l_1049 = (-1L);
                int32_t l_1052[9] = {(-6L),0x94B33D44L,(-6L),(-6L),0x94B33D44L,(-6L),(-6L),0x94B33D44L,(-6L)};
                union U2 *l_1075[10] = {&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84};
                union U2 **l_1074 = &l_1075[4];
                int32_t **l_1091 = &l_995;
                int32_t *l_1092 = &l_1047;
                int32_t *l_1093 = (void*)0;
                int32_t *l_1094 = &l_1053;
                int32_t *l_1095 = (void*)0;
                int32_t *l_1096 = &l_1031;
                int32_t *l_1097 = &l_1056;
                int32_t *l_1098[3][9] = {{&l_1050[1][2][0],&l_1050[3][0][0],&l_1049,&l_984[3],&l_984[3],&l_1049,&l_1050[3][0][0],&l_1050[1][2][0],&l_1050[3][0][0]},{(void*)0,(void*)0,&l_1049,&l_1049,(void*)0,(void*)0,&l_984[3],(void*)0,(void*)0},{(void*)0,&l_1050[3][0][0],&l_1050[3][0][0],(void*)0,&l_1050[1][2][0],(void*)0,&l_1050[1][2][0],(void*)0,&l_1050[3][0][0]}};
                int i, j;
                for (g_625.f0 = 0; (g_625.f0 <= 5); g_625.f0 += 1)
                { /* block id: 445 */
                    int16_t *l_1022 = &g_284;
                    int32_t l_1028 = 0xE4ED6C19L;
                    int32_t l_1058 = 1L;
                    union U2 **l_1088 = (void*)0;
                    int i, j;
                    l_1028 = (safe_mul_func_int8_t_s_s(((safe_div_func_uint32_t_u_u((l_1012 != l_1013), (l_984[g_625.f0] = g_107[g_625.f0][g_625.f0]))) , (-1L)), ((safe_lshift_func_int16_t_s_s((g_55 && (((safe_mod_func_int8_t_s_s(0x06L, (safe_div_func_int16_t_s_s(((*l_1022) |= ((**g_677) = ((*g_425) , (-4L)))), ((safe_lshift_func_int16_t_s_u((l_1026 = (+0x462C45C4L)), l_1027)) , p_25))))) > 0x22L) < 0x1DDF972BL)), 11)) <= p_25)));
                    for (l_990 = 0; (l_990 != (-5)); l_990 = safe_sub_func_int8_t_s_s(l_990, 5))
                    { /* block id: 453 */
                        int32_t l_1032[6] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
                        int32_t *l_1033 = &l_1028;
                        int32_t *l_1034 = &g_963[0][7][7];
                        int32_t *l_1035 = &g_963[2][4][5];
                        int32_t *l_1036 = (void*)0;
                        int32_t *l_1037 = &l_1028;
                        int32_t *l_1038 = &g_294[0][1];
                        int32_t *l_1039 = &l_984[g_625.f0];
                        int32_t *l_1040 = &l_984[g_625.f0];
                        int32_t *l_1041 = &g_963[2][4][5];
                        int32_t *l_1042 = &l_984[g_625.f0];
                        int32_t *l_1044[10] = {&g_9,(void*)0,&g_9,(void*)0,&g_9,(void*)0,&g_9,(void*)0,&g_9,(void*)0};
                        union U2 ***l_1067 = (void*)0;
                        union U2 *l_1070 = (void*)0;
                        union U2 **l_1069 = &l_1070;
                        union U2 ***l_1068 = &l_1069;
                        union U2 **l_1073 = &l_1070;
                        union U2 ***l_1072[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_1072[i] = &l_1073;
                        --l_1060;
                        l_1087[0] |= ((*l_1037) &= (safe_lshift_func_int16_t_s_s((safe_add_func_int64_t_s_s((((((*l_1068) = (void*)0) != (l_1071 , (l_1074 = (void*)0))) < p_25) , ((*g_425) = ((l_984[g_625.f0] < ((*l_1034) = (safe_sub_func_int16_t_s_s(((**g_677) &= (safe_mod_func_int64_t_s_s((1L | ((((((safe_sub_func_int16_t_s_s((1L <= (safe_sub_func_uint16_t_u_u((+(safe_sub_func_int16_t_s_s((p_23 == &g_234[1]), 0x2B85L))), (*l_988)))), 0xD115L)) | l_993) & (*l_1038)) | (*l_996)) , (*l_988)) != p_22)), p_25))), l_984[g_625.f0])))) || p_25))), p_22)), 10)));
                        (*l_996) = 0x3188AF12L;
                        (*l_1068) = l_1088;
                    }
                    for (l_994 = 0; (l_994 != 38); ++l_994)
                    { /* block id: 467 */
                        return p_22;
                    }
                }
                (*l_1091) = p_23;
                ++l_1099;
            }
            else
            { /* block id: 473 */
                int32_t *l_1102 = &g_963[2][3][0];
                int32_t *l_1103[5][6][8] = {{{(void*)0,&g_963[0][1][7],&g_5,&l_1059[0],&g_170.f2,&l_1050[1][2][0],&l_1087[4],&g_170.f2},{&g_963[2][0][2],&l_1059[0],&l_1087[6],&g_294[0][1],&l_1055,&g_5,&l_1059[0],&l_1031},{&l_1050[1][2][0],&l_1055,&l_984[0],&l_984[0],(void*)0,&g_9,&g_170.f2,&g_9},{(void*)0,&g_5,&g_170.f2,&g_5,&g_625.f2,&g_294[0][1],&l_1059[0],&g_75},{&l_1059[0],&l_1055,&l_984[5],&g_963[2][4][5],(void*)0,(void*)0,(void*)0,(void*)0},{&g_9,&g_7,&g_7,&g_9,&l_1059[0],&g_294[0][1],(void*)0,&g_5}},{{&g_963[1][0][2],&l_984[3],(void*)0,&g_625.f2,&g_7,&g_294[0][1],&l_1055,&l_1056},{&l_1087[0],&l_984[3],&l_984[2],(void*)0,&g_294[0][1],&g_294[0][1],&g_294[0][1],(void*)0},{(void*)0,&g_7,&l_1057,&l_984[5],(void*)0,&g_963[2][0][2],&g_294[1][4],(void*)0},{&l_1050[4][1][3],&l_1059[0],&g_963[1][0][2],&g_294[0][1],&g_7,&g_75,(void*)0,&l_1031},{&g_9,(void*)0,&g_9,&g_170.f2,&l_1055,&g_294[0][1],&l_1050[1][2][0],&g_9},{&l_1055,&g_294[0][1],&l_1059[0],&l_1055,&g_294[0][1],(void*)0,&g_625.f2,&l_1050[1][2][0]}},{{&l_1087[0],(void*)0,&g_294[0][1],(void*)0,&l_1059[0],&g_963[2][4][5],&g_963[2][4][5],&g_963[2][6][1]},{&l_1059[0],&g_9,&g_294[0][1],&g_5,&l_1050[4][1][3],&l_984[0],&l_1050[1][2][0],(void*)0},{&l_1087[4],(void*)0,&l_984[0],&l_1053,&g_963[2][3][4],&g_5,&g_75,&l_1031},{&g_5,&l_1050[1][2][0],(void*)0,&l_1087[0],&g_294[0][1],&l_1087[6],&g_9,&g_294[0][1]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1055,&l_984[0],(void*)0,&l_1055},{&l_1031,&l_1059[0],&l_1087[0],(void*)0,&l_1055,&g_170.f2,&g_963[2][4][5],&l_1087[0]}},{{(void*)0,(void*)0,&g_9,&g_7,&g_963[2][0][2],&l_984[5],&g_9,&l_1055},{&l_984[5],&l_1050[4][1][3],&g_294[0][1],&l_1050[3][0][0],&g_294[0][1],&l_1050[4][1][3],&l_984[5],&g_963[2][3][4]},{&g_75,&l_1050[1][2][0],&g_9,&g_75,&g_625.f2,&l_1087[6],&l_1050[1][2][0],&l_1055},{(void*)0,&l_1055,&g_170.f2,&l_1056,&g_625.f2,&l_984[5],&g_294[0][1],&l_1050[3][0][0]},{&g_75,&l_1087[0],&l_1087[6],&l_1055,&g_294[0][1],&g_75,&g_294[0][1],&l_984[2]},{&l_984[5],&g_9,&l_1087[0],&l_984[0],&g_963[2][0][2],&l_1059[0],&l_1087[1],&g_963[1][1][6]}},{{(void*)0,&l_1059[0],&g_294[0][1],(void*)0,&l_1055,&g_963[0][1][7],&g_5,(void*)0},{&l_1031,&l_1055,&g_7,&l_1087[6],&l_1055,&g_294[0][1],(void*)0,&g_294[1][4]},{(void*)0,&g_294[0][1],&l_1059[0],&l_1056,&g_294[0][1],&g_9,&l_1050[3][2][3],&l_1057},{&g_5,&g_294[0][1],&g_963[2][4][5],&g_963[1][0][2],&g_963[2][3][4],(void*)0,&l_1059[0],&g_963[2][4][5]},{&l_1087[4],&g_7,&g_625.f2,&g_294[1][4],&l_1050[4][1][3],(void*)0,&l_1087[0],&g_294[0][1]},{(void*)0,&g_963[1][0][2],&g_75,&l_1059[0],&g_9,(void*)0,&g_294[0][1],&l_1057}}};
                int64_t l_1104 = 0x39EDD469B2E2B823LL;
                int i, j, k;
                ++l_1106;
                for (l_1053 = 0; (l_1053 <= 8); l_1053 += 1)
                { /* block id: 477 */
                    uint8_t l_1109[10][10] = {{0xEDL,0UL,0x1FL,0x57L,255UL,0xCDL,0xEDL,255UL,0xBEL,0x57L},{0x1FL,1UL,0xBEL,0xCDL,0x6BL,0xCDL,0xBEL,1UL,0x1FL,255UL},{0xEDL,0xA5L,0UL,1UL,0x6BL,0x2CL,0xEAL,0x57L,0UL,0x57L},{0x6BL,0UL,255UL,1UL,255UL,0UL,0x6BL,255UL,0x1FL,1UL},{0x1FL,0x57L,255UL,0xCDL,0xEDL,255UL,0xBEL,0x57L,0xBEL,255UL},{0xEAL,0x57L,0UL,0x57L,0xEAL,0x2CL,0x6BL,1UL,0UL,0xA5L},{0xEAL,0UL,0xBEL,0xA5L,255UL,255UL,0xEAL,255UL,255UL,0xA5L},{0x1FL,0xA5L,0x1FL,0xCDL,0xEAL,0UL,0xBEL,0xA5L,255UL,255UL},{0x6BL,1UL,0UL,0xA5L,0xEDL,0x2CL,0xEDL,0xA5L,0UL,1UL},{0xEDL,0UL,0x1FL,0x57L,255UL,0xCDL,0xEDL,255UL,0xBEL,0x57L}};
                    int32_t l_1112 = 0xEBA05068L;
                    int32_t l_1114[10][5][5] = {{{0L,1L,0xCA755C20L,0L,0xE4536480L},{(-1L),0L,(-8L),(-4L),(-1L)},{1L,(-7L),0x802EFB3CL,0x2EFA453AL,5L},{0xE478EE62L,0x901E23F2L,0x2260EC89L,0xE478EE62L,0xDDBAB527L},{(-1L),(-4L),(-6L),0x4BF0E4E3L,(-4L)}},{{(-5L),0x627EFB10L,(-5L),0x98BF8673L,(-4L)},{1L,4L,0x745CA9B6L,0x47EEA9C6L,0L},{0xA385C955L,(-1L),0x47EEA9C6L,1L,8L},{0x3851B789L,(-4L),0xCEF267F0L,0x2EFA453AL,(-3L)},{0x2260EC89L,(-4L),(-10L),1L,0x901E23F2L}},{{0L,(-1L),8L,(-1L),(-6L)},{0L,4L,1L,0L,9L},{0x47EEA9C6L,0x627EFB10L,(-1L),(-1L),(-10L)},{(-1L),(-4L),0x6CD1EEEFL,0x79F4E52CL,0x2260EC89L},{(-10L),0x901E23F2L,(-1L),0x66389B13L,(-3L)}},{{(-4L),(-7L),9L,9L,(-7L)},{0x98BF8673L,1L,(-7L),0x275F8F20L,0x63CBCEBCL},{0x275F8F20L,(-10L),0xA385C955L,0xCA755C20L,8L},{(-1L),(-6L),0x08B0C95FL,1L,0x6CD1EEEFL},{0x275F8F20L,(-3L),(-1L),(-1L),0xBE43DD3DL}},{{0xA0CE24F8L,0xE478EE62L,(-1L),3L,0L},{0x63CBCEBCL,0x4F5D8970L,0xF5DF9264L,1L,(-4L)},{(-1L),0x39F655B2L,0xC66154C4L,(-1L),1L},{0x47EEA9C6L,0x51728E2AL,0x627EFB10L,0L,0xA0CE24F8L},{0xC66154C4L,3L,(-1L),0xB68D421CL,(-4L)}},{{0L,(-7L),(-5L),0x275F8F20L,(-10L)},{0x6CD1EEEFL,(-1L),1L,(-1L),0x95CD84D9L},{0xD4A9B6BCL,0x4F5D8970L,1L,0x08B0C95FL,0x95CD84D9L},{0x4155A4B1L,(-5L),0xED181EC3L,(-1L),(-10L)},{0xE478EE62L,(-1L),9L,(-4L),(-4L)}},{{1L,(-6L),0xAB7FDDABL,0x63CBCEBCL,0xA0CE24F8L},{(-1L),0xC42F073EL,0xF69EEAF0L,(-4L),1L},{0x745CA9B6L,(-1L),0L,8L,(-4L)},{(-1L),9L,1L,(-7L),0L},{0x2260EC89L,0xD4A9B6BCL,0xD4A9B6BCL,0x2260EC89L,0xBE43DD3DL}},{{0x745CA9B6L,1L,1L,(-6L),0x6CD1EEEFL},{1L,0x275F8F20L,(-1L),0xF69EEAF0L,8L},{8L,0x0B54E70FL,9L,(-6L),0x63CBCEBCL},{0xE478EE62L,(-4L),(-10L),0x2260EC89L,9L},{0xED181EC3L,8L,0xF5DF9264L,(-7L),0xCA755C20L}},{{0xD7AEEC2AL,1L,1L,8L,0xD4A9B6BCL},{0x6CD1EEEFL,(-4L),1L,(-4L),(-1L)},{1L,0x1F823D2FL,0x08B0C95FL,0x63CBCEBCL,0xC1B34F70L},{(-6L),0x802EFB3CL,0xBE43DD3DL,(-4L),0x39F655B2L},{0x47EEA9C6L,1L,(-3L),(-1L),0xD7AEEC2AL}},{{0x39F655B2L,0xD7AEEC2AL,0xCEF267F0L,0x08B0C95FL,0xCA755C20L},{1L,(-1L),0xCEF267F0L,(-1L),(-1L)},{0xA0CE24F8L,(-1L),(-3L),0x275F8F20L,1L},{0x802EFB3CL,(-10L),0xBE43DD3DL,0xB68D421CL,8L},{9L,0xBE43DD3DL,0x08B0C95FL,0L,0x437CE69AL}}};
                    int i, j, k;
                    if ((*p_23))
                        break;
                    for (l_1099 = 1; (l_1099 <= 8); l_1099 += 1)
                    { /* block id: 481 */
                        int32_t l_1113 = 0x5242009AL;
                        int32_t l_1115 = 1L;
                        union U1 l_1123 = {0UL};
                        int i;
                        ++l_1109[0][0];
                        g_1116++;
                        if ((*p_23))
                            continue;
                        (*l_996) = (safe_add_func_uint64_t_u_u((((safe_mul_func_int8_t_s_s((l_1087[l_1053] >= ((*p_24) >= (l_1123 , ((((*g_425) = (safe_mul_func_int16_t_s_s(((p_22 == (0x3AB8ECEBL == (l_1126 == g_1128[0]))) <= (safe_add_func_int16_t_s_s(((((l_1114[0][3][0] = (**g_424)) == p_22) || p_25) <= 0xF820L), p_25))), l_1109[0][0]))) <= p_25) <= g_118)))), (*p_24))) , (void*)0) == p_23), (*l_996)));
                    }
                }
            }
        }
    }
    else
    { /* block id: 556 */
        union U3 l_1274 = {6UL};
        int32_t l_1292 = 3L;
        int32_t l_1332[3];
        int16_t l_1358[8];
        union U2 *l_1367 = (void*)0;
        const union U1 *l_1378 = &g_397[0][9][1];
        uint8_t *l_1387 = &g_758;
        float l_1427 = 0xB.B9BB2Fp+62;
        const uint64_t l_1440[1] = {0UL};
        uint8_t l_1453 = 8UL;
        const uint64_t *l_1473 = &g_202[1][4];
        const uint64_t **l_1472 = &l_1473;
        uint16_t *l_1476 = &l_1060;
        int32_t *l_1495 = &l_984[1];
        int32_t l_1520[6][1] = {{(-1L)},{0x1F3436A6L},{(-1L)},{(-1L)},{0x1F3436A6L},{(-1L)}};
        uint32_t ***l_1589 = &g_1177;
        int16_t ***l_1630[3];
        int8_t *l_1700 = &g_107[1][3];
        int8_t l_1722 = 0x15L;
        float l_1734 = (-0x8.Dp-1);
        int32_t l_1779[6][2][3] = {{{(-1L),0L,0xCD3A1CE5L},{(-9L),(-9L),0L}},{{0L,(-1L),(-1L)},{(-1L),(-9L),(-1L)}},{{5L,0L,0xAD263C52L},{(-1L),0xFE0F03CBL,0xFE0F03CBL}},{{0xE62ECBB0L,(-1L),0xCD3A1CE5L},{0x8818847EL,(-1L),(-9L)}},{{0xE62ECBB0L,0xE62ECBB0L,0xAD263C52L},{(-1L),0x8818847EL,0L}},{{(-1L),0xE62ECBB0L,(-1L)},{0xFE0F03CBL,(-1L),0x7C09CFFFL}}};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1332[i] = 0xEDE1AE55L;
        for (i = 0; i < 8; i++)
            l_1358[i] = 0L;
        for (i = 0; i < 3; i++)
            l_1630[i] = &g_677;
        if ((*p_23))
        { /* block id: 557 */
            union U4 *l_1260 = &g_275;
            union U4 **l_1261 = &l_1260;
            g_1262 = ((*l_1261) = l_1260);
        }
        else
        { /* block id: 560 */
            union U1 ****l_1275 = &g_491;
            uint32_t l_1287 = 0xF585A0DAL;
            int32_t l_1330 = 0L;
            int32_t l_1331 = 1L;
            int32_t l_1336 = (-8L);
            int32_t l_1338 = 0L;
            int32_t l_1340 = 1L;
            int32_t l_1341 = (-7L);
            int32_t l_1342[7][8][2] = {{{(-4L),1L},{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)},{3L,(-4L)},{1L,0x3D72F10FL},{0x3D72F10FL,1L},{(-4L),3L},{(-4L),1L}},{{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)},{3L,(-4L)},{1L,0x3D72F10FL},{0x3D72F10FL,1L},{(-4L),3L},{(-4L),1L},{0x3D72F10FL,0x3D72F10FL}},{{1L,(-4L)},{3L,(-4L)},{1L,0x3D72F10FL},{0x3D72F10FL,1L},{(-4L),3L},{(-4L),1L},{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)}},{{3L,(-4L)},{1L,0x3D72F10FL},{0x3D72F10FL,1L},{(-4L),3L},{(-4L),1L},{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)},{3L,(-4L)}},{{1L,0x3D72F10FL},{0x3D72F10FL,1L},{(-4L),3L},{(-4L),1L},{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)},{3L,(-4L)},{1L,0x3D72F10FL}},{{0x3D72F10FL,1L},{(-4L),3L},{(-4L),1L},{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)},{3L,(-4L)},{1L,0x3D72F10FL},{0x3D72F10FL,1L}},{{(-4L),3L},{(-4L),1L},{0x3D72F10FL,0x3D72F10FL},{1L,(-4L)},{3L,(-4L)},{1L,0x3D72F10FL},{0x3D72F10FL,1L},{(-4L),3L}}};
            const union U1 **l_1379 = &l_1378;
            const int32_t *l_1382 = &g_7;
            int i, j, k;
            for (g_148 = 21; (g_148 > (-5)); g_148 = safe_sub_func_uint64_t_u_u(g_148, 9))
            { /* block id: 563 */
                uint32_t l_1271 = 0xBB73F0D9L;
                union U1 *****l_1276 = &l_1275;
                int32_t l_1286 = 0x4EFCE6BDL;
                union U4 l_1297[7][9][4] = {{{{0UL},{0xE9D84A8DL},{18446744073709551615UL},{0UL}},{{0UL},{18446744073709551615UL},{0xF32CE9BFL},{18446744073709551609UL}},{{0x9C7CC62BL},{18446744073709551615UL},{0x4907B568L},{1UL}},{{0xF32CE9BFL},{0UL},{1UL},{0UL}},{{0x424A5630L},{0UL},{1UL},{0UL}},{{0x7E92846CL},{1UL},{18446744073709551615UL},{0xAC77E5DAL}},{{0UL},{0UL},{0UL},{0x21F22D41L}},{{0UL},{0x23E249A8L},{18446744073709551615UL},{1UL}},{{0x7E92846CL},{0x21F22D41L},{1UL},{0xE9D84A8DL}}},{{{0x424A5630L},{1UL},{1UL},{0x4907B568L}},{{0xF32CE9BFL},{18446744073709551615UL},{0x4907B568L},{0x6573556AL}},{{0x9C7CC62BL},{0xF32CE9BFL},{0xF32CE9BFL},{0x9C7CC62BL}},{{0UL},{1UL},{18446744073709551615UL},{0x6F28E00AL}},{{0UL},{0xE25F55B8L},{0UL},{0UL}},{{1UL},{0x6573556AL},{0xF32CE9BFL},{0x424A5630L}},{{0x7E92846CL},{1UL},{18446744073709551613UL},{0x21F22D41L}},{{0x9C7CC62BL},{0UL},{18446744073709551615UL},{0xF32CE9BFL}},{{4UL},{0xE25F55B8L},{1UL},{0UL}}},{{{0UL},{0UL},{0UL},{0x6573556AL}},{{0xAC77E5DAL},{0x9C7CC62BL},{0xAC77E5DAL},{0UL}},{{18446744073709551615UL},{0UL},{0x9C7CC62BL},{18446744073709551615UL}},{{0x6F28E00AL},{0xE9D84A8DL},{0UL},{0UL}},{{18446744073709551609UL},{1UL},{0UL},{0x6F28E00AL}},{{0x6F28E00AL},{0UL},{0x9C7CC62BL},{0x4907B568L}},{{18446744073709551615UL},{0x7E92846CL},{0xAC77E5DAL},{18446744073709551615UL}},{{0xAC77E5DAL},{18446744073709551615UL},{0UL},{0UL}},{{0UL},{18446744073709551609UL},{1UL},{1UL}}},{{{4UL},{4UL},{18446744073709551615UL},{0x7E92846CL}},{{0x9C7CC62BL},{0UL},{18446744073709551613UL},{0UL}},{{0x7E92846CL},{0x4907B568L},{0xF32CE9BFL},{18446744073709551613UL}},{{18446744073709551615UL},{0x4907B568L},{1UL},{0UL}},{{0x4907B568L},{0UL},{18446744073709551609UL},{0x7E92846CL}},{{1UL},{4UL},{0xE25F55B8L},{1UL}},{{0xF32CE9BFL},{18446744073709551609UL},{0x6573556AL},{0UL}},{{0xE25F55B8L},{18446744073709551615UL},{18446744073709551615UL},{18446744073709551615UL}},{{18446744073709551613UL},{0x7E92846CL},{0UL},{0x4907B568L}}},{{{1UL},{0UL},{0UL},{0x6F28E00AL}},{{18446744073709551615UL},{1UL},{0UL},{0UL}},{{18446744073709551615UL},{0xE9D84A8DL},{0UL},{18446744073709551615UL}},{{1UL},{0UL},{0UL},{0UL}},{{18446744073709551613UL},{0x9C7CC62BL},{18446744073709551615UL},{0x6573556AL}},{{0xE25F55B8L},{0UL},{0x6573556AL},{0UL}},{{0xF32CE9BFL},{0xE25F55B8L},{0xE25F55B8L},{0xF32CE9BFL}},{{1UL},{0UL},{18446744073709551609UL},{0x21F22D41L}},{{0x4907B568L},{1UL},{1UL},{0x424A5630L}}},{{{18446744073709551615UL},{0x6573556AL},{0xF32CE9BFL},{0x424A5630L}},{{0x7E92846CL},{1UL},{18446744073709551613UL},{0x21F22D41L}},{{0x9C7CC62BL},{0UL},{18446744073709551615UL},{0xF32CE9BFL}},{{4UL},{0xE25F55B8L},{1UL},{0UL}},{{0UL},{0UL},{0UL},{0x6573556AL}},{{0xAC77E5DAL},{0x9C7CC62BL},{0xAC77E5DAL},{0UL}},{{18446744073709551615UL},{0UL},{0x9C7CC62BL},{18446744073709551615UL}},{{0x6F28E00AL},{0xE9D84A8DL},{0UL},{0UL}},{{18446744073709551609UL},{1UL},{0UL},{0x6F28E00AL}}},{{{0x6F28E00AL},{0UL},{0x9C7CC62BL},{0x4907B568L}},{{18446744073709551615UL},{0x7E92846CL},{0xAC77E5DAL},{18446744073709551615UL}},{{0xAC77E5DAL},{18446744073709551615UL},{0UL},{0UL}},{{0UL},{18446744073709551609UL},{1UL},{1UL}},{{4UL},{4UL},{18446744073709551615UL},{0x7E92846CL}},{{0x9C7CC62BL},{0UL},{18446744073709551613UL},{0UL}},{{0x7E92846CL},{0x4907B568L},{0xF32CE9BFL},{18446744073709551613UL}},{{18446744073709551615UL},{0x4907B568L},{1UL},{0UL}},{{0x6573556AL},{1UL},{1UL},{1UL}}}};
                int32_t l_1307 = (-1L);
                const union U1 *l_1312[6][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,&g_1313[5]},{&g_1313[1],&g_1313[5],&g_1313[5],&g_1313[5],&g_1313[5]},{&g_1313[5],(void*)0,&g_1313[5],&g_1313[5],&g_1313[5]},{&g_1313[3],&g_1313[5],&g_1313[3],&g_1313[5],&g_1313[5]},{&g_1313[0],(void*)0,&g_1313[5],(void*)0,&g_1313[0]},{&g_1313[3],&g_1313[1],&g_1313[5],&g_1313[5],&g_1313[5]}};
                const union U1 **l_1311 = &l_1312[5][4];
                const union U1 ***l_1310 = &l_1311;
                const union U1 ****l_1309 = &l_1310;
                int32_t l_1334 = 1L;
                int32_t l_1335 = 1L;
                int32_t l_1339 = (-8L);
                union U2 l_1356 = {0};
                int i, j, k;
                l_1292 ^= (safe_add_func_int16_t_s_s(0x2685L, (safe_lshift_func_int8_t_s_u(((safe_add_func_int64_t_s_s(l_1271, ((*l_988) != (safe_sub_func_uint16_t_u_u(((((((((p_25 = p_25) , (((((*l_1276) = (l_1274 , l_1275)) == &g_244) != ((safe_mod_func_int8_t_s_s((safe_lshift_func_int8_t_s_u(((((!(safe_sub_func_int16_t_s_s(0x6C39L, (l_1287 = (safe_div_func_int16_t_s_s((l_1286 &= (**g_677)), l_1271)))))) , l_1286) && l_1274.f2) > 255UL), p_25)), l_1274.f2)) || 0xE3023930F58C3992LL)) , (void*)0)) == l_1288[1]) ^ p_22) , p_25) && 0x48B6L) & (*g_1133)) & 0x70F50DF9806D6F96LL), p_22))))) & (***g_1176)), g_75))));
                (*g_1133) = l_1292;
                for (g_170.f2 = (-11); (g_170.f2 != 23); g_170.f2 = safe_add_func_uint8_t_u_u(g_170.f2, 2))
                { /* block id: 572 */
                    union U1 ***l_1304 = (void*)0;
                    float *l_1305 = &l_1054[1];
                    union U2 l_1306 = {0};
                    const union U1 *****l_1308 = (void*)0;
                    int32_t l_1333 = (-5L);
                    int32_t l_1337[4] = {0x1EDCCA0CL,0x1EDCCA0CL,0x1EDCCA0CL,0x1EDCCA0CL};
                    union U0 **l_1357 = &l_1014;
                    int i;
                    g_75 |= ((l_1274.f0 , (l_1309 = ((((p_22 ^ (safe_lshift_func_uint16_t_u_u(((((((*g_1210) = p_22) > (((*g_1262) = l_1297[1][6][1]) , (l_1286 >= ((safe_mul_func_float_f_f(l_1286, ((6L <= ((*g_425) = (((*g_1133) ^= l_1271) > ((l_1307 = (safe_mul_func_uint8_t_u_u((((safe_mul_func_float_f_f((((*l_1305) = ((((((((void*)0 == l_1304) != p_22) == (-6L)) , 0x31L) , (-1L)) , p_22) != p_22)) != 0x1.3p+1), 0x6.24AA50p+10)) , l_1306) , l_1292), 7L))) <= l_1297[1][6][1].f0)))) , g_1233[8][5][2]))) < (-0x1.5p+1))))) , 0x8D760B6EL) != (*p_23)) < 0x7C7A0B30E8866604LL), 10))) == l_1274.f1) == (***g_1176)) , (void*)0))) == l_1314);
                    for (l_1060 = 9; (l_1060 < 26); l_1060 = safe_add_func_uint16_t_u_u(l_1060, 2))
                    { /* block id: 583 */
                        int32_t *l_1317 = &l_1274.f2;
                        int32_t *l_1318 = &l_1274.f2;
                        int32_t *l_1319 = &l_984[4];
                        int32_t *l_1320 = &g_294[0][1];
                        int32_t *l_1321 = &g_1254;
                        int32_t *l_1322 = &g_294[2][4];
                        int32_t *l_1323 = &l_984[3];
                        int32_t *l_1324 = (void*)0;
                        int32_t *l_1325 = &g_75;
                        int32_t *l_1326 = &l_1087[0];
                        int32_t *l_1327 = &l_1307;
                        int32_t *l_1328 = &l_1087[8];
                        int32_t *l_1329[9][3][3] = {{{&l_1274.f2,&g_9,&l_1274.f2},{&g_294[2][0],&l_1286,(void*)0},{(void*)0,(void*)0,(void*)0}},{{&l_1307,&l_1286,&l_1286},{(void*)0,&g_9,&g_963[0][4][3]},{&l_1307,(void*)0,&l_1307}},{{(void*)0,(void*)0,&g_963[0][4][3]},{&g_294[2][0],&g_294[2][0],&l_1286},{&l_1274.f2,(void*)0,(void*)0}},{{&l_1286,(void*)0,(void*)0},{&l_1274.f2,&g_9,&l_1274.f2},{&g_294[2][0],&l_1286,(void*)0}},{{(void*)0,(void*)0,(void*)0},{&l_1307,&l_1286,&l_1286},{(void*)0,&g_9,&g_963[0][4][3]}},{{&l_1307,(void*)0,&l_1307},{(void*)0,(void*)0,&g_963[0][4][3]},{&g_294[2][0],&g_294[2][0],&l_1286}},{{&l_1274.f2,(void*)0,(void*)0},{&l_1286,(void*)0,(void*)0},{&l_1274.f2,&g_9,&l_1274.f2}},{{&g_294[2][0],&l_1286,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_1307,&l_1286,&l_1286}},{{(void*)0,&g_9,&g_963[0][4][3]},{&l_1307,(void*)0,&l_1307},{(void*)0,(void*)0,&g_963[0][4][3]}}};
                        float *l_1359[10][8][1] = {{{&g_189[1]},{&g_149[0][6][1]},{&g_189[1]},{&g_153},{(void*)0},{(void*)0},{&g_149[0][6][1]},{&g_149[4][5][2]}},{{(void*)0},{(void*)0},{&g_149[1][4][1]},{&g_189[1]},{&g_149[8][3][0]},{&g_189[1]},{&g_189[1]},{&g_149[0][6][1]}},{{(void*)0},{&g_189[1]},{(void*)0},{&g_149[0][6][1]},{&g_189[1]},{&g_189[1]},{&g_149[8][3][0]},{&g_189[1]}},{{&g_149[1][4][1]},{(void*)0},{(void*)0},{&g_149[4][5][2]},{&g_149[0][6][1]},{(void*)0},{(void*)0},{&g_153}},{{&g_189[1]},{&g_149[0][6][1]},{&g_189[1]},{&g_1205},{(void*)0},{&g_1205},{&g_189[1]},{&g_149[0][6][1]}},{{&g_189[1]},{&g_153},{(void*)0},{(void*)0},{&g_149[0][6][1]},{&g_149[4][5][2]},{(void*)0},{(void*)0}},{{&g_149[1][4][1]},{&g_189[1]},{&g_149[8][3][0]},{&g_189[1]},{&g_189[1]},{&g_149[0][6][1]},{(void*)0},{&g_189[1]}},{{(void*)0},{&g_149[0][6][1]},{&g_189[1]},{&g_189[1]},{&g_149[8][3][0]},{&g_189[1]},{&g_149[1][4][1]},{(void*)0}},{{(void*)0},{&g_149[4][5][2]},{&g_149[0][6][1]},{(void*)0},{(void*)0},{&g_153},{&g_189[1]},{&g_149[0][6][1]}},{{(void*)0},{(void*)0},{&g_233},{(void*)0},{(void*)0},{&g_149[0][6][1]},{(void*)0},{&g_189[1]}}};
                        int i, j, k;
                        g_1343--;
                        (*l_988) = (safe_div_func_float_f_f((l_1332[1] = (*g_1210)), (((0x1.299E7Dp+31 > ((*l_1305) = ((*g_1210) >= (safe_div_func_float_f_f(0x9.43830Dp-21, (-0x8.1p+1)))))) < (g_189[1] = (l_1307 = (safe_div_func_float_f_f((safe_mul_func_float_f_f(((*l_1323) = (safe_mul_func_float_f_f((*g_1210), ((l_1356 , 65528UL) , ((((l_1357 == (void*)0) >= l_1358[5]) >= 0x4.Fp+1) > p_22))))), 0x7.Ep-1)), (*g_1210)))))) >= p_25)));
                    }
                    if (l_1342[1][1][1])
                        break;
                    if ((*p_23))
                        continue;
                }
                for (g_118 = 8; (g_118 < 38); g_118 = safe_add_func_uint32_t_u_u(g_118, 5))
                { /* block id: 597 */
                    const union U1 l_1364 = {4294967295UL};
                    int32_t l_1373 = 0x481B3A54L;
                    g_294[3][3] ^= (safe_div_func_int16_t_s_s(((((l_1364 , (((l_1331 = (((((*g_1133) = (*p_23)) , (safe_sub_func_float_f_f(((*l_988) = ((*g_1210) = (((void*)0 == l_1367) < (*g_1210)))), (((safe_unary_minus_func_int16_t_s((l_1373 = ((**g_677) &= (safe_sub_func_int8_t_s_s((l_1339 = ((((((*g_1133) = ((0x380B3981L < (l_1332[1] >= 0x3C8657AFF885D241LL)) & (safe_div_func_int64_t_s_s(((*p_24) , p_22), p_25)))) >= 0x862FEC2BL) > p_25) >= 65526UL) >= l_1335)), l_1340)))))) , l_1274.f0) > l_1364.f0)))) > p_22) == g_234[1])) , (*g_425)) & (*g_425))) , &g_118) != (void*)0) & l_1364.f0), p_22));
                    for (g_170.f2 = (-10); (g_170.f2 != (-3)); ++g_170.f2)
                    { /* block id: 609 */
                        (*g_1133) ^= ((*l_988) = (*p_23));
                    }
                    for (g_758 = 6; (g_758 > 31); g_758 = safe_add_func_uint16_t_u_u(g_758, 9))
                    { /* block id: 615 */
                        return p_22;
                    }
                }
            }
            (*l_1379) = l_1378;
            for (g_170.f0 = 0; (g_170.f0 <= 8); g_170.f0 += 1)
            { /* block id: 623 */
                for (g_95 = 3; (g_95 <= 8); g_95 += 1)
                { /* block id: 626 */
                    int32_t **l_1380 = (void*)0;
                    int32_t **l_1381 = (void*)0;
                    l_1382 = (void*)0;
                    if (g_795)
                        goto lbl_1410;
                }
            }
        }
        (*l_988) = l_1292;
lbl_1410:
        if ((safe_div_func_int16_t_s_s((l_1292 &= (safe_mul_func_uint8_t_u_u((((*p_24) & g_241.f0) , (g_625 , ((*l_1387) = p_22))), (safe_rshift_func_int16_t_s_u((((*g_398) != (**g_244)) < ((**g_677) | ((safe_div_func_uint16_t_u_u(l_1332[2], p_25)) && l_1332[1]))), p_22))))), (**g_677))))
        { /* block id: 634 */
            volatile union U0 **l_1392[6][3] = {{&g_733,(void*)0,&g_733},{&g_733,&g_733,&g_733},{&g_733,&g_733,&g_733},{&g_733,(void*)0,&g_733},{&g_733,&g_733,&g_733},{&g_733,&g_733,&g_733}};
            int32_t **l_1393 = &g_509;
            int i, j;
            l_988 = p_23;
            g_733 = g_733;
            (*l_1393) = func_44(&g_148);
        }
        else
        { /* block id: 638 */
            uint32_t l_1397 = 4294967291UL;
            union U4 * const *l_1406 = &g_1262;
            for (g_286 = 10; (g_286 < 5); g_286--)
            { /* block id: 641 */
                int32_t *l_1396[8][1] = {{&l_1332[1]},{(void*)0},{&l_1332[1]},{(void*)0},{&l_1332[1]},{(void*)0},{&l_1332[1]},{(void*)0}};
                union U4 * const **l_1407 = &l_1406;
                int i, j;
                l_1397--;
                (*g_1133) &= (safe_mod_func_int8_t_s_s((p_22 < (safe_div_func_int32_t_s_s(((((safe_mul_func_int8_t_s_s((p_22 & ((&g_1262 != ((*l_1407) = l_1406)) <= 1UL)), ((*l_1387) = (g_1343 |= g_253[0][3][0])))) >= (safe_add_func_int8_t_s_s((&l_989[0] != &g_104[5]), 0x1AL))) || (**g_1177)) & 1UL), 1UL))), (*p_24)));
                return p_22;
            }
        }
        if ((((*g_1210) = l_1332[1]) , 0x5CBFC5EFL))
        { /* block id: 652 */
            int64_t *l_1425 = (void*)0;
            union U2 l_1426[1] = {{0}};
            int64_t l_1428 = 0L;
            int32_t l_1429 = (-1L);
            int64_t *l_1430 = (void*)0;
            int64_t *l_1431[10] = {(void*)0,&g_55,&g_55,&g_55,(void*)0,(void*)0,&g_55,&g_55,&g_55,(void*)0};
            int32_t l_1432 = (-1L);
            const int16_t *l_1447[8][8] = {{&g_284,&l_1358[2],&g_284,(void*)0,&l_990,&l_990,(void*)0,&g_284},{&l_1358[2],&l_1358[2],&l_990,&l_1358[0],&l_1358[6],&l_1358[0],&l_990,&l_1358[2]},{&l_1358[2],&g_284,(void*)0,&l_990,&l_990,(void*)0,&g_284,&l_1358[2]},{&g_284,&g_286,&l_1358[2],&l_1358[0],&l_1358[2],&g_286,&g_284,&g_284},{&g_286,&l_1358[0],(void*)0,(void*)0,&l_1358[0],&g_286,&l_990,&g_286},{&l_1358[0],&g_286,&l_990,&g_286,&l_1358[0],(void*)0,(void*)0,&l_1358[0]},{&g_286,&g_284,&g_284,&g_286,&l_1358[2],&l_1358[0],&l_1358[2],&g_286},{&g_284,&l_1358[2],&g_284,(void*)0,&l_990,&l_990,(void*)0,&g_284}};
            const int16_t **l_1446 = &l_1447[1][1];
            uint32_t l_1450 = 6UL;
            int32_t *l_1451 = &l_1332[0];
            int32_t *l_1452[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            union U0 *l_1457 = &g_1458;
            int i, j;
            if ((l_1432 = ((safe_sub_func_uint32_t_u_u(((safe_mod_func_int64_t_s_s((g_55 ^= (safe_add_func_uint16_t_u_u((((*p_23) == ((*g_398) == (void*)0)) == 0xB8837CF77C45DEA2LL), (l_1429 = ((safe_add_func_uint32_t_u_u((((*p_24) = (safe_div_func_uint64_t_u_u((((((**g_677) = (safe_sub_func_uint64_t_u_u(0x40C612DE76C576A8LL, (safe_rshift_func_uint8_t_u_u(((((*g_424) = l_1425) == (l_1426[0] , &g_55)) != ((void*)0 != &p_22)), g_202[6][3]))))) | l_1358[0]) != l_1428) , p_22), l_1358[5]))) ^ 1UL), 0xA9068ABBL)) <= l_1274.f1))))), 0xF6C4904B62B0A533LL)) >= g_625.f2), (*g_1133))) || l_1274.f0)))
            { /* block id: 659 */
                return (*l_988);
            }
            else
            { /* block id: 661 */
                int32_t * const l_1433 = (void*)0;
                int32_t **l_1434 = &g_1133;
                uint32_t l_1437[8] = {1UL,18446744073709551609UL,1UL,18446744073709551609UL,1UL,18446744073709551609UL,1UL,18446744073709551609UL};
                int16_t **l_1448 = &g_678[1];
                union U3 *l_1449[9] = {&l_1274,&l_1274,&l_1274,&l_1274,&l_1274,&l_1274,&l_1274,&l_1274,&l_1274};
                int i;
                (*l_1434) = l_1433;
                l_1429 ^= (((-2L) && (l_1432 < (safe_lshift_func_int16_t_s_s(((l_1437[5] <= ((l_1274 = func_38(&g_1116, &g_1116, (((safe_mul_func_uint8_t_u_u(p_22, l_1440[0])) < ((~((((safe_rshift_func_uint16_t_u_s(0x4C8FL, 13)) ^ (safe_div_func_int8_t_s_s((0x05L == l_1292), 2UL))) , l_1446) == l_1448)) & p_22)) | p_25))) , 0x9BA5L)) > 0x3AL), 5)))) | l_1450);
            }
            ++l_1453;
            l_1456[3] ^= (0xF5L & p_22);
            (*l_1013) = l_1457;
        }
        else
        { /* block id: 669 */
            union U4 *l_1468 = &g_275;
            int32_t *l_1477 = &l_1274.f2;
            const union U2 * const l_1484 = (void*)0;
            union U0 * const *l_1525[10][7] = {{(void*)0,(void*)0,(void*)0,&l_1014,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1014,&l_1014,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1014,&l_1014,&l_1014,&l_1014,&l_1014,&l_1014,&l_1014},{&l_1014,&l_1014,&l_1014,&l_1014,&l_1014,(void*)0,&l_1014},{&l_1014,&l_1014,(void*)0,&l_1014,&l_1014,&l_1014,(void*)0},{(void*)0,(void*)0,&l_1014,&l_1014,&l_1014,&l_1014,(void*)0},{(void*)0,(void*)0,&l_1014,&l_1014,(void*)0,(void*)0,&l_1014},{&l_1014,&l_1014,&l_1014,(void*)0,&l_1014,&l_1014,&l_1014},{&l_1014,(void*)0,&l_1014,&l_1014,(void*)0,&l_1014,(void*)0},{&l_1014,&l_1014,&l_1014,&l_1014,&l_1014,(void*)0,&l_1014}};
            uint64_t **l_1602 = &g_579;
            int16_t ***l_1639 = &g_677;
            union U1 l_1657 = {0x9EEAE656L};
            int32_t l_1666 = (-10L);
            int32_t *l_1671 = &l_1087[2];
            int32_t *l_1672 = &l_1520[1][0];
            int32_t *l_1673[3];
            uint64_t l_1674 = 5UL;
            int16_t * const ****l_1713 = &l_1289;
            int8_t l_1730 = 0L;
            uint32_t l_1764 = 0x435B469EL;
            float l_1778 = (-0x3.7p-1);
            union U3 *l_1782 = &g_170;
            union U3 **l_1781 = &l_1782;
            int i, j;
            for (i = 0; i < 3; i++)
                l_1673[i] = &g_1254;
            if ((p_25 >= ((((*g_1210) , ((~4294967292UL) , ((safe_lshift_func_uint16_t_u_s(p_22, (safe_add_func_int8_t_s_s(((safe_rshift_func_int8_t_s_s(((((*g_425) = ((safe_rshift_func_int8_t_s_s(g_286, ((g_241.f0 == (((void*)0 == l_1468) <= p_22)) == (*g_425)))) > 0UL)) || (*g_579)) , g_287[2][0]), l_1469[0])) <= p_25), (*p_24))))) > 0xB2L))) && l_1440[0]) > (-4L))))
            { /* block id: 671 */
                uint64_t l_1488 = 18446744073709551607UL;
                int32_t **l_1489 = &g_1133;
                (*g_1133) = (safe_mod_func_int16_t_s_s(((void*)0 != l_1472), 0xF8C5L));
                g_1205 = (safe_div_func_float_f_f(((*g_1210) = ((func_38(l_1476, &g_27, p_22) , (safe_add_func_int64_t_s_s((safe_mod_func_uint64_t_u_u(((safe_mod_func_int64_t_s_s((l_1484 != (*l_1243)), (safe_unary_minus_func_uint8_t_u((safe_add_func_int16_t_s_s(((void*)0 != &g_1133), 1L)))))) | (*l_988)), 0x5CBA49DE7F66668ELL)), 0UL))) , l_1488)), l_1332[1]));
                (*l_1489) = (void*)0;
            }
            else
            { /* block id: 676 */
                union U1 *** const *l_1509 = &g_491;
                int32_t l_1519 = 1L;
                int32_t l_1563 = 6L;
                int64_t l_1625 = 1L;
                int32_t l_1662 = 1L;
                if ((*g_1133))
                { /* block id: 677 */
                    union U1 *** const **l_1510 = &l_1509;
                    int16_t ***l_1542 = (void*)0;
                    int32_t l_1545 = 1L;
                    uint64_t l_1565 = 0x2C5039F70EEBF3A6LL;
                    int32_t **l_1586 = (void*)0;
                    int32_t **l_1587 = (void*)0;
                    int32_t **l_1588 = &g_509;
                    for (g_758 = 10; (g_758 > 34); g_758++)
                    { /* block id: 680 */
                        int32_t **l_1494[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_1494[i] = &l_1477;
                        (*g_1210) = (*g_1210);
                        l_1495 = ((safe_rshift_func_uint16_t_u_u(0xD538L, p_22)) , func_44(&g_287[3][0]));
                    }
                    if ((((safe_rshift_func_int8_t_s_s((safe_add_func_uint16_t_u_u(p_25, (+0x635CAD7EL))), (p_25 < (safe_mod_func_int8_t_s_s(((*p_24) |= (safe_div_func_int8_t_s_s((safe_mod_func_int64_t_s_s(0xCD2256A47271D8CELL, (safe_div_func_int32_t_s_s(((&g_244 == ((*l_1510) = l_1509)) > (*l_1477)), (*p_23))))), (safe_rshift_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u(((((**g_245) , p_22) <= (*l_1495)) , (-1L)), g_287[2][0])), l_1519)) | (*g_579)), 11)), l_1520[3][0]))))), 0x0BL))))) && 1UL) , 0x98A9CCC4L))
                    { /* block id: 686 */
                        union U0 * const **l_1526[4];
                        int32_t l_1546[4][2][7] = {{{1L,0xDD8BCE9CL,0x49C3CFFDL,0x228E8AE8L,0xF7C8C974L,0x8E298302L,0x8E298302L},{0xDD8BCE9CL,0x2619B081L,0x83CE4C7BL,0x2619B081L,0xDD8BCE9CL,0x8E298302L,(-8L)}},{{(-4L),1L,0x1A6B266FL,0xF7C8C974L,0x49C3CFFDL,0xFCC39C19L,1L},{0xD730F22CL,7L,(-9L),(-5L),5L,0xD730F22CL,(-8L)}},{{(-4L),0xF7C8C974L,0xD730F22CL,0x1A6B266FL,(-8L),0x1A6B266FL,0xD730F22CL},{0xDD8BCE9CL,0xDD8BCE9CL,0xD730F22CL,3L,(-8L),0L,0x8E298302L}},{{1L,0x49C3CFFDL,(-9L),(-8L),0xDD8BCE9CL,0x9841DDE0L,0xF7C8C974L},{0xF7676264L,5L,0x1A6B266FL,0x49C3CFFDL,(-8L),(-1L),5L}}};
                        int16_t l_1547 = 0x85ABL;
                        uint32_t *l_1548[5] = {&g_170.f1,&g_170.f1,&g_170.f1,&g_170.f1,&g_170.f1};
                        int32_t l_1549[3][4][3] = {{{0xE11EC4E9L,0x67F490BDL,0xFCFF1AE7L},{0x02E5A302L,0x67F490BDL,(-5L)},{0xB9EC6E9CL,0xE11EC4E9L,0xFCFF1AE7L},{0xB9EC6E9CL,0xB9EC6E9CL,0L}},{{0x02E5A302L,0xE11EC4E9L,0L},{0xE11EC4E9L,0x67F490BDL,0xFCFF1AE7L},{0x02E5A302L,0x67F490BDL,(-5L)},{0xB9EC6E9CL,0xE11EC4E9L,0xFCFF1AE7L}},{{0xB9EC6E9CL,0xB9EC6E9CL,0L},{0x02E5A302L,0xE11EC4E9L,0L},{0xE11EC4E9L,0x67F490BDL,0xFCFF1AE7L},{0x02E5A302L,0x67F490BDL,(-5L)}}};
                        int16_t l_1550 = 0x6DD9L;
                        uint32_t l_1564[9][9][3] = {{{18446744073709551615UL,18446744073709551615UL,0UL},{0x3D758CABL,0xC29C23DCL,0x93F47F60L},{0x191ED083L,0x3862ADB4L,18446744073709551613UL},{0x3AD209EDL,0x205B5E04L,0xE6AF65EBL},{0x8B82ADF4L,18446744073709551610UL,18446744073709551615UL},{2UL,0x73D9EA9EL,0xF8B74937L},{0UL,18446744073709551615UL,0xA9168D1AL},{0xFFD02562L,0xE4C63ACFL,0x843AC6F4L},{0x9E186B53L,0x8B82ADF4L,1UL}},{{0x827EFF94L,1UL,0x14F31917L},{18446744073709551615UL,0x825EDE61L,0x0F035472L},{0UL,0xDEA26A61L,0UL},{0xC29C23DCL,0x61A56507L,0xE4C63ACFL},{0UL,18446744073709551614UL,0x3D758CABL},{1UL,7UL,0x6C889182L},{0x3C9C6008L,1UL,0xFFD02562L},{0x3C9C6008L,0x17886399L,0UL},{1UL,0x72185BD5L,18446744073709551615UL}},{{0UL,0x3AD209EDL,7UL},{0xC29C23DCL,0x0F035472L,18446744073709551607UL},{0UL,0x65DF5D49L,0x58BB6BD2L},{18446744073709551615UL,0x191ED083L,2UL},{0x827EFF94L,0x10E0A2D6L,0x9E186B53L},{0x9E186B53L,18446744073709551613UL,1UL},{0xFFD02562L,0xAB6B33DAL,0x3AD209EDL},{0UL,2UL,1UL},{2UL,0xB34465A4L,18446744073709551613UL}},{{18446744073709551613UL,0x9E186B53L,1UL},{0xAF8D18E4L,1UL,1UL},{0xE8023A6FL,0x825EDE61L,1UL},{0xDA45CB1BL,0xD42B7737L,0xBB61FF36L},{0x3D758CABL,0x142620D7L,0xF8B74937L},{0x825EDE61L,0x8B82ADF4L,0x3C9C6008L},{0x055693FEL,0x142620D7L,5UL},{2UL,0xD42B7737L,0x6C889182L},{1UL,0x825EDE61L,0xE4276DFFL}},{{0xC03EF40BL,1UL,0x98EE57A5L},{0x0F035472L,0x9E186B53L,18446744073709551613UL},{0xFFD02562L,18446744073709551609UL,0x825EDE61L},{0x0BE665CAL,0xE4C63ACFL,0UL},{0x58BB6BD2L,0x93F47F60L,1UL},{0x17886399L,0xE4276DFFL,0xC03EF40BL},{0x843AC6F4L,18446744073709551615UL,18446744073709551615UL},{1UL,0xE8023A6FL,0x7C597C89L},{18446744073709551615UL,0x055693FEL,0xE8023A6FL}},{{0x142620D7L,0x63396BBFL,18446744073709551615UL},{3UL,0xAF8D18E4L,18446744073709551610UL},{0UL,0x1143DE96L,0xC0A9F727L},{0x5A966FF7L,1UL,0x8B82ADF4L},{7UL,0x0BE665CAL,0x8B82ADF4L},{0xA9168D1AL,0x3862ADB4L,0xC0A9F727L},{18446744073709551614UL,1UL,18446744073709551610UL},{0UL,0x30488554L,18446744073709551615UL},{0x7E5C8646L,7UL,0xE8023A6FL}},{{18446744073709551615UL,0xE6AF65EBL,0x7C597C89L},{0x193BCE84L,0x3AD209EDL,18446744073709551615UL},{0x93F47F60L,18446744073709551613UL,0xC03EF40BL},{0x2A2D714DL,0xB002B930L,1UL},{0xD101A045L,0x3C9C6008L,0UL},{0xB277A2A4L,0UL,0x825EDE61L},{0x8B82ADF4L,1UL,18446744073709551613UL},{0UL,18446744073709551615UL,0x98EE57A5L},{0xE4276DFFL,18446744073709551607UL,0xE4276DFFL}},{{1UL,0x73D9EA9EL,0x6C889182L},{1UL,18446744073709551615UL,5UL},{0xB34465A4L,2UL,0x3C9C6008L},{18446744073709551607UL,0x2A2D714DL,0xF8B74937L},{0xB34465A4L,0xB277A2A4L,0xBB61FF36L},{1UL,0x17886399L,1UL},{1UL,0xC03EF40BL,1UL},{0xE4276DFFL,0x7C597C89L,1UL},{0UL,18446744073709551610UL,0x142620D7L}},{{0x8B82ADF4L,0x193BCE84L,9UL},{0xB277A2A4L,18446744073709551607UL,0xAF8D18E4L},{0xD101A045L,9UL,18446744073709551615UL},{0x2A2D714DL,18446744073709551613UL,0x1CDC2F7BL},{0x93F47F60L,4UL,0xE4C63ACFL},{0x193BCE84L,3UL,0xD42B7737L},{18446744073709551615UL,0x1CDC2F7BL,0x42C8480BL},{0x7E5C8646L,18446744073709551614UL,0x3862ADB4L},{0UL,0xC29C23DCL,0UL}}};
                        uint16_t **l_1580 = &l_989[1];
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                            l_1526[i] = &l_1525[8][1];
                        l_1549[1][0][2] ^= (((safe_sub_func_uint32_t_u_u(((safe_div_func_int64_t_s_s(((*l_988) & (&g_733 != (g_1527 = (l_1525[8][4] = l_1525[8][4])))), p_22)) , (++(***g_1176))), ((g_241.f0 ^= ((safe_unary_minus_func_int8_t_s((safe_lshift_func_uint8_t_u_s((((*p_24) ^= (safe_div_func_uint8_t_u_u(((safe_add_func_int32_t_s_s(((((((0x4318AFB0490718C5LL <= ((safe_mod_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s((((l_1542 == (void*)0) != ((*l_1477) &= ((**g_677) = 0x3554L))) , (safe_lshift_func_uint16_t_u_u(((*p_23) > 0xA6ED9E58L), l_1545))), p_22)), 0x22L)) >= 0L)) & p_22) <= l_1519) && l_1546[2][0][4]) == 0L) > p_22), (*p_23))) >= g_346), l_1546[2][0][4]))) & p_25), 5)))) > l_1547)) | p_25))) , (*g_425)) , 0x0EB1B480L);
                        (*g_1210) = ((-0x1.8p-1) != ((l_1550 != l_1546[2][0][4]) >= ((0UL > (safe_mul_func_int16_t_s_s(((*g_1133) != ((safe_lshift_func_uint16_t_u_s((*l_1477), ((--(*l_1387)) & 0x4CL))) != (((*l_1476) = (((**l_1243) , (safe_mul_func_int8_t_s_s(((((*g_425) = (safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((l_1563 || (**g_1177)) == (*p_23)), 0x7CL)), p_22))) , p_22) == (*l_1495)), (*p_24)))) , l_1564[8][3][0])) || l_1565))), 0xD86AL))) , (*g_1210))));
                        l_1585 = (safe_sub_func_float_f_f((safe_mul_func_float_f_f(0x9.61D979p+31, (((*l_1378) , (safe_mod_func_uint16_t_u_u(l_1546[2][0][3], (((safe_mul_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u(((((safe_lshift_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((((func_38(&g_105[5][9][0], ((*l_1580) = &p_25), g_148) , ((**g_677) <= ((safe_mod_func_int32_t_s_s((safe_mul_func_int8_t_s_s((*p_24), g_625.f0)), p_22)) == (*l_1477)))) < (*p_24)) >= p_25), p_22)), (*l_1477))) > l_1545) != l_1565) | (***g_1176)), p_25)) ^ p_25), 4L)) , p_25) , 0x54E3L)))) , (-0x8.9p-1)))), 0xE.8DEF7Fp+5));
                    }
                    else
                    { /* block id: 701 */
                        return p_22;
                    }
                    (*l_1588) = &l_1087[0];
                }
                else
                { /* block id: 705 */
                    uint32_t ****l_1590 = &g_1176;
                    uint8_t **l_1599 = &l_1387;
                    uint64_t ***l_1603 = (void*)0;
                    uint64_t ***l_1604 = (void*)0;
                    uint64_t ***l_1605[1];
                    uint8_t *l_1610[8][5] = {{&g_1343,&g_1343,&g_1343,&g_1343,&g_118},{&g_758,(void*)0,&g_1343,(void*)0,(void*)0},{(void*)0,&g_1343,(void*)0,&g_1343,(void*)0},{&g_1343,(void*)0,&g_118,(void*)0,&g_1343},{&g_1343,&g_1343,(void*)0,&g_758,&g_758},{(void*)0,&g_118,(void*)0,(void*)0,&g_1343},{(void*)0,(void*)0,&g_1343,&l_1453,&g_758},{&g_1343,(void*)0,(void*)0,(void*)0,(void*)0}};
                    int32_t l_1627 = 9L;
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_1605[i] = (void*)0;
                    l_1591 = ((*l_1590) = l_1589);
                    (*l_1495) ^= (safe_add_func_uint64_t_u_u((+((safe_rshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u((((p_22 &= (*g_579)) != p_25) >= p_25), (1L <= (((*l_1599) = &g_758) != (((*g_1133) = (safe_rshift_func_uint8_t_u_u(g_118, 7))) , (((l_1602 = l_1602) == ((((safe_mul_func_uint8_t_u_u(((g_1343 = p_25) == ((safe_sub_func_uint8_t_u_u((((safe_add_func_int64_t_s_s((safe_rshift_func_int8_t_s_u((safe_add_func_uint32_t_u_u(((****l_1590) = (safe_add_func_uint64_t_u_u((((safe_mod_func_uint64_t_u_u((((safe_lshift_func_uint16_t_u_u((((0x3.622637p-0 == 0x9.948A73p-6) > p_25) , p_25), l_1625)) | (-9L)) != p_25), 0xBD0801BACF623E3ALL)) < p_25) || 0x7724L), 0x6BFB52F389584919LL))), p_25)), g_1254)), (*l_1477))) && 0L) >= g_1626[0]), p_25)) >= 0x9EA2L)), p_25)) ^ p_25) , (**g_424)) , (void*)0)) , &g_1343)))))), p_25)) , l_1627)), p_25));
                    if ((safe_rshift_func_int16_t_s_s(((void*)0 == l_1630[0]), 2)))
                    { /* block id: 715 */
lbl_1631:
                        l_1378 = (void*)0;
                    }
                    else
                    { /* block id: 717 */
                        if (g_625.f0)
                            goto lbl_1631;
                    }
                }
                for (g_148 = (-16); (g_148 == (-27)); g_148 = safe_sub_func_int16_t_s_s(g_148, 1))
                { /* block id: 723 */
                    uint32_t l_1634 = 0x940221CCL;
                    int16_t l_1635[1][2][9] = {{{7L,0xFF94L,0xFF94L,7L,0xFF94L,0xFF94L,7L,0xFF94L,0xFF94L},{7L,0xFF94L,0xFF94L,7L,0xFF94L,0xFF94L,7L,0xFF94L,0xFF94L}}};
                    int32_t l_1664 = (-1L);
                    int32_t l_1665 = (-7L);
                    int32_t l_1667 = 0xB35C9291L;
                    int i, j, k;
                }
            }
            ++l_1674;
            for (g_170.f2 = 28; (g_170.f2 <= 16); g_170.f2 = safe_sub_func_uint32_t_u_u(g_170.f2, 8))
            { /* block id: 747 */
                float *l_1679[9] = {&g_189[1],&g_189[1],&g_189[1],&g_189[1],&g_189[1],&g_189[1],&g_189[1],&g_189[1],&g_189[1]};
                uint32_t l_1697[3][4] = {{0xFDAA055CL,0xFDAA055CL,0xFDAA055CL,0xFDAA055CL},{0xFDAA055CL,0xFDAA055CL,0xFDAA055CL,0xFDAA055CL},{0xFDAA055CL,0xFDAA055CL,0xFDAA055CL,0xFDAA055CL}};
                int16_t ****l_1719 = &l_1630[0];
                int16_t *****l_1718 = &l_1719;
                int32_t l_1720 = 0x65835CE5L;
                int32_t l_1721 = 7L;
                int32_t l_1723 = (-6L);
                int32_t l_1724[4];
                uint8_t **l_1755[9][8] = {{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387},{&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387,&l_1387}};
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1724[i] = 0L;
                g_1205 = ((((*l_1495) = (((*g_1210) = 0x0.9p+1) == 0x4.6D00ACp-34)) != (safe_sub_func_float_f_f(0x2.FC521Bp-12, (safe_add_func_float_f_f((p_25 < (p_22 < (+0x0.9D4371p+40))), 0x5.DE507Cp-78))))) >= (-0x1.Dp+1));
                for (g_284 = 0; (g_284 == 9); g_284 = safe_add_func_uint16_t_u_u(g_284, 9))
                { /* block id: 753 */
                    int8_t **l_1701[3];
                    int32_t **l_1702[1][5][7] = {{{&l_1477,&g_509,&g_1133,&g_509,&l_1477,&l_1495,&l_1495},{&l_1495,&g_1133,(void*)0,&g_1133,&l_1495,&l_1477,&l_1477},{&l_1477,&g_509,&g_1133,&g_509,&l_1477,&l_1495,&l_1495},{&l_1495,&g_1133,(void*)0,&g_1133,&l_1495,&l_1477,&l_1477},{&l_1477,&g_509,&g_1133,&g_509,&l_1477,&l_1495,&l_1495}}};
                    const union U1 l_1711 = {0xCDDC2431L};
                    float l_1717[8] = {0x1.6p+1,0x1.6p+1,0x1.6p+1,0x1.6p+1,0x1.6p+1,0x1.6p+1,0x1.6p+1,0x1.6p+1};
                    uint64_t l_1725 = 18446744073709551606UL;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_1701[i] = &l_1700;
                    (*l_1671) |= ((((safe_add_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_u(((~((void*)0 != (*l_982))) == (0x6049L | (((safe_rshift_func_int16_t_s_s(0xEEF1L, 1)) && (255UL <= (1L && 0UL))) || l_1696))), g_1626[0])) && (-6L)) == 0x972BD013L), 0xF4L)), l_1697[1][1])) & g_241.f0) & (*g_425)) > (*g_579));
                    l_1224 = ((--(***g_1176)) , func_44((p_24 = l_1700)));
                    (*l_1671) &= (safe_lshift_func_uint8_t_u_s((safe_div_func_int32_t_s_s((safe_add_func_int64_t_s_s(p_25, ((*l_988) > (safe_sub_func_int16_t_s_s((*l_1495), 1L))))), ((l_1711 , (!((((((18446744073709551615UL | ((g_1714 = l_1713) != (l_1718 = ((safe_sub_func_uint64_t_u_u(0x25B5D9FA549CC362LL, ((((*g_425) ^= ((*l_1495) || p_22)) == (*l_1672)) == p_22))) , (void*)0)))) != l_1697[1][1]) , l_1697[0][1]) >= (***g_1176)) == (*l_988)) && 1L))) & 0x2FFAL))), 7));
                    ++l_1725;
                }
                for (l_1292 = (-9); (l_1292 == (-12)); --l_1292)
                { /* block id: 766 */
                    float l_1731 = 0xA.2F94C7p+22;
                    int32_t l_1732[10][4] = {{(-7L),0L,(-7L),0xDDFAB463L},{(-7L),0xDDFAB463L,0xDDFAB463L,(-7L)},{1L,0xDDFAB463L,(-4L),0xDDFAB463L},{0xDDFAB463L,0L,(-4L),(-4L)},{1L,1L,0xDDFAB463L,(-4L)},{(-7L),0L,(-7L),0xDDFAB463L},{(-7L),(-7L),(-7L),(-4L)},{0xDDFAB463L,(-7L),0L,(-7L)},{(-7L),1L,0L,0L},{0xDDFAB463L,0xDDFAB463L,(-7L),0L}};
                    int32_t l_1735 = 0xB17EA038L;
                    int i, j;
                    l_1736++;
                    (*l_1495) &= (safe_sub_func_uint16_t_u_u(0x090BL, ((**g_677) ^= (-1L))));
                }
                if ((safe_mul_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_u(0x2660L, (((*l_1700) = (safe_rshift_func_uint16_t_u_s((((p_22 <= 0UL) == (((**l_1602) = (((safe_add_func_int64_t_s_s((safe_mul_func_int16_t_s_s(((****l_1719) = (safe_sub_func_uint16_t_u_u((((l_1755[2][4] != (void*)0) ^ ((***l_1589)--)) , (p_22 & ((safe_mul_func_int16_t_s_s((((((p_23 != (l_1760 = p_23)) , (safe_mul_func_uint16_t_u_u(p_22, (**g_677)))) , (*l_1671)) , l_1697[1][1]) > p_22), 3L)) < (**g_1177)))), 0x9D96L))), (-1L))), (-6L))) || (*p_24)) ^ p_22)) != l_1724[2])) | l_1720), 8))) , p_25))) , 0x35L), l_1724[0])))
                { /* block id: 776 */
                    int32_t l_1763[1][4][10] = {{{0xF5B43F48L,0L,0x2F585278L,0x2F585278L,0L,0xF5B43F48L,0x08D4350FL,0xF5B43F48L,0L,0x2F585278L},{(-7L),0x9B0876F8L,(-7L),0x2F585278L,0x08D4350FL,0x08D4350FL,0x2F585278L,(-7L),0x9B0876F8L,(-7L)},{(-7L),0xF5B43F48L,0x9B0876F8L,0L,0x9B0876F8L,0xF5B43F48L,(-7L),(-7L),0xF5B43F48L,0x9B0876F8L},{0xF5B43F48L,(-7L),(-7L),0xF5B43F48L,0x9B0876F8L,0L,0x9B0876F8L,0xF5B43F48L,(-7L),(-7L)}}};
                    int i, j, k;
                    --l_1764;
                }
                else
                { /* block id: 778 */
                    union U4 **l_1780 = &g_1262;
                    for (l_1674 = 0; (l_1674 <= 0); l_1674 += 1)
                    { /* block id: 781 */
                        uint64_t ***l_1774 = &g_578[1];
                        uint64_t ****l_1775 = (void*)0;
                        uint64_t ****l_1776 = &l_1774;
                        int i, j;
                        (*l_1477) &= (((*g_425) = ((!l_1469[l_1674]) || (!((*l_1387) = (safe_unary_minus_func_uint64_t_u(((safe_lshift_func_int8_t_s_s((((l_1520[(l_1674 + 4)][l_1674] & 0x548FF99232F404E9LL) && ((*g_425) < ((l_1777 = ((*l_1776) = l_1774)) != (void*)0))) , (*l_988)), ((void*)0 == &g_578[1]))) > l_1779[5][1][2]))))))) >= 0x90BA491E94542C9FLL);
                        if ((*p_23))
                            break;
                    }
                    (*l_1780) = &g_275;
                }
            }
            (*l_1781) = &g_170;
        }
    }
    return (*l_988);
}


/* ------------------------------------------ */
/* 
 * reads : g_148 g_787 g_284 g_795 g_170.f2 g_2 g_95 g_202 g_234 g_349 g_625.f0 g_241.f0 g_625.f1 g_425 g_55 g_286 g_7 g_829 g_189 g_294 g_651 g_77 g_84 g_720 g_721 g_397.f0 g_104
 * writes: g_148 g_55 g_57 g_509 g_787 g_284 g_625.f2 g_758 g_170.f2 g_149 g_2 g_95 g_493 g_75 g_234 g_829 g_189 g_236 g_105 g_294
 */
static int8_t  func_35(union U3  p_36, float  p_37)
{ /* block id: 334 */
    uint64_t *l_768 = &g_202[4][6];
    uint64_t * const l_769[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t **l_773 = &g_104[3];
    int32_t *l_775 = (void*)0;
    int32_t *l_776 = (void*)0;
    int32_t *l_777 = &g_75;
    int32_t *l_778 = &g_170.f2;
    int32_t *l_779 = &g_75;
    int32_t *l_780 = (void*)0;
    int32_t *l_781 = &g_170.f2;
    int32_t *l_782 = &g_294[2][3];
    int32_t *l_783 = (void*)0;
    int32_t *l_784[6];
    float l_785[5];
    int8_t l_786 = 0x71L;
    union U2 l_858 = {0};
    uint32_t l_861 = 0xD7C96955L;
    int16_t ****l_863 = &g_676;
    uint32_t **l_869[8][1] = {{&g_651},{&g_651},{&g_651},{&g_651},{&g_651},{&g_651},{&g_651},{&g_651}};
    union U0 * const l_878[6][9][4] = {{{(void*)0,(void*)0,(void*)0,&g_891},{&g_913,(void*)0,&g_882[0][4],&g_884[3]},{(void*)0,&g_909[1][6][1],&g_955,&g_882[0][4]},{(void*)0,&g_909[1][6][1],(void*)0,&g_884[3]},{&g_909[1][6][1],(void*)0,(void*)0,&g_891},{&g_918,(void*)0,&g_897,&g_907[2]},{&g_938,&g_949,(void*)0,&g_929},{(void*)0,&g_893,&g_947,&g_883},{&g_913,&g_926,&g_901,(void*)0}},{{&g_953,&g_887,&g_942,&g_882[0][4]},{(void*)0,&g_931,&g_909[1][6][1],&g_926},{&g_887,(void*)0,(void*)0,&g_903},{(void*)0,(void*)0,(void*)0,&g_886},{&g_926,&g_949,&g_929,&g_955},{(void*)0,&g_940,&g_922,&g_949},{&g_913,&g_938,&g_922,&g_951},{(void*)0,(void*)0,&g_929,&g_882[0][4]},{&g_926,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_913},{&g_887,(void*)0,&g_909[1][6][1],&g_942},{(void*)0,&g_949,&g_942,&g_895},{&g_953,(void*)0,&g_901,&g_936},{&g_913,&g_951,&g_947,(void*)0},{(void*)0,&g_918,(void*)0,&g_882[0][4]},{&g_938,&g_897,&g_897,&g_938},{&g_918,&g_884[3],(void*)0,&g_924},{&g_909[1][6][1],&g_953,(void*)0,(void*)0}},{{(void*)0,&g_949,&g_955,(void*)0},{(void*)0,&g_953,&g_882[0][4],&g_924},{&g_913,&g_884[3],(void*)0,&g_938},{(void*)0,&g_897,&g_907[2],&g_940},{&g_931,&g_903,&g_913,&g_887},{&g_883,&g_931,&g_945,(void*)0},{&g_913,(void*)0,&g_903,&g_882[0][4]},{(void*)0,&g_929,&g_947,&g_922},{&g_920,&g_899,(void*)0,&g_895}},{{&g_895,(void*)0,&g_893,(void*)0},{&g_880,&g_936,&g_882[0][4],&g_940},{(void*)0,&g_924,&g_949,&g_931},{&g_936,&g_918,&g_945,&g_929},{&g_936,&g_920,&g_949,&g_934},{(void*)0,&g_929,&g_882[0][4],&g_947},{&g_880,&g_945,&g_893,&g_886},{&g_895,&g_887,(void*)0,&g_909[1][6][1]},{&g_920,&g_913,&g_947,&g_940}},{{(void*)0,&g_949,&g_903,&g_897},{&g_913,&g_909[1][6][1],&g_945,&g_942},{&g_883,&g_880,&g_913,&g_912[0][5][4]},{&g_931,&g_929,(void*)0,(void*)0},{(void*)0,(void*)0,&g_953,&g_955},{&g_895,&g_897,&g_940,(void*)0},{(void*)0,&g_891,&g_934,&g_940},{&g_887,&g_891,&g_936,(void*)0},{&g_891,&g_897,&g_945,&g_955}}};
    uint32_t l_959 = 1UL;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_784[i] = &g_170.f2;
    for (i = 0; i < 5; i++)
        l_785[i] = 0x0.8p+1;
    for (g_148 = (-18); (g_148 > 26); g_148 = safe_add_func_uint32_t_u_u(g_148, 9))
    { /* block id: 337 */
        uint64_t *l_766 = &g_202[0][5];
        uint64_t **l_767[4] = {&g_579,&g_579,&g_579,&g_579};
        int32_t *l_770 = (void*)0;
        int32_t *l_771 = (void*)0;
        int32_t **l_772 = &g_509;
        int32_t l_774 = 0L;
        int i;
        p_36.f2 = ((l_768 = l_766) == l_769[3]);
        (*l_772) = func_44(&g_148);
        l_774 = ((l_773 = l_773) == (void*)0);
        (*l_772) = &g_294[2][2];
    }
    --g_787;
    for (g_148 = 28; (g_148 > 11); g_148 = safe_sub_func_int64_t_s_s(g_148, 5))
    { /* block id: 348 */
        uint32_t l_792 = 0xDE6C006EL;
        l_792++;
        return p_36.f0;
    }
    for (g_284 = 4; (g_284 >= 1); g_284 -= 1)
    { /* block id: 354 */
        union U3 l_822 = {0UL};
        union U1 l_847[7][7][5] = {{{{0xC236E6ECL},{4294967295UL},{0x0E747ACFL},{4294967295UL},{0xC236E6ECL}},{{4294967290UL},{0x6BAB4CB9L},{2UL},{4UL},{4294967290UL}},{{7UL},{4294967295UL},{4294967289UL},{4294967295UL},{7UL}},{{4294967290UL},{4UL},{2UL},{0x6BAB4CB9L},{4294967290UL}},{{0xC236E6ECL},{4294967295UL},{0x0E747ACFL},{4294967295UL},{0xC236E6ECL}},{{4294967290UL},{0x6BAB4CB9L},{2UL},{4UL},{4294967290UL}},{{7UL},{4294967295UL},{4294967289UL},{4294967295UL},{7UL}}},{{{4294967290UL},{4UL},{2UL},{0x6BAB4CB9L},{4294967290UL}},{{0xC236E6ECL},{4294967295UL},{0x0E747ACFL},{4294967295UL},{0xC236E6ECL}},{{4294967290UL},{0x6BAB4CB9L},{2UL},{4UL},{4294967290UL}},{{7UL},{4294967295UL},{4294967289UL},{4294967295UL},{7UL}},{{4294967290UL},{4UL},{2UL},{0x6BAB4CB9L},{4294967290UL}},{{0xC236E6ECL},{4294967295UL},{0x0E747ACFL},{4294967295UL},{0xC236E6ECL}},{{4294967290UL},{0x6BAB4CB9L},{2UL},{4UL},{4294967290UL}}},{{{7UL},{4294967295UL},{4294967289UL},{4294967295UL},{7UL}},{{4294967290UL},{4UL},{2UL},{0x6BAB4CB9L},{4294967290UL}},{{0xC236E6ECL},{4294967295UL},{0x0E747ACFL},{4294967295UL},{0xC236E6ECL}},{{4294967290UL},{0x6BAB4CB9L},{2UL},{4UL},{4294967290UL}},{{7UL},{4294967295UL},{4294967289UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}}},{{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}},{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}},{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}},{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}}},{{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}},{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}},{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}},{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}},{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}}},{{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}},{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}},{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}},{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}}},{{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}},{{0x6BAB4CB9L},{0UL},{4294967295UL},{4294967287UL},{0x6BAB4CB9L}},{{0xB17D1A46L},{0x95ED3E5EL},{0UL},{0x95ED3E5EL},{0xB17D1A46L}},{{0x6BAB4CB9L},{4294967287UL},{4294967295UL},{0UL},{0x6BAB4CB9L}},{{0xE7E58850L},{0x95ED3E5EL},{4294967293UL},{0x95ED3E5EL},{0xE7E58850L}}}};
        int16_t ***l_848 = &g_677;
        int32_t l_957[5][10] = {{0x3EABD5AAL,2L,0xFA221EFCL,0xFA221EFCL,2L,0x3EABD5AAL,(-1L),0x3EABD5AAL,2L,0xFA221EFCL},{6L,(-9L),6L,0xFA221EFCL,(-1L),(-1L),0xFA221EFCL,6L,(-9L),6L},{6L,0x3EABD5AAL,(-9L),2L,(-9L),0x3EABD5AAL,6L,6L,0x3EABD5AAL,(-9L)},{0x3EABD5AAL,6L,6L,0x3EABD5AAL,(-9L),2L,(-9L),0x3EABD5AAL,6L,2L},{0x313CE63CL,2L,(-1L),(-9L),(-9L),(-1L),2L,0x313CE63CL,2L,(-1L)}};
        int32_t l_958 = 0xAAE1EB78L;
        int i, j, k;
        if (p_36.f0)
        { /* block id: 355 */
            for (g_625.f2 = 0; g_625.f2 < 10; g_625.f2 += 1)
            {
                for (g_758 = 0; g_758 < 7; g_758 += 1)
                {
                    for (g_170.f2 = 0; g_170.f2 < 3; g_170.f2 += 1)
                    {
                        g_149[g_625.f2][g_758][g_170.f2] = 0x5.42524Dp+76;
                    }
                }
            }
            return g_795;
        }
        else
        { /* block id: 358 */
            uint8_t l_809 = 0x63L;
            int32_t l_825 = 0x6D1E34C7L;
            int32_t l_826 = (-3L);
            uint16_t ***l_846 = &l_773;
            for (g_170.f2 = 1; (g_170.f2 >= 0); g_170.f2 -= 1)
            { /* block id: 361 */
                int8_t l_828 = 0xFFL;
                for (g_2 = 0; (g_2 <= 7); g_2 += 1)
                { /* block id: 364 */
                    int32_t l_810[1][10][7] = {{{0x977196F7L,0xA3357A42L,0xA3357A42L,0x977196F7L,0xA3357A42L,0xA3357A42L,0x977196F7L},{0x2FD7FFCBL,0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL,0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL},{0x977196F7L,0x977196F7L,8L,0x977196F7L,0x977196F7L,8L,0x977196F7L},{0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL,0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL,0xB210EDDAL},{0xA3357A42L,0x977196F7L,0xA3357A42L,0xA3357A42L,0x977196F7L,0xA3357A42L,0xA3357A42L},{0xB210EDDAL,0xB210EDDAL,0xFD63874FL,0xB210EDDAL,0xB210EDDAL,0xFD63874FL,0xB210EDDAL},{0x977196F7L,0xA3357A42L,0xA3357A42L,0x977196F7L,0xA3357A42L,0xA3357A42L,0x977196F7L},{0x2FD7FFCBL,0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL,0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL},{0x977196F7L,0x977196F7L,8L,0x977196F7L,0x977196F7L,8L,0x977196F7L},{0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL,0xB210EDDAL,0x2FD7FFCBL,0x2FD7FFCBL,0xB210EDDAL}}};
                    int i, j, k;
                    for (g_95 = 0; (g_95 <= 7); g_95 += 1)
                    { /* block id: 367 */
                        int32_t *l_823[4][10][6] = {{{&l_810[0][3][2],&l_810[0][4][1],&g_234[g_284],(void*)0,&l_810[0][8][4],&g_234[7]},{&g_234[7],&l_810[0][4][2],(void*)0,(void*)0,(void*)0,&g_234[g_284]},{&l_810[0][3][2],&g_234[1],&g_234[7],&g_234[g_284],&g_234[g_284],(void*)0},{&l_810[0][8][3],&l_810[0][7][4],&l_810[0][7][4],&g_234[g_284],&g_234[7],&g_234[g_284]},{&g_234[7],&l_810[0][5][2],(void*)0,&l_810[0][7][4],&l_810[0][5][2],&g_234[g_284]},{(void*)0,&l_810[0][7][4],&g_234[7],&l_810[0][4][3],&g_234[7],&g_234[g_284]},{&g_234[g_284],&l_810[0][7][4],(void*)0,&g_234[7],(void*)0,&g_234[g_284]},{&g_234[g_284],&g_234[1],&l_810[0][3][2],&l_810[0][4][3],(void*)0,&g_234[6]},{(void*)0,&g_234[7],&g_234[7],&l_810[0][7][4],&g_234[7],&l_810[0][7][4]},{&g_234[7],&l_810[0][4][1],&l_810[0][5][3],&g_234[g_284],&l_810[0][1][5],&g_234[g_284]}},{{&l_810[0][8][3],&g_234[3],&l_810[0][8][3],&g_234[g_284],(void*)0,&l_810[0][7][4]},{&l_810[0][3][2],&l_810[0][7][4],&g_234[7],(void*)0,&g_234[7],&g_234[7]},{&g_234[7],&g_234[7],(void*)0,(void*)0,&l_810[0][4][1],&g_234[g_284]},{&l_810[0][3][2],&g_234[1],&g_234[g_284],&g_234[g_284],&g_234[1],(void*)0},{&l_810[0][8][3],&g_234[g_284],&g_234[7],&g_234[g_284],&g_234[7],(void*)0},{&g_234[7],(void*)0,(void*)0,&l_810[0][7][4],(void*)0,&g_234[7]},{(void*)0,&l_810[0][5][2],&g_234[7],&l_810[0][4][3],&l_810[0][4][2],&l_810[0][7][4]},{&g_234[g_284],&g_234[g_284],(void*)0,&g_234[7],&g_234[g_284],&g_234[g_284]},{&g_234[g_284],&l_810[0][1][5],&g_234[4],&g_234[g_284],&g_234[g_284],&g_234[7]},{&l_810[0][7][4],&g_234[g_284],&g_234[g_284],&g_234[8],&l_810[0][7][4],&g_234[1]}},{{&l_810[0][8][1],&g_234[7],&l_810[0][2][5],(void*)0,&g_234[g_284],&g_234[7]},{&g_234[7],&l_810[0][7][4],(void*)0,(void*)0,&l_810[0][7][4],&g_234[7]},{(void*)0,&l_810[0][7][4],&l_810[0][8][1],&g_234[0],&l_810[0][7][4],&g_234[g_284]},{&g_234[g_284],&g_234[g_284],&g_234[8],&g_234[0],&g_234[7],&g_234[g_284]},{(void*)0,&g_234[7],&g_234[g_284],(void*)0,&g_234[7],&g_234[8]},{&g_234[7],(void*)0,&g_234[7],(void*)0,&g_234[g_284],&g_234[g_284]},{&l_810[0][8][1],&l_810[0][7][4],&g_234[8],&g_234[8],&l_810[0][7][4],&l_810[0][8][1]},{&l_810[0][7][4],(void*)0,&g_234[g_284],&g_234[g_284],(void*)0,&g_234[7]},{&g_234[7],(void*)0,&g_234[7],&l_810[0][2][5],&g_234[g_284],&l_810[0][9][6]},{&g_234[7],&g_234[7],&l_810[0][2][5],&g_234[g_284],&g_234[7],&g_234[g_284]}},{{&l_810[0][7][4],(void*)0,&g_234[4],&g_234[8],&g_234[g_284],&g_234[7]},{&l_810[0][8][1],&l_810[0][7][4],(void*)0,(void*)0,&g_234[6],&l_810[0][9][6]},{&g_234[7],&l_810[0][4][3],&g_234[0],(void*)0,&g_234[g_284],&g_234[7]},{(void*)0,&g_234[g_284],&g_234[g_284],&g_234[0],&g_234[g_284],(void*)0},{&g_234[g_284],&g_234[g_284],&l_810[0][7][4],&g_234[0],&l_810[0][7][4],&g_234[g_284]},{(void*)0,&g_234[7],&g_234[7],(void*)0,&g_234[7],&l_810[0][7][4]},{&g_234[7],&l_810[0][4][3],&g_234[g_284],(void*)0,(void*)0,(void*)0},{&l_810[0][8][1],(void*)0,&l_810[0][7][4],&g_234[8],(void*)0,&g_234[g_284]},{&l_810[0][7][4],&l_810[0][7][4],(void*)0,&g_234[g_284],&g_234[g_284],&g_234[7]},{&g_234[7],&l_810[0][4][3],(void*)0,&l_810[0][2][5],&g_234[7],(void*)0}}};
                        int32_t l_824 = 4L;
                        int32_t l_827[7][10] = {{(-9L),0xB7FD2C73L,(-9L),(-4L),0x678C6EADL,0x678C6EADL,(-4L),(-9L),0xB7FD2C73L,(-9L)},{(-9L),0xFDB96F2AL,0xB7FD2C73L,(-1L),0xB7FD2C73L,0xFDB96F2AL,(-9L),(-9L),0xFDB96F2AL,0xB7FD2C73L},{0xFDB96F2AL,(-9L),(-9L),0xFDB96F2AL,0xB7FD2C73L,(-1L),0xB7FD2C73L,0xFDB96F2AL,(-9L),(-9L)},{0xB7FD2C73L,(-9L),(-4L),0x678C6EADL,0x678C6EADL,(-4L),(-9L),0xB7FD2C73L,(-9L),(-4L)},{(-1L),0xFDB96F2AL,0x678C6EADL,0xFDB96F2AL,(-1L),(-4L),(-4L),(-1L),0xFDB96F2AL,0x678C6EADL},{0xB7FD2C73L,0xB7FD2C73L,0x678C6EADL,(-1L),0x4D2918A4L,(-1L),0x678C6EADL,0xB7FD2C73L,0xB7FD2C73L,0x678C6EADL},{(-9L),(-4L),0x678C6EADL,0x678C6EADL,(-4L),(-9L),0xB7FD2C73L,(-9L),(-4L),0x678C6EADL}};
                        int i, j, k;
                        l_810[0][7][4] = (safe_sub_func_int64_t_s_s(((safe_mod_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((0x55L || 0xACL), (((safe_unary_minus_func_uint64_t_u((+(!(g_202[(g_284 + 3)][g_170.f2] & ((g_493[(g_284 + 3)] = g_234[g_284]) > (g_349[g_95] != g_349[g_95]))))))) < (safe_div_func_uint64_t_u_u(p_36.f0, (l_809 = ((((safe_div_func_int8_t_s_s((g_625.f0 <= g_241.f0), 0xC5L)) && g_625.f1) | (*g_425)) && p_36.f0))))) & p_36.f2))), 0xE2AEB6BF8640CAFDLL)) , (*g_425)), 18446744073709551613UL));
                        (*l_779) = p_36.f1;
                        l_824 |= (p_36.f1 >= ((((((g_234[7] ^= (((safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f(p_36.f2, p_37)), (safe_sub_func_float_f_f(g_202[0][5], (safe_add_func_float_f_f((((+p_37) > p_37) != (0xA.6D77A3p+73 >= (p_36.f2 > (l_822 , 0x9.32FB75p-33)))), (-0x1.4p+1))))))), p_37)) , p_36.f1) < p_36.f1)) , 0xF20AL) | 8UL) && g_286) != g_7) != p_36.f1));
                        g_829--;
                    }
                    return g_202[(g_170.f2 + 5)][(g_284 + 4)];
                }
                for (l_786 = 5; (l_786 >= 0); l_786 -= 1)
                { /* block id: 380 */
                    float *l_832 = &g_149[4][5][2];
                    int i, j;
                    (*l_832) = g_202[(l_786 + 2)][(l_786 + 1)];
                    for (g_148 = 1; (g_148 >= 0); g_148 -= 1)
                    { /* block id: 384 */
                        int32_t l_840 = 0x1C1A9FB9L;
                        int i, j;
                        if (g_202[(l_786 + 1)][(g_170.f2 + 5)])
                            break;
                        if (p_36.f1)
                            continue;
                        g_189[1] = (g_189[g_148] > (0x0.8p+1 == (safe_add_func_float_f_f(p_36.f1, ((!p_36.f2) == (safe_sub_func_float_f_f((((*l_832) = g_294[0][4]) >= (p_37 > ((safe_sub_func_int32_t_s_s((-8L), (*g_651))) , (0x4.4p+1 != g_202[5][6])))), l_822.f1)))))));
                        l_840 = p_36.f0;
                    }
                    for (g_148 = 8; (g_148 >= 0); g_148 -= 1)
                    { /* block id: 393 */
                        g_189[1] = ((*l_832) = 0xE.72FFFDp+55);
                    }
                }
            }
            for (l_809 = 0; (l_809 <= 7); l_809 += 1)
            { /* block id: 401 */
                uint32_t l_843 = 18446744073709551606UL;
                union U2 l_855[7] = {{0},{0},{0},{0},{0},{0},{0}};
                int i;
                for (l_826 = 1; (l_826 >= 0); l_826 -= 1)
                { /* block id: 404 */
                    int8_t *l_849[3];
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_849[i] = (void*)0;
                    if ((safe_add_func_int32_t_s_s(((g_202[l_809][(g_284 + 4)] || ((&g_275 == (void*)0) , l_843)) , p_36.f2), ((safe_mul_func_int8_t_s_s((l_846 != &g_351), ((l_847[3][6][2] , l_848) != (void*)0))) != 1L))))
                    { /* block id: 405 */
                        float *l_851 = &g_149[0][6][1];
                        float **l_850 = &l_851;
                        if (p_36.f0)
                            break;
                        (*l_778) ^= ((((*l_850) = func_44(l_849[0])) == (void*)0) < (safe_sub_func_int64_t_s_s(l_826, (g_84 , (p_36.f1 && (!(l_855[6] , 0UL)))))));
                        if (p_36.f2)
                            break;
                    }
                    else
                    { /* block id: 410 */
                        union U2 *l_860 = &l_855[6];
                        union U2 **l_859 = &l_860;
                        int32_t l_862[5][7] = {{0x5C129541L,0x25B967A1L,0xB622B762L,0x25B967A1L,0x5C129541L,0x25B967A1L,0xB622B762L},{0L,0L,0x8108D56AL,1L,4L,4L,1L},{0x5C243B78L,0xF9BE0907L,0x5C243B78L,0x25B967A1L,0x5C243B78L,0xF9BE0907L,0x5C243B78L},{0L,1L,1L,0L,4L,0x8108D56AL,0x8108D56AL},{0x5C129541L,0xF9BE0907L,0xB622B762L,0xF9BE0907L,0x5C129541L,0xF9BE0907L,0xB622B762L}};
                        int i, j;
                        (*l_859) = ((safe_lshift_func_int16_t_s_s((l_858 , 0x7A9EL), 1)) , &l_855[6]);
                        l_862[3][1] &= (0xB6A84C21L == l_861);
                    }
                }
                if (p_36.f1)
                    break;
                for (g_829 = 0; (g_829 <= 5); g_829 += 1)
                { /* block id: 418 */
                    int16_t *****l_864 = &l_863;
                    (*l_864) = l_863;
                }
            }
            (*l_782) = (safe_sub_func_int16_t_s_s((((safe_sub_func_int16_t_s_s(((*g_720) != (g_397[0][4][1].f0 , l_869[2][0])), (l_826 = (safe_rshift_func_int16_t_s_s(p_36.f2, (safe_mul_func_int8_t_s_s(p_36.f2, l_825))))))) > p_36.f2) ^ (safe_lshift_func_int16_t_s_u(p_36.f1, ((***l_846) = ((safe_lshift_func_int16_t_s_s((l_878[5][0][0] != l_878[5][0][0]), l_847[3][6][2].f0)) ^ 3UL))))), l_809));
        }
        ++l_959;
        return (*l_782);
    }
    return p_36.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_286 g_148 g_287 g_758 g_651 g_77 g_7 g_625
 * writes: g_286 g_148 g_758
 */
static union U3  func_38(uint16_t * p_39, uint16_t * p_40, uint8_t  p_41)
{ /* block id: 318 */
    int16_t l_742 = 0x97FDL;
    float l_743 = 0x4.334B7Fp+75;
    int32_t l_744 = (-1L);
    int32_t l_745 = 0xEB5B9A7AL;
    int32_t l_746[5][2];
    int32_t *l_751 = (void*)0;
    int32_t *l_752 = &g_170.f2;
    int32_t *l_753 = &l_746[4][0];
    int32_t *l_754 = &l_746[2][0];
    int32_t *l_755 = (void*)0;
    int32_t *l_756 = (void*)0;
    int32_t *l_757[9][6] = {{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745},{&g_5,&g_5,&l_745,&g_5,&g_5,&l_745}};
    int32_t **l_761 = &l_756;
    int i, j;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
            l_746[i][j] = (-4L);
    }
    for (g_286 = 0; (g_286 <= 0); g_286 += 1)
    { /* block id: 321 */
        int16_t l_739 = 0xB8D3L;
        int32_t *l_740 = &g_294[0][1];
        int32_t *l_741[8][6] = {{&g_170.f2,&g_7,&g_625.f2,(void*)0,&g_75,(void*)0},{&g_625.f2,(void*)0,&g_625.f2,&g_170.f2,&g_75,&g_9},{&g_625.f2,&g_7,&g_170.f2,&g_625.f2,&g_625.f2,&g_170.f2},{&g_294[3][3],&g_294[3][3],(void*)0,&g_625.f2,&g_7,&g_170.f2},{&g_625.f2,(void*)0,&g_9,&g_170.f2,&g_9,(void*)0},{&g_625.f2,&g_625.f2,&g_9,(void*)0,&g_294[3][3],&g_170.f2},{&g_170.f2,(void*)0,(void*)0,(void*)0,(void*)0,&g_170.f2},{(void*)0,(void*)0,&g_170.f2,&g_75,&g_294[3][3],&g_9}};
        uint8_t l_747[9] = {0x64L,0x64L,0x64L,0x64L,0x64L,0x64L,0x64L,0x64L,0x64L};
        union U3 l_750 = {1UL};
        int i, j;
        l_747[3]++;
        for (g_148 = 0; (g_148 <= 7); g_148 += 1)
        { /* block id: 325 */
            int i, j;
            l_745 = g_287[(g_286 + 1)][g_286];
            return l_750;
        }
    }
    g_758++;
    (*l_761) = &g_294[0][1];
    (*l_754) &= ((safe_rshift_func_uint8_t_u_s((p_41 < p_41), ((p_41 < 1L) | (*g_651)))) > p_41);
    return g_625;
}


/* ------------------------------------------ */
/* 
 * reads : g_7 g_46 g_77 g_84 g_2 g_9 g_75 g_104 g_107 g_105 g_149 g_5 g_170 g_170.f0 g_170.f1 g_57 g_202 g_236 g_55 g_244 g_95 g_275 g_189 g_170.f2 g_275.f0 g_286 g_287 g_294 g_245 g_246 g_313 g_118 g_148 g_351 g_397 g_398 g_424 g_253 g_425 g_490 g_493 g_399 g_346 g_284 g_241.f0 g_234 g_578 g_397.f0 g_625 g_625.f0 g_153 g_651 g_720 g_733
 * writes: g_77 g_2 g_55 g_57 g_95 g_75 g_118 g_148 g_153 g_149 g_202 g_189 g_236 g_244 g_104 g_284 g_286 g_287 g_170.f0 g_294 g_313 g_170.f2 g_349 g_398 g_490 g_493 g_491 g_508 g_233 g_170.f1 g_275 g_651 g_676 g_105 g_399 g_733
 */
static uint16_t * func_42(int32_t * p_43)
{ /* block id: 11 */
    uint16_t l_63 = 65535UL;
    int16_t *l_71 = &g_57;
    union U4 l_72 = {0UL};
    const uint16_t *l_252 = &g_253[0][3][0];
    int32_t *l_293[9][7] = {{&g_294[1][3],&g_294[1][3],&g_7,&g_9,&g_5,&g_294[3][4],&g_294[1][3]},{&g_9,&g_7,&g_294[1][4],&g_5,(void*)0,(void*)0,&g_5},{&g_9,(void*)0,&g_9,&g_7,&g_5,&g_9,(void*)0},{&g_5,&g_5,&g_7,&g_7,&g_5,&g_294[0][1],&g_5},{&g_5,&g_294[1][1],&g_294[1][1],&g_5,&g_294[3][4],&g_5,&g_9},{&g_294[0][1],&g_7,(void*)0,&g_294[0][3],&g_7,&g_294[0][1],&g_294[0][1]},{&g_7,&g_9,(void*)0,&g_9,&g_7,&g_5,&g_9},{&g_294[1][4],&g_294[0][1],&g_7,&g_7,(void*)0,&g_7,(void*)0},{&g_294[1][1],(void*)0,&g_7,&g_7,(void*)0,&g_294[1][1],&g_294[3][4]}};
    union U3 l_333 = {0UL};
    union U1 ****l_379 = (void*)0;
    int64_t *l_423 = &g_55;
    int64_t **l_422 = &l_423;
    uint16_t l_426 = 65531UL;
    const union U2 l_551 = {0};
    uint16_t **l_560 = &g_104[4];
    union U4 *l_566 = &g_275;
    uint64_t l_582 = 0xEBD9F340703CBBC0LL;
    int16_t ***l_675 = (void*)0;
    uint64_t l_732[8][9] = {{18446744073709551615UL,0UL,0UL,18446744073709551615UL,0x6B3F6C187DEDC448LL,18446744073709551610UL,0x6B3F6C187DEDC448LL,18446744073709551615UL,0UL},{0x6B3F6C187DEDC448LL,0x6B3F6C187DEDC448LL,18446744073709551615UL,18446744073709551610UL,0UL,18446744073709551610UL,18446744073709551615UL,0x6B3F6C187DEDC448LL,0x6B3F6C187DEDC448LL},{0UL,18446744073709551615UL,0x6B3F6C187DEDC448LL,18446744073709551610UL,0x6B3F6C187DEDC448LL,18446744073709551615UL,0UL,0UL,18446744073709551615UL},{18446744073709551610UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551610UL,0xF348B6D2E941DB8CLL,0xF348B6D2E941DB8CLL,18446744073709551610UL,18446744073709551615UL},{0UL,0x6B3F6C187DEDC448LL,0UL,0xF348B6D2E941DB8CLL,18446744073709551615UL,18446744073709551615UL,0xF348B6D2E941DB8CLL,0UL,0x6B3F6C187DEDC448LL},{0x6B3F6C187DEDC448LL,0UL,0xF348B6D2E941DB8CLL,18446744073709551615UL,18446744073709551615UL,0xF348B6D2E941DB8CLL,0UL,0x6B3F6C187DEDC448LL,0UL},{18446744073709551615UL,18446744073709551610UL,0xF348B6D2E941DB8CLL,0xF348B6D2E941DB8CLL,18446744073709551610UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551610UL},{18446744073709551615UL,0UL,0UL,18446744073709551615UL,0x6B3F6C187DEDC448LL,18446744073709551610UL,0x6B3F6C187DEDC448LL,18446744073709551615UL,0UL}};
    int i, j;
lbl_369:
    g_294[1][1] |= (func_59((*p_43), l_63, (l_252 = func_64(func_66(l_71, l_72, l_63, g_46)))) , (-8L));
    g_294[1][3] ^= 6L;
    if (((void*)0 != &l_252))
    { /* block id: 126 */
        int32_t **l_295 = &l_293[7][3];
        uint32_t l_298 = 7UL;
        float l_339 = (-0x1.Ap+1);
        int8_t *l_343 = &g_107[3][5];
        int32_t l_350 = 0x9AF1FD50L;
        const uint16_t **l_353 = &g_352[6];
        uint16_t *l_361 = &l_63;
        int32_t l_365[8];
        int i;
        for (i = 0; i < 8; i++)
            l_365[i] = (-1L);
        (*l_295) = (void*)0;
        for (g_170.f0 = 0; (g_170.f0 <= 49); ++g_170.f0)
        { /* block id: 130 */
            if (l_298)
                break;
        }
        if (((*g_245) != (**g_244)))
        { /* block id: 133 */
            int32_t *l_306 = &g_170.f2;
            int32_t l_312 = 0xD7A1B219L;
            int16_t *l_335[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            float l_337 = 0xB.761E78p+51;
            int8_t *l_357 = (void*)0;
            int i;
            for (g_55 = 0; (g_55 < 21); g_55++)
            { /* block id: 136 */
                float l_309[4];
                int32_t l_311 = 0xD8F89D9CL;
                int i;
                for (i = 0; i < 4; i++)
                    l_309[i] = (-0x10.Bp-1);
                for (g_95 = 0; (g_95 > 33); ++g_95)
                { /* block id: 139 */
                    float *l_310 = &l_309[1];
                    for (l_63 = 2; (l_63 <= 7); l_63 += 1)
                    { /* block id: 142 */
                        uint64_t l_303 = 18446744073709551615UL;
                        int i, j;
                        l_303--;
                        (*l_295) = l_306;
                        if (g_202[l_63][l_63])
                            continue;
                    }
                    for (g_118 = (-7); (g_118 == 54); g_118++)
                    { /* block id: 149 */
                        if ((*p_43))
                            break;
                        return &g_105[5][2][0];
                    }
                    (*l_310) = l_309[1];
                    (*l_295) = p_43;
                }
                g_313++;
                if (g_118)
                    break;
            }
            for (l_72.f0 = 0; (l_72.f0 > 6); l_72.f0++)
            { /* block id: 161 */
                int64_t l_328 = 0L;
                uint64_t *l_331[3];
                int32_t l_332 = 0x1A24DA4DL;
                int16_t *l_334 = (void*)0;
                uint8_t *l_336 = &g_118;
                int32_t l_338 = 0xA34304A7L;
                int i;
                for (i = 0; i < 3; i++)
                    l_331[i] = (void*)0;
                if (((*l_306) , (safe_lshift_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s((-1L), ((safe_add_func_int32_t_s_s(((*l_306) = (((((safe_mod_func_uint8_t_u_u(((*l_336) = (g_287[1][0] == ((((*l_306) > ((safe_sub_func_uint64_t_u_u(3UL, l_328)) <= ((g_75 &= (((safe_sub_func_uint8_t_u_u((g_287[6][0] >= (l_332 &= (*l_306))), ((l_333 , l_334) != l_335[2]))) <= 0x7556C6A8L) & g_294[1][0])) < 0xEB3EB111L))) > l_328) >= 6L))), (*l_306))) ^ (*l_306)) == g_148) & (*l_306)) | l_328)), 0L)) & 0x23L))), g_236))))
                { /* block id: 166 */
                    uint8_t l_340 = 8UL;
                    l_340++;
                }
                else
                { /* block id: 168 */
                    const int32_t *l_345[4] = {&g_346,&g_346,&g_346,&g_346};
                    const int32_t **l_344 = &l_345[1];
                    const int32_t *l_348 = &g_75;
                    const int32_t **l_347[10] = {(void*)0,&l_348,(void*)0,&l_348,(void*)0,&l_348,(void*)0,&l_348,(void*)0,&l_348};
                    uint16_t **l_355[1];
                    uint16_t ***l_354 = &l_355[0];
                    float *l_356[8] = {&g_233,&g_233,&g_233,&g_233,&g_233,&g_233,&g_233,&g_233};
                    int i;
                    for (i = 0; i < 1; i++)
                        l_355[i] = &g_104[2];
                    g_349[1] = ((*l_344) = func_44(l_343));
                    g_189[0] = ((g_202[0][5] , ((l_350 = (*p_43)) || ((l_353 = ((&l_332 != (void*)0) , g_351)) != ((*l_354) = &g_104[4])))) , l_298);
                    for (l_338 = 0; l_338 < 10; l_338 += 1)
                    {
                        l_347[l_338] = (void*)0;
                    }
                    (*l_344) = ((*p_43) , func_44(l_357));
                }
                (*l_295) = p_43;
            }
        }
        else
        { /* block id: 180 */
            for (l_333.f0 = 26; (l_333.f0 < 4); l_333.f0 = safe_sub_func_int8_t_s_s(l_333.f0, 1))
            { /* block id: 183 */
                uint16_t *l_360[5] = {&g_236,&g_236,&g_236,&g_236,&g_236};
                int i;
                return &g_105[5][9][0];
            }
        }
        for (g_170.f0 = 0; (g_170.f0 < 46); ++g_170.f0)
        { /* block id: 189 */
            int32_t l_364[3];
            uint16_t l_366 = 1UL;
            int i;
            for (i = 0; i < 3; i++)
                l_364[i] = 0x0D74FEB7L;
            --l_366;
            if (g_170.f2)
                goto lbl_369;
        }
    }
    else
    { /* block id: 193 */
        uint32_t l_372 = 4294967286UL;
        int32_t l_380 = (-6L);
        int32_t l_383 = 7L;
        int32_t l_384 = 1L;
        int32_t l_385 = 0x08C9777DL;
        int32_t l_390[9][6] = {{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0xF736DF96L,0x91C5DBEEL,0xF736DF96L,0xF736DF96L},{0x91C5DBEEL,0xF736DF96L,0x643C4D1BL,0xF736DF96L,0x643C4D1BL,0x643C4D1BL}};
        uint32_t l_392 = 4294967295UL;
        union U1 **l_401[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint8_t *l_418 = &g_118;
        int32_t **l_506 = &l_293[7][4];
        uint16_t **l_559 = &g_104[1];
        union U4 *l_565 = &l_72;
        uint64_t l_626 = 0x93B276ECFAB23D29LL;
        uint64_t l_639[6][9] = {{0xED3504552B439EA3LL,18446744073709551606UL,0UL,18446744073709551606UL,0xED3504552B439EA3LL,18446744073709551615UL,0x15180C7C3B922847LL,0UL,0x8B63348D659E41D4LL},{9UL,0xFAD4F798EE1D0780LL,1UL,18446744073709551613UL,18446744073709551610UL,9UL,7UL,4UL,0xCF54DBE344073718LL},{1UL,0xED3504552B439EA3LL,1UL,4UL,0x8B63348D659E41D4LL,18446744073709551615UL,18446744073709551615UL,0x8B63348D659E41D4LL,4UL},{0x9E6C0A5AE07780E1LL,3UL,0x9E6C0A5AE07780E1LL,9UL,0x8B63348D659E41D4LL,6UL,3UL,0UL,18446744073709551606UL},{0x3A66A1A8DC9678F4LL,9UL,0x15180C7C3B922847LL,0x9E6C0A5AE07780E1LL,18446744073709551610UL,0xE994427BFCAE059CLL,18446744073709551606UL,1UL,6UL},{18446744073709551614UL,0x15180C7C3B922847LL,0xCF54DBE344073718LL,9UL,0xED3504552B439EA3LL,0xED3504552B439EA3LL,9UL,0xCF54DBE344073718LL,0x15180C7C3B922847LL}};
        uint32_t l_660[8] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
        union U3 l_704[7] = {{0x349DDBA6L},{0x349DDBA6L},{0x349DDBA6L},{0x349DDBA6L},{0x349DDBA6L},{0x349DDBA6L},{0x349DDBA6L}};
        uint8_t l_705 = 0x4CL;
        int i, j;
        for (g_2 = 12; (g_2 <= 56); g_2++)
        { /* block id: 196 */
            uint16_t *l_381[9];
            int32_t l_382 = (-8L);
            int32_t l_386 = 0xF9D87B19L;
            int32_t l_387 = 0x15048321L;
            int32_t l_388 = 8L;
            int32_t l_389 = (-2L);
            int32_t l_391 = 1L;
            union U1 ***l_400 = (void*)0;
            int i;
            for (i = 0; i < 9; i++)
                l_381[i] = &g_105[5][9][0];
            --l_372;
            g_294[0][1] = (1UL > ((((safe_lshift_func_uint16_t_u_s((((safe_lshift_func_uint8_t_u_u((65526UL || (((void*)0 != l_379) || (--l_392))), 6)) | (safe_mul_func_uint8_t_u_u(g_236, ((g_397[0][4][1] , (g_398 = g_398)) == l_401[4])))) , (safe_div_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u((((g_2 && 0xC81EL) , l_389) != g_170.f2), l_390[8][5])), 0xAAL))), 7)) <= l_387) != l_385) >= l_387));
        }
        if ((safe_rshift_func_uint16_t_u_u(l_380, (safe_add_func_uint32_t_u_u((((safe_add_func_uint16_t_u_u(0xD091L, l_385)) == (g_275 , (safe_mod_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u((((g_202[0][5] <= (safe_rshift_func_uint16_t_u_u((((((247UL && ((*l_418)--)) , ((!l_372) | 0xF8L)) < 0xF5L) , l_422) != g_424), 15))) < 0x71L) | 1L), g_107[3][4])) , (-1L)), l_426)))) , 1UL), (*p_43))))))
        { /* block id: 203 */
            int32_t l_427 = 0x974D873EL;
            float *l_428 = (void*)0;
            uint16_t l_443 = 65529UL;
            int64_t l_444[8][8] = {{(-3L),0xDE04096C07635F20LL,0xDE04096C07635F20LL,(-3L),0xA4A9FB644B5F292CLL,1L,0x304F4B403FD4E721LL,(-9L)},{0xCCB66470C1F8C9C4LL,0xFD23D2D6B36980B8LL,9L,0xA4A9FB644B5F292CLL,0x4CFB0A0D7B086B05LL,0xA4A9FB644B5F292CLL,9L,0xFD23D2D6B36980B8LL},{0xDE04096C07635F20LL,0xFD23D2D6B36980B8LL,(-9L),9L,0xF6C30EC9AAED7218LL,1L,0xD692D6A2865CCBA4LL,0xD692D6A2865CCBA4LL},{(-9L),0xDE04096C07635F20LL,0xED9EF949BDD1637CLL,0xED9EF949BDD1637CLL,0xDE04096C07635F20LL,(-9L),0xD692D6A2865CCBA4LL,0x4CFB0A0D7B086B05LL},{0x304F4B403FD4E721LL,0xED9EF949BDD1637CLL,(-9L),1L,9L,0xCCB66470C1F8C9C4LL,9L,1L},{9L,0xCCB66470C1F8C9C4LL,9L,1L,(-9L),0xED9EF949BDD1637CLL,0x304F4B403FD4E721LL,0x4CFB0A0D7B086B05LL},{0xD692D6A2865CCBA4LL,(-9L),0xDE04096C07635F20LL,0xED9EF949BDD1637CLL,0xED9EF949BDD1637CLL,0xDE04096C07635F20LL,(-9L),0xD692D6A2865CCBA4LL},{0xD692D6A2865CCBA4LL,1L,0xF6C30EC9AAED7218LL,9L,(-9L),0xFD23D2D6B36980B8LL,0xDE04096C07635F20LL,0xFD23D2D6B36980B8LL}};
            int32_t l_445 = 0x1C5282BAL;
            union U3 l_505 = {0UL};
            union U1 **l_540 = &g_399;
            uint64_t *l_576 = &g_202[0][5];
            uint64_t * const *l_575[7];
            int32_t l_596 = 2L;
            int32_t l_599 = (-1L);
            int i, j;
            for (i = 0; i < 7; i++)
                l_575[i] = &l_576;
            g_149[0][6][1] = l_427;
            for (l_333.f1 = 0; (l_333.f1 != 47); l_333.f1++)
            { /* block id: 207 */
                uint8_t l_435 = 0x2AL;
                int8_t *l_454[5][2] = {{&g_148,&g_148},{&g_148,&g_107[3][5]},{&g_287[9][0],&g_287[9][0]},{&g_107[3][5],&g_287[9][0]},{&g_287[9][0],&g_107[3][5]}};
                union U1 ***l_502 = (void*)0;
                uint32_t l_510 = 8UL;
                int32_t l_558 = 0x71A03A86L;
                union U3 l_577 = {0xA0D4482EL};
                int32_t l_592 = (-1L);
                int32_t l_593 = 0x3C1EF731L;
                int32_t l_595 = 0L;
                int32_t l_597[1];
                int i, j;
                for (i = 0; i < 1; i++)
                    l_597[i] = (-1L);
                if ((l_383 >= ((safe_mod_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(l_435, 8)), ((!255UL) , (safe_mul_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(l_435, ((safe_div_func_uint32_t_u_u(l_443, (l_445 = l_444[7][6]))) , ((l_383 != ((safe_add_func_uint64_t_u_u(g_253[0][3][0], (((g_287[9][0] = (safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u(((4294967292UL || l_384) || l_380), 3UL)), 4)), l_445))) & 0x57L) != 0xBB65L))) <= 0xD7C7L)) | l_390[4][5])))), g_202[6][7]))))) != (*g_425))))
                { /* block id: 210 */
                    const uint64_t l_489 = 0UL;
                    union U1 *****l_492 = &g_490[0][3];
                    int32_t ***l_507[2];
                    const union U1 *l_539[10];
                    const union U1 **l_538 = &l_539[6];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_507[i] = &l_506;
                    for (i = 0; i < 10; i++)
                        l_539[i] = &g_397[0][4][1];
                    l_380 |= (g_294[0][1] ^ (safe_div_func_uint64_t_u_u((l_445 >= (safe_mul_func_int16_t_s_s((((((*l_418) = (safe_sub_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((safe_div_func_int16_t_s_s((safe_sub_func_int8_t_s_s((((safe_mod_func_uint16_t_u_u(0x74ADL, ((safe_mul_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_s((safe_div_func_uint16_t_u_u(((((((safe_rshift_func_int8_t_s_s((8UL != (safe_lshift_func_int16_t_s_u((safe_add_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u(l_443, 1)), (((safe_mod_func_int64_t_s_s(((l_444[5][7] <= ((g_170 , (safe_lshift_func_int16_t_s_s(l_489, (((*l_492) = g_490[0][3]) != (void*)0)))) & 7L)) > l_443), (**g_424))) > l_385) && (*p_43)))), l_444[1][0]))), l_489)) <= l_390[8][5]) , &g_287[0][0]) != &g_107[3][5]) || (**g_424)) > (*p_43)), 0x1280L)), 3)), 4)) , l_489), g_9)) | g_493[7]))) , 0x66A1259B34656CEFLL) >= g_493[6]), l_383)), l_435)), l_489)), l_435))) & l_427) < (**g_424)) >= 0x30F8L), (-4L)))), l_384)));
                    if ((safe_mod_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(((**g_424) || ((**l_422) = l_445)), (safe_sub_func_uint32_t_u_u(((g_493[7] = l_390[8][5]) ^ ((((**l_492) = l_502) == &l_401[4]) != ((safe_add_func_int16_t_s_s(0L, (l_435 == ((g_508 = (l_505 , l_506)) != l_506)))) , (*p_43)))), 0x193FD8F1L)))), l_505.f2)), l_427)))
                    { /* block id: 218 */
                        l_510 &= (*p_43);
                    }
                    else
                    { /* block id: 220 */
                        uint16_t l_523 = 0UL;
                        float *l_541 = &g_233;
                        int32_t l_542 = (-7L);
                        uint64_t *l_546[6][6][6] = {{{&g_202[5][0],&g_202[5][0],&g_202[0][2],&g_95,&g_202[0][2],&g_202[5][0]},{&g_202[0][2],&g_202[0][5],&g_95,&g_95,&g_202[0][5],&g_202[0][2]},{&g_202[5][0],&g_202[0][2],&g_95,&g_202[0][2],&g_202[5][0],&g_202[5][0]},{&g_202[3][0],&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0]},{&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2],&g_202[3][0]},{&g_202[5][0],&g_202[5][0],&g_202[0][2],&g_95,&g_202[3][0],&g_202[0][2]}},{{&g_202[3][0],&g_202[5][0],&g_202[0][5],&g_202[0][5],&g_202[5][0],&g_202[3][0]},{&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2]},{&g_95,&g_202[3][0],&g_202[3][0],&g_95,&g_202[5][0],&g_95},{&g_95,&g_202[5][0],&g_95,&g_202[3][0],&g_202[3][0],&g_95},{&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2]},{&g_202[3][0],&g_202[5][0],&g_202[0][5],&g_202[0][5],&g_202[5][0],&g_202[3][0]}},{{&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2]},{&g_95,&g_202[3][0],&g_202[3][0],&g_95,&g_202[5][0],&g_95},{&g_95,&g_202[5][0],&g_95,&g_202[3][0],&g_202[3][0],&g_95},{&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2]},{&g_202[3][0],&g_202[5][0],&g_202[0][5],&g_202[0][5],&g_202[5][0],&g_202[3][0]},{&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2]}},{{&g_95,&g_202[3][0],&g_202[3][0],&g_95,&g_202[5][0],&g_95},{&g_95,&g_202[5][0],&g_95,&g_202[3][0],&g_202[3][0],&g_95},{&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2]},{&g_202[3][0],&g_202[5][0],&g_202[0][5],&g_202[0][5],&g_202[5][0],&g_202[3][0]},{&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2]},{&g_95,&g_202[3][0],&g_202[3][0],&g_95,&g_202[5][0],&g_95}},{{&g_95,&g_202[5][0],&g_95,&g_202[3][0],&g_202[3][0],&g_95},{&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2]},{&g_202[3][0],&g_202[5][0],&g_202[0][5],&g_202[0][5],&g_202[5][0],&g_202[3][0]},{&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2]},{&g_95,&g_202[3][0],&g_202[3][0],&g_95,&g_202[5][0],&g_95},{&g_95,&g_202[5][0],&g_95,&g_202[3][0],&g_202[3][0],&g_95}},{{&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2]},{&g_202[3][0],&g_202[5][0],&g_202[0][5],&g_202[0][5],&g_202[5][0],&g_202[3][0]},{&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2],&g_202[0][2]},{&g_95,&g_202[3][0],&g_202[3][0],&g_95,&g_202[5][0],&g_95},{&g_95,&g_202[5][0],&g_95,&g_202[3][0],&g_202[3][0],&g_95},{&g_202[0][2],&g_202[0][2],&g_202[3][0],&g_202[0][5],&g_202[3][0],&g_202[0][2]}}};
                        uint64_t **l_545 = &l_546[4][5][2];
                        float *l_557 = &g_153;
                        int i, j, k;
                        l_542 &= (((safe_lshift_func_uint16_t_u_u((safe_add_func_int16_t_s_s((safe_add_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(((*l_71) = 0xFAC8L), (safe_sub_func_uint16_t_u_u(l_523, ((safe_mod_func_int8_t_s_s(((((safe_div_func_float_f_f(((*l_541) = (((((*g_398) == (void*)0) , (((safe_mul_func_int8_t_s_s(0x34L, ((safe_sub_func_int64_t_s_s((safe_rshift_func_int8_t_s_u(l_505.f2, (g_170 , (safe_sub_func_uint8_t_u_u(l_523, ((safe_div_func_int16_t_s_s(((**g_398) , (-6L)), l_523)) == l_435)))))), 0L)) <= 4UL))) <= g_346) , l_538)) == l_540) >= l_523)), 0x3.0C2F3Ap-8)) , l_523) || l_435) || g_286), l_392)) >= l_435))))), 0UL)), l_523)), 0x4959L)), l_435)) < l_435) && l_435);
                        l_558 = ((safe_mul_func_float_f_f(((*l_557) = ((((*l_545) = l_423) == &g_95) <= ((safe_add_func_float_f_f(((*l_541) = (l_444[2][1] > l_443)), l_510)) < (((safe_add_func_float_f_f((l_551 , (g_55 > (!(safe_add_func_float_f_f((g_284 < (safe_div_func_float_f_f(l_510, l_542))), g_5))))), (-0x9.Fp-1))) > 0x0.1p+1) >= g_241.f0)))), 0x1.Fp+1)) > 0x3.6p+1);
                    }
                    if ((*p_43))
                        break;
                    l_560 = l_559;
                }
                else
                { /* block id: 231 */
                    uint16_t l_561 = 0x401AL;
                    int32_t l_564 = (-1L);
                    ++l_561;
                    l_564 = 9L;
                    l_566 = l_565;
                }
                if ((*p_43))
                { /* block id: 236 */
                    uint8_t *l_569[6][4][10] = {{{&l_435,&g_118,&l_435,&g_118,&l_435,&g_118,&g_118,&l_435,&l_435,&l_435},{&l_435,&l_435,(void*)0,&g_118,&l_435,&l_435,&g_118,&g_118,&l_435,(void*)0},{&g_118,&l_435,(void*)0,&l_435,&g_118,&l_435,(void*)0,&l_435,&g_118,(void*)0},{&l_435,&l_435,&g_118,&l_435,&l_435,&l_435,&l_435,&g_118,&g_118,&l_435}},{{&l_435,&g_118,&g_118,&l_435,(void*)0,&g_118,&l_435,&l_435,&g_118,&g_118},{&g_118,&g_118,&l_435,&l_435,(void*)0,&l_435,&g_118,&g_118,&l_435,&l_435},{&g_118,&g_118,&g_118,&g_118,(void*)0,&l_435,(void*)0,&l_435,&g_118,&g_118},{&l_435,&g_118,&l_435,(void*)0,(void*)0,&l_435,&g_118,&l_435,&g_118,&g_118}},{{&l_435,&g_118,(void*)0,&l_435,&l_435,&g_118,&g_118,&g_118,&g_118,(void*)0},{(void*)0,(void*)0,(void*)0,&l_435,&g_118,&g_118,&l_435,&l_435,(void*)0,&l_435},{&l_435,&l_435,&l_435,&g_118,&g_118,&g_118,&g_118,&l_435,&g_118,&g_118},{&g_118,(void*)0,&g_118,&g_118,&l_435,&g_118,&l_435,&g_118,(void*)0,&g_118}},{{&g_118,&l_435,&l_435,&g_118,&l_435,&l_435,&g_118,&l_435,&g_118,&l_435},{&l_435,&g_118,&g_118,&g_118,(void*)0,&g_118,(void*)0,&g_118,&g_118,&l_435},{&l_435,&g_118,&g_118,(void*)0,&l_435,&g_118,&g_118,&l_435,&l_435,&g_118},{&l_435,&l_435,(void*)0,&l_435,&g_118,&l_435,&l_435,&g_118,&l_435,(void*)0}},{{(void*)0,(void*)0,(void*)0,&l_435,&g_118,&l_435,&g_118,&l_435,&l_435,&g_118},{&g_118,&l_435,&l_435,&l_435,&l_435,&g_118,&l_435,&g_118,&g_118,&g_118},{&l_435,(void*)0,&l_435,&l_435,(void*)0,&g_118,&g_118,&g_118,(void*)0,&g_118},{&l_435,&g_118,&l_435,&l_435,&g_118,&g_118,&g_118,&g_118,&l_435,(void*)0}},{{&g_118,&g_118,&l_435,&g_118,(void*)0,&l_435,(void*)0,&g_118,&g_118,&l_435},{&g_118,&l_435,(void*)0,&g_118,&l_435,(void*)0,&l_435,&g_118,&l_435,(void*)0},{&g_118,&l_435,(void*)0,&l_435,&g_118,(void*)0,(void*)0,&g_118,&l_435,(void*)0},{(void*)0,&l_435,(void*)0,&g_118,&g_118,&g_118,&l_435,&g_118,&l_435,(void*)0}}};
                    int32_t l_580 = (-1L);
                    int i, j, k;
                    if (l_444[7][6])
                        break;
                    if ((*p_43))
                        break;
                    for (l_505.f0 = 0; (l_505.f0 <= 53); l_505.f0 = safe_add_func_int8_t_s_s(l_505.f0, 4))
                    { /* block id: 241 */
                        float *l_574 = &g_149[0][6][1];
                        int32_t l_581 = 0L;
                        l_582 = ((&g_118 != l_569[4][1][3]) || ((((safe_add_func_uint64_t_u_u(g_234[7], l_427)) >= (((((((*l_574) = (safe_mul_func_float_f_f((&g_284 == (void*)0), 0x6.4B4AA3p+40))) , l_575[3]) == (l_577 , g_578[1])) , g_170.f2) > l_580) , 7UL)) || l_581) , l_580));
                        l_445 |= (safe_rshift_func_int8_t_s_u((safe_mod_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((0x1317ECAB1AF46A5CLL ^ l_581) || (((0x7AF7012B9F9C0424LL == (l_580 <= (&g_107[0][2] == &g_107[1][5]))) && ((**g_424) = l_580)) | ((*l_418) = l_581))), l_580)), g_287[8][0])), g_234[7]));
                    }
                    if ((*p_43))
                        continue;
                }
                else
                { /* block id: 249 */
                    float l_589 = 0x1.7p-1;
                    int32_t l_590 = 0xE26A9951L;
                    int32_t l_591 = 0x56CE7C87L;
                    int32_t l_594 = 0xE53F3874L;
                    int32_t l_598 = (-1L);
                    uint32_t l_600 = 0x4988978AL;
                    --l_600;
                    g_294[2][1] = (~g_397[0][4][1].f0);
                }
                if ((*p_43))
                    break;
            }
            return (*l_559);
        }
        else
        { /* block id: 256 */
            union U4 l_624 = {18446744073709551615UL};
            union U1 **l_670 = (void*)0;
            int32_t l_679 = 0L;
            float *l_684[1];
            uint8_t l_689 = 0xFFL;
            int32_t l_690 = 1L;
            uint32_t **l_701 = &g_651;
            uint32_t ***l_700 = &l_701;
            uint32_t **l_703 = (void*)0;
            uint32_t ***l_702 = &l_703;
            int i;
            for (i = 0; i < 1; i++)
                l_684[i] = (void*)0;
            for (g_313 = 22; (g_313 == 52); ++g_313)
            { /* block id: 259 */
                uint32_t *l_608 = &g_170.f1;
                int32_t l_617 = 0x0D364636L;
                union U1 *l_635 = &g_241;
                int8_t *l_636 = (void*)0;
                int8_t *l_637[8][3][8] = {{{&g_287[0][0],&g_107[3][5],&g_148,&g_287[9][0],&g_107[3][5],&g_107[3][5],&g_107[3][5],&g_287[0][0]},{&g_107[3][5],&g_287[0][0],&g_287[3][0],&g_107[0][2],&g_107[3][5],&g_107[1][0],&g_107[1][0],&g_107[3][5]},{&g_287[0][0],&g_148,&g_148,&g_287[0][0],&g_107[3][5],&g_107[3][5],&g_287[8][0],&g_107[0][2]}},{{&g_107[3][5],&g_287[8][0],&g_287[4][0],&g_287[9][0],&g_107[3][5],&g_107[3][6],&g_287[9][0],&g_107[3][5]},{&g_107[3][5],&g_287[8][0],&g_287[3][0],&g_107[3][5],&g_107[1][4],&g_107[3][5],&g_287[3][0],&g_287[8][0]},{&g_287[8][0],&g_148,&g_107[1][0],&g_107[3][5],&g_287[3][0],&g_107[1][0],&g_107[3][5],&g_107[3][5]}},{{&g_107[3][5],&g_287[0][0],&g_148,&g_107[3][5],&g_287[8][0],&g_107[3][5],&g_107[3][5],&g_107[3][5]},{&g_107[0][2],&g_107[3][5],&g_107[1][0],&g_107[1][0],&g_107[3][5],&g_107[0][2],&g_287[3][0],&g_287[0][0]},{&g_107[3][5],&g_107[0][2],&g_287[3][0],&g_287[0][0],&g_107[3][5],&g_107[1][0],&g_287[9][0],&g_107[3][5]}},{{&g_287[0][0],&g_107[3][5],&g_287[4][0],&g_287[0][0],&g_287[8][0],&g_148,&g_287[8][0],&g_287[0][0]},{&g_148,&g_287[8][0],&g_148,&g_107[1][0],&g_107[3][5],&g_287[3][0],&g_107[1][0],&g_107[3][5]},{&g_287[8][0],&g_107[3][5],&g_287[3][0],&g_107[3][5],&g_107[3][5],&g_107[3][5],&g_107[3][5],&g_107[3][5]}},{{&g_287[8][0],&g_107[3][5],&g_148,&g_107[3][5],&g_107[3][5],&g_148,&g_107[3][5],&g_287[8][0]},{&g_148,&g_287[0][0],&g_107[3][5],&g_107[3][5],&g_287[8][0],&g_107[0][2],&g_148,&g_107[3][5]},{&g_287[0][0],&g_107[3][5],&g_107[1][0],&g_287[9][0],&g_107[3][5],&g_107[0][2],&g_107[3][5],&g_107[0][2]}},{{&g_107[3][5],&g_287[0][0],&g_107[3][6],&g_287[0][0],&g_107[3][5],&g_148,&g_287[9][0],&g_107[3][5]},{&g_107[0][2],&g_107[3][5],&g_148,&g_107[0][2],&g_287[8][0],&g_107[3][5],&g_107[3][5],&g_287[0][0]},{&g_107[3][5],&g_107[3][5],&g_148,&g_287[9][0],&g_287[3][0],&g_287[3][0],&g_287[9][0],&g_148}},{{&g_287[8][0],&g_287[8][0],&g_107[3][6],&g_107[3][5],&g_107[1][4],&g_148,&g_107[3][5],&g_107[3][5]},{(void*)0,&g_107[3][5],&g_107[3][6],&g_148,&g_107[3][5],&g_107[3][6],&g_148,&g_107[3][5]},{&g_107[3][5],&g_287[4][0],(void*)0,&g_107[1][0],(void*)0,&g_287[4][0],&g_107[3][5],&g_148}},{{&g_148,&g_107[1][0],&g_107[3][5],&g_287[3][0],&g_107[1][0],&g_107[3][5],&g_107[3][5],&g_148},{&g_148,&g_148,&g_148,&g_287[4][0],&g_107[1][0],&g_107[3][6],&g_107[3][6],&g_107[1][0]},{&g_148,&g_148,&g_148,&g_148,(void*)0,&g_148,&g_107[3][5],&g_287[4][0]}}};
                uint16_t *l_638 = &l_426;
                union U4 *l_640 = &l_624;
                int i, j, k;
                l_390[4][3] = ((safe_mul_func_int8_t_s_s(((++(*l_608)) > (safe_div_func_uint64_t_u_u((0xDFL == ((safe_rshift_func_int8_t_s_s(l_617, ((safe_div_func_uint8_t_u_u(((*l_418) = (safe_mod_func_uint64_t_u_u((g_346 | ((4UL > (g_294[0][1] ^= (*p_43))) <= g_275.f0)), (safe_rshift_func_uint8_t_u_u(((l_624 , &g_104[4]) != (g_625 , &g_352[6])), 6))))), l_624.f0)) > (-7L)))) < 0x6ACCL)), l_390[6][0]))), l_624.f0)) || l_624.f0);
                --l_626;
                if (((((*l_640) = ((*l_566) = ((*l_565) = g_275))) , g_7) , 0xEF804F6EL))
                { /* block id: 271 */
                    uint32_t *l_647 = &l_372;
                    uint32_t **l_648 = &l_608;
                    uint32_t *l_650 = &l_333.f0;
                    uint32_t **l_649[7][7][5] = {{{&l_650,&l_647,&l_647,(void*)0,(void*)0},{(void*)0,&l_647,(void*)0,&l_650,&l_650},{(void*)0,(void*)0,&l_650,&l_647,(void*)0},{&l_647,(void*)0,&l_647,&l_647,&l_650},{(void*)0,&l_650,&l_650,(void*)0,&l_650},{&l_650,&l_650,(void*)0,(void*)0,(void*)0},{&l_647,(void*)0,&l_647,(void*)0,&l_647}},{{&l_650,&l_647,&l_650,(void*)0,&l_647},{&l_647,(void*)0,&l_650,&l_647,&l_650},{(void*)0,&l_650,&l_647,&l_647,&l_647},{(void*)0,&l_647,&l_650,&l_650,&l_647},{&l_647,(void*)0,(void*)0,(void*)0,(void*)0},{&l_647,&l_647,&l_647,&l_650,&l_650},{(void*)0,&l_650,&l_650,(void*)0,&l_650}},{{(void*)0,&l_650,(void*)0,&l_650,(void*)0},{&l_647,&l_650,&l_647,&l_650,&l_650},{&l_650,&l_647,(void*)0,(void*)0,(void*)0},{&l_647,(void*)0,(void*)0,&l_647,&l_650},{&l_650,&l_647,&l_647,&l_647,&l_647},{(void*)0,&l_650,(void*)0,&l_650,&l_647},{&l_647,(void*)0,&l_650,&l_647,(void*)0}},{{(void*)0,&l_647,&l_647,&l_647,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_650,&l_650,&l_650,&l_650,(void*)0},{&l_650,&l_650,&l_647,&l_650,&l_647},{&l_650,(void*)0,&l_650,(void*)0,&l_647},{&l_650,(void*)0,&l_650,&l_650,&l_650},{&l_650,&l_647,&l_647,(void*)0,(void*)0}},{{(void*)0,&l_647,(void*)0,&l_650,&l_650},{(void*)0,(void*)0,&l_650,&l_647,(void*)0},{(void*)0,&l_650,(void*)0,&l_650,(void*)0},{&l_647,&l_650,&l_647,(void*)0,&l_650},{&l_650,&l_647,&l_647,&l_650,(void*)0},{&l_650,&l_650,&l_647,&l_650,&l_650},{&l_647,(void*)0,&l_647,(void*)0,(void*)0}},{{&l_650,&l_650,&l_647,&l_650,&l_647},{&l_650,(void*)0,&l_647,(void*)0,(void*)0},{&l_647,&l_650,&l_647,&l_647,&l_650},{(void*)0,&l_650,(void*)0,&l_650,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&l_650},{&l_647,(void*)0,&l_650,(void*)0,(void*)0},{&l_650,&l_647,&l_650,(void*)0,(void*)0}},{{&l_650,(void*)0,&l_647,&l_650,(void*)0},{&l_647,(void*)0,&l_650,(void*)0,&l_650},{&l_650,&l_650,&l_650,&l_650,&l_647},{&l_650,&l_650,&l_647,(void*)0,(void*)0},{&l_647,(void*)0,&l_650,&l_647,&l_650},{(void*)0,&l_650,&l_650,(void*)0,(void*)0},{&l_650,(void*)0,(void*)0,&l_650,&l_650}}};
                    float *l_657 = (void*)0;
                    float **l_656 = &l_657;
                    float *l_659[2][2];
                    float **l_658 = &l_659[0][0];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_659[i][j] = &g_149[0][6][1];
                    }
                    (*l_506) = p_43;
                    g_170.f2 = ((((((safe_div_func_int8_t_s_s((safe_mod_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s(g_55, (((*l_648) = l_647) != (g_651 = p_43)))), (safe_sub_func_uint16_t_u_u((--(*l_638)), 0xF836L)))), 0x0CL)) ^ g_95) == (0UL | (((*l_656) = l_650) != ((*l_658) = p_43)))) != 0x1306L) == l_617) ^ l_660[0]);
                }
                else
                { /* block id: 279 */
                    int16_t l_661 = 4L;
                    float *l_674 = &g_149[2][0][2];
                    l_661 = (l_617 && l_380);
                    (*l_674) = (safe_div_func_float_f_f((((safe_mul_func_float_f_f((0xA.157FC9p-17 != ((safe_add_func_float_f_f(((safe_div_func_float_f_f((l_617 >= ((l_670 != (void*)0) >= (safe_sub_func_float_f_f(g_625.f0, g_202[0][5])))), (+((((g_170.f1 <= (1L <= ((-7L) || 0xF81589649B0F1365LL))) , l_624.f0) >= 0x3.C599E9p-2) >= g_294[0][1])))) < (-0x8.0p+1)), l_624.f0)) == 0xA.5DBBEBp+34)), g_287[7][0])) > g_153) <= l_661), l_661));
                }
                g_676 = l_675;
            }
lbl_738:
            (*l_506) = func_44((l_551 , &g_148));
            g_149[0][6][1] = ((l_690 = ((((l_679 |= (0x999B743CL < (*g_651))) , 0xF.093525p+8) != (&l_401[4] == &l_401[6])) < ((safe_mul_func_float_f_f(((0xB.CD3814p+12 == (safe_add_func_float_f_f((((g_233 = g_118) < (safe_sub_func_float_f_f((l_624.f0 , (safe_sub_func_float_f_f(0x0.Ap-1, l_624.f0))), l_624.f0))) <= 0xE.E9F656p+45), g_153))) <= 0x0.Bp+1), 0x9.23D302p+98)) != l_689))) < l_624.f0);
            if ((l_679 > ((safe_add_func_int32_t_s_s((0xCFL <= (safe_lshift_func_uint16_t_u_u(l_690, ((safe_add_func_int64_t_s_s((**g_424), ((~(safe_div_func_uint16_t_u_u((((g_284 , (l_689 , ((**l_559) = ((((*l_700) = (void*)0) != ((*l_702) = &g_651)) > ((l_704[1] , g_107[1][0]) | l_705))))) & 0xD23CL) <= (*p_43)), l_704[1].f2))) >= l_679))) >= 0x6DB42D4EL)))), 4294967295UL)) < l_626)))
            { /* block id: 293 */
                uint32_t l_708[2];
                int16_t ***l_712 = &g_677;
                union U1 *l_713 = (void*)0;
                union U1 *l_714[4][10][6] = {{{(void*)0,(void*)0,&g_397[0][4][1],&g_397[0][4][1],&g_241,&g_397[0][4][1]},{(void*)0,(void*)0,&g_397[0][4][1],&g_397[0][4][1],(void*)0,&g_397[0][4][1]},{&g_241,&g_397[0][4][1],&g_241,(void*)0,&g_397[0][4][1],&g_241},{&g_397[1][6][1],&g_397[1][6][2],&g_397[0][6][1],&g_397[0][4][1],&g_397[0][4][1],&g_241},{&g_241,&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_241},{&g_397[0][4][1],&g_241,&g_397[0][4][1],&g_397[0][4][1],&g_241,&g_397[1][8][0]},{(void*)0,&g_241,(void*)0,&g_397[0][4][1],&g_397[0][4][1],&g_241},{(void*)0,&g_397[1][0][0],&g_397[0][4][1],&g_397[0][4][1],&g_397[1][6][1],(void*)0},{&g_397[0][4][1],&g_241,&g_241,&g_397[0][4][1],&g_397[0][4][1],&g_241},{&g_241,&g_241,&g_397[1][0][0],&g_397[0][4][1],&g_241,&g_397[0][4][1]}},{{&g_397[1][6][1],(void*)0,&g_241,(void*)0,&g_241,(void*)0},{&g_241,&g_397[0][4][1],&g_397[1][6][1],&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1]},{(void*)0,&g_397[1][8][0],&g_241,&g_397[0][4][1],(void*)0,&g_397[0][4][1]},{(void*)0,&g_397[1][8][0],&g_397[0][4][1],&g_397[0][6][3],&g_397[0][4][1],&g_241},{(void*)0,&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_241,&g_397[0][6][1]},{&g_241,(void*)0,(void*)0,&g_241,&g_397[0][4][1],&g_397[0][6][1]},{&g_397[0][4][1],&g_241,&g_241,&g_397[1][8][2],&g_397[1][8][2],&g_241},{&g_241,&g_241,&g_397[0][4][1],&g_397[0][4][1],(void*)0,&g_397[0][4][1]},{&g_241,&g_397[1][8][0],(void*)0,&g_397[0][1][0],(void*)0,&g_397[0][4][1]},{&g_397[0][4][1],&g_241,(void*)0,(void*)0,&g_241,&g_397[0][4][1]}},{{&g_397[0][4][1],(void*)0,&g_397[0][4][1],&g_397[1][6][2],&g_241,&g_241},{&g_397[1][6][2],&g_241,&g_241,(void*)0,(void*)0,&g_397[0][6][1]},{&g_397[0][4][1],&g_397[0][4][1],&g_397[1][6][1],&g_397[0][4][1],&g_241,&g_397[0][4][1]},{&g_241,(void*)0,&g_241,&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1]},{&g_397[0][1][0],&g_397[0][4][1],(void*)0,&g_397[1][6][1],&g_397[0][4][1],(void*)0},{&g_397[1][8][0],&g_397[0][4][1],(void*)0,&g_397[1][6][1],&g_397[0][4][1],&g_397[0][4][1]},{&g_397[0][1][0],&g_241,(void*)0,&g_397[0][4][1],(void*)0,&g_241},{&g_241,&g_397[0][6][1],&g_241,&g_397[0][4][1],(void*)0,&g_397[1][0][0]},{&g_397[0][4][1],&g_397[0][4][1],&g_397[1][8][0],(void*)0,&g_241,&g_241},{&g_397[1][6][2],&g_241,&g_241,&g_397[1][6][2],(void*)0,&g_397[1][6][1]}},{{&g_397[0][4][1],&g_241,&g_241,(void*)0,(void*)0,&g_241},{&g_397[0][4][1],&g_397[1][6][2],&g_241,&g_397[0][1][0],(void*)0,&g_397[0][4][1]},{&g_241,&g_241,(void*)0,&g_397[0][4][1],(void*)0,&g_397[0][4][1]},{&g_241,&g_241,&g_397[0][4][1],&g_397[1][8][2],&g_241,(void*)0},{&g_397[0][4][1],&g_397[0][4][1],&g_397[0][4][1],&g_241,(void*)0,&g_241},{(void*)0,&g_397[0][6][1],&g_397[0][4][1],&g_397[0][6][1],(void*)0,&g_397[1][6][2]},{&g_397[0][6][3],&g_241,&g_397[1][8][2],&g_241,&g_397[0][4][1],(void*)0},{&g_397[1][6][1],&g_397[0][4][1],&g_397[1][0][0],&g_241,&g_397[0][4][1],(void*)0},{(void*)0,&g_397[0][4][1],&g_397[1][8][2],&g_397[1][0][0],&g_397[0][4][1],&g_397[1][6][2]},{&g_397[0][4][1],(void*)0,&g_397[0][4][1],(void*)0,&g_241,&g_241}}};
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_708[i] = 0xB17AAF9FL;
                l_714[0][8][2] = ((*g_398) = ((safe_sub_func_int16_t_s_s(l_708[0], (safe_sub_func_uint8_t_u_u(((safe_unary_minus_func_int64_t_s(1L)) | ((void*)0 != l_712)), (l_689 , (l_624.f0 != l_624.f0)))))) , l_713));
            }
            else
            { /* block id: 296 */
                const uint8_t l_728 = 255UL;
                for (l_333.f2 = 14; (l_333.f2 <= (-25)); l_333.f2 = safe_sub_func_uint32_t_u_u(l_333.f2, 1))
                { /* block id: 299 */
                    float l_730 = 0x6.6p-1;
                    int32_t l_731 = 0x686F4DB7L;
                    volatile union U0 **l_735 = &g_733;
                    for (g_170.f0 = (-25); (g_170.f0 != 53); g_170.f0 = safe_add_func_int32_t_s_s(g_170.f0, 3))
                    { /* block id: 302 */
                        uint32_t ***l_724 = &l_703;
                        uint32_t ****l_725 = &l_724;
                        l_732[6][3] = ((~((g_720 == ((*l_725) = l_724)) | (--(**l_559)))) , (l_728 | (+l_731)));
                    }
                    (*l_735) = g_733;
                }
                for (l_690 = (-29); (l_690 <= (-11)); l_690 = safe_add_func_uint8_t_u_u(l_690, 4))
                { /* block id: 311 */
                    if (g_170.f1)
                        goto lbl_738;
                }
            }
        }
    }
    return (*l_560);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_55 g_57
 */
static int32_t * func_44(int8_t * p_45)
{ /* block id: 5 */
    uint16_t l_47 = 0x6BCFL;
    int64_t *l_54 = &g_55;
    int16_t *l_56 = &g_57;
    int32_t l_58 = 0x29794902L;
    l_47--;
    l_58 = (safe_rshift_func_int16_t_s_s(l_47, ((*l_56) = (safe_add_func_int64_t_s_s(0L, ((*l_54) = (0x5D518144L | (1UL && 0xF0L))))))));
    return &g_7;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_95 g_55 g_275 g_2 g_189 g_236 g_105 g_7 g_75 g_170.f2 g_202 g_275.f0 g_286 g_287 g_170.f0 g_1626
 * writes: g_202 g_95 g_104 g_153 g_149 g_189 g_55 g_57 g_118 g_284 g_286 g_287 g_170.f0
 */
static const union U4  func_59(int32_t  p_60, int64_t  p_61, const uint16_t * p_62)
{ /* block id: 104 */
    union U1 l_254 = {0xDFB2C95BL};
    uint64_t *l_258 = &g_202[0][7];
    union U1 ** const **l_259 = (void*)0;
    uint16_t **l_264 = &g_104[1];
    float *l_265 = &g_153;
    int32_t l_266 = 3L;
    float *l_267 = &g_189[1];
    int32_t *l_269[6] = {&l_266,&g_75,&l_266,&l_266,&g_75,&l_266};
    int32_t **l_268 = &l_269[3];
    int16_t *l_274 = &g_57;
    uint8_t *l_281 = &g_118;
    int16_t *l_282 = (void*)0;
    int16_t *l_283 = &g_284;
    int16_t *l_285 = &g_286;
    int64_t *l_291 = &g_55;
    int64_t **l_290 = &l_291;
    int i;
    (*l_267) = (l_254 , (l_266 = (g_149[0][6][1] = (safe_add_func_float_f_f(l_254.f0, (0x1.9p+1 != ((((~(g_5 <= (g_95 ^= ((*l_258) = l_254.f0)))) , (((void*)0 == l_259) , ((*l_265) = (safe_sub_func_float_f_f((safe_div_func_float_f_f(((((*l_264) = &g_236) != (void*)0) <= g_95), 0x4.0B3A3Bp+43)), p_61))))) >= p_60) >= g_55)))))));
    (*l_268) = func_44(&g_148);
    g_287[9][0] ^= ((((*l_285) ^= (0x4DL | (safe_rshift_func_uint8_t_u_s((safe_sub_func_uint64_t_u_u((((*l_274) = p_61) == (g_275 , ((*l_283) = (((g_2 & (safe_sub_func_uint8_t_u_u(((*l_281) = (((g_189[1] , (~(((((0x5308L <= (((*p_62) == (*p_62)) & (((safe_mul_func_int16_t_s_s(g_5, (*p_62))) <= 0x36F7DA083C917601LL) != g_5))) || (**l_268)) > g_170.f2) > p_60) , g_202[0][5]))) == 0xF1L) , 0x37L)), g_275.f0))) && 1UL) || p_61)))), p_61)), 0)))) ^ 0x8476L) || g_202[0][5]);
    for (g_170.f0 = 8; (g_170.f0 == 26); g_170.f0++)
    { /* block id: 120 */
        int64_t **l_292 = &l_291;
        l_292 = l_290;
    }
    return g_275;
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_84 g_7 g_9 g_75 g_104 g_107 g_105 g_77 g_149 g_5 g_170 g_170.f0 g_170.f1 g_57 g_202 g_236 g_55 g_244
 * writes: g_2 g_55 g_57 g_95 g_75 g_118 g_148 g_153 g_149 g_202 g_189 g_236 g_244 g_77
 */
static uint16_t * func_64(uint64_t  p_65)
{ /* block id: 15 */
    int32_t l_92 = 9L;
    int8_t *l_106[5][8][3] = {{{(void*)0,&g_107[3][5],&g_107[2][3]},{&g_107[5][4],&g_107[5][4],&g_107[4][5]},{&g_107[3][5],&g_107[3][5],&g_107[2][1]},{&g_107[5][4],&g_107[3][5],&g_107[3][5]},{(void*)0,&g_107[3][6],&g_107[1][0]},{&g_107[3][5],&g_107[5][4],&g_107[3][5]},{&g_107[2][1],&g_107[2][5],&g_107[2][1]},{&g_107[5][0],&g_107[3][5],&g_107[4][5]}},{{(void*)0,&g_107[2][5],&g_107[2][3]},{&g_107[3][2],&g_107[5][4],(void*)0},{&g_107[3][5],&g_107[3][6],&g_107[2][1]},{&g_107[3][2],&g_107[3][5],&g_107[3][5]},{(void*)0,&g_107[3][5],&g_107[1][0]},{&g_107[5][0],&g_107[5][4],&g_107[3][5]},{&g_107[2][1],&g_107[3][5],&g_107[2][1]},{&g_107[3][5],&g_107[3][5],(void*)0}},{{(void*)0,&g_107[3][5],&g_107[2][3]},{&g_107[5][4],&g_107[5][4],&g_107[4][5]},{&g_107[3][5],&g_107[3][5],&g_107[2][1]},{&g_107[5][4],&g_107[3][5],&g_107[3][5]},{(void*)0,&g_107[3][6],&g_107[1][0]},{&g_107[3][5],&g_107[5][4],&g_107[3][5]},{&g_107[2][1],&g_107[2][5],&g_107[2][1]},{&g_107[5][0],&g_107[3][5],&g_107[4][5]}},{{(void*)0,&g_107[2][5],&g_107[2][3]},{&g_107[3][2],&g_107[5][4],(void*)0},{&g_107[3][5],&g_107[3][6],&g_107[2][1]},{&g_107[3][2],&g_107[3][5],&g_107[3][5]},{(void*)0,&g_107[3][5],&g_107[1][0]},{&g_107[5][0],&g_107[5][4],&g_107[3][5]},{&g_107[2][1],&g_107[3][5],&g_107[2][1]},{&g_107[3][5],&g_107[3][5],(void*)0}},{{(void*)0,&g_107[3][5],&g_107[2][3]},{&g_107[5][4],&g_107[5][4],&g_107[4][5]},{&g_107[3][5],&g_107[3][5],&g_107[2][1]},{&g_107[5][4],&g_107[3][5],&g_107[3][5]},{(void*)0,&g_107[3][6],&g_107[1][0]},{&g_107[3][5],&g_107[5][4],&g_107[3][5]},{&g_107[2][1],&g_107[2][5],&g_107[2][1]},{&g_107[5][0],&g_107[3][5],&g_107[4][5]}}};
    uint64_t *l_120[7][2] = {{&g_95,&g_95},{&g_95,&g_95},{&g_95,&g_95},{&g_95,&g_95},{&g_95,&g_95},{&g_95,&g_95},{&g_95,&g_95}};
    union U4 l_140 = {18446744073709551615UL};
    union U3 l_219 = {0x805F1B04L};
    int i, j, k;
    for (g_2 = 17; (g_2 < 13); g_2--)
    { /* block id: 18 */
        uint16_t l_87 = 0xEB95L;
        int8_t *l_93 = (void*)0;
        uint64_t *l_94 = &g_95;
        uint16_t *l_96 = &l_87;
        int32_t l_97 = 8L;
        int32_t *l_98 = (void*)0;
        int32_t *l_99 = &g_75;
        union U2 l_119 = {0};
        int8_t l_143 = 0xBAL;
        int32_t l_231 = (-6L);
        union U1 *l_242 = &g_241;
        union U1 * const ***l_247 = &g_244;
        (*l_99) = (safe_add_func_uint64_t_u_u((((*l_96) = (((4294967291UL || (((g_84 , (p_65 && (safe_lshift_func_int8_t_s_u(l_87, 4)))) ^ ((*l_94) = (safe_mul_func_uint8_t_u_u(p_65, (g_2 ^ (safe_add_func_uint64_t_u_u((((g_2 & l_92) , func_44(l_93)) != &l_92), g_7))))))) ^ 18446744073709551612UL)) & g_2) >= g_9)) != l_92), l_97));
        for (p_65 = 26; (p_65 <= 27); p_65 = safe_add_func_int64_t_s_s(p_65, 4))
        { /* block id: 24 */
            int32_t l_103 = 0xAA25E9F1L;
            int16_t *l_180 = (void*)0;
            int32_t *l_199 = (void*)0;
            int32_t l_200 = 1L;
            int32_t l_201 = 0xF01B28DBL;
            if ((~p_65))
            { /* block id: 25 */
                (*l_99) &= (l_103 & l_103);
                return g_104[4];
            }
            else
            { /* block id: 28 */
                uint8_t *l_117[9] = {&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118};
                int32_t l_125[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
                int i;
                l_125[0] = (((void*)0 != l_106[1][3][2]) >= (safe_add_func_float_f_f(((((((safe_sub_func_uint16_t_u_u(((~18446744073709551606UL) >= (safe_mod_func_int8_t_s_s((safe_add_func_uint64_t_u_u(((((g_118 = 0xCFL) || l_92) && (l_119 , ((l_120[5][1] != (void*)0) , (safe_rshift_func_int8_t_s_u(((safe_mul_func_uint16_t_u_u(l_92, 65530UL)) || 4UL), l_103))))) || l_125[0]), g_107[3][5])), p_65))), p_65)) , p_65) && p_65) , 0x7FB1L) | l_103) , p_65), p_65)));
            }
            if (l_103)
            { /* block id: 32 */
                for (g_57 = 0; (g_57 > (-26)); g_57 = safe_sub_func_uint16_t_u_u(g_57, 4))
                { /* block id: 35 */
                    int32_t *l_136[1];
                    int32_t **l_137 = &l_98;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_136[i] = &g_9;
                    l_103 &= (g_75 = (safe_lshift_func_int16_t_s_u(1L, (safe_mod_func_uint16_t_u_u((p_65 | (g_7 != (p_65 >= g_107[5][1]))), (safe_rshift_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(((l_92 >= p_65) , ((((*l_137) = l_136[0]) == ((safe_add_func_int64_t_s_s((((l_140 , 18446744073709551606UL) ^ p_65) | 0xC631L), g_105[0][4][1])) , &l_103)) | 255UL)), 0x6F30DCE9L)), 7)))))));
                }
            }
            else
            { /* block id: 40 */
                uint16_t l_160 = 0xF99DL;
                int32_t l_196 = (-7L);
                union U4 l_222 = {0UL};
                float *l_225 = &g_189[1];
                int32_t l_232 = (-7L);
                if ((safe_mul_func_int16_t_s_s((-10L), ((g_84 , &g_118) != (g_77 , (void*)0)))))
                { /* block id: 41 */
                    if (l_143)
                        break;
                }
                else
                { /* block id: 43 */
                    int32_t l_186 = 1L;
                    int32_t l_188 = 0xC0E31BEAL;
                    uint64_t * const l_223 = &g_95;
                    for (l_92 = 1; (l_92 >= 0); l_92 -= 1)
                    { /* block id: 46 */
                        uint64_t l_145 = 0UL;
                        float *l_152 = &g_153;
                        float *l_161 = &g_149[0][6][1];
                        int16_t *l_179 = &g_57;
                        int16_t **l_178 = &l_179;
                        union U4 l_185 = {0x121430DBL};
                        uint8_t *l_187[3];
                        int32_t *l_190 = (void*)0;
                        int32_t *l_191 = &l_97;
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_187[i] = &g_118;
                        (*l_161) = ((+(l_103 = (((l_145 != ((safe_div_func_float_f_f(0x5.1p-1, ((g_148 = (*l_99)) != g_149[0][6][1]))) <= (((safe_mul_func_float_f_f((((*l_152) = (-0x1.Dp+1)) <= ((void*)0 != l_120[(l_92 + 2)][l_92])), (safe_sub_func_float_f_f((safe_mul_func_float_f_f(g_149[0][6][1], (safe_mul_func_float_f_f(g_5, l_160)))), 0x1.Cp-1)))) >= 0x0.1p-1) == l_145))) < p_65) != 0x4.49B5EAp-67))) <= 0x5.0DD9F3p+90);
                        (*l_191) ^= ((*l_99) = ((safe_sub_func_int8_t_s_s((safe_add_func_int64_t_s_s((-1L), (safe_mul_func_int16_t_s_s((((l_103 < (g_170 , (safe_add_func_uint8_t_u_u((g_118 = ((safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint16_t_u_u((g_170.f0 | g_170.f1), (((safe_mul_func_uint8_t_u_u(0x0AL, (((*l_178) = g_104[(l_92 + 3)]) == l_180))) | (((safe_rshift_func_int8_t_s_s((l_186 |= ((safe_rshift_func_uint8_t_u_u((l_185 , g_107[2][1]), 4)) <= 3L)), l_92)) < 0xC7L) >= g_105[4][6][2])) >= g_57))))) & 0xB8C6L)), 0x2CL)))) ^ g_7) || 0UL), (-7L))))), l_188)) , 2L));
                    }
                    for (l_87 = 0; (l_87 != 29); ++l_87)
                    { /* block id: 59 */
                        float *l_195[10];
                        int8_t *l_197 = &g_107[4][3];
                        int32_t **l_198[6] = {&l_98,&l_98,&l_98,&l_98,&l_98,&l_98};
                        int16_t *l_224[4][2] = {{&g_57,&g_57},{&g_57,&g_57},{&g_57,&g_57},{&g_57,&g_57}};
                        int i, j;
                        for (i = 0; i < 10; i++)
                            l_195[i] = &g_149[0][6][1];
                        if (l_103)
                            break;
                        l_199 = func_44(((l_196 = (-0x7.3p+1)) , l_197));
                        --g_202[0][5];
                        (*l_99) = (safe_rshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(g_107[5][2], p_65)), ((void*)0 == l_94))) <= (safe_sub_func_int8_t_s_s((*l_99), (safe_div_func_int16_t_s_s((g_57 = (l_219 , ((safe_sub_func_int8_t_s_s((l_222 , ((l_223 != (void*)0) == 0xFD118F9FL)), l_219.f2)) , l_92))), l_219.f1))))), g_105[5][9][0])), p_65)), g_202[4][2]));
                    }
                }
                (*l_225) = g_75;
                for (g_148 = 20; (g_148 < 6); --g_148)
                { /* block id: 71 */
                    int8_t l_235 = (-7L);
                    for (l_200 = 22; (l_200 >= 3); --l_200)
                    { /* block id: 74 */
                        int32_t *l_230[3][7] = {{&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,&g_75},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,&g_75}};
                        int i, j;
                        ++g_236;
                    }
                    for (g_236 = 0; (g_236 <= 2); g_236 += 1)
                    { /* block id: 79 */
                        uint32_t l_239[10][6][4] = {{{8UL,0x2205B7ECL,4294967295UL,6UL},{2UL,0x05A58BF4L,0xE9770F3FL,0xF8DD1E84L},{0xC932F838L,0x65A5B556L,0x27EE8240L,0x65A5B556L},{4294967294UL,0xC932F838L,8UL,0xF43F8A5EL},{6UL,4294967295UL,1UL,0xEA506B42L},{4294967293UL,0UL,0x1AB4B5E3L,0xCD2D6C20L}},{{4294967293UL,0UL,1UL,0x27EE8240L},{6UL,0xCD2D6C20L,8UL,1UL},{4294967294UL,0x09BA9FC3L,0x27EE8240L,1UL},{0xC932F838L,0UL,0xE9770F3FL,0x56D79854L},{2UL,4294967294UL,4294967295UL,1UL},{0x696A46F0L,3UL,8UL,0UL}},{{0xCD2D6C20L,1UL,3UL,0UL},{0xF8DD1E84L,0x27EE8240L,0x2205B7ECL,4294967295UL},{0xF5A9DCD6L,0x56D79854L,0x56D79854L,0xF5A9DCD6L},{0x2205B7ECL,0UL,4294967293UL,4294967292UL},{0UL,2UL,0UL,0xEA506B42L},{4294967294UL,4294967295UL,1UL,0xEA506B42L}},{{8UL,2UL,0UL,4294967292UL},{6UL,0UL,6UL,0xF5A9DCD6L},{0UL,0x56D79854L,0UL,4294967295UL},{2UL,0x27EE8240L,1UL,0UL},{7UL,1UL,0x393A02F1L,0UL},{0x1B94480FL,3UL,4294967292UL,1UL}},{{0x56D79854L,0UL,0x1AB4B5E3L,3UL},{4294967295UL,4294967292UL,0x78321BB0L,0xC932F838L},{0xC932F838L,4294967294UL,0xC932F838L,8UL},{0UL,0x05A58BF4L,3UL,0x445221C5L},{4294967295UL,4294967295UL,4294967294UL,0x05A58BF4L},{0x445221C5L,0UL,4294967294UL,6UL}},{{4294967295UL,4294967293UL,3UL,0x56D79854L},{0UL,1UL,0xC932F838L,6UL},{0xC932F838L,6UL,0x78321BB0L,0x1B94480FL},{4294967295UL,0xF5A9DCD6L,0x1AB4B5E3L,0xF8DD1E84L},{0x56D79854L,0x1AB4B5E3L,4294967292UL,0x2205B7ECL},{0x1B94480FL,0x5642BD77L,0x393A02F1L,4294967293UL}},{{7UL,4294967295UL,1UL,0x09BA9FC3L},{2UL,3UL,0UL,0UL},{0UL,0UL,6UL,0x1AB4B5E3L},{6UL,0xDC9C5CF6L,0UL,0UL},{8UL,0UL,1UL,0UL},{4294967294UL,0UL,0UL,0UL}},{{0UL,0xDC9C5CF6L,4294967293UL,0x1AB4B5E3L},{0x2205B7ECL,0UL,0x56D79854L,0UL},{0xF5A9DCD6L,3UL,0x2205B7ECL,0x09BA9FC3L},{0xF8DD1E84L,4294967295UL,3UL,4294967293UL},{0xCD2D6C20L,0x5642BD77L,8UL,0x2205B7ECL},{0x696A46F0L,0x1AB4B5E3L,4294967295UL,0xF8DD1E84L}},{{0x27EE8240L,0xF5A9DCD6L,0xCD2D6C20L,0x1B94480FL},{1UL,6UL,0x445221C5L,6UL},{0UL,1UL,0x696A46F0L,0x56D79854L},{0UL,4294967293UL,0xE9770F3FL,6UL},{1UL,0UL,0x65A5B556L,0x05A58BF4L},{1UL,4294967295UL,0xE9770F3FL,0x445221C5L}},{{0UL,0x05A58BF4L,0x696A46F0L,8UL},{0UL,4294967294UL,0x445221C5L,0xC932F838L},{1UL,4294967292UL,0xCD2D6C20L,3UL},{0x27EE8240L,0UL,4294967295UL,1UL},{0xF43F8A5EL,0UL,0x696A46F0L,4294967295UL},{0x05A58BF4L,0xC932F838L,0x2205B7ECL,0UL}}};
                        int i, j, k;
                        if (l_239[6][3][1])
                            break;
                        if (g_55)
                            break;
                    }
                    return &g_236;
                }
            }
            for (l_97 = 6; (l_97 >= 0); l_97 -= 1)
            { /* block id: 88 */
                union U1 *l_240 = &g_241;
                union U1 **l_243 = &l_240;
                (*l_243) = (l_242 = l_240);
            }
            if (g_55)
                continue;
        }
        (*l_247) = g_244;
    }
    for (g_77 = (-28); (g_77 <= 52); g_77++)
    { /* block id: 98 */
        const int16_t *l_250 = &g_57;
        int32_t *l_251 = &g_75;
        (*l_251) = (l_250 == l_250);
        return &g_105[6][7][0];
    }
    return &g_105[5][9][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_77
 * writes: g_77
 */
static uint64_t  func_66(int16_t * p_67, union U4  p_68, int32_t  p_69, int8_t * p_70)
{ /* block id: 12 */
    int32_t *l_73 = (void*)0;
    int32_t *l_74 = &g_75;
    int32_t *l_76[2][8] = {{&g_5,&g_75,&g_9,&g_75,&g_5,&g_5,&g_75,&g_9},{&g_5,&g_5,&g_75,&g_9,&g_75,&g_5,&g_5,&g_75}};
    int i, j;
    g_77++;
    return p_69;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_27, "g_27", print_hash_value);
    transparent_crc(g_55, "g_55", print_hash_value);
    transparent_crc(g_57, "g_57", print_hash_value);
    transparent_crc(g_75, "g_75", print_hash_value);
    transparent_crc(g_77, "g_77", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_105[i][j][k], "g_105[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_107[i][j], "g_107[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_118, "g_118", print_hash_value);
    transparent_crc(g_148, "g_148", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc_bytes(&g_149[i][j][k], sizeof(g_149[i][j][k]), "g_149[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_153, sizeof(g_153), "g_153", print_hash_value);
    transparent_crc(g_170.f0, "g_170.f0", print_hash_value);
    transparent_crc(g_170.f1, "g_170.f1", print_hash_value);
    transparent_crc(g_170.f2, "g_170.f2", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_189[i], sizeof(g_189[i]), "g_189[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_202[i][j], "g_202[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_233, sizeof(g_233), "g_233", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_234[i], "g_234[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_236, "g_236", print_hash_value);
    transparent_crc(g_241.f0, "g_241.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_253[i][j][k], "g_253[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_275.f0, "g_275.f0", print_hash_value);
    transparent_crc(g_284, "g_284", print_hash_value);
    transparent_crc(g_286, "g_286", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_287[i][j], "g_287[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_294[i][j], "g_294[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_313, "g_313", print_hash_value);
    transparent_crc(g_346, "g_346", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_397[i][j][k].f0, "g_397[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_493[i], "g_493[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_625.f0, "g_625.f0", print_hash_value);
    transparent_crc(g_625.f1, "g_625.f1", print_hash_value);
    transparent_crc(g_625.f2, "g_625.f2", print_hash_value);
    transparent_crc(g_723, "g_723", print_hash_value);
    transparent_crc(g_758, "g_758", print_hash_value);
    transparent_crc(g_787, "g_787", print_hash_value);
    transparent_crc(g_795, "g_795", print_hash_value);
    transparent_crc(g_829, "g_829", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_963[i][j][k], "g_963[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1116, "g_1116", print_hash_value);
    transparent_crc(g_1162, "g_1162", print_hash_value);
    transparent_crc_bytes (&g_1205, sizeof(g_1205), "g_1205", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1233[i][j][k], "g_1233[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1254, "g_1254", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1313[i].f0, "g_1313[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1343, "g_1343", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1626[i], "g_1626[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1645, "g_1645", print_hash_value);
    transparent_crc(g_1784, "g_1784", print_hash_value);
    transparent_crc(g_1832, "g_1832", print_hash_value);
    transparent_crc(g_2050, "g_2050", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 588
XXX total union variables: 46

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 49
breakdown:
   indirect level: 0, occurrence: 14
   indirect level: 1, occurrence: 13
   indirect level: 2, occurrence: 10
   indirect level: 3, occurrence: 4
   indirect level: 4, occurrence: 7
   indirect level: 5, occurrence: 1
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 168
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 16
XXX times a single bitfield on LHS: 1
XXX times a single bitfield on RHS: 19

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 308
   depth: 2, occurrence: 81
   depth: 3, occurrence: 10
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 4
   depth: 7, occurrence: 4
   depth: 9, occurrence: 2
   depth: 10, occurrence: 2
   depth: 11, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 3
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 5
   depth: 20, occurrence: 7
   depth: 21, occurrence: 6
   depth: 22, occurrence: 4
   depth: 23, occurrence: 3
   depth: 24, occurrence: 3
   depth: 25, occurrence: 1
   depth: 27, occurrence: 2
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 628

XXX times a variable address is taken: 1346
XXX times a pointer is dereferenced on RHS: 261
breakdown:
   depth: 1, occurrence: 219
   depth: 2, occurrence: 35
   depth: 3, occurrence: 7
XXX times a pointer is dereferenced on LHS: 254
breakdown:
   depth: 1, occurrence: 232
   depth: 2, occurrence: 15
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
XXX times a pointer is compared with null: 41
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 7979

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1513
   level: 2, occurrence: 213
   level: 3, occurrence: 38
   level: 4, occurrence: 11
   level: 5, occurrence: 4
XXX number of pointers point to pointers: 215
XXX number of pointers point to scalars: 372
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 41.6
XXX average alias set size: 1.67

XXX times a non-volatile is read: 1632
XXX times a non-volatile is write: 839
XXX times a volatile is read: 9
XXX    times read thru a pointer: 4
XXX times a volatile is write: 3
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 888
XXX percentage of non-volatile access: 99.5

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 315
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 29
   depth: 2, occurrence: 47
   depth: 3, occurrence: 63
   depth: 4, occurrence: 78
   depth: 5, occurrence: 64

XXX percentage a fresh-made variable is used: 18.4
XXX percentage an existing variable is used: 81.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


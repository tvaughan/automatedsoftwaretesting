/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1248573797
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   unsigned f0 : 17;
   uint32_t  f1;
};

union U1 {
   volatile uint16_t  f0;
   uint32_t  f1;
   int32_t  f2;
   volatile uint16_t  f3;
   int64_t  f4;
};

/* --- GLOBAL VARIABLES --- */
static uint16_t g_2 = 0xE6DAL;
static uint32_t g_37 = 0x38F58595L;
static int32_t g_42[7] = {0xC82F744EL,0L,0xC82F744EL,0xC82F744EL,0L,0xC82F744EL,0xC82F744EL};
static const int32_t g_65 = 0x6E149F7FL;
static const int32_t g_67 = 1L;
static const int32_t *g_66 = &g_67;
static volatile int32_t g_69 = 0x2392E18EL;/* VOLATILE GLOBAL g_69 */
static int32_t g_70 = 0x6354D184L;
static int32_t *g_76 = &g_70;
static int32_t **g_75[7] = {&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76};
static int32_t **g_84[5][3][9] = {{{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76}},{{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76}},{{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,(void*)0,&g_76,(void*)0},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76}},{{&g_76,&g_76,&g_76,&g_76,(void*)0,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,(void*)0,&g_76,&g_76}},{{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76},{&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76,&g_76}}};
static union U0 g_91[10] = {{0UL},{0x6B242CFFL},{0x6B242CFFL},{0UL},{0x6B242CFFL},{0x6B242CFFL},{0UL},{0x6B242CFFL},{0x6B242CFFL},{0UL}};
static uint8_t g_92 = 1UL;
static const int32_t ** volatile g_94 = &g_66;/* VOLATILE GLOBAL g_94 */
static uint8_t g_102 = 0UL;
static uint8_t g_105 = 0x3FL;
static uint64_t g_109 = 1UL;
static int16_t g_112[7] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
static int8_t g_118 = 0L;
static uint32_t g_162[7] = {4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL,4294967293UL};
static union U1 g_181 = {0xD4F9L};/* VOLATILE GLOBAL g_181 */
static uint32_t g_182 = 18446744073709551609UL;
static int8_t g_206[1][5] = {{0xECL,0xECL,0xECL,0xECL,0xECL}};
static int64_t g_208 = 0x39DA6AA3D2066672LL;
static float * volatile g_221 = (void*)0;/* VOLATILE GLOBAL g_221 */
static float g_223 = 0x4.2p-1;
static uint8_t g_259 = 0x26L;
static int16_t g_262 = 4L;
static uint64_t *g_278 = &g_109;
static const int32_t ** volatile g_307 = (void*)0;/* VOLATILE GLOBAL g_307 */
static uint16_t g_340 = 65534UL;
static uint64_t g_342 = 0x1076E64F549E078DLL;
static float * volatile g_367[6][6] = {{(void*)0,&g_223,&g_223,&g_223,(void*)0,&g_223},{&g_223,(void*)0,&g_223,&g_223,&g_223,&g_223},{&g_223,&g_223,&g_223,(void*)0,&g_223,&g_223},{(void*)0,&g_223,&g_223,&g_223,(void*)0,&g_223},{&g_223,(void*)0,&g_223,&g_223,&g_223,&g_223},{&g_223,&g_223,&g_223,(void*)0,&g_223,&g_223}};
static int16_t g_388 = 0x97CEL;
static volatile int64_t g_398 = 0L;/* VOLATILE GLOBAL g_398 */
static int8_t g_469 = 0x8CL;
static uint64_t **g_485 = &g_278;
static uint64_t ** const *g_484 = &g_485;
static uint8_t g_504 = 0x16L;
static const int32_t ** volatile g_579 = (void*)0;/* VOLATILE GLOBAL g_579 */
static volatile union U1 g_587 = {0x4AEEL};/* VOLATILE GLOBAL g_587 */
static int8_t g_601 = 0L;
static int32_t ** volatile * volatile g_624 = &g_84[3][1][0];/* VOLATILE GLOBAL g_624 */
static int32_t ** volatile * volatile *g_623[5][4][3] = {{{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624}},{{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624}},{{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624}},{{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624}},{{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624},{&g_624,&g_624,&g_624}}};
static union U1 g_674 = {0xF718L};/* VOLATILE GLOBAL g_674 */
static union U1 *g_708 = &g_181;
static union U1 ** volatile g_707 = &g_708;/* VOLATILE GLOBAL g_707 */
static volatile int32_t g_722 = 0xD256650AL;/* VOLATILE GLOBAL g_722 */
static uint16_t g_750 = 0x5029L;
static const volatile float * volatile ** volatile * volatile g_766 = (void*)0;/* VOLATILE GLOBAL g_766 */
static const volatile float g_771 = 0xD.502A1Cp+47;/* VOLATILE GLOBAL g_771 */
static const volatile float * volatile g_770 = &g_771;/* VOLATILE GLOBAL g_770 */
static float *g_785 = &g_223;
static float * const *g_784 = &g_785;
static float * const **g_783 = &g_784;
static union U1 ** volatile g_806[4][4][9] = {{{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708}},{{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708}},{{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708}},{{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708},{&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708,&g_708}}};
static union U1 ** volatile g_807 = &g_708;/* VOLATILE GLOBAL g_807 */
static int16_t **g_811 = (void*)0;
static int16_t *** volatile g_815 = &g_811;/* VOLATILE GLOBAL g_815 */
static const int8_t g_864 = (-5L);
static volatile int32_t *g_866 = &g_69;
static union U0 *g_870 = &g_91[7];
static union U0 ** volatile g_869[6][5] = {{&g_870,&g_870,&g_870,&g_870,&g_870},{(void*)0,&g_870,(void*)0,&g_870,(void*)0},{&g_870,&g_870,&g_870,(void*)0,&g_870},{&g_870,&g_870,&g_870,(void*)0,(void*)0},{&g_870,&g_870,&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870,(void*)0,&g_870}};
static uint8_t g_921 = 0xE0L;
static volatile union U1 g_1066 = {0xECD6L};/* VOLATILE GLOBAL g_1066 */
static const int32_t g_1083[2][5] = {{0x858F518EL,0x858F518EL,0x858F518EL,0x858F518EL,0x858F518EL},{0x858F518EL,0x858F518EL,0x858F518EL,0x858F518EL,0x858F518EL}};
static volatile int8_t g_1104 = 0L;/* VOLATILE GLOBAL g_1104 */
static volatile union U1 g_1174 = {1UL};/* VOLATILE GLOBAL g_1174 */
static int32_t g_1211 = 0xBA93A3E0L;
static int8_t g_1215 = 0L;
static int8_t * const g_1214[10] = {&g_1215,&g_1215,&g_1215,&g_1215,&g_1215,&g_1215,&g_1215,&g_1215,&g_1215,&g_1215};
static int8_t * const *g_1213 = &g_1214[2];
static int8_t * const **g_1212 = &g_1213;
static volatile uint32_t * volatile g_1264 = (void*)0;/* VOLATILE GLOBAL g_1264 */
static volatile uint32_t g_1266[6] = {0x4E625FAAL,1UL,0x4E625FAAL,0x4E625FAAL,1UL,0x4E625FAAL};
static volatile uint32_t *g_1265 = &g_1266[4];
static volatile uint32_t * volatile *g_1263[3][10][8] = {{{&g_1265,&g_1264,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1265},{&g_1264,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1264},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1264,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1264,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1265},{&g_1264,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1264},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265}},{{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1264,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1264,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1265},{&g_1264,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265,&g_1264},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1264,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265}},{{&g_1264,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1264},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1264,&g_1264,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1264,&g_1265,&g_1264,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1264,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1265},{&g_1264,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1264,&g_1264},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265},{&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265,&g_1265}}};
static int8_t *g_1302 = &g_1215;
static int8_t **g_1301[9] = {&g_1302,&g_1302,&g_1302,&g_1302,&g_1302,&g_1302,&g_1302,&g_1302,&g_1302};
static int8_t ***g_1300 = &g_1301[5];
static int8_t **** volatile g_1299[1][3][7] = {{{&g_1300,&g_1300,&g_1300,(void*)0,(void*)0,&g_1300,&g_1300},{(void*)0,&g_1300,&g_1300,&g_1300,&g_1300,(void*)0,&g_1300},{&g_1300,(void*)0,(void*)0,&g_1300,&g_1300,&g_1300,(void*)0}}};
static uint16_t *g_1312[3] = {(void*)0,(void*)0,(void*)0};
static uint8_t g_1321[6] = {0x23L,0x23L,0x23L,0x23L,0x23L,0x23L};
static union U1 ** volatile g_1348[4] = {&g_708,&g_708,&g_708,&g_708};
static union U1 ** volatile g_1349 = (void*)0;/* VOLATILE GLOBAL g_1349 */
static volatile union U1 g_1505 = {65532UL};/* VOLATILE GLOBAL g_1505 */
static uint32_t g_1580 = 1UL;
static int32_t ** volatile g_1582 = &g_76;/* VOLATILE GLOBAL g_1582 */
static int32_t ** volatile g_1597 = &g_76;/* VOLATILE GLOBAL g_1597 */
static volatile union U1 g_1600 = {1UL};/* VOLATILE GLOBAL g_1600 */
static const int8_t *g_1625[3] = {&g_206[0][3],&g_206[0][3],&g_206[0][3]};
static const int8_t **g_1624 = &g_1625[0];
static const int8_t ***g_1623 = &g_1624;
static const int8_t ****g_1622[8][6] = {{&g_1623,&g_1623,&g_1623,&g_1623,&g_1623,&g_1623},{&g_1623,&g_1623,&g_1623,&g_1623,&g_1623,(void*)0},{(void*)0,(void*)0,&g_1623,(void*)0,(void*)0,&g_1623},{(void*)0,&g_1623,(void*)0,&g_1623,&g_1623,&g_1623},{&g_1623,&g_1623,&g_1623,&g_1623,&g_1623,&g_1623},{&g_1623,&g_1623,(void*)0,&g_1623,&g_1623,&g_1623},{&g_1623,&g_1623,&g_1623,(void*)0,&g_1623,(void*)0},{(void*)0,&g_1623,&g_1623,&g_1623,&g_1623,&g_1623}};
static volatile union U1 g_1641 = {0UL};/* VOLATILE GLOBAL g_1641 */
static union U1 ** volatile g_1675 = &g_708;/* VOLATILE GLOBAL g_1675 */
static uint16_t **g_1699 = &g_1312[0];
static float * volatile g_1787 = (void*)0;/* VOLATILE GLOBAL g_1787 */
static float g_1789 = (-0x1.Ep+1);
static float * volatile g_1788 = &g_1789;/* VOLATILE GLOBAL g_1788 */
static int32_t ** volatile g_1790 = &g_76;/* VOLATILE GLOBAL g_1790 */
static volatile int32_t **g_1826 = &g_866;
static volatile int32_t ** volatile * volatile g_1825[5][9][4] = {{{&g_1826,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826}},{{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826}},{{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0}},{{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826}},{{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826},{(void*)0,(void*)0,(void*)0,&g_1826},{(void*)0,&g_1826,&g_1826,(void*)0},{&g_1826,&g_1826,&g_1826,&g_1826},{&g_1826,(void*)0,&g_1826,&g_1826},{&g_1826,&g_1826,&g_1826,&g_1826}}};
static volatile int32_t ** volatile * volatile *g_1824 = &g_1825[3][3][3];
static volatile int32_t ** volatile * volatile * volatile * volatile g_1823 = &g_1824;/* VOLATILE GLOBAL g_1823 */
static int32_t ** volatile g_1850 = (void*)0;/* VOLATILE GLOBAL g_1850 */
static int32_t ** volatile g_1852 = &g_76;/* VOLATILE GLOBAL g_1852 */
static union U1 g_1860 = {0x9305L};/* VOLATILE GLOBAL g_1860 */
static volatile union U1 g_1892 = {4UL};/* VOLATILE GLOBAL g_1892 */
static uint8_t g_1905 = 4UL;
static const int32_t **g_2005 = &g_66;
static const int32_t ***g_2004 = &g_2005;
static const int32_t ****g_2003 = &g_2004;
static const int32_t *****g_2002 = &g_2003;
static int32_t ***g_2008 = &g_75[3];
static int32_t ****g_2007 = &g_2008;
static int32_t *****g_2006 = &g_2007;
static int32_t g_2060[6] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
static union U1 g_2075 = {0xF2C4L};/* VOLATILE GLOBAL g_2075 */
static float g_2126[1] = {0x7.C10739p-37};
static int32_t g_2136 = 0x96977408L;
static int8_t ****g_2141 = (void*)0;
static int8_t ***** volatile g_2140 = &g_2141;/* VOLATILE GLOBAL g_2140 */
static volatile union U1 g_2181 = {0xA483L};/* VOLATILE GLOBAL g_2181 */
static volatile union U1 g_2243[5][10] = {{{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L}},{{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L}},{{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L}},{{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L}},{{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L},{0xCDA7L}}};
static volatile union U1 g_2244[3][7] = {{{0x3B58L},{0x3B58L},{0x8897L},{0x3B58L},{0x3B58L},{0x8897L},{0x3B58L}},{{0x3B58L},{0UL},{0UL},{0x3B58L},{0UL},{0UL},{0x3B58L}},{{0UL},{0x3B58L},{0UL},{0UL},{0x3B58L},{0UL},{0UL}}};
static uint32_t **g_2303 = (void*)0;
static uint16_t **** volatile g_2336 = (void*)0;/* VOLATILE GLOBAL g_2336 */
static uint16_t **** volatile g_2337 = (void*)0;/* VOLATILE GLOBAL g_2337 */
static int8_t ** const *g_2351 = (void*)0;
static int8_t ** const **g_2350[2] = {&g_2351,&g_2351};
static int8_t ** const ***g_2349[6][4][10] = {{{&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0]},{&g_2350[1],&g_2350[1],&g_2350[0],(void*)0,&g_2350[0],&g_2350[1],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0]},{&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],(void*)0,&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[1]},{&g_2350[1],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],(void*)0,&g_2350[0],(void*)0,&g_2350[1],&g_2350[0]}},{{(void*)0,&g_2350[1],&g_2350[1],&g_2350[1],(void*)0,&g_2350[0],&g_2350[1],&g_2350[1],(void*)0,(void*)0},{&g_2350[1],&g_2350[0],(void*)0,&g_2350[1],&g_2350[0],(void*)0,&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1]},{&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1]},{&g_2350[1],&g_2350[0],&g_2350[1],&g_2350[0],(void*)0,&g_2350[0],(void*)0,(void*)0,&g_2350[0],&g_2350[0]}},{{(void*)0,&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[1]},{&g_2350[0],(void*)0,&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0]},{&g_2350[0],&g_2350[0],(void*)0,&g_2350[1],&g_2350[0],&g_2350[1],&g_2350[0],(void*)0,(void*)0,&g_2350[0]},{(void*)0,(void*)0,&g_2350[0],&g_2350[1],&g_2350[1],&g_2350[0],(void*)0,&g_2350[1],&g_2350[1],(void*)0}},{{&g_2350[0],(void*)0,&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],(void*)0,&g_2350[1],&g_2350[0],&g_2350[0]},{&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1]},{&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[1],&g_2350[1],&g_2350[0],&g_2350[0]},{&g_2350[1],&g_2350[1],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0]}},{{&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1]},{&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0]},{&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0]},{(void*)0,&g_2350[0],(void*)0,&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[1]}},{{&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],(void*)0,&g_2350[0],&g_2350[1],&g_2350[1],&g_2350[0]},{&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0]},{&g_2350[0],&g_2350[0],&g_2350[1],(void*)0,&g_2350[0],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0]},{&g_2350[0],&g_2350[1],&g_2350[1],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[0],&g_2350[1],&g_2350[1],&g_2350[0]}}};
static int16_t ***g_2375 = &g_811;
static int16_t ****g_2374 = &g_2375;
static int16_t *****g_2373 = &g_2374;
static int64_t g_2379 = 2L;
static uint32_t g_2383 = 0x6EB2C5C1L;
static uint16_t ***g_2404 = &g_1699;
static uint16_t ****g_2403[1][1][1] = {{{&g_2404}}};
static const int8_t ***** volatile g_2431 = &g_1622[6][1];/* VOLATILE GLOBAL g_2431 */
static uint16_t *****g_2497 = (void*)0;
static union U1 g_2507 = {65533UL};/* VOLATILE GLOBAL g_2507 */
static uint32_t g_2593[4] = {18446744073709551610UL,18446744073709551610UL,18446744073709551610UL,18446744073709551610UL};
static const int32_t g_2614 = (-3L);
static volatile union U1 g_2672[6][10] = {{{4UL},{1UL},{4UL},{65534UL},{5UL},{3UL},{1UL},{65535UL},{9UL},{5UL}},{{7UL},{0xC8BEL},{4UL},{5UL},{65534UL},{1UL},{0xC8BEL},{65535UL},{5UL},{5UL}},{{65535UL},{65535UL},{4UL},{9UL},{9UL},{4UL},{65535UL},{65535UL},{0x8D18L},{65534UL}},{{3UL},{0xE048L},{4UL},{5UL},{0x8D18L},{7UL},{0xE048L},{65535UL},{65534UL},{9UL}},{{1UL},{0UL},{4UL},{0x8D18L},{5UL},{65535UL},{0UL},{65535UL},{5UL},{0x8D18L}},{{4UL},{1UL},{4UL},{65534UL},{5UL},{3UL},{1UL},{65535UL},{9UL},{5UL}}};
static float g_2677 = (-0x3.6p+1);
static float * volatile g_2676[7][5] = {{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677},{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677},{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677},{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677},{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677},{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677},{&g_2677,&g_2677,&g_2677,&g_2677,&g_2677}};
static float * volatile g_2678 = &g_2677;/* VOLATILE GLOBAL g_2678 */
static float g_2793 = 0x4.2p+1;
static uint32_t *g_2830 = &g_37;
static volatile uint16_t g_2844 = 65535UL;/* VOLATILE GLOBAL g_2844 */
static union U0 ** volatile g_2900 = &g_870;/* VOLATILE GLOBAL g_2900 */
static volatile float g_2922 = 0x6.D795A3p-57;/* VOLATILE GLOBAL g_2922 */
static int32_t g_2936[7][5] = {{(-5L),0xE93F1F0EL,0L,0L,0L},{0x433E796EL,0x078F7E82L,0x433E796EL,(-1L),0x433E796EL},{(-5L),0L,0xE93F1F0EL,0xE93F1F0EL,0L},{0x29E2B45FL,(-1L),0xC8E058CFL,(-1L),0x29E2B45FL},{0L,0xE93F1F0EL,0xE93F1F0EL,0L,(-5L)},{0x433E796EL,(-1L),0x433E796EL,0x078F7E82L,0x433E796EL},{0L,0L,0L,0xE93F1F0EL,(-5L)}};
static float ** volatile **g_3008[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static float ** volatile *** volatile g_3007[9][3] = {{&g_3008[1],&g_3008[2],&g_3008[1]},{&g_3008[1],&g_3008[1],&g_3008[1]},{&g_3008[2],&g_3008[1],&g_3008[1]},{&g_3008[1],&g_3008[2],&g_3008[1]},{&g_3008[1],&g_3008[1],&g_3008[1]},{&g_3008[2],&g_3008[1],&g_3008[1]},{&g_3008[1],&g_3008[2],&g_3008[1]},{&g_3008[1],&g_3008[1],&g_3008[1]},{&g_3008[2],&g_3008[1],&g_3008[1]}};
static union U1 g_3082 = {0x4B30L};/* VOLATILE GLOBAL g_3082 */
static union U1 g_3083[2][2] = {{{0xCFC2L},{0xCFC2L}},{{0xCFC2L},{0xCFC2L}}};
static union U1 g_3115 = {5UL};/* VOLATILE GLOBAL g_3115 */


/* --- FORWARD DECLARATIONS --- */
static union U1  func_1(void);
static uint8_t  func_3(const uint32_t  p_4, int32_t  p_5);
static uint16_t  func_6(int16_t  p_7);
static uint32_t  func_8(int32_t  p_9);
static int32_t * func_13(int32_t * p_14, int32_t  p_15);
static union U0  func_25(uint32_t  p_26, uint32_t  p_27);
static float  func_47(uint32_t * p_48, float  p_49, int32_t  p_50, uint32_t * p_51);
static uint32_t * func_52(int8_t  p_53);
static uint16_t  func_56(uint8_t  p_57, int32_t * p_58, uint64_t  p_59);
static int32_t * func_60(const int32_t * p_61, int32_t * p_62);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3115
 * writes:
 */
static union U1  func_1(void)
{ /* block id: 0 */
    uint16_t l_10[9][1];
    uint32_t *l_2592[2];
    uint32_t *l_2594[7][10][1] = {{{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[3]},{&g_2593[0]},{&g_2593[1]},{&g_2593[0]},{&g_2593[3]}},{{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[3]},{(void*)0},{&g_2593[0]},{&g_2593[0]},{&g_2593[1]}},{{&g_2593[1]},{&g_2593[1]},{(void*)0},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[0]},{&g_2593[0]},{(void*)0},{&g_2593[3]}},{{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[3]},{&g_2593[0]},{&g_2593[1]},{&g_2593[0]},{&g_2593[3]}},{{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[3]},{(void*)0},{&g_2593[0]},{&g_2593[0]},{&g_2593[1]}},{{&g_2593[1]},{&g_2593[1]},{(void*)0},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[0]},{&g_2593[0]},{(void*)0},{&g_2593[3]}},{{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[1]},{&g_2593[3]},{&g_2593[0]},{&g_2593[1]},{&g_2593[0]},{&g_2593[3]}}};
    uint32_t *l_2595 = (void*)0;
    uint32_t *l_2596[2];
    uint32_t *l_2597 = &g_2593[1];
    uint32_t l_2600 = 0x9474CA92L;
    uint16_t ** const *l_2606 = &g_1699;
    const int32_t *l_2613 = &g_2614;
    uint32_t l_2617 = 0x7526983BL;
    uint64_t l_2618[8] = {0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL,0x4E1AA4A55B05672ELL};
    int32_t l_2624 = (-1L);
    union U0 l_2653 = {6UL};
    int64_t l_2659 = 1L;
    const uint64_t l_2660 = 0xC48DF3936FDE1AA9LL;
    int16_t l_2661 = 0xD095L;
    uint32_t l_2691 = 4294967291UL;
    int64_t l_2694 = 0x2BA0A1505DAFDC3BLL;
    uint32_t l_2720 = 4294967289UL;
    uint16_t l_2821 = 0xC78CL;
    int32_t l_2831 = 0x6846997DL;
    int32_t l_2833 = 0x828773A4L;
    int32_t l_2869 = 0x3FA0DA03L;
    int32_t l_2871 = 0x0F158596L;
    int32_t l_2874 = 0xF1143C0FL;
    int32_t l_2876 = 0xE908E6E4L;
    int32_t l_2878 = 4L;
    int32_t l_2879 = 1L;
    int32_t l_2882 = 0x6F1517C1L;
    int32_t l_2883 = 0x8F78DC38L;
    int32_t l_2884 = (-4L);
    int32_t l_2890 = 6L;
    uint16_t *****l_2904 = &g_2403[0][0][0];
    int64_t l_2975 = (-1L);
    uint8_t l_2976 = 1UL;
    float **l_3025 = &g_785;
    int32_t *l_3092[2];
    int32_t *****l_3100 = &g_2007;
    float l_3102 = 0xA.EF2F69p-48;
    int16_t l_3106 = (-5L);
    int64_t l_3107 = (-1L);
    int32_t l_3108 = (-1L);
    float l_3109[3][2] = {{0xE.F19079p+55,(-0x2.8p+1)},{0xE.F19079p+55,0xE.F19079p+55},{(-0x2.8p+1),0xE.F19079p+55}};
    int32_t l_3110[9] = {1L,0xA8621683L,0xA8621683L,1L,0xA8621683L,0xA8621683L,1L,0xA8621683L,0xA8621683L};
    int32_t l_3111 = 0xA6900F93L;
    uint8_t l_3112 = 1UL;
    int i, j, k;
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 1; j++)
            l_10[i][j] = 65535UL;
    }
    for (i = 0; i < 2; i++)
        l_2592[i] = &g_2593[1];
    for (i = 0; i < 2; i++)
        l_2596[i] = &g_2593[0];
    for (i = 0; i < 2; i++)
        l_3092[i] = &g_2060[3];
    return g_3115;
}


/* ------------------------------------------ */
/* 
 * reads : g_707 g_708 g_181 g_674 g_42 g_76 g_1211 g_2136 g_70 g_2075.f0
 * writes: g_1211 g_2136 g_70
 */
static uint8_t  func_3(const uint32_t  p_4, int32_t  p_5)
{ /* block id: 1146 */
    int32_t **l_2599 = &g_76;
    (*g_76) &= ((**g_707) , (g_42[1] != (!(((void*)0 == l_2599) != (-1L)))));
    return g_2075.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_162 g_208 g_2431 g_807 g_708 g_181 g_674 g_484 g_485 g_783 g_784 g_785 g_1302 g_1215 g_76 g_340 g_1905 g_1826 g_866 g_469 g_262 g_2005 g_2507 g_1623 g_1624 g_1625 g_206 g_278 g_109 g_342 g_1641.f3 g_1211 g_2136 g_70
 * writes: g_109 g_1622 g_278 g_1789 g_2126 g_223 g_1215 g_1211 g_2136 g_70 g_69 g_469 g_76 g_1860.f2 g_870 g_182 g_66 g_2497 g_105 g_208 g_342 g_112
 */
static uint16_t  func_6(int16_t  p_7)
{ /* block id: 1072 */
    uint64_t l_2412[3][7] = {{0x1E9A342769300B2ALL,0xF80AD8FBA4E74F3CLL,9UL,0xF80AD8FBA4E74F3CLL,1UL,1UL,0xF80AD8FBA4E74F3CLL},{0xE93D664719483A7ALL,0x1E9A342769300B2ALL,0xE93D664719483A7ALL,0xF80AD8FBA4E74F3CLL,0xF80AD8FBA4E74F3CLL,0xE93D664719483A7ALL,0x1E9A342769300B2ALL},{0xF80AD8FBA4E74F3CLL,0x1E9A342769300B2ALL,9UL,9UL,0x1E9A342769300B2ALL,0xF80AD8FBA4E74F3CLL,0x1E9A342769300B2ALL}};
    int8_t *****l_2417[5];
    int8_t ***l_2427[7][5] = {{&g_1301[0],(void*)0,&g_1301[1],&g_1301[2],&g_1301[2]},{&g_1301[8],&g_1301[3],&g_1301[8],&g_1301[1],&g_1301[8]},{&g_1301[0],&g_1301[2],(void*)0,(void*)0,&g_1301[2]},{&g_1301[8],&g_1301[1],&g_1301[5],&g_1301[1],&g_1301[8]},{&g_1301[2],(void*)0,(void*)0,&g_1301[2],&g_1301[0]},{&g_1301[8],&g_1301[1],&g_1301[8],&g_1301[3],&g_1301[8]},{&g_1301[2],&g_1301[2],&g_1301[1],(void*)0,&g_1301[0]}};
    union U0 *l_2472 = &g_91[8];
    int32_t l_2520 = 1L;
    int32_t l_2546 = 0x1D605E28L;
    int8_t *l_2559 = &g_206[0][3];
    int8_t *l_2560 = &g_601;
    int32_t l_2567 = 7L;
    uint64_t l_2570 = 18446744073709551615UL;
    int32_t l_2571[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    uint8_t *l_2572[6][8] = {{(void*)0,&g_259,&g_1321[4],&g_259,(void*)0,&g_102,&g_259,&g_1905},{&g_1321[3],(void*)0,&g_105,&g_1905,&g_259,&g_1321[3],&g_1321[3],&g_259},{&g_921,&g_105,&g_105,&g_921,&g_1905,&g_259,&g_259,&g_105},{&g_259,(void*)0,&g_1321[4],&g_1905,(void*)0,(void*)0,(void*)0,&g_1905},{&g_259,(void*)0,&g_259,&g_105,&g_259,&g_259,&g_1905,&g_921},{(void*)0,&g_105,&g_1905,&g_259,&g_1321[3],&g_1321[3],&g_259,&g_1905}};
    int32_t l_2573 = 0L;
    int32_t l_2574 = 0x7D45F3F9L;
    int32_t l_2575 = (-1L);
    int32_t **l_2578 = (void*)0;
    int32_t *l_2588 = &g_70;
    int32_t **l_2589 = &g_76;
    uint16_t l_2590 = 1UL;
    int i, j;
    for (i = 0; i < 5; i++)
        l_2417[i] = &g_2141;
    if ((safe_lshift_func_uint16_t_u_u(l_2412[1][1], (safe_div_func_int16_t_s_s(((safe_add_func_int8_t_s_s((l_2417[0] != l_2417[3]), (+247UL))) && ((safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_u(l_2412[2][6], (l_2427[1][4] != (void*)0))) | (0x3FL == ((p_7 && (-9L)) | g_162[1]))), p_7)), g_208)), 1L)) , l_2412[1][1])), l_2412[1][3])))))
    { /* block id: 1073 */
        uint64_t *l_2461 = &l_2412[1][0];
        int32_t l_2463 = (-7L);
        int16_t l_2464 = 2L;
        int32_t *l_2465[2][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
        int i, j;
        for (g_109 = 0; (g_109 > 59); g_109 = safe_add_func_uint16_t_u_u(g_109, 6))
        { /* block id: 1076 */
            const int8_t ****l_2430 = &g_1623;
            int32_t l_2449 = 0xECE83930L;
            uint64_t *l_2459[7] = {&g_342,&g_342,&g_342,&g_342,&g_342,&g_342,&g_342};
            uint64_t **l_2460 = &l_2459[1];
            uint64_t *l_2462 = (void*)0;
            int i;
            (*g_2431) = l_2430;
            (**g_1826) = ((safe_sub_func_int64_t_s_s((safe_mod_func_int8_t_s_s((safe_mod_func_int8_t_s_s((p_7 && (safe_sub_func_uint64_t_u_u(((*l_2461) = ((**g_807) , (safe_div_func_uint32_t_u_u(((((((*g_76) = (safe_mul_func_int16_t_s_s(l_2412[0][0], (~(((safe_div_func_int8_t_s_s(((*g_1302) |= (safe_div_func_uint16_t_u_u(((l_2449 , (((((safe_sub_func_uint16_t_u_u(65533UL, (safe_div_func_uint32_t_u_u((safe_add_func_uint8_t_u_u(((((p_7 != p_7) >= (+(((((***g_783) = (safe_div_func_float_f_f((((*l_2460) = ((**g_484) = l_2459[1])) != (l_2462 = l_2461)), p_7))) != l_2463) == l_2464) != l_2464))) , p_7) == 0L), l_2464)), l_2412[1][1])))) ^ l_2412[1][1]) | l_2412[1][1]) , 0x2819B087L) <= p_7)) != (-6L)), l_2412[1][4]))), l_2449)) , 9L) && 0x2EL))))) , g_340) > g_1905) != p_7) , p_7), l_2412[1][3])))), 0x126CA8B886543DC6LL))), 0xFCL)), p_7)), 0UL)) & 8L);
            l_2465[0][3] = func_52(p_7);
        }
    }
    else
    { /* block id: 1088 */
        uint64_t *** const l_2480 = &g_485;
        uint32_t l_2487 = 0x087A01B4L;
        int32_t l_2513 = 0x208F54F9L;
        int8_t l_2515 = 0L;
        int32_t l_2518 = 0xD41F1FB9L;
        for (g_1860.f2 = 0; (g_1860.f2 < (-15)); g_1860.f2 = safe_sub_func_int8_t_s_s(g_1860.f2, 5))
        { /* block id: 1091 */
            union U0 *l_2473 = &g_91[5];
            union U0 *l_2474 = &g_91[0];
            union U0 **l_2475 = &g_870;
            uint32_t *l_2492 = &g_182;
            int32_t l_2512 = 0x97A9AF58L;
            int32_t l_2514 = 0x23452D26L;
            uint16_t *****l_2519 = &g_2403[0][0][0];
            int64_t *l_2536[3][10] = {{&g_2379,&g_208,&g_2507.f4,&g_2507.f4,&g_208,&g_2379,&g_2379,&g_2379,&g_208,&g_2507.f4},{&g_2379,&g_208,&g_2379,&g_2507.f4,&g_2379,&g_2379,&g_2507.f4,&g_2379,&g_208,&g_2379},{&g_2379,&g_2379,&g_208,&g_208,&g_208,&g_2379,&g_2379,&g_2379,&g_2379,&g_208}};
            int32_t l_2537[8][4][3] = {{{0xAFD102C8L,9L,9L},{0L,9L,0L},{1L,0xAFD102C8L,0xF659855AL},{0L,0L,0xF659855AL}},{{0xAFD102C8L,1L,0L},{9L,0L,9L},{9L,0xAFD102C8L,0L},{0xAFD102C8L,9L,9L}},{{0L,9L,0L},{1L,0xAFD102C8L,0xF659855AL},{0L,0L,0xF659855AL},{9L,0L,0xF659855AL}},{{1L,(-5L),1L},{1L,9L,(-5L)},{9L,1L,1L},{(-5L),1L,0xF659855AL}},{{0L,9L,0L},{(-5L),(-5L),0L},{9L,0L,0xF659855AL},{1L,(-5L),1L}},{{1L,9L,(-5L)},{9L,1L,1L},{(-5L),1L,0xF659855AL},{0L,9L,0L}},{{(-5L),(-5L),0L},{9L,0L,0xF659855AL},{1L,(-5L),1L},{1L,9L,(-5L)}},{{9L,1L,1L},{(-5L),1L,0xF659855AL},{0L,9L,0L},{(-5L),(-5L),0L}}};
            uint64_t *l_2538 = &g_342;
            int16_t *l_2545 = &g_112[6];
            int i, j, k;
            if (l_2412[1][1])
                break;
            if ((safe_lshift_func_uint8_t_u_s(((((*l_2492) = (l_2412[0][4] < (safe_lshift_func_int8_t_s_s((((l_2473 = l_2472) == ((*l_2475) = l_2474)) != (safe_rshift_func_uint8_t_u_s(((l_2480 == (void*)0) | (!(safe_mod_func_uint8_t_u_u(p_7, l_2412[2][5])))), 4))), (((safe_mod_func_int64_t_s_s((((+(l_2487 >= ((safe_sub_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_u((8L != 0xEBDA3EF9L), l_2487)) , l_2487), l_2412[1][1])) ^ l_2412[1][5]))) <= p_7) >= l_2487), p_7)) & p_7) & p_7))))) , g_262) < p_7), 7)))
            { /* block id: 1096 */
                int32_t *l_2493 = &g_1860.f2;
                uint16_t *****l_2496 = &g_2403[0][0][0];
                int64_t *l_2516 = &g_2075.f4;
                int64_t *l_2517[9][8] = {{&g_2379,&g_2379,&g_208,&g_2379,&g_2379,&g_208,(void*)0,(void*)0},{&g_2379,&g_2379,&g_2379,&g_2379,&g_2379,&g_2379,&g_208,&g_2379},{(void*)0,&g_208,&g_2379,(void*)0,&g_2379,&g_208,(void*)0,&g_2379},{&g_2379,&g_2379,&g_208,(void*)0,(void*)0,&g_208,&g_2379,&g_2379},{&g_2379,(void*)0,&g_208,&g_2379,(void*)0,&g_2379,&g_208,(void*)0},{&g_2379,&g_208,&g_2379,&g_2379,&g_2379,&g_2379,&g_2379,&g_2379},{(void*)0,(void*)0,&g_208,&g_2379,&g_2379,&g_208,&g_2379,&g_2379},{&g_2379,&g_2379,&g_2379,&g_2379,&g_2379,&g_208,&g_208,&g_2379},{&g_2379,&g_208,&g_208,&g_2379,&g_2379,&g_2379,&g_2379,&g_2379}};
                int32_t l_2525 = 1L;
                int i, j;
                (*g_2005) = l_2493;
                l_2520 = ((safe_sub_func_int64_t_s_s(((g_2497 = l_2496) == ((0UL >= ((safe_div_func_int64_t_s_s((l_2518 &= (65530UL == ((((safe_mul_func_int16_t_s_s(p_7, l_2487)) && (((safe_add_func_uint8_t_u_u(((l_2513 = (safe_add_func_uint16_t_u_u((safe_unary_minus_func_uint8_t_u((g_2507 , ((safe_rshift_func_int8_t_s_s((p_7 , p_7), ((*g_1302) &= (safe_div_func_int32_t_s_s(((l_2512 &= 0x347BC196C296BDF4LL) > 2UL), 0xDE3D2A13L))))) || p_7)))), l_2487))) > l_2514), (***g_1623))) && 0x5736E5A0L) , 1L)) & 0UL) || l_2515))), 0x0D18BC3AC4F9E214LL)) != g_206[0][0])) , l_2519)), (*g_278))) == l_2412[1][4]);
                for (l_2513 = 0; (l_2513 == 1); l_2513 = safe_add_func_uint64_t_u_u(l_2513, 6))
                { /* block id: 1106 */
                    int32_t *l_2526[3][1];
                    uint64_t l_2527 = 0UL;
                    int i, j;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_2526[i][j] = &l_2514;
                    }
                    for (g_105 = 12; (g_105 < 11); g_105 = safe_sub_func_uint16_t_u_u(g_105, 6))
                    { /* block id: 1109 */
                        return l_2412[1][1];
                    }
                    l_2527++;
                }
            }
            else
            { /* block id: 1114 */
                return l_2512;
            }
            (**g_1826) = (((safe_mul_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(((l_2514 &= (g_208 |= (safe_sub_func_int64_t_s_s((-2L), (l_2512 = ((***l_2480) &= 0x69445449C2089B40LL)))))) >= l_2412[1][1]), 1L)), ((++(*l_2538)) && (p_7 == (((!(((((l_2520 = (!g_1641.f3)) , (safe_rshift_func_int16_t_s_u(((*l_2545) = (0xF4EA9184L & ((-8L) ^ l_2412[2][3]))), 11))) & p_7) & 0x8CL) | p_7)) & p_7) | 0UL))))) , l_2546) <= p_7);
            for (g_342 = 0; (g_342 <= 13); g_342 = safe_add_func_int16_t_s_s(g_342, 4))
            { /* block id: 1127 */
                uint64_t l_2549 = 0x3A436A7561D4531ALL;
                return l_2549;
            }
        }
    }
    return (**l_2589);
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_2 g_42 g_70 g_75 g_67 g_91 g_69 g_91.f0 g_76 g_2136 g_94 g_92 g_102 g_105 g_109 g_65 g_206 g_484 g_485 g_278 g_118 g_262 g_181.f1 g_162 g_342 g_259 g_66 g_181.f2 g_587 g_601 g_398 g_623 g_340 g_182 g_181.f0 g_674 g_208 g_469 g_707 g_674.f3 g_785 g_1789 g_2126 g_223 g_1582 g_1580 g_1597 g_1600 g_1622 g_1505.f0 g_1905 g_784 g_1213 g_1214 g_1215 g_1302 g_1623 g_1624 g_1625 g_1790 g_1826 g_866 g_1852 g_504 g_674.f0 g_587.f0 g_2002 g_2006 g_1266 g_1083 g_2075 g_750 g_1211 g_2003 g_2004 g_2005 g_1265 g_1104 g_864 g_2140 g_1600.f3 g_783 g_2181 g_388 g_2243 g_2244 g_1699 g_1312 g_1321 g_2141 g_770 g_771 g_2349 g_2373 g_2383 g_1212 g_2336 g_2403 g_112 g_2007 g_2075.f1
 * writes: g_37 g_66 g_70 g_84 g_92 g_102 g_105 g_109 g_112 g_118 g_2136 g_340 g_181.f1 g_75 g_162 g_342 g_262 g_469 g_601 g_223 g_708 g_504 g_1211 g_76 g_1215 g_1580 g_750 g_1622 g_921 g_1905 g_1789 g_2126 g_674.f4 g_870 g_181.f2 g_1860.f2 g_69 g_2002 g_2006 g_181.f4 g_91.f1 g_1860.f1 g_2075.f2 g_2141 g_2075.f1 g_388 g_1321 g_785 g_2303 g_2383 g_2403
 */
static uint32_t  func_8(int32_t  p_9)
{ /* block id: 1 */
    int32_t l_20 = 9L;
    union U0 l_32 = {0UL};
    int32_t l_1571 = 1L;
    float l_1572 = 0x3.47ACC8p-94;
    uint32_t *l_1579 = &g_1580;
    int32_t l_2061 = 0x8AAB9AC8L;
    const uint8_t l_2063 = 8UL;
    int32_t l_2066 = 4L;
    union U1 *l_2175 = &g_2075;
    int8_t ***l_2222 = &g_1301[2];
    uint64_t *l_2269 = &g_342;
    const uint64_t *l_2272 = &g_342;
    int8_t ** const ***l_2352 = &g_2350[0];
    const int8_t l_2363 = 0xE5L;
    int16_t *****l_2376 = &g_2374;
    int16_t *****l_2377[1][9][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
    int32_t l_2378 = 7L;
    int32_t l_2380 = 0x6F653951L;
    int32_t l_2381 = 0x6C3B8124L;
    int32_t l_2382[1][7] = {{0x73FC7AFDL,0x73FC7AFDL,0x73FC7AFDL,0x73FC7AFDL,0x73FC7AFDL,0x73FC7AFDL,0x73FC7AFDL}};
    int8_t l_2388 = 0xDEL;
    uint32_t l_2390 = 0x7047FC0EL;
    int16_t l_2408 = (-5L);
    int i, j, k;
lbl_2409:
    for (p_9 = 0; (p_9 <= 15); p_9 = safe_add_func_int32_t_s_s(p_9, 7))
    { /* block id: 4 */
        uint32_t *l_35 = &l_32.f1;
        uint32_t *l_36[3][9][2] = {{{&g_37,&g_37},{(void*)0,&g_37},{&g_37,(void*)0},{(void*)0,&g_37},{&g_37,(void*)0},{&g_37,&g_37},{(void*)0,(void*)0},{&g_37,&g_37},{(void*)0,&g_37}},{{&g_37,(void*)0},{(void*)0,&g_37},{&g_37,(void*)0},{&g_37,&g_37},{(void*)0,(void*)0},{&g_37,&g_37},{(void*)0,&g_37},{(void*)0,&g_37},{&g_37,(void*)0}},{{&g_37,&g_37},{&g_37,(void*)0},{&g_37,&g_37},{(void*)0,&g_37},{&g_37,&g_37},{(void*)0,&g_37},{&g_37,(void*)0},{&g_37,&g_37},{&g_37,(void*)0}}};
        const int32_t *l_64[3];
        const int32_t **l_63[2];
        int32_t *l_2059[8] = {&g_2060[3],&g_2060[3],&g_2060[3],&g_2060[3],&g_2060[3],&g_2060[3],&g_2060[3],&g_2060[3]};
        int16_t *l_2062 = &g_262;
        int64_t l_2064 = 0x69B2524E1215561CLL;
        int16_t *l_2065[7];
        int64_t *l_2067 = (void*)0;
        int64_t *l_2068 = &g_181.f4;
        uint16_t l_2165 = 1UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_64[i] = &g_65;
        for (i = 0; i < 2; i++)
            l_63[i] = &l_64[1];
        for (i = 0; i < 7; i++)
            l_2065[i] = &g_388;
        (*g_1582) = func_13(((safe_div_func_int16_t_s_s((safe_add_func_int8_t_s_s(l_20, (l_20 >= ((safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_u((func_25((safe_mul_func_uint16_t_u_u((safe_div_func_int64_t_s_s((l_32 , (safe_div_func_uint32_t_u_u((--g_37), g_2))), ((((safe_div_func_int8_t_s_s(g_42[1], 254UL)) ^ ((*l_2068) = (((l_2066 ^= (safe_div_func_uint32_t_u_u(((l_32.f0 || ((((safe_mul_func_int16_t_s_s(((*l_2062) = (((l_2061 &= ((((func_47(func_52(((l_1571 &= (safe_rshift_func_uint16_t_u_s(func_56(p_9, func_60((g_66 = (void*)0), l_35), g_65), l_32.f0))) != l_32.f0)), l_32.f0, g_67, l_1579) > p_9) , (void*)0) != l_64[0]) || p_9)) , l_2059[5]) != (void*)0)), l_32.f0)) , p_9) , 0xB47936CBA8693954LL) == 0xC8C47D00CD1DBDECLL)) , l_2063), l_2064))) || 0xEDF4L) < 0x93L))) > p_9) , 0x1ADB1F9A50F5FFFDLL))), 0xA2B0L)), g_1083[1][4]) , p_9), 14)), 0L)) & 0x79L)))), p_9)) , &l_20), l_2165);
        if (l_20)
            break;
    }
    for (g_2075.f1 = 0; (g_2075.f1 > 12); ++g_2075.f1)
    { /* block id: 982 */
        uint8_t l_2203 = 246UL;
        int64_t *l_2204 = &g_181.f4;
        int16_t ***l_2252[1];
        int16_t ****l_2251[3];
        int32_t *l_2278 = &l_20;
        int32_t *l_2289[6] = {&g_1211,&g_1211,(void*)0,&g_1211,&g_1211,(void*)0};
        union U0 l_2322 = {0x9D242D67L};
        int i;
        for (i = 0; i < 1; i++)
            l_2252[i] = &g_811;
        for (i = 0; i < 3; i++)
            l_2251[i] = &l_2252[0];
        for (g_181.f4 = (-27); (g_181.f4 >= 11); ++g_181.f4)
        { /* block id: 985 */
            union U0 l_2176[6] = {{0x112B941AL},{0x112B941AL},{0x112B941AL},{0x112B941AL},{0x112B941AL},{0x112B941AL}};
            int32_t l_2225 = 0xD00C4687L;
            int32_t *l_2229[7];
            uint16_t l_2239 = 0xEC8AL;
            float **l_2249 = (void*)0;
            float ***l_2248 = &l_2249;
            uint32_t **l_2301[4] = {&l_1579,&l_1579,&l_1579,&l_1579};
            uint16_t ***l_2335 = &g_1699;
            uint64_t ** const *l_2357 = &g_485;
            int i;
            for (i = 0; i < 7; i++)
                l_2229[i] = &l_2225;
            if (((safe_mul_func_uint8_t_u_u((((safe_mul_func_uint8_t_u_u((p_9 || (l_2175 == (void*)0)), (l_2176[1] , (safe_sub_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((-1L) <= 0xCD35490DL), p_9)), p_9))))) , g_2181) , g_1211), 0xF3L)) >= p_9))
            { /* block id: 986 */
                uint8_t l_2209 = 3UL;
                uint8_t *l_2210 = &g_105;
                int8_t ***l_2221 = &g_1301[5];
                int32_t l_2228[1][6][1];
                int32_t l_2274 = 0xADA85AC7L;
                int i, j, k;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_2228[i][j][k] = 0xA0211C2EL;
                    }
                }
                (**g_1826) = ((1UL ^ ((safe_rshift_func_int8_t_s_s((safe_add_func_uint8_t_u_u(g_69, ((safe_rshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_s((safe_div_func_int32_t_s_s((((~(safe_rshift_func_uint8_t_u_s(((*l_2210) = (safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s((l_2203 = 0xBC39L), 15)), 0x84L)) , (l_2203 < ((l_2204 == (((safe_sub_func_float_f_f((safe_mul_func_float_f_f(0x9.9C51B1p+59, ((((l_1571 > p_9) > 0x2.9p+1) < p_9) <= p_9))), l_2209)) , l_2203) , (void*)0)) != 0UL))), 6)), l_32.f0))), p_9))) < 65535UL) ^ l_2176[1].f0), p_9)), 5)), 1)) >= 0xA17FL))), 5)) != p_9)) != 0x249C78A0L);
                l_1571 |= (safe_mul_func_int8_t_s_s(6L, (safe_rshift_func_int16_t_s_u(l_2203, p_9))));
                if (p_9)
                    continue;
                if ((safe_sub_func_int8_t_s_s(((safe_lshift_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((l_2221 == l_2222), ((l_2176[1].f0 < p_9) , p_9))), 3)) > ((*l_1579)++)), (l_2225 = (*g_1302)))))
                { /* block id: 994 */
                    int32_t *l_2226 = &g_70;
                    uint32_t l_2247 = 0xF0BC0F61L;
                    l_2229[6] = func_13(l_2226, (l_2228[0][5][0] = (!0x8A9B1481L)));
                    for (l_2225 = 0; (l_2225 >= 0); l_2225 -= 1)
                    { /* block id: 999 */
                        int16_t *l_2234 = &g_262;
                        int32_t l_2240 = 0x59D0C1D5L;
                        uint16_t *l_2250 = &g_340;
                        int i, j, k;
                        (**g_1826) = (safe_sub_func_uint16_t_u_u(((((safe_lshift_func_int16_t_s_u((g_388 ^= ((*l_2234) = 0x7585L)), ((safe_rshift_func_uint8_t_u_s((p_9 , ((safe_mul_func_uint8_t_u_u((l_2239 > ((*l_2250) = ((l_2240 | (2L || (*g_76))) & ((((safe_mod_func_int8_t_s_s(((*g_1302) = ((((g_2243[3][7] , (g_2244[1][5] , (((safe_add_func_uint32_t_u_u(l_2228[0][5][0], l_2240)) , p_9) | p_9))) != 0xB3L) != (*l_2226)) || l_2247)), 7L)) > g_91[8].f0) , l_2248) != (void*)0)))), l_2061)) & l_2240)), p_9)) >= 0x5C30L))) , l_32.f0) < p_9) > 4294967286UL), l_1571));
                    }
                }
                else
                { /* block id: 1006 */
                    int16_t ****l_2253 = &l_2252[0];
                    uint16_t *l_2254 = &g_750;
                    uint8_t *l_2265 = &g_504;
                    union U0 l_2268 = {0x71648138L};
                    uint64_t **l_2270 = (void*)0;
                    uint64_t **l_2271 = &l_2269;
                    const uint64_t **l_2273 = &l_2272;
                    uint64_t *l_2275 = &g_342;
                    (**g_1826) = (l_2228[0][5][0] = (((*l_2254) = (l_2251[0] == l_2253)) >= (((**g_485) , (*g_1699)) == (((g_1321[5] ^= (safe_mul_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s((((safe_div_func_int16_t_s_s((g_1905 != ((((*g_278) ^ ((((*l_2265) = ((*l_2210)--)) & (((*l_2275) = (safe_sub_func_int64_t_s_s((l_2268 , ((((*l_2271) = l_2269) != ((*l_2273) = l_2272)) ^ l_2274)), (-10L)))) == p_9)) | p_9)) >= l_2203) ^ l_2061)), p_9)) , l_2061) , l_2228[0][5][0]), 0x09L)), 0)), l_2203))) < 0xE8L) , (*g_1699)))));
                    if ((**g_1826))
                        break;
                    for (g_1215 = 0; (g_1215 <= 5); g_1215 = safe_add_func_uint8_t_u_u(g_1215, 4))
                    { /* block id: 1019 */
                        return l_2203;
                    }
                }
            }
            else
            { /* block id: 1023 */
                float *l_2298[7][2] = {{&g_2126[0],&g_2126[0]},{&g_2126[0],&g_2126[0]},{&g_2126[0],&g_2126[0]},{&g_2126[0],&g_2126[0]},{&g_2126[0],&g_2126[0]},{&g_2126[0],&g_2126[0]},{&g_2126[0],&g_2126[0]}};
                int8_t ****l_2300[6];
                int8_t *****l_2299 = &l_2300[2];
                uint32_t ***l_2302[3];
                int32_t ****l_2304 = &g_2008;
                uint64_t l_2333 = 18446744073709551615UL;
                int32_t l_2365 = 0x648EDCD6L;
                int32_t l_2366 = 2L;
                int32_t l_2367 = 8L;
                int32_t l_2368 = 0x6BAB54C7L;
                int32_t l_2369[3];
                int i, j;
                for (i = 0; i < 6; i++)
                    l_2300[i] = &l_2222;
                for (i = 0; i < 3; i++)
                    l_2302[i] = &l_2301[1];
                for (i = 0; i < 3; i++)
                    l_2369[i] = 0x1C5541E8L;
                l_2289[3] = func_13(l_2278, (safe_mul_func_int16_t_s_s(0xFBF5L, ((l_20 = (safe_mul_func_uint16_t_u_u((((safe_rshift_func_uint16_t_u_s((safe_div_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_s(((void*)0 != l_2289[0]), (safe_rshift_func_uint8_t_u_s((safe_add_func_uint32_t_u_u(0x9F272DAAL, 1L)), 7)))) > (((safe_mul_func_uint16_t_u_u((*l_2278), p_9)) == (safe_rshift_func_int16_t_s_u(((g_785 = l_2298[4][1]) == l_2229[2]), p_9))) > l_2061)), p_9)), 2)) != p_9) , l_20), p_9))) | (-1L)))));
                if (((((*g_2140) != ((*l_2299) = &g_1300)) , ((((g_2303 = l_2301[1]) == l_2301[1]) & (l_2304 == (void*)0)) | (safe_sub_func_uint32_t_u_u((safe_add_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((*g_770) , ((p_9 || p_9) | g_182)), l_2061)), 0x55L)), (*g_278))), p_9)))) || p_9))
                { /* block id: 1029 */
                    uint16_t l_2317[4] = {0xB484L,0xB484L,0xB484L,0xB484L};
                    uint16_t *l_2318[8][3] = {{&g_340,&g_340,&g_340},{&g_340,(void*)0,&g_340},{&g_340,&g_340,&g_340},{&g_340,(void*)0,&g_340},{&g_340,&g_340,&g_340},{&g_340,(void*)0,&g_340},{&g_340,&g_340,&g_340},{&g_340,(void*)0,&g_340}};
                    int32_t *l_2334 = &g_2136;
                    int i, j;
                    if (((g_1321[1] , ((l_2333 = (((((l_32.f0 > (l_1571 &= (safe_sub_func_int16_t_s_s((*l_2278), (g_112[6] = (safe_mod_func_int8_t_s_s(((**g_485) | 0x7CC61FBE054320CCLL), l_2317[0]))))))) == ((((safe_rshift_func_uint16_t_u_u((~(l_2322 , ((safe_mul_func_uint16_t_u_u(p_9, (safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_add_func_int8_t_s_s((safe_add_func_int8_t_s_s(p_9, 0x44L)), p_9)), l_2063)), l_2317[1])))) , (*l_2278)))), 14)) != 9UL) , p_9) != 1L)) , 65533UL) == p_9) > p_9)) && p_9)) , p_9))
                    { /* block id: 1033 */
                        uint16_t ****l_2338 = &l_2335;
                        l_2334 = &l_2225;
                        (*l_2338) = l_2335;
                    }
                    else
                    { /* block id: 1036 */
                        if ((*l_2334))
                            break;
                        (*l_2334) |= (p_9 < (&g_1299[0][0][0] == &g_1622[6][1]));
                        (*g_866) = p_9;
                        (***g_2003) = (void*)0;
                    }
                }
                else
                { /* block id: 1042 */
                    uint64_t ** const l_2360 = (void*)0;
                    uint64_t ** const *l_2359 = &l_2360;
                    int32_t l_2361 = 0L;
                    int32_t l_2362 = 0x303774D2L;
                    for (g_105 = (-4); (g_105 < 27); g_105 = safe_add_func_uint8_t_u_u(g_105, 9))
                    { /* block id: 1045 */
                        uint32_t l_2345[6];
                        int8_t * const ***l_2354 = &g_1212;
                        int8_t * const *** const *l_2353 = &l_2354;
                        uint64_t ** const **l_2358[1];
                        int32_t l_2364 = 1L;
                        uint8_t l_2370 = 1UL;
                        int i;
                        for (i = 0; i < 6; i++)
                            l_2345[i] = 0UL;
                        for (i = 0; i < 1; i++)
                            l_2358[i] = &l_2357;
                        l_2362 |= ((safe_div_func_int32_t_s_s((safe_add_func_uint16_t_u_u(((l_2345[3] != ((((((~(p_9 , g_1580)) || (((safe_sub_func_uint64_t_u_u((((l_2352 = g_2349[3][1][1]) == l_2353) , ((safe_div_func_uint32_t_u_u((((l_2359 = l_2357) != (void*)0) , (((0x3EL <= ((1L && l_2361) , 0x75L)) < l_2361) != 8UL)), 0xB344362DL)) | p_9)), (*l_2278))) , (void*)0) == (void*)0)) == 18446744073709551615UL) || l_2345[4]) < p_9) < 0UL)) , p_9), 0xA62EL)), 7L)) >= 0x61L);
                        (*l_2278) &= l_2363;
                        l_2370--;
                        l_2377[0][8][0] = (l_2376 = g_2373);
                    }
                }
            }
        }
        --g_2383;
    }
    (*g_2005) = func_60(((***g_2003) = (((((7UL ^ p_9) , (((((void*)0 == &g_1824) & ((safe_mod_func_int16_t_s_s(l_2388, ((((~(l_2390 & p_9)) < l_1571) > ((*g_76) ^= 0xE1040B63L)) , p_9))) & (-7L))) || l_2381) > (***g_1212))) <= l_20) ^ p_9) , &l_2378)), &l_20);
    for (g_1580 = 28; (g_1580 == 57); g_1580 = safe_add_func_uint16_t_u_u(g_1580, 5))
    { /* block id: 1064 */
        uint16_t *****l_2405 = &g_2403[0][0][0];
        int32_t l_2406 = 0xB70E2D7CL;
        uint16_t *l_2407 = &g_340;
        (*g_76) = (((safe_rshift_func_uint16_t_u_u(((safe_unary_minus_func_int16_t_s((-10L))) && (((safe_unary_minus_func_uint32_t_u(((1UL != ((safe_sub_func_uint32_t_u_u(p_9, ((((((*l_2407) = (((((g_70 , (safe_add_func_int8_t_s_s((**g_1213), l_2382[0][6]))) , g_2336) != ((*l_2405) = g_2403[0][0][0])) , l_2406) && l_2063)) | p_9) & g_112[6]) | 0x774B7937L) < (-1L)))) , g_206[0][3])) && l_2406))) , (void*)0) != (*g_2006))), l_2408)) , g_262) == 0UL);
        if (g_208)
            goto lbl_2409;
        (***g_783) = p_9;
    }
    return p_9;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_13(int32_t * p_14, int32_t  p_15)
{ /* block id: 974 */
    int32_t *l_2166[10];
    int i;
    for (i = 0; i < 10; i++)
        l_2166[i] = &g_2136;
    p_14 = p_14;
    return l_2166[5];
}


/* ------------------------------------------ */
/* 
 * reads : g_2075 g_750 g_1623 g_1624 g_1625 g_1905 g_259 g_92 g_484 g_485 g_278 g_109 g_1597 g_76 g_1211 g_1826 g_866 g_2003 g_2004 g_2005 g_1265 g_1266 g_262 g_1860.f1 g_1104 g_2136 g_864 g_2075.f2 g_2140 g_181.f1 g_1600.f3 g_783 g_784 g_785 g_223 g_1789 g_2126
 * writes: g_750 g_340 g_92 g_1580 g_70 g_69 g_66 g_162 g_37 g_91.f1 g_1860.f1 g_2075.f2 g_2141 g_181.f1 g_223 g_2136 g_1789 g_2126
 */
static union U0  func_25(uint32_t  p_26, uint32_t  p_27)
{ /* block id: 931 */
    uint16_t *l_2076 = &g_750;
    uint16_t *l_2079 = &g_340;
    int32_t l_2082 = (-1L);
    uint32_t l_2083 = 4294967295UL;
    uint16_t l_2084[7] = {0x5D7CL,0x5D7CL,0x5D7CL,0x5D7CL,0x5D7CL,0x5D7CL,0x5D7CL};
    uint8_t *l_2085 = &g_92;
    int8_t * const *l_2102 = &g_1214[6];
    union U0 l_2131 = {4294967289UL};
    uint64_t l_2132 = 0xE32B8073F35227E0LL;
    uint32_t l_2137 = 9UL;
    uint32_t *l_2148 = (void*)0;
    uint32_t *l_2149 = &g_1860.f1;
    uint32_t *l_2150 = &g_1860.f1;
    uint32_t *l_2151 = &g_181.f1;
    float l_2164[3][3];
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            l_2164[i][j] = (-0x1.7p+1);
    }
    if (((((p_26 | ((((((((*l_2085) ^= ((safe_div_func_int32_t_s_s(((18446744073709551612UL || ((safe_add_func_int64_t_s_s(p_27, (safe_mul_func_uint16_t_u_u(((*l_2079) = (g_2075 , (++(*l_2076)))), (((((((safe_sub_func_uint16_t_u_u((((**g_1623) != (void*)0) != ((l_2082 && (((((((&g_112[4] == (void*)0) , l_2082) && p_26) >= 0x1097L) , g_1905) ^ (-1L)) || 0x08L)) == l_2082)), p_26)) || p_27) == p_26) == l_2083) ^ 18446744073709551609UL) != l_2084[0]) < g_259))))) , 0x155AEE4DE7F71252LL)) == l_2084[6]), 0x9C9A99D2L)) > 0x78F7L)) || l_2083) | l_2084[0]) , p_27) & l_2084[0]) ^ l_2083) == p_27)) || p_26) & l_2084[0]) >= p_26))
    { /* block id: 935 */
        uint16_t l_2096 = 0UL;
        int16_t *l_2099 = (void*)0;
        int16_t *l_2100 = (void*)0;
        int8_t **l_2101 = &g_1302;
        (**g_1826) = (safe_rshift_func_uint16_t_u_u((((((**g_1597) = ((((safe_rshift_func_uint8_t_u_u((p_27 | ((safe_mul_func_uint8_t_u_u((safe_div_func_int16_t_s_s((l_2096 ^ 0x9C689FF3L), p_27)), (((l_2082 ^= (safe_add_func_uint16_t_u_u(0xE9DFL, 65526UL))) == l_2096) >= (((***g_484) != (l_2101 != l_2102)) < (-1L))))) > l_2084[6])), p_27)) ^ 0UL) & p_26) == p_26)) <= g_1211) >= l_2096) && l_2084[1]), 2));
    }
    else
    { /* block id: 939 */
        int32_t *l_2103 = (void*)0;
        uint8_t l_2124 = 0x5CL;
        int32_t l_2128 = 0xF0284A6CL;
        (***g_2003) = l_2103;
        for (p_26 = 0; (p_26 < 20); p_26 = safe_add_func_int8_t_s_s(p_26, 3))
        { /* block id: 943 */
            uint32_t l_2122 = 0xFB4F793AL;
            int32_t l_2123 = 0x119F7A43L;
            uint32_t *l_2125 = &g_162[1];
            uint32_t *l_2127 = &g_37;
            uint32_t *l_2129 = (void*)0;
            uint32_t *l_2130 = (void*)0;
            if ((l_2084[2] && ((((!(l_2082 > (((p_26 > (safe_mul_func_int16_t_s_s(0L, p_27))) != (safe_div_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u((g_91[8].f1 = ((((safe_add_func_int16_t_s_s((!((5UL != (safe_div_func_uint32_t_u_u(((((*l_2127) = (p_27 < ((0xF233E68EL & ((*l_2125) = ((safe_sub_func_int8_t_s_s((l_2124 = (((safe_rshift_func_int8_t_s_s((l_2122 = (0x8B9188FDL & p_26)), p_27)) == l_2123) ^ l_2123)), 0x55L)) , (*g_1265)))) == p_26))) <= g_109) , 0x0290027BL), p_26))) & l_2128)), p_27)) ^ l_2128) | g_262) < p_26)), 0xBC6AF4C5L)) > p_27), (-1L)))) >= 0x99A1C6C0L))) == p_26) | l_2084[3]) , 249UL)))
            { /* block id: 949 */
                for (g_1860.f1 = 0; (g_1860.f1 <= 5); g_1860.f1 += 1)
                { /* block id: 952 */
                    return l_2131;
                }
            }
            else
            { /* block id: 955 */
                l_2123 &= ((*g_76) = (p_26 | ((((l_2132 && (+(safe_lshift_func_int16_t_s_u(((void*)0 == &l_2076), 6)))) <= (g_1104 , g_2136)) & 0x948756993133B8B3LL) | g_864)));
            }
            if (l_2137)
                break;
        }
        for (g_2075.f2 = 26; (g_2075.f2 != 19); g_2075.f2--)
        { /* block id: 963 */
            (*g_2140) = &g_1300;
            if (p_26)
                continue;
        }
    }
    (*g_866) = (safe_add_func_int32_t_s_s(0x85D862F8L, ((*g_76) = ((safe_lshift_func_uint8_t_u_s((((l_2082 = l_2131.f0) ^ (&l_2131 == &l_2131)) != ((safe_sub_func_float_f_f((((++(*l_2151)) , ((*g_785) = ((!((g_1600.f3 , (safe_add_func_float_f_f(l_2084[3], (-l_2132)))) > (safe_sub_func_float_f_f((((safe_lshift_func_int16_t_s_u(((safe_div_func_int8_t_s_s(l_2084[5], p_27)) != g_259), p_26)) && l_2084[0]) , (***g_783)), (***g_783))))) < 0x1.5p-1))) != l_2164[0][0]), 0x0.E5491Dp-3)) , 0x6ABAL)), p_27)) , p_26))));
    return l_2131;
}


/* ------------------------------------------ */
/* 
 * reads : g_1582 g_1211 g_102 g_1580 g_1597 g_1600 g_76 g_70 g_75 g_67 g_91 g_69 g_91.f0 g_94 g_92 g_105 g_109 g_65 g_2 g_1622 g_1505.f0 g_1905 g_162 g_42 g_484 g_485 g_278 g_206 g_784 g_785 g_223 g_1213 g_1214 g_1215 g_1302 g_1623 g_1624 g_1625 g_342 g_1790 g_1826 g_866 g_1852 g_262 g_504 g_674.f0 g_587.f0 g_2002 g_2006 g_340 g_1266 g_1789 g_2126
 * writes: g_76 g_1211 g_601 g_340 g_92 g_1215 g_66 g_70 g_84 g_102 g_105 g_109 g_112 g_118 g_1580 g_750 g_469 g_1622 g_921 g_1905 g_223 g_674.f4 g_870 g_342 g_181.f2 g_262 g_1860.f2 g_69 g_2002 g_2006 g_1789 g_2126
 */
static float  func_47(uint32_t * p_48, float  p_49, int32_t  p_50, uint32_t * p_51)
{ /* block id: 695 */
    int32_t *l_1581 = &g_70;
    union U1 *l_1583 = &g_674;
    union U1 **l_1584[6] = {&g_708,&l_1583,&g_708,&g_708,&l_1583,&g_708};
    union U1 *l_1585 = &g_674;
    int64_t *l_1618 = (void*)0;
    const int8_t ***l_1651 = &g_1624;
    const uint16_t *l_1693 = &g_750;
    const uint16_t **l_1692 = &l_1693;
    uint32_t l_1731 = 0xE055F703L;
    uint16_t l_1741 = 5UL;
    int32_t l_1746[1][3][3];
    uint16_t l_1757 = 65534UL;
    const int32_t *l_1818 = (void*)0;
    int32_t l_1835[1];
    uint16_t l_1839 = 65534UL;
    float **l_1863 = &g_785;
    float **l_1875 = &g_785;
    int16_t ***l_2025 = &g_811;
    int8_t l_2027 = 0xEDL;
    int64_t l_2032 = 0x6E4AE4714FE20FE0LL;
    uint16_t l_2033 = 0x3723L;
    float *** const l_2057 = &l_1875;
    float *** const *l_2056 = &l_2057;
    uint8_t l_2058 = 0xD9L;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 3; k++)
                l_1746[i][j][k] = 0x6A1C43C9L;
        }
    }
    for (i = 0; i < 1; i++)
        l_1835[i] = 0xBE93573FL;
    (*g_1582) = l_1581;
    l_1585 = l_1583;
    for (g_1211 = 0; (g_1211 <= 17); g_1211 = safe_add_func_uint32_t_u_u(g_1211, 6))
    { /* block id: 700 */
        int8_t l_1646 = 0x4FL;
        union U1 *l_1660[1][9][2] = {{{&g_181,&g_181},{&g_181,&g_674},{&g_674,&g_181},{&g_181,&g_674},{&g_181,&g_674},{&g_181,&g_674},{&g_181,&g_181},{&g_674,&g_674},{&g_181,&g_181}}};
        int32_t l_1672 = 0xE95732DEL;
        union U0 l_1691 = {0x2EAB32D8L};
        const int32_t *l_1722 = &g_65;
        int32_t l_1732 = 0x258F850BL;
        uint32_t *l_1815 = (void*)0;
        uint32_t **l_1814[3];
        uint32_t ***l_1813[10][9][2] = {{{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[0],&l_1814[2]},{&l_1814[2],&l_1814[1]},{&l_1814[1],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[0],&l_1814[2]}},{{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[2],(void*)0},{(void*)0,&l_1814[2]},{&l_1814[2],&l_1814[2]},{(void*)0,&l_1814[2]},{(void*)0,&l_1814[2]},{&l_1814[2],&l_1814[2]}},{{&l_1814[0],&l_1814[0]},{&l_1814[2],(void*)0},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{(void*)0,&l_1814[2]},{&l_1814[2],(void*)0},{&l_1814[2],&l_1814[2]},{(void*)0,&l_1814[2]},{&l_1814[2],&l_1814[1]}},{{&l_1814[2],(void*)0},{&l_1814[2],&l_1814[0]},{&l_1814[0],&l_1814[2]},{&l_1814[2],&l_1814[2]},{(void*)0,&l_1814[2]},{(void*)0,&l_1814[2]},{&l_1814[2],&l_1814[2]},{(void*)0,(void*)0},{&l_1814[2],&l_1814[1]}},{{&l_1814[2],(void*)0},{&l_1814[2],&l_1814[0]},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[0],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[0]}},{{&l_1814[2],(void*)0},{&l_1814[2],&l_1814[1]},{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[1],(void*)0},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[2]}},{{&l_1814[1],&l_1814[2]},{(void*)0,&l_1814[2]},{&l_1814[2],&l_1814[1]},{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[1]},{&l_1814[2],&l_1814[2]},{(void*)0,&l_1814[2]}},{{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[1]},{&l_1814[2],(void*)0},{&l_1814[1],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[1]},{&l_1814[2],(void*)0}},{{&l_1814[2],&l_1814[0]},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[0],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[0]},{&l_1814[2],(void*)0}},{{&l_1814[2],&l_1814[1]},{&l_1814[1],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[2]},{&l_1814[1],(void*)0},{&l_1814[2],&l_1814[1]},{&l_1814[2],&l_1814[2]},{&l_1814[2],&l_1814[2]},{&l_1814[1],&l_1814[2]}}};
        uint64_t l_1909 = 0UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1814[i] = &l_1815;
        for (g_601 = 0; (g_601 == (-21)); g_601 = safe_sub_func_int32_t_s_s(g_601, 5))
        { /* block id: 703 */
            const int32_t *l_1601 = &g_65;
            int16_t l_1619 = 1L;
            float l_1636 = 0x0.6p-1;
            int16_t *l_1643 = (void*)0;
            int16_t **l_1642 = &l_1643;
            int16_t *l_1652 = &g_262;
            uint8_t *l_1673 = &g_1321[3];
            uint32_t l_1716 = 0x8BB4CDC5L;
            int32_t l_1759 = 0x43478B8DL;
            int32_t *l_1817 = &g_70;
            int64_t l_1819 = 0x4A38C85977DDD516LL;
            union U0 l_1822[9] = {{0x09E012D7L},{0x09E012D7L},{0x09E012D7L},{0x09E012D7L},{0x09E012D7L},{0x09E012D7L},{0x09E012D7L},{0x09E012D7L},{0x09E012D7L}};
            int32_t l_1838 = 0x30DC89AAL;
            int i;
            for (g_340 = 0; (g_340 > 1); g_340 = safe_add_func_int8_t_s_s(g_340, 2))
            { /* block id: 706 */
                union U1 *l_1596 = &g_674;
                for (g_92 = 25; (g_92 < 1); --g_92)
                { /* block id: 709 */
                    union U1 *l_1595 = (void*)0;
                    l_1596 = ((g_102 , (~(*p_51))) , l_1595);
                    (*g_1597) = p_51;
                }
                for (g_1215 = 0; (g_1215 >= (-28)); --g_1215)
                { /* block id: 715 */
                    const int32_t **l_1602 = &g_66;
                    (*l_1602) = func_60((g_1600 , ((*l_1602) = func_60(l_1601, func_60(func_60(((*l_1602) = l_1581), (*g_1597)), p_51)))), p_48);
                    if (p_50)
                        continue;
                }
                for (g_750 = 0; (g_750 == 18); g_750 = safe_add_func_int32_t_s_s(g_750, 7))
                { /* block id: 723 */
                    int8_t l_1621[1];
                    uint32_t l_1637 = 0x3CDBFEC5L;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1621[i] = (-10L);
                    for (g_469 = 8; (g_469 >= (-4)); --g_469)
                    { /* block id: 726 */
                        int16_t *l_1620 = &g_112[4];
                        const int8_t *****l_1626 = &g_1622[6][1];
                        int16_t *l_1635 = (void*)0;
                        int32_t l_1638[6][3];
                        int i, j;
                        for (i = 0; i < 6; i++)
                        {
                            for (j = 0; j < 3; j++)
                                l_1638[i][j] = (-1L);
                        }
                        l_1638[5][1] = (safe_mul_func_float_f_f((safe_add_func_float_f_f(((+(((0UL | (safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_u(0x09L, p_50)), (l_1621[0] = (safe_mul_func_int16_t_s_s((l_1618 != (void*)0), ((*l_1620) = l_1619))))))) , ((*l_1626) = g_1622[6][1])) != ((safe_mul_func_int16_t_s_s((safe_add_func_int64_t_s_s((safe_rshift_func_int16_t_s_u(((*l_1581) &= (safe_add_func_int32_t_s_s(p_50, 0x22A93F1BL))), 7)), 0x475992CFCD8BAC95LL)), l_1637)) , (void*)0))) > 0x8.033C12p-49), 0x1.49304Bp+28)), 0xD.1FBD4Dp-66));
                    }
                    return (*l_1601);
                }
            }
        }
        for (g_921 = 0; (g_921 <= 50); g_921 = safe_add_func_int64_t_s_s(g_921, 1))
        { /* block id: 805 */
            int32_t *l_1844 = &g_70;
            union U0 *l_1898 = &l_1691;
            int16_t *l_1899 = (void*)0;
            int16_t *l_1900 = (void*)0;
            int16_t *l_1901[5][6] = {{&g_112[6],&g_388,&g_112[6],&g_112[6],&g_388,&g_112[6]},{&g_112[6],&g_388,&g_112[6],&g_112[6],&g_388,&g_112[6]},{&g_112[6],&g_388,&g_112[6],&g_112[6],&g_388,&g_112[6]},{&g_112[6],&g_388,&g_112[6],&g_112[6],&g_388,&g_112[6]},{&g_112[6],&g_388,&g_112[6],&g_112[6],&g_388,&g_112[6]}};
            uint8_t *l_1908 = &g_92;
            const int16_t *l_1915 = (void*)0;
            const int16_t **l_1914[4] = {&l_1915,&l_1915,&l_1915,&l_1915};
            const int16_t ***l_1913[4];
            int32_t l_1938 = 0xCF5FBFDFL;
            int i, j;
            for (i = 0; i < 4; i++)
                l_1913[i] = &l_1914[1];
            l_1844 = p_51;
            for (g_1580 = 24; (g_1580 < 48); g_1580 = safe_add_func_int8_t_s_s(g_1580, 4))
            { /* block id: 809 */
                int16_t *l_1849[4] = {&g_112[5],&g_112[5],&g_112[5],&g_112[5]};
                int32_t l_1859 = 0L;
                int i;
            }
            l_1818 = func_60(func_60(func_60(l_1844, func_60((((((g_1505.f0 != ((l_1732 = (l_1898 == &g_91[8])) & ((*l_1581) = ((safe_lshift_func_uint8_t_u_s((((*l_1844) = 0xF83FL) <= (((((void*)0 != &g_1212) , ((*l_1908) = (safe_unary_minus_func_uint8_t_u(((g_1905--) | ((*p_48) ^ (0xD6L != (*l_1581)))))))) & g_42[5]) | l_1909)), p_50)) <= (*l_1722))))) ^ (***g_484)) != g_206[0][1]) , p_50) , (void*)0), p_51)), p_51), p_51);
            for (l_1839 = (-10); (l_1839 != 45); l_1839 = safe_add_func_int16_t_s_s(l_1839, 5))
            { /* block id: 840 */
                uint32_t l_1924 = 0x1F3A6309L;
                uint64_t *l_1931 = &l_1909;
                int64_t *l_1932 = &g_674.f4;
                if ((0x55F8L && (((((**g_784) = (!(**g_784))) , l_1913[2]) != &g_811) & ((safe_div_func_int8_t_s_s((safe_add_func_int64_t_s_s(((*l_1932) = ((*g_485) != ((safe_lshift_func_int8_t_s_u((((*l_1931) = ((*g_278) = (((*g_76) = ((((safe_rshift_func_int16_t_s_s(l_1924, 3)) ^ ((*g_1302) = (safe_rshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s((**g_1213), (safe_sub_func_uint32_t_u_u((((p_50 | ((*l_1818) , (*l_1722))) == l_1924) == l_1924), 0x9CC2072DL)))), p_50)))) > g_1505.f0) && p_50)) ^ p_50))) >= p_50), 4)) , (void*)0))), p_50)), (***g_1623))) <= p_50))))
                { /* block id: 847 */
                    union U0 *l_1935 = &g_91[8];
                    union U0 **l_1936 = (void*)0;
                    union U0 **l_1937 = &g_870;
                    (**l_1875) = ((safe_add_func_float_f_f((*l_1722), l_1924)) < ((void*)0 != (*l_1651)));
                    (*l_1937) = l_1935;
                }
                else
                { /* block id: 850 */
                    if (l_1938)
                        break;
                }
            }
        }
    }
    for (g_342 = 16; (g_342 < 40); g_342++)
    { /* block id: 858 */
        int32_t *l_1941 = &g_1860.f2;
        uint8_t l_1983 = 0x38L;
        uint8_t l_2001[8] = {0x27L,0xF0L,0xF0L,0x27L,0xF0L,0xF0L,0x27L,0xF0L};
        int32_t l_2028 = 1L;
        int32_t l_2029[6];
        int i;
        for (i = 0; i < 6; i++)
            l_2029[i] = (-1L);
        if (p_50)
        { /* block id: 859 */
            l_1941 = (*g_1790);
            return (**g_784);
        }
        else
        { /* block id: 862 */
            union U0 **l_1942 = &g_870;
            int32_t l_1979 = 0x8FA645A9L;
            int32_t l_2030[3];
            int i;
            for (i = 0; i < 3; i++)
                l_2030[i] = (-10L);
            (*l_1942) = (void*)0;
            for (l_1731 = 0; (l_1731 != 21); ++l_1731)
            { /* block id: 866 */
                uint32_t l_1969[4][7] = {{0xD3C3A58AL,0xD3C3A58AL,6UL,0xD3C3A58AL,0xD3C3A58AL,6UL,0xD3C3A58AL},{0xD3C3A58AL,0UL,0UL,0xD3C3A58AL,0UL,0UL,0xD3C3A58AL},{0UL,0xD3C3A58AL,0UL,0UL,0xD3C3A58AL,0UL,0UL},{0xD3C3A58AL,0xD3C3A58AL,6UL,0xD3C3A58AL,0xD3C3A58AL,0xD3C3A58AL,0UL}};
                union U0 l_1978 = {4294967293UL};
                int64_t l_2021 = 1L;
                int32_t l_2023 = 0xE29EA414L;
                int32_t l_2031 = 1L;
                int i, j;
                for (g_181.f2 = 2; (g_181.f2 >= 0); g_181.f2 -= 1)
                { /* block id: 869 */
                    uint8_t l_1956 = 0x45L;
                    for (g_601 = 2; (g_601 >= 0); g_601 -= 1)
                    { /* block id: 872 */
                        int32_t l_1945 = 2L;
                        int32_t *l_1946 = &l_1835[0];
                        int32_t *l_1947 = &g_674.f2;
                        int32_t *l_1948 = (void*)0;
                        int32_t *l_1949 = &g_674.f2;
                        int32_t *l_1950 = &l_1945;
                        int32_t *l_1951 = &g_1211;
                        int32_t *l_1952 = &g_1211;
                        int32_t *l_1953 = &g_1211;
                        int32_t *l_1954 = &g_1211;
                        int32_t *l_1955 = &g_70;
                        int i, j, k;
                        l_1956++;
                    }
                }
                for (g_921 = 14; (g_921 <= 39); g_921 = safe_add_func_uint8_t_u_u(g_921, 9))
                { /* block id: 878 */
                    int32_t l_1961[10][5][5] = {{{0x8B758AF7L,0x71BA1AB2L,0xCA7C44D3L,0x57DF3AEDL,0xCA7C44D3L},{(-8L),(-8L),1L,0x74B6025BL,0x8B758AF7L},{3L,0xDD77A232L,8L,0x6F0D931AL,0xF1CA0EB4L},{0x4FEC09E7L,0xCC3DA009L,0x5E5C252DL,7L,(-7L)},{0x57DF3AEDL,0xDD77A232L,0x54B73614L,1L,0x78913583L}},{{0x9F66F24DL,(-8L),3L,0xA4FEA429L,0xA6522C9CL},{(-10L),0x71BA1AB2L,0x6F0D931AL,0x4019A237L,0x110F6078L},{0x9A48DCB3L,(-1L),0xF622DE55L,(-2L),(-2L)},{0xCC3DA009L,(-10L),3L,0xCC3DA009L,1L},{5L,2L,0L,(-8L),0x6320849AL}},{{0xDD77A232L,0x4019A237L,0xCF837B3CL,(-3L),(-3L)},{0x6F0D931AL,(-4L),8L,1L,0L},{0x78913583L,5L,0x082E88E3L,0L,1L},{(-2L),0x5A78E38FL,0x22154AB5L,(-2L),0xAD8A3104L},{5L,0x78913583L,(-4L),0x558635B6L,0xAD8A3104L}},{{0x72394517L,1L,0xA4FEA429L,0x9A48DCB3L,1L},{0xF1CA0EB4L,0x9A48DCB3L,0x9851DF4FL,1L,0L},{0x558635B6L,0L,1L,3L,(-3L)},{0L,0xE4E0BA1DL,5L,0x434D4944L,0x6320849AL},{0x4019A237L,(-8L),0xF46C0F5AL,0x74B6025BL,1L}},{{0x27C1724BL,0x855A3108L,0xA35FED56L,1L,(-2L)},{0x72394517L,0xCA7C44D3L,0x6004AC53L,(-3L),0x110F6078L},{(-4L),(-4L),(-1L),0xA6522C9CL,0xA6522C9CL},{2L,0x855A3108L,2L,0xCD8B0407L,0x78913583L},{0x78913583L,1L,1L,(-10L),(-7L)}},{{0x434D4944L,0x74B6025BL,0xE753E65CL,(-2L),0xF1CA0EB4L},{8L,0L,1L,(-7L),0x8B758AF7L},{5L,0xA4FEA429L,2L,0x5A78E38FL,0xCA7C44D3L},{0L,(-3L),(-1L),1L,0xA4D8CA95L},{0xA4FEA429L,0x78913583L,0x6004AC53L,1L,0x4019A237L}},{{(-10L),4L,0xA35FED56L,0x5A78E38FL,5L},{1L,(-10L),0xF46C0F5AL,0x855A3108L,(-8L)},{1L,(-4L),5L,2L,0x74B6025BL},{0x4FEC09E7L,0x558635B6L,1L,(-10L),0x47766A8CL},{7L,(-2L),0x9851DF4FL,0xAABD2386L,(-10L)}},{{0x47766A8CL,(-10L),0xA4FEA429L,0L,0x4019A237L},{0x8B758AF7L,0x082E88E3L,(-4L),(-3L),0L},{0x8B758AF7L,0L,0x22154AB5L,0x57DF3AEDL,5L},{(-8L),0xA35FED56L,0xE753E65CL,0x7D770583L,3L},{1L,0x59711111L,0L,1L,0xF46C0F5AL}},{{(-7L),0x082E88E3L,0x4FEC09E7L,1L,0x71BA1AB2L},{0xD900E8A1L,0xCD8B0407L,0xA867C774L,0xDE0C5002L,4L},{0x6004AC53L,(-8L),(-1L),0xCC3DA009L,0xA867C774L},{0x6F0D931AL,3L,0x8B758AF7L,0xCA7C44D3L,0L},{0L,0x9F66F24DL,0x9851DF4FL,2L,0x27C1724BL}},{{0x082E88E3L,(-1L),(-1L),0x082E88E3L,(-5L)},{1L,0x27C1724BL,7L,0x110F6078L,(-7L)},{0x59711111L,0xCA7C44D3L,0x19C73ADAL,0x9FF63AEAL,1L},{1L,0x170B42EEL,0L,0x110F6078L,1L},{4L,0x6F0D931AL,1L,0x082E88E3L,0x558635B6L}}};
                    int32_t l_1962 = 0L;
                    int32_t *l_1963 = (void*)0;
                    int32_t *l_1964 = &g_70;
                    int32_t *l_1965 = &l_1962;
                    int32_t *l_1966 = &g_1211;
                    int32_t *l_1967 = &l_1835[0];
                    int32_t *l_1968 = &l_1962;
                    int32_t **l_1982 = &l_1965;
                    int16_t *l_1984 = &g_262;
                    int i, j, k;
                    l_1969[0][3]++;
                    (*l_1965) = p_50;
                    if ((l_1969[2][5] != ((*l_1984) &= (safe_lshift_func_int8_t_s_u(((*g_1826) != p_48), ((248UL == (safe_unary_minus_func_uint8_t_u((safe_unary_minus_func_uint64_t_u(p_50))))) , (safe_sub_func_uint32_t_u_u(0xED462589L, (l_1978 , (l_1983 = (l_1979 | (safe_mod_func_int64_t_s_s((((*l_1982) = (l_1581 = (*g_1852))) == (void*)0), 0x6397509D8359EABCLL)))))))))))))
                    { /* block id: 885 */
                        int16_t l_1992 = 0L;
                        (*g_76) ^= (safe_mul_func_int16_t_s_s((p_50 & (((((*l_1941) = (!(l_1978.f0 , p_50))) & (safe_sub_func_int8_t_s_s(0xF9L, (((safe_rshift_func_uint16_t_u_s((l_1979 && (((*g_784) == (void*)0) <= (g_342 != (0L || l_1979)))), 5)) & g_504) || l_1992)))) <= l_1979) , g_674.f0)), (-5L)));
                        return l_1969[1][0];
                    }
                    else
                    { /* block id: 889 */
                        (**g_1826) ^= (-4L);
                        if (p_50)
                            break;
                        if (p_50)
                            break;
                        return p_49;
                    }
                }
                for (g_92 = 23; (g_92 >= 14); --g_92)
                { /* block id: 898 */
                    uint32_t l_1997 = 9UL;
                    uint8_t *l_2013 = &l_1983;
                    int32_t *l_2024[4][9][6] = {{{&l_2023,&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023},{&l_2023,&l_1835[0],&l_1979,&g_1211,&g_1211,&l_1979},{&l_2023,&l_2023,&g_181.f2,&l_1979,&l_2023,&l_2023},{&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1835[0],&l_1835[0],&l_2023,&l_2023},{&l_2023,&l_1835[0],&g_181.f2,&g_1211,&l_2023,&l_1979},{&g_1211,&l_2023,&l_1979,&l_1979,&l_2023,&l_2023},{&g_1211,&l_1979,&l_1979,&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023}},{{&l_2023,&l_1835[0],&l_1979,&g_1211,&g_1211,&l_1979},{&l_2023,&l_2023,&g_181.f2,&l_1979,&l_2023,&l_2023},{&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1835[0],&l_1835[0],&l_2023,&l_2023},{&l_2023,&l_1835[0],&g_181.f2,&g_1211,&l_2023,&l_1979},{&g_1211,&l_2023,&l_1979,&l_1979,&l_2023,&l_2023},{&g_1211,&l_1979,&l_1979,&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023},{&l_2023,&l_1835[0],&l_1979,&g_1211,&g_1211,&l_1979}},{{&l_2023,&l_2023,&g_181.f2,&l_1979,&l_2023,&l_2023},{&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1835[0],&l_1835[0],&l_2023,&l_2023},{&l_2023,&l_1835[0],&g_181.f2,&g_1211,&l_2023,&l_1979},{&g_1211,&l_2023,&l_1979,&l_1979,&l_2023,&l_2023},{&g_1211,&l_1979,&l_1979,&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023},{&l_2023,&l_1835[0],&l_1979,&g_1211,&g_1211,&l_1979},{&l_2023,&l_2023,&g_181.f2,&l_1979,&l_2023,&l_2023}},{{&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1835[0],&l_1835[0],&l_2023,&l_2023},{&l_2023,&l_1835[0],&g_181.f2,&g_1211,&l_2023,&l_1979},{&g_1211,&l_2023,&l_1979,&l_1979,&l_2023,&l_2023},{&g_1211,&l_1979,&l_1979,&g_1211,&l_2023,&g_181.f2},{&l_2023,&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023},{&l_2023,&l_1835[0],&l_1979,&g_1211,&g_1211,&l_1979},{&l_2023,&l_2023,&g_181.f2,&l_1979,&l_2023,&l_2023},{&l_2023,&l_1979,&l_1835[0],&g_1211,&l_2023,&g_181.f2}}};
                    int i, j, k;
                    for (g_70 = 0; (g_70 < (-10)); g_70 = safe_sub_func_uint16_t_u_u(g_70, 4))
                    { /* block id: 901 */
                        int16_t *l_1998 = (void*)0;
                        int16_t *l_1999[2][8][1] = {{{&g_112[6]},{&g_388},{&g_388},{&g_112[6]},{&g_388},{&g_388},{&g_112[6]},{&g_388}},{{&g_388},{&g_112[6]},{&g_388},{&g_388},{&g_112[6]},{&g_388},{&g_388},{&g_112[6]}}};
                        int32_t l_2000 = (-8L);
                        int i, j, k;
                        l_2001[1] &= (4294967293UL <= ((g_587.f0 , ((*l_1941) = (-2L))) < (l_2000 = l_1997)));
                        return (*l_1581);
                    }
                    if ((((g_2002 = g_2002) == (g_2006 = g_2006)) != (((safe_div_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(g_340, ((*l_2013) = 0x2EL))), (safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((~((l_2023 = (safe_mul_func_uint16_t_u_u((1L <= (l_2021 , ((!g_42[1]) > (p_50 ^ ((p_50 >= l_1969[0][3]) , 0x67L))))), l_1997))) | l_1979)), 11)), 0x4800L)))) && 18446744073709551611UL) <= p_50)))
                    { /* block id: 911 */
                        (*l_1581) &= l_2023;
                        l_2024[2][0][3] = (*g_1852);
                    }
                    else
                    { /* block id: 914 */
                        float l_2026 = 0xC.C25429p+43;
                        (**g_1826) ^= (((void*)0 != l_2025) , p_50);
                        return l_2023;
                    }
                    --l_2033;
                }
            }
        }
        (*l_1941) = ((safe_sub_func_uint64_t_u_u((p_50 != (safe_sub_func_uint32_t_u_u((((safe_rshift_func_uint16_t_u_u(((safe_sub_func_float_f_f(((***l_2057) = (safe_sub_func_float_f_f((p_49 = (safe_add_func_float_f_f((((**g_784) >= ((-0xA.077DDAp+33) == (-0x8.5p-1))) >= (safe_div_func_float_f_f((safe_add_func_float_f_f(((((-((((((safe_sub_func_uint16_t_u_u((l_2056 != &g_783), (p_50 == (*l_1581)))) < (((p_49 , 0xE178L) , p_50) , g_1266[3])) & (*l_1581)) , 0x9.A73505p+59) <= 0x3.99A049p+19) <= l_2058)) == (-0x1.Cp+1)) < (*l_1581)) <= (*g_785)), (**g_784))), 0x0.Fp+1))), p_50))), (-0x8.2p-1)))), p_50)) , p_50), p_50)) & p_50) > (*l_1581)), (*l_1581)))), (*l_1581))) ^ (-2L));
    }
    return p_49;
}


/* ------------------------------------------ */
/* 
 * reads : g_469
 * writes: g_469 g_1211 g_76
 */
static uint32_t * func_52(int8_t  p_53)
{ /* block id: 685 */
    int32_t *l_1573 = &g_1211;
    int32_t *l_1574 = (void*)0;
    int32_t **l_1578 = &g_76;
lbl_1577:
    l_1574 = l_1573;
    for (g_469 = (-6); (g_469 >= (-7)); g_469 = safe_sub_func_int64_t_s_s(g_469, 2))
    { /* block id: 689 */
        (*l_1573) = 0x6BCAD3EFL;
        if (g_469)
            goto lbl_1577;
    }
    (*l_1578) = l_1574;
    return &g_162[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_76 g_70 g_206 g_91 g_484 g_485 g_278 g_109 g_118 g_262 g_181.f1 g_162 g_342 g_259 g_94 g_66 g_181.f2 g_91.f0 g_587 g_601 g_65 g_398 g_92 g_623 g_340 g_182 g_181.f0 g_674 g_208 g_469 g_102 g_707 g_674.f3 g_504 g_785 g_223 g_2136 g_1789 g_2126
 * writes: g_118 g_102 g_340 g_70 g_109 g_105 g_181.f1 g_92 g_84 g_75 g_162 g_342 g_66 g_262 g_469 g_601 g_112 g_223 g_708 g_504 g_2136
 */
static uint16_t  func_56(uint8_t  p_57, int32_t * p_58, uint64_t  p_59)
{ /* block id: 32 */
    int32_t ***l_142 = &g_84[3][1][0];
    int32_t *l_158 = (void*)0;
    int32_t *l_160[2][4][5] = {{{&g_70,(void*)0,(void*)0,&g_70,&g_70},{&g_70,(void*)0,(void*)0,&g_70,&g_70},{&g_70,&g_70,(void*)0,&g_70,&g_70},{&g_70,&g_70,(void*)0,&g_70,(void*)0}},{{(void*)0,&g_70,&g_70,&g_70,&g_70},{&g_70,&g_70,&g_70,&g_70,&g_70},{&g_70,&g_70,&g_70,&g_70,(void*)0},{&g_70,&g_70,&g_70,&g_70,&g_70}}};
    int32_t l_163 = (-1L);
    int32_t l_228 = 1L;
    uint64_t *l_276[5][7][1] = {{{(void*)0},{&g_109},{&g_109},{(void*)0},{&g_109},{&g_109},{&g_109}},{{&g_109},{&g_109},{&g_109},{&g_109},{(void*)0},{&g_109},{&g_109}},{{(void*)0},{&g_109},{&g_109},{&g_109},{&g_109},{&g_109},{&g_109}},{{&g_109},{(void*)0},{&g_109},{&g_109},{(void*)0},{&g_109},{&g_109}},{{&g_109},{&g_109},{&g_109},{&g_109},{&g_109},{(void*)0},{&g_109}}};
    const uint16_t l_290 = 0xA29AL;
    const int32_t *l_309[9];
    int32_t l_369 = 0x344D7334L;
    int32_t l_418 = (-1L);
    uint64_t * const *l_431 = &g_278;
    uint64_t * const **l_430 = &l_431;
    uint64_t **l_436[6];
    uint64_t ***l_435 = &l_436[1];
    float l_496 = (-0x6.4p-1);
    int32_t *l_507 = &l_163;
    union U0 l_517 = {0x0273ACFAL};
    int8_t l_655[5][9] = {{3L,0x05L,3L,0x05L,3L,0x05L,3L,0x05L,3L},{0x12L,0x12L,0xBBL,0xBBL,0x12L,0x12L,0xBBL,0xBBL,0x12L},{0L,0x05L,0L,0x05L,0L,0x05L,0L,0x05L,0L},{0x12L,0xBBL,0xBBL,0x12L,0x12L,0xBBL,0xBBL,0x12L,0x12L},{3L,0x05L,3L,0x05L,3L,0x05L,3L,0x05L,3L}};
    int32_t *l_733 = &g_70;
    float ***l_763 = (void*)0;
    const volatile float * volatile *l_769 = &g_770;
    const volatile float * volatile **l_768 = &l_769;
    const volatile float * volatile ** volatile * volatile l_767 = &l_768;/* VOLATILE GLOBAL l_767 */
    float * const ***l_810[9] = {&g_783,&g_783,&g_783,&g_783,&g_783,&g_783,&g_783,&g_783,&g_783};
    uint32_t l_892 = 4294967288UL;
    uint64_t l_993 = 0x7C834EF0A1CE8E80LL;
    uint8_t l_1020 = 0UL;
    int64_t l_1082 = (-1L);
    uint64_t l_1092 = 18446744073709551615UL;
    uint32_t l_1101 = 4294967295UL;
    union U1 *l_1105 = &g_674;
    int32_t l_1209[3][1];
    int8_t ***l_1298 = (void*)0;
    int8_t ***l_1303 = (void*)0;
    uint16_t *l_1315 = (void*)0;
    int32_t *l_1334[8] = {&l_163,&g_70,&l_163,&g_70,&l_163,&g_70,&l_163,&g_70};
    int32_t l_1362 = 0x9DBBC8EDL;
    int16_t l_1363 = 0x3FA9L;
    uint32_t *l_1433 = &g_162[6];
    const int32_t ***l_1494 = (void*)0;
    const int32_t ****l_1493 = &l_1494;
    const int32_t *****l_1492 = &l_1493;
    int32_t l_1511 = 3L;
    int16_t l_1570 = 0x1B49L;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_309[i] = &g_70;
    for (i = 0; i < 6; i++)
        l_436[i] = &l_276[1][0][0];
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_1209[i][j] = 9L;
    }
lbl_531:
    for (g_118 = 0; (g_118 != 10); g_118 = safe_add_func_int8_t_s_s(g_118, 1))
    { /* block id: 35 */
        int32_t ***l_140 = &g_75[3];
        int32_t *l_157 = &g_70;
        int32_t l_166 = 0xDAE6AD3CL;
        uint32_t l_210[6][5] = {{1UL,0x6C67E9A6L,0x02F6452DL,0x02F6452DL,0x6C67E9A6L},{1UL,18446744073709551608UL,0x02F6452DL,7UL,18446744073709551608UL},{1UL,0x6C67E9A6L,0xCDA5918FL,7UL,0x6C67E9A6L},{1UL,0x6C67E9A6L,0x02F6452DL,0x02F6452DL,0x6C67E9A6L},{1UL,18446744073709551608UL,0x02F6452DL,7UL,18446744073709551608UL},{1UL,0x6C67E9A6L,0xCDA5918FL,7UL,0x6C67E9A6L}};
        int32_t l_213 = 0x8B93ECFDL;
        int32_t l_215 = (-1L);
        int32_t l_216 = 1L;
        int16_t l_233 = 0L;
        int32_t l_235 = 1L;
        uint32_t *l_326 = &g_162[2];
        int64_t l_334 = 0x1546F4178D8C225BLL;
        float l_380[7] = {0xC.231B38p-44,0x1.0p+1,0xC.231B38p-44,0xC.231B38p-44,0x1.0p+1,0xC.231B38p-44,0xC.231B38p-44};
        int32_t l_384[6];
        uint32_t l_385[4] = {0UL,0UL,0UL,0UL};
        uint64_t **l_433[5];
        uint64_t ***l_432 = &l_433[3];
        union U0 l_452 = {0xF8E3D72FL};
        int i, j;
        for (i = 0; i < 6; i++)
            l_384[i] = 1L;
        for (i = 0; i < 5; i++)
            l_433[i] = (void*)0;
        for (g_102 = 2; (g_102 <= 9); g_102 += 1)
        { /* block id: 38 */
            int32_t ****l_141 = &l_140;
            int32_t **l_159[5][1] = {{&l_157},{&l_157},{&l_157},{&l_157},{&l_157}};
            uint32_t *l_161[9] = {(void*)0,&g_162[1],(void*)0,&g_162[1],(void*)0,&g_162[1],(void*)0,&g_162[1],(void*)0};
            uint32_t l_167 = 0x9698A885L;
            float *l_311[5][2][6] = {{{&g_223,&g_223,&g_223,&g_223,&g_223,&g_223},{&g_223,&g_223,(void*)0,&g_223,&g_223,&g_223}},{{&g_223,&g_223,&g_223,&g_223,(void*)0,(void*)0},{&g_223,&g_223,&g_223,(void*)0,&g_223,&g_223}},{{&g_223,(void*)0,(void*)0,&g_223,&g_223,&g_223},{&g_223,&g_223,&g_223,&g_223,&g_223,(void*)0}},{{&g_223,(void*)0,&g_223,&g_223,(void*)0,&g_223},{(void*)0,&g_223,(void*)0,(void*)0,&g_223,&g_223}},{{(void*)0,&g_223,&g_223,&g_223,&g_223,(void*)0},{&g_223,&g_223,&g_223,&g_223,&g_223,&g_223}}};
            float * const *l_310 = &l_311[2][1][3];
            int i, j, k;
        }
    }
    for (g_340 = 4; (g_340 > 32); g_340 = safe_add_func_uint64_t_u_u(g_340, 9))
    { /* block id: 201 */
        int32_t *l_518[4];
        uint64_t **l_519 = &l_276[1][0][0];
        union U0 *l_520[1];
        uint64_t l_539 = 0x2C3DC35C32B37EF4LL;
        uint8_t l_544[5][9][4] = {{{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL}},{{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL}},{{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL}},{{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL}},{{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL},{0x30L,0x30L,255UL,255UL}}};
        uint32_t l_696 = 0x1E622FB6L;
        int32_t l_723 = (-1L);
        union U1 *l_765 = &g_674;
        float * const **l_787 = &g_784;
        uint32_t l_789 = 2UL;
        int64_t *l_841 = &g_208;
        int8_t *l_865 = &g_601;
        uint16_t l_891 = 0UL;
        int64_t l_899 = (-6L);
        int16_t *l_977 = &g_388;
        int16_t **l_976 = &l_977;
        int32_t **** const l_1010 = (void*)0;
        uint16_t *l_1081 = &l_891;
        uint32_t l_1095 = 6UL;
        const int32_t l_1151 = 0x8F78E962L;
        int32_t l_1155[2];
        int32_t ****l_1170 = &l_142;
        int64_t l_1255 = 0x9D265B73D30C9C3BLL;
        int32_t l_1320 = 0x2EC39712L;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_518[i] = &g_181.f2;
        for (i = 0; i < 1; i++)
            l_520[i] = (void*)0;
        for (i = 0; i < 2; i++)
            l_1155[i] = 0xF32D5CF5L;
        (*g_76) ^= ((safe_add_func_int8_t_s_s((!((safe_lshift_func_int8_t_s_s(0x11L, 5)) | 0xEFFFL)), (*l_507))) < (((safe_lshift_func_int8_t_s_u(((l_518[2] = (l_517 , p_58)) == (void*)0), 3)) , l_519) == (*l_435)));
        if (((0x13L <= ((g_206[0][3] != ((void*)0 != l_520[0])) == (g_91[8] , 0x44FBL))) , (0x3994C2A45C54240CLL & (p_59 = (++(***g_484))))))
        { /* block id: 206 */
            float l_525 = 0x0.0p+1;
            int32_t l_527[8] = {0L,0L,0L,0L,0L,0L,0L,0L};
            int16_t l_535 = 0L;
            int i;
            for (g_105 = 0; (g_105 == 59); g_105 = safe_add_func_int64_t_s_s(g_105, 6))
            { /* block id: 209 */
                uint8_t l_528 = 9UL;
                int32_t l_532 = 0x2CB77A90L;
                int32_t l_537[1];
                int32_t l_551[5][3];
                int32_t l_555 = 7L;
                const int32_t **l_580 = &l_309[5];
                int i, j;
                for (i = 0; i < 1; i++)
                    l_537[i] = 0xA5E0AAD8L;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_551[i][j] = 0x35169122L;
                }
                for (g_181.f1 = 0; (g_181.f1 <= 1); g_181.f1 += 1)
                { /* block id: 212 */
                    for (g_92 = 1; (g_92 <= 6); g_92 += 1)
                    { /* block id: 215 */
                        int32_t l_526 = 0x338A6764L;
                        int i, j, k;
                        --l_528;
                    }
                    if (l_527[1])
                        continue;
                    if (g_118)
                        goto lbl_531;
                }
                if (l_528)
                { /* block id: 221 */
                    int8_t l_534 = 0xE9L;
                    int32_t l_536 = 0x75609334L;
                    int32_t l_538 = (-3L);
                    uint32_t *l_546 = &g_162[2];
                    uint64_t l_556 = 0xC50D621DFF8D507DLL;
                    for (g_102 = 0; (g_102 <= 2); g_102 += 1)
                    { /* block id: 224 */
                        int32_t l_533[10] = {0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L,0xD06116E7L};
                        int i, j, k;
                        --l_539;
                        if (p_59)
                            break;
                    }
                    for (g_181.f1 = 0; (g_181.f1 <= 2); g_181.f1 += 1)
                    { /* block id: 230 */
                        int32_t ***l_545 = &g_75[2];
                        int i, j, k;
                        l_538 ^= (safe_mul_func_uint8_t_u_u(p_59, g_262));
                        if (l_544[2][4][3])
                            break;
                        (*l_545) = (g_84[(g_181.f1 + 1)][g_181.f1][(g_181.f1 + 2)] = (void*)0);
                    }
                    if ((((*l_546) |= p_57) || (safe_div_func_int64_t_s_s((l_536 == ((g_342 |= ((**g_485) = (0xEEA56BB82871BE31LL && l_536))) == (safe_add_func_int16_t_s_s(l_551[1][0], ((safe_rshift_func_uint8_t_u_u(((((0x9AB1DACD6EA46786LL ^ l_535) ^ 4294967295UL) , g_91[2]) , p_59), 5)) >= g_259))))), 1L))))
                    { /* block id: 239 */
                        const int32_t **l_554 = &g_66;
                        int32_t l_577 = (-10L);
                        if (l_539)
                            goto lbl_531;
                        (*l_554) = (*g_94);
                        l_556 &= l_555;
                        l_577 &= ((safe_mul_func_int16_t_s_s(l_556, (18446744073709551609UL != p_57))) & (safe_sub_func_uint16_t_u_u(0x37AFL, (((4294967295UL ^ (safe_add_func_int32_t_s_s((l_555 = (l_527[2] >= (safe_mod_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(g_181.f2, (safe_sub_func_int64_t_s_s((((***l_430) ^= ((safe_lshift_func_int16_t_s_s((safe_mod_func_int32_t_s_s(0x271F2A85L, ((*l_546)--))), 10)) == l_527[1])) ^ 0xD2E8BBD93E7F5E44LL), l_535)))), 4)), g_91[8].f0)))), 0x8F0A214AL))) , 0x9EL) , (*l_507)))));
                    }
                    else
                    { /* block id: 247 */
                        uint32_t l_578 = 18446744073709551606UL;
                        (*l_507) ^= l_578;
                    }
                }
                else
                { /* block id: 250 */
                    if (l_555)
                        break;
                    if ((*l_507))
                        continue;
                }
                (*l_580) = (*g_94);
            }
            if (l_527[1])
                continue;
        }
        else
        { /* block id: 257 */
            uint16_t l_605 = 0x15D1L;
            int32_t l_610[9][3][6] = {{{(-10L),1L,0x43920F32L,3L,0xA161FCA8L,(-1L)},{(-2L),1L,(-10L),0xE84C1C06L,0xA161FCA8L,(-2L)},{(-1L),1L,0x26DC4606L,0L,(-9L),3L}},{{(-1L),(-1L),6L,8L,0x3C14DCBAL,0xF607375CL},{0L,(-7L),8L,0xE84C1C06L,(-2L),0xE84C1C06L},{0L,0x94629E9BL,0L,0x43920F32L,8L,0x26DC4606L}},{{(-1L),0xA161FCA8L,(-10L),0x5B9D7746L,(-7L),(-10L)},{(-1L),0xDCCA87AEL,(-9L),0x5B9D7746L,0xEE171BB7L,0x43920F32L},{(-1L),(-1L),6L,0x43920F32L,(-10L),(-1L)}},{{0L,0x40E7ADCAL,(-7L),0xE84C1C06L,0x94629E9BL,1L},{0L,1L,0x2E54A1E2L,8L,8L,0x2E54A1E2L},{2L,2L,8L,(-7L),0xAA69394AL,(-10L)}},{{(-10L),0x94629E9BL,0x63DBA0E6L,6L,(-1L),8L},{(-1L),(-10L),0x63DBA0E6L,0x40E7ADCAL,2L,(-10L)},{0xE84C1C06L,0x40E7ADCAL,8L,1L,0xEE171BB7L,0x2E54A1E2L}},{{1L,0xEE171BB7L,0x2E54A1E2L,0x43920F32L,(-1L),1L},{0xA161FCA8L,(-1L),(-7L),(-7L),(-7L),(-1L)},{(-7L),1L,6L,(-9L),(-1L),0x43920F32L}},{{2L,(-7L),(-9L),(-1L),3L,(-10L)},{0L,(-7L),(-10L),0x2E54A1E2L,(-1L),0x26DC4606L},{1L,1L,0L,0x40E7ADCAL,(-7L),0xE84C1C06L}},{{(-10L),(-1L),8L,0x5B9D7746L,(-1L),0xF607375CL},{0x40E7ADCAL,0xEE171BB7L,6L,6L,0xEE171BB7L,0x40E7ADCAL},{0xA161FCA8L,0x40E7ADCAL,0xA6DE7A63L,(-1L),2L,(-1L)}},{{0L,(-10L),0x43920F32L,0x26DC4606L,(-1L),0x2E54A1E2L},{0L,0x94629E9BL,0x26DC4606L,(-1L),0xAA69394AL,0xE84C1C06L},{0xA161FCA8L,2L,(-10L),6L,8L,0x3C14DCBAL}}};
            const union U0 *l_698 = &l_517;
            int16_t l_721 = 1L;
            uint32_t l_725 = 0x7F2CB4FCL;
            float *l_762[5];
            float **l_761 = &l_762[2];
            float *** const l_760 = &l_761;
            int16_t *l_814 = &g_112[6];
            int16_t **l_813[1];
            uint64_t *** const l_842 = (void*)0;
            const int32_t **l_856 = &l_309[5];
            const int32_t ***l_855 = &l_856;
            volatile int32_t *l_867 = &g_69;
            int32_t l_905 = 0xECC49313L;
            float l_940 = 0x1.72A454p-85;
            int32_t l_941 = 0L;
            int8_t l_996 = 0x77L;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_762[i] = (void*)0;
            for (i = 0; i < 1; i++)
                l_813[i] = &l_814;
            for (p_59 = 0; (p_59 <= 1); p_59 += 1)
            { /* block id: 260 */
                uint64_t l_602 = 0x2558843E94B08AE4LL;
                int32_t l_651 = 7L;
                int32_t l_654 = 0x819387C9L;
                int32_t l_656 = 7L;
                int32_t l_657 = 0x04153DAFL;
                int32_t l_658[4][5] = {{7L,(-6L),0xC9AF3229L,0xC9AF3229L,(-6L)},{0xF2079B2AL,(-2L),0xC587C38FL,0xC587C38FL,(-2L)},{7L,(-6L),0xC9AF3229L,0xC9AF3229L,(-6L)},{0xF2079B2AL,(-2L),0xC587C38FL,0xC587C38FL,(-2L)}};
                float l_663[7];
                const int16_t *l_692 = &g_388;
                uint32_t l_704 = 0UL;
                int32_t l_714 = (-1L);
                int64_t l_719 = 0xFB88EF06DE9681A0LL;
                int16_t l_724 = 0L;
                uint8_t *l_746 = (void*)0;
                uint8_t *l_747 = &l_544[2][4][3];
                uint16_t *l_748 = &l_605;
                uint16_t *l_749 = &g_750;
                int32_t *l_752 = &l_651;
                int16_t l_757 = 2L;
                float * const ***l_808 = &g_783;
                int i, j;
                for (i = 0; i < 7; i++)
                    l_663[i] = 0x0.Ep-1;
                for (g_262 = 0; (g_262 >= 0); g_262 -= 1)
                { /* block id: 263 */
                    uint32_t l_604 = 4294967295UL;
                    uint64_t ****l_649 = &l_435;
                    int32_t l_653[5][1];
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_653[i][j] = (-5L);
                    }
                    for (g_118 = 0; (g_118 <= 3); g_118 += 1)
                    { /* block id: 266 */
                        int8_t *l_598 = (void*)0;
                        int8_t *l_599 = &g_469;
                        int8_t *l_600 = &g_601;
                        uint64_t **l_603 = &g_278;
                        int16_t *l_606 = &g_112[3];
                        float *l_607 = &l_496;
                        int i, j;
                        (*l_607) = ((g_206[g_262][(g_118 + 1)] != (((((safe_mul_func_uint16_t_u_u(((4UL || ((*l_606) = ((safe_sub_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((((g_587 , &g_208) != &g_208) ^ (0UL | ((((safe_add_func_int8_t_s_s(((safe_lshift_func_int8_t_s_s((((safe_lshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((*l_599) = (safe_mul_func_int8_t_s_s(0x60L, g_206[g_262][(p_59 + 3)]))), ((*l_600) ^= (0x9DFEL | p_57)))), l_602)) , l_603) != (void*)0), p_59)) || g_601), l_604)) <= p_57) , l_605) == 1L))), g_162[1])), 0L)) | g_65))) == 0xB9L), 0x9526L)) , 0xD9L) >= g_206[0][3]) < g_162[2]) , l_602)) < p_59);
                    }
                    l_610[7][1][0] &= (safe_rshift_func_uint8_t_u_s(g_398, 7));
                    if ((g_92 | ((safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_rshift_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u((((*g_76) > (((safe_add_func_uint64_t_u_u(((void*)0 != g_623[2][2][0]), l_602)) || (-1L)) & ((safe_mul_func_int8_t_s_s(((((*l_507) ^= (safe_lshift_func_int8_t_s_u((+(--(**l_431))), p_57))) || (18446744073709551615UL == g_91[8].f0)) ^ 0xCDL), g_398)) , 0x9EL))) || p_59), (*g_76))), 11)), 0)) > g_259), g_340)), g_259)) >= 0x40L)))
                    { /* block id: 275 */
                        return g_182;
                    }
                    else
                    { /* block id: 277 */
                        int64_t *l_646 = (void*)0;
                        int16_t *l_647 = (void*)0;
                        int16_t *l_648 = &g_112[3];
                        uint16_t *l_650[4][5] = {{(void*)0,&g_340,(void*)0,(void*)0,&g_340},{&g_340,(void*)0,(void*)0,&g_340,(void*)0},{&l_605,&g_340,&l_605,&g_340,&g_340},{(void*)0,&g_340,(void*)0,(void*)0,&g_340}};
                        int32_t l_652[9] = {0x92CB646BL,0x92CB646BL,0x92CB646BL,0x92CB646BL,0x92CB646BL,0x92CB646BL,0x92CB646BL,0x92CB646BL,0x92CB646BL};
                        uint32_t l_659 = 0x49367B73L;
                        float *l_662 = &g_223;
                        int i, j;
                        (*l_507) &= (safe_div_func_uint64_t_u_u((--(*g_278)), (safe_mul_func_uint16_t_u_u(g_181.f0, (((l_651 = ((g_91[8] , (l_602 ^ ((((safe_lshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u(p_59, ((*l_648) = (g_92 < (safe_rshift_func_uint16_t_u_s((&g_208 == l_646), ((void*)0 == l_160[p_59][(p_59 + 2)][(p_59 + 2)]))))))) , p_57), g_70)) , &g_484) != l_649) == g_65))) > 0UL)) == 0x110DL) ^ 0x15E3BE40F4E41D07LL)))));
                        ++l_659;
                        (*l_662) = 0xE.D2326Fp-43;
                    }
                }
                if (l_654)
                    continue;
                if ((safe_mod_func_int16_t_s_s(3L, (*l_507))))
                { /* block id: 287 */
                    uint32_t l_694 = 0xB839464DL;
                    int32_t l_703[5] = {0xB55706FDL,0xB55706FDL,0xB55706FDL,0xB55706FDL,0xB55706FDL};
                    float l_709 = 0x0.Ep-1;
                    int i;
                    if (p_59)
                    { /* block id: 288 */
                        int16_t *l_691[4][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                        const int16_t **l_693 = &l_692;
                        int8_t *l_695 = &g_469;
                        int8_t *l_697 = &l_655[4][7];
                        float *l_699 = (void*)0;
                        int32_t l_700[6][10] = {{0x825E1C7DL,(-2L),0x10B40C33L,(-2L),0x825E1C7DL,1L,1L,0x825E1C7DL,(-2L),0x10B40C33L},{0xDD539EEBL,0xDD539EEBL,0x10B40C33L,0x825E1C7DL,(-5L),0x825E1C7DL,0x10B40C33L,0xDD539EEBL,0xDD539EEBL,0x10B40C33L},{(-2L),0x825E1C7DL,1L,1L,0x825E1C7DL,(-2L),0x10B40C33L,(-2L),0x825E1C7DL,1L},{0x66DBC040L,0xDD539EEBL,0x66DBC040L,1L,0x10B40C33L,0x10B40C33L,1L,0x66DBC040L,0xDD539EEBL,0x66DBC040L},{0x66DBC040L,(-2L),0xDD539EEBL,0x825E1C7DL,0xDD539EEBL,(-2L),0x66DBC040L,0x66DBC040L,(-2L),0xDD539EEBL},{(-2L),0x66DBC040L,0x66DBC040L,(-2L),0xDD539EEBL,0x825E1C7DL,0xDD539EEBL,(-2L),0x66DBC040L,0x66DBC040L}};
                        int32_t l_701[5][2];
                        int32_t l_702[10] = {1L,1L,1L,1L,1L,1L,1L,1L,1L,1L};
                        int i, j;
                        for (i = 0; i < 5; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_701[i][j] = 7L;
                        }
                        if (p_59)
                            break;
                        l_700[3][1] = ((l_657 , ((safe_add_func_int8_t_s_s(((*l_697) = (safe_lshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_s((safe_div_func_uint32_t_u_u((g_674 , (safe_mul_func_int16_t_s_s(((((safe_sub_func_int8_t_s_s(((*l_695) |= (safe_div_func_int64_t_s_s((((&l_290 != &g_340) >= (safe_mul_func_int16_t_s_s(p_59, (safe_sub_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u(((247UL == (safe_mul_func_int16_t_s_s((g_262 = (g_674 , 5L)), (((*l_693) = l_692) != (void*)0)))) <= g_208), g_162[6])) & p_59), 2)), 0x0C8CL))))) & p_57), l_694))), p_57)) == p_59) == p_59) < l_696), g_601))), g_109)), 10)), g_102))), g_91[8].f0)) , l_698)) != &g_91[2]);
                        --l_704;
                        return p_59;
                    }
                    else
                    { /* block id: 297 */
                        int32_t l_710[8][4][8] = {{{(-1L),0x76951195L,0xB78F4C53L,0x76951195L,(-1L),0L,0L,(-1L)},{0x76951195L,0L,0L,0x76951195L,0x0D909FE8L,(-1L),0x0D909FE8L,0x76951195L},{0L,0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L,0L},{0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L),(-1L),(-1L),0xB78F4C53L,0x0D909FE8L}},{{0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L,0L,0x0D909FE8L},{0L,0x76951195L,0x0D909FE8L,(-1L),0x0D909FE8L,0x76951195L,0L,0L},{0x76951195L,(-1L),0L,0L,(-1L),0x76951195L,0xB78F4C53L,0x76951195L},{(-1L),0x76951195L,0xB78F4C53L,0x76951195L,(-1L),0L,0L,(-1L)}},{{0x76951195L,0L,0L,0x76951195L,0x0D909FE8L,(-1L),0x0D909FE8L,0x76951195L},{0L,0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L,0L},{0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L),(-1L),(-1L),0xB78F4C53L,0x0D909FE8L},{0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L,0L,0x0D909FE8L}},{{0L,0x76951195L,0x0D909FE8L,(-1L),0x0D909FE8L,0x76951195L,0L,0L},{0x76951195L,(-1L),0L,0xB78F4C53L,0L,0L,0x0D909FE8L,0L},{0L,0L,0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L},{0L,(-1L),(-1L),0L,(-1L),0L,(-1L),0L}},{{(-1L),(-1L),(-1L),0xB78F4C53L,0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L)},{(-1L),(-1L),0x0D909FE8L,0L,0x76951195L,0L,0x0D909FE8L,(-1L)},{(-1L),(-1L),0xB78F4C53L,0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L),(-1L)},{(-1L),0L,(-1L),0L,(-1L),0L,(-1L),(-1L)}},{{0L,0L,0xB78F4C53L,0xB78F4C53L,0L,0L,0x0D909FE8L,0L},{0L,0L,0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L},{0L,(-1L),(-1L),0L,(-1L),0L,(-1L),0L},{(-1L),(-1L),(-1L),0xB78F4C53L,0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L)}},{{(-1L),(-1L),0x0D909FE8L,0L,0x76951195L,0L,0x0D909FE8L,(-1L)},{(-1L),(-1L),0xB78F4C53L,0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L),(-1L)},{(-1L),0L,(-1L),0L,(-1L),0L,(-1L),(-1L)},{0L,0L,0xB78F4C53L,0xB78F4C53L,0L,0L,0x0D909FE8L,0L}},{{0L,0L,0x0D909FE8L,0L,0L,0xB78F4C53L,0xB78F4C53L,0L},{0L,(-1L),(-1L),0L,(-1L),0L,(-1L),0L},{(-1L),(-1L),(-1L),0xB78F4C53L,0x0D909FE8L,0x0D909FE8L,0xB78F4C53L,(-1L)},{(-1L),(-1L),0x0D909FE8L,0L,0x76951195L,0L,0x0D909FE8L,(-1L)}}};
                        int32_t l_711 = (-10L);
                        int32_t l_712 = 5L;
                        int32_t l_713[6][1] = {{1L},{0x8FEC25F0L},{1L},{0x8FEC25F0L},{1L},{0x8FEC25F0L}};
                        uint32_t l_715 = 1UL;
                        int i, j, k;
                        (*g_707) = &g_674;
                        l_715++;
                    }
                }
                else
                { /* block id: 301 */
                    float *l_718[7] = {&l_663[4],&l_663[4],&l_663[4],&l_663[4],&l_663[4],&l_663[4],&l_663[4]};
                    int32_t l_720[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
                    int i;
                    l_658[3][0] = l_610[7][1][0];
                    ++l_725;
                    for (l_656 = 1; (l_656 >= 0); l_656 -= 1)
                    { /* block id: 306 */
                        uint64_t l_730 = 4UL;
                        int32_t l_731 = 1L;
                        int32_t l_732[3][6][8] = {{{5L,0L,0xD4F7EFB6L,(-1L),0L,0x0F495C8EL,1L,(-1L)},{(-1L),0xEB73EAA8L,(-1L),0x2996946CL,(-1L),0xEB73EAA8L,(-1L),0x0F495C8EL},{0L,0x02B55C84L,0xAC2F0D67L,(-1L),5L,9L,0xEB73EAA8L,(-7L)},{0x0F9DD16EL,0x0F495C8EL,0xAC2F0D67L,9L,5L,0L,0xCF06B7D4L,0x0F495C8EL},{0xD4F7EFB6L,0xAC2F0D67L,0x0BDD852CL,(-7L),9L,9L,(-7L),0x0BDD852CL},{0x85227BECL,0x85227BECL,0x2996946CL,0xEB73EAA8L,0xD4F7EFB6L,9L,(-1L),1L}},{{0x0BDD852CL,0xEB73EAA8L,(-1L),0L,1L,0xAC2F0D67L,0xCF06B7D4L,1L},{0xEB73EAA8L,0xCF06B7D4L,1L,0xEB73EAA8L,0L,0x0BDD852CL,0xC18C195CL,0x0BDD852CL},{0xC18C195CL,(-7L),9L,(-7L),0xC18C195CL,0x2996946CL,5L,0x0F495C8EL},{0x0BDD852CL,(-1L),0x734FF03FL,9L,(-1L),(-1L),0xAC2F0D67L,(-7L)},{5L,0xCF06B7D4L,0x734FF03FL,0x85227BECL,9L,1L,5L,0x0F9DD16EL},{(-1L),0xC18C195CL,9L,0x0F495C8EL,0x0F495C8EL,9L,0xC18C195CL,(-1L)}},{{0x0F9DD16EL,5L,1L,9L,0x85227BECL,0x734FF03FL,0xCF06B7D4L,5L},{(-7L),0xAC2F0D67L,(-1L),(-1L),9L,0x734FF03FL,(-1L),0x0BDD852CL},{0x0F495C8EL,5L,0x2996946CL,0xC18C195CL,(-7L),9L,(-7L),0xC18C195CL},{0x0BDD852CL,0xC18C195CL,0x0BDD852CL,0L,0xEB73EAA8L,1L,0xCF06B7D4L,0xEB73EAA8L},{1L,0xCF06B7D4L,0xAC2F0D67L,1L,0L,(-1L),0xEB73EAA8L,0x0BDD852CL},{1L,(-1L),9L,0xD4F7EFB6L,0xEB73EAA8L,0x2996946CL,0x85227BECL,0x85227BECL}}};
                        int i, j, k;
                        l_732[1][0][2] = (g_674.f3 , (l_731 = (safe_mul_func_float_f_f(l_730, 0x0.Dp+1))));
                        return g_65;
                    }
                    l_733 = &l_610[5][0][5];
                }
            }
            for (l_725 = (-28); (l_725 < 14); l_725++)
            { /* block id: 363 */
                int8_t l_843 = 0x21L;
                int32_t ***l_853 = &g_75[4];
                union U0 *l_868 = &g_91[8];
                int32_t l_898 = 8L;
                uint16_t l_902 = 1UL;
                int32_t l_907[3][7] = {{0xA14460E3L,0xA14460E3L,1L,5L,1L,0xA14460E3L,0xA14460E3L},{0xA14460E3L,1L,5L,1L,0xA14460E3L,0xA14460E3L,1L},{(-1L),0x573A17C9L,(-1L),1L,1L,(-1L),0x573A17C9L}};
                uint32_t l_909 = 0x18F32AB2L;
                float * const ***l_927 = &l_787;
                uint16_t l_939 = 0x738BL;
                uint64_t l_964[5];
                float l_994[1][5][4] = {{{0x0.C780C9p-91,0xB.4ECAD1p+94,0x1.6p+1,0x1.6p+1},{(-0x5.Bp+1),(-0x5.Bp+1),0x0.C780C9p-91,0x1.6p+1},{0x1.3BEC0Bp+78,0xB.4ECAD1p+94,0x1.3BEC0Bp+78,0x0.C780C9p-91},{0x1.3BEC0Bp+78,0x0.C780C9p-91,0x0.C780C9p-91,0x1.3BEC0Bp+78},{(-0x5.Bp+1),0x0.C780C9p-91,0x1.6p+1,0x0.C780C9p-91}}};
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_964[i] = 0x5F91385A0A7A67CELL;
            }
            if (l_996)
                break;
        }
        for (g_181.f1 = 0; (g_181.f1 <= 60); g_181.f1++)
        { /* block id: 440 */
            float *l_1009[3];
            int32_t l_1014 = (-1L);
            int32_t l_1017 = 0xB2F63835L;
            int32_t l_1018[3][4][5] = {{{(-1L),0x75250359L,0x47DA6241L,0L,(-3L)},{0x182EEC12L,0xD4327D46L,0xDD861589L,0xDD861589L,0xD4327D46L},{(-1L),0x75250359L,0x47DA6241L,0L,(-3L)},{0x182EEC12L,0xD4327D46L,0xDD861589L,0xDD861589L,0xD4327D46L}},{{(-1L),0x75250359L,0x47DA6241L,0L,(-3L)},{0x182EEC12L,(-8L),0xD4327D46L,0xD4327D46L,(-8L)},{3L,0x3CE52ECDL,(-3L),0x75250359L,2L},{(-1L),(-8L),0xD4327D46L,0xD4327D46L,(-8L)}},{{3L,0x3CE52ECDL,(-3L),0x75250359L,2L},{(-1L),(-8L),0xD4327D46L,0xD4327D46L,(-8L)},{3L,0x3CE52ECDL,(-3L),0x75250359L,2L},{(-1L),(-8L),0xD4327D46L,0xD4327D46L,(-8L)}}};
            int16_t **l_1099 = &l_977;
            int16_t l_1112[9][5] = {{0xDC03L,0xE3EBL,0xAD68L,0x4851L,0xE3EBL},{0xDC03L,0xA200L,3L,0x4851L,0xA200L},{0x39DBL,0xA200L,0xAD68L,0xAD68L,0xA200L},{0xDC03L,0xE3EBL,0xAD68L,0x4851L,0xE3EBL},{0xDC03L,0xA200L,3L,0x4851L,0xA200L},{0x39DBL,0xA200L,0xAD68L,0xAD68L,0xA200L},{0xDC03L,0xE3EBL,0xAD68L,0x4851L,0xE3EBL},{0xDC03L,0xA200L,3L,0x4851L,0xA200L},{0x39DBL,0xA200L,0xAD68L,0xAD68L,0xA200L}};
            int64_t l_1114 = 0xA340046AB3231F9BLL;
            union U0 **l_1118 = (void*)0;
            int32_t ***l_1207 = &g_75[3];
            int8_t * const **l_1216 = (void*)0;
            uint32_t l_1258[5][4] = {{0x2DC769F4L,0x2DC769F4L,0x2DC769F4L,0x2DC769F4L},{0x2DC769F4L,0x2DC769F4L,0x2DC769F4L,0x2DC769F4L},{0x2DC769F4L,0x2DC769F4L,0x2DC769F4L,0x2DC769F4L},{0x2DC769F4L,0x2DC769F4L,0x2DC769F4L,0x2DC769F4L},{0x2DC769F4L,0x2DC769F4L,0x2DC769F4L,0x2DC769F4L}};
            uint32_t l_1267 = 0xE56420F2L;
            uint16_t *l_1313 = &g_750;
            uint16_t **l_1314[8][4][8] = {{{&l_1313,(void*)0,(void*)0,&g_1312[0],(void*)0,&g_1312[0],&g_1312[0],&g_1312[1]},{(void*)0,&g_1312[0],(void*)0,(void*)0,&l_1313,&g_1312[0],&g_1312[0],&g_1312[1]},{(void*)0,&g_1312[1],&l_1081,(void*)0,&g_1312[0],&g_1312[0],(void*)0,(void*)0},{&g_1312[0],&l_1313,&l_1081,&g_1312[0],(void*)0,(void*)0,&l_1313,&l_1313}},{{&g_1312[0],&l_1313,(void*)0,&g_1312[0],&l_1081,(void*)0,&g_1312[0],&l_1081},{&g_1312[1],&l_1081,&g_1312[0],&g_1312[1],&l_1081,&g_1312[2],(void*)0,&l_1081},{&g_1312[0],(void*)0,&l_1313,&g_1312[2],&g_1312[1],&g_1312[0],&l_1313,(void*)0},{&g_1312[0],(void*)0,(void*)0,&l_1081,&l_1313,&l_1081,&l_1313,(void*)0}},{{&l_1081,&l_1313,&l_1081,&g_1312[0],(void*)0,&g_1312[1],&g_1312[1],&g_1312[0]},{(void*)0,&l_1081,&g_1312[0],&l_1313,(void*)0,&l_1313,&l_1313,&g_1312[1]},{&l_1081,(void*)0,&l_1081,(void*)0,&g_1312[0],&g_1312[0],(void*)0,&l_1081},{&l_1313,&l_1313,&g_1312[1],&l_1081,&g_1312[0],&l_1313,(void*)0,&g_1312[0]}},{{&l_1081,&l_1081,(void*)0,&g_1312[0],(void*)0,(void*)0,(void*)0,&g_1312[0]},{&l_1081,&l_1313,(void*)0,&l_1081,&g_1312[1],(void*)0,(void*)0,&l_1313},{&g_1312[0],(void*)0,&g_1312[0],&l_1081,(void*)0,&l_1313,&l_1081,&g_1312[0]},{&g_1312[2],&g_1312[0],&g_1312[0],&l_1313,&g_1312[0],&g_1312[0],&l_1313,&l_1081}},{{&g_1312[0],&l_1081,&l_1081,&g_1312[0],&g_1312[0],&g_1312[0],(void*)0,&g_1312[1]},{(void*)0,&g_1312[0],&l_1313,(void*)0,&l_1313,&g_1312[0],&l_1081,&l_1313},{&g_1312[0],&l_1313,(void*)0,&g_1312[0],(void*)0,&l_1081,&g_1312[0],(void*)0},{&g_1312[0],&l_1081,&g_1312[1],&g_1312[0],&g_1312[0],&l_1313,(void*)0,(void*)0}},{{&l_1081,&g_1312[2],(void*)0,&l_1313,&g_1312[1],&l_1081,&g_1312[0],&g_1312[1]},{&l_1313,&l_1313,&g_1312[0],&g_1312[0],&g_1312[0],(void*)0,(void*)0,&l_1313},{&l_1081,&l_1313,(void*)0,&g_1312[0],&g_1312[1],&g_1312[1],&g_1312[0],&g_1312[0]},{&l_1081,&g_1312[2],&l_1313,&l_1313,&g_1312[0],&g_1312[2],&g_1312[0],(void*)0}},{{&g_1312[0],&g_1312[0],&g_1312[0],(void*)0,&g_1312[0],&g_1312[0],&g_1312[0],&l_1313},{&g_1312[2],&g_1312[1],&g_1312[0],&l_1313,&g_1312[0],&l_1081,&g_1312[0],&l_1081},{&l_1081,&g_1312[0],&g_1312[0],&g_1312[0],&g_1312[0],(void*)0,(void*)0,(void*)0},{&g_1312[2],(void*)0,&l_1081,&l_1081,&g_1312[0],&l_1313,&l_1081,&l_1313}},{{&g_1312[0],&l_1313,(void*)0,&l_1081,&g_1312[0],&g_1312[0],&g_1312[0],&g_1312[2]},{&l_1081,&l_1081,&l_1313,&l_1081,&g_1312[1],&l_1313,&g_1312[0],&g_1312[0]},{&l_1081,(void*)0,&l_1313,&g_1312[0],&g_1312[0],&l_1081,&l_1313,&g_1312[0]},{&l_1313,&g_1312[0],&g_1312[2],&l_1081,&g_1312[1],&l_1313,&l_1313,&l_1081}}};
            uint32_t *l_1316[4][4][9] = {{{&l_1267,&g_162[4],&l_1095,&g_162[4],&l_1267,&l_1267,&g_162[4],&l_1095,&g_162[4]},{&g_162[4],&g_162[1],&l_1095,&l_1095,&g_162[1],&g_162[4],&g_162[1],&l_1095,&l_1095},{&l_1267,&l_1267,&g_162[4],&l_1095,&g_162[4],&l_1267,&l_1267,&g_162[4],&g_162[1]},{&l_1095,&l_1267,&l_1095,&l_1267,&l_1267,&l_1095,&l_1267,&l_1095,&l_1267}},{{&l_1095,&l_1267,&l_1267,&l_1095,&l_1267,&l_1095,&l_1267,&l_1267,&l_1095},{&g_162[4],&l_1267,&g_162[1],&l_1267,&g_162[4],&g_162[4],&l_1267,&g_162[1],&l_1267},{&l_1267,&l_1267,&g_162[1],&g_162[1],&l_1267,&l_1267,&l_1267,&g_162[1],&g_162[1]},{&g_162[4],&g_162[4],&l_1267,&g_162[1],&l_1267,&g_162[4],&g_162[4],&l_1267,&g_162[1]}},{{&l_1095,&l_1267,&l_1095,&l_1267,&l_1267,&l_1095,&l_1267,&l_1095,&l_1267},{&l_1095,&l_1267,&l_1267,&l_1095,&l_1267,&l_1095,&l_1267,&l_1267,&l_1095},{&g_162[4],&l_1267,&g_162[1],&l_1267,&g_162[4],&g_162[4],&l_1267,&g_162[1],&l_1267},{&l_1267,&l_1267,&g_162[1],&g_162[1],&l_1267,&l_1267,&l_1267,&g_162[1],&g_162[1]}},{{&g_162[4],&g_162[4],&l_1267,&g_162[1],&l_1267,&g_162[4],&g_162[4],&l_1267,&g_162[1]},{&l_1095,&l_1267,&l_1095,&l_1267,&l_1267,&l_1095,&l_1267,&l_1095,&l_1267},{&l_1095,&l_1267,&l_1267,&l_1095,&l_1267,&l_1095,&l_1267,&l_1267,&l_1095},{&g_162[4],&l_1267,&g_162[1],&l_1267,&g_162[4],&g_162[4],&l_1267,&g_162[1],&l_1267}}};
            int64_t *l_1319[5][2] = {{&l_899,&l_899},{&l_899,&l_899},{&l_899,&l_899},{&l_899,&l_899},{&l_899,&l_899}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1009[i] = &g_223;
        }
    }
lbl_1569:
    for (g_504 = 0; (g_504 == 51); g_504 = safe_add_func_uint64_t_u_u(g_504, 9))
    { /* block id: 583 */
        uint16_t l_1324 = 65535UL;
        int32_t l_1342 = 0xF75856DEL;
        union U1 *l_1347 = (void*)0;
        int32_t l_1352 = 0x764CDFABL;
        int32_t l_1355 = 0L;
        int32_t l_1357 = 0L;
        int32_t l_1359[7] = {0xA8EE4628L,0xA8EE4628L,0xA8EE4628L,0xA8EE4628L,0xA8EE4628L,0xA8EE4628L,0xA8EE4628L};
        uint16_t l_1364 = 0xC844L;
        int8_t l_1389 = 0x2AL;
        int8_t l_1430[1][10];
        const int16_t *l_1443 = &g_112[0];
        const int16_t **l_1442[5][1][1];
        const int16_t ***l_1441 = &l_1442[3][0][0];
        const int16_t **** const l_1440 = &l_1441;
        int16_t *l_1506 = &g_388;
        uint16_t *l_1524[3][4] = {{&g_750,&g_750,&g_750,&g_750},{&g_750,&g_750,&g_750,&g_750},{&g_750,&g_750,&g_750,&g_750}};
        float **l_1527 = &g_785;
        float *l_1529 = (void*)0;
        float **l_1528 = &l_1529;
        uint8_t *l_1530[3][2][1] = {{{&g_1321[3]},{&g_1321[3]}},{{&g_102},{&g_1321[3]}},{{&g_1321[3]},{&g_102}}};
        int16_t **l_1531[2];
        int8_t *l_1532 = &l_655[4][7];
        uint64_t l_1559 = 0UL;
        float l_1562 = 0xA.D1A92Ap-81;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 10; j++)
                l_1430[i][j] = 0xA1L;
        }
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 1; k++)
                    l_1442[i][j][k] = &l_1443;
            }
        }
        for (i = 0; i < 2; i++)
            l_1531[i] = &l_1506;
        ++l_1324;
    }
    for (p_59 = 8; (p_59 < 50); ++p_59)
    { /* block id: 677 */
        int64_t l_1568[10][6][4] = {{{4L,0xF89B9154D2C8A8D3LL,0L,0x79384A758EB73400LL},{0xC5292420231CF45CLL,(-1L),4L,0x749803A2296E65CBLL},{1L,0x136646D589D01F9ELL,0x950E2B0B7B81733FLL,0xF15EB20B4C723CCCLL},{5L,(-1L),0xD709398BB95700C7LL,0x9FB40EE34AF4CEE3LL},{0x9CA2541433682A79LL,(-1L),(-1L),0xF15EB20B4C723CCCLL},{4L,0x136646D589D01F9ELL,0x266EE6F5DA1EF8ABLL,0x749803A2296E65CBLL}},{{(-3L),(-1L),2L,0x79384A758EB73400LL},{0x950E2B0B7B81733FLL,0xF89B9154D2C8A8D3LL,0x950E2B0B7B81733FLL,(-1L)},{0L,0x402E4F7D1982E362LL,0x5B48EFB5E674DF8ELL,0x9FB40EE34AF4CEE3LL},{4L,(-1L),(-2L),0L},{9L,0xFD133D716D2EBA76LL,(-2L),0x136646D589D01F9ELL},{0xD709398BB95700C7LL,(-1L),5L,(-10L)}},{{0x266EE6F5DA1EF8ABLL,1L,0xBDB6D96E5E6A53F8LL,0x9FB40EE34AF4CEE3LL},{0xBDB6D96E5E6A53F8LL,0x9FB40EE34AF4CEE3LL,0x950E2B0B7B81733FLL,3L},{(-1L),0xE3E5E1F918869EEALL,0L,(-5L)},{9L,(-3L),(-3L),0x514A1C518B3971CBLL},{0x5B48EFB5E674DF8ELL,(-1L),1L,0x514A1C518B3971CBLL},{0L,(-3L),0xBDB6D96E5E6A53F8LL,(-5L)}},{{(-1L),0xE3E5E1F918869EEALL,9L,3L},{4L,0x9FB40EE34AF4CEE3LL,(-4L),0x9FB40EE34AF4CEE3LL},{9L,1L,0xDCB283244D78FEF2LL,(-10L)},{2L,(-1L),0L,0x136646D589D01F9ELL},{9L,0xFD133D716D2EBA76LL,0xBDB6D96E5E6A53F8LL,0L},{9L,(-5L),0L,3L}},{{2L,0L,0xDCB283244D78FEF2LL,0x93477057599C696ELL},{9L,(-1L),(-4L),0xF89B9154D2C8A8D3LL},{4L,(-1L),9L,0x1F227A34794BAF2CLL},{(-1L),0xC908D0F405E1FFF8LL,0xBDB6D96E5E6A53F8LL,0xE3E5E1F918869EEALL},{0L,0x93477057599C696ELL,1L,3L},{0x5B48EFB5E674DF8ELL,0x93477057599C696ELL,(-3L),0xE3E5E1F918869EEALL}},{{9L,0xC908D0F405E1FFF8LL,0L,0x1F227A34794BAF2CLL},{(-1L),(-1L),0x950E2B0B7B81733FLL,0xF89B9154D2C8A8D3LL},{0xBDB6D96E5E6A53F8LL,(-1L),0xBDB6D96E5E6A53F8LL,0x93477057599C696ELL},{0x266EE6F5DA1EF8ABLL,0L,5L,3L},{0xD709398BB95700C7LL,(-5L),(-2L),0L},{9L,0xFD133D716D2EBA76LL,(-2L),0x136646D589D01F9ELL}},{{0xD709398BB95700C7LL,(-1L),5L,(-10L)},{0x266EE6F5DA1EF8ABLL,1L,0xBDB6D96E5E6A53F8LL,0x9FB40EE34AF4CEE3LL},{0xBDB6D96E5E6A53F8LL,0x9FB40EE34AF4CEE3LL,0x950E2B0B7B81733FLL,3L},{(-1L),0xE3E5E1F918869EEALL,0L,(-5L)},{9L,(-3L),(-3L),0x514A1C518B3971CBLL},{0x5B48EFB5E674DF8ELL,(-1L),1L,0x514A1C518B3971CBLL}},{{0L,(-3L),0xBDB6D96E5E6A53F8LL,(-5L)},{(-1L),0xE3E5E1F918869EEALL,9L,3L},{4L,0x9FB40EE34AF4CEE3LL,(-4L),0x9FB40EE34AF4CEE3LL},{9L,1L,0xDCB283244D78FEF2LL,(-10L)},{2L,(-1L),0L,0x136646D589D01F9ELL},{9L,0xFD133D716D2EBA76LL,0xBDB6D96E5E6A53F8LL,0L}},{{9L,(-5L),0L,3L},{2L,0L,0xDCB283244D78FEF2LL,0x93477057599C696ELL},{9L,(-1L),(-4L),0xF89B9154D2C8A8D3LL},{4L,(-1L),9L,0x1F227A34794BAF2CLL},{(-1L),0xC908D0F405E1FFF8LL,0xBDB6D96E5E6A53F8LL,0xE3E5E1F918869EEALL},{0L,0x93477057599C696ELL,1L,3L}},{{0x5B48EFB5E674DF8ELL,0x93477057599C696ELL,(-3L),0xE3E5E1F918869EEALL},{9L,0xC908D0F405E1FFF8LL,0L,0x1F227A34794BAF2CLL},{(-1L),(-1L),0x950E2B0B7B81733FLL,0xF89B9154D2C8A8D3LL},{(-2L),(-1L),(-2L),0x749803A2296E65CBLL},{0L,(-1L),0L,0x514A1C518B3971CBLL},{1L,0x79384A758EB73400LL,(-3L),(-1L)}}};
        int i, j, k;
        (*g_76) ^= p_57;
        l_1568[2][1][0] = (p_57 >= ((*g_785) >= p_59));
        (*g_76) = p_57;
        if (g_340)
            goto lbl_1569;
    }
    return l_1570;
}


/* ------------------------------------------ */
/* 
 * reads : g_70 g_75 g_67 g_91 g_69 g_91.f0 g_76 g_94 g_92 g_102 g_105 g_109 g_65 g_2 g_1580 g_2136 g_1211
 * writes: g_70 g_84 g_92 g_66 g_102 g_105 g_109 g_112 g_118 g_1580 g_2136 g_1211
 */
static int32_t * func_60(const int32_t * p_61, int32_t * p_62)
{ /* block id: 7 */
    uint32_t l_68[3];
    int32_t l_93 = (-8L);
    int32_t l_95 = (-1L);
    uint32_t l_96 = 1UL;
    uint64_t *l_116 = &g_109;
    int8_t *l_117 = &g_118;
    int16_t l_132[4][8] = {{0x1FA8L,0xFD8CL,0xF264L,0xF264L,0xFD8CL,0x1FA8L,0xFD8CL,0xF264L},{(-3L),0xFD8CL,(-3L),0x1FA8L,0x1FA8L,(-3L),0xFD8CL,(-3L)},{1L,0x1FA8L,0xF264L,0x1FA8L,1L,1L,0x1FA8L,0xF264L},{1L,1L,0x1FA8L,0xF264L,0x1FA8L,1L,1L,0x1FA8L}};
    uint8_t *l_133 = &g_105;
    int i, j;
    for (i = 0; i < 3; i++)
        l_68[i] = 0x887B3417L;
    for (g_70 = 2; (g_70 >= 0); g_70 -= 1)
    { /* block id: 10 */
        int32_t **l_77 = &g_76;
        int32_t * const *l_80[4][8][2] = {{{&g_76,&g_76},{(void*)0,(void*)0},{(void*)0,&g_76},{&g_76,&g_76},{&g_76,&g_76},{&g_76,&g_76},{(void*)0,(void*)0},{(void*)0,&g_76}},{{&g_76,&g_76},{&g_76,&g_76},{&g_76,&g_76},{(void*)0,(void*)0},{(void*)0,&g_76},{&g_76,&g_76},{&g_76,&g_76},{&g_76,&g_76}},{{(void*)0,(void*)0},{(void*)0,&g_76},{&g_76,&g_76},{&g_76,&g_76},{&g_76,&g_76},{(void*)0,(void*)0},{(void*)0,&g_76},{&g_76,&g_76}},{{&g_76,&g_76},{&g_76,&g_76},{(void*)0,(void*)0},{(void*)0,&g_76},{&g_76,&g_76},{&g_76,&g_76},{&g_76,&g_76},{(void*)0,(void*)0}}};
        int32_t * const **l_81 = &l_80[1][5][1];
        int32_t **l_82 = &g_76;
        int32_t ***l_83[6] = {&g_75[0],&g_75[0],&g_75[6],&g_75[0],&g_75[0],&g_75[6]};
        uint8_t *l_99 = (void*)0;
        uint8_t *l_100 = &g_92;
        uint8_t *l_101 = &g_102;
        uint8_t *l_103 = (void*)0;
        uint8_t *l_104 = &g_105;
        uint64_t *l_108 = &g_109;
        int i, j, k;
        l_93 = (((g_92 = ((safe_rshift_func_int8_t_s_s(l_68[g_70], ((&g_66 != (l_77 = g_75[0])) <= ((((safe_div_func_int16_t_s_s((((*l_81) = l_80[1][5][1]) == (g_84[3][1][0] = l_82)), g_67)) , (safe_mul_func_int16_t_s_s(l_68[1], l_68[0]))) ^ (safe_lshift_func_int16_t_s_u((safe_mod_func_uint64_t_u_u(((g_91[8] , 0x8EDA297315BB970ELL) >= 0x54C1C085588CB35BLL), g_69)), 1))) != g_91[8].f0)))) == (**l_82))) == 0xAA88D18043A44ADELL) , l_68[0]);
        (*g_94) = p_61;
        l_96--;
        l_93 ^= (((((*l_101) ^= ((*l_100) |= ((**l_82) ^ l_96))) & (--(*l_104))) >= ((*l_108)--)) != (g_112[6] = 0x60535321785A3FFCLL));
    }
    (*g_76) = (safe_unary_minus_func_uint16_t_u((1L >= (safe_mul_func_uint8_t_u_u((((g_65 & ((l_116 != (void*)0) != (l_95 &= ((*l_117) = (-1L))))) , (((*l_116) ^= (safe_mul_func_uint8_t_u_u(((*l_133) = ((safe_add_func_float_f_f((g_69 < ((safe_mul_func_float_f_f((safe_div_func_float_f_f(((l_95 = ((safe_sub_func_float_f_f(((+g_65) < 0x5.AB6CACp-11), (safe_add_func_float_f_f(l_96, g_2)))) != l_95)) == l_96), g_65)), 0x2.26F512p+10)) < l_132[3][4])), (-0x3.5p+1))) , 3UL)), 0x6DL))) >= l_68[1])) || l_132[1][2]), 0UL)))));
    return p_62;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_37, "g_37", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_42[i], "g_42[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_65, "g_65", print_hash_value);
    transparent_crc(g_67, "g_67", print_hash_value);
    transparent_crc(g_69, "g_69", print_hash_value);
    transparent_crc(g_70, "g_70", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_91[i].f0, "g_91[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc(g_102, "g_102", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    transparent_crc(g_109, "g_109", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_112[i], "g_112[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_118, "g_118", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_162[i], "g_162[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_181.f0, "g_181.f0", print_hash_value);
    transparent_crc(g_181.f3, "g_181.f3", print_hash_value);
    transparent_crc(g_182, "g_182", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_206[i][j], "g_206[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_208, "g_208", print_hash_value);
    transparent_crc_bytes (&g_223, sizeof(g_223), "g_223", print_hash_value);
    transparent_crc(g_259, "g_259", print_hash_value);
    transparent_crc(g_262, "g_262", print_hash_value);
    transparent_crc(g_340, "g_340", print_hash_value);
    transparent_crc(g_342, "g_342", print_hash_value);
    transparent_crc(g_388, "g_388", print_hash_value);
    transparent_crc(g_398, "g_398", print_hash_value);
    transparent_crc(g_469, "g_469", print_hash_value);
    transparent_crc(g_504, "g_504", print_hash_value);
    transparent_crc(g_587.f0, "g_587.f0", print_hash_value);
    transparent_crc(g_587.f3, "g_587.f3", print_hash_value);
    transparent_crc(g_601, "g_601", print_hash_value);
    transparent_crc(g_674.f0, "g_674.f0", print_hash_value);
    transparent_crc(g_674.f3, "g_674.f3", print_hash_value);
    transparent_crc(g_722, "g_722", print_hash_value);
    transparent_crc(g_750, "g_750", print_hash_value);
    transparent_crc_bytes (&g_771, sizeof(g_771), "g_771", print_hash_value);
    transparent_crc(g_864, "g_864", print_hash_value);
    transparent_crc(g_921, "g_921", print_hash_value);
    transparent_crc(g_1066.f0, "g_1066.f0", print_hash_value);
    transparent_crc(g_1066.f3, "g_1066.f3", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1083[i][j], "g_1083[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1104, "g_1104", print_hash_value);
    transparent_crc(g_1174.f0, "g_1174.f0", print_hash_value);
    transparent_crc(g_1174.f3, "g_1174.f3", print_hash_value);
    transparent_crc(g_1211, "g_1211", print_hash_value);
    transparent_crc(g_1215, "g_1215", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1266[i], "g_1266[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1321[i], "g_1321[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1505.f0, "g_1505.f0", print_hash_value);
    transparent_crc(g_1505.f3, "g_1505.f3", print_hash_value);
    transparent_crc(g_1580, "g_1580", print_hash_value);
    transparent_crc(g_1600.f0, "g_1600.f0", print_hash_value);
    transparent_crc(g_1600.f3, "g_1600.f3", print_hash_value);
    transparent_crc(g_1641.f0, "g_1641.f0", print_hash_value);
    transparent_crc(g_1641.f3, "g_1641.f3", print_hash_value);
    transparent_crc_bytes (&g_1789, sizeof(g_1789), "g_1789", print_hash_value);
    transparent_crc(g_1860.f0, "g_1860.f0", print_hash_value);
    transparent_crc(g_1860.f3, "g_1860.f3", print_hash_value);
    transparent_crc(g_1892.f0, "g_1892.f0", print_hash_value);
    transparent_crc(g_1892.f3, "g_1892.f3", print_hash_value);
    transparent_crc(g_1905, "g_1905", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2060[i], "g_2060[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2075.f0, "g_2075.f0", print_hash_value);
    transparent_crc(g_2075.f3, "g_2075.f3", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_2126[i], sizeof(g_2126[i]), "g_2126[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2136, "g_2136", print_hash_value);
    transparent_crc(g_2181.f0, "g_2181.f0", print_hash_value);
    transparent_crc(g_2181.f3, "g_2181.f3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2243[i][j].f0, "g_2243[i][j].f0", print_hash_value);
            transparent_crc(g_2243[i][j].f3, "g_2243[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_2244[i][j].f0, "g_2244[i][j].f0", print_hash_value);
            transparent_crc(g_2244[i][j].f3, "g_2244[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2379, "g_2379", print_hash_value);
    transparent_crc(g_2383, "g_2383", print_hash_value);
    transparent_crc(g_2507.f0, "g_2507.f0", print_hash_value);
    transparent_crc(g_2507.f3, "g_2507.f3", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2593[i], "g_2593[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2614, "g_2614", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2672[i][j].f0, "g_2672[i][j].f0", print_hash_value);
            transparent_crc(g_2672[i][j].f3, "g_2672[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_2677, sizeof(g_2677), "g_2677", print_hash_value);
    transparent_crc_bytes (&g_2793, sizeof(g_2793), "g_2793", print_hash_value);
    transparent_crc(g_2844, "g_2844", print_hash_value);
    transparent_crc_bytes (&g_2922, sizeof(g_2922), "g_2922", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_2936[i][j], "g_2936[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3082.f0, "g_3082.f0", print_hash_value);
    transparent_crc(g_3082.f3, "g_3082.f3", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_3083[i][j].f0, "g_3083[i][j].f0", print_hash_value);
            transparent_crc(g_3083[i][j].f3, "g_3083[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3115.f0, "g_3115.f0", print_hash_value);
    transparent_crc(g_3115.f3, "g_3115.f3", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 848
XXX total union variables: 33

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 28
breakdown:
   indirect level: 0, occurrence: 14
   indirect level: 1, occurrence: 12
   indirect level: 2, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 20
XXX times a bitfields struct on LHS: 2
XXX times a bitfields struct on RHS: 32
XXX times a single bitfield on LHS: 1
XXX times a single bitfield on RHS: 26

XXX max expression depth: 55
breakdown:
   depth: 1, occurrence: 194
   depth: 2, occurrence: 56
   depth: 3, occurrence: 5
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 10, occurrence: 4
   depth: 11, occurrence: 1
   depth: 12, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 4
   depth: 22, occurrence: 2
   depth: 23, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 3
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 3
   depth: 32, occurrence: 3
   depth: 33, occurrence: 1
   depth: 36, occurrence: 1
   depth: 40, occurrence: 1
   depth: 43, occurrence: 1
   depth: 55, occurrence: 1

XXX total number of pointers: 669

XXX times a variable address is taken: 1646
XXX times a pointer is dereferenced on RHS: 315
breakdown:
   depth: 1, occurrence: 211
   depth: 2, occurrence: 72
   depth: 3, occurrence: 31
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 395
breakdown:
   depth: 1, occurrence: 324
   depth: 2, occurrence: 42
   depth: 3, occurrence: 26
   depth: 4, occurrence: 3
XXX times a pointer is compared with null: 67
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 19
XXX times a pointer is qualified to be dereferenced: 11087

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 3627
   level: 2, occurrence: 703
   level: 3, occurrence: 368
   level: 4, occurrence: 91
   level: 5, occurrence: 47
XXX number of pointers point to pointers: 303
XXX number of pointers point to scalars: 339
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30.3
XXX average alias set size: 1.55

XXX times a non-volatile is read: 2389
XXX times a non-volatile is write: 1240
XXX times a volatile is read: 132
XXX    times read thru a pointer: 25
XXX times a volatile is write: 58
XXX    times written thru a pointer: 34
XXX times a volatile is available for access: 7.87e+03
XXX percentage of non-volatile access: 95

XXX forward jumps: 0
XXX backward jumps: 13

XXX stmts: 209
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 32
   depth: 2, occurrence: 26
   depth: 3, occurrence: 26
   depth: 4, occurrence: 42
   depth: 5, occurrence: 51

XXX percentage a fresh-made variable is used: 17.2
XXX percentage an existing variable is used: 82.8
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


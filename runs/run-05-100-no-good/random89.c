/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2414804896
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   const int32_t  f0;
   float  f1;
   const int16_t  f2;
   volatile uint32_t  f3;
   uint32_t  f4;
};

union U1 {
   const volatile int64_t  f0;
   int8_t  f1;
   volatile int8_t  f2;
   const unsigned f3 : 19;
};

union U2 {
   volatile int16_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static volatile float g_2 = 0x9.21DED6p+23;/* VOLATILE GLOBAL g_2 */
static uint16_t g_3 = 0x5950L;
static int32_t g_5 = 1L;
static int32_t * volatile g_4 = &g_5;/* VOLATILE GLOBAL g_4 */
static int32_t g_24 = 0x071A0513L;
static uint16_t g_62 = 0x22E7L;
static uint8_t g_73 = 0x4BL;
static uint8_t g_77 = 248UL;
static uint64_t g_88[5][5] = {{0UL,0xD2618108949E3B93LL,0UL,0x1CC5CA36B4E13384LL,0xD2618108949E3B93LL},{0xD8FDC3C1A33FE8F1LL,3UL,0x1CC5CA36B4E13384LL,0xD8FDC3C1A33FE8F1LL,0x1CC5CA36B4E13384LL},{0xD8FDC3C1A33FE8F1LL,0xD8FDC3C1A33FE8F1LL,18446744073709551615UL,0xD2618108949E3B93LL,18446744073709551606UL},{0UL,18446744073709551606UL,0x1CC5CA36B4E13384LL,0x1CC5CA36B4E13384LL,18446744073709551606UL},{18446744073709551606UL,3UL,0UL,18446744073709551606UL,0x1CC5CA36B4E13384LL}};
static volatile uint32_t g_96 = 0x3D13389DL;/* VOLATILE GLOBAL g_96 */
static union U1 g_102 = {0x4CB066DBB316AA2CLL};/* VOLATILE GLOBAL g_102 */
static const union U1 g_111[3][1][6] = {{{{3L},{3L},{0L},{3L},{3L},{0L}}},{{{3L},{3L},{0L},{3L},{3L},{0L}}},{{{3L},{3L},{0L},{3L},{3L},{0L}}}};
static int16_t g_116 = 0xFBB5L;
static float g_118 = (-0x1.7p-1);
static struct S0 g_121 = {0L,-0x6.1p-1,0x23DCL,18446744073709551615UL,0UL};/* VOLATILE GLOBAL g_121 */
static int32_t g_123[3] = {7L,7L,7L};
static int32_t *g_153 = &g_24;
static int32_t ** volatile g_152[8] = {&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153};
static int32_t ** volatile g_154 = &g_153;/* VOLATILE GLOBAL g_154 */
static const union U1 *g_158[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const union U1 ** volatile g_157 = &g_158[1];/* VOLATILE GLOBAL g_157 */
static int8_t g_180 = 1L;
static volatile int64_t g_181 = 0xDDED77BE777DDB8ELL;/* VOLATILE GLOBAL g_181 */
static volatile union U2 g_191 = {0x9637L};/* VOLATILE GLOBAL g_191 */
static int8_t *g_202 = &g_102.f1;
static int8_t * volatile *g_201 = &g_202;
static int32_t ** volatile g_204 = &g_153;/* VOLATILE GLOBAL g_204 */
static uint8_t g_250 = 0x91L;
static uint32_t g_264 = 9UL;
static float * volatile g_288 = &g_121.f1;/* VOLATILE GLOBAL g_288 */
static volatile union U1 g_305 = {4L};/* VOLATILE GLOBAL g_305 */
static uint16_t g_317[10][3] = {{65535UL,0xC215L,9UL},{0xA003L,0xA003L,0xBBAAL},{5UL,0xC215L,0xC215L},{0xBBAAL,0x20DEL,0xA804L},{5UL,0x0E4DL,5UL},{0xA003L,0xBBAAL,0xA804L},{65535UL,65535UL,0xC215L},{1UL,0xBBAAL,0xBBAAL},{0xC215L,0x0E4DL,9UL},{1UL,0x20DEL,1UL}};
static struct S0 g_379 = {0xC3B839A9L,0x7.70D985p-13,-1L,0x15616ADCL,4294967293UL};/* VOLATILE GLOBAL g_379 */
static int8_t **g_386 = &g_202;
static volatile union U2 g_435 = {0L};/* VOLATILE GLOBAL g_435 */
static union U1 g_447[6] = {{0x991AD8954F009D10LL},{0x991AD8954F009D10LL},{0x991AD8954F009D10LL},{0x991AD8954F009D10LL},{0x991AD8954F009D10LL},{0x991AD8954F009D10LL}};
static int64_t g_482 = 0xC21C4D97326A47E8LL;
static int8_t *** const *g_590 = (void*)0;
static int8_t *** const ** volatile g_589 = &g_590;/* VOLATILE GLOBAL g_589 */
static union U2 g_596 = {0xB219L};/* VOLATILE GLOBAL g_596 */
static volatile union U2 g_605 = {0xA4AAL};/* VOLATILE GLOBAL g_605 */
static int32_t * volatile g_612 = &g_123[1];/* VOLATILE GLOBAL g_612 */
static union U1 *g_643 = &g_102;
static union U1 ** volatile g_642 = &g_643;/* VOLATILE GLOBAL g_642 */
static int32_t ** volatile g_663 = (void*)0;/* VOLATILE GLOBAL g_663 */
static volatile union U2 g_695 = {-1L};/* VOLATILE GLOBAL g_695 */
static float * const  volatile g_696 = &g_121.f1;/* VOLATILE GLOBAL g_696 */
static const volatile struct S0 g_700 = {0xED6A183AL,0x0.0128ACp-26,0x0FC2L,0xB7FBCEFBL,1UL};/* VOLATILE GLOBAL g_700 */
static uint8_t g_702 = 0xA4L;
static volatile union U2 g_706 = {0x0857L};/* VOLATILE GLOBAL g_706 */
static uint8_t g_717 = 0xA4L;
static volatile uint16_t g_768 = 0x8B52L;/* VOLATILE GLOBAL g_768 */
static int32_t ** volatile g_794 = (void*)0;/* VOLATILE GLOBAL g_794 */
static int32_t ** volatile g_795 = &g_153;/* VOLATILE GLOBAL g_795 */
static volatile uint32_t * volatile g_860 = (void*)0;/* VOLATILE GLOBAL g_860 */
static volatile uint32_t * volatile * volatile g_859 = &g_860;/* VOLATILE GLOBAL g_859 */
static int32_t *g_974 = &g_123[1];
static int32_t g_987[9][4] = {{(-3L),(-3L),6L,(-3L)},{(-3L),1L,1L,(-3L)},{1L,(-3L),1L,1L},{(-3L),(-3L),6L,(-3L)},{(-3L),1L,1L,(-3L)},{1L,(-3L),1L,1L},{(-3L),(-3L),6L,(-3L)},{(-3L),1L,1L,(-3L)},{1L,(-3L),1L,1L}};
static const union U2 * volatile g_1103[10] = {&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596,&g_596};
static const union U2 * volatile *g_1102 = &g_1103[4];
static const uint64_t g_1110 = 0x08084398751AE057LL;
static const uint64_t *g_1109 = &g_1110;
static const uint64_t **g_1108 = &g_1109;
static volatile union U1 g_1113[9] = {{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL},{0x0B12FDC890593702LL}};
static struct S0 g_1124 = {0x6404E7DDL,-0x6.0p+1,0xF597L,4UL,4294967293UL};/* VOLATILE GLOBAL g_1124 */
static struct S0 *g_1126 = &g_121;
static struct S0 **g_1125 = &g_1126;
static union U2 g_1130 = {0x70EFL};/* VOLATILE GLOBAL g_1130 */
static int32_t g_1157 = 0xB6751CB6L;
static uint16_t g_1165 = 65535UL;
static union U2 g_1197 = {0x2B12L};/* VOLATILE GLOBAL g_1197 */
static const volatile union U2 g_1248 = {1L};/* VOLATILE GLOBAL g_1248 */
static uint32_t g_1255 = 0x221D7AA9L;
static uint64_t g_1256[8] = {0x20CD38E3E593F346LL,0x07EA922F8593F743LL,0x07EA922F8593F743LL,0x20CD38E3E593F346LL,0x07EA922F8593F743LL,0x07EA922F8593F743LL,0x20CD38E3E593F346LL,0x07EA922F8593F743LL};
static int16_t g_1290 = 1L;
static uint64_t g_1303[4] = {0xF8EEA1FA8A7234E3LL,0xF8EEA1FA8A7234E3LL,0xF8EEA1FA8A7234E3LL,0xF8EEA1FA8A7234E3LL};
static uint8_t * volatile g_1318 = &g_717;/* VOLATILE GLOBAL g_1318 */
static uint8_t * volatile * volatile g_1317[8] = {&g_1318,&g_1318,&g_1318,&g_1318,&g_1318,&g_1318,&g_1318,&g_1318};
static uint8_t * volatile * volatile * const  volatile g_1319 = (void*)0;/* VOLATILE GLOBAL g_1319 */
static uint8_t * volatile * volatile * volatile g_1320 = &g_1317[1];/* VOLATILE GLOBAL g_1320 */
static union U1 g_1323 = {-10L};/* VOLATILE GLOBAL g_1323 */
static float * volatile g_1384 = &g_379.f1;/* VOLATILE GLOBAL g_1384 */
static volatile union U2 g_1391 = {3L};/* VOLATILE GLOBAL g_1391 */
static uint64_t *g_1402 = &g_1303[0];
static int32_t *g_1431 = (void*)0;
static union U2 g_1504 = {0x7144L};/* VOLATILE GLOBAL g_1504 */
static volatile union U2 g_1505 = {0xDB69L};/* VOLATILE GLOBAL g_1505 */
static uint16_t g_1542 = 65527UL;
static int8_t * const *g_1559 = &g_202;
static int8_t * const **g_1558 = &g_1559;
static int8_t * const ***g_1557 = &g_1558;
static int8_t * const ****g_1556 = &g_1557;
static float * volatile g_1571[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile int32_t g_1610 = 1L;/* VOLATILE GLOBAL g_1610 */
static volatile int32_t *g_1609 = &g_1610;
static volatile int32_t **g_1608 = &g_1609;
static volatile int32_t ** const  volatile *g_1607 = &g_1608;
static volatile int32_t ** const  volatile ** volatile g_1606 = &g_1607;/* VOLATILE GLOBAL g_1606 */
static int32_t g_1623 = 1L;
static int32_t g_1634 = 0x80286108L;
static int16_t g_1679 = 0x3B28L;
static int32_t g_1697 = 0xB5BED2F1L;
static int32_t **g_1734 = &g_974;
static int32_t ***g_1733 = &g_1734;
static struct S0 g_1742 = {1L,0xF.8A43E8p-88,0x66CAL,0xFF2B5FCDL,0x9D1D7AD5L};/* VOLATILE GLOBAL g_1742 */
static int16_t *g_1746[8] = {&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679,&g_1679};
static union U2 g_1759[8] = {{0L},{0L},{0L},{0L},{0L},{0L},{0L},{0L}};
static union U2 g_1762 = {0x706FL};/* VOLATILE GLOBAL g_1762 */
static volatile union U1 g_1764 = {7L};/* VOLATILE GLOBAL g_1764 */
static const int32_t * volatile g_1770 = &g_123[1];/* VOLATILE GLOBAL g_1770 */
static const int32_t * volatile * volatile g_1771 = &g_1770;/* VOLATILE GLOBAL g_1771 */
static int16_t g_1778 = 0x17C4L;
static struct S0 g_1811 = {2L,-0x1.7p-1,-1L,18446744073709551615UL,0x72C26CF6L};/* VOLATILE GLOBAL g_1811 */
static const int32_t * volatile * volatile *g_1850 = &g_1771;
static const int32_t * volatile * volatile ** volatile g_1849 = &g_1850;/* VOLATILE GLOBAL g_1849 */
static float * volatile g_1872 = &g_1811.f1;/* VOLATILE GLOBAL g_1872 */
static int64_t g_1873 = 0L;
static volatile struct S0 g_1885[3][9][1] = {{{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{5L,-0x1.2p+1,0x2751L,0x843B5D9DL,0x795CDC23L}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}}},{{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{5L,-0x1.2p+1,0x2751L,0x843B5D9DL,0x795CDC23L}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}}},{{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{5L,-0x1.2p+1,0x2751L,0x843B5D9DL,0x795CDC23L}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{0L,0x5.6p-1,-1L,0x43942FAAL,4UL}},{{-1L,-0x1.1p+1,0xCEE1L,0x9FECBCFBL,0xD2E5BC3AL}},{{0xDB172EBBL,0x4.3C5669p+5,0L,0x2F7832EFL,0xE7451860L}}}};
static volatile struct S0 g_1925[8] = {{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL},{1L,-0x3.6p-1,6L,0xA791C177L,4294967287UL}};
static int8_t g_1984 = (-1L);
static uint64_t g_2032 = 1UL;
static float * volatile g_2033[8] = {&g_121.f1,&g_121.f1,&g_121.f1,&g_121.f1,&g_121.f1,&g_121.f1,&g_121.f1,&g_121.f1};
static float * volatile g_2034 = &g_121.f1;/* VOLATILE GLOBAL g_2034 */
static const union U1 ** volatile g_2041 = (void*)0;/* VOLATILE GLOBAL g_2041 */
static const union U1 ** volatile g_2042 = (void*)0;/* VOLATILE GLOBAL g_2042 */
static union U2 g_2060 = {3L};/* VOLATILE GLOBAL g_2060 */


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static const int32_t  func_8(float  p_9, int32_t * p_10, int16_t  p_11);
static union U2  func_12(uint16_t  p_13);
static int16_t  func_16(int32_t * p_17, uint8_t  p_18, uint16_t  p_19);
static int32_t * func_20(int32_t  p_21);
static uint8_t  func_28(const int16_t  p_29, int32_t * p_30);
static uint16_t  func_31(float  p_32, int16_t  p_33, float  p_34);
static float  func_35(int32_t * p_36);
static int32_t  func_40(uint32_t  p_41, int64_t  p_42, uint8_t  p_43, uint8_t  p_44, int32_t * p_45);
static int16_t  func_48(int32_t * p_49, int32_t  p_50);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_4 g_5 g_1742.f4 g_1778 g_1157 g_1125 g_1126 g_1811 g_121 g_447.f3 g_153 g_24 g_379.f2 g_386 g_202 g_102.f1 g_987 g_1402 g_1303 g_204 g_1733 g_1734 g_768 g_1607 g_1608 g_1609 g_1384 g_379.f1 g_73 g_1872 g_1873 g_1165
 * writes: g_5 g_1742.f4 g_1157 g_1290 g_987 g_717 g_482 g_974 g_24 g_1610 g_1811.f4 g_102.f1 g_1124.f1 g_1811.f1 g_1126 g_1165
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int64_t l_25 = 4L;
    int32_t l_1485[6];
    float *l_1791 = &g_1124.f1;
    float l_1799 = 0x3.A5FC87p+96;
    int32_t l_1856[2];
    struct S0 *l_1874 = &g_1811;
    int32_t l_1875 = 1L;
    int32_t * const ** const l_1880 = (void*)0;
    int8_t l_1977 = 0xC6L;
    int32_t **l_1978[2];
    int64_t l_1992[8];
    uint16_t l_1994[10] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
    uint8_t ***l_2009 = (void*)0;
    int64_t l_2080 = 0xCBC1F2A48E5B3C21LL;
    int i;
    for (i = 0; i < 6; i++)
        l_1485[i] = 0x1A23960CL;
    for (i = 0; i < 2; i++)
        l_1856[i] = (-1L);
    for (i = 0; i < 2; i++)
        l_1978[i] = (void*)0;
    for (i = 0; i < 8; i++)
        l_1992[i] = 1L;
    (*g_4) &= g_3;
    for (g_5 = 0; (g_5 != (-12)); g_5--)
    { /* block id: 4 */
        int32_t *l_22 = &g_5;
        int32_t *l_23[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int i;
    }
    for (g_1742.f4 = 0; (g_1742.f4 <= 3); g_1742.f4 += 1)
    { /* block id: 758 */
        int8_t l_1792 = 0x78L;
        int8_t ***l_1795 = &g_386;
        int32_t l_1826 = 0xE2B6579EL;
        uint8_t *l_1830 = &g_250;
        uint8_t **l_1829 = &l_1830;
        uint8_t ***l_1828 = &l_1829;
        uint8_t ****l_1827 = &l_1828;
        int32_t l_1833 = 0xDAE1FD98L;
        union U2 *l_1844 = &g_596;
        int32_t l_1943[7][10][3] = {{{0xF80C0081L,1L,0L},{1L,0x4E5F7671L,(-4L)},{0L,0x5E5424F3L,1L},{0xD4C0EEE2L,0xD4C0EEE2L,0x3B37F516L},{0x3B40DAC0L,(-9L),0x2032C792L},{0x3B37F516L,(-2L),0x2AD2D590L},{(-1L),0xB07CCCFEL,0xBBC44240L},{0L,0x3B37F516L,0x2AD2D590L},{0L,0L,0x2032C792L},{4L,(-8L),0x3B37F516L}},{{0xF80C0081L,0x7F0997E0L,1L},{(-4L),0xB46B7C9DL,(-4L)},{0x03AD558CL,1L,0L},{0xBF55BAD5L,0xD4C0EEE2L,(-8L)},{(-5L),0xF80C0081L,(-9L)},{0x3B37F516L,0x632FFC0DL,0x4E520DEAL},{(-5L),(-7L),0xBBC44240L},{0xBF55BAD5L,0x6512C2EAL,0xC11B565CL},{0x03AD558CL,0L,0x5E5424F3L},{(-4L),0x30BAFEB9L,0x30BAFEB9L}},{{0xF80C0081L,0x6B468043L,(-8L)},{4L,0L,(-4L)},{0L,0x2032C792L,0xA37F75BEL},{0L,0xD4C0EEE2L,0x6512C2EAL},{(-1L),0x2032C792L,1L},{0x3B37F516L,0L,0xA3019F1CL},{0x3B40DAC0L,0x6B468043L,0xBBC44240L},{0xD4C0EEE2L,0x30BAFEB9L,(-6L)},{0L,0L,0xF80C0081L},{1L,0x6512C2EAL,(-1L)}},{{0xF80C0081L,(-7L),0L},{0L,0x632FFC0DL,(-4L)},{0xA2DC89C7L,0xF80C0081L,0L},{0L,0xD4C0EEE2L,(-1L)},{0xA679642FL,1L,0xF80C0081L},{0x3B37F516L,0xB46B7C9DL,(-6L)},{0xBBC44240L,0x7F0997E0L,0xBBC44240L},{(-6L),(-8L),0xA3019F1CL},{2L,0L,1L},{1L,0x3B37F516L,0x6512C2EAL}},{{0xF80C0081L,0xB07CCCFEL,0xA37F75BEL},{1L,(-2L),(-4L)},{2L,(-9L),(-8L)},{(-6L),0xD4C0EEE2L,0x30BAFEB9L},{0xBBC44240L,0x5E5424F3L,0x5E5424F3L},{0x3B37F516L,0x4E5F7671L,0xC11B565CL},{0xA679642FL,1L,0xBBC44240L},{0L,(-1L),0x4E520DEAL},{0xA2DC89C7L,0L,(-9L)},{0L,(-1L),(-8L)}},{{0xF80C0081L,1L,0L},{1L,0x4E5F7671L,(-4L)},{0L,0x5E5424F3L,1L},{0xD4C0EEE2L,0xD4C0EEE2L,0x3B37F516L},{0x3B40DAC0L,(-9L),0x2032C792L},{0x3B37F516L,(-2L),0x2AD2D590L},{(-1L),0xB07CCCFEL,0xBBC44240L},{0L,0L,0L},{(-9L),0xF80C0081L,(-5L)},{0x2AD2D590L,1L,0L}},{{(-1L),0L,0x7F0997E0L},{0x4E520DEAL,0xD4C0EEE2L,0x4E520DEAL},{1L,0x3B40DAC0L,0xB07CCCFEL},{0x30BAFEB9L,0x3B37F516L,1L},{1L,(-1L),0xA679642FL},{0L,0L,0x4E5F7671L},{1L,0L,0xA37F75BEL},{0x30BAFEB9L,4L,(-2L)},{1L,0xF80C0081L,0xBBC44240L},{0x4E520DEAL,(-4L),(-4L)}}};
        struct S0 *** const l_1959 = &g_1125;
        float l_1986 = 0x0.1p-1;
        float l_2028 = 0xF.2C0F57p+80;
        int32_t l_2030 = 0x05E628EDL;
        uint32_t l_2056 = 0xA8B6D45FL;
        uint64_t *l_2076 = &g_1256[0];
        int i, j, k;
        for (g_1157 = 0; (g_1157 <= 3); g_1157 += 1)
        { /* block id: 761 */
            int32_t l_1790 = 0x522A3142L;
            uint32_t l_1800 = 1UL;
            struct S0 *l_1810 = &g_1811;
            for (g_1290 = 2; (g_1290 >= 0); g_1290 -= 1)
            { /* block id: 764 */
                int8_t l_1779 = (-1L);
                float *l_1780 = &g_379.f1;
                int64_t *l_1809 = &l_25;
                uint64_t *l_1824 = (void*)0;
                uint64_t *l_1825[2][8][10] = {{{(void*)0,&g_1303[3],&g_88[1][3],&g_88[2][3],(void*)0,(void*)0,&g_88[2][3],&g_88[1][3],&g_1303[3],(void*)0},{&g_1303[0],&g_88[3][2],&g_88[2][3],&g_1303[2],&g_1303[0],&g_1303[0],&g_1303[3],&g_1303[0],&g_88[3][2],(void*)0},{&g_1256[3],&g_1303[0],&g_88[1][3],&g_88[3][2],&g_1303[0],&g_1303[1],(void*)0,(void*)0,(void*)0,(void*)0},{&g_1303[0],&g_1303[3],(void*)0,&g_88[1][3],(void*)0,&g_88[1][3],(void*)0,&g_1303[3],&g_1303[0],&g_1303[0]},{&g_88[2][3],(void*)0,&g_88[3][2],&g_1303[0],&g_1303[0],&g_1303[2],(void*)0,&g_1303[1],(void*)0,&g_1256[3]},{&g_88[1][3],&g_1256[3],(void*)0,&g_1303[0],&g_1303[1],(void*)0,&g_1303[0],&g_1303[2],&g_1303[0],&g_1303[0]},{&g_88[1][3],&g_1303[1],&g_1256[3],&g_88[1][3],&g_88[1][3],&g_1256[3],&g_1303[1],&g_88[1][3],(void*)0,&g_88[2][3]},{&g_1303[3],&g_88[1][3],&g_1303[0],&g_88[3][2],&g_1303[2],(void*)0,(void*)0,(void*)0,&g_88[3][2],&g_88[1][3]}},{{&g_1303[2],&g_1256[3],&g_1303[0],&g_1303[2],(void*)0,(void*)0,&g_1303[0],&g_88[1][3],&g_1303[3],&g_88[1][3]},{&g_88[1][3],&g_88[1][3],&g_1256[3],&g_88[2][3],&g_1256[3],&g_88[1][3],&g_88[1][3],&g_1303[2],&g_88[3][2],&g_1303[3]},{&g_1303[0],&g_1303[0],(void*)0,&g_1303[3],(void*)0,(void*)0,&g_1303[0],&g_1303[1],&g_1303[0],&g_1303[2]},{&g_1303[1],&g_1303[0],&g_88[3][2],(void*)0,(void*)0,&g_88[0][3],&g_88[1][3],&g_1303[3],&g_1303[3],&g_88[1][3]},{&g_88[3][2],&g_88[1][3],(void*)0,(void*)0,&g_88[1][3],&g_88[3][2],&g_1303[0],(void*)0,(void*)0,&g_1303[0]},{&g_1303[0],&g_1256[3],&g_88[1][3],(void*)0,(void*)0,&g_1303[2],(void*)0,&g_1303[0],&g_1256[3],&g_1303[1]},{&g_1303[0],&g_88[1][3],&g_88[2][3],&g_1303[0],&g_88[0][3],&g_88[3][2],&g_1303[1],&g_88[1][3],&g_1303[1],&g_88[3][2]},{&g_88[3][2],&g_1303[1],&g_88[1][3],(void*)0,&g_88[2][3],&g_1303[1],&g_1256[3],&g_1303[0],(void*)0,&g_1303[2]}}};
                uint8_t ****l_1831 = &l_1828;
                int32_t * const *l_1853 = &g_1431;
                int32_t * const **l_1852 = &l_1853;
                int32_t * const ** const *l_1851[10][5] = {{&l_1852,&l_1852,&l_1852,(void*)0,&l_1852},{&l_1852,&l_1852,&l_1852,&l_1852,(void*)0},{&l_1852,(void*)0,&l_1852,&l_1852,(void*)0},{(void*)0,&l_1852,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1852,(void*)0,&l_1852,&l_1852},{&l_1852,&l_1852,(void*)0,&l_1852,&l_1852},{(void*)0,&l_1852,&l_1852,(void*)0,&l_1852},{(void*)0,(void*)0,(void*)0,&l_1852,&l_1852},{&l_1852,(void*)0,(void*)0,&l_1852,&l_1852},{&l_1852,&l_1852,(void*)0,(void*)0,&l_1852}};
                int i, j, k;
                g_987[(g_1157 + 3)][g_1742.f4] ^= (((safe_div_func_int64_t_s_s((safe_add_func_uint8_t_u_u((safe_add_func_int64_t_s_s(0x989C6A1EC038C198LL, g_1778)), ((((l_1779 , l_1780) == ((l_1779 || (safe_sub_func_int16_t_s_s((safe_add_func_int32_t_s_s((safe_div_func_int8_t_s_s(((g_1157 != 0xAE1CC99AL) ^ (safe_add_func_int8_t_s_s(l_1485[4], (((!((**g_1125) , g_447[4].f3)) > (-10L)) == (*g_153))))), l_1790)), 6L)), l_1485[4]))) , l_1791)) , l_1792) || g_379.f2))), l_1779)) < 1L) <= (**g_386));
                for (g_717 = 0; (g_717 <= 2); g_717 += 1)
                { /* block id: 768 */
                    for (g_482 = 2; (g_482 >= 0); g_482 -= 1)
                    { /* block id: 771 */
                        int32_t l_1793[2];
                        uint32_t l_1796 = 0UL;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1793[i] = 1L;
                        (**g_1733) = func_20((l_1793[0] & (!(*g_1402))));
                        (*g_153) ^= ((void*)0 == l_1795);
                        ++l_1796;
                        if (l_1800)
                            continue;
                    }
                    return g_768;
                }
            }
        }
        (***g_1607) = 8L;
        for (g_1811.f4 = 0; (g_1811.f4 <= 2); g_1811.f4 += 1)
        { /* block id: 816 */
            uint8_t l_1867 = 0xB2L;
            int32_t l_1868 = (-10L);
            (*g_1872) = ((((safe_mul_func_float_f_f(0x1.D31112p-73, (safe_add_func_float_f_f(l_1792, (((l_1833 = (l_1867 , (l_1868 = ((**g_386) = (((void*)0 == &g_642) <= l_1856[0]))))) , (((-((((l_1868 != ((*l_1791) = (((safe_add_func_int8_t_s_s(0x31L, l_1856[0])) , (*g_1384)) == g_73))) == g_102.f1) >= 0x0.6p-1) != 0x4.705F64p-29)) > 0x5.0p+1) > 0x4.C1D496p+31)) <= l_1485[3]))))) == 0x1.8990CAp-12) != (-0x4.3p-1)) > l_1856[0]);
            for (l_1867 = 0; (l_1867 <= 2); l_1867 += 1)
            { /* block id: 824 */
                (*g_1125) = ((**g_1125) , ((**g_1125) , (g_1873 , l_1874)));
            }
        }
        (*g_1734) = &l_1833;
        for (g_1165 = 0; (g_1165 <= 2); g_1165 += 1)
        { /* block id: 831 */
            int32_t l_1888 = 0x46E684FCL;
            uint32_t l_1947 = 0xA507444CL;
            uint32_t l_1958[7][7][5] = {{{0x417A41D8L,0x68AEBBDBL,0x88CB5180L,0x68AEBBDBL,0x417A41D8L},{4294967295UL,0x17A0E327L,0x04A68F00L,0x30715872L,0UL},{0x2A01AEF4L,1UL,4294967295UL,0x617CA751L,0x88CB5180L},{0x04A68F00L,7UL,7UL,0x17A0E327L,0UL},{0x4461E26EL,0x617CA751L,0x2A01AEF4L,2UL,0x417A41D8L},{0UL,0UL,0UL,4294967295UL,1UL},{5UL,0xB0DB49C7L,0x9270686EL,1UL,0x4461E26EL}},{{0UL,1UL,4294967295UL,0x04A68F00L,4UL},{0x0FB916F5L,0xD0313670L,0x9270686EL,4294967295UL,0x3CCFD4A4L},{0xEAC84776L,4294967290UL,0UL,0UL,4294967290UL},{0xED52C4E4L,0xCA7226E1L,0x2A01AEF4L,0x6D273136L,0xA202A4BEL},{7UL,4294967290UL,7UL,0UL,0x79325BD5L},{0x9270686EL,1UL,4294967295UL,1UL,0x8029DCC9L},{7UL,4294967295UL,0x04A68F00L,0xEAC84776L,0x17A0E327L}},{{0xED52C4E4L,4294967286UL,0x88CB5180L,0UL,0x2A01AEF4L},{0xEAC84776L,0x04A68F00L,4294967295UL,7UL,3UL},{0x0FB916F5L,1UL,3UL,0x13818C85L,0x58D699CCL},{0UL,7UL,4294967290UL,7UL,0x724EBC69L},{5UL,1UL,1UL,0UL,1UL},{0UL,0UL,4294967290UL,0xEAC84776L,7UL},{0x4461E26EL,0x13818C85L,0UL,1UL,0xED52C4E4L}},{{0x04A68F00L,4294967295UL,1UL,0UL,0x30715872L},{0x2A01AEF4L,0x13818C85L,0x2F961731L,0x6D273136L,3UL},{4294967295UL,0UL,0UL,0UL,4294967295UL},{0x417A41D8L,1UL,0x5B13720CL,4294967295UL,0x58C023D5L},{0x17A0E327L,7UL,7UL,0x04A68F00L,0UL},{0x5B13720CL,1UL,0x3CCFD4A4L,1UL,0xA202A4BEL},{0x04A68F00L,4294967295UL,0UL,0x70C28562L,0x70C28562L}},{{0xA202A4BEL,4294967295UL,0xA202A4BEL,0x13818C85L,1UL},{4294967290UL,1UL,4294967295UL,0UL,0x04A68F00L},{1UL,0xCA7226E1L,0x0FB916F5L,2UL,0UL},{3UL,7UL,4294967295UL,0x04A68F00L,0xEAC84776L},{0x58C023D5L,0x8281BC0BL,0xA202A4BEL,0xEB3089C9L,1UL},{4UL,4294967295UL,0UL,4294967290UL,0x79325BD5L},{0x4461E26EL,1UL,4294967288UL,4294967295UL,0x2A01AEF4L}},{{4294967295UL,4294967290UL,0x30715872L,3UL,4UL},{0x4461E26EL,1UL,0x417A41D8L,0x68AEBBDBL,0x88CB5180L},{4UL,0x4C27C753L,0x4C27C753L,4UL,0UL},{0x58C023D5L,2UL,3UL,4294967295UL,0xED52C4E4L},{3UL,0x30715872L,4294967290UL,4294967295UL,0x17A0E327L},{1UL,0x63305EB7L,4294967295UL,4294967295UL,0x58D699CCL},{4294967290UL,0UL,4294967295UL,4UL,4294967295UL}},{{0xA202A4BEL,0xEB3089C9L,1UL,0x68AEBBDBL,4294967288UL},{0x04A68F00L,4294967295UL,7UL,3UL,4UL},{0x417A41D8L,1UL,1UL,4294967295UL,0x5B13720CL},{0UL,4294967295UL,1UL,4294967290UL,4294967290UL},{0x9270686EL,0xEB3089C9L,1UL,0xEB3089C9L,0x9270686EL},{0x70C28562L,0UL,4294967295UL,0x04A68F00L,0x724EBC69L},{0x88CB5180L,0x63305EB7L,0x4461E26EL,2UL,1UL}}};
            int8_t *l_1960 = &g_180;
            int64_t l_1987 = (-6L);
            int32_t l_1989[5][6];
            float l_2029[2][2][5] = {{{0x7.6p-1,0xD.CC9871p+10,0xD.CC9871p+10,0x7.6p-1,0xD.CC9871p+10},{0x6.Fp-1,0x6.Fp-1,0x1.2p+1,0x6.Fp-1,0x6.Fp-1}},{{0xD.CC9871p+10,0x7.6p-1,0xD.CC9871p+10,0xD.CC9871p+10,0x7.6p-1},{0x6.Fp-1,0xB.2F867Bp+54,0xB.2F867Bp+54,0x6.Fp-1,0xB.2F867Bp+54}}};
            int i, j, k;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 6; j++)
                    l_1989[i][j] = 0x7BD98928L;
            }
        }
    }
    return l_2080;
}


/* ------------------------------------------ */
/* 
 * reads : g_386 g_202 g_102.f1 g_1109 g_1110 g_116 g_288 g_121.f1 g_123 g_77 g_379.f4 g_317 g_1402 g_1303 g_305.f0 g_73 g_1248.f0 g_987 g_974 g_1542 g_264 g_696 g_1556 g_1157 g_62 g_1125 g_1126 g_1557 g_1558 g_1559 g_1606 g_88
 * writes: g_379.f1 g_121.f1 g_379.f4 g_77 g_482 g_102.f1 g_987 g_1542 g_116 g_62 g_264 g_1126 g_702 g_1559
 */
static const int32_t  func_8(float  p_9, int32_t * p_10, int16_t  p_11)
{ /* block id: 639 */
    int32_t *l_1506[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint8_t l_1507[5] = {0xB6L,0xB6L,0xB6L,0xB6L,0xB6L};
    float *l_1514 = &g_379.f1;
    uint64_t l_1517 = 18446744073709551614UL;
    int32_t l_1518 = 0x832E5DD8L;
    uint64_t l_1585[1][7][3] = {{{0x06088F4B9D1482D8LL,0x06088F4B9D1482D8LL,0x332AECAB43EFD64CLL},{1UL,0x49A1DB2E45EE3682LL,0x49A1DB2E45EE3682LL},{0x332AECAB43EFD64CLL,0x1E8AE99CD0073860LL,0xA286805F09780F13LL},{1UL,18446744073709551615UL,1UL},{0x06088F4B9D1482D8LL,0x332AECAB43EFD64CLL,0xA286805F09780F13LL},{0UL,0UL,0x49A1DB2E45EE3682LL},{0xB714D2791BDA276DLL,0x332AECAB43EFD64CLL,0x332AECAB43EFD64CLL}}};
    int32_t **l_1598 = (void*)0;
    int32_t ** const *l_1597 = &l_1598;
    int32_t ** const **l_1596[9][2][5] = {{{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}},{{&l_1597,&l_1597,&l_1597,&l_1597,(void*)0},{(void*)0,&l_1597,&l_1597,&l_1597,&l_1597}},{{&l_1597,&l_1597,&l_1597,(void*)0,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}},{{&l_1597,&l_1597,(void*)0,&l_1597,(void*)0},{(void*)0,&l_1597,&l_1597,&l_1597,&l_1597}},{{(void*)0,(void*)0,&l_1597,(void*)0,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}},{{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}},{{(void*)0,&l_1597,(void*)0,&l_1597,&l_1597},{(void*)0,&l_1597,(void*)0,&l_1597,&l_1597}},{{&l_1597,(void*)0,&l_1597,&l_1597,&l_1597},{&l_1597,&l_1597,&l_1597,&l_1597,&l_1597}},{{&l_1597,(void*)0,&l_1597,&l_1597,(void*)0},{&l_1597,&l_1597,(void*)0,&l_1597,&l_1597}}};
    const int16_t l_1617[4][9] = {{0L,0xE3BCL,1L,0xE3BCL,0L,1L,(-7L),(-7L),1L},{9L,0L,0x6CD1L,0L,9L,0x6CD1L,0xE94DL,0xE94DL,0x6CD1L},{0L,0xE3BCL,1L,0xE3BCL,0L,1L,(-7L),(-7L),1L},{9L,0L,0x6CD1L,0L,9L,0x6CD1L,0xE94DL,0xE94DL,0x6CD1L}};
    int8_t *** const l_1647 = &g_386;
    int8_t *** const **l_1755 = &g_590;
    union U2 * const l_1761 = &g_1762;
    union U2 * const *l_1760 = &l_1761;
    int i, j, k;
    l_1507[4]--;
    if ((safe_div_func_uint64_t_u_u((((-1L) < (**g_386)) < (*g_1109)), (g_116 | ((safe_mul_func_float_f_f((&l_1507[0] == &l_1507[4]), ((*g_288) = (((((0x5.B811DAp-48 != (((*l_1514) = p_9) <= (safe_sub_func_float_f_f(l_1517, (-0x3.2p+1))))) >= (*g_288)) <= g_123[2]) <= 0x9.3p-1) == l_1518)))) , p_11)))))
    { /* block id: 643 */
        uint16_t l_1537[5] = {0x8F91L,0x8F91L,0x8F91L,0x8F91L,0x8F91L};
        uint32_t l_1538[2];
        int32_t l_1540 = 0xCBDEBD38L;
        int32_t l_1541 = 0L;
        int32_t l_1575 = 0x2F0BEE13L;
        int32_t l_1576 = (-1L);
        float l_1579[9] = {(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1),(-0x1.Cp-1)};
        int32_t l_1580[2][9] = {{(-3L),9L,(-3L),(-3L),9L,(-3L),(-3L),9L,(-3L)},{0x00F5E793L,0x279A8C4DL,0x00F5E793L,0x00F5E793L,0x279A8C4DL,0x00F5E793L,0x00F5E793L,0x279A8C4DL,0x00F5E793L}};
        int i, j;
        for (i = 0; i < 2; i++)
            l_1538[i] = 4UL;
        for (g_379.f4 = 0; (g_379.f4 <= 3); g_379.f4 += 1)
        { /* block id: 646 */
            union U1 **l_1530 = &g_643;
            int32_t l_1539[2][3][10] = {{{(-6L),0xB2383214L,(-1L),0x20C206A5L,0xB2383214L,0x20C206A5L,(-1L),0xB2383214L,(-6L),(-6L)},{0x2AFF7B75L,0xB77B3958L,3L,0xB2383214L,0xB2383214L,3L,0xB77B3958L,0x2AFF7B75L,3L,0x2AFF7B75L},{0xB2383214L,(-1L),0x20C206A5L,0xB2383214L,0x20C206A5L,(-1L),0xB2383214L,(-6L),(-6L),0xB2383214L}},{{(-6L),0x2AFF7B75L,0x20C206A5L,0x20C206A5L,0x2AFF7B75L,(-6L),(-1L),0x2AFF7B75L,(-1L),(-6L)},{0xB77B3958L,0x2AFF7B75L,3L,0x2AFF7B75L,0xB77B3958L,3L,0xB2383214L,0xB2383214L,3L,0xB77B3958L},{0xB77B3958L,(-1L),(-1L),0xB77B3958L,0x20C206A5L,(-6L),0xB77B3958L,(-6L),0x20C206A5L,0xB77B3958L}}};
            int8_t * const ****l_1560 = &g_1557;
            uint32_t * const l_1565 = &l_1538[1];
            int8_t ***l_1570[5];
            int8_t ****l_1569 = &l_1570[3];
            int8_t *****l_1568 = &l_1569;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_1570[i] = &g_386;
            for (g_77 = 0; (g_77 <= 2); g_77 += 1)
            { /* block id: 649 */
                union U1 ***l_1531 = &l_1530;
                union U1 **l_1533[10][9] = {{(void*)0,&g_643,&g_643,(void*)0,&g_643,&g_643,&g_643,(void*)0,(void*)0},{&g_643,(void*)0,&g_643,&g_643,(void*)0,&g_643,(void*)0,&g_643,&g_643},{&g_643,&g_643,&g_643,(void*)0,&g_643,&g_643,(void*)0,&g_643,&g_643},{(void*)0,(void*)0,&g_643,&g_643,(void*)0,(void*)0,(void*)0,&g_643,&g_643},{&g_643,&g_643,&g_643,&g_643,&g_643,&g_643,&g_643,(void*)0,&g_643},{(void*)0,(void*)0,&g_643,(void*)0,(void*)0,&g_643,(void*)0,(void*)0,&g_643},{(void*)0,&g_643,&g_643,&g_643,&g_643,&g_643,&g_643,(void*)0,&g_643},{&g_643,(void*)0,&g_643,&g_643,&g_643,&g_643,&g_643,&g_643,&g_643},{(void*)0,&g_643,&g_643,&g_643,&g_643,(void*)0,&g_643,&g_643,&g_643},{&g_643,&g_643,&g_643,&g_643,(void*)0,&g_643,&g_643,(void*)0,&g_643}};
                union U1 ***l_1532 = &l_1533[0][3];
                uint8_t l_1534 = 0x09L;
                int i, j;
                g_987[(g_77 + 5)][g_77] &= (((safe_add_func_uint32_t_u_u((((safe_rshift_func_uint16_t_u_s(g_123[g_77], g_317[(g_379.f4 + 2)][g_77])) , g_317[(g_379.f4 + 2)][g_77]) > (~((safe_sub_func_uint16_t_u_u((((l_1538[1] = ((*g_1402) < (safe_sub_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u(g_1110, ((((*l_1532) = ((*l_1531) = l_1530)) != (void*)0) && l_1534))), (((*g_202) = (((((g_482 = ((safe_div_func_uint32_t_u_u((((g_305.f0 > (*g_1402)) >= g_317[0][1]) <= l_1537[4]), g_116)) < g_73)) >= 18446744073709551615UL) | 0xC5D7D32BL) , (void*)0) == (void*)0)) , g_1248.f0))))) , l_1539[0][1][0]) || p_11), g_1303[0])) || l_1537[4]))), 0x39EDF855L)) >= p_11) , g_317[(g_379.f4 + 2)][g_77]);
                if ((*g_974))
                    break;
                ++g_1542;
            }
            for (g_116 = 2; (g_116 >= 0); g_116 -= 1)
            { /* block id: 661 */
                int32_t l_1573 = (-1L);
                int32_t l_1577 = 0x3EC4A618L;
                int32_t l_1581 = 0x7BE9DFD3L;
                int32_t l_1582 = 0xA131C554L;
                int32_t l_1583 = (-10L);
                int32_t l_1584 = 0x025764F5L;
                union U2 *l_1605 = &g_1130;
                union U2 **l_1604[6] = {&l_1605,&l_1605,&l_1605,&l_1605,&l_1605,&l_1605};
                uint8_t *l_1611 = &g_702;
                int i, j;
                for (l_1540 = 0; (l_1540 <= 2); l_1540 += 1)
                { /* block id: 664 */
                    int32_t l_1572 = 7L;
                    int32_t l_1574 = 1L;
                    int32_t l_1578[4][6] = {{1L,1L,0x83E58392L,0x7F2D86E0L,0x83E58392L,1L},{0x83E58392L,0xB2004B5DL,0x7F2D86E0L,0x7F2D86E0L,0xB2004B5DL,0x83E58392L},{1L,0x83E58392L,0x7F2D86E0L,0x83E58392L,1L,1L},{(-7L),0x83E58392L,0x83E58392L,(-7L),0xB2004B5DL,(-7L)}};
                    int i, j;
                    if (g_123[l_1540])
                        break;
                    for (g_62 = 0; (g_62 <= 2); g_62 += 1)
                    { /* block id: 668 */
                        uint32_t *l_1549 = &l_1538[1];
                        uint32_t *l_1552 = &g_264;
                        uint32_t **l_1566 = &l_1552;
                        int16_t *l_1567[4][2][7] = {{{&g_1290,&g_1290,&g_1290,(void*)0,(void*)0,&g_1290,&g_1290},{&g_1290,&g_116,&g_116,&g_1290,&g_1290,&g_116,&g_116}},{{(void*)0,&g_1290,(void*)0,&g_1290,(void*)0,&g_116,&g_116},{&g_116,(void*)0,&g_116,&g_1290,(void*)0,&g_1290,&g_116}},{{&g_116,&g_116,&g_1290,(void*)0,&g_1290,&g_1290,&g_1290},{&g_116,&g_1290,&g_116,&g_116,&g_1290,&g_116,(void*)0}},{{(void*)0,&g_1290,&g_1290,&g_1290,&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290,&g_116,(void*)0,&g_116,&g_116}}};
                        int i, j, k;
                        (*g_696) = ((*l_1514) = (safe_sub_func_float_f_f((l_1537[2] < (safe_add_func_float_f_f(0x6.8EBA27p+80, (0x6.E7AC91p+65 == ((((*l_1552) ^= (--(*l_1549))) , (((!((((*g_696) > 0xF.DF36F9p+63) != (safe_mul_func_float_f_f((((l_1560 = g_1556) != ((safe_sub_func_int32_t_s_s(((**g_386) && (safe_mod_func_int8_t_s_s((((p_11 = (l_1565 == ((*l_1566) = p_10))) | 0x70FDL) >= l_1538[1]), 0xCCL))), (*p_10))) , l_1568)) == g_123[l_1540]), 0x2.F25FEDp+15))) >= 0x0.DB8790p-86)) > 0x0.5p-1) != l_1539[0][1][0])) < p_9))))), g_62)));
                    }
                    l_1585[0][2][1]++;
                }
                (*g_1125) = (*g_1125);
                g_987[(g_379.f4 + 4)][g_379.f4] = (safe_rshift_func_int8_t_s_u((g_123[g_116] ^ (0x85L & 247UL)), g_123[g_116]));
                (*l_1514) = (((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(g_123[g_116], ((*l_1611) = (l_1596[2][1][1] == ((((*****g_1556) <= (safe_sub_func_uint8_t_u_u(p_11, p_11))) > ((~(safe_mul_func_uint8_t_u_u(((*p_10) < (-1L)), (l_1604[0] == (void*)0)))) , l_1539[1][1][4])) , g_1606))))), g_88[1][3])), 14)) , 0x0EL) , l_1577);
            }
        }
    }
    else
    { /* block id: 685 */
        uint8_t l_1614[2][3] = {{255UL,255UL,255UL},{8UL,8UL,8UL}};
        int i, j;
        for (g_1542 = 0; (g_1542 != 41); g_1542 = safe_add_func_int32_t_s_s(g_1542, 1))
        { /* block id: 688 */
            (*g_1558) = (**g_1557);
        }
        --l_1614[0][0];
    }
    return (*p_10);
}


/* ------------------------------------------ */
/* 
 * reads : g_204 g_153 g_1504
 * writes: g_153
 */
static union U2  func_12(uint16_t  p_13)
{ /* block id: 636 */
    uint16_t l_1503[4] = {4UL,4UL,4UL,4UL};
    int i;
    (*g_204) = func_20(l_1503[0]);
    return g_1504;
}


/* ------------------------------------------ */
/* 
 * reads : g_1124.f4 g_180 g_1124.f2
 * writes: g_1124.f4 g_180 g_386
 */
static int16_t  func_16(int32_t * p_17, uint8_t  p_18, uint16_t  p_19)
{ /* block id: 622 */
    uint32_t l_1502 = 0UL;
    for (g_1124.f4 = (-26); (g_1124.f4 == 52); g_1124.f4 = safe_add_func_int64_t_s_s(g_1124.f4, 1))
    { /* block id: 625 */
        uint64_t l_1494[8] = {9UL,9UL,9UL,9UL,9UL,9UL,9UL,9UL};
        int i;
        --l_1494[4];
    }
    for (g_180 = 0; (g_180 < (-17)); g_180 = safe_sub_func_uint32_t_u_u(g_180, 8))
    { /* block id: 630 */
        int8_t **l_1499 = &g_202;
        int8_t ***l_1500 = &l_1499;
        int8_t ***l_1501 = &g_386;
        (*l_1501) = ((*l_1500) = l_1499);
        return l_1502;
    }
    return g_1124.f2;
}


/* ------------------------------------------ */
/* 
 * reads : g_204 g_153
 * writes:
 */
static int32_t * func_20(int32_t  p_21)
{ /* block id: 6 */
    int32_t *l_37[2][3] = {{(void*)0,(void*)0,(void*)0},{&g_24,&g_24,&g_24}};
    int64_t l_646 = 0x52C0D39086C62549LL;
    int64_t l_661 = (-6L);
    int8_t ***l_699[5][6] = {{&g_386,(void*)0,(void*)0,&g_386,&g_386,&g_386},{&g_386,(void*)0,(void*)0,&g_386,&g_386,(void*)0},{(void*)0,(void*)0,&g_386,(void*)0,&g_386,(void*)0},{&g_386,(void*)0,(void*)0,&g_386,&g_386,&g_386},{&g_386,(void*)0,(void*)0,&g_386,&g_386,(void*)0}};
    uint8_t l_744 = 0x69L;
    uint32_t l_747 = 0xA245FCD5L;
    uint64_t *l_809 = (void*)0;
    uint64_t **l_808[2];
    int32_t **l_831 = (void*)0;
    int32_t ***l_830[1];
    const uint32_t *l_862 = (void*)0;
    const uint32_t **l_861 = &l_862;
    int32_t l_904 = 2L;
    int32_t *l_1036[1][2][2] = {{{&g_123[0],&g_123[0]},{&g_123[0],&g_123[0]}}};
    int8_t **l_1089[6] = {&g_202,&g_202,&g_202,&g_202,&g_202,&g_202};
    struct S0 **l_1128 = (void*)0;
    struct S0 **l_1129 = (void*)0;
    uint32_t l_1314 = 0UL;
    int16_t l_1337 = 5L;
    int32_t *l_1381 = (void*)0;
    union U1 **l_1411 = &g_643;
    uint8_t *l_1454 = &g_73;
    uint8_t **l_1453[7][1][5] = {{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}},{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}},{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}},{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}},{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}},{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}},{{&l_1454,&l_1454,&l_1454,&l_1454,&l_1454}}};
    uint8_t ***l_1452 = &l_1453[4][0][0];
    int32_t *l_1472[6];
    uint32_t l_1482 = 0xAC5724FBL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_808[i] = &l_809;
    for (i = 0; i < 1; i++)
        l_830[i] = &l_831;
    for (i = 0; i < 6; i++)
        l_1472[i] = &g_5;
    return (*g_204);
}


/* ------------------------------------------ */
/* 
 * reads : g_379.f4 g_121.f4 g_612 g_123 g_96 g_121.f2 g_317 g_288 g_596.f0 g_116 g_3 g_62 g_642 g_482 g_121.f0
 * writes: g_379.f4 g_123 g_379.f1 g_121.f1 g_643 g_482
 */
static uint8_t  func_28(const int16_t  p_29, int32_t * p_30)
{ /* block id: 259 */
    uint8_t l_613 = 255UL;
    int8_t ***l_634 = &g_386;
    int8_t ****l_633[3];
    int8_t *****l_632 = &l_633[0];
    int32_t l_638 = 0xF29EFDFEL;
    int32_t *l_640 = &g_123[0];
    union U1 *l_641 = &g_447[4];
    int i;
    for (i = 0; i < 3; i++)
        l_633[i] = &l_634;
    for (g_379.f4 = 0; (g_379.f4 > 5); g_379.f4 = safe_add_func_uint8_t_u_u(g_379.f4, 1))
    { /* block id: 262 */
        int32_t l_611[7] = {(-3L),1L,1L,(-3L),1L,1L,(-3L)};
        uint16_t *l_629 = (void*)0;
        int32_t l_630 = 8L;
        int64_t *l_631 = (void*)0;
        float *l_635 = &g_379.f1;
        float *l_636[2];
        int32_t l_637 = 0xF7781105L;
        int32_t *l_639[1];
        int i;
        for (i = 0; i < 2; i++)
            l_636[i] = (void*)0;
        for (i = 0; i < 1; i++)
            l_639[i] = &g_123[1];
        (*g_612) |= (safe_sub_func_uint64_t_u_u(l_611[0], g_121.f4));
        if (l_613)
            break;
        g_123[1] ^= ((((p_29 > ((safe_sub_func_uint8_t_u_u(((((safe_sub_func_int32_t_s_s((safe_sub_func_uint64_t_u_u(l_613, (((&g_158[3] != (((-((safe_sub_func_float_f_f(((l_638 = (safe_add_func_float_f_f((l_637 = ((*g_288) = ((*l_635) = ((((+(safe_sub_func_int32_t_s_s((+(l_630 &= g_96)), (((void*)0 != l_631) <= g_121.f2)))) , l_632) == (void*)0) != g_317[0][1])))), (-0x1.2p-1)))) != (-0x3.Ep+1)), 0x4.CEF7ADp-85)) > 0x9.7p+1)) , g_596.f0) , (void*)0)) ^ 0UL) && p_29))), l_613)) <= 0x90L) , 0UL) ^ g_116), p_29)) > l_613)) > g_3) >= l_611[5]) > g_62);
    }
    (*l_640) &= l_613;
    (*g_642) = l_641;
    for (g_482 = 0; (g_482 <= 2); g_482 += 1)
    { /* block id: 276 */
        return (*l_640);
    }
    return g_121.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_24 g_3 g_62 g_73 g_77 g_5 g_96 g_102 g_102.f1 g_111 g_111.f2 g_111.f3 g_88 g_121 g_111.f0 g_4 g_154 g_157 g_123 g_116 g_153 g_180 g_181 g_191 g_201 g_204 g_111.f1 g_202 g_264 g_118
 * writes: g_62 g_73 g_77 g_24 g_96 g_4 g_102.f1 g_116 g_118 g_123 g_153 g_158 g_180 g_201 g_264 g_3 g_250
 */
static uint16_t  func_31(float  p_32, int16_t  p_33, float  p_34)
{ /* block id: 10 */
    int32_t *l_51[10][6][4] = {{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}},{{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24},{&g_24,&g_24,&g_24,&g_24},{&g_5,&g_24,&g_24,&g_24}}};
    uint8_t *l_249[10] = {&g_250,&g_250,&g_250,&g_250,&g_250,&g_250,&g_250,&g_250,&g_250,&g_250};
    float l_600[5][8][4] = {{{0x8.2AFC8Fp-98,0x8.2AFC8Fp-98,0xB.BB1930p+9,0x8.477354p+98},{(-0x10.Bp-1),0x0.1p+1,0x8.1p-1,0x4.2B45B7p+37},{0x0.0p+1,0x9.A56A52p+85,0xF.1AD3B7p+78,0x8.1p-1},{0x1.D7C041p-34,0x9.A56A52p+85,0x0.0B2B2Bp+74,0x4.2B45B7p+37},{0x9.A56A52p+85,0x0.1p+1,0x3.Ep-1,0x8.477354p+98},{0x0.193C50p-66,0x8.2AFC8Fp-98,0x1.598F1Fp-19,0x0.0p+1},{0xA.57CC34p+59,0xC.429ECEp+16,0x5.0612FEp-41,0x8.2AFC8Fp-98},{0x4.2B45B7p+37,0x0.8p-1,0xD.C194E3p-44,0x1.Dp+1}},{{(-0x10.4p+1),0x2.D6D4AFp-39,0x1.598F1Fp-19,(-0x10.4p+1)},{0xC.429ECEp+16,0x4.2B45B7p+37,(-0x1.Fp+1),0x4.6E00E3p-5},{0x9.A56A52p+85,0x1.4A6862p+70,0x4.Ap-1,0x4.EE817Fp+56},{0x0.0p-1,0x1.4A6862p+70,0x1.Dp+1,0x1.0p-1},{0x3.Ep-1,(-0x1.Ap-1),0x4.2B45B7p+37,(-0x1.Ap-1)},{0xD.C194E3p-44,0x6.0p-1,0xF.683C37p-20,0x7.BF7971p-73},{0x4.Ap-1,0xF.1AD3B7p+78,(-0x1.Fp+1),0xF.36CB20p-94},{0x1.4A6862p+70,0x4.EE817Fp+56,(-0x10.8p-1),0x8.1p-1}},{{0x1.4A6862p+70,0x0.5p+1,(-0x1.Fp+1),0x3.Ep-1},{0x4.Ap-1,0x8.1p-1,0xF.683C37p-20,(-0x9.Cp+1)},{0xD.C194E3p-44,0x0.8p-1,0x4.2B45B7p+37,(-0x10.4p+1)},{0x3.Ep-1,0x9.7p-1,0x1.Dp+1,0x6.9B5D0Ap-35},{0x0.0p-1,0x9.F81CEDp+56,0x4.Ap-1,0x6.0p-1},{0x9.F81CEDp+56,0x0.8p-1,0x4.9p-1,0x0.193C50p-66},{0x1.598F1Fp-19,0x4.Ap-1,0x1.15F7CFp-42,0x3.Ep-1},{0x2.CEED1Ep+23,0x1.598F1Fp-19,(-0x9.Cp+1),0x4.Ap-1}},{{(-0x10.4p+1),0x4.EE817Fp+56,0xC.429ECEp+16,0x0.0p+1},{0x7.BF7971p-73,0x1.15F7CFp-42,0x1.15F7CFp-42,0x7.BF7971p-73},{0x0.5p+1,(-0x10.4p+1),0x9.7p-1,0x0.0B2B2Bp+74},{0x9.F81CEDp+56,(-0x1.Ap-1),0x1.D7C041p-34,0x4.EE817Fp+56},{0x6.9B5D0Ap-35,(-0x1.5p+1),0x1.Dp+1,0x4.EE817Fp+56},{(-0x1.Fp+1),(-0x1.Ap-1),0x0.0p-1,0x0.0B2B2Bp+74},{0xD.C194E3p-44,(-0x10.4p+1),(-0x1.2p-1),0x7.BF7971p-73},{0x8.1p-1,0x1.15F7CFp-42,(-0x1.Fp+1),0x0.0p+1}},{{(-0x1.5p+1),0x4.EE817Fp+56,0xF.36CB20p-94,0x4.Ap-1},{0x1.4A6862p+70,0x1.598F1Fp-19,0x9.A56A52p+85,0x3.Ep-1},{0x8.1p-1,0x4.Ap-1,0xF.683C37p-20,0x0.193C50p-66},{0x5.0612FEp-41,0x0.8p-1,0x0.0p-1,0x6.0p-1},{0x3.Ep-1,0x9.F81CEDp+56,0x1.Dp+1,0x6.9B5D0Ap-35},{0x6.9B5D0Ap-35,0x9.7p-1,0x4.Ap-1,(-0x10.4p+1)},{0x9.7p-1,0x0.8p-1,0x9.7p-1,(-0x9.Cp+1)},{0x1.598F1Fp-19,0x8.1p-1,0x0.3p+1,0x3.Ep-1}}};
    int32_t l_601[8] = {0x41311C1AL,0L,0L,0x41311C1AL,0L,0L,0x41311C1AL,0L};
    int i, j, k;
    l_601[7] = func_40((safe_mul_func_int16_t_s_s(func_48(l_51[6][3][1], g_24), ((((l_249[9] = l_249[1]) != &g_250) | 0x2BL) != ((safe_unary_minus_func_int8_t_s((safe_lshift_func_uint16_t_u_u((((((((0x8.2977ADp+62 > (safe_add_func_float_f_f(p_32, ((safe_sub_func_uint8_t_u_u(((g_3 && 8L) >= 0xDAEAC2B9L), p_33)) , 0xC.CA6D61p-15)))) > p_32) != 0x8.4p-1) < g_88[3][3]) , p_33) >= g_111[0][0][1].f1) != 0x7D84L), 0)))) || p_33)))), p_33, p_33, p_33, &g_5);
    return g_264;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static float  func_35(int32_t * p_36)
{ /* block id: 8 */
    int64_t l_38 = 0x058BBE58ED0BD2F2LL;
    return l_38;
}


/* ------------------------------------------ */
/* 
 * reads : g_111.f0 g_264 g_3 g_118 g_88 g_121.f0 g_116 g_121.f4 g_121.f3 g_180 g_73 g_4 g_5
 * writes: g_264 g_62 g_3 g_250 g_73
 */
static int32_t  func_40(uint32_t  p_41, int64_t  p_42, uint8_t  p_43, uint8_t  p_44, int32_t * p_45)
{ /* block id: 124 */
    uint32_t *l_263 = &g_264;
    int32_t **l_270[4];
    int32_t ***l_269 = &l_270[0];
    int32_t ***l_271 = &l_270[0];
    uint16_t *l_274 = &g_62;
    uint32_t l_275 = 0x900534BFL;
    uint16_t *l_276[2][1][6] = {{{&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0}},{{&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0}}};
    uint32_t *l_277 = (void*)0;
    uint32_t *l_278 = &l_275;
    uint8_t *l_279 = &g_250;
    uint64_t l_280[5][5] = {{4UL,7UL,0x3EE8996AB2E62D11LL,0x96E4FF2E12BC1A5BLL,0x96E4FF2E12BC1A5BLL},{7UL,4UL,7UL,0x3EE8996AB2E62D11LL,0x96E4FF2E12BC1A5BLL},{0x89C417655FDF3245LL,0UL,0x96E4FF2E12BC1A5BLL,0UL,0x89C417655FDF3245LL},{7UL,0UL,4UL,0x89C417655FDF3245LL,4UL},{4UL,4UL,0x96E4FF2E12BC1A5BLL,0x89C417655FDF3245LL,0xAD0BF71D9775A7EFLL}};
    const uint16_t l_292 = 0x5F55L;
    int8_t ***l_404[5][1][4] = {{{&g_386,&g_386,&g_386,&g_386}},{{&g_386,&g_386,&g_386,&g_386}},{{&g_386,&g_386,&g_386,&g_386}},{{&g_386,&g_386,&g_386,&g_386}},{{&g_386,&g_386,&g_386,&g_386}}};
    int64_t *l_409 = (void*)0;
    uint64_t l_449 = 18446744073709551608UL;
    int16_t l_458[9] = {0xE808L,0x9A64L,0xE808L,0x9A64L,0xE808L,0x9A64L,0xE808L,0x9A64L,0xE808L};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_270[i] = &g_153;
    l_280[0][4] &= (((*l_279) = (((*l_278) = ((g_3 = (safe_mul_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u((!(((*l_274) = ((p_41 | (((((*l_263) |= g_111[0][0][1].f0) , ((void*)0 == l_263)) < (safe_sub_func_float_f_f((safe_div_func_float_f_f((l_269 != (l_271 = &l_270[0])), ((p_41 < p_43) < ((safe_mul_func_float_f_f(((g_3 , 0x0.Ep+1) <= g_118), g_88[4][4])) < g_121.f0)))), g_116))) , 0x118B436DL)) & p_44)) ^ g_121.f4)), 0)), l_275))) || g_121.f3)) == p_41)) <= g_180);
    for (g_73 = (-26); (g_73 != 29); g_73++)
    { /* block id: 134 */
        int32_t l_293[10][4][6] = {{{(-1L),1L,0xF35EB71DL,0xBB5ED8E8L,2L,5L},{0xB79A9950L,0x3D8EA6DCL,0x84F15128L,(-10L),0xD730CA24L,0xC8E73514L},{0xD632D0E1L,0x43A8D199L,(-1L),0x5E3DE920L,8L,(-9L)},{0x6A061BA8L,0L,1L,0xA2FA2E92L,(-1L),(-1L)}},{{0x3D8EA6DCL,1L,(-5L),0x9ACBCEC4L,(-3L),0x71CE6CB9L},{(-10L),(-1L),1L,0x88637AB9L,0x98BD8E4BL,0x0B92CAF5L},{0xF35EB71DL,(-1L),0xA2FA2E92L,0x049867E8L,0x71CE6CB9L,0xBB5ED8E8L},{0x5E3DE920L,(-10L),0x98BD8E4BL,(-3L),0xA2FA2E92L,(-3L)}},{{8L,0x71CE6CB9L,8L,1L,0x0B92CAF5L,0xA2FA2E92L},{(-1L),2L,(-1L),(-1L),0L,0x59E75CB0L},{0xD730CA24L,(-6L),0xC8E73514L,(-1L),0x6A061BA8L,1L},{(-1L),0x326CECA4L,0x91536337L,1L,(-1L),0x9A6C37CBL}},{{8L,0x0D48E6E6L,(-4L),(-3L),0x3D8EA6DCL,(-1L)},{0x5E3DE920L,6L,0x88637AB9L,0x049867E8L,0x91536337L,0x98BD8E4BL},{0xF35EB71DL,0xA2FA2E92L,(-1L),0x88637AB9L,0x3DF97D46L,(-6L)},{(-10L),0x049867E8L,(-1L),0x9ACBCEC4L,0x0D48E6E6L,0xE4AA0FABL}},{{0x3D8EA6DCL,1L,0x0D48E6E6L,0xA2FA2E92L,0x6FA1C930L,(-1L)},{0x6A061BA8L,0L,1L,0x5E3DE920L,0x5E3DE920L,1L},{0xD632D0E1L,0xD632D0E1L,1L,(-10L),(-9L),0xD75A21BDL},{0xB79A9950L,0x9ACBCEC4L,0x6A061BA8L,0xBB5ED8E8L,0x049867E8L,1L}},{{(-1L),0xB79A9950L,0x6A061BA8L,8L,0xD632D0E1L,0xD75A21BDL},{0x91536337L,8L,1L,1L,0xD2D7FA05L,1L},{1L,0xD2D7FA05L,1L,(-1L),0x88637AB9L,(-1L)},{1L,0x59E75CB0L,0x0D48E6E6L,(-6L),0x90C81069L,0xE4AA0FABL}},{{0x90C81069L,0xC8E73514L,(-1L),(-9L),0xE4AA0FABL,(-6L)},{0x2D8EEC57L,0x0B92CAF5L,(-1L),0xF35EB71DL,5L,0x98BD8E4BL},{(-1L),0x91536337L,0x88637AB9L,0x2D8EEC57L,1L,(-1L)},{(-5L),0x9A6C37CBL,(-4L),1L,(-4L),0x9A6C37CBL}},{{0L,0xD75A21BDL,0x91536337L,0xD730CA24L,0xBB5ED8E8L,1L},{0x7B3F76F2L,(-1L),0xC8E73514L,0x326CECA4L,0x9ACBCEC4L,0x59E75CB0L},{(-4L),(-1L),(-1L),0x90C81069L,0xBB5ED8E8L,0xA2FA2E92L},{0x98BD8E4BL,0xD75A21BDL,8L,0x0D48E6E6L,(-1L),1L}},{{(-9L),0L,0x0D48E6E6L,(-1L),(-10L),(-6L)},{0x3DF97D46L,0x3D8EA6DCL,0x98BD8E4BL,0x7B3F76F2L,0x88637AB9L,0L},{0x90C81069L,0L,0x9A6C37CBL,1L,(-1L),1L},{0x5E3DE920L,0xA2FA2E92L,6L,(-4L),0xD75A21BDL,(-9L)}},{{(-1L),0x7B3F76F2L,0xD730CA24L,0x43A8D199L,0x848BFD57L,0x90C81069L},{0xA2FA2E92L,0xC8E73514L,0x6FA1C930L,0x6FA1C930L,0xC8E73514L,0xA2FA2E92L},{(-1L),0x84F15128L,0x0B92CAF5L,0x3DF97D46L,0x326CECA4L,0x88637AB9L},{0x2D8EEC57L,1L,0x6A061BA8L,0L,2L,(-1L)}}};
        int32_t l_346 = 8L;
        int32_t l_351[9][4] = {{0xF0EC19EDL,0xF0EC19EDL,0x81E49874L,0xF0EC19EDL},{0xF0EC19EDL,0L,0L,0xF0EC19EDL},{0L,0xF0EC19EDL,0L,0L},{0xF0EC19EDL,0xF0EC19EDL,0x81E49874L,0xF0EC19EDL},{0xF0EC19EDL,0L,0L,0xF0EC19EDL},{0L,0xF0EC19EDL,0L,0L},{0xF0EC19EDL,0xF0EC19EDL,0x81E49874L,0xF0EC19EDL},{0xF0EC19EDL,0L,0L,0xF0EC19EDL},{0L,0xF0EC19EDL,0L,0L}};
        int32_t l_359[7][1][8] = {{{0x1549E2B6L,3L,5L,9L,0xA502A4A8L,0xA502A4A8L,9L,5L}},{{(-4L),(-4L),3L,0x0388B12EL,0xA502A4A8L,0L,(-1L),0L}},{{0x1549E2B6L,5L,0x0388B12EL,5L,0x1549E2B6L,5L,(-4L),0L}},{{5L,0xA502A4A8L,(-1L),0x0388B12EL,0x0388B12EL,(-1L),0xA502A4A8L,5L}},{{3L,5L,(-1L),9L,(-4L),0x1549E2B6L,(-4L),9L}},{{0x0388B12EL,0x670E5205L,0x0388B12EL,0L,9L,0x1549E2B6L,(-1L),(-1L)}},{{(-1L),5L,3L,3L,5L,(-1L),9L,(-4L)}}};
        const int8_t *l_374 = &g_180;
        const int8_t **l_373 = &l_374;
        int i, j, k;
    }
    for (g_264 = (-18); (g_264 > 48); ++g_264)
    { /* block id: 205 */
        uint8_t l_459 = 0UL;
        int16_t *l_476 = &l_458[1];
        int32_t l_499 = 3L;
        int32_t l_500 = (-8L);
        uint16_t *l_501 = &g_317[8][2];
        const int8_t *l_526[8] = {&g_180,&g_180,&g_180,&g_180,&g_180,&g_180,&g_180,&g_180};
        const int8_t ** const l_525 = &l_526[2];
        int32_t l_533 = 0x3666D1E0L;
        int64_t l_534 = 0x2A45D010D599FB76LL;
        int32_t l_535 = (-1L);
        int32_t l_536 = 0x0414D9A9L;
        int32_t l_537 = 0L;
        int32_t l_538 = 0x2373CDF0L;
        int32_t l_540 = 0xF3BFA72CL;
        int32_t l_542 = 0xA648954EL;
        int32_t l_543 = 1L;
        int32_t l_545 = 3L;
        int32_t l_546 = 1L;
        uint32_t l_563 = 0UL;
        int16_t l_570 = 0xDCEDL;
        uint32_t l_592 = 0x6D043A29L;
        int i;
    }
    return (*g_4);
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_62 g_24 g_73 g_77 g_5 g_96 g_102 g_102.f1 g_111 g_111.f2 g_111.f3 g_88 g_121 g_111.f0 g_4 g_154 g_157 g_123 g_116 g_153 g_180 g_181 g_191 g_201 g_204 g_111.f1 g_202
 * writes: g_62 g_73 g_77 g_24 g_96 g_4 g_102.f1 g_116 g_118 g_123 g_153 g_158 g_180 g_201
 */
static int16_t  func_48(int32_t * p_49, int32_t  p_50)
{ /* block id: 11 */
    int64_t l_56[1];
    int32_t l_57 = 0xA3B7DC4BL;
    int32_t ***l_58 = (void*)0;
    int32_t ****l_59 = &l_58;
    uint16_t *l_61 = &g_62;
    uint8_t *l_72 = &g_73;
    uint8_t *l_76 = &g_77;
    int32_t l_86[10] = {0xF81F9D3FL,0xF81F9D3FL,(-7L),0x324B7C80L,(-7L),0xF81F9D3FL,0xF81F9D3FL,(-7L),0x324B7C80L,(-7L)};
    uint64_t *l_87[8][6][5] = {{{&g_88[1][3],&g_88[1][3],(void*)0,(void*)0,(void*)0},{&g_88[2][1],(void*)0,&g_88[1][3],&g_88[4][4],&g_88[1][3]},{&g_88[1][3],&g_88[1][3],(void*)0,(void*)0,(void*)0},{&g_88[2][1],(void*)0,&g_88[1][3],&g_88[4][4],&g_88[1][3]},{&g_88[1][3],&g_88[1][3],(void*)0,(void*)0,(void*)0},{&g_88[2][1],(void*)0,&g_88[1][3],&g_88[4][4],&g_88[1][3]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]}},{{(void*)0,(void*)0,&g_88[4][4],&g_88[1][3],&g_88[4][4]},{&g_88[1][3],&g_88[1][2],&g_88[3][0],(void*)0,&g_88[3][0]},{(void*)0,&g_88[4][4],(void*)0,&g_88[1][3],(void*)0},{&g_88[3][0],&g_88[1][2],&g_88[2][0],(void*)0,&g_88[2][0]},{&g_88[4][4],&g_88[4][4],(void*)0,&g_88[1][3],(void*)0},{&g_88[3][0],&g_88[1][2],&g_88[2][0],(void*)0,&g_88[2][0]}}};
    int32_t l_92 = 0x94FBAEB3L;
    int32_t l_93 = 7L;
    int32_t l_94 = (-1L);
    int32_t l_95 = 0x78A3DA4FL;
    int32_t *l_99 = &g_5;
    int32_t l_135 = 0xEEDD9E1AL;
    int32_t l_142 = 0x6036A631L;
    int32_t l_143 = 0x59ABB1F7L;
    int32_t l_147[7][9] = {{0x486CFEE3L,0xB26268CBL,0L,0x486CFEE3L,0x11E18098L,0L,0L,0x11E18098L,0x486CFEE3L},{0x77C8831FL,(-7L),0x77C8831FL,1L,(-7L),1L,0x77C8831FL,(-7L),0x77C8831FL},{0x486CFEE3L,0x11E18098L,0L,0L,0x11E18098L,0x486CFEE3L,0L,0xB26268CBL,0x486CFEE3L},{(-10L),(-7L),(-10L),1L,0xAD9C61CCL,1L,(-10L),(-7L),(-10L)},{0x486CFEE3L,0xB26268CBL,0L,0x486CFEE3L,0x11E18098L,0L,0L,0x11E18098L,0x486CFEE3L},{0x77C8831FL,(-7L),0x77C8831FL,1L,(-7L),1L,0x77C8831FL,(-7L),0x77C8831FL},{0x486CFEE3L,0x11E18098L,0L,0L,0x11E18098L,0x486CFEE3L,0L,0xB26268CBL,0x486CFEE3L}};
    int8_t l_199 = (-4L);
    const uint32_t l_246 = 0UL;
    int32_t *l_247 = (void*)0;
    int32_t *l_248 = &l_147[0][6];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_56[i] = 0x4EB856A759F32F52LL;
    for (p_50 = 17; (p_50 < 4); p_50 = safe_sub_func_uint32_t_u_u(p_50, 5))
    { /* block id: 14 */
        int32_t *l_55 = &g_24;
        int32_t **l_54 = &l_55;
        (*l_54) = &g_5;
        l_57 = l_56[0];
    }
lbl_90:
    (*l_59) = l_58;
    if (((safe_unary_minus_func_uint16_t_u(((*l_61) &= g_3))) < (l_57 = ((safe_lshift_func_uint8_t_u_u((((g_24 == (safe_div_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((+(((safe_lshift_func_uint8_t_u_s(0xEAL, 5)) >= (((*l_72)--) | ((p_50 | ((*l_76)--)) || ((p_50 && ((safe_rshift_func_int16_t_s_s((1UL == ((((void*)0 != &g_3) & (safe_sub_func_uint8_t_u_u((0xBEC8B24CEC8E4446LL != p_50), g_5))) > g_77)), 4)) , p_50)) >= 1UL)))) , p_50)) < l_86[7]), g_5)), 0xD66AL))) <= 0xB9L) , p_50), 6)) ^ g_24))))
    { /* block id: 23 */
        int32_t *l_89[1];
        int i;
        for (i = 0; i < 1; i++)
            l_89[i] = &g_24;
        g_24 |= (l_57 &= 0x5D312257L);
    }
    else
    { /* block id: 26 */
        int32_t *l_91[3][5] = {{&g_24,&g_24,(void*)0,&g_5,&g_5},{&g_24,&g_24,&g_24,(void*)0,&g_5},{&l_57,&g_24,&g_5,&g_24,&l_57}};
        uint8_t * const l_159 = &g_73;
        int16_t l_217[6][8][5] = {{{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L}},{{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L}},{{0x0204L,0L,0L,0x0204L,0x1BB5L},{0x0E83L,0x4F5EL,0x4F5EL,0x0E83L,9L},{0x0204L,0L,0L,0x0204L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)}},{{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)}},{{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)}},{{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)},{0L,0x0204L,0x0204L,0L,(-1L)},{0x61BBL,0x0E83L,0x0E83L,0x61BBL,(-2L)}}};
        int i, j, k;
        if (g_77)
            goto lbl_90;
lbl_187:
        for (g_62 = 0; (g_62 <= 4); g_62 += 1)
        { /* block id: 30 */
            return g_24;
        }
        ++g_96;
        for (g_77 = 0; (g_77 <= 4); g_77 += 1)
        { /* block id: 36 */
            int8_t *l_109 = &g_102.f1;
            uint16_t *l_114 = &g_3;
            int16_t *l_115 = &g_116;
            float *l_117 = &g_118;
            int32_t l_125 = (-6L);
            int32_t l_128 = (-4L);
            int32_t l_132 = 0x2DBFB6B4L;
            int32_t l_134[8];
            uint32_t l_155 = 0x14682580L;
            int32_t l_192 = 1L;
            int32_t l_223 = (-1L);
            int i;
            for (i = 0; i < 8; i++)
                l_134[i] = 0x1C95C5D0L;
            g_4 = l_99;
            (*l_117) = ((safe_mul_func_uint16_t_u_u(l_86[(g_77 + 1)], (((*l_72) = p_50) ^ (((g_102 , ((safe_sub_func_uint8_t_u_u((0x3631664A1005E543LL == (((safe_mul_func_uint16_t_u_u(((((((((*l_99) && ((safe_add_func_int8_t_s_s(((*l_109) ^= 0xBDL), (~(g_111[0][0][1] , ((*l_115) = (safe_add_func_uint64_t_u_u(((void*)0 != l_114), 0UL))))))) && g_111[0][0][1].f2)) , 0xB7L) , g_24) , p_50) , &g_77) == &g_77) && p_50), l_86[(g_77 + 1)])) | g_111[0][0][1].f3) , p_50)), (-1L))) ^ g_77)) > g_24) | p_50)))) , 0x1.C05FA5p+68);
            for (g_102.f1 = 3; (g_102.f1 >= 0); g_102.f1 -= 1)
            { /* block id: 44 */
                int32_t **l_122 = &l_91[2][3];
                int32_t l_129 = 0xDE98D404L;
                int32_t l_136 = (-1L);
                int32_t l_138 = 0x1A0BB31FL;
                int32_t l_139 = 1L;
                int32_t l_141 = 0x071E4365L;
                int32_t l_145 = 0xBA6E04B3L;
                int32_t l_146 = 0x549B09EDL;
                int32_t l_148 = 0x2C73AE87L;
                int64_t l_200 = 7L;
                for (l_94 = 0; (l_94 <= 4); l_94 += 1)
                { /* block id: 47 */
                    int8_t l_124 = (-4L);
                    int32_t l_127 = 0x4284883CL;
                    int32_t l_130 = 0xFD9A5CC0L;
                    int32_t l_137 = 0xE7769F70L;
                    int32_t l_140 = (-4L);
                    int32_t l_144 = 0x9A86176EL;
                    uint16_t l_149 = 0x7A17L;
                    for (g_24 = 0; (g_24 <= 4); g_24 += 1)
                    { /* block id: 50 */
                        int32_t l_126 = 0x8EA379FFL;
                        int32_t l_131[10] = {0xA7C04A76L,0L,0L,0xA7C04A76L,(-10L),0xA7C04A76L,0L,0L,0xA7C04A76L,(-10L)};
                        int64_t l_133 = 1L;
                        int i, j, k;
                        g_123[1] = (safe_add_func_uint16_t_u_u(g_88[g_77][l_94], (g_121 , (g_111[0][0][1].f0 || (&g_4 == l_122)))));
                        if ((*g_4))
                            continue;
                        l_149--;
                    }
                    (*g_154) = ((*l_122) = (void*)0);
                    l_155 = p_50;
                    for (g_24 = 0; (g_24 <= 9); g_24 += 1)
                    { /* block id: 60 */
                        int64_t l_156 = 0xBFA45521639E2500LL;
                        uint16_t l_164 = 0xB7D1L;
                        int32_t l_165 = (-8L);
                        int i, j, k;
                        if (l_156)
                            break;
                        (*g_157) = &g_111[0][0][2];
                        l_140 ^= l_155;
                        l_165 ^= (((l_140 ^ 0xE276FF74A9DA0880LL) & g_123[1]) & (l_134[0] = (l_159 == ((safe_sub_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u((l_164 , g_121.f2), l_124)) >= ((void*)0 == &l_122)), g_116)) , &g_73))));
                    }
                }
                if ((*g_4))
                    break;
                for (l_143 = 0; (l_143 <= 9); l_143 += 1)
                { /* block id: 71 */
                    int i;
                    if ((l_86[(g_102.f1 + 4)] != p_50))
                    { /* block id: 72 */
                        const uint8_t l_166 = 3UL;
                        if (l_166)
                            break;
                    }
                    else
                    { /* block id: 74 */
                        p_49 = (*g_154);
                    }
                    l_93 = p_50;
                    for (l_142 = 9; (l_142 >= 0); l_142 -= 1)
                    { /* block id: 80 */
                        int32_t l_167 = 0L;
                        l_167 &= (l_128 == (-8L));
                    }
                }
                for (l_143 = 0; (l_143 <= 9); l_143 += 1)
                { /* block id: 86 */
                    int32_t l_168 = (-1L);
                    const int32_t *l_174 = &l_142;
                    const int32_t **l_173 = &l_174;
                    int32_t l_183 = 0xCA9B3D71L;
                    if (l_168)
                    { /* block id: 87 */
                        uint32_t l_175[10] = {1UL,1UL,4294967286UL,1UL,1UL,4294967286UL,1UL,1UL,4294967286UL,1UL};
                        int8_t *l_178 = (void*)0;
                        int8_t *l_179 = &g_180;
                        uint8_t l_182 = 0xA7L;
                        int i;
                        l_148 = p_50;
                        l_182 = (safe_sub_func_int8_t_s_s(1L, (((safe_sub_func_int8_t_s_s((l_173 != (l_175[4] , (void*)0)), (l_175[4] , (((((*l_61) = (**l_173)) != l_175[4]) | ((g_116 <= ((safe_mod_func_int8_t_s_s(((((*l_179) ^= (((p_50 , 0UL) == 18446744073709551615UL) >= 0xF4L)) , g_123[1]) < 0x36FC477129895135LL), p_50)) < g_181)) && g_121.f4)) <= g_88[2][3])))) && 7UL) == p_50)));
                    }
                    else
                    { /* block id: 92 */
                        uint32_t l_184[2][2] = {{4294967290UL,4294967290UL},{4294967290UL,4294967290UL}};
                        int i, j;
                        --l_184[1][0];
                        if (l_57)
                            goto lbl_187;
                        l_183 = (-9L);
                    }
                    l_200 = ((**l_173) & (~(((((((*l_115) ^= 0xE57FL) >= (safe_mul_func_int16_t_s_s(((g_191 , ((l_192 <= (g_180 <= 0x51F979E623694114LL)) ^ g_123[1])) != (((safe_mul_func_uint8_t_u_u(p_50, ((((safe_sub_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_u((l_76 != (void*)0), 3)) ^ l_134[4]), 0xBDFDB656643B3FE9LL)) , p_50) && 0xFEBEL) == 0xD0L))) | g_123[0]) && p_50)), 0UL))) == p_50) != p_50) && l_199) != 0x20B6152EL)));
                }
            }
            for (l_135 = 3; (l_135 <= 9); l_135 += 1)
            { /* block id: 103 */
                int8_t * volatile **l_203 = &g_201;
                int32_t l_205 = 0xC5206A44L;
                int32_t l_208 = 0x61717C3DL;
                int32_t l_218 = 0L;
                int8_t l_225 = 0x3EL;
                int32_t l_226[8] = {0x5599F8BFL,0x5599F8BFL,0x5599F8BFL,0x5599F8BFL,0x5599F8BFL,0x5599F8BFL,0x5599F8BFL,0x5599F8BFL};
                uint64_t l_228[4] = {0xFF99BD179C09EB90LL,0xFF99BD179C09EB90LL,0xFF99BD179C09EB90LL,0xFF99BD179C09EB90LL};
                int i;
                (*l_203) = g_201;
                for (g_116 = 0; (g_116 <= 4); g_116 += 1)
                { /* block id: 107 */
                    int64_t l_206 = 0x29711FA603679C85LL;
                    int32_t l_213 = 0x6BEC8285L;
                    int32_t l_214 = 0x7D6F02A8L;
                    int32_t l_216 = 1L;
                    int32_t l_222 = 0xD04AF1C7L;
                    int32_t l_224 = 0x50E9C848L;
                    int32_t l_227[1][8][7] = {{{(-5L),0x493E9099L,(-1L),0x8B7EBC46L,(-1L),6L,(-7L)},{1L,(-7L),4L,4L,(-7L),(-1L),1L},{(-1L),0L,0xA5DC35F6L,0xA1FDA284L,(-9L),(-1L),(-7L)},{0L,0x78CAF96EL,(-7L),0x1C91F2F8L,(-5L),0xA0BE3B21L,(-9L)},{(-5L),0L,0x2E92B18FL,6L,6L,0x2E92B18FL,0L},{8L,(-7L),0x2E92B18FL,0xA0BE3B21L,0x956FEEA5L,6L,0xA5DC35F6L},{(-1L),0xA1FDA284L,(-7L),(-8L),0x8B7EBC46L,0x493E9099L,0x1C91F2F8L},{0x2E92B18FL,(-8L),0xA5DC35F6L,0xA0BE3B21L,(-7L),(-7L),0xA0BE3B21L}}};
                    int i, j, k;
                    (*g_204) = p_49;
                    for (g_62 = 0; (g_62 <= 9); g_62 += 1)
                    { /* block id: 111 */
                        int32_t l_207 = 0x13447BCEL;
                        int32_t l_209 = 0xC29D257CL;
                        uint8_t l_210 = 0xCAL;
                        int32_t l_215 = 0xF2E56CC4L;
                        int32_t l_219 = 1L;
                        int32_t l_220 = 0x1F4020B1L;
                        int32_t l_221[5][8] = {{0L,(-9L),0x0C3B2E69L,0x0C3B2E69L,(-9L),0L,(-10L),(-10L)},{0L,(-9L),0x0C3B2E69L,0x0C3B2E69L,(-9L),0L,(-10L),(-10L)},{0L,(-9L),0x0C3B2E69L,0x0C3B2E69L,(-9L),0L,(-10L),(-10L)},{0L,(-9L),0x0C3B2E69L,0x0C3B2E69L,(-9L),0L,(-10L),(-10L)},{0L,(-9L),0x0C3B2E69L,0x0C3B2E69L,(-9L),0L,(-10L),(-10L)}};
                        int i, j;
                        ++l_210;
                        if (p_50)
                            break;
                        l_228[2]++;
                    }
                }
            }
        }
    }
    (*l_248) |= (safe_mod_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_u(g_111[0][0][1].f1, (((**g_201) = (safe_mul_func_uint16_t_u_u((((65535UL > (safe_lshift_func_int16_t_s_u((safe_div_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_u(((g_191 , l_87[1][2][2]) == l_87[2][3][4]), 8)) , (p_50 ^ 0x30DF48D2L)), (*l_99))) != (safe_unary_minus_func_int16_t_s(p_50))), (-1L))), 6))) , g_111[0][0][1].f3) ^ l_246), 0x2478L))) == 0L))), 0xECACBAFCE22FE3B4LL));
    return g_180;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc_bytes (&g_2, sizeof(g_2), "g_2", print_hash_value);
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_24, "g_24", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_77, "g_77", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_88[i][j], "g_88[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_96, "g_96", print_hash_value);
    transparent_crc(g_102.f1, "g_102.f1", print_hash_value);
    transparent_crc(g_102.f2, "g_102.f2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_111[i][j][k].f0, "g_111[i][j][k].f0", print_hash_value);
                transparent_crc(g_111[i][j][k].f1, "g_111[i][j][k].f1", print_hash_value);
                transparent_crc(g_111[i][j][k].f2, "g_111[i][j][k].f2", print_hash_value);
                transparent_crc(g_111[i][j][k].f3, "g_111[i][j][k].f3", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc_bytes (&g_118, sizeof(g_118), "g_118", print_hash_value);
    transparent_crc(g_121.f0, "g_121.f0", print_hash_value);
    transparent_crc_bytes (&g_121.f1, sizeof(g_121.f1), "g_121.f1", print_hash_value);
    transparent_crc(g_121.f2, "g_121.f2", print_hash_value);
    transparent_crc(g_121.f3, "g_121.f3", print_hash_value);
    transparent_crc(g_121.f4, "g_121.f4", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_123[i], "g_123[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_180, "g_180", print_hash_value);
    transparent_crc(g_181, "g_181", print_hash_value);
    transparent_crc(g_191.f0, "g_191.f0", print_hash_value);
    transparent_crc(g_250, "g_250", print_hash_value);
    transparent_crc(g_264, "g_264", print_hash_value);
    transparent_crc(g_305.f0, "g_305.f0", print_hash_value);
    transparent_crc(g_305.f1, "g_305.f1", print_hash_value);
    transparent_crc(g_305.f2, "g_305.f2", print_hash_value);
    transparent_crc(g_305.f3, "g_305.f3", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_317[i][j], "g_317[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_379.f0, "g_379.f0", print_hash_value);
    transparent_crc_bytes (&g_379.f1, sizeof(g_379.f1), "g_379.f1", print_hash_value);
    transparent_crc(g_379.f2, "g_379.f2", print_hash_value);
    transparent_crc(g_379.f3, "g_379.f3", print_hash_value);
    transparent_crc(g_379.f4, "g_379.f4", print_hash_value);
    transparent_crc(g_435.f0, "g_435.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_447[i].f0, "g_447[i].f0", print_hash_value);
        transparent_crc(g_447[i].f1, "g_447[i].f1", print_hash_value);
        transparent_crc(g_447[i].f2, "g_447[i].f2", print_hash_value);
        transparent_crc(g_447[i].f3, "g_447[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_482, "g_482", print_hash_value);
    transparent_crc(g_596.f0, "g_596.f0", print_hash_value);
    transparent_crc(g_605.f0, "g_605.f0", print_hash_value);
    transparent_crc(g_695.f0, "g_695.f0", print_hash_value);
    transparent_crc(g_700.f0, "g_700.f0", print_hash_value);
    transparent_crc_bytes (&g_700.f1, sizeof(g_700.f1), "g_700.f1", print_hash_value);
    transparent_crc(g_700.f2, "g_700.f2", print_hash_value);
    transparent_crc(g_700.f3, "g_700.f3", print_hash_value);
    transparent_crc(g_700.f4, "g_700.f4", print_hash_value);
    transparent_crc(g_702, "g_702", print_hash_value);
    transparent_crc(g_706.f0, "g_706.f0", print_hash_value);
    transparent_crc(g_717, "g_717", print_hash_value);
    transparent_crc(g_768, "g_768", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_987[i][j], "g_987[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1110, "g_1110", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1113[i].f0, "g_1113[i].f0", print_hash_value);
        transparent_crc(g_1113[i].f1, "g_1113[i].f1", print_hash_value);
        transparent_crc(g_1113[i].f2, "g_1113[i].f2", print_hash_value);
        transparent_crc(g_1113[i].f3, "g_1113[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1124.f0, "g_1124.f0", print_hash_value);
    transparent_crc_bytes (&g_1124.f1, sizeof(g_1124.f1), "g_1124.f1", print_hash_value);
    transparent_crc(g_1124.f2, "g_1124.f2", print_hash_value);
    transparent_crc(g_1124.f3, "g_1124.f3", print_hash_value);
    transparent_crc(g_1124.f4, "g_1124.f4", print_hash_value);
    transparent_crc(g_1130.f0, "g_1130.f0", print_hash_value);
    transparent_crc(g_1157, "g_1157", print_hash_value);
    transparent_crc(g_1165, "g_1165", print_hash_value);
    transparent_crc(g_1197.f0, "g_1197.f0", print_hash_value);
    transparent_crc(g_1248.f0, "g_1248.f0", print_hash_value);
    transparent_crc(g_1255, "g_1255", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1256[i], "g_1256[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1290, "g_1290", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1303[i], "g_1303[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1323.f0, "g_1323.f0", print_hash_value);
    transparent_crc(g_1323.f1, "g_1323.f1", print_hash_value);
    transparent_crc(g_1323.f2, "g_1323.f2", print_hash_value);
    transparent_crc(g_1323.f3, "g_1323.f3", print_hash_value);
    transparent_crc(g_1391.f0, "g_1391.f0", print_hash_value);
    transparent_crc(g_1504.f0, "g_1504.f0", print_hash_value);
    transparent_crc(g_1505.f0, "g_1505.f0", print_hash_value);
    transparent_crc(g_1542, "g_1542", print_hash_value);
    transparent_crc(g_1610, "g_1610", print_hash_value);
    transparent_crc(g_1623, "g_1623", print_hash_value);
    transparent_crc(g_1634, "g_1634", print_hash_value);
    transparent_crc(g_1679, "g_1679", print_hash_value);
    transparent_crc(g_1697, "g_1697", print_hash_value);
    transparent_crc(g_1742.f0, "g_1742.f0", print_hash_value);
    transparent_crc_bytes (&g_1742.f1, sizeof(g_1742.f1), "g_1742.f1", print_hash_value);
    transparent_crc(g_1742.f2, "g_1742.f2", print_hash_value);
    transparent_crc(g_1742.f3, "g_1742.f3", print_hash_value);
    transparent_crc(g_1742.f4, "g_1742.f4", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1759[i].f0, "g_1759[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1762.f0, "g_1762.f0", print_hash_value);
    transparent_crc(g_1764.f0, "g_1764.f0", print_hash_value);
    transparent_crc(g_1764.f1, "g_1764.f1", print_hash_value);
    transparent_crc(g_1764.f2, "g_1764.f2", print_hash_value);
    transparent_crc(g_1764.f3, "g_1764.f3", print_hash_value);
    transparent_crc(g_1778, "g_1778", print_hash_value);
    transparent_crc(g_1811.f0, "g_1811.f0", print_hash_value);
    transparent_crc_bytes (&g_1811.f1, sizeof(g_1811.f1), "g_1811.f1", print_hash_value);
    transparent_crc(g_1811.f2, "g_1811.f2", print_hash_value);
    transparent_crc(g_1811.f3, "g_1811.f3", print_hash_value);
    transparent_crc(g_1811.f4, "g_1811.f4", print_hash_value);
    transparent_crc(g_1873, "g_1873", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1885[i][j][k].f0, "g_1885[i][j][k].f0", print_hash_value);
                transparent_crc_bytes(&g_1885[i][j][k].f1, sizeof(g_1885[i][j][k].f1), "g_1885[i][j][k].f1", print_hash_value);
                transparent_crc(g_1885[i][j][k].f2, "g_1885[i][j][k].f2", print_hash_value);
                transparent_crc(g_1885[i][j][k].f3, "g_1885[i][j][k].f3", print_hash_value);
                transparent_crc(g_1885[i][j][k].f4, "g_1885[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1925[i].f0, "g_1925[i].f0", print_hash_value);
        transparent_crc_bytes(&g_1925[i].f1, sizeof(g_1925[i].f1), "g_1925[i].f1", print_hash_value);
        transparent_crc(g_1925[i].f2, "g_1925[i].f2", print_hash_value);
        transparent_crc(g_1925[i].f3, "g_1925[i].f3", print_hash_value);
        transparent_crc(g_1925[i].f4, "g_1925[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1984, "g_1984", print_hash_value);
    transparent_crc(g_2032, "g_2032", print_hash_value);
    transparent_crc(g_2060.f0, "g_2060.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 561
   depth: 1, occurrence: 7
XXX total union variables: 18

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 15
breakdown:
   indirect level: 0, occurrence: 7
   indirect level: 1, occurrence: 1
   indirect level: 2, occurrence: 5
   indirect level: 3, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 3
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 8
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 16

XXX max expression depth: 32
breakdown:
   depth: 1, occurrence: 112
   depth: 2, occurrence: 42
   depth: 3, occurrence: 1
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
   depth: 12, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 24, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 3
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1

XXX total number of pointers: 381

XXX times a variable address is taken: 743
XXX times a pointer is dereferenced on RHS: 183
breakdown:
   depth: 1, occurrence: 124
   depth: 2, occurrence: 48
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
   depth: 5, occurrence: 4
XXX times a pointer is dereferenced on LHS: 228
breakdown:
   depth: 1, occurrence: 208
   depth: 2, occurrence: 15
   depth: 3, occurrence: 5
XXX times a pointer is compared with null: 38
XXX times a pointer is compared with address of another variable: 7
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 6263

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 890
   level: 2, occurrence: 176
   level: 3, occurrence: 84
   level: 4, occurrence: 32
   level: 5, occurrence: 17
XXX number of pointers point to pointers: 159
XXX number of pointers point to scalars: 205
XXX number of pointers point to structs: 5
XXX percent of pointers has null in alias set: 28.9
XXX average alias set size: 1.32

XXX times a non-volatile is read: 1526
XXX times a non-volatile is write: 729
XXX times a volatile is read: 142
XXX    times read thru a pointer: 21
XXX times a volatile is write: 45
XXX    times written thru a pointer: 13
XXX times a volatile is available for access: 5.15e+03
XXX percentage of non-volatile access: 92.3

XXX forward jumps: 2
XXX backward jumps: 10

XXX stmts: 118
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 22
   depth: 2, occurrence: 11
   depth: 3, occurrence: 16
   depth: 4, occurrence: 16
   depth: 5, occurrence: 23

XXX percentage a fresh-made variable is used: 19.2
XXX percentage an existing variable is used: 80.8
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


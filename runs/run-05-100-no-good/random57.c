/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      406888106
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_5 = 0xA59C1763L;
static uint8_t g_21[6] = {0x58L,0x58L,0x58L,0x58L,0x58L,0x58L};
static int16_t g_33 = 9L;
static float g_64 = 0xE.496089p+23;
static uint32_t g_65 = 0x7CBC1764L;
static float *g_70[5] = {&g_64,&g_64,&g_64,&g_64,&g_64};
static float **g_69 = &g_70[4];
static int32_t g_74 = 0x634C778CL;
static volatile uint64_t g_75 = 0x952EB5F757037A9ELL;/* VOLATILE GLOBAL g_75 */
static float g_84[6][7][6] = {{{0x2.E3E2A5p-62,0xD.4C7704p-94,0xA.62DAC3p-25,0xF.E21AC5p-93,0x7.E06730p-82,0x4.7p-1},{0xC.AF600Dp-35,0x0.66401Ep-32,0xB.690C29p-53,0x2.F2280Fp+12,(-0x1.7p-1),0x1.2p+1},{0xA.36831Ap-35,0x0.66401Ep-32,0x8.3p+1,(-0x4.4p-1),0x7.E06730p-82,0x2.E3E2A5p-62},{0x2.F62330p-62,0xD.4C7704p-94,0x2.F2280Fp+12,0xF.870AB7p-66,0xE.90203Cp+58,(-0x1.9p-1)},{0xB.690C29p-53,0xF.C7D00Cp-73,0xA.36831Ap-35,0x1.B451AFp+37,0x3.F5D6BEp+40,0xF.870AB7p-66},{0x0.66401Ep-32,0xE.90203Cp+58,0xC.0ABB7Bp+80,0x1.9p-1,0x1.9p-1,0x0.11814Ap+70},{0x0.11814Ap+70,0x0.11814Ap+70,0xF.E21AC5p-93,0x1.2p+1,0x2.F62330p-62,0x1.9p-1}},{{0xE.90203Cp+58,0x0.66401Ep-32,0x1.0p-1,0x1.F4A390p+84,0xC.AF600Dp-35,0xF.E21AC5p-93},{0x0.2p+1,0xE.90203Cp+58,0x1.0p-1,0xA.36831Ap-35,0x0.11814Ap+70,0x1.9p-1},{0xF.870AB7p-66,0xA.36831Ap-35,0xF.E21AC5p-93,(-0x1.Ap+1),0x1.B451AFp+37,0x0.11814Ap+70},{(-0x1.Ap+1),0x1.B451AFp+37,0x0.11814Ap+70,(-0x1.7p-1),0x1.3p-1,(-0x1.9p-1)},{0x7.E06730p-82,0xF.E21AC5p-93,0xA.62DAC3p-25,0xD.4C7704p-94,0x2.E3E2A5p-62,0xD.4C7704p-94},{0x2.E3E2A5p-62,0xF.C7D00Cp-73,0x2.E3E2A5p-62,(-0x1.Ap-1),0xF.870AB7p-66,0x8.48A285p-13},{0x1.F4A390p+84,0x4.7p-1,0xC.AF600Dp-35,0xA.62DAC3p-25,0x8.48A285p-13,0x1.3p-1}},{{0x0.66401Ep-32,0x1.2p+1,0xA.36831Ap-35,0xA.62DAC3p-25,0x0.2p+1,(-0x1.Ap-1)},{0x1.F4A390p+84,0x2.E3E2A5p-62,0x2.F62330p-62,(-0x1.Ap-1),0xA.36831Ap-35,(-0x9.1p+1)},{0x2.E3E2A5p-62,(-0x1.9p-1),0xB.690C29p-53,0xD.4C7704p-94,0x3.5237E9p-98,(-0x10.0p+1)},{0x7.E06730p-82,0xF.870AB7p-66,0x0.66401Ep-32,(-0x1.7p-1),0xE.90203Cp+58,0xE.90203Cp+58},{(-0x1.Ap+1),0xC.0ABB7Bp+80,0xC.0ABB7Bp+80,(-0x1.Ap+1),(-0x1.7p-1),0xA.62DAC3p-25},{0xF.870AB7p-66,0x3.F5D6BEp+40,0x1.B451AFp+37,0xA.36831Ap-35,0xF.C7D00Cp-73,0xB.690C29p-53},{0x0.2p+1,(-0x1.Ap+1),0xA.A20CFBp+29,0x1.F4A390p+84,0xF.C7D00Cp-73,0x8.3p+1}},{{0xE.90203Cp+58,0x3.F5D6BEp+40,0x3.5237E9p-98,0x1.2p+1,(-0x1.7p-1),0x2.F2280Fp+12},{0x0.11814Ap+70,0xC.0ABB7Bp+80,0x7.E06730p-82,(-0x8.8p-1),0xE.90203Cp+58,0xA.36831Ap-35},{0x8.3p+1,0xF.870AB7p-66,0xD.4C7704p-94,0xE.90203Cp+58,0x3.5237E9p-98,0xC.0ABB7Bp+80},{0xA.36831Ap-35,(-0x1.9p-1),0x2.F2280Fp+12,(-0x1.9p-1),0xA.36831Ap-35,(-0x1.Ap+1)},{(-0x4.4p-1),0x2.E3E2A5p-62,(-0x8.8p-1),0x3.F5D6BEp+40,0x0.2p+1,0xC.AF600Dp-35},{0xA.62DAC3p-25,0x1.2p+1,(-0x10.0p+1),0x2.E3E2A5p-62,0x8.48A285p-13,0xC.AF600Dp-35},{0x1.0p-1,0x4.7p-1,(-0x8.8p-1),(-0x10.0p+1),0xF.870AB7p-66,(-0x1.Ap+1)}},{{0x8.48A285p-13,0xF.C7D00Cp-73,0x2.F2280Fp+12,0x1.0p-1,0x2.E3E2A5p-62,0xC.0ABB7Bp+80},{0x2.F62330p-62,0xF.E21AC5p-93,0xD.4C7704p-94,0xC.AF600Dp-35,0x1.3p-1,0xA.36831Ap-35},{0x2.F2280Fp+12,0x1.B451AFp+37,0x7.E06730p-82,0x7.E06730p-82,0x1.B451AFp+37,0x2.F2280Fp+12},{(-0x10.0p+1),0xA.36831Ap-35,0x3.5237E9p-98,0xF.C7D00Cp-73,0x0.11814Ap+70,0x8.3p+1},{0x4.7p-1,0xE.90203Cp+58,0xA.A20CFBp+29,0x1.B451AFp+37,0xC.AF600Dp-35,0xB.690C29p-53},{0x4.7p-1,0x0.66401Ep-32,0x1.B451AFp+37,0xF.C7D00Cp-73,0x2.F62330p-62,0xA.62DAC3p-25},{(-0x10.0p+1),0x0.11814Ap+70,0xC.0ABB7Bp+80,0x7.E06730p-82,(-0x8.8p-1),0xE.90203Cp+58}},{{0x2.F2280Fp+12,0xB.690C29p-53,0x0.66401Ep-32,0xC.AF600Dp-35,0x1.9p-1,(-0x10.0p+1)},{0x2.F62330p-62,(-0x9.1p+1),0xB.690C29p-53,0x1.0p-1,0xB.690C29p-53,(-0x9.1p+1)},{0x8.48A285p-13,0x7.E06730p-82,0x2.F62330p-62,(-0x10.0p+1),0xF.E21AC5p-93,0x1.B451AFp+37},{0x4.7p-1,0xC.AF600Dp-35,0xA.62DAC3p-25,0x8.48A285p-13,0x1.3p-1,0xA.A20CFBp+29},{0x2.F62330p-62,0xC.AF600Dp-35,0x1.0p-1,0x1.9p-1,0xF.E21AC5p-93,0x3.5237E9p-98},{0x3.F5D6BEp+40,(-0x1.Ap+1),0x8.48A285p-13,0xD.4C7704p-94,0xA.36831Ap-35,0x7.E06730p-82},{0xA.62DAC3p-25,0xC.0ABB7Bp+80,0x2.F62330p-62,0xB.690C29p-53,(-0x8.8p-1),0xD.4C7704p-94}}};
static int64_t g_95 = 0xCF43AC61AB63E390LL;
static uint32_t g_101 = 1UL;
static uint16_t g_114 = 65534UL;
static uint16_t g_121 = 3UL;
static int64_t g_148 = 0xDE4B32DF3E0A3195LL;
static const volatile uint16_t g_169 = 0xAF18L;/* VOLATILE GLOBAL g_169 */
static const volatile uint16_t *g_168 = &g_169;
static const volatile uint16_t * volatile *g_167 = &g_168;
static uint8_t g_172 = 1UL;
static int64_t g_174 = 0xE8C50E43E29B3848LL;
static int16_t g_191[8] = {(-6L),0L,0L,(-6L),0L,0L,(-6L),0L};
static int8_t g_209 = 0xEFL;
static const int32_t *g_211[3] = {&g_5,&g_5,&g_5};
static const int32_t **g_210 = &g_211[2];
static int32_t g_214 = 1L;
static uint16_t g_234 = 65526UL;
static uint32_t g_252[5][2][8] = {{{0UL,0x76213B6CL,0x758AB5E4L,0UL,5UL,0x65C06C30L,0xB3CD36BCL,4294967293UL},{2UL,0UL,4294967293UL,0x77E2E7F7L,9UL,0x77E2E7F7L,4294967293UL,0UL}},{{4294967293UL,0x6F7E9452L,0x625BB909L,4294967289UL,0x31457212L,4294967293UL,0xFF6F5AAAL,0UL},{2UL,0xFF6F5AAAL,0x77E2E7F7L,0x758AB5E4L,4294967293UL,0x244C01E1L,0xFF6F5AAAL,0x48A06586L}},{{1UL,0x758AB5E4L,0x625BB909L,4294967293UL,0x76213B6CL,0x76213B6CL,4294967293UL,0x625BB909L},{0x76213B6CL,0x76213B6CL,4294967293UL,0x625BB909L,0x758AB5E4L,1UL,0xB3CD36BCL,5UL}},{{0x244C01E1L,4294967293UL,0x758AB5E4L,0x77E2E7F7L,0xFF6F5AAAL,2UL,0x20A964B1L,5UL},{4294967293UL,0x31457212L,4294967289UL,0x625BB909L,0x6F7E9452L,4294967293UL,0x6F7E9452L,0x625BB909L}},{{0x77E2E7F7L,9UL,0x77E2E7F7L,4294967293UL,0UL,2UL,9UL,0x48A06586L},{0x65C06C30L,5UL,0UL,0x758AB5E4L,0x76213B6CL,0UL,0UL,0UL}}};
static float g_265 = 0x1.Ap-1;
static int32_t g_266 = 3L;
static uint32_t g_267 = 18446744073709551608UL;
static uint32_t g_296 = 0xFA3CC0CAL;
static volatile int32_t *g_299 = (void*)0;
static float g_331 = 0x3.D06509p+87;
static uint64_t g_333 = 0UL;
static uint64_t g_341 = 18446744073709551612UL;
static float ***g_401[8] = {&g_69,(void*)0,&g_69,&g_69,(void*)0,&g_69,&g_69,(void*)0};
static float **** volatile g_400 = &g_401[4];/* VOLATILE GLOBAL g_400 */
static const uint16_t g_487 = 0xCA6BL;
static const uint16_t *g_486[10] = {&g_487,&g_487,&g_487,&g_487,&g_487,&g_487,&g_487,&g_487,&g_487,&g_487};
static const uint32_t *g_504 = &g_296;
static const uint32_t **g_503 = &g_504;
static const uint32_t *** volatile g_502 = &g_503;/* VOLATILE GLOBAL g_502 */
static int32_t * volatile g_506 = (void*)0;/* VOLATILE GLOBAL g_506 */
static int32_t *g_512 = &g_214;
static volatile int16_t g_565 = 0x1BE3L;/* VOLATILE GLOBAL g_565 */
static volatile uint16_t g_566[6][8] = {{0xCD09L,0x678DL,0x678DL,0xCD09L,0x9688L,0xCD09L,0x678DL,0x678DL},{0x678DL,0x9688L,7UL,7UL,0x9688L,0x678DL,0x9688L,7UL},{0xCD09L,0x9688L,0xCD09L,0x678DL,0x678DL,0xCD09L,0x9688L,0xCD09L},{1UL,0x678DL,7UL,0x678DL,1UL,1UL,0x678DL,7UL},{1UL,1UL,0x678DL,7UL,0x678DL,1UL,1UL,0x678DL},{0xCD09L,0x678DL,0x678DL,0xCD09L,0x9688L,0xCD09L,0x678DL,0x678DL}};
static int16_t *g_576[8][10][3] = {{{&g_33,&g_33,&g_191[0]},{&g_191[5],&g_191[5],(void*)0},{&g_33,&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_33},{&g_191[5],&g_33,&g_33},{&g_191[5],(void*)0,&g_33},{&g_191[0],&g_33,(void*)0},{&g_191[0],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[0]}},{{(void*)0,&g_33,&g_191[0]},{&g_33,(void*)0,&g_191[5]},{&g_33,&g_33,&g_191[5]},{&g_33,&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_33},{&g_191[5],&g_33,&g_33},{&g_191[5],(void*)0,&g_33},{&g_191[0],&g_33,(void*)0},{&g_191[0],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[5]}},{{&g_191[5],(void*)0,&g_191[0]},{(void*)0,&g_33,&g_191[0]},{&g_33,(void*)0,&g_191[5]},{&g_33,&g_33,&g_191[5]},{&g_33,&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_33},{&g_191[5],&g_33,&g_33},{&g_191[5],(void*)0,&g_33},{&g_191[0],&g_33,(void*)0},{&g_191[0],(void*)0,&g_191[5]}},{{&g_191[5],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[0]},{(void*)0,&g_33,&g_191[0]},{&g_33,(void*)0,&g_191[5]},{&g_33,&g_33,&g_191[5]},{&g_33,&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_33},{&g_191[5],&g_33,&g_33},{&g_191[5],(void*)0,&g_33},{&g_191[0],&g_33,(void*)0}},{{&g_191[0],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[0]},{(void*)0,&g_33,&g_191[0]},{&g_33,(void*)0,&g_191[5]},{&g_33,&g_33,&g_191[5]},{&g_33,&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_33},{&g_191[5],&g_33,&g_33},{&g_191[5],(void*)0,&g_33}},{{&g_191[0],&g_33,(void*)0},{&g_191[0],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[0]},{(void*)0,&g_33,&g_191[0]},{&g_33,(void*)0,&g_191[5]},{&g_33,&g_33,&g_191[5]},{&g_33,&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_33},{&g_191[5],&g_33,&g_33}},{{&g_191[5],(void*)0,&g_33},{&g_191[0],&g_33,(void*)0},{&g_191[0],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[5]},{&g_191[5],(void*)0,&g_191[0]},{(void*)0,&g_33,&g_191[0]},{&g_191[0],(void*)0,(void*)0},{&g_33,&g_33,&g_33},{&g_191[0],&g_33,(void*)0},{(void*)0,&g_33,&g_191[0]}},{{&g_33,&g_33,&g_33},{(void*)0,(void*)0,&g_191[0]},{&g_191[5],&g_33,(void*)0},{&g_191[5],&g_191[5],&g_33},{(void*)0,&g_33,(void*)0},{&g_33,&g_191[5],&g_191[5]},{(void*)0,&g_33,&g_191[5]},{&g_191[0],(void*)0,(void*)0},{&g_33,&g_33,&g_33},{&g_191[0],&g_33,(void*)0}}};
static int32_t g_637 = 0x2AA353A1L;
static int16_t g_638[9][3] = {{1L,0x73F4L,0xCCA8L},{0x73F4L,(-1L),(-1L)},{0xF090L,1L,0xCCA8L},{0xFB5CL,0x315FL,1L},{0xFB5CL,(-10L),0x73F4L},{0xF090L,(-7L),0xF090L},{0x73F4L,(-10L),0xFB5CL},{1L,0x315FL,0xFB5CL},{0xCCA8L,1L,0xF090L}};
static int16_t g_641[6] = {0L,0L,0L,0L,0L,0L};
static uint16_t g_643[7] = {65532UL,0UL,65532UL,65532UL,0UL,65532UL,65532UL};
static uint64_t g_683[6][3][3] = {{{0UL,18446744073709551615UL,0x433BDB152E0C0317LL},{0x433BDB152E0C0317LL,0xD22FF91A264E88CFLL,0xE201B80C791EE181LL},{18446744073709551615UL,1UL,18446744073709551615UL}},{{18446744073709551615UL,0xE201B80C791EE181LL,9UL},{18446744073709551615UL,18446744073709551615UL,0x37F553C27A65FFEFLL},{0x433BDB152E0C0317LL,0UL,0UL}},{{0UL,18446744073709551615UL,0UL},{1UL,18446744073709551615UL,0x37F553C27A65FFEFLL},{0x3A56367524BDF455LL,1UL,9UL}},{{0xE4A9CC8A5B444A66LL,18446744073709551615UL,18446744073709551615UL},{4UL,1UL,0xE201B80C791EE181LL},{1UL,18446744073709551615UL,0x433BDB152E0C0317LL}},{{18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,0UL,4UL},{1UL,18446744073709551615UL,18446744073709551615UL}},{{4UL,0xE201B80C791EE181LL,1UL},{0xE4A9CC8A5B444A66LL,1UL,18446744073709551615UL},{0x3A56367524BDF455LL,0xD22FF91A264E88CFLL,4UL}}};
static uint16_t g_725[4] = {0x53D7L,0x53D7L,0x53D7L,0x53D7L};
static int32_t ** volatile g_730 = &g_512;/* VOLATILE GLOBAL g_730 */
static int32_t ** volatile g_740 = &g_512;/* VOLATILE GLOBAL g_740 */
static uint8_t g_746 = 0x26L;
static uint64_t g_785 = 0x2FB8001C74C3D815LL;
static int8_t g_860 = 9L;
static volatile int32_t *g_864 = (void*)0;
static volatile int32_t * volatile *g_863 = &g_864;
static volatile uint16_t g_887[7] = {0UL,65529UL,65529UL,0UL,65529UL,65529UL,0UL};
static volatile int32_t g_900[7] = {1L,1L,1L,1L,1L,1L,1L};
static volatile uint8_t g_911 = 7UL;/* VOLATILE GLOBAL g_911 */
static volatile float g_984 = 0x5.83048Cp+8;/* VOLATILE GLOBAL g_984 */
static volatile uint8_t g_985 = 0x37L;/* VOLATILE GLOBAL g_985 */
static volatile int8_t g_1085 = 0x73L;/* VOLATILE GLOBAL g_1085 */
static int32_t * const *g_1095 = (void*)0;
static uint8_t g_1111 = 0xB9L;
static uint8_t g_1138[1][2][2] = {{{1UL,1UL},{1UL,1UL}}};
static uint32_t *g_1141 = &g_252[3][1][1];
static const volatile int64_t g_1154[8][8] = {{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL},{0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL,0x48FF7537DF590FB9LL,0L,0L,0x48FF7537DF590FB9LL}};
static const volatile int64_t *g_1153 = &g_1154[3][0];
static const volatile int64_t * volatile * volatile g_1152 = &g_1153;/* VOLATILE GLOBAL g_1152 */
static uint8_t g_1275 = 0x13L;
static volatile uint8_t * volatile g_1277 = &g_911;/* VOLATILE GLOBAL g_1277 */
static volatile uint8_t * volatile * volatile g_1276[10] = {&g_1277,&g_1277,&g_1277,&g_1277,&g_1277,&g_1277,&g_1277,&g_1277,&g_1277,&g_1277};
static uint8_t g_1290 = 0UL;
static int32_t ** volatile g_1332[2][1][6] = {{{&g_512,&g_512,&g_512,&g_512,&g_512,&g_512}},{{&g_512,&g_512,&g_512,&g_512,&g_512,&g_512}}};
static const uint32_t g_1416 = 1UL;
static int32_t * const  volatile *g_1462 = &g_512;
static int32_t * const  volatile * volatile *g_1461 = &g_1462;
static int32_t * const  volatile * volatile ** volatile g_1460[4] = {&g_1461,&g_1461,&g_1461,&g_1461};
static int32_t * const  volatile * volatile ** volatile * volatile g_1463[10] = {&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3],&g_1460[3]};
static const float * const g_1498 = (void*)0;
static const float * const *g_1497[9] = {&g_1498,&g_1498,&g_1498,&g_1498,&g_1498,&g_1498,&g_1498,&g_1498,&g_1498};
static const float * const **g_1496 = &g_1497[7];
static const float * const ***g_1495 = &g_1496;
static volatile int32_t * volatile * volatile * volatile g_1501 = &g_863;/* VOLATILE GLOBAL g_1501 */
static volatile uint8_t g_1538 = 252UL;/* VOLATILE GLOBAL g_1538 */
static volatile uint8_t *g_1537 = &g_1538;
static volatile uint8_t * volatile *g_1536 = &g_1537;
static volatile uint8_t * volatile * volatile *g_1535 = &g_1536;
static int32_t * volatile * volatile g_1549 = &g_506;/* VOLATILE GLOBAL g_1549 */
static int32_t **g_1566 = (void*)0;
static uint32_t g_1654 = 0x0604B4FAL;
static int64_t * volatile g_1732[4][7] = {{&g_148,&g_148,&g_95,&g_95,&g_148,&g_148,&g_148},{(void*)0,&g_148,&g_148,(void*)0,&g_148,(void*)0,&g_148},{(void*)0,(void*)0,&g_148,&g_95,&g_148,(void*)0,(void*)0},{(void*)0,&g_148,&g_95,&g_148,(void*)0,(void*)0,&g_148}};
static int64_t * volatile *g_1731[8][1][3] = {{{&g_1732[0][1],&g_1732[0][1],(void*)0}},{{&g_1732[0][1],&g_1732[0][1],&g_1732[2][1]}},{{&g_1732[0][1],&g_1732[0][1],(void*)0}},{{&g_1732[0][1],&g_1732[0][1],&g_1732[2][1]}},{{&g_1732[0][1],&g_1732[0][1],(void*)0}},{{&g_1732[0][1],&g_1732[0][1],&g_1732[2][1]}},{{&g_1732[0][1],&g_1732[0][1],(void*)0}},{{&g_1732[0][1],&g_1732[0][1],&g_1732[2][1]}}};
static int32_t * volatile * volatile g_1761 = &g_512;/* VOLATILE GLOBAL g_1761 */
static int64_t g_1795 = 2L;
static volatile uint32_t g_1847 = 18446744073709551609UL;/* VOLATILE GLOBAL g_1847 */
static volatile uint32_t *g_1846 = &g_1847;
static volatile uint32_t * volatile *g_1845 = &g_1846;
static volatile uint32_t * volatile ** volatile g_1844 = &g_1845;/* VOLATILE GLOBAL g_1844 */
static volatile int32_t g_1883 = 0xDFFC7E7CL;/* VOLATILE GLOBAL g_1883 */
static int32_t **g_1895 = &g_512;
static int32_t ***g_1894 = &g_1895;
static const int32_t ** volatile g_1925 = &g_211[2];/* VOLATILE GLOBAL g_1925 */
static const volatile uint16_t *** volatile * volatile g_1955 = (void*)0;/* VOLATILE GLOBAL g_1955 */
static const volatile uint16_t *** volatile * volatile * volatile g_1954 = &g_1955;/* VOLATILE GLOBAL g_1954 */
static int8_t g_1982[7][10] = {{0xD7L,3L,0L,0xD7L,0x2EL,(-1L),3L,3L,(-1L),0x2EL},{0xD7L,(-9L),(-9L),0xD7L,0xFCL,0x73L,3L,(-9L),0x73L,0x2EL},{0x73L,3L,(-9L),0x73L,0x2EL,0x73L,(-9L),3L,0x73L,0xFCL},{0xD7L,3L,0L,0xD7L,0x2EL,(-1L),3L,3L,(-1L),0x2EL},{0xD7L,(-9L),(-9L),0xD7L,0xFCL,0x73L,3L,(-9L),0x73L,0x2EL},{0x73L,3L,(-9L),0x73L,0x2EL,0x73L,(-9L),3L,0x73L,0xFCL},{0xD7L,3L,0L,0xECL,(-9L),0x86L,0x73L,0x73L,0x86L,(-9L)}};
static uint32_t *g_2041[4] = {&g_65,&g_65,&g_65,&g_65};
static int32_t ****g_2069 = &g_1894;
static int32_t **** const *g_2068 = &g_2069;
static int32_t g_2079[9][7][1] = {{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L},{1L},{1L},{1L}}};
static uint16_t *g_2188 = &g_643[5];
static uint16_t ** volatile g_2187 = &g_2188;/* VOLATILE GLOBAL g_2187 */
static uint16_t ** volatile * volatile g_2186[3] = {&g_2187,&g_2187,&g_2187};
static uint16_t ** volatile * volatile *g_2185[3] = {&g_2186[1],&g_2186[1],&g_2186[1]};
static uint16_t ** volatile * volatile * volatile *g_2184[6] = {&g_2185[1],&g_2185[1],&g_2185[1],&g_2185[1],&g_2185[1],&g_2185[1]};


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static int32_t * func_2(int32_t * p_3);
static int32_t * func_6(int32_t * p_7, int32_t * p_8, uint16_t  p_9, int16_t  p_10, int8_t  p_11);
static const float  func_28(uint8_t * p_29, int16_t * p_30);
static int32_t  func_42(int16_t * p_43);
static int16_t * func_44(uint8_t * p_45);
static uint8_t * func_46(uint8_t * p_47, const uint8_t * p_48, const int64_t  p_49);
static uint16_t  func_53(uint8_t * p_54);
static uint8_t * func_55(uint32_t  p_56, int8_t  p_57);
static uint16_t  func_79(uint64_t  p_80, float * p_81, uint32_t  p_82);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_21 g_69 g_70 g_740 g_512 g_210 g_1461 g_1462 g_214 g_638 g_1141 g_209 g_33 g_1535 g_1536 g_1537 g_1538 g_75 g_252 g_487 g_234 g_860 g_121 g_503 g_504 g_296 g_2184 g_2079 g_64 g_148 g_1138
 * writes: g_21 g_5 g_64 g_211 g_214 g_267 g_638 g_252 g_209 g_69 g_333 g_341 g_401 g_860 g_2079 g_148
 */
static float  func_1(void)
{ /* block id: 0 */
    int32_t *l_4 = &g_5;
    int32_t *l_2107 = &g_2079[6][5][0];
    float ****l_2109 = (void*)0;
    float *****l_2108 = &l_2109;
    int16_t *l_2112 = &g_638[6][1];
    uint32_t **l_2122 = &g_2041[1];
    int32_t ***l_2123 = &g_1566;
    int32_t ****l_2124 = (void*)0;
    int32_t ****l_2125 = &l_2123;
    int8_t *l_2126 = &g_209;
    int32_t l_2127 = 1L;
    uint32_t l_2158 = 4294967288UL;
    int32_t l_2163 = (-9L);
    int32_t l_2164 = 0xD039EF23L;
    int32_t l_2165[9] = {0xB413C24FL,0xB413C24FL,0xB413C24FL,0xB413C24FL,0xB413C24FL,0xB413C24FL,0xB413C24FL,0xB413C24FL,0xB413C24FL};
    uint16_t l_2166 = 65531UL;
    int16_t l_2216 = 0x014BL;
    uint32_t l_2218 = 2UL;
    int i;
    l_2107 = func_2(l_4);
    l_2108 = l_2108;
    l_2127 ^= (((**g_1462) |= (*l_4)) || (((((*l_2112) ^= (*l_2107)) <= (((+(safe_sub_func_uint32_t_u_u((*l_2107), ((safe_mul_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((*l_4) , (safe_add_func_int8_t_s_s((((*g_1141) = ((void*)0 == l_2122)) == (*l_4)), (((((*l_2126) &= (&g_1095 == ((*l_2125) = l_2123))) , 0x6099L) , (*l_2107)) , g_33)))), 0xFF63L)), (*l_4))) | 0x31C8L)))) > 0xF6BAFE06BA68E707LL) < (***g_1535))) < (*l_4)) || 0xB6L));
    for (g_214 = 0; (g_214 >= 0); g_214 -= 1)
    { /* block id: 1033 */
        const float * const *l_2128 = &g_1498;
        float **l_2129 = &g_70[4];
        float ***l_2130 = (void*)0;
        float ***l_2131 = &g_69;
        uint16_t *l_2155 = &g_725[3];
        uint16_t **l_2154[10][3][3] = {{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}},{{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155},{&l_2155,&l_2155,&l_2155}}};
        uint16_t *** const l_2153 = &l_2154[0][0][1];
        int32_t l_2157 = (-1L);
        int32_t l_2159 = 0x3643BCA4L;
        int32_t l_2162[9];
        int32_t ****l_2199 = &l_2123;
        uint8_t l_2215 = 0x58L;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_2162[i] = 0xB64ED4F7L;
        if ((l_2128 == ((*l_2131) = l_2129)))
        { /* block id: 1035 */
            int16_t l_2132 = (-9L);
            uint64_t *l_2133 = (void*)0;
            uint64_t *l_2134 = &g_333;
            uint64_t *l_2135 = (void*)0;
            int32_t l_2145 = 0x1BD1336BL;
            float ***l_2150 = (void*)0;
            const uint64_t l_2156 = 0x60CAD8E5B6BEAE6DLL;
            int32_t l_2160 = 0x2E458729L;
            int32_t *l_2161[6][3][1] = {{{(void*)0},{(void*)0},{&g_2079[6][4][0]}},{{&g_5},{&l_2159},{&g_2079[6][4][0]}},{{&l_2159},{&g_74},{&g_74}},{{&l_2159},{&g_2079[6][4][0]},{&g_74}},{{&g_2079[6][4][0]},{&l_2159},{&g_74}},{{&g_74},{&l_2159},{&g_2079[6][4][0]}}};
            int i, j, k;
            l_2158 |= (((g_75 >= (g_341 = ((*l_2134) = l_2132))) | ((safe_sub_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_s((safe_div_func_int16_t_s_s(((*g_1141) == 0xC4B4214CL), 0x79FAL)), 4)) | (safe_lshift_func_uint16_t_u_u((safe_unary_minus_func_uint32_t_u((l_2145 & (safe_mul_func_uint8_t_u_u(((((((safe_add_func_float_f_f(((g_401[0] = &g_69) != l_2150), ((***l_2131) = ((safe_div_func_uint64_t_u_u(0xFC72CCFFE51A63BFLL, l_2145)) , (-0x3.Ap+1))))) >= 0x1.6p-1) > (*l_4)) , (void*)0) == l_2153) , 1UL), g_487))))), l_2156))), g_234)) <= (*l_2107))) , l_2157);
            ++l_2166;
            return (*l_2107);
        }
        else
        { /* block id: 1043 */
            uint32_t l_2169 = 18446744073709551615UL;
            int32_t l_2170[9][5][2] = {{{(-7L),(-9L)},{0xD2343198L,0x9AAD991CL},{0L,0xD40910D9L},{0x5EFFED8BL,(-1L)},{9L,9L}},{{0xBE38CD7FL,(-9L)},{0x9AAD991CL,(-7L)},{2L,1L},{(-9L),2L},{(-6L),(-1L)}},{{(-6L),2L},{(-9L),1L},{2L,(-7L)},{0x9AAD991CL,(-9L)},{0xBE38CD7FL,9L}},{{9L,(-1L)},{0x5EFFED8BL,0xD40910D9L},{0L,0x9AAD991CL},{0xD2343198L,(-9L)},{(-7L),(-9L)}},{{0xD2343198L,0x9AAD991CL},{0L,0xD40910D9L},{0x5EFFED8BL,(-1L)},{9L,9L},{0xBE38CD7FL,(-9L)}},{{0x9AAD991CL,(-7L)},{2L,1L},{(-9L),2L},{(-6L),(-1L)},{(-6L),2L}},{{(-9L),1L},{2L,(-7L)},{0x9AAD991CL,(-9L)},{0xBE38CD7FL,9L},{9L,(-1L)}},{{0x5EFFED8BL,0xD40910D9L},{0L,0x9AAD991CL},{0xD2343198L,(-9L)},{(-7L),(-9L)},{0xD2343198L,0x9AAD991CL}},{{0L,0xD40910D9L},{0x5EFFED8BL,(-1L)},{9L,9L},{0xBE38CD7FL,(-9L)},{0x9AAD991CL,(-7L)}}};
            int i, j, k;
            (*l_2107) |= (***g_1461);
            l_2170[8][0][1] |= l_2169;
        }
        for (g_860 = 0; (g_860 >= 0); g_860 -= 1)
        { /* block id: 1049 */
            uint8_t l_2189 = 249UL;
            (***l_2131) = (((safe_sub_func_float_f_f(((((*g_1537) <= ((safe_unary_minus_func_int64_t_s(0L)) , (((*l_4) <= (safe_mul_func_uint16_t_u_u((safe_add_func_int8_t_s_s(((*l_2107) , (l_2162[6] > (g_2079[8][5][0] &= ((safe_div_func_int16_t_s_s((((g_121 >= ((**g_503) < (g_2184[2] != (void*)0))) > g_252[4][1][1]) != 0xF914L), l_2157)) | 0x88C70605L)))), 0xA1L)), 0L))) != l_2189))) , (*l_4)) <= (**g_69)), (*l_2107))) > (*l_4)) > (-0x1.2p-1));
            for (g_148 = 0; (g_148 >= 0); g_148 -= 1)
            { /* block id: 1054 */
                int i, j, k;
                return g_2079[g_148][(g_148 + 4)][g_148];
            }
        }
        for (g_5 = 0; (g_5 <= 0); g_5 += 1)
        { /* block id: 1060 */
            const int32_t *l_2198 = &g_266;
            const int32_t * const *l_2197 = &l_2198;
            const int32_t * const **l_2196 = &l_2197;
            const int32_t * const ***l_2195 = &l_2196;
            int32_t l_2214 = (-10L);
            int32_t *l_2217[2][2];
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 2; j++)
                    l_2217[i][j] = &l_2165[1];
            }
            g_2079[3][0][0] ^= (l_2216 = (safe_add_func_uint8_t_u_u((*g_1537), ((!((safe_sub_func_uint16_t_u_u((l_2195 != l_2199), ((safe_lshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s((((*l_4) >= (g_148 >= (safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((&g_267 == &g_65) | (safe_lshift_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(((l_2159 = g_638[3][0]) , (safe_sub_func_uint64_t_u_u(1UL, l_2214))), l_2215)), 2))), l_2162[0])), g_1138[0][0][1])))) == (*l_2107)), 8)), 7)) == 65529UL))) , 1L)) > l_2214))));
            return l_2218;
        }
    }
    return (*l_4);
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_21 g_69 g_70 g_740 g_512 g_210 g_1461 g_1462 g_214
 * writes: g_21 g_5 g_64 g_211 g_214 g_267
 */
static int32_t * func_2(int32_t * p_3)
{ /* block id: 1 */
    int16_t l_14 = 0xADB2L;
    uint8_t *l_20[5][5][5] = {{{&g_21[2],(void*)0,&g_21[4],&g_21[4],&g_21[4]},{&g_21[4],&g_21[4],&g_21[5],&g_21[4],&g_21[4]},{&g_21[4],&g_21[4],&g_21[4],&g_21[4],&g_21[1]},{(void*)0,&g_21[4],&g_21[4],&g_21[4],(void*)0},{&g_21[4],&g_21[4],&g_21[4],(void*)0,&g_21[4]}},{{&g_21[4],&g_21[4],&g_21[4],(void*)0,&g_21[4]},{&g_21[2],&g_21[4],&g_21[4],&g_21[4],&g_21[4]},{&g_21[4],(void*)0,&g_21[5],&g_21[5],(void*)0},{&g_21[4],&g_21[4],&g_21[4],&g_21[2],&g_21[1]},{&g_21[4],&g_21[4],&g_21[4],&g_21[5],&g_21[4]}},{{&g_21[4],&g_21[4],&g_21[1],&g_21[4],&g_21[4]},{&g_21[4],&g_21[4],&g_21[4],(void*)0,&g_21[4]},{&g_21[4],&g_21[4],&g_21[1],(void*)0,(void*)0},{&g_21[4],&g_21[4],&g_21[4],&g_21[4],&g_21[4]},{&g_21[2],(void*)0,&g_21[4],&g_21[4],&g_21[4]}},{{&g_21[4],&g_21[4],&g_21[5],&g_21[4],&g_21[4]},{&g_21[4],&g_21[4],&g_21[4],&g_21[4],&g_21[1]},{(void*)0,&g_21[4],&g_21[4],&g_21[4],&g_21[5]},{&g_21[4],&g_21[4],(void*)0,&g_21[4],(void*)0},{&g_21[4],&g_21[4],&g_21[4],&g_21[5],&g_21[4]}},{{&g_21[4],(void*)0,&g_21[4],&g_21[4],(void*)0},{&g_21[4],&g_21[5],&g_21[4],&g_21[4],&g_21[5]},{(void*)0,(void*)0,&g_21[1],&g_21[4],&g_21[4]},{(void*)0,&g_21[4],(void*)0,&g_21[4],&g_21[4]},{&g_21[4],&g_21[4],&g_21[4],&g_21[4],&g_21[4]}}};
    int32_t l_22 = 0xA6001648L;
    int32_t l_23[2][10];
    int16_t *l_26 = (void*)0;
    int16_t *l_27[5][8][6] = {{{&l_14,(void*)0,&l_14,&l_14,&l_14,(void*)0},{&l_14,(void*)0,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,(void*)0,&l_14,&l_14,&l_14},{(void*)0,(void*)0,(void*)0,(void*)0,&l_14,&l_14},{&l_14,(void*)0,&l_14,(void*)0,&l_14,&l_14},{(void*)0,&l_14,&l_14,&l_14,&l_14,(void*)0},{&l_14,(void*)0,&l_14,&l_14,&l_14,(void*)0}},{{&l_14,(void*)0,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,(void*)0,&l_14,&l_14,&l_14},{(void*)0,(void*)0,(void*)0,(void*)0,&l_14,&l_14},{&l_14,(void*)0,&l_14,(void*)0,&l_14,&l_14},{(void*)0,&l_14,&l_14,&l_14,&l_14,(void*)0},{&l_14,(void*)0,&l_14,&l_14,&l_14,(void*)0},{&l_14,(void*)0,&l_14,&l_14,&l_14,&l_14}},{{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,(void*)0,&l_14,&l_14,&l_14},{(void*)0,(void*)0,(void*)0,(void*)0,&l_14,&l_14},{&l_14,(void*)0,&l_14,(void*)0,&l_14,&l_14},{(void*)0,&l_14,&l_14,&l_14,&l_14,(void*)0},{&l_14,(void*)0,&l_14,&l_14,&l_14,(void*)0},{&l_14,(void*)0,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14}},{{&l_14,&l_14,(void*)0,&l_14,&l_14,&l_14},{(void*)0,(void*)0,(void*)0,(void*)0,&l_14,&l_14},{&l_14,&l_14,&l_14,(void*)0,&l_14,&l_14},{(void*)0,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14}},{{(void*)0,&l_14,&l_14,(void*)0,&l_14,&l_14},{&l_14,&l_14,&l_14,(void*)0,&l_14,&l_14},{(void*)0,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{&l_14,&l_14,&l_14,&l_14,&l_14,&l_14},{(void*)0,&l_14,&l_14,(void*)0,&l_14,&l_14}}};
    int32_t l_1953 = 4L;
    const volatile uint16_t *** volatile * volatile * volatile l_1956 = &g_1955;/* VOLATILE GLOBAL l_1956 */
    uint16_t l_1966 = 0x4303L;
    uint32_t **l_2027 = &g_1141;
    uint32_t ***l_2026[7][4] = {{&l_2027,&l_2027,&l_2027,&l_2027},{&l_2027,&l_2027,&l_2027,&l_2027},{&l_2027,&l_2027,&l_2027,&l_2027},{&l_2027,&l_2027,&l_2027,&l_2027},{&l_2027,&l_2027,&l_2027,&l_2027},{&l_2027,&l_2027,&l_2027,&l_2027},{&l_2027,&l_2027,&l_2027,&l_2027}};
    uint8_t l_2034 = 0xEBL;
    uint16_t *l_2046 = &g_114;
    uint16_t ** const l_2045[9][1][10] = {{{&l_2046,&l_2046,&l_2046,(void*)0,(void*)0,&l_2046,&l_2046,(void*)0,&l_2046,(void*)0}},{{&l_2046,&l_2046,&l_2046,(void*)0,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046}},{{&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,(void*)0,&l_2046}},{{(void*)0,&l_2046,(void*)0,&l_2046,&l_2046,&l_2046,(void*)0,(void*)0,&l_2046,&l_2046}},{{(void*)0,&l_2046,(void*)0,&l_2046,&l_2046,(void*)0,(void*)0,&l_2046,(void*)0,&l_2046}},{{&l_2046,(void*)0,&l_2046,&l_2046,(void*)0,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046}},{{&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046,(void*)0,(void*)0,&l_2046}},{{(void*)0,&l_2046,&l_2046,&l_2046,&l_2046,(void*)0,&l_2046,&l_2046,&l_2046,&l_2046}},{{&l_2046,&l_2046,&l_2046,(void*)0,(void*)0,&l_2046,&l_2046,&l_2046,&l_2046,&l_2046}}};
    float l_2059 = (-0x1.Bp+1);
    int32_t l_2100 = 0xC33E8C30L;
    float ** const *l_2104 = &g_69;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 10; j++)
            l_23[i][j] = 0x3147F59EL;
    }
    (*g_210) = func_6(((65535UL <= (l_23[1][2] = (safe_add_func_int8_t_s_s((l_14 <= (safe_mul_func_uint8_t_u_u((((safe_add_func_uint32_t_u_u(((void*)0 == p_3), (*p_3))) || l_14) >= (safe_unary_minus_func_int16_t_s(l_14))), (0x0BL <= (++g_21[2]))))), 0L)))) , p_3), &l_22, l_22, l_14, g_5);
    (***g_1461) &= (-10L);
    for (g_267 = 0; (g_267 != 58); ++g_267)
    { /* block id: 943 */
        float l_1930 = 0x1.DEA54Cp-17;
        uint64_t *l_1931 = &g_683[2][2][2];
        int32_t l_1936 = 5L;
        int8_t *l_1939 = (void*)0;
        int8_t *l_1940 = &g_209;
        int32_t l_1952 = 0x9DDA4FE8L;
        uint64_t l_2010 = 18446744073709551615UL;
        uint32_t **l_2038 = (void*)0;
        uint32_t *l_2040 = (void*)0;
        uint32_t **l_2039[3];
        int32_t ***l_2058 = (void*)0;
        int64_t l_2081 = 0xAA0EC1DAC12F8009LL;
        int i;
        for (i = 0; i < 3; i++)
            l_2039[i] = &l_2040;
    }
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_69 g_70 g_740 g_512
 * writes: g_5 g_64
 */
static int32_t * func_6(int32_t * p_7, int32_t * p_8, uint16_t  p_9, int16_t  p_10, int8_t  p_11)
{ /* block id: 4 */
    uint8_t *l_31 = &g_21[4];
    int16_t *l_32 = &g_33;
    (**g_69) = func_28(l_31, l_32);
    return (*g_740);
}


/* ------------------------------------------ */
/* 
 * reads : g_5
 * writes: g_5
 */
static const float  func_28(uint8_t * p_29, int16_t * p_30)
{ /* block id: 5 */
    int32_t *l_34 = &g_5;
    int16_t *l_39 = &g_33;
    uint8_t *l_50 = &g_21[4];
    int16_t *l_1655 = &g_191[0];
    const int32_t ***l_1665[6] = {(void*)0,&g_210,(void*)0,(void*)0,&g_210,(void*)0};
    uint32_t l_1690[6] = {0x5437C276L,3UL,0x5437C276L,0x5437C276L,3UL,0x5437C276L};
    float l_1708[3][5] = {{0x2.2F25A4p+38,0x2.2F25A4p+38,0x2.2F25A4p+38,0x2.2F25A4p+38,0x2.2F25A4p+38},{0xA.AB4DF3p+17,0xA.AB4DF3p+17,0xA.AB4DF3p+17,0xA.AB4DF3p+17,0xA.AB4DF3p+17},{0x2.2F25A4p+38,0x2.2F25A4p+38,0x2.2F25A4p+38,0x2.2F25A4p+38,0x2.2F25A4p+38}};
    int32_t l_1734 = 0xE9D74983L;
    int32_t *l_1736 = &l_1734;
    int32_t **l_1735 = &l_1736;
    float ****l_1739[1];
    float *****l_1738 = &l_1739[0];
    int32_t l_1763 = (-8L);
    int32_t l_1764[7][6] = {{0L,0x595C6BCAL,0L,(-4L),2L,0x8EA935A3L},{2L,0x70BEB801L,0L,0x70BEB801L,2L,(-1L)},{0x8EA935A3L,0x595C6BCAL,0x58791715L,0xDE765ACAL,0x70BEB801L,0L},{0L,0xD22E03FFL,0x595C6BCAL,0x595C6BCAL,0xD22E03FFL,0L},{0xDE765ACAL,(-4L),0x58791715L,2L,0L,(-1L)},{0xD22E03FFL,0x8EA935A3L,0L,0L,0L,0x8EA935A3L},{0xD22E03FFL,(-1L),0L,2L,0x58791715L,(-4L)}};
    uint16_t l_1769 = 0x641EL;
    uint32_t l_1777 = 0x81B85A49L;
    uint64_t l_1793 = 0x4C1927E046FB2BBALL;
    uint32_t l_1798 = 0xD3E99789L;
    uint64_t l_1809[7] = {1UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL};
    uint8_t l_1837 = 0x38L;
    int32_t l_1884 = (-6L);
    float l_1904 = 0x8.3BCC13p-31;
    uint8_t l_1908 = 2UL;
    int i, j;
    for (i = 0; i < 1; i++)
        l_1739[i] = &g_401[4];
    (*l_34) ^= (l_34 != (void*)0);
    return (*l_34);
}


/* ------------------------------------------ */
/* 
 * reads : g_1462 g_512 g_214 g_167 g_168 g_169 g_1461 g_333 g_1275 g_211 g_210 g_746 g_730
 * writes: g_214 g_333 g_1275 g_211 g_267 g_746 g_486
 */
static int32_t  func_42(int16_t * p_43)
{ /* block id: 815 */
    int32_t l_1658[9];
    int i;
    for (i = 0; i < 9; i++)
        l_1658[i] = 0L;
    (**g_1462) ^= 0x90BE401CL;
    (***g_1461) = (safe_lshift_func_uint16_t_u_u(l_1658[4], (**g_167)));
    for (g_333 = 0; (g_333 <= 4); g_333 += 1)
    { /* block id: 820 */
        for (g_1275 = 0; (g_1275 <= 2); g_1275 += 1)
        { /* block id: 823 */
            float l_1659[2][6][10] = {{{0x5.DF3EA5p+55,(-0x1.4p-1),0x7.B8354Fp-43,0x2.E24AE7p+76,0x0.5p-1,0x1.1EFBB0p-14,0x5.DF3EA5p+55,0x5.DF3EA5p+55,0x1.1EFBB0p-14,0x0.5p-1},{(-0x1.4p-1),0x3.9p+1,0x3.9p+1,(-0x1.4p-1),0x1.0p-1,(-0x1.5p-1),0x0.5p-1,0xA.0E7988p-8,0x3.9p+1,0x5.DF3EA5p+55},{0x1.0p-1,0x5.DF3EA5p+55,0xF.2F64FEp+82,0x3.78E325p-18,0x6.11EF23p+10,0xF.2F64FEp+82,0x3.9p+1,0x0.5p-1,0x3.9p+1,0xF.2F64FEp+82},{0x2.E24AE7p+76,(-0x1.4p-1),(-0x1.Dp+1),(-0x1.4p-1),0x2.E24AE7p+76,0xF.58E550p-55,0x5.6FCA0Bp-8,0x2.E24AE7p+76,0x1.1EFBB0p-14,0x6.11EF23p+10},{0x0.5p-1,0x1.0p-1,0x3.78E325p-18,0x2.E24AE7p+76,0x8.5FDD3Cp-57,(-0x1.5p-1),0x6.11EF23p+10,0x1.0p-1,0x1.0p-1,0x6.11EF23p+10},{0xA.0E7988p-8,0x2.E24AE7p+76,0xF.2F64FEp+82,0xF.2F64FEp+82,0x2.E24AE7p+76,0xA.0E7988p-8,0x3.78E325p-18,(-0x1.4p-1),(-0x1.5p-1),0xF.2F64FEp+82}},{{0x5.DF3EA5p+55,0x0.5p-1,0x5.7663E3p+62,0x5.6FCA0Bp-8,0x6.11EF23p+10,(-0x1.Dp+1),0x5.6FCA0Bp-8,0x5.DF3EA5p+55,0x7.B8354Fp-43,0x5.DF3EA5p+55},{0x5.DF3EA5p+55,0xA.0E7988p-8,0x1.0p-1,0x2.E24AE7p+76,0x1.0p-1,0xA.0E7988p-8,0x5.DF3EA5p+55,0x3.9p+1,0xA.0E7988p-8,0x0.5p-1},{0xA.0E7988p-8,0x5.DF3EA5p+55,0x3.9p+1,0xA.0E7988p-8,0x0.5p-1,(-0x1.5p-1),0x1.0p-1,(-0x1.4p-1),0x3.9p+1,0x3.9p+1},{0x0.5p-1,0x5.DF3EA5p+55,0xF.58E550p-55,0x6.11EF23p+10,0x6.11EF23p+10,0xF.58E550p-55,0x5.DF3EA5p+55,0x0.5p-1,0x5.7663E3p+62,0x5.6FCA0Bp-8},{0x2.E24AE7p+76,0x5.7663E3p+62,0x7.B8354Fp-43,0x3.9p+1,0xF.58E550p-55,(-0x1.Dp+1),0x3.78E325p-18,0xF.58E550p-55,0x5.7663E3p+62,0x1.0p-1},{0x1.0p-1,0x8.5FDD3Cp-57,0x7.B8354Fp-43,0xF.58E550p-55,0xA.0E7988p-8,0xF.58E550p-55,0x7.B8354Fp-43,0x8.5FDD3Cp-57,0x1.0p-1,0x7.B8354Fp-43}}};
            int32_t l_1660 = 0x60B16FBAL;
            int i, j, k;
            (*g_210) = g_211[g_1275];
            for (g_267 = 0; (g_267 <= 3); g_267 += 1)
            { /* block id: 827 */
                return l_1660;
            }
            for (g_746 = 0; g_746 < 10; g_746 += 1)
            {
                g_486[g_746] = &g_725[3];
            }
        }
    }
    for (g_746 = 0; (g_746 < 39); g_746++)
    { /* block id: 835 */
        (*g_210) = (*g_730);
        (*g_210) = &l_1658[4];
    }
    return l_1658[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_785 g_512 g_214 g_1277 g_911 g_21 g_860 g_725 g_503 g_504 g_296 g_643 g_683 g_69 g_70 g_1152 g_1153 g_1154 g_641 g_1141 g_252 g_1138 g_1460 g_1461 g_1462 g_210 g_211 g_5 g_1290 g_746 g_84 g_1495 g_167 g_168 g_121 g_1535 g_1549 g_191
 * writes: g_785 g_266 g_214 g_209 g_860 g_21 g_84 g_64 g_1275 g_121 g_641 g_95 g_1290 g_1460 g_746 g_114 g_101 g_1495 g_1501 g_506 g_1566 g_70
 */
static int16_t * func_44(uint8_t * p_45)
{ /* block id: 659 */
    int32_t l_1344 = (-1L);
    int32_t l_1356[10] = {0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L,0xE3125815L};
    uint64_t l_1359 = 0x8CC64E8669790A90LL;
    int32_t l_1380 = (-4L);
    const int16_t *l_1397 = &g_191[0];
    int16_t *l_1431 = (void*)0;
    uint32_t l_1436 = 0UL;
    const int32_t ** const *l_1439 = &g_210;
    uint16_t *l_1447 = &g_114;
    uint16_t **l_1446[1][4];
    uint16_t ***l_1445 = &l_1446[0][1];
    uint32_t **l_1459 = &g_1141;
    uint32_t ***l_1458 = &l_1459;
    uint16_t l_1472 = 0xA347L;
    uint8_t l_1521 = 1UL;
    float *l_1584 = &g_84[5][6][0];
    float *l_1591 = &g_84[4][1][1];
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
            l_1446[i][j] = &l_1447;
    }
    for (g_785 = 0; (g_785 <= 5); g_785 += 1)
    { /* block id: 662 */
        uint16_t l_1345[2];
        int32_t l_1357 = 6L;
        int32_t l_1358 = 0xACE2AD6FL;
        int32_t l_1384 = (-1L);
        int32_t l_1386 = 0x87114A5DL;
        int32_t l_1387 = 0x43B06A76L;
        int32_t l_1388 = 1L;
        int32_t ***l_1430 = (void*)0;
        const int32_t ** const **l_1440 = &l_1439;
        uint16_t ****l_1448 = &l_1445;
        uint16_t ***l_1450 = (void*)0;
        uint16_t ****l_1449 = &l_1450;
        int64_t *l_1455 = &g_95;
        int32_t l_1469[7][9] = {{0x167F2B85L,0x5835FBBBL,1L,1L,0x3DF75BBBL,(-1L),(-1L),0x3DF75BBBL,1L},{0L,(-4L),0L,1L,(-1L),5L,1L,0L,1L},{(-3L),0L,0x3DF75BBBL,1L,0L,0L,0L,1L,0x3DF75BBBL},{1L,1L,(-4L),1L,(-3L),0L,0x3DF75BBBL,1L,0L},{5L,1L,0L,1L,0xF174ECF9L,0xF174ECF9L,1L,0L,1L},{0xF174ECF9L,0L,(-4L),0x167F2B85L,0x5835FBBBL,1L,1L,0x3DF75BBBL,(-1L)},{1L,5L,0x3DF75BBBL,1L,0x3BD990A3L,1L,0x3DF75BBBL,5L,1L}};
        float ****l_1493[8][3] = {{&g_401[2],&g_401[1],&g_401[4]},{&g_401[7],&g_401[4],&g_401[4]},{&g_401[2],&g_401[1],&g_401[4]},{&g_401[7],&g_401[4],&g_401[4]},{&g_401[2],&g_401[1],&g_401[4]},{&g_401[7],&g_401[4],&g_401[4]},{&g_401[2],&g_401[1],&g_401[4]},{&g_401[7],&g_401[4],&g_401[4]}};
        int16_t *l_1500[5][5][4] = {{{(void*)0,&g_641[4],&g_191[5],(void*)0},{&g_641[0],&g_191[0],&g_641[0],&g_641[0]},{&g_641[3],&g_191[5],&g_641[0],&g_191[6]},{(void*)0,&g_191[5],&g_191[5],&g_191[5]},{&g_191[5],&g_191[1],&g_191[5],&g_641[3]}},{{(void*)0,(void*)0,&g_641[0],&g_191[5]},{&g_641[3],&g_33,&g_641[0],&g_191[5]},{&g_641[0],&g_191[5],&g_191[5],(void*)0},{(void*)0,&g_191[5],&g_641[0],&g_641[0]},{&g_191[5],&g_638[3][0],(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_641[1],(void*)0},{(void*)0,&g_191[1],&g_191[5],&g_191[5]},{&g_641[0],&g_638[6][0],&g_641[0],&g_191[5]},{&g_641[4],&g_638[6][0],&g_641[0],&g_191[5]},{&g_638[6][0],&g_191[1],(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_638[6][0],(void*)0},{&g_641[3],&g_638[3][0],&g_191[6],&g_641[0]},{&g_641[0],&g_191[5],&g_641[1],(void*)0},{&g_191[5],&g_191[5],&g_191[5],&g_641[0]},{&g_638[0][2],&g_191[5],&g_33,&g_191[0]}},{{&g_638[6][0],&g_191[5],(void*)0,&g_33},{(void*)0,(void*)0,&g_33,&g_191[6]},{(void*)0,&g_638[0][2],(void*)0,&g_191[5]},{&g_638[6][0],&g_191[6],&g_33,&g_191[5]},{&g_638[0][2],&g_638[1][2],&g_191[0],&g_191[5]}}};
        int32_t *l_1563 = &g_266;
        int32_t **l_1562 = &l_1563;
        int32_t **l_1567 = &l_1563;
        float l_1581 = 0x9.180159p-25;
        uint32_t *l_1633 = &g_267;
        uint32_t **l_1632 = &l_1633;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1345[i] = 65535UL;
        if (l_1344)
        { /* block id: 663 */
            int8_t l_1348 = 0xB0L;
            int32_t *l_1349 = (void*)0;
            int32_t *l_1350 = &l_1344;
            int32_t *l_1351 = &g_214;
            int32_t *l_1352 = &g_637;
            int32_t *l_1353 = (void*)0;
            int32_t l_1354 = (-1L);
            int32_t *l_1355[10];
            int i;
            for (i = 0; i < 10; i++)
                l_1355[i] = &l_1354;
            --l_1345[0];
            --l_1359;
            if (l_1357)
                break;
        }
        else
        { /* block id: 667 */
            uint64_t l_1369 = 0xBDE947838F9B2F39LL;
            int8_t * const l_1371 = &g_209;
            int32_t l_1382 = 0L;
            int32_t l_1383 = 0x206023CAL;
            int32_t l_1385[10] = {0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL,0x48E2693FL};
            uint32_t l_1389 = 0UL;
            int16_t l_1429[6] = {0xF00EL,0xF00EL,0xDF5DL,0xF00EL,0xF00EL,0xDF5DL};
            int i;
            for (g_266 = 0; (g_266 <= 7); g_266 += 1)
            { /* block id: 670 */
                uint64_t *l_1378 = &l_1359;
                float *l_1379[7][8] = {{(void*)0,&g_84[5][6][0],&g_265,&g_265,&g_84[5][6][0],(void*)0,&g_84[5][6][0],&g_265},{&g_265,&g_84[5][6][0],&g_265,(void*)0,(void*)0,&g_265,&g_84[5][6][0],&g_265},{(void*)0,(void*)0,&g_265,(void*)0,(void*)0,(void*)0,(void*)0,&g_265},{(void*)0,(void*)0,(void*)0,&g_265,(void*)0,(void*)0,(void*)0,(void*)0},{&g_265,(void*)0,(void*)0,&g_265,&g_84[5][6][0],&g_265,(void*)0,(void*)0},{(void*)0,&g_84[5][6][0],&g_265,&g_265,&g_84[5][6][0],(void*)0,&g_84[5][6][0],&g_265},{&g_265,&g_84[5][6][0],&g_265,(void*)0,(void*)0,&g_265,&g_84[5][6][0],&g_265}};
                int32_t *l_1381[9][5][5] = {{{(void*)0,(void*)0,(void*)0,&l_1357,(void*)0},{&l_1358,&l_1356[0],&l_1357,&l_1358,(void*)0},{(void*)0,(void*)0,&g_5,&l_1358,&l_1380},{&g_637,&l_1356[0],(void*)0,&l_1380,&l_1358},{&g_637,(void*)0,&g_637,(void*)0,&g_5}},{{&g_637,&l_1380,&l_1358,(void*)0,&g_637},{(void*)0,&l_1344,&g_637,&l_1344,&l_1358},{(void*)0,&l_1344,&l_1344,(void*)0,&l_1358},{(void*)0,&g_637,(void*)0,&g_637,&l_1358},{&l_1356[4],&g_637,&l_1380,&l_1358,&g_5}},{{&l_1356[2],&l_1344,&l_1358,&l_1344,&l_1380},{&g_74,&l_1344,&l_1344,&g_637,(void*)0},{&l_1356[0],&l_1380,&g_214,&g_637,&l_1344},{(void*)0,(void*)0,&g_214,&l_1356[1],(void*)0},{&l_1357,&l_1356[0],(void*)0,&g_214,&l_1358}},{{&l_1380,(void*)0,&l_1380,(void*)0,&l_1380},{&l_1356[0],&l_1356[0],&l_1356[0],&l_1380,(void*)0},{&g_637,(void*)0,&l_1358,(void*)0,&g_637},{&g_637,&l_1356[4],(void*)0,(void*)0,&l_1380},{(void*)0,&l_1344,&l_1357,&l_1358,(void*)0}},{{&l_1380,&l_1344,&l_1358,&l_1344,&l_1356[2]},{&l_1344,&l_1357,&l_1358,&l_1356[0],&g_5},{&l_1357,&l_1344,&g_5,&l_1356[4],&l_1358},{&g_637,(void*)0,&g_5,&g_637,&g_5},{(void*)0,&g_74,&g_5,(void*)0,&g_74}},{{(void*)0,(void*)0,&l_1358,(void*)0,(void*)0},{(void*)0,&g_214,&g_214,&l_1344,&g_214},{(void*)0,&l_1356[0],&g_214,(void*)0,&l_1356[2]},{(void*)0,&g_5,&l_1358,&g_214,&g_5},{(void*)0,(void*)0,&g_5,&l_1344,&g_214}},{{&l_1344,(void*)0,&g_5,&l_1344,&g_74},{&g_637,(void*)0,&g_5,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1358,(void*)0,&g_637},{(void*)0,&g_214,&l_1358,&l_1358,&g_214},{&g_5,&l_1358,&l_1357,&g_5,&g_5}},{{(void*)0,&l_1357,(void*)0,&g_214,(void*)0},{&g_637,(void*)0,&l_1356[2],(void*)0,(void*)0},{(void*)0,&g_637,&l_1358,(void*)0,&g_5},{&g_5,(void*)0,(void*)0,&g_214,&l_1356[1]},{(void*)0,&l_1358,&l_1380,&l_1356[0],&l_1358}},{{(void*)0,(void*)0,&g_214,(void*)0,&g_214},{&g_637,&l_1380,(void*)0,&l_1344,&g_5},{&l_1344,&l_1356[0],&l_1344,&l_1356[0],(void*)0},{(void*)0,(void*)0,&l_1356[2],&g_637,&l_1344},{(void*)0,&l_1356[0],&g_637,(void*)0,&l_1380}}};
                int i, j, k;
                l_1357 ^= ((l_1380 = (((l_1344 = ((**g_69) = (safe_mul_func_float_f_f(0x5.1BB334p-62, ((((safe_lshift_func_uint16_t_u_s(((((((safe_div_func_int64_t_s_s((((((*l_1378) = ((+(((*g_512) |= l_1369) || (~(((l_1371 != (void*)0) & (safe_mul_func_uint8_t_u_u(((*p_45) = (((l_1356[2] = 0x3115L) >= 0x8E67L) && (safe_add_func_uint8_t_u_u((l_1358 < (*g_1277)), (g_860 ^= ((*l_1371) = (safe_mod_func_uint8_t_u_u((*p_45), l_1344)))))))), g_725[3]))) || (*p_45))))) ^ l_1369)) & l_1369) == (**g_503)) ^ (-7L)), l_1358)) | g_643[6]) >= (*g_504)) >= 0xF12DL) <= g_683[2][2][2]) && l_1359), 2)) , (void*)0) == (void*)0) > l_1369))))) <= 0xF.7F0B9Bp-54) <= 0x2.0p+1)) , 0xD51271ABL);
                l_1389--;
                for (g_1275 = 1; (g_1275 <= 5); g_1275 += 1)
                { /* block id: 684 */
                    for (l_1389 = 0; (l_1389 <= 5); l_1389 += 1)
                    { /* block id: 687 */
                        int32_t l_1392[9] = {3L,3L,3L,3L,3L,3L,3L,3L,3L};
                        int i;
                        if (l_1392[1])
                            break;
                    }
                }
            }
            for (g_121 = 0; (g_121 <= 5); g_121 += 1)
            { /* block id: 694 */
                const int16_t *l_1407 = &g_191[2];
                const uint32_t *l_1415 = &g_1416;
                const uint32_t **l_1414 = &l_1415;
                int16_t *l_1426 = &g_641[0];
                int32_t l_1427 = 0x338DA9D9L;
                int8_t l_1428[9][4] = {{0x2DL,0x80L,(-2L),0x45L},{0x2DL,(-2L),0x2DL,(-7L)},{0x80L,0x45L,(-7L),(-7L)},{(-2L),(-2L),0x90L,0x45L},{0x45L,0x80L,0x90L,0x80L},{(-2L),0x2DL,(-7L),0x90L},{0x80L,0x2DL,0x2DL,0x80L},{0x2DL,0x80L,(-2L),0x45L},{0x2DL,(-2L),0x2DL,(-7L)}};
                int i, j;
                l_1388 |= ((((*l_1371) = (safe_lshift_func_int16_t_s_u((safe_add_func_int16_t_s_s((l_1397 == ((safe_sub_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u((safe_div_func_uint16_t_u_u((l_1428[6][3] ^= (((safe_add_func_int32_t_s_s(((*g_512) = (((!(l_1407 != (void*)0)) > (safe_mod_func_uint16_t_u_u(l_1386, 0xE444L))) , 0x545A5FCBL)), (((safe_add_func_uint8_t_u_u((safe_add_func_int32_t_s_s(((&g_267 != ((*l_1414) = &l_1389)) ^ ((safe_mul_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((((safe_mod_func_int16_t_s_s(((((!((*l_1426) &= ((safe_div_func_int64_t_s_s((**g_1152), 18446744073709551615UL)) || l_1345[0]))) , (*g_1141)) != l_1384) || l_1382), 0x1D71L)) <= l_1383) | l_1344), l_1385[0])), l_1357)) >= l_1384)), (*g_504))), 0x83L)) , l_1356[5]) || l_1427))) != l_1359) > 0x0F49F51FA9CD7337LL)), 0xA0E7L)), l_1429[4])), g_1138[0][0][1])) , &g_638[3][0])), l_1344)), l_1429[1]))) , (void*)0) == l_1430);
                return l_1431;
            }
        }
        (*g_512) = (safe_mod_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(l_1436, (safe_sub_func_int8_t_s_s((((*l_1440) = l_1439) != &g_740), (((safe_mul_func_uint8_t_u_u(((((safe_mod_func_int8_t_s_s(((((*l_1448) = l_1445) == ((*l_1449) = &l_1446[0][2])) , (safe_mod_func_int32_t_s_s((safe_mod_func_int64_t_s_s(((*l_1455) = ((void*)0 != (*l_1448))), (safe_mod_func_uint16_t_u_u(((((void*)0 == l_1458) , l_1356[6]) ^ l_1380), 0x613BL)))), l_1356[7]))), (*p_45))) , l_1436) , (void*)0) != (void*)0), 0xA6L)) & (*p_45)) ^ 18446744073709551615UL))))), (*p_45)));
        for (g_1290 = 0; (g_1290 <= 4); g_1290 += 1)
        { /* block id: 711 */
            int32_t * const  volatile * volatile ** volatile *l_1464 = &g_1460[0];
            int32_t l_1466 = 0x1027ADFBL;
            int32_t l_1467 = 1L;
            int32_t l_1468 = 1L;
            int32_t l_1470 = 6L;
            int32_t l_1471[5][7][2] = {{{0xADE9415EL,(-8L)},{(-8L),0xADE9415EL},{(-8L),(-8L)},{0xADE9415EL,(-8L)},{(-8L),0xADE9415EL},{(-8L),(-8L)},{0xADE9415EL,(-8L)}},{{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L}},{{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL}},{{1L,0xADE9415EL},{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL}},{{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L},{0xADE9415EL,0xADE9415EL},{1L,0xADE9415EL},{0xADE9415EL,1L}}};
            float **l_1531[8] = {&g_70[4],(void*)0,&g_70[4],(void*)0,&g_70[4],(void*)0,&g_70[4],(void*)0};
            uint8_t ***l_1534 = (void*)0;
            float l_1539 = 0x1.8p-1;
            uint32_t l_1546 = 0UL;
            int i, j, k;
            (*l_1464) = g_1460[1];
            l_1387 &= (*****l_1464);
            for (g_746 = 1; (g_746 <= 4); g_746 += 1)
            { /* block id: 716 */
                int32_t *l_1465[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_1465[i] = (void*)0;
                l_1472--;
                if ((***l_1439))
                    break;
            }
            if ((***l_1439))
                break;
            for (g_746 = 0; (g_746 <= 4); g_746 += 1)
            { /* block id: 723 */
                int16_t l_1477 = 1L;
                int32_t l_1478 = 0x1C744F2BL;
                const uint64_t *l_1479[4] = {&g_683[4][2][1],&g_683[4][2][1],&g_683[4][2][1],&g_683[4][2][1]};
                int8_t *l_1484 = &g_860;
                uint32_t *l_1494 = &g_101;
                const float * const ****l_1499 = &g_1495;
                float **l_1533 = &g_70[4];
                int32_t l_1542[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_1542[i] = (-1L);
                l_1478 = (safe_sub_func_int16_t_s_s(0x8D2CL, (l_1477 , ((*l_1447) = ((*p_45) >= (***l_1439))))));
                if ((((void*)0 != l_1479[1]) <= l_1477))
                { /* block id: 726 */
                    if ((****l_1440))
                        break;
                    return l_1431;
                }
                else
                { /* block id: 729 */
                    int i, j, k;
                    (**g_69) = g_84[g_785][(g_1290 + 2)][(g_746 + 1)];
                    if ((*****l_1464))
                        break;
                }
                if ((safe_rshift_func_uint8_t_u_s((*g_1277), (safe_add_func_int8_t_s_s(((*l_1484) |= g_252[1][1][4]), ((safe_mod_func_int8_t_s_s(((***l_1439) , (****l_1440)), (safe_mod_func_uint64_t_u_u(0xB77C1B3037E761EBLL, ((safe_rshift_func_uint16_t_u_s((safe_div_func_int32_t_s_s(((l_1493[6][1] == ((*l_1499) = ((((*l_1455) = (***l_1439)) < (((*l_1494) = (*g_504)) != (8L >= 0x21L))) , g_1495))) >= 0x66L), (**g_503))), (***l_1439))) | (***l_1439)))))) <= (***l_1439)))))))
                { /* block id: 737 */
                    int i;
                    if (((g_70[g_1290] != (void*)0) != l_1477))
                    { /* block id: 738 */
                        return l_1500[0][0][0];
                    }
                    else
                    { /* block id: 740 */
                        (**g_1462) |= ((void*)0 == (****l_1464));
                    }
                    g_1501 = (void*)0;
                }
                else
                { /* block id: 744 */
                    int32_t l_1510 = 0x8F03B8B8L;
                    float **l_1532 = &g_70[3];
                    int32_t l_1541 = (-6L);
                    int32_t l_1543 = 0x95FD2AB9L;
                    int32_t l_1544 = 0xCC9D40B6L;
                    int16_t l_1545[4];
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1545[i] = 6L;
                    (*****l_1464) = ((safe_mul_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((+(safe_lshift_func_int16_t_s_u(((safe_unary_minus_func_uint8_t_u((((l_1510 || (l_1477 , ((void*)0 != (*g_167)))) == (safe_rshift_func_int16_t_s_u(((safe_sub_func_int64_t_s_s((*g_1153), (safe_mul_func_uint8_t_u_u((!(((*g_504) , l_1510) == 0x33L)), (safe_add_func_uint16_t_u_u((!(((l_1477 ^ (****l_1440)) || l_1478) >= 4294967295UL)), 6L)))))) && (*****l_1464)), 10))) , (*p_45)))) >= (***l_1439)), l_1521))), g_121)), 0x41L)) | (***l_1439));
                    l_1510 = ((safe_sub_func_int64_t_s_s(l_1477, (((((((safe_add_func_float_f_f((safe_add_func_float_f_f(l_1510, (***l_1439))), 0xC.56C1E6p-33)) == (-0x1.2p+1)) < (safe_add_func_float_f_f(0x6.4p-1, ((***l_1439) <= (-(((((((l_1532 = l_1531[4]) != (l_1533 = l_1533)) , (*****l_1464)) >= (***l_1439)) , l_1534) == g_1535) != 0x2.38E93Ap-43)))))) , (**g_1152)) != g_683[2][2][2]) , (***l_1439)) != l_1467))) , 0x2.4CF9B7p+52);
                    for (l_1472 = 0; (l_1472 <= 1); l_1472 += 1)
                    { /* block id: 751 */
                        int32_t *l_1540[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        ++l_1546;
                        (*g_1549) = (****l_1464);
                    }
                }
            }
        }
        for (g_101 = 0; (g_101 <= 5); g_101 += 1)
        { /* block id: 760 */
            int32_t ***l_1564 = (void*)0;
            int32_t ***l_1565[6];
            int32_t l_1571 = 1L;
            int8_t *l_1572 = &g_209;
            const uint16_t *l_1577 = &g_121;
            int16_t l_1578[6][10][2] = {{{0L,1L},{0xE08EL,0x5665L},{0x5665L,(-6L)},{0x155FL,(-1L)},{0L,0L},{(-1L),0xEEB4L},{(-2L),0xEEB4L},{(-1L),0L},{0L,(-1L)},{0x155FL,(-6L)}},{{0x5665L,0x5665L},{0xE08EL,1L},{0L,(-2L)},{0x666FL,1L},{1L,0x666FL},{1L,0x9A40L},{1L,0x666FL},{1L,1L},{0x666FL,(-2L)},{0L,1L}},{{0xE08EL,0x5665L},{0x5665L,(-6L)},{0x155FL,(-1L)},{0L,0L},{(-1L),0xEEB4L},{(-2L),0xEEB4L},{(-1L),0L},{0L,(-1L)},{0x155FL,(-6L)},{0x5665L,0x5665L}},{{0xE08EL,1L},{0L,(-2L)},{0x666FL,1L},{1L,0x666FL},{1L,0x9A40L},{1L,0x666FL},{1L,1L},{0x666FL,(-2L)},{0L,1L},{0xE08EL,0x5665L}},{{0x5665L,(-6L)},{0x155FL,(-1L)},{0L,0L},{(-1L),0xEEB4L},{(-2L),0xEEB4L},{(-1L),0L},{0L,(-1L)},{0x155FL,(-6L)},{0x5665L,0x5665L},{0xE08EL,1L}},{{0L,(-2L)},{0x666FL,1L},{1L,0x666FL},{1L,0x9A40L},{1L,0x666FL},{1L,1L},{0x666FL,(-2L)},{0L,1L},{0xE08EL,0x5665L},{0x5665L,0x5665L}}};
            int8_t *l_1579 = (void*)0;
            int8_t *l_1580[5][5] = {{&g_860,&g_860,&g_860,&g_860,&g_860},{(void*)0,&g_860,&g_860,(void*)0,(void*)0},{&g_860,&g_860,&g_860,&g_860,&g_860},{(void*)0,&g_860,&g_860,(void*)0,&g_860},{(void*)0,(void*)0,&g_860,&g_860,(void*)0}};
            float *l_1589[9] = {&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0],&g_84[1][6][0]};
            const float *l_1590[3];
            int32_t l_1592 = (-4L);
            const uint32_t ***l_1612[2][1][1];
            uint16_t ***l_1621 = &l_1446[0][1];
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_1565[i] = (void*)0;
            for (i = 0; i < 3; i++)
                l_1590[i] = &g_64;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_1612[i][j][k] = &g_503;
                }
            }
            (*g_512) ^= (safe_lshift_func_int8_t_s_s(6L, ((****l_1440) & (5UL > (safe_lshift_func_uint8_t_u_s(((safe_add_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u(((((safe_add_func_int8_t_s_s((l_1571 = ((safe_rshift_func_int16_t_s_s(((g_1566 = l_1562) == l_1567), (safe_div_func_uint32_t_u_u((0x14L || (((safe_unary_minus_func_uint16_t_u(l_1571)) , ((*l_1572) = 0x3EL)) != ((safe_lshift_func_int16_t_s_u((safe_sub_func_int32_t_s_s((l_1577 == (*g_167)), 1UL)), 8)) > 0x40L))), l_1578[2][5][1])))) != 0x0A91L)), g_191[5])) < (-1L)) != l_1578[0][1][0]) > l_1578[2][2][1]), l_1578[2][5][1])) && l_1578[2][5][1]), 0x35CC7790L)) < (-1L)), 3))))));
            l_1592 &= (safe_add_func_int64_t_s_s((((((*g_69) = l_1584) != (l_1591 = ((((((((((safe_lshift_func_int8_t_s_s((****l_1440), 2)) , 0x6.5802F3p-74) != ((****l_1440) != ((l_1589[7] = l_1584) == l_1590[0]))) , ((**g_1462) |= 1L)) <= ((*l_1450) != &l_1577)) == l_1571) > 0L) == l_1578[2][5][1]) , (**g_1152)) , l_1584))) , 0xFEL) , 0xBB9BE89467797AD9LL), l_1578[2][5][1]));
            for (l_1592 = 5; (l_1592 >= 0); l_1592 -= 1)
            { /* block id: 772 */
                uint8_t l_1599 = 0xB2L;
                int32_t *l_1602 = (void*)0;
                uint32_t ***l_1611 = (void*)0;
                int64_t l_1627 = 0xAFE53785438877C7LL;
                if ((***g_1461))
                    break;
                for (l_1521 = 0; (l_1521 <= 5); l_1521 += 1)
                { /* block id: 776 */
                    int32_t *l_1597 = &g_74;
                    int32_t *l_1598 = (void*)0;
                    (*g_512) &= (safe_rshift_func_int16_t_s_u(0x66AFL, (safe_lshift_func_uint8_t_u_u(0x28L, 4))));
                    l_1599++;
                }
            }
        }
    }
    return l_1447;
}


/* ------------------------------------------ */
/* 
 * reads : g_33 g_234 g_172 g_641 g_683 g_252 g_512 g_214 g_74
 * writes: g_33 g_234 g_172 g_683 g_214 g_74
 */
static uint8_t * func_46(uint8_t * p_47, const uint8_t * p_48, const int64_t  p_49)
{ /* block id: 7 */
    const uint8_t *l_59 = &g_21[0];
    float *l_83 = &g_84[5][6][0];
    int32_t l_301 = (-1L);
    float ***l_399 = &g_69;
    int32_t l_413 = 0L;
    float l_432 = 0x2.2CD79Fp+25;
    uint16_t *l_483 = &g_114;
    uint32_t l_553[3][8] = {{0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL},{0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL},{0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL,0x1FEB3588L,4294967295UL}};
    int32_t l_562 = 0xAD54A63EL;
    int32_t l_563 = 0xF29FB610L;
    int32_t *l_582 = &l_413;
    uint16_t l_649 = 0x3C03L;
    int16_t **l_689 = &g_576[1][8][0];
    int8_t l_701 = (-9L);
    int32_t l_731 = 0x7690E8A0L;
    uint8_t l_843[6] = {0x91L,0xA5L,0x91L,0x91L,0xA5L,0x91L};
    int16_t l_945 = 0x59CAL;
    int32_t l_946 = 0x10702B9BL;
    uint32_t **l_1041 = (void*)0;
    uint32_t ***l_1040[8][4] = {{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041},{&l_1041,&l_1041,&l_1041,&l_1041}};
    int32_t **l_1296[7][3] = {{(void*)0,(void*)0,(void*)0},{&g_512,&g_512,&g_512},{(void*)0,(void*)0,(void*)0},{&g_512,&g_512,&g_512},{(void*)0,(void*)0,(void*)0},{&g_512,&g_512,&g_512},{(void*)0,(void*)0,(void*)0}};
    uint32_t l_1328 = 0x8076D3A5L;
    int16_t l_1343 = (-1L);
    int i, j;
lbl_980:
    for (g_33 = (-20); (g_33 != 7); g_33++)
    { /* block id: 10 */
        float l_58[8][9] = {{0x5.970102p+48,(-0x4.4p-1),0x4.AAC465p+50,0x0.Fp+1,0x4.AAC465p+50,(-0x4.4p-1),0x5.970102p+48,0x4.AAC465p+50,0x8.63D476p+77},{0x7.60F2F4p+34,0x3.8p+1,0x0.7p+1,0x0.0p-1,0x8.63074Cp+10,0x0.7p+1,(-0x10.Ap+1),0x0.7p+1,0x8.63074Cp+10},{0xE.D293E3p+30,0x4.AAC465p+50,0x4.AAC465p+50,0xE.D293E3p+30,(-0x1.Dp+1),0xA.B9CA24p+56,(-0x10.1p-1),0x4.AAC465p+50,0xA.B9CA24p+56},{0x1.F91351p+77,0x8.63074Cp+10,0x6.E0DAEEp+68,0x0.0p-1,0x5.D6221Fp+87,0x5.D6221Fp+87,0x0.0p-1,0x6.E0DAEEp+68,0x8.63074Cp+10},{(-0x10.1p-1),(-0x1.Dp+1),0x8.63D476p+77,0x0.Fp+1,(-0x1.Dp+1),0x4.AAC465p+50,0x2.98A7B9p+29,0x8.63D476p+77,0x8.63D476p+77},{0x1.F91351p+77,0x5.D6221Fp+87,0x8.63074Cp+10,(-0x1.Bp-1),0x8.63074Cp+10,0x5.D6221Fp+87,0x1.F91351p+77,0x8.63074Cp+10,0x6.E0DAEEp+68},{0xE.D293E3p+30,(-0x1.Dp+1),0xA.B9CA24p+56,(-0x10.1p-1),0x4.AAC465p+50,0xA.B9CA24p+56,0x2.98A7B9p+29,0xA.B9CA24p+56,0x4.AAC465p+50},{0x7.60F2F4p+34,0x8.63074Cp+10,0x8.63074Cp+10,0x7.60F2F4p+34,0x3.8p+1,0x0.7p+1,0x0.0p-1,0x8.63074Cp+10,0x0.7p+1}};
        int32_t l_302[4] = {0x8100EF0EL,0x8100EF0EL,0x8100EF0EL,0x8100EF0EL};
        int16_t *l_319 = &g_33;
        float *** const l_364 = &g_69;
        int8_t *l_404 = &g_209;
        float *** const *l_465 = &l_364;
        int64_t *l_550[5][8][6] = {{{&g_148,&g_174,&g_174,&g_148,&g_148,&g_95},{&g_174,(void*)0,&g_95,(void*)0,&g_95,&g_148},{&g_174,&g_95,(void*)0,&g_148,&g_95,&g_148},{&g_148,&g_148,&g_148,&g_148,(void*)0,&g_148},{&g_174,&g_174,&g_174,(void*)0,(void*)0,&g_174},{&g_174,&g_95,&g_95,&g_148,&g_95,&g_95},{&g_148,(void*)0,&g_148,&g_148,&g_174,(void*)0},{&g_174,&g_95,&g_148,(void*)0,&g_174,&g_148}},{{&g_174,&g_95,&g_148,&g_148,&g_95,&g_174},{&g_148,&g_174,&g_174,&g_148,&g_148,&g_95},{&g_174,(void*)0,&g_95,(void*)0,&g_95,&g_148},{&g_174,&g_95,(void*)0,&g_148,&g_95,&g_148},{&g_148,&g_148,&g_148,&g_148,(void*)0,&g_148},{&g_174,&g_174,&g_174,(void*)0,(void*)0,&g_174},{&g_174,&g_95,&g_95,&g_148,&g_95,&g_95},{&g_148,(void*)0,&g_148,&g_148,&g_174,(void*)0}},{{&g_174,&g_95,&g_148,(void*)0,&g_174,&g_148},{&g_174,&g_95,&g_148,&g_148,&g_95,&g_174},{&g_148,&g_174,&g_174,&g_148,&g_148,&g_95},{&g_174,(void*)0,&g_95,(void*)0,&g_95,&g_148},{&g_174,&g_95,(void*)0,&g_148,&g_95,&g_148},{&g_148,&g_148,&g_148,&g_148,(void*)0,&g_148},{&g_174,&g_174,&g_174,(void*)0,(void*)0,&g_174},{&g_174,&g_95,&g_95,&g_148,&g_95,&g_95}},{{&g_148,(void*)0,&g_148,&g_148,&g_174,(void*)0},{&g_174,&g_95,&g_148,(void*)0,&g_174,&g_148},{&g_174,&g_95,&g_148,&g_148,&g_95,&g_174},{&g_148,&g_174,&g_174,&g_148,&g_148,&g_95},{&g_174,(void*)0,&g_95,(void*)0,&g_95,&g_148},{&g_174,&g_95,(void*)0,&g_148,&g_95,&g_148},{&g_148,&g_148,&g_148,&g_148,(void*)0,&g_148},{&g_174,&g_174,&g_148,&g_148,&g_148,&g_95}},{{&g_148,&g_148,&g_148,(void*)0,&g_148,(void*)0},{&g_148,&g_148,&g_95,(void*)0,&g_174,&g_148},{&g_148,&g_148,&g_148,&g_148,&g_174,&g_95},{&g_148,&g_95,(void*)0,(void*)0,&g_95,&g_148},{&g_148,&g_174,&g_95,(void*)0,&g_95,&g_148},{&g_148,&g_148,(void*)0,&g_148,&g_148,&g_95},{&g_148,(void*)0,&g_148,(void*)0,(void*)0,&g_148},{&g_148,&g_95,&g_95,(void*)0,&g_148,(void*)0}}};
        int32_t **l_557 = &g_512;
        int32_t l_560 = 0x3FD91E4EL;
        int32_t l_580 = 0xBB0ECB0DL;
        int32_t *l_581 = &l_413;
        int32_t l_602 = 0x05479616L;
        float l_619 = (-0x1.Fp+1);
        int8_t *l_648[5];
        const uint32_t l_719 = 0x1FE83338L;
        uint32_t l_793 = 0x49704FE0L;
        float *l_809 = &g_84[2][1][1];
        int32_t l_825 = 0xD32559EDL;
        uint32_t l_929 = 4294967287UL;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_648[i] = &g_209;
    }
    for (g_234 = (-6); (g_234 < 26); g_234++)
    { /* block id: 445 */
        int16_t l_974[2][4];
        float *l_975 = &g_84[5][1][5];
        float *l_976 = (void*)0;
        int16_t *l_977 = &g_641[2];
        int32_t l_982 = 8L;
        int32_t l_983 = 0L;
        int32_t l_1017 = (-6L);
        int32_t l_1021 = (-10L);
        int32_t **l_1039 = &l_582;
        uint32_t ****l_1053 = &l_1040[6][2];
        uint32_t *****l_1052 = &l_1053;
        float ** const **l_1070 = (void*)0;
        float * const *l_1075 = &l_975;
        float * const ** const l_1074[3][2] = {{&l_1075,&l_1075},{&l_1075,&l_1075},{&l_1075,&l_1075}};
        int16_t l_1158 = 1L;
        int i, j;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 4; j++)
                l_974[i][j] = 0x0EE0L;
        }
    }
    for (g_172 = 0; (g_172 <= 5); g_172 += 1)
    { /* block id: 585 */
        const float l_1183 = (-0x8.Fp+1);
        int32_t l_1189 = 0xB74C3AC9L;
        int32_t l_1201 = 0xE49A9F16L;
        int32_t l_1202 = 7L;
        uint64_t *l_1203 = (void*)0;
        uint64_t *l_1204 = &g_683[5][2][2];
        uint16_t **l_1221 = &l_483;
        int64_t *l_1252 = &g_148;
        int64_t **l_1251[4][2] = {{&l_1252,&l_1252},{&l_1252,&l_1252},{&l_1252,&l_1252},{&l_1252,&l_1252}};
        int32_t l_1289[4] = {6L,6L,6L,6L};
        float l_1342 = 0x6.261B6Dp+80;
        int i, j;
        if (p_49)
            break;
        (*g_512) |= ((p_49 ^ (((safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u(g_641[2], 5)), ((+((l_1189 = l_1189) , (safe_add_func_uint32_t_u_u((safe_div_func_uint16_t_u_u((p_49 | l_1189), 65532UL)), (safe_mod_func_int64_t_s_s(p_49, ((*l_1204) ^= (~(g_172 , (safe_mul_func_int16_t_s_s((l_1202 = (safe_div_func_int8_t_s_s((l_1201 = (18446744073709551611UL >= l_1189)), (*l_582)))), l_1189))))))))))) , (*l_582)))) , g_252[3][1][1]) > 255UL)) || 0xD3AEBB17L);
        for (g_214 = 5; (g_214 >= 0); g_214 -= 1)
        { /* block id: 594 */
            if (g_172)
                goto lbl_980;
            return p_47;
        }
        for (g_74 = 5; (g_74 >= 0); g_74 -= 1)
        { /* block id: 600 */
            float l_1206 = 0xF.62AAE6p+72;
            int32_t l_1212 = (-1L);
            int32_t l_1213[9][7][4] = {{{0xB635FE96L,0x4079B37BL,0x74FF3A87L,0xEEA2DA5FL},{0L,(-8L),(-8L),0xEEA2DA5FL},{(-1L),0x4079B37BL,(-3L),(-3L)},{0x62CBC22AL,0x87779DB4L,0L,(-8L)},{0L,0x1834C28FL,1L,0x60B3709DL},{1L,0xEAD026B9L,0x87779DB4L,(-8L)},{(-10L),0x62CBC22AL,(-10L),0x32233205L}},{{0xFFD75ED6L,1L,0x62CBC22AL,0L},{0xB4D2CD7DL,0xF5D96886L,0x60B3709DL,1L},{0xC4AA14E2L,0x74FF3A87L,0x60B3709DL,0xB4D2CD7DL},{0xB4D2CD7DL,0xB7C5F6ACL,0x62CBC22AL,0xB635FE96L},{0xFFD75ED6L,(-1L),(-10L),0xE9C50C30L},{(-10L),0xE9C50C30L,0x87779DB4L,0xB7C5F6ACL},{1L,0xC4AA14E2L,1L,1L}},{{0L,0L,0L,(-1L)},{0x62CBC22AL,0x32233205L,(-3L),0x87779DB4L},{(-1L),0xB635FE96L,(-8L),(-3L)},{0L,0xB635FE96L,0x74FF3A87L,0x87779DB4L},{0xB635FE96L,0x32233205L,0xC4AA14E2L,(-1L)},{(-4L),0L,0x1834C28FL,1L},{(-8L),0xC4AA14E2L,(-8L),0xB7C5F6ACL}},{{0x1834C28FL,0xE9C50C30L,0xFFD75ED6L,0xE9C50C30L},{(-3L),(-1L),0xB7C5F6ACL,0xB635FE96L},{0L,0xB7C5F6ACL,0xEAD026B9L,0xB4D2CD7DL},{0xE9C50C30L,0x74FF3A87L,0x732BF405L,1L},{0xE9C50C30L,0xF5D96886L,0xEAD026B9L,0L},{0L,1L,0xB7C5F6ACL,0x32233205L},{(-3L),0x62CBC22AL,0xFFD75ED6L,(-8L)}},{{0x1834C28FL,0xEAD026B9L,(-8L),0x60B3709DL},{(-8L),0x1834C28FL,0x1834C28FL,(-8L)},{0xB7C5F6ACL,(-4L),1L,1L},{(-8L),0xFFD75ED6L,0x62CBC22AL,(-3L)},{0L,0x60B3709DL,0x1834C28FL,(-3L)},{0L,0xFFD75ED6L,(-6L),1L},{(-8L),(-4L),0L,0x1834C28FL}},{{0L,0x4079B37BL,0x87779DB4L,0x732BF405L},{0xE9C50C30L,0xB635FE96L,(-4L),0x60B3709DL},{0xB4D2CD7DL,(-8L),0xB4D2CD7DL,0x74FF3A87L},{0xEAD026B9L,0xE9C50C30L,(-8L),0L},{(-3L),0x32233205L,0x732BF405L,0xE9C50C30L},{1L,0x62CBC22AL,0x732BF405L,(-3L)},{(-3L),0xEEA2DA5FL,(-8L),(-8L)}},{{0xEAD026B9L,0L,0xB4D2CD7DL,0xC4AA14E2L},{0xB4D2CD7DL,0xC4AA14E2L,(-4L),0xEEA2DA5FL},{0xE9C50C30L,1L,0x87779DB4L,0x87779DB4L},{0L,0L,0L,0L},{(-8L),0x74FF3A87L,(-6L),(-4L)},{0L,(-8L),0x1834C28FL,(-6L)},{0L,(-8L),0x62CBC22AL,(-4L)}},{{(-8L),0x74FF3A87L,1L,0L},{0xB7C5F6ACL,0L,0x4079B37BL,0x87779DB4L},{0x1834C28FL,1L,0x60B3709DL,0xEEA2DA5FL},{0x4079B37BL,0xC4AA14E2L,0xEAD026B9L,0xC4AA14E2L},{(-6L),0L,0xEEA2DA5FL,(-8L)},{0L,0xEEA2DA5FL,0xB635FE96L,(-3L)},{0xC4AA14E2L,0x62CBC22AL,(-1L),0xE9C50C30L}},{{0xC4AA14E2L,0x32233205L,0xB635FE96L,0L},{0L,0xE9C50C30L,0xEEA2DA5FL,0x74FF3A87L},{(-6L),(-8L),0xEAD026B9L,0x60B3709DL},{0x4079B37BL,0xB635FE96L,0x60B3709DL,0x732BF405L},{0x1834C28FL,0x4079B37BL,0x4079B37BL,0x1834C28FL},{0xB7C5F6ACL,(-4L),1L,1L},{(-8L),0xFFD75ED6L,0x62CBC22AL,(-3L)}}};
            uint16_t **l_1223 = &l_483;
            uint16_t l_1227 = 0xA1D4L;
            int32_t l_1249[2];
            uint64_t l_1281 = 1UL;
            uint32_t *l_1338[1];
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1249[i] = 0L;
            for (i = 0; i < 1; i++)
                l_1338[i] = (void*)0;
        }
    }
    return p_47;
}


/* ------------------------------------------ */
/* 
 * reads : g_65 g_75
 * writes: g_65 g_75
 */
static uint16_t  func_53(uint8_t * p_54)
{ /* block id: 20 */
    uint32_t l_78 = 1UL;
    for (g_65 = 29; (g_65 <= 6); --g_65)
    { /* block id: 23 */
        int32_t *l_73[8][4][6] = {{{&g_74,&g_74,&g_74,&g_74,&g_74,&g_5},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{(void*)0,&g_74,&g_5,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74}},{{&g_74,&g_74,&g_74,&g_74,(void*)0,&g_5},{(void*)0,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_5,&g_74,(void*)0,&g_74}},{{&g_74,&g_74,&g_74,&g_74,&g_74,&g_5},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{(void*)0,&g_74,&g_5,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74}},{{&g_74,&g_74,&g_74,&g_74,(void*)0,&g_5},{(void*)0,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_5,&g_74,(void*)0,&g_74}},{{&g_74,&g_74,&g_74,&g_74,&g_74,&g_5},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{(void*)0,&g_74,&g_5,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74}},{{&g_74,&g_74,&g_74,&g_74,(void*)0,&g_5},{(void*)0,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_5,&g_74,(void*)0,&g_74}},{{&g_74,&g_74,&g_74,&g_74,&g_74,&g_5},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{(void*)0,&g_74,&g_5,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74}},{{&g_74,&g_74,&g_74,&g_74,(void*)0,&g_5},{(void*)0,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_5,&g_74,(void*)0,&g_74}}};
        int i, j, k;
        --g_75;
    }
    return l_78;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_64 g_65 g_69
 */
static uint8_t * func_55(uint32_t  p_56, int8_t  p_57)
{ /* block id: 11 */
    uint8_t l_62 = 0x67L;
    float *l_67 = (void*)0;
    float **l_66 = &l_67;
    for (p_57 = (-4); (p_57 >= (-7)); p_57--)
    { /* block id: 14 */
        float *l_63 = &g_64;
        float ***l_68[10][8] = {{(void*)0,&l_66,(void*)0,(void*)0,&l_66,(void*)0,(void*)0,&l_66},{&l_66,(void*)0,(void*)0,&l_66,(void*)0,(void*)0,&l_66,(void*)0},{&l_66,&l_66,&l_66,&l_66,&l_66,&l_66,&l_66,&l_66},{(void*)0,&l_66,(void*)0,(void*)0,&l_66,(void*)0,(void*)0,&l_66},{&l_66,(void*)0,(void*)0,&l_66,(void*)0,(void*)0,&l_66,(void*)0},{&l_66,&l_66,&l_66,&l_66,&l_66,&l_66,&l_66,&l_66},{(void*)0,&l_66,(void*)0,(void*)0,&l_66,(void*)0,(void*)0,&l_66},{&l_66,&l_66,&l_66,(void*)0,&l_66,&l_66,(void*)0,&l_66},{(void*)0,(void*)0,&l_66,(void*)0,(void*)0,&l_66,(void*)0,(void*)0},{&l_66,(void*)0,&l_66,&l_66,(void*)0,&l_66,&l_66,(void*)0}};
        int i, j;
        g_65 = ((*l_63) = l_62);
        g_69 = l_66;
    }
    return &g_21[4];
}


/* ------------------------------------------ */
/* 
 * reads : g_21 g_5 g_33 g_74 g_101 g_95 g_84 g_167 g_121 g_174 g_114 g_148 g_210 g_209 g_214 g_234 g_172 g_252 g_267 g_296 g_299
 * writes: g_95 g_74 g_101 g_114 g_121 g_70 g_148 g_172 g_174 g_191 g_84 g_209 g_214 g_211 g_234 g_252 g_267 g_296 g_299
 */
static uint16_t  func_79(uint64_t  p_80, float * p_81, uint32_t  p_82)
{ /* block id: 27 */
    int32_t l_89 = 0xB6F065A9L;
    float **l_118 = (void*)0;
    uint16_t *l_120 = &g_121;
    uint32_t l_126 = 0x56E00778L;
    int64_t *l_183 = &g_148;
    int8_t *l_208 = &g_209;
    int32_t l_255 = 0x9B082438L;
    if ((g_21[1] >= ((safe_add_func_uint8_t_u_u(p_80, g_5)) <= (p_82 && (p_80 >= l_89)))))
    { /* block id: 28 */
        uint8_t l_90[10] = {0x03L,0x03L,0x03L,0x03L,0x03L,0x03L,0x03L,0x03L,0x03L,0x03L};
        uint16_t *l_93[1][2];
        int64_t *l_94 = &g_95;
        int32_t *l_96 = &g_74;
        int32_t l_97[3];
        int32_t *l_98 = (void*)0;
        int32_t *l_99 = &l_89;
        int32_t *l_100[4];
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_93[i][j] = (void*)0;
        }
        for (i = 0; i < 3; i++)
            l_97[i] = (-1L);
        for (i = 0; i < 4; i++)
            l_100[i] = &l_97[0];
        (*l_96) = ((((g_33 != 0UL) <= (((*l_94) = ((l_90[0] | ((safe_rshift_func_int8_t_s_u(((l_89 |= (g_33 , ((g_21[2] || g_21[0]) ^ l_90[7]))) , g_5), l_90[0])) , g_74)) < g_21[5])) > g_33)) || 0x0BL) > 0x07L);
        g_101++;
    }
    else
    { /* block id: 33 */
        int32_t l_125 = 6L;
        float *l_137 = (void*)0;
        uint16_t **l_166 = &l_120;
        for (g_95 = (-14); (g_95 > 28); ++g_95)
        { /* block id: 36 */
            uint8_t l_106 = 255UL;
            uint16_t *l_113 = &g_114;
            uint64_t l_119 = 0x2F89F941FD73A523LL;
            uint16_t **l_122 = (void*)0;
            uint16_t **l_123[8];
            uint16_t *l_124[2];
            float *l_139 = &g_84[5][6][0];
            int32_t l_270 = 0x3A589725L;
            int32_t l_295[6];
            int i;
            for (i = 0; i < 8; i++)
                l_123[i] = &l_120;
            for (i = 0; i < 2; i++)
                l_124[i] = &g_121;
            for (i = 0; i < 6; i++)
                l_295[i] = 0xE164FB72L;
            if (((((g_5 & l_106) < (safe_sub_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((0xEAL < (0xF3D3L < (g_121 = (safe_mul_func_uint16_t_u_u(((*l_113) = (p_80 , 0x2265L)), ((safe_add_func_int16_t_s_s(((l_124[0] = ((((safe_unary_minus_func_uint32_t_u((((g_33 && ((void*)0 != l_118)) >= l_106) >= l_106))) >= l_119) | 0xB0B60EEEL) , l_120)) == l_120), g_101)) && p_82)))))), 65531UL)), l_125))) == l_126) != 0xDA225054L))
            { /* block id: 40 */
                return l_125;
            }
            else
            { /* block id: 42 */
                int8_t l_185 = (-1L);
                int32_t l_215 = 0xB7180226L;
                if (g_74)
                { /* block id: 43 */
                    int8_t l_131 = 8L;
                    int64_t *l_147 = &g_148;
                    uint8_t *l_157 = &l_106;
                    int32_t *l_170 = &g_74;
                    uint8_t *l_171 = &g_172;
                    int64_t *l_173 = &g_174;
                    for (p_80 = 0; (p_80 == 10); ++p_80)
                    { /* block id: 46 */
                        uint32_t *l_136[9][9] = {{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101}};
                        float **l_140[1];
                        int32_t *l_141 = &l_125;
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_140[i] = &l_139;
                        (*l_141) |= (((p_82 != (p_80 ^ l_131)) > (safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(p_80, 0)), ((*l_113) = (g_21[4] ^ (g_101 = 1UL)))))) == (l_137 == ((~p_82) , (l_139 = (g_70[4] = ((((0xBBL < (-1L)) , (*p_81)) >= 0x9.8743DCp+37) , l_139))))));
                        if (p_82)
                            break;
                        return p_80;
                    }
                    if ((safe_lshift_func_uint16_t_u_u((safe_unary_minus_func_int64_t_s(((*l_173) |= (safe_sub_func_int64_t_s_s(((*l_147) = l_131), (safe_sub_func_int8_t_s_s(((!9L) & (safe_mod_func_uint32_t_u_u(((~g_95) == (6UL <= (l_125 , (((safe_mul_func_uint8_t_u_u(((*l_157) = p_80), ((*l_171) = (safe_add_func_uint16_t_u_u(((safe_sub_func_int64_t_s_s(((((p_80 || ((((safe_rshift_func_int8_t_s_u((safe_div_func_int32_t_s_s(((*l_170) = (l_166 == g_167)), p_82)), 2)) , g_21[4]) ^ l_125) , 0xA55BL)) , g_5) , l_89) & g_121), 18446744073709551612UL)) , 0x416DL), p_80))))) && l_125) , 0xFA80559AC6BCD61ELL)))), l_89))), 4L))))))), 10)))
                    { /* block id: 60 */
                        int32_t **l_175 = &l_170;
                        int32_t *l_176 = &l_89;
                        (*l_175) = &g_5;
                        (*l_176) &= g_114;
                        if ((**l_175))
                            break;
                        if (p_82)
                            continue;
                    }
                    else
                    { /* block id: 65 */
                        int8_t l_184 = 0xD8L;
                        int16_t *l_190 = &g_191[5];
                        int8_t *l_192 = &l_131;
                        (*p_81) = (((((safe_mul_func_int8_t_s_s(g_5, l_106)) , p_81) == (void*)0) != (safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((void*)0 != l_183) , l_184), ((l_185 != (((*l_192) = (((*l_170) |= (safe_div_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(((((*l_190) = (-3L)) >= 0x5699L) , g_95), p_82)), l_125))) , 0x1CL)) != g_95)) < (-2L)))), 6UL))) , l_184);
                    }
                }
                else
                { /* block id: 71 */
                    uint64_t *l_207[8] = {&l_119,&l_119,&l_119,&l_119,&l_119,&l_119,&l_119,&l_119};
                    uint8_t *l_212 = &g_172;
                    int32_t *l_213[3][5];
                    int i, j;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_213[i][j] = &l_125;
                    }
                    if (g_148)
                        break;
                    g_214 |= (g_101 < ((((safe_lshift_func_uint8_t_u_u(((*l_212) = (((((*l_208) |= (safe_mod_func_uint16_t_u_u((((((safe_unary_minus_func_uint64_t_u(l_89)) , (0xA0D4L ^ (((p_82 || ((((((p_80 = (safe_mul_func_uint8_t_u_u(254UL, (safe_mod_func_int64_t_s_s(0xB5A98973432FECA5LL, (safe_add_func_uint8_t_u_u(g_148, ((!l_119) != ((safe_sub_func_uint8_t_u_u((p_82 > 4294967286UL), g_121)) , l_185))))))))) > (-10L)) ^ p_82) | p_82) , l_208) == (void*)0)) > (-1L)) == l_106))) <= p_82) , g_210) != (void*)0), 3L))) > g_21[3]) && p_80) ^ g_74)), 3)) != g_114) | 0x9F26L) > g_21[4]));
                    l_215 = 0x79D215A4L;
                    (*g_210) = p_81;
                }
                for (g_114 = 5; (g_114 >= 36); g_114 = safe_add_func_int16_t_s_s(g_114, 7))
                { /* block id: 82 */
                    for (l_125 = 0; (l_125 >= 15); l_125 = safe_add_func_uint16_t_u_u(l_125, 8))
                    { /* block id: 85 */
                        return g_21[5];
                    }
                }
            }
            for (p_80 = 15; (p_80 <= 15); p_80++)
            { /* block id: 92 */
                int32_t *l_222 = &l_89;
                int32_t *l_223 = &g_214;
                int32_t *l_224 = &l_125;
                int32_t *l_225 = &l_125;
                int32_t *l_226 = (void*)0;
                int32_t *l_227 = &g_74;
                int32_t *l_228 = &l_89;
                int32_t *l_229 = &l_125;
                int32_t *l_230 = &l_89;
                int32_t *l_231 = &l_89;
                int32_t l_232 = (-7L);
                int32_t *l_233[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_233[i] = &g_74;
                (*g_210) = (void*)0;
                (*l_222) &= (-3L);
                --g_234;
            }
            for (p_80 = (-23); (p_80 > 31); ++p_80)
            { /* block id: 99 */
                float l_253[3][4] = {{0xC.91CB4Cp+50,0xE.296A02p-80,0xE.296A02p-80,0xC.91CB4Cp+50},{0xE.296A02p-80,0xC.91CB4Cp+50,0xE.296A02p-80,0xE.296A02p-80},{0xC.91CB4Cp+50,0xC.91CB4Cp+50,(-0x10.2p+1),0xC.91CB4Cp+50}};
                int32_t l_263 = 0x7286EC0BL;
                int32_t l_264 = 0xA4172F5BL;
                int32_t *l_271 = &l_270;
                int32_t l_293 = 0x841EF775L;
                int32_t l_294[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
                int i, j;
                for (g_101 = 0; (g_101 <= 1); g_101 += 1)
                { /* block id: 102 */
                    uint8_t *l_244[2];
                    uint32_t *l_251 = &g_252[3][1][1];
                    int16_t *l_254[10];
                    int32_t l_262 = (-1L);
                    int i;
                    for (i = 0; i < 2; i++)
                        l_244[i] = (void*)0;
                    for (i = 0; i < 10; i++)
                        l_254[i] = &g_191[5];
                    if ((safe_mod_func_int16_t_s_s(l_89, (l_255 &= (l_126 ^ ((*l_251) &= (safe_unary_minus_func_uint8_t_u((safe_div_func_uint8_t_u_u((g_172 ^= 0x5EL), (safe_add_func_int8_t_s_s(((65533UL > (((0x47L || 0x2EL) && (safe_sub_func_int64_t_s_s(0x24C425AE9B15D643LL, l_119))) <= ((safe_rshift_func_uint16_t_u_u(l_126, p_82)) >= 5UL))) != p_82), (-1L)))))))))))))
                    { /* block id: 106 */
                        int32_t *l_256 = (void*)0;
                        int32_t *l_257 = &l_255;
                        int32_t *l_258 = &g_214;
                        int32_t *l_259 = (void*)0;
                        int32_t *l_260 = (void*)0;
                        int32_t *l_261[6][10][1] = {{{&g_5},{&g_214},{&l_255},{&l_255},{&g_214},{&g_5},{&l_89},{&g_5},{&g_214},{&l_255}},{{&l_255},{&g_214},{&g_5},{&l_89},{&g_5},{&g_214},{&l_255},{&l_255},{&g_214},{&g_5}},{{&l_89},{&g_5},{&g_214},{&l_255},{&l_255},{&g_214},{&g_5},{&l_89},{&g_5},{&g_214}},{{&l_255},{&l_255},{&g_214},{&g_5},{&l_89},{&g_5},{&g_214},{&l_255},{&l_255},{&g_214}},{{&g_5},{&l_89},{&g_5},{&g_214},{&l_255},{&l_255},{&g_214},{&g_5},{&l_89},{&g_5}},{{&g_214},{&l_255},{&l_125},{&l_89},{&g_214},{&g_214},{&g_214},{&l_89},{&l_125},{&l_125}}};
                        int i, j, k;
                        (*g_210) = (void*)0;
                        g_267--;
                    }
                    else
                    { /* block id: 109 */
                        l_270 = l_125;
                        return g_21[4];
                    }
                    for (g_234 = 0; (g_234 <= 1); g_234 += 1)
                    { /* block id: 115 */
                        l_271 = &l_263;
                        l_271 = p_81;
                    }
                }
                for (g_172 = 0; (g_172 == 6); ++g_172)
                { /* block id: 122 */
                    uint8_t l_274[8] = {8UL,8UL,8UL,8UL,8UL,8UL,8UL,8UL};
                    int32_t *l_277 = &l_89;
                    int32_t *l_278 = &l_270;
                    int32_t *l_279 = &l_270;
                    int32_t *l_280 = &l_264;
                    int32_t *l_281 = &l_270;
                    int32_t *l_282 = (void*)0;
                    int32_t *l_283 = (void*)0;
                    int32_t *l_284 = &l_263;
                    int32_t *l_285 = &l_264;
                    int32_t *l_286 = &l_125;
                    int32_t *l_287 = &l_125;
                    int32_t *l_288 = &l_255;
                    int32_t *l_289 = (void*)0;
                    int32_t *l_290 = &l_89;
                    int32_t *l_291 = &l_270;
                    int32_t *l_292[1];
                    volatile int32_t **l_300 = &g_299;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_292[i] = &l_264;
                    --l_274[0];
                    g_296--;
                    (*l_300) = g_299;
                }
            }
        }
    }
    return g_21[4];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_21[i], "g_21[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc_bytes (&g_64, sizeof(g_64), "g_64", print_hash_value);
    transparent_crc(g_65, "g_65", print_hash_value);
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_75, "g_75", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_84[i][j][k], sizeof(g_84[i][j][k]), "g_84[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    transparent_crc(g_148, "g_148", print_hash_value);
    transparent_crc(g_169, "g_169", print_hash_value);
    transparent_crc(g_172, "g_172", print_hash_value);
    transparent_crc(g_174, "g_174", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_191[i], "g_191[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_209, "g_209", print_hash_value);
    transparent_crc(g_214, "g_214", print_hash_value);
    transparent_crc(g_234, "g_234", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_252[i][j][k], "g_252[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_265, sizeof(g_265), "g_265", print_hash_value);
    transparent_crc(g_266, "g_266", print_hash_value);
    transparent_crc(g_267, "g_267", print_hash_value);
    transparent_crc(g_296, "g_296", print_hash_value);
    transparent_crc_bytes (&g_331, sizeof(g_331), "g_331", print_hash_value);
    transparent_crc(g_333, "g_333", print_hash_value);
    transparent_crc(g_341, "g_341", print_hash_value);
    transparent_crc(g_487, "g_487", print_hash_value);
    transparent_crc(g_565, "g_565", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_566[i][j], "g_566[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_637, "g_637", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_638[i][j], "g_638[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_641[i], "g_641[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_643[i], "g_643[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_683[i][j][k], "g_683[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_725[i], "g_725[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_746, "g_746", print_hash_value);
    transparent_crc(g_785, "g_785", print_hash_value);
    transparent_crc(g_860, "g_860", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_887[i], "g_887[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_900[i], "g_900[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_911, "g_911", print_hash_value);
    transparent_crc_bytes (&g_984, sizeof(g_984), "g_984", print_hash_value);
    transparent_crc(g_985, "g_985", print_hash_value);
    transparent_crc(g_1085, "g_1085", print_hash_value);
    transparent_crc(g_1111, "g_1111", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1138[i][j][k], "g_1138[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1154[i][j], "g_1154[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1275, "g_1275", print_hash_value);
    transparent_crc(g_1290, "g_1290", print_hash_value);
    transparent_crc(g_1416, "g_1416", print_hash_value);
    transparent_crc(g_1538, "g_1538", print_hash_value);
    transparent_crc(g_1654, "g_1654", print_hash_value);
    transparent_crc(g_1795, "g_1795", print_hash_value);
    transparent_crc(g_1847, "g_1847", print_hash_value);
    transparent_crc(g_1883, "g_1883", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1982[i][j], "g_1982[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_2079[i][j][k], "g_2079[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 501
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 39
breakdown:
   depth: 1, occurrence: 139
   depth: 2, occurrence: 42
   depth: 3, occurrence: 5
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 16, occurrence: 2
   depth: 18, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 4
   depth: 22, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 30, occurrence: 1
   depth: 33, occurrence: 1
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1

XXX total number of pointers: 548

XXX times a variable address is taken: 1220
XXX times a pointer is dereferenced on RHS: 306
breakdown:
   depth: 1, occurrence: 209
   depth: 2, occurrence: 52
   depth: 3, occurrence: 31
   depth: 4, occurrence: 9
   depth: 5, occurrence: 5
XXX times a pointer is dereferenced on LHS: 301
breakdown:
   depth: 1, occurrence: 272
   depth: 2, occurrence: 21
   depth: 3, occurrence: 6
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 63
XXX times a pointer is compared with address of another variable: 11
XXX times a pointer is compared with another pointer: 15
XXX times a pointer is qualified to be dereferenced: 4952

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1028
   level: 2, occurrence: 233
   level: 3, occurrence: 137
   level: 4, occurrence: 61
   level: 5, occurrence: 34
XXX number of pointers point to pointers: 187
XXX number of pointers point to scalars: 361
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 27.9
XXX average alias set size: 1.43

XXX times a non-volatile is read: 1811
XXX times a non-volatile is write: 887
XXX times a volatile is read: 139
XXX    times read thru a pointer: 87
XXX times a volatile is write: 35
XXX    times written thru a pointer: 18
XXX times a volatile is available for access: 1.1e+03
XXX percentage of non-volatile access: 93.9

XXX forward jumps: 0
XXX backward jumps: 9

XXX stmts: 146
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 20
   depth: 2, occurrence: 30
   depth: 3, occurrence: 22
   depth: 4, occurrence: 24
   depth: 5, occurrence: 20

XXX percentage a fresh-made variable is used: 18.5
XXX percentage an existing variable is used: 81.5
********************* end of statistics **********************/


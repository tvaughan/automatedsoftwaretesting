/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      218201870
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   uint64_t  f0;
   uint32_t  f1;
   int32_t  f2;
   int8_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static const uint32_t g_5[4][3] = {{0x6210B999L,4UL,18446744073709551612UL},{0xED15DE05L,4UL,0xED15DE05L},{0xD00585B7L,0x6210B999L,18446744073709551612UL},{0xD00585B7L,0xD00585B7L,0x6210B999L}};
static int16_t g_37 = 1L;
static uint64_t g_57 = 0UL;
static uint64_t *g_56 = &g_57;
static int32_t *g_74 = (void*)0;
static int32_t g_83 = 0x698B42DCL;
static int32_t *** volatile g_88 = (void*)0;/* VOLATILE GLOBAL g_88 */
static uint16_t g_100[2] = {0xB66FL,0xB66FL};
static uint32_t g_102 = 0x4B1E4F37L;
static int32_t g_104 = 0L;
static float g_116 = 0x5.1C8BDCp-80;
static uint16_t g_125 = 6UL;
static int16_t g_129 = 0L;
static uint64_t g_131[7][8][4] = {{{0x3BE9465D988D0130LL,0x55F5DB90C34EF3EFLL,0x55F5DB90C34EF3EFLL,0x3BE9465D988D0130LL},{0xB69D60F7E9ACA868LL,1UL,1UL,18446744073709551606UL},{1UL,0x032C2DE0B6AD74ACLL,1UL,0x50AD34F8EFC5DF5ALL},{0x349BA410498C5D29LL,18446744073709551615UL,0xAFCE28766912E21ELL,0x50AD34F8EFC5DF5ALL},{1UL,0x032C2DE0B6AD74ACLL,3UL,18446744073709551606UL},{0x184417F0C4AEE0EBLL,1UL,0x15673F556C05CDAFLL,0x3BE9465D988D0130LL},{0x15C07A264E934167LL,0x55F5DB90C34EF3EFLL,2UL,0x846699B53D0E5225LL},{0xB8C27A4A205A567ALL,0x6B1ECDBA6A349BDALL,1UL,1UL}},{{0x184417F0C4AEE0EBLL,0xC92F557AD609A04ALL,0UL,0xDE22B755D73B7D96LL},{0xCB25977850406789LL,0xB25B5FE41CD94991LL,0x184417F0C4AEE0EBLL,0x15673F556C05CDAFLL},{0xC92F557AD609A04ALL,0xE71502624D59534ELL,0x846699B53D0E5225LL,1UL},{0xB36A152264B8EAE7LL,0x7010A815A6EF0A28LL,0x1574812A85F46675LL,0x4579C62F16C98A27LL},{0x4579C62F16C98A27LL,0xB69D60F7E9ACA868LL,4UL,0x6B1ECDBA6A349BDALL},{0xB25B5FE41CD94991LL,18446744073709551615UL,0xD2503B3C44AE9DA5LL,1UL},{18446744073709551610UL,18446744073709551606UL,0xCB25977850406789LL,1UL},{0x184417F0C4AEE0EBLL,0x55F5DB90C34EF3EFLL,0xB25B5FE41CD94991LL,0xB4028A08A75D01A8LL}},{{0UL,5UL,18446744073709551615UL,0x817FF2A7570BAEA4LL},{0x06A03D0BAECBAC11LL,0x50AD34F8EFC5DF5ALL,0x817FF2A7570BAEA4LL,1UL},{0xFA26B9DDBBE02B32LL,18446744073709551615UL,0UL,2UL},{0x27A07EE3387E9A70LL,0xC92F557AD609A04ALL,0x494F356F341A2B65LL,0xC92F557AD609A04ALL},{0UL,18446744073709551615UL,0xD2503B3C44AE9DA5LL,0x032C2DE0B6AD74ACLL},{0xC92F557AD609A04ALL,18446744073709551612UL,0UL,18446744073709551610UL},{18446744073709551615UL,0x06A03D0BAECBAC11LL,0x7010A815A6EF0A28LL,0x4579C62F16C98A27LL},{18446744073709551615UL,5UL,0UL,1UL}},{{0xC92F557AD609A04ALL,0x4579C62F16C98A27LL,0xD2503B3C44AE9DA5LL,3UL},{0UL,0x28E1FC50AAA13A23LL,0x494F356F341A2B65LL,0UL},{0x27A07EE3387E9A70LL,0x55F5DB90C34EF3EFLL,0UL,0x6B1ECDBA6A349BDALL},{0xFA26B9DDBBE02B32LL,4UL,0x817FF2A7570BAEA4LL,0x15C07A264E934167LL},{0x06A03D0BAECBAC11LL,0UL,18446744073709551615UL,18446744073709551610UL},{0UL,1UL,0xB25B5FE41CD94991LL,2UL},{0x184417F0C4AEE0EBLL,0xB25B5FE41CD94991LL,0xCB25977850406789LL,0x57036B52242021A1LL},{18446744073709551610UL,0xDE22B755D73B7D96LL,0xD2503B3C44AE9DA5LL,0x15673F556C05CDAFLL}},{{0xB25B5FE41CD94991LL,18446744073709551615UL,4UL,0xD73FCFB248F24B12LL},{0x4579C62F16C98A27LL,0x06A03D0BAECBAC11LL,0x1574812A85F46675LL,0x55F5DB90C34EF3EFLL},{0xB36A152264B8EAE7LL,4UL,0x846699B53D0E5225LL,18446744073709551615UL},{0xC92F557AD609A04ALL,18446744073709551615UL,0x184417F0C4AEE0EBLL,1UL},{0xCB25977850406789LL,0UL,0UL,0UL},{0x184417F0C4AEE0EBLL,0xB36A152264B8EAE7LL,18446744073709551615UL,1UL},{18446744073709551615UL,2UL,0x817FF2A7570BAEA4LL,0x817FF2A7570BAEA4LL},{0x7010A815A6EF0A28LL,0x7010A815A6EF0A28LL,0x15C07A264E934167LL,0xD73FCFB248F24B12LL}},{{0xAFCE28766912E21ELL,1UL,0UL,0xB69D60F7E9ACA868LL},{18446744073709551608UL,18446744073709551615UL,1UL,0UL},{18446744073709551610UL,18446744073709551615UL,0x184417F0C4AEE0EBLL,0xB69D60F7E9ACA868LL},{18446744073709551615UL,1UL,0xFA26B9DDBBE02B32LL,0xD73FCFB248F24B12LL},{18446744073709551615UL,0x7010A815A6EF0A28LL,0UL,0x817FF2A7570BAEA4LL},{0x55F5DB90C34EF3EFLL,2UL,0x846699B53D0E5225LL,1UL},{0xB25B5FE41CD94991LL,0xB36A152264B8EAE7LL,1UL,0UL},{0xD73FCFB248F24B12LL,0UL,0x494F356F341A2B65LL,1UL}},{{18446744073709551608UL,18446744073709551615UL,0x57036B52242021A1LL,18446744073709551615UL},{18446744073709551615UL,4UL,18446744073709551615UL,0x55F5DB90C34EF3EFLL},{0UL,0x06A03D0BAECBAC11LL,0xB36A152264B8EAE7LL,0xD73FCFB248F24B12LL},{0UL,18446744073709551615UL,18446744073709551615UL,0x15673F556C05CDAFLL},{18446744073709551608UL,0xDE22B755D73B7D96LL,1UL,0x57036B52242021A1LL},{0UL,0xB25B5FE41CD94991LL,1UL,2UL},{0xDE22B755D73B7D96LL,1UL,4UL,18446744073709551610UL},{0xB36A152264B8EAE7LL,0UL,6UL,0x7010A815A6EF0A28LL}}};
static float g_134 = 0x1.Cp+1;
static uint32_t g_135 = 0xBBFD28FEL;
static const uint16_t g_139 = 0xC76BL;
static const uint16_t *g_138 = &g_139;
static uint32_t g_157[9] = {0x76373520L,0x76373520L,0x76373520L,0x76373520L,0x76373520L,0x76373520L,0x76373520L,0x76373520L,0x76373520L};
static int32_t g_208 = 0x0A73095CL;
static volatile uint64_t g_218 = 0x17C74455C9E55F8CLL;/* VOLATILE GLOBAL g_218 */
static volatile uint64_t * const g_217 = &g_218;
static int32_t * volatile g_232 = &g_83;/* VOLATILE GLOBAL g_232 */
static int32_t * volatile * volatile g_233[4][10] = {{&g_74,&g_232,&g_232,(void*)0,&g_232,&g_232,&g_74,&g_232,&g_74,&g_232},{&g_232,&g_74,&g_74,&g_232,&g_74,&g_74,&g_232,&g_74,&g_74,&g_232},{(void*)0,&g_74,&g_74,&g_74,(void*)0,&g_232,&g_74,&g_232,(void*)0,&g_74},{&g_74,&g_232,&g_74,&g_74,(void*)0,&g_232,&g_232,&g_232,&g_232,&g_232}};
static int32_t * volatile * volatile g_234[10] = {&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232};
static union U0 g_240 = {0x172C5786C627A455LL};
static uint64_t g_299 = 0x2A38A6AAD285BDA2LL;
static float * volatile g_301[3] = {&g_134,&g_134,&g_134};
static uint8_t g_315 = 255UL;
static float * volatile g_344 = &g_134;/* VOLATILE GLOBAL g_344 */
static const uint32_t g_367 = 0xEA414231L;
static float * volatile g_385 = &g_116;/* VOLATILE GLOBAL g_385 */
static int32_t * volatile g_408 = &g_104;/* VOLATILE GLOBAL g_408 */
static uint32_t g_441 = 0x4CBD5D16L;
static uint32_t *g_446 = &g_102;
static uint32_t * volatile *g_445 = &g_446;
static uint32_t g_447 = 0x98998222L;
static int32_t * volatile g_448[10][1][5] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_83,(void*)0,&g_83,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_83,(void*)0,&g_83,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_83,(void*)0,&g_83,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_83,(void*)0,&g_83,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_83,(void*)0,&g_83,(void*)0}}};
static int64_t g_477[5] = {(-2L),(-2L),(-2L),(-2L),(-2L)};
static uint8_t g_486[6][1] = {{1UL},{1UL},{0UL},{1UL},{1UL},{0UL}};
static int32_t ** volatile g_492 = (void*)0;/* VOLATILE GLOBAL g_492 */
static int32_t ** volatile g_493 = &g_74;/* VOLATILE GLOBAL g_493 */
static int8_t * volatile g_496[1] = {(void*)0};
static int8_t *g_497 = &g_240.f3;
static uint16_t *g_499 = &g_100[1];
static uint16_t **g_498 = &g_499;
static int64_t g_528 = 1L;
static volatile int64_t g_553 = 0x1AE49C8437A68381LL;/* VOLATILE GLOBAL g_553 */
static volatile int64_t *g_552 = &g_553;
static volatile int64_t **g_551 = &g_552;
static int32_t **g_562 = &g_74;
static int32_t ***g_561 = &g_562;
static int32_t ****g_560[1][3] = {{&g_561,&g_561,&g_561}};
static int32_t ***** volatile g_559 = &g_560[0][2];/* VOLATILE GLOBAL g_559 */
static volatile uint32_t g_575 = 0xD3F1EBDFL;/* VOLATILE GLOBAL g_575 */
static volatile uint32_t *g_574 = &g_575;
static uint16_t g_595 = 0x4DDAL;
static uint32_t * volatile *** volatile g_599 = (void*)0;/* VOLATILE GLOBAL g_599 */
static uint32_t * volatile **g_601 = &g_445;
static uint32_t * volatile *** volatile g_600 = &g_601;/* VOLATILE GLOBAL g_600 */
static float * volatile g_645 = &g_134;/* VOLATILE GLOBAL g_645 */
static int64_t g_667 = 3L;
static float * const *g_697[3] = {(void*)0,(void*)0,(void*)0};
static float * const ** volatile g_696[2] = {&g_697[1],&g_697[1]};
static float * const ** volatile g_699[1] = {&g_697[1]};
static int8_t g_731 = 1L;
static volatile uint16_t *g_736 = (void*)0;
static volatile uint16_t **g_735 = &g_736;
static volatile uint16_t *** volatile g_734 = &g_735;/* VOLATILE GLOBAL g_734 */
static volatile uint16_t *** volatile *g_733[7][2] = {{&g_734,(void*)0},{&g_734,&g_734},{(void*)0,&g_734},{(void*)0,&g_734},{(void*)0,&g_734},{&g_734,(void*)0},{&g_734,(void*)0}};
static volatile uint8_t g_884[8] = {255UL,255UL,255UL,255UL,255UL,255UL,255UL,255UL};
static int32_t * const g_890 = (void*)0;
static int32_t ** const  volatile g_915[8] = {&g_74,(void*)0,&g_74,(void*)0,&g_74,(void*)0,&g_74,(void*)0};
static int32_t ** volatile g_916 = &g_74;/* VOLATILE GLOBAL g_916 */
static int32_t * volatile * volatile g_975 = &g_408;/* VOLATILE GLOBAL g_975 */
static int16_t g_1047 = 0xBE24L;
static int16_t * volatile g_1112 = &g_37;/* VOLATILE GLOBAL g_1112 */
static int16_t * volatile *g_1111 = &g_1112;
static uint16_t g_1119 = 0x664CL;
static int8_t g_1217 = 0x76L;
static volatile int32_t g_1219[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static int32_t * volatile g_1234 = &g_104;/* VOLATILE GLOBAL g_1234 */
static int32_t ** volatile g_1297 = &g_74;/* VOLATILE GLOBAL g_1297 */
static float * volatile g_1303 = &g_116;/* VOLATILE GLOBAL g_1303 */
static uint64_t g_1347 = 0x309EABE9BB8BFE43LL;
static int16_t *g_1405 = &g_37;
static int16_t **g_1404 = &g_1405;
static int16_t ** const *g_1403 = &g_1404;
static int32_t *g_1431 = &g_208;
static int32_t **g_1430 = &g_1431;
static int32_t *** volatile g_1429[1][2] = {{&g_1430,&g_1430}};
static int32_t g_1468 = 0xB49A283FL;
static const uint8_t g_1510 = 0x4AL;
static int32_t ** const  volatile g_1511 = &g_74;/* VOLATILE GLOBAL g_1511 */
static uint8_t g_1538 = 0UL;
static union U0 *g_1541 = &g_240;
static union U0 ** volatile g_1540 = &g_1541;/* VOLATILE GLOBAL g_1540 */
static int32_t * const  volatile g_1551 = &g_83;/* VOLATILE GLOBAL g_1551 */
static uint32_t * volatile * volatile *g_1652 = &g_445;
static float g_1704[1][7][7] = {{{0x1.1p-1,0x3.44845Ep-16,(-0x2.Bp-1),(-0x3.Fp-1),0x9.9p-1,0xF.F9D896p+40,0x1.Dp+1},{(-0x1.0p-1),0x1.Fp+1,0x1.A2B0C4p-49,0x1.948502p+51,0x1.8p+1,(-0x10.9p+1),0x1.8p+1},{(-0x3.Fp-1),0x1.A2B0C4p-49,0x1.A2B0C4p-49,0x1.1p-1,(-0x2.Bp-1),0x1.948502p+51,0x3.44845Ep-16},{0x1.948502p+51,0x1.A2B0C4p-49,0x1.Fp+1,(-0x1.0p-1),0xF.F9D896p+40,(-0x2.Bp-1),0x1.Dp+1},{0x3.44845Ep-16,0x5.E372B6p-60,0x1.948502p+51,(-0x3.Fp-1),0x1.948502p+51,0x5.E372B6p-60,0x3.44845Ep-16},{0x1.8p+1,(-0x3.Fp-1),0x5.E372B6p-60,0x1.Dp+1,0x1.948502p+51,0x5.29F4D6p-94,0x1.A2B0C4p-49},{0x1.Dp+1,0x5.29F4D6p-94,(-0x1.0p-1),0x1.948502p+51,0xF.F9D896p+40,0xF.F9D896p+40,0x1.948502p+51}}};
static float *g_1783 = &g_116;
static int8_t g_1889 = (-7L);
static int32_t *****g_1960 = &g_560[0][2];
static uint32_t * const *g_1962 = &g_446;
static uint32_t * const **g_1961[2][3][1] = {{{&g_1962},{&g_1962},{&g_1962}},{{&g_1962},{&g_1962},{&g_1962}}};
static uint16_t ****g_1990 = (void*)0;
static volatile int32_t g_2043 = 0x2737F46BL;/* VOLATILE GLOBAL g_2043 */
static volatile uint64_t g_2052[7][5][2] = {{{0x465758E027387FCFLL,0x30019CF6ACE8A5C4LL},{0x749EA498E5541F57LL,1UL},{18446744073709551615UL,1UL},{0UL,1UL},{18446744073709551615UL,1UL}},{{0x749EA498E5541F57LL,0x30019CF6ACE8A5C4LL},{0x465758E027387FCFLL,0x465758E027387FCFLL},{1UL,0UL},{0UL,18446744073709551614UL},{0xA14999C9003C6805LL,0x423862C717497482LL}},{{0xE1DBE54A2CCCBEF2LL,0xA14999C9003C6805LL},{0x47B8499D63E074B8LL,18446744073709551615UL},{0x47B8499D63E074B8LL,0xA14999C9003C6805LL},{0xE1DBE54A2CCCBEF2LL,0x423862C717497482LL},{0xA14999C9003C6805LL,18446744073709551614UL}},{{0UL,0UL},{1UL,0x465758E027387FCFLL},{0x465758E027387FCFLL,0x30019CF6ACE8A5C4LL},{0x749EA498E5541F57LL,1UL},{18446744073709551615UL,1UL}},{{0UL,1UL},{18446744073709551615UL,1UL},{0x749EA498E5541F57LL,0x30019CF6ACE8A5C4LL},{0x465758E027387FCFLL,0x465758E027387FCFLL},{1UL,0UL}},{{0UL,18446744073709551614UL},{0xA14999C9003C6805LL,0x423862C717497482LL},{0xE1DBE54A2CCCBEF2LL,0xA14999C9003C6805LL},{0x47B8499D63E074B8LL,18446744073709551615UL},{0x47B8499D63E074B8LL,0xA14999C9003C6805LL}},{{0xE1DBE54A2CCCBEF2LL,0x423862C717497482LL},{0xA14999C9003C6805LL,18446744073709551614UL},{0UL,0UL},{1UL,0x465758E027387FCFLL},{0x465758E027387FCFLL,0x30019CF6ACE8A5C4LL}}};
static uint32_t ****g_2187 = (void*)0;
static int32_t g_2189 = 0x536B3397L;
static int32_t g_2217 = 0xDEE9F124L;
static int32_t * volatile * volatile g_2223 = &g_232;/* VOLATILE GLOBAL g_2223 */
static uint16_t g_2392[9] = {0x3584L,0x3584L,0x3584L,0x3584L,0x3584L,0x3584L,0x3584L,0x3584L,0x3584L};
static uint64_t g_2426 = 18446744073709551615UL;
static volatile uint8_t *g_2619[10] = {&g_884[0],&g_884[0],&g_884[0],&g_884[0],&g_884[0],&g_884[0],&g_884[0],&g_884[0],&g_884[0],&g_884[0]};
static volatile uint8_t **g_2618 = &g_2619[9];
static uint8_t g_2643[6][10][2] = {{{252UL,0xDAL},{0xDFL,6UL},{0xDAL,0x93L},{0xCCL,0xDAL},{0x5EL,252UL},{252UL,0x7FL},{0xCCL,6UL},{0x7FL,6UL},{0xCCL,0x7FL},{252UL,252UL}},{{0x5EL,0xDAL},{0xCCL,0x93L},{0xDAL,6UL},{0xDFL,0xDAL},{252UL,0x5EL},{252UL,0xDAL},{0xDFL,6UL},{0xDAL,0x93L},{0xCCL,0xDAL},{0x5EL,252UL}},{{252UL,0x7FL},{0xCCL,6UL},{0x7FL,6UL},{0xCCL,0x7FL},{252UL,252UL},{0x5EL,0xDAL},{0xCCL,0x93L},{0xDAL,6UL},{0xDFL,0xDAL},{252UL,0x5EL}},{{252UL,0xDAL},{0x1DL,0x5EL},{0xDFL,1UL},{0x13L,0xDFL},{0xF3L,0x7FL},{0x7FL,0xE3L},{0x13L,0x5EL},{0xE3L,0x5EL},{0x13L,0xE3L},{0x7FL,0x7FL}},{{0xF3L,0xDFL},{0x13L,1UL},{0xDFL,0x5EL},{0x1DL,0xDFL},{0x7FL,0xF3L},{0x7FL,0xDFL},{0x1DL,0x5EL},{0xDFL,1UL},{0x13L,0xDFL},{0xF3L,0x7FL}},{{0x7FL,0xE3L},{0x13L,0x5EL},{0xE3L,0x5EL},{0x13L,0xE3L},{0x7FL,0x7FL},{0xF3L,0xDFL},{0x13L,1UL},{0xDFL,0x5EL},{0x1DL,0xDFL},{0x7FL,0xF3L}}};
static int8_t g_2658[6][7][6] = {{{(-1L),(-1L),(-7L),1L,(-4L),0x3DL},{(-1L),0x73L,0xFFL,0xF8L,0xD8L,0xAAL},{0xAAL,(-1L),(-4L),(-7L),0x4BL,(-1L)},{0x3DL,1L,0x4BL,3L,(-7L),(-1L)},{0x34L,(-9L),(-1L),0L,(-6L),0xFFL},{9L,(-1L),0xD1L,0x34L,0xC3L,0L},{0L,7L,(-1L),0xF8L,0L,6L}},{{0xB9L,0x89L,0L,0xA1L,0xAAL,0L},{0x13L,0L,(-1L),(-1L),0L,1L},{0xFFL,2L,0L,0xA6L,(-1L),(-1L)},{0xC3L,0x71L,(-10L),0x71L,0xC3L,0xACL},{(-7L),(-6L),0L,0xAAL,0x69L,(-1L)},{0xA7L,(-10L),0xF8L,(-6L),(-1L),(-1L)},{(-5L),1L,0L,0L,0xA1L,0xACL}},{{(-1L),0x82L,(-10L),6L,0x09L,(-1L)},{0x53L,(-1L),0L,0xC9L,(-4L),1L},{0L,(-7L),(-1L),0L,(-10L),0L},{0x1CL,0x69L,0L,1L,(-1L),6L},{0x1CL,(-4L),(-1L),(-10L),0xE1L,0L},{(-1L),0x10L,0xD1L,0xFEL,0xE1L,0xFFL},{0xC8L,0x56L,(-1L),0xD6L,0x4AL,(-1L)}},{{9L,0L,0x4BL,0xAAL,0x89L,(-1L)},{(-1L),0xACL,(-1L),0x23L,(-4L),0x1EL},{(-7L),0x5BL,0L,0L,(-8L),(-1L)},{0L,(-5L),0L,(-1L),0x71L,9L},{0x56L,0x10L,0L,(-2L),0x5BL,7L},{2L,(-1L),1L,0x84L,0xC3L,3L},{0x0CL,0L,(-1L),1L,(-2L),(-7L)}},{{0xE1L,0x34L,6L,0L,0x10L,0xAAL},{9L,0x13L,(-9L),2L,0xC8L,0L},{0x71L,(-8L),8L,0x10L,0xFFL,0xFFL},{0x84L,0x23L,0x23L,0x84L,0x53L,(-1L)},{3L,0x3DL,3L,0L,(-5L),0xA3L},{0L,(-1L),(-1L),(-9L),(-5L),0xD6L},{0L,0x3DL,(-9L),(-6L),0x53L,1L}},{{0x5BL,0x23L,0x09L,0xAAL,0xFFL,0x4BL},{0L,(-8L),0L,0x1EL,0xC8L,(-10L)},{0xA6L,0x13L,0x84L,(-1L),0x10L,6L},{0x64L,0x34L,0x8FL,(-9L),(-2L),1L},{1L,0L,(-1L),(-1L),0xC3L,1L},{0x73L,(-1L),0xE1L,0x8FL,0x5BL,(-6L)},{0x01L,0x10L,0xB9L,3L,0x71L,(-1L)}}};
static int32_t g_2664 = 6L;
static uint32_t *****g_2679 = &g_2187;
static volatile union U0 g_2691 = {0x42C3A19758B460CCLL};/* VOLATILE GLOBAL g_2691 */
static uint32_t * const ** const *g_2711 = &g_1961[1][2][0];
static uint32_t * const ** const **g_2710[7] = {&g_2711,&g_2711,&g_2711,&g_2711,&g_2711,&g_2711,&g_2711};
static int32_t *g_2724 = &g_2664;
static int32_t ** volatile g_2723 = &g_2724;/* VOLATILE GLOBAL g_2723 */
static const int32_t *g_2761 = &g_104;
static const int32_t ** volatile g_2760 = &g_2761;/* VOLATILE GLOBAL g_2760 */
static int16_t g_2794 = 0L;
static int8_t g_2809 = 1L;
static int32_t ** const *g_2845 = &g_1430;
static int32_t ** const * volatile *g_2844[3][1] = {{&g_2845},{&g_2845},{&g_2845}};
static uint32_t g_2871 = 0x9362B389L;
static int64_t *g_2916 = &g_667;
static int64_t **g_2915 = &g_2916;
static int64_t ** const *g_2914 = &g_2915;
static const uint32_t g_2934 = 1UL;
static const uint32_t *g_2933 = &g_2934;
static const int64_t *g_2938 = (void*)0;
static const int64_t **g_2937 = &g_2938;
static const int64_t ***g_2936 = &g_2937;
static const int64_t ****g_2935 = &g_2936;
static volatile float g_2945 = 0x9.69E931p+95;/* VOLATILE GLOBAL g_2945 */
static int8_t **g_3002 = &g_497;
static uint16_t g_3046 = 0x5182L;
static int64_t ***g_3061 = &g_2915;
static int64_t ****g_3060 = &g_3061;
static int32_t g_3101[4] = {0L,0L,0L,0L};
static int32_t * volatile * const  volatile g_3106 = (void*)0;/* VOLATILE GLOBAL g_3106 */
static int32_t * volatile * volatile g_3108 = (void*)0;/* VOLATILE GLOBAL g_3108 */
static int32_t ** const  volatile g_3153 = &g_74;/* VOLATILE GLOBAL g_3153 */
static int32_t *g_3207 = &g_1468;
static float g_3251[3][1][2] = {{{0x3.Dp+1,0xF.166294p-69}},{{0xF.166294p-69,0x3.Dp+1}},{{0xF.166294p-69,0xF.166294p-69}}};
static volatile int16_t **** volatile g_3391 = (void*)0;/* VOLATILE GLOBAL g_3391 */
static int16_t ** const **g_3394 = &g_1403;
static uint8_t g_3411 = 0x74L;
static volatile uint64_t g_3535 = 0UL;/* VOLATILE GLOBAL g_3535 */
static volatile uint64_t *g_3534 = &g_3535;
static volatile uint64_t **g_3533 = &g_3534;
static union U0 ** volatile g_3553 = &g_1541;/* VOLATILE GLOBAL g_3553 */
static uint32_t g_3678 = 0x5EEDAAB0L;


/* --- FORWARD DECLARATIONS --- */
static const float  func_1(void);
static int32_t  func_2(const uint32_t  p_3, uint32_t  p_4);
static uint16_t  func_18(float  p_19, int16_t  p_20, uint64_t  p_21, uint8_t  p_22, uint8_t  p_23);
static float  func_26(uint64_t  p_27, int8_t  p_28, uint32_t  p_29, uint64_t  p_30);
static uint64_t * func_40(uint64_t * p_41, uint64_t * const  p_42, float  p_43, uint64_t * p_44);
static uint64_t * func_45(float  p_46, uint16_t  p_47, uint64_t * p_48, uint64_t * p_49, uint32_t  p_50);
static float  func_53(uint64_t * p_54, uint32_t  p_55);
static int32_t * const  func_60(int16_t  p_61, const uint16_t  p_62, int64_t  p_63);
static uint64_t * func_64(uint64_t * p_65, uint64_t * p_66, int32_t  p_67);
static uint64_t * func_68(int32_t * p_69, int32_t * p_70, union U0  p_71, int64_t  p_72, uint64_t * const  p_73);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_1783 g_116
 * writes:
 */
static const float  func_1(void)
{ /* block id: 0 */
    uint8_t l_34[5][2][6] = {{{0xF4L,0x38L,0xF4L,0x12L,0x12L,0xF4L},{7UL,7UL,0x12L,0UL,0x12L,7UL}},{{0x12L,0x38L,0UL,0UL,0x38L,0x12L},{7UL,0x12L,0UL,0x12L,7UL,7UL}},{{0xF4L,0x12L,0x12L,0xF4L,0x38L,0xF4L},{0xF4L,0x38L,0xF4L,0x12L,0x12L,0xF4L}},{{7UL,7UL,0x12L,0UL,0x12L,7UL},{0x12L,0x38L,0UL,0UL,0x38L,0x12L}},{{7UL,0x12L,0UL,0x12L,0x12L,0x12L},{0UL,0xF4L,0xF4L,0UL,7UL,0UL}}};
    int32_t l_35 = 4L;
    uint64_t *l_36[2];
    float *l_1307 = &g_116;
    uint32_t l_1309 = 0xC59BCAFFL;
    uint64_t **l_1313 = (void*)0;
    uint64_t **l_1314 = &g_56;
    uint8_t l_2025 = 1UL;
    float l_2049 = 0x0.5p+1;
    float l_2080 = 0x1.947F95p+9;
    int16_t l_2081[5][1][4] = {{{8L,8L,8L,8L}},{{8L,8L,8L,8L}},{{8L,8L,8L,8L}},{{8L,8L,8L,8L}},{{8L,8L,8L,8L}}};
    uint8_t *l_2108[1];
    uint8_t **l_2107 = &l_2108[0];
    int32_t l_2117 = 0L;
    int32_t l_2119 = (-5L);
    int32_t l_2122 = 0xB810155AL;
    int32_t l_2123 = (-6L);
    int32_t l_2124 = 0xB5790F61L;
    int32_t ****l_2253 = (void*)0;
    uint32_t l_2256 = 1UL;
    float l_2279 = 0x5.D14625p+69;
    int32_t l_2318 = 0L;
    uint32_t **l_2355 = &g_446;
    uint32_t ***l_2354 = &l_2355;
    uint16_t * const *l_2365 = (void*)0;
    uint16_t * const **l_2364[7] = {&l_2365,&l_2365,&l_2365,&l_2365,&l_2365,&l_2365,&l_2365};
    int64_t l_2371 = 0x5D0B14302BD1931DLL;
    union U0 l_2424 = {1UL};
    int32_t l_2427 = 0x79C76EB9L;
    int32_t l_2451 = 5L;
    uint32_t l_2486 = 0xA925F321L;
    int32_t l_2609 = 0xD022C069L;
    uint32_t l_2659[7][2] = {{0x8DE3DAA3L,0x8DE3DAA3L},{0x8DE3DAA3L,0x8DE3DAA3L},{0x8DE3DAA3L,0x8DE3DAA3L},{0x8DE3DAA3L,0x8DE3DAA3L},{0x8DE3DAA3L,0x8DE3DAA3L},{0x8DE3DAA3L,0x8DE3DAA3L},{0x8DE3DAA3L,0x8DE3DAA3L}};
    int32_t l_2666 = 0L;
    int32_t l_2667[2][5] = {{(-5L),4L,4L,(-5L),4L},{(-5L),(-5L),0x458FCD6AL,(-5L),(-5L)}};
    uint8_t l_2688 = 0x86L;
    uint32_t l_2779 = 1UL;
    float *l_2795 = &g_1704[0][3][0];
    int16_t l_2807 = 0xE43EL;
    float *l_2808[9][3] = {{&l_2279,(void*)0,&l_2279},{(void*)0,(void*)0,(void*)0},{&l_2279,(void*)0,&l_2279},{(void*)0,(void*)0,(void*)0},{&l_2279,(void*)0,&l_2279},{(void*)0,(void*)0,(void*)0},{&l_2279,(void*)0,&l_2279},{(void*)0,(void*)0,(void*)0},{&l_2279,(void*)0,&l_2279}};
    int64_t l_2810 = 0L;
    int8_t l_2811 = (-1L);
    int8_t l_2815[6] = {0L,0xF3L,0L,0L,0xF3L,0L};
    uint16_t *****l_2822 = (void*)0;
    uint32_t l_2823 = 0x4B032A23L;
    int64_t *l_2846 = (void*)0;
    int64_t *l_2847 = &g_477[3];
    int32_t l_2848 = 0x8E5F4BFCL;
    int8_t *l_2849 = &l_2815[2];
    uint8_t l_2870 = 2UL;
    uint32_t l_2872 = 0x3AB1C92AL;
    int32_t *l_2876[2][4] = {{&l_2427,&g_2217,&l_2427,&l_2427},{&g_2217,&g_2217,&l_2427,&g_2217}};
    float l_2892 = 0x4.AED9B7p-81;
    uint32_t l_2893 = 0UL;
    uint8_t l_2901 = 0x23L;
    const uint32_t l_2902 = 0UL;
    int64_t ***l_2921[1];
    const int32_t **l_3067 = (void*)0;
    const int32_t *** const l_3066 = &l_3067;
    int64_t l_3068[10][6][4] = {{{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)},{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)}},{{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)},{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)}},{{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)},{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)}},{{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL},{(-3L),(-3L),0x02719AFE9AC74C56LL,(-3L)},{(-3L),0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L)},{0x1FC0C83CB998D2AFLL,(-3L),0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}},{{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL},{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}},{{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL},{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}},{{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL},{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}},{{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL},{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}},{{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL},{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}},{{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL},{0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL},{0x02719AFE9AC74C56LL,0x1FC0C83CB998D2AFLL,0x02719AFE9AC74C56LL,0x02719AFE9AC74C56LL},{0x1FC0C83CB998D2AFLL,0x1FC0C83CB998D2AFLL,(-3L),0x1FC0C83CB998D2AFLL}}};
    int64_t l_3110 = 8L;
    int32_t l_3126[8][4][8] = {{{0x998F58A0L,(-4L),0x13F7B34AL,0x507484D7L,0xA1710937L,0x507484D7L,0x13F7B34AL,(-4L)},{(-1L),0x13F7B34AL,(-9L),0x507484D7L,0L,0x31E1816BL,0x31E1816BL,0L},{0x1722989AL,0L,0L,0x1722989AL,(-1L),(-4L),0x31E1816BL,0x998F58A0L},{0x13F7B34AL,0x1722989AL,(-9L),0x31E1816BL,(-9L),0x1722989AL,0x13F7B34AL,0xA1710937L}},{{(-9L),0x1722989AL,0x13F7B34AL,0xA1710937L,(-4L),(-4L),0xA1710937L,0x13F7B34AL},{0L,0x31E1816BL,0L,0x507484D7L,(-9L),0x13F7B34AL,(-1L),0x13F7B34AL},{0x998F58A0L,0L,0x507484D7L,0L,0x998F58A0L,0x1722989AL,0x31E1816BL,0x13F7B34AL},{0L,(-9L),(-1L),0x507484D7L,0x507484D7L,(-1L),(-9L),0L}},{{0L,0x1722989AL,(-1L),(-4L),0x31E1816BL,0x998F58A0L,0x31E1816BL,(-4L)},{0x507484D7L,0xA1710937L,0x507484D7L,0x13F7B34AL,(-4L),0x998F58A0L,(-1L),(-1L)},{(-1L),0x1722989AL,0L,0L,0x1722989AL,(-1L),(-4L),0x31E1816BL},{(-1L),(-9L),0L,0x1722989AL,(-4L),0x1722989AL,0L,(-9L)}},{{0x507484D7L,0L,0x998F58A0L,0x1722989AL,0x31E1816BL,0x13F7B34AL,0x13F7B34AL,0x31E1816BL},{0L,0x31E1816BL,0x31E1816BL,0L,0x507484D7L,(-9L),0x13F7B34AL,(-1L)},{0L,0L,0x998F58A0L,0x13F7B34AL,0x998F58A0L,0L,0L,(-4L)},{0x998F58A0L,0L,0L,(-4L),(-9L),(-9L),(-4L),0L}},{{0x31E1816BL,0x31E1816BL,0L,0x507484D7L,(-9L),0x13F7B34AL,(-1L),0x13F7B34AL},{0x998F58A0L,0L,0x507484D7L,0L,0x998F58A0L,0x1722989AL,0x31E1816BL,0x13F7B34AL},{0L,(-9L),(-1L),0x507484D7L,0x507484D7L,(-1L),(-9L),0L},{0L,0x1722989AL,(-1L),(-4L),0x31E1816BL,0x998F58A0L,0x31E1816BL,(-4L)}},{{0x507484D7L,0xA1710937L,0x507484D7L,0x13F7B34AL,(-4L),0x998F58A0L,(-1L),(-1L)},{(-1L),0x1722989AL,0L,0L,0x1722989AL,(-1L),(-4L),0x31E1816BL},{(-1L),(-9L),0L,0x1722989AL,(-4L),0x1722989AL,0L,(-9L)},{0x507484D7L,0L,0x998F58A0L,0x1722989AL,0x31E1816BL,0x13F7B34AL,0x13F7B34AL,0x31E1816BL}},{{0L,0x31E1816BL,0x31E1816BL,0L,0x507484D7L,(-9L),0x13F7B34AL,(-1L)},{0L,0L,0x998F58A0L,0x13F7B34AL,0x998F58A0L,0L,0L,(-4L)},{0x998F58A0L,0L,0L,(-4L),(-9L),(-9L),(-4L),0L},{0x31E1816BL,0x31E1816BL,0L,0x507484D7L,(-9L),0x13F7B34AL,(-1L),0x13F7B34AL}},{{0x998F58A0L,0L,0x507484D7L,0L,0x998F58A0L,0x1722989AL,0x31E1816BL,0x13F7B34AL},{0xA1710937L,0x998F58A0L,0x507484D7L,0x1722989AL,0x1722989AL,0x507484D7L,0x998F58A0L,0xA1710937L},{0x31E1816BL,0L,0x507484D7L,(-9L),0x13F7B34AL,(-1L),0x13F7B34AL,(-9L)},{0x1722989AL,(-4L),0x1722989AL,0L,(-9L),(-1L),0x507484D7L,0x507484D7L}}};
    int16_t l_3136 = (-1L);
    int16_t l_3179 = 0xA0EDL;
    uint8_t l_3230 = 1UL;
    uint32_t l_3252 = 4294967290UL;
    int16_t l_3318 = 0x65AAL;
    int32_t *l_3332 = &g_208;
    int32_t *l_3349 = &g_83;
    uint16_t l_3369 = 5UL;
    uint32_t *l_3378[1];
    int8_t l_3511[10][1][8] = {{{0x3DL,0xEDL,(-9L),(-9L),(-9L),0xEDL,0x3DL,(-1L)}},{{0x2EL,(-9L),(-5L),0xC3L,7L,(-1L),8L,5L}},{{(-9L),0x29L,(-8L),7L,7L,(-8L),0x29L,(-9L)}},{{0x2EL,(-9L),0xEDL,5L,(-9L),0x29L,(-5L),0xCDL}},{{0x3DL,(-3L),0xCEL,0x29L,5L,0x29L,0xCEL,(-3L)}},{{(-1L),(-9L),0xCDL,0L,0x2EL,(-8L),(-9L),0xCEL}},{{(-3L),0x29L,7L,(-9L),(-1L),(-1L),(-9L),7L}},{{(-9L),(-9L),0xCDL,(-8L),0xC3L,0xEDL,0xCEL,0x3DL}},{{0xC3L,0xEDL,0xCEL,0x3DL,(-5L),7L,(-5L),0x3DL}},{{0xEDL,0xCDL,0xEDL,(-8L),8L,(-3L),0x29L,7L}}};
    union U0 *l_3586 = (void*)0;
    int32_t l_3587[3];
    const uint32_t l_3628[4] = {0UL,0UL,0UL,0UL};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_36[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_2108[i] = &g_1538;
    for (i = 0; i < 1; i++)
        l_2921[i] = &g_2915;
    for (i = 0; i < 1; i++)
        l_3378[i] = &l_2872;
    for (i = 0; i < 3; i++)
        l_3587[i] = (-1L);
    return (*g_1783);
}


/* ------------------------------------------ */
/* 
 * reads : g_1551 g_1405 g_37 g_240.f3 g_667 g_1219 g_232 g_83 g_57 g_56 g_1510 g_208 g_1403 g_1404 g_138 g_139 g_299 g_135 g_551 g_552 g_553 g_217 g_218 g_601 g_445 g_446 g_102 g_498 g_499 g_100 g_561 g_562 g_125 g_1541 g_240 g_1217 g_1234 g_104 g_1652 g_497 g_731 g_645 g_134 g_1704 g_1119 g_1347 g_1303 g_116 g_477 g_1112 g_600 g_1540 g_1431 g_1111 g_240.f1 g_884 g_1889 g_441 g_74 g_1783
 * writes: g_83 g_240.f3 g_667 g_37 g_116 g_299 g_135 g_486 g_102 g_74 g_125 g_57 g_1217 g_1405 g_477 g_100 g_1538 g_1783 g_1541 g_315 g_240.f1 g_562 g_441 g_1704
 */
static int32_t  func_2(const uint32_t  p_3, uint32_t  p_4)
{ /* block id: 599 */
    uint64_t l_1550 = 18446744073709551615UL;
    int32_t l_1552 = (-6L);
    int32_t l_1553[5][8][4] = {{{0x9053E671L,(-10L),0x0C8EB6FAL,0x6C40E054L},{(-9L),2L,0x4D3801FCL,8L},{0L,(-9L),0x13F4F577L,0x4795986CL},{0x13F4F577L,0x4795986CL,5L,3L},{0xF54D7293L,6L,1L,0x9053E671L},{0x68526442L,8L,0x987673F6L,1L},{0x37CFC375L,0L,8L,1L},{0xF883DC3BL,0x6690FC45L,0xF54D7293L,0xF54D7293L}},{{0x6C40E054L,0x6C40E054L,1L,0x88696901L},{(-4L),0xD77D3A82L,3L,0x4FC482D1L},{6L,5L,0x6690FC45L,3L},{0L,5L,0xF883DC3BL,0x4FC482D1L},{5L,0xD77D3A82L,(-3L),0x88696901L},{1L,0x6C40E054L,3L,0xF54D7293L},{0x30609772L,0x6690FC45L,(-9L),1L},{0x435A0CFBL,0L,0x4795986CL,1L}},{{(-9L),8L,0xD77D3A82L,0x9053E671L},{(-2L),6L,0x4FC482D1L,3L},{1L,0x4795986CL,(-9L),0x4795986CL},{0xD77D3A82L,(-9L),(-10L),8L},{3L,2L,0L,0x5F2FA021L},{0x435A0CFBL,(-4L),2L,(-9L)},{0x435A0CFBL,0xD77D3A82L,0L,0x6C40E054L},{5L,(-9L),(-4L),0x0C8EB6FAL}},{{0xF54D7293L,0x30609772L,0x9053E671L,(-3L)},{0L,0x13F4F577L,3L,0xF54D7293L},{(-3L),(-10L),0xF54D7293L,0L},{1L,0x4795986CL,0x6690FC45L,(-1L)},{0x13F4F577L,2L,1L,(-1L)},{1L,5L,5L,1L},{(-10L),3L,(-1L),2L},{6L,0L,0x4D3801FCL,0xD77D3A82L}},{{2L,0x0C8EB6FAL,0xA6C39B71L,0xD77D3A82L},{3L,0L,0xF883DC3BL,2L},{(-1L),3L,(-2L),1L},{0x5F2FA021L,5L,5L,(-1L)},{0x4D3801FCL,2L,(-9L),(-1L)},{0L,0x4795986CL,1L,0L},{0x30609772L,(-10L),0L,0xF54D7293L},{5L,0x13F4F577L,6L,(-3L)}}};
    uint64_t l_1555 = 0x540E74558850120ALL;
    float *l_1601[5][4][2] = {{{&g_134,&g_116},{&g_116,&g_116},{(void*)0,&g_134},{&g_116,(void*)0}},{{&g_134,&g_116},{&g_134,(void*)0},{&g_116,&g_134},{(void*)0,&g_116}},{{&g_116,&g_116},{&g_134,&g_134},{&g_134,&g_116},{&g_134,&g_116}},{{&g_134,&g_134},{&g_134,&g_116},{&g_116,&g_116},{(void*)0,&g_134}},{{&g_116,(void*)0},{&g_134,&g_116},{&g_134,(void*)0},{&g_116,&g_134}}};
    float **l_1600 = &l_1601[2][1][1];
    float ***l_1599 = &l_1600;
    int8_t *l_1713 = &g_240.f3;
    union U0 l_1738[5] = {{0xE98EC5717F9F9AD9LL},{0xE98EC5717F9F9AD9LL},{0xE98EC5717F9F9AD9LL},{0xE98EC5717F9F9AD9LL},{0xE98EC5717F9F9AD9LL}};
    const uint16_t **l_1741 = &g_138;
    float *l_1786 = &g_1704[0][5][2];
    uint8_t l_1803 = 5UL;
    const uint8_t l_1831[1] = {255UL};
    int16_t l_1842[6] = {0x013EL,0x013EL,0x013EL,0x013EL,0x013EL,0x013EL};
    int32_t **l_1892 = &g_74;
    uint64_t l_1897[8] = {4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL};
    int i, j, k;
lbl_1627:
    (*g_1551) = (l_1550 = (-7L));
    if ((l_1550 && (*g_1405)))
    { /* block id: 602 */
        int32_t *l_1554[7];
        float *l_1577 = &g_116;
        float **l_1576[5];
        float *** const l_1575 = &l_1576[2];
        uint32_t l_1582 = 0xC4196748L;
        const uint32_t *l_1608 = &g_441;
        const uint32_t **l_1607 = &l_1608;
        const uint32_t ***l_1606 = &l_1607;
        int64_t l_1623 = 0x113DAAB835E003ACLL;
        uint32_t l_1667 = 0x174D2D71L;
        int8_t *l_1714 = &g_1217;
        int16_t *l_1715 = &g_129;
        const uint64_t *l_1740 = (void*)0;
        const uint64_t **l_1739 = &l_1740;
        uint16_t ***l_1767 = &g_498;
        uint16_t ****l_1766 = &l_1767;
        float l_1800 = 0x0.1p+1;
        int32_t l_1839[10] = {0xC1B4C774L,0xC1B4C774L,(-1L),0xC1B4C774L,0xC1B4C774L,(-1L),0xC1B4C774L,0xC1B4C774L,(-1L),0xC1B4C774L};
        float l_1863 = 0x3.8p+1;
        const uint32_t *l_1875[4];
        int8_t l_1894 = 0L;
        int16_t l_1911 = 0x5FCDL;
        int i;
        for (i = 0; i < 7; i++)
            l_1554[i] = &g_83;
        for (i = 0; i < 5; i++)
            l_1576[i] = &l_1577;
        for (i = 0; i < 4; i++)
            l_1875[i] = &g_5[1][0];
        l_1555++;
        for (g_240.f3 = 0; (g_240.f3 <= 0); g_240.f3 += 1)
        { /* block id: 606 */
            int16_t l_1564 = 1L;
            uint16_t ***l_1615 = (void*)0;
            uint16_t ****l_1614 = &l_1615;
            int32_t l_1618[4] = {0x0F735798L,0x0F735798L,0x0F735798L,0x0F735798L};
            int i;
            for (g_667 = 0; (g_667 >= 0); g_667 -= 1)
            { /* block id: 609 */
                int32_t l_1574 = 0x6C78DB16L;
                int32_t l_1620[4][2][2] = {{{(-1L),(-1L)},{(-1L),(-1L)}},{{(-1L),(-1L)},{(-1L),(-1L)}},{{(-1L),(-1L)},{(-1L),(-1L)}},{{(-1L),(-1L)},{(-1L),(-1L)}}};
                int8_t **l_1628[7];
                int i, j, k;
                for (i = 0; i < 7; i++)
                    l_1628[i] = &g_497;
                if (((((((safe_unary_minus_func_uint16_t_u((g_1219[(g_667 + 1)] && (safe_div_func_int16_t_s_s(((***g_1403) = ((((+(safe_rshift_func_uint8_t_u_u((p_3 & (0xE9029137F7046BA9LL <= (l_1564 ^= 0x1863F15278BA199FLL))), 2))) != (safe_add_func_int16_t_s_s((~(*g_232)), ((safe_lshift_func_uint8_t_u_u(g_57, 5)) >= ((safe_mul_func_uint8_t_u_u((safe_div_func_uint64_t_u_u((*g_56), g_1510)), ((p_3 != 18446744073709551611UL) < (*g_56)))) <= g_208))))) && p_4) , 0x8434L)), (*g_138)))))) && l_1552) | p_3) == l_1574) , (void*)0) == l_1575))
                { /* block id: 612 */
                    uint32_t l_1578 = 0UL;
                    if (l_1578)
                        break;
                }
                else
                { /* block id: 614 */
                    uint32_t l_1579[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1579[i] = 0x6DE90B24L;
                    l_1579[2]--;
                    (*l_1577) = (0x5.4p-1 > ((p_4 , 18446744073709551609UL) , (((-0x9.3p-1) <= 0xB.543D8Ap-65) == p_3)));
                }
                for (g_299 = 0; (g_299 <= 0); g_299 += 1)
                { /* block id: 620 */
                    int32_t l_1611 = 1L;
                    int32_t l_1613[7] = {0x26B8FD9EL,0x26B8FD9EL,0x26B8FD9EL,0x26B8FD9EL,0x26B8FD9EL,0x26B8FD9EL,0x26B8FD9EL};
                    int i;
                    for (g_135 = 0; (g_135 <= 6); g_135 += 1)
                    { /* block id: 623 */
                        uint8_t *l_1594 = &g_486[2][0];
                        float ***l_1598 = &l_1576[2];
                        const uint32_t ****l_1609 = &l_1606;
                        int64_t *l_1610[4] = {&g_477[4],&g_477[4],&g_477[4],&g_477[4]};
                        int32_t l_1612 = 7L;
                        int i;
                        if (p_4)
                            break;
                        ++l_1582;
                        l_1552 = (safe_mod_func_uint16_t_u_u(((safe_add_func_int16_t_s_s((safe_div_func_uint64_t_u_u((!(safe_mul_func_int8_t_s_s((((**g_1404) &= ((((*l_1594) = 0x7EL) , ((**g_551) && (*g_552))) && (*g_217))) <= (safe_add_func_int32_t_s_s((l_1612 &= (p_4 , ((((*g_446) = ((safe_unary_minus_func_int32_t_s((l_1598 != l_1599))) || ((l_1611 &= (safe_mod_func_int16_t_s_s(p_3, (l_1553[1][6][1] = (safe_div_func_int16_t_s_s((((*l_1609) = l_1606) != &l_1607), 8UL)))))) , (***g_601)))) >= p_4) || 0x2FADL))), p_4))), 0x40L))), 0x9EC0B57A7403E5F0LL)), (**g_498))) | l_1613[6]), p_3));
                        (**g_561) = &l_1612;
                    }
                    for (g_125 = 0; (g_125 <= 0); g_125 += 1)
                    { /* block id: 638 */
                        uint16_t *****l_1616 = &l_1614;
                        int32_t l_1617 = 0x0DACEC40L;
                        int32_t l_1619 = 0x3C0C2673L;
                        int32_t l_1621 = 0x0F48DAB3L;
                        int32_t l_1622[4][3] = {{(-3L),(-3L),(-3L)},{0L,1L,0L},{(-3L),(-3L),(-3L)},{0L,1L,0L}};
                        uint32_t l_1624[5];
                        int i, j;
                        for (i = 0; i < 5; i++)
                            l_1624[i] = 18446744073709551615UL;
                        l_1617 = (((*g_1541) , &g_734) != ((*l_1616) = l_1614));
                        --l_1624[4];
                        if (g_102)
                            goto lbl_1627;
                    }
                    l_1574 = l_1564;
                    l_1628[2] = l_1628[1];
                }
                (*g_562) = &l_1620[2][0][1];
            }
            l_1618[0] = (0xB7L < g_667);
            for (g_102 = 0; (g_102 <= 0); g_102 += 1)
            { /* block id: 655 */
                l_1553[0][1][2] = p_4;
            }
        }
        for (g_57 = 18; (g_57 > 27); g_57 = safe_add_func_uint8_t_u_u(g_57, 3))
        { /* block id: 661 */
            int64_t l_1659 = 0x553D09D614BA806FLL;
            float **l_1665 = &l_1601[2][1][1];
            int64_t *l_1666 = &l_1623;
            for (g_1217 = (-12); (g_1217 != 20); g_1217 = safe_add_func_int8_t_s_s(g_1217, 8))
            { /* block id: 664 */
                if (p_3)
                    break;
            }
            l_1553[2][7][0] ^= (*g_1234);
            l_1667 |= ((safe_sub_func_int32_t_s_s((l_1552 = (g_1652 == (void*)0)), l_1553[3][1][3])) <= ((((safe_lshift_func_int16_t_s_u(((((*l_1666) ^= (((safe_div_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((((**g_445) = l_1659) | l_1555), 14)), (+((safe_rshift_func_int16_t_s_u(((((safe_div_func_uint8_t_u_u(255UL, p_3)) , l_1665) == l_1665) , (-7L)), p_4)) ^ (*g_552))))) <= g_1217) != (*g_497))) || p_3) || p_4), (*g_138))) || 0x85L) | 0x53L) , (***g_601)));
        }
        for (g_1217 = 0; (g_1217 > (-29)); g_1217 = safe_sub_func_uint32_t_u_u(g_1217, 6))
        { /* block id: 675 */
            float l_1686 = 0x6.63D7D3p-63;
            int32_t l_1687[7][4][7] = {{{0xFE4811D6L,(-2L),0xFE4811D6L,0xAA7FA7B6L,0x7389754BL,0x33533E4AL,1L},{0x7389754BL,0xFD280C27L,0x550D8A50L,1L,(-2L),2L,6L},{0xA8B6D6BAL,0x69654502L,0xAA7FA7B6L,0xA8B6D6BAL,0L,0x33533E4AL,0x03B940E9L},{0x07C15A1EL,1L,0x69654502L,5L,0x03B940E9L,7L,(-1L)}},{{1L,0x03B940E9L,0x550D8A50L,0x7389754BL,(-8L),0xBA71E67BL,0xE8FD86CEL},{(-6L),1L,0x33533E4AL,0x7389754BL,0xAA7FA7B6L,0xFE4811D6L,(-2L)},{0xD6DDDCA5L,0xFD280C27L,5L,5L,0xFD280C27L,0xD6DDDCA5L,1L},{0x82266CF9L,0x7389754BL,2L,0xA8B6D6BAL,6L,0xCD833B9CL,0x7389754BL}},{{(-8L),(-1L),0xD6DDDCA5L,1L,0xAA7FA7B6L,7L,(-8L)},{0x07C15A1EL,0x7389754BL,1L,0xAA7FA7B6L,0xA286B028L,(-1L),0x69654502L},{(-6L),0xFD280C27L,0xCD833B9CL,0xE8FD86CEL,0x7389754BL,0xCD833B9CL,6L},{0x03B940E9L,1L,5L,0L,0L,0x69654502L,6L}},{{0xFE4811D6L,0x03B940E9L,0x69654502L,0xB59CE8AAL,6L,0xB59CE8AAL,0x69654502L},{1L,1L,2L,1L,0xE8FD86CEL,0xBA71E67BL,(-8L)},{0xA286B028L,0x69654502L,0x230DDCA6L,0x7389754BL,7L,0x07C15A1EL,0x7389754BL},{0xFE4811D6L,0xFD280C27L,0xB59CE8AAL,0xAA7FA7B6L,0xE8FD86CEL,0xFE4811D6L,1L}},{{0xE8FD86CEL,(-2L),0x550D8A50L,0L,6L,2L,(-2L)},{0x82266CF9L,0x69654502L,0xD6DDDCA5L,0x82266CF9L,0L,0xB59CE8AAL,0xE8FD86CEL},{0x07C15A1EL,0xA286B028L,0xD6DDDCA5L,5L,0x7389754BL,(-1L),(-1L)},{0xA286B028L,0xE8FD86CEL,0x550D8A50L,0xE8FD86CEL,0xA286B028L,0xBA71E67BL,0x03B940E9L}},{{0xA8B6D6BAL,1L,0xB59CE8AAL,0x03B940E9L,0xAA7FA7B6L,0x33533E4AL,6L},{0xD6DDDCA5L,6L,0x230DDCA6L,5L,6L,0xAA7FA7B6L,1L},{0xA8B6D6BAL,0x03B940E9L,2L,(-6L),0xFD280C27L,0xCD833B9CL,0xE8FD86CEL},{0xA286B028L,(-1L),0x69654502L,1L,0xAA7FA7B6L,0x07C15A1EL,0xA286B028L}},{{0x07C15A1EL,0xE8FD86CEL,5L,0xAA7FA7B6L,(-8L),0x07C15A1EL,0x69654502L},{0x82266CF9L,(-2L),0xCD833B9CL,0x03B940E9L,0x03B940E9L,0xCD833B9CL,(-2L)},{0xE8FD86CEL,1L,1L,1L,0L,0xAA7FA7B6L,0xFD280C27L},{0xFE4811D6L,0x7389754BL,0x93A5F18DL,(-10L),(-1L),0x550D8A50L,0xCD833B9CL}}};
            uint8_t *l_1694[2][1][1];
            float **l_1700 = &l_1601[2][1][1];
            int32_t l_1701 = 0x533F0EF5L;
            int16_t *l_1709[7][5][7] = {{{&g_1047,&g_129,(void*)0,&g_37,&g_129,&g_129,(void*)0},{&g_1047,&g_1047,(void*)0,&g_37,&g_1047,&g_1047,&g_37},{&g_129,(void*)0,&g_129,&g_129,&g_129,&g_37,&g_1047},{&g_37,&g_1047,&g_1047,(void*)0,&g_37,&g_37,(void*)0},{(void*)0,&g_129,&g_129,&g_129,&g_129,&g_37,(void*)0}},{{&g_1047,&g_1047,&g_1047,&g_1047,&g_1047,&g_1047,&g_1047},{(void*)0,&g_129,(void*)0,&g_129,&g_129,&g_129,&g_129},{&g_1047,&g_37,&g_37,&g_1047,&g_37,&g_1047,&g_37},{&g_37,(void*)0,(void*)0,&g_1047,&g_37,&g_129,&g_37},{&g_37,&g_1047,&g_1047,&g_37,&g_129,&g_1047,&g_37}},{{&g_129,&g_129,&g_129,(void*)0,&g_37,&g_37,(void*)0},{&g_37,&g_37,&g_1047,&g_129,&g_1047,&g_1047,&g_37},{&g_1047,&g_1047,&g_129,(void*)0,&g_129,&g_129,&g_37},{&g_37,&g_129,(void*)0,&g_1047,(void*)0,&g_37,&g_37},{&g_37,&g_129,(void*)0,&g_129,&g_37,&g_1047,&g_129}},{{&g_129,&g_1047,&g_37,&g_1047,&g_129,&g_1047,&g_1047},{&g_129,(void*)0,&g_37,(void*)0,&g_129,(void*)0,(void*)0},{&g_1047,(void*)0,&g_37,&g_129,&g_37,&g_1047,(void*)0},{&g_37,&g_1047,&g_1047,(void*)0,(void*)0,&g_129,&g_1047},{&g_1047,&g_129,&g_37,&g_37,&g_37,&g_37,&g_37}},{{&g_129,&g_129,&g_37,&g_1047,(void*)0,&g_1047,(void*)0},{&g_129,&g_37,(void*)0,&g_1047,&g_1047,&g_37,&g_129},{&g_37,&g_1047,&g_129,&g_129,(void*)0,(void*)0,(void*)0},{&g_37,&g_37,&g_1047,&g_1047,&g_37,&g_37,&g_1047},{&g_1047,&g_1047,&g_129,&g_129,(void*)0,&g_1047,&g_129}},{{&g_37,&g_1047,&g_1047,(void*)0,&g_37,&g_129,&g_1047},{&g_129,&g_1047,&g_129,&g_1047,&g_37,&g_129,(void*)0},{(void*)0,&g_1047,(void*)0,&g_1047,&g_1047,&g_1047,&g_1047},{(void*)0,&g_129,&g_129,&g_1047,&g_1047,&g_1047,&g_129},{(void*)0,&g_1047,&g_1047,(void*)0,&g_1047,&g_1047,&g_1047}},{{&g_129,&g_129,&g_129,(void*)0,&g_129,&g_129,&g_129},{&g_129,&g_1047,&g_37,&g_1047,&g_37,&g_1047,&g_37},{&g_129,&g_37,&g_1047,&g_37,(void*)0,(void*)0,&g_1047},{&g_1047,&g_1047,&g_37,&g_1047,&g_1047,&g_1047,&g_1047},{&g_1047,&g_129,&g_129,&g_37,(void*)0,&g_129,&g_37}}};
            int8_t **l_1712[5];
            int64_t *l_1716 = &g_477[0];
            uint16_t *l_1717 = &g_125;
            uint16_t * const *l_1771 = &l_1717;
            uint16_t * const * const *l_1770 = &l_1771;
            uint16_t * const * const **l_1769 = &l_1770;
            union U0 *l_1807 = &g_240;
            uint8_t l_1830 = 0UL;
            int16_t l_1920[6];
            int32_t *****l_1922 = &g_560[0][2];
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_1694[i][j][k] = &g_486[2][0];
                }
            }
            for (i = 0; i < 5; i++)
                l_1712[i] = &g_497;
            for (i = 0; i < 6; i++)
                l_1920[i] = 0x5C78L;
            if ((safe_mul_func_uint16_t_u_u(((((safe_lshift_func_uint16_t_u_s((safe_add_func_int16_t_s_s(((((0xA9F63B66C75A6EAFLL != ((((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((safe_mod_func_int32_t_s_s((l_1687[3][2][4] = p_3), 4294967295UL)), 0)), 14)), p_4)), ((safe_sub_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((safe_div_func_uint32_t_u_u(((l_1553[2][0][1] &= p_4) , (**g_445)), 0x59F197C0L)), g_731)), (safe_div_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u(((l_1701 = ((((*l_1599) != l_1700) || p_4) > (**g_445))) && l_1701))), p_4)), l_1552)))) , p_4))) > (*g_499)) , p_4) < (*g_217))) ^ p_4) , l_1555) >= l_1550), (**g_1404))), p_4)) >= p_4) >= p_3) || p_3), p_3)))
            { /* block id: 679 */
                (**g_561) = (void*)0;
                l_1701 = (l_1552 = (l_1687[3][2][4] = ((*l_1577) = (safe_div_func_float_f_f((0xA.EBA43Bp-80 < (*g_645)), g_1704[0][3][0])))));
            }
            else
            { /* block id: 685 */
                if (p_3)
                    break;
            }
            if (((p_4 < (l_1553[3][1][3] = g_1119)) < (safe_mod_func_uint16_t_u_u(((*l_1717) = (((safe_mul_func_int16_t_s_s((g_1347 , (l_1709[6][4][4] == ((*g_1404) = (**g_1403)))), (safe_mul_func_uint16_t_u_u(((*g_499) ^= (((*l_1716) = (((l_1713 = (void*)0) == l_1714) && (((*g_1303) , l_1715) == (void*)0))) == 0x3720C1B64A49AF90LL)), l_1552)))) || g_477[2]) & 0x66B79917859AE883LL)), l_1552))))
            { /* block id: 694 */
                int32_t l_1725 = 0xE732699AL;
                int16_t l_1728 = (-2L);
                uint64_t *l_1737[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_1737[i] = &g_299;
                l_1553[4][6][1] ^= ((safe_sub_func_int16_t_s_s(p_3, (safe_div_func_int16_t_s_s((p_4 < 9UL), (safe_add_func_uint32_t_u_u(4294967293UL, 0x0AED9262L)))))) , (+((**g_445) > (l_1728 = (l_1725 >= (safe_div_func_int16_t_s_s(p_3, ((**g_498) = 0x2C58L))))))));
                (**g_561) = l_1554[6];
            }
            else
            { /* block id: 702 */
                uint8_t l_1747 = 0xA0L;
                int32_t l_1801[8][1] = {{0x6C32179DL},{(-1L)},{0x6C32179DL},{(-1L)},{0x6C32179DL},{(-1L)},{0x6C32179DL},{(-1L)}};
                int32_t *l_1834 = &g_83;
                int64_t l_1836 = 0xB596EA353D5868ABLL;
                int i, j;
                for (g_1538 = 0; (g_1538 < 48); ++g_1538)
                { /* block id: 705 */
                    const int16_t *l_1763 = &g_129;
                    const int16_t **l_1762 = &l_1763;
                    const int16_t ***l_1761 = &l_1762;
                    int16_t ***l_1764 = &g_1404;
                    int32_t l_1787 = 0x4C24E482L;
                    const int16_t l_1832[4] = {0x3873L,0x3873L,0x3873L,0x3873L};
                    int32_t l_1837 = 0L;
                    int32_t l_1838 = 0x209CA11AL;
                    int32_t l_1840 = 0xFB405C5FL;
                    int32_t l_1841 = 4L;
                    uint32_t l_1843 = 0x84668072L;
                    uint32_t l_1870 = 0x53DAD1BDL;
                    int i;
                    if ((p_4 == (0x5241EA5AL | (((*g_446) & p_3) || ((*g_1112) > ((**g_498) ^= (((*g_446) = (safe_mod_func_int16_t_s_s(p_4, (~(((l_1747 >= ((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int8_t_s_s((-7L), 7)), (safe_lshift_func_uint8_t_u_u(((+(safe_div_func_uint16_t_u_u(((0xC9L < 0x28L) && p_3), 0x636AL))) & g_299), g_1119)))) <= l_1747)) ^ 0xAEL) ^ 18446744073709551615UL))))) ^ p_3)))))))
                    { /* block id: 708 */
                        int32_t l_1765 = 0x436B7F99L;
                        uint16_t *****l_1768 = &l_1766;
                        uint16_t * const * const ***l_1772 = &l_1769;
                        l_1765 = (safe_add_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_u((**g_498), 11)) , l_1761) == ((0x047EE69A1869CFA2LL ^ 7L) , l_1764)), (*g_497)));
                        (*l_1577) = (((*l_1768) = l_1766) == ((*l_1772) = l_1769));
                    }
                    else
                    { /* block id: 713 */
                        uint64_t l_1780 = 18446744073709551615UL;
                        (***l_1575) = (((*g_56) = 18446744073709551615UL) , (p_4 == (!(safe_add_func_float_f_f((0x7.9D0EF7p+13 >= ((((safe_mul_func_uint8_t_u_u(((l_1552 |= 1UL) && ((((((***l_1770) |= (safe_mul_func_uint16_t_u_u((((p_4 , (*g_499)) == ((++l_1780) & ((g_1783 = ((*l_1600) = (*l_1700))) == ((safe_lshift_func_uint16_t_u_s(0UL, 15)) , (l_1786 = (void*)0))))) || (****g_600)), 1L))) ^ l_1787) >= (***g_1652)) , (void*)0) != &g_551)), g_102)) , l_1709[3][4][2]) == (*g_1404)) < (-0x9.3p-1))), 0x7.FC391Ep-76)))));
                    }
                    if (((l_1687[1][1][6] < (*g_552)) && (4L ^ (safe_mod_func_uint16_t_u_u(p_4, ((safe_add_func_int16_t_s_s((!(safe_div_func_int32_t_s_s((0x643D7726D946C90BLL >= (safe_div_func_uint32_t_u_u(((((!l_1550) >= ((***l_1764) = (safe_sub_func_uint32_t_u_u(0xC5E40E45L, (p_4 <= (l_1738[0].f0 != l_1555)))))) == (**g_551)) , 0xD2413F54L), l_1747))), 0x46B7A062L))), 0x807FL)) || l_1787))))))
                    { /* block id: 724 */
                        int32_t l_1802 = (-7L);
                        uint64_t l_1806 = 6UL;
                        if ((*g_1234))
                            break;
                        ++l_1803;
                        l_1806 = l_1802;
                    }
                    else
                    { /* block id: 728 */
                        union U0 **l_1808[6][4] = {{&l_1807,&g_1541,(void*)0,(void*)0},{&l_1807,&g_1541,&l_1807,&g_1541},{&g_1541,(void*)0,&l_1807,&g_1541},{&l_1807,&g_1541,(void*)0,(void*)0},{&g_1541,&g_1541,&l_1807,&l_1807},{&g_1541,(void*)0,(void*)0,&g_1541}};
                        const uint32_t l_1833 = 0xDBBBF3E0L;
                        int32_t l_1835[2][7][10] = {{{0xF930B7F3L,1L,(-1L),(-1L),6L,0x09F9CB73L,0xD25F5B67L,6L,0L,0x5E2AA146L},{0L,0x09F9CB73L,(-1L),6L,2L,6L,2L,6L,(-1L),0x09F9CB73L},{(-5L),(-7L),(-1L),0xF930B7F3L,0xE162DC0AL,0x5E2AA146L,1L,0x09F9CB73L,0x4EF3ACEFL,0L},{(-1L),0x6CE6418DL,(-4L),0L,0x4B2DD8CEL,0x5E2AA146L,(-5L),(-5L),0x5E2AA146L,0x4B2DD8CEL},{(-5L),0x05AB8F36L,0x05AB8F36L,(-5L),(-1L),6L,0x4EF3ACEFL,0L,0xD25F5B67L,1L},{0L,(-4L),0x6CE6418DL,(-1L),6L,0x09F9CB73L,0x05AB8F36L,0x4EF3ACEFL,0xD25F5B67L,0x4EF3ACEFL},{0xF930B7F3L,(-1L),(-7L),(-5L),(-7L),(-1L),0xF930B7F3L,0xE162DC0AL,0x5E2AA146L,1L}},{{6L,(-1L),0x09F9CB73L,0L,(-5L),1L,0L,1L,0x4EF3ACEFL,0xE162DC0AL},{(-1L),(-1L),1L,0xF930B7F3L,0x6CE6418DL,0x6CE6418DL,0xF930B7F3L,1L,(-1L),(-1L)},{0x5E2AA146L,(-1L),1L,6L,1L,0x09F9CB73L,0x0E9609DCL,0x4B2DD8CEL,1L,0xD25F5B67L},{0x5E2AA146L,6L,0L,0x05AB8F36L,0xE162DC0AL,1L,0L,1L,0xE162DC0AL,0x05AB8F36L},{0xE162DC0AL,0x0E9609DCL,0xE162DC0AL,(-1L),(-7L),1L,0xD25F5B67L,0L,0x4EF3ACEFL,6L},{0x0E9609DCL,(-7L),0L,0x5E2AA146L,0xD25F5B67L,2L,0x6CE6418DL,0L,0L,0x6CE6418DL},{1L,0x4EF3ACEFL,0xE162DC0AL,0xE162DC0AL,0x4EF3ACEFL,1L,0L,1L,(-5L),0L}}};
                        int i, j, k;
                        (*g_1540) = l_1807;
                        l_1834 = ((**g_561) = ((((0xEF120E4E25644E67LL && (l_1787 , ((safe_div_func_int32_t_s_s((p_3 ^ (l_1553[0][4][0] = (l_1701 = (~(safe_mul_func_uint8_t_u_u((g_486[2][0] = ((((*g_497) |= ((--(*g_56)) , (safe_rshift_func_uint16_t_u_s(0x978DL, 12)))) >= (p_4 , (safe_div_func_uint8_t_u_u((((*l_1716) = ((((*g_1431) , ((safe_sub_func_int8_t_s_s(l_1553[4][6][1], (safe_add_func_int8_t_s_s((safe_div_func_uint8_t_u_u((l_1687[3][1][0] |= (safe_lshift_func_int16_t_s_u((safe_sub_func_int64_t_s_s(((((l_1552 &= l_1830) || l_1787) || 18446744073709551611UL) <= 1UL), p_3)), 13))), l_1831[0])), l_1832[1])))) & 1UL)) , 0x9C6F2E4353E3B344LL) | 0x7F9E118339B979EALL)) != 0x9C6C38B10C50DA3DLL), p_3)))) | l_1833)), 0x11L)))))), 6L)) < 0xC9L))) ^ p_4) , p_4) , &l_1801[3][0]));
                        --l_1843;
                        if (l_1835[0][3][3])
                            break;
                    }
                    if (l_1550)
                        break;
                    for (l_1836 = 0; (l_1836 < (-18)); l_1836 = safe_sub_func_uint8_t_u_u(l_1836, 6))
                    { /* block id: 746 */
                        uint8_t **l_1868 = &l_1694[0][0][0];
                        int32_t l_1869 = 0xAF34D9A6L;
                        const uint32_t **l_1876 = &l_1875[0];
                        l_1838 |= (((safe_div_func_uint64_t_u_u(p_3, p_4)) < (safe_sub_func_int8_t_s_s((*l_1834), (safe_lshift_func_int16_t_s_s((0x3D4DL != ((**g_1111) = (**g_1404))), (~((safe_rshift_func_uint16_t_u_u(((safe_add_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((&l_1623 != (void*)0), (safe_sub_func_uint32_t_u_u((l_1842[4] , (safe_lshift_func_uint16_t_u_s(((((*l_1868) = ((safe_mul_func_uint8_t_u_u((*l_1834), l_1787)) , &g_1538)) != (void*)0) != (**g_445)), p_3))), p_4)))), g_477[3])) >= 1L), 2)) == (*g_56)))))))) || p_4);
                        (*g_562) = (void*)0;
                        l_1870++;
                        (*l_1834) &= (safe_lshift_func_uint8_t_u_u((g_486[2][0] = (((*l_1876) = l_1875[0]) == (void*)0)), 1));
                    }
                }
            }
            for (g_315 = 0; (g_315 <= 0); g_315 += 1)
            { /* block id: 760 */
                volatile uint16_t *** volatile *l_1877 = &g_734;
                int32_t l_1895 = (-6L);
                uint64_t l_1919 = 2UL;
                l_1877 = &g_734;
                for (g_240.f1 = 0; (g_240.f1 <= 7); g_240.f1 += 1)
                { /* block id: 764 */
                    uint32_t **l_1896 = (void*)0;
                    int32_t l_1918[9] = {0xAC1CC893L,0xAC1CC893L,0xAC1CC893L,0xAC1CC893L,0xAC1CC893L,0xAC1CC893L,0xAC1CC893L,0xAC1CC893L,0xAC1CC893L};
                    int32_t **l_1921 = &l_1554[6];
                    int i;
                    for (l_1552 = 0; (l_1552 >= 0); l_1552 -= 1)
                    { /* block id: 767 */
                        int32_t l_1886 = 0x224DEEF8L;
                        int32_t **l_1893 = &l_1554[6];
                        int i;
                        l_1553[4][6][1] |= (safe_mul_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((((((((*l_1717) = ((l_1701 |= 255UL) , (safe_rshift_func_uint8_t_u_u((l_1897[4] = (g_486[2][0] = ((safe_mul_func_uint16_t_u_u((((l_1886 = g_884[g_240.f1]) > 0xCCL) ^ (((p_4 , ((safe_mul_func_float_f_f(((g_1889 <= (((safe_lshift_func_uint8_t_u_s((((*g_561) = l_1892) != (l_1893 = l_1893)), 5)) >= (0x0B61BF74CC8B3ABDLL && (((**g_445) , &l_1555) == (void*)0))) == p_3)) , l_1894), l_1895)) , l_1896)) != l_1896) <= (-1L))), 0xC68CL)) || 4294967295UL))), 0)))) >= 0x00E3L) , (***g_1403)) >= p_3) | (*g_138)) || (**l_1893)), 0UL)), p_3));
                        if ((**l_1893))
                            continue;
                        l_1895 = ((!(((***l_1575) = (safe_sub_func_float_f_f((p_3 >= p_3), ((*g_1303) > (safe_sub_func_float_f_f(((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((p_4 >= p_3), ((safe_add_func_float_f_f(l_1911, ((((safe_mul_func_float_f_f((safe_add_func_float_f_f((&g_697[2] == &l_1600), (safe_div_func_float_f_f(0x9.4FC62Ap-38, 0x8.1p-1)))), l_1918[5])) >= l_1687[3][2][4]) > l_1830) != (-0x2.5p-1)))) != p_4))) >= l_1687[3][2][4]), (-0x7.Ap+1))), l_1919)) , l_1830), l_1920[5])))))) >= p_3)) , p_4);
                    }
                    for (g_1538 = 0; (g_1538 <= 7); g_1538 += 1)
                    { /* block id: 782 */
                        return (*g_1234);
                    }
                    (*l_1921) = ((*l_1892) = &l_1839[8]);
                    for (g_441 = 0; (g_441 <= 0); g_441 += 1)
                    { /* block id: 789 */
                        (*g_74) ^= (-1L);
                        if (p_3)
                            continue;
                        (***g_561) = (**g_562);
                        (**g_562) &= (l_1922 != &g_560[0][2]);
                    }
                }
            }
        }
    }
    else
    { /* block id: 798 */
        int32_t *l_1923[5];
        float **l_1924 = &l_1786;
        int i;
        for (i = 0; i < 5; i++)
            l_1923[i] = &g_83;
        (*g_562) = l_1923[0];
        (*g_1783) = (0x1.BA06A1p-45 >= (((*l_1786) = 0x9.801D0Ap-63) > ((-0x5.1p-1) > (l_1924 == (void*)0))));
    }
    return p_4;
}


/* ------------------------------------------ */
/* 
 * reads : g_1540
 * writes: g_1541
 */
static uint16_t  func_18(float  p_19, int16_t  p_20, uint64_t  p_21, uint8_t  p_22, uint8_t  p_23)
{ /* block id: 595 */
    union U0 *l_1539 = &g_240;
    int32_t l_1542 = 0xFA9451B4L;
    int32_t *l_1543 = &g_83;
    int32_t *l_1544 = (void*)0;
    int32_t *l_1545[3][10][3] = {{{&g_83,&g_104,(void*)0},{(void*)0,(void*)0,&l_1542},{&g_83,&g_83,&g_83},{(void*)0,(void*)0,(void*)0},{&g_104,&g_83,&g_104},{&g_1468,(void*)0,&l_1542},{&g_83,&g_104,&g_104},{&l_1542,&g_1468,(void*)0},{(void*)0,&g_83,&g_83},{&l_1542,&l_1542,&l_1542}},{{&g_83,(void*)0,(void*)0},{&g_1468,&l_1542,&g_1468},{&g_104,&g_83,&g_83},{(void*)0,&g_1468,&g_1468},{&g_83,&g_104,(void*)0},{(void*)0,(void*)0,&l_1542},{&g_83,&g_83,&g_83},{(void*)0,(void*)0,(void*)0},{&g_104,&g_83,&g_104},{&g_1468,(void*)0,&l_1542}},{{&g_83,&g_104,&g_104},{&l_1542,&g_1468,(void*)0},{(void*)0,&g_83,&g_83},{&l_1542,&l_1542,&l_1542},{&g_83,(void*)0,(void*)0},{&g_1468,&l_1542,&g_1468},{&g_104,&g_83,&g_83},{(void*)0,&g_1468,&g_1468},{&g_83,&g_104,(void*)0},{(void*)0,(void*)0,&l_1542}}};
    int64_t l_1546 = 2L;
    uint16_t l_1547 = 0UL;
    int i, j, k;
    (*g_1540) = l_1539;
    ++l_1547;
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_408 g_104 g_1111 g_1112 g_135 g_56 g_57 g_498 g_499 g_100 g_497 g_240.f3 g_1347 g_315 g_551 g_552 g_553 g_445 g_446 g_102 g_83 g_1403 g_344 g_134 g_734 g_735 g_299 g_595 g_385 g_486 g_1405 g_37 g_1468 g_1404 g_129 g_217 g_218 g_562 g_1430 g_1431 g_157 g_74 g_1510 g_139 g_1511 g_1303 g_600 g_601 g_240 g_1538 g_116
 * writes: g_104 g_102 g_37 g_135 g_315 g_477 g_100 g_240.f3 g_83 g_129 g_1403 g_1430 g_116 g_134 g_486 g_157 g_74 g_447 g_208
 */
static float  func_26(uint64_t  p_27, int8_t  p_28, uint32_t  p_29, uint64_t  p_30)
{ /* block id: 515 */
    uint32_t l_1315 = 0xDABD0538L;
    int32_t l_1324 = (-1L);
    const int32_t l_1325 = 0x3AE88B44L;
    int32_t l_1342 = (-4L);
    float *l_1344 = &g_134;
    float **l_1343[1];
    int32_t **l_1373 = &g_74;
    int32_t l_1382 = (-1L);
    int32_t l_1390 = 4L;
    int32_t l_1391 = 0L;
    int32_t l_1392 = (-5L);
    int32_t l_1395 = 0L;
    int32_t l_1396 = 0xCBE82D46L;
    int32_t l_1397[4][10] = {{(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L},{(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L},{(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L},{(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L,(-1L),6L}};
    int16_t ***l_1408 = (void*)0;
    int32_t *l_1427 = &g_208;
    int32_t **l_1426 = &l_1427;
    uint32_t * const *l_1443 = &g_446;
    uint32_t * const **l_1442 = &l_1443;
    uint32_t * const ***l_1441[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const int32_t ***l_1446 = (void*)0;
    const int32_t ****l_1445[9] = {&l_1446,&l_1446,&l_1446,&l_1446,&l_1446,&l_1446,&l_1446,&l_1446,&l_1446};
    const int32_t *****l_1444 = &l_1445[1];
    int32_t l_1451[2];
    int32_t *l_1479 = &g_240.f2;
    int8_t *l_1492 = &g_1217;
    union U0 l_1525[5] = {{0x37288399FF03D798LL},{0x37288399FF03D798LL},{0x37288399FF03D798LL},{0x37288399FF03D798LL},{0x37288399FF03D798LL}};
    int i, j;
    for (i = 0; i < 1; i++)
        l_1343[i] = &l_1344;
    for (i = 0; i < 2; i++)
        l_1451[i] = 2L;
lbl_1495:
    l_1324 = (l_1315 != ((l_1315 | (safe_mod_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((p_30 < 4294967295UL), 2)), (l_1315 ^ p_27)))) , (safe_sub_func_uint32_t_u_u(0x997E7479L, ((safe_mod_func_int32_t_s_s((&g_884[0] == (void*)0), 4294967286UL)) > l_1315)))));
lbl_1469:
    (*g_408) ^= l_1325;
    for (g_102 = (-19); (g_102 != 6); ++g_102)
    { /* block id: 520 */
        int8_t l_1332 = 0xC5L;
        uint32_t *l_1335 = &g_135;
        const uint32_t **l_1346 = (void*)0;
        const uint32_t ***l_1345 = &l_1346;
        int32_t ***l_1367 = &g_562;
        int32_t l_1374 = 0x2C2297C1L;
        int32_t l_1384 = (-1L);
        int32_t l_1388 = (-10L);
        int32_t l_1389[9] = {0x4A10B840L,0x4A10B840L,0x4A10B840L,0x4A10B840L,0x4A10B840L,0x4A10B840L,0x4A10B840L,0x4A10B840L,0x4A10B840L};
        uint64_t l_1398 = 1UL;
        int32_t *****l_1440[4];
        float *l_1467[3][6] = {{&g_116,&g_116,&g_116,&g_116,&g_134,&g_116},{&g_116,&g_134,&g_116,&g_116,&g_116,&g_116},{&g_116,&g_116,&g_116,(void*)0,&g_116,&g_116}};
        int32_t *l_1513 = (void*)0;
        union U0 l_1524 = {0x24C33E2EEE6D3CCELL};
        uint64_t **l_1526[8] = {(void*)0,&g_56,(void*)0,&g_56,(void*)0,&g_56,(void*)0,&g_56};
        int32_t l_1535 = 6L;
        uint32_t *l_1536 = &g_447;
        uint8_t *l_1537 = &g_486[2][0];
        int i, j;
        for (i = 0; i < 4; i++)
            l_1440[i] = &g_560[0][1];
        if ((((((safe_lshift_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((**g_1111) = (l_1324 , (0x13L > l_1332))), ((safe_div_func_int8_t_s_s((((--(*l_1335)) & (0xB011L ^ ((safe_lshift_func_int16_t_s_u(1L, (safe_mod_func_int16_t_s_s(((l_1342 ^ ((((((l_1343[0] == &l_1344) , l_1345) != (void*)0) >= 18446744073709551608UL) < (*g_56)) != 0x1A28L)) , 0L), (**g_498))))) > p_30))) , (-4L)), p_28)) , 0UL))), p_30)) | (*g_497)) || p_30) < g_1347) >= (-1L)))
        { /* block id: 523 */
            int8_t l_1355 = 0x92L;
            int32_t l_1380 = 6L;
            int32_t l_1381 = 0xA5DD0E91L;
            int32_t l_1383 = 1L;
            int32_t l_1385[1];
            int16_t ***l_1409 = (void*)0;
            uint8_t l_1433 = 0x5DL;
            uint8_t *l_1462 = &g_486[3][0];
            float *l_1466 = &g_134;
            int i;
            for (i = 0; i < 1; i++)
                l_1385[i] = 0xDEBDC309L;
            if (p_27)
            { /* block id: 524 */
                uint8_t *l_1354[8] = {&g_486[2][0],&g_315,&g_486[2][0],&g_315,&g_486[2][0],&g_315,&g_486[2][0],&g_315};
                int64_t *l_1368 = &g_477[1];
                const int32_t l_1371[8] = {0x1D5E91B6L,0xC9B8C319L,0x1D5E91B6L,0xC9B8C319L,0x1D5E91B6L,0xC9B8C319L,0x1D5E91B6L,0xC9B8C319L};
                int32_t *l_1372 = &g_83;
                int i;
                l_1374 = (((((((*l_1372) |= ((((**g_498) || (safe_sub_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((safe_mod_func_int8_t_s_s((((--g_315) ^ 250UL) , (p_28 = ((*g_497) = (((safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((safe_unary_minus_func_int64_t_s((((((*l_1368) = (safe_rshift_func_int8_t_s_u(l_1355, (l_1367 != (void*)0)))) > (**g_551)) >= (*g_497)) || (((**g_498) ^= p_27) >= ((safe_div_func_int8_t_s_s((((-4L) == p_28) < p_27), (-1L))) ^ l_1315))))), 0xC5DEL)), l_1371[7])), 12)) , (-5L)) & 0x65684BDBC27AF1F5LL)))), p_27)), l_1355)), g_135))) , (**g_445)) < p_30)) | 4294967295UL) , l_1373) != l_1373) < p_29) || (*g_552));
                return l_1355;
            }
            else
            { /* block id: 533 */
                int64_t l_1386 = 0x832E9F58741E559CLL;
                int32_t l_1387 = 0x8BE3F06FL;
                int32_t l_1393 = 0x9F8A2366L;
                int32_t l_1394 = 0xDDB94086L;
                int32_t **l_1418 = (void*)0;
                for (p_29 = 0; (p_29 <= 0); p_29 += 1)
                { /* block id: 536 */
                    int32_t *l_1375 = &g_104;
                    int32_t *l_1376 = &g_104;
                    int32_t *l_1377 = &l_1342;
                    int32_t *l_1378 = (void*)0;
                    int32_t *l_1379[4][4] = {{(void*)0,(void*)0,&g_104,(void*)0},{(void*)0,&l_1324,&l_1324,(void*)0},{&l_1324,(void*)0,&l_1324,&l_1324},{(void*)0,(void*)0,&g_104,(void*)0}};
                    int i, j;
                    l_1398--;
                }
                for (g_129 = 0; (g_129 > 6); g_129 = safe_add_func_int16_t_s_s(g_129, 3))
                { /* block id: 541 */
                    int16_t ** const **l_1406 = (void*)0;
                    int16_t ** const **l_1407 = &g_1403;
                    int32_t l_1423[8] = {0xE94F1424L,0x6FC8D032L,0xE94F1424L,0x6FC8D032L,0xE94F1424L,0x6FC8D032L,0xE94F1424L,0x6FC8D032L};
                    int32_t ***l_1428 = (void*)0;
                    int32_t ***l_1432 = &g_1430;
                    int i;
                    if ((((*l_1407) = g_1403) == (l_1409 = l_1408)))
                    { /* block id: 544 */
                        return (*g_344);
                    }
                    else
                    { /* block id: 546 */
                        int32_t l_1424 = 0xE5E84276L;
                        int32_t l_1425 = 0xE1AFF269L;
                        l_1425 |= (l_1393 |= (safe_rshift_func_uint8_t_u_u((0x22EC6F24L & (safe_add_func_uint8_t_u_u((l_1387 = ((void*)0 != &g_1403)), (safe_div_func_int8_t_s_s((safe_mod_func_uint64_t_u_u((((void*)0 != l_1418) , ((*g_734) == ((safe_add_func_float_f_f(((safe_mul_func_float_f_f((p_30 , (((void*)0 == &g_217) <= l_1423[1])), p_30)) == l_1424), l_1424)) , (void*)0))), 0x045359C16E2635A6LL)), 0x3DL))))), 0)));
                    }
                    (*l_1432) = l_1426;
                    if (p_30)
                        continue;
                }
                l_1433--;
            }
            l_1381 = ((((*l_1344) = ((*g_385) = (g_299 , (((((safe_mod_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(g_595, (l_1440[2] == ((((void*)0 != l_1441[3]) != (p_30 != (0x0.CC6349p-0 != (-0x10.Dp-1)))) , l_1444)))), 0x8BL)) || p_29) == 7L) & (-2L)) , (-0x3.3p-1))))) <= p_30) < 0x0.Ap-1);
            l_1385[0] = (safe_mod_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((p_29 | (l_1451[1] && (safe_lshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_s(((*l_1462) ^= (p_27 & p_29)), (l_1433 , (((g_157[8] = ((*l_1335) |= (*g_446))) >= p_29) > ((safe_unary_minus_func_int16_t_s(((--(*g_499)) >= (*g_1405)))) || ((l_1381 = (l_1466 == l_1467[0][5])) < 4294967295UL)))))) && (*g_497)), p_27)), 0xF94B9DA6L)), 7)), g_1468)))), p_29)), l_1380));
        }
        else
        { /* block id: 565 */
            int32_t *l_1478 = &g_208;
            int32_t l_1489 = 0x0607A404L;
            int16_t *l_1490 = &g_129;
            uint8_t *l_1491 = &g_315;
            int8_t **l_1493 = &l_1492;
            const int8_t l_1512 = 1L;
            int64_t * const l_1521 = &g_528;
            if (g_240.f3)
                goto lbl_1469;
            if ((safe_lshift_func_uint16_t_u_u(p_27, ((((((safe_rshift_func_int16_t_s_u(((***g_1403) = (safe_unary_minus_func_uint64_t_u((safe_lshift_func_int8_t_s_s(((((p_30 <= 0xD24EL) , ((-1L) & (+((*l_1426) == (l_1479 = l_1478))))) | (((*l_1493) = ((((*g_497) = 0x4FL) , (((safe_lshift_func_uint16_t_u_s(((!((*l_1491) = (safe_lshift_func_int16_t_s_u(((*l_1490) ^= ((((++(*g_499)) > (((safe_sub_func_float_f_f((p_28 , 0x1.Ep-1), (-0x2.Bp-1))) , l_1489) <= 5L)) < p_29) , (***g_1403))), 14)))) > (*g_217)), 7)) | p_28) , (*g_1405))) , l_1492)) != &p_28)) > l_1489), l_1489))))), 5)) < 1L) <= g_486[2][0]) || l_1489) & 0x0C93L) && 0L))))
            { /* block id: 574 */
                int32_t *l_1494 = &g_83;
                (**l_1367) = l_1494;
                if (g_57)
                    goto lbl_1495;
                (*g_1511) = (p_30 , func_60((safe_sub_func_int8_t_s_s(0xB8L, (safe_mul_func_uint8_t_u_u(((((1UL ^ l_1489) , ((safe_add_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((p_30 | (0xE2FD164EL || ((**l_1373) = ((((safe_lshift_func_int8_t_s_u((((((*g_217) , l_1478) == ((*l_1426) = (*g_1430))) , l_1491) != (void*)0), 4)) < g_157[8]) < (**g_1404)) ^ 5L)))), 0)), l_1489)), p_27)), 5UL)) != 18446744073709551609UL)) , (*g_497)) != l_1489), 246UL)))), g_1510, g_139));
            }
            else
            { /* block id: 580 */
                (**l_1367) = l_1513;
            }
            (*g_1303) = (safe_div_func_float_f_f((+(l_1489 = p_30)), p_30));
            l_1489 = (safe_mod_func_int32_t_s_s((((void*)0 != (***g_600)) && (0x8624L < p_28)), (safe_add_func_int32_t_s_s((((void*)0 != l_1521) || (&l_1426 == (void*)0)), (0xCCB964BDL && l_1489)))));
        }
        (*g_1303) = (p_27 , ((*l_1344) = (((*l_1427) = (safe_mod_func_int64_t_s_s((l_1524 , (((l_1525[4] , l_1526[2]) != &g_56) || (((safe_lshift_func_uint8_t_u_u(0xBCL, (safe_rshift_func_uint8_t_u_s(((*l_1537) = (safe_lshift_func_int8_t_s_u((safe_sub_func_int32_t_s_s((((*l_1536) = (((g_240 , ((l_1535 , 4294967288UL) ^ (*g_446))) == 6L) && 7L)) , (-2L)), 9L)), 0))), 5)))) < 0x53625558C16B1D70LL) != 18446744073709551615UL))), g_1538))) , (-0x5.6p-1))));
    }
    return (*g_385);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t * func_40(uint64_t * p_41, uint64_t * const  p_42, float  p_43, uint64_t * p_44)
{ /* block id: 510 */
    union U0 *l_1311 = &g_240;
    union U0 **l_1310 = &l_1311;
    uint64_t *l_1312 = &g_57;
    (*l_1310) = (void*)0;
    return l_1312;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t * func_45(float  p_46, uint16_t  p_47, uint64_t * p_48, uint64_t * p_49, uint32_t  p_50)
{ /* block id: 507 */
    const int32_t *l_1308 = &g_104;
    l_1308 = (void*)0;
    return p_48;
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_74 g_83 g_5 g_37 g_56 g_131 g_299 g_102 g_208 g_445 g_240.f1 g_408 g_104 g_240.f3 g_447 g_138 g_139 g_135 g_125 g_100 g_562 g_916 g_486 g_575 g_600 g_601 g_446 g_497 g_217 g_218 g_1111 g_551 g_552 g_1119 g_441 g_645 g_134 g_498 g_499 g_315 g_528 g_240.f0 g_1234 g_344 g_240 g_232 g_1112 g_1297 g_385 g_116 g_1303
 * writes: g_57 g_37 g_441 g_299 g_102 g_56 g_74 g_83 g_486 g_315 g_528 g_104 g_134 g_240.f3 g_667 g_135 g_116 g_208
 */
static float  func_53(uint64_t * p_54, uint32_t  p_55)
{ /* block id: 3 */
    int32_t l_588[2];
    uint32_t *l_998 = (void*)0;
    uint16_t l_1008[8];
    int8_t l_1075[1][10] = {{0L,5L,0L,5L,0L,5L,0L,5L,0L,5L}};
    int16_t *l_1103 = (void*)0;
    int16_t **l_1104 = &l_1103;
    float l_1105 = 0x2.538280p-20;
    int32_t l_1161[2];
    int32_t *l_1162 = &l_1161[0];
    uint64_t *l_1282 = &g_131[3][3][1];
    const int32_t l_1294 = 0xAC2BCC6EL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_588[i] = 3L;
    for (i = 0; i < 8; i++)
        l_1008[i] = 1UL;
    for (i = 0; i < 2; i++)
        l_1161[i] = 1L;
    for (g_57 = 0; (g_57 == 24); ++g_57)
    { /* block id: 6 */
        union U0 l_75[2] = {{0x531EAA473ECFB861LL},{0x531EAA473ECFB861LL}};
        int32_t *l_82[8][7] = {{&g_83,&g_83,(void*)0,&g_83,&g_83,&g_83,&g_83},{&g_83,&g_83,(void*)0,(void*)0,&g_83,&g_83,&g_83},{(void*)0,&g_83,(void*)0,&g_83,&g_83,&g_83,&g_83},{&g_83,(void*)0,(void*)0,(void*)0,&g_83,&g_83,&g_83},{&g_83,&g_83,&g_83,&g_83,&g_83,(void*)0,&g_83},{&g_83,&g_83,&g_83,&g_83,(void*)0,&g_83,&g_83},{&g_83,&g_83,&g_83,&g_83,&g_83,&g_83,&g_83},{&g_83,&g_83,&g_83,&g_83,&g_83,(void*)0,&g_83}};
        uint64_t **l_587 = &g_56;
        int32_t l_974 = 0x753D278AL;
        float l_1050 = (-0x1.Bp+1);
        uint32_t l_1056 = 0UL;
        int i, j;
        (*g_916) = func_60(((&g_57 == ((*l_587) = func_64(func_68(g_74, g_74, l_75[0], (safe_div_func_uint8_t_u_u(((safe_mod_func_uint32_t_u_u(((l_82[7][1] == (void*)0) == (&g_57 == (void*)0)), (safe_div_func_uint32_t_u_u(g_83, p_55)))) , g_5[3][0]), g_5[1][0])), &g_57), p_54, g_5[0][0]))) == l_588[0]), p_55, g_100[1]);
        for (g_83 = 0; (g_83 <= 1); g_83 += 1)
        { /* block id: 352 */
            uint32_t l_933 = 1UL;
            int32_t l_956 = 8L;
            int32_t l_957 = (-1L);
            int64_t l_973 = 0x08722AFCE0DA7C34LL;
            int32_t l_1051 = (-1L);
            int32_t l_1052 = 0x1E9AE273L;
            int32_t l_1053 = 5L;
            int32_t l_1054 = 0x9625C97CL;
            int32_t l_1055[5] = {2L,2L,2L,2L,2L};
            int i;
        }
    }
    if (((0x873FB436L > (safe_sub_func_int64_t_s_s(l_1075[0][5], (safe_div_func_int16_t_s_s((((+(safe_rshift_func_int16_t_s_s(((safe_rshift_func_int8_t_s_s((((safe_div_func_int64_t_s_s(l_588[0], (l_1008[7] || (safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s(p_55, 5)), (((safe_div_func_int16_t_s_s((0xF5AC129E783157D3LL < 3UL), ((safe_sub_func_uint32_t_u_u((((safe_sub_func_uint8_t_u_u(p_55, (safe_mul_func_int16_t_s_s((((safe_lshift_func_int16_t_s_u(((((((*l_1104) = l_1103) == &g_1047) <= g_486[2][0]) , p_55) > p_55), p_55)) > l_588[0]) ^ g_575), 0xF2D4L)))) > p_55) != 0xA061AD50L), 0UL)) ^ p_55))) || (****g_600)) <= p_55))), p_55)), (*g_497)))))) && p_55) != 0x3C8C7AD2FE4A2315LL), (*g_497))) >= p_55), l_588[0]))) , (*p_54)) >= l_1075[0][5]), l_1008[7]))))) ^ l_1008[7]))
    { /* block id: 421 */
        float l_1116 = 0x1.Ep-1;
        int32_t *l_1123 = &l_588[0];
        int32_t l_1165 = 0xBC0FCE16L;
        int32_t l_1173 = (-1L);
        int32_t l_1174 = 0x35E68F3BL;
        int32_t l_1175 = 0L;
        int32_t l_1176[6][7] = {{0L,3L,0xE84236FAL,3L,0L,1L,1L},{0L,3L,0xE84236FAL,3L,0L,1L,1L},{0L,3L,0xE84236FAL,3L,0L,1L,1L},{0L,3L,0xE84236FAL,3L,0L,1L,1L},{0L,3L,0xE84236FAL,0x89517F33L,1L,0xE84236FAL,0xE84236FAL},{1L,0x89517F33L,(-1L),0x89517F33L,1L,0xE84236FAL,0xE84236FAL}};
        uint64_t l_1181 = 0xDDA777DB22B5B1B3LL;
        uint32_t l_1209[9];
        int16_t l_1213 = 0x3B46L;
        int32_t l_1221 = 8L;
        uint32_t l_1222 = 0xFA8EDAE9L;
        int64_t *l_1227[2][4][9] = {{{&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0},{&g_667,&g_528,&g_667,&g_528,&g_477[3],&g_477[3],&g_667,(void*)0,(void*)0},{&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0},{&g_667,&g_528,&g_667,&g_528,&g_477[3],&g_477[3],&g_667,(void*)0,(void*)0}},{{&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0},{&g_667,&g_528,&g_667,&g_528,&g_477[3],&g_477[3],&g_667,(void*)0,(void*)0},{&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0,&g_477[3],(void*)0,(void*)0},{&g_667,&g_528,&g_667,&g_528,&g_477[3],&g_477[3],&g_667,(void*)0,(void*)0}}};
        int64_t * const * const l_1226 = &l_1227[1][1][3];
        int64_t * const * const *l_1225[4] = {&l_1226,&l_1226,&l_1226,&l_1226};
        int32_t *l_1229[8][2][2] = {{{(void*)0,&l_588[0]},{(void*)0,&l_1161[0]}},{{&g_104,&g_104},{&l_1161[0],(void*)0}},{{&l_588[0],(void*)0},{&l_1161[0],&g_104}},{{&g_104,&l_1161[0]},{(void*)0,&l_588[0]}},{{(void*)0,&l_1161[0]},{&g_104,&g_104}},{{&l_1161[0],(void*)0},{&l_588[0],(void*)0}},{{&l_1161[0],&g_104},{&g_104,&l_1161[0]}},{{(void*)0,&l_588[0]},{(void*)0,&l_1161[0]}}};
        uint32_t l_1230 = 0xEA5F3242L;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1209[i] = 18446744073709551615UL;
        if ((safe_rshift_func_uint8_t_u_u((++g_486[3][0]), p_55)))
        { /* block id: 423 */
            int64_t *l_1113 = &g_528;
            int32_t l_1117 = 1L;
            uint8_t *l_1118 = &g_486[0][0];
            int32_t l_1120 = 0xE58D7903L;
            int16_t **l_1130 = &l_1103;
            int16_t l_1136 = 0x0817L;
            int32_t l_1177 = 0xD95ADAE3L;
            int32_t l_1178 = (-1L);
            int32_t l_1179 = 0x2144219CL;
            int32_t l_1180 = (-1L);
            int32_t *l_1184[3];
            uint8_t l_1185 = 0UL;
            int i;
            for (i = 0; i < 3; i++)
                l_1184[i] = &l_1117;
            if ((l_1120 |= ((((((*g_217) < (&g_217 == &p_54)) , (!((*l_1118) |= ((((8L == (g_1111 == &l_1103)) != (((((*g_551) != l_1113) == (((((((((safe_mul_func_uint16_t_u_u(p_55, p_55)) != (-6L)) == 0L) | p_55) & l_1117) == (***g_601)) || 0x10L) <= 2L) > l_1117)) , l_1117) , 0xE5L)) <= 0x62L) && 0x498EBABDL)))) & l_1117) > 7L) <= g_1119)))
            { /* block id: 426 */
                return p_55;
            }
            else
            { /* block id: 428 */
                uint8_t l_1129 = 0x29L;
                int32_t l_1142 = 0x5D636B97L;
                int32_t *l_1166 = &l_588[0];
                int32_t *l_1167 = (void*)0;
                int32_t *l_1168 = &l_1165;
                int32_t *l_1169 = (void*)0;
                int32_t *l_1170 = (void*)0;
                int32_t *l_1171 = (void*)0;
                int32_t *l_1172[10] = {&l_588[1],&l_1165,&l_1165,&l_588[1],&l_1165,&l_1165,&l_588[1],&l_1165,&l_1165,&l_588[1]};
                int i;
                for (g_441 = 0; (g_441 == 2); g_441 = safe_add_func_int8_t_s_s(g_441, 1))
                { /* block id: 431 */
                    uint64_t l_1132 = 3UL;
                    int8_t l_1138 = 6L;
                    (*g_562) = l_1123;
                }
                (*l_1162) ^= 1L;
                (*l_1162) ^= (safe_rshift_func_int8_t_s_u((*g_497), 7));
                ++l_1181;
            }
            ++l_1185;
            return (*g_645);
        }
        else
        { /* block id: 460 */
            uint8_t *l_1210 = &g_315;
            int64_t *l_1211 = &g_528;
            uint8_t *l_1212 = &g_486[1][0];
            uint64_t l_1214 = 0x6056CDD7F1C3B557LL;
            int32_t *l_1215 = &l_1175;
            int32_t *l_1216[4];
            int32_t l_1218 = 4L;
            int32_t l_1220 = 0x22ECD74EL;
            int64_t * const * const *l_1228 = (void*)0;
            int i;
            for (i = 0; i < 4; i++)
                l_1216[i] = &l_1176[4][2];
            (*l_1215) ^= (((safe_add_func_int16_t_s_s(((*g_217) <= (safe_sub_func_int8_t_s_s((-3L), (safe_add_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((((((*l_1212) &= ((safe_mod_func_uint8_t_u_u((((*l_1211) ^= (1UL | (safe_mul_func_uint8_t_u_u(1UL, ((*l_1210) ^= (safe_mod_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u((((**g_498) , ((((*l_1123) = (safe_div_func_uint64_t_u_u(((*l_1162) != ((((~(*l_1123)) || (safe_lshift_func_int8_t_s_s((-6L), ((***g_601) >= 0x463E0651L)))) > 7L) != 0UL)), 1L))) , (void*)0) == (void*)0)) || g_5[2][1]), (*g_499))) >= (*g_446)), l_1209[8]))))))) < (*g_56)), g_240.f0)) && (*l_1123))) == 1L) < l_1213) & l_1214), p_55)), (*g_497)))))), 0x9C56L)) <= (*p_54)) < (*g_56));
            --l_1222;
            (*l_1123) = (*l_1215);
            l_1228 = l_1225[3];
        }
        l_1230++;
    }
    else
    { /* block id: 471 */
        uint16_t * const l_1233 = &g_100[0];
        int32_t ***l_1274[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        const int8_t l_1278 = (-2L);
        uint64_t *l_1283 = (void*)0;
        int32_t ** const *l_1301 = (void*)0;
        float *l_1302 = (void*)0;
        int i;
        (*l_1162) = (l_1233 != (void*)0);
        if (g_441)
            goto lbl_1300;
        (*g_1234) ^= (*l_1162);
        if ((((--(**g_445)) > 0x7A00D9ACL) ^ (*p_54)))
        { /* block id: 475 */
            float *l_1275 = &l_1105;
            int32_t l_1276[4][1];
            float *l_1277 = &g_134;
            int8_t *l_1279[8][8][4] = {{{&l_1075[0][2],(void*)0,&g_1217,&g_731},{(void*)0,&l_1075[0][8],&l_1075[0][5],&g_1217},{&l_1075[0][5],&l_1075[0][7],&g_731,(void*)0},{(void*)0,(void*)0,&g_1217,&l_1075[0][5]},{&l_1075[0][4],&l_1075[0][5],&g_1217,&l_1075[0][5]},{&g_731,(void*)0,&l_1075[0][5],&g_731},{&l_1075[0][5],&l_1075[0][9],&l_1075[0][7],&g_731},{(void*)0,&l_1075[0][3],&l_1075[0][5],(void*)0}},{{&g_1217,&g_731,(void*)0,(void*)0},{&g_1217,&g_1217,&g_731,&g_731},{&g_1217,&g_731,&g_731,&g_731},{&g_731,&g_1217,&l_1075[0][7],&g_1217},{&l_1075[0][5],&l_1075[0][5],&g_1217,&g_731},{&g_1217,&g_731,&l_1075[0][5],&g_1217},{&l_1075[0][5],&l_1075[0][0],&g_1217,&l_1075[0][5]},{&l_1075[0][0],&g_731,&l_1075[0][5],(void*)0}},{{&l_1075[0][5],&g_1217,&l_1075[0][0],&l_1075[0][5]},{&g_1217,&g_731,&g_1217,&g_1217},{&g_731,&l_1075[0][5],&l_1075[0][5],&g_1217},{&l_1075[0][5],&l_1075[0][5],(void*)0,&l_1075[0][5]},{&l_1075[0][5],&g_1217,&l_1075[0][5],&l_1075[0][5]},{&g_731,&g_731,&l_1075[0][5],&g_731},{&l_1075[0][7],(void*)0,&g_1217,&l_1075[0][5]},{&g_731,&l_1075[0][5],&l_1075[0][5],&g_731}},{{&l_1075[0][3],&l_1075[0][5],&l_1075[0][5],(void*)0},{&l_1075[0][0],&l_1075[0][9],&l_1075[0][5],(void*)0},{&g_731,&g_731,&g_1217,(void*)0},{&l_1075[0][8],&l_1075[0][9],&g_731,(void*)0},{&g_1217,&l_1075[0][5],&l_1075[0][5],&g_731},{&l_1075[0][2],&l_1075[0][5],&g_1217,&l_1075[0][5]},{&l_1075[0][5],(void*)0,&g_1217,&g_731},{(void*)0,&g_731,&l_1075[0][0],&l_1075[0][5]}},{{&g_731,&g_1217,&g_1217,&l_1075[0][5]},{&g_1217,&l_1075[0][5],&g_731,&g_1217},{&l_1075[0][7],&l_1075[0][5],&g_1217,&g_1217},{&g_1217,&g_731,(void*)0,&l_1075[0][5]},{&l_1075[0][5],&g_1217,&l_1075[0][5],(void*)0},{&l_1075[0][8],&g_731,&l_1075[0][5],&l_1075[0][5]},{&g_731,&l_1075[0][0],(void*)0,&g_1217},{&l_1075[0][5],&g_731,&l_1075[0][5],&l_1075[0][5]}},{{&l_1075[0][5],&g_731,(void*)0,&g_1217},{&l_1075[0][7],(void*)0,&l_1075[0][5],&l_1075[0][7]},{&l_1075[0][7],&g_1217,&g_731,&l_1075[0][5]},{&g_1217,&g_1217,&g_731,&g_731},{&g_731,(void*)0,(void*)0,&l_1075[0][0]},{(void*)0,(void*)0,(void*)0,&g_731},{&l_1075[0][7],&g_731,&l_1075[0][5],&l_1075[0][3]},{&g_1217,&l_1075[0][9],&g_731,(void*)0}},{{&g_1217,&l_1075[0][5],(void*)0,&g_1217},{&g_731,&g_731,&g_1217,&l_1075[0][5]},{&l_1075[0][5],&l_1075[0][9],(void*)0,&l_1075[0][3]},{&l_1075[0][5],(void*)0,&l_1075[0][5],&l_1075[0][3]},{(void*)0,&g_731,(void*)0,&l_1075[0][5]},{&g_1217,&l_1075[0][0],&g_731,&g_1217},{(void*)0,(void*)0,&g_731,&l_1075[0][0]},{&l_1075[0][5],&g_1217,&g_731,&l_1075[0][5]}},{{(void*)0,&l_1075[0][5],&g_731,&l_1075[0][7]},{&g_1217,(void*)0,(void*)0,&g_1217},{(void*)0,&g_1217,&l_1075[0][5],&g_1217},{&l_1075[0][5],&g_731,(void*)0,&l_1075[0][5]},{&l_1075[0][5],&g_731,&g_1217,&l_1075[0][6]},{&l_1075[0][5],(void*)0,&g_1217,&g_731},{&l_1075[0][3],&g_1217,&g_731,(void*)0},{(void*)0,&l_1075[0][1],&g_731,(void*)0}}};
            int32_t l_1280 = (-1L);
            int32_t l_1281 = 0x851AF82EL;
            int32_t l_1284 = 1L;
            int64_t l_1289[8];
            int64_t *l_1295 = (void*)0;
            int64_t *l_1296 = &g_667;
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1276[i][j] = 0x185E1DDEL;
            }
            for (i = 0; i < 8; i++)
                l_1289[i] = 0x64BE13CC39261650LL;
            l_588[0] ^= (safe_rshift_func_int8_t_s_u(((safe_sub_func_int32_t_s_s((l_1276[1][0] = (safe_div_func_int32_t_s_s((p_54 == (l_1283 = func_64(((safe_add_func_uint8_t_u_u((safe_div_func_int16_t_s_s(((((void*)0 == l_998) < (safe_mod_func_uint8_t_u_u((((l_1281 |= ((safe_sub_func_int8_t_s_s((l_1280 |= ((*g_497) = (((safe_rshift_func_uint8_t_u_u(((safe_sub_func_float_f_f(((((*g_344) , (safe_sub_func_float_f_f(((((p_55 == ((+(safe_rshift_func_int16_t_s_s((safe_mod_func_int32_t_s_s((((((*l_1277) = ((safe_mul_func_float_f_f(((safe_add_func_float_f_f((safe_mul_func_float_f_f((g_240 , ((*l_1275) = ((safe_sub_func_float_f_f(p_55, (safe_add_func_float_f_f(p_55, (safe_sub_func_float_f_f((((void*)0 != l_1274[0]) <= p_55), p_55)))))) >= (-0x1.6p+1)))), (-0x7.Cp-1))), l_1276[0][0])) > (*l_1162)), (*g_344))) < p_55)) , l_1276[0][0]) == 0L) | (*l_1162)), p_55)), (*l_1162)))) < (**g_498))) & (*g_138)) , 0x0.Fp+1) >= p_55), p_55))) <= 0x3.B82020p+44) < p_55), l_1276[1][0])) , l_1276[3][0]), 5)) , (*l_1162)) != l_1278))), l_1276[0][0])) <= p_55)) , (*p_54)) > 0xFDB7CF7A0D834CC7LL), p_55))) > (*p_54)), (*g_138))), l_1276[0][0])) , &g_131[0][7][0]), l_1282, l_1276[2][0]))), 0xC01C185AL))), l_1284)) || (*g_497)), p_55));
            (*g_232) = 0xD75AA4FEL;
            (*g_1297) = func_60((safe_mul_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u(l_1289[1], (p_55 & (*l_1162)))), p_55)), (*l_1162), ((*l_1296) = ((((((*l_1162) , (safe_add_func_int32_t_s_s((l_588[0] &= (safe_mul_func_int16_t_s_s(((**g_1111) &= (-2L)), ((((void*)0 == &g_131[3][6][2]) , p_55) | p_55)))), l_1294))) & 0x84B503CDL) > g_299) , p_55) | (*g_499))));
        }
        else
        { /* block id: 489 */
lbl_1300:
            for (g_135 = 0; (g_135 <= 22); g_135 = safe_add_func_int64_t_s_s(g_135, 9))
            { /* block id: 492 */
                return (*g_385);
            }
            (*g_1303) = ((void*)0 != l_1301);
        }
        for (g_208 = 29; (g_208 > 13); --g_208)
        { /* block id: 500 */
            int32_t *l_1306 = (void*)0;
            (*l_1162) = (&g_1219[7] != (l_1306 = l_998));
        }
    }
    return p_55;
}


/* ------------------------------------------ */
/* 
 * reads : g_562 g_74
 * writes:
 */
static int32_t * const  func_60(int16_t  p_61, const uint16_t  p_62, int64_t  p_63)
{ /* block id: 232 */
    uint8_t l_589 = 0x01L;
    int32_t *l_590 = &g_104;
    uint32_t l_607 = 1UL;
    float l_668 = 0x1.Ep+1;
    union U0 l_755 = {1UL};
    int32_t l_831 = 1L;
    int32_t ***l_836 = &g_562;
    int32_t l_871 = 0L;
    int32_t l_872[8] = {0x2C6952C1L,0x2C6952C1L,0xD6B6D437L,0x2C6952C1L,0x2C6952C1L,0xD6B6D437L,0x2C6952C1L,0x2C6952C1L};
    int32_t * const l_887 = &l_871;
    uint16_t ***l_893 = &g_498;
    uint16_t ****l_892 = &l_893;
    uint16_t *****l_891 = &l_892;
    uint8_t *l_902 = &g_486[2][0];
    int16_t *l_907[9][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
    uint8_t l_914 = 255UL;
    int i, j;
    return (**l_836);
}


/* ------------------------------------------ */
/* 
 * reads : g_102
 * writes: g_102
 */
static uint64_t * func_64(uint64_t * p_65, uint64_t * p_66, int32_t  p_67)
{ /* block id: 150 */
    int32_t l_457[8][1];
    uint8_t *l_480 = &g_315;
    int8_t l_481[1];
    uint16_t * const l_484 = (void*)0;
    uint16_t * const *l_483[8] = {&l_484,&l_484,&l_484,&l_484,&l_484,&l_484,&l_484,&l_484};
    int32_t l_489 = 9L;
    int32_t l_490[8] = {0x4DE93A34L,0x4DE93A34L,0x4DE93A34L,0x4DE93A34L,0x4DE93A34L,0x4DE93A34L,0x4DE93A34L,0x4DE93A34L};
    uint32_t *l_510 = &g_135;
    int64_t l_516 = 0x77C5DAA343B23D90LL;
    uint64_t l_530 = 0x58DDC39B893CF23ELL;
    uint32_t l_531 = 2UL;
    int32_t l_545 = 1L;
    uint32_t l_556 = 0x7AF3AAD1L;
    int i, j;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
            l_457[i][j] = (-3L);
    }
    for (i = 0; i < 1; i++)
        l_481[i] = 0xF1L;
    for (g_102 = 0; (g_102 > 8); g_102 = safe_add_func_int32_t_s_s(g_102, 2))
    { /* block id: 153 */
        int32_t l_456 = 8L;
        int64_t *l_476 = &g_477[3];
        union U0 l_482 = {0x64C16A2071DB97D2LL};
        uint8_t *l_485 = &g_486[2][0];
        int16_t l_487 = 0xE5C6L;
        int32_t *l_488[9][8][3];
        uint8_t l_534 = 255UL;
        int i, j, k;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 8; j++)
            {
                for (k = 0; k < 3; k++)
                    l_488[i][j][k] = &g_83;
            }
        }
    }
    return p_65;
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_56 g_57 g_299 g_102 g_208 g_445 g_240.f1 g_408 g_104 g_83 g_240.f3 g_447 g_138 g_139 g_135 g_125 g_131
 * writes: g_37 g_441 g_299
 */
static uint64_t * func_68(int32_t * p_69, int32_t * p_70, union U0  p_71, int64_t  p_72, uint64_t * const  p_73)
{ /* block id: 7 */
    int32_t l_86[8] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
    int32_t **l_87 = (void*)0;
    uint32_t * const l_140 = &g_135;
    uint64_t *l_166 = &g_131[2][1][1];
    const int16_t *l_178[5];
    int32_t l_210[2];
    float l_236 = 0xE.E4FC8Bp-98;
    uint32_t l_253 = 0x02F6F451L;
    int8_t *l_268 = &g_240.f3;
    int8_t l_269 = (-5L);
    int32_t l_300 = 0xBE684CF5L;
    uint32_t l_410 = 0x75CEDC32L;
    int8_t l_444[2];
    int32_t *l_449 = &l_86[4];
    int i;
    for (i = 0; i < 5; i++)
        l_178[i] = &g_129;
    for (i = 0; i < 2; i++)
        l_210[i] = 0x0431255FL;
    for (i = 0; i < 2; i++)
        l_444[i] = (-3L);
lbl_418:
    for (g_37 = 7; (g_37 >= 0); g_37 -= 1)
    { /* block id: 10 */
        int64_t l_92 = 4L;
        int32_t l_98 = (-1L);
        const uint16_t *l_136 = &g_125;
        union U0 l_204[3] = {{7UL},{7UL},{7UL}};
        int32_t l_245[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        uint32_t *l_263[8][10] = {{&l_253,&l_204[1].f1,&l_253,&l_253,&l_204[1].f1,&l_253,&g_240.f1,&l_253,&l_204[1].f1,&l_253},{&g_240.f1,&l_204[1].f1,&g_240.f1,&l_253,&g_240.f1,&g_240.f1,&l_253,&g_240.f1,&l_204[1].f1,&g_240.f1},{&g_240.f1,&l_253,&l_204[1].f1,&l_204[1].f1,&l_204[1].f1,&l_253,&g_240.f1,&g_240.f1,&l_253,&l_204[1].f1},{&l_253,&g_240.f1,&g_240.f1,&l_253,&l_204[1].f1,&l_204[1].f1,&l_204[1].f1,&l_253,&g_240.f1,&g_240.f1},{&l_204[1].f1,&g_240.f1,&l_253,&g_240.f1,&g_240.f1,&l_253,&g_240.f1,&l_204[1].f1,&g_240.f1,&l_253},{&l_204[1].f1,&l_253,&g_240.f1,&l_253,&l_204[1].f1,&l_253,&l_253,&l_204[1].f1,&l_253,&g_240.f1},{&l_204[1].f1,&l_204[1].f1,&g_240.f1,&l_204[1].f1,&l_253,&l_204[1].f1,&g_240.f1,&l_204[1].f1,&l_204[1].f1,&g_240.f1},{&l_253,&l_204[1].f1,&l_253,&l_253,&l_204[1].f1,&l_253,&g_240.f1,&l_253,&l_204[1].f1,&l_253}};
        uint32_t l_283 = 9UL;
        int8_t l_337 = 0x8AL;
        uint64_t *l_377 = &g_299;
        const uint8_t *l_380 = &g_315;
        uint64_t l_417 = 9UL;
        int i, j;
        for (p_72 = 7; (p_72 >= 1); p_72 -= 1)
        { /* block id: 13 */
            int32_t **l_89 = &g_74;
            uint16_t *l_99 = &g_100[0];
            uint32_t *l_101 = &g_102;
            int32_t *l_103 = &g_104;
            float *l_113 = (void*)0;
            float *l_114 = (void*)0;
            float *l_115 = &g_116;
            uint16_t *l_123 = (void*)0;
            uint16_t *l_124[9];
            int16_t *l_128[3][6];
            uint64_t *l_130 = &g_131[3][3][1];
            uint32_t l_209[8] = {0xEAFE74C3L,0x724B175CL,0xEAFE74C3L,0x724B175CL,0xEAFE74C3L,0x724B175CL,0xEAFE74C3L,0x724B175CL};
            int32_t l_246 = 0x03A9D8E9L;
            int32_t l_247 = (-10L);
            int32_t l_248 = 7L;
            int32_t l_249 = 0x1B8DFF5CL;
            int32_t l_250 = 0xA1B9127FL;
            int32_t l_251[3];
            int32_t l_252[3];
            int i, j;
            for (i = 0; i < 9; i++)
                l_124[i] = &g_125;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 6; j++)
                    l_128[i][j] = &g_129;
            }
            for (i = 0; i < 3; i++)
                l_251[i] = (-1L);
            for (i = 0; i < 3; i++)
                l_252[i] = 0L;
            l_89 = l_87;
            if (g_37)
                goto lbl_418;
        }
        if (l_86[g_37])
            continue;
    }
    (*l_449) = (p_71.f3 & (safe_sub_func_int32_t_s_s(((((safe_lshift_func_int16_t_s_s(((safe_add_func_uint32_t_u_u((0x96L | (p_71.f1 || ((safe_mod_func_int16_t_s_s((safe_mod_func_int8_t_s_s(((((((((p_71.f3 >= ((safe_rshift_func_uint16_t_u_s((safe_div_func_int8_t_s_s(((((p_71.f2 & (((safe_mul_func_uint16_t_u_u(((safe_div_func_int8_t_s_s(((safe_div_func_int64_t_s_s((g_441 = 0xFB4C694B760A30BFLL), (g_299 ^= (5UL <= (safe_lshift_func_uint8_t_u_s(p_72, ((*g_56) != p_71.f3))))))) || g_102), l_444[1])) != g_208), 0UL)) , g_445) != &g_446)) ^ 0x63A2B6B0734E1486LL) > l_210[1]) < 1UL), g_240.f1)), 13)) >= 0xE2L)) || (*g_408)) < g_83) , p_71.f2) , g_240.f3) , (*g_56)) && 0x554C0BE30A5FB24FLL) ^ g_447), p_71.f1)), (*g_138))) != g_104))), g_135)) , l_410), p_71.f2)) , &p_69) != l_87) <= g_125), 0x93A10A6DL)));
    return &g_131[3][3][1];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_5[i][j], "g_5[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_37, "g_37", print_hash_value);
    transparent_crc(g_57, "g_57", print_hash_value);
    transparent_crc(g_83, "g_83", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_100[i], "g_100[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_102, "g_102", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc_bytes (&g_116, sizeof(g_116), "g_116", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_131[i][j][k], "g_131[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_134, sizeof(g_134), "g_134", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_139, "g_139", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_157[i], "g_157[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_208, "g_208", print_hash_value);
    transparent_crc(g_218, "g_218", print_hash_value);
    transparent_crc(g_240.f0, "g_240.f0", print_hash_value);
    transparent_crc(g_240.f1, "g_240.f1", print_hash_value);
    transparent_crc(g_240.f2, "g_240.f2", print_hash_value);
    transparent_crc(g_240.f3, "g_240.f3", print_hash_value);
    transparent_crc(g_299, "g_299", print_hash_value);
    transparent_crc(g_315, "g_315", print_hash_value);
    transparent_crc(g_367, "g_367", print_hash_value);
    transparent_crc(g_441, "g_441", print_hash_value);
    transparent_crc(g_447, "g_447", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_477[i], "g_477[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_486[i][j], "g_486[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_528, "g_528", print_hash_value);
    transparent_crc(g_553, "g_553", print_hash_value);
    transparent_crc(g_575, "g_575", print_hash_value);
    transparent_crc(g_595, "g_595", print_hash_value);
    transparent_crc(g_667, "g_667", print_hash_value);
    transparent_crc(g_731, "g_731", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_884[i], "g_884[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1047, "g_1047", print_hash_value);
    transparent_crc(g_1119, "g_1119", print_hash_value);
    transparent_crc(g_1217, "g_1217", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1219[i], "g_1219[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1347, "g_1347", print_hash_value);
    transparent_crc(g_1468, "g_1468", print_hash_value);
    transparent_crc(g_1510, "g_1510", print_hash_value);
    transparent_crc(g_1538, "g_1538", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc_bytes(&g_1704[i][j][k], sizeof(g_1704[i][j][k]), "g_1704[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1889, "g_1889", print_hash_value);
    transparent_crc(g_2043, "g_2043", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2052[i][j][k], "g_2052[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2189, "g_2189", print_hash_value);
    transparent_crc(g_2217, "g_2217", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2392[i], "g_2392[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2426, "g_2426", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2643[i][j][k], "g_2643[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_2658[i][j][k], "g_2658[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2664, "g_2664", print_hash_value);
    transparent_crc(g_2691.f0, "g_2691.f0", print_hash_value);
    transparent_crc(g_2691.f1, "g_2691.f1", print_hash_value);
    transparent_crc(g_2691.f2, "g_2691.f2", print_hash_value);
    transparent_crc(g_2691.f3, "g_2691.f3", print_hash_value);
    transparent_crc(g_2794, "g_2794", print_hash_value);
    transparent_crc(g_2809, "g_2809", print_hash_value);
    transparent_crc(g_2871, "g_2871", print_hash_value);
    transparent_crc(g_2934, "g_2934", print_hash_value);
    transparent_crc_bytes (&g_2945, sizeof(g_2945), "g_2945", print_hash_value);
    transparent_crc(g_3046, "g_3046", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_3101[i], "g_3101[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc_bytes(&g_3251[i][j][k], sizeof(g_3251[i][j][k]), "g_3251[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3411, "g_3411", print_hash_value);
    transparent_crc(g_3535, "g_3535", print_hash_value);
    transparent_crc(g_3678, "g_3678", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 922
XXX total union variables: 23

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 58
breakdown:
   depth: 1, occurrence: 156
   depth: 2, occurrence: 35
   depth: 3, occurrence: 3
   depth: 4, occurrence: 4
   depth: 5, occurrence: 1
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 4
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 41, occurrence: 1
   depth: 58, occurrence: 1

XXX total number of pointers: 624

XXX times a variable address is taken: 1383
XXX times a pointer is dereferenced on RHS: 700
breakdown:
   depth: 1, occurrence: 476
   depth: 2, occurrence: 154
   depth: 3, occurrence: 46
   depth: 4, occurrence: 21
   depth: 5, occurrence: 3
XXX times a pointer is dereferenced on LHS: 500
breakdown:
   depth: 1, occurrence: 409
   depth: 2, occurrence: 63
   depth: 3, occurrence: 24
   depth: 4, occurrence: 4
XXX times a pointer is compared with null: 73
XXX times a pointer is compared with address of another variable: 20
XXX times a pointer is compared with another pointer: 25
XXX times a pointer is qualified to be dereferenced: 13218

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1850
   level: 2, occurrence: 659
   level: 3, occurrence: 317
   level: 4, occurrence: 138
   level: 5, occurrence: 72
XXX number of pointers point to pointers: 275
XXX number of pointers point to scalars: 341
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.5
XXX average alias set size: 1.37

XXX times a non-volatile is read: 3259
XXX times a non-volatile is write: 1480
XXX times a volatile is read: 256
XXX    times read thru a pointer: 136
XXX times a volatile is write: 67
XXX    times written thru a pointer: 24
XXX times a volatile is available for access: 2.97e+03
XXX percentage of non-volatile access: 93.6

XXX forward jumps: 2
XXX backward jumps: 10

XXX stmts: 153
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 24
   depth: 1, occurrence: 19
   depth: 2, occurrence: 31
   depth: 3, occurrence: 28
   depth: 4, occurrence: 20
   depth: 5, occurrence: 31

XXX percentage a fresh-made variable is used: 15.7
XXX percentage an existing variable is used: 84.3
********************* end of statistics **********************/


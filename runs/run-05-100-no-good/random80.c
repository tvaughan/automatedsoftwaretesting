/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2642777689
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   volatile int32_t  f0;
   int64_t  f1;
   const int32_t  f2;
   volatile uint16_t  f3;
};
#pragma pack(pop)

struct S1 {
   signed f0 : 25;
   uint64_t  f1;
   uint8_t  f2;
};

#pragma pack(push)
#pragma pack(1)
struct S2 {
   volatile signed f0 : 29;
   unsigned f1 : 4;
   signed f2 : 4;
   unsigned f3 : 15;
   unsigned f4 : 28;
   unsigned f5 : 22;
   signed f6 : 9;
   unsigned f7 : 10;
};
#pragma pack(pop)

union U3 {
   volatile uint16_t  f0;
   volatile int8_t * f1;
   volatile uint32_t  f2;
};

union U4 {
   float  f0;
   const int8_t * const  volatile  f1;
};

/* --- GLOBAL VARIABLES --- */
static int8_t g_5 = 1L;
static int32_t g_34 = 0x276AC116L;
static volatile int32_t g_35 = (-1L);/* VOLATILE GLOBAL g_35 */
static volatile uint32_t g_36[10] = {5UL,0x4FEF5379L,5UL,5UL,0x4FEF5379L,5UL,5UL,0x4FEF5379L,5UL,5UL};
static uint64_t g_53 = 0x04DBC0BA889B05F6LL;
static uint8_t g_55 = 255UL;
static float g_76 = (-0x9.0p+1);
static uint32_t g_84[1][5] = {{0xD5F09706L,0xD5F09706L,0xD5F09706L,0xD5F09706L,0xD5F09706L}};
static const int8_t g_123 = 0xB2L;
static int16_t g_135 = 0x319BL;
static uint32_t g_136 = 3UL;
static uint8_t g_139 = 0UL;
static struct S1 g_140 = {-4289,18446744073709551607UL,0xC9L};
static int8_t *g_147 = (void*)0;
static int8_t **g_146 = &g_147;
static int16_t g_151 = (-1L);
static int32_t g_153 = 0x170005B7L;
static int32_t g_156[7] = {0x1D063706L,0x1D063706L,0x1D063706L,0x1D063706L,0x1D063706L,0x1D063706L,0x1D063706L};
static int8_t g_157[1] = {1L};
static int64_t g_158 = 0x73F93019E490029ELL;
static uint8_t g_159[9][7][4] = {{{250UL,0UL,255UL,0UL},{0UL,0UL,0x83L,247UL},{0xEDL,0UL,246UL,0UL},{0xC4L,0UL,0x42L,0UL},{0x1FL,0x0DL,0UL,0x53L},{0x9AL,0xC8L,246UL,0UL},{0x1BL,251UL,0xD7L,247UL}},{{0xA9L,0x53L,0xC4L,0xAAL},{0UL,0xD5L,250UL,1UL},{0UL,0x0DL,0xC4L,255UL},{0xA9L,1UL,0xD7L,0UL},{0x1BL,0x2BL,1UL,0xD5L},{0x42L,0x0DL,0x6FL,0x2BL},{0xD7L,4UL,0x14L,0xAAL}},{{0UL,247UL,1UL,0x53L},{255UL,251UL,0xD1L,0x53L},{0xA9L,247UL,0x0CL,0xAAL},{0x1FL,4UL,250UL,0x2BL},{0x83L,0x0DL,0xF4L,0xD5L},{0xA9L,0x2BL,0xA9L,0UL},{255UL,1UL,1UL,255UL}},{{0xEDL,0x0DL,246UL,1UL},{0xD7L,0xD5L,246UL,0xAAL},{0xEDL,0x53L,1UL,247UL},{255UL,251UL,0xA9L,0UL},{0xA9L,0UL,0xF4L,0xAAL},{0x83L,255UL,250UL,0UL},{0x1FL,0x0DL,0x0CL,4UL}},{{0xA9L,0UL,0xD1L,0UL},{255UL,0UL,1UL,4UL},{0UL,0x0DL,0x14L,0UL},{0xD7L,255UL,0x6FL,0xAAL},{0x42L,0UL,1UL,0UL},{0x1BL,251UL,0xD7L,247UL},{0xA9L,0x53L,0xC4L,0xAAL}},{{0UL,0xD5L,250UL,1UL},{0UL,0x0DL,0xC4L,255UL},{0xA9L,1UL,0xD7L,0UL},{0x1BL,0x2BL,1UL,0xD5L},{0x42L,0x0DL,0x6FL,0x2BL},{0xD7L,4UL,0x14L,0xAAL},{0UL,247UL,1UL,0x53L}},{{255UL,251UL,0xD1L,0x53L},{0xA9L,247UL,0x0CL,0xAAL},{0x1FL,4UL,250UL,0x2BL},{0x83L,0x0DL,0xF4L,0xD5L},{0xA9L,0x2BL,0xA9L,0UL},{255UL,1UL,1UL,255UL},{0xEDL,0x0DL,246UL,1UL}},{{0xD7L,0xD5L,246UL,0xAAL},{0xEDL,0x53L,1UL,247UL},{255UL,251UL,0xA9L,0UL},{0xA9L,0UL,0xF4L,0xAAL},{0x83L,255UL,0x1FL,0x0DL},{0xD7L,0x0DL,0xC4L,255UL},{0x22L,0x0DL,0xEDL,0UL}},{{0xC4L,0x0DL,0UL,255UL},{0x6FL,0x0DL,250UL,0x0DL},{0xD7L,247UL,0x8DL,0xB6L},{0x14L,252UL,0UL,252UL},{0xF4L,0UL,0xD7L,0UL},{0x22L,0xAAL,0xD1L,0xB6L},{0xA9L,255UL,0x1FL,251UL}}};
static int8_t g_185 = 0x99L;
static int64_t g_186 = 1L;
static int32_t g_187[2][4] = {{0L,0L,0L,0L},{0L,0L,0L,0L}};
static int32_t g_191 = 8L;
static int64_t g_193 = 0L;
static uint64_t *g_206[9][6] = {{&g_53,&g_140.f1,(void*)0,&g_140.f1,(void*)0,&g_140.f1},{(void*)0,&g_140.f1,&g_140.f1,&g_140.f1,&g_53,&g_140.f1},{&g_53,(void*)0,(void*)0,(void*)0,(void*)0,&g_53},{&g_53,(void*)0,(void*)0,&g_140.f1,&g_53,(void*)0},{(void*)0,&g_140.f1,&g_53,&g_140.f1,(void*)0,&g_53},{(void*)0,&g_140.f1,&g_140.f1,&g_140.f1,&g_140.f1,&g_140.f1},{&g_53,&g_53,&g_140.f1,(void*)0,&g_140.f1,(void*)0},{&g_53,&g_53,&g_140.f1,&g_140.f1,&g_140.f1,&g_140.f1},{(void*)0,&g_53,&g_140.f1,&g_140.f1,&g_140.f1,&g_140.f1}};
static uint8_t *g_210 = (void*)0;
static int8_t g_221[3] = {0L,0L,0L};
static int8_t g_222 = 0x54L;
static uint64_t g_223 = 18446744073709551613UL;
static int32_t *g_226 = (void*)0;
static int32_t g_245 = 0xCCA9B615L;
static int32_t g_246 = 0L;
static uint8_t g_248 = 0UL;
static uint16_t g_310 = 1UL;
static uint16_t g_336 = 65534UL;
static uint32_t g_345 = 0x62294A89L;
static uint64_t g_353 = 8UL;
static const float g_428 = (-0x1.2p+1);
static const float g_430 = 0xE.580794p-41;
static const float *g_429 = &g_430;
static uint32_t g_447 = 18446744073709551615UL;
static uint8_t g_458 = 0xFAL;
static float *g_540 = &g_76;
static uint8_t g_549[3] = {0x9CL,0x9CL,0x9CL};
static uint8_t g_658[4][9][1] = {{{0x18L},{6UL},{0x09L},{6UL},{0x18L},{0UL},{0x09L},{0UL},{0x18L}},{{6UL},{0x09L},{6UL},{0x18L},{0UL},{0x09L},{0UL},{0x18L},{6UL}},{{0x09L},{6UL},{0x18L},{0UL},{0x09L},{0UL},{0x18L},{6UL},{0x09L}},{{6UL},{0x18L},{0UL},{0x09L},{0UL},{0x18L},{6UL},{0x09L},{6UL}}};
static volatile int16_t g_663[1][3][1] = {{{4L},{4L},{4L}}};
static volatile int16_t * volatile g_662 = &g_663[0][0][0];/* VOLATILE GLOBAL g_662 */
static volatile int16_t g_665[9] = {(-8L),(-8L),(-8L),(-8L),(-8L),(-8L),(-8L),(-8L),(-8L)};
static volatile int16_t g_666 = (-6L);/* VOLATILE GLOBAL g_666 */
static volatile int16_t g_667 = 0xF305L;/* VOLATILE GLOBAL g_667 */
static volatile int16_t g_668 = (-1L);/* VOLATILE GLOBAL g_668 */
static volatile int16_t *g_664[3][3] = {{&g_668,&g_666,&g_668},{&g_665[0],&g_665[0],&g_665[0]},{&g_668,&g_666,&g_668}};
static volatile int16_t * volatile *g_661[10][9][2] = {{{&g_664[1][1],(void*)0},{(void*)0,(void*)0},{&g_664[2][0],&g_664[2][1]},{(void*)0,&g_664[2][1]},{&g_664[2][0],&g_664[2][1]},{&g_664[2][1],&g_664[0][0]},{&g_664[0][0],&g_664[1][2]},{&g_664[2][2],(void*)0},{(void*)0,&g_664[2][1]}},{{(void*)0,&g_664[0][0]},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[1][1],(void*)0},{&g_664[2][1],&g_664[0][2]},{&g_664[2][1],&g_664[0][0]},{(void*)0,&g_664[2][1]},{&g_664[2][1],&g_664[2][1]},{(void*)0,&g_664[0][0]}},{{&g_664[2][1],&g_664[0][2]},{&g_664[0][2],(void*)0},{&g_664[0][0],&g_664[0][0]},{&g_664[1][0],&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[2][1],(void*)0},{&g_664[1][2],(void*)0},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],&g_664[2][1]}},{{&g_664[2][1],(void*)0},{&g_664[2][1],&g_664[0][2]},{(void*)0,&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[0][2],&g_664[2][2]},{&g_664[2][0],&g_664[1][2]},{&g_664[2][1],&g_664[0][1]},{&g_664[2][2],&g_664[2][0]},{&g_664[0][0],(void*)0}},{{&g_664[2][1],&g_664[2][1]},{&g_664[0][1],(void*)0},{&g_664[1][1],&g_664[1][2]},{(void*)0,&g_664[2][1]},{(void*)0,&g_664[1][0]},{&g_664[1][1],&g_664[1][1]},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],&g_664[2][1]},{&g_664[1][0],(void*)0}},{{(void*)0,&g_664[1][0]},{(void*)0,&g_664[2][2]},{(void*)0,&g_664[1][0]},{(void*)0,(void*)0},{&g_664[1][0],&g_664[2][1]},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],&g_664[1][1]},{&g_664[1][1],&g_664[1][0]},{(void*)0,&g_664[2][1]}},{{(void*)0,&g_664[1][2]},{&g_664[1][1],(void*)0},{&g_664[0][1],&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[0][0],&g_664[2][0]},{&g_664[2][2],&g_664[0][1]},{&g_664[2][1],&g_664[1][2]},{&g_664[2][0],&g_664[2][2]},{&g_664[0][2],(void*)0}},{{&g_664[2][1],&g_664[2][1]},{(void*)0,&g_664[0][2]},{&g_664[2][1],(void*)0},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[1][2],(void*)0},{&g_664[2][1],(void*)0},{&g_664[2][1],&g_664[2][1]}},{{&g_664[1][0],&g_664[0][0]},{&g_664[0][0],(void*)0},{&g_664[0][2],&g_664[0][2]},{&g_664[2][1],&g_664[0][0]},{(void*)0,&g_664[2][1]},{&g_664[2][1],&g_664[2][1]},{(void*)0,&g_664[0][0]},{&g_664[2][1],&g_664[0][2]},{&g_664[0][2],(void*)0}},{{&g_664[0][0],&g_664[0][0]},{&g_664[1][0],&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[2][1],(void*)0},{&g_664[1][2],(void*)0},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],&g_664[2][1]},{&g_664[2][1],(void*)0},{&g_664[2][1],&g_664[0][2]}}};
static volatile uint16_t g_682 = 7UL;/* VOLATILE GLOBAL g_682 */
static volatile uint16_t *g_681 = &g_682;
static union U4 g_700[8][3][4] = {{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}},{{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}},{{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1},{0x0.Ep-1}}}};
static const uint8_t g_710[5][10][5] = {{{0xBFL,1UL,0xDBL,0UL,4UL},{0xEDL,1UL,0x1AL,0x7AL,255UL},{2UL,0x08L,0x46L,0xDBL,0x1AL},{0x50L,0x26L,0x1DL,0UL,0x49L},{0xDBL,0UL,255UL,0x49L,0x26L},{0xDDL,1UL,0UL,255UL,0x9DL},{0xC4L,248UL,0x4CL,0UL,0xE4L},{0xA0L,0x4FL,246UL,0x27L,0x1DL},{0x7AL,0x6FL,0xCBL,255UL,0UL},{1UL,0x50L,6UL,6UL,0x50L}},{{6UL,255UL,0UL,0UL,255UL},{0UL,0x4CL,0x32L,0UL,0xBFL},{255UL,0UL,0x78L,0xEDL,1UL},{0UL,1UL,247UL,0x06L,0xE5L},{6UL,0xCBL,0xF0L,0xC0L,0UL},{1UL,0x8CL,6UL,1UL,0UL},{0x7AL,8UL,255UL,248UL,6UL},{0xA0L,255UL,0xDDL,0UL,248UL},{0xC4L,0xEDL,0x8CL,0x1AL,0xDBL},{0xDDL,255UL,1UL,0UL,8UL}},{{0xDBL,0UL,255UL,0xFBL,1UL},{0x50L,0UL,0x51L,255UL,0xCDL},{2UL,0x7EL,0x26L,0x32L,255UL},{0xEDL,0x51L,0x3EL,0x6FL,255UL},{0xBFL,0UL,1UL,1UL,0xCDL},{0UL,1UL,0UL,0xA0L,1UL},{0UL,0xB7L,255UL,248UL,255UL},{0xD2L,0xDBL,0x30L,8UL,0x51L},{0x88L,0xC0L,0x1DL,0x08L,0x55L},{0xB7L,0x88L,255UL,255UL,0xE4L}},{{0x7AL,0x4CL,0x55L,0xBDL,255UL},{0x49L,4UL,9UL,8UL,246UL},{0x9DL,0x26L,0x26L,0x9DL,0x78L},{0x78L,1UL,0xE5L,0xEDL,0xFDL},{0x51L,0xDBL,0xCBL,0x32L,0x30L},{0x27L,2UL,0UL,0xEDL,0UL},{248UL,1UL,0xC4L,0x9DL,0x46L},{0xAFL,0x7AL,1UL,8UL,0xFBL},{1UL,0UL,246UL,0xBDL,1UL},{0x3EL,0x49L,0x71L,255UL,0xEDL}},{{8UL,1UL,0xEDL,0x08L,253UL},{0x50L,251UL,1UL,8UL,0x06L},{0xFBL,247UL,0UL,248UL,0UL},{0x32L,1UL,0x6FL,0UL,0x08L},{1UL,0x08L,0x88L,0x0CL,0x49L},{255UL,255UL,0x6AL,255UL,8UL},{1UL,249UL,0x6AL,0x4CL,0xD2L},{0x7BL,255UL,0x88L,0xBFL,0xCBL},{0UL,0xC4L,0x6FL,0xE4L,0x26L},{1UL,0x7EL,0UL,0x1AL,247UL}}};
static const union U4 g_742 = {0x9.1p+1};/* VOLATILE GLOBAL g_742 */
static const union U4 *g_741 = &g_742;
static uint32_t g_743 = 0UL;
static float g_786 = 0x3.205230p+13;
static int16_t g_844 = 0x068CL;
static uint32_t g_845 = 0x414ED856L;
static int64_t g_868 = 1L;
static uint32_t g_871 = 0xE4089221L;
static uint16_t * volatile g_895 = &g_336;/* VOLATILE GLOBAL g_895 */
static uint16_t * volatile * volatile g_894 = &g_895;/* VOLATILE GLOBAL g_894 */
static uint16_t * volatile * volatile *g_893 = &g_894;
static int8_t ***g_927 = &g_146;
static uint32_t g_929 = 0x346D6969L;
static int64_t g_935[4][1] = {{0xFACB1955257F1481LL},{0x7BEB58294C6C565FLL},{0xFACB1955257F1481LL},{0x7BEB58294C6C565FLL}};
static int16_t g_936 = (-6L);
static int64_t g_938 = 0x63A185A2F596CEAELL;
static int8_t g_939[10] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
static int32_t g_940 = 0x80563863L;
static uint8_t g_942 = 8UL;
static uint32_t g_946 = 2UL;
static union U4 g_951 = {0xD.34C753p-41};/* VOLATILE GLOBAL g_951 */
static float g_1057 = 0x1.7p-1;
static uint64_t g_1107 = 0x1179EEF28594BD9BLL;
static union U4 g_1216 = {0x6.AB66EBp+16};/* VOLATILE GLOBAL g_1216 */
static union U4 *g_1215 = &g_1216;
static union U4 **g_1214[1][1] = {{&g_1215}};
static uint32_t g_1241 = 0x2E5BDBC1L;
static float **g_1249 = (void*)0;
static float **g_1250 = (void*)0;
static volatile uint32_t *g_1264 = (void*)0;
static volatile uint32_t * volatile *g_1263[5] = {&g_1264,&g_1264,&g_1264,&g_1264,&g_1264};
static uint8_t g_1305 = 2UL;
static int16_t g_1336 = 0x9509L;
static uint64_t g_1337 = 0xD7465AEABA247AEDLL;
static int32_t g_1348 = 0xFA57051FL;
static uint16_t g_1349 = 0UL;
static union U4 g_1357 = {0x3.99C309p-56};/* VOLATILE GLOBAL g_1357 */
static union U4 g_1358 = {-0x3.Dp-1};/* VOLATILE GLOBAL g_1358 */
static union U4 g_1359[3] = {{0x0.E1026Bp+44},{0x0.E1026Bp+44},{0x0.E1026Bp+44}};
static union U4 g_1360[6] = {{0x3.2p+1},{0x3.2p+1},{0x3.2p+1},{0x3.2p+1},{0x3.2p+1},{0x3.2p+1}};
static union U4 g_1361[10] = {{0x6.6723BAp-39},{0x6.6723BAp-39},{0x7.7A5655p+8},{0x6.6723BAp-39},{0x6.6723BAp-39},{0x7.7A5655p+8},{0x6.6723BAp-39},{0x6.6723BAp-39},{0x7.7A5655p+8},{0x6.6723BAp-39}};
static union U4 g_1363 = {0x7.69D085p+89};/* VOLATILE GLOBAL g_1363 */
static const volatile uint32_t *g_1366 = (void*)0;
static const volatile uint32_t **g_1365 = &g_1366;
static struct S1 g_1406[2] = {{570,18446744073709551610UL,255UL},{570,18446744073709551610UL,255UL}};
static int32_t g_1417 = 1L;
static uint32_t *g_1517 = &g_136;
static uint32_t *g_1518 = (void*)0;
static uint32_t g_1534 = 4294967291UL;
static union U3 g_1538[4] = {{0x54B2L},{0x54B2L},{0x54B2L},{0x54B2L}};
static int8_t g_1568 = (-1L);
static union U3 *g_1580 = (void*)0;
static struct S2 g_1598 = {-18472,0,0,152,1072,1705,1,7};/* VOLATILE GLOBAL g_1598 */
static struct S2 g_1601 = {12724,3,-3,21,14448,452,2,9};/* VOLATILE GLOBAL g_1601 */
static int16_t **g_1640 = (void*)0;
static int16_t ***g_1639 = &g_1640;
static volatile struct S2 g_1663 = {7695,0,-1,67,5859,39,-1,18};/* VOLATILE GLOBAL g_1663 */
static volatile struct S2 *g_1662 = &g_1663;
static volatile struct S2 **g_1661 = &g_1662;
static union U4 g_1671 = {0x2.4F5EE7p-55};/* VOLATILE GLOBAL g_1671 */
static union U4 g_1672 = {-0x1.Fp-1};/* VOLATILE GLOBAL g_1672 */
static volatile int32_t g_1758[5][10] = {{9L,0x70931367L,9L,9L,0x70931367L,9L,0x65162317L,9L,0x70931367L,9L},{0xC6A53CAAL,1L,0xC6A53CAAL,9L,0x65162317L,0x65162317L,9L,0xC6A53CAAL,1L,0xC6A53CAAL},{0xC6A53CAAL,9L,1L,0x70931367L,1L,9L,0xC6A53CAAL,0xC6A53CAAL,9L,1L},{0xC6A53CAAL,0x70931367L,0x70931367L,0xC6A53CAAL,1L,9L,1L,0xC6A53CAAL,0x70931367L,0x70931367L},{1L,0x70931367L,0x65162317L,1L,1L,0x65162317L,0x70931367L,1L,0x70931367L,0x65162317L}};
static volatile union U4 g_1777[7][8] = {{{0x7.2E1538p+74},{0x8.E686AFp+3},{-0x5.0p+1},{0x8.E686AFp+3},{0x7.2E1538p+74},{0x2.457A51p+88},{0x1.1AB044p+69},{-0x4.Cp+1}},{{0x7.2E1538p+74},{0x2.457A51p+88},{0x1.1AB044p+69},{-0x4.Cp+1},{0x1.1AB044p+69},{0x2.457A51p+88},{0x7.2E1538p+74},{0x8.E686AFp+3}},{{0x5.EF7E99p-82},{0x8.E686AFp+3},{0x1.1AB044p+69},{0x4.6p+1},{-0x1.Bp+1},{0x4.6p+1},{0x1.1AB044p+69},{0x8.E686AFp+3}},{{0x1.1AB044p+69},{0x0.C151EEp+82},{-0x5.0p+1},{-0x4.Cp+1},{-0x1.Bp+1},{0x8.E686AFp+3},{-0x1.Bp+1},{-0x4.Cp+1}},{{0x5.EF7E99p-82},{0x0.C151EEp+82},{0x5.EF7E99p-82},{0x8.E686AFp+3},{0x1.1AB044p+69},{0x4.6p+1},{-0x1.Bp+1},{0x4.6p+1}},{{0x7.2E1538p+74},{0x8.E686AFp+3},{-0x5.0p+1},{0x8.E686AFp+3},{0x7.2E1538p+74},{0x2.457A51p+88},{0x1.1AB044p+69},{-0x4.Cp+1}},{{0x7.2E1538p+74},{0x2.457A51p+88},{0x1.1AB044p+69},{-0x4.Cp+1},{0x1.1AB044p+69},{0x2.457A51p+88},{0x7.2E1538p+74},{0x8.E686AFp+3}}};
static volatile int16_t g_1795 = 0xC086L;/* VOLATILE GLOBAL g_1795 */
static struct S1 * volatile g_1814[10] = {&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140};
static struct S1 * volatile g_1815 = &g_140;/* VOLATILE GLOBAL g_1815 */
static volatile uint64_t g_1830 = 0x0D52084A7979558FLL;/* VOLATILE GLOBAL g_1830 */
static struct S1 * volatile g_1862 = &g_1406[0];/* VOLATILE GLOBAL g_1862 */
static volatile int8_t g_1870[1][8] = {{1L,1L,0x9CL,1L,1L,0x9CL,1L,1L}};
static struct S2 g_1886[5] = {{-6878,1,-1,174,6735,520,-1,4},{-6878,1,-1,174,6735,520,-1,4},{-6878,1,-1,174,6735,520,-1,4},{-6878,1,-1,174,6735,520,-1,4},{-6878,1,-1,174,6735,520,-1,4}};
static uint16_t g_1903 = 1UL;
static uint64_t g_1909 = 18446744073709551607UL;
static const union U4 *g_1918 = &g_1357;
static union U3 g_1921 = {65526UL};/* VOLATILE GLOBAL g_1921 */
static volatile int32_t g_1926 = 0x4918FBE9L;/* VOLATILE GLOBAL g_1926 */
static volatile float g_1927[2] = {0xC.E45C9Dp-87,0xC.E45C9Dp-87};
static int32_t ** volatile g_1932 = (void*)0;/* VOLATILE GLOBAL g_1932 */
static union U4 g_1964 = {-0x1.1p-1};/* VOLATILE GLOBAL g_1964 */
static float g_1979[6] = {0x1.Ep+1,0x1.Ep+1,0x1.Ep+1,0x1.Ep+1,0x1.Ep+1,0x1.Ep+1};
static volatile struct S0 g_2008[4][7][9] = {{{{0x860D4D3FL,0x536C41C9A28634C4LL,0x0B3D5550L,5UL},{0x8BECD810L,5L,0x87FAD47DL,1UL},{0L,-6L,0xA0F8B4DEL,0x0968L},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0x1217FC43L,0x0264869625EDC320LL,-3L,2UL},{0x1217FC43L,0x0264869625EDC320LL,-3L,2UL},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0L,-6L,0xA0F8B4DEL,0x0968L},{0x8BECD810L,5L,0x87FAD47DL,1UL}},{{0x24ACC74EL,1L,6L,0x4D36L},{0x6304567FL,0x18D4D31E9AD54990LL,0x34377454L,0xBDCEL},{0x4F8B8312L,-1L,0xFBE4A4C4L,0xC007L},{-1L,7L,0x37048C8BL,65533UL},{0x5B51814EL,0xFEB6CC7E3E5D8244LL,-4L,65531UL},{7L,0xF351D9A808F9EA61LL,-1L,0xE914L},{-3L,0xFFF765E96AB2956CLL,0x64BA08F4L,0UL},{1L,0x55137E62147BC3DALL,1L,0UL},{0x4FA30815L,0xE475B0C8CB501DD5LL,0xCFD874BEL,2UL}},{{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{0L,0xA7ADF4067475E74ELL,-1L,1UL},{0L,-6L,0xA0F8B4DEL,0x0968L},{-1L,0xCBB3B8B032745077LL,-1L,0UL},{0L,-6L,0xA0F8B4DEL,0x0968L},{0L,0xA7ADF4067475E74ELL,-1L,1UL},{0x8193956EL,0L,0x33EB5C9FL,0x6064L}},{{1L,0x6EF537B0C0A383BBLL,1L,65526UL},{0x6304567FL,0x18D4D31E9AD54990LL,0x34377454L,0xBDCEL},{1L,-7L,-3L,65534UL},{-7L,-1L,0x37DC8C3BL,0x2C4AL},{-1L,1L,-5L,1UL},{-5L,5L,0xE45B22D5L,0UL},{0x8CF803EEL,0x29AEB5C7A4FAE4F1LL,6L,0x69D2L},{0x7958726BL,0xB7BCB20436EB8CF3LL,0x5C730896L,0x9729L},{0x24ACC74EL,1L,6L,0x4D36L}},{{-1L,-1L,0x280CB640L,0xC688L},{0x8BECD810L,5L,0x87FAD47DL,1UL},{0x24059BF2L,1L,3L,0xAB9DL},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L}},{{1L,-7L,-3L,65534UL},{-5L,5L,0xE45B22D5L,0UL},{1L,-7L,-3L,65534UL},{7L,0xF351D9A808F9EA61LL,-1L,0xE914L},{0xD30F6D7DL,0xF38878847D12211CLL,-1L,0xAF6EL},{1L,0x9B8748D4908928D5LL,0x798D2CD9L,0x210EL},{-1L,4L,0L,1UL},{0xAE496449L,0xCC8A2655BC3BBF9CLL,0x49CF8A29L,0UL},{-1L,1L,-5L,1UL}},{{0xE76D7B4FL,0x82CAF0515AD2BD61LL,0xA7173B68L,0x883AL},{0x24059BF2L,1L,3L,0xAB9DL},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0L,3L,0xC43888FEL,0x4ED0L},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{0L,3L,0xC43888FEL,0x4ED0L},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L}}},{{{0x767165B1L,0x2B7D42D0035D7A22LL,5L,0x01A8L},{-3L,0xDCAD4E6792181CFBLL,0x26A48699L,1UL},{0x4F8B8312L,-1L,0xFBE4A4C4L,0xC007L},{7L,0xF351D9A808F9EA61LL,-1L,0xE914L},{1L,0xC4B3EC1A46F02B0CLL,0x108A0045L,0x94E6L},{0xF01D9A04L,0L,0xB9924FE6L,0xB41BL},{4L,0xC82FE8F7363D8060LL,-4L,0xFA39L},{0x09EF9A8AL,1L,-1L,0x3684L},{-1L,4L,0L,1UL}},{{0L,0xA7ADF4067475E74ELL,-1L,1UL},{4L,1L,0x0A51A35AL,0x4AB0L},{0L,-6L,0xA0F8B4DEL,0x0968L},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{-1L,0xCBB3B8B032745077LL,-1L,0UL},{-1L,0xCBB3B8B032745077LL,-1L,0UL},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL}},{{1L,0xC4B3EC1A46F02B0CLL,0x108A0045L,0x94E6L},{-3L,0xDCAD4E6792181CFBLL,0x26A48699L,1UL},{1L,-7L,-3L,65534UL},{-3L,0xDCAD4E6792181CFBLL,0x26A48699L,1UL},{0x4FA30815L,0xE475B0C8CB501DD5LL,0xCFD874BEL,2UL},{-5L,0xD77B64367BA5539BLL,-4L,3UL},{0x24ACC74EL,1L,6L,0x4D36L},{-1L,7L,0x37048C8BL,65533UL},{1L,0x6EF537B0C0A383BBLL,1L,65526UL}},{{0x860D4D3FL,0x536C41C9A28634C4LL,0x0B3D5550L,5UL},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{0xE76D7B4FL,0x82CAF0515AD2BD61LL,0xA7173B68L,0x883AL},{0L,-6L,0xA0F8B4DEL,0x0968L},{0L,3L,0xC43888FEL,0x4ED0L},{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L},{0L,0xA7ADF4067475E74ELL,-1L,1UL},{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L}},{{0x8CF803EEL,0x29AEB5C7A4FAE4F1LL,6L,0x69D2L},{-1L,0x075699735BF4BCF1LL,1L,65535UL},{0x767165B1L,0x2B7D42D0035D7A22LL,5L,0x01A8L},{7L,0xF351D9A808F9EA61LL,-1L,0xE914L},{-1L,4L,0L,1UL},{-5L,0xD77B64367BA5539BLL,-4L,3UL},{1L,8L,-5L,65529UL},{-7L,-1L,0x37DC8C3BL,0x2C4AL},{-3L,0xFFF765E96AB2956CLL,0x64BA08F4L,0UL}},{{4L,1L,0x0A51A35AL,0x4AB0L},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0L,0xA7ADF4067475E74ELL,-1L,1UL},{0L,3L,0xC43888FEL,0x4ED0L},{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L},{0x24059BF2L,1L,3L,0xAB9DL},{0x1217FC43L,0x0264869625EDC320LL,-3L,2UL},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L}},{{1L,8L,-5L,65529UL},{-6L,0x27ED1AFE88F507C0LL,0xAE581D7CL,0x44BDL},{1L,0xC4B3EC1A46F02B0CLL,0x108A0045L,0x94E6L},{-1L,7L,0x37048C8BL,65533UL},{1L,0xC4B3EC1A46F02B0CLL,0x108A0045L,0x94E6L},{-6L,0x27ED1AFE88F507C0LL,0xAE581D7CL,0x44BDL},{1L,8L,-5L,65529UL},{7L,0xF351D9A808F9EA61LL,-1L,0xE914L},{0xFDF403B5L,0L,0L,1UL}}},{{{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{0x24059BF2L,1L,3L,0xAB9DL},{0x8BECD810L,5L,0x87FAD47DL,1UL},{-1L,-1L,0x280CB640L,0xC688L},{0xE76D7B4FL,0x82CAF0515AD2BD61LL,0xA7173B68L,0x883AL},{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{0L,-6L,0xA0F8B4DEL,0x0968L}},{{0x767165B1L,0x2B7D42D0035D7A22LL,5L,0x01A8L},{0xAE496449L,0xCC8A2655BC3BBF9CLL,0x49CF8A29L,0UL},{0xFDF403B5L,0L,0L,1UL},{-6L,0x27ED1AFE88F507C0LL,0xAE581D7CL,0x44BDL},{-1L,1L,-5L,1UL},{0x09EF9A8AL,1L,-1L,0x3684L},{0x24ACC74EL,1L,6L,0x4D36L},{7L,0xF351D9A808F9EA61LL,-1L,0xE914L},{1L,0x0B480AD53C8C47F6LL,1L,0xE44BL}},{{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{0x1217FC43L,0x0264869625EDC320LL,-3L,2UL},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{4L,1L,0x0A51A35AL,0x4AB0L},{0x517735C2L,7L,0x4327E096L,6UL},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{0xE76D7B4FL,0x82CAF0515AD2BD61LL,0xA7173B68L,0x883AL}},{{1L,0x0B480AD53C8C47F6LL,1L,0xE44BL},{-7L,-1L,0x37DC8C3BL,0x2C4AL},{0x4F12353AL,0xD3712AE1C96AE1E7LL,0L,0x7FC4L},{1L,0x9B8748D4908928D5LL,0x798D2CD9L,0x210EL},{-3L,0xFFF765E96AB2956CLL,0x64BA08F4L,0UL},{1L,0x9B8748D4908928D5LL,0x798D2CD9L,0x210EL},{0x4F12353AL,0xD3712AE1C96AE1E7LL,0L,0x7FC4L},{-7L,-1L,0x37DC8C3BL,0x2C4AL},{1L,0x0B480AD53C8C47F6LL,1L,0xE44BL}},{{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L},{0x8BECD810L,5L,0x87FAD47DL,1UL},{4L,1L,0x0A51A35AL,0x4AB0L},{-1L,-1L,0x280CB640L,0xC688L},{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{0L,0xA7ADF4067475E74ELL,-1L,1UL},{0L,-6L,0xA0F8B4DEL,0x0968L}},{{0x4F8B8312L,-1L,0xFBE4A4C4L,0xC007L},{0x09EF9A8AL,1L,-1L,0x3684L},{0x76C53111L,0x8C4A95F93077DE9ALL,0xC4149792L,0x8FA5L},{0x7958726BL,0xB7BCB20436EB8CF3LL,0x5C730896L,0x9729L},{1L,-7L,-3L,65534UL},{6L,-1L,5L,0x0337L},{4L,0xC82FE8F7363D8060LL,-4L,0xFA39L},{-1L,7L,0x37048C8BL,65533UL},{0xFDF403B5L,0L,0L,1UL}},{{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L},{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{0x1217FC43L,0x0264869625EDC320LL,-3L,2UL},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{4L,1L,0x0A51A35AL,0x4AB0L},{0x517735C2L,7L,0x4327E096L,6UL},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L}}},{{{1L,0x0B480AD53C8C47F6LL,1L,0xE44BL},{0x7958726BL,0xB7BCB20436EB8CF3LL,0x5C730896L,0x9729L},{-3L,0xFFF765E96AB2956CLL,0x64BA08F4L,0UL},{-5L,0xD77B64367BA5539BLL,-4L,3UL},{0x76C53111L,0x8C4A95F93077DE9ALL,0xC4149792L,0x8FA5L},{-1L,0x075699735BF4BCF1LL,1L,65535UL},{0x76C53111L,0x8C4A95F93077DE9ALL,0xC4149792L,0x8FA5L},{-5L,0xD77B64367BA5539BLL,-4L,3UL},{-3L,0xFFF765E96AB2956CLL,0x64BA08F4L,0UL}},{{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{6L,0x36F3DFEE124460A8LL,0x75F6686EL,9UL},{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{0L,0x493A0086498C6ED1LL,-7L,0xFCC2L},{0x24059BF2L,1L,3L,0xAB9DL},{0x8BECD810L,5L,0x87FAD47DL,1UL},{-1L,-1L,0x280CB640L,0xC688L},{0xE76D7B4FL,0x82CAF0515AD2BD61LL,0xA7173B68L,0x883AL},{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L}},{{0x767165B1L,0x2B7D42D0035D7A22LL,5L,0x01A8L},{0x09EF9A8AL,1L,-1L,0x3684L},{0x4FA30815L,0xE475B0C8CB501DD5LL,0xCFD874BEL,2UL},{0x6304567FL,0x18D4D31E9AD54990LL,0x34377454L,0xBDCEL},{0x5B51814EL,0xFEB6CC7E3E5D8244LL,-4L,65531UL},{0L,-10L,0L,0xD935L},{1L,0x0B480AD53C8C47F6LL,1L,0xE44BL},{0xF01D9A04L,0L,0xB9924FE6L,0xB41BL},{1L,0x6EF537B0C0A383BBLL,1L,65526UL}},{{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{0x8BECD810L,5L,0x87FAD47DL,1UL},{0x57A8756FL,-6L,0xF7FE1D67L,0UL},{4L,1L,0x0A51A35AL,0x4AB0L},{0L,0xAA941AEDB6C542F4LL,1L,0xBAC0L},{0L,0xA7ADF4067475E74ELL,-1L,1UL},{0L,3L,0xC43888FEL,0x4ED0L},{5L,0xA71C05BCA014D940LL,0xDB1B78DCL,0x5669L},{0x24059BF2L,1L,3L,0xAB9DL}},{{1L,8L,-5L,65529UL},{-7L,-1L,0x37DC8C3BL,0x2C4AL},{-3L,0xFFF765E96AB2956CLL,0x64BA08F4L,0UL},{0x18B373EFL,0xEC55EF644D30E748LL,7L,65527UL},{4L,0xC82FE8F7363D8060LL,-4L,0xFA39L},{-5L,5L,0xE45B22D5L,0UL},{1L,0x6EF537B0C0A383BBLL,1L,65526UL},{-5L,5L,0xE45B22D5L,0UL},{4L,0xC82FE8F7363D8060LL,-4L,0xFA39L}},{{4L,1L,0x0A51A35AL,0x4AB0L},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{0x8193956EL,0L,0x33EB5C9FL,0x6064L},{4L,1L,0x0A51A35AL,0x4AB0L},{0x860D4D3FL,0x536C41C9A28634C4LL,0x0B3D5550L,5UL},{0xCC962EF5L,0xC55C98BD71122475LL,1L,65535UL},{0xE76D7B4FL,0x82CAF0515AD2BD61LL,0xA7173B68L,0x883AL},{0L,-6L,0xA0F8B4DEL,0x0968L},{0L,3L,0xC43888FEL,0x4ED0L}},{{0x8CF803EEL,0x29AEB5C7A4FAE4F1LL,6L,0x69D2L},{0xAE496449L,0xCC8A2655BC3BBF9CLL,0x49CF8A29L,0UL},{0x76C53111L,0x8C4A95F93077DE9ALL,0xC4149792L,0x8FA5L},{0x6304567FL,0x18D4D31E9AD54990LL,0x34377454L,0xBDCEL},{1L,0x0B480AD53C8C47F6LL,1L,0xE44BL},{0x18B373EFL,0xEC55EF644D30E748LL,7L,65527UL},{-1L,4L,0L,1UL},{-1L,0x075699735BF4BCF1LL,1L,65535UL},{1L,0xC4B3EC1A46F02B0CLL,0x108A0045L,0x94E6L}}}};
static uint32_t ***g_2031[1] = {(void*)0};
static int32_t ** volatile g_2047 = &g_226;/* VOLATILE GLOBAL g_2047 */
static union U3 **g_2082 = &g_1580;
static union U3 *** volatile g_2081 = &g_2082;/* VOLATILE GLOBAL g_2081 */


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static int16_t  func_13(int32_t  p_14);
static int64_t  func_28(int8_t * p_29);
static const uint64_t  func_47(uint32_t  p_48);
static uint16_t  func_56(uint16_t  p_57);
static uint16_t  func_58(int64_t  p_59, const int8_t * const  p_60);
static int64_t  func_93(int8_t * p_94, float  p_95, uint8_t  p_96, int8_t  p_97, uint32_t  p_98);
static int8_t  func_101(int32_t  p_102, uint16_t  p_103, int32_t  p_104);
static uint32_t  func_107(int16_t  p_108, uint64_t * p_109, int64_t  p_110, uint32_t  p_111, uint64_t * p_112);
static int16_t  func_115(uint64_t * p_116, int32_t  p_117, uint16_t  p_118);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_36 g_35 g_34 g_53 g_55 g_84 g_540 g_1337 g_1639 g_935 g_159 g_1661 g_223 g_1406.f0 g_246 g_193 g_1348 g_1601.f1 g_743 g_185 g_245 g_345 g_929 g_1568 g_1601.f2 g_1598.f7 g_1598.f2 g_156 g_458 g_936 g_157 g_140.f0 g_191 g_1107 g_336 g_1777 g_927 g_146 g_895 g_1534 g_1662 g_1663 g_1795 g_158 g_893 g_894 g_147 g_140 g_1815 g_741 g_742 g_1538 g_1830 g_681 g_682 g_429 g_430 g_710 g_1862 g_1886 g_1903 g_151 g_135 g_662 g_663 g_1909 g_1417 g_1918 g_1921 g_1926 g_938 g_1517 g_668 g_1598.f6 g_1349 g_939 g_139 g_1601.f5 g_2008 g_76 g_136 g_2031 g_1406.f1 g_2047 g_1921.f0 g_2081
 * writes: g_36 g_53 g_55 g_84 g_76 g_1639 g_1214 g_935 g_1406.f0 g_246 g_193 g_1534 g_140 g_1568 g_1601.f2 g_151 g_226 g_336 g_147 g_157 g_1663 g_1406 g_353 g_1216.f0 g_1357.f0 g_1909 g_1417 g_1918 g_5 g_136 g_35 g_868 g_1305 g_845 g_146 g_447 g_1601.f5 g_191 g_2031 g_223 g_206 g_549 g_2082
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    int8_t *l_4 = &g_5;
    int32_t l_6 = 0x78555145L;
    int8_t *l_32 = &g_5;
    int16_t *l_1746 = &g_151;
    uint64_t *l_1908 = &g_1909;
    int32_t *l_1910 = &g_1417;
    const union U4 **l_1919[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    union U4 *l_1920 = &g_1361[2];
    uint32_t l_1922 = 9UL;
    int32_t *l_1925 = &g_246;
    int32_t l_1935 = 0x9CDA960BL;
    int32_t l_1937 = 0xC074FF10L;
    int32_t l_1938 = (-1L);
    int16_t l_1939 = (-1L);
    int32_t l_1940 = 0xB47AEE98L;
    int32_t l_1941 = 0xC9B65863L;
    union U3 *l_1954 = &g_1921;
    const struct S1 l_1971 = {-4872,18446744073709551607UL,0x44L};
    int16_t ****l_1981 = &g_1639;
    uint8_t l_2028 = 250UL;
    int32_t l_2037 = 0xAB3FD9D1L;
    int32_t *l_2048 = &g_1417;
    int32_t *l_2049 = &g_246;
    int32_t *l_2050 = &g_191;
    int32_t *l_2051 = &l_1937;
    int32_t *l_2052 = &l_1938;
    int32_t *l_2053[8];
    int16_t l_2054[3][7] = {{0x98FBL,0x98FBL,0x98FBL,0x98FBL,0x98FBL,0x98FBL,0x98FBL},{9L,9L,9L,9L,9L,9L,9L},{0x98FBL,0x98FBL,0x98FBL,0x98FBL,0x98FBL,0x98FBL,0x98FBL}};
    uint32_t l_2055[4][10] = {{0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL,0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL},{0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL,0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL},{0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL,0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL},{0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL,0x65165C8AL,7UL,0x3E2224EEL,7UL,0x65165C8AL}};
    int32_t **l_2066 = &l_2052;
    int32_t **l_2067 = &l_2053[0];
    int32_t **l_2068 = (void*)0;
    int32_t **l_2069 = &l_2049;
    int32_t *l_2070 = &l_1935;
    uint8_t l_2071 = 0x17L;
    uint64_t **l_2072 = &g_206[3][5];
    uint64_t **l_2073 = &l_1908;
    const int64_t l_2079 = 0xFF3FF4E0076C03CCLL;
    uint8_t *l_2080 = &g_549[2];
    int i, j;
    for (i = 0; i < 8; i++)
        l_2053[i] = &g_940;
    (*l_1910) ^= ((safe_sub_func_int32_t_s_s((l_4 == &g_5), l_6)) && ((-1L) > (safe_div_func_int64_t_s_s(0L, ((*l_1908) |= (safe_lshift_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(func_13(((safe_div_func_uint32_t_u_u(((safe_add_func_int64_t_s_s((((safe_mul_func_int16_t_s_s(((*l_1746) = (((safe_mod_func_int32_t_s_s((((1L == (safe_mod_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s((safe_unary_minus_func_int16_t_s(0x42C7L)), 6)), func_28(((safe_add_func_uint16_t_u_u(g_5, (l_32 == (void*)0))) , &g_5))))) > l_6) ^ l_6), 0xDB3ED0DAL)) , l_32) == l_4)), l_6)) | l_6) == l_6), l_6)) < l_6), g_191)) , g_1107)), 1UL)), 7)))))));
    if ((((((safe_rshift_func_int8_t_s_s(((((*g_1517) = (((safe_sub_func_int32_t_s_s((*l_1910), (safe_sub_func_int32_t_s_s((*l_1910), ((+((g_1918 = g_1918) == l_1920)) , (g_1921 , (((*l_4) |= l_1922) <= (((((*l_1925) &= ((((*l_1910) && (safe_rshift_func_int16_t_s_s((((g_223 > (0x87BAL == (*l_1910))) , 0x4FBFL) || (*l_1910)), (*l_1910)))) || (*l_1910)) ^ (*l_1910))) == (*l_1910)) & g_1926) | g_938)))))))) == (*l_1910)) != 255UL)) , 18446744073709551615UL) > (*l_1910)), 2)) & (*l_1910)) ^ 0UL) ^ 0xA7L) || g_668))
    { /* block id: 790 */
        int32_t *l_1934[7][4] = {{&g_1417,&g_1348,&g_1417,(void*)0},{&l_6,&g_1348,&g_940,&g_1348},{&l_6,(void*)0,&g_1417,&g_1348},{&g_1417,&g_1348,&g_1417,(void*)0},{&l_6,&g_1348,&g_940,&g_1348},{&l_6,(void*)0,&g_1417,&g_1348},{&g_1417,&g_1348,&g_1417,(void*)0}};
        int32_t l_1936 = 0x67A58167L;
        uint32_t l_1942 = 0xA4F3AC8EL;
        uint32_t **l_1957 = &g_1518;
        uint32_t ***l_1956 = &l_1957;
        uint32_t **l_1959 = (void*)0;
        uint32_t ***l_1958 = &l_1959;
        uint32_t ****l_1960 = &l_1958;
        int16_t l_1961[3];
        int i, j;
        for (i = 0; i < 3; i++)
            l_1961[i] = 0x3AAFL;
        for (g_193 = 0; (g_193 > (-17)); g_193 = safe_sub_func_int64_t_s_s(g_193, 1))
        { /* block id: 793 */
            int32_t *l_1930 = &g_1417;
            int32_t **l_1931 = &g_226;
            int32_t **l_1933 = &l_1925;
            (*l_1933) = ((*l_1931) = l_1930);
        }
        --l_1942;
        (*g_540) = ((safe_add_func_float_f_f((safe_mul_func_float_f_f(((((-(safe_add_func_float_f_f((((safe_add_func_uint64_t_u_u(((*l_1908) = (((void*)0 == l_1954) > (((**g_1661) , 6UL) >= (g_1598.f6 == ((*l_1910) = (((safe_unary_minus_func_int32_t_s((l_1956 == &g_1263[4]))) , ((*l_1960) = l_1958)) == (void*)0)))))), (-1L))) == g_1886[2].f7) , (-0x1.4p+1)), (*g_429)))) >= (*g_429)) >= (*g_429)) > l_1961[2]), (*g_429))), (*g_429))) <= 0x0.6p+1);
        g_35 |= (*l_1910);
    }
    else
    { /* block id: 803 */
        int8_t **l_1972[6][3][3] = {{{&l_32,&l_32,&l_4},{&l_32,&l_32,&l_4},{&l_32,&l_32,(void*)0}},{{&l_32,&l_32,&g_147},{&l_32,&l_32,(void*)0},{&l_32,&l_32,&l_4}},{{&l_32,&l_32,&l_4},{&l_32,&l_32,(void*)0},{&l_32,&l_32,&g_147}},{{&l_32,&l_32,(void*)0},{&l_32,&l_32,&l_4},{&l_32,&l_32,&l_4}},{{&l_32,&l_32,(void*)0},{&l_32,&l_32,&g_147},{&l_32,&l_32,(void*)0}},{{&l_32,&l_32,&l_4},{&l_32,&l_32,&l_4},{&l_32,&l_32,(void*)0}}};
        int32_t l_1980 = 0xEE49A7ACL;
        int32_t *l_1991[3];
        struct S1 l_2014[9] = {{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL},{-528,0xEC089C9BA5999F83LL,0x7BL}};
        uint32_t l_2017[7];
        int32_t l_2042 = 0x29AB017DL;
        uint32_t l_2043[3];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1991[i] = (void*)0;
        for (i = 0; i < 7; i++)
            l_2017[i] = 18446744073709551615UL;
        for (i = 0; i < 3; i++)
            l_2043[i] = 0x856EA0EAL;
        for (g_1417 = 9; (g_1417 >= 0); g_1417 -= 1)
        { /* block id: 806 */
            union U4 *l_1962 = &g_1359[2];
            uint8_t *l_1986[1][5];
            int32_t l_1987[10][4][6] = {{{0x8FF029AFL,0xAAF6DC25L,(-6L),(-6L),0xAAF6DC25L,0x8FF029AFL},{0x1DD1EC91L,(-6L),0x0970773AL,8L,0xAAF6DC25L,0x34180009L},{8L,0xAAF6DC25L,0x34180009L,(-5L),0x8FF029AFL,0x8FF029AFL},{8L,(-5L),(-5L),8L,0x1DD1EC91L,0x0970773AL}},{{0x1DD1EC91L,0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL},{0x8FF029AFL,(-6L),0x34180009L,8L,8L,0x34180009L},{0xAAF6DC25L,0xAAF6DC25L,0x0970773AL,(-5L),0x1DD1EC91L,0x8FF029AFL},{0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL,0x0970773AL}},{{0x8FF029AFL,0xAAF6DC25L,(-6L),(-6L),0xAAF6DC25L,0x8FF029AFL},{0x1DD1EC91L,(-6L),0x0970773AL,8L,0xAAF6DC25L,0x34180009L},{8L,0xAAF6DC25L,0x34180009L,(-5L),0x8FF029AFL,0x8FF029AFL},{8L,(-5L),(-5L),8L,0x1DD1EC91L,0x0970773AL}},{{0x1DD1EC91L,0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL},{0x8FF029AFL,(-6L),0x34180009L,8L,8L,0x34180009L},{0xAAF6DC25L,0xAAF6DC25L,0x0970773AL,(-5L),0x1DD1EC91L,0x8FF029AFL},{0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL,0x0970773AL}},{{0x8FF029AFL,0xAAF6DC25L,(-6L),(-6L),0xAAF6DC25L,0x8FF029AFL},{0x1DD1EC91L,(-6L),0x0970773AL,8L,0xAAF6DC25L,0x34180009L},{8L,0xAAF6DC25L,0x34180009L,(-5L),0x8FF029AFL,0x8FF029AFL},{8L,(-5L),(-5L),8L,0x1DD1EC91L,0x0970773AL}},{{0x1DD1EC91L,0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL},{0x8FF029AFL,(-6L),0x34180009L,8L,8L,0x34180009L},{0xAAF6DC25L,0xAAF6DC25L,0x0970773AL,(-5L),0x1DD1EC91L,0x8FF029AFL},{0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL,0x0970773AL}},{{0x8FF029AFL,0xAAF6DC25L,(-6L),(-6L),0xAAF6DC25L,0x8FF029AFL},{0x1DD1EC91L,(-6L),0x0970773AL,8L,0xAAF6DC25L,0x34180009L},{8L,0xAAF6DC25L,0x34180009L,(-5L),0x8FF029AFL,0x8FF029AFL},{8L,(-5L),(-5L),8L,0x1DD1EC91L,0x0970773AL}},{{0x1DD1EC91L,0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL},{0x8FF029AFL,(-6L),0x34180009L,8L,8L,0x34180009L},{0xAAF6DC25L,0xAAF6DC25L,0x0970773AL,(-5L),0x1DD1EC91L,0x8FF029AFL},{0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL,0x0970773AL}},{{0x8FF029AFL,0xAAF6DC25L,(-6L),(-6L),0xAAF6DC25L,0x8FF029AFL},{0x1DD1EC91L,(-6L),0x0970773AL,8L,0xAAF6DC25L,0x34180009L},{8L,0xAAF6DC25L,0x34180009L,(-5L),0x8FF029AFL,0x8FF029AFL},{8L,(-5L),(-5L),8L,0x1DD1EC91L,0x0970773AL}},{{0x1DD1EC91L,0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL},{0x8FF029AFL,(-6L),0x34180009L,8L,8L,0x34180009L},{0xAAF6DC25L,0xAAF6DC25L,0x0970773AL,(-5L),0x1DD1EC91L,0x8FF029AFL},{0xAAF6DC25L,(-5L),(-6L),8L,0x8FF029AFL,0x0970773AL}}};
            uint16_t l_1993 = 1UL;
            int32_t l_2038[7][1] = {{0L},{(-7L)},{0L},{(-7L)},{0L},{(-7L)},{0L}};
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 5; j++)
                    l_1986[i][j] = &g_139;
            }
            for (g_868 = 2; (g_868 >= 0); g_868 -= 1)
            { /* block id: 809 */
                int32_t **l_1965 = &g_226;
                int32_t **l_1966 = &l_1925;
                for (g_1305 = 0; (g_1305 <= 2); g_1305 += 1)
                { /* block id: 812 */
                    for (g_845 = 0; (g_845 <= 2); g_845 += 1)
                    { /* block id: 815 */
                        union U4 *l_1963[8][4] = {{(void*)0,&g_1964,&g_951,&g_951},{&g_1360[1],&g_1360[1],&g_1361[7],&g_1964},{&g_1964,(void*)0,&g_1361[7],(void*)0},{&g_1360[1],(void*)0,&g_951,&g_1361[7]},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1360[1],&g_1964},{(void*)0,&g_1360[1],(void*)0,&g_951},{(void*)0,&g_1964,&g_951,&g_951}};
                        int i, j;
                        l_1963[7][0] = l_1962;
                    }
                }
            }
            if (((safe_mul_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(((l_1971 , (l_1972[4][0][2] == ((*g_927) = ((**g_1661) , (((((((*l_1925) = (safe_rshift_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((((safe_mul_func_int8_t_s_s(l_1980, ((void*)0 == l_1981))) != ((safe_rshift_func_uint16_t_u_u(0x1736L, (((0x51A5L && ((((safe_rshift_func_int16_t_s_u(l_1980, (**g_894))) , l_1986[0][1]) != &g_159[5][4][1]) , (-10L))) || (*l_1910)) > 0xF76BC6F7L))) != g_1886[2].f7)) , (*g_681)) ^ l_1980), 0x74L)), l_1980))) < 1L) < 0L) || 0xE1L) , g_1601.f2) , &l_32))))) , (-1L)), 13)), l_1987[5][1][5])) <= g_1349))
            { /* block id: 825 */
                int32_t **l_1990[2][4][10] = {{{&l_1925,&l_1910,&g_226,&g_226,&l_1910,&l_1925,&l_1910,(void*)0,&l_1925,&l_1925},{&l_1910,&l_1925,&l_1910,(void*)0,&g_226,&l_1910,&l_1925,&l_1925,&g_226,&g_226},{&l_1910,(void*)0,&g_226,&l_1925,&l_1910,&l_1925,&g_226,(void*)0,&l_1910,&l_1925},{&l_1925,&g_226,(void*)0,&l_1910,&l_1925,&l_1925,&l_1925,&g_226,(void*)0,&l_1925}},{{(void*)0,&l_1910,&l_1925,&l_1910,(void*)0,(void*)0,(void*)0,(void*)0,&l_1910,&l_1925},{&g_226,&g_226,&l_1910,&l_1925,&l_1910,(void*)0,&l_1925,&l_1925,&g_226,&l_1925},{&g_226,&l_1925,&l_1925,(void*)0,(void*)0,&g_226,&l_1925,&l_1925,&l_1925,&g_226},{&l_1925,&g_226,(void*)0,&g_226,&l_1925,&l_1925,(void*)0,(void*)0,&l_1910,&l_1910}}};
                int64_t l_1992 = 0L;
                int i, j, k;
                (*g_540) = (safe_div_func_float_f_f((*l_1910), 0x8.F910D2p-49));
                l_1991[1] = &g_940;
                --l_1993;
                for (g_1909 = (-23); (g_1909 <= 56); g_1909 = safe_add_func_uint32_t_u_u(g_1909, 9))
                { /* block id: 831 */
                    int16_t l_1998 = 0xB064L;
                    if (l_1998)
                        break;
                    for (g_447 = 22; (g_447 <= 28); g_447 = safe_add_func_uint32_t_u_u(g_447, 1))
                    { /* block id: 835 */
                        uint32_t *l_2006 = (void*)0;
                        uint32_t *l_2007[5][2][3] = {{{&g_84[0][0],&g_1534,(void*)0},{&g_84[0][0],(void*)0,&g_946}},{{&g_1534,&g_1534,&g_946},{&g_946,(void*)0,(void*)0}},{{&g_1534,(void*)0,&g_1534},{&g_84[0][0],&g_1534,(void*)0}},{{&g_84[0][0],(void*)0,&g_946},{&g_1534,&g_1534,&g_946}},{{&g_946,(void*)0,(void*)0},{&g_1534,(void*)0,&g_1534}}};
                        int i, j, k;
                        g_140.f0 &= ((0x74B8327BL > ((safe_add_func_int32_t_s_s(g_939[4], (g_1601.f5 &= ((+1UL) > (l_1998 ^ (g_139 != 0UL)))))) >= (g_2008[0][5][2] , (safe_add_func_int8_t_s_s(4L, 6L))))) > 8UL);
                    }
                    for (g_191 = 20; (g_191 != (-3)); --g_191)
                    { /* block id: 841 */
                        uint8_t l_2013[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_2013[i] = 0UL;
                        l_2013[2] = ((*l_1925) | (((*g_540) , 1L) || (((**g_1661) , l_32) != (**g_927))));
                        g_226 = &l_1987[5][1][5];
                        l_1991[0] = &l_1980;
                        l_1910 = &g_1417;
                    }
                    for (g_136 = 0; (g_136 <= 0); g_136 += 1)
                    { /* block id: 849 */
                        float l_2015[4][1][1] = {{{0x7.B4362Ep+37}},{{0xA.8717FBp-93}},{{0x7.B4362Ep+37}},{{0xA.8717FBp-93}}};
                        int i, j, k;
                        (*l_1925) |= (1L & (l_2014[2] , 0UL));
                        return l_1998;
                    }
                }
            }
            else
            { /* block id: 854 */
                int32_t l_2016 = (-3L);
                uint32_t ****l_2032 = &g_2031[0];
                int32_t l_2039 = 0xBAF12322L;
                l_2017[3]++;
                l_2039 &= (((((safe_mul_func_int16_t_s_s((safe_add_func_uint8_t_u_u(0x9EL, (safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((l_2028 , ((safe_mul_func_uint8_t_u_u((&g_1263[4] == &g_1263[4]), (&g_1365 != ((*l_2032) = g_2031[0])))) && (((*l_1910) , (((**g_894) <= ((((safe_mul_func_uint8_t_u_u((((***g_927) = ((*l_1910) > g_1406[0].f1)) & l_1993), 0x27L)) >= (*l_1925)) == l_2037) > l_1987[5][1][5])) || 0x32191D35L)) != 0xB9L))), l_2016)), 4294967295UL)))), 0x6104L)) & 1L) , 0x5.810CD1p+21) >= l_2038[1][0]) , l_2016);
            }
        }
        l_2043[1] = ((safe_div_func_float_f_f((*g_429), (-0x1.Cp-1))) > l_2042);
        for (g_223 = 3; (g_223 < 27); g_223 = safe_add_func_uint16_t_u_u(g_223, 6))
        { /* block id: 864 */
            int32_t *l_2046[10] = {&g_1348,(void*)0,&g_1417,&g_1417,(void*)0,&g_1348,(void*)0,&g_1417,&g_1417,(void*)0};
            int i;
            (*g_2047) = l_2046[1];
        }
        (*g_540) = (l_1910 == l_1910);
    }
    ++l_2055[2][4];
    (*g_2081) = ((safe_add_func_uint8_t_u_u((safe_mod_func_int8_t_s_s(((0x05BCL < (((*l_2072) = ((safe_lshift_func_int16_t_s_u(((*l_1746) |= ((&g_226 == l_2066) > (**l_2066))), (l_2071 = (((*l_2069) = ((*l_2067) = (void*)0)) == l_2070)))) , &g_1107)) != ((*l_2073) = &g_223))) && (safe_mul_func_uint8_t_u_u(((*l_2080) = (safe_unary_minus_func_uint16_t_u((safe_mod_func_uint16_t_u_u(((l_2079 >= 0x50F07D89L) < (*l_2048)), (*l_2052)))))), (*l_2052)))), 4L)), g_1921.f0)) , &g_1580);
    return (*l_1910);
}


/* ------------------------------------------ */
/* 
 * reads : g_1777 g_927 g_146 g_895 g_336 g_246 g_1534 g_1661 g_1662 g_1663 g_1795 g_158 g_893 g_894 g_147 g_157 g_140 g_1815 g_55 g_741 g_742 g_1538 g_1830 g_540 g_681 g_682 g_429 g_430 g_710 g_1862 g_1886 g_1903 g_151 g_135 g_662 g_663 g_53
 * writes: g_53 g_226 g_336 g_147 g_157 g_1534 g_1663 g_140.f2 g_140 g_55 g_76 g_1406 g_353 g_151 g_1216.f0 g_246 g_1357.f0
 */
static int16_t  func_13(int32_t  p_14)
{ /* block id: 718 */
    int16_t l_1753 = (-3L);
    int32_t l_1754[2][1][2];
    float l_1756 = 0x2.56EE80p-18;
    float l_1757 = 0x1.2p-1;
    uint64_t l_1761 = 0x6FD604196BF1D533LL;
    int32_t *l_1766 = &l_1754[1][0][1];
    int32_t *l_1767 = &g_246;
    int32_t *l_1768 = (void*)0;
    int32_t *l_1769 = (void*)0;
    int32_t *l_1770[2];
    int64_t l_1771 = (-2L);
    int32_t l_1772 = 0x100011C3L;
    float l_1773 = 0x1.7C9658p-11;
    uint32_t l_1774[4][2] = {{1UL,18446744073709551615UL},{1UL,18446744073709551615UL},{1UL,18446744073709551615UL},{1UL,18446744073709551615UL}};
    int8_t *l_1779 = &g_157[0];
    uint16_t l_1791 = 65528UL;
    uint32_t *l_1800 = &g_84[0][3];
    uint32_t **l_1799 = &l_1800;
    struct S1 l_1861 = {1069,0xF5CBF11C20F04D95LL,0x98L};
    int8_t l_1867 = 0L;
    int16_t l_1868 = 0x9AB9L;
    float l_1869 = 0xC.764E6Dp-51;
    float l_1871 = 0x1.2p-1;
    uint16_t l_1872 = 0UL;
    uint64_t l_1875 = 18446744073709551615UL;
    float **l_1889 = (void*)0;
    int32_t l_1898 = 0x2D6B17CFL;
    uint64_t *l_1899 = &l_1861.f1;
    uint64_t *l_1900 = (void*)0;
    uint64_t *l_1901 = (void*)0;
    uint64_t *l_1902 = &g_353;
    int16_t *l_1904[9][8] = {{(void*)0,&l_1753,&g_135,(void*)0,(void*)0,&g_135,&l_1753,(void*)0},{&g_844,(void*)0,&l_1868,(void*)0,&g_1336,&g_844,(void*)0,&g_151},{&g_1336,&g_844,(void*)0,&g_151,&g_135,&l_1868,(void*)0,&g_135},{&l_1868,(void*)0,(void*)0,&l_1868,&g_844,&g_151,(void*)0,(void*)0},{&g_135,&g_844,&g_936,&g_936,&g_844,&g_135,(void*)0,(void*)0},{&l_1868,(void*)0,&l_1868,(void*)0,&g_135,&l_1868,&g_844,&l_1868},{(void*)0,(void*)0,&l_1753,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1868,(void*)0,&g_135,&g_936,(void*)0,&l_1868,&l_1868,(void*)0},{&l_1868,&g_135,&g_135,&l_1868,(void*)0,&l_1868,(void*)0,&g_135}};
    float *l_1905 = &g_1216.f0;
    float *l_1906 = &g_1357.f0;
    float *l_1907 = &l_1756;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
                l_1754[i][j][k] = (-1L);
        }
    }
    for (i = 0; i < 2; i++)
        l_1770[i] = &g_153;
lbl_1792:
    for (g_53 = 0; (g_53 <= 46); g_53 = safe_add_func_uint64_t_u_u(g_53, 5))
    { /* block id: 721 */
        int32_t *l_1749 = (void*)0;
        int32_t *l_1750 = &g_34;
        int32_t *l_1751 = &g_940;
        int32_t *l_1752[1];
        int32_t l_1755 = (-10L);
        int64_t l_1759 = (-1L);
        int32_t l_1760 = 0xE4FB9371L;
        int32_t **l_1764 = (void*)0;
        int32_t **l_1765 = &g_226;
        int i;
        for (i = 0; i < 1; i++)
            l_1752[i] = &g_191;
        --l_1761;
        (*l_1765) = &p_14;
        for (g_336 = 0; (g_336 <= 3); g_336 += 1)
        { /* block id: 726 */
            return p_14;
        }
    }
    l_1774[0][1]++;
    if ((g_1777[2][6] , ((!(((*l_1779) = (((**g_927) = l_1779) != (void*)0)) != (safe_mod_func_uint8_t_u_u((*l_1766), p_14)))) | (safe_mod_func_uint64_t_u_u((!(((safe_sub_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((0xBFC5F799L ^ 0xB7A7B740L) >= (safe_lshift_func_int16_t_s_s(p_14, (0x7EB4AAB1L != l_1791)))), (*g_895))), (*l_1766))) == 1UL) >= (*l_1766))), (*l_1767))))))
    { /* block id: 733 */
        if (g_53)
            goto lbl_1792;
    }
    else
    { /* block id: 735 */
        uint64_t l_1811 = 8UL;
        int32_t l_1813 = 3L;
        uint32_t **l_1831 = (void*)0;
        uint32_t l_1856[8][4][5] = {{{0x562CC9DDL,4294967295UL,0x501BCC57L,4294967288UL,0x59A66FF4L},{4294967288UL,0xF54C1712L,0x33D92098L,4294967291UL,0x4807CF99L},{0x232FAF38L,1UL,0x3B2114B5L,4294967295UL,0xCD2864BBL},{0xF54C1712L,0x99640622L,0x5825B937L,2UL,2UL}},{{0x232FAF38L,0x6149550AL,0x232FAF38L,0xF620373AL,0x6F18035EL},{4294967288UL,0x33D92098L,0x874338ADL,0xF54C1712L,0x553F7C21L},{0x562CC9DDL,4294967295UL,4294967295UL,0x6DA55BFDL,0UL},{1UL,0x553F7C21L,0x874338ADL,0x553F7C21L,1UL}},{{0x59A66FF4L,4UL,0x232FAF38L,1UL,0x3B2114B5L},{4294967291UL,0x4807CF99L,0x5825B937L,4294967287UL,0xC3D11F56L},{0x35FC0553L,0xBF0AFD44L,0x3B2114B5L,4UL,0x3B2114B5L},{4294967287UL,4294967287UL,0x33D92098L,0x5825B937L,1UL}},{{0x3B2114B5L,4294967295UL,0x501BCC57L,1UL,0UL},{0x874338ADL,0xF2DD7FFCL,0xC3D11F56L,0x99640622L,0x553F7C21L},{0x02A2F5D4L,4294967295UL,0xCD2864BBL,0UL,0x6F18035EL},{0x4807CF99L,4294967287UL,0x5FA76A16L,1UL,2UL}},{{0x501BCC57L,0xBF0AFD44L,4294967287UL,4294967295UL,0xCD2864BBL},{0xF2DD7FFCL,0x4807CF99L,1UL,1UL,0x4807CF99L},{0UL,4UL,0x3710E3AEL,0UL,0x59A66FF4L},{0x2EC0CC06L,0x553F7C21L,4294967287UL,0x99640622L,0x5FA76A16L}},{{1UL,4294967295UL,1UL,1UL,4294967295UL},{0x2EC0CC06L,0x33D92098L,0x2EC0CC06L,0x5825B937L,4294967290UL},{0UL,0x6149550AL,0x35FC0553L,4UL,0UL},{0xF2DD7FFCL,0x99640622L,2UL,4294967287UL,4294967288UL}},{{0x501BCC57L,1UL,0x35FC0553L,1UL,0x501BCC57L},{0x4807CF99L,0xF54C1712L,0x2EC0CC06L,0x553F7C21L,4294967287UL},{0x02A2F5D4L,4294967295UL,1UL,0x6DA55BFDL,4294967287UL},{0x874338ADL,0x2EC0CC06L,4294967287UL,0xF54C1712L,0x874338ADL}},{{0x35FC0553L,4294967295UL,0UL,4294967295UL,0x232FAF38L},{0x874338ADL,0xC3D11F56L,0x2EC0CC06L,1UL,0x5825B937L},{1UL,1UL,4294967295UL,1UL,0x3B2114B5L},{4294967288UL,0xC3D11F56L,0xC3D11F56L,4294967288UL,0x33D92098L}}};
        int32_t l_1863[8][4] = {{0x833FF7D0L,3L,0x87054C57L,0x87054C57L},{0x82C87D35L,0x82C87D35L,0x833FF7D0L,0x87054C57L},{0x21A29AC0L,3L,0x21A29AC0L,0x833FF7D0L},{0x21A29AC0L,0x833FF7D0L,0x833FF7D0L,0x21A29AC0L},{0x82C87D35L,0x833FF7D0L,0x87054C57L,0x833FF7D0L},{0x833FF7D0L,3L,0x87054C57L,0x87054C57L},{0x82C87D35L,0x82C87D35L,0x833FF7D0L,0x87054C57L},{0x21A29AC0L,3L,0x21A29AC0L,0x833FF7D0L}};
        int64_t l_1864 = 0x5D61153D1F5D9719LL;
        int32_t l_1865 = 0xCBDF535AL;
        int32_t l_1866 = 0x15955715L;
        int i, j, k;
        for (g_1534 = 0; (g_1534 >= 36); g_1534 = safe_add_func_int32_t_s_s(g_1534, 8))
        { /* block id: 738 */
            uint8_t *l_1803 = &g_140.f2;
            uint32_t l_1804 = 0x1CE1055AL;
            int16_t *l_1812[2][10][4] = {{{(void*)0,&g_936,&g_936,(void*)0},{(void*)0,&g_936,&g_151,&g_936},{&g_936,(void*)0,&g_151,&g_151},{(void*)0,(void*)0,&g_936,&g_151},{(void*)0,(void*)0,(void*)0,&g_936},{(void*)0,&g_936,&g_936,(void*)0},{(void*)0,&g_936,&g_151,&g_936},{&g_936,(void*)0,&g_151,&g_151},{(void*)0,(void*)0,&g_936,&g_151},{(void*)0,(void*)0,(void*)0,&g_936}},{{(void*)0,&g_936,&g_936,(void*)0},{(void*)0,&g_936,&g_151,&g_936},{&g_936,(void*)0,&g_151,&g_151},{(void*)0,(void*)0,&g_936,&g_151},{(void*)0,(void*)0,(void*)0,&g_936},{(void*)0,&g_936,&g_936,(void*)0},{(void*)0,&g_936,&g_151,&g_936},{&g_936,(void*)0,&g_151,&g_151},{(void*)0,(void*)0,&g_936,&g_151},{(void*)0,(void*)0,(void*)0,&g_936}}};
            int32_t l_1849[4] = {0xB3B2DCB9L,0xB3B2DCB9L,0xB3B2DCB9L,0xB3B2DCB9L};
            uint32_t l_1855 = 0x24F37D36L;
            float ***l_1857 = &g_1249;
            float ****l_1858 = (void*)0;
            float ***l_1860 = &g_1249;
            float ****l_1859 = &l_1860;
            int i, j, k;
            (*g_1662) = (**g_1661);
            if ((g_1795 , (safe_rshift_func_int16_t_s_s((((*l_1766) ^= (!(((((l_1813 = (g_158 == (((l_1799 == &g_1366) != (((*l_1803) = p_14) >= ((l_1804 && ((((((safe_mul_func_int16_t_s_s((((***g_893) = ((p_14 , ((safe_mul_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u(l_1811, (***g_893))), 0xEBL)) && (*l_1767))) || l_1811)) > 0UL), p_14)) & 0L) == (**g_146)) & p_14) , 0UL) <= p_14)) <= l_1804))) ^ p_14))) == (*l_1767)) , l_1811) & l_1804) | (*g_147)))) > 0xF6F3F3FAL), 12))))
            { /* block id: 744 */
                (*g_1815) = g_140;
            }
            else
            { /* block id: 746 */
                int8_t l_1829 = 0x85L;
                int32_t l_1833 = 0xF36BA4D9L;
                uint64_t *l_1852[10] = {&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353,&g_353};
                int i;
                for (l_1761 = 0; (l_1761 <= 2); l_1761 += 1)
                { /* block id: 749 */
                    uint16_t l_1817 = 1UL;
                    int32_t l_1832[3][6][8] = {{{(-3L),(-3L),0L,1L,(-1L),1L,0L,(-3L)},{(-3L),0x70F2C52EL,0x24E86FE4L,0L,0L,0x24E86FE4L,0x70F2C52EL,(-3L)},{0x70F2C52EL,0xA170E1D4L,(-3L),1L,(-3L),0xA170E1D4L,0x70F2C52EL,0x70F2C52EL},{0xA170E1D4L,1L,0x24E86FE4L,0x24E86FE4L,1L,0xA170E1D4L,0L,0xA170E1D4L},{1L,0xA170E1D4L,0L,0xA170E1D4L,1L,0x24E86FE4L,0x24E86FE4L,1L},{0xA170E1D4L,0x70F2C52EL,0x70F2C52EL,0x70F2C52EL,(-1L),0x24E86FE4L,(-1L),0x70F2C52EL}},{{1L,(-1L),1L,0L,(-3L),(-3L),0L,1L},{(-1L),(-1L),(-3L),0x24E86FE4L,0xA170E1D4L,0x24E86FE4L,(-3L),(-1L)},{(-1L),1L,0L,(-3L),(-3L),0L,1L,(-1L)},{1L,0x70F2C52EL,(-1L),0x24E86FE4L,(-1L),0x70F2C52EL,1L,1L},{0x70F2C52EL,0x24E86FE4L,0L,0L,0x24E86FE4L,0x70F2C52EL,(-3L),0x70F2C52EL},{0x24E86FE4L,0x70F2C52EL,(-3L),0x70F2C52EL,0x24E86FE4L,0L,0L,0x24E86FE4L}},{{0x70F2C52EL,1L,1L,0x70F2C52EL,(-1L),0x24E86FE4L,(-1L),0x70F2C52EL},{1L,(-1L),1L,0L,(-3L),(-3L),0L,1L},{(-1L),(-1L),(-3L),0x24E86FE4L,0xA170E1D4L,0x24E86FE4L,(-3L),(-1L)},{(-1L),1L,0L,(-3L),(-3L),0L,1L,(-1L)},{1L,0x70F2C52EL,(-1L),0x24E86FE4L,(-1L),0x70F2C52EL,1L,1L},{0x70F2C52EL,0x24E86FE4L,0L,0L,0x24E86FE4L,0x70F2C52EL,(-3L),0x70F2C52EL}}};
                    uint8_t l_1834 = 0x7BL;
                    float *l_1848 = &l_1773;
                    int i, j, k;
                    for (g_55 = 0; (g_55 <= 2); g_55 += 1)
                    { /* block id: 752 */
                        int i, j;
                        l_1817 ^= ((&g_540 != &g_429) > ((*g_741) , ((0x876CL || (+(-1L))) == 6L)));
                        l_1832[0][1][6] = (((((g_1538[0] , (safe_lshift_func_int16_t_s_u((((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s((+p_14), ((safe_rshift_func_int8_t_s_s(((**g_146) &= p_14), 0)) , (0UL | ((((*g_146) != (void*)0) < ((safe_mod_func_int16_t_s_s(p_14, (((0L && (l_1829 != 65532UL)) < p_14) && g_1830))) >= 0x8C3BL)) && l_1811))))) <= l_1829), 5)) , l_1829) & 0xE0BE7390D31A608ALL), 1))) , l_1831) == l_1831) && p_14) > 4L);
                        l_1834++;
                    }
                    (*l_1766) ^= (safe_mod_func_uint64_t_u_u((l_1833 = (p_14 > (((((l_1849[1] = ((safe_add_func_float_f_f((+((*g_540) = p_14)), (0x2.87A956p-39 >= (safe_mul_func_float_f_f(0x0.Ep+1, (safe_div_func_float_f_f((0xC.7A0A7Dp-25 > ((((*l_1848) = (safe_sub_func_float_f_f(l_1833, p_14))) <= ((*g_681) , (0x9.684F5Ep+82 <= p_14))) != p_14)), (*g_429)))))))) < l_1813)) < (*g_429)) <= l_1833) , l_1849[1]) != l_1804))), l_1811));
                }
                (*l_1766) ^= (((safe_div_func_uint32_t_u_u(((((l_1811 | 0x0A50F505L) >= (2L >= p_14)) || ((l_1849[1] = l_1811) < g_710[4][6][1])) == 0x38BDL), 8L)) , (safe_rshift_func_int8_t_s_s((l_1855 | 7L), l_1856[2][0][3]))) ^ p_14);
            }
            (*g_540) = (l_1857 != ((*l_1859) = &g_1250));
        }
        (*g_1862) = l_1861;
        --l_1872;
        return l_1875;
    }
    (*l_1907) = (safe_div_func_float_f_f((safe_sub_func_float_f_f((((*l_1906) = (safe_mul_func_float_f_f((0xD.CCBECCp+4 == ((*l_1767) = (((*l_1905) = (safe_div_func_float_f_f(((*g_540) = (0x9.47A1E4p+58 != ((*l_1766) = ((safe_sub_func_uint64_t_u_u((((g_151 ^= (p_14 , (g_1886[2] , (safe_rshift_func_uint8_t_u_s(((l_1889 != (((safe_div_func_uint8_t_u_u((((*l_1902) = ((*l_1899) = (p_14 <= (((*l_1767) < (safe_add_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s(((((p_14 , (((*l_1766) , (*l_1766)) != p_14)) | (*l_1767)) | p_14) < 0xAAA43121L), 6)), 0xF0D750EFDCE64D26LL))) != l_1898)))) , p_14), g_1663.f1)) >= 0L) , (void*)0)) <= 3UL), g_1903))))) == (*l_1766)) | p_14), g_135)) , p_14)))), 0x0.3p-1))) != (*l_1767)))), 0xB.46A524p-85))) != p_14), p_14)), p_14));
    return (*g_662);
}


/* ------------------------------------------ */
/* 
 * reads : g_36 g_35 g_5 g_34 g_53 g_55 g_84 g_540 g_1337 g_1639 g_935 g_159 g_1661 g_223 g_1406.f0 g_246 g_193 g_1348 g_1601.f1 g_743 g_185 g_245 g_345 g_929 g_1568 g_1601.f2 g_1598.f7 g_1598.f2 g_156 g_458 g_936 g_157 g_140.f0
 * writes: g_36 g_53 g_55 g_84 g_76 g_1639 g_1214 g_935 g_1406.f0 g_246 g_193 g_1534 g_140 g_1568 g_1601.f2
 */
static int64_t  func_28(int8_t * p_29)
{ /* block id: 1 */
    int32_t *l_33[9] = {(void*)0,&g_34,(void*)0,&g_34,(void*)0,&g_34,(void*)0,&g_34,(void*)0};
    int64_t l_1726 = 0x1984D68F2A3BC2A7LL;
    uint32_t *l_1727[3];
    uint32_t l_1728 = 0xDB49EA69L;
    struct S1 l_1733 = {3044,0x00443F15F92D0E83LL,255UL};
    uint64_t l_1738 = 18446744073709551609UL;
    int8_t *l_1739 = &g_1568;
    int16_t *l_1740[1][6][3] = {{{&g_844,&g_844,&g_151},{&g_936,&g_151,&g_151},{&g_151,(void*)0,&g_844},{&g_936,(void*)0,&g_936},{&g_844,&g_151,&g_844},{&g_844,&g_844,&g_151}}};
    int32_t l_1741 = 0xF3DF452FL;
    const int32_t l_1742[9][8] = {{1L,0x16F871C3L,0x16F871C3L,1L,1L,1L,0x16F871C3L,0x16F871C3L},{0x16F871C3L,1L,1L,1L,1L,0x16F871C3L,1L,1L},{1L,1L,1L,0x16F871C3L,0x16F871C3L,1L,1L,1L},{(-10L),0x16F871C3L,1L,0x16F871C3L,(-10L),(-10L),0x16F871C3L,1L},{(-10L),(-10L),0x16F871C3L,1L,0x16F871C3L,(-10L),(-10L),0x16F871C3L},{1L,0x16F871C3L,0x16F871C3L,1L,1L,1L,0x16F871C3L,0x16F871C3L},{0x16F871C3L,1L,1L,1L,1L,0x16F871C3L,1L,1L},{1L,1L,1L,0x16F871C3L,0x16F871C3L,1L,1L,1L},{0x16F871C3L,1L,1L,1L,0x16F871C3L,0x16F871C3L,1L,1L}};
    int8_t l_1743 = 0L;
    int16_t l_1744 = 0xDCDFL;
    int8_t l_1745 = 0x88L;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1727[i] = (void*)0;
    g_36[2]--;
    g_1601.f2 = ((safe_lshift_func_uint16_t_u_s(((g_35 <= (safe_add_func_int64_t_s_s((safe_mod_func_uint64_t_u_u(g_5, (safe_mul_func_int16_t_s_s((func_47((!g_34)) >= (safe_lshift_func_uint16_t_u_u(((((safe_sub_func_int32_t_s_s((safe_add_func_int64_t_s_s((((safe_div_func_uint32_t_u_u(((((l_1741 = ((((((*l_1739) ^= (safe_mul_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s((l_1726 , g_185), (l_1728--))) , (safe_mul_func_int16_t_s_s((l_1733 , (safe_div_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((((g_185 , 2L) ^ 0xD47AL) & l_1738), g_245)), g_345))), g_929))), 0x9729L))) ^ (*p_29)) >= (*p_29)) | g_1601.f2) | (*p_29))) == g_34) < 0x6DD19A28L) < l_1742[4][4]), g_1598.f7)) | g_1598.f2) || 0xE70C8F533CA4E6DELL), l_1743)), g_156[5])) || l_1744) || 0x2B30F989L) ^ l_1745), 14))), g_458)))), g_345))) > g_936), 15)) | g_157[0]);
    return g_140.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_34 g_53 g_55 g_84 g_540 g_1337 g_1639 g_935 g_159 g_1661 g_223 g_1406.f0 g_246 g_193 g_1348 g_1601.f1 g_743
 * writes: g_53 g_55 g_84 g_76 g_1639 g_1214 g_935 g_1406.f0 g_246 g_193 g_1534 g_140
 */
static const uint64_t  func_47(uint32_t  p_48)
{ /* block id: 3 */
    uint32_t l_51[1];
    uint64_t *l_52 = &g_53;
    uint64_t *l_54 = (void*)0;
    const int8_t * const l_61 = &g_5;
    int32_t l_1685 = 0xFB988B00L;
    int32_t l_1686 = 1L;
    int8_t l_1687 = 1L;
    int32_t l_1688 = 0x5EB24EC5L;
    int32_t l_1689[5] = {0xD1872972L,0xD1872972L,0xD1872972L,0xD1872972L,0xD1872972L};
    int32_t l_1691 = 1L;
    int64_t l_1708 = 1L;
    uint64_t l_1709 = 0x13F1FCB2C62F156ELL;
    int i;
    for (i = 0; i < 1; i++)
        l_51[i] = 18446744073709551609UL;
    if ((((safe_unary_minus_func_uint16_t_u((((*l_52) = l_51[0]) & (g_55 = g_34)))) && func_56(func_58(p_48, l_61))) , 0x1DC3C527L))
    { /* block id: 696 */
        int32_t *l_1684[10] = {&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153};
        int32_t l_1690[6];
        uint8_t l_1692 = 0x4FL;
        union U4 **l_1701 = (void*)0;
        const uint16_t l_1702 = 0x634BL;
        uint32_t *l_1703 = &g_1534;
        struct S1 l_1706[4][5][2] = {{{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}},{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}},{{2439,18446744073709551606UL,0xB4L},{-3348,1UL,255UL}},{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}},{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}}},{{{2439,18446744073709551606UL,0xB4L},{-3348,1UL,255UL}},{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}},{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}},{{2439,18446744073709551606UL,0xB4L},{-3348,1UL,255UL}},{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}}},{{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}},{{2439,18446744073709551606UL,0xB4L},{-3348,1UL,255UL}},{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}},{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}},{{2439,18446744073709551606UL,0xB4L},{-3348,1UL,255UL}}},{{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}},{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}},{{2439,18446744073709551606UL,0xB4L},{-3348,1UL,255UL}},{{-3010,18446744073709551615UL,1UL},{-3684,18446744073709551615UL,0x1CL}},{{-3010,18446744073709551615UL,1UL},{-3348,1UL,255UL}}}};
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_1690[i] = 0x54D9B7D5L;
lbl_1704:
        l_1692++;
        if ((((*l_1703) = ((safe_mod_func_int16_t_s_s((l_1691 == p_48), (safe_mod_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_s((p_48 <= (((void*)0 == &l_1690[3]) , ((*l_52) = ((p_48 <= ((-0x10.Cp-1) != (&g_1215 != l_1701))) , p_48)))), p_48)) >= p_48), l_1702)))) , g_1601.f1)) <= g_743))
        { /* block id: 700 */
            if (g_1337)
                goto lbl_1704;
        }
        else
        { /* block id: 702 */
            int32_t *l_1705[6] = {&l_1686,&l_1686,&l_1686,&l_1686,&l_1686,&l_1686};
            struct S1 *l_1707 = &g_140;
            int i;
            l_1705[0] = (void*)0;
            (*l_1707) = l_1706[1][1][0];
        }
        l_1709++;
    }
    else
    { /* block id: 707 */
        struct S1 *l_1713[9] = {&g_1406[0],&g_140,&g_1406[0],&g_140,&g_1406[0],&g_140,&g_1406[0],&g_140,&g_1406[0]};
        struct S1 **l_1712 = &l_1713[0];
        int i;
        (*l_1712) = &g_140;
        return p_48;
    }
    return l_1691;
}


/* ------------------------------------------ */
/* 
 * reads : g_193 g_1348
 * writes: g_193
 */
static uint16_t  func_56(uint16_t  p_57)
{ /* block id: 688 */
    uint32_t l_1676[7] = {18446744073709551612UL,0x44D4EE39L,18446744073709551612UL,18446744073709551612UL,0x44D4EE39L,18446744073709551612UL,18446744073709551612UL};
    int32_t l_1677 = 1L;
    int32_t *l_1678 = &g_1348;
    int32_t *l_1679[1][6][2] = {{{&g_1417,&g_940},{&g_153,&g_246},{&g_940,&g_246},{&g_153,&g_940},{&g_1417,&g_1417},{&g_1417,&g_940}}};
    int32_t l_1680 = 0xE8F84460L;
    uint32_t l_1681 = 0xD81EF54AL;
    int i, j, k;
    for (g_193 = 0; (g_193 <= 29); ++g_193)
    { /* block id: 691 */
        return l_1676[2];
    }
    ++l_1681;
    return (*l_1678);
}


/* ------------------------------------------ */
/* 
 * reads : g_53 g_55 g_84 g_540 g_1337 g_1639 g_935 g_159 g_1661 g_223 g_1406.f0 g_246
 * writes: g_53 g_55 g_84 g_76 g_1639 g_1214 g_935 g_1406.f0 g_246
 */
static uint16_t  func_58(int64_t  p_59, const int8_t * const  p_60)
{ /* block id: 6 */
    uint32_t l_62[2][8][5] = {{{0xB9349714L,4294967295UL,0xFC007DFDL,4294967295UL,1UL},{0xBC3393A3L,0xFD6825D0L,0xDA8F9E1CL,1UL,1UL},{0xB9349714L,0xBC3393A3L,1UL,0xBC3393A3L,0xB9349714L},{0x72060300L,9UL,1UL,0xDA8F9E1CL,0xC5DE30EAL},{4294967295UL,0xBBE75688L,0xDA8F9E1CL,0xC3C737ECL,0x3FAAB93BL},{0x3FAAB93BL,0xB9349714L,0xFC007DFDL,9UL,0xC5DE30EAL},{1UL,0xC3C737ECL,0xC3C737ECL,1UL,0xB9349714L},{0xC5DE30EAL,0xC3C737ECL,0xD8E7DFBFL,4294967295UL,1UL}},{{9UL,0xB9349714L,0xC5DE30EAL,0xD6A391DEL,1UL},{0xD8E7DFBFL,0xBBE75688L,0xFD6825D0L,4294967295UL,4294967295UL},{0xBBE75688L,9UL,0xBBE75688L,1UL,0x3ECB6AEFL},{0xBBE75688L,0xBC3393A3L,0xD6A391DEL,9UL,0xDA8F9E1CL},{0xD8E7DFBFL,0xFD6825D0L,0xB9349714L,0xC3C737ECL,0x72060300L},{9UL,4294967295UL,0xD6A391DEL,0xDA8F9E1CL,0xD6A391DEL},{0xC5DE30EAL,0xC5DE30EAL,0xBBE75688L,0xBC3393A3L,0xD6A391DEL},{1UL,0x3ECB6AEFL,0xFD6825D0L,1UL,0x72060300L}}};
    int32_t l_75[3][8] = {{0xFBAA6DCEL,1L,0xFBAA6DCEL,0L,0L,0xFBAA6DCEL,1L,0xFBAA6DCEL},{(-1L),0L,(-4L),0L,(-1L),0L,0xFBAA6DCEL,1L},{0L,0L,0xFBAA6DCEL,1L,0xFBAA6DCEL,0L,0L,0xFBAA6DCEL}};
    uint64_t *l_119 = (void*)0;
    int16_t *l_1634 = &g_151;
    int16_t ** const l_1633[7][1][7] = {{{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,(void*)0,&l_1634}},{{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,(void*)0,&l_1634}},{{&l_1634,&l_1634,&l_1634,(void*)0,&l_1634,(void*)0,&l_1634}},{{&l_1634,&l_1634,&l_1634,(void*)0,&l_1634,&l_1634,&l_1634}},{{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634}},{{&l_1634,(void*)0,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634}},{{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634}}};
    int16_t **l_1636 = &l_1634;
    int16_t ***l_1635 = &l_1636;
    struct S2 **l_1638 = (void*)0;
    struct S2 ***l_1637 = &l_1638;
    int16_t ****l_1641 = &g_1639;
    uint16_t *l_1655 = (void*)0;
    uint32_t l_1656 = 0x1D62EF91L;
    union U4 **l_1657[4];
    union U4 ***l_1658 = (void*)0;
    uint64_t *l_1659[1];
    struct S1 l_1660 = {5086,0x06F00D97A570AB31LL,0x11L};
    int64_t *l_1664[2][9] = {{&g_193,&g_193,(void*)0,&g_193,&g_193,(void*)0,&g_193,&g_193,(void*)0},{&g_193,&g_193,(void*)0,&g_193,&g_193,(void*)0,&g_193,&g_193,(void*)0}};
    uint8_t l_1665 = 0xB6L;
    int32_t *l_1666 = (void*)0;
    int32_t *l_1667[9][4] = {{&g_191,&g_191,&g_153,&g_246},{&g_191,&g_34,&g_34,&g_191},{&g_1417,&g_246,&g_34,&g_34},{&g_191,&g_191,&g_153,&g_191},{&g_191,&g_34,&g_1417,&g_191},{&g_1417,&g_191,&g_34,&g_34},{&g_246,&g_246,&g_153,&g_191},{&g_246,&g_34,&g_34,&g_246},{&g_1417,&g_191,&g_1417,&g_34}};
    int64_t **l_1673[5][1] = {{&l_1664[0][8]},{&l_1664[1][7]},{&l_1664[0][8]},{&l_1664[1][7]},{&l_1664[0][8]}};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_1657[i] = &g_1215;
    for (i = 0; i < 1; i++)
        l_1659[i] = &g_223;
    for (g_53 = 0; (g_53 <= 1); g_53 += 1)
    { /* block id: 9 */
        uint64_t l_70 = 0x39D3FD5B128C85C7LL;
        int32_t l_74 = 0xED4B89D5L;
        int16_t l_78[9][4][7] = {{{1L,(-1L),1L,(-6L),0L,(-6L),1L},{1L,1L,0xD400L,1L,1L,0xD400L,1L},{0L,(-6L),1L,(-1L),1L,(-6L),0L},{0x519AL,1L,0x519AL,0x519AL,1L,0x519AL,0x519AL}},{{0L,(-1L),(-1L),(-1L),0L,0x1EB7L,0L},{1L,0x519AL,0x519AL,1L,0x519AL,0x519AL,1L},{1L,(-1L),1L,(-6L),0L,(-6L),1L},{1L,1L,0xD400L,1L,1L,0xD400L,1L}},{{0L,(-6L),1L,(-1L),1L,(-6L),0L},{0x519AL,1L,0x519AL,0x519AL,1L,0x519AL,0x519AL},{0L,(-1L),0L,(-6L),1L,(-1L),1L},{0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L,0x519AL}},{{(-1L),(-6L),(-1L),0x1EB7L,1L,0x1EB7L,(-1L)},{0x519AL,0x519AL,1L,0x519AL,0x519AL,1L,0x519AL},{1L,0x1EB7L,(-1L),(-6L),(-1L),0x1EB7L,1L},{0xD400L,0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L}},{{1L,(-6L),0L,(-6L),1L,(-1L),1L},{0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L,0x519AL},{(-1L),(-6L),(-1L),0x1EB7L,1L,0x1EB7L,(-1L)},{0x519AL,0x519AL,1L,0x519AL,0x519AL,1L,0x519AL}},{{1L,0x1EB7L,(-1L),(-6L),(-1L),0x1EB7L,1L},{0xD400L,0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L},{1L,(-6L),0L,(-6L),1L,(-1L),1L},{0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L,0x519AL}},{{(-1L),(-6L),(-1L),0x1EB7L,1L,0x1EB7L,(-1L)},{0x519AL,0x519AL,1L,0x519AL,0x519AL,1L,0x519AL},{1L,0x1EB7L,(-1L),(-6L),(-1L),0x1EB7L,1L},{0xD400L,0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L}},{{1L,(-6L),0L,(-6L),1L,(-1L),1L},{0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L,0x519AL},{(-1L),(-6L),(-1L),0x1EB7L,1L,0x1EB7L,(-1L)},{0x519AL,0x519AL,1L,0x519AL,0x519AL,1L,0x519AL}},{{1L,0x1EB7L,(-1L),(-6L),(-1L),0x1EB7L,1L},{0xD400L,0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L},{1L,(-6L),0L,(-6L),1L,(-1L),1L},{0x519AL,0xD400L,0xD400L,0x519AL,0xD400L,0xD400L,0x519AL}}};
        int32_t l_79 = 0xCDF3DC49L;
        int32_t l_81 = 0xA9F1E568L;
        int32_t l_83 = (-1L);
        int i, j, k;
        if (g_53)
            break;
        for (p_59 = 0; (p_59 <= 1); p_59 += 1)
        { /* block id: 13 */
            int32_t *l_63 = &g_34;
            int32_t *l_64 = &g_34;
            int32_t *l_65 = (void*)0;
            int32_t *l_66 = &g_34;
            int32_t *l_67 = (void*)0;
            int32_t *l_68 = &g_34;
            int32_t *l_69[3];
            uint64_t *l_120 = (void*)0;
            int i;
            for (i = 0; i < 3; i++)
                l_69[i] = (void*)0;
            ++l_70;
            for (g_55 = 0; (g_55 <= 1); g_55 += 1)
            { /* block id: 17 */
                int32_t l_73 = 0xF94618C4L;
                int32_t l_77 = 4L;
                int32_t l_80 = (-4L);
                int32_t l_82 = 0xF7FA13A8L;
                uint64_t l_1596[2][7][2] = {{{18446744073709551615UL,1UL},{0x6662F615AB1897D6LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL},{0xE377698EA3634DF9LL,18446744073709551615UL},{0x8EF263FFFDC10525LL,0x2422E90C8B3DBF1DLL},{1UL,0x2422E90C8B3DBF1DLL},{0x6662F615AB1897D6LL,1UL}},{{0x2422E90C8B3DBF1DLL,0xE377698EA3634DF9LL},{0x8EF263FFFDC10525LL,18446744073709551615UL},{1UL,0xAE57619367772839LL},{0xAE57619367772839LL,0xAE57619367772839LL},{1UL,18446744073709551615UL},{0x8EF263FFFDC10525LL,0xE377698EA3634DF9LL},{0x2422E90C8B3DBF1DLL,1UL}}};
                int32_t * const l_1626 = (void*)0;
                int32_t **l_1627 = &l_67;
                int i, j, k;
                ++g_84[0][0];
                for (l_80 = 1; (l_80 >= 0); l_80 -= 1)
                { /* block id: 21 */
                    const int8_t *l_122 = &g_123;
                    const int8_t **l_121 = &l_122;
                    int32_t *l_1541 = &g_187[0][3];
                    int32_t *l_1542[7];
                    int32_t **l_1625 = &g_226;
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_1542[i] = &g_156[0];
                }
                (*l_1627) = l_1626;
                (*g_540) = 0x0.4p-1;
            }
        }
    }
    g_1406[0].f0 &= (safe_mod_func_uint8_t_u_u(p_59, (((+(((((g_935[0][0] = (safe_add_func_uint16_t_u_u(((((g_1337 , (((l_1633[4][0][1] == ((*l_1635) = &l_1634)) > ((((*l_1637) = (void*)0) != (((((((*l_1641) = g_1639) == &g_1640) , (safe_mod_func_uint64_t_u_u((l_75[2][6] = (l_62[0][0][2] == ((g_1214[0][0] = ((((safe_mul_func_uint16_t_u_u((safe_sub_func_int16_t_s_s(((((safe_add_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u(((+(l_1656 = ((((g_935[0][0] || p_59) , (-1L)) < l_62[0][0][0]) & p_59))) , l_62[0][3][4]), 2)), p_59)), 6UL)) || p_59) ^ l_62[1][1][0]) > p_59), p_59)), 0x2872L)) && l_75[2][0]) < g_159[7][5][1]) , l_1657[0])) != (void*)0))), p_59))) < p_59) , l_1660) , g_1661)) != l_62[1][7][3])) >= l_1660.f0)) > 0x21L) || l_62[0][0][2]) == p_59), p_59))) < l_1665) | g_223) || p_59) & l_1665)) == 0x71BEF6136D6AF100LL) || 0x72L)));
    for (g_246 = (-13); (g_246 > 17); ++g_246)
    { /* block id: 682 */
        union U4 *l_1670[9] = {&g_1671,&g_1671,&g_1672,&g_1671,&g_1671,&g_1672,&g_1671,&g_1671,&g_1672};
        int i;
        (*g_540) = 0xC.093D3Fp-50;
        l_1670[1] = l_1670[1];
        l_1673[4][0] = (void*)0;
    }
    return p_59;
}


/* ------------------------------------------ */
/* 
 * reads : g_549
 * writes:
 */
static int64_t  func_93(int8_t * p_94, float  p_95, uint8_t  p_96, int8_t  p_97, uint32_t  p_98)
{ /* block id: 628 */
    const union U3 *l_1537 = &g_1538[0];
    const union U3 **l_1539 = (void*)0;
    const union U3 **l_1540 = &l_1537;
    (*l_1540) = l_1537;
    return g_549[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_540 g_140.f2 g_140 g_223 g_76 g_210 g_658 g_710 g_156 g_336 g_1107 g_84 g_310 g_153 g_139 g_946 g_743 g_34 g_844 g_549 g_222 g_158 g_248 g_1214 g_159 g_1241 g_353 g_1263 g_5 g_940 g_1305 g_939 g_151 g_1337 g_1349 g_221 g_1417 g_1406.f0 g_893 g_938 g_935 g_1406 g_135 g_191 g_929 g_1348 g_345 g_1534
 * writes: g_76 g_951.f0 g_210 g_786 g_700.f0 g_1057 g_140 g_223 g_658 g_336 g_1107 g_84 g_310 g_946 g_743 g_34 g_158 g_844 g_248 g_1241 g_1249 g_1250 g_353 g_191 g_135 g_940 g_1305 g_222 g_1337 g_1349 g_139 g_1406 g_226 g_157 g_935 g_151 g_1517 g_1518 g_345 g_1534
 */
static int8_t  func_101(int32_t  p_102, uint16_t  p_103, int32_t  p_104)
{ /* block id: 423 */
    float *l_1044 = &g_951.f0;
    uint8_t *l_1045 = (void*)0;
    uint8_t **l_1046 = &g_210;
    int32_t l_1049 = 0x1CFD7AC9L;
    int32_t l_1054 = (-2L);
    int32_t l_1055[5][2];
    float *l_1056 = &g_1057;
    int32_t l_1058[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    const uint64_t l_1096 = 0xB44F96E519818E68LL;
    int32_t l_1104 = (-1L);
    union U4 *** const l_1184[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    struct S1 l_1192 = {-5386,1UL,0xBCL};
    uint32_t l_1208[8][8] = {{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xCD8FE110L,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}};
    float **l_1227 = &g_540;
    uint8_t l_1246 = 9UL;
    uint32_t *l_1266 = &l_1208[1][1];
    uint32_t **l_1265 = &l_1266;
    uint64_t l_1288 = 18446744073709551615UL;
    int16_t *l_1289 = &g_135;
    int64_t l_1302 = 0xB3DDB1780AC41721LL;
    uint16_t *l_1342 = (void*)0;
    uint16_t ** const l_1341[8][1][10] = {{{&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,(void*)0,&l_1342}},{{(void*)0,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342}},{{&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342}},{{&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,(void*)0,&l_1342}},{{&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342}},{{(void*)0,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342}},{{&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342}},{{&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,(void*)0,&l_1342}}};
    uint16_t ** const *l_1340[4][2][6] = {{{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]},{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]}},{{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]},{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]}},{{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]},{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]}},{{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]},{&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9],&l_1341[0][0][9]}}};
    uint32_t l_1449 = 0UL;
    int8_t ***l_1513[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_1533[2][9];
    int i, j, k;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
            l_1055[i][j] = 0xC080F8B5L;
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
            l_1533[i][j] = 0x0CAEB1D4L;
    }
    if ((safe_lshift_func_uint16_t_u_u(((((*l_1056) = (safe_div_func_float_f_f((((*g_540) = (safe_div_func_float_f_f(p_102, 0x9.Cp-1))) < (0x1.Ep+1 != (safe_add_func_float_f_f(((*l_1044) = 0x0.FDBCCBp+89), ((-0x9.3p+1) > (((((l_1045 == ((*l_1046) = &g_658[1][2][0])) >= (safe_mul_func_float_f_f((l_1049 = (g_700[2][0][1].f0 = (g_786 = p_102))), (l_1055[4][1] = (((safe_sub_func_float_f_f(((safe_add_func_float_f_f(l_1054, p_104)) >= l_1054), 0x7.6p-1)) < p_103) < p_102))))) > p_102) > (-0x6.5p-1)) < g_140.f2)))))), p_104))) > p_104) , 65529UL), l_1058[4])))
    { /* block id: 432 */
        struct S1 *l_1059 = &g_140;
        uint64_t *l_1068 = (void*)0;
        uint64_t *l_1069 = &g_223;
        int16_t l_1074 = 5L;
        uint16_t * const l_1086 = &g_336;
        uint16_t * const *l_1085 = &l_1086;
        const int32_t l_1095 = (-9L);
        int32_t l_1097 = 7L;
        int32_t l_1105 = 0L;
        int32_t l_1106[6] = {0x01D875EFL,0L,0x01D875EFL,0x01D875EFL,0L,0x01D875EFL};
        uint32_t ***l_1155[2];
        int8_t ***l_1195 = (void*)0;
        const float *l_1197 = &g_1057;
        int i;
        for (i = 0; i < 2; i++)
            l_1155[i] = (void*)0;
        (*l_1059) = g_140;
        if ((safe_sub_func_uint16_t_u_u(((**l_1085) |= (safe_mod_func_uint8_t_u_u((safe_mod_func_int8_t_s_s((safe_div_func_int64_t_s_s(((p_102 > ((++(*l_1069)) <= 0x58A943C57C580FC5LL)) & (safe_rshift_func_int8_t_s_s(l_1074, (p_103 & ((safe_lshift_func_int16_t_s_s(p_103, 5)) >= ((safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_s((safe_mul_func_int16_t_s_s((((((*l_1044) = ((*l_1056) = (*g_540))) , ((void*)0 != l_1085)) >= ((safe_lshift_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u(((safe_add_func_uint8_t_u_u(((*g_210)--), (g_710[2][8][2] , 8UL))) != p_104), 0x5050D802L)) != 0x1473L), p_104)) != l_1074)) & 0x50L), g_156[0])), 10)), p_102)), 2)) == p_102)))))), p_103)), p_102)), l_1095))), l_1096)))
        { /* block id: 439 */
            return p_104;
        }
        else
        { /* block id: 441 */
            int32_t *l_1098 = &l_1097;
            int32_t *l_1099 = &l_1054;
            int32_t *l_1100 = &g_153;
            int32_t *l_1101 = (void*)0;
            int32_t *l_1102 = (void*)0;
            int32_t *l_1103[10] = {&g_34,&g_34,&g_34,&g_34,&g_34,&g_34,&g_34,&g_34,&g_34,&g_34};
            int32_t **l_1110 = &l_1103[8];
            uint32_t *l_1124[9][9][3] = {{{(void*)0,&g_84[0][0],&g_946},{&g_84[0][0],&g_946,&g_946},{&g_946,&g_946,&g_84[0][2]},{&g_946,(void*)0,&g_946},{&g_84[0][0],(void*)0,(void*)0},{(void*)0,&g_84[0][1],&g_946},{&g_84[0][3],&g_84[0][0],(void*)0},{&g_84[0][3],&g_84[0][0],&g_946},{&g_84[0][0],(void*)0,&g_84[0][2]}},{{&g_946,(void*)0,&g_946},{&g_84[0][0],&g_84[0][0],&g_946},{(void*)0,&g_84[0][0],&g_84[0][0]},{&g_946,&g_84[0][1],&g_84[0][2]},{(void*)0,(void*)0,&g_946},{&g_84[0][0],(void*)0,&g_946},{&g_946,&g_946,&g_946},{&g_84[0][0],&g_946,&g_946},{&g_84[0][3],&g_84[0][0],&g_84[0][2]}},{{&g_84[0][3],&g_84[0][1],&g_84[0][0]},{(void*)0,&g_84[0][0],&g_946},{&g_84[0][0],&g_946,&g_946},{&g_946,&g_946,&g_84[0][2]},{&g_946,(void*)0,&g_946},{&g_84[0][0],(void*)0,(void*)0},{(void*)0,&g_84[0][1],&g_946},{&g_84[0][3],&g_84[0][0],(void*)0},{&g_84[0][3],&g_84[0][0],&g_946}},{{&g_84[0][0],(void*)0,&g_84[0][2]},{&g_946,(void*)0,&g_946},{&g_84[0][0],&g_84[0][0],&g_946},{(void*)0,&g_84[0][0],&g_84[0][0]},{&g_946,&g_84[0][1],&g_84[0][2]},{(void*)0,(void*)0,&g_946},{&g_84[0][0],(void*)0,&g_946},{&g_946,&g_946,&g_946},{&g_84[0][0],&g_946,&g_946}},{{&g_84[0][3],&g_84[0][0],&g_84[0][2]},{&g_84[0][3],&g_84[0][1],&g_84[0][0]},{(void*)0,&g_84[0][0],&g_946},{&g_84[0][0],&g_946,&g_946},{&g_946,&g_946,&g_84[0][2]},{&g_946,(void*)0,&g_946},{&g_84[0][0],(void*)0,(void*)0},{&g_84[0][0],(void*)0,&g_946},{&g_84[0][4],&g_84[0][0],(void*)0}},{{&g_946,&g_946,&g_84[0][0]},{(void*)0,(void*)0,&g_946},{&g_84[0][1],(void*)0,&g_84[0][0]},{&g_84[0][4],&g_946,&g_84[0][3]},{(void*)0,&g_84[0][0],&g_84[0][3]},{&g_84[0][0],(void*)0,(void*)0},{(void*)0,(void*)0,&g_84[0][0]},{&g_84[0][4],&g_946,&g_946},{&g_84[0][1],&g_84[0][0],&g_946}},{{(void*)0,&g_84[0][0],&g_84[0][0]},{&g_946,(void*)0,(void*)0},{&g_84[0][4],&g_84[0][3],&g_84[0][3]},{&g_84[0][0],(void*)0,&g_84[0][3]},{&g_84[0][0],&g_84[0][0],&g_84[0][0]},{&g_84[0][0],&g_84[0][0],&g_946},{&g_84[0][0],&g_946,&g_84[0][0]},{&g_84[0][0],(void*)0,(void*)0},{&g_84[0][0],(void*)0,&g_946}},{{&g_84[0][4],&g_84[0][0],(void*)0},{&g_946,&g_946,&g_84[0][0]},{(void*)0,(void*)0,&g_946},{&g_84[0][1],(void*)0,&g_84[0][0]},{&g_84[0][4],&g_946,&g_84[0][3]},{(void*)0,&g_84[0][0],&g_84[0][3]},{&g_84[0][0],(void*)0,(void*)0},{(void*)0,(void*)0,&g_84[0][0]},{&g_84[0][4],&g_946,&g_946}},{{&g_84[0][1],&g_84[0][0],&g_946},{(void*)0,&g_84[0][0],&g_84[0][0]},{&g_946,(void*)0,(void*)0},{&g_84[0][4],&g_84[0][3],&g_84[0][3]},{&g_84[0][0],(void*)0,&g_84[0][3]},{&g_84[0][0],&g_84[0][0],&g_84[0][0]},{&g_84[0][0],&g_84[0][0],&g_946},{&g_84[0][0],&g_946,&g_84[0][0]},{&g_84[0][0],(void*)0,(void*)0}}};
            uint16_t *l_1138 = &g_310;
            int8_t *l_1141[7][4] = {{&g_221[1],&g_221[1],&g_221[1],&g_221[1]},{&g_221[1],&g_939[9],&g_939[9],&g_221[1]},{&g_939[9],&g_221[1],&g_939[9],&g_939[9]},{&g_221[1],&g_221[1],&g_221[1],&g_221[1]},{&g_221[1],&g_939[9],&g_939[9],&g_221[1]},{&g_939[9],&g_221[1],&g_939[9],&g_939[9]},{&g_221[1],&g_221[1],&g_221[1],&g_221[1]}};
            const uint32_t *l_1158[9];
            const uint32_t ** const l_1157 = &l_1158[2];
            const uint32_t ** const *l_1156 = &l_1157;
            const union U4 ***l_1182 = (void*)0;
            int i, j, k;
            for (i = 0; i < 9; i++)
                l_1158[i] = &g_871;
            g_1107--;
            (*l_1110) = l_1101;
            if ((safe_mod_func_int32_t_s_s((((safe_sub_func_int64_t_s_s(((((+p_102) < (((safe_add_func_uint16_t_u_u((+(safe_add_func_uint64_t_u_u((~((p_102 < (p_104 != (--g_84[0][0]))) > ((safe_lshift_func_int8_t_s_s(((g_140 , ((((~(safe_mod_func_uint32_t_u_u(((safe_div_func_int32_t_s_s((((safe_sub_func_int64_t_s_s((safe_mul_func_int8_t_s_s((l_1097 = ((p_103 != ((*l_1086) ^= l_1106[5])) == ((*l_1138)++))), (safe_mod_func_int16_t_s_s((((g_140 , (p_104 , p_103)) >= 0xFE6F57912AF4E2B9LL) != 0x2E31B869L), l_1074)))), p_104)) , l_1058[4]) > 0L), p_103)) , 0UL), p_104))) > (*l_1100)) < g_139) && g_156[0])) , p_104), 1)) >= p_104))), 0xD6D643DC78BFB720LL))), 0x2577L)) == 0x4BE20F6F8F4E8B0CLL) , g_946)) && 0x9EL) == (*l_1100)), p_102)) >= 0L) , l_1049), 0xCD8015F5L)))
            { /* block id: 448 */
                uint8_t l_1163 = 8UL;
                int32_t l_1189[1][3];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_1189[i][j] = 0x02B9EC97L;
                }
                if ((l_1096 >= (((g_946 = (((*l_1069) = p_104) == 0xBB059768F8DD9B33LL)) & 0x71707B43L) ^ ((safe_rshift_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((safe_sub_func_int16_t_s_s(p_104, (p_103 | p_103))), 10)), 6)) < 1L))))
                { /* block id: 451 */
                    uint8_t l_1188 = 254UL;
                    (*l_1098) |= (0x2731L && (safe_rshift_func_int16_t_s_u((!p_102), 1)));
                    for (g_743 = (-1); (g_743 > 20); g_743 = safe_add_func_uint64_t_u_u(g_743, 7))
                    { /* block id: 455 */
                        int8_t l_1164 = 0xADL;
                        uint32_t l_1165[5][1][4] = {{{4294967293UL,4294967295UL,4294967295UL,4294967293UL}},{{4294967295UL,4294967293UL,4294967295UL,4294967295UL}},{{4294967293UL,4294967293UL,1UL,4294967293UL}},{{4294967293UL,4294967295UL,4294967295UL,4294967293UL}},{{4294967295UL,4294967293UL,4294967295UL,4294967295UL}}};
                        const union U4 ****l_1183 = &l_1182;
                        int i, j, k;
                        g_140 = (*l_1059);
                        l_1165[0][0][2] &= (((((l_1163 = (g_140.f0 = ((l_1155[0] != l_1156) < ((g_34 < (8L >= (g_946 = (p_102 , (p_103 || (safe_sub_func_uint8_t_u_u(((*g_210)++), p_102))))))) , ((*l_1100) & ((0x1DDFEEFBL && 0x5C7CD9ABL) <= (*g_210))))))) != 0UL) < l_1164) || l_1097) ^ 4294967287UL);
                        l_1189[0][1] &= (p_104 >= (safe_sub_func_uint8_t_u_u(((safe_div_func_uint64_t_u_u(0xE4654AB0172B6735LL, ((safe_sub_func_int32_t_s_s((((safe_rshift_func_uint8_t_u_u((*g_210), 7)) , (safe_add_func_uint16_t_u_u((((safe_mod_func_uint64_t_u_u((safe_sub_func_int16_t_s_s((safe_lshift_func_int16_t_s_s(((((((*l_1183) = l_1182) == (g_844 , l_1184[3])) > (safe_add_func_int64_t_s_s(((void*)0 != &g_191), (((((!p_103) == p_102) < (*l_1100)) , 0x51F0L) > (-1L))))) , l_1165[0][0][2]) & (-10L)), 11)), g_549[2])), p_103)) , p_102) , 1UL), g_222))) , p_104), 0x3DBCB5C5L)) , 3UL))) , 0xABL), l_1188)));
                    }
                    g_34 ^= p_104;
                    for (g_158 = (-25); (g_158 == 24); g_158 = safe_add_func_uint64_t_u_u(g_158, 7))
                    { /* block id: 468 */
                        return p_103;
                    }
                }
                else
                { /* block id: 471 */
                    (*l_1059) = l_1192;
                }
                (*l_1110) = &l_1097;
            }
            else
            { /* block id: 475 */
                int8_t ****l_1196 = &l_1195;
                int32_t l_1198[10] = {0x92F9A229L,0x92F9A229L,1L,0x92F9A229L,0x92F9A229L,1L,0x92F9A229L,0x92F9A229L,1L,0x92F9A229L};
                int16_t *l_1203 = (void*)0;
                int i;
                (*l_1098) ^= (p_103 != ((((((*l_1196) = l_1195) != &g_146) >= (l_1197 != (void*)0)) , l_1198[6]) < (safe_sub_func_int8_t_s_s(((((*l_1059) = l_1192) , (safe_sub_func_int16_t_s_s((g_844 |= (-1L)), (((p_104 == 0UL) > 0x409CEF87L) && (-8L))))) || 6UL), 0x92L))));
            }
            for (g_248 = (-19); (g_248 < 52); g_248++)
            { /* block id: 483 */
                int16_t *l_1211 = &g_151;
                (*g_540) = (safe_sub_func_float_f_f((((((((*l_1056) = (0x3.Fp-1 >= l_1208[1][5])) != ((safe_div_func_float_f_f((l_1211 != l_1138), (*g_540))) >= (0xF.A3CF5Cp+68 >= ((*l_1044) = p_102)))) > p_104) == (safe_add_func_float_f_f((0x2.BCD2CDp+21 != l_1192.f1), (-0x7.3p-1)))) == p_104) , p_104), 0x5.1p+1));
            }
        }
    }
    else
    { /* block id: 489 */
        union U4 **l_1217 = (void*)0;
        union U4 **l_1218[10][3] = {{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215},{&g_1215,&g_1215,&g_1215}};
        int32_t *l_1219 = &l_1054;
        uint64_t *l_1224 = &g_1107;
        float ***l_1226[1][2];
        int32_t *l_1244 = &g_940;
        int32_t *l_1245[8] = {&g_940,&g_940,&g_940,&g_940,&g_940,&g_940,&g_940,&g_940};
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_1226[i][j] = (void*)0;
        }
        (*l_1219) ^= (g_310 == (((l_1217 = g_1214[0][0]) != l_1218[7][0]) > p_102));
        if ((safe_add_func_uint16_t_u_u((((*l_1224) = (safe_sub_func_uint16_t_u_u(p_102, g_159[7][2][1]))) && (!(*l_1219))), (&g_429 != (l_1227 = &g_540)))))
        { /* block id: 494 */
            int32_t *l_1228 = &g_34;
            int32_t *l_1229 = (void*)0;
            int32_t *l_1230 = (void*)0;
            int32_t *l_1231 = &l_1049;
            int32_t *l_1232 = &g_940;
            int32_t *l_1233 = &g_153;
            int32_t *l_1234 = (void*)0;
            int32_t *l_1235 = &g_246;
            int32_t l_1236 = 0L;
            int32_t *l_1237 = &l_1104;
            int32_t *l_1238 = &g_153;
            int32_t *l_1239 = &g_191;
            int32_t *l_1240[8][7] = {{&l_1049,&g_940,&l_1054,&g_34,&g_34,&l_1054,&g_940},{&l_1054,&g_246,&g_940,&g_246,&g_246,&l_1054,&l_1049},{&l_1054,&l_1049,&g_34,&g_940,&g_34,&l_1049,&l_1054},{&l_1049,&l_1054,&g_246,&g_246,&g_940,&g_246,&l_1054},{&g_940,&l_1054,&g_34,&g_34,&l_1054,&g_940,&l_1049},{&l_1049,&g_34,&g_246,&l_1049,&l_1236,&g_940,&g_940},{&g_246,&l_1236,&g_34,&l_1236,&g_246,&g_246,&l_1049},{&l_1049,&g_34,&g_940,&l_1054,&g_246,&l_1049,&g_246}};
            int i, j;
            ++g_1241;
        }
        else
        { /* block id: 496 */
            (*l_1219) |= p_104;
            return p_103;
        }
        --l_1246;
        g_1250 = (g_1249 = &g_540);
    }
    for (g_353 = (-2); (g_353 == 39); g_353 = safe_add_func_uint32_t_u_u(g_353, 3))
    { /* block id: 506 */
        struct S1 l_1257 = {3693,0x0BD9912FDBB56AA5LL,250UL};
        struct S1 *l_1258[2][6] = {{&l_1257,(void*)0,(void*)0,&l_1257,&l_1257,&l_1257},{&l_1257,&l_1257,&l_1257,(void*)0,(void*)0,&l_1257}};
        int32_t *l_1259 = &l_1104;
        int32_t *l_1260 = (void*)0;
        int32_t *l_1261 = (void*)0;
        int32_t *l_1262 = &g_191;
        float ***l_1269 = &l_1227;
        int i, j;
        l_1054 &= ((((safe_rshift_func_uint16_t_u_u(((((safe_rshift_func_uint8_t_u_s(p_102, 2)) & ((*l_1262) = ((*l_1259) = ((l_1192 = l_1257) , l_1257.f0)))) <= (g_1263[4] != l_1265)) < ((*g_210) = p_104)), 6)) & (safe_add_func_int16_t_s_s((p_102 , l_1096), (((*l_1269) = &g_540) != &g_429)))) , p_104) , p_103);
    }
    if (((((void*)0 != (*l_1046)) , (safe_mul_func_uint8_t_u_u((safe_add_func_int16_t_s_s(0x9A68L, ((safe_rshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_s(((*l_1289) = (safe_div_func_int16_t_s_s((safe_div_func_int8_t_s_s(((l_1246 <= 1L) , ((safe_add_func_int32_t_s_s(((safe_sub_func_int32_t_s_s((p_103 <= ((&g_710[4][6][1] != (*l_1046)) ^ (((l_1104 |= (((l_1054 = g_658[1][2][0]) & p_103) == l_1192.f0)) <= l_1208[1][5]) , 0x98FDL))), 0x2305B88AL)) || l_1288), p_103)) & 2L)), l_1208[4][7])), g_5))), 13)) ^ 0xB86CL), p_103)), l_1192.f2)) , p_104))), p_104))) > 0x7AA58AF5L))
    { /* block id: 517 */
        return p_104;
    }
    else
    { /* block id: 519 */
        int32_t *l_1290[4];
        int32_t l_1295 = 5L;
        int8_t *l_1298 = &g_157[0];
        int32_t l_1303 = (-4L);
        int32_t l_1304[9] = {0x58FC8B63L,7L,0x58FC8B63L,0x58FC8B63L,7L,0x58FC8B63L,0x58FC8B63L,7L,0x58FC8B63L};
        float l_1334 = 0xC.CF3382p-58;
        uint16_t ** const l_1345 = &l_1342;
        uint16_t ** const *l_1344[2][9] = {{(void*)0,(void*)0,&l_1345,(void*)0,&l_1345,(void*)0,&l_1345,(void*)0,(void*)0},{&l_1345,&l_1345,(void*)0,(void*)0,(void*)0,&l_1345,&l_1345,&l_1345,&l_1345}};
        int64_t *l_1368[9] = {&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938,&g_938};
        int32_t **l_1383 = &g_226;
        struct S1 l_1472 = {-5351,0xFAA454CB56914205LL,0xF2L};
        int8_t ** const *l_1514 = &g_146;
        int i, j;
        for (i = 0; i < 4; i++)
            l_1290[i] = &l_1055[4][1];
        if ((((*l_1289) = (l_1295 = (((((void*)0 != l_1290[2]) & (0xF7124E8F9D0D93AALL & ((safe_mul_func_uint8_t_u_u((l_1104 |= (safe_rshift_func_uint8_t_u_u(((**l_1046) = 1UL), 1))), ((p_104 || l_1295) >= (l_1192.f0 | 0L)))) , (safe_lshift_func_int8_t_s_s(((((((void*)0 == l_1298) < 0xD967050CDAC7A248LL) , (-2L)) < p_104) || 0UL), 2))))) , &g_210) != &g_210))) ^ l_1054))
        { /* block id: 524 */
            int32_t *l_1299 = &g_940;
            int32_t *l_1300 = &g_940;
            int32_t *l_1301[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            union U4 *l_1355 = (void*)0;
            float l_1389 = 0x0.Cp-1;
            int64_t *l_1402 = &g_935[1][0];
            struct S1 l_1407 = {-516,0x89B96A0476DCF753LL,7UL};
            float l_1408 = (-0x1.0p-1);
            float **l_1485 = &l_1044;
            int i;
            (*l_1299) &= 0xC654A569L;
            ++g_1305;
            if (p_103)
            { /* block id: 527 */
                const uint8_t l_1312 = 0x92L;
                uint8_t **l_1330 = &g_210;
                int32_t l_1333 = 0x9F422C12L;
                int32_t l_1335 = (-10L);
                int32_t l_1347 = 0x6C0D8A00L;
                union U4 *l_1356[6][5] = {{&g_1358,&g_1358,&g_1361[6],&g_1358,&g_1358},{&g_1357,&g_1360[1],&g_1357,&g_1357,&g_1360[1]},{&g_1358,(void*)0,(void*)0,&g_1358,(void*)0},{&g_1360[1],&g_1360[1],&g_1359[1],&g_1360[1],&g_1360[1]},{(void*)0,&g_1358,(void*)0,(void*)0,&g_1358},{&g_1360[1],&g_1357,&g_1357,&g_1360[1],&g_1357}};
                uint32_t l_1388 = 0x05BD7245L;
                uint16_t **l_1403[5];
                int i, j;
                for (i = 0; i < 5; i++)
                    l_1403[i] = &l_1342;
                for (g_222 = 4; (g_222 != 23); g_222 = safe_add_func_int32_t_s_s(g_222, 1))
                { /* block id: 530 */
                    uint32_t *l_1331 = &g_946;
                    uint32_t *l_1332[2][7];
                    int32_t l_1346 = (-5L);
                    union U4 *l_1362 = &g_1363;
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 7; j++)
                            l_1332[i][j] = (void*)0;
                    }
                    if ((((safe_sub_func_int16_t_s_s(g_939[9], l_1312)) <= ((((safe_mul_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((((l_1192.f0 = ((*l_1331) = (safe_mul_func_uint8_t_u_u(p_104, ((+(((safe_mod_func_int32_t_s_s(((safe_sub_func_int32_t_s_s(p_103, (safe_lshift_func_int16_t_s_s(((p_103 || p_103) , ((*l_1289) = ((((-1L) && (((safe_mod_func_uint16_t_u_u((safe_add_func_int32_t_s_s(((l_1330 == &g_210) > 0x1D7A9CB5253F0AF5LL), 0x1D9651C2L)), p_104)) , 0x1ECBL) & 0xCF89L)) , p_104) == l_1049))), 0)))) , l_1246), p_104)) < (*g_210)) == p_103)) != 0x1FDCL))))) < 0x48F84C5DL) || 0L), g_336)), p_102)) != p_102) < (*l_1299)) != g_151)) == (*g_210)))
                    { /* block id: 534 */
                        g_1337++;
                    }
                    else
                    { /* block id: 536 */
                        uint16_t ** const **l_1343[7][3][9] = {{{&l_1340[0][1][3],&l_1340[2][1][2],&l_1340[2][1][0],&l_1340[1][0][3],&l_1340[2][1][2],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[1][0][3]},{(void*)0,&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[0][1][3],(void*)0,&l_1340[1][0][3],&l_1340[1][0][1],&l_1340[3][1][4]},{&l_1340[3][0][2],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[1][0][3],(void*)0,&l_1340[1][1][3],&l_1340[1][0][3],&l_1340[0][1][0],&l_1340[1][0][3]}},{{&l_1340[3][1][2],(void*)0,&l_1340[1][0][3],&l_1340[3][1][2],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[2][0][5],&l_1340[3][0][4],&l_1340[1][0][3]},{(void*)0,&l_1340[2][0][5],&l_1340[1][0][3],&l_1340[2][0][0],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[3][1][1],&l_1340[1][0][3],&l_1340[1][0][3]},{&l_1340[3][0][4],(void*)0,&l_1340[1][0][3],&l_1340[1][0][0],&l_1340[1][0][3],&l_1340[1][1][0],&l_1340[1][0][3],&l_1340[1][0][0],&l_1340[1][0][3]}},{{&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[1][1][0],&l_1340[1][0][0],&l_1340[2][0][0],&l_1340[1][0][1],(void*)0,&l_1340[2][0][5],&l_1340[1][0][3]},{&l_1340[0][1][1],&l_1340[2][1][3],(void*)0,&l_1340[2][0][0],&l_1340[3][0][4],&l_1340[1][0][3],&l_1340[2][1][3],&l_1340[0][1][0],(void*)0},{(void*)0,&l_1340[3][1][1],&l_1340[1][1][0],&l_1340[3][1][2],&l_1340[1][0][3],(void*)0,&l_1340[3][0][4],&l_1340[3][1][1],&l_1340[1][0][3]}},{{(void*)0,&l_1340[3][1][1],&l_1340[1][0][3],&l_1340[2][0][5],&l_1340[1][0][3],(void*)0,(void*)0,&l_1340[3][1][0],&l_1340[1][1][0]},{&l_1340[3][0][4],&l_1340[2][1][3],&l_1340[1][0][3],&l_1340[1][0][1],(void*)0,&l_1340[1][1][0],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[2][1][2]},{(void*)0,&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[1][0][3],(void*)0,&l_1340[0][1][3],(void*)0,&l_1340[2][0][0],&l_1340[2][1][2]}},{{(void*)0,(void*)0,(void*)0,&l_1340[3][1][1],&l_1340[2][0][5],(void*)0,(void*)0,&l_1340[0][1][0],&l_1340[1][1][0]},{&l_1340[0][1][1],&l_1340[2][0][5],(void*)0,&l_1340[3][1][2],(void*)0,&l_1340[1][0][3],&l_1340[2][0][0],&l_1340[2][0][0],&l_1340[1][0][3]},{&l_1340[1][0][3],(void*)0,&l_1340[2][1][2],(void*)0,&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[2][0][0],&l_1340[1][0][3],(void*)0}},{{&l_1340[3][0][4],(void*)0,&l_1340[2][1][3],&l_1340[1][0][3],&l_1340[2][1][3],&l_1340[1][1][0],(void*)0,&l_1340[3][1][0],&l_1340[1][0][3]},{(void*)0,&l_1340[1][0][3],&l_1340[3][0][2],&l_1340[3][1][0],&l_1340[3][1][1],&l_1340[1][0][3],(void*)0,&l_1340[3][1][1],&l_1340[1][0][3]},{&l_1340[3][1][2],&l_1340[1][0][3],(void*)0,&l_1340[3][0][4],&l_1340[3][1][1],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[0][1][0],&l_1340[1][0][3]}},{{&l_1340[1][0][3],&l_1340[3][0][4],&l_1340[1][0][3],&l_1340[3][1][2],&l_1340[2][1][3],(void*)0,(void*)0,&l_1340[2][0][5],&l_1340[1][0][3]},{(void*)0,&l_1340[2][0][0],&l_1340[1][0][3],&l_1340[3][0][4],&l_1340[1][0][3],&l_1340[0][1][3],&l_1340[3][0][4],&l_1340[1][0][0],&l_1340[1][0][3]},{&l_1340[3][0][4],&l_1340[1][0][3],&l_1340[1][0][3],&l_1340[3][1][0],(void*)0,&l_1340[1][1][0],&l_1340[2][1][3],&l_1340[1][0][3],&l_1340[2][1][3]}}};
                        int32_t **l_1352 = &l_1300;
                        int i, j, k;
                        l_1344[0][1] = l_1340[1][0][3];
                        if (p_104)
                            break;
                        ++g_1349;
                        (*l_1352) = &l_1346;
                    }
                    l_1362 = (l_1356[2][0] = ((safe_lshift_func_uint8_t_u_s(253UL, 2)) , l_1355));
                }
                if (((p_103 , &g_935[0][0]) == l_1368[4]))
                { /* block id: 551 */
                    int64_t l_1375 = 0x93C4747CD3627776LL;
                    int32_t **l_1382 = &g_226;
                    g_140.f0 ^= (p_103 >= (((safe_mul_func_int16_t_s_s((safe_mod_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((l_1375 , (((safe_div_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((*l_1299), (safe_sub_func_int8_t_s_s(((l_1382 == (l_1303 , l_1383)) , (((safe_add_func_float_f_f(((((**l_1227) = ((safe_add_func_float_f_f(p_103, ((-0x9.2p+1) <= p_102))) < l_1192.f1)) != 0xA.777793p+23) < l_1049), 0x7.B282D4p+63)) == l_1295) , 0x5CL)), l_1096)))), p_104)) , l_1058[4]) || p_103)) , (*g_210)), 1UL)), l_1288)), l_1388)) == g_139) || g_221[2]));
                    return p_103;
                }
                else
                { /* block id: 555 */
                    uint16_t **l_1394[10] = {&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342,&l_1342};
                    int i;
                    for (l_1192.f1 = (-11); (l_1192.f1 >= 53); l_1192.f1 = safe_add_func_int64_t_s_s(l_1192.f1, 4))
                    { /* block id: 558 */
                        uint16_t ***l_1395 = &l_1394[4];
                        uint8_t *l_1399 = &g_139;
                        struct S1 *l_1404 = &g_140;
                        struct S1 *l_1405 = &g_1406[0];
                        if (p_104)
                            break;
                        (*l_1405) = ((*l_1404) = ((safe_sub_func_uint8_t_u_u(((*g_210) = (*g_210)), (((*l_1395) = l_1394[6]) != (((255UL & p_104) | (safe_lshift_func_uint8_t_u_u((~((((*l_1399)--) | ((((void*)0 != l_1402) || (0xB8FDL & p_104)) & 0x45A0C957AD139432LL)) && p_103)), l_1335))) , l_1403[3])))) , l_1192));
                        return p_102;
                    }
                    return p_104;
                }
            }
            else
            { /* block id: 569 */
                int16_t l_1409 = 0L;
                int32_t l_1447 = 0L;
                int8_t ** const **l_1515 = (void*)0;
                int8_t ** const **l_1516 = &l_1514;
lbl_1410:
                g_1406[0] = l_1407;
                for (l_1303 = 0; (l_1303 >= 0); l_1303 -= 1)
                { /* block id: 573 */
                    float l_1425 = 0x9.743AE6p+63;
                    int32_t l_1448 = 0L;
                    uint8_t *l_1475[4][9][7] = {{{(void*)0,&g_140.f2,&g_549[1],(void*)0,&g_159[7][5][1],&g_549[1],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_458,&g_1406[0].f2},{(void*)0,&g_159[7][5][1],&g_159[3][5][0],&g_159[7][5][1],&l_1192.f2,&l_1246,&g_159[7][5][1]},{&l_1472.f2,&g_458,(void*)0,&g_549[2],(void*)0,&g_159[3][5][0],&g_549[1]},{&l_1246,&g_159[7][5][1],&l_1407.f2,&g_1406[0].f2,&g_549[2],&g_1406[0].f2,&l_1407.f2},{&g_658[0][7][0],&g_658[0][7][0],(void*)0,&g_658[2][5][0],(void*)0,(void*)0,&g_1406[0].f2},{&l_1472.f2,&g_942,&g_248,&g_942,&g_1305,&l_1407.f2,&g_55},{&g_1305,(void*)0,&g_658[1][2][0],(void*)0,(void*)0,(void*)0,&l_1246},{&l_1246,&l_1246,&l_1472.f2,(void*)0,&g_549[2],(void*)0,(void*)0}},{{&l_1407.f2,&l_1407.f2,&g_549[2],&g_658[0][7][0],(void*)0,(void*)0,(void*)0},{&l_1407.f2,(void*)0,&g_159[0][0][2],(void*)0,&l_1192.f2,&g_1305,&g_139},{&g_1305,&g_140.f2,(void*)0,&g_1406[0].f2,&l_1472.f2,&l_1246,&g_658[2][5][0]},{&g_1305,&l_1246,&l_1407.f2,&l_1192.f2,&g_1406[0].f2,&l_1472.f2,&g_1305},{&g_159[0][0][2],&g_159[7][5][1],&l_1407.f2,&g_159[0][0][2],&g_140.f2,(void*)0,&l_1472.f2},{&g_1406[0].f2,&g_658[2][5][0],(void*)0,&g_159[7][5][1],&l_1246,&g_658[3][2][0],&g_159[7][5][1]},{&g_140.f2,&l_1407.f2,&g_159[0][0][2],&l_1472.f2,&l_1246,&g_1305,&g_549[2]},{&g_248,&g_55,&g_549[2],&g_1305,(void*)0,&g_1406[0].f2,&g_1406[0].f2},{&g_658[0][6][0],&l_1472.f2,&l_1472.f2,&l_1472.f2,&g_658[0][6][0],(void*)0,&g_658[2][5][0]}},{{&g_159[7][5][1],&g_549[2],&g_658[1][2][0],&l_1472.f2,(void*)0,&g_458,&g_942},{(void*)0,&g_658[0][6][0],&g_248,&l_1407.f2,&g_658[0][7][0],&l_1246,(void*)0},{&g_159[7][5][1],&l_1472.f2,(void*)0,&g_549[1],&g_1406[0].f2,&l_1246,(void*)0},{&g_658[0][6][0],&g_159[7][5][1],&l_1407.f2,&l_1192.f2,&g_159[7][5][1],(void*)0,&g_658[0][7][0]},{&g_248,&g_942,(void*)0,&g_159[2][4][0],&g_658[0][7][0],(void*)0,(void*)0},{&g_140.f2,&g_458,(void*)0,&g_55,(void*)0,(void*)0,&g_1406[0].f2},{&g_1406[0].f2,&g_139,&g_942,&g_658[0][7][0],(void*)0,&g_159[7][5][1],&l_1192.f2},{&g_159[0][0][2],&g_1406[0].f2,(void*)0,&g_248,(void*)0,&g_159[7][5][1],&g_159[0][0][2]},{&g_1305,&l_1472.f2,&l_1407.f2,(void*)0,&g_658[0][7][0],(void*)0,&g_159[7][5][1]}},{{&g_1305,(void*)0,(void*)0,&g_942,&g_458,(void*)0,&l_1472.f2},{&l_1407.f2,&l_1407.f2,&g_248,&g_458,&g_140.f2,(void*)0,&g_1305},{&l_1407.f2,&l_1472.f2,&l_1246,&g_1305,&g_458,&g_458,(void*)0},{&l_1407.f2,(void*)0,&g_458,(void*)0,&l_1246,&g_458,&g_658[1][2][0]},{&g_248,&g_1305,&l_1246,&l_1407.f2,&g_1305,&g_942,&g_55},{(void*)0,&g_942,(void*)0,(void*)0,&g_549[2],&l_1472.f2,(void*)0},{(void*)0,&g_55,(void*)0,&g_458,(void*)0,&l_1246,&g_1305},{&g_458,(void*)0,&g_248,(void*)0,&g_458,&l_1246,(void*)0},{(void*)0,&g_159[7][5][1],&g_140.f2,&l_1407.f2,(void*)0,&g_1305,&g_549[2]}}};
                    uint32_t *l_1476 = &l_1449;
                    int i, j, k;
                    if ((l_1192.f0 &= (p_102 , ((*l_1299) &= l_1409))))
                    { /* block id: 576 */
                        (*l_1383) = &g_1348;
                    }
                    else
                    { /* block id: 578 */
                        float l_1424 = (-0x7.Cp+1);
                        int32_t **l_1445 = (void*)0;
                        uint64_t *l_1446 = (void*)0;
                        struct S1 *l_1450 = &g_140;
                        if (g_34)
                            goto lbl_1410;
                        (*l_1044) = (((safe_add_func_float_f_f(((l_1409 < ((safe_mul_func_float_f_f((((**l_1227) = (safe_add_func_float_f_f(((g_1417 , (safe_add_func_float_f_f((safe_add_func_float_f_f(((safe_rshift_func_int16_t_s_u(p_102, 12)) , (safe_div_func_float_f_f((((safe_rshift_func_uint16_t_u_s(((p_103 >= ((*l_1402) |= (safe_mul_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u((g_1406[0].f0 == (((l_1447 = (~(safe_lshift_func_uint8_t_u_u((0x55296EADL ^ (safe_div_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(((void*)0 == g_893), (((*l_1298) = (l_1445 != &l_1301[3])) && 0xF9L))), g_938)), p_103))), 5)))) < l_1448) > 0x9DL)), p_102)), p_102)))) | 0x26E9L), 1)) , p_104) <= p_104), p_103))), 0x1.7CB439p-51)), l_1409))) < p_104), l_1054))) > p_103), 0x1.0p-1)) != l_1448)) >= l_1449), p_104)) > (*l_1300)) <= 0x7.6534A9p+91);
                        (*l_1450) = g_1406[1];
                    }
                    for (l_1295 = 0; (l_1295 <= 0); l_1295 += 1)
                    { /* block id: 589 */
                        uint32_t *l_1453 = &g_743;
                        int i, j;
                        (*l_1383) = (((void*)0 == &g_935[l_1303][l_1295]) , (((*l_1453) &= (--(**l_1265))) , l_1301[0]));
                        (*l_1383) = (((l_1298 != ((p_104 , ((safe_add_func_int16_t_s_s(l_1055[4][1], 65526UL)) , (safe_sub_func_uint8_t_u_u(9UL, ((safe_sub_func_uint64_t_u_u((safe_div_func_uint32_t_u_u((((+0x5CF9394F5F6A3EF9LL) & (safe_rshift_func_int16_t_s_s((g_151 &= ((*l_1289) ^= (((safe_sub_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(0xDEL, p_104)), (+(safe_rshift_func_int16_t_s_s((l_1472 , (-4L)), 3))))) > l_1304[7]) != p_104))), g_940))) <= l_1448), p_103)), 5L)) , 0L))))) , (void*)0)) & p_104) , (void*)0);
                    }
                    (*l_1300) &= (safe_add_func_int64_t_s_s(((l_1475[2][6][3] == ((*l_1046) = &g_139)) , (((*l_1476) = (0UL >= l_1104)) && g_84[0][0])), 0x3C6CB20A0FD89C94LL));
                }
                (*l_1300) |= (safe_mod_func_int64_t_s_s((safe_div_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s(((**l_1046) = (safe_mul_func_int8_t_s_s((l_1485 == (void*)0), (p_102 & ((safe_unary_minus_func_uint8_t_u((((*l_1402) &= ((((**l_1227) = p_103) == (safe_mul_func_float_f_f((safe_mul_func_float_f_f((0x9.1p-1 == (l_1192.f1 > (g_140 , 0xC.56C111p+49))), (0xD.87392Cp+82 == 0x9.8ECDD5p+93))), l_1049))) , g_939[9])) > g_191))) , p_104))))), p_103)), 0x0C233818L)), 1UL));
                if ((safe_div_func_int32_t_s_s((safe_mul_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(((p_103 ^ 4UL) , p_102), (g_135 = ((safe_sub_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(0x2CD58729L, ((safe_mod_func_uint16_t_u_u(((g_929 & (((l_1513[4] == ((*l_1516) = l_1514)) && ((g_1518 = (g_1517 = l_1301[0])) == &g_345)) == 0x16L)) < p_104), 0x3B79L)) || g_1348))), p_102)) && 0UL), g_135)), l_1409)), (-1L))), (-10L))) , p_103)))), l_1058[5])), 1UL)))
                { /* block id: 609 */
                    (*l_1300) = l_1058[2];
                    return p_102;
                }
                else
                { /* block id: 612 */
                    for (g_345 = 0; (g_345 <= 2); g_345 += 1)
                    { /* block id: 615 */
                        (*l_1299) = p_103;
                    }
                    return p_103;
                }
            }
        }
        else
        { /* block id: 621 */
            int32_t *l_1519 = &l_1303;
            int32_t *l_1520 = &g_34;
            int32_t *l_1521 = &g_153;
            int32_t *l_1522 = &l_1295;
            int32_t *l_1523 = &g_191;
            int32_t *l_1524 = (void*)0;
            int32_t *l_1525 = &g_191;
            int32_t *l_1526 = &l_1049;
            int32_t *l_1527 = &l_1104;
            int32_t *l_1528 = &g_246;
            int32_t *l_1529 = &l_1295;
            int32_t *l_1530 = (void*)0;
            int32_t *l_1531 = (void*)0;
            int32_t *l_1532[10] = {(void*)0,&g_940,&l_1054,&g_940,(void*)0,(void*)0,&g_940,&l_1054,&g_940,(void*)0};
            int i;
            ++g_1534;
            (*l_1519) = l_1472.f1;
        }
    }
    return p_104;
}


/* ------------------------------------------ */
/* 
 * reads : g_345 g_939
 * writes: g_345 g_226
 */
static uint32_t  func_107(int16_t  p_108, uint64_t * p_109, int64_t  p_110, uint32_t  p_111, uint64_t * p_112)
{ /* block id: 415 */
    int8_t l_1032 = (-4L);
    for (g_345 = 23; (g_345 <= 40); g_345++)
    { /* block id: 418 */
        int32_t * const l_1033 = &g_153;
        int32_t **l_1034 = (void*)0;
        int32_t **l_1035 = &g_226;
        if (l_1032)
            break;
        (*l_1035) = l_1033;
    }
    return g_939[3];
}


/* ------------------------------------------ */
/* 
 * reads : g_136 g_135 g_140 g_84 g_34 g_159 g_139 g_153 g_158 g_187 g_191 g_186 g_223 g_222 g_156 g_210 g_151 g_248 g_246 g_221 g_146 g_5 g_157 g_310 g_76 g_345 g_353 g_336 g_447 g_458 g_185 g_53 g_540 g_549 g_429 g_430 g_658 g_661 g_681
 * writes: g_136 g_139 g_146 g_84 g_140.f1 g_159 g_34 g_135 g_151 g_206 g_210 g_223 g_226 g_248 g_140.f0 g_310 g_76 g_336 g_345 g_353 g_157 g_156 g_222 g_140.f2 g_429 g_193 g_153 g_458 g_185 g_158 g_186 g_140
 */
static int16_t  func_115(uint64_t * p_116, int32_t  p_117, uint16_t  p_118)
{ /* block id: 24 */
    int32_t *l_124 = &g_34;
    float l_125 = (-0x1.8p-1);
    int32_t *l_126 = &g_34;
    int32_t *l_127 = &g_34;
    int32_t *l_128 = &g_34;
    int32_t *l_129 = &g_34;
    int32_t *l_130 = (void*)0;
    int32_t l_131[2];
    int32_t *l_132 = &g_34;
    int32_t *l_133 = &l_131[0];
    int32_t *l_134[6][6] = {{&l_131[0],&l_131[0],&l_131[1],&l_131[0],&l_131[0],&l_131[0]},{(void*)0,&l_131[0],&l_131[0],(void*)0,&g_34,(void*)0},{(void*)0,&g_34,(void*)0,&l_131[0],&l_131[0],(void*)0},{&l_131[0],&l_131[0],&l_131[0],&l_131[1],&l_131[0],&l_131[0]},{&l_131[0],&g_34,&l_131[1],&l_131[1],&g_34,&l_131[0]},{&l_131[0],&l_131[0],&l_131[1],&l_131[0],&l_131[0],&l_131[0]}};
    int8_t *l_145 = &g_5;
    int8_t **l_144 = &l_145;
    uint32_t *l_148 = (void*)0;
    uint32_t *l_149 = &g_84[0][0];
    uint16_t l_196 = 0xDD30L;
    struct S1 l_205 = {-5247,0UL,2UL};
    int64_t l_363 = 9L;
    uint8_t l_377[6] = {0x17L,0x17L,0x17L,0x17L,0x17L,0x17L};
    float *l_497[2];
    int8_t *l_558[2];
    uint8_t l_563[6];
    int16_t *l_565[5][10][5] = {{{&g_151,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_151,&g_135,&g_151,&g_151},{&g_151,(void*)0,&g_135,&g_151,&g_135},{&g_135,&g_151,&g_135,&g_151,&g_135},{&g_151,&g_151,(void*)0,&g_135,&g_135},{(void*)0,&g_151,&g_151,&g_151,&g_135},{&g_151,&g_135,&g_135,&g_151,&g_135},{&g_135,&g_151,&g_151,&g_135,&g_135},{&g_135,&g_151,&g_135,&g_135,&g_135},{&g_151,&g_151,&g_151,&g_135,&g_151}},{{&g_135,(void*)0,&g_135,&g_151,&g_135},{&g_151,&g_151,(void*)0,&g_151,(void*)0},{&g_135,&g_135,&g_135,&g_135,&g_151},{&g_151,&g_151,&g_151,&g_151,(void*)0},{&g_151,&g_135,&g_135,&g_151,&g_135},{(void*)0,&g_151,&g_151,&g_151,&g_135},{&g_151,&g_135,&g_135,&g_135,&g_135},{(void*)0,&g_151,&g_151,&g_151,(void*)0},{&g_151,(void*)0,(void*)0,&g_135,&g_151},{&g_135,&g_135,(void*)0,&g_135,&g_135}},{{&g_151,&g_135,&g_135,&g_135,&g_151},{&g_151,&g_135,&g_151,&g_151,&g_135},{&g_151,&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_151,(void*)0},{&g_151,&g_151,(void*)0,&g_135,&g_151},{(void*)0,&g_151,&g_135,&g_135,&g_135},{&g_151,&g_151,(void*)0,&g_151,&g_151},{&g_151,&g_151,&g_135,&g_151,&g_135},{&g_151,&g_151,&g_135,&g_135,&g_151},{&g_135,&g_151,&g_151,&g_151,(void*)0}},{{&g_135,&g_151,&g_135,&g_135,&g_135},{&g_151,&g_151,(void*)0,&g_151,&g_151},{&g_135,&g_151,&g_151,&g_135,&g_151},{&g_135,&g_135,(void*)0,&g_135,&g_151},{&g_151,&g_135,&g_135,&g_151,&g_151},{&g_151,&g_135,&g_135,&g_151,&g_151},{&g_151,&g_135,&g_135,(void*)0,&g_135},{(void*)0,&g_135,(void*)0,&g_151,(void*)0},{&g_151,&g_135,(void*)0,&g_151,&g_151},{&g_135,&g_151,&g_135,&g_135,&g_135}},{{&g_151,&g_151,(void*)0,&g_135,&g_151},{&g_151,&g_151,(void*)0,&g_151,&g_135},{&g_151,&g_135,&g_135,&g_135,&g_151},{&g_135,&g_151,&g_135,&g_151,(void*)0},{&g_135,&g_151,&g_135,&g_135,&g_135},{&g_135,&g_151,(void*)0,&g_151,&g_135},{&g_135,&g_135,&g_151,&g_151,&g_151},{&g_135,&g_135,(void*)0,&g_135,&g_135},{&g_151,&g_135,&g_135,&g_135,&g_151},{&g_151,&g_135,&g_151,&g_151,&g_135}}};
    int16_t **l_564 = &l_565[0][0][0];
    int8_t l_640 = 0xF9L;
    float l_838 = (-0x5.7p-1);
    float l_932 = (-0x3.Bp+1);
    int8_t l_1020 = 0x33L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_131[i] = 6L;
    for (i = 0; i < 2; i++)
        l_497[i] = &g_76;
    for (i = 0; i < 2; i++)
        l_558[i] = (void*)0;
    for (i = 0; i < 6; i++)
        l_563[i] = 0xBEL;
    g_136++;
    if (((g_135 != ((g_139 = ((void*)0 == p_116)) ^ (((p_118 < (((g_140 , (!(safe_mod_func_int64_t_s_s(((g_146 = l_144) == (g_140 , &g_147)), (((*l_149) &= g_136) , g_34))))) <= (*l_129)) >= 65527UL)) ^ 0x78B87F18L) & (*l_132)))) >= (*l_127)))
    { /* block id: 29 */
        int32_t l_183 = 0L;
        float l_184 = 0x3.769B55p+64;
        int32_t l_188 = 0xCFB8BCE6L;
        int32_t l_189 = 0x27E3E6D3L;
        int32_t l_190 = 1L;
        int32_t l_192 = (-9L);
        int32_t l_194 = 0x3E7CFBBAL;
        int32_t l_195[8] = {(-1L),(-1L),0x106F47B0L,(-1L),(-1L),0x106F47B0L,(-1L),(-1L)};
        uint64_t *l_200 = &g_140.f1;
        int16_t *l_201 = &g_151;
        int16_t *l_204 = &g_135;
        uint8_t *l_209 = &g_140.f2;
        uint32_t l_294 = 0xED0F16C2L;
        float *l_309 = &l_184;
        uint64_t l_420[3];
        const int64_t l_422 = 0x917285DC7D5EA36CLL;
        float l_441 = (-0x1.2p+1);
        float l_456 = 0xA.5BE0ADp+45;
        const int8_t l_493 = 0x87L;
        float l_505 = 0x7.F17F1Cp+79;
        struct S1 l_546 = {2729,18446744073709551607UL,255UL};
        struct S1 l_548 = {2725,18446744073709551615UL,2UL};
        int i;
        for (i = 0; i < 3; i++)
            l_420[i] = 0x52D05FB766F39B91LL;
        for (g_140.f1 = 0; (g_140.f1 <= 0); g_140.f1 += 1)
        { /* block id: 32 */
            int8_t l_150 = 0x0FL;
            int32_t l_152 = (-1L);
            int32_t l_154 = 6L;
            int32_t l_155 = (-1L);
            ++g_159[7][5][1];
            for (p_118 = 0; (p_118 <= 0); p_118 += 1)
            { /* block id: 36 */
                uint8_t *l_179 = &g_159[2][4][0];
                uint8_t *l_180 = &g_139;
                float *l_181[5][6] = {{&l_125,&g_76,&g_76,&l_125,&g_76,&g_76},{&g_76,&l_125,&g_76,&l_125,&l_125,&g_76},{&g_76,&g_76,&l_125,&l_125,&l_125,&l_125},{&l_125,&g_76,&l_125,&g_76,&l_125,&l_125},{&g_76,&l_125,&l_125,&l_125,&l_125,&g_76}};
                int16_t *l_182[6];
                int i, j;
                for (i = 0; i < 6; i++)
                    l_182[i] = &g_135;
                (*l_133) |= (safe_div_func_int64_t_s_s(((((g_84[g_140.f1][p_118] < 0L) || (safe_sub_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((~(g_135 = ((((*l_129) = (safe_add_func_float_f_f((((*l_180) = ((*l_179) = ((p_118 || (g_139 < ((((void*)0 == &g_153) > 4294967295UL) < ((g_153 , 6UL) , g_84[g_140.f1][p_118])))) >= l_154))) , l_155), (-0x5.3p-1)))) , 0xD65C57B0L) & g_158))) & 1L), g_84[g_140.f1][p_118])), 4)), g_140.f1)), l_183))) <= 0UL) && l_183), 1L));
            }
        }
        (*l_132) &= p_117;
        l_196--;
        if (((((~l_188) ^ ((l_200 == (g_206[3][5] = (((g_187[0][3] != ((*l_201) = (p_117 == 0xF50AE741L))) | (((safe_mul_func_int16_t_s_s(l_190, ((((*l_204) = 0x22D0L) , (l_205 = l_205)) , ((*l_204) &= (((18446744073709551615UL > 0x5185A720D68F9EE3LL) >= (-9L)) , g_136))))) , 0x53397D4F8AA34D09LL) <= 1UL)) , &g_53))) , g_191)) > p_117) != (-1L)))
        { /* block id: 51 */
            uint8_t *l_208 = &g_139;
            uint8_t **l_207 = &l_208;
            int32_t l_216 = 0x3FE0BE34L;
            int32_t l_218[1][10] = {{0L,0x0B6EC3E8L,6L,0x0B6EC3E8L,0L,0L,0x0B6EC3E8L,6L,0x0B6EC3E8L,0L}};
            float *l_231 = &l_184;
            uint64_t l_281 = 18446744073709551614UL;
            struct S1 l_342 = {5339,1UL,0xB7L};
            int32_t l_351 = 0xF9AAEEBDL;
            int32_t l_451 = 0L;
            float *l_496 = (void*)0;
            int i, j;
            if ((((*l_207) = l_145) != (g_210 = l_209)))
            { /* block id: 54 */
                int8_t l_214 = 0L;
                int32_t l_215 = 0xEB59E71AL;
                int32_t l_220 = 0xEC2FFA6EL;
                int32_t * const l_293 = &l_195[0];
                uint32_t l_378[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
                uint16_t l_386 = 8UL;
                int i;
                for (g_140.f1 = 0; (g_140.f1 >= 35); g_140.f1 = safe_add_func_uint8_t_u_u(g_140.f1, 1))
                { /* block id: 57 */
                    int8_t l_217 = (-8L);
                    int32_t l_219 = 8L;
                    uint8_t **l_244[3][10][2] = {{{(void*)0,&l_209},{&l_208,&l_208},{(void*)0,(void*)0},{(void*)0,&l_209},{&l_208,(void*)0},{&l_209,&l_208},{&l_209,&l_209},{&l_209,&l_208},{&l_209,(void*)0},{&l_208,&l_209}},{{(void*)0,(void*)0},{(void*)0,&l_208},{&l_208,&l_209},{(void*)0,&l_209},{&l_208,&l_208},{(void*)0,(void*)0},{(void*)0,&l_209},{&l_208,(void*)0},{&l_209,&l_208},{&l_209,&l_209}},{{&l_209,&l_208},{&l_209,(void*)0},{&l_208,&l_209},{(void*)0,(void*)0},{(void*)0,&l_208},{&l_208,&l_209},{(void*)0,&l_209},{&l_208,&l_208},{(void*)0,(void*)0},{(void*)0,&l_209}}};
                    int i, j, k;
                    for (g_136 = 1; (g_136 <= 6); g_136 += 1)
                    { /* block id: 60 */
                        uint64_t l_213 = 0x0DE5B823450267AALL;
                        if (g_191)
                            break;
                        (*l_127) ^= g_186;
                        if (l_213)
                            continue;
                    }
                    --g_223;
                    g_226 = (void*)0;
                    if (p_117)
                    { /* block id: 67 */
                        int16_t l_240 = 1L;
                        uint8_t *l_241[10];
                        int32_t l_247 = 0x6D356F01L;
                        int i;
                        for (i = 0; i < 10; i++)
                            l_241[i] = &g_139;
                        (*l_129) = (safe_sub_func_uint16_t_u_u(l_219, (safe_rshift_func_int8_t_s_u(((((void*)0 == l_231) && (safe_sub_func_int16_t_s_s(((((l_220 < (((void*)0 != &p_117) , (*l_128))) < ((safe_rshift_func_uint16_t_u_s((((((!(p_117 || (safe_lshift_func_int8_t_s_u(((safe_unary_minus_func_uint32_t_u(g_222)) ^ p_117), p_117)))) >= g_156[0]) & (*g_210)) || 0L) , l_219), g_156[0])) | l_218[0][5])) | l_217) & p_118), g_151))) >= 0xF2L), 3))));
                        if (p_117)
                            continue;
                        (*l_132) = (((*g_210) ^ (g_159[0][4][2]--)) | ((l_244[1][5][0] = &l_208) == &g_210));
                        --g_248;
                    }
                    else
                    { /* block id: 74 */
                        int64_t *l_263[2];
                        int32_t l_264 = 4L;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_263[i] = (void*)0;
                        l_264 = (1L < (safe_rshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(((g_140.f0 = (((safe_rshift_func_int16_t_s_s(0x573AL, 15)) > (((((p_118 < (((0UL || ((((*l_132) = ((((void*)0 != l_145) | (p_117 , (safe_mod_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(0x5FL, (((*l_204) = l_218[0][2]) | p_117))) <= g_246), p_118)), 0x4FL)))) == 0x9E15L)) & l_218[0][4]) , p_118)) && (-1L)) >= l_215)) | g_221[2]) > p_118) , (void*)0) != l_209)) > l_219)) != l_264), (*g_210))), 14)));
                    }
                }
                for (g_140.f1 = 0; (g_140.f1 < 15); g_140.f1 = safe_add_func_uint16_t_u_u(g_140.f1, 7))
                { /* block id: 83 */
                    uint64_t l_269 = 18446744073709551614UL;
                    uint32_t l_295[2][8][9] = {{{0x54766698L,0x3A090F8DL,0x949D9212L,0UL,5UL,0x54766698L,0x54766698L,5UL,0UL},{18446744073709551615UL,0x77D0F535L,18446744073709551615UL,0x46107686L,0x6BE9DE3BL,18446744073709551615UL,1UL,0x66B7A651L,0x46107686L},{0x5CEDE9DFL,5UL,0x949D9212L,0x5CEDE9DFL,0x52DBE1E3L,0x5CEDE9DFL,0x949D9212L,5UL,0x5CEDE9DFL},{1UL,0x6BE9DE3BL,18446744073709551615UL,0x46107686L,18446744073709551614UL,1UL,0x46107686L,0x08E61A97L,0x46107686L},{0x949D9212L,0x52DBE1E3L,0UL,0UL,0x52DBE1E3L,0x949D9212L,18446744073709551608UL,0x7BE0B4FAL,0UL},{1UL,18446744073709551614UL,0x46107686L,18446744073709551615UL,0x6BE9DE3BL,1UL,1UL,0x6BE9DE3BL,18446744073709551615UL},{0x5CEDE9DFL,0x52DBE1E3L,0x5CEDE9DFL,0x949D9212L,5UL,0x5CEDE9DFL,18446744073709551608UL,18446744073709551615UL,0x949D9212L},{18446744073709551615UL,0x6BE9DE3BL,0x46107686L,18446744073709551615UL,0x77D0F535L,18446744073709551615UL,0x46107686L,0x6BE9DE3BL,18446744073709551615UL}},{{0x54766698L,5UL,0UL,0x949D9212L,0x3A090F8DL,0x54766698L,0x949D9212L,0x7BE0B4FAL,0x949D9212L},{0x46107686L,0x77D0F535L,18446744073709551615UL,18446744073709551615UL,0x77D0F535L,0x46107686L,1UL,0x08E61A97L,18446744073709551615UL},{0x54766698L,0x3A090F8DL,0x949D9212L,0UL,5UL,0x54766698L,0x54766698L,5UL,0UL},{18446744073709551615UL,0x77D0F535L,18446744073709551615UL,0x46107686L,0x6BE9DE3BL,18446744073709551615UL,1UL,0x66B7A651L,0x46107686L},{0x5CEDE9DFL,0x5CEDE9DFL,0xF70EDA4BL,0UL,0x54766698L,0UL,0xF70EDA4BL,0x5CEDE9DFL,0UL},{0UL,18446744073709551615UL,18446744073709551615UL,0x06D691EEL,0x46107686L,0UL,0x06D691EEL,1UL,0x06D691EEL},{0xF70EDA4BL,0x54766698L,0x819C18BAL,0x819C18BAL,0x54766698L,0xF70EDA4BL,0x2D051CAAL,18446744073709551608UL,0x819C18BAL},{0UL,0x46107686L,0x06D691EEL,18446744073709551615UL,18446744073709551615UL,0UL,0UL,18446744073709551615UL,18446744073709551615UL}}};
                    uint8_t *l_298[8][7][2] = {{{&g_159[4][2][0],&g_55},{&g_248,&l_205.f2},{&l_205.f2,&g_159[7][5][1]},{&g_55,&l_205.f2},{&l_205.f2,&l_205.f2},{&g_55,&g_159[7][5][1]},{&l_205.f2,&l_205.f2}},{{&g_248,&g_55},{&g_159[4][2][0],&g_248},{(void*)0,(void*)0},{(void*)0,&g_248},{&g_159[4][2][0],&g_55},{&g_248,&l_205.f2},{&l_205.f2,&g_159[7][5][1]}},{{&g_55,&l_205.f2},{&l_205.f2,&l_205.f2},{&g_55,&g_159[7][5][1]},{&l_205.f2,&l_205.f2},{&g_248,&g_55},{&g_159[4][2][0],&g_248},{(void*)0,(void*)0}},{{(void*)0,&g_248},{&g_159[4][2][0],&g_55},{&g_248,&l_205.f2},{&l_205.f2,&g_159[7][5][1]},{&g_55,&l_205.f2},{&l_205.f2,&l_205.f2},{&g_55,&g_159[7][5][1]}},{{&l_205.f2,&l_205.f2},{&g_248,&g_55},{&g_159[4][2][0],&g_248},{(void*)0,(void*)0},{(void*)0,&g_248},{&g_159[4][2][0],&g_55},{&g_248,&l_205.f2}},{{&l_205.f2,&g_159[7][5][1]},{&g_55,&l_205.f2},{&l_205.f2,&l_205.f2},{&g_55,&g_159[7][5][1]},{&l_205.f2,&l_205.f2},{&g_248,&g_55},{&g_159[4][2][0],&g_248}},{{(void*)0,(void*)0},{(void*)0,&g_248},{&g_159[4][2][0],&g_55},{&g_248,&l_205.f2},{&l_205.f2,&g_159[7][5][1]},{&g_55,&l_205.f2},{&l_205.f2,&l_205.f2}},{{&g_55,&g_159[7][5][1]},{&l_205.f2,&l_205.f2},{&g_248,&g_55},{&g_159[4][2][0],&g_248},{(void*)0,(void*)0},{(void*)0,&g_248},{&g_159[4][2][0],&g_55}}};
                    int i, j, k;
                    for (p_117 = 26; (p_117 < (-2)); p_117 = safe_sub_func_int16_t_s_s(p_117, 3))
                    { /* block id: 86 */
                        int32_t **l_272 = &l_134[1][3];
                        l_269++;
                        (*l_272) = &l_131[0];
                        (*l_272) = (void*)0;
                        l_216 &= ((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(((safe_mul_func_int8_t_s_s(l_281, ((safe_lshift_func_int16_t_s_u((((safe_sub_func_int8_t_s_s((**g_146), 0x72L)) && ((((((safe_add_func_uint64_t_u_u((l_214 ^ ((((safe_sub_func_uint64_t_u_u((((((!((l_215 = ((*l_126) = (safe_mul_func_uint16_t_u_u(g_151, 3UL)))) != l_189)) == (&p_117 != l_293)) & 3UL) | 0xA02BF501L) | 0x81L), l_195[2])) < l_294) | p_117) < (**g_146))), 0L)) , 0x68C1B913420BF123LL) > 0x38D9B2E33A0EA851LL) == (*l_293)) , g_34) > p_117)) <= p_117), g_157[0])) > 0xCD140C127A3C4AB2LL))) || 0xCAC74BC94A9F235CLL), 5)), l_295[1][3][6])), (*l_133))) < l_195[0]);
                    }
                    if (p_117)
                        continue;
                    if ((safe_mul_func_int8_t_s_s(((p_117 < ((((((*l_207) = l_298[4][1][0]) == &g_159[6][0][2]) != (((safe_add_func_int8_t_s_s(0x42L, ((l_205 , (((safe_sub_func_int8_t_s_s(l_295[1][3][6], 255UL)) , ((*l_149) = (((((p_118 ^ (safe_mod_func_uint8_t_u_u((*g_210), 4UL))) || 0UL) && l_295[0][5][7]) >= g_156[4]) <= 0x14CD8B81C6B790A9LL))) && 1UL)) || 1L))) , g_191) & 0x51722BCEL)) & 0x37F4F738L) ^ 0x0C521786769AABB4LL)) | g_156[4]), l_269)))
                    { /* block id: 97 */
                        return g_191;
                    }
                    else
                    { /* block id: 99 */
                        uint32_t l_333[8];
                        float *l_334 = &g_76;
                        float *l_335 = &l_125;
                        int i;
                        for (i = 0; i < 8; i++)
                            l_333[i] = 0x4CFCB6A1L;
                        (*l_132) = (safe_mod_func_int8_t_s_s((safe_add_func_int32_t_s_s(((l_309 == (void*)0) , ((--g_310) <= ((g_336 = ((*l_335) = (safe_sub_func_float_f_f(((((safe_sub_func_float_f_f(((safe_div_func_float_f_f((safe_div_func_float_f_f(l_295[1][3][6], ((p_118 , p_118) < ((*l_334) = (((**g_146) , ((((safe_mod_func_uint32_t_u_u(((safe_sub_func_float_f_f((0x0.2p+1 <= (safe_mul_func_float_f_f(((safe_mul_func_float_f_f((safe_add_func_float_f_f((safe_sub_func_float_f_f((((*l_231) = g_187[0][3]) < (((l_333[2] != (*l_293)) > 255UL) , 0x5.Bp+1)), l_295[1][3][6])), g_5)), p_117)) < 0x0.Bp+1), p_117))), 0x1.85E364p-7)) , 4294967294UL), l_333[2])) != g_136) , g_76) <= 0x2.7D4500p+13)) , (*l_132)))))), g_187[1][2])) == (-0x1.5p-1)), p_118)) == p_118) , (void*)0) == (void*)0), p_117)))) , p_118))), p_118)), 0xD1L));
                    }
                }
                for (l_190 = 0; (l_190 >= 17); l_190++)
                { /* block id: 110 */
                    for (g_34 = (-12); (g_34 >= 4); ++g_34)
                    { /* block id: 113 */
                        uint64_t *l_341 = &l_205.f1;
                        int32_t l_343 = (-6L);
                        int32_t l_344 = (-1L);
                        (*l_293) ^= ((*g_210) || (l_341 != (l_342 , &g_53)));
                        ++g_345;
                        if ((*l_129))
                            break;
                        (*l_133) ^= 0x45463E3BL;
                    }
                }
                if ((p_117 < (safe_rshift_func_int16_t_s_u(((((&g_158 != ((((safe_unary_minus_func_int32_t_s((g_84[0][4] | g_222))) , &p_117) == &p_117) , &g_193)) && ((l_195[7] == (p_118 , l_351)) & 0L)) & 0UL) <= 0x14A3L), 8))))
                { /* block id: 120 */
                    int16_t l_352 = 0xE2A9L;
                    struct S1 l_369 = {5283,18446744073709551613UL,0UL};
                    int8_t *l_374 = &g_157[0];
                    int32_t *l_375 = &g_156[0];
                    const uint32_t l_376 = 0xCFE96971L;
                    g_353--;
                    l_378[7] ^= ((((safe_lshift_func_uint8_t_u_s((+(((((((((((((safe_div_func_int8_t_s_s(((*g_210) <= (l_216 = (5UL | (((safe_sub_func_int64_t_s_s(l_363, (safe_lshift_func_int8_t_s_u((0xDAL ^ ((l_281 , ((*l_375) ^= ((safe_mul_func_uint8_t_u_u((+((*l_374) |= ((l_369 , (safe_sub_func_uint64_t_u_u((l_205 , (*l_127)), (((safe_mod_func_uint16_t_u_u((l_281 , p_118), 0x25B6L)) > l_195[0]) & p_118)))) < l_369.f2))), 0xFFL)) <= (-1L)))) , p_117)), 3)))) | p_117) == 0xA7155FC610984C42LL)))), l_369.f0)) ^ l_376) , (void*)0) != (void*)0) > l_192) , &g_147) == &g_147) <= p_118) && 1UL) >= 0L) >= 1L) > 0UL) >= l_377[4])), (*l_132))) > 0x2213720E50DB3418LL) <= 0x7CAC5A81L) < 18446744073709551615UL);
                }
                else
                { /* block id: 126 */
                    uint32_t l_383 = 0x01B96956L;
                    (*l_129) = l_192;
                    (*l_129) &= p_118;
                    for (p_118 = (-16); (p_118 != 10); p_118++)
                    { /* block id: 131 */
                        return g_136;
                    }
                    (*l_133) &= (p_118 && (((safe_rshift_func_uint16_t_u_u(l_383, ((*g_210) , (safe_sub_func_int16_t_s_s(l_386, (safe_lshift_func_int16_t_s_s(((void*)0 != &l_378[4]), 11))))))) > (*l_293)) ^ p_118));
                }
            }
            else
            { /* block id: 136 */
                uint16_t l_404 = 1UL;
                int32_t l_421[2];
                struct S1 l_431 = {4740,0x3A8E642376F627E4LL,0x99L};
                uint8_t l_437 = 0x2DL;
                int32_t l_469 = 0L;
                uint64_t l_504[5];
                float * const l_513 = &g_76;
                uint64_t l_516 = 5UL;
                int i;
                for (i = 0; i < 2; i++)
                    l_421[i] = 0xD7D2F8A9L;
                for (i = 0; i < 5; i++)
                    l_504[i] = 1UL;
lbl_406:
                for (g_222 = 0; (g_222 > (-23)); g_222 = safe_sub_func_uint32_t_u_u(g_222, 5))
                { /* block id: 139 */
                    return p_117;
                }
                if ((&g_246 != (void*)0))
                { /* block id: 142 */
lbl_448:
                    for (g_151 = 0; (g_151 != (-30)); --g_151)
                    { /* block id: 145 */
                        return p_118;
                    }
                }
                else
                { /* block id: 148 */
                    uint16_t l_399 = 65535UL;
                    int8_t **l_403 = &g_147;
                    int64_t *l_444 = &g_193;
                    int32_t l_452 = 0x483F0C64L;
                    int32_t l_453 = 0x96FCDFB7L;
                    int32_t l_454 = 0L;
                    int32_t l_455 = 1L;
                    uint16_t l_498 = 65535UL;
                    if ((p_117 = (+(((((((safe_add_func_int64_t_s_s((!(safe_sub_func_uint16_t_u_u((p_118 |= l_399), g_345))), (((void*)0 == l_201) > (((*l_149) = (~((safe_rshift_func_int8_t_s_u((((*l_132) |= 0x3479EB4AL) , (&g_147 != (p_117 , l_403))), 7)) & ((*l_209) = ((l_188 < (**g_146)) , l_399))))) < p_117)))) && g_187[0][3]) || l_404) < p_117) , g_336) , p_118) ^ g_157[0]))))
                    { /* block id: 154 */
                        int32_t **l_405[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_405[i] = &l_134[1][3];
                        l_127 = &g_153;
                        if (l_404)
                            goto lbl_406;
                    }
                    else
                    { /* block id: 157 */
                        const int64_t l_413 = 0x991D99280769B278LL;
                        const float **l_425 = (void*)0;
                        const float *l_427 = &g_428;
                        const float **l_426[4][8][8] = {{{&l_427,&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427,(void*)0},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,(void*)0},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427}},{{(void*)0,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,(void*)0,&l_427,&l_427,(void*)0,(void*)0,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427,(void*)0},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,(void*)0,(void*)0},{&l_427,(void*)0,&l_427,&l_427,(void*)0,(void*)0,&l_427,&l_427},{&l_427,&l_427,(void*)0,&l_427,(void*)0,&l_427,&l_427,&l_427}},{{&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427},{(void*)0,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,(void*)0,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427}},{{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,(void*)0,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427,&l_427,&l_427},{&l_427,(void*)0,(void*)0,(void*)0,&l_427,&l_427,&l_427,&l_427},{&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427,(void*)0},{&l_427,&l_427,(void*)0,&l_427,&l_427,&l_427,&l_427,&l_427}}};
                        int i, j, k;
                        l_421[0] = (((safe_add_func_float_f_f(((void*)0 != &g_193), (safe_div_func_float_f_f((((safe_mod_func_uint32_t_u_u((0x42CCEFD581548BD2LL != l_413), (((*l_209) = ((p_117 , (p_118 == (g_140 , (safe_add_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(((((safe_lshift_func_uint8_t_u_s((g_84[0][3] , l_294), 5)) && (*l_126)) < p_118) | l_420[1]), 1)), 255UL))))) <= 1UL)) ^ l_413))) > (*l_129)) , p_118), (*l_129))))) , p_117) != p_117);
                        l_194 ^= ((g_84[0][2] , l_422) || (((safe_sub_func_uint16_t_u_u((l_294 == ((((g_429 = l_231) != (void*)0) <= (l_431 , (l_281 | ((+(safe_add_func_int32_t_s_s((safe_div_func_int64_t_s_s((l_437 == p_117), p_118)), g_222))) ^ l_342.f0)))) < g_353)), p_118)) != 0x80L) | p_117));
                        (*l_128) &= l_399;
                    }
                    for (l_399 = (-12); (l_399 != 2); l_399 = safe_add_func_int64_t_s_s(l_399, 7))
                    { /* block id: 166 */
                        int32_t **l_440 = &l_134[1][3];
                        (*l_126) |= g_136;
                        (*l_440) = &l_421[0];
                    }
                    if (((((*l_128) & (safe_mod_func_int16_t_s_s((((p_118 & (*g_210)) && l_218[0][5]) , (((((*l_444) = p_118) && p_117) < (safe_rshift_func_int8_t_s_u(l_183, (&g_353 == p_116)))) || g_187[0][3])), g_157[0]))) < g_447) != l_399))
                    { /* block id: 171 */
                        int64_t l_449 = 0x549DDB1E68D64E35LL;
                        int32_t l_450 = (-1L);
                        int32_t l_457 = (-1L);
                        (*l_127) &= 0x7C3B369DL;
                        if (l_205.f2)
                            goto lbl_448;
                        g_458++;
                    }
                    else
                    { /* block id: 175 */
                        int8_t l_481 = (-7L);
                        p_117 |= ((safe_add_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_mod_func_uint32_t_u_u((((safe_lshift_func_int16_t_s_s(((((l_469 = 0x0DL) , (safe_div_func_int8_t_s_s(0x6AL, 0x06L))) , (~p_118)) , ((g_159[7][5][1] < (((safe_mul_func_int8_t_s_s((safe_div_func_uint64_t_u_u((safe_mod_func_int64_t_s_s((safe_rshift_func_int8_t_s_s(l_481, ((safe_add_func_uint64_t_u_u((l_194 = (safe_add_func_int8_t_s_s((!(((*l_149) = (safe_lshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u((((g_157[0] && l_493) | ((*l_209) = (safe_mod_func_uint32_t_u_u(((l_496 == l_497[1]) && g_223), p_118)))) || 0UL), 4)), (*l_128))), 4))) , g_345)), l_498))), l_420[1])) & (**g_146)))), 0x071BF38D9014425ELL)), l_481)), p_118)) <= g_458) < l_431.f1)) , 0xFD1DL)), 6)) , (*l_129)) < l_437), l_481)), l_195[0])) <= g_159[0][0][3]), (-1L))) >= l_455);
                    }
                    for (g_185 = 0; (g_185 == 22); ++g_185)
                    { /* block id: 184 */
                        int64_t l_508 = 0xDB9DF99D806BE2D2LL;
                        uint16_t *l_521 = (void*)0;
                        uint16_t *l_522 = &l_498;
                        float **l_541 = &l_309;
                        g_153 ^= (((+((g_159[7][5][1] == (((safe_div_func_int64_t_s_s((l_504[2] || ((*l_133) &= ((*l_128) && (((safe_div_func_int64_t_s_s(l_508, (safe_rshift_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((l_351 != (0x08302F1DL != (l_421[0] = (l_513 == ((safe_lshift_func_int16_t_s_u(l_516, (safe_div_func_int64_t_s_s((((((*l_522) = (l_216 ^= (safe_add_func_uint32_t_u_u((((*l_444) = (0x4644L > p_117)) && g_139), 0UL)))) <= g_53) > 0xF17EL) <= g_139), p_118)))) , (void*)0))))), 0x1DE096B943D947F1LL)), p_118)))) && p_117) | (*g_210))))), l_342.f0)) && l_508) >= (-1L))) <= 0xB45D0DB4L)) && 0L) | l_195[5]);
                        (*l_133) = ((*l_128) ^= (((*l_522) = (~(p_118 | 0x06622119L))) >= ((((safe_lshift_func_uint16_t_u_s((((safe_sub_func_float_f_f(0x9.5p+1, ((-(safe_add_func_float_f_f((((safe_mul_func_uint8_t_u_u(((safe_sub_func_float_f_f((((safe_add_func_float_f_f((((safe_rshift_func_uint16_t_u_s((~((g_139 , ((*l_541) = g_540)) == (void*)0)), ((*l_204) = (((safe_add_func_int32_t_s_s(((safe_div_func_int8_t_s_s(((l_546 , (safe_unary_minus_func_uint8_t_u(((l_548 , 0xA3L) <= 1UL)))) , (-1L)), l_216)) ^ g_353), 0x050F4D02L)) || 0x1BEBL) == p_118)))) , l_281) <= p_117), l_342.f1)) == l_453) > g_549[2]), l_404)) , (*g_210)), p_117)) == l_508) , p_118), (-0x5.Dp+1)))) <= p_118))) , g_5) > l_404), g_156[0])) != 1UL) <= 1L) != l_351)));
                        if (p_118)
                            break;
                    }
                }
            }
            for (g_158 = 0; (g_158 <= 0); g_158 += 1)
            { /* block id: 202 */
                uint32_t l_550 = 1UL;
                l_550--;
                (*l_133) = ((safe_div_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((((((((*l_124) = 0x29836500L) & ((*g_210) < ((g_140.f1 | ((+((l_558[1] == (*l_144)) >= 9L)) | g_156[0])) ^ (safe_lshift_func_uint8_t_u_s(l_548.f0, (safe_div_func_int16_t_s_s((p_117 || l_195[1]), 1UL))))))) , (**g_146)) > p_117) , l_563[3]) || (-1L)), (**g_146))), p_118)) , p_118);
                return l_550;
            }
            (*l_132) = p_118;
            l_564 = l_564;
        }
        else
        { /* block id: 210 */
            int32_t *l_566 = &l_192;
            l_566 = &p_117;
            return g_84[0][0];
        }
    }
    else
    { /* block id: 214 */
        float l_593 = 0x7.74526Ep-68;
        int32_t l_614 = 0L;
        uint32_t * const l_651[1][3] = {{&g_84[0][0],&g_84[0][0],&g_84[0][0]}};
        const int32_t l_657 = 0x2A78727CL;
        int32_t l_660 = (-1L);
        int8_t **l_676 = &l_145;
        struct S1 l_694[8] = {{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL},{-4908,0xB8B746A1D72E0850LL,0x9FL}};
        struct S1 *l_695 = &l_694[6];
        const uint8_t * const l_709 = &g_710[4][6][1];
        const uint8_t * const *l_708[5][8][3] = {{{(void*)0,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{(void*)0,(void*)0,&l_709},{&l_709,&l_709,&l_709},{(void*)0,(void*)0,&l_709},{&l_709,&l_709,&l_709}},{{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,(void*)0,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709}},{{&l_709,(void*)0,&l_709},{(void*)0,&l_709,&l_709},{(void*)0,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,(void*)0},{&l_709,&l_709,&l_709}},{{&l_709,&l_709,&l_709},{&l_709,&l_709,(void*)0},{&l_709,&l_709,(void*)0},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{(void*)0,&l_709,&l_709},{&l_709,&l_709,&l_709},{(void*)0,&l_709,&l_709}},{{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,(void*)0,&l_709},{(void*)0,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,&l_709,&l_709},{&l_709,(void*)0,&l_709},{&l_709,&l_709,(void*)0}}};
        uint32_t * const l_749 = &g_447;
        uint32_t * const *l_748 = &l_749;
        int32_t l_869 = 0x431C2A1EL;
        uint32_t *l_903 = &g_345;
        uint32_t **l_902 = &l_903;
        int32_t l_934 = 7L;
        int32_t l_937 = 0xDD981C91L;
        int32_t l_941 = 0x0EF1DD3FL;
        union U4 *l_950 = &g_951;
        union U4 **l_949[6];
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_949[i] = &l_950;
        for (g_186 = 13; (g_186 != 13); g_186 = safe_add_func_uint64_t_u_u(g_186, 5))
        { /* block id: 217 */
            int8_t l_594 = 1L;
            int32_t l_595 = (-1L);
            uint64_t l_597 = 18446744073709551615UL;
            int32_t l_641 = 0L;
            int32_t * const *l_648 = &l_133;
            uint8_t **l_693 = &g_210;
            for (l_205.f2 = 0; (l_205.f2 != 12); l_205.f2++)
            { /* block id: 220 */
                float l_588[8] = {0xF.3E3301p-48,0xF.3E3301p-48,0xA.5E1DF6p+34,0xF.3E3301p-48,0xF.3E3301p-48,0xA.5E1DF6p+34,0xF.3E3301p-48,0xF.3E3301p-48};
                uint16_t *l_590 = &g_310;
                int32_t l_596 = (-4L);
                int i;
                l_597 = ((0x9.B7B12Dp-55 == (((((safe_rshift_func_int8_t_s_s((0UL == 255UL), 4)) != (safe_lshift_func_int8_t_s_u(0x5CL, ((safe_mul_func_int8_t_s_s(((safe_add_func_int8_t_s_s((((safe_add_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((l_595 = (0x4F3AL | (!((safe_lshift_func_int8_t_s_u(((((safe_lshift_func_uint16_t_u_u(((*l_590) = ((p_117 != g_139) == (!p_118))), (safe_add_func_int16_t_s_s(g_157[0], (*l_124))))) > (*l_124)) , 9L) & (*l_128)), l_594)) > p_117)))), l_594)), g_353)) > p_118) < p_118), p_118)) , p_118), (**g_146))) | l_594)))) == p_118) , l_596) >= (*g_429))) != (-0x1.Dp+1));
                for (g_140.f1 = 0; (g_140.f1 <= 5); g_140.f1 += 1)
                { /* block id: 226 */
                    p_117 = (-6L);
                }
            }
            for (g_353 = 0; (g_353 <= 5); g_353 += 1)
            { /* block id: 232 */
                uint16_t l_611[6][10] = {{0x413CL,65533UL,0x18B2L,0x18B2L,65533UL,0x413CL,0x6AF7L,1UL,0x18B2L,0x6AF7L},{1UL,65533UL,1UL,1UL,65533UL,65526UL,65533UL,1UL,1UL,65533UL},{1UL,0x6AF7L,0x18B2L,1UL,0x6AF7L,0x413CL,65533UL,0x18B2L,0x18B2L,65533UL},{7UL,0x3F37L,7UL,7UL,0x3F37L,7UL,0x2DD1L,0x6AF7L,7UL,0x2DD1L},{0xBC3AL,0x3F37L,65533UL,0x6AF7L,0x3F37L,0UL,0x3F37L,0x6AF7L,65533UL,0x3F37L},{0xBC3AL,0x2DD1L,7UL,0x6AF7L,0x2DD1L,7UL,0x3F37L,7UL,7UL,0x3F37L}};
                int32_t l_615 = 1L;
                int i, j;
                for (l_205.f2 = 0; (l_205.f2 <= 2); l_205.f2 += 1)
                { /* block id: 235 */
                    const int8_t l_600 = 4L;
                    uint32_t l_610 = 5UL;
                    uint8_t * const l_632[1] = {&l_563[3]};
                    int32_t l_642[5] = {0L,0L,0L,0L,0L};
                    int32_t *l_643 = &g_153;
                    uint64_t *l_652 = &l_597;
                    uint16_t *l_659[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j;
                    (*l_132) = ((((safe_lshift_func_uint16_t_u_s(g_221[l_205.f2], 6)) <= (g_140 , (l_600 <= (safe_mul_func_int8_t_s_s(l_600, ((safe_add_func_int64_t_s_s(((p_117 = (((*l_124) , g_353) <= (((safe_div_func_uint8_t_u_u((safe_div_func_int32_t_s_s((l_611[0][9] = ((~p_118) ^ l_610)), (((((safe_add_func_uint64_t_u_u(9UL, l_614)) & l_615) > (-1L)) , 1UL) | g_140.f0))), p_117)) < l_595) <= l_594))) <= p_118), p_118)) != g_153)))))) , 0x5ED0L) | 0xFB7CL);
                    if ((safe_lshift_func_uint8_t_u_s(g_221[0], ((*l_128) = (g_221[l_205.f2] , 0x7AL)))))
                    { /* block id: 240 */
                        int32_t **l_618 = &l_128;
                        uint32_t *l_639 = &l_610;
                        (*l_618) = &g_34;
                        l_641 ^= ((safe_sub_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u((!((safe_div_func_int8_t_s_s(((*l_133) = (((safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_u((l_614 | p_117), (safe_mod_func_uint8_t_u_u((**l_618), (((void*)0 == l_632[0]) & p_118))))) , ((((safe_lshift_func_int16_t_s_u((((g_185 & ((l_597 != (((safe_lshift_func_uint8_t_u_u((((safe_div_func_int32_t_s_s((((*l_639) ^= p_118) , p_117), l_595)) != p_117) || p_118), l_597)) > (**l_618)) && 250UL)) | p_117)) , p_117) || (-9L)), p_117)) , p_117) >= 0x061EE22CL) && (**l_618))), p_117)) < l_597) >= 1L)), 0xDCL)) <= 0xA6L)), l_640)), l_600)) <= g_84[0][0]);
                        (*l_126) = (g_458 | ((l_615 < (l_642[0] = g_84[0][0])) , l_614));
                    }
                    else
                    { /* block id: 247 */
                        int32_t **l_644 = (void*)0;
                        int32_t **l_645 = &l_643;
                        (*g_540) = (-0x1.8p-1);
                        (*l_645) = l_643;
                    }
                    if (((l_614 = ((p_117 , ((*g_540) < (+(+(&l_643 != l_648))))) , ((safe_mod_func_uint8_t_u_u((&g_84[0][0] != l_651[0][1]), (l_615 |= p_118))) || (++(*l_652))))) <= ((((l_660 |= ((safe_rshift_func_int16_t_s_u((l_657 , (((**l_648) = ((*l_129) &= ((g_658[1][2][0] , 0x3333L) < 1L))) && p_118)), (*l_643))) , l_611[0][9])) != 0xC103L) , g_661[4][3][0]) != (void*)0)))
                    { /* block id: 257 */
                        (*l_132) &= (safe_div_func_uint64_t_u_u(0x68A967D29A9991BDLL, (**l_648)));
                        return p_118;
                    }
                    else
                    { /* block id: 260 */
                        (*l_643) = ((p_118 | 0x17237C5A730128D4LL) == 0x699FC6544DF1C61ALL);
                    }
                    for (l_196 = 0; (l_196 <= 1); l_196 += 1)
                    { /* block id: 265 */
                        struct S1 *l_671 = &g_140;
                        int8_t ***l_677 = &l_144;
                        int64_t *l_683 = &l_363;
                        int64_t *l_684 = (void*)0;
                        int64_t *l_685 = &g_193;
                        int i, j, k;
                        p_117 = (((*l_671) = g_140) , (safe_mod_func_int16_t_s_s(((safe_div_func_int32_t_s_s((7L || (l_676 == ((*l_677) = l_676))), ((safe_div_func_uint32_t_u_u((!(((*l_685) = ((*l_683) = (l_565[l_205.f2][(g_353 + 2)][(l_196 + 1)] == g_681))) <= g_187[l_196][(l_196 + 1)])), ((safe_lshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(p_117, ((((safe_lshift_func_int8_t_s_s(((safe_unary_minus_func_uint64_t_u(((p_118 && 0x2392D961L) < (*l_126)))) >= p_118), 1)) , &g_210) != l_693) >= l_660))), g_140.f0)) | p_117))) || (**g_146)))) != 6UL), 65535UL)));
                    }
                }
            }
        }
        (*l_695) = l_694[6];
        for (g_151 = 5; (g_151 >= 0); g_151 -= 1)
        { /* block id: 278 */
            int32_t l_698 = 0x203B8898L;
            uint8_t *l_705 = &l_377[0];
            int8_t *l_737 = &g_221[0];
            const float *l_781 = &g_742.f0;
            int32_t l_837[5];
            int32_t l_855 = (-8L);
            int32_t l_898 = 0x93594B6EL;
            uint32_t **l_905 = &l_903;
            int32_t l_945[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
            union U4 ***l_952 = &l_949[0];
            int64_t l_1023 = (-1L);
            int i;
            for (i = 0; i < 5; i++)
                l_837[i] = 0L;
        }
    }
    return (*l_129);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    transparent_crc(g_35, "g_35", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_36[i], "g_36[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_53, "g_53", print_hash_value);
    transparent_crc(g_55, "g_55", print_hash_value);
    transparent_crc_bytes (&g_76, sizeof(g_76), "g_76", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_84[i][j], "g_84[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_123, "g_123", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_136, "g_136", print_hash_value);
    transparent_crc(g_139, "g_139", print_hash_value);
    transparent_crc(g_140.f0, "g_140.f0", print_hash_value);
    transparent_crc(g_140.f1, "g_140.f1", print_hash_value);
    transparent_crc(g_140.f2, "g_140.f2", print_hash_value);
    transparent_crc(g_151, "g_151", print_hash_value);
    transparent_crc(g_153, "g_153", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_156[i], "g_156[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_157[i], "g_157[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_158, "g_158", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_159[i][j][k], "g_159[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_185, "g_185", print_hash_value);
    transparent_crc(g_186, "g_186", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_187[i][j], "g_187[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_191, "g_191", print_hash_value);
    transparent_crc(g_193, "g_193", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_221[i], "g_221[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_222, "g_222", print_hash_value);
    transparent_crc(g_223, "g_223", print_hash_value);
    transparent_crc(g_245, "g_245", print_hash_value);
    transparent_crc(g_246, "g_246", print_hash_value);
    transparent_crc(g_248, "g_248", print_hash_value);
    transparent_crc(g_310, "g_310", print_hash_value);
    transparent_crc(g_336, "g_336", print_hash_value);
    transparent_crc(g_345, "g_345", print_hash_value);
    transparent_crc(g_353, "g_353", print_hash_value);
    transparent_crc_bytes (&g_428, sizeof(g_428), "g_428", print_hash_value);
    transparent_crc_bytes (&g_430, sizeof(g_430), "g_430", print_hash_value);
    transparent_crc(g_447, "g_447", print_hash_value);
    transparent_crc(g_458, "g_458", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_549[i], "g_549[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_658[i][j][k], "g_658[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_663[i][j][k], "g_663[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_665[i], "g_665[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_666, "g_666", print_hash_value);
    transparent_crc(g_667, "g_667", print_hash_value);
    transparent_crc(g_668, "g_668", print_hash_value);
    transparent_crc(g_682, "g_682", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc_bytes(&g_700[i][j][k].f0, sizeof(g_700[i][j][k].f0), "g_700[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_710[i][j][k], "g_710[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_742.f0, sizeof(g_742.f0), "g_742.f0", print_hash_value);
    transparent_crc(g_743, "g_743", print_hash_value);
    transparent_crc_bytes (&g_786, sizeof(g_786), "g_786", print_hash_value);
    transparent_crc(g_844, "g_844", print_hash_value);
    transparent_crc(g_845, "g_845", print_hash_value);
    transparent_crc(g_868, "g_868", print_hash_value);
    transparent_crc(g_871, "g_871", print_hash_value);
    transparent_crc(g_929, "g_929", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_935[i][j], "g_935[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_936, "g_936", print_hash_value);
    transparent_crc(g_938, "g_938", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_939[i], "g_939[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_940, "g_940", print_hash_value);
    transparent_crc(g_942, "g_942", print_hash_value);
    transparent_crc(g_946, "g_946", print_hash_value);
    transparent_crc_bytes (&g_951.f0, sizeof(g_951.f0), "g_951.f0", print_hash_value);
    transparent_crc_bytes (&g_1057, sizeof(g_1057), "g_1057", print_hash_value);
    transparent_crc(g_1107, "g_1107", print_hash_value);
    transparent_crc_bytes (&g_1216.f0, sizeof(g_1216.f0), "g_1216.f0", print_hash_value);
    transparent_crc(g_1241, "g_1241", print_hash_value);
    transparent_crc(g_1305, "g_1305", print_hash_value);
    transparent_crc(g_1336, "g_1336", print_hash_value);
    transparent_crc(g_1337, "g_1337", print_hash_value);
    transparent_crc(g_1348, "g_1348", print_hash_value);
    transparent_crc(g_1349, "g_1349", print_hash_value);
    transparent_crc_bytes (&g_1357.f0, sizeof(g_1357.f0), "g_1357.f0", print_hash_value);
    transparent_crc_bytes (&g_1358.f0, sizeof(g_1358.f0), "g_1358.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_1359[i].f0, sizeof(g_1359[i].f0), "g_1359[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_1360[i].f0, sizeof(g_1360[i].f0), "g_1360[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_1361[i].f0, sizeof(g_1361[i].f0), "g_1361[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1363.f0, sizeof(g_1363.f0), "g_1363.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1406[i].f0, "g_1406[i].f0", print_hash_value);
        transparent_crc(g_1406[i].f1, "g_1406[i].f1", print_hash_value);
        transparent_crc(g_1406[i].f2, "g_1406[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1417, "g_1417", print_hash_value);
    transparent_crc(g_1534, "g_1534", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1538[i].f0, "g_1538[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1568, "g_1568", print_hash_value);
    transparent_crc(g_1598.f0, "g_1598.f0", print_hash_value);
    transparent_crc(g_1598.f1, "g_1598.f1", print_hash_value);
    transparent_crc(g_1598.f2, "g_1598.f2", print_hash_value);
    transparent_crc(g_1598.f3, "g_1598.f3", print_hash_value);
    transparent_crc(g_1598.f4, "g_1598.f4", print_hash_value);
    transparent_crc(g_1598.f5, "g_1598.f5", print_hash_value);
    transparent_crc(g_1598.f6, "g_1598.f6", print_hash_value);
    transparent_crc(g_1598.f7, "g_1598.f7", print_hash_value);
    transparent_crc(g_1601.f0, "g_1601.f0", print_hash_value);
    transparent_crc(g_1601.f1, "g_1601.f1", print_hash_value);
    transparent_crc(g_1601.f2, "g_1601.f2", print_hash_value);
    transparent_crc(g_1601.f3, "g_1601.f3", print_hash_value);
    transparent_crc(g_1601.f4, "g_1601.f4", print_hash_value);
    transparent_crc(g_1601.f5, "g_1601.f5", print_hash_value);
    transparent_crc(g_1601.f6, "g_1601.f6", print_hash_value);
    transparent_crc(g_1601.f7, "g_1601.f7", print_hash_value);
    transparent_crc(g_1663.f0, "g_1663.f0", print_hash_value);
    transparent_crc(g_1663.f1, "g_1663.f1", print_hash_value);
    transparent_crc(g_1663.f2, "g_1663.f2", print_hash_value);
    transparent_crc(g_1663.f3, "g_1663.f3", print_hash_value);
    transparent_crc(g_1663.f4, "g_1663.f4", print_hash_value);
    transparent_crc(g_1663.f5, "g_1663.f5", print_hash_value);
    transparent_crc(g_1663.f6, "g_1663.f6", print_hash_value);
    transparent_crc(g_1663.f7, "g_1663.f7", print_hash_value);
    transparent_crc_bytes (&g_1671.f0, sizeof(g_1671.f0), "g_1671.f0", print_hash_value);
    transparent_crc_bytes (&g_1672.f0, sizeof(g_1672.f0), "g_1672.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1758[i][j], "g_1758[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc_bytes(&g_1777[i][j].f0, sizeof(g_1777[i][j].f0), "g_1777[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1795, "g_1795", print_hash_value);
    transparent_crc(g_1830, "g_1830", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1870[i][j], "g_1870[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1886[i].f0, "g_1886[i].f0", print_hash_value);
        transparent_crc(g_1886[i].f1, "g_1886[i].f1", print_hash_value);
        transparent_crc(g_1886[i].f2, "g_1886[i].f2", print_hash_value);
        transparent_crc(g_1886[i].f3, "g_1886[i].f3", print_hash_value);
        transparent_crc(g_1886[i].f4, "g_1886[i].f4", print_hash_value);
        transparent_crc(g_1886[i].f5, "g_1886[i].f5", print_hash_value);
        transparent_crc(g_1886[i].f6, "g_1886[i].f6", print_hash_value);
        transparent_crc(g_1886[i].f7, "g_1886[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1903, "g_1903", print_hash_value);
    transparent_crc(g_1909, "g_1909", print_hash_value);
    transparent_crc(g_1921.f0, "g_1921.f0", print_hash_value);
    transparent_crc(g_1926, "g_1926", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_1927[i], sizeof(g_1927[i]), "g_1927[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1964.f0, sizeof(g_1964.f0), "g_1964.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_1979[i], sizeof(g_1979[i]), "g_1979[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_2008[i][j][k].f0, "g_2008[i][j][k].f0", print_hash_value);
                transparent_crc(g_2008[i][j][k].f1, "g_2008[i][j][k].f1", print_hash_value);
                transparent_crc(g_2008[i][j][k].f2, "g_2008[i][j][k].f2", print_hash_value);
                transparent_crc(g_2008[i][j][k].f3, "g_2008[i][j][k].f3", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 478
   depth: 1, occurrence: 20
XXX total union variables: 2

XXX non-zero bitfields defined in structs: 9
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 21
breakdown:
   indirect level: 0, occurrence: 19
   indirect level: 1, occurrence: 1
   indirect level: 2, occurrence: 1
XXX full-bitfields structs in the program: 1
breakdown:
   indirect level: 0, occurrence: 1
XXX times a bitfields struct's address is taken: 24
XXX times a bitfields struct on LHS: 4
XXX times a bitfields struct on RHS: 39
XXX times a single bitfield on LHS: 10
XXX times a single bitfield on RHS: 25

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 284
   depth: 2, occurrence: 58
   depth: 3, occurrence: 6
   depth: 4, occurrence: 5
   depth: 5, occurrence: 2
   depth: 6, occurrence: 4
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 2
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 4
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 22, occurrence: 3
   depth: 23, occurrence: 5
   depth: 24, occurrence: 3
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 2
   depth: 33, occurrence: 3
   depth: 38, occurrence: 2
   depth: 39, occurrence: 1
   depth: 40, occurrence: 4
   depth: 41, occurrence: 1
   depth: 49, occurrence: 1

XXX total number of pointers: 445

XXX times a variable address is taken: 1132
XXX times a pointer is dereferenced on RHS: 180
breakdown:
   depth: 1, occurrence: 150
   depth: 2, occurrence: 27
   depth: 3, occurrence: 3
XXX times a pointer is dereferenced on LHS: 260
breakdown:
   depth: 1, occurrence: 248
   depth: 2, occurrence: 10
   depth: 3, occurrence: 2
XXX times a pointer is compared with null: 33
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 5936

XXX max dereference level: 3
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 955
   level: 2, occurrence: 117
   level: 3, occurrence: 56
XXX number of pointers point to pointers: 149
XXX number of pointers point to scalars: 266
XXX number of pointers point to structs: 15
XXX percent of pointers has null in alias set: 30.1
XXX average alias set size: 1.45

XXX times a non-volatile is read: 1546
XXX times a non-volatile is write: 776
XXX times a volatile is read: 24
XXX    times read thru a pointer: 11
XXX times a volatile is write: 9
XXX    times written thru a pointer: 3
XXX times a volatile is available for access: 2.52e+03
XXX percentage of non-volatile access: 98.6

XXX forward jumps: 0
XXX backward jumps: 8

XXX stmts: 274
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 45
   depth: 2, occurrence: 36
   depth: 3, occurrence: 37
   depth: 4, occurrence: 50
   depth: 5, occurrence: 73

XXX percentage a fresh-made variable is used: 15.4
XXX percentage an existing variable is used: 84.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1067283755
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int16_t g_8 = 1L;/* VOLATILE GLOBAL g_8 */
static float g_16 = 0x5.28ACE4p-19;
static uint64_t g_25 = 2UL;
static uint8_t g_44 = 0x69L;
static uint64_t g_51 = 0xF34FFDEABFD090D7LL;
static int32_t g_67[7][9][2] = {{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}},{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}},{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}},{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}},{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}},{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}},{{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}}};
static uint32_t g_70[6] = {0x90EB1F0CL,4294967295UL,0x90EB1F0CL,0x90EB1F0CL,4294967295UL,0x90EB1F0CL};
static uint16_t g_79 = 6UL;
static uint64_t g_117 = 0xD8C0438810CA98D2LL;
static int16_t g_119 = 0x451EL;
static int8_t g_121 = 0xD8L;
static uint64_t *g_138 = &g_51;
static int32_t g_143 = 8L;
static float g_161 = 0x1.5p+1;
static volatile uint16_t g_184[7] = {0xFAD2L,0xFAD2L,0xFAD2L,0xFAD2L,0xFAD2L,0xFAD2L,0xFAD2L};
static volatile uint16_t * volatile g_183 = &g_184[2];/* VOLATILE GLOBAL g_183 */
static volatile uint16_t * volatile *g_182 = &g_183;
static volatile uint16_t * volatile **g_181[5] = {&g_182,&g_182,&g_182,&g_182,&g_182};
static uint16_t *g_187[1][6][8] = {{{&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79}}};
static uint16_t * const *g_186[7][2] = {{&g_187[0][4][2],&g_187[0][4][7]},{&g_187[0][4][7],&g_187[0][4][2]},{&g_187[0][4][7],&g_187[0][4][7]},{&g_187[0][4][2],&g_187[0][4][7]},{&g_187[0][4][7],&g_187[0][4][2]},{&g_187[0][4][7],&g_187[0][4][7]},{&g_187[0][4][2],&g_187[0][4][7]}};
static uint16_t * const **g_185 = &g_186[3][1];
static uint64_t g_198 = 0x295CE5FD00375717LL;
static uint32_t g_232 = 1UL;
static int32_t * volatile *g_237 = (void*)0;
static int16_t g_262 = 0x6B0BL;
static uint16_t g_263 = 0x4C9FL;
static float *g_292 = &g_161;
static int64_t g_304 = (-4L);
static uint16_t ***g_378 = (void*)0;
static float g_417 = 0xD.A19F55p+99;
static int32_t g_418 = (-3L);
static float g_421 = 0x1.2p-1;
static int32_t * const g_444 = &g_67[4][6][1];
static uint32_t g_463[7] = {0xD7D87FD6L,0xD7D87FD6L,0xD7D87FD6L,0xD7D87FD6L,0xD7D87FD6L,0xD7D87FD6L,0xD7D87FD6L};
static uint32_t g_497 = 0x8D55BE72L;
static int64_t g_514 = 5L;
static int32_t *g_540 = &g_67[4][0][1];
static int64_t g_559 = 0xFBD534EC6BD21A55LL;
static float **g_633 = &g_292;
static float ***g_632[9] = {&g_633,&g_633,&g_633,&g_633,&g_633,&g_633,&g_633,&g_633,&g_633};
static uint16_t ** const ***g_640 = (void*)0;
static uint16_t *** const *g_647 = &g_378;
static uint16_t *** const **g_646 = &g_647;
static int8_t ** volatile *g_762 = (void*)0;
static int64_t *g_785[5] = {&g_559,&g_559,&g_559,&g_559,&g_559};
static uint8_t *g_825 = &g_44;
static volatile int8_t g_847 = (-1L);/* VOLATILE GLOBAL g_847 */
static volatile int8_t *g_846 = &g_847;
static volatile int8_t **g_845 = &g_846;
static volatile uint8_t **g_978 = (void*)0;
static volatile uint8_t ***g_977 = &g_978;
static int16_t g_997 = 0x966CL;
static int8_t *g_1004 = &g_121;
static int8_t **g_1003 = &g_1004;
static int8_t ***g_1002 = &g_1003;
static int8_t ****g_1001 = &g_1002;
static int64_t g_1032[3][5][9] = {{{1L,(-8L),0x72012AE7CFBC14ABLL,(-1L),1L,1L,(-1L),0x72012AE7CFBC14ABLL,(-8L)},{3L,(-2L),0xB11EF0DC4647651ALL,1L,0x8F3F6872B8BA286FLL,(-9L),1L,1L,(-4L)},{0x37E05B53621C3C99LL,3L,0xF148C21510E2D877LL,1L,0L,0x2AA1A1BC40336F88LL,1L,0L,0x4878973E435E39A1LL},{1L,(-2L),1L,0x3D858E06950BCD8ELL,0x8F3F6872B8BA286FLL,0x8F3F6872B8BA286FLL,0x3D858E06950BCD8ELL,1L,(-2L)},{1L,3L,(-1L),3L,0L,0x5AE0DFF2CE19CF3CLL,3L,0L,0xEAF8EBFBE92538F0LL}},{{0x3D858E06950BCD8ELL,(-2L),0xCF253E9ED950D764LL,(-8L),0x8F3F6872B8BA286FLL,0x85AA29ACFFD71D19LL,(-8L),1L,3L},{3L,3L,0L,0x37E05B53621C3C99LL,0L,0L,0x37E05B53621C3C99LL,0L,3L},{(-8L),(-2L),0xB11EF0DC4647651ALL,1L,0x8F3F6872B8BA286FLL,(-9L),1L,1L,(-4L)},{0x37E05B53621C3C99LL,3L,0xF148C21510E2D877LL,1L,0L,0x2AA1A1BC40336F88LL,1L,0L,0x4878973E435E39A1LL},{1L,(-2L),1L,0x3D858E06950BCD8ELL,0x8F3F6872B8BA286FLL,0x8F3F6872B8BA286FLL,0x3D858E06950BCD8ELL,1L,(-2L)}},{{1L,3L,(-1L),3L,0L,0x5AE0DFF2CE19CF3CLL,3L,0L,0xEAF8EBFBE92538F0LL},{0x3D858E06950BCD8ELL,(-2L),0xCF253E9ED950D764LL,(-8L),0x8F3F6872B8BA286FLL,0x85AA29ACFFD71D19LL,(-8L),1L,3L},{3L,3L,0L,0x37E05B53621C3C99LL,0L,0L,0x37E05B53621C3C99LL,0L,3L},{(-8L),(-2L),0xB11EF0DC4647651ALL,1L,0x8F3F6872B8BA286FLL,(-9L),1L,1L,(-4L)},{0x37E05B53621C3C99LL,3L,0xF148C21510E2D877LL,1L,0L,0x2AA1A1BC40336F88LL,1L,0L,0x4878973E435E39A1LL}}};
static int32_t g_1041 = 0x8B4E5B4FL;
static uint32_t g_1106 = 18446744073709551615UL;
static float g_1134[1][6][6] = {{{0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1},{0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1},{0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1},{0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1},{0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1},{0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1,0x8.Fp+1}}};
static int8_t g_1136 = (-10L);
static int32_t ** const g_1159 = &g_540;
static int32_t ** const *g_1158 = &g_1159;
static int16_t g_1191 = 0x7D74L;
static int16_t g_1211 = (-9L);
static const int32_t g_1279 = 0x299084A8L;
static uint32_t g_1328[1] = {0x7CA01B14L};
static uint8_t g_1450 = 1UL;
static uint8_t **g_1544[9][9][3] = {{{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825}},{{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825}},{{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825}},{{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825}},{{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825}},{{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825}},{{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825}},{{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825}},{{&g_825,&g_825,&g_825},{&g_825,(void*)0,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{&g_825,&g_825,&g_825},{(void*)0,(void*)0,&g_825},{&g_825,&g_825,&g_825},{(void*)0,&g_825,&g_825},{&g_825,&g_825,&g_825}}};
static uint8_t ***g_1543[2][8] = {{&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2]},{&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2],&g_1544[0][3][2]}};
static uint8_t g_1646[8] = {0x97L,0x97L,0xB8L,0x97L,0x97L,0xB8L,0x97L,0x97L};
static uint8_t ****g_1649 = (void*)0;
static uint64_t **g_1748 = &g_138;
static uint64_t ** const *g_1747 = &g_1748;
static uint64_t g_1762 = 7UL;
static uint32_t *g_1823[8] = {&g_70[2],&g_497,&g_70[2],&g_70[2],&g_497,&g_70[2],&g_70[2],&g_497};
static uint32_t **g_1822[8][1][1] = {{{(void*)0}},{{(void*)0}},{{(void*)0}},{{(void*)0}},{{(void*)0}},{{(void*)0}},{{(void*)0}},{{(void*)0}}};
static uint32_t **g_1824 = &g_1823[1];
static uint32_t g_1842 = 1UL;
static int32_t g_2023 = (-9L);
static int64_t g_2070[6] = {0L,0L,0L,0L,0L,0L};
static uint64_t g_2071 = 0xFFBB3F4301ED4495LL;
static const int64_t *g_2083 = &g_514;
static const int64_t **g_2082[6] = {&g_2083,&g_2083,&g_2083,&g_2083,&g_2083,&g_2083};
static const uint8_t *g_2092 = (void*)0;
static int8_t ** const *g_2121 = &g_1003;
static int8_t ** const **g_2120 = &g_2121;
static int16_t *g_2311 = &g_119;
static int16_t * const *g_2310 = &g_2311;
static int8_t g_2330 = 2L;
static int32_t g_2353 = 1L;
static uint32_t g_2363[8][1] = {{0x2E8735BAL},{18446744073709551615UL},{0x2E8735BAL},{18446744073709551615UL},{0x2E8735BAL},{18446744073709551615UL},{0x2E8735BAL},{18446744073709551615UL}};
static int32_t g_2448[1][9] = {{1L,1L,1L,1L,1L,1L,1L,1L,1L}};
static int64_t g_2450 = 0xE998C8F00AE4B68FLL;
static int8_t g_2454 = 7L;
static int64_t g_2455 = 0x81312DFD9DB7477FLL;
static uint32_t g_2523[7][7][3] = {{{0xA0F04530L,0xEC1C3569L,18446744073709551615UL},{1UL,0xBC4509F8L,9UL},{18446744073709551615UL,8UL,0x4E6F860AL},{0x0FFAD2CEL,1UL,0UL},{18446744073709551615UL,4UL,0xABFA65B3L},{1UL,0x0FFAD2CEL,18446744073709551609UL},{0xA0F04530L,0x6B62DB98L,8UL}},{{0x50D61601L,0xE7900332L,0x5E8F77CDL},{0xEC1C3569L,0xAF5EB7EDL,0x1A4D1A7CL},{0xBE76D1DCL,0x2AFAB632L,18446744073709551615UL},{7UL,0xEC1C3569L,0xB7517E2BL},{0UL,0UL,0x73F26528L},{18446744073709551615UL,0x3C8C5F04L,0x4E6F860AL},{9UL,0x8C9E42D0L,0xE5AC0142L}},{{0x71A8FC5FL,0x582FD22EL,18446744073709551615UL},{1UL,9UL,0xE5AC0142L},{0x6B62DB98L,0xA0F04530L,0x4E6F860AL},{0xF6D2039DL,0xE7900332L,0x73F26528L},{0xF88F248AL,4UL,0xB7517E2BL},{0x85553D11L,0UL,18446744073709551615UL},{0xA0F04530L,0xF88F248AL,0x1A4D1A7CL}},{{0xBC4509F8L,1UL,0x5E8F77CDL},{18446744073709551615UL,8UL,8UL},{0x5E8F77CDL,0x2AFAB632L,18446744073709551609UL},{2UL,0x582FD22EL,0xABFA65B3L},{0UL,0x5E8F77CDL,0UL},{18446744073709551615UL,7UL,0x4E6F860AL},{0x50D61601L,0x5E8F77CDL,9UL}},{{8UL,0x582FD22EL,18446744073709551615UL},{0x85553D11L,0x2AFAB632L,0x50D61601L},{0x6B62DB98L,8UL,18446744073709551606UL},{0x5E6861E9L,1UL,0x73F26528L},{18446744073709551615UL,0xF88F248AL,18446744073709551615UL},{0x50D61601L,6UL,0x60C06FB5L},{18446744073709551606UL,0x9C4743B1L,18446744073709551606UL}},{{0x0FFAD2CEL,0x50D61601L,0x07F6F75EL},{8UL,0x3C8C5F04L,2UL},{0xA0B54DFEL,0xF6D2039DL,0UL},{0x5BBC6558L,0xA0F04530L,4UL},{0xA0B54DFEL,0x29309792L,7UL},{8UL,0x5BBC6558L,0xF775FB06L},{0x0FFAD2CEL,8UL,0xF6D2039DL}},{{18446744073709551606UL,0x71A8FC5FL,4UL},{0x50D61601L,0x5E6861E9L,0UL},{0xBE31772BL,7UL,0xB7517E2BL},{0x00BC07F5L,0x50D61601L,0x8C9E42D0L},{0x091EB0EAL,0x091EB0EAL,18446744073709551615UL},{0xE5AC0142L,18446744073709551615UL,0UL},{2UL,0x9C4743B1L,0UL}}};
static int8_t g_2581 = 0xBAL;
static uint32_t *g_2652 = &g_232;
static uint32_t **g_2651[1][8] = {{&g_2652,&g_2652,&g_2652,&g_2652,&g_2652,&g_2652,&g_2652,&g_2652}};
static uint32_t ***g_2650 = &g_2651[0][5];
static volatile int32_t * const ** const * volatile *g_2655 = (void*)0;
static int8_t * const **g_2689 = (void*)0;
static int8_t * const ***g_2688 = &g_2689;
static int8_t * const ****g_2687[8] = {&g_2688,&g_2688,&g_2688,&g_2688,&g_2688,&g_2688,&g_2688,&g_2688};
static int32_t g_2710 = (-4L);
static int64_t * volatile * volatile g_2848 = &g_785[4];/* VOLATILE GLOBAL g_2848 */
static int64_t * volatile * volatile *g_2847 = &g_2848;
static int64_t * volatile * volatile ** volatile g_2846 = &g_2847;/* VOLATILE GLOBAL g_2846 */
static int64_t * volatile * volatile ** volatile *g_2845 = &g_2846;
static int32_t g_2872 = (-3L);


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static uint8_t  func_4(int8_t  p_5, uint32_t  p_6);
static uint64_t  func_12(float  p_13, const int32_t  p_14, uint16_t  p_15);
static uint16_t  func_20(float  p_21);
static float  func_22(uint64_t  p_23, float  p_24);
static float  func_29(uint32_t  p_30, const uint64_t  p_31, int32_t  p_32, int16_t  p_33);
static float  func_36(uint64_t  p_37, int8_t  p_38);
static uint64_t  func_39(uint8_t  p_40, const uint8_t  p_41, int32_t  p_42);
static const int32_t  func_58(uint8_t * p_59);
static uint8_t * func_60(uint64_t * p_61, uint32_t  p_62, const int64_t  p_63);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_8 g_16 g_25 g_44 g_51 g_70 g_67 g_79 g_1003 g_304 g_825 g_198 g_1032 g_138 g_143 g_1002 g_1004 g_1041 g_121 g_540 g_1747 g_1762 g_1159 g_1191 g_633 g_292 g_161 g_1279 g_263 g_1842 g_1748 g_1646 g_1136 g_1158 g_514 g_2523 g_1001 g_2120 g_2121 g_2311 g_444 g_2581 g_2310 g_119 g_1824 g_1823 g_497 g_845 g_846 g_847 g_184 g_2655 g_2650 g_2651 g_2710 g_559 g_183 g_2083 g_232 g_418 g_2688 g_2689 g_2845 g_2872
 * writes: g_44 g_51 g_70 g_79 g_1004 g_262 g_1032 g_1041 g_121 g_67 g_1747 g_161 g_785 g_825 g_304 g_497 g_187 g_263 g_1822 g_1824 g_1842 g_1543 g_1136 g_540 g_514 g_2523 g_119 g_2070 g_2581 g_143 g_2023 g_2650 g_1646 g_198 g_2655 g_2687 g_559 g_232 g_418 g_1001 g_25 g_1134 g_2872
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_17[2];
    uint64_t l_26 = 0x266D8FF8CD3BED16LL;
    int16_t l_2633 = 0x78BAL;
    int64_t *l_2635 = &g_1032[1][1][5];
    int32_t *l_2871 = &g_2872;
    int i;
    for (i = 0; i < 2; i++)
        l_17[i] = 0x34L;
    (*g_444) = ((*l_2871) &= (safe_add_func_uint16_t_u_u((((func_4(((safe_unary_minus_func_uint64_t_u(g_8)) || (((*g_2311) = (g_8 ^ (((safe_unary_minus_func_uint64_t_u(1UL)) , (((((*l_2635) = ((safe_add_func_int8_t_s_s((func_12((g_16 < l_17[0]), (safe_rshift_func_uint16_t_u_u(func_20(func_22(g_25, (l_26 != (l_17[1] >= (safe_add_func_float_f_f(((func_29(g_25, g_25, g_25, g_25) <= l_26) == 0x0.5p-1), 0x6.Ep-1)))))), l_2633)), l_26) != l_17[1]), 0x9FL)) , l_17[0])) <= 0L) < l_2633) <= l_2633)) ^ 0x3D321E82247479BBLL))) > l_26)), l_2633) & l_2633) < l_2633) , 0x2E73L), l_17[0])));
    return (*l_2871);
}


/* ------------------------------------------ */
/* 
 * reads : g_1158 g_1159 g_444 g_67 g_2311 g_825 g_44 g_1646 g_845 g_846 g_847 g_1824 g_1823 g_70 g_497 g_198 g_79 g_184 g_633 g_292 g_2655 g_161 g_2650 g_2651 g_138 g_51 g_2710 g_559 g_540 g_119 g_183 g_2083 g_514 g_232 g_418 g_2688 g_2689 g_1747 g_1748 g_25 g_1842 g_2845 g_263
 * writes: g_540 g_67 g_2650 g_119 g_44 g_1646 g_198 g_79 g_161 g_2655 g_2687 g_559 g_70 g_497 g_232 g_418 g_1001 g_263 g_51 g_25 g_1842 g_1134
 */
static uint8_t  func_4(int8_t  p_5, uint32_t  p_6)
{ /* block id: 1164 */
    int32_t *l_2636[8][6][5] = {{{&g_2023,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_67[4][0][1],&g_2023,&g_67[4][0][1]},{&g_67[4][0][1],&g_67[4][0][1],(void*)0,&g_2023,&g_67[4][0][1]},{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]},{&g_2023,&g_2023,&g_1041,&g_1041,&g_2023},{&g_67[4][0][1],&g_2023,&g_1041,(void*)0,(void*)0}},{{&g_2023,&g_67[4][0][1],&g_2023,&g_1041,(void*)0},{&g_2023,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_67[4][0][1],&g_2023,&g_67[4][0][1]},{&g_67[4][0][1],&g_67[4][0][1],(void*)0,&g_2023,&g_67[4][0][1]},{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]},{&g_2023,&g_2023,&g_1041,&g_1041,&g_2023}},{{&g_67[4][0][1],&g_2023,&g_1041,(void*)0,(void*)0},{&g_2023,&g_67[4][0][1],&g_2023,&g_1041,(void*)0},{&g_2023,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_67[4][0][1],&g_2023,&g_67[4][0][1]},{&g_67[4][0][1],&g_67[4][0][1],(void*)0,&g_2023,&g_67[4][0][1]},{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]}},{{&g_2023,&g_2023,&g_1041,&g_1041,&g_2023},{&g_67[4][0][1],&g_2023,&g_1041,(void*)0,(void*)0},{&g_2023,&g_67[4][0][1],&g_2023,&g_1041,(void*)0},{&g_2023,&g_2023,(void*)0,&g_2023,&g_2023},{&g_2023,&g_2023,&g_67[4][0][1],&g_2023,&g_67[4][0][1]},{&g_67[4][0][1],&g_67[4][0][1],(void*)0,&g_2023,&g_67[4][0][1]}},{{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]},{&g_2023,&g_1041,(void*)0,(void*)0,&g_1041},{&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1],&g_67[4][0][1]},{&g_2023,&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1]},{&g_1041,&g_2023,&g_67[4][0][1],&g_2023,&g_1041},{&g_2023,&g_2023,&g_67[4][0][1],&g_1041,&g_67[4][0][1]}},{{&g_67[4][0][1],&g_67[4][0][1],&g_67[4][0][1],&g_1041,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]},{&g_2023,&g_1041,(void*)0,(void*)0,&g_1041},{&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1],&g_67[4][0][1]},{&g_2023,&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1]},{&g_1041,&g_2023,&g_67[4][0][1],&g_2023,&g_1041}},{{&g_2023,&g_2023,&g_67[4][0][1],&g_1041,&g_67[4][0][1]},{&g_67[4][0][1],&g_67[4][0][1],&g_67[4][0][1],&g_1041,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]},{&g_2023,&g_1041,(void*)0,(void*)0,&g_1041},{&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1],&g_67[4][0][1]},{&g_2023,&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1]}},{{&g_1041,&g_2023,&g_67[4][0][1],&g_2023,&g_1041},{&g_2023,&g_2023,&g_67[4][0][1],&g_1041,&g_67[4][0][1]},{&g_67[4][0][1],&g_67[4][0][1],&g_67[4][0][1],&g_1041,&g_2023},{&g_2023,&g_2023,&g_2023,&g_2023,&g_67[4][0][1]},{&g_2023,&g_1041,(void*)0,(void*)0,&g_1041},{&g_67[4][0][1],&g_2023,(void*)0,&g_67[4][0][1],&g_67[4][0][1]}}};
    uint32_t *l_2638 = (void*)0;
    uint32_t **l_2637 = &l_2638;
    int8_t l_2641 = 0x69L;
    int16_t l_2656 = 1L;
    int64_t ** const l_2680 = &g_785[1];
    float **l_2696 = &g_292;
    int i, j, k;
    (**g_1158) = l_2636[5][5][2];
    (*g_444) ^= (l_2637 != &l_2638);
    if (((*g_444) ^= (safe_mul_func_uint8_t_u_u(((l_2641 , &g_785[1]) == (void*)0), (safe_lshift_func_int8_t_s_u((p_6 , (-3L)), ((((safe_lshift_func_uint16_t_u_u((((1UL && ((((((safe_mod_func_uint32_t_u_u(((safe_rshift_func_int16_t_s_s(((g_2650 = &l_2637) != &l_2637), ((((*g_825) ^= (((*g_2311) = (p_6 , 0x85CCL)) , p_6)) && (**g_845)) ^ p_5))) , 0UL), (**g_1824))) ^ p_6) == 249UL) , (void*)0) != (void*)0) , 0UL)) || p_5) && p_6), 11)) | 1L) || 1L) , p_6)))))))
    { /* block id: 1171 */
        uint8_t l_2657 = 0x58L;
        uint32_t **l_2676[3][6] = {{&g_1823[1],&g_1823[1],&g_1823[1],&g_1823[5],&g_1823[3],&g_1823[3]},{&g_1823[1],&g_1823[1],&g_1823[1],&g_1823[1],&g_1823[1],&g_1823[1]},{&g_1823[1],&g_1823[1],&g_1823[3],&g_1823[1],&g_1823[1],&g_1823[5]}};
        const int64_t ** const l_2679 = &g_2083;
        int32_t *l_2713 = &g_67[4][0][1];
        int32_t l_2723 = (-2L);
        uint32_t l_2725 = 0x26129C65L;
        int32_t l_2730 = 0x3CD30AD3L;
        uint64_t l_2768 = 0xA8053EDED6002657LL;
        uint32_t ***l_2771 = &g_2651[0][5];
        int i, j;
        for (g_198 = 24; (g_198 != 21); g_198 = safe_sub_func_uint8_t_u_u(g_198, 3))
        { /* block id: 1174 */
            uint32_t **l_2675 = &g_1823[1];
            int32_t l_2681 = 0xFF76E108L;
            int8_t *** const *l_2691 = &g_1002;
            int8_t *** const **l_2690 = &l_2691;
            int32_t l_2721 = 9L;
            int32_t l_2724[5][1];
            float l_2767 = 0x0.Fp+1;
            int i, j;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 1; j++)
                    l_2724[i][j] = (-1L);
            }
            for (g_79 = 1; (g_79 <= 4); g_79 += 1)
            { /* block id: 1177 */
                int i;
                (**g_633) = g_184[(g_79 + 2)];
                g_2655 = (p_5 , g_2655);
                if (p_5)
                    continue;
                l_2657++;
            }
            if ((safe_div_func_int64_t_s_s((((((safe_mod_func_int16_t_s_s(((*g_2311) = (safe_div_func_int32_t_s_s((+(safe_sub_func_int64_t_s_s((safe_rshift_func_uint8_t_u_u((((*g_292) == (((safe_div_func_int8_t_s_s((safe_lshift_func_int16_t_s_s((p_5 ^ (l_2675 == l_2676[2][1])), 3)), p_5)) , (*g_2650)) != (*g_2650))) , ((safe_add_func_int32_t_s_s((-10L), (l_2679 == l_2680))) , l_2657)), p_5)), (*g_138)))), l_2657))), p_6)) == p_6) >= p_5) != l_2681) <= 0x2F3A552EL), p_6)))
            { /* block id: 1184 */
                int32_t *l_2682 = &g_67[4][8][1];
                (**g_1158) = l_2682;
            }
            else
            { /* block id: 1186 */
                int8_t * const ***l_2686 = (void*)0;
                int8_t * const ****l_2685 = &l_2686;
                int32_t l_2714 = 0x4B190F90L;
                int32_t l_2717 = 0xC7AA6FCCL;
                int32_t l_2718 = 0x125E6427L;
                int32_t l_2722 = (-1L);
                uint16_t l_2739 = 0xD7D0L;
                int32_t *l_2747 = (void*)0;
                float l_2765 = 0x0.CD5B93p-50;
                if (((safe_mul_func_uint16_t_u_u((((g_2687[4] = l_2685) != l_2690) & (1UL | p_5)), (8L < ((safe_mod_func_uint64_t_u_u((safe_sub_func_uint16_t_u_u((l_2696 != (void*)0), (~(((*g_444) = p_6) | p_6)))), 2UL)) >= 0x4CL)))) | p_6))
                { /* block id: 1189 */
                    const int32_t *** const *l_2701 = (void*)0;
                    const int32_t *** const **l_2700 = &l_2701;
                    int64_t *l_2711 = &g_559;
                    int32_t l_2712 = (-9L);
                    int32_t l_2715 = (-8L);
                    int32_t l_2720 = 0xC01DF81FL;
                    if (((p_6 || (safe_mul_func_uint8_t_u_u(8UL, ((l_2681 = (**g_1824)) && ((l_2700 == g_2655) & 1UL))))) != (safe_mul_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(((*g_138) != (l_2712 ^= (safe_mod_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u(g_2710, (((*l_2711) |= l_2657) || p_5))) <= p_5), (-1L))))), p_5)), p_6))))
                    { /* block id: 1193 */
                        (**g_1158) = (**g_1158);
                        l_2713 = (*g_1159);
                        if (p_5)
                            break;
                    }
                    else
                    { /* block id: 1197 */
                        int8_t l_2716 = 4L;
                        int64_t l_2719[1][5][10] = {{{0xB3540A2EE578EF72LL,1L,0x0C3D1DC2B53D1CE7LL,1L,0xB3540A2EE578EF72LL,0x0C3D1DC2B53D1CE7LL,1L,1L,0x0C3D1DC2B53D1CE7LL,0xB3540A2EE578EF72LL},{0xB3540A2EE578EF72LL,0x0794831424561D83LL,0x0794831424561D83LL,0xB3540A2EE578EF72LL,0x49C17728AF92928ALL,(-9L),0xB3540A2EE578EF72LL,(-9L),0x49C17728AF92928ALL,0xB3540A2EE578EF72LL},{(-9L),0xB3540A2EE578EF72LL,(-9L),0x49C17728AF92928ALL,0xB3540A2EE578EF72LL,0x0794831424561D83LL,0x0794831424561D83LL,0xB3540A2EE578EF72LL,0x49C17728AF92928ALL,(-9L)},{1L,1L,0x0C3D1DC2B53D1CE7LL,0xB3540A2EE578EF72LL,1L,0x0C3D1DC2B53D1CE7LL,1L,0xB3540A2EE578EF72LL,0x0C3D1DC2B53D1CE7LL,1L},{1L,0x0794831424561D83LL,(-9L),1L,0x49C17728AF92928ALL,0x49C17728AF92928ALL,1L,(-9L),0x0794831424561D83LL,1L}}};
                        int i, j, k;
                        (*g_1159) = (void*)0;
                        l_2725--;
                        (*l_2713) |= p_6;
                    }
                    (*g_444) |= (((**g_1824) = l_2717) > ((safe_mul_func_int8_t_s_s(p_6, l_2730)) <= ((l_2739 = (safe_mul_func_uint16_t_u_u((((void*)0 != (*g_1158)) == (((safe_lshift_func_int8_t_s_s((&l_2686 != (((l_2724[4][0] != ((safe_div_func_uint32_t_u_u(((safe_lshift_func_int16_t_s_s((*g_2311), 10)) != (-3L)), p_5)) , p_6)) || 1UL) , (void*)0)), l_2724[0][0])) , (*g_138)) | 3UL)), l_2724[0][0]))) , p_6)));
                    if ((l_2721 >= (safe_unary_minus_func_uint64_t_u((!((l_2715 = (p_5 &= (p_6 & 255UL))) , p_6))))))
                    { /* block id: 1207 */
                        float l_2743 = 0xD.26E915p-53;
                        (*g_444) |= 0xE763F1DEL;
                        (*g_444) |= 0xD996B08CL;
                        (*g_444) = (!((void*)0 != &g_1823[6]));
                        return p_5;
                    }
                    else
                    { /* block id: 1212 */
                        uint16_t l_2744 = 0x0169L;
                        if (p_6)
                            break;
                        l_2744--;
                        (**g_1158) = l_2747;
                        l_2747 = (void*)0;
                    }
                }
                else
                { /* block id: 1218 */
                    uint32_t l_2760 = 18446744073709551615UL;
                    int32_t l_2764 = 0xBFCC4458L;
                    if (((((safe_sub_func_int8_t_s_s((p_5 = ((safe_mod_func_int64_t_s_s((+((((l_2724[0][0] & ((safe_lshift_func_uint8_t_u_u((+(safe_div_func_int64_t_s_s(p_6, (((*g_183) & (safe_mod_func_int64_t_s_s(((((((p_6 >= l_2760) , 255UL) < 2UL) > (safe_rshift_func_int16_t_s_u(p_6, 8))) < 0xE310L) || (*g_138)), (*g_2083)))) && 0xCE3008BFB70A8F94LL)))), 6)) ^ p_5)) <= (*l_2713)) , l_2760) && (*l_2713))), 5L)) , 0L)), p_6)) && (*l_2713)) == p_6) , p_6))
                    { /* block id: 1220 */
                        int32_t *l_2763 = &l_2723;
                        int32_t l_2766 = 0xD2657727L;
                        (**g_1158) = l_2763;
                        l_2768--;
                    }
                    else
                    { /* block id: 1223 */
                        uint32_t ****l_2772 = &l_2771;
                        (*l_2713) = (&g_2651[0][5] == ((*l_2772) = l_2771));
                        return (*g_825);
                    }
                }
                return p_5;
            }
        }
    }
    else
    { /* block id: 1232 */
        uint32_t l_2773[1][4][5] = {{{4294967294UL,1UL,1UL,4294967294UL,7UL},{0xB2923447L,0x719FBEBBL,0x719FBEBBL,4294967294UL,4294967295UL},{0xF409FDB1L,4294967294UL,4294967294UL,0xF409FDB1L,1UL},{4294967294UL,0xB2923447L,0xB2923447L,4294967294UL,4294967295UL}}};
        int32_t l_2774 = 0xE56B917DL;
        const uint32_t *l_2781 = &l_2773[0][2][1];
        const uint32_t **l_2780[2][2];
        const uint32_t ***l_2779 = &l_2780[1][1];
        int64_t **l_2801 = &g_785[0];
        int64_t ***l_2800 = &l_2801;
        int32_t l_2815 = 0xDDA8C3C3L;
        int32_t l_2816 = 0xB1852749L;
        uint8_t l_2818 = 0x5AL;
        int32_t l_2833 = (-10L);
        int32_t l_2836 = 8L;
        int32_t l_2837 = 0xD7D57089L;
        int32_t l_2838 = 0xF5C697F8L;
        int32_t l_2839 = 0xE8C7D2DEL;
        int32_t l_2840 = 0x12038A11L;
        int32_t l_2841 = 0x91A479ACL;
        int32_t l_2855 = 0x7EFA2493L;
        uint16_t ***l_2867 = (void*)0;
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 2; j++)
                l_2780[i][j] = &l_2781;
        }
        for (g_232 = 0; (g_232 <= 0); g_232 += 1)
        { /* block id: 1235 */
            const int8_t *l_2789 = &g_2581;
            const int8_t **l_2788 = &l_2789;
            const int8_t ** const *l_2787 = &l_2788;
            const int8_t ** const * const *l_2786 = &l_2787;
            uint64_t **l_2799 = &g_138;
            int64_t ***l_2803 = &l_2801;
            int64_t *** const l_2804[5] = {&l_2801,&l_2801,&l_2801,&l_2801,&l_2801};
            uint16_t l_2805[9][2][4] = {{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}},{{3UL,1UL,0x462CL,1UL},{3UL,0x634DL,0x462CL,0x634DL}}};
            int32_t l_2807 = 0x3CFF83E7L;
            int32_t l_2808[10];
            int32_t l_2821 = 0x5DA1C782L;
            uint32_t l_2822 = 0x85C86F99L;
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_2808[i] = 0x44722B9FL;
            (**l_2696) = (*g_292);
            (*g_444) = (l_2774 |= l_2773[0][3][3]);
            for (g_418 = 0; (g_418 >= 0); g_418 -= 1)
            { /* block id: 1241 */
                int8_t ****l_2785 = &g_1002;
                uint16_t *l_2790 = &g_263;
                int32_t l_2791[4] = {0x2B3E4535L,0x2B3E4535L,0x2B3E4535L,0x2B3E4535L};
                uint8_t l_2842 = 0xF6L;
                uint16_t **l_2869 = &l_2790;
                uint16_t ***l_2868 = &l_2869;
                int32_t l_2870 = 0xEB9C70F1L;
                int i;
                (**g_1158) = ((safe_add_func_int32_t_s_s(((((*l_2790) = (((*g_2311) = ((((safe_rshift_func_int16_t_s_u(((void*)0 != g_2655), 1)) & (&g_1822[4][0][0] != l_2779)) >= (+(((255UL & ((g_1001 = (l_2785 = &g_1002)) != l_2786)) , (*g_2688)) != (p_6 , (void*)0)))) || p_5)) || 65527UL)) <= 0x5138L) ^ l_2773[0][3][3]), 0xEDFC9860L)) , (void*)0);
                if (l_2773[0][1][3])
                { /* block id: 1247 */
                    if (p_6)
                        break;
                    l_2791[3] = p_5;
                }
                else
                { /* block id: 1250 */
                    int64_t ****l_2802[6][7][6] = {{{(void*)0,(void*)0,&l_2800,&l_2800,&l_2800,(void*)0},{&l_2800,&l_2800,&l_2800,&l_2800,(void*)0,(void*)0},{&l_2800,&l_2800,&l_2800,&l_2800,&l_2800,(void*)0},{&l_2800,&l_2800,&l_2800,&l_2800,&l_2800,&l_2800},{(void*)0,&l_2800,&l_2800,&l_2800,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,(void*)0,(void*)0,(void*)0}},{{&l_2800,&l_2800,(void*)0,&l_2800,&l_2800,&l_2800},{(void*)0,(void*)0,&l_2800,&l_2800,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2800,&l_2800,(void*)0},{&l_2800,&l_2800,&l_2800,(void*)0,(void*)0,&l_2800},{&l_2800,&l_2800,(void*)0,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,(void*)0,&l_2800,(void*)0,&l_2800}},{{(void*)0,&l_2800,(void*)0,(void*)0,&l_2800,&l_2800},{&l_2800,(void*)0,&l_2800,&l_2800,&l_2800,&l_2800},{(void*)0,&l_2800,&l_2800,&l_2800,(void*)0,(void*)0},{(void*)0,&l_2800,(void*)0,&l_2800,&l_2800,(void*)0},{&l_2800,&l_2800,&l_2800,&l_2800,&l_2800,&l_2800},{(void*)0,&l_2800,&l_2800,(void*)0,(void*)0,&l_2800},{(void*)0,(void*)0,&l_2800,&l_2800,&l_2800,&l_2800}},{{&l_2800,&l_2800,&l_2800,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2800,(void*)0,&l_2800,(void*)0,&l_2800,(void*)0},{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,(void*)0},{&l_2800,&l_2800,(void*)0,&l_2800,&l_2800,&l_2800}},{{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,(void*)0,&l_2800,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,(void*)0,&l_2800,&l_2800},{(void*)0,&l_2800,&l_2800,(void*)0,&l_2800,(void*)0},{&l_2800,(void*)0,(void*)0,&l_2800,(void*)0,&l_2800},{&l_2800,(void*)0,(void*)0,(void*)0,(void*)0,&l_2800},{(void*)0,&l_2800,&l_2800,&l_2800,&l_2800,(void*)0}},{{(void*)0,(void*)0,(void*)0,&l_2800,(void*)0,&l_2800},{(void*)0,&l_2800,&l_2800,&l_2800,&l_2800,(void*)0},{(void*)0,&l_2800,(void*)0,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,&l_2800,&l_2800,(void*)0,&l_2800},{&l_2800,&l_2800,(void*)0,(void*)0,&l_2800,&l_2800},{(void*)0,&l_2800,&l_2800,(void*)0,&l_2800,&l_2800},{&l_2800,&l_2800,(void*)0,&l_2800,&l_2800,&l_2800}}};
                    int32_t l_2806 = 7L;
                    int32_t l_2810 = 0xB160AE60L;
                    int32_t l_2811 = 1L;
                    int32_t l_2812 = 1L;
                    int32_t l_2814[3];
                    int32_t l_2817 = 1L;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_2814[i] = 0xF2D61222L;
                    if ((safe_mul_func_uint8_t_u_u(((~(((void*)0 != &g_2082[2]) >= ((safe_mod_func_uint8_t_u_u((((0xD484375C4D9B34BALL | (safe_lshift_func_int8_t_s_u(((*g_1747) == l_2799), ((l_2803 = l_2800) != l_2804[4])))) && (l_2805[2][0][3] >= ((*g_444) = (p_6 | p_5)))) & l_2773[0][3][3]), (*g_825))) ^ l_2805[5][0][2]))) <= 18446744073709551609UL), 253UL)))
                    { /* block id: 1253 */
                        return p_5;
                    }
                    else
                    { /* block id: 1255 */
                        int16_t l_2809 = (-8L);
                        int32_t l_2813[7];
                        int i;
                        for (i = 0; i < 7; i++)
                            l_2813[i] = 0xA2A93760L;
                        ++l_2818;
                        if (p_5)
                            continue;
                        return p_5;
                    }
                }
                for (g_51 = 0; (g_51 <= 0); g_51 += 1)
                { /* block id: 1263 */
                    int32_t l_2825 = 0x2BBE5E55L;
                    int32_t l_2827 = 0x1E26EF20L;
                    int32_t l_2828[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2828[i] = (-7L);
                    for (g_25 = 0; (g_25 <= 0); g_25 += 1)
                    { /* block id: 1266 */
                        l_2822++;
                    }
                    for (g_25 = 0; (g_25 <= 0); g_25 += 1)
                    { /* block id: 1271 */
                        return l_2825;
                    }
                    for (g_559 = 0; (g_559 >= 0); g_559 -= 1)
                    { /* block id: 1276 */
                        int16_t l_2826[3];
                        int32_t l_2829 = 1L;
                        int32_t l_2830 = 0x9597E0DDL;
                        int32_t l_2831 = 0x68B37A91L;
                        int32_t l_2832 = 0x7C3B7BCEL;
                        int32_t l_2834 = 0xF331F190L;
                        int32_t l_2835 = 0xDCFB30E6L;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_2826[i] = 0x6CB3L;
                        l_2842++;
                    }
                    if (p_6)
                        continue;
                    for (g_1842 = 0; (g_1842 <= 0); g_1842 += 1)
                    { /* block id: 1282 */
                        int64_t * volatile * volatile ** volatile *l_2849 = &g_2846;
                        int i, j, k;
                        l_2849 = g_2845;
                        l_2841 = ((safe_mul_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((p_5 == (!(((((**g_1824) = ((p_5 || 9L) | l_2855)) & ((safe_mul_func_int8_t_s_s(0x9BL, ((safe_mul_func_float_f_f(((!0x4AL) , (safe_div_func_float_f_f(((*g_292) = (safe_div_func_float_f_f(p_6, (g_1134[g_418][(g_51 + 5)][(g_418 + 1)] = ((((*l_2790)++) , l_2867) == l_2868))))), p_5))), l_2828[1])) , l_2870))) && p_5)) , (void*)0) != l_2696))), 252UL)), l_2870)) | l_2833);
                    }
                }
            }
        }
    }
    return p_5;
}


/* ------------------------------------------ */
/* 
 * reads : g_67
 * writes:
 */
static uint64_t  func_12(float  p_13, const int32_t  p_14, uint16_t  p_15)
{ /* block id: 1159 */
    int32_t *l_2634 = &g_67[4][0][1];
    l_2634 = l_2634;
    return (*l_2634);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t  func_20(float  p_21)
{ /* block id: 1156 */
    int16_t l_2625 = 0x3826L;
    int32_t l_2626 = 0xE0C43D98L;
    int32_t *l_2627 = &l_2626;
    int32_t *l_2628[5] = {&g_67[3][4][1],&g_67[3][4][1],&g_67[3][4][1],&g_67[3][4][1],&g_67[3][4][1]};
    uint16_t l_2629 = 0x619CL;
    uint32_t l_2632 = 0UL;
    int i;
    l_2629--;
    return l_2632;
}


/* ------------------------------------------ */
/* 
 * reads : g_1159 g_540 g_1001 g_1002 g_2120 g_2121 g_633 g_292 g_2311 g_444 g_2581 g_1748 g_138 g_143 g_1158 g_2310 g_119 g_1747 g_51 g_1824 g_1823 g_70 g_497 g_1041
 * writes: g_540 g_161 g_119 g_2070 g_67 g_2581 g_51 g_143 g_1041 g_2023
 */
static float  func_22(uint64_t  p_23, float  p_24)
{ /* block id: 1130 */
    int8_t ***l_2537 = &g_1003;
    int8_t ****l_2538 = &l_2537;
    int32_t l_2543 = (-1L);
    uint64_t ***l_2545[5] = {&g_1748,&g_1748,&g_1748,&g_1748,&g_1748};
    uint64_t ****l_2544 = &l_2545[2];
    int16_t **l_2555 = (void*)0;
    int32_t l_2561 = 0x5125C44AL;
    int32_t l_2562 = 0x13B57C79L;
    int32_t l_2565 = 9L;
    int32_t l_2569[9] = {0xCD133FFEL,0xCD133FFEL,0xCD133FFEL,0xCD133FFEL,0xCD133FFEL,0xCD133FFEL,0xCD133FFEL,0xCD133FFEL,0xCD133FFEL};
    int8_t l_2573[8][2] = {{0L,0L},{0L,0xE8L},{0L,0L},{0L,0xE8L},{0L,0L},{0L,0xE8L},{0L,0L},{0L,0xE8L}};
    float l_2597[6] = {(-0x1.Bp+1),(-0x1.Bp+1),(-0x1.Bp+1),(-0x1.Bp+1),(-0x1.Bp+1),(-0x1.Bp+1)};
    int32_t l_2599 = 0xFE931F38L;
    uint64_t l_2600 = 1UL;
    uint16_t l_2621 = 0UL;
    int32_t *l_2622 = (void*)0;
    int32_t *l_2623 = &g_1041;
    int32_t *l_2624[9][2] = {{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]},{&l_2569[6],&l_2569[6]}};
    int i, j;
    (*g_1159) = (*g_1159);
    if ((safe_add_func_int32_t_s_s(((safe_mul_func_uint8_t_u_u(((*g_1001) == (void*)0), ((safe_mod_func_uint16_t_u_u(((((0x7.C1A6D9p+11 != ((**g_633) = ((safe_add_func_float_f_f(p_24, ((safe_add_func_float_f_f(0x1.A695EBp+9, ((*g_2120) != ((*l_2538) = l_2537)))) < (((safe_mul_func_float_f_f((safe_mul_func_float_f_f(l_2543, 0x8.3532CEp-98)), 0x4.Cp-1)) < p_23) != (-0x7.Ep+1))))) != p_23))) <= (-0x5.5p-1)) , l_2544) != (void*)0), 0xB511L)) > 0x060651879F909436LL))) == 0x68CBL), p_23)))
    { /* block id: 1134 */
        int64_t *l_2546 = &g_2070[5];
        const int16_t *l_2554 = &g_997;
        const int16_t ** const l_2553 = &l_2554;
        uint16_t *l_2558[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_2559 = 0x84EA0D91L;
        int32_t l_2560 = 0x38A774F4L;
        int32_t l_2563 = 0x1E00A35AL;
        int32_t l_2564 = 0x650C8617L;
        int32_t l_2566 = 0x3E967C08L;
        int32_t l_2567 = 0x02CE9E7BL;
        int32_t l_2568[4][9] = {{(-1L),0xD8ABAF29L,0xED552C0BL,0xED552C0BL,0xD8ABAF29L,(-1L),(-7L),0xD8ABAF29L,(-7L)},{(-9L),0x47818773L,(-1L),(-1L),0x47818773L,(-9L),0xA8D6547CL,0x47818773L,0xA8D6547CL},{(-1L),0xD8ABAF29L,0xED552C0BL,0xED552C0BL,0xD8ABAF29L,(-1L),(-7L),0xD8ABAF29L,(-7L)},{(-9L),0x47818773L,(-1L),(-1L),0x47818773L,(-9L),0xA8D6547CL,0x47818773L,0xA8D6547CL}};
        int32_t l_2570[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int16_t l_2571[6][8][5] = {{{0x4A01L,0x35D8L,0x77F9L,0xCFF0L,0xF4C5L},{0xE4D0L,0L,0x8CF9L,3L,0x48F1L},{(-6L),0x35D8L,0L,0xF296L,0x77F9L},{(-9L),0L,0L,0L,0L},{0x35D8L,0x35D8L,0xF4C5L,0x311AL,(-3L)},{9L,0L,(-7L),0L,0x9A3BL},{5L,0x35D8L,(-3L),(-4L),0L},{6L,0L,0x48F1L,0x8E18L,(-7L)}},{{(-1L),0x35D8L,(-1L),1L,(-1L)},{(-4L),0L,0x9A3BL,3L,0x8CF9L},{0x4A01L,0x35D8L,0x77F9L,0xCFF0L,0xF4C5L},{0xE4D0L,0L,0x8CF9L,3L,0x48F1L},{(-6L),0x35D8L,0L,0xF296L,0x77F9L},{(-9L),0L,0L,0L,0L},{0x35D8L,0x35D8L,0xF4C5L,0x311AL,(-3L)},{9L,0L,(-7L),0L,0x9A3BL}},{{5L,0x35D8L,(-3L),(-4L),0L},{6L,0L,0x48F1L,0x8E18L,(-7L)},{(-1L),0x35D8L,(-1L),1L,(-1L)},{(-4L),0L,0x9A3BL,3L,0x8CF9L},{0x4A01L,0x35D8L,0x77F9L,0xCFF0L,0xF4C5L},{0xE4D0L,0L,0x8CF9L,3L,0x48F1L},{(-6L),0x35D8L,0L,0xF296L,0x77F9L},{(-9L),0L,0L,(-6L),0L}},{{0xF4C5L,0xF4C5L,2L,0x9B85L,(-2L)},{0L,(-7L),0x3C8CL,(-1L),(-1L)},{0L,0xF4C5L,(-2L),0xE60DL,0x94EDL},{0x8CF9L,(-7L),(-1L),0x420AL,0x3C8CL},{0x77F9L,0xF4C5L,7L,(-6L),7L},{0x9A3BL,(-7L),(-1L),0xC50AL,1L},{(-1L),0xF4C5L,0L,1L,2L},{0x48F1L,(-7L),1L,0xEBB9L,(-1L)}},{{(-3L),0xF4C5L,0x94EDL,0xAB09L,0L},{(-7L),(-7L),0L,(-6L),0L},{0xF4C5L,0xF4C5L,2L,0x9B85L,(-2L)},{0L,(-7L),0x3C8CL,(-1L),(-1L)},{0L,0xF4C5L,(-2L),0xE60DL,0x94EDL},{0x8CF9L,(-7L),(-1L),0x420AL,0x3C8CL},{0x77F9L,0xF4C5L,7L,(-6L),7L},{0x9A3BL,(-7L),(-1L),0xC50AL,1L}},{{(-1L),0xF4C5L,0L,1L,2L},{0x48F1L,(-7L),1L,0xEBB9L,(-1L)},{(-3L),0xF4C5L,0x94EDL,0xAB09L,0L},{(-7L),(-7L),0L,(-6L),0L},{0xF4C5L,0xF4C5L,2L,0x9B85L,(-2L)},{0L,(-7L),0x3C8CL,(-1L),(-1L)},{0L,0xF4C5L,(-2L),0xE60DL,0x94EDL},{0x8CF9L,(-7L),(-1L),0x420AL,0x3C8CL}}};
        int64_t l_2572 = 0x0720056E0B9CE57CLL;
        uint16_t l_2574 = 0x6AD8L;
        int i, j, k;
        g_2581 &= (((0xCC92E72AE3C392FALL && ((*l_2546) = ((((*g_2311) = ((*g_1159) == (void*)0)) <= 65531UL) && p_23))) , (safe_div_func_uint16_t_u_u(((safe_sub_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s(((p_23 && ((*g_444) = ((l_2553 == l_2555) , (safe_add_func_uint16_t_u_u((l_2574++), (safe_add_func_int16_t_s_s((((safe_div_func_float_f_f(l_2569[5], p_24)) , 0xF9L) | l_2564), 0xA7C5L))))))) && l_2565), 6)), 1UL)) > p_23), p_23))) <= p_23);
    }
    else
    { /* block id: 1140 */
        uint8_t l_2589 = 0x21L;
        int32_t l_2590 = 0x850FDB37L;
        int32_t l_2591 = 0x846BB145L;
        int32_t *l_2592 = &l_2543;
        int32_t *l_2593 = (void*)0;
        int32_t *l_2594 = &g_67[4][0][1];
        int32_t *l_2595[10][1] = {{&l_2590},{(void*)0},{(void*)0},{&l_2590},{(void*)0},{(void*)0},{&l_2590},{(void*)0},{(void*)0},{&l_2590}};
        int32_t l_2596 = (-1L);
        int32_t l_2598 = 7L;
        int i, j;
        l_2591 &= (((p_23 == l_2569[5]) || (safe_lshift_func_uint16_t_u_u((0L && (safe_div_func_int8_t_s_s(((p_23 >= p_23) != ((**g_1748) = l_2569[2])), ((p_23 != ((safe_mul_func_uint8_t_u_u((+p_23), (l_2589 = 0x82L))) ^ 0L)) , p_23)))), 13))) >= l_2590);
        l_2600++;
        for (g_143 = 7; (g_143 >= 0); g_143 -= 1)
        { /* block id: 1147 */
            int8_t l_2603 = 0L;
            if (l_2603)
                break;
        }
    }
    (**g_1158) = (void*)0;
    g_2023 = ((*l_2623) ^= ((safe_unary_minus_func_uint8_t_u(((safe_sub_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((safe_div_func_uint32_t_u_u((l_2573[0][1] , (((**g_2310) > l_2562) | 0xF7AA38F526B64210LL)), ((*g_444) = p_23))), l_2569[2])), (safe_mod_func_uint32_t_u_u(((safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((***g_1747) || (p_23 <= 0x0A92A7B16D6795EELL)) && l_2573[4][1]), 0x6F3AL)), (**g_2310))), (**g_1824))) , 4294967290UL), 4294967295UL)))), l_2600)) ^ p_23))) , l_2621));
    return p_24;
}


/* ------------------------------------------ */
/* 
 * reads : g_25 g_44 g_51 g_70 g_67 g_79 g_1003 g_304 g_825 g_198 g_1032 g_138 g_143 g_1002 g_1004 g_1041 g_121 g_540 g_1747 g_1762 g_1159 g_1191 g_633 g_292 g_161 g_1279 g_263 g_1842 g_1748 g_1646 g_1136 g_1158 g_514 g_2523
 * writes: g_44 g_51 g_70 g_79 g_1004 g_262 g_1032 g_1041 g_121 g_67 g_1747 g_161 g_785 g_825 g_304 g_497 g_187 g_263 g_1822 g_1824 g_1842 g_1543 g_1136 g_540 g_514 g_2523
 */
static float  func_29(uint32_t  p_30, const uint64_t  p_31, int32_t  p_32, int16_t  p_33)
{ /* block id: 1 */
    uint8_t *l_43 = &g_44;
    int32_t l_49 = 0x945AD33DL;
    uint64_t *l_50 = &g_51;
    int32_t l_2515 = 0xC78B94F3L;
    int32_t *l_2516 = &l_2515;
    int32_t *l_2517 = &g_2023;
    int32_t *l_2518 = &l_2515;
    int32_t l_2519 = 0x0EFD9AD1L;
    int32_t *l_2520 = &l_2515;
    int32_t *l_2521 = &g_2023;
    int32_t *l_2522[1][10] = {{&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1],&g_67[0][8][1]}};
    int i, j;
    (**g_633) = (safe_div_func_float_f_f(func_36(func_39(g_25, (++(*l_43)), ((safe_add_func_uint64_t_u_u((--(*l_50)), p_31)) , (safe_mul_func_uint8_t_u_u(g_51, g_51)))), (((safe_add_func_int64_t_s_s((p_30 & (safe_lshift_func_int8_t_s_u((-10L), 7))), ((0xF98C77ABL ^ (p_32 == p_33)) < p_32))) || 0x79283981L) , 0L)), l_49));
    if (g_121)
        goto lbl_2526;
    (**g_1158) = (*g_1159);
lbl_2526:
    --g_2523[2][6][1];
    (*g_292) = p_30;
    return p_30;
}


/* ------------------------------------------ */
/* 
 * reads : g_514 g_292 g_161
 * writes: g_514
 */
static float  func_36(uint64_t  p_37, int8_t  p_38)
{ /* block id: 1000 */
    uint32_t l_2205 = 4294967289UL;
    int8_t l_2240 = 0x5AL;
    int32_t *l_2247 = &g_67[4][0][1];
    int32_t l_2275 = 0xD3D90A31L;
    int32_t l_2276 = 0x8B7FE5EFL;
    int32_t l_2279 = 0x1DA16B08L;
    int32_t l_2282 = (-2L);
    int32_t l_2283 = 0xA35BAF04L;
    int32_t l_2285 = (-3L);
    int32_t l_2287 = 0x470E8DBBL;
    int32_t l_2291[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
    int8_t **l_2302[8];
    float **l_2403[10];
    int32_t * const *l_2410 = &g_444;
    int32_t * const **l_2409[9][1][2] = {{{(void*)0,&l_2410}},{{&l_2410,&l_2410}},{{(void*)0,&l_2410}},{{&l_2410,&l_2410}},{{&l_2410,&l_2410}},{{(void*)0,&l_2410}},{{&l_2410,&l_2410}},{{(void*)0,&l_2410}},{{&l_2410,&l_2410}}};
    int32_t * const ***l_2408 = &l_2409[2][0][0];
    float l_2461 = 0x1.Cp-1;
    uint8_t ***l_2489 = &g_1544[3][5][0];
    int32_t l_2508 = 0L;
    const int16_t *l_2512[5][3][7] = {{{&g_262,&g_997,&g_997,&g_997,&g_262,(void*)0,&g_997},{&g_1191,&g_1191,(void*)0,(void*)0,&g_1191,&g_1191,(void*)0},{(void*)0,&g_1191,&g_262,(void*)0,&g_262,&g_1191,(void*)0}},{{&g_1191,(void*)0,(void*)0,&g_997,&g_997,(void*)0,(void*)0},{&g_262,&g_1191,(void*)0,&g_997,&g_262,&g_997,(void*)0},{&g_997,&g_1191,(void*)0,&g_1211,&g_1191,&g_1191,&g_1211}},{{&g_262,&g_997,&g_262,&g_997,(void*)0,&g_997,(void*)0},{&g_1191,&g_1211,(void*)0,&g_997,&g_1191,&g_262,&g_262},{&g_262,(void*)0,&g_997,(void*)0,&g_262,&g_997,&g_997}},{{&g_997,&g_1191,&g_1211,(void*)0,&g_997,&g_1191,&g_262},{(void*)0,&g_997,&g_262,&g_997,&g_262,&g_997,(void*)0},{&g_997,(void*)0,&g_262,&g_997,&g_1191,(void*)0,&g_1211}},{{&g_262,&g_997,(void*)0,&g_1191,&g_262,&g_1191,(void*)0},{&g_1191,&g_1191,&g_262,&g_1211,&g_997,&g_1191,(void*)0},{&g_262,(void*)0,&g_262,&g_1191,(void*)0,(void*)0,(void*)0}}};
    const int16_t **l_2511 = &l_2512[3][2][3];
    const int16_t ***l_2510 = &l_2511;
    const int16_t **** const l_2509 = &l_2510;
    const int16_t ****l_2514 = &l_2510;
    const int16_t *****l_2513 = &l_2514;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_2302[i] = &g_1004;
    for (i = 0; i < 10; i++)
        l_2403[i] = &g_292;
    for (g_514 = 21; (g_514 <= 9); g_514 = safe_sub_func_int32_t_s_s(g_514, 3))
    { /* block id: 1003 */
        int64_t *l_2226 = &g_1032[1][3][5];
        uint32_t *l_2234 = &g_1106;
        uint32_t **l_2233[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_2239 = 0xA2FEFF61L;
        float **l_2241 = (void*)0;
        int32_t l_2242[3];
        int32_t l_2243[5][5] = {{0x794D5D57L,1L,1L,0x794D5D57L,0x794D5D57L},{(-5L),2L,(-5L),2L,(-5L)},{0x794D5D57L,0x794D5D57L,1L,1L,0x794D5D57L},{0xEFC176A1L,2L,0xEFC176A1L,2L,0xEFC176A1L},{0x794D5D57L,1L,1L,0x794D5D57L,0x794D5D57L}};
        uint64_t ***l_2245[5][4][9] = {{{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{&g_1748,&g_1748,&g_1748,(void*)0,(void*)0,&g_1748,&g_1748,&g_1748,(void*)0},{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{&g_1748,(void*)0,(void*)0,&g_1748,&g_1748,&g_1748,(void*)0,(void*)0,&g_1748}},{{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{&g_1748,(void*)0,&g_1748,&g_1748,(void*)0,&g_1748,(void*)0,&g_1748,&g_1748},{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{(void*)0,(void*)0,(void*)0,&g_1748,&g_1748,(void*)0,(void*)0,(void*)0,&g_1748}},{{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{(void*)0,&g_1748,&g_1748,&g_1748,(void*)0,(void*)0,&g_1748,&g_1748,&g_1748},{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{(void*)0,(void*)0,&g_1748,&g_1748,&g_1748,(void*)0,(void*)0,&g_1748,&g_1748}},{{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{(void*)0,&g_1748,&g_1748,(void*)0,(void*)0,(void*)0,&g_1748,&g_1748,(void*)0},{&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748,&g_1748},{&g_1748,(void*)0,&g_1748,&g_1748,(void*)0,&g_1748,(void*)0,&g_1748,&g_1748}}};
        uint64_t **** const l_2244 = &l_2245[0][3][0];
        uint64_t ****l_2246[3];
        int8_t l_2301[9][8][3] = {{{0xBDL,0xDCL,1L},{(-1L),0x0EL,0x0EL},{0x0EL,(-2L),0x91L},{0x1CL,0xBCL,0x75L},{1L,(-9L),0xF0L},{1L,0xA0L,1L},{0x75L,(-9L),0xA4L},{0x1CL,0xBCL,0xEAL}},{{8L,(-2L),(-1L)},{0x2BL,0x0EL,0x85L},{1L,0xDCL,0xA4L},{(-1L),0x1CL,1L},{0x05L,0x92L,0x2EL},{1L,0L,0xD7L},{0x25L,0x0EL,0xF0L},{0xBDL,0xD3L,(-9L)}},{{0x1CL,4L,(-9L)},{0x91L,1L,0xF0L},{(-1L),0xB7L,0xD7L},{0x2BL,(-9L),0x2EL},{0xDCL,0x3DL,1L},{8L,9L,0xA4L},{(-9L),(-1L),0x85L},{(-1L),0L,(-1L)}},{{1L,0x1CL,0xEAL},{0x6BL,(-4L),0xA4L},{1L,0x1CL,1L},{0x87L,(-1L),0xF0L},{1L,(-1L),0x2EL},{0xA4L,1L,0xB7L},{0x1CL,0xA4L,(-1L)},{0x85L,8L,0x1CL}},{{0x47L,0xA0L,0L},{(-8L),0xBDL,0L},{0x91L,0x7CL,0xBCL},{0xA4L,0x87L,1L},{0x85L,(-1L),0x9DL},{0x0CL,0xD3L,(-9L)},{(-9L),0xD3L,0xACL},{0x87L,(-1L),0L}},{{(-9L),0x87L,(-1L)},{(-10L),0x7CL,0x47L},{(-1L),0xBDL,0xB7L},{(-6L),0xA0L,(-10L)},{4L,8L,0L},{0xA4L,0xA4L,8L},{(-8L),1L,0xD7L},{(-1L),(-1L),0xBCL}},{{0x2EL,0x25L,0xF0L},{4L,(-1L),0xBCL},{0x1CL,9L,0xD7L},{(-9L),(-9L),8L},{(-1L),1L,0L},{0x91L,0x25L,(-10L)},{0x87L,0x7CL,0xB7L},{0xA4L,0x0EL,0x47L}},{{(-6L),0x47L,(-1L)},{0x9DL,0xDCL,0L},{0x2EL,0xA0L,0xACL},{0x91L,1L,(-9L)},{0x91L,1L,0x9DL},{0x2EL,(-1L),1L},{0x9DL,1L,0xBCL},{(-6L),0xD3L,0L}},{{0xA4L,(-9L),0L},{0x87L,0x91L,0x1CL},{0x91L,(-1L),(-1L)},{(-1L),(-1L),0xB7L},{(-9L),0xBDL,0x2EL},{0x1CL,0x47L,0x2CL},{4L,0x1CL,0x1CL},{0x2EL,0x47L,8L}}};
        uint32_t ***l_2388[4] = {&l_2233[2],&l_2233[2],&l_2233[2],&l_2233[2]};
        uint32_t *** const *l_2387 = &l_2388[0];
        uint32_t *** const **l_2386 = &l_2387;
        int32_t **l_2392 = &l_2247;
        int32_t ***l_2391[2];
        uint8_t l_2395 = 1UL;
        int8_t l_2459 = 0x83L;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_2242[i] = 0x010D5A0BL;
        for (i = 0; i < 3; i++)
            l_2246[i] = &l_2245[0][3][0];
        for (i = 0; i < 2; i++)
            l_2391[i] = &l_2392;
    }
    (*l_2513) = l_2509;
    return (*g_292);
}


/* ------------------------------------------ */
/* 
 * reads : g_25 g_70 g_67 g_79 g_1003 g_304 g_825 g_44 g_198 g_1032 g_138 g_51 g_143 g_1002 g_1004 g_1041 g_121 g_540 g_1747 g_1762 g_1159 g_1191 g_633 g_292 g_161 g_1279 g_263 g_1842 g_1748 g_1646 g_1136 g_1158
 * writes: g_70 g_79 g_1004 g_262 g_1032 g_1041 g_121 g_67 g_1747 g_51 g_44 g_161 g_785 g_825 g_304 g_497 g_187 g_263 g_1822 g_1824 g_1842 g_1543 g_1136 g_540
 */
static uint64_t  func_39(uint8_t  p_40, const uint8_t  p_41, int32_t  p_42)
{ /* block id: 4 */
    uint64_t *l_64 = &g_51;
    int32_t l_65 = 0xBE663439L;
    int32_t l_66 = 2L;
    uint32_t *l_68 = (void*)0;
    uint32_t *l_69[3][2][1] = {{{&g_70[0]},{&g_70[3]}},{{&g_70[0]},{&g_70[3]}},{{&g_70[0]},{&g_70[3]}}};
    int32_t l_71[1][6] = {{0x9BA4AEFCL,0L,0x9BA4AEFCL,0x9BA4AEFCL,0L,0x9BA4AEFCL}};
    uint8_t **l_1807 = &g_825;
    uint8_t ****l_1847 = &g_1543[1][0];
    uint32_t ** const *l_1887 = &g_1822[1][0][0];
    float l_1888 = 0xE.083BACp+9;
    int8_t *l_1911 = (void*)0;
    uint64_t l_2012 = 0xFDF9F9D4FCBE7CA3LL;
    const int64_t **l_2085 = &g_2083;
    uint32_t l_2104 = 0xDE6E3D10L;
    uint16_t **l_2137[1];
    uint16_t ***l_2136 = &l_2137[0];
    uint16_t l_2151 = 0x5A52L;
    uint32_t *l_2178 = &g_463[0];
    uint32_t **l_2177 = &l_2178;
    uint32_t ***l_2176 = &l_2177;
    uint32_t ****l_2175 = &l_2176;
    int16_t *l_2181[7][7];
    uint64_t l_2192 = 4UL;
    int32_t ** const **l_2194[10][3][1] = {{{(void*)0},{&g_1158},{&g_1158}},{{(void*)0},{(void*)0},{(void*)0}},{{&g_1158},{&g_1158},{(void*)0}},{{&g_1158},{(void*)0},{&g_1158}},{{&g_1158},{(void*)0},{(void*)0}},{{(void*)0},{&g_1158},{&g_1158}},{{(void*)0},{(void*)0},{(void*)0}},{{&g_1158},{&g_1158},{(void*)0}},{{&g_1158},{(void*)0},{&g_1158}},{{&g_1158},{(void*)0},{(void*)0}}};
    int32_t ** const ***l_2193 = &l_2194[2][0][0];
    uint64_t l_2195[10] = {18446744073709551606UL,1UL,18446744073709551606UL,18446744073709551606UL,1UL,18446744073709551606UL,18446744073709551606UL,1UL,18446744073709551606UL,18446744073709551606UL};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_2137[i] = &g_187[0][3][1];
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
            l_2181[i][j] = &g_119;
    }
    l_65 &= (safe_div_func_uint16_t_u_u((0x3FA9A180L < g_25), (func_58(((*l_1807) = func_60(l_64, (g_70[5]++), g_67[3][5][1]))) & ((l_66 = (safe_mul_func_int8_t_s_s(l_71[0][4], (&l_1807 != ((*l_1847) = &g_1544[3][7][2]))))) ^ (-10L)))));
    if (p_41)
    { /* block id: 827 */
        int32_t l_1852 = 0L;
        int8_t *l_1859 = &g_121;
        int32_t l_1866 = 0x3ABF688AL;
        uint32_t l_1869 = 0UL;
        uint16_t *l_1870 = &g_79;
        uint16_t *l_1871 = &g_263;
        int32_t *l_1886 = &g_67[1][6][0];
        if ((safe_mod_func_uint16_t_u_u((((safe_rshift_func_int8_t_s_u(((--(**g_1748)) < (((*l_1859) = (safe_add_func_int32_t_s_s(p_40, (safe_add_func_uint32_t_u_u(9UL, p_41))))) >= p_40)), 1)) , (safe_add_func_int8_t_s_s(p_40, (-5L)))) >= (((*l_1871) = ((*l_1870) = (safe_mod_func_uint32_t_u_u(((safe_mul_func_int8_t_s_s(l_1852, (l_1866 ^= p_42))) || ((((l_1866 = (safe_div_func_uint32_t_u_u((p_41 < (*g_825)), l_1869))) , 4294967292UL) > p_40) | p_41)), (-1L))))) || 65534UL)), 0x99CAL)))
        { /* block id: 834 */
            uint16_t ****l_1878[6][10][4] = {{{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378}},{{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378}},{{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378}},{{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378}},{{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378}},{{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378},{&g_378,&g_378,&g_378,&g_378}}};
            uint16_t *****l_1877 = &l_1878[0][7][3];
            int32_t l_1885 = 0x291FBBE5L;
            int i, j, k;
            for (g_1136 = 20; (g_1136 <= (-3)); g_1136--)
            { /* block id: 837 */
                uint16_t *****l_1879 = &l_1878[2][9][0];
                int16_t *l_1882[1][3];
                int32_t l_1883 = 0xA046CF7AL;
                int32_t l_1884 = 0L;
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_1882[i][j] = &g_1211;
                }
                (*g_540) = (+(safe_div_func_uint8_t_u_u((0x0FL & ((l_1877 == l_1879) != (p_40 || ((((l_1883 = (l_1866 , (safe_sub_func_int64_t_s_s(((((*g_292) = l_1869) < 0x7.A12F87p-18) , 0L), p_40)))) <= 0xEE9DL) != l_1884) == (***g_1747))))), p_41)));
            }
            l_1886 = (l_1885 , (**g_1158));
        }
        else
        { /* block id: 843 */
            (*g_540) = (l_1887 != &g_1822[2][0][0]);
        }
        (*g_540) ^= 0x01D30EFAL;
        (*g_1159) = (void*)0;
    }
    else
    { /* block id: 848 */
        uint32_t *l_1899 = (void*)0;
        uint32_t **l_1900 = (void*)0;
        int32_t l_1909[4][10];
        int8_t *l_1910 = (void*)0;
        int8_t **l_1912 = &g_1004;
        int8_t *l_1913[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t **l_1914 = &g_540;
        uint32_t * const *l_1916 = &l_68;
        uint32_t * const **l_1915 = &l_1916;
        int64_t l_1936 = 0x0DEDFDD19C7DD4B0LL;
        uint16_t *l_1955[8][8] = {{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263},{&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263}};
        int32_t l_2068[4] = {0xACF16FAFL,0xACF16FAFL,0xACF16FAFL,0xACF16FAFL};
        int64_t l_2069 = 0xF9CE4C8BB38C0F79LL;
        int32_t *l_2077 = &l_1909[3][5];
        uint32_t ** const **l_2123 = (void*)0;
        int32_t l_2131 = 0x92A305A8L;
        uint16_t l_2188 = 0xA613L;
        uint8_t *** const *l_2191 = &g_1543[1][7];
        int i, j;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 10; j++)
                l_1909[i][j] = 0x06401968L;
        }
        l_71[0][3] = (((*g_292) = l_1888) >= (safe_sub_func_float_f_f(l_66, (safe_sub_func_float_f_f((safe_sub_func_float_f_f(((safe_mod_func_uint16_t_u_u((safe_add_func_int64_t_s_s(p_42, (g_1646[3] < (((((((l_1899 = l_1899) == &g_1328[0]) & p_41) || ((1L != (***g_1747)) <= 0x30L)) , 0xE41C7189L) <= p_41) != 1UL)))), g_1041)) , p_40), p_40)), (-0x1.9p-1))))));
    }
    (*l_2193) = &g_1158;
    return l_2195[8];
}


/* ------------------------------------------ */
/* 
 * reads : g_1279 g_263 g_1842 g_1159 g_540 g_67
 * writes: g_304 g_1041 g_497 g_187 g_263 g_1822 g_1824 g_1842
 */
static const int32_t  func_58(uint8_t * p_59)
{ /* block id: 805 */
    uint32_t l_1812 = 0xA20407CAL;
    int16_t *l_1813[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_1814 = 0x97B69CFBL;
    uint16_t *l_1815 = &g_263;
    uint32_t *l_1820 = &l_1812;
    uint32_t **l_1819 = &l_1820;
    uint32_t ***l_1821[1];
    int8_t * const *l_1829 = &g_1004;
    uint32_t *l_1833 = &g_463[1];
    uint32_t **l_1832 = &l_1833;
    uint32_t ***l_1831[2];
    int32_t l_1837 = 0xCF7EA3F2L;
    int32_t l_1838 = 0L;
    int32_t l_1839[8][7][4] = {{{(-1L),(-8L),(-1L),0x1ED65784L},{0xF02102BDL,4L,0x79A6A248L,1L},{0x2AABDBD7L,(-4L),(-3L),1L},{0x83382CDFL,4L,0x100C0870L,0x1ED65784L},{7L,(-8L),0xF02102BDL,0x2897FEE7L},{9L,0x82782594L,0xDAD9C89DL,0x82782594L},{0xF02102BDL,(-1L),(-8L),0xB21372E8L}},{{8L,(-4L),0xA45DF055L,0x984E3BE6L},{7L,0x6DCC86EBL,0x100C0870L,0x54994D66L},{7L,0x1ED65784L,0xA45DF055L,0x2897FEE7L},{8L,0x54994D66L,(-8L),4L},{0xF02102BDL,8L,0xDAD9C89DL,(-5L)},{9L,(-4L),0xF02102BDL,(-2L)},{7L,(-1L),0x100C0870L,(-8L)}},{{0x83382CDFL,4L,(-3L),0x2897FEE7L},{0x2AABDBD7L,4L,0x79A6A248L,(-8L)},{0xF02102BDL,(-1L),1L,4L},{0xA45DF055L,4L,0x100C0870L,8L},{1L,(-4L),1L,(-1L)},{(-1L),1L,0x83382CDFL,1L},{(-3L),0xF14DB825L,0L,1L}},{{7L,1L,0L,(-1L)},{(-3L),4L,0x83382CDFL,0x6DCC86EBL},{(-1L),0L,1L,0x2897FEE7L},{1L,0x2897FEE7L,0x100C0870L,1L},{0xA45DF055L,0L,1L,0xF14DB825L},{7L,0xB1DAA7BCL,0x16D2C4F5L,(-1L)},{0x3FB7E861L,4L,7L,(-1L)}},{{0xDAD9C89DL,0xB1DAA7BCL,1L,0xF14DB825L},{0x79A6A248L,0L,7L,1L},{0xF02102BDL,0x2897FEE7L,(-8L),0x2897FEE7L},{7L,0L,(-9L),0x6DCC86EBL},{1L,4L,1L,(-1L)},{(-8L),1L,1L,1L},{(-8L),0xF14DB825L,1L,1L}},{{1L,1L,(-9L),(-1L)},{7L,(-4L),(-8L),8L},{0xF02102BDL,4L,7L,4L},{0x79A6A248L,0xE31CCE23L,1L,0L},{0xDAD9C89DL,(-1L),7L,1L},{0x3FB7E861L,(-1L),0x16D2C4F5L,0L},{7L,0xE31CCE23L,1L,4L}},{{0xA45DF055L,4L,0x100C0870L,8L},{1L,(-4L),1L,(-1L)},{(-1L),1L,0x83382CDFL,1L},{(-3L),0xF14DB825L,0L,1L},{7L,1L,0L,(-1L)},{(-3L),4L,0x83382CDFL,0x6DCC86EBL},{(-1L),0L,1L,0x2897FEE7L}},{{1L,0x2897FEE7L,0x100C0870L,1L},{0xA45DF055L,0L,1L,0xF14DB825L},{7L,0xB1DAA7BCL,0x16D2C4F5L,(-1L)},{0x3FB7E861L,4L,7L,(-1L)},{0xDAD9C89DL,0xB1DAA7BCL,1L,0xF14DB825L},{0x79A6A248L,0L,7L,1L},{0xF02102BDL,0x2897FEE7L,(-8L),0x2897FEE7L}}};
    int32_t *l_1840 = &l_1838;
    int32_t *l_1841[2][6][2] = {{{&l_1839[7][5][2],&l_1838},{&l_1814,&l_1837},{(void*)0,&l_1814},{&l_1837,&l_1838},{&l_1837,&l_1814},{(void*)0,&l_1837}},{{&l_1814,&l_1838},{&l_1839[7][5][2],&l_1839[7][5][2]},{(void*)0,&l_1839[7][5][2]},{&l_1839[7][5][2],&l_1838},{&l_1814,&l_1837},{(void*)0,&l_1814}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1821[i] = &l_1819;
    for (i = 0; i < 2; i++)
        l_1831[i] = &l_1832;
    for (g_304 = 0; g_304 < 1; g_304 += 1)
    {
        for (g_1041 = 0; g_1041 < 6; g_1041 += 1)
        {
            for (g_497 = 0; g_497 < 8; g_497 += 1)
            {
                g_187[g_304][g_1041][g_497] = &g_263;
            }
        }
    }
lbl_1836:
    if (((safe_unary_minus_func_int16_t_s((((((~(safe_div_func_int8_t_s_s(((l_1814 = (l_1812 || 0L)) != g_1279), l_1812))) , ((((*l_1815)--) , l_1812) & (+((g_1822[7][0][0] = l_1819) != (g_1824 = &g_1823[1]))))) && (safe_sub_func_uint8_t_u_u(1UL, (safe_rshift_func_int8_t_s_s(((l_1829 == (void*)0) && 0x995658D2L), 1))))) > l_1812) , 5L))) , l_1814))
    { /* block id: 811 */
        const uint8_t l_1830[8] = {255UL,255UL,255UL,255UL,255UL,255UL,255UL,255UL};
        int i;
        return l_1830[6];
    }
    else
    { /* block id: 813 */
        int32_t l_1835 = 0x8BCA33ACL;
        for (g_263 = 0; (g_263 <= 0); g_263 += 1)
        { /* block id: 816 */
            uint32_t ****l_1834 = &l_1831[0];
            (*l_1834) = l_1831[0];
            if (l_1835)
                continue;
            if (l_1835)
                goto lbl_1836;
        }
    }
    ++g_1842;
    return (**g_1159);
}


/* ------------------------------------------ */
/* 
 * reads : g_70 g_79 g_67 g_1003 g_304 g_825 g_44 g_198 g_1032 g_138 g_51 g_143 g_1002 g_1004 g_1041 g_121 g_540 g_1747 g_1762 g_1159 g_1191 g_633 g_292 g_161
 * writes: g_79 g_1004 g_262 g_1032 g_1041 g_121 g_67 g_1747 g_51 g_44 g_161 g_785
 */
static uint8_t * func_60(uint64_t * p_61, uint32_t  p_62, const int64_t  p_63)
{ /* block id: 6 */
    uint16_t *l_78 = &g_79;
    int32_t l_106 = 6L;
    uint8_t * const l_115 = &g_44;
    int32_t l_141 = 0x02D43064L;
    int32_t l_142[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    int8_t l_152 = 0xAFL;
    uint8_t *l_199 = &g_44;
    uint16_t ***l_230 = (void*)0;
    int32_t *l_239 = &g_67[4][0][1];
    int32_t **l_238[10][6] = {{&l_239,&l_239,(void*)0,&l_239,&l_239,&l_239},{&l_239,&l_239,&l_239,&l_239,&l_239,&l_239},{&l_239,(void*)0,&l_239,&l_239,&l_239,&l_239},{&l_239,&l_239,&l_239,&l_239,&l_239,&l_239},{&l_239,&l_239,&l_239,&l_239,&l_239,&l_239},{&l_239,&l_239,&l_239,&l_239,(void*)0,&l_239},{&l_239,&l_239,&l_239,&l_239,&l_239,&l_239},{&l_239,&l_239,&l_239,&l_239,&l_239,&l_239},{&l_239,&l_239,&l_239,&l_239,(void*)0,&l_239},{&l_239,&l_239,&l_239,&l_239,&l_239,&l_239}};
    float **l_328 = &g_292;
    uint64_t l_335 = 18446744073709551608UL;
    int8_t **l_475 = (void*)0;
    float l_526 = (-0x6.0p-1);
    int64_t l_635 = 0L;
    uint16_t *l_645[4][2][7] = {{{&g_79,&g_79,&g_79,(void*)0,&g_263,(void*)0,&g_263},{(void*)0,&g_263,&g_263,&g_79,&g_263,&g_79,&g_79}},{{&g_263,(void*)0,&g_263,&g_263,(void*)0,&g_263,&g_263},{&g_263,(void*)0,(void*)0,&g_263,&g_263,&g_263,(void*)0}},{{(void*)0,&g_79,(void*)0,&g_263,&g_79,&g_263,&g_263},{&g_79,(void*)0,&g_79,&g_79,&g_79,&g_79,(void*)0}},{{&g_263,(void*)0,&g_79,&g_263,&g_263,&g_79,(void*)0},{(void*)0,&g_263,(void*)0,&g_79,&g_263,&g_79,&g_263}}};
    uint16_t ** const l_644 = &l_645[0][1][0];
    uint16_t ** const *l_643 = &l_644;
    uint16_t ** const **l_642[7][4][9] = {{{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{(void*)0,(void*)0,&l_643,&l_643,&l_643,(void*)0,(void*)0,&l_643,&l_643},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,(void*)0}},{{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643}},{{(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,(void*)0},{&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643}},{{(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,(void*)0,&l_643,(void*)0,&l_643,&l_643,(void*)0},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643}},{{&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,(void*)0},{&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643}},{{&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,(void*)0},{&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643},{&l_643,&l_643,(void*)0,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643},{&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643}},{{&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643},{&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643},{&l_643,(void*)0,&l_643,&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643},{&l_643,&l_643,(void*)0,&l_643,&l_643,&l_643,&l_643,&l_643,&l_643}}};
    uint16_t ** const ***l_641 = &l_642[5][2][1];
    int8_t *l_663 = &l_152;
    uint32_t l_753 = 0x0F8C446FL;
    uint32_t l_820 = 1UL;
    int32_t l_821 = 0x7A3447CAL;
    uint64_t *l_918 = &l_335;
    uint16_t l_985 = 0x4F29L;
    uint32_t l_988 = 0x05CD761FL;
    uint64_t l_1049 = 18446744073709551615UL;
    uint8_t l_1193 = 0xECL;
    int64_t l_1214 = 0x0662A45AEE899817LL;
    uint32_t l_1289 = 0x23182399L;
    int16_t l_1343 = 1L;
    int32_t l_1364[7];
    float l_1401 = (-0x1.Dp-1);
    uint8_t ****l_1648[8][4] = {{(void*)0,&g_1543[0][5],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1543[0][5]},{(void*)0,&g_1543[0][6],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1543[0][5],(void*)0},{(void*)0,&g_1543[0][6],&g_1543[0][5],&g_1543[0][5]},{(void*)0,(void*)0,(void*)0,&g_1543[0][5]},{(void*)0,&g_1543[0][6],(void*)0,(void*)0}};
    int32_t l_1688[1][5][5] = {{{0xD689A0AFL,0L,0L,0L,0L},{0L,0L,0xD689A0AFL,0L,0L},{0x20D87C8CL,0L,0x16342E17L,0L,0x20D87C8CL},{0xD689A0AFL,5L,0L,0L,5L},{0x20D87C8CL,0L,0L,0x20D87C8CL,0L}}};
    int8_t l_1715[2];
    int64_t l_1729 = 0L;
    uint32_t l_1741 = 18446744073709551609UL;
    uint64_t ***l_1750 = (void*)0;
    int16_t l_1754 = 8L;
    int16_t *l_1801 = &l_1754;
    uint32_t l_1804[2];
    uint8_t *l_1806 = &g_1646[6];
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1364[i] = 1L;
    for (i = 0; i < 2; i++)
        l_1715[i] = (-3L);
    for (i = 0; i < 2; i++)
        l_1804[i] = 0x33839096L;
lbl_1092:
    if ((safe_add_func_uint16_t_u_u(((*l_78) &= ((safe_mul_func_int8_t_s_s(p_63, 0x9AL)) , (0x26471B2AL != (g_70[3] >= (p_62 || g_70[5]))))), 0xE6FCL)))
    { /* block id: 8 */
        uint32_t l_80 = 0UL;
        uint8_t *l_116 = &g_44;
        int16_t *l_118 = &g_119;
        int8_t *l_120 = &g_121;
        int16_t l_123 = 1L;
        int32_t l_126 = 0x2CBA3753L;
        uint64_t *l_137 = &g_51;
        int32_t l_144 = (-5L);
        int32_t l_145 = 7L;
        int32_t l_146 = 0x4216E3A4L;
        int32_t l_149 = 2L;
        int32_t l_154 = (-6L);
        int32_t l_155 = 0xDB775658L;
        uint32_t l_162[1];
        uint16_t **l_225 = (void*)0;
        uint16_t ***l_224[5] = {&l_225,&l_225,&l_225,&l_225,&l_225};
        const uint32_t l_231 = 0xD96624DCL;
        uint8_t l_294 = 0x9AL;
        const float *l_302[1];
        uint64_t l_349 = 0x1C3CD1D03B48941ELL;
        int32_t **l_386 = &l_239;
        uint32_t l_503 = 0xB2381BBBL;
        int16_t l_518 = 1L;
        int32_t l_525[4][10][6] = {{{0L,0xDCB62E05L,(-1L),0x2E04A297L,1L,0x2E04A297L},{0x474555E4L,1L,0x474555E4L,3L,(-1L),(-1L)},{1L,0x51A105BEL,0x8304AC09L,(-7L),0xDCB62E05L,(-5L)},{0xD6824F76L,(-1L),0x4BDA8C91L,(-7L),0x0B4D2265L,3L},{1L,0xD6824F76L,1L,3L,(-5L),1L},{0x474555E4L,(-1L),0xDCB62E05L,0x2E04A297L,1L,0x344C95DAL},{0L,0x4D8F7091L,0xB41D5E5CL,(-1L),(-1L),0xB41D5E5CL},{0xD50689ADL,0xD50689ADL,(-1L),(-1L),0xCFF6984CL,(-5L)},{0x8304AC09L,1L,1L,1L,(-9L),(-1L)},{1L,0x8304AC09L,1L,(-1L),0xD50689ADL,(-5L)}},{{0x2E04A297L,(-1L),(-1L),0x344C95DAL,0x0B4D2265L,0xB41D5E5CL},{0x344C95DAL,0x0B4D2265L,0xB41D5E5CL,3L,0xD6824F76L,0x344C95DAL},{0x51A105BEL,1L,0xDCB62E05L,(-1L),0xDCB62E05L,1L},{0xDCB62E05L,0x4D8F7091L,1L,0x4BDA8C91L,(-9L),3L},{0xD50689ADL,0xDCB62E05L,0x4BDA8C91L,0xD6824F76L,0x28889A25L,(-5L)},{0x474555E4L,0xDCB62E05L,0x8304AC09L,0xB41D5E5CL,(-9L),(-1L)},{0x344C95DAL,0x4D8F7091L,0x474555E4L,(-1L),0xDCB62E05L,0x2E04A297L},{(-5L),1L,(-1L),(-7L),0xD6824F76L,0x2843755CL},{(-1L),0x0B4D2265L,1L,1L,0x0B4D2265L,(-1L)},{0x51A105BEL,(-1L),0x1A14CBB5L,0xD6824F76L,0xD50689ADL,1L}},{{0L,0x8304AC09L,3L,(-1L),(-9L),0xB41D5E5CL},{0L,1L,(-1L),0xD6824F76L,0xCFF6984CL,0x2E04A297L},{0xD6824F76L,0xCFF6984CL,0x2E04A297L,(-9L),0L,0xDCB62E05L},{0x474555E4L,(-5L),0x0B4D2265L,0x4D8F7091L,0x28889A25L,0xB41D5E5CL},{3L,0x474555E4L,1L,0x474555E4L,3L,(-1L)},{1L,0xB41D5E5CL,(-1L),0x4BDA8C91L,0x2843755CL,1L},{(-1L),0x51A105BEL,0x9B7276B0L,0xB41D5E5CL,0x335D5B3CL,1L},{0xCFF6984CL,0xD6824F76L,(-1L),1L,0L,(-1L)},{0x335D5B3CL,0x28889A25L,1L,6L,1L,0xB41D5E5CL},{0xD6824F76L,0x335D5B3CL,0x0B4D2265L,(-1L),0xDCB62E05L,0xDCB62E05L}},{{1L,0x2E04A297L,0x2E04A297L,1L,0x28889A25L,1L},{1L,0x51A105BEL,0x1A14CBB5L,0x474555E4L,0xB41D5E5CL,0x4BDA8C91L},{0x8304AC09L,3L,(-1L),(-9L),0xB41D5E5CL,0x8304AC09L},{0x2E04A297L,0x51A105BEL,1L,6L,0x28889A25L,0x474555E4L},{0xCFF6984CL,0x2E04A297L,(-9L),0L,0xDCB62E05L,(-1L)},{0x9B7276B0L,0x335D5B3CL,0L,1L,1L,1L},{(-1L),0x28889A25L,(-1L),(-1L),0L,0x1A14CBB5L},{0x8304AC09L,0xD6824F76L,0x2E04A297L,0x4D8F7091L,0x335D5B3CL,3L},{0xB41D5E5CL,0x51A105BEL,1L,0x4D8F7091L,0x2843755CL,(-1L)},{0x8304AC09L,0xB41D5E5CL,(-9L),(-1L),3L,0x8304AC09L}}};
        uint32_t l_531 = 0xD20586ADL;
        uint16_t **l_592 = (void*)0;
        uint16_t *** const **l_648 = &g_647;
        uint32_t *l_649[2];
        int8_t **l_662[7][9] = {{&l_120,&l_120,(void*)0,(void*)0,&l_120,(void*)0,(void*)0,&l_120,&l_120},{&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120},{(void*)0,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120},{&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120},{&l_120,&l_120,&l_120,&l_120,(void*)0,(void*)0,&l_120,(void*)0,(void*)0},{&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,&l_120},{&l_120,&l_120,&l_120,&l_120,&l_120,&l_120,(void*)0,&l_120,(void*)0}};
        int32_t *l_681 = &l_154;
        const float * const *l_705[3][3][6] = {{{&l_302[0],&l_302[0],&l_302[0],&l_302[0],&l_302[0],&l_302[0]},{&l_302[0],&l_302[0],&l_302[0],&l_302[0],&l_302[0],&l_302[0]},{&l_302[0],&l_302[0],&l_302[0],&l_302[0],&l_302[0],&l_302[0]}},{{&l_302[0],&l_302[0],(void*)0,(void*)0,&l_302[0],(void*)0},{(void*)0,&l_302[0],(void*)0,(void*)0,&l_302[0],(void*)0},{(void*)0,&l_302[0],(void*)0,(void*)0,&l_302[0],(void*)0}},{{(void*)0,&l_302[0],(void*)0,(void*)0,&l_302[0],(void*)0},{(void*)0,&l_302[0],(void*)0,(void*)0,&l_302[0],(void*)0},{(void*)0,&l_302[0],(void*)0,(void*)0,&l_302[0],(void*)0}}};
        const float * const **l_704 = &l_705[1][0][1];
        uint16_t **l_730 = (void*)0;
        const uint32_t l_754 = 0UL;
        uint8_t *l_761 = &g_44;
        int32_t l_822 = 0L;
        int8_t l_866 = 0xEAL;
        uint32_t l_1006 = 6UL;
        uint8_t l_1010 = 0xBCL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_162[i] = 0x924F713FL;
        for (i = 0; i < 1; i++)
            l_302[i] = &g_161;
        for (i = 0; i < 2; i++)
            l_649[i] = &g_497;
        l_80 &= (-4L);
    }
    else
    { /* block id: 376 */
        int8_t *l_1020 = &l_152;
        int8_t **l_1021 = &l_663;
        int16_t l_1024[10][3] = {{0xADE5L,0xC6B9L,0L},{0L,0xC6B9L,0L},{0x5F78L,0xA99FL,0x18EDL},{0x99F8L,0x5F78L,0x5F78L},{0x5F78L,0x26D5L,0x577DL},{0L,0x7C90L,0x577DL},{0xADE5L,0x577DL,0x5F78L},{1L,0L,0x18EDL},{0x577DL,0x577DL,0L},{0xA99FL,0x7C90L,0L}};
        int32_t l_1027 = 1L;
        int16_t *l_1029 = &g_262;
        uint32_t l_1030[5] = {0x626660E3L,0x626660E3L,0x626660E3L,0x626660E3L,0x626660E3L};
        uint8_t *l_1031[7] = {&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44};
        int16_t *l_1040 = &l_1024[3][1];
        int i, j;
        l_1027 = ((~((safe_lshift_func_int16_t_s_s(((0L != ((*l_239) <= (((safe_rshift_func_uint16_t_u_u(((*l_78) = (((*g_1003) = l_1020) == ((*l_1021) = l_1020))), 1)) & (safe_mod_func_int8_t_s_s(l_1024[0][2], 1L))) == ((p_63 , l_1024[3][1]) , ((safe_lshift_func_uint8_t_u_u(l_1024[0][2], p_62)) == p_63))))) >= 0x60E0236AB3481E1ALL), g_304)) & 0xF95B6C26L)) > (*g_825));
        g_1041 &= ((!((*l_1029) = g_198)) , (((((*l_1029) = l_1030[1]) != (*l_239)) < (g_1032[1][3][1] |= (*g_825))) | (((safe_rshift_func_int16_t_s_s((((safe_div_func_uint32_t_u_u(((l_1027 ^= p_63) ^ p_62), (((safe_sub_func_uint8_t_u_u(((0UL <= (*g_138)) < (((+((*l_1040) = 0x2FFCL)) != p_63) , g_143)), (***g_1002))) > l_1030[1]) && l_1030[1]))) , g_79) , (-1L)), 14)) >= p_62) >= (***g_1002))));
    }
    for (p_62 = (-28); (p_62 < 48); p_62 = safe_add_func_uint16_t_u_u(p_62, 9))
    { /* block id: 390 */
        uint8_t l_1044 = 1UL;
        int32_t l_1052 = 0x3BE0E856L;
        int64_t *l_1053 = &g_514;
        int16_t *l_1054 = &g_262;
        uint16_t **l_1061 = &l_645[1][0][0];
        int32_t l_1105[3];
        int32_t *l_1109[4];
        int32_t *l_1162[7] = {&l_141,&l_141,&l_141,&l_141,&l_141,&l_141,&l_141};
        int32_t ** const l_1161 = &l_1162[0];
        int32_t ** const *l_1160[3];
        int8_t *l_1196 = &l_152;
        uint16_t ** const ***l_1227 = (void*)0;
        uint8_t l_1239 = 0x0AL;
        int8_t ** const *l_1310 = &l_475;
        uint64_t l_1349[10][1][4] = {{{18446744073709551615UL,0xCB83EC34202BA007LL,18446744073709551611UL,18446744073709551611UL}},{{18446744073709551615UL,18446744073709551615UL,0xD8A5D63B267D6E1ALL,0x3DA08123F095534FLL}},{{0x64450F8C42F41D1BLL,0xD8A5D63B267D6E1ALL,0x2793020D2D93B59BLL,3UL}},{{18446744073709551611UL,4UL,0x9C6B05475596FD8ELL,0x2793020D2D93B59BLL}},{{0x2793020D2D93B59BLL,4UL,18446744073709551615UL,3UL}},{{4UL,0xD8A5D63B267D6E1ALL,4UL,0x3DA08123F095534FLL}},{{0xCB83EC34202BA007LL,18446744073709551615UL,0x8C5061EDB6A0FB9CLL,18446744073709551611UL}},{{1UL,0xCB83EC34202BA007LL,0x3DA08123F095534FLL,18446744073709551615UL}},{{3UL,1UL,0x3DA08123F095534FLL,0xD01CAC32DA8A12CFLL}},{{1UL,0x8C5061EDB6A0FB9CLL,0x8C5061EDB6A0FB9CLL,1UL}}};
        uint32_t l_1376 = 0x8DCF85A6L;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1105[i] = (-1L);
        for (i = 0; i < 4; i++)
            l_1109[i] = &l_106;
        for (i = 0; i < 3; i++)
            l_1160[i] = &l_1161;
    }
    for (l_985 = 0; (l_985 == 56); ++l_985)
    { /* block id: 585 */
        int16_t l_1388 = 0x9533L;
        int8_t l_1418 = 0x9EL;
        int32_t l_1433 = (-9L);
        float l_1434[10][3] = {{0x1.0p-1,0x9.0p-1,0x1.0p-1},{0x0.6p-1,0x7.501EB6p-21,0x0.8p+1},{0x0.6p-1,0x0.6p-1,0x7.501EB6p-21},{0x1.0p-1,0x7.501EB6p-21,0x7.501EB6p-21},{0x7.501EB6p-21,0x9.0p-1,0x0.8p+1},{0x1.0p-1,0x9.0p-1,0x1.0p-1},{0x0.6p-1,0x7.501EB6p-21,0x0.8p+1},{0x0.6p-1,0x0.6p-1,0x7.501EB6p-21},{0x1.0p-1,0x7.501EB6p-21,0x7.501EB6p-21},{0x7.501EB6p-21,0x9.0p-1,0x0.8p+1}};
        uint64_t **l_1447 = &l_918;
        uint8_t *l_1456 = &g_44;
        int8_t l_1556[5];
        int32_t l_1563 = (-1L);
        int32_t l_1600 = 0x10C1F1E1L;
        int32_t l_1603 = 0xD8B0B52DL;
        int32_t l_1604 = (-1L);
        int16_t l_1635 = 3L;
        const uint32_t *l_1642[6];
        int32_t l_1692 = 0L;
        int32_t l_1694 = (-10L);
        int32_t l_1697 = 0x409C9F22L;
        int32_t l_1701 = (-10L);
        int32_t l_1705 = 0L;
        int32_t l_1709 = 9L;
        int32_t l_1711 = 0xAC6B477FL;
        int64_t l_1712 = (-10L);
        int32_t l_1713 = 0x4A2CBD1AL;
        int32_t l_1714 = 0xE244A7BBL;
        int32_t l_1716 = 0x1C7ED693L;
        int32_t l_1717 = 1L;
        int32_t l_1718 = 3L;
        int32_t l_1719 = 8L;
        int32_t l_1722 = (-4L);
        int32_t l_1728 = 0xD4377FE7L;
        int32_t l_1730 = 0xB8959404L;
        int32_t l_1731[1][4];
        int8_t l_1734 = 6L;
        int16_t l_1737 = 1L;
        uint32_t *l_1744 = &l_753;
        uint64_t ** const **l_1749 = &g_1747;
        uint64_t ****l_1751 = &l_1750;
        int i, j;
        for (i = 0; i < 5; i++)
            l_1556[i] = (-7L);
        for (i = 0; i < 6; i++)
            l_1642[i] = &g_1106;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 4; j++)
                l_1731[i][j] = 0L;
        }
        for (g_121 = 1; (g_121 <= 5); g_121 += 1)
        { /* block id: 588 */
            int64_t l_1387 = 0x85C01F0D7070B7EDLL;
            uint8_t **l_1407 = &g_825;
            uint8_t ***l_1406 = &l_1407;
            int32_t *l_1420 = &l_1364[2];
            int64_t l_1476 = 0x0AE8C518CBB4C6D3LL;
            uint16_t **l_1495 = &g_187[0][0][1];
            uint16_t **l_1496[10] = {(void*)0,&l_645[0][1][0],&l_645[0][1][0],(void*)0,&l_645[0][1][0],&l_645[0][1][0],(void*)0,&l_645[0][1][0],&l_645[0][1][0],(void*)0};
            uint16_t ****l_1534 = &g_378;
            uint16_t *****l_1533 = &l_1534;
            int32_t l_1565 = 0x006BBF7EL;
            uint8_t l_1595 = 1UL;
            int32_t l_1598 = 4L;
            uint32_t *l_1661 = &l_1289;
            int32_t ** const **l_1680 = &g_1158;
            int32_t l_1684[1][9] = {{0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L,0xFDBC5D22L}};
            int32_t l_1725 = (-5L);
            uint32_t l_1738 = 18446744073709551609UL;
            int i, j;
        }
        l_1741++;
        g_1041 ^= (((*g_540) = (-1L)) < ((-10L) || (((--(*l_1744)) , (((*l_1749) = g_1747) != ((*l_1751) = l_1750))) && (safe_add_func_int8_t_s_s(8L, (p_62 || ((((l_1754 <= (safe_sub_func_int16_t_s_s((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int8_t_s_s((0x9BL > (+((0x6BBBL | 0xD276L) && l_1734))), 6)) & g_1762), l_1600)), 0x79FDL))) > p_62) && p_63) | p_62)))))));
        if (g_198)
            goto lbl_1092;
    }
    for (p_62 = 0; (p_62 > 50); p_62++)
    { /* block id: 788 */
        uint16_t l_1786[9][7][2] = {{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}},{{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL},{5UL,5UL}}};
        int64_t **l_1802 = &g_785[0];
        int8_t l_1803[7][6] = {{0xC4L,8L,0xC4L,(-1L),8L,0xC4L},{0xC4L,1L,(-1L),(-1L),1L,0xC4L},{0xC4L,(-9L),0xC4L,(-1L),(-9L),(-1L)},{0xC4L,8L,0xC4L,(-1L),8L,0xC4L},{0xC4L,1L,(-1L),(-1L),1L,0xC4L},{0xC4L,(-9L),0xC4L,(-1L),(-9L),(-1L)},{0xC4L,8L,0xC4L,(-1L),8L,0xC4L}};
        int32_t l_1805 = 0L;
        int i, j, k;
        for (g_51 = 0; (g_51 <= 1); g_51 += 1)
        { /* block id: 791 */
            uint16_t **l_1788 = &l_645[2][0][0];
            uint16_t ***l_1787 = &l_1788;
            uint32_t l_1789 = 0x49861B3DL;
            uint8_t *l_1790 = &g_44;
            int i;
            (**l_328) = ((((safe_mul_func_uint8_t_u_u((safe_div_func_int32_t_s_s((((0xFFL >= l_1715[g_51]) >= (safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((safe_mod_func_int16_t_s_s((safe_mul_func_int16_t_s_s((safe_unary_minus_func_int64_t_s(p_63)), (safe_div_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(((safe_sub_func_uint32_t_u_u(4294967290UL, 0UL)) <= (safe_sub_func_int64_t_s_s((((*l_199) ^= (l_1786[4][1][0] & p_63)) | (l_1787 != &l_1788)), 0xA0F9835D6ABFD84BLL))), 18446744073709551609UL)), l_1789)))), p_63)), 0x71L)), l_1715[g_51]))) > (**g_1159)), g_1191)), p_63)) , 0xF.CEDDA3p-74) != l_1715[g_51]) > (**g_633));
            return l_1790;
        }
        l_1805 |= (safe_add_func_uint64_t_u_u(((((safe_add_func_uint64_t_u_u(((*l_918) = ((*g_138) = (&l_1786[4][1][0] == &l_1786[2][2][1]))), (((safe_add_func_int64_t_s_s(p_63, (((g_79 < (p_62 >= ((((&p_63 != ((*l_1802) = ((safe_sub_func_int16_t_s_s((safe_sub_func_int32_t_s_s(((**g_1159) = (&g_1211 == (l_1801 = (void*)0))), g_44)), p_62)) , p_61))) && p_63) | 0xF7C31BF2L) != p_63))) | 1L) != l_1786[4][1][0]))) <= g_70[3]) == 0x77F0FF1FL))) > l_1803[4][3]) < 0xDC2FL) != l_1804[0]), p_62));
    }
    return l_1806;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc_bytes (&g_16, sizeof(g_16), "g_16", print_hash_value);
    transparent_crc(g_25, "g_25", print_hash_value);
    transparent_crc(g_44, "g_44", print_hash_value);
    transparent_crc(g_51, "g_51", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_67[i][j][k], "g_67[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_70[i], "g_70[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    transparent_crc_bytes (&g_161, sizeof(g_161), "g_161", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_184[i], "g_184[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_232, "g_232", print_hash_value);
    transparent_crc(g_262, "g_262", print_hash_value);
    transparent_crc(g_263, "g_263", print_hash_value);
    transparent_crc(g_304, "g_304", print_hash_value);
    transparent_crc_bytes (&g_417, sizeof(g_417), "g_417", print_hash_value);
    transparent_crc(g_418, "g_418", print_hash_value);
    transparent_crc_bytes (&g_421, sizeof(g_421), "g_421", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_463[i], "g_463[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_497, "g_497", print_hash_value);
    transparent_crc(g_514, "g_514", print_hash_value);
    transparent_crc(g_559, "g_559", print_hash_value);
    transparent_crc(g_847, "g_847", print_hash_value);
    transparent_crc(g_997, "g_997", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_1032[i][j][k], "g_1032[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1041, "g_1041", print_hash_value);
    transparent_crc(g_1106, "g_1106", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_1134[i][j][k], sizeof(g_1134[i][j][k]), "g_1134[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1136, "g_1136", print_hash_value);
    transparent_crc(g_1191, "g_1191", print_hash_value);
    transparent_crc(g_1211, "g_1211", print_hash_value);
    transparent_crc(g_1279, "g_1279", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1328[i], "g_1328[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1450, "g_1450", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1646[i], "g_1646[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1762, "g_1762", print_hash_value);
    transparent_crc(g_1842, "g_1842", print_hash_value);
    transparent_crc(g_2023, "g_2023", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2070[i], "g_2070[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2071, "g_2071", print_hash_value);
    transparent_crc(g_2330, "g_2330", print_hash_value);
    transparent_crc(g_2353, "g_2353", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_2363[i][j], "g_2363[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_2448[i][j], "g_2448[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2450, "g_2450", print_hash_value);
    transparent_crc(g_2454, "g_2454", print_hash_value);
    transparent_crc(g_2455, "g_2455", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2523[i][j][k], "g_2523[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2581, "g_2581", print_hash_value);
    transparent_crc(g_2710, "g_2710", print_hash_value);
    transparent_crc(g_2872, "g_2872", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 878
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 38
breakdown:
   depth: 1, occurrence: 131
   depth: 2, occurrence: 24
   depth: 3, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 3
   depth: 17, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 3
   depth: 21, occurrence: 2
   depth: 22, occurrence: 3
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 27, occurrence: 1
   depth: 30, occurrence: 1
   depth: 38, occurrence: 1

XXX total number of pointers: 508

XXX times a variable address is taken: 1360
XXX times a pointer is dereferenced on RHS: 318
breakdown:
   depth: 1, occurrence: 215
   depth: 2, occurrence: 81
   depth: 3, occurrence: 15
   depth: 4, occurrence: 7
XXX times a pointer is dereferenced on LHS: 359
breakdown:
   depth: 1, occurrence: 287
   depth: 2, occurrence: 62
   depth: 3, occurrence: 10
XXX times a pointer is compared with null: 53
XXX times a pointer is compared with address of another variable: 18
XXX times a pointer is compared with another pointer: 23
XXX times a pointer is qualified to be dereferenced: 13810

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1092
   level: 2, occurrence: 409
   level: 3, occurrence: 169
   level: 4, occurrence: 75
   level: 5, occurrence: 44
XXX number of pointers point to pointers: 262
XXX number of pointers point to scalars: 246
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.3
XXX average alias set size: 1.42

XXX times a non-volatile is read: 2308
XXX times a non-volatile is write: 1167
XXX times a volatile is read: 6
XXX    times read thru a pointer: 2
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 145
XXX percentage of non-volatile access: 99.8

XXX forward jumps: 4
XXX backward jumps: 11

XXX stmts: 122
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 37
   depth: 1, occurrence: 21
   depth: 2, occurrence: 14
   depth: 3, occurrence: 11
   depth: 4, occurrence: 12
   depth: 5, occurrence: 27

XXX percentage a fresh-made variable is used: 16.7
XXX percentage an existing variable is used: 83.3
********************* end of statistics **********************/


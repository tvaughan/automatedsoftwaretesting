/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      512377081
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int32_t *g_8 = (void*)0;
static int32_t * const g_19 = (void*)0;
static uint32_t g_24 = 18446744073709551615UL;
static int32_t g_32 = 0x87A5EAFBL;
static int8_t g_48 = (-8L);
static float g_63 = 0x4.7p+1;
static int32_t *g_66 = &g_32;
static int32_t ** volatile g_65 = &g_66;/* VOLATILE GLOBAL g_65 */
static int32_t g_68 = (-1L);
static int8_t g_77 = 3L;
static volatile int32_t * volatile *g_94 = &g_8;
static uint8_t g_96 = 0xD4L;
static uint8_t *g_106 = &g_96;
static int16_t g_123[1] = {0L};
static uint64_t g_137 = 1UL;
static uint32_t g_141 = 4294967286UL;
static uint16_t g_150 = 0xDC7AL;
static const uint8_t g_156 = 0UL;
static uint16_t g_165 = 0x6EEDL;
static int32_t g_187 = 0L;
static int32_t * volatile g_186[2][2] = {{&g_68,&g_68},{&g_68,&g_68}};
static int32_t * volatile g_188[10] = {(void*)0,&g_68,&g_32,&g_32,&g_68,(void*)0,&g_68,&g_32,&g_32,&g_68};
static int32_t * volatile g_190 = &g_187;/* VOLATILE GLOBAL g_190 */
static int32_t *g_193 = &g_68;
static int32_t ** volatile g_192 = &g_193;/* VOLATILE GLOBAL g_192 */
static int32_t ** const  volatile g_236 = &g_66;/* VOLATILE GLOBAL g_236 */
static const int32_t g_238 = 9L;
static const int32_t *g_237 = &g_238;
static const int32_t *g_240 = &g_187;
static const int32_t ** volatile g_239 = &g_240;/* VOLATILE GLOBAL g_239 */
static const uint16_t *g_245 = &g_150;
static const uint16_t **g_244 = &g_245;
static const uint16_t *** volatile g_243 = &g_244;/* VOLATILE GLOBAL g_243 */
static int64_t g_259 = 0x7E752563AE6E0BB0LL;
static int64_t g_283 = 0x40A9256F5496F96BLL;
static volatile int64_t g_285[4] = {0x0128C6EC92A06D2CLL,0x0128C6EC92A06D2CLL,0x0128C6EC92A06D2CLL,0x0128C6EC92A06D2CLL};
static uint32_t g_286 = 18446744073709551615UL;
static uint64_t g_306 = 18446744073709551609UL;
static float g_309 = 0x8.90954Fp-63;
static int32_t ** volatile g_479 = (void*)0;/* VOLATILE GLOBAL g_479 */
static int32_t ** volatile g_483 = &g_193;/* VOLATILE GLOBAL g_483 */
static uint8_t g_511 = 0x01L;
static int32_t g_513 = 0xF82C1DCAL;
static uint64_t g_524[1] = {18446744073709551613UL};
static float *g_535[1][9][9] = {{{(void*)0,&g_63,&g_63,(void*)0,&g_63,&g_309,(void*)0,(void*)0,(void*)0},{&g_309,(void*)0,&g_63,&g_63,&g_63,(void*)0,&g_309,(void*)0,(void*)0},{&g_309,(void*)0,&g_63,(void*)0,&g_309,&g_309,&g_309,&g_309,&g_63},{&g_309,&g_63,&g_309,&g_309,&g_309,(void*)0,(void*)0,&g_309,&g_309},{(void*)0,&g_309,(void*)0,&g_63,&g_63,(void*)0,&g_309,&g_63,&g_63},{&g_309,&g_309,&g_63,(void*)0,(void*)0,&g_309,&g_309,&g_63,(void*)0},{(void*)0,(void*)0,(void*)0,&g_63,&g_63,(void*)0,(void*)0,(void*)0,(void*)0},{&g_63,(void*)0,&g_63,&g_309,&g_63,(void*)0,&g_63,(void*)0,&g_309},{(void*)0,&g_309,&g_63,(void*)0,(void*)0,(void*)0,&g_63,(void*)0,(void*)0}}};
static float **g_534 = &g_535[0][4][5];
static int32_t ** volatile g_539[7][2][5] = {{{&g_193,&g_66,&g_66,&g_193,&g_193},{&g_66,&g_193,(void*)0,&g_193,&g_66}},{{&g_66,&g_66,&g_66,&g_193,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_193}},{{&g_193,&g_66,(void*)0,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_193,&g_66}},{{&g_66,&g_66,&g_193,&g_66,&g_193},{&g_66,&g_66,(void*)0,&g_66,&g_193}},{{&g_66,&g_193,&g_193,&g_193,&g_193},{&g_193,&g_66,&g_66,&g_193,&g_193}},{{&g_66,&g_193,(void*)0,&g_193,&g_66},{&g_66,&g_66,&g_66,&g_193,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_193},{&g_193,&g_66,(void*)0,&g_66,&g_66}}};
static int8_t *g_578 = &g_77;
static int8_t **g_577 = &g_578;
static int32_t * volatile g_588 = &g_68;/* VOLATILE GLOBAL g_588 */
static volatile int32_t * const g_603 = (void*)0;
static volatile int32_t g_606[1] = {0x4EBEC2AEL};
static volatile int32_t *g_605 = &g_606[0];
static volatile int32_t ** volatile g_604[9][9] = {{(void*)0,&g_8,&g_605,&g_605,&g_605,&g_8,(void*)0,(void*)0,&g_8},{&g_605,&g_8,&g_605,&g_8,&g_605,&g_605,&g_605,&g_605,&g_8},{(void*)0,&g_605,(void*)0,&g_605,&g_605,&g_605,&g_605,(void*)0,&g_605},{&g_605,(void*)0,&g_605,&g_605,&g_605,&g_605,(void*)0,&g_605,(void*)0},{&g_8,&g_605,&g_605,&g_605,&g_605,&g_8,&g_605,&g_8,&g_605},{&g_8,(void*)0,(void*)0,&g_8,&g_605,&g_605,&g_605,&g_8,(void*)0},{&g_605,&g_605,&g_605,&g_605,&g_605,&g_605,&g_605,&g_605,&g_605},{(void*)0,&g_8,&g_605,&g_605,&g_605,(void*)0,&g_605,&g_605,(void*)0},{&g_605,(void*)0,&g_605,(void*)0,&g_605,&g_605,&g_605,&g_605,(void*)0}};
static volatile int32_t ** const  volatile g_607 = &g_605;/* VOLATILE GLOBAL g_607 */
static volatile uint32_t g_690 = 0xE7EB24D2L;/* VOLATILE GLOBAL g_690 */
static int32_t ** const  volatile g_730 = &g_193;/* VOLATILE GLOBAL g_730 */
static uint8_t **g_742 = &g_106;
static uint8_t *** volatile g_741 = &g_742;/* VOLATILE GLOBAL g_741 */
static int32_t **g_747 = &g_193;
static int32_t *** const g_746 = &g_747;
static int32_t *** const *g_745 = &g_746;
static int16_t ** volatile g_752 = (void*)0;/* VOLATILE GLOBAL g_752 */
static int16_t ** volatile * volatile g_751 = &g_752;/* VOLATILE GLOBAL g_751 */
static int32_t g_780[1] = {0L};
static int32_t *g_779 = &g_780[0];
static int64_t *g_785 = &g_283;
static uint32_t * volatile g_794 = &g_141;/* VOLATILE GLOBAL g_794 */
static uint32_t * volatile * volatile g_793 = &g_794;/* VOLATILE GLOBAL g_793 */
static int8_t g_841 = 3L;
static uint32_t g_856 = 4294967294UL;
static volatile int64_t g_877 = 0xB3D3A31FAA84D566LL;/* VOLATILE GLOBAL g_877 */
static uint32_t g_880 = 1UL;
static uint64_t *g_892[8][2] = {{&g_524[0],&g_524[0]},{&g_524[0],&g_137},{&g_137,&g_306},{&g_137,&g_306},{&g_137,&g_137},{&g_524[0],&g_524[0]},{&g_524[0],&g_137},{&g_137,&g_306}};
static uint64_t **g_891 = &g_892[7][0];
static int32_t **g_950 = &g_779;
static int64_t g_995 = (-1L);
static int64_t *g_1010 = &g_995;
static float g_1051 = 0x5.1p-1;
static int32_t ***g_1077[6] = {&g_950,&g_950,&g_950,&g_950,&g_950,&g_950};
static int32_t **** volatile g_1076 = &g_1077[4];/* VOLATILE GLOBAL g_1076 */
static int32_t **** volatile * volatile g_1078 = &g_1076;/* VOLATILE GLOBAL g_1078 */
static int32_t **** volatile * volatile g_1099 = &g_1076;/* VOLATILE GLOBAL g_1099 */
static int32_t **** volatile g_1100 = &g_1077[0];/* VOLATILE GLOBAL g_1100 */
static volatile int32_t g_1140 = 0xF59CFC43L;/* VOLATILE GLOBAL g_1140 */
static uint32_t g_1239 = 0x132E86B9L;
static volatile float ** volatile * volatile * volatile g_1248 = (void*)0;/* VOLATILE GLOBAL g_1248 */
static int32_t ***g_1280 = (void*)0;
static int32_t **** volatile g_1279 = &g_1280;/* VOLATILE GLOBAL g_1279 */
static volatile float g_1331 = 0x1.Ep-1;/* VOLATILE GLOBAL g_1331 */
static volatile int16_t g_1362 = 1L;/* VOLATILE GLOBAL g_1362 */
static const float *g_1409 = (void*)0;
static const float **g_1408[8] = {&g_1409,&g_1409,&g_1409,&g_1409,&g_1409,&g_1409,&g_1409,&g_1409};
static const float ***g_1407 = &g_1408[7];
static float ***g_1412 = &g_534;
static uint64_t g_1437 = 0xCAFB507C9F8AA025LL;
static uint8_t *g_1460[8] = {&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96};
static int8_t *g_1463 = &g_48;
static uint32_t *g_1473 = &g_141;
static uint32_t **g_1472 = &g_1473;
static uint32_t ***g_1471 = &g_1472;
static volatile uint32_t g_1500 = 18446744073709551606UL;/* VOLATILE GLOBAL g_1500 */
static const volatile uint8_t g_1689 = 0x51L;/* VOLATILE GLOBAL g_1689 */
static int32_t ****g_1736 = &g_1280;
static int32_t ***** volatile g_1735 = &g_1736;/* VOLATILE GLOBAL g_1735 */
static const uint8_t *g_1745 = &g_156;
static const uint8_t **g_1744 = &g_1745;
static const uint8_t ***g_1743[6] = {&g_1744,&g_1744,(void*)0,&g_1744,&g_1744,(void*)0};
static const uint8_t ***g_1747 = &g_1744;
static uint16_t g_1767 = 65535UL;
static const int32_t ** volatile g_1769 = (void*)0;/* VOLATILE GLOBAL g_1769 */
static uint16_t g_1791 = 0x2880L;
static int32_t ** volatile g_1811 = &g_66;/* VOLATILE GLOBAL g_1811 */
static uint32_t * volatile g_1993 = &g_286;/* VOLATILE GLOBAL g_1993 */
static uint32_t * const  volatile *g_1992 = &g_1993;
static volatile uint32_t **g_1994 = (void*)0;
static int32_t ** volatile g_2108 = (void*)0;/* VOLATILE GLOBAL g_2108 */
static int32_t ** volatile g_2109 = &g_66;/* VOLATILE GLOBAL g_2109 */
static int32_t ** volatile g_2165 = &g_66;/* VOLATILE GLOBAL g_2165 */
static volatile int64_t *g_2174[8] = {&g_285[3],&g_285[3],&g_285[1],&g_285[3],&g_285[3],&g_285[1],&g_285[3],&g_285[3]};
static volatile int64_t * volatile *g_2173 = &g_2174[5];
static volatile int64_t * volatile **g_2172 = &g_2173;
static uint32_t ** volatile * volatile g_2266 = &g_1472;/* VOLATILE GLOBAL g_2266 */
static uint32_t ** volatile * volatile *g_2265 = &g_2266;
static uint32_t ** volatile * volatile * const  volatile * volatile g_2264[2] = {&g_2265,&g_2265};
static volatile int16_t *g_2286 = (void*)0;
static volatile int16_t **g_2285 = &g_2286;
static int16_t g_2324[6][4][1] = {{{0xA684L},{(-1L)},{0xEB25L},{0xEB25L}},{{(-1L)},{0xA684L},{(-10L)},{0xA684L}},{{(-1L)},{0xEB25L},{0xEB25L},{(-1L)}},{{0xA684L},{(-10L)},{0xA684L},{(-1L)}},{{0xEB25L},{0xEB25L},{(-1L)},{0xA684L}},{{(-10L)},{0xA684L},{(-1L)},{0xEB25L}}};
static int16_t g_2356[1] = {(-8L)};
static uint16_t *g_2438 = &g_1791;
static uint16_t **g_2437 = &g_2438;
static uint16_t ***g_2436[9] = {&g_2437,&g_2437,&g_2437,&g_2437,&g_2437,&g_2437,&g_2437,&g_2437,&g_2437};
static int32_t *****g_2445 = &g_1736;
static volatile int32_t * volatile g_2454 = &g_606[0];/* VOLATILE GLOBAL g_2454 */
static int32_t * const * const * const * const *g_2503 = (void*)0;
static float ****g_2510 = &g_1412;
static int32_t *g_2548[6] = {(void*)0,&g_32,(void*)0,(void*)0,&g_32,(void*)0};
static const int8_t g_2563 = 0x50L;
static uint32_t ****g_2586 = &g_1471;
static uint32_t *****g_2585[3] = {&g_2586,&g_2586,&g_2586};
static volatile int32_t * volatile g_2594 = &g_606[0];/* VOLATILE GLOBAL g_2594 */
static int32_t ** volatile g_2908 = &g_2548[2];/* VOLATILE GLOBAL g_2908 */
static int32_t g_3034 = 0x2F5D07CBL;
static volatile int32_t g_3134 = (-1L);/* VOLATILE GLOBAL g_3134 */
static float * const  volatile g_3260 = &g_1051;/* VOLATILE GLOBAL g_3260 */
static uint32_t g_3322 = 0xB708CF0AL;
static const int8_t g_3358 = 0x77L;
static volatile uint8_t g_3380 = 250UL;/* VOLATILE GLOBAL g_3380 */
static const int32_t **g_3462 = (void*)0;
static const int32_t ***g_3461[8] = {&g_3462,&g_3462,&g_3462,&g_3462,&g_3462,&g_3462,&g_3462,&g_3462};
static const int32_t ****g_3460 = &g_3461[7];
static uint32_t * volatile *g_3464 = &g_794;
static uint32_t * volatile ** volatile g_3463 = &g_3464;/* VOLATILE GLOBAL g_3463 */
static uint32_t g_3532 = 1UL;
static int32_t g_3536 = (-2L);


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static const int32_t * func_2(uint64_t  p_3, int32_t * p_4, uint32_t  p_5, int32_t * p_6, int32_t * const  p_7);
static uint8_t  func_9(const uint32_t  p_10, int32_t * p_11, int32_t * p_12);
static const uint32_t  func_13(int32_t * const  p_14, int32_t * const  p_15, uint32_t  p_16, uint32_t  p_17, int32_t * p_18);
static int32_t * func_20(int8_t  p_21, int8_t  p_22, int64_t  p_23);
static const int16_t  func_26(uint32_t  p_27, int32_t * const  p_28);
static uint16_t  func_35(uint16_t  p_36, int32_t * p_37);
static const int8_t  func_40(int32_t * p_41, int32_t  p_42, uint32_t  p_43, int32_t  p_44);
static uint64_t  func_49(int32_t * p_50, int64_t  p_51, int16_t  p_52);
static int64_t  func_56(int16_t  p_57, int8_t * p_58, uint32_t  p_59);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_8 g_19 g_24 g_32 g_48 g_65 g_94 g_68 g_96 g_123 g_106 g_141 g_150 g_165 g_156 g_137 g_190 g_187 g_66 g_192 g_193 g_236 g_239 g_243 g_77 g_286 g_240 g_285 g_238 g_793 g_794 g_891 g_892 g_524 g_306 g_245 g_605 g_746 g_747 g_1010 g_995 g_745 g_577 g_578 g_742 g_606 g_1239 g_607 g_1248 g_741 g_1279 g_1280 g_856 g_1100 g_1077 g_950 g_1362 g_785 g_283 g_513 g_1407 g_237 g_1437 g_1471 g_1472 g_1473 g_588 g_880 g_1791 g_1463 g_1747 g_1744 g_1745 g_1992 g_1994 g_1993 g_1767 g_2109 g_2165 g_259 g_2172 g_2173 g_511 g_2264 g_751 g_752 g_2285 g_2286 g_2324 g_186 g_2356 g_1140 g_2436 g_2454 g_2437 g_2438 g_2548 g_2585 g_2594 g_841 g_2563 g_2908 g_3260 g_3322 g_3034 g_3380 g_1736 g_730 g_1811 g_483 g_3463 g_3532 g_535 g_3536
 * writes: g_48 g_63 g_66 g_68 g_106 g_96 g_77 g_123 g_137 g_141 g_150 g_165 g_187 g_193 g_237 g_240 g_244 g_259 g_286 g_306 g_309 g_606 g_892 g_995 g_1239 g_1248 g_1280 g_779 g_513 g_524 g_1407 g_1412 g_1437 g_1460 g_1463 g_1471 g_283 g_880 g_1791 g_1743 g_950 g_1767 g_1473 g_856 g_32 g_578 g_2436 g_2445 g_1140 g_841 g_2454 g_188 g_2503 g_2510 g_511 g_742 g_2548 g_2324 g_2356 g_1051 g_3034 g_3322 g_3380 g_24 g_891 g_3460 g_3463 g_3536
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_31[8][4][7] = {{{(void*)0,&g_32,(void*)0,&g_32,(void*)0,&g_32,(void*)0},{&g_32,(void*)0,&g_32,&g_32,(void*)0,&g_32,&g_32},{(void*)0,&g_32,&g_32,&g_32,(void*)0,&g_32,(void*)0},{(void*)0,&g_32,&g_32,(void*)0,&g_32,&g_32,(void*)0}},{{(void*)0,&g_32,(void*)0,&g_32,(void*)0,&g_32,(void*)0},{(void*)0,(void*)0,&g_32,(void*)0,(void*)0,&g_32,(void*)0},{(void*)0,&g_32,(void*)0,&g_32,(void*)0,&g_32,(void*)0},{&g_32,(void*)0,&g_32,&g_32,(void*)0,&g_32,&g_32}},{{(void*)0,&g_32,&g_32,&g_32,(void*)0,&g_32,(void*)0},{(void*)0,&g_32,&g_32,(void*)0,&g_32,&g_32,&g_32},{&g_32,&g_32,&g_32,&g_32,(void*)0,&g_32,&g_32},{&g_32,&g_32,(void*)0,&g_32,&g_32,(void*)0,&g_32}},{{(void*)0,&g_32,&g_32,&g_32,&g_32,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32},{(void*)0,&g_32,(void*)0,&g_32,(void*)0,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32}},{{&g_32,&g_32,&g_32,&g_32,(void*)0,&g_32,&g_32},{&g_32,&g_32,(void*)0,&g_32,&g_32,(void*)0,&g_32},{(void*)0,&g_32,&g_32,&g_32,&g_32,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32}},{{(void*)0,&g_32,(void*)0,&g_32,(void*)0,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32},{&g_32,&g_32,&g_32,&g_32,(void*)0,&g_32,&g_32},{&g_32,&g_32,(void*)0,&g_32,&g_32,(void*)0,&g_32}},{{(void*)0,&g_32,&g_32,&g_32,&g_32,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32},{(void*)0,&g_32,(void*)0,&g_32,(void*)0,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32}},{{&g_32,&g_32,&g_32,&g_32,(void*)0,&g_32,&g_32},{&g_32,&g_32,(void*)0,&g_32,&g_32,(void*)0,&g_32},{(void*)0,&g_32,&g_32,&g_32,&g_32,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32}}};
    uint32_t l_1838 = 4294967291UL;
    uint16_t l_2368 = 2UL;
    int32_t **l_2646 = (void*)0;
    int32_t *l_2647 = (void*)0;
    int32_t *l_3033 = &g_3034;
    uint16_t l_3533[10];
    uint64_t l_3534 = 0x8ED8BBA919719932LL;
    int32_t * const l_3535 = &g_3536;
    const int32_t **l_3558 = &g_237;
    int16_t l_3559 = 0x3779L;
    int32_t l_3568 = 0L;
    int8_t *l_3573 = &g_841;
    int64_t l_3574[8] = {(-1L),(-4L),(-1L),(-4L),(-1L),(-4L),(-1L),(-4L)};
    int32_t *l_3575[8][6] = {{&g_68,&g_187,&l_3568,&g_3034,&g_32,&g_32},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_32},{(void*)0,&g_32,&l_3568,(void*)0,&g_3034,(void*)0},{&g_187,&g_32,&g_187,&g_32,&g_3034,&g_68},{&l_3568,&g_32,(void*)0,(void*)0,(void*)0,&g_187},{&g_32,&g_32,&g_187,(void*)0,&g_68,&l_3568},{&g_32,&g_187,(void*)0,&l_3568,(void*)0,&g_187},{(void*)0,&g_32,(void*)0,&g_187,&g_32,&l_3568}};
    int32_t * const l_3576 = &l_3568;
    uint32_t l_3577 = 0x65DA3F32L;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_3533[i] = 65535UL;
    (*l_3558) = func_2((0L != ((g_8 != ((func_9(func_13(g_19, (l_31[0][3][3] = func_20(g_24, (safe_unary_minus_func_int16_t_s(func_26((((*g_588) = (((safe_rshift_func_uint8_t_u_u(((void*)0 == l_31[5][3][4]), 4)) & g_32) > ((safe_lshift_func_int16_t_s_u(g_24, (func_35(g_32, l_31[6][1][6]) != g_238))) <= 0L))) >= l_1838), l_31[1][1][3]))), l_2368)), g_156, l_1838, l_2647), l_3033, l_2647) , 1UL) , (void*)0)) < l_3533[9])), l_3033, l_3534, g_535[0][5][3], l_3535);
    l_3559 &= ((*l_3033) = ((*l_3535) ^= (*g_2454)));
    (*l_3576) = ((safe_add_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((((safe_mod_func_uint16_t_u_u(((safe_sub_func_uint16_t_u_u(((((**g_1744) && 0x12L) < ((*g_1463) = l_3568)) < (((*l_3535) , &l_1838) != (void*)0)), (*g_2438))) ^ (*g_785)), (*g_245))) && g_123[0]) < l_3577), 2)), (*g_2438))) != 0x7C35L);
    return (*l_3033);
}


/* ------------------------------------------ */
/* 
 * reads : g_2437 g_2438 g_1791 g_2356 g_607 g_605 g_606
 * writes: g_1791 g_2356 g_3034
 */
static const int32_t * func_2(uint64_t  p_3, int32_t * p_4, uint32_t  p_5, int32_t * p_6, int32_t * const  p_7)
{ /* block id: 1525 */
    uint8_t l_3541 = 0x42L;
    uint8_t ***l_3543[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint8_t ****l_3542 = &l_3543[5];
    int32_t l_3544[1];
    int16_t *l_3551 = &g_2356[0];
    int32_t l_3554 = (-1L);
    uint32_t l_3555[1][4][2] = {{{0x420C8286L,0x5298549DL},{0x420C8286L,0x5298549DL},{0x420C8286L,0x5298549DL},{0x420C8286L,0x5298549DL}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_3544[i] = 0x3722BC49L;
    (*p_4) = (safe_mul_func_uint16_t_u_u(((*g_2438) = (**g_2437)), (safe_lshift_func_int8_t_s_s((l_3541 , (((l_3542 != &l_3543[1]) & l_3544[0]) == ((safe_add_func_uint32_t_u_u(((safe_add_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((*l_3551) |= p_3), 0x6C88L)), (((8L <= (safe_div_func_uint32_t_u_u((--l_3555[0][0][0]), (l_3541 ^ l_3541)))) | 0L) , l_3555[0][3][1]))) , 4294967289UL), (**g_607))) || 0L))), 7))));
    return p_6;
}


/* ------------------------------------------ */
/* 
 * reads : g_187 g_245 g_150 g_3260 g_741 g_742 g_106 g_96 g_165 g_524 g_1745 g_156 g_577 g_578 g_77 g_2437 g_2438 g_1791 g_588 g_68 g_745 g_746 g_747 g_3322 g_2324 g_190 g_785 g_283 g_605 g_606 g_3034 g_891 g_892 g_137 g_306 g_141 g_3380 g_1736 g_1280 g_730 g_1472 g_193 g_794 g_1811 g_793 g_1010 g_995 g_483 g_3463 g_2594 g_238 g_1463 g_48 g_3532
 * writes: g_187 g_1767 g_32 g_63 g_1051 g_137 g_150 g_1239 g_3034 g_165 g_96 g_68 g_193 g_3322 g_995 g_2324 g_606 g_141 g_3380 g_1280 g_24 g_77 g_66 g_856 g_891 g_1791 g_880 g_524 g_306 g_283 g_3460 g_237 g_3463
 */
static uint8_t  func_9(const uint32_t  p_10, int32_t * p_11, int32_t * p_12)
{ /* block id: 1308 */
    int64_t *l_3039 = &g_995;
    int32_t l_3040 = 0xF50BB72CL;
    uint32_t **** const *l_3075 = &g_2586;
    float **l_3082 = &g_535[0][4][5];
    int16_t l_3088 = 0x59F5L;
    int32_t l_3096 = 0x81C32E04L;
    uint32_t ***l_3097 = &g_1472;
    int32_t l_3098 = 0x05C02027L;
    uint16_t l_3196 = 5UL;
    int64_t l_3257 = 0x33B52203A12F265CLL;
    int32_t l_3279 = 0x8E45BA00L;
    int32_t l_3280[4][5] = {{0x312EE580L,1L,0x312EE580L,1L,1L},{0x312EE580L,1L,0x312EE580L,1L,1L},{0x312EE580L,1L,0x312EE580L,1L,1L},{0x312EE580L,1L,0x312EE580L,1L,1L}};
    float l_3281 = 0x9.3p+1;
    float l_3286 = 0x2.1p-1;
    const uint32_t l_3310 = 0UL;
    int32_t l_3311 = 1L;
    const int8_t *l_3353 = &g_48;
    const uint8_t **l_3367 = (void*)0;
    uint32_t l_3373 = 5UL;
    int64_t l_3378 = 0L;
    uint64_t ***l_3455[3][7][10] = {{{&g_891,&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891,&g_891,&g_891},{(void*)0,&g_891,&g_891,&g_891,&g_891,(void*)0,(void*)0,&g_891,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891}},{{&g_891,&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891,(void*)0,&g_891},{&g_891,(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,(void*)0,(void*)0,&g_891,(void*)0},{&g_891,(void*)0,(void*)0,&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891,&g_891},{(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891}},{{&g_891,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891},{&g_891,&g_891,(void*)0,&g_891,&g_891,&g_891,(void*)0,&g_891,&g_891,&g_891},{(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,(void*)0},{&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891},{&g_891,&g_891,(void*)0,&g_891,(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891},{(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891},{(void*)0,&g_891,(void*)0,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891,&g_891}}};
    uint64_t l_3505[5][1] = {{0x5E0ABC20D0AA14B2LL},{0xFE08E875B059A0A9LL},{0x5E0ABC20D0AA14B2LL},{0xFE08E875B059A0A9LL},{0x5E0ABC20D0AA14B2LL}};
    uint32_t l_3506 = 4294967293UL;
    float *****l_3507 = &g_2510;
    int16_t *l_3515 = &g_123[0];
    int16_t **l_3514 = &l_3515;
    int16_t ***l_3513 = &l_3514;
    int16_t ****l_3512 = &l_3513;
    uint8_t **l_3525[4];
    int8_t *l_3531[9][5] = {{&g_841,&g_48,&g_841,&g_841,&g_841},{&g_841,&g_77,&g_841,&g_77,&g_77},{&g_841,&g_48,&g_841,&g_841,&g_841},{&g_841,&g_77,&g_841,&g_77,&g_77},{&g_841,&g_48,&g_841,&g_841,&g_841},{&g_841,&g_77,&g_841,&g_77,&g_77},{&g_841,&g_48,&g_841,&g_841,&g_841},{&g_841,&g_77,&g_841,&g_77,&g_77},{&g_841,&g_48,&g_841,&g_841,&g_841}};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_3525[i] = &g_106;
    for (g_187 = 12; (g_187 == 26); g_187 = safe_add_func_int16_t_s_s(g_187, 1))
    { /* block id: 1311 */
        float ***l_3076 = &g_534;
        int32_t l_3080 = (-7L);
        int32_t l_3081[1];
        int32_t ***l_3135 = &g_747;
        uint32_t l_3222 = 0xF73B750AL;
        int32_t ****l_3238 = &g_1077[0];
        int32_t *****l_3237 = &l_3238;
        uint32_t *l_3248 = (void*)0;
        int8_t l_3259 = 0x09L;
        int32_t l_3268 = 0L;
        int8_t l_3291[5];
        int16_t ***l_3342 = (void*)0;
        int16_t ****l_3341[5] = {&l_3342,&l_3342,&l_3342,&l_3342,&l_3342};
        uint64_t ** const *l_3344 = &g_891;
        uint64_t ** const **l_3343[10] = {&l_3344,&l_3344,&l_3344,&l_3344,&l_3344,&l_3344,&l_3344,&l_3344,&l_3344,&l_3344};
        const int8_t *l_3357 = &g_3358;
        uint8_t l_3412[3][6][9] = {{{0x03L,255UL,255UL,0x03L,5UL,0xA8L,0UL,4UL,0xF3L},{0x80L,248UL,0x80L,246UL,0UL,248UL,0UL,246UL,0x80L},{0xF3L,0xF3L,0xA8L,255UL,5UL,4UL,0UL,0UL,0x03L},{0x28L,6UL,0xAFL,246UL,0x28L,246UL,0xAFL,6UL,0x28L},{5UL,0UL,0xA8L,0x03L,0xF3L,0UL,255UL,4UL,4UL},{0UL,6UL,0x80L,0x03L,0x80L,6UL,0UL,0x03L,0UL}},{{5UL,0xF3L,255UL,0UL,0x03L,4UL,0xA8L,0xA8L,4UL},{0x28L,248UL,1UL,248UL,0x28L,0x03L,1UL,0x03L,0x28L},{0xF3L,255UL,0UL,0x03L,4UL,0xA8L,0xA8L,4UL,0x03L},{0x80L,0x03L,0x80L,6UL,0UL,0x03L,0UL,6UL,0x80L},{0x03L,0xF3L,0UL,255UL,4UL,4UL,255UL,0UL,0xF3L},{0x28L,246UL,0xAFL,6UL,0x28L,6UL,0xAFL,246UL,0x28L}},{{4UL,0UL,0UL,0x03L,0x03L,0UL,0UL,4UL,5UL},{0UL,246UL,0x80L,248UL,0x80L,246UL,0UL,248UL,0UL},{4UL,0xF3L,0UL,0UL,0xF3L,4UL,0UL,0xA8L,5UL},{0x28L,0x03L,1UL,0x03L,0x28L,248UL,1UL,248UL,0x28L},{0x03L,255UL,255UL,0x03L,5UL,0xA8L,0UL,4UL,0xF3L},{0x80L,248UL,0x80L,246UL,0UL,248UL,0UL,246UL,0x80L}}};
        int32_t l_3473 = 0xB3CCA64CL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_3081[i] = 0x509E7B3CL;
        for (i = 0; i < 5; i++)
            l_3291[i] = 0L;
        for (g_1767 = (-20); (g_1767 < 36); ++g_1767)
        { /* block id: 1314 */
            uint32_t l_3063 = 0xF0BD30E6L;
            int32_t l_3079 = 0L;
            uint32_t l_3115 = 0xB7C0AEFEL;
            int64_t **l_3142 = &l_3039;
            int64_t ***l_3141 = &l_3142;
            int32_t *l_3149[5][6] = {{&g_3034,&l_3081[0],(void*)0,&l_3081[0],(void*)0,&l_3081[0]},{(void*)0,&g_3034,&l_3098,&l_3081[0],&l_3081[0],&l_3098},{(void*)0,(void*)0,&l_3081[0],&l_3081[0],&l_3081[0],&l_3081[0]},{&g_3034,(void*)0,&g_3034,&l_3098,&l_3081[0],&l_3081[0]},{&l_3081[0],&g_3034,&g_3034,&l_3081[0],(void*)0,&l_3081[0]}};
            uint32_t ****l_3170 = &l_3097;
            uint32_t l_3171 = 0x71D4B5DEL;
            int16_t **l_3176 = (void*)0;
            int16_t ***l_3175[8][7][4] = {{{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,(void*)0,&l_3176,&l_3176},{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0},{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176}},{{(void*)0,(void*)0,&l_3176,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{(void*)0,&l_3176,&l_3176,&l_3176},{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176}},{{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,(void*)0,&l_3176,&l_3176},{(void*)0,&l_3176,(void*)0,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0}},{{&l_3176,&l_3176,&l_3176,(void*)0},{&l_3176,&l_3176,(void*)0,&l_3176},{&l_3176,&l_3176,(void*)0,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,(void*)0,&l_3176}},{{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{(void*)0,&l_3176,(void*)0,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,(void*)0,&l_3176}},{{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,(void*)0,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,(void*)0,&l_3176}},{{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,(void*)0,(void*)0},{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0}},{{&l_3176,&l_3176,&l_3176,(void*)0},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,&l_3176},{&l_3176,(void*)0,(void*)0,&l_3176},{(void*)0,&l_3176,&l_3176,&l_3176},{&l_3176,&l_3176,&l_3176,(void*)0},{&l_3176,&l_3176,(void*)0,&l_3176}}};
            int16_t ****l_3174 = &l_3175[1][3][2];
            int8_t ** const *l_3195[5] = {&g_577,&g_577,&g_577,&g_577,&g_577};
            uint32_t **l_3249 = &l_3248;
            uint32_t *l_3250 = (void*)0;
            float *l_3252 = &g_63;
            uint16_t ** const l_3258 = &g_2438;
            int i, j, k;
            for (g_32 = 2; (g_32 <= 8); g_32 += 1)
            { /* block id: 1317 */
                int16_t l_3049 = 5L;
                float **l_3083[4][2] = {{&g_535[0][4][5],&g_535[0][4][5]},{&g_535[0][1][6],&g_535[0][4][5]},{&g_535[0][4][5],&g_535[0][1][6]},{&g_535[0][4][5],&g_535[0][4][5]}};
                int32_t l_3095 = (-1L);
                const int64_t *l_3140 = &g_995;
                const int64_t **l_3139 = &l_3140;
                const int64_t ** const *l_3138 = &l_3139;
                int16_t ****l_3178[10][3][3] = {{{&l_3175[1][3][2],&l_3175[1][3][2],&l_3175[1][3][2]},{&l_3175[1][3][2],&l_3175[1][3][2],(void*)0},{(void*)0,(void*)0,(void*)0}},{{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0}},{{(void*)0,(void*)0,(void*)0},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]}},{{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{(void*)0,(void*)0,(void*)0},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0}},{{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{(void*)0,(void*)0,(void*)0}},{{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0}},{{(void*)0,(void*)0,(void*)0},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]}},{{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{(void*)0,(void*)0,(void*)0},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0}},{{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{(void*)0,(void*)0,(void*)0}},{{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0},{&l_3175[1][3][2],(void*)0,&l_3175[1][3][2]},{&l_3175[1][5][0],&l_3175[1][3][2],(void*)0}}};
                int32_t **** const *l_3239 = (void*)0;
                int i, j, k;
            }
            (*g_3260) = (safe_div_func_float_f_f((safe_div_func_float_f_f(p_10, (safe_add_func_float_f_f((safe_mul_func_float_f_f(p_10, ((((*l_3249) = l_3248) != l_3250) >= ((*l_3252) = (-(p_10 != 0x7.8p+1)))))), (((l_3098 && (((((-1L) > (((safe_add_func_int16_t_s_s((safe_div_func_uint32_t_u_u(4294967292UL, p_10)), p_10)) & (*g_245)) & (-10L))) | 0x90L) , l_3257) <= p_10)) , l_3258) == (void*)0))))), l_3259));
            for (g_137 = 0; (g_137 >= 41); g_137++)
            { /* block id: 1380 */
                float l_3276[1][1][3];
                int32_t l_3282 = (-2L);
                int32_t l_3283 = 6L;
                int32_t l_3284 = (-1L);
                int32_t l_3285 = (-10L);
                int32_t l_3288 = 1L;
                int32_t l_3289 = 0x85687F8AL;
                int32_t l_3290[1];
                int16_t l_3314 = 0L;
                int8_t l_3315 = 0xFCL;
                int i, j, k;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 3; k++)
                            l_3276[i][j][k] = 0x1.6p+1;
                    }
                }
                for (i = 0; i < 1; i++)
                    l_3290[i] = (-1L);
                for (g_150 = 0; (g_150 < 15); ++g_150)
                { /* block id: 1383 */
                    uint64_t l_3270 = 1UL;
                    int32_t l_3278[4][3][5] = {{{0x6ABD6C87L,(-1L),1L,(-1L),0x6ABD6C87L},{0L,(-1L),(-3L),0x4459B105L,0x4459B105L},{7L,(-1L),7L,7L,7L}},{{0L,0x4459B105L,(-1L),(-1L),0x4459B105L},{0x6ABD6C87L,7L,1L,7L,0x6ABD6C87L},{0x4459B105L,(-1L),(-1L),0x4459B105L,0L}},{{7L,7L,7L,(-1L),7L},{0x4459B105L,0x4459B105L,(-3L),(-1L),0L},{0x6ABD6C87L,(-1L),1L,(-1L),0x6ABD6C87L}},{{0L,(-1L),(-3L),0x4459B105L,0x4459B105L},{7L,(-1L),7L,7L,7L},{0L,0x4459B105L,(-1L),(-1L),0x4459B105L}}};
                    uint16_t l_3292 = 65535UL;
                    int32_t l_3312[7][8][4] = {{{0x5C479D58L,0xC67CEAC9L,0x75901B33L,0xA087ED49L},{0x1294D1DFL,0x4BE76E9FL,0x0141FCBEL,0L},{6L,1L,(-2L),0L},{(-1L),0x1294D1DFL,(-1L),(-10L)},{(-10L),1L,0xA087ED49L,8L},{0x5489FF24L,(-5L),(-1L),1L},{0xD0C206F6L,8L,(-1L),(-10L)},{0x5489FF24L,0xDB4EFE07L,0xA087ED49L,0x8A46C34FL}},{{(-10L),0x5489FF24L,(-1L),0x3463A2DCL},{(-1L),0x3463A2DCL,(-2L),0x08E19820L},{6L,(-2L),0x0141FCBEL,0xD0C206F6L},{0x1294D1DFL,0x5489FF24L,0xA7085E59L,0L},{0xEFAB19D7L,0L,0x5489FF24L,8L},{0xA087ED49L,(-1L),(-2L),(-2L)},{0L,0L,0L,1L},{0xB8CA4634L,6L,(-2L),2L}},{{0x08E19820L,(-2L),(-1L),(-2L)},{0L,(-2L),0xA087ED49L,2L},{(-2L),6L,0xC67CEAC9L,1L},{8L,0L,9L,(-2L)},{0x0141FCBEL,(-1L),0x4BE76E9FL,8L},{(-1L),0L,0x7B65D886L,0L},{0x4BE76E9FL,0x5489FF24L,6L,1L},{0xF21C6625L,0x08E19820L,2L,9L}},{{8L,0x7B65D886L,0xD0C206F6L,0x7B65D886L},{6L,0xD0C206F6L,0x4FD99347L,0x5489FF24L},{9L,(-5L),0xEFAB19D7L,0L},{1L,0x4FD99347L,0L,(-1L)},{1L,0L,0xEFAB19D7L,(-10L)},{9L,(-1L),0x4FD99347L,(-1L)},{6L,(-8L),0xD0C206F6L,0xF21C6625L},{8L,(-1L),2L,6L}},{{0xF21C6625L,8L,6L,(-10L)},{0x4BE76E9FL,0x3463A2DCL,0x7B65D886L,0xB8CA4634L},{(-1L),0xDB4EFE07L,0x4BE76E9FL,0xC67CEAC9L},{0x0141FCBEL,9L,9L,0x0141FCBEL},{8L,2L,0xC67CEAC9L,0xDB4EFE07L},{(-2L),(-10L),0xA087ED49L,0L},{0L,(-1L),(-1L),0L},{0x08E19820L,(-10L),(-2L),0xDB4EFE07L}},{{0xB8CA4634L,2L,0L,0x0141FCBEL},{0L,9L,(-2L),0xC67CEAC9L},{0xA087ED49L,0xDB4EFE07L,0x5489FF24L,0xB8CA4634L},{0xEFAB19D7L,0x3463A2DCL,0xA7085E59L,(-10L)},{(-8L),8L,8L,6L},{(-2L),(-1L),(-2L),0xF21C6625L},{0x08A3DA38L,(-8L),0x08A3DA38L,(-1L)},{(-1L),(-1L),(-10L),(-10L)}},{{0xD0C206F6L,0L,0x8A46C34FL,(-1L)},{1L,0x4FD99347L,0x8A46C34FL,0L},{0xD0C206F6L,(-5L),(-10L),0x5489FF24L},{(-1L),0xD0C206F6L,0x08A3DA38L,0x7B65D886L},{0x08A3DA38L,0x7B65D886L,(-2L),9L},{(-2L),0x08E19820L,8L,1L},{(-8L),0x5489FF24L,0xA7085E59L,0L},{0xEFAB19D7L,0L,0x5489FF24L,8L}}};
                    int16_t l_3313 = (-1L);
                    uint16_t l_3316 = 65527UL;
                    int16_t ****l_3339 = (void*)0;
                    int i, j, k;
                    for (g_1239 = 0; (g_1239 == 42); g_1239++)
                    { /* block id: 1386 */
                        int16_t l_3267[2][3][10] = {{{0xACAAL,0x8155L,(-1L),0x8155L,0xACAAL,0x8155L,(-1L),0x8155L,0xACAAL,0x8155L},{(-1L),0x8155L,(-1L),0x27F4L,(-1L),0x8155L,(-1L),0x27F4L,(-1L),0x8155L},{0xACAAL,0x27F4L,(-1L),0x27F4L,0xACAAL,0x27F4L,(-1L),0x27F4L,0xACAAL,0x27F4L}},{{(-1L),0x27F4L,(-1L),0x8155L,(-1L),0x27F4L,(-1L),0x8155L,(-1L),0x27F4L},{0xACAAL,0x8155L,(-1L),0x8155L,0xACAAL,0x8155L,(-1L),0x8155L,0xACAAL,0x8155L},{(-1L),0x8155L,(-1L),0x27F4L,(-1L),0x8155L,(-1L),0x27F4L,(-1L),0x8155L}}};
                        int i, j, k;
                        (*p_11) = l_3267[1][0][6];
                        l_3098 |= l_3268;
                        return (***g_741);
                    }
                    for (g_165 = 0; (g_165 <= 0); g_165 += 1)
                    { /* block id: 1393 */
                        int16_t l_3269 = 0L;
                        int32_t l_3273 = 0xF3534AD1L;
                        int32_t l_3274 = 0xD5EE7994L;
                        int32_t l_3275 = (-7L);
                        int32_t l_3277[8][10] = {{0x91156D03L,0xB3B472E0L,0x59AD3A4DL,1L,0x142C4C69L,(-2L),7L,(-2L),0x142C4C69L,1L},{0x91156D03L,(-2L),0x91156D03L,0xF6F92751L,0xEB0AC0B6L,7L,1L,(-3L),0x59AD3A4DL,0x21FA4D96L},{7L,1L,(-3L),0x59AD3A4DL,0x21FA4D96L,0x21FA4D96L,0x59AD3A4DL,(-3L),1L,7L},{0x89020150L,0xC7F7118EL,0x91156D03L,0xB3B472E0L,0x59AD3A4DL,1L,0x142C4C69L,(-2L),7L,(-2L)},{0xAD307771L,0x91156D03L,0x59AD3A4DL,0xC7F7118EL,0x59AD3A4DL,0x91156D03L,0xAD307771L,0x21FA4D96L,0x78FB2FA2L,7L},{0x59AD3A4DL,0x142C4C69L,0x3889FDE2L,(-3L),0x21FA4D96L,0xEB0AC0B6L,0xB3B472E0L,0xB3B472E0L,0xEB0AC0B6L,0x21FA4D96L},{0x78FB2FA2L,0x142C4C69L,0x142C4C69L,0x78FB2FA2L,0xEB0AC0B6L,0x59AD3A4DL,0xAD307771L,7L,0x5D8C93F5L,1L},{0x3889FDE2L,0x91156D03L,0x5D8C93F5L,0xAD307771L,0x142C4C69L,(-3L),0x142C4C69L,0xAD307771L,0x5D8C93F5L,0x91156D03L}};
                        int8_t l_3287 = 0L;
                        uint16_t **l_3301 = &g_2438;
                        int i, j;
                        l_3269 |= 0x303FC084L;
                        --l_3270;
                        --l_3292;
                        (*g_588) |= ((g_524[g_165] , ((l_3290[0] = ((p_10 && (safe_sub_func_uint8_t_u_u(0xDCL, (safe_add_func_int8_t_s_s(((*g_1745) & (safe_add_func_int8_t_s_s((**g_577), (l_3312[3][0][1] &= ((((((((void*)0 == l_3301) < (safe_sub_func_float_f_f(0x1.5p+1, ((((safe_lshift_func_uint8_t_u_u(((*g_106)--), ((l_3277[3][1] |= ((safe_lshift_func_int8_t_s_s(l_3310, l_3278[3][2][4])) != 0UL)) <= (**g_2437)))) , 0xE.780474p-64) <= p_10) != p_10)))) , 0x44376069L) ^ l_3311) > l_3269) , l_3310) , l_3280[1][4]))))), l_3288))))) <= l_3275)) , 0L)) && p_10);
                    }
                    l_3316++;
                    if (l_3222)
                    { /* block id: 1404 */
                        (***g_745) = p_12;
                        return p_10;
                    }
                    else
                    { /* block id: 1407 */
                        int16_t l_3319 = 1L;
                        int32_t l_3320 = 0xADA15279L;
                        int32_t l_3321 = 1L;
                        int16_t *****l_3340 = &l_3339;
                        g_3322--;
                        l_3040 = (safe_add_func_float_f_f((((0x503B903E7C2D3BBELL >= ((**l_3142) = p_10)) < (g_2324[3][1][0] &= l_3313)) , l_3316), (safe_div_func_float_f_f((((safe_add_func_int64_t_s_s((((safe_add_func_float_f_f((safe_div_func_float_f_f(0xA.A8B64Bp-38, l_3316)), ((safe_div_func_float_f_f((safe_div_func_float_f_f(l_3320, (((*l_3340) = l_3339) != l_3341[4]))), 0x2.79084Fp+54)) != p_10))) , p_10) | (*g_190)), (*g_785))) , 0xC47C81F9L) , (-0x1.3p+1)), p_10))));
                    }
                }
            }
        }
        (*g_605) ^= ((void*)0 == l_3343[5]);
        if ((safe_lshift_func_int8_t_s_s((-1L), ((((1L != ((((p_10 & 0xB74D00B4C5F408C2LL) , 2UL) & (safe_rshift_func_uint16_t_u_u((0x8F755E7BL || (&l_3311 != (void*)0)), (safe_mul_func_uint16_t_u_u(65529UL, p_10))))) != 18446744073709551612UL)) > p_10) >= l_3310) ^ 0x366DL))))
        { /* block id: 1418 */
            const int8_t **l_3354 = (void*)0;
            const int8_t **l_3355 = (void*)0;
            const int8_t **l_3356[3][8] = {{&l_3353,&l_3353,&l_3353,&l_3353,&l_3353,&l_3353,&l_3353,&l_3353},{&l_3353,&l_3353,&l_3353,&l_3353,&l_3353,&l_3353,&l_3353,&l_3353},{(void*)0,&l_3353,&l_3353,(void*)0,&l_3353,(void*)0,&l_3353,&l_3353}};
            int32_t l_3364 = 6L;
            uint32_t l_3370 = 0x19C3B3DEL;
            uint32_t *l_3371 = (void*)0;
            uint32_t *l_3372 = &g_141;
            int32_t l_3374[1];
            int32_t *l_3375 = (void*)0;
            int32_t *l_3376 = &l_3080;
            int32_t *l_3377[8][9] = {{(void*)0,&g_3034,(void*)0,&g_68,&l_3279,&l_3098,(void*)0,&g_513,(void*)0},{(void*)0,&l_3374[0],&g_3034,&l_3098,&l_3279,&g_513,&l_3279,&l_3098,&g_3034},{&g_3034,&g_3034,(void*)0,(void*)0,&l_3374[0],&l_3374[0],&l_3279,&l_3081[0],(void*)0},{&g_68,&l_3080,&l_3096,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_3374[0],(void*)0,(void*)0,&l_3374[0],&l_3098,&l_3081[0],&l_3279,&l_3096},{&l_3080,(void*)0,(void*)0,&l_3096,&l_3081[0],&l_3374[0],&g_3034,&l_3374[0],&l_3081[0]},{(void*)0,&g_3034,&g_3034,(void*)0,(void*)0,&l_3374[0],&l_3374[0],&l_3279,&l_3081[0]},{&l_3279,(void*)0,&g_68,&g_3034,(void*)0,&l_3096,&l_3374[0],&l_3374[0],&l_3096}};
            int64_t l_3379[9][1][4] = {{{0x1B1041B2CF736BA4LL,0x94E1206B13476F3DLL,9L,0xD04225DE57A74E2BLL}},{{9L,0xD04225DE57A74E2BLL,(-7L),0xD04225DE57A74E2BLL}},{{0x46807325D34DC6FDLL,0x94E1206B13476F3DLL,0x187AFDB92F15ADC3LL,9L}},{{(-8L),0x0DEC1B09A17E688ELL,0xD04225DE57A74E2BLL,7L}},{{0x187AFDB92F15ADC3LL,0x46807325D34DC6FDLL,(-1L),(-1L)}},{{0x187AFDB92F15ADC3LL,0x187AFDB92F15ADC3LL,0xD04225DE57A74E2BLL,(-2L)}},{{(-8L),(-1L),0x187AFDB92F15ADC3LL,0x0DEC1B09A17E688ELL}},{{0x46807325D34DC6FDLL,0x1B1041B2CF736BA4LL,(-7L),0x187AFDB92F15ADC3LL}},{{9L,0x1B1041B2CF736BA4LL,9L,0x0DEC1B09A17E688ELL}}};
            int16_t l_3411[1];
            const int32_t l_3440 = 0x23200BBCL;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_3374[i] = 0x1F8E24DCL;
            for (i = 0; i < 1; i++)
                l_3411[i] = 0x7E00L;
            l_3373 &= (((((*l_3372) &= (safe_sub_func_uint8_t_u_u(((*g_577) != (l_3357 = l_3353)), ((((safe_lshift_func_uint16_t_u_u((+(safe_sub_func_int32_t_s_s((l_3364 < (safe_rshift_func_int8_t_s_s(((l_3364 , l_3367) != (void*)0), ((l_3096 = (0xBACB8A0060B7D337LL < p_10)) <= (((((((p_10 >= 0L) <= (*g_785)) >= (*p_11)) , 0xD3572D8CL) , l_3364) , 0x394F8ED19F6FE89FLL) >= (**g_891)))))), 0xF9B30F69L))), (*g_2438))) == l_3370) <= l_3370) >= 4L)))) == p_10) , (void*)0) != l_3076);
            ++g_3380;
            for (l_3040 = 0; (l_3040 <= 5); l_3040 += 1)
            { /* block id: 1426 */
                int32_t *l_3383 = &l_3364;
                int16_t l_3387 = 0xE8ADL;
                uint8_t l_3389 = 0x57L;
                uint32_t **l_3395 = &l_3371;
                (*g_1736) = (*g_1736);
                if ((*p_11))
                    continue;
                (*g_730) = l_3383;
                for (l_3311 = 3; (l_3311 >= 0); l_3311 -= 1)
                { /* block id: 1432 */
                    int32_t l_3386 = 0x6C3E029DL;
                    int32_t l_3388[2];
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_3388[i] = 1L;
                    l_3386 |= (((safe_lshift_func_uint8_t_u_s((((void*)0 == (*g_577)) != l_3280[l_3311][l_3311]), 1)) > (*p_11)) != (&g_578 == (void*)0));
                    l_3389++;
                    for (g_24 = 1; (g_24 <= 5); g_24 += 1)
                    { /* block id: 1437 */
                        int32_t l_3410[5] = {0x105C9579L,0x105C9579L,0x105C9579L,0x105C9579L,0x105C9579L};
                        int32_t *l_3415[3][1];
                        int i, j;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_3415[i][j] = &l_3081[0];
                        }
                        l_3411[0] ^= (safe_unary_minus_func_int16_t_s((safe_mod_func_int32_t_s_s(((****g_745) = ((p_10 , l_3395) == (*l_3097))), (safe_rshift_func_int8_t_s_u((safe_div_func_int32_t_s_s((l_3280[2][2] = (l_3098 = ((*l_3376) = ((*p_11) = (*p_11))))), ((*l_3372) = ((~(p_10 & (safe_rshift_func_int8_t_s_u((safe_unary_minus_func_uint64_t_u((safe_div_func_int8_t_s_s(((safe_add_func_int16_t_s_s((safe_add_func_int64_t_s_s(l_3311, l_3410[4])), p_10)) | ((((**g_577) = ((void*)0 != &g_1077[l_3311])) , p_10) < (*g_794))), 0x80L)))), l_3410[4])))) ^ p_10)))), 4))))));
                        l_3412[2][4][4]--;
                        (*g_1811) = l_3415[1][0];
                    }
                    for (l_3378 = 0; (l_3378 <= 1); l_3378 += 1)
                    { /* block id: 1451 */
                        if ((*p_11))
                            break;
                    }
                }
            }
            for (g_24 = 1; (g_24 <= 7); g_24 += 1)
            { /* block id: 1458 */
                uint16_t l_3422[6] = {0xBAC0L,0xBAC0L,0xBAC0L,0xBAC0L,0xBAC0L,0xBAC0L};
                int32_t l_3441 = (-1L);
                const int32_t **l_3459 = &g_237;
                const int32_t ***l_3458 = &l_3459;
                const int32_t ****l_3457 = &l_3458;
                uint32_t l_3470[1][4] = {{0UL,0UL,0UL,0UL}};
                int i, j;
                for (g_856 = 0; (g_856 <= 7); g_856 += 1)
                { /* block id: 1461 */
                    int16_t l_3449 = 0xB77CL;
                    for (l_3257 = 0; (l_3257 <= 0); l_3257 += 1)
                    { /* block id: 1464 */
                        uint64_t ***l_3416 = (void*)0;
                        g_891 = &g_892[7][1];
                    }
                    if ((0x9E2CL | (safe_div_func_int16_t_s_s((safe_unary_minus_func_int8_t_s(((++(*g_2438)) & l_3422[2]))), (((l_3279 = (l_3441 = (((safe_add_func_uint64_t_u_u(((safe_mod_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s(((*g_578) != ((((*g_106) = (l_3310 == 0xD6E1L)) , ((safe_mul_func_int16_t_s_s(((*g_1745) >= (safe_div_func_int64_t_s_s((l_3373 || (safe_sub_func_int64_t_s_s(0x973C1B2167F2B85ELL, ((safe_sub_func_int64_t_s_s((((*l_3372) = (((safe_rshift_func_uint16_t_u_u((((((((~(0x1AD9777FL | (*p_11))) > l_3373) >= p_10) , (void*)0) == (void*)0) <= p_10) , p_10), 0)) < 18446744073709551615UL) , (**g_793))) > 0xCA3A3E26L), (**g_891))) && p_10)))), p_10))), p_10)) , (-1L))) == l_3422[3])), l_3440)) ^ p_10), p_10)) , p_10), l_3088)) < p_10) >= (*p_11)))) | l_3373) , 0xF221L)))))
                    { /* block id: 1472 */
                        l_3441 |= (safe_mod_func_int8_t_s_s((safe_sub_func_int32_t_s_s((((safe_lshift_func_uint8_t_u_s(((-1L) & (((-9L) != (((*g_1010) = ((2UL | ((!0UL) | 0UL)) ^ (253UL == l_3449))) <= (**g_891))) , ((0xFA7C5CD0L > 0xCEFD64A7L) & p_10))), (**g_577))) ^ l_3449) < 0xEE1F3028L), (*p_11))), (**g_742)));
                    }
                    else
                    { /* block id: 1475 */
                        (**g_746) = (void*)0;
                        return (*g_106);
                    }
                }
                for (g_165 = 2; (g_165 <= 8); g_165 += 1)
                { /* block id: 1482 */
                    return l_3422[2];
                }
                for (g_1791 = 0; (g_1791 <= 7); g_1791 += 1)
                { /* block id: 1487 */
                    uint16_t l_3467[3][9][4] = {{{3UL,0x7247L,0x48FDL,0x4940L},{0UL,0UL,0UL,0UL},{0xD9C5L,0xD9C5L,0x282AL,65535UL},{0xF72CL,0x9B0EL,0x9304L,0x48FDL},{65535UL,7UL,0x1F16L,0x9304L},{0x282AL,7UL,0x6D34L,0x48FDL},{7UL,0x9B0EL,0UL,65535UL},{0xB25EL,0xD9C5L,6UL,0UL},{0x1F16L,0UL,0x21ABL,0x4940L}},{{6UL,0x7247L,0x4C4FL,0x7247L},{0x9304L,65535UL,0x4940L,7UL},{0x6039L,0x4940L,2UL,5UL},{0x7247L,0x6D34L,0xB72FL,0UL},{0x7247L,1UL,2UL,0x282AL},{0x6039L,0UL,0x4940L,0x9B0EL},{0x9304L,0xF72CL,0x4C4FL,0x21ABL},{6UL,2UL,0x21ABL,3UL},{0x1F16L,6UL,6UL,0x1F16L}},{{0xB25EL,0x48FDL,0UL,65535UL},{7UL,0x08F2L,0x6D34L,0UL},{0x282AL,0x21ABL,0x1F16L,0UL},{65535UL,0x08F2L,0x9304L,65535UL},{0xF72CL,0x48FDL,0x282AL,0x1F16L},{0xD9C5L,6UL,0UL,3UL},{0UL,2UL,0x48FDL,0x21ABL},{3UL,0xF72CL,3UL,0x9B0EL},{0x4C4FL,0UL,0x1F16L,0xD9C5L}}};
                    int i, j, k;
                    for (g_880 = 0; (g_880 <= 7); g_880 += 1)
                    { /* block id: 1490 */
                        uint32_t l_3452[1][7][10] = {{{0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L},{0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L},{4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL},{0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L},{0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L},{4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL,0x0E9B7748L,4294967295UL,4294967295UL},{0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L,0x0E9B7748L,0xD68BB3EDL,0x0E9B7748L}}};
                        int32_t l_3456 = (-8L);
                        int i, j, k;
                        (*l_3376) = ((p_10 & ((safe_div_func_int8_t_s_s((((**g_577) ^ l_3452[0][3][3]) >= ((***l_3344)++)), ((((**g_2437) || (p_10 ^ ((*l_3039) &= ((*g_785) &= ((void*)0 != l_3455[0][0][4]))))) , (((l_3456 = (-2L)) , (g_3460 = l_3457)) != (void*)0)) ^ 0x0EBA38E1L))) , l_3310)) , (*p_11));
                        (*g_483) = (void*)0;
                        (*p_11) ^= ((*l_3376) = l_3452[0][3][3]);
                        if ((*p_11))
                            continue;
                    }
                    for (l_3364 = 6; (l_3364 >= 2); l_3364 -= 1)
                    { /* block id: 1504 */
                        uint32_t * volatile ** volatile *l_3465 = (void*)0;
                        int32_t l_3466 = 0x7FB9B41AL;
                        (**l_3458) = (void*)0;
                        g_3463 = g_3463;
                        l_3467[0][4][2]++;
                        --l_3470[0][3];
                    }
                }
            }
        }
        else
        { /* block id: 1512 */
            int32_t l_3474 = 0xA91B238EL;
            int32_t l_3475[1][1];
            int32_t *l_3476 = &l_3081[0];
            int32_t *l_3477 = &l_3081[0];
            int32_t *l_3478 = &g_32;
            int32_t *l_3479 = (void*)0;
            int32_t *l_3480 = &g_68;
            int32_t *l_3481 = &l_3280[3][0];
            int32_t *l_3482 = &g_513;
            int32_t *l_3483 = &g_3034;
            int32_t *l_3484 = &g_3034;
            int32_t *l_3485 = (void*)0;
            int32_t *l_3486 = (void*)0;
            int32_t *l_3487[2];
            int32_t l_3488 = 0xAFBB8368L;
            uint32_t l_3489 = 18446744073709551613UL;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                    l_3475[i][j] = (-1L);
            }
            for (i = 0; i < 2; i++)
                l_3487[i] = &l_3081[0];
            l_3489--;
            (*l_3480) &= (*p_11);
        }
    }
    (*g_605) = ((safe_lshift_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u(((safe_add_func_uint64_t_u_u(0x83C7407C3ED95B6BLL, (safe_unary_minus_func_uint64_t_u(p_10)))) == (((safe_mul_func_int8_t_s_s((safe_add_func_uint16_t_u_u((((safe_sub_func_uint8_t_u_u((p_10 == (l_3506 = l_3505[1][0])), ((**g_577) >= ((l_3507 = l_3507) != (((*g_2594) < (&l_3353 == &l_3353)) , (void*)0))))) , l_3280[1][1]) < (**g_577)), g_238)), l_3196)) > p_10) & p_10)), l_3280[1][4])) != p_10), l_3280[1][4])) , 0x3C32B5B1L);
    (*g_605) &= ((*p_11) = (safe_div_func_int32_t_s_s((l_3040 >= 0x11206265L), (safe_div_func_uint16_t_u_u((((void*)0 != l_3512) != (safe_rshift_func_uint16_t_u_s((((safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint8_t_u_u(0x9AL, (safe_unary_minus_func_uint64_t_u((!(+(l_3098 ^= ((l_3367 != (l_3525[1] = l_3525[1])) < (((safe_mod_func_int8_t_s_s((!((safe_lshift_func_int8_t_s_s(0x9BL, 0)) , (*g_1463))), l_3196)) >= l_3310) | p_10))))))))), 15)) <= l_3280[2][3]) | l_3373), g_3532))), p_10)))));
    return (*g_106);
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_286 g_605 g_606 g_741 g_841 g_742 g_106 g_96 g_891 g_892 g_137 g_524 g_306 g_513 g_1744 g_1745 g_156 g_794 g_141 g_793 g_2438 g_245 g_150 g_187 g_511 g_1747 g_1791 g_1010 g_785 g_2437 g_190 g_880 g_1463 g_77 g_48 g_577 g_578 g_2324 g_995 g_2563 g_283 g_746 g_747 g_239 g_745 g_2908 g_2356
 * writes: g_32 g_286 g_606 g_742 g_841 g_513 g_141 g_1791 g_511 g_96 g_995 g_283 g_880 g_2548 g_137 g_524 g_306 g_77 g_2324 g_1743 g_2356 g_150 g_187 g_193 g_240
 */
static const uint32_t  func_13(int32_t * const  p_14, int32_t * const  p_15, uint32_t  p_16, uint32_t  p_17, int32_t * p_18)
{ /* block id: 1113 */
    uint8_t l_2660 = 255UL;
    const uint32_t *l_2666 = (void*)0;
    const uint32_t **l_2665 = &l_2666;
    int32_t l_2672 = (-4L);
    int32_t l_2673 = 0L;
    uint8_t **l_2676 = &g_106;
    uint8_t **l_2678 = &g_1460[1];
    uint8_t ***l_2677 = &l_2678;
    float ***** const l_2679[10][10][2] = {{{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510}},{{(void*)0,&g_2510},{(void*)0,&g_2510},{(void*)0,(void*)0},{(void*)0,&g_2510},{(void*)0,(void*)0},{(void*)0,&g_2510},{(void*)0,&g_2510},{(void*)0,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510}},{{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{(void*)0,&g_2510},{&g_2510,&g_2510}},{{(void*)0,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510}},{{&g_2510,&g_2510},{(void*)0,&g_2510},{(void*)0,&g_2510},{(void*)0,(void*)0},{(void*)0,&g_2510},{(void*)0,(void*)0},{(void*)0,&g_2510},{(void*)0,&g_2510},{(void*)0,&g_2510},{&g_2510,&g_2510}},{{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{&g_2510,(void*)0}},{{&g_2510,(void*)0},{&g_2510,&g_2510},{(void*)0,&g_2510},{(void*)0,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{(void*)0,&g_2510},{&g_2510,&g_2510}},{{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510}},{{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{(void*)0,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{(void*)0,&g_2510},{(void*)0,&g_2510}},{{&g_2510,(void*)0},{&g_2510,(void*)0},{&g_2510,&g_2510},{(void*)0,&g_2510},{(void*)0,(void*)0},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,&g_2510},{&g_2510,(void*)0},{(void*)0,&g_2510}}};
    int32_t l_2698 = 1L;
    int32_t l_2699 = 0x90BD2ADCL;
    int32_t l_2700[2];
    uint8_t l_2701 = 0x3FL;
    uint64_t l_2729[7];
    uint16_t l_2751 = 0x54B5L;
    int32_t ****l_2782 = (void*)0;
    int8_t **l_2794 = (void*)0;
    uint8_t ****l_2843 = &l_2677;
    uint8_t *****l_2842 = &l_2843;
    int32_t l_2848 = 0x77608701L;
    int8_t l_2854 = 1L;
    const uint8_t ****l_2860[3][6] = {{&g_1747,&g_1747,&g_1747,&g_1747,&g_1747,&g_1747},{&g_1747,&g_1747,&g_1747,&g_1747,&g_1747,&g_1747},{&g_1747,&g_1747,&g_1747,&g_1747,&g_1747,&g_1747}};
    const uint8_t *****l_2859 = &l_2860[0][4];
    const uint64_t **l_2865 = (void*)0;
    int32_t *****l_2874 = &l_2782;
    int64_t **l_2876 = &g_785;
    int64_t ***l_2875[8][8][2];
    int32_t l_2887 = 0x8DBF9AE2L;
    int32_t l_2896 = 0x363E0BC4L;
    const uint16_t **l_2991 = (void*)0;
    uint32_t ****l_3017[10] = {&g_1471,&g_1471,&g_1471,&g_1471,&g_1471,&g_1471,&g_1471,&g_1471,&g_1471,&g_1471};
    uint32_t **** const l_3032 = &g_1471;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2700[i] = 1L;
    for (i = 0; i < 7; i++)
        l_2729[i] = 0x94A13B36195DDCC6LL;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 2; k++)
                l_2875[i][j][k] = &l_2876;
        }
    }
    for (g_32 = (-14); (g_32 < (-13)); g_32++)
    { /* block id: 1116 */
        int16_t l_2669 = 2L;
        float *l_2670 = (void*)0;
        float *l_2671[4][7] = {{&g_1051,(void*)0,&g_1051,&g_1051,(void*)0,(void*)0,&g_1051},{&g_1051,&g_1051,&g_1051,&g_63,&g_63,&g_63,&g_63},{&g_1051,(void*)0,&g_1051,&g_1051,(void*)0,(void*)0,&g_1051},{&g_1051,&g_1051,&g_1051,&g_63,&g_63,&g_63,&g_63}};
        int i, j;
        for (g_286 = 0; (g_286 <= 5); g_286 += 1)
        { /* block id: 1119 */
            (*g_605) |= (safe_div_func_uint32_t_u_u(0xCC9F2597L, (safe_rshift_func_uint16_t_u_u(((void*)0 != &g_2436[5]), 8))));
        }
        l_2673 = (safe_sub_func_float_f_f((-0x1.3p+1), ((safe_sub_func_float_f_f(0x8.02A46Ep-35, (((l_2672 = (safe_mul_func_float_f_f((((p_17 >= p_17) <= l_2660) <= (safe_mul_func_float_f_f(((p_17 == (l_2665 == &l_2666)) , ((safe_lshift_func_int16_t_s_s(l_2669, 11)) , p_16)), p_16))), l_2660))) > l_2660) != p_17))) <= 0xC.A0FBA8p+39)));
    }
    if ((safe_add_func_int64_t_s_s(p_17, ((((*g_741) = l_2676) == ((*l_2677) = l_2676)) , (l_2679[4][8][1] == &g_2510)))))
    { /* block id: 1127 */
        uint64_t l_2687 = 0x60FD8510D5B57BD9LL;
        int32_t l_2690 = (-1L);
        int32_t *l_2691 = (void*)0;
        int32_t *l_2692 = &g_187;
        int32_t *l_2693 = &l_2690;
        int32_t *l_2694 = (void*)0;
        int32_t *l_2695 = &g_513;
        int32_t l_2696 = 0x1D270B96L;
        int32_t *l_2697[4][5] = {{&l_2696,&g_32,&g_32,&l_2696,&l_2696},{&l_2696,&l_2673,&l_2696,&l_2673,&l_2696},{&l_2696,&l_2696,&g_32,&g_32,&l_2696},{(void*)0,&l_2673,(void*)0,&l_2673,(void*)0}};
        uint8_t ****l_2707 = &l_2677;
        uint8_t *****l_2706 = &l_2707;
        int32_t l_2750[5] = {0x91E4E1AFL,0x91E4E1AFL,0x91E4E1AFL,0x91E4E1AFL,0x91E4E1AFL};
        int i, j;
        for (g_841 = 0; (g_841 < 16); g_841 = safe_add_func_uint16_t_u_u(g_841, 1))
        { /* block id: 1130 */
            int32_t *l_2682 = (void*)0;
            int32_t *l_2683 = (void*)0;
            int32_t l_2684 = (-3L);
            int32_t l_2685[3];
            int32_t *l_2686 = (void*)0;
            int i;
            for (i = 0; i < 3; i++)
                l_2685[i] = (-6L);
            ++l_2687;
            return p_17;
        }
        l_2701--;
        (*l_2695) |= (l_2698 == (safe_add_func_int32_t_s_s(((-1L) != 0xFB81L), (((l_2706 != (void*)0) == ((*l_2693) |= (safe_rshift_func_int8_t_s_u((+(p_17 && (p_16 > (1UL > (safe_mod_func_uint64_t_u_u(((safe_div_func_uint8_t_u_u(l_2700[0], (***g_741))) , (**g_891)), l_2672)))))), 0)))) ^ l_2660))));
        (*l_2695) = (safe_mul_func_float_f_f((safe_sub_func_float_f_f((((*l_2693) = (safe_mul_func_float_f_f((p_17 <= (0x1.0F68CBp-47 == (safe_add_func_float_f_f(p_17, ((((((safe_mul_func_float_f_f(((*g_1744) != (((safe_lshift_func_int8_t_s_s(0x1FL, (safe_lshift_func_int16_t_s_u(((((l_2673 = l_2729[1]) , ((safe_rshift_func_int8_t_s_s((((**g_1744) | (safe_mod_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(((safe_sub_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(((*g_2438) = ((safe_div_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u(((safe_mod_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s((safe_div_func_int64_t_s_s((((l_2700[1] >= ((**g_793) = (*g_794))) , (**g_891)) <= l_2673), 0x57E4F9625D5A5766LL)), 6)) <= 0x4BDEL), 0x1B7BL)) >= 0x6B4C2A5C60533C7ELL), 0x29B185F9EB941F9DLL)) ^ p_17), p_17)) & p_16)), l_2750[0])), 0xC2L)) , 0x9C1DL), p_16)), 65535UL))) && p_17), 4)) >= l_2751)) ^ p_16) , l_2751), (*g_245))))) ^ p_17) , (void*)0)), 0x4.6p+1)) <= 0x1.Cp+1) == l_2699) < 0x3.7p+1) <= 0xD.58362Ap+83) == 0x1.7p-1))))), 0xE.EB656Ap-21))) < (*l_2692)), l_2751)), p_16));
    }
    else
    { /* block id: 1142 */
        const uint32_t *l_2779[1][5][10] = {{{(void*)0,&g_141,&g_141,(void*)0,&g_141,(void*)0,&g_141,&g_141,(void*)0,&g_141},{(void*)0,&g_141,&g_141,(void*)0,&g_141,(void*)0,&g_141,&g_141,(void*)0,&g_141},{(void*)0,&g_141,&g_141,(void*)0,&g_141,(void*)0,&g_141,&g_141,&g_880,(void*)0},{&g_880,(void*)0,(void*)0,&g_880,(void*)0,&g_880,(void*)0,(void*)0,&g_880,(void*)0},{&g_880,(void*)0,(void*)0,&g_880,(void*)0,&g_880,(void*)0,(void*)0,&g_880,(void*)0}}};
        const uint8_t ****l_2786 = &g_1743[3];
        int32_t l_2790 = (-4L);
        int32_t l_2846 = (-1L);
        const uint8_t *****l_2861[5][5] = {{&l_2860[2][5],&l_2860[0][4],&l_2860[2][5],&l_2860[0][4],&l_2860[2][5]},{&l_2860[0][4],(void*)0,(void*)0,&l_2860[0][4],&l_2860[0][4]},{&l_2860[0][4],&l_2860[0][4],&l_2860[0][4],&l_2860[0][4],&l_2860[0][4]},{&l_2860[0][4],&l_2860[0][4],(void*)0,(void*)0,&l_2860[0][4]},{&l_2860[2][5],&l_2860[0][4],&l_2860[2][5],&l_2860[0][4],&l_2860[2][5]}};
        uint32_t *l_2862 = (void*)0;
        uint32_t *l_2863 = &g_880;
        int32_t l_2864 = 0x8730F27EL;
        int i, j, k;
        l_2672 = (p_17 == p_16);
lbl_2849:
        for (p_17 = (-12); (p_17 > 48); p_17++)
        { /* block id: 1146 */
            uint8_t *****l_2755 = (void*)0;
            int32_t l_2780 = 0x70838710L;
            int32_t ****l_2781[1];
            int32_t l_2783 = 1L;
            uint32_t l_2785 = 4294967295UL;
            int i;
            for (i = 0; i < 1; i++)
                l_2781[i] = (void*)0;
            for (g_511 = 0; (g_511 <= 1); g_511 += 1)
            { /* block id: 1149 */
                float l_2784[4];
                const uint8_t ****l_2787 = &g_1743[3];
                int32_t l_2810[3];
                int i;
                for (i = 0; i < 4; i++)
                    l_2784[i] = (-0x1.2p-1);
                for (i = 0; i < 3; i++)
                    l_2810[i] = 1L;
                for (g_286 = 0; (g_286 <= 7); g_286 += 1)
                { /* block id: 1152 */
                    uint32_t *l_2778 = (void*)0;
                    int i, j, k;
                    if (((l_2700[g_511] , (((*g_106) = p_17) == (!(l_2755 == l_2755)))) > ((safe_mod_func_int32_t_s_s(((safe_sub_func_uint8_t_u_u(((((+((safe_sub_func_int8_t_s_s((((((((l_2780 = (safe_lshift_func_uint16_t_u_s(((**g_2437) = (((*g_785) = ((*g_1010) = (safe_sub_func_uint64_t_u_u((((-1L) | (***g_1747)) | (safe_lshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((((safe_sub_func_int32_t_s_s((~(safe_mod_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_u(((l_2778 = &p_17) == ((*l_2665) = l_2779[0][1][5])), 9)), l_2700[g_511]))), l_2780)) || (*g_2438)) ^ 0L) && 0x5EA4B4BCC05ABCEFLL), l_2700[g_511])), l_2729[2]))), 0xEE7E703B9314465ELL)))) | p_17)), 11))) > p_16) , l_2781[0]) != l_2782) , 0x713F2CA3L) > (*g_190)) > l_2783), p_17)) && l_2785)) , l_2786) != l_2787) , (***g_1747)), l_2785)) < 0x3176L), l_2698)) , p_17)))
                    { /* block id: 1160 */
                        int i;
                        l_2700[g_511] = (((-3L) | 0L) == (**g_742));
                    }
                    else
                    { /* block id: 1162 */
                        int16_t l_2788 = (-8L);
                        if (l_2788)
                            break;
                        return p_17;
                    }
                }
                for (g_880 = 0; (g_880 <= 1); g_880 += 1)
                { /* block id: 1169 */
                    int i, j, k;
                    l_2790 = (!p_17);
                    if (l_2790)
                        continue;
                    for (g_841 = 1; (g_841 >= 0); g_841 -= 1)
                    { /* block id: 1174 */
                        int32_t **l_2793 = &g_2548[0];
                        int i;
                        l_2700[g_511] = (safe_lshift_func_uint8_t_u_s(l_2700[g_511], 5));
                        (*l_2793) = ((p_16 <= ((-1L) & (*g_1463))) , p_14);
                    }
                    l_2810[0] = (((void*)0 != l_2794) , (safe_div_func_float_f_f((safe_div_func_float_f_f((((**g_891) &= (safe_lshift_func_uint8_t_u_u(1UL, 3))) , ((((l_2790 = p_17) != p_17) != (l_2783 = ((((p_17 , (safe_mul_func_float_f_f((((-0x3.0p-1) <= ((+(safe_add_func_float_f_f(((safe_div_func_float_f_f(l_2785, (safe_div_func_float_f_f(p_16, 0x9.544F44p+29)))) >= p_17), 0x1.9p+1))) <= 0x2.F9EC9Fp-95)) , p_17), p_16))) <= (-0x7.Ap+1)) < p_17) > p_17))) <= p_16)), 0xE.A59471p+5)), p_16)));
                }
            }
        }
        for (g_286 = 0; (g_286 < 14); g_286 = safe_add_func_int16_t_s_s(g_286, 1))
        { /* block id: 1187 */
            uint64_t l_2821 = 0xABF02F0CA726A1D8LL;
            uint64_t *l_2830 = &g_306;
            const int32_t ***l_2840 = (void*)0;
            int32_t l_2841 = 0x9D5A9B34L;
            uint32_t l_2844 = 0UL;
            uint16_t *l_2845[3][6][4] = {{{&g_165,&l_2751,&g_1767,&g_150},{&g_150,&l_2751,&g_1767,&l_2751},{&l_2751,&g_1767,&g_165,&l_2751},{&g_150,&g_165,&l_2751,&l_2751},{(void*)0,&g_165,&l_2751,&g_1767},{&g_165,&g_1767,&l_2751,&g_165}},{{&g_1767,&l_2751,&g_1767,&l_2751},{&g_1767,&l_2751,&l_2751,&g_1767},{&g_150,&g_150,&l_2751,(void*)0},{&g_150,&g_1767,&g_165,&g_165},{&g_150,&g_1767,&l_2751,&l_2751},{&g_150,&g_165,&l_2751,&g_1767}},{{&g_1767,&g_1767,&g_1767,&l_2751},{&g_1767,&g_150,&l_2751,(void*)0},{&g_165,&l_2751,&l_2751,&l_2751},{(void*)0,&l_2751,&l_2751,(void*)0},{&g_150,&l_2751,&g_165,&g_165},{&l_2751,&g_1767,&g_1767,&l_2751}}};
            int16_t *l_2847 = &g_2324[3][3][0];
            int i, j, k;
            l_2848 &= (safe_rshift_func_uint8_t_u_s((((safe_rshift_func_int16_t_s_u((safe_sub_func_uint64_t_u_u((((l_2790 || l_2699) & ((*l_2847) |= (safe_mod_func_int16_t_s_s((((l_2821++) || l_2673) >= (safe_mod_func_int32_t_s_s((((l_2846 = ((*g_2438) = ((((((*g_106) = (l_2672 && ((safe_div_func_int64_t_s_s(((*g_785) = ((safe_rshift_func_uint16_t_u_u((l_2830 == &l_2729[1]), (((safe_lshift_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(p_16, ((((**g_577) ^= ((safe_unary_minus_func_int8_t_s(((safe_sub_func_int8_t_s_s(((((*l_2830) = (&g_1743[3] == &g_1743[4])) , 4294967295UL) , l_2790), 0xD8L)) <= (-1L)))) != 0x722F9254L)) , l_2840) != l_2840))), l_2841)) > 0UL) ^ l_2790))) >= l_2700[1])), p_17)) > l_2698))) , l_2842) == (void*)0) , p_17) , l_2844))) >= 0x3217L) != l_2844), (-3L)))), 0xEF1EL)))) <= (*g_605)), p_16)), 13)) == p_17) < p_16), p_17));
            if (g_880)
                goto lbl_2849;
            return p_16;
        }
        l_2864 &= (l_2790 ^ (((*l_2863) = ((((p_16 >= (l_2790 , (l_2790 <= p_17))) , ((safe_sub_func_uint64_t_u_u((((safe_add_func_float_f_f(((-0x5.1p-1) <= (l_2854 != (safe_add_func_float_f_f(((safe_add_func_float_f_f(((l_2846 = ((p_16 >= ((l_2859 = (l_2861[1][0] = l_2859)) != &l_2786)) > 0xA.C46286p-43)) < 0x7.1p-1), p_17)) >= p_17), 0xD.BDAA54p-80)))), l_2790)) , l_2846) && (*g_1010)), 0x07FC92C2BD86C564LL)) >= l_2790)) || (*g_106)) > 0xAC98703DD7E3B0A9LL)) != 9L));
    }
    if ((((*g_794) | ((l_2699 , l_2865) != (void*)0)) != (((safe_sub_func_uint64_t_u_u(((l_2672 <= l_2700[1]) == ((safe_mod_func_int64_t_s_s((*g_1010), ((safe_add_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_s((((l_2874 = (void*)0) != (((((**g_742) , (void*)0) == l_2875[4][7][0]) > p_16) , &l_2782)) >= g_2563), p_17)) && (**g_577)), 0x196139EDFA68401DLL)) , (*g_785)))) && 0xE0L)), 7UL)) == 0xA40914C589FBCC56LL) <= l_2701)))
    { /* block id: 1207 */
        int8_t l_2885 = 1L;
        int32_t l_2886 = 0x7C93FD23L;
        uint32_t l_2892 = 18446744073709551607UL;
        for (l_2660 = 16; (l_2660 != 10); l_2660 = safe_sub_func_int64_t_s_s(l_2660, 2))
        { /* block id: 1210 */
            uint16_t l_2881 = 6UL;
            int16_t *l_2884 = &g_2356[0];
            for (g_1791 = (-1); (g_1791 >= 34); g_1791 = safe_add_func_int8_t_s_s(g_1791, 1))
            { /* block id: 1213 */
                for (l_2848 = 0; l_2848 < 6; l_2848 += 1)
                {
                    g_1743[l_2848] = &g_1744;
                }
            }
            (*g_605) |= l_2881;
            if ((safe_lshift_func_int16_t_s_s(((*l_2884) = p_16), (l_2886 ^= l_2885))))
            { /* block id: 1219 */
                l_2887 &= (l_2886 = (*g_190));
            }
            else
            { /* block id: 1222 */
                int32_t *l_2895 = &g_513;
                for (g_150 = 0; (g_150 != 42); g_150 = safe_add_func_int32_t_s_s(g_150, 6))
                { /* block id: 1225 */
                    uint64_t l_2904 = 0xF71711F68FDAE538LL;
                    for (g_187 = 0; (g_187 >= (-14)); --g_187)
                    { /* block id: 1228 */
                        (**g_746) = p_18;
                        l_2892 &= 0x5FFEF23AL;
                    }
                    if (l_2885)
                        break;
                    for (l_2887 = 2; (l_2887 < 16); ++l_2887)
                    { /* block id: 1235 */
                        int32_t *l_2897 = &l_2700[1];
                        int32_t *l_2898 = (void*)0;
                        int32_t l_2899 = 2L;
                        int32_t *l_2900 = (void*)0;
                        int32_t *l_2901 = &l_2699;
                        int32_t *l_2902 = &l_2673;
                        int32_t *l_2903[9] = {&l_2848,&l_2848,&l_2848,&l_2848,&l_2848,&l_2848,&l_2848,&l_2848,&l_2848};
                        int i;
                        (**g_746) = l_2895;
                        l_2904--;
                    }
                }
            }
        }
    }
    else
    { /* block id: 1242 */
        int32_t *l_2907 = &l_2848;
        uint32_t ***l_2918[2];
        const float **l_2928 = &g_1409;
        float **l_2930 = &g_535[0][4][5];
        float ***l_2967 = (void*)0;
        int i;
        for (i = 0; i < 2; i++)
            l_2918[i] = &g_1472;
        for (g_77 = 0; (g_77 <= 7); g_77 += 1)
        { /* block id: 1245 */
            (*g_239) = l_2907;
        }
        (*g_2908) = ((***g_745) = l_2907);
        for (l_2660 = (-29); (l_2660 > 23); l_2660++)
        { /* block id: 1252 */
            float **l_2929 = &g_535[0][0][0];
            int32_t l_2938 = 0x7CE3451DL;
            int32_t l_2939[4];
            int64_t l_2987 = 3L;
            int16_t *l_2996 = &g_123[0];
            int16_t **l_2995 = &l_2996;
            int16_t ***l_2994 = &l_2995;
            int16_t ****l_2993[7][10][3] = {{{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,(void*)0,&l_2994},{&l_2994,&l_2994,&l_2994}},{{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0}},{{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{(void*)0,(void*)0,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994}},{{(void*)0,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994}},{{&l_2994,(void*)0,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994}},{{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,(void*)0},{(void*)0,(void*)0,&l_2994},{&l_2994,&l_2994,&l_2994}},{{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,(void*)0},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994},{(void*)0,&l_2994,&l_2994},{&l_2994,&l_2994,&l_2994}}};
            int32_t l_2999 = 3L;
            int32_t **l_3012 = &g_779;
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_2939[i] = 9L;
        }
    }
    (*g_605) = (p_16 ^ (l_2848 = (l_3017[5] != ((safe_div_func_int64_t_s_s((((((safe_unary_minus_func_int8_t_s((safe_mod_func_uint8_t_u_u((*g_1745), (l_2699 &= (~(safe_rshift_func_uint16_t_u_u(8UL, ((0x5DEABCC8L >= ((safe_add_func_uint32_t_u_u((**g_793), ((safe_mul_func_uint8_t_u_u((l_2672 &= ((**g_742) = (0x75L > ((safe_lshift_func_int8_t_s_u((0x11L <= p_17), p_16)) > (**g_891))))), 0xB0L)) == l_2700[1]))) <= l_2701)) && l_2887))))))))) < l_2887) > g_2356[0]) < p_16) , l_2700[1]), (*g_785))) , l_3032))));
    return p_17;
}


/* ------------------------------------------ */
/* 
 * reads : g_785 g_283 g_1463 g_77 g_48 g_742 g_106 g_96 g_577 g_578 g_32 g_285 g_891 g_892 g_137 g_524 g_306 g_607 g_605 g_1767 g_606 g_1140 g_1791 g_794 g_141 g_746 g_747 g_2436 g_2454 g_2437 g_2438 g_1010 g_995 g_1992 g_1993 g_2548 g_793 g_2585 g_259 g_1239 g_24 g_240 g_187 g_68 g_2594 g_2172 g_2173 g_245 g_150 g_193
 * writes: g_150 g_1460 g_283 g_96 g_32 g_141 g_137 g_524 g_306 g_1767 g_309 g_1791 g_193 g_2436 g_2445 g_606 g_1140 g_856 g_841 g_2454 g_77 g_995 g_165 g_259 g_188 g_2503 g_2510 g_1743 g_511 g_68 g_578
 */
static int32_t * func_20(int8_t  p_21, int8_t  p_22, int64_t  p_23)
{ /* block id: 967 */
    uint64_t l_2380 = 0xBA3859457360C0ABLL;
    int16_t l_2392[3][8][4] = {{{0xA0BDL,0x80B9L,0xA0BDL,0x85FCL},{0xA5A0L,0x4500L,(-3L),0x39A9L},{1L,0xA0BDL,0L,0x4500L},{(-1L),(-1L),0L,(-1L)},{1L,0L,(-3L),(-3L)},{0xA5A0L,0xA5A0L,0xA0BDL,(-1L)},{0xA0BDL,(-1L),0xD3C6L,0x80B9L},{0x80B9L,0L,0xA5A0L,0xD3C6L}},{{0xF9B1L,0L,0x73D2L,0x80B9L},{0L,(-1L),0x85FCL,(-1L)},{(-1L),0xA5A0L,0x4500L,(-3L)},{0xA0BDL,(-1L),1L,0x73D2L},{0x85FCL,1L,(-1L),0xA5A0L},{0x85FCL,(-1L),1L,0xD3C6L},{0xA0BDL,0xA5A0L,0xA5A0L,0xA0BDL},{0x39A9L,(-1L),0xA0BDL,(-3L)}},{{(-1L),0xD3C6L,0x80B9L,0L},{0xB234L,0x73D2L,0xF9B1L,0L},{(-1L),0xD3C6L,0L,(-3L)},{(-1L),(-1L),(-1L),0xA0BDL},{0xF9B1L,0xA5A0L,0x85FCL,0xD3C6L},{0L,(-1L),(-3L),0xA5A0L},{0x4500L,1L,(-3L),0x73D2L},{0L,(-1L),0x85FCL,0x85FCL}}};
    uint32_t l_2393[9][7] = {{4294967290UL,0x1E4E035CL,1UL,0xFD84452CL,0x1E4E035CL,0x80FC2DDDL,0xA76B0EF2L},{4294967290UL,0xF02C85B7L,0xCE5CF1A0L,0xFD84452CL,0xF02C85B7L,0x21B0195BL,0xA76B0EF2L},{0x21B0195BL,0x1E4E035CL,0xCE5CF1A0L,0xCE5CF1A0L,0x1E4E035CL,0x21B0195BL,0x3CF29791L},{4294967290UL,0x1E4E035CL,1UL,0xFD84452CL,0x1E4E035CL,0x80FC2DDDL,0xA76B0EF2L},{4294967290UL,0xF02C85B7L,0xCE5CF1A0L,0xFD84452CL,0xF02C85B7L,0x21B0195BL,0xA76B0EF2L},{0x21B0195BL,0x1E4E035CL,0xCE5CF1A0L,0xCE5CF1A0L,0x1E4E035CL,0x21B0195BL,0x3CF29791L},{4294967290UL,0x1E4E035CL,1UL,0xFD84452CL,0x1E4E035CL,0x80FC2DDDL,0xA76B0EF2L},{4294967290UL,0xF02C85B7L,0xCE5CF1A0L,0xFD84452CL,0xF02C85B7L,0x21B0195BL,0xA76B0EF2L},{0x21B0195BL,0x1E4E035CL,0xCE5CF1A0L,0xCE5CF1A0L,0x1E4E035CL,0x21B0195BL,0x3CF29791L}};
    uint32_t ***l_2394 = (void*)0;
    volatile int32_t *l_2401 = &g_606[0];
    uint16_t *l_2424[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t * const *l_2423 = &l_2424[4];
    int32_t *****l_2443 = &g_1736;
    int32_t l_2446 = 0x7F311488L;
    int32_t l_2528 = 0xD3C1AD62L;
    int32_t l_2529 = (-4L);
    int32_t l_2530 = 8L;
    int32_t l_2534 = 0xC08EB28AL;
    uint32_t l_2545 = 0xC12765BEL;
    int8_t ***l_2570 = &g_577;
    int32_t l_2583 = 1L;
    int16_t l_2615 = (-6L);
    int32_t *l_2616 = &l_2534;
    int8_t *l_2632[8];
    int64_t **l_2633 = &g_1010;
    uint8_t **l_2634 = &g_1460[1];
    int32_t l_2635[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
    int64_t l_2636[9];
    int32_t *l_2644 = &l_2530;
    int32_t *l_2645 = (void*)0;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_2632[i] = &g_48;
    for (i = 0; i < 9; i++)
        l_2636[i] = 0x7EE186793AE9CA03LL;
lbl_2409:
    for (g_150 = 0; g_150 < 8; g_150 += 1)
    {
        g_1460[g_150] = &g_96;
    }
    if ((((safe_mod_func_uint64_t_u_u(((safe_add_func_int64_t_s_s((((*g_785) ^= 0x0014B0E7A36831ADLL) <= (((safe_mul_func_int16_t_s_s((safe_mod_func_uint32_t_u_u((safe_unary_minus_func_uint8_t_u((safe_add_func_uint8_t_u_u(l_2380, ((safe_mul_func_int16_t_s_s((p_21 , ((l_2392[0][7][2] = (safe_sub_func_int64_t_s_s((+(safe_mul_func_uint8_t_u_u(((**g_742) = ((l_2380 , (safe_add_func_float_f_f(l_2380, l_2380))) , ((l_2380 != (*g_1463)) < ((safe_mod_func_uint8_t_u_u((**g_742), l_2380)) && p_23)))), (*g_1463)))), l_2380))) && (**g_577))), 0xFA26L)) && l_2392[0][4][0]))))), 4294967295UL)), l_2393[8][3])) & 18446744073709551607UL) && 0x4DL)), p_22)) > l_2393[6][3]), l_2393[8][3])) , &g_1992) == l_2394))
    { /* block id: 972 */
        float l_2397 = 0x8.72B592p+31;
        int32_t l_2403[9][1] = {{0x22C6A011L},{0x16029222L},{0x22C6A011L},{0x16029222L},{0x22C6A011L},{0x16029222L},{0x22C6A011L},{0x16029222L},{0x22C6A011L}};
        int32_t l_2425 = (-2L);
        float l_2426 = 0x2.Fp+1;
        int32_t ****l_2435[3][10] = {{&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280},{&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280},{&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280,&g_1280}};
        uint16_t ****l_2439 = &g_2436[5];
        uint16_t ****l_2440 = (void*)0;
        uint16_t ***l_2442 = (void*)0;
        uint16_t ****l_2441 = &l_2442;
        int32_t *****l_2444 = &g_1736;
        int i, j;
        for (g_32 = 3; (g_32 >= 0); g_32 -= 1)
        { /* block id: 975 */
            uint32_t *l_2395 = &g_141;
            uint64_t *l_2398 = &l_2380;
            volatile int32_t *l_2399[6] = {&g_1140,&g_1140,&g_1140,&g_1140,&g_1140,&g_1140};
            volatile int32_t **l_2400[3];
            int i;
            for (i = 0; i < 3; i++)
                l_2400[i] = &l_2399[4];
            if (g_285[g_32])
                break;
            l_2399[0] = ((((*l_2395) = 0x0081CEE7L) | (safe_unary_minus_func_uint64_t_u(((*l_2398) ^= ((**g_891) &= p_21))))) , (*g_607));
            l_2401 = l_2399[5];
            for (g_1767 = 0; (g_1767 <= 3); g_1767 += 1)
            { /* block id: 984 */
                float *l_2402 = &g_309;
                int32_t *l_2404 = (void*)0;
                int32_t *l_2405 = &l_2403[7][0];
                int16_t ***l_2414 = (void*)0;
                l_2403[0][0] = ((*l_2402) = ((*l_2401) == p_22));
                if (((*l_2405) = 0x239F508DL))
                { /* block id: 988 */
                    uint32_t l_2406 = 7UL;
                    --l_2406;
                    if (p_22)
                        goto lbl_2409;
                }
                else
                { /* block id: 991 */
                    int16_t ****l_2415 = (void*)0;
                    int16_t *l_2419 = &l_2392[0][6][1];
                    int16_t **l_2418 = &l_2419;
                    int16_t ***l_2417 = &l_2418;
                    int16_t ****l_2416 = &l_2417;
                    int32_t l_2422 = 0x1D6552DCL;
                    (**g_746) = (((safe_mul_func_int8_t_s_s((safe_div_func_int8_t_s_s(((((((((**g_891) , l_2414) == ((*l_2416) = (void*)0)) | ((l_2403[0][0] >= (g_1791 &= (((*g_106) = 0x21L) , (l_2425 ^= ((safe_rshift_func_uint8_t_u_s(0xD7L, (l_2422 , (*l_2401)))) , (((p_21 , (void*)0) != l_2423) && (*l_2405))))))) || (*g_794))) & p_23) ^ p_23) | l_2422) || p_23), (*l_2405))), p_21)) , 0UL) , (void*)0);
                }
                return l_2402;
            }
        }
        (*l_2401) = (safe_mul_func_float_f_f(l_2403[3][0], ((((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((l_2435[1][4] != (void*)0), p_21)) != (p_21 > (&l_2423 == ((*l_2441) = ((*l_2439) = g_2436[5]))))), ((l_2444 = l_2443) == (g_2445 = &l_2435[1][8])))), (-0x1.Fp+1))) == p_21) == l_2446) < p_21)));
    }
    else
    { /* block id: 1006 */
        int8_t l_2473 = (-1L);
        int32_t l_2482 = 0xFDAD2A3BL;
        int32_t * const * const * const * const *l_2502 = (void*)0;
        int32_t l_2532 = 1L;
        int32_t l_2535 = 0xBAB34661L;
        int32_t l_2536 = 0L;
        int32_t l_2537 = 0L;
        int32_t l_2538 = 0x9F9E3E25L;
        int32_t l_2540 = 0xB304DB0AL;
        int32_t l_2542 = (-1L);
        int32_t l_2543 = 0xBC621D0DL;
        int64_t l_2584[8];
        uint32_t *****l_2587[3][10] = {{&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586},{&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586},{&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586,&g_2586}};
        uint8_t *l_2612 = &g_511;
        int32_t *l_2613 = &g_68;
        uint8_t l_2614[10] = {252UL,0x53L,252UL,0UL,0UL,252UL,0x53L,252UL,0UL,0UL};
        int8_t **l_2631 = (void*)0;
        uint64_t l_2637 = 0UL;
        int i, j;
        for (i = 0; i < 8; i++)
            l_2584[i] = (-1L);
        for (g_137 = 0; (g_137 < 14); ++g_137)
        { /* block id: 1009 */
            int32_t l_2456 = 1L;
            float * const *l_2475 = &g_535[0][0][7];
            float * const **l_2474[6] = {&l_2475,&l_2475,&l_2475,&l_2475,&l_2475,&l_2475};
            uint32_t ***l_2489 = &g_1472;
            int8_t **l_2492[3][10][8] = {{{&g_578,&g_1463,&g_1463,&g_1463,&g_1463,&g_578,&g_578,&g_1463},{&g_578,&g_578,&g_1463,&g_578,(void*)0,(void*)0,&g_578,&g_578},{&g_1463,(void*)0,&g_1463,&g_1463,&g_1463,&g_1463,(void*)0,&g_1463},{&g_578,&g_1463,(void*)0,&g_1463,&g_578,&g_1463,&g_1463,&g_578},{&g_1463,&g_578,&g_1463,&g_1463,&g_1463,&g_1463,&g_1463,&g_1463},{&g_1463,&g_1463,&g_1463,(void*)0,&g_1463,&g_578,&g_1463,(void*)0},{&g_578,(void*)0,&g_578,&g_1463,(void*)0,(void*)0,&g_578,(void*)0},{(void*)0,&g_1463,&g_578,(void*)0,(void*)0,&g_578,(void*)0,&g_1463},{&g_578,(void*)0,&g_1463,&g_1463,(void*)0,&g_1463,&g_1463,&g_578},{&g_578,&g_1463,&g_1463,&g_1463,&g_1463,&g_578,&g_578,&g_1463}},{{&g_1463,&g_578,&g_1463,&g_1463,&g_1463,&g_578,&g_1463,&g_578},{&g_578,(void*)0,&g_1463,&g_578,&g_1463,&g_1463,(void*)0,&g_1463},{&g_578,&g_1463,&g_578,&g_1463,&g_1463,&g_1463,&g_1463,&g_1463},{&g_578,&g_1463,&g_578,&g_1463,&g_1463,&g_1463,(void*)0,&g_578},{&g_1463,&g_578,&g_1463,(void*)0,&g_1463,&g_578,&g_1463,(void*)0},{&g_578,&g_578,&g_578,&g_1463,(void*)0,&g_578,&g_578,(void*)0},{&g_578,&g_578,&g_578,&g_578,(void*)0,&g_578,(void*)0,&g_578},{(void*)0,(void*)0,&g_1463,&g_1463,(void*)0,&g_1463,&g_1463,&g_1463},{&g_578,(void*)0,&g_1463,&g_1463,&g_1463,&g_578,&g_1463,&g_578},{&g_1463,&g_578,&g_1463,&g_578,&g_1463,&g_578,&g_578,&g_578}},{{&g_1463,&g_578,&g_578,&g_578,&g_578,&g_578,&g_578,&g_1463},{&g_1463,&g_578,&g_1463,(void*)0,&g_578,(void*)0,&g_1463,&g_578},{&g_1463,&g_578,&g_578,(void*)0,&g_1463,&g_1463,&g_1463,&g_578},{&g_1463,&g_578,&g_1463,(void*)0,&g_1463,&g_1463,&g_1463,&g_578},{&g_1463,(void*)0,&g_578,&g_1463,&g_578,&g_1463,&g_1463,&g_1463},{&g_578,&g_1463,&g_1463,&g_1463,&g_1463,&g_1463,&g_578,(void*)0},{&g_578,&g_1463,&g_578,&g_1463,&g_578,&g_1463,&g_578,&g_578},{(void*)0,&g_1463,&g_1463,&g_1463,&g_1463,(void*)0,&g_578,(void*)0},{&g_1463,&g_578,(void*)0,&g_578,&g_1463,&g_1463,&g_578,&g_1463},{&g_1463,(void*)0,&g_1463,&g_578,&g_578,&g_578,(void*)0,(void*)0}}};
            int32_t l_2499 = 0xE0652BE2L;
            int32_t l_2525 = 0x49FA879EL;
            int32_t l_2539[8];
            uint32_t *l_2557 = (void*)0;
            uint32_t *l_2558[1][9][5] = {{{(void*)0,&g_141,&g_141,(void*)0,&g_141},{&l_2545,(void*)0,(void*)0,&l_2545,&l_2545},{&g_141,(void*)0,&g_141,(void*)0,&g_141},{(void*)0,(void*)0,(void*)0,&l_2545,(void*)0},{&g_856,&g_880,(void*)0,(void*)0,&g_880},{&g_141,(void*)0,&l_2545,(void*)0,(void*)0},{(void*)0,(void*)0,&g_141,&g_141,&g_141},{(void*)0,(void*)0,(void*)0,&g_141,&l_2545},{(void*)0,&g_880,&g_856,&g_141,&g_141}}};
            const int8_t *l_2562 = &g_2563;
            const int8_t **l_2561 = &l_2562;
            int8_t ****l_2571 = &l_2570;
            int8_t ***l_2573 = &l_2492[2][0][7];
            int8_t ****l_2572 = &l_2573;
            const uint8_t ***l_2579 = &g_1744;
            const uint8_t ****l_2580 = &g_1743[3];
            uint8_t ** const *l_2581 = &g_742;
            const float l_2582 = 0x5.AD0C4Dp-0;
            int64_t *l_2588[3][1][2];
            int32_t l_2589 = (-1L);
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_2539[i] = 0L;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 2; k++)
                        l_2588[i][j][k] = &g_259;
                }
            }
            for (g_856 = 7; (g_856 <= 2); g_856--)
            { /* block id: 1012 */
                volatile int32_t * volatile *l_2455[8][1][7];
                int i, j, k;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 7; k++)
                            l_2455[i][j][k] = &g_605;
                    }
                }
                for (g_841 = 3; (g_841 >= 0); g_841 -= 1)
                { /* block id: 1015 */
                    int8_t l_2451 = 0xD9L;
                    int32_t *l_2452 = &l_2446;
                    int32_t l_2453[9][3][1] = {{{0x47FA05C2L},{0x4BCE8A26L},{0x8C8CD203L}},{{(-10L)},{0x8C8CD203L},{0x4BCE8A26L}},{{0x47FA05C2L},{0x4BCE8A26L},{0x8C8CD203L}},{{(-10L)},{0x8C8CD203L},{0x4BCE8A26L}},{{0x47FA05C2L},{0x4BCE8A26L},{0x8C8CD203L}},{{(-10L)},{0x8C8CD203L},{0x4BCE8A26L}},{{0x47FA05C2L},{0x4BCE8A26L},{0x8C8CD203L}},{{(-10L)},{0x8C8CD203L},{0x4BCE8A26L}},{{0x47FA05C2L},{0x4BCE8A26L},{0x8C8CD203L}}};
                    int i, j, k;
                    l_2453[5][0][0] &= ((*l_2452) |= (l_2451 = (&p_22 != (void*)0)));
                    l_2401 = (void*)0;
                }
                g_2454 = g_2454;
            }
            for (g_77 = 1; (g_77 >= 0); g_77 -= 1)
            { /* block id: 1025 */
                int16_t l_2461 = 1L;
                int16_t *l_2480[4][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                int32_t l_2481 = (-1L);
                uint32_t ***l_2488[2][1][1];
                uint32_t *l_2498 = &g_286;
                float ****l_2509[10][7] = {{&g_1412,(void*)0,&g_1412,&g_1412,&g_1412,&g_1412,&g_1412},{&g_1412,&g_1412,(void*)0,&g_1412,&g_1412,&g_1412,&g_1412},{&g_1412,&g_1412,&g_1412,&g_1412,(void*)0,(void*)0,&g_1412},{&g_1412,&g_1412,(void*)0,&g_1412,(void*)0,&g_1412,&g_1412},{&g_1412,&g_1412,(void*)0,&g_1412,&g_1412,&g_1412,&g_1412},{&g_1412,(void*)0,&g_1412,&g_1412,&g_1412,(void*)0,(void*)0},{&g_1412,&g_1412,(void*)0,&g_1412,&g_1412,&g_1412,&g_1412},{&g_1412,&g_1412,(void*)0,&g_1412,&g_1412,&g_1412,&g_1412},{&g_1412,(void*)0,&g_1412,&g_1412,(void*)0,(void*)0,&g_1412},{&g_1412,&g_1412,&g_1412,(void*)0,&g_1412,(void*)0,&g_1412}};
                int32_t l_2533 = 9L;
                int32_t l_2544 = 0x57DF2232L;
                int i, j, k;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_2488[i][j][k] = &g_1472;
                    }
                }
                (*g_605) = (((l_2456 ^ (0x57FEED6841EEF570LL ^ ((*g_1010) ^= (((safe_mod_func_int64_t_s_s(0x6395A3B436862238LL, (*g_785))) , ((safe_lshift_func_uint16_t_u_u(l_2461, (safe_div_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((l_2482 &= ((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(((l_2481 ^= ((!(safe_lshift_func_uint16_t_u_s((((l_2473 ^= 0x22L) , l_2474[5]) != &l_2475), 2))) , (safe_sub_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(l_2456, 3)), 0L)))) || p_21), (**g_577))), 1UL)) != l_2461)), (**g_2437))), p_23)))) != 1UL)) || p_22)))) & 1L) , l_2473);
                for (g_165 = 2; (g_165 <= 9); g_165 += 1)
                { /* block id: 1033 */
                    int32_t *l_2483 = (void*)0;
                    int8_t ***l_2493 = &l_2492[2][8][6];
                    for (g_259 = 0; (g_259 <= 9); g_259 += 1)
                    { /* block id: 1036 */
                        return l_2483;
                    }
                    l_2499 = (safe_div_func_float_f_f((safe_sub_func_float_f_f(0x6.AB4965p+35, ((((l_2488[0][0][0] = l_2488[0][0][0]) == l_2489) < ((*g_1992) != ((safe_div_func_uint8_t_u_u((p_21 & (0xCBAC53C36080E915LL < (((*l_2493) = l_2492[2][8][6]) != (void*)0))), (safe_div_func_int64_t_s_s((p_23 ^= (((safe_lshift_func_int8_t_s_u((p_22 &= 0L), 4)) && p_22) != p_21)), p_21)))) , l_2498))) < 0x3.CD36BCp+3))), p_21));
                }
                g_188[(g_77 + 4)] = &l_2499;
                for (g_141 = 0; (g_141 <= 1); g_141 += 1)
                { /* block id: 1048 */
                    uint8_t l_2517[3][8][6] = {{{0x98L,0x2EL,0xC1L,0x2EL,0x98L,0x0DL},{0xC0L,1UL,0xB4L,255UL,0x7BL,0x0DL},{1UL,255UL,0xC1L,1UL,0x4AL,0x0DL},{0x57L,0x2EL,0xB4L,0x2EL,0x57L,0x0DL},{0x4AL,1UL,0xC1L,255UL,1UL,0x0DL},{0x7BL,255UL,0xB4L,1UL,0xC0L,0x0DL},{0x98L,0x2EL,0xC1L,0x2EL,0x98L,0x0DL},{0xC0L,1UL,0xB4L,255UL,0x7BL,0x0DL}},{{1UL,255UL,0xC1L,1UL,0x4AL,0x0DL},{0x57L,0x2EL,0xB4L,0x2EL,0x57L,0x0DL},{0x4AL,1UL,0xC1L,255UL,1UL,0x0DL},{0x7BL,255UL,0xB4L,1UL,0xC0L,0x0DL},{0x98L,0x2EL,0xC1L,0x2EL,0x98L,0x0DL},{0xC0L,1UL,0xB4L,255UL,0x7BL,0x0DL},{1UL,255UL,0xC1L,1UL,0x4AL,0x0DL},{0x57L,0x2EL,0xB4L,0x2EL,0x57L,0x0DL}},{{0x4AL,1UL,0xC1L,255UL,1UL,0x0DL},{0x7BL,255UL,0xB4L,1UL,0xC0L,0x0DL},{0x98L,0x2EL,0xC1L,0x2EL,0x98L,0x0DL},{0xC0L,1UL,0xB4L,255UL,0x7BL,0x0DL},{1UL,255UL,0xC1L,1UL,0x4AL,0x0DL},{0x57L,0x2EL,0xB4L,0x2EL,0x57L,0x0DL},{0x4AL,1UL,1UL,1UL,254UL,255UL},{0x02L,1UL,0x7BL,0xD4L,0UL,255UL}}};
                    int32_t l_2520 = 0xC59F230FL;
                    int32_t l_2526 = 0x2B1C9C6FL;
                    int32_t l_2527 = 0xE62A4A42L;
                    int32_t l_2531 = (-6L);
                    int32_t l_2541 = 0x52B42D1CL;
                    int i, j, k;
                    for (g_165 = 0; (g_165 <= 1); g_165 += 1)
                    { /* block id: 1051 */
                        uint8_t l_2504 = 1UL;
                        float ****l_2508 = &g_1412;
                        float *****l_2507 = &l_2508;
                        int32_t l_2518 = (-5L);
                        int32_t *l_2519 = &l_2482;
                        int32_t *l_2521 = &g_68;
                        int32_t *l_2522 = &l_2518;
                        int32_t *l_2523 = &g_513;
                        int32_t *l_2524[1][2];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_2524[i][j] = &l_2446;
                        }
                        l_2499 = (((l_2482 = (safe_div_func_float_f_f(((p_22 < 0x1.B82195p+20) , (l_2504 = (&g_745 != (g_2503 = l_2502)))), 0x5.8p+1))) <= (safe_div_func_float_f_f((((*l_2507) = ((**g_2437) , &g_1412)) == (g_2510 = l_2509[0][0])), ((l_2518 = (safe_sub_func_float_f_f(((l_2517[0][2][4] = (safe_add_func_float_f_f((safe_mul_func_float_f_f(p_22, 0xC.591C3Bp-92)), 0x3.3894EBp-6))) , (-0x2.8p-1)), 0x9.D30CC3p+21))) >= 0x2.7p+1)))) < 0xE.A5D856p+96);
                        ++l_2545;
                    }
                    return g_2548[5];
                }
            }
            l_2589 ^= (((safe_div_func_int16_t_s_s((((**g_2437) = ((l_2525 && (((safe_mod_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((**g_793)++) | ((g_259 &= ((((((*l_2561) = &p_21) == ((((l_2587[1][4] = ((safe_add_func_uint8_t_u_u(((*g_785) , ((safe_add_func_int64_t_s_s((((((&g_577 != ((*l_2572) = ((*l_2571) = l_2570))) | 0UL) == (p_23 < (safe_sub_func_int16_t_s_s((~(l_2499 = (((((safe_div_func_uint64_t_u_u((((*l_2580) = l_2579) != l_2581), (**g_891))) == (*g_578)) <= p_21) ^ p_23) != 1UL))), (**g_2437))))) > l_2583) && l_2584[1]), (*g_1010))) != 0x242D1A56L)), (*g_578))) , g_2585[0])) != (void*)0) | p_22) , (*g_577))) , p_23) != l_2539[0]) <= (**g_891))) == p_21)), g_1239)), g_24)), (*g_240))) , (**g_891)) & p_22)) > 0x28L)) == l_2456), p_23)) , &l_2456) == (void*)0);
            for (g_32 = (-8); (g_32 != 24); g_32 = safe_add_func_uint64_t_u_u(g_32, 8))
            { /* block id: 1077 */
                uint64_t l_2597 = 8UL;
                int32_t *l_2598 = &l_2542;
                for (g_77 = (-16); (g_77 > (-4)); g_77 = safe_add_func_int16_t_s_s(g_77, 6))
                { /* block id: 1080 */
                    volatile int32_t * volatile l_2595[7];
                    int i;
                    for (i = 0; i < 7; i++)
                        l_2595[i] = &g_1140;
                    l_2595[6] = g_2594;
                }
                l_2534 |= ((((~l_2597) || p_23) , l_2499) , ((*l_2598) = ((l_2597 == 0x4.451A0Ep-65) , 1L)));
                (*g_605) &= (*l_2598);
            }
        }
        (**g_746) = (((((*g_578) <= 0x85L) , (safe_sub_func_uint8_t_u_u((p_22 , (safe_sub_func_int64_t_s_s((safe_div_func_int16_t_s_s((safe_unary_minus_func_uint8_t_u((safe_sub_func_int64_t_s_s((((*l_2613) = ((safe_add_func_int64_t_s_s(((p_21 &= ((***l_2570) |= (safe_lshift_func_int8_t_s_s(0x86L, 4)))) && p_23), ((((**g_742) = p_23) == (((*l_2612) = ((l_2532 == ((p_23 ^ (p_22 >= 0x41L)) <= p_22)) != 7L)) > p_23)) , 1L))) & 0x93C795F71308A2A7LL)) == l_2614[8]), (*g_1010))))), l_2615)), (*g_1010)))), p_23))) , 0x3BL) , l_2616);
        (*g_193) = (safe_sub_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_s(((safe_div_func_uint32_t_u_u(((*l_2616) | (((safe_mod_func_uint32_t_u_u((((safe_div_func_float_f_f(((safe_sub_func_float_f_f((((safe_rshift_func_int8_t_s_s(((((**l_2570) = (**l_2570)) != (l_2632[6] = &p_21)) < ((*g_2172) != l_2633)), (((*l_2613) = (((void*)0 == l_2634) == (p_23 , (0xE22147C3L < p_21)))) == p_21))) , p_21) < l_2635[7]), l_2636[3])) <= 0x4.11BC2Bp-73), 0x1.067932p-60)) < p_21) , p_21), p_22)) != p_22) != 1UL)), p_23)) | l_2637), 10)) >= (*g_245)), p_21));
        for (g_32 = 0; (g_32 <= 7); g_32 = safe_add_func_int64_t_s_s(g_32, 9))
        { /* block id: 1100 */
            int32_t *l_2640 = (void*)0;
            return l_2640;
        }
    }
    for (g_141 = 0; g_141 < 10; g_141 += 1)
    {
        g_188[g_141] = &g_187;
    }
    for (g_1791 = (-23); (g_1791 <= 40); g_1791++)
    { /* block id: 1107 */
        int32_t *l_2643 = &g_32;
        if (p_22)
            break;
        (*g_747) = l_2643;
    }
    return l_2645;
}


/* ------------------------------------------ */
/* 
 * reads : g_880 g_1239 g_1791 g_605 g_606 g_891 g_892 g_1463 g_77 g_48 g_785 g_283 g_742 g_106 g_96 g_1010 g_995 g_745 g_746 g_747 g_793 g_794 g_123 g_137 g_524 g_306 g_245 g_150 g_1747 g_1744 g_1745 g_156 g_239 g_240 g_187 g_68 g_1992 g_1994 g_1993 g_286 g_1472 g_1473 g_141 g_577 g_578 g_607 g_513 g_236 g_2109 g_165 g_856 g_2165 g_237 g_238 g_259 g_2172 g_1437 g_2173 g_190 g_511 g_2264 g_751 g_752 g_2285 g_2286 g_32 g_2324 g_186 g_2356 g_1767
 * writes: g_880 g_1239 g_1791 g_187 g_606 g_150 g_1743 g_892 g_165 g_193 g_137 g_524 g_306 g_283 g_950 g_513 g_68 g_1767 g_66 g_1473 g_856 g_286 g_77 g_259 g_1437 g_32 g_578
 */
static const int16_t  func_26(uint32_t  p_27, int32_t * const  p_28)
{ /* block id: 727 */
    const int64_t l_1839[7] = {0L,0L,0L,0L,0L,0L,0L};
    int32_t l_1879 = 0x9F340916L;
    int32_t l_1880 = 0x3676140BL;
    int32_t l_1881[9][6] = {{0xEAF91D70L,1L,0x074FF105L,1L,0xEAF91D70L,0xEAF91D70L},{0xA5E01542L,1L,1L,0xA5E01542L,(-1L),0xA5E01542L},{0xA5E01542L,(-1L),0xA5E01542L,1L,1L,0xA5E01542L},{0xEAF91D70L,0xEAF91D70L,1L,0x074FF105L,1L,0xEAF91D70L},{1L,(-1L),0x074FF105L,0x074FF105L,(-1L),1L},{0xEAF91D70L,1L,0x074FF105L,1L,0xEAF91D70L,0xEAF91D70L},{0xA5E01542L,1L,1L,0xA5E01542L,(-1L),0xA5E01542L},{0xA5E01542L,(-1L),0xA5E01542L,1L,1L,0xA5E01542L},{0xEAF91D70L,0xEAF91D70L,1L,0x074FF105L,1L,0xEAF91D70L}};
    int16_t l_1891 = 0x87EBL;
    int32_t *l_1896 = &g_32;
    int32_t l_1920 = 1L;
    int8_t l_1933 = 0x00L;
    int64_t l_1955 = 0L;
    float ****l_2064 = &g_1412;
    float ****l_2070[2];
    uint64_t l_2154 = 1UL;
    uint8_t l_2255 = 0xC2L;
    uint32_t ****l_2268 = &g_1471;
    uint32_t *****l_2267 = &l_2268;
    uint32_t l_2314 = 1UL;
    int32_t *l_2323[1];
    int16_t l_2325 = 0x2495L;
    int i, j;
    for (i = 0; i < 2; i++)
        l_2070[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_2323[i] = &l_1879;
    if ((p_27 == (l_1839[6] == 0xDEBFL)))
    { /* block id: 728 */
        int16_t l_1865 = 0L;
        int32_t l_1867 = 0x9BA6F108L;
        int16_t l_1878 = (-10L);
        int32_t l_1882 = 0x843EC7E8L;
        int32_t l_1883 = 0x8DD799D7L;
        int32_t l_1884 = (-7L);
        int32_t l_1885 = (-2L);
        int32_t l_1886 = 0xA4F3AC8EL;
        int32_t l_1887 = 1L;
        int32_t l_1888 = 0L;
        int32_t l_1889 = (-1L);
        int32_t l_1890 = 0xE573AD14L;
        for (p_27 = 0; (p_27 > 57); p_27++)
        { /* block id: 731 */
            uint32_t ****l_1855 = &g_1471;
            int32_t l_1860 = 0L;
            int32_t l_1861[8][7] = {{0xA9579171L,0xFD7D934CL,0x67690A9AL,0xA9579171L,0x67690A9AL,0xFD7D934CL,0xA9579171L},{(-8L),0L,0xF9455EA1L,0L,0L,0L,0xF9455EA1L},{0xA9579171L,0xA9579171L,1L,0xB6170F2DL,(-6L),1L,(-6L)},{0x18AB0FB6L,0xF9455EA1L,0xF9455EA1L,0x18AB0FB6L,0L,(-8L),0x18AB0FB6L},{0L,(-6L),0x67690A9AL,0x67690A9AL,(-6L),0L,0xFD7D934CL},{0xC9E3F5BFL,0x18AB0FB6L,0x387BACD1L,0L,0L,0x387BACD1L,0x18AB0FB6L},{(-6L),0xFD7D934CL,0L,(-6L),0x67690A9AL,0x67690A9AL,(-6L)},{(-8L),0x18AB0FB6L,(-8L),0L,0x18AB0FB6L,0xF9455EA1L,0xF9455EA1L}};
            uint64_t l_1862 = 18446744073709551612UL;
            int32_t *l_1866 = &l_1861[6][2];
            int32_t *l_1868 = &g_187;
            int32_t *l_1869 = &l_1861[3][4];
            int32_t *l_1870 = &g_68;
            int32_t *l_1871 = &l_1867;
            int32_t *l_1872 = &g_513;
            int32_t *l_1873 = &l_1867;
            int32_t *l_1874 = &g_187;
            int32_t *l_1875 = (void*)0;
            int32_t *l_1876 = &l_1860;
            int32_t *l_1877[10][5][5] = {{{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]},{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0},{(void*)0,(void*)0,&g_32,&l_1861[1][3],&l_1861[1][3]},{&l_1860,&l_1861[1][3],&l_1861[6][2],&l_1861[1][3],&l_1860},{&l_1861[1][3],&l_1861[1][3],&g_32,(void*)0,(void*)0}},{{(void*)0,&l_1861[1][3],(void*)0,&g_513,(void*)0},{&l_1861[1][3],(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0},{&l_1860,&g_513,&l_1861[6][2],&g_513,&l_1860},{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]},{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0}},{{(void*)0,(void*)0,&g_32,&l_1861[1][3],&l_1861[1][3]},{&l_1860,&l_1861[1][3],&l_1861[6][2],&l_1861[1][3],&l_1860},{&l_1861[1][3],&l_1861[1][3],&g_32,(void*)0,(void*)0},{(void*)0,&l_1861[1][3],(void*)0,&g_513,(void*)0},{&l_1861[1][3],(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0}},{{&l_1860,&g_513,&l_1861[6][2],&g_513,&l_1860},{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]},{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0},{(void*)0,(void*)0,&g_32,&l_1861[1][3],&l_1861[1][3]},{&l_1860,&l_1861[1][3],&l_1861[6][2],&l_1861[1][3],&l_1860}},{{&l_1861[1][3],&l_1861[1][3],&g_32,(void*)0,(void*)0},{(void*)0,&l_1861[1][3],(void*)0,&g_513,(void*)0},{&l_1861[1][3],(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0},{&l_1860,&g_513,&l_1861[6][2],&g_513,&l_1860},{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]}},{{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0},{(void*)0,(void*)0,&g_32,&l_1861[1][3],&l_1861[1][3]},{&l_1860,&l_1861[1][3],&l_1861[6][2],&l_1861[1][3],&l_1860},{&l_1861[1][3],&l_1861[1][3],&g_32,(void*)0,(void*)0},{(void*)0,&l_1861[1][3],(void*)0,&g_513,(void*)0}},{{&l_1861[1][3],(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0},{&l_1860,&g_513,&l_1861[6][2],&g_513,&l_1860},{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]},{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0},{(void*)0,(void*)0,&g_32,&l_1861[1][3],&l_1861[1][3]}},{{&l_1860,&l_1861[1][3],&l_1861[6][2],&l_1861[1][3],&l_1860},{&l_1861[1][3],&l_1861[1][3],&g_32,(void*)0,(void*)0},{(void*)0,&l_1861[1][3],(void*)0,&g_513,(void*)0},{&l_1861[1][3],(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0},{&l_1860,&g_513,&l_1861[6][2],&g_513,&l_1860}},{{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]},{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0},{(void*)0,(void*)0,&g_32,&l_1861[1][3],&l_1861[1][3]},{&l_1860,&l_1861[1][3],&l_1861[6][2],&l_1861[1][3],&l_1860},{&l_1861[1][3],&l_1861[1][3],&g_32,(void*)0,(void*)0}},{{(void*)0,&l_1861[1][3],(void*)0,&g_513,(void*)0},{&l_1861[1][3],(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0},{&l_1860,&g_513,&l_1861[6][2],&g_513,&l_1860},{(void*)0,&l_1861[1][3],&l_1861[1][3],(void*)0,&l_1861[1][3]},{(void*)0,&g_513,(void*)0,&l_1861[1][3],(void*)0}}};
            uint64_t l_1892[4] = {0x4644C683F661AA22LL,0x4644C683F661AA22LL,0x4644C683F661AA22LL,0x4644C683F661AA22LL};
            int i, j, k;
            for (g_880 = (-6); (g_880 == 23); g_880++)
            { /* block id: 734 */
                int32_t ****l_1846[10] = {(void*)0,(void*)0,&g_1280,&g_1280,(void*)0,&g_1280,&g_1280,(void*)0,&g_1280,&g_1280};
                int32_t *****l_1847 = &l_1846[4];
                uint32_t *l_1852 = &g_1239;
                uint16_t *l_1856 = &g_1791;
                int32_t *l_1859 = &g_187;
                int i;
                (*g_605) |= (safe_mod_func_int32_t_s_s((((*l_1847) = l_1846[4]) != (void*)0), (safe_mod_func_int32_t_s_s((((*l_1859) = ((l_1839[3] >= ((safe_rshift_func_uint8_t_u_u((p_27 & (p_27 ^ (((*l_1852)++) , p_27))), (&g_1471 == l_1855))) ^ (++(*l_1856)))) ^ 0x37F37EE2DAF73983LL)) , 0xBC286416L), 0x16EEDF34L))));
                --l_1862;
            }
            ++l_1892[0];
        }
        return p_27;
    }
    else
    { /* block id: 745 */
        int32_t **l_1895[10] = {&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66};
        uint64_t *l_1921 = &g_524[0];
        uint64_t **l_1922 = &l_1921;
        int16_t *l_1923[10][7] = {{(void*)0,&g_123[0],&g_123[0],(void*)0,&g_123[0],&g_123[0],(void*)0},{&g_123[0],&g_123[0],&g_123[0],&g_123[0],&l_1891,(void*)0,&l_1891},{&g_123[0],&g_123[0],&g_123[0],&g_123[0],&g_123[0],&l_1891,(void*)0},{(void*)0,&g_123[0],&g_123[0],&l_1891,&l_1891,&g_123[0],&g_123[0]},{&g_123[0],(void*)0,&g_123[0],&l_1891,&g_123[0],(void*)0,(void*)0},{(void*)0,(void*)0,&l_1891,(void*)0,(void*)0,&g_123[0],&l_1891},{&g_123[0],&l_1891,(void*)0,&l_1891,&g_123[0],&l_1891,(void*)0},{&l_1891,&l_1891,&g_123[0],&l_1891,&g_123[0],&l_1891,&g_123[0]},{&g_123[0],&l_1891,&g_123[0],&g_123[0],&l_1891,&g_123[0],&g_123[0]},{(void*)0,&g_123[0],(void*)0,&g_123[0],&g_123[0],&g_123[0],&g_123[0]}};
        uint64_t l_1924[9][10] = {{0x7ACF68B5BD6CE4CDLL,0x4EB568AF8E7720F6LL,0x7ACF68B5BD6CE4CDLL,0x70268A1497EFB7A7LL,0x4EB568AF8E7720F6LL,0xC27D951E07A83ACDLL,0xC27D951E07A83ACDLL,0x4EB568AF8E7720F6LL,0x70268A1497EFB7A7LL,0x7ACF68B5BD6CE4CDLL},{0xD346578283C94A4CLL,0xD346578283C94A4CLL,0xE3AD3CF4FEA26CDCLL,0x4EB568AF8E7720F6LL,18446744073709551615UL,0xE3AD3CF4FEA26CDCLL,18446744073709551615UL,0x4EB568AF8E7720F6LL,0xE3AD3CF4FEA26CDCLL,0xD346578283C94A4CLL},{18446744073709551615UL,0xC27D951E07A83ACDLL,0x7ACF68B5BD6CE4CDLL,18446744073709551615UL,0x70268A1497EFB7A7LL,0x70268A1497EFB7A7LL,18446744073709551615UL,0x7ACF68B5BD6CE4CDLL,0xC27D951E07A83ACDLL,18446744073709551615UL},{0x7ACF68B5BD6CE4CDLL,0xD346578283C94A4CLL,0xC27D951E07A83ACDLL,0x70268A1497EFB7A7LL,0xD346578283C94A4CLL,0x70268A1497EFB7A7LL,0xC27D951E07A83ACDLL,0xD346578283C94A4CLL,0x7ACF68B5BD6CE4CDLL,0x7ACF68B5BD6CE4CDLL},{18446744073709551615UL,0x4EB568AF8E7720F6LL,0xE3AD3CF4FEA26CDCLL,0xD346578283C94A4CLL,0xD346578283C94A4CLL,0xE3AD3CF4FEA26CDCLL,0x4EB568AF8E7720F6LL,18446744073709551615UL,0xE3AD3CF4FEA26CDCLL,18446744073709551615UL},{0xD346578283C94A4CLL,0xC27D951E07A83ACDLL,0x70268A1497EFB7A7LL,0xD346578283C94A4CLL,0x70268A1497EFB7A7LL,0xC27D951E07A83ACDLL,0xD346578283C94A4CLL,0x7ACF68B5BD6CE4CDLL,0x7ACF68B5BD6CE4CDLL,0xD346578283C94A4CLL},{0x7ACF68B5BD6CE4CDLL,18446744073709551615UL,0x70268A1497EFB7A7LL,0x70268A1497EFB7A7LL,18446744073709551615UL,0x7ACF68B5BD6CE4CDLL,0xC27D951E07A83ACDLL,18446744073709551615UL,0xC27D951E07A83ACDLL,0x7ACF68B5BD6CE4CDLL},{0x4EB568AF8E7720F6LL,18446744073709551615UL,0xE3AD3CF4FEA26CDCLL,18446744073709551615UL,0x4EB568AF8E7720F6LL,0xE3AD3CF4FEA26CDCLL,0xD346578283C94A4CLL,0xD346578283C94A4CLL,0xE3AD3CF4FEA26CDCLL,0x4EB568AF8E7720F6LL},{0x4EB568AF8E7720F6LL,0xC27D951E07A83ACDLL,0xC27D951E07A83ACDLL,0x4EB568AF8E7720F6LL,0x70268A1497EFB7A7LL,0x7ACF68B5BD6CE4CDLL,0x4EB568AF8E7720F6LL,0x7ACF68B5BD6CE4CDLL,0x70268A1497EFB7A7LL,0x4EB568AF8E7720F6LL}};
        uint16_t *l_1931 = &g_165;
        float l_1932 = 0x0.A19686p-34;
        uint16_t l_1951 = 0x5191L;
        const uint64_t l_1961[7][5][3] = {{{0xAB1C590100DD30F6LL,0xAB1C590100DD30F6LL,18446744073709551615UL},{8UL,0x78D9EA78EF04264BLL,6UL},{5UL,0xAB1C590100DD30F6LL,5UL},{5UL,8UL,18446744073709551615UL},{5UL,0x78D9EA78EF04264BLL,0x78D9EA78EF04264BLL}},{{18446744073709551615UL,0x78D9EA78EF04264BLL,18446744073709551615UL},{6UL,5UL,0xAB1C590100DD30F6LL},{18446744073709551615UL,18446744073709551615UL,0xAB1C590100DD30F6LL},{5UL,6UL,18446744073709551615UL},{0x78D9EA78EF04264BLL,18446744073709551615UL,0x78D9EA78EF04264BLL}},{{0x78D9EA78EF04264BLL,5UL,18446744073709551615UL},{5UL,0x78D9EA78EF04264BLL,0x78D9EA78EF04264BLL},{18446744073709551615UL,0x78D9EA78EF04264BLL,18446744073709551615UL},{6UL,5UL,0xAB1C590100DD30F6LL},{18446744073709551615UL,18446744073709551615UL,0xAB1C590100DD30F6LL}},{{5UL,6UL,18446744073709551615UL},{0x78D9EA78EF04264BLL,18446744073709551615UL,0x78D9EA78EF04264BLL},{0x78D9EA78EF04264BLL,5UL,18446744073709551615UL},{5UL,0x78D9EA78EF04264BLL,0x78D9EA78EF04264BLL},{18446744073709551615UL,0x78D9EA78EF04264BLL,18446744073709551615UL}},{{6UL,5UL,0xAB1C590100DD30F6LL},{18446744073709551615UL,18446744073709551615UL,0xAB1C590100DD30F6LL},{5UL,6UL,18446744073709551615UL},{0x78D9EA78EF04264BLL,18446744073709551615UL,0x78D9EA78EF04264BLL},{0x78D9EA78EF04264BLL,5UL,18446744073709551615UL}},{{5UL,0x78D9EA78EF04264BLL,0x78D9EA78EF04264BLL},{18446744073709551615UL,0x78D9EA78EF04264BLL,18446744073709551615UL},{6UL,5UL,0xAB1C590100DD30F6LL},{18446744073709551615UL,18446744073709551615UL,0xAB1C590100DD30F6LL},{5UL,6UL,18446744073709551615UL}},{{0x78D9EA78EF04264BLL,18446744073709551615UL,0x78D9EA78EF04264BLL},{0x78D9EA78EF04264BLL,5UL,18446744073709551615UL},{5UL,0x78D9EA78EF04264BLL,0x78D9EA78EF04264BLL},{18446744073709551615UL,0x78D9EA78EF04264BLL,18446744073709551615UL},{6UL,5UL,0xAB1C590100DD30F6LL}}};
        uint16_t l_2043 = 65535UL;
        const uint16_t l_2086 = 0x8F72L;
        int8_t l_2209 = 0x6CL;
        int i, j, k;
        for (g_150 = 0; g_150 < 6; g_150 += 1)
        {
            g_1743[g_150] = &g_1744;
        }
        l_1896 = p_28;
        if (((safe_sub_func_int64_t_s_s((((safe_add_func_float_f_f((safe_sub_func_float_f_f(((((safe_div_func_float_f_f((safe_sub_func_float_f_f(p_27, (p_27 , 0x1.0p+1))), (p_27 < (safe_mul_func_float_f_f(((safe_mul_func_uint8_t_u_u(((safe_div_func_uint64_t_u_u((!(!(safe_lshift_func_int16_t_s_u(((safe_unary_minus_func_int16_t_s((safe_rshift_func_int16_t_s_s((l_1920 ^ (((((*g_891) = (*g_891)) != ((*l_1922) = l_1921)) || ((l_1924[8][9] &= 0xD9DAL) && (((safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((((*l_1931) = (((((safe_lshift_func_int16_t_s_s(p_27, 13)) < p_27) >= p_27) == p_27) || (*g_1463))) | 1L), 6)), p_27)) < l_1933) >= p_27))) & p_27)), p_27)))) == (*g_1463)), 0)))), (*g_785))) & (**g_742)), p_27)) , l_1879), 0x9.1AC109p+13))))) >= p_27) != 0xF.029AFEp+87) != p_27), 0xB.A9D625p+94)), p_27)) , (*g_1010)) , (-1L)), p_27)) , p_27))
        { /* block id: 752 */
            uint32_t l_1934 = 6UL;
            int32_t l_1941 = 0x749435E6L;
            int32_t l_1942 = 0x02C79720L;
            int32_t l_1946 = 0x4DE355C6L;
            int32_t l_1949 = 0x37F00596L;
            int32_t l_1954[10] = {2L,0x8F0A1500L,2L,2L,0x8F0A1500L,2L,2L,0x8F0A1500L,2L,2L};
            uint64_t l_1956[6] = {1UL,1UL,1UL,1UL,1UL,1UL};
            int i;
            l_1934--;
            for (l_1891 = (-28); (l_1891 != (-24)); ++l_1891)
            { /* block id: 756 */
                float l_1939 = 0xF.8D6E80p-16;
                int32_t l_1940 = 0xB111933CL;
                int32_t l_1943 = (-1L);
                int32_t l_1944 = (-1L);
                int32_t l_1945 = 0L;
                int32_t l_1947 = (-1L);
                int32_t l_1948[10][6][1] = {{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}},{{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L},{0x42D61957L},{0x998DA540L}}};
                int32_t l_1950 = 9L;
                int i, j, k;
                l_1879 = l_1934;
                if (p_27)
                    break;
                ++l_1951;
            }
            (***g_745) = (void*)0;
            l_1956[1]--;
        }
        else
        { /* block id: 763 */
            int8_t l_1970[5][5] = {{0x97L,0L,0x97L,(-7L),0xC5L},{4L,0x3AL,0xC5L,0x3AL,4L},{0x97L,0x3AL,0L,4L,0L},{0L,0L,0xC5L,4L,0x0DL},{0x3AL,4L,4L,0x97L,0x0DL}};
            int32_t **l_1974 = &g_779;
            int32_t l_2000 = 0x3EF0EC55L;
            uint16_t **l_2023 = &l_1931;
            uint16_t ** const *l_2022 = &l_2023;
            uint16_t ** const **l_2021 = &l_2022;
            int32_t l_2046 = 0x30C0AD33L;
            int32_t l_2048[6] = {0xDAA71D04L,9L,9L,0xDAA71D04L,9L,9L};
            uint32_t l_2228[2][8] = {{0x9F89B89BL,0xF2D2A5BFL,0xF2D2A5BFL,0x9F89B89BL,0xF2D2A5BFL,0xF2D2A5BFL,0x9F89B89BL,0xF2D2A5BFL},{0x9F89B89BL,0x9F89B89BL,0xBA555018L,0x9F89B89BL,0x9F89B89BL,0xBA555018L,0x9F89B89BL,0x9F89B89BL}};
            int8_t **l_2295 = &g_578;
            int64_t *l_2303 = &g_995;
            uint16_t l_2304 = 65535UL;
            uint32_t l_2305 = 0xDF1ADB2BL;
            int i, j;
            if (((((safe_rshift_func_int16_t_s_u(l_1961[3][4][2], 15)) | (*g_1463)) != (0x11C3L != ((void*)0 == (*g_793)))) != (((+(safe_add_func_uint64_t_u_u((((65535UL | ((((safe_lshift_func_uint8_t_u_u((((((**g_891) &= (p_27 , (safe_unary_minus_func_int64_t_s((safe_sub_func_int16_t_s_s(g_123[0], p_27)))))) < 0xD619A7173B683883LL) <= p_27) >= (*g_245)), 4)) != l_1970[0][4]) , l_1880) == 1UL)) || p_27) || p_27), p_27))) , l_1970[0][4]) | (***g_1747))))
            { /* block id: 765 */
                int32_t l_1971[9][7][4] = {{{0x9F630456L,3L,0x99053437L,1L},{4L,(-1L),0x99053437L,0x7F61200DL},{0x9F630456L,0xDE809681L,0x5E5E914AL,0x8A0045E9L},{0L,0x5E74E445L,0x8A0045E9L,0L},{(-3L),0L,0L,0L},{(-1L),0x5E74E445L,0x93E53D3DL,0x8A0045E9L},{(-1L),0xDE809681L,0L,0x7F61200DL}},{{3L,(-1L),1L,1L},{3L,3L,0L,0x1E7CFAE7L},{(-1L),0x76454FA3L,0x93E53D3DL,9L},{(-1L),0L,0L,0x93E53D3DL},{(-3L),0L,0x8A0045E9L,9L},{0L,0x76454FA3L,0x5E5E914AL,0x1E7CFAE7L},{0x9F630456L,3L,0x99053437L,1L}},{{4L,(-1L),0x99053437L,0x7F61200DL},{0x9F630456L,0xDE809681L,0x5E5E914AL,0x8A0045E9L},{0L,0x5E74E445L,0x8A0045E9L,0L},{(-3L),0L,0L,0L},{(-1L),0x5E74E445L,0x93E53D3DL,0x8A0045E9L},{(-1L),0xDE809681L,0L,0x7F61200DL},{3L,(-1L),1L,1L}},{{3L,3L,0L,0x1E7CFAE7L},{(-1L),0x76454FA3L,0x93E53D3DL,9L},{(-1L),0L,0L,0x93E53D3DL},{(-3L),0L,0x8A0045E9L,9L},{0L,0x76454FA3L,0x5E5E914AL,0x1E7CFAE7L},{0x9F630456L,3L,0x99053437L,1L},{4L,(-1L),0x99053437L,0x7F61200DL}},{{0x9F630456L,0xDE809681L,0x5E5E914AL,0x8A0045E9L},{0L,0x5E74E445L,0x8A0045E9L,0L},{(-3L),0L,0L,0L},{(-1L),0x5E74E445L,0x93E53D3DL,0x8A0045E9L},{(-1L),0xDE809681L,0L,0x7F61200DL},{3L,(-1L),1L,1L},{3L,3L,0L,0x1E7CFAE7L}},{{(-1L),0x76454FA3L,0x93E53D3DL,9L},{(-1L),0L,0L,0x93E53D3DL},{(-3L),0L,0x8A0045E9L,9L},{0L,0x76454FA3L,0x5E5E914AL,0x1E7CFAE7L},{0x9F630456L,3L,0x99053437L,1L},{4L,(-1L),0x99053437L,0x7F61200DL},{0x9F630456L,0x5E74E445L,0L,1L}},{{0x9F630456L,0L,1L,0xD4EFA033L},{0x76454FA3L,0L,(-1L),0xD4EFA033L},{4L,0L,0L,1L},{(-3L),0x5E74E445L,0xD4EFA033L,0x93E53D3DL},{0L,4L,0x99053437L,0x99053437L},{0L,0L,0xD4EFA033L,9L},{(-3L),(-1L),0L,(-5L)}},{{4L,0x9F630456L,(-1L),0L},{0x76454FA3L,0x9F630456L,1L,(-5L)},{0x9F630456L,(-1L),0L,9L},{0xDE809681L,0L,0x7F61200DL,0x99053437L},{3L,4L,0x7F61200DL,0x93E53D3DL},{0xDE809681L,0x5E74E445L,0L,1L},{0x9F630456L,0L,1L,0xD4EFA033L}},{{0x76454FA3L,0L,(-1L),0xD4EFA033L},{4L,0L,0L,1L},{(-3L),0x5E74E445L,0xD4EFA033L,0x93E53D3DL},{0L,4L,0x99053437L,0x99053437L},{0L,0L,0xD4EFA033L,9L},{(-3L),(-1L),0L,(-5L)},{4L,0x9F630456L,(-1L),0L}}};
                int32_t l_1984[6] = {(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)};
                uint8_t *** const *l_2054 = (void*)0;
                uint8_t *** const **l_2053 = &l_2054;
                int i, j, k;
lbl_2087:
                for (l_1891 = 0; (l_1891 <= 5); l_1891 += 1)
                { /* block id: 768 */
                    for (g_283 = 5; (g_283 >= 0); g_283 -= 1)
                    { /* block id: 771 */
                        int32_t ***l_1975 = &g_950;
                        int32_t l_1976 = (-8L);
                        int32_t l_1977[2][2] = {{4L,4L},{4L,4L}};
                        uint8_t ****l_1978 = (void*)0;
                        uint8_t ***l_1980 = &g_742;
                        uint8_t ****l_1979 = &l_1980;
                        int i, j;
                        l_1971[2][2][2] = (**g_239);
                        l_1984[5] &= (safe_sub_func_int32_t_s_s((l_1977[0][1] = (l_1976 = (l_1974 != ((*l_1975) = l_1974)))), ((*g_1463) , ((((((*l_1979) = (void*)0) != (void*)0) > (+(safe_rshift_func_int16_t_s_s(p_27, 4)))) ^ 0xC4CCL) & l_1971[5][5][3]))));
                    }
                }
                if ((p_27 <= (safe_add_func_int64_t_s_s((safe_add_func_int16_t_s_s(l_1970[3][1], (safe_lshift_func_uint8_t_u_u(0xF8L, 3)))), (~((g_1992 != g_1994) , (4294967295UL < ((p_27 ^ (l_1970[0][4] , (safe_lshift_func_int16_t_s_u(0xE5E4L, 6)))) > p_27))))))))
                { /* block id: 780 */
                    int16_t l_1999[5][5][6] = {{{0x56F5L,0L,(-1L),1L,0L,0xF765L},{0x56F5L,0x44BDL,1L,0x44BDL,0x56F5L,0L},{1L,0L,0L,0x56F5L,0xF765L,0x22CDL},{0x350BL,0x7926L,0xF765L,0L,0x22CDL,0x22CDL},{(-10L),0L,0L,(-10L),0x162BL,0L}},{{0x22CDL,4L,1L,2L,0x44BDL,0xF765L},{1L,0x350BL,(-1L),0x7926L,0x44BDL,0x7926L},{0L,4L,0L,0x4A95L,0x162BL,0x350BL},{0L,0L,0x56F5L,0xF765L,0x22CDL,0x162BL},{2L,0x7926L,0x4A95L,0xF765L,0xF765L,0x4A95L}},{{0L,0L,4L,0x4A95L,0x56F5L,0L},{0L,0x44BDL,0L,0x7926L,0L,4L},{1L,0L,0L,2L,0L,0L},{0x22CDL,2L,4L,(-10L),1L,0x4A95L},{(-10L),1L,0x4A95L,0L,(-1L),0x162BL}},{{0x350BL,1L,0x56F5L,0L,0L,0x44BDL},{0L,(-10L),0x162BL,0L,1L,0x350BL},{0L,0x162BL,0xF765L,2L,(-1L),0x56F5L},{0L,0L,2L,0L,0L,1L},{0L,1L,(-1L),0L,0x56F5L,0x4A95L}},{{0x44BDL,0x350BL,0x56F5L,1L,0x4A95L,0x4A95L},{0x22CDL,(-1L),(-1L),0x22CDL,4L,1L},{0x4A95L,0x7926L,2L,(-10L),0L,0x56F5L},{2L,0x44BDL,0xF765L,0x350BL,0L,0x350BL},{0x162BL,0x7926L,0x162BL,1L,4L,0x44BDL}}};
                    int32_t l_2004 = 0xC40E6C5EL;
                    int32_t l_2005 = 0xBE3715B2L;
                    int32_t l_2006 = (-8L);
                    int32_t l_2007 = 4L;
                    int32_t l_2008 = (-1L);
                    uint16_t ***l_2026 = &l_2023;
                    uint16_t *** const *l_2025 = &l_2026;
                    int i, j, k;
                    for (l_1920 = 17; (l_1920 == (-30)); --l_1920)
                    { /* block id: 783 */
                        int32_t l_2001 = 0x53C8C47FL;
                        int32_t l_2002 = 0x71B9729CL;
                        int32_t l_2003 = 0x66F03287L;
                        int32_t l_2009 = 0x4263EFACL;
                        uint64_t l_2010 = 0xE0A1A155C18E5D1BLL;
                        --l_2010;
                        return l_1984[4];
                    }
                    for (g_513 = 15; (g_513 >= (-18)); g_513 = safe_sub_func_int8_t_s_s(g_513, 9))
                    { /* block id: 789 */
                        uint32_t l_2024[10][3][7] = {{{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L},{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL}},{{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L},{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L}},{{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L},{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL}},{{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L},{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L}},{{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L},{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL}},{{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L},{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,4294967295UL,0x56681885L,6UL,0x0F307DE4L,4294967289UL,0x6DC17B43L}},{{9UL,4294967289UL,0UL,0UL,4294967289UL,9UL,1UL},{1UL,6UL,0x6DC17B43L,4294967295UL,0x56681885L,0x0B84662DL,1UL},{4294967295UL,0UL,1UL,1UL,0UL,4294967295UL,4294967295UL}},{{0x9D4AFAC6L,6UL,0x6DC17B43L,4294967295UL,0x56681885L,0x0B84662DL,1UL},{4294967295UL,0UL,1UL,1UL,0UL,4294967295UL,4294967295UL},{0x9D4AFAC6L,6UL,0x6DC17B43L,4294967295UL,0x56681885L,0x0B84662DL,1UL}},{{4294967295UL,0UL,1UL,1UL,0UL,4294967295UL,4294967295UL},{0x9D4AFAC6L,6UL,0x6DC17B43L,4294967295UL,0x56681885L,0x0B84662DL,1UL},{4294967295UL,0UL,1UL,1UL,0UL,4294967295UL,4294967295UL}},{{0x9D4AFAC6L,6UL,0x6DC17B43L,4294967295UL,0x56681885L,0x0B84662DL,1UL},{4294967295UL,0UL,1UL,1UL,0UL,4294967295UL,4294967295UL},{0x9D4AFAC6L,6UL,0x6DC17B43L,4294967295UL,0x56681885L,0x0B84662DL,1UL}}};
                        int32_t l_2044 = 1L;
                        int32_t l_2045 = 0xB614C852L;
                        int32_t l_2047 = 0x3C7CBBAFL;
                        int32_t l_2049 = 0x5AFF5F82L;
                        uint16_t l_2050[4][4] = {{0x722DL,0xA7B6L,0xA7B6L,0x722DL},{0x722DL,0xA7B6L,0xA7B6L,0x722DL},{0x722DL,0xA7B6L,0xA7B6L,0x722DL},{0x722DL,0xA7B6L,0xA7B6L,0x722DL}};
                        int i, j, k;
                        l_2000 = (((safe_add_func_uint64_t_u_u((((safe_add_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(0x7BL, 4)), 0x606CL)) , l_2021) == (l_2024[5][1][1] , ((**g_1992) , l_2025))), (((safe_add_func_uint8_t_u_u(((**g_891) == (safe_mod_func_int16_t_s_s((l_2044 = (safe_rshift_func_uint16_t_u_s((safe_mod_func_uint16_t_u_u((safe_add_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(((safe_lshift_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(g_68, 0)), (l_1971[7][1][0] , l_1971[7][2][3]))) >= 1L), (*g_245))), l_2043)), l_1971[2][2][2])), p_27))), l_2000))), p_27)) , p_27) , 0x7495F897F6839B78LL))) >= l_1984[5]) <= l_1970[0][4]);
                        if (l_1984[0])
                            break;
                        if (p_27)
                            continue;
                        ++l_2050[3][0];
                    }
                    (**g_746) = &l_1971[0][0][3];
                    return p_27;
                }
                else
                { /* block id: 798 */
                    l_1984[5] &= l_2046;
                }
                for (g_68 = 0; (g_68 <= 9); g_68 += 1)
                { /* block id: 803 */
                    uint8_t *** const **l_2055 = &l_2054;
                    int i;
                    if (l_1971[2][2][2])
                        break;
                    l_2055 = l_2053;
                }
                for (g_1767 = 0; (g_1767 >= 5); g_1767++)
                { /* block id: 809 */
                    const int8_t l_2058[6] = {0x41L,0x41L,0x41L,0x41L,0x41L,0x41L};
                    uint64_t **l_2066 = (void*)0;
                    uint64_t **l_2069 = &g_892[7][0];
                    int i;
                    l_1971[4][0][0] &= (l_2058[0] > 0x6A57L);
                    for (l_2046 = 1; (l_2046 >= 0); l_2046 -= 1)
                    { /* block id: 813 */
                        float *****l_2065 = &l_2064;
                        uint64_t ***l_2067 = (void*)0;
                        uint64_t ***l_2068 = (void*)0;
                        int i, j;
                        (**g_607) |= (0x6439149D0AF857E1LL & (~(p_27 != (safe_mod_func_int8_t_s_s((0x2BDD79C5L & (safe_mul_func_uint8_t_u_u(l_1970[0][4], (((*l_2065) = l_2064) != ((l_2066 != (l_2069 = &l_1921)) , l_2070[0]))))), (((((safe_rshift_func_uint8_t_u_u((((safe_add_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u((((safe_unary_minus_func_uint32_t_u(((safe_mul_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((safe_add_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u((((0xD0L != p_27) != (*g_106)) <= l_2000), l_1971[2][2][2])), l_2086)), 0xB2L)), 0xE599L)) , (**g_1472)))) < (**g_577)) , p_27), (*g_106))), 0xCDF7AC61L)) == p_27) > p_27), p_27)) == p_27) != (-1L)) , p_27) , (**g_577)))))));
                        if (g_77)
                            goto lbl_2087;
                    }
                    l_1896 = ((**g_746) = &l_1971[2][2][2]);
                }
            }
            else
            { /* block id: 822 */
                int32_t * const l_2104 = (void*)0;
                int32_t l_2106 = 0xF8B2CDADL;
                int8_t l_2157[1][5] = {{0x62L,0x62L,0x62L,0x62L,0x62L}};
                int32_t *l_2164 = &g_187;
                int32_t l_2194[8][3][5] = {{{1L,(-5L),0x8BDD1AE2L,(-5L),1L},{0x394BC957L,0x91D3B3F1L,0x06EDDB18L,0x7630088BL,0x7630088BL},{0x56C30231L,(-5L),0x56C30231L,0x9169C985L,0x56C30231L}},{{0x394BC957L,0x7630088BL,0x91D3B3F1L,0x91D3B3F1L,0x7630088BL},{1L,0x9169C985L,0x8BDD1AE2L,0x9169C985L,1L},{0x7630088BL,0x91D3B3F1L,0x91D3B3F1L,0x7630088BL,0x394BC957L}},{{0x56C30231L,0x9169C985L,0x56C30231L,(-5L),0x56C30231L},{0x7630088BL,0x7630088BL,0x06EDDB18L,0x91D3B3F1L,0x394BC957L},{1L,(-5L),0x8BDD1AE2L,(-5L),1L}},{{0x394BC957L,0x91D3B3F1L,0x06EDDB18L,0x7630088BL,0x7630088BL},{0x56C30231L,(-5L),0x56C30231L,0x9169C985L,0x56C30231L},{0x394BC957L,0x7630088BL,0x91D3B3F1L,0x91D3B3F1L,0x7630088BL}},{{1L,0x9169C985L,0x8BDD1AE2L,0x9169C985L,1L},{0x7630088BL,0x91D3B3F1L,0x91D3B3F1L,0x7630088BL,0x394BC957L},{0x56C30231L,0x9169C985L,0x56C30231L,(-5L),0x56C30231L}},{{0x7630088BL,0x7630088BL,0x06EDDB18L,0x91D3B3F1L,0x394BC957L},{1L,(-5L),0x8BDD1AE2L,(-5L),1L},{0x394BC957L,0x91D3B3F1L,0x06EDDB18L,0x7630088BL,0x7630088BL}},{{0x56C30231L,(-5L),0x56C30231L,0x9169C985L,0x56C30231L},{0x394BC957L,0x7630088BL,0x91D3B3F1L,0x91D3B3F1L,0x7630088BL},{1L,0x9169C985L,0x8BDD1AE2L,0x9169C985L,1L}},{{0x7630088BL,0x91D3B3F1L,0x91D3B3F1L,0x7630088BL,0x394BC957L},{0x56C30231L,0x9169C985L,0x56C30231L,(-5L),0x56C30231L},{0x7630088BL,0x7630088BL,0x06EDDB18L,0x91D3B3F1L,0x394BC957L}}};
                uint16_t *l_2200 = &l_1951;
                int i, j, k;
                for (g_513 = 0; (g_513 >= 0); g_513 -= 1)
                { /* block id: 825 */
                    int32_t *l_2103 = &l_1920;
                    int32_t **l_2107 = (void*)0;
                    int32_t l_2129 = 0xEBB82A4FL;
                    int i;
                    (*g_236) = &l_2048[1];
                    for (p_27 = 0; (p_27 <= 0); p_27 += 1)
                    { /* block id: 829 */
                        uint32_t *l_2102 = (void*)0;
                        int32_t l_2105[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_2105[i] = 1L;
                        (*g_605) = 0x46DC3C0BL;
                        (*g_605) = (0xB1L && (safe_add_func_uint32_t_u_u((safe_add_func_int32_t_s_s(((safe_sub_func_float_f_f((safe_div_func_float_f_f(p_27, ((((!(**g_891)) , (safe_mul_func_float_f_f((safe_mul_func_float_f_f(0x6.9DAE90p+54, ((-(p_27 , ((((*g_1472) = l_2102) == ((l_2103 == l_2104) , p_28)) , p_27))) <= 0x0.4p+1))), 0x3.5A3F2Dp+81))) < l_2105[1]) >= p_27))), 0x1.472171p+8)) , p_27), l_2106)), 0x1D1FEC4EL)));
                    }
                    (*g_2109) = p_28;
                    (*g_605) ^= ((safe_add_func_uint64_t_u_u((safe_sub_func_int16_t_s_s(((1L == (+(safe_mul_func_int16_t_s_s(((safe_div_func_int16_t_s_s(p_27, (safe_rshift_func_uint16_t_u_u(l_2106, 4)))) <= p_27), 65535UL)))) && p_27), (++(*l_1931)))), (*g_1010))) >= (safe_mod_func_uint8_t_u_u((*g_106), (safe_lshift_func_int8_t_s_u((safe_add_func_uint16_t_u_u((*g_245), l_2106)), p_27)))));
                    for (g_856 = 0; (g_856 <= 0); g_856 += 1)
                    { /* block id: 839 */
                        if (l_2129)
                            break;
                    }
                }
                for (g_286 = 0; (g_286 < 27); g_286 = safe_add_func_int16_t_s_s(g_286, 1))
                { /* block id: 845 */
                    uint32_t l_2153[8][9][3] = {{{0xBE5EC019L,0x97B23352L,0xB42D88B3L},{4294967289UL,1UL,0UL},{1UL,0UL,0x255DEDECL},{0x6B788033L,0xA41AA9A6L,0x22CFA91EL},{0x61CE8DEDL,0xBE5EC019L,0UL},{4294967295UL,1UL,4294967295UL},{4294967295UL,4294967295UL,0x3FD85B0FL},{0xA8AAD6AEL,1UL,4294967295UL},{0x332A7094L,8UL,0x97B23352L}},{{0x0DB491D0L,4294967294UL,0x6684AE4FL},{0xF4648171L,0x332A7094L,0x97B23352L},{4294967295UL,0xAB82B479L,4294967295UL},{1UL,4294967295UL,0x3FD85B0FL},{0xA41AA9A6L,4294967290UL,4294967295UL},{0xFFBAD367L,4294967292UL,0UL},{0x9E93A988L,0x31A798A7L,0x22CFA91EL},{0xF7FF3054L,4294967295UL,0x255DEDECL},{2UL,4294967295UL,0UL}},{{3UL,4294967295UL,0xB42D88B3L},{9UL,4294967295UL,0x0DB491D0L},{0xB42D88B3L,0xEB819532L,0xC688A55DL},{0x31A798A7L,0x6B788033L,0x6B788033L},{0x97B23352L,0UL,3UL},{1UL,0UL,4294967294UL},{0x1D21A9B9L,0xF2DACDF7L,0xF4648171L},{4294967290UL,0x9E93A988L,4294967293UL},{4294967295UL,0xF2DACDF7L,0xFF16311EL}},{{9UL,0UL,0xAB82B479L},{0UL,0UL,0xE087E046L},{0xD2E39E2DL,0x6B788033L,9UL},{0UL,0xEB819532L,4294967295UL},{0UL,4294967295UL,4294967295UL},{1UL,4294967295UL,0xEB819532L},{0xCB95826BL,4294967295UL,0xD67C336DL},{4294967295UL,4294967295UL,4294967292UL},{3UL,0x31A798A7L,4294967289UL}},{{8UL,4294967292UL,0x332A7094L},{0x5D8C2160L,4294967290UL,0x5D8C2160L},{0UL,4294967295UL,4294967286UL},{5UL,0xAB82B479L,4294967295UL},{1UL,0x332A7094L,4294967295UL},{4294967295UL,4294967294UL,4294967290UL},{1UL,8UL,0x11B5D4EAL},{5UL,1UL,2UL},{0UL,4294967295UL,1UL}},{{0x5D8C2160L,1UL,5UL},{8UL,0xBE5EC019L,4294967295UL},{3UL,0xA41AA9A6L,0x9E93A988L},{4294967295UL,0UL,1UL},{0xCB95826BL,1UL,0x5E67AA87L},{1UL,0x97B23352L,4294967295UL},{0UL,3UL,0x143B2AE3L},{0UL,1UL,0xC688A55DL},{9UL,9UL,5UL}},{{0x11B5D4EAL,0xFFBAD367L,4294967295UL},{0x1AD0FF54L,3UL,0xD67C336DL},{1UL,4294967295UL,1UL},{0xA41AA9A6L,0x1AD0FF54L,0xD67C336DL},{0xB42D88B3L,4294967295UL,4294967295UL},{0UL,0x89C09070L,5UL},{0xEB819532L,0x97B23352L,0xC688A55DL},{4294967295UL,0x143B2AE3L,0x22CFA91EL},{0x777CA4F8L,0UL,0UL}},{{0xDA993C88L,0x5D8C2160L,1UL},{0xB86452E2L,4294967295UL,3UL},{8UL,0x6684AE4FL,1UL},{8UL,0xB42D88B3L,1UL},{1UL,4294967295UL,0UL},{0UL,4294967292UL,4294967292UL},{0xD67C336DL,4294967295UL,8UL},{0x61CE8DEDL,0x255DEDECL,4294967295UL},{0x9E93A988L,1UL,0xA41AA9A6L}}};
                    int32_t l_2155 = (-1L);
                    int i, j, k;
                    for (g_77 = 0; (g_77 < 13); g_77 = safe_add_func_int64_t_s_s(g_77, 4))
                    { /* block id: 848 */
                        int32_t l_2143 = 0x012A4F32L;
                        int32_t l_2156 = (-1L);
                        int32_t l_2158 = 0x7C0288E7L;
                        (*g_747) = ((p_27 > ((safe_add_func_uint16_t_u_u(((**g_577) && ((l_2106 |= ((safe_add_func_float_f_f(0xA.36ADBDp+78, ((-(safe_mul_func_float_f_f((safe_sub_func_float_f_f(l_2143, (safe_add_func_float_f_f(p_27, (l_2158 = (p_27 >= (safe_add_func_float_f_f((((+((safe_div_func_float_f_f((l_2156 = (l_2155 = (safe_add_func_float_f_f(p_27, (p_27 > ((p_27 <= l_2153[7][0][0]) == l_2154)))))), p_27)) > p_27)) != l_2157[0][4]) != p_27), l_1970[0][4])))))))), p_27))) <= 0x0.Ap-1))) , l_2157[0][3])) || 0x35BACEDAL)), (-6L))) , l_1970[0][4])) , (void*)0);
                        return p_27;
                    }
                    if (l_1970[2][3])
                        break;
                }
                for (g_1239 = (-4); (g_1239 >= 59); g_1239++)
                { /* block id: 860 */
                    int32_t *l_2163 = (void*)0;
                    int16_t *l_2166 = (void*)0;
                    int64_t **l_2176[4][2] = {{&g_785,&g_785},{&g_785,&g_785},{&g_785,&g_785},{&g_785,&g_785}};
                    int64_t ***l_2175[5];
                    int32_t l_2180[4] = {0x1212DE3BL,0x1212DE3BL,0x1212DE3BL,0x1212DE3BL};
                    int i, j;
                    for (i = 0; i < 5; i++)
                        l_2175[i] = &l_2176[1][1];
                    (*g_605) |= 0xBBA90EA3L;
                    for (p_27 = 0; (p_27 == 47); p_27 = safe_add_func_int64_t_s_s(p_27, 2))
                    { /* block id: 864 */
                        l_2164 = l_2163;
                        if (l_2000)
                            break;
                        (*g_2165) = p_28;
                    }
                    if ((&l_1891 == l_2166))
                    { /* block id: 869 */
                        int64_t l_2167 = 0xAE3D19F441A0A313LL;
                        l_2167 |= (-1L);
                        if ((*g_237))
                            continue;
                    }
                    else
                    { /* block id: 872 */
                        (*g_605) |= (l_2048[1] = p_27);
                    }
                    for (g_259 = 20; (g_259 < 9); g_259 = safe_sub_func_uint16_t_u_u(g_259, 8))
                    { /* block id: 878 */
                        uint32_t l_2178 = 0UL;
                        int32_t *****l_2179 = &g_1736;
                        int32_t l_2181 = 0x9FF2EFC7L;
                        l_2180[3] = (safe_sub_func_float_f_f(p_27, ((g_2172 == l_2175[3]) == (!(((l_2178 = (l_2048[3] = l_2046)) != ((void*)0 != l_2179)) , p_27)))));
                        l_2181 = 0x1.Cp-1;
                    }
                }
                for (l_2106 = 8; (l_2106 <= 5); l_2106 = safe_sub_func_uint64_t_u_u(l_2106, 4))
                { /* block id: 887 */
                    int32_t l_2186 = (-1L);
                    int32_t l_2193 = 0L;
                    uint8_t * const *l_2199 = &g_1460[0];
                    uint8_t * const **l_2198 = &l_2199;
                    l_2048[1] = (p_27 > (safe_mul_func_float_f_f((l_2048[1] != ((0x9.77B456p+83 != ((l_2186 = 0x1.4AFFCBp-32) < p_27)) >= (((safe_mod_func_uint8_t_u_u((p_27 > ((safe_sub_func_uint64_t_u_u((**g_891), (l_2048[5] && (((p_27 | (*g_245)) > 0xABL) , (*g_794))))) || 0UL)), (-1L))) > p_27) , 0xB.4E39CFp-80))), p_27)));
                    for (g_137 = (-19); (g_137 <= 23); ++g_137)
                    { /* block id: 892 */
                        uint32_t l_2195 = 18446744073709551612UL;
                        (*g_605) = 0L;
                        ++l_2195;
                        if (l_2048[1])
                            break;
                    }
                    l_2193 |= ((l_2198 == (((**g_577) = (((((void*)0 != l_2200) | (safe_mul_func_int8_t_s_s((safe_sub_func_int64_t_s_s(p_27, 0xA28B5F1410A32B7ALL)), ((void*)0 != &l_1891)))) < (p_27 <= 5L)) > l_2048[1])) , (void*)0)) , l_1955);
                }
            }
            (*g_605) &= (safe_mul_func_int8_t_s_s((safe_mod_func_uint64_t_u_u((((*g_785) = (*g_1010)) == (1UL < l_2209)), (((*g_578) = 0x51L) | (safe_lshift_func_uint8_t_u_u(0x75L, (safe_rshift_func_int8_t_s_u(((**g_742) , l_2048[4]), 1))))))), ((safe_lshift_func_int16_t_s_u((l_1881[8][0] >= (l_1881[6][0] >= p_27)), 11)) > 65535UL)));
            for (g_1437 = 0; (g_1437 != 45); g_1437++)
            { /* block id: 906 */
                int64_t **l_2229 = &g_785;
                int32_t l_2254 = 1L;
                uint64_t ***l_2273[7][1][1] = {{{&l_1922}},{{&g_891}},{{&l_1922}},{{&g_891}},{{&l_1922}},{{&g_891}},{{&l_1922}}};
                uint64_t ****l_2274 = &l_2273[2][0][0];
                uint64_t ***l_2276 = &g_891;
                uint64_t ****l_2275 = &l_2276;
                int i, j, k;
                (*g_605) = ((safe_lshift_func_uint16_t_u_s(p_27, (safe_add_func_int8_t_s_s((safe_div_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(((((l_2228[0][7] < ((l_2229 = l_2229) == (*g_2172))) != (safe_div_func_uint64_t_u_u((((safe_add_func_int32_t_s_s((l_1879 = (0x64L == ((((safe_lshift_func_uint8_t_u_u((((((safe_mul_func_uint16_t_u_u(((((safe_rshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_u((safe_div_func_uint32_t_u_u(((safe_rshift_func_int16_t_s_s(p_27, (0x778E5E5DL || (safe_rshift_func_int16_t_s_u(p_27, ((safe_lshift_func_int16_t_s_u(((safe_div_func_int64_t_s_s(((p_27 == 0x7E74L) , 0x33E9927C8B65CD14LL), p_27)) != (*g_190)), p_27)) && (**g_891))))))) < p_27), l_2254)), l_2228[0][7])), g_511)), p_27)) != l_2228[0][4]) != 5L) != l_2255), p_27)) , 0xC702C2E7L) > (*g_237)) && 65535UL) , l_2046), (**g_1744))) | 0x1DL) , p_27) ^ p_27))), p_27)) < p_27) != l_2254), 0x04F27FCD3AA4739BLL))) == 0xBDE9180C2E35C052LL) | l_1970[0][2]), p_27)), l_2254)), 0x4D78764FL)), l_2254)))) == 0L);
                l_2254 = ((0x1.Fp+1 < l_2254) <= ((+(l_2046 = (l_1970[0][4] >= 0xD.72567Bp+59))) < ((+0xB.7EC57Ep-8) == (p_27 , ((l_1970[3][1] < (l_2000 = (safe_add_func_float_f_f(p_27, (((((safe_sub_func_float_f_f(((safe_div_func_uint16_t_u_u(l_2228[0][7], (*g_245))) , p_27), 0x6.86C07Cp-67)) >= 0x9.5940E0p-46) == p_27) <= 0x8.6p-1) > p_27))))) <= 0x2.1p-1)))));
                if ((l_2046 ^= ((g_2264[0] == (l_1970[0][4] , l_2267)) , (safe_add_func_uint16_t_u_u(p_27, (safe_add_func_uint8_t_u_u((((((*l_2275) = ((*l_2274) = l_2273[0][0][0])) != (void*)0) || ((safe_mul_func_int16_t_s_s((safe_sub_func_int64_t_s_s((safe_div_func_uint8_t_u_u(0x61L, (l_2048[3] = (*g_578)))), ((18446744073709551615UL & p_27) , p_27))), l_2254)) && p_27)) ^ p_27), 0xC7L)))))))
                { /* block id: 917 */
                    int8_t **l_2296[8][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,&g_1463,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_1463,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_1463,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_1463,(void*)0}};
                    float l_2299[5][6][5] = {{{(-0x1.2p-1),(-0x1.2p-1),0xF.790EEFp-1,0x1.8p+1,0x0.Dp+1},{0xD.3478C1p-54,0x6.2p-1,0x0.2p+1,0x0.3p-1,0x2.227E94p+18},{0x9.87921Dp+88,(-0x1.5p-1),(-0x1.2p-1),0xA.D566C7p-47,0x7.Bp-1},{(-0x6.Ep-1),0x6.2p-1,0x0.D0F543p+59,0x0.D0F543p+59,0x6.2p-1},{0x8.9p-1,(-0x1.2p-1),0xE.42257Fp-75,0x2.ED4EDFp+90,0xA.62038Ep-79},{(-0x1.0p+1),(-0x7.Bp+1),0x4.F9A70Bp+57,(-0x6.Ep-1),0x0.3p-1}},{{0x1.8p+1,0x4.1p+1,0x7.Bp-1,(-0x1.5p-1),0x6.2p-1},{(-0x1.0p+1),(-0x1.Dp+1),0xD.3478C1p-54,(-0x1.Dp+1),(-0x1.0p+1)},{0x8.9p-1,0x3.1p-1,0x6.2p-1,0x7.Bp-1,(-0x1.2p-1)},{(-0x6.Ep-1),(-0x1.1p-1),(-0x9.4p+1),0x1.073DB7p+6,(-0x6.4p-1)},{0x9.87921Dp+88,0x9.00C4EBp-74,0xA.E138A0p+75,0x3.1p-1,(-0x1.2p-1)},{0xD.3478C1p-54,0x1.073DB7p+6,0x1.073DB7p+6,0xD.3478C1p-54,(-0x1.0p+1)}},{{(-0x1.2p-1),0xE.42257Fp-75,0x2.ED4EDFp+90,0xA.62038Ep-79,0x6.2p-1},{0x1.A03A65p+58,(-0x1.0p+1),0x1.9p-1,(-0x1.Dp-1),0x0.3p-1},{0x2.ED4EDFp+90,0xF.790EEFp-1,0x4.1p+1,0xA.62038Ep-79,0xA.62038Ep-79},{(-0x1.1p-1),0x1.A03A65p+58,(-0x1.1p-1),0xD.3478C1p-54,0x6.2p-1},{0xF.790EEFp-1,0x1.8p+1,0x0.Dp+1,0x3.1p-1,0x7.Bp-1},{0x0.D0F543p+59,0x0.2p+1,(-0x1.0p+1),0x1.073DB7p+6,0x2.227E94p+18}},{{0x4.1p+1,0x0.Dp+1,0x9.00C4EBp-74,0x2.ED4EDFp+90,0x9.00C4EBp-74},{(-0x6.4p-1),(-0x6.4p-1),0x2.227E94p+18,0x1.073DB7p+6,(-0x1.0p+1)},{0xF.790EEFp-1,(-0x1.5p-1),0xA.D566C7p-47,0xA.E138A0p+75,0xA.62038Ep-79},{(-0x7.3p-1),0x4.F9A70Bp+57,(-0x6.4p-1),(-0x9.4p+1),0x0.D0F543p+59},{0x7.Bp-1,(-0x1.5p-1),0x6.2p-1,0x6.2p-1,(-0x1.5p-1)},{(-0x7.Bp+1),(-0x6.4p-1),0x1.9p-1,0xD.3478C1p-54,(-0x1.Dp-1)}},{{0x1.8p+1,0x0.Dp+1,0x3.1p-1,0x7.Bp-1,0xA.E138A0p+75},{0x1.073DB7p+6,(-0x6.Ep-1),0x0.D0F543p+59,0x4.F9A70Bp+57,(-0x1.1p-1)},{0x1.8p+1,0xE.42257Fp-75,0xF.790EEFp-1,0xE.42257Fp-75,0x1.8p+1},{(-0x7.Bp+1),0x0.2p+1,(-0x1.1p-1),0x0.D0F543p+59,(-0x6.4p-1)},{0x7.Bp-1,0x8.9p-1,0x2.ED4EDFp+90,(-0x1.2p-1),0xE.3642EFp-79},{(-0x7.3p-1),(-0x1.Dp+1),0x1.A03A65p+58,0x0.2p+1,(-0x6.4p-1)}}};
                    int32_t l_2300 = 7L;
                    int i, j, k;
                    (*g_605) = ((*g_245) > (safe_sub_func_uint8_t_u_u((((l_1970[2][2] > p_27) != ((*g_751) != g_2285)) ^ ((safe_sub_func_int8_t_s_s(p_27, ((safe_lshift_func_int16_t_s_s(p_27, 13)) , (safe_lshift_func_int8_t_s_u((*g_1463), ((&p_28 == (void*)0) , p_27)))))) == p_27)), (**g_577))));
                    l_2304 = ((-0x8.Bp+1) == (safe_div_func_float_f_f((l_2254 = (l_2295 == l_2296[0][0])), ((((((safe_lshift_func_int16_t_s_u((l_2300 &= 0x10ADL), (((safe_sub_func_int32_t_s_s((l_1881[6][0] = l_1970[2][4]), p_27)) , l_2303) == (void*)0))) <= ((*g_2285) != l_1931)) == (*g_1463)) | p_27) , 0xA.394643p-43) > p_27))));
                    if (l_2305)
                        continue;
                }
                else
                { /* block id: 924 */
                    for (l_1951 = (-21); (l_1951 == 4); l_1951++)
                    { /* block id: 927 */
                        return l_2000;
                    }
                }
            }
        }
    }
    l_2325 = ((safe_add_func_float_f_f(((safe_div_func_float_f_f((safe_div_func_float_f_f((l_2314 == l_1880), 0x0.1874DAp-65)), p_27)) > 0x6.BDE64Fp-64), ((safe_add_func_float_f_f(((((g_32 |= (((((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((((-4L) & ((safe_rshift_func_uint8_t_u_s((p_27 > 0x18L), 5)) , p_27)) | l_1891), (*g_245))), 0x77L)) , p_27) , 4294967291UL) <= (*g_190)) , l_1839[6])) ^ g_2324[0][1][0]) , p_27) > p_27), p_27)) == 0x1.2p+1))) > p_27);
    for (g_150 = 0; (g_150 <= 7); g_150 += 1)
    { /* block id: 938 */
        int32_t l_2328[5][10] = {{0x61175C87L,(-1L),0L,(-1L),0x61175C87L,0x61175C87L,(-1L),0L,(-1L),0x61175C87L},{0x61175C87L,(-1L),0L,(-1L),0x61175C87L,0x61175C87L,(-1L),0L,(-1L),0x61175C87L},{0x61175C87L,(-1L),0L,(-1L),0x61175C87L,0x61175C87L,(-1L),0L,(-1L),0x61175C87L},{0x61175C87L,(-1L),0L,(-1L),0x61175C87L,0x61175C87L,(-1L),0L,(-1L),0x61175C87L},{0x61175C87L,(-1L),0L,(-1L),0x61175C87L,0x61175C87L,(-1L),0L,(-1L),0x61175C87L}};
        int64_t l_2342 = 0x401D8B2D29C6149ALL;
        int32_t *l_2366[7];
        int i, j;
        for (i = 0; i < 7; i++)
            l_2366[i] = &l_2328[1][9];
        if (p_27)
        { /* block id: 939 */
            uint64_t l_2341 = 0x79C14114C53FB5D6LL;
            int32_t l_2343 = 0L;
            int32_t l_2354[10] = {0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL,0x4906A58EL};
            uint8_t **l_2359 = &g_1460[1];
            int i;
            for (g_286 = 0; (g_286 <= 7); g_286 += 1)
            { /* block id: 942 */
                uint64_t l_2339 = 1UL;
                uint8_t **l_2357 = (void*)0;
                l_2328[0][5] = (safe_rshift_func_uint16_t_u_u(p_27, 12));
                for (l_2325 = 1; (l_2325 >= 0); l_2325 -= 1)
                { /* block id: 946 */
                    uint32_t *****l_2332[7];
                    uint16_t *l_2340[4][7][3] = {{{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791}},{{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0}},{{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791}},{{&g_1791,(void*)0,(void*)0},{&g_1791,&g_1791,&g_1791},{&g_1791,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{(void*)0,&g_1791,&g_1791},{&g_1791,(void*)0,(void*)0},{(void*)0,&g_1791,&g_1791}}};
                    const int8_t *l_2352 = &l_1933;
                    int16_t *l_2353[4][6] = {{&l_2325,&l_1891,&l_1891,&l_2325,&l_1891,&l_1891},{&l_2325,&l_1891,&l_1891,&l_2325,&l_1891,&l_1891},{&l_2325,&l_1891,&l_1891,&l_2325,&l_1891,&l_1891},{&l_2325,&l_1891,&l_1891,&l_2325,&l_1891,&l_1891}};
                    uint8_t ***l_2358[5];
                    int32_t l_2360[5][1];
                    int32_t l_2361 = 0x1537C3E8L;
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_2332[i] = &l_2268;
                    for (i = 0; i < 5; i++)
                        l_2358[i] = &l_2357;
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_2360[i][j] = (-6L);
                    }
                    l_2328[0][5] |= (+(safe_add_func_int8_t_s_s((((l_2332[1] == (void*)0) < ((**g_1744) < ((safe_mod_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s((((void*)0 != l_2332[2]) || (l_2339 != ((((g_186[l_2325][l_2325] == &p_27) != (l_2341 &= (*g_245))) != l_2342) , 4L))), 1)), 1UL)), (*g_245))) | p_27))) >= l_2343), l_2343)));
                    l_2361 ^= ((safe_mul_func_int16_t_s_s((safe_add_func_uint64_t_u_u((safe_add_func_int16_t_s_s(((safe_sub_func_int64_t_s_s((p_27 , (((*g_1993) , ((((*g_577) = (*g_577)) != l_2352) , (l_2354[9] = (l_2343 ^= (-3L))))) | (+((g_2356[0] == ((l_2357 = l_2357) != l_2359)) , 65535UL)))), l_2360[1][0])) >= l_2339), (*g_245))), (**g_891))), 0x330FL)) | 0x64F5ABE080F98811LL);
                    return l_2328[0][5];
                }
            }
            return p_27;
        }
        else
        { /* block id: 958 */
            int32_t **l_2362 = (void*)0;
            int32_t **l_2363 = &g_193;
            int32_t **l_2364 = &g_66;
            int32_t *l_2365[5][3] = {{&g_513,&l_2328[0][5],&l_2328[0][5]},{&l_1880,&l_1881[5][0],&l_1881[5][0]},{&g_513,&l_2328[0][5],&l_2328[0][5]},{&l_1880,&l_1881[5][0],&l_1881[5][0]},{&g_513,&l_2328[0][5],&l_2328[0][5]}};
            int32_t **l_2367[2];
            int i, j;
            for (i = 0; i < 2; i++)
                l_2367[i] = (void*)0;
            (*g_605) = (((l_2365[3][0] = (void*)0) != (l_2323[0] = (l_2366[3] = (void*)0))) >= p_27);
        }
        if (p_27)
            break;
    }
    return p_27;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_48 g_65 g_94 g_68 g_96 g_24 g_123 g_106 g_141 g_150 g_165 g_156 g_137 g_190 g_187 g_66 g_192 g_193 g_236 g_239 g_243 g_77 g_286 g_240 g_285 g_238 g_793 g_794 g_891 g_892 g_524 g_306 g_245 g_605 g_746 g_747 g_1010 g_995 g_745 g_577 g_578 g_742 g_606 g_1239 g_607 g_1248 g_741 g_1279 g_1280 g_856 g_1100 g_1077 g_950 g_1362 g_785 g_283 g_513 g_1407 g_237 g_1437 g_1471 g_1472 g_1473
 * writes: g_48 g_63 g_66 g_68 g_106 g_96 g_77 g_123 g_137 g_141 g_150 g_165 g_187 g_193 g_237 g_240 g_244 g_259 g_286 g_306 g_309 g_606 g_892 g_995 g_1239 g_1248 g_1280 g_779 g_513 g_524 g_1407 g_1412 g_1437 g_1460 g_1463 g_1471 g_283
 */
static uint16_t  func_35(uint16_t  p_36, int32_t * p_37)
{ /* block id: 1 */
    int8_t *l_47 = &g_48;
    int32_t *l_53 = &g_32;
    int8_t *l_76 = &g_77;
    int8_t **l_78 = &l_76;
    const int32_t l_343 = 0L;
    int32_t l_352 = 0x31365AFAL;
    int32_t l_358 = 1L;
    int32_t l_359[1];
    float l_493 = (-0x9.0p+1);
    uint32_t l_526 = 0x9E85AC3EL;
    int32_t l_528[9][8] = {{0x9B7B12D7L,0x82B9D657L,1L,(-4L),1L,0x82B9D657L,0x9B7B12D7L,0x70239D88L},{0x31FA74BAL,(-1L),0L,(-4L),0x9B7B12D7L,0xC88D81F1L,0L,0xDE7EF8DFL},{0x2AC313E8L,(-4L),0xDF41160DL,1L,0x9B7B12D7L,1L,0xDF41160DL,(-4L)},{0x31FA74BAL,0x82B9D657L,0x2AC313E8L,0xDE7EF8DFL,1L,1L,0x31FA74BAL,0x70239D88L},{0x9B7B12D7L,(-4L),0L,(-1L),0x31FA74BAL,0xC88D81F1L,0x31FA74BAL,(-1L)},{0x2AC313E8L,(-1L),0x2AC313E8L,1L,0L,0x82B9D657L,0xDF41160DL,(-1L)},{0L,0x82B9D657L,0xDF41160DL,(-1L),1L,0x70239D88L,0L,0x70239D88L},{0L,0xDE7EF8DFL,0L,0xDE7EF8DFL,0L,0xC88D81F1L,0x9B7B12D7L,(-4L)},{0x2AC313E8L,0xDE7EF8DFL,1L,1L,0x31FA74BAL,0x70239D88L,0xDF41160DL,0xDE7EF8DFL}};
    const uint64_t l_701 = 1UL;
    float l_710[5];
    uint8_t **l_740 = &g_106;
    int16_t *l_766 = &g_123[0];
    uint32_t l_786 = 0x8FDFF626L;
    float l_787 = 0x7.2p-1;
    int32_t l_788 = 0L;
    int16_t ***l_814 = (void*)0;
    uint32_t **l_835 = (void*)0;
    uint32_t l_860 = 0xB4CCAAB1L;
    uint8_t l_989 = 0x9EL;
    uint16_t *l_997[3];
    uint16_t **l_996 = &l_997[2];
    const float *l_1102 = &g_309;
    const float **l_1101 = &l_1102;
    uint64_t * const *l_1203 = &g_892[1][0];
    int64_t **l_1229 = &g_1010;
    uint32_t l_1275[9][7] = {{0x74546876L,7UL,0x50C10E91L,0xF4C4E0AAL,0x50C10E91L,7UL,0x74546876L},{0xA3EF989CL,6UL,0x3B02C096L,0UL,0x3B02C096L,6UL,0xA3EF989CL},{0x74546876L,7UL,0x50C10E91L,0xF4C4E0AAL,0x50C10E91L,7UL,0x74546876L},{0xA3EF989CL,6UL,0x3B02C096L,0UL,0x3B02C096L,6UL,0xA3EF989CL},{0x74546876L,7UL,0x50C10E91L,0xF4C4E0AAL,0x50C10E91L,7UL,0x74546876L},{0xA3EF989CL,6UL,0x3B02C096L,0UL,0x3B02C096L,6UL,0xA3EF989CL},{0x74546876L,7UL,0x50C10E91L,0xF4C4E0AAL,0x50C10E91L,7UL,0x74546876L},{0xA3EF989CL,6UL,0x3B02C096L,0UL,0x3B02C096L,6UL,0xA3EF989CL},{0x74546876L,7UL,0x50C10E91L,0xF4C4E0AAL,0x50C10E91L,7UL,0x74546876L}};
    uint16_t ***l_1375 = &l_996;
    uint16_t ****l_1374 = &l_1375;
    uint32_t *l_1397 = &g_1239;
    uint32_t **l_1396 = &l_1397;
    float l_1401[8][7][4] = {{{0xC.BDB537p+61,(-0x7.Bp-1),0x9.EE94F4p+13,0x0.0p-1},{0xD.7FB71Cp-28,0x1.B460B4p+45,0x6.D438C9p+89,0x9.7D92D7p-29},{0x6.D438C9p+89,0x9.7D92D7p-29,0x9.7D92D7p-29,0x6.D438C9p+89},{0xE.BD5FB9p+67,0xB.CF3E4Bp-88,0x0.0p-1,0x9.Cp+1},{0x7.4p-1,0xC.BDB537p+61,0x4.Ep+1,0x8.C6EBCFp-68},{0x9.EE94F4p+13,0x8.7p-1,0x3.Cp+1,0x8.C6EBCFp-68},{0x0.1p-1,0xC.BDB537p+61,0x6.D438C9p+89,0x9.Cp+1}},{{0x0.8p-1,0xB.CF3E4Bp-88,0x7.4p-1,0x6.D438C9p+89},{0x8.7p-1,0x9.7D92D7p-29,0x1.93CEA0p+7,0x9.7D92D7p-29},{0x7.4p-1,0x1.B460B4p+45,0x9.C717D2p+44,0x0.0p-1},{(-0x1.Cp+1),0x8.7p-1,0xB.CF3E4Bp-88,0x4.Ep+1},{0x4.FA8513p+35,0x5.6p+1,0x6.D438C9p+89,0x3.Cp+1},{0x4.FA8513p+35,0x9.Cp+1,0xB.CF3E4Bp-88,0x6.D438C9p+89},{(-0x1.Cp+1),0x3.Cp+1,0x9.C717D2p+44,0x7.4p-1}},{{0x7.4p-1,(-0x7.Bp-1),0x1.93CEA0p+7,0x1.93CEA0p+7},{0x8.7p-1,0x8.7p-1,0x7.4p-1,0x9.C717D2p+44},{0x0.8p-1,0xE.564A69p-26,0x6.D438C9p+89,0xB.CF3E4Bp-88},{0x0.1p-1,0x7.4p-1,0x3.Cp+1,0x6.D438C9p+89},{0x9.EE94F4p+13,0x7.4p-1,0x4.Ep+1,0xB.CF3E4Bp-88},{0x7.4p-1,0xE.564A69p-26,0x0.0p-1,0x9.C717D2p+44},{0xE.BD5FB9p+67,0x8.7p-1,0x9.7D92D7p-29,0x1.93CEA0p+7}},{{0x6.D438C9p+89,(-0x7.Bp-1),0x6.D438C9p+89,0x7.4p-1},{0xD.7FB71Cp-28,0x3.Cp+1,0x9.Cp+1,0x6.D438C9p+89},{0xF.5F5B6Ep-16,0x9.Cp+1,0x8.C6EBCFp-68,0x3.Cp+1},{0x7.4p-1,0x5.6p+1,0x8.C6EBCFp-68,0x4.Ep+1},{0xF.5F5B6Ep-16,0x8.7p-1,0x9.Cp+1,0x0.0p-1},{0xD.7FB71Cp-28,0x1.B460B4p+45,0x6.D438C9p+89,0x9.7D92D7p-29},{0x6.D438C9p+89,0x9.7D92D7p-29,0x9.7D92D7p-29,0x6.D438C9p+89}},{{0xE.BD5FB9p+67,0xB.CF3E4Bp-88,0x0.0p-1,0x9.Cp+1},{0x7.4p-1,0xC.BDB537p+61,0x4.Ep+1,0x8.C6EBCFp-68},{0x9.EE94F4p+13,0x8.7p-1,0x3.Cp+1,0x8.C6EBCFp-68},{0x0.1p-1,0xC.BDB537p+61,0x6.D438C9p+89,0x9.Cp+1},{0x0.8p-1,0xB.CF3E4Bp-88,0x7.4p-1,0x6.D438C9p+89},{0x8.7p-1,0x9.7D92D7p-29,0x1.93CEA0p+7,0x9.7D92D7p-29},{0x7.4p-1,0x1.B460B4p+45,0x9.C717D2p+44,0x0.0p-1}},{{(-0x1.Cp+1),0x8.7p-1,0xB.CF3E4Bp-88,0x4.Ep+1},{0x4.FA8513p+35,0x5.6p+1,0x6.D438C9p+89,0x3.Cp+1},{0x4.FA8513p+35,0x9.Cp+1,0xB.CF3E4Bp-88,0x6.D438C9p+89},{(-0x1.Cp+1),0x3.Cp+1,0x9.C717D2p+44,0x7.4p-1},{0x7.4p-1,(-0x7.Bp-1),0x1.93CEA0p+7,0x1.93CEA0p+7},{0x8.7p-1,0x8.7p-1,0x7.4p-1,0x9.C717D2p+44},{0x0.8p-1,0xE.564A69p-26,0x6.D438C9p+89,0xB.CF3E4Bp-88}},{{0x0.1p-1,0x7.4p-1,0x3.Cp+1,0x6.D438C9p+89},{0x9.EE94F4p+13,0x7.4p-1,0x4.Ep+1,0xB.CF3E4Bp-88},{0x7.4p-1,0xE.564A69p-26,0x0.0p-1,0x9.C717D2p+44},{0xE.BD5FB9p+67,0x8.7p-1,0x9.7D92D7p-29,0x1.93CEA0p+7},{0x6.D438C9p+89,(-0x7.Bp-1),0x6.D438C9p+89,0x7.4p-1},{0xD.7FB71Cp-28,0x3.Cp+1,0x9.Cp+1,0x6.D438C9p+89},{0xF.5F5B6Ep-16,0x9.Cp+1,0x1.B460B4p+45,0x0.8p-1}},{{0x4.FA8513p+35,0x9.EE94F4p+13,0x1.B460B4p+45,0xE.564A69p-26},{0x3.Cp+1,0x7.4p-1,0x0.1p-1,0x5.6p+1},{0x0.0p-1,0xE.BD5FB9p+67,0x8.C6EBCFp-68,0x6.D438C9p+89},{0x8.C6EBCFp-68,0x6.D438C9p+89,0x6.D438C9p+89,0x8.C6EBCFp-68},{0xB.CF3E4Bp-88,0xD.7FB71Cp-28,0x5.6p+1,0x0.1p-1},{0x4.FA8513p+35,0xF.5F5B6Ep-16,0xE.564A69p-26,0x1.B460B4p+45},{0x9.Cp+1,0x7.4p-1,0x0.8p-1,0x1.B460B4p+45}}};
    const uint8_t * const l_1440 = &l_989;
    float l_1513[8] = {(-0x1.Fp+1),(-0x1.Fp+1),0xB.2CC75Cp+45,(-0x1.Fp+1),(-0x1.Fp+1),0xB.2CC75Cp+45,(-0x1.Fp+1),(-0x1.Fp+1)};
    const uint64_t l_1560 = 18446744073709551612UL;
    float l_1616 = 0x0.0EEDB8p+44;
    int32_t l_1800 = 0L;
    int32_t l_1808 = 0xAA8921DBL;
    int32_t *l_1826 = &l_359[0];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_359[i] = 0L;
    for (i = 0; i < 5; i++)
        l_710[i] = 0x0.57D428p-18;
    for (i = 0; i < 3; i++)
        l_997[i] = (void*)0;
    if (((safe_mul_func_int8_t_s_s(func_40(p_37, g_32, ((((safe_add_func_int8_t_s_s(((*l_47) |= g_32), (func_49(l_53, (safe_mod_func_uint32_t_u_u((func_56(p_36, l_47, (*l_53)) == (((*l_78) = l_76) != &g_77)), (*l_53))), p_36) || 0L))) & p_36) , g_165) , 0x8BA1A4CFL), g_238), 0xD0L)) , 1L))
    { /* block id: 110 */
        int16_t *l_332 = &g_123[0];
        int32_t *l_333 = &g_187;
        int8_t l_351 = 0xE2L;
        int32_t l_353 = 0x64E6E990L;
        int32_t l_354 = 0xE4CFCB6AL;
        int32_t l_355 = 3L;
        int32_t l_356 = 0xDE2B1153L;
        int32_t l_357 = 0x5E36411CL;
        int32_t l_360 = 0x07E34465L;
        int32_t l_361 = 0x771989D6L;
        int32_t l_362 = 0xF9359177L;
        int32_t l_363 = 0x46D1FC2CL;
        int32_t l_364 = 0x5456450CL;
        int32_t l_365 = (-6L);
        int32_t l_366[4];
        uint64_t l_370 = 18446744073709551614UL;
        uint16_t l_451 = 0x6CDFL;
        int32_t ***l_462 = (void*)0;
        int16_t l_478 = 5L;
        int16_t l_556 = 0xD194L;
        float l_570 = 0x4.0B067Cp-29;
        int8_t l_601 = (-2L);
        int8_t ***l_621 = &l_78;
        uint8_t l_658 = 247UL;
        uint16_t l_661[6] = {0xEC43L,0xEC43L,0xEC43L,0xEC43L,0xEC43L,0xEC43L};
        int32_t l_722 = 0x40818F64L;
        uint8_t l_812 = 0x4EL;
        uint32_t l_813 = 4294967290UL;
        uint8_t l_822 = 248UL;
        uint32_t *l_834 = &l_526;
        uint32_t **l_833[7] = {&l_834,&l_834,&l_834,&l_834,&l_834,&l_834,&l_834};
        uint16_t *l_875 = &l_661[0];
        const float *l_895 = (void*)0;
        const float **l_894[4] = {&l_895,&l_895,&l_895,&l_895};
        const float ***l_893 = &l_894[0];
        int64_t *l_927 = &g_283;
        int8_t l_935[1];
        int32_t *l_936 = &l_359[0];
        int32_t *l_937 = (void*)0;
        int32_t *l_938 = (void*)0;
        int32_t *l_939 = &l_366[1];
        int32_t *l_940[5][7][4] = {{{&g_187,&l_353,(void*)0,&g_187},{(void*)0,&g_187,(void*)0,&l_355},{&l_361,&g_187,&l_356,&l_353},{&l_361,&l_354,&g_513,&l_364},{&l_359[0],&l_361,&g_187,&l_359[0]},{&l_354,&l_722,&l_722,&l_354},{&l_356,&l_366[1],&l_354,&g_187}},{{&l_358,&l_365,&g_187,&l_354},{&l_722,(void*)0,&l_366[2],&l_354},{(void*)0,&l_365,&g_187,&g_187},{&g_187,&l_366[1],&l_359[0],&l_354},{(void*)0,&l_722,&g_187,&l_359[0]},{&l_353,&l_361,&l_354,&l_364},{&l_366[1],&l_354,&g_68,&l_353}},{{&l_365,&g_187,&l_352,&l_355},{&l_354,&g_187,&l_364,&g_187},{&l_354,&l_353,&g_187,&g_513},{&g_513,&l_354,&l_359[0],&l_365},{&l_366[2],&g_513,&l_366[1],&l_355},{&l_366[2],&l_359[0],&l_359[0],&g_187},{&g_513,&l_355,&g_187,(void*)0}},{{&l_354,&l_359[0],&l_364,&l_366[1]},{&l_354,&l_355,&l_352,&l_359[0]},{&l_365,&l_352,&g_68,&l_354},{&l_366[1],&l_362,&l_354,(void*)0},{&l_353,&g_187,&g_187,&g_187},{(void*)0,(void*)0,&l_359[0],&l_361},{&g_187,(void*)0,&g_187,&l_366[1]}},{{(void*)0,&l_358,&l_366[2],&g_187},{&l_722,&l_358,&g_187,&l_366[1]},{&l_358,(void*)0,&l_354,&l_361},{&l_356,(void*)0,&l_722,&g_187},{&l_354,&g_187,&g_187,(void*)0},{&l_359[0],&l_362,&g_513,&g_513},{(void*)0,&l_362,&l_366[1],&g_68}}};
        uint32_t l_941[8][1][4] = {{{0x0CEF3980L,1UL,1UL,1UL}},{{0x0CEF3980L,0x4B21A42FL,1UL,0x4B21A42FL}},{{0x0CEF3980L,1UL,1UL,1UL}},{{0x0CEF3980L,0x4B21A42FL,1UL,0x4B21A42FL}},{{0x0CEF3980L,1UL,1UL,1UL}},{{0x0CEF3980L,0x4B21A42FL,1UL,0x4B21A42FL}},{{0x0CEF3980L,1UL,1UL,1UL}},{{0x0CEF3980L,0x4B21A42FL,1UL,0x4B21A42FL}}};
        int32_t **l_948 = &g_779;
        int32_t ***l_949 = (void*)0;
        int32_t ***l_951 = &l_948;
        int32_t ****l_957 = &l_462;
        int32_t *****l_956 = &l_957;
        uint64_t l_958 = 18446744073709551615UL;
        int8_t l_1001 = 0xAAL;
        uint16_t * const *l_1061 = (void*)0;
        uint16_t * const **l_1060 = &l_1061;
        uint16_t * const ***l_1059 = &l_1060;
        int64_t l_1142 = (-1L);
        int16_t l_1148[3];
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_366[i] = 0x697EB821L;
        for (i = 0; i < 1; i++)
            l_935[i] = 0x09L;
        for (i = 0; i < 3; i++)
            l_1148[i] = (-6L);
    }
    else
    { /* block id: 429 */
        const uint8_t *l_1166 = &g_511;
        int32_t l_1172[1];
        int32_t l_1271 = 0L;
        uint32_t l_1273 = 4UL;
        int32_t ***l_1278 = &g_747;
        int32_t ****l_1297 = &g_1280;
        int64_t **l_1422 = &g_1010;
        uint32_t ***l_1489 = (void*)0;
        const int32_t l_1516[10][9] = {{0x6330B103L,0xC03D1C29L,1L,7L,0xC03D1C29L,6L,1L,4L,(-9L)},{0xC2C83CAFL,0L,(-3L),7L,0xFFF7326FL,0x552E3185L,0xC574D026L,0xFFF7326FL,(-9L)},{0x3D1EBBE9L,0xC03D1C29L,0xA214F0A0L,6L,0xFFF7326FL,0xA92F80B5L,1L,0x01B75617L,0x6330B103L},{0x3D1EBBE9L,0xFFF7326FL,(-3L),0xA92F80B5L,0xC03D1C29L,0xA92F80B5L,(-3L),0xFFF7326FL,0x3D1EBBE9L},{0xC2C83CAFL,0xFFF7326FL,1L,6L,0L,0x552E3185L,(-3L),4L,0x6330B103L},{0x6330B103L,0xC03D1C29L,1L,7L,0xC03D1C29L,6L,1L,4L,(-9L)},{0xC2C83CAFL,0L,(-3L),7L,0xFFF7326FL,0x552E3185L,0xC574D026L,0xFFF7326FL,(-9L)},{0x3D1EBBE9L,0xC03D1C29L,0xA214F0A0L,6L,0xFFF7326FL,0xA92F80B5L,1L,0x01B75617L,0x6330B103L},{0x3D1EBBE9L,0xFFF7326FL,(-3L),0xA92F80B5L,0xC03D1C29L,0xA92F80B5L,(-3L),0xFFF7326FL,0x3D1EBBE9L},{0xC2C83CAFL,0xFFF7326FL,1L,6L,0L,0x552E3185L,(-3L),4L,0x6330B103L}};
        int32_t l_1578 = (-7L);
        uint32_t l_1601 = 0UL;
        uint32_t l_1605 = 0xFC0D72DCL;
        uint16_t l_1657[10][8] = {{0xEE19L,0x0E3AL,0xEE19L,1UL,65526UL,0UL,65530UL,0xF1F4L},{65530UL,0x0D1EL,0xA09DL,0x68C9L,0UL,0xA09DL,65526UL,5UL},{65530UL,7UL,65535UL,0x6090L,65526UL,0x9901L,0UL,2UL},{0xEE19L,0x044BL,0xC179L,0xCE2AL,0x6090L,65535UL,0x802BL,65526UL},{0x2E1BL,1UL,0xCE2AL,65535UL,0xA09DL,0xA09DL,65535UL,0xCE2AL},{65526UL,65526UL,0xDE26L,65530UL,65529UL,65535UL,0x0E3AL,0x2E1BL},{0xA09DL,0x0E3AL,0xC179L,0x0D1EL,0xF1F4L,0x802BL,0xA09DL,0x2E1BL},{0x0E3AL,0UL,1UL,65530UL,0xCE2AL,0x0D1EL,65526UL,0xCE2AL},{0x0D1EL,65528UL,65535UL,0x0D1EL,0x802BL,2UL,0xEE19L,0xCE2AL},{65528UL,1UL,65535UL,0x6E72L,65530UL,0x6E72L,65535UL,1UL}};
        float ****l_1661 = &g_1412;
        float *****l_1660 = &l_1661;
        float l_1696 = 0x9.0p-1;
        const int32_t * const l_1768 = &g_32;
        int16_t *l_1837[3][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_123[0],&g_123[0],&g_123[0],&g_123[0],&g_123[0],&g_123[0],&g_123[0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        int i, j;
        for (i = 0; i < 1; i++)
            l_1172[i] = 0xAD1D1BF7L;
        if ((*g_240))
        { /* block id: 430 */
            int8_t l_1165 = 0x64L;
            uint16_t ***l_1171 = &l_996;
            int32_t l_1178 = 0x9FD039AFL;
            const uint64_t *l_1185 = (void*)0;
            int8_t l_1211 = (-10L);
            uint32_t ***l_1244 = &l_835;
            uint32_t ****l_1243 = &l_1244;
            const int16_t *l_1256 = &g_123[0];
            float *l_1281 = &l_787;
lbl_1242:
            (*g_605) = (safe_sub_func_uint32_t_u_u(l_1165, (l_1166 != ((safe_mod_func_uint8_t_u_u(((safe_div_func_int64_t_s_s((((**l_740) |= ((((**g_793) , (p_36 , p_36)) , (**g_891)) < (((*l_1171) = (void*)0) != (void*)0))) != (7L != (*g_245))), (**g_891))) <= 0xE7L), p_36)) , (void*)0))));
            if ((l_1172[0] || 0xBA2053F5L))
            { /* block id: 434 */
                float *l_1177[3];
                int32_t l_1193 = 0x2E10831AL;
                int i;
                for (i = 0; i < 3; i++)
                    l_1177[i] = &l_710[1];
                (**g_746) = p_37;
                (*g_605) = (safe_div_func_int16_t_s_s(p_36, (safe_mod_func_int64_t_s_s(0x0327F3AF06BE2E86LL, (*g_1010)))));
                (***g_745) = (((l_1178 = 0x0.Fp-1) >= 0xB.5F9A85p+26) , p_37);
                for (p_36 = 0; (p_36 == 25); p_36++)
                { /* block id: 441 */
                    uint32_t *l_1191 = (void*)0;
                    uint32_t *l_1192 = &l_786;
                    int32_t l_1198 = 0x77821FE0L;
                    int32_t l_1230 = 0x2D20A359L;
                    uint32_t l_1235 = 6UL;
                    int8_t ***l_1236 = &l_78;
                    if ((safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((((*g_891) = (*g_891)) == (g_238 , l_1185)) == (g_150++)), (**g_577))), (((safe_add_func_uint64_t_u_u(((((*l_1192) = (+0x44L)) , (((l_1193 >= ((*l_53) == ((((0x9EA94E9A8ADA9CD6LL >= (safe_mul_func_int16_t_s_s((safe_add_func_uint32_t_u_u((3UL <= l_1165), (*g_794))), (-4L)))) , (-0x1.6p+1)) > l_1172[0]) == (-0x1.3p+1)))) <= 0x6.C0D8A0p+29) , p_37)) != (void*)0), l_1198)) == (*g_106)) , 0L))))
                    { /* block id: 445 */
                        uint64_t **l_1204 = (void*)0;
                        int32_t l_1212 = 3L;
                        (*g_605) |= ((safe_add_func_int8_t_s_s((safe_div_func_uint16_t_u_u(((l_1193 != (((((l_1193 < (p_36 , l_1172[0])) && l_1172[0]) , l_1203) == l_1204) < (p_36 >= ((((safe_lshift_func_int8_t_s_u((safe_div_func_uint32_t_u_u((safe_add_func_uint8_t_u_u((*g_106), l_1211)), 4294967295UL)), (**g_742))) | l_1193) , 4L) , p_36)))) == l_1212), (*l_53))), (*g_578))) == p_36);
                    }
                    else
                    { /* block id: 447 */
                        l_1178 = (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((((*g_794) > (((*l_766) = (((~(safe_sub_func_int32_t_s_s(l_1198, ((safe_sub_func_int32_t_s_s(((safe_sub_func_uint32_t_u_u((((((~((safe_mul_func_int16_t_s_s(((l_1230 &= (((void*)0 == l_1229) > 0L)) >= ((safe_div_func_int16_t_s_s(((((*g_1010) = (*l_53)) != (**g_891)) < (safe_sub_func_uint64_t_u_u((**g_891), 1UL))), g_96)) < 0UL)), l_1193)) < (*l_53))) < l_1235) >= 6UL) , l_1178) ^ (*l_53)), 7L)) && 0x6881102CL), (-1L))) != (-5L))))) , l_1235) < (**g_577))) > (-1L))) , &g_577) != l_1236), 0x5.316E25p-27)), p_36));
                        return (*g_245);
                    }
                    (*g_605) ^= 0x8D60CF3FL;
                    (*g_747) = p_37;
                }
            }
            else
            { /* block id: 457 */
                int32_t *l_1237 = &l_359[0];
                int32_t *l_1238[10] = {&l_352,&l_352,&l_352,&l_352,&l_352,&l_352,&l_352,&l_352,&l_352,&l_352};
                uint32_t *****l_1245 = &l_1243;
                int16_t *l_1246 = (void*)0;
                uint32_t l_1247[10][4][1];
                int i, j, k;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_1247[i][j][k] = 0UL;
                    }
                }
                g_1239--;
                if (g_77)
                    goto lbl_1242;
                (*l_1245) = l_1243;
                if ((((void*)0 == l_1246) <= (p_36 >= 0x7FL)))
                { /* block id: 461 */
                    l_1178 |= 1L;
                    for (l_1211 = 0; (l_1211 >= 0); l_1211 -= 1)
                    { /* block id: 465 */
                        (***g_745) = p_37;
                        l_359[0] = (**g_607);
                    }
                    l_1247[5][3][0] ^= 1L;
                    return p_36;
                }
                else
                { /* block id: 471 */
                    volatile float ** volatile * volatile * volatile *l_1249 = &g_1248;
                    int32_t l_1267 = 0x6CD3261CL;
                    int32_t l_1274[1][2];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_1274[i][j] = (-1L);
                    }
                    (*l_1249) = g_1248;
                    for (l_788 = 0; (l_788 >= 0); l_788 -= 1)
                    { /* block id: 475 */
                        const int16_t **l_1257 = &l_1256;
                        int16_t **l_1258 = &l_766;
                        int32_t l_1268 = 0x31EEE0B2L;
                        int32_t l_1272 = 0xB17CB439L;
                        (*g_747) = ((((((safe_div_func_float_f_f((((((safe_add_func_int64_t_s_s((safe_rshift_func_int8_t_s_u(((-10L) || (((((*l_1257) = l_1256) != ((*l_1258) = l_1246)) , 3UL) , ((safe_rshift_func_uint16_t_u_s(((l_1165 , (((safe_mul_func_int16_t_s_s((((*g_578) = (safe_div_func_int16_t_s_s(((l_1268 = (l_1172[0] = (l_1267 |= (p_36--)))) > ((void*)0 == (*l_1244))), (safe_lshift_func_uint8_t_u_s(((l_1271 && l_1272) || 0x6F3EL), l_1271))))) > (***g_741)), (*g_245))) , l_1172[0]) & 0x673CCCE1900F984DLL)) , l_1267), 8)) ^ (*g_245)))), (**g_742))), (*l_53))) , l_1165) >= l_1165) <= l_1272) != l_1178), l_1273)) , l_1178) , 0xADL) <= 0xDCL) || 0x6B5E5A2AL) , p_37);
                    }
                    l_1275[3][2]++;
                    (*g_1279) = l_1278;
                }
            }
            (*l_1281) = p_36;
        }
        else
        { /* block id: 490 */
            int32_t l_1282 = 1L;
            int32_t l_1294[10][8] = {{1L,(-7L),0xA8591CEAL,0L,(-8L),0xA8591CEAL,0x643E15C3L,5L},{(-7L),0xFB9CCE7EL,1L,(-10L),1L,0xFB9CCE7EL,(-7L),0x643E15C3L},{0x8F82B593L,(-10L),0x3C6CB20AL,8L,(-10L),0L,(-8L),0x8F82B593L},{0xFD89C94AL,0x8F82B593L,0L,0xFB9CCE7EL,(-10L),0xA8591CEAL,0xA8591CEAL,(-10L)},{0x8F82B593L,0x643E15C3L,0x643E15C3L,0x8F82B593L,1L,0xFD89C94AL,5L,0xFB9CCE7EL},{(-7L),5L,0L,8L,(-8L),0xF5661F6CL,8L,(-7L)},{1L,5L,0L,0xFD89C94AL,0L,0xFD89C94AL,0L,5L},{5L,0x643E15C3L,0xA8591CEAL,(-8L),0L,0xA8591CEAL,(-7L),1L},{(-7L),0x8F82B593L,1L,(-10L),5L,0L,(-7L),(-7L)},{0xFB9CCE7EL,(-10L),0xA8591CEAL,0xA8591CEAL,(-10L),0xFB9CCE7EL,0L,0x8F82B593L}};
            uint32_t l_1295[6] = {0xD2F6AD03L,0xA2595791L,0xA2595791L,0xD2F6AD03L,0xA2595791L,0xA2595791L};
            int32_t l_1296 = (-1L);
            int32_t *l_1300 = (void*)0;
            const uint8_t **l_1322[10];
            uint32_t l_1334 = 1UL;
            int32_t l_1363 = 0x0C4A92CFL;
            uint32_t *l_1395 = &l_1275[3][2];
            uint32_t **l_1394 = &l_1395;
            int64_t **l_1424[2][2][10] = {{{&g_785,(void*)0,(void*)0,&g_785,(void*)0,(void*)0,&g_785,(void*)0,(void*)0,&g_785},{(void*)0,&g_785,(void*)0,(void*)0,&g_785,(void*)0,(void*)0,&g_785,(void*)0,(void*)0}},{{&g_785,&g_785,&g_1010,&g_785,&g_785,&g_1010,&g_785,&g_785,&g_1010,&g_785},{&g_785,(void*)0,(void*)0,&g_785,(void*)0,(void*)0,&g_785,(void*)0,(void*)0,&g_785}}};
            uint8_t **l_1452[10][1] = {{(void*)0},{(void*)0},{&g_106},{&g_106},{&g_106},{(void*)0},{(void*)0},{&g_106},{&g_106},{&g_106}};
            uint32_t l_1454 = 0xF0B8B4E5L;
            uint32_t ****l_1490 = &g_1471;
            float *l_1491[1][10][2] = {{{&l_710[3],&g_1051},{(void*)0,(void*)0},{(void*)0,&g_1051},{&l_710[3],(void*)0},{&l_710[3],(void*)0},{&l_787,&l_710[3]},{&g_1051,&g_1051},{&g_1051,&l_710[3]},{&l_787,(void*)0},{&l_710[3],(void*)0}}};
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_1322[i] = &l_1166;
            for (l_786 = 2; (l_786 <= 8); l_786 += 1)
            { /* block id: 493 */
                int32_t ****l_1293 = &g_1280;
                const int32_t *l_1301[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                uint8_t **l_1323[4];
                int32_t l_1328[10] = {0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL,0x8AC3CD1BL};
                float ***l_1360 = &g_534;
                uint8_t ** const l_1378[4][2] = {{&g_106,(void*)0},{&g_106,(void*)0},{&g_106,(void*)0},{&g_106,(void*)0}};
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1323[i] = (void*)0;
                l_1296 |= ((l_1282 = l_1282) | (safe_add_func_uint32_t_u_u((safe_rshift_func_int16_t_s_s(((((**g_577) = ((safe_mod_func_int32_t_s_s((((safe_div_func_int8_t_s_s((safe_add_func_int8_t_s_s((((p_36 , p_36) || ((((*g_106) , &g_534) == (void*)0) | (((*l_1293) = (*g_1279)) != (*g_745)))) > ((*l_53) & 0xABB57E484789FCACLL)), 0x55L)), p_36)) && g_856) || (**g_891)), l_1294[9][7])) | l_1295[0])) > p_36) , l_1295[0]), 10)), l_1172[0])));
                if ((*l_53))
                    break;
                for (g_1239 = 2; (g_1239 <= 8); g_1239 += 1)
                { /* block id: 501 */
                    uint32_t l_1304 = 0x2A834EA7L;
                    uint8_t ***l_1324 = &l_1323[1];
                    int32_t *l_1327 = &g_513;
                    int32_t l_1332 = 8L;
                    int32_t l_1333 = 0x96D6FBA6L;
                    uint64_t l_1348 = 0UL;
                    uint32_t l_1365 = 0x783A27CFL;
                    uint32_t **l_1393 = (void*)0;
                    int i, j;
                    for (g_77 = 0; (g_77 <= 5); g_77 += 1)
                    { /* block id: 504 */
                        uint32_t *l_1305 = (void*)0;
                        int i;
                        l_1282 ^= ((((((((g_165 ^= ((void*)0 != &g_752)) & (l_1297 != &g_1280)) && (safe_add_func_uint8_t_u_u(((p_36 && (p_36 , ((((***g_1100) = l_1300) != l_1301[2]) != (safe_add_func_int8_t_s_s(((g_141 = l_1304) > (-10L)), (*g_578)))))) && (-7L)), 4L))) & p_36) && (*g_794)) | 0L) > p_36) > 0xFCL);
                    }
                    if ((safe_div_func_int32_t_s_s(((*l_1327) = (safe_add_func_int16_t_s_s(((*l_53) , (safe_lshift_func_int16_t_s_s((safe_sub_func_uint64_t_u_u(p_36, ((safe_mul_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u(((safe_div_func_int8_t_s_s((safe_mod_func_int16_t_s_s((((l_1322[0] == ((*l_1324) = l_1323[1])) | ((*l_47) ^= (*g_578))) > l_1304), p_36)), (safe_add_func_uint16_t_u_u((((0x1974C1C158F37A90LL > ((0x9C496CB1497639F2LL >= p_36) , 0x482DA1E3655275E7LL)) < (**g_577)) > l_1296), 0L)))) , 3L), 6)) & l_1295[2]), 4UL)) & p_36))), 3))), 0x64C4L))), 0x4ABC6F11L)))
                    { /* block id: 513 */
                        int32_t *l_1329 = &l_1328[2];
                        int32_t *l_1330[9][2] = {{&l_358,&l_1296},{&l_1296,&l_358},{&l_1328[3],&l_1328[3]},{&l_1328[3],&l_358},{&l_1296,&l_1296},{&l_358,&l_1296},{&l_1296,&l_358},{&l_1328[3],&l_1328[3]},{&l_1328[3],&l_358}};
                        uint32_t *l_1359 = &l_1304;
                        uint32_t *l_1361 = &l_860;
                        float *l_1364[1][5];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 5; j++)
                                l_1364[i][j] = &g_309;
                        }
                        l_1334--;
                        (*l_1329) = ((l_1365 = ((l_352 = ((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((*l_1329), 0)), ((*l_53) || ((**l_1203) = ((((((void*)0 != &l_997[2]) & ((*l_1327) = p_36)) & (safe_div_func_uint32_t_u_u(((*l_1361) |= ((safe_div_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s((safe_div_func_int16_t_s_s(((((l_1348 , (safe_lshift_func_uint8_t_u_u((***g_741), 5))) && ((safe_unary_minus_func_int32_t_s(((((**l_1229) &= (~((safe_sub_func_uint32_t_u_u(((*l_1359) = (safe_add_func_int32_t_s_s((safe_div_func_uint16_t_u_u(65535UL, (p_36 , p_36))), (*l_53)))), (*l_53))) <= (**g_891)))) ^ (**g_891)) || (**g_793)))) , p_36)) , (void*)0) == l_1360), 0xB96CL)))), 0x88L)) >= p_36)), g_141))) , g_1362) <= (*g_245)))))) , l_1363)) <= p_36)) >= p_36);
                        (*l_1327) = 0L;
                        (***g_745) = &l_1282;
                    }
                    else
                    { /* block id: 525 */
                        uint32_t l_1368 = 18446744073709551607UL;
                        uint8_t **l_1371[9][9] = {{&g_106,(void*)0,&g_106,&g_106,(void*)0,(void*)0,&g_106,&g_106,(void*)0},{&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106},{&g_106,&g_106,(void*)0,&g_106,(void*)0,&g_106,&g_106,&g_106,&g_106},{&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106},{&g_106,(void*)0,&g_106,(void*)0,&g_106,&g_106,&g_106,(void*)0,&g_106},{&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106},{(void*)0,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106},{(void*)0,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106},{&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106}};
                        uint32_t l_1391[6][6] = {{0x36777283L,5UL,0x36777283L,18446744073709551609UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551609UL,0x36777283L,0x36777283L,18446744073709551609UL,5UL,0x5AAE5761L},{0x5AAE5761L,18446744073709551609UL,18446744073709551615UL,18446744073709551609UL,0x5AAE5761L,18446744073709551609UL},{18446744073709551609UL,0x5AAE5761L,18446744073709551609UL,18446744073709551609UL,0x5AAE5761L,18446744073709551609UL},{0x36777283L,18446744073709551609UL,5UL,0x5AAE5761L,5UL,18446744073709551609UL},{5UL,0x36777283L,18446744073709551609UL,18446744073709551615UL,18446744073709551615UL,18446744073709551609UL}};
                        int32_t l_1392 = 0x2422E90CL;
                        int i, j;
                        l_1392 = (safe_mul_func_uint8_t_u_u((l_1368 != (safe_div_func_int16_t_s_s(((*l_766) = ((((*l_1324) = l_1371[1][5]) == ((safe_lshift_func_uint16_t_u_u((l_1374 == ((safe_rshift_func_int8_t_s_u((*g_578), 3)) , &g_243)), (*g_245))) , l_1378[0][0])) && ((safe_sub_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s(((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s((((safe_lshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_s(0x9D66L, 0)), (p_36 | 0x6CL))) && (*l_53)) < p_36), (*g_578))), (*g_785))) == p_36), p_36)), l_1391[3][5])) , l_1296))), (*l_1327)))), (**g_742)));
                        l_1394 = l_1393;
                        return (*g_245);
                    }
                    if ((*g_193))
                        continue;
                }
            }
            for (l_786 = 0; (l_786 <= 0); l_786 += 1)
            { /* block id: 537 */
                uint32_t ***l_1398 = (void*)0;
                uint32_t ***l_1399 = (void*)0;
                uint32_t ***l_1400 = &l_1396;
                int32_t l_1402 = 0L;
                float ****l_1451 = &g_1412;
                uint32_t ****l_1474 = &g_1471;
                int32_t *l_1475 = &l_358;
                int i;
                (*l_1400) = l_1396;
                l_1402 |= g_524[l_786];
                if (((((*g_106) = (((*g_578) &= ((void*)0 != &g_745)) ^ g_524[l_786])) , (**g_891)) <= (~l_1294[0][0])))
                { /* block id: 542 */
                    const float ****l_1410 = &g_1407;
                    float ****l_1411[9][1];
                    int32_t l_1415 = 0x0C3EAA7CL;
                    int64_t ***l_1423 = &l_1422;
                    int32_t l_1453 = 0x28A968B4L;
                    int i, j;
                    for (i = 0; i < 9; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_1411[i][j] = (void*)0;
                    }
                    l_359[0] &= ((+((safe_lshift_func_int8_t_s_u((((*l_1410) = g_1407) != (g_1412 = (void*)0)), (g_524[l_786] ^ (l_1415 >= (safe_mod_func_int8_t_s_s((safe_div_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((((**g_891) , 0x40C5L) < ((((*l_1423) = l_1422) == (l_1424[1][0][5] = (l_1229 = &g_785))) || (-10L))), (*g_578))), p_36)), p_36)))))) >= (**g_742))) == (*g_237));
                    if ((safe_div_func_int64_t_s_s(((*g_1010) = ((safe_div_func_int16_t_s_s(((*l_766) ^= (9L >= p_36)), g_524[l_786])) | p_36)), l_1415)))
                    { /* block id: 551 */
                        uint64_t *l_1436 = &g_1437;
                        int i;
                        (*g_605) = (~(((~((safe_add_func_uint64_t_u_u(((p_36 > (((((*g_794) && (*l_53)) || (safe_rshift_func_uint8_t_u_s((safe_unary_minus_func_uint64_t_u(((*l_1436) ^= (**g_891)))), (safe_div_func_uint16_t_u_u(((*g_742) != l_1440), 5UL))))) , (((safe_unary_minus_func_uint32_t_u(((safe_mod_func_int16_t_s_s((safe_div_func_uint8_t_u_u((((*l_766) = ((safe_sub_func_uint8_t_u_u(((l_1415 = p_36) == ((((safe_add_func_int16_t_s_s(((+(l_1451 != l_1451)) && (**g_891)), 0L)) , (-1L)) , l_1452[7][0]) == (void*)0)), 0L)) > l_1453)) & p_36), p_36)), 0x5970L)) , 0x5CA5C8D2L))) ^ l_1453) , (-1L))) || 1L)) < p_36), p_36)) > p_36)) && l_1454) >= p_36));
                        return p_36;
                    }
                    else
                    { /* block id: 557 */
                        return l_1415;
                    }
                }
                else
                { /* block id: 560 */
                    int16_t l_1455 = 0L;
                    l_1455 = 0xC9E4992AL;
                }
                (*l_1475) &= (safe_sub_func_int8_t_s_s((safe_add_func_int32_t_s_s(l_1294[9][7], ((**g_741) != (g_1460[1] = (*g_742))))), (safe_div_func_uint32_t_u_u((((*g_577) == (g_1463 = ((*l_78) = (*g_577)))) <= (safe_sub_func_uint16_t_u_u(((2UL > ((((((safe_sub_func_uint32_t_u_u(4294967286UL, g_137)) <= ((safe_unary_minus_func_int8_t_s((safe_add_func_uint64_t_u_u(((((*l_1474) = g_1471) != &l_835) , p_36), g_524[l_786])))) || 0x86L)) & 0x89FAL) <= 0x578AL) , l_1295[0]) , (**g_891))) ^ (**g_1472)), p_36))), l_1334))));
                for (g_1239 = 0; (g_1239 <= 1); g_1239 += 1)
                { /* block id: 570 */
                    int32_t *l_1476 = &l_1402;
                    int32_t *l_1477 = (void*)0;
                    int32_t *l_1478 = &l_1172[0];
                    int32_t *l_1479 = &l_1282;
                    int32_t *l_1480 = &g_187;
                    int32_t *l_1481 = &l_358;
                    int32_t *l_1482 = (void*)0;
                    int32_t *l_1483[2][3][6] = {{{&l_1294[9][7],&g_68,&l_359[0],&l_359[0],&g_68,&l_1294[9][7]},{&l_358,&l_1294[9][7],&l_359[0],&l_1294[9][7],&l_358,&l_358},{&g_187,&l_1294[9][7],&l_1294[9][7],&g_187,&g_68,&g_187}},{{&g_187,&g_68,&l_359[0],&g_187,&g_187,&l_359[0]},{&l_1294[9][7],&l_1294[9][7],&g_187,&g_68,&g_187,&l_1294[9][7]},{&g_187,&l_358,&g_68,&g_68,&l_358,&g_187}}};
                    uint8_t l_1484 = 255UL;
                    int i, j, k;
                    l_1484++;
                }
            }
            l_1282 = (safe_mul_func_float_f_f(((l_1489 != ((*l_1490) = &l_835)) > p_36), (-0x9.6p-1)));
            (***g_745) = &l_1294[9][7];
        }
        for (g_283 = (-20); (g_283 != 4); g_283 = safe_add_func_uint32_t_u_u(g_283, 4))
        { /* block id: 580 */
            float l_1512 = 0xE.70676Ap+10;
            int32_t l_1514 = 1L;
            uint32_t *l_1515 = &g_880;
            float **l_1543 = (void*)0;
            int32_t *l_1545[1][5];
            int32_t l_1577[5] = {0xC1BE0E7DL,0xC1BE0E7DL,0xC1BE0E7DL,0xC1BE0E7DL,0xC1BE0E7DL};
            int16_t l_1590 = 0x464EL;
            int32_t ***l_1597 = (void*)0;
            uint32_t l_1698 = 1UL;
            int8_t ***l_1703 = &g_577;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 5; j++)
                    l_1545[i][j] = (void*)0;
            }
        }
        (**l_1278) = p_37;
        (*g_605) &= (((safe_mul_func_uint8_t_u_u((*l_1826), (p_36 > (l_1271 |= ((((safe_rshift_func_uint8_t_u_s(((safe_rshift_func_int16_t_s_u(0L, (p_36 == p_36))) , 253UL), 0)) , p_36) , (l_1172[0] |= (((safe_mul_func_int8_t_s_s(((safe_add_func_uint64_t_u_u(((*l_1826) , (*l_1826)), p_36)) >= p_36), p_36)) >= 0UL) == p_36))) == (*l_1826)))))) < p_36) , p_36);
    }
    (**g_746) = &l_358;
    return (*l_53);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int8_t  func_40(int32_t * p_41, int32_t  p_42, uint32_t  p_43, int32_t  p_44)
{ /* block id: 108 */
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_94 g_68 g_96 g_24 g_123 g_106 g_141 g_150 g_165 g_156 g_137 g_190 g_187 g_65 g_66 g_192 g_193 g_236 g_239 g_243 g_77 g_286 g_240 g_285
 * writes: g_68 g_106 g_96 g_77 g_123 g_137 g_141 g_150 g_165 g_187 g_66 g_193 g_237 g_240 g_244 g_259 g_286 g_63 g_306 g_309
 */
static uint64_t  func_49(int32_t * p_50, int64_t  p_51, int16_t  p_52)
{ /* block id: 13 */
    int8_t l_79 = 0x2FL;
    int32_t *l_80 = &g_68;
    int8_t **l_113 = (void*)0;
    int32_t l_168 = 0xC7B641BAL;
    int32_t l_201[10][1][4] = {{{1L,0xBCEE7636L,0x4DCC5185L,0xBCEE7636L}},{{0xBCEE7636L,8L,(-9L),0xA9D02FAEL}},{{0x02268D07L,1L,0xAA4C988BL,(-9L)}},{{(-3L),(-1L),0xF2371A48L,0xBCEE7636L}},{{(-3L),0xAA4C988BL,0xAA4C988BL,(-3L)}},{{0x02268D07L,0xBCEE7636L,(-9L),0xAA4C988BL}},{{0xBCEE7636L,(-1L),0x4DCC5185L,0xA9D02FAEL}},{{1L,0x02268D07L,0xAA4C988BL,0xA9D02FAEL}},{{(-1L),(-1L),(-1L),0xAA4C988BL}},{{(-3L),0xBCEE7636L,0L,(-3L)}}};
    const uint16_t *l_242 = &g_165;
    const uint16_t **l_241 = &l_242;
    float **l_276 = (void*)0;
    int32_t l_284 = 0xA697CB47L;
    uint32_t l_305 = 0x8B896CBFL;
    int32_t l_315[9][9] = {{(-1L),0xE9EABD20L,1L,(-6L),1L,0xE9EABD20L,(-1L),0xE9EABD20L,1L},{0x60064F0CL,0xD5E205D0L,0xD5E205D0L,0x60064F0CL,0xEB7C2CE1L,0xD5E205D0L,4L,0xEB7C2CE1L,0xEB7C2CE1L},{1L,0xE9EABD20L,(-3L),(-6L),(-3L),0xE9EABD20L,1L,0xE9EABD20L,(-3L)},{0x60064F0CL,0xEB7C2CE1L,0xD5E205D0L,4L,0xEB7C2CE1L,0xEB7C2CE1L,4L,0xD5E205D0L,0xEB7C2CE1L},{(-1L),0xE9EABD20L,1L,(-6L),1L,0xE9EABD20L,(-1L),0xE9EABD20L,1L},{0x60064F0CL,0xD5E205D0L,0xD5E205D0L,0x60064F0CL,0xEB7C2CE1L,0xD5E205D0L,4L,0xEB7C2CE1L,0xEB7C2CE1L},{1L,0xE9EABD20L,(-3L),(-6L),(-3L),0xE9EABD20L,1L,0xE9EABD20L,(-3L)},{0x60064F0CL,0xEB7C2CE1L,0xD5E205D0L,4L,0xEB7C2CE1L,0xEB7C2CE1L,4L,0xD5E205D0L,0xEB7C2CE1L},{(-1L),0xE9EABD20L,1L,(-6L),1L,0xE9EABD20L,(-1L),0xE9EABD20L,1L}};
    int32_t *l_316 = (void*)0;
    int32_t *l_317 = (void*)0;
    int32_t *l_318 = &l_201[1][0][1];
    int32_t *l_319 = &g_187;
    int32_t *l_320 = &g_187;
    int32_t *l_321 = &g_187;
    int32_t *l_322 = &l_201[1][0][3];
    int32_t *l_323 = &g_68;
    int32_t *l_324[10][9] = {{&g_32,&l_201[9][0][0],&l_201[9][0][0],&g_32,&g_68,(void*)0,(void*)0,&g_68,&g_32},{&g_187,&g_32,&g_187,(void*)0,&g_187,&g_32,&g_187,(void*)0,&g_187},{&g_32,&g_32,(void*)0,&l_201[9][0][0],&g_68,&g_68,&l_201[9][0][0],(void*)0,&g_32},{&g_187,(void*)0,&l_201[4][0][0],(void*)0,&g_187,(void*)0,&l_201[4][0][0],(void*)0,&g_187},{&g_68,&l_201[9][0][0],(void*)0,&g_32,&g_32,(void*)0,&l_201[9][0][0],&g_68,&g_68},{&g_187,(void*)0,&g_187,&g_32,&g_187,(void*)0,&g_187,&g_32,&g_187},{&g_68,&g_32,&l_201[9][0][0],&l_201[9][0][0],&g_32,&g_68,(void*)0,(void*)0,&g_68},{&g_187,&g_32,&l_201[4][0][0],&g_32,&g_187,&g_32,&l_201[4][0][0],&g_32,&g_187},{&g_32,&l_201[9][0][0],&l_201[9][0][0],&g_32,&g_68,(void*)0,(void*)0,&g_68,&g_32},{&g_187,&g_32,&g_187,(void*)0,&g_187,&g_32,&g_187,(void*)0,&g_187}};
    uint16_t l_325 = 0xBE1EL;
    uint32_t *l_330 = &g_141;
    uint32_t **l_329 = &l_330;
    uint32_t ***l_328 = &l_329;
    uint8_t l_331 = 5UL;
    int i, j, k;
    if (((*l_80) = l_79))
    { /* block id: 15 */
        uint32_t l_85 = 18446744073709551606UL;
        int8_t *l_87 = (void*)0;
        int8_t **l_86 = &l_87;
        uint8_t *l_95[4][3][10] = {{{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96}},{{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96}},{{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96}},{{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96}}};
        int32_t l_97 = 0L;
        const uint8_t *l_155 = &g_156;
        uint16_t l_180 = 1UL;
        uint64_t *l_181 = &g_137;
        uint32_t l_185 = 0xEBDF0E56L;
        int32_t l_220 = 1L;
        int16_t l_232[5];
        float l_273[3];
        float *l_275 = (void*)0;
        float **l_274 = &l_275;
        int32_t *l_277 = &l_97;
        int32_t *l_278 = (void*)0;
        int32_t *l_279 = &l_97;
        int32_t *l_280 = &g_187;
        int32_t *l_281 = &g_68;
        int32_t *l_282[2][1][2];
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_232[i] = 9L;
        for (i = 0; i < 3; i++)
            l_273[i] = 0x6.7AAC25p+50;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 2; k++)
                    l_282[i][j][k] = &l_201[4][0][2];
            }
        }
        if ((safe_sub_func_uint8_t_u_u(g_32, (l_97 = ((safe_mul_func_int16_t_s_s(0xB2EDL, ((p_51 ^ (l_85 , ((0xD8E7DFBFL > ((((((*l_86) = &g_77) != &l_79) && (((safe_lshift_func_int16_t_s_s((safe_mod_func_int8_t_s_s((&p_50 == g_94), l_85)), 11)) , 0UL) == 0UL)) , (void*)0) != l_86)) && p_52))) & 0x251BL))) && 0xE9L)))))
        { /* block id: 18 */
            uint32_t l_130 = 0UL;
            int16_t l_135 = 0x7503L;
            float *l_200 = &g_63;
            int32_t l_222 = (-1L);
            int32_t l_225[4] = {0L,0L,0L,0L};
            uint32_t l_227 = 4294967295UL;
            int i;
            if ((g_68 < (safe_mod_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s(((*l_80) < (*l_80)), 5)), (g_96 | 1UL)))))
            { /* block id: 19 */
                uint8_t *l_105 = &g_96;
                int32_t l_117 = (-4L);
                int16_t *l_122 = &g_123[0];
                uint64_t *l_136 = &g_137;
                int32_t l_138 = 0x0E5BE992L;
                uint32_t *l_139 = (void*)0;
                uint32_t *l_140 = &g_141;
                uint16_t *l_149 = &g_150;
                uint16_t *l_164[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t *l_191 = &g_68;
                int32_t l_221 = 1L;
                int32_t l_223 = 1L;
                int32_t l_224 = (-1L);
                int i;
                (*l_80) = (safe_mul_func_uint8_t_u_u((((((~((((((*l_105) |= ((g_106 = l_105) != (void*)0)) != (safe_sub_func_int64_t_s_s((safe_mod_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((((void*)0 == l_113) == (g_77 = (~(safe_div_func_uint64_t_u_u(g_32, ((*l_80) , l_117)))))), 9L)), (((*l_122) = (safe_mul_func_uint8_t_u_u((((safe_rshift_func_int16_t_s_u(g_24, g_32)) , l_117) && l_117), g_68))) | g_32))), 0x9FCDB2BF7FA13A89LL))) > l_117) <= p_51) , (-1L))) == l_85) <= l_97) < l_117) ^ (*p_50)), 0xA1L));
                if (l_85)
                    goto lbl_142;
lbl_142:
                (*l_80) &= (((*l_140) |= (((safe_rshift_func_uint16_t_u_s(65535UL, (((((safe_lshift_func_int8_t_s_u((((p_52 || (safe_rshift_func_int8_t_s_u((g_123[0] , l_130), (p_51 ^ (g_123[0] != ((*l_136) = (((safe_mul_func_uint8_t_u_u((*g_106), (safe_mod_func_int32_t_s_s(l_135, (-1L))))) | 1L) , 0UL))))))) == g_96) | g_96), l_117)) != (*p_50)) , (-4L)) && p_51) ^ (-1L)))) >= l_138) ^ g_24)) && p_51);
                if ((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_s(((*l_149)--), (0UL || (*p_50)))) != (safe_div_func_uint64_t_u_u((((((((0xE16EA25DA633836DLL & p_52) | ((*g_106) |= (((((l_155 = &g_96) != (void*)0) , (safe_rshift_func_int8_t_s_u((safe_mod_func_uint64_t_u_u((!(safe_lshift_func_int16_t_s_u(p_51, 14))), ((--g_165) , g_156))), 4))) ^ ((void*)0 != (*l_86))) && p_51))) , l_135) , l_135) && p_52) , 18446744073709551613UL) ^ p_52), l_168))), l_135)), g_137)))
                { /* block id: 33 */
                    uint8_t l_175[1][10] = {{0x2BL,0x2BL,0x78L,0x2BL,0x2BL,0x78L,0x2BL,0x2BL,0x78L,0x2BL}};
                    int32_t l_184 = 0L;
                    int32_t *l_189 = (void*)0;
                    int i, j;
                    (*g_190) |= ((p_51 , p_51) , (((((l_185 = ((safe_mod_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s((l_117 = (safe_rshift_func_int16_t_s_s(l_175[0][0], (safe_mod_func_int8_t_s_s((safe_mod_func_int64_t_s_s((&g_137 == (l_180 , ((((void*)0 == l_181) == ((safe_add_func_uint16_t_u_u(g_141, ((((l_184 != 1UL) || g_137) || g_137) != 4294967295UL))) , 1L)) , (void*)0))), g_156)), 1UL))))), 5)), 1L)) > (*p_50))) , 0x84L) > (*g_106)) , g_24) || g_68));
                    (*g_192) = ((*g_65) = (l_191 = (*g_65)));
                }
                else
                { /* block id: 40 */
                    int32_t l_226 = 0xC9A1AEF9L;
                    int32_t l_230 = 0x1ED449BAL;
                    int32_t l_231[5] = {0x896E0AB1L,0x896E0AB1L,0x896E0AB1L,0x896E0AB1L,0x896E0AB1L};
                    uint8_t l_233 = 0xE7L;
                    int64_t *l_257 = (void*)0;
                    int64_t *l_258 = &g_259;
                    int i;
                    if ((safe_sub_func_uint8_t_u_u((((0xF1BF9891L | (((safe_sub_func_int64_t_s_s(((void*)0 != l_200), 1L)) , (*g_193)) < g_150)) < ((*l_140) = (((((&g_165 != &g_150) , l_136) == (void*)0) , (*l_80)) ^ l_85))) > p_52), 255UL)))
                    { /* block id: 42 */
                        return p_52;
                    }
                    else
                    { /* block id: 44 */
                        int32_t *l_202 = &g_187;
                        int32_t *l_203 = (void*)0;
                        int32_t *l_204 = (void*)0;
                        int32_t *l_205 = &l_201[6][0][0];
                        int32_t *l_206 = &g_68;
                        int32_t *l_207 = &l_117;
                        int32_t *l_208 = &l_117;
                        int32_t *l_209 = &g_187;
                        int32_t *l_210 = &g_187;
                        int32_t *l_211 = &l_201[1][0][3];
                        int32_t *l_212 = &g_68;
                        int32_t l_213 = 0x35FA0D3BL;
                        int32_t *l_214 = (void*)0;
                        int32_t *l_215 = (void*)0;
                        int32_t *l_216 = &l_201[1][0][3];
                        int32_t *l_217 = &l_97;
                        int32_t *l_218 = &l_201[1][0][3];
                        int32_t *l_219[1][10][8] = {{{&g_68,&g_68,&g_187,&g_187,&g_187,&g_68,&g_187,&g_187},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187},{&g_187,&g_187,&g_68,&g_187,&g_187,&g_68,&g_187,&g_187},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187},{&g_187,&g_187,&g_68,&g_187,&g_187,&g_68,&g_187,&g_187},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187},{&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187,&g_187},{&g_187,&g_187,&g_68,&g_187,&g_187,&g_68,&g_187,&g_187}}};
                        int i, j, k;
                        l_227++;
                        --l_233;
                    }
                    (*g_236) = &l_220;
                    if ((*l_191))
                    { /* block id: 49 */
                        (*g_239) = (g_237 = (*g_192));
                    }
                    else
                    { /* block id: 52 */
                        int32_t **l_246 = &l_80;
                        (*g_243) = l_241;
                        (*l_246) = (p_51 , (void*)0);
                        (*l_191) |= ((*p_50) >= ((((*l_149)++) <= (g_165 = 0x2F18L)) >= p_51));
                    }
                    (*l_191) = (0x0F09L & (safe_sub_func_int64_t_s_s(p_51, ((safe_mul_func_int8_t_s_s(((*l_87) ^= ((safe_mod_func_int16_t_s_s(g_156, (safe_div_func_uint64_t_u_u(((((*g_66) = (g_165 < ((*l_258) = l_220))) <= (safe_add_func_uint32_t_u_u(((&g_123[0] != &l_135) == 0L), (*p_50)))) <= (*l_191)), g_123[0])))) && p_51)), 246UL)) <= 1L))));
                }
            }
            else
            { /* block id: 64 */
                uint32_t *l_271[1][8][3] = {{{&l_130,&l_130,&l_130},{&g_141,&l_227,&g_141},{&l_130,&l_130,&l_130},{&g_141,&l_227,&g_141},{&l_130,&l_130,&l_130},{&g_141,&l_227,&g_141},{&l_130,&l_130,&l_130},{&g_141,&l_227,&g_141}}};
                int i, j, k;
                for (l_85 = 0; (l_85 <= 3); l_85 += 1)
                { /* block id: 67 */
                    float l_262 = 0x4.Bp-1;
                    int32_t l_265 = 0x22D0BAB9L;
                    for (l_222 = 0; (l_222 >= 0); l_222 -= 1)
                    { /* block id: 70 */
                        int32_t *l_263 = &g_68;
                        int32_t *l_264[5] = {&l_225[(l_222 + 1)],&l_225[(l_222 + 1)],&l_225[(l_222 + 1)],&l_225[(l_222 + 1)],&l_225[(l_222 + 1)]};
                        uint16_t l_266 = 0x676AL;
                        int i;
                        l_266++;
                    }
                    for (l_180 = 0; (l_180 <= 0); l_180 += 1)
                    { /* block id: 75 */
                        uint32_t *l_270 = &g_141;
                        uint32_t **l_269 = &l_270;
                        uint32_t **l_272 = &l_271[0][4][0];
                        int i;
                        (*l_80) = l_225[(l_180 + 1)];
                        (*g_193) ^= (((*l_269) = (void*)0) != ((*l_272) = l_271[0][4][0]));
                    }
                    for (g_187 = 0; (g_187 <= 0); g_187 += 1)
                    { /* block id: 83 */
                        int i;
                        return l_225[g_187];
                    }
                    if ((*g_190))
                        break;
                }
            }
        }
        else
        { /* block id: 89 */
            for (l_85 = 0; (l_85 <= 0); l_85 += 1)
            { /* block id: 92 */
                l_276 = l_274;
            }
        }
        g_286--;
    }
    else
    { /* block id: 97 */
        float *l_293 = &g_63;
        uint16_t *l_298 = &g_165;
        int32_t l_304 = 0x36901CA6L;
        uint16_t ***l_307 = (void*)0;
        float *l_308 = &g_309;
        int32_t **l_310 = &g_66;
        (*l_310) = ((safe_mul_func_float_f_f((safe_sub_func_float_f_f((p_52 , (((*l_293) = (-0x1.4p+1)) < (p_51 >= (safe_sub_func_float_f_f(((*l_308) = ((((g_306 = ((((((safe_mod_func_uint32_t_u_u(((((*l_80) ^ p_51) ^ 6UL) | ((l_298 != ((safe_lshift_func_int8_t_s_s((((safe_unary_minus_func_int8_t_s(((safe_unary_minus_func_uint64_t_u(((((*g_106) ^= 0x8BL) < (~(l_242 == &g_123[0]))) > (*g_240)))) || l_304))) || (-10L)) , 0L), 7)) , (*l_241))) <= (-4L))), g_137)) >= g_165) > (*l_80)) > l_305) , g_285[1]) <= (-0x1.4p+1))) , (void*)0) != l_307) == 0x1.Fp+1)), p_52))))), g_68)), l_304)) , p_50);
        (*l_80) ^= (safe_add_func_uint64_t_u_u((safe_div_func_uint16_t_u_u(0x7A46L, l_315[7][1])), p_51));
    }
    ++l_325;
    (*l_328) = (void*)0;
    return l_331;
}


/* ------------------------------------------ */
/* 
 * reads : g_65 g_32
 * writes: g_63 g_66
 */
static int64_t  func_56(int16_t  p_57, int8_t * p_58, uint32_t  p_59)
{ /* block id: 3 */
    int32_t *l_64 = (void*)0;
    int32_t *l_67 = &g_68;
    int32_t *l_69 = (void*)0;
    int32_t *l_70 = &g_68;
    int32_t *l_71 = &g_68;
    int32_t *l_72[9] = {&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32,&g_32};
    uint8_t l_73 = 0x5DL;
    int i;
    for (p_57 = (-19); (p_57 != (-20)); --p_57)
    { /* block id: 6 */
        float *l_62[1];
        int i;
        for (i = 0; i < 1; i++)
            l_62[i] = &g_63;
        g_63 = 0x0.7B364Fp-75;
        (*g_65) = l_64;
    }
    ++l_73;
    return g_32;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_24, "g_24", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_48, "g_48", print_hash_value);
    transparent_crc_bytes (&g_63, sizeof(g_63), "g_63", print_hash_value);
    transparent_crc(g_68, "g_68", print_hash_value);
    transparent_crc(g_77, "g_77", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_123[i], "g_123[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_137, "g_137", print_hash_value);
    transparent_crc(g_141, "g_141", print_hash_value);
    transparent_crc(g_150, "g_150", print_hash_value);
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc(g_165, "g_165", print_hash_value);
    transparent_crc(g_187, "g_187", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    transparent_crc(g_259, "g_259", print_hash_value);
    transparent_crc(g_283, "g_283", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_285[i], "g_285[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_286, "g_286", print_hash_value);
    transparent_crc(g_306, "g_306", print_hash_value);
    transparent_crc_bytes (&g_309, sizeof(g_309), "g_309", print_hash_value);
    transparent_crc(g_511, "g_511", print_hash_value);
    transparent_crc(g_513, "g_513", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_524[i], "g_524[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_606[i], "g_606[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_690, "g_690", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_780[i], "g_780[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_841, "g_841", print_hash_value);
    transparent_crc(g_856, "g_856", print_hash_value);
    transparent_crc(g_877, "g_877", print_hash_value);
    transparent_crc(g_880, "g_880", print_hash_value);
    transparent_crc(g_995, "g_995", print_hash_value);
    transparent_crc_bytes (&g_1051, sizeof(g_1051), "g_1051", print_hash_value);
    transparent_crc(g_1140, "g_1140", print_hash_value);
    transparent_crc(g_1239, "g_1239", print_hash_value);
    transparent_crc_bytes (&g_1331, sizeof(g_1331), "g_1331", print_hash_value);
    transparent_crc(g_1362, "g_1362", print_hash_value);
    transparent_crc(g_1437, "g_1437", print_hash_value);
    transparent_crc(g_1500, "g_1500", print_hash_value);
    transparent_crc(g_1689, "g_1689", print_hash_value);
    transparent_crc(g_1767, "g_1767", print_hash_value);
    transparent_crc(g_1791, "g_1791", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_2324[i][j][k], "g_2324[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2356[i], "g_2356[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2563, "g_2563", print_hash_value);
    transparent_crc(g_3034, "g_3034", print_hash_value);
    transparent_crc(g_3134, "g_3134", print_hash_value);
    transparent_crc(g_3322, "g_3322", print_hash_value);
    transparent_crc(g_3358, "g_3358", print_hash_value);
    transparent_crc(g_3380, "g_3380", print_hash_value);
    transparent_crc(g_3532, "g_3532", print_hash_value);
    transparent_crc(g_3536, "g_3536", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 913
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 365
   depth: 2, occurrence: 104
   depth: 3, occurrence: 7
   depth: 4, occurrence: 7
   depth: 5, occurrence: 1
   depth: 6, occurrence: 4
   depth: 7, occurrence: 5
   depth: 9, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 3
   depth: 15, occurrence: 4
   depth: 16, occurrence: 1
   depth: 17, occurrence: 6
   depth: 18, occurrence: 2
   depth: 19, occurrence: 7
   depth: 20, occurrence: 5
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 6
   depth: 24, occurrence: 4
   depth: 25, occurrence: 5
   depth: 26, occurrence: 2
   depth: 27, occurrence: 5
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 2
   depth: 38, occurrence: 1
   depth: 41, occurrence: 3
   depth: 45, occurrence: 2
   depth: 47, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 749

XXX times a variable address is taken: 1652
XXX times a pointer is dereferenced on RHS: 552
breakdown:
   depth: 1, occurrence: 415
   depth: 2, occurrence: 120
   depth: 3, occurrence: 17
XXX times a pointer is dereferenced on LHS: 450
breakdown:
   depth: 1, occurrence: 373
   depth: 2, occurrence: 61
   depth: 3, occurrence: 15
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 59
XXX times a pointer is compared with address of another variable: 25
XXX times a pointer is compared with another pointer: 21
XXX times a pointer is qualified to be dereferenced: 12693

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1468
   level: 2, occurrence: 687
   level: 3, occurrence: 346
   level: 4, occurrence: 228
   level: 5, occurrence: 40
XXX number of pointers point to pointers: 373
XXX number of pointers point to scalars: 376
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.7
XXX average alias set size: 1.45

XXX times a non-volatile is read: 3005
XXX times a non-volatile is write: 1387
XXX times a volatile is read: 119
XXX    times read thru a pointer: 26
XXX times a volatile is write: 89
XXX    times written thru a pointer: 40
XXX times a volatile is available for access: 2.53e+03
XXX percentage of non-volatile access: 95.5

XXX forward jumps: 2
XXX backward jumps: 9

XXX stmts: 377
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 42
   depth: 2, occurrence: 50
   depth: 3, occurrence: 65
   depth: 4, occurrence: 91
   depth: 5, occurrence: 94

XXX percentage a fresh-made variable is used: 15.8
XXX percentage an existing variable is used: 84.2
********************* end of statistics **********************/


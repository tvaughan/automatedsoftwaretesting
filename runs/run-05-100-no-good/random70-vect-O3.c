/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3042467040
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int64_t g_2[10][4] = {{0xB8FAAAEB9AE77AB3LL,0xE1265AB3E90A624CLL,0xE1265AB3E90A624CLL,0xB8FAAAEB9AE77AB3LL},{0xE1265AB3E90A624CLL,0xB8FAAAEB9AE77AB3LL,0xBCA63185299DADA4LL,1L},{0xE1265AB3E90A624CLL,0xBCA63185299DADA4LL,0xE1265AB3E90A624CLL,(-9L)},{0xB8FAAAEB9AE77AB3LL,1L,(-9L),(-9L)},{0xBCA63185299DADA4LL,0xBCA63185299DADA4LL,0L,1L},{1L,0xB8FAAAEB9AE77AB3LL,0L,0xB8FAAAEB9AE77AB3LL},{0xBCA63185299DADA4LL,0xE1265AB3E90A624CLL,(-9L),0L},{0xB8FAAAEB9AE77AB3LL,0xE1265AB3E90A624CLL,0xE1265AB3E90A624CLL,0xB8FAAAEB9AE77AB3LL},{0xE1265AB3E90A624CLL,0xB8FAAAEB9AE77AB3LL,0xBCA63185299DADA4LL,1L},{0xE1265AB3E90A624CLL,0xBCA63185299DADA4LL,0xE1265AB3E90A624CLL,(-9L)}};
static volatile int32_t g_3[5] = {0x04445559L,0x04445559L,0x04445559L,0x04445559L,0x04445559L};
static volatile int32_t g_4 = 1L;/* VOLATILE GLOBAL g_4 */
static int32_t g_5 = 1L;
static int32_t g_31 = (-4L);
static int32_t g_32 = 0xF0C0ED6DL;
static int32_t g_33 = 0x21779538L;
static uint32_t g_34[2] = {0xAA0BDEC7L,0xAA0BDEC7L};
static uint32_t g_41[7][6] = {{0x8462C7DCL,6UL,0x0EF155D2L,0x0EF155D2L,6UL,0x8462C7DCL},{0x08949E3BL,0x8462C7DCL,7UL,6UL,7UL,0x8462C7DCL},{7UL,0x08949E3BL,0x0EF155D2L,18446744073709551606UL,18446744073709551606UL,0x0EF155D2L},{7UL,7UL,18446744073709551606UL,6UL,0UL,6UL},{0x08949E3BL,7UL,0x08949E3BL,0x0EF155D2L,18446744073709551606UL,18446744073709551606UL},{0x8462C7DCL,0x08949E3BL,0x08949E3BL,0x8462C7DCL,7UL,6UL},{6UL,0x8462C7DCL,18446744073709551606UL,0x8462C7DCL,6UL,0x0EF155D2L}};
static uint32_t g_76 = 0xADD72F77L;
static uint32_t *g_75 = &g_76;
static uint8_t g_99 = 255UL;
static int32_t g_101 = 0x5CCA7CC9L;
static const uint32_t g_112 = 0xCABD61F3L;
static const uint32_t g_114[7][5] = {{4294967295UL,4294967289UL,4294967289UL,4294967295UL,4294967289UL},{0xA3617541L,0xA3617541L,0x0EB54A4BL,0xA3617541L,0xA3617541L},{4294967289UL,4294967295UL,4294967289UL,4294967289UL,4294967295UL},{0xA3617541L,0xE832032BL,0xE832032BL,0xA3617541L,0xE832032BL},{4294967295UL,4294967295UL,4294967288UL,4294967295UL,4294967295UL},{0xE832032BL,0xA3617541L,0xE832032BL,0xE832032BL,0xA3617541L},{4294967295UL,4294967289UL,4294967289UL,4294967295UL,4294967289UL}};
static int32_t * const g_126[2] = {(void*)0,(void*)0};
static int32_t * const * volatile g_125 = &g_126[1];/* VOLATILE GLOBAL g_125 */
static int32_t * const * volatile *g_124 = &g_125;
static uint16_t g_150 = 0xF219L;
static uint32_t g_152 = 0x24EADEB1L;
static int8_t g_161 = 0x18L;
static uint64_t g_164[1][3][7] = {{{8UL,5UL,8UL,18446744073709551615UL,5UL,0xE7FF7C57EFDBF381LL,0xE7FF7C57EFDBF381LL},{5UL,0x21B4F9A3E397D913LL,7UL,0x21B4F9A3E397D913LL,5UL,7UL,0x166068C90CB26268LL},{0x166068C90CB26268LL,0xE7FF7C57EFDBF381LL,18446744073709551615UL,0x166068C90CB26268LL,18446744073709551615UL,0xE7FF7C57EFDBF381LL,0x166068C90CB26268LL}}};
static uint64_t g_182 = 0x021B652CE760C5D1LL;
static uint64_t g_205 = 18446744073709551615UL;
static uint32_t g_222 = 0x714A00EAL;
static int32_t **g_272 = (void*)0;
static int32_t g_289 = 0xA8D414C4L;
static int64_t g_290[9] = {0x33A8C0956980EBA0LL,0xEA8EFB1D9A2A01E9LL,0x33A8C0956980EBA0LL,0x33A8C0956980EBA0LL,0xEA8EFB1D9A2A01E9LL,0x33A8C0956980EBA0LL,0x33A8C0956980EBA0LL,0xEA8EFB1D9A2A01E9LL,0x33A8C0956980EBA0LL};
static const int32_t g_301[7] = {0x94D7DD97L,0x94D7DD97L,0x94D7DD97L,0x94D7DD97L,0x94D7DD97L,0x94D7DD97L,0x94D7DD97L};
static uint8_t g_324[3] = {0x4FL,0x4FL,0x4FL};
static uint32_t *g_352[3] = {(void*)0,(void*)0,(void*)0};
static uint8_t g_355 = 0x54L;
static uint16_t g_358 = 4UL;
static uint64_t g_359 = 0UL;
static uint16_t g_360 = 0xC8B6L;
static int8_t g_365 = 0x18L;
static volatile uint64_t g_377[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static volatile uint64_t *g_376 = &g_377[6];
static volatile uint64_t **g_375 = &g_376;
static volatile uint64_t ** const  volatile *g_374 = &g_375;
static uint8_t *g_461 = &g_355;
static uint32_t g_496[10] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static float g_513 = 0x5.Dp-1;
static uint16_t g_515 = 1UL;
static int32_t g_521[10] = {0x8A11640FL,1L,1L,0x8A11640FL,0x7E989F72L,0x8A11640FL,1L,1L,0x8A11640FL,0x7E989F72L};
static uint8_t **g_567 = &g_461;
static int16_t g_596[10] = {1L,0L,0L,1L,0L,0L,1L,0L,0L,1L};
static int8_t g_615 = 7L;
static int64_t * const  volatile * volatile * volatile *g_645 = (void*)0;
static const int64_t *g_729 = (void*)0;
static const int64_t * volatile *g_728 = &g_729;
static int32_t *g_850 = &g_33;
static volatile int32_t g_919 = 0x4C7371FDL;/* VOLATILE GLOBAL g_919 */
static volatile int32_t *g_918 = &g_919;
static int8_t * volatile g_943[10] = {&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161};
static int8_t * volatile *g_942 = &g_943[4];
static int64_t g_991 = 0L;
static uint64_t *g_1012 = (void*)0;
static uint64_t **g_1011 = &g_1012;
static int8_t *g_1017 = &g_161;
static const int32_t *g_1048 = &g_289;
static uint32_t g_1051[6][3][2] = {{{0xE0681274L,0x48CC6800L},{0xF5F5850EL,0x51822D2DL},{0x51822D2DL,0xC91DACB3L}},{{0xE0681274L,9UL},{0xC91DACB3L,9UL},{0xE0681274L,0xC91DACB3L}},{{0x51822D2DL,0x51822D2DL},{0xF5F5850EL,0x48CC6800L},{0xE0681274L,1UL}},{{0x48CC6800L,9UL},{0xB54F6252L,0x180E33EFL},{0xC91DACB3L,0x1E5C144BL}},{{0xC91DACB3L,0x180E33EFL},{0xB54F6252L,0xF5F5850EL},{0x180E33EFL,4294967292UL}},{{0xFB39118CL,0x180E33EFL},{0x1E5C144BL,0xC91DACB3L},{0xC91DACB3L,0UL}}};
static int8_t g_1053 = (-10L);
static int16_t g_1114 = 6L;
static volatile uint16_t **g_1167 = (void*)0;
static volatile uint16_t ***g_1166[9] = {&g_1167,&g_1167,&g_1167,&g_1167,&g_1167,&g_1167,&g_1167,&g_1167,&g_1167};
static uint8_t g_1236 = 1UL;
static int8_t g_1237 = (-1L);
static int8_t **g_1241 = &g_1017;
static float g_1298 = 0x0.Dp-1;
static uint64_t g_1352 = 0xB29EB1C4B671243BLL;
static volatile float * volatile **g_1388 = (void*)0;
static int32_t ***g_1411[9][1][9] = {{{&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272}},{{&g_272,(void*)0,&g_272,(void*)0,&g_272,&g_272,(void*)0,&g_272,(void*)0}},{{&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272}},{{&g_272,&g_272,(void*)0,&g_272,(void*)0,&g_272,&g_272,(void*)0,&g_272}},{{&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272}},{{(void*)0,(void*)0,&g_272,&g_272,(void*)0,(void*)0,(void*)0,&g_272,&g_272}},{{&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272}}};
static int32_t ****g_1410 = &g_1411[0][0][6];
static int16_t g_1471 = 0x80D5L;
static uint64_t *** const **g_1567 = (void*)0;
static const int16_t g_1611 = (-1L);
static int64_t g_1626 = (-1L);
static uint32_t g_1674 = 0x79C12646L;
static uint32_t **g_1711 = (void*)0;
static uint32_t *** const g_1710 = &g_1711;
static uint64_t ***g_1765 = &g_1011;
static uint64_t ****g_1764 = &g_1765;
static int64_t g_1887 = 9L;
static float *g_1915[8][3][5] = {{{(void*)0,(void*)0,(void*)0,&g_513,&g_513},{&g_1298,&g_513,&g_1298,&g_513,&g_513},{&g_513,&g_513,&g_513,(void*)0,&g_513}},{{&g_513,&g_513,(void*)0,&g_1298,&g_1298},{&g_513,(void*)0,(void*)0,&g_513,&g_1298},{(void*)0,&g_1298,&g_1298,(void*)0,(void*)0}},{{&g_513,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1298,&g_1298,&g_513,(void*)0,&g_513},{(void*)0,(void*)0,&g_1298,(void*)0,&g_513}},{{&g_1298,(void*)0,(void*)0,(void*)0,&g_1298},{&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,(void*)0,(void*)0,&g_1298,&g_513}},{{&g_513,&g_513,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1298,(void*)0,&g_513,(void*)0},{&g_513,(void*)0,&g_1298,&g_513,&g_513}},{{&g_513,(void*)0,(void*)0,&g_1298,&g_1298},{&g_513,&g_1298,&g_513,&g_513,(void*)0},{(void*)0,(void*)0,&g_513,&g_1298,&g_513}},{{&g_513,&g_513,&g_1298,&g_513,&g_513},{(void*)0,(void*)0,&g_513,&g_513,(void*)0},{&g_513,(void*)0,&g_513,&g_1298,(void*)0}},{{&g_1298,&g_1298,(void*)0,&g_1298,&g_1298},{&g_1298,&g_513,(void*)0,(void*)0,&g_513},{(void*)0,(void*)0,(void*)0,(void*)0,&g_513}}};
static float **g_1914 = &g_1915[3][2][0];
static int64_t g_1995 = 1L;
static int16_t * volatile * volatile g_2041 = (void*)0;/* VOLATILE GLOBAL g_2041 */
static uint16_t **g_2081 = (void*)0;
static uint16_t ***g_2080 = &g_2081;
static uint32_t g_2090 = 1UL;
static uint64_t g_2129[5] = {0x69595580E0196FB8LL,0x69595580E0196FB8LL,0x69595580E0196FB8LL,0x69595580E0196FB8LL,0x69595580E0196FB8LL};
static const int64_t g_2134 = 0xC578E9210832D37ELL;
static int32_t ** volatile g_2155 = (void*)0;/* VOLATILE GLOBAL g_2155 */
static int32_t ** volatile g_2156 = &g_850;/* VOLATILE GLOBAL g_2156 */
static volatile uint32_t g_2177 = 0xE897DC10L;/* VOLATILE GLOBAL g_2177 */
static uint32_t g_2242 = 3UL;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int8_t  func_19(const uint64_t  p_20, uint32_t  p_21, float  p_22, uint32_t  p_23);
static uint32_t  func_24(uint16_t  p_25, float  p_26, int32_t  p_27);
static uint8_t  func_49(const uint32_t * p_50, int16_t  p_51, int32_t  p_52);
static const uint32_t * func_53(float  p_54, const int32_t  p_55);
static float  func_56(const uint16_t  p_57, float  p_58);
static float  func_59(uint32_t * p_60, uint16_t  p_61, uint32_t  p_62, int8_t  p_63);
static uint32_t * func_64(int32_t  p_65, int32_t  p_66);
static const int32_t  func_72(uint32_t * p_73, uint32_t  p_74);
static uint32_t  func_77(uint32_t * p_78, const uint8_t  p_79, uint32_t * p_80);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_515 g_164 g_567 g_461 g_355 g_33 g_324 g_1241 g_1017 g_161 g_1995 g_918 g_919 g_2129
 * writes: g_5 g_991 g_596 g_515 g_164 g_324 g_919
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint16_t l_14 = 0x6339L;
    int32_t l_1845 = 6L;
    int32_t l_1847 = 0xD6EF648FL;
    int32_t l_1853 = 0x8ACF46ADL;
    const int32_t l_1907 = 0x09526547L;
    const uint64_t l_1997 = 1UL;
    uint64_t l_2020 = 0x8547B824460BE89DLL;
    int8_t l_2062 = (-4L);
    uint16_t *l_2078 = &g_515;
    uint16_t **l_2077 = &l_2078;
    uint16_t ***l_2076 = &l_2077;
    uint16_t l_2108 = 1UL;
    uint32_t *l_2183 = &g_41[0][5];
    uint16_t l_2186 = 65527UL;
    uint64_t *****l_2245[2][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1764,&g_1764,&g_1764,&g_1764,&g_1764,&g_1764,&g_1764}};
    int16_t *l_2253 = &g_596[3];
    uint8_t *l_2264 = &g_324[0];
    int i, j;
lbl_2265:
    for (g_5 = 0; (g_5 <= 3); g_5 += 1)
    { /* block id: 3 */
        float l_12[5] = {0x9.77A124p+96,0x9.77A124p+96,0x9.77A124p+96,0x9.77A124p+96,0x9.77A124p+96};
        int32_t l_13[8][2][8] = {{{0x55FFF839L,1L,0x99649744L,0x99649744L,1L,0x55FFF839L,0L,0x1EA0627EL},{0xB91EE4CFL,0x15CC7C44L,1L,0L,0x1EA0627EL,(-1L),0x4B11F381L,0x9040C3F3L}},{{0xF5452FA8L,0x8AAE07EAL,(-7L),0L,1L,(-2L),0x15CC7C44L,0x1EA0627EL},{9L,1L,1L,0x99649744L,(-4L),0x8AAE07EAL,0L,0xF5452FA8L}},{{0x90412894L,0xB91EE4CFL,(-1L),0x8AAE07EAL,2L,1L,2L,0x8AAE07EAL},{0xC5706B37L,0x1EA0627EL,0xC5706B37L,0x0A7C6DD3L,0xB91EE4CFL,(-1L),9L,0xDAFFA898L}},{{(-7L),0x90412894L,0L,0xC5706B37L,0xAE974CA7L,0L,0xB91EE4CFL,(-2L)},{(-7L),0x4B11F381L,(-4L),9L,0xB91EE4CFL,7L,0x8BBE58EDL,0x9A130DFDL}},{{0xC5706B37L,0xBD2F23D3L,(-1L),(-1L),2L,9L,0x271DBCABL,1L},{0x90412894L,(-1L),1L,(-4L),(-4L),1L,(-1L),0x90412894L}},{{9L,0L,2L,0x1EA0627EL,1L,0x90412894L,1L,0x10DD013DL},{0xF5452FA8L,7L,0x10DD013DL,0x271DBCABL,0x1EA0627EL,0x90412894L,0xC5706B37L,0x55FFF839L}},{{0xB91EE4CFL,0L,0x9A130DFDL,7L,1L,1L,0xDAFFA898L,0L},{0x55FFF839L,(-1L),(-9L),0xFF26AF90L,(-2L),9L,0xBD2F23D3L,9L}},{{0L,0xBD2F23D3L,0x55FFF839L,0xBD2F23D3L,0L,7L,(-1L),(-7L)},{0x271DBCABL,0x4B11F381L,0xFF26AF90L,0x9040C3F3L,(-1L),0L,0x10DD013DL,0xBD2F23D3L}}};
        uint64_t ***l_1900 = &g_1011;
        float *l_1913 = (void*)0;
        float **l_1912 = &l_1913;
        int16_t l_1936 = 0x8574L;
        uint8_t l_1945 = 3UL;
        int32_t l_1996 = 0x81DC7DDDL;
        const int16_t l_2091 = 0xCB5BL;
        int64_t **l_2095[1];
        uint32_t l_2131[4];
        int32_t l_2133 = (-1L);
        uint32_t l_2184 = 4294967287UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2095[i] = (void*)0;
        for (i = 0; i < 4; i++)
            l_2131[i] = 0x56BB4381L;
    }
    (*g_918) |= (safe_add_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((safe_unary_minus_func_uint8_t_u((safe_mod_func_uint64_t_u_u(18446744073709551606UL, (g_991 = l_2020))))), (l_1853 , (((((*l_2253) = (l_2062 <= l_1853)) , 0x01L) == (safe_sub_func_int16_t_s_s((safe_add_func_uint16_t_u_u(((safe_sub_func_uint8_t_u_u(((*l_2264) |= ((((((*l_2078) &= (l_1997 <= 65535UL)) < (safe_lshift_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((g_164[0][1][5] &= 18446744073709551612UL), l_2062)), (**g_567)))) , l_1997) == g_33) || l_1845)), (**g_1241))) < g_1995), 0UL)), 0x6C4CL))) == l_1997)))) < 0x6CA36173L), l_2186));
    if (l_2062)
        goto lbl_2265;
    return g_2129[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_1236 g_1017 g_161
 * writes: g_1236
 */
static int8_t  func_19(const uint64_t  p_20, uint32_t  p_21, float  p_22, uint32_t  p_23)
{ /* block id: 800 */
    uint32_t l_1835 = 1UL;
    int32_t l_1839 = (-6L);
    int32_t l_1840[10] = {(-3L),0xC9F86C58L,(-3L),0xC9F86C58L,(-3L),0xC9F86C58L,(-3L),0xC9F86C58L,(-3L),0xC9F86C58L};
    int32_t l_1841 = 0x367560A0L;
    int i;
    for (g_1236 = 0; (g_1236 <= 2); g_1236++)
    { /* block id: 803 */
        int8_t l_1833 = (-1L);
        int32_t *l_1834[1][7] = {{&g_101,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101}};
        uint8_t l_1838 = 1UL;
        uint64_t l_1842[5] = {0x4C005E6F0E56A624LL,0x4C005E6F0E56A624LL,0x4C005E6F0E56A624LL,0x4C005E6F0E56A624LL,0x4C005E6F0E56A624LL};
        int i, j;
        l_1835++;
        if (l_1838)
            continue;
        l_1842[0]--;
    }
    return (*g_1017);
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_596 g_521 g_850 g_33 g_567 g_461 g_615 g_150 g_114 g_164 g_496 g_289 g_161 g_2 g_99 g_34 g_360 g_182 g_222 g_355 g_1017 g_1352 g_1048 g_290 g_1388 g_1051 g_1410 g_1053 g_1236 g_75 g_76 g_101 g_124 g_301 g_152 g_374 g_365 g_32 g_515 g_5 g_272 g_324 g_645 g_359 g_375 g_376 g_728 g_358 g_352 g_112 g_205 g_918 g_942 g_1114 g_1471 g_1241
 * writes: g_31 g_32 g_33 g_34 g_222 g_521 g_99 g_358 g_359 g_150 g_355 g_513 g_1236 g_1237 g_1241 g_290 g_991 g_76 g_596 g_161 g_850 g_1410 g_101 g_152 g_324 g_352 g_360 g_365 g_374 g_289 g_461 g_164 g_496 g_515 g_205 g_567 g_615 g_272 g_918 g_942 g_1114 g_75 g_1053
 */
static uint32_t  func_24(uint16_t  p_25, float  p_26, int32_t  p_27)
{ /* block id: 4 */
    uint64_t l_30[5][9] = {{1UL,0xD0BB265D63260039LL,6UL,0xB54C5F6B4AB19F88LL,7UL,0x35D7D231D5762CC6LL,0xCFB11589A458230BLL,0xCFB11589A458230BLL,0x35D7D231D5762CC6LL},{6UL,0xCFB11589A458230BLL,18446744073709551609UL,0xCFB11589A458230BLL,6UL,1UL,0x7DFE98D7E942FAFELL,18446744073709551615UL,18446744073709551615UL},{1UL,0xCFB11589A458230BLL,1UL,18446744073709551615UL,0xD0BB265D63260039LL,0UL,1UL,0UL,0xD0BB265D63260039LL},{0x7DFE98D7E942FAFELL,0xD0BB265D63260039LL,0xD0BB265D63260039LL,0x7DFE98D7E942FAFELL,0x35D7D231D5762CC6LL,1UL,18446744073709551609UL,1UL,0x67B11082E70D5DD2LL},{0x7DFE98D7E942FAFELL,18446744073709551615UL,0x1D55E0ECF58A4122LL,1UL,0x67B11082E70D5DD2LL,0x35D7D231D5762CC6LL,0x35D7D231D5762CC6LL,0x67B11082E70D5DD2LL,1UL}};
    uint16_t l_1027 = 65526UL;
    uint32_t *l_1050 = &g_1051[0][0][1];
    float l_1073 = 0x9.A13885p-90;
    int32_t *l_1115 = (void*)0;
    uint16_t l_1164 = 1UL;
    int32_t l_1187[5];
    int8_t **l_1239 = &g_1017;
    uint8_t ***l_1265 = (void*)0;
    int64_t l_1294 = 0x57D9EBB89269BEFALL;
    float *l_1366 = &g_513;
    float **l_1365 = &l_1366;
    int64_t l_1391 = 0xECC91B089F718FCALL;
    uint16_t l_1424 = 0x3075L;
    int64_t *l_1426 = &l_1391;
    uint8_t *l_1436 = &g_1236;
    int32_t **l_1439[6][8] = {{&g_850,&g_850,(void*)0,&g_850,&g_850,(void*)0,&g_850,&g_850},{&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850},{&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850},{&g_850,&g_850,(void*)0,&g_850,&g_850,(void*)0,&g_850,&g_850},{&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850},{&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850,&g_850}};
    int32_t *l_1440 = (void*)0;
    uint16_t *l_1525 = &g_360;
    uint64_t ***l_1574[6][4][9] = {{{&g_1011,(void*)0,&g_1011,(void*)0,(void*)0,&g_1011,&g_1011,(void*)0,(void*)0},{&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0},{&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0},{(void*)0,&g_1011,&g_1011,&g_1011,(void*)0,(void*)0,&g_1011,&g_1011,&g_1011}},{{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0,(void*)0,(void*)0},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0,(void*)0},{&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,&g_1011}},{{&g_1011,(void*)0,&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,(void*)0},{&g_1011,(void*)0,&g_1011,(void*)0,&g_1011,&g_1011,(void*)0,(void*)0,(void*)0},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0},{&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011}},{{(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,(void*)0,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0,(void*)0},{&g_1011,&g_1011,(void*)0,&g_1011,(void*)0,(void*)0,&g_1011,(void*)0,&g_1011},{&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011}},{{(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{(void*)0,&g_1011,(void*)0,(void*)0,&g_1011,&g_1011,&g_1011,(void*)0,&g_1011},{&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0},{&g_1011,&g_1011,(void*)0,(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011}},{{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{(void*)0,&g_1011,&g_1011,&g_1011,&g_1011,(void*)0,&g_1011,(void*)0,&g_1011},{&g_1011,(void*)0,&g_1011,&g_1011,(void*)0,&g_1011,(void*)0,&g_1011,&g_1011}}};
    uint64_t ****l_1573 = &l_1574[0][3][7];
    uint64_t ***** const l_1572 = &l_1573;
    float *l_1604 = (void*)0;
    int64_t ***l_1649 = (void*)0;
    int64_t l_1671 = 0x3323B2DD3B2ED240LL;
    int8_t l_1687 = 0L;
    int64_t l_1726 = (-7L);
    uint16_t **l_1728[3][9][5] = {{{(void*)0,(void*)0,&l_1525,(void*)0,&l_1525},{&l_1525,(void*)0,(void*)0,&l_1525,&l_1525},{(void*)0,(void*)0,&l_1525,&l_1525,&l_1525},{(void*)0,&l_1525,&l_1525,&l_1525,&l_1525},{&l_1525,(void*)0,&l_1525,&l_1525,(void*)0},{&l_1525,(void*)0,(void*)0,(void*)0,&l_1525},{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525},{&l_1525,(void*)0,&l_1525,&l_1525,&l_1525},{&l_1525,(void*)0,(void*)0,(void*)0,&l_1525}},{{(void*)0,(void*)0,&l_1525,&l_1525,&l_1525},{&l_1525,&l_1525,(void*)0,&l_1525,&l_1525},{(void*)0,(void*)0,&l_1525,(void*)0,&l_1525},{&l_1525,&l_1525,&l_1525,(void*)0,(void*)0},{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525},{(void*)0,&l_1525,(void*)0,&l_1525,&l_1525},{&l_1525,&l_1525,(void*)0,&l_1525,&l_1525},{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525},{(void*)0,&l_1525,&l_1525,&l_1525,&l_1525}},{{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525},{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525},{&l_1525,(void*)0,&l_1525,&l_1525,&l_1525},{(void*)0,&l_1525,&l_1525,(void*)0,&l_1525},{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525},{(void*)0,&l_1525,&l_1525,(void*)0,&l_1525},{&l_1525,&l_1525,(void*)0,&l_1525,&l_1525},{&l_1525,(void*)0,&l_1525,&l_1525,&l_1525},{&l_1525,&l_1525,&l_1525,&l_1525,&l_1525}}};
    uint16_t ***l_1727 = &l_1728[2][3][3];
    int8_t l_1769 = (-1L);
    uint16_t l_1813 = 9UL;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1187[i] = 0x64863E4AL;
    for (p_25 = (-5); (p_25 < 10); p_25 = safe_add_func_int16_t_s_s(p_25, 1))
    { /* block id: 7 */
        uint32_t l_35 = 0x37FF9BDEL;
        int32_t l_39 = 0xBFD7802FL;
        int32_t l_40 = 0x3C348FB8L;
        uint16_t l_1060 = 0UL;
        uint8_t *l_1178 = &g_99;
        int32_t **l_1188[5][8][3] = {{{&g_850,&l_1115,(void*)0},{&g_850,&l_1115,&g_850},{(void*)0,&l_1115,&l_1115},{&g_850,&g_850,&g_850},{&l_1115,&g_850,(void*)0},{&g_850,&g_850,&g_850},{&g_850,&g_850,&l_1115},{&l_1115,&l_1115,&l_1115}},{{&g_850,&l_1115,&g_850},{&l_1115,&l_1115,&g_850},{&g_850,&l_1115,&l_1115},{&l_1115,&g_850,&g_850},{&g_850,&g_850,&l_1115},{&g_850,&l_1115,&g_850},{&l_1115,&g_850,&l_1115},{&g_850,&g_850,&l_1115}},{{(void*)0,&g_850,&l_1115},{&g_850,&l_1115,&l_1115},{&g_850,&g_850,&g_850},{&l_1115,&g_850,&l_1115},{&l_1115,&l_1115,&g_850},{&l_1115,&l_1115,&g_850},{&l_1115,&l_1115,&g_850},{&l_1115,&l_1115,&l_1115}},{{&g_850,&g_850,&g_850},{&l_1115,&g_850,&l_1115},{&l_1115,&g_850,&l_1115},{&g_850,&g_850,&l_1115},{&l_1115,&l_1115,&l_1115},{&g_850,&l_1115,&g_850},{&l_1115,&l_1115,&l_1115},{&l_1115,&l_1115,&g_850}},{{&g_850,&l_1115,&l_1115},{&l_1115,&g_850,&g_850},{&l_1115,&l_1115,&g_850},{&l_1115,&g_850,&l_1115},{&l_1115,&l_1115,&g_850},{&g_850,&g_850,&l_1115},{&l_1115,&g_850,&l_1115},{&l_1115,&g_850,&g_850}}};
        int16_t l_1293 = 0L;
        int16_t l_1350[2][1];
        int32_t ***l_1353 = &l_1188[3][6][0];
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
                l_1350[i][j] = 4L;
        }
        for (p_27 = 4; (p_27 >= 1); p_27 -= 1)
        { /* block id: 10 */
            int64_t l_983 = (-10L);
            uint64_t **l_1054 = (void*)0;
            int32_t l_1059 = 7L;
            uint32_t l_1078 = 0x0A07FDB2L;
            int32_t *l_1095 = &l_1059;
            int8_t *l_1118 = (void*)0;
            uint16_t *l_1170 = &g_515;
            uint16_t * const *l_1169 = &l_1170;
            uint16_t * const ** const l_1168 = &l_1169;
            for (g_31 = 4; (g_31 >= 0); g_31 -= 1)
            { /* block id: 13 */
                int16_t l_1058 = 0x6B01L;
                int32_t *l_1074[8][3] = {{&g_101,&l_39,&l_39},{&l_39,&g_101,&g_101},{&l_39,&g_101,&g_32},{&g_521[0],&l_39,(void*)0},{&l_39,&l_39,(void*)0},{&l_39,&g_521[0],&g_32},{&g_101,&l_39,&g_101},{&g_101,&l_39,&l_39}};
                int16_t *l_1075[8][8] = {{(void*)0,&g_596[0],(void*)0,&l_1058,&g_596[0],&g_596[3],&g_596[0],&l_1058},{&g_596[3],&g_596[0],&g_596[3],&g_596[3],(void*)0,&g_596[3],&g_596[3],&g_596[8]},{(void*)0,&g_596[3],&g_596[6],(void*)0,&g_596[3],&g_596[3],(void*)0,&g_596[6]},{(void*)0,(void*)0,&g_596[3],&l_1058,(void*)0,&g_596[2],(void*)0,(void*)0},{&g_596[3],(void*)0,&g_596[3],&g_596[3],&g_596[0],&g_596[3],&g_596[3],(void*)0},{(void*)0,&g_596[3],&g_596[6],&l_1058,&g_596[3],&g_596[3],&g_596[0],&g_596[6]},{&g_596[8],&g_596[0],&g_596[4],(void*)0,(void*)0,&g_596[4],&g_596[0],&g_596[8]},{&g_596[3],(void*)0,&g_596[6],&g_596[3],(void*)0,&g_596[3],&g_596[3],&l_1058}};
                const uint32_t l_1136 = 0xF53B4383L;
                int i, j;
                for (g_32 = 1; (g_32 <= 4); g_32 += 1)
                { /* block id: 16 */
                    uint32_t *l_38[5][9][5] = {{{&l_35,(void*)0,(void*)0,(void*)0,(void*)0},{&l_35,&l_35,&l_35,&l_35,&l_35},{&l_35,(void*)0,(void*)0,&l_35,&l_35},{&l_35,&l_35,&l_35,(void*)0,(void*)0},{&l_35,&l_35,(void*)0,&l_35,&l_35},{&l_35,&l_35,&l_35,&l_35,(void*)0},{(void*)0,&l_35,(void*)0,(void*)0,&l_35},{&l_35,&l_35,(void*)0,&l_35,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&l_35}},{{(void*)0,&l_35,&l_35,&l_35,(void*)0},{(void*)0,&l_35,&l_35,(void*)0,&l_35},{&l_35,&l_35,&l_35,&l_35,&l_35},{&l_35,(void*)0,&l_35,&l_35,(void*)0},{&l_35,&l_35,&l_35,&l_35,&l_35},{&l_35,&l_35,&l_35,&l_35,&l_35},{(void*)0,&l_35,&l_35,&l_35,&l_35},{&l_35,&l_35,(void*)0,(void*)0,&l_35},{&l_35,&l_35,&l_35,&l_35,&l_35}},{{&l_35,(void*)0,&l_35,&l_35,(void*)0},{&l_35,&l_35,&l_35,&l_35,(void*)0},{&l_35,&l_35,&l_35,(void*)0,(void*)0},{&l_35,(void*)0,&l_35,(void*)0,&l_35},{&l_35,(void*)0,&l_35,(void*)0,(void*)0},{(void*)0,&l_35,&l_35,&l_35,&l_35},{&l_35,&l_35,(void*)0,(void*)0,(void*)0},{&l_35,&l_35,&l_35,&l_35,&l_35},{(void*)0,&l_35,&l_35,&l_35,(void*)0}},{{&l_35,&l_35,&l_35,&l_35,(void*)0},{&l_35,&l_35,&l_35,&l_35,(void*)0},{&l_35,&l_35,&l_35,&l_35,&l_35},{&l_35,&l_35,(void*)0,&l_35,&l_35},{&l_35,&l_35,&l_35,&l_35,&l_35},{(void*)0,(void*)0,(void*)0,&l_35,&l_35},{&l_35,(void*)0,&l_35,&l_35,&l_35},{(void*)0,&l_35,&l_35,&l_35,&l_35},{&l_35,&l_35,&l_35,&l_35,&l_35}},{{(void*)0,(void*)0,&l_35,(void*)0,(void*)0},{&l_35,&l_35,&l_35,&l_35,&l_35},{&l_35,(void*)0,&l_35,(void*)0,&l_35},{&l_35,&l_35,&l_35,(void*)0,&l_35},{&l_35,&l_35,&l_35,(void*)0,(void*)0},{&l_35,&l_35,&l_35,&l_35,&l_35},{(void*)0,(void*)0,(void*)0,&l_35,&l_35},{&l_35,&l_35,&l_35,&l_35,&l_35},{&l_35,&l_35,(void*)0,&l_35,(void*)0}}};
                    int32_t l_67 = 0x9270D6E0L;
                    float *l_1052[4];
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_1052[i] = &g_513;
                    for (g_33 = 0; (g_33 <= 4); g_33 += 1)
                    { /* block id: 19 */
                        int i, j;
                        l_35 &= (g_34[1] = l_30[g_31][(p_27 + 3)]);
                    }
                }
                for (g_222 = 0; (g_222 <= 4); g_222 += 1)
                { /* block id: 449 */
                    int32_t *l_1055 = (void*)0;
                    int32_t *l_1056 = &g_101;
                    int32_t *l_1057[9][1];
                    int i, j;
                    for (i = 0; i < 9; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_1057[i][j] = (void*)0;
                    }
                    g_521[(g_31 + 4)] &= ((l_30[g_31][(g_31 + 1)] < g_596[(p_27 + 2)]) && (((void*)0 != l_1054) < 2L));
                    (*g_850) &= g_596[(p_27 + 1)];
                    ++l_1060;
                    for (g_99 = 0; (g_99 <= 4); g_99 += 1)
                    { /* block id: 455 */
                        int64_t l_1063 = (-9L);
                        if (l_1063)
                            break;
                    }
                }
            }
        }
        for (g_358 = 0; (g_358 != 8); g_358++)
        { /* block id: 517 */
            uint32_t l_1175 = 4294967295UL;
            l_1187[3] ^= (l_1175 | ((((safe_sub_func_int32_t_s_s(p_25, (l_1178 == (*g_567)))) , &g_918) == &g_126[1]) | (safe_div_func_uint16_t_u_u(g_615, (safe_add_func_int8_t_s_s(4L, (safe_mod_func_uint32_t_u_u(((safe_mul_func_int16_t_s_s(p_25, 1L)) == 0x30DFL), p_27))))))));
        }
        l_1115 = &l_40;
        for (g_222 = 0; (g_222 > 60); g_222 = safe_add_func_int64_t_s_s(g_222, 2))
        { /* block id: 523 */
            const int64_t **l_1194 = (void*)0;
            const int64_t ***l_1193 = &l_1194;
            uint64_t *l_1195 = &g_359;
            int32_t l_1198 = 1L;
            uint16_t *l_1203 = &g_150;
            int32_t l_1208 = (-1L);
            const uint64_t *l_1213 = &g_164[0][1][5];
            const uint64_t **l_1212 = &l_1213;
            const uint64_t ***l_1211[5];
            float l_1231 = (-0x6.Ep-1);
            uint8_t **l_1238 = &l_1178;
            uint16_t **l_1255 = &l_1203;
            uint16_t ***l_1254 = &l_1255;
            int64_t *****l_1272 = (void*)0;
            int32_t l_1299 = 0L;
            int64_t l_1301 = 6L;
            int32_t l_1302 = (-1L);
            int64_t l_1304 = 0x0F1252B5CC5010DELL;
            int32_t l_1309 = 1L;
            int32_t l_1310 = 1L;
            int32_t l_1322 = 0x00F8EEA1L;
            int32_t l_1323 = 0xFDAC4C2EL;
            int32_t l_1324 = 1L;
            uint32_t l_1325 = 18446744073709551615UL;
            uint64_t *****l_1360 = (void*)0;
            int8_t ***l_1389[7][9][4] = {{{&g_1241,&l_1239,&g_1241,&l_1239},{&l_1239,(void*)0,(void*)0,&l_1239},{&g_1241,&g_1241,&g_1241,(void*)0},{&g_1241,&g_1241,&g_1241,&l_1239},{&g_1241,&g_1241,(void*)0,(void*)0},{&l_1239,&l_1239,&g_1241,&g_1241},{&g_1241,&g_1241,&g_1241,&l_1239},{&l_1239,&l_1239,&l_1239,&g_1241},{&g_1241,&l_1239,&l_1239,&l_1239}},{{&l_1239,&g_1241,&l_1239,&g_1241},{(void*)0,&l_1239,(void*)0,(void*)0},{&l_1239,&g_1241,&g_1241,&l_1239},{(void*)0,&g_1241,&l_1239,(void*)0},{(void*)0,&g_1241,&g_1241,&l_1239},{&l_1239,(void*)0,&l_1239,&g_1241},{&l_1239,&g_1241,&g_1241,(void*)0},{&g_1241,&g_1241,&l_1239,&g_1241},{&g_1241,&l_1239,&g_1241,&g_1241}},{{&g_1241,&g_1241,&l_1239,(void*)0},{(void*)0,&g_1241,(void*)0,&g_1241},{&g_1241,&l_1239,&l_1239,&g_1241},{&g_1241,(void*)0,(void*)0,&l_1239},{(void*)0,&g_1241,(void*)0,&l_1239},{&g_1241,&l_1239,&l_1239,&l_1239},{&g_1241,&g_1241,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1239,&g_1241},{&g_1241,&g_1241,&g_1241,&l_1239}},{{&g_1241,&g_1241,&l_1239,&g_1241},{&g_1241,(void*)0,&g_1241,(void*)0},{&l_1239,&g_1241,&l_1239,&l_1239},{&g_1241,&l_1239,&g_1241,&l_1239},{&l_1239,&g_1241,&g_1241,&l_1239},{&l_1239,(void*)0,&g_1241,&g_1241},{&g_1241,&l_1239,&l_1239,&g_1241},{&l_1239,&g_1241,&g_1241,(void*)0},{&g_1241,&g_1241,&l_1239,&g_1241}},{{&g_1241,&l_1239,&g_1241,&g_1241},{&g_1241,&g_1241,&l_1239,(void*)0},{(void*)0,&g_1241,(void*)0,&g_1241},{&g_1241,&l_1239,&l_1239,&g_1241},{&g_1241,(void*)0,(void*)0,&l_1239},{(void*)0,&g_1241,(void*)0,&l_1239},{&g_1241,&l_1239,&l_1239,&l_1239},{&g_1241,&g_1241,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1239,&g_1241}},{{&g_1241,&g_1241,&g_1241,&l_1239},{&g_1241,&g_1241,&l_1239,&g_1241},{&g_1241,(void*)0,&g_1241,(void*)0},{&l_1239,&g_1241,&l_1239,&l_1239},{&g_1241,&l_1239,&g_1241,&l_1239},{&l_1239,&g_1241,&g_1241,&l_1239},{&l_1239,(void*)0,&g_1241,&g_1241},{&g_1241,&l_1239,&l_1239,&g_1241},{&l_1239,&g_1241,&g_1241,(void*)0}},{{&g_1241,&g_1241,&l_1239,&g_1241},{&g_1241,&l_1239,&g_1241,&g_1241},{&g_1241,&g_1241,&l_1239,(void*)0},{(void*)0,&g_1241,(void*)0,&g_1241},{&g_1241,&l_1239,&l_1239,&g_1241},{&g_1241,(void*)0,(void*)0,&l_1239},{(void*)0,&g_1241,(void*)0,&l_1239},{&g_1241,&l_1239,&l_1239,&l_1239},{&g_1241,&g_1241,&l_1239,&l_1239}}};
            uint32_t l_1416 = 4294967290UL;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_1211[i] = &l_1212;
            if (p_27)
                break;
            if ((p_27 != (safe_mod_func_uint32_t_u_u((((((*l_1195) = (l_1193 == (void*)0)) & (safe_lshift_func_int16_t_s_s((l_1198 |= 2L), (p_27 & (safe_mul_func_uint16_t_u_u(((*l_1203) |= (safe_add_func_uint8_t_u_u(1UL, 0xE4L))), ((((**g_567) = ((safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s(((0x0B22L > (*l_1115)) || 0x47F4L), g_114[3][1])), p_27)) , 6UL)) >= p_27) <= p_27))))))) >= l_1208) < 0x367FL), p_27))))
            { /* block id: 529 */
                int32_t ***l_1215 = &g_272;
                int32_t ****l_1214 = &l_1215;
                float *l_1226 = &g_513;
                int64_t l_1232 = 0xA5D0CE2BF05FAFB1LL;
                float *l_1233 = &l_1073;
                float *l_1234 = (void*)0;
                float *l_1235[10];
                int i;
                for (i = 0; i < 10; i++)
                    l_1235[i] = &l_1231;
                g_1237 = (safe_mul_func_float_f_f((g_1236 = ((p_27 <= ((((((p_27 >= ((l_1211[3] == &l_1212) <= (((*l_1233) = (((((*l_1214) = &l_1188[3][3][1]) != (void*)0) != ((safe_sub_func_float_f_f((safe_add_func_float_f_f(((((l_1232 = (((safe_sub_func_float_f_f((((safe_sub_func_float_f_f(((*l_1226) = (safe_sub_func_float_f_f(g_521[6], 0x6.0C2A7Fp+70))), ((((safe_lshift_func_uint8_t_u_s(((safe_mul_func_int8_t_s_s(1L, 0UL)) <= 0UL), 6)) >= (*l_1115)) > g_164[0][0][5]) , 0xD.20FC0Cp+98))) < 0xE.712105p-46) <= 0x1.Dp+1), l_1231)) >= 0xB.97E113p-12) < g_164[0][1][5])) > g_496[7]) <= p_27) < l_1198), 0x1.4p+1)), (-0x3.4p+1))) > (*l_1115))) < (-0x1.Ap+1))) < (*l_1115)))) <= 0x7.C69943p+92) < (-0x4.7p-1)) != g_289) < p_26) == 0x1.8D5237p-29)) != 0xE.6AC373p-31)), p_27));
                return g_161;
            }
            else
            { /* block id: 537 */
                int64_t l_1253[7][1] = {{0xD2B3D7299979DBA6LL},{0L},{0xD2B3D7299979DBA6LL},{0L},{0xD2B3D7299979DBA6LL},{0L},{0xD2B3D7299979DBA6LL}};
                int32_t l_1258 = 2L;
                int16_t l_1292 = (-1L);
                int32_t l_1295 = (-1L);
                int32_t l_1297 = (-1L);
                int32_t l_1305 = 1L;
                int32_t l_1307 = (-1L);
                int32_t l_1308[4];
                uint8_t *l_1332 = &g_1236;
                uint32_t l_1351 = 1UL;
                float ** const l_1368[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int8_t *** const l_1423 = &g_1241;
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1308[i] = 0x217E91B9L;
                if (((*g_850) |= (l_1238 != (void*)0)))
                { /* block id: 539 */
                    int8_t ***l_1240[5][10][5] = {{{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,(void*)0,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,(void*)0,&l_1239,(void*)0,&l_1239}},{{&l_1239,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239}},{{(void*)0,(void*)0,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,(void*)0,&l_1239,(void*)0,&l_1239},{&l_1239,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239}},{{&l_1239,&l_1239,&l_1239,(void*)0,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,(void*)0,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,(void*)0,&l_1239}},{{&l_1239,&l_1239,&l_1239,&l_1239,(void*)0},{(void*)0,&l_1239,&l_1239,(void*)0,(void*)0},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,(void*)0,(void*)0,&l_1239},{&l_1239,&l_1239,(void*)0,&l_1239,&l_1239},{&l_1239,(void*)0,&l_1239,(void*)0,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{(void*)0,&l_1239,&l_1239,&l_1239,&l_1239},{&l_1239,(void*)0,&l_1239,&l_1239,&l_1239},{&l_1239,&l_1239,&l_1239,&l_1239,&l_1239}}};
                    int32_t l_1259 = 1L;
                    int32_t l_1260 = 7L;
                    int32_t *l_1264 = &l_1208;
                    uint64_t ***l_1289 = &g_1011;
                    uint64_t ****l_1288 = &l_1289;
                    int32_t l_1300 = 0x26500479L;
                    int32_t l_1315[2];
                    int32_t l_1316 = 0xED292267L;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1315[i] = 0x88EC0BBCL;
                    if (((g_1241 = l_1239) == (void*)0))
                    { /* block id: 541 */
                        int32_t l_1244 = 0x80B62E4FL;
                        int64_t *l_1245 = &g_290[5];
                        float *l_1256 = (void*)0;
                        float *l_1257 = &l_1073;
                        (*l_1257) = (safe_add_func_float_f_f((((l_1208 > p_27) , g_2[1][2]) > l_1208), (((*l_1245) = l_1244) , (safe_div_func_float_f_f((!(safe_add_func_float_f_f(((safe_div_func_float_f_f((l_1253[1][0] <= g_99), (l_1254 != (void*)0))) >= p_25), g_34[1]))), (*l_1115))))));
                    }
                    else
                    { /* block id: 544 */
                        uint32_t l_1261 = 0x21BEAD3EL;
                        l_1261++;
                    }
                    l_1264 = &l_1258;
                    if ((((void*)0 == l_1265) & (l_1198 = ((0L || ((g_34[1] == (safe_mul_func_int16_t_s_s((*l_1115), p_27))) && (p_25 != (((safe_mul_func_uint16_t_u_u((((p_25 , l_1272) != &g_645) | 0x2994L), g_34[1])) , l_1208) , p_25)))) & (*l_1264)))))
                    { /* block id: 549 */
                        int64_t *l_1277 = (void*)0;
                        int64_t *l_1278 = &g_991;
                        int32_t l_1283 = 0x589A04C5L;
                        uint64_t ****l_1291[10] = {&l_1289,&l_1289,&l_1289,&l_1289,&l_1289,&l_1289,&l_1289,&l_1289,&l_1289,&l_1289};
                        uint64_t *****l_1290 = &l_1291[8];
                        int i;
                        (*g_850) |= ((((((safe_add_func_int32_t_s_s(((safe_add_func_int64_t_s_s(((*l_1278) = (*l_1264)), (g_290[3] = l_1208))) , (p_25 ^ (((*l_1264) , &l_1258) == &l_1258))), (p_25 >= (p_25 >= p_25)))) < 0x12L) > 1UL) && p_27) , g_360) || l_1198);
                        l_1208 |= ((0xC969L ^ (safe_rshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u(p_27, 0xE7L)), l_1283))) , ((((safe_div_func_float_f_f((safe_sub_func_float_f_f((g_182 != g_289), ((l_1288 == ((*l_1290) = (void*)0)) < (0xD.9A3724p-42 <= (*l_1264))))), 0x6.1D8F77p-2)) , 0UL) > p_27) < (*l_1264)));
                        (*l_1115) = p_27;
                    }
                    else
                    { /* block id: 556 */
                        int16_t l_1296 = 0xA960L;
                        int32_t l_1303 = 0x678516EEL;
                        int32_t l_1306 = 0xA007493CL;
                        int32_t l_1311 = 9L;
                        uint8_t l_1312 = 0x0FL;
                        int32_t l_1317 = (-7L);
                        int32_t l_1318 = 1L;
                        int32_t l_1319 = 0x73DBE0A3L;
                        int32_t l_1320 = (-10L);
                        int32_t l_1321[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1321[i] = 0L;
                        ++l_1312;
                        l_1303 ^= (p_25 | (*l_1115));
                        ++l_1325;
                    }
                    for (g_76 = 2; (g_76 >= 6); g_76++)
                    { /* block id: 563 */
                        return p_27;
                    }
                }
                else
                { /* block id: 566 */
                    return l_1258;
                }
                if (((0x38309655L | (g_222 , l_1302)) > ((safe_mod_func_uint8_t_u_u(p_27, ((*l_1115) &= 0x29L))) >= ((0x24599473L || p_27) == l_1322))))
                { /* block id: 570 */
                    int32_t **l_1335 = &g_850;
                    int16_t *l_1338 = &l_1293;
                    int16_t *l_1339[9][3][6] = {{{&l_1292,&g_1114,&g_1114,&g_596[7],&g_1114,(void*)0},{&g_596[9],&g_1114,&g_596[3],&g_596[3],(void*)0,&g_1114},{&l_1292,&l_1292,&g_596[3],&l_1292,&g_596[3],&l_1292}},{{&g_596[2],&g_596[9],&g_1114,&g_1114,&g_596[3],&g_1114},{&g_1114,&l_1292,&g_596[3],&l_1292,&l_1292,(void*)0},{&g_596[3],(void*)0,(void*)0,&g_596[3],&g_596[3],&l_1292}},{{&l_1292,&l_1292,&l_1292,(void*)0,(void*)0,&g_596[3]},{&l_1292,&g_1114,&g_596[5],&g_596[3],&g_1114,&g_596[6]},{&g_596[5],&l_1292,&g_596[9],&l_1292,&l_1292,&l_1292}},{{(void*)0,&g_596[6],&g_596[3],(void*)0,&g_596[3],&l_1292},{&g_596[7],&g_596[3],&g_596[2],(void*)0,(void*)0,&g_596[2]},{&g_1114,&g_1114,&g_1114,&g_596[3],(void*)0,&l_1292}},{{(void*)0,&g_596[3],(void*)0,&g_596[3],&l_1292,&g_1114},{&g_596[2],(void*)0,(void*)0,&l_1292,&g_1114,&l_1292},{&g_596[2],&l_1292,&g_1114,&g_1114,&g_596[3],&g_596[2]}},{{&g_1114,&g_596[3],&g_596[2],&l_1292,(void*)0,&l_1292},{&g_1114,&g_596[3],&g_596[3],&g_1114,&l_1292,&l_1292},{&l_1292,&g_596[3],&g_596[9],(void*)0,&l_1292,&l_1292}},{{(void*)0,&l_1292,&g_1114,(void*)0,&g_1114,&l_1292},{&g_1114,(void*)0,&g_596[3],(void*)0,&g_1114,&l_1292},{&l_1292,&g_596[9],&g_1114,&g_596[3],&g_596[3],(void*)0}},{{&l_1292,&g_596[3],&g_596[3],&g_596[3],(void*)0,&l_1292},{&g_596[3],(void*)0,&g_596[2],&g_596[9],&l_1292,&g_596[9]},{&g_1114,&g_596[3],&g_1114,&g_596[8],&g_596[3],&g_1114}},{{&g_596[9],&l_1292,&g_1114,(void*)0,&l_1292,&g_596[3]},{&l_1292,(void*)0,&l_1292,(void*)0,&g_596[3],&g_596[8]},{&g_596[9],&g_596[5],&g_596[6],&g_596[8],(void*)0,&l_1292}}};
                    int32_t l_1390[1][7] = {{(-8L),(-8L),(-8L),(-8L),(-8L),(-8L),(-8L)}};
                    int i, j, k;
                    (*l_1115) &= (((*g_567) != (l_1332 = ((*l_1238) = (*g_567)))) && (p_27 , ((((*g_1017) &= (safe_sub_func_uint32_t_u_u(((l_1305 = (0xB130F6F0L <= ((l_1335 != (void*)0) < (g_596[3] = (((safe_mul_func_int16_t_s_s(((*l_1338) = 0x95D3L), (l_1258 | (l_1323 | (*g_461))))) >= (**l_1335)) == 0L))))) && p_25), 0xFE0377B9L))) == (-4L)) ^ 0x262846C3D92071A9LL)));
                    if ((safe_sub_func_uint32_t_u_u(4294967295UL, (((**l_1255) ^= ((*l_1115) == (safe_unary_minus_func_int8_t_s(((&g_272 != (l_1353 = ((((**g_567) , (!(((**g_567) = (&l_1187[0] != (void*)0)) , (((((safe_sub_func_uint8_t_u_u((safe_add_func_int32_t_s_s((*l_1115), (safe_mod_func_uint8_t_u_u(p_25, (((p_27 ^ (-7L)) < p_25) ^ l_1350[1][0]))))), 0xCBL)) ^ l_1325) < l_1351) , 0x2945L) || p_27)))) == g_1352) , (void*)0))) & 248UL))))) != p_25))))
                    { /* block id: 581 */
                        (**l_1335) = (-2L);
                        (*g_850) |= ((**g_567) ^ (safe_lshift_func_int16_t_s_u(p_27, 7)));
                        (*l_1115) |= (safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u((253UL ^ l_1302), 5)), ((void*)0 != l_1360)));
                    }
                    else
                    { /* block id: 585 */
                        float ***l_1367 = &l_1365;
                        int32_t l_1369 = 0L;
                        uint16_t l_1378 = 65535UL;
                        (*g_850) = (*g_1048);
                        l_1390[0][4] &= ((safe_add_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(((((*l_1367) = l_1365) == l_1368[4]) < ((l_1369 != (safe_lshift_func_int8_t_s_s(((*g_1017) = ((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u(65531UL, ((safe_mod_func_uint64_t_u_u(((*l_1195) = l_1322), l_1378)) != p_27))), (((**l_1335) ^= ((safe_mul_func_uint8_t_u_u((p_25 > (~((((((safe_mul_func_int8_t_s_s((safe_add_func_uint32_t_u_u((safe_sub_func_uint16_t_u_u(p_25, g_290[3])), p_25)), 0x5FL)) , g_1388) != (void*)0) && 1UL) , &g_1241) != l_1389[5][2][3]))), (*g_461))) | 0x72B8L)) && 0x9D6823D213B6E385LL))) && 0x9DL)), (*l_1115)))) & (*l_1115))), p_25)), p_25)) && 0xE8310DA462170148LL);
                        return l_1391;
                    }
                }
                else
                { /* block id: 594 */
                    int8_t l_1392[9][10][2] = {{{4L,1L},{1L,0x0BL},{0L,0x5DL},{1L,2L},{2L,2L},{1L,0x5DL},{0L,0x0BL},{1L,1L},{4L,1L},{(-1L),0x7BL}},{{(-1L),1L},{4L,1L},{1L,0x0BL},{0L,0x5DL},{1L,2L},{2L,2L},{1L,0x5DL},{0L,0x0BL},{1L,1L},{4L,1L}},{{(-1L),0x7BL},{(-1L),1L},{4L,1L},{1L,0x0BL},{0L,0x5DL},{1L,2L},{2L,2L},{1L,0x5DL},{0L,0x0BL},{1L,1L}},{{4L,1L},{(-1L),0x7BL},{(-1L),1L},{4L,1L},{1L,0x0BL},{0L,0x5DL},{1L,2L},{2L,2L},{1L,0x5DL},{0L,0x0BL}},{{1L,1L},{4L,1L},{(-1L),0x7BL},{(-1L),1L},{4L,1L},{1L,0x0BL},{0L,0x5DL},{1L,2L},{2L,2L},{1L,0x5DL}},{{0L,0x0BL},{1L,1L},{4L,1L},{(-1L),0x7BL},{(-1L),1L},{4L,1L},{1L,0x0BL},{0L,0x5DL},{1L,2L},{2L,2L}},{{1L,0x5DL},{0L,0x0BL},{1L,1L},{0L,0x7BL},{0x5DL,(-6L)},{0x5DL,0x7BL},{0L,2L},{0x7BL,4L},{(-1L),1L},{2L,0x8CL}},{{0x8CL,0x8CL},{2L,1L},{(-1L),4L},{0x7BL,2L},{0L,0x7BL},{0x5DL,(-6L)},{0x5DL,0x7BL},{0L,2L},{0x7BL,4L},{(-1L),1L}},{{2L,0x8CL},{0x8CL,0x8CL},{2L,1L},{(-1L),4L},{0x7BL,2L},{0L,0x7BL},{0x5DL,(-6L)},{0x5DL,0x7BL},{0L,2L},{0x7BL,4L}}};
                    uint8_t l_1393[7];
                    int32_t *l_1398 = &l_1308[0];
                    int32_t *****l_1412 = &g_1410;
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_1393[i] = 6UL;
                    l_1393[1]--;
                    g_850 = &l_1187[3];
                    for (l_1323 = 0; (l_1323 > (-29)); l_1323 = safe_sub_func_uint32_t_u_u(l_1323, 6))
                    { /* block id: 599 */
                        l_1398 = &l_1302;
                    }
                    if ((safe_rshift_func_uint8_t_u_s(((safe_add_func_int64_t_s_s(l_1325, (safe_mul_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(6L, g_1051[3][2][0])), ((p_25 <= 0x4CC256D08437CA67LL) , g_521[2]))))) <= (safe_add_func_uint32_t_u_u((!(((*l_1412) = g_1410) != (void*)0)), (safe_rshift_func_int8_t_s_s((&l_1187[3] == &l_1187[3]), p_27))))), 4)))
                    { /* block id: 603 */
                        if (p_27)
                            break;
                    }
                    else
                    { /* block id: 605 */
                        int32_t l_1415[2][8][5] = {{{0L,0xC88D198FL,(-4L),0L,1L},{0x18ECB4D5L,0x85C60EF9L,1L,1L,1L},{1L,1L,0xB9D38CECL,0xC88D198FL,1L},{(-4L),1L,(-4L),0x85C60EF9L,0xB423A057L},{1L,(-4L),(-4L),1L,0L},{0x18ECB4D5L,1L,(-1L),1L,0x18ECB4D5L},{0L,1L,(-4L),(-4L),1L},{0xB423A057L,0x85C60EF9L,(-4L),1L,(-4L)}},{{1L,0xC88D198FL,0xB9D38CECL,1L,1L},{1L,1L,1L,0x85C60EF9L,0x18ECB4D5L},{1L,0L,(-4L),0xC88D198FL,0L},{0xB423A057L,1L,9L,1L,0xB423A057L},{0L,0xC88D198FL,(-4L),0L,1L},{0x18ECB4D5L,0x85C60EF9L,1L,1L,1L},{1L,1L,0xB9D38CECL,0xC88D198FL,1L},{(-4L),1L,(-4L),0x85C60EF9L,0xB423A057L}}};
                        uint8_t l_1425 = 0xB5L;
                        int i, j, k;
                        ++l_1416;
                        (*l_1115) = (safe_add_func_int64_t_s_s((safe_mul_func_int8_t_s_s((l_1308[3] , ((p_25 , &g_1051[3][0][1]) == &g_152)), p_25)), (l_1425 = ((((void*)0 == l_1423) , p_25) >= (((p_25 , l_1424) || 0UL) < 0xACEA09C3L)))));
                        (*g_850) = p_25;
                    }
                }
            }
        }
    }
    l_1440 = func_64((((*l_1426) |= p_25) == ((~((void*)0 == (*g_567))) ^ (safe_sub_func_uint8_t_u_u(((*l_1436) |= (safe_div_func_int64_t_s_s((g_1053 < (safe_mod_func_uint8_t_u_u(p_27, p_25))), (safe_mod_func_uint8_t_u_u((**g_567), p_25))))), (safe_mod_func_int8_t_s_s(0x30L, (*g_461))))))), (*g_850));
    for (g_1114 = 0; (g_1114 >= 0); g_1114 -= 1)
    { /* block id: 620 */
        uint64_t ***l_1443[7][6] = {{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011},{&g_1011,&g_1011,&g_1011,&g_1011,&g_1011,&g_1011}};
        uint64_t ****l_1442 = &l_1443[3][4];
        int32_t l_1446[9][3];
        int32_t *l_1484 = &l_1446[3][0];
        int32_t l_1512 = 0L;
        uint32_t **l_1545[3][4][9] = {{{&g_352[0],&g_352[0],(void*)0,&g_352[0],(void*)0,&g_352[0],&g_352[0],(void*)0,&g_352[0]},{&g_352[0],&g_352[0],&g_75,&g_352[0],&g_352[2],&l_1050,&g_75,&g_352[0],&g_352[0]},{&g_352[2],&g_75,&g_75,&g_75,&g_352[1],&g_352[1],&g_75,&g_75,&g_75},{&g_75,&g_75,(void*)0,(void*)0,&g_352[1],(void*)0,&g_352[2],&g_75,&g_75}},{{(void*)0,&g_352[0],(void*)0,&g_75,&g_352[0],&g_352[0],(void*)0,&g_352[0],&g_352[0]},{(void*)0,&g_75,&g_75,(void*)0,&g_75,&g_352[0],&g_75,&g_352[0],&g_75},{&g_75,&g_75,&g_352[1],&l_1050,&g_352[0],&g_352[0],(void*)0,&g_75,&g_75},{&g_352[2],&g_352[0],&g_75,&l_1050,&g_75,&g_352[0],&g_352[2],&g_75,&g_352[0]}},{{&l_1050,&g_352[0],&g_352[1],(void*)0,&g_352[0],(void*)0,&g_75,&g_352[0],&g_352[0]},{&g_352[2],&g_75,&g_75,&g_75,&g_352[1],&g_352[1],&g_75,&g_75,&g_75},{&g_75,&g_75,(void*)0,(void*)0,&g_352[1],(void*)0,&g_352[2],&g_75,&g_75},{(void*)0,&g_352[0],(void*)0,&g_75,&g_352[0],&g_352[0],(void*)0,&g_352[0],&g_352[0]}}};
        uint32_t ***l_1544[7];
        uint32_t **l_1586 = &g_352[0];
        const int16_t *l_1610 = &g_1611;
        const int16_t **l_1609 = &l_1610;
        int16_t *l_1612 = &g_596[4];
        int8_t l_1613 = 0x41L;
        float *l_1614 = &l_1073;
        int32_t **l_1686 = (void*)0;
        const int32_t *l_1698 = &l_1512;
        const int64_t ***l_1713 = (void*)0;
        uint8_t ***l_1736 = &g_567;
        int i, j, k;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 3; j++)
                l_1446[i][j] = 0x6D0DC129L;
        }
        for (i = 0; i < 7; i++)
            l_1544[i] = &l_1545[2][3][1];
        for (g_355 = 0; (g_355 <= 0); g_355 += 1)
        { /* block id: 623 */
            uint64_t ****l_1441 = (void*)0;
            uint32_t **l_1445 = &g_352[0];
            uint32_t ***l_1444 = &l_1445;
            const int32_t *l_1495 = &g_31;
            uint64_t *l_1497 = &l_30[3][6];
            uint16_t *l_1534 = &g_150;
            int i;
            l_1442 = l_1441;
            (*l_1444) = &g_352[1];
            if (g_290[(g_1114 + 8)])
                continue;
            for (g_1237 = 0; (g_1237 <= 8); g_1237 += 1)
            { /* block id: 629 */
                uint32_t l_1469 = 0x9C1D611AL;
                int32_t l_1496 = 0x9168AF29L;
                int32_t *l_1551 = &l_1512;
                uint32_t l_1589[10] = {0xAF80C1FDL,0x439EDF85L,0x439EDF85L,0xAF80C1FDL,0x439EDF85L,0x439EDF85L,0xAF80C1FDL,0x439EDF85L,0x439EDF85L,0xAF80C1FDL};
                int i;
                for (g_359 = 0; (g_359 <= 8); g_359 += 1)
                { /* block id: 632 */
                    uint32_t l_1447 = 0x7B0873CAL;
                    uint16_t l_1470 = 0xE2ADL;
                    int32_t *l_1483 = &g_289;
                    int64_t l_1537 = 0L;
                    int i;
                    l_1447++;
                    if (((((safe_div_func_int64_t_s_s((safe_unary_minus_func_uint64_t_u((((safe_sub_func_int8_t_s_s(g_290[g_359], l_1187[g_1114])) && ((safe_sub_func_int64_t_s_s((((safe_div_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(((safe_rshift_func_int8_t_s_s(l_1187[(g_355 + 1)], 4)) ^ (((0x7FL || (+p_27)) , (safe_mod_func_int16_t_s_s(((+((safe_add_func_int8_t_s_s(l_1469, 1L)) && (l_1446[3][0] == l_1470))) < 254UL), l_1469))) && p_27)), l_1469)), l_1469)) | 0x96L) & p_25), p_27)) < g_34[1])) >= p_27))), 0x74F0474E31B8D14ALL)) <= 0xCAE6L) , p_25) , g_1471))
                    { /* block id: 634 */
                        uint8_t l_1478 = 0xBEL;
                        int32_t *l_1485[7] = {&g_32,&g_289,&g_32,&g_32,&g_289,&g_32,&g_32};
                        uint32_t *l_1492 = &g_496[7];
                        const int32_t **l_1493 = (void*)0;
                        const int32_t **l_1494[7] = {&g_1048,&g_1048,&g_1048,&g_1048,&g_1048,&g_1048,&g_1048};
                        int i;
                        l_1495 = func_53((safe_sub_func_float_f_f((((safe_mul_func_float_f_f((safe_sub_func_float_f_f(((l_1478 & (((safe_div_func_uint8_t_u_u((p_27 , 2UL), (safe_mul_func_uint16_t_u_u((p_27 , (((*l_1492) = (((l_1485[6] = (l_1484 = l_1483)) != &l_1187[g_1114]) && (safe_add_func_uint32_t_u_u(((safe_mod_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u((p_25 , p_27), 18446744073709551615UL)), p_27)) == p_27), 0x28D38F70L)))) , (*l_1484))), g_521[9])))) , 0x1142L) & g_1236)) , (-0x1.Ep-1)), (-0x1.0p+1))), (-0x9.Cp+1))) == g_615) < 0x4.19163Ap-33), 0x1.Ep+1)), l_1469);
                        if (l_1496)
                            continue;
                        (*l_1483) ^= p_25;
                    }
                    else
                    { /* block id: 641 */
                        if (p_27)
                            break;
                        (*l_1483) |= p_25;
                    }
                    if ((*l_1495))
                        break;
                    for (l_1496 = 0; (l_1496 >= 0); l_1496 -= 1)
                    { /* block id: 648 */
                        uint16_t *l_1502 = &g_358;
                        int32_t l_1505[10] = {1L,0x1388E261L,0x1388E261L,1L,0x1388E261L,0x1388E261L,1L,0x1388E261L,0x1388E261L,1L};
                        uint16_t **l_1526 = &l_1502;
                        int64_t *l_1533 = &g_290[3];
                        uint16_t **l_1535 = (void*)0;
                        uint16_t **l_1536 = &l_1534;
                        int i, j, k;
                        (*l_1366) = (((void*)0 != l_1497) , 0xF.67F2ABp+83);
                        if (l_1496)
                            continue;
                        l_1512 ^= (safe_lshift_func_int8_t_s_u(l_1496, ((+(((*l_1502) = (~g_521[7])) | ((((safe_div_func_int32_t_s_s(p_27, p_25)) | l_1505[8]) || ((p_25 == ((safe_mul_func_uint8_t_u_u(p_27, (((safe_add_func_int8_t_s_s(l_1505[7], ((safe_mod_func_uint64_t_u_u((g_164[g_1114][g_1114][(g_355 + 6)] = g_112), p_25)) > 4294967291UL))) , 0xFBL) <= p_25))) && p_25)) <= 0x2F852CE0L)) <= (*l_1484)))) < 0x82FA87ACL)));
                        (*l_1366) = (safe_add_func_float_f_f(((*l_1484) >= ((g_164[g_1114][g_1114][(g_1114 + 6)] = ((((safe_sub_func_uint16_t_u_u((((safe_mul_func_int8_t_s_s(((**g_1241) &= ((safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(((((*l_1426) = p_27) && ((((l_1525 = &g_515) == ((*l_1526) = (void*)0)) != (safe_add_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_u(((p_25 , ((-1L) | ((*l_1533) = ((*l_1426) = ((g_75 = &g_222) != (void*)0))))) > (((*l_1536) = l_1534) == &p_25)), p_27)) <= 0x131B820AB2AB14E5LL), (*l_1495)))) , p_27)) == l_1537), (*l_1495))), 6)), 0)) | (-1L))), 0L)) & 246UL) && g_164[g_1114][g_1114][(g_355 + 6)]), 0x9D57L)) , p_27) && (*l_1484)) | 0xDC36BC79A6D6DB29LL)) , p_26)), g_114[2][0]));
                    }
                }
                for (g_32 = 0; (g_32 <= 0); g_32 += 1)
                { /* block id: 668 */
                    uint32_t ****l_1546[5][4][10] = {{{&l_1444,&l_1544[0],&l_1544[3],&l_1544[5],&l_1544[0],&l_1544[0],&l_1544[5],&l_1544[3],&l_1544[0],&l_1444},{&l_1544[4],&l_1444,&l_1544[2],&l_1544[0],&l_1444,&l_1544[3],&l_1444,&l_1544[0],&l_1544[0],&l_1544[0]},{&l_1444,&l_1544[0],&l_1544[0],&l_1444,&l_1444,&l_1544[0],(void*)0,&l_1544[6],&l_1444,&l_1444},{&l_1444,&l_1444,&l_1544[0],&l_1544[0],&l_1544[0],&l_1544[0],(void*)0,&l_1544[5],&l_1444,&l_1544[1]}},{{&l_1544[6],&l_1444,&l_1444,&l_1544[4],&l_1544[0],&l_1444,&l_1544[0],&l_1544[2],(void*)0,&l_1544[2]},{&l_1444,&l_1544[0],&l_1544[0],&l_1544[1],&l_1544[0],&l_1544[0],&l_1444,&l_1444,&l_1544[0],&l_1444},{(void*)0,&l_1544[2],(void*)0,(void*)0,&l_1544[0],&l_1544[0],&l_1544[0],(void*)0,&l_1444,&l_1444},{&l_1444,&l_1544[3],&l_1444,(void*)0,&l_1444,&l_1444,&l_1544[0],&l_1544[2],&l_1544[0],&l_1544[6]}},{{(void*)0,&l_1544[0],&l_1444,&l_1544[4],&l_1444,&l_1444,&l_1544[0],&l_1444,&l_1544[5],&l_1544[5]},{(void*)0,&l_1444,&l_1544[0],&l_1544[3],&l_1544[0],(void*)0,&l_1444,&l_1444,(void*)0,&l_1544[0]},{&l_1544[2],(void*)0,(void*)0,&l_1544[2],&l_1544[5],&l_1544[0],(void*)0,(void*)0,&l_1544[0],&l_1544[3]},{&l_1544[3],&l_1544[0],&l_1544[1],&l_1444,&l_1544[5],&l_1444,(void*)0,(void*)0,&l_1544[0],&l_1544[0]}},{{(void*)0,&l_1544[5],(void*)0,&l_1544[2],&l_1544[4],&l_1544[0],&l_1444,&l_1544[3],(void*)0,&l_1544[0]},{&l_1544[6],&l_1444,&l_1544[4],&l_1544[3],&l_1544[0],&l_1544[1],&l_1444,&l_1544[6],&l_1544[5],&l_1444},{(void*)0,&l_1544[0],&l_1544[0],&l_1544[4],&l_1444,&l_1544[0],&l_1444,&l_1544[4],&l_1544[0],&l_1544[0]},{&l_1544[0],(void*)0,&l_1544[0],(void*)0,&l_1544[5],&l_1544[3],(void*)0,&l_1544[0],&l_1444,&l_1444}},{{&l_1544[0],&l_1444,&l_1544[0],(void*)0,&l_1444,&l_1544[3],&l_1444,&l_1544[5],&l_1544[0],&l_1544[6]},{&l_1544[0],&l_1544[0],&l_1544[0],&l_1544[5],&l_1544[0],&l_1544[0],&l_1544[5],(void*)0,(void*)0,&l_1544[0]},{(void*)0,&l_1544[0],&l_1544[0],&l_1544[0],&l_1544[0],&l_1544[1],&l_1444,&l_1544[0],&l_1544[0],&l_1544[0]},{&l_1544[6],&l_1544[3],&l_1544[0],&l_1444,&l_1444,&l_1544[0],&l_1544[3],&l_1544[6],&l_1444,&l_1444}}};
                    int32_t *l_1550 = (void*)0;
                    int16_t *l_1599 = &g_596[3];
                    int i, j, k;
                }
            }
        }
        (*l_1614) = (g_496[6] < ((p_27 , (safe_sub_func_float_f_f((safe_sub_func_float_f_f(((p_26 = ((**l_1365) = ((l_1604 != ((g_521[0] , ((p_25 > (((*l_1525) &= (((*l_1484) , 1L) < (safe_add_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((((*l_1609) = &g_1114) == l_1612) < 18446744073709551615UL), (*l_1484))), p_25)))) < l_1613)) , (*l_1484))) , l_1484)) <= g_150))) == 0x9.9E7C1Dp+47), g_150)), 0x7.0BD4D5p+90))) <= (*l_1484)));
        for (g_1053 = 0; (g_1053 <= 0); g_1053 += 1)
        { /* block id: 696 */
            int32_t l_1623 = 1L;
            int32_t l_1654[3][6] = {{0x714D2791L,0xEE3682C2L,0xEE3682C2L,0x714D2791L,0x76D168C7L,0x714D2791L},{0x714D2791L,0x76D168C7L,0x714D2791L,0xEE3682C2L,0xEE3682C2L,0x714D2791L},{0x025B0FC6L,0x025B0FC6L,0xEE3682C2L,(-1L),0xEE3682C2L,0x025B0FC6L}};
            int64_t l_1666[7] = {(-4L),(-10L),(-10L),(-4L),(-10L),(-10L),(-4L)};
            uint32_t ** const *l_1712 = &l_1545[2][1][2];
            int64_t ***** const l_1741 = (void*)0;
            uint32_t *l_1799 = (void*)0;
            int i, j;
        }
    }
    return p_25;
}


/* ------------------------------------------ */
/* 
 * reads : g_75 g_76 g_596
 * writes: g_513 g_1048
 */
static uint8_t  func_49(const uint32_t * p_50, int16_t  p_51, int32_t  p_52)
{ /* block id: 437 */
    float *l_1028[6][7][6] = {{{(void*)0,&g_513,&g_513,(void*)0,&g_513,&g_513},{&g_513,&g_513,&g_513,(void*)0,(void*)0,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,(void*)0},{&g_513,&g_513,&g_513,&g_513,(void*)0,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{(void*)0,&g_513,(void*)0,&g_513,&g_513,&g_513},{&g_513,(void*)0,&g_513,(void*)0,&g_513,&g_513}},{{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,(void*)0,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,(void*)0,&g_513,&g_513,&g_513,&g_513},{(void*)0,(void*)0,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{(void*)0,&g_513,&g_513,&g_513,(void*)0,&g_513}},{{&g_513,&g_513,&g_513,(void*)0,&g_513,&g_513},{(void*)0,&g_513,&g_513,&g_513,(void*)0,&g_513},{(void*)0,(void*)0,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,(void*)0,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,(void*)0,&g_513,&g_513,&g_513}},{{&g_513,&g_513,&g_513,(void*)0,&g_513,&g_513},{&g_513,&g_513,(void*)0,&g_513,&g_513,&g_513},{(void*)0,&g_513,&g_513,&g_513,&g_513,(void*)0},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{(void*)0,&g_513,&g_513,&g_513,(void*)0,&g_513},{&g_513,&g_513,(void*)0,&g_513,&g_513,&g_513},{&g_513,(void*)0,(void*)0,&g_513,(void*)0,&g_513}},{{&g_513,&g_513,&g_513,(void*)0,&g_513,(void*)0},{&g_513,(void*)0,&g_513,&g_513,(void*)0,&g_513},{&g_513,&g_513,&g_513,&g_513,(void*)0,(void*)0},{&g_513,&g_513,&g_513,(void*)0,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513}},{{(void*)0,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,(void*)0,(void*)0,&g_513,&g_513,(void*)0},{&g_513,&g_513,&g_513,&g_513,(void*)0,&g_513},{&g_513,&g_513,(void*)0,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,&g_513,&g_513},{&g_513,&g_513,&g_513,&g_513,(void*)0,&g_513},{&g_513,(void*)0,&g_513,&g_513,&g_513,&g_513}}};
    float **l_1029 = &l_1028[1][2][0];
    int64_t ** const *l_1034 = (void*)0;
    int64_t ** const **l_1033[2][1];
    int64_t ** const ***l_1032 = &l_1033[0][0];
    int64_t **** const *l_1035 = (void*)0;
    int32_t l_1040 = 8L;
    const int32_t **l_1045 = (void*)0;
    const int32_t *l_1047[10][7] = {{&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2],&g_301[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    const int32_t **l_1046[8][5][6] = {{{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]}},{{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]}},{{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[0][3]},{&l_1047[6][0],&l_1047[6][0],&l_1047[0][3],&l_1047[6][0],&l_1047[6][0],&l_1047[6][0]}},{{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]}},{{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]}},{{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]}},{{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]}},{{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]},{(void*)0,(void*)0,&l_1047[6][0],(void*)0,(void*)0,&l_1047[6][0]}}};
    int32_t l_1049 = 0x27A5B151L;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_1033[i][j] = &l_1034;
    }
    g_1048 = ((((*l_1029) = l_1028[4][0][5]) == ((safe_sub_func_uint64_t_u_u(p_51, 0UL)) , (void*)0)) , func_53((g_513 = (l_1032 != (l_1035 = l_1035))), (safe_mul_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(l_1040, ((safe_add_func_int8_t_s_s(((((safe_lshift_func_int16_t_s_u(((p_51 == (*g_75)) , g_596[3]), 0)) != l_1040) && l_1040) , p_51), p_51)) >= 0xDBAC251DD3BCB861LL))), p_52))));
    p_52 |= l_1049;
    return p_51;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const uint32_t * func_53(float  p_54, const int32_t  p_55)
{ /* block id: 435 */
    const uint32_t *l_1026 = &g_152;
    return l_1026;
}


/* ------------------------------------------ */
/* 
 * reads : g_33 g_34 g_1017 g_596
 * writes: g_1011 g_1017
 */
static float  func_56(const uint16_t  p_57, float  p_58)
{ /* block id: 427 */
    int64_t *l_1002[5][8] = {{&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0},{&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0},{&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0},{&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0},{&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0,&g_290[3],(void*)0}};
    int64_t **l_1001 = &l_1002[1][5];
    int64_t ***l_1000 = &l_1001;
    int64_t *****l_1007 = (void*)0;
    uint64_t **l_1008 = (void*)0;
    uint64_t *l_1010 = &g_164[0][1][5];
    uint64_t **l_1009 = &l_1010;
    uint16_t *l_1013 = &g_150;
    const int32_t l_1014[8][10] = {{(-1L),(-1L),3L,0x0862A550L,1L,0x3BA5515DL,0L,0L,0x3BA5515DL,1L},{0L,0x0862A550L,0x0862A550L,0L,0x44E5A5ABL,0xFA79F065L,0x3C30F417L,(-1L),2L,(-1L)},{0x78FC8BDBL,0x44E5A5ABL,(-1L),0x3BA5515DL,2L,0x3C30F417L,0x0862A550L,0x3C30F417L,2L,0x3BA5515DL},{(-1L),0x78FC8BDBL,(-1L),0L,0L,0x95536B59L,2L,8L,0x3BA5515DL,(-1L)},{(-1L),0x95536B59L,0x44E5A5ABL,0x0862A550L,8L,1L,1L,8L,0x0862A550L,0x44E5A5ABL},{(-1L),(-1L),(-1L),8L,0x3BA5515DL,0x78FC8BDBL,3L,0x3C30F417L,8L,0xFA79F065L},{1L,2L,(-1L),(-1L),3L,0L,3L,(-1L),(-1L),2L},{8L,(-1L),0x0862A550L,(-1L),0xFA79F065L,0L,1L,0L,8L,0x3C30F417L}};
    int32_t *l_1015[4] = {&g_521[0],&g_521[0],&g_521[0],&g_521[0]};
    int32_t l_1016 = 0xFEF19B5EL;
    int8_t **l_1018 = &g_1017;
    uint32_t l_1023 = 18446744073709551615UL;
    int8_t l_1024 = 0x9BL;
    int32_t l_1025[8] = {(-8L),0x3B9E0841L,(-8L),0x3B9E0841L,(-8L),0x3B9E0841L,(-8L),0x3B9E0841L};
    int i, j;
    l_1016 = (0UL | (safe_add_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s((g_33 >= (safe_lshift_func_int8_t_s_u((l_1000 == &l_1001), ((safe_rshift_func_uint16_t_u_s((safe_rshift_func_uint16_t_u_s((l_1007 == (void*)0), ((((g_1011 = (l_1009 = l_1008)) != ((l_1013 == &g_150) , &l_1010)) == 1UL) != p_57))), l_1014[7][3])) == p_57)))), p_57)) > g_34[1]), 3)), l_1014[1][8])));
    l_1025[5] = (((((*l_1018) = g_1017) != (void*)0) > (safe_lshift_func_uint16_t_u_s((safe_add_func_uint16_t_u_u(0x758AL, (0x0DL && (p_57 & ((((l_1023 , (((((((p_57 <= 0x89D246303165502ALL) != 0xCCD3L) , p_57) ^ 0UL) , 0x8E527845L) > l_1024) , 1L)) <= p_57) , (void*)0) != l_1013))))), 10))) < p_57);
    return g_596[3];
}


/* ------------------------------------------ */
/* 
 * reads : g_991
 * writes:
 */
static float  func_59(uint32_t * p_60, uint16_t  p_61, uint32_t  p_62, int8_t  p_63)
{ /* block id: 417 */
    uint32_t l_984 = 0x709463B2L;
    int32_t l_985 = 0x84967500L;
    int64_t l_986 = 0x0132CD35965C993ALL;
lbl_989:
    l_986 |= (l_985 = l_984);
    for (l_986 = 0; (l_986 != 7); ++l_986)
    { /* block id: 422 */
        int64_t l_990 = 2L;
        if (l_984)
            goto lbl_989;
        l_990 &= l_986;
    }
    return g_991;
}


/* ------------------------------------------ */
/* 
 * reads : g_75 g_34 g_76 g_33 g_101 g_99 g_114 g_124 g_290 g_301 g_150 g_152 g_182 g_355 g_374 g_164 g_365 g_289 g_161 g_360 g_32 g_515 g_5 g_496 g_615 g_272 g_324 g_521 g_645 g_359 g_375 g_376 g_728 g_222 g_358 g_352 g_31 g_567 g_461 g_112 g_850 g_205 g_596 g_918 g_942
 * writes: g_99 g_101 g_33 g_76 g_152 g_161 g_324 g_150 g_352 g_355 g_358 g_359 g_360 g_365 g_374 g_290 g_289 g_461 g_164 g_496 g_513 g_515 g_205 g_567 g_596 g_615 g_272 g_222 g_521 g_850 g_918 g_942
 */
static uint32_t * func_64(int32_t  p_65, int32_t  p_66)
{ /* block id: 24 */
    uint32_t *l_81 = &g_76;
    uint16_t *l_514 = &g_515;
    int32_t l_518 = 0L;
    int32_t *l_519 = (void*)0;
    int32_t *l_520[4][3][3] = {{{&l_518,(void*)0,(void*)0},{&g_521[0],&g_521[0],(void*)0},{(void*)0,&l_518,(void*)0}},{{&l_518,(void*)0,(void*)0},{&g_521[0],&g_521[0],(void*)0},{(void*)0,&l_518,(void*)0}},{{&l_518,(void*)0,(void*)0},{&g_521[0],&g_521[0],(void*)0},{(void*)0,&l_518,(void*)0}},{{&l_518,(void*)0,(void*)0},{&g_521[0],&g_521[0],(void*)0},{(void*)0,&l_518,(void*)0}}};
    uint64_t l_524 = 0UL;
    uint8_t **l_569[4][1];
    int32_t **l_625 = (void*)0;
    int64_t l_672 = 0x84FA94CC8FBD244ELL;
    uint64_t ****l_691 = (void*)0;
    int32_t ***l_723 = &g_272;
    uint64_t l_734 = 8UL;
    const int64_t **l_775 = &g_729;
    uint64_t *l_795 = &l_734;
    int64_t *l_838 = &g_2[7][1];
    int64_t **l_837[4][2][6] = {{{&l_838,&l_838,&l_838,&l_838,&l_838,&l_838},{&l_838,&l_838,&l_838,&l_838,&l_838,&l_838}},{{(void*)0,(void*)0,&l_838,&l_838,&l_838,(void*)0},{&l_838,&l_838,&l_838,&l_838,&l_838,&l_838}},{{(void*)0,&l_838,&l_838,&l_838,(void*)0,(void*)0},{&l_838,&l_838,&l_838,&l_838,&l_838,&l_838}},{{&l_838,&l_838,&l_838,&l_838,&l_838,&l_838},{(void*)0,(void*)0,&l_838,&l_838,&l_838,(void*)0}}};
    int64_t ***l_836 = &l_837[1][0][4];
    int32_t *l_944 = (void*)0;
    uint64_t l_964 = 18446744073709551615UL;
    uint16_t l_976 = 4UL;
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
            l_569[i][j] = (void*)0;
    }
lbl_868:
    p_66 &= (safe_sub_func_uint32_t_u_u(((*l_81) = (safe_div_func_int32_t_s_s(0L, func_72(g_75, func_77(l_81, g_34[0], &g_76))))), (((*l_514)++) > (((p_65 & (((void*)0 == &l_514) , l_518)) ^ 65527UL) != l_518))));
    if (((safe_mod_func_uint64_t_u_u(l_524, (2L || p_66))) & p_66))
    { /* block id: 209 */
        uint64_t l_528 = 1UL;
        uint64_t *l_539 = &g_205;
        uint64_t **l_538 = &l_539;
        uint64_t ***l_537 = &l_538;
        uint64_t ****l_536 = &l_537;
        int32_t l_542 = 0xE14E4104L;
        uint16_t *l_547 = &g_515;
        int32_t l_548 = 0xD68A6137L;
        int32_t l_551 = 0x130FEA67L;
        int32_t l_555 = 0x26967BBAL;
        int32_t l_556[3];
        uint8_t **l_568 = &g_461;
        const int32_t *l_575 = &g_5;
        uint64_t * const ***l_690 = (void*)0;
        int32_t ***l_720 = &l_625;
        uint32_t *l_771 = &g_222;
        int16_t l_851 = 0xF310L;
        uint8_t l_867[6][6] = {{0UL,0x6FL,0x4FL,0x4FL,0x6FL,0UL},{255UL,0UL,0x4FL,0UL,255UL,255UL},{0x10L,0UL,0x10L,0x4FL,255UL,0x4FL},{0x4FL,255UL,0x4FL,0x10L,0x10L,0x4FL},{0UL,0UL,0x10L,0x6FL,0x10L,0UL},{0x10L,255UL,0x6FL,0x6FL,255UL,0x10L}};
        int8_t l_916 = 0x22L;
        int64_t ** const l_924 = &l_838;
        uint32_t l_937 = 0xEB8B305AL;
        int32_t *l_940 = (void*)0;
        int i, j;
        for (i = 0; i < 3; i++)
            l_556[i] = 0xDBCFCD8AL;
        for (p_66 = 0; (p_66 <= 8); p_66 += 1)
        { /* block id: 212 */
            int8_t l_543 = 0xA0L;
            int32_t l_549 = 0x0E7C56A3L;
            int32_t l_550[8] = {0x0CD45CBEL,0x220D12E5L,0x0CD45CBEL,0x220D12E5L,0x0CD45CBEL,0x220D12E5L,0x0CD45CBEL,0x220D12E5L};
            int8_t l_562 = 0xC8L;
            uint64_t l_563 = 0x3491409476E90A6FLL;
            int16_t *l_633[3];
            int32_t *l_673 = &l_556[2];
            int i;
            for (i = 0; i < 3; i++)
                l_633[i] = (void*)0;
            for (p_65 = 1; (p_65 <= 8); p_65 += 1)
            { /* block id: 215 */
                uint8_t l_527 = 0xA2L;
                int8_t *l_529 = (void*)0;
                uint64_t ****l_540 = (void*)0;
                int32_t l_541 = 0x77567B50L;
                int32_t l_559[7] = {0xD78DBAA0L,0xA05F558AL,0xD78DBAA0L,0xD78DBAA0L,0xA05F558AL,0xD78DBAA0L,0xD78DBAA0L};
                int64_t l_574 = 1L;
                uint32_t *l_674 = &g_76;
                uint8_t ***l_677 = &l_568;
                uint64_t * const ****l_689[3][5];
                float *l_692 = (void*)0;
                int8_t *l_697 = &l_562;
                float l_706 = 0xF.2F6242p+7;
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 5; j++)
                        l_689[i][j] = (void*)0;
                }
                if (((-3L) > (g_161 = (+(p_65 != ((!g_290[8]) && (l_528 ^= l_527)))))))
                { /* block id: 218 */
                    uint8_t l_544[2];
                    int32_t l_554 = 1L;
                    int32_t l_558 = 0xDD33B24EL;
                    int32_t l_560 = 0x1389CB0DL;
                    int32_t l_561 = (-1L);
                    int i;
                    for (i = 0; i < 2; i++)
                        l_544[i] = 0x0AL;
                    for (g_360 = 0; (g_360 <= 8); g_360 += 1)
                    { /* block id: 221 */
                        if (p_66)
                            break;
                    }
                    if ((safe_mul_func_uint8_t_u_u((((g_114[1][1] <= (((**l_538) = ((l_541 = (safe_sub_func_int16_t_s_s(((l_527 >= p_65) , (safe_mul_func_int8_t_s_s(p_65, (18446744073709551606UL < (l_536 == l_540))))), p_65))) ^ (((l_542 ^ l_543) && g_515) && (-10L)))) <= l_544[0])) <= p_65) , l_541), p_65)))
                    { /* block id: 226 */
                        int32_t **l_545 = (void*)0;
                        int32_t **l_546 = &l_519;
                        (*l_546) = &l_541;
                        (*l_519) |= (&g_358 != l_547);
                    }
                    else
                    { /* block id: 229 */
                        int16_t l_552 = 1L;
                        int32_t l_553 = 0xD7277B6CL;
                        int32_t l_557[2][5] = {{0L,0L,0x8068A80DL,0L,0L},{0L,0L,0x8068A80DL,0L,0L}};
                        int i, j;
                        l_563++;
                        if (p_66)
                            continue;
                    }
                }
                else
                { /* block id: 233 */
                    for (l_548 = 8; (l_548 >= 1); l_548 -= 1)
                    { /* block id: 236 */
                        uint8_t ***l_566 = (void*)0;
                        g_567 = &g_461;
                        if (p_66)
                            break;
                        if (l_562)
                            continue;
                    }
                }
                for (g_515 = 0; (g_515 <= 8); g_515 += 1)
                { /* block id: 244 */
                    uint64_t l_594 = 0x111C21C4D97326A4LL;
                    int16_t *l_595 = &g_596[3];
                    int32_t l_612 = 8L;
                    if ((((l_568 != l_569[0][0]) | (safe_mul_func_uint16_t_u_u((g_360 &= (safe_sub_func_uint16_t_u_u((((l_574 <= ((&p_66 != l_575) >= ((safe_div_func_uint64_t_u_u((safe_mod_func_int64_t_s_s((safe_div_func_int32_t_s_s((p_65 <= (safe_sub_func_int32_t_s_s((((safe_sub_func_uint16_t_u_u(((l_550[3] = (((safe_rshift_func_int8_t_s_u((((safe_lshift_func_uint16_t_u_u(p_65, 12)) | (((safe_div_func_int16_t_s_s(((*l_595) = (((1UL <= (((g_34[1] , 0x84L) , 5UL) & l_562)) ^ 0x26BFL) < l_594)), g_290[5])) < l_562) | l_543)) | p_65), 0)) | p_65) < 0L)) | 7UL), 3UL)) , g_34[0]) , l_563), 0L))), p_65)), 18446744073709551615UL)), g_290[8])) , 0xBEE5L))) >= g_365) & (*l_575)), g_34[1]))), p_65))) > l_594))
                    { /* block id: 248 */
                        uint32_t l_607 = 18446744073709551613UL;
                        uint32_t **l_611 = &g_75;
                        uint32_t ***l_610 = &l_611;
                        int32_t ***l_624[8] = {&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272,&g_272};
                        uint64_t l_631 = 0UL;
                        int16_t *l_632 = &g_596[9];
                        const int64_t *l_637 = &g_290[3];
                        const int64_t **l_636 = &l_637;
                        const int64_t ***l_635 = &l_636;
                        const int64_t ****l_634 = &l_635;
                        const int64_t *****l_638 = &l_634;
                        int i;
                        l_555 = ((-1L) || (((l_612 = (safe_sub_func_float_f_f(0x4.8A15FDp+99, (safe_sub_func_float_f_f(((((((p_66 , ((safe_mul_func_uint8_t_u_u((((safe_lshift_func_uint16_t_u_u((g_301[3] <= p_66), l_607)) > l_594) ^ (((safe_sub_func_int8_t_s_s(((g_290[3] || (((*l_610) = &g_352[0]) != (void*)0)) , (-5L)), 6UL)) <= g_301[0]) , 0UL)), 255UL)) != p_66)) | 0x87L) , l_607) < 0x3.0309DAp+39) , 0x0.D8C71Cp-93) < g_496[1]), 0x0.6p-1))))) , (-8L)) & g_99));
                        l_555 = ((safe_div_func_uint32_t_u_u(g_615, p_66)) < ((safe_rshift_func_int16_t_s_u((safe_mod_func_uint16_t_u_u((p_65 && (l_612 <= 0xB8C2L)), 0x0CB2L)), ((safe_lshift_func_int16_t_s_u((g_596[3] = (safe_div_func_int64_t_s_s(((l_625 = g_272) != &l_575), (~(safe_sub_func_int32_t_s_s(((safe_mod_func_int16_t_s_s((p_65 || l_574), l_631)) != p_65), p_65)))))), g_33)) | 0xEEAFL))) != 0xDAL));
                        l_551 = (((g_324[0] && (g_32 <= (*l_575))) , ((l_632 != l_633[1]) , (((*l_638) = l_634) != ((safe_mul_func_float_f_f((safe_div_func_float_f_f((g_521[0] != ((0x3.7DD741p+28 < (safe_mul_func_float_f_f((g_152 > p_65), g_152))) != 0xD.DBD754p+84)), (-0x1.Bp+1))), g_33)) , g_645)))) == 0x1.DBB0C3p-67);
                    }
                    else
                    { /* block id: 257 */
                        int32_t l_646 = 0xE9A13F37L;
                        uint32_t l_666 = 18446744073709551615UL;
                        l_556[2] = (l_646 , (((safe_sub_func_int16_t_s_s(0xD45CL, ((p_66 != (safe_mul_func_int16_t_s_s(5L, (safe_sub_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_int16_t_s_u((safe_add_func_int32_t_s_s(((((*g_75) ^ (l_666 |= ((~p_65) | (safe_rshift_func_int8_t_s_s(l_594, (safe_lshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_u(((*l_595) = (*l_575)), 13)), 15))))))) <= ((((~(((safe_sub_func_int32_t_s_s((safe_mul_func_int16_t_s_s(g_114[1][1], 9L)), p_66)) | g_359) || l_612)) != p_65) ^ p_66) == l_559[3])) , 8L), p_65)), 11)) && 0x32C7L), p_65)), 0xB11FL))))) < 0x6724248BL))) || 65526UL) | 0x46C6L));
                        l_672 ^= p_66;
                        l_673 = &p_66;
                    }
                    return l_674;
                }
                l_541 = (safe_mul_func_float_f_f(((l_559[6] >= ((l_677 != (void*)0) == ((g_513 = (safe_add_func_float_f_f((safe_add_func_float_f_f((((!(((((((safe_add_func_int32_t_s_s((*l_673), (*l_575))) < (l_555 &= ((p_65 > ((safe_mod_func_int64_t_s_s((0x91B87DE84B6F2DE6LL ^ ((l_690 = (void*)0) != ((p_66 >= 7UL) , l_691))), l_541)) >= 0x56L)) , l_541))) && 0UL) , (void*)0) == (void*)0) != (*l_673)) | g_114[3][2])) ^ g_32) , g_515), 0x6.2p+1)), (-0x6.1p-1)))) < p_65))) , 0x1.8p-1), (-0x3.Cp-1)));
                l_541 &= ((safe_mod_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(((((((*l_673) |= (&l_563 == (*g_375))) & ((*l_697) = l_559[6])) == (((p_65 , ((p_66 , (((((((--(*l_547)) > (((safe_lshift_func_uint16_t_u_s((0x85D63546L == l_527), 10)) , (g_596[1] = 0x180EL)) | (safe_div_func_int32_t_s_s((safe_sub_func_int32_t_s_s(p_65, (*l_575))), (-5L))))) != g_33) == p_66) > (*l_575)) < g_182) , p_66)) , l_559[6])) > p_65) || p_66)) < p_65) , 4294967288UL), p_66)), g_76)) <= (*l_575));
            }
        }
        for (g_76 = (-24); (g_76 < 7); g_76++)
        { /* block id: 279 */
            int32_t *l_709 = (void*)0;
            int8_t *l_712 = (void*)0;
            int8_t *l_713 = (void*)0;
            int8_t *l_714 = &g_615;
            int32_t ***l_721 = &g_272;
            int32_t ****l_722[9] = {&l_720,&l_720,&l_720,&l_720,&l_720,&l_720,&l_720,&l_720,&l_720};
            const int64_t *l_725 = (void*)0;
            const int64_t **l_724[2][7][7] = {{{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725}},{{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725,&l_725,&l_725,&l_725,&l_725}}};
            const int64_t ***l_726 = &l_724[1][6][1];
            uint64_t *l_727[5] = {&l_528,&l_528,&l_528,&l_528,&l_528};
            uint16_t **l_737 = &l_514;
            int i, j, k;
            l_709 = &p_66;
            if ((safe_rshift_func_int8_t_s_u(((*l_714) = g_289), (safe_lshift_func_int16_t_s_u(((~(*l_575)) == (safe_div_func_int64_t_s_s(((l_723 = (l_721 = l_720)) == &g_272), p_65))), ((*l_514) = (((*l_726) = l_724[1][6][1]) == (((g_161 |= ((((*l_538) = l_727[4]) == (void*)0) <= p_66)) > g_324[2]) , g_728))))))))
            { /* block id: 288 */
                int8_t l_730 = 0x2FL;
                int32_t l_731 = (-1L);
                int32_t l_732 = 1L;
                int32_t l_733 = 1L;
                ++l_734;
            }
            else
            { /* block id: 290 */
                uint16_t ***l_738 = &l_737;
                uint16_t **l_740 = &l_547;
                uint16_t ***l_739 = &l_740;
                float *l_741 = &g_513;
                int32_t l_748 = (-1L);
                int32_t l_749 = 0x320BD02FL;
                int32_t **l_768 = (void*)0;
                (*l_739) = ((*l_738) = l_737);
                (*l_741) = g_290[3];
                l_749 &= (safe_lshift_func_uint8_t_u_u(p_66, (g_99 &= (p_65 <= (safe_rshift_func_uint16_t_u_u(((**l_737)--), (l_748 = p_66)))))));
                for (g_205 = 0; (g_205 < 11); g_205 = safe_add_func_uint64_t_u_u(g_205, 3))
                { /* block id: 300 */
                    uint8_t l_769[1][4] = {{0xD5L,0xD5L,0xD5L,0xD5L}};
                    uint32_t *l_770 = &g_222;
                    int i, j;
                    l_748 ^= ((((p_66 , ((((((safe_sub_func_uint64_t_u_u((l_749 && ((safe_add_func_int32_t_s_s(p_65, ((safe_div_func_uint16_t_u_u((((p_66 , &g_567) != &l_568) , (safe_add_func_int32_t_s_s(((((*l_770) &= (~((*l_714) = (safe_add_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((safe_unary_minus_func_uint8_t_u((safe_mul_func_uint16_t_u_u(0UL, p_65)))), ((g_272 = l_768) == &g_126[0]))) ^ l_769[0][1]), g_164[0][1][1]))))) , (void*)0) == (void*)0), 0x7133053DL))), p_66)) || g_99))) & (-1L))), p_66)) , p_65) > 1UL) , g_114[3][2]) >= p_66) <= g_358)) | (-3L)) == 6L) >= p_66);
                    return l_771;
                }
            }
        }
        for (l_551 = 0; (l_551 <= 2); l_551 += 1)
        { /* block id: 311 */
            uint8_t l_780 = 4UL;
            int32_t l_831 = 0L;
            int32_t l_854 = 0x23BE8044L;
            int32_t l_857[2][9][10] = {{{1L,0x821C4A95L,(-1L),0xFAC63E6AL,0x1101178DL,6L,0x25235C9BL,0x930FD067L,0x66F23120L,(-5L)},{0x821C4A95L,0x57448A87L,(-8L),2L,0xCF3EE9E5L,0xCF3EE9E5L,2L,(-8L),0x57448A87L,0x821C4A95L},{(-8L),0L,0x57448A87L,(-1L),0x6E59F310L,1L,(-1L),(-1L),(-1L),(-5L)},{0xE07128AEL,0x1101178DL,(-6L),0L,0x6E59F310L,0xED06A91CL,(-3L),(-1L),0x7EF154F9L,0xEDB568CEL},{0x6E59F310L,(-1L),0xEDB568CEL,0x66F23120L,0x1101178DL,0L,0xE07128AEL,0xCF3EE9E5L,1L,0xCF3EE9E5L},{1L,(-9L),0xA98650CFL,(-6L),0xA98650CFL,(-9L),1L,0L,(-5L),0x930FD067L},{2L,0x7EF154F9L,1L,0x7D3A69D1L,0x25235C9BL,0x598F5407L,(-1L),6L,0x66F23120L,0L},{(-6L),0x7EF154F9L,0x66F23120L,(-8L),0xFAC63E6AL,0xD6A183ACL,1L,0x930FD067L,0x821C4A95L,1L},{0L,(-9L),0x7D3A69D1L,0x1101178DL,0xED06A91CL,6L,0xE07128AEL,0xE07128AEL,6L,0xED06A91CL}},{{0xD6A183ACL,(-1L),(-1L),0xD6A183ACL,(-5L),0x7D3A69D1L,(-3L),(-1L),(-1L),0xA98650CFL},{0x7EF154F9L,0x1101178DL,(-1L),0xCF3EE9E5L,1L,0xA98650CFL,(-1L),(-3L),(-1L),0xE07128AEL},{0xFAC63E6AL,0L,0x930FD067L,0xD6A183ACL,0xCF3EE9E5L,0x6E59F310L,(-5L),0x598F5407L,6L,2L},{1L,0xFAC63E6AL,0x821C4A95L,0x1101178DL,0x57448A87L,(-1L),0x57448A87L,0x1101178DL,0x821C4A95L,0xFAC63E6AL},{0x25235C9BL,0xEDB568CEL,1L,(-8L),0xD6A183ACL,(-5L),(-9L),0xED06A91CL,0x66F23120L,(-1L)},{(-3L),0xD70B3048L,0L,0x7D3A69D1L,0xE07128AEL,(-5L),(-8L),(-6L),(-5L),0x7EF154F9L},{0x25235C9BL,(-1L),(-1L),(-6L),0x8B134A9EL,(-1L),6L,1L,1L,1L},{1L,0xA98650CFL,0x6E59F310L,0x66F23120L,0x66F23120L,0x6E59F310L,0xA98650CFL,1L,0x7EF154F9L,(-5L)},{0xFAC63E6AL,(-3L),0xCF3EE9E5L,0L,(-1L),0xA98650CFL,0x7EF154F9L,0x7D3A69D1L,(-1L),0x66F23120L}}};
            int64_t l_891 = 0x6C8A2ED0F5CF4A2CLL;
            int16_t * const l_900 = &l_851;
            uint32_t *l_917 = &g_152;
            uint8_t ***l_923 = (void*)0;
            int i, j, k;
            for (l_555 = 1; (l_555 <= 9); l_555 += 1)
            { /* block id: 314 */
                uint8_t ** const *l_776 = &l_568;
                int32_t l_779 = 0L;
                uint64_t *****l_784[1][5][3] = {{{&l_536,&l_536,&l_536},{&l_691,&l_536,&l_691},{&l_536,&l_536,&l_536},{&l_691,&l_536,&l_691},{&l_536,&l_536,&l_536}}};
                uint16_t *l_823 = &g_358;
                uint32_t l_847 = 0UL;
                int32_t l_852 = (-7L);
                int32_t l_853 = 0x8CFC5E6AL;
                int16_t l_856 = (-1L);
                int32_t l_860[8][3][9] = {{{(-7L),(-2L),(-5L),(-2L),0x3F22E422L,1L,0x3E9C2667L,0L,0xDE896473L},{0xCF10E406L,1L,(-5L),1L,1L,0xEB376952L,1L,0xDE896473L,(-1L)},{0x28AF9080L,1L,0xD391EBC0L,0x03F35300L,0xAA17BF59L,0xE2BC05BDL,5L,0xDE896473L,0xCF0FD5D0L}},{{0x74B023BBL,0xAA17BF59L,0xF4349C3CL,0xE2BC05BDL,0xCF10E406L,0xE2BC05BDL,0xF4349C3CL,0xAA17BF59L,0x74B023BBL},{0xCF0FD5D0L,0xDE896473L,5L,0xE2BC05BDL,0xAA17BF59L,0x03F35300L,0xD391EBC0L,1L,0x28AF9080L},{(-1L),0xDE896473L,0xF4349C3CL,0x03F35300L,(-5L),5L,(-4L),(-5L),0x28AF9080L}},{{0xCF0FD5D0L,0xAA17BF59L,0xD391EBC0L,5L,(-7L),0x7F8DDD6EL,(-4L),1L,0x74B023BBL},{0x74B023BBL,1L,(-4L),0x7F8DDD6EL,(-7L),5L,0xD391EBC0L,0xAA17BF59L,0xCF0FD5D0L},{0x28AF9080L,(-5L),(-4L),5L,(-5L),0x03F35300L,0xF4349C3CL,0xDE896473L,(-1L)}},{{0x28AF9080L,1L,0xD391EBC0L,0x03F35300L,0xAA17BF59L,0xE2BC05BDL,5L,0xDE896473L,0xCF0FD5D0L},{0x74B023BBL,0xAA17BF59L,0xF4349C3CL,0xE2BC05BDL,0xCF10E406L,0xE2BC05BDL,0xF4349C3CL,0xAA17BF59L,0x74B023BBL},{0xCF0FD5D0L,0xDE896473L,5L,0xE2BC05BDL,0xAA17BF59L,0x03F35300L,0xD391EBC0L,1L,0x28AF9080L}},{{(-1L),0xDE896473L,0xF4349C3CL,0x03F35300L,(-5L),5L,(-4L),(-5L),0x28AF9080L},{0xCF0FD5D0L,0xAA17BF59L,0xD391EBC0L,5L,(-7L),0x7F8DDD6EL,(-4L),1L,0x74B023BBL},{0x74B023BBL,1L,(-4L),0x7F8DDD6EL,(-7L),5L,0xD391EBC0L,0xAA17BF59L,0xCF0FD5D0L}},{{0x28AF9080L,(-5L),(-4L),5L,(-5L),0x03F35300L,0xF4349C3CL,0xDE896473L,(-1L)},{0x28AF9080L,1L,0xD391EBC0L,0x03F35300L,0xAA17BF59L,0xE2BC05BDL,5L,0xDE896473L,0xCF0FD5D0L},{0x74B023BBL,0xAA17BF59L,0xF4349C3CL,0xE2BC05BDL,0xCF10E406L,0xE2BC05BDL,0xF4349C3CL,0xAA17BF59L,0x74B023BBL}},{{0xCF0FD5D0L,0xDE896473L,5L,0xE2BC05BDL,0xAA17BF59L,0x03F35300L,0xD391EBC0L,1L,0x28AF9080L},{(-1L),0xDE896473L,0xF4349C3CL,0x03F35300L,(-5L),5L,(-4L),(-5L),0x28AF9080L},{0xCF0FD5D0L,0xAA17BF59L,0xD391EBC0L,5L,(-7L),0x7F8DDD6EL,(-4L),1L,0x74B023BBL}},{{0x74B023BBL,1L,(-4L),0x7F8DDD6EL,(-7L),5L,0xD391EBC0L,0xAA17BF59L,0xCF0FD5D0L},{0x28AF9080L,(-5L),(-4L),5L,(-5L),0x03F35300L,0xF4349C3CL,0xDE896473L,(-1L)},{(-1L),(-1L),0x8DD6CFB4L,0xFAD3EB6FL,0x1529B3EEL,0x83EEEAE0L,0x1694B197L,0xCF0FD5D0L,0xCC856534L}}};
                uint64_t l_863 = 18446744073709551615UL;
                uint32_t *l_866 = &g_222;
                int i, j, k;
                for (l_518 = 2; (l_518 >= 0); l_518 -= 1)
                { /* block id: 317 */
                    int32_t **l_772 = &l_520[3][1][0];
                    int64_t *l_774 = &g_290[3];
                    int64_t * const *l_773 = &l_774;
                    uint64_t ***** const l_783 = &l_691;
                    int i;
                    (*l_772) = g_352[l_551];
                    l_556[l_518] ^= (((l_773 != l_775) , g_324[l_551]) >= (g_31 >= ((g_365 &= 0x46L) , (l_776 == ((g_596[(l_518 + 3)] = ((*l_575) && (((p_66 > ((safe_lshift_func_int16_t_s_s(((void*)0 != &g_375), 3)) , l_779)) <= p_66) , l_780))) , &g_567)))));
                    for (g_152 = 2; (g_152 <= 9); g_152 += 1)
                    { /* block id: 324 */
                        int8_t *l_796 = &g_365;
                        int8_t *l_797 = &g_615;
                        int i;
                        g_521[g_152] |= (safe_lshift_func_int8_t_s_s(p_65, ((((p_66 , l_783) != l_784[0][0][0]) , ((*l_797) &= ((p_66 , 0x3AL) , ((*l_796) = (safe_lshift_func_int8_t_s_s(g_33, (((safe_lshift_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u(((((0xB068L ^ (l_795 != (void*)0)) , (void*)0) == (*g_567)) & p_65), l_779)), g_515)), 4)) & p_66) == p_66))))))) != 0UL)));
                        return g_352[l_518];
                    }
                    if (p_66)
                        continue;
                }
                if (((safe_mod_func_uint16_t_u_u(((*l_823) = (safe_div_func_int32_t_s_s((safe_sub_func_uint64_t_u_u((safe_unary_minus_func_int8_t_s((safe_rshift_func_int8_t_s_u((safe_div_func_int16_t_s_s((((safe_add_func_uint64_t_u_u(0UL, 1UL)) >= ((~(safe_mul_func_uint16_t_u_u(0x92F2L, g_324[l_551]))) < 0L)) , ((((p_65 >= p_66) > (safe_rshift_func_int16_t_s_u(((--g_150) >= ((*l_514) = (safe_mul_func_int8_t_s_s(((p_66 == (~(((*l_771) = ((*g_75)--)) , p_66))) > 9L), p_65)))), 14))) < g_365) && p_66)), (*l_575))), 5)))), l_780)), p_65))), p_65)) != p_65))
                { /* block id: 337 */
                    uint64_t l_849 = 0xFAEB98837B24E157LL;
                    for (l_542 = 0; (l_542 <= 2); l_542 += 1)
                    { /* block id: 340 */
                        int64_t *l_835 = (void*)0;
                        int64_t **l_834 = &l_835;
                        int64_t ***l_833 = &l_834;
                        int64_t ****l_832[4];
                        int32_t l_848 = 0xB08EC6DEL;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_832[i] = &l_833;
                        g_850 = ((+((safe_sub_func_uint64_t_u_u((safe_mul_func_int8_t_s_s(((((++(*g_75)) || 0x4C047F88L) <= (((l_831 &= p_66) , ((l_836 = (void*)0) == &g_728)) || (l_779 , (((safe_div_func_int8_t_s_s(0xD7L, (safe_rshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(p_66, 0x14L)), 8)))) ^ ((*l_514) = (((((safe_mul_func_uint8_t_u_u(p_66, g_324[l_551])) , 0x7F724559L) , p_65) > 0xD8L) , p_65))) == l_847)))) && p_65), l_848)), g_112)) >= l_849)) , &g_521[9]);
                    }
                }
                else
                { /* block id: 347 */
                    int32_t l_855 = (-1L);
                    int32_t l_858 = (-4L);
                    int32_t l_859 = 0x892AEDE2L;
                    int32_t l_861 = 0x272A664BL;
                    int32_t l_862 = (-2L);
                    int i;
                    l_863++;
                    return l_866;
                }
                (*g_850) |= l_779;
                for (l_831 = 9; (l_831 >= 2); l_831 -= 1)
                { /* block id: 354 */
                    int16_t *l_890[7][2] = {{(void*)0,&l_851},{(void*)0,&l_851},{(void*)0,&l_851},{(void*)0,&l_851},{(void*)0,&l_851},{(void*)0,&l_851},{(void*)0,&l_851}};
                    int32_t l_892 = (-3L);
                    int i, j;
                    (*g_850) |= p_66;
                    for (l_734 = 3; (l_734 <= 9); l_734 += 1)
                    { /* block id: 358 */
                        (*g_850) |= l_867[2][4];
                    }
                    if (p_66)
                        goto lbl_868;
                    l_519 = ((p_66 & (((((!(safe_mul_func_int16_t_s_s(p_66, (g_596[(l_551 + 4)] = (safe_unary_minus_func_uint64_t_u(g_31)))))) , ((void*)0 != &p_66)) < (((((safe_rshift_func_int8_t_s_s(((safe_add_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s(((safe_unary_minus_func_int32_t_s((l_891 = ((p_66 && ((safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(((((safe_add_func_int16_t_s_s((0xAD2E21BEL > (l_857[0][8][4] = (g_101 < (safe_mul_func_int8_t_s_s(1L, p_65))))), g_355)) > l_780) , (void*)0) == l_890[1][1]), 3L)), p_66)) > 4294967295UL)) ^ 0x44DAL)))) , l_892), 0)) > 0x201051E8L), 0xAFL)) == g_324[l_551]), 7)) < p_66) < p_66) , p_65) || 0xFD18L)) || (*l_575)) < (-1L))) , (void*)0);
                }
            }
            for (g_515 = 0; (g_515 <= 9); g_515 += 1)
            { /* block id: 370 */
                int32_t **l_922 = &l_519;
                int i;
                if (((g_324[l_551] && ((((safe_mul_func_uint8_t_u_u((+g_205), p_65)) , ((safe_mod_func_int64_t_s_s((safe_div_func_uint32_t_u_u(((void*)0 != l_900), (((safe_div_func_uint8_t_u_u((safe_add_func_int64_t_s_s((safe_mod_func_int64_t_s_s((p_66 , ((6UL & ((((~(((safe_rshift_func_int16_t_s_s((safe_mod_func_int32_t_s_s((l_556[l_551] = (safe_add_func_uint16_t_u_u(g_34[1], ((safe_rshift_func_uint16_t_u_s(9UL, 10)) && g_324[l_551])))), 4294967292UL)), (*l_575))) || 0x69E5CA473F93535FLL) & g_596[3])) | g_355) , g_324[l_551]) ^ 0xB07B935FL)) , (-1L))), l_916)), g_324[l_551])), p_65)) == 0x3CCBL) & g_290[3]))), 1UL)) <= 0xADE2L)) , g_365) || 0UL)) <= 9UL))
                { /* block id: 372 */
                    if (l_854)
                        break;
                    for (g_101 = 0; (g_101 <= 2); g_101 += 1)
                    { /* block id: 376 */
                        return l_917;
                    }
                }
                else
                { /* block id: 379 */
                    volatile int32_t **l_920 = (void*)0;
                    volatile int32_t **l_921 = &g_918;
                    (*l_921) = g_918;
                }
                (*l_922) = &p_66;
            }
            l_556[l_551] ^= ((((*l_547) ^= ((((((g_324[l_551] >= (8L && g_324[l_551])) >= 0x5D46L) & 0x2861A68599B29CBBLL) && (l_923 == ((((void*)0 == l_924) & ((safe_add_func_int16_t_s_s((safe_mod_func_int16_t_s_s(p_66, p_66)), g_150)) & l_831)) , &g_567))) == p_66) | 0x8654L)) != g_34[0]) == (*l_575));
            for (l_734 = 1; (l_734 <= 9); l_734 += 1)
            { /* block id: 388 */
                int8_t l_931[1][10][9] = {{{0xE8L,0L,0x17L,0L,0x6FL,0x6FL,0L,0x17L,0L},{0x67L,(-10L),(-1L),0x45L,8L,1L,1L,8L,0x45L},{9L,1L,9L,0x34L,0L,0xE8L,0xE9L,0xE9L,0xE8L},{(-1L),(-10L),0x67L,(-10L),(-1L),0x45L,8L,1L,1L},{0x17L,0L,0xE8L,0x34L,0xE8L,0L,0x17L,0L,0x6FL},{0L,0xE5L,(-10L),0x45L,0x54L,0x45L,(-10L),0xE5L,0L},{0L,1L,0xE9L,0L,0x80L,0xE8L,0x80L,0L,0xE9L},{8L,8L,(-1L),0x67L,0xE5L,1L,0L,1L,0xE5L},{0L,0x80L,0x80L,0L,9L,0x6FL,0x34L,0xE9L,0x34L},{0L,0x45L,(-1L),(-1L),0x45L,0L,0x54L,8L,(-1L)}}};
                int32_t l_933 = 1L;
                int32_t l_936 = 0xCE72C278L;
                int i, j, k;
                for (g_99 = 0; (g_99 <= 2); g_99 += 1)
                { /* block id: 391 */
                    int32_t *l_929 = &g_33;
                    int32_t l_932 = 0L;
                    int32_t l_934[1][9] = {{1L,(-1L),(-1L),1L,(-1L),(-1L),1L,(-1L),(-1L)}};
                    int8_t l_935 = 1L;
                    int i, j;
                    l_929 = &l_556[l_551];
                    for (l_780 = 0; (l_780 <= 2); l_780 += 1)
                    { /* block id: 395 */
                        int32_t l_930[5][7] = {{(-2L),(-2L),(-2L),(-2L),(-2L),(-2L),(-2L)},{(-1L),0x9E9088B4L,(-1L),0x9E9088B4L,(-1L),0x9E9088B4L,(-1L)},{(-2L),(-2L),(-2L),(-2L),(-2L),(-2L),(-2L)},{(-1L),0x9E9088B4L,(-1L),0x9E9088B4L,(-1L),0x9E9088B4L,(-1L)},{(-2L),(-2L),(-2L),(-2L),(-2L),(-2L),(-2L)}};
                        int32_t **l_941[4] = {&l_929,&l_929,&l_929,&l_929};
                        int i, j;
                        l_937--;
                        l_519 = (l_940 = &p_66);
                    }
                    g_942 = g_942;
                }
                if (p_66)
                    continue;
            }
        }
    }
    else
    { /* block id: 405 */
        float *l_945 = (void*)0;
        float *l_946 = &g_513;
        int32_t l_956 = 5L;
        uint32_t **l_971 = &l_81;
        uint32_t ***l_970 = &l_971;
        int32_t l_975 = 8L;
        int16_t *l_977[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_978 = 0xD34B2DCFL;
        int32_t l_979 = 0x7DE336D0L;
        int i;
        g_850 = l_944;
        (*l_946) = p_65;
        l_979 ^= ((~(safe_div_func_int16_t_s_s((l_956 = (g_324[0] || (safe_mod_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u(((++(*l_795)) <= ((((safe_mul_func_uint16_t_u_u((+(safe_lshift_func_int8_t_s_s((l_964 <= (((safe_div_func_int32_t_s_s((safe_add_func_uint8_t_u_u((l_975 ^= ((~(&l_81 != ((*l_970) = (void*)0))) , (p_65 & ((safe_add_func_uint16_t_u_u(((g_114[3][2] , p_65) & p_66), (((+l_956) != l_956) , 9UL))) , 0x87L)))), l_976)), p_65)) || 9L) < l_956)), l_956))), g_5)) >= 0x16FDL) ^ (-2L)) == 0xBD0FL)), p_65)), 2)), 0x4755704BFDF24BB5LL)))), l_978))) , l_956);
        p_66 |= (safe_add_func_int32_t_s_s(l_956, (!((0x631FL <= (l_975 = g_164[0][1][5])) == 0x9109L))));
    }
    return &g_222;
}


/* ------------------------------------------ */
/* 
 * reads : g_289 g_358 g_360 g_32
 * writes: g_289 g_358 g_496 g_150 g_360 g_513
 */
static const int32_t  func_72(uint32_t * p_73, uint32_t  p_74)
{ /* block id: 185 */
    int64_t l_480 = 6L;
    int64_t *l_509 = &g_290[3];
    int64_t **l_508 = &l_509;
    int32_t l_510 = 6L;
    int32_t l_511 = 1L;
    l_480 = 0xDFA84A01L;
    for (g_289 = (-21); (g_289 <= (-12)); g_289 = safe_add_func_int8_t_s_s(g_289, 4))
    { /* block id: 189 */
        int64_t *l_498 = (void*)0;
        int64_t **l_497 = &l_498;
        for (g_358 = 0; (g_358 >= 39); ++g_358)
        { /* block id: 192 */
            uint8_t l_493 = 0x6FL;
            float *l_512 = &g_513;
            for (l_480 = 0; (l_480 == (-18)); l_480 = safe_sub_func_int64_t_s_s(l_480, 5))
            { /* block id: 195 */
                uint16_t *l_494 = (void*)0;
                uint16_t *l_495[1];
                int32_t l_507 = 0x05CDAAA2L;
                int i;
                for (i = 0; i < 1; i++)
                    l_495[i] = &g_360;
                l_511 ^= (p_74 < ((safe_add_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((g_150 = ((safe_sub_func_int16_t_s_s(0x5385L, (g_496[7] = l_493))) < l_493)), 11)), p_74)) < ((l_510 ^= (l_497 != (((0x1098F471FAD2DBD1LL && (((safe_sub_func_uint16_t_u_u(((safe_add_func_int64_t_s_s(((p_74 == (g_360++)) <= (safe_rshift_func_int16_t_s_s((18446744073709551615UL > 18446744073709551614UL), g_32))), 0x8A81E49874BAE41FLL)) , l_507), l_480)) < g_32) == p_74)) & p_74) , l_508))) || l_507)));
            }
            (*l_512) = 0x9.7p+1;
        }
    }
    return p_74;
}


/* ------------------------------------------ */
/* 
 * reads : g_76 g_33 g_75 g_101 g_99 g_114 g_124 g_34 g_152 g_290 g_301 g_150 g_182 g_355 g_374 g_161 g_164 g_365 g_289
 * writes: g_99 g_101 g_33 g_76 g_152 g_161 g_324 g_150 g_352 g_355 g_358 g_359 g_360 g_365 g_374 g_290 g_289 g_461 g_164
 */
static uint32_t  func_77(uint32_t * p_78, const uint8_t  p_79, uint32_t * p_80)
{ /* block id: 25 */
    int8_t l_84 = (-8L);
    int16_t l_91 = 1L;
    uint64_t *l_98[1];
    int32_t *l_100 = &g_101;
    int32_t *l_102 = &g_33;
    int32_t l_106 = 0x9ADA34CBL;
    const uint32_t *l_111 = &g_112;
    const uint32_t **l_110[9][6][4] = {{{&l_111,(void*)0,&l_111,(void*)0},{(void*)0,(void*)0,&l_111,(void*)0},{&l_111,&l_111,&l_111,&l_111},{(void*)0,&l_111,(void*)0,&l_111},{(void*)0,&l_111,&l_111,&l_111},{&l_111,&l_111,(void*)0,&l_111}},{{&l_111,&l_111,(void*)0,&l_111},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,(void*)0},{(void*)0,&l_111,(void*)0,&l_111},{&l_111,&l_111,&l_111,(void*)0},{&l_111,(void*)0,(void*)0,&l_111}},{{&l_111,&l_111,&l_111,&l_111},{(void*)0,&l_111,(void*)0,&l_111},{&l_111,(void*)0,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111},{(void*)0,&l_111,(void*)0,&l_111},{&l_111,(void*)0,&l_111,(void*)0}},{{&l_111,&l_111,(void*)0,&l_111},{&l_111,&l_111,&l_111,(void*)0},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111}},{{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,(void*)0},{&l_111,&l_111,(void*)0,&l_111},{&l_111,(void*)0,&l_111,&l_111},{&l_111,&l_111,(void*)0,(void*)0}},{{(void*)0,&l_111,&l_111,&l_111},{&l_111,(void*)0,&l_111,&l_111},{&l_111,(void*)0,(void*)0,&l_111},{(void*)0,&l_111,&l_111,(void*)0},{&l_111,&l_111,(void*)0,&l_111},{&l_111,(void*)0,&l_111,&l_111}},{{&l_111,&l_111,(void*)0,(void*)0},{(void*)0,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,(void*)0,&l_111},{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,(void*)0,&l_111}},{{&l_111,&l_111,&l_111,&l_111},{&l_111,&l_111,&l_111,(void*)0},{(void*)0,&l_111,(void*)0,&l_111},{&l_111,&l_111,&l_111,(void*)0},{&l_111,(void*)0,(void*)0,&l_111},{&l_111,&l_111,&l_111,&l_111}},{{(void*)0,&l_111,(void*)0,&l_111},{&l_111,(void*)0,&l_111,&l_111},{&l_111,&l_111,&l_111,&l_111},{(void*)0,&l_111,(void*)0,&l_111},{&l_111,(void*)0,&l_111,(void*)0},{&l_111,&l_111,(void*)0,&l_111}}};
    const uint32_t *l_113 = &g_114[3][2];
    float l_121[3];
    int16_t l_183 = 4L;
    const float l_217 = 0x1.4p-1;
    int32_t * const *l_219 = (void*)0;
    int32_t * const **l_218 = &l_219;
    int32_t l_242[8];
    uint64_t l_261 = 2UL;
    uint16_t *l_317 = &g_150;
    int64_t *l_372 = &g_290[3];
    int64_t **l_371[4][6][4] = {{{(void*)0,(void*)0,(void*)0,(void*)0},{&l_372,&l_372,&l_372,(void*)0},{&l_372,(void*)0,(void*)0,&l_372},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_372,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_372,&l_372,&l_372,(void*)0},{&l_372,(void*)0,(void*)0,&l_372},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_372,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_372,&l_372,&l_372,(void*)0}},{{&l_372,(void*)0,(void*)0,&l_372},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_372,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_372,&l_372,&l_372,(void*)0},{&l_372,(void*)0,(void*)0,&l_372}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_372,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_372,&l_372,&l_372,(void*)0},{&l_372,(void*)0,(void*)0,&l_372},{(void*)0,(void*)0,(void*)0,(void*)0}}};
    int64_t ***l_370 = &l_371[1][0][2];
    uint8_t *l_460 = (void*)0;
    float l_478 = 0x6.4AA1F1p+29;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_98[i] = (void*)0;
    for (i = 0; i < 3; i++)
        l_121[i] = (-0x1.Fp+1);
    for (i = 0; i < 8; i++)
        l_242[i] = 0L;
    (*l_102) = (((*l_100) ^= (((safe_add_func_int64_t_s_s(l_84, (g_99 = (((safe_lshift_func_int8_t_s_s((safe_add_func_int32_t_s_s(((safe_sub_func_uint64_t_u_u(g_76, ((((l_91 < ((safe_sub_func_uint32_t_u_u((safe_lshift_func_int8_t_s_s(0x5CL, g_76)), 0L)) < p_79)) < ((0x2E219839L ^ (safe_sub_func_uint64_t_u_u((p_79 | l_84), 0UL))) != 6L)) , 1L) ^ l_84))) , (-8L)), 0xA4F77C9AL)), l_84)) <= g_76) & g_33)))) > (*g_75)) >= l_91)) , g_99);
    (*l_100) = p_79;
    if ((!(safe_mul_func_uint16_t_u_u(g_33, (((l_106 != 0x6DBB316AL) , (safe_div_func_uint64_t_u_u((((*l_102) , &g_76) != ((!(g_99 >= ((l_113 = (void*)0) == p_80))) , ((safe_lshift_func_uint16_t_u_s(((*l_102) <= (*l_102)), 6)) , (void*)0))), p_79))) | g_114[3][2])))))
    { /* block id: 31 */
        int32_t ** const l_117 = &l_102;
        int32_t ***l_118 = (void*)0;
        int32_t **l_120 = &l_100;
        int32_t ***l_119 = &l_120;
        int32_t *l_129 = &l_106;
        int64_t l_130[1][9][1] = {{{1L},{0L},{1L},{0L},{1L},{0L},{1L},{0L},{1L}}};
        int32_t l_227 = 0xE777DDB8L;
        int32_t l_241 = 0x0492FC4DL;
        int32_t l_243 = (-2L);
        int32_t l_244 = 0x8CD3D53BL;
        int32_t l_247[4] = {0x0183994FL,0x0183994FL,0x0183994FL,0x0183994FL};
        float l_286 = (-0x1.0p-1);
        int64_t *l_303 = (void*)0;
        int64_t **l_302 = &l_303;
        int64_t ***l_369 = &l_302;
        float l_405[4];
        uint32_t l_414 = 0xA21BD6B7L;
        uint16_t * const *l_439 = &l_317;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_405[i] = (-0x1.8p+1);
        (*l_119) = l_117;
        (*l_129) &= ((**l_120) = ((**l_120) <= (safe_sub_func_int8_t_s_s(p_79, ((((void*)0 != g_124) >= 0x8534C0AFL) >= (((*l_100) == (*l_102)) >= (((safe_sub_func_int32_t_s_s(p_79, 0x914D98EAL)) , &p_78) != &p_78)))))));
        if (l_130[0][0][0])
        { /* block id: 35 */
            int32_t l_147[10][10][2] = {{{0x8D72E106L,2L},{0L,1L},{(-1L),8L},{0x20A7EECAL,0x2AD0072BL},{0xB904D000L,0L},{(-1L),0xEF7775CBL},{0xEAA7743CL,0xD31E0CC6L},{1L,0xD04652DBL},{0x769F70AFL,(-1L)},{0x694C051FL,0x7DD34D88L}},{{0xD31E0CC6L,0x61C1CC91L},{0xB904D000L,(-5L)},{(-1L),(-1L)},{8L,0x694C051FL},{0x6A5DCCCDL,2L},{(-1L),0x8D72E106L},{0x20A7EECAL,0x104EED90L},{0x889BAE77L,0x61C1CC91L},{0x92686379L,0x12CE41FCL},{1L,0xD31E0CC6L}},{{0x7D408D08L,(-2L)},{1L,0x92686379L},{2L,(-8L)},{0xD31E0CC6L,0x54759984L},{0x889BAE77L,0x2AD0072BL},{1L,(-1L)},{0x8D72E106L,0x694C051FL},{(-1L),0x694C051FL},{0x8D72E106L,(-1L)},{1L,0x2AD0072BL}},{{0x889BAE77L,0x54759984L},{0xD31E0CC6L,(-8L)},{2L,0x92686379L},{1L,(-2L)},{0x7D408D08L,0xD31E0CC6L},{1L,0x12CE41FCL},{0x92686379L,0x61C1CC91L},{0x889BAE77L,0x104EED90L},{0x20A7EECAL,0x8D72E106L},{(-1L),2L}},{{8L,0x6A5DCCCDL},{0xBC257202L,0L},{0x104EED90L,(-1L)},{0xB41A0BB3L,0xEAA7743CL},{(-1L),(-1L)},{0x6A5DCCCDL,(-5L)},{(-1L),0xA3BFF2BCL},{0x54759984L,(-1L)},{9L,(-7L)},{(-5L),0x0F15932AL}},{{0xB41A0BB3L,0x7DD34D88L},{0x2AD0072BL,0xBC257202L},{0L,(-1L)},{0L,0x22342146L},{0x1CC493F2L,0L},{0x7C04A76FL,(-8L)},{0x7D408D08L,0x0F15932AL},{3L,0x58545D76L},{0x6A5DCCCDL,0x696D6245L},{0x61C1CC91L,7L}},{{(-1L),3L},{(-1L),(-7L)},{0x696D6245L,1L},{0x7D408D08L,(-1L)},{0x1CF9FCAAL,0x1CC493F2L},{0L,9L},{0x8D72E106L,(-1L)},{0L,0L},{0x104EED90L,(-8L)},{0x1FE2E2A7L,1L}},{{(-5L),0xABCB0199L},{0x22342146L,(-5L)},{0x61C1CC91L,0xB0B10695L},{0x61C1CC91L,(-5L)},{0x22342146L,0xABCB0199L},{(-5L),1L},{0x1FE2E2A7L,(-8L)},{0x104EED90L,0L},{0L,(-1L)},{0x8D72E106L,9L}},{{0L,0x1CC493F2L},{0x1CF9FCAAL,(-1L)},{0x7D408D08L,1L},{0x696D6245L,(-7L)},{(-1L),3L},{(-1L),7L},{0x61C1CC91L,0x696D6245L},{0x6A5DCCCDL,0x58545D76L},{3L,0x0F15932AL},{0x7D408D08L,(-8L)}},{{0x7C04A76FL,0L},{0x1CC493F2L,0x22342146L},{0L,(-1L)},{0L,0xBC257202L},{0x2AD0072BL,0x7DD34D88L},{0xB41A0BB3L,0x0F15932AL},{(-5L),(-7L)},{9L,(-1L)},{0x54759984L,0xA3BFF2BCL},{(-1L),(-5L)}}};
            int8_t *l_160 = &g_161;
            uint64_t *l_163 = &g_164[0][1][5];
            uint32_t **l_168 = &g_75;
            int32_t l_173 = (-2L);
            int32_t l_254 = (-9L);
            int32_t l_255 = 0x618A1754L;
            int32_t l_256 = 0xB4517E80L;
            int32_t l_259 = 4L;
            float l_282 = 0xD.E70FEEp+45;
            int i, j, k;
            for (g_76 = 6; (g_76 <= 39); ++g_76)
            { /* block id: 38 */
                uint64_t **l_135 = &l_98[0];
                int32_t l_141 = 0x6160A43FL;
                int32_t l_245 = (-9L);
                int32_t l_246 = 3L;
                int16_t l_248[5][9] = {{1L,1L,0x317CL,1L,1L,0x317CL,1L,1L,0x317CL},{0xB111L,0xB111L,(-4L),0xB111L,0xB111L,(-4L),0xB111L,0xB111L,(-4L)},{1L,1L,0x317CL,1L,1L,0x317CL,1L,1L,0x317CL},{0xB111L,0xB111L,(-4L),0xB111L,0xB111L,(-4L),0xB111L,0xB111L,(-4L)},{1L,1L,0x317CL,1L,1L,0x317CL,1L,1L,0x317CL}};
                int32_t l_249 = 0L;
                int32_t l_250 = 3L;
                int32_t l_251 = (-1L);
                int32_t l_257 = (-5L);
                int32_t l_258 = 1L;
                int32_t l_260 = 1L;
                int i, j;
                for (l_91 = 23; (l_91 != (-13)); l_91--)
                { /* block id: 41 */
                    uint64_t **l_136 = &l_98[0];
                    l_136 = l_135;
                }
            }
        }
        else
        { /* block id: 102 */
            int64_t ***l_304 = &l_302;
            const uint64_t *l_312[1];
            const uint64_t **l_311 = &l_312[0];
            const int32_t l_318 = 0xAF1C74D2L;
            int32_t l_393 = 3L;
            int i;
            for (i = 0; i < 1; i++)
                l_312[i] = &l_261;
            (*l_304) = l_302;
            for (l_106 = 3; (l_106 >= 0); l_106 -= 1)
            { /* block id: 106 */
                return (*g_75);
            }
            if ((safe_div_func_int64_t_s_s((65530UL || ((((safe_lshift_func_int16_t_s_s((***l_119), 9)) || (safe_rshift_func_int8_t_s_u(((g_34[1] , l_311) == (void*)0), (safe_rshift_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(((void*)0 == l_317), p_79)), 5))))) | ((*l_100) = p_79)) , l_318)), (*l_102))))
            { /* block id: 110 */
                int32_t l_373 = 0x18F8D0A3L;
                for (g_152 = 0; (g_152 <= 0); g_152 += 1)
                { /* block id: 113 */
                    uint32_t l_361 = 0x81D166E6L;
                    int64_t *l_362 = &g_290[3];
                    (**l_119) = (g_114[4][1] , p_78);
                    for (g_161 = 0; (g_161 >= 0); g_161 -= 1)
                    { /* block id: 117 */
                        float *l_323 = (void*)0;
                        int32_t l_343 = 0xC08C91F0L;
                        uint32_t **l_351 = (void*)0;
                        int64_t *l_353 = (void*)0;
                        int64_t *l_354[2][3][3] = {{{&g_290[2],&g_290[2],&l_130[0][1][0]},{&l_130[0][0][0],&g_2[7][1],&l_130[0][0][0]},{&g_290[2],&l_130[0][1][0],&l_130[0][1][0]}},{{&g_2[6][1],&g_2[7][1],&g_2[6][1]},{&g_290[2],&g_290[2],&l_130[0][1][0]},{&l_130[0][0][0],&g_2[7][1],&l_130[0][0][0]}}};
                        uint16_t *l_356 = (void*)0;
                        uint16_t *l_357[1];
                        float *l_363 = (void*)0;
                        float *l_364 = &l_121[2];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                            l_357[i] = &g_358;
                        if (p_79)
                            break;
                        g_365 = ((safe_add_func_float_f_f((safe_add_func_float_f_f((g_324[1] = p_79), ((safe_sub_func_float_f_f((safe_div_func_float_f_f((((-(*l_102)) >= ((*l_364) = (+((((safe_lshift_func_int8_t_s_u(p_79, (safe_div_func_int8_t_s_s((safe_add_func_uint64_t_u_u(((((safe_div_func_int8_t_s_s(g_290[3], (g_360 = (((g_359 = (g_358 = (safe_sub_func_uint16_t_u_u((((g_301[0] & p_79) ^ (g_355 ^= (safe_add_func_int16_t_s_s(((l_343 && (safe_add_func_uint16_t_u_u(((*l_317)--), (((~(safe_lshift_func_uint8_t_u_u(((g_352[0] = &g_152) == p_80), 1))) | g_152) , (*l_100))))) | p_79), g_182)))) != p_79), 0x1A67L)))) == 0xF99BL) , 5L)))) <= l_361) | 0xF902L) || l_361), 0xDB1DC92F31237E01LL)), p_79)))) , l_362) == (void*)0) <= 0xF.CDEE4Ap-42)))) >= (-0x6.Bp+1)), l_343)), 0x0.7p-1)) > l_343))), (-0x7.3p-1))) == (***l_119));
                    }
                    for (g_33 = 0; (g_33 >= 0); g_33 -= 1)
                    { /* block id: 131 */
                        int64_t ****l_368[1][7][8] = {{{&l_304,(void*)0,&l_304,(void*)0,&l_304,&l_304,(void*)0,&l_304},{(void*)0,(void*)0,(void*)0,&l_304,(void*)0,(void*)0,(void*)0,(void*)0},{&l_304,(void*)0,(void*)0,&l_304,&l_304,&l_304,(void*)0,(void*)0},{(void*)0,&l_304,&l_304,&l_304,&l_304,(void*)0,&l_304,&l_304},{&l_304,&l_304,&l_304,(void*)0,(void*)0,&l_304,&l_304,&l_304},{(void*)0,(void*)0,&l_304,(void*)0,(void*)0,(void*)0,(void*)0,&l_304},{(void*)0,(void*)0,(void*)0,&l_304,(void*)0,(void*)0,(void*)0,(void*)0}}};
                        int i, j, k;
                        l_373 ^= (safe_rshift_func_uint8_t_u_s(((l_369 = &l_302) != ((*p_80) , l_370)), 1));
                    }
                }
            }
            else
            { /* block id: 136 */
                volatile uint64_t ** const  volatile **l_378[3][7] = {{&g_374,&g_374,&g_374,&g_374,&g_374,&g_374,&g_374},{(void*)0,&g_374,(void*)0,&g_374,(void*)0,&g_374,(void*)0},{&g_374,&g_374,&g_374,&g_374,&g_374,&g_374,&g_374}};
                int i, j;
                g_374 = g_374;
            }
            for (g_161 = 0; (g_161 >= 0); g_161 -= 1)
            { /* block id: 141 */
                const uint16_t l_394 = 0UL;
                uint8_t *l_399 = &g_99;
                int16_t l_402 = 0xB1CBL;
                float *l_403 = &l_121[1];
                float *l_404 = &l_286;
                (*l_404) = ((safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f((safe_mul_func_float_f_f(((*l_403) = (safe_div_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((l_393 = p_79), 0x3.2C6048p+14)), (l_394 < (((safe_sub_func_int8_t_s_s(1L, (255UL != ((*l_399) &= (safe_mul_func_int8_t_s_s((p_79 < 0x1AL), g_33)))))) , ((safe_div_func_float_f_f(g_355, 0xB.E0F003p+81)) > g_164[0][1][1])) != l_402)))), p_79))), l_402)), p_79)), l_318)), g_365)) != 0xB.5409C4p-44);
                if ((*l_100))
                    break;
                for (l_106 = 0; (l_106 >= 0); l_106 -= 1)
                { /* block id: 149 */
                    (**l_119) = &l_106;
                    return (*g_75);
                }
            }
        }
        for (g_76 = 1; (g_76 <= 7); g_76 += 1)
        { /* block id: 157 */
            int32_t l_406 = 1L;
            int32_t *l_407 = &l_406;
            int32_t *l_408 = &l_242[g_76];
            int32_t *l_409 = &g_289;
            int32_t *l_410 = &g_33;
            int32_t *l_411 = &l_241;
            int32_t *l_412 = &l_247[2];
            int32_t *l_413[10][2][5] = {{{&l_106,&g_289,&g_32,&l_227,&l_243},{(void*)0,(void*)0,(void*)0,&g_31,&l_243}},{{&l_243,&l_406,&l_242[1],(void*)0,&l_406},{&g_32,&g_289,&l_106,(void*)0,&l_247[1]}},{{&g_289,&g_289,&l_106,&g_33,&l_106},{&l_247[1],&l_247[1],&l_242[1],&l_247[3],&l_242[g_76]}},{{&l_227,&l_242[g_76],(void*)0,&g_32,&l_247[3]},{&l_247[3],&l_242[1],&g_32,&g_33,&l_242[g_76]}},{{&l_243,&l_242[g_76],&l_227,&l_406,&g_31},{&g_289,&l_247[1],&l_243,&g_289,&l_247[3]}},{{&l_243,&g_289,&l_227,&g_31,&l_247[1]},{&l_243,&g_289,&g_289,&g_289,&l_243}},{{&g_289,&l_406,(void*)0,&l_242[g_76],&l_247[1]},{&l_243,(void*)0,&g_289,(void*)0,(void*)0}},{{&l_247[3],&g_289,(void*)0,&l_406,&l_247[1]},{&l_227,(void*)0,&l_106,&g_101,&l_243}},{{&l_247[1],&l_243,&g_289,&l_247[3],&l_247[1]},{&g_289,&l_242[g_76],&g_33,&l_247[3],&l_247[3]}},{{&g_32,&l_227,&g_32,&g_101,&g_31},{&l_243,&l_247[1],&l_227,&l_406,&l_242[g_76]}}};
            int8_t *l_429[3];
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_429[i] = &g_161;
            --l_414;
            (**l_119) = &l_242[g_76];
            if (((*l_129) = (safe_mul_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((((safe_mod_func_uint16_t_u_u((((*l_317) = (safe_mod_func_int64_t_s_s(l_242[g_76], (((safe_sub_func_int8_t_s_s((g_161 = (**l_120)), (safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_s(((+0x4594L) < (((((safe_sub_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(0x0985L, p_79)), (l_439 == &l_317))) > (safe_mul_func_int16_t_s_s((((&g_290[3] != (**l_369)) , (void*)0) == &l_219), 0x5B89L))) & p_79) >= g_289) < (**l_117))), (*l_411))), g_152)))) , g_152) , p_79)))) & (*l_129)), (-4L))) == p_79) > g_99), (*l_408))) ^ 0x6FCFL), 0x5F0BL)), (***l_119)))))
            { /* block id: 163 */
                float *l_442 = (void*)0;
                float *l_443 = &l_405[0];
                (*l_443) = p_79;
            }
            else
            { /* block id: 165 */
                uint16_t l_455 = 0x18DEL;
                int32_t l_456 = 0L;
                for (l_241 = 0; (l_241 <= 2); l_241 += 1)
                { /* block id: 168 */
                    int i;
                    (**l_119) = (l_121[l_241] , (*l_117));
                    (*l_409) = (safe_sub_func_int64_t_s_s((-1L), (safe_sub_func_int16_t_s_s(((((*l_372) ^= (-1L)) <= p_79) || (+((void*)0 == p_80))), ((l_456 &= (((~(g_161 = ((safe_rshift_func_int8_t_s_u((safe_add_func_int64_t_s_s((~((*l_408) = (l_455 <= p_79))), p_79)), 2)) & p_79))) , 8L) , (*l_407))) != g_99)))));
                }
            }
        }
    }
    else
    { /* block id: 178 */
        int64_t * const **l_457 = (void*)0;
        uint8_t *l_462 = &g_324[1];
        uint8_t **l_463 = &l_462;
        int32_t l_468 = 0xAFB6D8F9L;
        int32_t l_479 = (-1L);
        l_479 |= ((&l_371[1][0][2] != l_457) < ((safe_sub_func_int8_t_s_s(((g_164[0][1][5] = ((g_461 = l_460) != ((*l_463) = l_462))) || (((&g_222 == &g_222) > (safe_add_func_uint16_t_u_u((((safe_add_func_int64_t_s_s(l_468, (((((safe_sub_func_int8_t_s_s((+(safe_rshift_func_uint8_t_u_s((safe_sub_func_uint32_t_u_u(0x8856A357L, (safe_rshift_func_uint16_t_u_s((0x01L && 1UL), 6)))), 4))), g_161)) || 6L) , (*l_100)) > l_468) , (-1L)))) ^ 1UL) , 65528UL), p_79))) == l_468)), l_468)) <= p_79));
    }
    return (*g_75);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2[i][j], "g_2[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_3[i], "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_31, "g_31", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_33, "g_33", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_34[i], "g_34[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_41[i][j], "g_41[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_99, "g_99", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_114[i][j], "g_114[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_150, "g_150", print_hash_value);
    transparent_crc(g_152, "g_152", print_hash_value);
    transparent_crc(g_161, "g_161", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_164[i][j][k], "g_164[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_182, "g_182", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    transparent_crc(g_222, "g_222", print_hash_value);
    transparent_crc(g_289, "g_289", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_290[i], "g_290[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_301[i], "g_301[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_324[i], "g_324[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_355, "g_355", print_hash_value);
    transparent_crc(g_358, "g_358", print_hash_value);
    transparent_crc(g_359, "g_359", print_hash_value);
    transparent_crc(g_360, "g_360", print_hash_value);
    transparent_crc(g_365, "g_365", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_377[i], "g_377[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_496[i], "g_496[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_513, sizeof(g_513), "g_513", print_hash_value);
    transparent_crc(g_515, "g_515", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_521[i], "g_521[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_596[i], "g_596[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_615, "g_615", print_hash_value);
    transparent_crc(g_919, "g_919", print_hash_value);
    transparent_crc(g_991, "g_991", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1051[i][j][k], "g_1051[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1053, "g_1053", print_hash_value);
    transparent_crc(g_1114, "g_1114", print_hash_value);
    transparent_crc(g_1236, "g_1236", print_hash_value);
    transparent_crc(g_1237, "g_1237", print_hash_value);
    transparent_crc_bytes (&g_1298, sizeof(g_1298), "g_1298", print_hash_value);
    transparent_crc(g_1352, "g_1352", print_hash_value);
    transparent_crc(g_1471, "g_1471", print_hash_value);
    transparent_crc(g_1611, "g_1611", print_hash_value);
    transparent_crc(g_1626, "g_1626", print_hash_value);
    transparent_crc(g_1674, "g_1674", print_hash_value);
    transparent_crc(g_1887, "g_1887", print_hash_value);
    transparent_crc(g_1995, "g_1995", print_hash_value);
    transparent_crc(g_2090, "g_2090", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2129[i], "g_2129[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2134, "g_2134", print_hash_value);
    transparent_crc(g_2177, "g_2177", print_hash_value);
    transparent_crc(g_2242, "g_2242", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 570
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 206
   depth: 2, occurrence: 61
   depth: 3, occurrence: 4
   depth: 4, occurrence: 1
   depth: 5, occurrence: 4
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 3
   depth: 14, occurrence: 2
   depth: 15, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 3
   depth: 18, occurrence: 4
   depth: 19, occurrence: 1
   depth: 20, occurrence: 2
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 24, occurrence: 4
   depth: 25, occurrence: 1
   depth: 26, occurrence: 4
   depth: 27, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 5
   depth: 31, occurrence: 1
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1

XXX total number of pointers: 482

XXX times a variable address is taken: 1345
XXX times a pointer is dereferenced on RHS: 177
breakdown:
   depth: 1, occurrence: 144
   depth: 2, occurrence: 28
   depth: 3, occurrence: 5
XXX times a pointer is dereferenced on LHS: 224
breakdown:
   depth: 1, occurrence: 202
   depth: 2, occurrence: 22
XXX times a pointer is compared with null: 39
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 9
XXX times a pointer is qualified to be dereferenced: 6622

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1211
   level: 2, occurrence: 309
   level: 3, occurrence: 142
   level: 4, occurrence: 35
XXX number of pointers point to pointers: 223
XXX number of pointers point to scalars: 259
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 35.1
XXX average alias set size: 1.48

XXX times a non-volatile is read: 1726
XXX times a non-volatile is write: 771
XXX times a volatile is read: 12
XXX    times read thru a pointer: 10
XXX times a volatile is write: 10
XXX    times written thru a pointer: 8
XXX times a volatile is available for access: 255
XXX percentage of non-volatile access: 99.1

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 222
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 25
   depth: 2, occurrence: 25
   depth: 3, occurrence: 35
   depth: 4, occurrence: 52
   depth: 5, occurrence: 55

XXX percentage a fresh-made variable is used: 17.9
XXX percentage an existing variable is used: 82.1
********************* end of statistics **********************/


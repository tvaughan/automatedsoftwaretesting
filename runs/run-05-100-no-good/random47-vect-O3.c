/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1836014889
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile signed f0 : 19;
   uint32_t  f1;
   unsigned f2 : 9;
   int32_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static volatile uint16_t g_5 = 0x6694L;/* VOLATILE GLOBAL g_5 */
static uint32_t g_17[6] = {0x542AFA01L,0x542AFA01L,0x542AFA01L,0x542AFA01L,0x542AFA01L,0x542AFA01L};
static int32_t g_47 = (-1L);
static int32_t g_63 = 0xADB5D230L;
static int32_t g_65 = (-1L);
static int32_t g_66[10][10] = {{1L,(-1L),0L,0x1694A98AL,(-1L),(-7L),0x9E98C534L,0x01494D85L,0x9E98C534L,(-7L)},{0x1BC1F9C8L,0xCA51C18CL,0xDD6B9EC4L,0xCA51C18CL,0x1BC1F9C8L,0L,(-4L),0xB4FBF646L,0L,0L},{0L,0xC717898AL,0x9E98C534L,0L,0L,1L,3L,1L,0x1694A98AL,0L},{1L,0L,(-7L),0L,0x1BC1F9C8L,0xC717898AL,0xC717898AL,0x1BC1F9C8L,0L,(-7L)},{1L,1L,0xB4FBF646L,0xC717898AL,0L,(-4L),0x1694A98AL,0x9E98C534L,0xB4FBF646L,(-7L)},{0xAE54FD57L,0xDD6B9EC4L,2L,0x1694A98AL,2L,0x0138D7EFL,0x1694A98AL,(-6L),9L,0xAE54FD57L},{1L,1L,9L,2L,(-7L),2L,9L,1L,1L,2L},{0L,0x9E98C534L,(-4L),1L,1L,(-4L),0xC717898AL,0xAE54FD57L,(-9L),0x1694A98AL},{1L,9L,0x0138D7EFL,1L,2L,0L,1L,1L,1L,1L},{(-6L),0xAE54FD57L,2L,2L,0xAE54FD57L,(-6L),0xDD6B9EC4L,0L,9L,1L}};
static uint32_t g_67 = 0xB643E267L;
static int64_t g_110 = 0x904586DB868C9304LL;
static int32_t * volatile g_129 = (void*)0;/* VOLATILE GLOBAL g_129 */
static int32_t * volatile *g_128 = &g_129;
static uint32_t * volatile g_136[2][8] = {{&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67},{&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67}};
static uint32_t * volatile *g_135 = &g_136[0][6];
static uint32_t *g_138 = &g_67;
static uint32_t **g_137[4] = {&g_138,&g_138,&g_138,&g_138};
static int16_t g_171 = 0xFC08L;
static uint8_t g_181 = 0x77L;
static int8_t g_235 = (-2L);
static volatile uint64_t * volatile *g_238 = (void*)0;
static int32_t g_253 = 0x106ACACAL;
static uint32_t g_260 = 0xE770A445L;
static uint16_t g_275 = 0xA8C1L;
static uint32_t g_284[10][7] = {{8UL,0x135286ADL,0x920E4475L,0xAD8BE9D1L,0xE879045EL,8UL,0xAD8BE9D1L},{4294967286UL,0x1276FEF8L,2UL,5UL,5UL,2UL,0x1276FEF8L},{0x135286ADL,0x001BC32FL,0x920E4475L,0UL,0x001BC32FL,0xAD8BE9D1L,8UL},{4294967295UL,5UL,0UL,4294967295UL,0x1276FEF8L,4294967295UL,0UL},{0xE879045EL,0xE879045EL,4294967295UL,0UL,0x135286ADL,0UL,0xE879045EL},{0xA4EA3BCEL,0UL,3UL,5UL,0xD0E78556L,0xD0E78556L,5UL},{0UL,8UL,0UL,0xAD8BE9D1L,0x135286ADL,0UL,0x001BC32FL},{5UL,0x1276FEF8L,4294967294UL,0x8856AFF5L,0x1276FEF8L,4294967286UL,0x1276FEF8L},{0x1571C5A3L,0xAD8BE9D1L,0xAD8BE9D1L,0x1571C5A3L,0x001BC32FL,0UL,0x135286ADL},{0xD0E78556L,0xA4EA3BCEL,0UL,3UL,5UL,0xD0E78556L,0xD0E78556L}};
static float g_288[9][5][4] = {{{0x6.DE33CDp+95,0x4.7p+1,0x1.7p+1,0x6.DE33CDp+95},{(-0x5.Ep-1),0xF.020C03p-51,0x2.2p+1,0x6.DE33CDp+95},{(-0x1.8p-1),0x4.7p+1,0xF.020C03p-51,(-0x1.8p-1)},{(-0x5.Ep-1),0x4.7p+1,0x0.Cp-1,0x6.DE33CDp+95},{0x6.DE33CDp+95,0xF.020C03p-51,0xF.020C03p-51,0x6.DE33CDp+95}},{{0xF.A4A0F4p+7,0x4.7p+1,0x2.2p+1,(-0x1.8p-1)},{0x6.DE33CDp+95,0x4.7p+1,0x1.7p+1,0x6.DE33CDp+95},{(-0x5.Ep-1),0xF.020C03p-51,0x2.2p+1,0x6.DE33CDp+95},{(-0x1.8p-1),0x4.7p+1,0xF.020C03p-51,(-0x1.8p-1)},{(-0x5.Ep-1),0x4.7p+1,0x0.Cp-1,0x6.DE33CDp+95}},{{0x6.DE33CDp+95,0x1.7p+1,0x1.7p+1,(-0x1.8p-1)},{0x1.4EF959p+61,0xF.020C03p-51,0x0.Cp-1,0x0.1p+1},{(-0x1.8p-1),0xF.020C03p-51,0x4.7p+1,(-0x1.8p-1)},{0xF.A4A0F4p+7,0x1.7p+1,0x0.Cp-1,(-0x1.8p-1)},{0x0.1p+1,0xF.020C03p-51,0x1.7p+1,0x0.1p+1}},{{0xF.A4A0F4p+7,0xF.020C03p-51,0x0.9p-1,(-0x1.8p-1)},{(-0x1.8p-1),0x1.7p+1,0x1.7p+1,(-0x1.8p-1)},{0x1.4EF959p+61,0xF.020C03p-51,0x0.Cp-1,0x0.1p+1},{(-0x1.8p-1),0xF.020C03p-51,0x4.7p+1,(-0x1.8p-1)},{0xF.A4A0F4p+7,0x1.7p+1,0x0.Cp-1,(-0x1.8p-1)}},{{0x0.1p+1,0xF.020C03p-51,0x1.7p+1,0x0.1p+1},{0xF.A4A0F4p+7,0xF.020C03p-51,0x0.9p-1,(-0x1.8p-1)},{(-0x1.8p-1),0x1.7p+1,0x1.7p+1,(-0x1.8p-1)},{0x1.4EF959p+61,0xF.020C03p-51,0x0.Cp-1,0x0.1p+1},{(-0x1.8p-1),0xF.020C03p-51,0x4.7p+1,(-0x1.8p-1)}},{{0xF.A4A0F4p+7,0x1.7p+1,0x0.Cp-1,(-0x1.8p-1)},{0x0.1p+1,0xF.020C03p-51,0x1.7p+1,0x0.1p+1},{0xF.A4A0F4p+7,0xF.020C03p-51,0x0.9p-1,(-0x1.8p-1)},{(-0x1.8p-1),0x1.7p+1,0x1.7p+1,(-0x1.8p-1)},{0x1.4EF959p+61,0xF.020C03p-51,0x0.Cp-1,0x0.1p+1}},{{(-0x1.8p-1),0xF.020C03p-51,0x4.7p+1,(-0x1.8p-1)},{0xF.A4A0F4p+7,0x1.7p+1,0x0.Cp-1,(-0x1.8p-1)},{0x0.1p+1,0xF.020C03p-51,0x1.7p+1,0x0.1p+1},{0xF.A4A0F4p+7,0xF.020C03p-51,0x0.9p-1,(-0x1.8p-1)},{(-0x1.8p-1),0x1.7p+1,0x1.7p+1,(-0x1.8p-1)}},{{0x1.4EF959p+61,0xF.020C03p-51,0x0.Cp-1,0x0.1p+1},{(-0x1.8p-1),0xF.020C03p-51,0x4.7p+1,(-0x1.8p-1)},{0xF.A4A0F4p+7,0x1.7p+1,0x0.Cp-1,(-0x1.8p-1)},{0x0.1p+1,0xF.020C03p-51,0x1.7p+1,0x0.1p+1},{0xF.A4A0F4p+7,0xF.020C03p-51,0x0.9p-1,(-0x1.8p-1)}},{{(-0x1.8p-1),0x1.7p+1,0x1.7p+1,(-0x1.8p-1)},{0x1.4EF959p+61,0xF.020C03p-51,0x0.Cp-1,0x0.1p+1},{(-0x1.8p-1),0xF.020C03p-51,0x4.7p+1,(-0x1.8p-1)},{0xF.A4A0F4p+7,0x1.7p+1,0x0.Cp-1,(-0x1.8p-1)},{0x0.1p+1,0xF.020C03p-51,0x1.7p+1,0x0.1p+1}}};
static const int8_t g_320 = (-1L);
static const int8_t *g_319 = &g_320;
static int32_t g_332 = (-6L);
static uint8_t g_343 = 0xCBL;
static volatile int64_t g_355 = 0x413386D8614077CBLL;/* VOLATILE GLOBAL g_355 */
static volatile int64_t *g_354 = &g_355;
static uint16_t g_366 = 0x2A6EL;
static int16_t g_392 = (-2L);
static int8_t g_393 = 0x2FL;
static float *g_396[2] = {&g_288[7][1][3],&g_288[7][1][3]};
static float **g_395 = &g_396[1];
static uint32_t g_404 = 8UL;
static volatile uint32_t **g_412 = (void*)0;
static volatile uint32_t ***g_411 = &g_412;
static volatile uint32_t ****g_410 = &g_411;
static int8_t *g_422 = &g_393;
static uint32_t g_438 = 0x49ECD370L;
static float g_444 = 0xE.FB1E84p-93;
static uint64_t g_454[3][5][4] = {{{0x18B767EB7EE75F0ELL,0xE87A685396C799F6LL,0x18B767EB7EE75F0ELL,0xF753D0AA9BFA86D8LL},{18446744073709551611UL,18446744073709551615UL,0xCEFCBEEDD78CA85BLL,18446744073709551611UL},{18446744073709551615UL,0xF753D0AA9BFA86D8LL,0xC37A9DF633CEB34BLL,18446744073709551615UL},{0xF753D0AA9BFA86D8LL,0xE87A685396C799F6LL,0xC37A9DF633CEB34BLL,0xC37A9DF633CEB34BLL},{18446744073709551615UL,18446744073709551615UL,0xCEFCBEEDD78CA85BLL,18446744073709551611UL}},{{18446744073709551611UL,2UL,0x18B767EB7EE75F0ELL,18446744073709551615UL},{0x18B767EB7EE75F0ELL,18446744073709551615UL,0xF753D0AA9BFA86D8LL,0x18B767EB7EE75F0ELL},{18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551615UL},{18446744073709551615UL,2UL,0xC37A9DF633CEB34BLL,18446744073709551611UL},{0x6B7FB04761E1F90ELL,18446744073709551615UL,0xF753D0AA9BFA86D8LL,0xC37A9DF633CEB34BLL}},{{18446744073709551611UL,0xE87A685396C799F6LL,0x5EC055DD7B725A2ELL,18446744073709551615UL},{18446744073709551611UL,0xF753D0AA9BFA86D8LL,0xF753D0AA9BFA86D8LL,18446744073709551611UL},{0x6B7FB04761E1F90ELL,0x18B767EB7EE75F0ELL,0x6F8472449F7745F7LL,0x5EC055DD7B725A2ELL},{0x18B767EB7EE75F0ELL,0x6B7FB04761E1F90ELL,0xE87A685396C799F6LL,2UL},{0xF753D0AA9BFA86D8LL,0xCEFCBEEDD78CA85BLL,0x5EC055DD7B725A2ELL,2UL}}};
static float g_469 = 0xC.85C02Cp+29;
static int16_t g_475 = 0x1C5DL;
static int16_t g_478 = 0L;
static uint8_t g_480 = 251UL;
static int32_t *g_488 = &g_253;
static int64_t *g_494 = &g_110;
static int64_t **g_493[7][7][3] = {{{&g_494,&g_494,(void*)0},{&g_494,(void*)0,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494}},{{&g_494,&g_494,&g_494},{&g_494,(void*)0,(void*)0},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,(void*)0}},{{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{(void*)0,(void*)0,&g_494},{&g_494,&g_494,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494}},{{&g_494,&g_494,&g_494},{&g_494,&g_494,(void*)0},{(void*)0,(void*)0,&g_494},{&g_494,&g_494,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,(void*)0,(void*)0}},{{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{(void*)0,(void*)0,&g_494},{&g_494,&g_494,&g_494},{(void*)0,&g_494,&g_494}},{{&g_494,&g_494,(void*)0},{&g_494,(void*)0,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494}},{{&g_494,&g_494,&g_494},{&g_494,(void*)0,(void*)0},{&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494},{(void*)0,&g_494,&g_494},{&g_494,&g_494,&g_494},{&g_494,&g_494,(void*)0}}};
static int64_t ***g_492 = &g_493[0][5][2];
static int64_t g_505 = 3L;
static int16_t g_538 = 2L;
static int8_t g_548 = (-1L);
static uint32_t g_549 = 3UL;
static uint32_t g_590 = 0x38EAC8AAL;
static float g_614 = 0x0.D53803p-12;
static uint16_t g_621 = 2UL;
static int32_t g_690 = (-6L);
static const int16_t g_722 = 0xC6C5L;
static int32_t g_746 = 0xE8D3D7B8L;
static int32_t g_774 = (-1L);
static uint16_t g_775 = 65531UL;
static const int32_t g_781 = 0x2D70BE8DL;
static int32_t g_926 = (-1L);
static uint32_t g_927 = 0x52EB7A43L;
static uint32_t ***g_942[6][8] = {{&g_137[1],&g_137[1],&g_137[0],&g_137[1],&g_137[0],&g_137[1],&g_137[1],(void*)0},{&g_137[1],&g_137[1],&g_137[1],&g_137[1],&g_137[3],&g_137[1],&g_137[1],&g_137[3]},{&g_137[1],&g_137[1],&g_137[1],&g_137[1],&g_137[3],&g_137[0],(void*)0,&g_137[1]},{&g_137[1],&g_137[1],&g_137[1],&g_137[3],&g_137[0],&g_137[3],&g_137[1],&g_137[1]},{&g_137[1],&g_137[1],&g_137[3],&g_137[1],&g_137[0],&g_137[1],&g_137[0],&g_137[0]},{&g_137[3],&g_137[1],&g_137[1],&g_137[1],&g_137[1],&g_137[3],&g_137[0],(void*)0}};
static uint32_t ****g_941 = &g_942[4][5];
static float g_1028 = (-0x1.5p+1);
static uint32_t g_1029 = 0UL;
static uint8_t g_1092[10] = {0x38L,0x72L,0x38L,0x38L,0x72L,0x38L,0x38L,0x72L,0x38L,0x38L};
static uint32_t g_1145 = 0x8E833881L;
static uint64_t *g_1157 = &g_454[2][0][2];
static uint64_t **g_1156 = &g_1157;
static union U0 g_1160[7] = {{1L},{1L},{1L},{1L},{1L},{1L},{1L}};
static uint64_t g_1213[3] = {2UL,2UL,2UL};
static uint8_t g_1234 = 0x84L;
static const uint8_t *g_1277 = &g_181;
static const uint8_t * volatile *g_1276 = &g_1277;
static volatile union U0 g_1305 = {0x06607DC4L};/* VOLATILE GLOBAL g_1305 */
static volatile union U0 *g_1304[1][3] = {{&g_1305,&g_1305,&g_1305}};
static uint64_t g_1330 = 0xF9AC60715851B223LL;
static union U0 g_1334 = {4L};/* VOLATILE GLOBAL g_1334 */
static union U0 g_1335 = {0xD82E05D2L};/* VOLATILE GLOBAL g_1335 */
static union U0 g_1336 = {-8L};/* VOLATILE GLOBAL g_1336 */
static union U0 g_1337 = {0xC942FB97L};/* VOLATILE GLOBAL g_1337 */
static int8_t g_1355 = 7L;
static float g_1380 = (-0x5.2p-1);
static uint16_t g_1384[7][4][7] = {{{1UL,0x2482L,0x1097L,0x8F06L,65535UL,0xE939L,0x0C92L},{0xA9DCL,0x2482L,0xAA8EL,0xAA8EL,0x2482L,0xA9DCL,0xD1A8L},{0xDF5AL,0x2482L,0x632DL,65527UL,8UL,0UL,0x872EL},{0x3F4DL,0x2482L,65527UL,7UL,0UL,1UL,0UL}},{{0UL,0x2482L,0x198AL,0x1097L,9UL,0x3F4DL,0xD50FL},{0xE939L,0x2482L,7UL,0x632DL,0xFDC0L,0x9BA9L,0x2EE9L},{0x9BA9L,0x2482L,0x8F06L,0x198AL,0x969DL,0xDF5AL,0x8166L},{1UL,0x2482L,0x1097L,0x8F06L,65535UL,0xE939L,0x0C92L}},{{0xA9DCL,0x2482L,0xAA8EL,0xAA8EL,0x2482L,0xA9DCL,0xD1A8L},{0xDF5AL,0x2482L,0x632DL,65527UL,8UL,0UL,0x872EL},{0x3F4DL,0x2482L,65527UL,7UL,0UL,1UL,0UL},{0UL,0x2482L,0x198AL,0x1097L,9UL,0x3F4DL,0xD50FL}},{{0xE939L,0x2482L,7UL,0x632DL,0xFDC0L,0x9BA9L,0x2EE9L},{0x9BA9L,0x2482L,0x8F06L,0x198AL,0x969DL,0xDF5AL,0x8166L},{1UL,0x2482L,0x1097L,0x8F06L,65535UL,0xE939L,0x0C92L},{0xA9DCL,0x2482L,0xAA8EL,0xAA8EL,0x2482L,0xA9DCL,0xD1A8L}},{{0xDF5AL,0x2482L,0x632DL,65527UL,8UL,0UL,0x872EL},{0x3F4DL,0x2482L,65527UL,7UL,0UL,1UL,0UL},{0UL,0x2482L,0x198AL,0x1097L,9UL,0x3F4DL,0xD50FL},{0xE939L,0x2482L,7UL,0x632DL,0xFDC0L,0x9BA9L,0x2EE9L}},{{0x9BA9L,0x2482L,0x8F06L,0x198AL,0x969DL,0xDF5AL,0x8166L},{1UL,0x2482L,0x1097L,0x8F06L,65535UL,0xE939L,0x14DCL},{0x8DAAL,0x632DL,0x2EE9L,0x2EE9L,0x632DL,0x8DAAL,0x070CL},{0x68F4L,0x632DL,0xD1A8L,0xD50FL,7UL,0xCD78L,0xC21BL}},{{65526UL,0x632DL,0xD50FL,0UL,0xAA8EL,0x9D90L,0UL},{0xCD78L,0x632DL,0x0C92L,0x872EL,0x198AL,65526UL,5UL},{1UL,0x632DL,0UL,0xD1A8L,0x1097L,0x940FL,0UL},{0x940FL,0x632DL,0x8166L,0x0C92L,65527UL,0x68F4L,1UL}}};
static int32_t g_1518 = 0xDCCF9620L;
static uint32_t g_1519 = 3UL;
static int16_t * const g_1536 = (void*)0;
static int16_t * const *g_1535[10][2][8] = {{{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,(void*)0,&g_1536},{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,(void*)0}},{{(void*)0,&g_1536,&g_1536,&g_1536,&g_1536,(void*)0,&g_1536,&g_1536},{&g_1536,&g_1536,(void*)0,&g_1536,&g_1536,&g_1536,(void*)0,(void*)0}},{{&g_1536,&g_1536,(void*)0,&g_1536,(void*)0,&g_1536,&g_1536,(void*)0},{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536}},{{&g_1536,&g_1536,(void*)0,(void*)0,&g_1536,&g_1536,&g_1536,&g_1536},{&g_1536,&g_1536,&g_1536,&g_1536,(void*)0,&g_1536,&g_1536,&g_1536}},{{&g_1536,&g_1536,(void*)0,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536},{&g_1536,&g_1536,(void*)0,(void*)0,&g_1536,&g_1536,&g_1536,&g_1536}},{{(void*)0,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536},{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536}},{{&g_1536,&g_1536,&g_1536,(void*)0,&g_1536,&g_1536,&g_1536,&g_1536},{&g_1536,(void*)0,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536}},{{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536},{&g_1536,&g_1536,(void*)0,&g_1536,&g_1536,&g_1536,(void*)0,&g_1536}},{{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536},{&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536}},{{&g_1536,&g_1536,(void*)0,(void*)0,&g_1536,&g_1536,&g_1536,&g_1536},{(void*)0,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536,&g_1536}}};
static uint64_t g_1548 = 0xEA19EC4CBB586CE7LL;
static int64_t g_1565 = 1L;
static union U0 g_1571 = {0x41F28583L};/* VOLATILE GLOBAL g_1571 */
static uint32_t g_1592[7] = {0x1A4782ADL,0x1A4782ADL,0x2BBBE50AL,0x1A4782ADL,0x1A4782ADL,0x2BBBE50AL,0x1A4782ADL};
static uint32_t * const g_1596 = (void*)0;
static uint32_t * const *g_1595 = &g_1596;
static float g_1625 = (-0x5.Ep+1);
static uint16_t g_1629 = 0xBFBFL;
static uint32_t *g_1640 = &g_404;
static int8_t g_1653 = 0xBCL;
static int16_t g_1654[1][2][2] = {{{0x73D8L,0x73D8L},{0x73D8L,0x73D8L}}};
static int32_t g_1657[6] = {(-7L),(-7L),(-7L),(-7L),(-7L),(-7L)};
static int64_t g_1658 = 0xD1F89A0BD42D405ALL;
static uint32_t g_1659 = 4294967295UL;
static int16_t g_1665 = 0xA35FL;
static float g_1667 = 0x0.0p-1;
static uint32_t g_1668 = 0xFF26D1C8L;
static uint64_t g_1733[3] = {18446744073709551613UL,18446744073709551613UL,18446744073709551613UL};
static uint32_t g_1921 = 1UL;
static uint16_t g_1935 = 1UL;
static uint16_t *g_1943 = &g_1629;
static uint16_t * volatile *g_1942 = &g_1943;
static uint16_t * volatile **g_1941 = &g_1942;
static int32_t g_1960[3][1][6] = {{{0x94698DD2L,(-1L),(-1L),0x94698DD2L,0x94698DD2L,(-1L)}},{{0x94698DD2L,0x94698DD2L,(-1L),(-1L),0x94698DD2L,0x94698DD2L}},{{0x94698DD2L,(-1L),(-1L),0x94698DD2L,0x94698DD2L,(-1L)}}};
static const float g_2015 = 0x7.A68085p+6;
static volatile uint16_t g_2092 = 65535UL;/* VOLATILE GLOBAL g_2092 */
static volatile uint16_t *g_2091 = &g_2092;
static volatile uint16_t * const  volatile * volatile g_2090 = &g_2091;/* VOLATILE GLOBAL g_2090 */
static volatile uint16_t * const  volatile * const  volatile *g_2089 = &g_2090;
static volatile uint16_t * const  volatile * const  volatile **g_2088 = &g_2089;
static volatile uint16_t * const  volatile * const  volatile *** const g_2087 = &g_2088;
static int32_t g_2130[6][2][7] = {{{(-10L),(-3L),0L,(-10L),0xD24FE36BL,0x82E38651L,0x70FFC486L},{0xDC2E48F7L,0xB909AE5AL,0x7F7D4B99L,0xDC2E48F7L,0x46C3DB8BL,0xFB455003L,0x46C3DB8BL}},{{0x9529B100L,0L,0L,0x9529B100L,0x70FFC486L,0x82E38651L,0xD24FE36BL},{0xEBFAD574L,0xB909AE5AL,8L,0xEBFAD574L,0x46C3DB8BL,0xE240D9FEL,2L}},{{0x9529B100L,(-3L),0L,0x9529B100L,0xD24FE36BL,1L,0xD24FE36BL},{0xDC2E48F7L,8L,8L,0xDC2E48F7L,2L,0xE240D9FEL,0x46C3DB8BL}},{{(-10L),(-3L),0L,(-10L),0xD24FE36BL,0x82E38651L,0x70FFC486L},{0xDC2E48F7L,0xB909AE5AL,0x7F7D4B99L,0xDC2E48F7L,0x46C3DB8BL,0xFB455003L,0x46C3DB8BL}},{{0x9529B100L,0L,0L,0x9529B100L,0x70FFC486L,0x82E38651L,0xD24FE36BL},{0xEBFAD574L,0xB909AE5AL,8L,0xEBFAD574L,0x46C3DB8BL,0xE240D9FEL,2L}},{{0x9529B100L,(-3L),0L,0x9529B100L,0xD24FE36BL,1L,0xD24FE36BL},{0xDC2E48F7L,8L,8L,0xDC2E48F7L,2L,0xE240D9FEL,0x46C3DB8BL}}};
static union U0 g_2159[4][4] = {{{0x0EC9ED09L},{0L},{0x0EC9ED09L},{0x0EC9ED09L}},{{0L},{0L},{1L},{0L}},{{0L},{0x0EC9ED09L},{0x0EC9ED09L},{0L}},{{0x0EC9ED09L},{0L},{0x0EC9ED09L},{0x0EC9ED09L}}};
static uint16_t g_2285 = 4UL;
static union U0 g_2369 = {0x71E8EC06L};/* VOLATILE GLOBAL g_2369 */
static union U0 g_2371 = {0xB57B2ACDL};/* VOLATILE GLOBAL g_2371 */
static uint8_t *g_2422 = &g_1092[6];
static uint8_t **g_2421[8][7] = {{&g_2422,&g_2422,&g_2422,&g_2422,&g_2422,(void*)0,&g_2422},{&g_2422,(void*)0,(void*)0,&g_2422,&g_2422,(void*)0,&g_2422},{&g_2422,&g_2422,(void*)0,&g_2422,&g_2422,(void*)0,(void*)0},{&g_2422,&g_2422,(void*)0,&g_2422,&g_2422,&g_2422,&g_2422},{&g_2422,(void*)0,(void*)0,&g_2422,(void*)0,&g_2422,&g_2422},{(void*)0,&g_2422,&g_2422,&g_2422,&g_2422,&g_2422,&g_2422},{&g_2422,(void*)0,&g_2422,(void*)0,&g_2422,&g_2422,(void*)0},{(void*)0,&g_2422,(void*)0,&g_2422,&g_2422,&g_2422,&g_2422}};
static uint64_t ***g_2460 = &g_1156;
static uint64_t ****g_2459 = &g_2460;
static volatile int32_t *** volatile g_2494 = (void*)0;/* VOLATILE GLOBAL g_2494 */
static volatile int32_t **g_2496 = (void*)0;
static volatile int32_t ***g_2495[5] = {&g_2496,&g_2496,&g_2496,&g_2496,&g_2496};
static volatile int32_t *** volatile *g_2493[9] = {&g_2495[2],&g_2495[4],&g_2495[2],&g_2495[4],&g_2495[2],&g_2495[4],&g_2495[2],&g_2495[4],&g_2495[2]};
static uint16_t g_2531 = 3UL;
static const int32_t g_2612 = (-2L);
static float * volatile **g_2695[2] = {(void*)0,(void*)0};
static float * volatile ** volatile *g_2694 = &g_2695[1];
static float * volatile ** volatile * volatile *g_2693 = &g_2694;
static volatile uint32_t g_2858[6] = {0UL,0UL,0UL,0UL,0UL,0UL};
static volatile uint32_t *g_2857 = &g_2858[0];
static volatile uint32_t * volatile *g_2856 = &g_2857;
static volatile uint32_t * volatile **g_2855 = &g_2856;
static volatile uint32_t * volatile ** volatile *g_2854 = &g_2855;
static volatile uint32_t * volatile ** volatile * volatile *g_2853[4] = {&g_2854,&g_2854,&g_2854,&g_2854};
static uint64_t g_2990[3] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
static volatile int64_t * const * volatile ***g_2996 = (void*)0;
static int32_t *g_3041[4][4] = {{&g_926,&g_253,&g_253,&g_926},{&g_253,&g_926,&g_253,&g_253},{&g_926,&g_926,&g_47,&g_926},{&g_926,&g_253,&g_253,&g_926}};
static int64_t ****g_3186 = &g_492;
static int64_t g_3242[3][5] = {{(-2L),0xEF3BB995AF2BF19CLL,(-2L),(-2L),0xEF3BB995AF2BF19CLL},{0xE7CFC8E2312E50F9LL,5L,5L,0xE7CFC8E2312E50F9LL,5L},{0xEF3BB995AF2BF19CLL,0xEF3BB995AF2BF19CLL,0x88742B6D261E464BLL,0xEF3BB995AF2BF19CLL,0xEF3BB995AF2BF19CLL}};
static int32_t **g_3249 = &g_3041[0][2];
static int32_t ***g_3248 = &g_3249;
static int32_t ****g_3247 = &g_3248;
static int32_t *****g_3246 = &g_3247;
static union U0 g_3315 = {0x42366A57L};/* VOLATILE GLOBAL g_3315 */
static int8_t **g_3335[2][4][8] = {{{&g_422,&g_422,(void*)0,&g_422,&g_422,&g_422,(void*)0,&g_422},{(void*)0,&g_422,(void*)0,&g_422,&g_422,&g_422,(void*)0,&g_422},{&g_422,(void*)0,(void*)0,&g_422,&g_422,(void*)0,(void*)0,&g_422},{&g_422,(void*)0,&g_422,&g_422,&g_422,(void*)0,&g_422,(void*)0}},{{&g_422,(void*)0,&g_422,&g_422,&g_422,(void*)0,&g_422,&g_422},{&g_422,(void*)0,&g_422,(void*)0,&g_422,(void*)0,&g_422,(void*)0},{&g_422,(void*)0,&g_422,&g_422,&g_422,&g_422,&g_422,&g_422},{&g_422,&g_422,(void*)0,&g_422,&g_422,&g_422,&g_422,&g_422}}};
static int8_t ** volatile *g_3334[9][8] = {{(void*)0,(void*)0,&g_3335[1][1][5],&g_3335[0][1][1],&g_3335[0][3][7],&g_3335[1][1][6],&g_3335[1][1][6],&g_3335[1][1][6]},{&g_3335[0][0][3],&g_3335[1][1][6],&g_3335[1][0][7],&g_3335[1][1][5],&g_3335[1][0][7],&g_3335[1][1][6],&g_3335[0][0][3],&g_3335[1][1][6]},{&g_3335[1][1][5],&g_3335[1][1][6],&g_3335[1][1][6],&g_3335[0][3][5],(void*)0,&g_3335[1][1][6],&g_3335[1][1][6],&g_3335[1][1][6]},{(void*)0,&g_3335[1][1][6],&g_3335[1][1][6],(void*)0,(void*)0,(void*)0,&g_3335[1][1][6],&g_3335[1][1][5]},{&g_3335[1][1][5],&g_3335[1][1][6],(void*)0,&g_3335[1][1][6],&g_3335[1][0][7],&g_3335[1][1][6],&g_3335[1][1][6],&g_3335[1][0][7]},{&g_3335[0][0][3],(void*)0,(void*)0,&g_3335[0][0][3],&g_3335[0][3][7],&g_3335[1][1][6],&g_3335[1][2][7],&g_3335[1][1][6]},{(void*)0,&g_3335[1][0][7],&g_3335[0][0][3],(void*)0,&g_3335[1][1][6],&g_3335[1][1][6],(void*)0,&g_3335[0][3][7]},{&g_3335[1][1][6],(void*)0,&g_3335[0][0][3],&g_3335[1][2][7],&g_3335[0][1][2],(void*)0,&g_3335[1][1][6],&g_3335[1][1][6]},{&g_3335[1][1][6],&g_3335[1][1][6],&g_3335[1][1][6],&g_3335[0][1][1],&g_3335[1][1][6],&g_3335[0][1][1],&g_3335[1][1][6],&g_3335[1][1][6]}};
static uint32_t g_3412[3] = {0UL,0UL,0UL};
static volatile union U0 g_3433 = {-9L};/* VOLATILE GLOBAL g_3433 */
static union U0 g_3482 = {0xBB2424BBL};/* VOLATILE GLOBAL g_3482 */
static int64_t ***g_3530 = &g_493[0][5][2];
static union U0 *g_3578 = &g_1160[3];
static union U0 * const *g_3577[3][7] = {{&g_3578,&g_3578,&g_3578,&g_3578,&g_3578,&g_3578,&g_3578},{(void*)0,&g_3578,&g_3578,(void*)0,(void*)0,(void*)0,&g_3578},{&g_3578,&g_3578,&g_3578,&g_3578,&g_3578,&g_3578,&g_3578}};
static union U0 * const * const *g_3576 = &g_3577[1][0];
static const union U0 g_3583 = {-8L};/* VOLATILE GLOBAL g_3583 */
static int32_t g_3602 = 0x47243CCEL;
static union U0 g_3662 = {0L};/* VOLATILE GLOBAL g_3662 */
static volatile uint8_t g_3800 = 0UL;/* VOLATILE GLOBAL g_3800 */


/* --- FORWARD DECLARATIONS --- */
static float  func_1(void);
static int8_t  func_8(uint64_t  p_9, uint32_t  p_10, float  p_11, uint8_t  p_12);
static int32_t  func_20(float  p_21, int16_t  p_22, int8_t  p_23, int8_t  p_24, int16_t  p_25);
static int8_t  func_27(int64_t  p_28, uint64_t  p_29, int32_t  p_30, const uint32_t  p_31);
static int16_t  func_34(int32_t  p_35, const int32_t  p_36, int32_t  p_37, float  p_38, const uint32_t  p_39);
static int32_t * func_40(uint8_t  p_41);
static uint32_t  func_42(int32_t * p_43, int32_t  p_44, const int32_t * p_45);
static uint64_t  func_74(uint32_t  p_75, const float  p_76, uint8_t  p_77, int32_t  p_78, float  p_79);
static uint8_t  func_85(uint32_t  p_86);
static uint64_t  func_94(uint32_t * p_95);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_17 g_67 g_47 g_65 g_480 g_1277 g_181 g_275 g_1092 g_1276 g_548 g_1157 g_395 g_396 g_475 g_478 g_319 g_320 g_549 g_454 g_1519 g_1535 g_1156 g_1548 g_488 g_253 g_1565 g_110 g_926 g_332 g_1234 g_1330 g_1592 g_422 g_393 g_63 g_138 g_492 g_493 g_1213 g_1029 g_1384 g_690 g_722 g_288 g_1355 g_1921 g_1935 g_1941 g_1942 g_1943 g_1629 g_1640 g_494 g_746 g_1960 g_3246 g_3247 g_3248 g_3249 g_1304 g_2422 g_1145 g_2855 g_2856 g_2857 g_2858 g_354 g_355 g_3041 g_128 g_2693 g_2694 g_3662 g_411 g_412 g_2459 g_2460 g_3242 g_2990 g_2087 g_2088 g_2089 g_2090 g_2091 g_2092 g_3800
 * writes: g_67 g_47 g_480 g_275 g_438 g_475 g_454 g_288 g_181 g_1519 g_343 g_1548 g_1234 g_253 g_332 g_1330 g_1384 g_393 g_690 g_1935 g_404 g_548 g_171 g_1092 g_1960 g_3041 g_1304 g_1145 g_926 g_129 g_110 g_3242 g_65 g_2990 g_1629 g_549 g_621
 */
static float  func_1(void)
{ /* block id: 0 */
    uint16_t l_2 = 0xBF5CL;
    int32_t l_26[5][3] = {{(-1L),0xD7C869B7L,(-1L)},{(-1L),(-1L),0xD7C869B7L},{9L,0xD7C869B7L,0xD7C869B7L},{0xD7C869B7L,0xC4DDB84CL,(-1L)},{9L,0xC4DDB84CL,9L}};
    uint32_t l_2049 = 0x346824E8L;
    int32_t l_3427 = 1L;
    int16_t *l_3430 = &g_475;
    const int8_t l_3476 = 0xBEL;
    int64_t *****l_3490 = &g_3186;
    int32_t l_3547 = 4L;
    int32_t *l_3561[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t l_3627 = 0x0639965EL;
    uint64_t l_3636 = 18446744073709551610UL;
    int16_t l_3654 = (-8L);
    float ***l_3664 = &g_395;
    float ****l_3663[8][10] = {{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664},{&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664,&l_3664}};
    float l_3680 = 0x3.F775ABp+27;
    int16_t l_3682 = 0x9CA4L;
    int16_t l_3689 = 0xE235L;
    int16_t l_3777 = 3L;
    uint32_t **l_3799[8][8] = {{&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,(void*)0,&g_1640,&g_1640},{&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640},{&g_1640,&g_1640,&g_1640,(void*)0,&g_1640,&g_1640,&g_1640,(void*)0},{&g_1640,&g_1640,&g_1640,(void*)0,&g_1640,&g_1640,&g_1640,&g_1640},{&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640},{&g_1640,(void*)0,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640},{&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640},{&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640,&g_1640}};
    uint16_t l_3801 = 0x29DBL;
    uint32_t **l_3841[2];
    const union U0 *l_3842 = &g_3583;
    const union U0 **l_3843 = &l_3842;
    int16_t l_3844 = 0xE89CL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_3841[i] = &g_138;
    if ((l_2 > ((l_2 != l_2) != ((*l_3430) = ((safe_mul_func_uint8_t_u_u(g_5, ((*g_2422) = (safe_sub_func_int64_t_s_s(((func_8((safe_mod_func_uint16_t_u_u((0L || (((safe_div_func_int64_t_s_s(g_17[0], (safe_add_func_int32_t_s_s((l_3427 |= ((*g_488) = func_20(l_26[0][2], g_17[2], func_27((safe_rshift_func_int16_t_s_u(func_34(l_26[0][2], g_17[0], l_2, l_26[0][2], l_26[1][2]), 1)), l_2049, l_2049, l_2049), l_26[0][1], l_26[0][2]))), l_2049)))) | 0x6C49L) && l_3427)), 1UL)), g_1629, l_2049, l_2049) >= 0UL) | l_26[4][0]), l_26[0][2]))))) <= 0x72C89830L)))))
    { /* block id: 1610 */
        uint32_t l_3438 = 0x9B291599L;
        int32_t *l_3457 = &g_47;
        uint16_t **l_3481 = &g_1943;
        int64_t *l_3509 = &g_1658;
        float l_3540 = 0x7.6p+1;
        float *l_3568[2];
        int32_t l_3605 = 0x69521C0CL;
        uint16_t l_3637 = 0x170DL;
        const int32_t l_3648[8] = {0x0C889E61L,0L,0x0C889E61L,0L,0x0C889E61L,0L,0x0C889E61L,0L};
        int i;
        for (i = 0; i < 2; i++)
            l_3568[i] = &g_1667;
        for (g_1935 = 1; (g_1935 != 12); ++g_1935)
        { /* block id: 1613 */
            uint16_t l_3455[8][10] = {{0x365AL,65535UL,0xA7B3L,65535UL,65535UL,0xA7B3L,65535UL,0x365AL,65535UL,0xA7B3L},{65535UL,1UL,65535UL,1UL,65535UL,0xA7B3L,0xA7B3L,65535UL,1UL,65535UL},{0x365AL,0x365AL,65535UL,65535UL,0xB02AL,65535UL,65535UL,0x365AL,0x365AL,65535UL},{1UL,65535UL,0xA7B3L,0xA7B3L,65535UL,1UL,65535UL,1UL,65535UL,0xA7B3L},{65535UL,0x365AL,65535UL,0xA7B3L,65535UL,65535UL,0xA7B3L,65535UL,0x365AL,65535UL},{65535UL,1UL,0x365AL,65535UL,0x365AL,1UL,65535UL,65535UL,1UL,0x365AL},{1UL,65535UL,65535UL,1UL,0x365AL,65535UL,0x365AL,1UL,65535UL,65535UL},{0x365AL,65535UL,0xA7B3L,65535UL,65535UL,0xA7B3L,65535UL,0x365AL,65535UL,0xA7B3L}};
            int32_t l_3489[4][5][8] = {{{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L}},{{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L}},{{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L}},{{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L},{(-1L),1L,1L,1L,(-1L),1L,1L,1L}}};
            int32_t *****l_3517 = &g_3247;
            uint64_t l_3572[7][6][6] = {{{18446744073709551615UL,1UL,0xD475C14499568D29LL,0x719425BDB7AB9E13LL,0xF3F4826761BCDAE4LL,0x4E6D71A8AD96C658LL},{0x7FD7F224AB2FC0C0LL,0UL,0x41E6A13A70B73EDDLL,0x9CBA1D16032B0128LL,0x9709C6048A6CFA05LL,18446744073709551607UL},{0x4DD3B8CCC4A50B9FLL,0xD475C14499568D29LL,0UL,0UL,0xD475C14499568D29LL,0x4DD3B8CCC4A50B9FLL},{0xEF2333162D84A397LL,0UL,18446744073709551615UL,0UL,0x8C0A5BB4027DDEF9LL,0UL},{8UL,0xFEDACF6937980E84LL,0xAA33D911F3740136LL,0x0CCC1CC0EEAFE9A7LL,18446744073709551611UL,0UL},{8UL,0x0FF8D48A8AFF25A9LL,0x0CCC1CC0EEAFE9A7LL,0UL,0x2C2A725F49680F4BLL,0UL}},{{0xEF2333162D84A397LL,0x8C0A5BB4027DDEF9LL,0x349BE2DA1F31D78BLL,0UL,0UL,0xA5994F2EE489860FLL},{0x4DD3B8CCC4A50B9FLL,18446744073709551615UL,0xC0BE1A550238E106LL,0x9CBA1D16032B0128LL,18446744073709551613UL,18446744073709551614UL},{0x7FD7F224AB2FC0C0LL,0xCCFB8D6590ACFD62LL,0x8C0A5BB4027DDEF9LL,0x719425BDB7AB9E13LL,9UL,18446744073709551611UL},{18446744073709551615UL,0xDB0A48B1668F6BE1LL,0x08A91B30C75282DCLL,0x386FA070041B990ELL,0UL,1UL},{0xE7E909CEC6FB6896LL,0x3A3F6216598B80C3LL,18446744073709551615UL,8UL,0UL,0UL},{0x349BE2DA1F31D78BLL,0UL,0UL,0xFEDACF6937980E84LL,0xEB759574E1E28C40LL,0x09ED593671FA0039LL}},{{0xD475C14499568D29LL,0xE0B0749A53FDA438LL,0x0FD0475A97DBC23FLL,18446744073709551607UL,18446744073709551614UL,18446744073709551615UL},{0xA31024F38A70F43BLL,0x9D4483DB49EE8575LL,0UL,0x3A3F6216598B80C3LL,0xFE6A7EBB2B6C3E2BLL,18446744073709551611UL},{0UL,0UL,0xE4497C1699DFBFB7LL,0xCCFB8D6590ACFD62LL,2UL,0xDB0A48B1668F6BE1LL},{0xDB0A48B1668F6BE1LL,0x59348C9738260B39LL,2UL,0x9F34D583A4D55D73LL,0x386FA070041B990ELL,0x583107AE10E44DB7LL},{6UL,0x6C4D0A210C1E24DCLL,18446744073709551607UL,0x1E100B1E7AFEADA4LL,0xA428B1CEE2AF285ELL,18446744073709551606UL},{2UL,0UL,18446744073709551611UL,18446744073709551615UL,8UL,0x5CF47C3595B7D248LL}},{{0x7525A058FE321612LL,18446744073709551615UL,0UL,0xD5948865639527ACLL,0x90416A25163A0A0CLL,1UL},{0xA5994F2EE489860FLL,0x386FA070041B990ELL,0UL,0x6576A87AA054C0E0LL,0x4DD3B8CCC4A50B9FLL,0x9C182AA02B16352CLL},{0xAA33D911F3740136LL,0x09ED593671FA0039LL,0x9D4483DB49EE8575LL,0x583107AE10E44DB7LL,0x98261DEF3E401DE5LL,0UL},{0UL,0x6142C5BB5DC80E9ALL,0xD340C0B201D0D9BFLL,0x6142C5BB5DC80E9ALL,0UL,18446744073709551615UL},{0x1844C90AF2D82F55LL,0x9CBA1D16032B0128LL,18446744073709551606UL,18446744073709551615UL,3UL,18446744073709551613UL},{0xF761F5FCAA440994LL,0xD5948865639527ACLL,0UL,0x9CBA1D16032B0128LL,0x5CF47C3595B7D248LL,18446744073709551613UL}},{{18446744073709551607UL,0UL,18446744073709551606UL,0x7FD7F224AB2FC0C0LL,0UL,18446744073709551615UL},{0x5CF47C3595B7D248LL,0x9D4483DB49EE8575LL,0xD340C0B201D0D9BFLL,0UL,0UL,0UL},{0xDB0A48B1668F6BE1LL,8UL,0x9D4483DB49EE8575LL,0UL,0UL,0x9C182AA02B16352CLL},{0UL,0UL,0UL,18446744073709551607UL,18446744073709551615UL,0x3A3F6216598B80C3LL},{0xE2409AE9723340C4LL,0x1E100B1E7AFEADA4LL,0xEB759574E1E28C40LL,0x98261DEF3E401DE5LL,18446744073709551610UL,8UL},{18446744073709551611UL,0UL,0xEF2333162D84A397LL,0x41E6A13A70B73EDDLL,0x1E100B1E7AFEADA4LL,0x1844C90AF2D82F55LL}},{{0xA31024F38A70F43BLL,0xD475C14499568D29LL,18446744073709551611UL,0x08A91B30C75282DCLL,0xA5994F2EE489860FLL,0UL},{0x41E6A13A70B73EDDLL,0UL,0xE7E909CEC6FB6896LL,0xEF2333162D84A397LL,0x08A91B30C75282DCLL,1UL},{0x90416A25163A0A0CLL,0x7FD7F224AB2FC0C0LL,0xD5948865639527ACLL,0x09ED593671FA0039LL,0x349BE2DA1F31D78BLL,0xEF2333162D84A397LL},{1UL,3UL,0xDB4108752630C877LL,0xAA33D911F3740136LL,0UL,18446744073709551611UL},{0xDB4108752630C877LL,0x59348C9738260B39LL,0xE2409AE9723340C4LL,0xE4497C1699DFBFB7LL,1UL,18446744073709551614UL},{0xFEDACF6937980E84LL,0xCEABD7FF09924CDELL,0xDB0A48B1668F6BE1LL,1UL,1UL,0x5CF47C3595B7D248LL}},{{0x98261DEF3E401DE5LL,18446744073709551606UL,18446744073709551611UL,0x3EE7C38FEC265BD9LL,0UL,0x583107AE10E44DB7LL},{0xA311C290EA22568ALL,0xA428B1CEE2AF285ELL,18446744073709551615UL,0x9709C6048A6CFA05LL,0x9F34D583A4D55D73LL,0xC0BE1A550238E106LL},{0x4E6D71A8AD96C658LL,0UL,0UL,0UL,0x0CCC1CC0EEAFE9A7LL,0x289992479C239F71LL},{0xD5948865639527ACLL,0xE0B0749A53FDA438LL,0x9D4AD36CE3B9F6EALL,18446744073709551607UL,0x3EE7C38FEC265BD9LL,0x3EE7C38FEC265BD9LL},{0UL,0xA71388794719FF0ELL,0xA71388794719FF0ELL,0UL,0x1844C90AF2D82F55LL,0xF3F4826761BCDAE4LL},{0xC25B8E054ACC4667LL,0xA311C290EA22568ALL,0xCCFB8D6590ACFD62LL,0x0CCC1CC0EEAFE9A7LL,0x90416A25163A0A0CLL,18446744073709551607UL}}};
            float l_3586 = 0xF.8A3137p+52;
            const uint32_t **l_3631 = (void*)0;
            const uint32_t ***l_3630 = &l_3631;
            const uint32_t ****l_3629 = &l_3630;
            const uint32_t *****l_3628[8] = {&l_3629,(void*)0,&l_3629,(void*)0,&l_3629,(void*)0,&l_3629,(void*)0};
            int i, j, k;
        }
        for (g_1145 = 0; (g_1145 <= 6); g_1145 += 1)
        { /* block id: 1696 */
            uint32_t l_3647 = 0x0EC0B075L;
            (*l_3457) = (-0x1.9p-1);
            (****g_3247) = ((safe_mod_func_int32_t_s_s(((safe_add_func_int64_t_s_s(((*g_422) > ((safe_mul_func_uint16_t_u_u(((((*l_3457) || ((((void*)0 != l_3430) , (~((((((***g_2855) , ((&l_3438 != (void*)0) || (((safe_add_func_uint64_t_u_u(((*l_3457) && l_3647), (*g_354))) <= l_3647) != (*g_1943)))) != (*l_3457)) ^ 1UL) < (***g_3248)) & l_3648[0]))) < (*l_3457))) != (*l_3457)) == (*l_3457)), 0x4C06L)) == 0x31L)), (*g_1157))) | (*l_3457)), l_3647)) ^ 0xC9L);
            return (**g_395);
        }
        (*g_128) = l_3568[0];
    }
    else
    { /* block id: 1702 */
        uint32_t l_3657 = 4UL;
        int32_t l_3660 = 4L;
        int32_t l_3661[5][6][8] = {{{(-1L),0x6238A3AFL,0x4544B7EFL,0xB9661C57L,0x5D3B6BBEL,(-9L),(-9L),0x5D3B6BBEL},{0xD4DB1020L,0xA8B29A7AL,0xA8B29A7AL,0xD4DB1020L,1L,1L,(-1L),0x0BA30E1AL},{0xA8B29A7AL,1L,0L,(-1L),5L,0x4544B7EFL,0xC758EBDBL,0L},{0xB6C18C55L,1L,(-1L),0x5D3B6BBEL,0x0FC63D13L,1L,5L,0xB6C18C55L},{1L,0xA8B29A7AL,(-1L),0x4544B7EFL,0xA075B4BFL,(-9L),0xA8B29A7AL,(-1L)},{0x4CCF3668L,0x6238A3AFL,(-1L),0x389D2A99L,(-1L),0xA8B29A7AL,0x5D3B6BBEL,1L}},{{0L,0xE8241FA6L,(-1L),(-9L),(-9L),0L,0xB6C18C55L,0x8B9751EBL},{0xE8241FA6L,0x4CCF3668L,1L,4L,(-9L),(-1L),0xA075B4BFL,0xC758EBDBL},{0L,(-1L),0x389D2A99L,0x8B9751EBL,(-1L),(-1L),0x5F81D65AL,0L},{0x5D3B6BBEL,(-1L),0x0BC12726L,(-1L),(-1L),(-1L),(-1L),0x0BC12726L},{0x0FC63D13L,0x0FC63D13L,0xB6C18C55L,0xC758EBDBL,0xD4DB1020L,0xA4401608L,(-1L),(-1L)},{0xFAB865A3L,1L,(-1L),(-1L),1L,(-9L),1L,(-1L)}},{{1L,0x4CCF3668L,0x804C974CL,0xC758EBDBL,0x5F81D65AL,0x8B9751EBL,(-1L),0x0BC12726L},{0L,(-9L),(-9L),(-1L),0xE8241FA6L,0L,1L,0L},{0xA4401608L,0x8B9751EBL,1L,0x8B9751EBL,0xA4401608L,0x4544B7EFL,(-1L),0xC758EBDBL},{0xA8B29A7AL,0x5D3B6BBEL,1L,4L,(-1L),0xA4401608L,0x0FC63D13L,0x8B9751EBL},{0x389D2A99L,(-1L),1L,(-9L),1L,(-1L),(-1L),1L},{(-1L),0x5F81D65AL,1L,0xC758EBDBL,0x6238A3AFL,0xE8241FA6L,1L,0x804C974CL}},{{0xFAB865A3L,(-1L),(-9L),0xB6C18C55L,0xB6C18C55L,(-9L),(-1L),0xFAB865A3L},{0x5D3B6BBEL,0x8B9751EBL,0x804C974CL,0xE8241FA6L,0x0FC63D13L,0xA8B29A7AL,1L,0x0BC12726L},{0xA8B29A7AL,0xA4401608L,(-1L),5L,0xE8241FA6L,0xA8B29A7AL,(-1L),0x8B9751EBL},{(-1L),0x8B9751EBL,0xB6C18C55L,0L,(-9L),(-9L),(-1L),0xE8241FA6L},{0x8B9751EBL,(-1L),0x0BC12726L,4L,0x5F81D65AL,0xE8241FA6L,0x5F81D65AL,4L},{0x389D2A99L,0x5F81D65AL,0x389D2A99L,(-1L),0xB6C18C55L,(-1L),0xA075B4BFL,0x389D2A99L}},{{0x0FC63D13L,(-1L),1L,0xE8241FA6L,0x0BA30E1AL,0xA4401608L,0xB6C18C55L,0x804C974CL},{0x0FC63D13L,0x5D3B6BBEL,(-1L),1L,0xB6C18C55L,0x4544B7EFL,0x5D3B6BBEL,(-1L)},{0x389D2A99L,0x8B9751EBL,(-1L),(-1L),0x5F81D65AL,0L,1L,1L},{0x8B9751EBL,(-9L),5L,5L,(-9L),0x8B9751EBL,1L,4L},{(-1L),0x4CCF3668L,(-1L),0x8B9751EBL,0xE8241FA6L,(-9L),0xA075B4BFL,(-1L)},{0xA8B29A7AL,1L,0x0BC12726L,0x389D2A99L,0xD4DB1020L,(-1L),0xB9661C57L,(-1L)}}};
        int32_t l_3665[5] = {4L,4L,4L,4L,4L};
        float l_3752 = (-0x10.6p+1);
        uint64_t *l_3759 = &g_2990[2];
        uint32_t **l_3771 = &g_1640;
        uint32_t *****l_3831[3][9][9] = {{{&g_941,&g_941,&g_941,(void*)0,&g_941,(void*)0,&g_941,&g_941,(void*)0},{(void*)0,(void*)0,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941},{&g_941,&g_941,(void*)0,&g_941,(void*)0,&g_941,&g_941,&g_941,&g_941},{(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0},{&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,(void*)0,(void*)0,&g_941,(void*)0,&g_941},{&g_941,(void*)0,&g_941,(void*)0,&g_941,&g_941,&g_941,(void*)0,&g_941},{(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941}},{{(void*)0,&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941},{&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941},{(void*)0,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941,(void*)0},{(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941},{&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941,(void*)0,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0}},{{&g_941,&g_941,&g_941,&g_941,(void*)0,(void*)0,&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941,(void*)0,&g_941,(void*)0,&g_941,&g_941,(void*)0},{(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,(void*)0,&g_941},{(void*)0,(void*)0,&g_941,(void*)0,&g_941,&g_941,(void*)0,&g_941,&g_941},{&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941},{&g_941,(void*)0,&g_941,(void*)0,&g_941,&g_941,&g_941,(void*)0,&g_941},{&g_941,&g_941,(void*)0,(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941},{&g_941,(void*)0,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941,&g_941},{&g_941,&g_941,(void*)0,&g_941,&g_941,&g_941,(void*)0,&g_941,&g_941}}};
        int i, j, k;
        for (g_47 = 0; (g_47 == 10); g_47++)
        { /* block id: 1705 */
            const uint32_t l_3653 = 0x51A1CA26L;
            int16_t l_3658 = 0xBD1BL;
            int16_t *l_3659[1][9][7] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654,&l_3654},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
            int i, j, k;
            l_3654 = (safe_add_func_int16_t_s_s(((void*)0 == (*g_3246)), l_3653));
            l_3665[0] = (safe_lshift_func_uint8_t_u_s((((((((*g_488) = l_3657) != l_3658) > ((l_3661[1][4][1] = (l_3660 = ((*l_3430) |= l_3653))) | l_3653)) <= ((*g_2693) != (((*g_1943) < (g_3662 , (l_3657 <= (1L == (*g_354))))) , l_3663[3][8]))) & (***g_1941)) | l_3657), 3));
        }
        if ((!((void*)0 != (*g_411))))
        { /* block id: 1713 */
            const uint32_t l_3671[8] = {5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL};
            float *****l_3681 = &l_3663[3][5];
            int32_t l_3750 = 0x4E428ECCL;
            int32_t l_3751 = 0xD42EE00AL;
            uint32_t **l_3770 = (void*)0;
            uint32_t l_3772 = 0UL;
            uint8_t l_3774 = 1UL;
            uint32_t ***l_3797 = (void*)0;
            uint32_t ***l_3798[8] = {&l_3770,(void*)0,&l_3770,&l_3770,(void*)0,&l_3770,&l_3770,(void*)0};
            int i;
            for (g_926 = (-26); (g_926 == 13); g_926++)
            { /* block id: 1716 */
                uint32_t l_3679 = 18446744073709551615UL;
                int16_t *l_3683 = (void*)0;
                int16_t *l_3684 = (void*)0;
                int16_t *l_3685[5][10] = {{(void*)0,&g_478,&g_392,&g_392,&g_478,(void*)0,&g_1654[0][1][0],&g_1654[0][1][0],(void*)0,&g_478},{&g_478,&g_392,&g_392,&g_478,(void*)0,&g_1654[0][1][0],&g_1654[0][1][0],(void*)0,&g_478,&g_392},{&g_478,&g_478,&g_1654[0][1][0],&g_392,(void*)0,(void*)0,&g_392,&g_1654[0][1][0],&g_478,&g_478},{(void*)0,&g_392,&g_1654[0][1][0],&g_478,&g_478,&g_1654[0][1][0],&g_392,(void*)0,(void*)0,&g_392},{(void*)0,&g_478,&g_392,&g_392,&g_478,(void*)0,&g_1654[0][1][0],&g_1654[0][1][0],(void*)0,&g_478}};
                int32_t l_3686 = 0x76EF1059L;
                int32_t l_3687 = (-10L);
                int64_t *l_3688 = &g_3242[0][1];
                float l_3706[10][3];
                int i, j;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_3706[i][j] = 0x0.1p-1;
                }
                (**g_3248) = (((safe_mul_func_float_f_f(((**g_395) = (l_3671[3] != (((0x9.AA9962p+24 >= ((safe_div_func_float_f_f((((****g_2459) < ((*l_3688) ^= ((l_3687 = (l_3686 = ((l_3661[3][1][6] ^ ((9UL && ((safe_mul_func_uint8_t_u_u(((((*g_494) = ((((~((*l_3430) ^= (l_3671[1] | (((void*)0 != &g_1535[1][1][0]) , ((((l_3679 | (*g_494)) >= l_3671[6]) , &g_2694) != l_3681))))) , l_3665[0]) < l_3679) <= (***g_2855))) >= l_3671[3]) > l_3660), l_3682)) <= (**g_1942))) && 0xAFL)) != 0x073A72A525B8FFF9LL))) > l_3671[0]))) , 0xF.74BD95p+56), l_3689)) < 0x0.Fp+1)) == l_3660) == l_3665[1]))), 0x0.A978E8p+51)) , (***g_2460)) , &l_3547);
                for (g_65 = 0; g_65 < 10; g_65 += 1)
                {
                    g_1092[g_65] = 1UL;
                }
            }
            l_3660 ^= (safe_mod_func_uint16_t_u_u(((***g_1941) = (l_3661[0][0][4] || ((((void*)0 != &g_2854) & (0xF5ABL & (safe_add_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((++(*l_3759)), (((*g_422) = (safe_div_func_int32_t_s_s(0L, ((****g_3247) = (safe_mul_func_int8_t_s_s(((((*****g_2087) , (0xFBL == (safe_rshift_func_int16_t_s_s(((((((safe_lshift_func_int8_t_s_u((safe_add_func_int64_t_s_s((~((((l_3799[4][5] = (l_3771 = &g_1640)) != &g_1640) , 0xA07B37C0BAF52AE6LL) , 0x9F9E1256L)), 0L)), g_3800)) && 0x71L) || 0xEC231BC0L) && l_3665[3]) && 4294967295UL) <= 4294967295UL), l_3801)))) && l_3774) == 1L), l_3665[0])))))) > l_3774))), 0UL)))) , 0xAA30L))), l_3657));
            (**g_3249) = (~9L);
        }
        else
        { /* block id: 1765 */
            float l_3823 = 0x1.3p+1;
            const int32_t l_3830 = (-1L);
            int32_t l_3835 = 1L;
            int32_t l_3836 = 0x24147E98L;
            uint16_t l_3837 = 0x87A9L;
            for (g_549 = (-22); (g_549 < 7); ++g_549)
            { /* block id: 1768 */
                int16_t l_3812 = (-1L);
                uint16_t *l_3832 = (void*)0;
                uint16_t *l_3833 = (void*)0;
                uint16_t *l_3834 = &g_621;
                uint32_t **l_3840[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_3840[i] = &g_138;
                (**g_3249) &= (l_3835 ^= (l_3657 , (safe_mul_func_uint16_t_u_u((*****g_2087), (~(safe_lshift_func_uint8_t_u_s((safe_add_func_int32_t_s_s((l_3812 ^ (safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u(((((*l_3834) = (l_3657 <= (safe_sub_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u(((++(**g_1942)) <= (((l_3812 || ((*l_3430) = ((safe_add_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(l_3657, 3L)), ((safe_mod_func_uint8_t_u_u(l_3830, 0x81L)) == l_3830))) | 0xAE5E908D84FCE933LL))) , l_3831[1][8][8]) == (void*)0)), l_3830)), l_3812)))) || l_3830) , l_3657), (-7L))), (*g_422)))), l_3812)), l_3660)))))));
                --l_3837;
                if (l_3812)
                    continue;
                l_3841[0] = l_3840[1];
            }
        }
    }
    (*l_3843) = l_3842;
    return l_3844;
}


/* ------------------------------------------ */
/* 
 * reads : g_1304
 * writes: g_1304
 */
static int8_t  func_8(uint64_t  p_9, uint32_t  p_10, float  p_11, uint8_t  p_12)
{ /* block id: 1605 */
    volatile union U0 **l_3428[7][8] = {{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]},{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]},{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]},{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]},{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]},{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]},{&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1],&g_1304[0][1],(void*)0,(void*)0,&g_1304[0][1]}};
    int32_t l_3429 = 1L;
    int i, j;
    g_1304[0][1] = g_1304[0][1];
    return l_3429;
}


/* ------------------------------------------ */
/* 
 * reads : g_3246 g_3247 g_3248 g_3249 g_47
 * writes: g_3041
 */
static int32_t  func_20(float  p_21, int16_t  p_22, int8_t  p_23, int8_t  p_24, int16_t  p_25)
{ /* block id: 1599 */
    int32_t *l_3426 = &g_47;
    l_3426 = ((****g_3246) = l_3426);
    return (*l_3426);
}


/* ------------------------------------------ */
/* 
 * reads : g_488 g_1330 g_253 g_1960 g_475 g_395 g_396
 * writes: g_253 g_1330 g_1960 g_475 g_288
 */
static int8_t  func_27(int64_t  p_28, uint64_t  p_29, int32_t  p_30, const uint32_t  p_31)
{ /* block id: 940 */
    uint16_t **l_2053[1][6] = {{&g_1943,&g_1943,&g_1943,&g_1943,&g_1943,&g_1943}};
    uint16_t ***l_2052[1][2];
    uint16_t ****l_2051[4][3] = {{(void*)0,&l_2052[0][1],(void*)0},{&l_2052[0][1],&l_2052[0][1],&l_2052[0][1]},{(void*)0,&l_2052[0][1],(void*)0},{&l_2052[0][1],&l_2052[0][1],&l_2052[0][1]}};
    uint8_t l_2067 = 255UL;
    uint32_t l_2071[6] = {0xDFD6E861L,0xDFD6E861L,0xDFD6E861L,0xDFD6E861L,0xDFD6E861L,0xDFD6E861L};
    int32_t l_2115 = 0L;
    int32_t l_2128 = 0L;
    int32_t l_2131 = 0L;
    int32_t l_2133 = 1L;
    int32_t l_2134 = 0xFA593587L;
    int32_t l_2135 = 4L;
    float l_2139[3];
    int32_t ** const l_2231 = &g_488;
    uint32_t l_2232 = 0xB65433B5L;
    uint16_t l_2317[9] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    int8_t *l_2377[9] = {&g_548,(void*)0,&g_548,&g_548,(void*)0,&g_548,&g_548,(void*)0,&g_548};
    uint32_t l_2387 = 0xFFFA565FL;
    int32_t *l_2390 = &g_65;
    float ***l_2415 = &g_395;
    uint64_t ***l_2518 = &g_1156;
    uint32_t l_2610 = 0x33C1F3ABL;
    int16_t *l_2615 = &g_538;
    const int8_t l_2666[6] = {0xB3L,0xB3L,0xB3L,0xB3L,0xB3L,0xB3L};
    uint8_t ***l_2692 = (void*)0;
    int32_t l_2734 = 0L;
    const uint8_t l_2785[5] = {247UL,247UL,247UL,247UL,247UL};
    const float *****l_2803 = (void*)0;
    uint32_t l_2822 = 0UL;
    int64_t l_2917 = 0xA4EEA3259E9505AALL;
    uint8_t l_2969 = 0x28L;
    const int32_t l_2972 = 0L;
    int64_t *** const l_2978 = &g_493[0][5][2];
    int32_t l_3001 = 1L;
    int16_t l_3142[5] = {0L,0L,0L,0L,0L};
    int32_t l_3176 = 0x450562DBL;
    uint32_t l_3198 = 0xFC88EDF3L;
    int32_t **l_3220 = &g_3041[1][2];
    int32_t ***l_3219 = &l_3220;
    int32_t ****l_3218 = &l_3219;
    int32_t *****l_3217[8][6] = {{&l_3218,(void*)0,(void*)0,&l_3218,&l_3218,&l_3218},{&l_3218,&l_3218,&l_3218,&l_3218,&l_3218,&l_3218},{&l_3218,&l_3218,&l_3218,&l_3218,&l_3218,&l_3218},{&l_3218,&l_3218,&l_3218,&l_3218,&l_3218,&l_3218},{(void*)0,(void*)0,&l_3218,&l_3218,&l_3218,&l_3218},{&l_3218,&l_3218,&l_3218,&l_3218,&l_3218,&l_3218},{&l_3218,&l_3218,&l_3218,&l_3218,(void*)0,&l_3218},{&l_3218,&l_3218,&l_3218,&l_3218,&l_3218,&l_3218}};
    uint32_t l_3235 = 0xBC4BE58EL;
    uint32_t **l_3285 = &g_138;
    union U0 *l_3314 = &g_3315;
    union U0 **l_3313 = &l_3314;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_2052[i][j] = &l_2053[0][2];
    }
    for (i = 0; i < 3; i++)
        l_2139[i] = 0x1.8p-1;
    (*g_488) = (-3L);
    for (g_1330 = 0; (g_1330 <= 6); g_1330 += 1)
    { /* block id: 944 */
        int32_t *l_2050 = &g_1960[0][0][0];
        int16_t * const l_2070 = &g_538;
        int32_t l_2126 = 0xD459D645L;
        int32_t l_2137 = 8L;
        uint8_t l_2153 = 0xAEL;
        uint16_t **** const *l_2244 = &l_2051[1][0];
        uint64_t *l_2273 = (void*)0;
        int32_t l_2309 = 0L;
        int32_t l_2310 = 6L;
        int32_t l_2311 = (-1L);
        int32_t l_2313 = 0xEDCE407FL;
        int32_t l_2314 = (-8L);
        int32_t l_2316 = 0xA5E83C2FL;
        const uint16_t l_2355 = 1UL;
        union U0 *l_2368 = &g_2369;
        uint64_t l_2382 = 0x1E6CAA82D92BD2ACLL;
        int8_t l_2486 = 1L;
        (*l_2050) |= (*g_488);
    }
    for (g_475 = 0; (g_475 < 3); g_475++)
    { /* block id: 1169 */
        int64_t l_2528 = (-6L);
        int8_t l_2529 = 0xB8L;
        uint64_t *** const * const l_2562 = &l_2518;
        uint64_t *** const * const *l_2561 = &l_2562;
        int16_t *l_2624 = &g_1665;
        int32_t l_2628 = 0x94360228L;
        int32_t l_2635 = 0x7B270177L;
        int32_t l_2639 = (-7L);
        int32_t l_2640 = 0x17CC9282L;
        int32_t l_2641[7] = {(-1L),(-1L),0x788B1782L,(-1L),(-1L),0x788B1782L,(-1L)};
        uint8_t ***l_2691 = (void*)0;
        uint16_t l_2727 = 9UL;
        int32_t **l_2743 = (void*)0;
        int32_t ***l_2742 = &l_2743;
        int32_t ****l_2741 = &l_2742;
        int32_t *****l_2740 = &l_2741;
        int8_t **l_2816 = &l_2377[1];
        union U0 *l_2819 = (void*)0;
        const int16_t l_2821[5] = {(-3L),(-3L),(-3L),(-3L),(-3L)};
        uint64_t l_2829 = 0xB2A2CB73A7AD93CDLL;
        int32_t *l_2867[9] = {&g_1657[0],&g_1657[0],&g_1657[3],&g_1657[0],&g_1657[0],&g_1657[3],&g_1657[0],&g_1657[0],&g_1657[3]};
        int32_t *l_2868 = (void*)0;
        int32_t l_2970 = 0x901364F4L;
        int64_t ***l_2977 = &g_493[0][5][2];
        uint8_t l_3053 = 0xA9L;
        uint32_t ****l_3114 = &g_942[4][5];
        uint8_t l_3215 = 0x2AL;
        float l_3257 = (-0x1.Dp+1);
        uint32_t **l_3339 = &g_1640;
        uint32_t l_3379 = 9UL;
        int i;
    }
    (***l_2415) = p_28;
    return p_28;
}


/* ------------------------------------------ */
/* 
 * reads : g_17 g_67 g_47 g_65 g_480 g_1277 g_181 g_275 g_1092 g_1276 g_548 g_1157 g_395 g_396 g_475 g_478 g_319 g_320 g_549 g_454 g_1519 g_1535 g_1156 g_1548 g_488 g_253 g_1565 g_110 g_926 g_332 g_1234 g_1330 g_1592 g_422 g_393 g_63 g_138 g_492 g_493 g_1213 g_1029 g_1384 g_690 g_722 g_288 g_1355 g_1921 g_1935 g_1941 g_1942 g_1943 g_1629 g_1640 g_494 g_746
 * writes: g_67 g_47 g_480 g_275 g_438 g_475 g_454 g_288 g_181 g_1519 g_343 g_1548 g_1234 g_253 g_332 g_1330 g_1384 g_393 g_690 g_1935 g_404 g_548 g_171 g_1092
 */
static int16_t  func_34(int32_t  p_35, const int32_t  p_36, int32_t  p_37, float  p_38, const uint32_t  p_39)
{ /* block id: 1 */
    int32_t *l_46[6] = {&g_47,&g_47,&g_47,&g_47,&g_47,&g_47};
    int32_t **l_2023 = &l_46[1];
    uint16_t **l_2024 = (void*)0;
    int32_t l_2027 = 0x84223BD7L;
    uint32_t ** const * const l_2032[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t ** const * const *l_2031 = &l_2032[3];
    uint32_t ** const * const ** const l_2030 = &l_2031;
    uint8_t *l_2037 = &g_1092[7];
    uint16_t l_2038 = 65535UL;
    int i;
    (*l_2023) = func_40((g_17[3] & (p_37 && (func_42(l_46[2], (p_37 = g_17[1]), l_46[3]) > ((safe_unary_minus_func_uint8_t_u(p_36)) == (0xA0L ^ (0x31L > 0x97L)))))));
    l_2038 |= (((((void*)0 == l_2024) , ((*l_2037) = ((((0xBA31D72FE29E97EALL != l_2027) < ((safe_lshift_func_uint16_t_u_s((**g_1942), 7)) == ((((void*)0 == l_2030) != (safe_sub_func_int32_t_s_s((*g_488), (safe_rshift_func_int8_t_s_u(((((*g_488) >= (-6L)) != (-9L)) > 0x6AL), 6))))) , p_36))) == (*g_488)) <= p_35))) == p_35) & (**l_2023));
    (*g_488) |= (safe_add_func_int8_t_s_s(((((*g_422) != ((safe_rshift_func_uint16_t_u_s(p_39, p_36)) >= ((*l_2037)++))) == 0x47C9L) & (safe_mul_func_uint8_t_u_u((**g_1276), (*g_422)))), (safe_sub_func_int64_t_s_s(((g_690 = ((0x84EBD110A40F5BDDLL > (**l_2023)) == (((0L | p_39) , p_35) || (**l_2023)))) , p_36), p_35))));
    return p_39;
}


/* ------------------------------------------ */
/* 
 * reads : g_488
 * writes: g_253 g_926 g_746
 */
static int32_t * func_40(uint8_t  p_41)
{ /* block id: 930 */
    uint8_t l_2021 = 1UL;
    int32_t *l_2022 = &g_746;
    (*g_488) = l_2021;
    return l_2022;
}


/* ------------------------------------------ */
/* 
 * reads : g_67 g_47 g_65 g_480 g_1277 g_181 g_275 g_1092 g_1276 g_548 g_1157 g_395 g_396 g_475 g_478 g_319 g_320 g_549 g_454 g_1519 g_1535 g_1156 g_1548 g_488 g_253 g_1565 g_110 g_926 g_332 g_1234 g_1330 g_1592 g_422 g_393 g_63 g_138 g_492 g_493 g_1213 g_1029 g_1384 g_690 g_722 g_288 g_1355 g_1921 g_1935 g_1941 g_1942 g_1943 g_1629 g_1640 g_494
 * writes: g_67 g_47 g_480 g_275 g_438 g_475 g_454 g_288 g_181 g_1519 g_343 g_1548 g_1234 g_253 g_332 g_1330 g_1384 g_393 g_690 g_1935 g_404 g_548 g_171
 */
static uint32_t  func_42(int32_t * p_43, int32_t  p_44, const int32_t * p_45)
{ /* block id: 3 */
    int32_t *l_48 = &g_47;
    int32_t *l_49 = &g_47;
    int32_t *l_50 = &g_47;
    int32_t *l_51 = &g_47;
    int32_t *l_52 = (void*)0;
    int32_t l_53 = (-10L);
    int32_t *l_54 = &l_53;
    int32_t *l_55 = &g_47;
    int32_t *l_56 = (void*)0;
    int32_t *l_57 = &g_47;
    int32_t *l_58 = &l_53;
    int32_t *l_59 = &l_53;
    int32_t *l_60 = &l_53;
    int32_t l_61[6][4] = {{0x7C74E2B7L,0x7C74E2B7L,1L,1L},{0x7C74E2B7L,0x7C74E2B7L,1L,1L},{0x7C74E2B7L,0x7C74E2B7L,1L,1L},{0x7C74E2B7L,0x7C74E2B7L,1L,1L},{0x7C74E2B7L,0x7C74E2B7L,1L,1L},{0x7C74E2B7L,0x7C74E2B7L,1L,1L}};
    int32_t *l_62 = &g_47;
    int32_t *l_64[5];
    uint8_t l_1562 = 0x92L;
    int32_t l_1563[5][8] = {{0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L},{0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L},{0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L},{0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L},{0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L,0x740B55F9L}};
    int8_t l_1766 = 0xA5L;
    float ****l_1812[5];
    int16_t l_1954 = 7L;
    int32_t l_1964 = (-1L);
    uint32_t l_1970 = 0xDA80C2F1L;
    uint16_t *l_1989[4][1][1];
    float l_1990 = 0xE.EC8FEDp-32;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_64[i] = &l_53;
    for (i = 0; i < 5; i++)
        l_1812[i] = (void*)0;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
                l_1989[i][j][k] = &g_1384[3][0][3];
        }
    }
lbl_1938:
    g_67--;
    if ((safe_lshift_func_uint8_t_u_u(4UL, (*l_48))))
    { /* block id: 5 */
        int32_t *l_81 = &l_61[4][3];
        int16_t *l_82[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint32_t *l_87 = &g_67;
        uint8_t *l_1559 = &g_1234;
        const int32_t l_1564 = (-1L);
        uint16_t l_1765 = 0x3A12L;
        float ***l_1794 = &g_395;
        int32_t **l_1815[3];
        int32_t ***l_1814 = &l_1815[0];
        int32_t l_1859[8] = {4L,0x8A91567DL,0x8A91567DL,4L,0x8A91567DL,0x8A91567DL,4L,0x8A91567DL};
        uint64_t * const *l_1865 = &g_1157;
        uint64_t * const **l_1864 = &l_1865;
        uint32_t l_1923 = 4294967295UL;
        uint32_t **l_1944 = &g_1640;
        int i;
        for (i = 0; i < 3; i++)
            l_1815[i] = &l_50;
        (**g_395) = (((((((l_1765 ^= ((safe_rshift_func_uint8_t_u_s(g_65, 1)) | func_74((safe_unary_minus_func_int16_t_s((((((*l_81) = (l_81 == l_81)) | ((((safe_mul_func_uint8_t_u_u(func_85(((*l_51) ^= (--(*l_87)))), (safe_lshift_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u(((((((*l_1559) = (p_44 ^ 0x5C8B2B5BL)) , ((p_44 & (*g_488)) , (safe_sub_func_uint16_t_u_u((((l_1562 <= p_44) , p_44) >= p_44), l_1563[3][4])))) | (*g_488)) & p_44) || 0x2B4E2036F7401CB8LL), (*l_54))) < p_44), l_1564)))) ^ 6L) > (*g_488)) , 0UL)) > 65535UL) != p_44))), (*l_60), g_1565, g_110, p_44))) & (*l_60)) , g_549) > 0xEE88L) != l_1766) , (void*)0) == p_45);
        for (g_332 = 0; (g_332 == (-11)); --g_332)
        { /* block id: 819 */
            float l_1803 = 0x6.36BBC1p+42;
            int32_t l_1808[3];
            int32_t ***l_1809 = (void*)0;
            int64_t **l_1838 = &g_494;
            int8_t l_1863 = 0x54L;
            float ** const l_1919 = &g_396[0];
            int i;
            for (i = 0; i < 3; i++)
                l_1808[i] = 0x3B79A344L;
            if ((*l_81))
            { /* block id: 820 */
                const uint8_t l_1793[9] = {6UL,6UL,0xBFL,6UL,6UL,0xBFL,6UL,6UL,0xBFL};
                float * const *l_1797 = (void*)0;
                float * const ** const l_1796 = &l_1797;
                int i;
                (*p_43) &= (p_45 == p_45);
                if ((((0xF44BL <= (0x57L < (safe_lshift_func_uint8_t_u_s((++(*l_1559)), (*l_81))))) , (*l_51)) & (-9L)))
                { /* block id: 823 */
                    uint32_t l_1773 = 0xB77D260EL;
                    return l_1773;
                }
                else
                { /* block id: 825 */
                    const float ****l_1813 = (void*)0;
                    for (g_1330 = (-7); (g_1330 <= 16); g_1330 = safe_add_func_int64_t_s_s(g_1330, 5))
                    { /* block id: 828 */
                        float ***l_1795 = (void*)0;
                        int64_t l_1798 = 0x457D8DB2A3B87101LL;
                        int32_t l_1799 = (-8L);
                        l_1799 |= ((((((safe_sub_func_uint16_t_u_u(p_44, (safe_mul_func_int8_t_s_s(p_44, (-5L))))) & (safe_lshift_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s((safe_mod_func_int8_t_s_s(((~(safe_lshift_func_uint8_t_u_u((**g_1276), (safe_mod_func_uint64_t_u_u(18446744073709551606UL, (safe_sub_func_int64_t_s_s((l_1793[6] && (p_44 >= l_1793[7])), (((l_1795 = l_1794) != l_1796) > p_44)))))))) || 1UL), 0x58L)), (-1L))) >= 4L), g_1592[5]))) , 0x412E9160D90DDEDBLL) >= l_1798) , (*g_422)) > (-2L));
                        (*l_60) &= ((safe_mod_func_int8_t_s_s((safe_unary_minus_func_int8_t_s((*g_319))), (*g_422))) , ((*p_43) = ((safe_mul_func_uint16_t_u_u(0x47E0L, ((safe_lshift_func_int16_t_s_u((l_1808[2] = l_1808[2]), l_1798)) || 65529UL))) & ((l_1809 == (((safe_add_func_int64_t_s_s((*l_81), (l_1799 &= (l_1812[0] != l_1813)))) > p_44) , l_1814)) < (*g_488)))));
                        if ((*g_488))
                            continue;
                    }
                }
                (*l_48) &= (-1L);
                (*p_43) &= ((*l_81) < (-5L));
            }
            else
            { /* block id: 840 */
                int64_t *l_1830 = &g_1658;
                int32_t l_1837 = 0L;
                float **l_1920 = (void*)0;
                int32_t *l_1922 = &g_1657[3];
                for (p_44 = 0; (p_44 <= 3); p_44 += 1)
                { /* block id: 843 */
                    uint16_t *l_1820 = &g_1384[6][1][3];
                    int32_t l_1839[9][7] = {{0x27964C33L,0xB69A5C11L,1L,0xB69A5C11L,0x27964C33L,1L,0xB69A5C11L},{0xB84EA009L,0x6BAA4F03L,0L,0xB69A5C11L,0x5C354582L,0L,0L},{0x5C354582L,0L,(-1L),(-1L),0L,0x5C354582L,0x6BAA4F03L},{0xB84EA009L,0xB69A5C11L,0xF3682F49L,0xB84EA009L,0L,0x0F5E55E7L,0xB69A5C11L},{0x27964C33L,7L,0x5C354582L,0x0AAD175DL,0x5C354582L,7L,0x27964C33L},{7L,0xB69A5C11L,(-1L),0x5C354582L,0x27964C33L,7L,0x5C354582L},{0xB84EA009L,0L,0x0F5E55E7L,0xB69A5C11L,0xB69A5C11L,0x0F5E55E7L,0L},{0xB69A5C11L,0x6BAA4F03L,(-1L),0x0AAD175DL,0x6BAA4F03L,0x5C354582L,0L},{0L,0xB69A5C11L,0x5C354582L,0L,0L,0L,0x5C354582L}};
                    int32_t l_1866 = (-5L);
                    int i, j;
                    if ((safe_lshift_func_uint8_t_u_s((*g_1277), (safe_sub_func_uint16_t_u_u((((((*l_1820) = ((void*)0 == p_43)) ^ p_44) & (!((*g_488) = (safe_lshift_func_uint16_t_u_s(g_63, (safe_add_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(((*l_81) ^= (safe_mod_func_uint64_t_u_u((((void*)0 == l_1830) < (((((((safe_mul_func_int16_t_s_s((+(++(*g_138))), p_44)) & (((*g_422) = (+l_1837)) || (*g_422))) >= p_44) , (**g_1276)) >= p_44) , (*g_492)) != l_1838)), l_1839[0][1]))), (*p_43))), 0x93L))))))) , 0xEBB2L), g_1213[1])))))
                    { /* block id: 849 */
                        uint16_t l_1840[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1840[i] = 1UL;
                        (*l_51) = l_1840[1];
                    }
                    else
                    { /* block id: 851 */
                        uint32_t * const *l_1849 = &g_1640;
                        int32_t l_1852[2][6][9] = {{{0x7B5BBFBBL,0L,0xF2A2F6C8L,0x837E3C3BL,0xE64F179CL,0x4AE5DB12L,(-10L),(-8L),2L},{0x6A862A95L,0x4AE5DB12L,0xF2A2F6C8L,0x87491ACBL,8L,(-3L),(-3L),8L,0x87491ACBL},{2L,(-7L),2L,8L,1L,0x2BEF9C44L,0xF2D03070L,0L,(-3L)},{8L,(-1L),0x4AE5DB12L,(-1L),2L,1L,0xF2A2F6C8L,(-1L),1L},{0L,0L,1L,0x6F9481EDL,(-8L),0L,0xCE7628C2L,0L,0x3594E2DBL},{0L,0x6E887F9DL,8L,(-1L),0xAF7F6375L,(-8L),0xD036F791L,0xE64F179CL,0x9B5F507BL}},{{0x87491ACBL,5L,0xE64F179CL,0xF2D03070L,0xAF7F6375L,(-1L),(-10L),(-10L),(-1L)},{(-8L),0x7B5BBFBBL,(-3L),0x7B5BBFBBL,(-8L),0xCE7628C2L,1L,0L,1L},{1L,0L,0x289BE616L,(-1L),0L,0L,0xDF3CE1CDL,0xB9BEF7C9L,2L},{0xDF3CE1CDL,0xF2D03070L,0xD036F791L,0xF2A2F6C8L,0x1C8F1DB1L,0xCE7628C2L,0L,0x2BEF9C44L,0xEAB9FE82L},{0x2BEF9C44L,0L,0xAF7F6375L,(-7L),0x6F9481EDL,(-1L),(-3L),1L,0x84658ED2L},{1L,0xD036F791L,0x6F9481EDL,0L,(-1L),(-8L),(-3L),(-7L),(-7L)}}};
                        int i, j, k;
                        (*p_43) = (p_44 || (safe_mod_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s((safe_div_func_int16_t_s_s((l_1852[0][4][5] = (0x23FEC9A5L && (((((*l_1820) = l_1837) , 0xDAD02A64L) <= (safe_sub_func_uint8_t_u_u(((((p_44 , &g_1640) == l_1849) ^ (safe_lshift_func_int16_t_s_u(l_1852[1][5][5], 8))) & (safe_sub_func_int32_t_s_s((safe_add_func_int16_t_s_s((safe_mod_func_int32_t_s_s(0L, (*g_488))), (-6L))), (*p_45)))), p_44))) | p_44))), l_1837)), 8)) != 1UL), l_1859[6])));
                        if ((*p_45))
                            continue;
                        (*l_59) ^= (*p_45);
                        return p_44;
                    }
                    l_1866 &= ((safe_unary_minus_func_uint32_t_u((0UL <= (l_1837 >= (safe_lshift_func_int16_t_s_u((((*g_1277) , (l_1839[0][1] , ((p_44 <= ((*g_1157) |= l_1863)) == ((*l_60) |= (*l_50))))) | (&g_238 != l_1864)), g_1029)))))) < 0x82AD6C73L);
                    for (g_690 = 3; (g_690 >= 0); g_690 -= 1)
                    { /* block id: 864 */
                        const float **l_1877 = (void*)0;
                        int i, j, k;
                        (*l_81) = (((((*l_1820)--) , g_1384[(p_44 + 2)][g_690][(g_690 + 1)]) , ((((-1L) < ((**l_1865) = (safe_unary_minus_func_int64_t_s((safe_mod_func_uint32_t_u_u(((0xBEL && (g_1384[(p_44 + 1)][p_44][(g_690 + 1)] , (safe_div_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(g_722, (+(0x2E89L || 0x4255L)))), (0xF5085A93325C202BLL | (&g_396[1] == l_1877)))))) >= (***l_1814)), 0x3145FFD1L)))))) , p_44) , (*l_60))) || 3UL);
                    }
                    if ((((safe_mul_func_int8_t_s_s((((safe_sub_func_uint64_t_u_u(((p_44 , l_1866) | ((*g_138) = (~(((*l_81) = (*l_57)) >= ((((safe_sub_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((((*l_1559) = (~l_1837)) >= ((safe_mod_func_uint16_t_u_u((((~0x17L) == ((safe_div_func_float_f_f((-((**g_395) = (((safe_add_func_uint32_t_u_u((*l_54), (p_44 < (safe_rshift_func_uint16_t_u_s((safe_div_func_uint64_t_u_u(((*g_1157) = (((((safe_add_func_uint64_t_u_u((safe_div_func_uint8_t_u_u(l_1837, (safe_unary_minus_func_int64_t_s(((((safe_mul_func_int16_t_s_s((safe_rshift_func_int16_t_s_u((safe_mod_func_uint32_t_u_u((safe_div_func_int32_t_s_s(((((safe_add_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f(p_44, (**g_395))), (**g_395))), (**g_395))) > p_44) != 0x0.9F1153p-95) , l_1837), (*g_488))), 0x5F5CC12AL)), 14)), 0x2429L)) <= p_44) , l_1919) != l_1920))))), 0xC435BB2B5E995411LL)) > l_1866) == 18446744073709551614UL) == (*g_319)) , (*g_1157))), p_44)), p_44))))) ^ g_1355) , (**g_395)))), 0x1.D7E556p-5)) , 0x6A47A63BL)) < 0x51F6L), g_1921)) , 255UL)), 0x29C913D78216C784LL)), p_44)) ^ p_44) , l_1922) == &g_332))))), 1UL)) , (***l_1814)) <= 0L), p_44)) > (-1L)) , (*p_43)))
                    { /* block id: 874 */
                        const int32_t *l_1927 = &l_1866;
                        const int32_t **l_1926 = &l_1927;
                        uint16_t ***l_1928 = (void*)0;
                        uint16_t ***l_1929 = (void*)0;
                        uint16_t **l_1931 = &l_1820;
                        uint16_t ***l_1930 = &l_1931;
                        --l_1923;
                        (*l_1926) = p_45;
                        (*l_1930) = (void*)0;
                    }
                    else
                    { /* block id: 878 */
                        uint8_t l_1932 = 251UL;
                        (**g_395) = (((*l_54) = l_1837) >= p_44);
                        (**l_1814) = &g_253;
                        l_1932--;
                    }
                }
                return p_44;
            }
            --g_1935;
            if (g_47)
                goto lbl_1938;
            if ((*p_43))
                break;
        }
        (*g_488) = ((*l_59) &= ((((((***l_1814) = ((**g_395) = (safe_sub_func_float_f_f((*l_81), ((void*)0 == g_1941))))) , (void*)0) != (void*)0) , ((l_1944 != (void*)0) & ((*p_45) | (*l_49)))) & (**g_1156)));
    }
    else
    { /* block id: 895 */
        uint64_t l_1945 = 0x11E1B374D7F11B33LL;
        int32_t l_1955[6];
        int i;
        for (i = 0; i < 6; i++)
            l_1955[i] = (-2L);
        --l_1945;
        for (l_1945 = 0; (l_1945 < 29); ++l_1945)
        { /* block id: 899 */
            const int32_t **l_1950 = (void*)0;
            const int32_t **l_1951 = (void*)0;
            const int32_t *l_1953 = &l_61[4][3];
            const int32_t **l_1952 = &l_1953;
            int32_t l_1956 = (-1L);
            int32_t l_1957 = (-9L);
            int32_t l_1958 = 1L;
            int32_t l_1959 = 0xF94C1D16L;
            int32_t l_1961 = 0L;
            int32_t l_1962 = 0L;
            int32_t l_1963 = 2L;
            int32_t l_1965 = (-1L);
            int32_t l_1966 = 0xD4C50682L;
            int32_t l_1967 = (-10L);
            int32_t l_1968 = 0x49218B31L;
            int32_t l_1969 = 0x414B7F3BL;
            (*l_1952) = p_45;
            l_1970++;
            if ((*p_43))
                break;
        }
        if (l_1945)
            goto lbl_1938;
    }
    (**g_395) = ((safe_sub_func_float_f_f((safe_div_func_float_f_f(((((*g_422) = (safe_div_func_uint16_t_u_u(((safe_lshift_func_int16_t_s_u((((((*g_1157) >= ((safe_rshift_func_int8_t_s_u((safe_sub_func_int16_t_s_s(p_44, (((safe_add_func_int16_t_s_s((-1L), (65528UL && (((*g_1640) = (safe_mul_func_uint16_t_u_u((**g_1942), (((l_1989[3][0][0] != ((*g_1157) , l_1989[2][0][0])) , &g_253) != p_45)))) , 0x6F7EL)))) , 4L) < (*l_62)))), 7)) || (*l_58))) | (*l_54)) == 8L) || p_44), p_44)) <= (*l_59)), (*l_50)))) , 1L) , p_44), p_44)), l_1990)) != (-0x1.4p+1));
    for (g_548 = 0; (g_548 != (-24)); g_548 = safe_sub_func_int16_t_s_s(g_548, 2))
    { /* block id: 911 */
        uint8_t l_1993 = 0x68L;
        const float *l_2014 = &g_2015;
        const float **l_2013 = &l_2014;
        --l_1993;
        for (g_1548 = 0; (g_1548 == 52); ++g_1548)
        { /* block id: 915 */
            return (*l_55);
        }
        (*l_54) ^= (safe_div_func_uint64_t_u_u((safe_sub_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((*l_51) != (safe_add_func_uint32_t_u_u((*g_138), 0x3A1E480BL))), 7)), (*g_1943))), ((((((void*)0 != &l_1563[3][4]) < (safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_u((+(((safe_div_func_int8_t_s_s(((&g_942[4][5] != (void*)0) , (((*l_2013) = (*g_395)) == (void*)0)), (**g_1276))) , p_44) || (-1L))), l_1993)), 8))) != p_44) == (*g_494)) ^ 0UL)));
        for (g_171 = (-16); (g_171 < (-3)); ++g_171)
        { /* block id: 922 */
            int32_t l_2018 = 0L;
            int32_t l_2019 = 0xC99EED66L;
            p_43 = &g_253;
            l_2019 = ((l_2018 = ((**g_395) = l_1993)) != 0x8.4A22A8p+80);
        }
    }
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_1276 g_1277 g_181 g_488 g_253 g_480 g_926
 * writes: g_253 g_480
 */
static uint64_t  func_74(uint32_t  p_75, const float  p_76, uint8_t  p_77, int32_t  p_78, float  p_79)
{ /* block id: 692 */
    int32_t *l_1567 = &g_1518;
    const float l_1598 = 0xC.35DA5Fp+40;
    int32_t l_1621[3][8][1] = {{{(-1L)},{(-4L)},{0L},{0xEEEA7E9DL},{0L},{(-4L)},{(-1L)},{0x67AEB777L}},{{0L},{0x67AEB777L},{(-1L)},{(-4L)},{0L},{0xEEEA7E9DL},{0L},{(-4L)}},{{(-1L)},{0x67AEB777L},{0L},{0x67AEB777L},{(-1L)},{(-4L)},{0L},{0xEEEA7E9DL}}};
    const int32_t *l_1739 = &g_926;
    int i, j, k;
    if (((**g_1276) == (**g_1276)))
    { /* block id: 693 */
lbl_1569:
        (*g_488) = (*g_488);
        return p_77;
    }
    else
    { /* block id: 696 */
        int32_t l_1566 = 9L;
        int32_t l_1614 = (-1L);
        uint32_t *l_1615 = &g_260;
        int32_t l_1628 = 0xB4628224L;
        uint32_t *l_1639 = &g_438;
        int32_t l_1652 = 0x43507A67L;
        int32_t **l_1701 = &g_488;
        int32_t ***l_1700 = &l_1701;
        uint8_t l_1708 = 247UL;
        for (g_480 = 0; (g_480 <= 3); g_480 += 1)
        { /* block id: 699 */
            int32_t **l_1568 = &l_1567;
            union U0 *l_1570 = &g_1571;
            uint32_t **l_1586 = &g_138;
            int32_t l_1622 = 0x5379564AL;
            int32_t l_1624[5][1] = {{1L},{0xC24C2CA3L},{1L},{0xC24C2CA3L},{1L}};
            int8_t l_1695 = 0x67L;
            int16_t l_1722 = 0x1303L;
            int16_t l_1729 = 1L;
            int32_t l_1740 = 0x7647795CL;
            int i, j;
            if (l_1566)
                break;
            (*l_1568) = l_1567;
            if (g_480)
                goto lbl_1569;
        }
        return (*l_1739);
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_480 g_67 g_1277 g_181 g_275 g_1092 g_1276 g_548 g_1157 g_395 g_396 g_475 g_478 g_319 g_320 g_549 g_454 g_1519 g_1535 g_1156 g_1548
 * writes: g_47 g_480 g_67 g_275 g_438 g_475 g_454 g_288 g_181 g_1519 g_343 g_1548
 */
static uint8_t  func_85(uint32_t  p_86)
{ /* block id: 9 */
    int32_t l_90[7][6] = {{0L,1L,1L,0L,0x47090205L,1L},{0x25687F7FL,0L,0x6366113CL,1L,1L,0x70922B85L},{1L,0x47090205L,0x6B3C8337L,0x47090205L,1L,0xA325A448L},{0x70922B85L,0L,0x456AA7D8L,0L,0x47090205L,0x6366113CL},{0x6366113CL,1L,0L,0L,1L,0x6366113CL},{0L,1L,0x456AA7D8L,1L,0x6366113CL,0xA325A448L},{1L,0x70922B85L,0x6B3C8337L,0x6366113CL,0x6B3C8337L,0x70922B85L}};
    const float l_1271 = 0xC.8F6EB9p+53;
    int32_t l_1282[6][6][1] = {{{0xE23EE63CL},{9L},{9L},{0xE23EE63CL},{0x9BDF80F5L},{(-9L)}},{{(-1L)},{0L},{(-1L)},{(-9L)},{0x9BDF80F5L},{0xE23EE63CL}},{{9L},{9L},{0xE23EE63CL},{0x9BDF80F5L},{(-9L)},{(-1L)}},{{0L},{(-1L)},{(-9L)},{0x9BDF80F5L},{0xE23EE63CL},{9L}},{{9L},{0xE23EE63CL},{0x9BDF80F5L},{(-9L)},{(-1L)},{0L}},{{(-1L)},{(-9L)},{0x9BDF80F5L},{0xE23EE63CL},{9L},{9L}}};
    int64_t ****l_1293 = &g_492;
    int32_t l_1303[10];
    int32_t *l_1342 = &l_1282[4][4][0];
    int8_t *l_1359 = &g_393;
    uint32_t **** const l_1462 = &g_942[4][5];
    int32_t *l_1545 = &l_1282[4][2][0];
    int32_t *l_1546[3];
    int64_t l_1547 = (-3L);
    int32_t l_1551 = 4L;
    uint8_t l_1552 = 255UL;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_1303[i] = (-5L);
    for (i = 0; i < 3; i++)
        l_1546[i] = &l_1303[8];
    if (l_90[6][2])
    { /* block id: 10 */
        int32_t *l_91 = &g_47;
        int32_t l_1233[10][1][5] = {{{0x5BA7286CL,5L,0x5BA7286CL,0x5BA7286CL,5L}},{{0xD108B738L,0x4891C89CL,0x4891C89CL,0xD108B738L,1L}},{{0x5BA7286CL,0x5BA7286CL,5L,0x5BA7286CL,0x5BA7286CL}},{{1L,0x4891C89CL,1L,1L,0x4891C89CL}},{{0x5BA7286CL,(-2L),(-2L),0x5BA7286CL,(-2L)}},{{0x4891C89CL,0x4891C89CL,0xD108B738L,0x4891C89CL,0x4891C89CL}},{{(-2L),0x5BA7286CL,(-2L),(-2L),0x5BA7286CL}},{{0x4891C89CL,1L,1L,0x4891C89CL,1L}},{{0x5BA7286CL,0x5BA7286CL,5L,0x5BA7286CL,0x5BA7286CL}},{{1L,0x4891C89CL,1L,1L,0x4891C89CL}}};
        uint32_t l_1243 = 0x788F52C1L;
        int64_t **l_1253 = &g_494;
        int16_t l_1382 = 5L;
        uint64_t l_1403 = 7UL;
        int i, j, k;
        (*l_91) = p_86;
        for (g_47 = 0; (g_47 >= 6); g_47++)
        { /* block id: 14 */
            uint32_t l_107 = 0UL;
            int32_t l_1230[8] = {0L,0L,0L,0L,0L,0L,0L,0L};
            int32_t l_1272[6][8][1] = {{{0x8C76FD86L},{0L},{0x8C814670L},{0L},{0x8C76FD86L},{0x1BDAF747L},{0x8C76FD86L},{0L}},{{0x8C814670L},{0L},{0x8C76FD86L},{0x1BDAF747L},{0x8C76FD86L},{0L},{0x8C814670L},{0L}},{{0x8C76FD86L},{0x1BDAF747L},{0x8C76FD86L},{0L},{0x8C814670L},{0L},{0x8C76FD86L},{0x1BDAF747L}},{{0x8C76FD86L},{0L},{0x8C814670L},{0L},{0x8C76FD86L},{0x1BDAF747L},{0x8C76FD86L},{0L}},{{0x8C814670L},{0L},{0x8C76FD86L},{0x1BDAF747L},{0x8C76FD86L},{0L},{0x8C814670L},{0L}},{{0x8C76FD86L},{0x1BDAF747L},{0x8C76FD86L},{0L},{0x8C814670L},{0L},{0x8C76FD86L},{0x1BDAF747L}}};
            int32_t l_1273[8] = {7L,0x00FE3871L,7L,0x00FE3871L,7L,0x00FE3871L,7L,0x00FE3871L};
            int64_t ****l_1292 = (void*)0;
            int16_t l_1383 = 0L;
            float * const *l_1417[3][7][2] = {{{&g_396[1],&g_396[1]},{(void*)0,&g_396[1]},{(void*)0,&g_396[1]},{&g_396[1],&g_396[1]},{(void*)0,&g_396[1]},{(void*)0,&g_396[1]},{&g_396[1],&g_396[1]}},{{(void*)0,&g_396[1]},{(void*)0,&g_396[1]},{&g_396[1],&g_396[1]},{(void*)0,&g_396[1]},{(void*)0,&g_396[1]},{&g_396[1],&g_396[1]},{(void*)0,&g_396[1]}},{{(void*)0,&g_396[1]},{&g_396[1],&g_396[1]},{(void*)0,&g_396[1]},{(void*)0,&g_396[1]},{&g_396[1],&g_396[1]},{(void*)0,&g_396[1]},{(void*)0,&g_396[1]}}};
            float * const **l_1416 = &l_1417[1][0][0];
            int32_t l_1442 = 8L;
            int i, j, k;
        }
    }
    else
    { /* block id: 652 */
        uint32_t *****l_1476 = &g_941;
        uint16_t *l_1496 = &g_1384[0][2][2];
        uint32_t *l_1497 = &g_927;
        int32_t l_1504 = 9L;
        for (g_480 = 0; (g_480 <= 50); g_480 = safe_add_func_uint64_t_u_u(g_480, 1))
        { /* block id: 655 */
            uint32_t l_1479 = 4294967291UL;
            int32_t l_1500 = 0x2F7868DEL;
            for (g_67 = 0; (g_67 >= 53); g_67 = safe_add_func_int8_t_s_s(g_67, 8))
            { /* block id: 658 */
                uint16_t *l_1451 = &g_275;
                uint32_t *l_1458 = &g_438;
                uint32_t * const *l_1461 = &g_138;
                uint32_t * const **l_1460 = &l_1461;
                uint32_t * const ***l_1459[9] = {&l_1460,&l_1460,&l_1460,&l_1460,&l_1460,&l_1460,&l_1460,&l_1460,&l_1460};
                int16_t *l_1463 = (void*)0;
                int16_t *l_1464 = &g_475;
                uint32_t l_1477 = 4294967289UL;
                int32_t l_1478 = 0xDA90F6D1L;
                int16_t *l_1498 = (void*)0;
                int16_t *l_1499 = (void*)0;
                int32_t l_1501 = 0x964882ECL;
                int i;
                (**g_395) = (((safe_div_func_int16_t_s_s(((*g_1277) & (p_86 ^ ((*g_1157) = ((((((safe_rshift_func_uint16_t_u_s(((*l_1451)--), (((((safe_mod_func_int8_t_s_s(((((p_86 != (safe_lshift_func_int16_t_s_s(((*l_1464) = ((((*l_1458) = p_86) , l_1459[4]) != l_1462)), 10))) ^ (safe_lshift_func_int16_t_s_u((((safe_lshift_func_int16_t_s_u((!(-1L)), (safe_lshift_func_int8_t_s_s((safe_div_func_uint16_t_u_u((safe_div_func_uint32_t_u_u((l_1476 != &l_1459[4]), p_86)), p_86)), 3)))) , l_1477) == 1L), p_86))) ^ 0xBBL) ^ g_1092[3]), p_86)) , l_1477) < p_86) || (**g_1276)) , g_548))) < 0x0108336BL) > l_1478) != 0x93L) , l_1479) , p_86)))), 3L)) , 0x6.Bp-1) == 0x8.8C4C13p+46);
                (*l_1342) = (p_86 >= (-7L));
                l_1501 &= ((l_1500 = ((((l_1342 == &g_438) != p_86) & (safe_lshift_func_int16_t_s_u(((*l_1464) &= l_1478), 11))) && (((safe_lshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s(g_478, (((~(((((safe_mul_func_uint8_t_u_u((g_181 = 0xD4L), (0x710140E5L > (safe_lshift_func_int8_t_s_s(3L, 4))))) , (((((safe_mod_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((+(((((l_1478 | l_1479) > p_86) <= 0x63B0L) , 18446744073709551608UL) , (*g_319))), p_86)), g_549)) || (*g_1157)) & (-6L)) , l_1478) , l_1496)) == (void*)0) == 247UL) & (*g_319))) , l_1497) == &l_1479))) >= l_1478), 8)) ^ (*g_1157)) < 0x77D2L))) > g_320);
                for (g_181 = (-25); (g_181 < 10); ++g_181)
                { /* block id: 671 */
                    float l_1517 = 0x4.33387Dp+79;
                    uint64_t l_1522 = 0UL;
                    int32_t l_1537 = 0x109BAF4DL;
                    if ((((-3L) || 4UL) , l_1504))
                    { /* block id: 672 */
                        int32_t *l_1505 = &l_1282[3][1][0];
                        int32_t *l_1506 = &l_1303[8];
                        int32_t *l_1507 = &g_253;
                        int32_t *l_1508 = &l_1501;
                        int32_t *l_1509 = &l_1504;
                        int32_t *l_1510 = &l_1282[3][3][0];
                        int32_t *l_1511 = &l_1478;
                        int32_t *l_1512 = &g_253;
                        int32_t *l_1513 = &l_1303[2];
                        int32_t *l_1514 = (void*)0;
                        int32_t *l_1515 = &g_253;
                        int32_t *l_1516[10];
                        int i;
                        for (i = 0; i < 10; i++)
                            l_1516[i] = (void*)0;
                        g_1519--;
                        (**g_395) = 0x2.7p-1;
                        if (l_1522)
                            continue;
                    }
                    else
                    { /* block id: 676 */
                        uint8_t *l_1543[2];
                        int32_t l_1544 = (-1L);
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1543[i] = &g_1092[0];
                        l_1544 = (safe_mod_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((((*g_319) ^ (p_86 , (*g_1277))) && (((~(p_86 = p_86)) == ((safe_rshift_func_int16_t_s_s(((-1L) && (safe_sub_func_uint8_t_u_u((g_343 = (~(safe_add_func_int64_t_s_s((((l_1537 |= (g_1535[1][1][0] == (void*)0)) != ((!(safe_mod_func_int64_t_s_s((safe_div_func_uint8_t_u_u((l_1504 > l_1522), (*l_1342))), l_1500))) ^ l_1504)) <= 0x361AAA86L), (**g_1156))))), 0x80L))), l_1522)) < 0x1B43330CL)) , 1UL)), l_1501)) != 0x9E3BL), l_1504));
                        return (*g_1277);
                    }
                }
            }
        }
        return l_1504;
    }
    g_1548++;
    l_1552--;
    return (**g_1276);
}


/* ------------------------------------------ */
/* 
 * reads : g_17 g_67 g_110 g_65 g_47 g_128 g_66 g_135 g_137 g_253 g_138 g_171 g_63 g_275 g_284 g_181 g_235 g_343 g_354 g_366 g_392 g_393 g_395 g_260 g_404 g_410 g_319 g_320 g_438 g_411 g_396 g_454 g_480 g_492 g_332 g_494 g_422 g_538 g_288 g_488 g_549 g_548 g_941 g_775 g_722 g_493 g_238 g_478 g_926 g_690 g_621 g_1029 g_927 g_1092 g_505 g_590 g_475 g_1145 g_1156 g_1157 g_1213 g_781
 * writes: g_110 g_63 g_171 g_181 g_67 g_260 g_275 g_128 g_284 g_238 g_253 g_137 g_319 g_332 g_343 g_235 g_366 g_392 g_393 g_395 g_404 g_410 g_422 g_438 g_288 g_444 g_454 g_480 g_488 g_492 g_505 g_475 g_538 g_549 g_548 g_941 g_621 g_775 g_590 g_1029 g_927 g_1092 g_478 g_1156 g_469
 */
static uint64_t  func_94(uint32_t * p_95)
{ /* block id: 15 */
    int8_t l_108[4];
    int32_t *l_109 = (void*)0;
    uint64_t l_117 = 0x66077482110C122BLL;
    int64_t *l_118[7][10] = {{&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110},{(void*)0,(void*)0,&g_110,(void*)0,(void*)0,&g_110,(void*)0,(void*)0,&g_110,(void*)0},{(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110,(void*)0},{&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110,&g_110,&g_110,&g_110},{&g_110,&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110},{&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110},{&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110}};
    int32_t l_119 = 2L;
    int32_t l_120[2];
    uint32_t ***l_160 = &g_137[1];
    volatile uint64_t * volatile *l_239 = (void*)0;
    uint32_t l_266 = 1UL;
    int8_t *l_317 = &g_235;
    int64_t l_321 = 2L;
    uint16_t * const l_341[1] = {&g_275};
    int16_t l_394[1][9][4] = {{{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL},{0x19FEL,0x19FEL,0x19FEL,0x19FEL}}};
    int64_t ***l_496 = &g_493[0][5][2];
    uint32_t l_587[1];
    int16_t *l_718 = &g_392;
    const uint8_t l_724 = 0x45L;
    float **l_732 = (void*)0;
    const int32_t *l_783 = &g_253;
    uint8_t l_859 = 255UL;
    int64_t ***l_898 = &g_493[0][5][2];
    uint64_t *l_909 = (void*)0;
    uint64_t * const *l_908[7][7][5] = {{{(void*)0,&l_909,(void*)0,&l_909,(void*)0},{(void*)0,&l_909,&l_909,(void*)0,&l_909},{(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{(void*)0,&l_909,(void*)0,&l_909,(void*)0},{&l_909,(void*)0,&l_909,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,&l_909}},{{&l_909,&l_909,&l_909,&l_909,(void*)0},{(void*)0,(void*)0,&l_909,&l_909,(void*)0},{(void*)0,&l_909,&l_909,&l_909,&l_909},{(void*)0,(void*)0,&l_909,(void*)0,(void*)0},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,(void*)0,&l_909,(void*)0},{&l_909,(void*)0,(void*)0,&l_909,&l_909}},{{&l_909,&l_909,(void*)0,&l_909,&l_909},{(void*)0,&l_909,&l_909,(void*)0,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,(void*)0},{&l_909,(void*)0,&l_909,(void*)0,&l_909},{&l_909,&l_909,&l_909,&l_909,(void*)0}},{{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,(void*)0,(void*)0,&l_909,(void*)0},{&l_909,&l_909,&l_909,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909}},{{(void*)0,&l_909,(void*)0,&l_909,(void*)0},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,(void*)0,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,(void*)0}},{{&l_909,&l_909,(void*)0,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,(void*)0},{&l_909,&l_909,(void*)0,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909}},{{(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,(void*)0},{(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,(void*)0,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,&l_909}}};
    uint64_t * const **l_907[3];
    uint8_t l_930 = 255UL;
    int32_t l_967 = 0x6D2F9516L;
    const uint16_t l_1020[1] = {1UL};
    int32_t *l_1021[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int8_t l_1022 = (-4L);
    float l_1066 = (-0x1.Bp+1);
    uint32_t l_1200 = 0xE5DE983CL;
    uint16_t l_1201 = 0x87E7L;
    int64_t l_1225 = 0xD3D5A898E23519FELL;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_108[i] = 0x06L;
    for (i = 0; i < 2; i++)
        l_120[i] = 0x1534744AL;
    for (i = 0; i < 1; i++)
        l_587[i] = 0x3DC338EBL;
    for (i = 0; i < 3; i++)
        l_907[i] = &l_908[6][4][1];
lbl_510:
    g_110 = (0L | l_108[3]);
    if (((l_109 = p_95) == ((l_120[0] &= (((l_119 &= (((((safe_sub_func_int64_t_s_s(g_17[1], g_67)) >= l_108[1]) == 0x6388D178L) || (0xFAL != (l_108[3] >= ((safe_lshift_func_uint16_t_u_s(g_110, g_65)) >= l_117)))) , l_117)) || l_108[3]) & 7L)) , &l_119)))
    { /* block id: 20 */
        int32_t **l_123 = &l_109;
        uint32_t *l_131 = (void*)0;
        uint32_t **l_130 = &l_131;
        int32_t *l_132 = &l_120[0];
        uint16_t l_236 = 0x6E02L;
        int64_t l_279 = 0xE6F1DCE19F1BC87BLL;
        int32_t l_287[7] = {(-8L),(-8L),(-8L),(-8L),(-8L),(-8L),(-8L)};
        int64_t **l_294[3];
        int8_t *l_429[10][3][8] = {{{&l_108[0],&g_393,&l_108[3],&g_393,(void*)0,&l_108[0],&g_235,&l_108[3]},{(void*)0,(void*)0,(void*)0,&l_108[3],&l_108[3],&l_108[3],(void*)0,(void*)0},{(void*)0,&l_108[2],&g_393,(void*)0,&l_108[3],(void*)0,&l_108[2],&l_108[2]}},{{(void*)0,(void*)0,(void*)0,&l_108[3],(void*)0,(void*)0,&l_108[2],(void*)0},{&l_108[3],&l_108[3],&g_393,(void*)0,&g_393,&l_108[3],(void*)0,&l_108[3]},{&g_393,&l_108[3],(void*)0,&l_108[3],&l_108[3],&g_235,&g_235,&l_108[3]}},{{(void*)0,&l_108[3],&l_108[3],&l_108[3],(void*)0,&g_393,&l_108[0],(void*)0},{&l_108[3],(void*)0,&l_108[3],&l_108[3],&g_393,(void*)0,&l_108[0],&l_108[3]},{&g_393,(void*)0,&l_108[2],(void*)0,(void*)0,&g_393,&g_393,&g_393}},{{&l_108[3],&l_108[3],&l_108[3],(void*)0,(void*)0,(void*)0,&l_108[3],&l_108[3]},{&l_108[3],&l_108[3],&l_108[0],(void*)0,&l_108[0],&l_108[3],&l_108[3],&l_108[3]},{&l_108[3],(void*)0,&g_393,(void*)0,&g_235,&l_108[3],(void*)0,(void*)0}},{{(void*)0,&l_108[0],(void*)0,&l_108[2],&g_235,&g_393,&l_108[0],(void*)0},{&l_108[3],&g_393,&l_108[3],(void*)0,&l_108[0],&g_393,(void*)0,&l_108[3]},{&l_108[3],&l_108[0],&l_108[3],&g_393,(void*)0,(void*)0,&g_393,&l_108[3]}},{{&l_108[3],&l_108[3],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_108[0]},{&g_393,&l_108[3],&g_393,&l_108[2],&g_393,(void*)0,&l_108[3],&l_108[0]},{&l_108[3],&l_108[0],&l_108[3],(void*)0,(void*)0,(void*)0,&g_393,&l_108[3]}},{{&l_108[3],&g_235,(void*)0,&g_393,(void*)0,&l_108[3],&g_393,&l_108[3]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_393,(void*)0},{&l_108[3],&l_108[3],&l_108[3],&l_108[2],&l_108[0],(void*)0,&l_108[3],(void*)0}},{{&l_108[3],&l_108[0],&l_108[3],(void*)0,&g_393,&l_108[2],&g_393,&l_108[3]},{&l_108[0],(void*)0,(void*)0,(void*)0,&l_108[3],(void*)0,&g_393,&l_108[3]},{&g_393,&g_393,(void*)0,(void*)0,(void*)0,(void*)0,&g_393,&g_393}},{{&l_108[3],(void*)0,&l_108[3],(void*)0,&l_108[3],&l_108[3],&l_108[3],&l_108[3]},{&l_108[3],(void*)0,&g_393,&l_108[3],(void*)0,&l_108[3],(void*)0,(void*)0},{&g_393,(void*)0,(void*)0,&l_108[3],&g_235,(void*)0,&g_393,(void*)0}},{{(void*)0,&g_393,&l_108[3],&l_108[2],(void*)0,(void*)0,(void*)0,&l_108[2]},{&l_108[3],(void*)0,&l_108[3],&g_393,(void*)0,&l_108[2],&l_108[0],&l_108[3]},{&l_108[3],&l_108[0],(void*)0,(void*)0,&l_108[3],(void*)0,(void*)0,&l_108[3]}}};
        uint32_t l_591[5] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
        uint32_t ** const *l_618 = &g_137[1];
        uint32_t ** const **l_617 = &l_618;
        int16_t *l_648 = &g_478;
        int16_t *l_651 = &g_392;
        int8_t l_661[7];
        uint8_t l_730 = 0x1AL;
        uint64_t ***l_731 = (void*)0;
        const int32_t *l_782 = (void*)0;
        uint8_t l_858 = 255UL;
        int32_t l_884 = 9L;
        float l_906 = 0x4.Cp-1;
        uint32_t *****l_943 = &g_941;
        int8_t l_949 = 0L;
        int32_t *l_965[2];
        uint8_t *l_966 = &g_480;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_294[i] = &l_118[4][3];
        for (i = 0; i < 7; i++)
            l_661[i] = 1L;
        for (i = 0; i < 2; i++)
            l_965[i] = &l_884;
        if ((1UL > ((*l_132) |= (((safe_sub_func_uint64_t_u_u((p_95 != ((p_95 == ((*l_123) = &g_47)) , ((*l_130) = (((g_47 != ((safe_rshift_func_int8_t_s_u(((safe_rshift_func_int16_t_s_s((l_123 != g_128), 2)) != (-7L)), (*l_109))) > g_67)) > 18446744073709551613UL) , l_109)))), g_66[5][5])) || 65535UL) && (**l_123)))))
        { /* block id: 24 */
            const uint32_t *l_155[10] = {&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67,&g_67};
            const uint32_t **l_154 = &l_155[1];
            const uint32_t ***l_153 = &l_154;
            int32_t l_156 = 0L;
            int32_t l_269 = 0x49847874L;
            uint16_t l_344 = 0UL;
            int i;
            for (g_63 = 0; (g_63 <= 9); g_63 += 1)
            { /* block id: 27 */
                uint32_t ***l_139 = &l_130;
                int32_t l_141 = 0x5C24141EL;
                int8_t *l_142 = &l_108[2];
                int32_t **l_178 = &l_132;
                int32_t l_289 = (-1L);
                uint16_t l_290 = 0x7738L;
                int64_t **l_296[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_296[i] = &l_118[1][5];
                if (((((safe_mul_func_int8_t_s_s((g_135 != ((*l_139) = g_137[1])), ((*l_142) = (~(g_110 , l_141))))) | 0x24B411FAL) , ((safe_rshift_func_int16_t_s_u(((safe_sub_func_uint16_t_u_u((safe_sub_func_int8_t_s_s(((safe_add_func_int8_t_s_s((safe_div_func_uint32_t_u_u(4294967295UL, (**l_123))), ((void*)0 == l_153))) & (*g_138)), (**l_123))), (*l_109))) <= l_156), 6)) != 0xEEC5F198L)) & l_141))
                { /* block id: 30 */
                    for (g_110 = 9; (g_110 >= 0); g_110 -= 1)
                    { /* block id: 33 */
                        int64_t ***l_157 = (void*)0;
                        int64_t **l_159[1][4];
                        int64_t ***l_158 = &l_159[0][1];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 4; j++)
                                l_159[i][j] = &l_118[4][3];
                        }
                        (*l_158) = &l_118[2][0];
                        (*l_132) = l_141;
                        l_160 = &g_137[1];
                        (*l_123) = &l_120[0];
                    }
                }
                else
                { /* block id: 39 */
                    int32_t l_182 = 0L;
                    int32_t l_183 = 0xF6A8FE63L;
                    for (l_156 = 6; (l_156 >= 0); l_156 -= 1)
                    { /* block id: 42 */
                        if (g_67)
                            break;
                        if (l_156)
                            continue;
                    }
                    for (l_156 = 0; (l_156 <= 1); l_156 += 1)
                    { /* block id: 48 */
                        int16_t *l_167 = (void*)0;
                        int16_t *l_168 = (void*)0;
                        int16_t *l_169 = (void*)0;
                        int16_t *l_170 = &g_171;
                        int i;
                        l_183 &= (safe_rshift_func_int8_t_s_s(((l_120[l_156] | 1L) && (safe_sub_func_uint16_t_u_u((!(((~((*l_170) |= l_120[l_156])) != ((safe_mod_func_uint32_t_u_u(((safe_sub_func_uint8_t_u_u((((g_181 = (safe_mul_func_int8_t_s_s(((((*l_109) | (((void*)0 == l_142) , ((void*)0 == l_178))) , ((safe_rshift_func_uint16_t_u_s((g_63 & 0x70BDL), g_17[4])) ^ g_67)) ^ (**l_123)), (**l_178)))) | (-3L)) , (**l_123)), g_65)) | g_110), 0x0186BA22L)) != 0x1FB71B8916C188A1LL)) < l_182)), (**l_178)))), l_120[l_156]));
                    }
                }
                for (l_156 = 0; (l_156 <= 6); l_156 += 1)
                { /* block id: 56 */
                    float l_184 = 0x1.2p-1;
                    int32_t l_195 = 3L;
                    uint8_t *l_210 = &g_181;
                    uint32_t *l_241[2];
                    int32_t l_251[7] = {0x58E6F13FL,0xE7776E9EL,0xE7776E9EL,0x58E6F13FL,0xE7776E9EL,0xE7776E9EL,0x58E6F13FL};
                    int i;
                    for (i = 0; i < 2; i++)
                        l_241[i] = &g_67;
                }
                for (g_67 = 2; (g_67 <= 9); g_67 += 1)
                { /* block id: 89 */
                    int32_t *l_280 = &g_253;
                    int32_t *l_281 = &l_269;
                    int32_t *l_282 = (void*)0;
                    int32_t *l_283[6] = {&l_141,&l_120[0],&l_141,&l_141,&l_120[0],&l_141};
                    int i;
                    for (g_260 = 2; (g_260 <= 6); g_260 += 1)
                    { /* block id: 92 */
                        const int32_t *l_271 = (void*)0;
                        const int32_t **l_270 = &l_271;
                        int32_t *l_272 = (void*)0;
                        int32_t *l_273 = &l_269;
                        int32_t *l_274[1];
                        int32_t * volatile **l_278 = &g_128;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_274[i] = (void*)0;
                        (*l_270) = l_155[g_67];
                        g_275--;
                        (*l_278) = g_128;
                    }
                    ++g_284[3][2];
                    if (l_269)
                        continue;
                    if ((0x804DBB215A1ACACCLL && (*l_109)))
                    { /* block id: 99 */
                        return g_66[5][9];
                    }
                    else
                    { /* block id: 101 */
                        if (l_156)
                            break;
                        --l_290;
                    }
                    for (l_279 = 0; (l_279 <= 9); l_279 += 1)
                    { /* block id: 107 */
                        volatile uint64_t * volatile **l_293 = &g_238;
                        int64_t ***l_295[4] = {&l_294[0],&l_294[0],&l_294[0],&l_294[0]};
                        int i;
                        (*l_293) = l_239;
                        l_296[0] = l_294[2];
                    }
                }
                for (g_181 = 2; (g_181 <= 6); g_181 += 1)
                { /* block id: 114 */
                    uint16_t l_299 = 0xC6A0L;
                    (*l_123) = (*l_178);
                    for (l_141 = 0; (l_141 <= 6); l_141 += 1)
                    { /* block id: 118 */
                        if (g_275)
                            break;
                    }
                    g_253 ^= ((**l_123) &= (safe_lshift_func_int8_t_s_s(((*g_138) && (l_299 != (~(safe_mod_func_uint8_t_u_u(g_67, g_275))))), 6)));
                    for (l_269 = 6; (l_269 >= 0); l_269 -= 1)
                    { /* block id: 125 */
                        uint32_t l_303 = 0xC7B452A9L;
                        int32_t ***l_310 = &l_123;
                        int32_t *l_311 = &l_287[6];
                        (*l_109) |= (g_181 <= l_303);
                        (*l_123) = &g_253;
                        (*l_311) ^= (~(0x6E328507D9A1DD2ALL < ((((safe_mul_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u((l_269 == (((*l_132) || (~l_303)) || (l_310 == (void*)0))), g_235)) >= (**l_178)), ((*l_132) , 0x95L))) | 0x25L) >= (*l_109)) != l_269)));
                        (*l_160) = &g_138;
                    }
                }
            }
            for (g_181 = 0; (g_181 < 15); g_181++)
            { /* block id: 135 */
                uint64_t l_314[1];
                int8_t *l_316 = &l_108[3];
                int8_t **l_315[3];
                const int8_t *l_318[7][9] = {{&g_235,&l_108[3],&g_235,&l_108[3],&g_235,&l_108[3],&l_108[2],&l_108[3],&l_108[2]},{&l_108[1],&l_108[3],&l_108[3],&l_108[3],&l_108[3],&l_108[1],&l_108[3],&l_108[3],&l_108[3]},{&g_235,&l_108[3],&l_108[2],&l_108[3],&l_108[2],&l_108[3],&g_235,&l_108[3],&g_235},{(void*)0,&l_108[3],(void*)0,&l_108[1],&l_108[1],(void*)0,&l_108[3],(void*)0,&l_108[1]},{&l_108[3],&l_108[3],&l_108[2],&l_108[3],&g_235,&l_108[3],&l_108[2],&l_108[3],&l_108[3]},{&l_108[3],&l_108[1],&l_108[3],&l_108[1],&l_108[3],&l_108[3],&l_108[1],&l_108[3],&l_108[1]},{&l_108[2],&l_108[3],&g_235,&l_108[3],&g_235,&l_108[3],&g_235,&l_108[3],&g_235}};
                int32_t l_329 = 0xDD1925C7L;
                uint16_t *l_365 = &g_275;
                int i, j;
                for (i = 0; i < 1; i++)
                    l_314[i] = 1UL;
                for (i = 0; i < 3; i++)
                    l_315[i] = &l_316;
                if (((*l_132) = (l_314[0] >= ((l_317 = &g_235) != (g_319 = l_318[2][6])))))
                { /* block id: 139 */
                    uint16_t l_322 = 0xBD90L;
                    uint16_t *l_327[1];
                    int32_t l_328[7] = {1L,1L,1L,1L,1L,1L,1L};
                    uint8_t *l_342[9];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_327[i] = &l_236;
                    for (i = 0; i < 9; i++)
                        l_342[i] = &g_343;
                    (*l_132) &= (l_321 = l_269);
                    for (l_321 = 0; (l_321 <= 2); l_321 += 1)
                    { /* block id: 144 */
                        (*l_132) &= l_156;
                    }
                    if (l_322)
                        break;
                    l_344 &= (safe_add_func_int8_t_s_s(((*l_109) <= 0xB4D2L), (g_343 = (safe_add_func_uint16_t_u_u(g_110, (((((g_275++) < (g_332 = 0xC7B6L)) || ((safe_mod_func_int8_t_s_s((*l_109), 1UL)) > ((g_67 || (((safe_lshift_func_uint16_t_u_s(((safe_mod_func_int8_t_s_s((((void*)0 != l_341[0]) ^ 0x0DL), g_66[5][9])) , l_269), g_63)) ^ 0L) >= l_329)) != l_328[1]))) != l_314[0]) <= g_66[2][3]))))));
                }
                else
                { /* block id: 152 */
                    uint16_t l_345 = 1UL;
                    const int64_t *l_353 = &l_321;
                    uint8_t *l_369 = (void*)0;
                    uint64_t l_371 = 18446744073709551615UL;
                    l_345--;
                    if (((safe_lshift_func_uint8_t_u_u((l_345 && (safe_lshift_func_uint16_t_u_u(((g_110 ^= l_156) >= g_343), ((!0x1E1686B45E094BE1LL) && (l_329 = ((l_353 != g_354) <= ((void*)0 != &g_320))))))), ((**l_123) , 0x86L))) , g_181))
                    { /* block id: 156 */
                        int8_t l_358 = 0x05L;
                        int16_t *l_359[2];
                        int32_t l_360[6];
                        uint8_t *l_368 = &g_343;
                        uint8_t **l_367[9];
                        int32_t *l_370 = &l_269;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_359[i] = &g_171;
                        for (i = 0; i < 6; i++)
                            l_360[i] = 0x128A37D8L;
                        for (i = 0; i < 9; i++)
                            l_367[i] = &l_368;
                        (*l_370) ^= ((l_369 = ((((safe_rshift_func_uint8_t_u_s((g_17[1] && 0x05L), (g_366 ^= (((&l_344 != (((l_360[5] = ((g_275 = ((0x4366L >= (l_358 && (**l_123))) & l_345)) && (g_171 = l_314[0]))) | ((l_119 = (safe_div_func_int16_t_s_s(((((*l_317) = (safe_mul_func_uint8_t_u_u(g_47, l_344))) ^ 0x73L) , l_329), g_65))) && l_156)) , l_365)) < g_17[0]) <= 252UL)))) && 1L) <= g_253) , (void*)0)) != l_316);
                        (*l_370) &= ((void*)0 != l_341[0]);
                        return l_371;
                    }
                    else
                    { /* block id: 167 */
                        uint64_t *l_374 = &l_314[0];
                        int16_t *l_377 = &g_171;
                        const int32_t l_382[7] = {1L,1L,1L,1L,1L,1L,1L};
                        int16_t *l_391 = &g_392;
                        int i;
                        g_393 |= (((((*g_138) |= ((safe_mod_func_uint16_t_u_u(((*l_365) = (((*l_374)--) >= (((*l_377) |= (&g_181 == (void*)0)) || 0xA1EEL))), (l_269 ^= ((safe_rshift_func_uint16_t_u_u((((safe_div_func_uint64_t_u_u((l_382[1] == 0x96L), l_371)) | g_63) ^ ((((safe_rshift_func_int8_t_s_s((g_366 | (safe_div_func_int16_t_s_s(((*l_391) ^= ((safe_mul_func_int8_t_s_s((g_235 |= (safe_div_func_uint8_t_u_u(0x8BL, l_345))), 0L)) , (*l_109))), 6L))), l_156)) , (*l_109)) >= 0x8C187EB0L) == 0x46F0L)), 4)) , (*l_109))))) == (**l_123))) > 0x0D4928ACL) , l_382[1]) , 0L);
                    }
                    if (((*l_132) = l_394[0][6][0]))
                    { /* block id: 178 */
                        float ***l_397 = &g_395;
                        float **l_399 = &g_396[1];
                        float ***l_398 = &l_399;
                        (*l_398) = ((*l_397) = g_395);
                    }
                    else
                    { /* block id: 181 */
                        if (g_65)
                            break;
                    }
                }
                for (g_260 = 0; (g_260 <= 0); g_260 += 1)
                { /* block id: 187 */
                    int32_t l_400 = 3L;
                    int32_t *l_401 = &l_269;
                    int32_t *l_402 = (void*)0;
                    int32_t *l_403[7][4][9] = {{{&g_253,&g_65,&l_119,&l_287[3],&g_253,&g_65,(void*)0,(void*)0,&l_156},{&g_253,&l_119,&l_119,&g_65,&l_119,(void*)0,(void*)0,(void*)0,&l_400},{&l_120[0],&l_329,&l_287[0],(void*)0,&g_65,(void*)0,&g_65,(void*)0,&l_287[0]},{&g_47,&g_47,&l_120[1],&l_287[4],(void*)0,&l_287[4],&l_329,(void*)0,&l_156}},{{(void*)0,(void*)0,(void*)0,(void*)0,&l_287[0],&l_119,&l_120[1],&l_120[0],&l_329},{&l_400,&l_287[6],&l_120[1],&l_120[0],&g_47,&l_119,(void*)0,&g_65,&l_329},{&l_120[0],&l_269,&l_287[0],&l_400,&l_287[4],&l_400,&g_253,&l_120[0],&l_287[6]},{(void*)0,&l_287[4],&l_119,&l_400,&l_287[4],&l_287[6],&l_120[0],&l_400,&g_253}},{{&g_65,&g_253,&l_119,(void*)0,&l_287[4],&l_120[1],&l_120[0],&l_119,&l_120[0]},{&g_253,&l_120[0],&l_287[6],&g_65,&l_119,&l_119,&l_120[0],&g_47,&l_120[0]},{&l_120[1],&g_65,&g_253,&g_253,&g_65,&l_120[1],(void*)0,&l_120[0],&l_156},{(void*)0,&l_120[0],&l_287[4],&l_120[0],&l_119,&l_287[6],&l_287[2],(void*)0,&l_119}},{{&g_47,(void*)0,&l_119,&g_65,&l_120[0],(void*)0,(void*)0,&l_120[0],(void*)0},{(void*)0,(void*)0,&l_400,&l_156,&l_119,&g_253,&l_120[0],&l_120[0],&l_120[0]},{&l_400,(void*)0,(void*)0,&l_269,&l_287[6],(void*)0,&l_120[0],&l_119,&l_287[4]},{&l_287[6],(void*)0,&l_400,&l_156,&l_120[0],(void*)0,&l_156,&l_119,&l_287[4]}},{{&l_120[0],&l_329,&g_65,&l_120[1],&l_119,&l_287[6],(void*)0,&l_119,&l_120[0]},{&l_156,(void*)0,&l_156,&l_120[0],&l_287[6],&l_287[6],&l_120[0],&l_156,(void*)0},{&l_119,&l_119,&g_65,&l_287[6],&l_156,&g_65,&g_65,(void*)0,&l_119},{&l_120[0],&l_120[0],&l_156,&g_253,&g_65,(void*)0,&l_120[0],&l_120[1],&l_156}},{{(void*)0,&l_119,(void*)0,(void*)0,&l_400,&l_156,&l_119,&g_253,&l_120[0]},{&l_120[0],(void*)0,&g_253,&l_119,&l_120[0],&g_253,&l_400,&l_400,&l_156},{&g_253,&l_329,&l_120[1],&l_400,&g_253,&l_120[0],&g_65,&l_156,&l_400},{&l_156,(void*)0,&g_253,&l_400,&g_47,&g_65,&l_400,(void*)0,&l_400}},{{&l_287[4],(void*)0,&g_65,&l_119,(void*)0,&l_120[0],&g_253,&g_253,&l_120[0]},{&l_120[0],(void*)0,&l_287[3],(void*)0,&l_120[0],&l_156,&l_156,(void*)0,&l_287[6]},{&l_329,(void*)0,&l_287[6],&g_253,&l_287[0],&g_253,(void*)0,&l_287[6],&l_287[4]},{&g_65,&l_120[0],&l_329,&l_287[6],&l_156,&l_156,(void*)0,&l_269,&l_119}}};
                    int i, j, k;
                    (*l_132) |= g_66[(g_260 + 8)][(g_260 + 6)];
                    for (g_63 = 0; (g_63 >= 0); g_63 -= 1)
                    { /* block id: 191 */
                        int i, j;
                        (*l_123) = &g_65;
                        return g_284[g_63][(g_260 + 6)];
                    }
                    --g_404;
                }
            }
            g_253 = l_344;
        }
        else
        { /* block id: 199 */
            uint32_t l_409[1][8][9] = {{{18446744073709551615UL,0x611923D3L,0xBFC873EBL,0UL,0x9DC2FB92L,18446744073709551607UL,18446744073709551607UL,0x9DC2FB92L,0UL},{18446744073709551615UL,0x9DC2FB92L,18446744073709551615UL,0UL,0UL,0xBB1916C4L,18446744073709551607UL,0UL,0x163A1735L},{18446744073709551615UL,0UL,1UL,0UL,0x38A3F5D5L,7UL,0x2E7DB46CL,0x38A3F5D5L,0x8790D6BEL},{0UL,0x38A3F5D5L,0x9AF5CD88L,0UL,0xBB1916C4L,0x2E7DB46CL,0x2E7DB46CL,0xBB1916C4L,0UL},{0UL,0xBB1916C4L,0UL,0UL,18446744073709551607UL,0xBCCEDB56L,0x2E7DB46CL,18446744073709551607UL,18446744073709551610UL},{0UL,18446744073709551607UL,0xCA15E359L,0UL,0x38A3F5D5L,7UL,0x2E7DB46CL,0x38A3F5D5L,0x8790D6BEL},{0UL,0x38A3F5D5L,0x9AF5CD88L,0UL,0xBB1916C4L,0x2E7DB46CL,0x2E7DB46CL,0xBB1916C4L,0UL},{0UL,0xBB1916C4L,0UL,0UL,18446744073709551607UL,0xBCCEDB56L,0x2E7DB46CL,18446744073709551607UL,18446744073709551610UL}}};
            int32_t l_442 = 1L;
            uint16_t l_461[6] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
            uint32_t ** const **l_462 = (void*)0;
            int32_t l_470 = (-9L);
            int32_t l_471 = 1L;
            int32_t l_474 = 0xE340233AL;
            int32_t l_476 = 0x4FC9B9BDL;
            uint32_t **l_506 = &l_131;
            int32_t l_539 = 0xF7CF9BE0L;
            int i, j, k;
            (*l_123) = &l_120[1];
            for (g_181 = (-6); (g_181 < 26); g_181 = safe_add_func_int64_t_s_s(g_181, 5))
            { /* block id: 203 */
                int16_t l_425[7] = {8L,0xF6A4L,8L,8L,0xF6A4L,8L,8L};
                int i;
                if ((*l_109))
                    break;
                if (l_409[0][3][8])
                { /* block id: 205 */
                    uint64_t l_413 = 0x83ACA40FAEA75927LL;
                    int32_t l_414 = 0x4316215EL;
                    int8_t **l_421[5];
                    uint8_t *l_426 = (void*)0;
                    uint8_t *l_427[2];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_421[i] = &l_317;
                    for (i = 0; i < 2; i++)
                        l_427[i] = &g_343;
                    (*l_123) = &g_253;
                    g_410 = g_410;
                    l_414 |= l_413;
                    (*l_132) ^= ((safe_mul_func_uint16_t_u_u((safe_add_func_int32_t_s_s(((*l_109) = ((safe_lshift_func_uint8_t_u_u((((g_422 = &g_235) == ((((g_343 = ((safe_lshift_func_uint16_t_u_s(((l_425[0] | 0xF78AE2C3L) != l_425[0]), 4)) <= l_413)) | (*g_319)) , (safe_unary_minus_func_uint64_t_u(0UL))) , l_429[1][1][1])) && (-6L)), 5)) != l_414)), 4294967288UL)), g_393)) , 1L);
                }
                else
                { /* block id: 213 */
                    return g_253;
                }
            }
            for (g_404 = 0; (g_404 > 57); g_404 = safe_add_func_uint32_t_u_u(g_404, 3))
            { /* block id: 219 */
                int32_t l_441 = 0x9589938AL;
                uint32_t ****l_463[3];
                int32_t l_464 = (-1L);
                int32_t l_465 = 0x96A9CD39L;
                int32_t l_472 = 0L;
                int32_t l_473 = 0xE93E1E05L;
                int32_t l_477 = 0L;
                int16_t l_479 = 1L;
                int64_t ****l_495[10][9][2] = {{{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,(void*)0},{(void*)0,&g_492},{&g_492,&g_492}},{{&g_492,(void*)0},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492}},{{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492}},{{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492}},{{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492}},{{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492}},{{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492}},{{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492}},{{&g_492,&g_492},{&g_492,&g_492},{(void*)0,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492}},{{(void*)0,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,(void*)0}}};
                int8_t *l_515 = (void*)0;
                int8_t *l_516[3];
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_463[i] = &l_160;
                for (i = 0; i < 3; i++)
                    l_516[i] = &l_108[3];
                for (l_321 = 6; (l_321 >= 0); l_321 -= 1)
                { /* block id: 222 */
                    int32_t l_433 = 0xD1984112L;
                    int32_t l_437[7][9][3] = {{{(-3L),0x8E1F630AL,0x61C2B218L},{0x2E9158BBL,0x2E9158BBL,0x3051B704L},{0x18FB7409L,0x2E9158BBL,0x0B45DA27L},{0x7B18CDE9L,0x8E1F630AL,0x2E9158BBL},{0x3B4D7E92L,1L,(-8L)},{0x61C2B218L,0x7B18CDE9L,0x2E9158BBL},{(-6L),0x0B45DA27L,0x0B45DA27L},{0x7EC23776L,1L,0x3051B704L},{0x7EC23776L,0x3051B704L,0x61C2B218L}},{{(-6L),0x18FB7409L,7L},{0x61C2B218L,0x3B4D7E92L,9L},{0x3B4D7E92L,0x18FB7409L,(-3L)},{0x7B18CDE9L,0x3051B704L,0x7B18CDE9L},{0x18FB7409L,1L,0x7B18CDE9L},{0x2E9158BBL,0x0B45DA27L,(-3L)},{(-3L),0x7B18CDE9L,9L},{1L,1L,7L},{(-3L),0x8E1F630AL,0x61C2B218L}},{{0x2E9158BBL,0x2E9158BBL,0x3051B704L},{0x18FB7409L,0x2E9158BBL,0x0B45DA27L},{0x7B18CDE9L,0x8E1F630AL,0x2E9158BBL},{0x3B4D7E92L,1L,(-8L)},{0x61C2B218L,0x7B18CDE9L,0x2E9158BBL},{(-6L),0x0B45DA27L,0x0B45DA27L},{0x7EC23776L,1L,0x3051B704L},{0x7EC23776L,0x3051B704L,0x61C2B218L},{(-6L),0x18FB7409L,7L}},{{0x61C2B218L,0x3B4D7E92L,9L},{0x3B4D7E92L,0x18FB7409L,(-3L)},{0x7B18CDE9L,0x3051B704L,0x7B18CDE9L},{0x18FB7409L,1L,0x7B18CDE9L},{0x2E9158BBL,0x0B45DA27L,(-3L)},{(-3L),0x7B18CDE9L,9L},{1L,1L,7L},{(-3L),0x8E1F630AL,0x61C2B218L},{0x2E9158BBL,0x2E9158BBL,0x3051B704L}},{{0x18FB7409L,0x2E9158BBL,0x0B45DA27L},{0x7B18CDE9L,0x8E1F630AL,0x2E9158BBL},{0x3B4D7E92L,1L,(-8L)},{0x61C2B218L,0x7B18CDE9L,0x2E9158BBL},{(-6L),0x0B45DA27L,0x0B45DA27L},{0x7EC23776L,1L,0x3051B704L},{0x7EC23776L,0x3051B704L,0x61C2B218L},{(-6L),0x18FB7409L,7L},{0x61C2B218L,0x3B4D7E92L,9L}},{{0x3B4D7E92L,0x18FB7409L,(-3L)},{0x56A4E0A7L,1L,0x56A4E0A7L},{0x0B45DA27L,(-6L),0x56A4E0A7L},{0x61C2B218L,9L,0x7B18CDE9L},{0x7B18CDE9L,0x56A4E0A7L,(-8L)},{(-6L),0x3B4D7E92L,0x18FB7409L},{0x7B18CDE9L,0x2E9158BBL,1L},{0x61C2B218L,0x61C2B218L,1L},{0x0B45DA27L,0x61C2B218L,9L}},{{0x56A4E0A7L,0x2E9158BBL,0x61C2B218L},{0x8E1F630AL,0x3B4D7E92L,(-3L)},{1L,0x56A4E0A7L,0x61C2B218L},{0x7EC23776L,9L,9L},{7L,(-6L),1L},{7L,1L,1L},{0x7EC23776L,0x0B45DA27L,0x18FB7409L},{1L,0x8E1F630AL,(-8L)},{0x8E1F630AL,0x0B45DA27L,0x7B18CDE9L}}};
                    int32_t l_449 = (-10L);
                    int32_t *l_489 = &l_433;
                    int i, j, k;
                    if (g_284[(l_321 + 1)][l_321])
                    { /* block id: 223 */
                        int32_t *l_432 = &g_253;
                        int32_t *l_434 = &l_120[0];
                        int32_t *l_435 = (void*)0;
                        int32_t *l_436[1];
                        float *l_443 = &g_444;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_436[i] = &g_253;
                        --g_438;
                        l_119 &= (*l_109);
                        if (l_441)
                            break;
                        (*l_443) = ((-0x1.7p-1) <= (l_442 = (((*g_410) == (void*)0) , ((**g_395) = (*l_132)))));
                    }
                    else
                    { /* block id: 230 */
                        uint64_t *l_452 = &l_117;
                        uint64_t **l_451 = &l_452;
                        uint64_t ***l_450 = &l_451;
                        uint64_t *l_453 = &g_454[2][0][1];
                        int32_t *l_466 = (void*)0;
                        int32_t *l_467 = &l_437[6][8][2];
                        int32_t *l_468[5][7][7] = {{{(void*)0,&l_464,&l_437[3][5][2],&l_119,&l_287[6],(void*)0,(void*)0},{&l_464,(void*)0,(void*)0,(void*)0,&g_65,(void*)0,(void*)0},{&l_287[6],&l_287[6],&l_437[6][8][2],&l_433,(void*)0,(void*)0,&l_287[5]},{&l_120[0],&l_287[6],(void*)0,&l_119,&l_464,&l_120[0],(void*)0},{(void*)0,&g_253,(void*)0,&l_287[5],(void*)0,&g_253,(void*)0},{&l_119,(void*)0,&l_433,&l_437[2][8][0],&g_65,&l_287[6],&l_119},{(void*)0,&l_437[3][5][2],(void*)0,(void*)0,&l_287[6],(void*)0,(void*)0}},{{&g_47,&l_120[0],&l_433,&l_120[0],&g_47,(void*)0,&l_442},{&l_437[6][8][2],&l_433,(void*)0,(void*)0,&l_287[5],&l_120[0],&l_287[6]},{&l_287[6],(void*)0,(void*)0,&l_437[6][8][2],&l_433,&l_442,&g_253},{&l_437[6][8][2],(void*)0,&l_437[6][8][2],(void*)0,&l_464,&l_437[3][5][2],&l_119},{&g_47,(void*)0,(void*)0,(void*)0,&g_253,&l_287[6],&l_119},{(void*)0,&l_437[6][8][2],&l_437[3][5][2],&g_253,&g_253,&l_437[3][5][2],&l_437[6][8][2]},{&l_119,&l_119,&l_119,&l_287[0],&l_287[6],&l_442,&l_464}},{{(void*)0,(void*)0,&l_287[6],(void*)0,&l_119,&l_120[0],(void*)0},{&l_120[0],&l_287[6],&l_433,&l_287[0],(void*)0,(void*)0,&l_120[0]},{&l_287[6],(void*)0,(void*)0,&g_253,(void*)0,(void*)0,&l_464},{&l_464,&l_437[6][8][2],&g_65,(void*)0,(void*)0,&l_287[6],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_253,&l_120[0]},{&l_433,&l_287[6],&l_120[1],&l_437[6][8][2],&l_119,&l_120[0],&l_433},{(void*)0,&l_433,(void*)0,&l_437[3][5][2],(void*)0,&l_437[3][5][2],(void*)0}},{{&l_464,(void*)0,&l_119,&l_119,&g_47,(void*)0,&l_433},{&l_287[5],&l_287[6],(void*)0,&l_287[6],(void*)0,(void*)0,&l_119},{&l_120[1],&l_120[0],&g_47,&l_287[0],&g_47,&l_120[0],&l_120[1]},{&g_253,&l_437[3][5][2],&l_437[6][8][2],(void*)0,(void*)0,&l_119,&l_287[6]},{&l_433,(void*)0,&l_287[6],&l_119,&l_287[6],(void*)0,&l_442},{&l_433,(void*)0,&l_437[6][8][2],(void*)0,&l_433,&l_287[5],&l_287[6]},{&l_119,&l_119,&g_47,(void*)0,&l_433,(void*)0,&l_287[6]}},{{(void*)0,&l_437[6][8][2],(void*)0,&l_120[0],&l_437[6][8][2],&l_437[6][8][2],&l_120[0]},{&l_119,(void*)0,&l_119,&l_287[6],&g_253,(void*)0,(void*)0},{&l_433,(void*)0,(void*)0,&l_287[5],&l_120[0],&l_287[6],&g_253},{&l_433,&l_120[0],&l_120[0],&l_120[0],&g_253,(void*)0,&l_119},{&g_253,(void*)0,&l_287[6],(void*)0,(void*)0,&l_437[6][8][2],(void*)0},{&l_120[1],&l_120[0],&l_464,(void*)0,(void*)0,(void*)0,&g_65},{&l_287[5],&l_119,(void*)0,(void*)0,&l_119,&l_287[5],&l_437[3][5][2]}}};
                        int i, j, k;
                        if (l_441)
                            break;
                        (*l_132) = (safe_rshift_func_uint8_t_u_u((l_441 , 251UL), ((0UL > ((((((((*l_453) &= ((**l_451) = (safe_add_func_int64_t_s_s((l_449 >= 6UL), (l_442 = ((l_450 != (void*)0) || 0xD1E3544E29CE6E52LL)))))) != (safe_sub_func_int64_t_s_s((g_110 = (safe_mod_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(1UL, l_461[0])), 0x66508C638B5E7460LL))), (-6L)))) >= g_66[5][9]) != (*l_109)) | 18446744073709551615UL) , l_462) != l_463[1])) & l_433)));
                        l_464 = g_392;
                        ++g_480;
                    }
                    l_109 = &g_47;
                    for (l_470 = 1; (l_470 >= 0); l_470 -= 1)
                    { /* block id: 243 */
                        uint64_t *l_484 = &l_117;
                        uint16_t l_486 = 65532UL;
                        int32_t **l_487[9] = {&l_132,&l_132,&l_132,&l_132,&l_132,&l_132,&l_132,&l_132,&l_132};
                        int i;
                        l_489 = (g_488 = ((*l_123) = (((+(((*l_484) = l_433) > (+(l_486 && g_454[2][0][1])))) ^ (p_95 == p_95)) , &g_65)));
                    }
                }
                if (((l_119 |= ((*l_109) && (*l_109))) < (safe_rshift_func_uint8_t_u_s(((l_496 = (g_492 = g_492)) != (void*)0), 0))))
                { /* block id: 253 */
                    int32_t l_504 = 1L;
                    for (g_67 = 1; (g_67 <= 6); g_67 += 1)
                    { /* block id: 256 */
                        int16_t *l_509 = &l_394[0][5][3];
                        int i;
                        l_287[g_67] = ((((~((*l_509) ^= (((safe_div_func_int64_t_s_s((!(((safe_unary_minus_func_int16_t_s(0xBAABL)) && (*l_109)) <= (safe_mod_func_uint16_t_u_u((g_505 = l_504), (((&p_95 == (l_506 = ((*l_160) = &g_138))) >= (safe_rshift_func_int16_t_s_u(g_332, g_66[5][9]))) , g_393))))), ((*g_494) = (g_67 , l_504)))) , 4294967294UL) ^ (*g_138)))) | 0x8EL) , l_476) , l_472);
                    }
                    (*l_132) |= l_465;
                    if (g_343)
                        goto lbl_510;
                }
                else
                { /* block id: 266 */
                    uint16_t l_533 = 0UL;
                    int16_t *l_536 = &l_394[0][6][0];
                    int16_t *l_537 = &g_475;
                    int32_t *l_540 = &l_474;
                    if (((safe_add_func_int32_t_s_s(((*g_422) > (*l_109)), (((safe_add_func_uint8_t_u_u((((l_429[0][0][4] = l_515) != l_516[1]) <= ((safe_div_func_uint32_t_u_u((*g_138), (safe_sub_func_uint8_t_u_u(((safe_add_func_int32_t_s_s((l_442 , (safe_div_func_int8_t_s_s((*g_422), (safe_rshift_func_int8_t_s_s((g_538 &= (safe_mod_func_uint64_t_u_u((((safe_mod_func_uint64_t_u_u(((safe_mul_func_int8_t_s_s(l_533, (safe_sub_func_int16_t_s_s(((*l_537) = ((*l_536) ^= 0xE795L)), 8L)))) <= (*l_132)), 18446744073709551615UL)) , 1UL) < 0xF2A685DBE9D8C445LL), l_472))), (*g_422)))))), (*l_132))) & 0x4CC4L), 0x21L)))) , (-4L))), (*g_422))) > l_533) & g_47))) , l_539))
                    { /* block id: 271 */
                        uint32_t l_543 = 0UL;
                        l_540 = (((*g_494) = (*l_132)) , p_95);
                        l_464 = ((**g_395) = (**g_395));
                        l_464 = (safe_add_func_float_f_f(l_543, ((**g_395) = l_409[0][3][8])));
                        if ((*g_488))
                            continue;
                    }
                    else
                    { /* block id: 279 */
                        int32_t *l_544 = (void*)0;
                        int32_t *l_545 = &l_474;
                        int32_t *l_546 = &l_442;
                        int32_t *l_547[10] = {&g_253,&g_253,&g_253,&g_253,&g_253,&g_253,&g_253,&g_253,&g_253,&g_253};
                        int i;
                        if (g_392)
                            goto lbl_510;
                        g_549++;
                        (*l_123) = &g_253;
                        if ((*g_488))
                            break;
                    }
                }
                (*l_123) = &g_65;
            }
        }
        for (g_366 = 0; (g_366 < 28); g_366++)
        { /* block id: 291 */
            int8_t l_561 = 1L;
            int32_t l_562[10][2] = {{0L,0xAFFA7EBEL},{0L,0L},{0L,0xAFFA7EBEL},{0L,0L},{0L,0xAFFA7EBEL},{0L,0L},{0L,0xAFFA7EBEL},{0L,0L},{0L,0xAFFA7EBEL},{0L,0L}};
            int32_t *l_563[8][5] = {{&l_120[0],&l_120[0],&l_120[0],&l_120[0],&l_120[0]},{(void*)0,&l_120[0],(void*)0,&l_120[0],(void*)0},{&l_120[0],&l_120[0],&l_120[0],&l_120[0],&l_120[0]},{(void*)0,&l_120[0],(void*)0,&l_120[0],(void*)0},{&l_120[0],&l_120[0],&l_120[0],&l_120[0],&l_120[0]},{(void*)0,&l_120[0],(void*)0,&l_120[0],(void*)0},{&l_120[0],&l_120[0],&l_120[0],&l_120[0],&l_120[0]},{(void*)0,&l_120[0],(void*)0,&l_120[0],(void*)0}};
            int i, j;
        }
        for (g_548 = 0; (g_548 <= 2); g_548 += 1)
        { /* block id: 323 */
            float ***l_652 = &g_395;
            uint32_t ***l_660 = (void*)0;
            int32_t l_684 = (-9L);
            int32_t l_687 = 0x89F52F20L;
            int32_t l_689 = 0xF487A3D6L;
            int16_t l_820 = 0xBBC4L;
            int16_t **l_856 = &l_718;
            int32_t *l_857 = (void*)0;
            uint8_t l_920 = 0x6CL;
            int i;
        }
        l_967 ^= (safe_sub_func_int64_t_s_s((*l_783), ((((safe_add_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(((((*l_966) |= (((*l_943) = g_941) == (((safe_unary_minus_func_uint64_t_u((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((l_949 >= ((**l_123) || (safe_div_func_int8_t_s_s((((safe_div_func_int64_t_s_s((((*l_132) = ((safe_mod_func_uint8_t_u_u((**l_123), ((safe_sub_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((g_621 = 65535UL), (g_275 = ((((safe_mul_func_int8_t_s_s(((~(((g_775 ^= (((l_119 = ((safe_div_func_uint64_t_u_u(0x8E049DE3F9943964LL, (**l_123))) , (**l_123))) <= 0x15ACDE51L) & (*l_132))) ^ 65535UL) > (*g_494))) , (*g_422)), 0x74L)) == (*l_132)) & 0x83D4L) < (**l_123))))), 9L)) || (*l_783)))) >= 0xB542BCD4L)) == (*l_783)), 3L)) != 0xD294L) == (*g_138)), (*g_422))))), (*l_783))), (*g_422))))) > 3UL) , &g_411))) | (*g_422)) && (*l_109)), 2)), (*l_783))), 0UL)) & (*g_488)) , g_722) , 0x58977D4DE743C361LL)));
    }
    else
    { /* block id: 449 */
        uint32_t l_990 = 8UL;
        float ** const l_998 = &g_396[0];
        uint8_t *l_999[4] = {&l_859,&l_859,&l_859,&l_859};
        int32_t l_1000[6] = {(-7L),1L,(-2L),1L,(-2L),(-2L)};
        uint16_t l_1001 = 65535UL;
        uint32_t ***l_1007 = &g_137[3];
        int32_t **l_1015 = &l_109;
        int i;
        (**g_395) = (safe_add_func_float_f_f(((safe_sub_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s(((((*l_718) = ((((*g_494) = (*l_783)) || ((*l_783) <= ((!((*l_783) >= (l_1000[0] = (~(safe_add_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s((safe_sub_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s((safe_add_func_int32_t_s_s((l_990 & (l_990 > (safe_mod_func_uint32_t_u_u(((!(safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(251UL, g_67)), g_17[0]))) != ((l_998 != (void*)0) < l_990)), l_990)))), l_990)), 12)), 8UL)), 12)), 0x4EL)))))) ^ l_1001))) | l_1001)) , (*g_492)) == (*l_898)), l_990)) >= (*l_783)), (*l_783))) <= l_990), g_181)) < (*l_783)), l_1001)) , l_1000[0]), (-0x8.6p+1)));
        (*l_1015) = ((((safe_unary_minus_func_uint64_t_u(l_1000[0])) > (safe_div_func_int64_t_s_s(((((0xF384L >= g_181) & ((((safe_lshift_func_uint8_t_u_s(((l_1007 != l_160) <= ((safe_lshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s((!((0L < (((((((safe_rshift_func_int8_t_s_u(((**g_395) , (((l_732 != (void*)0) , g_238) != g_238)), 2)) > (*g_422)) , (void*)0) == (void*)0) != 8UL) < 4294967286UL) >= l_1000[5])) & (*l_783))), g_478)), 0)) | g_926)), 5)) , g_690) > (*l_783)) | l_1000[0])) <= 0xDBL) , 0L), l_990))) || (*l_783)) , p_95);
        for (g_549 = 18; (g_549 >= 57); ++g_549)
        { /* block id: 457 */
            if ((*l_783))
                break;
            for (g_590 = (-2); (g_590 > 32); ++g_590)
            { /* block id: 461 */
                (*l_1015) = p_95;
                return g_67;
            }
            return g_438;
        }
        (**l_998) = (l_1015 != (void*)0);
    }
    l_1022 |= ((0x04C93C3BL == l_1020[0]) , 1L);
    for (g_621 = (-2); (g_621 != 5); g_621 = safe_add_func_uint8_t_u_u(g_621, 7))
    { /* block id: 472 */
        int32_t *l_1025[7];
        int8_t **l_1058 = (void*)0;
        uint64_t **l_1101[7][5][7] = {{{&l_909,&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,(void*)0,&l_909,&l_909,(void*)0},{&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909}},{{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909}},{{&l_909,&l_909,&l_909,&l_909,(void*)0,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,(void*)0,&l_909},{&l_909,&l_909,(void*)0,(void*)0,&l_909,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,(void*)0,&l_909,&l_909},{&l_909,&l_909,(void*)0,&l_909,(void*)0,&l_909,&l_909}},{{&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,(void*)0,&l_909,&l_909},{&l_909,&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909}},{{&l_909,&l_909,(void*)0,&l_909,&l_909,&l_909,(void*)0},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909}},{{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,(void*)0,(void*)0},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909}},{{&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,(void*)0,&l_909,&l_909,&l_909,&l_909,&l_909},{&l_909,&l_909,&l_909,&l_909,&l_909,(void*)0,(void*)0}}};
        int16_t l_1119 = (-4L);
        uint8_t *l_1180 = &g_343;
        uint32_t l_1183 = 0x4107CF6CL;
        uint32_t l_1199 = 0xC10007E5L;
        float l_1202 = 0x1.66F3F2p+22;
        int8_t *l_1203 = &l_108[0];
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_1025[i] = (void*)0;
        for (g_260 = 0; (g_260 <= 2); g_260 += 1)
        { /* block id: 475 */
            int32_t **l_1026 = &l_1025[1];
            int32_t l_1027 = 0x12B9A10FL;
            uint64_t *l_1078 = (void*)0;
            int8_t *l_1143 = &g_393;
            (*l_1026) = l_1025[0];
            ++g_1029;
            for (g_63 = 1; (g_63 <= 6); g_63 += 1)
            { /* block id: 480 */
                uint8_t l_1032 = 4UL;
                int8_t l_1064 = 0x79L;
                int32_t l_1065 = 0L;
                int32_t l_1067 = 0xBF9EE03EL;
                int32_t l_1120[8][4][8] = {{{0xA8DCCBCFL,0x8D889825L,0x89474210L,0L,(-1L),0x034D2ADCL,(-1L),0x5CD6167FL},{(-1L),0x034D2ADCL,(-1L),0x5CD6167FL,0xC8915A20L,0x191BF796L,0xC8915A20L,0x5CD6167FL},{(-10L),0xFEE66A65L,(-10L),0L,1L,0xD5402FA3L,1L,0x191BF796L},{0xEC94DE7AL,0x01F8C086L,1L,0x8D889825L,(-1L),2L,1L,0xBA693D6DL}},{{0xEC94DE7AL,0L,0xC8915A20L,(-7L),1L,0x8D889825L,0x12CCC787L,0xD5402FA3L},{(-10L),0xF781F75BL,0xA8DCCBCFL,0xF028E87BL,0xC8915A20L,0xFEE66A65L,0xD4B6BC8EL,0xFEE66A65L},{(-1L),0xF781F75BL,0x12CCC787L,0xF781F75BL,(-1L),0x8D889825L,0xEC94DE7AL,3L},{0xA8DCCBCFL,0L,(-1L),0x191BF796L,0L,2L,(-1L),0xF781F75BL}},{{0xD4B6BC8EL,0x01F8C086L,(-1L),(-3L),0x64C58D5DL,0xD5402FA3L,0xEC94DE7AL,(-1L)},{0L,0xFEE66A65L,0x12CCC787L,3L,(-1L),0x191BF796L,0xD4B6BC8EL,0x8D889825L},{0x12CCC787L,0x034D2ADCL,0xA8DCCBCFL,3L,0xA8DCCBCFL,0x034D2ADCL,0x12CCC787L,(-1L)},{0x64C58D5DL,0x8D889825L,0xC8915A20L,(-3L),0x05A3B2E2L,(-1L),1L,0xF781F75BL}},{{0L,0xF028E87BL,1L,0x191BF796L,0x05A3B2E2L,0L,1L,3L},{0x64C58D5DL,3L,(-10L),0xF781F75BL,0xA8DCCBCFL,0xF028E87BL,0xC8915A20L,0xFEE66A65L},{0x12CCC787L,0xD5402FA3L,(-1L),0xF028E87BL,(-1L),0xF028E87BL,(-1L),0xD5402FA3L},{0L,3L,0x89474210L,(-7L),0x64C58D5DL,0L,0xA8DCCBCFL,0xBA693D6DL}},{{0xD4B6BC8EL,0xF028E87BL,0x05A3B2E2L,0x8D889825L,0L,0L,0x12CCC787L,(-3L)},{0x12CCC787L,0x5CD6167FL,1L,0x01F8C086L,(-10L),2L,0x05A3B2E2L,3L},{(-10L),2L,0x05A3B2E2L,3L,(-1L),(-3L),(-1L),3L},{1L,0xF028E87BL,1L,0x01F8C086L,0x89474210L,0xF781F75BL,0xD4B6BC8EL,(-3L)}},{{0L,0L,0x64C58D5DL,0x5CD6167FL,0x05A3B2E2L,0xFEE66A65L,0x89474210L,0x191BF796L},{0L,0xBA693D6DL,(-1L),(-1L),0x89474210L,0x5CD6167FL,0L,0xF781F75BL},{1L,0x8D889825L,0x12CCC787L,0xD5402FA3L,(-1L),0xF028E87BL,(-1L),0xF028E87BL},{(-10L),0x8D889825L,0L,0x8D889825L,(-10L),0x5CD6167FL,0L,(-7L)}},{{0x12CCC787L,0xBA693D6DL,(-10L),(-3L),1L,0xFEE66A65L,0xA8DCCBCFL,0x8D889825L},{(-1L),0L,(-10L),0x034D2ADCL,0xC8915A20L,0xF781F75BL,0L,0L},{1L,0xF028E87BL,0L,(-7L),0xA8DCCBCFL,(-3L),(-1L),0x5CD6167FL},{0L,2L,0x12CCC787L,(-7L),0x12CCC787L,2L,0L,0L}},{{0xC8915A20L,0x5CD6167FL,(-1L),0x034D2ADCL,(-1L),0L,0x89474210L,0x8D889825L},{0xEC94DE7AL,0xD5402FA3L,0x64C58D5DL,(-3L),(-1L),0x01F8C086L,0xD4B6BC8EL,(-7L)},{0xC8915A20L,(-7L),1L,0x8D889825L,0x12CCC787L,0xD5402FA3L,(-1L),0xF028E87BL},{0L,0xF781F75BL,0x05A3B2E2L,0xD5402FA3L,0xA8DCCBCFL,0xD5402FA3L,0x05A3B2E2L,0xF781F75BL}}};
                int64_t **l_1122 = &l_118[g_63][(g_260 + 1)];
                union U0 *l_1159 = &g_1160[1];
                int i, j, k;
                for (g_927 = 0; (g_927 <= 3); g_927 += 1)
                { /* block id: 483 */
                    const int32_t l_1061 = (-1L);
                    int i, j, k;
                    l_1032++;
                    if (g_454[g_260][(g_260 + 1)][g_927])
                    { /* block id: 485 */
                        int32_t l_1059 = (-10L);
                        int8_t l_1060 = 0x45L;
                        uint32_t *l_1062 = &l_266;
                        int8_t *l_1063 = &l_108[3];
                        int i, j, k;
                        g_288[(g_927 + 1)][(g_260 + 2)][g_927] = ((safe_unary_minus_func_int16_t_s(0x6350L)) , (l_118[g_927][g_927] != l_118[g_927][(g_927 + 6)]));
                        l_1067 = (+((((safe_sub_func_uint64_t_u_u(g_65, (((g_549 | ((l_1065 = (safe_mod_func_uint64_t_u_u(((((((*l_1063) = ((((safe_lshift_func_int16_t_s_u((safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u((((*l_718) = ((safe_div_func_int64_t_s_s(((((*l_1062) |= (safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u(((((0x834B51A3ED1D3440LL != (((+9L) ^ (g_1029 > (((g_275 = g_538) != (l_1058 != &g_319)) && (((((((((((*g_422) = (((l_1059 && l_1059) ^ 0x8F36162F9A604221LL) < l_1032)) && l_1027) < (-2L)) & g_775) && 0x69L) & (*g_494)) == l_1060) == 8L) && 0x929FE786C600F2C4LL) | l_1061)))) > (*g_138))) != l_1032) >= l_1032) ^ g_343), 7)) ^ g_454[0][1][1]), l_1027)), 254UL))) == (*g_138)) >= 65532UL), l_1059)) , (*l_783))) <= l_1032), g_1029)), 0x73L)), g_404)) >= l_1032) < 0xCBL) >= (-8L))) , l_1064) , 0xDFL) & l_1060) || 0xC4L), 0xED74134E6CBEC4E6LL))) , l_1032)) , 7UL) & l_1060))) == 0L) | (*g_494)) ^ 0xBFL));
                        if (l_1060)
                            break;
                    }
                    else
                    { /* block id: 495 */
                        int32_t l_1068 = (-5L);
                        int32_t l_1069[7];
                        uint64_t l_1070[5][6][1] = {{{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL}},{{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL}},{{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL}},{{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL}},{{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL},{0UL},{0x2F015143D549C4AALL}}};
                        int64_t l_1075 = 4L;
                        int64_t **l_1077 = &g_494;
                        int64_t ***l_1076 = &l_1077;
                        uint8_t *l_1079 = (void*)0;
                        uint8_t *l_1080 = (void*)0;
                        uint8_t *l_1081[8][7][4] = {{{&g_181,&g_343,&g_480,&g_480},{(void*)0,&g_480,&l_859,&g_181},{&l_859,&l_1032,(void*)0,&g_181},{&g_181,(void*)0,&l_1032,&l_1032},{&g_181,(void*)0,&l_1032,&g_480},{&l_859,&g_181,&g_181,(void*)0},{&l_1032,&g_181,&l_1032,&g_181}},{{&g_181,&g_181,&l_1032,&l_859},{&l_1032,&g_343,&l_930,&g_181},{&g_343,&g_181,(void*)0,&l_1032},{&g_343,&g_343,&g_181,&l_1032},{&l_930,&g_343,&g_343,&g_181},{&g_343,&g_181,&g_181,&g_480},{&g_181,&g_480,&l_859,&g_181}},{{&g_480,&l_1032,&g_480,(void*)0},{(void*)0,&l_930,&l_1032,&l_859},{&l_1032,(void*)0,&g_181,&g_181},{&l_1032,&l_1032,(void*)0,&g_181},{&g_480,&l_859,&l_859,(void*)0},{&g_480,&g_181,(void*)0,&l_859},{&l_930,&g_181,&l_1032,(void*)0}},{{&g_181,&l_859,(void*)0,&g_181},{&g_480,&l_1032,&g_480,&g_181},{&l_1032,(void*)0,(void*)0,&l_859},{&l_1032,&l_930,&g_181,(void*)0},{(void*)0,&l_1032,&l_859,&g_181},{&g_181,&g_480,(void*)0,&g_480},{&l_859,&g_181,&g_343,&g_181}},{{&l_859,&g_343,&g_181,&l_1032},{&g_480,&g_343,(void*)0,&l_1032},{&g_181,&g_181,&l_930,&g_181},{&g_181,&g_480,(void*)0,(void*)0},{&g_480,&g_181,&g_181,&l_930},{&l_859,&g_480,&g_343,&g_181},{&l_859,&l_1032,(void*)0,&l_859}},{{&g_181,&g_480,&l_859,&g_181},{(void*)0,&g_181,&g_181,&g_480},{&l_1032,&g_181,(void*)0,(void*)0},{&l_1032,&g_480,&g_480,&l_1032},{&g_480,(void*)0,(void*)0,&g_181},{&g_181,&g_181,&l_1032,&g_480},{&l_930,&l_930,(void*)0,&g_480}},{{&g_480,&g_181,&l_859,&g_181},{&g_480,(void*)0,(void*)0,&l_1032},{&l_1032,&g_480,&g_181,(void*)0},{&l_1032,&g_181,&l_1032,&g_480},{(void*)0,&g_181,&g_480,&g_181},{&g_480,&g_480,&l_859,&l_859},{&g_181,&l_1032,&g_181,&g_181}},{{&g_343,&g_480,&g_343,&l_930},{&l_930,&g_181,&g_181,(void*)0},{&g_343,&g_480,(void*)0,&g_181},{&g_181,&g_181,(void*)0,&l_1032},{&g_343,&g_343,&g_181,&l_1032},{&l_930,&g_343,&g_343,&g_181},{&g_343,&g_181,&g_181,&g_480}}};
                        int i, j, k;
                        for (i = 0; i < 7; i++)
                            l_1069[i] = (-6L);
                        l_1070[3][1][0]--;
                        l_1065 |= (safe_rshift_func_int16_t_s_s(2L, 14));
                        l_1067 = ((*g_422) & ((((*g_422) != (g_480 |= (((((0xC3L & (18446744073709551611UL == l_1075)) , (*g_492)) != ((*l_1076) = (*g_492))) == (&g_454[1][2][2] != l_1078)) != l_1027))) , l_1070[1][2][0]) | 1L));
                        g_288[(g_260 + 3)][g_927][g_927] = (**g_395);
                    }
                }
                if (((((((g_181 = (g_454[g_260][g_260][g_260] > (safe_add_func_int64_t_s_s(5L, (safe_sub_func_int64_t_s_s(0x29DE7E92CBA06D3ALL, (safe_lshift_func_uint8_t_u_s((safe_mod_func_uint8_t_u_u(((((g_454[g_260][g_260][g_260] | (safe_mul_func_int8_t_s_s(l_1064, (g_366 > ((g_1092[3] &= g_454[g_260][g_260][g_260]) || (0x5B38L & (((safe_mul_func_uint8_t_u_u(((((safe_sub_func_float_f_f(0x6.0p-1, (**g_395))) , 0UL) || 0x3F4EAA9AL) & g_454[g_260][g_260][g_260]), g_284[3][2])) && l_1064) , g_454[2][0][1]))))))) != 0x218EL) & g_454[g_260][g_260][g_260]) , 0x02L), g_505)), 4)))))))) < g_590) != 0xE035L) , 7L) < 0x61D76C8717134BF6LL) , g_454[g_260][g_260][g_260]))
                { /* block id: 506 */
                    const uint32_t l_1106 = 0x89AC47F8L;
                    int16_t *l_1118[10][7] = {{&g_538,&g_538,&l_394[0][5][0],&g_538,(void*)0,&g_538,(void*)0},{(void*)0,&l_394[0][6][1],&l_394[0][6][1],(void*)0,&l_394[0][6][1],&l_394[0][6][1],(void*)0},{&l_394[0][5][0],(void*)0,&l_394[0][5][0],&l_394[0][5][0],(void*)0,&l_394[0][5][0],&l_394[0][5][0]},{(void*)0,(void*)0,&l_394[0][7][1],(void*)0,(void*)0,&l_394[0][7][1],(void*)0},{(void*)0,&l_394[0][5][0],&l_394[0][5][0],(void*)0,&l_394[0][5][0],&l_394[0][5][0],(void*)0},{&l_394[0][6][1],(void*)0,&l_394[0][6][1],&l_394[0][6][1],(void*)0,&l_394[0][6][1],&l_394[0][6][1]},{(void*)0,(void*)0,&g_538,(void*)0,(void*)0,&g_538,(void*)0},{(void*)0,&l_394[0][6][1],&l_394[0][6][1],(void*)0,&l_394[0][6][1],&l_394[0][6][1],(void*)0},{&l_394[0][5][0],(void*)0,&l_394[0][5][0],&l_394[0][5][0],(void*)0,&l_394[0][5][0],&l_394[0][5][0]},{(void*)0,(void*)0,&l_394[0][7][1],(void*)0,(void*)0,&l_394[0][7][1],(void*)0}};
                    int32_t l_1121 = 3L;
                    int32_t ***l_1133 = &l_1026;
                    int i, j;
                    if ((*g_488))
                        break;
                    if ((safe_lshift_func_uint16_t_u_s((((safe_mod_func_uint64_t_u_u((g_722 == (l_1121 &= (g_478 ^= (l_1120[5][3][3] |= ((((void*)0 == l_1101[5][1][6]) && (safe_mul_func_uint16_t_u_u(((((*g_422) = (safe_add_func_uint64_t_u_u(0UL, l_1106))) == (-4L)) >= ((((safe_lshift_func_uint16_t_u_s((!(safe_sub_func_int64_t_s_s((l_1067 = ((*g_494) ^= (safe_lshift_func_uint8_t_u_s(((l_1065 |= ((safe_div_func_int16_t_s_s((g_475 &= ((*l_718) = (g_284[3][2] , g_343))), (safe_sub_func_int32_t_s_s((-7L), 0xB9762002L)))) , g_1029)) ^ 0L), g_1092[0])))), l_1106))), g_927)) , 2UL) , l_1119) <= g_538)), 0xDA32L))) ^ g_927))))), g_17[0])) || (*g_494)) | 0x60L), 7)))
                    { /* block id: 517 */
                        int8_t l_1123 = (-6L);
                        int8_t *l_1144 = &g_235;
                        l_1123 |= (((0x27L != (*g_422)) == ((0xA3L <= (l_1120[5][1][2] && (((((p_95 == (void*)0) != (((&g_548 == (void*)0) , l_1122) != (*g_492))) & l_1106) < 0x140CL) || 0x3BL))) && 0x46D0CAA3L)) ^ g_63);
                        l_1065 = ((l_1106 , l_1120[4][0][5]) & (l_1123 <= (safe_mul_func_uint8_t_u_u(l_1106, ((+((safe_lshift_func_uint8_t_u_s(((((safe_sub_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((((l_1133 != ((safe_mul_func_int16_t_s_s(g_454[g_260][g_260][g_260], ((safe_div_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u(((*g_488) > (safe_add_func_int16_t_s_s((~((l_1144 = l_1143) == &l_1123)), (-1L)))), 5)), l_1123)) >= (*g_138)))) , (void*)0)) > l_1123) || g_181), 4294967295UL)), g_366)) ^ g_454[g_260][g_260][g_260]) >= l_1027) , l_1065), g_1145)) >= 0x71L)) > 255UL)))));
                    }
                    else
                    { /* block id: 521 */
                        int i;
                        l_1025[g_63] = (void*)0;
                    }
                }
                else
                { /* block id: 524 */
                    float l_1150 = 0x6.BB389Dp-86;
                    int32_t l_1155 = (-1L);
                    uint64_t ***l_1158 = &g_1156;
                    if ((safe_mul_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((((((((g_775--) ^ g_320) , (safe_mul_func_uint16_t_u_u(l_1155, g_480))) && 1L) && l_1032) > 0x159CFBBBL) > (((*g_488) > (((*l_1158) = g_1156) != (void*)0)) != g_17[5])), l_1155)), 0x47EFL)))
                    { /* block id: 527 */
                        union U0 **l_1161 = &l_1159;
                        (*l_1161) = l_1159;
                        l_1067 |= (*g_488);
                    }
                    else
                    { /* block id: 530 */
                        uint32_t l_1162 = 5UL;
                        g_253 = (l_1162 != (((0x49C88E1AL > ((g_275 , 0x04FA053CL) < ((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_s((0xA6E6981DL & ((g_454[g_260][g_260][g_260] , l_1143) == &l_1032)), l_1155)), g_690)) ^ g_284[3][2]))) , (*g_494)) != 0L));
                    }
                }
            }
            for (g_480 = 0; (g_480 <= 3); g_480 += 1)
            { /* block id: 537 */
                int32_t l_1169 = 0L;
                uint16_t l_1170[2][9][4] = {{{65530UL,0x2FB8L,65527UL,0x3822L},{65527UL,0x3822L,5UL,0x6850L},{0x4FECL,0x34DCL,0x3400L,0x6850L},{0x666AL,0x3822L,0x4FECL,0x3822L},{0x79AEL,0x2FB8L,0x99F1L,8UL},{5UL,0x79DCL,0x666AL,6UL},{0x34DCL,0x6850L,6UL,5UL},{0x34DCL,65530UL,0x666AL,0x666AL},{5UL,5UL,0x99F1L,0UL}},{{0x79AEL,0x99F1L,0x4FECL,0x2FB8L},{0x666AL,65527UL,0x3400L,0x4FECL},{0x4FECL,65527UL,5UL,0x2FB8L},{65527UL,0x99F1L,65527UL,0UL},{65530UL,5UL,0x79DCL,0x666AL},{8UL,65530UL,0UL,5UL},{0x2FB8L,0x6850L,0UL,6UL},{8UL,0x79DCL,0x79DCL,8UL},{65530UL,0x2FB8L,65527UL,0x3822L}}};
                int32_t *l_1173 = &g_253;
                int i, j, k;
                ++l_1170[1][3][3];
                l_1173 = (void*)0;
                return g_454[g_260][(g_260 + 2)][g_480];
            }
        }
        if ((safe_mod_func_int8_t_s_s(((*l_1203) = (safe_div_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u(((*l_1180) ^= ((*l_783) == (*g_1157))), ((*g_422) = ((safe_add_func_int16_t_s_s((((--l_1183) && (*l_783)) && ((((safe_add_func_uint64_t_u_u((safe_mod_func_uint16_t_u_u((safe_sub_func_int16_t_s_s(((+(safe_add_func_int8_t_s_s((((***l_160) ^= (l_783 == &g_66[5][2])) && (safe_rshift_func_uint16_t_u_u(0x1BF3L, 13))), ((safe_lshift_func_uint8_t_u_s(g_392, (*g_422))) != 1L)))) , l_1199), g_235)), l_1200)), (-1L))) <= g_393) , 9UL) <= (-1L))), g_775)) || l_1201)))) || (*g_488)), (*l_783)))), g_66[5][1])))
        { /* block id: 548 */
            uint32_t l_1208 = 18446744073709551615UL;
            int32_t l_1226 = 1L;
            for (g_260 = 0; (g_260 >= 35); g_260++)
            { /* block id: 551 */
                uint32_t l_1214[2];
                float *l_1215 = (void*)0;
                float *l_1216 = &g_469;
                int32_t l_1227 = 0x5E9EE8B8L;
                int i;
                for (i = 0; i < 2; i++)
                    l_1214[i] = 0x3CBF9A01L;
                (*l_1216) = (safe_mul_func_float_f_f(((((void*)0 != p_95) == 0x8.A338D1p-97) >= 0x0.5513BBp-23), ((**g_395) = ((l_1208 <= (**g_395)) < (safe_sub_func_float_f_f((l_1208 >= (safe_add_func_float_f_f(l_1208, g_1213[0]))), l_1214[1]))))));
                l_1227 &= (((safe_sub_func_uint16_t_u_u(l_1214[1], (((&g_253 != (void*)0) & (safe_lshift_func_uint16_t_u_u(l_1214[1], (safe_rshift_func_uint8_t_u_u(g_781, 1))))) > (*l_783)))) != (l_1225 ^ ((*g_138) > ((*g_494) & 0xA13B71A1BEC0A8A9LL)))) || l_1226);
            }
            if ((*g_488))
                continue;
        }
        else
        { /* block id: 557 */
            int8_t l_1228 = 0xB0L;
            if ((*g_488))
                break;
            if (l_1228)
                continue;
            return (**g_1156);
        }
        (**g_395) = (**g_395);
    }
    return (**g_1156);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_17[i], "g_17[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_63, "g_63", print_hash_value);
    transparent_crc(g_65, "g_65", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_66[i][j], "g_66[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_67, "g_67", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_171, "g_171", print_hash_value);
    transparent_crc(g_181, "g_181", print_hash_value);
    transparent_crc(g_235, "g_235", print_hash_value);
    transparent_crc(g_253, "g_253", print_hash_value);
    transparent_crc(g_260, "g_260", print_hash_value);
    transparent_crc(g_275, "g_275", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_284[i][j], "g_284[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc_bytes(&g_288[i][j][k], sizeof(g_288[i][j][k]), "g_288[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_320, "g_320", print_hash_value);
    transparent_crc(g_332, "g_332", print_hash_value);
    transparent_crc(g_343, "g_343", print_hash_value);
    transparent_crc(g_355, "g_355", print_hash_value);
    transparent_crc(g_366, "g_366", print_hash_value);
    transparent_crc(g_392, "g_392", print_hash_value);
    transparent_crc(g_393, "g_393", print_hash_value);
    transparent_crc(g_404, "g_404", print_hash_value);
    transparent_crc(g_438, "g_438", print_hash_value);
    transparent_crc_bytes (&g_444, sizeof(g_444), "g_444", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_454[i][j][k], "g_454[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_469, sizeof(g_469), "g_469", print_hash_value);
    transparent_crc(g_475, "g_475", print_hash_value);
    transparent_crc(g_478, "g_478", print_hash_value);
    transparent_crc(g_480, "g_480", print_hash_value);
    transparent_crc(g_505, "g_505", print_hash_value);
    transparent_crc(g_538, "g_538", print_hash_value);
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc(g_549, "g_549", print_hash_value);
    transparent_crc(g_590, "g_590", print_hash_value);
    transparent_crc_bytes (&g_614, sizeof(g_614), "g_614", print_hash_value);
    transparent_crc(g_621, "g_621", print_hash_value);
    transparent_crc(g_690, "g_690", print_hash_value);
    transparent_crc(g_722, "g_722", print_hash_value);
    transparent_crc(g_746, "g_746", print_hash_value);
    transparent_crc(g_774, "g_774", print_hash_value);
    transparent_crc(g_775, "g_775", print_hash_value);
    transparent_crc(g_781, "g_781", print_hash_value);
    transparent_crc(g_926, "g_926", print_hash_value);
    transparent_crc(g_927, "g_927", print_hash_value);
    transparent_crc_bytes (&g_1028, sizeof(g_1028), "g_1028", print_hash_value);
    transparent_crc(g_1029, "g_1029", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1092[i], "g_1092[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1145, "g_1145", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1160[i].f0, "g_1160[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1213[i], "g_1213[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1234, "g_1234", print_hash_value);
    transparent_crc(g_1305.f0, "g_1305.f0", print_hash_value);
    transparent_crc(g_1330, "g_1330", print_hash_value);
    transparent_crc(g_1334.f0, "g_1334.f0", print_hash_value);
    transparent_crc(g_1335.f0, "g_1335.f0", print_hash_value);
    transparent_crc(g_1336.f0, "g_1336.f0", print_hash_value);
    transparent_crc(g_1337.f0, "g_1337.f0", print_hash_value);
    transparent_crc(g_1355, "g_1355", print_hash_value);
    transparent_crc_bytes (&g_1380, sizeof(g_1380), "g_1380", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1384[i][j][k], "g_1384[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1518, "g_1518", print_hash_value);
    transparent_crc(g_1519, "g_1519", print_hash_value);
    transparent_crc(g_1548, "g_1548", print_hash_value);
    transparent_crc(g_1565, "g_1565", print_hash_value);
    transparent_crc(g_1571.f0, "g_1571.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1592[i], "g_1592[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1625, sizeof(g_1625), "g_1625", print_hash_value);
    transparent_crc(g_1629, "g_1629", print_hash_value);
    transparent_crc(g_1653, "g_1653", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1654[i][j][k], "g_1654[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1657[i], "g_1657[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1658, "g_1658", print_hash_value);
    transparent_crc(g_1659, "g_1659", print_hash_value);
    transparent_crc(g_1665, "g_1665", print_hash_value);
    transparent_crc_bytes (&g_1667, sizeof(g_1667), "g_1667", print_hash_value);
    transparent_crc(g_1668, "g_1668", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1733[i], "g_1733[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1921, "g_1921", print_hash_value);
    transparent_crc(g_1935, "g_1935", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1960[i][j][k], "g_1960[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_2015, sizeof(g_2015), "g_2015", print_hash_value);
    transparent_crc(g_2092, "g_2092", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_2130[i][j][k], "g_2130[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2159[i][j].f0, "g_2159[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2285, "g_2285", print_hash_value);
    transparent_crc(g_2369.f0, "g_2369.f0", print_hash_value);
    transparent_crc(g_2371.f0, "g_2371.f0", print_hash_value);
    transparent_crc(g_2531, "g_2531", print_hash_value);
    transparent_crc(g_2612, "g_2612", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2858[i], "g_2858[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2990[i], "g_2990[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_3242[i][j], "g_3242[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3315.f0, "g_3315.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_3412[i], "g_3412[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3433.f0, "g_3433.f0", print_hash_value);
    transparent_crc(g_3482.f0, "g_3482.f0", print_hash_value);
    transparent_crc(g_3583.f0, "g_3583.f0", print_hash_value);
    transparent_crc(g_3602, "g_3602", print_hash_value);
    transparent_crc(g_3662.f0, "g_3662.f0", print_hash_value);
    transparent_crc(g_3800, "g_3800", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 873
XXX total union variables: 3

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 17
breakdown:
   indirect level: 0, occurrence: 3
   indirect level: 1, occurrence: 10
   indirect level: 2, occurrence: 3
   indirect level: 3, occurrence: 1
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 38
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 3
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 57
breakdown:
   depth: 1, occurrence: 278
   depth: 2, occurrence: 67
   depth: 3, occurrence: 7
   depth: 4, occurrence: 1
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 5
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 1
   depth: 20, occurrence: 3
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 3
   depth: 26, occurrence: 2
   depth: 28, occurrence: 5
   depth: 31, occurrence: 2
   depth: 33, occurrence: 3
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 42, occurrence: 1
   depth: 45, occurrence: 1
   depth: 52, occurrence: 1
   depth: 57, occurrence: 1

XXX total number of pointers: 757

XXX times a variable address is taken: 1990
XXX times a pointer is dereferenced on RHS: 709
breakdown:
   depth: 1, occurrence: 493
   depth: 2, occurrence: 159
   depth: 3, occurrence: 35
   depth: 4, occurrence: 12
   depth: 5, occurrence: 10
XXX times a pointer is dereferenced on LHS: 541
breakdown:
   depth: 1, occurrence: 426
   depth: 2, occurrence: 68
   depth: 3, occurrence: 32
   depth: 4, occurrence: 13
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 86
XXX times a pointer is compared with address of another variable: 24
XXX times a pointer is compared with another pointer: 26
XXX times a pointer is qualified to be dereferenced: 8753

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2101
   level: 2, occurrence: 833
   level: 3, occurrence: 275
   level: 4, occurrence: 149
   level: 5, occurrence: 71
XXX number of pointers point to pointers: 327
XXX number of pointers point to scalars: 417
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 33.2
XXX average alias set size: 1.5

XXX times a non-volatile is read: 3545
XXX times a non-volatile is write: 1744
XXX times a volatile is read: 98
XXX    times read thru a pointer: 93
XXX times a volatile is write: 13
XXX    times written thru a pointer: 13
XXX times a volatile is available for access: 399
XXX percentage of non-volatile access: 97.9

XXX forward jumps: 0
XXX backward jumps: 14

XXX stmts: 271
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 35
   depth: 2, occurrence: 41
   depth: 3, occurrence: 36
   depth: 4, occurrence: 46
   depth: 5, occurrence: 80

XXX percentage a fresh-made variable is used: 17.5
XXX percentage an existing variable is used: 82.5
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


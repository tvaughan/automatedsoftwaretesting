/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      428874497
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   signed f0 : 31;
   int32_t  f1;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = (-1L);
static int16_t g_11[6][7] = {{(-5L),0x900FL,0x900FL,(-5L),(-1L),0xCD33L,(-1L)},{0xB696L,0x5637L,0x5637L,0xB696L,0xFA31L,0x9995L,0xFA31L},{(-5L),0x900FL,0x900FL,(-5L),(-1L),0xCD33L,(-1L)},{0xB696L,0x5637L,0x5637L,0xB696L,0xFA31L,0x9995L,0xFA31L},{(-5L),0x900FL,0x900FL,(-5L),(-1L),0xCD33L,(-1L)},{0xB696L,0x5637L,0x5637L,0xB696L,0xFA31L,0x9995L,0xFA31L}};
static uint8_t g_25 = 0xDEL;
static uint8_t g_36 = 248UL;
static volatile int32_t g_49 = 8L;/* VOLATILE GLOBAL g_49 */
static volatile int32_t g_50[10][9][1] = {{{(-1L)},{1L},{1L},{(-1L)},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}},{{0xCCDF493AL},{1L},{1L},{0xCCDF493AL},{(-1L)},{(-10L)},{0xCCDF493AL},{(-10L)},{(-1L)}}};
static int32_t g_51[4][3][9] = {{{0L,0x63070B7BL,0L,0x63070B7BL,0L,0L,0L,0L,0x63070B7BL},{0xAAC3BC5FL,0x912742E7L,0xAAC3BC5FL,1L,(-2L),0xAE7D5126L,0x9ACCE304L,0L,0x6462EF7CL},{0x20FAE236L,0xFD7F9B64L,0L,0L,0L,0L,0xFD7F9B64L,0x20FAE236L,0xFD7F9B64L}},{{(-1L),0x06546D79L,0x9ACCE304L,1L,4L,0x474477D7L,(-2L),0x474477D7L,4L},{0x63070B7BL,0xFD7F9B64L,0xFD7F9B64L,0x63070B7BL,0x20FAE236L,0L,0x20FAE236L,0x63070B7BL,0xFD7F9B64L},{0x6462EF7CL,0x912742E7L,(-2L),0x06546D79L,0xC2B13F3BL,0x06546D79L,(-2L),0x912742E7L,0x6462EF7CL}},{{0xFD7F9B64L,0x63070B7BL,0x20FAE236L,0L,0x20FAE236L,0x63070B7BL,0xFD7F9B64L,0xFD7F9B64L,0x63070B7BL},{4L,0x474477D7L,(-2L),0x474477D7L,4L,1L,0x9ACCE304L,0x06546D79L,(-1L)},{0xFD7F9B64L,0x20FAE236L,0xFD7F9B64L,0L,0L,0L,0L,0xFD7F9B64L,0x20FAE236L}},{{0x6462EF7CL,0L,0x9ACCE304L,0xAE7D5126L,(-2L),1L,0xAAC3BC5FL,0x912742E7L,0xAAC3BC5FL},{0x63070B7BL,0L,0L,0L,0L,0x63070B7BL,0L,0x63070B7BL,0L},{(-1L),0L,0xAAC3BC5FL,0x474477D7L,0x6462EF7CL,0x06546D79L,0xC2B13F3BL,0L,4L}}};
static volatile int32_t g_54 = 0x5A33B115L;/* VOLATILE GLOBAL g_54 */
static int32_t g_55 = 0xF01E8351L;
static struct S0 g_61 = {3366,-2L};
static uint32_t g_68 = 0xDF81C58CL;
static uint8_t g_76 = 0x3EL;
static uint8_t *g_77 = &g_36;
static struct S0 ** volatile g_84 = (void*)0;/* VOLATILE GLOBAL g_84 */
static struct S0 *g_86 = &g_61;
static struct S0 ** volatile g_85 = &g_86;/* VOLATILE GLOBAL g_85 */
static uint64_t g_143 = 1UL;
static int32_t g_144[9][8][1] = {{{0x9DDDC56DL},{0xAE73F2C4L},{0xA435C01AL},{0xEB260A6FL},{0xEB260A6FL},{0xA435C01AL},{0xAE73F2C4L},{0x9DDDC56DL}},{{0xB5B8E14EL},{(-1L)},{0xACBF7365L},{(-1L)},{0xB5B8E14EL},{0x9DDDC56DL},{0xAE73F2C4L},{0xA435C01AL}},{{0xEB260A6FL},{0xEB260A6FL},{0xA435C01AL},{0xAE73F2C4L},{0x9DDDC56DL},{0xB5B8E14EL},{(-1L)},{0xACBF7365L}},{{(-1L)},{0xB5B8E14EL},{0x9DDDC56DL},{0xAE73F2C4L},{0xA435C01AL},{0xEB260A6FL},{0xEB260A6FL},{0xA435C01AL}},{{0xAE73F2C4L},{0x9DDDC56DL},{0xB5B8E14EL},{(-1L)},{0xACBF7365L},{(-1L)},{0xB5B8E14EL},{0x9DDDC56DL}},{{0xAE73F2C4L},{0xA435C01AL},{0xEB260A6FL},{0xEB260A6FL},{0xA435C01AL},{0xAE73F2C4L},{0x9DDDC56DL},{0xB5B8E14EL}},{{(-1L)},{0xACBF7365L},{(-1L)},{0xB5B8E14EL},{0x9DDDC56DL},{0xAE73F2C4L},{0xA435C01AL},{0xEB260A6FL}},{{0xEB260A6FL},{0xA435C01AL},{0xAE73F2C4L},{0x9DDDC56DL},{0xB5B8E14EL},{(-1L)},{0xACBF7365L},{(-1L)}},{{0xB5B8E14EL},{0x9DDDC56DL},{0xAE73F2C4L},{0xA435C01AL},{0xEB260A6FL},{0xEB260A6FL},{0xA435C01AL},{0xAE73F2C4L}}};
static int16_t g_162 = 0x4A47L;
static struct S0 g_165 = {-31310,0xCFFD04ABL};
static struct S0 * volatile g_164[6][4][3] = {{{&g_165,&g_61,&g_165},{&g_61,&g_165,&g_165},{&g_165,&g_165,&g_165},{&g_61,&g_61,&g_165}},{{&g_165,&g_165,&g_165},{&g_61,&g_165,&g_165},{&g_165,&g_165,&g_165},{&g_165,&g_61,&g_61}},{{&g_61,&g_165,&g_165},{&g_61,&g_165,&g_165},{&g_165,&g_61,&g_165},{&g_61,&g_61,&g_165}},{{&g_61,&g_165,&g_165},{&g_165,&g_61,&g_165},{&g_165,&g_61,&g_165},{&g_61,&g_165,&g_165}},{{&g_165,&g_165,&g_165},{&g_61,&g_61,&g_165},{&g_165,&g_165,&g_165},{&g_61,&g_165,&g_165}},{{&g_165,&g_165,&g_165},{&g_165,&g_61,&g_61},{&g_61,&g_165,&g_165},{&g_61,&g_165,&g_165}}};
static struct S0 * volatile g_166 = &g_165;/* VOLATILE GLOBAL g_166 */
static volatile int32_t *g_185 = &g_54;
static volatile int32_t ** volatile g_184[5][5][4] = {{{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,&g_185,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185}},{{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,&g_185,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185}},{{&g_185,(void*)0,&g_185,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,&g_185,&g_185}},{{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,&g_185,&g_185},{&g_185,(void*)0,(void*)0,&g_185}},{{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,&g_185,&g_185},{&g_185,(void*)0,(void*)0,&g_185},{&g_185,(void*)0,(void*)0,&g_185}}};
static volatile int32_t ** const  volatile g_186 = &g_185;/* VOLATILE GLOBAL g_186 */
static volatile int16_t g_190 = 0x42D0L;/* VOLATILE GLOBAL g_190 */
static volatile uint32_t g_196 = 0x8240C372L;/* VOLATILE GLOBAL g_196 */
static struct S0 **g_204 = &g_86;
static struct S0 ** volatile *g_203 = &g_204;
static int16_t g_228[5] = {0xE453L,0xE453L,0xE453L,0xE453L,0xE453L};
static int8_t g_255 = 0L;
static int64_t g_285 = 0xD8CB65861C7F1546LL;
static float g_289 = (-0x1.0p+1);
static float g_291 = 0x1.Bp-1;
static float * volatile g_290 = &g_291;/* VOLATILE GLOBAL g_290 */
static int32_t *g_293 = (void*)0;
static int32_t ** volatile g_311 = &g_293;/* VOLATILE GLOBAL g_311 */
static uint16_t g_335[1][10][1] = {{{0x6B95L},{0xCF7DL},{0x6B95L},{1UL},{1UL},{0x6B95L},{0xCF7DL},{0x6B95L},{1UL},{1UL}}};
static volatile uint32_t * const g_363 = &g_196;
static volatile uint32_t * const * volatile g_362[1] = {&g_363};
static uint32_t *g_480 = &g_68;
static uint32_t *g_484 = &g_68;
static int8_t * volatile *g_502 = (void*)0;
static volatile uint64_t g_573 = 0x729EFDE7555AE452LL;/* VOLATILE GLOBAL g_573 */
static uint32_t ** const *g_596 = (void*)0;
static uint16_t *g_673 = (void*)0;
static uint16_t ** volatile g_672 = &g_673;/* VOLATILE GLOBAL g_672 */
static uint16_t ** volatile * volatile g_674[4][5] = {{&g_672,(void*)0,(void*)0,&g_672,(void*)0},{&g_672,&g_672,(void*)0,&g_672,(void*)0},{&g_672,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_672,(void*)0,(void*)0}};
static const int32_t * volatile g_722 = (void*)0;/* VOLATILE GLOBAL g_722 */
static uint32_t **g_739 = (void*)0;
static uint32_t *** volatile g_743 = &g_739;/* VOLATILE GLOBAL g_743 */
static int32_t ** volatile g_793 = &g_293;/* VOLATILE GLOBAL g_793 */
static volatile uint32_t g_801[9] = {0xEC9758B9L,0xEC9758B9L,0xEC9758B9L,0xEC9758B9L,0xEC9758B9L,0xEC9758B9L,0xEC9758B9L,0xEC9758B9L,0xEC9758B9L};
static uint32_t g_809 = 6UL;
static struct S0 ***g_823 = &g_204;
static struct S0 ****g_822 = &g_823;
static struct S0 ***** volatile g_821[1][1] = {{&g_822}};
static const struct S0 **g_831 = (void*)0;
static const struct S0 ***g_830 = &g_831;
static const struct S0 ****g_829 = &g_830;
static const struct S0 *****g_828 = &g_829;
static const struct S0 *****g_832 = &g_829;
static int16_t g_847[1][8][8] = {{{0L,0L,(-2L),(-10L),0xA669L,0L,0xA669L,(-10L)},{(-8L),(-3L),(-8L),0L,(-4L),0xAE4EL,(-3L),0L},{(-10L),0xC6BBL,0xE474L,1L,0x44AFL,0xBB9BL,(-4L),0xE474L},{(-10L),0x44AFL,0xC6BBL,(-2L),(-4L),(-5L),(-5L),(-4L)},{(-8L),5L,5L,(-8L),0xA669L,0L,0xC6BBL,0L},{0L,0xCFF6L,1L,0xE474L,(-1L),0L,0L,(-2L)},{0L,0xCFF6L,(-8L),0xC6BBL,1L,0L,0xCFF6L,0L},{0x44AFL,5L,0x889FL,5L,0x44AFL,(-5L),1L,0x889FL}}};
static float * volatile g_863 = &g_291;/* VOLATILE GLOBAL g_863 */
static uint16_t *g_880[3][7] = {{&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]},{&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]},{&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]}};
static int8_t g_883 = 2L;
static uint16_t g_899 = 65535UL;
static uint64_t g_902 = 6UL;
static uint16_t **g_937 = &g_673;
static uint16_t ***g_936 = &g_937;
static int32_t ** volatile g_983 = (void*)0;/* VOLATILE GLOBAL g_983 */
static uint16_t g_994 = 0x85A4L;
static uint16_t g_995 = 0x4D6EL;
static uint16_t * const g_997[3] = {(void*)0,(void*)0,(void*)0};
static uint16_t * const *g_996[3] = {&g_997[2],&g_997[2],&g_997[2]};
static uint16_t g_1000 = 0x6801L;
static uint32_t g_1027[9][6][4] = {{{18446744073709551615UL,0xEEE3B5BFL,1UL,18446744073709551615UL},{0xEEE3B5BFL,0x8FE64973L,0UL,0x37C8FFADL},{0x299FA847L,18446744073709551615UL,18446744073709551615UL,0x37C8FFADL},{4UL,0x8FE64973L,4UL,18446744073709551615UL},{5UL,0xEEE3B5BFL,0x4A264581L,5UL},{0x299FA847L,18446744073709551615UL,1UL,0xEEE3B5BFL}},{{18446744073709551615UL,0x8FE64973L,1UL,1UL},{0x299FA847L,0x299FA847L,0x4A264581L,0x37C8FFADL},{5UL,18446744073709551615UL,4UL,0xEEE3B5BFL},{4UL,0xEEE3B5BFL,18446744073709551615UL,4UL},{0x299FA847L,0xEEE3B5BFL,0UL,0xEEE3B5BFL},{0xEEE3B5BFL,18446744073709551615UL,1UL,0x37C8FFADL}},{{18446744073709551615UL,0x299FA847L,18446744073709551615UL,1UL},{5UL,0x8FE64973L,0xE1515F3BL,0xEEE3B5BFL},{5UL,18446744073709551615UL,18446744073709551615UL,5UL},{18446744073709551615UL,0xEEE3B5BFL,1UL,18446744073709551615UL},{0xEEE3B5BFL,0x8FE64973L,0UL,0x37C8FFADL},{0x299FA847L,18446744073709551615UL,18446744073709551615UL,0x37C8FFADL}},{{4UL,0x8FE64973L,4UL,18446744073709551615UL},{5UL,0xEEE3B5BFL,0x4A264581L,5UL},{0x299FA847L,18446744073709551615UL,1UL,0xEEE3B5BFL},{18446744073709551615UL,0x8FE64973L,1UL,1UL},{0x299FA847L,0x299FA847L,0x4A264581L,0x37C8FFADL},{5UL,18446744073709551615UL,4UL,0xEEE3B5BFL}},{{4UL,0xEEE3B5BFL,18446744073709551615UL,4UL},{0x299FA847L,0xEEE3B5BFL,0UL,0xEEE3B5BFL},{0xEEE3B5BFL,18446744073709551615UL,1UL,0x37C8FFADL},{18446744073709551615UL,0x299FA847L,18446744073709551615UL,1UL},{5UL,0x8FE64973L,0xE1515F3BL,0xEEE3B5BFL},{5UL,18446744073709551615UL,18446744073709551615UL,5UL}},{{18446744073709551615UL,0xEEE3B5BFL,1UL,18446744073709551615UL},{0xEEE3B5BFL,0x8FE64973L,0UL,0x37C8FFADL},{0x299FA847L,18446744073709551615UL,18446744073709551615UL,0x37C8FFADL},{4UL,0x8FE64973L,4UL,18446744073709551615UL},{5UL,0xEEE3B5BFL,0x4A264581L,5UL},{0x299FA847L,18446744073709551615UL,1UL,0xEEE3B5BFL}},{{18446744073709551615UL,0x8FE64973L,1UL,1UL},{0x299FA847L,0x299FA847L,0x4A264581L,0x37C8FFADL},{5UL,18446744073709551615UL,4UL,0xEEE3B5BFL},{4UL,0xEEE3B5BFL,18446744073709551615UL,4UL},{0x299FA847L,0xEEE3B5BFL,0UL,0xEEE3B5BFL},{0xEEE3B5BFL,18446744073709551615UL,1UL,0x37C8FFADL}},{{18446744073709551615UL,0x299FA847L,18446744073709551615UL,1UL},{5UL,0x8FE64973L,0xE1515F3BL,0xEEE3B5BFL},{5UL,18446744073709551615UL,18446744073709551615UL,5UL},{18446744073709551615UL,0xEEE3B5BFL,1UL,18446744073709551615UL},{0xEEE3B5BFL,0x8FE64973L,0UL,0x37C8FFADL},{18446744073709551615UL,0x4A264581L,0xE1515F3BL,18446744073709551615UL}},{{0UL,18446744073709551615UL,0UL,0xE1515F3BL},{1UL,4UL,5UL,1UL},{18446744073709551615UL,0xE1515F3BL,0xF13BA174L,4UL},{0xE1515F3BL,18446744073709551615UL,0xF13BA174L,0xF13BA174L},{18446744073709551615UL,18446744073709551615UL,5UL,18446744073709551615UL},{1UL,18446744073709551607UL,0UL,4UL}}};
static const uint32_t g_1076 = 0x49121A36L;
static const uint32_t *g_1075 = &g_1076;
static const uint32_t **g_1074 = &g_1075;
static int32_t g_1139 = (-9L);
static volatile int64_t g_1159 = 0x43CDD027EC15D283LL;/* VOLATILE GLOBAL g_1159 */
static int64_t *g_1175 = (void*)0;
static int64_t * const * volatile g_1174[1] = {&g_1175};
static uint8_t g_1202 = 1UL;
static float *g_1256 = &g_291;
static int32_t ** volatile g_1260 = &g_293;/* VOLATILE GLOBAL g_1260 */
static int32_t ** volatile g_1368 = &g_293;/* VOLATILE GLOBAL g_1368 */
static const volatile uint8_t * volatile *g_1388 = (void*)0;
static const volatile uint8_t * volatile **g_1387 = &g_1388;
static const volatile uint8_t * volatile ** volatile * const  volatile g_1386 = &g_1387;/* VOLATILE GLOBAL g_1386 */
static const volatile uint8_t * volatile ** volatile * const  volatile *g_1385[7][4][9] = {{{&g_1386,&g_1386,(void*)0,&g_1386,(void*)0,&g_1386,&g_1386,(void*)0,&g_1386},{(void*)0,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,&g_1386,(void*)0},{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0},{(void*)0,(void*)0,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386}},{{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386},{(void*)0,&g_1386,&g_1386,(void*)0,&g_1386,(void*)0,&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,(void*)0,(void*)0,&g_1386},{&g_1386,(void*)0,&g_1386,&g_1386,&g_1386,(void*)0,(void*)0,&g_1386,(void*)0}},{{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386},{&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,(void*)0},{&g_1386,&g_1386,(void*)0,(void*)0,&g_1386,&g_1386,(void*)0,(void*)0,&g_1386}},{{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,(void*)0},{&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,(void*)0,&g_1386,&g_1386,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,(void*)0,&g_1386,&g_1386,(void*)0}},{{&g_1386,&g_1386,(void*)0,(void*)0,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,&g_1386},{(void*)0,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,(void*)0,&g_1386}},{{&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386,&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386},{&g_1386,(void*)0,&g_1386,&g_1386,(void*)0,(void*)0,(void*)0,&g_1386,&g_1386},{&g_1386,(void*)0,&g_1386,(void*)0,&g_1386,(void*)0,&g_1386,(void*)0,&g_1386}},{{&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,(void*)0,&g_1386,(void*)0,&g_1386},{&g_1386,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386,&g_1386,(void*)0},{&g_1386,(void*)0,(void*)0,&g_1386,&g_1386,&g_1386,(void*)0,&g_1386,&g_1386},{(void*)0,&g_1386,&g_1386,(void*)0,(void*)0,&g_1386,&g_1386,&g_1386,&g_1386}}};
static float g_1439 = 0x0.4p-1;
static int32_t g_1470 = 0x9F40DD23L;
static volatile int32_t g_1475 = (-1L);/* VOLATILE GLOBAL g_1475 */
static const float g_1494 = 0x0.1p+1;
static const float g_1508 = (-0x1.7p+1);
static const float *g_1507 = &g_1508;
static const uint8_t *g_1538[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const uint8_t **g_1537 = &g_1538[4];
static const uint8_t ***g_1536 = &g_1537;
static const uint8_t ****g_1535 = &g_1536;
static uint64_t * volatile g_1572 = &g_902;/* VOLATILE GLOBAL g_1572 */
static uint64_t * volatile *g_1571 = &g_1572;
static uint64_t * volatile **g_1570 = &g_1571;
static uint64_t g_1636 = 0x626CCC4215F04FCFLL;
static int64_t g_1682[8][9] = {{(-1L),0xF3AA7004933C98E7LL,0x92AEE9FC967336A7LL,(-1L),0xC1A6A4C11A129D98LL,0xC1A6A4C11A129D98LL,(-1L),0x92AEE9FC967336A7LL,0xF3AA7004933C98E7LL},{(-5L),0x92AEE9FC967336A7LL,0x3A8D897ADFFF0D96LL,0xDDC1E984DB7AB786LL,1L,1L,0L,1L,0L},{(-5L),(-8L),1L,0L,0xE5B4F39783F07882LL,0x3A8D897ADFFF0D96LL,0x7F558A7A8A37B344LL,0x0D2E00D1D35CB916LL,0x3A8D897ADFFF0D96LL},{(-1L),0x92AEE9FC967336A7LL,1L,0L,0x3A8D897ADFFF0D96LL,0L,0x90A629E9BCBE77FALL,1L,1L},{0L,0xF3AA7004933C98E7LL,0x3A8D897ADFFF0D96LL,0L,0x3A8D897ADFFF0D96LL,0xF3AA7004933C98E7LL,0L,0xE5B4F39783F07882LL,0xC1A6A4C11A129D98LL},{0x7F558A7A8A37B344LL,0xC1A6A4C11A129D98LL,0x92AEE9FC967336A7LL,0xDDC1E984DB7AB786LL,0xE5B4F39783F07882LL,0L,0L,0xC1A6A4C11A129D98LL,0x0D2E00D1D35CB916LL},{(-2L),0L,0x0D2E00D1D35CB916LL,(-1L),1L,0x3A8D897ADFFF0D96LL,0x90A629E9BCBE77FALL,0xE5B4F39783F07882LL,0x0D2E00D1D35CB916LL},{0x90A629E9BCBE77FALL,1L,0xF3AA7004933C98E7LL,(-5L),0xC1A6A4C11A129D98LL,1L,0x7F558A7A8A37B344LL,1L,0xC1A6A4C11A129D98LL}};
static float g_1687 = 0x9.5p+1;
static float * volatile g_1724 = (void*)0;/* VOLATILE GLOBAL g_1724 */
static uint32_t g_1731 = 4294967290UL;
static int32_t g_1743[6] = {0x94511950L,0x32376C40L,0x32376C40L,0x94511950L,0x32376C40L,0x32376C40L};
static int8_t g_1965[4] = {0x73L,0x73L,0x73L,0x73L};
static uint8_t g_2008 = 0xDBL;
static int32_t ** volatile g_2013 = &g_293;/* VOLATILE GLOBAL g_2013 */
static int32_t ** volatile g_2023 = &g_293;/* VOLATILE GLOBAL g_2023 */
static uint16_t g_2108 = 0UL;
static volatile int8_t g_2184 = 0x18L;/* VOLATILE GLOBAL g_2184 */
static int32_t ** volatile g_2190[2] = {(void*)0,(void*)0};
static int32_t ** volatile g_2191 = &g_293;/* VOLATILE GLOBAL g_2191 */
static uint32_t g_2235 = 0xBA36B31EL;
static const volatile uint64_t g_2344 = 18446744073709551608UL;/* VOLATILE GLOBAL g_2344 */
static uint8_t g_2376 = 1UL;
static volatile int32_t g_2441 = 1L;/* VOLATILE GLOBAL g_2441 */
static uint32_t g_2442[10] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
static const uint64_t *g_2459 = &g_143;
static const uint64_t **g_2458 = &g_2459;
static struct S0 g_2492 = {-33741,0xF03B7372L};
static struct S0 * const  volatile g_2493 = &g_2492;/* VOLATILE GLOBAL g_2493 */
static volatile uint16_t g_2599 = 0xC550L;/* VOLATILE GLOBAL g_2599 */
static uint32_t g_2600 = 1UL;
static uint32_t *g_2648 = &g_1027[0][3][0];
static volatile float g_2672 = (-0x10.4p-1);/* VOLATILE GLOBAL g_2672 */
static volatile float * volatile g_2671 = &g_2672;/* VOLATILE GLOBAL g_2671 */
static volatile float * volatile *g_2670[3][1] = {{&g_2671},{&g_2671},{&g_2671}};
static uint32_t g_2758 = 0x79A6DBF7L;


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static struct S0  func_5(uint16_t  p_6, uint32_t  p_7);
static struct S0  func_8(int64_t  p_9);
static uint64_t  func_14(uint32_t  p_15);
static const int32_t  func_18(int64_t  p_19, uint64_t  p_20, int32_t  p_21, float  p_22);
static float  func_28(const struct S0  p_29, uint8_t * p_30, int64_t  p_31, int16_t  p_32, uint16_t  p_33);
static uint16_t  func_37(uint8_t * p_38);
static uint8_t * func_39(int64_t  p_40, uint8_t * p_41);
static uint8_t * func_42(uint8_t * p_43, const int16_t  p_44);
static uint8_t * func_45(const uint16_t  p_46, uint8_t * p_47);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_1202 g_2013 g_293 g_2023 g_186 g_185 g_54 g_995 g_484 g_1571 g_1572 g_902 g_285 g_68 g_743 g_739 g_1965 g_51 g_823 g_204 g_899 g_801 g_480 g_1470 g_1027 g_2108 g_1000 g_1507 g_1508 g_863 g_291 g_203 g_86 g_165 g_77 g_36 g_1256 g_11 g_822 g_2184 g_2191 g_1682 g_255 g_847 g_162 g_809 g_936 g_2344 g_228 g_363 g_196 g_2376 g_1139 g_55 g_1074 g_1075 g_1076 g_2441 g_1743 g_50 g_2442 g_2458 g_2459 g_143 g_832 g_829 g_830 g_76 g_2492 g_2493 g_61.f0 g_994 g_290 g_190 g_1570 g_1731 g_144 g_2599 g_2600 g_1636 g_311 g_883 g_2008 g_166 g_335 g_2671 g_2672 g_828 g_2758
 * writes: g_2 g_1202 g_293 g_68 g_285 g_1139 g_739 g_51 g_1470 g_86 g_899 g_1682 g_54 g_2108 g_1000 g_1687 g_1439 g_291 g_1636 g_883 g_335 g_994 g_36 g_61.f1 g_255 g_162 g_809 g_936 g_76 g_937 g_228 g_902 g_2376 g_1731 g_847 g_2492 g_143 g_77 g_2648 g_165.f0 g_165 g_2670 g_289
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int16_t l_2028 = 0x0ABBL;
    const int8_t *l_2034 = (void*)0;
    struct S0 l_2401 = {36623,0x8E3318F5L};
    float **l_2433 = &g_1256;
    int32_t l_2435[3][10][2] = {{{0x475B19B6L,(-1L)},{(-1L),0xB399706BL},{(-1L),(-1L)},{0x475B19B6L,0xB399706BL},{0x475B19B6L,(-1L)},{(-1L),0xB399706BL},{(-1L),(-1L)},{0x475B19B6L,0xB399706BL},{0x475B19B6L,(-1L)},{(-1L),0xB399706BL}},{{(-1L),(-1L)},{0x475B19B6L,0xB399706BL},{0x475B19B6L,(-1L)},{(-1L),0xB399706BL},{(-1L),(-1L)},{0x475B19B6L,0xB399706BL},{0x475B19B6L,(-1L)},{(-1L),0xB399706BL},{(-1L),(-1L)},{0x475B19B6L,0xB399706BL}},{{0x475B19B6L,(-1L)},{(-1L),0xB399706BL},{(-1L),(-1L)},{0x475B19B6L,0xB399706BL},{0x475B19B6L,(-1L)},{(-1L),0xB399706BL},{(-1L),(-1L)},{0x475B19B6L,0xB399706BL},{0x475B19B6L,(-1L)},{(-1L),0xB399706BL}}};
    int16_t l_2444[7][8][4] = {{{(-1L),(-1L),0x30C1L,(-1L)},{0x19D0L,(-1L),6L,(-1L)},{0x19D0L,0x30C1L,0x30C1L,0x19D0L},{(-1L),(-1L),(-1L),0x30C1L},{(-1L),(-1L),0xBA1EL,9L},{(-1L),(-1L),0x30C1L,9L},{0xE3B7L,(-1L),0xE3B7L,0x30C1L},{0x19D0L,(-1L),0x105EL,0x19D0L}},{{(-1L),0x30C1L,(-1L),(-1L)},{0x30C1L,(-1L),(-1L),(-1L)},{(-1L),(-1L),0x105EL,9L},{0x19D0L,(-9L),0xE3B7L,(-1L)},{0xE3B7L,(-1L),0x30C1L,0xE3B7L},{(-1L),(-1L),0xBA1EL,(-1L)},{(-1L),(-9L),(-1L),9L},{(-1L),(-1L),0x30C1L,(-1L)}},{{0x19D0L,(-1L),6L,(-1L)},{0x19D0L,0x30C1L,0x30C1L,0x19D0L},{(-1L),(-1L),(-1L),0x30C1L},{(-1L),(-1L),0xBA1EL,9L},{(-1L),(-1L),0x30C1L,9L},{0xE3B7L,(-1L),0xE3B7L,0x30C1L},{0x19D0L,(-1L),0x105EL,0x19D0L},{(-1L),0x30C1L,(-1L),(-1L)}},{{0x30C1L,(-1L),(-1L),(-1L)},{(-1L),(-1L),0x105EL,9L},{0x19D0L,(-9L),0xE3B7L,(-1L)},{0xE3B7L,(-1L),0x30C1L,0xE3B7L},{(-1L),(-1L),0xBA1EL,(-1L)},{(-1L),(-9L),(-1L),9L},{(-1L),(-1L),0x30C1L,(-1L)},{0x19D0L,(-1L),6L,0xE3B7L}},{{(-1L),6L,6L,(-1L)},{0x105EL,0xE3B7L,1L,6L},{0xE3B7L,(-1L),(-1L),(-9L)},{0x30C1L,0x105EL,6L,(-9L)},{0xBA1EL,(-1L),0xBA1EL,6L},{(-1L),0xE3B7L,0x19D0L,(-1L)},{0x30C1L,6L,1L,0xE3B7L},{6L,(-1L),1L,1L}},{{0x30C1L,0x30C1L,0x19D0L,(-9L)},{(-1L),0L,0xBA1EL,0xE3B7L},{0xBA1EL,0xE3B7L,6L,0xBA1EL},{0x30C1L,0xE3B7L,(-1L),0xE3B7L},{0xE3B7L,0L,1L,(-9L)},{0x105EL,0x30C1L,6L,1L},{(-1L),(-1L),9L,0xE3B7L},{(-1L),6L,6L,(-1L)}},{{0x105EL,0xE3B7L,1L,6L},{0xE3B7L,(-1L),(-1L),(-9L)},{0x30C1L,0x105EL,6L,(-9L)},{0xBA1EL,(-1L),0xBA1EL,6L},{(-1L),0xE3B7L,0x19D0L,(-1L)},{0x30C1L,6L,1L,0xE3B7L},{6L,(-1L),1L,1L},{0x30C1L,0x30C1L,0x19D0L,(-9L)}}};
    int8_t l_2457[2][4][2] = {{{0L,0xFCL},{0L,0L},{0xFCL,0L},{0L,0xFCL}},{{0L,0L},{0xFCL,0L},{0L,0xFCL},{0L,0L}}};
    uint64_t l_2464 = 0xBE835FC95CD86355LL;
    uint8_t l_2489 = 253UL;
    int32_t **l_2497[7];
    int32_t ** const *l_2496 = &l_2497[2];
    int16_t l_2535 = 0x0B18L;
    struct S0 ***l_2548 = &g_204;
    uint32_t l_2576 = 9UL;
    struct S0 l_2581 = {-38207,-10L};
    uint8_t *l_2582[6][5] = {{&g_2376,&g_25,&g_36,&g_2376,&g_2376},{(void*)0,&g_76,(void*)0,(void*)0,&g_25},{&g_25,&g_2376,&g_25,(void*)0,(void*)0},{&g_25,&g_36,&g_2376,&g_2376,&g_36},{&g_36,&g_1202,&g_25,(void*)0,&g_76},{&g_76,&g_1202,(void*)0,(void*)0,&g_36}};
    int8_t l_2587 = 0xE0L;
    int32_t l_2594[3][9][1] = {{{6L},{1L},{6L},{(-1L)},{6L},{1L},{6L},{(-1L)},{6L}},{{1L},{6L},{(-1L)},{6L},{1L},{6L},{(-1L)},{6L},{1L}},{{6L},{(-1L)},{6L},{1L},{6L},{(-1L)},{6L},{1L},{6L}}};
    int16_t *l_2595[3];
    uint8_t l_2596 = 3UL;
    const uint32_t l_2597 = 18446744073709551613UL;
    uint32_t l_2598 = 9UL;
    int8_t *l_2601 = &g_883;
    uint8_t l_2602 = 255UL;
    uint64_t *l_2603 = &g_902;
    uint32_t l_2604[2];
    const int32_t l_2667 = 0L;
    uint64_t l_2736 = 6UL;
    const uint64_t l_2750 = 18446744073709551614UL;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_2497[i] = &g_293;
    for (i = 0; i < 3; i++)
        l_2595[i] = &g_162;
    for (i = 0; i < 2; i++)
        l_2604[i] = 1UL;
    for (g_2 = 11; (g_2 != (-17)); g_2 = safe_sub_func_uint16_t_u_u(g_2, 7))
    { /* block id: 3 */
        int8_t l_10[6][10] = {{0x54L,(-6L),0x9DL,0x9DL,0x9DL,(-6L),0x54L,0x89L,1L,1L},{0x54L,1L,(-4L),0L,0L,(-4L),1L,0x54L,0xE9L,0x89L},{(-4L),0x9DL,0xB0L,(-6L),1L,(-6L),0xB0L,0x9DL,0L,0xE9L},{(-4L),(-10L),0xB0L,1L,0x9DL,0x9DL,1L,0xB0L,(-10L),(-4L)},{(-10L),(-6L),0L,1L,0x9DL,(-4L),0x9DL,1L,0L,(-6L)},{0xE9L,0xB0L,(-4L),(-6L),0x9DL,0x54L,0x54L,0x9DL,(-6L),(-4L)}};
        struct S0 l_2029 = {21726,0x04CF9590L};
        int i, j;
        l_2401 = func_5((((func_8(l_10[0][7]) , ((safe_rshift_func_uint16_t_u_u(l_2028, (l_2029 , (safe_add_func_int16_t_s_s(g_995, ((((*g_484) = 0UL) == ((l_2029.f0 , &l_10[0][3]) == ((safe_mod_func_uint64_t_u_u(0xBB4F42A52CA6DB87LL, (**g_1571))) , l_2034))) != l_10[5][8])))))) == l_2029.f0)) ^ l_2028) , l_2028), l_2028);
        if (l_2029.f0)
            break;
        return l_10[0][7];
    }
    for (g_2376 = 0; (g_2376 > 51); g_2376 = safe_add_func_uint8_t_u_u(g_2376, 7))
    { /* block id: 1155 */
        int8_t *l_2421[3][7][6] = {{{&g_883,&g_883,&g_883,&g_1965[1],(void*)0,&g_883},{&g_883,&g_883,(void*)0,&g_883,&g_255,&g_1965[1]},{&g_255,(void*)0,&g_255,(void*)0,&g_255,(void*)0},{&g_255,&g_1965[1],&g_255,&g_1965[2],&g_883,&g_1965[1]},{&g_883,&g_1965[2],(void*)0,&g_883,(void*)0,&g_883},{&g_883,(void*)0,&g_883,&g_883,&g_1965[1],&g_1965[2]},{&g_883,(void*)0,&g_883,&g_1965[2],(void*)0,&g_255}},{{&g_255,&g_1965[1],&g_1965[1],(void*)0,(void*)0,(void*)0},{&g_255,(void*)0,&g_255,&g_883,&g_1965[1],&g_883},{&g_883,(void*)0,&g_1965[1],&g_1965[1],(void*)0,&g_883},{&g_883,&g_1965[2],&g_255,&g_255,&g_883,(void*)0},{&g_883,&g_1965[1],&g_1965[1],&g_883,&g_255,&g_255},{&g_883,(void*)0,&g_883,&g_255,&g_255,&g_1965[2]},{&g_883,&g_883,&g_883,&g_1965[1],(void*)0,&g_883}},{{&g_883,&g_883,(void*)0,&g_883,&g_255,&g_1965[1]},{&g_255,(void*)0,&g_255,(void*)0,&g_255,(void*)0},{&g_255,&g_1965[1],&g_255,&g_1965[2],&g_883,&g_1965[1]},{&g_883,&g_1965[2],(void*)0,&g_883,(void*)0,&g_883},{&g_883,(void*)0,&g_883,&g_883,&g_1965[1],&g_1965[2]},{&g_883,(void*)0,&g_883,&g_1965[2],(void*)0,&g_255},{&g_255,&g_1965[1],&g_1965[1],(void*)0,(void*)0,(void*)0}}};
        int32_t l_2422 = 0x3F009AFDL;
        int32_t l_2423[6];
        uint32_t *l_2424 = (void*)0;
        uint32_t *l_2425 = (void*)0;
        uint32_t *l_2426 = &g_1731;
        int32_t l_2434[5][9] = {{6L,0xC01E435DL,0xC01E435DL,6L,(-6L),6L,0xC01E435DL,0xC01E435DL,6L},{(-1L),0x1F3DE264L,0x0BD5225CL,0x1F3DE264L,(-1L),(-1L),0x1F3DE264L,0x0BD5225CL,0x1F3DE264L},{0xC01E435DL,(-6L),8L,8L,(-6L),0xC01E435DL,(-6L),(-6L),(-6L)},{0x1F3DE264L,0x1F3DE264L,1L,0L,1L,0x1F3DE264L,0x1F3DE264L,1L,0L},{8L,(-2L),8L,6L,6L,8L,(-2L),8L,6L}};
        float **l_2436 = &g_1256;
        uint16_t *l_2437 = (void*)0;
        uint16_t *l_2438 = &g_335[0][3][0];
        int32_t *l_2439 = &g_1470;
        float l_2440 = (-0x9.Bp+1);
        uint16_t *l_2443 = &g_994;
        uint64_t * const l_2452 = &g_902;
        uint64_t * const *l_2451 = &l_2452;
        uint64_t * const **l_2450 = &l_2451;
        uint64_t * const ***l_2449 = &l_2450;
        const uint64_t **l_2460 = &g_2459;
        struct S0 l_2466[7] = {{3481,1L},{-25448,-1L},{3481,1L},{3481,1L},{-25448,-1L},{3481,1L},{3481,1L}};
        int32_t *l_2562 = &l_2422;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_2423[i] = (-8L);
        if (((!(safe_add_func_uint16_t_u_u(((*l_2443) = ((((safe_sub_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_u(((((safe_mod_func_int32_t_s_s(((safe_div_func_int8_t_s_s((((safe_add_func_uint64_t_u_u((((-9L) && ((*l_2439) |= (((*g_480) = (safe_lshift_func_int16_t_s_s((g_1139 , g_36), 0))) | (safe_rshift_func_uint16_t_u_s(((*l_2438) = (((l_2422 = (-6L)) < l_2028) != (((g_1202 = (((*l_2426) = l_2423[4]) & (((l_2434[4][2] = (safe_mul_func_int8_t_s_s(((-6L) & g_55), (safe_div_func_int32_t_s_s((safe_add_func_uint64_t_u_u(((&g_1507 == l_2433) <= l_2423[4]), 5L)), (**g_1074)))))) && l_2434[4][2]) | l_2435[1][2][0]))) , l_2436) == l_2433))), l_2423[4]))))) == l_2423[5]), l_2401.f1)) , (*g_1075)) || (**g_1074)), g_2441)) || 0xCA2E8FD7EA17F105LL), 4294967295UL)) <= g_1743[3]) , (*l_2439)) , 255UL), l_2435[1][1][1])) | l_2435[0][8][1]), 9UL)) && g_50[5][3][0]) | 0xC47878FDL) != g_2442[8])), g_2108))) | l_2444[2][0][0]))
        { /* block id: 1164 */
            return g_55;
        }
        else
        { /* block id: 1166 */
            uint64_t l_2456 = 0x3D97945B4F08F818LL;
            int32_t l_2461 = 0x41265D29L;
            struct S0 l_2503 = {-10891,8L};
            int64_t *l_2517 = (void*)0;
            int32_t l_2518 = 0xA3DB543AL;
            uint32_t l_2534 = 0x7051FD3DL;
            uint32_t ***l_2538 = &g_739;
            struct S0 ***l_2546 = &g_204;
            uint64_t l_2564 = 0UL;
            (*g_185) ^= (*l_2439);
            if (((*l_2439) <= (((((((((*l_2439) != ((*g_77) = 255UL)) >= (safe_lshift_func_int16_t_s_u(((((l_2449 != (((l_2444[2][0][0] >= (((((((safe_unary_minus_func_int32_t_s((safe_div_func_uint16_t_u_u(l_2456, l_2457[1][2][0])))) , (**l_2449)) != (l_2460 = g_2458)) == l_2457[1][1][0]) , g_51[1][0][8]) , (*g_2459)) | 0xAFEEAECD31ADC18CLL)) | 0x86D8L) , &l_2450)) && l_2457[1][2][0]) >= l_2401.f0) | 0x3206B796L), l_2401.f1))) , (-7L)) , (void*)0) == (**g_832)) || l_2461) && l_2456) != 0x2E39L)))
            { /* block id: 1170 */
                int64_t l_2465[1];
                int32_t l_2471 = 0xFA3EC8E2L;
                int32_t l_2482 = (-5L);
                int i;
                for (i = 0; i < 1; i++)
                    l_2465[i] = 0xB37FD45FC42187B3LL;
                if ((((safe_rshift_func_int8_t_s_u((l_2464 = 0x95L), 1)) , (void*)0) == (l_2466[2] , (**g_823))))
                { /* block id: 1172 */
                    int16_t *l_2479 = &g_847[0][3][5];
                    int64_t *l_2480 = (void*)0;
                    int64_t *l_2481 = &l_2465[0];
                    int32_t l_2488 = 1L;
                    uint32_t ***l_2536 = &g_739;
                    (*l_2439) ^= 1L;
                    if ((safe_mod_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((l_2471 = 0xE0L), (2UL <= 1UL))) , (((*l_2481) |= (safe_mul_func_int16_t_s_s((((safe_lshift_func_int16_t_s_s(((*l_2479) = ((safe_div_func_int32_t_s_s(l_2435[1][2][0], (0UL || (*l_2439)))) <= ((+(*l_2439)) | (0x18378F94L || l_2471)))), g_76)) && l_2461) ^ l_2461), 0xDD8DL))) > 1UL)), l_2482)))
                    { /* block id: 1177 */
                        return (*l_2439);
                    }
                    else
                    { /* block id: 1179 */
                        int32_t *l_2483 = &g_55;
                        int32_t *l_2484 = &g_144[2][4][0];
                        int32_t *l_2485 = &g_51[1][0][8];
                        int32_t *l_2486 = &l_2461;
                        int32_t *l_2487[1][10][5] = {{{&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8]},{&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1]},{&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8]},{&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1]},{&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8]},{&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1]},{&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8]},{&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1]},{&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8],&g_51[3][0][8]},{&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1],&l_2434[3][1]}}};
                        int32_t ** const l_2495 = &l_2483;
                        int32_t ** const * const l_2494 = &l_2495;
                        int i, j, k;
                        l_2489--;
                        if (l_2488)
                            break;
                        (*g_2493) = g_2492;
                        l_2496 = l_2494;
                    }
                    if (((*l_2439) = (l_2465[0] > (*l_2439))))
                    { /* block id: 1186 */
                        uint64_t l_2498 = 0UL;
                        uint64_t *l_2506 = &g_143;
                        if (l_2498)
                            break;
                        l_2488 ^= (safe_sub_func_int32_t_s_s((*g_185), ((safe_div_func_uint16_t_u_u(((*l_2443) |= (&g_1074 != (((l_2503 , (++(*l_2452))) != ((g_162 >= (--(*l_2506))) == (g_2492.f0 &= ((l_2465[0] , (((((safe_lshift_func_uint8_t_u_u((*g_77), 1)) && (*l_2439)) != (safe_mod_func_uint64_t_u_u((safe_sub_func_int16_t_s_s((safe_div_func_uint32_t_u_u(((l_2517 == &g_285) ^ g_61.f0), l_2498)), l_2518)), 0xE1BC562197281257LL))) <= (*g_77)) | l_2498)) | l_2498)))) , (void*)0))), g_165.f0)) != l_2498)));
                    }
                    else
                    { /* block id: 1193 */
                        uint32_t ****l_2537[1][9];
                        int32_t l_2539 = 0L;
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 9; j++)
                                l_2537[i][j] = &l_2536;
                        }
                        l_2471 = (((safe_add_func_float_f_f((-(safe_div_func_float_f_f((safe_mul_func_float_f_f((((safe_div_func_float_f_f(l_2488, ((safe_div_func_float_f_f(((l_2538 = ((*l_2439) , ((safe_add_func_uint16_t_u_u(((&g_1174[0] != (void*)0) ^ (((safe_sub_func_float_f_f((l_2518 , 0x0.3p-1), ((*g_1256) == (((&l_2488 == (void*)0) != (*g_1256)) > l_2534)))) != 0x7.758DB4p+45) , l_2535)), l_2534)) , l_2536))) != (void*)0), l_2488)) != (*g_290)))) < (*l_2439)) <= 0x0.9E0910p+74), 0xD.A5E22Ep+18)), (*g_1507)))), (*g_1507))) , l_2488) < l_2539);
                        l_2488 |= (*l_2439);
                    }
                    for (g_162 = 0; (g_162 > (-18)); g_162 = safe_sub_func_uint32_t_u_u(g_162, 5))
                    { /* block id: 1200 */
                        if ((*l_2439))
                            break;
                    }
                }
                else
                { /* block id: 1203 */
                    struct S0 l_2547 = {-13386,0x37907B84L};
                    int32_t l_2558 = (-2L);
                    int8_t l_2559 = 0x14L;
                    for (g_255 = (-16); (g_255 != 19); g_255 = safe_add_func_int16_t_s_s(g_255, 7))
                    { /* block id: 1206 */
                        l_2471 &= ((((0xB181D3445FC5A362LL ^ ((safe_sub_func_int32_t_s_s((l_2546 != (l_2547 , l_2548)), (((safe_rshift_func_uint8_t_u_s(255UL, ((**g_1074) > (safe_add_func_uint16_t_u_u((((l_2558 = ((((safe_div_func_uint16_t_u_u(l_2547.f0, (safe_sub_func_uint64_t_u_u((~((g_190 >= (0x5CL != l_2482)) > g_1027[0][3][0])), 0x7AC57EBF05526341LL)))) != 0xE9524CBF0AFFAF91LL) && 65535UL) , 0x995EL)) || l_2559) == 1UL), 0x4F20L))))) , 0xC695C2E2L) , 4L))) || 9L)) | l_2461) || (*l_2439)) || l_2518);
                    }
                    (*l_2439) &= (safe_sub_func_uint32_t_u_u(((*g_480) = ((((**g_823) = (void*)0) != (void*)0) < 0L)), ((*g_2458) == (**g_1570))));
                }
                l_2562 = &l_2518;
            }
            else
            { /* block id: 1215 */
                uint32_t l_2563 = 0x78EB1019L;
                return l_2563;
            }
            return l_2564;
        }
    }
    for (g_1731 = 6; (g_1731 != 6); g_1731 = safe_add_func_int32_t_s_s(g_1731, 8))
    { /* block id: 1223 */
        int64_t l_2567 = 9L;
        return l_2567;
    }
    if (((((((*l_2603) |= (safe_mul_func_int16_t_s_s((((safe_div_func_float_f_f(((*g_1256) = ((safe_div_func_int64_t_s_s((l_2576 ^ (safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(g_144[6][5][0], ((*l_2601) = (0UL & ((((l_2581 , (g_77 = l_2582[5][4])) == ((safe_div_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(((((((*g_480) = (l_2587 == (safe_rshift_func_uint16_t_u_s(((safe_mod_func_uint32_t_u_u(((l_2596 ^= (((safe_mul_func_uint16_t_u_u(l_2594[1][6][0], (g_162 = g_809))) , &l_2028) != (void*)0)) < 1UL), l_2597)) && l_2598), 4)))) || 0x215C68B9L) >= 1UL) , g_143) , g_2599), g_144[2][4][0])), (**g_2458))) , &l_2596)) != g_1743[3]) , g_2600))))), l_2602))), g_847[0][2][1])) , (*g_290))), 0x1.4A73AEp-72)) , (*g_290)) , 0L), 65529UL))) == g_11[2][4]) >= 0x245297091C26FDCELL) > l_2604[0]) || (*g_185)))
    { /* block id: 1233 */
        int32_t l_2605 = 5L;
        int32_t l_2613[6];
        int32_t l_2622[6] = {0xE0358EF5L,0xE0358EF5L,0xE0358EF5L,0xE0358EF5L,0xE0358EF5L,0xE0358EF5L};
        int8_t l_2623 = 0x82L;
        int16_t l_2627 = 0xADCCL;
        uint32_t *l_2647 = &g_2600;
        int32_t l_2649 = 0L;
        int i;
        for (i = 0; i < 6; i++)
            l_2613[i] = (-6L);
        (*g_185) = ((l_2605 | (safe_mul_func_uint8_t_u_u(l_2605, (safe_lshift_func_int8_t_s_s(g_1965[1], (safe_unary_minus_func_int32_t_s((safe_sub_func_int16_t_s_s((((l_2613[0] = l_2605) ^ (safe_add_func_int16_t_s_s(g_1470, (l_2605 < g_228[4])))) ^ (65534UL | ((l_2622[5] = ((safe_add_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((((safe_div_func_int8_t_s_s(g_2344, g_2108)) , g_1636) >= l_2605), l_2605)), l_2605)) != l_2605)) <= l_2605))), 0x0085L))))))))) == l_2623);
        if ((g_1743[3] , (safe_lshift_func_int8_t_s_s(((*l_2601) = 0L), 5))))
        { /* block id: 1238 */
            int32_t *l_2626 = &g_165.f1;
            (*g_311) = l_2626;
            return l_2627;
        }
        else
        { /* block id: 1241 */
            int32_t l_2634 = 0x84E76D32L;
            const int64_t l_2650 = 5L;
            (*g_185) = (safe_add_func_int8_t_s_s(((*l_2601) |= 0L), (safe_mod_func_uint64_t_u_u((**g_1571), ((safe_sub_func_uint8_t_u_u((++g_76), ((safe_rshift_func_int8_t_s_s((((safe_add_func_int32_t_s_s((((void*)0 == &g_1175) & 1UL), (safe_sub_func_int32_t_s_s(((safe_lshift_func_int16_t_s_u(g_994, 5)) != (*g_2459)), (g_51[1][0][8] |= (((((((safe_sub_func_uint32_t_u_u((((g_165.f0 = (g_144[3][2][0] & ((g_2648 = l_2647) != (void*)0))) > g_2492.f0) , l_2649), 0L)) && (**g_2458)) <= (**g_1074)) > g_68) | l_2634) & g_2008) ^ 6L)))))) != 18446744073709551615UL) > l_2622[1]), l_2650)) && l_2627))) || 0L)))));
            return g_2600;
        }
    }
    else
    { /* block id: 1250 */
        uint64_t l_2653[3][9][9] = {{{0UL,18446744073709551614UL,18446744073709551607UL,0x56FA021A320B2A64LL,0x74E384A7072B3804LL,18446744073709551614UL,0xCE6137154276A039LL,0x77BC1F5CC90C8A6ALL,0UL},{0x29AF865F43948529LL,0x075BA8F57616D79FLL,8UL,0UL,2UL,0x3F5A7C87E8C1D81ELL,0x47C10900F25AF5ECLL,0xA348EDD63A9674E6LL,18446744073709551614UL},{0UL,0x74E384A7072B3804LL,0xCE6137154276A039LL,2UL,0UL,18446744073709551615UL,0xCDFDD124FA087A71LL,0x5E8D5DDCD62164C4LL,0x74E384A7072B3804LL},{0x5DDB9512D51CD737LL,18446744073709551615UL,0x3F5A7C87E8C1D81ELL,0UL,0UL,2UL,0x77BC1F5CC90C8A6ALL,0x5DDB9512D51CD737LL,0xCE6137154276A039LL},{18446744073709551614UL,0xB603F3C3961DD22BLL,0x3F5A7C87E8C1D81ELL,0xCDFDD124FA087A71LL,0x0958EB19117DBEC2LL,0x075BA8F57616D79FLL,0UL,0x075BA8F57616D79FLL,0x0958EB19117DBEC2LL},{0x075BA8F57616D79FLL,0xCE6137154276A039LL,0xCE6137154276A039LL,0x075BA8F57616D79FLL,0x5DDB9512D51CD737LL,0xA348EDD63A9674E6LL,0x0F5001292DBCA262LL,0UL,0x5E8D5DDCD62164C4LL},{0x5730F7C476644420LL,0x5DDB9512D51CD737LL,8UL,0xB603F3C3961DD22BLL,0x35C11D04A187AB88LL,0x0958EB19117DBEC2LL,6UL,0UL,18446744073709551607UL},{0xB336C644D7FA12E5LL,0xD1E525542039AD51LL,18446744073709551607UL,0xD8C8BC25CE51257ALL,0x5DDB9512D51CD737LL,0UL,0UL,18446744073709551615UL,0x4200F0FD61B2CA3BLL},{0x29AF865F43948529LL,0x4200F0FD61B2CA3BLL,0xCDE8B219D0A205F3LL,0xDF3DD239411AE862LL,0x0958EB19117DBEC2LL,0x47C10900F25AF5ECLL,2UL,0xA348EDD63A9674E6LL,0x075BA8F57616D79FLL}},{{18446744073709551615UL,0x0F5001292DBCA262LL,0UL,18446744073709551615UL,0UL,18446744073709551615UL,2UL,18446744073709551615UL,0x0F5001292DBCA262LL},{18446744073709551607UL,0UL,0x88A823D6CC2BF1C4LL,0UL,0UL,0x88A823D6CC2BF1C4LL,0UL,18446744073709551607UL,18446744073709551615UL},{0xDF3DD239411AE862LL,0xB603F3C3961DD22BLL,1UL,0x47C10900F25AF5ECLL,2UL,18446744073709551614UL,6UL,18446744073709551614UL,0x0958EB19117DBEC2LL},{18446744073709551615UL,0xD8C8BC25CE51257ALL,2UL,0x77BC1F5CC90C8A6ALL,1UL,0x88A823D6CC2BF1C4LL,18446744073709551615UL,0xCE6137154276A039LL,0xB603F3C3961DD22BLL},{0x1F3C6DCF44BDB66ELL,0xBBA4D87F574A5AE5LL,0UL,0x3F5A7C87E8C1D81ELL,0xD1E525542039AD51LL,2UL,0x5DDB9512D51CD737LL,0x35C11D04A187AB88LL,18446744073709551615UL},{0x0958EB19117DBEC2LL,18446744073709551615UL,0xBBA4D87F574A5AE5LL,0x88A823D6CC2BF1C4LL,0x35C11D04A187AB88LL,0xB336C644D7FA12E5LL,0x56FA021A320B2A64LL,0xA348EDD63A9674E6LL,0UL},{18446744073709551615UL,18446744073709551615UL,0x74E384A7072B3804LL,18446744073709551615UL,0x3F5A7C87E8C1D81ELL,8UL,8UL,0x3F5A7C87E8C1D81ELL,18446744073709551615UL},{0x56FA021A320B2A64LL,0xBBA4D87F574A5AE5LL,0x56FA021A320B2A64LL,6UL,0x74E384A7072B3804LL,0x5C235BA34D76C80BLL,0xCDE8B219D0A205F3LL,0x0958EB19117DBEC2LL,0xBBA4D87F574A5AE5LL},{18446744073709551615UL,0xD8C8BC25CE51257ALL,0xCDE8B219D0A205F3LL,0x5DDB9512D51CD737LL,0x0F5001292DBCA262LL,0UL,0x0958EB19117DBEC2LL,18446744073709551615UL,2UL}},{{0x77BC1F5CC90C8A6ALL,1UL,0UL,6UL,18446744073709551615UL,18446744073709551615UL,0x5730F7C476644420LL,0xB336C644D7FA12E5LL,0xCDFDD124FA087A71LL},{18446744073709551615UL,0xA348EDD63A9674E6LL,0x29AF865F43948529LL,18446744073709551615UL,7UL,0x47C10900F25AF5ECLL,18446744073709551615UL,0x77BC1F5CC90C8A6ALL,0x0958EB19117DBEC2LL},{0x35C11D04A187AB88LL,18446744073709551615UL,18446744073709551607UL,0x88A823D6CC2BF1C4LL,0xD1E525542039AD51LL,0x47C10900F25AF5ECLL,0x5C235BA34D76C80BLL,1UL,1UL},{0xB603F3C3961DD22BLL,18446744073709551615UL,0xBBA4D87F574A5AE5LL,0x3F5A7C87E8C1D81ELL,0xBBA4D87F574A5AE5LL,18446744073709551615UL,0xB603F3C3961DD22BLL,0xD8C8BC25CE51257ALL,0x5E8D5DDCD62164C4LL},{18446744073709551615UL,0xCE6137154276A039LL,0x5DDB9512D51CD737LL,0x77BC1F5CC90C8A6ALL,2UL,0UL,0UL,0x3F5A7C87E8C1D81ELL,18446744073709551615UL},{2UL,0UL,0xB603F3C3961DD22BLL,0xCDE8B219D0A205F3LL,18446744073709551607UL,0x5C235BA34D76C80BLL,18446744073709551613UL,0xD8C8BC25CE51257ALL,0UL},{1UL,0x0958EB19117DBEC2LL,18446744073709551615UL,0x5DDB9512D51CD737LL,0x5C235BA34D76C80BLL,8UL,2UL,1UL,0x56FA021A320B2A64LL},{0xCE6137154276A039LL,1UL,8UL,0UL,0x47C10900F25AF5ECLL,0xB336C644D7FA12E5LL,0x0F5001292DBCA262LL,0x77BC1F5CC90C8A6ALL,0xCDFDD124FA087A71LL},{0xCE6137154276A039LL,0x29AF865F43948529LL,0xD8C8BC25CE51257ALL,0xB336C644D7FA12E5LL,18446744073709551615UL,2UL,18446744073709551615UL,0xB336C644D7FA12E5LL,0xD8C8BC25CE51257ALL}}};
        struct S0 l_2654 = {-6778,-4L};
        int16_t l_2665[6][9] = {{(-1L),0x05AEL,(-1L),(-1L),0x05AEL,(-1L),(-1L),0x05AEL,(-1L)},{0xD1DDL,(-1L),0xD1DDL,0xD1DDL,(-1L),0xD1DDL,0xD1DDL,(-1L),0xD1DDL},{(-1L),0x05AEL,(-1L),(-1L),0x05AEL,(-1L),(-1L),0x05AEL,(-1L)},{0xD1DDL,(-1L),0xD1DDL,0xD1DDL,(-1L),0xD1DDL,0xD1DDL,(-1L),0xD1DDL},{(-1L),0x05AEL,(-1L),(-1L),0x05AEL,(-1L),(-1L),0x05AEL,(-1L)},{0xD1DDL,(-1L),0xD1DDL,0xD1DDL,(-1L),0xD1DDL,0xD1DDL,(-1L),0xD1DDL}};
        uint8_t l_2666 = 0x84L;
        float l_2702 = 0x6.1p+1;
        uint64_t l_2755 = 0x592123271BCC58E7LL;
        int i, j, k;
        g_2492.f0 |= (safe_mod_func_uint32_t_u_u(l_2653[1][2][6], (((*g_1256) = (((-0x1.1p-1) < (l_2654 , (safe_mul_func_float_f_f((((safe_div_func_float_f_f(((safe_mul_func_float_f_f(0x5.Fp-1, ((((safe_mul_func_uint8_t_u_u(248UL, 1UL)) > ((*g_1256) , (safe_lshift_func_uint16_t_u_s(0x4224L, (((l_2653[1][2][6] & l_2653[1][2][6]) != l_2665[3][6]) || l_2666))))) > 0xBCL) , l_2653[1][2][6]))) != 0x2.651BC4p-1), l_2665[3][6])) != (*g_290)) > l_2667), (*g_1256))))) != (*g_1507))) , (*g_484))));
        for (l_2489 = 0; (l_2489 <= 0); l_2489 += 1)
        { /* block id: 1255 */
            struct S0 l_2668 = {-30315,2L};
            int32_t l_2705 = 0xC8EED80DL;
            int32_t *l_2725[6][4][5] = {{{&l_2705,&g_144[7][3][0],&g_55,&g_2,&l_2705},{&g_165.f1,&g_165.f1,(void*)0,&g_165.f1,&g_165.f1},{&g_1470,&g_144[7][3][0],&l_2668.f1,&g_2,&l_2594[1][6][0]},{(void*)0,&l_2401.f1,(void*)0,&l_2705,&l_2705}},{{(void*)0,&g_55,&g_51[3][2][3],&g_144[7][3][0],&l_2594[1][6][0]},{(void*)0,&l_2705,&g_165.f1,&l_2654.f1,&g_165.f1},{&l_2594[1][6][0],&l_2594[1][6][0],&g_1470,(void*)0,&l_2705},{(void*)0,(void*)0,(void*)0,&l_2705,(void*)0}},{{(void*)0,&g_55,(void*)0,&g_165.f1,&g_2},{(void*)0,(void*)0,(void*)0,&l_2401.f1,(void*)0},{&g_1470,&l_2594[1][6][0],&l_2594[1][6][0],&g_1470,(void*)0},{&g_165.f1,&l_2705,(void*)0,&l_2705,&g_51[1][0][8]}},{{&g_51[3][2][3],&g_55,(void*)0,&g_55,&g_51[3][2][3]},{(void*)0,&l_2401.f1,(void*)0,&l_2705,&g_2492.f1},{&l_2668.f1,&g_144[7][3][0],&g_1470,&g_1470,&g_144[7][3][0]},{(void*)0,&g_165.f1,&g_165.f1,&l_2401.f1,&g_2492.f1}},{{&g_55,&g_1470,&g_51[3][2][3],&g_165.f1,&g_51[3][2][3]},{&g_2492.f1,&l_2668.f1,(void*)0,&l_2705,&g_51[1][0][8]},{&g_55,&l_2705,&l_2668.f1,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2654.f1,(void*)0}},{{&l_2668.f1,&l_2705,&g_55,&g_144[7][3][0],&g_2},{(void*)0,&l_2668.f1,&g_2492.f1,&l_2705,(void*)0},{&g_51[3][2][3],&g_1470,&g_55,&g_2,&l_2705},{&g_165.f1,&g_165.f1,(void*)0,&g_165.f1,&g_165.f1}}};
            int64_t *l_2731 = &g_285;
            int i, j, k;
            g_1139 ^= (**g_186);
            (*g_166) = l_2668;
            for (l_2602 = 0; (l_2602 <= 0); l_2602 += 1)
            { /* block id: 1260 */
                uint64_t l_2703 = 0UL;
                int32_t l_2706 = 2L;
                uint64_t l_2707 = 0x08C26BBC63FDD265LL;
                uint32_t l_2757 = 0xF56752CFL;
                for (g_255 = 0; (g_255 >= 0); g_255 -= 1)
                { /* block id: 1263 */
                    struct S0 * const *l_2680 = &g_86;
                    struct S0 * const ** const l_2679 = &l_2680;
                    for (l_2576 = 0; (l_2576 <= 0); l_2576 += 1)
                    { /* block id: 1266 */
                        float ***l_2669[8][1] = {{&l_2433},{&l_2433},{&l_2433},{&l_2433},{&l_2433},{&l_2433},{&l_2433},{&l_2433}};
                        int32_t *l_2697[2][10] = {{&g_1743[3],&l_2435[1][2][0],&g_1743[2],&g_1743[2],&l_2435[1][2][0],&g_1743[3],&l_2435[1][2][0],&g_1743[2],&g_1743[2],&l_2435[1][2][0]},{&g_1743[3],&l_2435[1][2][0],&g_1743[2],&g_1743[2],&l_2435[1][2][0],&g_1743[3],&l_2435[1][2][0],&g_1743[2],&g_1743[2],&l_2435[1][2][0]}};
                        uint64_t *l_2704 = &l_2653[1][2][6];
                        int i, j, k;
                        g_2670[1][0] = (void*)0;
                        l_2706 ^= (((0UL != ((safe_add_func_uint64_t_u_u(((*l_2704) &= (safe_mul_func_int16_t_s_s((((safe_lshift_func_int16_t_s_s((l_2679 == (*g_829)), 5)) > ((safe_rshift_func_uint16_t_u_s(g_335[l_2576][(l_2602 + 9)][g_255], g_50[(l_2602 + 9)][(l_2489 + 5)][l_2576])) && (((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_u((safe_unary_minus_func_int32_t_s((safe_div_func_uint16_t_u_u((((safe_div_func_uint64_t_u_u(((*l_2603) &= ((+((safe_mod_func_uint16_t_u_u(g_50[(l_2602 + 1)][(g_255 + 4)][l_2602], (safe_div_func_uint8_t_u_u(((l_2668.f0 &= ((void*)0 == &g_1175)) , (safe_sub_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(0xA0L, 5)), g_335[0][2][0]))), l_2668.f1)))) & l_2668.f1)) , 0UL)), l_2668.f1)) == l_2703) < g_2442[8]), 65532UL)))), 12)), 0xA4FD2CCC5F13A063LL)) , (*g_1570)) == (void*)0))) | 0xDF41L), g_335[l_2576][(l_2602 + 9)][g_255]))), (*g_2459))) , l_2703)) == l_2705) , l_2668.f0);
                    }
                }
                for (l_2598 = 0; (l_2598 <= 0); l_2598 += 1)
                { /* block id: 1276 */
                    int64_t l_2716 = 1L;
                    int32_t *l_2726 = &l_2705;
                    int32_t l_2734 = 0x5DADC7EAL;
                    for (g_1000 = 0; (g_1000 <= 0); g_1000 += 1)
                    { /* block id: 1279 */
                        int16_t l_2732 = 9L;
                        int32_t l_2733[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_2733[i] = 0x2C49F694L;
                        if (l_2707)
                            break;
                        l_2734 |= ((((((safe_rshift_func_int8_t_s_s(((((((safe_div_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((safe_div_func_uint64_t_u_u(l_2716, (((safe_mod_func_uint32_t_u_u((*g_1075), (safe_mod_func_int8_t_s_s(((((*l_2726) = (((safe_lshift_func_int8_t_s_u(((-5L) ^ g_36), (((safe_sub_func_uint32_t_u_u(((l_2725[4][1][0] = (void*)0) != l_2726), (safe_rshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_u(l_2668.f0, (l_2665[1][1] > (l_2731 == (void*)0)))), (*l_2726))))) , (*g_2671)) , 246UL))) ^ l_2668.f1) | l_2654.f1)) , l_2732) , l_2703), l_2732)))) <= (*g_484)) , (***g_1570)))), 1)), 0xABD9L)) , l_2732) >= g_228[2]) ^ l_2707) && (-3L)) != l_2733[2]), 1)) , l_2707) <= l_2733[2]) & l_2668.f1) | (**g_2458)) <= 0x36L);
                    }
                    if (l_2665[3][6])
                        break;
                }
                for (g_36 = 0; (g_36 <= 0); g_36 += 1)
                { /* block id: 1289 */
                    int8_t l_2735 = 0x4DL;
                    int32_t *l_2739 = &l_2594[1][6][0];
                    float *l_2756 = &g_289;
                    ++l_2736;
                    l_2654.f0 ^= (((*g_1075) <= (l_2739 != &l_2706)) > ((((((((((safe_div_func_float_f_f((safe_div_func_float_f_f((*g_290), ((safe_add_func_float_f_f((((safe_add_func_float_f_f((((*l_2756) = ((9L != (((l_2654.f1 ^= (((l_2750 | (safe_sub_func_uint64_t_u_u(((((*l_2739) == (safe_mul_func_int16_t_s_s(0L, (g_11[0][0] && (*g_2459))))) == (*l_2739)) <= l_2707), 0xB8CF30C79C192D50LL))) && l_2755) , l_2706)) > 0L) | l_2703)) , (*g_1507))) < 0x4.6p+1), 0x9.89E7FBp+90)) > l_2653[1][2][6]) >= (*g_1507)), (*g_1507))) == (*l_2739)))), (*g_1256))) <= l_2757) == (*g_1507)) != (*g_1256)) , (void*)0) != (void*)0) ^ 0x87D2L) , (void*)0) != (*g_828)) & 0x377EL));
                }
            }
        }
        return g_2758;
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_484 g_68 g_743 g_739 g_1965 g_51 g_823 g_204 g_899 g_801 g_480 g_1470 g_185 g_1027 g_2108 g_1507 g_1508 g_2013 g_293 g_2023 g_186 g_54 g_863 g_291 g_203 g_86 g_165 g_77 g_36 g_1256 g_11 g_822 g_2184 g_2191 g_1682 g_1572 g_902 g_847 g_162 g_809 g_936 g_2344 g_228 g_363 g_196 g_2376 g_1571 g_285 g_1000 g_255
 * writes: g_285 g_1139 g_293 g_739 g_51 g_1470 g_86 g_899 g_68 g_1682 g_54 g_2108 g_1000 g_1202 g_1687 g_1439 g_291 g_1636 g_883 g_335 g_994 g_36 g_61.f1 g_255 g_162 g_809 g_936 g_76 g_937 g_228 g_902
 */
static struct S0  func_5(uint16_t  p_6, uint32_t  p_7)
{ /* block id: 984 */
    int32_t *l_2039 = (void*)0;
    uint32_t l_2054 = 4294967295UL;
    uint32_t **l_2056 = &g_484;
    struct S0 *l_2062[10];
    int8_t *l_2078 = (void*)0;
    int8_t ** const l_2077 = &l_2078;
    int64_t *l_2080 = &g_285;
    int8_t l_2089 = 0xD4L;
    uint8_t l_2090 = 0xF8L;
    uint32_t l_2091 = 4294967286UL;
    uint16_t l_2105[8];
    uint8_t ***l_2106 = (void*)0;
    int32_t l_2107 = (-2L);
    int32_t l_2119 = 0xDEE6EE3EL;
    int32_t l_2124 = 0x649E8EE0L;
    int32_t l_2125 = 3L;
    int32_t l_2126 = 0xE1B0F05BL;
    int32_t l_2127 = 0L;
    struct S0 *****l_2135 = &g_822;
    int32_t l_2176 = 0xD0E3497FL;
    struct S0 ***l_2180 = &g_204;
    int16_t *l_2182 = &g_228[4];
    int32_t l_2240 = 0x62A66E88L;
    int32_t l_2241 = 0L;
    int32_t l_2242[5][5][8] = {{{0x4BD90867L,0x1351B3B9L,(-8L),(-6L),0x4665AAABL,0xE3C27CBAL,0x64EDA8F5L,0x04513ECEL},{0x6F9C3A90L,0xA0CCB0C8L,0xAC60CB9EL,0x4D6F5E02L,0x4A96F268L,5L,(-8L),0x46F7589BL},{0xE3233A39L,(-3L),0x46F7589BL,1L,1L,0x46F7589BL,(-3L),0xE3233A39L},{0x637826DAL,0xAC60CB9EL,0x0982D639L,0xB2BD5D16L,0xAEA97A0AL,1L,0x8CE4A9CAL,(-1L)},{0L,1L,0xE3233A39L,0x5A0D50F8L,0xB2BD5D16L,1L,(-10L),0x2A20E4E1L}},{{0x4A96F268L,0xAC60CB9EL,4L,0L,0x687C5984L,(-8L),0xAC60CB9EL,(-1L)},{0xAE3A3262L,5L,0x0982D639L,0x64EDA8F5L,(-2L),0x17AF087EL,0x4BD90867L,(-10L)},{0xE3233A39L,0x4BD90867L,1L,(-9L),1L,(-10L),0x46F7589BL,0xA7AF2F1AL},{0x609B5DD4L,0x5A0D50F8L,0x2D896DF7L,0L,0xB2BD5D16L,0x4665AAABL,0x4D6F5E02L,(-1L)},{0x07F3C8A6L,0x2D896DF7L,1L,0x6F2389C7L,1L,0x4BD90867L,0x609B5DD4L,0x4BD90867L}},{{(-9L),0x04513ECEL,0L,0x04513ECEL,(-9L),0L,0x17AF087EL,(-10L)},{0L,0x609B5DD4L,4L,1L,0x8CE4A9CAL,0x1351B3B9L,0x687C5984L,0x04513ECEL},{0x1351B3B9L,(-2L),4L,5L,(-7L),0xA7AF2F1AL,0x17AF087EL,6L},{0x8CE4A9CAL,0xAC60CB9EL,0L,(-10L),0x4BD90867L,0x4A96F268L,0x609B5DD4L,0xE3C27CBAL},{(-1L),0x637826DAL,1L,0L,0x62B3BEA0L,0xA0CCB0C8L,0x4D6F5E02L,0x46F7589BL}},{{0L,0x4665AAABL,0x2D896DF7L,0xAEA97A0AL,0xAC60CB9EL,0x252A31B2L,0x46F7589BL,0L},{0L,6L,1L,0x2D896DF7L,1L,4L,0x4BD90867L,0xB2BD5D16L},{0xAEA97A0AL,0xB2BD5D16L,0x0982D639L,0xAC60CB9EL,0x637826DAL,0x637826DAL,0xAC60CB9EL,0x0982D639L},{4L,4L,0x6F2389C7L,0x6F9C3A90L,0L,0x8CE4A9CAL,1L,0L},{1L,(-8L),0x4A96F268L,0x5A0D50F8L,0L,0x04513ECEL,0L,0L}},{{(-8L),0x07F3C8A6L,1L,0x6F9C3A90L,(-10L),0xAE3A3262L,5L,0x0982D639L},{(-1L),0x687C5984L,(-8L),0xAC60CB9EL,(-1L),0L,(-3L),0xB2BD5D16L},{1L,1L,(-5L),0x2D896DF7L,(-6L),0x5A0D50F8L,0x62B3BEA0L,0L},{0x4BD90867L,(-1L),(-3L),0xAEA97A0AL,6L,0x4D6F5E02L,0x8CE4A9CAL,0x46F7589BL},{0x64EDA8F5L,4L,0xB2BD5D16L,0L,0x5A0D50F8L,1L,9L,0xE3C27CBAL}}};
    int8_t l_2255[5][6] = {{(-1L),0xAAL,0x06L,0x06L,0xAAL,(-1L)},{0xD3L,(-1L),(-4L),0xAAL,(-4L),(-1L)},{(-4L),0xD3L,0x06L,(-1L),(-1L),0x06L},{(-4L),(-4L),(-1L),0xAAL,(-9L),0xAAL},{0xD3L,(-4L),0xD3L,0x06L,(-1L),(-1L)}};
    int64_t l_2324 = (-7L);
    int16_t l_2355 = (-9L);
    uint64_t l_2377 = 0x53C175A2C40A77AALL;
    int16_t l_2396[4];
    uint32_t l_2397 = 0x606F1CA0L;
    struct S0 l_2400 = {34213,-1L};
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_2062[i] = &g_165;
    for (i = 0; i < 8; i++)
        l_2105[i] = 0UL;
    for (i = 0; i < 4; i++)
        l_2396[i] = 0x8D26L;
lbl_2298:
    for (g_285 = 0; (g_285 < 23); g_285 = safe_add_func_int16_t_s_s(g_285, 4))
    { /* block id: 987 */
        const int32_t l_2043 = 0L;
        uint32_t *l_2059 = &g_68;
        uint16_t l_2079 = 0x948DL;
        int32_t l_2113[1][1][7] = {{{0xE2B79DC8L,0x695CD1D2L,0xE2B79DC8L,0xE2B79DC8L,0x695CD1D2L,(-1L),(-1L)}}};
        struct S0 l_2114 = {-14561,0x7F13BCF5L};
        struct S0 *****l_2136 = &g_822;
        uint64_t *l_2159 = &g_1636;
        uint64_t **l_2158 = &l_2159;
        uint64_t l_2183 = 0x7C4FD0C6BF6C9303LL;
        int32_t l_2208 = 0L;
        int i, j, k;
        for (g_1139 = 26; (g_1139 >= 14); --g_1139)
        { /* block id: 990 */
            int32_t **l_2040 = &g_293;
            uint32_t ***l_2055[5];
            int32_t *l_2060 = (void*)0;
            int32_t *l_2061 = &g_1470;
            uint16_t *l_2065 = &g_899;
            int8_t *l_2076 = &g_1965[1];
            int8_t **l_2075[9][3] = {{&l_2076,(void*)0,&l_2076},{&l_2076,(void*)0,&l_2076},{(void*)0,(void*)0,(void*)0},{(void*)0,&l_2076,&l_2076},{(void*)0,&l_2076,&l_2076},{(void*)0,(void*)0,(void*)0},{(void*)0,&l_2076,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_2076,&l_2076,(void*)0}};
            int64_t *l_2088 = &g_1682[6][2];
            int32_t l_2120 = 3L;
            int32_t l_2121 = 0xDDD5D57DL;
            int32_t l_2122[5];
            struct S0 **** const *l_2137 = &g_822;
            uint64_t *l_2147 = &g_902;
            uint64_t **l_2146[6][7][6] = {{{&l_2147,(void*)0,(void*)0,&l_2147,(void*)0,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,(void*)0,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,(void*)0,(void*)0},{&l_2147,&l_2147,&l_2147,&l_2147,(void*)0,&l_2147},{&l_2147,(void*)0,&l_2147,&l_2147,&l_2147,(void*)0},{&l_2147,(void*)0,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,(void*)0}},{{&l_2147,&l_2147,&l_2147,&l_2147,(void*)0,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,(void*)0,(void*)0,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,(void*)0,(void*)0,&l_2147,&l_2147},{&l_2147,&l_2147,(void*)0,&l_2147,&l_2147,&l_2147}},{{&l_2147,&l_2147,&l_2147,(void*)0,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,(void*)0,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,(void*)0},{&l_2147,(void*)0,&l_2147,&l_2147,(void*)0,&l_2147},{&l_2147,(void*)0,&l_2147,&l_2147,(void*)0,(void*)0},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147}},{{&l_2147,&l_2147,&l_2147,(void*)0,&l_2147,(void*)0},{(void*)0,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{(void*)0,&l_2147,&l_2147,(void*)0,&l_2147,&l_2147},{&l_2147,&l_2147,(void*)0,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,(void*)0,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,(void*)0}},{{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{(void*)0,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,(void*)0,&l_2147,&l_2147,&l_2147}},{{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,(void*)0,&l_2147,&l_2147,&l_2147},{(void*)0,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,(void*)0},{&l_2147,&l_2147,&l_2147,&l_2147,&l_2147,&l_2147},{&l_2147,&l_2147,&l_2147,(void*)0,&l_2147,(void*)0}}};
            uint64_t ***l_2145 = &l_2146[5][1][3];
            uint64_t ****l_2144 = &l_2145;
            uint64_t *****l_2143 = &l_2144;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_2055[i] = &g_739;
            for (i = 0; i < 5; i++)
                l_2122[i] = (-10L);
            (*l_2040) = l_2039;
            (**g_823) = ((((safe_div_func_uint32_t_u_u(l_2043, ((*l_2061) = (g_51[3][2][4] |= (safe_mod_func_uint32_t_u_u(((safe_sub_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_s(1UL, 10)) | ((*g_484) == (safe_add_func_int16_t_s_s(p_6, ((l_2043 == (safe_lshift_func_uint8_t_u_s((l_2054 ^ (((*g_743) = (*g_743)) != l_2056)), 2))) , (safe_div_func_int16_t_s_s((l_2059 == (void*)0), 5UL))))))), g_1965[1])) && 0L), (*g_484))))))) >= (-10L)) != p_6) , l_2062[8]);
            (*g_185) = (safe_lshift_func_uint16_t_u_u((--(*l_2065)), (safe_unary_minus_func_int32_t_s(((p_6 <= ((safe_rshift_func_int16_t_s_s(g_801[4], (safe_add_func_uint64_t_u_u((l_2089 = (safe_rshift_func_int16_t_s_u((l_2075[5][1] == l_2077), ((((l_2079 , (l_2080 != (void*)0)) || ((((((+p_7) <= (safe_div_func_int64_t_s_s(((*l_2088) = (((*g_480) = (safe_mul_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(p_6, p_7)), 246UL))) == p_7)), 6L))) || 0x9F9DL) ^ p_6) ^ l_2079) != g_1965[1])) || 0xB5ADBFA8L) < (-6L))))), (*l_2061))))) == l_2090)) , l_2091)))));
            if (((void*)0 == &g_293))
            { /* block id: 1001 */
                float l_2092 = 0x9.DEE458p-23;
                int32_t l_2100 = 0L;
                struct S0 *l_2112[5] = {&g_165,&g_165,&g_165,&g_165,&g_165};
                int32_t l_2118[10];
                int32_t l_2123 = 0x72793645L;
                uint32_t l_2128[3];
                int i;
                for (i = 0; i < 10; i++)
                    l_2118[i] = (-1L);
                for (i = 0; i < 3; i++)
                    l_2128[i] = 18446744073709551614UL;
                g_2108 |= (p_7 > (safe_rshift_func_uint8_t_u_u((((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(((g_1027[5][4][3] && (~(l_2100 | (((safe_lshift_func_int16_t_s_u(((safe_mul_func_uint8_t_u_u((&g_362[0] == (void*)0), l_2105[1])) != p_7), 4)) ^ (l_2106 != (void*)0)) | p_6)))) >= l_2107), 0)), p_6)) , 18446744073709551615UL) == 18446744073709551615UL), 4)));
                for (g_1000 = (-9); (g_1000 > 3); g_1000 = safe_add_func_uint32_t_u_u(g_1000, 3))
                { /* block id: 1005 */
                    uint32_t l_2111 = 2UL;
                    int32_t *l_2115 = &l_2114.f1;
                    int32_t *l_2116 = (void*)0;
                    int32_t *l_2117[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_2117[i] = (void*)0;
                    l_2111 = (*g_1507);
                    (**g_823) = l_2112[4];
                    (*g_2023) = ((l_2114 = func_8((l_2113[0][0][0] ^= l_2043))) , &l_2100);
                    --l_2128[0];
                }
                if (l_2114.f1)
                    continue;
            }
            else
            { /* block id: 1014 */
                float *l_2140 = &g_1687;
                float *l_2141 = (void*)0;
                float *l_2142[6] = {&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439};
                int i;
                (*g_1256) = ((safe_mul_func_float_f_f((safe_mul_func_float_f_f((*g_863), (g_1439 = ((((l_2136 = l_2135) != ((***g_203) , l_2137)) ^ 0x99L) , ((*l_2140) = ((safe_div_func_uint16_t_u_u(p_6, ((&g_1175 == (void*)0) | (((*g_77) >= p_6) ^ 0xCE3A63A677434CA8LL)))) , 0x2.4C43DCp+43)))))), 0x1.Fp-1)) <= (*g_1256));
                l_2143 = (void*)0;
            }
        }
        for (l_2079 = 1; (l_2079 <= 5); l_2079 += 1)
        { /* block id: 1024 */
            uint64_t *l_2157 = &g_1636;
            uint64_t **l_2156 = &l_2157;
            uint8_t l_2175 = 255UL;
            int8_t *l_2177 = &l_2089;
            uint32_t l_2181 = 0x65A8C894L;
            int8_t *l_2185 = (void*)0;
            int8_t *l_2186 = &g_883;
            uint16_t *l_2187 = &g_335[0][3][0];
            int32_t l_2188 = 0xED700720L;
            struct S0 l_2227 = {43610,0L};
            uint32_t * const l_2234 = &g_2235;
            uint32_t * const *l_2233[9] = {&l_2234,&l_2234,&l_2234,&l_2234,&l_2234,&l_2234,&l_2234,&l_2234,&l_2234};
            int32_t **l_2236 = &g_293;
            int32_t l_2243 = 0x9957DAADL;
            int32_t l_2244 = 1L;
            int32_t l_2245 = 0xC1FB8274L;
            uint32_t l_2246 = 4294967295UL;
            int i, j;
            if (((l_2188 = ((*l_2187) = ((safe_lshift_func_uint8_t_u_s((((((*l_2186) = (((safe_mul_func_int8_t_s_s(g_11[l_2079][l_2079], (safe_mul_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((((((l_2156 == l_2158) | (p_6 & (safe_mod_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_u((p_7 < (safe_div_func_int8_t_s_s((p_6 >= (safe_div_func_int32_t_s_s((((safe_rshift_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((+l_2175), (((*l_2177) = (l_2176 = p_7)) & (((((*l_2157) = ((safe_lshift_func_int8_t_s_s((l_2180 == (**l_2135)), l_2181)) && p_7)) , l_2182) != (void*)0) , p_6)))), p_6)) == g_11[l_2079][l_2079]), 14)) || l_2183) != p_6), g_2184))), l_2113[0][0][0]))), 14)), 0x3F393C2DF5847FBBLL)))) , p_6) , p_6) && 0x4F98L), p_6)), g_11[l_2079][l_2079])))) >= p_6) ^ p_7)) , p_7) < (-1L)) <= l_2114.f0), p_6)) | p_7))) & l_2043))
            { /* block id: 1031 */
                int32_t *l_2189 = (void*)0;
                (*g_2191) = l_2189;
            }
            else
            { /* block id: 1033 */
                uint32_t l_2230 = 0xF734E6BAL;
                for (g_994 = 0; (g_994 <= 4); g_994 += 1)
                { /* block id: 1036 */
                    int32_t *l_2192[2][1][2];
                    int32_t **l_2193 = &l_2192[1][0][0];
                    int64_t *l_2209 = &g_1682[3][6];
                    struct S0 l_2228 = {42971,0x951E9A90L};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 1; j++)
                        {
                            for (k = 0; k < 2; k++)
                                l_2192[i][j][k] = &l_2113[0][0][2];
                        }
                    }
                    (*l_2193) = l_2192[0][0][0];
                    if (p_7)
                        continue;
                    if ((safe_rshift_func_int16_t_s_u(((p_6 = (**l_2193)) , 7L), (safe_rshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_s((((*g_77)++) == p_7), (safe_rshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_u((safe_unary_minus_func_int16_t_s(((~(((*l_2186) = ((p_7 && l_2208) , (((*l_2209) &= p_7) || ((*g_1572) , ((safe_lshift_func_int8_t_s_s(((!(safe_sub_func_int32_t_s_s(((safe_lshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((safe_add_func_int32_t_s_s((-5L), ((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u(((*l_2187) = l_2043), p_7)) >= p_7), 3)), p_7)) | (**l_2193)))), 4294967289UL)), 3)) != 0xB455L), p_7))) & 4L), 6)) > 0xB2L))))) != 0L)) || 4294967292UL))), p_7)), 8)))), g_11[l_2079][l_2079])))))
                    { /* block id: 1044 */
                        (**l_2193) = p_7;
                        if (p_6)
                            continue;
                        if (p_6)
                            break;
                    }
                    else
                    { /* block id: 1048 */
                        int32_t l_2229 = 0x9ECD52D1L;
                        l_2228 = l_2227;
                        --l_2230;
                        (**l_2193) &= l_2230;
                    }
                    for (g_61.f1 = 4; (g_61.f1 >= 0); g_61.f1 -= 1)
                    { /* block id: 1055 */
                        (*g_1256) = ((*g_743) == (l_2233[1] = l_2056));
                    }
                }
                if (p_7)
                    continue;
                (*g_185) &= 0xB373B1A0L;
            }
            (*l_2236) = &l_2188;
            for (g_255 = 3; (g_255 >= 0); g_255 -= 1)
            { /* block id: 1066 */
                int32_t *l_2237 = &l_2227.f1;
                int32_t *l_2238 = &g_1470;
                int32_t *l_2239[6][7][2] = {{{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1}},{{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1}},{{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{&g_51[3][2][6],&g_61.f1},{(void*)0,&g_61.f1},{&g_51[3][2][6],(void*)0},{(void*)0,(void*)0},{(void*)0,&g_51[3][2][6]},{&l_2227.f1,(void*)0}}};
                int8_t *l_2265[7] = {&g_1965[0],(void*)0,&g_1965[0],&g_1965[0],(void*)0,&g_1965[0],&g_1965[0]};
                int i, j, k;
                (*l_2236) = &l_2126;
                (*l_2236) = &l_2113[0][0][4];
                --l_2246;
                l_2114.f1 |= ((safe_sub_func_int64_t_s_s((((safe_sub_func_int64_t_s_s((safe_add_func_int8_t_s_s((func_8(p_6) , l_2255[4][4]), p_7)), (-2L))) , (safe_mul_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u(((safe_add_func_int32_t_s_s((!0x2604L), (((l_2265[0] == l_2265[1]) | 0xE7F329C724748E89LL) < l_2114.f0))) & 0UL), p_7)), l_2113[0][0][0])) | (*l_2238)) != (**l_2236)) == (*g_1572)), 254UL))) | p_6), 0L)) & g_847[0][5][3]);
            }
        }
        (*g_185) |= p_7;
    }
    for (g_1470 = 0; (g_1470 > (-13)); g_1470 = safe_sub_func_int32_t_s_s(g_1470, 7))
    { /* block id: 1077 */
        int32_t l_2291 = 0x0137E068L;
        struct S0 l_2295 = {21761,0x14D4D221L};
        int32_t l_2305 = (-3L);
        int32_t l_2308[5][6][8] = {{{0xEC2A0EC8L,0x2B9730A0L,0xB6C8E8B9L,(-1L),0xD2489EBFL,0xCE8B87F2L,(-4L),(-1L)},{0x4265089CL,5L,(-1L),0L,(-1L),(-1L),0xDE6C3C61L,(-1L)},{0x2B9730A0L,(-1L),0xD2489EBFL,0xDE6C3C61L,3L,0x488655CEL,3L,0xDE6C3C61L},{1L,9L,1L,0x747C8EBBL,(-1L),0x69C654EAL,(-9L),0L},{0x0886D678L,(-1L),9L,3L,2L,0x4265089CL,(-1L),1L},{0x0886D678L,0x5A1F3C9AL,0x69C654EAL,0x2B9730A0L,(-1L),(-2L),0x488655CEL,(-1L)}},{{1L,2L,0xCE8B87F2L,7L,3L,0x9390C548L,1L,0x69C654EAL},{0x2B9730A0L,0xD2489EBFL,7L,0x3414256CL,(-1L),5L,0x69C654EAL,0xB6C8E8B9L},{0x4265089CL,0x0886D678L,(-9L),(-1L),0xD2489EBFL,0xD2489EBFL,(-1L),(-9L)},{0xEC2A0EC8L,0xEC2A0EC8L,(-3L),0xD2489EBFL,(-9L),1L,0L,0x5227CD69L},{0x747C8EBBL,0xB6C8E8B9L,5L,(-4L),1L,0x1D17CBE8L,0x2B9730A0L,0x5227CD69L},{0xB6C8E8B9L,1L,0x5A1F3C9AL,0xD2489EBFL,0L,9L,0xEEA28F6CL,(-9L)}},{{(-1L),0xDE6C3C61L,0x3414256CL,(-1L),0x747C8EBBL,0L,0x93E920F8L,0xB6C8E8B9L},{(-2L),0x1D17CBE8L,0x747C8EBBL,0x3414256CL,0x0886D678L,2L,0xD2489EBFL,0x69C654EAL},{0xEEA28F6CL,(-1L),(-2L),7L,1L,7L,(-2L),(-1L)},{(-1L),(-1L),(-1L),0x2B9730A0L,0x9390C548L,0x747C8EBBL,(-3L),1L},{(-9L),(-4L),0x8DAECFDCL,3L,(-1L),0L,(-3L),0L},{0L,3L,(-1L),0x747C8EBBL,(-1L),7L,(-2L),0xDE6C3C61L}},{{(-1L),7L,(-2L),0xDE6C3C61L,0x8DAECFDCL,0x0886D678L,0xD2489EBFL,(-1L)},{(-1L),0x69C654EAL,0x747C8EBBL,0L,1L,(-1L),0x93E920F8L,(-1L)},{0x4DD1A172L,0xEEA28F6CL,0x3414256CL,(-1L),(-1L),0x3414256CL,0xEEA28F6CL,0x4DD1A172L},{0x3414256CL,0x9390C548L,0x5A1F3C9AL,7L,0xEC2A0EC8L,0x93E920F8L,0x2B9730A0L,0xCE8B87F2L},{9L,0x488655CEL,5L,0L,0x0886D678L,0xCE8B87F2L,5L,0xEEA28F6CL},{0x4DD1A172L,(-1L),0L,0x69C654EAL,0x3414256CL,0xD2489EBFL,9L,(-1L)}},{{0xEEA28F6CL,0x93E920F8L,0x747C8EBBL,(-4L),0xD2489EBFL,0xFD0B5B68L,0x9390C548L,1L},{0x5227CD69L,0x9390C548L,0x0886D678L,5L,(-3L),0x488655CEL,(-1L),0xFD0B5B68L},{0L,0x0886D678L,1L,(-1L),1L,0x0886D678L,0L,0L},{(-4L),(-1L),0x9390C548L,0xCE8B87F2L,3L,5L,0x2B9730A0L,0x4DD1A172L},{0xCE8B87F2L,0xEC2A0EC8L,(-1L),0L,3L,(-1L),0x747C8EBBL,(-1L)},{(-4L),0xFD0B5B68L,(-1L),0x4DD1A172L,1L,0x5227CD69L,(-1L),(-1L)}}};
        uint64_t l_2315 = 1UL;
        int32_t l_2325 = 0x8350BCA1L;
        uint64_t l_2327 = 0xC7C52FBDF935E8AALL;
        uint16_t ****l_2345 = &g_936;
        int8_t l_2378 = 0L;
        int32_t *l_2395[4][5] = {{(void*)0,(void*)0,&l_2124,(void*)0,(void*)0},{&g_165.f1,&l_2305,&g_165.f1,&g_165.f1,&l_2305},{(void*)0,&l_2242[4][1][5],&l_2242[4][1][5],(void*)0,&l_2242[4][1][5]},{&l_2305,&l_2305,&l_2242[3][0][1],&l_2305,&l_2305}};
        int i, j, k;
        for (g_162 = 4; (g_162 >= 0); g_162 -= 1)
        { /* block id: 1080 */
            int8_t l_2272 = 0x66L;
            int32_t l_2277 = 0x7FB2EB3CL;
            struct S0 l_2292 = {9901,-1L};
            int32_t l_2307 = 7L;
            int32_t l_2311 = 0L;
            int32_t l_2312[3];
            float l_2352 = 0x5.64E5E4p-16;
            uint16_t l_2356 = 8UL;
            const uint32_t **l_2375 = &g_1075;
            int32_t l_2390 = 1L;
            int i;
            for (i = 0; i < 3; i++)
                l_2312[i] = (-1L);
            for (g_809 = 0; (g_809 <= 2); g_809 += 1)
            { /* block id: 1083 */
                struct S0 **** const l_2274 = &g_823;
                int32_t l_2278[8][4] = {{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L}};
                struct S0 l_2293 = {-2191,0x58B087D6L};
                int i, j;
                for (l_2126 = 2; (l_2126 >= 0); l_2126 -= 1)
                { /* block id: 1086 */
                    int16_t l_2294 = 0L;
                    int i, j, k;
                    for (g_36 = 0; (g_36 <= 2); g_36 += 1)
                    { /* block id: 1089 */
                        struct S0 ****l_2273 = &g_823;
                        struct S0 l_2296 = {33486,9L};
                        int32_t **l_2297 = &l_2039;
                        int i, j, k;
                        (*g_185) |= (safe_add_func_int32_t_s_s((l_2242[g_162][g_162][(g_162 + 3)] = l_2105[(l_2126 + 1)]), ((l_2277 = (0x604C85C1D9208B22LL ^ ((safe_mul_func_uint16_t_u_u(g_1965[(g_809 + 1)], (((((*l_2080) = (l_2272 >= p_6)) == 0xC20B6E1421FC0D30LL) , l_2273) == l_2274))) ^ ((**l_2056) |= ((safe_sub_func_int64_t_s_s(p_6, 1UL)) && p_6))))) || 1UL)));
                        l_2293 = l_2292;
                        l_2296 = l_2295;
                        (*l_2297) = &l_2241;
                    }
                    l_2242[g_162][l_2126][(l_2126 + 1)] ^= l_2105[(g_162 + 2)];
                    if (l_2107)
                        goto lbl_2298;
                }
                return l_2292;
            }
            for (l_2125 = 0; (l_2125 <= 7); l_2125 += 1)
            { /* block id: 1110 */
                int16_t l_2302 = 8L;
                int32_t l_2303 = (-6L);
                int32_t l_2304 = 0x2C0C1F5AL;
                int32_t l_2306 = 0x28CF33F1L;
                int32_t l_2309 = 0L;
                int32_t l_2310 = 6L;
                int32_t l_2313 = (-3L);
                int32_t l_2314[4][9][1] = {{{5L},{0x7372CF45L},{0x48F8D640L},{(-6L)},{0xE462A4FBL},{0x58F7840EL},{0xE462A4FBL},{(-6L)},{0x48F8D640L}},{{0x7372CF45L},{5L},{0x71E00FC7L},{0x71E00FC7L},{5L},{0x7372CF45L},{0x48F8D640L},{(-6L)},{0xE462A4FBL}},{{0x58F7840EL},{0xE462A4FBL},{(-6L)},{0x48F8D640L},{0x7372CF45L},{5L},{0x71E00FC7L},{0x71E00FC7L},{5L}},{{0x7372CF45L},{0x48F8D640L},{(-6L)},{0xE462A4FBL},{0x58F7840EL},{0xE462A4FBL},{(-6L)},{0x48F8D640L},{0x7372CF45L}}};
                int32_t *l_2318 = &l_2311;
                int32_t *l_2319 = &l_2310;
                int32_t *l_2320 = &l_2304;
                int32_t *l_2321 = (void*)0;
                int32_t *l_2322 = &l_2119;
                int32_t *l_2323[7] = {&l_2304,&l_2305,&l_2305,&l_2304,&l_2305,&l_2305,&l_2304};
                float l_2326 = (-0x2.4p-1);
                uint16_t ****l_2346 = &g_936;
                float l_2387[5][9][5] = {{{0x3.2DC3F6p+19,0x1.3p+1,0x1.9p-1,0xF.CF6E17p-58,0x1.9p-1},{0x1.1p-1,0x1.1p-1,0x1.DF63D2p-36,0x5.6p+1,(-0x7.8p-1)},{0x1.3p+1,0x3.2DC3F6p+19,0x3.2DC3F6p+19,0x1.3p+1,0x1.9p-1},{0xE.7C98DBp-4,0x5.6p+1,0xB.7F6A0Dp-20,0xB.7F6A0Dp-20,0x5.6p+1},{0x1.9p-1,0x3.2DC3F6p+19,(-0x1.6p-1),0x8.ED9C10p-88,0x8.ED9C10p-88},{0x8.3886E9p-32,0x1.1p-1,0x8.3886E9p-32,0xB.7F6A0Dp-20,0x1.DF63D2p-36},{0xF.CF6E17p-58,0x1.3p+1,0x8.ED9C10p-88,0x1.3p+1,0xF.CF6E17p-58},{0x8.3886E9p-32,0x8.3886E9p-32,(-0x7.8p-1),0xB.7F6A0Dp-20,(-0x7.8p-1)},{0xC.E024D8p+58,0xC.E024D8p+58,0x1.9p-1,(-0x1.6p-1),0x1.3p+1}},{{0x8.3886E9p-32,0x5.6p+1,0x5.6p+1,0x8.3886E9p-32,(-0x7.8p-1)},{0x3.2DC3F6p+19,(-0x1.6p-1),0x8.ED9C10p-88,0x8.ED9C10p-88,(-0x1.6p-1)},{(-0x7.8p-1),0x5.6p+1,0x1.DF63D2p-36,0x1.1p-1,0x1.1p-1},{0xF.CF6E17p-58,0xC.E024D8p+58,0xF.CF6E17p-58,0x8.ED9C10p-88,0x1.9p-1},{0xB.7F6A0Dp-20,0x8.3886E9p-32,0x1.1p-1,0x8.3886E9p-32,0xB.7F6A0Dp-20},{0xF.CF6E17p-58,0x3.2DC3F6p+19,0xC.E024D8p+58,(-0x1.6p-1),0xC.E024D8p+58},{(-0x7.8p-1),(-0x7.8p-1),0x1.1p-1,0xB.7F6A0Dp-20,0xE.7C98DBp-4},{0x3.2DC3F6p+19,0xF.CF6E17p-58,0xF.CF6E17p-58,0x3.2DC3F6p+19,0xC.E024D8p+58},{0x8.3886E9p-32,0xB.7F6A0Dp-20,0x1.DF63D2p-36,0x1.DF63D2p-36,0xB.7F6A0Dp-20}},{{0xC.E024D8p+58,0xF.CF6E17p-58,0x8.ED9C10p-88,0x1.9p-1,0x1.9p-1},{0x5.6p+1,(-0x7.8p-1),0x5.6p+1,0x1.DF63D2p-36,0x1.1p-1},{(-0x1.6p-1),0x3.2DC3F6p+19,0x1.9p-1,0x3.2DC3F6p+19,(-0x1.6p-1)},{0x5.6p+1,0x8.3886E9p-32,(-0x7.8p-1),0xB.7F6A0Dp-20,(-0x7.8p-1)},{0xC.E024D8p+58,0xC.E024D8p+58,0x1.9p-1,(-0x1.6p-1),0x1.3p+1},{0x8.3886E9p-32,0x5.6p+1,0x5.6p+1,0x8.3886E9p-32,(-0x7.8p-1)},{0x3.2DC3F6p+19,(-0x1.6p-1),0x8.ED9C10p-88,0x8.ED9C10p-88,(-0x1.6p-1)},{(-0x7.8p-1),0x5.6p+1,0x1.DF63D2p-36,0x1.1p-1,0x1.1p-1},{0xF.CF6E17p-58,0xC.E024D8p+58,0xF.CF6E17p-58,0x8.ED9C10p-88,0x1.9p-1}},{{0xB.7F6A0Dp-20,0x8.3886E9p-32,0x1.1p-1,0x8.3886E9p-32,0xB.7F6A0Dp-20},{0xF.CF6E17p-58,0x3.2DC3F6p+19,0xC.E024D8p+58,(-0x1.6p-1),0xC.E024D8p+58},{(-0x7.8p-1),(-0x7.8p-1),0x1.1p-1,0xB.7F6A0Dp-20,0xE.7C98DBp-4},{0x3.2DC3F6p+19,0xF.CF6E17p-58,0xF.CF6E17p-58,0x3.2DC3F6p+19,0xC.E024D8p+58},{0x8.3886E9p-32,0xB.7F6A0Dp-20,0x1.DF63D2p-36,0x1.DF63D2p-36,0xB.7F6A0Dp-20},{0xC.E024D8p+58,0xF.CF6E17p-58,0x8.ED9C10p-88,0x1.9p-1,0x1.9p-1},{0x5.6p+1,(-0x7.8p-1),0x5.6p+1,0x1.DF63D2p-36,0x1.1p-1},{(-0x1.6p-1),0x3.2DC3F6p+19,0x1.9p-1,0x3.2DC3F6p+19,(-0x1.6p-1)},{0x5.6p+1,0x8.3886E9p-32,(-0x7.8p-1),0xB.7F6A0Dp-20,(-0x7.8p-1)}},{{0xC.E024D8p+58,0xC.E024D8p+58,0x1.9p-1,(-0x1.6p-1),0x1.3p+1},{0x8.3886E9p-32,0x5.6p+1,0x5.6p+1,0x8.3886E9p-32,(-0x7.8p-1)},{0x3.2DC3F6p+19,(-0x1.6p-1),0x8.ED9C10p-88,0x8.ED9C10p-88,(-0x1.6p-1)},{(-0x7.8p-1),0x5.6p+1,0x1.DF63D2p-36,0x1.1p-1,0x1.1p-1},{0xF.CF6E17p-58,0xC.E024D8p+58,0xF.CF6E17p-58,0x8.ED9C10p-88,0x1.9p-1},{0xB.7F6A0Dp-20,0x8.3886E9p-32,0x1.1p-1,0x8.3886E9p-32,0xB.7F6A0Dp-20},{0xF.CF6E17p-58,0x3.2DC3F6p+19,0xC.E024D8p+58,(-0x1.6p-1),0xC.E024D8p+58},{(-0x7.8p-1),(-0x7.8p-1),0x1.1p-1,0xB.7F6A0Dp-20,0xE.7C98DBp-4},{0x3.2DC3F6p+19,0xF.CF6E17p-58,0xF.CF6E17p-58,0x3.2DC3F6p+19,0xC.E024D8p+58}}};
                float l_2389 = 0x7.8A0CDDp-16;
                int i, j, k;
                for (l_2124 = 0; (l_2124 <= 4); l_2124 += 1)
                { /* block id: 1113 */
                    uint16_t ****l_2299 = &g_936;
                    int32_t *l_2300 = &l_2119;
                    int32_t *l_2301[1];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_2301[i] = &l_2242[l_2124][g_162][(g_162 + 1)];
                    (*l_2299) = &g_937;
                    l_2315++;
                }
                l_2327++;
                for (g_76 = 0; (g_76 <= 7); g_76 += 1)
                { /* block id: 1120 */
                    int8_t l_2347 = 4L;
                    int32_t l_2348 = 1L;
                    int32_t l_2349 = 0x85C54A5DL;
                    int32_t l_2350[3];
                    uint32_t l_2391 = 0x14818A72L;
                    uint8_t l_2394 = 0x2DL;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_2350[i] = (-9L);
                    for (l_2305 = 0; (l_2305 <= 7); l_2305 += 1)
                    { /* block id: 1123 */
                        uint16_t **l_2330 = (void*)0;
                        int32_t l_2351 = 0xDE79A634L;
                        int32_t l_2353 = 6L;
                        int32_t l_2354[4][5][9] = {{{0x6BC5BDBBL,0x2CFFF82BL,(-4L),(-1L),0x6BC5BDBBL,(-1L),(-4L),0x2EC08D5CL,(-4L)},{0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L,0x55F2F5C6L,0x7DB4C886L,0L},{0xC56AF300L,0x2EC08D5CL,0xC56AF300L,(-1L),0x1E34A7E3L,(-1L),0xC56AF300L,0x2EC08D5CL,0xC56AF300L},{0L,0x7DB4C886L,0x55F2F5C6L,0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L},{(-4L),0x2EC08D5CL,(-4L),(-1L),0x6BC5BDBBL,(-1L),(-4L),0x2EC08D5CL,(-4L)}},{{0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L,0x55F2F5C6L,0x7DB4C886L,0L},{0xC56AF300L,0x2EC08D5CL,0xC56AF300L,(-1L),0x1E34A7E3L,(-1L),0xC56AF300L,0x2EC08D5CL,0xC56AF300L},{0L,0x7DB4C886L,0x55F2F5C6L,0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L},{(-4L),0x2EC08D5CL,(-4L),(-1L),0x6BC5BDBBL,(-1L),(-4L),0x2EC08D5CL,(-4L)},{0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L,0x55F2F5C6L,0x7DB4C886L,0L}},{{0xC56AF300L,0x2EC08D5CL,0xC56AF300L,(-1L),0x1E34A7E3L,(-1L),0xC56AF300L,0x2EC08D5CL,0xC56AF300L},{0L,0x7DB4C886L,0x55F2F5C6L,0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L},{(-4L),0x2EC08D5CL,(-4L),(-1L),0x6BC5BDBBL,(-1L),(-4L),0x2EC08D5CL,(-4L)},{0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L,0x55F2F5C6L,0x7DB4C886L,0L},{0xC56AF300L,0x2EC08D5CL,0xC56AF300L,(-1L),0x1E34A7E3L,(-1L),0xC56AF300L,0x2EC08D5CL,0xC56AF300L}},{{0L,0x7DB4C886L,0x55F2F5C6L,0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L},{(-4L),0x2EC08D5CL,(-4L),(-1L),0x6BC5BDBBL,(-1L),(-4L),0x2EC08D5CL,(-4L)},{0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L,0x55F2F5C6L,0x7DB4C886L,0L},{0xC56AF300L,0x2EC08D5CL,0xC56AF300L,(-1L),0x1E34A7E3L,(-1L),0xC56AF300L,0x2EC08D5CL,0xC56AF300L},{0L,0x7DB4C886L,0x55F2F5C6L,0L,0xBCEE8DF8L,0x55F2F5C6L,0x55F2F5C6L,0xBCEE8DF8L,0L}}};
                        int i, j, k;
                        if (l_2105[l_2125])
                            break;
                        (*g_936) = l_2330;
                        (*g_185) = ((safe_lshift_func_int16_t_s_u((p_6 , p_6), 5)) != ((safe_div_func_int16_t_s_s(0xEAF6L, ((*l_2182) &= (safe_mod_func_int64_t_s_s((((safe_add_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_s((p_7 < 0xA06C4D78A4ADEA55LL), ((safe_rshift_func_int8_t_s_u(((*l_2322) &= (((+g_2344) <= (((p_6 , l_2345) == l_2346) || 1UL)) <= p_6)), l_2347)) < (*l_2318)))) , p_6), l_2327)) , l_2295.f1) || 0L), l_2347))))) >= l_2311));
                        l_2356--;
                    }
                    (*l_2319) |= (p_6 , 0x3B8406B6L);
                    if ((safe_sub_func_int64_t_s_s((0x93L < p_6), ((**g_1571) = (l_2308[4][2][3] || (l_2105[l_2125] = (((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(((-1L) > (((safe_lshift_func_int8_t_s_s(((*g_363) == (safe_unary_minus_func_uint32_t_u(((*g_480) = (((((*l_2322) > (0L & ((!(65535UL < (safe_add_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((l_2375 != l_2056), p_7)), 8L)))) ^ p_7))) , p_7) | 0UL) & 1L))))), 5)) | p_7) <= p_6)), 1UL)), 1)), g_2376)) , p_7) == l_2377)))))))
                    { /* block id: 1135 */
                        int32_t l_2379 = 0x045B80AEL;
                        int32_t l_2380 = (-4L);
                        int32_t l_2381 = 0xD79E98B8L;
                        int32_t l_2382 = 0xA5D6E7B4L;
                        int32_t l_2383 = 6L;
                        int32_t l_2384 = 0xA86C76ADL;
                        int32_t l_2385 = 0L;
                        int32_t l_2386[9][3] = {{1L,0L,1L},{0x743C9B88L,0xD8F853E9L,0x743C9B88L},{1L,0L,1L},{0x743C9B88L,0xD8F853E9L,0x743C9B88L},{(-1L),1L,(-1L)},{0xF420F41BL,0x743C9B88L,0xF420F41BL},{(-1L),1L,(-1L)},{0xF420F41BL,0x743C9B88L,0xF420F41BL},{(-1L),1L,(-1L)}};
                        int32_t l_2388[4][4][7] = {{{5L,0xB4FE453BL,1L,6L,4L,0x663D6987L,0x34793C0BL},{(-10L),0x6FE887E1L,0x59A42EA0L,0L,4L,8L,(-1L)},{0xA3A10FEBL,0L,0xCFD2FA0CL,(-7L),4L,(-7L),0xCFD2FA0CL},{0x70C54345L,0x70C54345L,(-1L),8L,4L,0L,0x59A42EA0L}},{{0L,0xA3A10FEBL,0x34793C0BL,0x663D6987L,4L,6L,1L},{0x6FE887E1L,(-10L),0L,(-1L),4L,0x8C3A4854L,2L},{0xB4FE453BL,5L,2L,0x8C3A4854L,4L,(-1L),0L},{5L,0xB4FE453BL,1L,6L,4L,0x663D6987L,0x34793C0BL}},{{(-10L),0x6FE887E1L,0x59A42EA0L,0L,4L,8L,(-1L)},{0xA3A10FEBL,0L,0xCFD2FA0CL,(-7L),4L,(-7L),0xCFD2FA0CL},{0x70C54345L,0x70C54345L,(-1L),8L,4L,0L,0x59A42EA0L},{0L,0xA3A10FEBL,0x34793C0BL,0x663D6987L,4L,6L,1L}},{{0x6FE887E1L,(-10L),0L,(-1L),4L,0x8C3A4854L,2L},{0xB4FE453BL,5L,2L,0x8C3A4854L,4L,(-1L),0L},{5L,0xB4FE453BL,1L,6L,4L,0x663D6987L,0x34793C0BL},{(-10L),0x6FE887E1L,0x59A42EA0L,0L,4L,8L,(-1L)}}};
                        int i, j, k;
                        (*g_1256) = 0x0.Cp-1;
                        if (p_6)
                            continue;
                        if (p_6)
                            continue;
                        l_2391--;
                    }
                    else
                    { /* block id: 1140 */
                        (*l_2319) = l_2394;
                    }
                }
            }
        }
        ++l_2397;
    }
    return l_2400;
}


/* ------------------------------------------ */
/* 
 * reads : g_1202 g_2013 g_293 g_2023 g_186 g_185 g_54
 * writes: g_1202 g_293
 */
static struct S0  func_8(int64_t  p_9)
{ /* block id: 4 */
    uint8_t *l_35 = &g_36;
    const int32_t l_48[3][3][10] = {{{0L,0x511F0B9FL,0xB1F8487BL,0L,0x592840CCL,0xAB171C25L,0xCC72FDDDL,(-1L),(-1L),0xC73B0172L},{(-1L),0x2870B1B9L,0x592840CCL,0x97BDE515L,0xE33A9025L,0x97BDE515L,0x592840CCL,0x2870B1B9L,(-1L),(-1L)},{0xC73B0172L,0xAEE34D1CL,0x4BF7A832L,(-1L),0x830AE734L,(-1L),0x5477A75EL,(-1L),0xB1F8487BL,(-1L)}},{{(-1L),0x0B1C5ADFL,(-1L),(-1L),0xAEE34D1CL,0x830AE734L,0xAB171C25L,0x8CA40452L,(-1L),(-1L)},{0xCC72FDDDL,(-1L),1L,0x97BDE515L,0xE525B78CL,0xE525B78CL,0x97BDE515L,1L,(-1L),0xCC72FDDDL},{0x830AE734L,1L,0x2870B1B9L,0L,0x964069A3L,0L,(-1L),0xB1F8487BL,0xAEE34D1CL,0xE33A9025L}},{{(-1L),(-1L),0x8CA40452L,1L,0xB0C3E5F8L,0xC73B0172L,0x97BDE515L,(-1L),0x8CA40452L,(-1L)},{0xB0C3E5F8L,0x511F0B9FL,1L,0xE33A9025L,0xB1F8487BL,0x5477A75EL,0xB1F8487BL,0xE33A9025L,1L,0x511F0B9FL},{0xAB171C25L,0x5477A75EL,0xE525B78CL,0x592840CCL,(-1L),0x8CA40452L,0xE33A9025L,0xAEE34D1CL,0xB1F8487BL,(-1L)}}};
    int32_t l_2016 = 1L;
    int64_t l_2024 = 0L;
    struct S0 l_2025 = {-18886,0x75DC10C0L};
    int i, j, k;
    for (p_9 = 0; (p_9 <= 5); p_9 += 1)
    { /* block id: 7 */
        uint8_t *l_24 = &g_25;
        const struct S0 l_34 = {32294,0x00B6FB96L};
        int32_t *l_2014[3];
        uint64_t l_2015 = 0x6E460F19B6AE7700LL;
        int i;
        for (i = 0; i < 3; i++)
            l_2014[i] = &g_1139;
    }
    for (g_1202 = (-27); (g_1202 < 15); g_1202 = safe_add_func_uint32_t_u_u(g_1202, 3))
    { /* block id: 977 */
        if (l_48[1][1][7])
            break;
    }
    (*g_2023) = (*g_2013);
    l_2024 |= (**g_186);
    return l_2025;
}


/* ------------------------------------------ */
/* 
 * reads : g_995 g_77 g_36 g_847 g_1535 g_185 g_50 g_54 g_899 g_25 g_1368 g_144 g_1570 g_1000 g_801 g_335 g_51 g_61.f0 g_68 g_293 g_1027 g_1636 g_290 g_291 g_61.f1 g_1571 g_1572 g_902 g_1475 g_165.f1 g_863 g_1507 g_1508 g_289 g_1682 g_2 g_255 g_2013
 * writes: g_1507 g_902 g_143 g_36 g_1000 g_51 g_165 g_1535 g_50 g_54 g_25 g_293 g_899 g_883 g_68 g_144 g_1439 g_289 g_1687 g_291 g_255
 */
static uint64_t  func_14(uint32_t  p_15)
{ /* block id: 703 */
    int64_t l_1495 = (-8L);
    int32_t l_1496 = 7L;
    float *l_1504 = &g_289;
    struct S0 l_1533 = {16916,-1L};
    uint64_t *l_1543 = &g_143;
    uint64_t ** const l_1542[6] = {&l_1543,&l_1543,&l_1543,&l_1543,&l_1543,&l_1543};
    uint64_t **l_1545 = &l_1543;
    uint64_t ***l_1544 = &l_1545;
    uint8_t * const *l_1546 = &g_77;
    uint8_t *****l_1551[3];
    int32_t *l_1553 = &l_1533.f1;
    int32_t *l_1605 = &g_61.f1;
    uint64_t l_1652 = 18446744073709551612UL;
    int32_t l_1688 = 0L;
    uint16_t l_1698 = 65531UL;
    int32_t l_1807 = 0x3228FCBFL;
    int64_t l_1842 = 0xE9B6D8502C2B92E1LL;
    struct S0 ** const *l_1939 = &g_204;
    struct S0 ** const **l_1938 = &l_1939;
    struct S0 ** const ***l_1937 = &l_1938;
    struct S0 l_1976 = {-19023,0L};
    int i;
    for (i = 0; i < 3; i++)
        l_1551[i] = (void*)0;
    if ((l_1496 = l_1495))
    { /* block id: 705 */
        uint64_t l_1499[10][6] = {{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL},{0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL,0xAE92FD00501CE7B3LL,0xAE92FD00501CE7B3LL,1UL}};
        const float *l_1506 = &g_1494;
        const float **l_1505[6] = {(void*)0,(void*)0,&l_1506,(void*)0,(void*)0,&l_1506};
        uint32_t l_1523 = 0x58E04206L;
        uint64_t *l_1524 = &g_902;
        uint64_t *l_1525 = &g_143;
        int8_t *l_1526[7] = {&g_255,&g_255,&g_255,&g_255,&g_255,&g_255,&g_255};
        int32_t l_1527 = 1L;
        uint16_t ****l_1528 = &g_936;
        uint64_t l_1529[6][5][2] = {{{0UL,18446744073709551610UL},{0x7197E1246B51E57DLL,0UL},{7UL,7UL},{7UL,0UL},{0x7197E1246B51E57DLL,18446744073709551610UL}},{{0UL,18446744073709551610UL},{0x7197E1246B51E57DLL,0UL},{7UL,7UL},{7UL,0UL},{0x7197E1246B51E57DLL,18446744073709551610UL}},{{0UL,18446744073709551610UL},{0x7197E1246B51E57DLL,0UL},{7UL,7UL},{7UL,0UL},{0x7197E1246B51E57DLL,18446744073709551610UL}},{{0UL,18446744073709551610UL},{0x7197E1246B51E57DLL,0UL},{7UL,7UL},{7UL,0UL},{0x7197E1246B51E57DLL,18446744073709551610UL}},{{0UL,18446744073709551610UL},{0x7197E1246B51E57DLL,0UL},{7UL,7UL},{7UL,0UL},{0x7197E1246B51E57DLL,18446744073709551610UL}},{{0UL,18446744073709551610UL},{0x7197E1246B51E57DLL,0UL},{7UL,7UL},{7UL,0UL},{0x7197E1246B51E57DLL,18446744073709551610UL}}};
        int32_t *l_1530 = &g_1470;
        int i, j, k;
        l_1530 = ((safe_add_func_float_f_f(l_1499[4][2], ((((safe_div_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_s(((l_1504 == (g_1507 = l_1504)) <= l_1496), 2)) > (safe_div_func_uint32_t_u_u(((safe_mul_func_uint16_t_u_u(((safe_add_func_int8_t_s_s((p_15 != (((p_15 <= ((((safe_lshift_func_uint16_t_u_s(p_15, (safe_sub_func_uint64_t_u_u(((-1L) < ((((safe_add_func_int8_t_s_s((((l_1527 = (((*l_1525) = ((*l_1524) = (safe_mod_func_uint16_t_u_u(l_1523, 0xC809L)))) > 1L)) == l_1499[4][2]) , g_995), l_1499[4][1])) , (void*)0) == l_1528) , p_15)), l_1529[5][2][1])))) , (void*)0) != (void*)0) || p_15)) <= p_15) , 0x637093E5L)), (*g_77))) | l_1495), g_847[0][2][1])) <= l_1499[4][2]), l_1529[5][2][1]))), (-1L))) || 0xCCE28787L) ^ (-10L)) , l_1496))) , &l_1496);
        for (g_36 = 0; g_36 < 4; g_36 += 1)
        {
            for (g_1000 = 0; g_1000 < 3; g_1000 += 1)
            {
                for (l_1495 = 0; l_1495 < 9; l_1495 += 1)
                {
                    g_51[g_36][g_1000][l_1495] = 0xFC75C4FEL;
                }
            }
        }
    }
    else
    { /* block id: 712 */
        struct S0 l_1531 = {-29916,0xA16EC76CL};
        struct S0 *l_1532 = &g_165;
        uint8_t ****l_1534 = (void*)0;
        const uint8_t *****l_1539 = &g_1535;
        (*g_185) ^= ((l_1533 = ((*l_1532) = l_1531)) , (l_1534 == ((*l_1539) = g_1535)));
    }
    if ((safe_lshift_func_uint8_t_u_u((((l_1542[2] != ((*l_1544) = &l_1543)) | g_899) || 18446744073709551615UL), ((l_1546 != (void*)0) != (((((safe_add_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((l_1533.f0 | (((l_1551[2] = l_1551[2]) != &g_1535) && 4UL)), 1)), 0UL)) & 255UL) , &l_1542[0]) == &l_1542[3]) < l_1496)))))
    { /* block id: 720 */
        int32_t **l_1552[8][6] = {{(void*)0,&g_293,&g_293,&g_293,&g_293,&g_293},{(void*)0,(void*)0,&g_293,&g_293,&g_293,&g_293},{(void*)0,&g_293,&g_293,&g_293,&g_293,&g_293},{&g_293,(void*)0,&g_293,&g_293,(void*)0,&g_293},{&g_293,&g_293,(void*)0,(void*)0,&g_293,&g_293},{(void*)0,&g_293,&g_293,&g_293,&g_293,&g_293},{&g_293,(void*)0,&g_293,(void*)0,&g_293,&g_293},{&g_293,(void*)0,(void*)0,&g_293,&g_293,&g_293}};
        struct S0 l_1567 = {1541,0xE329A53CL};
        float l_1602 = 0x9.1p-1;
        uint8_t l_1603 = 0x13L;
        int32_t l_1642[2][5] = {{0x497888E2L,(-10L),(-10L),0x497888E2L,(-10L)},{0x497888E2L,0x497888E2L,0x83F8C4B6L,0x497888E2L,0x497888E2L}};
        float *l_1707 = &g_1687;
        uint8_t **l_1720 = &g_77;
        uint8_t ***l_1719 = &l_1720;
        uint8_t ****l_1718 = &l_1719;
        uint8_t **** const *l_1717[10][7] = {{&l_1718,(void*)0,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{(void*)0,(void*)0,(void*)0,&l_1718,&l_1718,&l_1718,(void*)0},{(void*)0,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,(void*)0,&l_1718,(void*)0,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,(void*)0,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,(void*)0,&l_1718,&l_1718,&l_1718,&l_1718,&l_1718}};
        uint8_t *****l_1721[5][10][5] = {{{(void*)0,&l_1718,(void*)0,&l_1718,&l_1718},{(void*)0,&l_1718,(void*)0,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,(void*)0,&l_1718,&l_1718,&l_1718},{(void*)0,&l_1718,&l_1718,(void*)0,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,(void*)0,(void*)0,&l_1718}},{{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,(void*)0,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,(void*)0,(void*)0},{&l_1718,&l_1718,&l_1718,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,(void*)0,&l_1718},{&l_1718,&l_1718,(void*)0,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718}},{{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{(void*)0,&l_1718,&l_1718,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{(void*)0,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,(void*)0,&l_1718,&l_1718},{(void*)0,(void*)0,&l_1718,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{(void*)0,(void*)0,&l_1718,&l_1718,(void*)0}},{{(void*)0,(void*)0,(void*)0,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,(void*)0,&l_1718,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,(void*)0,&l_1718},{(void*)0,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,(void*)0,&l_1718,&l_1718,&l_1718},{&l_1718,&l_1718,(void*)0,&l_1718,&l_1718},{&l_1718,(void*)0,(void*)0,&l_1718,&l_1718},{&l_1718,&l_1718,(void*)0,(void*)0,(void*)0}},{{&l_1718,&l_1718,(void*)0,&l_1718,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,(void*)0},{&l_1718,(void*)0,(void*)0,&l_1718,&l_1718},{(void*)0,&l_1718,(void*)0,(void*)0,&l_1718},{&l_1718,&l_1718,&l_1718,&l_1718,&l_1718},{&l_1718,(void*)0,&l_1718,(void*)0,&l_1718},{&l_1718,(void*)0,&l_1718,&l_1718,&l_1718},{&l_1718,(void*)0,(void*)0,&l_1718,&l_1718},{&l_1718,(void*)0,&l_1718,&l_1718,&l_1718},{(void*)0,(void*)0,(void*)0,&l_1718,(void*)0}}};
        int64_t l_1722 = 0x29E43DA57DB9CBDFLL;
        float *l_1723 = &l_1602;
        float *l_1725[1];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1725[i] = &g_291;
        l_1553 = (void*)0;
        for (g_25 = 0; (g_25 <= 8); g_25 += 1)
        { /* block id: 724 */
            int32_t *l_1554 = &g_144[2][4][0];
            struct S0 l_1608 = {-41056,-1L};
            uint16_t l_1689 = 0UL;
            (*g_1368) = l_1554;
            l_1553 = ((p_15 & ((safe_add_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(((*l_1554) || (!0xE084A500L)), 14)), ((safe_lshift_func_uint8_t_u_u((+(-5L)), 6)) && (l_1567 , ((safe_add_func_int16_t_s_s((g_1570 != &l_1542[5]), ((&g_185 == (void*)0) != p_15))) < 0xDC723B4AL))))) == 0x9DL)) , (void*)0);
            for (g_1000 = 0; (g_1000 <= 8); g_1000 += 1)
            { /* block id: 729 */
                uint32_t l_1589 = 0xB1D78015L;
                int32_t l_1639 = 0L;
                int32_t l_1643 = (-1L);
                int32_t l_1647 = 0xD03BF474L;
                int32_t l_1651 = 0x81607382L;
                int64_t l_1686 = (-5L);
                struct S0 *l_1692 = &l_1533;
                for (g_899 = 0; (g_899 <= 8); g_899 += 1)
                { /* block id: 732 */
                    int8_t l_1584 = 0xDEL;
                    int8_t *l_1585 = (void*)0;
                    int8_t *l_1586 = &g_883;
                    uint32_t *l_1600 = (void*)0;
                    int32_t l_1601 = 0L;
                    uint32_t *l_1604[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1604[i] = &g_68;
                    (*g_293) = (!(safe_mod_func_int8_t_s_s(g_801[3], (((g_68 &= (safe_mul_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((*l_1554), (0x2EL <= (safe_add_func_uint8_t_u_u(((((*l_1586) = l_1584) != l_1584) , (safe_mod_func_uint64_t_u_u((l_1589 | (++(*g_77))), (safe_div_func_uint8_t_u_u((((*l_1554) && (((((safe_mod_func_uint32_t_u_u((((safe_rshift_func_int8_t_s_s(p_15, (((safe_sub_func_uint32_t_u_u((l_1601 = ((void*)0 == &g_996[2])), p_15)) == l_1589) ^ l_1589))) , l_1589) , p_15), 0xC409AD9CL)) && p_15) | g_335[0][3][0]) >= p_15) || g_51[0][0][3])) | p_15), 4UL))))), 250UL))))), 8)) | g_61.f0), l_1603))) < (*l_1554)) | p_15))));
                    l_1553 = l_1605;
                    if ((*l_1554))
                        continue;
                }
                if ((safe_mul_func_int8_t_s_s(0x56L, p_15)))
                { /* block id: 741 */
                    struct S0 *l_1609 = &l_1567;
                    (*l_1609) = l_1608;
                }
                else
                { /* block id: 743 */
                    int32_t l_1650 = 2L;
                    int64_t *l_1668[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    struct S0 l_1670[6][10][4] = {{{{-35924,0xFD7E36C3L},{-19609,-1L},{10919,-1L},{-16751,0x3AC74F42L}},{{29746,1L},{34377,-1L},{29746,1L},{-16751,0x3AC74F42L}},{{10919,-1L},{-19609,-1L},{-35924,0xFD7E36C3L},{-44593,-1L}},{{-44454,0L},{4422,0x6BB2460BL},{-19609,-1L},{-19609,-1L}},{{29501,0x439D4771L},{29501,0x439D4771L},{-19609,-1L},{29746,1L}},{{-44454,0L},{10919,-1L},{34377,-1L},{-19609,-1L}},{{-44454,0L},{34377,-1L},{4422,0x6BB2460BL},{34377,-1L}},{{4422,0x6BB2460BL},{34377,-1L},{-44454,0L},{-19609,-1L}},{{34377,-1L},{10919,-1L},{29746,1L},{4422,0x6BB2460BL}},{{29501,0x439D4771L},{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L}}},{{{29501,0x439D4771L},{-19609,-1L},{29746,1L},{-7229,0x803795E7L}},{{34377,-1L},{29501,0x439D4771L},{-44454,0L},{-35924,0xFD7E36C3L}},{{4422,0x6BB2460BL},{-44593,-1L},{4422,0x6BB2460BL},{-35924,0xFD7E36C3L}},{{-44454,0L},{29501,0x439D4771L},{34377,-1L},{-7229,0x803795E7L}},{{29746,1L},{-19609,-1L},{29501,0x439D4771L},{29501,0x439D4771L}},{{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L},{4422,0x6BB2460BL}},{{29746,1L},{10919,-1L},{34377,-1L},{-19609,-1L}},{{-44454,0L},{34377,-1L},{4422,0x6BB2460BL},{34377,-1L}},{{4422,0x6BB2460BL},{34377,-1L},{-44454,0L},{-19609,-1L}},{{34377,-1L},{10919,-1L},{29746,1L},{4422,0x6BB2460BL}}},{{{29501,0x439D4771L},{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L}},{{29501,0x439D4771L},{-19609,-1L},{29746,1L},{-7229,0x803795E7L}},{{34377,-1L},{29501,0x439D4771L},{-44454,0L},{-35924,0xFD7E36C3L}},{{4422,0x6BB2460BL},{-44593,-1L},{4422,0x6BB2460BL},{-35924,0xFD7E36C3L}},{{-44454,0L},{29501,0x439D4771L},{34377,-1L},{-7229,0x803795E7L}},{{29746,1L},{-19609,-1L},{29501,0x439D4771L},{29501,0x439D4771L}},{{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L},{4422,0x6BB2460BL}},{{29746,1L},{10919,-1L},{34377,-1L},{-19609,-1L}},{{-44454,0L},{34377,-1L},{4422,0x6BB2460BL},{34377,-1L}},{{4422,0x6BB2460BL},{34377,-1L},{-44454,0L},{-19609,-1L}}},{{{34377,-1L},{10919,-1L},{29746,1L},{4422,0x6BB2460BL}},{{29501,0x439D4771L},{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L}},{{29501,0x439D4771L},{-19609,-1L},{29746,1L},{-7229,0x803795E7L}},{{34377,-1L},{29501,0x439D4771L},{-44454,0L},{-35924,0xFD7E36C3L}},{{4422,0x6BB2460BL},{-44593,-1L},{4422,0x6BB2460BL},{-35924,0xFD7E36C3L}},{{-44454,0L},{29501,0x439D4771L},{34377,-1L},{-7229,0x803795E7L}},{{29746,1L},{-19609,-1L},{29501,0x439D4771L},{29501,0x439D4771L}},{{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L},{4422,0x6BB2460BL}},{{29746,1L},{10919,-1L},{34377,-1L},{-19609,-1L}},{{-44454,0L},{34377,-1L},{4422,0x6BB2460BL},{34377,-1L}}},{{{4422,0x6BB2460BL},{34377,-1L},{-44454,0L},{-19609,-1L}},{{34377,-1L},{10919,-1L},{29746,1L},{4422,0x6BB2460BL}},{{29501,0x439D4771L},{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L}},{{29501,0x439D4771L},{-19609,-1L},{29746,1L},{-7229,0x803795E7L}},{{34377,-1L},{29501,0x439D4771L},{-44454,0L},{-35924,0xFD7E36C3L}},{{4422,0x6BB2460BL},{-44593,-1L},{4422,0x6BB2460BL},{-35924,0xFD7E36C3L}},{{-44454,0L},{29501,0x439D4771L},{34377,-1L},{-7229,0x803795E7L}},{{29746,1L},{-19609,-1L},{29501,0x439D4771L},{29501,0x439D4771L}},{{-16751,0x3AC74F42L},{-16751,0x3AC74F42L},{29501,0x439D4771L},{4422,0x6BB2460BL}},{{29746,1L},{10919,-1L},{34377,-1L},{-19609,-1L}}},{{{-44454,0L},{34377,-1L},{4422,0x6BB2460BL},{34377,-1L}},{{4422,0x6BB2460BL},{34377,-1L},{-44454,0L},{-19609,-1L}},{{34377,-1L},{10919,-1L},{29746,1L},{4422,0x6BB2460BL}},{{-16751,0x3AC74F42L},{-35924,0xFD7E36C3L},{-35924,0xFD7E36C3L},{-16751,0x3AC74F42L}},{{-16751,0x3AC74F42L},{29501,0x439D4771L},{4422,0x6BB2460BL},{10919,-1L}},{{-44593,-1L},{-16751,0x3AC74F42L},{29746,1L},{34377,-1L}},{{-19609,-1L},{-7229,0x803795E7L},{-19609,-1L},{34377,-1L}},{{29746,1L},{-16751,0x3AC74F42L},{-44593,-1L},{10919,-1L}},{{4422,0x6BB2460BL},{29501,0x439D4771L},{-16751,0x3AC74F42L},{-16751,0x3AC74F42L}},{{-35924,0xFD7E36C3L},{-35924,0xFD7E36C3L},{-16751,0x3AC74F42L},{-19609,-1L}}}};
                    uint32_t l_1678 = 0xFA870ADDL;
                    int i, j, k;
                    if ((safe_div_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(p_15, 7)), ((*g_77) = (safe_mul_func_int16_t_s_s(g_899, (safe_sub_func_int64_t_s_s(((l_1589 || l_1589) >= ((*g_77) <= (0xC60844F5L == ((safe_div_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(l_1589, (+(((((safe_sub_func_uint8_t_u_u((safe_add_func_int16_t_s_s(g_1027[0][3][0], (*l_1554))), 0x0EL)) & p_15) > l_1589) && p_15) || (*l_1554))))), 0x62DCL)) != 4294967286UL)))), 0xE09979DC11BDD1BDLL))))))))
                    { /* block id: 745 */
                        uint32_t l_1629[5] = {0xB3E32BC2L,0xB3E32BC2L,0xB3E32BC2L,0xB3E32BC2L,0xB3E32BC2L};
                        int i;
                        l_1639 |= (0L || (l_1629[0] , ((safe_lshift_func_uint8_t_u_s(((((((safe_add_func_int32_t_s_s(((*g_293) = (*g_293)), 0xCD670DFAL)) | (((***l_1544) = g_1636) == ((((safe_add_func_float_f_f((-0x8.8p+1), ((-0x1.6p+1) == ((*g_290) >= (0x5.8p+1 < (*l_1605)))))) > p_15) , g_1027[4][3][2]) == p_15))) , 0L) , p_15) && p_15) | p_15), 3)) >= g_61.f1)));
                    }
                    else
                    { /* block id: 749 */
                        int16_t l_1644 = 0xBDFBL;
                        int32_t l_1645 = 0x8FF56AA2L;
                        int32_t l_1646 = (-7L);
                        int32_t l_1648 = 0xF9E6C136L;
                        int32_t l_1649 = 2L;
                        (*g_293) = (safe_add_func_int8_t_s_s(l_1642[1][1], p_15));
                        l_1652++;
                        return (**g_1571);
                    }
                    for (l_1495 = 0; (l_1495 <= 8); l_1495 += 1)
                    { /* block id: 756 */
                        return (**g_1571);
                    }
                    if ((safe_div_func_int16_t_s_s(p_15, (safe_mod_func_int16_t_s_s(((g_1475 , (safe_lshift_func_int16_t_s_u((-1L), 7))) == (safe_add_func_int64_t_s_s((safe_unary_minus_func_uint16_t_u(((((l_1643 &= (!((l_1647 |= ((l_1651 ^= (((((*l_1554) = ((safe_mul_func_int16_t_s_s(1L, (*l_1605))) != ((p_15 == ((safe_unary_minus_func_int32_t_s(((((g_335[0][9][0] , l_1567) , 0xB1D566B9D155564ALL) , (*l_1554)) <= l_1639))) != g_165.f1)) >= 0xBE8FB4E81588BC75LL))) && l_1650) , p_15) <= 65526UL)) | (-1L))) == p_15))) , (*l_1554)) , p_15) >= 0xF3ECL))), 0x46DE321023CD9261LL))), l_1589)))))
                    { /* block id: 763 */
                        (*l_1554) ^= (6UL > (-1L));
                        if ((*g_293))
                            break;
                    }
                    else
                    { /* block id: 766 */
                        struct S0 *l_1669 = &g_165;
                        float *l_1679 = &l_1602;
                        int32_t l_1683 = 0x27C3D64AL;
                        l_1670[3][9][2] = ((*l_1669) = l_1567);
                        l_1683 = (safe_sub_func_float_f_f((0x2.Ap-1 == (safe_div_func_float_f_f(((*g_863) <= (*g_1507)), 0x7.6p-1))), ((g_1439 = ((safe_div_func_float_f_f((p_15 , 0x4.980A85p-35), ((*l_1679) = (l_1639 != (+l_1678))))) > (safe_div_func_float_f_f(g_1682[3][6], p_15)))) != 0x5.CF07C9p+30)));
                        (*g_185) &= (safe_sub_func_uint8_t_u_u(p_15, p_15));
                    }
                }
                ++l_1689;
                (*l_1692) = l_1533;
            }
        }
        (*g_290) = (+(safe_sub_func_float_f_f((((((*l_1504) = (safe_add_func_float_f_f((l_1698 == (*g_863)), (*l_1605)))) < (safe_sub_func_float_f_f((l_1533 , (*l_1605)), (safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_div_func_float_f_f((((*l_1707) = 0x8.B15665p-36) != (+(safe_mul_func_float_f_f(((*l_1723) = (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_sub_func_float_f_f(((((g_2 , l_1717[0][0]) != l_1721[3][1][2]) > p_15) > (*l_1605)), l_1722)), 0x4.Ep-1)), p_15))), 0x8.8D7EE0p+62)))), (*l_1605))), p_15)), p_15))))) != (*l_1605)) > (-0x1.Dp-1)), 0xF.102C96p+2)));
    }
    else
    { /* block id: 783 */
        uint64_t l_1748 = 18446744073709551615UL;
        int32_t l_1752 = 0L;
        int32_t l_1754 = 0x2BE281DCL;
        int32_t l_1757 = 1L;
        int32_t l_1759 = 0xC159F886L;
        int32_t *l_1764 = &l_1533.f1;
        struct S0 ***l_1768[5] = {&g_204,&g_204,&g_204,&g_204,&g_204};
        uint32_t ***l_1771 = &g_739;
        int8_t *l_1797 = &g_883;
        uint16_t ****l_1873 = (void*)0;
        uint16_t *****l_1872 = &l_1873;
        uint64_t ****l_1972[9] = {(void*)0,&l_1544,(void*)0,&l_1544,(void*)0,&l_1544,(void*)0,&l_1544,(void*)0};
        const uint32_t l_2007 = 8UL;
        int32_t **l_2009 = &l_1764;
        int i;
        for (g_255 = 0; (g_255 <= 3); g_255 += 1)
        { /* block id: 786 */
            int64_t l_1734 = (-4L);
            int32_t l_1741 = 3L;
            int32_t l_1746 = 5L;
            int32_t l_1747[8][4][8] = {{{0x37390B74L,0xFBD9CB6AL,0xB7CC03C4L,0x74BB12DEL,0L,0L,0x74BB12DEL,0xB7CC03C4L},{0x04EE29C5L,0x04EE29C5L,1L,0xDDB79F38L,0xF07D35A2L,8L,0x28355AC6L,0x04EE29C5L},{0xFBD9CB6AL,0x37390B74L,0x32A2F85FL,0L,0x28355AC6L,0x32A2F85FL,0xB7CC03C4L,0x04EE29C5L},{0x37390B74L,0xBEC7A262L,0xFBD9CB6AL,0xDDB79F38L,0xFBD9CB6AL,0xBEC7A262L,0x37390B74L,0xB7CC03C4L}},{{(-7L),0xDDB79F38L,0x8080044BL,0x74BB12DEL,0xDDB79F38L,0L,0x28355AC6L,(-7L)},{(-5L),(-7L),0L,0xBEC7A262L,0xDDB79F38L,0x32A2F85FL,0x32A2F85FL,0xDDB79F38L},{(-7L),0xB7CC03C4L,0xB7CC03C4L,(-7L),0xFBD9CB6AL,(-5L),0x04EE29C5L,0xBEC7A262L},{0x37390B74L,0x04EE29C5L,1L,0x74BB12DEL,0x28355AC6L,1L,0x74BB12DEL,0x37390B74L}},{{0xFBD9CB6AL,0x04EE29C5L,0L,(-5L),0xF07D35A2L,(-5L),0L,0x04EE29C5L},{0x04EE29C5L,0xB7CC03C4L,0x32A2F85FL,0x28355AC6L,0L,0x32A2F85FL,0x37390B74L,0xFBD9CB6AL},{0x37390B74L,(-7L),(-9L),0xDDB79F38L,0x04EE29C5L,0L,0x37390B74L,0x37390B74L},{0xBEC7A262L,0xDDB79F38L,0x32A2F85FL,0x32A2F85FL,0xDDB79F38L,0xBEC7A262L,0L,(-7L)}},{{0xDDB79F38L,0xBEC7A262L,0L,(-7L),(-5L),0x32A2F85FL,0x74BB12DEL,(-5L)},{(-7L),0x37390B74L,1L,(-7L),0x04EE29C5L,8L,0x04EE29C5L,(-7L)},{0xB7CC03C4L,0x04EE29C5L,0xB7CC03C4L,0x32A2F85FL,0x28355AC6L,0L,0x32A2F85FL,0x37390B74L},{0x04EE29C5L,0xFBD9CB6AL,0L,0xDDB79F38L,0xDA692107L,(-5L),0x28355AC6L,0xFBD9CB6AL}},{{0x04EE29C5L,0x37390B74L,0x8080044BL,0x28355AC6L,0x28355AC6L,0x8080044BL,0x37390B74L,0x04EE29C5L},{0xB7CC03C4L,(-7L),0xFBD9CB6AL,(-5L),0x04EE29C5L,0xBEC7A262L,0xB7CC03C4L,(-5L)},{0xB7CC03C4L,0x8080044BL,1L,0L,0x8080044BL,1L,0xFBD9CB6AL,1L},{0x32A2F85FL,0xB7CC03C4L,0x04EE29C5L,0xB7CC03C4L,0x32A2F85FL,0x28355AC6L,0L,0x32A2F85FL}},{{1L,(-5L),8L,1L,0xDA692107L,0x8080044BL,6L,0xB7CC03C4L},{(-5L),6L,8L,0L,(-9L),(-9L),0L,8L},{0xDA692107L,0xDA692107L,0x04EE29C5L,0x32A2F85FL,0xBEC7A262L,0x74BB12DEL,0xFBD9CB6AL,0xDA692107L},{6L,(-5L),1L,(-9L),0xFBD9CB6AL,1L,8L,0xDA692107L}},{{(-5L),1L,6L,0x32A2F85FL,6L,1L,(-5L),8L},{0xB7CC03C4L,0x32A2F85FL,0x28355AC6L,0L,0x32A2F85FL,0x37390B74L,0xFBD9CB6AL,0xB7CC03C4L},{0x8080044BL,0xB7CC03C4L,(-9L),1L,0x32A2F85FL,1L,1L,0x32A2F85FL},{0xB7CC03C4L,8L,8L,0xB7CC03C4L,6L,0x8080044BL,0xDA692107L,1L}},{{(-5L),0xDA692107L,0xDDB79F38L,0L,0xFBD9CB6AL,0x04EE29C5L,0L,(-5L)},{6L,0xDA692107L,(-9L),0x8080044BL,0xBEC7A262L,0x8080044BL,(-9L),0xDA692107L},{0xDA692107L,8L,1L,0xFBD9CB6AL,(-9L),1L,(-5L),6L},{(-5L),0xB7CC03C4L,0xF07D35A2L,0x32A2F85FL,0xDA692107L,0x37390B74L,(-5L),(-5L)}}};
            int32_t l_1753 = 1L;
            uint32_t *l_1781 = &g_1731;
            struct S0 l_1790 = {21227,0x214B160FL};
            uint32_t l_1808[7][8][1] = {{{0xA90CE119L},{0x694B92D9L},{0xA90CE119L},{4294967289UL},{0x7CCDC3FCL},{3UL},{0x58021013L},{0xE1CD6AD6L}},{{2UL},{0xC3930881L},{0UL},{0UL},{0xC3930881L},{2UL},{0xE1CD6AD6L},{0x58021013L}},{{3UL},{0x7CCDC3FCL},{4294967289UL},{0xA90CE119L},{0x694B92D9L},{0xA90CE119L},{4294967289UL},{0x7CCDC3FCL}},{{3UL},{0x58021013L},{0xE1CD6AD6L},{2UL},{0xC3930881L},{0UL},{0UL},{0xC3930881L}},{{2UL},{0xE1CD6AD6L},{0x58021013L},{3UL},{0x7CCDC3FCL},{4294967289UL},{0xA90CE119L},{0x694B92D9L}},{{0xA90CE119L},{4294967289UL},{0x7CCDC3FCL},{3UL},{0x58021013L},{0xE1CD6AD6L},{2UL},{0xC3930881L}},{{0UL},{0UL},{0xC3930881L},{2UL},{0xE1CD6AD6L},{0x58021013L},{3UL},{0x7CCDC3FCL}}};
            uint64_t ***l_1815 = &l_1545;
            uint64_t ****l_1816 = &l_1544;
            int64_t **l_1931 = (void*)0;
            uint32_t l_1966 = 4294967295UL;
            int32_t *l_1978 = &g_165.f1;
            uint16_t * const **l_2006[3][4][8] = {{{&g_996[0],&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1],&g_996[0],&g_996[1]},{&g_996[0],&g_996[0],(void*)0,&g_996[0],&g_996[0],(void*)0,&g_996[0],&g_996[0]},{&g_996[1],&g_996[0],&g_996[1],&g_996[1],&g_996[0],&g_996[1],(void*)0,&g_996[1]},{&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0}},{{&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1]},{(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1]},{&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0},{&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1]}},{{(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1]},{&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0},{&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1],&g_996[0],&g_996[1],&g_996[1]},{(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1],(void*)0,(void*)0,&g_996[1]}}};
            uint16_t * const ** const *l_2005[5] = {&l_2006[2][3][4],&l_2006[2][3][4],&l_2006[2][3][4],&l_2006[2][3][4],&l_2006[2][3][4]};
            int i, j, k;
            for (g_68 = 0; (g_68 <= 3); g_68 += 1)
            { /* block id: 789 */
                int32_t **l_1726 = &l_1605;
                int32_t l_1742 = 0x23E477B5L;
                int32_t l_1751[4];
                int16_t l_1758[8] = {0x2D2FL,0x2D2FL,0x92F3L,0x2D2FL,0x2D2FL,0x92F3L,0x2D2FL,0x2D2FL};
                uint8_t l_1760 = 0xE5L;
                struct S0 l_1791 = {-27317,-1L};
                int i;
                for (i = 0; i < 4; i++)
                    l_1751[i] = 0x1FD04B7DL;
                if (p_15)
                    break;
                (*l_1726) = &l_1496;
                for (g_883 = 3; (g_883 >= 0); g_883 -= 1)
                { /* block id: 794 */
                    for (l_1533.f1 = 3; (l_1533.f1 >= 0); l_1533.f1 -= 1)
                    { /* block id: 797 */
                        int i, j, k;
                        if (g_1027[g_255][l_1533.f1][l_1533.f1])
                            break;
                        (*g_185) &= ((p_15 & (-4L)) ^ 0x29717197L);
                        (*g_185) = 8L;
                    }
                }
            }
        }
        (*l_2009) = &l_1688;
    }
    for (g_25 = 0; (g_25 > 19); g_25 = safe_add_func_uint16_t_u_u(g_25, 4))
    { /* block id: 949 */
        int32_t *l_2012 = &l_1688;
        (*g_2013) = l_2012;
    }
    return (***g_1570);
}


/* ------------------------------------------ */
/* 
 * reads : g_162 g_363 g_196 g_335 g_77 g_936 g_51 g_185 g_50 g_54 g_801 g_937 g_25 g_285 g_793 g_143 g_144 g_2 g_1027 g_36 g_863 g_291 g_902 g_883 g_1074 g_1075 g_1076 g_68 g_255 g_1174 g_502 g_1202 g_311 g_293 g_573 g_823 g_809 g_55 g_290 g_1000 g_11 g_1260 g_880 g_1256 g_186 g_1368 g_847
 * writes: g_36 g_936 g_50 g_54 g_51 g_285 g_673 g_293 g_996 g_883 g_1027 g_809 g_1074 g_902 g_143 g_61.f1 g_255 g_55 g_76 g_11 g_204 g_291 g_1000 g_162 g_1256 g_847 g_899 g_335 g_144
 */
static const int32_t  func_18(int64_t  p_19, uint64_t  p_20, int32_t  p_21, float  p_22)
{ /* block id: 449 */
    struct S0 l_906 = {41936,0xDD63DE66L};
    int64_t *l_907 = &g_285;
    int64_t **l_908 = &l_907;
    int64_t *l_910 = (void*)0;
    int64_t **l_909 = &l_910;
    const int16_t l_923 = 0L;
    int16_t *l_924 = (void*)0;
    int32_t l_925[2][3] = {{0x77303E70L,(-1L),0x77303E70L},{0x77303E70L,(-1L),0x77303E70L}};
    int32_t l_926 = 0x568EADACL;
    int32_t l_935 = 0x7D9876B6L;
    int32_t l_951 = (-1L);
    int64_t l_953[9][6][4] = {{{0x9CE39D030872C37DLL,0xBD91084907DABF00LL,1L,1L},{0x9C1BA180526D7DE7LL,0L,0x993B7A0D18191FC8LL,0L},{0L,1L,0x9CE39D030872C37DLL,0L},{0x9CE39D030872C37DLL,0L,1L,1L},{0xBD91084907DABF00LL,0xBD91084907DABF00LL,0x993B7A0D18191FC8LL,0x9C1BA180526D7DE7LL},{0xBD91084907DABF00LL,1L,1L,0xBD91084907DABF00LL}},{{0x9CE39D030872C37DLL,0x9C1BA180526D7DE7LL,0x9CE39D030872C37DLL,1L},{0L,0x9C1BA180526D7DE7LL,0x993B7A0D18191FC8LL,0xBD91084907DABF00LL},{0x9C1BA180526D7DE7LL,1L,1L,0x9C1BA180526D7DE7LL},{0x9CE39D030872C37DLL,0xBD91084907DABF00LL,1L,1L},{0x9C1BA180526D7DE7LL,0L,0x993B7A0D18191FC8LL,0L},{0L,1L,0x9CE39D030872C37DLL,0L}},{{0x9CE39D030872C37DLL,0L,1L,1L},{0xBD91084907DABF00LL,0xBD91084907DABF00LL,0x993B7A0D18191FC8LL,0x9C1BA180526D7DE7LL},{0xBD91084907DABF00LL,1L,1L,0xBD91084907DABF00LL},{0x9CE39D030872C37DLL,0x9C1BA180526D7DE7LL,0x9CE39D030872C37DLL,1L},{0L,0x9C1BA180526D7DE7LL,0x993B7A0D18191FC8LL,0xBD91084907DABF00LL},{0x9C1BA180526D7DE7LL,1L,1L,0x9C1BA180526D7DE7LL}},{{0x9CE39D030872C37DLL,0xBD91084907DABF00LL,1L,1L},{0x9C1BA180526D7DE7LL,0L,0x993B7A0D18191FC8LL,0L},{0L,1L,0x9CE39D030872C37DLL,0L},{0x9CE39D030872C37DLL,0L,1L,1L},{0xBD91084907DABF00LL,0xBD91084907DABF00LL,0x993B7A0D18191FC8LL,0x9C1BA180526D7DE7LL},{0xBD91084907DABF00LL,1L,1L,0xBD91084907DABF00LL}},{{0x9CE39D030872C37DLL,1L,0xA0AC74C6C4A150BELL,9L},{1L,1L,0xBD91084907DABF00LL,0x9CE39D030872C37DLL},{1L,0x993B7A0D18191FC8LL,0x993B7A0D18191FC8LL,1L},{0xA0AC74C6C4A150BELL,0x9CE39D030872C37DLL,0x993B7A0D18191FC8LL,9L},{1L,1L,0xBD91084907DABF00LL,1L},{1L,0x993B7A0D18191FC8LL,0xA0AC74C6C4A150BELL,1L}},{{0xA0AC74C6C4A150BELL,1L,9L,9L},{0x9CE39D030872C37DLL,0x9CE39D030872C37DLL,0xBD91084907DABF00LL,1L},{0x9CE39D030872C37DLL,0x993B7A0D18191FC8LL,9L,0x9CE39D030872C37DLL},{0xA0AC74C6C4A150BELL,1L,0xA0AC74C6C4A150BELL,9L},{1L,1L,0xBD91084907DABF00LL,0x9CE39D030872C37DLL},{1L,0x993B7A0D18191FC8LL,0x993B7A0D18191FC8LL,1L}},{{0xA0AC74C6C4A150BELL,0x9CE39D030872C37DLL,0x993B7A0D18191FC8LL,9L},{1L,1L,0xBD91084907DABF00LL,1L},{1L,0x993B7A0D18191FC8LL,0xA0AC74C6C4A150BELL,1L},{0xA0AC74C6C4A150BELL,1L,9L,9L},{0x9CE39D030872C37DLL,0x9CE39D030872C37DLL,0xBD91084907DABF00LL,1L},{0x9CE39D030872C37DLL,0x993B7A0D18191FC8LL,9L,0x9CE39D030872C37DLL}},{{0xA0AC74C6C4A150BELL,1L,0xA0AC74C6C4A150BELL,9L},{1L,1L,0xBD91084907DABF00LL,0x9CE39D030872C37DLL},{1L,0x993B7A0D18191FC8LL,0x993B7A0D18191FC8LL,1L},{0xA0AC74C6C4A150BELL,0x9CE39D030872C37DLL,0x993B7A0D18191FC8LL,9L},{1L,1L,0xBD91084907DABF00LL,1L},{1L,0x993B7A0D18191FC8LL,0xA0AC74C6C4A150BELL,1L}},{{0xA0AC74C6C4A150BELL,1L,9L,9L},{0x9CE39D030872C37DLL,0x9CE39D030872C37DLL,0xBD91084907DABF00LL,1L},{0x9CE39D030872C37DLL,0x993B7A0D18191FC8LL,9L,0x9CE39D030872C37DLL},{0xA0AC74C6C4A150BELL,1L,0xA0AC74C6C4A150BELL,9L},{1L,1L,0xBD91084907DABF00LL,0x9CE39D030872C37DLL},{1L,0x993B7A0D18191FC8LL,0x993B7A0D18191FC8LL,1L}}};
    uint32_t l_954 = 0xC70FDFD3L;
    struct S0 l_960 = {23024,0L};
    uint16_t **l_989 = &g_880[2][2];
    uint16_t * const l_999 = &g_1000;
    uint16_t * const *l_998[1];
    uint32_t l_1007 = 0UL;
    int32_t l_1058 = 0xB8E13055L;
    uint32_t **l_1059 = (void*)0;
    uint16_t l_1082[4][9][7] = {{{0x706CL,0x0C39L,65535UL,0x0E8EL,0UL,0xEF6DL,9UL},{0UL,6UL,65532UL,0UL,65527UL,1UL,0xEE22L},{0xD6A5L,0x3058L,65534UL,0xD5BBL,0xEF6DL,65535UL,0xB31EL},{0x7BCAL,0x79BFL,6UL,6UL,0x79BFL,0x7BCAL,65535UL},{0x2788L,0x219AL,0x05E8L,9UL,0x706CL,0xD5BBL,65535UL},{65535UL,8UL,0x0E0FL,0UL,0xC040L,3UL,65532UL},{0UL,0x219AL,65535UL,0xBBF6L,0x0E8EL,0x8E99L,65535UL},{8UL,0x79BFL,0x6182L,0x8565L,65535UL,0UL,0UL},{0x68C1L,0x3058L,0UL,0x05E8L,0x05E8L,0UL,0x3058L}},{{65526UL,6UL,0x64F3L,0x6278L,8UL,0xA403L,7UL},{65534UL,0x0C39L,0x706CL,0x0C39L,65535UL,0x0E8EL,0UL},{0xA68DL,0xA403L,0UL,0x33D3L,0UL,1UL,0UL},{0xD6A5L,0UL,0UL,0UL,0x3058L,65535UL,0UL},{8UL,0UL,3UL,0x5423L,65527UL,65527UL,0x5423L},{0x412CL,0x8E99L,0x412CL,0x2981L,0xB31EL,1UL,65534UL},{65527UL,3UL,0xA68DL,0x8565L,0x5460L,0x6278L,0xA403L},{65535UL,0xD5BBL,65535UL,0xEF6DL,65534UL,1UL,1UL},{0x33D3L,0x7BCAL,0x6182L,8UL,0UL,65527UL,65535UL}},{{0x2788L,65535UL,1UL,65535UL,0x3ACCL,65535UL,65535UL},{65535UL,1UL,65532UL,1UL,65535UL,1UL,6UL},{0xB31EL,0xEF6DL,0x0C39L,65535UL,0x8E99L,0x0E8EL,0xBBF6L},{3UL,65535UL,65535UL,0x64F3L,0xC040L,0x79BFL,0x33D3L},{0xB31EL,65535UL,65535UL,65534UL,0x0C39L,0xD6A5L,7UL},{65535UL,0x6B9FL,0x64F3L,65527UL,0x2AC2L,1UL,0xC040L},{0x2788L,0xF166L,0x0E8EL,65534UL,0x0E8EL,0xF166L,0x2788L},{0x33D3L,65535UL,0x09C4L,7UL,0x7BCAL,0xEDC4L,0x6182L},{65535UL,65535UL,65534UL,0x412CL,1UL,65535UL,0xF166L}},{{65527UL,0x0491L,0x09C4L,65535UL,0x5423L,65532UL,0xA949L},{0x412CL,0x2506L,0x0E8EL,1UL,65535UL,65534UL,65535UL},{8UL,0x6278L,0x64F3L,6UL,65526UL,6UL,0x64F3L},{0xD6A5L,0xD6A5L,65535UL,65535UL,0x2788L,0x05E8L,0x412CL},{0xA68DL,0xEDC4L,65535UL,65526UL,65535UL,0x0E0FL,0UL},{0xBBF6L,0x68C1L,0x0C39L,0x8E99L,0x2788L,65535UL,0x3ACCL},{0xEE22L,7UL,65532UL,0xA68DL,65526UL,0x6182L,1UL},{0xD5BBL,0xB31EL,1UL,0x3058L,65535UL,0UL,65535UL},{1UL,0x6182L,0x6182L,1UL,0x5423L,0x64F3L,0x0491L}}};
    int16_t l_1117 = 0x604EL;
    float l_1152 = (-0x3.Dp-1);
    float l_1156 = 0x0.5p+1;
    int16_t l_1157 = 0xD182L;
    uint32_t l_1161 = 2UL;
    uint8_t l_1204 = 248UL;
    const int32_t l_1261 = 0x49B3EDC2L;
    uint16_t l_1306[2][9][1];
    uint32_t l_1334 = 1UL;
    const uint16_t * const l_1361 = &g_995;
    const uint16_t * const *l_1360 = &l_1361;
    const uint16_t * const **l_1359 = &l_1360;
    const uint16_t * const ** const *l_1358 = &l_1359;
    uint8_t **l_1400[8] = {&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77};
    uint8_t *** const l_1399 = &l_1400[3];
    uint64_t l_1440[6][2] = {{1UL,18446744073709551613UL},{0xE74CBEDAD5E0BAE8LL,18446744073709551612UL},{18446744073709551615UL,1UL},{0xE74CBEDAD5E0BAE8LL,0xE74CBEDAD5E0BAE8LL},{0xE74CBEDAD5E0BAE8LL,1UL},{18446744073709551615UL,18446744073709551612UL}};
    uint32_t l_1485 = 0x8638609CL;
    int16_t *l_1486 = (void*)0;
    int16_t *l_1487[9][2][2] = {{{(void*)0,&g_162},{&g_11[1][1],&g_228[4]}},{{&g_847[0][4][3],&g_11[1][1]},{&g_228[4],&g_162}},{{&g_228[4],&g_11[1][1]},{&g_847[0][4][3],&g_228[4]}},{{&g_11[1][1],&g_162},{(void*)0,(void*)0}},{{&g_847[0][4][3],(void*)0},{(void*)0,&g_162}},{{&g_11[1][1],&g_228[4]},{&g_847[0][4][3],&g_11[1][1]}},{{&g_228[4],&g_162},{&g_228[4],&g_11[1][1]}},{{&g_847[0][4][3],&g_228[4]},{&g_11[1][1],&g_162}},{{(void*)0,(void*)0},{&g_847[0][4][3],(void*)0}}};
    const uint64_t l_1490 = 18446744073709551615UL;
    int32_t *l_1493[7][4][6] = {{{&l_906.f1,&g_61.f1,&g_144[7][0][0],(void*)0,(void*)0,&g_165.f1},{&g_51[1][0][8],&g_51[1][0][3],&g_165.f1,&g_61.f1,(void*)0,&g_144[2][4][0]},{(void*)0,(void*)0,(void*)0,&l_925[1][0],&l_1058,&l_925[1][0]},{&l_925[1][2],&g_165.f1,(void*)0,&g_144[2][4][0],&l_926,&g_1139}},{{&l_926,&g_61.f1,&g_51[1][0][8],&g_165.f1,&l_960.f1,&l_906.f1},{&g_165.f1,&l_925[1][0],&g_144[2][4][0],&g_1139,&g_1139,&g_1139},{(void*)0,&l_1058,&l_1058,(void*)0,&g_51[1][0][3],&g_144[3][5][0]},{&g_144[2][4][0],&g_1139,&g_1470,&g_2,&l_925[1][2],(void*)0}},{{&l_960.f1,&l_926,&g_51[0][1][7],&g_165.f1,&l_925[1][2],&g_51[2][0][0]},{&g_61.f1,&g_1139,&g_165.f1,&g_55,&g_51[1][0][3],&l_926},{&g_144[2][4][0],&l_1058,(void*)0,&g_1139,&g_1139,&l_1058},{&l_926,&l_925[1][0],&l_960.f1,(void*)0,&l_960.f1,&g_165.f1}},{{&g_61.f1,&g_61.f1,&g_55,&g_165.f1,&l_926,&l_925[1][2]},{&g_51[1][0][8],&g_165.f1,(void*)0,&l_926,&l_1058,&g_165.f1},{&l_925[1][0],(void*)0,&g_165.f1,&l_925[1][0],(void*)0,&g_165.f1},{&g_165.f1,&g_51[1][0][3],&g_165.f1,(void*)0,(void*)0,&g_61.f1}},{{&g_144[2][4][0],&g_61.f1,&l_925[1][0],&l_925[1][2],&g_165.f1,&g_2},{(void*)0,&l_906.f1,&g_144[2][4][0],&l_906.f1,(void*)0,&g_1470},{&g_1139,&l_960.f1,&g_51[1][1][5],&l_925[1][0],&g_51[0][1][7],(void*)0},{(void*)0,&l_926,(void*)0,&l_960.f1,&l_925[1][0],(void*)0}},{{&g_144[2][4][0],&l_1058,&g_51[1][1][5],&g_165.f1,&g_165.f1,&g_1470},{&l_925[1][0],&g_165.f1,&g_144[2][4][0],&g_51[1][0][8],&l_926,&g_2},{&g_144[3][5][0],&g_144[7][0][0],&l_925[1][0],&g_144[2][4][0],&l_925[1][0],&g_61.f1},{&g_2,(void*)0,&g_165.f1,&l_926,(void*)0,&g_165.f1}},{{&g_61.f1,&g_61.f1,&g_51[1][0][8],&g_1139,&g_2,&g_144[7][0][0]},{&g_165.f1,&g_51[2][0][0],&g_51[1][0][8],&g_144[2][4][0],&l_925[1][0],&g_1470},{&g_165.f1,&g_61.f1,&l_925[1][0],(void*)0,&l_906.f1,&g_51[1][0][8]},{&g_165.f1,&g_1139,&g_51[1][0][8],&g_61.f1,&g_165.f1,&g_51[1][1][5]}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_998[i] = &l_999;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
                l_1306[i][j][k] = 65527UL;
        }
    }
    if ((((l_906 = l_906) , l_906) , (((l_906 , ((*l_909) = ((*l_908) = l_907))) != (void*)0) < (((~(safe_add_func_int8_t_s_s(((safe_sub_func_uint16_t_u_u((l_926 = ((l_925[1][0] &= ((safe_mul_func_uint8_t_u_u(7UL, ((safe_mul_func_uint16_t_u_u(65530UL, (safe_div_func_uint8_t_u_u((((+l_906.f0) & (((&g_285 != (void*)0) | l_923) == g_162)) | 0x641532C6F5F419E2LL), l_923)))) <= l_906.f1))) & (*g_363))) | l_906.f1)), l_906.f0)) == l_906.f0), p_21))) ^ g_335[0][3][0]) , 65535UL))))
    { /* block id: 455 */
        uint16_t ****l_938 = &g_936;
        int32_t l_939 = (-1L);
        uint32_t *l_941 = &g_68;
        int32_t *l_942 = &g_165.f1;
        int32_t *l_943 = &l_926;
        int32_t *l_944 = &l_926;
        int32_t *l_945 = &l_925[1][0];
        int32_t *l_946 = &l_925[1][0];
        int32_t *l_947 = &g_55;
        int32_t *l_948[5] = {&g_51[1][0][8],&g_51[1][0][8],&g_51[1][0][8],&g_51[1][0][8],&g_51[1][0][8]};
        int32_t l_949[5][4] = {{1L,1L,0xDD5631FFL,0xB4DD12B2L},{6L,0x3AF1FF1DL,6L,0xDD5631FFL},{6L,0xDD5631FFL,0xDD5631FFL,6L},{1L,0xDD5631FFL,0xB4DD12B2L,0xDD5631FFL},{0xDD5631FFL,0x3AF1FF1DL,0xB4DD12B2L,0xB4DD12B2L}};
        int8_t l_950 = 0x93L;
        int32_t l_952 = (-1L);
        int i, j;
        (*g_185) |= (((safe_rshift_func_int16_t_s_u(((((safe_sub_func_uint32_t_u_u(p_20, (safe_lshift_func_uint16_t_u_u(p_20, 7)))) , ((*g_77) = 0x17L)) , (&g_672 != ((safe_div_func_int32_t_s_s(l_923, l_935)) , ((*l_938) = g_936)))) || l_939), ((+(l_941 != (void*)0)) <= p_19))) ^ g_51[1][2][6]) , p_21);
        --l_954;
    }
    else
    { /* block id: 460 */
        uint64_t l_965[4][3][3] = {{{9UL,9UL,9UL},{0UL,0UL,0UL},{9UL,9UL,9UL}},{{0UL,0UL,0UL},{9UL,9UL,9UL},{0UL,0UL,0UL}},{{9UL,9UL,9UL},{0UL,0UL,0UL},{9UL,9UL,9UL}},{{0UL,0UL,0UL},{9UL,9UL,9UL},{0UL,0UL,0UL}}};
        int32_t l_980 = 1L;
        uint16_t * const l_993[2][5] = {{&g_994,&g_994,&g_994,&g_994,&g_994},{&g_995,&g_995,&g_995,&g_995,&g_995}};
        uint16_t * const *l_992 = &l_993[1][3];
        int32_t l_1024 = (-1L);
        int32_t l_1025[5][2];
        int8_t l_1026 = (-1L);
        uint16_t ** const *l_1037 = &l_989;
        uint16_t ** const **l_1036[3];
        const uint32_t *l_1072 = &g_68;
        const uint32_t **l_1071[6];
        int32_t l_1130 = 0x70A0D52AL;
        int64_t l_1140 = (-7L);
        int64_t l_1150 = 0xFE5B783956462C4ALL;
        int8_t l_1154 = 0L;
        int8_t *l_1201 = &g_883;
        int8_t * const *l_1200 = &l_1201;
        uint16_t l_1218 = 0x3DB3L;
        int8_t l_1221 = 7L;
        struct S0 l_1227 = {-9452,1L};
        int64_t l_1266[4] = {7L,7L,7L,7L};
        uint16_t ****l_1356[6];
        uint8_t ** const l_1377 = &g_77;
        uint8_t ** const *l_1376 = &l_1377;
        uint8_t ** const **l_1375 = &l_1376;
        struct S0 ***** const l_1450[10][8][3] = {{{(void*)0,(void*)0,&g_822},{&g_822,(void*)0,&g_822},{&g_822,&g_822,(void*)0},{&g_822,&g_822,&g_822},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,(void*)0,(void*)0}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,(void*)0,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,(void*)0},{&g_822,&g_822,(void*)0},{&g_822,&g_822,&g_822},{(void*)0,&g_822,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,(void*)0,(void*)0},{&g_822,&g_822,(void*)0},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{(void*)0,&g_822,&g_822},{&g_822,&g_822,&g_822}},{{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{(void*)0,&g_822,(void*)0},{&g_822,&g_822,(void*)0},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,(void*)0},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,(void*)0},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,(void*)0,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,(void*)0},{&g_822,&g_822,(void*)0},{&g_822,&g_822,&g_822}},{{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822},{(void*)0,&g_822,&g_822},{&g_822,&g_822,&g_822},{&g_822,&g_822,&g_822}}};
        float l_1469 = 0x7.Dp-1;
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 2; j++)
                l_1025[i][j] = 0x81011288L;
        }
        for (i = 0; i < 3; i++)
            l_1036[i] = &l_1037;
        for (i = 0; i < 6; i++)
            l_1071[i] = &l_1072;
        for (i = 0; i < 6; i++)
            l_1356[i] = &g_936;
        for (l_926 = 28; (l_926 < (-3)); l_926--)
        { /* block id: 463 */
            struct S0 l_959 = {-12005,0x911DAB49L};
            int32_t *l_966 = &g_51[1][0][5];
            uint64_t *l_971 = &l_965[0][2][2];
            uint16_t **l_988 = &g_880[0][3];
            int32_t *l_1011 = &l_951;
            int32_t *l_1012 = &g_51[1][0][5];
            int32_t *l_1013 = (void*)0;
            int32_t *l_1014 = &g_51[3][2][4];
            int32_t *l_1015 = (void*)0;
            int32_t *l_1016 = &l_959.f1;
            int32_t *l_1017 = &g_51[1][0][8];
            int32_t *l_1018 = &g_165.f1;
            int32_t *l_1019 = (void*)0;
            int32_t *l_1020 = &g_51[0][2][7];
            int32_t l_1021 = 0x0979578BL;
            int32_t *l_1022 = (void*)0;
            int32_t *l_1023[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1023[i] = &l_1021;
            l_960 = (l_959 = l_906);
            if (((((!1L) != 0x429C3B61L) && ((*l_971) = (((*l_966) &= (safe_unary_minus_func_uint8_t_u((safe_lshift_func_uint8_t_u_u(l_965[0][2][2], 2))))) , (safe_rshift_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s(0UL, (0x1E10L & 0x14BCL))), 14))))) >= g_801[3]))
            { /* block id: 468 */
                int32_t *l_981[7];
                uint16_t * const *l_991 = (void*)0;
                uint16_t * const **l_990[7] = {&l_991,(void*)0,(void*)0,&l_991,(void*)0,(void*)0,&l_991};
                int8_t *l_1010 = &g_883;
                int i;
                for (i = 0; i < 7; i++)
                    l_981[i] = &l_960.f1;
                for (l_935 = 18; (l_935 != 2); l_935 = safe_sub_func_int8_t_s_s(l_935, 8))
                { /* block id: 471 */
                    for (l_954 = (-4); (l_954 < 9); ++l_954)
                    { /* block id: 474 */
                        int32_t l_978 = 0x579B6E40L;
                        uint16_t *l_979 = (void*)0;
                        int32_t **l_982 = &l_966;
                        int32_t **l_984 = (void*)0;
                        int32_t *l_985 = &l_925[1][0];
                        l_980 = ((((**l_909) = p_21) , (safe_div_func_int32_t_s_s(((l_925[0][2] = l_978) , ((((*g_937) = (void*)0) != l_979) != ((*l_966) >= g_25))), (g_285 | 0x85B6L)))) , p_19);
                        l_985 = ((*l_982) = ((*g_793) = l_981[3]));
                    }
                }
                if (p_19)
                    continue;
                l_980 = (((l_989 = l_988) == (l_998[0] = (g_996[1] = (l_992 = (*g_936))))) & (g_801[3] < (0x069FL && (((*l_907) = ((1UL | (((p_21 , (safe_lshift_func_uint16_t_u_u((((((safe_lshift_func_uint8_t_u_u(l_1007, 5)) == ((*l_966) = (0L == (safe_rshift_func_int8_t_s_u(((*l_1010) = g_143), 5))))) && 0x950653832B520F9ELL) , 0xEDL) > p_21), 3))) ^ p_19) < 6UL)) <= g_144[8][7][0])) & g_2))));
            }
            else
            { /* block id: 493 */
                l_960 = l_959;
            }
            (*g_185) ^= 0x22DDC531L;
            --g_1027[0][3][0];
        }
lbl_1084:
        for (g_809 = 0; (g_809 > 1); g_809++)
        { /* block id: 501 */
            int8_t *l_1033 = &g_883;
            uint16_t ** const ***l_1038 = &l_1036[1];
            int32_t l_1039[4] = {0L,0L,0L,0L};
            int i;
            l_980 &= (l_1025[0][0] = (((*l_1033) = (safe_unary_minus_func_uint8_t_u((1L <= (-1L))))) ^ (g_51[3][0][5] && ((((*l_1038) = l_1036[1]) == ((l_965[2][1][1] && l_965[0][2][2]) , (void*)0)) != l_1039[0]))));
            return p_21;
        }
        if ((((((*g_77) || ((safe_mod_func_uint16_t_u_u((p_19 , (~(safe_add_func_uint64_t_u_u(g_285, (l_1024 ^= l_965[0][2][2]))))), (safe_rshift_func_int8_t_s_s((((void*)0 != &l_960) ^ ((safe_div_func_uint8_t_u_u(l_1025[1][0], (safe_mul_func_int8_t_s_s((safe_mod_func_int16_t_s_s((((safe_mod_func_uint8_t_u_u(((safe_add_func_int8_t_s_s(l_1026, ((((safe_unary_minus_func_int32_t_s(l_951)) & l_965[1][0][2]) , 0xCEBF0F8AL) , p_21))) >= (-1L)), (*g_77))) == 0xB63315FAL) >= l_965[0][2][2]), l_1058)), (*g_77))))) ^ 0xD0L)), 4)))) == 0x52L)) == p_19) <= 0x28842546AFF7696CLL) < p_19))
        { /* block id: 509 */
            uint32_t **l_1060 = (void*)0;
            uint32_t ***l_1070 = &l_1060;
            const uint32_t ***l_1073[1][3];
            float l_1080 = (-0x2.2p-1);
            int32_t l_1081 = (-1L);
            float *l_1083[4][4] = {{&g_291,&g_291,&g_291,&g_291},{&g_291,&g_291,&g_291,&g_291},{&g_291,&g_291,&g_291,&g_291},{&g_291,&g_291,&g_291,&g_291}};
            uint16_t **l_1100[5] = {&g_880[2][2],&g_880[2][2],&g_880[2][2],&g_880[2][2],&g_880[2][2]};
            int8_t *l_1113 = &l_1026;
            int8_t l_1138[6][5] = {{0x9EL,0x1AL,0L,0x1AL,0x9EL},{0xCFL,0x1AL,1L,(-1L),0x9EL},{0x9EL,(-1L),0x2FL,4L,0x6AL},{(-1L),4L,1L,4L,(-1L)},{0x6AL,4L,0x2FL,0xC0L,(-1L)},{(-1L),0xC0L,0x2FL,4L,0x6AL}};
            int32_t l_1143 = 0L;
            int32_t l_1144 = 0xDFF96B86L;
            int32_t l_1145 = 0xCB6EA205L;
            int32_t l_1146 = 0x9D0BFD73L;
            int32_t l_1147 = 4L;
            int32_t l_1148 = (-7L);
            int32_t l_1149 = 0L;
            int32_t l_1151[9];
            int8_t l_1153[9][5] = {{1L,1L,0xE1L,0x83L,0xFBL},{0x29L,0L,0L,0L,0L},{0xFBL,0x5CL,0x94L,0xFBL,0xD3L},{0xDAL,0L,0x71L,0L,0xDAL},{0x94L,1L,0x5CL,0xD3L,1L},{0xDAL,0x11L,0x11L,0xDAL,0L},{0xFBL,0x83L,0xE1L,1L,1L},{0x29L,0xDAL,0x29L,0L,0xDAL},{1L,0x5CL,0xD3L,1L,0xD3L}};
            float l_1158 = 0x6.8C4A70p-63;
            int64_t *l_1207 = (void*)0;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1073[i][j] = &l_1071[4];
            }
            for (i = 0; i < 9; i++)
                l_1151[i] = (-1L);
            if (((((0xB67FE06CBBE9A993LL == (l_1059 != (l_1060 = l_1059))) && (((safe_sub_func_float_f_f((p_22 = (!((safe_add_func_int64_t_s_s(((((safe_sub_func_int32_t_s_s((l_906.f0 <= ((safe_sub_func_uint32_t_u_u((((*l_1070) = l_1059) == (g_1074 = l_1071[4])), (0x237BB35DDE3BD708LL <= ((**l_908) = (safe_mod_func_int64_t_s_s((+((p_22 < ((l_1080 <= 0xE.D5C9E2p-49) == 0x1.6p+1)) , l_1081)), p_20)))))) == l_1082[0][8][3])), 4294967295UL)) && 8UL) > l_1081) && 18446744073709551615UL), l_953[6][0][0])) , 0x0.1p+1))), (*g_863))) != 0xD.9CA0F8p-1) , l_1081)) , l_1081) | 65535UL))
            { /* block id: 515 */
                if (l_906.f0)
                    goto lbl_1084;
            }
            else
            { /* block id: 517 */
                uint8_t **l_1122 = (void*)0;
                int32_t l_1124[1];
                uint16_t l_1131 = 0xFFB2L;
                float l_1155 = 0x0.Fp+1;
                uint64_t l_1193[10][9][2] = {{{0xA0E772DCDAA859BCLL,0xF72040F56D70D77DLL},{18446744073709551615UL,0x50BBDF51A35518A7LL},{0UL,0x9D28841CCECDCCCELL},{0x36B28B21B76D9D0BLL,0x188CA49B4746B873LL},{1UL,0xB638BC5232A6CC93LL},{18446744073709551610UL,0x6881284AAEDF5596LL},{0UL,0xF5DF097C384A00ECLL},{1UL,0x36B28B21B76D9D0BLL},{18446744073709551612UL,0UL}},{{1UL,18446744073709551615UL},{4UL,0xD7E5191ACF9E4766LL},{0xB638BC5232A6CC93LL,0x5DC81AC36E7F06EBLL},{18446744073709551615UL,0xD0FA7DDC4F1EE22ALL},{0x190E40CD07323265LL,0xD9B3FBE51F95F3D9LL},{0x0CA4BD55786EB7E1LL,6UL},{0x6881284AAEDF5596LL,1UL},{0xD9B3FBE51F95F3D9LL,1UL},{0UL,18446744073709551615UL}},{{0xD0FA7DDC4F1EE22ALL,0x87CF9A6EDC8627DBLL},{0xD81B435BC1F2E6B6LL,0x605519110F3B216FLL},{0xC08FB7A6F7657353LL,0xC08FB7A6F7657353LL},{1UL,0UL},{0xF5DF097C384A00ECLL,0x0CA4BD55786EB7E1LL},{18446744073709551610UL,0xF39686755FF384FDLL},{18446744073709551608UL,18446744073709551610UL},{0x988A6087ACF8C186LL,18446744073709551615UL},{0x988A6087ACF8C186LL,18446744073709551610UL}},{{18446744073709551608UL,0xF39686755FF384FDLL},{18446744073709551610UL,0x0CA4BD55786EB7E1LL},{0xF5DF097C384A00ECLL,0UL},{1UL,0xC08FB7A6F7657353LL},{0xC08FB7A6F7657353LL,0x605519110F3B216FLL},{0xD81B435BC1F2E6B6LL,0x87CF9A6EDC8627DBLL},{0xD0FA7DDC4F1EE22ALL,18446744073709551615UL},{0UL,1UL},{0xD9B3FBE51F95F3D9LL,1UL}},{{0x6881284AAEDF5596LL,6UL},{0x0CA4BD55786EB7E1LL,0xD9B3FBE51F95F3D9LL},{0x190E40CD07323265LL,0xD0FA7DDC4F1EE22ALL},{18446744073709551615UL,0x5DC81AC36E7F06EBLL},{0xB638BC5232A6CC93LL,0xD7E5191ACF9E4766LL},{4UL,18446744073709551615UL},{1UL,0UL},{18446744073709551612UL,0x36B28B21B76D9D0BLL},{1UL,0xF5DF097C384A00ECLL}},{{0UL,0x6881284AAEDF5596LL},{18446744073709551610UL,0xB638BC5232A6CC93LL},{1UL,0x188CA49B4746B873LL},{0x36B28B21B76D9D0BLL,0x9D28841CCECDCCCELL},{0UL,0x50BBDF51A35518A7LL},{18446744073709551615UL,0xF72040F56D70D77DLL},{0xA0E772DCDAA859BCLL,0x954207434B265E4DLL},{0xD7E5191ACF9E4766LL,0UL},{18446744073709551615UL,0UL}},{{1UL,1UL},{0x262A4307B1CEC729LL,1UL},{1UL,0UL},{18446744073709551615UL,0UL},{0xD7E5191ACF9E4766LL,0x954207434B265E4DLL},{0xA0E772DCDAA859BCLL,0xF72040F56D70D77DLL},{18446744073709551615UL,0x50BBDF51A35518A7LL},{0UL,0x9D28841CCECDCCCELL},{0x36B28B21B76D9D0BLL,0x188CA49B4746B873LL}},{{1UL,0UL},{0xF39686755FF384FDLL,0xD7E5191ACF9E4766LL},{0x32A8B6261F277B12LL,0x0CA4BD55786EB7E1LL},{1UL,1UL},{0UL,0xA0E772DCDAA859BCLL},{0x190E40CD07323265LL,1UL},{18446744073709551610UL,0xF60A6359A2A55A1ELL},{0UL,0UL},{18446744073709551610UL,0UL}},{{0xF72040F56D70D77DLL,0xB638BC5232A6CC93LL},{0x7AB59822720E07F1LL,18446744073709551615UL},{0xD7E5191ACF9E4766LL,18446744073709551615UL},{0xB638BC5232A6CC93LL,0x9E80385F7C919DEFLL},{0UL,0xF5DF097C384A00ECLL},{0UL,0xD0FA7DDC4F1EE22ALL},{0x262A4307B1CEC729LL,18446744073709551612UL},{0x954207434B265E4DLL,0x954207434B265E4DLL},{0UL,18446744073709551610UL}},{{0x0CA4BD55786EB7E1LL,0x7AB59822720E07F1LL},{6UL,18446744073709551612UL},{4UL,6UL},{18446744073709551608UL,18446744073709551610UL},{18446744073709551608UL,6UL},{4UL,18446744073709551612UL},{6UL,0x7AB59822720E07F1LL},{0x0CA4BD55786EB7E1LL,18446744073709551610UL},{0UL,0x954207434B265E4DLL}}};
                uint32_t *l_1196 = &g_809;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_1124[i] = 0L;
                for (g_902 = 0; (g_902 >= 49); g_902 = safe_add_func_int64_t_s_s(g_902, 4))
                { /* block id: 520 */
                    int8_t *l_1097 = &l_1026;
                    uint64_t *l_1098 = &g_143;
                    uint64_t *l_1099 = &l_965[0][2][2];
                    uint16_t * const **l_1101 = &g_996[1];
                    (*g_185) &= (safe_mul_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((l_1024 , ((*l_1097) = (safe_add_func_int32_t_s_s((l_906.f0 = 0x65762CAFL), (safe_rshift_func_uint8_t_u_s(p_20, 2)))))), (0x2A86B8F253456C66LL >= ((*l_1099) = ((*l_1098) = 18446744073709551611UL))))), (l_1100[3] == ((*l_1101) = &l_999)))), ((g_883 == (l_1081 >= (**g_1074))) <= p_20)));
                    (*g_185) = (*g_185);
                }
lbl_1180:
                for (g_61.f1 = 15; (g_61.f1 > (-18)); g_61.f1--)
                { /* block id: 531 */
                    int8_t *l_1109 = (void*)0;
                    int8_t *l_1110 = (void*)0;
                    int32_t l_1120 = 0xF1E250F5L;
                    int32_t *l_1132 = &l_1025[4][0];
                    int32_t *l_1133 = &l_1120;
                    int32_t *l_1134 = &l_925[1][0];
                    int32_t *l_1135 = &l_925[1][0];
                    int32_t *l_1136 = &l_1025[4][0];
                    int32_t *l_1137[10];
                    int8_t l_1141 = 0x69L;
                    int8_t l_1142[1];
                    int16_t l_1160 = 0xA2BAL;
                    int i;
                    for (i = 0; i < 10; i++)
                        l_1137[i] = (void*)0;
                    for (i = 0; i < 1; i++)
                        l_1142[i] = 0x86L;
                    (*g_185) = (safe_lshift_func_uint16_t_u_s((safe_sub_func_uint16_t_u_u((((~0x4B54L) != ((((l_960.f1 = (g_255 ^= p_19)) == (*g_77)) , ((((safe_mod_func_uint64_t_u_u((l_1113 != l_1109), 0x1B3892E038924645LL)) > ((safe_rshift_func_int16_t_s_u(0x79DCL, 3)) != (-1L))) | (!(((g_50[5][3][0] ^ 0x46D10BC1A541BB1BLL) & g_285) , p_20))) , (-9L))) , p_19)) != l_1081), p_19)), l_1117));
                    for (l_954 = 0; (l_954 <= 1); l_954 += 1)
                    { /* block id: 537 */
                        int32_t *l_1121 = &g_55;
                        int16_t *l_1123[4][8][5] = {{{(void*)0,&l_1117,&l_1117,&l_1117,(void*)0},{&g_847[0][2][1],&g_162,&g_847[0][3][3],&g_847[0][2][1],&g_228[4]},{(void*)0,&g_228[4],&g_162,&l_1117,&g_162},{&g_228[4],&g_228[4],(void*)0,&g_162,&g_228[4]},{&g_162,&l_1117,&g_162,&g_228[4],(void*)0},{&g_228[4],&g_847[0][3][3],&g_847[0][3][3],&g_228[4],&g_847[0][2][1]},{(void*)0,&l_1117,&g_228[4],&l_1117,(void*)0},{&g_847[0][2][1],&g_228[4],&g_847[0][3][3],&g_847[0][3][3],&g_228[4]}},{{(void*)0,&g_228[4],&g_162,&l_1117,&g_162},{&g_228[4],&g_162,(void*)0,&g_228[4],&g_228[4]},{&g_162,&l_1117,&g_162,&g_228[4],(void*)0},{&g_228[4],&g_847[0][2][1],&g_847[0][3][3],&g_162,&g_847[0][2][1]},{(void*)0,&l_1117,&l_1117,&l_1117,(void*)0},{&g_847[0][2][1],&g_162,&g_847[0][3][3],&g_847[0][2][1],&g_228[4]},{(void*)0,&g_228[4],&g_162,&l_1117,&g_162},{&g_228[4],&g_228[4],(void*)0,&g_162,&g_228[4]}},{{&g_162,&l_1117,&g_162,&g_228[4],(void*)0},{&g_228[4],&g_847[0][3][3],&g_847[0][3][3],&g_228[4],&g_847[0][2][1]},{(void*)0,&l_1117,&g_228[4],&l_1117,(void*)0},{&g_847[0][2][1],&g_228[4],&g_847[0][3][3],&g_847[0][3][3],&g_228[4]},{(void*)0,&g_228[4],&g_162,&l_1117,&g_162},{&g_228[4],&g_162,(void*)0,&g_228[4],&g_228[4]},{&g_162,&l_1117,&g_162,&g_228[4],(void*)0},{&g_228[4],&g_847[0][2][1],&g_847[0][3][3],&g_162,&g_847[0][2][1]}},{{(void*)0,&l_1117,&l_1117,&l_1117,(void*)0},{&g_847[0][2][1],&g_162,&g_847[0][3][3],&g_847[0][2][1],&g_228[4]},{(void*)0,&g_228[4],&g_162,&l_1117,&g_162},{&g_228[4],&g_228[4],(void*)0,&g_847[0][3][3],&g_847[0][2][1]},{&l_1117,&g_228[4],&l_1117,&g_11[2][1],&g_162},{&g_847[0][2][1],(void*)0,(void*)0,&g_847[0][2][1],&l_1117},{&g_162,&g_228[4],(void*)0,&g_228[4],&g_162},{&l_1117,&g_847[0][2][1],(void*)0,(void*)0,&g_847[0][2][1]}}};
                        int i, j, k;
                        (*g_185) = (((l_1025[l_954][l_954] != (((((safe_mul_func_uint8_t_u_u(((p_20 == ((*l_1121) = l_1120)) <= (((l_1122 == (void*)0) != ((-1L) & (l_1124[0] = p_19))) , (((l_1124[0] != (safe_add_func_int32_t_s_s((safe_unary_minus_func_uint16_t_u((safe_div_func_uint32_t_u_u((**g_1074), p_19)))), p_21))) && l_1120) < (-10L)))), 247UL)) , 0xDBF1597FADE179D2LL) ^ p_20) | p_20) ^ l_1130)) < 0xE483L) || l_1131);
                        if (p_20)
                            continue;
                    }
                    l_1161--;
                }
                for (l_980 = 0; (l_980 > 21); l_980 = safe_add_func_uint8_t_u_u(l_980, 5))
                { /* block id: 547 */
                    float l_1178[8][3][7] = {{{0x1.Fp-1,0x9.F94B5Ap-94,(-0x10.Ap+1),0x1.6p+1,0x8.DC5C79p-72,(-0x1.Ep+1),0x1.Bp-1},{(-0x10.Ap+1),(-0x7.0p-1),0x9.6A698Fp+37,0x0.1D8DDAp-55,(-0x3.9p-1),0xE.35333Bp+68,0xA.EA774Fp-77},{0xF.6670E6p+14,0xE.35333Bp+68,0x0.1D8DDAp-55,(-0x6.Dp+1),0x9.C1DCBFp-44,(-0x6.3p+1),(-0x8.Bp-1)}},{{0x9.C81708p+90,0x8.DC5C79p-72,0x1.Bp-1,0x6.B5D328p+42,0x7.8535ADp-34,0x9.6A698Fp+37,0xF.CF1751p+53},{0x0.8p+1,0x1.6p+1,0x4.723B37p+33,0x1.Ap-1,0xF.6670E6p+14,0xA.EA774Fp-77,0x7.CC641Ep-23},{(-0x1.6p+1),0x1.Fp-1,0x1.0p-1,0xD.930834p+52,0xF.6670E6p+14,0x6.4p+1,0x7.3687BCp+19}},{{(-0x8.Bp-1),0x4.BFDE9Cp+42,(-0x6.Dp+1),0x6.3C4F93p+81,0x7.8535ADp-34,0x1.Fp-1,0x8.2096D3p-6},{0x1.7p+1,0x8.45FB9Ap+11,0x9.C1DCBFp-44,0x1.0p-1,0x9.C1DCBFp-44,0x8.45FB9Ap+11,0x1.7p+1},{0x3.9E9962p-87,0x0.1D8DDAp-55,0xA.EFB8D8p-41,0x9.F94B5Ap-94,(-0x3.9p-1),0x0.Dp+1,0x7.0p-1}},{{0x9.C1DCBFp-44,0x3.9E9962p-87,0x7.8535ADp-34,0x1.9p+1,0x8.DC5C79p-72,0x1.Cp+1,0x0.5p-1},{(-0x7.0p-1),(-0x8.Bp-1),0xA.EFB8D8p-41,(-0x1.6p+1),0x8.Cp-1,0x9.C81708p+90,0x8.DC5C79p-72},{0x0.1D8DDAp-55,(-0x6.Dp+1),0x9.C1DCBFp-44,(-0x6.3p+1),(-0x8.Bp-1),(-0x10.Ap+1),(-0x1.Ep+1)}},{{0xF.CF1751p+53,(-0x1.Ep+1),(-0x6.Dp+1),0x3.9E9962p-87,0x4.0AE422p+20,0x9.FEF026p-4,0x9.C95134p+1},{(-0x1.Ep-1),0x6.4p+1,0x1.0p-1,(-0x7.0p-1),0x9.FEF026p-4,0x7.3687BCp+19,0x8.Cp-1},{(-0x1.Ep-1),0x6.3C4F93p+81,0x4.723B37p+33,(-0x1.Ep+1),(-0x1.8p-1),0x4.0AE422p+20,(-0x7.0p-1)}},{{0xF.CF1751p+53,0xD.C00020p-78,0x1.Bp-1,0x4.723B37p+33,0x0.1D8DDAp-55,0x0.1D8DDAp-55,0x4.723B37p+33},{0x0.1D8DDAp-55,0x7.3687BCp+19,0x0.1D8DDAp-55,0x8.2096D3p-6,0x6.4p+1,0x6.3C4F93p+81,(-0x1.6p+1)},{(-0x7.0p-1),(-0x1.Cp+1),0x9.6A698Fp+37,0x9.C95134p+1,0x1.Bp-1,0xF.CF1751p+53,0x6.B5D328p+42}},{{0x9.C1DCBFp-44,0x4.723B37p+33,(-0x10.Ap+1),0x4.0AE422p+20,0x9.C95134p+1,0x6.3C4F93p+81,0xE.35333Bp+68},{0x3.9E9962p-87,(-0x3.9p-1),0x7.0p-1,0x1.Fp-1,0x2.1p-1,0x0.1D8DDAp-55,(-0x1.Ep-1)},{0x1.7p+1,(-0x1.6p+1),0x9.C95134p+1,(-0x9.7p+1),0x7.3687BCp+19,0x4.0AE422p+20,0xD.C00020p-78}},{{(-0x8.Bp-1),0xF.CF1751p+53,0x1.7p+1,(-0x1.Cp+1),(-0x6.3p+1),0x7.3687BCp+19,0x9.6A698Fp+37},{(-0x1.6p+1),0x1.Bp-1,0x0.Fp+1,(-0x1.Cp+1),(-0x6.Dp+1),0x9.FEF026p-4,0x1.Fp-1},{0x0.8p+1,(-0x1.8p-1),0xD.930834p+52,(-0x9.7p+1),0xF.CF1751p+53,(-0x10.Ap+1),(-0x6.3p+1)}}};
                    int32_t l_1179 = 0x9AE8926EL;
                    int32_t l_1203 = 1L;
                    struct S0 l_1205[4][9][6] = {{{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{2394,0x74DB6D74L}},{{23235,0x9B5A77DBL},{2394,0x74DB6D74L},{-23755,0L},{2394,0x74DB6D74L},{23235,0x9B5A77DBL},{-27539,1L}},{{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L}},{{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L}},{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{2394,0x74DB6D74L}},{{23235,0x9B5A77DBL},{2394,0x74DB6D74L},{-23755,0L},{2394,0x74DB6D74L},{23235,0x9B5A77DBL},{-27539,1L}},{{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L}}},{{{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L}},{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{2394,0x74DB6D74L}},{{23235,0x9B5A77DBL},{2394,0x74DB6D74L},{-23755,0L},{2394,0x74DB6D74L},{23235,0x9B5A77DBL},{-27539,1L}},{{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L}},{{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L}},{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{2394,0x74DB6D74L}},{{23235,0x9B5A77DBL},{2394,0x74DB6D74L},{-23755,0L},{2394,0x74DB6D74L},{23235,0x9B5A77DBL},{-27539,1L}}},{{{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L}},{{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L}},{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{2394,0x74DB6D74L}},{{23235,0x9B5A77DBL},{2394,0x74DB6D74L},{-23755,0L},{2394,0x74DB6D74L},{23235,0x9B5A77DBL},{-27539,1L}},{{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L}},{{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L}},{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{2394,0x74DB6D74L}}},{{{23235,0x9B5A77DBL},{2394,0x74DB6D74L},{-23755,0L},{2394,0x74DB6D74L},{23235,0x9B5A77DBL},{-27539,1L}},{{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L}},{{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L},{-21639,0xE9B094F9L},{-20815,1L}},{{23235,0x9B5A77DBL},{-27539,1L},{-21639,0xE9B094F9L},{-31650,1L},{-21639,0xE9B094F9L},{-27539,1L}},{{-21639,0xE9B094F9L},{12182,0x1D025C10L},{-23755,0L},{-31650,1L},{32207,0x68C91364L},{-20815,1L}},{{-21639,0xE9B094F9L},{-20815,1L},{32207,0x68C91364L},{-20815,1L},{-21639,0xE9B094F9L},{2394,0x74DB6D74L}},{{-23755,0L},{-20815,1L},{7092,0x4BDB503CL},{-31650,1L},{23235,0x9B5A77DBL},{-31650,1L}},{{-23755,0L},{-27539,1L},{-23755,0L},{-20815,1L},{7092,0x4BDB503CL},{-31650,1L}},{{-21639,0xE9B094F9L},{2394,0x74DB6D74L},{7092,0x4BDB503CL},{12182,0x1D025C10L},{7092,0x4BDB503CL},{2394,0x74DB6D74L}}}};
                    int i, j, k;
                    for (g_76 = (-13); (g_76 <= 35); g_76++)
                    { /* block id: 550 */
                        const uint64_t *l_1173 = (void*)0;
                        const uint64_t **l_1172 = &l_1173;
                        int16_t *l_1176 = (void*)0;
                        int16_t *l_1177 = &g_11[4][6];
                        l_1179 ^= ((((safe_add_func_int64_t_s_s((3L ^ ((*l_1113) = (((safe_mul_func_int16_t_s_s((&p_20 == ((*l_1172) = &g_902)), ((*l_1177) = ((l_1072 == (void*)0) | (((l_906 , 0x5CL) , g_1174[0]) != (void*)0))))) || p_21) && (*g_1075)))), p_21)) , (void*)0) == g_502) < l_1149);
                        if (g_162)
                            goto lbl_1180;
                    }
                    if ((((safe_div_func_float_f_f(((((safe_sub_func_int16_t_s_s((safe_lshift_func_int8_t_s_u((safe_div_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u((l_1193[1][3][0] >= (1UL && 7UL)), (safe_rshift_func_uint16_t_u_s((l_1083[2][1] != l_1196), (l_1179 = (((((!0x1.3p-1) == ((((safe_div_func_float_f_f((p_22 = (l_1200 == (void*)0)), 0x0.4p+1)) > (-0x9.Dp-1)) < (-0x7.9p-1)) >= 0x4.F63203p+6)) , (*g_185)) && g_1202) , p_19)))))), l_1130)), l_1203)), l_906.f1)), 0x2DB5L)) >= p_21) , (*g_863)) >= l_1150), p_21)) != l_1204) , p_19))
                    { /* block id: 559 */
                        l_1205[0][8][5] = l_1205[3][5][3];
                        l_1124[0] &= p_19;
                    }
                    else
                    { /* block id: 562 */
                        int32_t **l_1206[8] = {&g_293,&g_293,&g_293,&g_293,&g_293,&g_293,&g_293,&g_293};
                        int i;
                        (*g_793) = &l_1025[1][0];
                        if ((**g_311))
                            break;
                    }
                }
                (*g_185) ^= ((void*)0 == l_1207);
            }
            for (l_980 = 0; (l_980 <= 3); l_980 += 1)
            { /* block id: 571 */
                const int32_t l_1215 = 0x770E82A1L;
                (*g_823) = ((safe_mul_func_uint16_t_u_u((((+p_21) <= p_19) == (safe_sub_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(l_1215, (safe_div_func_int32_t_s_s(l_1215, ((*g_77) , (l_1218 && (safe_lshift_func_uint8_t_u_u(l_1221, ((l_1215 < (safe_rshift_func_uint16_t_u_u(l_906.f0, p_19))) ^ g_573))))))))), l_1143))), g_1027[0][3][0])) , (void*)0);
                for (g_809 = 0; (g_809 <= 3); g_809 += 1)
                { /* block id: 575 */
                    int32_t l_1241[6][7][6] = {{{(-8L),8L,0xA64C22F2L,(-1L),(-1L),0xA64C22F2L},{6L,6L,0xAC089687L,0xDF525F3EL,0x0086C54CL,0x9E31E6EDL},{0xCC2D44CFL,0xACC30378L,0x1CCFB5F0L,(-1L),8L,0xAC089687L},{(-3L),0xCC2D44CFL,0x1CCFB5F0L,(-1L),6L,0x9E31E6EDL},{0xB8C035D3L,(-1L),0xAC089687L,0x731F7214L,0xACC30378L,0xA64C22F2L},{0x731F7214L,0xACC30378L,0xA64C22F2L,0x0086C54CL,0xCC2D44CFL,0xA64C22F2L},{(-3L),0x98363B1DL,0xAC089687L,0x14441FDEL,(-1L),0x9E31E6EDL}},{{(-1L),(-9L),0x1CCFB5F0L,0xDF525F3EL,0xACC30378L,0xAC089687L},{0x98363B1DL,0x0086C54CL,0x1CCFB5F0L,0x0086C54CL,0x98363B1DL,0x9E31E6EDL},{(-8L),0xCC2D44CFL,0xAC089687L,0xB8C035D3L,(-9L),0xA64C22F2L},{0xB8C035D3L,(-9L),0xA64C22F2L,0xCC2D44CFL,0x0086C54CL,0xA64C22F2L},{0x98363B1DL,(-3L),0xAC089687L,(-1L),0xCC2D44CFL,0x9E31E6EDL},{0x0086C54CL,8L,0x1CCFB5F0L,0x14441FDEL,(-9L),0xAC089687L},{6L,(-1L),0x1CCFB5F0L,0xCC2D44CFL,(-3L),0x9E31E6EDL}},{{0x731F7214L,0x0086C54CL,0xAC089687L,(-8L),8L,0xA64C22F2L},{(-8L),8L,0xA64C22F2L,(-1L),(-1L),0xA64C22F2L},{6L,6L,0xAC089687L,0xDF525F3EL,0x0086C54CL,0x9E31E6EDL},{0xCC2D44CFL,0xACC30378L,0x1CCFB5F0L,(-1L),8L,0xAC089687L},{(-3L),0xCC2D44CFL,0x1CCFB5F0L,(-1L),6L,0x9E31E6EDL},{0xB8C035D3L,(-1L),0xAC089687L,0x731F7214L,0xACC30378L,0xA64C22F2L},{0x731F7214L,0xACC30378L,0xA64C22F2L,0x0086C54CL,0xCC2D44CFL,0xA64C22F2L}},{{(-3L),0x98363B1DL,0xAC089687L,0x14441FDEL,(-1L),0x9E31E6EDL},{(-1L),(-9L),0x1CCFB5F0L,0xDF525F3EL,0xACC30378L,0xAC089687L},{0x98363B1DL,0x0086C54CL,0x1CCFB5F0L,0x0086C54CL,0x98363B1DL,0x9E31E6EDL},{(-8L),0xCC2D44CFL,0xAC089687L,0xB8C035D3L,(-9L),0xA64C22F2L},{0xB8C035D3L,(-9L),0xA64C22F2L,0xCC2D44CFL,0x0086C54CL,0xA64C22F2L},{0x98363B1DL,(-3L),0xB8C035D3L,(-8L),0x77ABA9B6L,(-1L)},{0L,(-1L),(-9L),(-9L),0L,0xB8C035D3L}},{{0L,0x0497346EL,(-9L),0x77ABA9B6L,0L,(-1L)},{0x78D944A8L,0L,0xB8C035D3L,0x1958DB80L,(-1L),0x14441FDEL},{0x1958DB80L,(-1L),0x14441FDEL,0x0497346EL,0x0497346EL,0x14441FDEL},{0L,0L,0xB8C035D3L,0xFD96DEB2L,0L,(-1L)},{0x77ABA9B6L,0x0F2F7356L,(-9L),(-8L),(-1L),0xB8C035D3L},{0L,0x77ABA9B6L,(-9L),0x0497346EL,0L,(-1L)},{(-7L),0x0497346EL,0xB8C035D3L,0x78D944A8L,0x0F2F7356L,0x14441FDEL}},{{0x78D944A8L,0x0F2F7356L,0x14441FDEL,0L,0x77ABA9B6L,0x14441FDEL},{0L,0xCFAACB6AL,0xB8C035D3L,(-9L),0x0497346EL,(-1L)},{0x0497346EL,0L,(-9L),0xFD96DEB2L,0x0F2F7356L,0xB8C035D3L},{0xCFAACB6AL,0L,(-9L),0L,0xCFAACB6AL,(-1L)},{0x1958DB80L,0x77ABA9B6L,0xB8C035D3L,(-7L),0L,0x14441FDEL},{(-7L),0L,0x14441FDEL,0x77ABA9B6L,0L,0x14441FDEL},{0xCFAACB6AL,0L,0xB8C035D3L,(-8L),0x77ABA9B6L,(-1L)}}};
                    int i, j, k;
                    for (g_143 = 0; (g_143 <= 3); g_143 += 1)
                    { /* block id: 578 */
                        uint64_t l_1224 = 18446744073709551609UL;
                        --l_1224;
                    }
                    if (p_19)
                        continue;
                    (*g_185) &= (l_1227 , l_954);
                    for (g_55 = 3; (g_55 >= 0); g_55 -= 1)
                    { /* block id: 585 */
                        int i, j, k;
                        l_1025[1][0] = (((l_953[(l_980 + 4)][(g_55 + 2)][g_55] >= (safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f(((((safe_div_func_float_f_f(((-0x1.Cp+1) <= ((*g_290) = (*g_290))), ((!l_1161) != l_1215))) , (safe_div_func_float_f_f((p_22 = 0x9.9p+1), (safe_sub_func_float_f_f((p_19 , (p_21 > (*g_863))), l_1082[2][0][3]))))) == l_1241[1][3][4]) >= l_953[(l_980 + 4)][(g_55 + 2)][g_55]), p_21)), 0x1.2p-1)), p_20))) == p_21) != g_1000);
                    }
                }
            }
        }
        else
        { /* block id: 592 */
            uint64_t l_1246 = 1UL;
            float *l_1255 = &l_1152;
            for (g_1000 = 0; (g_1000 <= 3); g_1000 += 1)
            { /* block id: 595 */
                uint32_t l_1254 = 18446744073709551613UL;
                uint8_t **l_1283 = &g_77;
                uint8_t ***l_1282 = &l_1283;
                uint8_t ****l_1281[8][6] = {{&l_1282,&l_1282,&l_1282,&l_1282,&l_1282,&l_1282},{&l_1282,&l_1282,&l_1282,&l_1282,&l_1282,&l_1282},{&l_1282,(void*)0,&l_1282,(void*)0,&l_1282,&l_1282},{&l_1282,&l_1282,(void*)0,&l_1282,&l_1282,&l_1282},{&l_1282,&l_1282,&l_1282,&l_1282,&l_1282,&l_1282},{&l_1282,&l_1282,(void*)0,&l_1282,&l_1282,&l_1282},{&l_1282,(void*)0,&l_1282,(void*)0,&l_1282,&l_1282},{(void*)0,&l_1282,&l_1282,&l_1282,&l_1282,&l_1282}};
                int i, j;
                if (p_21)
                    break;
                l_926 &= p_20;
                for (g_162 = 0; (g_162 <= 3); g_162 += 1)
                { /* block id: 600 */
                    int32_t *l_1242 = &l_980;
                    int32_t *l_1243 = (void*)0;
                    int32_t *l_1244 = &l_1024;
                    int32_t *l_1245 = &g_144[2][4][0];
                    int32_t l_1258 = 0xA330ED0DL;
                    uint32_t **l_1278 = &g_484;
                    uint8_t *****l_1284 = &l_1281[5][5];
                    struct S0 l_1285 = {-34967,0x76BE6EB2L};
                    l_1246--;
                    for (l_1218 = 0; (l_1218 <= 2); l_1218 += 1)
                    { /* block id: 604 */
                        int32_t **l_1249 = &l_1243;
                        int i, j, k;
                        (*l_1249) = l_1242;
                    }
                    if ((safe_mod_func_int32_t_s_s((safe_mul_func_int8_t_s_s((l_1254 , (((((g_144[8][3][0] , (g_1256 = l_1255)) == l_1072) < (&l_998[0] == (void*)0)) || (p_21 & g_11[1][6])) , (-4L))), p_20)), 0x88C669D3L)))
                    { /* block id: 608 */
                        int16_t *l_1257[5] = {&g_228[4],&g_228[4],&g_228[4],&g_228[4],&g_228[4]};
                        int32_t *l_1259[4] = {&l_980,&l_980,&l_980,&l_980};
                        int i;
                        l_1258 &= (p_21 || ((*l_1242) = (g_847[0][2][1] = (&g_902 != &p_20))));
                        (*g_1260) = l_1259[2];
                    }
                    else
                    { /* block id: 613 */
                        uint64_t *l_1279 = &g_902;
                        int32_t l_1280[7] = {0x8E9571B1L,0x8E9571B1L,0x375BB804L,0x8E9571B1L,0x8E9571B1L,0x375BB804L,0x8E9571B1L};
                        int i;
                        if (l_1261)
                            break;
                        (*g_185) = (safe_mod_func_uint64_t_u_u((((*l_1279) = (safe_div_func_int32_t_s_s(l_1266[0], (safe_sub_func_int32_t_s_s(l_1266[0], (safe_unary_minus_func_uint64_t_u((safe_rshift_func_int16_t_s_u((safe_add_func_uint64_t_u_u((((*l_1244) | 0x0C1CL) , l_1246), (p_20 != ((p_21 || ((((safe_div_func_uint32_t_u_u(0x3A384305L, (safe_sub_func_uint16_t_u_u(((&l_1072 != l_1278) & l_954), 4UL)))) == 5L) , p_20) || 3L)) & 0x5AL)))), 5))))))))) && p_19), l_1280[1]));
                    }
                    (*l_1284) = l_1281[5][5];
                    for (g_899 = 0; (g_899 <= 2); g_899 += 1)
                    { /* block id: 621 */
                        struct S0 **** const l_1286 = &g_823;
                        (*l_1244) &= ((l_1285 , (void*)0) != l_1286);
                    }
                }
            }
            for (l_1007 = 7; (l_1007 >= 29); l_1007++)
            { /* block id: 628 */
                int32_t *l_1289 = (void*)0;
                int32_t *l_1290 = &l_925[0][2];
                int32_t *l_1291 = &l_980;
                int32_t *l_1292 = &l_926;
                int32_t *l_1293 = &l_925[1][0];
                int32_t *l_1294 = &g_1139;
                int32_t *l_1295 = &g_51[1][0][8];
                int32_t l_1296 = 0L;
                int32_t *l_1297 = &l_1025[4][0];
                int32_t *l_1298 = &l_926;
                int32_t *l_1299 = &l_925[0][1];
                int32_t *l_1300 = &l_1227.f1;
                int32_t *l_1301 = &l_960.f1;
                int32_t *l_1302 = (void*)0;
                int32_t *l_1303 = &g_55;
                int32_t *l_1304 = &l_951;
                int32_t *l_1305[7][8] = {{&g_1139,&l_925[1][0],&l_926,&l_1024,&l_1024,&l_926,&l_925[1][0],&g_1139},{&g_144[2][4][0],&l_1025[3][0],&l_926,&g_144[2][4][0],&l_1296,&g_144[2][4][0],&l_1296,&g_144[2][4][0]},{&l_1024,&g_61.f1,&l_1024,(void*)0,&g_144[2][4][0],&g_144[2][4][0],&l_926,&l_926},{&l_926,&l_1025[3][0],&g_144[2][4][0],&g_144[2][4][0],&l_1025[3][0],&l_926,&g_144[2][4][0],&l_1296},{&l_1024,&g_144[2][4][0],&g_61.f1,&g_144[2][4][0],&l_925[1][0],&g_144[2][4][0],&g_61.f1,&g_144[2][4][0]},{&l_1025[3][0],&g_61.f1,&l_926,&g_144[2][4][0],(void*)0,&g_1139,&g_1139,(void*)0},{&l_1296,(void*)0,(void*)0,&l_1296,&l_1025[3][0],&g_144[2][4][0],&g_1139,&l_1024}};
                int i, j;
                l_1306[0][8][0]--;
            }
        }
        if ((safe_rshift_func_int8_t_s_u(0xBFL, (safe_lshift_func_int8_t_s_s(p_19, ((((**l_1200) = (l_1026 <= 1L)) & (safe_mul_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((safe_mod_func_int16_t_s_s(p_20, (safe_sub_func_int64_t_s_s((+((((safe_rshift_func_int16_t_s_s((((p_21 == ((**l_989)++)) < (safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((((safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((p_19 >= 0x2FL), 7)), (0xFE18ECE7L < 4L))) < 0x5D673176L) != 0x31L), 13)), p_20))) != 4UL), l_1221)) && 65532UL) && l_1334) != p_20)), (-10L))))), p_21)), 0x0BE4L))) , p_21))))))
        { /* block id: 634 */
            int64_t l_1339 = 0x2F3DE89A12D8190BLL;
            uint64_t *l_1340 = &l_965[0][2][2];
            uint16_t *****l_1357[2];
            int32_t *l_1367 = &g_144[2][4][0];
            int64_t l_1436 = 0x72FEF1E13A0DA2DALL;
            int8_t **l_1452[5][10] = {{(void*)0,&l_1201,&l_1201,&l_1201,(void*)0,(void*)0,(void*)0,&l_1201,&l_1201,&l_1201},{&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201},{&l_1201,&l_1201,&l_1201,&l_1201,(void*)0,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201},{&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,(void*)0,&l_1201,&l_1201,&l_1201,&l_1201},{(void*)0,&l_1201,&l_1201,&l_1201,&l_1201,&l_1201,(void*)0,&l_1201,&l_1201,&l_1201}};
            float * const l_1460 = &g_291;
            int32_t *l_1463 = &l_925[1][0];
            int32_t *l_1464 = &l_925[1][0];
            int32_t *l_1465 = &l_1058;
            int32_t *l_1466 = (void*)0;
            int32_t *l_1467 = (void*)0;
            int32_t *l_1468[9] = {(void*)0,&l_926,(void*)0,(void*)0,&l_926,(void*)0,(void*)0,&l_926,(void*)0};
            uint8_t l_1471 = 1UL;
            int i, j;
            for (i = 0; i < 2; i++)
                l_1357[i] = &l_1356[3];
            for (l_1117 = 0; (l_1117 == 1); l_1117 = safe_add_func_int16_t_s_s(l_1117, 8))
            { /* block id: 637 */
                (*g_1256) = (safe_div_func_float_f_f((l_1339 , (*g_290)), (*g_1256)));
                l_926 = p_21;
            }
            if (((((*l_1340) = p_19) || (safe_div_func_int8_t_s_s(l_1227.f1, (p_20 , (safe_add_func_int8_t_s_s((!(safe_sub_func_uint64_t_u_u(0UL, p_20))), (~l_1266[2]))))))) != ((safe_div_func_uint16_t_u_u(65534UL, (safe_add_func_int32_t_s_s((safe_rshift_func_int8_t_s_u((safe_unary_minus_func_int8_t_s(((l_1356[2] = l_1356[3]) != l_1358))), 7)), (**g_1074))))) , p_21)))
            { /* block id: 643 */
                uint32_t l_1366[6] = {4294967295UL,1UL,4294967295UL,4294967295UL,1UL,4294967295UL};
                int32_t * const l_1391 = (void*)0;
                const uint32_t l_1405 = 0x88F9B2A1L;
                int i;
                for (l_1161 = 0; (l_1161 != 57); l_1161++)
                { /* block id: 646 */
                    for (l_1221 = 0; (l_1221 > (-30)); l_1221 = safe_sub_func_uint16_t_u_u(l_1221, 7))
                    { /* block id: 649 */
                        (*g_185) &= (((*g_1256) = l_1366[0]) , p_21);
                        return (**g_186);
                    }
                    return p_21;
                }
                (*g_1368) = l_1367;
                for (l_1157 = 0; (l_1157 >= 10); l_1157 = safe_add_func_int8_t_s_s(l_1157, 1))
                { /* block id: 659 */
                    int32_t *l_1382 = &l_980;
                    uint8_t *** const *l_1384 = (void*)0;
                    uint8_t *** const ** const l_1383[4][8][7] = {{{&l_1384,(void*)0,(void*)0,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,(void*)0,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,(void*)0,&l_1384,&l_1384,&l_1384,&l_1384,(void*)0},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384}},{{&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,(void*)0,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,(void*)0,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384}},{{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,(void*)0,(void*)0,&l_1384,&l_1384,(void*)0,&l_1384},{&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384,(void*)0},{&l_1384,&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,(void*)0,&l_1384}},{{&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,(void*)0,&l_1384,&l_1384,&l_1384},{&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384,&l_1384}}};
                    int16_t *l_1437 = (void*)0;
                    int16_t *l_1438 = &l_1117;
                    int32_t l_1451[7][8][4] = {{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}},{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}},{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}},{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}},{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}},{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}},{{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L},{8L,0L,0L,8L}}};
                    int i, j, k;
                }
            }
            else
            { /* block id: 689 */
                l_1452[0][9] = (void*)0;
            }
            (*l_1463) |= ((*l_1367) = ((safe_sub_func_float_f_f(((*l_1460) = (((&g_1386 == &g_1386) <= (l_965[0][2][2] <= (safe_lshift_func_uint16_t_u_s(0x8682L, 10)))) , (safe_add_func_float_f_f((l_1024 == (!(l_1072 == l_1460))), (*g_863))))), ((safe_sub_func_float_f_f(0xF.304C1Dp+19, 0x1.119CB0p+73)) >= 0x4.2B3AD2p+21))) , 6L));
            l_1471--;
        }
        else
        { /* block id: 696 */
            int32_t *l_1474[7] = {&g_165.f1,&g_165.f1,(void*)0,&g_165.f1,&g_165.f1,(void*)0,&g_165.f1};
            uint32_t l_1476 = 6UL;
            int i;
            ++l_1476;
        }
    }
    g_144[2][4][0] ^= ((((safe_lshift_func_uint16_t_u_u(65535UL, 3)) , (g_847[0][2][1] , ((safe_add_func_uint16_t_u_u(((!(~(l_951 = l_1485))) <= (((((safe_mul_func_float_f_f((l_1440[3][0] , ((l_926 != (*g_1256)) == (0xD.7C0AF7p+12 < ((l_1490 >= (safe_div_func_float_f_f(0x0.FA050Cp-19, 0x0.A7F02Ep-85))) != p_21)))), 0xC.CF3B5Ep-71)) == l_1440[3][0]) <= 0x0.4p-1) == (*g_863)) , p_21)), 1L)) || p_21))) , 3L) != p_21);
    return p_19;
}


/* ------------------------------------------ */
/* 
 * reads : g_61.f1 g_144 g_50 g_68 g_77 g_36 g_2 g_11 g_362 g_228 g_162 g_255 g_285 g_335 g_51 g_290 g_291 g_203 g_204 g_311 g_185 g_54 g_165.f0 g_165.f1 g_502 g_186 g_55 g_484 g_196 g_143 g_573 g_76 g_596 g_49 g_86 g_61 g_672 g_85 g_480 g_722 g_739 g_743 g_793 g_801 g_821 g_822 g_823 g_190 g_863 g_847
 * writes: g_335 g_68 g_228 g_162 g_143 g_291 g_36 g_86 g_293 g_50 g_54 g_165.f1 g_285 g_480 g_484 g_55 g_289 g_77 g_573 g_739 g_673 g_801 g_828 g_832 g_255 g_847 g_823
 */
static float  func_28(const struct S0  p_29, uint8_t * p_30, int64_t  p_31, int16_t  p_32, uint16_t  p_33)
{ /* block id: 152 */
    int16_t l_316 = (-1L);
    int8_t l_338 = 0xF0L;
    int32_t l_342 = 6L;
    int32_t l_419 = 0L;
    int32_t *l_513 = &l_342;
    uint8_t l_524 = 1UL;
    int32_t l_567 = (-1L);
    int32_t l_568[10][7][3] = {{{(-1L),0x02D8A8B2L,(-1L)},{1L,(-1L),(-2L)},{(-6L),0x7422E1D8L,0x778ED3A0L},{0x4417DB7DL,0x63FFD87AL,(-4L)},{0x5733F39DL,0x7422E1D8L,(-1L)},{0x757915E8L,(-1L),(-7L)},{0xF52A11FEL,0x02D8A8B2L,0xF52A11FEL}},{{0x4417DB7DL,(-4L),(-7L)},{0x237A9FF5L,0x2D395C6BL,(-1L)},{1L,1L,(-2L)},{0xF52A11FEL,0x2D395C6BL,0xF4D85337L},{(-1L),0x63FFD87AL,(-7L)},{(-1L),(-1L),0x5DC651B0L},{0x757915E8L,0x757915E8L,(-2L)}},{{0x778ED3A0L,(-1L),0xF52A11FEL},{1L,0x63FFD87AL,0xB2D5BB77L},{(-1L),0x2D395C6BL,(-1L)},{0x757915E8L,1L,0xB2D5BB77L},{0xB0785C3CL,0x7422E1D8L,0xF52A11FEL},{(-1L),(-2L),(-2L)},{0x5733F39DL,0x2D395C6BL,0x5DC651B0L}},{{(-1L),0x4417DB7DL,(-7L)},{0xB0785C3CL,(-1L),0xF4D85337L},{0x757915E8L,(-4L),(-2L)},{(-1L),(-1L),0x5733F39DL},{1L,0x4417DB7DL,0xB2D5BB77L},{0x778ED3A0L,0x2D395C6BL,0x778ED3A0L},{0x757915E8L,(-2L),0xB2D5BB77L}},{{(-1L),0x7422E1D8L,0x5733F39DL},{(-1L),1L,(-2L)},{0xF52A11FEL,0x2D395C6BL,0xF4D85337L},{(-1L),0x63FFD87AL,(-7L)},{(-1L),(-1L),0x5DC651B0L},{0x757915E8L,0x757915E8L,(-2L)},{0x778ED3A0L,(-1L),0xF52A11FEL}},{{1L,0x63FFD87AL,0xB2D5BB77L},{(-1L),0x2D395C6BL,(-1L)},{0x757915E8L,1L,0xB2D5BB77L},{0xB0785C3CL,0x7422E1D8L,0xF52A11FEL},{(-1L),(-2L),(-2L)},{0x5733F39DL,0x2D395C6BL,0x5DC651B0L},{(-1L),0x4417DB7DL,(-7L)}},{{0xB0785C3CL,(-1L),0xF4D85337L},{0x757915E8L,(-4L),(-2L)},{(-1L),(-1L),0x5733F39DL},{1L,0x4417DB7DL,0xB2D5BB77L},{0x778ED3A0L,0x2D395C6BL,0x778ED3A0L},{0x757915E8L,(-2L),0xB2D5BB77L},{(-1L),0x7422E1D8L,0x5733F39DL}},{{(-1L),1L,(-2L)},{0xF52A11FEL,0x2D395C6BL,0xF4D85337L},{(-1L),0x63FFD87AL,(-7L)},{(-1L),(-1L),0x5DC651B0L},{0x757915E8L,0x757915E8L,(-2L)},{0x778ED3A0L,(-1L),0xF52A11FEL},{1L,0x63FFD87AL,0xB2D5BB77L}},{{(-1L),0x2D395C6BL,(-1L)},{0x757915E8L,1L,0xB2D5BB77L},{0xB0785C3CL,0x7422E1D8L,0xF52A11FEL},{(-1L),(-2L),(-2L)},{0x5733F39DL,0x2D395C6BL,0x5DC651B0L},{(-1L),0x4417DB7DL,(-7L)},{0xB0785C3CL,(-1L),0xF4D85337L}},{{0x757915E8L,(-4L),(-2L)},{(-1L),(-1L),0x5733F39DL},{1L,0x4417DB7DL,0xB2D5BB77L},{0x778ED3A0L,0x2D395C6BL,0x778ED3A0L},{0x757915E8L,(-2L),0xB2D5BB77L},{(-1L),0x7422E1D8L,0x5733F39DL},{(-1L),1L,(-2L)}}};
    int16_t l_603 = 0L;
    uint32_t **l_626 = &g_480;
    uint16_t ** volatile l_676 = (void*)0;/* VOLATILE GLOBAL l_676 */
    int16_t l_837 = 0xC52FL;
    struct S0 ***l_848 = &g_204;
    uint16_t *l_879 = (void*)0;
    uint16_t *l_881 = &g_335[0][2][0];
    uint32_t l_896 = 0UL;
    uint32_t l_898[2];
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_898[i] = 0x4C89C594L;
    if ((safe_lshift_func_uint16_t_u_s(g_61.f1, 7)))
    { /* block id: 153 */
        int32_t *l_315[2][4][2] = {{{&g_51[1][0][8],&g_51[1][0][8]},{&g_144[1][3][0],&g_51[1][0][8]},{&g_165.f1,&g_165.f1},{&g_165.f1,&g_51[1][0][8]}},{{&g_144[1][3][0],&g_165.f1},{&g_144[1][3][0],&g_165.f1},{(void*)0,&g_144[1][3][0]},{&g_51[1][0][8],&g_51[1][0][8]}}};
        uint16_t l_406 = 65535UL;
        uint16_t *l_416[9] = {&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]};
        uint16_t **l_415 = &l_416[7];
        struct S0 l_424[2] = {{45843,0x4977D6ACL},{45843,0x4977D6ACL}};
        float l_433 = (-0x1.6p+1);
        uint32_t *l_449 = &g_68;
        uint32_t ** const l_448 = &l_449;
        struct S0 *l_625 = &l_424[0];
        uint8_t *l_636 = &g_36;
        int i, j, k;
        if ((l_316 = p_32))
        { /* block id: 155 */
            uint16_t *l_334[7];
            int32_t l_340 = 5L;
            uint32_t *l_341[2][7] = {{&g_68,&g_68,&g_68,&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68,&g_68,&g_68,&g_68}};
            int16_t *l_349 = &g_162;
            uint64_t *l_350 = &g_143;
            const uint32_t l_351 = 0x667029BFL;
            int16_t l_366 = 0xB378L;
            float *l_381 = &g_291;
            struct S0 *l_411[1][10][10] = {{{&g_61,&g_61,&g_61,&g_165,(void*)0,&g_165,&g_61,&g_61,&g_61,&g_61},{&g_165,&g_61,&g_61,&g_61,&g_61,&g_61,&g_61,&g_165,(void*)0,&g_165},{(void*)0,&g_61,&g_165,&g_61,&g_61,&g_165,&g_61,&g_61,&g_165,&g_61},{&g_61,&g_165,&g_165,&g_61,(void*)0,&g_61,&g_61,&g_165,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_165,&g_165,&g_165,&g_165,&g_61,&g_61,&g_61},{(void*)0,&g_61,&g_61,&g_61,&g_61,&g_61,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_61,&g_61,&g_61,&g_61,&g_165,&g_61,&g_61,(void*)0,&g_61},{&g_61,&g_165,&g_61,&g_61,&g_165,&g_61,&g_61,&g_165,&g_61,&g_61},{&g_61,&g_61,&g_61,&g_165,(void*)0,&g_165,&g_61,&g_61,&g_61,&g_61},{&g_165,&g_61,&g_61,&g_61,&g_61,&g_61,&g_61,&g_165,(void*)0,&g_165}}};
            const int8_t *l_485 = &l_338;
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_334[i] = &g_335[0][3][0];
            l_340 = ((((safe_lshift_func_int16_t_s_s((safe_add_func_int64_t_s_s(((safe_mul_func_int16_t_s_s(0L, (((safe_div_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u(((*l_350) = (((*l_349) = ((safe_add_func_uint8_t_u_u(((((~(safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_s((g_228[4] = (((((g_335[0][3][0] = g_144[2][4][0]) | ((safe_sub_func_uint32_t_u_u(l_338, 9UL)) || ((safe_unary_minus_func_uint32_t_u(g_50[1][3][0])) & 18446744073709551614UL))) <= (l_340 < (g_68++))) == (safe_sub_func_int16_t_s_s((safe_mod_func_int64_t_s_s(l_340, ((*g_77) , 0x3B3F291D95F65124LL))), 0xFD87L))) <= (-1L))), 14)), l_316))) == p_29.f1) || g_2) && (-9L)), (*p_30))) <= (*g_77))) == g_2)), p_31)), 5L)) >= l_351) < g_11[4][0]))) | l_351), 1UL)), 10)) ^ p_29.f0) , 9L) <= 0x77L);
            l_342 = (((*l_381) = (safe_sub_func_float_f_f((safe_mul_func_float_f_f(((((safe_add_func_float_f_f(l_351, ((((((safe_sub_func_float_f_f(((g_50[5][3][0] , (safe_mul_func_float_f_f(((void*)0 == g_362[0]), (p_29.f0 == (safe_div_func_float_f_f(((((l_366 >= (safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f((((*l_349) &= (safe_sub_func_int32_t_s_s(g_11[4][5], (g_228[4] && ((void*)0 != &g_55))))) , p_29.f0), g_2)), 0x2.44C800p+30)), l_351)), g_228[4])), g_255))) > p_29.f1) >= l_342) < g_285), g_335[0][9][0])))))) >= l_342), g_2)) <= 0x1.9p-1) <= p_29.f1) , p_29.f0) != 0xD.FD2821p+25) >= p_31))) >= l_342) < l_340) >= 0x2.55BFD8p+0), 0x8.698714p+77)), l_342))) < p_31);
            if ((safe_rshift_func_int16_t_s_s(((safe_add_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u((~(safe_rshift_func_int8_t_s_u(((((safe_mod_func_uint16_t_u_u((safe_sub_func_int16_t_s_s(p_31, ((0x1C40L && (++g_335[0][0][0])) , (l_351 >= (((~g_51[1][0][1]) && (p_29.f0 > ((safe_div_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u((safe_div_func_uint8_t_u_u(((*p_30) = (((safe_mod_func_int8_t_s_s(g_144[0][5][0], g_162)) ^ (&l_338 != &l_338)) < 0L)), 251UL)), g_68)), g_285)) >= p_32))) , p_31))))), p_29.f0)) != l_351) <= g_285) && 0UL), l_406))), 0xB9L)) , 0xFDL), g_144[4][5][0])) == l_316), 15)))
            { /* block id: 167 */
                int8_t l_417 = 0x84L;
                int32_t l_418 = 0x3727745DL;
                uint32_t l_420 = 5UL;
                int32_t *l_423 = &g_165.f1;
                uint64_t l_427 = 1UL;
                int8_t *l_457 = &l_338;
                int32_t l_458 = 0x8EF1B5AAL;
                uint32_t l_459 = 4294967291UL;
                uint32_t l_487 = 1UL;
lbl_517:
                if ((7UL <= (safe_lshift_func_uint8_t_u_s((((p_29 , p_30) == ((*g_290) , p_30)) , (safe_rshift_func_int8_t_s_u((&l_316 != &g_162), 2))), 7))))
                { /* block id: 168 */
                    (**g_203) = l_411[0][4][0];
                    for (g_143 = 0; (g_143 <= 28); ++g_143)
                    { /* block id: 172 */
                        uint16_t **l_414 = &l_334[2];
                        l_415 = l_414;
                        l_340 = (((6L >= g_50[5][3][0]) , p_29.f1) >= (l_417 != 4294967292UL));
                        --l_420;
                        (*g_311) = l_423;
                    }
                }
                else
                { /* block id: 178 */
                    l_424[0] = p_29;
                    for (l_316 = (-21); (l_316 <= (-23)); l_316 = safe_sub_func_uint32_t_u_u(l_316, 8))
                    { /* block id: 182 */
                        if ((*g_185))
                            break;
                        l_427 |= 0x190E7199L;
                        return (*g_290);
                    }
                    for (l_406 = 0; (l_406 == 45); l_406 = safe_add_func_int32_t_s_s(l_406, 3))
                    { /* block id: 189 */
                        uint32_t l_434[4] = {2UL,2UL,2UL,2UL};
                        int i;
                        (*g_185) = (+(safe_add_func_uint64_t_u_u(l_342, ((void*)0 != l_423))));
                        if (l_340)
                            continue;
                        (*g_185) = l_434[0];
                    }
                }
                if ((!((l_458 = ((*l_423) = (safe_rshift_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((p_32 & (((65528UL >= p_29.f0) == (safe_add_func_int16_t_s_s(g_68, ((safe_rshift_func_uint8_t_u_s((*g_77), 1)) == ((*l_457) &= (&g_203 != (((*l_349) = ((safe_add_func_uint64_t_u_u(((safe_add_func_int8_t_s_s((l_448 == &l_449), (safe_rshift_func_int8_t_s_u((safe_mod_func_int8_t_s_s(((safe_mul_func_int8_t_s_s(((safe_unary_minus_func_uint64_t_u(1UL)) ^ g_50[5][3][0]), g_2)) <= 0L), 255UL)), (*g_77))))) || g_228[4]), 0L)) < (*g_77))) , (void*)0))))))) ^ g_165.f0)) && p_31), 0xC0F36C03C98EF9ACLL)), (*g_77))))) || p_31)))
                { /* block id: 199 */
                    uint32_t l_462[6][4][9] = {{{5UL,6UL,9UL,0xBCB01CC6L,4294967295UL,0xF71092A6L,4294967295UL,0x6DF625C1L,4294967290UL},{0x0634E5C6L,0x458C6CE4L,0xBE78E112L,0x09A29872L,0xC3113554L,4294967295UL,0x413EBA81L,1UL,0x3E0784B6L},{0x245B4F4AL,5UL,4294967294UL,0x1D82194DL,0xFD15A72FL,4294967295UL,0x413EBA81L,0xBCB01CC6L,0x74289052L},{4294967289UL,0x039EA269L,0x920138E4L,0x98993C5AL,9UL,1UL,4294967295UL,4294967295UL,1UL}},{{0xAE589828L,0x74289052L,0UL,0x74289052L,0xAE589828L,0xDD569613L,0x9656308CL,0xBCC30DC4L,4294967295UL},{0x6DF625C1L,0x9656308CL,0xAE5680CEL,0UL,4294967290UL,1UL,0x458C6CE4L,0x311969E6L,4294967295UL},{1UL,4294967295UL,0xDD569613L,4294967295UL,0x98993C5AL,0x980C13D9L,4294967295UL,0x7F29A429L,5UL},{0x15D6B1DCL,9UL,4294967294UL,4UL,0xDD569613L,0xFD15A72FL,0x3C2BE4A1L,0x2B4D4061L,0x14CF7988L}},{{4294967295UL,0UL,0xEA100E0DL,0x980C13D9L,0x2B4D4061L,4294967289UL,4294967294UL,0x920138E4L,9UL},{4294967295UL,0UL,0x413EBA81L,0x7F29A429L,0x2B4D4061L,1UL,4294967289UL,0x15D6B1DCL,4294967289UL},{4294967295UL,0x14CF7988L,6UL,4294967286UL,0xDD569613L,4294967287UL,4294967287UL,0xDD569613L,4294967286UL},{0xFD15A72FL,4UL,0xFD15A72FL,0x3E0784B6L,0x98993C5AL,0x1D82194DL,0xAE589828L,0x458C6CE4L,4294967295UL}},{{0xAE589828L,4294967294UL,0x09A29872L,0UL,0xBE78E112L,0x39DBC944L,0x31B5EE36L,0xFD15A72FL,0x7F29A429L},{0xBE78E112L,0xAE589828L,0x15D6B1DCL,0x3E0784B6L,0x039EA269L,5UL,5UL,0xED9CE5A0L,0x920138E4L},{1UL,0x9656308CL,0x3E0784B6L,4294967286UL,0x0634E5C6L,4294967289UL,0UL,5UL,0x6DF625C1L},{4294967286UL,0xBE78E112L,0xAE5680CEL,0x7F29A429L,9UL,0xBCC30DC4L,0xDD569613L,0x1D82194DL,0x3E0784B6L}},{{0x1D82194DL,1UL,0xAE5680CEL,0x980C13D9L,0UL,0x245B4F4AL,0x0634E5C6L,4UL,4294967295UL},{5UL,0x2B4D4061L,0x3E0784B6L,4UL,1UL,1UL,4UL,0x3E0784B6L,0x2B4D4061L},{6UL,4294967289UL,0x15D6B1DCL,4294967295UL,0xEA100E0DL,0x3E0784B6L,0x62857A60L,4294967287UL,0x09A29872L},{0UL,0x6DF625C1L,0x09A29872L,4294967295UL,0x245B4F4AL,0xDD569613L,0x9F054EEFL,1UL,0x3C2BE4A1L}},{{0x74289052L,4294967289UL,0xFD15A72FL,0x39DBC944L,4294967295UL,0x62857A60L,0x15D6B1DCL,0UL,0xED9CE5A0L},{0x09A29872L,0x2B4D4061L,6UL,1UL,0xAE5680CEL,1UL,4294967295UL,0x9656308CL,5UL},{0x7F29A429L,1UL,0x413EBA81L,5UL,4UL,0xAE5680CEL,0x6DF625C1L,0xC3113554L,0x98993C5AL},{0x7F29A429L,0xBE78E112L,0xEA100E0DL,0xAE5680CEL,0x458C6CE4L,0xBCB01CC6L,0x39DBC944L,0UL,1UL}}};
                    int i, j, k;
                    for (l_427 = 0; (l_427 <= 0); l_427 += 1)
                    { /* block id: 202 */
                        if (p_33)
                            break;
                    }
                    l_459++;
                    l_462[1][1][6]++;
                }
                else
                { /* block id: 207 */
                    const int8_t *l_470 = (void*)0;
                    const int8_t **l_469 = &l_470;
                    int64_t *l_475[5];
                    uint32_t * const l_478 = (void*)0;
                    uint32_t *l_479 = &l_459;
                    uint32_t **l_481 = (void*)0;
                    uint32_t **l_482 = (void*)0;
                    uint32_t **l_483 = (void*)0;
                    int32_t l_486[1][4] = {{0x7458C6ADL,0x7458C6ADL,0x7458C6ADL,0x7458C6ADL}};
                    int i, j;
                    for (i = 0; i < 5; i++)
                        l_475[i] = &g_285;
                    (*g_185) &= ((((safe_mul_func_uint8_t_u_u(((((safe_rshift_func_uint16_t_u_s((18446744073709551613UL != (((*l_469) = &l_338) == ((safe_div_func_uint8_t_u_u((((((*l_415) = &p_33) != &p_33) > ((((((*l_457) = p_29.f0) < ((g_285 = (-9L)) == (safe_rshift_func_uint8_t_u_s((l_478 == (g_484 = ((*l_448) = (g_480 = l_479)))), (((l_342 = l_366) , (*l_423)) <= 1L))))) <= 0x568861B0A7452203LL) == g_165.f0) < (*p_30))) >= g_335[0][3][0]), 7UL)) , l_485))), l_486[0][3])) < l_487) && p_31) > (*p_30)), g_228[4])) & g_51[2][2][8]) || p_29.f0) >= p_33);
                    if (((safe_add_func_uint64_t_u_u(g_144[6][6][0], ((p_33 = (safe_rshift_func_int8_t_s_u(((safe_div_func_uint32_t_u_u(((safe_mod_func_uint16_t_u_u(((safe_sub_func_int16_t_s_s(((l_419 ^= ((0xF259A592L ^ (safe_rshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_s(((g_502 == g_502) & ((l_366 , (safe_mod_func_int64_t_s_s(((safe_unary_minus_func_int64_t_s(((safe_div_func_uint64_t_u_u(((*p_30) < l_340), (+0xBE29L))) , (((safe_add_func_int64_t_s_s(g_162, (*l_423))) <= (**g_186)) ^ (*p_30))))) > p_33), p_32))) == p_32)), 5)), l_366))) != g_335[0][3][0])) & (-1L)), g_55)) , 65532UL), (*l_423))) , 7UL), (-10L))) | 0xDCL), (*p_30)))) > (*l_423)))) | 0L))
                    { /* block id: 219 */
                        l_486[0][3] &= ((((*g_484) = (p_32 < (((void*)0 == (*g_203)) | ((void*)0 != &g_255)))) | 0xEFE8513EL) == g_228[4]);
lbl_514:
                        (*g_290) = l_342;
                        return l_486[0][3];
                    }
                    else
                    { /* block id: 224 */
                        int32_t **l_511 = (void*)0;
                        int32_t **l_512 = &g_293;
                        (*l_381) = (*g_290);
                        l_513 = ((*l_512) = &l_340);
                        if (l_338)
                            goto lbl_514;
                    }
                    for (l_459 = 2; (l_459 <= 8); l_459 += 1)
                    { /* block id: 232 */
                        uint32_t ***l_516 = (void*)0;
                        uint32_t ****l_515 = &l_516;
                        (*l_515) = (void*)0;
                        if (l_351)
                            goto lbl_517;
                    }
                }
                if (g_68)
                    goto lbl_517;
            }
            else
            { /* block id: 238 */
                for (l_419 = 0; (l_419 > 16); ++l_419)
                { /* block id: 241 */
                    return (*g_290);
                }
            }
        }
        else
        { /* block id: 245 */
            struct S0 l_520 = {12806,0xDA991648L};
            int32_t * const l_529[7] = {&g_61.f1,&g_61.f1,&g_144[5][0][0],&g_61.f1,&g_61.f1,&g_144[5][0][0],&g_61.f1};
            uint32_t l_547 = 0xDE504678L;
            int8_t l_562 = 0x49L;
            float l_585 = 0x9.8p-1;
            uint8_t *l_587 = (void*)0;
            uint32_t **l_627 = &g_484;
            int8_t l_668 = 0L;
            uint16_t ** volatile *l_675[8];
            int i;
            for (i = 0; i < 8; i++)
                l_675[i] = &g_672;
            (*g_185) |= (l_520 , p_29.f0);
            if (((*l_513) = (*g_185)))
            { /* block id: 248 */
                int32_t l_521[3][5] = {{0xA08A68A7L,0xDB95117AL,0xA08A68A7L,0xDB95117AL,0xA08A68A7L},{(-2L),(-2L),(-2L),(-2L),(-2L)},{0xA08A68A7L,0xDB95117AL,0xA08A68A7L,0xDB95117AL,0xA08A68A7L}};
                int32_t l_522 = 4L;
                int32_t l_523[6] = {1L,0x8AB51282L,0x8AB51282L,0x8AB51282L,0x5FB5C043L,0x5FB5C043L};
                int16_t l_544 = 0x48A3L;
                int8_t *l_545 = &l_338;
                int8_t *l_546[5];
                uint32_t *l_593 = &g_68;
                uint8_t l_610 = 1UL;
                int i, j;
                for (i = 0; i < 5; i++)
                    l_546[i] = &g_255;
                l_524++;
                for (g_55 = 0; (g_55 > (-23)); g_55--)
                { /* block id: 252 */
                    int32_t **l_530 = &g_293;
                    (*l_530) = l_529[5];
                    if (p_32)
                        continue;
                    for (l_316 = (-13); (l_316 == (-19)); --l_316)
                    { /* block id: 257 */
                        return p_31;
                    }
                }
                if (((safe_lshift_func_uint8_t_u_u(((1L && (0L || (safe_sub_func_uint8_t_u_u((((*l_513) | p_32) , (((*l_513) = (safe_lshift_func_int8_t_s_u((l_547 |= (((p_32 = ((!(((0x68L | ((*l_545) = ((safe_mod_func_int16_t_s_s(((g_51[0][1][2] < 18446744073709551615UL) || (safe_rshift_func_uint8_t_u_u((*p_30), (g_196 > l_544)))), p_31)) <= p_29.f0))) , 0x109421A9L) , (*p_30))) , g_335[0][3][0])) || 65528UL) , p_32)), 4))) < 1UL)), 0xAFL)))) | 0xB3EBCF5EL), l_544)) ^ p_31))
                { /* block id: 265 */
                    float *l_549 = &g_289;
                    uint8_t **l_559 = &g_77;
                    int32_t l_560[10];
                    int16_t *l_561 = &l_544;
                    int32_t l_563 = (-5L);
                    uint16_t l_586 = 65527UL;
                    uint8_t l_590[9][6][3] = {{{0UL,1UL,0x2FL},{0UL,1UL,0UL},{0UL,0UL,0x38L},{0x2FL,3UL,1UL},{1UL,7UL,8UL},{0x35L,0UL,1UL}},{{0x25L,1UL,8UL},{6UL,0xE5L,1UL},{248UL,248UL,0x38L},{4UL,9UL,0UL},{1UL,255UL,0x2FL},{0xE5L,4UL,0UL}},{{0UL,0x25L,251UL},{0x2FL,0UL,0UL},{251UL,1UL,255UL},{250UL,0UL,255UL},{0UL,251UL,1UL},{255UL,0UL,1UL}},{{248UL,251UL,1UL},{0UL,0UL,0UL},{1UL,1UL,248UL},{6UL,0UL,255UL},{253UL,0x25L,0x38L},{1UL,4UL,3UL}},{{248UL,255UL,255UL},{0x35L,9UL,0x35L},{0UL,248UL,252UL},{255UL,0xE5L,0UL},{0UL,1UL,5UL},{3UL,0UL,0UL}},{{0UL,7UL,0x8AL},{255UL,3UL,255UL},{0UL,0UL,1UL},{0x35L,1UL,4UL},{248UL,1UL,8UL},{1UL,0xF7L,249UL}},{{253UL,248UL,1UL},{6UL,6UL,3UL},{1UL,0x87L,5UL},{0UL,9UL,0UL},{248UL,0x76L,0UL},{255UL,0UL,0UL}},{{0UL,0UL,5UL},{250UL,1UL,3UL},{251UL,7UL,1UL},{0x2FL,3UL,249UL},{0UL,251UL,8UL},{0xE5L,255UL,4UL}},{{1UL,1UL,1UL},{4UL,0xF7L,255UL},{248UL,0x76L,0x8AL},{6UL,4UL,0UL},{0x25L,253UL,5UL},{0x35L,4UL,0UL}}};
                    int32_t **l_592 = &l_315[1][1][0];
                    uint32_t *l_599 = &g_68;
                    uint32_t ** const l_598 = &l_599;
                    uint32_t ** const *l_597 = &l_598;
                    int i, j, k;
                    for (i = 0; i < 10; i++)
                        l_560[i] = 0x062CA0BCL;
                    if ((l_563 &= (0xD3CAL | ((!(((*l_549) = 0x2.C6C558p-6) , (p_31 = l_523[2]))) & ((safe_lshift_func_int8_t_s_u(((p_32 = p_29.f1) < (l_562 ^= (safe_sub_func_int16_t_s_s(((((safe_rshift_func_int16_t_s_u(((*l_561) = ((g_335[0][3][0] ^= (p_33 | (((p_29.f0 > ((*p_30) != ((safe_sub_func_uint16_t_u_u(g_11[0][1], ((!((((*l_559) = p_30) == &g_76) >= l_523[3])) > l_523[0]))) ^ p_29.f1))) && (-1L)) == l_560[3]))) >= p_29.f0)), 14)) <= p_29.f1) | (*l_513)) == g_11[3][5]), g_55)))), g_196)) >= g_143)))))
                    { /* block id: 274 */
                        int32_t l_564 = (-3L);
                        int32_t l_565 = 0x60AA78F8L;
                        int32_t l_566 = 0xC419E307L;
                        int32_t l_569 = 1L;
                        int32_t l_570 = 0x2A495F7BL;
                        int32_t l_571 = (-1L);
                        int32_t l_572 = 0xA9D470ABL;
                        --g_573;
                        return (*g_290);
                    }
                    else
                    { /* block id: 277 */
                        int32_t **l_576 = &l_315[1][2][1];
                        float l_591 = 0x4.58FAA3p+0;
                        (*l_576) = l_529[5];
                        (*g_185) &= (((((-1L) ^ ((((*p_30) = (*p_30)) || ((safe_div_func_int8_t_s_s((safe_add_func_int8_t_s_s((l_563 , ((*l_545) |= ((safe_rshift_func_uint8_t_u_s(((**l_559) |= 249UL), g_165.f0)) && (safe_sub_func_int16_t_s_s((p_32 || g_228[4]), 65527UL))))), (l_586 , g_255))), p_32)) > g_76)) != 0xEEL)) || (-2L)) , l_587) != p_30);
                        l_563 |= ((*l_513) &= l_586);
                        (*l_513) ^= ((safe_div_func_int16_t_s_s(g_335[0][3][0], g_50[9][0][0])) & l_590[1][4][0]);
                    }
                    (*l_592) = &l_523[2];
                    l_522 |= (l_593 != (((((safe_mul_func_uint8_t_u_u((*g_77), (((p_33 , &l_448) == (l_597 = g_596)) < ((((((((-8L) || (0x5A56DFD5E3458302LL >= ((safe_mul_func_uint8_t_u_u((!(((((&g_596 != (void*)0) , p_32) > 0L) == p_31) , g_49)), (*g_77))) <= p_31))) || p_29.f0) , p_29.f0) == l_603) | 9UL) & 18446744073709551615UL) && 0x0C25L)))) || p_32) , 0x538EB57AL) & p_33) , l_529[4]));
                }
                else
                { /* block id: 290 */
                    uint64_t l_604 = 18446744073709551615UL;
                    int32_t l_607 = 0x2D796259L;
                    int32_t l_608 = 0xF8CC614EL;
                    int32_t l_609 = 0x6607FF3EL;
                    ++l_604;
                    l_610--;
                    for (g_68 = (-20); (g_68 < 60); ++g_68)
                    { /* block id: 295 */
                        float *l_615 = &l_585;
                        float *l_616 = &g_289;
                        float *l_617 = (void*)0;
                        float *l_618 = &l_433;
                        (*l_618) = ((*l_616) = ((*l_615) = p_29.f0));
                        (*g_311) = l_529[5];
                    }
                }
                for (g_143 = 16; (g_143 == 29); ++g_143)
                { /* block id: 304 */
                    float *l_628 = &g_291;
                    (*l_628) = ((safe_div_func_float_f_f(0x0.9p-1, (*g_290))) <= (safe_add_func_float_f_f(((**g_203) == (l_625 = &l_520)), ((((**g_204) , l_626) == l_627) , (-0x5.6p-1)))));
                    for (l_406 = 0; (l_406 > 40); l_406 = safe_add_func_uint32_t_u_u(l_406, 3))
                    { /* block id: 309 */
                        uint16_t l_631 = 0UL;
                        ++l_631;
                        g_289 = ((*l_628) = (safe_sub_func_float_f_f((*g_290), (((l_523[4] = ((*l_513) = p_29.f1)) , (l_636 == &g_36)) , (-g_61.f1)))));
                    }
                    if ((**g_186))
                        continue;
                }
            }
            else
            { /* block id: 318 */
                int64_t *l_661 = &g_285;
                int32_t l_662 = 0L;
                float *l_663 = &l_585;
                int8_t *l_666 = &l_562;
                int32_t l_667[5][3][8] = {{{0xBF0D90F2L,0xA3FE6872L,0x5AA8F102L,0x7AD92928L,0x5AA8F102L,0xA3FE6872L,0xBF0D90F2L,(-4L)},{0x070178A1L,0x5AA8F102L,4L,0x076FDF1DL,1L,(-4L),(-2L),1L},{0x7AD92928L,1L,0x645D804CL,1L,1L,0x645D804CL,1L,0x7AD92928L}},{{0x070178A1L,(-1L),(-2L),0x5AA8F102L,0xBF0D90F2L,0x6501040DL,0xFE444475L,1L},{0x7AD92928L,1L,1L,0x6501040DL,0x5AA8F102L,0x6501040DL,1L,1L},{0x645D804CL,(-1L),1L,(-4L),4L,0x070178A1L,0xBF0D90F2L,1L}},{{1L,0x6501040DL,0x076FDF1DL,0xBF0D90F2L,0x645D804CL,0x645D804CL,0xBF0D90F2L,0x076FDF1DL},{0xBF0D90F2L,0xBF0D90F2L,1L,0x070178A1L,0xA3FE6872L,(-2L),1L,0x7AD92928L},{0xA3FE6872L,(-2L),1L,0x7AD92928L,0xFE444475L,0x076FDF1DL,0xFE444475L,0x7AD92928L}},{{(-2L),1L,(-2L),0x070178A1L,1L,1L,0x6501040DL,0x076FDF1DL},{(-4L),4L,0x070178A1L,0xBF0D90F2L,1L,1L,1L,1L},{(-4L),0xFE444475L,0xFE444475L,(-4L),1L,0xBF0D90F2L,0x7AD92928L,1L}},{{(-2L),0x5AA8F102L,0xBF0D90F2L,0x6501040DL,0xFE444475L,1L,1L,1L},{0xA3FE6872L,0x5AA8F102L,0x7AD92928L,0x5AA8F102L,0xA3FE6872L,0xBF0D90F2L,(-4L),(-1L)},{0xBF0D90F2L,0xFE444475L,0xA3FE6872L,0x076FDF1DL,0x645D804CL,1L,0x5AA8F102L,0x5AA8F102L}}};
                uint8_t l_669 = 255UL;
                int i, j, k;
                (*l_663) = (((safe_div_func_uint8_t_u_u((((((*g_290) , (((((--(*g_77)) , g_165.f1) ^ (safe_add_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((g_335[0][3][0] = (safe_add_func_int16_t_s_s(p_29.f1, ((((safe_sub_func_uint16_t_u_u(((safe_sub_func_int16_t_s_s((g_502 != g_502), p_33)) | (~(safe_mod_func_int64_t_s_s((((((*l_661) = ((8UL == 0x1D41L) , ((safe_sub_func_uint32_t_u_u(((*g_484) = (((safe_sub_func_uint16_t_u_u(g_49, (-7L))) < 0x0F18L) || p_33)), 0x77C6B4E0L)) == 0x1697L))) || p_33) , 1L) >= (-7L)), (*l_513))))), g_11[3][0])) && 0L) | 4294967291UL) || p_32)))), g_61.f0)), 0UL)) >= p_29.f0), (*l_513)))) == p_29.f1) <= 0L)) || l_662) == 0x15L) & 0UL), 0x7AL)) <= 0xA685L) , (*g_290));
                (*l_513) |= (safe_mul_func_int8_t_s_s(p_29.f0, ((*l_666) |= p_29.f0)));
                l_669++;
            }
            l_676 = g_672;
            (**g_203) = (*g_85);
        }
    }
    else
    { /* block id: 331 */
        uint64_t l_677 = 2UL;
        uint16_t *l_687 = &g_335[0][3][0];
        int32_t l_688 = 1L;
        uint64_t *l_695 = &g_143;
        uint64_t l_712 = 0xE10CE2831270F693LL;
        uint16_t l_713 = 6UL;
        int64_t *l_714 = &g_285;
        int32_t l_715 = 1L;
        struct S0 l_732 = {8996,-7L};
        uint32_t **l_741 = (void*)0;
        struct S0 *** const *l_744 = (void*)0;
        uint32_t l_770[7];
        int32_t l_785 = 1L;
        int32_t l_786 = 0x2B8F0E42L;
        int32_t l_787 = 0L;
        int32_t l_788 = (-7L);
        int32_t l_789 = (-8L);
        uint64_t l_790 = 18446744073709551615UL;
        int32_t *l_810 = &l_786;
        int i;
        for (i = 0; i < 7; i++)
            l_770[i] = 0x20934C7EL;
        l_677++;
lbl_724:
        (*g_185) &= (((safe_add_func_uint64_t_u_u(p_32, (!((safe_add_func_uint64_t_u_u(((((l_688 = (safe_lshift_func_uint16_t_u_u(((*g_484) , (*l_513)), ((*l_687) = p_33)))) , (0L > ((safe_add_func_int8_t_s_s(((g_36 < g_61.f1) && (safe_sub_func_uint64_t_u_u(((safe_mod_func_int64_t_s_s(((*l_714) = ((++(*l_695)) ^ ((safe_mul_func_uint8_t_u_u((safe_div_func_int16_t_s_s(g_285, (safe_mul_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((safe_div_func_int16_t_s_s((-1L), p_33)) , (-6L)), (*l_513))), p_29.f1)) , p_29.f1), l_712)) , l_677) , (-1L)), l_713)))), l_713)) && g_76))), p_32)) || (*g_77)), (-2L)))), 0x18L)) == (*p_30)))) == p_29.f1) | l_715), (-1L))) && p_33)))) , p_29) , 0L);
        for (g_162 = 18; (g_162 < 13); g_162 = safe_sub_func_uint8_t_u_u(g_162, 8))
        { /* block id: 340 */
            uint8_t l_729 = 255UL;
            uint32_t ***l_746[7][8] = {{&l_626,&g_739,&l_626,&l_626,(void*)0,(void*)0,&l_626,&l_626},{&l_741,&l_741,&l_626,&g_739,&l_741,(void*)0,&g_739,&l_626},{(void*)0,&g_739,(void*)0,&l_741,(void*)0,&g_739,(void*)0,&l_626},{&g_739,(void*)0,&l_741,&g_739,&l_626,&l_741,&l_741,&l_626},{&l_626,(void*)0,(void*)0,&l_626,&l_626,&g_739,&l_626,&l_741},{&g_739,&l_741,&g_739,&l_626,(void*)0,&l_626,&g_739,&l_741},{(void*)0,&l_741,&l_741,&g_739,&l_741,&g_739,&g_739,&g_739}};
            struct S0 ***l_766 = &g_204;
            struct S0 ****l_765 = &l_766;
            int32_t l_781 = 0xA751CAA6L;
            int32_t l_782[9][9][3] = {{{0x42112283L,0x06037E6DL,0L},{0L,1L,0L},{1L,0x620A9406L,0x8A057039L},{(-3L),0x67A3739DL,3L},{5L,0x78D2FFBFL,0xB0684D93L},{0x76C29F63L,0x9969556FL,0xE7921155L},{5L,0xAE6DDC8AL,0x5E8FF13CL},{(-3L),0xEA75494FL,(-1L)},{1L,(-7L),0xF556CDABL}},{{0L,0x647CDCD1L,0x7117F4D2L},{0x42112283L,0xFCC4DF7DL,0xAEE2F688L},{0x9F969D39L,0xE6EB3E02L,0L},{0xAE6DDC8AL,0x76C29F63L,(-1L)},{1L,3L,0x67A3739DL},{1L,0xAEE2F688L,5L},{(-1L),0x1FEF55F9L,0xB415F05CL},{0L,1L,0x49F5ACF1L},{0x5F482F5CL,5L,4L}},{{0x5431E17AL,0x7117F4D2L,1L},{0x8734ABE3L,0x620A9406L,0L},{0x7117F4D2L,0xEF92C8B4L,0xF556CDABL},{9L,0x73276BD0L,0x8B45B402L},{0xC3FC049EL,0x73276BD0L,1L},{(-7L),0xEF92C8B4L,6L},{0xDFCBCCD5L,0x620A9406L,0x42112283L},{1L,0x7117F4D2L,0xB717E545L},{0xEF60F410L,5L,3L}},{{0x67A3739DL,1L,1L},{0x96C91E82L,0x1FEF55F9L,(-1L)},{(-1L),0xAEE2F688L,1L},{(-1L),3L,0x96C91E82L},{(-4L),0xAC863691L,1L},{0x766D54D8L,0L,1L},{6L,(-1L),8L},{0x50834793L,0x9F969D39L,6L},{(-1L),(-1L),(-4L)}},{{1L,(-1L),0x61353BB3L},{1L,0xEA75494FL,0xA579AEB0L},{0xAE6DDC8AL,1L,0L},{0x4C3E97BBL,1L,0xA579AEB0L},{0xEF92C8B4L,0xE6EB3E02L,0x61353BB3L},{6L,0x9E1C0634L,(-4L)},{0xA3E081EAL,0xF8ED4A76L,6L},{0x76C29F63L,0xE7921155L,8L},{1L,0L,1L}},{{0xB415F05CL,0xDFCBCCD5L,1L},{0x61353BB3L,1L,0x96C91E82L},{0x8B45B402L,0x42112283L,1L},{5L,1L,(-1L)},{0x620A9406L,(-1L),1L},{1L,(-1L),3L},{0x647CDCD1L,0x85B93419L,0xB717E545L},{0x9E1C0634L,1L,0x42112283L},{0xF556CDABL,1L,6L}},{{(-4L),1L,1L},{0x6633BF34L,0x766D54D8L,0x8B45B402L},{0x6633BF34L,0L,0xF556CDABL},{(-4L),0xBF2B6E2BL,0L},{0xF556CDABL,0x76C29F63L,1L},{0x9E1C0634L,3L,4L},{0x647CDCD1L,0xAE6DDC8AL,0x49F5ACF1L},{1L,0xA3E081EAL,0xB415F05CL},{0x620A9406L,0xB0684D93L,5L}},{{5L,3L,0x67A3739DL},{0x8B45B402L,(-1L),0x76C29F63L},{0x61353BB3L,0xB415F05CL,3L},{0xB415F05CL,0xFCC4DF7DL,0xEF60F410L},{1L,0L,1L},{0x76C29F63L,1L,1L},{0xA3E081EAL,0x4C3E97BBL,0L},{6L,0x49F5ACF1L,0xF5F61FF7L},{0xEF92C8B4L,0xEF60F410L,1L}},{{0x4C3E97BBL,0x7DC430F9L,1L},{0xAE6DDC8AL,0xEF60F410L,0x8A057039L},{1L,0x49F5ACF1L,0x78D2FFBFL},{1L,0x4C3E97BBL,3L},{(-1L),1L,(-1L)},{3L,5L,0xEF60F410L},{0x8B45B402L,(-1L),(-1L)},{0x7DF85470L,0x42112283L,0x647CDCD1L},{0x49F5ACF1L,(-4L),(-4L)}}};
            int i, j, k;
            (*g_185) |= p_32;
            for (l_567 = (-15); (l_567 <= 25); ++l_567)
            { /* block id: 344 */
                const int32_t * volatile l_723 = &l_568[6][0][1];/* VOLATILE GLOBAL l_723 */
                struct S0 l_730 = {-29970,0x47F1508EL};
                uint32_t **l_742 = &g_484;
                uint16_t *l_748 = &g_335[0][3][0];
                int64_t l_749 = 1L;
                int32_t l_783 = 0xC5FC9D22L;
                int32_t l_784[2][4][9] = {{{0x3B4E5D0CL,0x0056F379L,0xDF86731EL,1L,0xDF86731EL,0x0056F379L,0x3B4E5D0CL,(-1L),0L},{(-2L),0L,(-2L),1L,1L,(-2L),0L,(-2L),1L},{0xFD972D13L,(-1L),0xDF86731EL,0x71B69FCBL,0x7CEA7507L,0x71B69FCBL,0xDF86731EL,(-1L),0xFD972D13L},{1L,1L,(-1L),1L,1L,1L,1L,(-1L),1L}},{{0xDF86731EL,4L,0L,1L,0x7CEA7507L,(-1L),0x7CEA7507L,1L,0L},{1L,1L,1L,(-1L),1L,1L,1L,1L,(-1L)},{0xFD972D13L,4L,0xFD972D13L,(-1L),0xDF86731EL,0x71B69FCBL,0x7CEA7507L,0x71B69FCBL,0xDF86731EL},{(-2L),1L,1L,(-2L),0L,(-2L),1L,1L,(-2L)}}};
                int i, j, k;
                for (l_712 = 0; (l_712 <= 2); l_712 += 1)
                { /* block id: 347 */
                    for (l_677 = 0; (l_677 <= 0); l_677 += 1)
                    { /* block id: 350 */
                        int32_t **l_725 = (void*)0;
                        int32_t **l_726 = (void*)0;
                        l_723 = (((**l_626)--) , g_722);
                        if (g_55)
                            goto lbl_724;
                        (*g_311) = &l_419;
                    }
                }
                if (p_29.f1)
                    continue;
                if ((**g_186))
                { /* block id: 358 */
                    int32_t l_737 = 1L;
                    int16_t *l_738 = &l_603;
                    float *l_745 = &g_291;
                    int32_t l_780[2][1];
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_780[i][j] = 0x526837D8L;
                    }
                    if ((safe_mul_func_int16_t_s_s(p_29.f0, ((((l_729 < ((l_730 , ((safe_unary_minus_func_int64_t_s(p_29.f1)) >= ((*l_738) &= ((l_732 , ((safe_rshift_func_uint8_t_u_s(((safe_div_func_int16_t_s_s((((0x998F4964L <= (p_29 , l_729)) & p_31) != l_729), 0x4029L)) | g_51[2][0][2]), g_285)) && l_737)) , (*l_513))))) < (*g_77))) >= 65535UL) , p_31) || l_729))))
                    { /* block id: 360 */
                        uint32_t ***l_740[3][1];
                        int i, j;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_740[i][j] = &g_739;
                        }
                        (*l_513) ^= (((g_739 = g_739) == (l_741 = &g_484)) , ((g_2 <= (&g_363 == l_626)) != p_29.f1));
                        return p_32;
                    }
                    else
                    { /* block id: 365 */
                        uint32_t ****l_747 = &l_746[3][5];
                        struct S0 ** const *l_751 = &g_204;
                        struct S0 ** const **l_750 = &l_751;
                        struct S0 ** const ***l_752 = (void*)0;
                        struct S0 ** const ***l_753 = &l_750;
                        (*g_743) = l_742;
                        (*l_513) |= ((l_744 = (void*)0) == ((*l_753) = ((((*p_30) = (((l_745 != (void*)0) > (((((((((*l_747) = l_746[3][6]) != &l_626) , &p_33) != ((*g_672) = (l_748 = (void*)0))) != (l_749 , (l_732.f0 || g_165.f1))) , 0xEFCE7802L) , (void*)0) != l_745)) , 0UL)) < l_749) , l_750)));
                        (*l_513) = (safe_add_func_int32_t_s_s(p_29.f1, (safe_unary_minus_func_uint64_t_u(l_729))));
                    }
                    if ((safe_sub_func_int16_t_s_s(p_33, (safe_mod_func_int32_t_s_s(l_737, (p_29.f0 ^ (safe_mul_func_uint16_t_u_u(((*l_513) >= (((((safe_div_func_uint16_t_u_u((p_33 <= ((p_31 < (((*l_513) , l_744) != l_765)) ^ g_51[0][2][4])), l_677)) , 0x65DB5F1CL) >= 4294967291UL) && (*p_30)) , (*g_77))), 2UL))))))))
                    { /* block id: 376 */
                        const int32_t l_769 = 0x124A3511L;
                        uint8_t ***l_772 = (void*)0;
                        uint8_t **l_774[4];
                        uint8_t ***l_773 = &l_774[1];
                        int32_t *l_775 = &g_61.f1;
                        int32_t *l_776 = &g_51[2][1][3];
                        int32_t *l_777 = &l_688;
                        int32_t *l_778 = &g_55;
                        int32_t *l_779[3];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_774[i] = &g_77;
                        for (i = 0; i < 3; i++)
                            l_779[i] = (void*)0;
                        (*l_745) = (safe_div_func_float_f_f(((l_769 , (*g_290)) != l_770[6]), (!(&p_30 == ((*l_773) = &p_30)))));
                        --l_790;
                        (*g_793) = &l_780[0][0];
                        if (p_29.f1)
                            break;
                    }
                    else
                    { /* block id: 382 */
                        int32_t *l_794 = &l_730.f1;
                        int32_t *l_795 = &l_785;
                        int32_t *l_796 = (void*)0;
                        int32_t *l_797 = &l_730.f1;
                        int32_t *l_798 = &g_55;
                        int32_t *l_799 = &l_781;
                        int32_t *l_800[1][4] = {{&l_784[1][0][4],&l_784[1][0][4],&l_784[1][0][4],&l_784[1][0][4]}};
                        uint32_t *l_808[1][7] = {{&g_809,&g_809,&g_809,&g_809,&g_809,&g_809,&g_809}};
                        int i, j;
                        (*l_513) &= (-3L);
                        g_801[3]--;
                        l_810 = ((l_730.f1 = (safe_div_func_int32_t_s_s((safe_add_func_uint64_t_u_u((p_31 | 0x731149B2423F7A16LL), p_31)), (*g_484)))) , &l_568[6][0][1]);
                        if (p_33)
                            break;
                    }
                    if ((*l_513))
                        break;
                }
                else
                { /* block id: 390 */
                    return (*g_290);
                }
            }
        }
    }
    for (g_143 = 0; (g_143 >= 29); g_143 = safe_add_func_uint16_t_u_u(g_143, 1))
    { /* block id: 398 */
        int32_t **l_813 = (void*)0;
        int32_t **l_814 = &l_513;
        const struct S0 **l_827[3];
        const struct S0 ***l_826 = &l_827[0];
        const struct S0 ****l_825 = &l_826;
        const struct S0 *****l_824 = &l_825;
        uint16_t *l_838[3][6] = {{&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]},{&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]},{&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0],&g_335[0][3][0]}};
        uint16_t l_839 = 65529UL;
        uint64_t l_840 = 1UL;
        int8_t *l_841 = &g_255;
        const float l_842 = 0x8.71EB74p-20;
        int32_t *l_843 = &g_55;
        int64_t *l_854[10][9] = {{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285},{&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285,&g_285}};
        uint32_t ***l_859 = &l_626;
        uint32_t ****l_858 = &l_859;
        int i, j;
        for (i = 0; i < 3; i++)
            l_827[i] = (void*)0;
        (*l_814) = (void*)0;
        (*l_843) ^= (safe_add_func_int8_t_s_s((((safe_mod_func_int8_t_s_s(((*l_841) = (((((7UL && ((-6L) < ((g_821[0][0] == (g_832 = (g_828 = l_824))) > ((*g_77) == (safe_sub_func_uint32_t_u_u((p_31 < ((p_33 |= (254UL | ((((g_335[0][5][0] = (safe_rshift_func_int8_t_s_s((((0x53L != (0x0AL < (*p_30))) <= p_32) | 0xA72BBFEFB99707A8LL), l_837))) , p_32) || 0xACDEE0FEL) <= l_839))) < 0x7DA2L)), p_32)))))) | p_29.f0) & l_840) != p_32) || g_36)), g_144[3][5][0])) , g_144[2][3][0]) && 0x480BA8C76A241CBCLL), l_837));
        for (l_342 = 0; (l_342 < (-25)); l_342 = safe_sub_func_int8_t_s_s(l_342, 4))
        { /* block id: 408 */
            uint64_t *l_846[10] = {(void*)0,(void*)0,&l_840,&l_840,(void*)0,(void*)0,(void*)0,&l_840,&l_840,(void*)0};
            int32_t l_860[4][5][1] = {{{(-1L)},{1L},{(-1L)},{0xAD1B97C8L},{(-1L)}},{{1L},{(-1L)},{0xAD1B97C8L},{(-1L)},{1L}},{{(-1L)},{0xAD1B97C8L},{(-1L)},{1L},{(-1L)}},{{0xAD1B97C8L},{(-1L)},{1L},{(-1L)},{0xAD1B97C8L}}};
            int32_t l_861 = 0x643A3C38L;
            int32_t l_862 = 3L;
            int32_t *l_864 = &g_144[2][4][0];
            int i, j, k;
            (*g_863) = (((g_847[0][2][1] = g_162) < ((((((*g_822) = (*g_822)) != l_848) , (0x614DL || (safe_div_func_uint8_t_u_u((((safe_mul_func_uint8_t_u_u((!(((void*)0 == l_854[2][8]) || ((l_861 = ((+(((*p_30) = (g_144[4][5][0] < (l_860[2][3][0] = (safe_sub_func_uint8_t_u_u((*p_30), (((*l_843) = ((((p_31 = ((l_858 == (void*)0) & 0xE3L)) , (-1L)) <= l_860[1][1][0]) < g_61.f0)) | p_29.f1)))))) >= g_335[0][3][0])) || 1L)) == 0xF40A098FCD635E84LL))), 0UL)) == g_190) && p_29.f0), l_862)))) , 0xF5061469L) ^ p_29.f0)) , p_29.f0);
            l_864 = (void*)0;
            l_862 &= (l_861 |= (((safe_add_func_uint64_t_u_u(0x46FF49DBAC3C11DFLL, (g_285 |= ((safe_mul_func_float_f_f((g_51[1][0][3] , (((void*)0 == &l_838[2][3]) , (p_29.f1 == ((((safe_add_func_uint8_t_u_u(((*l_843) >= 0xAE48C9B5L), ((safe_rshift_func_uint16_t_u_u((((*l_841) ^= p_29.f1) && (*p_30)), 8)) , g_801[3]))) , 0x6.1FA6D1p-69) < g_51[2][1][3]) >= p_29.f1)))), g_55)) , g_847[0][3][7])))) | 0x39F91645L) && 1L));
        }
        for (g_255 = 0; (g_255 != 0); g_255 = safe_add_func_uint16_t_u_u(g_255, 6))
        { /* block id: 425 */
            int16_t *l_875 = (void*)0;
            int16_t *l_876 = &g_228[3];
            uint16_t **l_877 = (void*)0;
            uint16_t **l_878[9][4][3] = {{{(void*)0,&g_673,&l_838[2][1]},{&g_673,&l_838[2][4],(void*)0},{&l_838[1][5],&l_838[2][1],&l_838[1][5]},{&l_838[1][2],&l_838[1][5],&l_838[1][5]}},{{&l_838[1][5],&l_838[1][5],&l_838[1][5]},{&g_673,&l_838[1][5],&l_838[0][2]},{(void*)0,&l_838[2][1],&l_838[1][5]},{&l_838[1][5],&g_673,&l_838[1][5]}},{{&g_673,&g_673,&l_838[1][5]},{&g_673,&g_673,(void*)0},{&l_838[2][1],&l_838[2][1],&l_838[2][1]},{&l_838[1][2],&l_838[1][5],&l_838[1][5]}},{{&l_838[2][1],&l_838[1][5],&g_673},{&g_673,&l_838[1][5],&l_838[0][2]},{&g_673,&l_838[2][1],&g_673},{&l_838[1][5],&l_838[2][4],&l_838[1][5]}},{{(void*)0,&g_673,&l_838[2][1]},{&g_673,&l_838[2][4],(void*)0},{&l_838[1][5],&l_838[2][1],&l_838[1][5]},{&l_838[1][2],&l_838[1][5],&l_838[1][5]}},{{&l_838[1][5],&l_838[1][5],&l_838[1][5]},{&g_673,&l_838[1][5],&l_838[0][2]},{(void*)0,&l_838[2][1],&l_838[1][5]},{&l_838[1][5],&g_673,&l_838[1][5]}},{{&g_673,&g_673,&l_838[1][5]},{&g_673,&g_673,(void*)0},{&l_838[2][1],&l_838[2][1],&l_838[2][1]},{&l_838[1][2],&l_838[1][5],&l_838[1][5]}},{{&l_838[2][1],&l_838[1][5],&g_673},{&g_673,&l_838[1][5],&l_838[0][2]},{&g_673,&l_838[2][1],&g_673},{&l_838[1][5],&l_838[2][4],&l_838[1][5]}},{{(void*)0,&g_673,&l_838[2][1]},{&g_673,&l_838[2][4],(void*)0},{&l_838[1][5],&l_838[2][1],&l_838[1][5]},{&l_838[1][2],&l_838[1][5],&l_838[1][5]}}};
            int8_t *l_882[2];
            uint64_t *l_895 = &l_840;
            int32_t l_897 = (-1L);
            uint64_t *l_900 = (void*)0;
            uint64_t *l_901 = &g_902;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_882[i] = &l_338;
        }
    }
    return (*g_863);
}


/* ------------------------------------------ */
/* 
 * reads : g_290 g_291 g_311 g_285
 * writes: g_285 g_293
 */
static uint16_t  func_37(uint8_t * p_38)
{ /* block id: 142 */
    int32_t l_301[1][2][3] = {{{0x5E4D65C0L,0x5E4D65C0L,0x5E4D65C0L},{7L,7L,7L}}};
    uint32_t l_308[10][3][6] = {{{4294967286UL,0x9AE92379L,1UL,0x995E21B5L,4294967295UL,0UL},{4UL,4294967295UL,8UL,3UL,1UL,0x8F0CB5B0L},{6UL,0xCD85B9FEL,0xB100D8B4L,0x9AE92379L,0UL,1UL}},{{4294967295UL,0x38D84C4EL,0UL,0xDAD2676FL,1UL,0x995E21B5L},{4294967295UL,0x995E21B5L,0x4773189BL,0x4773189BL,0x995E21B5L,4294967295UL},{0UL,0xD745D6A0L,0x38D84C4EL,4UL,4294967295UL,0x5A19FAAEL}},{{0xB100D8B4L,1UL,0x4773189BL,6UL,1UL,0UL},{0xB100D8B4L,0xCD85B9FEL,6UL,0x82EBC4E1L,3UL,1UL},{0xD745D6A0L,4294967295UL,4294967295UL,0x995E21B5L,4UL,0UL}},{{0xDAD2676FL,1UL,4294967295UL,1UL,0UL,4294967295UL},{1UL,0x8A251592L,0xD745D6A0L,1UL,1UL,3UL},{0UL,0x9AE92379L,1UL,0x9AE92379L,0UL,4294967295UL}},{{0x82EBC4E1L,0xC8555D86L,4294967295UL,1UL,1UL,0x3BC88870L},{0x4773189BL,3UL,0x8F0CB5B0L,0xC8555D86L,0x5A19FAAEL,0x3BC88870L},{4294967295UL,4294967295UL,4294967295UL,0UL,0xCD85B9FEL,4294967295UL}},{{0x5A19FAAEL,4UL,1UL,4294967295UL,4294967295UL,3UL},{4294967295UL,0x82EBC4E1L,0xD745D6A0L,0xDAD2676FL,0x4773189BL,4294967295UL},{4294967295UL,8UL,4294967295UL,0xA740972DL,0UL,0UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,0x8A251592L,1UL},{0x995E21B5L,4294967295UL,6UL,0x8F0CB5B0L,0x82EBC4E1L,0UL},{4294967295UL,0xB7FAA7E2L,0x4773189BL,0UL,0x82EBC4E1L,0x5A19FAAEL}},{{1UL,4294967295UL,0x3BC88870L,4294967295UL,0x8A251592L,0xDAD2676FL},{4294967295UL,4294967295UL,0x995E21B5L,4UL,0UL,1UL},{0x76CD814CL,8UL,0UL,0x5A19FAAEL,0x4773189BL,0x47477040L}},{{0xCD85B9FEL,0x82EBC4E1L,0x8A251592L,0x38D84C4EL,4294967295UL,0x38D84C4EL},{0xB7FAA7E2L,4UL,0xB7FAA7E2L,4294967295UL,0xCD85B9FEL,4294967295UL},{0UL,4294967295UL,4UL,0xCD85B9FEL,0x5A19FAAEL,0x8F0CB5B0L}},{{4294967286UL,3UL,0x38D84C4EL,0xCD85B9FEL,1UL,4294967295UL},{0UL,0xC8555D86L,0x9AE92379L,4294967295UL,0UL,4294967286UL},{0xB7FAA7E2L,0x9AE92379L,0x47477040L,0x38D84C4EL,1UL,0x4773189BL}}};
    int i, j, k;
lbl_312:
    for (g_285 = 13; (g_285 == (-11)); g_285 = safe_sub_func_uint8_t_u_u(g_285, 1))
    { /* block id: 145 */
        int8_t l_302[9] = {0xF2L,0xF2L,0xF2L,0xF2L,0xF2L,0xF2L,0xF2L,0xF2L,0xF2L};
        int32_t *l_303 = &g_51[3][2][1];
        int32_t *l_304 = (void*)0;
        int32_t *l_305 = (void*)0;
        int32_t *l_306 = &g_165.f1;
        int32_t *l_307[7] = {(void*)0,&g_165.f1,(void*)0,(void*)0,&g_165.f1,(void*)0,(void*)0};
        int i;
        l_302[1] = (safe_div_func_float_f_f(l_301[0][1][0], (*g_290)));
        ++l_308[8][0][1];
    }
    (*g_311) = &l_301[0][1][0];
    if (g_285)
        goto lbl_312;
    return l_301[0][1][0];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_293
 */
static uint8_t * func_39(int64_t  p_40, uint8_t * p_41)
{ /* block id: 137 */
    int32_t * const l_292[7][9][3] = {{{&g_2,&g_144[2][4][0],&g_165.f1},{&g_2,&g_61.f1,&g_2},{&g_55,(void*)0,&g_51[1][0][8]},{&g_2,&g_51[1][0][3],&g_55},{(void*)0,&g_51[1][0][8],&g_2},{&g_144[6][4][0],&g_144[2][4][0],&g_51[0][2][2]},{(void*)0,&g_2,&g_55},{&g_165.f1,&g_165.f1,(void*)0},{(void*)0,&g_2,&g_165.f1}},{{&g_51[0][2][2],&g_2,&g_51[1][0][8]},{&g_51[1][0][8],&g_55,&g_2},{&g_55,&g_165.f1,&g_51[1][0][8]},{&g_2,&g_51[1][0][8],&g_144[2][4][0]},{&g_165.f1,(void*)0,&g_2},{(void*)0,(void*)0,&g_2},{&g_51[2][2][6],&g_2,&g_165.f1},{&g_51[1][0][8],&g_2,&g_144[2][4][0]},{&g_165.f1,&g_144[2][4][0],&g_61.f1}},{{&g_144[5][2][0],&g_55,&g_144[2][4][0]},{&g_165.f1,&g_51[1][0][8],&g_144[6][4][0]},{&g_55,&g_61.f1,&g_144[2][4][0]},{&g_51[1][0][8],&g_61.f1,&g_55},{&g_61.f1,&g_51[1][0][8],&g_2},{(void*)0,&g_55,(void*)0},{(void*)0,&g_144[2][4][0],(void*)0},{&g_144[2][4][0],&g_2,&g_165.f1},{(void*)0,&g_2,(void*)0}},{{&g_55,(void*)0,&g_55},{(void*)0,(void*)0,(void*)0},{&g_2,&g_51[1][0][8],(void*)0},{(void*)0,&g_165.f1,&g_165.f1},{&g_144[3][0][0],&g_55,&g_51[1][0][8]},{(void*)0,&g_2,&g_61.f1},{&g_2,(void*)0,&g_51[2][2][6]},{(void*)0,(void*)0,(void*)0},{&g_55,&g_61.f1,&g_144[5][2][0]}},{{(void*)0,&g_61.f1,&g_51[1][1][6]},{&g_144[2][4][0],&g_2,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_2,&g_165.f1},{&g_61.f1,&g_51[1][0][8],&g_144[4][1][0]},{&g_51[1][0][8],&g_51[1][0][8],&g_144[4][1][0]},{&g_55,&g_144[6][4][0],&g_165.f1},{&g_165.f1,&g_51[1][0][8],(void*)0},{&g_144[5][2][0],&g_55,(void*)0}},{{&g_165.f1,(void*)0,&g_51[1][1][6]},{&g_51[1][0][8],&g_144[2][4][0],&g_144[5][2][0]},{&g_51[2][2][6],(void*)0,(void*)0},{(void*)0,&g_144[6][4][0],&g_51[2][2][6]},{&g_165.f1,&g_144[5][2][0],&g_61.f1},{&g_2,(void*)0,&g_51[1][0][8]},{&g_55,&g_51[0][2][2],&g_165.f1},{&g_51[1][0][8],(void*)0,(void*)0},{&g_165.f1,&g_144[5][2][0],(void*)0}},{{&g_2,&g_144[6][4][0],&g_55},{(void*)0,(void*)0,(void*)0},{&g_51[1][0][3],&g_144[2][4][0],&g_165.f1},{&g_165.f1,(void*)0,(void*)0},{&g_2,&g_55,(void*)0},{&g_144[6][4][0],&g_51[1][0][8],&g_2},{&g_2,&g_144[6][4][0],&g_55},{&g_51[1][0][8],&g_51[1][0][8],&g_144[2][4][0]},{&g_51[1][0][8],&g_51[1][0][8],&g_144[6][4][0]}}};
    int32_t *l_294 = (void*)0;
    struct S0 l_295[8] = {{40444,0x08EBED17L},{40444,0x08EBED17L},{40444,0x08EBED17L},{40444,0x08EBED17L},{40444,0x08EBED17L},{40444,0x08EBED17L},{40444,0x08EBED17L},{40444,0x08EBED17L}};
    uint8_t *l_296 = &g_36;
    int i, j, k;
    g_293 = l_292[3][1][2];
    l_294 = l_292[3][1][2];
    l_295[1] = l_295[5];
    return l_296;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_255 g_54 g_165.f1 g_50
 */
static uint8_t * func_42(uint8_t * p_43, const int16_t  p_44)
{ /* block id: 134 */
    for (g_255 = 0; g_255 < 10; g_255 += 1)
    {
        for (g_54 = 0; g_54 < 9; g_54 += 1)
        {
            for (g_165.f1 = 0; g_165.f1 < 1; g_165.f1 += 1)
            {
                g_50[g_255][g_54][g_165.f1] = 5L;
            }
        }
    }
    return p_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_11 g_51 g_61.f0 g_2 g_54 g_55 g_36 g_76 g_77 g_50 g_85 g_61.f1 g_68 g_144 g_162 g_166 g_186 g_196 g_185 g_203 g_143 g_228 g_165.f1 g_86 g_61 g_204 g_49 g_165.f0 g_290
 * writes: g_51 g_55 g_61 g_68 g_86 g_54 g_76 g_143 g_144 g_162 g_165 g_185 g_196 g_228 g_255 g_50 g_285 g_289 g_291
 */
static uint8_t * func_45(const uint16_t  p_46, uint8_t * p_47)
{ /* block id: 9 */
    struct S0 l_60 = {-14636,1L};
    int32_t l_81 = (-7L);
    struct S0 ** volatile *l_89 = &g_84;
    int32_t l_97[9][8][3] = {{{0xE173883CL,1L,(-1L)},{0xA8554240L,1L,0xB8C42DAAL},{1L,0xBF897C6CL,0L},{3L,(-1L),0xA8554240L},{0x9814C775L,1L,0x3271FE85L},{(-1L),(-1L),7L},{0xE10C3079L,0xFC4F5E6FL,0x3271FE85L},{0x0D84A95BL,0xE10C3079L,0x63587EC0L}},{{(-3L),(-1L),0x29F58EF4L},{0x66946F2EL,0x2B1B5CBCL,0L},{0x942222B0L,0xA81BD54BL,0xC582B1A1L},{0x942222B0L,(-1L),(-3L)},{0x66946F2EL,0xE173883CL,0x2FE9626EL},{(-3L),0x92A33AFFL,0xC8606661L},{1L,0xB8C42DAAL,(-1L)},{(-4L),1L,1L}},{{(-5L),0xB8C42DAAL,0x35CAECAFL},{8L,0x92A33AFFL,0xBC801A57L},{0xBC801A57L,0xE173883CL,7L},{1L,(-1L),6L},{0x63587EC0L,0xA81BD54BL,6L},{7L,0x2B1B5CBCL,7L},{0xC0ED61D3L,(-1L),0xBC801A57L},{0x9FB5A3AAL,0xE10C3079L,0x35CAECAFL}},{{0x29F58EF4L,0L,1L},{(-5L),3L,(-1L)},{0x29F58EF4L,(-10L),0xC8606661L},{0x9FB5A3AAL,0x9814C775L,0x2FE9626EL},{0xC0ED61D3L,2L,(-3L)},{7L,0xA347B796L,0xC582B1A1L},{0x63587EC0L,0xA347B796L,0L},{1L,2L,0x29F58EF4L}},{{0xBC801A57L,0x9814C775L,0x63587EC0L},{8L,(-10L),0x942222B0L},{(-5L),3L,0xDF514A7BL},{(-4L),0L,0x942222B0L},{1L,0xE10C3079L,0x63587EC0L},{(-3L),(-1L),0x29F58EF4L},{0x66946F2EL,0x2B1B5CBCL,0L},{0x942222B0L,0xA81BD54BL,0xC582B1A1L}},{{0x942222B0L,(-1L),(-3L)},{0x66946F2EL,0xE173883CL,0x2FE9626EL},{(-3L),0x92A33AFFL,0xC8606661L},{1L,0xB8C42DAAL,(-1L)},{(-4L),1L,1L},{(-5L),0xB8C42DAAL,0x35CAECAFL},{8L,0x92A33AFFL,0xBC801A57L},{0xBC801A57L,0xE173883CL,7L}},{{1L,(-1L),6L},{0x63587EC0L,0xA81BD54BL,6L},{7L,0x2B1B5CBCL,7L},{0xC0ED61D3L,(-1L),0xBC801A57L},{0x9FB5A3AAL,0xE10C3079L,0x35CAECAFL},{0x29F58EF4L,0L,1L},{(-5L),3L,(-1L)},{0x29F58EF4L,(-10L),0xC8606661L}},{{0x9FB5A3AAL,0x9814C775L,0x2FE9626EL},{0xC0ED61D3L,2L,(-3L)},{7L,0xA347B796L,0xC582B1A1L},{0x63587EC0L,0xA347B796L,0L},{1L,2L,0x29F58EF4L},{0xBC801A57L,0x9814C775L,0x63587EC0L},{8L,(-10L),0x942222B0L},{(-5L),3L,0xDF514A7BL}},{{(-4L),0L,0x942222B0L},{1L,0xE10C3079L,0x63587EC0L},{(-3L),(-1L),0x29F58EF4L},{0x66946F2EL,0x2B1B5CBCL,0L},{0x942222B0L,0xA81BD54BL,0xC582B1A1L},{0x942222B0L,(-1L),(-3L)},{0x66946F2EL,0xE173883CL,0x9B24318CL},{0L,0x0D5DAD17L,1L}}};
    int8_t l_132 = 0L;
    int64_t l_146 = 1L;
    int32_t *l_174 = &g_144[2][5][0];
    int32_t l_180[2][5] = {{0L,2L,0L,2L,0L},{0x3968E837L,0x3968E837L,0x3968E837L,0x3968E837L,0x3968E837L}};
    int64_t l_193[9] = {0x66F19170E0B8F0D8LL,0x66F19170E0B8F0D8LL,0xD3679061F160161ALL,0x66F19170E0B8F0D8LL,0x66F19170E0B8F0D8LL,0xD3679061F160161ALL,0x66F19170E0B8F0D8LL,0x66F19170E0B8F0D8LL,0xD3679061F160161ALL};
    int8_t l_287 = 0L;
    int i, j, k;
    if (g_11[1][3])
    { /* block id: 10 */
        uint16_t l_66 = 1UL;
        struct S0 *l_83 = &g_61;
        int8_t l_93 = 0x87L;
        int32_t l_94 = 0L;
        int32_t l_96[10][9][2] = {{{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L}},{{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL}},{{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L}},{{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL}},{{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L}},{{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L}},{{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL}},{{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L}},{{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL},{0L,0L},{0L,0xA057597BL},{(-1L),0L},{0xA057597BL,0L},{(-1L),0xA057597BL}},{{0L,0L},{0L,0xA057597BL},{(-1L),0L},{(-1L),0L},{1L,(-1L)},{0xA057597BL,0xA057597BL},{0xA057597BL,(-1L)},{1L,0L},{(-1L),0L}}};
        uint8_t l_181[4];
        int64_t l_195 = 0L;
        const struct S0 **l_216[1][2];
        const struct S0 ***l_215 = &l_216[0][1];
        uint16_t l_241 = 0xEB59L;
        volatile int32_t *l_268 = &g_50[9][4][0];
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_181[i] = 0xADL;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_216[i][j] = (void*)0;
        }
        for (g_51[1][0][8] = 0; (g_51[1][0][8] >= (-17)); g_51[1][0][8] = safe_sub_func_int64_t_s_s(g_51[1][0][8], 8))
        { /* block id: 13 */
            struct S0 l_58 = {-44257,0x52D49A33L};
            int32_t l_92 = (-1L);
            int32_t l_95[1];
            uint16_t l_98 = 0UL;
            struct S0 **l_172 = &l_83;
            struct S0 ***l_171[1][9][4] = {{{(void*)0,&l_172,(void*)0,(void*)0},{&l_172,&l_172,&l_172,&l_172},{&l_172,(void*)0,&l_172,(void*)0},{&l_172,&l_172,(void*)0,&l_172},{(void*)0,&l_172,&l_172,(void*)0},{&l_172,(void*)0,&l_172,&l_172},{&l_172,&l_172,&l_172,(void*)0},{(void*)0,&l_172,(void*)0,(void*)0},{&l_172,&l_172,&l_172,&l_172}}};
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_95[i] = (-1L);
            for (g_55 = 0; (g_55 > 13); g_55 = safe_add_func_uint16_t_u_u(g_55, 2))
            { /* block id: 16 */
                struct S0 *l_59[6][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                uint32_t *l_67 = &g_68;
                uint8_t l_75 = 0x7BL;
                int32_t *l_79 = &l_60.f1;
                int i, j;
                g_61 = (l_60 = l_58);
                if ((safe_lshift_func_int16_t_s_s(((((safe_mul_func_uint16_t_u_u(g_61.f0, (6L ^ l_66))) < g_2) , (((((*l_67) = p_46) ^ ((((safe_lshift_func_int16_t_s_s(g_54, (safe_div_func_uint8_t_u_u((((((0xA5CD88FCBDC27B9FLL || (g_2 & (safe_mul_func_int8_t_s_s(g_55, p_46)))) && 3UL) , 1L) && l_75) , 0x7AL), g_36)))) == p_46) , 0x32L) , l_75)) > p_46) == l_66)) & g_76), 7)))
                { /* block id: 20 */
                    return g_77;
                }
                else
                { /* block id: 22 */
                    int32_t *l_78 = &l_58.f1;
                    (*l_78) &= g_2;
                    if (l_66)
                        continue;
                    l_79 = l_67;
                }
            }
            if ((~l_81))
            { /* block id: 28 */
                int32_t *l_82 = &g_55;
                (*l_82) = p_46;
                for (l_58.f1 = 0; (l_58.f1 >= 0); l_58.f1 -= 1)
                { /* block id: 32 */
                    (*l_82) = p_46;
                    for (l_81 = 0; (l_81 >= 0); l_81 -= 1)
                    { /* block id: 36 */
                        int i, j, k;
                        if (g_50[(l_81 + 5)][(l_81 + 5)][l_58.f1])
                            break;
                        return p_47;
                    }
                }
                (*g_85) = l_83;
            }
            else
            { /* block id: 42 */
                struct S0 ** volatile *l_88 = &g_85;
                struct S0 ** volatile **l_87[7] = {&l_88,&l_88,(void*)0,&l_88,&l_88,(void*)0,&l_88};
                int32_t *l_90 = (void*)0;
                int32_t *l_91[5] = {&g_61.f1,&g_61.f1,&g_61.f1,&g_61.f1,&g_61.f1};
                int i;
                l_89 = &g_85;
                if (l_58.f0)
                    break;
                --l_98;
            }
            for (l_66 = 0; (l_66 <= 1); l_66 += 1)
            { /* block id: 49 */
                uint8_t l_103 = 0x46L;
                int32_t l_125 = 0x9160EC46L;
                const struct S0 *l_135 = &l_60;
                const struct S0 **l_134 = &l_135;
                const struct S0 ***l_133 = &l_134;
                int32_t *l_175 = &g_144[3][1][0];
                int32_t *l_176 = &l_94;
                int32_t *l_177 = &l_58.f1;
                int32_t *l_178 = (void*)0;
                int32_t *l_179[4] = {&l_97[1][0][0],&l_97[1][0][0],&l_97[1][0][0],&l_97[1][0][0]};
                int i;
                for (g_61.f1 = 2; (g_61.f1 >= 0); g_61.f1 -= 1)
                { /* block id: 52 */
                    int32_t *l_101 = &l_95[0];
                    int32_t *l_102[5];
                    uint32_t *l_123 = (void*)0;
                    uint32_t *l_124[9] = {&g_68,&g_68,&g_68,&g_68,&g_68,&g_68,&g_68,&g_68,&g_68};
                    uint8_t *l_130 = &g_76;
                    uint8_t *l_131[4][4] = {{&l_103,&l_103,&l_103,&l_103},{&l_103,&l_103,&l_103,&l_103},{&l_103,&l_103,&l_103,&l_103},{&l_103,&l_103,&l_103,&l_103}};
                    uint64_t *l_142 = &g_143;
                    int8_t *l_145[9] = {&l_93,&l_93,&l_93,&l_93,&l_93,&l_93,&l_93,&l_93,&l_93};
                    uint32_t l_167[3][5] = {{1UL,0x5EB35AFFL,0xDAB9027DL,0xDAB9027DL,0x5EB35AFFL},{0x838B9C54L,3UL,0xDAB9027DL,5UL,5UL},{3UL,0x838B9C54L,3UL,0xDAB9027DL,5UL}};
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_102[i] = &l_58.f1;
                    l_103++;
                    g_54 = (safe_add_func_int8_t_s_s(((safe_mod_func_int16_t_s_s(1L, l_96[(l_66 + 6)][(l_66 + 3)][l_66])) == (((safe_sub_func_int8_t_s_s(l_96[(g_61.f1 + 6)][(l_66 + 4)][l_66], ((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u(((l_94 = p_46) > ((0x4A38BF42L < (((safe_mod_func_int64_t_s_s(l_95[0], (~p_46))) | (&g_36 == &l_103)) >= (-7L))) >= p_46)), (*g_77))), p_46)) != 0x5960B8A5674ED491LL))) <= (-8L)) && l_60.f1)), p_46));
                    if (((++g_68) <= (g_50[5][3][0] , ((safe_sub_func_uint8_t_u_u((*g_77), (l_132 = ((*l_130) = (*g_77))))) <= (((l_133 == (void*)0) >= (*g_77)) >= (l_146 = (safe_unary_minus_func_uint64_t_u(((l_94 = ((18446744073709551615UL ^ (safe_unary_minus_func_uint8_t_u(l_97[1][7][0]))) || (((g_144[2][4][0] = (safe_lshift_func_uint16_t_u_s((((*l_142) = (safe_sub_func_uint16_t_u_u(p_46, 0x4D08L))) ^ 0xA5BA704CDBB5A92DLL), 4))) | 0x644126BBL) , l_60.f0))) && 1L)))))))))
                    { /* block id: 63 */
                        uint32_t **l_157 = &l_123;
                        const int32_t l_160 = 0L;
                        int16_t *l_161 = &g_162;
                        struct S0 *l_163 = (void*)0;
                        uint16_t l_170[1][7];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 7; j++)
                                l_170[i][j] = 0xF460L;
                        }
                        (*l_101) = (safe_sub_func_int64_t_s_s((((safe_div_func_uint16_t_u_u(((((((safe_rshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s(p_46, ((*l_161) |= (g_144[3][0][0] <= (1UL < (((safe_lshift_func_int16_t_s_u(0x396CL, 10)) != (1UL || l_103)) , ((&g_68 != ((*l_157) = l_102[0])) < (safe_lshift_func_int8_t_s_u(l_60.f1, l_160))))))))), l_58.f0)) >= p_46) ^ g_144[2][4][0]) , 65528UL) <= 1L) <= 0xC6L), l_66)) > g_54) , g_76), g_55));
                        (*g_166) = (*l_135);
                        l_167[1][0]--;
                        (*l_101) |= (l_170[0][2] == (l_171[0][5][3] == &g_84));
                    }
                    else
                    { /* block id: 70 */
                        int32_t **l_173[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_173[i] = (void*)0;
                        l_102[1] = (l_174 = &l_125);
                    }
                }
                ++l_181[1];
            }
        }
        for (l_81 = 2; (l_81 >= 0); l_81 -= 1)
        { /* block id: 80 */
            int32_t *l_187 = &l_97[2][7][0];
            int32_t *l_188 = (void*)0;
            int32_t *l_189[9];
            float l_191 = 0x0.0p+1;
            int16_t l_192 = 9L;
            int32_t l_194 = 0xF7480214L;
            uint64_t *l_237[1][3][10] = {{{(void*)0,(void*)0,&g_143,&g_143,(void*)0,&g_143,&g_143,(void*)0,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143,(void*)0,(void*)0,(void*)0},{&g_143,(void*)0,&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143,&g_143,(void*)0}}};
            struct S0 l_244[8][6][5] = {{{{-32596,6L},{-4226,0x31F9C665L},{32825,0x85D93583L},{7822,3L},{35703,0L}},{{30383,-1L},{-34864,0x23A73AA3L},{-33196,7L},{-12361,4L},{18458,0L}},{{-30836,0x2EA4D41DL},{-32408,0xA8706C08L},{31790,0xC1906637L},{-12702,0x87BD513DL},{-7880,0x21605337L}},{{20342,0x9B1A158EL},{-31647,0xB32F9698L},{-34864,0x23A73AA3L},{20342,0x9B1A158EL},{-24935,7L}},{{14473,0xF55045BDL},{-32596,6L},{17420,1L},{35703,0L},{-8805,-1L}},{{13513,1L},{575,0xB0E09EC5L},{3359,-5L},{2226,0xD2665F52L},{-31647,0xB32F9698L}}},{{{29380,0xF7CC52B9L},{14473,0xF55045BDL},{-11928,0xF19964D2L},{-11928,0xF19964D2L},{14473,0xF55045BDL}},{{-19193,0xCA9C73ABL},{1807,-1L},{36049,-5L},{18458,0L},{-176,0xCE802EA0L}},{{29465,0x64ABD414L},{-32596,6L},{-30836,0x2EA4D41DL},{29380,0xF7CC52B9L},{-27930,0xC3CA4362L}},{{1807,-1L},{37724,0L},{-4297,0xEA81F63EL},{41548,0x28FC56EBL},{-33196,7L}},{{29465,0x64ABD414L},{-3462,0xDF23C8F3L},{-23468,0xF471A302L},{-39687,0x2D723578L},{-30645,0x4829650CL}},{{45834,-1L},{11670,0x1199373EL},{15497,-1L},{20342,0x9B1A158EL},{38271,0xC7521459L}}},{{{-22172,0x367D3F51L},{29380,0xF7CC52B9L},{12793,-1L},{18395,0L},{7822,3L}},{{29058,6L},{7347,1L},{39279,-9L},{38271,0xC7521459L},{-26303,-9L}},{{17420,1L},{-15941,-9L},{-40677,0L},{-32596,6L},{-20672,0xE005B330L}},{{37220,0xE032A6B0L},{-34864,0x23A73AA3L},{-4297,0xEA81F63EL},{-29546,0xED8611DEL},{-10897,0x598CF16EL}},{{-11982,2L},{-23468,0xF471A302L},{18395,0L},{-15941,-9L},{18395,0L}},{{-7012,0x34CA4F0BL},{-7012,0x34CA4F0BL},{-40862,-2L},{45255,-1L},{29058,6L}}},{{{18395,0L},{-27930,0xC3CA4362L},{42306,0xBA860023L},{12793,-1L},{7822,3L}},{{-24220,0x35CB6FE5L},{-19689,0x41C1ADFDL},{10755,0x0654CC57L},{37724,0L},{7347,1L}},{{4507,0L},{-27930,0xC3CA4362L},{8349,0x4598FF70L},{4507,0L},{32825,0x85D93583L}},{{-176,0xCE802EA0L},{-7012,0x34CA4F0BL},{3359,-5L},{-26303,-9L},{-33196,7L}},{{-25657,0x5918745FL},{-23468,0xF471A302L},{-18304,-5L},{-11982,2L},{31790,0xC1906637L}},{{-18463,7L},{-34864,0x23A73AA3L},{575,0xB0E09EC5L},{29058,6L},{-34864,0x23A73AA3L}}},{{{-42100,0L},{-15941,-9L},{-3462,0xDF23C8F3L},{-43961,1L},{17420,1L}},{{-39078,-1L},{7347,1L},{-29546,0xED8611DEL},{37724,0L},{3359,-5L}},{{-15941,-9L},{29380,0xF7CC52B9L},{14473,0xF55045BDL},{-11928,0xF19964D2L},{-11928,0xF19964D2L}},{{44366,0x2BD6711FL},{11670,0x1199373EL},{44366,0x2BD6711FL},{-4297,0xEA81F63EL},{-11340,-7L}},{{7822,3L},{-3462,0xDF23C8F3L},{-19291,1L},{-15941,-9L},{-32041,0L}},{{-18463,7L},{37724,0L},{13513,1L},{-7012,0x34CA4F0BL},{45834,-1L}}},{{{42306,0xBA860023L},{-32596,6L},{-19291,1L},{-32041,0L},{-25657,0x5918745FL}},{{-34864,0x23A73AA3L},{1807,-1L},{44366,0x2BD6711FL},{38271,0xC7521459L},{-29546,0xED8611DEL}},{{4507,0L},{17420,1L},{14473,0xF55045BDL},{-20672,0xE005B330L},{-8805,-1L}},{{39279,-9L},{-39078,-1L},{-29546,0xED8611DEL},{1807,-1L},{-7012,0x34CA4F0BL}},{{-20672,0xE005B330L},{18395,0L},{-3462,0xDF23C8F3L},{-39687,0x2D723578L},{42306,0xBA860023L}},{{-7012,0x34CA4F0BL},{-14984,0x19C23526L},{575,0xB0E09EC5L},{-33196,7L},{45834,-1L}}},{{{-19291,1L},{-4252,6L},{-18304,-5L},{-30756,-1L},{-32596,6L}},{{18458,0L},{3359,-5L},{3359,-5L},{18458,0L},{35921,4L}},{{17420,1L},{-20672,0xE005B330L},{8349,0x4598FF70L},{-25657,0x5918745FL},{-11928,0xF19964D2L}},{{-26303,-9L},{-39078,-1L},{10755,0x0654CC57L},{39279,-9L},{-14984,0x19C23526L}},{{-4252,6L},{10662,-10L},{42306,0xBA860023L},{-25657,0x5918745FL},{10662,-10L}},{{45834,-1L},{20342,0x9B1A158EL},{-40862,-2L},{18458,0L},{-34864,0x23A73AA3L}}},{{{-23468,0xF471A302L},{-32596,6L},{18395,0L},{-30756,-1L},{-27930,0xC3CA4362L}},{{20342,0x9B1A158EL},{-44575,0x42941F78L},{-4297,0xEA81F63EL},{-33196,7L},{41548,0x28FC56EBL}},{{29465,0x64ABD414L},{-26870,0xFF4E7C8DL},{-40677,0L},{-39687,0x2D723578L},{32825,0x85D93583L}},{{38776,0xB2255C3AL},{-44575,0x42941F78L},{-27242,0xEADA3C97L},{-19193,0xCA9C73ABL},{-29546,0xED8611DEL}},{{-32041,0L},{-7880,0x21605337L},{21206,0L},{-19291,1L},{14473,0xF55045BDL}},{{44366,0x2BD6711FL},{-29546,0xED8611DEL},{20342,0x9B1A158EL},{-29546,0xED8611DEL},{44366,0x2BD6711FL}}}};
            uint64_t l_274 = 0x310098D11F52AC1ALL;
            int i, j, k;
            for (i = 0; i < 9; i++)
                l_189[i] = (void*)0;
            (*g_186) = &g_50[5][3][0];
            ++g_196;
            for (l_132 = 2; (l_132 >= 0); l_132 -= 1)
            { /* block id: 85 */
                struct S0 ***l_202 = (void*)0;
                int32_t l_262 = 0x7335CC24L;
                int32_t l_283 = (-6L);
                float *l_284 = (void*)0;
                float *l_286 = &l_191;
                float *l_288 = &g_289;
                for (g_165.f1 = 0; (g_165.f1 <= 2); g_165.f1 += 1)
                { /* block id: 88 */
                    int32_t **l_201 = &l_188;
                    struct S0 ****l_234[1];
                    int32_t l_240 = 0x13B5C5A4L;
                    uint16_t *l_254[8][7] = {{&l_241,&l_66,&l_241,&l_66,&l_66,&l_66,&l_241},{&l_66,&l_66,&l_241,&l_241,&l_241,&l_241,&l_241},{&l_241,&l_241,&l_66,&l_241,&l_66,&l_241,&l_66},{&l_66,&l_66,&l_66,&l_66,&l_66,&l_66,&l_66},{&l_241,&l_66,&l_66,&l_66,&l_66,&l_66,&l_241},{&l_66,&l_241,&l_241,&l_66,&l_241,&l_66,&l_66},{&l_241,&l_241,&l_66,&l_66,&l_66,&l_66,&l_66},{&l_66,&l_241,&l_241,&l_241,&l_66,&l_66,&l_66}};
                    uint8_t *l_260 = &g_76;
                    int8_t *l_261 = &l_93;
                    uint8_t *l_263 = (void*)0;
                    uint8_t *l_264 = &l_181[1];
                    int16_t *l_265 = &g_162;
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_234[i] = &l_202;
                    if ((safe_sub_func_uint16_t_u_u((((*l_201) = (void*)0) != (*g_186)), (p_46 == ((l_202 != g_203) | (((safe_lshift_func_uint16_t_u_u((p_46 && ((safe_div_func_uint16_t_u_u((((p_46 ^ ((g_68 == (((((safe_rshift_func_int16_t_s_u((safe_add_func_uint16_t_u_u((((*l_187) &= (safe_lshift_func_uint8_t_u_u(((p_46 > 0L) && 3L), 7))) || (-6L)), g_51[1][0][8])), 6)) , g_55) , (void*)0) != l_215) <= p_46)) >= p_46)) <= l_94) && (*l_187)), 0x3DEEL)) && p_46)), p_46)) >= 65529UL) != 249UL))))))
                    { /* block id: 91 */
                        uint64_t *l_217 = &g_143;
                        uint8_t *l_220[9] = {&g_76,&l_181[0],&g_76,&l_181[0],&g_76,&l_181[0],&g_76,&l_181[0],&g_76};
                        struct S0 l_227 = {15247,8L};
                        int64_t *l_233 = &l_146;
                        int i, j, k;
                        g_51[1][1][8] &= (((((l_96[1][0][0] < (g_76 = (p_46 > (++(*l_217))))) ^ ((safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(((l_227 , (g_228[4] = g_196)) , (safe_div_func_uint16_t_u_u((((*l_233) = (((safe_mul_func_int8_t_s_s((((*g_77) , 0x55L) && g_144[2][4][0]), ((g_228[4] , &g_228[4]) == (void*)0))) < p_46) | p_46)) > g_165.f1), g_165.f1))), 0x44L)), (*g_77))), 65535UL)) & l_181[2])) | 1L) || 0x8074L) >= (-6L));
                        (*l_187) ^= p_46;
                        (*l_187) = (((p_46 < ((l_234[0] == (void*)0) , p_46)) && (safe_mul_func_int8_t_s_s(((*g_86) , ((-1L) >= ((void*)0 != l_237[0][0][8]))), ((safe_mod_func_uint32_t_u_u(p_46, 0xDB94CB15L)) == l_227.f1)))) , l_240);
                        (*l_201) = (*l_201);
                    }
                    else
                    { /* block id: 100 */
                        (*l_201) = &l_94;
                        l_241++;
                        (***g_203) = l_244[5][2][1];
                    }
                    if (p_46)
                        break;
                    if ((safe_mod_func_uint64_t_u_u(18446744073709551608UL, ((safe_rshift_func_int16_t_s_u(((*l_265) = (l_96[7][0][0] , ((safe_rshift_func_uint8_t_u_u((+(g_143 = ((safe_add_func_uint16_t_u_u((0x997CL > (g_255 = ((1L && p_46) ^ g_68))), 0x88DFL)) ^ p_46))), ((l_94 |= ((((((safe_mul_func_uint8_t_u_u(((*l_264) = ((safe_lshift_func_uint8_t_u_u((((*l_261) = (((((((*l_260) |= (p_46 , 0x2CL)) , g_49) , p_46) == 65534UL) & g_165.f0) <= p_46)) < p_46), l_262)) || g_165.f0)), (*g_77))) <= p_46) != (*l_187)) , p_46) >= 1UL) >= p_46)) | p_46))) & (*l_187)))), p_46)) | p_46))))
                    { /* block id: 113 */
                        struct S0 l_266 = {21274,6L};
                        l_266 = (**g_204);
                    }
                    else
                    { /* block id: 115 */
                        volatile int32_t **l_267[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_267[i] = (void*)0;
                        l_268 = (*g_186);
                        return &g_76;
                    }
                }
                (*g_185) = (*g_185);
                if ((*l_187))
                    break;
                (*g_290) = ((p_46 <= (((*l_288) = ((0x0.3p+1 < ((-0x1.Cp-1) < (!((*l_286) = (g_165.f1 , (safe_sub_func_float_f_f((safe_sub_func_float_f_f(l_274, ((g_285 = (safe_add_func_float_f_f((((safe_rshift_func_uint16_t_u_s(g_144[0][6][0], 2)) & ((*l_187) = 7L)) , (l_262 = ((p_46 <= (safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u((*g_77), g_228[1])), 4))) , p_46))), l_283))) == 0x0.5p-1))), l_283))))))) < l_287)) != g_144[2][4][0])) <= p_46);
            }
        }
        return &g_76;
    }
    else
    { /* block id: 131 */
        return p_47;
    }
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_11[i][j], "g_11[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_25, "g_25", print_hash_value);
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_49, "g_49", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_50[i][j][k], "g_50[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_51[i][j][k], "g_51[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_54, "g_54", print_hash_value);
    transparent_crc(g_55, "g_55", print_hash_value);
    transparent_crc(g_61.f0, "g_61.f0", print_hash_value);
    transparent_crc(g_61.f1, "g_61.f1", print_hash_value);
    transparent_crc(g_68, "g_68", print_hash_value);
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_144[i][j][k], "g_144[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc(g_165.f0, "g_165.f0", print_hash_value);
    transparent_crc(g_165.f1, "g_165.f1", print_hash_value);
    transparent_crc(g_190, "g_190", print_hash_value);
    transparent_crc(g_196, "g_196", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_228[i], "g_228[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_255, "g_255", print_hash_value);
    transparent_crc(g_285, "g_285", print_hash_value);
    transparent_crc_bytes (&g_289, sizeof(g_289), "g_289", print_hash_value);
    transparent_crc_bytes (&g_291, sizeof(g_291), "g_291", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_335[i][j][k], "g_335[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_573, "g_573", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_801[i], "g_801[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_809, "g_809", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_847[i][j][k], "g_847[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_883, "g_883", print_hash_value);
    transparent_crc(g_899, "g_899", print_hash_value);
    transparent_crc(g_902, "g_902", print_hash_value);
    transparent_crc(g_994, "g_994", print_hash_value);
    transparent_crc(g_995, "g_995", print_hash_value);
    transparent_crc(g_1000, "g_1000", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1027[i][j][k], "g_1027[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1076, "g_1076", print_hash_value);
    transparent_crc(g_1139, "g_1139", print_hash_value);
    transparent_crc(g_1159, "g_1159", print_hash_value);
    transparent_crc(g_1202, "g_1202", print_hash_value);
    transparent_crc_bytes (&g_1439, sizeof(g_1439), "g_1439", print_hash_value);
    transparent_crc(g_1470, "g_1470", print_hash_value);
    transparent_crc(g_1475, "g_1475", print_hash_value);
    transparent_crc_bytes (&g_1494, sizeof(g_1494), "g_1494", print_hash_value);
    transparent_crc_bytes (&g_1508, sizeof(g_1508), "g_1508", print_hash_value);
    transparent_crc(g_1636, "g_1636", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1682[i][j], "g_1682[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1687, sizeof(g_1687), "g_1687", print_hash_value);
    transparent_crc(g_1731, "g_1731", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1743[i], "g_1743[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1965[i], "g_1965[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2008, "g_2008", print_hash_value);
    transparent_crc(g_2108, "g_2108", print_hash_value);
    transparent_crc(g_2184, "g_2184", print_hash_value);
    transparent_crc(g_2235, "g_2235", print_hash_value);
    transparent_crc(g_2344, "g_2344", print_hash_value);
    transparent_crc(g_2376, "g_2376", print_hash_value);
    transparent_crc(g_2441, "g_2441", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2442[i], "g_2442[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2492.f0, "g_2492.f0", print_hash_value);
    transparent_crc(g_2492.f1, "g_2492.f1", print_hash_value);
    transparent_crc(g_2599, "g_2599", print_hash_value);
    transparent_crc(g_2600, "g_2600", print_hash_value);
    transparent_crc_bytes (&g_2672, sizeof(g_2672), "g_2672", print_hash_value);
    transparent_crc(g_2758, "g_2758", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 653
   depth: 1, occurrence: 47
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 80
breakdown:
   indirect level: 0, occurrence: 47
   indirect level: 1, occurrence: 5
   indirect level: 2, occurrence: 0
   indirect level: 3, occurrence: 12
   indirect level: 4, occurrence: 7
   indirect level: 5, occurrence: 9
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 64
XXX times a bitfields struct on LHS: 19
XXX times a bitfields struct on RHS: 49
XXX times a single bitfield on LHS: 8
XXX times a single bitfield on RHS: 59

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 449
   depth: 2, occurrence: 110
   depth: 3, occurrence: 13
   depth: 4, occurrence: 1
   depth: 5, occurrence: 3
   depth: 6, occurrence: 4
   depth: 7, occurrence: 1
   depth: 8, occurrence: 3
   depth: 9, occurrence: 2
   depth: 11, occurrence: 2
   depth: 12, occurrence: 3
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 3
   depth: 18, occurrence: 5
   depth: 19, occurrence: 3
   depth: 20, occurrence: 5
   depth: 21, occurrence: 2
   depth: 22, occurrence: 6
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 5
   depth: 27, occurrence: 7
   depth: 28, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 31, occurrence: 3
   depth: 32, occurrence: 3
   depth: 33, occurrence: 2
   depth: 35, occurrence: 3
   depth: 36, occurrence: 3
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1
   depth: 43, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 639

XXX times a variable address is taken: 1558
XXX times a pointer is dereferenced on RHS: 281
breakdown:
   depth: 1, occurrence: 236
   depth: 2, occurrence: 39
   depth: 3, occurrence: 6
XXX times a pointer is dereferenced on LHS: 348
breakdown:
   depth: 1, occurrence: 322
   depth: 2, occurrence: 23
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 46
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 10460

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1256
   level: 2, occurrence: 257
   level: 3, occurrence: 116
   level: 4, occurrence: 38
   level: 5, occurrence: 9
XXX number of pointers point to pointers: 253
XXX number of pointers point to scalars: 370
XXX number of pointers point to structs: 16
XXX percent of pointers has null in alias set: 28.2
XXX average alias set size: 1.44

XXX times a non-volatile is read: 2002
XXX times a non-volatile is write: 1020
XXX times a volatile is read: 123
XXX    times read thru a pointer: 34
XXX times a volatile is write: 82
XXX    times written thru a pointer: 44
XXX times a volatile is available for access: 1.9e+03
XXX percentage of non-volatile access: 93.6

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 441
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 45
   depth: 2, occurrence: 57
   depth: 3, occurrence: 80
   depth: 4, occurrence: 95
   depth: 5, occurrence: 131

XXX percentage a fresh-made variable is used: 15.2
XXX percentage an existing variable is used: 84.8
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


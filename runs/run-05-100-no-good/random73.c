/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3649549409
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint32_t g_5 = 1UL;
static int32_t g_9 = 9L;
static float g_11[3] = {0xD.754057p+49,0xD.754057p+49,0xD.754057p+49};
static uint16_t g_12 = 65533UL;
static int32_t g_14 = 1L;
static volatile uint8_t g_27 = 250UL;/* VOLATILE GLOBAL g_27 */


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_27 g_9
 * writes: g_9 g_11 g_12 g_27
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int8_t l_4 = 0x42L;
    float *l_8 = (void*)0;
    float *l_10 = &g_11[0];
    int32_t *l_13 = &g_14;
    int32_t *l_15 = (void*)0;
    int32_t *l_16 = &g_14;
    int32_t l_17 = 1L;
    int32_t *l_18 = &g_14;
    int32_t *l_19 = &g_14;
    int32_t *l_20 = (void*)0;
    int32_t *l_21 = (void*)0;
    int32_t *l_22 = &g_14;
    int32_t *l_23 = &l_17;
    int32_t *l_24 = &g_14;
    int32_t *l_25 = &l_17;
    int32_t *l_26 = (void*)0;
    g_12 = (safe_div_func_float_f_f(l_4, (g_5 , (((g_5 >= 0x1.Ep+1) != ((g_5 , ((*l_10) = ((g_9 = (g_5 != ((safe_div_func_float_f_f(g_5, g_5)) >= g_5))) >= l_4))) <= g_5)) <= (-0x9.Ap-1)))));
    --g_27;
    return g_9;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_11[i], sizeof(g_11[i]), "g_11[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_27, "g_27", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 6
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 14
breakdown:
   depth: 1, occurrence: 4
   depth: 14, occurrence: 1

XXX total number of pointers: 14

XXX times a variable address is taken: 9
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 1
breakdown:
   depth: 1, occurrence: 1
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 21

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 14
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 35.7
XXX average alias set size: 1

XXX times a non-volatile is read: 11
XXX times a non-volatile is write: 4
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 1
XXX percentage of non-volatile access: 93.8

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 3
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 3

XXX percentage a fresh-made variable is used: 37.5
XXX percentage an existing variable is used: 62.5
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


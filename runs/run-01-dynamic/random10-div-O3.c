/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3698038577
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   signed f0 : 23;
   volatile unsigned f1 : 18;
   volatile unsigned f2 : 13;
   const signed f3 : 4;
   signed f4 : 5;
   unsigned f5 : 10;
};

#pragma pack(push)
#pragma pack(1)
struct S1 {
   volatile uint64_t  f0;
   uint32_t  f1;
   int32_t  f2;
   uint8_t  f3;
   uint32_t  f4;
   unsigned f5 : 4;
   int64_t  f6;
   int32_t  f7;
};
#pragma pack(pop)

struct S2 {
   volatile uint8_t  f0;
   const uint16_t  f1;
   uint32_t  f2;
   int8_t  f3;
   int32_t  f4;
   uint8_t  f5;
   const volatile uint32_t  f6;
   unsigned f7 : 19;
   unsigned f8 : 27;
};

#pragma pack(push)
#pragma pack(1)
struct S3 {
   volatile signed f0 : 9;
   unsigned f1 : 8;
   unsigned f2 : 8;
   volatile int8_t  f3;
   signed f4 : 30;
   signed f5 : 8;
   volatile unsigned f6 : 31;
   unsigned f7 : 31;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static struct S0 g_8 = {-2109,215,30,-2,-4,26};/* VOLATILE GLOBAL g_8 */
static struct S0 *g_7[1] = {&g_8};
static struct S0 ** volatile g_11 = (void*)0;/* VOLATILE GLOBAL g_11 */
static struct S0 ** volatile g_12 = &g_7[0];/* VOLATILE GLOBAL g_12 */
static int32_t g_15 = 0xAB462A77L;
static struct S0 g_27 = {-506,8,4,-2,0,31};/* VOLATILE GLOBAL g_27 */
static int32_t g_35 = 0x46BB218AL;
static uint16_t g_61 = 0xCC39L;
static uint32_t g_82 = 0xE73BF60CL;
static int64_t g_98 = 0xAC0D74891BC53C27LL;
static struct S3 g_102[9][3] = {{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}},{{15,5,11,0L,-27786,14,9270,19325},{15,5,11,0L,-27786,14,9270,19325},{20,11,8,0x65L,-14357,-0,40402,7218}}};
static volatile struct S3 g_111 = {2,13,15,1L,5937,-14,30449,21241};/* VOLATILE GLOBAL g_111 */
static uint8_t g_137[6] = {0xD1L,0xD1L,0xD1L,0xD1L,0xD1L,0xD1L};
static int8_t g_147 = 0L;
static volatile struct S1 g_155 = {0UL,4294967295UL,0xFB5FCD5AL,0x5CL,4294967295UL,2,0x66AEAC40E40D8D5CLL,0L};/* VOLATILE GLOBAL g_155 */
static struct S3 g_158 = {-13,13,2,0x62L,-18620,-14,24800,42053};/* VOLATILE GLOBAL g_158 */
static struct S3 *g_157 = &g_158;
static int32_t g_160 = 0xDB709881L;
static uint64_t g_163 = 0UL;
static int16_t g_168 = 0xE203L;
static int32_t g_194 = 0xDC83B1EFL;
static struct S1 g_218 = {0xFB04089B8EC4C58FLL,0UL,4L,1UL,0xA87665D8L,2,0xBC52F4FCE7EEA9CBLL,0x2FEDD841L};/* VOLATILE GLOBAL g_218 */
static struct S1 g_220[6][10] = {{{18446744073709551615UL,0x2D112435L,1L,251UL,0xE640AC8EL,1,2L,0x78BBDB83L},{8UL,4UL,-1L,1UL,0UL,3,0xA79F738797E1D416LL,-1L},{0x5DCCD6D889F70B1BLL,0xB0722B24L,0x686F9EF0L,255UL,2UL,3,-9L,0xFC65C19EL},{0xC85A50DE016D7211LL,4294967295UL,1L,0xBAL,1UL,3,0x2AB164A008EE9788LL,0x754541C6L},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L},{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{0xC85A50DE016D7211LL,4294967295UL,1L,0xBAL,1UL,3,0x2AB164A008EE9788LL,0x754541C6L},{0x5DCCD6D889F70B1BLL,0xB0722B24L,0x686F9EF0L,255UL,2UL,3,-9L,0xFC65C19EL}},{{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L},{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L},{18446744073709551615UL,0x2D112435L,1L,251UL,0xE640AC8EL,1,2L,0x78BBDB83L},{0xD0803106AD7F4E27LL,4294967289UL,-2L,0x61L,4294967288UL,3,0x6CF05B7820DD73F1LL,0L},{0x2F2EF202E73300B0LL,4294967286UL,-3L,0xBFL,0UL,1,-1L,-1L},{2UL,0UL,-3L,0x62L,0UL,1,-4L,-8L},{0xC85A50DE016D7211LL,4294967295UL,1L,0xBAL,1UL,3,0x2AB164A008EE9788LL,0x754541C6L},{0xE1CF2D15554DE583LL,0x3069C672L,-1L,0x89L,4294967286UL,0,-1L,0xCC9AE339L},{0xC85A50DE016D7211LL,4294967295UL,1L,0xBAL,1UL,3,0x2AB164A008EE9788LL,0x754541C6L},{2UL,0UL,-3L,0x62L,0UL,1,-4L,-8L}},{{8UL,4UL,-1L,1UL,0UL,3,0xA79F738797E1D416LL,-1L},{18446744073709551615UL,0x2D112435L,1L,251UL,0xE640AC8EL,1,2L,0x78BBDB83L},{0xDA2AAD204E769F5BLL,7UL,0L,0xC7L,1UL,2,0x5F25224E78F1FB96LL,-10L},{18446744073709551615UL,0x2D112435L,1L,251UL,0xE640AC8EL,1,2L,0x78BBDB83L},{8UL,4UL,-1L,1UL,0UL,3,0xA79F738797E1D416LL,-1L},{0x5DCCD6D889F70B1BLL,0xB0722B24L,0x686F9EF0L,255UL,2UL,3,-9L,0xFC65C19EL},{0xC85A50DE016D7211LL,4294967295UL,1L,0xBAL,1UL,3,0x2AB164A008EE9788LL,0x754541C6L},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L},{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L}},{{0xD0803106AD7F4E27LL,4294967289UL,-2L,0x61L,4294967288UL,3,0x6CF05B7820DD73F1LL,0L},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{9UL,0x58C23108L,5L,0xF2L,0UL,3,0xB23A127701506DAALL,7L},{2UL,0UL,-3L,0x62L,0UL,1,-4L,-8L},{2UL,0UL,-3L,0x62L,0UL,1,-4L,-8L},{9UL,0x58C23108L,5L,0xF2L,0UL,3,0xB23A127701506DAALL,7L},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{0xD0803106AD7F4E27LL,4294967289UL,-2L,0x61L,4294967288UL,3,0x6CF05B7820DD73F1LL,0L},{0xDA2AAD204E769F5BLL,7UL,0L,0xC7L,1UL,2,0x5F25224E78F1FB96LL,-10L},{0xBF6C481FF659E5E0LL,0x0025C80AL,0xDA366398L,1UL,5UL,0,0x1F5A4721E7CADE12LL,-1L}},{{0xE1CF2D15554DE583LL,0x3069C672L,-1L,0x89L,4294967286UL,0,-1L,0xCC9AE339L},{2UL,0UL,-3L,0x62L,0UL,1,-4L,-8L},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{1UL,0x91DCC661L,0x9C2D32F8L,1UL,0UL,2,-9L,0xCEAAE59BL},{8UL,4UL,-1L,1UL,0UL,3,0xA79F738797E1D416LL,-1L},{0xC85A50DE016D7211LL,4294967295UL,1L,0xBAL,1UL,3,0x2AB164A008EE9788LL,0x754541C6L},{8UL,4UL,-1L,1UL,0UL,3,0xA79F738797E1D416LL,-1L},{1UL,0x91DCC661L,0x9C2D32F8L,1UL,0UL,2,-9L,0xCEAAE59BL},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{2UL,0UL,-3L,0x62L,0UL,1,-4L,-8L}},{{9UL,0x58C23108L,5L,0xF2L,0UL,3,0xB23A127701506DAALL,7L},{0x5DCCD6D889F70B1BLL,0xB0722B24L,0x686F9EF0L,255UL,2UL,3,-9L,0xFC65C19EL},{0x10621FF5390B466FLL,1UL,-4L,0x05L,0x2D9FCF23L,2,1L,0x0B2D570DL},{8UL,4UL,-1L,1UL,0UL,3,0xA79F738797E1D416LL,-1L},{0x2F2EF202E73300B0LL,4294967286UL,-3L,0xBFL,0UL,1,-1L,-1L},{1UL,0x91DCC661L,0x9C2D32F8L,1UL,0UL,2,-9L,0xCEAAE59BL},{0xD0803106AD7F4E27LL,4294967289UL,-2L,0x61L,4294967288UL,3,0x6CF05B7820DD73F1LL,0L},{0xD0803106AD7F4E27LL,4294967289UL,-2L,0x61L,4294967288UL,3,0x6CF05B7820DD73F1LL,0L},{1UL,0x91DCC661L,0x9C2D32F8L,1UL,0UL,2,-9L,0xCEAAE59BL},{0x2F2EF202E73300B0LL,4294967286UL,-3L,0xBFL,0UL,1,-1L,-1L}}};
static struct S1 * volatile g_221 = &g_220[3][9];/* VOLATILE GLOBAL g_221 */
static uint16_t g_241 = 0x0BAFL;
static volatile struct S3 * volatile g_254 = &g_111;/* VOLATILE GLOBAL g_254 */
static struct S1 * volatile g_255 = &g_220[4][3];/* VOLATILE GLOBAL g_255 */
static volatile int8_t g_273 = 0xBEL;/* VOLATILE GLOBAL g_273 */
static volatile int16_t g_276[8][9] = {{0xF53FL,0L,1L,0xEA80L,0xEA80L,1L,0L,0xF53FL,0L},{0x90D0L,0x23D6L,0x0120L,2L,0L,(-8L),0x79E2L,(-8L),0L},{(-10L),0L,0L,(-10L),0xF53FL,0xC82CL,0xF53FL,(-10L),0L},{0xECE7L,0L,0x79E2L,0x23D6L,0L,0x23D6L,0x79E2L,0L,0xECE7L},{0L,(-10L),0xF53FL,0xC82CL,0xF53FL,(-10L),0L,0L,(-10L)},{0L,(-8L),0x79E2L,(-8L),0L,2L,0x0120L,0x23D6L,0x90D0L},{0L,0xF53FL,0L,1L,0xEA80L,0xEA80L,1L,0L,0xF53FL},{0xECE7L,0x21D1L,0x0120L,0xFC24L,0x79E2L,2L,0xDCC2L,0L,0xDCC2L}};
static volatile int8_t g_277 = 0xD2L;/* VOLATILE GLOBAL g_277 */
static int16_t g_278 = (-4L);
static int32_t g_279 = 0xCCC253A7L;
static volatile int8_t g_280[3][9][7] = {{{(-1L),0x46L,(-1L),0x48L,0L,0x01L,0xDDL},{0xCDL,(-1L),(-1L),0xE6L,0x41L,9L,0x01L},{0xB4L,(-1L),0xE6L,0x56L,0x8DL,9L,(-6L)},{(-6L),0x64L,0xDDL,0xC6L,7L,0x01L,0x9AL},{0L,0x13L,0L,0x79L,0xB4L,1L,0xCFL},{0L,0xB4L,0xC6L,9L,9L,0xC6L,0xB4L},{0x6FL,0x97L,0x75L,0x64L,(-1L),0xBAL,0xE6L},{0xDDL,0x41L,0x64L,0xCEL,0x53L,0x90L,(-1L)},{0x81L,9L,1L,0x64L,5L,0x84L,0x65L}},{{1L,(-1L),0x46L,9L,0x97L,0x81L,1L},{0xB7L,0xB4L,0x6BL,0xB0L,0x65L,0x60L,0x9DL},{0xDDL,0xE6L,0x8DL,5L,0x97L,0x9EL,0xCDL},{0x9DL,0x90L,1L,0xBBL,9L,(-1L),0x75L},{0x97L,0x90L,5L,0xB3L,0x23L,0xB4L,0xB7L},{0xB0L,0xE6L,0x56L,0x97L,0xDDL,0x97L,0xCDL},{(-1L),0xB4L,0x9EL,0x06L,0x91L,0xE6L,0x91L},{0L,0x84L,0x84L,0L,0x79L,0x8DL,0x6FL},{0xBAL,5L,0x91L,9L,(-1L),0xBBL,0x9AL}},{{0xCFL,0x97L,0xBBL,0x56L,0x81L,0x90L,0x6FL},{0xB7L,0L,0x13L,(-6L),(-1L),0x9DL,0x91L},{0x6BL,0xE6L,0xDDL,0xCEL,0x84L,3L,0xCDL},{5L,1L,0xBAL,0x02L,0x08L,(-1L),0xB7L},{6L,(-5L),0x53L,0x13L,0xB4L,0xE6L,0x75L},{(-1L),0x9EL,0xCEL,0x41L,0xB4L,7L,0xCDL},{0L,0x6FL,0x97L,0x47L,0x08L,0x48L,0x9DL},{(-1L),0x75L,0x23L,0x79L,0x84L,0x9AL,1L},{0x02L,(-1L),(-1L),0x84L,(-1L),(-1L),0x02L}}};
static uint16_t g_281 = 0x81AEL;
static int32_t * volatile g_291[2][7] = {{&g_220[3][9].f7,&g_220[3][9].f7,&g_220[3][9].f7,&g_220[3][9].f7,&g_220[3][9].f7,&g_220[3][9].f7,&g_220[3][9].f7},{&g_220[3][9].f2,&g_220[3][9].f2,&g_220[3][9].f2,&g_220[3][9].f2,&g_220[3][9].f2,&g_220[3][9].f2,&g_220[3][9].f2}};
static struct S1 * volatile g_303 = &g_220[4][8];/* VOLATILE GLOBAL g_303 */
static int32_t *g_309 = &g_218.f7;
static int32_t ** volatile g_308 = &g_309;/* VOLATILE GLOBAL g_308 */
static int32_t *g_318 = &g_35;
static volatile struct S3 g_325 = {-10,1,12,0x7CL,26673,14,35784,22866};/* VOLATILE GLOBAL g_325 */
static uint64_t *g_340 = &g_163;
static uint64_t **g_339 = &g_340;
static uint64_t *** volatile g_338[10][1] = {{&g_339},{&g_339},{&g_339},{&g_339},{&g_339},{&g_339},{&g_339},{&g_339},{&g_339},{&g_339}};
static uint64_t *** volatile g_341 = (void*)0;/* VOLATILE GLOBAL g_341 */
static struct S2 g_344 = {0xA9L,0x6157L,4294967287UL,-3L,0x470DD217L,0xA2L,0x294CD86EL,440,4264};/* VOLATILE GLOBAL g_344 */
static struct S2 g_347 = {0UL,65527UL,0UL,0xF4L,9L,0UL,0x200B15A4L,34,11061};/* VOLATILE GLOBAL g_347 */
static volatile struct S1 g_358 = {0xAFA6C8B988105997LL,0xA24920AFL,2L,251UL,1UL,0,0xCA69A0212BC34BEFLL,0L};/* VOLATILE GLOBAL g_358 */
static uint8_t g_369 = 0UL;
static struct S2 *g_377[4] = {&g_344,&g_344,&g_344,&g_344};
static struct S2 **g_376 = &g_377[2];
static volatile struct S1 g_381 = {0x61ABF1F3988503C6LL,0x532C45FCL,0x5E2840A6L,0x4CL,0x9E78C4C9L,0,-1L,0xD1AF450DL};/* VOLATILE GLOBAL g_381 */
static uint64_t g_400 = 0x1A638DAC64B3BD3BLL;
static volatile struct S3 g_406[8] = {{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376},{-1,5,1,-1L,-23742,-1,29384,12376}};
static volatile uint32_t g_424 = 0xE77A634DL;/* VOLATILE GLOBAL g_424 */
static struct S2 g_451 = {247UL,0xE759L,0x13B34C5EL,0x5CL,0x93DA23F7L,255UL,0x9A3AB49BL,526,7520};/* VOLATILE GLOBAL g_451 */
static const struct S2 g_520[8] = {{0x8BL,0x2F52L,0UL,0xE9L,0xEE769874L,246UL,0UL,581,7480},{0x8BL,0x2F52L,0UL,0xE9L,0xEE769874L,246UL,0UL,581,7480},{0xFAL,65531UL,0UL,0x10L,-4L,0xF0L,9UL,216,8470},{0x8BL,0x2F52L,0UL,0xE9L,0xEE769874L,246UL,0UL,581,7480},{0x8BL,0x2F52L,0UL,0xE9L,0xEE769874L,246UL,0UL,581,7480},{0xFAL,65531UL,0UL,0x10L,-4L,0xF0L,9UL,216,8470},{0x8BL,0x2F52L,0UL,0xE9L,0xEE769874L,246UL,0UL,581,7480},{0x8BL,0x2F52L,0UL,0xE9L,0xEE769874L,246UL,0UL,581,7480}};
static volatile int16_t g_522 = 6L;/* VOLATILE GLOBAL g_522 */
static struct S2 g_530 = {0xC6L,5UL,4294967295UL,0xC6L,0x8336A177L,0xDAL,1UL,479,11182};/* VOLATILE GLOBAL g_530 */
static uint16_t *g_554 = &g_241;
static struct S3 * const  volatile *g_592 = &g_157;
static struct S3 * const  volatile * volatile *g_591 = &g_592;
static volatile int32_t g_595[1] = {0x82497A15L};
static struct S1 g_601 = {18446744073709551615UL,0x7CA94EC4L,-1L,0xCCL,4294967295UL,3,0x7253B6347D624FF1LL,0xFD9C6C5EL};/* VOLATILE GLOBAL g_601 */
static volatile struct S3 g_627[5] = {{19,13,13,0xA2L,-15378,-6,11240,3326},{19,13,13,0xA2L,-15378,-6,11240,3326},{19,13,13,0xA2L,-15378,-6,11240,3326},{19,13,13,0xA2L,-15378,-6,11240,3326},{19,13,13,0xA2L,-15378,-6,11240,3326}};
static volatile struct S3 * volatile g_638 = (void*)0;/* VOLATILE GLOBAL g_638 */
static struct S1 g_642 = {1UL,1UL,1L,0UL,0UL,0,-6L,0x62FBE874L};/* VOLATILE GLOBAL g_642 */
static struct S1 *g_641 = &g_642;
static const volatile struct S1 g_666 = {0x9FF1F9E7BB8480AFLL,0xF15CA4E8L,-3L,0x25L,4294967295UL,3,-1L,3L};/* VOLATILE GLOBAL g_666 */
static struct S3 ** const g_684 = (void*)0;
static struct S3 ** const *g_683 = &g_684;
static struct S3 ** const **g_682 = &g_683;
static volatile int8_t g_706[4] = {0x8EL,0x8EL,0x8EL,0x8EL};
static struct S3 g_714 = {-15,12,13,0xD0L,32305,7,23251,14828};/* VOLATILE GLOBAL g_714 */
static volatile struct S2 g_719 = {248UL,0xA52FL,3UL,0L,6L,247UL,4294967295UL,473,836};/* VOLATILE GLOBAL g_719 */
static volatile struct S2 * volatile g_718[6] = {&g_719,&g_719,&g_719,&g_719,&g_719,&g_719};
static volatile struct S2 * volatile *g_717 = &g_718[2];
static volatile struct S2 * volatile **g_716[10][6] = {{&g_717,(void*)0,(void*)0,&g_717,&g_717,&g_717},{(void*)0,&g_717,&g_717,(void*)0,&g_717,(void*)0},{(void*)0,(void*)0,(void*)0,&g_717,(void*)0,&g_717},{(void*)0,&g_717,&g_717,&g_717,&g_717,(void*)0},{(void*)0,&g_717,&g_717,&g_717,&g_717,&g_717},{(void*)0,(void*)0,(void*)0,&g_717,&g_717,(void*)0},{(void*)0,(void*)0,(void*)0,&g_717,&g_717,(void*)0},{&g_717,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_717,(void*)0,(void*)0,&g_717,&g_717,&g_717},{&g_717,&g_717,&g_717,(void*)0,&g_717,(void*)0}};
static volatile struct S2 * volatile *** volatile g_715[10][8] = {{&g_716[6][1],&g_716[3][2],&g_716[6][1],&g_716[6][1],&g_716[0][2],(void*)0,&g_716[6][1],&g_716[7][4]},{&g_716[6][1],&g_716[8][1],&g_716[6][1],&g_716[2][3],&g_716[6][1],&g_716[9][1],&g_716[6][3],&g_716[0][2]},{&g_716[6][1],&g_716[6][1],&g_716[2][3],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[2][3]},{&g_716[0][2],&g_716[0][2],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][1]},{&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][0],&g_716[6][1],&g_716[6][3],&g_716[6][1],&g_716[6][1]},{&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[2][3],&g_716[6][0],&g_716[0][2],&g_716[2][3]},{&g_716[6][1],&g_716[6][1],&g_716[2][0],&g_716[6][1],&g_716[8][1],&g_716[7][3],&g_716[6][1],&g_716[0][2]},{&g_716[6][3],&g_716[7][4],&g_716[6][1],&g_716[2][3],&g_716[6][1],&g_716[2][3],&g_716[6][1],&g_716[7][4]},{&g_716[6][1],&g_716[9][1],&g_716[6][0],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[6][1],&g_716[1][5]},{&g_716[0][2],&g_716[6][1],&g_716[6][1],(void*)0,&g_716[6][1],(void*)0,&g_716[6][1],&g_716[6][1]}};
static volatile struct S2 * volatile *** volatile * const  volatile g_721 = &g_715[5][5];/* VOLATILE GLOBAL g_721 */
static struct S0 g_725 = {-1553,84,44,3,-0,21};/* VOLATILE GLOBAL g_725 */
static volatile uint32_t *g_747 = &g_381.f1;
static volatile uint32_t ** volatile g_746 = &g_747;/* VOLATILE GLOBAL g_746 */
static struct S0 g_755 = {-1285,289,34,1,0,16};/* VOLATILE GLOBAL g_755 */
static struct S3 **g_853 = &g_157;
static struct S3 ***g_852 = &g_853;
static struct S3 **** volatile g_851 = &g_852;/* VOLATILE GLOBAL g_851 */
static struct S0 g_858[2][4][9] = {{{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}},{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}},{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}},{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}}},{{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}},{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}},{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}},{{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6},{-1115,120,51,3,-1,6}}}};
static struct S1 * volatile g_867 = &g_642;/* VOLATILE GLOBAL g_867 */
static struct S2 g_902 = {255UL,1UL,0xB6EE9F21L,4L,-7L,0xE3L,4294967289UL,710,730};/* VOLATILE GLOBAL g_902 */
static struct S2 * const g_901 = &g_902;
static struct S2 * const *g_900 = &g_901;
static struct S2 ***g_1012 = &g_376;
static struct S2 ****g_1011 = &g_1012;
static struct S2 *****g_1010 = &g_1011;
static struct S1 g_1025[3][10] = {{{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{0x3A1D5B0D9C9FAB5DLL,0xA650A564L,0x9658922CL,0x28L,0x1813C6A2L,3,0L,1L},{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL},{0xED95060A57F28FA5LL,0x5D416A9EL,0xECAD1E9EL,0x6FL,1UL,3,0x6909DB8DC516F30DLL,0x4428E3AFL},{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL},{0x3A1D5B0D9C9FAB5DLL,0xA650A564L,0x9658922CL,0x28L,0x1813C6A2L,3,0L,1L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{0x3A1D5B0D9C9FAB5DLL,0xA650A564L,0x9658922CL,0x28L,0x1813C6A2L,3,0L,1L},{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL}},{{0x3A1D5B0D9C9FAB5DLL,0xA650A564L,0x9658922CL,0x28L,0x1813C6A2L,3,0L,1L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{0x3A1D5B0D9C9FAB5DLL,0xA650A564L,0x9658922CL,0x28L,0x1813C6A2L,3,0L,1L},{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL},{0xED95060A57F28FA5LL,0x5D416A9EL,0xECAD1E9EL,0x6FL,1UL,3,0x6909DB8DC516F30DLL,0x4428E3AFL},{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL},{0x3A1D5B0D9C9FAB5DLL,0xA650A564L,0x9658922CL,0x28L,0x1813C6A2L,3,0L,1L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L}},{{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{18446744073709551615UL,8UL,5L,0x30L,1UL,0,8L,0xE520F558L},{0x407A0853D56BBC2ALL,0x19A92BE5L,2L,1UL,0UL,3,0L,-1L},{0x407A0853D56BBC2ALL,0x19A92BE5L,2L,1UL,0UL,3,0L,-1L},{18446744073709551615UL,8UL,5L,0x30L,1UL,0,8L,0xE520F558L},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{18446744073709551606UL,0xF8BA3941L,0x8C946CC5L,0xD1L,0x2F964746L,3,4L,0xBAC16D1EL},{18446744073709551607UL,0x7B223EF7L,4L,0x33L,1UL,1,0xB8EB4529A9345381LL,0x88EDD0B3L},{18446744073709551615UL,8UL,5L,0x30L,1UL,0,8L,0xE520F558L}}};
static const int32_t *g_1046 = &g_194;
static const int32_t **g_1045 = &g_1046;
static const int32_t ***g_1044[9][1] = {{&g_1045},{&g_1045},{&g_1045},{&g_1045},{&g_1045},{&g_1045},{&g_1045},{&g_1045},{&g_1045}};
static struct S0 g_1070[4] = {{1141,248,47,0,4,12},{1141,248,47,0,4,12},{1141,248,47,0,4,12},{1141,248,47,0,4,12}};
static struct S0 g_1072 = {652,458,32,-3,-4,24};/* VOLATILE GLOBAL g_1072 */
static struct S2 * const **g_1089 = (void*)0;
static struct S2 * const ***g_1088 = &g_1089;
static volatile struct S1 * volatile g_1094 = &g_381;/* VOLATILE GLOBAL g_1094 */
static int32_t ** volatile g_1106 = &g_309;/* VOLATILE GLOBAL g_1106 */
static struct S0 g_1108 = {-2081,428,3,1,3,4};/* VOLATILE GLOBAL g_1108 */
static struct S1 *g_1114 = &g_220[3][9];
static struct S1 *g_1115[8] = {&g_220[4][3],&g_220[4][3],&g_220[4][3],&g_220[4][3],&g_220[4][3],&g_220[4][3],&g_220[4][3],&g_220[4][3]};
static struct S3 g_1132 = {-12,13,7,0x45L,24551,4,6718,19813};/* VOLATILE GLOBAL g_1132 */
static int32_t * volatile g_1143 = &g_220[3][9].f2;/* VOLATILE GLOBAL g_1143 */
static volatile int64_t g_1147 = 0xAB8CCC50124A0B7ELL;/* VOLATILE GLOBAL g_1147 */
static int32_t * const  volatile g_1148 = (void*)0;/* VOLATILE GLOBAL g_1148 */
static int32_t * volatile g_1149 = &g_218.f2;/* VOLATILE GLOBAL g_1149 */
static int64_t g_1189 = 0x6201ED21B9DABEADLL;
static struct S1 g_1333 = {18446744073709551615UL,0xAEA2CB8FL,0x9464FA1FL,0UL,1UL,2,0x6FFA545220D1340DLL,0xEE009A0CL};/* VOLATILE GLOBAL g_1333 */
static int32_t ** volatile g_1359 = &g_318;/* VOLATILE GLOBAL g_1359 */
static volatile int32_t * volatile ** volatile * volatile * volatile g_1371 = (void*)0;/* VOLATILE GLOBAL g_1371 */
static struct S0 g_1396 = {2032,190,32,-3,0,18};/* VOLATILE GLOBAL g_1396 */
static struct S1 g_1437 = {0UL,0xB4E29BFCL,5L,255UL,4294967295UL,2,0x1D421EB2B0CB8D5ELL,0L};/* VOLATILE GLOBAL g_1437 */
static volatile struct S1 g_1439 = {0xF9394BFD3308C979LL,0x8261F866L,0x1CD720A0L,0xCBL,1UL,1,1L,-7L};/* VOLATILE GLOBAL g_1439 */
static int32_t * volatile g_1440 = (void*)0;/* VOLATILE GLOBAL g_1440 */
static uint32_t g_1477 = 1UL;
static struct S1 g_1481 = {0x32AEC8A1F0BAEFE9LL,0x603B7893L,8L,0x89L,0x7A9CF117L,3,0xFF7E62C232E1EC4FLL,0x679F5310L};/* VOLATILE GLOBAL g_1481 */
static struct S1 g_1495 = {18446744073709551612UL,0xEAD1F438L,0xD3FEF650L,248UL,4294967295UL,3,2L,-8L};/* VOLATILE GLOBAL g_1495 */
static struct S1 g_1506 = {0xC258D8F4E58201D6LL,4294967295UL,0x9D87717AL,0x17L,9UL,1,0xEF3F4784026AF81ELL,3L};/* VOLATILE GLOBAL g_1506 */
static uint32_t *g_1508 = &g_1477;
static uint32_t ** volatile g_1507 = &g_1508;/* VOLATILE GLOBAL g_1507 */
static struct S1 g_1510 = {0x4F709DB708B58253LL,0x847BCA5BL,0xC68BA136L,0UL,0xF9B8BAE2L,2,-1L,0xC0259F1AL};/* VOLATILE GLOBAL g_1510 */


/* --- FORWARD DECLARATIONS --- */
static struct S1  func_1(void);
static struct S0 * func_2(struct S0 * p_3, struct S0 * p_4, int32_t  p_5, uint8_t  p_6);
static struct S0 ** func_18(struct S0 ** const  p_19, uint8_t  p_20, int32_t * p_21);
static struct S0 ** const  func_22(struct S0 * p_23, uint8_t  p_24, uint32_t  p_25);
static int32_t * func_39(int32_t * p_40, const int32_t  p_41, uint8_t  p_42, int8_t  p_43, struct S0 * p_44);
static int64_t  func_46(int64_t  p_47, uint8_t  p_48, int32_t * p_49, int64_t  p_50);
static uint32_t  func_54(uint64_t  p_55, int32_t * const  p_56, uint64_t  p_57);
static const struct S2  func_64(uint16_t * p_65, int32_t ** const  p_66, const struct S0 * p_67, uint32_t  p_68);
static uint16_t * func_69(int16_t  p_70);
static int32_t * func_85(struct S0 * p_86, uint32_t  p_87, const int16_t  p_88, uint32_t  p_89, struct S0 * p_90);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_12 g_15 g_27.f4 g_8.f4 g_35 g_1106 g_309 g_1046 g_194 g_1010 g_1011 g_218.f1 g_852 g_853 g_157 g_725.f4 g_241 g_1149 g_218.f2 g_308 g_218.f7 g_102.f7 g_520.f2 g_1025.f4 g_218.f6 g_318 g_220.f5 g_706 g_347.f2 g_340 g_163 g_1333 g_554 g_902.f1 g_530.f5 g_1359 g_642.f6 g_1094 g_381 g_1012 g_376 g_1045 g_746 g_747 g_1437 g_1440 g_1143 g_220.f2 g_137 g_339 g_158.f1 g_278 g_61 g_451.f5 g_642.f7 g_591 g_592 g_1477 g_851 g_1481 g_451.f3 g_155.f1 g_627.f1 g_218.f3 g_902.f2 g_530.f2 g_1495 g_347.f4 g_1506 g_1507 g_1510
 * writes: g_7 g_15 g_35 g_218.f1 g_641 g_1114 g_1115 g_218.f7 g_344.f4 g_344.f2 g_218.f6 g_347.f2 g_530.f3 g_642.f4 g_318 g_642.f6 g_601.f6 g_530.f0 g_642.f7 g_280 g_377 g_344.f5 g_168 g_278 g_601.f1 g_1439 g_241 g_98 g_1012 g_1477 g_451.f3 g_1010 g_218.f3 g_530.f2 g_347.f4 g_1507
 */
static struct S1  func_1(void)
{ /* block id: 0 */
    struct S0 *l_9 = &g_8;
    uint8_t l_10 = 250UL;
    int32_t *l_14 = &g_15;
    int32_t **l_13 = &l_14;
    int32_t l_1410 = 2L;
    int32_t l_1419[5][2] = {{0L,1L},{1L,0L},{1L,1L},{0L,1L},{1L,0L}};
    uint32_t l_1420 = 0xAF938901L;
    int i, j;
    (*g_12) = func_2(g_7[0], l_9, l_10, l_10);
    (*l_13) = (void*)0;
    for (g_15 = (-14); (g_15 >= (-29)); g_15 = safe_sub_func_int64_t_s_s(g_15, 9))
    { /* block id: 8 */
        struct S0 *l_26[5];
        int32_t l_28 = 0x7F20BAB1L;
        struct S0 * const l_1367 = (void*)0;
        struct S0 * const *l_1366 = &l_1367;
        struct S0 * const **l_1365 = &l_1366;
        uint16_t **l_1392 = &g_554;
        int32_t *l_1426 = &g_194;
        struct S2 **l_1435[9] = {&g_377[2],&g_377[2],&g_377[2],&g_377[2],&g_377[2],&g_377[2],&g_377[2],&g_377[2],&g_377[2]};
        int32_t l_1436 = (-9L);
        int32_t l_1471 = 0x9ADC99CEL;
        uint32_t l_1473 = 4294967295UL;
        struct S3 *l_1484 = &g_714;
        uint32_t ** volatile *l_1509 = &g_1507;
        int i;
        for (i = 0; i < 5; i++)
            l_26[i] = &g_27;
        if ((&l_9 == ((*l_1365) = func_18(func_22(func_2((*g_12), l_26[2], l_28, g_27.f4), g_8.f4, g_8.f4), g_241, &l_28))))
        { /* block id: 640 */
            int32_t ***l_1370 = &l_13;
            int32_t ****l_1369 = &l_1370;
            int32_t *****l_1368 = &l_1369;
            uint16_t **l_1391 = &g_554;
            uint64_t *l_1393 = &g_400;
            uint32_t *l_1394[6][7][6] = {{{&g_902.f2,&g_82,&g_347.f2,&g_1333.f4,&g_601.f4,&g_1333.f4},{&g_347.f2,(void*)0,&g_347.f2,&g_82,&g_902.f2,&g_1333.f4},{&g_601.f1,&g_218.f4,&g_347.f2,&g_220[3][9].f1,&g_347.f2,&g_218.f4},{&g_902.f2,(void*)0,(void*)0,&g_220[3][9].f1,&g_1025[0][8].f1,&g_82},{&g_601.f1,&g_82,&g_642.f1,&g_82,&g_601.f1,&g_218.f4},{&g_347.f2,&g_82,&g_902.f2,&g_1333.f4,&g_1025[0][8].f1,&g_1333.f4},{&g_902.f2,(void*)0,&g_902.f2,&g_82,&g_347.f2,&g_1333.f4}},{{&g_530.f2,&g_218.f4,&g_902.f2,&g_220[3][9].f1,&g_902.f2,&g_218.f4},{&g_347.f2,(void*)0,&g_642.f1,&g_220[3][9].f1,&g_601.f4,&g_82},{&g_530.f2,&g_82,(void*)0,&g_82,&g_530.f2,&g_218.f4},{&g_902.f2,&g_82,&g_347.f2,&g_1333.f4,&g_601.f4,&g_1333.f4},{&g_347.f2,(void*)0,&g_347.f2,&g_82,&g_902.f2,&g_1333.f4},{&g_601.f1,&g_218.f4,&g_347.f2,&g_220[3][9].f1,&g_347.f2,&g_218.f4},{&g_902.f2,(void*)0,(void*)0,&g_220[3][9].f1,&g_1025[0][8].f1,&g_82}},{{&g_601.f1,&g_82,&g_642.f1,&g_82,&g_601.f1,&g_218.f4},{&g_347.f2,&g_82,&g_902.f2,&g_1333.f4,&g_1025[0][8].f1,&g_1333.f4},{&g_902.f2,(void*)0,&g_902.f2,&g_82,&g_347.f2,&g_1333.f4},{&g_530.f2,&g_218.f4,&g_902.f2,&g_220[3][9].f1,&g_902.f2,&g_218.f4},{&g_347.f2,(void*)0,&g_642.f1,&g_220[3][9].f1,&g_601.f1,&g_1333.f4},{&g_902.f2,&g_1333.f4,&g_1025[0][8].f1,&g_1333.f4,&g_902.f2,&g_82},{&g_642.f1,&g_1333.f4,&g_902.f2,&g_220[3][9].f1,&g_601.f1,&g_220[3][9].f1}},{{(void*)0,&g_218.f4,(void*)0,&g_1333.f4,&g_347.f2,&g_220[3][9].f1},{&g_347.f2,&g_82,&g_902.f2,(void*)0,&g_902.f2,&g_82},{&g_347.f2,&g_218.f4,&g_1025[0][8].f1,(void*)0,&g_530.f2,&g_1333.f4},{&g_347.f2,&g_1333.f4,&g_601.f4,&g_1333.f4,&g_347.f2,&g_82},{(void*)0,&g_1333.f4,&g_347.f2,&g_220[3][9].f1,&g_530.f2,&g_220[3][9].f1},{&g_642.f1,&g_218.f4,&g_642.f1,&g_1333.f4,&g_902.f2,&g_220[3][9].f1},{&g_902.f2,&g_82,&g_347.f2,(void*)0,&g_347.f2,&g_82}},{{&g_902.f2,&g_218.f4,&g_601.f4,(void*)0,&g_601.f1,&g_1333.f4},{&g_902.f2,&g_1333.f4,&g_1025[0][8].f1,&g_1333.f4,&g_902.f2,&g_82},{&g_642.f1,&g_1333.f4,&g_902.f2,&g_220[3][9].f1,&g_601.f1,&g_220[3][9].f1},{(void*)0,&g_218.f4,(void*)0,&g_1333.f4,&g_347.f2,&g_220[3][9].f1},{&g_347.f2,&g_82,&g_902.f2,(void*)0,&g_902.f2,&g_82},{&g_347.f2,&g_218.f4,&g_1025[0][8].f1,(void*)0,&g_530.f2,&g_1333.f4},{&g_347.f2,&g_1333.f4,&g_601.f4,&g_1333.f4,&g_347.f2,&g_82}},{{(void*)0,&g_1333.f4,&g_347.f2,&g_220[3][9].f1,&g_530.f2,&g_220[3][9].f1},{&g_642.f1,&g_218.f4,&g_642.f1,&g_1333.f4,&g_902.f2,&g_220[3][9].f1},{&g_902.f2,&g_82,&g_347.f2,(void*)0,&g_347.f2,&g_82},{&g_902.f2,&g_218.f4,&g_601.f4,(void*)0,&g_601.f1,&g_1333.f4},{&g_902.f2,&g_1333.f4,&g_1025[0][8].f1,&g_1333.f4,&g_902.f2,&g_82},{&g_642.f1,&g_1333.f4,&g_902.f2,&g_220[3][9].f1,&g_601.f1,&g_220[3][9].f1},{(void*)0,&g_218.f4,(void*)0,&g_1333.f4,&g_347.f2,&g_220[3][9].f1}}};
            struct S0 *l_1395 = &g_1396;
            int32_t l_1406 = 0x473F99DAL;
            int32_t l_1418 = 0x7E3C3D1AL;
            int i, j, k;
            (**l_1370) = (*g_1106);
            for (g_642.f6 = (-23); (g_642.f6 <= 10); g_642.f6 = safe_add_func_uint16_t_u_u(g_642.f6, 6))
            { /* block id: 647 */
                int32_t *l_1399 = &g_347.f4;
                int32_t *l_1400 = &g_530.f4;
                int32_t *l_1401 = &g_218.f7;
                int32_t *l_1402 = &g_218.f2;
                int32_t *l_1403 = (void*)0;
                int32_t *l_1404 = (void*)0;
                int32_t *l_1405 = &g_451.f4;
                int32_t *l_1407 = &g_451.f4;
                int32_t *l_1408 = (void*)0;
                int32_t *l_1409 = &g_218.f7;
                int32_t *l_1411 = &g_902.f4;
                int32_t *l_1412 = &g_1025[0][8].f7;
                int32_t *l_1413 = &g_347.f4;
                int32_t *l_1414 = (void*)0;
                int32_t *l_1415 = &g_451.f4;
                int32_t *l_1416 = &g_1333.f7;
                int32_t *l_1417[9] = {&g_344.f4,(void*)0,&g_344.f4,(void*)0,&g_344.f4,(void*)0,&g_344.f4,(void*)0,&g_344.f4};
                int i;
                --l_1420;
                for (g_601.f6 = 0; g_601.f6 < 3; g_601.f6 += 1)
                {
                    for (g_530.f0 = 0; g_530.f0 < 9; g_530.f0 += 1)
                    {
                        for (g_642.f7 = 0; g_642.f7 < 7; g_642.f7 += 1)
                        {
                            g_280[g_601.f6][g_530.f0][g_642.f7] = 0L;
                        }
                    }
                }
                if ((*****l_1368))
                    continue;
            }
            return (*g_1094);
        }
        else
        { /* block id: 653 */
            uint64_t l_1433[8] = {0x7EE45436B020A0CBLL,0x7EE45436B020A0CBLL,0x9A2742BB865856C1LL,0x7EE45436B020A0CBLL,0x7EE45436B020A0CBLL,0x9A2742BB865856C1LL,0x7EE45436B020A0CBLL,0x7EE45436B020A0CBLL};
            struct S2 ***l_1463 = &l_1435[1];
            int32_t l_1470 = 0xA6234CDDL;
            int32_t l_1502 = 3L;
            uint32_t l_1503 = 2UL;
            int i;
            if (((void*)0 == (*g_12)))
            { /* block id: 654 */
                int64_t *l_1460 = &g_98;
                const int32_t l_1461 = (-8L);
                int32_t l_1469 = 0x39EE3B76L;
                int32_t l_1472[2][2] = {{5L,5L},{5L,5L}};
                int i, j;
                if ((**g_1359))
                { /* block id: 655 */
                    struct S2 *l_1423 = &g_451;
                    (***g_1011) = l_1423;
                }
                else
                { /* block id: 657 */
                    uint8_t *l_1427 = &g_344.f5;
                    struct S2 **l_1430[10] = {&g_377[3],&g_377[3],&g_377[3],&g_377[3],&g_377[3],&g_377[3],&g_377[3],&g_377[3],&g_377[3],&g_377[3]};
                    int16_t *l_1432 = (void*)0;
                    int16_t *l_1434[3];
                    uint64_t *l_1459 = &g_163;
                    int32_t l_1462[8][10][3] = {{{(-4L),1L,1L},{(-4L),(-1L),(-4L)},{1L,1L,(-4L)},{0L,0xC4FDD8D8L,0xE8C1F6FFL},{0xF1498AE2L,(-1L),0xEE25243AL},{0x2EF75226L,0x2EF75226L,0x608EF295L},{0xF1498AE2L,1L,8L},{(-1L),0x2EF75226L,0xE8C1F6FFL},{9L,0xAE7A8113L,0xAE7A8113L},{9L,(-1L),0xE8C1F6FFL}},{{0xEE25243AL,0xF1498AE2L,8L},{(-4L),0x608EF295L,0x2EF75226L},{1L,0x8F31050FL,1L},{0x2EF75226L,0x608EF295L,(-4L)},{8L,0xF1498AE2L,0xEE25243AL},{0xE8C1F6FFL,(-1L),9L},{0xAE7A8113L,0xAE7A8113L,9L},{0xE8C1F6FFL,0x2EF75226L,(-1L)},{8L,9L,(-4L)},{0x2EF75226L,0xEB8C93E9L,0xEB8C93E9L}},{{1L,8L,(-4L)},{(-4L),0x95F5715FL,(-1L)},{0xEE25243AL,1L,9L},{9L,(-1L),9L},{9L,1L,0xEE25243AL},{(-1L),0x95F5715FL,(-4L)},{(-4L),8L,1L},{0xEB8C93E9L,0xEB8C93E9L,0x2EF75226L},{(-4L),9L,8L},{(-1L),0x2EF75226L,0xE8C1F6FFL}},{{9L,0xAE7A8113L,0xAE7A8113L},{9L,(-1L),0xE8C1F6FFL},{0xEE25243AL,0xF1498AE2L,8L},{(-4L),0x608EF295L,0x2EF75226L},{1L,0x8F31050FL,1L},{0x2EF75226L,0x608EF295L,(-4L)},{8L,0xF1498AE2L,0xEE25243AL},{0xE8C1F6FFL,(-1L),9L},{0xAE7A8113L,0xAE7A8113L,9L},{0xE8C1F6FFL,0x2EF75226L,(-1L)}},{{8L,9L,(-4L)},{0x2EF75226L,0xEB8C93E9L,0xEB8C93E9L},{1L,8L,(-4L)},{(-4L),0x95F5715FL,(-1L)},{0xEE25243AL,1L,9L},{9L,(-1L),9L},{9L,1L,0xEE25243AL},{(-1L),0x95F5715FL,(-4L)},{(-4L),8L,1L},{0xEB8C93E9L,0xEB8C93E9L,0x2EF75226L}},{{(-4L),9L,8L},{(-1L),0x2EF75226L,0xE8C1F6FFL},{9L,0xAE7A8113L,0xAE7A8113L},{9L,(-1L),0xE8C1F6FFL},{0xEE25243AL,0xF1498AE2L,8L},{(-4L),0x608EF295L,0x2EF75226L},{1L,0x8F31050FL,1L},{0x2EF75226L,0x608EF295L,(-4L)},{8L,0xF1498AE2L,0xEE25243AL},{0xE8C1F6FFL,(-1L),9L}},{{0xAE7A8113L,0xAE7A8113L,9L},{0xE8C1F6FFL,0x2EF75226L,(-1L)},{8L,9L,(-4L)},{0x2EF75226L,0xEB8C93E9L,0xEB8C93E9L},{1L,8L,(-4L)},{(-4L),0x95F5715FL,(-1L)},{0xEE25243AL,1L,9L},{9L,(-1L),9L},{9L,1L,0xEE25243AL},{(-1L),0x95F5715FL,(-4L)}},{{0xEE25243AL,0x8F31050FL,1L},{0L,0L,0xEB8C93E9L},{0xEE25243AL,0xAE7A8113L,0x8F31050FL},{(-1L),0xEB8C93E9L,(-4L)},{0xAE7A8113L,(-1L),(-1L)},{0x608EF295L,(-1L),(-4L)},{1L,(-4L),0x8F31050FL},{9L,0x2EF75226L,0xEB8C93E9L},{1L,0x048348DDL,1L},{0xEB8C93E9L,0x2EF75226L,9L}}};
                    const int8_t l_1489 = 0x7DL;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_1434[i] = (void*)0;
                    if ((((*l_1427) = (l_28 = (safe_rshift_func_uint32_t_u_u((l_1426 == (*g_1045)), 16)))) > (safe_mul_func_int16_t_s_s(((void*)0 != l_1430[4]), (safe_unary_minus_func_uint64_t_u((((g_278 = (g_168 = (l_1433[2] ^= 0xB439L))) == (((0x4C39E3B2L == (g_601.f1 = (**g_746))) | 0x7D3998FDL) != ((l_1435[1] != l_1430[4]) != l_1436))) & 0xAE12AA4CL)))))))
                    { /* block id: 664 */
                        return g_1437;
                    }
                    else
                    { /* block id: 666 */
                        volatile struct S1 *l_1438[9][2] = {{(void*)0,&g_381},{(void*)0,(void*)0},{(void*)0,&g_381},{(void*)0,(void*)0},{(void*)0,&g_381},{(void*)0,(void*)0},{(void*)0,&g_381},{(void*)0,(void*)0},{(void*)0,&g_381}};
                        int32_t * volatile *l_1441 = &l_14;
                        int i, j;
                        g_1439 = (*g_1094);
                        (*l_1441) = g_1440;
                        (*g_1011) = ((safe_mod_func_int32_t_s_s((safe_lshift_func_int64_t_s_s(((*l_1460) = (safe_lshift_func_uint8_t_u_s(((g_278 &= (safe_add_func_int64_t_s_s(((((*g_1143) < (safe_div_func_int64_t_s_s(g_137[0], (((*g_554) ^= ((l_1462[1][0][0] = (safe_mod_func_uint64_t_u_u(((safe_mul_func_uint64_t_u_u((+(safe_add_func_uint16_t_u_u(1UL, (((((*g_340) , ((-1L) < ((*g_339) != l_1459))) < (&g_1189 != l_1460)) >= 0x2459L) == l_1461)))), 0UL)) >= l_1436), g_158.f1))) <= (**g_339))) | 0x4363L)))) >= l_1436) > 0x26L), 0x0B75F3D65ED985DDLL))) == 1L), g_61))), g_451.f5)), 1UL)) , l_1463);
                    }
                    for (g_642.f7 = 0; (g_642.f7 == 17); g_642.f7 = safe_add_func_uint8_t_u_u(g_642.f7, 1))
                    { /* block id: 677 */
                        int32_t *l_1466 = (void*)0;
                        int32_t *l_1467 = &g_601.f2;
                        int32_t *l_1468[5][5][7] = {{{&g_642.f7,&g_1437.f2,&g_642.f7,(void*)0,&g_347.f4,(void*)0,&g_1025[0][8].f2},{&g_218.f7,&g_601.f2,&g_1025[0][8].f7,&g_1437.f7,(void*)0,&l_1419[3][1],&g_1025[0][8].f7},{&g_347.f4,&g_35,&g_220[3][9].f2,&l_1410,&l_1462[0][8][1],(void*)0,&g_344.f4},{&g_1437.f7,&g_451.f4,&g_218.f7,&g_218.f7,&g_902.f4,(void*)0,&g_601.f2},{&g_601.f2,&l_28,(void*)0,(void*)0,&l_1419[3][1],&l_1436,&g_642.f7}},{{&g_1025[0][8].f7,&g_1437.f7,&g_1025[0][8].f7,&g_35,&l_1462[1][3][2],&g_220[3][9].f7,&l_1462[1][3][2]},{&g_220[3][9].f2,&g_220[3][9].f7,&g_220[3][9].f7,&g_220[3][9].f2,&g_344.f4,&g_220[3][9].f2,&g_1437.f2},{&g_218.f7,(void*)0,&l_1436,&g_451.f4,&g_35,&l_28,&l_1462[1][0][0]},{&g_642.f2,&g_530.f4,&g_1025[0][8].f7,&l_1436,&l_1419[1][0],&g_1333.f2,&g_1437.f2},{&l_28,&g_15,&l_1410,&l_1419[2][1],&g_218.f7,&g_218.f7,&l_1462[1][3][2]}},{{&g_1025[0][8].f7,&g_642.f7,&g_1333.f2,&l_28,(void*)0,&l_28,&g_642.f7},{(void*)0,(void*)0,&l_1419[2][1],&g_220[3][9].f7,&g_218.f7,&g_601.f2,&g_601.f2},{(void*)0,&g_35,&g_1333.f2,&g_35,(void*)0,&g_642.f2,&g_344.f4},{&g_220[3][9].f7,&g_35,&g_218.f7,(void*)0,&g_642.f2,&g_15,&g_1025[0][8].f7},{(void*)0,&g_601.f2,&g_344.f4,&l_1419[1][0],(void*)0,(void*)0,&g_1025[0][8].f2}},{{&g_220[3][9].f7,(void*)0,&l_1419[3][1],&l_28,&l_28,(void*)0,&g_1025[0][8].f7},{(void*)0,&g_347.f4,&l_1436,&l_1410,&g_1437.f2,&g_1437.f2,&g_220[3][9].f2},{(void*)0,&g_1437.f2,&g_35,&g_15,&g_1333.f7,&l_1462[1][0][0],&g_218.f7},{&g_1025[0][8].f7,(void*)0,&g_601.f2,&g_601.f2,(void*)0,&g_1025[0][8].f7,&g_601.f2},{&l_28,&l_1462[1][0][0],&g_601.f2,&l_1462[1][3][2],&g_218.f7,&l_1419[3][0],&g_451.f4}},{{&g_642.f2,(void*)0,(void*)0,&l_1410,&l_1462[0][8][1],&g_1025[0][8].f7,&g_220[3][9].f7},{(void*)0,(void*)0,&g_220[3][9].f7,&l_1419[3][0],&l_1462[1][3][2],&l_1410,&g_218.f7},{(void*)0,(void*)0,&g_642.f7,&l_1410,&g_530.f4,&l_1462[0][8][1],&g_220[3][9].f7},{&l_28,&g_15,&l_1419[3][1],&g_601.f2,(void*)0,(void*)0,(void*)0},{&g_530.f4,&g_642.f7,&l_28,&l_1436,&l_28,&g_642.f7,&g_530.f4}}};
                        uint32_t *l_1476 = &g_1477;
                        uint64_t * const l_1480 = (void*)0;
                        int i, j, k;
                        l_1473--;
                        (**g_1359) = ((l_1436 , ((**g_591) != (((*l_1476)--) , (***g_851)))) > (((*g_339) != l_1480) >= 0x16CB3710CE570AF3LL));
                        return g_1481;
                    }
                    for (g_451.f3 = (-24); (g_451.f3 == (-14)); ++g_451.f3)
                    { /* block id: 685 */
                        uint64_t l_1487 = 0UL;
                        int32_t *l_1488 = &l_1470;
                        int32_t *l_1492 = &l_1419[3][1];
                        l_1484 = (**g_852);
                        (*l_1492) ^= (l_1472[1][0] = (((g_1477 = (((l_1472[1][1] <= 0x0BL) ^ (safe_add_func_int32_t_s_s(((*g_318) = (g_155.f1 , l_1433[2])), ((*l_1488) = l_1487)))) && l_1471)) , l_1489) , (((safe_sub_func_uint8_t_u_u((g_218.f3 &= (((*l_1427) = ((g_1010 = &g_1011) == &g_1088)) == g_627[4].f1)), 2L)) < g_902.f2) || l_1462[2][9][0])));
                    }
                    for (g_530.f2 = 0; (g_530.f2 < 59); g_530.f2++)
                    { /* block id: 698 */
                        (*g_318) = 0x5ABDB03EL;
                    }
                }
                return g_1495;
            }
            else
            { /* block id: 703 */
                if ((*g_1149))
                    break;
            }
            for (g_347.f4 = 0; (g_347.f4 != 0); g_347.f4 = safe_add_func_uint8_t_u_u(g_347.f4, 5))
            { /* block id: 708 */
                int32_t *l_1498 = &l_1436;
                int32_t *l_1499 = &l_1436;
                int32_t *l_1500 = &g_35;
                int32_t *l_1501[5] = {&g_642.f2,&g_642.f2,&g_642.f2,&g_642.f2,&g_642.f2};
                int i;
                --l_1503;
                return g_1506;
            }
        }
        (*l_1509) = g_1507;
    }
    return g_1510;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static struct S0 * func_2(struct S0 * p_3, struct S0 * p_4, int32_t  p_5, uint8_t  p_6)
{ /* block id: 1 */
    p_4 = &g_8;
    return p_4;
}


/* ------------------------------------------ */
/* 
 * reads : g_344.f4 g_218.f1 g_344.f2 g_1149 g_218.f2 g_308 g_309 g_218.f7 g_102.f7 g_520.f2 g_1025.f4 g_218.f6 g_318 g_35 g_220.f5 g_706 g_347.f2 g_530.f3 g_642.f4 g_340 g_163 g_1333 g_554 g_241 g_902.f1 g_530.f5 g_1106 g_1359
 * writes: g_344.f4 g_218.f1 g_344.f2 g_218.f6 g_347.f2 g_530.f3 g_642.f4 g_35 g_318 g_218.f7
 */
static struct S0 ** func_18(struct S0 ** const  p_19, uint8_t  p_20, int32_t * p_21)
{ /* block id: 603 */
    int32_t *l_1265[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t l_1317 = 0x4C79L;
    struct S2 *l_1328 = &g_344;
    uint32_t l_1361[1];
    struct S0 **l_1364[8][1][6];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1361[i] = 18446744073709551612UL;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
                l_1364[i][j][k] = &g_7[0];
        }
    }
    l_1265[6] = p_21;
    for (g_344.f4 = 0; (g_344.f4 == (-21)); g_344.f4 = safe_sub_func_uint32_t_u_u(g_344.f4, 6))
    { /* block id: 607 */
        int64_t l_1273 = 0xE4F1861849E19AC7LL;
        struct S2 *l_1298 = &g_530;
        int32_t l_1310 = 1L;
        int32_t l_1314 = (-8L);
        int32_t l_1315 = 0x615AF09EL;
        int32_t l_1316[8] = {0xF5374C17L,(-1L),0xF5374C17L,(-1L),0xF5374C17L,(-1L),0xF5374C17L,(-1L)};
        uint64_t l_1321 = 0x2C35DDF0E63F695DLL;
        struct S0 **l_1360 = &g_7[0];
        int i;
        for (g_218.f1 = 20; (g_218.f1 == 50); ++g_218.f1)
        { /* block id: 610 */
            int8_t l_1272 = 0L;
            int8_t *l_1307[5][8] = {{&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0},{&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0},{&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0},{&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0},{&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0,&g_451.f3,(void*)0}};
            int i, j;
            for (g_344.f2 = 0; (g_344.f2 > 21); ++g_344.f2)
            { /* block id: 613 */
                uint8_t l_1274 = 247UL;
                int32_t **l_1300 = &l_1265[6];
                int32_t ***l_1299 = &l_1300;
                int64_t *l_1305 = &g_218.f6;
                int32_t l_1306 = (-5L);
                uint32_t *l_1308 = (void*)0;
                uint32_t *l_1309 = &g_347.f2;
                --l_1274;
                l_1310 = (((*l_1309) ^= (safe_add_func_int64_t_s_s((l_1273 || (safe_sub_func_int16_t_s_s((safe_mod_func_int32_t_s_s((((safe_add_func_int8_t_s_s((safe_sub_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((((((safe_mod_func_int32_t_s_s(((*p_21) = 0x3613C67FL), (*g_1149))) , ((safe_mod_func_uint32_t_u_u((((*l_1305) ^= ((((+(p_20 & (safe_div_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s(((void*)0 == l_1298), 5)) > (l_1299 != (((safe_mul_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(((**g_308) != 0UL), g_102[5][1].f7)), p_20)) != g_520[0].f2) , &g_1106))), g_1025[0][8].f4)))) , p_20) , 0xCC44L) | 0x563EL)) >= l_1306), (*g_318))) < 3L)) , l_1307[2][5]) == (void*)0) != p_20), 14)), g_220[3][9].f5)), l_1272)) && g_706[3]) == p_20), 0x2D47D664L)), 0xE48BL))), p_20))) && p_20);
            }
        }
        for (g_530.f3 = (-26); (g_530.f3 >= (-3)); g_530.f3 = safe_add_func_int8_t_s_s(g_530.f3, 9))
        { /* block id: 623 */
            int32_t l_1313[3];
            int16_t l_1320 = 0xAEDCL;
            int16_t *l_1358 = &l_1320;
            int i;
            for (i = 0; i < 3; i++)
                l_1313[i] = 0x5D8BAB13L;
            l_1317--;
            --l_1321;
            for (g_642.f4 = 0; (g_642.f4 < 32); g_642.f4++)
            { /* block id: 628 */
                (*g_318) = ((*g_340) & 18446744073709551615UL);
            }
            (*g_318) = ((safe_rshift_func_uint16_t_u_u(((((4UL & (l_1328 != ((safe_lshift_func_uint64_t_u_s((safe_div_func_uint8_t_u_u((g_1333 , (safe_lshift_func_int32_t_s_s(((safe_add_func_int8_t_s_s(1L, (((p_20 && ((((*g_554) , ((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(0xCA9AL, ((((safe_mod_func_uint16_t_u_u(3UL, (safe_sub_func_uint64_t_u_u((safe_sub_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(((safe_sub_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_s(((safe_mul_func_int32_t_s_s((safe_add_func_uint16_t_u_u(((l_1358 = &g_168) != (void*)0), p_20)), 4294967289UL)) < g_902.f1), g_530.f5)) == p_20), 1L)) || p_20), 0xC9A5L)), p_20)), 0xD43EFB767EDE9007LL)))) <= p_20) != l_1321) && 0xBECEBAA1L))), (*g_554))) >= 65535UL)) ^ p_20) == p_20)) , (*p_21)) == l_1314))) || p_20), (*p_21)))), 0x04L)), 27)) , (void*)0))) <= 0UL) == 0x3D8B44EAL) == 0xC8L), 11)) < p_20);
        }
        (*g_1359) = (*g_1106);
        return l_1360;
    }
    l_1361[0]--;
    return l_1364[3][0][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_27.f4 g_35 g_1106 g_309 g_1046 g_194 g_1010 g_1011 g_218.f1 g_852 g_853 g_157 g_725.f4 g_15 g_7
 * writes: g_35 g_218.f1 g_641 g_1114 g_1115 g_218.f7
 */
static struct S0 ** const  func_22(struct S0 * p_23, uint8_t  p_24, uint32_t  p_25)
{ /* block id: 9 */
    int32_t l_38 = (-4L);
    struct S0 *l_1107 = &g_1108;
    int32_t l_1125 = (-5L);
    int32_t l_1163[1];
    struct S3 **** const l_1198 = &g_852;
    uint64_t l_1219 = 18446744073709551613UL;
    int32_t *l_1264 = &g_601.f7;
    int i;
    for (i = 0; i < 1; i++)
        l_1163[i] = 0L;
    for (p_25 = (-13); (p_25 >= 14); p_25 = safe_add_func_int16_t_s_s(p_25, 1))
    { /* block id: 12 */
        const int32_t *l_33 = &g_15;
        int32_t *l_34 = &g_35;
        uint64_t **l_1150[1];
        int32_t l_1153[10] = {(-4L),5L,(-4L),5L,(-4L),5L,(-4L),5L,(-4L),5L};
        uint16_t l_1167 = 65533UL;
        struct S3 *l_1190 = &g_102[2][2];
        int32_t l_1221 = 0x2AFF2B10L;
        struct S3 ****l_1222 = &g_852;
        uint32_t *l_1252 = (void*)0;
        uint32_t *l_1253 = (void*)0;
        uint32_t *l_1254 = &g_218.f1;
        int32_t **l_1263[8] = {&l_34,&l_34,&l_34,&l_34,&l_34,&l_34,&l_34,&l_34};
        int i;
        for (i = 0; i < 1; i++)
            l_1150[i] = &g_340;
        (*l_34) ^= (safe_sub_func_int16_t_s_s(g_27.f4, ((void*)0 != l_33)));
        for (p_24 = (-29); (p_24 >= 7); p_24 = safe_add_func_int16_t_s_s(p_24, 5))
        { /* block id: 16 */
            int8_t l_51 = 0xFAL;
            uint64_t l_1124[9][8] = {{0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL,1UL,0xD42607D7E6B8C1EDLL,0xB5A7F6FD5308D8D1LL,0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL,1UL},{0xB5A7F6FD5308D8D1LL,0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL,1UL,0xD42607D7E6B8C1EDLL,0xB5A7F6FD5308D8D1LL,0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL},{0x65C3DE6174DB6858LL,0xD42607D7E6B8C1EDLL,0xD42607D7E6B8C1EDLL,0x65C3DE6174DB6858LL,0UL,0x65C3DE6174DB6858LL,0xD42607D7E6B8C1EDLL,0xD42607D7E6B8C1EDLL},{0xD42607D7E6B8C1EDLL,0UL,1UL,1UL,0UL,0xD42607D7E6B8C1EDLL,0UL,1UL},{0x65C3DE6174DB6858LL,0UL,0x65C3DE6174DB6858LL,0xD42607D7E6B8C1EDLL,0xD42607D7E6B8C1EDLL,0x65C3DE6174DB6858LL,0UL,0x65C3DE6174DB6858LL},{0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL,1UL,0xD42607D7E6B8C1EDLL,0xB5A7F6FD5308D8D1LL,0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL,1UL},{0xB5A7F6FD5308D8D1LL,0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL,1UL,0xD42607D7E6B8C1EDLL,0xB5A7F6FD5308D8D1LL,0xB5A7F6FD5308D8D1LL,0xD42607D7E6B8C1EDLL},{0x65C3DE6174DB6858LL,0xD42607D7E6B8C1EDLL,0xD42607D7E6B8C1EDLL,0x65C3DE6174DB6858LL,0UL,0x65C3DE6174DB6858LL,0xD42607D7E6B8C1EDLL,0xD42607D7E6B8C1EDLL},{0xD42607D7E6B8C1EDLL,0UL,1UL,1UL,0UL,0xD42607D7E6B8C1EDLL,0UL,1UL}};
            struct S2 ****l_1141[5];
            int32_t l_1154[8];
            int i, j;
            for (i = 0; i < 5; i++)
                l_1141[i] = &g_1012;
            for (i = 0; i < 8; i++)
                l_1154[i] = 1L;
        }
        l_1264 = func_39((*g_1106), (*g_1046), p_24, ((safe_lshift_func_uint16_t_u_u((((void*)0 == (*g_1010)) , (safe_mul_func_uint32_t_u_u((++(*l_1254)), ((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint64_t_u_u(((((0UL & ((void*)0 == (***l_1222))) & (safe_lshift_func_uint64_t_u_s(p_24, 8))) >= l_1163[0]) | 0x5C2145F6939ED741LL), 25)), l_1125)) <= g_725.f4)))), (*l_33))) , 0xBBL), g_7[0]);
        if (p_25)
            break;
    }
    return &g_7[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_1106 g_309
 * writes: g_641 g_1114 g_1115 g_35 g_218.f7
 */
static int32_t * func_39(int32_t * p_40, const int32_t  p_41, uint8_t  p_42, int8_t  p_43, struct S0 * p_44)
{ /* block id: 549 */
    struct S1 *l_1110 = &g_218;
    struct S1 **l_1111 = &g_641;
    struct S1 *l_1113[9] = {&g_220[3][9],&g_220[3][9],&g_220[3][9],&g_220[3][9],&g_220[3][9],&g_220[3][9],&g_220[3][9],&g_220[3][9],&g_220[3][9]};
    struct S1 **l_1112[3];
    int32_t l_1116 = 0x4083B4AAL;
    int i;
    for (i = 0; i < 3; i++)
        l_1112[i] = &l_1113[6];
    l_1116 = ((*p_40) = (((*l_1111) = l_1110) == (g_1115[7] = (g_1114 = &g_642))));
    return (*g_1106);
}


/* ------------------------------------------ */
/* 
 * reads : g_1106
 * writes: g_309
 */
static int64_t  func_46(int64_t  p_47, uint8_t  p_48, int32_t * p_49, int64_t  p_50)
{ /* block id: 544 */
    struct S0 *l_1095 = &g_1072;
    int32_t l_1098 = 0x4863120FL;
    int32_t l_1099 = (-1L);
    uint32_t *l_1100 = &g_451.f2;
    int8_t *l_1105 = &g_451.f3;
    (*g_1106) = p_49;
    return p_48;
}


/* ------------------------------------------ */
/* 
 * reads : g_27.f2 g_82 g_158.f5 g_520 g_522 g_347.f4 g_309 g_530 g_325.f2 g_318 g_35 g_220.f6 g_98 g_339 g_340 g_61 g_254 g_111 g_27.f4 g_163 g_8.f3 g_218.f7 g_554 g_137 g_591 g_595 g_358.f4 g_601 g_218.f2 g_627 g_8 g_27 g_406.f1 g_641 g_102.f0 g_717 g_718 g_747 g_381.f1
 * writes: g_82 g_218.f7 g_35 g_98 g_163 g_554 g_61 g_137 g_369 g_530.f2 g_347.f4 g_218.f2 g_281 g_347.f5 g_111 g_309 g_601.f4 g_725.f5 g_601.f7 g_718
 */
static uint32_t  func_54(uint64_t  p_55, int32_t * const  p_56, uint64_t  p_57)
{ /* block id: 21 */
    const struct S0 *l_512 = (void*)0;
    int32_t l_521 = 4L;
    int32_t l_523 = (-1L);
    int64_t *l_524[10][5] = {{&g_218.f6,&g_98,(void*)0,&g_98,&g_98},{&g_220[3][9].f6,&g_220[3][9].f6,&g_218.f6,&g_218.f6,(void*)0},{&g_218.f6,&g_98,&g_98,&g_218.f6,&g_98},{&g_98,&g_218.f6,&g_98,(void*)0,&g_98},{(void*)0,&g_218.f6,&g_218.f6,&g_220[3][9].f6,&g_98},{&g_220[3][9].f6,&g_98,(void*)0,(void*)0,&g_98},{&g_98,(void*)0,&g_218.f6,&g_218.f6,&g_98},{&g_218.f6,(void*)0,&g_218.f6,&g_218.f6,&g_98},{(void*)0,&g_218.f6,(void*)0,&g_98,&g_98},{&g_218.f6,&g_218.f6,&g_218.f6,(void*)0,&g_98}};
    uint64_t *** const l_539[5][8] = {{&g_339,&g_339,&g_339,&g_339,(void*)0,(void*)0,&g_339,&g_339},{&g_339,&g_339,&g_339,&g_339,(void*)0,(void*)0,&g_339,&g_339},{&g_339,&g_339,&g_339,&g_339,(void*)0,(void*)0,&g_339,&g_339},{&g_339,&g_339,&g_339,&g_339,(void*)0,(void*)0,&g_339,&g_339},{&g_339,&g_339,&g_339,&g_339,(void*)0,(void*)0,&g_339,&g_339}};
    int32_t l_594 = 7L;
    uint64_t ***l_602 = &g_339;
    int32_t l_609 = 0x8F6EED0CL;
    int32_t l_610 = 0x32CC4519L;
    int32_t l_615 = 1L;
    int32_t l_622[3][1];
    struct S3 *l_648 = &g_158;
    int32_t **l_674 = &g_309;
    const uint16_t l_732 = 65535UL;
    int32_t l_733 = 0x7D75679BL;
    int64_t l_741 = 0xBE76B8D577DA0A0ALL;
    struct S0 *l_745[8] = {&g_27,&g_27,&g_27,&g_27,&g_27,&g_27,&g_27,&g_27};
    struct S3 **l_782 = &l_648;
    struct S3 ***l_781[7][4] = {{&l_782,&l_782,&l_782,&l_782},{&l_782,&l_782,&l_782,&l_782},{&l_782,&l_782,&l_782,&l_782},{&l_782,&l_782,&l_782,&l_782},{&l_782,&l_782,&l_782,&l_782},{&l_782,&l_782,&l_782,&l_782},{&l_782,&l_782,&l_782,&l_782}};
    uint32_t *l_790 = &g_601.f4;
    int32_t l_791[3];
    uint64_t l_792[6][2] = {{1UL,0UL},{0UL,0UL},{1UL,0UL},{18446744073709551610UL,18446744073709551610UL},{18446744073709551610UL,0UL},{1UL,0UL}};
    uint64_t l_793 = 0x2A24ADEFA02B6383LL;
    int32_t *l_794 = &g_601.f7;
    struct S2 *l_855 = (void*)0;
    uint8_t l_859[8] = {0x73L,1UL,0x73L,0x73L,1UL,0x73L,0x73L,1UL};
    int8_t l_943 = 0x64L;
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_622[i][j] = 0xA1F06D90L;
    }
    for (i = 0; i < 3; i++)
        l_791[i] = 0xAE444B4DL;
    if ((((l_523 = (((((safe_add_func_uint32_t_u_u((func_64(func_69(g_27.f2), &g_318, l_512, g_158.f5) , 0xF96DEEFAL), ((l_521 = p_57) | ((void*)0 == &g_377[2])))) , g_522) | g_347.f4) & l_523) == 0xE37FL)) && 18446744073709551615UL) >= 6UL))
    { /* block id: 274 */
        uint16_t l_540 = 8UL;
        uint16_t *l_555 = &g_61;
        struct S3 **l_590 = &g_157;
        struct S3 ***l_589 = &l_590;
        int32_t l_608 = 0x418EA428L;
        int32_t l_612 = 0xE47E7542L;
        int32_t l_613 = 0xD6AD1A5BL;
        int32_t l_614 = 0xB3301E41L;
        int32_t l_619 = 0xDCED1D32L;
        int32_t l_620 = 0x08C797A8L;
        int32_t l_621[9] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
        const struct S1 *l_677[1][4][7] = {{{&g_220[3][9],&g_601,&g_220[3][8],&g_218,&g_220[3][8],&g_601,&g_220[3][9]},{&g_601,&g_642,&g_601,&g_601,&g_642,&g_601,&g_601},{&g_220[3][9],&g_218,(void*)0,&g_218,&g_220[3][9],(void*)0,&g_220[3][9]},{&g_642,&g_601,&g_601,&g_642,&g_601,&g_601,&g_642}}};
        uint16_t l_708 = 65534UL;
        struct S2 *l_734 = &g_347;
        int i, j, k;
        for (l_521 = 18; (l_521 == 27); l_521 = safe_add_func_int64_t_s_s(l_521, 1))
        { /* block id: 277 */
            uint16_t l_527[4];
            uint16_t **l_553 = (void*)0;
            int32_t l_562 = 0x154FC3B1L;
            uint16_t *l_563 = &g_61;
            struct S3 **l_567 = &g_157;
            struct S3 ***l_566 = &l_567;
            int32_t l_568 = 0xFA36AFAAL;
            int32_t l_569 = 0L;
            int32_t l_623 = 0L;
            uint16_t l_624 = 1UL;
            int i;
            for (i = 0; i < 4; i++)
                l_527[i] = 0x1F0AL;
            (*g_309) = l_527[1];
            l_523 = (safe_mod_func_uint64_t_u_u((((g_530 , func_69(l_523)) == (void*)0) && (((safe_mul_func_uint32_t_u_u((safe_div_func_int16_t_s_s(g_325.f2, ((-1L) | (((safe_rshift_func_uint64_t_u_u(((**g_339) = ((l_540 = ((((*g_318) ^= (-1L)) , (((safe_mod_func_int64_t_s_s((g_98 |= (g_220[3][9].f6 & 0L)), 1L)) , l_539[1][1]) == (void*)0)) > p_57)) , 18446744073709551607UL)), 60)) > p_57) >= l_527[1])))), p_57)) && l_540) , 18446744073709551615UL)), p_55));
            if (((*g_309) &= (l_521 <= (l_569 &= (safe_sub_func_int16_t_s_s(p_55, (p_55 , (((*p_56) = (l_568 ^= ((safe_lshift_func_int32_t_s_u(((safe_lshift_func_int32_t_s_s((safe_lshift_func_int32_t_s_u(((safe_mul_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u(((g_554 = &l_540) != (l_555 = &g_281)), (safe_rshift_func_uint32_t_u_s((+(!(safe_rshift_func_uint16_t_u_u(((*l_563)--), ((((*g_254) , ((*l_566) = &g_157)) != (void*)0) ^ g_27.f4))))), l_562)))), p_55)) | p_57), 27)), (*g_318))) & (*g_340)), g_8.f3)) > 0L))) , p_55))))))))
            { /* block id: 292 */
                uint8_t *l_585 = &g_137[0];
                uint8_t *l_588 = &g_369;
                uint32_t *l_593 = &g_530.f2;
                int32_t *l_596 = &g_347.f4;
                uint64_t ****l_603 = (void*)0;
                uint64_t ****l_604 = &l_602;
                (*l_596) |= ((safe_add_func_uint8_t_u_u((((safe_mul_func_int8_t_s_s((safe_unary_minus_func_int32_t_s((safe_lshift_func_int16_t_s_u((safe_unary_minus_func_int64_t_s((-1L))), ((((((((safe_mod_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((~((((&g_218 == (void*)0) <= ((*g_554) < p_55)) != (l_594 = (safe_mul_func_int32_t_s_s(((0xACE1250D6CBC7449LL <= (p_57 || (((((*l_588) = (++(*l_585))) & 1L) != (l_523 |= ((*l_593) |= (l_589 == g_591)))) == l_540))) >= l_521), l_521)))) >= 1L)), g_595[0])), p_55)) > 0x5DDA45B8L) > p_57) >= (*g_309)) ^ p_57) || l_568) || g_358.f4) != 0x80EC02B98DAD9A5FLL))))), l_521)) < g_520[0].f8) | 0x1F75FAF8L), g_520[0].f1)) , (*p_56));
                (*g_309) |= (safe_rshift_func_int16_t_s_u(p_55, (safe_div_func_uint16_t_u_u((g_601 , ((l_540 < p_57) , (((*l_604) = l_602) == (void*)0))), 65534UL))));
            }
            else
            { /* block id: 301 */
                int64_t l_611 = 0x0B1624597E2329E9LL;
                int32_t l_616 = 2L;
                int32_t l_617 = (-4L);
                int32_t l_618[10][10] = {{0x7F2F737DL,(-5L),0xB4001764L,0x6906157CL,(-4L),9L,0x4A5ABF21L,9L,(-10L),5L},{6L,0x7F2F737DL,0xF53F5CD7L,0x26C3A5D3L,0L,(-5L),0L,0x26C3A5D3L,0xF53F5CD7L,0x7F2F737DL},{(-9L),(-4L),5L,0x6906157CL,1L,0x5EE3B5C5L,0x6906157CL,0L,1L,(-9L)},{5L,6L,(-4L),1L,0x7F2F737DL,0x5EE3B5C5L,9L,6L,(-9L),(-9L)},{(-9L),0x26C3A5D3L,(-5L),6L,6L,(-5L),0x26C3A5D3L,(-9L),(-7L),0L},{6L,9L,0x5EE3B5C5L,0x7F2F737DL,1L,(-4L),6L,5L,0L,6L},{0L,0x6906157CL,0x5EE3B5C5L,1L,0x6906157CL,5L,(-4L),(-9L),(-4L),5L},{0x26C3A5D3L,0L,(-5L),0L,0x26C3A5D3L,0xF53F5CD7L,0x7F2F737DL,6L,0xF53F5CD7L,9L},{9L,9L,(-4L),9L,0x5EE3B5C5L,(-9L),0x4A5ABF21L,0L,1L,9L},{0L,9L,5L,0x5EE3B5C5L,0x26C3A5D3L,9L,9L,0x26C3A5D3L,0x5EE3B5C5L,5L}};
                int32_t *l_637 = (void*)0;
                const struct S1 *l_640 = &g_218;
                int i, j;
                for (g_218.f2 = (-26); (g_218.f2 >= 10); g_218.f2 = safe_add_func_uint16_t_u_u(g_218.f2, 2))
                { /* block id: 304 */
                    int32_t *l_607[10][5] = {{&l_568,(void*)0,&g_220[3][9].f7,&g_601.f7,&g_344.f4},{&g_530.f4,&l_594,(void*)0,&l_594,&g_601.f2},{(void*)0,&l_568,&g_220[3][9].f7,&g_344.f4,&l_521},{&g_347.f4,&g_601.f7,&g_347.f4,&g_347.f4,&g_601.f7},{&g_601.f2,&g_601.f7,&g_601.f7,&l_568,&g_347.f4},{&g_347.f4,&l_568,&l_521,&g_530.f4,&g_601.f7},{&g_220[3][9].f7,&l_594,&g_347.f4,(void*)0,(void*)0},{&g_347.f4,(void*)0,&l_594,&g_347.f4,&g_601.f7},{&g_601.f2,&g_530.f4,&g_530.f4,&g_601.f2,&g_601.f7},{&g_347.f4,&l_594,(void*)0,&g_347.f4,(void*)0}};
                    int i, j;
                    l_624--;
                    for (g_281 = 0; g_281 < 10; g_281 += 1)
                    {
                        for (g_347.f5 = 0; g_347.f5 < 10; g_347.f5 += 1)
                        {
                            l_618[g_281][g_347.f5] = 0x17B81FB9L;
                        }
                    }
                    if (((g_627[4] , l_619) , (((*p_56) |= (!1UL)) >= ((((p_57 , 0x95L) >= (((((safe_add_func_int32_t_s_s((safe_mod_func_int64_t_s_s(p_57, (g_8 , p_57))), ((safe_rshift_func_uint16_t_u_s(((safe_div_func_uint16_t_u_u(l_540, p_55)) > p_55), p_57)) , 0L))) > 0UL) , g_8) , &p_55) != (void*)0)) != (-1L)) ^ p_57))))
                    { /* block id: 308 */
                        volatile struct S3 *l_639 = &g_111;
                        l_637 = &l_622[1][0];
                        (*l_639) = (*g_254);
                    }
                    else
                    { /* block id: 311 */
                        (*g_309) |= (((g_27 , g_406[2].f1) , l_640) == g_641);
                    }
                }
                (*g_318) &= 1L;
            }
        }
        for (l_594 = (-24); (l_594 != (-7)); l_594++)
        { /* block id: 320 */
            uint8_t l_661 = 1UL;
            int32_t l_703 = 0L;
            int32_t l_704 = (-6L);
            int32_t l_707 = 5L;
            uint16_t l_742 = 8UL;
            int32_t *l_750 = &g_279;
            struct S2 **l_753 = &g_377[3];
            struct S0 *l_754[3];
            int32_t *l_756 = &l_622[1][0];
            int i;
            for (i = 0; i < 3; i++)
                l_754[i] = &g_755;
        }
        (*l_674) = &l_620;
    }
    else
    { /* block id: 379 */
        int32_t *l_757 = &g_601.f7;
        int32_t *l_758 = &g_451.f4;
        int32_t *l_759 = &g_601.f2;
        int32_t *l_760 = &g_218.f2;
        int32_t *l_761 = &g_530.f4;
        int32_t *l_762 = &l_610;
        int32_t *l_763[1];
        uint32_t l_764 = 0xDB9954D8L;
        int i;
        for (i = 0; i < 1; i++)
            l_763[i] = &g_642.f7;
        ++l_764;
    }
    (*l_794) |= (~(safe_lshift_func_uint64_t_u_s((0x9BF85C3FL <= (safe_div_func_int8_t_s_s((safe_add_func_int64_t_s_s((0L & ((((safe_rshift_func_uint8_t_u_s(((((safe_unary_minus_func_uint32_t_u(((((((g_601.f1 | ((safe_add_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((p_57 = (*g_340)), ((void*)0 == l_781[1][0]))), (safe_mul_func_int16_t_s_s(g_358.f4, ((((safe_div_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u((((g_725.f5 = ((!((*l_790) = (p_55 & l_732))) && g_530.f5)) == 5UL) | l_732), l_791[1])), (*p_56))) , p_55) ^ p_55) && p_55))))) || p_55)) & p_55) < (-1L)) , p_55) , l_792[2][0]) >= g_8.f0))) || (*p_56)) && g_102[5][1].f0) != p_55), p_55)) & 0xEA8D4E24L) , 0x8657L) , l_793)), (**g_339))), 255UL))), 2)));
    (*g_717) = (*g_717);
    for (g_530.f2 = 0; (g_530.f2 <= 1); g_530.f2 += 1)
    { /* block id: 389 */
        int32_t *l_795 = (void*)0;
        int32_t *l_796 = &g_601.f2;
        int32_t *l_797 = &l_733;
        int32_t *l_798 = &l_521;
        int32_t l_799 = (-10L);
        int32_t l_800 = (-10L);
        int32_t *l_801 = &g_35;
        int32_t *l_802 = &l_615;
        int32_t *l_803 = &l_799;
        int32_t *l_804 = &g_35;
        int32_t *l_805 = &l_800;
        int32_t *l_806 = &g_218.f7;
        int32_t *l_807[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        uint16_t l_808 = 5UL;
        uint32_t l_817 = 1UL;
        int16_t *l_887 = &g_278;
        struct S2 ***l_898 = &g_376;
        struct S2 * const **l_899[2];
        uint8_t *l_910 = (void*)0;
        uint8_t *l_911 = &g_220[3][9].f3;
        uint32_t l_912[10][5] = {{3UL,3UL,0x8B185650L,6UL,4294967295UL},{0xDE789E89L,3UL,1UL,0xBDF583FDL,4294967295UL},{3UL,0xDE789E89L,1UL,6UL,1UL},{3UL,3UL,0x8B185650L,6UL,4294967295UL},{0xDE789E89L,3UL,1UL,0xBDF583FDL,4294967295UL},{3UL,0xDE789E89L,1UL,6UL,1UL},{3UL,3UL,0x8B185650L,6UL,4294967295UL},{0xDE789E89L,3UL,1UL,0xBDF583FDL,4294967295UL},{3UL,0xDE789E89L,1UL,6UL,1UL},{1UL,1UL,0xE2FD0EA3L,4294967286UL,0x26B5F05AL}};
        uint8_t l_988 = 255UL;
        struct S2 *****l_1013 = (void*)0;
        struct S2 ** const *l_1082 = (void*)0;
        struct S2 ** const **l_1081 = &l_1082;
        int i, j;
        for (i = 0; i < 2; i++)
            l_899[i] = (void*)0;
        for (g_601.f4 = 0; (g_601.f4 <= 7); g_601.f4 += 1)
        { /* block id: 392 */
            int i, j;
            if (l_792[(g_530.f2 + 3)][g_530.f2])
                break;
        }
        l_808--;
    }
    return (*g_747);
}


/* ------------------------------------------ */
/* 
 * reads : g_520
 * writes:
 */
static const struct S2  func_64(uint16_t * p_65, int32_t ** const  p_66, const struct S0 * p_67, uint32_t  p_68)
{ /* block id: 269 */
    int32_t *l_513[8] = {(void*)0,&g_220[3][9].f2,(void*)0,&g_220[3][9].f2,(void*)0,&g_220[3][9].f2,(void*)0,&g_220[3][9].f2};
    int8_t l_514[6][4] = {{0xE3L,0xE3L,0xE3L,0xE3L},{0xE3L,0xE3L,0xE3L,0xE3L},{0xE3L,0xE3L,0xE3L,0xE3L},{0xE3L,0xE3L,0xE3L,0xE3L},{0xE3L,0xE3L,0xE3L,0xE3L},{0xE3L,0xE3L,0xE3L,0xE3L}};
    int32_t l_515[1];
    int32_t l_516 = 0xD5C248F8L;
    uint16_t l_517 = 0xD777L;
    int i, j;
    for (i = 0; i < 1; i++)
        l_515[i] = 0xC7C2A8DDL;
    --l_517;
    return g_520[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_82
 * writes: g_82
 */
static uint16_t * func_69(int16_t  p_70)
{ /* block id: 22 */
    uint8_t l_77 = 255UL;
    int32_t l_81 = 0x781FA89BL;
    struct S0 *l_91 = &g_27;
    struct S0 *l_92 = &g_27;
    int64_t *l_97 = &g_98;
    int32_t **l_314 = &g_309;
    int32_t *l_315[4];
    int32_t **l_316 = (void*)0;
    int32_t **l_317[9];
    struct S2 *l_346[4][2] = {{(void*)0,&g_347},{(void*)0,(void*)0},{&g_347,(void*)0},{(void*)0,&g_347}};
    struct S3 *l_437 = &g_102[5][1];
    uint8_t l_484 = 0xB9L;
    uint16_t *l_497 = &g_281;
    struct S2 ***l_502 = &g_376;
    uint32_t *l_510 = &g_220[3][9].f4;
    int8_t l_511 = (-4L);
    int i, j;
    for (i = 0; i < 4; i++)
        l_315[i] = &g_218.f2;
    for (i = 0; i < 9; i++)
        l_317[i] = &g_309;
    for (p_70 = 0; (p_70 != (-15)); p_70 = safe_sub_func_uint16_t_u_u(p_70, 5))
    { /* block id: 25 */
        int32_t *l_73 = &g_35;
        int32_t *l_74 = (void*)0;
        int32_t *l_75 = &g_35;
        int32_t *l_76[10];
        int16_t l_80 = 0L;
        int i;
        for (i = 0; i < 10; i++)
            l_76[i] = &g_35;
        --l_77;
        g_82--;
    }
    return &g_61;
}


/* ------------------------------------------ */
/* 
 * reads : g_27.f4 g_35 g_102 g_8.f5 g_61 g_111 g_27.f3 g_27.f5 g_8.f4 g_137 g_147 g_155 g_27 g_157 g_163 g_15 g_168 g_160 g_194 g_218 g_158.f5 g_220 g_221 g_241 g_254 g_255 g_8.f0 g_281 g_158.f0 g_8.f1 g_98 g_303 g_308 g_309
 * writes: g_35 g_102 g_61 g_82 g_8.f0 g_137 g_147 g_98 g_160 g_163 g_168 g_194 g_8.f4 g_220 g_241 g_111 g_281 g_218.f2 g_309 g_218.f4
 */
static int32_t * func_85(struct S0 * p_86, uint32_t  p_87, const int16_t  p_88, uint32_t  p_89, struct S0 * p_90)
{ /* block id: 30 */
    int32_t l_99 = (-7L);
    uint16_t *l_140 = &g_61;
    struct S0 **l_166 = &g_7[0];
    const int8_t l_169 = 0x63L;
    int32_t l_230[6][2][1] = {{{0x2F6EB1F3L},{0x5954D822L}},{{0x3F1AEBCFL},{0x5954D822L}},{{0x2F6EB1F3L},{8L}},{{0x2F6EB1F3L},{0x5954D822L}},{{0x3F1AEBCFL},{0x5954D822L}},{{0x2F6EB1F3L},{8L}}};
    struct S2 *l_313 = (void*)0;
    int i, j, k;
    l_99 = g_27.f4;
    for (g_35 = 0; (g_35 == (-4)); g_35 = safe_sub_func_uint32_t_u_u(g_35, 3))
    { /* block id: 34 */
        struct S3 *l_103 = &g_102[4][2];
        int32_t *l_106[3][2][2] = {{{&g_35,&g_35},{&g_35,&g_35}},{{&g_35,&g_35},{&g_35,&g_35}},{{&g_35,&g_35},{&g_35,&g_35}}};
        int i, j, k;
        (*l_103) = g_102[5][1];
        for (l_99 = 17; (l_99 > (-2)); l_99 = safe_sub_func_uint8_t_u_u(l_99, 1))
        { /* block id: 38 */
            return &g_15;
        }
        return l_106[2][1][0];
    }
    if (g_8.f5)
    { /* block id: 43 */
        const int64_t l_115[10][7][3] = {{{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL}},{{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL}},{{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL}},{{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL}},{{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL}},{{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL}},{{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL}},{{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL}},{{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL}},{{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL},{0x6794CA015B29D1CALL,0x6794CA015B29D1CALL,0x6794CA015B29D1CALL},{0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL,0x69C76AAD2A6E1962LL}}};
        uint32_t l_123 = 8UL;
        struct S3 * const l_124 = &g_102[3][1];
        struct S3 *l_125 = (void*)0;
        uint16_t *l_136[6][3] = {{(void*)0,&g_61,(void*)0},{&g_61,&g_61,&g_61},{(void*)0,&g_61,(void*)0},{&g_61,&g_61,&g_61},{(void*)0,&g_61,(void*)0},{&g_61,&g_61,&g_61}};
        uint16_t **l_141 = &l_136[5][2];
        int8_t *l_146 = &g_147;
        int32_t *l_156 = &g_15;
        int32_t l_197 = 5L;
        int32_t l_236[1];
        uint32_t l_302 = 0xA706C5D2L;
        int32_t **l_307 = (void*)0;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_236[i] = (-1L);
        for (g_61 = 13; (g_61 < 17); ++g_61)
        { /* block id: 46 */
            uint64_t l_114 = 0UL;
            uint32_t *l_116 = &g_82;
            int32_t *l_126 = (void*)0;
            int32_t *l_127 = &l_99;
            int32_t *l_128 = (void*)0;
            int32_t *l_129 = &g_35;
            uint16_t *l_130 = (void*)0;
            uint16_t **l_131 = &l_130;
            (*l_129) = ((*l_127) = (safe_div_func_uint16_t_u_u((((((g_111 , (safe_div_func_int32_t_s_s((l_114 < ((*l_116) = (l_115[1][2][0] & l_99))), ((g_27.f3 && g_35) , p_88)))) != l_114) != (safe_rshift_func_int32_t_s_u((g_8.f0 = ((((((safe_sub_func_uint8_t_u_u(((((((safe_mod_func_int16_t_s_s(((((0x826EL > g_27.f5) ^ g_102[5][1].f4) <= p_87) == g_8.f5), p_89)) , l_123) || p_89) != 0xC1L) > l_115[1][6][0]) , g_102[5][1].f4), l_123)) == p_89) , 0x7220L) > 1L) && p_89) , g_102[5][1].f4)), 14))) , l_124) == l_125), 0xCB49L)));
            if (g_8.f5)
                continue;
            (*l_127) |= (((*l_131) = l_130) == ((safe_rshift_func_int64_t_s_u((safe_lshift_func_int8_t_s_s(((void*)0 == l_136[5][2]), 0)), g_8.f4)) , l_136[5][2]));
            return &g_35;
        }
lbl_306:
        if ((((*l_146) &= (g_102[5][1].f5 != ((g_137[0]--) < (g_111.f1 & ((l_140 != ((*l_141) = l_140)) & (safe_add_func_int8_t_s_s(l_115[1][2][0], (safe_mod_func_uint16_t_u_u(((void*)0 == l_125), p_88))))))))) | (-10L)))
        { /* block id: 59 */
            return &g_35;
        }
        else
        { /* block id: 61 */
            int64_t *l_159 = &g_98;
            uint64_t *l_161 = (void*)0;
            uint64_t *l_162[10];
            int16_t *l_167[10] = {&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168};
            int32_t l_192 = 0L;
            uint32_t l_193 = 4294967295UL;
            int8_t l_195[8][5] = {{0x24L,(-10L),0x24L,(-10L),0x24L},{0xCEL,0xCEL,0x00L,0x00L,0xCEL},{0xB7L,(-10L),0xB7L,(-10L),0xB7L},{0xCEL,0x00L,0x00L,0xCEL,0xCEL},{0x24L,(-10L),0x24L,(-10L),0x24L},{0xCEL,0xCEL,0x00L,0x00L,0xCEL},{0xB7L,(-10L),0xB7L,(-10L),0xB7L},{0xCEL,0x00L,0x00L,0xCEL,0xCEL}};
            uint32_t l_196 = 0x14F9EE90L;
            struct S3 *l_214[4][8] = {{&g_158,&g_102[8][2],&g_158,(void*)0,&g_158,(void*)0,&g_158,&g_102[8][2]},{&g_158,(void*)0,&g_158,&g_102[8][2],&g_158,(void*)0,&g_158,(void*)0},{&g_158,&g_102[8][2],&g_158,&g_102[8][2],&g_158,(void*)0,&g_158,&g_102[8][2]},{&g_158,&g_102[8][2],&g_158,(void*)0,&g_158,(void*)0,&g_158,&g_102[8][2]}};
            int32_t l_229 = 0x5F07CB16L;
            int32_t l_235 = (-1L);
            int32_t l_238 = 0x0BC8F227L;
            int8_t l_253 = 1L;
            int32_t l_261 = 0xCB2F4937L;
            int32_t l_262 = (-9L);
            int32_t l_264 = 0x2856A77FL;
            int32_t l_270 = 0xA13E250AL;
            int32_t l_271 = 0xB03AE8D2L;
            int32_t l_272[7][1];
            int32_t l_275 = (-1L);
            uint32_t l_288 = 0UL;
            int32_t *l_292 = &l_192;
            int i, j;
            for (i = 0; i < 10; i++)
                l_162[i] = &g_163;
            for (i = 0; i < 7; i++)
            {
                for (j = 0; j < 1; j++)
                    l_272[i][j] = (-1L);
            }
            if (((l_99 = (safe_mod_func_int8_t_s_s(l_99, ((+((safe_mul_func_uint64_t_u_u((safe_rshift_func_int8_t_s_s(((*l_146) = (((g_160 = ((*l_159) = (g_155 , (((g_27 , (void*)0) == l_156) != ((void*)0 == g_157))))) != (--g_163)) < (((l_166 = (void*)0) == (void*)0) > p_89))), 0)), 0x89A75344A4BF25F1LL)) | 0UL)) && (*l_156))))) , l_169))
            { /* block id: 68 */
                int32_t *l_173 = &g_35;
                for (g_168 = 18; (g_168 != (-7)); g_168 = safe_sub_func_uint16_t_u_u(g_168, 1))
                { /* block id: 71 */
                    int32_t **l_172[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_172[i] = &l_156;
                    l_173 = &g_35;
                    for (g_160 = 0; (g_160 < 14); g_160++)
                    { /* block id: 75 */
                        int32_t l_178 = 0x2178CD30L;
                        l_178 &= (safe_div_func_uint64_t_u_u(0xB425E4176206665CLL, p_88));
                        l_197 ^= (safe_unary_minus_func_int8_t_s(((l_99 = (l_195[6][3] = (g_194 ^= (safe_add_func_int8_t_s_s(((*l_146) &= ((0x91L ^ (safe_add_func_int64_t_s_s(p_88, (p_87 > p_88)))) , (p_87 ^ (safe_lshift_func_uint32_t_u_u(4294967295UL, 28))))), (((l_192 = (safe_div_func_int64_t_s_s((g_111 , (safe_lshift_func_int32_t_s_s((safe_mul_func_uint64_t_u_u((l_178 , ((0xF12BL & p_88) && 6L)), 0x20CA041FCA2756FDLL)), 2))), p_89))) && l_193) > p_88)))))) <= l_196)));
                    }
                }
                return &g_35;
            }
            else
            { /* block id: 86 */
                struct S3 **l_215[2][4][10] = {{{&l_214[3][4],(void*)0,&l_125,&l_214[3][4],(void*)0,&l_214[2][3],&g_157,&l_125,&l_214[1][6],&l_214[1][6]},{(void*)0,&l_125,&l_214[2][3],&l_214[2][3],&l_214[2][3],&l_214[2][3],&l_125,(void*)0,&l_214[3][4],&g_157},{(void*)0,&l_125,(void*)0,(void*)0,&l_214[0][4],&l_214[2][3],(void*)0,&g_157,(void*)0,&l_214[2][3]},{&l_214[2][3],&l_214[0][4],(void*)0,&l_214[0][4],&l_214[2][3],&g_157,&l_214[3][4],(void*)0,&l_125,&l_214[2][3]}},{{&l_214[3][4],(void*)0,&l_214[2][3],&g_157,&l_125,&l_214[1][6],&l_214[1][6],&l_125,&g_157,&l_214[2][3]},{&g_157,&g_157,&l_125,&l_214[2][3],&l_214[2][3],&g_157,&l_125,(void*)0,(void*)0,&l_214[2][3]},{(void*)0,(void*)0,&l_125,&l_125,&l_214[0][4],&l_125,&l_125,(void*)0,(void*)0,&g_157},{(void*)0,&g_157,(void*)0,&l_214[2][3],&l_214[2][3],(void*)0,&l_214[1][6],&l_214[0][4],&l_214[0][4],&l_214[1][6]}}};
                uint16_t l_219 = 0xC77CL;
                int32_t l_231 = (-8L);
                int32_t l_232 = 4L;
                int32_t l_233 = 0xFCF1D40AL;
                int32_t l_234 = 0xF02F135AL;
                int32_t l_237 = 6L;
                int32_t l_239 = 0xC7762DA0L;
                int32_t l_240 = 0x894450F6L;
                int32_t l_263 = 0xF677C72FL;
                int32_t l_266 = (-8L);
                int32_t l_267 = 0x726DD106L;
                int32_t l_268 = 0L;
                int32_t l_269[9][4];
                int32_t l_274 = 1L;
                int i, j, k;
                for (i = 0; i < 9; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_269[i][j] = 2L;
                }
                if (((safe_add_func_uint64_t_u_u((safe_lshift_func_uint64_t_u_u((safe_unary_minus_func_uint8_t_u((~((safe_lshift_func_uint8_t_u_s((p_88 || (safe_mul_func_uint32_t_u_u((safe_mod_func_int64_t_s_s((safe_mul_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u((((l_214[2][3] = l_214[2][3]) != &g_111) <= (l_192 = 0x82FC2C680C6B32BBLL)), l_195[6][3])), (g_8.f4 &= (l_169 >= (safe_sub_func_int32_t_s_s(((g_218 , g_158.f5) < g_35), l_219)))))), (*l_156))), p_87))), l_169)) & g_137[0])))), 15)), 0x60BA4DF932BF6445LL)) > g_218.f4))
                { /* block id: 90 */
                    int32_t *l_222 = &g_220[3][9].f7;
                    int32_t *l_223 = &l_192;
                    int32_t *l_224 = &g_35;
                    int32_t *l_225 = &g_220[3][9].f2;
                    int32_t *l_226 = &g_218.f2;
                    int32_t *l_227 = (void*)0;
                    int32_t *l_228[1][7];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 7; j++)
                            l_228[i][j] = &g_218.f7;
                    }
                    (*g_221) = g_220[3][9];
                    --g_241;
                    if ((safe_mod_func_int64_t_s_s((safe_add_func_uint32_t_u_u((safe_unary_minus_func_uint8_t_u((safe_sub_func_uint64_t_u_u(((0L < p_87) != (*l_225)), p_89)))), (((safe_add_func_int8_t_s_s(0x80L, g_8.f4)) != l_240) >= ((((((g_220[3][9].f5 > ((*l_146) = p_89)) , l_169) >= g_220[3][9].f1) <= 0x23L) | 4294967288UL) | l_253)))), l_195[5][0])))
                    { /* block id: 94 */
                        (*g_254) = g_111;
                    }
                    else
                    { /* block id: 96 */
                        (*g_255) = g_218;
                    }
                    (*l_225) &= (safe_sub_func_uint32_t_u_u(g_8.f0, (((void*)0 != &l_195[6][3]) , p_87)));
                }
                else
                { /* block id: 100 */
                    int32_t *l_258 = &l_236[0];
                    int32_t *l_259 = &l_235;
                    int32_t *l_260[2][2];
                    int64_t l_265 = 0x36C75DEE242F2EF0LL;
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_260[i][j] = &g_218.f7;
                    }
                    --g_281;
                    for (l_240 = 0; (l_240 < (-12)); l_240 = safe_sub_func_int16_t_s_s(l_240, 1))
                    { /* block id: 104 */
                        int64_t l_286[7][2] = {{1L,1L},{0x714EEDF3ECA1C97FLL,1L},{1L,0x714EEDF3ECA1C97FLL},{1L,1L},{0x714EEDF3ECA1C97FLL,1L},{1L,0x714EEDF3ECA1C97FLL},{1L,1L}};
                        int32_t l_287 = 0x9342A3F1L;
                        int i, j;
                        ++l_288;
                        if (p_88)
                            goto lbl_293;
                    }
                    return &g_35;
                }
            }
lbl_293:
            (*l_292) &= l_235;
            if (((safe_add_func_int8_t_s_s(((((*l_146) = g_158.f0) <= (((9UL < (((safe_sub_func_int32_t_s_s((g_218.f2 = p_87), ((+0L) == (p_89 , 0L)))) >= ((+p_87) , (safe_mul_func_int32_t_s_s((((g_137[0] , ((((((g_163 = g_27.f3) | p_88) , g_218.f3) > p_88) , (*l_292)) > p_88)) & (*l_292)) >= 9UL), p_87)))) || 0x03E0CC88L)) || g_218.f3) , p_88)) || g_8.f1), l_302)) ^ g_98))
            { /* block id: 115 */
                (*g_303) = g_220[4][7];
                if (g_8.f0)
                    goto lbl_306;
            }
            else
            { /* block id: 117 */
                int32_t **l_304 = &l_292;
                int32_t **l_305 = &l_156;
                (*l_305) = ((*l_304) = &g_35);
            }
        }
        (*g_308) = &l_197;
        for (g_218.f4 = 0; (g_218.f4 != 26); g_218.f4++)
        { /* block id: 126 */
            struct S2 *l_312 = (void*)0;
            if ((*l_156))
                break;
            l_313 = l_312;
        }
    }
    else
    { /* block id: 130 */
        return (*g_308);
    }
    return &g_35;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_8.f0, "g_8.f0", print_hash_value);
    transparent_crc(g_8.f1, "g_8.f1", print_hash_value);
    transparent_crc(g_8.f2, "g_8.f2", print_hash_value);
    transparent_crc(g_8.f3, "g_8.f3", print_hash_value);
    transparent_crc(g_8.f4, "g_8.f4", print_hash_value);
    transparent_crc(g_8.f5, "g_8.f5", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_27.f0, "g_27.f0", print_hash_value);
    transparent_crc(g_27.f1, "g_27.f1", print_hash_value);
    transparent_crc(g_27.f2, "g_27.f2", print_hash_value);
    transparent_crc(g_27.f3, "g_27.f3", print_hash_value);
    transparent_crc(g_27.f4, "g_27.f4", print_hash_value);
    transparent_crc(g_27.f5, "g_27.f5", print_hash_value);
    transparent_crc(g_35, "g_35", print_hash_value);
    transparent_crc(g_61, "g_61", print_hash_value);
    transparent_crc(g_82, "g_82", print_hash_value);
    transparent_crc(g_98, "g_98", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_102[i][j].f0, "g_102[i][j].f0", print_hash_value);
            transparent_crc(g_102[i][j].f1, "g_102[i][j].f1", print_hash_value);
            transparent_crc(g_102[i][j].f2, "g_102[i][j].f2", print_hash_value);
            transparent_crc(g_102[i][j].f3, "g_102[i][j].f3", print_hash_value);
            transparent_crc(g_102[i][j].f4, "g_102[i][j].f4", print_hash_value);
            transparent_crc(g_102[i][j].f5, "g_102[i][j].f5", print_hash_value);
            transparent_crc(g_102[i][j].f6, "g_102[i][j].f6", print_hash_value);
            transparent_crc(g_102[i][j].f7, "g_102[i][j].f7", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_111.f0, "g_111.f0", print_hash_value);
    transparent_crc(g_111.f1, "g_111.f1", print_hash_value);
    transparent_crc(g_111.f2, "g_111.f2", print_hash_value);
    transparent_crc(g_111.f3, "g_111.f3", print_hash_value);
    transparent_crc(g_111.f4, "g_111.f4", print_hash_value);
    transparent_crc(g_111.f5, "g_111.f5", print_hash_value);
    transparent_crc(g_111.f6, "g_111.f6", print_hash_value);
    transparent_crc(g_111.f7, "g_111.f7", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_137[i], "g_137[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_147, "g_147", print_hash_value);
    transparent_crc(g_155.f0, "g_155.f0", print_hash_value);
    transparent_crc(g_155.f1, "g_155.f1", print_hash_value);
    transparent_crc(g_155.f2, "g_155.f2", print_hash_value);
    transparent_crc(g_155.f3, "g_155.f3", print_hash_value);
    transparent_crc(g_155.f4, "g_155.f4", print_hash_value);
    transparent_crc(g_155.f5, "g_155.f5", print_hash_value);
    transparent_crc(g_155.f6, "g_155.f6", print_hash_value);
    transparent_crc(g_155.f7, "g_155.f7", print_hash_value);
    transparent_crc(g_158.f0, "g_158.f0", print_hash_value);
    transparent_crc(g_158.f1, "g_158.f1", print_hash_value);
    transparent_crc(g_158.f2, "g_158.f2", print_hash_value);
    transparent_crc(g_158.f3, "g_158.f3", print_hash_value);
    transparent_crc(g_158.f4, "g_158.f4", print_hash_value);
    transparent_crc(g_158.f5, "g_158.f5", print_hash_value);
    transparent_crc(g_158.f6, "g_158.f6", print_hash_value);
    transparent_crc(g_158.f7, "g_158.f7", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    transparent_crc(g_163, "g_163", print_hash_value);
    transparent_crc(g_168, "g_168", print_hash_value);
    transparent_crc(g_194, "g_194", print_hash_value);
    transparent_crc(g_218.f0, "g_218.f0", print_hash_value);
    transparent_crc(g_218.f1, "g_218.f1", print_hash_value);
    transparent_crc(g_218.f2, "g_218.f2", print_hash_value);
    transparent_crc(g_218.f3, "g_218.f3", print_hash_value);
    transparent_crc(g_218.f4, "g_218.f4", print_hash_value);
    transparent_crc(g_218.f5, "g_218.f5", print_hash_value);
    transparent_crc(g_218.f6, "g_218.f6", print_hash_value);
    transparent_crc(g_218.f7, "g_218.f7", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_220[i][j].f0, "g_220[i][j].f0", print_hash_value);
            transparent_crc(g_220[i][j].f1, "g_220[i][j].f1", print_hash_value);
            transparent_crc(g_220[i][j].f2, "g_220[i][j].f2", print_hash_value);
            transparent_crc(g_220[i][j].f3, "g_220[i][j].f3", print_hash_value);
            transparent_crc(g_220[i][j].f4, "g_220[i][j].f4", print_hash_value);
            transparent_crc(g_220[i][j].f5, "g_220[i][j].f5", print_hash_value);
            transparent_crc(g_220[i][j].f6, "g_220[i][j].f6", print_hash_value);
            transparent_crc(g_220[i][j].f7, "g_220[i][j].f7", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_241, "g_241", print_hash_value);
    transparent_crc(g_273, "g_273", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_276[i][j], "g_276[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_277, "g_277", print_hash_value);
    transparent_crc(g_278, "g_278", print_hash_value);
    transparent_crc(g_279, "g_279", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_280[i][j][k], "g_280[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_281, "g_281", print_hash_value);
    transparent_crc(g_325.f0, "g_325.f0", print_hash_value);
    transparent_crc(g_325.f1, "g_325.f1", print_hash_value);
    transparent_crc(g_325.f2, "g_325.f2", print_hash_value);
    transparent_crc(g_325.f3, "g_325.f3", print_hash_value);
    transparent_crc(g_325.f4, "g_325.f4", print_hash_value);
    transparent_crc(g_325.f5, "g_325.f5", print_hash_value);
    transparent_crc(g_325.f6, "g_325.f6", print_hash_value);
    transparent_crc(g_325.f7, "g_325.f7", print_hash_value);
    transparent_crc(g_344.f0, "g_344.f0", print_hash_value);
    transparent_crc(g_344.f1, "g_344.f1", print_hash_value);
    transparent_crc(g_344.f2, "g_344.f2", print_hash_value);
    transparent_crc(g_344.f3, "g_344.f3", print_hash_value);
    transparent_crc(g_344.f4, "g_344.f4", print_hash_value);
    transparent_crc(g_344.f5, "g_344.f5", print_hash_value);
    transparent_crc(g_344.f6, "g_344.f6", print_hash_value);
    transparent_crc(g_344.f7, "g_344.f7", print_hash_value);
    transparent_crc(g_344.f8, "g_344.f8", print_hash_value);
    transparent_crc(g_347.f0, "g_347.f0", print_hash_value);
    transparent_crc(g_347.f1, "g_347.f1", print_hash_value);
    transparent_crc(g_347.f2, "g_347.f2", print_hash_value);
    transparent_crc(g_347.f3, "g_347.f3", print_hash_value);
    transparent_crc(g_347.f4, "g_347.f4", print_hash_value);
    transparent_crc(g_347.f5, "g_347.f5", print_hash_value);
    transparent_crc(g_347.f6, "g_347.f6", print_hash_value);
    transparent_crc(g_347.f7, "g_347.f7", print_hash_value);
    transparent_crc(g_347.f8, "g_347.f8", print_hash_value);
    transparent_crc(g_358.f0, "g_358.f0", print_hash_value);
    transparent_crc(g_358.f1, "g_358.f1", print_hash_value);
    transparent_crc(g_358.f2, "g_358.f2", print_hash_value);
    transparent_crc(g_358.f3, "g_358.f3", print_hash_value);
    transparent_crc(g_358.f4, "g_358.f4", print_hash_value);
    transparent_crc(g_358.f5, "g_358.f5", print_hash_value);
    transparent_crc(g_358.f6, "g_358.f6", print_hash_value);
    transparent_crc(g_358.f7, "g_358.f7", print_hash_value);
    transparent_crc(g_369, "g_369", print_hash_value);
    transparent_crc(g_381.f0, "g_381.f0", print_hash_value);
    transparent_crc(g_381.f1, "g_381.f1", print_hash_value);
    transparent_crc(g_381.f2, "g_381.f2", print_hash_value);
    transparent_crc(g_381.f3, "g_381.f3", print_hash_value);
    transparent_crc(g_381.f4, "g_381.f4", print_hash_value);
    transparent_crc(g_381.f5, "g_381.f5", print_hash_value);
    transparent_crc(g_381.f6, "g_381.f6", print_hash_value);
    transparent_crc(g_381.f7, "g_381.f7", print_hash_value);
    transparent_crc(g_400, "g_400", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_406[i].f0, "g_406[i].f0", print_hash_value);
        transparent_crc(g_406[i].f1, "g_406[i].f1", print_hash_value);
        transparent_crc(g_406[i].f2, "g_406[i].f2", print_hash_value);
        transparent_crc(g_406[i].f3, "g_406[i].f3", print_hash_value);
        transparent_crc(g_406[i].f4, "g_406[i].f4", print_hash_value);
        transparent_crc(g_406[i].f5, "g_406[i].f5", print_hash_value);
        transparent_crc(g_406[i].f6, "g_406[i].f6", print_hash_value);
        transparent_crc(g_406[i].f7, "g_406[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_424, "g_424", print_hash_value);
    transparent_crc(g_451.f0, "g_451.f0", print_hash_value);
    transparent_crc(g_451.f1, "g_451.f1", print_hash_value);
    transparent_crc(g_451.f2, "g_451.f2", print_hash_value);
    transparent_crc(g_451.f3, "g_451.f3", print_hash_value);
    transparent_crc(g_451.f4, "g_451.f4", print_hash_value);
    transparent_crc(g_451.f5, "g_451.f5", print_hash_value);
    transparent_crc(g_451.f6, "g_451.f6", print_hash_value);
    transparent_crc(g_451.f7, "g_451.f7", print_hash_value);
    transparent_crc(g_451.f8, "g_451.f8", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_520[i].f0, "g_520[i].f0", print_hash_value);
        transparent_crc(g_520[i].f1, "g_520[i].f1", print_hash_value);
        transparent_crc(g_520[i].f2, "g_520[i].f2", print_hash_value);
        transparent_crc(g_520[i].f3, "g_520[i].f3", print_hash_value);
        transparent_crc(g_520[i].f4, "g_520[i].f4", print_hash_value);
        transparent_crc(g_520[i].f5, "g_520[i].f5", print_hash_value);
        transparent_crc(g_520[i].f6, "g_520[i].f6", print_hash_value);
        transparent_crc(g_520[i].f7, "g_520[i].f7", print_hash_value);
        transparent_crc(g_520[i].f8, "g_520[i].f8", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_522, "g_522", print_hash_value);
    transparent_crc(g_530.f0, "g_530.f0", print_hash_value);
    transparent_crc(g_530.f1, "g_530.f1", print_hash_value);
    transparent_crc(g_530.f2, "g_530.f2", print_hash_value);
    transparent_crc(g_530.f3, "g_530.f3", print_hash_value);
    transparent_crc(g_530.f4, "g_530.f4", print_hash_value);
    transparent_crc(g_530.f5, "g_530.f5", print_hash_value);
    transparent_crc(g_530.f6, "g_530.f6", print_hash_value);
    transparent_crc(g_530.f7, "g_530.f7", print_hash_value);
    transparent_crc(g_530.f8, "g_530.f8", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_595[i], "g_595[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_601.f0, "g_601.f0", print_hash_value);
    transparent_crc(g_601.f1, "g_601.f1", print_hash_value);
    transparent_crc(g_601.f2, "g_601.f2", print_hash_value);
    transparent_crc(g_601.f3, "g_601.f3", print_hash_value);
    transparent_crc(g_601.f4, "g_601.f4", print_hash_value);
    transparent_crc(g_601.f5, "g_601.f5", print_hash_value);
    transparent_crc(g_601.f6, "g_601.f6", print_hash_value);
    transparent_crc(g_601.f7, "g_601.f7", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_627[i].f0, "g_627[i].f0", print_hash_value);
        transparent_crc(g_627[i].f1, "g_627[i].f1", print_hash_value);
        transparent_crc(g_627[i].f2, "g_627[i].f2", print_hash_value);
        transparent_crc(g_627[i].f3, "g_627[i].f3", print_hash_value);
        transparent_crc(g_627[i].f4, "g_627[i].f4", print_hash_value);
        transparent_crc(g_627[i].f5, "g_627[i].f5", print_hash_value);
        transparent_crc(g_627[i].f6, "g_627[i].f6", print_hash_value);
        transparent_crc(g_627[i].f7, "g_627[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_642.f0, "g_642.f0", print_hash_value);
    transparent_crc(g_642.f1, "g_642.f1", print_hash_value);
    transparent_crc(g_642.f2, "g_642.f2", print_hash_value);
    transparent_crc(g_642.f3, "g_642.f3", print_hash_value);
    transparent_crc(g_642.f4, "g_642.f4", print_hash_value);
    transparent_crc(g_642.f5, "g_642.f5", print_hash_value);
    transparent_crc(g_642.f6, "g_642.f6", print_hash_value);
    transparent_crc(g_642.f7, "g_642.f7", print_hash_value);
    transparent_crc(g_666.f0, "g_666.f0", print_hash_value);
    transparent_crc(g_666.f1, "g_666.f1", print_hash_value);
    transparent_crc(g_666.f2, "g_666.f2", print_hash_value);
    transparent_crc(g_666.f3, "g_666.f3", print_hash_value);
    transparent_crc(g_666.f4, "g_666.f4", print_hash_value);
    transparent_crc(g_666.f5, "g_666.f5", print_hash_value);
    transparent_crc(g_666.f6, "g_666.f6", print_hash_value);
    transparent_crc(g_666.f7, "g_666.f7", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_706[i], "g_706[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_714.f0, "g_714.f0", print_hash_value);
    transparent_crc(g_714.f1, "g_714.f1", print_hash_value);
    transparent_crc(g_714.f2, "g_714.f2", print_hash_value);
    transparent_crc(g_714.f3, "g_714.f3", print_hash_value);
    transparent_crc(g_714.f4, "g_714.f4", print_hash_value);
    transparent_crc(g_714.f5, "g_714.f5", print_hash_value);
    transparent_crc(g_714.f6, "g_714.f6", print_hash_value);
    transparent_crc(g_714.f7, "g_714.f7", print_hash_value);
    transparent_crc(g_719.f0, "g_719.f0", print_hash_value);
    transparent_crc(g_719.f1, "g_719.f1", print_hash_value);
    transparent_crc(g_719.f2, "g_719.f2", print_hash_value);
    transparent_crc(g_719.f3, "g_719.f3", print_hash_value);
    transparent_crc(g_719.f4, "g_719.f4", print_hash_value);
    transparent_crc(g_719.f5, "g_719.f5", print_hash_value);
    transparent_crc(g_719.f6, "g_719.f6", print_hash_value);
    transparent_crc(g_719.f7, "g_719.f7", print_hash_value);
    transparent_crc(g_719.f8, "g_719.f8", print_hash_value);
    transparent_crc(g_725.f0, "g_725.f0", print_hash_value);
    transparent_crc(g_725.f1, "g_725.f1", print_hash_value);
    transparent_crc(g_725.f2, "g_725.f2", print_hash_value);
    transparent_crc(g_725.f3, "g_725.f3", print_hash_value);
    transparent_crc(g_725.f4, "g_725.f4", print_hash_value);
    transparent_crc(g_725.f5, "g_725.f5", print_hash_value);
    transparent_crc(g_755.f0, "g_755.f0", print_hash_value);
    transparent_crc(g_755.f1, "g_755.f1", print_hash_value);
    transparent_crc(g_755.f2, "g_755.f2", print_hash_value);
    transparent_crc(g_755.f3, "g_755.f3", print_hash_value);
    transparent_crc(g_755.f4, "g_755.f4", print_hash_value);
    transparent_crc(g_755.f5, "g_755.f5", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_858[i][j][k].f0, "g_858[i][j][k].f0", print_hash_value);
                transparent_crc(g_858[i][j][k].f1, "g_858[i][j][k].f1", print_hash_value);
                transparent_crc(g_858[i][j][k].f2, "g_858[i][j][k].f2", print_hash_value);
                transparent_crc(g_858[i][j][k].f3, "g_858[i][j][k].f3", print_hash_value);
                transparent_crc(g_858[i][j][k].f4, "g_858[i][j][k].f4", print_hash_value);
                transparent_crc(g_858[i][j][k].f5, "g_858[i][j][k].f5", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_902.f0, "g_902.f0", print_hash_value);
    transparent_crc(g_902.f1, "g_902.f1", print_hash_value);
    transparent_crc(g_902.f2, "g_902.f2", print_hash_value);
    transparent_crc(g_902.f3, "g_902.f3", print_hash_value);
    transparent_crc(g_902.f4, "g_902.f4", print_hash_value);
    transparent_crc(g_902.f5, "g_902.f5", print_hash_value);
    transparent_crc(g_902.f6, "g_902.f6", print_hash_value);
    transparent_crc(g_902.f7, "g_902.f7", print_hash_value);
    transparent_crc(g_902.f8, "g_902.f8", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1025[i][j].f0, "g_1025[i][j].f0", print_hash_value);
            transparent_crc(g_1025[i][j].f1, "g_1025[i][j].f1", print_hash_value);
            transparent_crc(g_1025[i][j].f2, "g_1025[i][j].f2", print_hash_value);
            transparent_crc(g_1025[i][j].f3, "g_1025[i][j].f3", print_hash_value);
            transparent_crc(g_1025[i][j].f4, "g_1025[i][j].f4", print_hash_value);
            transparent_crc(g_1025[i][j].f5, "g_1025[i][j].f5", print_hash_value);
            transparent_crc(g_1025[i][j].f6, "g_1025[i][j].f6", print_hash_value);
            transparent_crc(g_1025[i][j].f7, "g_1025[i][j].f7", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1070[i].f0, "g_1070[i].f0", print_hash_value);
        transparent_crc(g_1070[i].f1, "g_1070[i].f1", print_hash_value);
        transparent_crc(g_1070[i].f2, "g_1070[i].f2", print_hash_value);
        transparent_crc(g_1070[i].f3, "g_1070[i].f3", print_hash_value);
        transparent_crc(g_1070[i].f4, "g_1070[i].f4", print_hash_value);
        transparent_crc(g_1070[i].f5, "g_1070[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1072.f0, "g_1072.f0", print_hash_value);
    transparent_crc(g_1072.f1, "g_1072.f1", print_hash_value);
    transparent_crc(g_1072.f2, "g_1072.f2", print_hash_value);
    transparent_crc(g_1072.f3, "g_1072.f3", print_hash_value);
    transparent_crc(g_1072.f4, "g_1072.f4", print_hash_value);
    transparent_crc(g_1072.f5, "g_1072.f5", print_hash_value);
    transparent_crc(g_1108.f0, "g_1108.f0", print_hash_value);
    transparent_crc(g_1108.f1, "g_1108.f1", print_hash_value);
    transparent_crc(g_1108.f2, "g_1108.f2", print_hash_value);
    transparent_crc(g_1108.f3, "g_1108.f3", print_hash_value);
    transparent_crc(g_1108.f4, "g_1108.f4", print_hash_value);
    transparent_crc(g_1108.f5, "g_1108.f5", print_hash_value);
    transparent_crc(g_1132.f0, "g_1132.f0", print_hash_value);
    transparent_crc(g_1132.f1, "g_1132.f1", print_hash_value);
    transparent_crc(g_1132.f2, "g_1132.f2", print_hash_value);
    transparent_crc(g_1132.f3, "g_1132.f3", print_hash_value);
    transparent_crc(g_1132.f4, "g_1132.f4", print_hash_value);
    transparent_crc(g_1132.f5, "g_1132.f5", print_hash_value);
    transparent_crc(g_1132.f6, "g_1132.f6", print_hash_value);
    transparent_crc(g_1132.f7, "g_1132.f7", print_hash_value);
    transparent_crc(g_1147, "g_1147", print_hash_value);
    transparent_crc(g_1189, "g_1189", print_hash_value);
    transparent_crc(g_1333.f0, "g_1333.f0", print_hash_value);
    transparent_crc(g_1333.f1, "g_1333.f1", print_hash_value);
    transparent_crc(g_1333.f2, "g_1333.f2", print_hash_value);
    transparent_crc(g_1333.f3, "g_1333.f3", print_hash_value);
    transparent_crc(g_1333.f4, "g_1333.f4", print_hash_value);
    transparent_crc(g_1333.f5, "g_1333.f5", print_hash_value);
    transparent_crc(g_1333.f6, "g_1333.f6", print_hash_value);
    transparent_crc(g_1333.f7, "g_1333.f7", print_hash_value);
    transparent_crc(g_1396.f0, "g_1396.f0", print_hash_value);
    transparent_crc(g_1396.f1, "g_1396.f1", print_hash_value);
    transparent_crc(g_1396.f2, "g_1396.f2", print_hash_value);
    transparent_crc(g_1396.f3, "g_1396.f3", print_hash_value);
    transparent_crc(g_1396.f4, "g_1396.f4", print_hash_value);
    transparent_crc(g_1396.f5, "g_1396.f5", print_hash_value);
    transparent_crc(g_1437.f0, "g_1437.f0", print_hash_value);
    transparent_crc(g_1437.f1, "g_1437.f1", print_hash_value);
    transparent_crc(g_1437.f2, "g_1437.f2", print_hash_value);
    transparent_crc(g_1437.f3, "g_1437.f3", print_hash_value);
    transparent_crc(g_1437.f4, "g_1437.f4", print_hash_value);
    transparent_crc(g_1437.f5, "g_1437.f5", print_hash_value);
    transparent_crc(g_1437.f6, "g_1437.f6", print_hash_value);
    transparent_crc(g_1437.f7, "g_1437.f7", print_hash_value);
    transparent_crc(g_1439.f0, "g_1439.f0", print_hash_value);
    transparent_crc(g_1439.f1, "g_1439.f1", print_hash_value);
    transparent_crc(g_1439.f2, "g_1439.f2", print_hash_value);
    transparent_crc(g_1439.f3, "g_1439.f3", print_hash_value);
    transparent_crc(g_1439.f4, "g_1439.f4", print_hash_value);
    transparent_crc(g_1439.f5, "g_1439.f5", print_hash_value);
    transparent_crc(g_1439.f6, "g_1439.f6", print_hash_value);
    transparent_crc(g_1439.f7, "g_1439.f7", print_hash_value);
    transparent_crc(g_1477, "g_1477", print_hash_value);
    transparent_crc(g_1481.f0, "g_1481.f0", print_hash_value);
    transparent_crc(g_1481.f1, "g_1481.f1", print_hash_value);
    transparent_crc(g_1481.f2, "g_1481.f2", print_hash_value);
    transparent_crc(g_1481.f3, "g_1481.f3", print_hash_value);
    transparent_crc(g_1481.f4, "g_1481.f4", print_hash_value);
    transparent_crc(g_1481.f5, "g_1481.f5", print_hash_value);
    transparent_crc(g_1481.f6, "g_1481.f6", print_hash_value);
    transparent_crc(g_1481.f7, "g_1481.f7", print_hash_value);
    transparent_crc(g_1495.f0, "g_1495.f0", print_hash_value);
    transparent_crc(g_1495.f1, "g_1495.f1", print_hash_value);
    transparent_crc(g_1495.f2, "g_1495.f2", print_hash_value);
    transparent_crc(g_1495.f3, "g_1495.f3", print_hash_value);
    transparent_crc(g_1495.f4, "g_1495.f4", print_hash_value);
    transparent_crc(g_1495.f5, "g_1495.f5", print_hash_value);
    transparent_crc(g_1495.f6, "g_1495.f6", print_hash_value);
    transparent_crc(g_1495.f7, "g_1495.f7", print_hash_value);
    transparent_crc(g_1506.f0, "g_1506.f0", print_hash_value);
    transparent_crc(g_1506.f1, "g_1506.f1", print_hash_value);
    transparent_crc(g_1506.f2, "g_1506.f2", print_hash_value);
    transparent_crc(g_1506.f3, "g_1506.f3", print_hash_value);
    transparent_crc(g_1506.f4, "g_1506.f4", print_hash_value);
    transparent_crc(g_1506.f5, "g_1506.f5", print_hash_value);
    transparent_crc(g_1506.f6, "g_1506.f6", print_hash_value);
    transparent_crc(g_1506.f7, "g_1506.f7", print_hash_value);
    transparent_crc(g_1510.f0, "g_1510.f0", print_hash_value);
    transparent_crc(g_1510.f1, "g_1510.f1", print_hash_value);
    transparent_crc(g_1510.f2, "g_1510.f2", print_hash_value);
    transparent_crc(g_1510.f3, "g_1510.f3", print_hash_value);
    transparent_crc(g_1510.f4, "g_1510.f4", print_hash_value);
    transparent_crc(g_1510.f5, "g_1510.f5", print_hash_value);
    transparent_crc(g_1510.f6, "g_1510.f6", print_hash_value);
    transparent_crc(g_1510.f7, "g_1510.f7", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 373
   depth: 1, occurrence: 27
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 16
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 4
XXX structs with bitfields in the program: 93
breakdown:
   indirect level: 0, occurrence: 27
   indirect level: 1, occurrence: 37
   indirect level: 2, occurrence: 12
   indirect level: 3, occurrence: 6
   indirect level: 4, occurrence: 9
   indirect level: 5, occurrence: 2
XXX full-bitfields structs in the program: 2
breakdown:
   indirect level: 0, occurrence: 2
XXX times a bitfields struct's address is taken: 80
XXX times a bitfields struct on LHS: 1
XXX times a bitfields struct on RHS: 48
XXX times a single bitfield on LHS: 6
XXX times a single bitfield on RHS: 86

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 144
   depth: 2, occurrence: 30
   depth: 3, occurrence: 1
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 8, occurrence: 2
   depth: 13, occurrence: 2
   depth: 16, occurrence: 1
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 3
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 2
   depth: 40, occurrence: 1

XXX total number of pointers: 402

XXX times a variable address is taken: 856
XXX times a pointer is dereferenced on RHS: 148
breakdown:
   depth: 1, occurrence: 112
   depth: 2, occurrence: 32
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 175
breakdown:
   depth: 1, occurrence: 162
   depth: 2, occurrence: 7
   depth: 3, occurrence: 5
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 22
XXX times a pointer is compared with address of another variable: 8
XXX times a pointer is compared with another pointer: 4
XXX times a pointer is qualified to be dereferenced: 5474

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 763
   level: 2, occurrence: 141
   level: 3, occurrence: 40
   level: 4, occurrence: 14
   level: 5, occurrence: 2
XXX number of pointers point to pointers: 138
XXX number of pointers point to scalars: 203
XXX number of pointers point to structs: 61
XXX percent of pointers has null in alias set: 27.9
XXX average alias set size: 1.52

XXX times a non-volatile is read: 1017
XXX times a non-volatile is write: 546
XXX times a volatile is read: 93
XXX    times read thru a pointer: 15
XXX times a volatile is write: 27
XXX    times written thru a pointer: 8
XXX times a volatile is available for access: 4.59e+03
XXX percentage of non-volatile access: 92.9

XXX forward jumps: 2
XXX backward jumps: 2

XXX stmts: 137
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 26
   depth: 2, occurrence: 25
   depth: 3, occurrence: 21
   depth: 4, occurrence: 17
   depth: 5, occurrence: 19

XXX percentage a fresh-made variable is used: 19
XXX percentage an existing variable is used: 81
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
XXX total OOB instances added: 0
********************* end of statistics **********************/


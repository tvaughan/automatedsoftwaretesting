/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3128538008
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   volatile uint32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static volatile int64_t g_2 = 3L;/* VOLATILE GLOBAL g_2 */
static int32_t g_4 = 8L;
static int32_t g_6 = 0xC62E0897L;
static uint64_t g_37 = 18446744073709551614UL;
static int8_t g_41 = 0x54L;
static int8_t g_43 = (-8L);
static int32_t g_48 = 0x320525C7L;
static int64_t g_58 = 0x0F1939EFD9132E88LL;
static uint16_t g_61[2][7] = {{0xDC4EL,0xDC4EL,0xDC4EL,0xDC4EL,0xDC4EL,0xDC4EL,0xDC4EL},{65535UL,0x99EDL,65535UL,0x99EDL,65535UL,0x99EDL,65535UL}};
static int32_t g_91 = 0x2B2FE073L;
static int32_t * volatile g_90 = &g_91;/* VOLATILE GLOBAL g_90 */
static int64_t g_108 = 0L;
static int32_t g_132 = 0xBC330632L;
static int64_t g_135 = 0xAAE810F1DDE4E4C4LL;
static struct S0 g_142 = {4294967295UL};/* VOLATILE GLOBAL g_142 */
static uint16_t g_154 = 1UL;
static uint16_t * const g_153 = &g_154;
static uint32_t g_158 = 0x086DEEB8L;
static int16_t g_164 = 0x0FB8L;
static uint16_t g_175 = 9UL;
static volatile struct S0 g_179 = {0x7714DF77L};/* VOLATILE GLOBAL g_179 */
static uint8_t g_183 = 0xD6L;
static uint16_t *g_189 = &g_154;
static uint16_t **g_188 = &g_189;
static uint16_t *** volatile g_187 = &g_188;/* VOLATILE GLOBAL g_187 */
static volatile int16_t g_196 = 4L;/* VOLATILE GLOBAL g_196 */
static volatile int16_t g_197 = (-6L);/* VOLATILE GLOBAL g_197 */
static volatile int32_t g_198 = 0x3D635D9AL;/* VOLATILE GLOBAL g_198 */
static int16_t g_199 = (-1L);
static volatile int32_t g_200 = 1L;/* VOLATILE GLOBAL g_200 */
static volatile uint64_t g_201[5] = {0x04C5D88C53941594LL,0x04C5D88C53941594LL,0x04C5D88C53941594LL,0x04C5D88C53941594LL,0x04C5D88C53941594LL};
static int32_t * volatile g_209 = &g_91;/* VOLATILE GLOBAL g_209 */
static int32_t *g_211 = &g_4;
static int32_t ** volatile g_210[4] = {&g_211,&g_211,&g_211,&g_211};
static int32_t ** volatile g_212[8] = {&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211};
static volatile int16_t *g_218[7][10] = {{&g_197,&g_197,&g_196,&g_197,&g_197,&g_196,&g_197,&g_197,&g_196,&g_197},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_196,&g_197,&g_196,&g_196,&g_197},{&g_196,&g_197,&g_196,&g_196,&g_197,&g_196,&g_196,&g_197,&g_196,&g_196},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197},{&g_197,&g_196,&g_196,&g_197,&g_196,&g_196,&g_197,&g_196,&g_196,&g_197},{&g_196,&g_197,&g_196,&g_196,&g_197,&g_196,&g_196,&g_197,&g_196,&g_196},{&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197,&g_197}};
static volatile int16_t **g_217 = &g_218[5][8];
static const int64_t g_228 = 0xCCD6D71EDEFA84F3LL;
static struct S0 g_241 = {8UL};/* VOLATILE GLOBAL g_241 */
static struct S0 g_283 = {0x157AF347L};/* VOLATILE GLOBAL g_283 */
static int64_t *g_296 = (void*)0;
static uint8_t *g_350 = &g_183;
static int32_t * volatile g_352 = &g_132;/* VOLATILE GLOBAL g_352 */
static int32_t * const  volatile g_360 = &g_48;/* VOLATILE GLOBAL g_360 */
static struct S0 *g_396[1] = {&g_241};
static struct S0 ** const  volatile g_395 = &g_396[0];/* VOLATILE GLOBAL g_395 */
static uint8_t g_414[6] = {0x17L,0x97L,0x17L,0x17L,0x97L,0x17L};
static struct S0 g_420 = {0x3A11EBBAL};/* VOLATILE GLOBAL g_420 */
static int8_t g_449[9][7] = {{9L,0x74L,0xE2L,0x81L,0x81L,0xE2L,0x74L},{0L,0x97L,(-4L),0L,0L,(-4L),0x97L},{9L,0x74L,0xE2L,0x81L,0x81L,0xE2L,0x74L},{0L,0x97L,(-4L),0L,0L,(-4L),0x97L},{9L,0x74L,0xE2L,0x81L,0x81L,0xE2L,0x74L},{0L,0x97L,(-4L),0L,0L,(-4L),0x97L},{9L,0x74L,0xE2L,0x81L,0x81L,0xE2L,0x74L},{0L,0x97L,(-4L),0L,0L,(-4L),0x97L},{9L,0x74L,0xE2L,0x81L,0x81L,0xE2L,0x74L}};
static volatile struct S0 g_498[3][1][5] = {{{{0x9F95C795L},{0x9F95C795L},{0x9F95C795L},{0x9F95C795L},{0x9F95C795L}}},{{{0xF00F302BL},{0xF00F302BL},{0xF00F302BL},{0xF00F302BL},{0xF00F302BL}}},{{{0x9F95C795L},{0x9F95C795L},{0x9F95C795L},{0x9F95C795L},{0x9F95C795L}}}};
static volatile int32_t g_537[4] = {1L,1L,1L,1L};
static int64_t g_542 = 0x2379D7F501F7BDC5LL;
static struct S0 g_546 = {0x0138B086L};/* VOLATILE GLOBAL g_546 */
static struct S0 * volatile g_547[3][8][5] = {{{&g_283,&g_283,&g_283,(void*)0,&g_241},{&g_283,&g_142,&g_241,&g_546,&g_241},{&g_142,&g_546,&g_142,&g_142,&g_283},{&g_241,&g_142,&g_142,&g_241,&g_283},{&g_142,&g_283,&g_142,&g_241,&g_546},{&g_241,&g_142,&g_142,&g_142,&g_142},{&g_546,&g_142,&g_241,&g_241,&g_142},{&g_241,&g_546,&g_283,&g_241,&g_142}},{{&g_142,&g_283,&g_283,&g_142,&g_142},{&g_546,&g_142,&g_142,&g_241,&g_241},{&g_283,&g_142,&g_283,&g_142,&g_283},{(void*)0,&g_142,&g_546,&g_241,&g_283},{&g_283,&g_546,&g_241,&g_142,(void*)0},{&g_142,&g_283,&g_546,&g_283,&g_142},{&g_142,&g_241,&g_283,&g_283,&g_142},{&g_241,&g_142,&g_142,&g_142,&g_546}},{{&g_546,&g_283,&g_142,&g_241,&g_142},{&g_142,&g_142,&g_142,&g_142,&g_142},{&g_142,&g_142,&g_142,&g_241,(void*)0},{&g_546,&g_283,&g_546,&g_546,&g_283},{&g_241,&g_142,&g_142,&g_241,&g_283},{&g_142,&g_142,&g_142,&g_142,&g_241},{&g_142,&g_283,&g_546,&g_546,&g_142},{&g_283,&g_142,&g_142,&g_142,&g_142}}};
static struct S0 * volatile g_548 = &g_142;/* VOLATILE GLOBAL g_548 */
static struct S0 g_573 = {0xF6C1B85EL};/* VOLATILE GLOBAL g_573 */
static volatile int64_t g_583 = (-1L);/* VOLATILE GLOBAL g_583 */
static int16_t g_585 = 0x2F80L;
static struct S0 g_636[5] = {{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL},{4294967295UL}};
static struct S0 g_667 = {5UL};/* VOLATILE GLOBAL g_667 */
static struct S0 g_683 = {4294967295UL};/* VOLATILE GLOBAL g_683 */
static uint64_t g_685 = 0x903C8E3550220C45LL;
static int32_t g_702 = 0xDB87A606L;
static int32_t **g_723 = &g_211;
static int32_t g_748[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint16_t *g_768 = &g_61[1][0];
static struct S0 g_835 = {4294967291UL};/* VOLATILE GLOBAL g_835 */
static volatile struct S0 g_838 = {0x380603CEL};/* VOLATILE GLOBAL g_838 */
static uint8_t *g_878 = &g_414[5];
static volatile struct S0 g_895 = {0x210C8D5EL};/* VOLATILE GLOBAL g_895 */
static int64_t **g_919 = &g_296;
static uint16_t g_976[1][6][8] = {{{0xD74DL,0xD74DL,8UL,0xD74DL,0xD74DL,8UL,0xD74DL,0xD74DL},{4UL,0xD74DL,4UL,4UL,0xD74DL,4UL,4UL,0xD74DL},{0xD74DL,4UL,4UL,0xD74DL,4UL,4UL,0xD74DL,4UL},{0xD74DL,0xD74DL,8UL,0xD74DL,0xD74DL,8UL,0xD74DL,0xD74DL},{4UL,0xD74DL,4UL,4UL,0xD74DL,4UL,4UL,0xD74DL},{0xD74DL,4UL,4UL,0xD74DL,4UL,4UL,0xD74DL,4UL}}};
static struct S0 g_979 = {4UL};/* VOLATILE GLOBAL g_979 */
static int32_t ** volatile g_1013 = &g_211;/* VOLATILE GLOBAL g_1013 */
static volatile uint16_t * volatile * volatile *** volatile g_1047 = (void*)0;/* VOLATILE GLOBAL g_1047 */
static struct S0 * volatile g_1051 = (void*)0;/* VOLATILE GLOBAL g_1051 */
static struct S0 g_1053 = {0xBC7D83DFL};/* VOLATILE GLOBAL g_1053 */
static volatile uint32_t * volatile g_1120 = (void*)0;/* VOLATILE GLOBAL g_1120 */
static volatile uint32_t * volatile * const g_1119 = &g_1120;
static int16_t ***g_1136 = (void*)0;
static struct S0 g_1162 = {0xEB957599L};/* VOLATILE GLOBAL g_1162 */
static const uint8_t *g_1190 = &g_414[1];
static const uint8_t **g_1189 = &g_1190;
static volatile struct S0 g_1201 = {9UL};/* VOLATILE GLOBAL g_1201 */
static int32_t ** const  volatile g_1209 = &g_211;/* VOLATILE GLOBAL g_1209 */
static int32_t ** volatile g_1210 = &g_211;/* VOLATILE GLOBAL g_1210 */
static int32_t ** volatile g_1211[8] = {&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211};
static int32_t ** volatile g_1212 = &g_211;/* VOLATILE GLOBAL g_1212 */
static struct S0 g_1242 = {0x719BFF94L};/* VOLATILE GLOBAL g_1242 */
static struct S0 g_1257 = {0xAA5B38E4L};/* VOLATILE GLOBAL g_1257 */
static volatile uint16_t ** volatile * volatile g_1281[4][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
static volatile uint16_t ** volatile * volatile * const g_1280 = &g_1281[0][0];
static volatile uint16_t ** volatile * volatile * const * const g_1279 = &g_1280;
static struct S0 g_1285 = {0xD23B87ECL};/* VOLATILE GLOBAL g_1285 */
static struct S0 g_1315 = {0x4AAE5C15L};/* VOLATILE GLOBAL g_1315 */
static int32_t ** volatile g_1321[7] = {&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211};
static int32_t ** volatile g_1322 = &g_211;/* VOLATILE GLOBAL g_1322 */
static uint8_t *g_1338 = &g_414[2];
static int32_t g_1354 = (-6L);
static uint32_t g_1356[6][2][1] = {{{0x378BEE5FL},{0UL}},{{0x378BEE5FL},{1UL}},{{1UL},{0x378BEE5FL}},{{0UL},{0x378BEE5FL}},{{1UL},{1UL}},{{0x378BEE5FL},{0UL}}};
static const uint8_t *** volatile g_1363 = (void*)0;/* VOLATILE GLOBAL g_1363 */
static const uint8_t *** volatile g_1364 = &g_1189;/* VOLATILE GLOBAL g_1364 */
static uint8_t g_1396[9] = {0xD1L,0xAAL,0xD1L,0xAAL,0xD1L,0xAAL,0xD1L,0xAAL,0xD1L};
static uint8_t g_1398 = 1UL;
static struct S0 ** const g_1431 = (void*)0;
static struct S0 ** const  volatile *g_1430 = &g_1431;
static volatile struct S0 g_1432 = {0x21881973L};/* VOLATILE GLOBAL g_1432 */
static volatile struct S0 * volatile g_1433 = (void*)0;/* VOLATILE GLOBAL g_1433 */
static volatile struct S0 * const  volatile g_1434 = &g_179;/* VOLATILE GLOBAL g_1434 */
static volatile struct S0 g_1443 = {4294967286UL};/* VOLATILE GLOBAL g_1443 */
static volatile int64_t * volatile g_1474 = &g_583;/* VOLATILE GLOBAL g_1474 */
static volatile int64_t * volatile *g_1473 = &g_1474;
static volatile int64_t * volatile **g_1472 = &g_1473;
static volatile int64_t * volatile *** volatile g_1471[8] = {&g_1472,&g_1472,&g_1472,&g_1472,&g_1472,&g_1472,&g_1472,&g_1472};
static int32_t ** volatile g_1538 = &g_211;/* VOLATILE GLOBAL g_1538 */
static const uint16_t ****g_1555 = (void*)0;
static const uint16_t *****g_1554[3] = {&g_1555,&g_1555,&g_1555};
static const uint32_t g_1583 = 0x4DAAD0B4L;
static int32_t ** volatile g_1586 = &g_211;/* VOLATILE GLOBAL g_1586 */
static const volatile struct S0 g_1588 = {0x8347C3FCL};/* VOLATILE GLOBAL g_1588 */
static volatile struct S0 * volatile g_1589 = &g_1432;/* VOLATILE GLOBAL g_1589 */
static volatile struct S0 g_1617 = {0x3E53D8F1L};/* VOLATILE GLOBAL g_1617 */
static int8_t g_1642 = 0x7CL;
static volatile struct S0 g_1723 = {0xFD262A0EL};/* VOLATILE GLOBAL g_1723 */
static volatile struct S0 g_1747 = {0x2AA82F07L};/* VOLATILE GLOBAL g_1747 */
static uint16_t g_1755 = 0x5347L;
static volatile struct S0 g_1771[1] = {{0xEDA68F50L}};
static struct S0 g_1776 = {4294967295UL};/* VOLATILE GLOBAL g_1776 */
static struct S0 g_1777 = {0xCBFDBA7BL};/* VOLATILE GLOBAL g_1777 */
static volatile struct S0 g_1788[5] = {{0x8EC8EC26L},{0x8EC8EC26L},{0x8EC8EC26L},{0x8EC8EC26L},{0x8EC8EC26L}};
static int64_t g_1789 = 0xE0F5A9B411BE96B4LL;
static int32_t ** volatile g_1790 = &g_211;/* VOLATILE GLOBAL g_1790 */
static const struct S0 * volatile ** const  volatile * volatile g_1826 = (void*)0;/* VOLATILE GLOBAL g_1826 */
static const struct S0 * volatile ** const  volatile * volatile *g_1825 = &g_1826;
static volatile struct S0 g_1829 = {6UL};/* VOLATILE GLOBAL g_1829 */
static struct S0 g_1848 = {0x809FC08CL};/* VOLATILE GLOBAL g_1848 */
static struct S0 g_1877 = {1UL};/* VOLATILE GLOBAL g_1877 */
static struct S0 * const  volatile g_1878[8][1] = {{&g_636[4]},{&g_636[4]},{&g_636[4]},{&g_636[4]},{&g_636[4]},{&g_636[4]},{&g_636[4]},{&g_636[4]}};
static struct S0 g_1880 = {3UL};/* VOLATILE GLOBAL g_1880 */
static int32_t ** volatile g_1914 = &g_211;/* VOLATILE GLOBAL g_1914 */
static int32_t * volatile g_1952 = &g_4;/* VOLATILE GLOBAL g_1952 */
static struct S0 g_1983 = {0xD47CCEBEL};/* VOLATILE GLOBAL g_1983 */
static const int16_t *g_1996 = &g_585;
static const int16_t **g_1995 = &g_1996;
static const int16_t ***g_1994 = &g_1995;
static const int16_t *** const *g_1993 = &g_1994;
static const int16_t *** const *g_1998[8][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
static volatile struct S0 g_2014[5][1][3] = {{{{0xE65E56ACL},{0x4D3E934DL},{0xE65E56ACL}}},{{{4294967288UL},{0x657A52D4L},{0x6DE032B0L}}},{{{4294967288UL},{4294967288UL},{0x657A52D4L}}},{{{0xE65E56ACL},{0x657A52D4L},{0x657A52D4L}}},{{{0x657A52D4L},{0x4D3E934DL},{0x6DE032B0L}}}};
static const struct S0 g_2016[6][10] = {{{0xF873EBD0L},{3UL},{0x5E41A848L},{0UL},{0UL},{4294967294UL},{5UL},{1UL},{0xA0A96B00L},{1UL}},{{5UL},{0x26DA8CF4L},{3UL},{0x281E6B89L},{3UL},{0x26DA8CF4L},{5UL},{0xF873EBD0L},{4UL},{4294967295UL}},{{4294967288UL},{0x39017D8DL},{1UL},{4294967295UL},{4UL},{9UL},{0x5E41A848L},{2UL},{0x31A2ED38L},{0xF873EBD0L}},{{2UL},{0x39017D8DL},{5UL},{1UL},{4294967294UL},{2UL},{5UL},{4294967295UL},{4294967295UL},{5UL}},{{5UL},{0x26DA8CF4L},{9UL},{9UL},{0x26DA8CF4L},{5UL},{5UL},{0xF84756D9L},{0UL},{4294967288UL}},{{0x31A2ED38L},{3UL},{5UL},{4294967294UL},{9UL},{0xF873EBD0L},{0UL},{0xF86C4AE5L},{0x1531318BL},{2UL}}};
static struct S0 g_2019 = {0UL};/* VOLATILE GLOBAL g_2019 */
static int64_t g_2044 = 0x1C94D8D2A37B9BC8LL;
static uint32_t *g_2065[1][5] = {{&g_1356[2][0][0],&g_1356[2][0][0],&g_1356[2][0][0],&g_1356[2][0][0],&g_1356[2][0][0]}};
static uint8_t ****g_2069 = (void*)0;
static volatile struct S0 g_2085[9] = {{0UL},{0UL},{0xCC3919DCL},{0UL},{0UL},{0xCC3919DCL},{0UL},{0UL},{0xCC3919DCL}};
static volatile struct S0 g_2087 = {4294967287UL};/* VOLATILE GLOBAL g_2087 */
static volatile struct S0 g_2088[6][4] = {{{0x934E85DFL},{0x934E85DFL},{4294967287UL},{0x934E85DFL}},{{0x934E85DFL},{4294967295UL},{4294967295UL},{0x934E85DFL}},{{4294967295UL},{0x934E85DFL},{4294967295UL},{4294967295UL}},{{0x934E85DFL},{0x934E85DFL},{4294967287UL},{0x934E85DFL}},{{0x934E85DFL},{4294967295UL},{4294967295UL},{0x934E85DFL}},{{4294967295UL},{0x934E85DFL},{4294967295UL},{4294967295UL}}};
static int32_t g_2112[10] = {0x26598830L,0x26598830L,0x26598830L,0x26598830L,0x26598830L,0x26598830L,0x26598830L,0x26598830L,0x26598830L,0x26598830L};
static struct S0 g_2172 = {0x8271FC21L};/* VOLATILE GLOBAL g_2172 */
static uint32_t g_2175 = 18446744073709551609UL;
static struct S0 g_2187 = {0x081D119AL};/* VOLATILE GLOBAL g_2187 */
static volatile struct S0 * volatile g_2188 = &g_2088[0][1];/* VOLATILE GLOBAL g_2188 */
static const uint32_t g_2224 = 18446744073709551615UL;
static int32_t ** volatile g_2254 = &g_211;/* VOLATILE GLOBAL g_2254 */
static struct S0 g_2321 = {0x77BBB94FL};/* VOLATILE GLOBAL g_2321 */
static struct S0 g_2366 = {1UL};/* VOLATILE GLOBAL g_2366 */
static int32_t g_2396 = 0x608FB79CL;
static volatile uint8_t g_2414 = 0UL;/* VOLATILE GLOBAL g_2414 */
static struct S0 ** volatile g_2439[10][2] = {{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]},{&g_396[0],&g_396[0]}};
static volatile uint16_t g_2458 = 0xEE61L;/* VOLATILE GLOBAL g_2458 */
static volatile uint16_t * volatile g_2457 = &g_2458;/* VOLATILE GLOBAL g_2457 */
static volatile uint16_t * volatile * volatile g_2456[5][3] = {{&g_2457,&g_2457,&g_2457},{&g_2457,&g_2457,&g_2457},{&g_2457,&g_2457,&g_2457},{&g_2457,&g_2457,&g_2457},{&g_2457,&g_2457,&g_2457}};
static volatile uint16_t * volatile * volatile *g_2455 = &g_2456[4][1];
static volatile uint16_t * volatile * volatile **g_2454[10][2] = {{&g_2455,&g_2455},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_2455,&g_2455},{&g_2455,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_2455},{&g_2455,&g_2455},{(void*)0,&g_2455},{&g_2455,&g_2455}};
static const int64_t *g_2507 = &g_2044;
static const int64_t * const *g_2506 = &g_2507;
static const int64_t * const **g_2505 = &g_2506;
static const int64_t * const ** const *g_2504 = &g_2505;
static volatile uint8_t g_2510 = 0x3CL;/* VOLATILE GLOBAL g_2510 */
static struct S0 g_2520 = {0x53696E02L};/* VOLATILE GLOBAL g_2520 */
static volatile struct S0 g_2540 = {1UL};/* VOLATILE GLOBAL g_2540 */
static struct S0 g_2592[10][10][2] = {{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}},{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0UL},{0UL}}},{{{0UL},{0xC970C754L}},{{0xD0B5AA5CL},{0xCBDD7E79L}},{{0xC970C754L},{0xCBDD7E79L}},{{0xD0B5AA5CL},{0xC970C754L}},{{0xC970C754L},{0xC970C754L}},{{0xC970C754L},{0xD0B5AA5CL}},{{4294967295UL},{0UL}},{{0xD0B5AA5CL},{0UL}},{{4294967295UL},{0xD0B5AA5CL}},{{0xC970C754L},{0xC970C754L}}}};
static struct S0 * volatile g_2593 = (void*)0;/* VOLATILE GLOBAL g_2593 */
static struct S0 ** const *g_2647 = &g_1431;
static struct S0 ** const **g_2646 = &g_2647;
static struct S0 g_2675 = {0xF0B3B0FEL};/* VOLATILE GLOBAL g_2675 */
static struct S0 g_2680[6][6][6] = {{{{1UL},{4294967295UL},{0x5A45A9FDL},{4294967293UL},{0x97CB76F6L},{1UL}},{{9UL},{1UL},{4294967290UL},{0UL},{0UL},{1UL}},{{0x2A03B29EL},{0x058BA22EL},{0xDAC2C7B4L},{4294967295UL},{0xDAC667A1L},{0UL}},{{0x73A75D4EL},{0UL},{0x5C5504C1L},{0UL},{4294967295UL},{1UL}},{{4294967292UL},{0x9DE08AC1L},{0xF64A7FD8L},{1UL},{4294967295UL},{4294967290UL}},{{0xAD0B6BBAL},{0x1F444ABCL},{0x058BA22EL},{0x94EE7D0BL},{4294967295UL},{0x8BAD667DL}}},{{{0xCCCDEBB2L},{0x3477974FL},{4294967295UL},{1UL},{0UL},{1UL}},{{4294967290UL},{0UL},{4UL},{5UL},{0x4FE60449L},{4294967295UL}},{{2UL},{0x94EE7D0BL},{0UL},{5UL},{4294967288UL},{4294967295UL}},{{4294967295UL},{0xEFB8E5B1L},{0x4FE60449L},{1UL},{4294967295UL},{2UL}},{{4UL},{1UL},{0x45FBA784L},{4294967288UL},{0xC1D5FEB8L},{0UL}},{{4294967295UL},{0UL},{0UL},{0x4FE60449L},{1UL},{0x4FE60449L}}},{{{0x4EA40F44L},{0x3900B72BL},{0x4EA40F44L},{0xEFB8E5B1L},{0x1F444ABCL},{9UL}},{{4294967295UL},{0x5C5504C1L},{4294967295UL},{0x058BA22EL},{0UL},{4294967292UL}},{{0x94EE7D0BL},{0x2A03B29EL},{0xB2A7DBF5L},{0x058BA22EL},{4294967295UL},{0xEFB8E5B1L}},{{4294967295UL},{4294967295UL},{1UL},{0xEFB8E5B1L},{4294967295UL},{0xAD0B6BBAL}},{{0x4EA40F44L},{0xA78B0151L},{4294967295UL},{0x4FE60449L},{9UL},{1UL}},{{4294967295UL},{0xF64A7FD8L},{0x97CB76F6L},{4294967288UL},{0x5C5504C1L},{0x9DE08AC1L}}},{{{4UL},{4294967295UL},{3UL},{1UL},{0xCCCDEBB2L},{1UL}},{{4294967295UL},{0xDAC667A1L},{0x94EE7D0BL},{5UL},{0xAD2A0A1EL},{4294967295UL}},{{2UL},{0x4EA40F44L},{4294967288UL},{5UL},{0x3477974FL},{0xF64A7FD8L}},{{4294967290UL},{5UL},{0x2A03B29EL},{1UL},{1UL},{4294967295UL}},{{0xCCCDEBB2L},{1UL},{4294967288UL},{0x94EE7D0BL},{0UL},{1UL}},{{0xAD0B6BBAL},{4294967295UL},{4294967288UL},{1UL},{0x058BA22EL},{0x5C5504C1L}}},{{{4294967292UL},{0x5A45A9FDL},{0xEFB8E5B1L},{0UL},{2UL},{0x058BA22EL}},{{0x73A75D4EL},{4294967295UL},{4294967292UL},{4294967295UL},{0xA78B0151L},{0x5A45A9FDL}},{{0x2A03B29EL},{4294967295UL},{4294967295UL},{0UL},{0UL},{4294967295UL}},{{9UL},{9UL},{4294967295UL},{4294967293UL},{4294967295UL},{0xA763E715L}},{{1UL},{4294967292UL},{0x3900B72BL},{4294967290UL},{3UL},{4294967295UL}},{{5UL},{1UL},{0x3900B72BL},{9UL},{9UL},{0x4FE60449L}}},{{{0x5A45A9FDL},{4294967292UL},{0UL},{4294967290UL},{0x45FBA784L},{0x5C5504C1L}},{{4294967290UL},{0x45FBA784L},{0x5C5504C1L},{0UL},{4294967290UL},{0xB2A7DBF5L}},{{0xDAC2C7B4L},{4294967295UL},{0x73A75D4EL},{0UL},{0x5C5504C1L},{0UL}},{{0x5C5504C1L},{4294967287UL},{1UL},{3UL},{4294967295UL},{5UL}},{{0xF64A7FD8L},{4294967295UL},{4294967295UL},{4294967295UL},{0x28507D9AL},{0UL}},{{0x058BA22EL},{9UL},{3UL},{0x2A03B29EL},{0UL},{4294967288UL}}}};
static struct S0 * const g_2679 = &g_2680[1][2][5];
static struct S0 * const *g_2678 = &g_2679;
static struct S0 * const **g_2677[3][10] = {{&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678},{&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678},{&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678,&g_2678}};
static struct S0 * const ***g_2676 = &g_2677[0][9];
static struct S0 g_2681 = {1UL};/* VOLATILE GLOBAL g_2681 */
static int32_t *g_2683 = &g_91;
static int32_t ** volatile g_2682[8] = {&g_2683,&g_2683,&g_2683,&g_2683,&g_2683,&g_2683,&g_2683,&g_2683};
static struct S0 g_2692 = {0x2B502ABDL};/* VOLATILE GLOBAL g_2692 */
static volatile struct S0 g_2704[8][3][9] = {{{{4294967289UL},{0xC54D09E0L},{0UL},{0UL},{4294967295UL},{4294967289UL},{0UL},{0x77CB0A0FL},{0x77CB0A0FL}},{{0x6F8ED4CEL},{4294967295UL},{0x72003204L},{0UL},{0x72003204L},{4294967295UL},{0x6F8ED4CEL},{4294967295UL},{0UL}},{{0x19F2878EL},{4294967295UL},{1UL},{4294967291UL},{4294967287UL},{0x83C76BE2L},{4294967295UL},{4294967292UL},{4294967289UL}}},{{{4294967292UL},{0UL},{0xEC21474BL},{0xF7CBCD23L},{0x8ED956E7L},{0xC0205D17L},{4294967295UL},{4294967295UL},{0xC0205D17L}},{{0x50C3F117L},{0UL},{0x77CB0A0FL},{0UL},{0x50C3F117L},{0x094BE114L},{7UL},{0x77CB0A0FL},{4294967291UL}},{{0x11C388CCL},{0xA24C56DEL},{0x862CCEC3L},{0x18FD47A4L},{4294967295UL},{0x11C388CCL},{0x8ED956E7L},{0UL},{0UL}}},{{{1UL},{0x50C3F117L},{4294967287UL},{0UL},{4294967291UL},{0x094BE114L},{0x094BE114L},{4294967291UL},{0UL}},{{0xF7CBCD23L},{4294967287UL},{0xF7CBCD23L},{0x11C388CCL},{0xEC21474BL},{0xC0205D17L},{4294967287UL},{5UL},{0x72003204L}},{{0UL},{4294967289UL},{7UL},{1UL},{0xC54D09E0L},{0x83C76BE2L},{0UL},{4294967287UL},{0x19F2878EL}}},{{{1UL},{4294967295UL},{0UL},{0x11C388CCL},{4294967295UL},{4294967295UL},{0x11C388CCL},{0UL},{4294967295UL}},{{4294967289UL},{0x5C94974DL},{0xE9CBFC77L},{0UL},{4294967295UL},{4294967289UL},{0xC54D09E0L},{0UL},{0UL}},{{0xE807B865L},{4294967295UL},{4294967295UL},{0x18FD47A4L},{0UL},{0xF0DABAFAL},{0x6F8ED4CEL},{0x11C388CCL},{0x18FD47A4L}}},{{{4294967292UL},{0x5C94974DL},{1UL},{0UL},{0UL},{1UL},{0x5C94974DL},{0UL},{0UL}},{{0UL},{0xC0205D17L},{0x386D8614L},{0x18FD47A4L},{0xF0DABAFAL},{0UL},{5UL},{0xF7CBCD23L},{0x862CCEC3L}},{{4294967287UL},{0x83C76BE2L},{4294967295UL},{4294967292UL},{4294967289UL},{0UL},{0UL},{4294967295UL},{0UL}}},{{{4294967292UL},{4294967295UL},{4294967287UL},{4294967287UL},{4294967295UL},{4294967292UL},{0xF0DABAFAL},{0x8ED956E7L},{0xE1686B45L}},{{0UL},{4294967289UL},{4294967292UL},{4294967295UL},{0x83C76BE2L},{4294967287UL},{4294967291UL},{1UL},{4294967295UL}},{{0UL},{0xF0DABAFAL},{0x18FD47A4L},{0x386D8614L},{0xC0205D17L},{0UL},{0xF0DABAFAL},{0UL},{0xC0205D17L}}},{{{0UL},{1UL},{1UL},{0UL},{0x094BE114L},{0UL},{0UL},{0x83C76BE2L},{0UL}},{{4294967295UL},{0x386D8614L},{0xE1686B45L},{0xC0205D17L},{0x11C388CCL},{0UL},{5UL},{0xE1686B45L},{0xF7CBCD23L}},{{0x83C76BE2L},{4294967295UL},{0xC54D09E0L},{4294967295UL},{0x094BE114L},{0x83C76BE2L},{0x094BE114L},{4294967295UL},{0xC54D09E0L}}},{{{0x11C388CCL},{0x11C388CCL},{4294967292UL},{0xA24C56DEL},{0xC0205D17L},{0x72003204L},{0xEC21474BL},{0x386D8614L},{0xA24C56DEL}},{{0xE9CBFC77L},{0UL},{0UL},{0x19F2878EL},{0x83C76BE2L},{0x77CB0A0FL},{0UL},{0UL},{4294967292UL}},{{0x862CCEC3L},{5UL},{4294967292UL},{0x18FD47A4L},{4294967295UL},{0x18FD47A4L},{4294967292UL},{5UL},{0x862CCEC3L}}}};
static volatile uint64_t * volatile *g_2707 = (void*)0;
static int16_t *g_2715[10] = {&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199,&g_199};
static int16_t **g_2714 = &g_2715[8];
static const uint8_t g_2732[5][6] = {{0x61L,0xDDL,0x6AL,0xD0L,0x6AL,0xDDL},{0x61L,0xDDL,0x6AL,0xD0L,0x6AL,0xDDL},{0x61L,0xDDL,0x6AL,0xD0L,0x6AL,0xDDL},{0x61L,0xDDL,0x6AL,0xD0L,0x6AL,0xDDL},{0x61L,0xDDL,0x6AL,0xD0L,0x6AL,0xDDL}};
static int64_t g_2753 = 0xAF5CD88ABFC873EBLL;
static volatile int8_t g_2803[3] = {0x72L,0x72L,0x72L};
static int32_t *g_2825 = &g_748[5];
static struct S0 g_2851 = {0UL};/* VOLATILE GLOBAL g_2851 */
static const struct S0 *g_2859 = (void*)0;
static const struct S0 **g_2858 = &g_2859;
static const struct S0 ***g_2857 = &g_2858;
static const struct S0 ****g_2856[5][7] = {{(void*)0,&g_2857,(void*)0,&g_2857,(void*)0,&g_2857,(void*)0},{(void*)0,&g_2857,(void*)0,&g_2857,(void*)0,&g_2857,(void*)0},{(void*)0,&g_2857,(void*)0,&g_2857,(void*)0,&g_2857,(void*)0},{(void*)0,&g_2857,(void*)0,&g_2857,(void*)0,&g_2857,(void*)0},{(void*)0,&g_2857,(void*)0,&g_2857,(void*)0,&g_2857,(void*)0}};
static const int32_t *** volatile g_2875 = (void*)0;/* VOLATILE GLOBAL g_2875 */
static struct S0 g_2897 = {4294967295UL};/* VOLATILE GLOBAL g_2897 */
static struct S0 ***g_2916[9][4][7] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
static int16_t g_3003 = 0xABF4L;
static struct S0 g_3004 = {0x4EDCC579L};/* VOLATILE GLOBAL g_3004 */


/* --- FORWARD DECLARATIONS --- */
static struct S0  func_1(void);
static int32_t * const  func_7(const int32_t  p_8, const int32_t  p_9, uint64_t  p_10);
static int32_t  func_11(int32_t * p_12, uint32_t  p_13);
static int32_t * func_14(int32_t * p_15, uint64_t  p_16, int32_t * p_17, int32_t * p_18);
static int32_t * func_19(int8_t  p_20);
static int32_t * func_29(int64_t  p_30, int32_t * p_31, int32_t * p_32, uint8_t  p_33);
static int8_t  func_34(int16_t  p_35);
static int64_t  func_68(int32_t ** p_69, uint16_t  p_70, int32_t ** p_71);
static int32_t ** func_72(uint32_t  p_73, int32_t * p_74);
static int32_t  func_86(int32_t * p_87, int32_t  p_88);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_4 g_6 g_175 g_2366 g_91 g_352 g_135 g_723 g_1586 g_1013 g_211 g_1538 g_1356 g_2065 g_158 g_1398 g_350 g_183 g_878 g_414 g_2188 g_2088 g_3004
 * writes: g_4 g_6 g_175 g_91 g_132 g_135 g_211 g_1356 g_158 g_723 g_414
 */
static struct S0  func_1(void)
{ /* block id: 0 */
    int32_t *l_44 = &g_6;
    uint64_t l_50 = 0x1B09C821F997EC53LL;
    int32_t *l_736 = &g_91;
    int8_t l_2146[10] = {0xE6L,(-1L),0xE6L,(-1L),0xE6L,(-1L),0xE6L,(-1L),0xE6L,(-1L)};
    int32_t l_2149 = 8L;
    int32_t l_2153[4] = {(-9L),(-9L),(-9L),(-9L)};
    int8_t l_2155 = 0x88L;
    struct S0 ** const ***l_2219 = (void*)0;
    int16_t *** const *l_2220 = &g_1136;
    int16_t l_2223 = (-7L);
    const uint16_t *l_2232 = &g_976[0][0][0];
    const uint16_t * const *l_2231[9][5] = {{&l_2232,&l_2232,&l_2232,&l_2232,&l_2232},{(void*)0,&l_2232,(void*)0,&l_2232,(void*)0},{&l_2232,(void*)0,(void*)0,(void*)0,&l_2232},{(void*)0,(void*)0,(void*)0,(void*)0,&l_2232},{&l_2232,&l_2232,(void*)0,&l_2232,&l_2232},{&l_2232,&l_2232,&l_2232,(void*)0,&l_2232},{(void*)0,&l_2232,&l_2232,(void*)0,&l_2232},{&l_2232,&l_2232,&l_2232,&l_2232,(void*)0},{(void*)0,&l_2232,&l_2232,&l_2232,&l_2232}};
    const uint16_t * const **l_2230 = &l_2231[3][3];
    const uint16_t * const ***l_2229[2];
    const uint16_t * const ****l_2228 = &l_2229[1];
    uint8_t **l_2316 = (void*)0;
    const int16_t l_2320 = 4L;
    uint64_t l_2362 = 18446744073709551615UL;
    int64_t ***l_2410 = &g_919;
    uint32_t l_2443 = 0xD0FA5166L;
    uint64_t *l_2706 = &l_50;
    uint64_t **l_2705 = &l_2706;
    int8_t l_2720 = 0x95L;
    const uint8_t *l_2731 = &g_2732[4][5];
    uint32_t l_2852 = 0xE8E13BC2L;
    const uint64_t l_2854 = 0UL;
    const int32_t *l_2874 = &g_2396;
    const int32_t **l_2873[3][5][5] = {{{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874}},{{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874}},{{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874},{&l_2874,&l_2874,&l_2874,&l_2874,&l_2874}}};
    int32_t l_2877 = 0xAFFA7EBEL;
    uint16_t l_2887 = 65535UL;
    uint8_t ****l_2923 = (void*)0;
    int32_t l_2958 = (-1L);
    int64_t ****l_2965 = &l_2410;
    int64_t *****l_2964 = &l_2965;
    int64_t l_2985 = 0L;
    uint16_t ***l_2987[8];
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2229[i] = &l_2230;
    for (i = 0; i < 8; i++)
        l_2987[i] = &g_188;
    if (g_2)
    { /* block id: 1 */
        int32_t *l_3 = &g_4;
        int32_t *l_5 = &g_6;
        int8_t *l_40 = &g_41;
        int8_t *l_42 = &g_43;
        int32_t **l_49 = &l_44;
        int16_t l_752 = 0xC22AL;
        int32_t **l_2136 = &g_211;
        int32_t l_2145 = 0L;
        int8_t l_2147 = 0x2EL;
        int8_t l_2150 = 0L;
        int32_t l_2151 = 0L;
        int32_t l_2152 = (-9L);
        int32_t l_2154 = 0x64D0D7B9L;
        uint16_t l_2157 = 2UL;
        int16_t ****l_2280 = &g_1136;
        int16_t *****l_2279 = &l_2280;
        uint32_t l_2282 = 0xFCFD1277L;
        uint64_t l_2293 = 0xF924CDE81FA8B1ABLL;
        uint16_t * const l_2317[9] = {&g_154,&g_1755,&g_154,&g_1755,&g_154,&g_1755,&g_154,&g_1755,&g_154};
        struct S0 *** const l_2319[1][5][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
        int i, j, k;
        (*l_5) ^= ((*l_3) &= g_2);
    }
    else
    { /* block id: 1028 */
        uint32_t l_2367 = 0xD366875AL;
        int32_t l_2374[1];
        int16_t l_2379 = 0x746CL;
        int64_t l_2382 = 0L;
        uint32_t l_2383[9] = {0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL,0xC40FFE2DL};
        int64_t l_2423 = 0x0A9E56BE2B6EC240LL;
        const uint8_t ***l_2475[10] = {&g_1189,&g_1189,&g_1189,&g_1189,&g_1189,&g_1189,&g_1189,&g_1189,&g_1189,&g_1189};
        const uint8_t ****l_2474[8] = {&l_2475[7],&l_2475[7],&l_2475[7],&l_2475[7],&l_2475[7],&l_2475[7],&l_2475[7],&l_2475[7]};
        const uint8_t *****l_2473[7][6][6] = {{{&l_2474[2],&l_2474[3],&l_2474[5],&l_2474[3],&l_2474[3],&l_2474[5]},{&l_2474[3],(void*)0,(void*)0,&l_2474[7],(void*)0,&l_2474[3]},{&l_2474[3],(void*)0,&l_2474[1],&l_2474[3],&l_2474[3],&l_2474[1]},{&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3]},{&l_2474[2],&l_2474[3],&l_2474[1],&l_2474[3],&l_2474[1],&l_2474[3]},{(void*)0,&l_2474[2],&l_2474[1],&l_2474[3],&l_2474[3],&l_2474[3]}},{{(void*)0,&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[1]},{&l_2474[3],&l_2474[3],&l_2474[1],&l_2474[1],&l_2474[2],&l_2474[3]},{&l_2474[3],&l_2474[0],(void*)0,&l_2474[1],&l_2474[3],&l_2474[5]},{&l_2474[3],&l_2474[7],&l_2474[5],&l_2474[3],&l_2474[2],&l_2474[3]},{(void*)0,&l_2474[3],&l_2474[3],&l_2474[3],(void*)0,&l_2474[1]},{(void*)0,&l_2474[1],&l_2474[2],&l_2474[3],(void*)0,&l_2474[7]}},{{&l_2474[2],&l_2474[3],&l_2474[5],&l_2474[3],&l_2474[2],&l_2474[3]},{&l_2474[3],&l_2474[7],(void*)0,&l_2474[3],&l_2474[3],&l_2474[3]},{&l_2474[3],&l_2474[0],&l_2474[3],&l_2474[7],&l_2474[2],&l_2474[3]},{&l_2474[3],&l_2474[3],(void*)0,&l_2474[3],&l_2474[3],&l_2474[3]},{&l_2474[2],&l_2474[3],&l_2474[5],(void*)0,&l_2474[3],&l_2474[7]},{&l_2474[3],&l_2474[2],&l_2474[2],&l_2474[3],&l_2474[1],&l_2474[1]}},{{&l_2474[3],&l_2474[3],&l_2474[3],(void*)0,&l_2474[3],&l_2474[3]},{&l_2474[2],&l_2474[3],&l_2474[5],&l_2474[3],&l_2474[3],&l_2474[5]},{&l_2474[3],(void*)0,(void*)0,&l_2474[7],(void*)0,&l_2474[3]},{&l_2474[3],(void*)0,&l_2474[1],&l_2474[3],&l_2474[3],&l_2474[1]},{&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3]},{&l_2474[2],&l_2474[3],&l_2474[1],&l_2474[3],&l_2474[1],&l_2474[3]}},{{(void*)0,&l_2474[2],&l_2474[1],&l_2474[3],&l_2474[3],&l_2474[3]},{(void*)0,&l_2474[3],&l_2474[2],(void*)0,&l_2474[3],&l_2474[3]},{(void*)0,&l_2474[3],&l_2474[3],&l_2474[7],(void*)0,&l_2474[2]},{(void*)0,&l_2474[4],&l_2474[3],&l_2474[7],(void*)0,(void*)0},{(void*)0,&l_2474[0],&l_2474[3],(void*)0,&l_2474[3],&l_2474[3]},{&l_2474[3],(void*)0,&l_2474[2],&l_2474[6],&l_2474[6],&l_2474[3]}},{{&l_2474[1],&l_2474[7],(void*)0,(void*)0,&l_2474[6],&l_2474[1]},{&l_2474[3],(void*)0,(void*)0,(void*)0,&l_2474[3],(void*)0},{&l_2474[3],&l_2474[0],&l_2474[3],&l_2474[5],(void*)0,&l_2474[3]},{&l_2474[3],&l_2474[4],&l_2474[3],&l_2474[0],(void*)0,&l_2474[3]},{(void*)0,&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[3],(void*)0},{(void*)0,&l_2474[6],(void*)0,&l_2474[6],&l_2474[3],&l_2474[1]}},{{&l_2474[6],&l_2474[3],(void*)0,&l_2474[5],&l_2474[7],&l_2474[3]},{&l_2474[6],&l_2474[5],&l_2474[2],&l_2474[6],&l_2474[5],&l_2474[3]},{(void*)0,&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[5],(void*)0},{(void*)0,&l_2474[3],&l_2474[3],&l_2474[0],&l_2474[1],&l_2474[2]},{&l_2474[3],&l_2474[3],&l_2474[3],&l_2474[5],&l_2474[5],&l_2474[3]},{&l_2474[3],&l_2474[3],&l_2474[2],(void*)0,&l_2474[5],&l_2474[0]}}};
        int32_t l_2511[5][1][5] = {{{0xFD9125BEL,1L,0xFD9125BEL,1L,0xFD9125BEL}},{{0xB99485C8L,0xB99485C8L,0xB99485C8L,0xB99485C8L,0xB99485C8L}},{{0xFD9125BEL,1L,0xFD9125BEL,1L,0xFD9125BEL}},{{0xB99485C8L,0xB99485C8L,0xB99485C8L,0xB99485C8L,0xB99485C8L}},{{0xFD9125BEL,1L,0xFD9125BEL,1L,0xFD9125BEL}}};
        uint8_t l_2512 = 0x34L;
        uint16_t *l_2530 = &g_61[1][0];
        int16_t l_2557 = 0x8404L;
        int64_t l_2606 = 0x5AC24F387BF19889LL;
        uint8_t l_2652 = 1UL;
        int32_t l_2655 = 0x161C9ACDL;
        int64_t * const *l_2686 = (void*)0;
        int16_t **l_2719[2];
        int16_t **l_2752[7][10][3] = {{{&g_2715[8],(void*)0,&g_2715[2]},{&g_2715[8],&g_2715[2],&g_2715[8]},{&g_2715[8],(void*)0,&g_2715[0]},{&g_2715[1],&g_2715[8],&g_2715[8]},{&g_2715[8],&g_2715[2],&g_2715[8]},{&g_2715[9],&g_2715[3],&g_2715[6]},{&g_2715[8],&g_2715[3],(void*)0},{(void*)0,&g_2715[2],&g_2715[6]},{&g_2715[5],&g_2715[8],&g_2715[6]},{&g_2715[1],(void*)0,&g_2715[8]}},{{(void*)0,&g_2715[2],&g_2715[7]},{&g_2715[8],(void*)0,&g_2715[8]},{(void*)0,&g_2715[8],&g_2715[0]},{&g_2715[8],&g_2715[2],&g_2715[5]},{(void*)0,&g_2715[3],&g_2715[5]},{&g_2715[4],&g_2715[3],&g_2715[3]},{&g_2715[7],&g_2715[2],&g_2715[8]},{(void*)0,&g_2715[8],&g_2715[3]},{&g_2715[8],(void*)0,&g_2715[1]},{&g_2715[8],&g_2715[2],&g_2715[8]}},{{&g_2715[8],(void*)0,&g_2715[7]},{&g_2715[4],&g_2715[8],(void*)0},{&g_2715[6],&g_2715[8],&g_2715[5]},{&g_2715[7],&g_2715[8],&g_2715[6]},{&g_2715[8],&g_2715[8],(void*)0},{&g_2715[5],&g_2715[8],&g_2715[8]},{&g_2715[8],(void*)0,&g_2715[8]},{&g_2715[8],&g_2715[8],&g_2715[8]},{&g_2715[7],&g_2715[8],&g_2715[9]},{(void*)0,&g_2715[8],(void*)0}},{{&g_2715[3],(void*)0,(void*)0},{&g_2715[9],&g_2715[8],&g_2715[1]},{&g_2715[8],&g_2715[8],&g_2715[8]},{(void*)0,&g_2715[8],&g_2715[8]},{&g_2715[6],&g_2715[8],&g_2715[8]},{&g_2715[6],(void*)0,&g_2715[8]},{&g_2715[2],&g_2715[8],&g_2715[8]},{&g_2715[8],&g_2715[8],&g_2715[8]},{&g_2715[5],&g_2715[8],&g_2715[7]},{(void*)0,(void*)0,&g_2715[9]}},{{&g_2715[3],&g_2715[8],(void*)0},{&g_2715[8],&g_2715[8],&g_2715[8]},{&g_2715[0],&g_2715[8],&g_2715[8]},{&g_2715[3],&g_2715[8],&g_2715[4]},{&g_2715[8],(void*)0,&g_2715[8]},{&g_2715[1],&g_2715[8],&g_2715[8]},{&g_2715[0],&g_2715[8],&g_2715[5]},{&g_2715[8],&g_2715[8],(void*)0},{&g_2715[8],(void*)0,&g_2715[8]},{&g_2715[6],&g_2715[8],&g_2715[5]}},{{&g_2715[7],&g_2715[8],&g_2715[6]},{&g_2715[8],&g_2715[8],(void*)0},{&g_2715[5],&g_2715[8],&g_2715[8]},{&g_2715[8],(void*)0,&g_2715[8]},{&g_2715[8],&g_2715[8],&g_2715[8]},{&g_2715[7],&g_2715[8],&g_2715[9]},{(void*)0,&g_2715[8],(void*)0},{&g_2715[3],(void*)0,(void*)0},{&g_2715[9],&g_2715[8],&g_2715[1]},{&g_2715[8],&g_2715[8],&g_2715[8]}},{{(void*)0,&g_2715[8],&g_2715[8]},{&g_2715[6],&g_2715[8],&g_2715[8]},{&g_2715[6],(void*)0,&g_2715[8]},{&g_2715[2],&g_2715[8],&g_2715[8]},{&g_2715[8],&g_2715[8],&g_2715[8]},{&g_2715[5],&g_2715[8],&g_2715[7]},{(void*)0,(void*)0,&g_2715[9]},{&g_2715[3],&g_2715[8],(void*)0},{&g_2715[8],&g_2715[8],&g_2715[8]},{&g_2715[0],&g_2715[8],&g_2715[8]}}};
        uint16_t l_2758 = 65526UL;
        uint8_t l_2779 = 0x2BL;
        int32_t l_2829 = (-3L);
        struct S0 *l_2871 = (void*)0;
        int32_t l_2886[1][2][4];
        struct S0 **l_2915[7];
        struct S0 ***l_2914 = &l_2915[2];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2374[i] = 1L;
        for (i = 0; i < 2; i++)
            l_2719[i] = &g_2715[8];
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 4; k++)
                    l_2886[i][j][k] = 0xC66114BFL;
            }
        }
        for (i = 0; i < 7; i++)
            l_2915[i] = &g_396[0];
        for (g_175 = 0; (g_175 <= 2); g_175 += 1)
        { /* block id: 1031 */
            return g_2366;
        }
        (*g_352) = ((*l_44) |= ((*l_736) ^= l_2367));
        for (g_175 = 0; (g_175 < 41); g_175++)
        { /* block id: 1039 */
            int32_t l_2370 = 0L;
            int32_t l_2378[5][4] = {{0x2BD1E648L,0x2BD1E648L,0x2BD1E648L,0x2BD1E648L},{0x2BD1E648L,0x2BD1E648L,0x2BD1E648L,0x2BD1E648L},{0x2BD1E648L,0x2BD1E648L,0x2BD1E648L,0x2BD1E648L},{0x2BD1E648L,0x2BD1E648L,0x2BD1E648L,0x2BD1E648L},{0x2BD1E648L,0x2BD1E648L,0x2BD1E648L,0x2BD1E648L}};
            int8_t l_2380 = 0x70L;
            int32_t l_2381 = 0x6AD792F1L;
            int32_t l_2401 = 0x37F401B0L;
            int i, j;
            if (l_2370)
            { /* block id: 1040 */
                int32_t l_2373 = 0xF1A10D38L;
                int32_t *l_2375 = &g_2112[4];
                int32_t *l_2376 = &l_2374[0];
                int32_t *l_2377[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_2377[i] = &g_6;
                for (g_135 = 0; (g_135 != (-18)); --g_135)
                { /* block id: 1043 */
                    (*g_723) = &l_2153[1];
                    (*g_1586) = (void*)0;
                }
                --l_2383[3];
                (*g_723) = (*g_1013);
                if (l_2378[2][2])
                    break;
            }
            else
            { /* block id: 1051 */
                int8_t l_2400[7][2] = {{0x16L,0x16L},{0x7FL,0x16L},{0x16L,0x7FL},{0x16L,0x16L},{0x7FL,0x16L},{0x16L,0x7FL},{0x16L,0x16L}};
                int i, j;
                for (l_50 = 0; (l_50 <= 0); l_50 += 1)
                { /* block id: 1054 */
                    int16_t *l_2395 = &l_2223;
                    int16_t **l_2394 = &l_2395;
                    int16_t ***l_2393 = &l_2394;
                    int32_t l_2399 = 0x1303902CL;
                    (*g_211) = (**g_1538);
                    for (l_2382 = 0; (l_2382 >= 0); l_2382 -= 1)
                    { /* block id: 1058 */
                        int i, j;
                        (*g_723) = g_2065[l_50][l_50];
                    }
                    for (g_158 = 0; (g_158 <= 0); g_158 += 1)
                    { /* block id: 1063 */
                        int32_t l_2386 = 0x5E620AE3L;
                        (*l_736) = ((*g_211) |= (l_2386 ^ (safe_sub_func_int8_t_s_s((((safe_add_func_int8_t_s_s(((safe_lshift_func_int16_t_s_u(0x1C18L, (((g_723 = &g_211) == &g_360) >= (l_2374[0] = ((void*)0 != l_2393))))) , g_1398), (*g_350))) == ((((*g_878)++) && (*l_736)) & l_2399)) >= l_2400[5][0]), l_2401))));
                    }
                }
            }
            return (*g_2188);
        }
        for (l_2382 = 0; (l_2382 <= 7); l_2382 += 1)
        { /* block id: 1076 */
            uint32_t l_2407 = 0xB0B74626L;
            uint8_t *l_2408 = &g_414[1];
            int32_t l_2419 = 1L;
            int64_t *l_2420 = &g_542;
            int32_t l_2483 = 0x2300B631L;
            struct S0 *l_2499 = &g_636[1];
            uint8_t l_2513 = 1UL;
            int32_t **l_2519 = &l_736;
            uint16_t *l_2529 = &g_154;
            uint16_t ***l_2541 = &g_188;
            uint16_t **l_2543 = &l_2529;
            uint16_t ***l_2542 = &l_2543;
            uint16_t **l_2545[3];
            uint16_t ***l_2544 = &l_2545[0];
            uint64_t *l_2546 = &l_50;
            uint64_t *l_2547 = &g_685;
            int32_t l_2556[3][1][3] = {{{0x8EBF8EC9L,0xCADDF3CDL,0x8EBF8EC9L}},{{0x8EBF8EC9L,0xCADDF3CDL,0x8EBF8EC9L}},{{0x8EBF8EC9L,0xCADDF3CDL,0x8EBF8EC9L}}};
            int64_t ***l_2618 = (void*)0;
            int16_t l_2619 = 0x3810L;
            int8_t l_2620 = 0xC7L;
            uint16_t ****l_2633[9][7] = {{(void*)0,&l_2541,(void*)0,(void*)0,&l_2541,&l_2541,(void*)0},{&l_2544,(void*)0,&l_2544,&l_2541,&l_2541,&l_2544,(void*)0},{(void*)0,(void*)0,&l_2541,&l_2541,(void*)0,(void*)0,(void*)0},{&l_2544,&l_2541,&l_2541,&l_2544,(void*)0,&l_2544,&l_2541},{&l_2541,&l_2541,(void*)0,&l_2541,(void*)0,&l_2541,&l_2541},{&l_2542,&l_2541,&l_2541,&l_2541,&l_2542,&l_2542,&l_2541},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2541,(void*)0,&l_2541,&l_2541,(void*)0,&l_2541,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
            uint16_t *****l_2632 = &l_2633[4][0];
            struct S0 ** const **l_2649 = (void*)0;
            struct S0 ***l_2651 = (void*)0;
            struct S0 ****l_2650[4] = {&l_2651,&l_2651,&l_2651,&l_2651};
            int32_t l_2770[6][7] = {{2L,0x62763AF5L,(-1L),0x0EAA1C20L,(-1L),0x62763AF5L,2L},{0x62763AF5L,0x4A35671BL,(-1L),0x25822FE2L,0x05826A11L,(-1L),0x05826A11L},{0x62763AF5L,0x05826A11L,0x05826A11L,0x62763AF5L,0x594FE416L,0x19FFAC41L,0x0EAA1C20L},{2L,0x19FFAC41L,(-1L),0x594FE416L,0x594FE416L,(-1L),0x19FFAC41L},{0x594FE416L,2L,(-1L),0x4A35671BL,0x05826A11L,0x0EAA1C20L,0x0EAA1C20L},{(-1L),2L,0x594FE416L,2L,(-1L),0x4A35671BL,0x05826A11L}};
            int16_t l_2811 = (-1L);
            uint32_t l_2853 = 0x420665E1L;
            int64_t l_2893[2][10][4] = {{{0L,0L,(-1L),0L},{0L,0xDF5F9EABAC11EBC7LL,0xDF5F9EABAC11EBC7LL,0L},{0xD63C4D2870161610LL,0L,0L,0x52B47B027DC054A8LL},{0x3DF8AB15005DFCC7LL,0L,1L,(-1L)},{(-4L),0x9AA7848E98236AF1LL,0x5F96EFB3321356F6LL,(-1L)},{0L,0L,(-1L),0x52B47B027DC054A8LL},{0L,0L,0L,0L},{0x5F96EFB3321356F6LL,0xDF5F9EABAC11EBC7LL,0L,0L},{(-1L),0L,0x52B47B027DC054A8LL,0xDF5F9EABAC11EBC7LL},{0x5C1CF46B5271CAC1LL,(-1L),0x52B47B027DC054A8LL,0x9AA7848E98236AF1LL}},{{(-1L),0x11AF98608C39DE97LL,0L,0L},{0x5F96EFB3321356F6LL,0x5F96EFB3321356F6LL,0L,0x5C1CF46B5271CAC1LL},{0L,0x5C1CF46B5271CAC1LL,(-1L),0L},{0L,0x3DF8AB15005DFCC7LL,0x5F96EFB3321356F6LL,(-1L)},{(-4L),0x3DF8AB15005DFCC7LL,1L,0L},{0x3DF8AB15005DFCC7LL,0x5C1CF46B5271CAC1LL,0L,0x5C1CF46B5271CAC1LL},{0xD63C4D2870161610LL,0x5F96EFB3321356F6LL,0xDF5F9EABAC11EBC7LL,0L},{0L,0x11AF98608C39DE97LL,(-1L),0x9AA7848E98236AF1LL},{0L,(-1L),0x3DF8AB15005DFCC7LL,0xDF5F9EABAC11EBC7LL},{0L,0L,(-1L),0L}}};
            struct S0 **l_2913 = &g_396[0];
            struct S0 *** const l_2912[5][5][7] = {{{(void*)0,(void*)0,(void*)0,&l_2913,&l_2913,&l_2913,(void*)0},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,(void*)0,&l_2913,&l_2913,&l_2913,(void*)0,(void*)0},{(void*)0,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,(void*)0,&l_2913,&l_2913,(void*)0,&l_2913,&l_2913}},{{&l_2913,&l_2913,(void*)0,&l_2913,(void*)0,(void*)0,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{(void*)0,(void*)0,(void*)0,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,&l_2913,(void*)0,&l_2913,&l_2913,(void*)0,&l_2913}},{{&l_2913,&l_2913,&l_2913,&l_2913,(void*)0,&l_2913,&l_2913},{&l_2913,(void*)0,&l_2913,(void*)0,&l_2913,&l_2913,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,(void*)0,&l_2913},{(void*)0,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913}},{{&l_2913,&l_2913,&l_2913,(void*)0,&l_2913,(void*)0,&l_2913},{&l_2913,&l_2913,(void*)0,(void*)0,(void*)0,&l_2913,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,(void*)0},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{(void*)0,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,(void*)0}},{{&l_2913,(void*)0,(void*)0,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913,&l_2913},{(void*)0,&l_2913,&l_2913,&l_2913,&l_2913,(void*)0,&l_2913},{&l_2913,(void*)0,&l_2913,&l_2913,&l_2913,&l_2913,(void*)0}}};
            uint32_t l_2936 = 0x5591C9B4L;
            uint32_t l_2939[3];
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2545[i] = &g_768;
            for (i = 0; i < 3; i++)
                l_2939[i] = 0x8C5D8B13L;
        }
    }
    return g_3004;
}


/* ------------------------------------------ */
/* 
 * reads : g_542 g_1914 g_1189 g_1190 g_702 g_919 g_296 g_37 g_1364 g_414 g_1642 g_61 g_48 g_1952 g_4 g_1398 g_90 g_91 g_211 g_1212 g_189 g_154 g_748 g_1242.f0 g_768 g_878 g_1983 g_1993 g_1474 g_583 g_58 g_199 g_188 g_2014 g_2016 g_183 g_350 g_209 g_108 g_1994 g_1995 g_1996 g_585 g_1789 g_2044 g_636 g_1356 g_2069 g_1338 g_1354 g_175 g_132 g_2085 g_2088 g_1788.f0 g_135 g_396 g_1396 g_1210
 * writes: g_542 g_175 g_396 g_211 g_919 g_296 g_37 g_1642 g_48 g_4 g_58 g_702 g_154 g_1257 g_1993 g_1998 g_414 g_1788 g_2019 g_183 g_164 g_108 g_636 g_1356 g_2065 g_2069 g_748 g_1354 g_132 g_2087 g_135 g_1396
 */
static int32_t * const  func_7(const int32_t  p_8, const int32_t  p_9, uint64_t  p_10)
{ /* block id: 833 */
    int16_t l_1910 = 0x0572L;
    uint64_t l_1917 = 18446744073709551614UL;
    int64_t **l_1926 = &g_296;
    int32_t l_1940 = 0L;
    int32_t l_1966 = 0xDFFF472DL;
    uint32_t l_1988 = 0xF887FE08L;
    int64_t l_1991[5][7] = {{0xFBCA4168106CCE64LL,4L,0xB00704B18C1D172BLL,4L,0xFBCA4168106CCE64LL,4L,0xB00704B18C1D172BLL},{(-1L),(-1L),0x6132543156647731LL,(-1L),0x9732FAB108ADEC5CLL,0x9732FAB108ADEC5CLL,(-1L)},{1L,0x3FC1F095F0F35A83LL,1L,4L,1L,0x3FC1F095F0F35A83LL,1L},{(-1L),(-1L),(-1L),(-1L),0x9732FAB108ADEC5CLL,0x6132543156647731LL,0x6132543156647731LL},{0xFBCA4168106CCE64LL,0x3FC1F095F0F35A83LL,0xB00704B18C1D172BLL,0x3FC1F095F0F35A83LL,0xFBCA4168106CCE64LL,0x3FC1F095F0F35A83LL,0xB00704B18C1D172BLL}};
    int32_t l_2013 = 0x1B5EED64L;
    int32_t **l_2031 = &g_211;
    const uint8_t ***l_2068 = (void*)0;
    const struct S0 *l_2078 = &g_573;
    const struct S0 * const *l_2077[2];
    const struct S0 * const **l_2076 = &l_2077[0];
    uint16_t l_2079[2];
    struct S0 *l_2103 = (void*)0;
    int16_t l_2115 = 6L;
    int32_t l_2118 = (-1L);
    int32_t l_2120 = 0xFE5864F2L;
    int32_t l_2124 = 0x0958B6D8L;
    int32_t l_2125 = 0x1B02A4F4L;
    int64_t l_2128 = 1L;
    int32_t l_2129 = 0x3DCB282CL;
    int32_t l_2131 = 1L;
    uint32_t l_2133 = 0xA10E0BF0L;
    int i, j;
    for (i = 0; i < 2; i++)
        l_2077[i] = &l_2078;
    for (i = 0; i < 2; i++)
        l_2079[i] = 0xC189L;
    if (l_1910)
    { /* block id: 834 */
        int32_t * const l_1913 = &g_702;
        int64_t ***l_1916[4][9] = {{&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919},{&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919},{&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919},{&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919,&g_919}};
        uint8_t *l_1925[8][2];
        int32_t l_1961 = 1L;
        int32_t l_1967 = 0xBE207E45L;
        int32_t l_1970 = 0xF8A9DE7AL;
        uint64_t l_1985 = 0x7B0DB628079829D9LL;
        uint64_t l_2009 = 0x23A1579613CBEBDELL;
        int32_t l_2011 = 0x57DD942DL;
        int16_t l_2012 = (-8L);
        int i, j;
        for (i = 0; i < 8; i++)
        {
            for (j = 0; j < 2; j++)
                l_1925[i][j] = &g_183;
        }
        for (g_542 = (-20); (g_542 < 4); g_542++)
        { /* block id: 837 */
            for (g_175 = 0; g_175 < 1; g_175 += 1)
            {
                g_396[g_175] = &g_835;
            }
        }
        (*g_1914) = l_1913;
        if ((safe_unary_minus_func_uint64_t_u(((l_1910 ^ ((g_919 = &g_296) != (((l_1917 != ((((safe_mod_func_uint8_t_u_u((l_1910 == p_9), l_1910)) || (((~(safe_mod_func_uint32_t_u_u((((safe_sub_func_int16_t_s_s(5L, (((*g_1189) == l_1925[2][1]) && (*l_1913)))) , p_9) > 0x2DDBE85FL), p_10))) && l_1910) != 0x5555D374L)) <= 0x437CL) , (-7L))) < p_9) , l_1926))) >= 4294967295UL))))
        { /* block id: 842 */
            const uint8_t l_1939 = 0UL;
            int64_t *l_1941 = (void*)0;
            uint64_t *l_1942[1][7][1] = {{{(void*)0},{&l_1917},{&l_1917},{(void*)0},{&l_1917},{&l_1917},{(void*)0}}};
            int8_t *l_1947 = &g_1642;
            int32_t *l_1948 = (void*)0;
            int32_t *l_1949 = &g_48;
            int i, j, k;
            (*l_1949) &= (safe_mod_func_int8_t_s_s((((((*l_1913) | (safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s((safe_mul_func_int32_t_s_s(((safe_rshift_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(l_1939, (g_37 &= ((l_1940 = (p_8 , 0x3991DF907D145792LL)) <= (l_1941 == ((*l_1926) = (*g_919))))))), 31)) && (p_8 || (***g_1364))), (((safe_sub_func_int8_t_s_s(((*l_1947) &= (safe_add_func_uint64_t_u_u((p_10 = p_9), 0L))), (-1L))) == 0x29L) ^ g_61[0][2]))), 0L)), (*l_1913)))) ^ 0x59C98242L) , (void*)0) != (void*)0), 255UL));
            if (g_1642)
                goto lbl_1953;
        }
        else
        { /* block id: 849 */
lbl_1953:
            (*g_1952) |= (safe_rshift_func_uint16_t_u_u(p_8, (*l_1913)));
            for (g_58 = 0; (g_58 <= 7); g_58 += 1)
            { /* block id: 854 */
                const int32_t l_1958 = (-9L);
                int32_t *l_1968 = (void*)0;
                int32_t *l_1969[9][10] = {{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91},{&l_1940,(void*)0,&l_1940,&g_91,&l_1967,&g_6,&g_91,&g_6,&l_1967,&g_91}};
                int16_t *l_1982 = &g_199;
                int16_t **l_1981 = &l_1982;
                int i, j;
                l_1970 |= (safe_sub_func_int16_t_s_s((0xD11DL < ((safe_rshift_func_uint16_t_u_u(((l_1958 > (l_1966 = ((((*g_211) = (safe_lshift_func_uint16_t_u_s(l_1961, (safe_div_func_uint32_t_u_u(((p_8 , g_1398) != (safe_mul_func_uint32_t_u_u(4294967295UL, (p_10 == l_1917)))), (((0x42A6B4330E4101C6LL & 0x9DD3540E04C09841LL) , (*g_90)) && p_8)))))) || (**g_1212)) > 0x1B1BL))) <= g_37), (*g_189))) , l_1967)), 0xF0CAL));
                if ((((safe_mod_func_int16_t_s_s(((p_10 , p_9) == ((safe_mul_func_int8_t_s_s(g_748[2], g_1642)) != ((((safe_mod_func_uint32_t_u_u((((((65535UL | (safe_add_func_uint16_t_u_u(0xCC12L, ((*g_768) = (safe_lshift_func_uint8_t_u_s((**g_1189), g_1242.f0)))))) == (l_1940 = ((*l_1913) ^ p_8))) >= l_1917) & (*g_878)) | p_9), p_10)) , 0xEA3C56029C34BE97LL) & l_1966) < (*l_1913)))), 65535UL)) , &g_218[1][1]) == l_1981))
                { /* block id: 860 */
                    struct S0 *l_1984 = &g_1257;
                    (*l_1984) = g_1983;
                    --l_1985;
                    if ((*g_1952))
                        break;
                    for (l_1940 = 7; (l_1940 >= 1); l_1940 -= 1)
                    { /* block id: 866 */
                        int32_t l_1992 = (-6L);
                        if (l_1917)
                            break;
                        l_1992 ^= ((l_1988 < ((p_8 < ((*g_211) = p_9)) == (safe_lshift_func_int64_t_s_s(l_1991[3][4], 16)))) ^ p_8);
                        l_1992 &= (-6L);
                    }
                }
                else
                { /* block id: 872 */
                    const int16_t *** const **l_1997 = &g_1993;
                    const uint8_t **l_2010 = &g_1190;
                    volatile struct S0 *l_2015 = &g_1788[0];
                    g_1998[4][0] = ((*l_1997) = g_1993);
                    (*g_211) = (safe_sub_func_uint16_t_u_u((safe_mul_func_uint64_t_u_u((safe_lshift_func_int64_t_s_u((safe_div_func_uint16_t_u_u(((**g_188) = (((g_748[2] > (*g_1474)) == (((safe_mul_func_uint32_t_u_u((0x96L >= ((*g_878) ^= (&g_878 != (l_2009 , ((*l_1913) , l_2010))))), l_2011)) | ((((p_10 , (*l_1913)) | p_8) || 0x463FF92DE7292BF6LL) < p_10)) || 0UL)) | g_199)), l_2012)), 11)), l_2013)), 0x2CB6L));
                    (*l_2015) = g_2014[3][0][0];
                }
                for (l_1917 = 2; (l_1917 <= 7); l_1917 += 1)
                { /* block id: 882 */
                    struct S0 *l_2017[7];
                    struct S0 *l_2018[4] = {&g_1983,&g_1983,&g_1983,&g_1983};
                    int i;
                    for (i = 0; i < 7; i++)
                        l_2017[i] = &g_420;
                    g_2019 = g_2016[2][2];
                }
                if (g_542)
                    goto lbl_1953;
            }
        }
    }
    else
    { /* block id: 888 */
        const uint64_t l_2041 = 6UL;
        uint32_t l_2045 = 18446744073709551615UL;
        int32_t l_2046 = 0x83FACA62L;
        int32_t l_2050 = 0x42E36ED3L;
        int32_t l_2053 = 0x35BE27EAL;
        uint32_t *l_2064 = &g_158;
        int32_t *l_2084 = &g_702;
        int32_t l_2127[3][8][3] = {{{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L}},{{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L}},{{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L},{0L,0L,0L},{0xE7EA4B84L,0xE7EA4B84L,0xE7EA4B84L}}};
        int i, j, k;
lbl_2108:
        for (g_175 = 2; (g_175 <= 6); g_175 += 1)
        { /* block id: 891 */
            int32_t ***l_2037 = &g_723;
            int32_t l_2052[3][7] = {{0xE9BFAE72L,0xDDD6A819L,0xDDD6A819L,0xE9BFAE72L,0xDDD6A819L,0xDDD6A819L,0xE9BFAE72L},{1L,9L,1L,1L,9L,1L,1L},{0xE9BFAE72L,0xE9BFAE72L,0x8755551FL,0xE9BFAE72L,0xE9BFAE72L,0x8755551FL,0xE9BFAE72L}};
            uint64_t l_2054 = 0x20F1F340525BD6B0LL;
            struct S0 ** const *l_2075[9][2] = {{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431},{&g_1431,&g_1431}};
            int32_t *l_2082 = &g_132;
            int i, j;
            for (g_183 = 1; (g_183 <= 4); g_183 += 1)
            { /* block id: 894 */
                int32_t ***l_2030[10][10][2] = {{{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{(void*)0,(void*)0}},{{(void*)0,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723}},{{(void*)0,(void*)0},{(void*)0,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723}},{{&g_723,&g_723},{(void*)0,(void*)0},{(void*)0,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723}},{{&g_723,&g_723},{&g_723,&g_723},{(void*)0,(void*)0},{(void*)0,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723}},{{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{(void*)0,(void*)0},{(void*)0,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723}},{{&g_723,&g_723},{(void*)0,&g_723},{(void*)0,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{(void*)0,&g_723},{(void*)0,&g_723},{&g_723,&g_723}},{{&g_723,&g_723},{&g_723,&g_723},{(void*)0,&g_723},{(void*)0,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{(void*)0,&g_723},{(void*)0,&g_723}},{{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{(void*)0,&g_723},{(void*)0,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{(void*)0,&g_723}},{{(void*)0,&g_723},{&g_723,&g_723},{&g_723,&g_723},{&g_723,&g_723},{(void*)0,&g_723},{(void*)0,&g_723},{&g_723,(void*)0},{&g_723,&g_723},{&g_723,(void*)0},{&g_723,&g_723}}};
                int16_t *l_2032[8] = {&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0,&g_199,&g_199};
                int32_t l_2040 = 0x4E2C95A3L;
                int64_t *l_2042 = &g_58;
                int64_t *l_2043 = &g_108;
                uint8_t *****l_2070 = &g_2069;
                int32_t *l_2073 = &g_748[g_183];
                int32_t *l_2074[5][4];
                int i, j, k;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_2074[i][j] = &l_2040;
                }
                if ((safe_add_func_uint64_t_u_u((safe_div_func_int16_t_s_s((safe_rshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((g_164 = (&g_352 != (l_2031 = (void*)0))), 6)), (((safe_add_func_int16_t_s_s((g_748[g_183] , 1L), (((*l_2043) |= ((*l_2042) |= ((-7L) >= (safe_add_func_uint32_t_u_u((((&g_1211[0] == l_2037) ^ (((g_748[g_183] | (((((safe_lshift_func_uint64_t_u_s(18446744073709551615UL, 20)) , 0x455ECE74L) > 4294967290UL) ^ (*g_350)) != (*g_209))) == p_8) >= 0UL)) >= l_2040), l_2041))))) , (****g_1993)))) >= g_1789) & g_2044))), 15)), 0x3F5EL)), l_2045)))
                { /* block id: 899 */
                    int32_t l_2047 = 0x54A107C4L;
                    int32_t l_2048 = 0xCBB2C0CEL;
                    int32_t l_2049 = (-1L);
                    int32_t l_2051 = 0L;
                    uint32_t *l_2059 = (void*)0;
                    uint32_t *l_2060 = &g_1356[2][0][0];
                    int i;
                    g_636[g_183] = g_636[g_183];
                    if ((*g_1952))
                        continue;
                    --l_2054;
                    l_1940 = (safe_sub_func_int32_t_s_s((((*l_2060)--) , (((~(((*g_768) ^= p_10) | p_10)) , l_2064) != (g_2065[0][4] = &g_158))), p_8));
                }
                else
                { /* block id: 907 */
                    l_2050 |= (safe_sub_func_int8_t_s_s(((void*)0 != &g_1555), (l_2068 != &g_1189)));
                }
                l_2079[1] = ((((*l_2070) = g_2069) != &g_1363) > (((l_2050 = ((((*g_1994) != (void*)0) , ((l_1940 , p_9) <= (&g_1211[0] == ((g_1354 |= ((*l_2073) = (safe_mul_func_uint8_t_u_u(1UL, (*g_1338))))) , &g_1914)))) | 8L)) , l_2075[2][1]) != l_2076));
            }
            (*l_2082) |= ((0L ^ g_748[g_175]) & (safe_div_func_uint8_t_u_u(0UL, g_61[1][0])));
        }
        for (l_2053 = 0; (l_2053 <= 0); l_2053 += 1)
        { /* block id: 920 */
            int32_t **l_2083[2];
            volatile struct S0 *l_2086[8][4] = {{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]},{(void*)0,(void*)0,&g_498[2][0][3],&g_498[2][0][3]}};
            int64_t l_2126 = 0xAFEE55EAE3A79956LL;
            int64_t l_2132 = (-1L);
            int i, j;
            for (i = 0; i < 2; i++)
                l_2083[i] = &g_211;
            l_2084 = (void*)0;
            g_2087 = g_2085[3];
            for (g_37 = 0; (g_37 <= 2); g_37 += 1)
            { /* block id: 925 */
                int64_t *l_2100 = &g_135;
                struct S0 **l_2102[5];
                int32_t l_2106 = (-1L);
                uint8_t *l_2107[8][7][1] = {{{(void*)0},{&g_183},{(void*)0},{&g_1398},{&g_1396[1]},{&g_1398},{(void*)0}},{{&g_183},{(void*)0},{&g_1398},{&g_1396[1]},{&g_1398},{(void*)0},{&g_183}},{{(void*)0},{&g_1398},{&g_1396[1]},{&g_1398},{(void*)0},{&g_183},{(void*)0}},{{&g_1398},{&g_1396[1]},{&g_1398},{(void*)0},{&g_183},{(void*)0},{&g_1398}},{{&g_1396[1]},{&g_1398},{(void*)0},{&g_183},{(void*)0},{&g_1398},{&g_1396[1]}},{{&g_1398},{(void*)0},{&g_183},{(void*)0},{&g_1398},{&g_1396[1]},{&g_1398}},{{(void*)0},{&g_183},{(void*)0},{&g_1398},{&g_1396[1]},{&g_1398},{(void*)0}},{{&g_183},{(void*)0},{&g_1398},{&g_1396[1]},{&g_1398},{(void*)0},{&g_183}}};
                int32_t l_2110 = (-2L);
                int32_t l_2114[10] = {(-6L),(-6L),(-1L),(-6L),(-6L),(-1L),(-6L),(-6L),(-1L),(-6L)};
                int8_t l_2123 = 0x92L;
                int64_t l_2130[4] = {3L,3L,3L,3L};
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_2102[i] = &g_396[0];
                l_2046 ^= (g_2088[5][1] , (safe_mod_func_int8_t_s_s((((safe_div_func_uint64_t_u_u(((safe_add_func_uint8_t_u_u(1UL, (((((safe_mod_func_int8_t_s_s(g_1788[4].f0, (g_1396[1] |= ((*g_1338) & (((safe_mul_func_uint64_t_u_u(((safe_unary_minus_func_uint64_t_u(((((*l_2100) ^= p_10) <= ((4294967295UL >= (!((((l_2086[6][1] == (l_2103 = g_396[0])) != (safe_lshift_func_int8_t_s_u(0L, (*g_350)))) , 1UL) , p_10))) , p_10)) | l_2106))) > p_9), p_10)) ^ 1UL) > l_2079[1]))))) , p_9) != p_9) && 0xE06E31D0L) <= g_183))) , 0x98C3CD9C8AAD25BALL), p_10)) , p_10) | 0x3EA77F50F8122513LL), p_8)));
                if (g_199)
                    goto lbl_2108;
                for (l_2106 = 0; (l_2106 <= 2); l_2106 += 1)
                { /* block id: 933 */
                    int8_t l_2109[6] = {0x34L,0x34L,0x34L,0x34L,0x34L,0x34L};
                    int32_t l_2111 = 0xC068F3C0L;
                    int32_t l_2113 = 0L;
                    int32_t l_2116 = (-6L);
                    int32_t l_2117 = 0x07474740L;
                    int32_t l_2119 = 0xBACB4B46L;
                    int32_t l_2121 = 0xBB3067F2L;
                    int32_t l_2122[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_2122[i] = 3L;
                    ++l_2133;
                }
                if (p_10)
                    break;
            }
        }
    }
    return (*g_1210);
}


/* ------------------------------------------ */
/* 
 * reads : g_198 g_108 g_188 g_352 g_132 g_748 g_211 g_4 g_350 g_183 g_702 g_48 g_91 g_187 g_189 g_154 g_6 g_420.f0 g_1119 g_542 g_90 g_1430 g_1431 g_1554 g_1210 g_61 g_1583 g_878 g_414 g_1586 g_1588 g_1589 g_37 g_1013 g_209 g_1434 g_179 g_158 g_1396 g_199 g_1776 g_1189 g_1190 g_1788 g_1789 g_1790 g_1285.f0 g_175 g_976 g_1825 g_1829 g_1472 g_1473 g_1474 g_583 g_1848 g_449 g_546.f0 g_1338 g_1555 g_1356 g_1877 g_1120 g_585
 * writes: g_189 g_768 g_158 g_449 g_702 g_48 g_748 g_542 g_1136 g_91 g_211 g_414 g_183 g_1432 g_37 g_1617 g_1356 g_199 g_164 g_1777 g_585 g_175 g_1825 g_108 g_546.f0 g_43 g_218 g_1880 g_878 g_396 g_1443
 */
static int32_t  func_11(int32_t * p_12, uint32_t  p_13)
{ /* block id: 293 */
    uint16_t *l_767 = &g_154;
    int32_t l_769 = 0x2D1C3B1CL;
    uint32_t *l_770 = &g_158;
    int8_t *l_771 = &g_449[4][4];
    int16_t *l_772 = &g_585;
    uint16_t ***l_784 = &g_188;
    uint16_t ****l_783 = &l_784;
    uint16_t ** const *l_788 = &g_188;
    uint16_t ** const **l_787 = &l_788;
    int32_t l_799[2][2] = {{0xB682FA26L,0xB682FA26L},{0xB682FA26L,0xB682FA26L}};
    int32_t *l_898 = &g_91;
    struct S0 **l_1214 = &g_396[0];
    struct S0 *** const l_1213 = &l_1214;
    int8_t l_1255[2][5][3] = {{{2L,1L,0x61L},{2L,0x35L,(-1L)},{(-1L),0x19L,(-1L)},{(-1L),0x35L,2L},{0x61L,1L,2L}},{{9L,0x61L,(-1L)},{(-7L),(-7L),(-1L)},{9L,(-1L),0x61L},{0x61L,(-1L),9L},{(-1L),(-7L),(-7L)}}};
    int8_t l_1286 = (-9L);
    const int32_t *l_1289 = &g_48;
    const uint8_t **l_1362 = &g_1190;
    int64_t ***l_1404 = &g_919;
    int64_t ***l_1428 = &g_919;
    int32_t l_1450[4] = {0x04923BAEL,0x04923BAEL,0x04923BAEL,0x04923BAEL};
    const uint16_t *****l_1556 = &g_1555;
    uint32_t l_1562 = 0UL;
    uint32_t l_1644 = 0x1CABD54CL;
    uint16_t l_1666 = 65535UL;
    uint32_t l_1669 = 0x4BF5DB6EL;
    int8_t l_1824 = (-9L);
    int64_t l_1883[3][1];
    int32_t l_1906[9] = {0xA82EFE41L,0xA82EFE41L,0xA82EFE41L,0xA82EFE41L,0xA82EFE41L,0xA82EFE41L,0xA82EFE41L,0xA82EFE41L,0xA82EFE41L};
    uint32_t l_1907 = 4294967295UL;
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_1883[i][j] = 0x6F27C39BB346842CLL;
    }
    if ((safe_add_func_uint16_t_u_u((safe_div_func_int16_t_s_s(((safe_mod_func_int32_t_s_s((((((*l_771) = (((safe_sub_func_int16_t_s_s(((4294967293UL <= (safe_sub_func_int8_t_s_s(((g_198 & g_108) , (((p_13 == ((safe_div_func_int64_t_s_s(((((safe_mul_func_uint32_t_u_u(((((*g_188) = l_767) != (g_768 = l_767)) || 0x646BL), ((*l_770) = (l_769 < 255UL)))) < (*g_352)) >= l_769) > 0xD0L), p_13)) ^ (*p_12))) ^ p_13) && l_769)), l_769))) == (*g_211)), 0xD2FBL)) && l_769) , p_13)) >= (*g_350)) , l_772) != l_767), 6L)) || 0x5C3B055921085885LL), p_13)), 0x854FL)))
    { /* block id: 298 */
        int32_t l_773 = (-2L);
        uint16_t ****l_786 = &l_784;
        uint8_t l_800 = 0x30L;
        int16_t **l_881 = &l_772;
        const struct S0 *l_894[8] = {&g_241,&g_835,&g_835,&g_241,&g_835,&g_835,&g_241,&g_835};
        const struct S0 **l_893 = &l_894[7];
        const int64_t ** const l_901[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_945 = 7L;
        int32_t l_947 = 0xFEDD7872L;
        int32_t l_948 = 0xC99173EBL;
        int32_t l_949[8] = {0xC47D18EAL,(-9L),0xC47D18EAL,(-9L),0xC47D18EAL,(-9L),0xC47D18EAL,(-9L)};
        const uint16_t ****l_1050 = (void*)0;
        const uint16_t *****l_1049 = &l_1050;
        int8_t l_1068 = 1L;
        uint64_t l_1080 = 18446744073709551615UL;
        int32_t l_1137 = 0x910059C6L;
        uint32_t l_1194 = 1UL;
        struct S0 ***l_1216 = &l_1214;
        struct S0 ****l_1215 = &l_1216;
        int i;
        for (g_702 = 2; (g_702 >= 0); g_702 -= 1)
        { /* block id: 301 */
            uint32_t l_794 = 6UL;
            int16_t l_883 = 0x901FL;
            int64_t **l_902 = &g_296;
            uint32_t l_921 = 0x0D6A6343L;
            int32_t l_922 = (-3L);
            int16_t l_950[2];
            int32_t l_953 = 0x672D4EBCL;
            int32_t l_954 = 0xFC4797DBL;
            int32_t l_957 = 0x093F2D0BL;
            int32_t l_959 = 0x975A399FL;
            int32_t l_961 = 0x39AFF29CL;
            int32_t l_964 = 0xC97C3235L;
            int32_t l_966 = 0x562402DEL;
            int32_t l_967 = 0xC1B117EBL;
            int32_t l_968 = 9L;
            int32_t l_970 = 0xD134B847L;
            int32_t l_971[6] = {0x193168FFL,0x193168FFL,0x193168FFL,0x193168FFL,0x193168FFL,0x193168FFL};
            uint32_t l_1022 = 0UL;
            const uint16_t *****l_1048 = (void*)0;
            uint8_t l_1087 = 0UL;
            int i;
            for (i = 0; i < 2; i++)
                l_950[i] = 8L;
        }
        for (p_13 = 1; (p_13 <= 6); p_13 += 1)
        { /* block id: 422 */
            int32_t **l_1104 = &l_898;
            int i;
            (*l_1104) = func_29(((((g_748[p_13] > g_748[p_13]) , (+g_748[p_13])) > (safe_div_func_int32_t_s_s((safe_lshift_func_int8_t_s_s((0x909ECC34L && ((p_13 || (g_183 == (*p_12))) < (safe_rshift_func_int16_t_s_u(((*l_898) > ((((safe_lshift_func_uint64_t_u_u((!((safe_mul_func_uint16_t_u_u(1UL, 65530UL)) | (***g_187))), p_13)) , p_13) > g_6) && (**g_188))), g_748[p_13])))), g_48)), (*p_12)))) , p_13), &g_4, &g_91, (*l_898));
            (*l_898) = ((*p_12) = ((safe_add_func_int32_t_s_s((*p_12), ((safe_rshift_func_uint16_t_u_s((safe_sub_func_uint32_t_u_u(g_132, (0xBCL >= (safe_rshift_func_int32_t_s_s((!(safe_mul_func_int8_t_s_s(g_420.f0, (**l_1104)))), (*g_211)))))), g_108)) ^ (3UL >= (safe_unary_minus_func_int16_t_s((g_1119 != (void*)0))))))) && 0x2BL));
            if ((**l_1104))
                break;
        }
        for (g_542 = 13; (g_542 != (-20)); --g_542)
        { /* block id: 430 */
            int16_t ***l_1134[8] = {&l_881,&l_881,&l_881,&l_881,&l_881,&l_881,&l_881,&l_881};
            int16_t ****l_1135[6] = {&l_1134[4],&l_1134[4],&l_1134[4],&l_1134[4],&l_1134[4],&l_1134[4]};
            int32_t l_1142 = 1L;
            uint16_t ***l_1181 = &g_188;
            const uint8_t **l_1191 = &g_1190;
            int i;
            l_769 |= ((*l_898) = (safe_lshift_func_int32_t_s_u((safe_add_func_int8_t_s_s((safe_mod_func_int8_t_s_s(((*l_771) = (((safe_sub_func_uint64_t_u_u((((*p_12) != ((safe_mod_func_uint64_t_u_u(((safe_unary_minus_func_uint32_t_u(p_13)) < (((*p_12) > ((((&l_881 != (g_1136 = l_1134[4])) > l_1137) && (safe_sub_func_int8_t_s_s((*l_898), 0xA3L))) ^ (safe_mul_func_int32_t_s_s((*l_898), (*l_898))))) , 0x12L)), l_773)) | 3UL)) >= p_13), p_13)) >= p_13) != p_13)), l_1142)), p_13)), 19)));
            for (g_91 = 0; (g_91 <= (-17)); g_91 = safe_sub_func_uint32_t_u_u(g_91, 4))
            { /* block id: 437 */
                return (*g_90);
            }
        }
        (*l_1215) = l_1213;
    }
    else
    { /* block id: 476 */
        int8_t l_1249 = 0xECL;
        int16_t **l_1250 = &l_772;
        int32_t l_1304 = 0xBCD17E08L;
        int64_t **l_1331 = &g_296;
        uint64_t *l_1352 = &g_37;
        int32_t *l_1353 = &g_1354;
        uint32_t *l_1355 = &g_1356[2][1][0];
        int32_t *l_1367 = &l_1304;
        int32_t l_1496 = 0x20E51465L;
        int32_t l_1498 = 1L;
        uint32_t l_1499 = 0xC02F1958L;
    }
    if (((*g_1430) == ((*l_1213) = &g_396[0])))
    { /* block id: 644 */
        int32_t l_1569 = (-2L);
        int16_t l_1584[1][7];
        int32_t l_1585 = 0x117640C7L;
        int32_t l_1633 = 0x6990C331L;
        int32_t l_1637 = 0L;
        int32_t l_1639 = 0xAADF7739L;
        int32_t l_1641[6] = {0x6065B30DL,0x6D2E7FC6L,0x6065B30DL,0x6065B30DL,0x6D2E7FC6L,0x6065B30DL};
        uint64_t l_1665 = 18446744073709551615UL;
        struct S0 **l_1730 = &g_396[0];
        uint16_t **l_1746[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int64_t l_1778 = 1L;
        uint32_t l_1809[5][1];
        uint8_t **l_1817 = &g_878;
        const uint64_t l_1849 = 18446744073709551606UL;
        int32_t *l_1850 = (void*)0;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 7; j++)
                l_1584[i][j] = 0xB0DCL;
        }
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
                l_1809[i][j] = 1UL;
        }
        if ((safe_rshift_func_uint32_t_u_u((safe_mod_func_uint32_t_u_u((((p_13 >= (safe_sub_func_uint8_t_u_u(0x66L, (safe_unary_minus_func_int64_t_s(((l_1556 = g_1554[2]) == (void*)0)))))) , ((void*)0 == l_1289)) ^ (*p_12)), (safe_unary_minus_func_uint32_t_u((0xC0L <= ((+4294967290UL) | 0xAEL)))))), p_13)))
        { /* block id: 646 */
            int32_t **l_1559 = &g_211;
            (*l_1559) = (*g_1210);
            l_1585 &= (safe_add_func_int16_t_s_s(l_1562, (safe_mod_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(0L, (p_13 , (((safe_sub_func_int16_t_s_s(((((((g_61[1][0] == l_1569) , ((safe_add_func_uint8_t_u_u(((*g_878) &= ((safe_unary_minus_func_int32_t_s(((*l_898) ^= (*p_12)))) & (((((safe_div_func_int16_t_s_s((**l_1559), (safe_lshift_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((**l_1559), (**l_1559))), g_1583)), 5)) || (**l_1559)), l_1584[0][6])))) > (*p_12)) , 0x12FEC46DL) , 65535UL) <= p_13))), 5L)) , &g_1472)) != (void*)0) != p_13) , 0xF9L) == p_13), (**l_1559))) <= 65527UL) <= p_13)))), l_1584[0][1]))));
        }
        else
        { /* block id: 651 */
            int8_t l_1609 = 0x36L;
            uint16_t l_1612 = 0x66CCL;
            int32_t l_1635 = 1L;
            int32_t l_1636 = (-5L);
            int32_t l_1638 = 1L;
            int32_t l_1640[6];
            struct S0 **l_1731 = &g_396[0];
            int i;
            for (i = 0; i < 6; i++)
                l_1640[i] = 0xDD870E64L;
lbl_1587:
            (*g_1586) = (*g_1210);
            if ((*p_12))
            { /* block id: 653 */
                if (g_154)
                    goto lbl_1587;
                p_12 = (void*)0;
            }
            else
            { /* block id: 656 */
                int64_t l_1596 = 0x18851594E273CF55LL;
                int32_t l_1610 = 0xACC2615DL;
                int32_t l_1630[1];
                int16_t ***l_1663 = (void*)0;
                uint32_t *l_1664 = &g_1356[0][1][0];
                int i;
                for (i = 0; i < 1; i++)
                    l_1630[i] = 0x1CE4B2D1L;
                for (g_183 = 0; (g_183 <= 2); g_183 += 1)
                { /* block id: 659 */
                    int16_t l_1605 = 0xC8F0L;
                    uint16_t *l_1608 = &g_976[0][5][1];
                    volatile struct S0 *l_1615 = (void*)0;
                    volatile struct S0 *l_1616[7][8] = {{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179},{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179},{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179},{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179},{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179},{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179},{&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179,&g_1432,&g_179}};
                    int32_t l_1624 = (-5L);
                    int32_t l_1631 = 1L;
                    int32_t l_1632 = 0x54A59FEAL;
                    int32_t l_1634 = (-4L);
                    int32_t l_1643[8][7] = {{7L,(-5L),(-1L),0x2FB31A0DL,(-9L),0x2FB31A0DL,(-1L)},{(-8L),(-8L),4L,0x3A324785L,4L,(-8L),(-8L)},{0x5C28F56AL,(-5L),0L,(-5L),0x5C28F56AL,0x271112F5L,(-1L)},{0L,(-8L),0x3A324785L,0L,0L,0x3A324785L,(-8L)},{7L,0x271112F5L,(-9L),1L,0x5C28F56AL,0x2FB31A0DL,0x5C28F56AL},{0x3A324785L,0L,0L,0x3A324785L,(-8L),0x3A324785L,0L},{(-1L),(-5L),7L,1L,7L,(-5L),(-1L)},{4L,0L,0x7D59C931L,0L,4L,4L,0L}};
                    int i, j;
                    (*g_1589) = g_1588;
                    (*l_898) ^= 0x9402C7EFL;
                    for (g_37 = 0; (g_37 <= 6); g_37 += 1)
                    { /* block id: 664 */
                        uint64_t l_1595 = 0xBAFE42BCE2DC8079LL;
                        int64_t *l_1603 = (void*)0;
                        int64_t *l_1604[10] = {&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58,&g_58};
                        int32_t *l_1611[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_1611[i] = &l_1610;
                        (*p_12) ^= (safe_add_func_int16_t_s_s((safe_unary_minus_func_int8_t_s((safe_sub_func_uint8_t_u_u((l_1595 < (**g_1013)), (l_1596 && (safe_add_func_int16_t_s_s((l_1596 , (safe_sub_func_int64_t_s_s((((l_1605 = (safe_rshift_func_int64_t_s_u(l_1596, p_13))) || (((l_1595 == 0xC89C7C3E9EB4097FLL) || (safe_rshift_func_int32_t_s_s(((l_1608 != l_1608) || l_1605), (*g_211)))) == 0x91D062ADD92321E7LL)) != l_1596), l_1609))), l_1595))))))), l_1596));
                        ++l_1612;
                        (*p_12) &= (*g_209);
                    }
                    g_1617 = (*g_1434);
                    for (l_1562 = 0; (l_1562 <= 6); l_1562 += 1)
                    { /* block id: 673 */
                        int32_t *l_1618 = &l_1610;
                        int32_t *l_1619 = &g_132;
                        int32_t *l_1620 = &l_769;
                        int32_t *l_1621 = &l_799[1][0];
                        int32_t *l_1622 = &l_1585;
                        int32_t *l_1623 = (void*)0;
                        int32_t *l_1625 = &l_1624;
                        int32_t *l_1626 = &l_1610;
                        int32_t *l_1627 = &l_1624;
                        int32_t *l_1628 = &g_132;
                        int32_t *l_1629[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1629[i] = &g_702;
                        --l_1644;
                    }
                }
                (*p_12) = (safe_sub_func_uint8_t_u_u(0xA3L, ((((p_13 , ((safe_div_func_uint64_t_u_u((g_37 = ((safe_sub_func_int8_t_s_s((safe_mul_func_int32_t_s_s((safe_rshift_func_uint32_t_u_u(((*l_770)--), (p_13++))), ((*l_1289) ^ (*g_189)))), ((safe_rshift_func_uint64_t_u_s((l_1610 < ((((l_1663 == &g_217) == ((*l_1664) = g_1396[5])) <= (4UL & (*l_898))) > (*p_12))), 47)) & (*l_898)))) , 0xF418DAD09F10802DLL)), 1UL)) || 1L)) == l_1633) , l_1630[0]) , l_1639)));
                return l_1665;
            }
            for (g_199 = 8; (g_199 >= 2); g_199 -= 1)
            { /* block id: 686 */
                int64_t l_1676 = 0x2885C184BE54433ALL;
                int32_t l_1687[1][8];
                int8_t l_1760 = 7L;
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 8; j++)
                        l_1687[i][j] = (-1L);
                }
            }
            return (**g_1210);
        }
        for (l_1665 = 0; (l_1665 == 5); ++l_1665)
        { /* block id: 752 */
            int64_t l_1774 = 0xC32FCB9795AA4E6ALL;
            int32_t *l_1775 = (void*)0;
            int32_t l_1802 = (-2L);
            uint8_t *l_1886 = &g_414[1];
            volatile struct S0 *l_1898 = &g_1443;
            for (l_1666 = 0; (l_1666 <= 0); l_1666 += 1)
            { /* block id: 755 */
                int32_t l_1807[8] = {(-1L),(-1L),0x5EA3BF90L,(-1L),(-1L),0x5EA3BF90L,(-1L),(-1L)};
                uint8_t **l_1816[2];
                uint16_t **** const l_1840 = &l_784;
                int i;
                for (i = 0; i < 2; i++)
                    l_1816[i] = &g_350;
                l_1774 = g_414[l_1666];
                for (g_37 = 0; (g_37 <= 3); g_37 += 1)
                { /* block id: 759 */
                    (*l_898) = ((void*)0 == l_1775);
                    for (g_164 = 0; (g_164 >= 0); g_164 -= 1)
                    { /* block id: 763 */
                        g_1777 = g_1776;
                        (*p_12) &= (*g_90);
                        if (l_1778)
                            break;
                    }
                    (*p_12) = (((**g_1189) || ((((safe_div_func_int16_t_s_s(0xC25FL, (4UL ^ 0x2AB7L))) || p_13) || (safe_div_func_int32_t_s_s(1L, ((*l_770) = 4UL)))) && (safe_mod_func_int64_t_s_s((((~(safe_mod_func_int8_t_s_s(((g_1788[4] , (2UL >= p_13)) && p_13), l_1585))) & 0x77EC2EF330A2A191LL) , l_1774), g_414[2])))) , g_1789);
                    (*g_1790) = l_1775;
                }
                for (l_1669 = 0; (l_1669 <= 6); l_1669 += 1)
                { /* block id: 774 */
                    int16_t l_1799 = 0x2E37L;
                    const uint8_t *** volatile *l_1851 = &g_1363;
                    const int32_t **l_1871 = &l_1289;
                    uint16_t ****l_1872 = &l_784;
                    (*p_12) ^= (safe_lshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_u(((0xD8E7L && (l_1802 = ((*l_772) = (((*l_898) = 0L) == ((l_1774 , p_13) > ((safe_mul_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u(((l_1799 , l_1799) , (((void*)0 == &g_1136) <= (safe_mod_func_int16_t_s_s((0x2068L & l_1799), 1UL)))), 11)), g_1285.f0)) != p_13)))))) , p_13), (*g_189))), 10));
                    for (g_175 = 0; (g_175 <= 6); g_175 += 1)
                    { /* block id: 781 */
                        int32_t *l_1803 = &g_91;
                        int32_t *l_1804 = &l_1639;
                        int32_t *l_1805 = (void*)0;
                        int32_t *l_1806 = &l_1637;
                        int32_t *l_1808[4] = {&l_1585,&l_1585,&l_1585,&l_1585};
                        int64_t *l_1847 = &g_108;
                        int i, j, k;
                        l_1809[4][0]++;
                        (*g_90) ^= (safe_lshift_func_uint16_t_u_u((g_976[l_1666][(l_1666 + 2)][g_175] & p_13), (safe_sub_func_uint64_t_u_u(((l_1816[1] != l_1817) , ((((safe_div_func_int16_t_s_s((*l_1289), (safe_lshift_func_int64_t_s_s((((*l_1289) , (safe_add_func_int32_t_s_s((-1L), ((*p_12) , ((18446744073709551615UL <= 18446744073709551610UL) >= (-1L)))))) || l_1824), p_13)))) | 1L) == l_1641[1]) && p_13)), p_13))));
                        g_1825 = g_1825;
                        l_1850 = func_14(((safe_mul_func_uint8_t_u_u((g_1829 , ((*g_350) ^= (safe_lshift_func_uint64_t_u_s((((safe_mod_func_uint64_t_u_u(((safe_div_func_int8_t_s_s((safe_rshift_func_uint64_t_u_s((((((safe_mul_func_uint16_t_u_u(((void*)0 != l_1840), ((***g_1472) || (l_1585 >= p_13)))) <= p_13) , ((safe_div_func_int64_t_s_s(((*l_1847) = ((safe_rshift_func_int32_t_s_u(((((safe_mul_func_int8_t_s_s((p_12 != (void*)0), (-1L))) > p_13) | l_1799) || (-1L)), l_1807[1])) , 0x343BE942ACBCEADALL)), p_13)) != 0xC4L)) && 0x76L) , 0x404D042C2C849E9CLL), p_13)), (*g_878))) ^ (*l_1289)), g_154)) , g_1848) , l_1849), l_1637)))), 0x37L)) , (void*)0), p_13, &l_1807[2], &l_1585);
                    }
                    l_1851 = &g_1364;
                    (*p_12) = (((*g_350) ^= (((((((safe_lshift_func_uint32_t_u_u(l_1637, 3)) > (safe_unary_minus_func_int16_t_s(((safe_mod_func_int8_t_s_s((((&l_799[1][0] != ((*l_1871) = ((safe_mul_func_int8_t_s_s((((safe_lshift_func_int32_t_s_s((safe_rshift_func_int8_t_s_s(g_546.f0, 2)), (safe_lshift_func_uint32_t_u_s(p_13, ((*l_898) = (g_748[4] || g_199)))))) , (safe_lshift_func_uint8_t_u_s(((*l_1289) , ((safe_mod_func_uint32_t_u_u((g_37 & (safe_mod_func_int32_t_s_s((p_13 , 0xB06C2C44L), 0x9D04E947L))), (*p_12))) >= g_37)), (*l_1289)))) || (*p_12)), (*g_1338))) , p_12))) < p_13) & p_13), p_13)) != g_976[0][3][0])))) , l_1802) || p_13) && g_414[1]) , l_1872) == (*l_1556))) & g_1356[2][1][0]);
                }
            }
            for (l_1637 = 0; (l_1637 != 3); ++l_1637)
            { /* block id: 798 */
                if (((**g_1189) <= (0x2CL < (safe_add_func_uint16_t_u_u((*l_898), p_13)))))
                { /* block id: 799 */
                    struct S0 *l_1879[8][7] = {{&g_979,(void*)0,(void*)0,(void*)0,&g_979,&g_1848,&g_979},{&g_546,&g_1242,&g_420,(void*)0,&g_1242,&g_1242,(void*)0},{&g_420,(void*)0,&g_420,(void*)0,&g_667,(void*)0,&g_420},{&g_546,(void*)0,(void*)0,&g_546,&g_546,(void*)0,(void*)0},{&g_979,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_979},{&g_1242,&g_546,&g_420,&g_420,&g_546,&g_1242,&g_420},{&g_667,(void*)0,&g_420,(void*)0,&g_667,&g_1848,&g_667},{&g_546,&g_420,&g_420,&g_546,&g_1242,&g_420,(void*)0}};
                    int32_t l_1896 = 2L;
                    int i, j;
                    g_1880 = g_1877;
                    for (p_13 = 0; (p_13 == 41); ++p_13)
                    { /* block id: 803 */
                        int32_t l_1891 = 0x1A5094CCL;
                        int32_t l_1897 = 0L;
                        l_1883[0][0] = (((*g_1119) != l_1775) > p_13);
                        (*g_90) &= (safe_lshift_func_uint8_t_u_s((p_13 | ((l_1886 != ((*l_1817) = l_1886)) | (((*p_12) >= (safe_mul_func_int16_t_s_s(0L, (safe_sub_func_uint32_t_u_u((l_1802 = p_13), l_1891))))) & (safe_div_func_uint16_t_u_u((((safe_sub_func_uint16_t_u_u(0xD5E2L, ((*l_772) |= ((*l_1289) , l_1896)))) | l_1897) <= p_13), p_13))))), 6));
                    }
                }
                else
                { /* block id: 810 */
                    (*l_1730) = (void*)0;
                }
            }
            (*l_1898) = (*g_1434);
            if ((*p_12))
                break;
        }
        p_12 = p_12;
    }
    else
    { /* block id: 818 */
        volatile int64_t * volatile *l_1899 = (void*)0;
        (*l_898) &= (*p_12);
        l_1899 = (*g_1472);
        for (g_585 = 0; (g_585 < 27); g_585 = safe_add_func_uint32_t_u_u(g_585, 4))
        { /* block id: 823 */
            int8_t l_1902 = 7L;
            return l_1902;
        }
    }
    for (g_37 = 4; (g_37 > 18); g_37 = safe_add_func_uint32_t_u_u(g_37, 2))
    { /* block id: 829 */
        int32_t *l_1905[6] = {&g_48,&g_48,&g_48,&g_48,&g_48,&g_48};
        int i;
        ++l_1907;
    }
    return (*l_898);
}


/* ------------------------------------------ */
/* 
 * reads : g_350 g_183 g_449 g_91 g_702
 * writes: g_546.f0 g_43 g_218 g_183 g_748 g_91 g_702
 */
static int32_t * func_14(int32_t * p_15, uint64_t  p_16, int32_t * p_17, int32_t * p_18)
{ /* block id: 283 */
    const uint16_t ***l_742 = (void*)0;
    const uint16_t ****l_743 = &l_742;
    uint16_t ***l_744 = &g_188;
    uint16_t ****l_745 = &l_744;
    uint32_t l_746 = 0xCE43AE74L;
    int32_t *l_747 = &g_748[4];
    uint8_t l_749[2];
    int i;
    for (i = 0; i < 2; i++)
        l_749[i] = 2UL;
    for (g_546.f0 = 0; g_546.f0 < 7; g_546.f0 += 1)
    {
        for (g_43 = 0; g_43 < 10; g_43 += 1)
        {
            g_218[g_546.f0][g_43] = (void*)0;
        }
    }
    (*p_17) &= ((((-1L) | (*g_350)) , ((g_449[4][2] , ((((*g_350) = p_16) & 0x7CL) , (safe_add_func_int8_t_s_s((~(((*l_747) = ((safe_lshift_func_int8_t_s_s((((((*l_743) = l_742) != ((*l_745) = l_744)) , l_746) != ((void*)0 != &g_188)), 3)) == l_746)) , 1L)), 0x3EL)))) ^ l_749[0])) && p_16);
    (*p_17) = (safe_lshift_func_int8_t_s_s(0xF1L, 4));
    (*p_17) &= 0xBBAB9423L;
    return l_747;
}


/* ------------------------------------------ */
/* 
 * reads : g_41 g_350 g_183 g_683.f0 g_4 g_414 g_228 g_211 g_48
 * writes: g_41 g_723 g_414 g_48
 */
static int32_t * func_19(int8_t  p_20)
{ /* block id: 14 */
    uint64_t l_57 = 0x0DEEA39BA5493A50LL;
    int32_t *l_304 = &g_4;
    int32_t **l_303 = &l_304;
    uint8_t *l_734 = &g_414[1];
    int32_t *l_735[1];
    int i;
    for (i = 0; i < 1; i++)
        l_735[i] = (void*)0;
    for (p_20 = 0; (p_20 >= 4); p_20 = safe_add_func_uint8_t_u_u(p_20, 2))
    { /* block id: 17 */
        uint16_t l_79 = 0xF3A6L;
        int32_t *l_89 = &g_4;
        int32_t l_709[6] = {0xEBFFF38BL,0xEBFFF38BL,0xEBFFF38BL,0xEBFFF38BL,0xEBFFF38BL,0xEBFFF38BL};
        int i;
        for (g_41 = 0; (g_41 != 4); ++g_41)
        { /* block id: 20 */
            uint16_t *l_59 = (void*)0;
            uint16_t *l_60 = &g_61[1][0];
            int64_t *l_80 = &g_58;
            int32_t *l_85 = &g_48;
            int8_t *l_699 = &g_449[5][3];
            int32_t *l_700 = (void*)0;
            int32_t *l_701 = &g_702;
            int32_t *l_703 = &g_48;
            int32_t *l_704 = &g_91;
            int32_t *l_705 = &g_48;
            int32_t *l_706 = &g_702;
            int32_t *l_707 = &g_48;
            int32_t *l_708[2];
            int32_t l_710[1][2];
            uint8_t l_711[9][5] = {{0xA8L,3UL,1UL,0xC0L,3UL},{0xA5L,0UL,0UL,0xA5L,0xA5L},{0xF3L,0xF4L,0UL,3UL,3UL},{0x8EL,0xA5L,0x8EL,0xA5L,0xA5L},{3UL,1UL,0xC0L,3UL,0xC0L},{249UL,249UL,1UL,0xA5L,1UL},{0xA8L,0xF3L,0xC0L,0xC0L,0xF3L},{1UL,0UL,0x8EL,1UL,0xA5L},{0xF4L,0xF3L,0UL,0xF3L,0xF4L}};
            int i, j;
            for (i = 0; i < 2; i++)
                l_708[i] = &g_132;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_710[i][j] = 7L;
            }
        }
    }
    (*l_303) = func_29((safe_sub_func_int32_t_s_s(p_20, (safe_div_func_uint8_t_u_u(p_20, (+(safe_sub_func_int8_t_s_s(((((safe_rshift_func_int32_t_s_u(((g_723 = &l_304) == (((safe_rshift_func_int8_t_s_s((safe_sub_func_int64_t_s_s(((void*)0 == l_304), (safe_rshift_func_int64_t_s_u(((0xE409L > (safe_mod_func_uint8_t_u_u((*g_350), (((((*l_734) ^= (safe_mul_func_int8_t_s_s(g_683.f0, (**l_303)))) && (-8L)) && 0x25C8L) ^ p_20)))) > g_228), 24)))), 7)) < (*g_211)) , &l_304)), 31)) < (*l_304)) , &g_41) == (void*)0), (-1L)))))))), &g_6, &g_48, (**l_303));
    return l_735[0];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_48
 */
static int32_t * func_29(int64_t  p_30, int32_t * p_31, int32_t * p_32, uint8_t  p_33)
{ /* block id: 9 */
    int16_t l_45 = 0xF57BL;
    int32_t *l_46 = (void*)0;
    int32_t *l_47 = &g_48;
    (*l_47) = l_45;
    return &g_48;
}


/* ------------------------------------------ */
/* 
 * reads : g_37
 * writes: g_37
 */
static int8_t  func_34(int16_t  p_35)
{ /* block id: 4 */
    int32_t *l_36[3][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_6,(void*)0,&g_6,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    int i, j;
    g_37--;
    return p_35;
}


/* ------------------------------------------ */
/* 
 * reads : g_217 g_218 g_197 g_196 g_58 g_228 g_135 g_108 g_158 g_352 g_132 g_90 g_360 g_209 g_6 g_4 g_199 g_153 g_154 g_48 g_241 g_211 g_91 g_395 g_189 g_43 g_41 g_414 g_420 g_200 g_187 g_498 g_546 g_548 g_164 g_573 g_449 g_350 g_183 g_175 g_636 g_542 g_283 g_683 g_685
 * writes: g_58 g_108 g_158 g_350 g_132 g_91 g_48 g_164 g_396 g_183 g_43 g_414 g_211 g_449 g_188 g_175 g_179 g_154 g_142 g_135 g_542 g_667
 */
static int64_t  func_68(int32_t ** p_69, uint16_t  p_70, int32_t ** p_71)
{ /* block id: 96 */
    uint64_t l_307 = 0x736123B1B58F1CD6LL;
    int16_t *l_337 = (void*)0;
    int16_t **l_336 = &l_337;
    uint16_t *** const l_340 = (void*)0;
    int32_t l_359[8];
    int32_t l_384 = 0x382C8051L;
    struct S0 *l_394[8] = {&g_142,&g_142,&g_142,&g_142,&g_142,&g_142,&g_142,&g_142};
    uint32_t *l_448 = &g_158;
    int8_t l_450 = 2L;
    int32_t l_578 = 0L;
    int32_t l_586[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    int64_t l_587 = 0x7AA126F0A58C4BF6LL;
    uint8_t l_588[2][1][4] = {{{247UL,0xCDL,247UL,247UL}},{{0xCDL,0xCDL,0x56L,0xCDL}}};
    uint64_t l_591 = 0xDC6D6AE9E3DE8B0CLL;
    int32_t *l_686 = &l_359[5];
    int32_t *l_687 = &l_359[5];
    int32_t *l_688 = &g_132;
    int32_t *l_689 = &g_132;
    int32_t *l_690 = (void*)0;
    int32_t *l_691[2];
    uint32_t l_692 = 0x28CC0FC5L;
    int16_t l_695 = 0xCAF3L;
    uint32_t l_696 = 4294967294UL;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_359[i] = 0x6AC65B61L;
    for (i = 0; i < 2; i++)
        l_691[i] = &l_578;
    if ((safe_div_func_uint16_t_u_u(p_70, l_307)))
    { /* block id: 97 */
        int16_t *l_313 = &g_164;
        int16_t ** const l_312 = &l_313;
        int64_t *l_332 = &g_108;
        int32_t l_333[8] = {0x29E7C32AL,0x29E7C32AL,0x29E7C32AL,0x29E7C32AL,0x29E7C32AL,0x29E7C32AL,0x29E7C32AL,0x29E7C32AL};
        int16_t **l_335 = &l_313;
        int16_t ***l_334[5][2] = {{&l_335,&l_335},{&l_335,&l_335},{&l_335,&l_335},{&l_335,&l_335},{&l_335,&l_335}};
        uint16_t **l_457 = &g_189;
        uint32_t l_472 = 18446744073709551615UL;
        uint8_t *l_483 = &g_414[1];
        const uint32_t *l_486 = &g_158;
        struct S0 **l_489 = &l_394[5];
        uint8_t l_543 = 9UL;
        uint64_t l_566 = 1UL;
        int i, j;
        if ((safe_add_func_uint16_t_u_u((safe_add_func_int16_t_s_s((**g_217), (l_312 != (l_336 = ((safe_div_func_uint8_t_u_u(((safe_lshift_func_uint64_t_u_s((((safe_add_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_add_func_uint32_t_u_u(1UL, ((safe_mul_func_int64_t_s_s(0xBE093A50B06DFE74LL, (safe_lshift_func_int64_t_s_s((g_58 |= (-1L)), l_307)))) && (safe_unary_minus_func_int16_t_s(((+g_228) ^ (safe_div_func_int16_t_s_s(((254UL && (((*l_332) = p_70) < 0x023A199CE5FD471ELL)) | l_333[6]), 0x7553L)))))))), p_70)) && p_70), g_135)) , (-9L)) , g_108), p_70)) > l_333[4]), p_70)) , (void*)0))))), 0x560AL)))
        { /* block id: 101 */
            uint32_t *l_349 = &g_158;
            uint16_t l_351 = 0x7886L;
            int16_t ***l_382 = &l_336;
            int16_t **l_413 = (void*)0;
            uint8_t l_418 = 255UL;
            const uint32_t l_495 = 0UL;
            int32_t l_510 = 0xA5321530L;
            int32_t l_512 = (-10L);
            int32_t l_516 = 0xD22A20EBL;
            int32_t l_517 = 9L;
            int32_t l_518 = 0xF827ED63L;
            int32_t l_519 = 0x8DF442A4L;
            int32_t l_520 = 3L;
            int32_t l_521[5][1][3];
            uint8_t l_522 = 0x0EL;
            int i, j, k;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 3; k++)
                        l_521[i][j][k] = 0x6305D6EFL;
                }
            }
lbl_419:
            (*g_352) &= ((((((safe_mul_func_uint16_t_u_u((l_340 == (void*)0), (safe_add_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_s((safe_sub_func_uint32_t_u_u(((*l_349) &= (+(+0L))), ((g_350 = (void*)0) != &g_183))), ((void*)0 != &g_296))), l_351)))) || p_70) , p_70) & p_70) , l_307) & p_70);
            for (p_70 = 0; (p_70 < 57); p_70 = safe_add_func_int64_t_s_s(p_70, 7))
            { /* block id: 107 */
                uint32_t l_356[8];
                int16_t *l_372 = &g_164;
                int32_t *l_387 = &g_48;
                const int32_t *l_392 = &l_359[5];
                int i;
                for (i = 0; i < 8; i++)
                    l_356[i] = 0xE57F3D41L;
                (*g_360) = ((*g_90) = (((safe_unary_minus_func_int32_t_s(l_356[3])) | 0xDCL) || (((((0x770AL > l_307) == p_70) || (safe_mul_func_int64_t_s_s(0xDBDDC878D31A1BC2LL, g_132))) & 1UL) <= ((l_359[5] = (p_70 >= p_70)) , l_307))));
                for (g_164 = 1; (g_164 != 19); g_164 = safe_add_func_uint16_t_u_u(g_164, 9))
                { /* block id: 113 */
                    int32_t *l_363[7] = {&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132};
                    uint64_t l_366 = 18446744073709551615UL;
                    const int32_t *l_391 = (void*)0;
                    uint16_t l_415[2][6][3] = {{{65535UL,0x955FL,8UL},{0xD6EDL,0x3045L,1UL},{65535UL,0UL,65535UL},{0xD6EDL,0x6C99L,65532UL},{65535UL,65527UL,0x7F8DL},{0xD6EDL,1UL,0xD6EDL}},{{65535UL,0x955FL,8UL},{0xD6EDL,0x3045L,1UL},{65535UL,0UL,65535UL},{0xD6EDL,0x6C99L,65532UL},{65535UL,65527UL,0x7F8DL},{0xD6EDL,1UL,0xD6EDL}}};
                    int i, j, k;
                    (*g_209) = 0x7C5DDF7BL;
                    if ((safe_div_func_int16_t_s_s((g_6 ^ (((*g_217) != (void*)0) | (**g_217))), ((l_366 > g_4) && ((safe_mul_func_int8_t_s_s(p_70, (g_199 ^ (((*l_332) ^= ((*g_153) ^ p_70)) ^ 18446744073709551607UL)))) < 0x35L)))))
                    { /* block id: 116 */
                        int64_t l_371 = (-1L);
                        int32_t l_383 = 0x608C04ADL;
                        uint64_t l_388 = 0xF00378A03A67FCE7LL;
                        const int32_t *l_390 = (void*)0;
                        const int32_t **l_389[10];
                        int i;
                        for (i = 0; i < 10; i++)
                            l_389[i] = &l_390;
                        (*g_360) |= 0x50DD05D6L;
                        (*g_352) = (l_333[6] = (((safe_mul_func_uint64_t_u_u((&g_217 != ((((l_371 , (*g_217)) != l_372) | ((0UL == (safe_sub_func_int16_t_s_s(p_70, ((((safe_lshift_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(((safe_unary_minus_func_int64_t_s((&l_351 != (void*)0))) || (safe_mod_func_uint32_t_u_u((0xC259659D1DFF827CLL || l_371), p_70))), l_307)), p_70)) , 0x3DE9391A50DC0335LL) < p_70) > 0x69L)))) > p_70)) , l_382)), p_70)) != p_70) && p_70));
                        l_392 = (g_241 , (l_391 = func_29(l_383, ((*p_71) = (*p_69)), func_29(((*l_332) &= (((l_384 == 0xAF9EL) , (*g_217)) != (void*)0)), func_29(((l_351 == 0L) , l_371), l_387, &l_333[6], p_70), l_387, l_351), l_388)));
                    }
                    else
                    { /* block id: 124 */
                        int8_t *l_407 = &g_43;
                        uint64_t *l_410 = &l_307;
                        int32_t l_416 = 8L;
                        int32_t l_417 = (-7L);
                        (*l_387) &= ((*g_209) &= (+255UL));
                        (*g_395) = l_394[5];
                        (*p_69) = func_29(((l_417 &= ((safe_div_func_uint16_t_u_u(((*g_189) , 0xACDEL), (safe_div_func_int8_t_s_s((((((safe_mod_func_uint32_t_u_u(((safe_rshift_func_int64_t_s_u((((((((g_183 = 0UL) , (safe_sub_func_int8_t_s_s((((((*l_407) ^= l_333[3]) >= (g_414[1] = ((safe_mul_func_int32_t_s_s((((*l_410) |= l_359[5]) == (((l_351 == (safe_sub_func_uint32_t_u_u(4294967295UL, 0UL))) , l_413) == (*l_382))), g_6)) | (**p_69)))) || 8UL) >= (-1L)), 0x27L))) > l_359[5]) , p_70) ^ g_41) <= 0x392BCE64L) | 65532UL), l_415[1][2][2])) ^ (-1L)), l_416)) >= 0xCB5ADCCEL) & 1UL) != l_359[5]) & l_333[5]), (*l_387))))) && g_414[1])) > 0UL), (*p_71), (*p_69), l_418);
                        if (g_48)
                            goto lbl_419;
                    }
                }
                (*l_387) = (0xC8L != (g_420 , 0xE1L));
            }
            for (g_132 = (-6); (g_132 != (-24)); g_132 = safe_sub_func_int16_t_s_s(g_132, 9))
            { /* block id: 141 */
                int16_t *l_425[7];
                uint64_t *l_442 = &l_307;
                int32_t l_451 = 0L;
                int64_t **l_452 = &g_296;
                const int16_t *l_455 = &g_164;
                const int16_t **l_454 = &l_455;
                const int16_t ***l_453 = &l_454;
                int32_t *l_456 = &l_359[5];
                int32_t *l_458 = &l_333[6];
                int32_t *l_459 = &l_333[5];
                int32_t *l_460 = &l_333[6];
                int32_t *l_461 = &g_48;
                int32_t *l_462 = &g_91;
                int32_t *l_463 = &l_359[5];
                int32_t *l_464 = (void*)0;
                int32_t *l_465 = &g_48;
                int32_t *l_466 = (void*)0;
                int32_t *l_467 = &g_91;
                int32_t *l_468 = &l_451;
                int32_t *l_469 = &g_48;
                int32_t *l_470 = &g_48;
                int32_t *l_471[10][1][2] = {{{&g_132,&g_132}},{{&g_6,&l_359[5]}},{{&g_48,&l_359[5]}},{{&g_6,&g_132}},{{&g_132,&g_6}},{{&l_359[5],&g_48}},{{&l_359[5],&g_6}},{{&g_132,&g_132}},{{&g_6,&l_359[5]}},{{&g_48,&l_359[5]}}};
                int i, j, k;
                for (i = 0; i < 7; i++)
                    l_425[i] = &g_199;
                (*l_456) = ((((void*)0 == &l_359[5]) && (0x218CA9857A78FE82LL != 0xA8217980DC1767E7LL)) , (safe_mul_func_uint16_t_u_u((l_425[0] != (*g_217)), ((*l_313) = ((((safe_lshift_func_int16_t_s_s(((((((safe_sub_func_uint64_t_u_u((g_449[4][2] = ((safe_div_func_int16_t_s_s((safe_add_func_uint64_t_u_u(g_4, (safe_mul_func_uint32_t_u_u((p_70 ^ (l_333[6] = (safe_sub_func_int64_t_s_s((safe_add_func_uint64_t_u_u((((safe_lshift_func_uint64_t_u_u((++(*l_442)), (safe_lshift_func_int32_t_s_s((((+((*l_332) = ((l_359[5] , l_448) == &g_158))) >= l_333[6]) && l_351), 1)))) || l_333[6]) , p_70), 1L)), g_6)))), p_70)))), p_70)) > 0x6E97BB8AL)), g_200)) == l_450) == l_451) , l_452) != (void*)0) , l_450), 12)) , l_453) != &l_336) && p_70)))));
                (*g_187) = l_457;
                l_472++;
            }
            for (l_450 = 7; (l_450 >= 0); l_450 -= 1)
            { /* block id: 153 */
                int32_t *l_499 = &l_359[5];
                int32_t *l_500 = &l_333[6];
                int32_t *l_501 = &g_48;
                int32_t *l_502 = &l_333[6];
                int32_t *l_503 = (void*)0;
                int32_t *l_504 = &l_333[6];
                int32_t *l_505 = &l_333[6];
                int32_t *l_506 = (void*)0;
                int32_t l_507 = (-8L);
                int32_t *l_508 = &l_333[6];
                int32_t *l_509 = &l_333[6];
                int32_t *l_511 = &g_132;
                int32_t *l_513 = &g_48;
                int32_t *l_514 = (void*)0;
                int32_t *l_515[5][9][5] = {{{&l_333[6],&l_359[5],&l_333[6],&l_510,&l_333[6]},{&l_507,(void*)0,&l_359[5],&l_507,&l_507},{&l_359[0],&l_510,&l_359[0],&l_359[5],(void*)0},{&l_507,(void*)0,&l_507,(void*)0,(void*)0},{&l_333[6],&l_510,&g_4,&l_510,&l_333[6]},{(void*)0,(void*)0,&l_507,(void*)0,&l_507},{(void*)0,&l_359[5],&l_359[0],&l_510,&l_359[0]},{&l_507,&l_507,&l_359[5],(void*)0,&l_507},{&l_333[6],&l_510,&l_333[6],&l_359[5],&l_333[6]}},{{&l_507,&l_507,&l_507,&l_507,(void*)0},{(void*)0,&l_510,(void*)0,&l_510,(void*)0},{(void*)0,&l_507,&l_507,&l_507,&l_507},{&l_333[6],&l_359[5],&l_333[6],&l_510,&l_333[6]},{&l_507,(void*)0,&l_359[5],&l_507,&l_507},{&l_359[0],&l_510,&l_359[0],&l_359[5],(void*)0},{&l_507,(void*)0,&l_507,(void*)0,(void*)0},{&l_333[6],&l_510,&g_4,&l_510,&l_333[6]},{(void*)0,(void*)0,&l_507,(void*)0,&l_507}},{{(void*)0,&l_359[5],&l_359[0],&l_359[5],(void*)0},{(void*)0,(void*)0,(void*)0,&l_507,(void*)0},{&g_4,&l_359[5],&g_4,(void*)0,&l_333[6]},{(void*)0,&l_359[5],&l_359[5],(void*)0,&g_132},{&l_359[0],&l_359[5],(void*)0,&l_359[5],&l_359[0]},{&g_132,(void*)0,&l_359[5],&l_359[5],(void*)0},{&l_333[6],(void*)0,&g_4,&l_359[5],&g_4},{(void*)0,&l_507,(void*)0,(void*)0,(void*)0},{(void*)0,&l_359[5],(void*)0,(void*)0,&l_359[0]}},{{(void*)0,&g_132,&l_359[5],&l_507,&g_132},{&l_333[6],&l_359[5],&l_333[6],&l_359[5],&l_333[6]},{&g_132,&l_507,&l_359[5],&g_132,(void*)0},{&l_359[0],(void*)0,(void*)0,&l_359[5],(void*)0},{(void*)0,(void*)0,(void*)0,&l_507,(void*)0},{&g_4,&l_359[5],&g_4,(void*)0,&l_333[6]},{(void*)0,&l_359[5],&l_359[5],(void*)0,&g_132},{&l_359[0],&l_359[5],(void*)0,&l_359[5],&l_359[0]},{&g_132,(void*)0,&l_359[5],&l_359[5],(void*)0}},{{&l_333[6],(void*)0,&g_4,&l_359[5],&g_4},{(void*)0,&l_507,(void*)0,(void*)0,(void*)0},{(void*)0,&l_359[5],(void*)0,(void*)0,&l_359[0]},{(void*)0,&g_132,&l_359[5],&l_507,&g_132},{&l_333[6],&l_359[5],&l_333[6],&l_359[5],&l_333[6]},{&g_132,&l_507,&l_359[5],&g_132,(void*)0},{&l_359[0],(void*)0,(void*)0,&l_359[5],(void*)0},{(void*)0,(void*)0,(void*)0,&l_507,(void*)0},{&g_4,&l_359[5],&g_4,(void*)0,&l_333[6]}}};
                int i, j, k;
                for (g_175 = 2; (g_175 <= 7); g_175 += 1)
                { /* block id: 156 */
                    int64_t l_494 = 0xB3B1A930AC77B250LL;
                    if ((0x56L || p_70))
                    { /* block id: 157 */
                        int32_t *l_496 = &g_91;
                        uint32_t **l_497[1][9][10] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_349,(void*)0,(void*)0,&l_349,(void*)0,(void*)0,&l_349,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_349,(void*)0,(void*)0,&l_349,(void*)0,(void*)0,&l_349,(void*)0},{(void*)0,&l_349,&l_349,(void*)0,&l_349,&l_349,(void*)0,&l_349,&l_349,(void*)0},{&l_349,(void*)0,&l_349,&l_349,(void*)0,&l_349,&l_349,(void*)0,&l_349,&l_349},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
                        int i, j, k;
                        (*l_496) ^= (safe_add_func_uint64_t_u_u((((((safe_mod_func_uint32_t_u_u(g_196, ((safe_lshift_func_uint32_t_u_s((l_359[5] , (safe_mul_func_int64_t_s_s(((g_350 = l_483) != ((safe_rshift_func_int8_t_s_s((l_486 != (l_448 = func_29(((safe_div_func_int16_t_s_s((((((*l_483) = (l_489 != (g_43 , (void*)0))) || 0xCCL) >= (safe_rshift_func_int32_t_s_s(((((((safe_rshift_func_uint8_t_u_s((((((**p_69) && (*g_211)) >= l_494) && l_495) < 0xFDA9F7B7DE32D582LL), 6)) < p_70) , l_495) || l_494) == 0xA541CA58404EA0C6LL) ^ 0xE32876BD8309ED0CLL), 8))) , 0x3BD0L), l_333[1])) == p_70), l_496, (*p_69), g_6))), 4)) , (void*)0)), 0x5B9E783FFCBA9512LL))), 10)) , l_333[6]))) | 0UL) && (-1L)) ^ g_4) & 9L), 0x05FA5C55A6DB96E4LL));
                        (*p_71) = l_349;
                    }
                    else
                    { /* block id: 163 */
                        return l_351;
                    }
                    (*p_71) = (*p_69);
                    g_179 = g_498[1][0][2];
                }
                --l_522;
            }
        }
        else
        { /* block id: 171 */
            int32_t l_529 = 1L;
            int32_t l_530 = 0x3EBA1BA3L;
            int32_t l_531 = 0x9A998834L;
            int32_t l_533[6];
            int i;
            for (i = 0; i < 6; i++)
                l_533[i] = 2L;
            if ((**p_69))
            { /* block id: 172 */
                int32_t l_526 = 0x76D82210L;
                int32_t l_527 = 9L;
                int32_t l_528 = (-9L);
                int32_t l_532 = 0x874411FAL;
                int32_t l_534 = 0x25B84161L;
                int32_t l_535 = 0L;
                int32_t l_536[5][9][5] = {{{1L,0L,2L,(-10L),0x38F53EC4L},{0x6608AE73L,0x54B1F418L,6L,0x54B1F418L,0x6608AE73L},{1L,5L,(-9L),(-7L),0L},{0xE032BECDL,0xF4251CABL,0x9FAC7718L,0xCCD61FACL,0xE1A42C4EL},{9L,1L,2L,5L,0L},{(-1L),0xCCD61FACL,0x03B3050BL,0L,0x6608AE73L},{0L,2L,(-10L),0x38F53EC4L,0x38F53EC4L},{3L,0xF4251CABL,3L,0L,0x9FAC7718L},{(-7L),9L,0x719E0550L,5L,(-7L)}},{{0x1FDA103CL,0x54B1F418L,(-5L),0xCCD61FACL,(-5L)},{0L,(-9L),0x719E0550L,(-7L),1L},{(-6L),(-7L),3L,0x54B1F418L,0xE032BECDL},{9L,(-7L),(-10L),(-10L),(-7L)},{(-5L),(-7L),0x03B3050BL,0x54B03DB6L,6L},{1L,(-9L),2L,(-2L),0x38F53EC4L},{0x53165345L,0x54B1F418L,0x9FAC7718L,0x54B1F418L,0x53165345L},{1L,9L,(-9L),0x40730CD2L,0L},{(-5L),0xF4251CABL,6L,0xCCD61FACL,(-7L)}},{{9L,2L,2L,9L,0L},{(-6L),0xCCD61FACL,0xA08369C1L,0L,0x53165345L},{0L,1L,(-10L),0x719E0550L,0x38F53EC4L},{0x1FDA103CL,0xF4251CABL,0x1FDA103CL,0L,6L},{(-7L),5L,0x719E0550L,9L,(-7L)},{3L,0x54B1F418L,0xE032BECDL,0xCCD61FACL,0xE032BECDL},{0L,0L,0x719E0550L,0x40730CD2L,1L},{(-1L),(-7L),0x1FDA103CL,0x54B1F418L,(-5L)},{9L,0x40730CD2L,(-10L),(-2L),(-7L)}},{{0xE032BECDL,(-7L),0xA08369C1L,0x54B03DB6L,0x9FAC7718L},{1L,0L,2L,(-10L),0x38F53EC4L},{0x6608AE73L,0x54B1F418L,6L,0x54B1F418L,0x6608AE73L},{1L,5L,(-9L),(-7L),0L},{0xE032BECDL,0xF4251CABL,0x9FAC7718L,0xCCD61FACL,(-6L)},{1L,(-7L),0x40730CD2L,2L,1L},{0x1FDA103CL,0L,6L,0x54B03DB6L,0x03B3050BL},{1L,0x40730CD2L,0x719E0550L,0L,0L},{0x53165345L,1L,0x53165345L,0x54B03DB6L,0xE032BECDL}},{{(-2L),1L,(-9L),2L,(-2L)},{0x6608AE73L,(-7L),(-7L),0L,(-7L)},{1L,1L,(-9L),(-2L),9L},{3L,0xCCD61FACL,0x53165345L,(-7L),0xE1A42C4EL},{1L,(-2L),0x719E0550L,0x719E0550L,(-2L)},{(-7L),0xCCD61FACL,6L,0xF4251CABL,(-5L)},{(-7L),1L,0x40730CD2L,0x38F53EC4L,0L},{0xA08369C1L,(-7L),0xE032BECDL,(-7L),0xA08369C1L},{(-7L),1L,1L,(-10L),1L}}};
                int i, j, k;
                for (g_108 = 4; (g_108 >= 0); g_108 -= 1)
                { /* block id: 175 */
                    int32_t *l_525[6][9][4] = {{{(void*)0,&g_48,&l_359[5],(void*)0},{(void*)0,&l_333[6],&l_333[5],&g_132},{&g_132,&l_359[5],(void*)0,&g_4},{&l_359[5],&g_91,(void*)0,&l_333[1]},{(void*)0,&g_48,(void*)0,&l_333[5]},{&l_333[6],&g_48,&l_359[5],&l_333[0]},{(void*)0,&g_132,(void*)0,&g_48},{&g_48,&l_359[5],&l_333[4],(void*)0},{(void*)0,&g_6,&l_359[5],&g_91}},{{&g_91,&g_91,(void*)0,&g_132},{(void*)0,&g_48,&l_333[6],(void*)0},{&l_333[0],&l_333[4],&g_6,&g_4},{&l_333[5],&g_4,(void*)0,&g_4},{&g_91,(void*)0,&l_359[5],&g_91},{(void*)0,&g_132,&g_6,&g_48},{(void*)0,&g_48,&g_48,(void*)0},{&g_6,&g_132,&g_48,&g_132},{(void*)0,&g_132,&l_333[4],&g_48}},{{&g_4,&l_333[4],(void*)0,(void*)0},{(void*)0,&l_359[1],(void*)0,&g_48},{&l_333[6],&l_333[6],&l_333[5],&g_132},{(void*)0,&l_359[5],(void*)0,&l_333[6]},{&g_132,&g_4,(void*)0,&l_333[1]},{(void*)0,&g_91,&l_333[5],&l_333[5]},{&l_333[6],&g_132,(void*)0,&g_132},{(void*)0,&g_132,(void*)0,&g_4},{&g_4,&l_333[4],&l_333[4],(void*)0}},{{(void*)0,&l_359[5],&g_48,&g_91},{&g_6,&l_359[5],&g_48,&g_4},{(void*)0,&g_132,&g_6,&l_333[5]},{(void*)0,&l_333[4],&l_359[5],&l_333[6]},{&g_91,(void*)0,&g_91,&l_333[6]},{(void*)0,&g_4,&g_91,(void*)0},{&l_359[5],&g_48,&l_333[6],&l_359[5]},{&g_91,&g_91,(void*)0,(void*)0},{&g_4,&l_359[5],&g_4,(void*)0}},{{&l_333[3],&g_48,&l_333[0],(void*)0},{&l_333[4],&g_132,&l_359[5],&g_48},{(void*)0,&l_359[5],(void*)0,&l_359[5]},{&g_4,(void*)0,&l_359[5],&l_359[5]},{(void*)0,(void*)0,(void*)0,&l_333[4]},{&l_333[6],(void*)0,&g_48,&g_48},{(void*)0,(void*)0,&l_333[6],&l_333[6]},{(void*)0,(void*)0,(void*)0,&g_132},{&l_359[3],&g_48,(void*)0,&l_333[4]}},{{&g_6,&l_333[0],&g_48,(void*)0},{&l_333[3],&l_333[0],&l_359[1],&l_333[4]},{&l_333[0],&g_48,&l_359[5],&g_132},{&g_132,(void*)0,&l_333[6],&l_333[6]},{(void*)0,(void*)0,&g_48,&g_48},{&l_359[5],(void*)0,&l_333[0],&l_333[4]},{&g_48,(void*)0,&g_91,&l_359[5]},{(void*)0,(void*)0,&g_132,&l_359[5]},{&g_132,&l_359[5],(void*)0,&g_48}}};
                    uint32_t l_538 = 5UL;
                    int i, j, k;
                    l_538++;
                    for (g_154 = 0; (g_154 <= 7); g_154 += 1)
                    { /* block id: 179 */
                        int32_t l_541 = 1L;
                        int i;
                        if (g_414[(g_108 + 1)])
                            break;
                        (*p_69) = &l_333[g_154];
                        if ((**p_69))
                            continue;
                        l_543--;
                    }
                    (*p_69) = l_525[2][7][3];
                    return p_70;
                }
            }
            else
            { /* block id: 188 */
                const int64_t l_558[2] = {1L,1L};
                int32_t *l_559 = &l_533[0];
                int32_t *l_565 = &l_359[5];
                int i;
                (*g_548) = g_546;
                for (g_164 = 3; (g_164 > (-16)); g_164 = safe_sub_func_uint16_t_u_u(g_164, 7))
                { /* block id: 192 */
                    uint32_t l_551 = 0x52832849L;
                    return l_551;
                }
                (*l_565) &= ((*g_360) = (((((safe_mul_func_int32_t_s_s((safe_add_func_uint16_t_u_u(((l_333[6] = ((safe_mod_func_int64_t_s_s(0x1E7BE73610A4E49DLL, ((((l_558[0] || ((*l_559) &= (**p_69))) ^ (p_70 , (safe_mod_func_uint64_t_u_u((!(safe_lshift_func_uint8_t_u_s(p_70, 5))), p_70)))) | ((l_531 , ((0xFB3080E2CB6419F0LL <= p_70) || p_70)) <= l_531)) , (*l_559)))) && 0x52E0L)) <= p_70), l_472)), 0xCE95772EL)) | p_70) | p_70) , (**p_69)) || (*l_559)));
            }
        }
        (*p_69) = ((l_359[5] | p_70) , func_29(l_566, (*p_69), func_29(p_70, func_29((((safe_mul_func_int8_t_s_s((p_70 < (((safe_add_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((g_573 , (safe_mul_func_int32_t_s_s(((((safe_mul_func_uint32_t_u_u(p_70, (&l_312 == (void*)0))) == l_359[5]) | p_70) == l_578), l_307))), 0)), p_70)) , 0xCF94L) < 65528UL)), p_70)) , l_340) != (void*)0), &g_48, &g_91, g_4), &l_333[6], l_543), p_70));
    }
    else
    { /* block id: 202 */
        int32_t *l_579 = &g_91;
        int32_t *l_580 = (void*)0;
        int32_t *l_581 = &l_578;
        int32_t *l_582[4][10] = {{&l_359[0],&l_578,(void*)0,(void*)0,&l_578,&l_359[0],(void*)0,(void*)0,&l_359[5],(void*)0},{&l_359[5],(void*)0,&g_4,&g_132,(void*)0,&g_132,&g_4,(void*)0,&l_359[5],&l_359[0]},{&l_359[5],(void*)0,&g_91,(void*)0,&g_132,&l_359[0],&l_359[0],&g_132,(void*)0,&g_91},{&l_359[0],&l_359[0],&g_132,(void*)0,&g_91,(void*)0,&l_359[5],&l_578,&l_359[5],(void*)0}};
        int16_t l_584 = 0x41A1L;
        uint32_t l_621 = 18446744073709551615UL;
        uint8_t *l_637[5][6][4] = {{{&g_414[5],&l_588[1][0][3],&g_183,(void*)0},{&l_588[0][0][3],&l_588[1][0][3],&g_414[1],&g_183},{&l_588[0][0][1],&g_414[1],&g_183,&l_588[0][0][3]},{&l_588[1][0][3],&l_588[0][0][2],&g_183,&l_588[0][0][1]},{&l_588[0][0][3],&l_588[0][0][3],&l_588[0][0][3],&g_414[1]},{&l_588[1][0][3],&l_588[1][0][3],&l_588[0][0][0],&l_588[1][0][3]}},{{&l_588[1][0][3],&l_588[0][0][2],&l_588[1][0][3],&l_588[1][0][3]},{&g_183,&l_588[0][0][1],&l_588[1][0][3],&g_183},{&l_588[1][0][3],(void*)0,&l_588[0][0][0],&l_588[1][0][3]},{&l_588[1][0][3],&l_588[1][0][3],&l_588[0][0][3],&g_414[5]},{&l_588[0][0][3],&g_414[5],&g_183,&g_183},{&l_588[1][0][3],&l_588[1][0][3],&g_183,&g_414[2]}},{{&l_588[0][0][1],&l_588[0][0][2],&g_414[1],&g_414[1]},{&l_588[0][0][3],&g_414[2],&g_183,&g_414[1]},{&g_414[5],&g_414[2],&g_414[1],&l_588[0][0][3]},{&l_588[0][0][2],&l_588[1][0][3],&g_183,&l_588[0][0][2]},{&l_588[1][0][2],&g_183,&l_588[1][0][3],&l_588[1][0][2]},{(void*)0,&g_183,&g_414[1],&g_183}},{{&g_183,&l_588[0][0][0],&l_588[1][0][2],&g_414[1]},{&g_183,&g_183,&l_588[0][0][2],&l_588[1][0][2]},{&l_588[0][0][3],&g_183,&l_588[0][0][1],&l_588[1][0][3]},{&l_588[0][0][3],&l_588[1][0][3],&l_588[0][0][2],&g_183},{&g_183,&l_588[1][0][3],&l_588[1][0][2],&g_414[5]},{&g_183,(void*)0,&g_414[1],&g_183}},{{(void*)0,&l_588[1][0][3],&l_588[1][0][3],(void*)0},{&l_588[1][0][2],&l_588[0][0][3],&g_183,&l_588[1][0][2]},{&l_588[0][0][2],&g_414[1],&g_414[1],&g_183},{&g_183,&l_588[0][0][0],&l_588[1][0][3],&g_183},{&g_183,&g_414[1],&g_414[5],&l_588[1][0][2]},{&g_183,&l_588[0][0][3],&l_588[0][0][1],(void*)0}}};
        int64_t *l_651 = &g_108;
        int i, j, k;
        --l_588[1][0][3];
lbl_616:
        ++l_591;
        for (g_135 = 22; (g_135 < (-9)); --g_135)
        { /* block id: 207 */
            int64_t l_596[5] = {0xA98CAFE4EF1D1E84LL,0xA98CAFE4EF1D1E84LL,0xA98CAFE4EF1D1E84LL,0xA98CAFE4EF1D1E84LL,0xA98CAFE4EF1D1E84LL};
            int32_t l_597[2];
            uint64_t l_603 = 18446744073709551607UL;
            uint32_t *l_617 = &g_158;
            int i;
            for (i = 0; i < 2; i++)
                l_597[i] = 2L;
            for (g_48 = 0; (g_48 <= 7); g_48 += 1)
            { /* block id: 210 */
                uint8_t l_598 = 0x3AL;
                l_598--;
            }
            for (g_175 = 0; (g_175 != 45); ++g_175)
            { /* block id: 215 */
                int32_t *l_604 = (void*)0;
                int8_t *l_634 = &g_43;
                int64_t *l_635[5][8] = {{&g_135,&g_135,&g_108,&g_108,&g_135,&g_135,&g_108,&g_108},{&g_135,&g_135,&g_108,&g_108,&g_135,&g_135,&g_108,&g_108},{&g_135,&g_135,&g_108,&g_108,&g_135,&g_135,&g_108,&g_108},{&g_135,&g_135,&g_108,&g_108,&g_135,&g_135,&g_108,&g_108},{&g_135,&g_135,&g_108,&g_108,&g_135,&g_135,&g_108,&g_108}};
                int i, j;
                l_603 |= ((*l_581) = 6L);
                for (g_91 = 0; (g_91 <= 3); g_91 += 1)
                { /* block id: 220 */
                    int i;
                    for (g_183 = 0; (g_183 <= 7); g_183 += 1)
                    { /* block id: 223 */
                        int64_t *l_611[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_611[i] = &g_58;
                        (*p_71) = ((*p_69) = l_604);
                        (*p_71) = func_29(((safe_div_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u((g_414[g_91] != (((*l_581) = (1L == (safe_lshift_func_int64_t_s_s((g_108 = (g_58 = (g_414[(g_91 + 2)] == g_164))), (0xC1L | (safe_mod_func_uint16_t_u_u((0xE4L & (safe_add_func_int8_t_s_s(g_420.f0, ((void*)0 == &p_70)))), g_449[1][0]))))))) , (*l_581))), 7L)), l_588[0][0][3])) , 0xAA16ABF18609B40ALL), &l_597[0], &g_91, (*g_350));
                        if ((**p_71))
                            break;
                        if ((*g_360))
                            break;
                    }
                    if (g_175)
                        goto lbl_616;
                    (*p_71) = &l_359[0];
                    (**p_71) &= (-8L);
                }
                (*l_581) &= (l_617 != (void*)0);
                (*p_71) = func_29((safe_unary_minus_func_int16_t_s(((safe_add_func_uint8_t_u_u((g_199 , (((l_621 || (&g_135 == (void*)0)) <= p_70) , (safe_add_func_uint16_t_u_u(((*g_189) &= ((safe_unary_minus_func_int8_t_s(((safe_mul_func_int16_t_s_s(0x0554L, l_596[0])) != (g_542 = ((safe_mod_func_int64_t_s_s(((~((safe_add_func_int16_t_s_s((((*l_634) ^= ((g_449[4][2] , ((safe_lshift_func_int8_t_s_s(g_41, g_108)) >= 0xE5L)) & 4294967291UL)) && p_70), 5L)) != 0xA5DDB566800675CDLL)) ^ 5UL), l_603)) & (**g_217)))))) >= 0x46L)), p_70)))), l_597[1])) == p_70))), &g_6, l_579, p_70);
            }
            return p_70;
        }
        if ((g_636[3] , (((g_48 , l_637[3][4][3]) != (((safe_mod_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(((!(p_70 || (g_183 || (safe_lshift_func_uint8_t_u_s((l_359[5] = (l_591 | g_542)), 3))))) < ((safe_add_func_int32_t_s_s((((*l_651) ^= (safe_rshift_func_uint8_t_u_s((*g_350), ((((safe_lshift_func_int8_t_s_s((0x09C80E5A8A937E8ALL == 18446744073709551612UL), p_70)) & 1UL) , l_307) | l_450)))) < l_587), 0xD72196E3L)) > 0UL)), 0x105920C2L)), l_586[6])) < p_70) , &g_414[0])) < l_588[0][0][1])))
        { /* block id: 247 */
            for (g_58 = 0; (g_58 <= 23); g_58++)
            { /* block id: 250 */
                int16_t l_658 = 9L;
                int32_t l_666 = (-2L);
                for (g_132 = 0; (g_132 <= 23); g_132 = safe_add_func_uint8_t_u_u(g_132, 1))
                { /* block id: 253 */
                    int64_t * const l_665 = &g_58;
                    g_48 &= ((safe_sub_func_uint32_t_u_u(l_658, (safe_mod_func_int32_t_s_s((((*l_579) ^= l_578) , (safe_add_func_uint64_t_u_u(p_70, ((p_70 , 0xA2L) != (*g_350))))), (safe_rshift_func_int16_t_s_u((l_666 &= (l_665 != (void*)0)), ((-4L) >= p_70))))))) , (*g_209));
                    g_667 = g_283;
                    (*p_71) = &l_359[3];
                }
                if ((**p_71))
                    continue;
            }
        }
        else
        { /* block id: 262 */
            for (l_307 = 0; (l_307 < 19); l_307++)
            { /* block id: 265 */
                int16_t l_674 = 3L;
                int32_t *l_684 = &l_578;
                (*p_69) = func_29((((safe_div_func_uint8_t_u_u((safe_div_func_uint32_t_u_u((&g_218[0][9] != (void*)0), (l_674 , ((safe_mul_func_int64_t_s_s(g_58, ((safe_add_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u(((safe_sub_func_uint16_t_u_u(((g_683 , &l_651) != &l_651), 0x4474L)) || g_197), 1)), p_70)) == 7UL))) , 0x543A167AL)))), 0xDAL)) > 0xD152L) > g_228), (*p_69), l_684, g_685);
            }
            for (g_183 = 0; g_183 < 8; g_183 += 1)
            {
                l_359[g_183] = 0x9B8029C4L;
            }
        }
    }
    l_692--;
    l_696++;
    return p_70;
}


/* ------------------------------------------ */
/* 
 * reads : g_183 g_241 g_211 g_43 g_4 g_132 g_153 g_154 g_199 g_197 g_48 g_158 g_58 g_283
 * writes: g_183 g_158 g_241 g_296 g_211
 */
static int32_t ** func_72(uint32_t  p_73, int32_t * p_74)
{ /* block id: 82 */
    uint8_t *l_236[1];
    int32_t l_242 = 6L;
    uint32_t *l_250 = &g_158;
    int32_t l_259[3][2][6] = {{{0L,9L,0x9B35F8E2L,0L,0x9B35F8E2L,9L},{0L,9L,(-1L),0L,0x9B35F8E2L,0x9B35F8E2L}},{{0x446F3898L,9L,9L,0x446F3898L,0x9B35F8E2L,(-1L)},{0L,9L,0x9B35F8E2L,0L,0x9B35F8E2L,9L}},{{0L,9L,(-1L),0L,0x9B35F8E2L,0x9B35F8E2L},{0x446F3898L,9L,9L,0x446F3898L,0x9B35F8E2L,(-1L)}}};
    int64_t *l_262 = (void*)0;
    int64_t *l_263[4][1][9] = {{{&g_135,&g_135,&g_58,&g_58,&g_135,&g_135,&g_58,&g_58,&g_135}},{{(void*)0,&g_135,(void*)0,&g_135,(void*)0,&g_135,(void*)0,&g_135,(void*)0}},{{&g_135,&g_58,&g_58,&g_135,&g_135,&g_58,&g_58,&g_135,&g_135}},{{&g_58,&g_135,&g_58,&g_135,&g_58,&g_135,&g_58,&g_135,&g_58}}};
    uint8_t *l_272 = (void*)0;
    int32_t l_281 = 0x9E0C19E0L;
    int32_t l_282 = (-6L);
    int64_t **l_294 = &l_263[3][0][8];
    int64_t **l_295 = &l_262;
    uint8_t l_299[9] = {0xB4L,0xB4L,0xB4L,0xB4L,0xB4L,0xB4L,0xB4L,0xB4L,0xB4L};
    int16_t *l_300 = (void*)0;
    int16_t *l_301[5][10] = {{&g_164,&g_199,&g_199,&g_164,&g_199,&g_199,&g_164,&g_199,&g_164,&g_199},{&g_199,&g_164,&g_199,&g_164,&g_199,&g_199,&g_164,&g_199,&g_164,&g_199},{&g_199,&g_164,&g_199,&g_164,&g_199,&g_199,&g_164,&g_199,&g_164,&g_199},{&g_199,&g_164,&g_199,&g_164,&g_199,&g_199,&g_164,&g_199,&g_164,&g_199},{&g_199,&g_164,&g_199,&g_164,&g_199,&g_199,&g_164,&g_199,&g_164,&g_199}};
    int32_t **l_302 = &g_211;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_236[i] = &g_183;
    l_259[1][1][2] &= (safe_rshift_func_int16_t_s_s((safe_mod_func_int64_t_s_s(((safe_mod_func_int16_t_s_s((((((g_183--) , (safe_lshift_func_uint16_t_u_s((g_241 , l_242), (safe_div_func_uint8_t_u_u((safe_unary_minus_func_int8_t_s(((safe_lshift_func_uint8_t_u_s(3UL, (((safe_div_func_uint32_t_u_u(((*l_250) = p_73), (safe_lshift_func_uint32_t_u_s(((g_211 != p_74) > ((((safe_sub_func_uint64_t_u_u(l_242, (((((((((safe_div_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_s(g_43, 0)) > (*g_211)), g_132)) > p_73) != 4294967295UL) <= 18446744073709551615UL) | p_73) ^ g_43) <= 18446744073709551615UL) , 0UL) && 0x5982L))) , (*g_153)) | p_73) , 0xF5F8L)), 2)))) == (*p_74)) && g_183))) || (-1L)))), 0x87L))))) , p_73) , 0xB3L) != l_242), g_199)) || p_73), 8L)), p_73));
    l_282 |= ((safe_mod_func_int64_t_s_s(((l_259[2][0][3] = ((*g_211) | g_197)) != (safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((((safe_lshift_func_int8_t_s_u((&g_183 == l_272), (safe_div_func_int16_t_s_s(g_48, ((safe_add_func_int32_t_s_s((safe_mod_func_int16_t_s_s((l_242 || (((safe_rshift_func_int16_t_s_u(((l_242 = ((g_154 , g_158) , 1L)) >= p_73), 11)) | (*p_74)) , 0x4C6DL)), l_281)), l_281)) && 0xDD6CL))))) == l_281) && p_73) && 4UL), 3)), l_281)), g_58))), l_281)) | l_281);
    g_241 = g_283;
    (*l_302) = ((safe_add_func_int64_t_s_s((safe_mul_func_int8_t_s_s(((0x7014L < (g_4 , ((p_73 , (l_281 |= (safe_add_func_uint8_t_u_u((((safe_div_func_int8_t_s_s((((((p_73 != ((((*l_294) = (void*)0) != (g_296 = ((*l_295) = l_262))) <= ((!(((~0xDE2AL) == (l_259[0][0][5] & l_299[4])) == 65535UL)) | 0x2550B3B61917E9C4LL))) , 0xA821B95DFF0F661DLL) && p_73) & p_73) != (-1L)), l_282)) > 1L) < 0x2AAD3B0C1B5C86E0LL), l_299[4])))) != 0xD723L))) && (*p_74)), 0xBCL)), l_299[5])) , p_74);
    return l_302;
}


/* ------------------------------------------ */
/* 
 * reads : g_41 g_90 g_58 g_4 g_108 g_91 g_2 g_135 g_6 g_132 g_142 g_153 g_158 g_179 g_183 g_175 g_187 g_201 g_209 g_199 g_217 g_218 g_197 g_196
 * writes: g_91 g_58 g_108 g_43 g_135 g_132 g_158 g_164 g_154 g_183 g_188 g_201 g_199
 */
static int32_t  func_86(int32_t * p_87, int32_t  p_88)
{ /* block id: 24 */
    int64_t l_116 = (-3L);
    int32_t l_138 = 5L;
    int8_t *l_149 = (void*)0;
    int32_t *l_215 = &g_91;
    int16_t l_216 = (-1L);
    int16_t *l_220 = &g_164;
    int16_t **l_219 = &l_220;
    const uint16_t ** const l_223[1][1] = {{(void*)0}};
    const uint16_t ** const *l_222[4] = {&l_223[0][0],&l_223[0][0],&l_223[0][0],&l_223[0][0]};
    int i, j;
    (*g_90) = g_41;
    for (g_58 = 0; (g_58 >= (-7)); g_58 = safe_sub_func_uint16_t_u_u(g_58, 3))
    { /* block id: 28 */
        uint64_t l_96 = 18446744073709551610UL;
        int64_t *l_113 = &g_108;
        int32_t l_133 = 0x7EAB3286L;
        int8_t *l_152 = (void*)0;
        uint16_t *l_174 = &g_175;
        uint16_t ** const l_186[1][9][3] = {{{(void*)0,&l_174,(void*)0},{&l_174,&l_174,(void*)0},{&l_174,&l_174,&l_174},{&l_174,&l_174,&l_174},{(void*)0,&l_174,&l_174},{&l_174,&l_174,&l_174},{&l_174,&l_174,&l_174},{&l_174,&l_174,&l_174},{(void*)0,&l_174,&l_174}}};
        int i, j, k;
        for (g_91 = (-19); (g_91 < 7); g_91 = safe_add_func_uint16_t_u_u(g_91, 9))
        { /* block id: 31 */
            int64_t *l_107 = &g_108;
            int32_t *l_129 = (void*)0;
            int32_t *l_130 = (void*)0;
            int32_t *l_131[3][7][4] = {{{&g_4,&g_91,&g_4,&g_6},{&g_6,&g_6,&g_132,&g_6},{&g_132,&g_6,&g_6,&g_6},{&g_6,&g_91,&g_6,&g_6},{&g_132,&g_132,&g_132,&g_4},{&g_6,&g_132,&g_4,&g_6},{&g_4,&g_6,&g_6,&g_4}},{{&g_132,&g_6,&g_132,&g_6},{&g_6,&g_132,&g_6,&g_4},{&g_132,&g_132,&g_6,&g_6},{&g_6,&g_91,&g_132,&g_6},{&g_6,&g_6,&g_6,&g_6},{&g_132,&g_6,&g_6,&g_6},{&g_6,&g_91,&g_132,&g_4}},{{&g_132,&g_132,&g_6,&g_4},{&g_4,&g_91,&g_4,&g_6},{&g_6,&g_6,&g_132,&g_6},{&g_132,&g_6,&g_6,&g_6},{&g_6,&g_91,&g_6,&g_6},{&g_132,&g_132,&g_132,&g_4},{&g_6,&g_132,&g_4,&g_6}}};
            int8_t *l_134 = &g_43;
            uint32_t *l_157 = &g_158;
            int16_t *l_193[5][2] = {{&g_164,&g_164},{&g_164,&g_164},{&g_164,&g_164},{&g_164,&g_164},{&g_164,&g_164}};
            int i, j, k;
            g_132 |= ((l_96 <= (safe_add_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_u((safe_sub_func_uint64_t_u_u((((*p_87) && ((((*l_107) = (safe_rshift_func_int32_t_s_u((-3L), 9))) || (safe_mul_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s(((((p_88 | 0xE0L) & ((l_113 = (void*)0) != (void*)0)) || (safe_div_func_int8_t_s_s((g_135 &= (l_116 , (safe_mod_func_int64_t_s_s((g_108 = ((((*l_134) = (safe_rshift_func_uint64_t_u_u((safe_div_func_int32_t_s_s((l_133 = ((safe_add_func_int32_t_s_s((safe_mod_func_uint64_t_u_u((safe_div_func_uint16_t_u_u(p_88, g_108)), p_88)), (-4L))) && (-1L))), p_88)), g_91))) & 253UL) | l_116)), g_2)))), l_96))) , l_133), g_58)), 1UL))) , l_116)) != 0UL), g_6)), g_4)) , 0x73L), g_91))) > g_58);
            for (g_108 = (-15); (g_108 == (-3)); g_108++)
            { /* block id: 41 */
                uint16_t l_139[6];
                int32_t *l_143 = (void*)0;
                uint32_t l_144[5];
                int i;
                for (i = 0; i < 6; i++)
                    l_139[i] = 65526UL;
                for (i = 0; i < 5; i++)
                    l_144[i] = 2UL;
                --l_139[3];
                l_143 = (g_142 , &g_6);
                l_138 ^= l_144[2];
            }
            l_138 = (safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_u(((&g_43 == l_149) != (safe_mod_func_uint32_t_u_u(((*l_157) |= ((&g_43 == l_152) <= ((g_153 == &g_154) < ((p_88 , ((safe_add_func_uint32_t_u_u(p_88, 0xE71BBE26L)) , p_88)) , 3L)))), (*g_90)))), 3)), p_88));
            for (l_96 = (-4); (l_96 != 25); ++l_96)
            { /* block id: 50 */
                int16_t *l_163 = &g_164;
                uint32_t l_171 = 0x2702ECBAL;
                uint16_t *l_176[7];
                uint8_t *l_182 = &g_183;
                int i;
                for (i = 0; i < 7; i++)
                    l_176[i] = (void*)0;
                g_132 &= (safe_lshift_func_uint64_t_u_s((((*l_163) = p_88) > ((*g_153) = p_88)), ((safe_div_func_uint16_t_u_u((safe_rshift_func_uint64_t_u_u(((((safe_mod_func_int16_t_s_s(l_171, (safe_lshift_func_uint64_t_u_u((l_174 != (l_176[0] = l_174)), 30)))) <= (safe_sub_func_uint8_t_u_u(((l_116 == ((*l_182) |= (g_179 , (safe_mod_func_uint64_t_u_u(l_171, ((0x54D72C3A22099EBFLL > l_171) | 0xA75DB76206065F2BLL)))))) , 0x42L), g_175))) , p_88) , l_116), g_108)), 0x6111L)) || g_58)));
                for (g_183 = 0; (g_183 > 46); g_183 = safe_add_func_int64_t_s_s(g_183, 7))
                { /* block id: 58 */
                    int16_t l_194 = 0x1F1EL;
                    int32_t l_195 = 0x82D9615DL;
                    (*g_187) = l_186[0][6][2];
                    for (g_132 = 0; (g_132 <= 6); g_132 += 1)
                    { /* block id: 62 */
                        int16_t **l_192 = &l_163;
                        int i;
                        l_194 ^= (safe_mul_func_uint8_t_u_u(g_183, (((*l_192) = l_176[g_132]) == l_193[2][1])));
                    }
                    g_201[4]++;
                }
            }
        }
        (*g_209) ^= (safe_add_func_int16_t_s_s((0L != (safe_div_func_int64_t_s_s((-1L), 0xB34E314DBE924755LL))), (+p_88)));
        p_87 = p_87;
    }
    for (g_199 = 0; (g_199 <= 2); g_199 = safe_add_func_uint64_t_u_u(g_199, 9))
    { /* block id: 75 */
        uint16_t ***l_221 = (void*)0;
        int64_t *l_226 = &g_135;
        const int64_t *l_227 = &g_228;
        int32_t l_229 = 0x5A46787CL;
        l_215 = &l_138;
        (*l_215) = ((&g_188 != (l_216 , &g_188)) , ((((g_217 == l_219) , l_221) == l_222[2]) < ((1L != 6L) && (**g_217))));
        (*l_215) = (g_197 ^ (safe_lshift_func_int16_t_s_s((l_226 != (((((&l_116 != (l_227 = &g_108)) || (*l_215)) & l_229) == ((*l_215) | (((p_88 ^ 0xAB333187L) , 1UL) != l_229))) , (void*)0)), 6)));
    }
    return (*g_90);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_37, "g_37", print_hash_value);
    transparent_crc(g_41, "g_41", print_hash_value);
    transparent_crc(g_43, "g_43", print_hash_value);
    transparent_crc(g_48, "g_48", print_hash_value);
    transparent_crc(g_58, "g_58", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_61[i][j], "g_61[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_91, "g_91", print_hash_value);
    transparent_crc(g_108, "g_108", print_hash_value);
    transparent_crc(g_132, "g_132", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_142.f0, "g_142.f0", print_hash_value);
    transparent_crc(g_154, "g_154", print_hash_value);
    transparent_crc(g_158, "g_158", print_hash_value);
    transparent_crc(g_164, "g_164", print_hash_value);
    transparent_crc(g_175, "g_175", print_hash_value);
    transparent_crc(g_179.f0, "g_179.f0", print_hash_value);
    transparent_crc(g_183, "g_183", print_hash_value);
    transparent_crc(g_196, "g_196", print_hash_value);
    transparent_crc(g_197, "g_197", print_hash_value);
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_199, "g_199", print_hash_value);
    transparent_crc(g_200, "g_200", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_201[i], "g_201[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_228, "g_228", print_hash_value);
    transparent_crc(g_241.f0, "g_241.f0", print_hash_value);
    transparent_crc(g_283.f0, "g_283.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_414[i], "g_414[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_420.f0, "g_420.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_449[i][j], "g_449[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_498[i][j][k].f0, "g_498[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_537[i], "g_537[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_542, "g_542", print_hash_value);
    transparent_crc(g_546.f0, "g_546.f0", print_hash_value);
    transparent_crc(g_573.f0, "g_573.f0", print_hash_value);
    transparent_crc(g_583, "g_583", print_hash_value);
    transparent_crc(g_585, "g_585", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_636[i].f0, "g_636[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_667.f0, "g_667.f0", print_hash_value);
    transparent_crc(g_683.f0, "g_683.f0", print_hash_value);
    transparent_crc(g_685, "g_685", print_hash_value);
    transparent_crc(g_702, "g_702", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_748[i], "g_748[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_835.f0, "g_835.f0", print_hash_value);
    transparent_crc(g_838.f0, "g_838.f0", print_hash_value);
    transparent_crc(g_895.f0, "g_895.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_976[i][j][k], "g_976[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_979.f0, "g_979.f0", print_hash_value);
    transparent_crc(g_1053.f0, "g_1053.f0", print_hash_value);
    transparent_crc(g_1162.f0, "g_1162.f0", print_hash_value);
    transparent_crc(g_1201.f0, "g_1201.f0", print_hash_value);
    transparent_crc(g_1242.f0, "g_1242.f0", print_hash_value);
    transparent_crc(g_1257.f0, "g_1257.f0", print_hash_value);
    transparent_crc(g_1285.f0, "g_1285.f0", print_hash_value);
    transparent_crc(g_1315.f0, "g_1315.f0", print_hash_value);
    transparent_crc(g_1354, "g_1354", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1356[i][j][k], "g_1356[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1396[i], "g_1396[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1398, "g_1398", print_hash_value);
    transparent_crc(g_1432.f0, "g_1432.f0", print_hash_value);
    transparent_crc(g_1443.f0, "g_1443.f0", print_hash_value);
    transparent_crc(g_1583, "g_1583", print_hash_value);
    transparent_crc(g_1588.f0, "g_1588.f0", print_hash_value);
    transparent_crc(g_1617.f0, "g_1617.f0", print_hash_value);
    transparent_crc(g_1642, "g_1642", print_hash_value);
    transparent_crc(g_1723.f0, "g_1723.f0", print_hash_value);
    transparent_crc(g_1747.f0, "g_1747.f0", print_hash_value);
    transparent_crc(g_1755, "g_1755", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1771[i].f0, "g_1771[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1776.f0, "g_1776.f0", print_hash_value);
    transparent_crc(g_1777.f0, "g_1777.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1788[i].f0, "g_1788[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1789, "g_1789", print_hash_value);
    transparent_crc(g_1829.f0, "g_1829.f0", print_hash_value);
    transparent_crc(g_1848.f0, "g_1848.f0", print_hash_value);
    transparent_crc(g_1877.f0, "g_1877.f0", print_hash_value);
    transparent_crc(g_1880.f0, "g_1880.f0", print_hash_value);
    transparent_crc(g_1983.f0, "g_1983.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2014[i][j][k].f0, "g_2014[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2016[i][j].f0, "g_2016[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2019.f0, "g_2019.f0", print_hash_value);
    transparent_crc(g_2044, "g_2044", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2085[i].f0, "g_2085[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2087.f0, "g_2087.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2088[i][j].f0, "g_2088[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2112[i], "g_2112[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2172.f0, "g_2172.f0", print_hash_value);
    transparent_crc(g_2175, "g_2175", print_hash_value);
    transparent_crc(g_2187.f0, "g_2187.f0", print_hash_value);
    transparent_crc(g_2224, "g_2224", print_hash_value);
    transparent_crc(g_2321.f0, "g_2321.f0", print_hash_value);
    transparent_crc(g_2366.f0, "g_2366.f0", print_hash_value);
    transparent_crc(g_2396, "g_2396", print_hash_value);
    transparent_crc(g_2414, "g_2414", print_hash_value);
    transparent_crc(g_2458, "g_2458", print_hash_value);
    transparent_crc(g_2510, "g_2510", print_hash_value);
    transparent_crc(g_2520.f0, "g_2520.f0", print_hash_value);
    transparent_crc(g_2540.f0, "g_2540.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2592[i][j][k].f0, "g_2592[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2675.f0, "g_2675.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_2680[i][j][k].f0, "g_2680[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2681.f0, "g_2681.f0", print_hash_value);
    transparent_crc(g_2692.f0, "g_2692.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_2704[i][j][k].f0, "g_2704[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_2732[i][j], "g_2732[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2753, "g_2753", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2803[i], "g_2803[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2851.f0, "g_2851.f0", print_hash_value);
    transparent_crc(g_2897.f0, "g_2897.f0", print_hash_value);
    transparent_crc(g_3003, "g_3003", print_hash_value);
    transparent_crc(g_3004.f0, "g_3004.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 678
   depth: 1, occurrence: 53
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 257
   depth: 2, occurrence: 71
   depth: 3, occurrence: 4
   depth: 4, occurrence: 5
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 3
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 17, occurrence: 5
   depth: 18, occurrence: 1
   depth: 20, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 23, occurrence: 3
   depth: 24, occurrence: 1
   depth: 25, occurrence: 4
   depth: 26, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 3
   depth: 30, occurrence: 2
   depth: 32, occurrence: 2
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 2
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1

XXX total number of pointers: 675

XXX times a variable address is taken: 1457
XXX times a pointer is dereferenced on RHS: 484
breakdown:
   depth: 1, occurrence: 392
   depth: 2, occurrence: 65
   depth: 3, occurrence: 19
   depth: 4, occurrence: 8
XXX times a pointer is dereferenced on LHS: 401
breakdown:
   depth: 1, occurrence: 382
   depth: 2, occurrence: 16
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 56
XXX times a pointer is compared with address of another variable: 24
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 10205

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2075
   level: 2, occurrence: 359
   level: 3, occurrence: 88
   level: 4, occurrence: 28
   level: 5, occurrence: 5
XXX number of pointers point to pointers: 266
XXX number of pointers point to scalars: 366
XXX number of pointers point to structs: 43
XXX percent of pointers has null in alias set: 29
XXX average alias set size: 1.53

XXX times a non-volatile is read: 2405
XXX times a non-volatile is write: 1115
XXX times a volatile is read: 191
XXX    times read thru a pointer: 46
XXX times a volatile is write: 52
XXX    times written thru a pointer: 7
XXX times a volatile is available for access: 8.05e+03
XXX percentage of non-volatile access: 93.5

XXX forward jumps: 1
XXX backward jumps: 12

XXX stmts: 253
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 34
   depth: 2, occurrence: 44
   depth: 3, occurrence: 50
   depth: 4, occurrence: 55
   depth: 5, occurrence: 37

XXX percentage a fresh-made variable is used: 17.1
XXX percentage an existing variable is used: 82.9
XXX total OOB instances added: 0
********************* end of statistics **********************/


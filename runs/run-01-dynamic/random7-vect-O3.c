/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3103244950
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   int8_t  f0;
   int32_t  f1;
   volatile int8_t  f2;
   uint64_t  f3;
   volatile uint32_t  f4;
   const volatile uint32_t  f5;
};
#pragma pack(pop)

union U1 {
   int32_t  f0;
   volatile uint8_t  f1;
   const uint16_t  f2;
   int16_t  f3;
};

union U2 {
   unsigned f0 : 6;
   struct S0  f1;
   int32_t  f2;
   unsigned f3 : 17;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_11 = 0x32FD2D1AL;
static uint64_t g_21 = 0UL;
static union U2 g_26 = {1UL};/* VOLATILE GLOBAL g_26 */
static volatile int32_t g_29 = 0x38DB075AL;/* VOLATILE GLOBAL g_29 */
static volatile int64_t g_41 = 8L;/* VOLATILE GLOBAL g_41 */
static volatile int32_t g_42 = (-1L);/* VOLATILE GLOBAL g_42 */
static int16_t g_43 = 0x81A6L;
static volatile int32_t g_44 = (-9L);/* VOLATILE GLOBAL g_44 */
static int32_t g_78 = 0x212E26E4L;
static union U1 g_79 = {0L};/* VOLATILE GLOBAL g_79 */
static int32_t *g_83 = &g_78;
static uint32_t g_93 = 4UL;
static uint8_t g_94 = 255UL;
static uint64_t g_109 = 0x2924758F72B24498LL;
static int8_t g_126 = 0xC6L;
static int32_t **g_132 = &g_83;
static int32_t ***g_131 = &g_132;
static int32_t **** volatile g_130 = &g_131;/* VOLATILE GLOBAL g_130 */
static const int64_t g_145 = 0xED5B990560A05798LL;
static union U1 g_148 = {0xF8E74D01L};/* VOLATILE GLOBAL g_148 */
static uint8_t g_190 = 0xD2L;
static int64_t g_200 = 0L;
static struct S0 g_204 = {0x61L,0xB3A83FA9L,-3L,18446744073709551615UL,7UL,0UL};/* VOLATILE GLOBAL g_204 */
static struct S0 *g_206 = &g_204;
static struct S0 ** volatile g_205 = &g_206;/* VOLATILE GLOBAL g_205 */
static uint32_t * volatile g_212[2][5][5] = {{{(void*)0,&g_93,&g_93,&g_93,&g_93},{&g_93,(void*)0,&g_93,&g_93,(void*)0},{(void*)0,&g_93,&g_93,&g_93,&g_93},{&g_93,(void*)0,&g_93,&g_93,(void*)0},{(void*)0,&g_93,&g_93,&g_93,&g_93}},{{&g_93,(void*)0,&g_93,&g_93,(void*)0},{(void*)0,&g_93,&g_93,&g_93,&g_93},{&g_93,(void*)0,&g_93,&g_93,(void*)0},{(void*)0,&g_93,&g_93,&g_93,&g_93},{&g_93,&g_93,(void*)0,(void*)0,&g_93}}};
static uint32_t * volatile * volatile g_211 = &g_212[0][4][0];/* VOLATILE GLOBAL g_211 */
static uint8_t g_218 = 0x16L;
static volatile union U1 g_224 = {-3L};/* VOLATILE GLOBAL g_224 */
static volatile union U2 g_225 = {0xDB485D11L};/* VOLATILE GLOBAL g_225 */
static int32_t g_231[4] = {(-1L),(-1L),(-1L),(-1L)};
static union U2 g_265 = {0xB146E210L};/* VOLATILE GLOBAL g_265 */
static struct S0 g_300 = {8L,0xC79D939CL,0x61L,1UL,0x0E8CCE15L,0xEA9A78E2L};/* VOLATILE GLOBAL g_300 */
static const struct S0 *g_299[4][2] = {{&g_300,&g_300},{&g_300,&g_300},{&g_300,&g_300},{&g_300,&g_300}};
static const struct S0 **g_298 = &g_299[3][0];
static uint16_t g_331 = 0x0582L;
static volatile int32_t * volatile g_337 = (void*)0;/* VOLATILE GLOBAL g_337 */
static volatile int32_t * volatile *g_336 = &g_337;
static volatile int32_t * volatile **g_335 = &g_336;
static volatile int32_t * volatile ** volatile *g_334 = &g_335;
static volatile int32_t * volatile ** volatile * volatile *g_333 = &g_334;
static struct S0 g_340 = {0xE6L,0xD2A5B001L,0x80L,1UL,0xA22073FCL,18446744073709551613UL};/* VOLATILE GLOBAL g_340 */
static uint16_t g_343[5][3] = {{0x66CEL,0x66CEL,0x66CEL},{0x66CEL,0x66CEL,0x66CEL},{0x66CEL,0x66CEL,0x66CEL},{0x66CEL,0x66CEL,0x66CEL},{0x66CEL,0x66CEL,0x66CEL}};
static struct S0 g_383 = {-1L,-1L,-6L,0xA3A408A126E37BDELL,0xD24434F7L,1UL};/* VOLATILE GLOBAL g_383 */
static volatile uint32_t g_386 = 0xDB0300A9L;/* VOLATILE GLOBAL g_386 */
static volatile uint32_t *g_385 = &g_386;
static volatile uint32_t **g_384 = &g_385;
static volatile union U2 * volatile *g_404 = (void*)0;
static int64_t g_442 = (-3L);
static union U2 g_482 = {0x9DCB5690L};/* VOLATILE GLOBAL g_482 */
static union U1 g_491 = {7L};/* VOLATILE GLOBAL g_491 */
static union U1 *g_490 = &g_491;
static union U1 g_493 = {-1L};/* VOLATILE GLOBAL g_493 */
static uint32_t *g_551 = &g_93;
static uint32_t **g_550 = &g_551;
static volatile uint32_t *g_556 = (void*)0;
static volatile uint32_t * volatile *g_555[4][1] = {{&g_556},{&g_556},{&g_556},{&g_556}};
static volatile uint32_t * volatile ** volatile g_557 = (void*)0;/* VOLATILE GLOBAL g_557 */
static const int32_t *g_581 = &g_204.f1;
static const int32_t **g_580[2][4] = {{(void*)0,&g_581,(void*)0,(void*)0},{&g_581,&g_581,&g_581,&g_581}};
static const int32_t ***g_579[10] = {&g_580[1][2],(void*)0,&g_580[1][2],(void*)0,&g_580[1][2],(void*)0,&g_580[1][2],(void*)0,&g_580[1][2],(void*)0};
static union U1 g_585 = {-2L};/* VOLATILE GLOBAL g_585 */
static uint32_t g_594[8] = {0xC579BE28L,0xF308F430L,0xC579BE28L,0xC579BE28L,0xF308F430L,0xC579BE28L,0xC579BE28L,0xF308F430L};
static int64_t *g_611 = &g_442;
static int64_t **g_610 = &g_611;
static int64_t g_632[9] = {0xEEF940EDE54D4107LL,0L,0xEEF940EDE54D4107LL,0L,0xEEF940EDE54D4107LL,0L,0xEEF940EDE54D4107LL,0L,0xEEF940EDE54D4107LL};
static volatile union U1 g_651 = {0xC5E00063L};/* VOLATILE GLOBAL g_651 */
static uint32_t ***g_676[7][6] = {{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550},{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550},{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550},{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550},{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550},{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550},{&g_550,&g_550,&g_550,&g_550,&g_550,&g_550}};
static union U2 g_681 = {1UL};/* VOLATILE GLOBAL g_681 */
static int32_t g_687 = 0x949DD7A9L;
static struct S0 g_696 = {1L,0L,3L,2UL,0UL,0x41DE1768L};/* VOLATILE GLOBAL g_696 */
static const struct S0 g_723 = {0x70L,-8L,0xA3L,18446744073709551609UL,0xF7746837L,0x62705B55L};/* VOLATILE GLOBAL g_723 */
static union U2 **g_778 = (void*)0;
static union U2 ***g_777[6][8] = {{&g_778,&g_778,&g_778,(void*)0,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,(void*)0,(void*)0,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,(void*)0,&g_778},{&g_778,&g_778,&g_778,(void*)0,&g_778,&g_778,&g_778,&g_778}};
static union U2 **** volatile g_776 = &g_777[2][2];/* VOLATILE GLOBAL g_776 */
static union U1 g_791 = {0L};/* VOLATILE GLOBAL g_791 */
static union U1 g_830 = {-1L};/* VOLATILE GLOBAL g_830 */
static union U1 g_832 = {0xAFA3ED51L};/* VOLATILE GLOBAL g_832 */
static const volatile union U1 g_861 = {4L};/* VOLATILE GLOBAL g_861 */
static volatile union U2 g_953 = {3UL};/* VOLATILE GLOBAL g_953 */
static volatile struct S0 g_979 = {-1L,0x5516C689L,-1L,18446744073709551611UL,18446744073709551615UL,18446744073709551608UL};/* VOLATILE GLOBAL g_979 */
static union U1 g_986 = {1L};/* VOLATILE GLOBAL g_986 */
static union U1 g_995 = {7L};/* VOLATILE GLOBAL g_995 */
static union U2 **** volatile g_1000 = (void*)0;/* VOLATILE GLOBAL g_1000 */
static union U2 **** volatile g_1001 = (void*)0;/* VOLATILE GLOBAL g_1001 */
static const uint32_t *g_1024 = (void*)0;
static const uint32_t **g_1023 = &g_1024;
static union U1 g_1048[2][3] = {{{0x56A1382BL},{0x56A1382BL},{0x56A1382BL}},{{-1L},{-1L},{-1L}}};
static const volatile struct S0 g_1064 = {0xD1L,0x79954871L,1L,1UL,0xDC3F71E2L,0UL};/* VOLATILE GLOBAL g_1064 */
static volatile union U1 g_1123 = {0xCB2758F6L};/* VOLATILE GLOBAL g_1123 */
static volatile union U1 *g_1122 = &g_1123;
static volatile union U1 ** volatile g_1121[1][10][3] = {{{&g_1122,&g_1122,&g_1122},{&g_1122,(void*)0,&g_1122},{&g_1122,(void*)0,&g_1122},{&g_1122,&g_1122,&g_1122},{&g_1122,&g_1122,&g_1122},{&g_1122,&g_1122,&g_1122},{&g_1122,(void*)0,&g_1122},{&g_1122,(void*)0,&g_1122},{&g_1122,&g_1122,&g_1122},{&g_1122,&g_1122,&g_1122}}};
static volatile union U1 ** volatile *g_1120 = &g_1121[0][0][2];
static int16_t *g_1141 = &g_830.f3;
static int16_t **g_1140 = &g_1141;
static int16_t g_1146 = (-3L);
static int16_t * const g_1145 = &g_1146;
static int16_t * const *g_1144 = &g_1145;
static struct S0 g_1156 = {0x80L,0x0FCDF7FAL,0xBEL,0x05E7C371584F6C60LL,0UL,0x63E95B70L};/* VOLATILE GLOBAL g_1156 */
static int32_t g_1159 = 5L;
static union U1 g_1161 = {8L};/* VOLATILE GLOBAL g_1161 */
static int32_t *g_1172 = &g_383.f1;
static int32_t ** volatile g_1171 = &g_1172;/* VOLATILE GLOBAL g_1171 */
static volatile uint16_t g_1179 = 0x0B0FL;/* VOLATILE GLOBAL g_1179 */
static volatile uint16_t *g_1178 = &g_1179;
static volatile uint16_t * volatile *g_1177 = &g_1178;
static volatile uint16_t * volatile ** volatile g_1180 = &g_1177;/* VOLATILE GLOBAL g_1180 */
static uint32_t ** volatile *g_1219 = &g_550;
static uint32_t ** volatile * const *g_1218 = &g_1219;
static volatile struct S0 g_1228 = {1L,1L,0x86L,0xE5DCE5DCF205B0E3LL,0x23359F73L,18446744073709551612UL};/* VOLATILE GLOBAL g_1228 */
static union U1 g_1229 = {0xDA0E542BL};/* VOLATILE GLOBAL g_1229 */
static union U2 g_1327 = {0UL};/* VOLATILE GLOBAL g_1327 */
static int64_t * const **g_1440 = (void*)0;
static union U1 g_1496[7][8][1] = {{{{0x2D6312DEL}},{{5L}},{{-1L}},{{5L}},{{0x2D6312DEL}},{{1L}},{{0x51B47A64L}},{{0x35D41C97L}}},{{{0x97A6999CL}},{{-10L}},{{0x97A6999CL}},{{0x35D41C97L}},{{0x51B47A64L}},{{1L}},{{0x2D6312DEL}},{{5L}}},{{{-1L}},{{5L}},{{0x2D6312DEL}},{{1L}},{{0x51B47A64L}},{{0x35D41C97L}},{{0x97A6999CL}},{{-10L}}},{{{0x97A6999CL}},{{0x35D41C97L}},{{0x51B47A64L}},{{1L}},{{0x2D6312DEL}},{{5L}},{{-1L}},{{5L}}},{{{0x2D6312DEL}},{{1L}},{{0x51B47A64L}},{{0x35D41C97L}},{{0x97A6999CL}},{{-10L}},{{0x97A6999CL}},{{0x35D41C97L}}},{{{0x51B47A64L}},{{1L}},{{0x2D6312DEL}},{{5L}},{{-1L}},{{5L}},{{0x2D6312DEL}},{{1L}}},{{{0x51B47A64L}},{{0x35D41C97L}},{{0x97A6999CL}},{{-10L}},{{0x97A6999CL}},{{0x35D41C97L}},{{0x51B47A64L}},{{1L}}}};
static const union U2 *****g_1515 = (void*)0;
static volatile int32_t *g_1530 = &g_44;
static volatile int32_t ** const  volatile g_1529 = &g_1530;/* VOLATILE GLOBAL g_1529 */
static int8_t g_1550 = 0x3FL;
static volatile struct S0 g_1555 = {0x82L,0xA1E8FF81L,0xFBL,18446744073709551615UL,18446744073709551615UL,0xC1BB076FL};/* VOLATILE GLOBAL g_1555 */
static volatile struct S0 g_1562 = {1L,-1L,5L,18446744073709551615UL,0xCC9E1452L,0x8D8DE33FL};/* VOLATILE GLOBAL g_1562 */
static struct S0 g_1572 = {0x4DL,0x8F8DCEE5L,-10L,18446744073709551608UL,18446744073709551609UL,0x11783848L};/* VOLATILE GLOBAL g_1572 */
static volatile union U1 g_1573 = {0x27968C58L};/* VOLATILE GLOBAL g_1573 */
static volatile struct S0 g_1577[9] = {{0xECL,0x6A97197BL,0x41L,0x1E9FFCE965906266LL,0x1FADC4EFL,18446744073709551609UL},{0xECL,0x6A97197BL,0x41L,0x1E9FFCE965906266LL,0x1FADC4EFL,18446744073709551609UL},{-1L,-2L,0L,0UL,1UL,0xF554BF79L},{0xECL,0x6A97197BL,0x41L,0x1E9FFCE965906266LL,0x1FADC4EFL,18446744073709551609UL},{0xECL,0x6A97197BL,0x41L,0x1E9FFCE965906266LL,0x1FADC4EFL,18446744073709551609UL},{-1L,-2L,0L,0UL,1UL,0xF554BF79L},{0xECL,0x6A97197BL,0x41L,0x1E9FFCE965906266LL,0x1FADC4EFL,18446744073709551609UL},{0xECL,0x6A97197BL,0x41L,0x1E9FFCE965906266LL,0x1FADC4EFL,18446744073709551609UL},{-1L,-2L,0L,0UL,1UL,0xF554BF79L}};
static volatile union U1 g_1594 = {1L};/* VOLATILE GLOBAL g_1594 */
static volatile int8_t *g_1606 = &g_1577[8].f0;
static volatile int8_t **g_1605 = &g_1606;
static volatile int8_t *** volatile g_1604[3][8][4] = {{{(void*)0,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{(void*)0,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605}},{{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,(void*)0,&g_1605,&g_1605},{&g_1605,(void*)0,&g_1605,&g_1605},{(void*)0,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605}},{{&g_1605,&g_1605,&g_1605,&g_1605},{(void*)0,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605},{&g_1605,&g_1605,&g_1605,&g_1605}}};
static volatile uint64_t g_1649 = 8UL;/* VOLATILE GLOBAL g_1649 */
static union U2 g_1674 = {0xEB847293L};/* VOLATILE GLOBAL g_1674 */
static union U2 g_1726 = {4294967295UL};/* VOLATILE GLOBAL g_1726 */
static union U2 g_1748 = {0x8B83C1D8L};/* VOLATILE GLOBAL g_1748 */
static uint32_t g_1750 = 1UL;
static union U2 *****g_1784 = (void*)0;
static uint32_t g_1820 = 1UL;
static volatile union U1 g_1831 = {0x47CC4268L};/* VOLATILE GLOBAL g_1831 */
static volatile union U1 g_1842 = {-8L};/* VOLATILE GLOBAL g_1842 */
static uint16_t *g_1854 = (void*)0;
static uint16_t **g_1853[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint16_t ***g_1852[2][6][6] = {{{&g_1853[6],&g_1853[3],&g_1853[0],&g_1853[3],&g_1853[6],&g_1853[0]},{(void*)0,&g_1853[0],&g_1853[3],&g_1853[3],&g_1853[0],(void*)0},{&g_1853[3],&g_1853[5],&g_1853[6],&g_1853[0],(void*)0,&g_1853[0]},{&g_1853[4],&g_1853[6],&g_1853[0],&g_1853[0],&g_1853[6],&g_1853[4]},{&g_1853[3],&g_1853[5],&g_1853[0],&g_1853[3],(void*)0,&g_1853[6]},{&g_1853[5],&g_1853[3],(void*)0,&g_1853[6],&g_1853[4],&g_1853[0]}},{{&g_1853[5],&g_1853[0],&g_1853[6],&g_1853[3],&g_1853[6],&g_1853[0]},{&g_1853[3],(void*)0,(void*)0,&g_1853[0],&g_1853[0],&g_1853[0]},{&g_1853[4],(void*)0,&g_1853[3],(void*)0,&g_1853[0],&g_1853[6]},{&g_1853[0],(void*)0,&g_1853[0],&g_1853[0],&g_1853[0],&g_1853[0]},{(void*)0,(void*)0,(void*)0,&g_1853[0],&g_1853[6],&g_1853[5]},{&g_1853[3],&g_1853[0],&g_1853[0],&g_1853[4],&g_1853[4],(void*)0}}};
static union U2 g_1872 = {0UL};/* VOLATILE GLOBAL g_1872 */
static uint8_t g_1919 = 1UL;
static volatile uint8_t g_1926 = 251UL;/* VOLATILE GLOBAL g_1926 */
static union U1 g_1939 = {-2L};/* VOLATILE GLOBAL g_1939 */
static union U2 g_1945 = {0xC587B906L};/* VOLATILE GLOBAL g_1945 */
static volatile union U1 g_1948[4] = {{8L},{8L},{8L},{8L}};
static uint32_t *g_1990 = (void*)0;
static union U2 g_1998 = {0xC8B5A403L};/* VOLATILE GLOBAL g_1998 */
static const int32_t g_2054 = 0x33AB7C18L;
static union U2 g_2071 = {0xFA1267D6L};/* VOLATILE GLOBAL g_2071 */
static const volatile union U1 g_2072 = {8L};/* VOLATILE GLOBAL g_2072 */
static uint8_t g_2077 = 0UL;
static union U1 g_2081 = {0xD4BFE413L};/* VOLATILE GLOBAL g_2081 */
static union U1 g_2082 = {4L};/* VOLATILE GLOBAL g_2082 */
static union U1 ** volatile g_2109 = &g_490;/* VOLATILE GLOBAL g_2109 */
static int32_t ** volatile g_2129 = &g_1172;/* VOLATILE GLOBAL g_2129 */
static int8_t *g_2137 = &g_1550;
static int32_t * volatile g_2142[6][10] = {{&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0,&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0},{&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0,&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0},{&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0,&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0},{&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0,&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0},{&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0,&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0},{&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0,&g_1048[0][0].f0,(void*)0,&g_11,(void*)0,&g_1048[0][0].f0}};
static volatile union U2 g_2205 = {0x2F8B7297L};/* VOLATILE GLOBAL g_2205 */
static const volatile union U2 g_2218 = {0UL};/* VOLATILE GLOBAL g_2218 */
static int32_t *g_2290 = &g_687;
static int32_t *g_2291 = &g_482.f2;
static volatile union U1 g_2330 = {0x01414C5AL};/* VOLATILE GLOBAL g_2330 */
static uint32_t **g_2338 = &g_1990;
static int8_t ** volatile g_2370 = (void*)0;/* VOLATILE GLOBAL g_2370 */
static int8_t ** volatile *g_2369 = &g_2370;
static int8_t ** volatile * volatile *g_2368 = &g_2369;
static int8_t ** volatile * volatile **g_2367 = &g_2368;
static union U2 g_2387 = {4294967294UL};/* VOLATILE GLOBAL g_2387 */
static union U2 g_2408 = {0x05533571L};/* VOLATILE GLOBAL g_2408 */
static uint32_t g_2417 = 4294967288UL;
static uint32_t g_2444 = 0UL;
static struct S0 g_2514 = {0x4AL,0L,0L,0x58FEE062075E4EBALL,0x9A2C06FEL,0xA54A1519L};/* VOLATILE GLOBAL g_2514 */
static int64_t ** volatile *g_2540 = (void*)0;
static int64_t ** volatile **g_2539 = &g_2540;
static int64_t ** volatile ***g_2538 = &g_2539;
static uint16_t ****g_2547 = (void*)0;
static uint16_t ***** volatile g_2546 = &g_2547;/* VOLATILE GLOBAL g_2546 */
static union U2 g_2551 = {8UL};/* VOLATILE GLOBAL g_2551 */
static union U1 g_2556 = {0xAEDB1328L};/* VOLATILE GLOBAL g_2556 */
static volatile union U2 g_2589 = {0x12201DADL};/* VOLATILE GLOBAL g_2589 */
static uint16_t g_2612[4] = {65528UL,65528UL,65528UL,65528UL};
static const volatile union U2 g_2628 = {0x6CE900BCL};/* VOLATILE GLOBAL g_2628 */
static volatile union U1 g_2633[7][6][6] = {{{{-1L},{0x77D7F2AFL},{0xCF163A15L},{2L},{0x273CE81FL},{0xC86E61B7L}},{{0xE9F13E21L},{2L},{-1L},{0x52728AACL},{-1L},{0x73F68B5DL}},{{0x52728AACL},{-1L},{0x73F68B5DL},{0x6536C7F4L},{-5L},{0x7187E54CL}},{{0xE9F13E21L},{0xACFBA1E8L},{0xFA001688L},{0x7187E54CL},{-1L},{0x190557B0L}},{{-1L},{-5L},{0x6DE086CFL},{-1L},{-1L},{-1L}},{{0x77D7F2AFL},{0xACFBA1E8L},{0x264D0DC1L},{1L},{-5L},{0xFA001688L}}},{{{0x273CE81FL},{-1L},{0xACFBA1E8L},{0xACFBA1E8L},{-1L},{0x273CE81FL}},{{1L},{2L},{0x264D0DC1L},{-1L},{0x273CE81FL},{0xF17B1833L}},{{-1L},{0x77D7F2AFL},{0x6DE086CFL},{0xFA001688L},{0L},{0x77D7F2AFL}},{{-1L},{0xF17B1833L},{0xFA001688L},{-1L},{0x241E11B2L},{2L}},{{1L},{0x273CE81FL},{0x73F68B5DL},{0xACFBA1E8L},{0xC86E61B7L},{-1L}},{{0x273CE81FL},{0xFA001688L},{0x190557B0L},{0x7187E54CL},{0L},{0x73F68B5DL}}},{{{1L},{0x241E11B2L},{0xACFBA1E8L},{0x190557B0L},{0xE9F13E21L},{0x77D7F2AFL}},{{0xC86E61B7L},{0L},{0xACFBA1E8L},{-1L},{-5L},{0x73F68B5DL}},{{-1L},{-1L},{0x190557B0L},{0xFA001688L},{0x52728AACL},{0x52728AACL}},{{0x273CE81FL},{0x6536C7F4L},{0x6536C7F4L},{0x273CE81FL},{0x77D7F2AFL},{0xF17B1833L}},{{-1L},{-1L},{-1L},{0xF17B1833L},{0xC86E61B7L},{1L}},{{0xC86E61B7L},{0x190557B0L},{0x20DF3EF8L},{0x77D7F2AFL},{0xC86E61B7L},{0x264D0DC1L}}},{{{1L},{-1L},{0x6DE086CFL},{2L},{0x77D7F2AFL},{-5L}},{{-5L},{0x6536C7F4L},{0x73F68B5DL},{-1L},{0x52728AACL},{-1L}},{{0x7187E54CL},{-1L},{0x6DE086CFL},{0xACFBA1E8L},{-5L},{0x241E11B2L}},{{0x52728AACL},{0L},{0x20DF3EF8L},{-5L},{0xE9F13E21L},{0L}},{{0x52728AACL},{0x241E11B2L},{-1L},{0xACFBA1E8L},{0L},{-1L}},{{0x7187E54CL},{-1L},{0x6536C7F4L},{-1L},{-1L},{0x6536C7F4L}}},{{{-5L},{-5L},{0x190557B0L},{2L},{0L},{-1L}},{{1L},{0x264D0DC1L},{0xACFBA1E8L},{0x77D7F2AFL},{0xE9F13E21L},{0x190557B0L}},{{0xC86E61B7L},{1L},{0xACFBA1E8L},{0xF17B1833L},{-5L},{-1L}},{{-1L},{0xF17B1833L},{0x190557B0L},{0x273CE81FL},{0x52728AACL},{0x6536C7F4L}},{{0x273CE81FL},{0x52728AACL},{0x6536C7F4L},{0xFA001688L},{0x77D7F2AFL},{-1L}},{{-1L},{0x73F68B5DL},{-1L},{-1L},{0xC86E61B7L},{0L}}},{{{0xC86E61B7L},{0x77D7F2AFL},{0x20DF3EF8L},{0x190557B0L},{0xC86E61B7L},{0x241E11B2L}},{{1L},{0x73F68B5DL},{0x6DE086CFL},{0x7187E54CL},{0x77D7F2AFL},{-1L}},{{-5L},{0x52728AACL},{0x73F68B5DL},{0x73F68B5DL},{0x52728AACL},{-5L}},{{0x7187E54CL},{0xF17B1833L},{0x6DE086CFL},{0xC86E61B7L},{-5L},{0x264D0DC1L}},{{0x52728AACL},{1L},{0x20DF3EF8L},{-1L},{0xE9F13E21L},{1L}},{{0x52728AACL},{0x264D0DC1L},{-1L},{0xC86E61B7L},{0L},{0xF17B1833L}}},{{{0x7187E54CL},{-5L},{0x6536C7F4L},{0x73F68B5DL},{-1L},{0x52728AACL}},{{-5L},{-1L},{0x190557B0L},{0x7187E54CL},{0L},{0x73F68B5DL}},{{1L},{0x241E11B2L},{0xACFBA1E8L},{0x190557B0L},{0xE9F13E21L},{0x77D7F2AFL}},{{0xC86E61B7L},{0L},{0xACFBA1E8L},{-1L},{-5L},{0x73F68B5DL}},{{-1L},{-1L},{0x190557B0L},{0xFA001688L},{0x52728AACL},{0x52728AACL}},{{0x273CE81FL},{0x6536C7F4L},{0x6536C7F4L},{0x273CE81FL},{0x77D7F2AFL},{0xF17B1833L}}}};
static int32_t *g_2654 = &g_986.f0;
static union U1 g_2667 = {0x4CCFCB9DL};/* VOLATILE GLOBAL g_2667 */
static const union U1 g_2710[9] = {{-10L},{-10L},{-10L},{-10L},{-10L},{-10L},{-10L},{-10L},{-10L}};
static volatile union U2 g_2718 = {0xF214BDCBL};/* VOLATILE GLOBAL g_2718 */
static volatile int32_t * volatile g_2725 = &g_2330.f0;/* VOLATILE GLOBAL g_2725 */
static union U2 g_2736 = {6UL};/* VOLATILE GLOBAL g_2736 */
static union U2 g_2737 = {0x96C756DBL};/* VOLATILE GLOBAL g_2737 */
static const union U1 g_2741 = {0x87DB7EB7L};/* VOLATILE GLOBAL g_2741 */
static struct S0 g_2744[7] = {{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL},{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL},{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL},{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL},{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL},{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL},{-7L,0x3C0940A9L,0x51L,0x227BB04419CA8EDELL,0x4534D0B4L,1UL}};
static union U1 g_2774 = {0x88C1C2F4L};/* VOLATILE GLOBAL g_2774 */
static volatile union U1 g_2855 = {0xE194A55CL};/* VOLATILE GLOBAL g_2855 */
static int64_t ****g_2858 = (void*)0;
static volatile union U1 g_2935 = {9L};/* VOLATILE GLOBAL g_2935 */
static uint8_t * volatile g_3003 = &g_2077;/* VOLATILE GLOBAL g_3003 */
static uint8_t * volatile * volatile g_3002 = &g_3003;/* VOLATILE GLOBAL g_3002 */
static union U1 g_3052[8][4][1] = {{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}},{{{0L}},{{0x832A2E2AL}},{{0L}},{{0x832A2E2AL}}}};
static volatile union U1 g_3065 = {0xD36F3A84L};/* VOLATILE GLOBAL g_3065 */
static volatile union U2 g_3076[7] = {{0xA07FEEDBL},{0xA07FEEDBL},{0xA07FEEDBL},{0xA07FEEDBL},{0xA07FEEDBL},{0xA07FEEDBL},{0xA07FEEDBL}};
static union U2 g_3116 = {4UL};/* VOLATILE GLOBAL g_3116 */
static union U2 g_3213 = {0x362A3000L};/* VOLATILE GLOBAL g_3213 */
static int32_t g_3240[3] = {0x73A7C355L,0x73A7C355L,0x73A7C355L};
static uint8_t **g_3249 = (void*)0;
static uint8_t ***g_3248 = &g_3249;
static union U2 g_3252 = {0x4566704EL};/* VOLATILE GLOBAL g_3252 */
static uint32_t g_3275 = 0x63BDF969L;
static uint64_t g_3276 = 0UL;
static struct S0 g_3284 = {0x4DL,0xC16EE1F4L,-3L,0xE7D8EAA850E57FDFLL,4UL,18446744073709551615UL};/* VOLATILE GLOBAL g_3284 */
static volatile struct S0 g_3310 = {-7L,1L,0x2BL,1UL,18446744073709551615UL,0xDF414977L};/* VOLATILE GLOBAL g_3310 */
static union U1 g_3342 = {-1L};/* VOLATILE GLOBAL g_3342 */
static const uint16_t *g_3363 = (void*)0;
static const uint16_t ** const g_3362[7] = {(void*)0,&g_3363,&g_3363,(void*)0,&g_3363,&g_3363,(void*)0};
static const uint16_t ** const *g_3361 = &g_3362[5];
static const uint16_t ** const **g_3360 = &g_3361;
static volatile union U2 g_3367[10] = {{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L},{0xBBC3C426L}};
static const int32_t *g_3413 = &g_204.f1;
static const int32_t ** const  volatile g_3412 = &g_3413;/* VOLATILE GLOBAL g_3412 */
static const volatile int64_t g_3483[4] = {0xB715A50409B74C6ALL,0xB715A50409B74C6ALL,0xB715A50409B74C6ALL,0xB715A50409B74C6ALL};
static volatile union U2 g_3491 = {0UL};/* VOLATILE GLOBAL g_3491 */
static volatile union U1 ** volatile ** volatile g_3494 = &g_1120;/* VOLATILE GLOBAL g_3494 */
static uint8_t ****g_3504[10] = {&g_3248,&g_3248,&g_3248,&g_3248,&g_3248,&g_3248,&g_3248,&g_3248,&g_3248,&g_3248};
static uint8_t ***** volatile g_3503 = &g_3504[7];/* VOLATILE GLOBAL g_3503 */


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static int32_t * func_2(int16_t  p_3, uint8_t  p_4, int32_t  p_5, int32_t * p_6);
static int16_t  func_14(uint32_t  p_15, int32_t  p_16, int32_t  p_17, uint16_t  p_18);
static int8_t  func_22(uint16_t  p_23, uint16_t  p_24, int32_t  p_25);
static int32_t  func_33(int32_t  p_34, int32_t * p_35, uint16_t  p_36, uint8_t  p_37);
static int32_t ** func_48(uint32_t  p_49);
static int32_t  func_56(int32_t  p_57, uint32_t  p_58, uint8_t  p_59);
static int32_t  func_60(const uint32_t  p_61, uint64_t  p_62, const int8_t  p_63, uint16_t  p_64);
static uint64_t  func_69(int32_t * p_70);
static int32_t * func_71(int64_t  p_72);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_11 g_26 g_26.f0 g_29 g_42 g_2071 g_2072 g_2054 g_1606 g_1577.f0 g_581 g_204.f1 g_2077 g_132 g_83 g_78 g_2081 g_2082 g_594 g_986.f0 g_551 g_1172 g_333 g_334 g_335 g_336 g_337 g_2109 g_130 g_131 g_1919 g_610 g_611 g_442 g_1178 g_1179 g_383.f1 g_126 g_200 g_2071.f2 g_776 g_343 g_2137 g_1550 g_1145 g_1146 g_550 g_204.f0 g_300.f0 g_2367 g_2387 g_1177 g_2408 g_723.f0 g_1171 g_2718 g_93 g_79 g_79.f0 g_94 g_109 g_145 g_148 g_43 g_148.f0 g_190 g_21 g_205 g_211 g_218 g_383.f3 g_2725 g_2330.f0 g_2369 g_2370 g_1144 g_778 g_832.f0 g_2855 g_2858 g_2538 g_2539 g_2612 g_2368 g_2741.f0 g_2654 g_2935 g_340.f0 g_1180 g_386 g_1562.f4 g_1562.f1 g_3002 g_1218 g_1219 g_696.f1 g_490 g_3003 g_2387.f0 g_1945.f0 g_482.f1.f1 g_3367 g_3412 g_3483 g_2129 g_791.f0 g_231 g_1159 g_3491 g_3494 g_3116.f2 g_3503
 * writes: g_21 g_29 g_2077 g_78 g_204.f3 g_343 g_93 g_383.f1 g_337 g_490 g_132 g_1919 g_26.f1.f1 g_83 g_442 g_1748.f2 g_2071.f2 g_1748.f1.f3 g_94 g_1229.f0 g_777 g_204.f1 g_1872.f2 g_331 g_610 g_265.f1.f3 g_2417 g_26.f2 g_109 g_79.f0 g_190 g_200 g_206 g_211 g_218 g_131 g_383.f3 g_2330.f0 g_11 g_832.f0 g_986.f0 g_2858 g_681.f1.f3 g_1748.f1.f0 g_2444 g_2612 g_2551.f1.f3 g_1146 g_676 g_696.f1 g_1550 g_482.f1.f1 g_581 g_3413 g_2654 g_1327.f1.f1 g_791.f0 g_1159 g_1120 g_3116.f2 g_3504 g_1172
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_10 = &g_11;
    int32_t *l_2119 = (void*)0;
    int32_t *l_2357[9][9] = {{&g_2082.f0,&g_585.f0,&g_1156.f1,&g_585.f0,&g_2082.f0,&g_585.f0,&g_1156.f1,&g_585.f0,&g_2082.f0},{&g_986.f0,&g_491.f0,&g_491.f0,&g_986.f0,&g_231[3],&g_148.f0,&g_148.f0,&g_231[3],&g_986.f0},{&g_1048[0][0].f0,&g_585.f0,&g_1048[0][0].f0,&g_830.f0,&g_1048[0][0].f0,&g_585.f0,&g_1048[0][0].f0,&g_830.f0,&g_1048[0][0].f0},{&g_986.f0,&g_986.f0,&g_148.f0,&g_491.f0,&g_231[3],&g_231[3],&g_491.f0,&g_148.f0,&g_986.f0},{&g_2082.f0,&g_830.f0,&g_1156.f1,&g_830.f0,&g_2082.f0,&g_830.f0,&g_1156.f1,&g_830.f0,&g_2082.f0},{&g_231[3],&g_491.f0,&g_148.f0,&g_986.f0,&g_986.f0,&g_148.f0,&g_491.f0,&g_231[3],&g_231[3]},{&g_1048[0][0].f0,&g_830.f0,&g_1048[0][0].f0,&g_585.f0,&g_1048[0][0].f0,&g_830.f0,&g_1048[0][0].f0,&g_585.f0,&g_1048[0][0].f0},{&g_231[3],&g_986.f0,&g_491.f0,&g_491.f0,&g_986.f0,&g_231[3],&g_148.f0,&g_148.f0,&g_231[3]},{&g_2082.f0,&g_585.f0,&g_1156.f1,&g_585.f0,&g_2082.f0,&g_585.f0,&g_1156.f1,&g_585.f0,&g_2082.f0}};
    int32_t **l_3505[7][6][3] = {{{(void*)0,&g_83,(void*)0},{&g_1172,&g_2654,&l_2357[5][0]},{&l_2357[7][5],&g_83,&g_83},{&g_1172,&g_83,&g_1172},{(void*)0,&g_2654,(void*)0},{(void*)0,&g_1172,&g_1172}},{{(void*)0,(void*)0,&g_2654},{&g_1172,&g_1172,&g_2654},{&g_83,&g_2654,(void*)0},{&g_2654,&g_2654,&g_1172},{(void*)0,&l_2357[7][5],&g_83},{&g_1172,(void*)0,&g_1172}},{{&g_83,&l_2357[0][3],&g_2654},{(void*)0,&g_83,&g_2654},{&l_2357[7][5],&g_83,(void*)0},{&g_1172,(void*)0,(void*)0},{&l_2357[7][5],(void*)0,(void*)0},{(void*)0,&g_1172,&g_2654}},{{&g_83,(void*)0,&l_2357[7][5]},{&g_1172,&g_2654,&l_2357[0][3]},{(void*)0,(void*)0,&g_83},{(void*)0,&g_1172,&l_2357[5][0]},{&g_83,(void*)0,&g_2654},{&g_2654,(void*)0,&g_1172}},{{&g_2654,&g_83,&g_2654},{&g_83,&g_83,&l_2357[5][0]},{(void*)0,&l_2357[0][3],&g_83},{(void*)0,(void*)0,&l_2357[0][3]},{(void*)0,&l_2357[7][5],&l_2357[7][5]},{(void*)0,&g_2654,&g_2654}},{{(void*)0,&g_83,(void*)0},{&g_83,&g_2654,(void*)0},{&g_2654,&g_83,(void*)0},{&g_2654,&g_2654,&g_2654},{&g_83,&g_83,&g_2654},{(void*)0,&g_2654,&g_1172}},{{(void*)0,&l_2357[7][5],&g_83},{&g_1172,(void*)0,&g_1172},{&g_83,&l_2357[0][3],&g_2654},{(void*)0,&g_83,&g_2654},{&l_2357[7][5],&g_83,(void*)0},{&g_1172,(void*)0,(void*)0}}};
    uint32_t l_3506 = 0x2334A790L;
    int i, j, k;
    (*g_2129) = (l_10 = func_2((0x78L & ((0L ^ (!(((void*)0 == l_10) == (safe_lshift_func_int16_t_s_s((func_14((safe_rshift_func_uint32_t_u_s((*l_10), 30)), (g_1748.f2 = (((g_21 = (l_10 == &g_11)) , func_22((&g_11 != (g_26 , (void*)0)), g_26.f0, g_11)) ^ g_126)), g_200, (*l_10)) , 0xCCC0L), (*l_10)))))) & (*g_2137))), (*l_10), (*l_10), l_2357[0][3]));
    ++l_3506;
    return (*g_611);
}


/* ------------------------------------------ */
/* 
 * reads : g_204.f0 g_300.f0 g_2367 g_1172 g_2387 g_1177 g_1178 g_1179 g_611 g_442 g_383.f1 g_2408 g_723.f0 g_1171 g_2718 g_550 g_551 g_93 g_79 g_11 g_78 g_79.f0 g_94 g_109 g_145 g_148 g_126 g_43 g_148.f0 g_132 g_83 g_131 g_190 g_21 g_42 g_130 g_205 g_211 g_218 g_383.f3 g_2725 g_2330.f0 g_343 g_2369 g_2370 g_1144 g_1145 g_1146 g_778 g_204.f1 g_832.f0 g_986.f0 g_2855 g_2858 g_2538 g_2539 g_2137 g_1550 g_2612 g_581 g_2368 g_2741.f0 g_2654 g_2935 g_340.f0 g_1180 g_386 g_1562.f4 g_1562.f1 g_3002 g_1218 g_1219 g_696.f1 g_2109 g_490 g_3003 g_2077 g_2387.f0 g_1945.f0 g_482.f1.f1 g_3367 g_3412 g_333 g_334 g_335 g_336 g_3483 g_2129 g_791.f0 g_231 g_1159 g_3491 g_3494 g_337 g_3116.f2 g_3503
 * writes: g_343 g_383.f1 g_331 g_610 g_265.f1.f3 g_2417 g_93 g_26.f2 g_78 g_83 g_94 g_21 g_109 g_26.f1.f1 g_79.f0 g_190 g_200 g_206 g_211 g_218 g_131 g_383.f3 g_2330.f0 g_204.f1 g_11 g_832.f0 g_986.f0 g_2858 g_681.f1.f3 g_1748.f1.f0 g_2444 g_2612 g_2551.f1.f3 g_1146 g_2077 g_676 g_696.f1 g_490 g_1550 g_482.f1.f1 g_581 g_3413 g_337 g_2654 g_1327.f1.f1 g_791.f0 g_1159 g_1120 g_3116.f2 g_3504
 */
static int32_t * func_2(int16_t  p_3, uint8_t  p_4, int32_t  p_5, int32_t * p_6)
{ /* block id: 1142 */
    int32_t l_2364 = 0x1DF7E3E0L;
    int8_t **l_2374[2][5] = {{&g_2137,&g_2137,&g_2137,&g_2137,&g_2137},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    int8_t ***l_2373 = &l_2374[0][0];
    int8_t ****l_2372 = &l_2373;
    int8_t *****l_2371 = &l_2372;
    int32_t l_2377 = 0xE14019BBL;
    uint8_t *l_2378 = (void*)0;
    uint8_t *l_2379[3][3] = {{&g_94,&g_94,&g_2077},{&g_94,&g_94,&g_2077},{&g_94,&g_94,&g_2077}};
    int32_t l_2380 = 0L;
    uint16_t *l_2381 = &g_343[2][2];
    uint16_t *l_2382 = &g_331;
    uint32_t l_2393 = 4294967288UL;
    struct S0 **l_2414 = &g_206;
    struct S0 ***l_2413 = &l_2414;
    uint16_t ****l_2419[10][4][3] = {{{&g_1852[1][4][1],&g_1852[1][2][4],&g_1852[1][4][1]},{&g_1852[1][5][3],&g_1852[0][2][5],&g_1852[0][1][2]},{&g_1852[0][5][0],&g_1852[1][2][4],&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][2]}},{{&g_1852[0][5][1],&g_1852[0][4][3],&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][2][5]},{&g_1852[0][5][0],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[1][5][3],&g_1852[0][4][3],&g_1852[0][2][5]}},{{&g_1852[1][4][1],(void*)0,&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][2][5],&g_1852[0][4][2]},{&g_1852[0][5][0],(void*)0,&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][1][2]}},{{&g_1852[0][5][1],&g_1852[0][4][3],&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[0][5][0],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3]}},{{&g_1852[1][4][1],&g_1852[1][2][4],&g_1852[1][4][1]},{&g_1852[1][5][3],&g_1852[0][2][5],&g_1852[0][1][2]},{&g_1852[0][5][0],&g_1852[1][2][4],&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][2]}},{{&g_1852[0][5][1],&g_1852[0][4][3],&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][2][5]},{&g_1852[0][5][0],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[1][5][3],&g_1852[0][4][3],&g_1852[0][2][5]}},{{&g_1852[1][4][1],(void*)0,&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][2][5],&g_1852[0][4][2]},{&g_1852[0][5][0],(void*)0,&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][1][2]}},{{&g_1852[0][5][1],&g_1852[0][4][3],&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[0][5][0],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3]}},{{&g_1852[1][4][1],&g_1852[1][2][4],&g_1852[1][4][1]},{&g_1852[1][5][3],&g_1852[0][2][5],&g_1852[0][1][2]},{&g_1852[0][5][0],&g_1852[1][2][4],&g_1852[0][4][3]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][2]}},{{&g_1852[0][5][1],&g_1852[0][4][3],&g_1852[1][4][1]},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][2][5]},{&g_1852[0][5][0],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[1][5][3],&g_1852[0][4][3],&g_1852[0][2][5]}}};
    int32_t l_2471 = 2L;
    int32_t l_2473 = 1L;
    int32_t l_2476 = (-6L);
    int32_t l_2477 = 0xA15B6274L;
    int32_t l_2481 = 0x6684E8D7L;
    int32_t l_2482 = 0xF738DB22L;
    int32_t l_2483 = (-1L);
    int32_t l_2484 = 0xF110B0F3L;
    int32_t l_2485 = 1L;
    int32_t l_2486 = (-4L);
    int32_t l_2487 = 0xDE23EDE1L;
    uint8_t l_2488 = 0xD1L;
    union U2 ***l_2506 = &g_778;
    const struct S0 **l_2562[2];
    int8_t l_2573 = 0x45L;
    int64_t **l_2593 = &g_611;
    const uint8_t *l_2614 = &g_94;
    const uint8_t **l_2613 = &l_2614;
    int32_t l_2632 = 0xD1E30747L;
    int16_t l_2701 = 0xC92AL;
    int32_t l_2702 = 0x758D018AL;
    int32_t l_2703[3];
    int32_t l_2705 = 0x7CACD449L;
    uint32_t l_2707 = 0x024599EFL;
    const uint64_t l_2739 = 0x99DB5C2E3D9B4B65LL;
    int8_t l_2880 = 0xC3L;
    int32_t *l_2882[10][9] = {{&l_2483,(void*)0,&g_1048[0][0].f0,&l_2364,&l_2486,(void*)0,&l_2477,&g_2556.f0,(void*)0},{&g_1048[0][0].f0,&l_2477,&g_79.f0,&g_300.f1,&g_986.f0,(void*)0,&l_2473,(void*)0,(void*)0},{(void*)0,&g_11,&g_1496[3][2][0].f0,&g_791.f0,(void*)0,&g_2556.f0,&l_2364,&l_2473,&l_2364},{(void*)0,&g_11,(void*)0,(void*)0,&g_11,(void*)0,(void*)0,&g_491.f0,(void*)0},{&l_2364,&l_2477,&l_2486,&g_1572.f1,&g_2556.f0,(void*)0,&g_2556.f0,&g_2556.f0,&g_79.f0},{&l_2477,(void*)0,&g_11,&g_491.f0,&l_2473,&g_1572.f1,(void*)0,&g_340.f1,&g_986.f0},{&g_830.f0,&g_2556.f0,(void*)0,(void*)0,&l_2471,&g_300.f1,&l_2364,&g_11,&g_11},{&l_2473,&g_78,(void*)0,&g_1048[0][0].f0,(void*)0,&g_78,&l_2473,(void*)0,&g_791.f0},{&g_986.f0,&g_791.f0,&g_11,(void*)0,&g_491.f0,(void*)0,&l_2477,&g_1572.f1,(void*)0},{&g_1572.f1,&g_340.f1,&l_2486,&g_791.f0,(void*)0,&g_2556.f0,(void*)0,(void*)0,(void*)0}};
    int16_t l_2883[2][9] = {{0x5505L,(-8L),(-8L),0x5505L,0xF038L,0x5505L,(-8L),(-8L),0x5505L},{0xD137L,(-8L),(-1L),(-8L),0xD137L,0xD137L,(-8L),(-1L),(-8L)}};
    uint64_t l_2884 = 0x83B067F61F044905LL;
    uint32_t l_2904 = 0xF121DB53L;
    union U1 ****l_2915 = (void*)0;
    int8_t l_2922 = 0x49L;
    int32_t ***l_2933 = &g_132;
    int32_t *** const l_2934 = &g_132;
    uint8_t l_2944 = 1UL;
    int16_t ** const l_2975[10][9] = {{&g_1141,(void*)0,&g_1141,&g_1141,(void*)0,&g_1141,(void*)0,&g_1141,&g_1141},{&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141},{&g_1141,(void*)0,&g_1141,&g_1141,&g_1141,&g_1141,(void*)0,&g_1141,&g_1141},{&g_1141,&g_1141,&g_1141,&g_1141,(void*)0,&g_1141,&g_1141,&g_1141,&g_1141},{&g_1141,&g_1141,(void*)0,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141},{&g_1141,&g_1141,(void*)0,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,(void*)0},{&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,(void*)0},{&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141},{&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141},{&g_1141,&g_1141,(void*)0,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141,&g_1141}};
    uint32_t **l_2976 = &g_551;
    int8_t l_3060 = (-1L);
    int8_t *l_3063 = &l_3060;
    uint16_t l_3072 = 0x2E49L;
    uint32_t *l_3143 = (void*)0;
    uint32_t l_3175 = 18446744073709551608UL;
    uint32_t ****l_3180 = &g_676[0][0];
    uint32_t l_3200 = 0x6A47DE33L;
    uint8_t l_3220 = 0xFDL;
    int8_t l_3274 = 0x63L;
    int32_t *l_3364 = &g_1327.f1.f1;
    int32_t l_3431 = 0x6E3E3D9EL;
    volatile int32_t * volatile l_3496 = &g_651.f0;/* VOLATILE GLOBAL l_3496 */
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2562[i] = &g_299[2][1];
    for (i = 0; i < 3; i++)
        l_2703[i] = 0x52FAC2CCL;
    if ((((*l_2382) = ((safe_div_func_uint8_t_u_u(0xE5L, (safe_add_func_int32_t_s_s(((*g_1172) = (p_4 <= (safe_rshift_func_int32_t_s_u((l_2364 = l_2364), ((safe_mul_func_uint64_t_u_u(g_204.f0, g_300.f0)) != ((*l_2381) = ((g_2367 == l_2371) >= ((l_2377 = (((((safe_div_func_uint32_t_u_u(l_2377, 0x634CA90CL)) < 0x153C9B17DC58FE80LL) & 1UL) && l_2377) >= l_2377)) > l_2380)))))))), p_3)))) && 4294967288UL)) & l_2380))
    { /* block id: 1148 */
        int64_t **l_2388[7];
        int64_t ***l_2389 = &g_610;
        int32_t l_2390 = 0x7F904F90L;
        struct S0 **l_2412[6];
        struct S0 ***l_2411 = &l_2412[3];
        union U1 *l_2423 = &g_995;
        int32_t l_2430 = 1L;
        int32_t l_2432[10][7] = {{0x0CE76AE5L,0xCCEF7F2CL,0L,0xD0A2372AL,0x46CC9AF6L,0L,0xDDAB611DL},{1L,2L,1L,0xDC003897L,0x4ECC0F1DL,7L,0L},{0x2C9D1F30L,(-1L),0xB97092ACL,7L,0x1E2AE2DBL,0xAA25A0D5L,0xAA25A0D5L},{0x642CDEA5L,7L,6L,7L,0x642CDEA5L,0x510B027CL,0xCCEF7F2CL},{0xD0A2372AL,0xB97092ACL,0xDDAB611DL,0xDC003897L,(-4L),0x46CC9AF6L,0x39911E4AL},{0x5783E04DL,0x642CDEA5L,(-10L),0xD0A2372AL,2L,0x4ECC0F1DL,(-4L)},{0xD0A2372AL,0xDC003897L,(-6L),1L,6L,0x1E2AE2DBL,0x2C9D1F30L},{0x642CDEA5L,6L,0L,0L,6L,0x642CDEA5L,0x4ECC0F1DL},{0x2C9D1F30L,0x39911E4AL,0x642CDEA5L,0x0CE76AE5L,2L,(-4L),(-1L)},{1L,0xACC2F3EAL,7L,(-1L),(-4L),2L,0x0CE76AE5L}};
        int32_t l_2474 = 0x6E903ADFL;
        int32_t l_2475[1][7][7] = {{{(-3L),0x48FFD526L,0x48FFD526L,(-3L),0x48FFD526L,0x48FFD526L,(-3L)},{7L,(-10L),7L,7L,(-10L),7L,7L},{(-3L),(-3L),(-1L),(-3L),(-3L),(-1L),(-3L)},{(-10L),7L,7L,(-10L),7L,7L,(-10L)},{0x48FFD526L,(-3L),0x48FFD526L,0x48FFD526L,(-3L),0x48FFD526L,0x48FFD526L},{(-10L),(-10L),8L,(-10L),(-10L),8L,(-10L)},{(-3L),0x48FFD526L,0x48FFD526L,(-3L),0x48FFD526L,0x48FFD526L,(-3L)}}};
        union U2 ***l_2511 = &g_778;
        const int64_t ** const **l_2542 = (void*)0;
        const int64_t ** const ***l_2541 = &l_2542;
        uint16_t * const l_2611 = &g_2612[1];
        uint16_t * const *l_2610 = &l_2611;
        uint16_t * const **l_2609[9][6][1] = {{{&l_2610},{&l_2610},{&l_2610},{&l_2610},{&l_2610},{&l_2610}},{{&l_2610},{&l_2610},{&l_2610},{&l_2610},{&l_2610},{&l_2610}},{{&l_2610},{&l_2610},{&l_2610},{&l_2610},{&l_2610},{&l_2610}},{{(void*)0},{&l_2610},{&l_2610},{&l_2610},{&l_2610},{(void*)0}},{{(void*)0},{(void*)0},{&l_2610},{&l_2610},{&l_2610},{&l_2610}},{{(void*)0},{&l_2610},{(void*)0},{&l_2610},{(void*)0},{&l_2610}},{{(void*)0},{&l_2610},{&l_2610},{&l_2610},{&l_2610},{(void*)0}},{{(void*)0},{(void*)0},{&l_2610},{&l_2610},{&l_2610},{&l_2610}},{{(void*)0},{&l_2610},{(void*)0},{&l_2610},{(void*)0},{&l_2610}}};
        uint16_t * const ***l_2608[4];
        uint16_t * const ****l_2607[10][9] = {{&l_2608[2],&l_2608[3],(void*)0,(void*)0,&l_2608[3],&l_2608[2],&l_2608[3],&l_2608[2],&l_2608[3]},{&l_2608[3],&l_2608[2],&l_2608[2],&l_2608[0],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[0],&l_2608[2]},{&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3],(void*)0,&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3]},{&l_2608[2],&l_2608[0],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[0],&l_2608[2],&l_2608[2],&l_2608[3]},{&l_2608[3],&l_2608[2],&l_2608[3],&l_2608[2],&l_2608[3],(void*)0,(void*)0,&l_2608[3],&l_2608[2]},{&l_2608[2],&l_2608[3],&l_2608[2],&l_2608[1],&l_2608[3],&l_2608[2],&l_2608[0],&l_2608[2],&l_2608[3]},{&l_2608[3],&l_2608[3],(void*)0,&l_2608[3],&l_2608[3],(void*)0,&l_2608[3],&l_2608[3],&l_2608[3]},{&l_2608[3],&l_2608[3],&l_2608[0],&l_2608[1],&l_2608[1],&l_2608[0],&l_2608[3],&l_2608[0],&l_2608[1]},{&l_2608[2],&l_2608[3],&l_2608[3],&l_2608[2],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[2],&l_2608[3]},{&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3],&l_2608[3]}};
        int32_t ****l_2650 = &g_131;
        int64_t l_2653 = (-1L);
        union U1 ***l_2715 = (void*)0;
        union U1 **l_2717 = &g_490;
        union U1 ***l_2716 = &l_2717;
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_2388[i] = &g_611;
        for (i = 0; i < 6; i++)
            l_2412[i] = &g_206;
        for (i = 0; i < 4; i++)
            l_2608[i] = &l_2609[3][1][0];
        if ((safe_lshift_func_uint64_t_u_s((safe_lshift_func_int64_t_s_u(((((((g_2387 , (((l_2388[3] != ((*l_2389) = (void*)0)) >= (l_2390 != (-1L))) , 0x7F2814B9CAFD0E4ELL)) > p_5) & (safe_sub_func_int32_t_s_s(((void*)0 != &p_4), l_2390))) , (**g_1177)) > p_3) || l_2393), 41)), 25)))
        { /* block id: 1150 */
            uint32_t l_2401 = 6UL;
            l_2390 = (safe_div_func_int16_t_s_s(((safe_div_func_int64_t_s_s(p_3, (~((safe_add_func_uint8_t_u_u(8UL, l_2401)) <= ((*g_611) >= (safe_sub_func_uint32_t_u_u((p_4 != p_5), ((*g_1172) = (*g_1172))))))))) > 0L), l_2390));
        }
        else
        { /* block id: 1153 */
            const uint32_t l_2407 = 0x457AC718L;
            uint64_t *l_2415 = (void*)0;
            uint64_t *l_2416 = &g_265.f1.f3;
            (**g_1171) = ((!((safe_sub_func_int32_t_s_s(l_2380, l_2407)) != ((g_2408 , (*g_1178)) , 0x9308L))) > (safe_rshift_func_int32_t_s_u(l_2393, (((*l_2416) = (l_2411 == l_2413)) >= g_723.f0))));
            g_2417 = 0x8392B008L;
        }
    }
    else
    { /* block id: 1275 */
        uint8_t l_2727 = 0x22L;
        int8_t **l_2732 = (void*)0;
        uint32_t l_2733 = 0x1EC0BD15L;
        union U2 *l_2735[9] = {&g_2736,&g_2736,&g_2736,&g_2736,&g_2736,&g_2736,&g_2736,&g_2736,&g_2736};
        union U2 **l_2734 = &l_2735[7];
        uint8_t **l_2738 = &l_2379[1][1];
        int32_t l_2780 = 0x6024F062L;
        int32_t l_2784 = (-1L);
        uint8_t l_2818 = 251UL;
        uint16_t *****l_2881 = &l_2419[3][3][1];
        int i;
        l_2486 ^= ((*g_1172) = (g_2718 , l_2485));
        (*g_132) = ((l_2703[2] , (safe_sub_func_uint32_t_u_u((p_3 , (9L | 0x9BL)), (--(**g_550))))) , func_71(p_3));
        for (g_383.f3 = 0; (g_383.f3 > 10); g_383.f3 = safe_add_func_uint16_t_u_u(g_383.f3, 1))
        { /* block id: 1282 */
            volatile int32_t * volatile l_2726 = (void*)0;/* VOLATILE GLOBAL l_2726 */
            l_2726 = g_2725;
            (*l_2726) ^= l_2727;
        }
        if (((**g_132) = (((*l_2381)++) >= ((((void*)0 == &l_2593) >= (safe_lshift_func_uint16_t_u_s((l_2732 == (*g_2369)), 5))) != (((l_2486 | ((0xE5L > (((((((((-1L) | ((**g_1144) <= (0L > 253UL))) != p_3) , p_5) , l_2733) , l_2734) != (*l_2506)) , (void*)0) == l_2738)) ^ l_2733)) || l_2739) && p_3)))))
        { /* block id: 1288 */
            const union U1 *l_2740 = &g_2741;
            int64_t ****l_2746 = (void*)0;
            int64_t *****l_2745 = &l_2746;
            uint8_t l_2764[5] = {0x8AL,0x8AL,0x8AL,0x8AL,0x8AL};
            int32_t l_2792 = (-6L);
            int32_t l_2803 = 0x530D260DL;
            int32_t l_2804[4][4][3] = {{{6L,0x35BC6A01L,1L},{1L,1L,1L},{1L,6L,0x1290EFDBL},{0L,1L,8L}},{{1L,1L,(-8L)},{1L,0L,0x5060506FL},{6L,1L,6L},{1L,1L,(-6L)}},{{0x35BC6A01L,6L,6L},{(-6L),1L,0x5060506FL},{0L,0x35BC6A01L,(-8L)},{(-6L),(-6L),8L}},{{0x35BC6A01L,0L,0x1290EFDBL},{1L,(-6L),1L},{6L,0x35BC6A01L,1L},{1L,1L,1L}}};
            union U1 * const *l_2817 = &g_490;
            union U1 * const **l_2816 = &l_2817;
            int i, j, k;
            (**g_132) ^= p_5;
            l_2740 = (void*)0;
            for (g_832.f0 = 1; (g_832.f0 >= 0); g_832.f0 -= 1)
            { /* block id: 1293 */
                int64_t l_2751[8] = {0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL,0x5100CFA2386A4DEELL};
                int32_t l_2778[2];
                int64_t * const * const l_2844[10][9] = {{&g_611,(void*)0,&g_611,&g_611,&g_611,&g_611,(void*)0,&g_611,&g_611},{&g_611,&g_611,&g_611,(void*)0,&g_611,&g_611,&g_611,(void*)0,&g_611},{&g_611,&g_611,(void*)0,(void*)0,&g_611,&g_611,&g_611,&g_611,&g_611},{&g_611,(void*)0,&g_611,(void*)0,&g_611,(void*)0,&g_611,(void*)0,&g_611},{&g_611,(void*)0,(void*)0,&g_611,&g_611,&g_611,&g_611,&g_611,&g_611},{&g_611,(void*)0,&g_611,&g_611,&g_611,(void*)0,&g_611,&g_611,&g_611},{&g_611,&g_611,&g_611,(void*)0,&g_611,&g_611,(void*)0,&g_611,&g_611},{&g_611,&g_611,&g_611,&g_611,&g_611,&g_611,&g_611,&g_611,&g_611},{&g_611,(void*)0,&g_611,&g_611,&g_611,&g_611,(void*)0,&g_611,&g_611},{&g_611,&g_611,&g_611,(void*)0,&g_611,&g_611,&g_611,(void*)0,&g_611}};
                int64_t * const * const * const l_2843 = &l_2844[0][2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_2778[i] = (-2L);
            }
        }
        else
        { /* block id: 1348 */
            uint32_t l_2849 = 1UL;
            int32_t l_2868[7] = {0x7DC05503L,0x7DC05503L,4L,0x7DC05503L,0x7DC05503L,4L,0x7DC05503L};
            int i;
            for (g_986.f0 = (-27); (g_986.f0 >= (-20)); g_986.f0 = safe_add_func_int32_t_s_s(g_986.f0, 3))
            { /* block id: 1351 */
                uint32_t l_2854 = 0xA57D91C3L;
                int64_t *****l_2859 = &g_2858;
                uint64_t *l_2877[8][5][6] = {{{&g_300.f3,&g_1156.f3,&g_383.f3,&g_204.f3,&g_1572.f3,&g_340.f3},{&g_696.f3,(void*)0,&g_21,&g_2744[1].f3,&g_1572.f3,&g_300.f3},{&g_1156.f3,(void*)0,&g_204.f3,&g_696.f3,&g_1572.f3,&g_1572.f3},{&g_1156.f3,&g_2744[1].f3,&g_2744[1].f3,&g_1156.f3,&g_204.f3,&g_1156.f3},{&g_1572.f3,&g_340.f3,&g_340.f3,&g_21,&g_2744[1].f3,&g_300.f3}},{{&g_21,&g_2744[1].f3,&g_2744[1].f3,(void*)0,&g_2744[1].f3,&g_383.f3},{&g_696.f3,&g_340.f3,&g_1572.f3,&g_1572.f3,&g_204.f3,&g_1156.f3},{&g_204.f3,&g_2744[1].f3,&g_300.f3,&g_2744[1].f3,&g_1572.f3,&g_696.f3},{&g_2744[1].f3,(void*)0,&g_1156.f3,&g_1156.f3,&g_1572.f3,&g_204.f3},{&g_109,(void*)0,&g_340.f3,(void*)0,&g_1572.f3,&g_204.f3}},{{(void*)0,&g_1156.f3,(void*)0,&g_1156.f3,&g_109,&g_1156.f3},{(void*)0,&g_383.f3,&g_340.f3,&g_1572.f3,&g_383.f3,&g_2514.f3},{&g_109,&g_1572.f3,(void*)0,(void*)0,&g_1156.f3,&g_2744[1].f3},{(void*)0,&g_300.f3,(void*)0,&g_300.f3,(void*)0,(void*)0},{&g_1156.f3,&g_696.f3,&g_383.f3,&g_340.f3,&g_204.f3,&g_1572.f3}},{{&g_340.f3,&g_109,(void*)0,(void*)0,&g_2744[1].f3,&g_1572.f3},{&g_696.f3,(void*)0,(void*)0,&g_1156.f3,&g_1156.f3,&g_300.f3},{&g_2744[1].f3,&g_21,&g_696.f3,&g_340.f3,&g_1572.f3,&g_1572.f3},{&g_1572.f3,&g_696.f3,&g_1156.f3,&g_300.f3,(void*)0,&g_2744[1].f3},{&g_1156.f3,(void*)0,&g_2514.f3,&g_300.f3,&g_2744[1].f3,&g_2744[1].f3}},{{&g_1156.f3,(void*)0,&g_340.f3,&g_21,&g_109,&g_340.f3},{&g_21,&g_300.f3,(void*)0,&g_1156.f3,&g_383.f3,&g_1156.f3},{&g_2744[1].f3,&g_340.f3,&g_2744[1].f3,&g_383.f3,(void*)0,&g_383.f3},{&g_300.f3,&g_340.f3,&g_109,&g_109,&g_340.f3,&g_300.f3},{&g_21,&g_204.f3,&g_383.f3,(void*)0,&g_1156.f3,(void*)0}},{{(void*)0,(void*)0,&g_1572.f3,(void*)0,(void*)0,&g_1572.f3},{(void*)0,&g_109,(void*)0,(void*)0,&g_300.f3,&g_340.f3},{&g_21,&g_1156.f3,&g_1156.f3,&g_109,&g_696.f3,&g_696.f3},{&g_300.f3,&g_300.f3,&g_1572.f3,&g_383.f3,&g_109,&g_109},{&g_2744[1].f3,&g_2744[1].f3,&g_204.f3,&g_1156.f3,&g_2744[1].f3,(void*)0}},{{&g_21,&g_1572.f3,&g_109,&g_21,&g_21,&g_204.f3},{&g_1156.f3,&g_340.f3,(void*)0,&g_300.f3,(void*)0,(void*)0},{&g_1156.f3,&g_1156.f3,&g_2744[1].f3,&g_300.f3,&g_21,&g_1572.f3},{&g_1572.f3,&g_1156.f3,&g_1156.f3,&g_340.f3,&g_1156.f3,&g_1156.f3},{&g_2744[1].f3,&g_2744[1].f3,&g_1156.f3,&g_1156.f3,&g_2514.f3,&g_1572.f3}},{{&g_696.f3,&g_109,&g_1572.f3,(void*)0,&g_1156.f3,&g_340.f3},{&g_340.f3,&g_109,&g_1156.f3,&g_300.f3,&g_2514.f3,&g_1156.f3},{&g_300.f3,&g_2744[1].f3,&g_109,&g_1572.f3,&g_1156.f3,&g_2744[1].f3},{&g_2514.f3,&g_1156.f3,&g_204.f3,&g_340.f3,&g_21,(void*)0},{&g_340.f3,&g_1156.f3,&g_2744[1].f3,(void*)0,(void*)0,&g_340.f3}}};
                int32_t l_2878 = 0x59AF4B20L;
                int32_t l_2879 = 0L;
                int i, j, k;
                (**g_132) = (safe_lshift_func_int32_t_s_u(l_2849, 18));
                (*g_1172) ^= (safe_add_func_int8_t_s_s(((safe_mul_func_int32_t_s_s(l_2854, (0x662030D9L == (((g_2855 , (safe_rshift_func_int64_t_s_u((((*l_2859) = g_2858) == (*g_2538)), (((safe_add_func_int64_t_s_s(((l_2879 = ((safe_mod_func_uint8_t_u_u((((safe_mod_func_int32_t_s_s((((p_3 || (safe_mod_func_uint16_t_u_u(l_2868[4], (safe_mod_func_int32_t_s_s((((safe_rshift_func_int8_t_s_s(((((safe_rshift_func_uint32_t_u_s((((((*g_551) &= (safe_mod_func_uint64_t_u_u(p_3, (g_681.f1.f3 = (l_2878 = (((**g_1177) || (**g_1144)) ^ p_3)))))) , (*g_1178)) | l_2854) , p_5), 19)) >= l_2854) , 0x321FL) & l_2868[4]), (*g_2137))) , 0x34DFL) != p_3), l_2784))))) != 0UL) && 0x42L), p_4)) != l_2818) <= 0x29B519A1389BD78DLL), l_2477)) && 0xFDF6L)) & l_2854), 0UL)) || (*g_1145)) & l_2854)))) <= p_5) || l_2780)))) ^ l_2880), 0x90L));
                l_2881 = &l_2419[8][1][0];
            }
            return (**g_131);
        }
    }
lbl_3055:
    l_2884++;
    for (l_2471 = 3; (l_2471 >= 0); l_2471 -= 1)
    { /* block id: 1367 */
        uint64_t l_2889[6][5][8] = {{{0xAB624B1381959B73LL,1UL,4UL,0UL,1UL,0x11887ABCBAE3D0A5LL,1UL,0x66AA2C4D427B10F5LL},{1UL,1UL,8UL,18446744073709551615UL,0xFD8F5C03BA9DEF1ELL,0xACF1C41C638C37C2LL,0x122687A14C54D5ABLL,0xD1D808DF00451FEALL},{6UL,18446744073709551609UL,0x6F55C9730241C555LL,0UL,0x86D573599C4F0BA4LL,1UL,0x7D15C7D16E3A984FLL,18446744073709551611UL},{0xACF1C41C638C37C2LL,0UL,0x69BF0E4A2BB54D2ALL,1UL,0x383E9C240C9EFA26LL,4UL,0UL,0x5D8F6D015335EFA7LL},{0x6F55C9730241C555LL,0xD1D808DF00451FEALL,0xAB624B1381959B73LL,0x383E9C240C9EFA26LL,0xAB624B1381959B73LL,0xD1D808DF00451FEALL,0x6F55C9730241C555LL,0x11887ABCBAE3D0A5LL}},{{0x5D8F6D015335EFA7LL,1UL,0x66AA2C4D427B10F5LL,6UL,4UL,0x0875E39D635EA5EELL,0x7D15C7D16E3A984FLL,0UL},{0x4B4CF3FDB9ECE949LL,18446744073709551615UL,0x0875E39D635EA5EELL,18446744073709551615UL,4UL,0x5D8F6D015335EFA7LL,0xEBDBE6C6F79AD954LL,0UL},{0x5D8F6D015335EFA7LL,0x256DB0726DE4C1B2LL,0x11C36A85D4C9828ELL,0UL,1UL,0x86D573599C4F0BA4LL,6UL,1UL},{0xAB624B1381959B73LL,18446744073709551609UL,1UL,0x4B4CF3FDB9ECE949LL,0UL,0UL,0x122687A14C54D5ABLL,18446744073709551615UL},{0x59AFA7BB1E311425LL,1UL,0xFD8F5C03BA9DEF1ELL,1UL,0xEBDBE6C6F79AD954LL,0xEBDBE6C6F79AD954LL,1UL,0xFD8F5C03BA9DEF1ELL}},{{18446744073709551615UL,18446744073709551615UL,0x383E9C240C9EFA26LL,0xD1D808DF00451FEALL,0x256DB0726DE4C1B2LL,0xFD8F5C03BA9DEF1ELL,18446744073709551609UL,0xACF1C41C638C37C2LL},{18446744073709551609UL,1UL,0x8848D1648C84018CLL,4UL,0xE9432A6FAF0B6337LL,0x4B4CF3FDB9ECE949LL,0x86D573599C4F0BA4LL,0xACF1C41C638C37C2LL},{1UL,0x6F55C9730241C555LL,0UL,0xD1D808DF00451FEALL,0x0875E39D635EA5EELL,1UL,18446744073709551615UL,0xFD8F5C03BA9DEF1ELL},{18446744073709551612UL,0x8848D1648C84018CLL,0xE9432A6FAF0B6337LL,1UL,0xAB624B1381959B73LL,18446744073709551615UL,1UL,18446744073709551615UL},{0xE9432A6FAF0B6337LL,0xFD8F5C03BA9DEF1ELL,0x6F55C9730241C555LL,0x4B4CF3FDB9ECE949LL,0xACF1C41C638C37C2LL,1UL,1UL,1UL}},{{1UL,0UL,0x256DB0726DE4C1B2LL,0UL,1UL,0xD7FB2EAC16B911CBLL,0x0875E39D635EA5EELL,0UL},{0x86D573599C4F0BA4LL,0UL,0x59AFA7BB1E311425LL,18446744073709551615UL,0x6F55C9730241C555LL,18446744073709551612UL,1UL,0UL},{0xD1D808DF00451FEALL,18446744073709551615UL,0x59AFA7BB1E311425LL,6UL,0UL,0x8848D1648C84018CLL,0x0875E39D635EA5EELL,0x11887ABCBAE3D0A5LL},{0x6F55C9730241C555LL,18446744073709551611UL,0x256DB0726DE4C1B2LL,0UL,0xD7FB2EAC16B911CBLL,0xEF45F098888BA70CLL,1UL,0x0875E39D635EA5EELL},{0UL,0x122687A14C54D5ABLL,0x6F55C9730241C555LL,0x66AA2C4D427B10F5LL,0x5D8F6D015335EFA7LL,18446744073709551609UL,1UL,18446744073709551612UL}},{{0x7D15C7D16E3A984FLL,18446744073709551615UL,0xE9432A6FAF0B6337LL,0UL,0UL,0xE9432A6FAF0B6337LL,18446744073709551615UL,0x7D15C7D16E3A984FLL},{0x122687A14C54D5ABLL,8UL,0UL,18446744073709551615UL,18446744073709551615UL,0x11887ABCBAE3D0A5LL,0x86D573599C4F0BA4LL,0x5D8F6D015335EFA7LL},{1UL,0x5D8F6D015335EFA7LL,0x8848D1648C84018CLL,18446744073709551615UL,18446744073709551615UL,0x11887ABCBAE3D0A5LL,18446744073709551609UL,1UL},{0xEF45F098888BA70CLL,8UL,0x383E9C240C9EFA26LL,18446744073709551612UL,0x8848D1648C84018CLL,0xE9432A6FAF0B6337LL,1UL,0xAB624B1381959B73LL},{1UL,18446744073709551615UL,0xFD8F5C03BA9DEF1ELL,0xEF45F098888BA70CLL,1UL,18446744073709551609UL,0x122687A14C54D5ABLL,0x69BF0E4A2BB54D2ALL}},{{18446744073709551611UL,0x122687A14C54D5ABLL,1UL,0x6F55C9730241C555LL,6UL,0xEF45F098888BA70CLL,6UL,0x6F55C9730241C555LL},{0x11C36A85D4C9828ELL,18446744073709551611UL,0x11C36A85D4C9828ELL,0x5D8F6D015335EFA7LL,0x7D15C7D16E3A984FLL,0x8848D1648C84018CLL,0xEBDBE6C6F79AD954LL,8UL},{0x256DB0726DE4C1B2LL,18446744073709551615UL,0x0875E39D635EA5EELL,1UL,1UL,18446744073709551612UL,0x7D15C7D16E3A984FLL,4UL},{0x256DB0726DE4C1B2LL,0UL,0x66AA2C4D427B10F5LL,0xFD8F5C03BA9DEF1ELL,0x7D15C7D16E3A984FLL,0UL,1UL,0x86D573599C4F0BA4LL},{8UL,0x8848D1648C84018CLL,18446744073709551609UL,0xEBDBE6C6F79AD954LL,18446744073709551615UL,0xE9432A6FAF0B6337LL,0xD7FB2EAC16B911CBLL,0UL}}};
        int32_t l_2897 = 1L;
        int32_t l_2899 = 4L;
        uint8_t l_2920 = 0xCBL;
        int32_t ***l_2931 = &g_132;
        int64_t ***l_2947[2];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2947[i] = &l_2593;
        for (g_218 = 0; (g_218 <= 1); g_218 += 1)
        { /* block id: 1370 */
            int32_t l_2892 = (-4L);
            int32_t l_2896 = 0L;
            int32_t l_2898[4][9][6] = {{{0xF77B25E4L,(-1L),4L,(-1L),1L,0x79504B4EL},{4L,(-1L),0L,0xD5330D02L,1L,0x4455B0DEL},{(-3L),(-1L),0xBE8E71C7L,0x8452B6E9L,(-1L),0L},{(-8L),(-1L),0x6C7C67BBL,0xC7EB9E32L,1L,0L},{0xD61FE913L,0x3451C279L,1L,(-4L),0xD5330D02L,(-1L)},{0L,(-1L),(-1L),(-6L),0x304C13A5L,(-1L)},{0x96AC451DL,0xD5330D02L,(-7L),0xD5330D02L,0x96AC451DL,4L},{0x8DD2F87FL,0x79504B4EL,7L,1L,(-10L),9L},{9L,0x7F27B21FL,0L,0x79504B4EL,1L,9L}},{{1L,(-1L),7L,0L,0xF77B25E4L,4L},{1L,0x9C0F563BL,(-7L),(-1L),0xB10DA035L,(-1L)},{0x3451C279L,(-1L),(-1L),0xBE8E71C7L,0xE8B673E4L,(-1L)},{1L,0xF77B25E4L,1L,0L,(-1L),0L},{9L,(-1L),0x6C7C67BBL,0x57F5AFC4L,7L,0L},{4L,4L,0xBE8E71C7L,4L,0xB5BF056AL,0x4455B0DEL},{0xBE8E71C7L,0x9C0F563BL,0L,(-6L),(-1L),0x79504B4EL},{1L,4L,4L,0xC7EB9E32L,0x3451C279L,0x3451C279L},{(-1L),1L,1L,(-1L),(-10L),0x8452B6E9L}},{{(-8L),(-1L),9L,4L,0xBE8E71C7L,0x8DD2F87FL},{0x96AC451DL,0x4455B0DEL,0xB5BF056AL,4L,0xBE8E71C7L,4L},{(-1L),(-1L),(-10L),(-1L),(-10L),0xF77B25E4L},{(-1L),1L,6L,0x79504B4EL,0x3451C279L,(-1L)},{1L,4L,7L,(-3L),(-1L),1L},{0L,0x9C0F563BL,0L,0xF77B25E4L,0xB5BF056AL,0x8DD2F87FL},{0x3451C279L,4L,0xF77B25E4L,0L,7L,(-1L)},{0x57F5AFC4L,(-1L),0xD383F60BL,6L,(-1L),1L},{(-1L),0xF77B25E4L,4L,0x57F5AFC4L,0xE8B673E4L,(-9L)}},{{0x8DD2F87FL,(-1L),0xBE8E71C7L,0x6C7C67BBL,0xB10DA035L,0x6C7C67BBL},{(-3L),0x9C0F563BL,(-3L),4L,0xF77B25E4L,0L},{1L,(-1L),0xD5330D02L,(-4L),1L,0x3451C279L},{9L,0xD61FE913L,0x52329993L,(-1L),4L,1L},{0xBE8E71C7L,0L,(-10L),1L,0x79504B4EL,0x57F5AFC4L},{6L,0x8DD2F87FL,(-8L),(-2L),(-9L),(-10L)},{(-1L),0x3451C279L,(-7L),0x8452B6E9L,0x8DD2F87FL,4L},{0xD5330D02L,0xC7EB9E32L,0L,0x7F27B21FL,(-1L),0xD5330D02L},{0x8452B6E9L,4L,0xB10DA035L,(-1L),0x4455B0DEL,(-2L)}}};
            union U1 **l_2919 = &g_490;
            union U1 ***l_2918 = &l_2919;
            union U1 ****l_2917[6][8][5] = {{{&l_2918,(void*)0,&l_2918,&l_2918,&l_2918},{(void*)0,&l_2918,&l_2918,(void*)0,&l_2918},{(void*)0,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2918,&l_2918,(void*)0,&l_2918,&l_2918},{&l_2918,(void*)0,&l_2918,(void*)0,&l_2918}},{{&l_2918,&l_2918,&l_2918,(void*)0,&l_2918},{&l_2918,&l_2918,(void*)0,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,&l_2918,&l_2918,(void*)0,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,(void*)0,&l_2918,&l_2918,&l_2918},{(void*)0,&l_2918,&l_2918,&l_2918,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2918,&l_2918}},{{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,(void*)0,&l_2918},{&l_2918,&l_2918,(void*)0,&l_2918,(void*)0},{&l_2918,&l_2918,(void*)0,&l_2918,&l_2918},{&l_2918,(void*)0,&l_2918,(void*)0,&l_2918},{&l_2918,(void*)0,&l_2918,(void*)0,&l_2918},{&l_2918,(void*)0,&l_2918,&l_2918,&l_2918},{(void*)0,&l_2918,&l_2918,(void*)0,&l_2918}},{{(void*)0,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2918,&l_2918,(void*)0,&l_2918,&l_2918},{&l_2918,(void*)0,&l_2918,(void*)0,&l_2918},{&l_2918,&l_2918,&l_2918,(void*)0,&l_2918},{(void*)0,&l_2918,&l_2918,(void*)0,(void*)0}},{{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,&l_2918,(void*)0,(void*)0,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,&l_2918,(void*)0,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,(void*)0,&l_2918,&l_2918,(void*)0},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,(void*)0,&l_2918,(void*)0,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{(void*)0,&l_2918,(void*)0,&l_2918,&l_2918},{&l_2918,&l_2918,&l_2918,&l_2918,&l_2918},{&l_2918,(void*)0,(void*)0,&l_2918,(void*)0},{&l_2918,&l_2918,&l_2918,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
            int i, j, k;
            (*g_132) = (void*)0;
            for (l_2473 = 6; (l_2473 >= 2); l_2473 -= 1)
            { /* block id: 1374 */
                uint8_t l_2900 = 0x24L;
                int32_t ****l_2932[9] = {&l_2931,&l_2931,&l_2931,&l_2931,&l_2931,&l_2931,&l_2931,&l_2931,&l_2931};
                uint64_t *l_2936 = &l_2884;
                int i;
                for (l_2884 = 0; (l_2884 <= 6); l_2884 += 1)
                { /* block id: 1377 */
                    int32_t l_2887 = 0x6C3A1417L;
                    int32_t l_2903 = 9L;
                    union U1 *****l_2916 = (void*)0;
                    uint64_t *l_2921[1][9] = {{(void*)0,&g_696.f3,&g_696.f3,(void*)0,&g_696.f3,&g_696.f3,(void*)0,&g_696.f3,&g_696.f3}};
                    int i, j;
                    for (g_1748.f1.f0 = 0; (g_1748.f1.f0 <= 2); g_1748.f1.f0 += 1)
                    { /* block id: 1380 */
                        int32_t l_2888 = 0x6BBE7C61L;
                        int i;
                        ++l_2889[1][4][6];
                        if (g_2612[l_2471])
                            continue;
                        if ((*g_581))
                            continue;
                    }
                    for (g_2444 = 0; (g_2444 <= 1); g_2444 += 1)
                    { /* block id: 1387 */
                        uint32_t l_2893 = 1UL;
                        --l_2893;
                        ++l_2900;
                        if (l_2889[0][1][1])
                            break;
                        l_2904++;
                    }
                    (*g_2654) = (safe_add_func_uint32_t_u_u((((l_2374[g_218][(g_218 + 3)] != (***g_2367)) >= (g_2741.f0 & (safe_lshift_func_uint16_t_u_s(((l_2898[1][8][5] = (((*g_1172) = (((p_4 > ((l_2887 &= (g_2612[(g_218 + 2)]--)) && (((((((*l_2371) == (*g_2367)) & ((**g_550) = p_3)) , p_5) || ((safe_sub_func_int64_t_s_s(((l_2917[0][5][3] = l_2915) != (void*)0), p_4)) >= g_2612[l_2471])) >= 0xE9B4BBE1L) || 65535UL))) || (-8L)) > p_5)) ^ l_2920)) ^ p_3), 8)))) , l_2922), l_2897));
                }
                (*g_2654) &= (g_2612[(g_218 + 2)] < (((l_2703[(g_218 + 1)] = (safe_rshift_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_u(0x4E7CL, (((g_2612[l_2471] , l_2703[g_218]) < ((l_2933 = l_2931) != l_2934)) & ((void*)0 != (*g_2538))))) && ((*l_2936) = (g_2935 , 0x396CE71206D68BEELL))) || p_4), 4))) <= 0L) < g_340.f0));
                if (p_4)
                    continue;
            }
        }
        for (l_2702 = 0; (l_2702 <= 1); l_2702 += 1)
        { /* block id: 1410 */
            uint8_t l_2937 = 0x6CL;
            uint16_t ****l_2954 = &g_1852[0][4][3];
            struct S0 *l_2956 = &g_2744[0];
            int32_t l_2979 = (-1L);
            uint32_t ***l_3008 = &l_2976;
            l_2937++;
            for (g_2551.f1.f3 = 0; (g_2551.f1.f3 <= 1); g_2551.f1.f3 += 1)
            { /* block id: 1414 */
                int64_t ***l_2948[1][6][10] = {{{&l_2593,&g_610,&l_2593,(void*)0,(void*)0,(void*)0,&l_2593,(void*)0,(void*)0,(void*)0},{&g_610,&g_610,&g_610,(void*)0,&l_2593,&l_2593,&l_2593,&l_2593,&g_610,&l_2593},{&g_610,(void*)0,&g_610,&g_610,&g_610,(void*)0,&g_610,&l_2593,&l_2593,&g_610},{&l_2593,&l_2593,&g_610,(void*)0,&g_610,&g_610,&g_610,(void*)0,&g_610,&l_2593},{&g_610,&l_2593,&l_2593,&l_2593,&l_2593,(void*)0,&g_610,&g_610,&g_610,(void*)0},{(void*)0,(void*)0,&l_2593,(void*)0,(void*)0,(void*)0,&l_2593,&g_610,&l_2593,&l_2593}}};
                uint8_t *l_2992 = &g_94;
                int i, j, k;
                (*g_2654) |= (safe_sub_func_uint8_t_u_u(0x7CL, ((((**g_1144) |= g_2612[l_2702]) >= 0L) , l_2944)));
                if (((((safe_mul_func_int16_t_s_s((**g_1144), p_3)) > p_4) , l_2947[1]) == l_2948[0][0][5]))
                { /* block id: 1417 */
                    uint16_t *****l_2955 = (void*)0;
                    int16_t l_2980 = 0L;
                    int32_t l_2995 = 0xE2B92FD3L;
                    if (((safe_add_func_uint8_t_u_u(((p_5 , (((!(p_5 >= ((***g_1180) >= (**g_1144)))) , (safe_sub_func_int16_t_s_s(((*l_2593) != (void*)0), p_3))) >= ((l_2419[2][2][1] = l_2954) != (void*)0))) <= p_5), p_3)) , 0xCB2AA37CL))
                    { /* block id: 1419 */
                        (**l_2934) = (g_2612[l_2702] , &p_5);
                        if ((***l_2931))
                            continue;
                    }
                    else
                    { /* block id: 1422 */
                        uint32_t **l_2977 = &g_551;
                        const int32_t l_2978 = 5L;
                        (**l_2413) = l_2956;
                        (*g_1172) &= ((safe_unary_minus_func_uint16_t_u(((safe_sub_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(p_3, (18446744073709551615UL >= g_386))), ((((+(l_2979 |= ((((--p_4) > (g_2077 = ((0UL == g_1550) == ((!(safe_div_func_uint8_t_u_u((!(l_2937 || (safe_lshift_func_uint16_t_u_s(((safe_mul_func_int8_t_s_s((((safe_sub_func_uint8_t_u_u(252UL, (p_3 ^ (&g_1141 == l_2975[7][8])))) , l_2976) != l_2977), p_5)) , p_3), 13)))), p_3))) & l_2978)))) & (**g_1144)) & l_2978))) & 65535UL) && l_2978) | l_2980))) || (-2L)))) >= p_5);
                        (**l_2934) = &p_5;
                    }
                    (*g_1172) &= (safe_lshift_func_int64_t_s_u(0L, ((((*g_2654) &= ((!(safe_add_func_int16_t_s_s((l_2979 = ((p_4 >= (safe_sub_func_int64_t_s_s((l_2995 ^= (safe_mod_func_uint16_t_u_u(((safe_rshift_func_uint64_t_u_s((((l_2992 != &p_4) == (((g_1562.f4 >= (l_2980 != l_2980)) || (***l_2931)) > (safe_mul_func_int8_t_s_s((***l_2931), 6L)))) >= l_2979), p_5)) , 5UL), p_5))), p_5))) ^ p_5)), 7UL))) & p_5)) != g_2612[l_2702]) , g_1562.f1)));
                    (**g_132) = l_2980;
                }
                else
                { /* block id: 1435 */
                    uint32_t ****l_3009[5][1] = {{&l_3008},{&g_676[0][0]},{&l_3008},{&g_676[0][0]},{&l_3008}};
                    int32_t l_3014 = 0L;
                    uint64_t l_3015 = 0UL;
                    int i, j;
                    l_3015 ^= (safe_rshift_func_uint8_t_u_u(p_4, (safe_mod_func_uint16_t_u_u((g_2612[l_2471] = (safe_div_func_int16_t_s_s((((((*g_551) = (&l_2992 != g_3002)) <= (safe_lshift_func_int16_t_s_u(l_2937, ((safe_mod_func_int16_t_s_s(((**g_1144) >= ((g_676[1][2] = l_3008) != (*g_1218))), (safe_mod_func_uint32_t_u_u((((*l_2992) = (safe_sub_func_int16_t_s_s(l_2937, p_3))) != p_3), 0x393DE254L)))) | p_5)))) & l_3014) , p_5), p_3))), p_4))));
                }
            }
            for (g_696.f1 = 0; (g_696.f1 <= 3); g_696.f1 += 1)
            { /* block id: 1445 */
                union U1 **l_3016 = &g_490;
                (*l_3016) = (*g_2109);
            }
        }
    }
    if (((safe_mul_func_uint16_t_u_u((safe_div_func_uint64_t_u_u(((((*g_2137) = (p_3 < 0xDF40L)) , (((*g_2137) &= (safe_add_func_uint8_t_u_u((safe_unary_minus_func_int64_t_s((safe_rshift_func_int8_t_s_u((safe_div_func_uint8_t_u_u((**g_3002), 0x62L)), 2)))), (safe_add_func_uint64_t_u_u((((*l_2382) = (p_3 != p_5)) == ((((p_3 || g_2387.f0) , ((safe_sub_func_int16_t_s_s((safe_add_func_int64_t_s_s(p_3, 18446744073709551615UL)), 1UL)) == p_4)) <= p_5) >= 0x4497B21D90DAA54BLL)), p_5))))) & p_3)) || 0xD84DL), 0x663A7DFBB719CDA4LL)), p_4)) > 0xFF365251L))
    { /* block id: 1453 */
        int32_t ***l_3036 = &g_132;
        int8_t **l_3048 = &g_2137;
        uint32_t **l_3049 = &g_551;
        int32_t l_3050 = 0x03391BACL;
        union U1 *l_3051 = &g_3052[5][2][0];
        int32_t ****l_3080 = &g_131;
        uint32_t l_3177[1][2][8] = {{{0UL,0UL,0x559E2FDDL,0x2FB0F679L,0x559E2FDDL,0UL,0UL,0x559E2FDDL},{0x437CFD4CL,0x559E2FDDL,0x559E2FDDL,0x437CFD4CL,4294967286UL,0x437CFD4CL,0x559E2FDDL,0x559E2FDDL}}};
        int8_t l_3235 = (-4L);
        int32_t l_3255[6][8] = {{0x20CB86EEL,0x031AE04AL,0L,(-1L),0xFC649632L,(-10L),(-10L),3L},{0x936DBF92L,0xFC649632L,0xFC649632L,0x936DBF92L,0x2DE5A1D9L,0L,0x84806CA7L,0x7CB4A6B4L},{(-10L),(-1L),0L,2L,0xFC649632L,1L,0x7CB4A6B4L,1L},{0x6D5307FEL,(-1L),0x84806CA7L,(-1L),0x6D5307FEL,0L,0x936DBF92L,0x20CB86EEL},{0L,0xFC649632L,0x6D5307FEL,1L,0L,0x2DE5A1D9L,(-1L),(-1L)},{0x7CB4A6B4L,2L,0x6D5307FEL,0x6D5307FEL,2L,0x7CB4A6B4L,0x936DBF92L,0L}};
        uint64_t *l_3272 = (void*)0;
        uint8_t l_3302 = 255UL;
        int32_t *l_3303[4][5][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
        int16_t l_3335 = 9L;
        int16_t l_3345 = (-1L);
        uint32_t l_3355[6] = {0xDD5399DFL,0xDD5399DFL,0xDD5399DFL,0xDD5399DFL,0xDD5399DFL,0xDD5399DFL};
        const uint16_t **l_3358 = (void*)0;
        const uint16_t ** const *l_3357 = &l_3358;
        const uint16_t ** const **l_3356 = &l_3357;
        int i, j, k;
        l_3051 = (((safe_rshift_func_int64_t_s_s(0x60EF48B12B40E4CBLL, 21)) , ((((void*)0 != l_3036) < (((*g_3003) < ((((g_1945.f0 <= 0x05E5CD6CC3A969B3LL) == (!p_3)) , ((((((safe_mul_func_int8_t_s_s(((safe_add_func_uint32_t_u_u((((((g_2387.f0 , (safe_add_func_uint8_t_u_u((((safe_mod_func_int8_t_s_s((safe_add_func_uint16_t_u_u((((l_3048 == l_3048) , l_3049) == l_3049), p_3)), p_5)) || (*g_1145)) , p_5), p_3))) == p_4) < 0xA155743FA188A094LL) && g_340.f0) && l_3050), (*g_581))) == g_204.f0), 0x67L)) & p_5) , (**g_1144)) , (-1L)) , 4294967295UL) == (-1L))) ^ p_5)) == g_78)) ^ p_3)) , (void*)0);
        for (g_482.f1.f1 = (-20); (g_482.f1.f1 <= (-30)); g_482.f1.f1 = safe_sub_func_uint64_t_u_u(g_482.f1.f1, 4))
        { /* block id: 1457 */
            int16_t l_3064 = 0xE242L;
            int16_t l_3075 = 0xB66FL;
            uint32_t ***l_3077 = &g_550;
            int32_t *l_3084[3];
            const int8_t ** const ** const *l_3093 = (void*)0;
            struct S0 *l_3107 = &g_696;
            const int32_t *l_3115 = &g_78;
            uint8_t l_3132[3][3][6] = {{{0x89L,252UL,0x6AL,0x6AL,252UL,0x89L},{0x2EL,0x89L,0UL,252UL,0UL,0x89L},{0UL,0x2EL,0x6AL,252UL,252UL,0x6AL}},{{0UL,0UL,252UL,252UL,1UL,252UL},{0x2EL,0UL,0x2EL,0x6AL,252UL,252UL},{0x89L,0x2EL,0x2EL,0x89L,0UL,252UL}},{{252UL,0x89L,252UL,0x89L,252UL,0x6AL},{0x89L,252UL,0x6AL,0x6AL,252UL,0x89L},{0x2EL,0x89L,0UL,252UL,0UL,0x89L}}};
            int32_t l_3135 = 0xEF27B0CAL;
            int32_t l_3140 = (-8L);
            uint16_t **l_3150 = &l_2381;
            uint64_t l_3188 = 0x2E9914651D261F03LL;
            int32_t l_3228 = 9L;
            uint64_t l_3229 = 0x01623332E3A3ECEALL;
            uint8_t **l_3237 = &l_2379[0][2];
            union U2 *l_3251 = &g_3252;
            uint8_t ***l_3350 = &l_3237;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_3084[i] = &g_832.f0;
            if (g_300.f0)
                goto lbl_3055;
            (*g_2654) |= (safe_rshift_func_uint32_t_u_s(((safe_sub_func_uint16_t_u_u((0xA5D9L | ((*l_2382) = ((p_3 , l_3060) > ((*g_2725) < ((*g_2137) | p_5))))), (safe_mod_func_int16_t_s_s(((*g_1145) &= ((0L <= (((0xFE0DD9DA5DCBE474LL & (((*l_3048) == l_3063) ^ p_4)) >= l_3064) > 0x2CL)) & p_4)), l_3064)))) < p_4), (*g_581)));
        }
        return (*g_1171);
    }
    else
    { /* block id: 1607 */
        uint64_t l_3371 = 0xD0D4F950FEE96586LL;
        union U2 ** const *l_3372 = &g_778;
        struct S0 *** const l_3377[4] = {&l_2414,&l_2414,&l_2414,&l_2414};
        int32_t l_3428 = (-9L);
        int32_t l_3429 = 8L;
        int32_t l_3430 = 1L;
        int32_t l_3433 = 0xE0A35970L;
        int32_t l_3436 = 0L;
        int32_t l_3438 = 0xC03673A9L;
        int32_t l_3439 = (-1L);
        int32_t l_3440 = 0L;
        int32_t l_3441 = 0x542F83E3L;
        int32_t l_3442 = 9L;
        int32_t l_3443 = 0x9291A46FL;
        int32_t l_3444[9] = {0x95B0CA09L,0x95B0CA09L,(-10L),0x95B0CA09L,0x95B0CA09L,(-10L),0x95B0CA09L,0x95B0CA09L,(-10L)};
        uint8_t l_3445[10][7] = {{0UL,0x98L,0UL,0x98L,0UL,0x98L,0UL},{255UL,8UL,8UL,255UL,255UL,8UL,8UL},{0x8DL,0x98L,0x8DL,0x98L,0x8DL,0x98L,0x8DL},{255UL,255UL,8UL,8UL,255UL,255UL,8UL},{0UL,0x98L,0UL,0x98L,0UL,0x98L,0UL},{255UL,8UL,8UL,255UL,255UL,8UL,8UL},{0x8DL,0x98L,0x8DL,0x98L,0x8DL,0x98L,0x8DL},{255UL,255UL,8UL,8UL,255UL,255UL,8UL},{0UL,0x98L,0UL,0x98L,0UL,0x98L,0UL},{255UL,8UL,8UL,255UL,255UL,8UL,8UL}};
        int16_t l_3486 = 8L;
        uint8_t ****l_3502 = &g_3248;
        int i, j;
        if ((safe_add_func_int16_t_s_s((l_2506 != (g_3367[0] , ((safe_sub_func_uint16_t_u_u((+0x36L), l_3371)) , l_3372))), (**g_1144))))
        { /* block id: 1608 */
            int32_t l_3386 = (-1L);
            int32_t l_3403 = 0L;
            int32_t **l_3425 = &g_2654;
            for (g_832.f0 = (-29); (g_832.f0 == (-28)); ++g_832.f0)
            { /* block id: 1611 */
                uint64_t l_3375 = 3UL;
                uint32_t ***l_3398 = &l_2976;
                int8_t l_3399 = (-1L);
                union U1 ** const l_3402 = &g_490;
                union U1 ** const *l_3401[8][1] = {{&l_3402},{&l_3402},{&l_3402},{&l_3402},{&l_3402},{&l_3402},{&l_3402},{&l_3402}};
                union U1 ** const **l_3400 = &l_3401[5][0];
                int32_t l_3407[4];
                union U1 ****l_3414[2];
                int i, j;
                for (i = 0; i < 4; i++)
                    l_3407[i] = 0x352D6232L;
                for (i = 0; i < 2; i++)
                    l_3414[i] = (void*)0;
                (*g_1172) ^= l_3375;
                (*g_1172) = (+(l_3377[0] != (((*g_2654) &= p_3) , ((safe_div_func_int64_t_s_s(((~(safe_unary_minus_func_uint32_t_u((((((safe_add_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u(((l_3386 = p_5) == (p_4++)), 2L)), (((safe_div_func_int64_t_s_s((safe_mul_func_int32_t_s_s((l_3375 >= (**g_1177)), 4294967291UL)), (((+(safe_rshift_func_int32_t_s_u((safe_lshift_func_int8_t_s_u((((((((-5L) == p_5) , (*g_1218)) == l_3398) > l_3399) , l_3400) != (void*)0), p_5)), l_3403))) == 0x0D328097L) , 18446744073709551615UL))) || 250UL) < p_3))) | g_43) && l_3399) , p_3) , 0x5C61A22CL)))) , p_3), 0xF67389DDB37CB430LL)) , l_3377[3]))));
                for (g_26.f2 = 0; (g_26.f2 == (-8)); g_26.f2--)
                { /* block id: 1619 */
                    uint32_t l_3408 = 0UL;
                    if ((safe_unary_minus_func_int8_t_s(((&g_1120 == &g_1120) <= l_3403))))
                    { /* block id: 1620 */
                        const int32_t **l_3411 = &g_581;
                        l_3408++;
                        if (p_3)
                            break;
                        (*g_3412) = ((*l_3411) = (*g_132));
                        (**l_2934) = &p_5;
                    }
                    else
                    { /* block id: 1626 */
                        uint32_t ***l_3421[1][7] = {{&l_2976,&l_2976,&l_2976,&l_2976,&l_2976,&l_2976,&l_2976}};
                        int32_t l_3424 = 0L;
                        int i, j;
                        (*g_2654) &= ((l_3414[0] != (void*)0) || (p_5 <= (0xBE5AE523B6EBB4C9LL > (safe_mul_func_uint16_t_u_u((safe_div_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((0xC1L <= ((void*)0 != l_3421[0][3])), ((safe_div_func_int32_t_s_s((l_3407[1] | p_5), p_5)) == 18446744073709551608UL))), p_3)), p_3)))));
                        (*g_1172) = 3L;
                        if (l_3424)
                            continue;
                    }
                }
            }
            (****g_333) = (void*)0;
            (*l_3425) = ((**l_2933) = func_71(l_3371));
        }
        else
        { /* block id: 1636 */
            int16_t l_3426 = 0L;
            int32_t l_3427 = (-6L);
            int16_t l_3432 = 0x413BL;
            int32_t l_3434 = (-4L);
            int32_t l_3435 = (-1L);
            int32_t l_3437[9][1][3] = {{{0xB57F6C2BL,0xA2D6B5E5L,(-3L)}},{{0xC0A9BB25L,0xC0A9BB25L,0x9D620A27L}},{{(-8L),0xA2D6B5E5L,0xA2D6B5E5L}},{{0x9D620A27L,0x4A694EFEL,(-1L)}},{{(-8L),(-6L),(-8L)}},{{0xC0A9BB25L,0x9D620A27L,(-1L)}},{{0xB57F6C2BL,0xB57F6C2BL,0xA2D6B5E5L}},{{(-7L),0x9D620A27L,0x9D620A27L}},{{0xA2D6B5E5L,(-6L),(-3L)}}};
            uint64_t *l_3487[6][1][9] = {{{&g_3284.f3,(void*)0,&g_2514.f3,&g_21,&g_3276,&g_3284.f3,&g_3276,&g_21,&g_2514.f3}},{{&g_21,&g_21,(void*)0,&g_3276,&g_3284.f3,(void*)0,&g_2514.f3,&g_21,&g_3276}},{{&g_2744[1].f3,&g_1572.f3,&g_3284.f3,&g_21,&g_1748.f1.f3,&g_1748.f1.f3,&g_21,&g_3284.f3,&g_1572.f3}},{{&g_1748.f1.f3,&g_2514.f3,(void*)0,&g_204.f3,&g_204.f3,&g_3276,&g_21,&g_2514.f3,&g_1572.f3}},{{&g_21,&g_2744[1].f3,&g_2514.f3,&g_1572.f3,&g_1156.f3,&g_1572.f3,&g_2514.f3,&g_2744[1].f3,&g_21}},{{&g_204.f3,&g_2514.f3,(void*)0,&g_1156.f3,&g_21,&g_1572.f3,&g_3276,(void*)0,&g_2744[1].f3}}};
            uint32_t ***l_3500 = (void*)0;
            int i, j, k;
            (*g_1172) &= (-7L);
            l_3445[2][6]--;
            l_3444[1] ^= (safe_rshift_func_uint8_t_u_s((safe_mod_func_int16_t_s_s(((safe_sub_func_uint16_t_u_u((+(l_3427 = ((safe_rshift_func_int64_t_s_s(l_3445[8][2], (safe_add_func_int16_t_s_s((safe_sub_func_uint64_t_u_u((safe_sub_func_int32_t_s_s(p_3, (safe_unary_minus_func_uint8_t_u(((l_3438 &= p_4) >= (l_3439 = (((!((safe_add_func_int32_t_s_s(((((void*)0 != &g_1144) > (safe_sub_func_int64_t_s_s(((safe_mod_func_int16_t_s_s((((safe_mul_func_uint32_t_u_u(l_3432, ((*l_3364) = (safe_div_func_uint64_t_u_u((safe_div_func_uint64_t_u_u(((!((safe_lshift_func_uint32_t_u_s(((((!p_5) & (safe_mod_func_int8_t_s_s(g_3483[0], (safe_div_func_int8_t_s_s(0x97L, (*g_2137)))))) || p_5) , p_4), l_3442)) ^ l_3433)) >= 0x8FFAL), l_3371)), p_3))))) >= l_3426) & 0xB914731AL), 0x2AA8L)) < l_3486), l_3442))) , (**g_2129)), (*g_1172))) <= l_3439)) ^ p_4) , 0x18L))))))), (*g_611))), 0xD328L)))) , 0xD41DE77CD00149C0LL))), (*g_1145))) <= (*g_1145)), p_3)), l_3442));
            if (p_5)
            { /* block id: 1644 */
                if (l_3437[6][0][0])
                { /* block id: 1645 */
                    uint32_t l_3488 = 4294967288UL;
                    for (g_791.f0 = 3; (g_791.f0 >= 0); g_791.f0 -= 1)
                    { /* block id: 1648 */
                        int i;
                        (*g_2654) |= g_231[g_791.f0];
                        l_3488--;
                        if (l_3438)
                            continue;
                    }
                    for (g_1159 = 0; (g_1159 >= 0); g_1159 -= 1)
                    { /* block id: 1655 */
                        volatile union U1 ** volatile **l_3492 = (void*)0;
                        volatile union U1 ** volatile **l_3493 = (void*)0;
                        int32_t l_3495 = 1L;
                        (*g_3494) = (g_3491 , &g_1121[0][1][2]);
                        (*l_3364) = (l_3495 ^ 1L);
                    }
                    l_3496 = (****g_333);
                }
                else
                { /* block id: 1660 */
                    for (g_3116.f2 = 0; (g_3116.f2 != (-12)); --g_3116.f2)
                    { /* block id: 1663 */
                        int32_t *l_3499[4][8] = {{(void*)0,&l_2476,&g_585.f0,&l_2476,(void*)0,&l_2476,&g_585.f0,&l_2476},{(void*)0,&l_2476,&g_585.f0,&l_2476,(void*)0,&l_2476,&g_585.f0,&l_2476},{(void*)0,&l_2476,&g_585.f0,&l_2476,(void*)0,&l_2476,&g_585.f0,&l_2476},{(void*)0,&l_2476,&g_585.f0,&l_2476,(void*)0,&l_2476,&g_585.f0,&l_2476}};
                        int i, j;
                        return (*g_1171);
                    }
                    return (*g_2129);
                }
                l_3435 = ((p_5 || (l_3433 = ((void*)0 == l_2506))) <= (l_3500 == (void*)0));
            }
            else
            { /* block id: 1670 */
                int32_t *l_3501 = &l_3430;
                return (*g_1171);
            }
        }
        (*g_3503) = l_3502;
    }
    return (*g_2129);
}


/* ------------------------------------------ */
/* 
 * reads : g_2071.f2 g_776 g_1178 g_1179 g_343 g_2137 g_1550 g_1145 g_1146 g_550 g_551 g_204.f1
 * writes: g_2071.f2 g_1748.f1.f3 g_94 g_383.f1 g_1229.f0 g_777 g_343 g_93 g_204.f1 g_1872.f2
 */
static int16_t  func_14(uint32_t  p_15, int32_t  p_16, int32_t  p_17, uint16_t  p_18)
{ /* block id: 1035 */
    int32_t l_2152 = 0x3BFBF7A8L;
    int32_t l_2156[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
    int8_t l_2221 = (-2L);
    const union U2 *l_2247 = &g_681;
    const union U2 **l_2246 = &l_2247;
    const union U2 ***l_2245 = &l_2246;
    uint32_t **l_2341 = &g_1990;
    int i;
    for (g_2071.f2 = 0; (g_2071.f2 <= 1); g_2071.f2 += 1)
    { /* block id: 1038 */
        uint32_t l_2128 = 0x90288987L;
        int32_t l_2139[4][2] = {{(-9L),(-9L)},{(-9L),(-9L)},{(-9L),(-9L)},{(-9L),(-9L)}};
        uint16_t l_2140 = 0xA4A2L;
        int8_t l_2171 = (-5L);
        union U2 ***l_2195[10][6] = {{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778},{(void*)0,&g_778,(void*)0,&g_778,(void*)0,&g_778},{&g_778,&g_778,&g_778,(void*)0,&g_778,(void*)0},{&g_778,&g_778,&g_778,&g_778,(void*)0,(void*)0},{(void*)0,&g_778,(void*)0,(void*)0,&g_778,&g_778},{&g_778,(void*)0,&g_778,(void*)0,&g_778,&g_778},{(void*)0,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,(void*)0,&g_778,&g_778},{&g_778,(void*)0,&g_778,&g_778,&g_778,&g_778}};
        int32_t l_2275 = (-4L);
        uint32_t **l_2285 = (void*)0;
        uint32_t ***l_2284[5];
        uint16_t l_2313 = 65526UL;
        const uint32_t l_2331 = 0xF2F2D38EL;
        int i, j;
        for (i = 0; i < 5; i++)
            l_2284[i] = &l_2285;
        for (g_1748.f1.f3 = 0; (g_1748.f1.f3 <= 8); g_1748.f1.f3 += 1)
        { /* block id: 1041 */
            uint32_t l_2121 = 0xA1A10EF5L;
            int32_t l_2149 = (-2L);
            int32_t l_2153 = 0x06D0A9D0L;
            int32_t l_2157 = 0x1215D904L;
            int32_t l_2163 = 1L;
            int32_t l_2164 = 0L;
            int32_t l_2170 = 4L;
            int8_t **l_2200 = &g_2137;
            int8_t ***l_2199[8][7] = {{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200},{&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200,&l_2200}};
            int8_t ****l_2198[3];
            int32_t l_2219 = 0xEB73E12FL;
            union U2 ***l_2258[5][9] = {{&g_778,(void*)0,&g_778,(void*)0,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,(void*)0,&g_778,(void*)0,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778,&g_778},{&g_778,(void*)0,&g_778,(void*)0,&g_778,&g_778,&g_778,&g_778,&g_778}};
            int i, j;
            for (i = 0; i < 3; i++)
                l_2198[i] = &l_2199[5][0];
            for (g_94 = 0; (g_94 <= 1); g_94 += 1)
            { /* block id: 1044 */
                int32_t *l_2120[5][6] = {{&g_79.f0,&g_1496[3][2][0].f0,&g_1496[3][2][0].f0,&g_79.f0,&g_79.f0,&g_1496[3][2][0].f0},{&g_79.f0,&g_79.f0,&g_1496[3][2][0].f0,&g_1496[3][2][0].f0,&g_79.f0,&g_79.f0},{&g_79.f0,&g_1496[3][2][0].f0,&g_1496[3][2][0].f0,&g_79.f0,&g_79.f0,&g_1496[3][2][0].f0},{&g_79.f0,&g_79.f0,&g_1496[3][2][0].f0,&g_1496[3][2][0].f0,&g_79.f0,&g_79.f0},{&g_79.f0,&g_1496[3][2][0].f0,&g_1496[3][2][0].f0,&g_79.f0,&g_79.f0,&g_1496[3][2][0].f0}};
                int i, j;
                l_2121--;
            }
            for (g_383.f1 = 0; (g_383.f1 <= 1); g_383.f1 += 1)
            { /* block id: 1049 */
                uint16_t l_2141 = 0x27D6L;
                int32_t l_2145 = (-1L);
                int32_t l_2151 = (-6L);
                int32_t l_2155 = 1L;
                int8_t l_2158 = (-1L);
                int32_t l_2159 = 0x34F8ADAEL;
                int32_t l_2160 = 0x08F4D3C5L;
                int32_t l_2162[4][10] = {{(-10L),0xF46C7DDDL,0x4FC56E31L,0xF46C7DDDL,(-10L),(-10L),0xF46C7DDDL,0x4FC56E31L,0xF46C7DDDL,(-10L)},{(-10L),0xF46C7DDDL,0x4FC56E31L,(-1L),0xF46C7DDDL,0xF46C7DDDL,(-1L),1L,(-1L),0xF46C7DDDL},{0xF46C7DDDL,(-1L),1L,(-1L),0xF46C7DDDL,0xF46C7DDDL,(-1L),1L,(-1L),0xF46C7DDDL},{0xF46C7DDDL,(-1L),1L,(-1L),0xF46C7DDDL,0xF46C7DDDL,(-1L),1L,(-1L),0xF46C7DDDL}};
                int32_t l_2196[5] = {(-4L),(-4L),(-4L),(-4L),(-4L)};
                uint8_t *l_2210[1];
                union U2 ** const *l_2249 = &g_778;
                union U2 ****l_2259 = &l_2195[2][2];
                uint16_t *l_2262 = &g_343[4][2];
                int32_t *l_2263 = &g_204.f1;
                int i, j;
                for (i = 0; i < 1; i++)
                    l_2210[i] = &g_2077;
                for (g_1229.f0 = 0; (g_1229.f0 >= 0); g_1229.f0 -= 1)
                { /* block id: 1052 */
                    int8_t *l_2134 = &g_300.f0;
                    struct S0 *l_2138 = &g_383;
                    int32_t l_2144 = 0x2D9385D2L;
                    int32_t l_2154 = 0xDD8A072AL;
                    int32_t l_2161 = (-8L);
                    int32_t l_2165 = 0x12F888F8L;
                    int32_t l_2166 = 0x5626E2A3L;
                    int32_t l_2167 = (-1L);
                    int32_t l_2168 = 0x19CBEABBL;
                    int32_t l_2169[5][4][10] = {{{0x8C05C279L,0L,0xE6346C96L,1L,1L,(-9L),0xA51D05CAL,(-2L),0x0BD23DEDL,0xF90F1947L},{0L,(-1L),0xFCC7CD65L,0x77C66194L,1L,1L,0x77C66194L,0xFCC7CD65L,(-1L),0L},{(-9L),0x7FF7ADA9L,1L,0xFCC7CD65L,0L,(-4L),4L,8L,(-1L),0x46C96B7EL},{0xA51D05CAL,0L,0xB1F7DBBCL,0x7FF7ADA9L,0L,0x873C6848L,0xF90F1947L,0L,8L,0L}},{{0L,4L,0x638D2FA5L,1L,1L,1L,(-2L),0x75AEDEFFL,(-2L),0xF90F1947L},{0x638D2FA5L,(-1L),(-1L),2L,1L,(-1L),1L,1L,1L,1L},{(-1L),0L,1L,0x7FF7ADA9L,1L,0xB1F7DBBCL,0x8C05C279L,1L,(-2L),1L},{0x7FF7ADA9L,2L,9L,1L,9L,2L,0x7FF7ADA9L,0x638D2FA5L,(-1L),0L}},{{1L,(-2L),(-1L),1L,0x8C05C279L,9L,0x638D2FA5L,0L,0x75AEDEFFL,0x638D2FA5L},{0xDFC1AB75L,(-2L),(-9L),1L,(-2L),(-2L),0x7FF7ADA9L,0L,0L,0L},{0L,2L,(-2L),0xDFC1AB75L,1L,1L,0x8C05C279L,(-1L),(-9L),(-1L)},{(-7L),0L,1L,0x75AEDEFFL,0L,(-1L),1L,1L,4L,1L}},{{0xDFC1AB75L,(-1L),7L,0xF6E8625BL,0xA51D05CAL,1L,(-2L),(-2L),1L,0xA51D05CAL},{(-5L),4L,4L,(-5L),(-7L),2L,0xF90F1947L,(-1L),1L,(-1L)},{9L,0L,0x1B4F0FEAL,9L,0x77C66194L,(-2L),4L,0xF90F1947L,1L,9L},{(-1L),0x7FF7ADA9L,0L,(-5L),(-2L),(-7L),0x77C66194L,0x8C05C279L,0L,1L}},{{1L,(-2L),0x0BD23DEDL,0x1B4F0FEAL,0L,0x9046377FL,0x46C96B7EL,0L,0L,0x75AEDEFFL},{8L,1L,0xA1834268L,0xE6346C96L,(-7L),(-7L),(-9L),9L,0L,1L},{0x46C96B7EL,(-2L),0xA51D05CAL,0xFCC7CD65L,0x638D2FA5L,0x77C66194L,0x638D2FA5L,0xFCC7CD65L,0xA51D05CAL,(-2L)},{2L,8L,0L,1L,1L,0L,0x46C96B7EL,0x8549E061L,0xE6346C96L,9L}}};
                    uint8_t l_2220 = 249UL;
                    int32_t *l_2222 = &g_78;
                    const int8_t *l_2233[2][6] = {{(void*)0,&l_2171,(void*)0,(void*)0,&l_2171,(void*)0},{(void*)0,&l_2171,(void*)0,(void*)0,&l_2171,(void*)0}};
                    const int8_t **l_2232 = &l_2233[0][4];
                    const int8_t ***l_2231 = &l_2232;
                    const int8_t **** const l_2230 = &l_2231;
                    const int8_t **** const *l_2229 = &l_2230;
                    uint32_t ***l_2244 = &g_550;
                    int i, j, k;
                }
                (*l_2263) ^= (safe_add_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((((((((*g_776) = ((*l_2259) = l_2258[3][4])) != (void*)0) ^ (l_2139[3][1] = ((-9L) > l_2156[7]))) , (safe_div_func_uint32_t_u_u(((**g_550) = (((*l_2262) ^= (*g_1178)) , (((l_2171 > (*g_2137)) || (((*g_1145) ^ l_2170) ^ p_17)) , 4294967295UL))), l_2153))) != p_18) & 0xE9L), 9)), 0x430BB31DA38A80EDLL));
            }
        }
        for (g_1872.f2 = 1; (g_1872.f2 >= 0); g_1872.f2 -= 1)
        { /* block id: 1103 */
            int32_t **l_2269 = &g_1172;
            uint32_t ****l_2274 = &g_676[3][4];
            int32_t l_2305[10][4] = {{(-1L),(-1L),(-1L),0L},{0x439D55C4L,(-1L),0x17715F9DL,(-1L)},{0x439D55C4L,0L,(-1L),(-1L)},{(-1L),(-1L),(-1L),0L},{0x439D55C4L,(-1L),0x17715F9DL,(-1L)},{0x439D55C4L,0L,(-1L),(-1L)},{(-1L),(-1L),(-1L),0L},{0x439D55C4L,(-1L),0x17715F9DL,(-1L)},{0x439D55C4L,0L,(-1L),(-1L)},{(-1L),(-1L),(-1L),0L}};
            union U1 *l_2309 = &g_832;
            const int16_t *l_2353 = &g_43;
            const int16_t **l_2352 = &l_2353;
            const int16_t ***l_2351 = &l_2352;
            int i, j;
        }
    }
    return p_18;
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_11 g_26.f0 g_42 g_2071 g_2072 g_2054 g_1606 g_1577.f0 g_581 g_204.f1 g_2077 g_132 g_83 g_78 g_2081 g_2082 g_594 g_986.f0 g_551 g_1172 g_333 g_334 g_335 g_336 g_337 g_2109 g_130 g_131 g_1919 g_610 g_611 g_442 g_1178 g_1179 g_383.f1
 * writes: g_29 g_2077 g_78 g_204.f3 g_343 g_93 g_383.f1 g_337 g_490 g_132 g_1919 g_26.f1.f1 g_83 g_442
 */
static int8_t  func_22(uint16_t  p_23, uint16_t  p_24, int32_t  p_25)
{ /* block id: 2 */
    int32_t *l_27 = (void*)0;
    int32_t *l_28[8][9][1] = {{{&g_11},{&g_11},{&g_11},{&g_11},{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11}},{{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11},{&g_11},{&g_11},{&g_11},{(void*)0}},{{(void*)0},{&g_11},{&g_11},{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11},{&g_11}},{{&g_11},{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11},{&g_11},{(void*)0},{(void*)0}},{{&g_11},{&g_11},{&g_11},{&g_11},{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11}},{{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11},{&g_11},{&g_11},{&g_11},{(void*)0}},{{(void*)0},{&g_11},{&g_11},{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11},{&g_11}},{{&g_11},{&g_11},{(void*)0},{(void*)0},{&g_11},{&g_11},{&g_11},{(void*)0},{(void*)0}}};
    int i, j, k;
    g_29 |= (-3L);
    if (p_24)
    { /* block id: 4 */
        uint16_t l_30 = 65535UL;
        ++l_30;
    }
    else
    { /* block id: 6 */
        int8_t l_2117 = (-10L);
        int32_t *l_2118 = &g_585.f0;
        l_2117 &= func_33(g_11, &g_11, p_24, g_26.f0);
        l_2118 = l_28[2][1][0];
        (*g_132) = l_27;
    }
    (*g_1172) &= (p_25 , (p_25 & (((**g_610) &= p_24) , (0UL > (*g_1178)))));
    return p_25;
}


/* ------------------------------------------ */
/* 
 * reads : g_42 g_2071 g_2072 g_2054 g_1606 g_1577.f0 g_581 g_204.f1 g_2077 g_132 g_83 g_78 g_2081 g_2082 g_594 g_986.f0 g_551 g_1172 g_333 g_334 g_335 g_336 g_337 g_2109 g_130 g_131 g_1919 g_11
 * writes: g_2077 g_78 g_204.f3 g_343 g_93 g_383.f1 g_337 g_490 g_132 g_1919 g_26.f1.f1
 */
static int32_t  func_33(int32_t  p_34, int32_t * p_35, uint16_t  p_36, uint8_t  p_37)
{ /* block id: 7 */
    int32_t *l_38 = &g_26.f1.f1;
    int32_t *l_39 = &g_26.f1.f1;
    int32_t *l_40[6][6][6] = {{{&g_11,&g_11,&g_11,(void*)0,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,(void*)0},{&g_26.f1.f1,&g_11,&g_11,&g_11,(void*)0,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,(void*)0,&g_11,&g_11,&g_11}},{{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,(void*)0,&g_11},{&g_11,(void*)0,&g_11,&g_11,&g_11,(void*)0},{(void*)0,&g_11,&g_11,&g_11,(void*)0,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_26.f1.f1,(void*)0},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11}},{{&g_11,&g_11,(void*)0,&g_11,&g_11,&g_11},{(void*)0,&g_11,&g_11,&g_11,&g_11,&g_11},{(void*)0,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_26.f1.f1,(void*)0,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,(void*)0}},{{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{(void*)0,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_26.f1.f1,&g_11,&g_11,(void*)0,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,(void*)0},{&g_11,&g_11,&g_11,(void*)0,&g_11,&g_11}},{{(void*)0,&g_11,&g_11,&g_11,&g_26.f1.f1,(void*)0},{&g_11,&g_11,&g_11,&g_11,(void*)0,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,(void*)0},{&g_11,(void*)0,&g_11,(void*)0,(void*)0,&g_11},{&g_11,&g_11,(void*)0,&g_11,&g_11,&g_11},{&g_11,&g_11,(void*)0,&g_11,&g_11,(void*)0}},{{&g_11,&g_11,(void*)0,&g_11,&g_11,&g_11},{&g_11,&g_11,(void*)0,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11,(void*)0,&g_11,(void*)0},{&g_11,&g_11,&g_11,&g_11,&g_11,&g_11},{&g_11,&g_11,&g_11,&g_11,&g_11,(void*)0},{&g_11,&g_11,&g_11,(void*)0,&g_11,&g_11}}};
    uint64_t l_45 = 7UL;
    uint64_t l_50 = 0xB19957B093197C82LL;
    int i, j, k;
    --l_45;
    if (l_45)
        goto lbl_2110;
lbl_2110:
    (*g_131) = func_48(l_50);
    for (g_1919 = 13; (g_1919 < 35); g_1919 = safe_add_func_int32_t_s_s(g_1919, 1))
    { /* block id: 1023 */
        uint32_t ***l_2115 = &g_550;
        int32_t l_2116 = (-1L);
        (*l_39) = ((safe_add_func_uint64_t_u_u(((void*)0 == l_2115), p_34)) , ((l_2116 , &g_1177) == &g_1853[0]));
    }
    return (*p_35);
}


/* ------------------------------------------ */
/* 
 * reads : g_42 g_2071 g_2072 g_2054 g_1606 g_1577.f0 g_581 g_204.f1 g_2077 g_132 g_83 g_78 g_2081 g_2082 g_594 g_986.f0 g_551 g_1172 g_333 g_334 g_335 g_336 g_337 g_2109 g_130 g_131
 * writes: g_2077 g_78 g_204.f3 g_343 g_93 g_383.f1 g_337 g_490
 */
static int32_t ** func_48(uint32_t  p_49)
{ /* block id: 9 */
    int64_t l_1960[3];
    int32_t l_1964 = 0x8285F0DFL;
    int32_t l_1966 = (-1L);
    int32_t l_1969 = 0x4435AFDBL;
    int32_t l_1971 = 0x21C29928L;
    int32_t l_1973[10];
    union U2 *l_1997 = &g_1998;
    union U2 * const * const l_1996 = &l_1997;
    union U2 * const * const *l_1995[9][6][4] = {{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}},{{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996},{&l_1996,&l_1996,&l_1996,&l_1996}}};
    int32_t l_1999[3];
    union U1 *l_2000 = &g_1939;
    uint64_t *l_2044[8][7][4] = {{{&g_300.f3,&g_340.f3,(void*)0,&g_383.f3},{&g_109,(void*)0,&g_383.f3,&g_21},{&g_21,&g_21,&g_340.f3,&g_340.f3},{&g_383.f3,&g_109,&g_204.f3,&g_340.f3},{&g_383.f3,&g_1572.f3,&g_383.f3,&g_1156.f3},{&g_696.f3,&g_21,&g_696.f3,&g_21},{&g_696.f3,(void*)0,&g_300.f3,&g_21}},{{(void*)0,(void*)0,&g_300.f3,(void*)0},{&g_696.f3,(void*)0,&g_696.f3,(void*)0},{&g_696.f3,&g_204.f3,&g_383.f3,&g_696.f3},{&g_383.f3,&g_696.f3,&g_204.f3,(void*)0},{&g_383.f3,&g_340.f3,&g_340.f3,(void*)0},{&g_21,(void*)0,&g_383.f3,&g_300.f3},{&g_109,(void*)0,(void*)0,&g_340.f3}},{{&g_300.f3,&g_383.f3,&g_696.f3,&g_1156.f3},{&g_383.f3,(void*)0,(void*)0,&g_204.f3},{&g_696.f3,&g_21,&g_21,&g_1572.f3},{(void*)0,&g_21,&g_300.f3,&g_300.f3},{&g_1572.f3,&g_1572.f3,&g_21,(void*)0},{&g_1156.f3,&g_21,&g_300.f3,(void*)0},{&g_696.f3,&g_696.f3,(void*)0,&g_300.f3}},{{&g_340.f3,&g_696.f3,&g_383.f3,(void*)0},{&g_696.f3,&g_21,&g_340.f3,(void*)0},{&g_109,&g_1572.f3,(void*)0,&g_300.f3},{&g_383.f3,&g_21,&g_1156.f3,&g_1572.f3},{&g_300.f3,&g_21,&g_204.f3,&g_204.f3},{&g_340.f3,(void*)0,(void*)0,&g_1156.f3},{&g_204.f3,&g_383.f3,&g_21,&g_340.f3}},{{&g_696.f3,(void*)0,&g_21,&g_300.f3},{&g_1156.f3,(void*)0,&g_383.f3,(void*)0},{&g_1156.f3,&g_340.f3,&g_696.f3,(void*)0},{&g_696.f3,&g_696.f3,(void*)0,&g_696.f3},{(void*)0,&g_204.f3,&g_383.f3,(void*)0},{&g_383.f3,(void*)0,(void*)0,(void*)0},{&g_383.f3,(void*)0,&g_1156.f3,&g_21}},{{&g_383.f3,(void*)0,(void*)0,&g_21},{&g_383.f3,&g_21,&g_383.f3,&g_1156.f3},{(void*)0,&g_1572.f3,(void*)0,&g_340.f3},{&g_696.f3,&g_109,&g_696.f3,&g_340.f3},{&g_1156.f3,&g_21,&g_383.f3,&g_21},{&g_1156.f3,(void*)0,&g_21,&g_383.f3},{&g_696.f3,&g_696.f3,&g_1572.f3,&g_383.f3}},{{&g_1156.f3,&g_21,&g_21,&g_1572.f3},{(void*)0,(void*)0,&g_1156.f3,&g_383.f3},{&g_383.f3,&g_696.f3,(void*)0,(void*)0},{&g_383.f3,&g_204.f3,&g_204.f3,&g_383.f3},{(void*)0,&g_383.f3,&g_1156.f3,&g_696.f3},{&g_340.f3,&g_1572.f3,&g_204.f3,&g_204.f3},{(void*)0,&g_1572.f3,(void*)0,&g_204.f3}},{{&g_340.f3,&g_1572.f3,&g_383.f3,&g_696.f3},{(void*)0,&g_383.f3,&g_1156.f3,&g_383.f3},{&g_1156.f3,&g_204.f3,(void*)0,(void*)0},{&g_383.f3,&g_696.f3,&g_1572.f3,&g_383.f3},{(void*)0,(void*)0,(void*)0,&g_1572.f3},{(void*)0,&g_21,&g_340.f3,&g_383.f3},{&g_383.f3,&g_696.f3,&g_383.f3,(void*)0}}};
    const int32_t *l_2053 = &g_2054;
    int8_t *l_2063 = &g_126;
    int8_t * const * const l_2062 = &l_2063;
    int8_t * const * const *l_2061[3][6] = {{&l_2062,(void*)0,&l_2062,&l_2062,&l_2062,(void*)0},{&l_2062,&l_2062,&l_2062,&l_2062,&l_2062,&l_2062},{&l_2062,&l_2062,&l_2062,&l_2062,&l_2062,&l_2062}};
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1960[i] = 0x023E9F26A454185CLL;
    for (i = 0; i < 10; i++)
        l_1973[i] = 0xA5DE9F05L;
    for (i = 0; i < 3; i++)
        l_1999[i] = (-5L);
    if ((safe_div_func_int64_t_s_s(g_42, p_49)))
    { /* block id: 10 */
        uint16_t l_55 = 0x7125L;
        struct S0 * const l_695 = &g_696;
        struct S0 * const *l_694 = &l_695;
        struct S0 * const **l_693[5][8][6] = {{{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694}},{{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694}},{{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694}},{{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694}},{{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694},{&l_694,&l_694,&l_694,&l_694,&l_694,&l_694}}};
        int32_t l_1967 = (-5L);
        int32_t l_1968 = 0xA973E977L;
        int32_t l_1970 = 0xA07867A9L;
        int32_t l_1972[8];
        uint8_t l_1974[9];
        int32_t *l_2042 = &l_1999[2];
        uint32_t l_2078 = 4294967289UL;
        uint16_t *l_2105 = &g_343[2][0];
        int i, j, k;
        for (i = 0; i < 8; i++)
            l_1972[i] = 0x52CE8E2AL;
        for (i = 0; i < 9; i++)
            l_1974[i] = 0xECL;
        for (p_49 = 16; (p_49 > 37); ++p_49)
        { /* block id: 13 */
            uint16_t l_692 = 1UL;
            int32_t l_1962 = 0L;
            int32_t l_1963[8][3];
            int64_t l_1965 = 2L;
            uint8_t l_1994[9];
            union U1 **l_2012 = &l_2000;
            union U1 ***l_2011[2][4] = {{&l_2012,&l_2012,&l_2012,&l_2012},{&l_2012,&l_2012,&l_2012,&l_2012}};
            uint32_t l_2022 = 0UL;
            uint64_t * const l_2041 = &g_383.f3;
            const int32_t *l_2055 = &g_1161.f0;
            int i, j;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1963[i][j] = 3L;
            }
            for (i = 0; i < 9; i++)
                l_1994[i] = 0xACL;
        }
        (**g_132) = ((safe_add_func_int32_t_s_s(((((safe_div_func_uint8_t_u_u((l_2061[0][1] == (void*)0), ((safe_mul_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((p_49 , (g_2077 &= (safe_rshift_func_int16_t_s_s((~(g_2071 , ((l_1971 = (g_2072 , ((&l_2062 != &g_1605) <= (safe_div_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_s(((*l_2053) && (*l_2053)), 1)) ^ p_49), (*g_1606)))))) ^ (*g_581)))), (*l_2053))))), 0x6B8DL)), (**g_132))) , l_2078))) , 0UL) ^ (-9L)) & l_1968), p_49)) , p_49);
        (**g_132) ^= (safe_mul_func_int16_t_s_s(((g_2081 , g_2082) , (*l_2053)), 1L));
        (*g_1172) = ((0xBFC8DD1A724B8FCELL ^ (safe_mul_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(1L, (safe_add_func_uint32_t_u_u(p_49, ((safe_mul_func_uint8_t_u_u((((*g_551) = (safe_mod_func_int8_t_s_s(((safe_sub_func_uint8_t_u_u(p_49, p_49)) ^ (safe_lshift_func_uint16_t_u_u((safe_rshift_func_int32_t_s_s(p_49, (safe_mod_func_uint64_t_u_u(g_594[0], (g_204.f3 = (*l_2053)))))), ((*l_2105) = (safe_div_func_uint8_t_u_u((safe_lshift_func_int32_t_s_u((((p_49 ^ 0x98539BBDL) > 0xA5C62C9D7E3ACF82LL) >= (*l_2053)), p_49)), g_986.f0)))))), 0xB2L))) != p_49), (*l_2053))) , p_49))))), 0xE20971FFL))) <= l_1968);
    }
    else
    { /* block id: 1012 */
        volatile int32_t * volatile l_2106[1][7] = {{&g_1123.f0,&g_224.f0,&g_224.f0,&g_1123.f0,&g_224.f0,&g_224.f0,&g_1123.f0}};
        union U1 *l_2108 = &g_2081;
        int i, j;
lbl_2107:
        l_2106[0][0] = (****g_333);
        (****g_333) = l_2106[0][6];
        if (g_78)
            goto lbl_2107;
        (*g_2109) = l_2108;
    }
    return (**g_130);
}


/* ------------------------------------------ */
/* 
 * reads : g_300.f1 g_1604 g_383 g_1156.f0 g_1145 g_1146 g_132 g_832.f0 g_83 g_1649 g_94 g_696.f3 g_336 g_337 g_335 g_1577.f5 g_1141 g_830.f3 g_1550 g_126 g_21 g_1172 g_384 g_385 g_386 g_1555 g_1218 g_1219 g_550 g_551 g_131 g_200 g_1156.f1 g_1144 g_340.f3 g_333 g_334 g_491.f0 g_594 g_1726 g_1180 g_1177 g_1178 g_1179 g_204.f3 g_93 g_1140 g_300.f0 g_585.f0 g_340.f0 g_130 g_1327.f1.f0 g_1048.f0 g_696.f2 g_1820 g_632 g_1831 g_1228.f0 g_1842 g_1852 g_1171 g_1872 g_995.f0 g_1572.f1 g_26.f2 g_1919 g_1674.f0 g_79 g_11 g_78 g_79.f0 g_109 g_145 g_148 g_43 g_148.f0 g_190 g_42 g_205 g_211 g_218 g_1229.f0 g_1926 g_1939 g_1945 g_581 g_204.f1 g_1948
 * writes: g_300.f1 g_83 g_832.f0 g_493.f0 g_830.f3 g_94 g_337 g_1550 g_383.f1 g_93 g_190 g_1146 g_340.f3 g_491.f0 g_1750 g_343 g_340.f0 g_1784 g_1327.f1.f0 g_1820 g_1852 g_26.f2 g_78 g_21 g_109 g_26.f1.f1 g_79.f0 g_200 g_206 g_211 g_218 g_131 g_1229.f0 g_1926 g_610
 */
static int32_t  func_56(int32_t  p_57, uint32_t  p_58, uint8_t  p_59)
{ /* block id: 766 */
    int32_t l_1636 = 0L;
    int32_t l_1639 = 0x65C8784FL;
    int8_t **l_1641 = (void*)0;
    union U1 * const *l_1655 = &g_490;
    union U1 * const **l_1654 = &l_1655;
    union U1 * const ** const *l_1653 = &l_1654;
    int32_t l_1684[5];
    int64_t *** const l_1699[7][7] = {{(void*)0,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610,(void*)0,&g_610},{(void*)0,&g_610,&g_610,&g_610,&g_610,(void*)0,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,(void*)0,&g_610,&g_610,&g_610,&g_610},{(void*)0,&g_610,(void*)0,&g_610,&g_610,&g_610,&g_610}};
    union U2 *l_1702 = &g_1327;
    union U2 **l_1701 = &l_1702;
    uint8_t *l_1703[8][2][8] = {{{&g_94,&g_94,&g_94,&g_94,&g_190,(void*)0,&g_94,(void*)0},{&g_218,&g_94,&g_190,&g_190,&g_218,&g_190,&g_190,&g_94}},{{&g_94,&g_94,(void*)0,&g_190,&g_94,(void*)0,&g_218,&g_218},{(void*)0,&g_94,&g_94,&g_94,&g_94,(void*)0,&g_218,&g_218}},{{&g_94,&g_94,(void*)0,(void*)0,&g_190,&g_218,&g_190,(void*)0},{&g_190,&g_218,&g_190,(void*)0,(void*)0,&g_94,&g_94,&g_218}},{{&g_218,(void*)0,&g_94,&g_94,&g_94,&g_94,(void*)0,&g_218},{&g_218,(void*)0,&g_94,&g_190,(void*)0,&g_94,&g_94,&g_94}},{{&g_190,&g_190,&g_218,&g_190,&g_190,&g_94,&g_218,(void*)0},{&g_94,(void*)0,&g_190,&g_94,&g_94,&g_94,&g_94,&g_190}},{{(void*)0,(void*)0,&g_190,&g_218,&g_94,&g_94,&g_218,&g_94},{&g_94,&g_218,&g_218,(void*)0,&g_218,&g_218,&g_94,&g_94}},{{&g_218,&g_94,&g_94,&g_218,&g_190,(void*)0,(void*)0,&g_190},{&g_94,&g_94,&g_94,&g_94,&g_190,(void*)0,&g_94,(void*)0}},{{&g_218,&g_94,&g_190,&g_190,&g_218,&g_190,&g_190,&g_94},{&g_94,&g_94,(void*)0,&g_190,&g_94,(void*)0,&g_218,&g_218}}};
    const int32_t ***l_1713[2];
    uint64_t *l_1714 = &g_340.f3;
    int8_t l_1749 = 1L;
    int8_t l_1751[1];
    int32_t l_1778 = 0xC4DC7E57L;
    union U2 ****l_1786 = &g_777[2][2];
    union U2 *****l_1785 = &l_1786;
    int16_t l_1788 = 0xE0E0L;
    uint16_t l_1847 = 1UL;
    uint16_t *l_1851 = &g_331;
    uint16_t **l_1850 = &l_1851;
    uint16_t ***l_1849 = &l_1850;
    int64_t ** const * const *l_1915 = (void*)0;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1684[i] = 1L;
    for (i = 0; i < 2; i++)
        l_1713[i] = &g_580[1][0];
    for (i = 0; i < 1; i++)
        l_1751[i] = 0xB1L;
    for (g_300.f1 = 0; (g_300.f1 != 8); g_300.f1 = safe_add_func_uint8_t_u_u(g_300.f1, 4))
    { /* block id: 769 */
        int8_t **l_1602 = (void*)0;
        int8_t ***l_1601[3][7][4] = {{{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,(void*)0,&l_1602},{(void*)0,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602}},{{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,(void*)0},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,(void*)0,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602}},{{(void*)0,&l_1602,(void*)0,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,&l_1602,&l_1602},{&l_1602,&l_1602,(void*)0,&l_1602},{(void*)0,(void*)0,&l_1602,&l_1602}}};
        int8_t ****l_1603 = &l_1601[1][0][2];
        uint64_t l_1635 = 2UL;
        int16_t *l_1637 = (void*)0;
        int16_t *l_1638[8][4][8] = {{{&g_1146,&g_43,&g_1146,&g_1146,&g_43,&g_1146,&g_43,&g_43},{&g_1146,(void*)0,&g_43,(void*)0,&g_1146,&g_1146,&g_1146,(void*)0},{&g_1146,&g_43,(void*)0,&g_43,(void*)0,&g_43,&g_43,(void*)0},{(void*)0,&g_43,(void*)0,&g_1146,&g_43,&g_43,&g_1146,&g_1146}},{{(void*)0,&g_1146,&g_43,&g_43,&g_43,&g_1146,(void*)0,&g_1146},{&g_1146,(void*)0,&g_1146,(void*)0,(void*)0,(void*)0,&g_43,(void*)0},{&g_43,(void*)0,&g_43,&g_43,(void*)0,(void*)0,&g_1146,&g_43},{&g_1146,&g_1146,&g_1146,(void*)0,&g_43,(void*)0,&g_1146,&g_1146}},{{(void*)0,&g_43,(void*)0,&g_1146,&g_43,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_43,&g_43,(void*)0,(void*)0,&g_1146,&g_43},{&g_1146,&g_43,(void*)0,&g_43,&g_1146,&g_1146,&g_1146,(void*)0},{&g_1146,&g_1146,&g_43,&g_43,&g_43,&g_43,&g_1146,&g_43}},{{&g_1146,&g_43,&g_43,&g_43,(void*)0,&g_43,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1146,&g_1146,(void*)0,&g_1146,(void*)0,&g_1146},{&g_1146,(void*)0,&g_43,(void*)0,&g_1146,&g_1146,&g_1146,&g_43},{&g_1146,&g_43,(void*)0,&g_43,(void*)0,&g_43,&g_43,(void*)0}},{{&g_43,(void*)0,(void*)0,(void*)0,&g_1146,&g_43,&g_1146,&g_1146},{(void*)0,&g_1146,&g_43,&g_43,&g_43,&g_1146,(void*)0,&g_1146},{&g_1146,(void*)0,&g_1146,&g_1146,(void*)0,(void*)0,(void*)0,(void*)0},{&g_43,&g_43,&g_43,&g_43,(void*)0,(void*)0,&g_1146,(void*)0}},{{&g_1146,&g_43,&g_43,(void*)0,&g_43,(void*)0,&g_1146,&g_43},{&g_43,&g_1146,(void*)0,&g_43,&g_43,&g_1146,(void*)0,(void*)0},{(void*)0,(void*)0,&g_43,&g_43,(void*)0,(void*)0,(void*)0,&g_1146},{&g_1146,&g_43,(void*)0,&g_1146,&g_43,(void*)0,&g_43,&g_1146}},{{&g_43,&g_1146,&g_43,&g_1146,(void*)0,&g_43,&g_43,&g_1146},{(void*)0,(void*)0,(void*)0,&g_43,&g_43,(void*)0,(void*)0,(void*)0},{(void*)0,&g_1146,&g_43,&g_43,(void*)0,&g_43,&g_43,&g_1146},{&g_43,&g_1146,&g_1146,&g_1146,&g_43,&g_1146,(void*)0,(void*)0}},{{&g_1146,&g_1146,&g_1146,(void*)0,(void*)0,(void*)0,(void*)0,&g_1146},{(void*)0,(void*)0,&g_1146,(void*)0,&g_43,(void*)0,&g_1146,&g_1146},{(void*)0,&g_1146,&g_1146,(void*)0,&g_1146,(void*)0,(void*)0,&g_1146},{&g_1146,&g_43,&g_43,(void*)0,&g_1146,(void*)0,(void*)0,&g_1146}}};
        const int32_t l_1640 = 0x547FEAF9L;
        union U2 *l_1673 = &g_1674;
        uint8_t *l_1700 = &g_190;
        int i, j, k;
        (*g_132) = ((((((4294967289UL < (((*l_1603) = l_1601[2][2][2]) == g_1604[0][5][1])) , (((safe_lshift_func_uint64_t_u_u((safe_rshift_func_uint64_t_u_u((safe_div_func_uint32_t_u_u(((safe_mul_func_int64_t_s_s(p_57, 0x2D48A7A480F763B0LL)) ^ (safe_add_func_uint32_t_u_u(((safe_lshift_func_uint32_t_u_s((((safe_div_func_int64_t_s_s((safe_sub_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((((safe_rshift_func_int16_t_s_s(((safe_div_func_int16_t_s_s((l_1639 |= ((safe_div_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(((safe_sub_func_uint16_t_u_u((g_383 , l_1635), g_1156.f0)) < ((g_1156.f0 >= l_1636) , l_1636)), (*g_1145))) || p_57), l_1636)) || l_1636)), p_59)) , p_58), 11)) & p_58) <= l_1636), p_58)), 0x1DF60CA0L)), 1UL)) , (void*)0) == (void*)0), p_57)) & p_59), p_57))), l_1640)), 51)), p_59)) , 0xF6L) && p_59)) , l_1639) , l_1641) != l_1602) , &l_1639);
        for (g_832.f0 = (-21); (g_832.f0 != 9); g_832.f0 = safe_add_func_uint8_t_u_u(g_832.f0, 6))
        { /* block id: 775 */
            int8_t **l_1656 = (void*)0;
            (**g_132) ^= l_1635;
            for (g_493.f0 = 0; (g_493.f0 >= 25); g_493.f0++)
            { /* block id: 779 */
                int16_t l_1667[8][4] = {{0xBB71L,0xD3CDL,0x1976L,0x1976L},{0x8F93L,0x8F93L,0x1976L,0x84C2L},{0xBB71L,(-9L),0xABD9L,0xD3CDL},{0xE558L,0xABD9L,0x84C2L,0xABD9L},{0x84C2L,0xABD9L,0xE558L,0xD3CDL},{0xABD9L,(-9L),0xBB71L,0x84C2L},{0x1976L,0x8F93L,0x8F93L,0x1976L},{0x1976L,0xD3CDL,0xBB71L,(-5L)}};
                int i, j;
                for (g_830.f3 = 17; (g_830.f3 != 10); g_830.f3 = safe_sub_func_uint64_t_u_u(g_830.f3, 1))
                { /* block id: 782 */
                    union U1 **l_1652[2][3][8] = {{{&g_490,&g_490,&g_490,&g_490,&g_490,&g_490,&g_490,&g_490},{(void*)0,&g_490,&g_490,(void*)0,&g_490,&g_490,(void*)0,&g_490},{&g_490,&g_490,&g_490,&g_490,(void*)0,&g_490,(void*)0,&g_490}},{{&g_490,&g_490,&g_490,&g_490,&g_490,&g_490,&g_490,&g_490},{&g_490,&g_490,&g_490,&g_490,&g_490,&g_490,&g_490,&g_490},{&g_490,(void*)0,&g_490,(void*)0,&g_490,&g_490,&g_490,&g_490}}};
                    union U1 *** const l_1651 = &l_1652[0][1][0];
                    union U1 *** const *l_1650 = &l_1651;
                    uint8_t *l_1657[4][6] = {{&g_94,&g_218,&g_94,&g_94,&g_190,&g_94},{&g_94,&g_218,&g_94,&g_94,&g_190,&g_94},{&g_94,&g_218,&g_94,&g_94,&g_190,&g_94},{&g_94,&g_218,&g_94,&g_94,&g_190,&g_94}};
                    const int32_t l_1672[4][1] = {{0xC174548DL},{0xC174548DL},{0xC174548DL},{0xC174548DL}};
                    int64_t *l_1683[4][3] = {{&g_200,&g_632[6],&g_200},{&g_632[6],&g_632[6],&g_632[6]},{&g_200,&g_632[6],&g_200},{&g_632[6],&g_632[6],&g_632[6]}};
                    int i, j, k;
                    if (((safe_unary_minus_func_int16_t_s((g_1649 | (l_1650 == l_1653)))) <= ((l_1641 == l_1656) , ((--g_94) <= (l_1639 ^= (safe_mul_func_uint16_t_u_u(((safe_unary_minus_func_uint32_t_u(((safe_sub_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(g_696.f3, l_1667[0][2])), 0xBDL)) && ((((safe_div_func_int32_t_s_s((safe_sub_func_int16_t_s_s(0x7D6FL, 0x23EDL)), 0x7591068AL)) > l_1672[0][0]) , 8L) , p_59)))) == l_1635), p_59)))))))
                    { /* block id: 785 */
                        (**g_335) = (*g_336);
                    }
                    else
                    { /* block id: 787 */
                        l_1673 = (void*)0;
                    }
                    (*g_1172) ^= ((p_57 || (l_1684[0] ^= ((!p_58) , ((((g_1577[8].f5 == p_57) || 1L) >= (*g_1141)) != (g_832.f0 | (+(safe_add_func_int16_t_s_s((safe_div_func_int8_t_s_s(((((l_1639 = (safe_mod_func_uint16_t_u_u((1L >= (g_1550 |= 0L)), p_57))) ^ g_126) > p_59) , 0x97L), g_21)), p_59)))))))) ^ (-1L));
                }
            }
            (*g_83) = (safe_rshift_func_uint32_t_u_s((**g_384), 24));
        }
        l_1684[0] = p_57;
        (*g_1172) |= ((safe_add_func_int64_t_s_s((((*g_1145) ^ (safe_mod_func_int64_t_s_s((((safe_lshift_func_int8_t_s_s((0xC3C0ADB8BA2C7B08LL <= (((safe_lshift_func_uint8_t_u_u(((0x18L < (4294967295UL < (g_1555 , (((((l_1684[1] = ((*l_1700) = (l_1684[0] == (!(((!((((****g_1218) = (safe_add_func_int64_t_s_s((l_1699[5][2] == (void*)0), 1UL))) || 0xF55C5315L) , l_1640)) || 1UL) & p_57))))) <= 7L) <= l_1636) ^ 0x1DA0L) != l_1635)))) && l_1635), 7)) && l_1640) ^ (***g_131))), 1)) == g_200) <= p_57), g_1156.f1))) != (-10L)), l_1635)) > 0x7565B15B8EEAD511LL);
    }
    (*g_1172) ^= (g_300.f1 > ((l_1684[0] ^= ((void*)0 != l_1701)) >= (l_1639 = (((*l_1714) ^= (!((((**g_1144) = ((safe_mod_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((safe_rshift_func_int16_t_s_s(((void*)0 == &l_1654), 0)), ((l_1639 , (safe_lshift_func_int8_t_s_u(0xCEL, (0x2759B8AEB3A73CF8LL || ((void*)0 == l_1713[1]))))) || p_57))) && p_59), p_59)) & 65529UL)) , l_1703[7][0][4]) == l_1703[3][1][7]))) > p_57))));
lbl_1841:
    (****g_333) = (**g_335);
    for (g_491.f0 = 0; (g_491.f0 <= 7); g_491.f0 += 1)
    { /* block id: 812 */
        union U2 *l_1747 = &g_1748;
        union U2 **l_1746 = &l_1747;
        int32_t l_1777[8][7] = {{0L,0L,0L,0L,0L,0L,0L},{1L,1L,1L,1L,1L,1L,1L},{0L,0L,0L,0L,0L,0L,0L},{1L,1L,1L,1L,1L,1L,1L},{0L,0L,0L,0L,0L,0L,0L},{1L,1L,1L,1L,1L,1L,1L},{0L,0L,0L,0L,0L,0L,0L},{1L,1L,1L,1L,1L,1L,1L}};
        int16_t l_1779 = 0x96C8L;
        int32_t l_1780[8][10][3] = {{{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL}},{{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL}},{{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL}},{{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0xED3A7FAAL},{0xED3A7FAAL,0xE329B266L,0x247211B5L}},{{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L}},{{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L}},{{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L}},{{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L},{0x247211B5L,0xED3A7FAAL,0x247211B5L}}};
        struct S0 * const l_1781 = &g_340;
        union U1 *l_1848 = (void*)0;
        uint16_t l_1888 = 0xCDE6L;
        uint32_t l_1889 = 0xA1D8A0D4L;
        int32_t *l_1890 = &g_995.f0;
        int64_t **l_1944 = &g_611;
        int i, j, k;
        if ((((safe_div_func_int32_t_s_s((g_594[g_491.f0] != (g_1750 = ((safe_mod_func_uint8_t_u_u((safe_lshift_func_int64_t_s_u(((safe_mul_func_int16_t_s_s((((***g_1219) = (+(safe_mul_func_uint8_t_u_u((g_1726 , (safe_sub_func_int32_t_s_s(((safe_mul_func_int16_t_s_s((((***g_1180) < (safe_add_func_int64_t_s_s(((safe_add_func_int64_t_s_s(((safe_rshift_func_int64_t_s_s(((+(((*g_1145) = (p_59 | (((safe_unary_minus_func_int32_t_s(g_594[g_491.f0])) & ((safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((safe_div_func_int8_t_s_s(((!((*l_1714) ^= ((*l_1701) == ((*l_1746) = (*l_1701))))) >= p_58), 1UL)), g_594[g_491.f0])), p_58)) || 253UL)) , g_383.f1))) & g_594[3])) > p_59), 38)) , p_58), 0L)) <= p_58), g_594[g_491.f0]))) , 0x9A57L), g_594[g_491.f0])) > g_204.f3), 0x3D59F39DL))), (-1L))))) ^ l_1749), g_594[4])) != 0L), g_594[g_491.f0])), g_594[g_491.f0])) >= g_594[g_491.f0]))), p_58)) <= p_57) , l_1751[0]))
        { /* block id: 818 */
            uint16_t *l_1759 = &g_343[4][1];
            int32_t l_1776[5] = {(-5L),(-5L),(-5L),(-5L),(-5L)};
            int i;
            p_57 = (safe_unary_minus_func_uint8_t_u((65535UL != (safe_sub_func_int16_t_s_s((((safe_rshift_func_uint16_t_u_u(65535UL, ((safe_div_func_uint16_t_u_u(((*l_1759) = 0x1B85L), g_594[g_491.f0])) ^ ((((****g_1218)--) <= ((p_58 || (l_1780[5][3][2] = (safe_rshift_func_uint8_t_u_u(((safe_mul_func_uint64_t_u_u(((*l_1714) &= (safe_mod_func_int64_t_s_s(((safe_lshift_func_int16_t_s_s(((**g_1144) = (((((l_1777[0][5] |= (p_59 = (((((**g_1140) = p_59) ^ ((((*g_1172) = (safe_lshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(255UL, 2L)), ((safe_mod_func_int16_t_s_s((-1L), l_1776[4])) ^ 247UL)))) > (*g_385)) && 0x9E3C9A78L)) != p_57) ^ 0x48L))) , p_58) >= 5L) < g_300.f0) == l_1778)), 7)) & 8UL), l_1779))), p_57)) & p_58), 2)))) >= p_58)) , g_585.f0)))) ^ l_1776[3]) || 1UL), p_57)))));
        }
        else
        { /* block id: 829 */
            uint8_t l_1796 = 0x21L;
            int64_t **l_1803 = (void*)0;
            int32_t l_1818 = 0xC0C5EA19L;
            int32_t l_1834 = 1L;
            uint8_t l_1836 = 0x41L;
            int32_t l_1858[8] = {(-1L),0x16849673L,0x16849673L,(-1L),0x16849673L,0x16849673L,(-1L),0x16849673L};
            int32_t l_1892 = 1L;
            uint32_t *l_1912 = &g_594[g_491.f0];
            int64_t ** const * const **l_1916 = (void*)0;
            int64_t ** const * const **l_1917 = &l_1915;
            int8_t *l_1918 = &l_1749;
            int i;
            for (g_340.f0 = 8; (g_340.f0 >= 0); g_340.f0 -= 1)
            { /* block id: 832 */
                uint8_t l_1782 = 0xE5L;
                union U2 *****l_1783 = (void*)0;
                int32_t l_1792[6][9] = {{0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L},{0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L},{0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L},{0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L},{0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L},{0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L,0x4277BE6CL,0x4277BE6CL,1L}};
                uint32_t ****l_1814 = &g_676[0][0];
                uint32_t ****l_1816 = (void*)0;
                uint32_t *****l_1815 = &l_1816;
                int64_t **l_1817 = &g_611;
                uint64_t *l_1840 = &g_21;
                uint8_t l_1893 = 0xD5L;
                int i, j;
                p_57 &= (((void*)0 != l_1781) || (l_1782 < ((g_1784 = l_1783) == l_1785)));
                if (((*g_1172) = (-2L)))
                { /* block id: 836 */
                    int64_t l_1793[1];
                    int32_t l_1794 = 0x501A75D3L;
                    int32_t l_1795 = (-5L);
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1793[i] = 0L;
                    for (g_340.f3 = 1; (g_340.f3 <= 8); g_340.f3 += 1)
                    { /* block id: 839 */
                        uint16_t l_1787[8][4] = {{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL}};
                        int32_t *l_1789 = &g_78;
                        int32_t *l_1790 = (void*)0;
                        int32_t *l_1791[5][10][1] = {{{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78},{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78}},{{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78},{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0}},{{&g_231[1]},{&g_78},{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78},{&g_231[1]},{&g_148.f0}},{{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78},{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78}},{{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0},{&g_231[1]},{&g_78},{&g_231[1]},{&g_148.f0},{&g_491.f0},{&g_148.f0}}};
                        int i, j, k;
                        l_1788 &= (p_59 , l_1787[3][1]);
                        ++l_1796;
                        if (p_57)
                            continue;
                        if (l_1794)
                            break;
                    }
                }
                else
                { /* block id: 845 */
                    for (g_1550 = 0; (g_1550 <= 1); g_1550 += 1)
                    { /* block id: 848 */
                        int i, j, k;
                        (**g_335) = (void*)0;
                        (***g_130) = &l_1777[(g_1550 + 6)][(g_1550 + 4)];
                        (*g_1172) = ((*g_83) = (0x00L || l_1777[g_491.f0][g_1550]));
                    }
                    for (g_1327.f1.f0 = 8; (g_1327.f1.f0 >= 0); g_1327.f1.f0 -= 1)
                    { /* block id: 856 */
                        return p_58;
                    }
                }
                (*g_1172) ^= (((p_58 != ((*l_1714) = l_1777[5][2])) < (safe_mod_func_uint32_t_u_u(((safe_add_func_int32_t_s_s(0x740D0077L, (((l_1803 != ((((safe_mul_func_uint64_t_u_u((((safe_sub_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s(((l_1792[3][0] ^= 7UL) > ((l_1684[0] = g_1048[0][0].f0) == ((safe_div_func_int64_t_s_s(((l_1814 == ((*l_1815) = l_1814)) < ((void*)0 != (*g_1219))), g_696.f2)) & (****g_1218)))), 4)) || (*g_1145)), 3)), 0xB8A69ED38F1E1EDBLL)) <= g_696.f3) & p_57), 0x66264467CA98BD0CLL)) , 0x9404L) & p_58) , l_1817)) ^ 3UL) == l_1780[2][3][0]))) < 0x9CDDL), p_57))) && p_59);
                for (g_94 = 0; (g_94 <= 8); g_94 += 1)
                { /* block id: 867 */
                    int32_t *l_1819[7] = {&g_1572.f1,&g_1572.f1,&g_1572.f1,&g_1572.f1,&g_1572.f1,&g_1572.f1,&g_1572.f1};
                    int i;
                    ++g_1820;
                    if (((*g_1172) = ((safe_rshift_func_uint8_t_u_s(g_632[g_340.f0], ((safe_sub_func_uint32_t_u_u((((void*)0 == &p_57) , ((safe_div_func_int16_t_s_s(((**g_1144) ^= ((**g_550) , p_58)), (safe_rshift_func_uint8_t_u_u((((g_1831 , (((safe_rshift_func_int64_t_s_u(0xE8DE3D42CF20298DLL, 33)) || g_1228.f0) && p_57)) < 1UL) || 0x18FBAAFBL), l_1796)))) > l_1796)), 0x0F457491L)) , p_59))) <= 0L)))
                    { /* block id: 871 */
                        int8_t l_1835 = 0x12L;
                        uint32_t *l_1839[8] = {&g_594[5],(void*)0,&g_594[5],&g_594[5],(void*)0,&g_594[5],&g_594[5],(void*)0};
                        uint16_t ****l_1855 = (void*)0;
                        uint16_t ****l_1856[9][4][7] = {{{&g_1852[0][4][3],(void*)0,&g_1852[0][1][4],&l_1849,&g_1852[0][4][3],&g_1852[0][4][3],&l_1849},{&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][1][1],&l_1849,&g_1852[0][4][3]},{&g_1852[0][1][1],(void*)0,&g_1852[1][1][2],&g_1852[0][4][1],&g_1852[0][4][3],&g_1852[0][3][2],&l_1849},{&l_1849,(void*)0,&g_1852[0][4][3],&l_1849,&g_1852[0][4][3],&l_1849,&g_1852[1][0][0]}},{{&g_1852[0][4][1],&g_1852[0][2][2],(void*)0,(void*)0,&g_1852[1][0][0],&g_1852[0][4][3],&g_1852[0][4][0]},{&g_1852[0][2][2],&g_1852[1][0][0],&g_1852[1][1][2],&g_1852[0][1][1],&g_1852[0][4][3],(void*)0,&l_1849},{&g_1852[0][4][3],&g_1852[0][4][3],&l_1849,&g_1852[0][1][1],&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[1][4][5],(void*)0,(void*)0,(void*)0,(void*)0,&g_1852[1][4][5],&g_1852[0][4][3]}},{{&g_1852[0][4][3],&g_1852[0][1][1],&g_1852[0][3][2],&l_1849,&l_1849,(void*)0,&g_1852[0][1][1]},{&g_1852[0][4][3],&g_1852[0][4][0],&g_1852[1][4][5],&g_1852[0][4][1],&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3]},{&g_1852[0][4][1],&g_1852[0][1][1],&g_1852[0][1][4],&g_1852[0][4][3],&l_1849,&g_1852[0][4][0],(void*)0},{&g_1852[0][4][3],(void*)0,(void*)0,&l_1849,&g_1852[0][1][1],(void*)0,&l_1849}},{{&g_1852[1][0][0],&g_1852[0][4][3],(void*)0,&l_1849,&g_1852[0][4][3],(void*)0,&l_1849},{&g_1852[0][4][3],&g_1852[1][0][0],(void*)0,&g_1852[0][0][4],&l_1849,&g_1852[1][2][2],(void*)0},{&g_1852[1][4][5],&g_1852[1][4][5],&l_1849,&g_1852[0][4][3],&g_1852[0][0][4],&g_1852[1][0][0],&g_1852[0][4][3]},{(void*)0,(void*)0,(void*)0,&l_1849,&g_1852[0][4][3],&l_1849,&l_1849}},{{&l_1849,&g_1852[0][4][3],&g_1852[1][2][2],&g_1852[1][1][5],&g_1852[0][0][4],&l_1849,(void*)0},{&g_1852[0][0][4],&g_1852[0][4][0],&l_1849,&l_1849,&g_1852[0][4][1],&l_1849,&g_1852[0][4][0]},{(void*)0,(void*)0,&l_1849,(void*)0,&g_1852[0][4][3],&g_1852[1][2][2],&g_1852[0][0][4]},{&l_1849,(void*)0,&l_1849,&g_1852[0][4][3],&l_1849,&g_1852[0][3][2],&g_1852[0][3][2]}},{{(void*)0,&g_1852[0][0][4],&l_1849,&g_1852[0][0][4],(void*)0,&g_1852[1][0][0],&g_1852[0][4][3]},{&g_1852[0][1][4],(void*)0,&g_1852[1][2][2],&g_1852[0][4][3],&g_1852[1][1][5],&g_1852[1][1][2],&g_1852[0][4][1]},{&l_1849,&g_1852[0][4][1],(void*)0,&g_1852[0][4][3],&g_1852[0][4][1],&g_1852[1][1][5],(void*)0},{&g_1852[0][1][4],&g_1852[0][4][3],&l_1849,&g_1852[0][4][3],&g_1852[0][4][3],&l_1849,&g_1852[0][0][4]}},{{(void*)0,&g_1852[0][3][2],(void*)0,&g_1852[0][4][3],&g_1852[1][1][5],&l_1849,(void*)0},{&l_1849,&g_1852[0][0][4],&g_1852[0][4][3],&g_1852[1][1][5],&g_1852[0][4][3],&l_1849,(void*)0},{(void*)0,&g_1852[0][4][0],&l_1849,&g_1852[0][4][3],&g_1852[0][4][3],&l_1849,&g_1852[0][4][0]},{&g_1852[0][0][4],(void*)0,&l_1849,&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[1][1][5],&g_1852[0][4][3]}},{{&l_1849,&l_1849,&l_1849,&g_1852[1][2][2],&g_1852[0][4][0],&g_1852[1][1][2],(void*)0},{(void*)0,&g_1852[0][4][3],&l_1849,&g_1852[0][4][3],&l_1849,&g_1852[1][0][0],&g_1852[1][4][5]},{&g_1852[1][4][5],(void*)0,&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][4][3],&g_1852[0][3][2],&g_1852[0][4][3]},{&l_1849,&g_1852[0][4][1],&g_1852[1][1][2],&g_1852[1][1][5],&g_1852[0][4][3],&g_1852[1][2][2],(void*)0}},{{&g_1852[0][4][3],&g_1852[0][4][1],&l_1849,&g_1852[0][4][3],&g_1852[0][4][3],&l_1849,&g_1852[0][4][3]},{&g_1852[0][4][3],(void*)0,(void*)0,&g_1852[0][4][3],&g_1852[0][4][3],&l_1849,&l_1849},{&l_1849,&g_1852[0][4][3],&g_1852[1][1][5],&g_1852[0][4][3],&g_1852[0][0][4],&l_1849,&g_1852[0][3][2]},{&g_1852[0][4][3],&l_1849,&l_1849,&g_1852[0][4][3],&g_1852[1][4][5],&g_1852[1][0][0],&l_1849}}};
                        int i, j, k;
                        l_1836++;
                        p_57 |= ((((**g_1140) = (0xE6L > g_300.f1)) , ((l_1792[2][3] = p_58) , 248UL)) < ((l_1840 = &g_109) != &g_21));
                        if (g_1550)
                            goto lbl_1841;
                        l_1792[2][1] = (g_1842 , (((safe_mod_func_int32_t_s_s((safe_sub_func_uint64_t_u_u((l_1847 >= (l_1803 != &g_611)), ((void*)0 != l_1848))), (l_1835 || (l_1849 == (g_1852[0][0][3] = g_1852[0][4][3]))))) < p_58) & l_1782));
                    }
                    else
                    { /* block id: 880 */
                        uint16_t l_1865 = 9UL;
                        p_57 &= (+l_1858[2]);
                        if ((**g_1171))
                            break;
                        (*g_1172) = (((safe_sub_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u((--(****g_1218)), p_59)), p_57)) & 0x3355C4C9L) | l_1792[2][2]);
                        --l_1865;
                    }
                    if ((l_1858[2] == (p_57 & (safe_sub_func_int8_t_s_s((safe_sub_func_int8_t_s_s(l_1858[2], (g_1872 , (safe_mod_func_uint16_t_u_u((((safe_unary_minus_func_int32_t_s((safe_add_func_uint32_t_u_u(((safe_mod_func_int8_t_s_s((safe_div_func_uint8_t_u_u(l_1777[0][4], (safe_rshift_func_int32_t_s_s(0x59D6C7CFL, 0)))), p_58)) > (((**g_1144) = (safe_rshift_func_int16_t_s_u((((**g_1140) = ((safe_mod_func_uint32_t_u_u(p_57, ((6L <= l_1888) , l_1889))) && 0UL)) | 8L), 9))) ^ l_1818)), 4294967295UL)))) > l_1777[0][5]) <= l_1792[0][8]), (-6L)))))), p_59)))))
                    { /* block id: 889 */
                        int32_t *l_1891[1][6][5] = {{{&g_696.f1,&g_696.f1,&l_1818,&g_832.f0,&g_1229.f0},{&l_1777[3][3],&g_1572.f1,&g_1572.f1,&l_1777[3][3],(void*)0},{&l_1780[6][6][2],&g_832.f0,&g_340.f1,&g_340.f1,&g_832.f0},{(void*)0,&g_1572.f1,(void*)0,(void*)0,(void*)0},{&l_1834,&g_696.f1,&l_1834,&g_340.f1,&l_1818},{&l_1792[4][5],&l_1777[3][3],(void*)0,&l_1777[3][3],&l_1792[4][5]}}};
                        int i, j, k;
                        l_1891[0][0][1] = l_1890;
                        l_1893++;
                        if ((*l_1890))
                            break;
                    }
                    else
                    { /* block id: 893 */
                        uint32_t l_1896 = 0xC27E98CFL;
                        int32_t *l_1897 = &g_696.f1;
                        p_57 = 1L;
                        p_57 ^= 0x712FB18FL;
                        if (l_1896)
                            break;
                        l_1897 = &p_57;
                    }
                }
            }
            l_1892 &= ((p_58 ^ (safe_add_func_uint16_t_u_u(((((p_59 != ((!(safe_lshift_func_int16_t_s_u((((l_1796 , (safe_mul_func_uint64_t_u_u(g_1572.f1, (!((((((*l_1714)++) || (((*g_1172) ^= (255UL <= ((((safe_sub_func_int16_t_s_s(((l_1912 == (void*)0) ^ ((*l_1890) & (safe_sub_func_int8_t_s_s(((*l_1918) ^= ((((*l_1917) = l_1915) == &g_1440) && p_59)), g_26.f2)))), p_59)) | (-4L)) , 0UL) < g_1919))) & p_57)) >= (*l_1890)) <= 0x1696D010C64D0E55LL) , p_57))))) > g_1179) < 0xDAC48E52L), 6))) >= g_93)) , p_58) <= 4UL) > g_1674.f0), p_58))) != p_58);
            (*g_132) = func_71((safe_div_func_uint64_t_u_u(p_59, g_696.f3)));
            if ((*l_1890))
                break;
        }
        for (g_1229.f0 = 0; (g_1229.f0 <= 8); g_1229.f0 += 1)
        { /* block id: 911 */
            if (p_59)
                break;
            return p_57;
        }
        for (g_1820 = 0; (g_1820 <= 8); g_1820 += 1)
        { /* block id: 917 */
            int32_t *l_1922 = &l_1780[1][6][0];
            int32_t *l_1923 = &g_1748.f1.f1;
            int32_t *l_1924 = &l_1780[5][3][2];
            int32_t *l_1925[9][4] = {{&g_300.f1,&g_1572.f1,(void*)0,&g_300.f1},{(void*)0,(void*)0,&l_1684[0],&g_79.f0},{&g_11,(void*)0,&g_995.f0,(void*)0},{(void*)0,&g_1156.f1,(void*)0,&g_1572.f1},{&g_491.f0,&g_11,&l_1684[0],(void*)0},{&g_986.f0,&g_340.f1,&l_1684[0],(void*)0},{&g_986.f0,&l_1684[0],&l_1684[0],&g_986.f0},{&g_491.f0,(void*)0,(void*)0,&l_1684[0]},{(void*)0,&g_340.f1,&g_995.f0,&g_1572.f1}};
            int64_t **l_1946 = &g_611;
            union U2 * const *l_1947 = &l_1702;
            uint32_t l_1951[7];
            int i, j;
            for (i = 0; i < 7; i++)
                l_1951[i] = 7UL;
            g_1926++;
            if (((((safe_div_func_uint64_t_u_u((g_632[g_491.f0] != 0UL), (safe_div_func_uint64_t_u_u((((((~((void*)0 == &l_1747)) | (~(safe_rshift_func_uint64_t_u_s((g_1939 , (((safe_rshift_func_uint32_t_u_u((((safe_lshift_func_uint32_t_u_u((***g_1219), 9)) == (((g_610 = l_1944) != (g_1945 , l_1946)) ^ g_200)) , p_59), p_58)) <= p_59) && (-10L))), 29)))) > (****g_1218)) > 0x2D1366FAE1F64DD3LL) > (*g_581)), p_57)))) || 1UL) , 0x83101A6BL) <= p_59))
            { /* block id: 920 */
                (*g_1172) = (((void*)0 != l_1947) || (g_1948[3] , (+0x1069L)));
            }
            else
            { /* block id: 922 */
                int32_t l_1950 = 0xE47BF3B4L;
                return l_1950;
            }
            l_1951[5]--;
        }
        if (p_57)
            break;
    }
    return p_59;
}


/* ------------------------------------------ */
/* 
 * reads : g_130 g_131 g_132 g_83 g_94 g_204.f1 g_265.f2 g_581 g_224.f0 g_953 g_21 g_29 g_26.f1.f3 g_585.f0 g_300.f1 g_979 g_204 g_986 g_491.f0 g_109 g_610 g_611 g_442 g_231 g_300.f0 g_1023 g_26 g_190 g_723.f3 g_681.f2 g_1172 g_383.f1 g_331 g_1180 g_1177 g_1178 g_1179 g_1218 g_1228 g_1229 g_1156.f2 g_1048.f0 g_550 g_551 g_93 g_1141 g_1144 g_1145 g_1146 g_340.f0 g_1122 g_1123 g_1219 g_1161.f0 g_594 g_43 g_1064.f1 g_696.f0 g_1327 g_11 g_696.f1 g_145 g_723.f0 g_300.f3 g_200 g_1171 g_79 g_78 g_79.f0 g_148 g_126 g_148.f0 g_42 g_205 g_211 g_218 g_482.f0 g_681.f1.f0 g_632 g_265.f0 g_1594 g_343 g_832.f0
 * writes: g_83 g_94 g_204.f1 g_265.f2 g_26.f1.f3 g_585.f0 g_300.f1 g_265.f1.f1 g_493.f3 g_300.f3 g_791.f3 g_300.f0 g_1023 g_681.f2 g_442 g_611 g_383.f1 g_610 g_830.f3 g_1146 g_43 g_190 g_331 g_93 g_26.f2 g_78 g_21 g_109 g_26.f1.f1 g_79.f0 g_200 g_206 g_211 g_218 g_131 g_340.f0 g_681.f1.f0 g_1161.f0 g_265.f1.f3 g_1327.f1.f0 g_1440 g_343 g_148.f0 g_832.f0
 */
static int32_t  func_60(const uint32_t  p_61, uint64_t  p_62, const int8_t  p_63, uint16_t  p_64)
{ /* block id: 335 */
    int32_t l_702 = 8L;
    int32_t l_711 = (-10L);
    int32_t l_712[10] = {0x8ACACA9CL,(-1L),(-1L),0x8ACACA9CL,(-1L),(-1L),0x8ACACA9CL,(-1L),(-1L),0x8ACACA9CL};
    int8_t l_742 = 0xF8L;
    union U2 **l_775 = (void*)0;
    union U2 ***l_774 = &l_775;
    int32_t l_785 = 0L;
    uint32_t l_786 = 18446744073709551609UL;
    uint32_t **l_863[9];
    uint32_t ***l_862 = &l_863[5];
    uint32_t *l_928 = &l_786;
    uint32_t **l_927 = &l_928;
    int32_t l_941 = (-1L);
    uint32_t ***l_1012 = &l_863[6];
    uint8_t * const l_1016 = &g_190;
    int64_t l_1095 = (-1L);
    int32_t *l_1148[2];
    struct S0 **l_1153[4];
    struct S0 *** const l_1152 = &l_1153[2];
    int8_t l_1188 = 0x8DL;
    int8_t l_1211[1][7] = {{0x0CL,0x0CL,0x0CL,0x0CL,0x0CL,0x0CL,0x0CL}};
    const int32_t l_1249[4][6][7] = {{{7L,1L,0x42548362L,0x5FB15688L,2L,0L,5L},{4L,0xAE180807L,0xE9A41511L,1L,0xE9A41511L,0xAE180807L,4L},{0xE213A86FL,0L,0x36F5C925L,5L,7L,(-7L),1L},{0x4AC72B17L,0x7E718A82L,0x12E38CC9L,0xE9A41511L,0xDAB77B56L,0xDAB77B56L,0xE9A41511L},{0x36F5C925L,0x10986407L,0x36F5C925L,0x9FFDAC59L,0L,0x0EDD8A45L,(-7L)},{0xAE180807L,0L,0xE9A41511L,0x7E718A82L,(-2L),0x6D6C9837L,0x4AC72B17L}},{{0x0EDD8A45L,0xE213A86FL,0x42548362L,0x42548362L,0xE213A86FL,0x0EDD8A45L,0x1ADAB5C7L},{0xE3370DF0L,0xE9A41511L,(-2L),(-6L),0x12E38CC9L,0xDAB77B56L,1L},{1L,0x0EDD8A45L,9L,(-7L),5L,(-7L),9L},{0xE9A41511L,0xE9A41511L,0x6D6C9837L,7L,(-6L),0xAE180807L,0x2526D13BL},{0x1ADAB5C7L,0xE213A86FL,0x9FFDAC59L,7L,9L,0L,0L},{(-6L),0L,4L,0L,(-6L),0xE9A41511L,0x02FD594AL}},{{2L,0x10986407L,1L,0L,5L,0x5FB15688L,0x0EDD8A45L},{1L,0x7E718A82L,7L,0x12E38CC9L,0x12E38CC9L,7L,0x7E718A82L},{2L,0L,0x10986407L,0x1ADAB5C7L,0xE213A86FL,0x9FFDAC59L,7L},{(-6L),0xAE180807L,0x2526D13BL,4L,(-2L),0x12E38CC9L,(-2L)},{0x1ADAB5C7L,1L,1L,0x1ADAB5C7L,0L,7L,1L},{0xE9A41511L,(-2L),(-6L),0x12E38CC9L,0xDAB77B56L,1L,0L}},{{1L,0x36F5C925L,7L,0L,7L,0x36F5C925L,1L},{0xE3370DF0L,1L,0xAE180807L,0L,0xE9A41511L,0x7E718A82L,(-2L)},{0x0EDD8A45L,(-7L),0x42548362L,1L,0x9FFDAC59L,0x9FFDAC59L,1L},{1L,0x7E718A82L,1L,0xE3370DF0L,0x6D6C9837L,0L,0x4AC72B17L},{0L,7L,1L,0x0EDD8A45L,9L,(-7L),5L},{0L,(-2L),0xAE180807L,0xAE180807L,(-2L),0L,0xDAB77B56L}}};
    int8_t l_1370 = 0x48L;
    int32_t ** const *l_1396 = &g_132;
    int16_t l_1419 = 0x57E6L;
    union U1 *l_1451 = &g_491;
    const int64_t l_1473 = 0x6F2C5D7D83727A8ELL;
    int8_t *l_1478[6];
    int8_t **l_1477 = &l_1478[0];
    int8_t ***l_1476[7] = {&l_1477,&l_1477,&l_1477,&l_1477,&l_1477,&l_1477,&l_1477};
    int64_t ***l_1506[6][1][7] = {{{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610}},{{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610}},{{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610}},{{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610}},{{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610}},{{&g_610,&g_610,&g_610,&g_610,&g_610,&g_610,&g_610}}};
    int64_t ****l_1505 = &l_1506[5][0][0];
    uint32_t l_1538 = 0x8376912EL;
    int32_t l_1580[3][10] = {{(-1L),0xEB94CF37L,0xEB94CF37L,(-1L),0xEB94CF37L,0xEB94CF37L,(-1L),0xEB94CF37L,0xEB94CF37L,(-1L)},{0xEB94CF37L,(-1L),0xEB94CF37L,0xEB94CF37L,(-1L),0xEB94CF37L,0xEB94CF37L,(-1L),0xEB94CF37L,0xEB94CF37L},{(-1L),(-1L),3L,(-1L),(-1L),3L,(-1L),(-1L),3L,(-1L)}};
    uint8_t l_1585 = 1UL;
    uint16_t *l_1597 = &g_343[1][1];
    int64_t l_1598 = 0x309356585AAD4308LL;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_863[i] = &g_551;
    for (i = 0; i < 2; i++)
        l_1148[i] = (void*)0;
    for (i = 0; i < 4; i++)
        l_1153[i] = &g_206;
    for (i = 0; i < 6; i++)
        l_1478[i] = &g_696.f0;
    (*g_132) = (***g_130);
    if (((*g_83) |= ((++g_94) & ((safe_lshift_func_uint16_t_u_s(0x0DE4L, 13)) <= l_702))))
    { /* block id: 339 */
        int16_t l_703 = 5L;
        int32_t l_704 = 0x27D9FA71L;
        int32_t *l_705 = &l_702;
        int32_t *l_706 = &g_231[1];
        int32_t *l_707 = (void*)0;
        int32_t l_708 = 0xD2C78D77L;
        int32_t *l_709 = &g_231[1];
        int32_t *l_710[8];
        uint16_t l_713[5];
        int32_t l_736 = 0x6BA3565DL;
        int64_t *l_826 = (void*)0;
        union U1 *l_831[1];
        uint32_t l_853 = 0UL;
        int32_t ***l_930 = &g_132;
        int i;
        for (i = 0; i < 8; i++)
            l_710[i] = &g_491.f0;
        for (i = 0; i < 5; i++)
            l_713[i] = 5UL;
        for (i = 0; i < 1; i++)
            l_831[i] = &g_832;
        ++l_713[2];
        for (g_265.f2 = 0; (g_265.f2 == 6); g_265.f2++)
        { /* block id: 343 */
            uint64_t *l_718 = &g_300.f3;
            uint16_t *l_721 = &g_343[1][1];
            uint16_t *l_722 = &l_713[0];
            int32_t l_737 = 0xE008601FL;
            int32_t l_741 = 0L;
            int32_t l_744 = 0x4EB20A22L;
            int32_t l_746 = 1L;
            int32_t l_747 = 7L;
            int32_t l_748 = (-4L);
            int32_t l_750 = (-7L);
            uint32_t l_751 = 0xEB87C087L;
            union U1 *l_829 = &g_830;
            uint32_t ****l_847[8] = {&g_676[0][0],&g_676[1][3],&g_676[1][3],&g_676[0][0],&g_676[1][3],&g_676[1][3],&g_676[0][0],&g_676[1][3]};
            int32_t l_887 = 1L;
            int8_t l_889 = 0xE7L;
            int32_t l_890 = (-6L);
            int32_t l_893 = 0xF8F6BE0CL;
            int32_t l_894 = 0xF56C8089L;
            uint8_t l_895 = 0UL;
            int i;
        }
    }
    else
    { /* block id: 417 */
        int64_t l_933 = (-8L);
        int32_t l_934 = 0x7DD4356BL;
        int32_t l_935 = 1L;
        int32_t l_936 = 0xE1200B8EL;
        int32_t l_937 = (-1L);
        int32_t l_938 = 0x50B113A8L;
        int32_t l_939 = 0xF9029C7CL;
        int32_t l_940[9];
        const int32_t l_954[5] = {0x4991FE81L,0x4991FE81L,0x4991FE81L,0x4991FE81L,0x4991FE81L};
        int32_t *l_955 = &l_712[1];
        int16_t *l_1017 = &g_43;
        uint32_t **l_1033 = &g_551;
        struct S0 ***l_1154 = (void*)0;
        union U1 *l_1160[1];
        const uint64_t l_1163 = 18446744073709551615UL;
        uint64_t l_1194[2][7][5] = {{{0x9A743EC21E2DFA36LL,0x9A743EC21E2DFA36LL,0xFDC0844A5A759A16LL,1UL,18446744073709551615UL},{7UL,18446744073709551615UL,18446744073709551615UL,7UL,0xC455E99869123008LL},{0x0C26177BC2675B35LL,1UL,18446744073709551611UL,18446744073709551611UL,1UL},{0xC455E99869123008LL,18446744073709551615UL,4UL,0x7DC1B7AD5FBD6E58LL,0x7DC1B7AD5FBD6E58LL},{0xF21560176F4C5AC0LL,0x9A743EC21E2DFA36LL,0xF21560176F4C5AC0LL,18446744073709551611UL,0xFDC0844A5A759A16LL},{0x906EC04CC7406411LL,7UL,0x7DC1B7AD5FBD6E58LL,7UL,0x906EC04CC7406411LL},{0xF21560176F4C5AC0LL,0x0C26177BC2675B35LL,0x9A743EC21E2DFA36LL,1UL,0x9A743EC21E2DFA36LL}},{{0xC455E99869123008LL,0xC455E99869123008LL,0x7DC1B7AD5FBD6E58LL,0x906EC04CC7406411LL,0x7958AC76FE595DDDLL},{0x0C26177BC2675B35LL,0xF21560176F4C5AC0LL,0xF21560176F4C5AC0LL,0x0C26177BC2675B35LL,0x9A743EC21E2DFA36LL},{7UL,0x906EC04CC7406411LL,4UL,4UL,0x906EC04CC7406411LL},{0x9A743EC21E2DFA36LL,0xF21560176F4C5AC0LL,18446744073709551611UL,0xFDC0844A5A759A16LL,0xFDC0844A5A759A16LL},{18446744073709551615UL,0xC455E99869123008LL,18446744073709551615UL,4UL,0x7DC1B7AD5FBD6E58LL},{1UL,0x0C26177BC2675B35LL,0xFDC0844A5A759A16LL,0x0C26177BC2675B35LL,1UL},{18446744073709551615UL,7UL,0xC455E99869123008LL,0x906EC04CC7406411LL,0xC455E99869123008LL}}};
        uint16_t l_1199 = 0x73EDL;
        union U1 **l_1234 = &l_1160[0];
        union U1 ***l_1233 = &l_1234;
        const uint16_t *l_1238 = (void*)0;
        const uint16_t **l_1237 = &l_1238;
        int32_t *l_1359[2];
        uint16_t l_1368 = 65528UL;
        union U2 ****l_1390[2][1];
        uint32_t l_1400 = 4294967293UL;
        int32_t l_1405 = 0xEADB4912L;
        int8_t l_1424[4][7][9] = {{{1L,(-7L),0x1BL,1L,(-2L),(-1L),0x1BL,(-4L),(-10L)},{0x8EL,0xEBL,(-1L),(-4L),(-1L),0x25L,0L,0xFFL,0x89L},{0xE9L,4L,1L,1L,(-1L),0xE9L,4L,0x8EL,(-1L)},{0L,0xD5L,0x79L,0x25L,0x8EL,0x4DL,(-1L),0x1BL,0x23L},{0x79L,(-1L),0x1BL,0x1BL,0xD9L,0L,(-10L),1L,1L},{(-1L),(-1L),0xFFL,0xAEL,0xAAL,0L,(-1L),3L,0x05L},{(-10L),0xD5L,0xC7L,1L,(-1L),1L,0x56L,0x89L,0x13L}},{{4L,4L,0xB4L,0xFFL,0x44L,0L,0L,0x44L,0xFFL},{0x1AL,0xEBL,0x1AL,0xB4L,0x23L,0x59L,1L,0x63L,1L},{0x79L,0x99L,0x44L,(-1L),0xD5L,0x63L,0x05L,0x1BL,1L},{(-2L),1L,3L,0xB4L,(-7L),(-1L),0x44L,(-1L),0x05L},{(-1L),1L,0xD6L,0xFFL,8L,0x8EL,1L,0xD6L,0L},{0xCBL,(-7L),0x68L,1L,(-1L),0x77L,(-2L),0x44L,0x32L},{(-1L),0L,1L,0xAEL,(-1L),1L,0x69L,0x59L,0x1BL}},{{7L,1L,0x99L,0x1BL,1L,1L,0x05L,0x68L,0x59L},{(-7L),0xAEL,0L,0x25L,(-1L),0x77L,0xAEL,(-4L),(-10L)},{(-1L),1L,0xC7L,1L,1L,0x8EL,(-1L),(-1L),(-1L)},{(-9L),(-1L),1L,(-4L),1L,(-1L),(-9L),0x8EL,1L},{0x00L,0xEBL,(-1L),1L,(-1L),0x63L,0L,0L,(-1L)},{0x59L,(-1L),1L,0x79L,0x86L,0x59L,(-10L),0x8EL,(-1L)},{(-2L),0x05L,1L,0x25L,0xCBL,0L,(-1L),(-1L),0x23L}},{{1L,(-1L),(-1L),(-1L),(-1L),(-1L),0xCBL,0xA8L,0L},{0x23L,1L,(-1L),0L,1L,0xAEL,(-1L),1L,0x69L},{(-4L),0x70L,0x13L,0x00L,(-1L),(-1L),(-7L),(-1L),0x63L},{(-4L),0x47L,7L,(-1L),(-1L),3L,(-1L),0x63L,(-1L)},{0x23L,1L,(-1L),7L,0x68L,(-1L),0x59L,0xD9L,(-1L)},{0x1AL,0L,1L,(-1L),0x70L,0x79L,0x70L,(-1L),1L},{0x1BL,0x1BL,1L,0x86L,0x1AL,0x23L,0x25L,(-9L),0x69L}}};
        uint8_t l_1475[8][3] = {{0x86L,0x86L,252UL},{0x01L,255UL,252UL},{255UL,0x01L,252UL},{0x86L,0x86L,252UL},{0x01L,255UL,252UL},{255UL,0x01L,252UL},{0x86L,0x86L,252UL},{0x01L,255UL,252UL}};
        int16_t ***l_1528 = &g_1140;
        uint64_t l_1569 = 0x7458C441F790972BLL;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_940[i] = 0xA57D9068L;
        for (i = 0; i < 1; i++)
            l_1160[i] = &g_1161;
        for (i = 0; i < 2; i++)
            l_1359[i] = &g_265.f2;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
                l_1390[i][j] = (void*)0;
        }
        if ((*g_581))
        { /* block id: 418 */
            int32_t *l_932[4] = {&g_482.f1.f1,&g_482.f1.f1,&g_482.f1.f1,&g_482.f1.f1};
            uint64_t l_942[9][10] = {{0x69B7DA330A8DCE22LL,18446744073709551606UL,0x042436284219D82CLL,0x5E72ACA0EC2AE6C0LL,0xF8109094F1F66642LL,0xF8109094F1F66642LL,0x5E72ACA0EC2AE6C0LL,0x042436284219D82CLL,18446744073709551606UL,0x69B7DA330A8DCE22LL},{18446744073709551606UL,1UL,0xA1AD9F22B140391CLL,0x5E72ACA0EC2AE6C0LL,0x6D2FBA04C6E05933LL,0x69B7DA330A8DCE22LL,0x6D2FBA04C6E05933LL,0x5E72ACA0EC2AE6C0LL,0xA1AD9F22B140391CLL,1UL},{0xF8109E38D9E5CF7ALL,0x042436284219D82CLL,0x69B7DA330A8DCE22LL,1UL,0x6D2FBA04C6E05933LL,0xA0EBBEB318ED5D5CLL,0xA0EBBEB318ED5D5CLL,0x6D2FBA04C6E05933LL,1UL,0x69B7DA330A8DCE22LL},{0x6D2FBA04C6E05933LL,0x6D2FBA04C6E05933LL,18446744073709551606UL,0xF8109E38D9E5CF7ALL,0xF8109094F1F66642LL,0xA0EBBEB318ED5D5CLL,0xA1AD9F22B140391CLL,0xA0EBBEB318ED5D5CLL,0xF8109094F1F66642LL,0xF8109E38D9E5CF7ALL},{0xF8109E38D9E5CF7ALL,0x40413A51D8B025B3LL,0xF8109E38D9E5CF7ALL,0xA0EBBEB318ED5D5CLL,0x5E72ACA0EC2AE6C0LL,0x69B7DA330A8DCE22LL,0xA1AD9F22B140391CLL,0xA1AD9F22B140391CLL,0x69B7DA330A8DCE22LL,0x5E72ACA0EC2AE6C0LL},{18446744073709551606UL,0x6D2FBA04C6E05933LL,0x6D2FBA04C6E05933LL,18446744073709551606UL,0xF8109E38D9E5CF7ALL,0xF8109094F1F66642LL,0xA0EBBEB318ED5D5CLL,0xA1AD9F22B140391CLL,0xA0EBBEB318ED5D5CLL,0xF8109094F1F66642LL},{0x69B7DA330A8DCE22LL,0x042436284219D82CLL,0xF8109E38D9E5CF7ALL,0x042436284219D82CLL,0x69B7DA330A8DCE22LL,1UL,0x6D2FBA04C6E05933LL,0xA0EBBEB318ED5D5CLL,0xA0EBBEB318ED5D5CLL,0x6D2FBA04C6E05933LL},{0xA1AD9F22B140391CLL,1UL,18446744073709551606UL,18446744073709551606UL,1UL,0xA1AD9F22B140391CLL,0x5E72ACA0EC2AE6C0LL,0x6D2FBA04C6E05933LL,0x69B7DA330A8DCE22LL,0x6D2FBA04C6E05933LL},{0x042436284219D82CLL,18446744073709551606UL,0x69B7DA330A8DCE22LL,0xA0EBBEB318ED5D5CLL,0x69B7DA330A8DCE22LL,18446744073709551606UL,0x042436284219D82CLL,0x5E72ACA0EC2AE6C0LL,0xF8109094F1F66642LL,0xF8109094F1F66642LL}};
            int i, j;
            ++l_942[5][9];
            if ((safe_mod_func_uint64_t_u_u((((((0x270297F3L <= p_63) || p_62) ^ (((safe_mod_func_uint64_t_u_u(((l_940[2] || ((-2L) && (g_224.f0 , (safe_sub_func_int8_t_s_s((safe_div_func_uint32_t_u_u(4294967295UL, (g_953 , l_954[0]))), 6L))))) > l_941), 0xD99354117804A507LL)) ^ g_21) >= 0xF3L)) && p_61) >= (-4L)), l_785)))
            { /* block id: 420 */
                (***g_131) = (-1L);
                (**g_131) = l_955;
            }
            else
            { /* block id: 423 */
                (**g_132) = p_63;
            }
        }
        else
        { /* block id: 426 */
            uint64_t l_976[2][5][3] = {{{0x50613C189BE2F481LL,0xA7B510557668A25CLL,0xA7B510557668A25CLL},{0x0CA513528442B177LL,0x44911ACBE74D7175LL,0x32663C71A523AF39LL},{0x50613C189BE2F481LL,0xA7B510557668A25CLL,0xA7B510557668A25CLL},{0x0CA513528442B177LL,0x44911ACBE74D7175LL,0x32663C71A523AF39LL},{0x50613C189BE2F481LL,0xA7B510557668A25CLL,0xB36BF5E70073D244LL}},{{0x32663C71A523AF39LL,0UL,18446744073709551611UL},{0xA7B510557668A25CLL,0xB36BF5E70073D244LL,0xB36BF5E70073D244LL},{0x32663C71A523AF39LL,0UL,18446744073709551611UL},{0xA7B510557668A25CLL,0xB36BF5E70073D244LL,0xB36BF5E70073D244LL},{0x32663C71A523AF39LL,0UL,18446744073709551611UL}}};
            uint32_t l_991[3];
            union U1 *l_994 = &g_995;
            uint32_t l_1108 = 0x880E80FBL;
            uint32_t * const *l_1112[4][9] = {{&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551},{&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551},{&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551},{&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551,&g_551}};
            uint32_t * const **l_1111[7][10] = {{&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7]},{&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7]},{&l_1112[0][7],&l_1112[0][7],(void*)0,&l_1112[2][6],&l_1112[2][6],(void*)0,&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],(void*)0},{&l_1112[0][7],&l_1112[0][7],&l_1112[2][6],&l_1112[0][7],&l_1112[0][7],(void*)0,(void*)0,&l_1112[0][7],&l_1112[0][7],&l_1112[2][6]},{&l_1112[0][7],&l_1112[0][7],&l_1112[2][6],&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],&l_1112[2][6],&l_1112[0][7],&l_1112[0][7],&l_1112[2][6]},{&l_1112[0][7],&l_1112[0][7],(void*)0,(void*)0,&l_1112[0][7],&l_1112[0][7],&l_1112[2][6],&l_1112[0][7],&l_1112[0][7],(void*)0},{&l_1112[0][7],&l_1112[0][7],&l_1112[0][7],(void*)0,&l_1112[2][6],&l_1112[2][6],(void*)0,&l_1112[0][7],&l_1112[0][7],&l_1112[0][7]}};
            uint32_t * const ***l_1110[1][7] = {{&l_1111[3][5],&l_1111[3][5],&l_1111[3][5],&l_1111[3][5],&l_1111[3][5],&l_1111[3][5],&l_1111[3][5]}};
            int16_t l_1115 = (-10L);
            int16_t * const *l_1143 = &l_1017;
            uint32_t **l_1157 = &l_928;
            int32_t l_1173[8] = {0x244BBB05L,0x3431BBA9L,0x244BBB05L,0x3431BBA9L,0x244BBB05L,0x3431BBA9L,0x244BBB05L,0x3431BBA9L};
            uint64_t *l_1198[10][4] = {{(void*)0,&g_300.f3,(void*)0,&l_976[1][0][1]},{&g_109,&g_300.f3,&g_21,(void*)0},{&g_300.f3,&l_976[0][0][0],&l_976[0][0][0],&g_300.f3},{(void*)0,(void*)0,&l_976[0][0][0],&l_976[1][0][1]},{&g_300.f3,&g_109,&g_21,&g_109},{&g_109,&l_976[0][0][0],(void*)0,&g_109},{(void*)0,&g_109,&l_976[1][0][1],&l_976[1][0][1]},{(void*)0,(void*)0,&g_21,&g_300.f3},{(void*)0,&l_976[0][0][0],&l_976[1][0][1],(void*)0},{(void*)0,&g_300.f3,(void*)0,&l_976[1][0][1]}};
            struct S0 * const *l_1221 = (void*)0;
            uint16_t **l_1236 = (void*)0;
            int64_t **l_1247 = &g_611;
            uint8_t l_1288 = 0x97L;
            int32_t l_1352 = 0x8D55727FL;
            int32_t *l_1356 = &g_26.f2;
            struct S0 *l_1454 = &g_300;
            int32_t l_1456[3][3] = {{0xE72B7859L,0x860621D4L,0xE72B7859L},{1L,1L,1L},{0xE72B7859L,0x860621D4L,0xE72B7859L}};
            int8_t ** const *l_1479[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint8_t l_1480 = 251UL;
            uint64_t l_1483[2];
            int32_t l_1494 = 0L;
            int32_t l_1539[8][3][3] = {{{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)}},{{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL}},{{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)}},{{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL}},{{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)}},{{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL}},{{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)}},{{0x4C2CE1FCL,7L,0x4C2CE1FCL},{(-6L),(-2L),(-6L)},{0x4C2CE1FCL,7L,0x4C2CE1FCL}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_991[i] = 4294967289UL;
            for (i = 0; i < 2; i++)
                l_1483[i] = 0x8E06F1EE5000FDA8LL;
            if (((***g_131) = ((p_63 ^ 6L) == (safe_rshift_func_int16_t_s_s(g_29, 9)))))
            { /* block id: 428 */
                int32_t l_960 = 0x82E84E6CL;
                uint32_t ***l_1014[8] = {&l_863[5],&l_863[5],&l_863[5],&l_863[5],&l_863[5],&l_863[5],&l_863[5],&l_863[5]};
                uint32_t ***l_1015 = &l_863[5];
                int i;
                (**g_132) ^= (safe_lshift_func_uint32_t_u_s(8UL, (l_960 = p_63)));
                (*g_132) = &l_934;
                for (g_26.f1.f3 = 0; (g_26.f1.f3 <= 42); g_26.f1.f3++)
                { /* block id: 434 */
                    for (g_585.f0 = 0; (g_585.f0 < 25); ++g_585.f0)
                    { /* block id: 437 */
                        uint32_t l_965 = 0UL;
                        l_965++;
                    }
                }
                if (g_94)
                    goto lbl_1197;
                for (g_300.f1 = (-11); (g_300.f1 >= 21); ++g_300.f1)
                { /* block id: 443 */
                    int32_t l_977 = 1L;
                    union U2 ***l_999 = &g_778;
                    uint32_t ****l_1013[4][3][9] = {{{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0}},{{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0}},{{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0}},{{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0},{&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0,&l_1012,(void*)0,(void*)0}}};
                    int16_t *l_1018[5];
                    int32_t l_1019 = 0x796D4EC1L;
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_1018[i] = (void*)0;
                    for (g_265.f1.f1 = 0; (g_265.f1.f1 <= 8); g_265.f1.f1 += 1)
                    { /* block id: 446 */
                        int16_t *l_978 = &g_493.f3;
                        uint64_t *l_987 = &g_300.f3;
                        int16_t *l_988 = (void*)0;
                        int16_t *l_989 = &g_791.f3;
                        int32_t l_990[4];
                        int32_t l_992[8] = {0L,0L,0L,0L,0L,0L,0L,0L};
                        union U1 **l_993[4] = {&g_490,&g_490,&g_490,&g_490};
                        int i;
                        for (i = 0; i < 4; i++)
                            l_990[i] = 0x49DFB074L;
                        l_994 = (((((safe_mod_func_uint8_t_u_u((((*l_989) = (((safe_add_func_uint8_t_u_u((((((safe_rshift_func_uint64_t_u_s((*l_955), (l_976[0][1][0] = p_61))) , (l_977 != ((*l_978) = 6L))) == (g_979 , (((((safe_rshift_func_uint64_t_u_s(l_960, 8)) & ((safe_sub_func_int16_t_s_s((l_990[2] ^= (safe_rshift_func_int64_t_s_u(0x0A62F08CD306CB20LL, (g_204 , ((*l_987) = (g_986 , g_204.f5)))))), g_491.f0)) <= g_21)) | 3UL) , (*l_955)) != l_991[2]))) < (**g_132)) , p_63), g_109)) < (**g_610)) ^ (**g_132))) || l_990[3]), l_992[6])) >= g_231[1]) <= l_711) < p_62) , (void*)0);
                        if (l_991[2])
                            continue;
                    }
                }
            }
            else
            { /* block id: 468 */
                int32_t l_1028 = 0x6FAF5A1CL;
                for (g_300.f0 = 0; (g_300.f0 <= 3); g_300.f0++)
                { /* block id: 471 */
                    const uint32_t ***l_1025 = &g_1023;
                    int64_t **l_1030 = &g_611;
                    (***g_131) = (~((((*l_1025) = g_1023) != (g_26 , (void*)0)) >= ((((l_1028 = (safe_rshift_func_uint16_t_u_u((p_63 | g_190), 7))) , ((safe_unary_minus_func_int32_t_s(0x6CE8A421L)) , (void*)0)) == l_1030) & g_723.f3)));
                    return (**g_132);
                }
            }
lbl_1197:
            for (g_681.f2 = 0; (g_681.f2 <= 7); g_681.f2 += 1)
            { /* block id: 480 */
                const union U2 *l_1032 = &g_482;
                uint32_t **l_1034 = (void*)0;
                int32_t l_1041[1][4];
                uint32_t ****l_1109[1];
                int32_t l_1190 = (-1L);
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_1041[i][j] = 0L;
                }
                for (i = 0; i < 1; i++)
                    l_1109[i] = &l_1012;
            }
            if (((l_1199 = p_63) ^ (((*g_611) = ((safe_lshift_func_uint16_t_u_u((p_61 || ((safe_lshift_func_int16_t_s_u((~(0xF47AL || (safe_lshift_func_int32_t_s_u((*g_1172), p_64)))), 0)) && 0x2A7DL)), (((*l_955) , 0xB775L) != (safe_mul_func_int16_t_s_s(((safe_div_func_int8_t_s_s(g_331, p_64)) >= 0x313735A3CFFA999BLL), (*l_955)))))) || l_1211[0][3])) != p_61)))
            { /* block id: 562 */
                uint32_t ****l_1217 = &l_1012;
                uint32_t *****l_1216 = &l_1217;
                int32_t l_1220 = 0x99999477L;
                int32_t l_1222 = 0x2147DB84L;
                int32_t l_1287[8][2][3] = {{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}},{{(-5L),(-5L),(-5L)},{2L,2L,2L}}};
                int32_t l_1322 = (-1L);
                uint32_t l_1351 = 0x017B9E24L;
                uint32_t l_1369 = 0xAB4F7465L;
                int i, j, k;
                if (((p_63 & (l_1173[1] , (safe_rshift_func_uint8_t_u_u(((0xCA1D6A8BL ^ ((***g_1180) && 6UL)) != (0x1DDEEDF2L == (((*g_610) = l_1198[2][3]) != &g_200))), (((*l_1216) = &g_676[0][0]) == g_1218))))) && l_1220))
                { /* block id: 565 */
                    int16_t l_1223 = 0xABE3L;
                    int32_t l_1224 = 0xF189B693L;
                    (*g_1172) = ((void*)0 != l_1221);
                    if (p_63)
                    { /* block id: 567 */
                        uint64_t l_1225 = 0xFD3F55DA886E7BEELL;
                        l_1225++;
                        return l_1224;
                    }
                    else
                    { /* block id: 570 */
                        int16_t l_1230 = (-8L);
                        union U1 ***l_1235[10] = {&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234};
                        const uint16_t ***l_1239 = &l_1237;
                        int16_t ***l_1246 = &g_1140;
                        int64_t ***l_1248 = &g_610;
                        int i;
                        (*l_955) ^= (((g_1228 , g_1229) , l_1230) , (safe_mul_func_uint64_t_u_u(l_1222, (l_1233 != l_1235[7]))));
                        (**g_132) = ((*l_955) ^= ((l_1236 != ((*l_1239) = l_1237)) , ((g_1156.f2 == (safe_div_func_uint8_t_u_u((g_1048[0][0].f0 , (safe_mul_func_int32_t_s_s((safe_sub_func_uint16_t_u_u((l_1246 != &l_1143), ((((*l_1248) = l_1247) != (void*)0) | l_1249[0][2][6]))), l_1223))), 0x41L))) != p_63)));
                    }
                }
                else
                { /* block id: 577 */
                    uint32_t l_1283 = 0x5A943345L;
                    uint32_t **l_1284 = (void*)0;
                    int16_t *l_1285 = &l_1115;
                    int32_t l_1286[5] = {(-2L),(-2L),(-2L),(-2L),(-2L)};
                    int32_t l_1304 = 0x483C89E6L;
                    int i;
                    if (((*l_955) && ((*l_955) = p_63)))
                    { /* block id: 579 */
                        (**g_132) = ((safe_add_func_int16_t_s_s((((+(0x0BB43D8DL >= (**g_550))) || ((1UL != l_1220) || (((safe_sub_func_int16_t_s_s(l_1220, ((((**g_1144) = ((*g_1141) = (safe_rshift_func_int16_t_s_s(p_63, 14)))) || (safe_add_func_int8_t_s_s((((l_1222 = ((safe_mul_func_int64_t_s_s(((safe_sub_func_int16_t_s_s((*g_1145), ((void*)0 != (*l_862)))) || (*g_1178)), p_63)) > p_64)) < p_64) <= 0x65414A5A5653727ELL), g_340.f0))) == g_491.f0))) ^ l_1220) ^ 6L))) >= p_61), g_681.f2)) , p_61);
                    }
                    else
                    { /* block id: 584 */
                        uint64_t l_1282 = 0x06C9549B46F9DB5FLL;
                        l_1148[0] = &l_1222;
                        (*g_1172) &= ((((**l_1143) |= ((p_62 >= p_61) > (safe_add_func_int16_t_s_s((((((void*)0 != l_955) && (!(((p_61 > ((*l_955) = (safe_add_func_uint64_t_u_u(((*l_1143) == (((((*l_1012) = (((((safe_mod_func_uint32_t_u_u(((((safe_lshift_func_uint32_t_u_u(((safe_rshift_func_int32_t_s_u(((**g_132) = (((safe_sub_func_int8_t_s_s(p_63, (((safe_sub_func_int16_t_s_s(0xF54AL, (safe_div_func_int64_t_s_s(((*g_1122) , l_1220), l_1282)))) && p_64) != p_64))) != 65527UL) || p_63)), (****g_1218))) , 1UL), 14)) | g_1161.f0) | l_1283) <= 9L), 0xDCA516D2L)) , l_1108) < p_61) >= 0x22L) , (void*)0)) != l_1284) || (-7L)) , l_1285)), l_976[0][1][0])))) | g_594[6]) != 0L))) & 65533UL) <= 1UL), (-9L))))) || 0UL) & 0xC00BFBE0L);
                    }
                    l_1288++;
                    for (g_26.f1.f3 = 0; (g_26.f1.f3 >= 28); ++g_26.f1.f3)
                    { /* block id: 595 */
                        int32_t *l_1293[5][7][2] = {{{&l_1222,&g_1161.f0},{(void*)0,&g_986.f0},{&l_1286[4],&g_231[1]},{&l_1287[4][0][1],(void*)0},{(void*)0,(void*)0},{(void*)0,&l_939},{(void*)0,(void*)0}},{{&l_1220,&g_696.f1},{&l_712[5],&l_1220},{&l_935,&l_712[2]},{&l_935,&l_1220},{&l_712[5],&g_696.f1},{&l_1220,(void*)0},{(void*)0,&l_939}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{&l_1287[4][0][1],&g_231[1]},{&l_1286[4],&g_986.f0},{(void*)0,&g_1161.f0},{&l_1222,&l_1287[2][1][2]},{(void*)0,&l_1173[1]}},{{&l_936,(void*)0},{&g_986.f0,&l_935},{&l_935,&l_1286[4]},{(void*)0,&l_1286[4]},{&l_935,&l_935},{&g_986.f0,(void*)0},{&l_936,&l_1173[1]}},{{(void*)0,&l_1287[2][1][2]},{&l_1222,&g_1161.f0},{(void*)0,&g_986.f0},{&l_1286[4],&g_231[1]},{&l_1287[4][0][1],(void*)0},{(void*)0,(void*)0},{(void*)0,&l_939}}};
                        int64_t *l_1321 = &l_1095;
                        int i, j, k;
                        (*g_132) = l_1293[1][6][1];
                        l_1287[6][1][2] ^= (safe_add_func_int32_t_s_s((safe_rshift_func_int64_t_s_s((safe_mod_func_uint8_t_u_u((safe_mul_func_int64_t_s_s(((safe_mul_func_uint64_t_u_u(0x4AF2F63C37F71F3ALL, (247UL ^ ((((l_1304 != (safe_sub_func_uint16_t_u_u((p_61 != ((((*l_1016)--) , (safe_lshift_func_int64_t_s_s(((0x858CF357L ^ (((((l_1286[1] & (safe_rshift_func_uint64_t_u_u((((safe_sub_func_uint8_t_u_u((safe_mul_func_uint32_t_u_u((safe_lshift_func_uint64_t_u_s(g_1064.f1, ((*l_1321) = (-1L)))), (*l_955))), l_1115)) , 0x222AD956L) >= (*l_955)), l_1322))) > (*g_551)) || (**g_1177)) == l_1304) != 0x6C93A03DL)) != 6UL), p_61))) != (-5L))), 0x4CCBL))) | l_1173[0]) == 0x233DE874D2EF2DFFLL) , p_64)))) <= l_1322), g_696.f0)), 0xE2L)), 40)), 0x6650A732L));
                        if (p_62)
                            continue;
                    }
                }
                for (p_62 = 28; (p_62 > 7); p_62 = safe_sub_func_uint8_t_u_u(p_62, 3))
                { /* block id: 605 */
                    uint16_t *l_1334[9] = {&l_1199,&l_1199,&l_1199,&l_1199,&l_1199,&l_1199,&l_1199,&l_1199,&l_1199};
                    int32_t l_1353 = (-4L);
                    int32_t *l_1358 = (void*)0;
                    int i;
                    if (((safe_sub_func_uint8_t_u_u(((((g_1327 , (void*)0) != (void*)0) < (safe_div_func_uint64_t_u_u((safe_sub_func_int64_t_s_s(p_62, (!((((g_331 ^= (~1UL)) ^ (safe_add_func_int32_t_s_s(((l_991[0] >= (safe_sub_func_int64_t_s_s(p_62, (p_62 == (((safe_add_func_uint8_t_u_u((safe_rshift_func_uint64_t_u_s(g_11, l_1108)), p_61)) && g_696.f1) , g_340.f0))))) ^ p_61), p_61))) ^ p_62) && g_231[1])))), g_145))) == g_723.f0), g_594[1])) != l_1287[4][0][0]))
                    { /* block id: 607 */
                        uint32_t ** const *l_1349 = (void*)0;
                        uint32_t ** const **l_1350 = &l_1349;
                        l_1220 ^= ((*g_1172) = ((((safe_rshift_func_uint16_t_u_u((((***g_1180) >= (0xE6D4L || (g_300.f3 > (((safe_div_func_int16_t_s_s((((*l_1350) = l_1349) == &l_1033), l_1222)) | ((l_1352 = (0x6066L < (l_1287[4][0][0] && (l_1173[1] = ((***g_1219) = ((((*l_955) < g_200) , l_1351) != l_991[1])))))) == g_43)) >= (*g_1172))))) < 0x61B8L), 6)) <= l_1115) <= g_383.f1) <= 0xF1CDB114822D2B5ELL));
                        if (l_1353)
                            continue;
                    }
                    else
                    { /* block id: 615 */
                        int32_t **l_1357[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1357[i] = &l_1356;
                        (**g_1171) ^= ((l_1353 && 6UL) <= (safe_rshift_func_uint32_t_u_u((((l_1358 = l_1356) != (l_1359[1] = l_1356)) == (0L >= (safe_div_func_int8_t_s_s((-1L), (safe_lshift_func_uint16_t_u_s(((safe_lshift_func_int16_t_s_s(p_61, ((**g_1144) = (p_61 & (safe_div_func_int64_t_s_s(l_1368, (*l_955))))))) || p_61), 2)))))), l_976[0][1][0])));
                    }
                    return l_1369;
                }
                return (*g_1172);
            }
            else
            { /* block id: 624 */
                uint32_t l_1371 = 0xCEDC0DD7L;
                int32_t l_1375 = 0xE639CB7DL;
                int8_t *l_1416 = &l_1211[0][3];
                int8_t ** const l_1415 = &l_1416;
                int64_t * const *l_1439[2];
                int64_t * const **l_1438 = &l_1439[0];
                int i;
                for (i = 0; i < 2; i++)
                    l_1439[i] = (void*)0;
                if (p_61)
                { /* block id: 625 */
                    uint32_t l_1374 = 4294967291UL;
                    union U2 *****l_1391 = &l_1390[1][0];
                    int8_t *l_1397 = &g_340.f0;
                    int32_t l_1398 = 0x65398F91L;
                    (*g_132) = func_71(p_64);
                    l_1371++;
                    l_1398 = ((*l_955) = (((p_62 == (l_1375 = ((**g_550) &= l_1374))) & ((g_218 |= ((*l_955) , (safe_sub_func_uint32_t_u_u(p_62, ((safe_mul_func_uint16_t_u_u((**g_1177), ((((((*l_1016) = ((safe_lshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u((((safe_sub_func_int8_t_s_s(((*l_1397) = (safe_rshift_func_int16_t_s_s(p_63, (safe_rshift_func_uint16_t_u_u((((*l_1391) = l_1390[0][0]) == ((safe_add_func_int32_t_s_s((0x172577FDA4894BB9LL <= (safe_mod_func_uint64_t_u_u(((((*g_1145) = ((((*g_1172) = (((&l_775 != &l_775) && l_1374) == 0x62L)) , (void*)0) != l_1396)) , 0UL) < 0UL), 5UL))), l_1374)) , (void*)0)), 10))))), 0x16L)) , (-1L)) , 6UL), 0xE67AAAD7C664A141LL)), 6)) , p_64)) & g_482.f0) >= p_62) >= 0xAFA30820L) && 0x9EC44311FD84CA7FLL))) , l_1374))))) == g_723.f3)) || 0xB9F19C4C1735D5E7LL));
                }
                else
                { /* block id: 638 */
                    for (g_681.f1.f0 = 5; (g_681.f1.f0 >= 0); g_681.f1.f0 -= 1)
                    { /* block id: 641 */
                        uint64_t l_1399 = 0x58A069B95EBF88C0LL;
                        int32_t l_1401 = 0x45236B03L;
                        (**l_1396) = func_71(l_1399);
                        l_1401 = l_1400;
                    }
                }
                for (l_1370 = 7; (l_1370 >= 0); l_1370 -= 1)
                { /* block id: 648 */
                    int i;
                    l_1173[l_1370] = (p_61 ^ (~1L));
                }
                for (p_62 = 0; (p_62 <= 7); p_62 += 1)
                { /* block id: 653 */
                    const union U2 **l_1423 = (void*)0;
                    const union U2 ***l_1422 = &l_1423;
                    int32_t l_1425[4] = {0x2CA820FEL,0x2CA820FEL,0x2CA820FEL,0x2CA820FEL};
                    uint16_t ***l_1430 = &l_1236;
                    int64_t * const *l_1434 = &g_611;
                    int64_t * const **l_1433 = &l_1434;
                    int i;
                    for (g_1161.f0 = 0; (g_1161.f0 >= 0); g_1161.f0 -= 1)
                    { /* block id: 656 */
                        int i, j;
                        if (l_1211[g_1161.f0][(g_1161.f0 + 5)])
                            break;
                    }
                    (**l_1396) = &l_1173[p_62];
                    for (g_265.f1.f3 = 2; (g_265.f1.f3 <= 7); g_265.f1.f3 += 1)
                    { /* block id: 662 */
                        int32_t l_1403[7] = {0x0750B60CL,0x0750B60CL,0x0750B60CL,0x0750B60CL,0x0750B60CL,0x0750B60CL,0x0750B60CL};
                        int8_t l_1404[6] = {0xB9L,0xB9L,0xB9L,0xB9L,0xB9L,0xB9L};
                        uint16_t l_1406 = 65529UL;
                        int64_t * const **l_1435[1][7] = {{&l_1434,&l_1434,&l_1434,&l_1434,&l_1434,&l_1434,&l_1434}};
                        int64_t * const ***l_1436 = (void*)0;
                        int64_t * const ***l_1437[8] = {&l_1433,&l_1433,&l_1433,&l_1433,&l_1433,&l_1433,&l_1433,&l_1433};
                        uint32_t *l_1448 = &g_93;
                        int i, j;
                        ++l_1406;
                        if (g_632[(p_62 + 1)])
                            continue;
                        l_1425[0] &= (safe_div_func_int64_t_s_s((g_383.f1 || ((0xF56FL <= l_1173[p_62]) , (g_1327.f1.f0 = (safe_rshift_func_uint8_t_u_u((&l_775 != (((safe_mod_func_int64_t_s_s(((void*)0 == l_1415), (safe_sub_func_uint8_t_u_u(((*l_1016) = ((l_1419 == (l_1173[1] > ((**l_1415) = (safe_lshift_func_uint8_t_u_u((p_61 <= p_61), g_204.f2))))) , 0xEDL)), g_1146)))) && 0xEDC8316F3DF76AF7LL) , l_1422)), g_265.f0))))), l_1424[3][5][5]));
                        (*l_955) = (safe_rshift_func_int16_t_s_s(((*l_1017) = (safe_div_func_int32_t_s_s((l_1430 == (p_63 , (void*)0)), ((safe_div_func_int16_t_s_s((((g_1440 = (l_1438 = (l_1435[0][3] = l_1433))) != &g_610) , p_63), (safe_div_func_int32_t_s_s((safe_div_func_int8_t_s_s((+((safe_rshift_func_uint8_t_u_u(g_1064.f1, 3)) , (((void*)0 != l_1448) , p_61))), p_64)), g_632[(p_62 + 1)])))) & g_340.f0)))), p_61));
                    }
                }
            }
            for (l_936 = (-27); (l_936 > 13); l_936 = safe_add_func_int32_t_s_s(l_936, 3))
            { /* block id: 679 */
                int32_t ***l_1452 = &g_132;
                int32_t l_1507 = 0x657DED27L;
                int32_t l_1508 = (-7L);
                int32_t l_1509 = (-7L);
                const union U2 ****l_1517 = (void*)0;
                const union U2 *****l_1516[10] = {&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517};
                union U1 **l_1533 = &l_1451;
                int i;
            }
        }
    }
    g_832.f0 |= ((g_148.f0 ^= ((*g_1172) = (safe_add_func_uint16_t_u_u(p_63, (safe_add_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s(((safe_add_func_int64_t_s_s((((*g_1145) |= ((((*l_1597) &= (((g_1594 , (****g_1218)) > (safe_mod_func_uint16_t_u_u(((void*)0 != (*l_1505)), p_64))) , ((((p_63 && 0xF030L) , p_61) , 250UL) ^ 0x6DL))) <= p_63) , 9L)) ^ g_594[7]), (*g_611))) < p_61), 4)), p_62)))))) ^ l_1598);
    (*g_1172) &= p_62;
    return p_63;
}


/* ------------------------------------------ */
/* 
 * reads : g_224 g_225 g_132 g_21 g_204.f1 g_109 g_204.f5 g_265 g_204.f3 g_11 g_79.f0 g_78 g_231 g_83 g_218 g_225.f0 g_93 g_298 g_681
 * writes: g_83 g_21 g_26.f1.f3 g_204.f1 g_204.f0 g_109 g_204.f3 g_218 g_93 g_490
 */
static uint64_t  func_69(int32_t * p_70)
{ /* block id: 143 */
    int32_t *l_226 = &g_204.f1;
    uint64_t *l_229 = &g_26.f1.f3;
    uint64_t *l_230[2];
    int32_t l_232 = 4L;
    int32_t l_233 = 0x5C60F542L;
    int32_t l_234 = (-2L);
    int32_t l_235 = 0x54F50ACFL;
    int8_t *l_242 = (void*)0;
    uint32_t l_243 = 0x83BD034AL;
    uint32_t l_246 = 0xD04D0B8DL;
    uint32_t l_247 = 0xCCA06ABCL;
    uint64_t l_274[4];
    const struct S0 **l_301 = (void*)0;
    int8_t l_305[10] = {0L,3L,0L,(-1L),(-1L),0L,3L,0L,(-1L),(-1L)};
    struct S0 **l_414 = &g_206;
    int64_t *l_441 = &g_442;
    const int16_t l_445 = (-1L);
    int32_t l_465 = 0L;
    int32_t l_486 = 0x22580578L;
    union U1 *l_487 = &g_148;
    const union U2 *l_529 = &g_482;
    int32_t l_563 = 0x7E5FB1E7L;
    int32_t ***l_577 = &g_132;
    const uint32_t *l_679 = &g_93;
    const uint32_t **l_678 = &l_679;
    const uint32_t ***l_677 = &l_678;
    union U1 **l_682 = &g_490;
    int i;
    for (i = 0; i < 2; i++)
        l_230[i] = &g_204.f3;
    for (i = 0; i < 4; i++)
        l_274[i] = 18446744073709551615UL;
    (*g_132) = (g_224 , (g_225 , l_226));
    if ((((void*)0 != &g_131) == ((safe_add_func_uint32_t_u_u(((((*l_229) = (g_21++)) , (((l_230[0] == l_230[1]) > (safe_rshift_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((((g_204.f0 = ((*l_226) |= 0x55L)) < l_234) == ((*l_229) = 0x630B6AAB4845D3B3LL)), l_243)), 2))) && ((safe_rshift_func_uint64_t_u_s(18446744073709551614UL, 49)) > (*l_226)))) <= l_246), l_247)) > l_234)))
    { /* block id: 150 */
        uint32_t l_252 = 0xCB9C9697L;
        int32_t l_273 = 2L;
        int32_t *l_276 = &g_204.f1;
        int32_t *l_277 = &g_79.f0;
        int32_t *l_278 = &l_233;
        int32_t *l_279 = &l_233;
        int32_t *l_280 = &g_79.f0;
        uint8_t l_281 = 0x82L;
        uint8_t *l_288[9][3] = {{&g_94,&g_94,&g_190},{(void*)0,&l_281,&g_190},{&l_281,(void*)0,&g_190},{&g_94,&g_94,&g_190},{(void*)0,&l_281,&g_190},{&l_281,(void*)0,&g_190},{&g_94,&g_94,&g_190},{(void*)0,&l_281,&g_190},{&l_281,(void*)0,&g_190}};
        uint32_t *l_294 = &g_93;
        struct S0 **l_297[6][8][5] = {{{(void*)0,(void*)0,&g_206,&g_206,(void*)0},{(void*)0,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,(void*)0,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,(void*)0},{&g_206,(void*)0,&g_206,&g_206,&g_206},{(void*)0,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{(void*)0,&g_206,&g_206,&g_206,&g_206}},{{(void*)0,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,(void*)0,(void*)0,&g_206},{&g_206,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{(void*)0,&g_206,&g_206,&g_206,&g_206},{(void*)0,&g_206,(void*)0,(void*)0,&g_206}},{{&g_206,&g_206,(void*)0,&g_206,&g_206},{&g_206,&g_206,(void*)0,(void*)0,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,(void*)0,(void*)0},{&g_206,(void*)0,(void*)0,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206}},{{&g_206,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,(void*)0,&g_206,&g_206},{(void*)0,(void*)0,&g_206,&g_206,&g_206},{(void*)0,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,&g_206,&g_206,(void*)0},{(void*)0,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,(void*)0,&g_206}},{{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{(void*)0,&g_206,&g_206,(void*)0,&g_206},{(void*)0,(void*)0,&g_206,&g_206,&g_206},{&g_206,&g_206,(void*)0,(void*)0,&g_206},{&g_206,&g_206,(void*)0,&g_206,&g_206}},{{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206},{&g_206,&g_206,(void*)0,&g_206,&g_206},{&g_206,&g_206,&g_206,&g_206,(void*)0},{&g_206,&g_206,&g_206,(void*)0,&g_206},{&g_206,&g_206,(void*)0,&g_206,&g_206},{&g_206,&g_206,(void*)0,(void*)0,&g_206},{&g_206,&g_206,&g_206,&g_206,&g_206}}};
        int32_t *l_302[2][4] = {{&g_78,&g_78,&g_148.f0,&g_78},{&g_78,&l_232,&l_232,&g_78}};
        int64_t l_455 = 0x89F0EA6DABB3B7EDLL;
        union U1 *l_492 = &g_493;
        union U2 *l_530 = (void*)0;
        uint32_t l_532[9][7][1] = {{{0xD977D785L},{0x8B56258CL},{0x6A4E2D68L},{3UL},{1UL},{1UL},{3UL}},{{0x6A4E2D68L},{0x8B56258CL},{0xD977D785L},{1UL},{0x8B56258CL},{4294967288UL},{0x8B56258CL}},{{1UL},{0xD977D785L},{0x8B56258CL},{0x6A4E2D68L},{3UL},{1UL},{1UL}},{{3UL},{0x6A4E2D68L},{0x8B56258CL},{0xD977D785L},{1UL},{0x8B56258CL},{4294967288UL}},{{0x8B56258CL},{1UL},{0xD977D785L},{0x8B56258CL},{0x6A4E2D68L},{3UL},{1UL}},{{1UL},{3UL},{0x6A4E2D68L},{0x8B56258CL},{0xD977D785L},{1UL},{0x8B56258CL}},{{4294967288UL},{0x8B56258CL},{1UL},{0xD977D785L},{0x8B56258CL},{0x6A4E2D68L},{3UL}},{{1UL},{1UL},{3UL},{0x6A4E2D68L},{0x8B56258CL},{0xD977D785L},{1UL}},{{0x8B56258CL},{4294967288UL},{0x8B56258CL},{1UL},{0xD977D785L},{0x8B56258CL},{0x6A4E2D68L}}};
        volatile uint32_t * volatile *l_559 = &g_556;
        uint32_t l_566 = 4294967295UL;
        int32_t *** const *l_572 = (void*)0;
        int32_t *** const ** const l_571 = &l_572;
        union U1 **l_591 = &g_490;
        union U1 ***l_590 = &l_591;
        int64_t **l_612 = &g_611;
        int32_t l_663 = (-3L);
        uint32_t *l_670 = &g_594[4];
        uint32_t **l_669 = &l_670;
        int i, j, k;
        for (g_109 = 0; (g_109 >= 39); g_109 = safe_add_func_int8_t_s_s(g_109, 6))
        { /* block id: 153 */
            int32_t l_262 = 0x25F73CB7L;
            int64_t *l_263[6][2] = {{&g_200,&g_200},{&g_200,&g_200},{&g_200,&g_200},{&g_200,&g_200},{&g_200,&g_200},{&g_200,&g_200}};
            int32_t l_264 = (-1L);
            int32_t l_266 = 7L;
            uint8_t *l_272[5];
            int32_t ****l_275 = (void*)0;
            int i, j;
            for (i = 0; i < 5; i++)
                l_272[i] = &g_94;
            (*g_83) = (((safe_sub_func_int16_t_s_s(l_252, ((((safe_div_func_int64_t_s_s(((safe_lshift_func_uint64_t_u_u(g_204.f5, l_252)) , (safe_div_func_uint8_t_u_u((l_273 = (g_224 , (l_252 == ((!(safe_rshift_func_int64_t_s_u((l_264 = l_262), (g_265 , (++g_204.f3))))) || (+((safe_mod_func_int32_t_s_s((l_264 , (*p_70)), 1L)) <= l_266)))))), l_274[1]))), g_21)) >= g_231[1]) , l_275) == &g_131))) && 18446744073709551615UL) != (*l_226));
        }
        l_281--;
        l_232 ^= (safe_sub_func_int32_t_s_s((safe_div_func_int8_t_s_s(1L, (++g_218))), ((((~g_225.f0) & ((g_21 != (safe_lshift_func_uint32_t_u_s((--(*l_294)), 11))) > ((*l_278) |= (*p_70)))) , l_297[3][2][3]) != (l_301 = g_298))));
        for (g_204.f3 = 1; (g_204.f3 != 19); g_204.f3 = safe_add_func_uint64_t_u_u(g_204.f3, 6))
        { /* block id: 167 */
            uint64_t *l_329 = &l_274[1];
            int32_t l_339 = (-2L);
            int32_t l_345 = 0x6B528C98L;
            int32_t l_348 = 0x822A9634L;
            uint32_t l_411 = 0xB49C120BL;
            struct S0 **l_415 = &g_206;
            uint32_t l_466 = 0xDB2E7EE6L;
            union U1 *l_510 = (void*)0;
            struct S0 ***l_520 = &l_297[0][3][3];
            int32_t l_531 = (-1L);
            uint32_t *l_533 = &l_252;
            int64_t l_534[7][1] = {{0L},{0L},{0L},{0L},{0L},{0L},{0L}};
            uint32_t **l_573 = &g_551;
            uint32_t **l_660 = &l_533;
            uint32_t ***l_659[10] = {(void*)0,(void*)0,&l_660,(void*)0,(void*)0,&l_660,(void*)0,(void*)0,(void*)0,&l_660};
            const int8_t l_664 = 1L;
            int16_t *l_673 = (void*)0;
            int16_t *l_674 = &g_585.f3;
            int16_t *l_675[8][4][3] = {{{&g_43,&g_43,(void*)0},{&g_43,&g_43,&g_43},{&g_43,&g_43,(void*)0},{&g_43,&g_43,(void*)0}},{{&g_491.f3,&g_43,&g_43},{(void*)0,(void*)0,&g_43},{(void*)0,&g_43,&g_43},{&g_491.f3,&g_43,&g_43}},{{&g_43,(void*)0,&g_491.f3},{&g_43,&g_491.f3,&g_43},{&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43}},{{&g_43,&g_43,&g_43},{&g_43,&g_43,(void*)0},{&g_43,&g_491.f3,(void*)0},{&g_43,(void*)0,&g_43}},{{&g_43,&g_43,(void*)0},{&g_43,&g_43,&g_43},{&g_43,(void*)0,&g_43},{&g_43,&g_43,(void*)0}},{{&g_43,&g_43,&g_43},{&g_43,&g_43,(void*)0},{&g_43,&g_43,(void*)0},{&g_491.f3,&g_43,&g_43}},{{(void*)0,(void*)0,&g_43},{(void*)0,&g_43,&g_43},{&g_491.f3,&g_43,&g_43},{&g_43,(void*)0,&g_491.f3}},{{&g_43,&g_491.f3,&g_43},{&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43}}};
            int i, j, k;
        }
    }
    else
    { /* block id: 330 */
        int8_t l_680[5][9] = {{0x37L,0x37L,0xB6L,(-1L),0L,(-1L),0xB6L,0x37L,0x37L},{1L,(-3L),0xCAL,0xB7L,0xCAL,(-3L),1L,1L,(-3L)},{(-1L),0xA8L,0xB6L,0xA8L,(-1L),0L,0L,(-1L),0xA8L},{1L,0xCAL,1L,0x29L,(-1L),(-1L),0x29L,1L,0xCAL},{0x37L,0x18L,0L,0xB6L,0xB6L,0L,0x18L,0x37L,0x18L}};
        int i, j;
        return l_680[3][6];
    }
    (*l_682) = (g_681 , &g_148);
    return (***l_577);
}


/* ------------------------------------------ */
/* 
 * reads : g_26.f2 g_79 g_11 g_78 g_93 g_79.f0 g_94 g_21 g_109 g_145 g_148 g_126 g_43 g_148.f0 g_132 g_83 g_131 g_190 g_42 g_130 g_205 g_211 g_218
 * writes: g_26.f2 g_78 g_83 g_93 g_94 g_21 g_109 g_26.f1.f1 g_79.f0 g_190 g_200 g_206 g_211 g_218 g_131
 */
static int32_t * func_71(int64_t  p_72)
{ /* block id: 15 */
    int32_t *l_82[10][2][3] = {{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_78,&g_79.f0,&g_79.f0},{&g_78,&g_11,&g_11}},{{&g_79.f0,&g_11,&g_11},{&g_11,&g_78,&g_78}},{{&g_79.f0,&g_11,&g_11},{&g_11,&g_78,&g_78}}};
    int32_t **l_89 = (void*)0;
    int32_t *l_91 = &g_11;
    int32_t **l_90 = &l_91;
    uint32_t *l_92 = &g_93;
    int32_t l_95 = 1L;
    uint32_t l_124 = 0xAA92F95BL;
    uint64_t l_127 = 1UL;
    uint8_t *l_151 = &g_94;
    uint64_t l_152 = 18446744073709551615UL;
    int32_t l_169 = (-1L);
    uint32_t l_171 = 0x534E026DL;
    int32_t *l_223 = &g_11;
    int i, j, k;
    for (g_26.f2 = (-23); (g_26.f2 != (-24)); g_26.f2--)
    { /* block id: 18 */
        int32_t *l_75 = (void*)0;
        int32_t *l_76 = (void*)0;
        int32_t *l_77 = &g_78;
        (*l_77) = 2L;
    }
    if ((g_94 ^= (((g_79 , (safe_mod_func_int64_t_s_s(((g_79 , (((g_83 = l_82[3][1][0]) != l_82[4][0][1]) < 0xA8E06690L)) < ((0x26CBAAD5L ^ ((*l_92) |= (safe_sub_func_uint32_t_u_u(p_72, ((!((((safe_sub_func_int32_t_s_s(((((&g_78 != ((*l_90) = (void*)0)) < g_11) & p_72) > g_78), p_72)) , (*l_90)) == &g_11) , p_72)) ^ p_72))))) < g_79.f0)), 0x6EB9B4FE2E886BE4LL))) < 1L) || 0x4D9FAD1AL)))
    { /* block id: 25 */
        return &g_78;
    }
    else
    { /* block id: 27 */
        uint16_t l_96 = 0x48ECL;
        l_96++;
    }
lbl_140:
    for (g_21 = (-17); (g_21 >= 42); g_21++)
    { /* block id: 32 */
        int16_t l_101 = 0x6465L;
        int32_t l_102 = 0L;
        int32_t l_103 = 0x1C656C0CL;
        int32_t l_104 = 0xE8D41E4DL;
        int32_t l_105 = 0x62A55EFAL;
        int32_t l_106 = 0L;
        int32_t l_107 = 1L;
        int32_t l_108[5] = {0x7964C2DCL,0x7964C2DCL,0x7964C2DCL,0x7964C2DCL,0x7964C2DCL};
        int16_t l_135[8];
        int i;
        for (i = 0; i < 8; i++)
            l_135[i] = 0xE657L;
        --g_109;
        for (g_26.f2 = (-26); (g_26.f2 < 2); ++g_26.f2)
        { /* block id: 36 */
            const uint32_t l_128 = 18446744073709551613UL;
            int32_t ****l_136 = &g_131;
            int32_t l_139 = 1L;
        }
        if (g_94)
            goto lbl_140;
    }
    if ((g_26.f1.f1 = ((l_152 = (g_78 ^= (safe_mod_func_int8_t_s_s((safe_add_func_uint64_t_u_u((((g_145 != (safe_mul_func_int16_t_s_s(0xED6AL, 0xA769L))) ^ ((((((g_93 & p_72) < ((*l_151) = (((g_148 , 0xD3BF3B3623E6BFF2LL) == 0xE68E44B7FDB35930LL) != (((safe_sub_func_int32_t_s_s(p_72, g_11)) != g_126) || p_72)))) , g_43) || (-1L)) | p_72) , p_72)) , 18446744073709551615UL), (-4L))), g_148.f0)))) == p_72)))
    { /* block id: 62 */
        uint64_t *l_155 = &l_152;
        int32_t l_162 = (-8L);
        int32_t l_167 = (-1L);
        int32_t l_168 = (-1L);
        int32_t l_170[8][8] = {{(-2L),0x87112B94L,0x05A60EC3L,0x87112B94L,(-2L),(-10L),0xCA9E05B6L,1L},{(-2L),(-10L),0xCA9E05B6L,1L,0xCA9E05B6L,(-10L),(-2L),0x87112B94L},{(-3L),0x87112B94L,0xCA9E05B6L,0L,0L,0L,0xCA9E05B6L,0x87112B94L},{0xCA9E05B6L,0xFA42621BL,0x05A60EC3L,1L,0L,0x87112B94L,0L,1L},{(-3L),0xFA42621BL,(-3L),0x87112B94L,0xCA9E05B6L,0L,0L,0L},{(-2L),0x87112B94L,0x05A60EC3L,0x87112B94L,(-2L),(-10L),0xCA9E05B6L,1L},{(-2L),(-10L),0xCA9E05B6L,1L,0xCA9E05B6L,(-10L),(-2L),0x87112B94L},{(-3L),0x87112B94L,0xCA9E05B6L,0L,0L,0L,0xCA9E05B6L,0x87112B94L}};
        int i, j;
        if ((((safe_div_func_int64_t_s_s(0xF5B9073D22E32B80LL, (++(*l_155)))) , &g_93) == &g_93))
        { /* block id: 64 */
            uint64_t l_158 = 0UL;
            l_158++;
            return (*g_132);
        }
        else
        { /* block id: 67 */
            int32_t ***l_161 = (void*)0;
            int32_t l_166[3];
            struct S0 *l_203 = &g_204;
            struct S0 **l_202 = &l_203;
            int i;
            for (i = 0; i < 3; i++)
                l_166[i] = 1L;
lbl_174:
            l_162 |= ((void*)0 == l_161);
            for (l_152 = 0; (l_152 >= 22); ++l_152)
            { /* block id: 71 */
                (*l_90) = ((**g_131) = &l_162);
            }
            for (l_95 = 1; (l_95 >= 0); l_95 -= 1)
            { /* block id: 77 */
                int16_t l_165 = 0x666CL;
                uint8_t * const l_183 = (void*)0;
                int32_t **l_193[10][2][9] = {{{&l_82[3][1][0],&l_82[3][1][0],(void*)0,&l_82[3][1][0],&l_82[3][1][0],(void*)0,(void*)0,&l_82[3][0][0],&l_91},{&l_82[0][0][2],&l_82[3][0][0],&l_91,&l_91,(void*)0,&l_82[4][1][1],(void*)0,&l_91,(void*)0}},{{&g_83,&l_82[0][0][1],&g_83,&l_82[3][0][2],(void*)0,(void*)0,&l_82[1][1][0],(void*)0,&l_82[3][1][0]},{&l_82[3][1][0],&g_83,&g_83,&g_83,&l_82[2][0][1],&g_83,&l_82[5][1][2],&l_91,&l_82[3][1][0]}},{{(void*)0,&l_82[3][0][0],&l_82[9][0][0],(void*)0,&l_82[3][1][0],&l_82[2][0][1],&g_83,&l_82[0][0][2],&l_91},{(void*)0,&l_82[5][1][2],&l_82[3][1][0],(void*)0,&l_82[5][1][2],&l_82[3][0][0],&l_82[3][0][2],&l_82[0][0][2],&l_82[3][1][0]}},{{&l_91,&g_83,&g_83,(void*)0,&l_91,&l_91,&l_82[3][1][0],&l_91,(void*)0},{&l_82[3][1][0],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_82[3][1][0]}},{{&l_82[3][0][0],(void*)0,(void*)0,&l_82[3][1][0],(void*)0,&g_83,&l_82[5][0][0],&l_91,(void*)0},{(void*)0,&l_91,&l_82[5][1][2],(void*)0,(void*)0,&g_83,&l_82[4][1][1],&l_82[3][0][0],(void*)0}},{{&l_82[3][0][0],(void*)0,&l_82[5][0][0],&l_82[8][1][2],&l_82[6][0][1],&l_91,&l_82[0][0][1],(void*)0,&l_82[3][1][0]},{&l_82[3][1][0],&g_83,(void*)0,&l_82[3][1][0],&l_82[3][1][0],&l_82[3][0][2],&l_82[3][1][0],&l_82[3][1][0],(void*)0}},{{&l_91,&l_82[3][1][0],&l_82[5][1][2],&l_82[3][1][0],(void*)0,&l_82[3][1][0],&l_91,&l_82[3][1][0],&l_82[6][1][2]},{&g_83,&l_82[9][0][0],&l_82[5][1][2],&g_83,&l_82[1][1][0],&l_82[5][0][0],(void*)0,&l_82[3][1][0],&l_82[2][0][1]}},{{(void*)0,&g_83,&l_82[6][1][2],&l_82[3][1][0],&l_91,&l_82[0][0][1],&l_82[3][1][0],&l_82[0][0][1],&l_91},{&g_83,&l_82[3][1][0],&l_82[3][1][0],&g_83,&l_82[3][1][0],&l_91,&l_82[3][1][0],&l_82[4][1][1],(void*)0}},{{&l_82[2][0][1],&l_82[6][0][1],(void*)0,&l_82[3][0][0],&l_82[7][0][1],(void*)0,&l_82[6][0][2],&l_82[5][0][0],&l_91},{&l_91,(void*)0,&l_82[6][0][2],&l_82[3][1][0],&l_82[3][1][0],&l_82[4][1][1],(void*)0,(void*)0,&l_82[0][0][2]}},{{(void*)0,&l_82[8][1][2],&g_83,&g_83,&l_91,&l_82[3][0][2],(void*)0,&l_82[3][1][0],(void*)0},{&g_83,(void*)0,&l_82[3][1][0],(void*)0,&l_82[1][1][0],(void*)0,(void*)0,&l_82[3][0][2],&g_83}}};
                int i, j, k;
                if ((***g_131))
                    break;
                l_171++;
                for (g_78 = 1; (g_78 >= 0); g_78 -= 1)
                { /* block id: 82 */
                    const uint64_t l_188 = 6UL;
                    int32_t l_194 = 0x8B2B7316L;
                    uint8_t *l_195[6][6] = {{&g_190,&g_190,&g_190,&g_190,&g_190,&g_190},{&g_94,&g_190,&g_190,&g_190,&g_94,&g_94},{(void*)0,&g_190,&g_190,(void*)0,&g_190,(void*)0},{(void*)0,&g_190,(void*)0,&g_190,&g_190,(void*)0},{&g_94,&g_94,&g_190,&g_190,&g_190,&g_94},{&g_190,&g_190,&g_190,&g_190,&g_190,&g_190}};
                    int i, j;
                    for (g_93 = 0; (g_93 <= 1); g_93 += 1)
                    { /* block id: 85 */
                        int i, j, k;
                        if (g_94)
                            goto lbl_174;
                        return l_82[(l_95 + 5)][g_78][(l_95 + 1)];
                    }
                    if ((**g_132))
                        continue;
                    g_79.f0 |= l_162;
                    for (g_109 = 0; (g_109 <= 1); g_109 += 1)
                    { /* block id: 93 */
                        uint8_t *l_189 = &g_190;
                        int32_t l_196 = 0x6A746CB8L;
                        int i, j, k;
                        l_82[(l_95 + 4)][g_78][g_109] = l_82[(g_78 + 2)][g_109][g_78];
                        l_196 ^= (safe_div_func_uint16_t_u_u((safe_mul_func_uint64_t_u_u(((*l_155) = l_165), (safe_add_func_uint16_t_u_u((safe_rshift_func_int32_t_s_s((l_183 != (((((*l_151) = (safe_unary_minus_func_uint64_t_u(0x7C4F4EFCB6989C77LL))) != (safe_unary_minus_func_int64_t_s(((safe_add_func_int8_t_s_s(l_188, ((*l_189) &= g_11))) > ((l_194 = (0x053127A1L && (((safe_mul_func_uint32_t_u_u(((*g_83) >= l_188), ((((l_193[7][1][3] == (*g_131)) ^ p_72) || p_72) > g_21))) , (void*)0) == l_155))) > 0L))))) <= p_72) , l_195[1][2])), 19)), g_42)))), p_72));
                    }
                }
                if ((****g_130))
                    break;
                for (l_168 = 1; (l_168 >= 0); l_168 -= 1)
                { /* block id: 105 */
                    int32_t l_201 = (-3L);
                    for (g_93 = 0; (g_93 <= 1); g_93 += 1)
                    { /* block id: 108 */
                        uint16_t l_198 = 0x0D5EL;
                        int64_t *l_199 = &g_200;
                        l_166[0] = (safe_unary_minus_func_int16_t_s(p_72));
                        l_162 = (l_198 ^= p_72);
                        l_201 ^= (((*l_199) = l_168) | p_72);
                    }
                    for (p_72 = 0; (p_72 <= 1); p_72 += 1)
                    { /* block id: 117 */
                        int i, j, k;
                        l_166[0] |= (****g_130);
                        return l_82[(l_95 + 5)][l_95][(l_95 + 1)];
                    }
                }
            }
            (*g_205) = ((*l_202) = (void*)0);
        }
        l_168 = (***g_131);
        for (l_169 = 0; (l_169 <= 16); l_169 = safe_add_func_int64_t_s_s(l_169, 4))
        { /* block id: 129 */
            int16_t l_214 = 1L;
            int32_t l_215 = 0x85F6979FL;
            int32_t l_216 = 0xF3C6F657L;
            int32_t l_217 = 0xAC803910L;
            int32_t ****l_221 = &g_131;
            for (g_79.f0 = 27; (g_79.f0 >= (-27)); g_79.f0--)
            { /* block id: 132 */
                uint32_t * volatile * volatile *l_213[3][5] = {{&g_211,(void*)0,(void*)0,&g_211,&g_211},{&g_211,(void*)0,(void*)0,(void*)0,(void*)0},{&g_211,(void*)0,(void*)0,&g_211,&g_211}};
                int i, j;
                g_211 = g_211;
            }
            g_218--;
            (*l_221) = (*g_130);
            if (p_72)
                break;
        }
    }
    else
    { /* block id: 139 */
        int32_t *l_222 = &g_204.f1;
        return l_222;
    }
    return l_223;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_29, "g_29", print_hash_value);
    transparent_crc(g_41, "g_41", print_hash_value);
    transparent_crc(g_42, "g_42", print_hash_value);
    transparent_crc(g_43, "g_43", print_hash_value);
    transparent_crc(g_44, "g_44", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_79.f0, "g_79.f0", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_94, "g_94", print_hash_value);
    transparent_crc(g_109, "g_109", print_hash_value);
    transparent_crc(g_126, "g_126", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    transparent_crc(g_148.f0, "g_148.f0", print_hash_value);
    transparent_crc(g_190, "g_190", print_hash_value);
    transparent_crc(g_200, "g_200", print_hash_value);
    transparent_crc(g_204.f0, "g_204.f0", print_hash_value);
    transparent_crc(g_204.f1, "g_204.f1", print_hash_value);
    transparent_crc(g_204.f2, "g_204.f2", print_hash_value);
    transparent_crc(g_204.f3, "g_204.f3", print_hash_value);
    transparent_crc(g_204.f4, "g_204.f4", print_hash_value);
    transparent_crc(g_204.f5, "g_204.f5", print_hash_value);
    transparent_crc(g_218, "g_218", print_hash_value);
    transparent_crc(g_224.f0, "g_224.f0", print_hash_value);
    transparent_crc(g_225.f0, "g_225.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_231[i], "g_231[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_265.f0, "g_265.f0", print_hash_value);
    transparent_crc(g_300.f0, "g_300.f0", print_hash_value);
    transparent_crc(g_300.f1, "g_300.f1", print_hash_value);
    transparent_crc(g_300.f2, "g_300.f2", print_hash_value);
    transparent_crc(g_300.f3, "g_300.f3", print_hash_value);
    transparent_crc(g_300.f4, "g_300.f4", print_hash_value);
    transparent_crc(g_300.f5, "g_300.f5", print_hash_value);
    transparent_crc(g_331, "g_331", print_hash_value);
    transparent_crc(g_340.f0, "g_340.f0", print_hash_value);
    transparent_crc(g_340.f1, "g_340.f1", print_hash_value);
    transparent_crc(g_340.f2, "g_340.f2", print_hash_value);
    transparent_crc(g_340.f3, "g_340.f3", print_hash_value);
    transparent_crc(g_340.f4, "g_340.f4", print_hash_value);
    transparent_crc(g_340.f5, "g_340.f5", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_343[i][j], "g_343[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_383.f0, "g_383.f0", print_hash_value);
    transparent_crc(g_383.f1, "g_383.f1", print_hash_value);
    transparent_crc(g_383.f2, "g_383.f2", print_hash_value);
    transparent_crc(g_383.f3, "g_383.f3", print_hash_value);
    transparent_crc(g_383.f4, "g_383.f4", print_hash_value);
    transparent_crc(g_383.f5, "g_383.f5", print_hash_value);
    transparent_crc(g_386, "g_386", print_hash_value);
    transparent_crc(g_442, "g_442", print_hash_value);
    transparent_crc(g_482.f0, "g_482.f0", print_hash_value);
    transparent_crc(g_491.f0, "g_491.f0", print_hash_value);
    transparent_crc(g_493.f0, "g_493.f0", print_hash_value);
    transparent_crc(g_585.f0, "g_585.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_594[i], "g_594[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_632[i], "g_632[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_651.f0, "g_651.f0", print_hash_value);
    transparent_crc(g_681.f0, "g_681.f0", print_hash_value);
    transparent_crc(g_687, "g_687", print_hash_value);
    transparent_crc(g_696.f0, "g_696.f0", print_hash_value);
    transparent_crc(g_696.f1, "g_696.f1", print_hash_value);
    transparent_crc(g_696.f2, "g_696.f2", print_hash_value);
    transparent_crc(g_696.f3, "g_696.f3", print_hash_value);
    transparent_crc(g_696.f4, "g_696.f4", print_hash_value);
    transparent_crc(g_696.f5, "g_696.f5", print_hash_value);
    transparent_crc(g_723.f0, "g_723.f0", print_hash_value);
    transparent_crc(g_723.f1, "g_723.f1", print_hash_value);
    transparent_crc(g_723.f2, "g_723.f2", print_hash_value);
    transparent_crc(g_723.f3, "g_723.f3", print_hash_value);
    transparent_crc(g_723.f4, "g_723.f4", print_hash_value);
    transparent_crc(g_723.f5, "g_723.f5", print_hash_value);
    transparent_crc(g_791.f0, "g_791.f0", print_hash_value);
    transparent_crc(g_830.f0, "g_830.f0", print_hash_value);
    transparent_crc(g_832.f0, "g_832.f0", print_hash_value);
    transparent_crc(g_861.f0, "g_861.f0", print_hash_value);
    transparent_crc(g_953.f0, "g_953.f0", print_hash_value);
    transparent_crc(g_979.f0, "g_979.f0", print_hash_value);
    transparent_crc(g_979.f1, "g_979.f1", print_hash_value);
    transparent_crc(g_979.f2, "g_979.f2", print_hash_value);
    transparent_crc(g_979.f3, "g_979.f3", print_hash_value);
    transparent_crc(g_979.f4, "g_979.f4", print_hash_value);
    transparent_crc(g_979.f5, "g_979.f5", print_hash_value);
    transparent_crc(g_986.f0, "g_986.f0", print_hash_value);
    transparent_crc(g_995.f0, "g_995.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1048[i][j].f0, "g_1048[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1064.f0, "g_1064.f0", print_hash_value);
    transparent_crc(g_1064.f1, "g_1064.f1", print_hash_value);
    transparent_crc(g_1064.f2, "g_1064.f2", print_hash_value);
    transparent_crc(g_1064.f3, "g_1064.f3", print_hash_value);
    transparent_crc(g_1064.f4, "g_1064.f4", print_hash_value);
    transparent_crc(g_1064.f5, "g_1064.f5", print_hash_value);
    transparent_crc(g_1123.f0, "g_1123.f0", print_hash_value);
    transparent_crc(g_1146, "g_1146", print_hash_value);
    transparent_crc(g_1156.f0, "g_1156.f0", print_hash_value);
    transparent_crc(g_1156.f1, "g_1156.f1", print_hash_value);
    transparent_crc(g_1156.f2, "g_1156.f2", print_hash_value);
    transparent_crc(g_1156.f3, "g_1156.f3", print_hash_value);
    transparent_crc(g_1156.f4, "g_1156.f4", print_hash_value);
    transparent_crc(g_1156.f5, "g_1156.f5", print_hash_value);
    transparent_crc(g_1159, "g_1159", print_hash_value);
    transparent_crc(g_1161.f0, "g_1161.f0", print_hash_value);
    transparent_crc(g_1179, "g_1179", print_hash_value);
    transparent_crc(g_1228.f0, "g_1228.f0", print_hash_value);
    transparent_crc(g_1228.f1, "g_1228.f1", print_hash_value);
    transparent_crc(g_1228.f2, "g_1228.f2", print_hash_value);
    transparent_crc(g_1228.f3, "g_1228.f3", print_hash_value);
    transparent_crc(g_1228.f4, "g_1228.f4", print_hash_value);
    transparent_crc(g_1228.f5, "g_1228.f5", print_hash_value);
    transparent_crc(g_1229.f0, "g_1229.f0", print_hash_value);
    transparent_crc(g_1327.f0, "g_1327.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1496[i][j][k].f0, "g_1496[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1550, "g_1550", print_hash_value);
    transparent_crc(g_1555.f0, "g_1555.f0", print_hash_value);
    transparent_crc(g_1555.f1, "g_1555.f1", print_hash_value);
    transparent_crc(g_1555.f2, "g_1555.f2", print_hash_value);
    transparent_crc(g_1555.f3, "g_1555.f3", print_hash_value);
    transparent_crc(g_1555.f4, "g_1555.f4", print_hash_value);
    transparent_crc(g_1555.f5, "g_1555.f5", print_hash_value);
    transparent_crc(g_1562.f0, "g_1562.f0", print_hash_value);
    transparent_crc(g_1562.f1, "g_1562.f1", print_hash_value);
    transparent_crc(g_1562.f2, "g_1562.f2", print_hash_value);
    transparent_crc(g_1562.f3, "g_1562.f3", print_hash_value);
    transparent_crc(g_1562.f4, "g_1562.f4", print_hash_value);
    transparent_crc(g_1562.f5, "g_1562.f5", print_hash_value);
    transparent_crc(g_1572.f0, "g_1572.f0", print_hash_value);
    transparent_crc(g_1572.f1, "g_1572.f1", print_hash_value);
    transparent_crc(g_1572.f2, "g_1572.f2", print_hash_value);
    transparent_crc(g_1572.f3, "g_1572.f3", print_hash_value);
    transparent_crc(g_1572.f4, "g_1572.f4", print_hash_value);
    transparent_crc(g_1572.f5, "g_1572.f5", print_hash_value);
    transparent_crc(g_1573.f0, "g_1573.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1577[i].f0, "g_1577[i].f0", print_hash_value);
        transparent_crc(g_1577[i].f1, "g_1577[i].f1", print_hash_value);
        transparent_crc(g_1577[i].f2, "g_1577[i].f2", print_hash_value);
        transparent_crc(g_1577[i].f3, "g_1577[i].f3", print_hash_value);
        transparent_crc(g_1577[i].f4, "g_1577[i].f4", print_hash_value);
        transparent_crc(g_1577[i].f5, "g_1577[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1594.f0, "g_1594.f0", print_hash_value);
    transparent_crc(g_1649, "g_1649", print_hash_value);
    transparent_crc(g_1674.f0, "g_1674.f0", print_hash_value);
    transparent_crc(g_1726.f0, "g_1726.f0", print_hash_value);
    transparent_crc(g_1748.f2, "g_1748.f2", print_hash_value);
    transparent_crc(g_1750, "g_1750", print_hash_value);
    transparent_crc(g_1820, "g_1820", print_hash_value);
    transparent_crc(g_1831.f0, "g_1831.f0", print_hash_value);
    transparent_crc(g_1842.f0, "g_1842.f0", print_hash_value);
    transparent_crc(g_1919, "g_1919", print_hash_value);
    transparent_crc(g_1926, "g_1926", print_hash_value);
    transparent_crc(g_1939.f0, "g_1939.f0", print_hash_value);
    transparent_crc(g_1945.f0, "g_1945.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1948[i].f0, "g_1948[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1998.f0, "g_1998.f0", print_hash_value);
    transparent_crc(g_2054, "g_2054", print_hash_value);
    transparent_crc(g_2071.f2, "g_2071.f2", print_hash_value);
    transparent_crc(g_2072.f0, "g_2072.f0", print_hash_value);
    transparent_crc(g_2077, "g_2077", print_hash_value);
    transparent_crc(g_2081.f0, "g_2081.f0", print_hash_value);
    transparent_crc(g_2082.f0, "g_2082.f0", print_hash_value);
    transparent_crc(g_2205.f0, "g_2205.f0", print_hash_value);
    transparent_crc(g_2218.f0, "g_2218.f0", print_hash_value);
    transparent_crc(g_2330.f0, "g_2330.f0", print_hash_value);
    transparent_crc(g_2387.f0, "g_2387.f0", print_hash_value);
    transparent_crc(g_2408.f0, "g_2408.f0", print_hash_value);
    transparent_crc(g_2417, "g_2417", print_hash_value);
    transparent_crc(g_2444, "g_2444", print_hash_value);
    transparent_crc(g_2514.f0, "g_2514.f0", print_hash_value);
    transparent_crc(g_2514.f1, "g_2514.f1", print_hash_value);
    transparent_crc(g_2514.f2, "g_2514.f2", print_hash_value);
    transparent_crc(g_2514.f3, "g_2514.f3", print_hash_value);
    transparent_crc(g_2514.f4, "g_2514.f4", print_hash_value);
    transparent_crc(g_2514.f5, "g_2514.f5", print_hash_value);
    transparent_crc(g_2551.f0, "g_2551.f0", print_hash_value);
    transparent_crc(g_2556.f0, "g_2556.f0", print_hash_value);
    transparent_crc(g_2589.f0, "g_2589.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2612[i], "g_2612[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2628.f0, "g_2628.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_2633[i][j][k].f0, "g_2633[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2667.f0, "g_2667.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2710[i].f0, "g_2710[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2718.f0, "g_2718.f0", print_hash_value);
    transparent_crc(g_2736.f0, "g_2736.f0", print_hash_value);
    transparent_crc(g_2737.f0, "g_2737.f0", print_hash_value);
    transparent_crc(g_2741.f0, "g_2741.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2744[i].f0, "g_2744[i].f0", print_hash_value);
        transparent_crc(g_2744[i].f1, "g_2744[i].f1", print_hash_value);
        transparent_crc(g_2744[i].f2, "g_2744[i].f2", print_hash_value);
        transparent_crc(g_2744[i].f3, "g_2744[i].f3", print_hash_value);
        transparent_crc(g_2744[i].f4, "g_2744[i].f4", print_hash_value);
        transparent_crc(g_2744[i].f5, "g_2744[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2774.f0, "g_2774.f0", print_hash_value);
    transparent_crc(g_2855.f0, "g_2855.f0", print_hash_value);
    transparent_crc(g_2935.f0, "g_2935.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_3052[i][j][k].f0, "g_3052[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_3065.f0, "g_3065.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_3076[i].f0, "g_3076[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3213.f0, "g_3213.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_3240[i], "g_3240[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3252.f0, "g_3252.f0", print_hash_value);
    transparent_crc(g_3275, "g_3275", print_hash_value);
    transparent_crc(g_3276, "g_3276", print_hash_value);
    transparent_crc(g_3284.f0, "g_3284.f0", print_hash_value);
    transparent_crc(g_3284.f1, "g_3284.f1", print_hash_value);
    transparent_crc(g_3284.f2, "g_3284.f2", print_hash_value);
    transparent_crc(g_3284.f3, "g_3284.f3", print_hash_value);
    transparent_crc(g_3284.f4, "g_3284.f4", print_hash_value);
    transparent_crc(g_3284.f5, "g_3284.f5", print_hash_value);
    transparent_crc(g_3310.f0, "g_3310.f0", print_hash_value);
    transparent_crc(g_3310.f1, "g_3310.f1", print_hash_value);
    transparent_crc(g_3310.f2, "g_3310.f2", print_hash_value);
    transparent_crc(g_3310.f3, "g_3310.f3", print_hash_value);
    transparent_crc(g_3310.f4, "g_3310.f4", print_hash_value);
    transparent_crc(g_3310.f5, "g_3310.f5", print_hash_value);
    transparent_crc(g_3342.f0, "g_3342.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_3367[i].f0, "g_3367[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_3483[i], "g_3483[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3491.f0, "g_3491.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 970
   depth: 1, occurrence: 14
XXX total union variables: 52

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 61
breakdown:
   indirect level: 0, occurrence: 22
   indirect level: 1, occurrence: 11
   indirect level: 2, occurrence: 7
   indirect level: 3, occurrence: 13
   indirect level: 4, occurrence: 3
   indirect level: 5, occurrence: 5
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 15
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 27
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 16

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 318
   depth: 2, occurrence: 87
   depth: 3, occurrence: 9
   depth: 4, occurrence: 2
   depth: 5, occurrence: 5
   depth: 6, occurrence: 8
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 10, occurrence: 2
   depth: 12, occurrence: 2
   depth: 13, occurrence: 4
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 3
   depth: 19, occurrence: 5
   depth: 20, occurrence: 2
   depth: 21, occurrence: 2
   depth: 22, occurrence: 4
   depth: 23, occurrence: 4
   depth: 24, occurrence: 6
   depth: 25, occurrence: 3
   depth: 28, occurrence: 3
   depth: 31, occurrence: 3
   depth: 32, occurrence: 2
   depth: 34, occurrence: 2
   depth: 38, occurrence: 1
   depth: 39, occurrence: 2
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1
   depth: 42, occurrence: 2

XXX total number of pointers: 733

XXX times a variable address is taken: 1910
XXX times a pointer is dereferenced on RHS: 402
breakdown:
   depth: 1, occurrence: 271
   depth: 2, occurrence: 77
   depth: 3, occurrence: 37
   depth: 4, occurrence: 17
XXX times a pointer is dereferenced on LHS: 490
breakdown:
   depth: 1, occurrence: 359
   depth: 2, occurrence: 95
   depth: 3, occurrence: 26
   depth: 4, occurrence: 10
XXX times a pointer is compared with null: 66
XXX times a pointer is compared with address of another variable: 23
XXX times a pointer is compared with another pointer: 27
XXX times a pointer is qualified to be dereferenced: 11322

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1838
   level: 2, occurrence: 601
   level: 3, occurrence: 422
   level: 4, occurrence: 175
   level: 5, occurrence: 78
XXX number of pointers point to pointers: 407
XXX number of pointers point to scalars: 280
XXX number of pointers point to structs: 10
XXX percent of pointers has null in alias set: 32.6
XXX average alias set size: 1.65

XXX times a non-volatile is read: 2784
XXX times a non-volatile is write: 1567
XXX times a volatile is read: 230
XXX    times read thru a pointer: 101
XXX times a volatile is write: 63
XXX    times written thru a pointer: 31
XXX times a volatile is available for access: 8.52e+03
XXX percentage of non-volatile access: 93.7

XXX forward jumps: 3
XXX backward jumps: 13

XXX stmts: 324
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 40
   depth: 1, occurrence: 53
   depth: 2, occurrence: 56
   depth: 3, occurrence: 47
   depth: 4, occurrence: 46
   depth: 5, occurrence: 82

XXX percentage a fresh-made variable is used: 19
XXX percentage an existing variable is used: 81
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
XXX total OOB instances added: 0
********************* end of statistics **********************/


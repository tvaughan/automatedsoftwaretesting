/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3013334825
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   const int32_t  f0;
   const volatile uint64_t  f1;
};
#pragma pack(pop)

union U1 {
   uint16_t  f0;
   const uint16_t  f1;
   volatile int8_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2[1] = {0x328F6C74L};
static volatile int32_t g_3 = 0x276B87D0L;/* VOLATILE GLOBAL g_3 */
static volatile int32_t g_4 = 0x166941FCL;/* VOLATILE GLOBAL g_4 */
static volatile int32_t g_5 = 0x7B5BF2D3L;/* VOLATILE GLOBAL g_5 */
static int32_t g_6 = (-1L);
static volatile int32_t g_9[4] = {1L,1L,1L,1L};
static volatile int32_t g_10 = 0x76EEA530L;/* VOLATILE GLOBAL g_10 */
static volatile int32_t g_11 = 0xCE3051EBL;/* VOLATILE GLOBAL g_11 */
static int32_t g_12 = (-1L);
static volatile union U1 g_15[1][4] = {{{65531UL},{65531UL},{65531UL},{65531UL}}};
static int32_t g_21 = 0L;
static int32_t * volatile g_20 = &g_21;/* VOLATILE GLOBAL g_20 */
static volatile int8_t g_24 = 1L;/* VOLATILE GLOBAL g_24 */
static struct S0 g_28 = {0L,0x5FAFE44FA3FDA37BLL};/* VOLATILE GLOBAL g_28 */


/* --- FORWARD DECLARATIONS --- */
static struct S0  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_12 g_15 g_20 g_28
 * writes: g_6 g_12 g_21
 */
static struct S0  func_1(void)
{ /* block id: 0 */
    uint8_t l_25 = 0x70L;
    for (g_6 = 0; (g_6 >= (-18)); g_6 = safe_sub_func_int32_t_s_s(g_6, 8))
    { /* block id: 3 */
        uint64_t l_19 = 0UL;
        for (g_12 = 0; (g_12 >= (-22)); g_12 = safe_sub_func_int8_t_s_s(g_12, 8))
        { /* block id: 6 */
            int32_t l_16[7];
            int32_t *l_22 = &g_21;
            int32_t *l_23[6];
            int i;
            for (i = 0; i < 7; i++)
                l_16[i] = (-9L);
            for (i = 0; i < 6; i++)
                l_23[i] = &g_21;
            (*g_20) = (g_15[0][3] , (l_16[4] < (safe_lshift_func_uint32_t_u_s(l_19, 23))));
            ++l_25;
        }
    }
    return g_28;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_9[i], "g_9[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_10, "g_10", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_15[i][j].f0, "g_15[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_24, "g_24", print_hash_value);
    transparent_crc(g_28.f0, "g_28.f0", print_hash_value);
    transparent_crc(g_28.f1, "g_28.f1", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 5
   depth: 1, occurrence: 1
XXX total union variables: 1

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 4
breakdown:
   depth: 1, occurrence: 4
   depth: 2, occurrence: 2
   depth: 4, occurrence: 1

XXX total number of pointers: 3

XXX times a variable address is taken: 3
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 1
breakdown:
   depth: 1, occurrence: 1
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 32

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 3
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 0
XXX average alias set size: 1

XXX times a non-volatile is read: 5
XXX times a non-volatile is write: 4
XXX times a volatile is read: 1
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 13
XXX percentage of non-volatile access: 81.8

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 5
XXX max block depth: 2
breakdown:
   depth: 0, occurrence: 2
   depth: 1, occurrence: 1
   depth: 2, occurrence: 2

XXX percentage a fresh-made variable is used: 54.5
XXX percentage an existing variable is used: 45.5
XXX total OOB instances added: 0
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3338831949
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2[9][10] = {{(-7L),0xC90D2972L,(-5L),0xCB50BF34L,0L,(-7L),1L,1L,1L,1L},{(-7L),1L,1L,1L,1L,(-7L),0L,0xCB50BF34L,(-5L),0xC90D2972L},{(-7L),0L,0xCB50BF34L,(-5L),0xC90D2972L,(-7L),0xDA0E60F9L,0L,(-10L),(-6L)},{(-7L),0xDA0E60F9L,0L,(-10L),(-6L),(-7L),(-6L),(-10L),0L,0xDA0E60F9L},{(-7L),(-6L),(-10L),0L,0xDA0E60F9L,(-7L),(-1L),(-6L),0xDA0E60F9L,0x896E6A37L},{0L,(-1L),(-6L),0xDA0E60F9L,0x896E6A37L,0L,0x97A61EEFL,1L,1L,0x97A61EEFL},{0L,0x97A61EEFL,1L,1L,0x97A61EEFL,0L,0x896E6A37L,0xDA0E60F9L,(-6L),(-1L)},{0L,0x896E6A37L,0xDA0E60F9L,(-6L),(-1L),0L,1L,0xC90D2972L,0L,(-1L)},{0L,1L,0xC90D2972L,0L,(-1L),0L,(-1L),0L,0xC90D2972L,1L}};
static int32_t g_3 = 0x3D188047L;
static uint16_t g_37[5] = {0xCB2DL,0xCB2DL,0xCB2DL,0xCB2DL,0xCB2DL};
static int32_t g_40 = 0x0FE1F230L;
static int32_t g_64[7] = {0L,0L,0L,0L,0L,0L,0L};
static int64_t g_89 = 0xBDDF9C202D2A5DBDLL;
static uint32_t g_93 = 0x79F61339L;
static uint32_t g_95 = 0xEE702D15L;
static volatile int32_t **g_109 = (void*)0;
static int32_t **g_112 = (void*)0;
static int32_t ***g_111 = &g_112;
static int32_t g_122 = 0x800AB31FL;
static int32_t g_127 = (-4L);
static uint16_t g_130 = 0UL;
static uint16_t g_176 = 0UL;
static int8_t g_183 = 0x9BL;
static int16_t g_184 = 0L;
static uint32_t g_185 = 0x997C5296L;
static uint8_t g_236 = 0x32L;
static uint8_t *g_235 = &g_236;
static uint8_t *g_239 = &g_236;
static uint16_t *g_277 = &g_176;
static uint16_t **g_276[2][8] = {{&g_277,&g_277,&g_277,&g_277,&g_277,&g_277,&g_277,&g_277},{&g_277,&g_277,&g_277,&g_277,&g_277,&g_277,&g_277,&g_277}};
static int16_t g_292 = 2L;
static int8_t *g_307 = &g_183;
static int8_t *g_308 = &g_183;
static uint32_t g_345[3] = {0UL,0UL,0UL};
static uint64_t g_347 = 0x4363F4B67C917898LL;
static int64_t *g_361 = &g_89;
static int64_t **g_360 = &g_361;
static uint64_t g_381[3][2] = {{18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL}};
static int64_t g_448 = 1L;
static int64_t g_497[8][9][3] = {{{0xFCA12B8B8B2C49F1LL,0x0F970F96B302796ALL,0L},{0xB5CDEC1C79E02B04LL,0xFEBE960DF4136C63LL,4L},{0x3D02DF21E2A7AB1BLL,0xB5CDEC1C79E02B04LL,0x2AD4530274ED0901LL},{0xB5CDEC1C79E02B04LL,1L,0x01D7B5F51031C20FLL},{0xFCA12B8B8B2C49F1LL,0x149ED41A6293B7DFLL,0x01D7B5F51031C20FLL},{0x3DED776CB7329FADLL,0xF4561DA41F62F64ALL,0x2AD4530274ED0901LL},{0x4956DCCC2DD43AD8LL,(-4L),4L},{0xF4561DA41F62F64ALL,0xF4561DA41F62F64ALL,0L},{0xFEBE960DF4136C63LL,0x149ED41A6293B7DFLL,0xAFD5F46ADF3B0F00LL}},{{0xFEBE960DF4136C63LL,1L,0x58D211D77902963CLL},{0xF4561DA41F62F64ALL,0xB5CDEC1C79E02B04LL,0x8DF0CABE6C9771F1LL},{0x4956DCCC2DD43AD8LL,0xFEBE960DF4136C63LL,0x58D211D77902963CLL},{0x3DED776CB7329FADLL,0x0F970F96B302796ALL,0xAFD5F46ADF3B0F00LL},{0xFCA12B8B8B2C49F1LL,0x0F970F96B302796ALL,0L},{0xB5CDEC1C79E02B04LL,0xFEBE960DF4136C63LL,4L},{0x3D02DF21E2A7AB1BLL,0xB5CDEC1C79E02B04LL,0x2AD4530274ED0901LL},{0xB5CDEC1C79E02B04LL,1L,0x01D7B5F51031C20FLL},{0xFCA12B8B8B2C49F1LL,0x149ED41A6293B7DFLL,0x01D7B5F51031C20FLL}},{{0x3DED776CB7329FADLL,0xF4561DA41F62F64ALL,0x2AD4530274ED0901LL},{0x4956DCCC2DD43AD8LL,(-4L),4L},{0xF4561DA41F62F64ALL,0xF4561DA41F62F64ALL,0L},{0xFEBE960DF4136C63LL,0x149ED41A6293B7DFLL,0xAFD5F46ADF3B0F00LL},{0xFEBE960DF4136C63LL,1L,0x58D211D77902963CLL},{0xF4561DA41F62F64ALL,0xB5CDEC1C79E02B04LL,0x8DF0CABE6C9771F1LL},{0x4956DCCC2DD43AD8LL,0xFEBE960DF4136C63LL,0x58D211D77902963CLL},{0x3DED776CB7329FADLL,0x0F970F96B302796ALL,0xAFD5F46ADF3B0F00LL},{0xFCA12B8B8B2C49F1LL,0x0F970F96B302796ALL,0L}},{{0xB5CDEC1C79E02B04LL,0xFEBE960DF4136C63LL,4L},{0x3D02DF21E2A7AB1BLL,0xB5CDEC1C79E02B04LL,0x2AD4530274ED0901LL},{0xB5CDEC1C79E02B04LL,1L,0x01D7B5F51031C20FLL},{0xFCA12B8B8B2C49F1LL,0x149ED41A6293B7DFLL,0x01D7B5F51031C20FLL},{0x3DED776CB7329FADLL,0xF4561DA41F62F64ALL,0x2AD4530274ED0901LL},{0x4956DCCC2DD43AD8LL,(-4L),4L},{0xF4561DA41F62F64ALL,0xF4561DA41F62F64ALL,0L},{0xFEBE960DF4136C63LL,0x149ED41A6293B7DFLL,0xAFD5F46ADF3B0F00LL},{0xFEBE960DF4136C63LL,1L,0x58D211D77902963CLL}},{{0xF4561DA41F62F64ALL,0xB5CDEC1C79E02B04LL,0xB5CDEC1C79E02B04LL},{0L,1L,0x149ED41A6293B7DFLL},{0xBCD321F1388155D4LL,3L,7L},{(-1L),3L,0xFEBE960DF4136C63LL},{(-6L),1L,0xFCA12B8B8B2C49F1LL},{0x31099D7967F707F6LL,(-6L),0x3D02DF21E2A7AB1BLL},{(-6L),0x39A31A2C8E1978DELL,(-4L)},{(-1L),0x926203AC50C93FF7LL,(-4L)},{0xBCD321F1388155D4LL,0xB7A2FF9E693D86FALL,0x3D02DF21E2A7AB1BLL}},{{0L,(-9L),0xFCA12B8B8B2C49F1LL},{0xB7A2FF9E693D86FALL,0xB7A2FF9E693D86FALL,0xFEBE960DF4136C63LL},{1L,0x926203AC50C93FF7LL,7L},{1L,0x39A31A2C8E1978DELL,0x149ED41A6293B7DFLL},{0xB7A2FF9E693D86FALL,(-6L),0xB5CDEC1C79E02B04LL},{0L,1L,0x149ED41A6293B7DFLL},{0xBCD321F1388155D4LL,3L,7L},{(-1L),3L,0xFEBE960DF4136C63LL},{(-6L),1L,0xFCA12B8B8B2C49F1LL}},{{0x31099D7967F707F6LL,(-6L),0x3D02DF21E2A7AB1BLL},{(-6L),0x39A31A2C8E1978DELL,(-4L)},{(-1L),0x926203AC50C93FF7LL,(-4L)},{0xBCD321F1388155D4LL,0xB7A2FF9E693D86FALL,0x3D02DF21E2A7AB1BLL},{0L,(-9L),0xFCA12B8B8B2C49F1LL},{0xB7A2FF9E693D86FALL,0xB7A2FF9E693D86FALL,0xFEBE960DF4136C63LL},{1L,0x926203AC50C93FF7LL,7L},{1L,0x39A31A2C8E1978DELL,0x149ED41A6293B7DFLL},{0xB7A2FF9E693D86FALL,(-6L),0xB5CDEC1C79E02B04LL}},{{0L,1L,0x149ED41A6293B7DFLL},{0xBCD321F1388155D4LL,3L,7L},{(-1L),3L,0xFEBE960DF4136C63LL},{(-6L),1L,0xFCA12B8B8B2C49F1LL},{0x31099D7967F707F6LL,(-6L),0x3D02DF21E2A7AB1BLL},{(-6L),0x39A31A2C8E1978DELL,(-4L)},{(-1L),0x926203AC50C93FF7LL,(-4L)},{0xBCD321F1388155D4LL,0xB7A2FF9E693D86FALL,0x3D02DF21E2A7AB1BLL},{0L,(-9L),0xFCA12B8B8B2C49F1LL}}};
static const uint16_t *g_520 = &g_176;
static const uint16_t **g_519 = &g_520;
static uint32_t g_572 = 0x55A083DCL;
static int32_t *g_590 = &g_40;
static uint16_t * volatile * volatile * volatile *g_663 = (void*)0;
static int16_t *g_671 = &g_292;
static int16_t **g_670[6][5][4] = {{{&g_671,(void*)0,&g_671,&g_671},{(void*)0,&g_671,&g_671,(void*)0},{&g_671,&g_671,&g_671,&g_671},{&g_671,(void*)0,&g_671,&g_671},{&g_671,&g_671,&g_671,(void*)0}},{{&g_671,&g_671,&g_671,&g_671},{&g_671,(void*)0,&g_671,&g_671},{&g_671,&g_671,&g_671,&g_671},{&g_671,&g_671,&g_671,&g_671},{(void*)0,&g_671,&g_671,&g_671}},{{&g_671,&g_671,&g_671,&g_671},{&g_671,&g_671,&g_671,&g_671},{(void*)0,&g_671,&g_671,&g_671},{&g_671,&g_671,&g_671,&g_671},{&g_671,(void*)0,&g_671,&g_671}},{{(void*)0,&g_671,&g_671,(void*)0},{(void*)0,&g_671,&g_671,&g_671},{&g_671,(void*)0,&g_671,&g_671},{&g_671,&g_671,&g_671,(void*)0},{(void*)0,&g_671,&g_671,&g_671}},{{&g_671,(void*)0,&g_671,&g_671},{&g_671,&g_671,(void*)0,&g_671},{&g_671,&g_671,&g_671,&g_671},{(void*)0,&g_671,(void*)0,&g_671},{&g_671,&g_671,&g_671,&g_671}},{{&g_671,&g_671,&g_671,&g_671},{&g_671,&g_671,&g_671,&g_671},{&g_671,&g_671,&g_671,&g_671},{&g_671,(void*)0,(void*)0,&g_671},{(void*)0,&g_671,&g_671,&g_671}}};
static uint64_t **g_750 = (void*)0;
static uint8_t g_951 = 251UL;
static uint32_t g_970 = 0x618E9F49L;
static uint32_t *g_1020[5] = {&g_95,&g_95,&g_95,&g_95,&g_95};
static int64_t g_1049 = 0x3194992E34158B79LL;
static uint8_t g_1050 = 255UL;
static int8_t **g_1077 = &g_308;
static int32_t * const * const g_1226 = &g_590;
static int32_t * const * const *g_1225[6] = {(void*)0,&g_1226,(void*)0,(void*)0,&g_1226,(void*)0};
static int32_t * const * const **g_1224[10] = {&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4],&g_1225[4]};
static int32_t g_1295 = 0x5919F63AL;
static uint32_t g_1375[7][8][4] = {{{0x1079D437L,8UL,0xD2ED8EB2L,3UL},{0x4EDD74CEL,0x80C6532FL,4UL,0xABF3A065L},{18446744073709551613UL,0UL,0x1DE66101L,0xFDEE38BAL},{4UL,18446744073709551608UL,0xDB3AAD7CL,0UL},{8UL,18446744073709551611UL,0xABF3A065L,0x2FE4C788L},{0x68C1B700L,0xD2ED8EB2L,2UL,18446744073709551612UL},{18446744073709551611UL,0UL,0UL,0xB37605A5L},{2UL,18446744073709551613UL,18446744073709551611UL,0x7F1BC7A8L}},{{0xAA45DAC4L,0x80C6532FL,1UL,1UL},{1UL,1UL,0x27056F38L,0x6659D8CDL},{4UL,18446744073709551612UL,0x2FE4C788L,0x4EDD74CEL},{0x44E26842L,0xE1ACAE64L,3UL,0x2FE4C788L},{18446744073709551608UL,0xE1ACAE64L,0x1079D437L,0x4EDD74CEL},{0xE1ACAE64L,18446744073709551612UL,0UL,0x6659D8CDL},{2UL,1UL,2UL,1UL},{0x426A3ED1L,0x80C6532FL,0UL,0x7F1BC7A8L}},{{0UL,18446744073709551613UL,0x311DC4A2L,0xB37605A5L},{4UL,0UL,0x7D85D970L,18446744073709551612UL},{2UL,0xD2ED8EB2L,0x5527773CL,0x2FE4C788L},{18446744073709551612UL,18446744073709551611UL,0xE1ACAE64L,0UL},{0xD2ED8EB2L,18446744073709551608UL,0UL,0xFDEE38BAL},{18446744073709551615UL,0UL,18446744073709551615UL,0xABF3A065L},{0UL,0x80C6532FL,0x7F1BC7A8L,3UL},{0UL,8UL,1UL,0x80C6532FL}},{{4UL,0x426A3ED1L,1UL,0xAA45DAC4L},{0UL,18446744073709551615UL,0x7F1BC7A8L,0x2FE4C788L},{0UL,0x1079D437L,18446744073709551615UL,0x68C1B700L},{18446744073709551615UL,0x68C1B700L,0UL,4UL},{0xD2ED8EB2L,0UL,0xE1ACAE64L,0x5527773CL},{18446744073709551612UL,0x80C6532FL,0x5527773CL,4UL},{2UL,0x44E26842L,0x7D85D970L,0UL},{4UL,0xAA45DAC4L,0x311DC4A2L,18446744073709551608UL}},{{0UL,2UL,0UL,0x2FE4C788L},{0x426A3ED1L,2UL,0xB37605A5L,2UL},{0xFDEE38BAL,18446744073709551615UL,18446744073709551611UL,1UL},{0x6659D8CDL,18446744073709551612UL,0UL,0x7D85D970L},{0xD2ED8EB2L,0x7F1BC7A8L,0xDB3AAD7CL,0x7D85D970L},{0xD6CC154FL,18446744073709551612UL,8UL,1UL},{4UL,18446744073709551615UL,2UL,2UL},{1UL,0xB37605A5L,0x27056F38L,8UL}},{{18446744073709551611UL,0xFDEE38BAL,4UL,0xD2ED8EB2L},{0xB37605A5L,18446744073709551611UL,18446744073709551611UL,0UL},{4UL,0xD6CC154FL,0xFDEE38BAL,0x2FE4C788L},{0x1079D437L,0x7F1BC7A8L,0x311DC4A2L,1UL},{0UL,1UL,0UL,4UL},{4UL,0x1079D437L,18446744073709551613UL,0x1079D437L},{0xF6E8CEE5L,0UL,0x2FE4C788L,8UL},{18446744073709551615UL,1UL,0x80C6532FL,18446744073709551611UL}},{{0UL,2UL,18446744073709551611UL,0x7F1BC7A8L},{0UL,0UL,0x80C6532FL,0xDB3AAD7CL},{18446744073709551615UL,0x7F1BC7A8L,0x2FE4C788L,0x311DC4A2L},{0xF6E8CEE5L,18446744073709551611UL,18446744073709551613UL,0xABF3A065L},{4UL,0xD2ED8EB2L,0UL,0xE1ACAE64L},{0UL,4UL,0x311DC4A2L,8UL},{0x1079D437L,0x80C6532FL,0xFDEE38BAL,2UL},{4UL,0xE1ACAE64L,18446744073709551611UL,0x5527773CL}}};
static int32_t ****g_1545[1] = {&g_111};
static int16_t g_1548 = 0x951FL;
static uint16_t g_1621 = 0UL;
static uint16_t * const g_1620 = &g_1621;
static uint16_t * const *g_1619[10] = {&g_1620,&g_1620,&g_1620,&g_1620,&g_1620,&g_1620,&g_1620,&g_1620,&g_1620,&g_1620};
static uint16_t * const **g_1618[7][1][2] = {{{(void*)0,&g_1619[8]}},{{(void*)0,&g_1619[8]}},{{(void*)0,&g_1619[8]}},{{(void*)0,&g_1619[8]}},{{(void*)0,&g_1619[8]}},{{(void*)0,&g_1619[8]}},{{(void*)0,&g_1619[8]}}};
static uint16_t * const ***g_1617 = &g_1618[4][0][1];
static int8_t g_1648 = 0x63L;
static int8_t g_1649 = (-1L);
static int8_t g_1650 = 0x4FL;
static int8_t g_1651[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static int8_t g_1652 = 1L;
static int8_t g_1653 = 2L;
static int8_t g_1654 = 0x97L;
static int8_t g_1655 = 0L;
static int8_t g_1656[1] = {0x22L};
static int8_t g_1657 = 0xD1L;
static int8_t g_1658[8][7][4] = {{{0xD1L,(-2L),0xB0L,(-8L)},{0xDEL,(-1L),1L,(-1L)},{0L,(-1L),0x3CL,(-1L)},{1L,(-1L),0L,(-8L)},{0x89L,(-2L),0xDEL,0L},{(-1L),0x32L,(-5L),0x32L},{0xDEL,0x8DL,0xA0L,0xB0L}},{{0x04L,(-1L),(-2L),1L},{0L,3L,0L,0x3CL},{0L,(-8L),(-2L),0L},{0x04L,0x3CL,0xA0L,0xDEL},{0xDEL,0xF4L,(-5L),(-5L)},{(-1L),(-1L),0xDEL,0xA0L},{0x89L,0x35L,0L,(-2L)}},{{1L,0xDEL,0x3CL,0L},{0L,0xDEL,1L,(-2L)},{0xDEL,0x35L,0xB0L,0xA0L},{(-2L),0xDEL,0L,0xF4L},{(-1L),(-1L),(-1L),0L},{0xB0L,0x89L,1L,(-1L)},{0x3CL,1L,0x8DL,0x89L}},{{0L,0L,0x8DL,0x35L},{0x3CL,0xDEL,1L,3L},{0xB0L,0xD1L,(-1L),0L},{(-1L),0L,0L,(-1L)},{(-2L),0L,3L,1L},{0L,0x85L,0x35L,0x8DL},{(-8L),0xDEL,0x89L,0x8DL}},{{(-5L),0x85L,(-1L),1L},{1L,0L,0L,(-1L)},{0xDEL,0L,0xF4L,0L},{0L,0xD1L,(-1L),3L},{0x32L,0xDEL,0L,0x35L},{0xA0L,0L,(-1L),0x89L},{0xA0L,1L,0L,(-1L)}},{{0x32L,0x89L,(-1L),0L},{0L,(-1L),0xF4L,0xF4L},{0xDEL,0xDEL,0L,(-1L)},{1L,0x04L,(-1L),0L},{(-5L),0L,0x89L,(-1L)},{(-8L),0L,0x35L,0L},{0L,0x04L,3L,(-1L)}},{{(-2L),0xDEL,0L,0xF4L},{(-1L),(-1L),(-1L),0L},{0xB0L,0x89L,1L,(-1L)},{0x3CL,1L,0x8DL,0x89L},{0L,0L,0x8DL,0x35L},{0x3CL,0xDEL,1L,3L},{0xB0L,0xD1L,(-1L),0L}},{{(-1L),0L,0L,(-1L)},{(-2L),0L,3L,1L},{0L,0x85L,0x35L,0x8DL},{(-8L),0xDEL,0x89L,0x8DL},{(-5L),0x85L,(-1L),1L},{1L,0L,0L,(-1L)},{0xDEL,0L,0xF4L,0L}}};
static int8_t g_1659 = 0xD8L;
static int8_t g_1660 = 0x65L;
static volatile int8_t g_1706 = 0xC2L;/* VOLATILE GLOBAL g_1706 */
static uint32_t *g_1708 = &g_345[2];
static uint32_t **g_1707 = &g_1708;
static int32_t ** volatile g_1712 = &g_590;/* VOLATILE GLOBAL g_1712 */
static int32_t g_1724 = (-4L);
static int64_t * const **g_1758 = (void*)0;
static volatile uint64_t * volatile ** volatile ** volatile g_1780 = (void*)0;/* VOLATILE GLOBAL g_1780 */
static volatile uint64_t ** volatile * volatile * volatile *g_1781[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile int32_t g_1831 = 1L;/* VOLATILE GLOBAL g_1831 */
static const volatile uint16_t g_1906[3] = {0x80F0L,0x80F0L,0x80F0L};
static const volatile uint16_t * volatile g_1905 = &g_1906[2];/* VOLATILE GLOBAL g_1905 */
static int32_t ** const  volatile g_1910 = &g_590;/* VOLATILE GLOBAL g_1910 */
static volatile uint8_t g_1928 = 0xA5L;/* VOLATILE GLOBAL g_1928 */
static volatile uint8_t * const  volatile g_1927 = &g_1928;/* VOLATILE GLOBAL g_1927 */
static volatile uint8_t * const  volatile *g_1926 = &g_1927;
static volatile uint8_t * const  volatile **g_1925[6][8] = {{&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926},{&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926},{&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926},{&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926},{&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926},{&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926,&g_1926}};
static int16_t g_1940 = 0x9C9CL;
static uint64_t * volatile **g_1982 = (void*)0;
static uint64_t * volatile ***g_1981 = &g_1982;
static uint32_t g_2009[1] = {0x794F344AL};
static volatile int32_t g_2175 = 0x8B1CFCCFL;/* VOLATILE GLOBAL g_2175 */
static int32_t g_2219[4][2] = {{1L,1L},{1L,1L},{1L,1L},{1L,1L}};
static uint64_t *g_2233[3] = {&g_381[1][0],&g_381[1][0],&g_381[1][0]};
static volatile uint64_t *g_2271 = (void*)0;
static volatile uint64_t **g_2270 = &g_2271;
static volatile uint64_t ** volatile * volatile g_2269[2] = {&g_2270,&g_2270};
static volatile uint64_t ** volatile * volatile * volatile g_2268 = &g_2269[1];/* VOLATILE GLOBAL g_2268 */
static int32_t ** volatile g_2332 = &g_590;/* VOLATILE GLOBAL g_2332 */
static uint32_t ** volatile *g_2459 = (void*)0;
static uint32_t ** volatile ** volatile g_2458 = &g_2459;/* VOLATILE GLOBAL g_2458 */
static uint32_t ** volatile ** volatile * volatile g_2457 = &g_2458;/* VOLATILE GLOBAL g_2457 */
static const int16_t g_2544 = 0xB8EFL;
static const int16_t *g_2543 = &g_2544;
static uint32_t ***g_2634 = &g_1707;
static int8_t g_2661 = (-1L);


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t * func_6(uint32_t  p_7);
static uint8_t  func_10(int32_t * p_11, int32_t * p_12);
static int32_t * func_13(int64_t  p_14);
static uint16_t  func_21(int8_t  p_22, int32_t  p_23, const uint32_t  p_24, int32_t * p_25);
static uint8_t  func_29(int32_t  p_30, int32_t  p_31, int32_t  p_32, int32_t * p_33);
static int32_t  func_41(uint8_t  p_42, int32_t * p_43);
static int32_t * func_44(int32_t  p_45);
static uint32_t  func_50(uint32_t  p_51, uint32_t  p_52);
static uint32_t  func_53(int32_t * p_54, int32_t * const  p_55);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_2332 g_590 g_40 g_2634 g_1707 g_1708 g_345 g_235 g_236 g_361 g_89 g_1226 g_1649 g_1621 g_1651
 * writes: g_3 g_590 g_345 g_122 g_40 g_1649
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint16_t l_2670 = 1UL;
    int32_t l_2674 = 1L;
    for (g_3 = 0; (g_3 < (-4)); --g_3)
    { /* block id: 3 */
        uint32_t l_8 = 0x57F6CC9CL;
        int32_t l_9[9];
        int32_t **l_2664 = &g_590;
        uint16_t l_2693[7] = {8UL,8UL,8UL,8UL,8UL,8UL,8UL};
        int16_t ** const *l_2697 = &g_670[4][0][3];
        int16_t ** const **l_2696 = &l_2697;
        uint64_t *l_2698[6][7][2] = {{{(void*)0,(void*)0},{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]},{(void*)0,(void*)0},{(void*)0,&g_381[0][0]}},{{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]},{(void*)0,(void*)0},{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]}},{{&g_347,&g_381[0][0]},{(void*)0,(void*)0},{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]},{(void*)0,(void*)0}},{{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]},{(void*)0,(void*)0},{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]}},{{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]},{(void*)0,(void*)0},{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]}},{{(void*)0,(void*)0},{(void*)0,&g_381[0][0]},{&g_347,&g_381[2][0]},{&g_381[0][0],&g_381[2][0]},{&g_347,&g_381[0][0]},{(void*)0,(void*)0},{(void*)0,&g_381[0][0]}}};
        int16_t l_2699[8][8] = {{0L,0xBB4AL,0L,0L,0xBB4AL,0L,0L,0xBB4AL},{0xBB4AL,0L,0L,0xBB4AL,0L,0L,0xBB4AL,0L},{0xBB4AL,0xBB4AL,(-3L),0xBB4AL,0xBB4AL,(-3L),0xBB4AL,0xBB4AL},{0L,0xBB4AL,0L,0L,0xBB4AL,0L,0L,0xBB4AL},{0xBB4AL,0L,0L,0xBB4AL,0L,0L,0xBB4AL,0L},{0xBB4AL,0xBB4AL,(-3L),0xBB4AL,0xBB4AL,(-3L),0xBB4AL,0xBB4AL},{0L,0xBB4AL,0L,0L,0xBB4AL,0L,0L,0xBB4AL},{0xBB4AL,0L,0L,0xBB4AL,0L,0L,0L,(-3L)}};
        uint32_t l_2700 = 18446744073709551612UL;
        uint16_t l_2701[3];
        uint8_t l_2702 = 0x5DL;
        int32_t *l_2703 = (void*)0;
        int32_t *l_2704 = &g_122;
        uint8_t l_2705 = 0xD1L;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_9[i] = 0xB4956E74L;
        for (i = 0; i < 3; i++)
            l_2701[i] = 2UL;
        (*l_2664) = func_6((l_9[7] &= l_8));
        (**g_1226) = ((safe_div_func_uint32_t_u_u((safe_rshift_func_uint64_t_u_u((!l_2670), 43)), (safe_add_func_uint16_t_u_u((!(l_2674 = (**l_2664))), (safe_sub_func_int32_t_s_s(((safe_lshift_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u((++(***g_2634)), ((*l_2704) = (l_9[4] ^= (safe_rshift_func_uint64_t_u_u((safe_mod_func_uint32_t_u_u(l_2670, ((safe_add_func_int8_t_s_s(((((l_2700 = (safe_div_func_int16_t_s_s((((**l_2664) , ((0x3AFFDF95F3AE26F9LL != (l_2699[2][5] = (((l_2693[6] ^ (safe_mul_func_uint8_t_u_u((**l_2664), ((void*)0 == l_2696)))) && (**l_2664)) > (*g_235)))) , l_2670)) || (*g_361)), 65534UL))) & l_2670) < 0x72ACL) == l_2701[2]), l_2702)) && 18446744073709551614UL))), 47)))))) == (**l_2664)), 1UL)), 6)) , 0L), l_2670)))))) ^ l_2705);
    }
    for (g_1649 = 6; (g_1649 == (-22)); --g_1649)
    { /* block id: 1303 */
        return g_1621;
    }
    return g_1651[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_2332 g_590
 * writes:
 */
static int32_t * func_6(uint32_t  p_7)
{ /* block id: 5 */
    int8_t l_26[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
    uint16_t *l_36 = &g_37[2];
    int32_t *l_38 = (void*)0;
    int32_t *l_39 = &g_40;
    int16_t *l_1547[7];
    const int32_t l_1549 = 5L;
    int32_t *l_1723 = &g_1724;
    int64_t * const *l_1756 = &g_361;
    int64_t * const **l_1755 = &l_1756;
    int16_t l_1788 = 0xEE16L;
    int32_t l_1830 = 0L;
    int32_t l_1834[4] = {(-10L),(-10L),(-10L),(-10L)};
    uint8_t **l_1845[9][1];
    uint16_t ***l_1919 = (void*)0;
    uint64_t l_1964[5][10][5] = {{{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL}},{{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL}},{{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL}},{{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL}},{{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL},{0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL,0x83BDC4903DCDE8B6LL},{6UL,6UL,6UL,6UL,6UL}}};
    int32_t l_1984 = 0x85D03EE2L;
    int32_t *****l_1988 = (void*)0;
    int32_t l_2006 = 0xAB904DDEL;
    int32_t l_2026[9][10][2] = {{{(-5L),(-2L)},{0xD4417CE4L,4L},{0xD952307AL,0L},{0L,1L},{2L,(-2L)},{7L,0xE483C8E0L},{0xF4EB9112L,(-7L)},{0x871A436AL,0x070DCC28L},{0xDC1721C4L,0L},{0xE12ABCE1L,0xB5033AE5L}},{{0x6DBF4FF3L,0x9FE02A62L},{1L,0L},{0xE483C8E0L,2L},{1L,0x4BCD0AB7L},{0xDE328EA6L,0xD4417CE4L},{(-1L),0xFB31F908L},{0L,0xDE328EA6L},{4L,0xDFA0702BL},{0x182BFE1AL,0xD32A483CL},{(-7L),0x49043233L}},{{0x1C6973F5L,(-1L)},{0L,(-1L)},{0x1C6973F5L,0x49043233L},{(-7L),0xD32A483CL},{0x182BFE1AL,0xDFA0702BL},{4L,0xDE328EA6L},{0L,0xFB31F908L},{(-1L),0xD4417CE4L},{0xDE328EA6L,0x4BCD0AB7L},{1L,2L}},{{0xE483C8E0L,0L},{1L,0x9FE02A62L},{0x6DBF4FF3L,0xB5033AE5L},{0xE12ABCE1L,0L},{0xDC1721C4L,0x070DCC28L},{0x871A436AL,(-7L)},{0xF4EB9112L,0xE483C8E0L},{7L,(-2L)},{2L,1L},{0L,0L}},{{0xD952307AL,4L},{0xD4417CE4L,(-2L)},{(-5L),7L},{0L,(-5L)},{0xFB31F908L,1L},{0xFB31F908L,(-5L)},{0L,7L},{(-5L),(-2L)},{0xD4417CE4L,4L},{0xD952307AL,0L}},{{0L,1L},{2L,(-2L)},{7L,0xE483C8E0L},{0xF4EB9112L,(-7L)},{0x871A436AL,0x070DCC28L},{0xDC1721C4L,0L},{0xE12ABCE1L,0xB5033AE5L},{0x6DBF4FF3L,0x9FE02A62L},{1L,0L},{0L,0xB5033AE5L}},{{0x282A5C1BL,(-7L)},{(-10L),0xF4EB9112L},{0xDE328EA6L,0L},{0x4BCD0AB7L,(-10L)},{(-1L),2L},{0x2A4BA4E2L,0x1C6973F5L},{1L,0L},{0L,0L},{0L,0L},{0L,0L}},{{1L,0x1C6973F5L},{0x2A4BA4E2L,2L},{(-1L),(-10L)},{0x4BCD0AB7L,0L},{0xDE328EA6L,0xF4EB9112L},{(-10L),(-7L)},{0x282A5C1BL,0xB5033AE5L},{0L,0x4BCD0AB7L},{(-2L),0L},{0x9EE224E6L,4L}},{{4L,2L},{0xDFA0702BL,(-1L)},{7L,1L},{0xE12ABCE1L,0L},{0xE483C8E0L,0xD952307AL},{0xB5033AE5L,(-2L)},{0x871A436AL,0x871A436AL},{0x9FE02A62L,0xC84E006EL},{0xF4EB9112L,(-1L)},{0x49043233L,0xE483C8E0L}}};
    uint64_t l_2033 = 18446744073709551613UL;
    uint64_t *l_2035 = (void*)0;
    uint32_t **l_2043 = &g_1708;
    uint16_t l_2089 = 65535UL;
    uint32_t l_2091 = 4294967286UL;
    uint16_t ** const *l_2126 = &g_276[1][0];
    uint16_t ** const **l_2125[8] = {&l_2126,&l_2126,&l_2126,&l_2126,&l_2126,&l_2126,&l_2126,&l_2126};
    uint16_t ** const ***l_2124 = &l_2125[4];
    int16_t l_2191[7][2] = {{4L,4L},{0x2157L,4L},{4L,0x2157L},{4L,4L},{0x2157L,4L},{4L,0x2157L},{4L,4L}};
    int32_t l_2287 = 0x9FE29F19L;
    int32_t *l_2297 = &g_40;
    uint64_t ***l_2325[8][3][8] = {{{&g_750,(void*)0,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750},{&g_750,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750},{&g_750,&g_750,(void*)0,&g_750,&g_750,(void*)0,&g_750,&g_750}},{{(void*)0,&g_750,&g_750,(void*)0,&g_750,&g_750,(void*)0,(void*)0},{&g_750,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750},{(void*)0,&g_750,(void*)0,&g_750,&g_750,(void*)0,(void*)0,&g_750}},{{&g_750,&g_750,&g_750,&g_750,&g_750,(void*)0,&g_750,&g_750},{&g_750,&g_750,(void*)0,&g_750,(void*)0,&g_750,&g_750,(void*)0},{(void*)0,&g_750,&g_750,(void*)0,&g_750,&g_750,(void*)0,&g_750}},{{&g_750,(void*)0,(void*)0,(void*)0,(void*)0,&g_750,(void*)0,&g_750},{&g_750,(void*)0,&g_750,&g_750,(void*)0,&g_750,&g_750,&g_750},{(void*)0,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750,(void*)0}},{{&g_750,&g_750,(void*)0,&g_750,(void*)0,(void*)0,&g_750,&g_750},{&g_750,&g_750,&g_750,(void*)0,&g_750,&g_750,&g_750,&g_750},{&g_750,(void*)0,(void*)0,&g_750,(void*)0,&g_750,&g_750,&g_750}},{{&g_750,&g_750,&g_750,&g_750,(void*)0,&g_750,(void*)0,(void*)0},{&g_750,&g_750,(void*)0,(void*)0,&g_750,&g_750,(void*)0,(void*)0},{(void*)0,&g_750,&g_750,(void*)0,&g_750,&g_750,&g_750,&g_750}},{{&g_750,(void*)0,&g_750,(void*)0,&g_750,(void*)0,&g_750,&g_750},{(void*)0,&g_750,(void*)0,&g_750,&g_750,&g_750,&g_750,&g_750},{&g_750,&g_750,&g_750,(void*)0,&g_750,(void*)0,&g_750,(void*)0}},{{(void*)0,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750,&g_750},{&g_750,&g_750,&g_750,&g_750,&g_750,(void*)0,&g_750,&g_750},{(void*)0,&g_750,(void*)0,&g_750,&g_750,&g_750,&g_750,&g_750}}};
    const int32_t *l_2382[7];
    const int32_t **l_2381 = &l_2382[2];
    const int32_t ***l_2380 = &l_2381;
    int8_t l_2486[8];
    int64_t l_2529 = 0xA8C57C3C3130D0C7LL;
    int16_t * const ** const l_2561 = (void*)0;
    uint32_t l_2578 = 0xA682E6C3L;
    uint8_t l_2603 = 0x42L;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1547[i] = &g_1548;
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 1; j++)
            l_1845[i][j] = &g_235;
    }
    for (i = 0; i < 7; i++)
        l_2382[i] = &g_64[1];
    for (i = 0; i < 8; i++)
        l_2486[i] = 0x0AL;
    return (*g_2332);
}


/* ------------------------------------------ */
/* 
 * reads : g_671 g_292 g_184 g_590 g_360 g_361 g_89 g_239 g_277 g_235 g_236 g_448 g_40 g_1648 g_347 g_1706 g_1707 g_64 g_183 g_572 g_519 g_520 g_176 g_122 g_663 g_130 g_127 g_381 g_497 g_750 g_37 g_93 g_1712
 * writes: g_184 g_40 g_236 g_176 g_1050 g_185 g_183 g_1617 g_292 g_448 g_1295 g_345 g_127 g_1707 g_89 g_572 g_64 g_670 g_130 g_381 g_122 g_347 g_93 g_671 g_37 g_590
 */
static uint8_t  func_10(int32_t * p_11, int32_t * p_12)
{ /* block id: 822 */
    int8_t l_1578 = 0x2FL;
    int16_t l_1589 = 0xC3B9L;
    int32_t l_1590 = 0x06CF2E14L;
    int16_t *l_1595 = &g_184;
    uint8_t *l_1608[10][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
    int64_t l_1609 = 0xE974A014239584E4LL;
    int32_t l_1610 = 0xED68B69AL;
    int8_t *l_1612 = &l_1578;
    int8_t *l_1613 = (void*)0;
    int8_t *l_1614 = &g_183;
    uint16_t * const ***l_1615 = (void*)0;
    uint16_t * const ****l_1616[4][4] = {{&l_1615,&l_1615,(void*)0,&l_1615},{&l_1615,&l_1615,&l_1615,&l_1615},{&l_1615,&l_1615,&l_1615,&l_1615},{&l_1615,&l_1615,(void*)0,&l_1615}};
    uint16_t ***l_1623[5][2][8] = {{{(void*)0,(void*)0,&g_276[0][4],&g_276[0][4],(void*)0,(void*)0,&g_276[0][4],&g_276[0][4]},{&g_276[0][6],(void*)0,(void*)0,&g_276[0][4],&g_276[0][7],(void*)0,&g_276[0][4],&g_276[0][4]}},{{(void*)0,(void*)0,&g_276[0][4],&g_276[0][4],(void*)0,(void*)0,&g_276[0][4],&g_276[0][4]},{&g_276[0][6],(void*)0,(void*)0,&g_276[0][4],&g_276[0][7],(void*)0,&g_276[0][4],&g_276[0][4]}},{{(void*)0,(void*)0,&g_276[0][4],&g_276[0][4],(void*)0,(void*)0,&g_276[0][4],&g_276[0][4]},{&g_276[0][6],(void*)0,(void*)0,&g_276[0][4],&g_276[0][7],(void*)0,&g_276[0][4],&g_276[0][4]}},{{(void*)0,(void*)0,&g_276[0][4],&g_276[0][4],(void*)0,(void*)0,&g_276[0][4],&g_276[0][4]},{&g_276[0][6],(void*)0,(void*)0,&g_276[0][4],&g_276[0][7],(void*)0,&g_276[0][4],&g_276[0][4]}},{{(void*)0,(void*)0,&g_276[0][4],&g_276[0][4],(void*)0,(void*)0,&g_276[0][4],&g_276[0][4]},{&g_276[0][6],(void*)0,(void*)0,&g_276[0][4],&g_276[0][7],(void*)0,&g_276[0][4],&g_276[0][4]}}};
    uint16_t ****l_1622 = &l_1623[2][0][0];
    uint32_t *l_1672 = &g_345[0];
    int i, j, k;
    l_1610 = (safe_unary_minus_func_int64_t_s((safe_add_func_int8_t_s_s(((l_1578 , (safe_mul_func_uint32_t_u_u((g_185 = ((safe_sub_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((safe_div_func_int32_t_s_s((safe_sub_func_uint32_t_u_u(1UL, ((-1L) ^ (l_1590 = l_1589)))), ((g_1050 = (safe_mod_func_int32_t_s_s((safe_sub_func_int16_t_s_s(((*l_1595) ^= (*g_671)), (((*g_277) = ((l_1589 , ((((safe_mod_func_int32_t_s_s(((safe_mod_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((*g_239) = ((safe_div_func_uint16_t_u_u(((((safe_lshift_func_int32_t_s_u((safe_mod_func_int32_t_s_s(((*g_590) = l_1589), l_1589)), 31)) && ((4UL != l_1578) > (**g_360))) , (void*)0) == (void*)0), l_1578)) > l_1589)), l_1589)), 0xE9L)) | l_1589), l_1578)) || l_1578) , (void*)0) == l_1595)) >= l_1578)) , l_1589))), l_1589))) ^ 0xFFL))), 0x550C15BEL)), (-9L))) == 0xC82EF17018692B89LL)), 4294967295UL))) , l_1590), l_1609))));
    l_1610 |= (l_1589 , (((*l_1614) = ((*l_1612) = (!l_1609))) < (*g_235)));
    if ((((g_1617 = l_1615) == (l_1622 = l_1622)) < ((((safe_div_func_int16_t_s_s(((*g_671) = (safe_lshift_func_uint8_t_u_u(((*g_235) = ((l_1578 ^ l_1589) > l_1609)), 3))), l_1590)) ^ (safe_sub_func_int16_t_s_s((safe_add_func_int64_t_s_s(l_1578, ((l_1589 ^ (l_1610 = (l_1578 | 0x17DFC3A508865AADLL))) & 1L))), 1UL))) != l_1589) >= (-5L))))
    { /* block id: 839 */
        uint32_t l_1639 = 4294967286UL;
        uint64_t ***l_1668 = &g_750;
        uint64_t ****l_1667[8][7] = {{&l_1668,(void*)0,&l_1668,&l_1668,&l_1668,&l_1668,&l_1668},{&l_1668,&l_1668,&l_1668,&l_1668,(void*)0,&l_1668,(void*)0},{&l_1668,(void*)0,&l_1668,&l_1668,&l_1668,&l_1668,(void*)0},{(void*)0,&l_1668,&l_1668,&l_1668,&l_1668,(void*)0,(void*)0},{&l_1668,&l_1668,&l_1668,&l_1668,&l_1668,(void*)0,&l_1668},{&l_1668,&l_1668,&l_1668,&l_1668,&l_1668,&l_1668,&l_1668},{&l_1668,&l_1668,&l_1668,&l_1668,(void*)0,(void*)0,&l_1668},{&l_1668,&l_1668,&l_1668,&l_1668,&l_1668,&l_1668,&l_1668}};
        int i, j;
        for (g_448 = 0; (g_448 != 13); g_448++)
        { /* block id: 842 */
            uint64_t l_1640 = 18446744073709551608UL;
            int8_t * const l_1647[9][7] = {{&g_1648,&g_1657,&g_1648,&g_1659,&g_1652,&g_1652,&g_1659},{&g_1655,&g_1651[0],&g_1655,&g_1653,&g_1658[7][3][0],&g_1658[7][3][0],&g_1653},{&g_1648,&g_1657,&g_1648,&g_1659,&g_1652,&g_1652,&g_1659},{&g_1655,&g_1651[0],&g_1655,&g_1653,&g_1658[7][3][0],&g_1658[7][3][0],&g_1653},{&g_1648,&g_1657,&g_1648,&g_1659,&g_1652,&g_1652,&g_1659},{&g_1655,&g_1651[0],&g_1655,&g_1653,&g_1658[7][3][0],&g_1658[7][3][0],&g_1653},{&g_1648,&g_1657,&g_1648,&g_1659,&g_1652,&g_1652,&g_1659},{&g_1655,&g_1651[0],&g_1655,&g_1653,&g_1658[7][3][0],&g_1658[7][3][0],&g_1653},{&g_1648,&g_1657,&g_1648,&g_1659,&g_1652,&g_1652,&g_1659}};
            int8_t * const *l_1646 = &l_1647[0][2];
            int8_t * const **l_1645 = &l_1646;
            uint16_t ****l_1661 = &l_1623[2][0][0];
            int32_t l_1662 = (-1L);
            int32_t *l_1663 = (void*)0;
            int32_t *l_1664 = (void*)0;
            int32_t *l_1665 = &g_1295;
            int32_t **l_1666 = &l_1665;
            uint64_t *****l_1669[6];
            int i, j;
            for (i = 0; i < 6; i++)
                l_1669[i] = &l_1667[2][0];
            (*l_1666) = func_13(((*p_12) >= (safe_lshift_func_int16_t_s_u(((l_1610 & (0L || (((*l_1665) = (safe_sub_func_uint16_t_u_u(65535UL, (!(((**g_360) , ((-4L) || ((l_1590 == ((l_1640 &= l_1639) > (safe_unary_minus_func_uint64_t_u(((((((safe_mul_func_int32_t_s_s((((*l_1645) = (((!0xF4L) & 0xD7BBL) , &l_1612)) == &g_308), 0x47BC2801L)) , l_1661) == l_1661) >= l_1589) && l_1662) >= 0x7BL))))) ^ l_1639))) , l_1610))))) && l_1639))) > l_1639), 7))));
            l_1667[2][6] = l_1667[3][2];
        }
    }
    else
    { /* block id: 849 */
        int32_t l_1680 = 0L;
        uint32_t **l_1710 = &g_1708;
        if ((safe_sub_func_int16_t_s_s(l_1609, (l_1672 != (void*)0))))
        { /* block id: 850 */
            uint8_t l_1677[10][5][5] = {{{1UL,0UL,0x7DL,0xB8L,0xCBL},{0x1FL,0x94L,0x1BL,0x94L,0x1FL},{0x1FL,1UL,246UL,0x5EL,0x94L},{1UL,1UL,5UL,246UL,0x1BL},{246UL,5UL,1UL,1UL,0x94L}},{{0x5EL,246UL,1UL,0x1FL,0x1FL},{0x94L,0x1BL,0x94L,0x1FL,0xCBL},{0xB8L,0x7DL,0UL,1UL,0x5EL},{0x1BL,0xCBL,9UL,246UL,0x52L},{1UL,0x4AL,0UL,0x5EL,1UL}},{{1UL,255UL,0x94L,0x94L,255UL},{0x52L,255UL,1UL,0xB8L,4UL},{5UL,0x4AL,1UL,0x1BL,0xA7L},{0UL,0xCBL,5UL,1UL,255UL},{5UL,0xCBL,0x1FL,0x94L,0x1BL}},{{0x23L,252UL,252UL,0x23L,0x1BL},{0x94L,0x1FL,0xCBL,0x5EL,0x52L},{255UL,0x5EL,255UL,0x7DL,1UL},{252UL,1UL,5UL,0x5EL,0xA7L},{0x4AL,0x1BL,9UL,0x23L,9UL}},{{4UL,4UL,9UL,0x94L,1UL},{246UL,0x7DL,5UL,255UL,0x23L},{0x1FL,1UL,255UL,252UL,246UL},{0x1BL,0x7DL,0xCBL,0x4AL,255UL},{0UL,4UL,252UL,4UL,0UL}},{{0UL,0x1BL,0x1FL,246UL,4UL},{0x1BL,1UL,0x5EL,0x1FL,252UL},{0x1FL,0x5EL,1UL,0x1BL,4UL},{246UL,0x1FL,0x1BL,0UL,0UL},{4UL,252UL,4UL,0UL,255UL}},{{0x4AL,0xCBL,0x7DL,0x1BL,246UL},{252UL,255UL,1UL,0x1FL,0x23L},{255UL,5UL,0x7DL,246UL,1UL},{0x94L,9UL,4UL,4UL,9UL},{0x23L,9UL,0x1BL,0x4AL,0xA7L}},{{0x5EL,5UL,1UL,252UL,1UL},{0x7DL,255UL,0x5EL,255UL,0x52L},{0x5EL,0xCBL,0x1FL,0x94L,0x1BL},{0x23L,252UL,252UL,0x23L,0x1BL},{0x94L,0x1FL,0xCBL,0x5EL,0x52L}},{{255UL,0x5EL,255UL,0x7DL,1UL},{252UL,1UL,5UL,0x5EL,0xA7L},{0x4AL,0x1BL,9UL,0x23L,9UL},{4UL,4UL,9UL,0x94L,1UL},{246UL,0x7DL,5UL,255UL,0x23L}},{{0x1FL,1UL,255UL,252UL,246UL},{0x1BL,0x7DL,0xCBL,0x4AL,255UL},{0UL,4UL,252UL,4UL,0UL},{0UL,0x1BL,0x1FL,246UL,4UL},{0x1BL,1UL,0x5EL,0x1FL,252UL}}};
            int64_t **l_1703 = &g_361;
            int16_t l_1704 = 0x74EDL;
            int32_t *l_1705 = &g_127;
            int i, j, k;
            l_1680 = ((((*l_1612) = ((((((*l_1595) |= 0x488FL) == ((safe_add_func_uint8_t_u_u(((0x37B8L ^ (l_1610 , ((((*l_1614) = l_1677[2][0][4]) >= ((safe_rshift_func_uint8_t_u_s(l_1680, 1)) == (safe_div_func_int64_t_s_s((((((*l_1705) = (((safe_mul_func_uint32_t_u_u((((*l_1672) = (safe_sub_func_int64_t_s_s(((*p_12) <= (safe_div_func_int32_t_s_s(((l_1578 | ((safe_add_func_uint16_t_u_u((safe_mul_func_uint32_t_u_u((l_1677[1][2][4] , (safe_mul_func_int32_t_s_s((safe_div_func_uint64_t_u_u((safe_add_func_uint64_t_u_u((((safe_div_func_uint32_t_u_u((&g_361 == l_1703), l_1680)) & l_1680) && l_1677[2][0][4]), l_1578)), g_1648)), l_1677[1][3][1]))), l_1609)), (*g_671))) <= l_1680)) , l_1680), l_1704))), l_1680))) != (*p_12)), g_347)) ^ 0xDEL) || 0UL)) , l_1680) , (*g_239)) , (**g_360)), l_1704)))) <= g_1706))) != l_1677[2][0][4]), 0x32L)) | 0x87L)) , l_1677[2][0][4]) , 0xFCL) > 0x36L)) < (*g_235)) ^ 0x23C2L);
        }
        else
        { /* block id: 857 */
            uint32_t ***l_1709 = &g_1707;
            uint32_t **l_1711 = (void*)0;
            (*g_1712) = func_44((((*l_1709) = g_1707) != (l_1578 , (l_1711 = l_1710))));
        }
    }
    (*p_12) = ((safe_div_func_int16_t_s_s(((void*)0 == p_12), ((l_1610 == (safe_mod_func_int16_t_s_s(0L, 65533UL))) || (safe_div_func_int16_t_s_s(((*g_671) ^= (safe_sub_func_int16_t_s_s(l_1609, (safe_mod_func_int64_t_s_s((((((void*)0 != l_1672) == l_1609) | 4UL) && l_1578), 3UL))))), 1L))))) || l_1578);
    return (*g_239);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_13(int64_t  p_14)
{ /* block id: 820 */
    int32_t *l_1574 = (void*)0;
    return l_1574;
}


/* ------------------------------------------ */
/* 
 * reads : g_360 g_361 g_89 g_40 g_1049 g_671 g_292 g_1226 g_590
 * writes: g_1077 g_40
 */
static uint16_t  func_21(int8_t  p_22, int32_t  p_23, const uint32_t  p_24, int32_t * p_25)
{ /* block id: 811 */
    uint64_t l_1554 = 0x8540048EAE6207DBLL;
    int8_t ***l_1555 = &g_1077;
    int8_t **l_1557[8] = {&g_308,&g_308,&g_308,&g_308,&g_308,&g_308,&g_308,&g_308};
    int8_t ***l_1556 = &l_1557[3];
    int8_t **l_1558 = &g_307;
    int8_t ***l_1559 = &l_1558;
    int64_t ** const l_1570 = &g_361;
    int32_t l_1571 = (-8L);
    int32_t l_1572 = 0L;
    int32_t l_1573 = (-2L);
    int i;
    l_1573 &= ((**g_360) > (safe_mod_func_uint64_t_u_u((l_1571 = (((*p_25) || (*p_25)) , (l_1554 < ((((*l_1556) = ((*l_1555) = &g_307)) != ((*l_1559) = l_1558)) <= (safe_add_func_int64_t_s_s(((((**g_1226) ^= ((((safe_mod_func_int32_t_s_s((0xE204CE578BC97011LL <= (safe_lshift_func_int16_t_s_s(((safe_lshift_func_uint32_t_u_u(((l_1554 == ((safe_mul_func_int64_t_s_s(l_1554, p_24)) | 18446744073709551614UL)) == 0xC4L), 13)) | l_1554), 7))), g_1049)) > (*g_671)) , (void*)0) == l_1570)) , p_24) , (*g_361)), p_22)))))), l_1572)));
    p_25 = &l_1573;
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_40 g_37 g_122 g_519 g_520 g_176 g_277 g_381 g_3 g_360 g_361 g_64 g_89 g_130 g_292 g_183 g_572 g_239 g_236 g_590 g_663 g_671 g_127 g_497 g_235 g_750 g_184 g_93 g_347 g_1226 g_448
 * writes: g_40 g_308 g_307 g_519 g_276 g_89 g_176 g_448 g_183 g_572 g_292 g_236 g_345 g_64 g_670 g_130 g_381 g_122 g_347 g_93 g_671 g_37 g_127
 */
static uint8_t  func_29(int32_t  p_30, int32_t  p_31, int32_t  p_32, int32_t * p_33)
{ /* block id: 8 */
    int32_t *l_56[2][7];
    int32_t *l_58[2];
    int32_t **l_57 = &l_58[1];
    int32_t *l_60 = &g_3;
    int32_t **l_59[3][6] = {{&l_56[1][5],&l_60,&l_56[1][5],&l_56[0][0],&l_56[1][5],&l_56[0][0]},{&l_56[0][0],&l_56[1][5],&l_56[0][0],&l_56[0][0],&l_56[1][5],&l_56[0][0]},{&l_56[0][0],&l_56[1][5],&l_56[0][0],&l_56[0][0],&l_56[1][5],&l_56[0][0]}};
    int32_t **l_61 = &l_60;
    int16_t *l_556 = &g_292;
    int16_t **l_555 = &l_556;
    int64_t l_557 = 0xDBD5D52FD6D98BF0LL;
    int32_t l_558 = 4L;
    int8_t l_1452 = 1L;
    int32_t l_1453 = 0x49B882DBL;
    uint32_t l_1454[6][8] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0UL,3UL,0UL,18446744073709551615UL,18446744073709551615UL},{3UL,0UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0UL,3UL,0UL},{3UL,18446744073709551615UL,0x2B79A694L,18446744073709551615UL,3UL,1UL,3UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0UL,3UL,0UL,18446744073709551615UL,18446744073709551615UL},{3UL,0UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0UL,3UL,0UL},{3UL,18446744073709551615UL,0x2B79A694L,18446744073709551615UL,3UL,1UL,3UL,18446744073709551615UL}};
    uint8_t l_1485[9] = {0xE3L,1UL,0xE3L,1UL,0xE3L,1UL,0xE3L,1UL,0xE3L};
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
            l_56[i][j] = &g_3;
    }
    for (i = 0; i < 2; i++)
        l_58[i] = &g_40;
lbl_1459:
    p_30 |= func_41(p_31, func_44((g_40 , (safe_sub_func_uint8_t_u_u(((safe_div_func_int8_t_s_s((func_50(func_53(l_56[1][5], ((*l_61) = ((*l_57) = &g_40))), g_37[3]) != (safe_mod_func_int32_t_s_s((safe_add_func_uint32_t_u_u((l_555 != &l_556), g_37[0])), g_130))), g_292)) >= l_557), l_558)))));
    if ((*p_33))
    { /* block id: 739 */
        int8_t l_1450[5] = {5L,5L,5L,5L,5L};
        int32_t l_1451[2];
        int i;
        for (i = 0; i < 2; i++)
            l_1451[i] = 0x927AECCFL;
        l_1454[3][0]--;
    }
    else
    { /* block id: 741 */
        int32_t l_1484 = 0x5334D175L;
        int16_t * const **l_1490 = (void*)0;
        int32_t l_1507 = 0x538D16B4L;
        int32_t l_1524 = (-8L);
        int32_t l_1529[6][1];
        uint16_t l_1541 = 0x1F24L;
        int i, j;
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 1; j++)
                l_1529[i][j] = 0x30F1587FL;
        }
        for (p_31 = 19; (p_31 != (-26)); --p_31)
        { /* block id: 744 */
            uint64_t *l_1471 = &g_381[2][0];
            int16_t l_1478[3][6][5] = {{{0xFF5FL,0xF91EL,(-1L),0xD335L,5L},{1L,0xF91EL,0xD335L,0xD729L,0x0D89L},{8L,0xFF5FL,0xFF5FL,8L,0xD729L},{1L,1L,5L,1L,0xD729L},{0xFF5FL,1L,0x0D89L,(-1L),0x0D89L},{(-1L),(-1L),0xD729L,1L,5L}},{{0xDD71L,0x65B6L,0xD729L,8L,0xFF5FL},{0xD335L,0xD729L,0x0D89L,0xD729L,0xD335L},{1L,0x65B6L,5L,0xD335L,(-1L)},{1L,(-1L),0xFF5FL,1L,1L},{0xD335L,1L,0xD335L,0x65B6L,(-1L)},{0xDD71L,1L,(-1L),0x65B6L,0xD335L}},{{(-1L),0xFF5FL,1L,1L,0xFF5FL},{0xFF5FL,0xF91EL,(-1L),0xD335L,5L},{1L,0xF91EL,0xD335L,0xD729L,0x0D89L},{8L,0xFF5FL,0xFF5FL,8L,0xD729L},{1L,1L,5L,1L,0xD729L},{0xFF5FL,1L,0x0D89L,(-1L),0x0D89L}}};
            uint8_t *l_1483 = &g_1050;
            int32_t l_1506[3][9][7] = {{{0x5B5067EBL,1L,0x9FD88045L,1L,1L,(-1L),(-3L)},{0L,(-1L),0xB47F7FE0L,0x924B8692L,0x924B8692L,0xB47F7FE0L,(-1L)},{1L,(-1L),0x7F851A8BL,5L,0xCDCC2CB6L,1L,3L},{(-10L),1L,0xB10F5C67L,0x4AD3F8F9L,0x5B5067EBL,0x7F851A8BL,0xB47F7FE0L},{0xB10F5C67L,1L,(-10L),3L,0L,1L,1L},{1L,0xB47F7FE0L,0L,0xB47F7FE0L,1L,1L,0L},{0x9FD88045L,(-1L),0x4AD3F8F9L,0L,(-10L),0x7F851A8BL,(-1L)},{3L,0x5B5067EBL,(-1L),0xCDCC2CB6L,0L,3L,1L},{0x9FD88045L,0L,3L,0x4AD3F8F9L,1L,0x4AD3F8F9L,3L}},{{1L,1L,3L,0x0CE948DDL,0L,0x924B8692L,1L},{0xB10F5C67L,(-10L),(-1L),0L,1L,(-3L),0L},{(-2L),0L,0x4AD3F8F9L,(-10L),0L,5L,0L},{3L,1L,0L,0L,1L,3L,0L},{5L,0L,(-10L),0x4AD3F8F9L,0L,(-2L),0L},{(-3L),1L,0L,(-1L),(-10L),0xB10F5C67L,1L},{0x924B8692L,0L,0x0CE948DDL,3L,1L,1L,3L},{0x4AD3F8F9L,1L,0x4AD3F8F9L,3L,0L,0x9FD88045L,1L},{3L,0L,0xCDCC2CB6L,(-1L),0x5B5067EBL,3L,(-1L)}},{{0x7F851A8BL,(-10L),0L,0x4AD3F8F9L,(-1L),0x9FD88045L,0L},{1L,1L,0xB47F7FE0L,0L,0xB47F7FE0L,1L,1L},{1L,0L,3L,(-10L),1L,0xB10F5C67L,0xB47F7FE0L},{0x7F851A8BL,0x5B5067EBL,0x4AD3F8F9L,0L,3L,(-2L),0x5B5067EBL},{3L,(-1L),3L,0x0CE948DDL,(-1L),3L,(-1L)},{0x4AD3F8F9L,0xB47F7FE0L,0xB47F7FE0L,0x4AD3F8F9L,(-1L),5L,(-10L)},{0x924B8692L,1L,0L,0xCDCC2CB6L,3L,(-3L),1L},{(-3L),3L,0xCDCC2CB6L,0L,1L,0x924B8692L,(-10L)},{5L,(-1L),0x4AD3F8F9L,0xB47F7FE0L,0xB47F7FE0L,0x4AD3F8F9L,(-1L)}}};
            int i, j, k;
            if (g_130)
                goto lbl_1459;
        }
    }
    (**g_1226) = ((**l_61) , 0xE1839AE7L);
    return (*g_235);
}


/* ------------------------------------------ */
/* 
 * reads : g_448 g_361 g_89 g_64 g_183 g_572 g_519 g_520 g_176 g_292 g_239 g_236 g_590 g_40 g_122 g_663 g_277 g_130 g_671 g_127 g_381 g_360 g_497 g_235 g_750 g_184 g_37 g_93 g_347 g_1226
 * writes: g_448 g_89 g_183 g_176 g_572 g_292 g_236 g_345 g_64 g_40 g_670 g_130 g_381 g_122 g_347 g_93 g_671 g_37 g_127
 */
static int32_t  func_41(uint8_t  p_42, int32_t * p_43)
{ /* block id: 469 */
    int8_t l_921[9];
    int32_t *l_922[7][3][10] = {{{&g_40,(void*)0,&g_64[3],&g_122,(void*)0,&g_40,&g_64[1],&g_64[1],&g_122,&g_64[3]},{&g_40,&g_64[1],&g_64[1],&g_122,&g_64[3],&g_40,&g_64[4],&g_3,&g_3,&g_64[4]},{&g_40,&g_64[4],&g_3,&g_3,&g_64[4],&g_40,&g_3,(void*)0,(void*)0,&g_64[1]}},{{(void*)0,&g_3,(void*)0,(void*)0,&g_64[1],(void*)0,&g_40,&g_64[1],&g_64[3],&g_3},{(void*)0,&g_40,&g_64[1],&g_64[3],&g_3,(void*)0,&g_3,&g_64[3],&g_64[1],&g_40},{(void*)0,&g_3,&g_64[3],&g_64[1],&g_40,(void*)0,&g_64[1],(void*)0,(void*)0,&g_3}},{{(void*)0,&g_64[1],(void*)0,(void*)0,&g_3,(void*)0,&g_3,&g_64[4],&g_64[4],&g_3},{(void*)0,&g_3,&g_64[4],&g_64[4],&g_3,(void*)0,&g_3,(void*)0,(void*)0,&g_64[1]},{(void*)0,&g_3,(void*)0,(void*)0,&g_64[1],(void*)0,&g_40,&g_64[1],&g_64[3],&g_3}},{{(void*)0,&g_40,&g_64[1],&g_64[3],&g_3,(void*)0,&g_3,&g_64[3],&g_64[1],&g_40},{(void*)0,&g_3,&g_64[3],&g_64[1],&g_40,(void*)0,&g_64[1],(void*)0,(void*)0,&g_3},{(void*)0,&g_64[1],(void*)0,(void*)0,&g_3,(void*)0,&g_3,&g_64[4],&g_64[4],&g_3}},{{(void*)0,&g_3,&g_64[4],&g_64[4],&g_3,(void*)0,&g_3,(void*)0,(void*)0,&g_64[1]},{(void*)0,&g_3,(void*)0,(void*)0,&g_64[1],(void*)0,&g_40,&g_64[1],&g_64[3],&g_3},{(void*)0,&g_40,&g_64[1],&g_64[3],&g_3,(void*)0,&g_3,&g_64[3],&g_64[1],&g_40}},{{(void*)0,&g_3,&g_64[3],&g_64[1],&g_40,(void*)0,&g_64[1],(void*)0,(void*)0,&g_3},{(void*)0,&g_64[1],(void*)0,(void*)0,&g_3,(void*)0,&g_3,&g_64[4],&g_64[4],&g_3},{(void*)0,&g_3,&g_64[4],&g_64[4],&g_3,(void*)0,&g_3,(void*)0,(void*)0,&g_64[1]}},{{(void*)0,&g_3,(void*)0,(void*)0,&g_64[1],(void*)0,&g_40,&g_64[1],&g_64[3],&g_3},{(void*)0,&g_40,&g_64[1],&g_64[3],&g_3,(void*)0,&g_3,&g_64[3],&g_64[1],&g_40},{(void*)0,&g_3,&g_64[3],&g_64[1],&g_40,(void*)0,&g_64[1],(void*)0,(void*)0,&g_3}}};
    int8_t l_923 = (-1L);
    uint16_t l_924 = 5UL;
    int32_t *****l_927 = (void*)0;
    uint16_t ***l_948 = &g_276[0][4];
    uint16_t ****l_947 = &l_948;
    int32_t l_958 = (-1L);
    uint32_t l_959 = 0x8095BFC4L;
    int32_t *l_1092 = &g_64[1];
    uint64_t *l_1093 = &g_347;
    uint16_t l_1108 = 0xD561L;
    int16_t *l_1116 = &g_292;
    int16_t l_1161 = 7L;
    int32_t l_1203 = 4L;
    uint32_t l_1270 = 18446744073709551612UL;
    uint8_t l_1274[5] = {6UL,6UL,6UL,6UL,6UL};
    uint8_t *l_1280 = &g_951;
    int64_t ***l_1333 = (void*)0;
    uint32_t l_1340 = 0x62499510L;
    int32_t l_1402 = 1L;
    uint64_t l_1430 = 0x9314F6F97A7DCCDDLL;
    uint32_t l_1445 = 1UL;
    int8_t *l_1446 = (void*)0;
    int8_t *l_1447[1];
    uint8_t l_1448 = 0x77L;
    uint32_t l_1449 = 1UL;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_921[i] = 0x3FL;
    for (i = 0; i < 1; i++)
        l_1447[i] = &g_183;
    l_922[5][1][2] = func_44(l_921[2]);
    l_924--;
    for (g_127 = 4; (g_127 >= 0); g_127 -= 1)
    { /* block id: 474 */
        uint32_t l_939[3];
        uint64_t *l_949 = (void*)0;
        int32_t l_957[4][7][8] = {{{1L,0x594B2C1BL,0xF77F00DAL,0x4ACC414BL,0x30A76730L,0xD7F0B160L,0x30A76730L,0x4ACC414BL},{0xC1BCE218L,(-10L),0xC1BCE218L,0x8A526867L,9L,0x6514BEE8L,1L,0x7B55D527L},{0x6852DCB4L,0xB8E6D9C0L,0x4ACC414BL,5L,0xCD52FB18L,0xC70D4E7AL,9L,0x5BE87703L},{0x6852DCB4L,0xE4A8907CL,(-10L),1L,9L,0xC1BCE218L,5L,0x0D305902L},{0xC1BCE218L,0x8C5203A6L,1L,0xE4A8907CL,0x30A76730L,0x30A76730L,0xE4A8907CL,1L},{0x7B55D527L,0x7B55D527L,1L,0xD7F0B160L,0L,1L,0x8A526867L,0x6514BEE8L},{0xD7F0B160L,1L,0x594B2C1BL,0x9D14A5B4L,0x8C5203A6L,9L,0L,0x6514BEE8L}},{{1L,0x8A526867L,0x0D305902L,0xD7F0B160L,0L,5L,0x2BE95A58L,1L},{0x594B2C1BL,0x0D305902L,0L,0xE4A8907CL,0x5F0F06DFL,0xE4A8907CL,0L,0x0D305902L},{0x9D14A5B4L,0xD7F0B160L,0xE4A8907CL,1L,5L,0x8A526867L,0x4ACC414BL,0x5BE87703L},{0x8C5203A6L,0L,0x5F0F06DFL,5L,0x9D14A5B4L,0L,0x4ACC414BL,0x7B55D527L},{9L,5L,0xE4A8907CL,0x8A526867L,0L,0x2BE95A58L,0L,0x4ACC414BL},{0L,0x2BE95A58L,0L,0x4ACC414BL,0x4ACC414BL,0L,0x2BE95A58L,0L},{0x6514BEE8L,1L,0x0D305902L,0x5BE87703L,0x7B55D527L,0x4ACC414BL,0L,0x9D14A5B4L}},{{1L,0L,0x594B2C1BL,0x30A76730L,0x5BE87703L,0x4ACC414BL,0x8A526867L,5L},{(-10L),1L,1L,0x594B2C1BL,0x0D305902L,0L,0xE4A8907CL,0x5F0F06DFL},{5L,0x2BE95A58L,1L,0L,1L,0x2BE95A58L,5L,0L},{0xC70D4E7AL,5L,(-10L),1L,0x6514BEE8L,0L,9L,0x8C5203A6L},{0x0D305902L,0L,0x4ACC414BL,0x6852DCB4L,0x6514BEE8L,0x8A526867L,1L,0L},{0xC70D4E7AL,0xD7F0B160L,0xC1BCE218L,0x8C5203A6L,1L,0xE4A8907CL,0x30A76730L,0x30A76730L},{5L,0x0D305902L,0xF77F00DAL,0xF77F00DAL,0x0D305902L,5L,0xC1BCE218L,9L}},{{(-10L),0x8A526867L,0x9D14A5B4L,1L,0x5BE87703L,9L,0xC70D4E7AL,0xCD52FB18L},{1L,1L,0L,1L,0x7B55D527L,1L,0x6514BEE8L,9L},{0x6514BEE8L,0x7B55D527L,0L,0xF77F00DAL,0x4ACC414BL,0x30A76730L,0xD7F0B160L,0x30A76730L},{1L,0x0D305902L,0x2BE95A58L,0x0D305902L,1L,0x6852DCB4L,0xB8E6D9C0L,0x4ACC414BL},{1L,0x30A76730L,1L,0x5BE87703L,0xCD52FB18L,0x2BE95A58L,0x7B55D527L,0x0D305902L},{0x0D305902L,0x8A526867L,1L,0x7B55D527L,0x8C5203A6L,0xE4A8907CL,0xB8E6D9C0L,(-10L)},{0xCD52FB18L,0x594B2C1BL,0x2BE95A58L,0x4ACC414BL,0xD7F0B160L,9L,9L,0xD7F0B160L}}};
        const int32_t *l_977[8][1][8] = {{{(void*)0,&g_40,&g_3,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_3,(void*)0,(void*)0,&g_3,(void*)0,&g_64[1],&g_64[1],&g_122}},{{(void*)0,&g_3,&g_40,(void*)0,&g_64[1],&g_40,&g_64[1],&g_122}},{{&g_122,&g_3,&g_3,(void*)0,&g_40,&g_64[1],&g_40,(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_122,&g_3,(void*)0,&g_3,&g_40}},{{(void*)0,&g_40,(void*)0,&g_122,(void*)0,&g_40,&g_3,&g_3}},{{(void*)0,&g_3,&g_40,&g_40,&g_3,&g_3,&g_40,&g_40}},{{(void*)0,(void*)0,&g_40,&g_122,&g_40,&g_3,(void*)0,&g_64[1]}}};
        int64_t l_979 = 8L;
        uint32_t l_994 = 1UL;
        int8_t *l_996 = &g_183;
        uint32_t *l_1017 = &l_994;
        uint64_t l_1066[1];
        const uint16_t *l_1117 = &g_37[2];
        int8_t l_1135 = 0L;
        const uint32_t l_1175 = 4294967295UL;
        uint16_t l_1196 = 1UL;
        uint16_t ****l_1245 = &l_948;
        uint8_t l_1272 = 0xDBL;
        uint8_t *l_1279 = &g_951;
        uint64_t l_1400 = 0x02574E437F3C0921LL;
        int64_t l_1404 = (-9L);
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_939[i] = 0UL;
        for (i = 0; i < 1; i++)
            l_1066[i] = 1UL;
    }
    (**g_1226) |= ((p_42 > (l_1448 = 0L)) | 0x39EED82A9E95EF6ALL);
    return l_1449;
}


/* ------------------------------------------ */
/* 
 * reads : g_448 g_361 g_89 g_64 g_183 g_572 g_519 g_520 g_176 g_292 g_239 g_236 g_590 g_40 g_122 g_663 g_277 g_130 g_671 g_127 g_381 g_360 g_497 g_235 g_750 g_184 g_37 g_93 g_347
 * writes: g_448 g_89 g_183 g_176 g_572 g_292 g_236 g_345 g_64 g_40 g_670 g_130 g_381 g_122 g_347 g_93 g_671 g_37
 */
static int32_t * func_44(int32_t  p_45)
{ /* block id: 273 */
    int8_t l_564 = 0x28L;
    int32_t l_574 = 0x3ABB4506L;
    int32_t *l_584 = &g_122;
    int32_t l_608 = 1L;
    int32_t l_615 = 0x11F99312L;
    uint64_t l_617 = 1UL;
    uint64_t *l_774 = &g_347;
    uint64_t **l_773 = &l_774;
    int64_t ***l_785 = (void*)0;
    int64_t ***l_786[2][9][10] = {{{&g_360,&g_360,(void*)0,&g_360,&g_360,(void*)0,(void*)0,&g_360,&g_360,(void*)0},{(void*)0,(void*)0,&g_360,&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,(void*)0,&g_360,&g_360},{&g_360,(void*)0,(void*)0,&g_360,&g_360,(void*)0,(void*)0,&g_360,&g_360,&g_360},{&g_360,&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360},{(void*)0,(void*)0,(void*)0,&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,(void*)0},{&g_360,(void*)0,(void*)0,&g_360,&g_360,&g_360,&g_360,(void*)0,(void*)0,(void*)0},{(void*)0,&g_360,&g_360,(void*)0,&g_360,&g_360,(void*)0,&g_360,(void*)0,(void*)0},{&g_360,&g_360,&g_360,&g_360,(void*)0,(void*)0,&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,(void*)0,&g_360,(void*)0,(void*)0,&g_360,(void*)0,&g_360,&g_360},{&g_360,&g_360,&g_360,(void*)0,&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,(void*)0,(void*)0,&g_360,&g_360,&g_360,&g_360,(void*)0,(void*)0},{&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,&g_360},{&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,&g_360,&g_360,(void*)0},{&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,&g_360,(void*)0,(void*)0},{&g_360,&g_360,&g_360,(void*)0,(void*)0,&g_360,&g_360,&g_360,&g_360,(void*)0},{&g_360,(void*)0,&g_360,(void*)0,&g_360,&g_360,&g_360,(void*)0,&g_360,(void*)0}}};
    int16_t l_790[3][9] = {{0xCD39L,0x7332L,8L,(-1L),8L,(-6L),(-10L),(-10L),(-6L)},{(-1L),0x6C0BL,0x4C9EL,0x6C0BL,(-1L),0x7332L,0xFF80L,8L,0x3757L},{0xFF80L,0x7332L,(-1L),0x6C0BL,0x4C9EL,0x6C0BL,(-1L),0x7332L,0xFF80L}};
    uint16_t *l_810[8] = {&g_37[2],&g_37[2],&g_37[2],&g_37[2],&g_37[2],&g_37[2],&g_37[2],&g_37[2]};
    int32_t l_831 = 0xB6C3D937L;
    int32_t l_832 = (-3L);
    int32_t l_833 = (-1L);
    int8_t l_834 = 8L;
    int32_t l_835 = (-8L);
    int32_t l_836 = 0x8B29F4FDL;
    int32_t l_838 = 1L;
    int32_t l_839 = 0x6C1E2A0CL;
    const int16_t l_918 = 0x5041L;
    int i, j, k;
    for (g_448 = 28; (g_448 > (-19)); g_448--)
    { /* block id: 276 */
        uint64_t l_561 = 0UL;
        int32_t ***l_568 = &g_112;
        int32_t ****l_569 = &l_568;
        int8_t *l_573[10] = {(void*)0,&g_183,(void*)0,(void*)0,&g_183,(void*)0,(void*)0,&g_183,(void*)0,(void*)0};
        int32_t l_588[1][10];
        const int32_t l_595 = 0L;
        int32_t l_605 = 0x568ED448L;
        uint16_t * const *l_686 = &g_277;
        uint16_t * const **l_685[8][1][3] = {{{(void*)0,(void*)0,&l_686}},{{(void*)0,&l_686,(void*)0}},{{(void*)0,(void*)0,&l_686}},{{(void*)0,&l_686,(void*)0}},{{(void*)0,(void*)0,&l_686}},{{(void*)0,&l_686,(void*)0}},{{(void*)0,(void*)0,&l_686}},{{(void*)0,&l_686,(void*)0}}};
        uint16_t * const ***l_684 = &l_685[6][0][2];
        uint64_t **l_709 = (void*)0;
        uint64_t *l_711 = &l_617;
        uint64_t **l_710 = &l_711;
        int32_t *l_716[10][1] = {{(void*)0},{&l_605},{(void*)0},{&l_605},{(void*)0},{&l_605},{(void*)0},{&l_605},{(void*)0},{&l_605}};
        uint64_t *l_717 = (void*)0;
        uint64_t *l_718[1];
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 10; j++)
                l_588[i][j] = 0xAF2316B2L;
        }
        for (i = 0; i < 1; i++)
            l_718[i] = (void*)0;
        l_574 = (((l_561 , (g_183 = ((safe_add_func_int64_t_s_s(((*g_361) &= (l_564 > ((!1UL) && p_45))), ((safe_lshift_func_uint8_t_u_u((((p_45 <= (((((*l_569) = l_568) == (p_45 , &g_112)) != (safe_div_func_int16_t_s_s(((g_64[1] , 255UL) >= p_45), g_183))) && p_45)) , p_45) & l_564), g_572)) != (**g_519)))) == 0x05L))) != 1UL) || 0x41L);
        for (l_574 = 7; (l_574 > 27); ++l_574)
        { /* block id: 283 */
            int8_t l_609 = 0x66L;
            int32_t l_614 = 1L;
            int32_t l_616[2];
            uint8_t l_622 = 0xBBL;
            int i;
            for (i = 0; i < 2; i++)
                l_616[i] = 0L;
            for (g_176 = 0; (g_176 > 57); g_176 = safe_add_func_uint64_t_u_u(g_176, 2))
            { /* block id: 286 */
                int32_t *l_583[5][6][1] = {{{&l_574},{&g_64[1]},{&g_40},{&l_574},{&g_40},{&g_64[1]}},{{&l_574},{(void*)0},{&g_122},{(void*)0},{&l_574},{&g_64[1]}},{{&g_40},{&l_574},{&g_40},{&g_64[1]},{&l_574},{(void*)0}},{{&g_122},{(void*)0},{&l_574},{&g_64[1]},{&g_40},{&l_574}},{{&g_40},{&g_64[1]},{&l_574},{&l_574},{&l_574},{&l_574}}};
                int8_t l_606[8] = {0x1AL,(-1L),(-1L),0x1AL,(-1L),(-1L),0x1AL,(-1L)};
                int i, j, k;
            }
            return &g_64[6];
        }
        for (g_572 = 5; (g_572 > 36); g_572++)
        { /* block id: 310 */
            uint8_t l_641[3][10];
            int64_t **l_647 = &g_361;
            int64_t ***l_648 = (void*)0;
            uint16_t **l_687 = &g_277;
            int32_t *l_700[10][2][10] = {{{&l_588[0][0],&g_40,&g_122,&l_615,&g_40,&g_40,(void*)0,(void*)0,&g_40,&g_40},{(void*)0,(void*)0,(void*)0,(void*)0,&l_588[0][0],(void*)0,&l_588[0][0],(void*)0,&l_608,&l_608}},{{&g_3,&l_608,&l_574,(void*)0,&l_615,&l_574,(void*)0,&l_588[0][0],&l_608,&l_608},{&g_122,&l_615,&g_40,(void*)0,&l_574,&g_64[1],&g_64[1],&l_588[0][6],&g_40,&l_608}},{{(void*)0,&l_588[0][0],&l_588[0][0],&l_615,&g_122,&l_615,(void*)0,&g_64[6],&g_64[1],(void*)0},{&l_588[0][0],&g_40,&l_588[0][5],&g_40,&l_615,&l_588[0][0],(void*)0,&g_122,(void*)0,&g_3}},{{&g_122,(void*)0,&g_64[1],&g_122,&g_40,&g_64[1],&l_588[0][6],&g_40,&g_64[2],&g_3},{(void*)0,&l_588[0][1],&l_588[0][0],&g_122,&g_3,&l_588[0][5],&g_122,(void*)0,&g_3,&g_40}},{{&g_3,&l_615,&l_615,&g_3,&g_64[0],&l_588[0][0],&l_588[0][5],&g_122,&l_588[0][5],&l_588[0][0]},{&l_608,&g_3,&g_64[1],&g_3,&l_608,&g_40,&g_122,&g_40,(void*)0,&l_588[0][0]}},{{&l_615,(void*)0,&l_574,&g_64[1],&l_588[0][0],&l_574,&g_122,&g_64[6],&g_40,&l_588[0][0]},{&g_64[6],&g_64[1],(void*)0,(void*)0,&l_608,(void*)0,&l_588[0][1],&l_608,&l_588[0][0],&l_588[0][0]}},{{(void*)0,(void*)0,&g_40,&l_608,&g_64[0],&g_122,&g_122,&g_64[1],&l_615,&g_40},{&g_3,&l_588[0][0],&g_3,&g_40,&g_3,&l_588[0][0],(void*)0,(void*)0,(void*)0,&g_3}},{{&l_588[0][0],(void*)0,(void*)0,&g_122,&g_40,&g_3,&l_588[0][1],(void*)0,&l_608,&g_3},{&g_64[0],&l_588[0][0],&g_122,(void*)0,&l_615,&l_615,&l_608,&g_64[1],&g_64[2],(void*)0}},{{&g_122,(void*)0,&l_608,(void*)0,&g_122,&g_64[1],&l_608,(void*)0,(void*)0,&l_608},{(void*)0,&l_574,&g_122,&g_122,&l_574,(void*)0,&l_588[0][5],&g_64[0],(void*)0,&l_608}},{{&l_588[0][1],(void*)0,&l_615,&g_3,&l_574,&g_40,(void*)0,(void*)0,(void*)0,&g_40},{&l_588[0][1],(void*)0,&g_40,(void*)0,(void*)0,&l_574,&g_40,(void*)0,(void*)0,(void*)0}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 10; j++)
                    l_641[i][j] = 0xA7L;
            }
            for (g_292 = (-24); (g_292 < (-25)); g_292 = safe_sub_func_uint64_t_u_u(g_292, 7))
            { /* block id: 313 */
                int32_t *l_630 = &g_40;
                int32_t *l_631 = &l_588[0][0];
                int32_t *l_632 = &l_588[0][7];
                int32_t *l_633 = &g_64[1];
                int32_t *l_634 = &l_574;
                int32_t *l_635 = &g_64[1];
                int32_t *l_636 = &l_588[0][0];
                int32_t *l_637 = &g_64[1];
                int32_t *l_638 = &g_64[4];
                int32_t *l_639 = &g_64[6];
                int32_t *l_640[1][7][6] = {{{(void*)0,&g_122,&g_64[1],&g_64[1],&g_122,(void*)0},{&l_574,(void*)0,&g_64[1],(void*)0,&l_574,&l_574},{&l_615,(void*)0,(void*)0,&l_615,&g_122,&l_615},{&l_615,&g_122,&l_615,(void*)0,(void*)0,&l_615},{&l_574,&l_574,(void*)0,&g_64[1],(void*)0,&l_574},{(void*)0,&g_122,&g_64[1],&g_64[1],&g_122,(void*)0},{&l_574,(void*)0,&g_64[1],(void*)0,&l_574,&l_574}}};
                int64_t **l_645 = (void*)0;
                int64_t ***l_646[1];
                uint32_t *l_653 = &g_345[0];
                uint32_t l_656 = 1UL;
                uint16_t ** const *l_661 = &g_276[0][4];
                uint16_t ** const **l_660[1];
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_646[i] = &g_360;
                for (i = 0; i < 1; i++)
                    l_660[i] = &l_661;
                l_641[0][0]--;
                if (((((safe_unary_minus_func_uint8_t_u(((l_647 = (p_45 , l_645)) == (void*)0))) , &l_645) != l_648) <= (((((0UL > (safe_sub_func_uint32_t_u_u(((*l_653) = (+((!((*g_239) |= 1UL)) <= p_45))), (safe_mod_func_int32_t_s_s(((*g_590) ^= ((*l_637) = p_45)), l_656))))) != g_89) >= (*l_584)) > p_45) , 0UL)))
                { /* block id: 320 */
                    int32_t *l_659[4];
                    uint16_t ***l_683[9][1][1];
                    uint16_t ****l_682 = &l_683[6][0][0];
                    int64_t **l_698 = &g_361;
                    int32_t *l_701 = &g_3;
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_659[i] = &g_40;
                    for (i = 0; i < 9; i++)
                    {
                        for (j = 0; j < 1; j++)
                        {
                            for (k = 0; k < 1; k++)
                                l_683[i][j][k] = &g_276[1][1];
                        }
                    }
                    for (l_656 = 0; (l_656 > 44); l_656 = safe_add_func_int32_t_s_s(l_656, 1))
                    { /* block id: 323 */
                        uint16_t ** const ***l_662 = &l_660[0];
                        uint16_t *l_678 = &g_130;
                        uint64_t *l_681 = &g_381[2][0];
                        int32_t l_699 = 0xF480FDA3L;
                        l_659[2] = l_640[0][1][5];
                        (*g_590) = (((((*l_662) = l_660[0]) != g_663) ^ (safe_lshift_func_int16_t_s_s(g_176, 7))) | ((*l_681) &= (((*l_584) > ((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int64_t_s_s(((g_670[4][0][3] = (void*)0) != (void*)0), (safe_mul_func_int32_t_s_s(((*l_634) = (safe_sub_func_uint32_t_u_u(((((p_45 || (safe_sub_func_int8_t_s_s(((((*g_277) == (((*l_678)--) | 0x514BL)) == g_89) < 0L), 0xEEL))) , 6L) && p_45) <= (*g_671)), 4294967292UL))), p_45)))), 13)) >= p_45)) <= g_127)));
                        (*l_584) &= (((l_682 != l_684) ^ (l_687 == ((safe_add_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u(p_45, ((safe_mod_func_uint16_t_u_u((((4294967295UL && ((*l_637) = (safe_mul_func_int8_t_s_s(p_45, (((**g_360) , 0x5243FD99L) & (safe_div_func_uint16_t_u_u((((void*)0 == l_698) && p_45), l_699))))))) >= 0x184BC0FDCAB1E477LL) != p_45), 0xB2CFL)) && 1L))) || p_45), (*g_239))) , (void*)0))) >= p_45);
                    }
                    return l_701;
                }
                else
                { /* block id: 335 */
                    return &g_40;
                }
            }
        }
        l_608 |= ((*g_590) = (p_45 , ((safe_unary_minus_func_uint64_t_u((safe_div_func_int16_t_s_s(((((254UL | (*l_584)) == (safe_mul_func_uint64_t_u_u((g_347 = ((0x7871L < ((*l_584) < (((safe_div_func_int64_t_s_s((((*l_710) = &l_561) != (void*)0), (safe_sub_func_uint32_t_u_u(((p_45 = (safe_add_func_uint32_t_u_u((l_573[9] != ((0xBD7CL ^ (*l_584)) , l_573[0])), (*l_584)))) , (*l_584)), (*g_590))))) || (*l_584)) > 0x63EDA480L))) | 65527UL)), g_381[2][0]))) && 0UL) || p_45), (*l_584))))) , p_45)));
    }
    for (g_448 = 0; (g_448 >= (-5)); g_448--)
    { /* block id: 348 */
        int64_t l_725 = 0L;
        uint32_t *l_732 = (void*)0;
        uint32_t *l_733 = (void*)0;
        uint32_t *l_734 = &g_345[0];
        uint64_t *l_735 = (void*)0;
        uint64_t *l_736 = (void*)0;
        uint64_t *l_737 = &g_381[2][0];
        (*g_590) &= (~((p_45 && (((+(safe_add_func_int64_t_s_s((((l_725 >= (safe_lshift_func_uint32_t_u_s((*l_584), 14))) && p_45) ^ l_725), ((*l_737) = ((((l_725 != (((((*l_734) = ((((1UL & (safe_mul_func_int8_t_s_s(0xB7L, (safe_sub_func_int16_t_s_s((-5L), l_725))))) != 0xEB8015C2L) , (*g_671)) | 0xEBEBL)) || 0xD0A5A858L) ^ (*g_671)) != p_45)) < g_497[5][8][0]) , l_725) < p_45))))) , p_45) >= p_45)) , (*g_235)));
    }
    (*g_590) ^= (*l_584);
    for (g_183 = 0; (g_183 <= 1); g_183 += 1)
    { /* block id: 356 */
        uint64_t l_739[5][9] = {{7UL,0x3E10DE8473C66A19LL,1UL,0x54C33D59B69A5BA6LL,0xB7CDED12F01EFF69LL,18446744073709551611UL,0xF7B5A59AEBB28ECBLL,18446744073709551611UL,0xB7CDED12F01EFF69LL},{0x572B19767016DED8LL,0xB7CDED12F01EFF69LL,0xB7CDED12F01EFF69LL,0x572B19767016DED8LL,8UL,1UL,0x01F69067B43EFAA3LL,0xF7B5A59AEBB28ECBLL,1UL},{0x572B19767016DED8LL,4UL,0x7A2D62BFD81E23A7LL,1UL,1UL,8UL,8UL,1UL,1UL},{7UL,18446744073709551611UL,7UL,0x7A2D62BFD81E23A7LL,8UL,0xF7B5A59AEBB28ECBLL,0x54C33D59B69A5BA6LL,0xE0BCB3069B9480DFLL,1UL},{0x5E76EA5B2C4D943BLL,7UL,1UL,0x01F69067B43EFAA3LL,0xB7CDED12F01EFF69LL,0xE0BCB3069B9480DFLL,0xB7CDED12F01EFF69LL,0x01F69067B43EFAA3LL,1UL}};
        int32_t *l_740[2][9] = {{&g_64[1],&g_64[1],&g_64[1],&g_64[1],&g_64[1],&g_64[1],&g_64[1],&g_64[1],&g_64[1]},{&l_574,&l_574,&l_574,&l_574,&l_574,&l_574,&l_574,&l_574,&l_574}};
        int32_t l_742 = 0xB81BF327L;
        int32_t l_743[3][5] = {{(-1L),(-1L),(-1L),(-1L),(-1L)},{(-4L),(-4L),(-4L),(-4L),(-4L)},{(-1L),(-1L),(-1L),(-1L),(-1L)}};
        uint16_t *l_753 = &g_37[2];
        uint8_t l_767[5] = {253UL,253UL,253UL,253UL,253UL};
        uint32_t l_842 = 18446744073709551611UL;
        int32_t *l_915[4][3] = {{&l_833,&g_3,&l_833},{&l_833,&l_833,&l_833},{&l_833,&g_3,&l_833},{&l_833,&l_833,&l_833}};
        int i, j;
        if ((+(0xDFL || l_739[1][1])))
        { /* block id: 357 */
            int32_t **l_741 = &l_584;
            (*l_741) = l_740[0][2];
            if (l_742)
                break;
            (*g_590) ^= (l_743[2][0] == 0xB6L);
        }
        else
        { /* block id: 361 */
            uint8_t l_754[4] = {0x15L,0x15L,0x15L,0x15L};
            int8_t *l_755 = &l_564;
            int16_t **l_760 = &g_671;
            int64_t *l_766[2][7][9] = {{{&g_89,&g_89,(void*)0,(void*)0,&g_448,&g_497[4][7][2],&g_448,(void*)0,&g_448},{&g_497[6][4][2],&g_89,&g_89,&g_89,&g_89,&g_497[6][4][2],&g_89,(void*)0,&g_497[2][5][0]},{&g_497[5][8][0],&g_497[4][7][2],&g_448,&g_89,&g_448,(void*)0,&g_448,&g_497[6][3][2],&g_89},{&g_497[2][5][0],&g_89,&g_89,(void*)0,&g_497[2][5][0],&g_497[2][5][0],(void*)0,&g_89,(void*)0},{&g_89,(void*)0,&g_448,(void*)0,&g_448,&g_497[6][3][2],&g_497[5][8][0],&g_497[5][8][0],&g_89},{&g_89,&g_89,&g_89,&g_497[5][4][0],(void*)0,&g_497[6][4][2],&g_497[5][8][0],&g_497[5][8][0],&g_497[6][4][2]},{&g_448,(void*)0,&g_89,(void*)0,&g_448,(void*)0,&g_448,&g_497[6][3][2],&g_497[5][8][0]}},{{&g_89,(void*)0,&g_497[6][4][2],&g_497[5][4][0],&g_497[6][4][2],(void*)0,&g_89,(void*)0,&g_497[2][5][0]},{&g_448,&g_497[5][8][0],&g_497[3][0][1],(void*)0,&g_448,(void*)0,&g_497[3][0][1],&g_497[5][8][0],&g_448},{(void*)0,&g_89,&g_497[5][8][0],(void*)0,&g_89,&g_497[6][4][2],&g_89,(void*)0,&g_497[5][8][0]},{&g_448,&g_497[5][8][0],(void*)0,&g_89,&g_89,&g_497[6][3][2],&g_448,&g_497[6][3][2],&g_89},{(void*)0,&g_89,&g_89,(void*)0,&g_89,&g_497[2][5][0],&g_497[5][4][0],&g_497[5][8][0],&g_497[5][4][0]},{&g_448,(void*)0,(void*)0,&g_89,&g_89,&g_89,&g_448,&g_497[5][8][0],&g_448},{&g_89,&g_497[2][5][0],&g_497[5][8][0],&g_89,&g_89,&g_497[5][8][0],&g_497[2][5][0],&g_89,&g_89}}};
            uint64_t *l_768 = &g_381[2][0];
            int32_t l_769 = 0x9E19607FL;
            int i, j, k;
            (*g_590) = (((safe_rshift_func_int8_t_s_s(((*l_755) = ((safe_sub_func_int8_t_s_s(4L, ((safe_mul_func_int16_t_s_s(((3L || p_45) != (4L <= ((void*)0 == g_750))), ((*g_520) != (*l_584)))) > (safe_mul_func_uint64_t_u_u(((((void*)0 != l_753) || l_754[0]) || 0x424F27090C69250BLL), g_184))))) , p_45)), 1)) , (*l_584)) >= p_45);
            (*g_590) |= p_45;
            (*g_590) = ((safe_div_func_uint64_t_u_u((g_37[2] <= (safe_lshift_func_uint16_t_u_s(p_45, (((0x60ECL == ((void*)0 != l_760)) <= 65535UL) ^ p_45)))), (l_769 = ((*l_768) = ((safe_rshift_func_uint64_t_u_u(((!(safe_rshift_func_uint8_t_u_u(p_45, (((*g_360) != l_766[0][4][6]) , p_45)))) && l_754[2]), l_767[1])) && 4L))))) | 9L);
        }
        for (g_93 = 0; (g_93 <= 1); g_93 += 1)
        { /* block id: 371 */
            int8_t l_789[2][4] = {{1L,0xC9L,1L,0xC9L},{1L,0xC9L,1L,0xC9L}};
            int32_t l_813 = 0xBD3C5F94L;
            uint16_t *l_819[8][9][3] = {{{&g_130,&g_176,&g_130},{&g_176,&g_176,&g_176},{&g_37[0],&g_130,&g_130},{&g_37[2],&g_37[2],&g_37[4]},{&g_130,&g_130,&g_130},{&g_37[4],&g_176,(void*)0},{&g_130,&g_176,&g_130},{&g_37[2],&g_37[4],(void*)0},{&g_37[0],&g_130,&g_130}},{{&g_176,&g_37[4],&g_37[4]},{&g_130,&g_176,&g_130},{&g_176,&g_176,&g_176},{&g_37[0],&g_130,&g_130},{&g_37[2],&g_37[2],&g_37[4]},{&g_130,&g_130,&g_130},{&g_37[4],&g_176,(void*)0},{&g_130,&g_176,&g_130},{&g_37[2],&g_37[4],(void*)0}},{{&g_37[0],&g_130,&g_130},{&g_176,&g_37[4],&g_37[4]},{&g_130,&g_176,&g_130},{&g_176,&g_176,&g_176},{&g_37[0],&g_130,&g_130},{&g_37[2],&g_37[2],&g_37[4]},{&g_130,&g_130,&g_130},{&g_37[4],&g_176,(void*)0},{&g_130,&g_176,&g_130}},{{&g_37[2],&g_37[4],(void*)0},{&g_37[0],&g_130,&g_130},{(void*)0,&g_176,&g_176},{&g_130,&g_130,&g_176},{(void*)0,&g_37[2],(void*)0},{&g_130,&g_130,&g_176},{&g_37[4],&g_37[4],&g_176},{&g_130,&g_130,&g_130},{&g_176,&g_37[2],&g_176}},{{&g_130,&g_130,&g_130},{&g_37[4],&g_176,&g_176},{&g_130,&g_130,&g_130},{(void*)0,&g_176,&g_176},{&g_130,&g_130,&g_176},{(void*)0,&g_37[2],(void*)0},{&g_130,&g_130,&g_176},{&g_37[4],&g_37[4],&g_176},{&g_130,&g_130,&g_130}},{{&g_176,&g_37[2],&g_176},{&g_130,&g_130,&g_130},{&g_37[4],&g_176,&g_176},{&g_130,&g_130,&g_130},{(void*)0,&g_176,&g_176},{&g_130,&g_130,&g_176},{(void*)0,&g_37[2],(void*)0},{&g_130,&g_130,&g_176},{&g_37[4],&g_37[4],&g_176}},{{&g_130,&g_130,&g_130},{&g_176,&g_37[2],&g_176},{&g_130,&g_130,&g_130},{&g_37[4],&g_176,&g_176},{&g_130,&g_130,&g_130},{(void*)0,&g_176,&g_176},{&g_130,&g_130,&g_176},{(void*)0,&g_37[2],(void*)0},{&g_130,&g_130,&g_176}},{{&g_37[4],&g_37[4],&g_176},{&g_130,&g_130,&g_130},{&g_176,&g_37[2],&g_176},{&g_130,&g_130,&g_130},{&g_37[4],&g_176,&g_176},{&g_130,&g_130,&g_130},{(void*)0,&g_176,&g_176},{&g_130,&g_130,&g_176},{(void*)0,&g_37[2],(void*)0}}};
            uint16_t *l_820 = &g_37[2];
            int32_t l_837 = 0x8B86D895L;
            int32_t l_841 = 6L;
            int64_t ***l_885 = &g_360;
            int16_t l_913 = 0x0E9DL;
            int i, j, k;
            if ((*g_590))
            { /* block id: 372 */
                (*l_584) = 0x062B6874L;
                if ((*g_590))
                    continue;
                for (g_176 = 0; (g_176 <= 1); g_176 += 1)
                { /* block id: 377 */
                    uint32_t l_770 = 0x7E0A1EE4L;
                    l_770--;
                }
            }
            else
            { /* block id: 380 */
                int8_t l_791 = 0x84L;
                uint16_t l_798[3];
                int8_t *l_803 = (void*)0;
                int8_t *l_804 = (void*)0;
                int8_t *l_805 = &l_791;
                uint8_t l_811 = 5UL;
                int32_t l_812 = 0xE0317336L;
                uint16_t **l_818[1];
                int i, j;
                for (i = 0; i < 3; i++)
                    l_798[i] = 0x722AL;
                for (i = 0; i < 1; i++)
                    l_818[i] = &g_277;
                for (l_608 = 0; (l_608 <= 4); l_608 += 1)
                { /* block id: 383 */
                    int64_t l_788 = 0xE8DAFBD45427CF17LL;
                    int16_t *l_792 = &g_292;
                    int16_t *l_795 = &g_292;
                    for (l_564 = 0; (l_564 <= 1); l_564 += 1)
                    { /* block id: 386 */
                        int32_t l_775[7] = {6L,6L,6L,6L,6L,6L,6L};
                        int i;
                        l_775[4] = ((void*)0 == l_773);
                    }
                    for (l_574 = 1; (l_574 >= 0); l_574 -= 1)
                    { /* block id: 391 */
                        int32_t l_787 = 0xA96EF367L;
                        int16_t **l_793 = &g_671;
                        int16_t **l_794 = &l_792;
                        int16_t *l_796 = &g_292;
                        int16_t **l_797 = &l_795;
                        int i, j;
                        l_791 |= (safe_mul_func_int8_t_s_s(((0L > (((((((*g_361) = (safe_add_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s(0x8EL, l_767[g_93])) , p_45), (safe_unary_minus_func_int32_t_s((safe_lshift_func_uint32_t_u_u(((l_787 = ((l_785 = (void*)0) == (l_786[1][3][0] = &g_360))) , (p_45 || ((-8L) <= (*g_235)))), l_788))))))) > g_381[2][1]) < l_789[1][2]) > 18446744073709551611UL) >= l_790[0][7]) & 0x0CB8L)) & p_45), 0x50L));
                        if (p_45)
                            break;
                        (*g_590) &= (((*l_794) = ((*l_793) = l_792)) == ((*l_797) = (l_796 = l_795)));
                    }
                }
                --l_798[0];
                (*l_584) &= ((((safe_div_func_int8_t_s_s(((*l_805) = (-1L)), (~(l_813 = ((*g_361) = (+(l_812 = ((-1L) | (((safe_mod_func_uint16_t_u_u(p_45, 0x44F9L)) < (0xC3CAL < (l_810[7] == l_810[2]))) , l_811))))))))) < (safe_sub_func_int16_t_s_s(((*g_671) = (safe_rshift_func_uint8_t_u_u((((l_819[5][2][0] = (g_183 , l_753)) == l_820) >= 0xE2A7L), 0))), 1L))) ^ p_45) <= p_45);
                (*g_590) = (l_789[1][3] && ((18446744073709551613UL != (0x72L & l_812)) , l_789[1][2]));
            }
            for (g_130 = 0; (g_130 >= 18); g_130++)
            { /* block id: 417 */
                int32_t *l_825 = (void*)0;
                int32_t l_830[9][7][4] = {{{0x48C1A3F3L,0xEC2C3860L,0x8174846AL,0x6D8EE001L},{8L,0xA583AC71L,(-3L),0x8DA05BE2L},{1L,2L,0xAEC328B5L,0xA583AC71L},{0xD7A3AFA6L,0x0A2107F2L,0xEA0415A9L,0xEA0415A9L},{0xDF7DF524L,0xDF7DF524L,(-3L),0L},{1L,0xD13F095AL,0x8B227DDFL,0x6CD9B573L},{0x48C1A3F3L,(-1L),0xD7A3AFA6L,0x8B227DDFL}},{{1L,(-1L),7L,0x6CD9B573L},{(-1L),0xD13F095AL,8L,0L},{0xC6DBBF7CL,0xDF7DF524L,2L,0xEA0415A9L},{8L,0x0A2107F2L,0x71950572L,0xA583AC71L},{0x8DA05BE2L,2L,0xD7A3AFA6L,0x8DA05BE2L},{0xC6DBBF7CL,0xA583AC71L,0xEA0415A9L,0x6D8EE001L},{0x6CD9B573L,0xC6DBBF7CL,0xEC2C3860L,0xD13F095AL}},{{0xD7A3AFA6L,(-3L),0x00543274L,0x63C076BBL},{0L,(-8L),0L,0x00543274L},{0xEA0415A9L,0x8174846AL,1L,0xAEC328B5L},{2L,0x71950572L,(-8L),0x8174846AL},{0xFEF214A4L,0xC6DBBF7CL,(-8L),0x5132B0EDL},{2L,0x6D8EE001L,1L,0xC55A361DL},{0xEA0415A9L,0xE11110DAL,0L,0x8B227DDFL}},{{0L,0x8B227DDFL,0x00543274L,0x59F17279L},{0xD7A3AFA6L,0x48C1A3F3L,0xEC2C3860L,0x8174846AL},{0x48C1A3F3L,0xE11110DAL,0x5132B0EDL,0x63C076BBL},{5L,2L,0xFEF214A4L,0x5132B0EDL},{0xEA0415A9L,(-8L),0x1DE32154L,0x48C1A3F3L},{8L,0x71950572L,0x71950572L,8L},{5L,0x48C1A3F3L,(-8L),0x00543274L}},{{0x8174846AL,0xE5484D10L,0xEC2C3860L,0xC55A361DL},{0x6D8EE001L,(-3L),0xFEF214A4L,0xC55A361DL},{0L,0xE5484D10L,0x59F17279L,0x00543274L},{0xAEC328B5L,0x48C1A3F3L,1L,8L},{0xD7A3AFA6L,0x71950572L,0x5132B0EDL,0x48C1A3F3L},{0xFEF214A4L,(-8L),0x73870A89L,0x5132B0EDL},{0x6D8EE001L,2L,1L,0x63C076BBL}},{{8L,0xE11110DAL,7L,0x8174846AL},{0L,0x48C1A3F3L,0x71950572L,0x59F17279L},{2L,0x8B227DDFL,0xEC2C3860L,0x8B227DDFL},{0x8B227DDFL,0xE11110DAL,0x73870A89L,0xC55A361DL},{5L,0x6D8EE001L,0x00543274L,0x5132B0EDL},{0xAEC328B5L,0xC6DBBF7CL,0x1DE32154L,0x8174846AL},{0xAEC328B5L,0x71950572L,0x00543274L,0xAEC328B5L}},{{5L,0x8174846AL,0x73870A89L,0x00543274L},{0x8B227DDFL,(-8L),0xEC2C3860L,0x63C076BBL},{2L,(-3L),0x71950572L,0xD13F095AL},{0L,0xC6DBBF7CL,7L,0x00543274L},{8L,0x8B227DDFL,1L,0xEA0415A9L},{0x6D8EE001L,0x71950572L,0x73870A89L,0x8B227DDFL},{0xFEF214A4L,0xE5484D10L,0x5132B0EDL,0x5132B0EDL}},{{0xD7A3AFA6L,0xD7A3AFA6L,1L,0xD13F095AL},{0xAEC328B5L,0xE11110DAL,0x59F17279L,0x48C1A3F3L},{0L,0x8174846AL,0xFEF214A4L,0x59F17279L},{0x6D8EE001L,0x8174846AL,0xEC2C3860L,0x48C1A3F3L},{0x8174846AL,0xE11110DAL,(-8L),0xD13F095AL},{5L,0xD7A3AFA6L,0x71950572L,0x5132B0EDL},{8L,0xE5484D10L,0x1DE32154L,0x8B227DDFL}},{{0xEA0415A9L,0x71950572L,0xFEF214A4L,0xEA0415A9L},{5L,0x8B227DDFL,0x5132B0EDL,0x00543274L},{0x48C1A3F3L,0xC6DBBF7CL,0xEC2C3860L,0xD13F095AL},{0xD7A3AFA6L,(-3L),0x00543274L,0x63C076BBL},{0L,(-8L),0L,0x00543274L},{0x5132B0EDL,7L,0xAEC328B5L,0x73870A89L},{0x71950572L,0x1DE32154L,0xDF7DF524L,7L}}};
                uint32_t l_856 = 3UL;
                int32_t l_864 = 0x8E2E3B02L;
                int i, j, k;
                if ((*l_584))
                { /* block id: 418 */
                    for (l_617 = (-14); (l_617 == 9); l_617 = safe_add_func_uint32_t_u_u(l_617, 3))
                    { /* block id: 421 */
                        (*l_584) = (*g_590);
                    }
                    return l_825;
                }
                else
                { /* block id: 425 */
                    int8_t l_827[4];
                    int32_t l_829[3];
                    int i;
                    for (i = 0; i < 4; i++)
                        l_827[i] = 0xF8L;
                    for (i = 0; i < 3; i++)
                        l_829[i] = 0x1244BAEDL;
                    (*l_584) = 0x0E0E0859L;
                    if (p_45)
                    { /* block id: 427 */
                        int32_t l_826 = 0xA0B90758L;
                        int32_t l_828[2];
                        int16_t l_840 = 2L;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_828[i] = 0L;
                        (*g_590) ^= ((l_753 == (void*)0) <= 0xCE1B060EL);
                        l_842++;
                    }
                    else
                    { /* block id: 430 */
                        uint16_t l_845 = 65534UL;
                        --l_845;
                    }
                }
                if ((*g_590))
                { /* block id: 434 */
                    int16_t l_873 = 0x399FL;
                    int32_t l_884 = 1L;
                    for (g_122 = 5; (g_122 >= 0); g_122 -= 1)
                    { /* block id: 437 */
                        int8_t *l_857 = &g_183;
                        int64_t l_863 = 0x3A4F115575B18C2ALL;
                        uint8_t *l_880 = &l_767[1];
                        int16_t *l_883[6][9] = {{(void*)0,&l_790[2][1],&l_790[0][7],&l_790[0][7],&l_790[2][1],(void*)0,&l_790[2][1],&l_790[0][7],&l_790[0][7]},{(void*)0,(void*)0,&l_790[0][7],&l_873,&l_790[0][7],(void*)0,(void*)0,&l_790[0][7],&l_873},{&l_790[0][4],&l_790[2][1],&l_790[0][4],(void*)0,(void*)0,&l_790[0][4],&l_790[2][1],&l_790[0][4],(void*)0},{&l_790[0][7],&l_790[0][7],&l_790[0][7],&l_790[0][7],&l_873,&l_790[0][7],&l_790[0][7],&l_790[0][7],&l_790[0][7]},{&l_790[0][7],(void*)0,&l_790[0][7],(void*)0,&l_790[0][7],&l_790[0][7],(void*)0,&l_790[0][7],(void*)0},{&l_790[0][7],&l_873,&l_873,&l_873,&l_873,&l_790[0][7],&l_873,&l_873,&l_873}};
                        int32_t l_886 = 1L;
                        int i, j;
                        g_64[g_122] = (safe_rshift_func_int64_t_s_u((safe_add_func_int32_t_s_s((safe_rshift_func_uint64_t_u_u(((**l_773) = (g_64[g_122] && g_64[g_122])), 18)), (p_45 != ((safe_sub_func_uint16_t_u_u((((**g_360) <= (((((0xADL == l_856) < ((void*)0 == l_857)) & (safe_add_func_uint16_t_u_u(((l_864 = (~(safe_lshift_func_uint16_t_u_s((l_863 > 1UL), p_45)))) , p_45), (*l_584)))) ^ (*g_590)) && (-1L))) & p_45), 0x2BE6L)) && 1UL)))), l_813));
                        (*g_590) &= (((0UL > (*l_584)) <= ((*g_277) ^= p_45)) > (p_45 & (~((((((*g_671) &= (!p_45)) == (((l_813 != (((g_345[2] = (safe_div_func_uint64_t_u_u((g_89 && (safe_lshift_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u(((l_886 = (l_873 , (safe_rshift_func_uint64_t_u_s(((*l_774) = (+((+(l_884 = ((--(*g_235)) ^ (++(*l_880))))) , (l_786[0][4][4] != l_885)))), 50)))) , 0x4B82L), p_45)) >= p_45), 6))), (*l_584)))) > p_45) != 18446744073709551615UL)) , p_45) < p_45)) | p_45) < 0x9037392CA8A1D418LL) , 0x3E6E6578L))));
                        if ((*g_590))
                            break;
                    }
                }
                else
                { /* block id: 452 */
                    const uint32_t l_893 = 1UL;
                    int64_t **l_894[1][10] = {{&g_361,&g_361,&g_361,&g_361,&g_361,&g_361,&g_361,&g_361,&g_361,&g_361}};
                    int8_t *l_895 = (void*)0;
                    int8_t *l_896 = &l_834;
                    int i, j;
                    (*l_584) = (((safe_lshift_func_int32_t_s_s(((safe_add_func_uint8_t_u_u((safe_add_func_uint8_t_u_u((l_893 && ((*l_896) = (l_894[0][8] != (void*)0))), (((safe_add_func_int64_t_s_s((*g_361), ((safe_mul_func_uint64_t_u_u(g_347, (safe_mod_func_int16_t_s_s(((safe_sub_func_int32_t_s_s(l_789[1][2], (safe_add_func_uint8_t_u_u((((safe_unary_minus_func_uint8_t_u(((!(((*l_584) < (p_45 , (((safe_add_func_int64_t_s_s((((safe_add_func_int16_t_s_s((0x5560FAE4L | p_45), 0xEEEAL)) < (*g_235)) >= 0UL), p_45)) > p_45) == l_893))) > p_45)) > p_45))) , (-1L)) , l_913), 249UL)))) , p_45), (*g_520))))) ^ (*g_520)))) | l_813) , (*l_584)))), 0UL)) & 0L), 31)) < (*l_584)) ^ p_45);
                    for (g_448 = 1; (g_448 >= 0); g_448 -= 1)
                    { /* block id: 457 */
                        uint16_t * volatile * volatile * volatile **l_914 = &g_663;
                        l_914 = &g_663;
                        return &g_64[4];
                    }
                    l_915[3][0] = &l_830[2][6][1];
                }
            }
            (*g_590) |= ((safe_sub_func_int64_t_s_s((2UL || l_918), 0xC70DD7BBD98953E2LL)) & ((*l_820)--));
        }
    }
    return &g_122;
}


/* ------------------------------------------ */
/* 
 * reads : g_519 g_520 g_176 g_277 g_381 g_3 g_360 g_361 g_64 g_89 g_122 g_40
 * writes: g_308 g_307 g_519 g_276 g_89 g_176 g_40
 */
static uint32_t  func_50(uint32_t  p_51, uint32_t  p_52)
{ /* block id: 259 */
    int8_t **l_504[8][5] = {{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307},{&g_307,&g_307,&g_307,&g_307,&g_307}};
    int32_t l_505 = 0xFD1D986FL;
    const uint16_t ***l_521 = &g_519;
    uint16_t ***l_526 = &g_276[0][4];
    int32_t l_527 = 0xA2F3E26EL;
    int32_t *l_528 = &l_527;
    int32_t *l_529 = &g_64[1];
    int32_t *l_530 = &g_64[6];
    int32_t *l_531 = &g_64[4];
    int32_t *l_532[8] = {&g_122,&g_122,&g_122,&g_122,&g_122,&g_122,&g_122,&g_122};
    uint8_t l_533 = 1UL;
    uint64_t l_542 = 1UL;
    int32_t ** const *l_550 = &g_112;
    int i, j;
    l_505 = ((((g_308 = (void*)0) != (void*)0) >= (((g_307 = (void*)0) == (l_505 , &g_183)) ^ (safe_div_func_uint64_t_u_u((safe_add_func_int64_t_s_s(((**g_360) = (safe_lshift_func_uint8_t_u_s(((safe_sub_func_uint8_t_u_u((~((safe_mul_func_uint64_t_u_u(((((((l_527 = (safe_rshift_func_int8_t_s_u((((*l_521) = g_519) != ((*l_526) = (((*g_520) <= (safe_sub_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((p_51 , (l_505 ^ 0xF07BL)), l_505)), (*g_277)))) , &g_277))), 2))) == 1UL) ^ g_381[2][0]) , 4294967295UL) <= p_52) & l_505), p_51)) > p_52)), 0x02L)) & g_3), 4))), 0x08D63B1437AB88D4LL)), 0x8148308D4F6ADA81LL)))) > p_52);
    (*l_528) = 0x50E47745L;
    ++l_533;
    g_40 |= (safe_sub_func_uint8_t_u_u(p_51, ((safe_mul_func_int64_t_s_s((safe_unary_minus_func_int8_t_s((+l_542))), 0x0CB4EE2C292A9E3ALL)) , (((*l_530) <= ((((((safe_div_func_int8_t_s_s(p_51, (safe_sub_func_int64_t_s_s((*g_361), (((((((*l_528) &= ((void*)0 == &l_542)) , ((((safe_unary_minus_func_uint8_t_u((safe_rshift_func_uint64_t_u_s((((((*g_277) = (*g_277)) < (*l_528)) < p_51) , 0x7BE077AB678E71ECLL), 47)))) , (void*)0) != l_550) <= p_52)) , 9L) , p_52) | (**g_360)) ^ (*l_529)))))) , 0x16L) , (*l_528)) ^ p_51) ^ (-10L)) , g_122)) ^ 6UL))));
    return p_51;
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_40 g_122
 * writes: g_40
 */
static uint32_t  func_53(int32_t * p_54, int32_t * const  p_55)
{ /* block id: 11 */
    int8_t l_118 = 0xC9L;
    int32_t *l_121[9][1][6] = {{{&g_40,&g_64[6],(void*)0,&g_64[3],&g_122,&g_3}},{{&g_122,&g_122,&g_40,(void*)0,&g_122,(void*)0}},{{&g_64[3],&g_64[3],&g_64[3],&g_64[2],(void*)0,&g_122}},{{&g_122,&g_122,(void*)0,&g_64[1],(void*)0,&g_122}},{{&g_122,(void*)0,&g_122,&g_64[1],&g_64[2],&g_64[2]}},{{&g_122,&g_40,&g_122,&g_64[2],&g_64[2],&g_122}},{{&g_64[3],&g_64[3],&g_3,(void*)0,&g_122,&g_122}},{{&g_122,(void*)0,&g_122,&g_64[3],&g_122,&g_3}},{{&g_40,&g_122,&g_122,&g_122,&g_64[3],&g_122}}};
    uint32_t l_123 = 0x5D77E079L;
    uint16_t *l_133 = &g_37[2];
    int32_t * const *l_149 = &l_121[1][0][1];
    int32_t * const **l_148 = &l_149;
    int8_t l_168 = 0xA2L;
    int8_t l_201 = 0x2CL;
    uint32_t l_204 = 0x09CE9216L;
    uint32_t l_225 = 0x4F88DFB8L;
    uint8_t *l_238 = &g_236;
    uint32_t l_355 = 1UL;
    int64_t *l_389 = &g_89;
    uint32_t *l_407 = &l_204;
    uint32_t *l_408 = &g_93;
    uint64_t *l_411[1][6][7] = {{{&g_347,&g_347,&g_347,&g_381[1][0],&g_381[1][0],&g_347,&g_347},{&g_381[2][0],&g_347,(void*)0,&g_347,&g_381[1][0],(void*)0,&g_381[2][0]},{&g_381[2][0],(void*)0,&g_347,(void*)0,&g_347,&g_381[2][0],&g_347},{(void*)0,&g_381[2][0],(void*)0,(void*)0,&g_381[2][0],(void*)0,&g_347},{&g_381[2][0],&g_347,(void*)0,&g_347,&g_347,(void*)0,&g_381[2][0]},{(void*)0,&g_347,&g_381[2][0],(void*)0,&g_347,&g_381[2][0],&g_347}}};
    uint16_t l_412 = 0xD24CL;
    int8_t **l_413 = &g_308;
    int8_t l_449[6] = {1L,1L,1L,1L,1L,1L};
    uint16_t ***l_499 = &g_276[0][4];
    int i, j, k;
    (*p_55) &= g_37[3];
    return g_122;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2[i][j], "g_2[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_37[i], "g_37[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_40, "g_40", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_64[i], "g_64[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_130, "g_130", print_hash_value);
    transparent_crc(g_176, "g_176", print_hash_value);
    transparent_crc(g_183, "g_183", print_hash_value);
    transparent_crc(g_184, "g_184", print_hash_value);
    transparent_crc(g_185, "g_185", print_hash_value);
    transparent_crc(g_236, "g_236", print_hash_value);
    transparent_crc(g_292, "g_292", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_345[i], "g_345[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_347, "g_347", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_381[i][j], "g_381[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_448, "g_448", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_497[i][j][k], "g_497[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_572, "g_572", print_hash_value);
    transparent_crc(g_951, "g_951", print_hash_value);
    transparent_crc(g_970, "g_970", print_hash_value);
    transparent_crc(g_1049, "g_1049", print_hash_value);
    transparent_crc(g_1050, "g_1050", print_hash_value);
    transparent_crc(g_1295, "g_1295", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1375[i][j][k], "g_1375[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1548, "g_1548", print_hash_value);
    transparent_crc(g_1621, "g_1621", print_hash_value);
    transparent_crc(g_1648, "g_1648", print_hash_value);
    transparent_crc(g_1649, "g_1649", print_hash_value);
    transparent_crc(g_1650, "g_1650", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1651[i], "g_1651[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1652, "g_1652", print_hash_value);
    transparent_crc(g_1653, "g_1653", print_hash_value);
    transparent_crc(g_1654, "g_1654", print_hash_value);
    transparent_crc(g_1655, "g_1655", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1656[i], "g_1656[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1657, "g_1657", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1658[i][j][k], "g_1658[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1659, "g_1659", print_hash_value);
    transparent_crc(g_1660, "g_1660", print_hash_value);
    transparent_crc(g_1706, "g_1706", print_hash_value);
    transparent_crc(g_1724, "g_1724", print_hash_value);
    transparent_crc(g_1831, "g_1831", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1906[i], "g_1906[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1928, "g_1928", print_hash_value);
    transparent_crc(g_1940, "g_1940", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2009[i], "g_2009[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2175, "g_2175", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2219[i][j], "g_2219[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2544, "g_2544", print_hash_value);
    transparent_crc(g_2661, "g_2661", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 703
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 101
   depth: 2, occurrence: 27
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
   depth: 5, occurrence: 3
   depth: 6, occurrence: 2
   depth: 15, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1
   depth: 40, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 519

XXX times a variable address is taken: 1399
XXX times a pointer is dereferenced on RHS: 377
breakdown:
   depth: 1, occurrence: 306
   depth: 2, occurrence: 61
   depth: 3, occurrence: 10
XXX times a pointer is dereferenced on LHS: 367
breakdown:
   depth: 1, occurrence: 333
   depth: 2, occurrence: 27
   depth: 3, occurrence: 6
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 54
XXX times a pointer is compared with address of another variable: 18
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 7340

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1758
   level: 2, occurrence: 375
   level: 3, occurrence: 201
   level: 4, occurrence: 46
   level: 5, occurrence: 13
XXX number of pointers point to pointers: 265
XXX number of pointers point to scalars: 254
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.9
XXX average alias set size: 1.5

XXX times a non-volatile is read: 2298
XXX times a non-volatile is write: 1144
XXX times a volatile is read: 36
XXX    times read thru a pointer: 13
XXX times a volatile is write: 4
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 476
XXX percentage of non-volatile access: 98.9

XXX forward jumps: 0
XXX backward jumps: 8

XXX stmts: 106
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 14
   depth: 2, occurrence: 17
   depth: 3, occurrence: 11
   depth: 4, occurrence: 14
   depth: 5, occurrence: 16

XXX percentage a fresh-made variable is used: 18.1
XXX percentage an existing variable is used: 81.9
XXX total OOB instances added: 0
********************* end of statistics **********************/


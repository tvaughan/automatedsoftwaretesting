/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3628973994
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int16_t g_2[5] = {0x59C1L,0x59C1L,0x59C1L,0x59C1L,0x59C1L};
static uint32_t g_10[8] = {0x2C87A752L,1UL,1UL,0x2C87A752L,1UL,1UL,0x2C87A752L,1UL};
static int32_t * volatile g_26[3] = {(void*)0,(void*)0,(void*)0};
static int32_t g_28[5][10][5] = {{{7L,0x2C3B03C3L,(-4L),7L,7L},{0xDEB256CDL,0xF162C80FL,0xDEB256CDL,0x1580EB60L,0xE45DB0FFL},{7L,8L,1L,0x2C3B03C3L,8L},{0L,0xF162C80FL,0x31E34F9AL,0xF162C80FL,0L},{8L,0x2C3B03C3L,1L,8L,7L},{0xE45DB0FFL,0x1580EB60L,0xDEB256CDL,0xF162C80FL,0xDEB256CDL},{8L,8L,0x2C3B03C3L,1L,8L},{0x31E34F9AL,0x1580EB60L,0x31E34F9AL,(-4L),0L},{8L,(-4L),(-4L),8L,0x46C8C5CEL},{0xDEB256CDL,0x1580EB60L,0xE45DB0FFL,0x1580EB60L,0xDEB256CDL}},{{0x46C8C5CEL,8L,(-4L),(-4L),8L},{0L,(-4L),0x31E34F9AL,0x1580EB60L,0x31E34F9AL},{8L,1L,0x2C3B03C3L,8L,8L},{0xB2B0402AL,0x1580EB60L,0xB2B0402AL,(-4L),0xDEB256CDL},{8L,0x46C8C5CEL,(-4L),1L,0x46C8C5CEL},{0L,0x1580EB60L,0L,0x1580EB60L,0L},{0x46C8C5CEL,1L,(-4L),0x46C8C5CEL,8L},{0xDEB256CDL,(-4L),0xB2B0402AL,0x1580EB60L,0xB2B0402AL},{8L,8L,0x2C3B03C3L,1L,8L},{0x31E34F9AL,0x1580EB60L,0x31E34F9AL,(-4L),0L}},{{8L,(-4L),(-4L),8L,0x46C8C5CEL},{0xDEB256CDL,0x1580EB60L,0xE45DB0FFL,0x1580EB60L,0xDEB256CDL},{0x46C8C5CEL,8L,(-4L),(-4L),8L},{0L,(-4L),0x31E34F9AL,0x1580EB60L,0x31E34F9AL},{8L,1L,0x2C3B03C3L,8L,8L},{0xB2B0402AL,0x1580EB60L,0xB2B0402AL,(-4L),0xDEB256CDL},{8L,0x46C8C5CEL,(-4L),1L,0x46C8C5CEL},{0L,0x1580EB60L,0L,0x1580EB60L,0L},{0x46C8C5CEL,1L,(-4L),0x46C8C5CEL,8L},{0xDEB256CDL,(-4L),0xB2B0402AL,0x1580EB60L,0xB2B0402AL}},{{8L,8L,0x2C3B03C3L,1L,8L},{0x31E34F9AL,0x1580EB60L,0x31E34F9AL,(-4L),0L},{8L,(-4L),(-4L),8L,0x46C8C5CEL},{0xDEB256CDL,0x1580EB60L,0xE45DB0FFL,0x1580EB60L,0xDEB256CDL},{0x46C8C5CEL,8L,(-4L),(-4L),8L},{0L,(-4L),0x31E34F9AL,0x1580EB60L,0x31E34F9AL},{8L,1L,0x2C3B03C3L,8L,8L},{0xB2B0402AL,0x1580EB60L,0xB2B0402AL,(-4L),0xDEB256CDL},{8L,0x46C8C5CEL,(-4L),1L,0x46C8C5CEL},{0L,0x1580EB60L,0L,0x1580EB60L,0L}},{{0x46C8C5CEL,1L,(-4L),0x46C8C5CEL,8L},{0xDEB256CDL,(-4L),0xB2B0402AL,0x1580EB60L,0xB2B0402AL},{8L,8L,0x2C3B03C3L,1L,8L},{0x31E34F9AL,0x1580EB60L,0x31E34F9AL,(-4L),0L},{8L,0x2C3B03C3L,0x2C3B03C3L,0x46C8C5CEL,7L},{0xB2B0402AL,(-4L),0xDEB256CDL,(-4L),0xB2B0402AL},{7L,0x46C8C5CEL,0x2C3B03C3L,0x2C3B03C3L,0x46C8C5CEL},{0x31E34F9AL,0xF162C80FL,0L,(-4L),0L},{0x46C8C5CEL,(-4L),1L,0x46C8C5CEL,0x46C8C5CEL},{0xE45DB0FFL,(-4L),0xE45DB0FFL,0xF162C80FL,0xB2B0402AL}}};
static int32_t * const  volatile g_27 = &g_28[0][7][0];/* VOLATILE GLOBAL g_27 */


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static int16_t  func_11(int64_t  p_12, uint64_t  p_13, int8_t  p_14, int8_t  p_15, int64_t  p_16);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_10 g_27 g_28
 * writes: g_10 g_28
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    uint16_t l_19 = 0x7071L;
    uint32_t *l_20 = (void*)0;
    int32_t l_21 = 0x488A9733L;
    uint16_t l_22 = 1UL;
    int32_t *l_25 = &l_21;
    (*g_27) |= ((g_2[2] > (((safe_rshift_func_int8_t_s_s(g_2[1], 3)) ^ (safe_lshift_func_uint32_t_u_s(g_2[2], 9))) && (~((*l_25) = (safe_div_func_uint8_t_u_u(((g_10[4] = ((-9L) && (g_2[2] , 7UL))) && ((g_2[0] < ((func_11((safe_div_func_uint32_t_u_u((g_10[4] != ((l_21 = (0x5F84255EL || l_19)) , l_22)), g_2[2])), l_22, g_10[3], g_2[2], g_10[4]) >= l_22) >= 0xBEL)) == 8UL)), g_2[4])))))) >= g_2[1]);
    return g_2[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_2
 * writes:
 */
static int16_t  func_11(int64_t  p_12, uint64_t  p_13, int8_t  p_14, int8_t  p_15, int64_t  p_16)
{ /* block id: 3 */
    for (p_16 = (-2); (p_16 < 6); p_16++)
    { /* block id: 6 */
        return p_12;
    }
    return g_2[2];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_10[i], "g_10[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_28[i][j][k], "g_28[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 5
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 27
breakdown:
   depth: 1, occurrence: 4
   depth: 2, occurrence: 1
   depth: 27, occurrence: 1

XXX total number of pointers: 4

XXX times a variable address is taken: 2
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 2
breakdown:
   depth: 1, occurrence: 2
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 8

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 4
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 4
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 50
XXX average alias set size: 1

XXX times a non-volatile is read: 20
XXX times a non-volatile is write: 6
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 1
XXX percentage of non-volatile access: 96.3

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 5
XXX max block depth: 1
breakdown:
   depth: 0, occurrence: 4
   depth: 1, occurrence: 1

XXX percentage a fresh-made variable is used: 19.2
XXX percentage an existing variable is used: 80.8
XXX total OOB instances added: 0
********************* end of statistics **********************/


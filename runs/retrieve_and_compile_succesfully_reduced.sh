#!/bin/bash
set -e
SSH="tvaughan@ast-epyc2.inf.ethz.ch"

RUN_PATH=$1
if [ ! -d "$RUN_PATH" ] || [ ! -f "${RUN_PATH}/run.log" ]; then
    echo "Please provide the path as the first argument, with run.log inside the folder";
    exit 1;
fi

readarray -t files  < <(cat "${RUN_PATH}/run.log" | grep "\*\*" | sed 's/^\s*[*]* \(.*\) [*]*/\1/g' )

REMOTE_DIR=$(ssh ${SSH} mktemp -d)
for file in ${files[@]}; do
    filename=$(basename -- $file)
    remote_file="${REMOTE_DIR}/${filename}"

    echo "Processing ${filename}"

    scp "${RUN_PATH}/${filename}" ${SSH}:$REMOTE_DIR;

    ssh ${SSH} gcc-11 -O3 -no-pie -mavx -ftree-vectorize -I/usr/include/csmith/ -o "${remote_file%.*}-gcc" $remote_file
    ssh ${SSH} clang-17 -O3 -no-pie -mavx -ftree-vectorize -I/usr/include/csmith/ -o "${remote_file%.*}-clang" $remote_file 

    mkdir -p ${RUN_PATH}/successes/
    scp ${SSH}:"${remote_file%.*}-gcc" ${RUN_PATH}/successes/
    scp ${SSH}:"${remote_file%.*}-clang" ${RUN_PATH}/successes/
    cp "${RUN_PATH}/${filename}" ${RUN_PATH}/successes/
done;

/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1131941929
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   unsigned f0 : 19;
   volatile signed f1 : 30;
   signed f2 : 9;
   unsigned f3 : 16;
   volatile unsigned f4 : 1;
   unsigned f5 : 22;
};
#pragma pack(pop)

union U1 {
   float  f0;
   volatile uint32_t  f1;
   const uint16_t  f2;
};

union U2 {
   int32_t  f0;
   int8_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static uint64_t g_10 = 0xD3F5047C4D1454BCLL;
static float g_12 = 0x9.849B76p-3;
static float * volatile g_11[5][4][2] = {{{(void*)0,&g_12},{&g_12,&g_12},{&g_12,&g_12},{(void*)0,&g_12}},{{&g_12,&g_12},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_12}},{{&g_12,&g_12},{(void*)0,&g_12},{&g_12,&g_12},{&g_12,&g_12}},{{(void*)0,&g_12},{&g_12,&g_12},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,&g_12},{&g_12,&g_12},{(void*)0,&g_12},{&g_12,&g_12}}};
static float * const  volatile g_13 = &g_12;/* VOLATILE GLOBAL g_13 */
static volatile int32_t g_16 = 0L;/* VOLATILE GLOBAL g_16 */
static volatile int32_t g_17 = 0L;/* VOLATILE GLOBAL g_17 */
static int32_t g_21[8] = {6L,6L,6L,6L,6L,6L,6L,6L};
static int32_t * volatile g_20[1][2][6] = {{{(void*)0,&g_21[6],(void*)0,(void*)0,&g_21[6],(void*)0},{(void*)0,&g_21[6],(void*)0,(void*)0,&g_21[6],(void*)0}}};
static int16_t g_56 = 0x94A9L;
static const int32_t g_69[2] = {0xF6B3DAD9L,0xF6B3DAD9L};
static const int32_t *g_68 = &g_69[1];
static const int32_t g_73 = (-3L);
static const int32_t *g_72 = &g_73;
static int8_t g_90 = 0L;
static int8_t g_93 = 0x6BL;
static uint8_t g_103 = 252UL;
static float g_107 = 0x6.6p+1;
static float g_110 = 0x6.39B44Ap-83;
static float * const g_109 = &g_110;
static float * const *g_108 = &g_109;
static int32_t * const g_114 = &g_21[7];
static volatile union U1 g_115[9][4][2] = {{{{0x6.6E099Fp-91},{0x0.3F40BDp+44}},{{0x6.6E099Fp-91},{0x3.3p+1}},{{0x8.CC3AA3p+16},{0x0.0p+1}},{{0x3.3p+1},{0x7.5p+1}}},{{{-0x7.3p-1},{0x8.CC3AA3p+16}},{{-0x1.Fp+1},{-0x10.2p-1}},{{-0x10.2p-1},{0xE.EC887Cp+3}},{{0x0.8p-1},{0x7.6F079Cp-29}}},{{{0xA.05D9C6p+25},{-0x7.3p-1}},{{0x4.4A690Ep-19},{0x4.1CAE3Ap-98}},{{0x7.5p+1},{0x4.1CAE3Ap-98}},{{0x4.4A690Ep-19},{-0x7.3p-1}}},{{{0xA.05D9C6p+25},{0x7.6F079Cp-29}},{{0x0.8p-1},{0xE.EC887Cp+3}},{{-0x10.2p-1},{-0x10.2p-1}},{{-0x1.Fp+1},{0x8.CC3AA3p+16}}},{{{-0x7.3p-1},{0x7.5p+1}},{{0x3.3p+1},{0x0.0p+1}},{{0x8.CC3AA3p+16},{0x3.3p+1}},{{0x6.6E099Fp-91},{0x0.3F40BDp+44}}},{{{0x6.6E099Fp-91},{0x3.3p+1}},{{0x8.CC3AA3p+16},{0x0.0p+1}},{{0x3.3p+1},{0x7.5p+1}},{{-0x7.3p-1},{0x8.CC3AA3p+16}}},{{{-0x1.Fp+1},{-0x10.2p-1}},{{-0x10.2p-1},{0xE.EC887Cp+3}},{{0x0.8p-1},{0x7.6F079Cp-29}},{{0xA.05D9C6p+25},{-0x7.3p-1}}},{{{0x4.4A690Ep-19},{0x4.1CAE3Ap-98}},{{0x7.5p+1},{0x4.1CAE3Ap-98}},{{0x4.4A690Ep-19},{-0x7.3p-1}},{{0xA.05D9C6p+25},{0x7.6F079Cp-29}}},{{{0x0.8p-1},{0xE.EC887Cp+3}},{{-0x10.2p-1},{-0x10.2p-1}},{{-0x1.Fp+1},{0x8.CC3AA3p+16}},{{-0x7.3p-1},{0x7.5p+1}}}};
static uint16_t g_131[9][10] = {{65530UL,1UL,0xED9FL,0x0CEAL,0xED9FL,1UL,65530UL,0xED8DL,0x0CEAL,0x0F79L},{0UL,1UL,0x0F79L,65531UL,0x66F4L,8UL,65530UL,0xED9FL,65531UL,0xED8DL},{1UL,1UL,6UL,65535UL,0x0F79L,65530UL,65530UL,0x0F79L,65535UL,6UL},{1UL,1UL,0x66F4L,0xA911L,0xED8DL,0UL,65530UL,6UL,0xA911L,0xED9FL},{8UL,1UL,0xED8DL,65535UL,6UL,1UL,65530UL,0x66F4L,65535UL,0x66F4L},{65530UL,1UL,0xED9FL,0x0CEAL,0xED9FL,1UL,65530UL,0xED8DL,0x0CEAL,0x0F79L},{0UL,1UL,0x0F79L,65531UL,0x66F4L,8UL,65530UL,0xED9FL,65531UL,0xED8DL},{1UL,1UL,6UL,65535UL,0x0F79L,65530UL,65530UL,0x0F79L,65535UL,6UL},{1UL,1UL,0x66F4L,0xA911L,0xED8DL,0UL,65530UL,6UL,0xA911L,0xED9FL}};
static const volatile union U1 g_143[10] = {{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60},{0x1.A9C47Ep+60}};
static union U2 g_144 = {5L};
static struct S0 g_147 = {536,13147,8,131,0,5};/* VOLATILE GLOBAL g_147 */
static volatile union U1 g_162[8] = {{0x0.0p+1},{0x0.0p+1},{0x0.0p+1},{0x0.0p+1},{0x0.0p+1},{0x0.0p+1},{0x0.0p+1},{0x0.0p+1}};
static uint16_t g_184 = 0x0362L;
static uint8_t g_206 = 253UL;
static int64_t g_208 = 0xA3AA6587CEBA9940LL;
static int32_t *g_225 = &g_144.f0;
static const float g_244 = 0x0.856D30p-82;
static union U1 g_249 = {0x0.4p+1};/* VOLATILE GLOBAL g_249 */
static uint32_t g_266 = 4294967295UL;
static volatile union U1 *g_291 = (void*)0;
static volatile union U1 ** volatile g_290 = &g_291;/* VOLATILE GLOBAL g_290 */
static struct S0 *g_297 = &g_147;
static volatile int8_t g_329 = 0x61L;/* VOLATILE GLOBAL g_329 */
static volatile int8_t * volatile g_328[8][10] = {{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0},{(void*)0,&g_329,&g_329,(void*)0,(void*)0,(void*)0,&g_329,&g_329,(void*)0,(void*)0}};
static volatile int8_t * const  volatile *g_327 = &g_328[7][5];
static volatile int8_t * const  volatile ** volatile g_326 = &g_327;/* VOLATILE GLOBAL g_326 */
static uint64_t *g_364 = &g_10;
static int32_t ** volatile g_371[2] = {&g_225,&g_225};
static int32_t ** volatile g_372[10] = {&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225};
static volatile struct S0 g_404 = {67,-7022,19,1,0,1848};/* VOLATILE GLOBAL g_404 */
static volatile float *g_412 = &g_115[4][3][1].f0;
static volatile float * volatile *g_411 = &g_412;
static volatile float * volatile **g_410 = &g_411;
static int8_t *g_422 = &g_90;
static int8_t **g_421 = &g_422;
static union U1 g_430 = {0x8.9p+1};/* VOLATILE GLOBAL g_430 */
static union U1 g_457 = {0xD.A9DA4Ep+91};/* VOLATILE GLOBAL g_457 */
static uint64_t g_461 = 18446744073709551608UL;
static uint64_t g_487 = 0UL;
static int16_t g_488 = 7L;
static const volatile struct S0 g_491[7][8] = {{{378,-13001,14,198,0,597},{378,-13001,14,198,0,597},{464,32166,-7,191,0,809},{112,9131,-5,6,0,937},{464,32166,-7,191,0,809},{378,-13001,14,198,0,597},{378,-13001,14,198,0,597},{464,32166,-7,191,0,809}},{{708,24616,-15,208,0,1521},{464,32166,-7,191,0,809},{464,32166,-7,191,0,809},{708,24616,-15,208,0,1521},{114,18519,5,158,0,1297},{708,24616,-15,208,0,1521},{464,32166,-7,191,0,809},{464,32166,-7,191,0,809}},{{464,32166,-7,191,0,809},{114,18519,5,158,0,1297},{112,9131,-5,6,0,937},{112,9131,-5,6,0,937},{114,18519,5,158,0,1297},{464,32166,-7,191,0,809},{114,18519,5,158,0,1297},{112,9131,-5,6,0,937}},{{708,24616,-15,208,0,1521},{114,18519,5,158,0,1297},{708,24616,-15,208,0,1521},{464,32166,-7,191,0,809},{464,32166,-7,191,0,809},{708,24616,-15,208,0,1521},{114,18519,5,158,0,1297},{708,24616,-15,208,0,1521}},{{378,-13001,14,198,0,597},{464,32166,-7,191,0,809},{112,9131,-5,6,0,937},{464,32166,-7,191,0,809},{378,-13001,14,198,0,597},{378,-13001,14,198,0,597},{464,32166,-7,191,0,809},{112,9131,-5,6,0,937}},{{378,-13001,14,198,0,597},{378,-13001,14,198,0,597},{464,32166,-7,191,0,809},{112,9131,-5,6,0,937},{464,32166,-7,191,0,809},{378,-13001,14,198,0,597},{378,-13001,14,198,0,597},{464,32166,-7,191,0,809}},{{708,24616,-15,208,0,1521},{464,32166,-7,191,0,809},{464,32166,-7,191,0,809},{708,24616,-15,208,0,1521},{114,18519,5,158,0,1297},{708,24616,-15,208,0,1521},{464,32166,-7,191,0,809},{464,32166,-7,191,0,809}}};
static union U1 g_505[1] = {{0x0.2p+1}};
static volatile struct S0 g_531 = {58,13663,-9,152,0,464};/* VOLATILE GLOBAL g_531 */
static volatile uint32_t g_548 = 18446744073709551615UL;/* VOLATILE GLOBAL g_548 */
static int32_t ** volatile g_557 = &g_225;/* VOLATILE GLOBAL g_557 */
static struct S0 g_574[7] = {{32,15331,7,150,0,310},{32,15331,7,150,0,310},{32,15331,7,150,0,310},{32,15331,7,150,0,310},{32,15331,7,150,0,310},{32,15331,7,150,0,310},{32,15331,7,150,0,310}};
static struct S0 *** volatile g_639 = (void*)0;/* VOLATILE GLOBAL g_639 */
static int8_t g_652[10][9] = {{0L,1L,1L,0x0DL,0x02L,0x27L,0xA6L,0x02L,(-1L)},{(-9L),0x62L,4L,(-8L),0x27L,1L,(-8L),0x10L,0x10L},{(-8L),0x62L,0x10L,0xA6L,0x10L,0x62L,(-8L),0xB1L,1L},{(-8L),1L,5L,0x05L,0L,9L,1L,0x17L,5L},{0x02L,5L,1L,0x7AL,(-10L),(-6L),(-1L),0L,9L},{0x02L,1L,(-6L),0x95L,1L,5L,0x7AL,(-7L),0xEEL},{4L,0x17L,1L,0x27L,1L,1L,0x27L,1L,0x17L},{1L,0x37L,5L,1L,(-10L),0x17L,0x7AL,1L,0x17L},{1L,1L,(-7L),0x62L,0L,0x37L,(-1L),5L,0xEEL},{(-1L),0x37L,0L,0x62L,(-7L),1L,1L,0x37L,9L}};
static int32_t ** volatile g_656 = &g_225;/* VOLATILE GLOBAL g_656 */
static volatile union U1 g_662 = {0x1.E1EDD5p+70};/* VOLATILE GLOBAL g_662 */
static int32_t ** volatile g_681 = &g_225;/* VOLATILE GLOBAL g_681 */
static union U1 g_685 = {0x6.Ap-1};/* VOLATILE GLOBAL g_685 */
static float *g_691 = &g_505[0].f0;
static union U1 g_692 = {-0x5.5p-1};/* VOLATILE GLOBAL g_692 */
static const volatile struct S0 g_694 = {379,-5972,-19,220,0,233};/* VOLATILE GLOBAL g_694 */
static union U1 *g_697[1] = {&g_505[0]};
static union U1 ** volatile g_696 = &g_697[0];/* VOLATILE GLOBAL g_696 */
static const union U1 g_698 = {0xF.593C16p-73};/* VOLATILE GLOBAL g_698 */
static volatile struct S0 g_702[1] = {{290,9029,-18,124,0,648}};
static struct S0 g_706 = {627,256,6,204,0,1380};/* VOLATILE GLOBAL g_706 */
static int64_t g_733 = 0xFC398A9D9FEA173FLL;
static const union U1 *g_784 = (void*)0;
static struct S0 g_790 = {689,-32492,21,249,0,253};/* VOLATILE GLOBAL g_790 */
static uint16_t * const g_825 = &g_131[4][0];
static uint16_t * const *g_824[10] = {&g_825,&g_825,&g_825,&g_825,&g_825,&g_825,&g_825,&g_825,&g_825,&g_825};
static uint16_t * const **g_823 = &g_824[9];
static uint16_t * const *** volatile g_822 = &g_823;/* VOLATILE GLOBAL g_822 */
static uint16_t *g_829 = &g_184;
static uint16_t **g_828[3] = {&g_829,&g_829,&g_829};
static uint16_t ***g_827[9][4] = {{(void*)0,&g_828[0],&g_828[0],(void*)0},{&g_828[0],&g_828[0],&g_828[0],&g_828[0]},{(void*)0,&g_828[0],&g_828[1],&g_828[0]},{&g_828[0],&g_828[0],&g_828[0],&g_828[0]},{&g_828[0],&g_828[0],&g_828[0],&g_828[0]},{&g_828[0],&g_828[0],&g_828[1],(void*)0},{&g_828[0],&g_828[0],&g_828[0],&g_828[0]},{&g_828[0],(void*)0,&g_828[0],&g_828[0]},{&g_828[0],(void*)0,&g_828[1],&g_828[0]}};
static uint16_t ****g_826 = &g_827[7][0];
static uint16_t ** const *g_836[9][9][1] = {{{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]}},{{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0}},{{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]}},{{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]}},{{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0}},{{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]}},{{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0}},{{&g_828[0]},{&g_828[0]},{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]}},{{(void*)0},{(void*)0},{&g_828[0]},{&g_828[0]},{(void*)0},{&g_828[0]},{&g_828[0]},{&g_828[0]},{&g_828[0]}}};
static uint16_t ** const **g_835 = &g_836[4][3][0];
static volatile int16_t g_854 = 1L;/* VOLATILE GLOBAL g_854 */
static volatile int16_t * volatile g_853 = &g_854;/* VOLATILE GLOBAL g_853 */
static volatile int16_t * volatile *g_852 = &g_853;
static struct S0 g_894 = {271,-3758,-16,28,0,1088};/* VOLATILE GLOBAL g_894 */
static union U1 g_949 = {0x3.708947p-17};/* VOLATILE GLOBAL g_949 */
static volatile struct S0 g_956 = {370,-4441,9,72,0,645};/* VOLATILE GLOBAL g_956 */
static volatile struct S0 g_967[2] = {{460,-18120,6,22,0,1997},{460,-18120,6,22,0,1997}};
static union U1 **g_971[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t ** volatile g_974[8][7][1] = {{{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225}},{{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0}},{{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225}},{{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0}},{{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225}},{{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0}},{{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225}},{{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0},{&g_225},{(void*)0}}};
static int32_t ** volatile g_975[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t ** const  volatile g_976 = &g_225;/* VOLATILE GLOBAL g_976 */
static int32_t ** volatile g_995 = &g_225;/* VOLATILE GLOBAL g_995 */
static union U2 *g_1008 = &g_144;
static union U2 ** volatile g_1007 = &g_1008;/* VOLATILE GLOBAL g_1007 */
static const struct S0 g_1011[7] = {{513,14667,4,236,0,676},{513,14667,4,236,0,676},{513,14667,4,236,0,676},{513,14667,4,236,0,676},{513,14667,4,236,0,676},{513,14667,4,236,0,676},{513,14667,4,236,0,676}};
static int32_t * volatile g_1054[3] = {&g_144.f0,&g_144.f0,&g_144.f0};
static int32_t * const  volatile g_1055[3] = {(void*)0,(void*)0,(void*)0};
static volatile struct S0 g_1072[2] = {{2,165,15,79,0,1007},{2,165,15,79,0,1007}};
static uint32_t g_1073 = 0x3EFD66D0L;
static uint64_t g_1074 = 3UL;
static const struct S0 *g_1080 = &g_574[4];
static const struct S0 ** volatile g_1079 = &g_1080;/* VOLATILE GLOBAL g_1079 */
static int32_t ** volatile g_1100[4][9][7] = {{{(void*)0,(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,(void*)0,&g_225,(void*)0,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,(void*)0,(void*)0,(void*)0},{&g_225,&g_225,(void*)0,&g_225,&g_225,(void*)0,&g_225},{(void*)0,(void*)0,&g_225,&g_225,(void*)0,&g_225,&g_225},{(void*)0,&g_225,&g_225,&g_225,&g_225,(void*)0,(void*)0},{&g_225,&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225},{&g_225,(void*)0,&g_225,&g_225,(void*)0,&g_225,&g_225},{&g_225,&g_225,(void*)0,(void*)0,&g_225,&g_225,&g_225}},{{(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225,(void*)0},{&g_225,&g_225,&g_225,&g_225,&g_225,(void*)0,&g_225},{&g_225,(void*)0,&g_225,&g_225,(void*)0,(void*)0,&g_225},{&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225},{&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,(void*)0,(void*)0,&g_225,&g_225,&g_225}},{{&g_225,(void*)0,(void*)0,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,(void*)0,(void*)0},{&g_225,(void*)0,&g_225,&g_225,(void*)0,&g_225,&g_225},{(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,(void*)0,(void*)0},{(void*)0,(void*)0,&g_225,(void*)0,&g_225,&g_225,&g_225}},{{&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225,(void*)0},{&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225,&g_225},{(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225,(void*)0},{(void*)0,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,(void*)0,&g_225,&g_225,&g_225},{&g_225,&g_225,&g_225,&g_225,&g_225,&g_225,&g_225}}};
static int32_t ** volatile g_1101 = &g_225;/* VOLATILE GLOBAL g_1101 */
static int32_t ** volatile g_1102[2] = {&g_225,&g_225};
static struct S0 g_1118 = {649,15348,9,174,0,1139};/* VOLATILE GLOBAL g_1118 */
static uint8_t g_1124 = 6UL;
static volatile union U1 g_1133[6] = {{0x1.6p+1},{0x1.6p+1},{0x1.6p+1},{0x1.6p+1},{0x1.6p+1},{0x1.6p+1}};
static int32_t ** volatile g_1143 = &g_225;/* VOLATILE GLOBAL g_1143 */
static union U1 g_1194[6][2][6] = {{{{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1},{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1}},{{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1},{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1}}},{{{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1},{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1}},{{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1},{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1}}},{{{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1},{-0x10.Ep+1},{-0x10.Ep+1},{0x1.9p-1}},{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}}},{{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}},{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}}},{{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}},{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}}},{{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}},{{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1},{-0x7.5p-1},{-0x7.5p-1},{-0x10.Ep+1}}}};
static uint32_t g_1212[6][3] = {{0xFBA76913L,0xFBA76913L,0xFBA76913L},{4294967295UL,0x04C7A034L,4294967295UL},{0xFBA76913L,0xFBA76913L,0xFBA76913L},{4294967295UL,0x04C7A034L,4294967295UL},{0xFBA76913L,0xFBA76913L,0xFBA76913L},{4294967295UL,0x04C7A034L,4294967295UL}};
static volatile int8_t g_1224 = 0x8BL;/* VOLATILE GLOBAL g_1224 */
static volatile int8_t * volatile g_1223 = &g_1224;/* VOLATILE GLOBAL g_1223 */
static volatile int8_t g_1226 = (-1L);/* VOLATILE GLOBAL g_1226 */
static volatile int8_t *g_1225 = &g_1226;
static volatile int8_t * volatile *g_1222[1][6][5] = {{{&g_1225,&g_1225,&g_1225,&g_1225,&g_1225},{&g_1225,&g_1225,(void*)0,&g_1225,&g_1225},{&g_1225,&g_1223,&g_1223,&g_1225,&g_1225},{(void*)0,&g_1225,(void*)0,&g_1225,&g_1225},{(void*)0,(void*)0,&g_1225,(void*)0,(void*)0},{&g_1225,&g_1225,&g_1225,&g_1225,&g_1223}}};
static volatile int8_t * volatile **g_1221 = &g_1222[0][4][3];
static volatile int8_t * volatile ** volatile * volatile g_1220 = &g_1221;/* VOLATILE GLOBAL g_1220 */
static volatile int8_t * volatile ** volatile * volatile *g_1219 = &g_1220;
static int32_t ** volatile g_1300 = &g_225;/* VOLATILE GLOBAL g_1300 */
static struct S0 g_1357 = {714,26716,11,157,0,1409};/* VOLATILE GLOBAL g_1357 */
static int8_t g_1376 = 0x5AL;
static const union U1 g_1407 = {0x7.FBAE74p-59};/* VOLATILE GLOBAL g_1407 */
static volatile union U1 **g_1470 = &g_291;
static volatile union U1 ***g_1469 = &g_1470;
static volatile union U1 ****g_1468 = &g_1469;
static int32_t g_1472[3][5][4] = {{{(-1L),(-1L),0xC735472AL,0x70F94B4CL},{(-1L),0L,(-6L),0x70F94B4CL},{0L,(-1L),(-6L),(-6L)},{(-1L),(-1L),0xC735472AL,0x70F94B4CL},{(-1L),0L,(-6L),0x70F94B4CL}},{{0L,(-1L),(-6L),(-6L)},{(-1L),(-1L),0xC735472AL,0x70F94B4CL},{(-1L),0L,(-6L),0x70F94B4CL},{0L,(-1L),(-6L),(-6L)},{(-1L),(-1L),0xC735472AL,0x70F94B4CL}},{{(-1L),0L,(-6L),0x70F94B4CL},{0L,(-1L),(-6L),(-6L)},{(-1L),(-1L),0xC735472AL,0x70F94B4CL},{(-1L),0L,(-6L),0x70F94B4CL},{0L,(-1L),(-6L),(-6L)}}};
static uint16_t ***g_1553 = (void*)0;
static uint16_t ***g_1554 = &g_828[0];
static int32_t ** volatile g_1581 = &g_225;/* VOLATILE GLOBAL g_1581 */
static int32_t g_1598 = 0x6982BF35L;
static struct S0 g_1672 = {30,-26915,15,232,0,1599};/* VOLATILE GLOBAL g_1672 */
static volatile uint32_t *g_1692[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile uint32_t ** volatile g_1691 = &g_1692[1];/* VOLATILE GLOBAL g_1691 */
static int32_t ** const  volatile g_1707 = &g_225;/* VOLATILE GLOBAL g_1707 */
static volatile int16_t * volatile **g_1715[9] = {&g_852,&g_852,&g_852,&g_852,&g_852,&g_852,&g_852,&g_852,&g_852};
static volatile int16_t * volatile *** volatile g_1714 = &g_1715[5];/* VOLATILE GLOBAL g_1714 */
static uint32_t *g_1775 = &g_1212[5][2];
static uint32_t **g_1774 = &g_1775;
static volatile uint8_t * volatile * volatile g_1796 = (void*)0;/* VOLATILE GLOBAL g_1796 */
static union U1 g_1842[6][1][6] = {{{{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50},{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50}}},{{{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50},{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50}}},{{{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50},{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50}}},{{{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50},{0x7.0p-1},{0x9.460565p+50},{0x9.460565p+50}}},{{{0x9.460565p+50},{0x0.Ap-1},{0x0.Ap-1},{0x9.460565p+50},{0x0.Ap-1},{0x0.Ap-1}}},{{{0x9.460565p+50},{0x0.Ap-1},{0x0.Ap-1},{0x9.460565p+50},{0x0.Ap-1},{0x0.Ap-1}}}};
static uint32_t g_1864 = 0x79F559CDL;
static int32_t ** volatile g_1872 = (void*)0;/* VOLATILE GLOBAL g_1872 */
static struct S0 g_1892 = {598,-1269,1,120,0,721};/* VOLATILE GLOBAL g_1892 */
static const volatile union U1 g_1949 = {-0x1.3p+1};/* VOLATILE GLOBAL g_1949 */
static int32_t g_1974 = 0x9F12BE3AL;
static int32_t * const g_1973 = &g_1974;
static int32_t * const *g_1972 = &g_1973;
static union U1 g_1991 = {0xB.6F174Fp-62};/* VOLATILE GLOBAL g_1991 */
static int32_t ** volatile g_1993 = &g_225;/* VOLATILE GLOBAL g_1993 */
static int32_t ** volatile g_2050[6] = {&g_225,&g_225,&g_225,&g_225,&g_225,&g_225};
static int32_t g_2061[9] = {0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL,0x9C2EEFDDL};
static int32_t g_2063 = 1L;
static int32_t g_2064 = (-1L);
static int32_t g_2065 = 0L;
static int32_t g_2066 = 0x9C7346DDL;
static int32_t g_2068 = 0xA47852D3L;
static volatile int32_t g_2076 = 0x3F77D208L;/* VOLATILE GLOBAL g_2076 */
static union U1 g_2084 = {0x3.9p+1};/* VOLATILE GLOBAL g_2084 */
static volatile union U1 g_2096 = {-0x1.Dp-1};/* VOLATILE GLOBAL g_2096 */
static union U1 g_2135 = {0x1.Fp-1};/* VOLATILE GLOBAL g_2135 */
static uint32_t *g_2142 = (void*)0;
static int32_t ** volatile g_2182 = &g_225;/* VOLATILE GLOBAL g_2182 */
static volatile int32_t * volatile * const  volatile g_2197 = (void*)0;/* VOLATILE GLOBAL g_2197 */
static int32_t g_2202 = 0x9E2EC00AL;
static float g_2221 = (-0x1.Ep-1);
static float g_2222 = 0x7.563AC0p-53;
static float * const *g_2223[2][3][10] = {{{&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,(void*)0,&g_109,&g_109}},{{&g_109,(void*)0,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,(void*)0,&g_109,(void*)0,&g_109,(void*)0,&g_109,&g_109,(void*)0},{&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109,&g_109}}};
static volatile float g_2238 = 0xF.E410DAp+0;/* VOLATILE GLOBAL g_2238 */
static int16_t ** const g_2281 = (void*)0;
static int16_t ** const *g_2280 = &g_2281;
static union U1 g_2319 = {0x5.DC3CB3p+20};/* VOLATILE GLOBAL g_2319 */
static union U1 g_2336 = {0xC.C38D66p-84};/* VOLATILE GLOBAL g_2336 */
static float g_2337 = (-0x7.0p-1);
static uint32_t g_2362 = 0UL;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static uint16_t  func_2(const uint32_t  p_3);
static uint16_t  func_8(int32_t  p_9);
static uint16_t  func_39(const int32_t * p_40, int32_t * p_41);
static const int32_t * func_42(int32_t * p_43, const float * p_44, uint32_t  p_45);
static int32_t * func_46(float  p_47, const int32_t * p_48);
static float  func_49(int64_t  p_50, int32_t * p_51);
static int32_t * func_52(float * p_53);
static float * func_54(int64_t  p_55);
static int32_t * const  func_57(float * p_58, union U2  p_59, float  p_60, float  p_61, int32_t * p_62);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2182 g_225 g_144.f0
 * writes:
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint16_t *l_998 = &g_184;
    int32_t l_1016[6];
    uint32_t l_1017 = 0x5A312373L;
    int8_t *l_1018 = (void*)0;
    int8_t *l_1019 = &g_93;
    int32_t l_1041 = 0x31C71FA2L;
    uint8_t l_1060 = 7UL;
    uint16_t ****l_1066[9] = {&g_827[7][0],&g_827[7][0],&g_827[7][0],&g_827[7][0],&g_827[7][0],&g_827[7][0],&g_827[7][0],&g_827[7][0],&g_827[7][0]};
    int32_t l_1077 = 0x95374530L;
    int32_t *l_1094[5][4] = {{&g_21[6],(void*)0,(void*)0,&g_21[6]},{(void*)0,&g_21[6],(void*)0,(void*)0},{&g_21[6],&g_21[6],(void*)0,&g_21[6]},{&g_21[6],(void*)0,(void*)0,&g_21[6]},{(void*)0,&g_21[6],(void*)0,(void*)0}};
    uint32_t l_1105 = 0x81028E94L;
    uint8_t l_1110 = 0xC7L;
    uint8_t l_1168[2][4] = {{250UL,250UL,250UL,250UL},{250UL,250UL,250UL,250UL}};
    int32_t l_1185 = 0L;
    int8_t l_1188[6][5];
    struct S0 *l_1282 = &g_574[5];
    int8_t ***l_1319 = (void*)0;
    int8_t ****l_1318 = &l_1319;
    union U2 **l_1362 = &g_1008;
    const uint16_t *****l_1394 = (void*)0;
    uint16_t l_1400 = 1UL;
    int64_t *l_1429 = &g_208;
    int16_t l_1436[6][10] = {{0xE1DFL,0x953DL,(-1L),(-1L),0x371DL,7L,(-8L),0xF973L,0L,0xF973L},{(-2L),(-1L),0x371DL,0x953DL,0x371DL,(-1L),(-2L),(-7L),0x35C1L,0L},{0x371DL,(-8L),0x1B41L,0xC345L,(-7L),0x8439L,(-1L),(-1L),0x8439L,(-7L)},{0x35C1L,(-8L),(-8L),0x35C1L,0x8439L,0x371DL,(-2L),0L,0xB115L,7L},{0x1B41L,(-1L),0xB115L,(-2L),(-8L),0xC345L,(-8L),(-2L),0xB115L,(-1L)},{(-7L),0x953DL,0xF973L,0x35C1L,0xE1DFL,(-1L),0x371DL,0xB115L,0x8439L,0x8439L}};
    int16_t l_1437 = 3L;
    union U1 ***l_1451 = &g_971[7];
    union U1 ****l_1450[10][6] = {{&l_1451,&l_1451,(void*)0,(void*)0,&l_1451,(void*)0},{&l_1451,&l_1451,(void*)0,(void*)0,&l_1451,&l_1451},{&l_1451,(void*)0,(void*)0,&l_1451,&l_1451,&l_1451},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,(void*)0},{&l_1451,&l_1451,(void*)0,(void*)0,&l_1451,&l_1451},{(void*)0,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451},{&l_1451,(void*)0,(void*)0,&l_1451,&l_1451,(void*)0},{&l_1451,&l_1451,&l_1451,&l_1451,&l_1451,&l_1451},{(void*)0,&l_1451,(void*)0,(void*)0,&l_1451,&l_1451},{&l_1451,(void*)0,(void*)0,&l_1451,&l_1451,&l_1451}};
    int8_t l_1471 = 1L;
    int32_t *l_1504 = &l_1185;
    struct S0 **l_1530[1][3][9] = {{{&g_297,&g_297,(void*)0,(void*)0,&g_297,&g_297,(void*)0,(void*)0,&g_297},{&g_297,(void*)0,&g_297,(void*)0,&g_297,(void*)0,&g_297,(void*)0,&g_297},{&g_297,(void*)0,(void*)0,&g_297,&g_297,(void*)0,(void*)0,&g_297,&g_297}}};
    struct S0 ***l_1529 = &l_1530[0][1][8];
    uint16_t **** const *l_1555 = &l_1066[7];
    float l_1559[2];
    int32_t l_1573[7][7][3] = {{{0L,1L,0xA1191A43L},{7L,(-6L),0x3CA37340L},{3L,0x5B21FB15L,0x174D3E28L},{0x5B21FB15L,9L,0L},{(-8L),9L,(-1L)},{0xDB6754F9L,0x5B21FB15L,0xA1257858L},{(-6L),(-6L),0x75419EC8L}},{{(-1L),1L,0x9912D864L},{0x3CA37340L,0x641F72FFL,0L},{3L,0x7EC74B04L,(-8L)},{0L,0x3CA37340L,0L},{(-6L),7L,0x9912D864L},{0xEA2828E0L,0x5B21FB15L,0x75419EC8L},{0x174D3E28L,0x641F72FFL,0xA1257858L}},{{(-1L),0xFB97A2B2L,(-1L)},{7L,0x174D3E28L,0L},{7L,0L,0x174D3E28L},{(-1L),0x3CA37340L,0x3CA37340L},{0x174D3E28L,9L,0xA1191A43L},{0xEA2828E0L,0x7EC74B04L,0xA9C30E0DL},{(-6L),0x174D3E28L,0xA1257858L}},{{0L,0xDB6754F9L,0x9912D864L},{3L,0x174D3E28L,0x3CA37340L},{0x3CA37340L,0x7EC74B04L,0L},{(-1L),9L,3L},{(-6L),0x3CA37340L,0xA1191A43L},{0xDB6754F9L,0L,0x75419EC8L},{(-8L),0x174D3E28L,0x75419EC8L}},{{0x5B21FB15L,0xFB97A2B2L,0xA1191A43L},{3L,0x641F72FFL,3L},{7L,0x5B21FB15L,0L},{0L,7L,0x3CA37340L},{(-8L),0x3CA37340L,0x9912D864L},{1L,0x7EC74B04L,0xA1257858L},{(-8L),0x641F72FFL,0xA9C30E0DL}},{{0L,1L,0xA1191A43L},{7L,(-6L),0x3CA37340L},{(-1L),3L,0xE5E10642L},{3L,0xDB6754F9L,0xFB97A2B2L},{0x5B55CF90L,0xDB6754F9L,0xA1257858L},{0x9912D864L,3L,0x641F72FFL},{0x28CB231CL,0x28CB231CL,(-8L)}},{{0L,(-1L),0xA9C30E0DL},{0xEA2828E0L,(-1L),0xFB97A2B2L},{(-1L),7L,0x5B55CF90L},{0x3CA37340L,0xEA2828E0L,0xFB97A2B2L},{0x28CB231CL,1L,0xA9C30E0DL},{0xA1191A43L,3L,(-8L)},{0xE5E10642L,(-1L),0x641F72FFL}}};
    int8_t l_1585[6][8] = {{0L,0x06L,0L,0xFEL,0xFEL,0L,0x06L,0L},{0x88L,0xFEL,1L,0xFEL,0x88L,0x88L,0xFEL,1L},{0x88L,0x88L,0xFEL,1L,0xFEL,0x88L,0x88L,0xFEL},{0L,0xFEL,0xFEL,0L,0x06L,0L,0xFEL,0xFEL},{0xFEL,0x06L,1L,1L,0x06L,0xFEL,0x06L,1L},{0L,0x06L,0L,0xFEL,0xFEL,0L,0x06L,0L}};
    uint64_t l_1611[7];
    uint8_t *l_1624 = &g_103;
    uint32_t l_1682 = 4294967290UL;
    uint32_t *l_1690 = &g_1212[2][0];
    uint32_t **l_1689 = &l_1690;
    float *l_1745 = &g_12;
    int8_t ** const *l_1813 = &g_421;
    int8_t ** const **l_1812 = &l_1813;
    int8_t ** const ***l_1811 = &l_1812;
    int8_t l_1931 = (-1L);
    uint8_t l_1960 = 0UL;
    int8_t l_1981 = 0x6AL;
    union U2 *l_1994 = &g_144;
    uint16_t l_1996[1];
    int8_t l_2052 = 0x75L;
    int32_t *l_2148 = &l_1573[3][3][0];
    int32_t **l_2147 = &l_2148;
    float ****l_2166 = (void*)0;
    uint32_t l_2261 = 1UL;
    uint8_t l_2263 = 7UL;
    int32_t ***l_2270 = &l_2147;
    int32_t l_2318 = 0x17AD1F93L;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1016[i] = 0x0C55105CL;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
            l_1188[i][j] = 0x9CL;
    }
    for (i = 0; i < 2; i++)
        l_1559[i] = 0xB.FA118Dp-12;
    for (i = 0; i < 7; i++)
        l_1611[i] = 0UL;
    for (i = 0; i < 1; i++)
        l_1996[i] = 0x6352L;
    return (**g_2182);
}


/* ------------------------------------------ */
/* 
 * reads : g_1007
 * writes: g_225 g_1008
 */
static uint16_t  func_2(const uint32_t  p_3)
{ /* block id: 506 */
    int32_t *l_999[7] = {(void*)0,&g_144.f0,(void*)0,(void*)0,&g_144.f0,(void*)0,(void*)0};
    int16_t l_1000 = 0x4DF3L;
    float l_1001 = 0x1.8p-1;
    uint16_t l_1002 = 0UL;
    float *l_1005 = &g_685.f0;
    int32_t **l_1006 = &g_225;
    int i;
    l_1002++;
    (*l_1006) = l_999[5];
    (*g_1007) = &g_144;
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_10 g_13
 * writes: g_12 g_10 g_16 g_17 g_11
 */
static uint16_t  func_8(int32_t  p_9)
{ /* block id: 1 */
    uint64_t l_23 = 4UL;
    int64_t l_24 = 0x400B8F3BEC767DBFLL;
    uint16_t ** const **l_838 = &g_836[2][2][0];
    const float *l_843 = &g_430.f0;
    int32_t l_934 = 5L;
    (*g_13) = g_10;
    for (p_9 = 0; (p_9 <= 2); p_9 = safe_add_func_int16_t_s_s(p_9, 9))
    { /* block id: 5 */
        int32_t *l_22[10][4][2] = {{{&g_21[3],(void*)0},{&g_21[6],(void*)0},{&g_21[7],&g_21[0]},{(void*)0,&g_21[6]}},{{&g_21[4],&g_21[6]},{&g_21[2],&g_21[6]},{(void*)0,(void*)0},{&g_21[2],&g_21[3]}},{{&g_21[0],&g_21[6]},{&g_21[6],&g_21[6]},{&g_21[2],&g_21[6]},{&g_21[2],&g_21[2]}},{{&g_21[2],&g_21[6]},{&g_21[2],&g_21[6]},{&g_21[6],&g_21[6]},{&g_21[0],&g_21[3]}},{{&g_21[2],(void*)0},{(void*)0,&g_21[6]},{&g_21[2],&g_21[6]},{&g_21[4],&g_21[6]}},{{(void*)0,&g_21[0]},{&g_21[7],(void*)0},{&g_21[6],(void*)0},{&g_21[3],&g_21[6]}},{{&g_21[4],&g_21[4]},{(void*)0,&g_21[7]},{&g_21[0],&g_21[4]},{(void*)0,&g_21[2]}},{{&g_21[4],(void*)0},{(void*)0,&g_21[6]},{&g_21[6],&g_21[6]},{(void*)0,(void*)0}},{{&g_21[4],&g_21[2]},{(void*)0,&g_21[4]},{&g_21[0],&g_21[7]},{(void*)0,&g_21[4]}},{{&g_21[4],&g_21[6]},{&g_21[3],(void*)0},{&g_21[6],(void*)0},{&g_21[7],&g_21[0]}}};
        const struct S0 *l_928 = &g_147;
        uint16_t **l_950 = &g_829;
        uint64_t *l_961 = &g_487;
        uint16_t l_985[2][1];
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
                l_985[i][j] = 1UL;
        }
        for (g_10 = 0; g_10 < 5; g_10 += 1)
        {
            for (g_16 = 0; g_16 < 4; g_16 += 1)
            {
                for (g_17 = 0; g_17 < 2; g_17 += 1)
                {
                    g_11[g_10][g_16][g_17] = &g_12;
                }
            }
        }
        l_23 = (safe_mul_func_int8_t_s_s(g_10, p_9));
        if (l_24)
            break;
        for (l_24 = 0; (l_24 >= 19); l_24 = safe_add_func_uint16_t_u_u(l_24, 1))
        { /* block id: 11 */
            float l_33 = 0x8.4p+1;
            int32_t l_34 = (-5L);
            uint16_t *****l_830 = &g_826;
            uint16_t ** const ***l_837[5];
            int32_t *l_895 = (void*)0;
            int64_t *l_902 = (void*)0;
            int64_t *l_903 = &g_733;
            union U1 **l_968[6][7][6] = {{{(void*)0,&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0]},{(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0]},{&g_697[0],(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0]}},{{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],(void*)0,&g_697[0],(void*)0,&g_697[0]},{&g_697[0],(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],(void*)0,(void*)0,&g_697[0],(void*)0,(void*)0},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]}},{{&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0],(void*)0},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0},{&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0],(void*)0},{&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0],(void*)0},{(void*)0,&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0]},{(void*)0,(void*)0,(void*)0,&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],(void*)0,(void*)0,&g_697[0],(void*)0,&g_697[0]}},{{&g_697[0],&g_697[0],(void*)0,&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],(void*)0,(void*)0,&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],(void*)0,&g_697[0],(void*)0,&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0},{(void*)0,(void*)0,&g_697[0],&g_697[0],(void*)0,(void*)0},{(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0}},{{&g_697[0],(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]},{(void*)0,&g_697[0],&g_697[0],&g_697[0],&g_697[0],(void*)0},{&g_697[0],&g_697[0],(void*)0,&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],(void*)0,&g_697[0],&g_697[0],&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0],&g_697[0]}},{{&g_697[0],&g_697[0],&g_697[0],(void*)0,(void*)0,&g_697[0]},{(void*)0,&g_697[0],&g_697[0],&g_697[0],(void*)0,(void*)0},{&g_697[0],(void*)0,(void*)0,&g_697[0],&g_697[0],(void*)0},{(void*)0,(void*)0,&g_697[0],(void*)0,&g_697[0],&g_697[0]},{&g_697[0],(void*)0,(void*)0,(void*)0,&g_697[0],&g_697[0]},{&g_697[0],(void*)0,(void*)0,(void*)0,(void*)0,&g_697[0]},{&g_697[0],&g_697[0],&g_697[0],(void*)0,&g_697[0],(void*)0}}};
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_837[i] = (void*)0;
        }
    }
    return l_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_144.f0
 * writes:
 */
static uint16_t  func_39(const int32_t * p_40, int32_t * p_41)
{ /* block id: 461 */
    int32_t *l_896 = &g_144.f0;
    int32_t *l_897[2][1];
    int64_t l_898 = (-1L);
    uint32_t l_899 = 0x98D8337EL;
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_897[i][j] = &g_21[6];
    }
    ++l_899;
    return (*l_896);
}


/* ------------------------------------------ */
/* 
 * reads : g_826 g_827 g_852 g_853 g_854 g_131 g_21 g_422 g_90 g_574.f4 g_10 g_225 g_114 g_364 g_702.f5 g_829 g_184 g_574.f5 g_421 g_894 g_297
 * writes: g_827 g_144.f0 g_21 g_147
 */
static const int32_t * func_42(int32_t * p_43, const float * p_44, uint32_t  p_45)
{ /* block id: 452 */
    union U2 l_846 = {0x8250DEFEL};
    uint16_t ***l_847 = &g_828[0];
    int16_t *l_864 = &g_488;
    int16_t **l_863[8] = {&l_864,&l_864,&l_864,&l_864,&l_864,&l_864,&l_864,&l_864};
    int32_t l_865 = 1L;
    struct S0 *l_884[2];
    int32_t *l_889 = &l_846.f0;
    int32_t *l_890[2][5] = {{(void*)0,(void*)0,(void*)0,&l_865,&l_865},{(void*)0,(void*)0,(void*)0,&l_865,&l_865}};
    uint8_t l_891[5][10] = {{1UL,1UL,0xF1L,3UL,0xF1L,1UL,1UL,1UL,1UL,0xF1L},{1UL,1UL,1UL,1UL,0xF1L,3UL,0xF1L,1UL,1UL,1UL},{0xF1L,1UL,0x28L,0x11L,0x11L,0x28L,1UL,0xF1L,1UL,0x28L},{3UL,1UL,0x11L,1UL,3UL,0x28L,0x28L,3UL,1UL,0x11L},{0xF1L,0xF1L,0x11L,3UL,255UL,3UL,0x11L,0xF1L,0xF1L,0x11L}};
    int i, j;
    for (i = 0; i < 2; i++)
        l_884[i] = &g_706;
    (*g_225) = (safe_add_func_uint64_t_u_u((((l_846 , ((l_847 != ((*g_826) = (*g_826))) <= ((safe_sub_func_int64_t_s_s((p_45 == (safe_mul_func_int8_t_s_s(p_45, ((l_865 |= (g_852 == (((((safe_mod_func_uint64_t_u_u(((*g_853) & (((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_s(p_45, p_45)) & g_131[2][5]), g_21[3])) ^ p_45) == (-1L))), g_21[6])) != (*g_422)) | 9UL) && l_846.f0) , l_863[0]))) , g_574[5].f4)))), g_10)) <= p_45))) >= (-8L)) <= l_846.f1), (-2L)));
    (*g_225) = ((l_846 , (safe_mul_func_uint8_t_u_u(((safe_div_func_uint16_t_u_u(p_45, (safe_mod_func_int8_t_s_s((((safe_lshift_func_int16_t_s_s(0xC045L, 3)) != (safe_mod_func_uint16_t_u_u(((safe_add_func_int32_t_s_s(((*g_114) ^= l_865), ((safe_add_func_int64_t_s_s(((*g_364) < (0xA9L | ((safe_mul_func_int16_t_s_s((l_884[1] != (void*)0), (((safe_mul_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_s(0xB7L, 3)) ^ p_45) | g_702[0].f5), (*g_829))) , 9UL) | p_45))) == 0x22393011L))), g_574[5].f5)) & 0UL))) <= 0x4C06L), l_846.f1))) , (**g_421)), p_45)))) > p_45), (-1L)))) == (**g_421));
    l_891[2][0]--;
    (*g_297) = g_894;
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_114 g_21
 * writes: g_21
 */
static int32_t * func_46(float  p_47, const int32_t * p_48)
{ /* block id: 448 */
    uint32_t l_839[3];
    int8_t ***l_841 = &g_421;
    int8_t ****l_840 = &l_841;
    int32_t *l_842 = (void*)0;
    int i;
    for (i = 0; i < 3; i++)
        l_839[i] = 0x3666D187L;
    (*g_114) |= l_839[1];
    (*l_840) = (void*)0;
    return l_842;
}


/* ------------------------------------------ */
/* 
 * reads : g_10 g_73 g_574 g_421 g_422 g_90 g_364 g_147.f0 g_93 g_17 g_114 g_691 g_505.f0
 * writes: g_10 g_131 g_90 g_144.f1 g_21
 */
static float  func_49(int64_t  p_50, int32_t * p_51)
{ /* block id: 181 */
    int32_t *l_380 = &g_21[6];
    uint32_t l_387[5][6][7] = {{{0xBA8A3959L,1UL,1UL,0xEB6B3D84L,4294967292UL,0xBD1524B6L,0UL},{1UL,0xE820B6D7L,0xAA1C2015L,1UL,0xF503F601L,0xEB6B3D84L,0xEF924D50L},{0xBA8A3959L,0xEB6B3D84L,1UL,0x0B395936L,0xE820B6D7L,0x7B1FB3B8L,1UL},{4294967295UL,0x416522CDL,4294967292UL,0UL,1UL,0xC9C2B50DL,4294967295UL},{0x416522CDL,0x5A7728B8L,1UL,1UL,0UL,0x034EC305L,4294967292UL},{1UL,4294967289UL,4294967288UL,1UL,5UL,0xCBC59CDDL,0x721688ACL}},{{1UL,4294967289UL,0xAA1C2015L,0xB6866283L,1UL,0x3BB9B582L,4294967289UL},{0x7B1FB3B8L,0x5A7728B8L,1UL,0xFFAC4130L,0x21888E1FL,8UL,1UL},{0x0CA60B45L,0x416522CDL,0x35671BD1L,4294967290UL,0x35671BD1L,0x416522CDL,0x0CA60B45L},{0x31A7A64CL,0xEB6B3D84L,5UL,4294967292UL,1UL,4294967295UL,0x3BB9B582L},{1UL,0xE820B6D7L,0xCBC59CDDL,0xA11987D1L,0x3BB9B582L,1UL,0xAF5582DEL},{4294967289UL,1UL,5UL,0x4DCDECD8L,0x416522CDL,4294967288UL,0x034EC305L}},{{0xB6866283L,0xBD1524B6L,0xBA8A3959L,0x82F886A3L,0xC9C2B50DL,5UL,4294967289UL},{4294967292UL,0x3BB9B582L,4294967288UL,0xE8CED9D1L,0xCBC59CDDL,1UL,0xA11987D1L},{1UL,0x0B395936L,0xCACE2543L,1UL,4294967289UL,0x5A7728B8L,0xFFAC4130L},{1UL,4294967295UL,0x82F886A3L,1UL,0x0B395936L,1UL,1UL},{0xCBC59CDDL,0x31A7A64CL,1UL,0xE8CED9D1L,0x034EC305L,0x7B1FB3B8L,4294967295UL},{0x4DCDECD8L,4294967290UL,0xCBC59CDDL,0x82F886A3L,4294967295UL,0x82F886A3L,0xCBC59CDDL}},{{1UL,1UL,0xBD1524B6L,0x9CF59EB5L,4294967295UL,0x3BB9B582L,1UL},{0xEB6B3D84L,4294967288UL,0xCACE2543L,0x7B1FB3B8L,0x3BB9B582L,1UL,4294967288UL},{1UL,0x6F3951A5L,1UL,0xCBC59CDDL,4294967295UL,0x92B9A8F9L,4294967293UL},{0x0B395936L,0x31A7A64CL,0xEF924D50L,0xF503F601L,4294967295UL,4294967289UL,0x46EADA6BL},{0UL,4294967291UL,4294967295UL,0xB6866283L,0x034EC305L,5UL,0x0B395936L},{1UL,1UL,0x7B1FB3B8L,0xE820B6D7L,0x0B395936L,1UL,0xEB6B3D84L}},{{1UL,0x7B1FB3B8L,0x57A73086L,8UL,4294967289UL,1UL,4294967288UL},{0xB6866283L,0x416522CDL,3UL,0xBD1524B6L,0xCBC59CDDL,5UL,0x31A7A64CL},{0xFFAC4130L,0x0CA60B45L,0xF503F601L,0xE8CED9D1L,0xC9C2B50DL,4294967289UL,0xC9C2B50DL},{4294967290UL,0x0B395936L,0x0B395936L,4294967290UL,0x416522CDL,0x92B9A8F9L,0xFFAC4130L},{4294967292UL,0x5A7728B8L,0xBD1524B6L,4294967289UL,0x0B395936L,1UL,1UL},{0xA11987D1L,4294967289UL,1UL,4294967291UL,1UL,0x3BB9B582L,0xFFAC4130L}}};
    int16_t l_394 = 0L;
    int8_t *l_420 = &g_144.f1;
    int8_t **l_419[6] = {&l_420,&l_420,&l_420,&l_420,&l_420,&l_420};
    int64_t *l_425[8] = {&g_208,&g_208,&g_208,&g_208,&g_208,&g_208,&g_208,&g_208};
    int16_t *l_506 = &g_56;
    int32_t l_516 = 0x117F9502L;
    int32_t l_520 = 0x905C72F0L;
    uint8_t l_526 = 0x79L;
    int64_t l_564[5][9][2] = {{{0xF917EF735F4DDDEDLL,3L},{(-8L),(-1L)},{0xF5F9EABAC11EBC7ELL,0x60B742C8A954A38ALL},{0x60B742C8A954A38ALL,(-8L)},{0x98608C39DE970178LL,0x3BA10BED8832B160LL},{1L,1L},{3L,9L},{(-5L),0x480B29A98CE11C2CLL},{(-1L),0x161022FD75EB5E28LL}},{{0xF46B5271CAC1D831LL,1L},{1L,0L},{(-7L),1L},{(-5L),1L},{(-7L),0L},{1L,1L},{0xF46B5271CAC1D831LL,0x161022FD75EB5E28LL},{(-1L),0x480B29A98CE11C2CLL},{(-5L),9L}},{{3L,1L},{1L,0x3BA10BED8832B160LL},{0x98608C39DE970178LL,(-8L)},{0x60B742C8A954A38ALL,0x60B742C8A954A38ALL},{0xF5F9EABAC11EBC7ELL,(-1L)},{(-8L),0x161022FD75EB5E28LL},{0x6A492D2211892289LL,1L},{1L,0x6A492D2211892289LL},{0x3BA10BED8832B160LL,0x60B742C8A954A38ALL}},{{0x3BA10BED8832B160LL,0x6A492D2211892289LL},{1L,1L},{0x6A492D2211892289LL,0x161022FD75EB5E28LL},{(-7L),0x6307F35D760F49AALL},{0xF46B5271CAC1D831LL,0xAEC77D63C4D28701LL},{0xAEC77D63C4D28701LL,(-7L)},{(-1L),(-1L)},{(-1L),(-5L)},{0x161022FD75EB5E28LL,(-1L)}},{{9L,3L},{0L,0xF917EF735F4DDDEDLL},{(-8L),0L},{(-5L),0x3BA10BED8832B160LL},{1L,(-1L)},{(-5L),(-1L)},{1L,0x3BA10BED8832B160LL},{(-5L),0L},{(-8L),0xF917EF735F4DDDEDLL}}};
    int32_t l_576 = 0xBF9971DDL;
    uint16_t *l_577 = &g_131[1][5];
    int8_t ***l_579 = &g_421;
    int32_t l_596 = 7L;
    int32_t l_597 = 0x131A09C9L;
    int32_t l_599 = 0x7EF65A4BL;
    int32_t l_614 = 0xD6EA847DL;
    int32_t l_615 = 4L;
    int32_t l_616[9] = {0x0A0288DAL,0x0A0288DAL,0x0A0288DAL,0x0A0288DAL,0x0A0288DAL,0x0A0288DAL,0x0A0288DAL,0x0A0288DAL,0x0A0288DAL};
    uint8_t l_618 = 0x64L;
    int64_t l_636 = 1L;
    struct S0 **l_637 = (void*)0;
    uint16_t l_667[3][2] = {{0x04DFL,0x04DFL},{0x04DFL,0x04DFL},{0x04DFL,0x04DFL}};
    int i, j, k;
    for (g_10 = 2; (g_10 <= 9); g_10 += 1)
    { /* block id: 184 */
        int32_t *l_379 = (void*)0;
        int16_t l_397 = 0xB5A4L;
        int8_t * const * const l_474 = &l_420;
        int8_t * const * const *l_473 = &l_474;
        int32_t l_514 = 9L;
        int32_t l_515 = 0xFFD0442BL;
        int32_t l_519[5][2][6] = {{{0L,0xA3B2A788L,0x48085CE6L,0xB3EEB261L,8L,(-9L)},{1L,2L,0xB3EEB261L,0xA3B2A788L,0xABA3DC34L,(-9L)}},{{(-1L),0L,0x48085CE6L,(-9L),2L,0xF0CCCB45L},{0xABA3DC34L,(-4L),0x6B01C9FDL,0x6B01C9FDL,(-4L),0xABA3DC34L}},{{0xF0CCCB45L,2L,(-9L),0x48085CE6L,0L,(-1L)},{(-9L),0xABA3DC34L,0xA3B2A788L,0xB3EEB261L,2L,1L}},{{(-9L),8L,0xB3EEB261L,0x48085CE6L,0xA3B2A788L,0L},{0xF0CCCB45L,0L,(-3L),0x6B01C9FDL,(-3L),0L}},{{0xABA3DC34L,0x5CDF3E45L,(-1L),(-9L),1L,0xABA3DC34L},{(-1L),0x48085CE6L,(-3L),0xA3B2A788L,0L,0x62F0825AL}}};
        union U1 ***l_545[1][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        volatile union U1 ** volatile *l_553[8][1] = {{&g_290},{(void*)0},{&g_290},{(void*)0},{&g_290},{(void*)0},{&g_290},{(void*)0}};
        int i, j, k;
        l_380 = l_379;
    }
    (*g_114) = (safe_rshift_func_uint8_t_u_u((((((((g_73 , (safe_mul_func_int8_t_s_s(((*l_420) = ((safe_lshift_func_int8_t_s_s(l_564[1][6][0], ((**g_421) = (l_520 < (safe_mod_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((((((*l_577) = (safe_mul_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(((0xD677D69CA24B9B60LL > ((safe_unary_minus_func_int16_t_s(0x223DL)) >= (p_50 <= (g_574[5] , (safe_unary_minus_func_uint8_t_u((l_576 | (l_520 || (**g_421))))))))) & 8UL), (*g_364))), g_147.f0))) , p_50) != g_93) , 0xC4L), (**g_421))), p_50)))))) || g_17)), p_50))) ^ 0x869F001AL) && (*g_422)) <= p_50) == p_50) < p_50) == l_526), 0));
    for (l_526 = 1; (l_526 <= 8); l_526 += 1)
    { /* block id: 284 */
        uint16_t l_600 = 0x43E0L;
        int32_t l_611 = 0xF487A3D6L;
        int32_t l_612 = (-1L);
        int32_t l_613[1][6] = {{0x29714C98L,0x29714C98L,0x0A621B5AL,0x29714C98L,0x29714C98L,0x0A621B5AL}};
        union U1 *l_632[6][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0],&g_505[0]}};
        uint32_t *l_664 = &l_387[0][0][6];
        int8_t l_723 = (-2L);
        union U2 l_740[5][7] = {{{0x2ED18B4EL},{3L},{0xD34E30B5L},{3L},{0x2ED18B4EL},{3L},{0xD34E30B5L}},{{0x12E8EEF0L},{0xFD4EDA81L},{1L},{1L},{0xFD4EDA81L},{0x12E8EEF0L},{-4L}},{{0x952ECD14L},{8L},{0x952ECD14L},{3L},{0x952ECD14L},{8L},{0x952ECD14L}},{{0x12E8EEF0L},{1L},{-4L},{0xFD4EDA81L},{0xFD4EDA81L},{-4L},{1L}},{{0x2ED18B4EL},{8L},{0xD34E30B5L},{8L},{0x2ED18B4EL},{8L},{0xD34E30B5L}}};
        int32_t ** const l_744 = &g_225;
        int8_t ****l_752 = &l_579;
        int8_t l_803 = 0x7BL;
        int i, j;
    }
    return (*g_691);
}


/* ------------------------------------------ */
/* 
 * reads : g_144 g_206 g_208 g_109
 * writes: g_364 g_107 g_208 g_110 g_505.f0 g_12 g_685.f0 g_1974 g_1073
 */
static int32_t * func_52(float * p_53)
{ /* block id: 168 */
    uint64_t *l_363 = &g_10;
    int32_t l_365[8] = {0xD31CA354L,0L,0xD31CA354L,0xD31CA354L,0L,0xD31CA354L,0xD31CA354L,0L};
    int32_t l_366[7][8] = {{0x19AAE0EBL,0L,0xBB65BC47L,0L,(-6L),0xBB65BC47L,0xE4A07327L,0xEC6E6118L},{0xE4A07327L,0x3E299DC2L,0x68C83916L,0L,0x19AAE0EBL,0x1F2B5618L,0xE4A07327L,0xE4A07327L},{0x156648DEL,0L,0xBB65BC47L,0xBB65BC47L,0L,0x156648DEL,(-6L),0x3E299DC2L},{0L,0x156648DEL,(-6L),0x3E299DC2L,(-1L),0xBB65BC47L,0xA51F8BB1L,(-1L)},{0x3E299DC2L,0xE4A07327L,0x3116A7E1L,0x3E299DC2L,0x19AAE0EBL,1L,0x19AAE0EBL,0x3E299DC2L},{0L,0x19AAE0EBL,0L,0xBB65BC47L,0L,(-6L),0xBB65BC47L,0xE4A07327L},{0x19AAE0EBL,0xEC6E6118L,(-6L),0L,(-2L),(-1L),0L,0xEC6E6118L}};
    union U2 l_369 = {0x52E7DB46L};
    int32_t **l_370 = (void*)0;
    int32_t **l_373[2][2] = {{&g_225,&g_225},{&g_225,&g_225}};
    int32_t *l_374[8][7][2] = {{{&l_365[3],&l_366[1][4]},{(void*)0,&l_366[4][3]},{(void*)0,(void*)0},{&l_365[4],&l_365[7]},{&l_365[7],&l_365[1]},{&g_144.f0,(void*)0},{&g_21[6],&l_366[4][6]}},{{&l_365[1],&l_366[5][1]},{&l_365[6],(void*)0},{(void*)0,&l_365[3]},{&l_369.f0,&g_21[6]},{&l_365[4],&g_144.f0},{&l_365[4],(void*)0},{&l_366[5][1],&l_365[7]}},{{(void*)0,&l_365[7]},{&l_366[5][1],(void*)0},{&l_365[4],&g_144.f0},{&l_365[4],&g_21[6]},{&l_369.f0,&l_365[3]},{(void*)0,(void*)0},{&l_365[6],&l_366[5][1]}},{{&l_365[1],&l_366[4][6]},{&g_21[6],(void*)0},{&g_144.f0,&l_365[1]},{&l_365[7],&l_365[7]},{&l_365[4],(void*)0},{(void*)0,&l_366[4][3]},{(void*)0,&l_366[1][4]}},{{&l_365[3],(void*)0},{&l_366[4][1],&l_365[3]},{&l_366[4][1],(void*)0},{&l_365[3],&l_366[1][4]},{(void*)0,&l_366[4][3]},{(void*)0,(void*)0},{&l_365[4],&l_365[7]}},{{&l_365[7],&l_365[1]},{&g_144.f0,(void*)0},{&g_21[6],&l_366[4][6]},{&l_365[1],&l_366[5][1]},{&l_365[6],(void*)0},{(void*)0,&l_365[3]},{&l_369.f0,&g_21[6]}},{{&l_365[4],&g_144.f0},{&l_365[4],(void*)0},{&l_366[5][1],&l_365[7]},{(void*)0,&l_365[7]},{&l_366[5][1],(void*)0},{&l_365[4],&g_144.f0},{&l_365[4],&g_21[6]}},{{&l_366[4][6],&l_365[4]},{&l_369.f0,&g_144.f0},{&l_365[4],(void*)0},{&l_366[1][4],(void*)0},{&l_366[5][1],&l_366[4][3]},{&l_365[1],&l_366[1][4]},{&l_369.f0,&l_369.f0}}};
    volatile int8_t * const  volatile ** volatile *l_378 = &g_326;
    volatile int8_t * const  volatile ** volatile **l_377 = &l_378;
    int i, j, k;
    l_366[1][2] = ((l_365[3] = ((g_364 = l_363) == (((*p_53) = (&g_266 != (g_144 , p_53))) , (void*)0))) == (l_366[4][3] == ((((((l_366[1][5] , (safe_sub_func_uint64_t_u_u(l_366[6][5], g_206))) != 1L) , l_366[4][3]) , l_366[4][3]) , 0x7.90D6BEp-8) > l_366[4][3])));
    l_374[4][3][1] = (l_369 , (l_365[3] , &l_365[3]));
    for (g_208 = 0; (g_208 != 0); g_208 = safe_add_func_int16_t_s_s(g_208, 8))
    { /* block id: 176 */
        (*g_109) = (l_363 != &g_208);
    }
    (*l_377) = &g_326;
    return p_53;
}


/* ------------------------------------------ */
/* 
 * reads : g_10 g_21 g_73 g_90 g_69 g_16 g_114 g_115 g_68 g_56 g_115.f2 g_115.f1 g_93 g_143
 * writes: g_10 g_68 g_72 g_90 g_103 g_108 g_21 g_131 g_93
 */
static float * func_54(int64_t  p_55)
{ /* block id: 14 */
    union U2 l_64 = {-9L};
    float *l_66[4][2][10] = {{{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12}},{{&g_12,(void*)0,&g_12,&g_12,&g_12,&g_12,&g_12,(void*)0,&g_12,&g_12},{&g_12,&g_12,&g_12,(void*)0,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12}},{{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,(void*)0,&g_12},{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12}},{{&g_12,(void*)0,&g_12,(void*)0,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12,&g_12}}};
    uint64_t l_75 = 18446744073709551615UL;
    uint32_t l_164 = 0xBD7285B3L;
    uint32_t l_247[5];
    uint8_t *l_253 = (void*)0;
    union U1 *l_269[9][7] = {{&g_249,&g_249,(void*)0,(void*)0,&g_249,&g_249,&g_249},{(void*)0,&g_249,&g_249,(void*)0,&g_249,(void*)0,&g_249},{&g_249,&g_249,&g_249,(void*)0,&g_249,&g_249,&g_249},{&g_249,&g_249,&g_249,&g_249,&g_249,&g_249,&g_249},{&g_249,&g_249,&g_249,&g_249,&g_249,&g_249,&g_249},{&g_249,&g_249,&g_249,&g_249,&g_249,&g_249,&g_249},{&g_249,&g_249,&g_249,&g_249,&g_249,&g_249,&g_249},{&g_249,&g_249,&g_249,&g_249,&g_249,&g_249,&g_249},{&g_249,&g_249,(void*)0,&g_249,&g_249,&g_249,&g_249}};
    union U1 **l_268 = &l_269[2][3];
    float **l_351 = (void*)0;
    float ***l_350 = &l_351;
    float *l_362 = &g_107;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_247[i] = 0x3425632BL;
    for (g_10 = 0; (g_10 <= 1); g_10 += 1)
    { /* block id: 17 */
        float *l_63 = (void*)0;
        union U2 *l_65 = &l_64;
        union U2 l_67 = {0x4EA99669L};
        const int32_t *l_71 = &g_69[0];
        const int32_t **l_70[10] = {(void*)0,&l_71,(void*)0,&l_71,&l_71,(void*)0,&l_71,(void*)0,&l_71,&l_71};
        int32_t *l_74 = &g_21[(g_10 + 5)];
        int32_t **l_125 = &l_74;
        uint64_t *l_127 = &l_75;
        uint16_t *l_130 = &g_131[1][5];
        int32_t *l_139[2];
        union U2 l_243 = {1L};
        int8_t *l_287[8][6][5] = {{{&g_144.f1,&l_243.f1,&g_144.f1,&l_243.f1,&g_144.f1},{&g_144.f1,&l_67.f1,&l_243.f1,&l_64.f1,&l_243.f1},{&l_64.f1,&l_64.f1,&l_64.f1,(void*)0,&l_64.f1},{&l_243.f1,&g_93,(void*)0,&l_243.f1,&g_93},{&g_144.f1,&g_144.f1,(void*)0,&g_144.f1,&g_90},{&l_67.f1,(void*)0,&g_144.f1,&l_67.f1,&l_243.f1}},{{&g_90,&l_67.f1,&l_243.f1,&l_67.f1,(void*)0},{&l_67.f1,&l_243.f1,&l_67.f1,&g_90,&l_243.f1},{&l_243.f1,&g_90,(void*)0,&l_67.f1,&g_90},{&g_93,&g_144.f1,&g_90,(void*)0,&g_90},{&l_64.f1,&g_90,&g_93,&g_144.f1,&l_243.f1},{&g_144.f1,&g_144.f1,&l_243.f1,(void*)0,(void*)0}},{{&l_243.f1,(void*)0,&l_243.f1,&l_64.f1,&l_243.f1},{&g_144.f1,&g_90,&g_144.f1,&g_144.f1,&g_90},{&l_243.f1,&g_144.f1,&l_64.f1,&l_67.f1,&g_144.f1},{&l_67.f1,&l_243.f1,&g_144.f1,&g_90,&l_67.f1},{&g_90,&g_90,&l_243.f1,&l_243.f1,&g_93},{&l_67.f1,&g_90,&l_243.f1,&l_67.f1,&l_243.f1}},{{&g_144.f1,&l_64.f1,&g_93,&g_90,&l_67.f1},{&l_67.f1,&g_93,&g_90,&l_64.f1,&l_67.f1},{&l_67.f1,(void*)0,(void*)0,(void*)0,&g_144.f1},{&g_144.f1,&l_243.f1,&l_67.f1,(void*)0,&l_243.f1},{&l_67.f1,&l_243.f1,&l_243.f1,&l_67.f1,&l_64.f1},{&g_90,&l_67.f1,&g_144.f1,&l_67.f1,(void*)0}},{{&l_67.f1,&l_64.f1,&l_67.f1,&g_144.f1,(void*)0},{&l_243.f1,&g_90,&l_67.f1,&l_67.f1,&g_90},{&g_144.f1,&g_144.f1,&g_144.f1,&l_67.f1,&g_90},{&l_243.f1,&l_243.f1,&g_93,(void*)0,(void*)0},{&g_144.f1,(void*)0,&l_64.f1,(void*)0,(void*)0},{&l_64.f1,(void*)0,(void*)0,&l_64.f1,&l_243.f1}},{{&g_93,(void*)0,&g_144.f1,&g_90,&l_243.f1},{&l_243.f1,(void*)0,(void*)0,&l_67.f1,&g_93},{&l_67.f1,&l_243.f1,&l_64.f1,&l_243.f1,&l_67.f1},{&g_90,&g_144.f1,&l_243.f1,&g_90,&l_67.f1},{&l_67.f1,&g_90,&l_64.f1,&l_67.f1,&l_67.f1},{&g_90,&l_64.f1,&g_144.f1,&g_144.f1,&l_67.f1}},{{&g_93,&l_67.f1,&g_90,&l_64.f1,&l_67.f1},{&l_67.f1,&l_243.f1,&l_67.f1,(void*)0,&g_93},{&g_90,&l_243.f1,&g_144.f1,&g_144.f1,&l_243.f1},{(void*)0,(void*)0,&l_243.f1,(void*)0,&l_243.f1},{&g_90,&g_93,&l_243.f1,&l_67.f1,(void*)0},{&l_67.f1,&l_64.f1,&g_144.f1,&g_90,(void*)0}},{{&g_93,&g_90,&l_67.f1,&l_67.f1,&l_243.f1},{&l_64.f1,&g_93,(void*)0,&g_93,&g_144.f1},{&l_64.f1,&g_93,&g_90,&g_93,&l_67.f1},{&g_93,&g_93,&g_93,(void*)0,&l_67.f1},{&l_64.f1,(void*)0,&l_64.f1,&g_90,&g_144.f1},{&l_64.f1,(void*)0,&g_144.f1,(void*)0,&g_93}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_139[i] = &l_67.f0;
        (*l_125) = func_57(l_63, ((*l_65) = l_64), (g_21[(g_10 + 5)] , ((l_66[1][1][7] != l_63) != ((((g_72 = (l_67 , (g_68 = &g_21[1]))) != l_74) | g_73) , (-0x1.9p-1)))), l_75, &g_21[5]);
        l_64.f0 = (g_21[(g_10 + 1)] = ((((*l_127) = (!p_55)) == ((((g_56 , (safe_mul_func_uint16_t_u_u(((*l_130) = 0x897BL), (p_55 == (~l_64.f0))))) , (((safe_lshift_func_uint8_t_u_s((l_64.f1 | ((safe_div_func_int32_t_s_s((l_64.f1 ^ (**l_125)), ((safe_add_func_int64_t_s_s((*l_74), g_115[4][3][1].f2)) || p_55))) <= (**l_125))), p_55)) & 6UL) , g_115[4][3][1].f1)) < p_55) ^ 4294967288UL)) || g_93));
        for (l_67.f0 = 0; (l_67.f0 <= 1); l_67.f0 += 1)
        { /* block id: 44 */
            float *l_142 = &g_110;
            float *l_145 = &g_107;
            int32_t l_146 = 0x3CB5D9AAL;
            int8_t *l_150 = &g_90;
            const uint32_t l_163 = 0UL;
            uint64_t l_185 = 0x0F42965CC132FBB4LL;
            uint32_t l_187 = 0UL;
            uint16_t l_242 = 1UL;
            union U1 ** const l_271 = &l_269[2][3];
            int32_t *l_286[4][8][2];
            union U2 l_317 = {-1L};
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    for (k = 0; k < 2; k++)
                        l_286[i][j][k] = &l_146;
                }
            }
            l_146 = ((*g_114) = (0x13958EBFL < ((l_142 = l_139[0]) == (g_143[4] , l_145))));
            if (p_55)
                continue;
            if ((*g_114))
                continue;
            for (g_93 = 0; (g_93 <= 1); g_93 += 1)
            { /* block id: 52 */
                float **l_157 = &l_66[1][1][7];
                float ***l_156 = &l_157;
                int32_t l_200[3];
                union U2 l_272 = {1L};
                uint16_t *l_321 = &g_184;
                const uint16_t *l_359 = &g_249.f2;
                const uint16_t **l_358 = &l_359;
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_200[i] = 0x6E159BBAL;
            }
        }
    }
    return l_362;
}


/* ------------------------------------------ */
/* 
 * reads : g_90 g_69 g_16 g_21 g_114 g_115 g_68 g_73 g_144.f0
 * writes: g_90 g_103 g_108 g_21 g_144.f0
 */
static int32_t * const  func_57(float * p_58, union U2  p_59, float  p_60, float  p_61, int32_t * p_62)
{ /* block id: 21 */
    float *l_85[5][6][4] = {{{&g_12,(void*)0,&g_12,&g_12},{&g_12,(void*)0,(void*)0,&g_12},{&g_12,&g_12,&g_12,(void*)0},{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,(void*)0,&g_12},{&g_12,&g_12,&g_12,&g_12}},{{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,(void*)0,(void*)0},{&g_12,&g_12,&g_12,&g_12},{&g_12,(void*)0,&g_12,&g_12},{&g_12,(void*)0,(void*)0,&g_12},{&g_12,&g_12,&g_12,(void*)0}},{{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,(void*)0,&g_12},{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,(void*)0,(void*)0},{&g_12,&g_12,&g_12,&g_12}},{{&g_12,(void*)0,&g_12,&g_12},{&g_12,(void*)0,(void*)0,&g_12},{&g_12,&g_12,&g_12,(void*)0},{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,(void*)0,&g_12},{&g_12,&g_12,&g_12,&g_12}},{{&g_12,&g_12,&g_12,&g_12},{&g_12,&g_12,(void*)0,(void*)0},{&g_12,&g_12,&g_12,&g_12},{&g_12,(void*)0,&g_12,&g_12},{&g_12,(void*)0,(void*)0,&g_12},{&g_12,&g_12,&g_12,(void*)0}}};
    float *l_87 = &g_12;
    float **l_86 = &l_87;
    int8_t *l_88 = (void*)0;
    int8_t *l_89 = &g_90;
    int8_t *l_91 = (void*)0;
    int8_t *l_92[10] = {&g_93,(void*)0,&g_93,(void*)0,&g_93,(void*)0,&g_93,(void*)0,&g_93,(void*)0};
    int32_t l_94 = 0xC1CE2A3FL;
    uint8_t *l_101 = (void*)0;
    uint8_t *l_102 = &g_103;
    float * const l_106 = &g_107;
    float * const *l_105 = &l_106;
    float * const **l_104[5][4] = {{&l_105,&l_105,&l_105,&l_105},{&l_105,&l_105,&l_105,&l_105},{&l_105,&l_105,(void*)0,&l_105},{&l_105,&l_105,(void*)0,(void*)0},{&l_105,&l_105,&l_105,(void*)0}};
    uint8_t l_111 = 0x46L;
    uint64_t l_119 = 0xB1138347E9BEE6A4LL;
    int i, j, k;
    (*p_62) = (!((((safe_mod_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u((safe_mul_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u((l_94 &= ((*l_89) &= (l_85[3][0][3] == ((*l_86) = l_85[3][0][3])))), 3)) , ((g_69[1] < (safe_sub_func_int16_t_s_s(p_59.f1, ((safe_div_func_int16_t_s_s((safe_mod_func_uint8_t_u_u(((*l_102) = g_16), (((g_108 = &l_87) != &g_109) || ((void*)0 == &p_59)))), p_59.f1)) , p_59.f1)))) > g_69[1])), p_59.f0)), l_111)) , g_16), g_21[6])) || 0xF5F5C31AFCD19E8FLL) | l_111) >= 0x66L));
    for (p_59.f1 = 20; (p_59.f1 >= (-25)); p_59.f1 = safe_sub_func_uint8_t_u_u(p_59.f1, 8))
    { /* block id: 30 */
        return g_114;
    }
    (*p_62) = ((&g_11[2][2][1] == (l_86 = (g_115[4][3][1] , &p_58))) | (safe_sub_func_int16_t_s_s(((((((l_119 = (+(65535UL || p_59.f1))) > ((safe_div_func_uint8_t_u_u(((*p_62) || (!0UL)), ((*g_68) ^ ((((p_59.f1 >= p_59.f1) >= (-6L)) & l_94) >= (*g_68))))) < l_111)) != l_94) ^ 1UL) , g_73) & g_90), l_111)));
    return p_62;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_10, "g_10", print_hash_value);
    transparent_crc_bytes (&g_12, sizeof(g_12), "g_12", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_17, "g_17", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_21[i], "g_21[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_56, "g_56", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_69[i], "g_69[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc_bytes (&g_107, sizeof(g_107), "g_107", print_hash_value);
    transparent_crc_bytes (&g_110, sizeof(g_110), "g_110", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc_bytes(&g_115[i][j][k].f0, sizeof(g_115[i][j][k].f0), "g_115[i][j][k].f0", print_hash_value);
                transparent_crc(g_115[i][j][k].f1, "g_115[i][j][k].f1", print_hash_value);
                transparent_crc(g_115[i][j][k].f2, "g_115[i][j][k].f2", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_131[i][j], "g_131[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_143[i].f0, sizeof(g_143[i].f0), "g_143[i].f0", print_hash_value);
        transparent_crc(g_143[i].f1, "g_143[i].f1", print_hash_value);
        transparent_crc(g_143[i].f2, "g_143[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_144.f0, "g_144.f0", print_hash_value);
    transparent_crc(g_144.f1, "g_144.f1", print_hash_value);
    transparent_crc(g_147.f0, "g_147.f0", print_hash_value);
    transparent_crc(g_147.f1, "g_147.f1", print_hash_value);
    transparent_crc(g_147.f2, "g_147.f2", print_hash_value);
    transparent_crc(g_147.f3, "g_147.f3", print_hash_value);
    transparent_crc(g_147.f4, "g_147.f4", print_hash_value);
    transparent_crc(g_147.f5, "g_147.f5", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc_bytes(&g_162[i].f0, sizeof(g_162[i].f0), "g_162[i].f0", print_hash_value);
        transparent_crc(g_162[i].f1, "g_162[i].f1", print_hash_value);
        transparent_crc(g_162[i].f2, "g_162[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_184, "g_184", print_hash_value);
    transparent_crc(g_206, "g_206", print_hash_value);
    transparent_crc(g_208, "g_208", print_hash_value);
    transparent_crc_bytes (&g_244, sizeof(g_244), "g_244", print_hash_value);
    transparent_crc_bytes (&g_249.f0, sizeof(g_249.f0), "g_249.f0", print_hash_value);
    transparent_crc(g_249.f1, "g_249.f1", print_hash_value);
    transparent_crc(g_249.f2, "g_249.f2", print_hash_value);
    transparent_crc(g_266, "g_266", print_hash_value);
    transparent_crc(g_329, "g_329", print_hash_value);
    transparent_crc(g_404.f0, "g_404.f0", print_hash_value);
    transparent_crc(g_404.f1, "g_404.f1", print_hash_value);
    transparent_crc(g_404.f2, "g_404.f2", print_hash_value);
    transparent_crc(g_404.f3, "g_404.f3", print_hash_value);
    transparent_crc(g_404.f4, "g_404.f4", print_hash_value);
    transparent_crc(g_404.f5, "g_404.f5", print_hash_value);
    transparent_crc_bytes (&g_430.f0, sizeof(g_430.f0), "g_430.f0", print_hash_value);
    transparent_crc(g_430.f1, "g_430.f1", print_hash_value);
    transparent_crc(g_430.f2, "g_430.f2", print_hash_value);
    transparent_crc_bytes (&g_457.f0, sizeof(g_457.f0), "g_457.f0", print_hash_value);
    transparent_crc(g_457.f1, "g_457.f1", print_hash_value);
    transparent_crc(g_457.f2, "g_457.f2", print_hash_value);
    transparent_crc(g_461, "g_461", print_hash_value);
    transparent_crc(g_487, "g_487", print_hash_value);
    transparent_crc(g_488, "g_488", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_491[i][j].f0, "g_491[i][j].f0", print_hash_value);
            transparent_crc(g_491[i][j].f1, "g_491[i][j].f1", print_hash_value);
            transparent_crc(g_491[i][j].f2, "g_491[i][j].f2", print_hash_value);
            transparent_crc(g_491[i][j].f3, "g_491[i][j].f3", print_hash_value);
            transparent_crc(g_491[i][j].f4, "g_491[i][j].f4", print_hash_value);
            transparent_crc(g_491[i][j].f5, "g_491[i][j].f5", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_505[i].f0, sizeof(g_505[i].f0), "g_505[i].f0", print_hash_value);
        transparent_crc(g_505[i].f1, "g_505[i].f1", print_hash_value);
        transparent_crc(g_505[i].f2, "g_505[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_531.f0, "g_531.f0", print_hash_value);
    transparent_crc(g_531.f1, "g_531.f1", print_hash_value);
    transparent_crc(g_531.f2, "g_531.f2", print_hash_value);
    transparent_crc(g_531.f3, "g_531.f3", print_hash_value);
    transparent_crc(g_531.f4, "g_531.f4", print_hash_value);
    transparent_crc(g_531.f5, "g_531.f5", print_hash_value);
    transparent_crc(g_548, "g_548", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_574[i].f0, "g_574[i].f0", print_hash_value);
        transparent_crc(g_574[i].f1, "g_574[i].f1", print_hash_value);
        transparent_crc(g_574[i].f2, "g_574[i].f2", print_hash_value);
        transparent_crc(g_574[i].f3, "g_574[i].f3", print_hash_value);
        transparent_crc(g_574[i].f4, "g_574[i].f4", print_hash_value);
        transparent_crc(g_574[i].f5, "g_574[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_652[i][j], "g_652[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_662.f0, sizeof(g_662.f0), "g_662.f0", print_hash_value);
    transparent_crc(g_662.f1, "g_662.f1", print_hash_value);
    transparent_crc(g_662.f2, "g_662.f2", print_hash_value);
    transparent_crc_bytes (&g_685.f0, sizeof(g_685.f0), "g_685.f0", print_hash_value);
    transparent_crc(g_685.f1, "g_685.f1", print_hash_value);
    transparent_crc(g_685.f2, "g_685.f2", print_hash_value);
    transparent_crc_bytes (&g_692.f0, sizeof(g_692.f0), "g_692.f0", print_hash_value);
    transparent_crc(g_692.f1, "g_692.f1", print_hash_value);
    transparent_crc(g_692.f2, "g_692.f2", print_hash_value);
    transparent_crc(g_694.f0, "g_694.f0", print_hash_value);
    transparent_crc(g_694.f1, "g_694.f1", print_hash_value);
    transparent_crc(g_694.f2, "g_694.f2", print_hash_value);
    transparent_crc(g_694.f3, "g_694.f3", print_hash_value);
    transparent_crc(g_694.f4, "g_694.f4", print_hash_value);
    transparent_crc(g_694.f5, "g_694.f5", print_hash_value);
    transparent_crc_bytes (&g_698.f0, sizeof(g_698.f0), "g_698.f0", print_hash_value);
    transparent_crc(g_698.f1, "g_698.f1", print_hash_value);
    transparent_crc(g_698.f2, "g_698.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_702[i].f0, "g_702[i].f0", print_hash_value);
        transparent_crc(g_702[i].f1, "g_702[i].f1", print_hash_value);
        transparent_crc(g_702[i].f2, "g_702[i].f2", print_hash_value);
        transparent_crc(g_702[i].f3, "g_702[i].f3", print_hash_value);
        transparent_crc(g_702[i].f4, "g_702[i].f4", print_hash_value);
        transparent_crc(g_702[i].f5, "g_702[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_706.f0, "g_706.f0", print_hash_value);
    transparent_crc(g_706.f1, "g_706.f1", print_hash_value);
    transparent_crc(g_706.f2, "g_706.f2", print_hash_value);
    transparent_crc(g_706.f3, "g_706.f3", print_hash_value);
    transparent_crc(g_706.f4, "g_706.f4", print_hash_value);
    transparent_crc(g_706.f5, "g_706.f5", print_hash_value);
    transparent_crc(g_733, "g_733", print_hash_value);
    transparent_crc(g_790.f0, "g_790.f0", print_hash_value);
    transparent_crc(g_790.f1, "g_790.f1", print_hash_value);
    transparent_crc(g_790.f2, "g_790.f2", print_hash_value);
    transparent_crc(g_790.f3, "g_790.f3", print_hash_value);
    transparent_crc(g_790.f4, "g_790.f4", print_hash_value);
    transparent_crc(g_790.f5, "g_790.f5", print_hash_value);
    transparent_crc(g_854, "g_854", print_hash_value);
    transparent_crc(g_894.f0, "g_894.f0", print_hash_value);
    transparent_crc(g_894.f1, "g_894.f1", print_hash_value);
    transparent_crc(g_894.f2, "g_894.f2", print_hash_value);
    transparent_crc(g_894.f3, "g_894.f3", print_hash_value);
    transparent_crc(g_894.f4, "g_894.f4", print_hash_value);
    transparent_crc(g_894.f5, "g_894.f5", print_hash_value);
    transparent_crc_bytes (&g_949.f0, sizeof(g_949.f0), "g_949.f0", print_hash_value);
    transparent_crc(g_949.f1, "g_949.f1", print_hash_value);
    transparent_crc(g_949.f2, "g_949.f2", print_hash_value);
    transparent_crc(g_956.f0, "g_956.f0", print_hash_value);
    transparent_crc(g_956.f1, "g_956.f1", print_hash_value);
    transparent_crc(g_956.f2, "g_956.f2", print_hash_value);
    transparent_crc(g_956.f3, "g_956.f3", print_hash_value);
    transparent_crc(g_956.f4, "g_956.f4", print_hash_value);
    transparent_crc(g_956.f5, "g_956.f5", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_967[i].f0, "g_967[i].f0", print_hash_value);
        transparent_crc(g_967[i].f1, "g_967[i].f1", print_hash_value);
        transparent_crc(g_967[i].f2, "g_967[i].f2", print_hash_value);
        transparent_crc(g_967[i].f3, "g_967[i].f3", print_hash_value);
        transparent_crc(g_967[i].f4, "g_967[i].f4", print_hash_value);
        transparent_crc(g_967[i].f5, "g_967[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1011[i].f0, "g_1011[i].f0", print_hash_value);
        transparent_crc(g_1011[i].f1, "g_1011[i].f1", print_hash_value);
        transparent_crc(g_1011[i].f2, "g_1011[i].f2", print_hash_value);
        transparent_crc(g_1011[i].f3, "g_1011[i].f3", print_hash_value);
        transparent_crc(g_1011[i].f4, "g_1011[i].f4", print_hash_value);
        transparent_crc(g_1011[i].f5, "g_1011[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1072[i].f0, "g_1072[i].f0", print_hash_value);
        transparent_crc(g_1072[i].f1, "g_1072[i].f1", print_hash_value);
        transparent_crc(g_1072[i].f2, "g_1072[i].f2", print_hash_value);
        transparent_crc(g_1072[i].f3, "g_1072[i].f3", print_hash_value);
        transparent_crc(g_1072[i].f4, "g_1072[i].f4", print_hash_value);
        transparent_crc(g_1072[i].f5, "g_1072[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1073, "g_1073", print_hash_value);
    transparent_crc(g_1074, "g_1074", print_hash_value);
    transparent_crc(g_1118.f0, "g_1118.f0", print_hash_value);
    transparent_crc(g_1118.f1, "g_1118.f1", print_hash_value);
    transparent_crc(g_1118.f2, "g_1118.f2", print_hash_value);
    transparent_crc(g_1118.f3, "g_1118.f3", print_hash_value);
    transparent_crc(g_1118.f4, "g_1118.f4", print_hash_value);
    transparent_crc(g_1118.f5, "g_1118.f5", print_hash_value);
    transparent_crc(g_1124, "g_1124", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_1133[i].f0, sizeof(g_1133[i].f0), "g_1133[i].f0", print_hash_value);
        transparent_crc(g_1133[i].f1, "g_1133[i].f1", print_hash_value);
        transparent_crc(g_1133[i].f2, "g_1133[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_1194[i][j][k].f0, sizeof(g_1194[i][j][k].f0), "g_1194[i][j][k].f0", print_hash_value);
                transparent_crc(g_1194[i][j][k].f1, "g_1194[i][j][k].f1", print_hash_value);
                transparent_crc(g_1194[i][j][k].f2, "g_1194[i][j][k].f2", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1212[i][j], "g_1212[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1224, "g_1224", print_hash_value);
    transparent_crc(g_1226, "g_1226", print_hash_value);
    transparent_crc(g_1357.f0, "g_1357.f0", print_hash_value);
    transparent_crc(g_1357.f1, "g_1357.f1", print_hash_value);
    transparent_crc(g_1357.f2, "g_1357.f2", print_hash_value);
    transparent_crc(g_1357.f3, "g_1357.f3", print_hash_value);
    transparent_crc(g_1357.f4, "g_1357.f4", print_hash_value);
    transparent_crc(g_1357.f5, "g_1357.f5", print_hash_value);
    transparent_crc(g_1376, "g_1376", print_hash_value);
    transparent_crc_bytes (&g_1407.f0, sizeof(g_1407.f0), "g_1407.f0", print_hash_value);
    transparent_crc(g_1407.f1, "g_1407.f1", print_hash_value);
    transparent_crc(g_1407.f2, "g_1407.f2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1472[i][j][k], "g_1472[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1598, "g_1598", print_hash_value);
    transparent_crc(g_1672.f0, "g_1672.f0", print_hash_value);
    transparent_crc(g_1672.f1, "g_1672.f1", print_hash_value);
    transparent_crc(g_1672.f2, "g_1672.f2", print_hash_value);
    transparent_crc(g_1672.f3, "g_1672.f3", print_hash_value);
    transparent_crc(g_1672.f4, "g_1672.f4", print_hash_value);
    transparent_crc(g_1672.f5, "g_1672.f5", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_1842[i][j][k].f0, sizeof(g_1842[i][j][k].f0), "g_1842[i][j][k].f0", print_hash_value);
                transparent_crc(g_1842[i][j][k].f1, "g_1842[i][j][k].f1", print_hash_value);
                transparent_crc(g_1842[i][j][k].f2, "g_1842[i][j][k].f2", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1864, "g_1864", print_hash_value);
    transparent_crc(g_1892.f0, "g_1892.f0", print_hash_value);
    transparent_crc(g_1892.f1, "g_1892.f1", print_hash_value);
    transparent_crc(g_1892.f2, "g_1892.f2", print_hash_value);
    transparent_crc(g_1892.f3, "g_1892.f3", print_hash_value);
    transparent_crc(g_1892.f4, "g_1892.f4", print_hash_value);
    transparent_crc(g_1892.f5, "g_1892.f5", print_hash_value);
    transparent_crc_bytes (&g_1949.f0, sizeof(g_1949.f0), "g_1949.f0", print_hash_value);
    transparent_crc(g_1949.f1, "g_1949.f1", print_hash_value);
    transparent_crc(g_1949.f2, "g_1949.f2", print_hash_value);
    transparent_crc(g_1974, "g_1974", print_hash_value);
    transparent_crc_bytes (&g_1991.f0, sizeof(g_1991.f0), "g_1991.f0", print_hash_value);
    transparent_crc(g_1991.f1, "g_1991.f1", print_hash_value);
    transparent_crc(g_1991.f2, "g_1991.f2", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2061[i], "g_2061[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2063, "g_2063", print_hash_value);
    transparent_crc(g_2064, "g_2064", print_hash_value);
    transparent_crc(g_2065, "g_2065", print_hash_value);
    transparent_crc(g_2066, "g_2066", print_hash_value);
    transparent_crc(g_2068, "g_2068", print_hash_value);
    transparent_crc(g_2076, "g_2076", print_hash_value);
    transparent_crc_bytes (&g_2084.f0, sizeof(g_2084.f0), "g_2084.f0", print_hash_value);
    transparent_crc(g_2084.f1, "g_2084.f1", print_hash_value);
    transparent_crc(g_2084.f2, "g_2084.f2", print_hash_value);
    transparent_crc_bytes (&g_2096.f0, sizeof(g_2096.f0), "g_2096.f0", print_hash_value);
    transparent_crc(g_2096.f1, "g_2096.f1", print_hash_value);
    transparent_crc(g_2096.f2, "g_2096.f2", print_hash_value);
    transparent_crc_bytes (&g_2135.f0, sizeof(g_2135.f0), "g_2135.f0", print_hash_value);
    transparent_crc(g_2135.f1, "g_2135.f1", print_hash_value);
    transparent_crc(g_2135.f2, "g_2135.f2", print_hash_value);
    transparent_crc(g_2202, "g_2202", print_hash_value);
    transparent_crc_bytes (&g_2221, sizeof(g_2221), "g_2221", print_hash_value);
    transparent_crc_bytes (&g_2222, sizeof(g_2222), "g_2222", print_hash_value);
    transparent_crc_bytes (&g_2238, sizeof(g_2238), "g_2238", print_hash_value);
    transparent_crc_bytes (&g_2319.f0, sizeof(g_2319.f0), "g_2319.f0", print_hash_value);
    transparent_crc(g_2319.f1, "g_2319.f1", print_hash_value);
    transparent_crc(g_2319.f2, "g_2319.f2", print_hash_value);
    transparent_crc_bytes (&g_2336.f0, sizeof(g_2336.f0), "g_2336.f0", print_hash_value);
    transparent_crc(g_2336.f1, "g_2336.f1", print_hash_value);
    transparent_crc(g_2336.f2, "g_2336.f2", print_hash_value);
    transparent_crc_bytes (&g_2337, sizeof(g_2337), "g_2337", print_hash_value);
    transparent_crc(g_2362, "g_2362", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 653
   depth: 1, occurrence: 17
XXX total union variables: 34

XXX non-zero bitfields defined in structs: 6
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 2
XXX structs with bitfields in the program: 32
breakdown:
   indirect level: 0, occurrence: 17
   indirect level: 1, occurrence: 11
   indirect level: 2, occurrence: 1
   indirect level: 3, occurrence: 3
XXX full-bitfields structs in the program: 17
breakdown:
   indirect level: 0, occurrence: 17
XXX times a bitfields struct's address is taken: 19
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 20
XXX times a single bitfield on LHS: 5
XXX times a single bitfield on RHS: 82

XXX max expression depth: 30
breakdown:
   depth: 1, occurrence: 49
   depth: 2, occurrence: 11
   depth: 3, occurrence: 1
   depth: 6, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 21, occurrence: 2
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 30, occurrence: 1

XXX total number of pointers: 546

XXX times a variable address is taken: 1430
XXX times a pointer is dereferenced on RHS: 317
breakdown:
   depth: 1, occurrence: 254
   depth: 2, occurrence: 49
   depth: 3, occurrence: 11
   depth: 4, occurrence: 3
XXX times a pointer is dereferenced on LHS: 356
breakdown:
   depth: 1, occurrence: 323
   depth: 2, occurrence: 22
   depth: 3, occurrence: 10
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 55
XXX times a pointer is compared with address of another variable: 20
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 8036

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1407
   level: 2, occurrence: 220
   level: 3, occurrence: 59
   level: 4, occurrence: 36
   level: 5, occurrence: 6
XXX number of pointers point to pointers: 267
XXX number of pointers point to scalars: 246
XXX number of pointers point to structs: 15
XXX percent of pointers has null in alias set: 31
XXX average alias set size: 1.42

XXX times a non-volatile is read: 1909
XXX times a non-volatile is write: 1036
XXX times a volatile is read: 141
XXX    times read thru a pointer: 44
XXX times a volatile is write: 40
XXX    times written thru a pointer: 15
XXX times a volatile is available for access: 6.39e+03
XXX percentage of non-volatile access: 94.2

XXX forward jumps: 0
XXX backward jumps: 14

XXX stmts: 47
XXX max block depth: 2
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 10
   depth: 2, occurrence: 4

XXX percentage a fresh-made variable is used: 18.4
XXX percentage an existing variable is used: 81.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2686380971
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 0xD16652D0L;


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3
 * writes: g_3
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_2[5];
    int i;
    for (i = 0; i < 5; i++)
        l_2[i] = 0x3FE5E81A7ECC2443LL;
    for (g_3 = 0; (g_3 <= 4); g_3 += 1)
    { /* block id: 3 */
        int32_t l_4[8][8][4] = {{{0x783518F4L,0xC74600EBL,0xAD8EA5F3L,1L},{8L,0x9FACD4A7L,(-5L),0L},{0xEF51AE9AL,0x34CCBB24L,9L,(-8L)},{(-5L),0xAC9D73DFL,0x783518F4L,0xD2AFDDF3L},{(-6L),0x783518F4L,0xBF0A0CD9L,(-1L)},{(-8L),0xAE8EF335L,0x03FA9A18L,0x83FB18BAL},{0x9FACD4A7L,0x45D764CEL,(-5L),8L},{0L,0xEABFD090L,0x848308FAL,0xFE9A5A16L}},{{(-2L),(-9L),0xAC9D73DFL,0L},{0x0F6B34C2L,0x74498B94L,8L,(-1L)},{0xAC9D73DFL,1L,0L,0L},{0xEE3199F2L,0xEE3199F2L,0xEF51AE9AL,0xAE8EF335L},{0x83FB18BAL,0x8D96753FL,8L,0x528ACE4BL},{1L,(-5L),0xB2B534DEL,8L},{(-5L),(-5L),0x34CCBB24L,0x528ACE4BL},{(-5L),0x8D96753FL,0xDB370CC3L,0xAE8EF335L}},{{0x74498B94L,0xFE9A5A16L,0xFF8CD3BEL,1L},{1L,(-6L),1L,0x6F444694L},{0L,0x528ACE4BL,0x848308FAL,0L},{(-8L),9L,0x34CCBB24L,0xEF51AE9AL},{0xD2AFDDF3L,0x9FACD4A7L,7L,0xC74600EBL},{(-1L),0x82D564C7L,4L,0x34CCBB24L},{0x83FB18BAL,0x45D764CEL,1L,0x6F444694L},{8L,0xFF8CD3BEL,(-2L),0xBF0A0CD9L}},{{0xFE9A5A16L,4L,0xC74600EBL,(-1L)},{0L,0x8D96753FL,0xEABFD090L,(-3L)},{(-1L),(-8L),0xB2B534DEL,0xEABFD090L},{0L,(-5L),0L,0x528ACE4BL},{0x1536C745L,(-2L),0x848308FAL,0L},{0x74498B94L,4L,0x5EA6EDADL,(-2L)},{0x6F444694L,(-6L),0x5EA6EDADL,(-3L)},{0x74498B94L,(-9L),0x848308FAL,0x34CCBB24L}},{{0x1536C745L,0xFB29D945L,0L,0xEF51AE9AL},{0L,0xEF51AE9AL,0xB2B534DEL,1L},{(-1L),9L,0xEABFD090L,0xD2AFDDF3L},{0L,0x45D764CEL,0xC74600EBL,(-3L)},{0xFE9A5A16L,(-1L),(-2L),1L},{8L,1L,1L,(-1L)},{0x83FB18BAL,(-2L),4L,(-1L)},{(-1L),(-5L),7L,4L}},{{0xD2AFDDF3L,(-5L),0x34CCBB24L,(-3L)},{(-8L),0x783518F4L,0x848308FAL,0xAE8EF335L},{0L,1L,1L,(-2L)},{1L,0xFF8CD3BEL,0xFF8CD3BEL,1L},{0x74498B94L,0x528ACE4BL,0xDB370CC3L,0xD2AFDDF3L},{(-5L),0xFB29D945L,0x34CCBB24L,0xC74600EBL},{(-5L),6L,0xB2B534DEL,0xC74600EBL},{1L,0xFB29D945L,8L,0xD2AFDDF3L}},{{0x83FB18BAL,0x528ACE4BL,0xEF51AE9AL,1L},{0xFE9A5A16L,0xFF8CD3BEL,1L,(-2L)},{4L,1L,0xC74600EBL,0xAE8EF335L},{(-5L),0x783518F4L,4L,(-3L)},{1L,(-5L),0x0E72EE72L,4L},{0L,(-5L),0xD2AFDDF3L,(-1L)},{(-8L),(-2L),0xDB370CC3L,(-1L)},{(-1L),1L,0x5EA6EDADL,1L}},{{0xAC9D73DFL,(-1L),0xFF8CD3BEL,(-3L)},{0L,0x45D764CEL,0xAD8EA5F3L,0xD2AFDDF3L},{0x1536C745L,9L,0xD2AFDDF3L,1L},{(-5L),0x1536C745L,8L,0x1536C745L},{1L,(-1L),0x9FACD4A7L,0x8D96753FL},{0xD2AFDDF3L,9L,0x1536C745L,1L},{0xC74600EBL,0xAC9D73DFL,0x5EA6EDADL,1L},{0xC74600EBL,1L,0x1536C745L,(-3L)}}};
        int i, j, k;
        return l_4[3][6][2];
    }
    return g_3;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 2
breakdown:
   depth: 1, occurrence: 2
   depth: 2, occurrence: 1

XXX total number of pointers: 0

XXX times a non-volatile is read: 3
XXX times a non-volatile is write: 1
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 3
XXX max block depth: 1
breakdown:
   depth: 0, occurrence: 2
   depth: 1, occurrence: 1

XXX percentage a fresh-made variable is used: 50
XXX percentage an existing variable is used: 50
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


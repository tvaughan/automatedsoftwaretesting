/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1658217465
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   unsigned f0 : 27;
   int64_t  f1;
};

struct S1 {
   signed f0 : 28;
   unsigned f1 : 30;
   signed f2 : 9;
   const signed f3 : 9;
   volatile unsigned f4 : 30;
};

/* --- GLOBAL VARIABLES --- */
static uint8_t g_13 = 0x97L;
static struct S0 g_37 = {4152,0xB6A7F4EA14C5530CLL};
static uint16_t g_54 = 0xE209L;
static int64_t g_59[6][6] = {{0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL},{0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL},{0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL},{0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL},{0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL},{0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL,0x160EC253D5AB883ELL}};
static struct S1 g_67 = {-8145,9934,-8,-18,13068};/* VOLATILE GLOBAL g_67 */
static uint8_t g_73 = 0x1DL;
static int16_t g_75 = 1L;
static int32_t g_77 = 3L;
static uint8_t *g_83 = &g_13;
static uint8_t **g_82 = &g_83;
static uint8_t *** volatile g_81 = &g_82;/* VOLATILE GLOBAL g_81 */
static uint32_t g_97 = 0x9FE43734L;
static float g_103[10][4][1] = {{{0x1.8p-1},{0x9.8p-1},{0x2.7D3B87p-61},{0x5.D76640p-55}},{{0x2.7D3B87p-61},{0x9.8p-1},{0x1.8p-1},{0x9.8p-1}},{{0x2.7D3B87p-61},{0x5.D76640p-55},{0x1.8p-1},{0x5.D76640p-55}},{{0xD.5734C6p+44},{0x5.D76640p-55},{0x1.8p-1},{(-0x3.7p-1)}},{{0x1.8p-1},{0x5.D76640p-55},{0xD.5734C6p+44},{0x5.D76640p-55}},{{0x1.8p-1},{(-0x3.7p-1)},{0x1.8p-1},{0x5.D76640p-55}},{{0xD.5734C6p+44},{0x5.D76640p-55},{0x1.8p-1},{(-0x3.7p-1)}},{{0x1.8p-1},{0x5.D76640p-55},{0xD.5734C6p+44},{0x5.D76640p-55}},{{0x1.8p-1},{(-0x3.7p-1)},{0x1.8p-1},{0x5.D76640p-55}},{{0xD.5734C6p+44},{0x5.D76640p-55},{0x1.8p-1},{(-0x3.7p-1)}}};
static int32_t *g_111 = &g_77;
static volatile int32_t g_129 = 0x79C0CF39L;/* VOLATILE GLOBAL g_129 */
static volatile uint16_t g_131[3][7][6] = {{{0xFAAEL,65535UL,65535UL,2UL,0x73F9L,2UL},{65535UL,0xFAAEL,65535UL,65534UL,0x0029L,1UL},{0x41C1L,65534UL,2UL,0x3D51L,1UL,1UL},{0x3D51L,1UL,1UL,0x3D51L,2UL,65534UL},{0x41C1L,1UL,0x0029L,65534UL,65535UL,0xFAAEL},{65535UL,2UL,0x73F9L,2UL,65535UL,65535UL},{0xFAAEL,1UL,4UL,65535UL,2UL,0x0029L}},{{0x0029L,1UL,1UL,1UL,1UL,0x0029L},{65535UL,65534UL,4UL,65535UL,0x0029L,65535UL},{1UL,0xFAAEL,0x73F9L,0x0029L,0x73F9L,0xFAAEL},{1UL,65535UL,0x0029L,65535UL,4UL,65534UL},{65535UL,0x0029L,1UL,1UL,1UL,1UL},{0x0029L,0x0029L,2UL,65535UL,4UL,1UL},{0xFAAEL,65535UL,65535UL,2UL,0x73F9L,2UL}},{{65535UL,0xFAAEL,65535UL,65534UL,0x0029L,1UL},{0x41C1L,65534UL,2UL,0x3D51L,1UL,1UL},{0x3D51L,1UL,1UL,0x3D51L,2UL,65534UL},{0x41C1L,1UL,0x0029L,65534UL,65535UL,0xFAAEL},{65535UL,2UL,0x73F9L,2UL,65535UL,65535UL},{0xFAAEL,1UL,4UL,65535UL,2UL,0x0029L},{0x0029L,1UL,1UL,1UL,1UL,0x0029L}}};
static float * volatile g_136[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static struct S1 g_143 = {-1989,31371,-7,-11,28221};/* VOLATILE GLOBAL g_143 */
static int8_t g_146 = 0xD2L;
static int32_t ** volatile g_149 = (void*)0;/* VOLATILE GLOBAL g_149 */
static int32_t ** volatile g_150 = &g_111;/* VOLATILE GLOBAL g_150 */
static int32_t g_171[9][4] = {{(-1L),1L,(-1L),(-1L)},{(-1L),(-1L),(-1L),1L},{(-1L),1L,(-1L),(-1L)},{(-1L),(-1L),(-1L),1L},{(-1L),1L,(-1L),(-1L)},{(-1L),(-1L),(-1L),1L},{(-1L),1L,(-1L),(-1L)},{(-1L),(-1L),(-1L),1L},{(-1L),1L,(-1L),(-1L)}};
static int32_t ** volatile g_172 = &g_111;/* VOLATILE GLOBAL g_172 */
static volatile uint8_t g_232 = 0x10L;/* VOLATILE GLOBAL g_232 */
static uint32_t g_251 = 4294967295UL;
static uint64_t g_286 = 0xD434E0CEF9308573LL;
static int8_t g_292 = 0L;
static uint16_t g_293 = 0x9E0EL;
static int8_t g_295 = (-7L);
static uint64_t g_300 = 1UL;
static uint16_t *g_311 = &g_293;
static int8_t *g_318 = &g_292;
static int8_t * volatile * volatile g_317 = &g_318;/* VOLATILE GLOBAL g_317 */
static uint8_t g_353[1][4] = {{0UL,0UL,0UL,0UL}};
static int32_t *g_361 = &g_77;
static int32_t ** volatile g_360 = &g_361;/* VOLATILE GLOBAL g_360 */
static int32_t ** volatile g_375 = &g_111;/* VOLATILE GLOBAL g_375 */
static struct S0 g_377 = {1872,0x5463E3B50CB846D1LL};
static struct S0 * const  volatile g_376 = &g_377;/* VOLATILE GLOBAL g_376 */
static volatile struct S1 * volatile * volatile g_378 = (void*)0;/* VOLATILE GLOBAL g_378 */
static struct S1 g_381 = {-9879,5520,-14,7,17185};/* VOLATILE GLOBAL g_381 */
static int32_t **g_427 = &g_111;
static int32_t ***g_426 = &g_427;
static int32_t **** volatile g_425 = &g_426;/* VOLATILE GLOBAL g_425 */
static struct S1 g_435[3] = {{-75,16338,21,-9,31819},{-75,16338,21,-9,31819},{-75,16338,21,-9,31819}};
static int32_t * volatile * volatile g_448[6] = {&g_111,&g_111,&g_111,&g_111,&g_111,&g_111};
static int32_t * volatile * volatile g_449 = &g_111;/* VOLATILE GLOBAL g_449 */
static uint16_t g_470 = 8UL;
static volatile uint64_t * volatile g_482 = (void*)0;/* VOLATILE GLOBAL g_482 */
static volatile uint64_t * volatile * volatile g_481 = &g_482;/* VOLATILE GLOBAL g_481 */
static volatile uint64_t * volatile * volatile * volatile g_480 = &g_481;/* VOLATILE GLOBAL g_480 */
static struct S0 * volatile g_509 = &g_377;/* VOLATILE GLOBAL g_509 */
static uint64_t *g_533 = &g_286;
static uint64_t **g_532 = &g_533;
static volatile struct S1 g_559 = {10913,23048,6,-11,26892};/* VOLATILE GLOBAL g_559 */
static volatile int16_t g_569 = 1L;/* VOLATILE GLOBAL g_569 */
static volatile struct S1 g_587 = {-10574,12323,-14,3,25630};/* VOLATILE GLOBAL g_587 */
static const uint32_t g_621 = 18446744073709551615UL;
static int8_t g_623 = 0xC7L;
static uint32_t g_672 = 2UL;
static uint8_t g_673 = 1UL;
static int32_t g_798 = 0xBF338A69L;
static int32_t ** volatile g_814 = &g_361;/* VOLATILE GLOBAL g_814 */
static uint64_t g_817[3][8][2] = {{{1UL,18446744073709551615UL},{1UL,0xE3C95C43870D0AACLL},{0xE3C95C43870D0AACLL,1UL},{18446744073709551615UL,1UL},{18446744073709551615UL,1UL},{0xE3C95C43870D0AACLL,0xE3C95C43870D0AACLL},{1UL,18446744073709551615UL},{1UL,18446744073709551615UL}},{{1UL,0xE3C95C43870D0AACLL},{0xE3C95C43870D0AACLL,1UL},{18446744073709551615UL,1UL},{18446744073709551615UL,1UL},{0xE3C95C43870D0AACLL,0xE3C95C43870D0AACLL},{1UL,18446744073709551615UL},{1UL,18446744073709551615UL},{1UL,0xE3C95C43870D0AACLL}},{{0xE3C95C43870D0AACLL,1UL},{18446744073709551615UL,1UL},{18446744073709551615UL,1UL},{0xE3C95C43870D0AACLL,0xE3C95C43870D0AACLL},{1UL,18446744073709551615UL},{1UL,18446744073709551615UL},{1UL,0xE3C95C43870D0AACLL},{0xE3C95C43870D0AACLL,1UL}}};
static float * volatile *g_825[3] = {&g_136[5],&g_136[5],&g_136[5]};
static float * volatile ** volatile g_824 = &g_825[2];/* VOLATILE GLOBAL g_824 */
static int64_t *g_828 = &g_59[1][4];
static const int32_t g_913 = (-4L);
static struct S1 g_942 = {-15201,18582,21,19,15792};/* VOLATILE GLOBAL g_942 */
static struct S1 *g_941 = &g_942;
static float * volatile g_1039 = &g_103[9][0][0];/* VOLATILE GLOBAL g_1039 */
static volatile uint32_t g_1040[10] = {0x3BA69203L,18446744073709551615UL,0x3BA69203L,0x3BA69203L,18446744073709551615UL,0x3BA69203L,0x3BA69203L,18446744073709551615UL,0x3BA69203L,0x3BA69203L};
static const int32_t *g_1043 = &g_77;
static const int32_t ** volatile g_1042 = &g_1043;/* VOLATILE GLOBAL g_1042 */
static volatile uint16_t *g_1068 = (void*)0;
static volatile uint16_t * const  volatile * volatile g_1067 = &g_1068;/* VOLATILE GLOBAL g_1067 */
static volatile uint16_t * const  volatile * volatile *g_1066 = &g_1067;
static uint64_t g_1097 = 0x6582C4FA0F272E2CLL;
static const int32_t **g_1121 = (void*)0;
static const int32_t ***g_1120[2] = {&g_1121,&g_1121};
static const int32_t ****g_1119[4] = {&g_1120[1],&g_1120[1],&g_1120[1],&g_1120[1]};
static int32_t *** const *g_1189 = (void*)0;
static int32_t *** const **g_1188 = &g_1189;
static struct S1 *g_1197 = (void*)0;
static struct S1 ** volatile g_1196 = &g_1197;/* VOLATILE GLOBAL g_1196 */
static uint32_t g_1202 = 1UL;
static volatile struct S1 g_1225[9][8][3] = {{{{9750,19945,-4,-8,5749},{9244,29860,-4,19,21393},{4581,26948,8,11,13337}},{{5134,12733,-9,-6,10745},{-11978,11753,-1,11,4872},{-7099,25952,-19,17,14281}},{{15329,11460,-0,-11,7171},{9750,19945,-4,-8,5749},{-15175,12936,16,1,7714}},{{-15023,12043,18,12,19485},{10262,19554,-11,-13,29860},{-16124,29137,4,8,18230}},{{8908,14911,-4,-15,807},{9750,19945,-4,-8,5749},{14151,10104,19,-13,21377}},{{16014,27492,-21,-9,18792},{-11978,11753,-1,11,4872},{4516,6484,10,11,8657}},{{8026,23268,1,-5,527},{9244,29860,-4,19,21393},{-15023,3858,4,3,24667}},{{15329,11460,-0,-11,7171},{16014,27492,-21,-9,18792},{4581,26948,8,11,13337}}},{{{10262,19554,-11,-13,29860},{-15023,12043,18,12,19485},{5330,19225,-13,11,2932}},{{13649,18144,8,7,11364},{9750,19945,-4,-8,5749},{5330,19225,-13,11,2932}},{{-10876,26455,0,-19,21704},{-10669,31604,3,-14,13012},{4581,26948,8,11,13337}},{{8908,14911,-4,-15,807},{-6156,3968,-1,5,24732},{-15023,3858,4,3,24667}},{{-7679,27503,-11,-12,3784},{-10876,26455,0,-19,21704},{4516,6484,10,11,8657}},{{-15023,12043,18,12,19485},{-7679,27503,-11,-12,3784},{14151,10104,19,-13,21377}},{{13649,18144,8,7,11364},{16014,27492,-21,-9,18792},{-16124,29137,4,8,18230}},{{-10382,12708,-16,-11,11694},{-11978,11753,-1,11,4872},{-15175,12936,16,1,7714}}},{{{13649,18144,8,7,11364},{-6156,3968,-1,5,24732},{-7099,25952,-19,17,14281}},{{-15023,12043,18,12,19485},{-10382,12708,-16,-11,11694},{4581,26948,8,11,13337}},{{-7679,27503,-11,-12,3784},{15329,11460,-0,-11,7171},{14151,10104,19,-13,21377}},{{8908,14911,-4,-15,807},{-10876,26455,0,-19,21704},{13488,30638,-20,-11,15936}},{{-10876,26455,0,-19,21704},{9244,29860,-4,19,21393},{-9124,4537,2,0,10631}},{{13649,18144,8,7,11364},{9244,29860,-4,19,21393},{7081,31003,-8,-21,1690}},{{10262,19554,-11,-13,29860},{-10876,26455,0,-19,21704},{-15175,12936,16,1,7714}},{{15329,11460,-0,-11,7171},{15329,11460,-0,-11,7171},{5330,19225,-13,11,2932}}},{{{8026,23268,1,-5,527},{-10382,12708,-16,-11,11694},{-16124,29137,4,8,18230}},{{16014,27492,-21,-9,18792},{-6156,3968,-1,5,24732},{-9124,4537,2,0,10631}},{{8908,14911,-4,-15,807},{-11978,11753,-1,11,4872},{-14198,32626,-0,-15,9319}},{{-15023,12043,18,12,19485},{16014,27492,-21,-9,18792},{-9124,4537,2,0,10631}},{{15329,11460,-0,-11,7171},{-7679,27503,-11,-12,3784},{-16124,29137,4,8,18230}},{{5134,12733,-9,-6,10745},{-10876,26455,0,-19,21704},{5330,19225,-13,11,2932}},{{9750,19945,-4,-8,5749},{-6156,3968,-1,5,24732},{-15175,12936,16,1,7714}},{{8026,23268,1,-5,527},{-10669,31604,3,-14,13012},{7081,31003,-8,-21,1690}}},{{{-7679,27503,-11,-12,3784},{9750,19945,-4,-8,5749},{-9124,4537,2,0,10631}},{{-7679,27503,-11,-12,3784},{-15023,12043,18,12,19485},{13488,30638,-20,-11,15936}},{{1306,30903,-7,-11,24721},{7468,11933,10,-17,12059},{5134,12733,-9,-6,10745}},{{981,21201,21,7,27919},{10171,2619,12,-5,29647},{-1520,27033,-21,-6,14561}},{{-2988,6028,14,-20,21418},{-1493,15814,-12,8,12748},{-3481,9446,-9,-18,11808}},{{-7756,14284,-5,0,22310},{981,21201,21,7,27919},{-6156,3968,-1,5,24732}},{{-15540,24199,4,11,10134},{92,16025,2,1,22381},{-16337,629,-17,4,8612}},{{-589,27442,-20,-6,13863},{981,21201,21,7,27919},{5134,12733,-9,-6,10745}}},{{{7468,11933,10,-17,12059},{-1493,15814,-12,8,12748},{-14395,10988,-20,-17,5761}},{{1306,30903,-7,-11,24721},{10171,2619,12,-5,29647},{-556,28638,0,6,10418}},{{-7756,14284,-5,0,22310},{7468,11933,10,-17,12059},{-1520,27033,-21,-6,14561}},{{92,16025,2,1,22381},{-15540,24199,4,11,10134},{13649,18144,8,7,11364}},{{-12154,24670,-14,-1,12980},{981,21201,21,7,27919},{13649,18144,8,7,11364}},{{5281,31404,-6,-1,31390},{10990,9724,-4,-20,30808},{-1520,27033,-21,-6,14561}},{{-589,27442,-20,-6,13863},{16198,1146,-19,-2,24161},{-556,28638,0,6,10418}},{{-2139,30467,-12,21,22080},{5281,31404,-6,-1,31390},{-14395,10988,-20,-17,5761}}},{{{-15540,24199,4,11,10134},{-2139,30467,-12,21,22080},{5134,12733,-9,-6,10745}},{{-12154,24670,-14,-1,12980},{7468,11933,10,-17,12059},{-16337,629,-17,4,8612}},{{16091,19136,1,10,3255},{-1493,15814,-12,8,12748},{-6156,3968,-1,5,24732}},{{-12154,24670,-14,-1,12980},{16198,1146,-19,-2,24161},{-3481,9446,-9,-18,11808}},{{-15540,24199,4,11,10134},{16091,19136,1,10,3255},{-1520,27033,-21,-6,14561}},{{-2139,30467,-12,21,22080},{-7756,14284,-5,0,22310},{5134,12733,-9,-6,10745}},{{-589,27442,-20,-6,13863},{5281,31404,-6,-1,31390},{-8768,30056,-4,12,25706}},{{5281,31404,-6,-1,31390},{10171,2619,12,-5,29647},{-10669,31604,3,-14,13012}}},{{{-12154,24670,-14,-1,12980},{10171,2619,12,-5,29647},{1277,23177,14,0,26914}},{{92,16025,2,1,22381},{5281,31404,-6,-1,31390},{-6156,3968,-1,5,24732}},{{-7756,14284,-5,0,22310},{-7756,14284,-5,0,22310},{13649,18144,8,7,11364}},{{1306,30903,-7,-11,24721},{16091,19136,1,10,3255},{-16337,629,-17,4,8612}},{{7468,11933,10,-17,12059},{16198,1146,-19,-2,24161},{-10669,31604,3,-14,13012}},{{-589,27442,-20,-6,13863},{-1493,15814,-12,8,12748},{-3306,27901,7,-12,14792}},{{-15540,24199,4,11,10134},{7468,11933,10,-17,12059},{-10669,31604,3,-14,13012}},{{-7756,14284,-5,0,22310},{-2139,30467,-12,21,22080},{-16337,629,-17,4,8612}}},{{{-2988,6028,14,-20,21418},{5281,31404,-6,-1,31390},{13649,18144,8,7,11364}},{{981,21201,21,7,27919},{16198,1146,-19,-2,24161},{-6156,3968,-1,5,24732}},{{1306,30903,-7,-11,24721},{10990,9724,-4,-20,30808},{1277,23177,14,0,26914}},{{-2139,30467,-12,21,22080},{981,21201,21,7,27919},{-10669,31604,3,-14,13012}},{{-2139,30467,-12,21,22080},{-15540,24199,4,11,10134},{-8768,30056,-4,12,25706}},{{1306,30903,-7,-11,24721},{7468,11933,10,-17,12059},{5134,12733,-9,-6,10745}},{{981,21201,21,7,27919},{10171,2619,12,-5,29647},{-1520,27033,-21,-6,14561}},{{-2988,6028,14,-20,21418},{-1493,15814,-12,8,12748},{-3481,9446,-9,-18,11808}}}};
static uint8_t g_1242 = 0x57L;
static volatile int32_t * const g_1269 = &g_129;
static volatile int32_t *g_1271 = &g_129;
static volatile int32_t ** volatile g_1270 = &g_1271;/* VOLATILE GLOBAL g_1270 */
static struct S1 g_1314 = {-2250,32114,21,-3,20431};/* VOLATILE GLOBAL g_1314 */
static volatile uint32_t g_1319 = 1UL;/* VOLATILE GLOBAL g_1319 */
static float *g_1375 = &g_103[1][0][0];
static struct S1 g_1381 = {15927,862,5,2,25446};/* VOLATILE GLOBAL g_1381 */
static uint8_t g_1505[7][8][4] = {{{5UL,0UL,255UL,0x89L},{0x00L,0xFDL,0x49L,0UL},{255UL,2UL,0x49L,251UL},{0x00L,0xEEL,255UL,0x91L},{5UL,0x24L,255UL,0xC7L},{255UL,0xC7L,255UL,0xC7L},{0x49L,0x24L,0x94L,0x91L},{253UL,0xEEL,0x81L,251UL}},{{0x94L,2UL,1UL,0UL},{0x94L,0xFDL,0x81L,0x89L},{253UL,0UL,0x94L,0xEEL},{0x49L,0x6CL,255UL,0xFDL},{255UL,0x6CL,255UL,0xEEL},{5UL,0UL,255UL,0x89L},{0x00L,0xFDL,0x49L,0UL},{255UL,2UL,0x49L,251UL}},{{0x00L,0xEEL,255UL,0x91L},{5UL,0x24L,255UL,0xC7L},{255UL,0xC7L,255UL,0xC7L},{0x49L,0x19L,0x81L,0UL},{255UL,0x24L,5UL,0xFDL},{0x81L,251UL,0x49L,2UL},{0x81L,0xC7L,5UL,0xEEL},{255UL,2UL,0x81L,0x24L}},{{255UL,0x89L,253UL,0xC7L},{1UL,0x89L,1UL,0x24L},{0x00L,2UL,255UL,0xEEL},{247UL,0xC7L,255UL,2UL},{0x94L,251UL,255UL,0xFDL},{247UL,0x24L,255UL,0UL},{0x00L,0x19L,1UL,0x6CL},{1UL,0x6CL,253UL,0x6CL}},{{255UL,0x19L,0x81L,0UL},{255UL,0x24L,5UL,0xFDL},{0x81L,251UL,0x49L,2UL},{0x81L,0xC7L,5UL,0xEEL},{255UL,2UL,0x81L,0x24L},{255UL,0x89L,253UL,0xC7L},{1UL,0x89L,1UL,0x24L},{0x00L,2UL,255UL,0xEEL}},{{247UL,0xC7L,255UL,2UL},{0x94L,251UL,255UL,0xFDL},{247UL,0x24L,255UL,0UL},{0x00L,0x19L,1UL,0x6CL},{1UL,0x6CL,253UL,0x6CL},{255UL,0x19L,0x81L,0UL},{255UL,0x24L,5UL,0xFDL},{0x81L,251UL,0x49L,2UL}},{{0x81L,0xC7L,5UL,0xEEL},{255UL,2UL,0x81L,0x24L},{255UL,0x89L,253UL,0xC7L},{1UL,0x89L,1UL,0x24L},{0x00L,2UL,255UL,0xEEL},{247UL,0xC7L,255UL,2UL},{0x94L,251UL,255UL,0xFDL},{247UL,0x24L,255UL,0UL}}};
static uint32_t **** volatile g_1586 = (void*)0;/* VOLATILE GLOBAL g_1586 */
static uint32_t *g_1590 = &g_251;
static uint32_t **g_1589 = &g_1590;
static uint32_t ***g_1588 = &g_1589;
static uint32_t **** volatile g_1587 = &g_1588;/* VOLATILE GLOBAL g_1587 */
static struct S0 *g_1611[8] = {&g_37,&g_37,&g_37,&g_37,&g_37,&g_37,&g_37,&g_37};
static struct S0 ** volatile g_1610 = &g_1611[3];/* VOLATILE GLOBAL g_1610 */
static int16_t g_1654 = 0L;
static volatile struct S1 g_1656 = {-5472,29796,4,-9,2772};/* VOLATILE GLOBAL g_1656 */
static const int32_t ** volatile g_1661[9][5] = {{&g_1043,&g_1043,&g_1043,&g_1043,&g_1043},{&g_1043,&g_1043,(void*)0,&g_1043,(void*)0},{&g_1043,(void*)0,&g_1043,&g_1043,&g_1043},{&g_1043,&g_1043,(void*)0,(void*)0,(void*)0},{&g_1043,&g_1043,&g_1043,&g_1043,(void*)0},{&g_1043,&g_1043,(void*)0,&g_1043,&g_1043},{&g_1043,&g_1043,&g_1043,&g_1043,(void*)0},{&g_1043,&g_1043,&g_1043,&g_1043,&g_1043},{&g_1043,&g_1043,&g_1043,&g_1043,(void*)0}};
static const int32_t ** volatile g_1662 = &g_1043;/* VOLATILE GLOBAL g_1662 */
static int8_t **** volatile g_1695 = (void*)0;/* VOLATILE GLOBAL g_1695 */
static int16_t *g_1730 = (void*)0;
static volatile int16_t g_1754 = 2L;/* VOLATILE GLOBAL g_1754 */
static struct S0 * volatile g_1761 = &g_377;/* VOLATILE GLOBAL g_1761 */
static uint64_t g_1777 = 0x17270F8F98642D8CLL;
static struct S0 * volatile g_1810 = (void*)0;/* VOLATILE GLOBAL g_1810 */
static struct S0 * volatile g_1812 = &g_37;/* VOLATILE GLOBAL g_1812 */
static int32_t **** volatile g_1818 = &g_426;/* VOLATILE GLOBAL g_1818 */
static struct S0 * volatile g_1870 = (void*)0;/* VOLATILE GLOBAL g_1870 */
static struct S1 g_1920 = {7682,24900,1,-6,16394};/* VOLATILE GLOBAL g_1920 */
static float g_1945 = 0x5.129392p+62;
static uint16_t g_1981 = 2UL;
static struct S0 **g_1987 = (void*)0;
static uint8_t ***g_2067 = &g_82;
static float **g_2075 = &g_1375;
static float ***g_2074 = &g_2075;
static uint32_t g_2101 = 0xE11B22FEL;
static volatile uint32_t g_2116 = 0UL;/* VOLATILE GLOBAL g_2116 */
static uint32_t g_2192 = 1UL;
static int8_t **** volatile *g_2217 = &g_1695;
static struct S1 g_2223 = {11365,6358,1,-21,11576};/* VOLATILE GLOBAL g_2223 */
static volatile int32_t * volatile *g_2250 = (void*)0;
static struct S0 * volatile g_2252 = &g_377;/* VOLATILE GLOBAL g_2252 */
static uint32_t g_2255[1] = {4294967289UL};
static struct S0 * volatile g_2257 = &g_377;/* VOLATILE GLOBAL g_2257 */
static volatile uint64_t g_2281 = 1UL;/* VOLATILE GLOBAL g_2281 */
static int32_t *g_2350 = (void*)0;
static int32_t **g_2349 = &g_2350;
static int32_t ***g_2348 = &g_2349;
static struct S0 ** const  volatile g_2370[2][1] = {{(void*)0},{(void*)0}};
static float * volatile g_2438 = &g_1945;/* VOLATILE GLOBAL g_2438 */
static struct S1 g_2490[9][2] = {{{16225,17774,-0,19,28946},{-10446,18708,19,-4,10671}},{{16225,17774,-0,19,28946},{-12599,13269,-13,20,14940}},{{-7995,1602,6,-9,20072},{16225,17774,-0,19,28946}},{{-12599,13269,-13,20,14940},{-10446,18708,19,-4,10671}},{{-8045,11021,2,11,2056},{-8045,11021,2,11,2056}},{{-7995,1602,6,-9,20072},{-8045,11021,2,11,2056}},{{-8045,11021,2,11,2056},{-10446,18708,19,-4,10671}},{{-12599,13269,-13,20,14940},{16225,17774,-0,19,28946}},{{-7995,1602,6,-9,20072},{-12599,13269,-13,20,14940}}};
static uint64_t g_2501 = 0x3AFF28C12B718022LL;
static volatile uint64_t * volatile ** volatile *g_2510 = (void*)0;
static struct S1 g_2556 = {-15382,31986,-8,8,17109};/* VOLATILE GLOBAL g_2556 */
static struct S1 g_2557 = {10195,12790,21,-7,30847};/* VOLATILE GLOBAL g_2557 */
static struct S1 g_2558 = {13598,9235,21,2,21747};/* VOLATILE GLOBAL g_2558 */
static uint8_t **** volatile g_2580[6][2] = {{&g_2067,&g_2067},{&g_2067,&g_2067},{&g_2067,&g_2067},{&g_2067,&g_2067},{&g_2067,&g_2067},{&g_2067,&g_2067}};
static int32_t * volatile * volatile g_2593 = &g_111;/* VOLATILE GLOBAL g_2593 */
static const int64_t g_2596 = 0x6F32E0827B6874CDLL;
static struct S1 ***g_2608 = (void*)0;
static struct S1 **** volatile g_2607[4] = {&g_2608,&g_2608,&g_2608,&g_2608};
static struct S1 ** volatile g_2635 = &g_1197;/* VOLATILE GLOBAL g_2635 */
static int32_t ** volatile g_2638[9][2] = {{(void*)0,(void*)0},{&g_111,&g_361},{&g_111,(void*)0},{(void*)0,&g_111},{(void*)0,&g_361},{&g_111,&g_111},{(void*)0,&g_111},{&g_111,&g_361},{(void*)0,&g_111}};
static int32_t ** volatile g_2639[6] = {&g_361,&g_361,&g_361,&g_361,&g_361,&g_361};
static uint32_t g_2652 = 18446744073709551612UL;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t  func_2(int32_t  p_3, int32_t  p_4, uint32_t  p_5, uint16_t  p_6);
static uint64_t  func_9(uint8_t  p_10, float  p_11, uint8_t  p_12);
static float  func_14(int8_t  p_15, int64_t  p_16, uint32_t  p_17);
static int8_t  func_18(uint32_t  p_19, int16_t  p_20, uint32_t  p_21);
static uint32_t  func_22(uint16_t  p_23, int32_t  p_24, int32_t  p_25, uint16_t  p_26);
static uint8_t  func_30(float  p_31, int64_t  p_32, float  p_33);
static int64_t  func_34(struct S0  p_35, int32_t  p_36);
static uint8_t  func_40(uint8_t  p_41, uint16_t  p_42);
static int64_t  func_45(int8_t  p_46, int32_t  p_47);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_13 g_37 g_67 g_54 g_75 g_77 g_81 g_82 g_73 g_381.f4 g_587 g_143.f2 g_311 g_293 g_136 g_318 g_111 g_621 g_623 g_426 g_427 g_361 g_292 g_377.f1 g_251 g_672 g_673 g_150 g_59 g_470 g_509 g_480 g_481 g_353 g_532 g_533 g_286 g_360 g_300 g_376 g_377 g_798 g_317 g_814 g_817 g_824 g_83 g_825 g_143.f0 g_482 g_172 g_103 g_375 g_97 g_232 g_435.f2 g_1039 g_1040 g_1042 g_146 g_1043 g_131 g_435.f3 g_143.f3 g_1269 g_129 g_2223 g_2074 g_2075 g_1375 g_1271 g_1981 g_1777 g_2252 g_1654 g_2255 g_435.f0 g_2257 g_1270 g_1589 g_1590 g_2281 g_1505 g_1587 g_1588 g_913 g_2348
 * writes: g_54 g_59 g_73 g_75 g_77 g_82 g_97 g_67.f2 g_67.f0 g_103 g_293 g_146 g_292 g_295 g_623 g_427 g_377.f1 g_251 g_470 g_377 g_111 g_300 g_361 g_817 g_825 g_828 g_286 g_672 g_941 g_353 g_673 g_1043 g_129 g_37.f1 g_1981 g_1777 g_2250 g_798 g_1097 g_2281 g_1611 g_2348
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int64_t l_27 = 0xB9725245F5B1B6FDLL;
    uint16_t *l_52 = (void*)0;
    uint16_t *l_53 = &g_54;
    int32_t *l_105 = &g_77;
    uint8_t *l_106 = &g_73;
    int8_t l_1548 = (-4L);
    int16_t l_1549[1][3];
    int16_t l_1553[10][3] = {{0L,0x6984L,1L},{9L,0L,(-1L)},{0xCC98L,0xCC98L,0xE7DBL},{9L,0xE7DBL,0L},{0L,0xE7DBL,9L},{0xE7DBL,0xCC98L,0xCC98L},{(-1L),0L,9L},{1L,0x6984L,0L},{1L,0xFB73L,0xE7DBL},{(-1L),(-1L),(-1L)}};
    int32_t l_1555 = (-6L);
    int32_t l_2290 = (-1L);
    int32_t l_2294 = 1L;
    int32_t l_2297 = 0xCAAC7F83L;
    int32_t l_2301 = 0x5A491C2EL;
    uint16_t l_2320 = 7UL;
    int32_t *l_2329 = &g_77;
    int16_t l_2331 = 0x10A3L;
    float l_2333 = 0x8.E00457p+87;
    int32_t ***l_2352 = &g_2349;
    struct S1 **l_2448[3];
    int8_t **l_2454 = &g_318;
    int8_t *** const l_2453 = &l_2454;
    int8_t *** const *l_2452 = &l_2453;
    int8_t *** const **l_2451 = &l_2452;
    uint32_t l_2457 = 0x4AF86A2FL;
    int32_t *l_2513 = &l_2294;
    uint32_t l_2543[7] = {0UL,1UL,0UL,0UL,1UL,0UL,0UL};
    const struct S0 l_2554 = {8992,0L};
    const float ***l_2605 = (void*)0;
    struct S1 ***l_2610[2][1][7];
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_1549[i][j] = (-1L);
    }
    for (i = 0; i < 3; i++)
        l_2448[i] = &g_1197;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 7; k++)
                l_2610[i][j][k] = (void*)0;
        }
    }
    if (func_2((safe_rshift_func_int8_t_s_s((((*g_533) = func_9(g_13, func_14(func_18(func_22((((*l_105) = ((l_27 | (safe_rshift_func_int8_t_s_u(5L, 2))) , func_30(g_13, func_34(g_37, (safe_rshift_func_uint8_t_u_u(func_40(((*l_106) = (((*l_105) = (safe_sub_func_int64_t_s_s((l_27 || func_45((safe_rshift_func_int16_t_s_u((safe_mul_func_uint8_t_u_u((((((*l_53) = l_27) >= l_27) >= ((((((g_37.f0 != l_27) & g_37.f1) , 0x58D83F7CD3E5C7D8LL) , l_53) != l_53) || l_27)) && g_37.f1), g_37.f0)), 14)), g_37.f0)), g_67.f3))) , (*l_105))), g_13), 3))), l_27))) , (*l_105)), l_1548, l_27, l_1549[0][0]), g_37.f0, l_1553[1][0]), g_143.f3, l_1555), (*g_83))) , (**g_317)), 3)), g_1654, g_2255[0], g_435[2].f0))
    { /* block id: 1068 */
        int8_t l_2277 = 0x33L;
        int32_t l_2279 = 0xCB577C89L;
        int32_t l_2280 = (-9L);
        int32_t l_2291 = (-9L);
        int32_t l_2292 = 0xA056AB27L;
        int32_t l_2293 = 0x91C3253EL;
        int32_t l_2295 = 0x16DA7510L;
        int32_t l_2299[4][3] = {{3L,3L,0xCD1AFAFEL},{0xB2CD053CL,0xB2CD053CL,1L},{3L,3L,0xCD1AFAFEL},{0xB2CD053CL,0xB2CD053CL,1L}};
        uint16_t l_2303 = 0UL;
        int i, j;
lbl_2326:
        (*g_1269) |= (*l_105);
        for (g_798 = 1; (g_798 >= 0); g_798 -= 1)
        { /* block id: 1072 */
            struct S0 l_2256[8][4][4] = {{{{10449,-7L},{7493,0x745333E9927C8B65LL},{10449,-7L},{4640,-3L}},{{4293,0xA1C2E72957769D2BLL},{5253,-10L},{11566,0x33D700105A9EF877LL},{8279,-1L}},{{718,-8L},{4830,-1L},{1705,0x0021D5063E64A9CFLL},{5253,-10L}},{{9748,0x8F1836A1E2A06C7FLL},{10106,0xF05783C8453058CFLL},{1705,0x0021D5063E64A9CFLL},{666,0L}}},{{{718,-8L},{4516,4L},{11566,0x33D700105A9EF877LL},{7879,-1L}},{{4293,0xA1C2E72957769D2BLL},{1265,0x161F2D2A5BF5A22CLL},{10449,-7L},{11566,0x33D700105A9EF877LL}},{{10449,-7L},{11566,0x33D700105A9EF877LL},{10306,0xA18972C45B591A1CLL},{8195,-5L}},{{5153,9L},{9584,0xA555018014BE1A95LL},{9966,0x7BF11C72B189C229LL},{2016,4L}}},{{{5253,-10L},{10106,0xF05783C8453058CFLL},{8377,0L},{1705,0x0021D5063E64A9CFLL}},{{1596,0x14194B381A1C12F2LL},{2828,-6L},{2895,0L},{7540,0xDC3CBC4D2BB94D77LL}},{{10203,0L},{2049,0L},{4293,0xA1C2E72957769D2BLL},{4640,-3L}},{{10449,-7L},{5244,0x429D93D9F89B89B8LL},{2016,4L},{304,0L}}},{{{1392,-3L},{5253,-10L},{3197,-1L},{3575,-3L}},{{2895,0L},{4061,0x581EC702C2E78CB3LL},{1705,0x0021D5063E64A9CFLL},{1705,0x0021D5063E64A9CFLL}},{{4460,1L},{4460,1L},{5253,-10L},{666,0L}},{{9367,1L},{895,-8L},{10203,0L},{4830,-1L}}},{{{4293,0xA1C2E72957769D2BLL},{11566,0x33D700105A9EF877LL},{2016,4L},{10203,0L}},{{1708,-1L},{11566,0x33D700105A9EF877LL},{3376,1L},{4830,-1L}},{{423,0L},{1392,-3L},{2887,-1L},{4061,0x581EC702C2E78CB3LL}},{{9748,0x8F1836A1E2A06C7FLL},{4640,-3L},{304,0L},{10106,0xF05783C8453058CFLL}}},{{{8377,0L},{1596,0x14194B381A1C12F2LL},{666,0L},{5964,0xF6460E12876B70CDLL}},{{2782,0x82663AC4CCC6DB80LL},{7255,0xCAB95C1456CF16B0LL},{9367,1L},{3197,-1L}},{{4830,-1L},{8279,-1L},{4061,0x581EC702C2E78CB3LL},{10203,0L}},{{9367,1L},{4460,1L},{7493,0x745333E9927C8B65LL},{895,-8L}}},{{{2016,4L},{5253,-10L},{7255,0xCAB95C1456CF16B0LL},{10106,0xF05783C8453058CFLL}},{{4601,0x8A701300C51033E6LL},{2222,0xF803D108DC1D8A1ALL},{8377,0L},{8195,-5L}},{{666,0L},{4293,0xA1C2E72957769D2BLL},{423,0L},{1705,0x0021D5063E64A9CFLL}},{{2895,0L},{423,0L},{4061,0x581EC702C2E78CB3LL},{423,0L}}},{{{8195,-5L},{2119,0x9E178AE6044E135DLL},{3695,1L},{2049,0L}},{{5244,0x429D93D9F89B89B8LL},{10306,0xA18972C45B591A1CLL},{10449,-7L},{4061,0x581EC702C2E78CB3LL}},{{8377,0L},{2222,0xF803D108DC1D8A1ALL},{4601,0x8A701300C51033E6LL},{7255,0xCAB95C1456CF16B0LL}},{{8377,0L},{8149,1L},{10449,-7L},{5595,0xD95E8458E222CBA1LL}}}};
            int32_t l_2271 = 0x2159FF6EL;
            int16_t l_2296 = 0x6A40L;
            int32_t l_2302 = 0x11BE2107L;
            const float l_2324[6][2][4] = {{{0x8.CE3E63p-55,0x0.ED3C15p+4,0xD.2C87A7p+10,0x0.ED3C15p+4},{0x0.ED3C15p+4,0xA.233712p+12,0xD.2C87A7p+10,0xD.2C87A7p+10}},{{0x8.CE3E63p-55,0x8.CE3E63p-55,0x0.ED3C15p+4,0xD.2C87A7p+10},{0x7.9E672Cp+32,0xA.233712p+12,0x7.9E672Cp+32,0x0.ED3C15p+4}},{{0x7.9E672Cp+32,0x0.ED3C15p+4,0x0.ED3C15p+4,0x7.9E672Cp+32},{0x8.CE3E63p-55,0x0.ED3C15p+4,0xD.2C87A7p+10,0x0.ED3C15p+4}},{{0x0.ED3C15p+4,0xA.233712p+12,0xD.2C87A7p+10,0xD.2C87A7p+10},{0x8.CE3E63p-55,0x8.CE3E63p-55,0x0.ED3C15p+4,0xD.2C87A7p+10}},{{0x7.9E672Cp+32,0xA.233712p+12,0x7.9E672Cp+32,0x0.ED3C15p+4},{0x7.9E672Cp+32,0x0.ED3C15p+4,0x0.ED3C15p+4,0x7.9E672Cp+32}},{{0x8.CE3E63p-55,0x0.ED3C15p+4,0xD.2C87A7p+10,0x0.ED3C15p+4},{0x0.ED3C15p+4,0xA.233712p+12,0xD.2C87A7p+10,0xD.2C87A7p+10}}};
            int i, j, k;
            (*g_2257) = l_2256[0][2][0];
            for (l_27 = 0; (l_27 <= 1); l_27 += 1)
            { /* block id: 1076 */
                uint32_t l_2270 = 0xDE9180C2L;
                int16_t *l_2274 = &l_1549[0][0];
                int64_t *l_2278 = &g_59[3][3];
                int32_t l_2284 = (-1L);
                int32_t l_2298 = 0x22B4026FL;
                int32_t l_2300[1][10][9] = {{{(-2L),(-1L),0xAED10E5DL,0xAED10E5DL,(-1L),(-2L),0L,9L,(-10L)},{0x17AB7AD7L,0x121E2721L,0xAED10E5DL,0x4E9F68DCL,9L,(-1L),0xB2F901E6L,0xB2F901E6L,(-1L)},{0xAED10E5DL,0xB2F901E6L,(-10L),0xB2F901E6L,0xAED10E5DL,0x17AB7AD7L,0L,(-1L),0xAC701D5EL},{0x957B9589L,0xB2F901E6L,0x17AB7AD7L,0xAC701D5EL,0x121E2721L,0x72BB11D7L,(-2L),0x72BB11D7L,0x121E2721L},{0L,0x121E2721L,0x121E2721L,0L,(-1L),0x17AB7AD7L,(-10L),(-2L),6L},{0L,(-1L),0x8E74B540L,0x17AB7AD7L,6L,(-1L),(-1L),6L,0x17AB7AD7L},{0x957B9589L,0x72BB11D7L,0x957B9589L,0x8E74B540L,(-1L),(-2L),0xAC701D5EL,0x4E9F68DCL,0x17AB7AD7L},{0xAED10E5DL,0x957B9589L,6L,(-10L),0x121E2721L,0x4E9F68DCL,0x121E2721L,(-10L),6L},{0x17AB7AD7L,0x17AB7AD7L,0x72BB11D7L,0x8E74B540L,0xAED10E5DL,0x957B9589L,6L,(-10L),0x121E2721L},{(-2L),0xAC701D5EL,0x4E9F68DCL,0x17AB7AD7L,9L,0x8E74B540L,(-10L),0xAED10E5DL,(-2L)}}};
                struct S0 l_2325 = {4850,0x8E92EDA8ED40A165LL};
                int i, j, k;
                l_2280 ^= (safe_sub_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u(((-3L) || (!(safe_mod_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s((safe_sub_func_uint16_t_u_u(((void*)0 == &g_1818), ((*l_2274) = ((+(l_2271 = ((*g_111) |= l_2270))) <= (safe_rshift_func_uint8_t_u_s(l_2256[0][2][0].f0, ((*g_318) = (*g_318)))))))), 5)), (safe_div_func_uint8_t_u_u(((void*)0 == &g_1730), (l_2279 = ((((*l_2278) = ((0xF3L <= l_2277) >= l_2270)) , (**g_1270)) & 3L)))))))), (*g_311))), (**g_1589)));
                for (g_1097 = 0; (g_1097 <= 3); g_1097 += 1)
                { /* block id: 1086 */
                    g_2281--;
                }
                for (g_673 = 0; (g_673 <= 3); g_673 += 1)
                { /* block id: 1091 */
                    int8_t l_2285 = 7L;
                    int32_t *l_2286 = &g_77;
                    int32_t l_2287 = (-1L);
                    int32_t *l_2288 = &g_77;
                    int32_t *l_2289[9];
                    int i, j, k;
                    for (i = 0; i < 9; i++)
                        l_2289[i] = &l_2271;
                    ++l_2303;
                    if ((((((((safe_rshift_func_int8_t_s_s((((safe_mul_func_int16_t_s_s(g_817[l_27][(g_673 + 3)][l_27], 0xF543L)) & g_817[(g_798 + 1)][(l_27 + 4)][g_798]) > (safe_unary_minus_func_uint32_t_u(g_1505[(l_27 + 1)][(l_27 + 1)][g_673]))), 7)) , (safe_mod_func_uint64_t_u_u((l_2303 & ((((0x95L & (((*g_533) < (~((*l_2288) = l_2284))) & (safe_add_func_uint16_t_u_u(((+(safe_div_func_int64_t_s_s(((void*)0 == &l_2287), l_2300[0][4][4]))) , l_2293), l_2299[3][0])))) == 0xAEDDL) != 0x1DL) & g_377.f1)), 0x9EBA1E0D8B48D68DLL))) | g_292) , 7L) && (****g_1587)) >= 0xFE7DF283L) , 0x9AA0147AL))
                    { /* block id: 1094 */
                        int64_t l_2319 = 0x1F23B110C93B4E1ALL;
                        return l_2319;
                    }
                    else
                    { /* block id: 1096 */
                        struct S0 **l_2323 = &g_1611[3];
                        --l_2320;
                        (*l_2323) = (void*)0;
                        l_2299[0][0] ^= (**g_375);
                        l_2256[7][1][0] = l_2325;
                    }
                    if (g_798)
                        goto lbl_2326;
                }
            }
        }
    }
    else
    { /* block id: 1106 */
        return (*l_105);
    }
    for (g_672 = 0; (g_672 == 38); g_672++)
    { /* block id: 1111 */
        l_2329 = (void*)0;
        return (**g_1042);
    }
    (**g_375) |= 0x84225189L;
    if ((safe_unary_minus_func_uint8_t_u((*l_2329))))
    { /* block id: 1116 */
        uint64_t l_2332 = 0x15B45BEAC3EFC167LL;
        int32_t *l_2334 = &l_2297;
        struct S1 **l_2446[2][9][3] = {{{&g_941,&g_1197,&g_941},{&g_941,(void*)0,(void*)0},{&g_941,&g_941,(void*)0},{&g_941,&g_941,&g_1197},{&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941},{&g_941,&g_941,&g_941},{(void*)0,&g_941,&g_941},{&g_1197,&g_941,&g_941}},{{&g_941,(void*)0,&g_941},{&g_941,&g_1197,&g_941},{&g_941,&g_941,&g_1197},{&g_1197,&g_941,(void*)0},{(void*)0,&g_941,(void*)0},{&g_941,&g_1197,&g_941},{&g_941,(void*)0,(void*)0},{&g_941,&g_941,(void*)0},{&g_941,&g_941,&g_1197}}};
        int8_t l_2447 = 0L;
        int32_t *l_2459 = &l_2294;
        uint64_t l_2494 = 0x65C56C76D5697FC6LL;
        uint32_t l_2497 = 0x41DE62A7L;
        uint64_t l_2504 = 0x5C9F24154FC8DD5CLL;
        uint16_t **l_2509 = &l_53;
        int32_t ***l_2550 = &g_2349;
        struct S0 l_2572 = {11542,0L};
        int32_t l_2583 = (-7L);
        int32_t l_2588 = 0x3DA9A9F9L;
        uint16_t l_2590 = 0xACDFL;
        uint16_t l_2600 = 0xC88CL;
        int32_t l_2649 = (-2L);
        int i, j, k;
        l_2332 = l_2331;
        (*l_2334) &= (**g_150);
        l_2334 = l_2334;
        for (l_2297 = (-6); (l_2297 <= 22); l_2297 = safe_add_func_uint32_t_u_u(l_2297, 8))
        { /* block id: 1122 */
            float l_2347 = 0x9.6947A3p+6;
            int32_t ****l_2351 = &g_2348;
            int32_t ****l_2353 = &l_2352;
            int32_t l_2363[6][10] = {{(-1L),0x12106DF3L,(-1L),0xC58AC374L,0x5E2623EEL,(-1L),0x144DB285L,0x5C875FF9L,0xC58AC374L,0xC58AC374L},{0x5C875FF9L,0x12106DF3L,0x69E1D621L,0x5E2623EEL,0x5E2623EEL,0x69E1D621L,0x12106DF3L,0x5C875FF9L,0x3E3674DCL,0x5E2623EEL},{0x5C875FF9L,0x144DB285L,(-1L),0x5E2623EEL,0xC58AC374L,(-1L),0x12106DF3L,(-1L),0xC58AC374L,0x5E2623EEL},{(-1L),0x12106DF3L,(-1L),0xC58AC374L,0x5E2623EEL,(-1L),0x144DB285L,0x5C875FF9L,0xC58AC374L,0xC58AC374L},{0x5C875FF9L,0x12106DF3L,0x69E1D621L,0x5E2623EEL,0x5E2623EEL,0x69E1D621L,0x12106DF3L,0x5C875FF9L,0x3E3674DCL,0x5E2623EEL},{0x5C875FF9L,0x144DB285L,(-1L),0x5E2623EEL,0xC58AC374L,(-1L),0x12106DF3L,(-1L),0xC58AC374L,0x5E2623EEL}};
            int32_t l_2364 = 0x6461D90BL;
            int32_t *l_2365 = &l_2301;
            int32_t *l_2366 = &l_2290;
            struct S0 *l_2371 = &g_37;
            uint8_t * const *l_2412 = &g_83;
            uint8_t * const **l_2411 = &l_2412;
            uint8_t * const ***l_2410 = &l_2411;
            uint8_t * const ****l_2409[10][3] = {{&l_2410,&l_2410,&l_2410},{&l_2410,&l_2410,&l_2410},{&l_2410,&l_2410,(void*)0},{(void*)0,&l_2410,&l_2410},{(void*)0,&l_2410,&l_2410},{(void*)0,&l_2410,(void*)0},{&l_2410,(void*)0,&l_2410},{&l_2410,&l_2410,&l_2410},{&l_2410,(void*)0,(void*)0},{&l_2410,&l_2410,&l_2410}};
            uint32_t l_2419[8] = {7UL,0xDEC5ABB5L,7UL,7UL,0xDEC5ABB5L,7UL,7UL,0xDEC5ABB5L};
            const int64_t l_2463[1] = {0x5BC65A43AFDC62DBLL};
            const uint64_t *l_2536[5][3];
            const uint64_t **l_2535 = &l_2536[1][0];
            const uint64_t ***l_2534 = &l_2535;
            int32_t l_2578[1];
            int64_t l_2621[3];
            struct S0 l_2626 = {9796,-9L};
            uint32_t l_2630[4][3][10] = {{{18446744073709551609UL,18446744073709551610UL,0x1F5CB25AL,0x884DB12DL,1UL,18446744073709551607UL,0xB0D7DC54L,18446744073709551610UL,0xB0D7DC54L,18446744073709551607UL},{0xE99BC534L,18446744073709551610UL,0xEA479D4BL,18446744073709551610UL,0xE99BC534L,18446744073709551615UL,0UL,0x032C7D45L,0xEA479D4BL,18446744073709551615UL},{0xE99BC534L,0x00FF412FL,0xB0D7DC54L,18446744073709551615UL,0x1F5CB25AL,18446744073709551607UL,0xE99BC534L,18446744073709551607UL,0x1F5CB25AL,18446744073709551615UL}},{{18446744073709551609UL,18446744073709551615UL,18446744073709551609UL,0x884DB12DL,0xB0D7DC54L,18446744073709551615UL,0xEA479D4BL,0x00FF412FL,0x3A8DCC95L,18446744073709551608UL},{18446744073709551609UL,18446744073709551607UL,0UL,0x00FF412FL,0x1F5CB25AL,0x032C7D45L,0x1F5CB25AL,0x00FF412FL,0UL,18446744073709551607UL},{0x1F5CB25AL,18446744073709551615UL,0x75A03B73L,0x884DB12DL,0x3A8DCC95L,0xFDE35EEEL,0x1F5CB25AL,18446744073709551608UL,0xEA479D4BL,0x884DB12DL}},{{0x75A03B73L,18446744073709551607UL,0xEA479D4BL,0xFDE35EEEL,18446744073709551609UL,0xFDE35EEEL,0xEA479D4BL,18446744073709551607UL,0x75A03B73L,18446744073709551608UL},{0x1F5CB25AL,0x00FF412FL,0UL,18446744073709551607UL,18446744073709551609UL,0x032C7D45L,0xB0D7DC54L,0x884DB12DL,0UL,0x884DB12DL},{18446744073709551609UL,18446744073709551615UL,0x3A8DCC95L,18446744073709551607UL,0x3A8DCC95L,18446744073709551615UL,18446744073709551609UL,18446744073709551608UL,0x75A03B73L,18446744073709551607UL}},{{0x75A03B73L,0x884DB12DL,0x3A8DCC95L,0xFDE35EEEL,0x1F5CB25AL,18446744073709551608UL,0xEA479D4BL,0x884DB12DL,0xEA479D4BL,18446744073709551608UL},{0xB0D7DC54L,0x884DB12DL,0UL,0x884DB12DL,0xB0D7DC54L,0x032C7D45L,18446744073709551609UL,18446744073709551607UL,0UL,0x00FF412FL},{0xB0D7DC54L,18446744073709551615UL,0xEA479D4BL,0x00FF412FL,0x3A8DCC95L,18446744073709551608UL,0xB0D7DC54L,18446744073709551608UL,0x3A8DCC95L,0x00FF412FL}}};
            int32_t *l_2642 = &l_2290;
            int32_t *l_2643 = &l_2363[2][8];
            int32_t *l_2644 = &l_2290;
            int32_t *l_2645 = &l_2301;
            int32_t *l_2646 = &l_2290;
            int32_t *l_2647 = (void*)0;
            int32_t *l_2648 = &g_77;
            int32_t *l_2650 = &l_2649;
            int32_t *l_2651[10][1][9] = {{{(void*)0,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77,&g_77,(void*)0}},{{&l_2363[2][0],&g_77,&l_2363[2][7],&g_77,&l_2363[2][0],&l_2363[2][0],&g_77,&l_2363[2][7],&g_77}},{{&g_77,&g_77,&l_2363[2][7],&l_2363[2][7],&g_77,&g_77,&g_77,&l_2363[2][7],&l_2363[2][7]}},{{&l_2363[2][0],&l_2363[2][0],&g_77,&l_2363[2][7],&g_77,&l_2363[2][0],&l_2363[2][0],&g_77,&l_2363[2][7]}},{{(void*)0,&g_77,(void*)0,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77}},{{(void*)0,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77,&g_77,(void*)0}},{{&l_2363[2][0],&g_77,&l_2363[2][7],&g_77,&l_2363[2][0],&l_2363[2][0],&g_77,&l_2363[2][7],&g_77}},{{&g_77,&g_77,&l_2363[2][7],&l_2363[2][7],&g_77,&g_77,&g_77,&l_2363[2][7],&l_2363[2][7]}},{{&l_2363[2][0],&l_2363[2][0],&g_77,&l_2363[2][7],&g_77,&l_2363[2][0],&l_2363[2][0],&g_77,&l_2363[2][7]}},{{(void*)0,&g_77,(void*)0,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77}}};
            int i, j, k;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 3; j++)
                    l_2536[i][j] = (void*)0;
            }
            for (i = 0; i < 1; i++)
                l_2578[i] = 1L;
            for (i = 0; i < 3; i++)
                l_2621[i] = 0L;
            (*l_2366) |= ((safe_add_func_uint16_t_u_u((*l_105), (((safe_sub_func_int32_t_s_s(((*l_2365) = ((safe_add_func_int32_t_s_s((*l_105), ((**g_1589) = (((safe_mod_func_uint32_t_u_u((**g_1589), (safe_lshift_func_uint8_t_u_s(((**g_375) == 0x6883CE71L), (((*l_2351) = g_2348) != ((*l_2353) = l_2352)))))) || (safe_sub_func_uint32_t_u_u((safe_sub_func_int32_t_s_s((*l_2334), ((safe_unary_minus_func_uint8_t_u((safe_sub_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(((*g_318) |= ((*l_2334) , l_2363[2][0])), 5)), (*l_105))))) > l_2364))), (**g_1589)))) > 0x6E0DAEE2L)))) , l_2364)), 0x59701027L)) || 0x94L) , 1L))) >= 0x9C14114C53FB5D62LL);
        }
    }
    else
    { /* block id: 1239 */
        int32_t *l_2657 = &l_2297;
        (*g_375) = l_2657;
    }
    return (*l_105);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_2(int32_t  p_3, int32_t  p_4, uint32_t  p_5, uint16_t  p_6)
{ /* block id: 1066 */
    return p_4;
}


/* ------------------------------------------ */
/* 
 * reads : g_2223 g_2074 g_2075 g_1375 g_103 g_37.f1 g_1269 g_129 g_1271 g_293 g_1981 g_1777 g_2252
 * writes: g_103 g_37.f1 g_129 g_293 g_1981 g_1777 g_2250 g_377
 */
static uint64_t  func_9(uint8_t  p_10, float  p_11, uint8_t  p_12)
{ /* block id: 1031 */
    struct S0 l_2224 = {4362,0xEFC7C226F778ECBELL};
    int8_t *****l_2231 = (void*)0;
    float *l_2234[4] = {&g_1945,&g_1945,&g_1945,&g_1945};
    int32_t l_2235 = 4L;
    int32_t l_2236 = (-1L);
    int32_t l_2237[4];
    int32_t l_2238 = 0L;
    int32_t *l_2253 = &l_2238;
    uint32_t l_2254 = 0UL;
    int i;
    for (i = 0; i < 4; i++)
        l_2237[i] = 4L;
    l_2238 = (l_2237[1] = (safe_add_func_float_f_f((safe_sub_func_float_f_f((((p_11 = ((((*g_1375) = ((g_2223 , p_11) == (***g_2074))) != ((l_2224 , (safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f((-0x1.Cp-1), (l_2224.f0 <= (l_2236 = (l_2235 = ((l_2231 == l_2231) , (((safe_add_func_float_f_f((0x0.1p+1 == l_2224.f0), l_2224.f0)) != l_2224.f0) <= 0xA.6D9D65p+17))))))), p_11)), 0x3.35240Ep+3))) < l_2224.f1)) == l_2224.f0)) <= p_12) == 0x3.9CF528p-62), l_2224.f0)), p_10)));
    for (g_37.f1 = 0; (g_37.f1 <= 3); g_37.f1 += 1)
    { /* block id: 1040 */
        int64_t l_2239 = (-1L);
        int16_t *l_2240 = &g_1654;
        int i;
        if (l_2237[3])
            break;
        (*g_1269) |= (l_2237[g_37.f1] = ((l_2224.f0 <= ((l_2239 ^ (l_2240 != (void*)0)) && (((safe_rshift_func_int16_t_s_u(l_2239, 12)) , l_2237[1]) , 0x8BDD1AE2D06EDDB1LL))) && l_2239));
        (*g_1271) ^= 0x6C302318L;
        for (g_293 = 0; (g_293 <= 3); g_293 += 1)
        { /* block id: 1047 */
            int32_t ***l_2246 = (void*)0;
            int32_t *l_2249 = &g_171[3][0];
            int32_t **l_2248[7] = {&l_2249,&l_2249,&l_2249,&l_2249,&l_2249,&l_2249,&l_2249};
            int32_t ***l_2247[4][5][8] = {{{(void*)0,&l_2248[5],(void*)0,&l_2248[5],&l_2248[0],&l_2248[5],&l_2248[5],&l_2248[5]},{&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],(void*)0,&l_2248[5],&l_2248[0],&l_2248[5]},{&l_2248[5],(void*)0,(void*)0,&l_2248[4],(void*)0,(void*)0,&l_2248[5],(void*)0},{&l_2248[5],(void*)0,&l_2248[0],&l_2248[4],&l_2248[5],(void*)0,&l_2248[5],(void*)0},{&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[4]}},{{&l_2248[5],&l_2248[5],&l_2248[4],(void*)0,(void*)0,&l_2248[4],&l_2248[5],&l_2248[5]},{&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[0],(void*)0,&l_2248[6],(void*)0,&l_2248[5]},{&l_2248[5],(void*)0,(void*)0,&l_2248[5],&l_2248[0],&l_2248[6],&l_2248[5],&l_2248[5]},{&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[4],&l_2248[5],&l_2248[4],&l_2248[5],&l_2248[5]},{&l_2248[5],&l_2248[5],&l_2248[6],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5]}},{{&l_2248[5],&l_2248[5],&l_2248[4],(void*)0,&l_2248[5],(void*)0,&l_2248[5],&l_2248[5]},{&l_2248[5],(void*)0,&l_2248[6],&l_2248[5],(void*)0,(void*)0,&l_2248[5],&l_2248[6]},{(void*)0,(void*)0,&l_2248[5],&l_2248[6],(void*)0,&l_2248[5],&l_2248[5],&l_2248[0]},{(void*)0,&l_2248[5],(void*)0,&l_2248[4],&l_2248[5],&l_2248[5],(void*)0,&l_2248[0]},{&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[6],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[6]}},{{&l_2248[4],&l_2248[5],&l_2248[4],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5]},{&l_2248[6],&l_2248[0],&l_2248[5],(void*)0,(void*)0,&l_2248[5],&l_2248[5],&l_2248[5]},{&l_2248[6],(void*)0,&l_2248[0],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5]},{&l_2248[4],(void*)0,(void*)0,&l_2248[4],&l_2248[5],&l_2248[5],&l_2248[0],&l_2248[5]},{&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5],&l_2248[5]}}};
            struct S0 l_2251 = {2358,0xCEC97358CD22851BLL};
            int i, j, k;
            for (g_1981 = 0; (g_1981 <= 3); g_1981 += 1)
            { /* block id: 1050 */
                uint32_t l_2243 = 0xA699FF61L;
                ++l_2243;
            }
            for (g_1777 = 0; (g_1777 <= 3); g_1777 += 1)
            { /* block id: 1055 */
                (*g_1271) &= 0xDDAF07D2L;
            }
            g_2250 = (void*)0;
            (*g_2252) = l_2251;
        }
    }
    (*g_1271) = l_2224.f1;
    l_2253 = &l_2237[1];
    return l_2254;
}


/* ------------------------------------------ */
/* 
 * reads : g_54 g_1269 g_129 g_1039 g_103
 * writes: g_54 g_129
 */
static float  func_14(int8_t  p_15, int64_t  p_16, uint32_t  p_17)
{ /* block id: 741 */
    struct S1 **l_1570 = &g_1197;
    struct S1 ***l_1569 = &l_1570;
    int32_t l_1571[1];
    const uint8_t *l_1600 = &g_353[0][3];
    const uint8_t **l_1599 = &l_1600;
    const uint8_t ***l_1598 = &l_1599;
    const uint8_t ****l_1597 = &l_1598;
    int16_t l_1659 = 0xF74FL;
    uint8_t *l_1743 = (void*)0;
    int32_t l_1807 = 0x39396DB4L;
    const int8_t *l_1876 = (void*)0;
    const int8_t **l_1875[5];
    uint32_t ***l_1901[2][1];
    const struct S0 l_1921 = {7873,0x283EDB766E662E3ALL};
    struct S0 l_1926[10][10][2] = {{{{8915,-1L},{6857,0x86D9D9A34630A4F4LL}},{{6835,0L},{6284,-7L}},{{6835,0L},{6857,0x86D9D9A34630A4F4LL}},{{8915,-1L},{6835,0L}},{{6857,0x86D9D9A34630A4F4LL},{6284,-7L}},{{6107,0L},{6107,0L}},{{8915,-1L},{6107,0L}},{{6107,0L},{6284,-7L}},{{6857,0x86D9D9A34630A4F4LL},{6835,0L}},{{8915,-1L},{6857,0x86D9D9A34630A4F4LL}}},{{{6835,0L},{6284,-7L}},{{6835,0L},{6857,0x86D9D9A34630A4F4LL}},{{8915,-1L},{6835,0L}},{{6857,0x86D9D9A34630A4F4LL},{6284,-7L}},{{6107,0L},{6107,0L}},{{8915,-1L},{6107,0L}},{{6107,0L},{6284,-7L}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}}},{{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}}},{{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}}},{{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}}},{{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}}},{{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}}},{{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}}},{{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}}},{{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}},{{2035,0x87A4A9EDDF6460C0LL},{3761,0x51BE4D01D13ED74CLL}},{{2035,0x87A4A9EDDF6460C0LL},{6284,-7L}},{{2194,0xED3E66C9FF823C5CLL},{2035,0x87A4A9EDDF6460C0LL}},{{6284,-7L},{3761,0x51BE4D01D13ED74CLL}},{{8915,-1L},{8915,-1L}},{{2194,0xED3E66C9FF823C5CLL},{8915,-1L}},{{8915,-1L},{3761,0x51BE4D01D13ED74CLL}},{{6284,-7L},{2035,0x87A4A9EDDF6460C0LL}},{{2194,0xED3E66C9FF823C5CLL},{6284,-7L}}}};
    struct S0 **l_1985 = &g_1611[3];
    int32_t *l_2218 = (void*)0;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1571[i] = (-1L);
    for (i = 0; i < 5; i++)
        l_1875[i] = &l_1876;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_1901[i][j] = &g_1589;
    }
    for (g_54 = 0; (g_54 == 51); g_54 = safe_add_func_int32_t_s_s(g_54, 1))
    { /* block id: 744 */
        const int32_t l_1558 = 7L;
        int8_t * const * const l_1592[1][1] = {{&g_318}};
        uint8_t ***l_1608 = &g_82;
        uint8_t ****l_1607 = &l_1608;
        const uint16_t *l_1621 = (void*)0;
        const int32_t *l_1660 = &l_1571[0];
        int32_t l_1669[1][2];
        int16_t l_1731[6];
        struct S0 l_1760[9][9][3] = {{{{11464,0xA488C78F1302D85ELL},{804,0x43710E3AE62EC0CCLL},{2680,0xE588E389B789465ALL}},{{3112,-9L},{8261,-8L},{2251,4L}},{{3292,0xF7AF54FAD9744100LL},{10079,0xF4A3F252F1FB4F28LL},{10946,0x65EE53CB09439562LL}},{{6558,0xCA58996AD999BE2BLL},{3112,-9L},{2251,4L}},{{2680,0xE588E389B789465ALL},{7262,0xFA1ED93FC922A25BLL},{2680,0xE588E389B789465ALL}},{{1672,-3L},{1248,0x4C171246DA55BFD5LL},{10025,1L}},{{10653,2L},{8725,0x33D920985BF0AFD4LL},{11317,2L}},{{4672,0x7CC716EC584A73A0LL},{3112,-9L},{7760,1L}},{{11464,0xA488C78F1302D85ELL},{8006,1L},{3173,-1L}}},{{{4672,0x7CC716EC584A73A0LL},{8261,-8L},{8261,-8L}},{{10653,2L},{9212,0x7E7A7666E057CD5FLL},{10946,0x65EE53CB09439562LL}},{{1672,-3L},{8012,0x6485ADE77FDC9D0BLL},{1479,0x912FCD1193C62AD3LL}},{{2680,0xE588E389B789465ALL},{1493,1L},{3173,-1L}},{{6558,0xCA58996AD999BE2BLL},{1248,0x4C171246DA55BFD5LL},{5314,0L}},{{3292,0xF7AF54FAD9744100LL},{1493,1L},{11317,2L}},{{3112,-9L},{8012,0x6485ADE77FDC9D0BLL},{1360,-1L}},{{11464,0xA488C78F1302D85ELL},{9212,0x7E7A7666E057CD5FLL},{2680,0xE588E389B789465ALL}},{{8012,0x6485ADE77FDC9D0BLL},{8261,-8L},{1248,0x4C171246DA55BFD5LL}}},{{{3292,0xF7AF54FAD9744100LL},{8006,1L},{10946,0x65EE53CB09439562LL}},{{708,0x8CD2864BB75825B9LL},{3112,-9L},{1248,0x4C171246DA55BFD5LL}},{{2680,0xE588E389B789465ALL},{8725,0x33D920985BF0AFD4LL},{2680,0xE588E389B789465ALL}},{{4860,0x59808F0555E3B5F2LL},{1248,0x4C171246DA55BFD5LL},{1360,-1L}},{{10653,2L},{7262,0xFA1ED93FC922A25BLL},{11317,2L}},{{4979,0L},{3112,-9L},{5314,0L}},{{11464,0xA488C78F1302D85ELL},{10079,0xF4A3F252F1FB4F28LL},{3173,-1L}},{{4979,0L},{8261,-8L},{1479,0x912FCD1193C62AD3LL}},{{10653,2L},{804,0x43710E3AE62EC0CCLL},{10946,0x65EE53CB09439562LL}}},{{{4860,0x59808F0555E3B5F2LL},{8012,0x6485ADE77FDC9D0BLL},{8261,-8L}},{{2680,0xE588E389B789465ALL},{2476,0L},{3173,-1L}},{{708,0x8CD2864BB75825B9LL},{1248,0x4C171246DA55BFD5LL},{7760,1L}},{{3292,0xF7AF54FAD9744100LL},{2476,0L},{11317,2L}},{{8012,0x6485ADE77FDC9D0BLL},{8012,0x6485ADE77FDC9D0BLL},{10025,1L}},{{11464,0xA488C78F1302D85ELL},{804,0x43710E3AE62EC0CCLL},{2680,0xE588E389B789465ALL}},{{3112,-9L},{8261,-8L},{2251,4L}},{{3292,0xF7AF54FAD9744100LL},{10079,0xF4A3F252F1FB4F28LL},{10946,0x65EE53CB09439562LL}},{{6558,0xCA58996AD999BE2BLL},{3112,-9L},{2251,4L}}},{{{2680,0xE588E389B789465ALL},{7262,0xFA1ED93FC922A25BLL},{2680,0xE588E389B789465ALL}},{{1672,-3L},{1248,0x4C171246DA55BFD5LL},{10025,1L}},{{10653,2L},{8725,0x33D920985BF0AFD4LL},{11317,2L}},{{4672,0x7CC716EC584A73A0LL},{3112,-9L},{7760,1L}},{{11464,0xA488C78F1302D85ELL},{8006,1L},{3173,-1L}},{{4672,0x7CC716EC584A73A0LL},{8261,-8L},{8261,-8L}},{{10653,2L},{9212,0x7E7A7666E057CD5FLL},{10946,0x65EE53CB09439562LL}},{{1672,-3L},{8012,0x6485ADE77FDC9D0BLL},{1479,0x912FCD1193C62AD3LL}},{{2680,0xE588E389B789465ALL},{1493,1L},{3173,-1L}}},{{{6558,0xCA58996AD999BE2BLL},{1248,0x4C171246DA55BFD5LL},{5314,0L}},{{3292,0xF7AF54FAD9744100LL},{1493,1L},{11317,2L}},{{3112,-9L},{8012,0x6485ADE77FDC9D0BLL},{1360,-1L}},{{11464,0xA488C78F1302D85ELL},{9212,0x7E7A7666E057CD5FLL},{2680,0xE588E389B789465ALL}},{{8012,0x6485ADE77FDC9D0BLL},{8261,-8L},{1248,0x4C171246DA55BFD5LL}},{{3292,0xF7AF54FAD9744100LL},{8006,1L},{10946,0x65EE53CB09439562LL}},{{708,0x8CD2864BB75825B9LL},{3112,-9L},{1248,0x4C171246DA55BFD5LL}},{{2680,0xE588E389B789465ALL},{8725,0x33D920985BF0AFD4LL},{2680,0xE588E389B789465ALL}},{{4860,0x59808F0555E3B5F2LL},{1248,0x4C171246DA55BFD5LL},{1360,-1L}}},{{{10653,2L},{7262,0xFA1ED93FC922A25BLL},{11317,2L}},{{4979,0L},{3112,-9L},{5314,0L}},{{11464,0xA488C78F1302D85ELL},{10079,0xF4A3F252F1FB4F28LL},{3173,-1L}},{{4979,0L},{8261,-8L},{1479,0x912FCD1193C62AD3LL}},{{10653,2L},{804,0x43710E3AE62EC0CCLL},{10946,0x65EE53CB09439562LL}},{{4860,0x59808F0555E3B5F2LL},{8012,0x6485ADE77FDC9D0BLL},{8261,-8L}},{{2680,0xE588E389B789465ALL},{2476,0L},{3173,-1L}},{{708,0x8CD2864BB75825B9LL},{1248,0x4C171246DA55BFD5LL},{7760,1L}},{{3292,0xF7AF54FAD9744100LL},{2476,0L},{11317,2L}}},{{{8012,0x6485ADE77FDC9D0BLL},{8012,0x6485ADE77FDC9D0BLL},{10025,1L}},{{11464,0xA488C78F1302D85ELL},{804,0x43710E3AE62EC0CCLL},{2680,0xE588E389B789465ALL}},{{3112,-9L},{8261,-8L},{2251,4L}},{{3292,0xF7AF54FAD9744100LL},{10079,0xF4A3F252F1FB4F28LL},{10946,0x65EE53CB09439562LL}},{{6558,0xCA58996AD999BE2BLL},{3112,-9L},{2251,4L}},{{2680,0xE588E389B789465ALL},{7262,0xFA1ED93FC922A25BLL},{2680,0xE588E389B789465ALL}},{{1672,-3L},{1248,0x4C171246DA55BFD5LL},{10025,1L}},{{10653,2L},{8725,0x33D920985BF0AFD4LL},{11317,2L}},{{4672,0x7CC716EC584A73A0LL},{3112,-9L},{7760,1L}}},{{{11464,0xA488C78F1302D85ELL},{8006,1L},{3173,-1L}},{{4672,0x7CC716EC584A73A0LL},{8261,-8L},{8261,-8L}},{{10653,2L},{9212,0x7E7A7666E057CD5FLL},{10946,0x65EE53CB09439562LL}},{{1672,-3L},{8012,0x6485ADE77FDC9D0BLL},{1479,0x912FCD1193C62AD3LL}},{{2680,0xE588E389B789465ALL},{1493,1L},{3173,-1L}},{{6558,0xCA58996AD999BE2BLL},{1248,0x4C171246DA55BFD5LL},{5314,0L}},{{3292,0xF7AF54FAD9744100LL},{1493,1L},{11317,2L}},{{3112,-9L},{8012,0x6485ADE77FDC9D0BLL},{1360,-1L}},{{11464,0xA488C78F1302D85ELL},{9212,0x7E7A7666E057CD5FLL},{2680,0xE588E389B789465ALL}}}};
        struct S1 **l_1883 = &g_1197;
        uint32_t ***l_1902 = &g_1589;
        uint32_t l_1924[5];
        float l_1952 = 0xB.E6E107p-4;
        int64_t l_1955 = 4L;
        int32_t l_2009 = 0x09DF009EL;
        int32_t l_2017[6][8][4] = {{{0xE1C96AE1L,0xFADF048BL,0x74394C94L,0x95E5E914L},{0L,1L,(-8L),1L},{0xFADF048BL,1L,(-1L),(-8L)},{0x2353A5D3L,0L,0L,0xC9F63045L},{0x7818E5B8L,(-4L),0xE1C96AE1L,(-1L)},{0x7818E5B8L,0xFADF048BL,0L,0x49905343L},{0x2353A5D3L,(-1L),(-1L),0x536C41C9L},{0xFADF048BL,0xFFA3FA39L,(-8L),0x65C9465CL}},{{0L,0x2353A5D3L,0x74394C94L,0xC9F63045L},{0xE1C96AE1L,1L,0x49905343L,0x74394C94L},{0x20C82FE8L,0L,0L,0x20C82FE8L},{0L,1L,0x1C455697L,(-1L)},{0L,0xFFA3FA39L,2L,(-1L)},{0x2353A5D3L,0L,0x536C41C9L,(-1L)},{0xE1C96AE1L,0xFFA3FA39L,0x7048C8B7L,(-1L)},{0x95E5E914L,1L,0x74394C94L,0x20C82FE8L}},{{0x18D4D31EL,0L,(-1L),0x74394C94L},{1L,1L,2L,0xC9F63045L},{0L,0x2353A5D3L,0xFADF048BL,0x65C9465CL},{0x7818E5B8L,0xFFA3FA39L,0x7818E5B8L,0x536C41C9L},{0x20C82FE8L,(-1L),0x536C41C9L,0x49905343L},{0x18D4D31EL,0xFADF048BL,(-8L),(-1L)},{(-1L),(-4L),(-8L),0xC9F63045L},{0x18D4D31EL,0L,0x536C41C9L,(-8L)}},{{0x20C82FE8L,1L,0x7818E5B8L,1L},{0x7818E5B8L,1L,0xFADF048BL,0x95E5E914L},{0L,0xFADF048BL,2L,0x536C41C9L},{1L,0x8E0D0F0BL,(-1L),(-1L)},{0x18D4D31EL,0x18D4D31EL,0x74394C94L,0x65C9465CL},{0x95E5E914L,(-4L),0x7048C8B7L,1L},{0xE1C96AE1L,0L,0x536C41C9L,0x7048C8B7L},{0x2353A5D3L,0L,2L,1L}},{{0L,(-4L),0x1C455697L,0x65C9465CL},{0L,0x18D4D31EL,0L,(-1L)},{0x20C82FE8L,0x8E0D0F0BL,0x49905343L,0x536C41C9L},{0xE1C96AE1L,0xFADF048BL,0x74394C94L,0x95E5E914L},{0L,1L,(-8L),1L},{0xFADF048BL,1L,(-1L),(-8L)},{0x2353A5D3L,0L,0L,0xC9F63045L},{0x7818E5B8L,(-4L),0xE1C96AE1L,(-1L)}},{{0x7818E5B8L,0xFADF048BL,0x95E5E914L,(-8L)},{0L,0x49905343L,0x4493AAC9L,0x7048C8B7L},{4L,0x040B1B5BL,0xFFA3FA39L,0x71BC493AL},{0x536C41C9L,0L,0xE1C96AE1L,1L},{0L,0L,(-8L),0xE1C96AE1L},{2L,0x95E5E914L,0x95E5E914L,2L},{(-1L),0x7818E5B8L,3L,0x49905343L},{0x95E5E914L,0x040B1B5BL,0x8E0D0F0BL,0x4493AAC9L}}};
        int16_t l_2059 = 1L;
        float **l_2065 = (void*)0;
        float ***l_2064 = &l_2065;
        uint8_t l_2138 = 9UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_1669[i][j] = (-6L);
        }
        for (i = 0; i < 6; i++)
            l_1731[i] = 7L;
        for (i = 0; i < 5; i++)
            l_1924[i] = 1UL;
        (*g_1269) &= (&g_317 == &g_317);
    }
    l_2218 = &l_1571[0];
    return (*g_1039);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_18(uint32_t  p_19, int16_t  p_20, uint32_t  p_21)
{ /* block id: 738 */
    int64_t l_1554[7][7][2] = {{{3L,(-10L)},{(-1L),0x270715E867437A05LL},{2L,0x270715E867437A05LL},{(-1L),(-10L)},{3L,2L},{(-4L),(-6L)},{5L,0xCF53465D2F621EB8LL}},{{0x96F15CC83F75BA0BLL,(-1L)},{0xED79F6B44F8B1783LL,0xED79F6B44F8B1783LL},{0x7E2BD243CDF6D97DLL,3L},{0xCE9C686F8322FA14LL,0xF925C04CC8AB8D22LL},{0xFA4279CE912C4A4CLL,1L},{1L,0xFA4279CE912C4A4CLL},{(-6L),(-1L)}},{{(-6L),0xFA4279CE912C4A4CLL},{1L,1L},{0xFA4279CE912C4A4CLL,0xF925C04CC8AB8D22LL},{0xCE9C686F8322FA14LL,3L},{0x7E2BD243CDF6D97DLL,0xED79F6B44F8B1783LL},{0xED79F6B44F8B1783LL,(-1L)},{0x96F15CC83F75BA0BLL,0xCF53465D2F621EB8LL}},{{5L,(-6L)},{(-4L),2L},{3L,(-10L)},{(-1L),0x270715E867437A05LL},{2L,0x270715E867437A05LL},{(-1L),(-10L)},{3L,2L}},{{(-4L),(-6L)},{5L,0xCF53465D2F621EB8LL},{0x96F15CC83F75BA0BLL,(-1L)},{0xED79F6B44F8B1783LL,0xED79F6B44F8B1783LL},{0x7E2BD243CDF6D97DLL,3L},{0xCE9C686F8322FA14LL,0xF925C04CC8AB8D22LL},{0xFA4279CE912C4A4CLL,1L}},{{1L,0xFA4279CE912C4A4CLL},{(-6L),(-1L)},{(-6L),0xFA4279CE912C4A4CLL},{1L,1L},{0xFA4279CE912C4A4CLL,0xF925C04CC8AB8D22LL},{0xCE9C686F8322FA14LL,3L},{0x7E2BD243CDF6D97DLL,0xED79F6B44F8B1783LL}},{{0xED79F6B44F8B1783LL,(-1L)},{0x96F15CC83F75BA0BLL,0xCF53465D2F621EB8LL},{5L,(-6L)},{(-4L),2L},{3L,(-10L)},{(-1L),0x270715E867437A05LL},{2L,0x270715E867437A05LL}}};
    int i, j, k;
    l_1554[2][0][1] ^= (-2L);
    return p_21;
}


/* ------------------------------------------ */
/* 
 * reads : g_435.f3
 * writes:
 */
static uint32_t  func_22(uint16_t  p_23, int32_t  p_24, int32_t  p_25, uint16_t  p_26)
{ /* block id: 734 */
    int16_t l_1550 = 2L;
    int32_t l_1551 = 0x514974C3L;
    int32_t *l_1552[2];
    int i;
    for (i = 0; i < 2; i++)
        l_1552[i] = &g_77;
    l_1551 |= l_1550;
    l_1552[1] = &p_25;
    return g_435[2].f3;
}


/* ------------------------------------------ */
/* 
 * reads : g_77
 * writes:
 */
static uint8_t  func_30(float  p_31, int64_t  p_32, float  p_33)
{ /* block id: 730 */
    int32_t l_1536 = 1L;
    int32_t *l_1537 = &g_77;
    int32_t *l_1538 = &g_77;
    int32_t *l_1539 = &g_77;
    int32_t l_1540 = (-3L);
    int32_t *l_1541 = &l_1540;
    int32_t *l_1542 = &l_1540;
    int32_t *l_1543[4][3][3] = {{{(void*)0,(void*)0,(void*)0},{&g_77,&l_1540,(void*)0},{&g_77,&l_1540,&l_1540}},{{(void*)0,(void*)0,&l_1540},{&l_1540,&g_77,&l_1540},{(void*)0,&g_77,(void*)0}},{{(void*)0,&g_77,(void*)0},{(void*)0,&g_77,(void*)0},{&g_77,(void*)0,(void*)0}},{{(void*)0,&l_1540,(void*)0},{(void*)0,&l_1540,(void*)0},{(void*)0,(void*)0,(void*)0}}};
    int32_t l_1544 = 5L;
    uint32_t l_1545 = 0x5AFDA2A9L;
    int i, j, k;
    ++l_1545;
    return (*l_1538);
}


/* ------------------------------------------ */
/* 
 * reads : g_77 g_311 g_293 g_824 g_825 g_532 g_533 g_286 g_377.f1 g_111 g_292 g_317 g_318 g_54 g_143.f0 g_376 g_377 g_73 g_59 g_480 g_481 g_482 g_353 g_172 g_83 g_13 g_143.f2 g_103 g_587.f2 g_375 g_97 g_232 g_435.f2 g_623 g_1039 g_1040 g_1042 g_146 g_673 g_621 g_1043 g_131
 * writes: g_59 g_377.f1 g_77 g_292 g_73 g_54 g_75 g_672 g_286 g_295 g_941 g_293 g_353 g_623 g_673 g_103 g_1043
 */
static int64_t  func_34(struct S0  p_35, int32_t  p_36)
{ /* block id: 398 */
    int64_t l_835 = (-4L);
    int32_t *l_836 = &g_77;
    int32_t *l_837 = (void*)0;
    int32_t *l_838 = &g_77;
    int32_t *l_839[6][1][8] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_77,(void*)0}},{{&g_77,(void*)0,&g_77,(void*)0,&g_77,&g_77,(void*)0,&g_77}},{{&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77,&g_77,(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77}},{{(void*)0,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77,(void*)0}},{{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77}}};
    uint64_t l_840[8] = {0xA71D9EB03D1D0D4CLL,0x608E4F9B41EA6944LL,0x608E4F9B41EA6944LL,0xA71D9EB03D1D0D4CLL,0x608E4F9B41EA6944LL,0x608E4F9B41EA6944LL,0xA71D9EB03D1D0D4CLL,0x608E4F9B41EA6944LL};
    int64_t *l_861 = &g_59[2][1];
    int64_t *l_862 = &l_835;
    const float *l_864 = &g_103[0][1][0];
    const float **l_863 = &l_864;
    const int8_t l_865 = 0x48L;
    int32_t l_866 = 0x7761FA47L;
    float l_867 = 0x2.7C3CD3p-38;
    uint64_t l_868 = 18446744073709551615UL;
    int64_t *l_869 = &g_377.f1;
    struct S1 *l_880[5][10][5] = {{{&g_381,&g_67,&g_143,&g_435[2],&g_435[2]},{&g_143,&g_435[2],&g_143,&g_435[2],(void*)0},{&g_381,&g_435[2],&g_435[1],&g_381,(void*)0},{&g_67,&g_435[0],&g_435[2],&g_435[0],&g_67},{(void*)0,&g_143,&g_143,&g_435[1],&g_143},{&g_435[2],&g_143,&g_435[2],&g_381,(void*)0},{&g_435[2],&g_435[2],&g_381,&g_143,&g_143},{(void*)0,&g_381,&g_435[2],(void*)0,&g_67},{&g_143,&g_67,&g_143,&g_143,(void*)0},{&g_381,&g_67,&g_67,&g_381,(void*)0}},{{(void*)0,&g_435[1],&g_435[2],&g_435[2],&g_435[2]},{&g_143,&g_143,&g_143,(void*)0,(void*)0},{&g_435[2],&g_143,&g_435[2],&g_67,&g_67},{&g_67,&g_381,&g_143,&g_67,&g_143},{&g_67,(void*)0,&g_435[2],&g_67,&g_435[2]},{&g_67,&g_435[2],&g_143,&g_143,&g_435[2]},{&g_67,&g_143,&g_435[2],&g_435[2],&g_435[2]},{(void*)0,&g_435[0],&g_143,&g_67,&g_381},{&g_143,(void*)0,&g_435[2],(void*)0,&g_67},{&g_435[2],&g_435[2],&g_67,&g_381,&g_435[2]}},{{&g_381,&g_67,&g_143,&g_143,&g_67},{&g_381,&g_435[2],&g_435[2],(void*)0,(void*)0},{&g_435[2],(void*)0,&g_381,&g_381,(void*)0},{&g_381,&g_435[2],&g_435[2],&g_435[0],&g_143},{&g_435[2],&g_435[2],&g_143,&g_67,&g_67},{&g_381,&g_435[2],&g_435[2],(void*)0,&g_435[2]},{&g_381,&g_435[1],&g_435[1],&g_435[2],&g_67},{&g_435[2],(void*)0,&g_143,&g_143,&g_381},{&g_143,&g_435[2],&g_143,&g_67,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_435[2]}},{{&g_67,&g_67,(void*)0,&g_381,&g_381},{&g_67,&g_435[2],&g_435[0],&g_143,&g_143},{&g_67,&g_381,&g_435[2],&g_143,&g_381},{(void*)0,&g_143,(void*)0,&g_67,&g_381},{&g_143,&g_67,&g_435[2],(void*)0,&g_143},{&g_67,&g_67,&g_435[0],(void*)0,&g_435[2]},{(void*)0,&g_435[2],&g_67,&g_435[2],&g_435[2]},{&g_435[2],&g_67,&g_67,(void*)0,&g_67},{&g_435[1],&g_435[1],&g_143,&g_67,&g_435[2]},{&g_435[1],&g_143,&g_381,&g_143,&g_381}},{{&g_435[2],&g_67,&g_143,&g_435[2],(void*)0},{&g_381,&g_143,(void*)0,&g_435[2],&g_67},{(void*)0,&g_435[1],(void*)0,&g_435[2],(void*)0},{&g_435[2],&g_67,&g_143,&g_381,&g_143},{&g_435[2],&g_435[2],&g_67,&g_67,&g_143},{&g_381,&g_67,&g_435[2],&g_435[2],&g_67},{&g_381,&g_67,&g_435[2],&g_67,&g_381},{&g_435[2],&g_143,&g_435[2],(void*)0,&g_143},{&g_435[2],&g_435[2],&g_435[1],(void*)0,(void*)0},{&g_67,&g_67,&g_435[2],&g_143,&g_143}}};
    uint64_t ***l_896 = &g_532;
    int32_t ** const *l_909[1];
    uint16_t *l_1011 = &g_54;
    uint64_t l_1036 = 18446744073709551612UL;
    struct S0 *l_1085 = &g_37;
    int32_t ** const *l_1089[6] = {&g_427,&g_427,&g_427,&g_427,&g_427,&g_427};
    struct S1 **l_1095 = &g_941;
    int32_t **l_1208 = &l_837;
    int8_t l_1276 = 3L;
    uint32_t l_1307 = 0x2C951F16L;
    int32_t l_1368 = (-3L);
    uint16_t l_1369 = 65535UL;
    int16_t l_1396 = 1L;
    uint32_t l_1444[3];
    float **l_1490 = &g_1375;
    float ***l_1489[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    float ****l_1488 = &l_1489[3];
    int32_t l_1506 = 0x65B50716L;
    int16_t l_1513[2];
    uint8_t l_1514 = 0x13L;
    uint32_t l_1523[4][5][5] = {{{0xFDFE9198L,0xC94D70F1L,0xCDE5606AL,0x957FB7A1L,18446744073709551615UL},{0xA6961DFCL,0x755AB9BBL,0xBD49C60DL,1UL,0x1FB0EDE4L},{0xC94D70F1L,0x4F0DE031L,0x4F0DE031L,0xC94D70F1L,0UL},{0x5B6E9710L,0x96641578L,0x7668518FL,2UL,0x96641578L},{0UL,0xFDFE9198L,0UL,1UL,0xB5A03225L}},{{18446744073709551613UL,0xA6961DFCL,1UL,2UL,0xBD49C60DL},{0x957FB7A1L,0xC94D70F1L,0xEC0CB129L,0xC94D70F1L,0x957FB7A1L},{1UL,0x5B6E9710L,0xA6961DFCL,1UL,0x755AB9BBL},{18446744073709551615UL,0UL,1UL,0x957FB7A1L,0xF2A3BC2AL},{0x5B6E9710L,18446744073709551613UL,0xB51BF95FL,0x5B6E9710L,0x755AB9BBL}},{{1UL,0x957FB7A1L,0UL,0UL,0x957FB7A1L},{0x755AB9BBL,1UL,0xC93399DCL,0x72530172L,0xBD49C60DL},{0xFDFE9198L,18446744073709551615UL,18446744073709551615UL,7UL,0xB5A03225L},{1UL,0x5B6E9710L,0xBD49C60DL,6UL,0x96641578L},{0xFDFE9198L,1UL,0UL,0x957FB7A1L,0UL}},{{0x755AB9BBL,0x755AB9BBL,0xADF06CA3L,0x96641578L,0x1FB0EDE4L},{1UL,0xFDFE9198L,0x4F0DE031L,1UL,18446744073709551615UL},{0x5B6E9710L,1UL,0xC93399DCL,2UL,0x7668518FL},{0xF2A3BC2AL,0xBEE2E60FL,0xB5A03225L,0UL,0UL},{0xCC029E57L,18446744073709551608UL,0xCC029E57L,0xADF06CA3L,0xA6961DFCL}}};
    const uint8_t l_1533[5][9][4] = {{{1UL,0xC8L,252UL,1UL},{252UL,1UL,0UL,0UL},{0UL,0UL,1UL,247UL},{0UL,0xC8L,0UL,0UL},{252UL,247UL,252UL,0UL},{1UL,247UL,1UL,0UL},{247UL,0xC8L,0xC8L,247UL},{252UL,0UL,0xC8L,0UL},{247UL,1UL,1UL,1UL}},{{1UL,0xC8L,252UL,1UL},{252UL,1UL,0UL,0UL},{0UL,0UL,1UL,247UL},{0UL,0xC8L,0UL,0UL},{252UL,247UL,252UL,0UL},{1UL,247UL,1UL,0UL},{247UL,0xC8L,0xC8L,247UL},{252UL,0UL,0xC8L,0UL},{247UL,1UL,1UL,1UL}},{{1UL,0xC8L,252UL,1UL},{252UL,1UL,0UL,0UL},{0UL,0UL,1UL,247UL},{0UL,0xC8L,0UL,0UL},{252UL,247UL,252UL,0UL},{1UL,247UL,1UL,0UL},{247UL,0xC8L,0xC8L,247UL},{252UL,0UL,0xC8L,0UL},{247UL,1UL,1UL,1UL}},{{1UL,0xC8L,252UL,1UL},{252UL,1UL,0UL,0UL},{0UL,0UL,1UL,247UL},{0UL,0xC8L,0UL,0UL},{252UL,247UL,252UL,0UL},{1UL,247UL,1UL,0UL},{247UL,0xC8L,0xC8L,247UL},{252UL,0UL,0xC8L,0UL},{247UL,1UL,1UL,1UL}},{{1UL,0xC8L,252UL,1UL},{252UL,1UL,0UL,0UL},{0UL,0UL,1UL,247UL},{0UL,0xC8L,0UL,0UL},{252UL,247UL,252UL,0UL},{1UL,247UL,1UL,0UL},{247UL,0xC8L,0xC8L,247UL},{252UL,0UL,0xC8L,0UL},{247UL,1UL,1UL,0UL}}};
    int8_t l_1534[4][2] = {{0xF3L,0xF3L},{0xF3L,0xF3L},{0xF3L,0xF3L},{0xF3L,0xF3L}};
    uint64_t l_1535[2];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_909[i] = &g_427;
    for (i = 0; i < 3; i++)
        l_1444[i] = 18446744073709551615UL;
    for (i = 0; i < 2; i++)
        l_1513[i] = (-8L);
    for (i = 0; i < 2; i++)
        l_1535[i] = 0UL;
    --l_840[1];
    if ((((*g_111) = (safe_mod_func_int64_t_s_s((p_35.f1 = ((*l_869) |= ((((safe_div_func_uint64_t_u_u(((((safe_mul_func_uint8_t_u_u(0x0AL, ((*l_836) , 0x25L))) <= 0x06L) && (safe_rshift_func_uint16_t_u_u(((~(safe_div_func_uint32_t_u_u((((((*g_311) <= (safe_mod_func_int32_t_s_s((safe_unary_minus_func_uint64_t_u(((((((safe_sub_func_uint64_t_u_u(p_35.f0, (safe_div_func_int64_t_s_s(((*l_861) = (-1L)), ((*l_862) |= (-8L)))))) > (l_863 != (*g_824))) ^ (*l_838)) < p_35.f1) || 0xA7842E43L) , 1UL))), l_865))) || p_35.f1) >= (**g_532)) | l_866), p_35.f1))) , p_35.f0), (*g_311)))) , l_868), (*g_533))) | 0L) , 0L) < (*l_838)))), (*l_838)))) , 1L))
    { /* block id: 405 */
        int32_t l_870 = 0xB58C2AD2L;
        int32_t l_871 = 0xB464C806L;
        int32_t l_872 = (-7L);
        uint32_t l_873 = 2UL;
        int32_t l_987 = (-1L);
        int32_t l_992 = 0xC94CF332L;
        int32_t l_993[10][2];
        int64_t l_997 = 1L;
        uint16_t l_998 = 0UL;
        uint32_t l_1032 = 4UL;
        int i, j;
        for (i = 0; i < 10; i++)
        {
            for (j = 0; j < 2; j++)
                l_993[i][j] = 0x0D6E88D0L;
        }
        l_873++;
        for (g_292 = 0; (g_292 <= 5); g_292 += 1)
        { /* block id: 409 */
            struct S1 **l_881 = &l_880[0][3][4];
            int32_t l_892 = 1L;
            uint8_t *l_893 = &g_73;
            uint16_t *l_894 = &g_54;
            int16_t *l_895 = &g_75;
            int32_t ** const **l_910 = &l_909[0];
            int32_t l_938 = 0x470A2184L;
            int32_t l_979 = 0x0FDAD366L;
            int32_t l_986 = (-2L);
            int32_t l_988 = 0x53776739L;
            int32_t l_989[1][4][9] = {{{0x62212923L,0x62212923L,0xF3980FCBL,0x62212923L,0x62212923L,0xF3980FCBL,0x62212923L,0x62212923L,0xF3980FCBL},{(-1L),(-1L),0xB4103CCBL,(-1L),(-1L),0xB4103CCBL,(-1L),(-1L),0xB4103CCBL},{0x62212923L,0x62212923L,0xF3980FCBL,0x62212923L,0x62212923L,0xF3980FCBL,0x62212923L,0x62212923L,0xF3980FCBL},{(-1L),(-1L),0xB4103CCBL,(-1L),(-1L),0xB4103CCBL,(-1L),(-1L),0xB4103CCBL}}};
            int16_t l_995 = 0L;
            int i, j, k;
            l_872 &= ((l_892 = (safe_mod_func_int16_t_s_s(1L, ((*l_895) = (safe_div_func_uint32_t_u_u(((((*l_881) = l_880[0][3][4]) == &g_435[0]) , (safe_add_func_uint32_t_u_u(((safe_rshift_func_uint16_t_u_u(((*l_894) &= (((*l_893) = ((2UL == (((((safe_mul_func_uint8_t_u_u(p_35.f0, (**g_317))) | (*g_111)) != p_35.f0) <= (safe_rshift_func_int8_t_s_s(((safe_add_func_int64_t_s_s(p_35.f1, p_36)) != l_892), (*g_318)))) < (*l_838))) , 4UL)) , (*g_311))), p_36)) > l_892), (*g_111)))), l_892)))))) || g_143.f0);
            for (g_672 = 0; (g_672 <= 5); g_672 += 1)
            { /* block id: 418 */
                uint32_t l_899 = 18446744073709551614UL;
                float *l_908 = &g_103[7][1][0];
                float **l_907 = &l_908;
                float ***l_906 = &l_907;
                p_36 = ((*g_376) , (l_896 == ((safe_mod_func_int64_t_s_s((l_899 || (safe_sub_func_int8_t_s_s((p_35.f1 >= (0x99L != ((safe_add_func_uint64_t_u_u(((**g_532) = (((l_872 <= l_899) == (((*l_906) = (void*)0) != (void*)0)) | p_35.f1)), l_899)) != l_899))), p_36))), (-1L))) , (void*)0)));
            }
            (*l_910) = l_909[0];
            for (l_868 = 0; (l_868 <= 5); l_868 += 1)
            { /* block id: 426 */
                int32_t l_943 = 0xC6E4364CL;
                int32_t ** const *l_954 = (void*)0;
                int32_t ** const **l_953 = &l_954;
                struct S1 ***l_955 = (void*)0;
                struct S1 ***l_956 = &l_881;
                int32_t l_983 = 8L;
                int32_t l_984[3];
                int32_t l_996 = 0L;
                int i;
                for (i = 0; i < 3; i++)
                    l_984[i] = 0L;
                for (g_73 = 0; (g_73 <= 0); g_73 += 1)
                { /* block id: 429 */
                    const int32_t *l_912 = &g_913;
                    const int32_t **l_911 = &l_912;
                    const int32_t *l_915 = &l_892;
                    const int32_t **l_914 = &l_915;
                    int8_t *l_939 = &g_295;
                    const float l_940[7][1] = {{(-0x1.Ap+1)},{(-0x1.Ap+1)},{(-0x9.Ep+1)},{(-0x1.Ap+1)},{(-0x1.Ap+1)},{(-0x9.Ep+1)},{(-0x1.Ap+1)}};
                    int i, j;
                    (*l_914) = ((*l_911) = &l_872);
                    if ((((+(safe_lshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s(((((((*l_881) = (void*)0) == (g_941 = (((((void*)0 == &g_311) , (safe_lshift_func_uint16_t_u_u(g_59[(g_73 + 2)][g_292], (safe_sub_func_int16_t_s_s((safe_add_func_int32_t_s_s(((safe_mul_func_int8_t_s_s(((*l_939) = (((**g_480) != (void*)0) || (safe_sub_func_uint16_t_u_u(0xDC2AL, ((safe_unary_minus_func_int8_t_s((safe_div_func_int8_t_s_s((safe_add_func_uint32_t_u_u(g_353[g_73][g_73], (safe_rshift_func_int8_t_s_s(l_938, 2)))), p_35.f1)))) && (*g_311)))))), p_35.f1)) , l_870), 0x2F0F15E3L)), p_35.f1))))) != 0x394D0C06L) , (void*)0))) & l_870) ^ 2L) > p_35.f0), (*g_318))), (*l_836)))) | 65535UL) || l_938))
                    { /* block id: 435 */
                        return p_36;
                    }
                    else
                    { /* block id: 437 */
                        (*l_911) = ((*l_914) = (*g_172));
                        return l_943;
                    }
                }
                if ((safe_sub_func_int64_t_s_s(((+(0xBE736375L | (((*l_862) = (+((((*g_311) , p_35) , l_873) | 65535UL))) , (~((safe_sub_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_u(((*l_893) = p_35.f1), 1)) , (((*l_956) = (((((*l_910) == ((*l_953) = (*l_910))) || l_892) >= (*g_318)) , &l_880[0][4][2])) != &g_941)), 1L)) > (**g_532)))))) || p_35.f0), 0x5A2F0031B21596DELL)))
                { /* block id: 447 */
                    for (l_835 = 0; (l_835 >= 0); l_835 -= 1)
                    { /* block id: 450 */
                        int i, j, k;
                        (**g_375) = (safe_add_func_uint16_t_u_u((*g_311), (safe_lshift_func_int16_t_s_s((0x70L <= ((*g_83) < (((*g_533) ^= (safe_rshift_func_int8_t_s_s((safe_add_func_uint64_t_u_u(((((((safe_mod_func_int32_t_s_s((&g_470 != (void*)0), (safe_div_func_int8_t_s_s((safe_add_func_uint8_t_u_u((((g_143.f2 >= p_35.f1) >= ((p_35.f1 , (safe_mod_func_int32_t_s_s((safe_add_func_uint16_t_u_u(((*l_894) ^= (*g_311)), (*g_311))), p_36))) ^ (-1L))) <= (-10L)), p_35.f0)), (**g_317))))) <= 0x76E6L) , g_103[(l_835 + 1)][l_835][l_835]) != g_103[(l_868 + 4)][l_835][l_835]) , g_587.f2) || l_872), 1L)), 7))) == (-9L)))), g_59[5][4]))));
                    }
                }
                else
                { /* block id: 455 */
                    int32_t l_977 = (-8L);
                    int32_t l_978 = 0L;
                    int32_t l_980 = 1L;
                    int32_t l_981 = 1L;
                    int32_t l_982 = 1L;
                    int32_t l_985 = 0x156F61A4L;
                    int32_t l_990 = (-1L);
                    int32_t l_991[6][4] = {{(-1L),1L,(-1L),1L},{(-1L),1L,(-1L),1L},{(-1L),1L,(-1L),1L},{(-1L),1L,(-1L),1L},{(-1L),1L,(-1L),1L},{(-1L),1L,(-1L),1L}};
                    int64_t l_994 = (-3L);
                    const int32_t **l_1001 = (void*)0;
                    const int32_t *l_1003[10][4][2] = {{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}},{{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913},{&g_913,&g_913}}};
                    const int32_t **l_1002 = &l_1003[6][0][0];
                    const int32_t *l_1005 = &l_871;
                    const int32_t **l_1004 = &l_1005;
                    uint16_t **l_1010[6][8][5] = {{{&l_894,&g_311,&g_311,&g_311,&g_311},{&g_311,&l_894,&g_311,&g_311,&g_311},{&l_894,&g_311,&l_894,&g_311,&g_311},{&l_894,&g_311,&g_311,&g_311,&g_311},{(void*)0,(void*)0,&g_311,&g_311,&g_311},{&l_894,&g_311,&g_311,&g_311,(void*)0},{&g_311,(void*)0,&l_894,&l_894,&g_311},{&l_894,&l_894,(void*)0,&g_311,&g_311}},{{&g_311,&l_894,&g_311,(void*)0,&g_311},{&l_894,&l_894,(void*)0,&g_311,&g_311},{&g_311,&l_894,&l_894,&l_894,&g_311},{&g_311,&l_894,&g_311,&g_311,&g_311},{(void*)0,(void*)0,&g_311,&l_894,&g_311},{&l_894,&g_311,&g_311,&l_894,&g_311},{&l_894,&l_894,&l_894,(void*)0,(void*)0},{&g_311,&g_311,&g_311,(void*)0,&l_894}},{{(void*)0,(void*)0,&g_311,&g_311,(void*)0},{&g_311,&l_894,&l_894,&l_894,&g_311},{&g_311,&l_894,&g_311,&g_311,(void*)0},{(void*)0,&l_894,&g_311,&g_311,&g_311},{&g_311,&l_894,&g_311,&l_894,&l_894},{&l_894,&l_894,&g_311,&g_311,&l_894},{&g_311,&l_894,&g_311,(void*)0,&g_311},{&g_311,(void*)0,&l_894,(void*)0,&l_894}},{{&g_311,&l_894,&l_894,&l_894,&g_311},{&g_311,&g_311,&g_311,&l_894,&g_311},{&g_311,&g_311,&g_311,&g_311,&g_311},{&l_894,(void*)0,&l_894,&l_894,&g_311},{&g_311,&l_894,&l_894,&g_311,&l_894},{(void*)0,&g_311,&l_894,(void*)0,&l_894},{&g_311,&g_311,&l_894,&g_311,&g_311},{&g_311,(void*)0,&g_311,(void*)0,&g_311}},{{(void*)0,&l_894,&g_311,(void*)0,(void*)0},{&g_311,&g_311,&l_894,&l_894,&g_311},{&l_894,&g_311,&l_894,&l_894,(void*)0},{&l_894,&l_894,&g_311,&g_311,&g_311},{(void*)0,&g_311,&g_311,(void*)0,&g_311},{&g_311,&g_311,&g_311,&l_894,&l_894},{&g_311,&l_894,&g_311,&g_311,&l_894},{&l_894,&l_894,&g_311,&g_311,&g_311}},{{&g_311,&g_311,&l_894,&g_311,&g_311},{&l_894,&g_311,&g_311,&l_894,&g_311},{&l_894,&l_894,&g_311,&l_894,&g_311},{&l_894,&g_311,&l_894,(void*)0,&l_894},{&l_894,&g_311,&g_311,&l_894,&g_311},{&g_311,&g_311,(void*)0,(void*)0,&g_311},{(void*)0,&l_894,&g_311,&g_311,&g_311},{&g_311,(void*)0,(void*)0,&l_894,&g_311}}};
                    uint8_t *l_1022 = (void*)0;
                    uint8_t *l_1023 = &g_353[0][3];
                    int8_t *l_1024 = &g_623;
                    uint8_t *l_1025 = &g_673;
                    float *l_1026 = &g_103[9][0][0];
                    int i, j, k;
                    (*l_836) ^= (safe_mul_func_uint16_t_u_u(((*g_311) = 0x2231L), (p_35 , g_97)));
                    l_998++;
                    (*l_1004) = ((*l_1002) = &l_980);
                    (*l_1026) = ((safe_lshift_func_uint8_t_u_u((--(*l_893)), ((*l_1025) = ((((g_377.f0 != (&g_54 != (l_1011 = (void*)0))) == p_35.f1) <= (p_36 >= g_232)) && ((*l_895) = ((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((((safe_mod_func_int16_t_s_s(g_435[2].f2, ((safe_sub_func_int8_t_s_s(((*l_1024) &= ((safe_add_func_uint8_t_u_u((((*l_1023) = ((l_993[5][0] = l_870) , p_36)) && 0xD4L), 1UL)) , 0xA9L)), p_36)) ^ p_35.f0))) == l_997) || p_36), (*l_836))), p_36)) != l_987)))))) , (-0x1.3p-1));
                }
            }
        }
        for (l_866 = 0; (l_866 <= 0); l_866 += 1)
        { /* block id: 474 */
            int64_t l_1027[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1027[i] = 0L;
        }
        for (l_866 = (-8); (l_866 != (-16)); l_866 = safe_sub_func_int64_t_s_s(l_866, 8))
        { /* block id: 498 */
            int32_t l_1035[6];
            int i;
            for (i = 0; i < 6; i++)
                l_1035[i] = 0x588EF511L;
            --l_1032;
            --l_1036;
            (*g_1039) = p_35.f1;
            if (g_1040[3])
                break;
        }
    }
    else
    { /* block id: 504 */
        const int32_t *l_1041 = &g_913;
        (*g_1042) = l_1041;
        return g_146;
    }
    for (g_673 = 0; (g_673 <= 32); ++g_673)
    { /* block id: 510 */
        int32_t l_1069 = 0xCF6977BEL;
        int32_t l_1073 = (-1L);
        uint8_t ***l_1086 = &g_82;
        uint32_t l_1096 = 0xD8966ACEL;
        uint8_t *l_1104 = &g_353[0][3];
        uint64_t *l_1118[6][1];
        const int32_t *****l_1122 = &g_1119[1];
        float *l_1123 = (void*)0;
        float *l_1124[5][3][9] = {{{&l_867,(void*)0,(void*)0,&g_103[5][2][0],&g_103[9][0][0],&l_867,&l_867,&l_867,&l_867},{(void*)0,&l_867,&l_867,&l_867,(void*)0,&l_867,(void*)0,&g_103[8][2][0],&g_103[9][0][0]},{&g_103[2][2][0],&l_867,&l_867,&g_103[9][0][0],(void*)0,&l_867,&g_103[9][0][0],&l_867,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,&l_867,&l_867,&l_867,&g_103[9][0][0],&g_103[9][2][0]},{(void*)0,&g_103[8][0][0],&g_103[7][3][0],&l_867,&g_103[9][2][0],&l_867,&l_867,&g_103[9][2][0],&l_867},{&g_103[2][2][0],&l_867,&g_103[2][2][0],&g_103[7][3][0],&l_867,&g_103[9][0][0],&g_103[9][0][0],&g_103[5][2][0],&l_867}},{{(void*)0,&g_103[2][2][0],&g_103[9][2][0],&l_867,(void*)0,&g_103[5][2][0],(void*)0,&l_867,&g_103[9][2][0]},{&l_867,&l_867,&l_867,&g_103[7][3][0],(void*)0,&g_103[2][2][0],&g_103[9][2][0],&l_867,(void*)0},{&g_103[9][0][0],&g_103[9][0][0],&g_103[5][2][0],&l_867,&g_103[9][0][0],&g_103[9][0][0],&l_867,&g_103[5][2][0],&g_103[9][0][0]}},{{&g_103[9][0][0],&g_103[8][2][0],&l_867,(void*)0,&g_103[8][0][0],&g_103[7][3][0],&l_867,&g_103[9][2][0],&l_867},{&l_867,&g_103[9][0][0],&g_103[9][2][0],&g_103[9][0][0],&l_867,&g_103[9][0][0],&g_103[9][2][0],&g_103[9][0][0],&l_867},{&g_103[8][0][0],&g_103[8][2][0],&g_103[2][2][0],&l_867,&l_867,&g_103[9][0][0],(void*)0,&l_867,&g_103[9][0][0]}},{{&g_103[8][2][0],&g_103[9][0][0],&g_103[7][3][0],&g_103[5][2][0],&g_103[5][2][0],&g_103[7][3][0],&g_103[9][0][0],&g_103[8][2][0],(void*)0},{&g_103[8][0][0],&l_867,(void*)0,(void*)0,&g_103[5][2][0],&g_103[9][0][0],&l_867,&l_867,&l_867},{&l_867,&g_103[2][2][0],&l_867,&g_103[8][2][0],&l_867,&g_103[2][2][0],&l_867,&g_103[9][0][0],(void*)0}}};
        float **l_1144 = &l_1124[3][0][0];
        float ***l_1143 = &l_1144;
        int32_t ****l_1190 = &g_426;
        const uint64_t l_1315 = 0xC6A8BE7FC894300ALL;
        int8_t l_1363 = 0x73L;
        uint64_t ***l_1425[10] = {&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532};
        const uint64_t *l_1431 = &l_1315;
        const uint64_t **l_1430[5] = {&l_1431,&l_1431,&l_1431,&l_1431,&l_1431};
        const uint64_t ***l_1429 = &l_1430[1];
        uint16_t l_1461 = 0x4E57L;
        struct S0 l_1504[2] = {{9602,-1L},{9602,-1L}};
        int32_t l_1512[3][8][5] = {{{0x8B562A98L,0xA4CF41B3L,9L,9L,0xA4CF41B3L},{0xD5CA0C89L,0x5D59AB34L,0xFE21026EL,(-8L),(-8L)},{0xEE24B18DL,3L,0xEE24B18DL,9L,(-1L)},{0xB220099CL,0xE6F165EAL,(-8L),0xE6F165EAL,0xB220099CL},{0xEE24B18DL,0x8B562A98L,3L,0xA4CF41B3L,9L},{0x2E39776CL,0x2E39776CL,0xD5CA0C89L,0xFE21026EL,0xE6F165EAL},{0xEE24B18DL,0xA4CF41B3L,0xA4CF41B3L,0xEE24B18DL,9L},{0x5D59AB34L,0xFE21026EL,(-8L),(-8L),0xFE21026EL}},{{9L,0xA4CF41B3L,(-1L),3L,3L},{0xB220099CL,0x2E39776CL,0xB220099CL,(-8L),0xD5CA0C89L},{9L,0xEE24B18DL,3L,0xEE24B18DL,9L},{0xB220099CL,0x5D59AB34L,0x2E39776CL,0xFE21026EL,0x2E39776CL},{9L,9L,3L,9L,0x8B562A98L},{0x5D59AB34L,0xB220099CL,0xB220099CL,0x5D59AB34L,0x2E39776CL},{0xEE24B18DL,9L,(-1L),(-1L),9L},{0x2E39776CL,0xB220099CL,(-8L),0xD5CA0C89L,0xD5CA0C89L}},{{0xA4CF41B3L,9L,0xA4CF41B3L,(-1L),3L},{0xFE21026EL,0x5D59AB34L,0xD5CA0C89L,0x5D59AB34L,0xFE21026EL},{0xA4CF41B3L,0xEE24B18DL,9L,9L,9L},{0x2E39776CL,0x2E39776CL,0xD5CA0C89L,0xFE21026EL,0xE6F165EAL},{0xEE24B18DL,0xA4CF41B3L,0xA4CF41B3L,0xEE24B18DL,9L},{0x5D59AB34L,0xFE21026EL,(-8L),(-8L),0xFE21026EL},{9L,0xA4CF41B3L,(-1L),3L,3L},{0xB220099CL,0x2E39776CL,0xB220099CL,(-8L),0xD5CA0C89L}}};
        int i, j, k;
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 1; j++)
                l_1118[i][j] = &g_817[0][7][1];
        }
    }
    l_1535[1] |= (((0xB19BL <= (safe_mod_func_int8_t_s_s((&l_1514 != &l_1514), 0x98L))) & (safe_mod_func_int32_t_s_s((safe_mod_func_int64_t_s_s((l_1523[0][4][3] > (safe_mod_func_uint64_t_u_u((p_36 == (safe_div_func_int8_t_s_s(0L, ((safe_add_func_uint8_t_u_u((p_36 > (!((((((*g_318) = ((safe_sub_func_uint32_t_u_u(1UL, 0x1D47A5D8L)) != g_621)) && p_35.f0) , (**g_317)) != 0x9EL) , (*l_838)))), l_1533[0][8][0])) | p_36)))), l_1534[1][1]))), 0xB383B30072AD6F25LL)), (*g_1043)))) & (*g_311));
    return g_131[2][2][3];
}


/* ------------------------------------------ */
/* 
 * reads : g_381.f4 g_587 g_73 g_143.f2 g_311 g_293 g_136 g_77 g_318 g_111 g_621 g_623 g_426 g_427 g_75 g_361 g_292 g_377.f1 g_251 g_672 g_673 g_150 g_59 g_470 g_54 g_509 g_480 g_481 g_353 g_532 g_533 g_286 g_360 g_300 g_376 g_377 g_67.f0 g_798 g_317 g_814 g_817 g_824 g_83 g_13
 * writes: g_77 g_73 g_293 g_146 g_292 g_295 g_623 g_427 g_75 g_377.f1 g_251 g_103 g_470 g_54 g_377 g_111 g_300 g_361 g_817 g_825 g_828 g_59 g_286 g_82
 */
static uint8_t  func_40(uint8_t  p_41, uint16_t  p_42)
{ /* block id: 22 */
    struct S0 l_109 = {1024,0L};
    int32_t *l_110 = &g_77;
    int32_t l_120 = 0x969D30BDL;
    int32_t l_122 = (-9L);
    int32_t l_125[3][4] = {{0x10C6DFD3L,1L,0x10C6DFD3L,0x10C6DFD3L},{1L,1L,0L,1L},{1L,0x10C6DFD3L,0x10C6DFD3L,1L}};
    uint16_t *l_165[1];
    uint16_t l_218 = 0xCD61L;
    uint32_t l_260 = 4294967295UL;
    int8_t *l_316 = &g_146;
    int8_t **l_315 = &l_316;
    uint8_t l_337[5] = {0x0BL,0x0BL,0x0BL,0x0BL,0x0BL};
    const uint32_t l_338 = 0UL;
    float l_535[8][10] = {{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1},{0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1,0x8.3p-1}};
    int16_t l_572[4][9][2] = {{{(-7L),0x01EAL},{0xE673L,0xE673L},{0xE673L,0x01EAL},{(-7L),0x5E14L},{0x01EAL,0x5E14L},{(-7L),0x01EAL},{0xE673L,0xE673L},{0xE673L,0x01EAL},{(-7L),0x5E14L}},{{0x01EAL,0x5E14L},{(-7L),0x01EAL},{0xE673L,0xE673L},{0xE673L,0x01EAL},{(-7L),0x5E14L},{0x01EAL,0x5E14L},{(-7L),0x01EAL},{0xE673L,0xE673L},{0xE673L,0x01EAL}},{{(-7L),0x5E14L},{0x01EAL,0x5E14L},{(-7L),0x01EAL},{0xE673L,0xE673L},{0xE673L,0x01EAL},{(-7L),0x5E14L},{0x01EAL,0x5E14L},{(-7L),0x01EAL},{0xE673L,0xE673L}},{{0xE673L,0x01EAL},{(-7L),0x5E14L},{0x01EAL,0x5E14L},{(-7L),0x01EAL},{0xE673L,0xE673L},{0xE673L,0x01EAL},{(-7L),0x5E14L},{0x01EAL,0x5E14L},{(-7L),0x01EAL}}};
    uint32_t *l_588 = (void*)0;
    uint32_t *l_589[4][5] = {{(void*)0,&g_97,&g_97,(void*)0,&g_97},{(void*)0,&g_251,&g_251,&g_251,&g_251},{&g_97,&g_97,&g_251,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_251,&g_97}};
    uint8_t *l_590 = &g_73;
    float l_670[6] = {0x4.C8A878p+50,0x5.43EDD1p-62,0x4.C8A878p+50,0x4.C8A878p+50,0x5.43EDD1p-62,0x4.C8A878p+50};
    uint64_t ***l_695 = &g_532;
    struct S1 *l_764[8][10] = {{&g_67,&g_381,&g_67,&g_143,&g_381,(void*)0,(void*)0,&g_381,&g_143,&g_67},{&g_143,&g_143,&g_381,&g_381,&g_435[2],&g_381,&g_435[2],&g_381,&g_381,&g_143},{&g_435[2],(void*)0,&g_67,&g_435[2],&g_143,&g_143,&g_435[2],&g_67,(void*)0,&g_435[2]},{&g_67,&g_143,(void*)0,&g_143,&g_143,&g_143,(void*)0,&g_143,&g_67,&g_67},{&g_435[2],&g_381,&g_381,&g_143,&g_143,&g_381,&g_381,&g_435[2],&g_381,&g_435[2]},{&g_143,(void*)0,&g_143,&g_143,&g_143,(void*)0,&g_143,&g_67,&g_67,&g_143},{&g_67,&g_435[2],&g_143,&g_143,&g_435[2],&g_67,(void*)0,&g_435[2],(void*)0,&g_67},{&g_381,&g_435[2],&g_381,&g_435[2],&g_381,&g_381,&g_143,&g_143,&g_381,&g_381}};
    int32_t l_805 = 0x129F49B9L;
    float l_815 = 0x9.2F73C8p-41;
    uint8_t ***l_834 = &g_82;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_165[i] = &g_54;
    for (p_41 = 23; (p_41 > 43); ++p_41)
    { /* block id: 25 */
        float l_124 = 0xC.DA8B21p+99;
        int32_t l_127 = 0x06370641L;
        int32_t l_128 = 1L;
        struct S0 l_178 = {10838,0x7554994F792DA7FELL};
        int32_t l_229 = (-1L);
        int32_t l_230[10][3] = {{4L,0x2DBCC6B9L,0x2DBCC6B9L},{0x9850A8AAL,0x1C9156F2L,0x1C9156F2L},{4L,0x2DBCC6B9L,0x2DBCC6B9L},{0x9850A8AAL,0x1C9156F2L,0xF5A8E44CL},{0x2DBCC6B9L,0x9750AC28L,0x9750AC28L},{0x1C9156F2L,0xF5A8E44CL,0xF5A8E44CL},{0x2DBCC6B9L,0x9750AC28L,0x9750AC28L},{0x1C9156F2L,0xF5A8E44CL,0xF5A8E44CL},{0x2DBCC6B9L,0x9750AC28L,0x9750AC28L},{0x1C9156F2L,0xF5A8E44CL,0xF5A8E44CL}};
        int64_t l_252 = 0xA282962CD732F0C6LL;
        int32_t *l_253 = (void*)0;
        int32_t l_298 = 0x4530BE24L;
        uint16_t **l_308 = (void*)0;
        uint16_t ***l_309 = &l_308;
        uint16_t **l_310[2][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0}};
        int32_t *l_357 = (void*)0;
        struct S1 *l_380 = &g_381;
        struct S1 **l_379 = &l_380;
        uint64_t *l_432 = &g_300;
        uint64_t **l_431 = &l_432;
        float * const l_437 = &l_124;
        float * const *l_436[9][10][2] = {{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}},{{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437},{&l_437,&l_437}}};
        int64_t l_440 = 0x0D8D69B9A8EBB0A3LL;
        int32_t *l_451[9];
        const float l_460 = (-0x8.8p-1);
        int32_t ****l_538 = &g_426;
        int16_t l_550 = 0xE3EFL;
        int64_t l_568 = 0x9C243F79390E117BLL;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_451[i] = &l_125[1][0];
    }
    if (((safe_div_func_uint8_t_u_u(((*l_590) ^= (((*l_110) = (safe_lshift_func_uint8_t_u_u((g_381.f4 && ((safe_mod_func_int64_t_s_s((&l_338 == &l_260), (safe_mul_func_int16_t_s_s((((safe_lshift_func_int16_t_s_u((-1L), 15)) , &l_165[0]) != ((&l_338 == (g_587 , &l_260)) , (void*)0)), p_42)))) == p_41)), p_41))) | 4294967295UL)), p_41)) <= 0UL))
    { /* block id: 284 */
        uint16_t l_610 = 6UL;
        int8_t *l_620 = &g_295;
        int8_t *l_622 = &g_623;
        struct S1 * const l_676[3][5][2] = {{{&g_143,(void*)0},{(void*)0,&g_143},{(void*)0,(void*)0},{&g_143,(void*)0},{(void*)0,&g_143}},{{(void*)0,(void*)0},{&g_143,(void*)0},{(void*)0,&g_143},{(void*)0,(void*)0},{&g_143,(void*)0}},{{(void*)0,&g_143},{(void*)0,(void*)0},{&g_143,(void*)0},{(void*)0,&g_143},{(void*)0,(void*)0}}};
        int i, j, k;
        (***g_426) = (((g_143.f2 , (*g_311)) && (safe_sub_func_uint32_t_u_u((safe_sub_func_int64_t_s_s((safe_add_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(0x38L, 4)), (safe_lshift_func_uint8_t_u_s(0UL, ((*l_622) ^= (safe_sub_func_uint8_t_u_u(((*l_590) = (((safe_rshift_func_uint8_t_u_u((((!((safe_mod_func_int8_t_s_s(l_610, ((255UL == ((*l_620) = (safe_rshift_func_uint16_t_u_u(((*g_311)++), (safe_mul_func_uint16_t_u_u(((g_136[5] != ((safe_unary_minus_func_uint8_t_u((safe_sub_func_int64_t_s_s((*l_110), (((*g_318) = ((*l_316) = (*l_110))) > 0x64L))))) , g_111)) >= 0x3D18L), p_42)))))) & (*l_110)))) | g_621)) > 0xC3CDL) | 0x3BF081D7L), 5)) && (-8L)) | p_41)), l_610))))))), 0x3AL)), 0x1D3F37E45825A18BLL)), 9UL))) == 0xD19413CFL);
        for (l_610 = 0; (l_610 != 40); l_610 = safe_add_func_uint32_t_u_u(l_610, 5))
        { /* block id: 294 */
            uint16_t *l_649 = &l_218;
            int32_t l_650 = 0xE42D5A34L;
            uint8_t l_664[10][4] = {{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL},{253UL,1UL,1UL,253UL}};
            uint64_t l_671 = 18446744073709551607UL;
            int i, j;
            for (l_122 = 0; (l_122 != 12); l_122 = safe_add_func_int8_t_s_s(l_122, 6))
            { /* block id: 297 */
                int32_t **l_643 = (void*)0;
                int32_t ***l_642 = &l_643;
                uint16_t **l_646 = &g_311;
                int16_t *l_647 = &g_75;
                uint16_t **l_648[3][8] = {{&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0]},{&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0]},{&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0],&l_165[0]}};
                int i, j;
                (*g_361) &= (p_42 == ((*l_590) = (g_587.f4 < g_75)));
                (*g_361) = ((safe_sub_func_int64_t_s_s((0x93L <= (((*l_316) = (safe_div_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_u(((*g_318) ^= (safe_mul_func_int16_t_s_s((safe_div_func_uint16_t_u_u((*g_311), (*g_311))), (safe_sub_func_int64_t_s_s(((p_42 = (*g_311)) , ((((*g_426) = &l_110) == ((*l_642) = (void*)0)) <= (safe_lshift_func_int16_t_s_s(((*l_647) = (l_646 == &l_165[0])), ((((l_165[0] = (l_649 = &l_218)) == &l_610) , l_650) , l_610))))), 0UL))))), 1)) , 246UL), p_41))) != (*l_110))), l_610)) != p_41);
            }
            for (g_377.f1 = 0; (g_377.f1 < 25); g_377.f1 = safe_add_func_int16_t_s_s(g_377.f1, 5))
            { /* block id: 312 */
                uint64_t ***l_667 = &g_532;
                struct S1 *l_678 = (void*)0;
                struct S1 **l_677 = &l_678;
                for (g_251 = 0; (g_251 != 36); g_251 = safe_add_func_uint64_t_u_u(g_251, 3))
                { /* block id: 315 */
                    uint32_t l_674[2][5] = {{18446744073709551610UL,18446744073709551610UL,18446744073709551610UL,18446744073709551610UL,18446744073709551610UL},{2UL,0xC2D1741EL,2UL,0xC2D1741EL,2UL}};
                    float *l_675 = &g_103[8][2][0];
                    int i, j;
                    (*l_675) = ((safe_add_func_float_f_f((safe_mul_func_float_f_f(p_41, 0x1.9p-1)), (p_42 < (p_41 < ((!(safe_add_func_float_f_f(0x8.EABC92p-96, (safe_div_func_float_f_f(((-0x1.Bp+1) < ((l_664[5][0] , ((safe_mul_func_float_f_f((l_667 == (((((((*l_110) = (safe_mul_func_uint8_t_u_u(((((((((*l_110) | 0xD7L) != p_42) & 1L) <= l_671) || g_672) || p_41) , p_41), (*g_318)))) == p_42) < p_41) , (void*)0) == (void*)0) , l_667)), p_42)) != 0x2.Bp-1)) >= p_41)), g_673))))) == p_41))))) >= l_674[0][3]);
                }
                (*l_677) = l_676[1][4][1];
                if ((*l_110))
                    continue;
                (**g_150) = (-6L);
            }
        }
    }
    else
    { /* block id: 324 */
        uint32_t * const l_683[8][5][6] = {{{&g_97,(void*)0,(void*)0,&g_97,(void*)0,&g_672},{&g_251,&l_260,&g_97,&g_672,&l_260,&g_251},{&l_260,(void*)0,&g_251,(void*)0,&l_260,(void*)0},{&g_251,&l_260,&l_260,&g_251,(void*)0,&g_97},{&g_97,(void*)0,&l_260,&l_260,(void*)0,&g_97}},{{&g_251,&g_672,&l_260,&l_260,&g_97,(void*)0},{(void*)0,&g_251,&g_251,&g_97,&g_251,&g_251},{(void*)0,(void*)0,&g_97,&l_260,&l_260,&g_672},{&g_251,&g_97,(void*)0,&l_260,&l_260,(void*)0},{&g_97,&g_97,(void*)0,&g_251,&l_260,&l_260}},{{&g_251,(void*)0,&l_260,(void*)0,&g_251,(void*)0},{&l_260,&g_251,&l_260,&g_672,&g_97,&l_260},{&g_251,&g_672,&g_251,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_251,(void*)0},{&l_260,(void*)0,(void*)0,(void*)0,&l_260,&g_251}},{{&l_260,&g_251,&g_251,&g_251,&l_260,&l_260},{&g_251,(void*)0,&g_251,&g_672,&g_251,(void*)0},{(void*)0,&g_97,(void*)0,(void*)0,&g_97,(void*)0},{&g_672,(void*)0,&g_251,&l_260,(void*)0,&l_260},{&g_97,&g_251,&g_251,(void*)0,&g_251,&g_251}},{{&g_97,&l_260,(void*)0,&l_260,&g_251,(void*)0},{&g_672,(void*)0,&g_97,(void*)0,(void*)0,&g_97},{(void*)0,(void*)0,&g_251,&g_672,&g_251,(void*)0},{&g_251,&l_260,&l_260,&g_251,&g_251,&g_251},{&l_260,&g_251,&l_260,(void*)0,(void*)0,(void*)0}},{{&l_260,(void*)0,&g_251,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_251,(void*)0},{&l_260,(void*)0,(void*)0,(void*)0,&l_260,&g_251},{&l_260,&g_251,&g_251,&g_251,&l_260,&l_260},{&g_251,(void*)0,&g_251,&g_672,&g_251,(void*)0}},{{(void*)0,&g_97,(void*)0,(void*)0,&g_97,(void*)0},{&g_672,(void*)0,&g_251,&l_260,(void*)0,&l_260},{&g_97,&g_251,&g_251,(void*)0,&g_251,&g_251},{&g_97,&l_260,(void*)0,&l_260,&g_251,(void*)0},{&g_672,(void*)0,&g_97,(void*)0,(void*)0,&g_97}},{{(void*)0,(void*)0,&g_251,&g_672,&g_251,(void*)0},{&g_251,&l_260,&l_260,&g_251,&g_251,&g_251},{&l_260,&g_251,&l_260,(void*)0,(void*)0,(void*)0},{&l_260,(void*)0,&g_251,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_251,(void*)0}}};
        int32_t l_687 = 0x36D6B7CCL;
        int32_t l_688 = (-8L);
        int32_t l_689 = 0L;
        int32_t l_690[3];
        uint32_t l_692[4];
        int64_t *l_696[10][5][5] = {{{(void*)0,&g_59[0][0],(void*)0,&l_109.f1,&g_59[2][2]},{&g_377.f1,&l_109.f1,&l_109.f1,(void*)0,&g_59[2][1]},{&g_37.f1,(void*)0,&l_109.f1,(void*)0,(void*)0},{&l_109.f1,&g_59[2][2],&g_59[0][0],(void*)0,(void*)0},{&g_59[5][2],&l_109.f1,&g_59[5][2],&g_37.f1,&g_59[2][1]}},{{(void*)0,&l_109.f1,&l_109.f1,&g_37.f1,&g_59[2][1]},{&l_109.f1,&l_109.f1,&g_59[1][5],&l_109.f1,&l_109.f1},{&g_59[2][1],&g_59[2][1],&l_109.f1,&g_59[2][1],(void*)0},{&g_59[2][2],&g_59[2][1],&g_377.f1,&l_109.f1,&g_37.f1},{&l_109.f1,(void*)0,(void*)0,&l_109.f1,&g_59[1][5]}},{{&l_109.f1,&l_109.f1,&l_109.f1,&l_109.f1,&l_109.f1},{&g_59[2][2],&l_109.f1,(void*)0,(void*)0,&l_109.f1},{&g_59[2][1],&g_59[0][0],&l_109.f1,&l_109.f1,&l_109.f1},{&l_109.f1,&g_37.f1,(void*)0,(void*)0,&l_109.f1},{(void*)0,&g_37.f1,(void*)0,&l_109.f1,(void*)0}},{{&g_377.f1,&l_109.f1,&g_37.f1,&l_109.f1,(void*)0},{&l_109.f1,&l_109.f1,&g_59[2][2],&l_109.f1,&l_109.f1},{&l_109.f1,&g_37.f1,(void*)0,&g_59[2][1],&g_59[2][1]},{&g_59[2][2],&g_37.f1,&g_59[2][1],&l_109.f1,(void*)0},{(void*)0,&g_59[0][0],&l_109.f1,&g_37.f1,&g_59[2][1]}},{{(void*)0,&l_109.f1,&l_109.f1,&l_109.f1,&l_109.f1},{&g_59[2][1],&l_109.f1,&g_37.f1,&g_59[2][1],(void*)0},{&l_109.f1,(void*)0,&g_37.f1,&g_37.f1,(void*)0},{&l_109.f1,&g_59[2][1],&l_109.f1,&g_59[0][0],&l_109.f1},{&g_59[0][0],&g_59[2][1],&l_109.f1,(void*)0,&l_109.f1}},{{(void*)0,&l_109.f1,&g_59[2][1],&g_59[2][2],&l_109.f1},{&g_59[0][0],&l_109.f1,(void*)0,&g_59[2][1],&l_109.f1},{&l_109.f1,(void*)0,&g_59[2][2],(void*)0,&g_59[1][5]},{&l_109.f1,(void*)0,&g_37.f1,(void*)0,&g_37.f1},{&g_59[2][1],&g_59[2][1],(void*)0,&g_59[2][1],(void*)0}},{{(void*)0,&l_109.f1,(void*)0,&g_59[2][2],&l_109.f1},{(void*)0,(void*)0,&l_109.f1,(void*)0,&g_59[2][1]},{&g_59[2][2],&l_109.f1,(void*)0,&g_59[0][0],(void*)0},{&l_109.f1,&g_59[2][1],&l_109.f1,&g_37.f1,&g_59[2][1]},{&l_109.f1,(void*)0,(void*)0,&g_59[2][1],&g_59[2][1]}},{{&g_377.f1,(void*)0,&g_377.f1,&l_109.f1,(void*)0},{(void*)0,&l_109.f1,&l_109.f1,&g_37.f1,&g_59[2][1]},{&l_109.f1,&l_109.f1,&g_59[1][5],&l_109.f1,&l_109.f1},{&g_59[2][1],&g_59[2][1],&l_109.f1,&g_59[2][1],(void*)0},{&g_59[2][2],&g_59[2][1],&g_377.f1,&l_109.f1,&g_37.f1}},{{&l_109.f1,(void*)0,(void*)0,&l_109.f1,&g_59[1][5]},{&l_109.f1,&l_109.f1,&l_109.f1,&l_109.f1,&l_109.f1},{&g_59[2][2],&l_109.f1,(void*)0,(void*)0,&l_109.f1},{(void*)0,(void*)0,&l_109.f1,(void*)0,(void*)0},{(void*)0,&l_109.f1,&g_59[0][0],&g_59[2][1],&l_109.f1}},{{&l_109.f1,&l_109.f1,&g_59[2][1],&l_109.f1,&g_37.f1},{&g_59[2][2],(void*)0,&l_109.f1,(void*)0,&l_109.f1},{&g_59[2][1],(void*)0,(void*)0,(void*)0,&g_59[2][1]},{&l_109.f1,&l_109.f1,&g_37.f1,&l_109.f1,&g_59[5][2]},{&g_59[2][1],&l_109.f1,&g_59[1][5],&l_109.f1,&g_37.f1}}};
        struct S0 l_726[1] = {{9822,-1L}};
        const uint16_t **l_820 = (void*)0;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_690[i] = 1L;
        for (i = 0; i < 4; i++)
            l_692[i] = 6UL;
        if ((safe_mul_func_int8_t_s_s((*g_318), ((l_683[0][2][4] != &g_251) >= 0L))))
        { /* block id: 325 */
            int32_t *l_684 = &l_120;
            int32_t *l_685 = &g_77;
            int32_t *l_686[5];
            int8_t l_691[4][9] = {{0x69L,0xE4L,0xD4L,0xE4L,0x69L,0xD4L,(-1L),(-1L),0xD4L},{0x69L,0xE4L,0xD4L,0xE4L,0x69L,0xD4L,(-1L),(-1L),0xD4L},{0x69L,0xE4L,0xD4L,0xE4L,0x69L,0xD4L,(-1L),(-1L),0xD4L},{0x69L,0xE4L,0xD4L,0xE4L,0x69L,0xD4L,(-1L),(-1L),0xD4L}};
            int i, j;
            for (i = 0; i < 5; i++)
                l_686[i] = &l_125[0][0];
            ++l_692[1];
        }
        else
        { /* block id: 327 */
            int16_t l_711 = (-9L);
            uint8_t **l_795 = &l_590;
            uint64_t l_797[7] = {18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL,18446744073709551612UL};
            int32_t * const l_810[1] = {&l_122};
            uint32_t l_827[9];
            int i;
            for (i = 0; i < 9; i++)
                l_827[i] = 4294967295UL;
            if (((l_695 = &g_532) != ((((l_696[6][4][3] == (void*)0) , (safe_add_func_int32_t_s_s((***g_426), ((safe_div_func_int32_t_s_s(l_692[0], (((void*)0 == &l_120) , ((+(safe_mul_func_uint16_t_u_u(((void*)0 != &g_481), (*g_311)))) , 1L)))) , g_59[5][0])))) && l_690[2]) , &g_532)))
            { /* block id: 329 */
                int16_t l_727[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_727[i] = 0x2C45L;
                for (g_470 = 14; (g_470 < 52); g_470 = safe_add_func_int64_t_s_s(g_470, 3))
                { /* block id: 332 */
                    struct S0 l_706 = {4404,0x46D194BAF11C047BLL};
                    int32_t l_744 = 0x6FBDD325L;
                    int64_t *l_750[9] = {&l_726[0].f1,&l_706.f1,&l_726[0].f1,&l_706.f1,&l_726[0].f1,&l_706.f1,&l_726[0].f1,&l_706.f1,&l_726[0].f1};
                    int i;
                    for (g_54 = 2; (g_54 <= 6); g_54 += 1)
                    { /* block id: 335 */
                        (*g_509) = l_706;
                    }
                    for (g_251 = 0; (g_251 != 25); ++g_251)
                    { /* block id: 340 */
                        int16_t *l_714 = &l_572[2][0][0];
                        int32_t l_745 = 1L;
                        int32_t *l_746 = (void*)0;
                        int32_t *l_747 = &l_687;
                        uint8_t *l_761 = &l_337[4];
                        if ((*l_110))
                            break;
                        (*l_747) ^= (((safe_sub_func_int16_t_s_s((l_711 != ((safe_lshift_func_int16_t_s_u(((*l_714) = 0xF8FFL), (p_42 = ((safe_div_func_uint8_t_u_u(((*l_590) = ((safe_rshift_func_int8_t_s_u(((safe_div_func_uint16_t_u_u(0x5B8FL, ((safe_unary_minus_func_uint16_t_u(((*g_311) = ((safe_mul_func_int16_t_s_s((((~(safe_unary_minus_func_int8_t_s((l_726[0] , l_727[0])))) , (((safe_div_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_s((((safe_sub_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((p_41 = (safe_rshift_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u(((p_42 , l_690[2]) <= 0x58C3L), p_42)) || (*g_311)), (*l_110))) , p_41), p_42))), 4)), g_293)) , p_41) , l_690[2]), 13)), 6)), 0xC462463286DBDFB8LL)) != p_42) , (void*)0)) == (*g_480)), l_744)) || (*g_311))))) , (*l_110)))) < (*l_110)), (*l_110))) & 0x26L)), 0xACL)) || l_744)))) , l_745)), 0x49E8L)) != 0xC0FE5D28L) || 0x0CF565C4L);
                        l_690[2] = (safe_lshift_func_uint16_t_u_s(((((void*)0 == l_750[6]) == (safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((((((g_353[0][2] > (**g_532)) >= ((safe_lshift_func_uint8_t_u_u((++(*l_590)), ((*l_761)--))) != (((*l_110) &= 252UL) == 0x91L))) & (p_42 >= (l_764[6][1] != (void*)0))) , (-10L)) == l_689), p_41)), (*g_318)))) >= l_711), 3));
                    }
                    (**g_426) = (*g_360);
                }
            }
            else
            { /* block id: 355 */
                const int32_t *l_774 = &l_690[1];
                const int32_t **l_773 = &l_774;
                const int32_t ***l_772 = &l_773;
                int32_t l_784 = 0xE06D45D2L;
                int32_t l_803 = 1L;
                int32_t l_804[8][4][5] = {{{0x6B83DA8EL,0L,0xEDD8A704L,(-7L),0x6459C89AL},{1L,(-1L),0x4D65327BL,0xB07EC47FL,(-1L)},{0x7AB515D1L,0x78049938L,0xEDD8A704L,0x6459C89AL,0x70504405L},{3L,(-1L),0x3DC563EFL,1L,0x6FC7A628L}},{{0L,0x5CCA6590L,0x70504405L,0xDD05B69CL,6L},{0xD4702663L,0x5C6511FEL,0x5C6511FEL,0xD4702663L,(-1L)},{4L,(-5L),0x6B83DA8EL,0L,(-2L)},{(-1L),0xA31116A0L,0xD4702663L,(-1L),1L}},{{0L,(-1L),0x2A3A1AEAL,0L,(-5L)},{0x9A6F340EL,0x6FC7A628L,0x424E7759L,0xD4702663L,0xA4CACD4CL},{0x1DA6892FL,0L,(-7L),0xDD05B69CL,0x206D64A2L},{0x5C6511FEL,1L,3L,1L,0x5C6511FEL}},{{(-2L),(-7L),0x5CCA6590L,0x6459C89AL,0x5C13E5ABL},{1L,0L,0xEC0234D1L,0xB07EC47FL,0x85DD9BDDL},{0L,0x9FCC8E9EL,0xE32E3A4FL,(-7L),0x5C13E5ABL},{1L,0xB07EC47FL,0x051B371DL,0L,0x5C6511FEL}},{{0x5C13E5ABL,0xDD2FAC4AL,0xE5786F48L,0xD47E534AL,0x206D64A2L},{0xB07EC47FL,0xA4CACD4CL,1L,1L,0xA4CACD4CL},{0xEDD8A704L,0x7AB515D1L,0xDD05B69CL,0x9FCC8E9EL,(-5L)},{0xA31116A0L,(-7L),(-1L),0L,1L}},{{0x5CCA6590L,0xE32E3A4FL,6L,0x6459C89AL,0xDD05B69CL},{(-3L),(-1L),1L,1L,0x9A6F340EL},{0x8E00DEBBL,0L,0x2A3A1AEAL,0xD47E534AL,0x7AB515D1L},{1L,0xA31116A0L,2L,0xEC0234D1L,2L}},{{0xCF3A8B06L,0xCF3A8B06L,0x5C13E5ABL,0x373CAC75L,0xD47E534AL},{0x424E7759L,0L,0xA31116A0L,5L,0x85DD9BDDL},{0x5C13E5ABL,0xD47E534AL,0xDD2FAC4AL,0xEDD8A704L,(-5L)},{5L,0L,0xD4702663L,(-3L),0x424E7759L}},{{0xDD05B69CL,0xCF3A8B06L,0x78049938L,(-7L),6L},{(-7L),0xA31116A0L,0xEC0234D1L,0x4D65327BL,0x4D65327BL},{(-4L),0L,(-4L),0xE32E3A4FL,0L},{0xD4702663L,(-1L),0x85DD9BDDL,2L,0xB07EC47FL}}};
                uint32_t **l_826 = &l_589[1][2];
                int i, j, k;
                for (g_300 = 22; (g_300 <= 31); g_300 = safe_add_func_uint32_t_u_u(g_300, 1))
                { /* block id: 358 */
                    struct S1 **l_767 = &l_764[6][8];
                    int32_t l_796 = 0xA12C6634L;
                    int32_t l_806[9] = {0x5120F688L,0x5120F688L,0x5120F688L,0x5120F688L,0x5120F688L,0x5120F688L,0x5120F688L,0x5120F688L,0x5120F688L};
                    uint32_t l_807 = 0xC968471BL;
                    int16_t l_816 = 4L;
                    int i;
                    (*l_767) = &g_435[2];
                    if ((safe_lshift_func_int16_t_s_s((((**g_532) < (((safe_rshift_func_uint16_t_u_s(l_711, 15)) == l_726[0].f0) , p_42)) , ((void*)0 == l_772)), (((safe_rshift_func_int8_t_s_s((p_41 == g_300), (*l_110))) && (*g_311)) ^ 0xD9L))))
                    { /* block id: 360 */
                        int32_t *l_799 = &l_687;
                        float *l_800 = &g_103[4][2][0];
                        int32_t l_801 = 0xE58BEDAAL;
                        int32_t *l_802[10][7][2] = {{{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]}},{{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]}},{{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]}},{{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]}},{{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]}},{{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]}},{{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]}},{{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]}},{{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]}},{{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]},{&l_784,&l_125[1][2]},{&l_690[2],&l_125[1][2]}}};
                        int i, j, k;
                        (*l_799) ^= (!(((safe_div_func_uint64_t_u_u(((*g_376) , (0xFCL ^ (p_42 , (safe_div_func_uint8_t_u_u(((safe_mul_func_uint32_t_u_u(((-1L) & (l_784 = (0xACC3EC1E7974EB85LL && 0xE86C784389F6E582LL))), (((*g_111) = ((((g_67.f0 , ((safe_div_func_int64_t_s_s((0x9E8CL > ((safe_lshift_func_int8_t_s_s(((((safe_add_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_mod_func_uint8_t_u_u(((l_795 != (void*)0) == l_796), 250UL)), p_41)), (-2L))) >= (*g_311)) <= 0xCED9L) && p_41), 7)) != l_797[6])), p_41)) | 65535UL)) == p_42) ^ 0xE15EL) & l_688)) < (*l_774)))) || 0xEBL), p_42))))), g_798)) || 0xDB929EA6C02225CELL) || (**g_317)));
                        l_688 |= (*l_110);
                        (*l_800) = 0xD.5D674Bp-67;
                        l_807--;
                    }
                    else
                    { /* block id: 367 */
                        int32_t **l_811[1];
                        int32_t **l_812[3];
                        int32_t **l_813 = (void*)0;
                        const uint16_t ***l_821 = &l_820;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_811[i] = &g_111;
                        for (i = 0; i < 3; i++)
                            l_812[i] = &g_111;
                        (*g_814) = l_810[0];
                        ++g_817[2][4][1];
                        (*l_821) = l_820;
                        return l_816;
                    }
                    for (l_803 = 0; (l_803 == 5); l_803 = safe_add_func_uint16_t_u_u(l_803, 7))
                    { /* block id: 375 */
                        (***g_426) &= (-1L);
                    }
                    for (l_218 = 0; (l_218 <= 2); l_218 += 1)
                    { /* block id: 380 */
                        int i;
                        (*g_824) = &g_136[(l_218 + 2)];
                    }
                }
                (**g_360) ^= (((*l_826) = ((***l_772) , (void*)0)) == &g_251);
                l_690[0] |= (*g_361);
                l_827[2] = ((*g_111) = ((void*)0 != &p_42));
            }
            l_690[2] &= ((&g_59[5][3] != (g_828 = (void*)0)) , (((*g_533) = (((safe_div_func_int64_t_s_s((g_59[4][5] = ((void*)0 == &l_726[0])), (safe_rshift_func_uint8_t_u_s((~(p_42 != ((void*)0 == l_696[6][4][3]))), 3)))) | (p_42 > (((*l_110) ^ 0x2211L) >= p_41))) , p_41)) | 0x70CC58CC989DCF08LL));
        }
    }
    (*l_834) = (void*)0;
    return (*g_83);
}


/* ------------------------------------------ */
/* 
 * reads : g_37.f1 g_67 g_37.f0 g_54 g_75 g_77 g_81 g_82 g_73
 * writes: g_59 g_73 g_75 g_77 g_82 g_97 g_67.f2 g_67.f0 g_103
 */
static int64_t  func_45(int8_t  p_46, int32_t  p_47)
{ /* block id: 2 */
    int64_t *l_58 = &g_59[2][1];
    int64_t l_60 = 1L;
    uint8_t *l_72 = &g_73;
    int16_t *l_74 = &g_75;
    int32_t *l_76[8][4][8] = {{{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77},{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77},{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77},{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77}},{{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77},{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77},{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77},{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77}},{{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77},{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77},{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77},{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77}},{{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77},{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77},{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77},{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77}},{{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77},{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77},{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77},{(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77}},{{(void*)0,&g_77,&g_77,&g_77,(void*)0,&g_77,(void*)0,&g_77},{&g_77,&g_77,&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77},{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77}},{{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77}},{{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,(void*)0,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77},{&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77,&g_77}}};
    uint8_t ***l_100 = (void*)0;
    int32_t l_104 = 6L;
    int i, j, k;
    g_77 |= (((*l_74) ^= (((*l_58) = ((!p_47) , (((safe_mod_func_uint8_t_u_u(g_37.f1, (-1L))) && 0xF0BDE8AFF65CE17CLL) ^ (0x7EL | 4L)))) && ((*l_58) = (l_60 | ((safe_lshift_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(((safe_div_func_int32_t_s_s((((*l_72) = (((g_67 , ((safe_div_func_int32_t_s_s((safe_add_func_uint16_t_u_u(((g_67.f3 ^ p_46) , g_37.f0), 0xCBC3L)), 3UL)) >= l_60)) && 0x73ECL) || p_46)) >= 0xC2L), g_54)) & g_67.f3), p_47)), p_47)) > 0x2EL))))) < 65534UL);
    for (g_75 = 0; (g_75 < 15); g_75++)
    { /* block id: 10 */
        uint8_t **l_80 = &l_72;
        uint32_t *l_96 = &g_97;
        int32_t l_98 = (-7L);
        uint32_t l_99 = 18446744073709551608UL;
        int32_t l_101 = 7L;
        float *l_102[3];
        int i;
        for (i = 0; i < 3; i++)
            l_102[i] = &g_103[9][0][0];
        (*g_81) = l_80;
        g_103[9][3][0] = (((safe_sub_func_uint32_t_u_u(((g_75 >= (((g_67.f0 = (g_67.f2 = (l_101 = ((((((safe_mod_func_uint16_t_u_u(((((((~(((((g_67.f0 | (p_46 | g_37.f0)) >= ((safe_mul_func_uint8_t_u_u((((safe_add_func_uint8_t_u_u((0xCDF3L == (((*l_96) = (((safe_rshift_func_uint8_t_u_u(0x4FL, 1)) , (((void*)0 == &l_80) | (~p_47))) != p_47)) && l_98)), p_46)) & l_98) , (***g_81)), p_47)) > p_46)) && g_73) >= l_98) | 0x706B0045B34FEB13LL)) != p_46) , 0xA1A09BBF73BC91A0LL) , 7L) , l_98) > (**g_82)), 0xF33DL)) == l_99) && g_75) , (void*)0) == l_100) >= g_67.f0)))) && p_46) > p_47)) & p_47), g_37.f0)) && p_47) , 0x2.0p-1);
        g_77 &= 0L;
    }
    return l_104;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_37.f0, "g_37.f0", print_hash_value);
    transparent_crc(g_37.f1, "g_37.f1", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_59[i][j], "g_59[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_67.f0, "g_67.f0", print_hash_value);
    transparent_crc(g_67.f1, "g_67.f1", print_hash_value);
    transparent_crc(g_67.f2, "g_67.f2", print_hash_value);
    transparent_crc(g_67.f3, "g_67.f3", print_hash_value);
    transparent_crc(g_67.f4, "g_67.f4", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_75, "g_75", print_hash_value);
    transparent_crc(g_77, "g_77", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc_bytes(&g_103[i][j][k], sizeof(g_103[i][j][k]), "g_103[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_129, "g_129", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_131[i][j][k], "g_131[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_143.f0, "g_143.f0", print_hash_value);
    transparent_crc(g_143.f1, "g_143.f1", print_hash_value);
    transparent_crc(g_143.f2, "g_143.f2", print_hash_value);
    transparent_crc(g_143.f3, "g_143.f3", print_hash_value);
    transparent_crc(g_143.f4, "g_143.f4", print_hash_value);
    transparent_crc(g_146, "g_146", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_171[i][j], "g_171[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_232, "g_232", print_hash_value);
    transparent_crc(g_251, "g_251", print_hash_value);
    transparent_crc(g_286, "g_286", print_hash_value);
    transparent_crc(g_292, "g_292", print_hash_value);
    transparent_crc(g_293, "g_293", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_300, "g_300", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_353[i][j], "g_353[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_377.f0, "g_377.f0", print_hash_value);
    transparent_crc(g_377.f1, "g_377.f1", print_hash_value);
    transparent_crc(g_381.f0, "g_381.f0", print_hash_value);
    transparent_crc(g_381.f1, "g_381.f1", print_hash_value);
    transparent_crc(g_381.f2, "g_381.f2", print_hash_value);
    transparent_crc(g_381.f3, "g_381.f3", print_hash_value);
    transparent_crc(g_381.f4, "g_381.f4", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_435[i].f0, "g_435[i].f0", print_hash_value);
        transparent_crc(g_435[i].f1, "g_435[i].f1", print_hash_value);
        transparent_crc(g_435[i].f2, "g_435[i].f2", print_hash_value);
        transparent_crc(g_435[i].f3, "g_435[i].f3", print_hash_value);
        transparent_crc(g_435[i].f4, "g_435[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_470, "g_470", print_hash_value);
    transparent_crc(g_559.f0, "g_559.f0", print_hash_value);
    transparent_crc(g_559.f1, "g_559.f1", print_hash_value);
    transparent_crc(g_559.f2, "g_559.f2", print_hash_value);
    transparent_crc(g_559.f3, "g_559.f3", print_hash_value);
    transparent_crc(g_559.f4, "g_559.f4", print_hash_value);
    transparent_crc(g_569, "g_569", print_hash_value);
    transparent_crc(g_587.f0, "g_587.f0", print_hash_value);
    transparent_crc(g_587.f1, "g_587.f1", print_hash_value);
    transparent_crc(g_587.f2, "g_587.f2", print_hash_value);
    transparent_crc(g_587.f3, "g_587.f3", print_hash_value);
    transparent_crc(g_587.f4, "g_587.f4", print_hash_value);
    transparent_crc(g_621, "g_621", print_hash_value);
    transparent_crc(g_623, "g_623", print_hash_value);
    transparent_crc(g_672, "g_672", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_798, "g_798", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_817[i][j][k], "g_817[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_913, "g_913", print_hash_value);
    transparent_crc(g_942.f0, "g_942.f0", print_hash_value);
    transparent_crc(g_942.f1, "g_942.f1", print_hash_value);
    transparent_crc(g_942.f2, "g_942.f2", print_hash_value);
    transparent_crc(g_942.f3, "g_942.f3", print_hash_value);
    transparent_crc(g_942.f4, "g_942.f4", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1040[i], "g_1040[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1097, "g_1097", print_hash_value);
    transparent_crc(g_1202, "g_1202", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1225[i][j][k].f0, "g_1225[i][j][k].f0", print_hash_value);
                transparent_crc(g_1225[i][j][k].f1, "g_1225[i][j][k].f1", print_hash_value);
                transparent_crc(g_1225[i][j][k].f2, "g_1225[i][j][k].f2", print_hash_value);
                transparent_crc(g_1225[i][j][k].f3, "g_1225[i][j][k].f3", print_hash_value);
                transparent_crc(g_1225[i][j][k].f4, "g_1225[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1242, "g_1242", print_hash_value);
    transparent_crc(g_1314.f0, "g_1314.f0", print_hash_value);
    transparent_crc(g_1314.f1, "g_1314.f1", print_hash_value);
    transparent_crc(g_1314.f2, "g_1314.f2", print_hash_value);
    transparent_crc(g_1314.f3, "g_1314.f3", print_hash_value);
    transparent_crc(g_1314.f4, "g_1314.f4", print_hash_value);
    transparent_crc(g_1319, "g_1319", print_hash_value);
    transparent_crc(g_1381.f0, "g_1381.f0", print_hash_value);
    transparent_crc(g_1381.f1, "g_1381.f1", print_hash_value);
    transparent_crc(g_1381.f2, "g_1381.f2", print_hash_value);
    transparent_crc(g_1381.f3, "g_1381.f3", print_hash_value);
    transparent_crc(g_1381.f4, "g_1381.f4", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1505[i][j][k], "g_1505[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1654, "g_1654", print_hash_value);
    transparent_crc(g_1656.f0, "g_1656.f0", print_hash_value);
    transparent_crc(g_1656.f1, "g_1656.f1", print_hash_value);
    transparent_crc(g_1656.f2, "g_1656.f2", print_hash_value);
    transparent_crc(g_1656.f3, "g_1656.f3", print_hash_value);
    transparent_crc(g_1656.f4, "g_1656.f4", print_hash_value);
    transparent_crc(g_1754, "g_1754", print_hash_value);
    transparent_crc(g_1777, "g_1777", print_hash_value);
    transparent_crc(g_1920.f0, "g_1920.f0", print_hash_value);
    transparent_crc(g_1920.f1, "g_1920.f1", print_hash_value);
    transparent_crc(g_1920.f2, "g_1920.f2", print_hash_value);
    transparent_crc(g_1920.f3, "g_1920.f3", print_hash_value);
    transparent_crc(g_1920.f4, "g_1920.f4", print_hash_value);
    transparent_crc_bytes (&g_1945, sizeof(g_1945), "g_1945", print_hash_value);
    transparent_crc(g_1981, "g_1981", print_hash_value);
    transparent_crc(g_2101, "g_2101", print_hash_value);
    transparent_crc(g_2116, "g_2116", print_hash_value);
    transparent_crc(g_2192, "g_2192", print_hash_value);
    transparent_crc(g_2223.f0, "g_2223.f0", print_hash_value);
    transparent_crc(g_2223.f1, "g_2223.f1", print_hash_value);
    transparent_crc(g_2223.f2, "g_2223.f2", print_hash_value);
    transparent_crc(g_2223.f3, "g_2223.f3", print_hash_value);
    transparent_crc(g_2223.f4, "g_2223.f4", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2255[i], "g_2255[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2281, "g_2281", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2490[i][j].f0, "g_2490[i][j].f0", print_hash_value);
            transparent_crc(g_2490[i][j].f1, "g_2490[i][j].f1", print_hash_value);
            transparent_crc(g_2490[i][j].f2, "g_2490[i][j].f2", print_hash_value);
            transparent_crc(g_2490[i][j].f3, "g_2490[i][j].f3", print_hash_value);
            transparent_crc(g_2490[i][j].f4, "g_2490[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2501, "g_2501", print_hash_value);
    transparent_crc(g_2556.f0, "g_2556.f0", print_hash_value);
    transparent_crc(g_2556.f1, "g_2556.f1", print_hash_value);
    transparent_crc(g_2556.f2, "g_2556.f2", print_hash_value);
    transparent_crc(g_2556.f3, "g_2556.f3", print_hash_value);
    transparent_crc(g_2556.f4, "g_2556.f4", print_hash_value);
    transparent_crc(g_2557.f0, "g_2557.f0", print_hash_value);
    transparent_crc(g_2557.f1, "g_2557.f1", print_hash_value);
    transparent_crc(g_2557.f2, "g_2557.f2", print_hash_value);
    transparent_crc(g_2557.f3, "g_2557.f3", print_hash_value);
    transparent_crc(g_2557.f4, "g_2557.f4", print_hash_value);
    transparent_crc(g_2558.f0, "g_2558.f0", print_hash_value);
    transparent_crc(g_2558.f1, "g_2558.f1", print_hash_value);
    transparent_crc(g_2558.f2, "g_2558.f2", print_hash_value);
    transparent_crc(g_2558.f3, "g_2558.f3", print_hash_value);
    transparent_crc(g_2558.f4, "g_2558.f4", print_hash_value);
    transparent_crc(g_2596, "g_2596", print_hash_value);
    transparent_crc(g_2652, "g_2652", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 596
   depth: 1, occurrence: 41
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 6
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 68
breakdown:
   indirect level: 0, occurrence: 41
   indirect level: 1, occurrence: 12
   indirect level: 2, occurrence: 12
   indirect level: 3, occurrence: 3
XXX full-bitfields structs in the program: 12
breakdown:
   indirect level: 0, occurrence: 12
XXX times a bitfields struct's address is taken: 143
XXX times a bitfields struct on LHS: 7
XXX times a bitfields struct on RHS: 57
XXX times a single bitfield on LHS: 5
XXX times a single bitfield on RHS: 102

XXX max expression depth: 53
breakdown:
   depth: 1, occurrence: 153
   depth: 2, occurrence: 35
   depth: 3, occurrence: 1
   depth: 4, occurrence: 4
   depth: 10, occurrence: 1
   depth: 12, occurrence: 1
   depth: 15, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 2
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1
   depth: 53, occurrence: 1

XXX total number of pointers: 591

XXX times a variable address is taken: 1581
XXX times a pointer is dereferenced on RHS: 414
breakdown:
   depth: 1, occurrence: 334
   depth: 2, occurrence: 70
   depth: 3, occurrence: 7
   depth: 4, occurrence: 3
XXX times a pointer is dereferenced on LHS: 389
breakdown:
   depth: 1, occurrence: 361
   depth: 2, occurrence: 22
   depth: 3, occurrence: 6
XXX times a pointer is compared with null: 58
XXX times a pointer is compared with address of another variable: 20
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 8302

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1438
   level: 2, occurrence: 260
   level: 3, occurrence: 164
   level: 4, occurrence: 58
   level: 5, occurrence: 49
XXX number of pointers point to pointers: 269
XXX number of pointers point to scalars: 291
XXX number of pointers point to structs: 31
XXX percent of pointers has null in alias set: 27.9
XXX average alias set size: 1.44

XXX times a non-volatile is read: 2062
XXX times a non-volatile is write: 1068
XXX times a volatile is read: 138
XXX    times read thru a pointer: 37
XXX times a volatile is write: 68
XXX    times written thru a pointer: 22
XXX times a volatile is available for access: 6.38e+03
XXX percentage of non-volatile access: 93.8

XXX forward jumps: 0
XXX backward jumps: 4

XXX stmts: 141
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 27
   depth: 2, occurrence: 20
   depth: 3, occurrence: 19
   depth: 4, occurrence: 19
   depth: 5, occurrence: 23

XXX percentage a fresh-made variable is used: 18
XXX percentage an existing variable is used: 82
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


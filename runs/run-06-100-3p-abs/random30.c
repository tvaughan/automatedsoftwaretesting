/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2603444646
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   uint16_t  f0;
   uint32_t  f1;
   uint32_t  f2;
   const int32_t  f3;
   volatile float  f4;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 0xD2EDC755L;
static int16_t g_9 = 0x1957L;
static volatile int32_t g_10 = 0L;/* VOLATILE GLOBAL g_10 */
static int8_t g_11 = 0L;
static uint64_t g_12 = 0x6E74A6E8F59172DDLL;
static uint8_t g_73 = 0x7EL;
static volatile int32_t *g_78 = &g_10;
static volatile int32_t ** volatile g_77 = &g_78;/* VOLATILE GLOBAL g_77 */
static uint16_t g_83 = 9UL;
static uint16_t g_85 = 0xC132L;
static int32_t *g_87[4][2][10] = {{{&g_3,(void*)0,(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,(void*)0},{&g_3,(void*)0,&g_3,&g_3,&g_3,&g_3,(void*)0,&g_3,(void*)0,&g_3}},{{(void*)0,&g_3,&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3},{&g_3,&g_3,&g_3,(void*)0,&g_3,(void*)0,&g_3,&g_3,&g_3,&g_3}},{{&g_3,(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3,(void*)0,&g_3,&g_3},{(void*)0,&g_3,(void*)0,&g_3,&g_3,&g_3,&g_3,(void*)0,&g_3,(void*)0}},{{(void*)0,(void*)0,&g_3,&g_3,&g_3,(void*)0,(void*)0,(void*)0,(void*)0,&g_3},{(void*)0,(void*)0,(void*)0,(void*)0,&g_3,&g_3,&g_3,(void*)0,(void*)0,(void*)0}}};
static uint8_t g_93 = 7UL;
static int32_t ** volatile g_101 = &g_87[2][1][8];/* VOLATILE GLOBAL g_101 */
static uint8_t **g_203 = (void*)0;
static float g_206 = 0x0.FDE11Bp+52;
static uint64_t g_207 = 0xBE4332A91C8B06D9LL;
static int64_t g_212 = 0x70B1722911373275LL;
static int64_t *g_211 = &g_212;
static float g_221 = 0x5.988CC2p-38;
static const int64_t g_227 = 0L;
static const int64_t g_229[5][8][5] = {{{0xCF053E8E71EE0A1ELL,(-1L),0x0EA170A1CF68CDDBLL,0x19102758DB9AF29FLL,1L},{0L,(-1L),0x1172B58F86E45068LL,3L,0xE8656E99656D3FD8LL},{(-7L),0x1C8C39424F6457C8LL,1L,0x4A979D5404555042LL,0x490400B35B371F4CLL},{0L,(-1L),0x7415AF54D6FE3569LL,(-10L),0x7415AF54D6FE3569LL},{0xCF053E8E71EE0A1ELL,0xCF053E8E71EE0A1ELL,7L,0x490400B35B371F4CLL,2L},{(-6L),0x122F168FF3D68D92LL,(-5L),(-1L),0L},{0x1C8C39424F6457C8LL,(-7L),0x0EA170A1CF68CDDBLL,0x864B6CB0D4D54530LL,(-7L)},{0L,0x122F168FF3D68D92LL,(-10L),3L,0xE8656E99656D3FD8LL}},{{(-1L),0xCF053E8E71EE0A1ELL,(-1L),0x0EA170A1CF68CDDBLL,0x19102758DB9AF29FLL},{0x7CCC286839DCD777LL,(-1L),0xB0F99A31C92298D2LL,1L,(-5L)},{0x03632AA098A50625LL,0x1C8C39424F6457C8LL,0x4FFC0F78E1B70F6BLL,0x490400B35B371F4CLL,0xCF053E8E71EE0A1ELL},{0x7415AF54D6FE3569LL,(-1L),0xB0F99A31C92298D2LL,1L,(-10L)},{0x1C8C39424F6457C8LL,(-1L),(-1L),0x1C8C39424F6457C8LL,1L},{0xDC185641E5C07114LL,(-10L),(-10L),3L,0x7CCC286839DCD777LL},{1L,0x03632AA098A50625LL,0x0EA170A1CF68CDDBLL,1L,0x19102758DB9AF29FLL},{0L,0x1DE2C0B917678978LL,(-5L),3L,0xB0F99A31C92298D2LL}},{{0x490400B35B371F4CLL,0x1C8C39424F6457C8LL,7L,0x1C8C39424F6457C8LL,0x490400B35B371F4CLL},{(-5L),1L,0x7415AF54D6FE3569LL,1L,0L},{0xCF053E8E71EE0A1ELL,1L,1L,0x490400B35B371F4CLL,0L},{0xDC185641E5C07114LL,0x122F168FF3D68D92LL,0x1172B58F86E45068LL,1L,0L},{0x4A979D5404555042LL,0x490400B35B371F4CLL,0x0EA170A1CF68CDDBLL,0x0EA170A1CF68CDDBLL,0x490400B35B371F4CLL},{0L,(-1L),0xF3114D939EB30AE1LL,3L,0xB0F99A31C92298D2LL},{0x03632AA098A50625LL,0xCF053E8E71EE0A1ELL,0x0140DCD4049CAE6DLL,0x864B6CB0D4D54530LL,0x19102758DB9AF29FLL},{(-5L),1L,0xB0F99A31C92298D2LL,(-1L),0x7CCC286839DCD777LL}},{{0x03632AA098A50625LL,0x4A979D5404555042LL,1L,0x490400B35B371F4CLL,1L},{0L,(-1L),3L,(-10L),(-10L)},{0x4A979D5404555042LL,0x03632AA098A50625LL,(-1L),0x4A979D5404555042LL,0xCF053E8E71EE0A1ELL},{0xDC185641E5C07114LL,1L,0xF3114D939EB30AE1LL,3L,(-5L)},{0xCF053E8E71EE0A1ELL,0x03632AA098A50625LL,(-8L),0x19102758DB9AF29FLL,0x19102758DB9AF29FLL},{(-5L),(-1L),(-5L),(-2L),0xE8656E99656D3FD8LL},{0x490400B35B371F4CLL,0x4A979D5404555042LL,1L,0x1C8C39424F6457C8LL,(-7L)},{0L,1L,(-10L),(-2L),0xE5B8610FE71C2523LL}},{{4L,0L,0x4FFC0F78E1B70F6BLL,(-8L),0x4A979D5404555042LL},{(-5L),0x0C64B01C82AA6F4FLL,0xC54636818AD0E44ALL,(-1L),(-1L)},{(-1L),0x0EA170A1CF68CDDBLL,0x19102758DB9AF29FLL,1L,0x0EA170A1CF68CDDBLL},{(-1L),1L,3L,1L,0x7415AF54D6FE3569LL},{(-7L),4L,0x8B6F583C0778F2E3LL,1L,0x4FFC0F78E1B70F6BLL},{0xF3114D939EB30AE1LL,(-1L),0L,(-1L),0xF3114D939EB30AE1LL},{0x8B6F583C0778F2E3LL,(-1L),4L,(-8L),0L},{0xE5B8610FE71C2523LL,(-1L),(-7L),(-2L),1L}}};
static const int64_t *g_228 = &g_229[2][1][3];
static volatile uint16_t g_235 = 65533UL;/* VOLATILE GLOBAL g_235 */
static volatile union U0 g_258 = {65535UL};/* VOLATILE GLOBAL g_258 */
static int16_t g_262 = 0L;
static int32_t ** volatile g_301 = &g_87[0][0][2];/* VOLATILE GLOBAL g_301 */
static volatile uint32_t g_307 = 0xEA3051C1L;/* VOLATILE GLOBAL g_307 */
static uint32_t g_334 = 0x4E945F99L;
static uint32_t g_338 = 18446744073709551615UL;
static volatile union U0 g_379[2] = {{65528UL},{65528UL}};
static const volatile union U0 g_420 = {0x1670L};/* VOLATILE GLOBAL g_420 */
static uint8_t ***g_494 = &g_203;
static uint8_t ****g_493[10] = {&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494};
static uint8_t ***** volatile g_492 = &g_493[6];/* VOLATILE GLOBAL g_492 */
static volatile int64_t g_495 = 0xE661701DABD2DE31LL;/* VOLATILE GLOBAL g_495 */
static int64_t *g_532[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const uint32_t g_545 = 0x2AE18FC3L;
static int32_t g_548 = 0xA6DAEB1DL;
static int32_t *g_547 = &g_548;
static volatile union U0 g_558 = {0x9201L};/* VOLATILE GLOBAL g_558 */
static volatile union U0 g_559 = {65535UL};/* VOLATILE GLOBAL g_559 */
static int32_t ** volatile g_566 = &g_547;/* VOLATILE GLOBAL g_566 */
static int32_t ** volatile g_569[6][6] = {{&g_87[0][0][2],&g_87[0][0][2],&g_87[1][0][4],(void*)0,&g_547,(void*)0},{&g_87[0][0][2],&g_547,(void*)0,(void*)0,(void*)0,(void*)0},{&g_87[0][0][2],&g_87[0][0][2],&g_547,(void*)0,&g_87[1][0][4],&g_87[0][0][2]},{&g_87[0][0][2],&g_547,&g_87[0][0][2],&g_547,&g_87[0][0][2],&g_547},{&g_547,&g_87[0][0][2],&g_87[0][0][2],&g_87[0][0][2],&g_87[0][0][2],&g_87[0][0][2]},{&g_547,&g_87[0][0][2],&g_547,&g_87[0][0][6],&g_87[0][0][2],(void*)0}};
static volatile float g_665 = 0x4.7p+1;/* VOLATILE GLOBAL g_665 */
static float * volatile g_671 = (void*)0;/* VOLATILE GLOBAL g_671 */
static volatile uint32_t *g_743 = &g_307;
static volatile uint32_t **g_742 = &g_743;
static uint8_t *****g_847 = &g_493[6];
static volatile union U0 g_859[5] = {{65529UL},{65529UL},{65529UL},{65529UL},{65529UL}};
static volatile union U0 g_862[2] = {{0x7195L},{0x7195L}};
static uint8_t g_863 = 0xA1L;
static volatile int16_t g_923[3] = {0xC85DL,0xC85DL,0xC85DL};
static uint32_t g_925 = 0xA04DE14BL;
static int64_t g_926[7] = {0L,0L,4L,0L,0L,4L,0L};
static uint32_t *g_935 = &g_338;
static uint32_t **g_934[5][7] = {{&g_935,&g_935,&g_935,&g_935,&g_935,&g_935,&g_935},{&g_935,&g_935,&g_935,&g_935,&g_935,&g_935,&g_935},{&g_935,&g_935,(void*)0,(void*)0,(void*)0,(void*)0,&g_935},{&g_935,&g_935,&g_935,&g_935,&g_935,&g_935,&g_935},{&g_935,&g_935,&g_935,&g_935,&g_935,&g_935,&g_935}};
static volatile uint8_t g_954[4] = {246UL,246UL,246UL,246UL};
static volatile union U0 g_980 = {65534UL};/* VOLATILE GLOBAL g_980 */
static volatile union U0 g_1003 = {0x06A2L};/* VOLATILE GLOBAL g_1003 */
static volatile int32_t ** volatile *g_1044 = &g_77;
static volatile int32_t ** volatile * volatile *g_1043 = &g_1044;
static int16_t g_1049 = 0x4BA7L;
static float * volatile g_1074 = &g_206;/* VOLATILE GLOBAL g_1074 */
static volatile uint32_t g_1159 = 0x45A6AA52L;/* VOLATILE GLOBAL g_1159 */
static int32_t g_1175 = 0x6FCFB659L;
static volatile float g_1204 = (-0x7.Dp+1);/* VOLATILE GLOBAL g_1204 */
static volatile int32_t * volatile * volatile g_1205 = &g_78;/* VOLATILE GLOBAL g_1205 */
static const int32_t *g_1211[6][7][5] = {{{&g_1175,&g_1175,&g_548,&g_3,&g_548},{&g_548,&g_548,&g_548,&g_3,(void*)0},{&g_1175,&g_1175,&g_1175,&g_1175,&g_548},{&g_1175,&g_3,(void*)0,(void*)0,&g_3},{&g_548,&g_1175,(void*)0,&g_548,&g_548},{&g_1175,&g_548,&g_1175,(void*)0,&g_548},{&g_3,&g_1175,&g_548,&g_1175,&g_3}},{{&g_1175,&g_1175,&g_548,(void*)0,(void*)0},{(void*)0,(void*)0,&g_548,(void*)0,&g_1175},{&g_1175,&g_3,&g_3,&g_1175,(void*)0},{&g_1175,(void*)0,&g_548,&g_548,(void*)0},{(void*)0,&g_3,&g_548,&g_548,&g_548},{&g_3,(void*)0,&g_3,&g_548,&g_548},{(void*)0,&g_1175,&g_548,&g_1175,(void*)0}},{{&g_3,&g_1175,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_548,(void*)0,&g_1175},{&g_1175,&g_3,&g_3,&g_1175,(void*)0},{&g_1175,(void*)0,&g_548,&g_548,(void*)0},{(void*)0,&g_3,&g_548,&g_548,&g_548},{&g_3,(void*)0,&g_3,&g_548,&g_548},{(void*)0,&g_1175,&g_548,&g_1175,(void*)0}},{{&g_3,&g_1175,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_548,(void*)0,&g_1175},{&g_1175,&g_3,&g_3,&g_1175,(void*)0},{&g_1175,(void*)0,&g_548,&g_548,(void*)0},{(void*)0,&g_3,&g_548,&g_548,&g_548},{&g_3,(void*)0,&g_3,&g_548,&g_548},{(void*)0,&g_1175,&g_548,&g_1175,(void*)0}},{{&g_3,&g_1175,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_548,(void*)0,&g_1175},{&g_1175,&g_3,&g_3,&g_1175,(void*)0},{&g_1175,(void*)0,&g_548,&g_548,(void*)0},{(void*)0,&g_3,&g_548,&g_548,&g_548},{&g_3,(void*)0,&g_3,&g_548,&g_548},{(void*)0,&g_1175,&g_548,&g_1175,(void*)0}},{{&g_3,&g_1175,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_548,(void*)0,&g_1175},{&g_1175,&g_3,&g_3,&g_1175,(void*)0},{&g_1175,(void*)0,&g_548,&g_548,(void*)0},{(void*)0,&g_3,&g_548,&g_548,&g_548},{&g_3,(void*)0,&g_3,&g_548,&g_548},{(void*)0,&g_1175,&g_548,&g_1175,(void*)0}}};
static uint8_t *** const **g_1232 = (void*)0;
static int64_t **g_1238 = (void*)0;
static volatile int32_t g_1268 = (-8L);/* VOLATILE GLOBAL g_1268 */
static volatile uint32_t g_1272 = 0x6B794539L;/* VOLATILE GLOBAL g_1272 */
static uint32_t g_1283 = 1UL;
static volatile union U0 g_1298 = {65531UL};/* VOLATILE GLOBAL g_1298 */
static uint32_t g_1300 = 0x6327477DL;
static union U0 g_1302[6] = {{0x8F3EL},{0x8F3EL},{0x8F3EL},{0x8F3EL},{0x8F3EL},{0x8F3EL}};
static float * volatile g_1338 = &g_221;/* VOLATILE GLOBAL g_1338 */
static int16_t *g_1339 = &g_1049;
static float * const  volatile g_1427 = (void*)0;/* VOLATILE GLOBAL g_1427 */
static float * volatile g_1428 = (void*)0;/* VOLATILE GLOBAL g_1428 */
static float * volatile g_1429[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static union U0 g_1433 = {0xAC19L};/* VOLATILE GLOBAL g_1433 */
static volatile union U0 *g_1454 = &g_379[1];
static volatile union U0 ** volatile g_1453 = &g_1454;/* VOLATILE GLOBAL g_1453 */
static const int32_t ** volatile g_1460 = (void*)0;/* VOLATILE GLOBAL g_1460 */
static const int32_t ** volatile g_1461 = &g_1211[3][5][1];/* VOLATILE GLOBAL g_1461 */
static uint32_t * volatile *g_1534 = &g_935;
static uint32_t * volatile **g_1533 = &g_1534;
static uint32_t * volatile *** const g_1532 = &g_1533;
static uint32_t * volatile *** const  volatile *g_1531 = &g_1532;
static uint32_t ****g_1536 = (void*)0;
static uint32_t *****g_1535 = &g_1536;
static volatile int8_t g_1541[5] = {1L,1L,1L,1L,1L};
static const int64_t g_1671 = 0xA00813D88AA0339CLL;
static volatile uint64_t g_1707 = 1UL;/* VOLATILE GLOBAL g_1707 */
static volatile uint64_t * volatile g_1706 = &g_1707;/* VOLATILE GLOBAL g_1706 */
static volatile uint64_t * volatile *g_1705[8] = {&g_1706,&g_1706,&g_1706,&g_1706,&g_1706,&g_1706,&g_1706,&g_1706};
static union U0 g_1719[6][8][5] = {{{{0UL},{0xBD53L},{0x33F0L},{0xC794L},{0x37E2L}},{{0xBB94L},{3UL},{65535UL},{0UL},{0x3E91L}},{{0xB3E2L},{5UL},{0xBD53L},{1UL},{65533UL}},{{0x3E91L},{65532UL},{0UL},{65535UL},{0x7B89L}},{{0x92FDL},{0UL},{0x1EDFL},{65533UL},{0x1131L}},{{0UL},{0UL},{65531UL},{0xD5B1L},{65535UL}},{{0xC433L},{65532UL},{0x7B89L},{0xC433L},{0xD5B1L}},{{1UL},{5UL},{1UL},{1UL},{0UL}}},{{{0x33F0L},{3UL},{0xE4C8L},{1UL},{0xB1ACL}},{{0x7211L},{0xBD53L},{0x56B5L},{0UL},{0xD563L}},{{0UL},{0UL},{0UL},{0x2101L},{0xBA04L}},{{65531UL},{4UL},{0xB19EL},{0xD5B1L},{65533UL}},{{1UL},{0xC868L},{1UL},{0xBB94L},{1UL}},{{0UL},{0x3E91L},{3UL},{5UL},{5UL}},{{65531UL},{65535UL},{65531UL},{1UL},{0x3E91L}},{{1UL},{0UL},{1UL},{65535UL},{0xAF0DL}}},{{{65535UL},{0xFBCAL},{65535UL},{65535UL},{0x92FDL}},{{0x33F0L},{65535UL},{1UL},{0xAF0DL},{0x0E6EL}},{{3UL},{65535UL},{65531UL},{0UL},{0xE4C8L}},{{0x4DA7L},{0x92FDL},{3UL},{0x2101L},{0xACD0L}},{{0UL},{5UL},{1UL},{0xAB63L},{4UL}},{{0xBA04L},{0x8B73L},{0xB19EL},{0x3E91L},{0xE4C8L}},{{0x7211L},{0x3E91L},{0UL},{65535UL},{0x1131L}},{{0xB3E2L},{0x3312L},{0x56B5L},{1UL},{0xBA04L}}},{{{65533UL},{5UL},{0xE4C8L},{4UL},{0xAF0DL}},{{65535UL},{65535UL},{1UL},{0xBB94L},{0x7211L}},{{65535UL},{0x7211L},{0x7B89L},{0x37E2L},{0xC794L}},{{0x3300L},{0x8B73L},{65531UL},{0UL},{1UL}},{{0UL},{0xC794L},{0x1EDFL},{0UL},{0xBB94L}},{{0x8614L},{0xFBCAL},{0UL},{0x37E2L},{0xFBCAL}},{{0xBA04L},{65535UL},{0xBD53L},{0xBB94L},{0xD563L}},{{3UL},{0UL},{65535UL},{4UL},{0x15B7L}}},{{{8UL},{0xBA04L},{0x33F0L},{1UL},{0xACD0L}},{{1UL},{0UL},{0xD563L},{65535UL},{0xD5B1L}},{{0xFBCAL},{1UL},{1UL},{0x3E91L},{0xB1ACL}},{{0x8614L},{0x7211L},{0xD5B1L},{0xAB63L},{0x0E6EL}},{{0x9043L},{0xC868L},{0x56B5L},{0x2101L},{0x7B89L}},{{0xBB94L},{65528UL},{1UL},{65528UL},{0UL}},{{0x35DEL},{0x7CE4L},{1UL},{65535UL},{0x56B5L}},{{0xC868L},{0UL},{0x5310L},{65532UL},{0x99A5L}}},{{{0x37E2L},{0x5D75L},{5UL},{0x7CE4L},{0x56B5L}},{{0x61C8L},{65532UL},{0UL},{1UL},{0UL}},{{0x56B5L},{0x5310L},{0x99A5L},{0x33F0L},{1UL}},{{8UL},{0UL},{4UL},{0UL},{0xE4C8L}},{{65535UL},{0xD563L},{65535UL},{65535UL},{1UL}},{{1UL},{0x5310L},{0UL},{1UL},{65535UL}},{{0x5D75L},{0x7B89L},{0x15B7L},{1UL},{0x8B73L}},{{0UL},{65535UL},{0x0420L},{65526UL},{0xB3E2L}}}};
static volatile union U0 g_1771[5][8] = {{{0x3ABEL},{0UL},{0xBC19L},{0x5359L},{0x5359L},{0xBC19L},{0UL},{0x3ABEL}},{{0x16E5L},{0xF669L},{65535UL},{3UL},{4UL},{65527UL},{65529UL},{0xBC19L}},{{0xDE0CL},{0x06A1L},{4UL},{0x02F4L},{3UL},{65527UL},{3UL},{0x02F4L}},{{0UL},{0xF669L},{0UL},{65535UL},{65527UL},{0xBC19L},{0x06A1L},{0x16E5L}},{{0x02F4L},{0UL},{3UL},{65529UL},{0xDE0CL},{0UL},{65527UL},{65527UL}}};
static int8_t * volatile g_1784 = &g_11;/* VOLATILE GLOBAL g_1784 */
static int8_t * volatile * volatile g_1783[1][1][2] = {{{&g_1784,&g_1784}}};
static volatile union U0 g_1804 = {0UL};/* VOLATILE GLOBAL g_1804 */
static union U0 *g_1877 = (void*)0;
static union U0 ** volatile g_1876 = &g_1877;/* VOLATILE GLOBAL g_1876 */
static volatile int32_t g_1917 = 0xD75892BFL;/* VOLATILE GLOBAL g_1917 */
static const int32_t **g_2035 = &g_1211[2][3][2];
static const int32_t ***g_2034[10][4][5] = {{{&g_2035,&g_2035,(void*)0,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,(void*)0,(void*)0},{&g_2035,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_2035,&g_2035,(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_2035,(void*)0,(void*)0},{&g_2035,&g_2035,&g_2035,(void*)0,&g_2035},{(void*)0,&g_2035,&g_2035,(void*)0,(void*)0},{(void*)0,(void*)0,&g_2035,&g_2035,&g_2035}},{{&g_2035,(void*)0,&g_2035,(void*)0,(void*)0},{&g_2035,&g_2035,(void*)0,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,(void*)0,(void*)0},{&g_2035,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_2035,&g_2035,(void*)0,(void*)0},{(void*)0,(void*)0,&g_2035,(void*)0,(void*)0},{&g_2035,&g_2035,&g_2035,(void*)0,&g_2035},{(void*)0,&g_2035,&g_2035,(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_2035,&g_2035,&g_2035},{&g_2035,(void*)0,&g_2035,(void*)0,(void*)0},{&g_2035,&g_2035,(void*)0,&g_2035,(void*)0},{&g_2035,&g_2035,&g_2035,&g_2035,(void*)0}},{{&g_2035,(void*)0,&g_2035,&g_2035,(void*)0},{(void*)0,&g_2035,&g_2035,(void*)0,&g_2035},{&g_2035,(void*)0,&g_2035,(void*)0,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,(void*)0},{&g_2035,(void*)0,&g_2035,&g_2035,(void*)0},{(void*)0,&g_2035,&g_2035,(void*)0,&g_2035},{&g_2035,(void*)0,&g_2035,(void*)0,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{(void*)0,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035}},{{&g_2035,&g_2035,&g_2035,&g_2035,&g_2035},{&g_2035,&g_2035,&g_2035,&g_2035,(void*)0},{&g_2035,(void*)0,&g_2035,&g_2035,(void*)0},{(void*)0,&g_2035,&g_2035,(void*)0,&g_2035}}};
static int64_t ***g_2051 = (void*)0;
static union U0 g_2099 = {65528UL};/* VOLATILE GLOBAL g_2099 */
static int8_t *g_2117 = (void*)0;
static int8_t **g_2116 = &g_2117;
static int8_t ***g_2115 = &g_2116;
static int8_t ****g_2114 = &g_2115;
static uint32_t g_2170 = 18446744073709551608UL;
static int16_t g_2188 = 0x3B75L;
static volatile uint8_t *g_2204 = &g_954[1];
static volatile uint8_t ** volatile g_2203 = &g_2204;/* VOLATILE GLOBAL g_2203 */
static union U0 g_2213 = {0x25DCL};/* VOLATILE GLOBAL g_2213 */
static union U0 g_2215 = {0xB264L};/* VOLATILE GLOBAL g_2215 */
static volatile union U0 g_2249 = {0xA8F9L};/* VOLATILE GLOBAL g_2249 */
static float g_2316 = 0x0.1p+1;
static int32_t g_2340 = 0x7F31D307L;
static const union U0 g_2369 = {0UL};/* VOLATILE GLOBAL g_2369 */
static int32_t * const *g_2399 = &g_547;
static int32_t * const ** const  volatile g_2398 = &g_2399;/* VOLATILE GLOBAL g_2398 */
static volatile union U0 ** volatile g_2401 = &g_1454;/* VOLATILE GLOBAL g_2401 */
static int32_t g_2405 = 0x90A75D76L;
static uint32_t g_2406 = 1UL;
static int32_t **g_2423 = &g_87[0][0][2];
static int32_t ***g_2422 = &g_2423;
static int32_t ****g_2421 = &g_2422;
static float * volatile g_2435 = &g_2316;/* VOLATILE GLOBAL g_2435 */


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static int32_t  func_15(int32_t  p_16, int32_t * p_17, int32_t * p_18);
static int32_t * func_19(const int32_t * p_20, uint16_t  p_21, int32_t * p_22, const uint32_t  p_23, int32_t * p_24);
static const int32_t * func_25(uint16_t  p_26, const int32_t * p_27, int32_t * p_28);
static uint16_t  func_29(int32_t * p_30);
static int32_t * func_31(int32_t * p_32, int16_t  p_33);
static int16_t  func_34(int32_t * p_35, int32_t * p_36);
static int32_t  func_37(int16_t  p_38, uint32_t  p_39, int64_t  p_40, int32_t * p_41);
static int64_t  func_46(uint32_t  p_47, int16_t  p_48, float  p_49, int32_t * p_50, float  p_51);
static int32_t * func_52(int8_t  p_53);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_12 g_10 g_11 g_73 g_9 g_77 g_83 g_78 g_3 g_101 g_93 g_85 g_211 g_87 g_207 g_235 g_229 g_258 g_227 g_228 g_212 g_301 g_307 g_262 g_334 g_338 g_379 g_420 g_492 g_495 g_420.f0 g_547 g_558 g_559 g_548 g_566 g_258.f0 g_379.f0 g_742 g_859 g_862 g_559.f0 g_923 g_934 g_926 g_954 g_545 g_925 g_980 g_1003 g_558.f0 g_863 g_1043 g_1049 g_1074 g_1003.f0 g_1044 g_1159 g_1298.f0 g_1300 g_206 g_1338 g_1339 g_1175 g_1433 g_1302.f0 g_1433.f0 g_1453 g_1461 g_1454 g_935 g_862.f0 g_1531 g_1535 g_1541 g_1433.f2 g_1532 g_1433.f1 g_1876 g_2099.f0 g_1429 g_1671 g_2035 g_2398 g_2401 g_2406 g_1784 g_2170 g_2213.f0 g_2435 g_2405
 * writes: g_12 g_73 g_9 g_78 g_83 g_3 g_10 g_87 g_93 g_85 g_11 g_203 g_206 g_207 g_228 g_211 g_235 g_262 g_212 g_307 g_334 g_338 g_221 g_493 g_548 g_547 g_847 g_863 g_925 g_926 g_954 g_1049 g_532 g_1159 g_1175 g_1283 g_1300 g_1339 g_934 g_1302.f0 g_1454 g_1211 g_1433.f0 g_1535 g_1433.f2 g_1003.f0 g_1298.f0 g_559.f1 g_1433.f1 g_1877 g_2099.f0 g_2399 g_2406 g_2421 g_2316 g_2405
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = &g_3;
    int32_t l_4 = (-5L);
    int32_t *l_5 = &l_4;
    int32_t *l_6 = &g_3;
    int32_t *l_7 = &l_4;
    int32_t *l_8[1];
    int32_t **l_511 = &l_8[0];
    int64_t l_2461 = 1L;
    int32_t *l_2462 = &g_2405;
    uint8_t ***l_2463 = &g_203;
    int8_t *l_2466[2];
    int8_t l_2471 = (-1L);
    uint32_t l_2476 = 0x513DD669L;
    uint16_t l_2479 = 0xBB0CL;
    int16_t l_2480 = 0L;
    int i;
    for (i = 0; i < 1; i++)
        l_8[i] = (void*)0;
    for (i = 0; i < 2; i++)
        l_2466[i] = &g_11;
    --g_12;
    (*l_2462) |= (func_15((*l_5), func_19(func_25(func_29(((*l_511) = func_31(&g_3, func_34(&g_3, &g_3)))), &l_4, (((*l_5) && g_229[2][1][3]) , g_547)), g_227, g_935, (*l_5), &l_4), g_935) , l_2461);
    (*l_2462) |= (((void*)0 == l_2463) | (safe_mod_func_int64_t_s_s((((*g_1784) = (*l_5)) && ((safe_sub_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((l_2471 & ((safe_sub_func_int8_t_s_s((-9L), (safe_div_func_int64_t_s_s((l_2476 | (*l_5)), ((((*l_2) > (((safe_mul_func_uint16_t_u_u(((*l_6) & (*l_6)), (*l_2))) == (*l_2)) < 0xAB405F7768B8579ALL)) | l_2479) & (-1L)))))) | 4L)), (*l_5))), 4L)) >= (*l_6))), 0x6FDB93B97466EF61LL)));
    return l_2480;
}


/* ------------------------------------------ */
/* 
 * reads : g_334 g_1876 g_83 g_2099.f0 g_338 g_1429 g_1298.f0 g_1671 g_863 g_2035 g_3 g_954 g_228 g_212 g_229 g_1339 g_262 g_1049 g_2398 g_1453 g_1454 g_2401 g_2406 g_548 g_1784 g_11 g_926 g_2170 g_2213.f0 g_2435 g_93
 * writes: g_1877 g_83 g_334 g_212 g_2099.f0 g_9 g_925 g_338 g_863 g_1211 g_3 g_2399 g_1454 g_2406 g_2421 g_2316 g_262 g_1049 g_548
 */
static int32_t  func_15(int32_t  p_16, int32_t * p_17, int32_t * p_18)
{ /* block id: 1040 */
    uint16_t l_2322 = 65533UL;
    union U0 * const l_2323[8] = {(void*)0,&g_1433,(void*)0,&g_1433,(void*)0,&g_1433,(void*)0,&g_1433};
    int32_t l_2356 = 0xAC23FC58L;
    int32_t l_2359 = 0xD58A6F27L;
    int32_t l_2361[5][4][5] = {{{0x8ABD9249L,0xE450C1D2L,0xE450C1D2L,0x8ABD9249L,(-3L)},{0x538FED00L,8L,0xB9C6B382L,1L,5L},{0L,(-2L),0L,0x65B360EFL,0x13B53BB8L},{0xBDD6291BL,0L,0L,1L,1L}},{{0L,(-3L),0x5E8B2C41L,0x8ABD9249L,0x5E8B2C41L},{0x1A9E36F5L,0x1A9E36F5L,0xD3C6725DL,0x538FED00L,0L},{0x42F59D8BL,0x65B360EFL,(-2L),0L,0x8ABD9249L},{8L,0x68B38836L,0L,0xBDD6291BL,0xF0ADDF9DL}},{{(-3L),0x65B360EFL,0x9CE4963AL,0L,0L},{(-1L),0x1A9E36F5L,0xBDD6291BL,0x1A9E36F5L,(-1L)},{0xB7989F28L,(-3L),0x4FDFC621L,0x42F59D8BL,0x9482F86CL},{1L,0L,1L,8L,0xBDD6291BL}},{{0x4FDFC621L,(-2L),(-5L),(-3L),0x9482F86CL},{0xF0ADDF9DL,8L,1L,(-1L),(-1L)},{0x9482F86CL,0xE450C1D2L,0x9482F86CL,0xB7989F28L,0L},{0xD7AFF630L,0xB9C6B382L,0xB9C6B382L,0xBDD6291BL,8L}},{{0x3C69C585L,0x5E8B2C41L,0L,0xB7989F28L,0x980C733FL},{5L,1L,0xB9C6B382L,8L,0x538FED00L},{0x9482F86CL,0x2F85193FL,0x0471CC74L,0x0471CC74L,0x2F85193FL},{0xBFFE0594L,0x68B38836L,0xBDD6291BL,0L,0x8CBB9BCFL}}};
    uint32_t l_2363[6][7][6] = {{{0xDDA7D469L,0xC03C0FEAL,1UL,1UL,0x8E25E921L,0x976974A8L},{0UL,0UL,7UL,0UL,0UL,0UL},{1UL,4294967295UL,8UL,0UL,4294967295UL,0x4F3A628DL},{7UL,4294967295UL,0UL,5UL,5UL,0UL},{0x213D0C7CL,0x213D0C7CL,1UL,0x1E018DE9L,0xF4849B7BL,0UL},{0UL,1UL,4294967295UL,0x9AD04334L,0xD89C7B4AL,1UL},{8UL,0UL,4294967295UL,4294967294UL,0x213D0C7CL,0UL}},{{4294967291UL,4294967294UL,1UL,0x9DF98057L,0UL,0UL},{0x9DF98057L,0UL,0UL,0x96A7537CL,0x0514DB47L,0x4F3A628DL},{2UL,0UL,8UL,4294967294UL,0UL,0UL},{0UL,4294967288UL,7UL,0x0D0173B7L,7UL,0x976974A8L},{0UL,0xE6F2A284L,1UL,0xDDA7D469L,4294967287UL,7UL},{0UL,0xB4F16BE1L,0x81FC5167L,0UL,0x96A7537CL,0x2A6C3B22L},{0UL,4294967295UL,0x1E018DE9L,0x0FB132EDL,0x0D0173B7L,0x62CD814CL}},{{0xEE2F40C1L,4294967295UL,0x8F2B9AB5L,4294967287UL,8UL,4294967287UL},{0x213D0C7CL,4294967287UL,0x525BE554L,1UL,0x1E9DD07AL,0x9C37AF40L},{9UL,0UL,0UL,0UL,0x4F3A628DL,1UL},{0x1E018DE9L,7UL,0x4F3A628DL,1UL,0x18C1422CL,0UL},{0x9FD2C490L,0xA0399461L,9UL,1UL,0UL,0UL},{0x435B6E95L,1UL,1UL,0xD9FE2B2FL,0xC03C0FEAL,1UL},{0x4F3A628DL,1UL,4294967287UL,4294967294UL,0xFEC59831L,0xA96F66ADL}},{{4294967295UL,0xDDA7D469L,0xEE2F40C1L,4294967287UL,0xA5283708L,0xF4849B7BL},{4294967288UL,4294967288UL,1UL,0x525BE554L,0x1E018DE9L,0x21763DC0L},{0xB0D87B0AL,0x1E9DD07AL,0UL,0xA0399461L,0x171D35FCL,0xA0399461L},{5UL,4294967295UL,5UL,5UL,5UL,4294967295UL},{1UL,0xA5283708L,0x22F1BDA6L,1UL,4294967293UL,0x9FD2C490L},{0x213D0C7CL,0x0514DB47L,0x9DF98057L,1UL,0x926699BEL,5UL},{1UL,0x435B6E95L,4294967292UL,5UL,0x575C4E6EL,1UL}},{{4294967294UL,4294967295UL,0x22F1BDA6L,0UL,0UL,0x81FC5167L},{5UL,4294967295UL,0x0514DB47L,0xC03C0FEAL,1UL,4294967287UL},{0x022B33F4L,9UL,4UL,0xB4F16BE1L,0x9C37AF40L,0x22F1BDA6L},{0UL,4294967288UL,0x95B54F28L,4294967295UL,0x8F2B9AB5L,4294967295UL},{0UL,0x213D0C7CL,0xEADA25E5L,0UL,4294967295UL,4294967295UL},{4294967287UL,1UL,0UL,0UL,0UL,0xD89C7B4AL},{0xAB2FBD10L,4294967295UL,0UL,0x435B6E95L,0UL,4294967288UL}},{{0x2A6C3B22L,0x96A7537CL,0UL,0x81FC5167L,0xB4F16BE1L,0UL},{4294967295UL,4294967295UL,1UL,0x393908CAL,0UL,4UL},{0x96E9BF4CL,0x9C37AF40L,4294967287UL,0xA0399461L,0xD89C7B4AL,0UL},{0x3FC3EFA6L,0xC03C0FEAL,4294967287UL,4294967294UL,0x9FD2C490L,4294967288UL},{0UL,4294967295UL,4294967290UL,1UL,0UL,0UL},{4294967293UL,0x9DF98057L,0x213D0C7CL,0xE6F2A284L,1UL,8UL},{0x18C1422CL,0xDDA7D469L,4294967293UL,0x1E9DD07AL,0UL,4294967287UL}}};
    const union U0 *l_2368 = &g_2369;
    int64_t **** const l_2384 = &g_2051;
    uint32_t ***l_2389 = &g_934[4][3];
    int32_t ****l_2418[4][3][2];
    const uint32_t l_2434[9] = {0xA9819A10L,0xA9819A10L,0xA9819A10L,0xA9819A10L,0xA9819A10L,0xA9819A10L,0xA9819A10L,0xA9819A10L,0xA9819A10L};
    uint8_t l_2441[2];
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
                l_2418[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 2; i++)
        l_2441[i] = 0xB4L;
    if ((l_2322 ^ g_334))
    { /* block id: 1041 */
lbl_2332:
        (*g_1876) = l_2323[6];
    }
    else
    { /* block id: 1043 */
        uint16_t l_2335 = 0x3268L;
        uint64_t * const l_2336 = (void*)0;
        int16_t *l_2354 = (void*)0;
        int32_t *l_2402 = &g_548;
        int32_t *l_2403[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int16_t l_2404 = (-5L);
        int16_t l_2415 = 3L;
        int32_t ****l_2419 = (void*)0;
        int32_t *****l_2420[4] = {&l_2419,&l_2419,&l_2419,&l_2419};
        int i;
        for (g_83 = (-1); (g_83 != 54); ++g_83)
        { /* block id: 1046 */
            uint32_t ***l_2355 = &g_934[1][1];
            int8_t l_2362 = 0xF3L;
            int32_t *l_2371 = &g_3;
            const uint8_t *l_2396 = &g_93;
            const uint8_t **l_2395 = &l_2396;
            const uint8_t ***l_2394 = &l_2395;
            const uint8_t ****l_2393 = &l_2394;
            for (g_334 = 0; (g_334 < 22); g_334 = safe_add_func_int8_t_s_s(g_334, 8))
            { /* block id: 1049 */
                uint8_t l_2344 = 253UL;
                const float l_2370 = 0x0.8AC6A7p+45;
                int32_t l_2385 = (-1L);
                int32_t * const *l_2397 = (void*)0;
                for (g_212 = 0; (g_212 != (-2)); g_212--)
                { /* block id: 1052 */
                    for (g_2099.f0 = (-25); (g_2099.f0 < 39); g_2099.f0 = safe_add_func_uint8_t_u_u(g_2099.f0, 3))
                    { /* block id: 1055 */
                        if (g_2099.f0)
                            goto lbl_2332;
                        return (*p_18);
                    }
                }
                for (g_9 = 29; (g_9 > 15); g_9 = safe_sub_func_int64_t_s_s(g_9, 7))
                { /* block id: 1062 */
                    uint16_t l_2351 = 65532UL;
                    if (l_2335)
                    { /* block id: 1063 */
                        int32_t *l_2339[1][7][6] = {{{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340},{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340},{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340},{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340},{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340},{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340},{&g_2340,&g_2340,&g_2340,&g_2340,&g_2340,&g_2340}}};
                        uint32_t *l_2341 = &g_925;
                        uint16_t *l_2357 = (void*)0;
                        uint16_t *l_2358 = &l_2351;
                        uint8_t *l_2360 = &g_863;
                        int i, j, k;
                        (*p_18) ^= (((((void*)0 == l_2336) >= ((((0xE.638A31p-12 >= l_2322) , (safe_sub_func_int64_t_s_s((g_1429[2] == l_2339[0][6][4]), p_16))) < p_16) > ((*l_2341) = 0x539BA07FL))) & p_16) < (*p_17));
                        l_2362 ^= ((safe_mul_func_uint8_t_u_u((l_2361[0][0][2] &= ((*l_2360) ^= ((l_2359 |= ((*l_2358) = (l_2344 && (((l_2356 &= ((p_16 & (safe_add_func_int64_t_s_s((safe_add_func_uint16_t_u_u(g_1298.f0, ((safe_add_func_uint8_t_u_u((l_2351 && ((((safe_rshift_func_int16_t_s_s(p_16, 1)) == (p_16 < ((void*)0 != l_2354))) , l_2355) == (void*)0)), p_16)) > 0x49AC581D854D2E0CLL))), g_1671))) <= 4294967294UL)) && 4294967289UL) , l_2335)))) | p_16))), 255UL)) >= l_2335);
                        (*g_2035) = func_52(p_16);
                        --l_2363[3][5][2];
                    }
                    else
                    { /* block id: 1074 */
                        const union U0 *l_2366 = &g_2215;
                        const union U0 **l_2367[5][5][5] = {{{&l_2366,&l_2366,(void*)0,&l_2366,(void*)0},{&l_2366,&l_2366,&l_2366,&l_2366,&l_2366},{(void*)0,&l_2366,(void*)0,&l_2366,&l_2366},{(void*)0,&l_2366,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2366,&l_2366,&l_2366,&l_2366}},{{&l_2366,(void*)0,&l_2366,(void*)0,&l_2366},{&l_2366,&l_2366,&l_2366,&l_2366,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2366,(void*)0},{&l_2366,&l_2366,(void*)0,&l_2366,(void*)0},{&l_2366,&l_2366,&l_2366,&l_2366,&l_2366}},{{(void*)0,&l_2366,(void*)0,&l_2366,&l_2366},{(void*)0,&l_2366,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2366,&l_2366,&l_2366,&l_2366},{&l_2366,(void*)0,&l_2366,(void*)0,&l_2366},{&l_2366,&l_2366,&l_2366,&l_2366,(void*)0}},{{(void*)0,(void*)0,(void*)0,&l_2366,(void*)0},{&l_2366,&l_2366,(void*)0,&l_2366,(void*)0},{&l_2366,&l_2366,&l_2366,&l_2366,&l_2366},{(void*)0,&l_2366,(void*)0,&l_2366,&l_2366},{(void*)0,&l_2366,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_2366,&l_2366,&l_2366,&l_2366},{&l_2366,(void*)0,&l_2366,(void*)0,&l_2366},{&l_2366,&l_2366,&l_2366,&l_2366,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2366,(void*)0},{&l_2366,&l_2366,(void*)0,&l_2366,(void*)0}}};
                        int i, j, k;
                        l_2368 = l_2366;
                        if (l_2335)
                            continue;
                        (*p_17) ^= ((*p_18) &= 0xC2FD106FL);
                    }
                }
                for (g_3 = 0; (g_3 <= 3); g_3 += 1)
                { /* block id: 1083 */
                    int32_t l_2383 = (-6L);
                    uint32_t ***l_2390 = (void*)0;
                    int i;
                    (*g_2035) = l_2371;
                    (*p_18) |= ((*p_17) = ((safe_rshift_func_int16_t_s_s((((safe_mod_func_int8_t_s_s((safe_add_func_int16_t_s_s((l_2385 = ((g_954[g_3] , l_2371) != ((((&g_1339 != ((~(((safe_sub_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(p_16, l_2383)), 2UL)) < ((void*)0 != l_2384)) != ((*g_228) , l_2344))) , &g_1339)) & l_2363[3][5][2]) & p_16) , p_18))), 1L)), 0x59L)) < 0xF6L) ^ l_2361[2][0][2]), (*g_1339))) == p_16));
                    if ((p_16 && ((safe_mul_func_uint8_t_u_u((((l_2385 > (!(l_2389 == l_2390))) == p_16) , ((((((((0xB49F2DE34E35C125LL > 0x5BA1AFD96858601BLL) >= (p_16 >= ((void*)0 == l_2393))) == l_2383) & 1L) , 0UL) != 0x51E7L) != 253UL) <= (-1L))), l_2363[3][5][2])) & 0xE8207C40C1327159LL)))
                    { /* block id: 1088 */
                        int8_t l_2400 = 0x7DL;
                        (*g_2035) = p_17;
                        (*g_2398) = l_2397;
                        if (l_2400)
                            break;
                        (*g_2401) = (*g_1453);
                    }
                    else
                    { /* block id: 1093 */
                        (*p_18) |= 0x6E66161CL;
                    }
                }
            }
            if ((*p_18))
                continue;
        }
        g_2406--;
lbl_2460:
        (*g_2435) = ((safe_rshift_func_int8_t_s_u((((0x389586FDC5E22E70LL && (((safe_rshift_func_uint16_t_u_u(((void*)0 == l_2336), 1)) && (safe_mod_func_int32_t_s_s((l_2415 , (*l_2402)), (p_16 && (((safe_add_func_float_f_f(((l_2418[1][2][0] = l_2418[0][2][0]) != (g_2421 = l_2419)), ((!((safe_add_func_float_f_f(((safe_sub_func_uint64_t_u_u((((safe_mul_func_int16_t_s_s(((safe_add_func_int16_t_s_s((((+((void*)0 == &g_1454)) < p_16) , 0xA4F3L), p_16)) <= (*g_1784)), g_926[4])) , &g_1159) != (void*)0), p_16)) , l_2434[6]), g_2170)) >= (-0x1.Bp+1))) == l_2434[6]))) , 3L) > (*g_1339)))))) > g_2213.f0)) | 0UL) == 0xCA4A73BDA03D9497LL), p_16)) , p_16);
        if ((1UL < (+(7L & (+p_16)))))
        { /* block id: 1104 */
            int16_t l_2438 = 0x14C3L;
            int32_t l_2439 = (-2L);
            int32_t l_2440[5] = {1L,1L,1L,1L,1L};
            int16_t *l_2459 = &l_2404;
            int i;
            ++l_2441[1];
            (*l_2402) = (((*g_1339) |= 0xD278L) != (safe_rshift_func_uint8_t_u_u(((((*l_2459) ^= ((safe_mul_func_uint8_t_u_u((((~(((*g_1453) != g_1454) > (p_16 > p_16))) , ((5L ^ (p_16 == ((safe_sub_func_uint64_t_u_u(p_16, ((safe_sub_func_int64_t_s_s(((((((safe_div_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s((safe_div_func_uint32_t_u_u(0x72ECE0B8L, (*p_18))), 3)) & l_2440[2]), p_16)) ^ p_16) <= 0x58L) <= p_16) == 0xA35CL) <= 0x124CL), 0x8B309F70E0C279BALL)) == 3UL))) != l_2439))) < 0x6FL)) > p_16), 1L)) , g_93)) == g_2170) && p_16), 3)));
        }
        else
        { /* block id: 1109 */
            (*p_18) &= (*p_17);
            if (g_262)
                goto lbl_2460;
        }
    }
    return (*p_18);
}


/* ------------------------------------------ */
/* 
 * reads : g_1433.f2 g_1043 g_1044 g_77 g_78 g_338 g_1302.f0 g_1531 g_1532 g_1433.f1
 * writes: g_338 g_1433.f2 g_78 g_12 g_83 g_1003.f0 g_1298.f0 g_559.f1 g_87 g_1433.f1
 */
static int32_t * func_19(const int32_t * p_20, uint16_t  p_21, int32_t * p_22, const uint32_t  p_23, int32_t * p_24)
{ /* block id: 664 */
    uint16_t l_1542 = 0xEA5CL;
    int32_t l_1551[8][5] = {{0x39F64EF0L,0x2900C032L,0xE628F573L,0L,0xDA04DFC0L},{0x9C84A72BL,(-2L),0x00E7DCB4L,1L,0x39F64EF0L},{4L,0xDA04DFC0L,0xE628F573L,0xDA04DFC0L,4L},{0xEAAFB639L,0L,1L,0xDA04DFC0L,1L},{1L,0xEAAFB639L,0xD5C25432L,1L,0x5DFC2C53L},{0xE628F573L,1L,1L,0L,1L},{1L,1L,0x2900C032L,0xD5C25432L,4L},{1L,0x2525CDD3L,4L,0x00E7DCB4L,0x39F64EF0L}};
    uint32_t *** const *l_1568 = (void*)0;
    const int8_t *l_1665 = &g_11;
    uint32_t ***l_1694 = &g_934[2][3];
    uint8_t l_1786 = 252UL;
    uint32_t l_1801 = 0x4B16A169L;
    uint32_t l_1827 = 0x6CA2E0A8L;
    union U0 *l_1846 = &g_1302[5];
    union U0 **l_1845 = &l_1846;
    int16_t l_1913 = (-1L);
    int8_t *l_1936 = &g_11;
    int8_t **l_1935 = &l_1936;
    int8_t ***l_1934 = &l_1935;
    int8_t ****l_1933[1][5][5] = {{{(void*)0,&l_1934,&l_1934,&l_1934,&l_1934},{&l_1934,&l_1934,&l_1934,&l_1934,&l_1934},{&l_1934,&l_1934,&l_1934,&l_1934,&l_1934},{&l_1934,&l_1934,&l_1934,&l_1934,(void*)0},{&l_1934,(void*)0,&l_1934,&l_1934,(void*)0}}};
    int64_t ***l_2052[3][1][1];
    const int64_t **l_2066 = &g_228;
    int8_t * const **l_2112 = (void*)0;
    int8_t * const ***l_2111 = &l_2112;
    uint8_t *****l_2177 = &g_493[6];
    float *l_2263[2];
    int64_t l_2285 = (-1L);
    uint32_t l_2291[10] = {0xB12BF80AL,0xD421DEF1L,18446744073709551615UL,0xD421DEF1L,0xB12BF80AL,0xB12BF80AL,0xD421DEF1L,18446744073709551615UL,0xD421DEF1L,0xB12BF80AL};
    int32_t l_2314 = 9L;
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
                l_2052[i][j][k] = &g_1238;
        }
    }
    for (i = 0; i < 2; i++)
        l_2263[i] = &g_221;
    if (((*p_22) = l_1542))
    { /* block id: 666 */
        uint32_t l_1543 = 0x9015789EL;
        l_1543--;
        return p_24;
    }
    else
    { /* block id: 669 */
        uint32_t l_1549 = 0x22C6563DL;
        int32_t l_1553 = 2L;
        float l_1556[7] = {0x0.E8AAD3p+82,0x0.E8AAD3p+82,0x0.E8AAD3p+82,0x0.E8AAD3p+82,0x0.E8AAD3p+82,0x0.E8AAD3p+82,0x0.E8AAD3p+82};
        uint32_t **l_1567 = (void*)0;
        int32_t l_1569 = 0x05118643L;
        int32_t *l_1570 = &l_1569;
        int32_t *l_1571 = &g_1175;
        int32_t *l_1572 = &l_1553;
        int32_t *l_1573[4][8][8] = {{{&l_1551[4][2],(void*)0,&l_1551[1][2],&l_1553,(void*)0,&l_1569,&l_1553,&l_1569},{(void*)0,(void*)0,(void*)0,&l_1569,(void*)0,&l_1569,&g_1175,&l_1551[4][2]},{&l_1553,(void*)0,&l_1553,&l_1551[4][2],(void*)0,(void*)0,&l_1553,&l_1553},{&g_548,&g_1175,&g_1175,&g_3,(void*)0,(void*)0,&l_1569,&l_1551[4][2]},{&g_548,&l_1553,&l_1553,&l_1551[4][2],&l_1551[7][2],&l_1551[4][2],(void*)0,&l_1553},{&l_1551[4][2],&g_1175,&g_548,&l_1551[4][2],(void*)0,(void*)0,&g_548,(void*)0},{&g_1175,&l_1553,(void*)0,&l_1551[3][3],&l_1569,&l_1551[7][4],&g_1175,&g_548},{(void*)0,&g_548,(void*)0,(void*)0,&l_1551[1][2],&l_1569,&g_548,&g_1175}},{{&g_1175,&g_548,&l_1551[1][2],(void*)0,&l_1569,&l_1551[4][2],&g_548,(void*)0},{&g_1175,&l_1551[7][2],&l_1553,&g_1175,&g_548,&l_1553,&g_548,&g_1175},{&g_3,(void*)0,&g_3,&l_1551[1][2],&l_1553,(void*)0,&l_1551[7][2],&g_1175},{&l_1553,&g_1175,&l_1569,&g_548,&l_1551[7][3],&l_1551[7][4],&l_1553,&g_548},{&l_1553,&l_1553,&g_1175,&g_1175,&l_1553,&l_1553,&g_548,(void*)0},{&g_3,&l_1551[4][2],&l_1553,(void*)0,&g_548,&l_1551[4][2],&l_1569,&l_1551[1][2]},{&g_1175,&l_1553,&l_1553,(void*)0,&l_1569,&g_548,&l_1553,&g_548},{&g_1175,&l_1569,(void*)0,&g_1175,&l_1551[1][2],&g_548,&g_1175,&l_1551[4][2]}},{{(void*)0,(void*)0,&g_548,&g_548,&l_1569,&l_1551[7][3],&g_1175,(void*)0},{&g_1175,&l_1553,&g_548,(void*)0,(void*)0,(void*)0,&l_1551[7][2],&g_1175},{&l_1551[4][2],&l_1551[1][2],&l_1551[4][2],(void*)0,&l_1551[7][2],&g_3,&l_1569,(void*)0},{&g_548,&l_1551[2][3],&l_1551[4][2],&l_1569,(void*)0,&l_1551[1][2],(void*)0,&g_1175},{&g_548,(void*)0,&l_1551[7][2],&l_1551[1][2],&l_1551[1][2],(void*)0,&l_1553,&g_3},{(void*)0,(void*)0,&l_1569,&l_1551[3][3],&g_1175,&g_1175,&l_1551[3][3],&l_1569},{&l_1551[4][2],&l_1551[4][2],&g_1175,&g_548,&l_1553,&g_548,&g_548,&l_1551[4][2]},{(void*)0,(void*)0,&g_1175,&l_1553,&l_1551[7][2],&l_1551[4][2],&g_548,&l_1551[4][2]}},{{(void*)0,&g_1175,(void*)0,&g_548,&l_1553,&g_1175,&g_548,&l_1569},{&g_548,&l_1553,(void*)0,&l_1551[3][3],&l_1569,&l_1553,(void*)0,&g_3},{(void*)0,&g_1175,(void*)0,&l_1551[1][2],(void*)0,&l_1569,&g_1175,&l_1551[4][2]},{&l_1551[1][2],(void*)0,&g_3,(void*)0,(void*)0,&l_1551[3][3],&g_1175,(void*)0},{(void*)0,&l_1551[4][2],(void*)0,&l_1569,(void*)0,&l_1569,&l_1551[7][2],(void*)0},{&g_1175,&l_1569,&l_1551[7][3],&g_548,&g_1175,(void*)0,&l_1551[7][2],&l_1551[2][3]},{&l_1569,&l_1569,&g_1175,&g_548,&g_548,&g_548,&g_1175,&l_1569},{&l_1569,&g_3,&l_1551[4][2],&g_548,&l_1551[3][3],&l_1569,(void*)0,&g_1175}}};
        uint32_t l_1574 = 0x9809ACF9L;
        const uint8_t * const l_1604 = &g_73;
        const uint8_t * const *l_1603 = &l_1604;
        uint8_t *l_1614 = &g_73;
        uint8_t **l_1613 = &l_1614;
        const int16_t l_1655 = 0x1C4CL;
        float l_1656 = (-0x1.3p+1);
        uint32_t l_1672 = 1UL;
        int32_t *l_1731 = (void*)0;
        uint8_t *****l_1732 = &g_493[2];
        int16_t l_1733 = 0x7712L;
        uint16_t *l_1772 = &g_85;
        uint16_t l_1812[1];
        uint32_t l_1830[10][1][2] = {{{1UL,0x33852829L}},{{0xCC1674BFL,3UL}},{{0x6DC8D03CL,0xCC1674BFL}},{{3UL,0x33852829L}},{{3UL,0xCC1674BFL}},{{0x6DC8D03CL,3UL}},{{0xCC1674BFL,0x33852829L}},{{1UL,1UL}},{{0x6DC8D03CL,1UL}},{{1UL,0x33852829L}}};
        uint32_t l_1901 = 1UL;
        int32_t l_1940 = 5L;
        int8_t l_1986 = (-3L);
        int64_t **l_2012[9][3] = {{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]},{(void*)0,(void*)0,&g_532[3]}};
        const uint32_t ****l_2020 = (void*)0;
        int32_t l_2038 = 0xAA990206L;
        union U0 *l_2189[2][7][2] = {{{&g_1433,&g_1719[1][7][1]},{&g_1719[0][3][4],&g_1302[3]},{&g_1719[0][3][4],&g_1719[1][7][1]},{&g_1433,&g_1302[2]},{&g_1719[1][7][1],&g_1302[2]},{&g_2099,&g_1719[0][4][3]},{&g_1433,&g_1433}},{{&g_1433,&g_1433},{&g_2099,&g_1433},{&g_1433,&g_1433},{&g_1433,&g_1719[0][4][3]},{&g_2099,&g_1302[2]},{&g_1719[1][7][1],&g_1302[2]},{&g_1433,&g_1719[1][7][1]}}};
        uint64_t *l_2198 = (void*)0;
        int8_t **** const *l_2262 = (void*)0;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1812[i] = 65534UL;
        for (g_1433.f2 = 0; (g_1433.f2 <= 4); g_1433.f2 += 1)
        { /* block id: 672 */
            int64_t l_1548[9] = {0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL,0x321021BA541E526DLL};
            int32_t l_1550 = 0xBF4F93E8L;
            int i;
            for (p_21 = 1; (p_21 <= 4); p_21 += 1)
            { /* block id: 675 */
                uint64_t l_1546 = 6UL;
                (**g_1044) = (***g_1043);
                for (g_12 = 0; (g_12 <= 4); g_12 += 1)
                { /* block id: 679 */
                    uint16_t l_1547 = 0xB516L;
                    if (l_1546)
                        break;
                    l_1549 |= ((l_1547 , l_1546) && l_1548[3]);
                    if ((*p_24))
                        break;
                }
                l_1550 = (l_1549 >= p_23);
                for (g_83 = 0; (g_83 <= 4); g_83 += 1)
                { /* block id: 687 */
                    uint32_t l_1557 = 0xD4B828B7L;
                    for (g_1003.f0 = 0; g_1003.f0 < 4; g_1003.f0 += 1)
                    {
                        for (g_1298.f0 = 0; g_1298.f0 < 2; g_1298.f0 += 1)
                        {
                            for (g_559.f1 = 0; g_559.f1 < 10; g_559.f1 += 1)
                            {
                                g_87[g_1003.f0][g_1298.f0][g_559.f1] = &g_1175;
                            }
                        }
                    }
                    if ((*p_22))
                    { /* block id: 689 */
                        int64_t l_1552 = (-1L);
                        int32_t *l_1554 = (void*)0;
                        int32_t *l_1555[9][8] = {{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175},{&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175,&g_1175}};
                        int i, j;
                        l_1557--;
                    }
                    else
                    { /* block id: 691 */
                        int8_t l_1564[3][6] = {{0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L}};
                        uint8_t *** const *l_1566 = &g_494;
                        uint8_t *** const **l_1565 = &l_1566;
                        int i, j;
                        l_1569 &= (p_23 & (((safe_add_func_uint64_t_u_u((((((l_1564[2][2] == (0xC8930B8DDD541480LL && g_1302[2].f0)) >= ((l_1565 == ((l_1553 != l_1564[1][0]) , &g_493[6])) != (p_23 < l_1550))) || l_1546) , l_1567) != l_1567), 0xCA379A742F76210CLL)) , (*g_1531)) == l_1568));
                    }
                }
            }
        }
        ++l_1574;
        for (g_1433.f1 = 0; (g_1433.f1 <= 4); g_1433.f1 += 1)
        { /* block id: 700 */
            const uint32_t *l_1627 = &g_1433.f2;
            int32_t l_1628 = 1L;
            int8_t *l_1687 = &g_11;
            int8_t **l_1686 = &l_1687;
            uint32_t ****l_1695 = &l_1694;
            uint32_t ***l_1697 = (void*)0;
            uint32_t ****l_1696 = &l_1697;
            uint32_t ***l_1699 = &l_1567;
            uint32_t ****l_1698 = &l_1699;
            uint32_t *l_1711 = &g_1300;
            uint16_t *l_1712 = &g_1302[2].f0;
            uint8_t l_1736 = 0x5CL;
            int8_t ***l_1823 = &l_1686;
            int32_t l_1829 = (-8L);
            int8_t l_1916[4][6][1] = {{{0xC3L},{0xEFL},{0xC3L},{0xEFL},{0xC3L},{0xEFL}},{{0xC3L},{0xEFL},{0xC3L},{0xEFL},{0xC3L},{0xEFL}},{{0xC3L},{0xEFL},{0xC3L},{0xEFL},{0xC3L},{0xEFL}},{{0xC3L},{0xEFL},{0xC3L},{0xEFL},{0xC3L},{0xEFL}}};
            int32_t l_1918 = (-1L);
            int32_t l_1920 = 0x3460DFBDL;
            int32_t l_1921 = 0xA44D0514L;
            int32_t l_1922 = (-1L);
            int32_t l_1923 = 0L;
            int32_t l_1925 = 3L;
            int32_t l_1926[7][6][2] = {{{1L,0x4EF31703L},{0xDD70D694L,0L},{8L,8L},{0x4EF31703L,(-1L)},{0x43490B02L,1L},{0L,0xE3207427L}},{{0xE1FE7135L,0L},{0xDFE21E6FL,1L},{0xDFE21E6FL,0L},{0xE1FE7135L,0xE3207427L},{0L,1L},{0x43490B02L,(-1L)}},{{0x4EF31703L,8L},{8L,0L},{0xDD70D694L,0x4EF31703L},{1L,0L},{(-1L),0L},{1L,0x4EF31703L}},{{0xDD70D694L,0L},{8L,8L},{0x4EF31703L,(-1L)},{0x43490B02L,1L},{0L,0xE3207427L},{0xE1FE7135L,0L}},{{0xDFE21E6FL,1L},{0xDFE21E6FL,0L},{0xE1FE7135L,0xE3207427L},{0L,1L},{0x43490B02L,(-1L)},{0x4EF31703L,8L}},{{8L,0L},{0xDD70D694L,0x4EF31703L},{1L,0L},{(-1L),0L},{1L,0x4EF31703L},{0xDD70D694L,0L}},{{8L,8L},{0x4EF31703L,(-1L)},{0x43490B02L,1L},{0L,0xE3207427L},{0xE1FE7135L,0L},{0xDFE21E6FL,1L}}};
            int64_t l_1944[1][6] = {{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}};
            uint8_t **l_1966 = &l_1614;
            int8_t l_1981 = 0xB0L;
            uint64_t l_2017 = 0x554EDF78782F0CEBLL;
            float *l_2121 = &l_1656;
            float ** const l_2120 = &l_2121;
            uint32_t *****l_2163 = (void*)0;
            uint32_t l_2165 = 4294967287UL;
            union U0 *l_2212 = &g_2213;
            int64_t **l_2216 = &g_532[0];
            uint64_t l_2315 = 0xC2202B94DD07668ELL;
            int i, j, k;
        }
        (*p_22) = (*p_22);
    }
    l_2314 &= l_2291[1];
    return p_24;
}


/* ------------------------------------------ */
/* 
 * reads : g_558 g_559 g_262 g_228 g_212 g_229 g_547 g_548 g_566 g_77 g_78 g_11 g_83 g_93 g_9 g_258.f0 g_207 g_12 g_3 g_85 g_10 g_379.f0 g_742 g_334 g_338 g_101 g_87 g_420.f0 g_73 g_859 g_862 g_235 g_559.f0 g_923 g_934 g_926 g_954 g_545 g_925 g_980 g_1003 g_258 g_558.f0 g_863 g_1043 g_1049 g_1074 g_227 g_301 g_1003.f0 g_1044 g_1159 g_307 g_1298.f0 g_1300 g_206 g_1338 g_1339 g_495 g_1175 g_1433 g_1302.f0 g_1433.f0 g_1453 g_1461 g_1454 g_379 g_935 g_862.f0 g_1531 g_1535 g_1541
 * writes: g_548 g_11 g_547 g_78 g_9 g_221 g_212 g_206 g_3 g_85 g_73 g_12 g_334 g_83 g_262 g_847 g_863 g_338 g_925 g_926 g_954 g_10 g_1049 g_87 g_532 g_1159 g_1175 g_1283 g_1300 g_1339 g_934 g_1302.f0 g_1454 g_1211 g_1433.f0 g_1535
 */
static const int32_t * func_25(uint16_t  p_26, const int32_t * p_27, int32_t * p_28)
{ /* block id: 240 */
    uint64_t l_549[3];
    uint8_t * const *l_563 = (void*)0;
    uint8_t * const **l_562[5][3] = {{&l_563,&l_563,&l_563},{&l_563,&l_563,&l_563},{&l_563,&l_563,&l_563},{&l_563,&l_563,&l_563},{&l_563,&l_563,&l_563}};
    uint8_t * const ***l_561 = &l_562[2][2];
    uint8_t * const ****l_560 = &l_561;
    int32_t l_592 = 0L;
    const int64_t *l_612 = (void*)0;
    int32_t l_622[3][2] = {{0x0F0A4358L,9L},{0x0F0A4358L,0x0F0A4358L},{9L,0x0F0A4358L}};
    uint32_t l_623 = 0x6CF35489L;
    int32_t ** const l_630 = &g_87[0][0][2];
    int16_t *l_664 = &g_9;
    const int32_t l_792 = 0xC5404649L;
    uint64_t l_864 = 0xA2B4347D6CC8C3BALL;
    uint64_t l_1063 = 1UL;
    uint32_t *l_1121 = &g_925;
    int32_t l_1140 = 0x0769220EL;
    int32_t *l_1156 = &l_622[0][1];
    int32_t *l_1157[9] = {&l_622[0][1],&l_592,&l_592,&l_622[0][1],&l_592,&l_592,&l_622[0][1],&l_592,&l_592};
    int32_t l_1158[5];
    uint32_t l_1172 = 0xB34511E6L;
    int64_t *l_1173 = (void*)0;
    int64_t *l_1174[7] = {&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,&g_212};
    uint16_t l_1176 = 0x2291L;
    float l_1181 = 0xC.0BEA6Ap+5;
    int16_t l_1206 = 0x8507L;
    float l_1215 = 0x1.0p+1;
    int8_t l_1247 = (-7L);
    uint8_t l_1299 = 0xEDL;
    uint32_t ****l_1379 = (void*)0;
    uint32_t **** const *l_1378 = &l_1379;
    int64_t l_1513 = 1L;
    uint32_t l_1515[6][3] = {{0x8A144CA1L,0x8A144CA1L,6UL},{0x7413FCABL,0x61E3AE53L,0x23D9E2C2L},{6UL,0x8A144CA1L,6UL},{6UL,0x7413FCABL,0x8A144CA1L},{0x7413FCABL,6UL,6UL},{18446744073709551614UL,0x61E3AE53L,6UL}};
    int32_t l_1519 = 0x2501B637L;
    int i, j;
    for (i = 0; i < 3; i++)
        l_549[i] = 1UL;
    for (i = 0; i < 5; i++)
        l_1158[i] = 0x92D2164AL;
lbl_798:
    if (((*p_28) = (p_26 <= l_549[2])))
    { /* block id: 242 */
        uint8_t * const ****l_564 = &l_561;
        int32_t **l_565 = (void*)0;
        volatile int32_t **l_567 = &g_78;
        int32_t *l_568 = (void*)0;
        int32_t *l_570 = &g_548;
        (*g_547) ^= (safe_mod_func_uint64_t_u_u((safe_add_func_int32_t_s_s((8UL <= ((p_26 | (g_11 = (safe_lshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u((g_558 , (g_559 , (((l_564 = l_560) == (void*)0) <= (1UL < (p_26 , 18446744073709551615UL))))), (-1L))), 9)))) < 1L)), g_262)), (*g_228)));
        (*g_566) = p_28;
        (*l_567) = (*g_77);
        l_570 = l_568;
    }
    else
    { /* block id: 249 */
        int32_t **l_588 = &g_87[0][0][2];
        int32_t ***l_587 = &l_588;
        uint8_t ***l_600[2];
        int64_t *l_613 = &g_212;
        int32_t *l_616 = &g_3;
        int32_t *l_617 = &g_548;
        int32_t *l_618 = &l_592;
        int32_t *l_619 = &g_3;
        int32_t *l_620[9][7][4] = {{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}}};
        float l_621 = (-0x1.Bp-1);
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_600[i] = &g_203;
        for (g_11 = 7; (g_11 != (-11)); g_11 = safe_sub_func_uint8_t_u_u(g_11, 1))
        { /* block id: 252 */
            uint8_t * const **l_584 = &l_563;
            int32_t l_591[8] = {0x84D61302L,0x84D61302L,0x84D61302L,0x84D61302L,0x84D61302L,0x84D61302L,0x84D61302L,0x84D61302L};
            volatile int32_t *l_615 = &g_10;
            int i;
            if (((safe_div_func_uint8_t_u_u((((((safe_rshift_func_uint8_t_u_s((l_592 = (((safe_mul_func_uint16_t_u_u((((((~(safe_add_func_uint8_t_u_u((p_26 , l_549[0]), (safe_rshift_func_uint8_t_u_s(((((l_584 = (void*)0) == ((safe_mul_func_int8_t_s_s(p_26, ((((p_26 , &g_569[1][5]) == l_587) && l_549[0]) , ((safe_rshift_func_uint8_t_u_u(p_26, 0)) , g_83)))) , (void*)0)) | 0xD855L) , p_26), 3))))) && l_591[3]) != 0x1D635AB49233D27DLL) ^ l_549[2]) >= (*p_27)), p_26)) || (*p_28)) , p_26)), 7)) >= 18446744073709551613UL) != p_26) >= 0xB573L) <= l_591[1]), g_93)) & 0x01321F9047A34AC2LL))
            { /* block id: 255 */
                float *l_593 = &g_221;
                for (g_9 = 2; (g_9 <= 9); g_9 += 1)
                { /* block id: 258 */
                    (*p_28) = (**g_566);
                }
                (*p_28) = 4L;
                (*l_593) = 0x2.BBEA51p+50;
            }
            else
            { /* block id: 263 */
                uint8_t ***l_601 = &g_203;
                uint8_t *l_611[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                volatile int32_t **l_614[9] = {&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78};
                int i;
                (*g_547) = ((safe_mod_func_int64_t_s_s((-10L), ((safe_lshift_func_uint16_t_u_s(l_549[2], 14)) && ((l_600[1] != l_601) > ((p_26 > (~((*l_613) = (p_26 , ((l_592 = (safe_mod_func_uint16_t_u_u(p_26, p_26))) , ((safe_lshift_func_uint16_t_u_u(((safe_div_func_uint16_t_u_u((((safe_mod_func_int16_t_s_s(((((void*)0 == l_611[2]) & (*g_228)) & p_26), p_26)) , l_612) == l_613), (-7L))) == g_11), p_26)) | 1UL)))))) <= (*p_27)))))) | 0x55L);
                l_615 = (*g_77);
            }
            return p_27;
        }
        l_623++;
    }
    if (((*p_28) = (-9L)))
    { /* block id: 274 */
        uint8_t l_654 = 253UL;
        int32_t l_668 = 0x2F99CA20L;
        int32_t l_685 = 0x936D1508L;
        int8_t *l_728 = &g_11;
        uint8_t *l_730 = &g_73;
        uint8_t **l_729 = &l_730;
        uint32_t *l_741 = &l_623;
        uint32_t **l_740 = &l_741;
        uint8_t *****l_789[10] = {&g_493[9],&g_493[6],&g_493[3],&g_493[3],&g_493[6],&g_493[9],&g_493[6],&g_493[3],&g_493[3],&g_493[6]};
        int64_t l_813[10][7] = {{0xF480FE4BEA779115LL,(-1L),0x470CB926A698E7B6LL,(-3L),(-5L),0x58352F4EE5217BA5LL,0xF95D5A1DC3DD3550LL},{0x58352F4EE5217BA5LL,0xF92F940387D9F35ELL,0xF480FE4BEA779115LL,0x6C0083C60E6EA020LL,1L,0xF95D5A1DC3DD3550LL,1L},{(-5L),(-1L),(-1L),(-5L),0L,(-3L),0xDC795F66CE260CABLL},{(-5L),0xF95D5A1DC3DD3550LL,0x504CA6F6CE57C863LL,0xF92F940387D9F35ELL,8L,0x2098D2E2ED89F7A8LL,0x470CB926A698E7B6LL},{0x58352F4EE5217BA5LL,0L,0xDC795F66CE260CABLL,0x9C895EC4CFA02319LL,0xF480FE4BEA779115LL,0x9C895EC4CFA02319LL,0xDC795F66CE260CABLL},{0xF480FE4BEA779115LL,0xF480FE4BEA779115LL,(-2L),0x95CDD3830EB65C90LL,0x2098D2E2ED89F7A8LL,0x9C895EC4CFA02319LL,1L},{0x6C0083C60E6EA020LL,(-2L),1L,(-1L),0x58352F4EE5217BA5LL,0x2098D2E2ED89F7A8LL,0xF95D5A1DC3DD3550LL},{0x9C895EC4CFA02319LL,(-3L),0x2098D2E2ED89F7A8LL,1L,0x2098D2E2ED89F7A8LL,(-3L),0x9C895EC4CFA02319LL},{8L,0x9C895EC4CFA02319LL,(-5L),1L,0xF480FE4BEA779115LL,0xF95D5A1DC3DD3550LL,(-1L)},{0x470CB926A698E7B6LL,0x504CA6F6CE57C863LL,0L,(-1L),8L,0x58352F4EE5217BA5LL,0x58352F4EE5217BA5LL}};
        int32_t ** const * const l_824[7][1][5] = {{{&l_630,&l_630,&l_630,(void*)0,&l_630}},{{&l_630,&l_630,&l_630,&l_630,&l_630}},{{&l_630,&l_630,&l_630,&l_630,&l_630}},{{&l_630,&l_630,(void*)0,&l_630,&l_630}},{{&l_630,&l_630,&l_630,&l_630,&l_630}},{{(void*)0,&l_630,&l_630,&l_630,&l_630}},{{&l_630,&l_630,&l_630,&l_630,(void*)0}}};
        int64_t **l_972 = &g_532[2];
        int16_t *l_974 = (void*)0;
        uint16_t l_988 = 0xE49EL;
        const uint8_t l_1031 = 0x4AL;
        int64_t l_1064 = 9L;
        uint32_t l_1071 = 1UL;
        const uint64_t *l_1106[6] = {&g_207,&g_207,&g_207,&g_207,&g_207,&g_207};
        const int32_t l_1107 = 0x75630F56L;
        int i, j, k;
        if ((((((safe_lshift_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((l_630 == (void*)0), (*p_28))), 6)) < (safe_mul_func_uint8_t_u_u((p_26 && ((g_262 ^ (safe_mul_func_uint16_t_u_u((((((((safe_mul_func_int8_t_s_s(((safe_add_func_int32_t_s_s(1L, ((safe_lshift_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((safe_sub_func_int8_t_s_s(p_26, (safe_unary_minus_func_int8_t_s((safe_lshift_func_uint8_t_u_s((safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint8_t_u_u((p_26 , 255UL), p_26)), p_26)), g_258.f0)))))), p_26)), p_26)) == 65535UL))) & l_654), l_654)) != g_262) == g_11) , 0xD4736151L) >= 0UL) > 0x8D0C9F73L) >= l_654), g_207))) , g_12)), l_654))) > 1L) & 0x21L) >= p_26))
        { /* block id: 275 */
            int8_t l_658 = (-1L);
            int16_t *l_663 = &g_9;
            int8_t *l_666 = (void*)0;
            int8_t *l_667[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            int32_t l_669 = 9L;
            int32_t l_670 = 1L;
            float *l_672 = &g_206;
            int32_t *l_673 = &l_622[2][1];
            int32_t *l_674 = &l_622[1][0];
            int32_t *l_675 = &l_670;
            int32_t *l_676 = &l_668;
            int32_t *l_677 = &l_670;
            int32_t *l_678 = &g_3;
            int32_t *l_679 = &g_548;
            int32_t *l_680 = (void*)0;
            int32_t *l_681 = &l_622[1][0];
            int32_t *l_682 = &l_622[0][1];
            int32_t *l_683[4] = {&g_3,&g_3,&g_3,&g_3};
            int16_t l_684 = 1L;
            uint32_t l_686 = 0x9FFF035FL;
            int i;
            (*l_672) = ((~p_26) , (safe_sub_func_float_f_f(0xA.5AAECBp-64, (l_658 <= (((l_670 = (((l_669 = ((safe_sub_func_uint16_t_u_u(((((safe_rshift_func_int8_t_s_u((l_668 = (0L || (((&g_9 != (l_664 = l_663)) > (0x548EEA04L ^ g_262)) > ((*g_547) != 0L)))), p_26)) ^ 0x2E21L) , 65535UL) <= 0x3359L), p_26)) && l_654)) , 8UL) < 2UL)) ^ 0x13L) , 0x0.Cp-1)))));
            l_686++;
            for (l_686 = (-25); (l_686 == 53); l_686 = safe_add_func_int8_t_s_s(l_686, 6))
            { /* block id: 284 */
                int32_t l_695[10] = {0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL,0x105F054BL};
                int32_t l_697 = 0x3C27231EL;
                int i;
                for (g_3 = 0; (g_3 >= (-5)); g_3 = safe_sub_func_uint8_t_u_u(g_3, 3))
                { /* block id: 287 */
                    float l_696[5][7] = {{0x4.6p+1,0x4.6p+1,0xC.4C03B1p+71,0xD.081D11p+49,0xC.4C03B1p+71,0x4.6p+1,0x4.6p+1},{0x5.EFEA8Bp+54,0x0.Fp+1,(-0x1.8p+1),0x0.Fp+1,0x5.EFEA8Bp+54,0x5.EFEA8Bp+54,0x0.Fp+1},{0xE.D6AD76p+16,(-0x1.5p+1),0xE.D6AD76p+16,0xC.4C03B1p+71,0xC.4C03B1p+71,0xE.D6AD76p+16,(-0x1.5p+1)},{0x0.Fp+1,0x1.2p+1,(-0x1.8p+1),(-0x1.8p+1),0x1.2p+1,0x0.Fp+1,0x1.2p+1},{0xE.D6AD76p+16,0xC.4C03B1p+71,0xC.4C03B1p+71,0xE.D6AD76p+16,(-0x1.5p+1),0xE.D6AD76p+16,0xC.4C03B1p+71}};
                    int32_t l_698[6][10] = {{0xA76F7029L,(-5L),2L,2L,(-5L),0xA76F7029L,3L,(-4L),(-1L),(-4L)},{0xBD3B2C68L,2L,(-1L),5L,(-1L),2L,0xBD3B2C68L,3L,3L,3L},{0xBD3B2C68L,3L,0xA76F7029L,0x9D9321D2L,0x9D9321D2L,0xA76F7029L,3L,0xBD3B2C68L,(-5L),3L},{0xA76F7029L,3L,0xBD3B2C68L,(-5L),3L,(-5L),0xBD3B2C68L,3L,0xA76F7029L,0x9D9321D2L},{(-1L),2L,0xBD3B2C68L,3L,3L,3L,3L,0xBD3B2C68L,2L,(-1L)},{2L,(-5L),0xA76F7029L,3L,(-4L),(-1L),(-4L),3L,0xA76F7029L,(-5L)}};
                    uint8_t *l_708 = &l_654;
                    int i, j;
                    for (g_85 = 0; (g_85 <= 1); ++g_85)
                    { /* block id: 290 */
                        uint32_t l_699 = 18446744073709551615UL;
                        l_699++;
                    }
                    (*l_682) = ((l_697 > ((safe_mod_func_int8_t_s_s((safe_div_func_int16_t_s_s((p_26 > 0x0D12L), g_10)), p_26)) , (l_698[0][6] & (l_708 == l_667[3])))) != (safe_lshift_func_int8_t_s_s(p_26, p_26)));
                    g_206 = (l_668 = (safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f(g_379[1].f0, ((((l_654 , l_654) , &g_9) == &g_9) >= ((((-4L) ^ ((**l_729) = ((safe_div_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_s(((((safe_unary_minus_func_uint8_t_u(((void*)0 == l_728))) , &l_708) != l_729) != (*l_678)), p_26)) != 0xAEC923CBL), p_26)), l_668)), 0x6B9AF378B9419109LL)) > 0xA3L))) > l_695[5]) , g_9)))), (*l_674))), 0x1.Ep+1)));
                }
            }
        }
        else
        { /* block id: 299 */
            int16_t * const l_735 = &g_262;
            int32_t l_780 = 0xEBB9D20DL;
            int32_t * const *l_782[8];
            int32_t * const **l_781[3][7][4] = {{{&l_782[5],&l_782[5],(void*)0,&l_782[5]},{(void*)0,&l_782[5],(void*)0,&l_782[7]},{&l_782[5],(void*)0,(void*)0,&l_782[7]},{&l_782[5],&l_782[5],&l_782[5],&l_782[5]},{&l_782[5],&l_782[5],&l_782[5],&l_782[5]},{(void*)0,&l_782[5],&l_782[5],&l_782[5]},{(void*)0,&l_782[7],&l_782[5],(void*)0}},{{(void*)0,&l_782[5],&l_782[5],&l_782[5]},{(void*)0,(void*)0,&l_782[5],&l_782[5]},{&l_782[5],&l_782[5],&l_782[5],&l_782[5]},{&l_782[5],(void*)0,(void*)0,&l_782[5]},{&l_782[5],(void*)0,(void*)0,&l_782[5]},{(void*)0,&l_782[5],(void*)0,&l_782[5]},{&l_782[5],(void*)0,&l_782[5],&l_782[5]}},{{&l_782[5],&l_782[5],&l_782[5],(void*)0},{&l_782[5],&l_782[7],&l_782[5],&l_782[5]},{&l_782[5],(void*)0,(void*)0,&l_782[5]},{&l_782[5],&l_782[5],&l_782[5],(void*)0},{&l_782[5],(void*)0,&l_782[5],(void*)0},{&l_782[5],&l_782[5],&l_782[5],(void*)0},{(void*)0,(void*)0,&l_782[5],(void*)0}}};
            int8_t l_783 = 2L;
            const uint8_t *l_808[4][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
            const uint8_t **l_807 = &l_808[1][4];
            const uint8_t ***l_806 = &l_807;
            const uint8_t ****l_805 = &l_806;
            const uint8_t *****l_809 = &l_805;
            uint8_t ****l_810 = &g_494;
            int16_t l_820 = 0L;
            uint32_t l_856 = 0UL;
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_782[i] = (void*)0;
            for (g_12 = 0; (g_12 <= 1); g_12 += 1)
            { /* block id: 302 */
                int32_t *l_734 = &l_622[0][1];
                uint32_t *l_744 = (void*)0;
                uint32_t *l_745 = &g_334;
                uint8_t *****l_771 = (void*)0;
                const uint8_t **l_775[1];
                const uint8_t ***l_774 = &l_775[0];
                const uint8_t ****l_773 = &l_774;
                const uint8_t *****l_772 = &l_773;
                int i;
                for (i = 0; i < 1; i++)
                    l_775[i] = (void*)0;
                if (((safe_sub_func_int16_t_s_s((((safe_unary_minus_func_uint64_t_u(((0xD.4B3BCAp-35 > ((l_734 != p_27) == ((l_664 == l_735) , (((safe_div_func_int8_t_s_s((7UL & ((safe_div_func_uint8_t_u_u(((((p_26 == ((*l_745) = (0xA0B1FB49L & ((l_740 != g_742) >= 0xDA32L)))) < 0x7E50DF66L) , (*l_734)) > 6UL), (*l_734))) ^ (*l_734))), 0x8CL)) && 0x77L) , p_26)))) , p_26))) , (void*)0) == &l_741), p_26)) < l_685))
                { /* block id: 304 */
                    int64_t l_778 = 0x6283C409492B8A02LL;
                    uint16_t *l_779 = &g_83;
                    (*g_547) = ((((((safe_add_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((((*l_741) = 1UL) , (safe_sub_func_int16_t_s_s((p_26 <= (p_26 ^ ((safe_lshift_func_int8_t_s_s(((safe_div_func_uint16_t_u_u(5UL, (safe_sub_func_int32_t_s_s((safe_mul_func_int8_t_s_s(((((safe_sub_func_int8_t_s_s((~p_26), ((safe_add_func_int32_t_s_s((p_26 < (safe_div_func_int16_t_s_s(((*l_664) = (safe_sub_func_int64_t_s_s(((safe_mul_func_uint8_t_u_u(((l_771 == l_772) , (((safe_lshift_func_uint16_t_u_u(((*l_779) = l_778), 7)) != l_685) | 0x4EC516FEL)), p_26)) > l_780), 8UL))), (-1L)))), p_26)) >= p_26))) >= p_26) , l_781[1][0][2]) == &g_569[1][5]), (*l_734))), l_685)))) | 0x38L), 6)) , l_783))), g_334))), p_26)), l_549[2])) , 1L) && p_26) ^ 0UL) | p_26) ^ p_26);
                    return p_27;
                }
                else
                { /* block id: 310 */
                    uint64_t l_784 = 0xD348C4A42443578ELL;
                    uint8_t *****l_788[9];
                    int i;
                    for (i = 0; i < 9; i++)
                        l_788[i] = (void*)0;
                    if ((l_784 , ((l_730 == (void*)0) || l_784)))
                    { /* block id: 311 */
                        uint64_t *l_796 = &l_549[2];
                        int32_t l_797 = 0xCAE7B430L;
                        (*g_547) |= ((g_558 , ((!1UL) <= (safe_add_func_int16_t_s_s(l_784, 0x62E0L)))) <= ((&l_773 == (l_789[9] = l_788[7])) <= ((safe_mod_func_int64_t_s_s(((0x2420B9ECL == l_792) <= (safe_div_func_int8_t_s_s((+(((*l_796) ^= 0x933F2E6C3C1F3850LL) >= l_797)), p_26))), g_262)) , g_338)));
                    }
                    else
                    { /* block id: 315 */
                        return p_28;
                    }
                }
                if (l_668)
                    goto lbl_798;
                for (l_783 = 1; (l_783 >= 0); l_783 -= 1)
                { /* block id: 322 */
                    if (g_334)
                        goto lbl_798;
                    return (*g_101);
                }
            }
            l_668 |= ((0x796CD0EDE6279F76LL <= (((((l_741 == ((l_685 &= ((safe_lshift_func_int8_t_s_u(((safe_div_func_uint16_t_u_u(((safe_div_func_int32_t_s_s(((l_654 & (((*l_809) = l_805) == l_810)) > (p_26 ^ (&g_83 != &p_26))), (*p_27))) <= p_26), 3UL)) & g_420.f0), p_26)) == (*p_27))) , p_27)) ^ p_26) || l_813[5][6]) > 18446744073709551607UL) | (*g_547))) , (-1L));
            if ((safe_lshift_func_uint16_t_u_u(((p_26 > g_548) & (((((safe_add_func_int32_t_s_s((safe_mod_func_uint16_t_u_u(g_334, ((*l_735) = g_207))), (p_28 == p_27))) , 0x2576L) && g_212) <= ((*l_728) = (l_820 >= (*p_28)))) <= g_334)), 4)))
            { /* block id: 332 */
                float l_823[3][9] = {{0x3.Cp-1,0x5.6p-1,(-0x8.Cp+1),(-0x1.7p-1),(-0x8.Cp+1),0x5.6p-1,0x3.Cp-1,0x5.39E42Cp+59,(-0x1.Cp-1)},{0x5.39E42Cp+59,0x0.Bp+1,(-0x1.7p-1),(-0x1.2p+1),0xF.848AE0p-11,0x5.6p-1,0xF.848AE0p-11,(-0x1.2p+1),(-0x1.7p-1)},{(-0x1.7p-1),(-0x1.7p-1),0x0.Cp-1,0x6.497DEFp+79,(-0x1.Cp-1),0x0.Bp+1,0x5.6p-1,0x5.39E42Cp+59,0x5.6p-1}};
                float *l_826 = &g_206;
                float **l_825 = &l_826;
                int32_t l_832[10][9] = {{(-1L),(-2L),0x629C638EL,0x56592CEDL,0x56592CEDL,0x629C638EL,(-2L),(-1L),(-9L)},{0xDAA26500L,(-1L),(-1L),(-9L),0x6FBB7285L,0x6FBB7285L,(-9L),(-1L),(-1L)},{0x56592CEDL,(-1L),6L,(-9L),0x10C83A0CL,0L,0L,0x10C83A0CL,(-9L)},{(-1L),0x5D010F6BL,(-1L),0L,(-9L),0xDAA26500L,0xF75DB19AL,0xF75DB19AL,0xDAA26500L},{6L,(-1L),0x56592CEDL,(-1L),6L,(-9L),0x10C83A0CL,0L,0L},{(-1L),(-1L),0xDAA26500L,0L,0xDAA26500L,(-1L),(-1L),(-9L),0x6FBB7285L},{0x629C638EL,(-2L),(-1L),(-9L),0xA3B428E1L,(-9L),(-1L),(-2L),0x629C638EL},{(-1L),8L,0xF75DB19AL,(-9L),0xA49A4BCEL,0xDAA26500L,0xA49A4BCEL,(-9L),0xF75DB19AL},{0x10C83A0CL,0x10C83A0CL,0xEFAE7097L,0x56592CEDL,(-2L),0L,0x629C638EL,0L,(-2L)},{(-1L),0xA49A4BCEL,0xA49A4BCEL,(-1L),(-1L),0x6FBB7285L,0L,0xF75DB19AL,0L}};
                int8_t *l_837[4] = {&l_783,&l_783,&l_783,&l_783};
                int32_t l_838[10];
                int32_t l_839 = (-7L);
                int i, j;
                for (i = 0; i < 10; i++)
                    l_838[i] = 0xE225EC84L;
                l_839 |= (safe_add_func_int64_t_s_s(p_26, (((((void*)0 != l_824[2][0][3]) , (l_838[9] &= (((*l_728) = ((((*l_825) = p_28) != (((p_26 , (safe_sub_func_uint8_t_u_u(p_26, (safe_mul_func_int16_t_s_s(((l_832[1][5] = (safe_unary_minus_func_int16_t_s(((*l_735) &= p_26)))) != (safe_lshift_func_uint8_t_u_u(((*l_730)--), (0x0C67ADA94415D7D3LL ^ p_26)))), g_93))))) , p_26) , p_27)) == p_26)) > 0x79L))) , 0x2A1CL) , 0x1F068CB4128F1E53LL)));
            }
            else
            { /* block id: 340 */
                const uint8_t *****l_848 = (void*)0;
                uint16_t *l_849[8][1] = {{&g_83},{&g_85},{&g_83},{&g_85},{&g_83},{&g_85},{&g_83},{&g_85}};
                int32_t l_860 = 1L;
                float *l_861 = &g_221;
                int i, j;
                (*p_28) = (safe_lshift_func_uint8_t_u_s(6UL, (g_863 = ((((*l_861) = (((safe_div_func_float_f_f((((safe_mul_func_uint16_t_u_u((g_85 = (+((g_847 = l_789[4]) == l_848))), ((!p_26) < p_26))) | (+(safe_rshift_func_int8_t_s_s(((*l_728) |= (safe_rshift_func_uint8_t_u_u((0xCF14F38FL < (((*l_735) &= (1UL && l_856)) , (safe_mul_func_uint8_t_u_u((g_859[2] , 0xBEL), g_262)))), p_26))), l_860)))) , 0x7.D37B47p+71), g_93)) < p_26) >= 0x1.3ECD62p+88)) , g_862[1]) , g_235))));
            }
        }
        if (l_864)
        { /* block id: 350 */
            float l_871 = 0x4.F6A6AFp-26;
            int32_t l_872[9][6][4] = {{{1L,6L,1L,8L},{0x24E7077CL,1L,0xDF47AFD6L,0x24E7077CL},{8L,7L,1L,1L},{7L,0x70504FE9L,1L,1L},{8L,8L,0xDF47AFD6L,1L},{0x24E7077CL,5L,0L,1L}},{{0L,1L,7L,0L},{8L,1L,6L,1L},{1L,5L,1L,1L},{0xFBF71A74L,8L,7L,1L},{0x24E7077CL,0x70504FE9L,(-8L),1L},{0x24E7077CL,7L,7L,0x24E7077CL}},{{0xFBF71A74L,1L,1L,7L},{1L,0x70504FE9L,6L,1L},{8L,0xFBF71A74L,7L,1L},{0L,0x70504FE9L,0L,7L},{0x24E7077CL,1L,0xDF47AFD6L,0x24E7077CL},{8L,7L,1L,1L}},{{7L,0x70504FE9L,1L,1L},{8L,8L,0xDF47AFD6L,1L},{0x24E7077CL,5L,0L,1L},{0L,1L,7L,0L},{8L,1L,6L,1L},{1L,5L,1L,1L}},{{0xFBF71A74L,8L,7L,1L},{0x24E7077CL,0x70504FE9L,(-8L),1L},{0x24E7077CL,7L,7L,0x24E7077CL},{0xFBF71A74L,1L,1L,7L},{1L,0x70504FE9L,6L,1L},{8L,0xFBF71A74L,7L,1L}},{{0L,0x70504FE9L,0L,7L},{0x24E7077CL,1L,0xDF47AFD6L,0x24E7077CL},{8L,7L,1L,1L},{7L,0x70504FE9L,1L,1L},{8L,8L,0xDF47AFD6L,1L},{0x24E7077CL,5L,0L,1L}},{{0L,1L,7L,0L},{8L,1L,6L,1L},{1L,5L,1L,1L},{0xFBF71A74L,8L,7L,1L},{0x24E7077CL,0x70504FE9L,(-8L),1L},{0x24E7077CL,7L,7L,0x24E7077CL}},{{0xFBF71A74L,1L,1L,7L},{1L,0x70504FE9L,6L,1L},{8L,0xFBF71A74L,7L,1L},{0L,0x70504FE9L,0L,7L},{0x24E7077CL,1L,0xDF47AFD6L,0x24E7077CL},{8L,7L,1L,1L}},{{7L,0x70504FE9L,1L,0xC6DC9A1CL},{7L,7L,0x24E7077CL,5L},{1L,0xD9608A6EL,6L,0L},{6L,0L,(-8L),6L},{7L,0L,0x70504FE9L,0L},{0L,0xD9608A6EL,0xC6DC9A1CL,5L}}};
            uint32_t l_927[4][2][5] = {{{0xFDAFFE1CL,0UL,0xF0322181L,0x479CBB2DL,0xF0322181L},{1UL,1UL,0xF3AEBCEBL,0x704C9CFBL,0xF3AEBCEBL}},{{0xFDAFFE1CL,0UL,0xF0322181L,0x479CBB2DL,0xF0322181L},{1UL,1UL,0xF3AEBCEBL,0x704C9CFBL,0xF3AEBCEBL}},{{0xFDAFFE1CL,0UL,0xF0322181L,0x479CBB2DL,0xF0322181L},{1UL,1UL,0xF3AEBCEBL,0x704C9CFBL,0xF3AEBCEBL}},{{0xFDAFFE1CL,0UL,0xF0322181L,0x479CBB2DL,0xF0322181L},{1UL,1UL,0xF3AEBCEBL,0x704C9CFBL,0xF3AEBCEBL}}};
            int32_t l_929 = 0xE87C7AA2L;
            uint16_t l_947[9][1][10] = {{{0x1851L,0x1851L,0x0065L,0xCDE5L,0UL,0x750EL,0xE6F7L,0x750EL,0UL,0xCDE5L}},{{0xCDE5L,0xA270L,0xCDE5L,0x750EL,0x6F7BL,0xEB3AL,0xE6F7L,0xE6F7L,0xEB3AL,0x6F7BL}},{{0x0065L,0x1851L,0x1851L,0x0065L,0xCDE5L,0UL,0x750EL,0xE6F7L,0x750EL,0UL}},{{0xEB3AL,0x52ABL,0xCDE5L,0x52ABL,0xEB3AL,65528UL,0x1851L,0x750EL,0x750EL,0x1851L}},{{0xE6F7L,65528UL,0x0065L,0x0065L,65528UL,0xE6F7L,0x6F7BL,0x1851L,0xEB3AL,0x1851L}},{{0x52ABL,0x0065L,0xEB3AL,0x750EL,0xEB3AL,0x0065L,0x52ABL,0x6F7BL,0UL,0UL}},{{0x52ABL,0UL,0xE6F7L,0xCDE5L,0xCDE5L,0xE6F7L,0UL,0x52ABL,65528UL,0x6F7BL}},{{0xE6F7L,0UL,0x52ABL,65528UL,0x6F7BL,65528UL,0x52ABL,0UL,0xE6F7L,0xCDE5L}},{{0xEB3AL,0x0065L,0x52ABL,0x6F7BL,0UL,0UL,0x6F7BL,0x52ABL,0x0065L,0xEB3AL}}};
            uint32_t ***l_969 = &g_934[1][1];
            uint16_t *l_973 = &g_85;
            uint64_t *l_975 = &l_549[2];
            uint64_t *l_976[2];
            uint8_t ****l_981 = &g_494;
            int32_t l_1006[9][4][7] = {{{0x60E70981L,4L,1L,1L,0x5A7328C4L,0L,0x5A7328C4L},{0x8C9B2D95L,(-8L),0x4A4B9EF3L,0L,(-1L),1L,0x42516DCCL},{0x0AE99435L,0x64680805L,0x1F7B33DEL,0xEF883C20L,(-1L),0xB4B64885L,0x7709816BL},{(-1L),(-1L),0x1C797EEEL,0x23DD0333L,(-8L),0x42516DCCL,0x42516DCCL}},{{0x26506E57L,0xA0286336L,0x0F9AC7F1L,0xA0286336L,0x26506E57L,4L,0x5A7328C4L},{(-1L),1L,(-1L),0x333053FEL,(-9L),0x8C9B2D95L,6L},{(-7L),(-4L),(-1L),4L,0x942F3624L,0xA0286336L,(-1L)},{(-1L),0L,0x4A4B9EF3L,(-8L),0x8C9B2D95L,0xCF244440L,(-7L)}},{{1L,(-1L),0x44F896AAL,0x64680805L,0x26506E57L,0L,9L},{0x23DD0333L,(-9L),0x333053FEL,(-1L),1L,(-1L),0xCF244440L},{3L,(-1L),0x1F7B33DEL,0xA0286336L,0x1F7B33DEL,(-1L),3L},{(-8L),0L,0xCF244440L,1L,0x516D47F2L,0x68D1BB72L,0x42516DCCL}},{{0x5A7328C4L,(-4L),(-5L),(-1L),(-2L),0L,0xED2406BBL},{6L,0L,0xCF244440L,0x333053FEL,0x23DD0333L,(-1L),0x1C797EEEL},{1L,0x2517AA39L,0x1F7B33DEL,(-8L),(-7L),0xA0286336L,0x0AE99435L},{(-1L),6L,0x333053FEL,6L,(-1L),0L,0x42516DCCL}},{{0x0F9AC7F1L,0L,0x44F896AAL,(-8L),9L,(-8L),0x44F896AAL},{0x516D47F2L,0x516D47F2L,0x4A4B9EF3L,0x333053FEL,0L,0xC540D5A4L,0xCF244440L},{(-1L),(-8L),(-1L),(-1L),0x4A312050L,0xEF883C20L,0x7709816BL},{(-1L),0x4A4B9EF3L,6L,1L,0L,0x1C797EEEL,(-7L)}},{{(-2L),1L,(-2L),0xA0286336L,9L,0xB4B64885L,0x0F9AC7F1L},{6L,(-8L),0x68D1BB72L,(-1L),(-1L),(-8L),6L},{(-1L),(-8L),(-1L),0x64680805L,(-7L),0xB4B64885L,(-7L)},{(-8L),0x42516DCCL,0x42516DCCL,(-8L),0x23DD0333L,0x1C797EEEL,(-1L)}},{{0x44F896AAL,0L,0x0F9AC7F1L,4L,(-2L),0xEF883C20L,0x26506E57L},{0x23DD0333L,6L,0x68D1BB72L,0L,0x516D47F2L,0xC540D5A4L,(-1L)},{0x1F7B33DEL,0x2517AA39L,1L,0x2517AA39L,0x1F7B33DEL,(-8L),(-7L)},{(-1L),0x1C797EEEL,6L,0x8C9B2D95L,1L,0L,6L}},{{(-5L),(-4L),0x5A7328C4L,4L,0x26506E57L,0xA0286336L,0x0F9AC7F1L},{(-1L),0x8C9B2D95L,0x4A4B9EF3L,0x4A4B9EF3L,0x8C9B2D95L,(-1L),(-7L)},{0x1F7B33DEL,(-1L),3L,0x64680805L,0x942F3624L,0L,0x7709816BL},{0x23DD0333L,0x20401B5BL,0x333053FEL,0xC540D5A4L,1L,0x68D1BB72L,0xCF244440L}},{{0x44F896AAL,(-1L),1L,0xA0286336L,1L,(-1L),1L},{0x516D47F2L,(-8L),0x333053FEL,0x1C797EEEL,0xC540D5A4L,(-1L),(-1L)},{(-7L),0xA0286336L,0x0AE99435L,(-4L),0x4A312050L,0xB4B64885L,3L},{(-9L),0x68D1BB72L,0x333053FEL,0x8C9B2D95L,0x8C9B2D95L,0x333053FEL,0x68D1BB72L}}};
            int8_t l_1105 = (-1L);
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_976[i] = &g_12;
            for (g_338 = 0; (g_338 > 12); g_338++)
            { /* block id: 353 */
                uint16_t *l_881 = &g_83;
                int32_t l_886[6] = {0xBECFBC05L,0xBECFBC05L,0xBECFBC05L,0xBECFBC05L,0xBECFBC05L,0xBECFBC05L};
                int32_t l_899 = 0xBE0B592AL;
                uint8_t *****l_906 = &g_493[9];
                uint16_t l_928 = 0xF07FL;
                uint32_t **l_936 = &l_741;
                int i;
                if ((safe_rshift_func_uint16_t_u_u((p_26 , (safe_mod_func_uint16_t_u_u(0x9611L, ((g_559.f0 | l_872[8][5][1]) & ((safe_sub_func_uint32_t_u_u(((safe_rshift_func_uint8_t_u_u(p_26, (p_26 > (safe_mul_func_uint16_t_u_u(g_212, ((safe_lshift_func_int16_t_s_s(0x16BBL, ((((++(*l_881)) || ((*l_881) ^= (g_207 , 65528UL))) | 0xA7L) & p_26))) | g_334)))))) | g_338), (*p_28))) ^ 0x798CDEEAL))))), 15)))
                { /* block id: 356 */
                    int8_t l_898 = 0xBCL;
                    int64_t *l_900 = &l_813[7][5];
                    int64_t *l_901[7];
                    const int32_t l_902 = 0xC66E6471L;
                    int i;
                    for (i = 0; i < 7; i++)
                        l_901[i] = &g_212;
                    for (l_668 = 0; (l_668 <= (-26)); l_668 = safe_sub_func_uint16_t_u_u(l_668, 6))
                    { /* block id: 359 */
                        l_886[0] |= (*p_28);
                        return p_27;
                    }
                    (*p_28) = (safe_lshift_func_int16_t_s_u((safe_mul_func_int8_t_s_s((+(+(((p_26 >= (((safe_rshift_func_uint16_t_u_u(p_26, ((!l_886[0]) & ((l_872[8][5][1] = ((*l_900) &= (((l_899 = (0x8FL != ((safe_mul_func_uint16_t_u_u((((p_27 == &g_334) && p_26) | ((l_898 &= ((**l_729) = 0x7FL)) , 0x8CL)), p_26)) && l_886[1]))) != l_872[8][5][1]) < p_26))) & 0xFA269E0F1613F0BALL)))) > 0x34L) < l_902)) || 18446744073709551613UL) || 0x34E8ACAA7A0DE5F1LL))), 0L)), p_26));
                }
                else
                { /* block id: 369 */
                    uint8_t *****l_905[1][6][10] = {{{&g_493[6],&g_493[6],(void*)0,&g_493[6],&g_493[6],(void*)0,&g_493[6],&g_493[6],(void*)0,&g_493[6]},{&g_493[6],(void*)0,&g_493[0],&g_493[6],&g_493[0],(void*)0,&g_493[6],(void*)0,&g_493[0],&g_493[6]},{&g_493[0],&g_493[6],&g_493[0],(void*)0,&g_493[6],(void*)0,&g_493[0],&g_493[6],&g_493[0],(void*)0},{&g_493[6],&g_493[6],(void*)0,&g_493[6],&g_493[6],(void*)0,&g_493[6],&g_493[6],(void*)0,&g_493[6]},{&g_493[6],(void*)0,&g_493[0],&g_493[6],&g_493[0],(void*)0,&g_493[6],(void*)0,&g_493[0],&g_493[6]},{&g_493[0],&g_493[6],&g_493[0],(void*)0,&g_493[6],(void*)0,&g_493[0],&g_493[6],&g_493[0],(void*)0}}};
                    uint32_t *l_924 = &g_925;
                    int32_t l_945 = 0x0E1DC83EL;
                    int i, j, k;
                    if (((safe_rshift_func_int16_t_s_u((l_905[0][1][5] == l_906), (l_929 ^= ((~(((safe_mod_func_int32_t_s_s((((safe_unary_minus_func_uint16_t_u((((safe_mod_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u(l_886[3], 6)), (safe_lshift_func_int8_t_s_u((g_334 >= (safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((((*l_924) = (safe_lshift_func_uint8_t_u_s((0xCFL < g_923[2]), 7))) , (g_926[4] = ((0xCE75230FAFD04A0BLL != (0xC07CL >= ((*l_881) &= (l_872[8][5][1] || l_872[8][5][1])))) ^ g_258.f0))) , l_927[3][1][3]), p_26)), g_3))), l_927[1][0][2])))) , g_11) | (-1L)))) != l_928) , (*g_78)), (*g_547))) , 1L) || 0x172ED080L)) & 0xFA16L)))) <= (*g_228)))
                    { /* block id: 374 */
                        uint32_t ***l_932 = (void*)0;
                        uint32_t ***l_933 = &l_740;
                        const int32_t l_946 = 0x13CFD30EL;
                        if (l_929)
                            break;
                        (*p_28) |= ((((((safe_sub_func_int64_t_s_s((((*l_933) = (void*)0) == (l_936 = g_934[1][1])), 0x42FEAB3BDF125B64LL)) ^ l_886[0]) == (safe_add_func_uint8_t_u_u(((g_262 <= (safe_mod_func_uint64_t_u_u(((safe_div_func_uint32_t_u_u((p_26 || ((safe_lshift_func_uint8_t_u_u((g_10 | 0xAF31L), ((*l_730) = p_26))) == g_229[2][1][3])), g_338)) != g_926[6]), p_26))) || 1UL), 7L))) , l_945) <= p_26) , (*p_27));
                        if (l_946)
                            break;
                    }
                    else
                    { /* block id: 381 */
                        ++l_947[2][0][1];
                    }
                    (*g_547) |= (*g_78);
                    if ((**g_77))
                        break;
                }
                for (l_654 = 17; (l_654 != 2); l_654--)
                { /* block id: 389 */
                    int16_t l_952 = 5L;
                    int32_t l_953[4][1][6];
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 1; j++)
                        {
                            for (k = 0; k < 6; k++)
                                l_953[i][j][k] = 0x851AF7C3L;
                        }
                    }
                    g_954[3]++;
                }
            }
lbl_1036:
            if ((((((l_929 = ((*l_975) &= ((((safe_div_func_int16_t_s_s(0x0222L, ((safe_sub_func_uint32_t_u_u(((safe_mod_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s(((((safe_rshift_func_int16_t_s_s(p_26, ((*l_664) = (safe_mod_func_int16_t_s_s((l_969 == &l_740), (safe_rshift_func_uint16_t_u_u(((*l_973) = (l_972 != &g_228)), 1))))))) != p_26) < ((((l_872[8][5][1] , (void*)0) != (void*)0) || (-3L)) && 0x55E6L)) > g_954[0]), l_947[4][0][0])), 65526UL)) >= g_545), p_26)) && p_26))) , l_974) != l_973) == 0L))) , 0xFF2B95B0L) | (*p_27)) || g_925) > g_548))
            { /* block id: 397 */
                uint64_t l_977 = 0x6D0729193AC87F6CLL;
                int32_t l_987 = 0x31A24C22L;
                for (g_11 = 1; (g_11 >= 0); g_11 -= 1)
                { /* block id: 400 */
                    uint32_t l_986 = 3UL;
                    int32_t l_996 = 0x3E126966L;
                    (*p_28) = ((l_864 == ((l_977 <= ((safe_mul_func_uint16_t_u_u(((g_980 , (*l_560)) == l_981), ((l_977 < (p_26 ^ ((l_986 = (p_26 == (safe_mod_func_int16_t_s_s(((*l_664) = (safe_rshift_func_uint16_t_u_u(((l_872[8][5][1] &= 0xD6219A6148A8C965LL) > l_977), p_26))), g_545)))) || g_548))) > 18446744073709551613UL))) , l_987)) , g_258.f0)) && l_929);
                    for (l_929 = 1; (l_929 >= 0); l_929 -= 1)
                    { /* block id: 407 */
                        int64_t *l_993 = &g_212;
                        uint16_t l_1007 = 65527UL;
                        uint32_t *l_1008 = (void*)0;
                        uint32_t *l_1009 = &g_925;
                        int i, j;
                        (*g_547) = ((l_988 ^= l_622[g_11][g_11]) > ((*l_1009) &= (safe_mod_func_int64_t_s_s(((l_996 = (safe_add_func_int8_t_s_s((((*l_993) = (-1L)) == (safe_lshift_func_uint16_t_u_s((0x1EL || p_26), p_26))), p_26))) && (safe_rshift_func_int8_t_s_u((safe_sub_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_s((g_1003 , ((((g_258 , (++(**l_729))) , p_26) , 1L) ^ g_229[2][1][3])), 2)) || l_1006[1][2][1]) == l_1007), p_26)), 1))), p_26))));
                        return p_28;
                    }
                }
                (*g_78) &= (*p_28);
            }
            else
            { /* block id: 418 */
                int64_t *l_1029 = &g_926[0];
                int32_t l_1030 = 0L;
                (*p_28) = ((l_872[8][5][1] |= p_26) || ((*l_975) = (((((((safe_mul_func_int8_t_s_s(0x51L, (safe_mod_func_uint8_t_u_u((g_334 <= (((safe_rshift_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((1UL ^ (safe_div_func_uint32_t_u_u((((253UL & (safe_mul_func_int8_t_s_s(p_26, ((l_927[3][1][3] , ((*l_1029) = (safe_sub_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(((*l_728) = (+g_558.f0)), 1)), ((*g_228) ^ g_9))))) , l_1030)))) >= l_1031) < 0x8A7AE09DL), 1UL))), p_26)), p_26)) || p_26), 2)) >= l_1030) > g_548)), 0x7CL)))) ^ 0x69L) , l_1030) , p_26) ^ 0L) , l_1029) == l_976[0])));
            }
            for (g_334 = 0; (g_334 < 24); ++g_334)
            { /* block id: 427 */
                uint32_t l_1045 = 1UL;
                uint32_t **l_1059 = (void*)0;
                uint32_t l_1109 = 18446744073709551615UL;
                uint8_t ****l_1112 = (void*)0;
                uint32_t l_1128 = 1UL;
                for (g_863 = 25; (g_863 <= 36); g_863++)
                { /* block id: 430 */
                    uint32_t l_1046[8] = {0x723DDA35L,18446744073709551609UL,0x723DDA35L,18446744073709551609UL,0x723DDA35L,18446744073709551609UL,0x723DDA35L,18446744073709551609UL};
                    uint32_t ** const l_1060 = &g_935;
                    int32_t l_1103 = 0L;
                    int i;
                    if (l_654)
                        goto lbl_1036;
                    (*l_630) = func_52((g_1049 |= (p_26 <= (safe_div_func_int64_t_s_s((safe_mod_func_int64_t_s_s(((((safe_lshift_func_int8_t_s_s(((*l_728) &= (l_1046[5] = (p_26 < ((((void*)0 == g_1043) != l_1045) > (l_872[8][5][1] = (p_26 && p_26)))))), 7)) , (p_26 != (l_872[8][5][1] = (safe_add_func_uint32_t_u_u(p_26, g_379[1].f0))))) || g_258.f0) < p_26), 4L)), g_926[1])))));
                    if (((*p_28) = (((++(*l_975)) || (((((((safe_rshift_func_int8_t_s_s(((*l_728) = g_229[2][1][3]), (p_26 ^ (*p_28)))) > (~(safe_div_func_uint16_t_u_u(((((l_1059 == l_1060) || 0x9127L) , (safe_mod_func_int32_t_s_s((0xF8423E39490ED995LL <= ((*l_975) ^= (((p_26 || g_85) == 0x28A580C7F3DF104ALL) != 1UL))), p_26))) != l_1063), g_93)))) >= g_9) > g_926[4]) < l_1064) == (-10L)) ^ 0x05154B87L)) || 0L)))
                    { /* block id: 442 */
                        int64_t *l_1066[7];
                        int i;
                        for (i = 0; i < 7; i++)
                            l_1066[i] = &l_813[5][6];
                        (*g_1074) = (((((((((safe_unary_minus_func_uint8_t_u(p_26)) , l_1066[4]) != ((*l_972) = &g_926[4])) == ((!p_26) <= p_26)) <= ((p_26 < (((((!(safe_lshift_func_int16_t_s_u(l_1071, (((p_26 == (safe_div_func_int8_t_s_s(g_10, p_26))) , 4294967290UL) && 4UL)))) < l_1045) , l_664) == (void*)0) <= g_83)) > 0L)) <= g_212) > g_83) , (void*)0) == (void*)0);
                        return p_28;
                    }
                    else
                    { /* block id: 446 */
                        uint32_t *l_1087[9];
                        int32_t ** const l_1090[6][4] = {{&g_87[3][0][9],&g_87[0][0][4],&g_87[0][0][2],&g_547},{&g_87[0][1][3],&g_547,&g_87[0][0][2],&g_547},{&g_547,&g_87[0][0][2],&g_87[3][0][9],&g_547},{&g_87[3][0][9],&g_547,&g_547,&g_547},{&g_87[0][0][4],&g_87[0][0][4],&g_87[0][0][2],&g_87[0][1][3]},{&g_87[0][0][4],&g_87[0][0][2],&g_547,&g_87[0][0][4]}};
                        int8_t *l_1108[8][9] = {{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105},{&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,&l_1105,(void*)0}};
                        int i, j;
                        for (i = 0; i < 9; i++)
                            l_1087[i] = (void*)0;
                        (*p_28) = (safe_rshift_func_int16_t_s_u(((l_929 = ((*l_728) = (((((safe_mod_func_int32_t_s_s((((safe_mod_func_uint16_t_u_u(((*l_973) = (safe_sub_func_int8_t_s_s(((-1L) <= (((safe_sub_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((((((g_925--) != l_1046[6]) , (l_1090[1][1] != (void*)0)) > ((((safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(((l_1103 ^= (safe_rshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(((l_872[7][0][2] |= (p_26 > 0x003AC063B6E06D3ELL)) , g_227), 7)), 3))) == (*g_228)), (safe_unary_minus_func_int16_t_s((0x9EL >= 0x1AL))))), (-6L))), l_1046[5])), 0xFD13L)) , p_26) > g_926[5]) <= l_1105)) , (void*)0) != l_1106[2]), 0xB5L)), l_1045)) <= p_26) < 0x7C41L)), l_1107))), (-10L))) >= (*g_228)) | 0xBF98L), (*g_547))) ^ (*g_228)) >= 0x1F4EL) && l_1046[5]) || 0xEEC8L))) || g_338), l_1109));
                        return (*g_301);
                    }
                }
                (*g_547) &= (safe_mod_func_int16_t_s_s((((l_1045 , l_981) == l_1112) == (safe_div_func_uint16_t_u_u((p_26 > (((safe_add_func_int16_t_s_s(((safe_mod_func_uint64_t_u_u((safe_sub_func_int16_t_s_s((((*l_728) = p_26) < (l_872[8][5][1] |= (&g_334 != l_1121))), (safe_div_func_int64_t_s_s(((safe_sub_func_int32_t_s_s(0x22839A6AL, (safe_div_func_uint16_t_u_u((l_1045 == p_26), 1UL)))) && l_1128), l_927[3][1][3])))), 18446744073709551615UL)) >= g_954[2]), g_212)) == 0x0C197593F11DC0F9LL) > g_93)), p_26))), g_227));
            }
        }
        else
        { /* block id: 461 */
            uint8_t l_1135 = 248UL;
            int32_t l_1138 = 0x690E4AEEL;
            int32_t l_1139 = 0x46152354L;
            int32_t l_1142 = (-1L);
            int32_t l_1143 = 0x39F3755FL;
            int32_t l_1144 = 0x9D26E46CL;
            int32_t l_1146 = 0L;
            int32_t l_1147 = 0xD4210AF4L;
            int32_t l_1148 = 0x171888C6L;
            int32_t l_1149 = 0x6AB58567L;
            int32_t l_1150 = 0xF0FAECC9L;
            int32_t l_1151 = 4L;
            int32_t l_1152[6][8] = {{0L,(-8L),0L,0L,(-8L),0L,0L,(-8L)},{(-8L),0L,0L,(-8L),0L,0L,(-8L),0L},{(-8L),(-8L),(-9L),(-8L),(-8L),(-9L),(-8L),(-8L)},{0L,(-8L),0L,0L,(-8L),0L,0L,(-8L)},{(-8L),0L,0L,(-8L),0L,0L,(-8L),0L},{(-8L),(-8L),(-9L),(-8L),(-8L),(-9L),(-8L),(-8L)}};
            uint8_t l_1153 = 0x16L;
            int i, j;
            for (l_668 = 0; (l_668 != 4); l_668 = safe_add_func_uint64_t_u_u(l_668, 2))
            { /* block id: 464 */
                int64_t l_1141[3];
                int32_t l_1145[8] = {(-1L),7L,(-1L),(-1L),7L,(-1L),(-1L),7L};
                int i;
                for (i = 0; i < 3; i++)
                    l_1141[i] = 0x0ACE11DEFB2817FFLL;
                (*p_28) = ((*p_28) >= (((safe_rshift_func_uint8_t_u_s((safe_add_func_int32_t_s_s(0xC008447DL, 7UL)), 6)) , (p_26 != ((*l_728) ^= (&g_493[6] == &g_493[6])))) ^ ((l_1135 = (0x29L || 1L)) < (safe_mul_func_uint16_t_u_u((p_26 > 0x48L), g_1003.f0)))));
                --l_1153;
            }
        }
    }
    else
    { /* block id: 471 */
        (****g_1043) = ((void*)0 != &l_864);
    }
    g_1159--;
    if (((0xEEL >= (((safe_mod_func_uint64_t_u_u((((void*)0 != &l_1063) | 4294967295UL), ((safe_sub_func_int32_t_s_s((((*l_1156) ^ ((g_3 , (g_1175 = ((safe_mul_func_int8_t_s_s(g_307, (((safe_rshift_func_int16_t_s_s((((**l_561) == (**l_561)) < l_1172), p_26)) | 249UL) != (*g_547)))) | 0x5F26L))) != p_26)) ^ (-9L)), g_926[4])) | (*l_1156)))) | l_1176) ^ 0x59L)) , (-6L)))
    { /* block id: 476 */
        int32_t l_1177 = 0x2F594638L;
        int16_t l_1178 = 0xBFBDL;
        int32_t l_1179[3];
        int32_t l_1180 = 0x02004017L;
        int32_t l_1182 = 0x4762F85BL;
        uint32_t l_1183[3][4][1] = {{{0x05AC896EL},{0x971A5CD9L},{0x05AC896EL},{0x05AC896EL}},{{0x971A5CD9L},{0x05AC896EL},{0x05AC896EL},{0x971A5CD9L}},{{0x05AC896EL},{0x05AC896EL},{0x971A5CD9L},{0x05AC896EL}}};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1179[i] = 0x4DD7F103L;
        l_1183[2][2][0]++;
    }
    else
    { /* block id: 478 */
        int64_t *l_1201 = &g_212;
        int32_t l_1207[4] = {0x4221DBB6L,0x4221DBB6L,0x4221DBB6L,0x4221DBB6L};
        int32_t l_1240[3][8][8] = {{{0xE8D090E6L,2L,(-1L),0x1E74EED8L,(-1L),0x60BF5A69L,0x60BF5A69L,(-1L)},{0x3BCD5632L,(-1L),(-1L),0x3BCD5632L,0xE8D090E6L,0xFBA99932L,0x60BF5A69L,(-3L)},{2L,0x3BCD5632L,(-1L),0x60BF5A69L,(-1L),0x3BCD5632L,2L,0x5D147B16L},{(-1L),0x3BCD5632L,2L,0x5D147B16L,0xFBA99932L,0xFBA99932L,0x5D147B16L,2L},{(-1L),(-1L),0x3BCD5632L,0xE8D090E6L,0xFBA99932L,0x60BF5A69L,(-3L),0x60BF5A69L},{(-1L),2L,0xE8D090E6L,2L,(-1L),0x1E74EED8L,(-1L),0x60BF5A69L},{2L,0xFBA99932L,(-3L),0xE8D090E6L,0xE8D090E6L,(-3L),0xFBA99932L,2L},{0x3BCD5632L,0x1E74EED8L,(-3L),0x5D147B16L,(-1L),(-1L),(-1L),0x5D147B16L}},{{0xE8D090E6L,0xF9DFAC16L,0xE8D090E6L,0x60BF5A69L,0x5D147B16L,(-1L),(-3L),(-3L)},{(-3L),0x1E74EED8L,0x3BCD5632L,0x3BCD5632L,0x1E74EED8L,(-3L),0x5D147B16L,(-1L)},{(-3L),0xFBA99932L,2L,0x1E74EED8L,0x5D147B16L,0x1E74EED8L,2L,0xFBA99932L},{0xE8D090E6L,2L,(-1L),0x1E74EED8L,(-1L),0x60BF5A69L,0x60BF5A69L,(-1L)},{0x3BCD5632L,(-1L),(-1L),0x3BCD5632L,0xE8D090E6L,0xFBA99932L,0x60BF5A69L,(-3L)},{2L,0x3BCD5632L,(-1L),0x60BF5A69L,(-1L),0x3BCD5632L,2L,0x5D147B16L},{(-1L),0x3BCD5632L,2L,0x5D147B16L,0xFBA99932L,0xFBA99932L,0xFBA99932L,0xF9DFAC16L},{0x60BF5A69L,0x60BF5A69L,(-1L),0x1E74EED8L,(-1L),2L,0xE8D090E6L,2L}},{{(-3L),0xF9DFAC16L,0x1E74EED8L,0xF9DFAC16L,(-3L),0x3BCD5632L,0x60BF5A69L,2L},{0xF9DFAC16L,(-1L),0xE8D090E6L,0x1E74EED8L,0x1E74EED8L,0xE8D090E6L,(-1L),0xF9DFAC16L},{(-1L),0x3BCD5632L,0xE8D090E6L,0xFBA99932L,0x60BF5A69L,(-3L),0x60BF5A69L,0xFBA99932L},{0x1E74EED8L,0x5D147B16L,0x1E74EED8L,2L,0xFBA99932L,(-3L),0xE8D090E6L,0xE8D090E6L},{0xE8D090E6L,0x3BCD5632L,(-1L),(-1L),0x3BCD5632L,0xE8D090E6L,0xFBA99932L,0x60BF5A69L},{0xE8D090E6L,(-1L),0xF9DFAC16L,0x3BCD5632L,0xFBA99932L,0x3BCD5632L,0xF9DFAC16L,(-1L)},{0x1E74EED8L,0xF9DFAC16L,(-3L),0x3BCD5632L,0x60BF5A69L,2L,2L,0x60BF5A69L},{(-1L),0x60BF5A69L,0x60BF5A69L,(-1L),0x1E74EED8L,(-1L),2L,0xE8D090E6L}}};
        uint32_t l_1248 = 3UL;
        int16_t *l_1311 = &l_1206;
        uint8_t l_1325 = 255UL;
        int16_t * const l_1342 = &g_9;
        uint8_t *****l_1448 = &g_493[6];
        union U0 *l_1488[7] = {&g_1302[0],&g_1302[4],&g_1302[0],&g_1302[0],&g_1302[4],&g_1302[0],&g_1302[0]};
        int8_t *l_1492[1][5][6] = {{{&g_11,&g_11,&l_1247,&l_1247,&g_11,&l_1247},{(void*)0,&g_11,(void*)0,&l_1247,&l_1247,&l_1247},{&l_1247,(void*)0,(void*)0,&l_1247,&g_11,&l_1247},{&l_1247,&l_1247,&l_1247,&l_1247,&l_1247,&l_1247},{&l_1247,&l_1247,&l_1247,&l_1247,&l_1247,&l_1247}}};
        int8_t **l_1491 = &l_1492[0][2][5];
        int i, j, k;
        for (g_1049 = 0; (g_1049 < 22); g_1049 = safe_add_func_int64_t_s_s(g_1049, 7))
        { /* block id: 481 */
            int32_t **l_1188[3];
            int64_t **l_1202 = (void*)0;
            int64_t **l_1203[1];
            int8_t *l_1208 = &g_11;
            uint32_t l_1226 = 8UL;
            int i;
            for (i = 0; i < 3; i++)
                l_1188[i] = &g_547;
            for (i = 0; i < 1; i++)
                l_1203[i] = &g_532[3];
            if ((****g_1043))
                break;
            (*l_630) = (*g_101);
        }
        for (g_73 = 0; (g_73 != 52); g_73 = safe_add_func_uint64_t_u_u(g_73, 4))
        { /* block id: 536 */
            int16_t l_1319 = (-5L);
            int32_t l_1320 = 0xEBDE5F11L;
            int32_t l_1322[5][2] = {{0xACF96C72L,0xACF96C72L},{0x2DC86882L,0xACF96C72L},{0xACF96C72L,0x2DC86882L},{0xACF96C72L,0xACF96C72L},{0x2DC86882L,0xACF96C72L}};
            uint8_t *l_1335 = &g_73;
            uint8_t l_1336 = 0x81L;
            int16_t **l_1340 = (void*)0;
            int16_t **l_1341 = &l_1311;
            uint32_t ***l_1360 = (void*)0;
            uint32_t **** const *l_1377 = (void*)0;
            uint16_t l_1408 = 0xE892L;
            int8_t *l_1426[9][6] = {{&l_1247,&l_1247,&l_1247,&l_1247,&g_11,&l_1247},{&l_1247,&g_11,&l_1247,&l_1247,&l_1247,&l_1247},{&l_1247,&l_1247,&l_1247,&l_1247,&l_1247,&l_1247},{&l_1247,&g_11,&l_1247,&l_1247,&g_11,&l_1247},{&l_1247,&l_1247,&l_1247,&l_1247,&l_1247,&l_1247},{&l_1247,&l_1247,&l_1247,&l_1247,&g_11,&l_1247},{&l_1247,&g_11,&l_1247,&l_1247,&l_1247,&l_1247},{&l_1247,&l_1247,&l_1247,&l_1247,&l_1247,&l_1247},{&l_1247,&g_11,&l_1247,&l_1247,&g_11,&l_1247}};
            int8_t ** const l_1425[5] = {&l_1426[7][4],&l_1426[7][4],&l_1426[7][4],&l_1426[7][4],&l_1426[7][4]};
            volatile int32_t ** volatile **l_1452 = &g_1044;
            union U0 *l_1455 = &g_1433;
            uint64_t *l_1481 = (void*)0;
            int i, j;
            for (g_1283 = 0; (g_1283 > 5); g_1283 = safe_add_func_uint8_t_u_u(g_1283, 4))
            { /* block id: 539 */
                int8_t l_1318 = 0xA9L;
                int32_t l_1321 = (-1L);
                int32_t l_1323 = 1L;
                int32_t l_1324 = 0xDD18872AL;
                float *l_1337 = (void*)0;
                (*g_1338) = ((((((((*l_664) = (((g_207 < ((p_26 ^ (safe_sub_func_uint64_t_u_u((g_1298.f0 ^ 0xB2B1L), ((((l_1325--) ^ (g_229[4][3][3] < (0xA8F2L ^ (safe_sub_func_uint16_t_u_u((!((g_1300++) >= l_1321)), ((l_1324 && (safe_rshift_func_int8_t_s_s(((&p_27 == (void*)0) <= l_1207[2]), 5))) <= g_545)))))) && 4UL) < g_227)))) , g_334)) , l_1335) != l_1335)) && l_1323) < p_26) , l_1207[3]) == p_26) , l_1336) , (*g_1074));
            }
            if (l_1320)
                continue;
            if ((((*l_1341) = (l_664 = (g_1339 = &g_262))) == l_1342))
            { /* block id: 549 */
                uint32_t l_1352 = 0x2D7DC91EL;
                uint32_t ****l_1361 = &l_1360;
                int32_t l_1374 = 0L;
                int8_t *l_1375[5] = {&l_1247,&l_1247,&l_1247,&l_1247,&l_1247};
                int32_t l_1376[5][7];
                uint64_t *l_1401 = (void*)0;
                float *l_1404[3][6][4] = {{{(void*)0,&g_221,&l_1181,&g_221},{&g_221,(void*)0,&l_1181,&l_1181},{&g_221,&g_221,&g_206,&l_1181},{&l_1181,(void*)0,&l_1181,&g_206},{&l_1181,&g_206,&g_206,&l_1181},{&g_221,&g_206,&l_1181,&g_206}},{{&g_206,(void*)0,&l_1181,&l_1181},{&g_221,&g_221,&g_206,&l_1181},{&l_1181,(void*)0,&l_1181,&g_206},{&l_1181,&g_206,&g_206,&l_1181},{&g_221,&g_206,&l_1181,&g_206},{&g_206,(void*)0,&l_1181,&l_1181}},{{&g_221,&g_221,&g_206,&l_1181},{&l_1181,(void*)0,&l_1181,&g_206},{&l_1181,&g_206,&g_206,&l_1181},{&g_221,&g_206,&l_1181,&g_206},{&g_206,(void*)0,&l_1181,&l_1181},{&g_221,&g_221,&g_206,&l_1181}}};
                float **l_1403[5] = {&l_1404[1][2][0],&l_1404[1][2][0],&l_1404[1][2][0],&l_1404[1][2][0],&l_1404[1][2][0]};
                int i, j, k;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 7; j++)
                        l_1376[i][j] = 0x9827C3AAL;
                }
                (*l_1156) &= (((18446744073709551610UL < ((safe_mod_func_int8_t_s_s(((*p_28) , (safe_mod_func_uint16_t_u_u((+((((safe_mul_func_uint8_t_u_u((((((*p_28) = (((safe_mod_func_int16_t_s_s((p_27 != (void*)0), ((l_1352 & 0xA3L) | (l_1322[2][0] &= (safe_div_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((((l_1207[0] ^ (!(safe_rshift_func_uint8_t_u_u((((&g_934[4][6] != ((*l_1361) = l_1360)) || (*g_228)) , 0UL), 2)))) , 0x9E93E42FL) <= 0x96E11944L), (*g_1339))), 0xCEL)))))) != l_1207[2]) , (-5L))) ^ 1UL) & 0x90L) >= g_923[1]), 0xCFL)) > l_1325) > l_1336) != l_1352)), p_26))), 0x9BL)) == 9UL)) || (*p_28)) < l_1325);
                if ((*p_28))
                    continue;
                if (((safe_div_func_int8_t_s_s((l_1376[3][2] &= (safe_lshift_func_uint16_t_u_s((1L | ((((safe_sub_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(p_26, 15)), (l_1361 != (p_26 , (void*)0)))) && ((*g_78) , l_1207[3])) , ((safe_mod_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u((p_26 & (l_1374 = 1UL)), g_495)), p_26)) >= 0xC6L)) & l_1325)), (*g_1339)))), p_26)) <= g_212))
                { /* block id: 557 */
                    uint16_t l_1388 = 0xD2FAL;
                    uint16_t l_1394 = 0UL;
                    for (p_26 = 0; (p_26 <= 2); p_26 += 1)
                    { /* block id: 560 */
                        l_1378 = l_1377;
                    }
                    for (l_864 = 6; (l_864 <= 28); l_864 = safe_add_func_uint8_t_u_u(l_864, 6))
                    { /* block id: 565 */
                        float l_1384[7] = {0x3.F0D13Fp+66,0x3.F0D13Fp+66,0x3.F0D13Fp+66,0x3.F0D13Fp+66,0x3.F0D13Fp+66,0x3.F0D13Fp+66,0x3.F0D13Fp+66};
                        int32_t l_1385 = (-1L);
                        uint16_t *l_1391 = &l_1388;
                        int i;
                        if ((*p_27))
                            break;
                        (***g_1044) = ((safe_rshift_func_uint8_t_u_u(p_26, 3)) < (l_1385 || ((safe_sub_func_int16_t_s_s((*g_1339), ((*l_1391) = (((*l_1201) = l_1388) < (safe_sub_func_uint64_t_u_u(((g_934[3][4] = (void*)0) == &g_935), (l_1385 |= (l_1322[1][1] = ((0x8CL && ((void*)0 != &g_1339)) || p_26))))))))) && p_26)));
                    }
                    for (l_1172 = 26; (l_1172 <= 9); l_1172 = safe_sub_func_int64_t_s_s(l_1172, 5))
                    { /* block id: 576 */
                        uint64_t **l_1402 = &l_1401;
                        l_1394++;
                        l_1240[2][4][7] = (((((safe_div_func_int16_t_s_s(0x776EL, (safe_mul_func_int16_t_s_s((l_1376[3][2] ^= (p_26 < p_26)), ((l_1207[2] , l_1201) == ((*l_1402) = l_1401)))))) , l_1403[4]) != &g_1074) , (*g_1074)) <= g_338);
                        (*l_1156) |= ((*g_547) |= 0L);
                    }
                }
                else
                { /* block id: 584 */
                    int32_t l_1407 = 0x2CA96FA1L;
                    for (g_12 = 0; (g_12 <= 23); ++g_12)
                    { /* block id: 587 */
                        --l_1408;
                    }
                }
                (*l_630) = func_52(p_26);
            }
            else
            { /* block id: 592 */
                uint8_t *l_1421 = (void*)0;
                int32_t l_1434[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_1434[i] = 0x5387C623L;
                for (g_338 = 0; (g_338 != 40); g_338 = safe_add_func_int32_t_s_s(g_338, 5))
                { /* block id: 595 */
                    uint8_t *l_1419 = &g_863;
                    uint8_t **l_1420[5][4] = {{&l_1419,(void*)0,&l_1419,&l_1335},{&l_1419,(void*)0,&l_1335,&l_1419},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_1419,&l_1419,(void*)0,&l_1335},{(void*)0,&l_1419,&l_1335,&l_1419}};
                    const int32_t l_1422[6] = {0L,0L,0L,0L,0L,0L};
                    uint8_t l_1424 = 252UL;
                    float *l_1430 = &g_206;
                    uint16_t *l_1441 = &g_1302[2].f0;
                    int32_t l_1442 = 0L;
                    uint32_t *l_1443[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j;
                    (*l_1430) = (safe_mul_func_float_f_f(((*g_1074) > (safe_mul_func_float_f_f((((safe_mul_func_float_f_f(((((void*)0 != &g_1339) , l_1419) == (l_1421 = &l_1299)), g_1175)) <= l_1422[4]) <= (!l_1424)), ((void*)0 != l_1425[0])))), g_338));
                    if ((safe_mul_func_uint8_t_u_u(((g_1433 , l_1434[2]) < (safe_div_func_int32_t_s_s((*g_547), p_26))), ((l_1240[2][4][7] , ((((*l_1156) &= ((((l_1434[2] | ((*g_1339) = ((safe_div_func_int64_t_s_s(((*l_1201) |= ((g_1283 = (p_26 , ((*l_1121) |= (l_1442 = (((*l_1441) &= ((void*)0 == l_1341)) > (-1L)))))) || 0x8EEE09DAL)), 0x578D7F208324C8CBLL)) > p_26))) != p_26) ^ g_1433.f0) && p_26)) , l_1336) , 0xE46B6501L)) == g_545))))
                    { /* block id: 605 */
                        uint64_t l_1451 = 0x24AA1E7B0DE9A1BELL;
                        union U0 **l_1456 = &l_1455;
                        (**g_1044) = ((((void*)0 == l_1342) || (~(safe_rshift_func_uint8_t_u_s(((65535UL && (l_1207[2] & (8L == ((safe_unary_minus_func_uint32_t_u(0xA80A3643L)) && (((((l_1448 = &g_493[4]) == (void*)0) >= (safe_mod_func_uint16_t_u_u(0UL, 0x44BAL))) < l_1451) < l_1207[2]))))) , l_1320), 0)))) , (void*)0);
                        l_1452 = &g_1044;
                        (*g_1453) = &g_859[1];
                        (*l_1456) = l_1455;
                    }
                    else
                    { /* block id: 611 */
                        uint32_t l_1457 = 0UL;
                        ++l_1457;
                        if (l_1434[0])
                            break;
                        (*l_1156) = (****g_1043);
                    }
                    return (*g_101);
                }
                (*g_1461) = p_27;
                for (g_1300 = 0; (g_1300 < 3); g_1300++)
                { /* block id: 621 */
                    int64_t ** const l_1470 = &g_532[3];
                    int32_t l_1485[5][6] = {{0x095EC90BL,0x1715410BL,0x1715410BL,0x095EC90BL,0x095EC90BL,0x1715410BL},{0x095EC90BL,0x095EC90BL,0x1715410BL,0x1715410BL,0x095EC90BL,0x095EC90BL},{0x095EC90BL,0x1715410BL,0x1715410BL,0x095EC90BL,0x095EC90BL,0x1715410BL},{0x095EC90BL,0x095EC90BL,0x1715410BL,0x1715410BL,0x095EC90BL,0x095EC90BL},{0x095EC90BL,0x1715410BL,0x1715410BL,0x095EC90BL,0x095EC90BL,0x1715410BL}};
                    int i, j;
                    for (g_548 = 0; (g_548 >= 9); g_548 = safe_add_func_int64_t_s_s(g_548, 7))
                    { /* block id: 624 */
                        uint64_t *l_1471 = &g_12;
                        int32_t l_1482 = (-7L);
                        uint16_t *l_1483 = &l_1408;
                        const uint64_t l_1484 = 18446744073709551606UL;
                        l_1485[0][3] = ((safe_div_func_uint16_t_u_u(((((*l_1471) = (safe_rshift_func_int8_t_s_s((0xA1D2L | (((**l_1341) = (&l_1201 != l_1470)) , 65528UL)), 1))) >= 1L) | (((*l_1483) = ((0x1BB5148BAB15DDE4LL | (safe_rshift_func_uint16_t_u_u((5L < (g_11 ^= (safe_lshift_func_uint8_t_u_s((safe_add_func_uint32_t_u_u(((*g_1454) , ((+(--g_1433.f0)) || ((*l_1121) = ((((l_1481 != (void*)0) , l_1434[2]) > l_1482) , 1UL)))), 4294967295UL)), 0)))), g_83))) | l_1482)) < l_1484)), p_26)) & l_1434[6]);
                    }
                }
            }
        }
        for (g_863 = 0; (g_863 < 48); g_863 = safe_add_func_uint16_t_u_u(g_863, 1))
        { /* block id: 638 */
            (*p_28) = (l_1488[4] == (void*)0);
        }
        for (g_212 = (-23); (g_212 == 19); g_212++)
        { /* block id: 643 */
            int8_t ***l_1493 = (void*)0;
            int8_t ***l_1494 = &l_1491;
            int8_t **l_1496 = &l_1492[0][3][0];
            int8_t ***l_1495 = &l_1496;
            int32_t l_1500 = 0L;
            uint8_t *l_1512[10] = {(void*)0,&l_1299,&l_1299,(void*)0,&g_863,(void*)0,&l_1299,&l_1299,(void*)0,&g_863};
            uint8_t **l_1511[10][9][2] = {{{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],(void*)0},{&l_1512[0],&l_1512[0]},{&l_1512[8],&l_1512[0]},{&l_1512[4],&l_1512[8]},{(void*)0,&l_1512[7]},{(void*)0,&l_1512[8]},{&l_1512[4],&l_1512[0]}},{{&l_1512[8],&l_1512[0]},{&l_1512[0],(void*)0},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],(void*)0},{&l_1512[0],&l_1512[0]},{&l_1512[8],&l_1512[0]},{&l_1512[4],&l_1512[8]},{(void*)0,&l_1512[7]}},{{(void*)0,&l_1512[8]},{&l_1512[4],&l_1512[0]},{&l_1512[7],&l_1512[4]},{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{(void*)0,&l_1512[4]},{&l_1512[7],&l_1512[0]}},{{&l_1512[0],&l_1512[7]},{(void*)0,(void*)0},{(void*)0,&l_1512[7]},{&l_1512[0],&l_1512[0]},{&l_1512[7],&l_1512[4]},{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]}},{{(void*)0,&l_1512[4]},{&l_1512[7],&l_1512[0]},{&l_1512[0],&l_1512[7]},{(void*)0,(void*)0},{(void*)0,&l_1512[7]},{&l_1512[0],&l_1512[0]},{&l_1512[7],&l_1512[4]},{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]}},{{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{(void*)0,&l_1512[4]},{&l_1512[7],&l_1512[0]},{&l_1512[0],&l_1512[7]},{(void*)0,(void*)0},{(void*)0,&l_1512[7]},{&l_1512[0],&l_1512[0]},{&l_1512[7],&l_1512[4]}},{{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{(void*)0,&l_1512[4]},{&l_1512[7],&l_1512[0]},{&l_1512[0],&l_1512[7]},{(void*)0,(void*)0},{(void*)0,&l_1512[7]}},{{&l_1512[0],&l_1512[0]},{&l_1512[7],&l_1512[4]},{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{(void*)0,&l_1512[4]},{&l_1512[7],&l_1512[0]},{&l_1512[0],&l_1512[7]}},{{(void*)0,(void*)0},{(void*)0,&l_1512[7]},{&l_1512[0],&l_1512[0]},{&l_1512[7],&l_1512[4]},{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]},{(void*)0,&l_1512[4]}},{{&l_1512[7],&l_1512[0]},{&l_1512[0],&l_1512[7]},{(void*)0,(void*)0},{(void*)0,&l_1512[7]},{&l_1512[0],&l_1512[0]},{&l_1512[7],&l_1512[4]},{(void*)0,&l_1512[0]},{&l_1512[0],&l_1512[0]},{&l_1512[0],&l_1512[0]}}};
            int i, j, k;
            (*l_1495) = ((*l_1494) = l_1491);
            if ((safe_unary_minus_func_uint8_t_u(((safe_mod_func_uint16_t_u_u(((*l_1156) != l_1500), (g_863 ^ (safe_rshift_func_uint8_t_u_u((((&g_1339 == (((*g_935) = (safe_rshift_func_int8_t_s_s(((l_1500 > (*g_1074)) , l_1207[1]), ((l_1248 ^ (((safe_mod_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s((l_1511[3][4][1] != (void*)0), l_1500)) ^ g_926[3]), g_229[2][4][2])) && l_1240[2][4][7]) < (*g_1339))) != g_83)))) , (void*)0)) < g_12) , l_1500), 3))))) , l_1500))))
            { /* block id: 647 */
                int64_t l_1514 = 1L;
                float *l_1518[2][9][6] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1181,(void*)0,&l_1181}},{{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181},{&l_1181,(void*)0,&l_1181,&l_1181,(void*)0,&l_1181}}};
                int i, j, k;
                --l_1515[3][0];
                l_1519 = 0x5.4p-1;
                if ((*p_28))
                    break;
            }
            else
            { /* block id: 651 */
                uint8_t l_1528 = 0x54L;
                (*g_566) = (void*)0;
                (**g_1044) = ((((((g_12 , ((**l_1495) = &g_11)) != (void*)0) == ((l_1528 = (safe_div_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_s(g_559.f0, (p_26 , 0L))) , l_1500) , p_26), (safe_lshift_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((*g_1339) = (((*p_27) | 0x14083822L) < (-1L))), g_334)), 14))))) && 7L)) & (*p_28)) > 4294967295UL) , (void*)0);
            }
            (*g_1461) = func_52((p_26 > ((((safe_lshift_func_uint16_t_u_u(0UL, g_862[1].f0)) , g_1531) != (g_1535 = g_1535)) , ((safe_div_func_uint16_t_u_u(((((void*)0 == p_27) ^ ((safe_div_func_uint64_t_u_u((((*l_664) ^= (l_1500 >= 0x0215847907722C0FLL)) && g_73), g_1541[3])) , 0x31C4L)) > 18446744073709551610UL), l_1325)) , p_26))));
        }
    }
    return (*g_301);
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_78 g_12 g_85 g_3 g_10 g_420.f0 g_207 g_73 g_11 g_301 g_87 g_93 g_235 g_307 g_229 g_262 g_212 g_227 g_334 g_338 g_228 g_258 g_379 g_83 g_77 g_420 g_492 g_495
 * writes: g_9 g_3 g_10 g_12 g_85 g_87 g_11 g_73 g_212 g_307 g_262 g_83 g_334 g_338 g_221 g_206 g_211 g_493 g_207 g_93
 */
static uint16_t  func_29(int32_t * p_30)
{ /* block id: 211 */
    float l_512 = 0x7.8A8936p+90;
    int32_t l_516 = 0x708BA00FL;
    int64_t l_521 = (-5L);
    int32_t **l_522 = &g_87[0][0][2];
    int64_t **l_523 = &g_211;
    int64_t **l_524 = &g_211;
    int64_t **l_525 = &g_211;
    int64_t **l_526 = &g_211;
    int64_t **l_527 = &g_211;
    int64_t *l_528 = &g_212;
    int64_t **l_529 = &g_211;
    int64_t **l_530 = &g_211;
    int64_t **l_531 = &g_211;
    int64_t *l_533 = &g_212;
    uint32_t *l_538 = &g_338;
    uint32_t **l_539 = &l_538;
    uint32_t **l_540 = (void*)0;
    uint32_t *l_542[1][2][4];
    uint32_t **l_541 = &l_542[0][1][2];
    const uint32_t *l_544[5][5] = {{&g_545,&g_545,&g_545,&g_545,&g_545},{&g_545,&g_545,&g_545,&g_545,&g_545},{&g_545,&g_545,&g_545,&g_545,&g_545},{&g_545,&g_545,&g_545,&g_545,&g_545},{&g_545,&g_545,&g_545,&g_545,&g_545}};
    const uint32_t **l_543 = &l_544[4][4];
    int16_t l_546 = 1L;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 4; k++)
                l_542[i][j][k] = &g_338;
        }
    }
    for (g_9 = 0; (g_9 <= 9); g_9 += 1)
    { /* block id: 214 */
        uint32_t l_515 = 0UL;
        (*g_78) = ((*p_30) = 0xDD61B1F7L);
        for (g_12 = 0; (g_12 <= 1); g_12 += 1)
        { /* block id: 219 */
            for (g_85 = 0; (g_85 <= 1); g_85 += 1)
            { /* block id: 222 */
                int i, j, k;
                if ((*p_30))
                    break;
                (*g_78) = ((*p_30) = (safe_mul_func_uint16_t_u_u((&g_493[(g_12 + 7)] != (void*)0), (g_10 , l_515))));
            }
            return l_516;
        }
    }
    (*p_30) = ((safe_sub_func_int16_t_s_s(7L, (safe_lshift_func_int16_t_s_s(l_516, 15)))) <= l_521);
    (*l_522) = p_30;
    (*l_522) = func_31(p_30, g_420.f0);
    return g_262;
}


/* ------------------------------------------ */
/* 
 * reads : g_85 g_207 g_9 g_3 g_12 g_73 g_11 g_301 g_212 g_87 g_93 g_235 g_307 g_229 g_262 g_227 g_78 g_10 g_334 g_338 g_228 g_258 g_379 g_83 g_77 g_420 g_492 g_495
 * writes: g_11 g_3 g_73 g_87 g_212 g_12 g_307 g_262 g_83 g_85 g_334 g_338 g_10 g_221 g_206 g_211 g_493 g_207 g_93
 */
static int32_t * func_31(int32_t * p_32, int16_t  p_33)
{ /* block id: 108 */
    const uint64_t l_266 = 1UL;
    int32_t l_267 = (-1L);
    float *l_302[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int16_t l_306 = 0xFADBL;
    uint64_t l_329 = 0x29830DE378C8CA38LL;
    int32_t l_371 = 0xF539C1A3L;
    int32_t **l_392 = &g_87[0][1][7];
    int32_t ***l_391 = &l_392;
    int64_t l_406[6][2] = {{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}};
    uint16_t l_417 = 0xF60BL;
    int32_t l_443 = 0xD13061C2L;
    int32_t l_447[9][2][10] = {{{(-10L),0x498FA7BCL,0x89448A1BL,0x9FFBFB0BL,0x498FA7BCL,1L,0x8C14D8FBL,0x5E89D6E7L,(-1L),0xF7417FCEL},{(-4L),0x498FA7BCL,(-3L),0xD6D75B78L,1L,2L,0x300EDCB5L,0x300EDCB5L,2L,1L}},{{(-7L),(-1L),(-1L),(-7L),0x27660146L,0xF7417FCEL,(-4L),(-10L),0x89448A1BL,(-7L)},{(-10L),1L,1L,0x27660146L,(-4L),0x8C14D8FBL,(-1L),(-4L),0x89448A1BL,0xF7417FCEL}},{{0x48BC0D39L,1L,2L,(-7L),0x5E89D6E7L,(-3L),0x5E89D6E7L,(-7L),2L,1L},{0x498FA7BCL,0x8C14D8FBL,0xF7417FCEL,0xD6D75B78L,0x9FFBFB0BL,0x27660146L,0xD6D75B78L,(-10L),(-1L),0x498FA7BCL}},{{0xF7417FCEL,0x48BC0D39L,0x8C14D8FBL,0x9FFBFB0BL,1L,0x27660146L,(-1L),0x48BC0D39L,1L,1L},{0x498FA7BCL,(-7L),(-3L),0x48BC0D39L,0x48BC0D39L,(-3L),(-7L),0x498FA7BCL,0L,0x5E89D6E7L}},{{0x48BC0D39L,(-1L),0x27660146L,1L,0x9FFBFB0BL,0x8C14D8FBL,0x48BC0D39L,0xF7417FCEL,(-10L),0x48BC0D39L},{(-10L),0x27660146L,0x74D289BEL,6L,0x27660146L,0x1AFF9783L,(-3L),0x9FFBFB0BL,(-3L),0x1AFF9783L}},{{(-1L),0x89448A1BL,1L,0x89448A1BL,(-1L),0x48BC0D39L,(-10L),0xF7417FCEL,0x48BC0D39L,0x8C14D8FBL},{0x8C14D8FBL,0L,(-3L),0x8C14D8FBL,0x74D289BEL,0x8317209AL,(-1L),0x01157953L,6L,0x8C14D8FBL}},{{0x01157953L,0x8C14D8FBL,0x1AFF9783L,0x74D289BEL,(-1L),0L,0L,(-1L),0x74D289BEL,0x1AFF9783L},{1L,1L,0x48BC0D39L,(-1L),0x27660146L,1L,0x9FFBFB0BL,0x8C14D8FBL,0x48BC0D39L,0xF7417FCEL}},{{0x89448A1BL,(-3L),0x8317209AL,0x9FFBFB0BL,6L,1L,0x9FFBFB0BL,0x01157953L,0L,0x89448A1BL},{0x1AFF9783L,1L,0L,6L,0xF7417FCEL,6L,0L,1L,0x1AFF9783L,0x8317209AL}},{{0x27660146L,0x8C14D8FBL,1L,1L,(-10L),1L,(-1L),0x89448A1BL,0x300EDCB5L,0x9FFBFB0BL},{(-10L),0L,1L,1L,6L,2L,(-10L),0x1AFF9783L,0x1AFF9783L,(-10L)}}};
    uint16_t l_448[10][4][6] = {{{0xCBB8L,0x72A4L,0xC6FEL,0x131CL,0xA0E1L,0xDCACL},{0xCBB8L,0x2A46L,0x131CL,65535UL,0UL,65529UL},{8UL,65527UL,0x78ABL,0x2676L,0UL,0UL},{5UL,0x87BEL,0xA0E1L,1UL,0UL,0xF3A3L}},{{0x6309L,65535UL,0xC19CL,65535UL,0x352BL,65535UL},{5UL,0xA0E1L,1UL,0x72A4L,0x96DFL,0x6309L},{0UL,5UL,2UL,0xEFA7L,0UL,65527UL},{65535UL,0x6309L,65532UL,0x56ADL,0x8895L,65535UL}},{{1UL,0x352BL,0xDCACL,0x352BL,1UL,0UL},{1UL,0xDCACL,5UL,65527UL,0xF3A3L,0x7A03L},{0x56ADL,8UL,0UL,0xDCACL,0x87BEL,0x7A03L},{0x7A03L,65535UL,5UL,1UL,0xC19CL,0UL}},{{0x87BEL,65535UL,0xDCACL,2UL,5UL,65535UL},{0xF396L,0UL,65532UL,0x807EL,65535UL,65527UL},{65529UL,1UL,2UL,0xC6FEL,1UL,0x6309L},{0x807EL,0x2676L,1UL,0UL,1UL,65535UL}},{{65527UL,1UL,0xC19CL,8UL,65535UL,0xF3A3L},{0UL,1UL,65535UL,0xA0E1L,65535UL,65535UL},{1UL,65535UL,65535UL,1UL,0xEFA7L,0xF396L},{0x8895L,65535UL,0x3AACL,65535UL,0x96DFL,1UL}},{{0xCBB8L,0UL,0xF3A3L,8UL,0x96DFL,0xAC42L},{65535UL,65535UL,0xC19CL,0x345EL,0xEFA7L,5UL},{0xDCACL,65535UL,0UL,0x2A46L,65535UL,65527UL},{2UL,1UL,1UL,1UL,65532UL,0UL}},{{65535UL,0UL,0x6309L,0xC6FEL,1UL,0x6D59L},{0x345EL,0UL,0x131CL,1UL,0x352BL,1UL},{0xF3A3L,0UL,1UL,65535UL,0x6D59L,8UL},{0x2A46L,0x345EL,0x352BL,1UL,1UL,1UL}},{{65535UL,0x6D59L,65535UL,0x4DF0L,1UL,1UL},{0UL,0x96DFL,0xCBB8L,0UL,0UL,0x7A03L},{0xEFA7L,2UL,5UL,0UL,2UL,0x4DF0L},{0UL,1UL,0x8895L,0x4DF0L,0UL,0x3AACL}},{{65535UL,5UL,65529UL,1UL,65535UL,0xCBB8L},{0x2A46L,0x2676L,1UL,65535UL,0x8895L,0x352BL},{0xF3A3L,1UL,0xF396L,1UL,65527UL,2UL},{0x345EL,65535UL,65532UL,0xC6FEL,5UL,0UL}},{{65535UL,0x87BEL,5UL,1UL,6UL,0UL},{2UL,0UL,0x87BEL,0x2A46L,0x2A46L,0x87BEL},{0xDCACL,0xDCACL,0UL,0x345EL,65527UL,65535UL},{65535UL,0xC6FEL,2UL,8UL,65535UL,0UL}}};
    uint16_t l_500 = 0UL;
    int32_t l_507[1][8][5] = {{{(-1L),0xE60D9A7CL,0xE60D9A7CL,(-1L),(-1L)},{(-1L),0xE60D9A7CL,0xA27EBD4CL,3L,(-1L)},{9L,(-1L),(-1L),(-10L),0xA3A3F106L},{0xA27EBD4CL,(-8L),0xB57AD20DL,3L,3L},{(-8L),9L,(-8L),(-1L),0x0589F3EAL},{(-8L),0x6DE73A0EL,(-10L),9L,(-1L)},{0xA27EBD4CL,0xB57AD20DL,(-1L),0xE60D9A7CL,0x79B08DC5L},{9L,3L,(-10L),(-1L),(-10L)}}};
    int8_t l_509 = 0L;
    int i, j, k;
    if ((g_85 == l_266))
    { /* block id: 109 */
        int32_t l_283 = 0xE6F25B05L;
        int64_t *l_284[9][8][2] = {{{&g_212,(void*)0},{&g_212,&g_212},{(void*)0,&g_212},{(void*)0,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,&g_212}},{{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212}},{{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0}},{{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212}},{{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212}},{{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0}},{{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212}},{{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212}},{{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0},{&g_212,&g_212},{&g_212,&g_212},{&g_212,(void*)0}}};
        int32_t l_288[1][7][6];
        uint16_t l_296 = 0UL;
        int16_t *l_327 = &g_262;
        uint16_t *l_328 = &g_83;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 7; j++)
            {
                for (k = 0; k < 6; k++)
                    l_288[i][j][k] = 1L;
            }
        }
        for (p_33 = 0; (p_33 <= 1); p_33 += 1)
        { /* block id: 112 */
            const uint32_t l_278 = 0x271D079FL;
            int64_t *l_285[5][3][10] = {{{&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0,&g_212,&g_212,&g_212,&g_212},{&g_212,&g_212,(void*)0,&g_212,&g_212,(void*)0,&g_212,(void*)0,&g_212,&g_212},{&g_212,&g_212,(void*)0,&g_212,&g_212,&g_212,(void*)0,&g_212,&g_212,&g_212}},{{&g_212,(void*)0,&g_212,&g_212,&g_212,(void*)0,(void*)0,&g_212,&g_212,&g_212},{&g_212,(void*)0,&g_212,(void*)0,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0},{&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0,(void*)0,(void*)0,&g_212}},{{&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0,(void*)0,&g_212,(void*)0},{&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0,&g_212,(void*)0,&g_212},{&g_212,&g_212,(void*)0,(void*)0,&g_212,&g_212,&g_212,(void*)0,&g_212,&g_212}},{{&g_212,&g_212,(void*)0,&g_212,&g_212,(void*)0,&g_212,&g_212,&g_212,(void*)0},{(void*)0,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0,&g_212,&g_212,(void*)0},{&g_212,&g_212,&g_212,&g_212,(void*)0,&g_212,&g_212,&g_212,&g_212,&g_212}},{{&g_212,(void*)0,&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0},{(void*)0,(void*)0,(void*)0,&g_212,&g_212,&g_212,(void*)0,(void*)0,(void*)0,&g_212},{&g_212,&g_212,&g_212,&g_212,&g_212,&g_212,(void*)0,&g_212,(void*)0,&g_212}}};
            int8_t *l_286 = (void*)0;
            int8_t *l_287 = &g_11;
            int32_t l_294 = 0x53C0D599L;
            int32_t l_295[7] = {5L,5L,5L,5L,5L,5L,5L};
            int i, j, k;
            l_267 &= 8L;
            (*p_32) = (l_288[0][4][4] ^= (safe_mul_func_int8_t_s_s(((*l_287) = (safe_add_func_int16_t_s_s(l_266, (safe_sub_func_uint32_t_u_u(p_33, (safe_div_func_uint64_t_u_u((g_207 <= ((safe_rshift_func_int8_t_s_u(0x42L, l_278)) & (((((safe_add_func_int8_t_s_s((safe_add_func_uint8_t_u_u((p_33 & ((g_9 != ((g_3 , 0x2E37C5671B14C737LL) & p_33)) , l_278)), l_283)), 0x1FL)) , l_278) , l_284[4][5][1]) == l_285[2][2][8]) , g_12))), p_33))))))), g_73)));
            for (g_73 = 0; (g_73 <= 1); g_73 += 1)
            { /* block id: 119 */
                int32_t *l_289 = &l_267;
                int32_t *l_290 = &l_288[0][0][0];
                int32_t *l_291 = &l_288[0][4][4];
                int32_t *l_292 = &l_288[0][4][4];
                int32_t *l_293[4];
                int32_t **l_299 = &l_290;
                int32_t **l_300 = (void*)0;
                int i;
                for (i = 0; i < 4; i++)
                    l_293[i] = &l_267;
                ++l_296;
                (*g_301) = ((*l_299) = func_52(g_11));
                for (g_212 = 0; (g_212 <= 1); g_212 += 1)
                { /* block id: 125 */
                    int i, j, k;
                    l_295[0] ^= (g_87[(p_33 + 1)][g_73][(g_73 + 2)] != l_302[3]);
                    for (g_12 = 0; (g_12 <= 1); g_12 += 1)
                    { /* block id: 129 */
                        int16_t *l_303[5];
                        int16_t *l_304 = &g_9;
                        int32_t l_305 = 0xC96F0BE2L;
                        int i;
                        for (i = 0; i < 5; i++)
                            l_303[i] = &g_262;
                        l_305 = (0x1.7p-1 <= (((g_93 , l_303[3]) == l_304) > g_235));
                        if (l_306)
                            continue;
                    }
                    g_87[g_73][p_33][g_73] = func_52(l_266);
                }
            }
        }
        g_307--;
        l_329 ^= ((safe_mul_func_float_f_f((l_306 == ((safe_mul_func_float_f_f((l_267 > p_33), (((safe_mod_func_uint16_t_u_u((((g_85 = ((*l_328) = (safe_rshift_func_int8_t_s_s(((((((((~(((*l_327) |= (((safe_sub_func_int32_t_s_s((0xCF886568L > (*p_32)), 1L)) , (safe_rshift_func_uint16_t_u_u((safe_add_func_uint64_t_u_u(((safe_mod_func_uint32_t_u_u((&p_33 == &p_33), (1L ^ l_283))) || p_33), g_9)), g_229[2][1][3]))) & p_33)) > g_307)) ^ l_283) > g_212) || g_227) , p_33) , 0L) , 0x3B1655B86115999DLL) <= l_296), p_33)))) | g_229[2][7][0]) , l_266), l_267)) , l_267) != 0xF.DFDF90p+6))) != (-0x4.4p+1))), g_227)) , (*g_78));
    }
    else
    { /* block id: 142 */
        int32_t l_331 = 0xA2385136L;
        uint32_t *l_332 = (void*)0;
        uint32_t *l_333 = &g_334;
        uint32_t *l_337[10] = {&g_338,&g_338,&g_338,&g_338,&g_338,&g_338,&g_338,&g_338,&g_338,&g_338};
        uint8_t *l_372[8] = {&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73};
        int32_t l_373 = 0x209B4CA9L;
        int8_t l_374 = (-8L);
        uint16_t *l_397 = (void*)0;
        int32_t l_414 = 0xAA21EBC4L;
        int32_t l_415[10][9] = {{0x63799410L,0xC7D45C31L,(-3L),0x37E3313DL,0xC52450ACL,0x137A1798L,0xA5F42B46L,0xF06F2B2EL,1L},{0x137A1798L,(-3L),0x2094E8B0L,(-9L),(-7L),0x31B0ECFDL,9L,0xA5F42B46L,9L},{9L,(-3L),1L,1L,(-3L),9L,(-7L),(-1L),0xF06F2B2EL},{9L,0xC7D45C31L,0L,(-5L),1L,(-7L),0xCCC43AD8L,0x31B0ECFDL,(-3L)},{0xC7D45C31L,0x6CD55B52L,(-3L),(-1L),0xA5F42B46L,(-5L),(-7L),0L,0xC52450ACL},{0x4132EE4AL,0x31B0ECFDL,3L,1L,1L,0x37E3313DL,9L,(-3L),(-3L)},{0xA5F42B46L,(-5L),3L,0x63799410L,3L,(-5L),0xA5F42B46L,0x4E34CD74L,(-9L)},{0xC52450ACL,0x21D4D259L,(-3L),0x4E34CD74L,(-1L),3L,0xC7D45C31L,(-5L),9L},{(-5L),0L,0L,0x14A22903L,0xF06F2B2EL,1L,1L,(-5L),0x137A1798L},{9L,0x2094E8B0L,0x137A1798L,9L,3L,(-5L),(-1L),(-1L),0L}};
        int64_t *l_430 = &g_212;
        uint32_t l_432 = 0x1D2A51F5L;
        float l_471 = 0x4.9p+1;
        uint32_t l_478[5] = {0xB2021810L,0xB2021810L,0xB2021810L,0xB2021810L,0xB2021810L};
        uint16_t l_484[9];
        int i, j;
        for (i = 0; i < 9; i++)
            l_484[i] = 7UL;
        if ((((~(l_331 < (--(*l_333)))) && ((((((g_338++) , (safe_lshift_func_uint8_t_u_s((safe_add_func_uint64_t_u_u(((((safe_mod_func_int16_t_s_s((safe_add_func_int32_t_s_s((safe_sub_func_int8_t_s_s(g_229[4][3][1], (((~8L) , (p_33 <= ((safe_lshift_func_int16_t_s_u((((((safe_rshift_func_uint16_t_u_u(((*g_228) > (safe_mod_func_int32_t_s_s((safe_sub_func_int8_t_s_s(l_267, (safe_rshift_func_uint16_t_u_u(0x0584L, ((safe_lshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u(0x12L, 7)), (g_85 = ((((l_331 ^= (+(safe_sub_func_int32_t_s_s(((l_371 = (l_267 >= l_266)) , (*p_32)), l_267)))) , &g_77) != &g_101) , l_329)))) , l_267))))), l_306))), 2)) >= 0xA030C4B6L) && 1L) ^ 0xF1L) == l_373), 6)) == l_266))) == l_373))), g_9)), p_33)) , g_258) , l_329) < 18446744073709551611UL), p_33)), g_207))) >= l_374) , p_33) , &l_374) != &g_11)) <= 0x0D0EL))
        { /* block id: 148 */
            int32_t l_384 = 2L;
            int32_t **l_390[1][6] = {{&g_87[0][1][2],&g_87[0][1][2],&g_87[0][1][2],&g_87[0][1][2],&g_87[0][1][2],&g_87[0][1][2]}};
            int32_t ***l_389 = &l_390[0][5];
            int64_t l_413 = (-1L);
            int8_t l_416 = (-1L);
            int i, j;
            (*g_78) = (safe_div_func_int64_t_s_s((safe_add_func_uint64_t_u_u((g_379[1] , 0xB96D85EFD67946D1LL), (safe_lshift_func_uint8_t_u_s((l_267 = (l_331 = (safe_lshift_func_int16_t_s_u(((p_33 , l_384) < (safe_mul_func_int32_t_s_s(l_331, (((safe_mod_func_uint64_t_u_u(((((l_373 = (((((0x0.4C0A7Ep-47 < (l_389 == (void*)0)) <= ((void*)0 == l_391)) >= p_33) > (-0x10.Cp+1)) >= 0x5.6p-1)) == 0x6.Ep-1) != g_73) , 18446744073709551611UL), p_33)) , g_229[2][1][3]) >= (*p_32))))), p_33)))), p_33)))), p_33));
            l_373 = (g_221 = p_33);
            l_413 = ((safe_mul_func_float_f_f((safe_div_func_float_f_f((&g_235 != (l_374 , l_397)), (safe_mul_func_float_f_f(0x2.301523p-51, ((g_206 = (l_331 , g_227)) >= (safe_sub_func_float_f_f(((safe_add_func_float_f_f((safe_add_func_float_f_f(0x0.B7DF14p-45, (((l_406[4][0] = g_83) != (((safe_div_func_int64_t_s_s((safe_add_func_uint32_t_u_u(((*l_333)--), (**g_77))), 0xC4B146E38B36EF01LL)) , 0xD.92F0E6p+28) > l_331)) < g_262))), 0xF.8176E4p+31)) >= g_229[3][4][4]), p_33))))))), g_93)) < g_11);
            --l_417;
        }
        else
        { /* block id: 160 */
            uint64_t *l_421 = &l_329;
            int64_t **l_431 = &g_211;
            int32_t *l_433 = (void*)0;
            int32_t *l_434 = &l_415[6][5];
            int32_t l_437 = 8L;
            int32_t l_438 = 1L;
            int32_t l_439 = 0x56E5ACCBL;
            int32_t l_440 = 0xA0F93E4DL;
            int32_t l_441 = 0x11B6E7ABL;
            int32_t l_442 = 5L;
            int32_t l_444 = 0x4D3F1518L;
            int32_t l_445 = 0x389E73C5L;
            int32_t l_446[4][8][2] = {{{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL}},{{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL}},{{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)}},{{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL},{0x208D26ECL,(-1L)},{0x208D26ECL,0x208D26ECL},{(-1L),0x208D26ECL}}};
            int i, j, k;
            if (((*l_434) ^= (((((((((*l_333) |= (((g_420 , ((((*l_421) |= (255UL > g_12)) >= (safe_div_func_int64_t_s_s((safe_mul_func_int16_t_s_s((safe_add_func_uint8_t_u_u(0xF6L, p_33)), (((safe_add_func_uint16_t_u_u(g_229[0][0][2], ((l_430 != ((*l_431) = l_421)) | (0xC7AFL >= g_12)))) <= 0x2B3F791AL) ^ 4294967294UL))), (*g_228)))) , (*p_32))) && 3UL) , l_432)) || 4294967295UL) | 0x565BL) <= l_432) >= g_11) , 0x7DL) > l_373) != g_212)))
            { /* block id: 165 */
                int32_t *l_435 = &g_3;
                int32_t *l_436[8];
                int i;
                for (i = 0; i < 8; i++)
                    l_436[i] = &g_3;
                ++l_448[0][2][4];
            }
            else
            { /* block id: 167 */
                int8_t l_463 = 0x03L;
                int32_t l_483[9][4] = {{(-1L),1L,0x283642A8L,(-1L)},{0x71348B7AL,0L,0x71348B7AL,0x283642A8L},{0x3EB240ADL,0L,0x10DD2896L,(-1L)},{0L,1L,1L,0L},{0x71348B7AL,(-1L),1L,0x283642A8L},{0L,0x3EB240ADL,0x10DD2896L,0x3EB240ADL},{0x3EB240ADL,1L,0x71348B7AL,0x3EB240ADL},{0x71348B7AL,0x3EB240ADL,0x283642A8L,0x283642A8L},{(-1L),(-1L),0x10DD2896L,0L}};
                int i, j;
                if ((0xF0L < g_83))
                { /* block id: 168 */
                    int32_t l_479 = 0x72854C02L;
                    for (l_443 = (-12); (l_443 >= 20); l_443++)
                    { /* block id: 171 */
                        if ((*p_32))
                            break;
                    }
                    for (l_445 = 0; (l_445 >= 5); l_445 = safe_add_func_uint32_t_u_u(l_445, 2))
                    { /* block id: 176 */
                        int8_t *l_476 = &g_11;
                        int32_t l_477[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_477[i] = (-7L);
                        (*p_32) = (((1L ^ (safe_lshift_func_uint16_t_u_s((safe_div_func_uint32_t_u_u(((((safe_sub_func_uint8_t_u_u(5UL, ((((((safe_div_func_int8_t_s_s(l_463, (safe_mod_func_int64_t_s_s(0x6177223381966F0FLL, ((safe_unary_minus_func_int16_t_s((safe_rshift_func_uint8_t_u_u(g_3, 5)))) ^ ((safe_div_func_uint32_t_u_u((((*g_78) >= p_33) & (((safe_rshift_func_int8_t_s_s(((*l_476) ^= 0x75L), g_12)) > 18446744073709551609UL) , g_227)), l_477[0])) == g_12)))))) , (*l_434)) , (*l_434)) <= (*g_228)) < (*l_434)) | l_478[3]))) != 0x09L) < l_479) != 4L), (*p_32))), g_85))) ^ 0x013C8EFAAF13C814LL) || 0xF5745F8BEFAD5F02LL);
                        if ((*l_434))
                            continue;
                        (**l_391) = p_32;
                    }
                }
                else
                { /* block id: 182 */
                    int32_t *l_480 = &l_414;
                    int32_t *l_481 = (void*)0;
                    int32_t *l_482[9] = {&l_414,&l_414,&l_414,&l_414,&l_414,&l_414,&l_414,&l_414,&l_414};
                    uint64_t *l_489 = &l_329;
                    uint8_t ***l_491 = &g_203;
                    uint8_t ****l_490 = &l_491;
                    int i;
                    ++l_484[1];
                    for (g_83 = 0; (g_83 != 44); g_83 = safe_add_func_uint16_t_u_u(g_83, 4))
                    { /* block id: 186 */
                        (*g_492) = (((*l_421) = (g_93 && ((void*)0 == l_489))) , l_490);
                        if (g_495)
                            break;
                    }
                }
            }
        }
    }
    for (g_207 = 1; (g_207 <= 4); g_207 += 1)
    { /* block id: 197 */
        int16_t l_505 = 0x2A10L;
        int32_t *l_506[6][10][4] = {{{&l_443,&l_371,&l_447[7][1][9],&l_371},{&l_447[7][1][9],&l_371,&l_443,&l_443},{&l_447[7][0][3],&l_447[7][0][3],&l_371,(void*)0},{&l_371,(void*)0,&l_447[4][1][0],&l_447[8][0][3]},{&l_371,&l_443,&l_371,&l_267},{&l_447[7][0][3],&l_447[8][0][3],&l_443,(void*)0},{&l_447[7][1][9],&l_443,&l_447[7][1][9],(void*)0},{&l_443,&l_447[8][0][3],&l_447[7][0][3],&l_267},{&l_371,&l_443,&l_371,&l_447[8][0][3]},{&l_447[4][1][0],(void*)0,&l_371,(void*)0}},{{&l_371,&l_447[7][0][3],&l_447[7][0][3],&l_443},{&l_443,&l_371,&l_447[7][1][9],&l_371},{&l_447[7][1][9],&l_371,&l_443,&l_443},{&l_447[7][0][3],&l_447[7][0][3],&l_371,&l_443},{&l_447[4][1][0],(void*)0,&l_443,(void*)0},{&l_447[4][1][0],&l_447[8][0][3],&l_447[7][1][9],&l_447[7][0][3]},{(void*)0,(void*)0,&l_371,&l_371},{(void*)0,&l_267,(void*)0,&l_371},{&l_371,(void*)0,(void*)0,&l_447[7][0][3]},{&l_447[7][1][9],&l_447[8][0][3],&l_447[4][1][0],(void*)0}},{{&l_443,(void*)0,&l_447[4][1][0],&l_443},{&l_447[7][1][9],&l_447[3][0][8],(void*)0,&l_447[8][0][3]},{&l_371,&l_443,(void*)0,&l_443},{(void*)0,&l_443,&l_371,&l_447[8][0][3]},{(void*)0,&l_447[3][0][8],&l_447[7][1][9],&l_443},{&l_447[4][1][0],(void*)0,&l_443,(void*)0},{&l_447[4][1][0],&l_447[8][0][3],&l_447[7][1][9],&l_447[7][0][3]},{(void*)0,(void*)0,&l_371,&l_371},{(void*)0,&l_267,(void*)0,&l_371},{&l_371,(void*)0,(void*)0,&l_447[7][0][3]}},{{&l_447[7][1][9],&l_447[8][0][3],&l_447[4][1][0],(void*)0},{&l_443,(void*)0,&l_447[4][1][0],&l_443},{&l_447[7][1][9],&l_447[3][0][8],(void*)0,&l_447[8][0][3]},{&l_371,&l_443,(void*)0,&l_443},{(void*)0,&l_443,&l_371,&l_447[8][0][3]},{(void*)0,&l_447[3][0][8],&l_447[7][1][9],&l_443},{&l_447[4][1][0],(void*)0,&l_443,(void*)0},{&l_447[4][1][0],&l_447[8][0][3],&l_447[7][1][9],&l_447[7][0][3]},{(void*)0,(void*)0,&l_371,&l_371},{(void*)0,&l_267,(void*)0,&l_371}},{{&l_371,(void*)0,(void*)0,&l_447[7][0][3]},{&l_447[7][1][9],&l_447[8][0][3],&l_447[4][1][0],(void*)0},{&l_443,(void*)0,&l_447[4][1][0],&l_443},{&l_447[7][1][9],&l_447[3][0][8],(void*)0,&l_447[8][0][3]},{&l_371,&l_443,(void*)0,&l_443},{(void*)0,&l_443,&l_371,&l_447[8][0][3]},{(void*)0,&l_447[3][0][8],&l_447[7][1][9],&l_443},{&l_447[4][1][0],(void*)0,&l_443,(void*)0},{&l_447[4][1][0],&l_447[8][0][3],&l_447[7][1][9],&l_447[7][0][3]},{(void*)0,(void*)0,&l_371,&l_371}},{{(void*)0,&l_267,(void*)0,&l_371},{&l_371,(void*)0,(void*)0,&l_447[7][0][3]},{&l_447[7][1][9],&l_447[8][0][3],&l_447[4][1][0],(void*)0},{&l_443,(void*)0,&l_447[4][1][0],&l_443},{&l_447[7][1][9],&l_447[3][0][8],(void*)0,&l_447[8][0][3]},{&l_371,&l_443,(void*)0,&l_443},{(void*)0,&l_443,&l_371,&l_447[8][0][3]},{(void*)0,&l_447[3][0][8],&l_447[7][1][9],&l_443},{&l_447[4][1][0],(void*)0,&l_443,(void*)0},{&l_447[4][1][0],&l_447[8][0][3],&l_447[7][1][9],&l_447[7][0][3]}}};
        uint16_t *l_508[9][3] = {{&l_500,(void*)0,&l_500},{&l_500,&l_448[6][2][3],(void*)0},{&l_500,(void*)0,(void*)0},{&g_85,(void*)0,&l_417},{(void*)0,&l_500,(void*)0},{&g_85,&g_85,(void*)0},{&l_500,(void*)0,&l_417},{(void*)0,&g_85,(void*)0},{(void*)0,&l_500,&g_85}};
        uint8_t *l_510 = &g_93;
        int i, j, k;
        (**g_77) = (safe_mod_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u(l_500, (g_85 = (((254UL >= 0x7FL) != ((((1UL >= (safe_mod_func_int32_t_s_s(((*p_32) = (*p_32)), (l_447[6][1][2] = (safe_mul_func_int16_t_s_s(l_505, ((((l_443 = ((void*)0 != &g_334)) ^ 0x74C4286AL) > 0xB421L) >= p_33))))))) , &g_262) != (void*)0) == l_507[0][0][2])) && (-9L))))) != g_334), 0x8128L));
        (*g_78) |= (((*l_510) = l_509) , 1L);
        (**l_391) = p_32;
    }
    (**l_391) = func_52((0xE4A5E2FFL ^ ((*p_32) = (**g_77))));
    return p_32;
}


/* ------------------------------------------ */
/* 
 * reads : g_10 g_12 g_11 g_73 g_9 g_77 g_83 g_78 g_3 g_101 g_93 g_85 g_211 g_87 g_207 g_235 g_229 g_258 g_227 g_228 g_212
 * writes: g_73 g_9 g_78 g_83 g_3 g_10 g_87 g_93 g_85 g_11 g_203 g_206 g_207 g_228 g_211 g_235 g_262
 */
static int16_t  func_34(int32_t * p_35, int32_t * p_36)
{ /* block id: 2 */
    float l_54 = 0x0.Fp+1;
    int32_t l_55 = 0x46DF8000L;
    uint8_t *l_71 = (void*)0;
    uint8_t *l_72 = &g_73;
    float *l_74 = &l_54;
    int32_t l_75 = (-2L);
    volatile int32_t **l_265 = &g_78;
    volatile int32_t ***l_264 = &l_265;
    if (func_37((safe_mul_func_uint8_t_u_u(1UL, (safe_sub_func_int64_t_s_s((func_46(g_10, g_12, g_11, func_52(l_55), (l_75 = ((*l_74) = ((safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f(l_55, ((safe_lshift_func_int8_t_s_u(0xA8L, ((*l_72) ^= (0x3CL && 248UL)))) , g_9))), 0xD.F607DBp+75)), g_9)), g_11)), l_55)) >= g_9)))) == g_12), 0x5292FD4AE77DDBA4LL)))), l_55, l_55, p_36))
    { /* block id: 98 */
        float l_238 = 0xC.C00202p-66;
        int32_t **l_239 = &g_87[0][1][2];
        (*l_239) = func_52(g_229[2][1][3]);
    }
    else
    { /* block id: 100 */
        int64_t * const l_246[10][3] = {{(void*)0,&g_212,&g_212},{&g_212,&g_212,&g_212},{(void*)0,&g_212,&g_212},{&g_212,&g_212,&g_212},{(void*)0,&g_212,&g_212},{&g_212,&g_212,&g_212},{(void*)0,&g_212,&g_212},{&g_212,&g_212,&g_212},{(void*)0,&g_212,&g_212},{&g_212,&g_212,&g_212}};
        float l_259 = 0x8.11A4CFp+97;
        int16_t *l_260 = &g_9;
        int16_t *l_261 = &g_262;
        int32_t l_263 = 0x1FB3B3D4L;
        int i, j;
        l_55 = (((((-1L) > (safe_rshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(l_75, (((safe_rshift_func_int8_t_s_s((l_246[0][0] == &g_229[3][6][1]), (safe_rshift_func_uint8_t_u_u((((safe_rshift_func_int8_t_s_s(((((void*)0 != &g_87[2][0][3]) , (safe_div_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(65535UL, ((*l_261) = ((*l_260) = ((safe_div_func_int8_t_s_s((((l_55 , ((*p_35) = ((+(g_258 , 4294967295UL)) ^ (-8L)))) , 0x6C8AD918L) && g_85), l_55)) , g_227))))), (-1L)))) & l_55), g_85)) , 2UL) | g_93), 3)))) <= l_263) > (*g_228)))), 11))) | l_263) == l_55) < l_263);
    }
    (*l_264) = &g_78;
    return (**l_265);
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_101 g_3 g_11 g_93 g_85 g_83 g_9 g_78 g_10 g_77 g_211 g_87 g_207 g_235
 * writes: g_73 g_87 g_85 g_11 g_3 g_10 g_203 g_206 g_207 g_83 g_228 g_211 g_235
 */
static int32_t  func_37(int16_t  p_38, uint32_t  p_39, int64_t  p_40, int32_t * p_41)
{ /* block id: 33 */
    int32_t *l_99 = &g_3;
    int32_t l_105 = 1L;
    int32_t l_109 = (-1L);
    int32_t l_128 = 0x2ABC6E4DL;
    int32_t l_129 = 0xEE0ECED6L;
    int32_t l_130 = 4L;
    int32_t l_131[7] = {0x20E1B1DCL,7L,7L,0x20E1B1DCL,7L,7L,0x20E1B1DCL};
    uint8_t *l_178 = &g_93;
    int32_t *l_233 = &l_105;
    int32_t *l_234[5] = {&l_131[0],&l_131[0],&l_131[0],&l_131[0],&l_131[0]};
    int i;
    for (g_73 = 13; (g_73 == 3); g_73 = safe_sub_func_int16_t_s_s(g_73, 9))
    { /* block id: 36 */
        int32_t **l_100 = (void*)0;
        int32_t l_102 = 0xFA607100L;
        int32_t l_119 = 0x6935E7B0L;
        int32_t l_122 = 7L;
        int32_t l_123 = (-6L);
        int64_t l_124 = 0x050866D82068EE18LL;
        int32_t l_125 = 0x171BA0D8L;
        int32_t l_126 = 0x1D28FF55L;
        int32_t l_127[5][3][2] = {{{0L,(-7L)},{0L,0L},{0L,(-7L)}},{{0L,0L},{0L,(-7L)},{0L,0L}},{{0L,(-7L)},{0L,0L},{0L,(-7L)}},{{0L,0L},{0L,(-7L)},{0L,0L}},{{0L,(-7L)},{0L,0L},{0L,(-7L)}}};
        uint8_t l_133 = 0x06L;
        int i, j, k;
        (*g_101) = l_99;
        for (g_85 = 0; (g_85 <= 1); g_85 += 1)
        { /* block id: 40 */
            int32_t *l_103 = (void*)0;
            int32_t *l_104 = &g_3;
            int32_t *l_106 = &g_3;
            int32_t *l_107 = &l_102;
            int32_t *l_108 = &g_3;
            int32_t *l_110 = &l_102;
            int32_t *l_111 = &l_105;
            int32_t *l_112 = &l_102;
            int32_t *l_113 = &l_105;
            int32_t *l_114 = &g_3;
            int32_t *l_115 = &l_105;
            int8_t l_116[2];
            int32_t *l_117 = &g_3;
            int32_t *l_118 = &g_3;
            int32_t *l_120 = &g_3;
            int32_t *l_121[3][5][8] = {{{&l_119,&l_119,(void*)0,&l_105,(void*)0,&l_105,(void*)0,&l_119},{&l_119,&l_109,&l_105,(void*)0,(void*)0,&l_105,&l_109,&l_119},{&l_109,(void*)0,&l_119,&l_105,&l_119,(void*)0,&l_109,&l_109},{(void*)0,&l_105,&l_105,&l_105,&l_105,(void*)0,(void*)0,(void*)0},{&l_105,(void*)0,(void*)0,(void*)0,&l_105,&l_105,&l_105,&l_105}},{{(void*)0,&l_109,&l_109,(void*)0,&l_119,&l_105,&l_119,(void*)0},{&l_109,&l_119,&l_109,&l_105,(void*)0,(void*)0,&l_105,&l_109},{&l_119,&l_119,(void*)0,&l_105,(void*)0,&l_105,(void*)0,&l_119},{&l_119,&l_109,&l_105,(void*)0,(void*)0,&l_105,&l_109,&l_119},{&l_109,(void*)0,&l_119,&l_105,&l_119,(void*)0,&l_109,&l_109}},{{(void*)0,&l_105,&l_105,&l_105,&l_105,(void*)0,(void*)0,(void*)0},{&l_105,(void*)0,(void*)0,(void*)0,&l_105,&l_105,&l_105,&l_105},{(void*)0,&l_109,&l_105,&l_109,(void*)0,&l_105,(void*)0,&l_109},{&l_105,(void*)0,&l_105,(void*)0,&l_119,&l_119,(void*)0,&l_105},{(void*)0,(void*)0,&l_119,&l_105,(void*)0,&l_105,&l_119,(void*)0}}};
            int32_t l_132 = 0xC935D89AL;
            int8_t *l_142 = &l_116[1];
            uint16_t l_149 = 65535UL;
            int8_t *l_150 = &g_11;
            uint8_t *l_158 = &l_133;
            int64_t *l_179 = &l_124;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_116[i] = 0x4EL;
            l_133--;
            (*l_111) |= (((&g_87[1][0][7] == &g_87[0][0][2]) , ((*l_150) &= (((safe_lshift_func_uint8_t_u_s((*l_99), 2)) >= (((safe_mul_func_int16_t_s_s(((safe_lshift_func_int8_t_s_s(((*l_142) = p_38), (((safe_mul_func_uint16_t_u_u((+p_39), (safe_mul_func_uint8_t_u_u(p_39, (0x3A27L == (!((0xA1E35D45L | (((0xBBL <= p_38) , 0UL) & 0x1FL)) | 4L))))))) ^ (-5L)) > 0xD1L))) , 0x118BL), (*l_99))) || 1L) & (-2L))) || l_149))) <= p_38);
            (*g_78) ^= ((*l_117) = ((!((p_40 ^ (((l_150 != l_142) & 0x96L) | (safe_rshift_func_int16_t_s_u((((((safe_div_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_u(((((*l_158) = 1UL) , (((safe_mul_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u(((((((((safe_mod_func_uint64_t_u_u(0UL, p_40)) == ((safe_mod_func_uint64_t_u_u((safe_add_func_uint64_t_u_u(g_73, (g_93 & (*l_106)))), p_38)) > (*l_99))) != g_85) && p_38) < g_3) , p_40) & g_11) || 2UL), 0xF1L)) >= g_11), g_83)) > 0xC2FE16E7L) , g_11)) || (*l_118)), 6)), 0x6F25517FA6E5F86DLL)) | g_9) >= (*p_41)) , g_93) , g_73), 7)))) <= 0x6E37L)) , 0L));
            if ((safe_div_func_int16_t_s_s(g_83, ((((safe_sub_func_int64_t_s_s((safe_mul_func_int8_t_s_s(p_38, (safe_sub_func_int64_t_s_s(((*l_179) = ((+p_38) , ((0x5924L & (p_39 ^ g_85)) != ((p_38 != (l_178 != l_178)) > 4294967295UL)))), 1L)))), 0xD70503560D2B78A1LL)) && p_40) && (*g_78)) , 0x42F9L))))
            { /* block id: 49 */
                int16_t l_188 = (-5L);
                uint8_t **l_202[3][10] = {{&l_158,(void*)0,&l_158,&l_158,(void*)0,&l_158,(void*)0,&l_158,&l_158,(void*)0},{&l_158,(void*)0,&l_158,&l_158,(void*)0,&l_158,(void*)0,&l_158,&l_158,(void*)0},{&l_158,(void*)0,&l_158,&l_158,(void*)0,&l_158,(void*)0,&l_158,&l_158,(void*)0}};
                uint8_t ***l_201[2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_201[i] = &l_202[2][2];
                (*l_120) = (safe_sub_func_uint8_t_u_u(g_3, (safe_mod_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((safe_mod_func_uint32_t_u_u(l_188, (safe_lshift_func_uint8_t_u_s((0xDBL >= (safe_sub_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(g_85, (0x96L | ((safe_rshift_func_uint16_t_u_s((*l_99), 11)) , g_83)))), ((safe_lshift_func_int16_t_s_s((0xF0CFL > (0x518A76D6EEA9B94ALL != g_73)), 2)) <= g_9)))), g_85)))), (*p_41))), g_10))));
                g_203 = &l_158;
            }
            else
            { /* block id: 52 */
                float *l_205 = &g_206;
                int32_t l_208 = (-1L);
                int32_t *l_214[5][6][7] = {{{&l_109,&l_119,&l_102,&l_123,&l_102,&l_130,&l_128},{&l_122,&l_130,&l_131[6],(void*)0,(void*)0,(void*)0,&l_130},{&l_131[6],&l_123,&l_102,&l_127[1][1][1],&l_119,&l_127[1][1][1],&l_102},{&l_131[6],&l_123,(void*)0,&l_129,&l_131[0],&l_125,&l_129},{&l_119,(void*)0,&l_131[6],&l_102,&l_128,&l_123,&l_122},{&l_127[1][1][1],(void*)0,&l_123,&l_125,&l_131[0],&l_127[1][1][1],&l_131[0]}},{{&l_127[3][1][0],&l_128,&l_128,&l_127[3][1][0],&l_119,&l_125,(void*)0},{&l_127[1][1][1],&l_125,(void*)0,&g_3,(void*)0,&l_123,&l_129},{&l_102,&l_131[6],(void*)0,&l_119,&l_102,&l_119,(void*)0},{&l_129,(void*)0,&l_131[6],&l_123,(void*)0,&l_129,&l_131[0]},{&l_127[1][1][1],&l_102,&l_123,&l_131[6],&l_123,(void*)0,&l_122},{(void*)0,&l_109,&l_122,&l_109,&l_127[1][1][1],&l_126,&l_129}},{{&l_123,&l_102,&l_119,&l_109,&l_109,&l_119,&l_102},{(void*)0,(void*)0,&l_122,&l_126,&l_105,&l_208,&l_130},{(void*)0,&l_131[6],&l_122,&l_119,&l_125,&l_105,&l_128},{&l_122,&l_125,&l_130,&l_126,&l_122,&l_130,&g_3},{&l_102,&l_128,&l_102,&l_109,&l_122,&l_123,&l_127[3][1][0]},{&l_127[1][1][1],(void*)0,&l_131[0],&l_109,(void*)0,(void*)0,&l_127[4][1][0]}},{{&l_128,(void*)0,&l_105,&l_131[6],&l_129,&l_123,&l_123},{&l_127[0][1][1],&l_123,&l_122,&l_123,&l_127[0][1][1],(void*)0,(void*)0},{&l_105,&l_122,&l_123,&l_128,&l_102,&l_131[6],(void*)0},{&l_130,(void*)0,&l_127[1][1][1],&l_131[6],(void*)0,&l_125,&l_127[4][1][0]},{&l_105,&l_128,(void*)0,(void*)0,(void*)0,&l_128,&l_105},{&l_123,&l_109,&l_127[0][1][1],(void*)0,&l_105,(void*)0,&l_125}},{{&l_119,&l_105,(void*)0,&l_109,(void*)0,(void*)0,&l_122},{&l_127[1][1][1],&l_130,&l_127[0][1][1],&l_123,&l_122,&l_123,&l_127[0][1][1]},{&l_129,&l_129,(void*)0,&l_102,&l_127[1][1][1],&l_123,&l_127[3][1][0]},{&l_122,&l_129,&l_127[1][1][1],&l_208,&l_125,&l_130,(void*)0},{&l_123,&l_131[6],&l_123,&l_102,&l_127[1][1][1],&l_119,&l_127[1][1][1]},{&l_127[4][1][0],&l_208,&l_125,(void*)0,&l_122,(void*)0,&l_127[1][1][1]}}};
                int i, j, k;
                g_207 = (-((*l_205) = p_39));
                if (l_208)
                { /* block id: 55 */
                    (*l_113) = (*l_99);
                }
                else
                { /* block id: 57 */
                    int64_t **l_213 = &l_179;
                    int32_t * const *l_216[6] = {&l_111,&l_214[1][1][6],&l_214[1][1][6],&l_111,&l_214[1][1][6],&l_214[1][1][6]};
                    int32_t * const **l_215 = &l_216[2];
                    int32_t * const ***l_217 = &l_215;
                    int i;
                    for (p_40 = 0; (p_40 <= 1); p_40 += 1)
                    { /* block id: 60 */
                        int i, j, k;
                        (*l_117) = (**g_77);
                    }
                    g_206 = (safe_mul_func_float_f_f((l_208 <= (((*l_213) = g_211) == (void*)0)), l_208));
                    l_214[1][3][6] = (*g_101);
                    (*l_217) = l_215;
                }
                (*p_41) ^= (*g_78);
                for (l_105 = 0; (l_105 <= 1); l_105 += 1)
                { /* block id: 71 */
                    if ((*p_41))
                        break;
                }
            }
            for (l_109 = 1; (l_109 >= 0); l_109 -= 1)
            { /* block id: 77 */
                uint16_t *l_220 = &g_83;
                uint16_t *l_222 = &l_149;
                int i, j, k;
                g_87[g_85][g_85][(g_85 + 6)] = func_52((safe_rshift_func_uint16_t_u_s(((*l_222) = ((*l_220) ^= g_207)), p_39)));
                for (l_119 = 5; (l_119 <= (-11)); l_119--)
                { /* block id: 83 */
                    const int64_t *l_226[8] = {&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227};
                    const int64_t **l_225[2][10] = {{&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3]},{&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3],&l_226[3]}};
                    int64_t *l_230 = (void*)0;
                    int64_t **l_231 = &g_211;
                    int32_t l_232 = 1L;
                    int i, j;
                    if (((g_228 = &g_212) != ((*l_231) = l_230)))
                    { /* block id: 86 */
                        if ((*p_41))
                            break;
                    }
                    else
                    { /* block id: 88 */
                        return l_232;
                    }
                    return (*p_41);
                }
            }
        }
    }
    --g_235;
    return (**g_77);
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_77 g_73 g_83 g_78 g_3
 * writes: g_9 g_78 g_73 g_83 g_3 g_10 g_87 g_93
 */
static int64_t  func_46(uint32_t  p_47, int16_t  p_48, float  p_49, int32_t * p_50, float  p_51)
{ /* block id: 9 */
    int32_t l_76[5][1][7] = {{{1L,1L,9L,(-10L),9L,1L,1L}},{{1L,9L,(-10L),9L,1L,1L,9L}},{{0x232038BEL,1L,0x232038BEL,9L,9L,0x232038BEL,1L}},{{9L,1L,(-10L),(-10L),1L,9L,1L}},{{0x232038BEL,9L,9L,0x232038BEL,1L,0x232038BEL,9L}}};
    int i, j, k;
    for (g_9 = 0; (g_9 <= 0); g_9 += 1)
    { /* block id: 12 */
        int64_t l_81[7];
        int32_t l_86 = 0xBD8E7029L;
        int i;
        for (i = 0; i < 7; i++)
            l_81[i] = 0L;
        (*g_77) = &g_10;
        for (g_73 = 0; (g_73 <= 0); g_73 += 1)
        { /* block id: 16 */
            uint32_t l_82 = 0x7F794FC7L;
            uint16_t *l_84[5][4] = {{&g_85,&g_85,&g_85,&g_85},{&g_85,(void*)0,&g_85,&g_85},{&g_85,&g_85,(void*)0,&g_85},{&g_85,&g_85,&g_85,&g_85},{&g_85,&g_85,&g_85,&g_85}};
            int32_t *l_89 = (void*)0;
            int32_t **l_88 = &l_89;
            uint8_t *l_92[10][3][3] = {{{&g_73,&g_73,(void*)0},{&g_73,&g_73,&g_73},{&g_93,&g_93,&g_73}},{{&g_73,&g_93,&g_73},{&g_93,&g_73,&g_73},{&g_73,&g_73,&g_73}},{{&g_73,&g_93,&g_73},{(void*)0,&g_73,&g_73},{(void*)0,&g_73,&g_73}},{{&g_73,&g_93,(void*)0},{&g_73,(void*)0,&g_93},{&g_73,&g_93,&g_93}},{{&g_73,(void*)0,(void*)0},{&g_73,&g_73,&g_73},{&g_93,(void*)0,(void*)0}},{{&g_93,&g_93,(void*)0},{&g_93,&g_73,&g_73},{(void*)0,&g_93,&g_73}},{{(void*)0,&g_73,&g_93},{&g_93,&g_73,&g_73},{&g_93,&g_93,&g_93}},{{&g_93,(void*)0,&g_73},{&g_73,(void*)0,&g_73},{&g_73,&g_93,&g_73}},{{&g_73,&g_73,&g_73},{&g_73,&g_93,&g_73},{&g_93,&g_93,&g_93}},{{&g_93,&g_93,&g_73},{&g_93,&g_73,&g_93},{&g_73,&g_93,&g_73}}};
            int i, j, k;
            (*g_78) = ((*p_50) = (safe_lshift_func_uint16_t_u_u((l_86 = (g_83 |= (l_81[2] | ((l_82 , (void*)0) != (void*)0)))), 11)));
            (*l_88) = (g_87[0][0][2] = p_50);
            (**l_88) = (safe_rshift_func_uint8_t_u_u((g_93 = 0x40L), 2));
            for (g_3 = 0; (g_3 <= 0); g_3 += 1)
            { /* block id: 27 */
                uint32_t l_94 = 0xEE4D5011L;
                l_94--;
            }
        }
    }
    return g_9;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_52(int8_t  p_53)
{ /* block id: 3 */
    int32_t *l_56 = &g_3;
    int32_t **l_57 = (void*)0;
    int32_t **l_58 = &l_56;
    (*l_58) = l_56;
    return &g_3;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_10, "g_10", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_83, "g_83", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc_bytes (&g_206, sizeof(g_206), "g_206", print_hash_value);
    transparent_crc(g_207, "g_207", print_hash_value);
    transparent_crc(g_212, "g_212", print_hash_value);
    transparent_crc_bytes (&g_221, sizeof(g_221), "g_221", print_hash_value);
    transparent_crc(g_227, "g_227", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_229[i][j][k], "g_229[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_235, "g_235", print_hash_value);
    transparent_crc(g_258.f0, "g_258.f0", print_hash_value);
    transparent_crc(g_262, "g_262", print_hash_value);
    transparent_crc(g_307, "g_307", print_hash_value);
    transparent_crc(g_334, "g_334", print_hash_value);
    transparent_crc(g_338, "g_338", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_379[i].f0, "g_379[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_420.f0, "g_420.f0", print_hash_value);
    transparent_crc(g_495, "g_495", print_hash_value);
    transparent_crc(g_545, "g_545", print_hash_value);
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc(g_558.f0, "g_558.f0", print_hash_value);
    transparent_crc(g_559.f0, "g_559.f0", print_hash_value);
    transparent_crc_bytes (&g_665, sizeof(g_665), "g_665", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_859[i].f0, "g_859[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_862[i].f0, "g_862[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_863, "g_863", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_923[i], "g_923[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_925, "g_925", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_926[i], "g_926[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_954[i], "g_954[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_980.f0, "g_980.f0", print_hash_value);
    transparent_crc(g_1003.f0, "g_1003.f0", print_hash_value);
    transparent_crc(g_1049, "g_1049", print_hash_value);
    transparent_crc(g_1159, "g_1159", print_hash_value);
    transparent_crc(g_1175, "g_1175", print_hash_value);
    transparent_crc_bytes (&g_1204, sizeof(g_1204), "g_1204", print_hash_value);
    transparent_crc(g_1268, "g_1268", print_hash_value);
    transparent_crc(g_1272, "g_1272", print_hash_value);
    transparent_crc(g_1283, "g_1283", print_hash_value);
    transparent_crc(g_1298.f0, "g_1298.f0", print_hash_value);
    transparent_crc(g_1300, "g_1300", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1302[i].f0, "g_1302[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1433.f0, "g_1433.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1541[i], "g_1541[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1671, "g_1671", print_hash_value);
    transparent_crc(g_1707, "g_1707", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1719[i][j][k].f0, "g_1719[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1771[i][j].f0, "g_1771[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1804.f0, "g_1804.f0", print_hash_value);
    transparent_crc(g_1917, "g_1917", print_hash_value);
    transparent_crc(g_2099.f0, "g_2099.f0", print_hash_value);
    transparent_crc(g_2170, "g_2170", print_hash_value);
    transparent_crc(g_2188, "g_2188", print_hash_value);
    transparent_crc(g_2213.f0, "g_2213.f0", print_hash_value);
    transparent_crc(g_2215.f0, "g_2215.f0", print_hash_value);
    transparent_crc(g_2249.f0, "g_2249.f0", print_hash_value);
    transparent_crc_bytes (&g_2316, sizeof(g_2316), "g_2316", print_hash_value);
    transparent_crc(g_2340, "g_2340", print_hash_value);
    transparent_crc(g_2369.f0, "g_2369.f0", print_hash_value);
    transparent_crc(g_2405, "g_2405", print_hash_value);
    transparent_crc(g_2406, "g_2406", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 613
XXX total union variables: 17

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 272
   depth: 2, occurrence: 78
   depth: 3, occurrence: 7
   depth: 4, occurrence: 4
   depth: 5, occurrence: 6
   depth: 8, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 3
   depth: 14, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 5
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 3
   depth: 21, occurrence: 6
   depth: 22, occurrence: 7
   depth: 23, occurrence: 3
   depth: 24, occurrence: 3
   depth: 25, occurrence: 6
   depth: 26, occurrence: 2
   depth: 27, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 5
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 37, occurrence: 1
   depth: 42, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 514

XXX times a variable address is taken: 1140
XXX times a pointer is dereferenced on RHS: 269
breakdown:
   depth: 1, occurrence: 239
   depth: 2, occurrence: 16
   depth: 3, occurrence: 6
   depth: 4, occurrence: 7
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 335
breakdown:
   depth: 1, occurrence: 303
   depth: 2, occurrence: 20
   depth: 3, occurrence: 8
   depth: 4, occurrence: 4
XXX times a pointer is compared with null: 49
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 27
XXX times a pointer is qualified to be dereferenced: 7227

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1123
   level: 2, occurrence: 197
   level: 3, occurrence: 111
   level: 4, occurrence: 50
   level: 5, occurrence: 23
XXX number of pointers point to pointers: 239
XXX number of pointers point to scalars: 262
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.2
XXX average alias set size: 1.38

XXX times a non-volatile is read: 1927
XXX times a non-volatile is write: 978
XXX times a volatile is read: 142
XXX    times read thru a pointer: 33
XXX times a volatile is write: 45
XXX    times written thru a pointer: 21
XXX times a volatile is available for access: 1.38e+03
XXX percentage of non-volatile access: 94

XXX forward jumps: 0
XXX backward jumps: 9

XXX stmts: 286
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 40
   depth: 2, occurrence: 47
   depth: 3, occurrence: 56
   depth: 4, occurrence: 53
   depth: 5, occurrence: 57

XXX percentage a fresh-made variable is used: 17.4
XXX percentage an existing variable is used: 82.6
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2594980089
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   float  f0;
   volatile int64_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static volatile uint32_t g_2[4][2] = {{0x6DF37D45L,0x6DF37D45L},{0x6DF37D45L,0x6DF37D45L},{0x6DF37D45L,0x6DF37D45L},{0x6DF37D45L,0x6DF37D45L}};
static int32_t g_3 = 0x4DC9ED9BL;
static uint16_t g_11[1] = {0x2E7CL};
static int64_t g_42[6] = {0x57520C256FAE7A48LL,0x57520C256FAE7A48LL,0x57520C256FAE7A48LL,0x57520C256FAE7A48LL,0x57520C256FAE7A48LL,0x57520C256FAE7A48LL};
static int32_t *g_97 = &g_3;
static int32_t **g_96[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t g_99[1] = {0UL};
static int32_t g_103 = 9L;
static uint64_t g_116 = 3UL;
static int64_t g_122 = 0x5447BFCA48CC9215LL;
static int64_t *g_121[6][9][4] = {{{(void*)0,&g_122,(void*)0,&g_42[2]},{&g_122,&g_42[0],&g_122,(void*)0},{&g_122,(void*)0,&g_42[2],(void*)0},{&g_42[0],&g_42[0],&g_42[0],&g_42[2]},{(void*)0,&g_122,&g_122,(void*)0},{&g_122,&g_42[2],(void*)0,&g_122},{&g_42[2],(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_42[2],(void*)0,&g_42[0]},{(void*)0,(void*)0,&g_122,&g_42[0]}},{{&g_122,&g_42[0],&g_122,&g_122},{&g_42[2],&g_42[0],&g_42[0],&g_42[0]},{&g_42[0],(void*)0,&g_42[0],&g_42[0]},{&g_122,&g_42[2],&g_122,&g_42[0]},{(void*)0,(void*)0,(void*)0,&g_42[0]},{(void*)0,&g_122,&g_122,(void*)0},{&g_122,&g_42[0],&g_42[0],&g_122},{&g_42[0],(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_122,&g_122,&g_42[0]}},{{&g_122,(void*)0,&g_122,&g_122},{(void*)0,&g_42[0],(void*)0,(void*)0},{&g_42[2],&g_122,&g_42[0],&g_42[0]},{&g_122,(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_42[2],(void*)0,&g_42[0]},{(void*)0,(void*)0,&g_122,&g_42[0]},{&g_122,&g_42[0],&g_122,&g_122},{&g_42[2],&g_42[0],&g_42[0],&g_42[0]},{&g_42[0],(void*)0,&g_42[0],&g_42[0]}},{{&g_122,&g_42[2],&g_122,&g_42[0]},{(void*)0,(void*)0,(void*)0,&g_42[0]},{(void*)0,&g_122,&g_122,(void*)0},{&g_122,&g_42[0],&g_42[0],&g_122},{&g_42[0],(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_122,&g_122,&g_42[0]},{&g_122,(void*)0,&g_122,&g_122},{(void*)0,&g_42[0],(void*)0,(void*)0},{&g_42[2],&g_122,&g_42[0],&g_42[0]}},{{&g_122,(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_42[2],(void*)0,&g_42[0]},{(void*)0,(void*)0,&g_122,&g_42[0]},{&g_122,&g_42[0],&g_122,&g_122},{&g_42[2],&g_42[0],&g_42[0],&g_42[0]},{&g_42[0],(void*)0,&g_42[0],&g_42[0]},{&g_122,&g_42[2],&g_122,&g_42[0]},{(void*)0,(void*)0,(void*)0,&g_42[0]},{(void*)0,&g_122,&g_122,(void*)0}},{{&g_122,&g_42[0],&g_42[0],&g_122},{&g_42[0],(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_122,&g_122,&g_42[0]},{&g_122,(void*)0,&g_122,&g_122},{(void*)0,&g_42[0],(void*)0,(void*)0},{&g_42[2],&g_122,&g_42[0],&g_42[0]},{&g_122,(void*)0,&g_42[0],&g_42[0]},{&g_42[2],&g_42[2],(void*)0,&g_42[0]},{(void*)0,(void*)0,&g_122,&g_42[0]}}};
static float g_127[7] = {0xC.83560Ep-35,(-0x9.1p+1),0xC.83560Ep-35,0xC.83560Ep-35,(-0x9.1p+1),0xC.83560Ep-35,0xC.83560Ep-35};
static float * volatile g_126[2] = {&g_127[6],&g_127[6]};
static int8_t g_159 = 0x6FL;
static int64_t g_182[10] = {3L,0L,3L,3L,0L,3L,3L,0L,3L,3L};
static int8_t g_200 = 0L;
static int32_t g_202 = 0xF830CD01L;
static int8_t g_203 = 0x8AL;
static uint32_t g_204 = 0xEDAC480BL;
static float g_221[7] = {0x9.Fp-1,0x9.Fp-1,0x9.Fp-1,0x9.Fp-1,0x9.Fp-1,0x9.Fp-1,0x9.Fp-1};
static int32_t g_222 = (-1L);
static int16_t g_258 = 0x4673L;
static int16_t *g_257[1] = {&g_258};
static int16_t ** volatile g_256 = &g_257[0];/* VOLATILE GLOBAL g_256 */
static const int16_t **g_259 = (void*)0;
static int8_t g_268 = 0x8CL;
static int64_t g_272 = (-1L);
static uint8_t g_273 = 248UL;
static int8_t g_274[4] = {0xADL,0xADL,0xADL,0xADL};
static volatile union U0 g_285 = {0x0.3p+1};/* VOLATILE GLOBAL g_285 */
static volatile union U0 g_288[9][2] = {{{0x0.Ep-1},{0x0.Ep-1}},{{0x1.C1FC5Ep+20},{0x0.5p+1}},{{0x0.Cp+1},{0x0.5p+1}},{{0x1.C1FC5Ep+20},{0x0.Ep-1}},{{0x0.Ep-1},{0x1.C1FC5Ep+20}},{{0x0.5p+1},{0x0.Cp+1}},{{0x0.5p+1},{0x1.C1FC5Ep+20}},{{0x0.Ep-1},{0x0.Ep-1}},{{0x1.C1FC5Ep+20},{0x0.5p+1}}};
static int32_t ***g_309 = &g_96[2];
static int32_t ****g_308 = &g_309;
static uint8_t **g_319 = (void*)0;
static uint8_t ***g_318 = &g_319;
static uint8_t ****g_317 = &g_318;
static uint8_t ***** volatile g_316 = &g_317;/* VOLATILE GLOBAL g_316 */
static int32_t ** volatile g_329[5] = {&g_97,&g_97,&g_97,&g_97,&g_97};
static int32_t ** volatile g_330 = &g_97;/* VOLATILE GLOBAL g_330 */
static volatile float g_498[3] = {0x3.6p-1,0x3.6p-1,0x3.6p-1};
static union U0 g_499 = {0x6.32540Bp+6};/* VOLATILE GLOBAL g_499 */
static uint32_t g_505 = 0xBC8BC854L;
static int16_t ** volatile *g_516[1][7][10] = {{{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0},{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0},{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0},{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0},{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0},{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0},{&g_256,&g_256,(void*)0,&g_256,(void*)0,&g_256,&g_256,(void*)0,&g_256,(void*)0}}};
static int16_t ** volatile ** volatile g_515 = &g_516[0][0][8];/* VOLATILE GLOBAL g_515 */
static uint64_t g_552 = 0xF8FF190ACEAA2F22LL;
static int16_t g_573 = 0L;
static float * volatile *g_576[1] = {&g_126[1]};
static float * volatile * volatile *g_575 = &g_576[0];
static float * volatile * volatile ** const  volatile g_577 = &g_575;/* VOLATILE GLOBAL g_577 */
static int32_t ** volatile g_659[9] = {&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97};
static int32_t g_670[10][8] = {{(-1L),(-1L),0x126435A9L,(-1L),0x126435A9L,(-1L),(-1L),0xF1F4A713L},{0L,1L,0L,(-4L),0x80750435L,0x09AACEE4L,(-4L),(-1L)},{0xA92FF0EFL,0x30940B1FL,(-1L),0x82C445F3L,0x80750435L,0xAA681FEEL,1L,(-1L)},{0L,0L,0x82C445F3L,(-1L),0x126435A9L,1L,0x30940B1FL,0xAA681FEEL},{(-1L),0x80750435L,0xC45C7F01L,0x646B0842L,0x646B0842L,0xC45C7F01L,0x80750435L,(-1L)},{(-1L),1L,1L,0xF1F4A713L,0x30940B1FL,0x126435A9L,0xA92FF0EFL,(-4L)},{0x646B0842L,0xA986EB13L,0x82C445F3L,(-1L),0xF1F4A713L,0xC45C7F01L,0x82C445F3L,(-7L)},{0x82C445F3L,(-1L),0x30940B1FL,0xA92FF0EFL,(-1L),0x646B0842L,0L,0xAA681FEEL},{(-7L),0x126435A9L,0L,0xE7EFE565L,0L,0xE7EFE565L,0L,0x126435A9L},{1L,0L,0xC45C7F01L,(-1L),1L,0L,0L,1L}};
static int8_t g_698[4][2][8] = {{{0xEAL,(-7L),4L,0x45L,0x24L,0x6EL,0x6EL,0x24L},{(-7L),1L,1L,(-7L),0x8CL,1L,4L,1L}},{{1L,0x0BL,0xDBL,0xE2L,1L,1L,(-1L),0x45L},{0xC3L,0x0BL,(-7L),4L,0x1CL,1L,0x0BL,1L}},{{(-7L),1L,0xFDL,1L,(-7L),0x6EL,0x1CL,0xFDL},{0x31L,(-7L),0x24L,0xDBL,1L,1L,0x6EL,1L}},{{0L,4L,0x24L,(-7L),(-1L),1L,0x1CL,0x1CL},{1L,1L,0xFDL,0xFDL,1L,1L,0x0BL,0xDBL}}};
static float * volatile g_700 = &g_499.f0;/* VOLATILE GLOBAL g_700 */
static uint8_t g_727[6][2][1] = {{{3UL},{0x94L}},{{0UL},{0x94L}},{{3UL},{3UL}},{{0x94L},{0UL}},{{0x94L},{3UL}},{{3UL},{0x94L}}};
static int32_t ** const  volatile g_763[6][1] = {{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}};
static union U0 g_775 = {0x2.4F394Fp+94};/* VOLATILE GLOBAL g_775 */
static int8_t g_776 = 4L;
static uint64_t g_804 = 0x05AD22FF7BF0D2DFLL;
static int8_t *g_823 = &g_274[0];
static int32_t g_830[1][4] = {{2L,2L,2L,2L}};
static int32_t *g_829[2] = {&g_830[0][3],&g_830[0][3]};
static uint8_t *****g_858[8][5] = {{(void*)0,&g_317,&g_317,(void*)0,(void*)0},{&g_317,&g_317,&g_317,&g_317,&g_317},{&g_317,(void*)0,(void*)0,(void*)0,&g_317},{&g_317,&g_317,&g_317,&g_317,&g_317},{(void*)0,(void*)0,(void*)0,&g_317,&g_317},{&g_317,&g_317,&g_317,&g_317,&g_317},{(void*)0,&g_317,&g_317,&g_317,&g_317},{&g_317,&g_317,&g_317,&g_317,&g_317}};
static const uint8_t * const ****g_859 = (void*)0;
static int8_t g_907 = 0xE6L;
static const uint8_t ***g_914 = (void*)0;
static uint64_t *g_951[6][7][3] = {{{&g_116,&g_552,&g_116},{&g_804,&g_552,&g_804},{&g_116,&g_552,&g_116},{&g_804,&g_552,&g_804},{&g_116,&g_552,&g_116},{&g_804,&g_552,&g_804},{&g_116,&g_552,&g_116}},{{&g_804,&g_552,&g_804},{&g_116,&g_552,&g_116},{&g_804,&g_552,&g_804},{&g_116,&g_552,&g_116},{&g_804,&g_552,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804}},{{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804}},{{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804}},{{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804}},{{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804},{&g_804,&g_116,&g_804},{&g_804,(void*)0,&g_804}}};
static volatile uint32_t g_1042 = 1UL;/* VOLATILE GLOBAL g_1042 */
static union U0 g_1055[2] = {{0x0.Ep+1},{0x0.Ep+1}};
static int32_t ***** volatile g_1084 = &g_308;/* VOLATILE GLOBAL g_1084 */
static uint16_t g_1118 = 5UL;
static uint16_t g_1121 = 0x76DCL;
static int32_t ** const  volatile g_1122 = &g_97;/* VOLATILE GLOBAL g_1122 */
static int32_t ** volatile g_1124 = (void*)0;/* VOLATILE GLOBAL g_1124 */
static int32_t ** volatile g_1138 = (void*)0;/* VOLATILE GLOBAL g_1138 */
static int32_t g_1233 = 1L;
static int64_t g_1278[8][10][3] = {{{(-1L),1L,(-10L)},{0xFE75E9E20F0B231DLL,0x2F80B5F54E379A48LL,0xEC95FC4F0079C9F2LL},{0x722E70676A8081B3LL,(-6L),0x7C77001F4F2CA548LL},{0x58D987B44CF6FD9ELL,0x6FC3E9B8BD31DC9ALL,0x330B103401B75617LL},{(-1L),0x3A22719FE4BA75B2LL,0x7896AB7494438B89LL},{(-9L),0L,(-9L)},{0L,6L,(-8L)},{0xC75CCA341F1691DBLL,(-6L),0x3E07B9A58E9E020ALL},{(-1L),4L,0xF165EAECD611DB2DLL},{0x1BCCA9214910FACELL,(-9L),2L}},{{(-1L),0xDD792506A1634B97LL,(-5L)},{0xC75CCA341F1691DBLL,(-1L),0L},{0L,0x3E07B9A58E9E020ALL,0x49CB639980BDAAEELL},{(-9L),0x84D3C41E903EF7F3LL,1L},{(-1L),0xFDA33A5C574D0261LL,0L},{0x58D987B44CF6FD9ELL,6L,1L},{0x722E70676A8081B3LL,1L,6L},{0xFE75E9E20F0B231DLL,0xC75CCA341F1691DBLL,(-1L)},{(-1L),0x6738820DA20D1872LL,0x60C55C5B13D1EBBELL},{(-6L),0x4EC529F6A9C2A695LL,0x60C55C5B13D1EBBELL}},{{0x835ECEB9A6EB223CLL,0x78548EBAEFDC5C54LL,(-1L)},{0xCC2AD66853F5A552LL,2L,6L},{1L,0xE51DB7B314EA9257LL,1L},{0x330B103401B75617LL,0xEC95FC4F0079C9F2LL,0L},{0x6738820DA20D1872LL,4L,1L},{0L,0x49CB639980BDAAEELL,0x49CB639980BDAAEELL},{0x7C77001F4F2CA548LL,0L,0L},{(-1L),1L,(-5L)},{6L,0x7896AB7494438B89LL,2L},{0x2F80B5F54E379A48LL,0L,0xF165EAECD611DB2DLL}},{{0xC004D4030DA6E5EBLL,0x7896AB7494438B89LL,0x3E07B9A58E9E020ALL},{4L,1L,(-8L)},{0xD0C2C83CAF1FFF73LL,0L,(-9L)},{0xD0C2C83CAF1FFF73LL,0L,0x3A22719FE4BA75B2LL},{1L,0x4EC529F6A9C2A695LL,0x3E07B9A58E9E020ALL},{(-1L),(-1L),0x49CB639980BDAAEELL},{0xD94996CEEEA30E34LL,0x7C77001F4F2CA548LL,(-1L)},{0xF165EAECD611DB2DLL,0x835ECEB9A6EB223CLL,4L},{(-1L),(-9L),0x7C77001F4F2CA548LL},{0x835ECEB9A6EB223CLL,(-6L),0xFDA33A5C574D0261LL}},{{0x835ECEB9A6EB223CLL,6L,0xF165EAECD611DB2DLL},{(-1L),0xEC95FC4F0079C9F2LL,0x722E70676A8081B3LL},{0xF165EAECD611DB2DLL,0L,0x2F80B5F54E379A48LL},{0xD94996CEEEA30E34LL,0L,0x4EC529F6A9C2A695LL},{(-1L),0xDD792506A1634B97LL,0xA390D8A7A56A45E2LL},{1L,0x4D01526254BA240CLL,(-1L)},{0xD0C2C83CAF1FFF73LL,0xD0C2C83CAF1FFF73LL,0x84D3C41E903EF7F3LL},{0x84D3C41E903EF7F3LL,0xA0EE7EA53A214F0ALL,0xC53C03D1C298CCCELL},{0xA390D8A7A56A45E2LL,0L,1L},{(-5L),1L,2L}},{{0x6FC3E9B8BD31DC9ALL,0xA390D8A7A56A45E2LL,1L},{0L,0x1806C17B68B94498LL,0xC53C03D1C298CCCELL},{0L,0L,0x84D3C41E903EF7F3LL},{0x49CB639980BDAAEELL,6L,(-1L)},{0xCDFB988B00374AEELL,0xD94996CEEEA30E34LL,0xA390D8A7A56A45E2LL},{6L,(-1L),0x4EC529F6A9C2A695LL},{0x3E07B9A58E9E020ALL,(-10L),0x2F80B5F54E379A48LL},{0x1BCCA9214910FACELL,0x6FC3E9B8BD31DC9ALL,0x722E70676A8081B3LL},{0x58D987B44CF6FD9ELL,0x7896AB7494438B89LL,0xF165EAECD611DB2DLL},{0x330B103401B75617LL,0x6738820DA20D1872LL,0xFDA33A5C574D0261LL}},{{0x1806C17B68B94498LL,0x6738820DA20D1872LL,0x7C77001F4F2CA548LL},{0x2F80B5F54E379A48LL,0x7896AB7494438B89LL,4L},{0x60C55C5B13D1EBBELL,0x6FC3E9B8BD31DC9ALL,(-1L)},{0x78548EBAEFDC5C54LL,(-10L),0x49CB639980BDAAEELL},{0xC53C03D1C298CCCELL,(-1L),0x3E07B9A58E9E020ALL},{(-6L),0xD94996CEEEA30E34LL,0x3A22719FE4BA75B2LL},{1L,6L,1L},{6L,0L,0xC75CCA341F1691DBLL},{0xEC95FC4F0079C9F2LL,0x1806C17B68B94498LL,0xD0C2C83CAF1FFF73LL},{0xA0EE7EA53A214F0ALL,0xA390D8A7A56A45E2LL,(-1L)}},{{0xC004D4030DA6E5EBLL,1L,0x835ECEB9A6EB223CLL},{0xA0EE7EA53A214F0ALL,0L,0x6738820DA20D1872LL},{0xEC95FC4F0079C9F2LL,0xA0EE7EA53A214F0ALL,0xCDFB988B00374AEELL},{6L,0xD0C2C83CAF1FFF73LL,0L},{1L,0x4D01526254BA240CLL,0x1BCCA9214910FACELL},{(-6L),0xDD792506A1634B97LL,(-8L)},{0xC53C03D1C298CCCELL,0L,0L},{0x78548EBAEFDC5C54LL,0L,0L},{0x60C55C5B13D1EBBELL,0xEC95FC4F0079C9F2LL,0L},{0x2F80B5F54E379A48LL,6L,0xE51DB7B314EA9257LL}}};
static uint8_t g_1281[4][2] = {{0xCBL,0xCBL},{0xCBL,0xCBL},{0xCBL,0xCBL},{0xCBL,0xCBL}};
static int16_t g_1311 = 0x2802L;
static float * const  volatile g_1357 = &g_1055[1].f0;/* VOLATILE GLOBAL g_1357 */
static uint16_t g_1367 = 0xCAE1L;
static volatile union U0 g_1373 = {-0x4.3p-1};/* VOLATILE GLOBAL g_1373 */
static union U0 *g_1428 = &g_775;
static int8_t * volatile *g_1445 = &g_823;
static int8_t * volatile ** const g_1444 = &g_1445;
static int8_t * volatile ** const * volatile g_1443 = &g_1444;/* VOLATILE GLOBAL g_1443 */
static uint16_t * volatile ** volatile g_1458 = (void*)0;/* VOLATILE GLOBAL g_1458 */
static uint16_t *g_1461 = &g_11[0];
static uint16_t ** const g_1460 = &g_1461;
static uint16_t ** const *g_1459 = &g_1460;
static uint16_t *g_1468 = &g_11[0];
static uint16_t ** const g_1467[4] = {&g_1468,&g_1468,&g_1468,&g_1468};
static uint16_t ** const *g_1466 = &g_1467[1];
static float *g_1483[8] = {&g_127[6],&g_127[6],&g_127[6],&g_127[6],&g_127[6],&g_127[6],&g_127[6],&g_127[6]};
static float **g_1482 = &g_1483[0];
static int8_t **g_1492 = &g_823;
static volatile union U0 g_1496[10][10] = {{{0x7.8p+1},{0x0.Ep+1},{0xF.4F75BBp-97},{0x5.9982C8p+11},{0x3.0p+1},{0x4.1B2DEBp-31},{0x0.1p-1},{0x0.Ap+1},{0x0.1p-1},{0x4.1B2DEBp-31}},{{0x7.8p+1},{0xE.9A5615p-91},{-0x4.Cp-1},{0xE.9A5615p-91},{0x7.8p+1},{0x5.9982C8p+11},{0x4.1B2DEBp-31},{0x5.4C5737p-96},{0x6.0F7572p+50},{0x0.0p+1}},{{0x5.9982C8p+11},{0x4.1B2DEBp-31},{0x5.4C5737p-96},{0x6.0F7572p+50},{0x0.0p+1},{0xE.CD7898p-50},{-0x10.5p-1},{-0x10.5p-1},{0xE.CD7898p-50},{0x0.0p+1}},{{0x4.1B2DEBp-31},{0x6.1153D1p-73},{0x6.1153D1p-73},{0x4.1B2DEBp-31},{0xE.9A5615p-91},{0xE.CD7898p-50},{0x8.E701F8p+57},{0x5.4C5737p-96},{0xF.4F75BBp-97},{0x0.1p-1}},{{0x0.Ap+1},{0xE.9A5615p-91},{0x0.0p+1},{0x7.8p+1},{0xF.4F75BBp-97},{0x8.E701F8p+57},{0x6.1153D1p-73},{0x8.E701F8p+57},{0xF.4F75BBp-97},{0x7.8p+1}},{{0x0.1p-1},{0x0.Ap+1},{0x0.1p-1},{0x4.1B2DEBp-31},{0x3.0p+1},{0x5.9982C8p+11},{0xF.4F75BBp-97},{0x0.Ep+1},{0x7.8p+1},{-0x4.Cp-1}},{{-0x4.Cp-1},{0x5.9982C8p+11},{0xE.9A5615p-91},{0x6.1153D1p-73},{0x0.Ep+1},{0x3.A11A68p-59},{0x3.A11A68p-59},{0x0.Ep+1},{0x6.1153D1p-73},{0xE.9A5615p-91}},{{0x0.0p+1},{0x0.0p+1},{0x0.1p-1},{0x6.0F7572p+50},{0x7.8p+1},{0x0.Ap+1},{0x9.B7D1AFp+93},{0x8.E701F8p+57},{0x0.Ep+1},{0xE.CD7898p-50}},{{0x3.A11A68p-59},{0xF.4F75BBp-97},{0x0.0p+1},{0x5.4C5737p-96},{0x9.B7D1AFp+93},{0x3.0p+1},{0x9.B7D1AFp+93},{0x5.4C5737p-96},{0x0.0p+1},{0xF.4F75BBp-97}},{{0x0.Ep+1},{0x0.0p+1},{0x6.1153D1p-73},{-0x4.Cp-1},{0xE.CD7898p-50},{0x4.1B2DEBp-31},{0x3.A11A68p-59},{0x3.0p+1},{0x6.0F7572p+50},{0x8.E701F8p+57}}};
static volatile union U0 g_1618 = {-0x1.7p-1};/* VOLATILE GLOBAL g_1618 */
static int32_t ** volatile g_1714[7][10][1] = {{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}},{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}},{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}},{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}},{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}},{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}},{{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97},{&g_97}}};
static uint8_t g_1745 = 0xD1L;
static int32_t * const **g_1784 = (void*)0;
static int32_t * const ** const *g_1783[4][5][6] = {{{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,(void*)0,&g_1784,&g_1784},{&g_1784,&g_1784,(void*)0,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,(void*)0,&g_1784},{&g_1784,&g_1784,(void*)0,&g_1784,&g_1784,&g_1784}},{{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{(void*)0,&g_1784,&g_1784,&g_1784,(void*)0,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{(void*)0,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784}},{{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,(void*)0,&g_1784,&g_1784},{&g_1784,&g_1784,(void*)0,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784}},{{&g_1784,&g_1784,(void*)0,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{(void*)0,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{&g_1784,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784},{(void*)0,&g_1784,&g_1784,&g_1784,&g_1784,&g_1784}}};
static int32_t * const ** const **g_1782 = &g_1783[0][1][0];


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static const int32_t  func_15(uint8_t  p_16, uint16_t * p_17);
static uint32_t  func_23(uint16_t * p_24, int32_t  p_25, uint16_t * p_26);
static uint16_t * func_28(uint16_t * p_29, uint16_t * p_30, uint16_t  p_31, float  p_32);
static uint16_t * func_33(uint16_t * p_34, int64_t  p_35);
static uint16_t * func_36(uint16_t * p_37, int64_t  p_38, uint16_t * p_39);
static int32_t  func_55(int64_t  p_56, uint8_t  p_57);
static int32_t  func_63(uint64_t  p_64, int32_t  p_65, int64_t  p_66);
static int8_t  func_79(uint32_t  p_80, int32_t * p_81, int64_t  p_82, int32_t ** p_83, int64_t * p_84);
static uint8_t  func_89(int16_t  p_90, uint16_t * p_91, int32_t ** p_92, const int64_t * p_93, int32_t  p_94);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_3 g_11 g_42 g_96 g_99 g_97 g_103 g_116 g_122 g_272 g_330 g_575 g_577 g_200 g_576 g_126 g_127 g_316 g_317 g_318 g_319 g_274 g_202 g_159 g_288 g_258 g_670 g_182 g_698 g_700 g_552 g_727 g_268 g_222 g_775 g_776 g_505 g_804 g_515 g_516 g_829 g_823 g_859 g_204 g_907 g_273 g_1468 g_1618 g_1281 g_1459 g_1460 g_1461 g_573 g_1367 g_1428 g_1444 g_1445 g_1118 g_1492 g_1042 g_1482 g_1483 g_499.f0 g_1055.f0 g_308 g_309 g_1782 g_1466 g_1467
 * writes: g_11 g_42 g_3 g_99 g_103 g_116 g_121 g_122 g_127 g_96 g_221 g_499.f0 g_573 g_575 g_552 g_203 g_204 g_258 g_268 g_317 g_727 g_182 g_97 g_698 g_272 g_505 g_804 g_222 g_823 g_829 g_202 g_858 g_859 g_200 g_318 g_274 g_1367 g_1745 g_670 g_1311
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int8_t l_6 = 0xC9L;
    uint16_t *l_10[10][6] = {{(void*)0,&g_11[0],(void*)0,&g_11[0],&g_11[0],&g_11[0]},{(void*)0,&g_11[0],&g_11[0],&g_11[0],&g_11[0],(void*)0},{(void*)0,&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0]},{(void*)0,&g_11[0],(void*)0,&g_11[0],&g_11[0],&g_11[0]},{(void*)0,&g_11[0],&g_11[0],&g_11[0],&g_11[0],(void*)0},{(void*)0,&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0]},{(void*)0,&g_11[0],(void*)0,&g_11[0],&g_11[0],&g_11[0]},{(void*)0,&g_11[0],&g_11[0],&g_11[0],&g_11[0],(void*)0},{(void*)0,&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0]},{(void*)0,&g_11[0],(void*)0,&g_11[0],&g_11[0],&g_11[0]}};
    int32_t l_14[10] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
    int16_t l_1832 = 0xC7E3L;
    int i, j;
    l_14[9] ^= ((g_2[2][0] != (g_3 != 0x7EL)) > ((safe_lshift_func_int16_t_s_u(l_6, (safe_unary_minus_func_int64_t_s((safe_sub_func_int16_t_s_s(0xF4C5L, (++g_11[0]))))))) < (g_3 & g_3)));
    l_1832 ^= func_15(g_2[2][0], l_10[9][5]);
    return g_1042;
}


/* ------------------------------------------ */
/* 
 * reads : g_11 g_2 g_3 g_42 g_96 g_99 g_97 g_103 g_116 g_122 g_272 g_330 g_575 g_577 g_200 g_576 g_126 g_127 g_316 g_317 g_318 g_319 g_274 g_202 g_159 g_288 g_258 g_670 g_182 g_698 g_700 g_552 g_727 g_268 g_222 g_775 g_776 g_505 g_804 g_515 g_516 g_829 g_823 g_859 g_204 g_907 g_273 g_1468 g_1618 g_1281 g_1459 g_1460 g_1461 g_573 g_1367 g_1428 g_1444 g_1445 g_1118 g_1492 g_1042 g_1482 g_1483 g_499.f0 g_1055.f0 g_308 g_309 g_1782 g_1466 g_1467
 * writes: g_42 g_3 g_99 g_103 g_116 g_121 g_122 g_127 g_96 g_221 g_499.f0 g_573 g_575 g_552 g_203 g_204 g_258 g_268 g_317 g_727 g_182 g_97 g_698 g_272 g_505 g_804 g_222 g_823 g_829 g_202 g_858 g_859 g_200 g_318 g_11 g_274 g_1367 g_1745 g_670 g_1311
 */
static const int32_t  func_15(uint8_t  p_16, uint16_t * p_17)
{ /* block id: 3 */
    uint16_t *l_18 = (void*)0;
    int32_t l_27 = 0xE6A0122DL;
    int16_t l_40 = 0xB441L;
    int64_t *l_41[5];
    int32_t *l_1611 = &g_3;
    int8_t *l_1671 = &g_274[2];
    int64_t l_1748 = 0x5F500F6C4450DEBALL;
    int16_t l_1749[1];
    int32_t *** const *l_1781[5][9] = {{(void*)0,(void*)0,&g_309,(void*)0,(void*)0,&g_309,(void*)0,(void*)0,&g_309},{&g_309,&g_309,(void*)0,&g_309,&g_309,(void*)0,&g_309,&g_309,(void*)0},{(void*)0,(void*)0,&g_309,(void*)0,(void*)0,&g_309,(void*)0,(void*)0,&g_309},{&g_309,&g_309,(void*)0,&g_309,&g_309,(void*)0,&g_309,&g_309,(void*)0},{(void*)0,(void*)0,&g_309,(void*)0,(void*)0,&g_309,(void*)0,(void*)0,&g_309}};
    int32_t *** const **l_1780 = &l_1781[3][5];
    int16_t * const * const l_1823 = &g_257[0];
    uint16_t l_1825 = 5UL;
    int i, j;
    for (i = 0; i < 5; i++)
        l_41[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1749[i] = 0x8F34L;
    (*l_1611) = ((((void*)0 == l_18) , (((safe_add_func_int16_t_s_s(((((((safe_sub_func_uint32_t_u_u(func_23(p_17, l_27, func_28(func_33(func_36(p_17, ((g_11[0] , ((g_42[0] = (3L >= (l_40 |= p_16))) <= (((((0xE.17C927p+64 == g_2[2][0]) >= g_11[0]) <= l_27) , g_2[0][0]) , 0x3D5AB883EB999C82LL))) | p_16), l_18), p_16), l_18, g_274[0], p_16)), g_273)) , l_27) != l_27) == l_27) || p_16) || 0L), l_27)) > 1UL) == (*g_1468))) , l_40);
    if ((0x616F9455L <= ((void*)0 != &g_1496[7][1])))
    { /* block id: 737 */
        float l_1621 = 0x1.3p+1;
        int32_t l_1622 = (-5L);
        uint32_t *l_1623 = &g_505;
        const int32_t l_1632 = (-1L);
        uint8_t l_1701 = 0UL;
        int32_t l_1706 = 0L;
        const float ****l_1707 = (void*)0;
        uint16_t * const *l_1711[5][7][6] = {{{(void*)0,&l_18,&g_1461,(void*)0,&g_1468,(void*)0},{&l_18,&g_1468,&g_1468,&g_1468,(void*)0,(void*)0},{&g_1461,(void*)0,(void*)0,&g_1461,&g_1468,&g_1468},{(void*)0,&g_1461,&g_1461,&g_1468,(void*)0,&l_18},{(void*)0,(void*)0,&g_1468,&g_1468,(void*)0,&g_1461},{&g_1468,&g_1461,&l_18,&g_1468,&g_1468,&g_1468},{(void*)0,(void*)0,&l_18,(void*)0,(void*)0,(void*)0}},{{&g_1468,&g_1468,(void*)0,&g_1461,&g_1468,(void*)0},{&l_18,&l_18,&g_1461,&g_1468,&g_1468,(void*)0},{&g_1461,(void*)0,(void*)0,&l_18,&g_1468,(void*)0},{&g_1468,&l_18,&l_18,&l_18,&l_18,&g_1468},{(void*)0,&g_1468,&l_18,(void*)0,(void*)0,&g_1461},{(void*)0,&g_1468,&g_1468,&g_1461,&l_18,&l_18},{(void*)0,&g_1468,&g_1461,(void*)0,&g_1468,&g_1468}},{{(void*)0,(void*)0,(void*)0,&l_18,(void*)0,(void*)0},{&g_1468,&g_1468,&g_1468,&l_18,&g_1461,&g_1468},{&g_1461,(void*)0,&g_1468,&g_1468,(void*)0,(void*)0},{&l_18,(void*)0,&g_1468,&g_1461,&g_1461,(void*)0},{&g_1468,&g_1468,&g_1461,(void*)0,(void*)0,&g_1461},{(void*)0,(void*)0,&g_1468,&g_1468,&g_1468,&l_18},{&g_1468,&g_1468,&g_1468,&g_1468,&l_18,&g_1468}},{{(void*)0,&g_1468,&g_1468,&g_1468,(void*)0,&l_18},{(void*)0,&g_1468,&g_1468,&g_1461,&l_18,&g_1461},{&g_1461,&l_18,&g_1461,&g_1468,&g_1468,(void*)0},{&l_18,(void*)0,&g_1468,&g_1468,&g_1468,(void*)0},{&g_1468,&l_18,&g_1468,&g_1468,&g_1468,&g_1468},{&l_18,&g_1468,&g_1468,&g_1468,(void*)0,(void*)0},{&g_1461,(void*)0,(void*)0,&g_1461,&g_1468,&g_1468}},{{(void*)0,&g_1461,&g_1461,&g_1468,(void*)0,&l_18},{(void*)0,(void*)0,&g_1468,&g_1468,(void*)0,&g_1461},{&g_1468,&g_1461,&l_18,&g_1468,&g_1468,&g_1468},{(void*)0,(void*)0,&l_18,(void*)0,(void*)0,(void*)0},{&g_1468,&g_1468,(void*)0,&g_1461,&g_1468,&l_18},{&g_1468,(void*)0,&g_1468,(void*)0,&l_18,&l_18},{&g_1468,&g_1468,(void*)0,&l_18,&g_1468,&g_1468}}};
        uint16_t * const **l_1710 = &l_1711[1][6][5];
        uint16_t * const ***l_1709[4][8][1] = {{{(void*)0},{(void*)0},{&l_1710},{(void*)0},{(void*)0},{&l_1710},{(void*)0},{(void*)0}},{{&l_1710},{(void*)0},{(void*)0},{&l_1710},{(void*)0},{(void*)0},{&l_1710},{(void*)0}},{{(void*)0},{&l_1710},{(void*)0},{(void*)0},{&l_1710},{(void*)0},{(void*)0},{&l_1710}},{{(void*)0},{(void*)0},{&l_1710},{(void*)0},{(void*)0},{&l_1710},{(void*)0},{(void*)0}}};
        int i, j, k;
        if (((safe_lshift_func_int16_t_s_u((((safe_mod_func_int32_t_s_s(((safe_div_func_uint16_t_u_u((g_1618 , (safe_mod_func_uint8_t_u_u((((p_16 <= (p_16 && (l_1622 = (-1L)))) , ((*l_1611) & (++(*l_1623)))) == (p_16 <= (safe_mul_func_uint8_t_u_u((safe_div_func_int16_t_s_s(0x33CEL, (safe_lshift_func_uint8_t_u_s(l_1632, 1)))), ((g_122 < g_272) , l_1632))))), (-1L)))), 0xFA55L)) , p_16), 0x823C5C34L)) | (*l_1611)) , 3L), 14)) < 0x7E76L))
        { /* block id: 740 */
            uint8_t *l_1657 = &g_99[0];
            uint8_t * const *l_1656 = &l_1657;
            uint8_t * const **l_1655 = &l_1656;
            int32_t l_1658 = 0L;
            uint16_t *****l_1768 = (void*)0;
            int32_t l_1793 = (-2L);
            int32_t l_1796 = 0xF8B5449DL;
            int32_t *l_1797[7] = {&g_670[7][0],&g_670[7][0],&g_670[7][0],&g_670[7][0],&g_670[7][0],&g_670[7][0],&g_670[7][0]};
            int i;
            if (p_16)
            { /* block id: 741 */
                const int32_t l_1633 = 0L;
                return l_1633;
            }
            else
            { /* block id: 743 */
                int16_t l_1641 = 0L;
                uint8_t ***l_1651 = &g_319;
                int16_t *l_1654 = &g_573;
                uint64_t *l_1659 = &g_116;
                uint64_t *l_1660 = &g_804;
                int32_t *l_1680[5] = {&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1]};
                uint64_t l_1681[4] = {18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL};
                const float *****l_1708 = &l_1707;
                uint16_t * const ****l_1712[5];
                const uint8_t *** const *l_1777 = &g_914;
                const uint8_t *** const **l_1776 = &l_1777;
                int32_t ****l_1779 = &g_309;
                int i;
                for (i = 0; i < 5; i++)
                    l_1712[i] = &l_1709[0][2][0];
                if (((((safe_mul_func_uint16_t_u_u(((((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((~1L), 0)), ((l_1641 < ((safe_add_func_uint64_t_u_u(((*l_1660) = ((*l_1659) = (safe_rshift_func_uint8_t_u_s((~l_1641), (safe_div_func_int32_t_s_s((*l_1611), (safe_mod_func_int64_t_s_s((((l_1658 = (l_1622 = (((*g_317) = l_1651) != ((safe_mul_func_uint16_t_u_u(((void*)0 == &l_27), ((*l_1654) = p_16))) , l_1655)))) & p_16) == 1L), l_1641)))))))), 0xDCECA5E015429BF7LL)) , 0x95B6B3C9AE25F62ELL)) ^ p_16))) > (*l_1611)) || (*g_1468)) <= 0xD7L), p_16)) < p_16) & 18446744073709551614UL) , l_1658))
                { /* block id: 750 */
                    for (g_122 = 0; (g_122 <= 1); g_122 += 1)
                    { /* block id: 753 */
                        uint32_t l_1678 = 0xEF4252F5L;
                        int32_t *l_1679[9] = {&g_103,&g_103,&g_103,&g_103,&g_103,&g_103,&g_103,&g_103,&g_103};
                        int i;
                        (*l_1611) = (g_1281[2][1] && ((*l_1623) = (((*l_1654) &= (p_16 && (((*g_823) = (65530UL && (safe_mod_func_uint32_t_u_u((p_16 == 0xDA30L), (safe_mod_func_uint16_t_u_u(((***g_1459) = ((safe_mod_func_uint64_t_u_u((((safe_rshift_func_int16_t_s_s(((l_1671 == l_1671) , (l_1678 &= (((*l_1660)++) && (safe_div_func_int8_t_s_s((7UL && (((safe_mod_func_int64_t_s_s(l_1641, p_16)) < p_16) , 65535UL)), l_1658))))), 0)) ^ l_1658) && 0xCCL), p_16)) <= l_1622)), p_16)))))) & l_1641))) >= p_16)));
                        l_1680[4] = l_1679[7];
                        l_1681[1]--;
                    }
                    return p_16;
                }
                else
                { /* block id: 765 */
                    for (g_1367 = 0; (g_1367 <= 2); g_1367 += 1)
                    { /* block id: 768 */
                        uint16_t l_1684 = 0x266EL;
                        l_1684--;
                    }
                }
                if (((safe_mod_func_int16_t_s_s((safe_add_func_int16_t_s_s((safe_mod_func_int64_t_s_s((l_27 = (safe_add_func_uint64_t_u_u(((((safe_lshift_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(((***g_1444) = (safe_rshift_func_int8_t_s_s(l_1701, (safe_rshift_func_int16_t_s_s((((*l_1611) && ((((*g_1428) , (l_1709[0][4][0] = (((l_1701 | ((*l_1611) |= 0xA1D53001759CF59CLL)) > (*g_823)) , ((safe_mod_func_uint8_t_u_u(((&g_575 == ((*l_1708) = ((0xB2B7ED4EL > (l_1706 |= l_1622)) , l_1707))) , p_16), p_16)) , l_1709[0][4][0])))) != &l_1710) <= l_1622)) && p_16), l_1658))))), 1L)), 0)) || g_1118) < p_16) & l_1632), p_16))), p_16)), (**g_1460))), (**g_1460))) , p_16))
                { /* block id: 778 */
                    const int8_t l_1713 = 0xACL;
                    uint8_t *****l_1778 = &g_317;
                    int32_t l_1792[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1792[i] = 0x45D0420DL;
                    if ((((**g_1492) == (*l_1611)) , 1L))
                    { /* block id: 779 */
                        return l_1713;
                    }
                    else
                    { /* block id: 781 */
                        int32_t **l_1715 = &l_1680[4];
                        float l_1743[9][7] = {{0x1.4p+1,0x1.96863Cp+86,0x1.96863Cp+86,0x1.4p+1,0x1.1p+1,0x1.4p+1,0x1.96863Cp+86},{0x1.1p+1,0x1.1p+1,(-0x7.7p-1),0x1.99E125p+19,(-0x7.7p-1),0x1.1p+1,0x1.1p+1},{0x0.86CC23p-22,0x1.96863Cp+86,0x6.0p+1,0x1.96863Cp+86,0x0.86CC23p-22,0x0.86CC23p-22,0x1.96863Cp+86},{(-0x1.9p+1),0x2.BF5774p-61,(-0x1.9p+1),(-0x7.7p-1),(-0x7.7p-1),(-0x1.9p+1),0x2.BF5774p-61},{0x1.96863Cp+86,0x1.1p+1,0x6.0p+1,0x6.0p+1,0x1.1p+1,0x1.96863Cp+86,0x1.1p+1},{(-0x1.9p+1),(-0x7.7p-1),(-0x7.7p-1),(-0x1.9p+1),0x2.BF5774p-61,(-0x1.9p+1),(-0x7.7p-1)},{0x0.86CC23p-22,0x0.86CC23p-22,0x1.96863Cp+86,0x6.0p+1,0x1.96863Cp+86,0x0.86CC23p-22,0x0.86CC23p-22},{0x1.1p+1,(-0x7.7p-1),0x1.99E125p+19,(-0x7.7p-1),0x1.1p+1,0x1.1p+1,(-0x7.7p-1)},{0x1.4p+1,0x1.1p+1,0x1.4p+1,0x1.96863Cp+86,0x1.96863Cp+86,0x1.4p+1,0x1.1p+1}};
                        float *l_1744 = &l_1743[1][2];
                        float *l_1746 = &g_221[4];
                        int32_t l_1747 = 0xFEDCEF89L;
                        uint16_t ** const **l_1767[4][2] = {{&g_1459,&g_1466},{&g_1459,&g_1466},{&g_1459,&g_1466},{&g_1459,&g_1466}};
                        uint16_t ** const ** const *l_1766 = &l_1767[1][0];
                        int32_t * const ** const **l_1785[8][1];
                        int64_t l_1794 = 0L;
                        int32_t l_1795 = 0x8F73773AL;
                        int i, j;
                        for (i = 0; i < 8; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1785[i][j] = &g_1783[0][1][0];
                        }
                        (*l_1715) = l_1680[4];
                        (**l_1715) ^= ((g_1042 , (((*l_1659) = (safe_add_func_int64_t_s_s(((0xAB5AL || (safe_add_func_uint32_t_u_u((!((l_1713 != (safe_div_func_float_f_f((-0x1.Dp-1), ((safe_add_func_float_f_f(p_16, ((safe_div_func_float_f_f((safe_div_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f((*l_1611), ((*l_1746) = ((safe_add_func_float_f_f(((g_1745 = ((-0x1.2p-1) != (safe_sub_func_float_f_f(((*l_1744) = ((safe_sub_func_float_f_f(((-0x2.9p-1) == ((safe_mul_func_float_f_f(((***g_575) = (0x0.E8E3F5p+16 >= 0x0.2p-1)), p_16)) < l_1701)), l_1632)) == l_1743[1][2])), (-0x5.2p-1))))) <= p_16), l_1701)) <= (-0x1.Fp+1))))), 0xA.039111p+39)), l_1747)), p_16)), l_1748)) < p_16))) > 0xD.2B2B52p-84)))) , 0UL)), p_16))) && (*g_1461)), l_1749[0]))) & (*l_1611))) , p_16);
                        (**g_308) = ((safe_sub_func_float_f_f(((****g_577) = ((safe_add_func_float_f_f(p_16, (safe_sub_func_float_f_f((l_1713 > 0x8.FE0274p+89), ((((*g_700) = ((**g_1482) < 0x2.9p-1)) == (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f((p_16 , (((*l_1746) = ((*l_1744) = (0xB.13211Fp+87 >= (((safe_mul_func_float_f_f((safe_mul_func_float_f_f((l_1766 != l_1768), l_1658)), 0x1.5750D3p-3)) < p_16) > p_16)))) >= p_16)), 0x3.8D3D50p-13)), 0x3.66EC5Dp-81)), 0x5.B8523Fp+30))) >= 0xA.540973p-37))))) != 0x3.9p+1)), l_1713)) , (void*)0);
                        l_1796 ^= ((safe_mod_func_int32_t_s_s((safe_mod_func_int64_t_s_s(((safe_rshift_func_int8_t_s_s((!g_204), (((l_1776 != l_1778) <= (**l_1715)) , ((**g_1492) ^= (((((void*)0 == l_1779) > (((l_1780 == (l_1785[2][0] = g_1782)) <= ((***g_1466)++)) >= (safe_lshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_s(((*l_1654) = (((((l_1792[0] = p_16) && l_1792[0]) && p_16) & l_1793) != 0x7FL)), l_1658)), 2)))) != 0x7C8EL) == (**l_1715)))))) , l_1794), (-10L))), l_1658)) != l_1795);
                    }
                }
                else
                { /* block id: 801 */
                    for (g_3 = 0; g_3 < 4; g_3 += 1)
                    {
                        for (g_1745 = 0; g_1745 < 8; g_1745 += 1)
                        {
                            for (g_1311 = 0; g_1311 < 1; g_1311 += 1)
                            {
                                l_1709[g_3][g_1745][g_1311] = &l_1710;
                            }
                        }
                    }
                    (***g_575) = (0x1.Ep-1 != 0x6.Ep-1);
                    l_1797[3] = (void*)0;
                }
                return p_16;
            }
        }
        else
        { /* block id: 808 */
            int32_t *l_1798 = (void*)0;
            const uint16_t l_1799[7][10] = {{0x12B5L,65535UL,0UL,0UL,65535UL,0x12B5L,0x2C2FL,0x12B5L,65535UL,0UL},{0xED2CL,65526UL,0xED2CL,0UL,0x2C2FL,0x2C2FL,0UL,0xED2CL,65526UL,0xED2CL},{0xED2CL,0x12B5L,65526UL,65535UL,65526UL,0x12B5L,0xED2CL,0xED2CL,0x12B5L,65526UL},{0x12B5L,0xED2CL,0xED2CL,0x12B5L,65526UL,65535UL,65526UL,0x12B5L,0xED2CL,0xED2CL},{65526UL,0xED2CL,0UL,0x2C2FL,0x2C2FL,0UL,0xED2CL,65526UL,0xED2CL,0x2C2FL},{0UL,0xED2CL,65526UL,0xED2CL,0UL,0x2C2FL,0x2C2FL,0UL,0xED2CL,65526UL},{65529UL,65529UL,65526UL,0UL,0x12B5L,0UL,65526UL,65529UL,65529UL,65526UL}};
            int i, j;
            l_1798 = l_1798;
            return l_1799[0][2];
        }
    }
    else
    { /* block id: 812 */
        uint16_t l_1802 = 0xE54EL;
        int8_t **l_1808 = (void*)0;
        int8_t ***l_1809 = &l_1808;
        int32_t * const *l_1818 = &g_829[0];
        int16_t ** const l_1824 = &g_257[0];
        uint32_t *l_1826 = (void*)0;
        uint32_t *l_1827 = &g_204;
        uint64_t *l_1828 = &g_804;
        int32_t l_1829 = 3L;
        for (g_202 = 0; (g_202 == 24); g_202++)
        { /* block id: 815 */
            l_1802--;
        }
        l_1829 = (~((safe_add_func_uint8_t_u_u((((*l_1809) = l_1808) == (void*)0), (safe_div_func_uint16_t_u_u((((*l_1828) ^= ((*l_1611) <= (safe_unary_minus_func_uint32_t_u(((*l_1827) ^= (safe_sub_func_int64_t_s_s(((0x98BDL ^ ((*l_1611) || ((+(safe_add_func_float_f_f((((void*)0 == l_1818) , (p_16 > (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((l_1823 != l_1824) <= (*g_700)), p_16)), l_1825)))), 0x0.D8CA6Cp+44))) , (-1L)))) != 3L), 0xFF65E36E322FA1E2LL))))))) || p_16), 65531UL)))) != p_16));
        for (g_122 = 0; (g_122 >= 4); ++g_122)
        { /* block id: 824 */
            return p_16;
        }
    }
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads : g_204
 * writes:
 */
static uint32_t  func_23(uint16_t * p_24, int32_t  p_25, uint16_t * p_26)
{ /* block id: 394 */
    uint64_t *l_913 = &g_116;
    const uint8_t ****l_915 = &g_914;
    int32_t l_917 = 0xEE7B7A98L;
    int32_t l_925 = 2L;
    int32_t l_926 = 0xB79ACA84L;
    int32_t l_929 = 1L;
    int32_t l_933 = 0xB68F70E7L;
    int32_t l_935[6][4][3] = {{{0L,0L,0xE02B9EC9L},{0xB3E5175EL,0x1A988651L,0xB3E5175EL},{0xE02B9EC9L,0L,0L},{0x5EC9C387L,1L,1L}},{{(-1L),0x96F66B4BL,0x09D949F6L},{0L,0L,(-1L)},{(-1L),0xE02B9EC9L,0x96F66B4BL},{0x5EC9C387L,(-1L),0x946A9B05L}},{{0xE02B9EC9L,0x170A8A11L,0x170A8A11L},{0xB3E5175EL,0x5EC9C387L,0x946A9B05L},{0L,3L,0x96F66B4BL},{1L,0xA97D4984L,(-1L)}},{{0x09D949F6L,0x104EB282L,0x09D949F6L},{(-1L),0xA97D4984L,1L},{0x96F66B4BL,3L,0L},{0x946A9B05L,0x5EC9C387L,0xB3E5175EL}},{{0x170A8A11L,0x170A8A11L,0xE02B9EC9L},{0x946A9B05L,(-1L),0x5EC9C387L},{0x96F66B4BL,0xE02B9EC9L,(-1L)},{(-1L),0L,0L}},{{0x09D949F6L,0x96F66B4BL,(-1L)},{1L,1L,0x5EC9C387L},{0L,0L,0xE02B9EC9L},{0xB3E5175EL,0x1A988651L,0xB3E5175EL}}};
    int16_t **l_956[4][2][5];
    uint32_t l_958 = 18446744073709551607UL;
    int32_t **l_980 = &g_829[1];
    int8_t * const *l_1003 = &g_823;
    int8_t * const **l_1002 = &l_1003;
    uint64_t l_1078 = 18446744073709551615UL;
    int64_t l_1079 = (-1L);
    int32_t *l_1089[7][5] = {{&l_929,&g_103,&g_3,&g_3,&g_103},{&l_929,&g_103,&g_3,&g_3,&g_103},{&l_929,&g_103,&g_3,&g_3,&g_103},{&l_929,&g_103,&g_3,&g_3,&g_103},{&l_929,&g_103,&g_3,&g_3,&g_103},{&l_929,&g_103,&g_3,&g_3,&g_103},{&l_929,&g_103,&g_3,&g_3,&g_103}};
    uint16_t l_1130 = 0xC713L;
    int32_t l_1186 = 1L;
    uint32_t l_1262[6][3][8] = {{{18446744073709551611UL,18446744073709551611UL,0xE983162BL,18446744073709551613UL,18446744073709551606UL,8UL,0x93F929DEL,1UL},{9UL,0x93F929DEL,0xB267212BL,18446744073709551611UL,0xB267212BL,0x93F929DEL,9UL,1UL},{0x93F929DEL,8UL,18446744073709551606UL,18446744073709551613UL,0xE983162BL,18446744073709551611UL,18446744073709551611UL,0xE983162BL}},{{0x1D9309ABL,9UL,9UL,0x1D9309ABL,0xE983162BL,0UL,1UL,18446744073709551611UL},{0x93F929DEL,0xBEF6136DL,0x86F1C4BDL,0xE983162BL,0xB267212BL,0xE983162BL,0x86F1C4BDL,0xBEF6136DL},{9UL,0xBEF6136DL,18446744073709551611UL,0x86F1C4BDL,18446744073709551606UL,0UL,18446744073709551613UL,18446744073709551613UL}},{{18446744073709551611UL,9UL,8UL,8UL,9UL,18446744073709551611UL,18446744073709551613UL,0xB267212BL},{1UL,8UL,18446744073709551611UL,0UL,0x86F1C4BDL,0x93F929DEL,0x86F1C4BDL,0xBEF6136DL},{8UL,18446744073709551613UL,8UL,0xBEF6136DL,0xE983162BL,9UL,0x86F1C4BDL,1UL}},{{0UL,0xE983162BL,0x1D9309ABL,9UL,9UL,0x1D9309ABL,0xE983162BL,0UL},{0UL,0xBEF6136DL,0xB267212BL,8UL,0xE983162BL,18446744073709551606UL,0x1D9309ABL,18446744073709551606UL},{8UL,0x93F929DEL,1UL,0x93F929DEL,8UL,18446744073709551606UL,18446744073709551613UL,0xE983162BL}},{{0x86F1C4BDL,0xBEF6136DL,0x93F929DEL,18446744073709551611UL,0x1D9309ABL,0x1D9309ABL,18446744073709551611UL,0x93F929DEL},{0xE983162BL,0xE983162BL,0x93F929DEL,0UL,0xB267212BL,9UL,18446744073709551613UL,0x86F1C4BDL},{0x1D9309ABL,18446744073709551613UL,1UL,0xE983162BL,1UL,18446744073709551613UL,0x1D9309ABL,0x86F1C4BDL}},{{18446744073709551613UL,9UL,0xB267212BL,0UL,0x93F929DEL,0xE983162BL,0xE983162BL,0x93F929DEL},{18446744073709551611UL,0x1D9309ABL,0x1D9309ABL,18446744073709551611UL,0x93F929DEL,0xBEF6136DL,0x86F1C4BDL,0xE983162BL},{18446744073709551613UL,18446744073709551606UL,8UL,0x93F929DEL,1UL,0x93F929DEL,8UL,18446744073709551606UL}}};
    int32_t l_1287 = 8L;
    uint16_t l_1288[2];
    int32_t **l_1316[3][4][6] = {{{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]}},{{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]}},{{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]},{&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0],&l_1089[6][0]}}};
    uint16_t l_1387 = 0xC282L;
    uint16_t **l_1486 = &g_1468;
    uint16_t ***l_1485 = &l_1486;
    int32_t *l_1532 = &l_929;
    int16_t **l_1541 = &g_257[0];
    uint8_t *l_1585 = (void*)0;
    uint8_t **l_1584 = &l_1585;
    uint64_t *l_1600 = (void*)0;
    uint64_t *l_1601 = (void*)0;
    uint64_t *l_1602 = (void*)0;
    uint64_t *l_1603 = &g_552;
    uint8_t *** const *l_1604[9][3][8] = {{{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{&g_318,&g_318,&g_318,(void*)0,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,(void*)0,&g_318,&g_318,(void*)0},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{(void*)0,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0}},{{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{(void*)0,&g_318,&g_318,&g_318,&g_318,(void*)0,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{&g_318,(void*)0,&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{(void*)0,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318}},{{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318,&g_318},{&g_318,&g_318,&g_318,&g_318,&g_318,(void*)0,&g_318,&g_318}}};
    int32_t *l_1607 = &g_222;
    int64_t *l_1609 = (void*)0;
    int64_t *l_1610 = &g_42[3];
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
                l_956[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 2; i++)
        l_1288[i] = 0xBE33L;
    return g_204;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t * func_28(uint16_t * p_29, uint16_t * p_30, uint16_t  p_31, float  p_32)
{ /* block id: 392 */
    uint16_t *l_908 = &g_11[0];
    return l_908;
}


/* ------------------------------------------ */
/* 
 * reads : g_97 g_103 g_288 g_727 g_99 g_575 g_576 g_126 g_316 g_317 g_182 g_222 g_698 g_330 g_670 g_122 g_775 g_776 g_505 g_3 g_804 g_515 g_516 g_268 g_829 g_577 g_11 g_823 g_859 g_272 g_204 g_202 g_2 g_200 g_127 g_907
 * writes: g_3 g_103 g_221 g_127 g_268 g_317 g_727 g_182 g_97 g_203 g_698 g_272 g_505 g_804 g_222 g_823 g_829 g_202 g_858 g_859 g_99 g_122 g_200
 */
static uint16_t * func_33(uint16_t * p_34, int64_t  p_35)
{ /* block id: 287 */
    int16_t l_716 = 0x9E0FL;
    int32_t *l_717 = &g_103;
    float *l_725 = &g_221[4];
    float **l_724[7][3][3] = {{{&l_725,&l_725,&l_725},{(void*)0,&l_725,(void*)0},{&l_725,&l_725,&l_725}},{{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725}},{{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725}},{{&l_725,&l_725,&l_725},{(void*)0,&l_725,&l_725},{(void*)0,&l_725,&l_725}},{{&l_725,&l_725,&l_725},{&l_725,&l_725,(void*)0},{&l_725,&l_725,(void*)0}},{{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725}},{{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725},{&l_725,&l_725,&l_725}}};
    float ***l_723 = &l_724[4][1][0];
    float ****l_722 = &l_723;
    uint64_t *l_739 = (void*)0;
    uint64_t l_787 = 0x229109433FE3279CLL;
    int32_t l_788 = 0x24095F35L;
    int32_t l_881 = 0L;
    int32_t l_882 = 0xF2595C66L;
    int i, j, k;
lbl_806:
    (*l_717) |= ((*g_97) = l_716);
lbl_886:
    (***g_575) = (safe_add_func_float_f_f((safe_mul_func_float_f_f(((*l_717) = (*l_717)), ((g_288[8][0] , p_35) == (((void*)0 == l_722) != (l_716 >= ((-((*l_725) = g_727[4][0][0])) , (safe_add_func_float_f_f((!((safe_mul_func_float_f_f((((0x392EA0C9L != (safe_rshift_func_int8_t_s_s((((safe_div_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(0xDBL, l_716)), p_35)) < p_35) < g_99[0]), 2))) , l_739) == l_739), p_35)) > p_35)), 0xB.2687F1p-19)))))))), p_35));
    for (g_268 = 0; (g_268 < (-4)); g_268 = safe_sub_func_int8_t_s_s(g_268, 2))
    { /* block id: 295 */
        uint8_t *****l_751 = &g_317;
        const int32_t l_752[5] = {0x8AA79369L,0x8AA79369L,0x8AA79369L,0x8AA79369L,0x8AA79369L};
        int16_t *l_757[1][5][3] = {{{(void*)0,&g_258,&g_573},{&g_258,&l_716,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_716,&g_258,&g_573},{&g_258,(void*)0,&g_258}}};
        int32_t l_758 = 0x668B6982L;
        uint8_t *** const *l_759 = &g_318;
        uint8_t *l_760 = &g_727[4][0][0];
        int64_t *l_761 = (void*)0;
        int64_t *l_762 = &g_182[1];
        int32_t **l_764 = &g_97;
        uint64_t l_883[4][6][7] = {{{0xE41179EEF28594BDLL,18446744073709551611UL,0xCD302D510396E882LL,0xBBE704C7648326E0LL,0x8F247689AACBAB24LL,0xE41179EEF28594BDLL,0x8F247689AACBAB24LL},{0xBB2272C5DBC883ABLL,0x38B381EB58800D7CLL,0x38B381EB58800D7CLL,0xBB2272C5DBC883ABLL,0xA7FAF01E1D4A8BB2LL,0x5C74B063D1928D40LL,0UL},{0UL,0xD86A5C1400692F0ELL,0x2087578F003B4A2BLL,0x553C364B36DD6B62LL,0xE41179EEF28594BDLL,0UL,18446744073709551614UL},{18446744073709551615UL,1UL,0x5B9BC5410B40DAE1LL,18446744073709551614UL,6UL,0x414E1A8EE3CAFB0ALL,0UL},{0x6A0A8CD63F4A6C57LL,0x8F247689AACBAB24LL,0x9021A0E1FEB8DCB4LL,18446744073709551607UL,18446744073709551607UL,0x9021A0E1FEB8DCB4LL,0x8F247689AACBAB24LL},{1UL,0xA7FAF01E1D4A8BB2LL,0x16BB8C728DB434C3LL,0xB1CF67D91B44F96ELL,3UL,1UL,0xDDEC09DAE3E220B8LL}},{{18446744073709551615UL,0xE41179EEF28594BDLL,0x1095B0ADC0663E77LL,0x9021A0E1FEB8DCB4LL,0xE68BE0B30654504FLL,18446744073709551609UL,18446744073709551607UL},{0x9AFC99CEBC6352E6LL,6UL,0UL,0xB1CF67D91B44F96ELL,18446744073709551614UL,0UL,0UL},{0xD86A5C1400692F0ELL,18446744073709551607UL,0x0F9CF7118A89961FLL,18446744073709551607UL,0xD86A5C1400692F0ELL,18446744073709551614UL,0x1095B0ADC0663E77LL},{0UL,3UL,0xBB2272C5DBC883ABLL,18446744073709551614UL,0UL,18446744073709551613UL,3UL},{0xE41179EEF28594BDLL,0xE68BE0B30654504FLL,0x8BB39CB79FFDF28BLL,0x553C364B36DD6B62LL,0x808B45862E3B87A1LL,0xE41179EEF28594BDLL,0UL},{0UL,18446744073709551614UL,1UL,0xBB2272C5DBC883ABLL,0x6C5B0568897A25DBLL,0xBB2272C5DBC883ABLL,1UL}},{{0xD86A5C1400692F0ELL,0xD86A5C1400692F0ELL,0xE41179EEF28594BDLL,0xBBE704C7648326E0LL,0x1095B0ADC0663E77LL,0UL,18446744073709551614UL},{0x9AFC99CEBC6352E6LL,0UL,0xB611E2787ADDEFC7LL,0xEE2FF5DE4F0043DALL,6UL,18446744073709551615UL,18446744073709551614UL},{18446744073709551615UL,0x808B45862E3B87A1LL,0x9021A0E1FEB8DCB4LL,0x5FD1D5A53642F7ADLL,0x1095B0ADC0663E77LL,0x6A0A8CD63F4A6C57LL,0x808B45862E3B87A1LL},{1UL,0x6C5B0568897A25DBLL,0x89F901D875EF126CLL,0x89F901D875EF126CLL,0x6C5B0568897A25DBLL,1UL,0UL},{0x6A0A8CD63F4A6C57LL,0x1095B0ADC0663E77LL,0x5FD1D5A53642F7ADLL,0x9021A0E1FEB8DCB4LL,0x808B45862E3B87A1LL,18446744073709551615UL,0x2087578F003B4A2BLL},{18446744073709551615UL,1UL,0UL,0xA7FAF01E1D4A8BB2LL,0x1D974426C843B394LL,0xEE2FF5DE4F0043DALL,18446744073709551614UL}},{{18446744073709551607UL,6UL,0x808B45862E3B87A1LL,18446744073709551609UL,0xE41179EEF28594BDLL,0xE41179EEF28594BDLL,18446744073709551609UL},{0xB611E2787ADDEFC7LL,6UL,0xB611E2787ADDEFC7LL,18446744073709551613UL,0x5C74B063D1928D40LL,0x5B9BC5410B40DAE1LL,6UL},{18446744073709551609UL,18446744073709551615UL,18446744073709551611UL,0x8F247689AACBAB24LL,0UL,18446744073709551609UL,0UL},{0x16BB8C728DB434C3LL,0x1D974426C843B394LL,0x5C74B063D1928D40LL,0xB611E2787ADDEFC7LL,0x414E1A8EE3CAFB0ALL,0x5B9BC5410B40DAE1LL,0UL},{0x2087578F003B4A2BLL,0xE41179EEF28594BDLL,0x9021A0E1FEB8DCB4LL,0UL,0x9021A0E1FEB8DCB4LL,0xE41179EEF28594BDLL,0x2087578F003B4A2BLL},{18446744073709551614UL,0x5C74B063D1928D40LL,0xDDEC09DAE3E220B8LL,0x1D974426C843B394LL,1UL,0xEE2FF5DE4F0043DALL,0xBB2272C5DBC883ABLL}}};
        int i, j, k;
        (*l_764) = (((((safe_mul_func_uint8_t_u_u((255UL <= ((safe_add_func_int64_t_s_s(((*l_762) = (((!(((*l_760) = (safe_add_func_uint64_t_u_u(0UL, (p_35 ^ (((safe_mod_func_int16_t_s_s((((*l_751) = (*g_316)) == ((l_752[4] ^ ((safe_div_func_uint32_t_u_u((*l_717), (safe_unary_minus_func_int64_t_s((1UL > (((!((l_758 &= p_35) , p_35)) == 1L) & g_182[7])))))) | p_35)) , l_759)), p_35)) && 3L) , 1UL))))) , 0xD806L)) ^ l_752[4]) != g_222)), (*l_717))) & 4294967291UL)), g_698[0][1][5])) != 0x031BL) < p_35) > 0xDB42BF9BL) , (*g_330));
        for (g_203 = (-3); (g_203 < (-17)); g_203--)
        { /* block id: 303 */
            float l_771 = 0xE.F1DD3Fp+92;
            int32_t l_772 = 0x9D9CB5DCL;
            int8_t *l_777 = &g_698[1][0][4];
            uint16_t l_801 = 0UL;
            int16_t **l_825 = &l_757[0][1][0];
            int16_t ***l_824 = &l_825;
            int32_t *l_906 = &l_788;
            if ((safe_lshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_s(p_35, (((((p_35 >= ((*l_760) = (((g_670[4][1] & g_122) >= ((*l_717) & p_35)) == (l_772 , (((safe_mod_func_int8_t_s_s((g_775 , ((**l_764) = ((*l_777) = g_776))), 0xE0L)) & g_505) , (-3L)))))) , l_772) , (**g_330)) & p_35) , p_35))), 9)))
            { /* block id: 307 */
                uint32_t l_789 = 4294967290UL;
                (***g_575) = ((((*l_725) = (*l_717)) <= 0x5.0F44B4p-92) <= ((((safe_add_func_float_f_f(((((safe_unary_minus_func_uint16_t_u((0x809CL != (l_788 |= (g_775 , (safe_mod_func_int8_t_s_s(((g_122 | (&g_11[0] != p_34)) > ((((l_772 = ((safe_mod_func_int64_t_s_s((safe_sub_func_uint32_t_u_u((*l_717), l_772)), l_772)) ^ 1UL)) & 1L) & l_787) && 0L)), 0x25L))))))) , g_182[8]) && (-1L)) , p_35), p_35)) > p_35) >= (*l_717)) == l_789));
                for (g_272 = (-29); (g_272 > 20); ++g_272)
                { /* block id: 314 */
                    int32_t *l_805 = &l_772;
                    for (g_505 = 0; (g_505 > 49); g_505 = safe_add_func_uint32_t_u_u(g_505, 7))
                    { /* block id: 317 */
                        uint32_t l_800 = 4294967287UL;
                        (**l_764) ^= (safe_rshift_func_int16_t_s_u((safe_mod_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_s(2UL, 0)), p_35)), 13));
                        (**l_764) &= l_800;
                        l_801++;
                    }
                    g_804 &= (**l_764);
                    l_805 = (*l_764);
                    if (g_776)
                        goto lbl_806;
                }
            }
            else
            { /* block id: 326 */
                uint64_t l_807[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_807[i] = 1UL;
                (*g_97) &= l_807[0];
            }
            for (g_222 = 1; (g_222 >= 0); g_222 -= 1)
            { /* block id: 331 */
                int8_t l_821[8] = {0x06L,0x6BL,0x06L,0x6BL,0x06L,0x6BL,0x06L,0x6BL};
                int8_t **l_822[7];
                float *l_831 = &g_221[6];
                uint32_t l_855 = 0x68E021D2L;
                uint64_t *l_899 = &g_552;
                int i;
                for (i = 0; i < 7; i++)
                    l_822[i] = &l_777;
                if (((((safe_div_func_int32_t_s_s((safe_sub_func_int32_t_s_s((!((**l_764) == (safe_div_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u(((((*l_777) = (safe_mul_func_uint8_t_u_u((safe_add_func_int16_t_s_s(0x8967L, l_821[3])), ((*g_515) == (((g_823 = l_777) != (p_35 , &g_698[2][0][7])) , l_824))))) | ((*l_760) |= ((((safe_mul_func_uint8_t_u_u((**l_764), (-9L))) < p_35) & 0x57L) | g_99[0]))) >= (**l_764)), 1)), (*l_717))))), 0UL)), g_103)) < g_268) == p_35) | 0xB3L))
                { /* block id: 335 */
                    uint8_t * const *l_843 = &l_760;
                    uint8_t * const ** const l_842 = &l_843;
                    uint8_t * const ** const *l_841[5][5][7] = {{{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,&l_842,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,(void*)0,&l_842}},{{(void*)0,&l_842,&l_842,&l_842,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,&l_842,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842}},{{(void*)0,&l_842,&l_842,(void*)0,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,&l_842,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,&l_842,&l_842,(void*)0,&l_842}},{{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,&l_842,&l_842,(void*)0,&l_842},{(void*)0,&l_842,&l_842,(void*)0,&l_842,&l_842,&l_842},{(void*)0,&l_842,(void*)0,&l_842,&l_842,(void*)0,&l_842}},{{(void*)0,&l_842,(void*)0,(void*)0,&l_842,(void*)0,&l_842},{&l_842,&l_842,&l_842,&l_842,&l_842,&l_842,&l_842},{&l_842,&l_842,(void*)0,&l_842,&l_842,(void*)0,&l_842},{(void*)0,&l_842,(void*)0,(void*)0,&l_842,(void*)0,&l_842},{&l_842,&l_842,&l_842,&l_842,&l_842,&l_842,&l_842}}};
                    int32_t l_844 = 0x4EA574E5L;
                    uint16_t *l_847 = &g_11[0];
                    int i, j, k;
                    for (l_758 = 0; (l_758 >= 0); l_758 -= 1)
                    { /* block id: 338 */
                        uint32_t *l_839[7][6][1] = {{{&g_505},{&g_204},{&g_505},{&g_204},{(void*)0},{(void*)0}},{{&g_204},{&g_505},{&g_204},{&g_505},{&g_204},{(void*)0}},{{(void*)0},{&g_204},{&g_505},{&g_204},{&g_505},{&g_204}},{{(void*)0},{(void*)0},{&g_204},{&g_505},{&g_204},{&g_505}},{{&g_204},{(void*)0},{(void*)0},{&g_204},{&g_505},{&g_204}},{{&g_505},{&g_204},{(void*)0},{(void*)0},{&g_204},{&g_505}},{{&g_204},{&g_505},{&g_204},{(void*)0},{(void*)0},{&g_204}}};
                        uint16_t *l_840 = &l_801;
                        int32_t *l_846 = &g_830[0][2];
                        int32_t **l_845 = &l_846;
                        int i, j, k;
                        (*g_97) = (((+((g_829[0] = g_829[0]) != ((*l_845) = ((g_182[(l_758 + 1)] = (((g_288[(l_758 + 2)][g_222] , (***g_577)) != l_831) | (((0x736AL <= g_11[l_758]) != (safe_add_func_int64_t_s_s((((((*l_751) = &g_318) != ((safe_rshift_func_int16_t_s_u((0x9C7C0EC5F8E2B8E2LL < (((*l_840) = (((*l_760) = ((g_505 = ((safe_add_func_uint8_t_u_u((+(((**l_764) > p_35) , p_35)), (*g_823))) , 0xE11E3AF0L)) | p_35)) <= p_35)) ^ 7L)), l_821[7])) , l_841[2][2][0])) < 0x1BL) >= l_844), 0xBF801CFCA9898B50LL))) ^ p_35))) , &g_830[0][3])))) < p_35) != l_821[3]);
                        if (p_35)
                            break;
                    }
                    if (p_35)
                        continue;
                    for (g_202 = 0; (g_202 <= 7); g_202 += 1)
                    { /* block id: 352 */
                        return l_847;
                    }
                }
                else
                { /* block id: 355 */
                    int32_t *l_848 = &l_758;
                    int32_t *l_849 = (void*)0;
                    int32_t l_850 = 0L;
                    int32_t *l_851 = &l_850;
                    int32_t *l_852 = &l_850;
                    int32_t *l_853 = &g_202;
                    int32_t *l_854[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    ++l_855;
                    for (l_801 = 0; (l_801 <= 1); l_801 += 1)
                    { /* block id: 359 */
                        uint8_t *l_870 = (void*)0;
                        uint8_t *l_871 = &g_99[0];
                        int32_t l_880[10][5][5] = {{{1L,1L,(-1L),1L,(-10L)},{0L,(-3L),(-3L),0L,0L},{0xC1460FF1L,1L,(-2L),8L,0x3B0FB83BL},{0L,1L,(-1L),9L,0L},{1L,0L,(-1L),8L,(-10L)}},{{1L,(-3L),(-1L),0L,(-5L)},{0xC1460FF1L,0L,(-2L),1L,0x3B0FB83BL},{1L,1L,(-3L),9L,(-5L)},{1L,1L,(-1L),1L,(-10L)},{0L,(-3L),(-3L),0L,0L}},{{0xC1460FF1L,1L,(-2L),8L,0x3B0FB83BL},{0L,1L,(-1L),9L,0L},{1L,0L,(-1L),8L,(-10L)},{1L,(-3L),(-1L),0L,(-5L)},{0xC1460FF1L,0L,(-2L),1L,0x3B0FB83BL}},{{1L,1L,(-3L),9L,(-5L)},{1L,1L,(-1L),1L,(-10L)},{0L,(-3L),(-3L),0L,0L},{0xC1460FF1L,1L,(-2L),8L,0x3B0FB83BL},{0L,1L,(-1L),9L,0L}},{{0xDB35B804L,8L,0x9458A943L,0L,0xFFE67060L},{0x1DF1A1E4L,9L,(-1L),0x6F39C8BCL,(-6L)},{0x67168CF8L,8L,0xC1460FF1L,1L,1L},{0x1DF1A1E4L,0L,9L,(-1L),(-6L)},{0xDB35B804L,1L,0x9458A943L,1L,0xFFE67060L}},{{0x6F39C8BCL,9L,9L,0x6F39C8BCL,(-1L)},{0x67168CF8L,1L,0xC1460FF1L,0L,1L},{0x6F39C8BCL,0L,(-1L),(-1L),(-1L)},{0xDB35B804L,8L,0x9458A943L,0L,0xFFE67060L},{0x1DF1A1E4L,9L,(-1L),0x6F39C8BCL,(-6L)}},{{0x67168CF8L,8L,0xC1460FF1L,1L,1L},{0x1DF1A1E4L,0L,9L,(-1L),(-6L)},{0xDB35B804L,1L,0x9458A943L,1L,0xFFE67060L},{0x6F39C8BCL,9L,9L,0x6F39C8BCL,(-1L)},{0x67168CF8L,1L,0xC1460FF1L,0L,1L}},{{0x6F39C8BCL,0L,(-1L),(-1L),(-1L)},{0xDB35B804L,8L,0x9458A943L,0L,0xFFE67060L},{0x1DF1A1E4L,9L,(-1L),0x6F39C8BCL,(-6L)},{0x67168CF8L,8L,0xC1460FF1L,1L,1L},{0x1DF1A1E4L,0L,9L,(-1L),(-6L)}},{{0xDB35B804L,1L,0x9458A943L,1L,0xFFE67060L},{0x6F39C8BCL,9L,9L,0x6F39C8BCL,(-1L)},{0x67168CF8L,1L,0xC1460FF1L,0L,1L},{0x6F39C8BCL,0L,(-1L),(-1L),(-1L)},{0xDB35B804L,8L,0x9458A943L,0L,0xFFE67060L}},{{0x1DF1A1E4L,9L,(-1L),0x6F39C8BCL,(-6L)},{0x67168CF8L,8L,0xC1460FF1L,1L,1L},{0x1DF1A1E4L,0L,9L,(-1L),(-6L)},{0xDB35B804L,1L,0x9458A943L,1L,0xFFE67060L},{0x6F39C8BCL,9L,9L,0x6F39C8BCL,(-1L)}}};
                        int i, j, k;
                        (*l_717) = (((5L <= ((((p_35 || ((g_858[1][0] = &g_317) != (g_859 = g_859))) == 0L) == (((*l_853) ^= ((*g_97) = ((safe_sub_func_int32_t_s_s((safe_lshift_func_int8_t_s_u((safe_add_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(((**l_764) ^ ((l_821[0] , (++(*l_760))) != (++(*l_871)))), (safe_mul_func_int16_t_s_s(0x863CL, (safe_div_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s(((*g_823) = ((**g_330) , (*g_823))), (*l_848))), g_272)))))), g_204)), p_35)), (*l_851))) != p_35))) ^ p_35)) > l_880[5][1][4])) && g_2[2][0]) <= l_821[2]);
                        l_883[3][2][4]--;
                        if (g_804)
                            goto lbl_886;
                    }
                    if (l_821[6])
                        break;
                }
                for (g_122 = 0; (g_122 >= 0); g_122 -= 1)
                { /* block id: 375 */
                    uint32_t l_889 = 0xBC1D5458L;
                    int i;
                    for (g_200 = 0; (g_200 >= 0); g_200 -= 1)
                    { /* block id: 378 */
                        float l_887 = 0x5.17EBEFp-56;
                        int32_t *l_888[9] = {&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1],&g_670[4][1]};
                        int i;
                        --l_889;
                        if (g_11[g_200])
                            break;
                    }
                    (***g_575) = ((safe_mul_func_float_f_f(g_11[g_122], (-(safe_mul_func_float_f_f((0x2.6557C6p+72 <= p_35), ((safe_div_func_float_f_f((0x1.0p-1 < (((void*)0 == l_899) , (safe_mul_func_float_f_f(p_35, ((safe_div_func_float_f_f((*l_717), (((safe_sub_func_float_f_f((((void*)0 != l_906) == (***g_575)), p_35)) <= g_907) > 0x0.C66656p-53))) >= (-0x1.0p+1)))))), 0x8.538A82p-37)) == g_11[g_122])))))) <= (-0x2.3p-1));
                }
                l_906 = (*l_764);
                (*l_717) = l_821[3];
            }
        }
        if ((*l_717))
            break;
        (*l_764) = &l_788;
    }
    return p_34;
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_42 g_11 g_96 g_2 g_99 g_97 g_103 g_116 g_122 g_272 g_330 g_575 g_577 g_200 g_576 g_126 g_127 g_316 g_317 g_318 g_319 g_274 g_202 g_159 g_288 g_258 g_670 g_182 g_698 g_700 g_552
 * writes: g_3 g_99 g_103 g_116 g_121 g_122 g_127 g_96 g_221 g_499.f0 g_573 g_575 g_552 g_203 g_204 g_258
 */
static uint16_t * func_36(uint16_t * p_37, int64_t  p_38, uint16_t * p_39)
{ /* block id: 6 */
    int32_t *l_44 = &g_3;
    int32_t **l_43[7] = {&l_44,&l_44,&l_44,&l_44,&l_44,&l_44,&l_44};
    int32_t *l_45 = &g_3;
    const int32_t l_54 = 0xCBC3393AL;
    int16_t l_701[2];
    uint32_t l_715 = 0xA757537FL;
    int i;
    for (i = 0; i < 2; i++)
        l_701[i] = 0x2C18L;
    l_45 = (void*)0;
    g_3 = p_38;
    for (g_3 = (-26); (g_3 < (-5)); ++g_3)
    { /* block id: 11 */
        uint32_t l_48[5];
        float *l_699 = (void*)0;
        int32_t l_710 = 0x91CA8814L;
        uint8_t *l_711[4] = {&g_273,&g_273,&g_273,&g_273};
        int32_t l_712 = 0xA94408F6L;
        uint32_t l_713 = 0x4A9AFE82L;
        int32_t l_714[6] = {0xAFC34005L,0x4124CBB0L,0x4124CBB0L,0xAFC34005L,0x4124CBB0L,0x4124CBB0L};
        int i;
        for (i = 0; i < 5; i++)
            l_48[i] = 4294967293UL;
        (*g_700) = (l_48[4] != (safe_mul_func_float_f_f(((safe_sub_func_float_f_f((l_48[4] , p_38), (((~(g_42[0] != (p_38 == l_54))) & 0xD8E7DFBF73ECB6AELL) , ((g_11[0] == (func_55(((safe_lshift_func_uint16_t_u_s(g_11[0], p_38)) >= p_38), g_42[3]) , p_38)) , 0x0.7p+1)))) <= p_38), p_38)));
        l_714[3] &= ((l_48[1] <= (g_552 != (l_701[1] != 0x63L))) >= (l_713 = ((65535UL != (safe_div_func_int64_t_s_s(0xAF760E8348494996LL, (safe_lshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s((l_712 = (((l_710 = (l_48[3] > (safe_add_func_int8_t_s_s((-8L), ((4294967295UL & g_182[0]) == l_48[4]))))) == 0UL) != l_48[4])), 7)), p_38))))) || 6L)));
        if (l_715)
            break;
    }
    return p_37;
}


/* ------------------------------------------ */
/* 
 * reads : g_96 g_2 g_42 g_99 g_11 g_97 g_3 g_103 g_116 g_122 g_272 g_330 g_575 g_577 g_552 g_200 g_576 g_126 g_127 g_316 g_317 g_318 g_319 g_274 g_202 g_159 g_288 g_258 g_670 g_182 g_698
 * writes: g_99 g_103 g_116 g_121 g_122 g_127 g_96 g_221 g_499.f0 g_573 g_575 g_552 g_203 g_204 g_258
 */
static int32_t  func_55(int64_t  p_56, uint8_t  p_57)
{ /* block id: 12 */
    uint32_t l_60 = 0UL;
    int16_t l_67[7];
    int64_t *l_71 = &g_42[0];
    int64_t **l_70 = &l_71;
    uint16_t *l_95[8] = {&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0],&g_11[0]};
    uint8_t *l_98 = &g_99[0];
    int32_t *l_100 = (void*)0;
    int64_t *l_101 = &g_42[0];
    int32_t *l_102 = &g_103;
    int32_t l_230 = 0xBD061763L;
    int32_t l_232 = 0x54AE1B38L;
    int32_t l_233 = (-3L);
    int32_t l_234 = 0x603C690FL;
    int32_t l_235 = (-1L);
    int32_t l_236 = 0xC671D187L;
    int32_t l_237 = 0x54C7A393L;
    int32_t l_238 = 0xF67E56BBL;
    int32_t l_239 = (-6L);
    int32_t l_240 = 0L;
    int32_t l_241 = 0x56362364L;
    int32_t l_242 = 0xBBA61EA9L;
    int8_t l_271 = 4L;
    float l_275 = 0x0.60064Fp+95;
    int32_t l_276 = 0xF9308573L;
    uint8_t l_291 = 0UL;
    uint8_t ****l_338 = &g_318;
    int32_t l_367 = (-6L);
    int16_t l_426 = (-10L);
    uint32_t l_443[2];
    int64_t l_450 = 0xCA1418A09570DAFALL;
    uint32_t l_456 = 0x6ED8F880L;
    uint16_t l_508 = 0UL;
    int32_t l_538[4][9] = {{0L,(-4L),0xB9437433L,1L,0xB9437433L,(-4L),1L,1L,0L},{0xB2941505L,0L,0xB9437433L,0L,0xB2941505L,0x50267124L,0x50267124L,0xB2941505L,0L},{1L,0x6E629093L,1L,0x50267124L,0xB9437433L,0xB9437433L,0x50267124L,1L,0x6E629093L},{0x6E629093L,1L,0x50267124L,0xB9437433L,0xB9437433L,0x50267124L,1L,0x6E629093L,1L}};
    int32_t l_555 = 5L;
    float l_630 = 0x5.6p-1;
    float l_697 = 0x0.3p-1;
    int i, j;
    for (i = 0; i < 7; i++)
        l_67[i] = 0L;
    for (i = 0; i < 2; i++)
        l_443[i] = 3UL;
    l_60++;
    if (((*l_102) = func_63(l_67[4], (safe_rshift_func_int16_t_s_u((((*l_70) = (void*)0) == &g_42[2]), (!((*l_102) = (safe_div_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u((safe_add_func_int32_t_s_s(0L, ((l_60 , func_79(((safe_add_func_uint16_t_u_u((((((safe_mod_func_uint8_t_u_u(((*l_98) = func_89((9UL | 4294967291UL), l_95[4], g_96[2], &g_42[2], l_60)), g_2[1][0])) != 0x87L) , p_57) || l_67[4]) | 0UL), 7L)) , g_42[0]), l_100, l_67[4], &l_100, l_101)) | g_11[0]))), 0UL)), p_57)))))), p_57)))
    { /* block id: 36 */
        float l_151 = (-0x2.Cp+1);
        int32_t l_152[2];
        int32_t *l_185 = &g_103;
        uint32_t l_187 = 0xC7566D79L;
        int16_t l_266[5][6][8] = {{{0xEE9CL,0xFA38L,(-9L),1L,0L,(-4L),(-1L),0xFA38L},{0x2F53L,(-1L),8L,(-6L),0L,1L,0x87CCL,0xA003L},{0xEE9CL,0L,0xEC2FL,0xFA38L,0x2F53L,0x66F5L,0L,0x66F5L},{(-2L),0xFA38L,0x291FL,0xFA38L,(-2L),0x56F0L,(-1L),0xA003L},{0xD275L,0L,0x2F53L,(-6L),(-1L),0xB2FDL,0xD275L,0xFA38L},{0xEE9CL,1L,0x2F53L,1L,0x87CCL,0x66F5L,(-1L),1L}},{{(-1L),(-1L),0x291FL,0xA003L,0L,6L,0L,0xA003L},{0xEC2FL,(-1L),0xEC2FL,1L,(-1L),0x66F5L,0x87CCL,1L},{(-2L),1L,8L,0xFA38L,0xD275L,0xB2FDL,(-1L),(-6L)},{(-2L),0L,(-9L),0xA003L,(-1L),0x56F0L,(-2L),0xFA38L},{0xEC2FL,0xFA38L,0x2F53L,0x66F5L,0L,0x66F5L,0x2F53L,0xFA38L},{(-1L),0L,8L,0xA003L,0x87CCL,1L,0L,(-6L)}},{{0xEE9CL,(-1L),(-3L),0xFA38L,(-1L),(-4L),0L,1L},{0xD275L,0xFA38L,8L,1L,(-2L),0xB2FDL,0x2F53L,0xA003L},{(-2L),0xB2FDL,0x2F53L,0xA003L,0x2F53L,0xB2FDL,(-2L),1L},{0xEE9CL,0xFA38L,(-9L),1L,0L,(-4L),(-1L),0xFA38L},{0x2F53L,(-1L),8L,(-6L),0L,1L,0x87CCL,0xA003L},{0xEE9CL,0L,0xEC2FL,0xFA38L,0x2F53L,0x66F5L,0L,0x66F5L}},{{(-2L),0xFA38L,0x291FL,0xFA38L,(-2L),0x56F0L,(-1L),0xA003L},{0xD275L,0L,0x2F53L,(-6L),(-1L),0xB2FDL,0xD275L,0xFA38L},{0xEE9CL,(-4L),(-3L),(-6L),8L,1L,0xEC2FL,(-4L)},{0xEC2FL,0xB2FDL,0L,0L,0xD275L,1L,0xD275L,0L},{0x291FL,0xB2FDL,0x291FL,(-4L),0xEC2FL,1L,8L,(-6L)},{0x2F53L,(-4L),8L,0x66F5L,(-9L),6L,0xEC2FL,(-1L)}},{{0x2F53L,1L,0xEE9CL,0L,0xEC2FL,0xFA38L,0x2F53L,0x66F5L},{0x291FL,0x66F5L,(-3L),1L,0xD275L,1L,(-3L),0x66F5L},{0xEC2FL,0x56F0L,8L,0L,8L,(-4L),0xD275L,(-1L)},{8L,0xB2FDL,(-1L),0x66F5L,0xEC2FL,0xA003L,0xD275L,(-6L)},{(-9L),0x66F5L,8L,(-4L),0x2F53L,6L,(-3L),0L},{0x2F53L,6L,(-3L),0L,(-3L),6L,0x2F53L,(-4L)}}};
        int32_t l_277 = 0x943AF525L;
        int64_t l_278[5] = {0x56E9C9E93917A0F0LL,0x56E9C9E93917A0F0LL,0x56E9C9E93917A0F0LL,0x56E9C9E93917A0F0LL,0x56E9C9E93917A0F0LL};
        uint32_t l_368 = 0x6ABEB55AL;
        int8_t *l_398 = (void*)0;
        uint32_t l_420 = 0xAAC9A1FCL;
        int8_t *l_424 = &g_274[0];
        int32_t l_425 = 0x15E38941L;
        float *l_431 = (void*)0;
        uint16_t *l_439[1];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_152[i] = (-1L);
        for (i = 0; i < 1; i++)
            l_439[i] = (void*)0;
        for (g_122 = (-3); (g_122 >= (-5)); g_122--)
        { /* block id: 39 */
            uint16_t l_140[5] = {0x319BL,0x319BL,0x319BL,0x319BL,0x319BL};
            int32_t ***l_147 = &g_96[2];
            int32_t ***l_150 = &g_96[1];
            int32_t l_190 = (-8L);
            int32_t *l_223 = &l_152[0];
            int32_t l_224 = 2L;
            int32_t l_225 = 0xB7C538D4L;
            int32_t l_226 = 0x8E3F2653L;
            int32_t l_227 = 0x3E7CFBBAL;
            int32_t l_228[7] = {0L,0L,0L,0L,0L,0L,0L};
            int64_t **l_290[8] = {&l_101,&l_101,&l_101,&l_101,&l_101,&l_101,&l_101,&l_101};
            uint64_t * const l_311 = &g_116;
            uint8_t **l_315 = &l_98;
            uint8_t ***l_314 = &l_315;
            uint8_t ****l_313 = &l_314;
            uint16_t l_387 = 0xA5CEL;
            int32_t ***** const l_404[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int8_t l_407 = 1L;
            uint64_t *l_551 = &g_552;
            int16_t *l_556 = &l_266[2][5][7];
            int i;
            for (p_56 = 0; (p_56 != (-5)); --p_56)
            { /* block id: 42 */
                int32_t ***l_133 = &g_96[2];
                int32_t *l_134 = &g_103;
                int32_t *l_135 = &g_103;
                int32_t *l_136 = &g_103;
                int32_t *l_137 = &g_103;
                int32_t *l_138 = &g_103;
                int32_t *l_139 = &g_103;
                (*l_133) = &g_97;
                l_140[2]++;
            }
        }
        l_100 = &l_152[0];
    }
    else
    { /* block id: 226 */
        float *l_567 = &g_221[3];
        float *l_570 = &g_499.f0;
        float *l_571[3][2][8] = {{{&g_127[2],(void*)0,&g_127[2],&g_127[6],&g_127[6],&g_127[2],(void*)0,&g_127[2]},{(void*)0,&g_127[6],&g_127[6],&g_127[6],(void*)0,(void*)0,&g_127[6],&g_127[6]}},{{(void*)0,(void*)0,&g_127[6],&g_127[6],&g_127[6],(void*)0,(void*)0,&g_127[6]},{&g_127[2],&g_127[6],&g_127[6],&g_127[2],(void*)0,&g_127[2],&g_127[6],&g_127[6]}},{{&g_127[6],(void*)0,&g_127[6],&g_127[6],(void*)0,&g_127[6],(void*)0,&g_127[6]},{&g_127[2],(void*)0,&g_127[2],&g_127[6],&g_127[6],&g_127[2],(void*)0,&g_127[2]}}};
        int32_t l_572[2][4][6] = {{{(-1L),(-9L),0xC4E9A1AFL,(-9L),(-1L),(-1L)},{(-9L),(-9L),(-9L),(-9L),0L,(-9L)},{(-9L),0L,(-9L),(-9L),(-9L),(-9L)},{(-1L),(-1L),(-9L),0xC4E9A1AFL,(-9L),(-1L)}},{{(-9L),0L,0xC4E9A1AFL,0xC4E9A1AFL,0L,(-9L)},{(-1L),(-9L),0xC4E9A1AFL,(-9L),(-9L),(-9L)},{0xC4E9A1AFL,(-9L),(-9L),0xC4E9A1AFL,(-1L),0xC4E9A1AFL},{0xC4E9A1AFL,(-1L),0xC4E9A1AFL,(-9L),(-9L),0xC4E9A1AFL}}};
        int32_t l_574 = (-4L);
        int64_t l_619 = 0xE50AC199CF226F6FLL;
        int8_t l_638 = 0xB7L;
        int8_t l_640 = 0xC8L;
        uint8_t l_641 = 0x13L;
        int64_t l_676 = 0L;
        int i, j, k;
        if ((((((safe_rshift_func_uint16_t_u_s(g_122, ((p_57 , 250UL) & (g_99[0] == (g_2[1][0] != (((~((g_573 = (safe_div_func_float_f_f((safe_mul_func_float_f_f((l_572[1][2][3] = (g_127[6] = (!(safe_sub_func_float_f_f(((g_11[0] <= (((*l_567) = (-0x7.8p-1)) != g_272)) == (!((*l_570) = (!(0x0.6p+1 == g_272))))), 0x1.Ap-1))))), g_122)), g_42[0]))) , 0x1DL)) ^ (*g_97)) ^ l_574)))))) || (*l_102)) == g_11[0]) == p_56) , (**g_330)))
        { /* block id: 232 */
            (*g_577) = g_575;
            return l_572[0][2][2];
        }
        else
        { /* block id: 235 */
            uint64_t l_581 = 0xD9C593780470D5ACLL;
            const int32_t l_626 = (-1L);
            int32_t l_627[3];
            uint16_t l_650 = 65535UL;
            uint8_t l_678 = 255UL;
            int32_t *l_692[10] = {&l_555,&l_555,&l_555,&l_555,&l_555,&l_555,&l_555,&l_555,&l_555,&l_555};
            int16_t *l_693 = &g_258;
            int32_t l_694 = 0xA8CEB0D1L;
            uint16_t **l_696 = &l_95[4];
            uint16_t ***l_695 = &l_696;
            int i;
            for (i = 0; i < 3; i++)
                l_627[i] = (-1L);
            for (g_552 = 0; (g_552 <= 21); g_552 = safe_add_func_uint32_t_u_u(g_552, 4))
            { /* block id: 238 */
                int8_t l_582[2];
                int32_t l_631 = 0x943659E9L;
                int32_t l_632 = 1L;
                int32_t l_633 = 0xB4D186A0L;
                int32_t l_634 = 1L;
                int32_t l_635 = (-1L);
                int32_t l_636 = 8L;
                int32_t l_637 = 0x10215176L;
                int32_t l_639[6] = {4L,0x2D3ADE36L,4L,4L,0x2D3ADE36L,4L};
                int32_t *l_662 = &l_242;
                int32_t *l_663 = &l_636;
                int32_t *l_664 = &l_632;
                int32_t *l_665 = &l_632;
                int32_t *l_666 = (void*)0;
                int32_t *l_667 = &l_367;
                int32_t *l_668 = &l_240;
                int32_t *l_669 = &l_538[1][7];
                int32_t *l_671 = &g_670[4][4];
                int32_t *l_672 = &l_241;
                int32_t *l_673 = (void*)0;
                int32_t *l_674 = &l_538[0][4];
                int32_t *l_675[4];
                int32_t l_677 = 1L;
                int i;
                for (i = 0; i < 2; i++)
                    l_582[i] = 0x34L;
                for (i = 0; i < 4; i++)
                    l_675[i] = &l_627[2];
                if ((((p_56 > p_57) <= (-1L)) > (l_582[0] = (!(p_57 < (p_56 >= l_581))))))
                { /* block id: 240 */
                    float l_590 = 0x4.CF8857p+48;
                    for (l_230 = 0; (l_230 != 9); ++l_230)
                    { /* block id: 243 */
                        int8_t l_587[5] = {0x00L,0x00L,0x00L,0x00L,0x00L};
                        int16_t *l_597 = &l_67[4];
                        int i;
                        (*l_102) ^= (l_587[0] & ((**g_330) == (p_56 <= 0xF101L)));
                        (***g_575) = (((((safe_add_func_float_f_f((p_56 >= ((((*l_567) = (l_590 < p_56)) <= l_572[1][2][3]) == (0x8.9D4692p-71 == (safe_add_func_float_f_f(((*l_570) = (safe_add_func_float_f_f(((safe_mul_func_int16_t_s_s(((*l_597) &= l_581), g_200)) , (p_57 < (((safe_sub_func_float_f_f((((safe_mul_func_float_f_f((safe_add_func_float_f_f(0x1.F11F1Ep+30, p_56)), 0x1.5p+1)) > l_582[1]) == 0x1.Ap+1), l_581)) >= l_572[0][1][4]) > 0x0.72B297p+46))), p_57))), (-0x3.Cp+1)))))), (****g_577))) < l_587[0]) , 9UL) , l_582[0]) == p_57);
                        if (p_57)
                            continue;
                    }
                }
                else
                { /* block id: 251 */
                    int32_t l_616 = (-4L);
                    int64_t **l_620[8][10] = {{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]},{&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2],&g_121[0][6][1],&g_121[2][4][2],&g_121[1][3][3],&g_121[1][3][3],&g_121[2][4][2]}};
                    uint8_t **l_622[6] = {&l_98,&l_98,&l_98,&l_98,&l_98,&l_98};
                    int8_t *l_623 = &g_203;
                    uint64_t *l_625 = &l_581;
                    int32_t *l_628 = &g_202;
                    int32_t *l_629[10] = {&g_103,&g_103,&g_222,&g_103,&g_103,&g_222,&g_103,&g_103,&g_222,&g_103};
                    int i, j;
                    l_627[2] |= (safe_add_func_int64_t_s_s((safe_div_func_int8_t_s_s((l_574 &= (safe_mul_func_int16_t_s_s(((((safe_add_func_int16_t_s_s((((*l_625) ^= (((safe_lshift_func_int16_t_s_s((safe_div_func_uint16_t_u_u(((l_616 <= (safe_rshift_func_uint16_t_u_u(((l_619 <= ((*l_623) = ((((l_620[3][3] == &l_71) , (+l_572[1][2][3])) , l_622[4]) == (***g_316)))) , g_274[0]), 10))) == (safe_unary_minus_func_int8_t_s((l_582[0] | g_274[2])))), (-1L))), p_57)) , g_103) , l_616)) <= (-10L)), 1UL)) , 1L) ^ 0x7F76L) != p_57), 0xBD12L))), l_626)), l_626));
                    l_641--;
                    for (l_234 = 10; (l_234 <= 12); l_234 = safe_add_func_uint16_t_u_u(l_234, 1))
                    { /* block id: 259 */
                        uint16_t l_655 = 0UL;
                        int32_t *l_658 = &l_241;
                        int32_t **l_660 = (void*)0;
                        int32_t **l_661 = &l_629[6];
                        if (p_56)
                            break;
                        (*l_102) ^= (((g_204 = ((*l_628) , (safe_lshift_func_int8_t_s_u(((safe_mod_func_uint16_t_u_u((l_650 > (p_56 ^ 18446744073709551615UL)), (safe_rshift_func_int8_t_s_u(((((((safe_lshift_func_uint8_t_u_u(p_56, 5)) == (l_634 |= l_655)) < (((safe_sub_func_float_f_f(p_57, (***g_575))) < p_57) , p_56)) , g_159) | l_619) <= g_274[1]), 2)))) & p_56), 4)))) != l_639[3]) >= p_57);
                        (*l_661) = l_658;
                    }
                }
                --l_678;
            }
            l_572[0][3][0] ^= (safe_add_func_int16_t_s_s(((((((*l_695) = (((((***g_575) = (l_626 >= ((l_694 = ((l_574 |= (0x3FL > ((+(safe_div_func_int16_t_s_s((((safe_mod_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s(((p_57 | ((void*)0 == l_567)) & (*g_97)), ((*l_693) &= (((g_288[8][0] , ((l_627[2] = (safe_mod_func_uint64_t_u_u(0x80750F193DA0B4B6LL, (p_56 & p_57)))) , (*l_102))) , p_57) < 18446744073709551606UL)))) || g_670[4][1]), g_182[8])) || g_3) <= 0x9F07L), p_57))) , l_626))) , (-0x1.4p+1))) == p_57))) > 0xE.1311BCp+38) >= p_57) , (void*)0)) == (void*)0) & 0UL) & 0xDCA6L) == l_678), l_626));
        }
    }
    return g_698[0][1][5];
}


/* ------------------------------------------ */
/* 
 * reads : g_97 g_3 g_42 g_11 g_103 g_116 g_99 g_122 g_2
 * writes: g_116 g_121 g_122 g_127 g_103
 */
static int32_t  func_63(uint64_t  p_64, int32_t  p_65, int64_t  p_66)
{ /* block id: 21 */
    int32_t *l_108 = &g_103;
    for (p_66 = (-23); (p_66 >= (-27)); p_66 = safe_sub_func_int32_t_s_s(p_66, 6))
    { /* block id: 24 */
        uint64_t *l_115 = &g_116;
        int32_t l_123 = (-1L);
        int64_t *l_124 = &g_122;
        int32_t l_125 = 0x2C044B22L;
        float *l_128 = &g_127[0];
        if ((*g_97))
            break;
        (*l_128) = (((safe_sub_func_uint8_t_u_u(0x8CL, ((((l_108 == l_108) & (p_66 & g_42[0])) < ((*l_124) ^= ((safe_lshift_func_uint16_t_u_s(g_11[0], ((*l_108) > (p_65 = (safe_sub_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u((l_123 ^= (((*l_115)--) || ((safe_lshift_func_int8_t_s_s((((g_121[1][3][3] = l_115) == &g_122) < g_99[0]), 3)) & 18446744073709551609UL))), 6)), (-8L))))))) && g_42[0]))) != l_125))) , (-0x4.4p-1)) < g_2[0][0]);
        (*l_108) &= 0L;
    }
    return (*l_108);
}


/* ------------------------------------------ */
/* 
 * reads : g_99
 * writes:
 */
static int8_t  func_79(uint32_t  p_80, int32_t * p_81, int64_t  p_82, int32_t ** p_83, int64_t * p_84)
{ /* block id: 18 */
    return g_99[0];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_89(int16_t  p_90, uint16_t * p_91, int32_t ** p_92, const int64_t * p_93, int32_t  p_94)
{ /* block id: 15 */
    return p_94;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2[i][j], "g_2[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_11[i], "g_11[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_42[i], "g_42[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_99[i], "g_99[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc_bytes(&g_127[i], sizeof(g_127[i]), "g_127[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_159, "g_159", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_182[i], "g_182[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_200, "g_200", print_hash_value);
    transparent_crc(g_202, "g_202", print_hash_value);
    transparent_crc(g_203, "g_203", print_hash_value);
    transparent_crc(g_204, "g_204", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc_bytes(&g_221[i], sizeof(g_221[i]), "g_221[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_222, "g_222", print_hash_value);
    transparent_crc(g_258, "g_258", print_hash_value);
    transparent_crc(g_268, "g_268", print_hash_value);
    transparent_crc(g_272, "g_272", print_hash_value);
    transparent_crc(g_273, "g_273", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_274[i], "g_274[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_285.f0, sizeof(g_285.f0), "g_285.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc_bytes(&g_288[i][j].f0, sizeof(g_288[i][j].f0), "g_288[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_498[i], sizeof(g_498[i]), "g_498[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_499.f0, sizeof(g_499.f0), "g_499.f0", print_hash_value);
    transparent_crc(g_505, "g_505", print_hash_value);
    transparent_crc(g_552, "g_552", print_hash_value);
    transparent_crc(g_573, "g_573", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_670[i][j], "g_670[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_698[i][j][k], "g_698[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_727[i][j][k], "g_727[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_775.f0, sizeof(g_775.f0), "g_775.f0", print_hash_value);
    transparent_crc(g_776, "g_776", print_hash_value);
    transparent_crc(g_804, "g_804", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_830[i][j], "g_830[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_907, "g_907", print_hash_value);
    transparent_crc(g_1042, "g_1042", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_1055[i].f0, sizeof(g_1055[i].f0), "g_1055[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1118, "g_1118", print_hash_value);
    transparent_crc(g_1121, "g_1121", print_hash_value);
    transparent_crc(g_1233, "g_1233", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1278[i][j][k], "g_1278[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1281[i][j], "g_1281[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1311, "g_1311", print_hash_value);
    transparent_crc(g_1367, "g_1367", print_hash_value);
    transparent_crc_bytes (&g_1373.f0, sizeof(g_1373.f0), "g_1373.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc_bytes(&g_1496[i][j].f0, sizeof(g_1496[i][j].f0), "g_1496[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1618.f0, sizeof(g_1618.f0), "g_1618.f0", print_hash_value);
    transparent_crc(g_1745, "g_1745", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 476
XXX total union variables: 8

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 36
breakdown:
   depth: 1, occurrence: 114
   depth: 2, occurrence: 23
   depth: 3, occurrence: 3
   depth: 4, occurrence: 2
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 3
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1

XXX total number of pointers: 424

XXX times a variable address is taken: 949
XXX times a pointer is dereferenced on RHS: 151
breakdown:
   depth: 1, occurrence: 109
   depth: 2, occurrence: 31
   depth: 3, occurrence: 7
   depth: 4, occurrence: 4
XXX times a pointer is dereferenced on LHS: 254
breakdown:
   depth: 1, occurrence: 216
   depth: 2, occurrence: 19
   depth: 3, occurrence: 18
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 29
XXX times a pointer is compared with address of another variable: 7
XXX times a pointer is compared with another pointer: 8
XXX times a pointer is qualified to be dereferenced: 6568

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 768
   level: 2, occurrence: 180
   level: 3, occurrence: 197
   level: 4, occurrence: 104
   level: 5, occurrence: 23
XXX number of pointers point to pointers: 188
XXX number of pointers point to scalars: 233
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.5
XXX average alias set size: 1.45

XXX times a non-volatile is read: 1350
XXX times a non-volatile is write: 763
XXX times a volatile is read: 70
XXX    times read thru a pointer: 18
XXX times a volatile is write: 50
XXX    times written thru a pointer: 31
XXX times a volatile is available for access: 3.91e+03
XXX percentage of non-volatile access: 94.6

XXX forward jumps: 0
XXX backward jumps: 6

XXX stmts: 116
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 23
   depth: 1, occurrence: 17
   depth: 2, occurrence: 12
   depth: 3, occurrence: 15
   depth: 4, occurrence: 23
   depth: 5, occurrence: 26

XXX percentage a fresh-made variable is used: 16.2
XXX percentage an existing variable is used: 83.8
********************* end of statistics **********************/

